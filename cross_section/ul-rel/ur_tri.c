#include "../h/nlo_functions.h"
#include "../h/dilog.h"

#define Pi       3.141592653589793238462643383279502884L
#define Power pq



long double ur_tri(double *vars)
{
    long double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    long double aG=1/Power(4.*Pi,2);
    long double a=aG*((8*(-(Power(s1,6)*(Power(s2,2) - Power(t1,2))*t2*
            (s2*(t1 - t2) + t1*(-7*t1 + 8*t2))) - 
         Power(s1,5)*(s2 - t1)*
          (Power(t1,2)*t2*(2*Power(t1,2) + 39*t1*t2 - 42*Power(t2,2)) + 
            Power(s2,3)*(2*Power(t1,2) - t1*t2 - Power(t2,2)) + 
            s2*t1*(2*Power(t1,3) - 40*Power(t1,2)*t2 + 
               51*t1*Power(t2,2) - 14*Power(t2,3)) + 
            Power(s2,2)*(-4*Power(t1,3) + 19*Power(t1,2)*t2 - 
               20*t1*Power(t2,2) + 4*Power(t2,3))) + 
         s2*(s2 - t1)*t1*Power(t2,3)*
          (Power(s2,4)*(8*t1 - 7*t2) + 
            Power(s2,3)*(-34*Power(t1,2) + 32*t1*t2 + 2*Power(t2,2)) + 
            2*t1*Power(t1 - t2,2)*
             (10*Power(t1,2) - 15*t1*t2 + 11*Power(t2,2)) + 
            Power(s2,2)*(56*Power(t1,3) - 102*Power(t1,2)*t2 + 
               47*t1*Power(t2,2) - 2*Power(t2,3)) + 
            s2*(-50*Power(t1,4) + 140*Power(t1,3)*t2 - 
               153*Power(t1,2)*Power(t2,2) + 74*t1*Power(t2,3) - 
               11*Power(t2,4))) + 
         Power(s1,4)*(-5*Power(s2,4)*Power(t1 - t2,2)*(t1 + t2) + 
            Power(s2,5)*(Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
            Power(t1,3)*t2*(-2*Power(t1,3) + 45*Power(t1,2)*t2 - 
               134*t1*Power(t2,2) + 90*Power(t2,3)) + 
            s2*Power(t1,2)*(2*Power(t1,4) - 37*Power(t1,3)*t2 + 
               91*Power(t1,2)*Power(t2,2) + 27*t1*Power(t2,3) - 
               85*Power(t2,4)) + 
            Power(s2,2)*t1*(-7*Power(t1,4) + 67*Power(t1,3)*t2 - 
               221*Power(t1,2)*Power(t2,2) + 174*t1*Power(t2,3) - 
               10*Power(t2,4)) + 
            Power(s2,3)*(9*Power(t1,4) - 34*Power(t1,3)*t2 + 
               82*Power(t1,2)*Power(t2,2) - 63*t1*Power(t2,3) + 
               6*Power(t2,4))) + 
         Power(s1,3)*(Power(s2,6)*t1*(t1 - t2) + 
            Power(s2,5)*(-4*Power(t1,3) + 19*Power(t1,2)*t2 - 
               20*t1*Power(t2,2) + 6*Power(t2,3)) + 
            Power(t1,3)*t2*(-11*Power(t1,4) + 76*Power(t1,3)*t2 - 
               200*Power(t1,2)*Power(t2,2) + 242*t1*Power(t2,3) - 
               106*Power(t2,4)) + 
            Power(s2,4)*(6*Power(t1,4) - 63*Power(t1,3)*t2 + 
               82*Power(t1,2)*Power(t2,2) - 34*t1*Power(t2,3) + 
               9*Power(t2,4)) + 
            3*s2*Power(t1,2)*t2*
             (8*Power(t1,4) - 20*Power(t1,3)*t2 + 
               31*Power(t1,2)*Power(t2,2) - 58*t1*Power(t2,3) + 
               40*Power(t2,4)) + 
            Power(s2,2)*t1*(Power(t1,5) - 55*Power(t1,4)*t2 + 
               9*Power(t1,3)*Power(t2,2) + 220*Power(t1,2)*Power(t2,3) - 
               160*t1*Power(t2,4) - 12*Power(t2,5)) - 
            Power(s2,3)*(4*Power(t1,5) - 87*Power(t1,4)*t2 + 
               87*Power(t1,3)*Power(t2,2) + 87*Power(t1,2)*Power(t2,3) - 
               87*t1*Power(t2,4) + 4*Power(t2,5))) + 
         s1*Power(t2,2)*(Power(s2,6)*t1*(-t1 + t2) - 
            2*Power(t1,3)*Power(t1 - t2,2)*t2*
             (11*Power(t1,2) - 15*t1*t2 + 10*Power(t2,2)) + 
            2*Power(s2,5)*(14*Power(t1,3) + 6*Power(t1,2)*t2 - 
               21*t1*Power(t2,2) + Power(t2,3)) + 
            3*s2*Power(t1,2)*Power(t1 - t2,2)*
             (10*Power(t1,3) - Power(t1,2)*t2 - t1*Power(t2,2) + 
               10*Power(t2,3)) + 
            Power(s2,4)*(-85*Power(t1,4) + 27*Power(t1,3)*t2 + 
               91*Power(t1,2)*Power(t2,2) - 37*t1*Power(t2,3) + 
               2*Power(t2,4)) + 
            3*Power(s2,3)*t1*(40*Power(t1,4) - 58*Power(t1,3)*t2 + 
               31*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
               8*Power(t2,4)) - 
            Power(s2,2)*t1*(92*Power(t1,5) - 219*Power(t1,4)*t2 + 
               249*Power(t1,3)*Power(t2,2) - 
               162*Power(t1,2)*Power(t2,3) + 31*t1*Power(t2,4) + 
               10*Power(t2,5))) + 
         Power(s1,2)*t2*(Power(s2,6)*t1*(-8*t1 + 7*t2) + 
            Power(s2,5)*(18*Power(t1,3) - 71*Power(t1,2)*t2 + 
               59*t1*Power(t2,2) - 6*Power(t2,3)) + 
            Power(s2,4)*(-10*Power(t1,4) + 174*Power(t1,3)*t2 - 
               221*Power(t1,2)*Power(t2,2) + 67*t1*Power(t2,3) - 
               7*Power(t2,4)) + 
            Power(t1,3)*t2*(33*Power(t1,4) - 148*Power(t1,3)*t2 + 
               255*Power(t1,2)*Power(t2,2) - 210*t1*Power(t2,3) + 
               70*Power(t2,4)) + 
            Power(s2,3)*(-12*Power(t1,5) - 160*Power(t1,4)*t2 + 
               220*Power(t1,3)*Power(t2,2) + 9*Power(t1,2)*Power(t2,3) - 
               55*t1*Power(t2,4) + Power(t2,5)) + 
            2*Power(s2,2)*t1*(11*Power(t1,5) + 24*Power(t1,4)*t2 - 
               36*Power(t1,3)*Power(t2,2) - 36*Power(t1,2)*Power(t2,3) + 
               24*t1*Power(t2,4) + 11*Power(t2,5)) - 
            s2*Power(t1,2)*(10*Power(t1,5) + 31*Power(t1,4)*t2 - 
               162*Power(t1,3)*Power(t2,2) + 
               249*Power(t1,2)*Power(t2,3) - 219*t1*Power(t2,4) + 
               92*Power(t2,5))) + 
         Power(s,6)*(Power(t1,2) - Power(t2,2))*
          (Power(s1,2)*(8*s2 - 7*t1) + (7*s2 - 6*t1)*t2*(s2 - t1 + t2) + 
            s1*(-8*Power(s2,2) + 15*s2*(t1 - t2) + t1*(-7*t1 + 13*t2))) - 
         Power(s,5)*(t1 - t2)*
          (2*Power(s1,3)*(2*Power(s2,2) + 5*s2*(2*t1 + t2) - 
               t1*(11*t1 + 5*t2)) + 
            Power(s1,2)*(-4*Power(s2,3) - 6*Power(s2,2)*(t1 - t2) + 
               s2*(9*Power(t1,2) - 58*t1*t2 - 41*Power(t2,2)) + 
               t1*(2*Power(t1,2) + 45*t1*t2 + 40*Power(t2,2))) + 
            t2*(2*Power(s2,3)*(5*t1 + 11*t2) - 
               Power(s2,2)*(40*Power(t1,2) + 45*t1*t2 + 2*Power(t2,2)) + 
               s2*(53*Power(t1,3) + 7*Power(t1,2)*t2 + 
                  16*t1*Power(t2,2) - 24*Power(t2,3)) + 
               t1*(-23*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  17*t1*Power(t2,2) + 23*Power(t2,3))) - 
            s1*(10*Power(s2,3)*(t1 + 2*t2) + 
               Power(s2,2)*(-41*Power(t1,2) - 58*t1*t2 + 9*Power(t2,2)) + 
               s2*(55*Power(t1,3) + 24*Power(t1,2)*t2 - 
                  24*t1*Power(t2,2) - 55*Power(t2,3)) + 
               t1*(-24*Power(t1,3) + 16*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) + 53*Power(t2,3)))) + 
         Power(s,4)*(Power(s1,4)*
             (Power(s2,2)*(7*t1 - 6*t2) + 
               s2*(24*Power(t1,2) - 20*t1*t2 - 6*Power(t2,2)) + 
               t1*(-30*Power(t1,2) + 24*t1*t2 + 7*Power(t2,2))) - 
            Power(s1,3)*(Power(s2,3)*(t1 + t2) + 
               Power(s2,2)*(-26*Power(t1,2) - 9*t1*t2 + 
                  33*Power(t2,2)) + 
               s2*(62*Power(t1,3) + 56*Power(t1,2)*t2 - 
                  77*t1*Power(t2,2) - 42*Power(t2,3)) + 
               t1*(-37*Power(t1,3) - 42*Power(t1,2)*t2 + 
                  34*t1*Power(t2,2) + 46*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,4)*(-6*t1 + 7*t2) + 
               Power(s2,3)*(-33*Power(t1,2) + 9*t1*t2 + 
                  26*Power(t2,2)) + 
               Power(s2,2)*(119*Power(t1,3) - 121*Power(t1,2)*t2 - 
                  121*t1*Power(t2,2) + 119*Power(t2,3)) + 
               s2*(-122*Power(t1,4) + 255*Power(t1,3)*t2 + 
                  52*Power(t1,2)*Power(t2,2) - 94*t1*Power(t2,3) - 
                  90*Power(t2,4)) + 
               t1*(40*Power(t1,4) - 152*Power(t1,3)*t2 + 
                  59*Power(t1,2)*Power(t2,2) - 43*t1*Power(t2,3) + 
                  96*Power(t2,4))) + 
            t2*(Power(s2,4)*(7*Power(t1,2) + 24*t1*t2 - 
                  30*Power(t2,2)) + 
               2*t1*Power(t1 - t2,2)*
                (14*Power(t1,3) + 11*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,3)*(-46*Power(t1,3) - 34*Power(t1,2)*t2 + 
                  42*t1*Power(t2,2) + 37*Power(t2,3)) + 
               Power(s2,2)*(96*Power(t1,4) - 43*Power(t1,3)*t2 + 
                  59*Power(t1,2)*Power(t2,2) - 152*t1*Power(t2,3) + 
                  40*Power(t2,4)) - 
               s2*(85*Power(t1,5) - 85*Power(t1,4)*t2 + 
                  79*Power(t1,3)*Power(t2,2) - 
                  116*Power(t1,2)*Power(t2,3) + 10*t1*Power(t2,4) + 
                  27*Power(t2,5))) - 
            s1*(Power(s2,4)*(6*Power(t1,2) + 20*t1*t2 - 24*Power(t2,2)) - 
               3*s2*Power(t1 - t2,2)*
                (27*Power(t1,3) + 71*Power(t1,2)*t2 + 
                  71*t1*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s2,3)*(-42*Power(t1,3) - 77*Power(t1,2)*t2 + 
                  56*t1*Power(t2,2) + 62*Power(t2,3)) + 
               Power(s2,2)*(90*Power(t1,4) + 94*Power(t1,3)*t2 - 
                  52*Power(t1,2)*Power(t2,2) - 255*t1*Power(t2,3) + 
                  122*Power(t2,4)) + 
               t1*(27*Power(t1,5) + 10*Power(t1,4)*t2 - 
                  116*Power(t1,3)*Power(t2,2) + 
                  79*Power(t1,2)*Power(t2,3) - 85*t1*Power(t2,4) + 
                  85*Power(t2,5)))) + 
         Power(s,3)*(-2*Power(s1,5)*
             (5*Power(s2,2)*(t1 - t2) + 
               s2*(6*Power(t1,2) - 5*t1*t2 - 2*Power(t2,2)) + 
               t1*(-11*Power(t1,2) + 10*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,4)*(18*Power(s2,3)*(t1 - t2) + 
               Power(s2,2)*(-84*Power(t1,2) + 79*t1*t2 + 
                  7*Power(t2,2)) + 
               s2*(114*Power(t1,3) - 10*Power(t1,2)*t2 - 
                  91*t1*Power(t2,2) - 19*Power(t2,3)) + 
               t1*(-47*Power(t1,3) - 52*Power(t1,2)*t2 + 
                  82*t1*Power(t2,2) + 21*Power(t2,3))) + 
            Power(s1,3)*(-18*Power(s2,4)*(t1 - t2) + 
               Power(s2,3)*(79*Power(t1,2) - 166*t1*t2 + 
                  79*Power(t2,2)) - 
               Power(s2,2)*(120*Power(t1,3) - 399*Power(t1,2)*t2 + 
                  150*t1*Power(t2,2) + 121*Power(t2,3)) + 
               t1*(-4*Power(t1,4) + 109*Power(t1,3)*t2 + 
                  72*Power(t1,2)*Power(t2,2) - 117*t1*Power(t2,3) - 
                  62*Power(t2,4)) + 
               s2*(65*Power(t1,4) - 366*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) + 246*t1*Power(t2,3) + 
                  54*Power(t2,4))) + 
            Power(s1,2)*(10*Power(s2,5)*(t1 - t2) + 
               Power(s2,4)*(7*Power(t1,2) + 79*t1*t2 - 84*Power(t2,2)) - 
               Power(s2,3)*(121*Power(t1,3) + 150*Power(t1,2)*t2 - 
                  399*t1*Power(t2,2) + 120*Power(t2,3)) + 
               Power(s2,2)*(218*Power(t1,4) + 35*Power(t1,3)*t2 - 
                  518*Power(t1,2)*Power(t2,2) + 35*t1*Power(t2,3) + 
                  218*Power(t2,4)) + 
               s2*(-169*Power(t1,5) + 174*Power(t1,4)*t2 + 
                  146*Power(t1,3)*Power(t2,2) + 
                  235*Power(t1,2)*Power(t2,3) - 306*t1*Power(t2,4) - 
                  78*Power(t2,5)) + 
               t1*(55*Power(t1,5) - 138*Power(t1,4)*t2 + 
                  68*Power(t1,3)*Power(t2,2) - 
                  150*Power(t1,2)*Power(t2,3) + 76*t1*Power(t2,4) + 
                  89*Power(t2,5))) + 
            t2*(Power(s2,5)*(-4*Power(t1,2) - 20*t1*t2 + 
                  22*Power(t2,2)) + 
               Power(s2,4)*(21*Power(t1,3) + 82*Power(t1,2)*t2 - 
                  52*t1*Power(t2,2) - 47*Power(t2,3)) + 
               Power(s2,3)*(-62*Power(t1,4) - 117*Power(t1,3)*t2 + 
                  72*Power(t1,2)*Power(t2,2) + 109*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               t1*Power(t1 - t2,2)*
                (11*Power(t1,4) + 55*Power(t1,3)*t2 - 
                  24*Power(t1,2)*Power(t2,2) + 55*t1*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(s2,2)*(89*Power(t1,5) + 76*Power(t1,4)*t2 - 
                  150*Power(t1,3)*Power(t2,2) + 
                  68*Power(t1,2)*Power(t2,3) - 138*t1*Power(t2,4) + 
                  55*Power(t2,5)) - 
               s2*(55*Power(t1,6) + 54*Power(t1,5)*t2 - 
                  225*Power(t1,4)*Power(t2,2) + 
                  286*Power(t1,3)*Power(t2,3) - 
                  270*Power(t1,2)*Power(t2,4) + 90*t1*Power(t2,5) + 
                  10*Power(t2,6))) + 
            s1*(2*Power(s2,5)*(2*Power(t1,2) + 5*t1*t2 - 6*Power(t2,2)) - 
               Power(s2,4)*(19*Power(t1,3) + 91*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) - 114*Power(t2,3)) + 
               s2*Power(t1 - t2,2)*
                (49*Power(t1,4) + 329*Power(t1,3)*t2 + 
                  137*Power(t1,2)*Power(t2,2) + 329*t1*Power(t2,3) + 
                  49*Power(t2,4)) + 
               Power(s2,3)*(54*Power(t1,4) + 246*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) - 366*t1*Power(t2,3) + 
                  65*Power(t2,4)) + 
               Power(s2,2)*(-78*Power(t1,5) - 306*Power(t1,4)*t2 + 
                  235*Power(t1,3)*Power(t2,2) + 
                  146*Power(t1,2)*Power(t2,3) + 174*t1*Power(t2,4) - 
                  169*Power(t2,5)) - 
               t1*(10*Power(t1,6) + 90*Power(t1,5)*t2 - 
                  270*Power(t1,4)*Power(t2,2) + 
                  286*Power(t1,3)*Power(t2,3) - 
                  225*Power(t1,2)*Power(t2,4) + 54*t1*Power(t2,5) + 
                  55*Power(t2,6)))) + 
         Power(s,2)*(Power(s1,6)*(Power(s2,2) - Power(t1,2))*
             (7*t1 - 8*t2) + Power(s1,5)*
             (Power(s2,3)*(-10*t1 + 11*t2) + 
               Power(s2,2)*(43*Power(t1,2) - 73*t1*t2 + 
                  30*Power(t2,2)) + 
               t1*(26*Power(t1,3) + 39*Power(t1,2)*t2 - 
                  66*t1*Power(t2,2) - 4*Power(t2,3)) + 
               s2*(-59*Power(t1,3) + 23*Power(t1,2)*t2 + 
                  36*t1*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,4)*(Power(s2,3)*
                (3*Power(t1,2) + 68*t1*t2 - 71*Power(t2,2)) - 
               Power(s2,2)*(29*Power(t1,3) + 249*Power(t1,2)*t2 - 
                  289*t1*Power(t2,2) + 7*Power(t2,3)) + 
               s2*(42*Power(t1,4) + 239*Power(t1,3)*t2 - 
                  125*Power(t1,2)*Power(t2,2) - 151*t1*Power(t2,3) - 
                  13*Power(t2,4)) + 
               t1*(-16*Power(t1,4) - 57*Power(t1,3)*t2 - 
                  97*Power(t1,2)*Power(t2,2) + 160*t1*Power(t2,3) + 
                  14*Power(t2,4))) + 
            Power(s1,3)*(Power(s2,5)*(11*t1 - 10*t2) + 
               Power(s2,4)*(-71*Power(t1,2) + 68*t1*t2 + 
                  3*Power(t2,2)) + 
               3*Power(s2,3)*
                (55*Power(t1,3) - 58*Power(t1,2)*t2 - 
                  58*t1*Power(t2,2) + 55*Power(t2,3)) + 
               Power(s2,2)*(-199*Power(t1,4) + 213*Power(t1,3)*t2 + 
                  543*Power(t1,2)*Power(t2,2) - 462*t1*Power(t2,3) - 
                  82*Power(t2,4)) + 
               s2*(123*Power(t1,5) - 201*Power(t1,4)*t2 - 
                  326*Power(t1,3)*Power(t2,2) + 
                  85*Power(t1,2)*Power(t2,3) + 301*t1*Power(t2,4) + 
                  22*Power(t2,5)) - 
               t1*(29*Power(t1,5) - 106*Power(t1,4)*t2 + 
                  54*Power(t1,3)*Power(t2,2) - 
                  228*Power(t1,2)*Power(t2,3) + 225*t1*Power(t2,4) + 
                  26*Power(t2,5))) + 
            t2*(Power(s2,6)*(8*t1 - 7*t2)*t2 + 
               Power(s2,5)*(-4*Power(t1,3) - 66*Power(t1,2)*t2 + 
                  39*t1*Power(t2,2) + 26*Power(t2,3)) + 
               Power(t1,2)*Power(t1 - t2,2)*t2*
                (33*Power(t1,3) - Power(t1,2)*t2 - t1*Power(t2,2) + 
                  33*Power(t2,3)) + 
               Power(s2,4)*(14*Power(t1,4) + 160*Power(t1,3)*t2 - 
                  97*Power(t1,2)*Power(t2,2) - 57*t1*Power(t2,3) - 
                  16*Power(t2,4)) - 
               Power(s2,3)*(26*Power(t1,5) + 225*Power(t1,4)*t2 - 
                  228*Power(t1,3)*Power(t2,2) + 
                  54*Power(t1,2)*Power(t2,3) - 106*t1*Power(t2,4) + 
                  29*Power(t2,5)) + 
               s2*t1*(-10*Power(t1,6) - 132*Power(t1,5)*t2 + 
                  304*Power(t1,4)*Power(t2,2) - 
                  270*Power(t1,3)*Power(t2,3) + 
                  89*Power(t1,2)*Power(t2,4) + 72*t1*Power(t2,5) - 
                  53*Power(t2,6)) + 
               Power(s2,2)*(26*Power(t1,6) + 222*Power(t1,5)*t2 - 
                  400*Power(t1,4)*Power(t2,2) + 
                  315*Power(t1,3)*Power(t2,3) - 
                  207*Power(t1,2)*Power(t2,4) + 24*t1*Power(t2,5) + 
                  20*Power(t2,6))) + 
            Power(s1,2)*(Power(s2,6)*(-8*t1 + 7*t2) + 
               Power(s2,5)*(30*Power(t1,2) - 73*t1*t2 + 
                  43*Power(t2,2)) - 
               Power(s2,4)*(7*Power(t1,3) - 289*Power(t1,2)*t2 + 
                  249*t1*Power(t2,2) + 29*Power(t2,3)) + 
               Power(s2,3)*(-82*Power(t1,4) - 462*Power(t1,3)*t2 + 
                  543*Power(t1,2)*Power(t2,2) + 213*t1*Power(t2,3) - 
                  199*Power(t2,4)) + 
               Power(s2,2)*(121*Power(t1,5) + 313*Power(t1,4)*t2 - 
                  442*Power(t1,3)*Power(t2,2) - 
                  442*Power(t1,2)*Power(t2,3) + 313*t1*Power(t2,4) + 
                  121*Power(t2,5)) - 
               s2*(74*Power(t1,6) + 98*Power(t1,5)*t2 - 
                  302*Power(t1,4)*Power(t2,2) + 
                  38*Power(t1,3)*Power(t2,3) - 
                  266*Power(t1,2)*Power(t2,4) + 337*t1*Power(t2,5) + 
                  21*Power(t2,6)) + 
               t1*(20*Power(t1,6) + 24*Power(t1,5)*t2 - 
                  207*Power(t1,4)*Power(t2,2) + 
                  315*Power(t1,3)*Power(t2,3) - 
                  400*Power(t1,2)*Power(t2,4) + 222*t1*Power(t2,5) + 
                  26*Power(t2,6))) + 
            s1*(Power(s2,5)*(4*Power(t1,3) + 36*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) - 59*Power(t2,3)) + 
               Power(s2,4)*(-13*Power(t1,4) - 151*Power(t1,3)*t2 - 
                  125*Power(t1,2)*Power(t2,2) + 239*t1*Power(t2,3) + 
                  42*Power(t2,4)) + 
               s2*Power(t1 - t2,2)*
                (8*Power(t1,5) + 220*Power(t1,4)*t2 + 
                  111*Power(t1,3)*Power(t2,2) + 
                  111*Power(t1,2)*Power(t2,3) + 220*t1*Power(t2,4) + 
                  8*Power(t2,5)) + 
               Power(s2,3)*(22*Power(t1,5) + 301*Power(t1,4)*t2 + 
                  85*Power(t1,3)*Power(t2,2) - 
                  326*Power(t1,2)*Power(t2,3) - 201*t1*Power(t2,4) + 
                  123*Power(t2,5)) + 
               t1*t2*(-53*Power(t1,6) + 72*Power(t1,5)*t2 + 
                  89*Power(t1,4)*Power(t2,2) - 
                  270*Power(t1,3)*Power(t2,3) + 
                  304*Power(t1,2)*Power(t2,4) - 132*t1*Power(t2,5) - 
                  10*Power(t2,6)) - 
               Power(s2,2)*(21*Power(t1,6) + 337*Power(t1,5)*t2 - 
                  266*Power(t1,4)*Power(t2,2) + 
                  38*Power(t1,3)*Power(t2,3) - 
                  302*Power(t1,2)*Power(t2,4) + 98*t1*Power(t2,5) + 
                  74*Power(t2,6)))) - 
         s*(Power(s1,6)*(Power(s2,2) - Power(t1,2))*
             (-7*Power(t1,2) + s2*(t1 - t2) + t1*t2 + 8*Power(t2,2)) + 
            Power(s1,5)*(s2 - t1)*
             (3*Power(s2,3)*(t1 - t2) + 
               Power(s2,2)*(9*Power(t1,2) - 2*t1*t2 - 9*Power(t2,2)) + 
               t1*(4*Power(t1,3) + 61*Power(t1,2)*t2 - 
                  23*t1*Power(t2,2) - 46*Power(t2,3)) + 
               s2*(-36*Power(t1,3) + 16*Power(t1,2)*t2 + 
                  38*t1*Power(t2,2) - 20*Power(t2,3))) - 
            Power(s1,4)*(3*Power(s2,5)*(t1 - t2) - 
               8*Power(s2,4)*Power(t1 - t2,2) - 
               Power(s2,3)*(8*Power(t1,3) - 60*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + 47*Power(t2,3)) + 
               s2*t1*(-37*Power(t1,4) + 151*Power(t1,3)*t2 + 
                  120*Power(t1,2)*Power(t2,2) - 156*t1*Power(t2,3) - 
                  84*Power(t2,4)) + 
               Power(s2,2)*(49*Power(t1,4) - 241*Power(t1,3)*t2 + 
                  19*Power(t1,2)*Power(t2,2) + 197*t1*Power(t2,3) - 
                  18*Power(t2,4)) + 
               Power(t1,2)*(Power(t1,4) + 17*Power(t1,3)*t2 - 
                  127*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
                  104*Power(t2,4))) + 
            Power(t2,2)*(-(Power(s2,6)*
                  (8*Power(t1,2) + t1*t2 - 7*Power(t2,2))) - 
               2*Power(t1,3)*Power(t1 - t2,2)*t2*
                (11*Power(t1,2) - 14*t1*t2 + 11*Power(t2,2)) + 
               Power(s2,5)*(46*Power(t1,3) + 23*Power(t1,2)*t2 - 
                  61*t1*Power(t2,2) - 4*Power(t2,3)) - 
               Power(s2,4)*(104*Power(t1,4) + 5*Power(t1,3)*t2 - 
                  127*Power(t1,2)*Power(t2,2) + 17*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*t1*
                (-96*Power(t1,5) + 77*Power(t1,4)*t2 + 
                  125*Power(t1,3)*Power(t2,2) - 
                  234*Power(t1,2)*Power(t2,3) + 181*t1*Power(t2,4) - 
                  53*Power(t2,5)) + 
               Power(s2,3)*(132*Power(t1,5) - 79*Power(t1,4)*t2 - 
                  99*Power(t1,3)*Power(t2,2) + 
                  74*Power(t1,2)*Power(t2,3) - 40*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               s2*Power(t1,2)*
                (30*Power(t1,5) + 7*Power(t1,4)*t2 - 
                  171*Power(t1,3)*Power(t2,2) + 
                  283*Power(t1,2)*Power(t2,3) - 215*t1*Power(t2,4) + 
                  66*Power(t2,5))) + 
            Power(s1,3)*(Power(s2,6)*(-t1 + t2) + 
               Power(s2,5)*(-9*Power(t1,2) + t1*t2 + 6*Power(t2,2)) + 
               Power(s2,4)*(47*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  60*t1*Power(t2,2) + 8*Power(t2,3)) - 
               2*Power(s2,3)*(39*Power(t1,4) + 45*Power(t1,3)*t2 - 
                  178*Power(t1,2)*Power(t2,2) + 45*t1*Power(t2,3) + 
                  39*Power(t2,4)) + 
               Power(s2,2)*t1*
                (54*Power(t1,4) + 200*Power(t1,3)*t2 - 
                  585*Power(t1,2)*Power(t2,2) + 25*t1*Power(t2,3) + 
                  296*Power(t2,4)) - 
               s2*t1*(23*Power(t1,5) + 77*Power(t1,4)*t2 - 
                  211*Power(t1,3)*Power(t2,2) - 
                  154*Power(t1,2)*Power(t2,3) + 145*t1*Power(t2,4) + 
                  126*Power(t2,5)) + 
               Power(t1,2)*(10*Power(t1,5) - 40*Power(t1,4)*t2 + 
                  74*Power(t1,3)*Power(t2,2) - 
                  99*Power(t1,2)*Power(t2,3) - 79*t1*Power(t2,4) + 
                  132*Power(t2,5))) + 
            s1*t2*(Power(s2,6)*(t1 - t2)*t2 + 
               Power(s2,5)*(-26*Power(t1,3) - 61*Power(t1,2)*t2 + 
                  45*t1*Power(t2,2) + 40*Power(t2,3)) - 
               s2*t1*Power(t1 - t2,2)*
                (36*Power(t1,4) + 179*Power(t1,3)*t2 - 
                  162*Power(t1,2)*Power(t2,2) + 179*t1*Power(t2,3) + 
                  36*Power(t2,4)) + 
               Power(s2,4)*(84*Power(t1,4) + 156*Power(t1,3)*t2 - 
                  120*Power(t1,2)*Power(t2,2) - 151*t1*Power(t2,3) + 
                  37*Power(t2,4)) - 
               Power(s2,3)*(126*Power(t1,5) + 145*Power(t1,4)*t2 - 
                  154*Power(t1,3)*Power(t2,2) - 
                  211*Power(t1,2)*Power(t2,3) + 77*t1*Power(t2,4) + 
                  23*Power(t2,5)) + 
               Power(t1,2)*t2*
                (66*Power(t1,5) - 215*Power(t1,4)*t2 + 
                  283*Power(t1,3)*Power(t2,2) - 
                  171*Power(t1,2)*Power(t2,3) + 7*t1*Power(t2,4) + 
                  30*Power(t2,5)) + 
               Power(s2,2)*(104*Power(t1,6) + 90*Power(t1,5)*t2 - 
                  347*Power(t1,4)*Power(t2,2) + 
                  297*Power(t1,3)*Power(t2,3) - 
                  267*Power(t1,2)*Power(t2,4) + 117*t1*Power(t2,5) + 
                  8*Power(t2,6))) + 
            Power(s1,2)*(Power(s2,6)*
                (8*Power(t1,2) + t1*t2 - 7*Power(t2,2)) + 
               Power(s2,5)*(-20*Power(t1,3) + 47*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) - 45*Power(t2,3)) + 
               Power(s2,4)*(18*Power(t1,4) - 197*Power(t1,3)*t2 - 
                  19*Power(t1,2)*Power(t2,2) + 241*t1*Power(t2,3) - 
                  49*Power(t2,4)) + 
               Power(s2,3)*t2*
                (296*Power(t1,4) + 25*Power(t1,3)*t2 - 
                  585*Power(t1,2)*Power(t2,2) + 200*t1*Power(t2,3) + 
                  54*Power(t2,4)) + 
               Power(t1,2)*t2*
                (-53*Power(t1,5) + 181*Power(t1,4)*t2 - 
                  234*Power(t1,3)*Power(t2,2) + 
                  125*Power(t1,2)*Power(t2,3) + 77*t1*Power(t2,4) - 
                  96*Power(t2,5)) + 
               Power(s2,2)*(-14*Power(t1,6) - 211*Power(t1,5)*t2 + 
                  69*Power(t1,4)*Power(t2,2) + 
                  324*Power(t1,3)*Power(t2,3) + 
                  69*Power(t1,2)*Power(t2,4) - 211*t1*Power(t2,5) - 
                  14*Power(t2,6)) + 
               s2*t1*(8*Power(t1,6) + 117*Power(t1,5)*t2 - 
                  267*Power(t1,4)*Power(t2,2) + 
                  297*Power(t1,3)*Power(t2,3) - 
                  347*Power(t1,2)*Power(t2,4) + 90*t1*Power(t2,5) + 
                  104*Power(t2,6))))))/
     (s1*s2*(s2 - t1)*(-s + s2 - t1)*t1*(s + t1)*(s1 - t2)*(-s + s1 - t2)*
       (t1 - t2)*(s1 - s2 + t1 - t2)*t2*(s + t2)) + 
    (8*(-(Power(s,6)*(2*s1 - t2)*t2*(t1 + t2)) + 
         2*Power(s1,6)*(s2 - t1)*(Power(s2,2) - s2*t1 + t1*t2) + 
         Power(s1,4)*t2*(-3*Power(s2,4) + 
            2*Power(t1,2)*(3*t1 - 13*t2)*t2 + 
            Power(s2,2)*t2*(-3*t1 + t2) + Power(s2,3)*(6*t1 + 4*t2) + 
            s2*t1*(-3*Power(t1,2) + 8*t1*t2 - 7*Power(t2,2))) + 
         s2*Power(t2,5)*(3*Power(s2,3) + 2*Power(s2,2)*(-6*t1 + t2) - 
            2*t1*(6*Power(t1,2) + t1*t2 + Power(t2,2)) + 
            s2*(18*Power(t1,2) + 2*t1*t2 + Power(t2,2))) + 
         s1*Power(t2,4)*(-5*Power(s2,4) + 
            2*Power(t1,2)*t2*(-3*t1 + t2) + Power(s2,3)*(33*t1 + 5*t2) - 
            Power(s2,2)*t1*(48*t1 + 31*t2) + 
            5*s2*t1*(6*Power(t1,2) + 3*t1*t2 + Power(t2,2))) + 
         Power(s1,5)*(Power(s2,4) - Power(t1,2)*(t1 - 14*t2)*t2 - 
            2*Power(s2,3)*(t1 + 2*t2) - s2*t1*t2*(5*t1 + 6*t2) + 
            Power(s2,2)*(Power(t1,2) + 10*t1*t2 + 2*Power(t2,2))) + 
         Power(s1,2)*Power(t2,3)*
          (3*Power(t1,2)*(5*t1 - 2*t2)*t2 - 
            Power(s2,3)*(31*t1 + 14*t2) + 
            Power(s2,2)*(51*Power(t1,2) + 58*t1*t2 + 2*Power(t2,2)) - 
            s2*t1*(30*Power(t1,2) + 18*t1*t2 + 19*Power(t2,2))) + 
         Power(s1,3)*Power(t2,2)*
          (4*Power(s2,4) + Power(s2,3)*(6*t1 + 5*t2) + 
            2*Power(t1,2)*t2*(-7*t1 + 9*t2) - 
            Power(s2,2)*(21*Power(t1,2) + 32*t1*t2 + 6*Power(t2,2)) + 
            s2*t1*(15*Power(t1,2) + 2*t1*t2 + 27*Power(t2,2))) + 
         Power(s,5)*(Power(s1,2)*
             (-4*s2*t1 - 2*Power(t1,2) - 6*s2*t2 + 15*t1*t2 + 
               Power(t2,2)) + 
            t2*(2*t2*(Power(t1,2) + 7*t1*t2 + Power(t2,2)) - 
               3*s2*(Power(t1,2) + 2*Power(t2,2))) + 
            s1*(-(Power(t2,2)*(32*t1 + 3*t2)) + 
               s2*(3*Power(t1,2) + 4*t1*t2 + 14*Power(t2,2)))) - 
         s*(2*Power(s1,6)*s2*(s2 - t1) + 
            Power(s1,5)*(3*Power(s2,3) - 12*Power(t1,2)*t2 - 
               Power(s2,2)*(7*t1 + 4*t2) + 
               2*s2*(2*Power(t1,2) + Power(t2,2))) + 
            s1*Power(t2,3)*(8*Power(s2,4) - 
               6*Power(s2,3)*(8*t1 + 5*t2) + 
               5*t1*t2*(6*Power(t1,2) + 3*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(88*Power(t1,2) + 149*t1*t2 + 
                  13*Power(t2,2)) + 
               s2*(-72*Power(t1,3) - 91*Power(t1,2)*t2 - 
                  76*t1*Power(t2,2) + Power(t2,3))) + 
            Power(s1,2)*Power(t2,2)*
             (4*Power(s2,4) + Power(s2,3)*(37*t1 + 33*t2) - 
               t1*t2*(46*Power(t1,2) + 6*t1*t2 + 19*Power(t2,2)) - 
               Power(s2,2)*(82*Power(t1,2) + 182*t1*t2 + 
                  41*Power(t2,2)) + 
               s2*(63*Power(t1,3) + 77*Power(t1,2)*t2 + 
                  149*t1*Power(t2,2) + Power(t2,3))) + 
            Power(t2,4)*(-6*Power(s2,4) + Power(s2,3)*(17*t1 + 6*t2) - 
               2*t1*t2*(3*Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(-32*Power(t1,2) - 39*t1*t2 + 
                  2*Power(t2,2)) + 
               s2*(30*Power(t1,3) + 33*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,4)*(2*Power(s2,4) - 6*Power(s2,3)*(t1 + 2*t2) + 
               t1*t2*(-7*Power(t1,2) + 43*t1*t2 - 11*Power(t2,2)) + 
               3*Power(s2,2)*(Power(t1,2) + 3*t1*t2 - 2*Power(t2,2)) + 
               s2*(Power(t1,3) - 14*Power(t1,2)*t2 + 
                  38*t1*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s1,3)*t2*(-8*Power(s2,4) + 
               t1*t2*(30*Power(t1,2) - 38*t1*t2 + 27*Power(t2,2)) + 
               Power(s2,2)*(20*Power(t1,2) + 70*t1*t2 + 
                  34*Power(t2,2)) - 
               s2*(20*Power(t1,3) + 13*Power(t1,2)*t2 + 
                  122*t1*Power(t2,2) + 9*Power(t2,3)))) + 
         Power(s,4)*(Power(s1,3)*
             (4*Power(s2,2) + 4*s2*t1 + 8*Power(t1,2) + 11*s2*t2 - 
               20*t1*t2 - Power(t2,2)) + 
            Power(s1,2)*(2*Power(t1,3) - 2*Power(t1,2)*t2 + 
               78*t1*Power(t2,2) + 3*Power(t2,3) + 
               8*Power(s2,2)*(t1 + t2) - 
               s2*(9*Power(t1,2) + 49*t1*t2 + 32*Power(t2,2))) + 
            t2*(Power(s2,2)*
                (3*Power(t1,2) - 5*t1*t2 + 12*Power(t2,2)) + 
               t2*(3*Power(t1,3) + 17*Power(t1,2)*t2 + 
                  32*t1*Power(t2,2) + 2*Power(t2,3)) - 
               s2*(3*Power(t1,3) + 11*Power(t1,2)*t2 + 
                  32*t1*Power(t2,2) + 10*Power(t2,3))) - 
            s1*(Power(s2,2)*(3*Power(t1,2) + 2*t1*t2 + 25*Power(t2,2)) + 
               t2*(4*Power(t1,3) + 24*Power(t1,2)*t2 + 
                  89*t1*Power(t2,2) + Power(t2,3)) - 
               s2*(3*Power(t1,3) + 18*Power(t1,2)*t2 + 
                  79*t1*Power(t2,2) + 33*Power(t2,3)))) + 
         Power(s,2)*(2*Power(s1,5)*(Power(s2,2) + t1*(2*t1 - t2)) + 
            Power(s1,4)*(8*Power(s2,3) - 2*Power(s2,2)*(5*t1 + t2) + 
               s2*(9*Power(t1,2) - 29*t1*t2 - 12*Power(t2,2)) + 
               2*(Power(t1,3) - 15*Power(t1,2)*t2 + 11*t1*Power(t2,2) + 
                  Power(t2,3))) + 
            Power(t2,3)*(3*Power(s2,4) - Power(s2,3)*(t1 + 18*t2) + 
               Power(s2,2)*(19*Power(t1,2) + 61*t1*t2 + 
                  5*Power(t2,2)) + 
               t2*(15*Power(t1,3) + 21*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) + Power(t2,3)) - 
               s2*(30*Power(t1,3) + 59*Power(t1,2)*t2 + 
                  55*t1*Power(t2,2) + 2*Power(t2,3))) + 
            s1*Power(t2,2)*(-3*Power(s2,4) + 
               Power(s2,3)*(15*t1 + 41*t2) - 
               3*Power(s2,2)*
                (19*Power(t1,2) + 59*t1*t2 + 17*Power(t2,2)) + 
               t2*(-44*Power(t1,3) - 62*Power(t1,2)*t2 - 
                  43*t1*Power(t2,2) + Power(t2,3)) + 
               s2*(63*Power(t1,3) + 129*Power(t1,2)*t2 + 
                  218*t1*Power(t2,2) + 7*Power(t2,3))) - 
            Power(s1,2)*t2*(3*Power(s2,4) + 14*Power(s2,3)*(t1 + t2) - 
               Power(s2,2)*(49*Power(t1,2) + 151*t1*t2 + 
                  85*Power(t2,2)) + 
               t2*(-45*Power(t1,3) - 40*Power(t1,2)*t2 - 
                  86*t1*Power(t2,2) + Power(t2,3)) + 
               s2*(46*Power(t1,3) + 78*Power(t1,2)*t2 + 
                  297*t1*Power(t2,2) + 28*Power(t2,3))) + 
            Power(s1,3)*(3*Power(s2,4) - 17*Power(s2,3)*t2 - 
               8*Power(s2,2)*(Power(t1,2) + 3*t1*t2 + 5*Power(t2,2)) - 
               t2*(19*Power(t1,3) - 30*Power(t1,2)*t2 + 
                  70*t1*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(9*Power(t1,3) - Power(t1,2)*t2 + 165*t1*Power(t2,2) + 
                  33*Power(t2,3)))) + 
         Power(s,3)*(-(Power(s1,4)*
               (6*Power(s2,2) + 2*s2*t1 + 10*Power(t1,2) + 7*s2*t2 - 
                 9*t1*t2 - 2*Power(t2,2))) - 
            Power(s1,3)*(7*Power(s2,3) + 4*Power(t1,3) - 
               22*Power(t1,2)*t2 + 65*t1*Power(t2,2) + 4*Power(t2,3) + 
               Power(s2,2)*(t1 + 2*t2) + 
               s2*(Power(t1,2) - 64*t1*t2 - 37*Power(t2,2))) + 
            Power(t2,2)*(2*Power(s2,3)*(2*t1 - 5*t2) + 
               Power(s2,2)*(8*Power(t1,2) + 19*t1*t2 + 20*Power(t2,2)) + 
               2*t2*(6*Power(t1,3) + 16*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) + Power(t2,3)) - 
               s2*(15*Power(t1,3) + 36*Power(t1,2)*t2 + 
                  76*t1*Power(t2,2) + 4*Power(t2,3))) + 
            s1*t2*(16*Power(s2,3)*t2 - 
               Power(s2,2)*(20*Power(t1,2) + 61*t1*t2 + 
                  63*Power(t2,2)) + 
               t2*(-24*Power(t1,3) - 69*Power(t1,2)*t2 - 
                  97*t1*Power(t2,2) + Power(t2,3)) + 
               s2*(24*Power(t1,3) + 68*Power(t1,2)*t2 + 
                  222*t1*Power(t2,2) + 27*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,3)*(-4*t1 + t2) + 
               Power(s2,2)*(13*Power(t1,2) + 45*t1*t2 + 49*Power(t2,2)) + 
               t2*(17*Power(t1,3) + 25*Power(t1,2)*t2 + 
                  132*t1*Power(t2,2) + Power(t2,3)) - 
               s2*(11*Power(t1,3) + 35*Power(t1,2)*t2 + 
                  204*t1*Power(t2,2) + 55*Power(t2,3)))))*Log(-s1))/
     (s1*s2*(-s + s2 - t1)*t1*Power(s1 - t2,2)*(-s + s1 - t2)*t2*
       Power(s + t2,2)) + (8*(Power(s1,4)*Power(s2 - t1,2)*
          (Power(s2,3) - Power(s2,2)*t1 + s2*Power(t1,2) + 3*Power(t1,3)) \
- Power(s,6)*(2*s2 - t1)*t1*(t1 + t2) - 
         s2*(s2 - t1)*t1*Power(t2,2)*
          (2*Power(s2,4) + Power(s2,2)*t1*(14*t1 - 5*t2) + 
            2*Power(t1,3)*(t1 - 3*t2) + Power(s2,3)*(-12*t1 + t2) + 
            s2*Power(t1,2)*(-4*t1 + 9*t2)) + 
         Power(s1,3)*(s2 - t1)*
          (2*Power(s2,5) - 2*Power(t1,4)*(t1 - 6*t2) - 
            2*Power(s2,4)*(t1 + t2) + 2*Power(s2,3)*t1*(t1 + 2*t2) - 
            7*s2*Power(t1,3)*(t1 + 3*t2) + 
            Power(s2,2)*Power(t1,2)*(7*t1 + 10*t2)) + 
         s1*t2*(2*Power(s2,6)*(t1 + t2) - Power(s2,5)*t1*(6*t1 + 5*t2) + 
            Power(s2,4)*t1*(-7*Power(t1,2) + 8*t1*t2 - 3*Power(t2,2)) - 
            2*Power(t1,5)*(Power(t1,2) + t1*t2 + 6*Power(t2,2)) + 
            5*s2*Power(t1,4)*(Power(t1,2) + 3*t1*t2 + 6*Power(t2,2)) + 
            Power(s2,3)*Power(t1,2)*
             (27*Power(t1,2) + 2*t1*t2 + 15*Power(t2,2)) - 
            Power(s2,2)*Power(t1,3)*
             (19*Power(t1,2) + 18*t1*t2 + 30*Power(t2,2))) + 
         Power(s1,2)*(Power(s2,4)*Power(t1,2)*(t1 - 3*t2) - 
            4*Power(s2,6)*t2 - s2*Power(t1,4)*t2*(31*t1 + 48*t2) + 
            Power(s2,5)*(2*Power(t1,2) + 10*t1*t2 + Power(t2,2)) + 
            Power(t1,5)*(Power(t1,2) + 2*t1*t2 + 18*Power(t2,2)) - 
            Power(s2,3)*Power(t1,2)*
             (6*Power(t1,2) + 32*t1*t2 + 21*Power(t2,2)) + 
            Power(s2,2)*Power(t1,3)*
             (2*Power(t1,2) + 58*t1*t2 + 51*Power(t2,2))) + 
         Power(s,5)*(-(s2*Power(t1,2)*(3*t1 + 32*t2)) + 
            Power(s2,2)*(Power(t1,2) + 15*t1*t2 - 2*Power(t2,2)) + 
            2*Power(t1,2)*(Power(t1,2) + 7*t1*t2 + Power(t2,2)) + 
            s1*(-2*Power(s2,2)*(3*t1 + 2*t2) - 
               3*t1*(2*Power(t1,2) + Power(t2,2)) + 
               s2*(14*Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)))) + 
         Power(s,4)*(-(Power(s2,3)*
               (Power(t1,2) + 20*t1*t2 - 8*Power(t2,2))) + 
            Power(s2,2)*(3*Power(t1,3) + 78*Power(t1,2)*t2 - 
               2*t1*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,2)*(2*Power(t1,3) + 32*Power(t1,2)*t2 + 
               17*t1*Power(t2,2) + 3*Power(t2,3)) - 
            s2*t1*(Power(t1,3) + 89*Power(t1,2)*t2 + 
               24*t1*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s1,2)*(4*Power(s2,3) + 8*Power(s2,2)*(t1 + t2) + 
               t1*(12*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) - 
               s2*(25*Power(t1,2) + 2*t1*t2 + 3*Power(t2,2))) + 
            s1*(Power(s2,3)*(11*t1 + 4*t2) - 
               Power(s2,2)*(32*Power(t1,2) + 49*t1*t2 + 9*Power(t2,2)) - 
               t1*(10*Power(t1,3) + 32*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(33*Power(t1,3) + 79*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) + 3*Power(t2,3)))) + 
         Power(s,3)*(-(Power(s1,3)*(s2 - t1)*
               (7*Power(s2,2) + 6*s2*t1 - 10*Power(t1,2) + 4*s2*t2 + 
                 4*t1*t2)) + Power(s2,4)*
             (2*Power(t1,2) + 9*t1*t2 - 10*Power(t2,2)) + 
            s2*Power(t1,2)*(Power(t1,3) - 97*Power(t1,2)*t2 - 
               69*t1*Power(t2,2) - 24*Power(t2,3)) - 
            Power(s2,3)*(4*Power(t1,3) + 65*Power(t1,2)*t2 - 
               22*t1*Power(t2,2) + 4*Power(t2,3)) + 
            2*Power(t1,3)*(Power(t1,3) + 14*Power(t1,2)*t2 + 
               16*t1*Power(t2,2) + 6*Power(t2,3)) + 
            Power(s2,2)*t1*(Power(t1,3) + 132*Power(t1,2)*t2 + 
               25*t1*Power(t2,2) + 17*Power(t2,3)) + 
            Power(s1,2)*(-6*Power(s2,4) - Power(s2,3)*(2*t1 + t2) + 
               Power(t1,2)*(20*Power(t1,2) + 19*t1*t2 + 
                  8*Power(t2,2)) + 
               Power(s2,2)*(49*Power(t1,2) + 45*t1*t2 + 
                  13*Power(t2,2)) - 
               s2*t1*(63*Power(t1,2) + 61*t1*t2 + 20*Power(t2,2))) - 
            s1*(Power(s2,4)*(7*t1 + 2*t2) + 
               Power(s2,3)*(-37*Power(t1,2) - 64*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(55*Power(t1,3) + 204*Power(t1,2)*t2 + 
                  35*t1*Power(t2,2) + 11*Power(t2,3)) + 
               Power(t1,2)*(4*Power(t1,3) + 76*Power(t1,2)*t2 + 
                  36*t1*Power(t2,2) + 15*Power(t2,3)) - 
               s2*t1*(27*Power(t1,3) + 222*Power(t1,2)*t2 + 
                  68*t1*Power(t2,2) + 24*Power(t2,3)))) - 
         s*(2*Power(s1,4)*Power(s2 - t1,2)*
             (Power(s2,2) - 2*s2*t1 - 3*Power(t1,2)) + 
            Power(s1,3)*(3*Power(s2,5) - 6*Power(s2,4)*(2*t1 + t2) - 
               6*s2*Power(t1,3)*(5*t1 + 8*t2) + 
               Power(t1,4)*(6*t1 + 17*t2) + 
               Power(s2,2)*Power(t1,2)*(33*t1 + 37*t2)) - 
            t1*t2*(12*Power(s2,5)*t2 + 
               Power(s2,3)*t1*
                (-27*Power(t1,2) + 38*t1*t2 - 30*Power(t2,2)) + 
               2*Power(t1,4)*(Power(t1,2) + 2*t1*t2 + 3*Power(t2,2)) - 
               5*s2*Power(t1,3)*
                (Power(t1,2) + 3*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,4)*(11*Power(t1,2) - 43*t1*t2 + 
                  7*Power(t2,2)) + 
               Power(s2,2)*Power(t1,2)*
                (19*Power(t1,2) + 6*t1*t2 + 46*Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,6) - Power(s2,5)*(4*t1 + 7*t2) + 
               Power(t1,4)*(2*Power(t1,2) - 39*t1*t2 - 
                  32*Power(t2,2)) + 
               Power(s2,4)*(-6*Power(t1,2) + 9*t1*t2 + 3*Power(t2,2)) + 
               2*Power(s2,3)*t1*
                (17*Power(t1,2) + 35*t1*t2 + 10*Power(t2,2)) - 
               Power(s2,2)*Power(t1,2)*
                (41*Power(t1,2) + 182*t1*t2 + 82*Power(t2,2)) + 
               s2*Power(t1,3)*
                (13*Power(t1,2) + 149*t1*t2 + 88*Power(t2,2))) + 
            s1*(-2*Power(s2,6)*t2 + 
               2*Power(s2,5)*(Power(t1,2) + 2*Power(t2,2)) + 
               s2*Power(t1,3)*
                (Power(t1,3) - 76*Power(t1,2)*t2 - 91*t1*Power(t2,2) - 
                  72*Power(t2,3)) + 
               Power(s2,4)*(3*Power(t1,3) + 38*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + Power(t2,3)) - 
               Power(s2,3)*t1*
                (9*Power(t1,3) + 122*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 20*Power(t2,3)) + 
               Power(t1,4)*(2*Power(t1,3) + 13*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*
                (Power(t1,3) + 149*Power(t1,2)*t2 + 77*t1*Power(t2,2) + 
                  63*Power(t2,3)))) + 
         Power(s,2)*(3*Power(s1,4)*Power(s2 - t1,2)*(s2 + t1) - 
            2*Power(s2,5)*(t1 - 2*t2)*t2 + 
            s2*Power(t1,3)*(Power(t1,3) - 43*Power(t1,2)*t2 - 
               62*t1*Power(t2,2) - 44*Power(t2,3)) + 
            2*Power(s2,4)*(Power(t1,3) + 11*Power(t1,2)*t2 - 
               15*t1*Power(t2,2) + Power(t2,3)) + 
            Power(t1,4)*(Power(t1,3) + 11*Power(t1,2)*t2 + 
               21*t1*Power(t2,2) + 15*Power(t2,3)) - 
            Power(s2,3)*t1*(3*Power(t1,3) + 70*Power(t1,2)*t2 - 
               30*t1*Power(t2,2) + 19*Power(t2,3)) + 
            Power(s2,2)*Power(t1,2)*
             (-Power(t1,3) + 86*Power(t1,2)*t2 + 40*t1*Power(t2,2) + 
               45*Power(t2,3)) + 
            Power(s1,3)*(s2 - t1)*
             (8*Power(s2,3) - 9*Power(s2,2)*t1 + 
               Power(t1,2)*(18*t1 + t2) - s2*t1*(23*t1 + 14*t2)) + 
            Power(s1,2)*(2*Power(s2,5) - 2*Power(s2,4)*(t1 + 5*t2) - 
               8*Power(s2,3)*(5*Power(t1,2) + 3*t1*t2 + Power(t2,2)) - 
               3*s2*Power(t1,2)*
                (17*Power(t1,2) + 59*t1*t2 + 19*Power(t2,2)) + 
               Power(t1,3)*(5*Power(t1,2) + 61*t1*t2 + 19*Power(t2,2)) + 
               Power(s2,2)*t1*
                (85*Power(t1,2) + 151*t1*t2 + 49*Power(t2,2))) - 
            s1*(Power(s2,4)*(12*Power(t1,2) + 29*t1*t2 - 9*Power(t2,2)) + 
               Power(s2,3)*(-33*Power(t1,3) - 165*Power(t1,2)*t2 + 
                  t1*Power(t2,2) - 9*Power(t2,3)) + 
               Power(t1,3)*(2*Power(t1,3) + 55*Power(t1,2)*t2 + 
                  59*t1*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s2,2)*t1*
                (28*Power(t1,3) + 297*Power(t1,2)*t2 + 
                  78*t1*Power(t2,2) + 46*Power(t2,3)) - 
               s2*Power(t1,2)*
                (7*Power(t1,3) + 218*Power(t1,2)*t2 + 
                  129*t1*Power(t2,2) + 63*Power(t2,3)))))*Log(-s2))/
     (s1*s2*Power(s2 - t1,2)*(-s + s2 - t1)*t1*Power(s + t1,2)*
       (-s + s1 - t2)*t2) - (8*
       (-(Power(s,4)*(3*Power(s2,2) - 4*s2*t1 + 2*Power(t1,2))*
            Power(t1 - t2,2)*(t1 + t2)) - 
         Power(s1,4)*Power(s2 - t1,2)*(s2 + t1)*
          (2*Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
         s2*(s2 - t1)*Power(t2,2)*
          (Power(s2,4)*(2*t1 - t2) + 
            Power(s2,2)*t2*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
            Power(s2,3)*(-3*Power(t1,2) + 2*Power(t2,2)) + 
            s2*t1*(Power(t1,3) + 2*t1*Power(t2,2) - 2*Power(t2,3)) + 
            2*Power(t1,2)*(-Power(t1,3) + 2*Power(t1,2)*t2 - 
               2*t1*Power(t2,2) + Power(t2,3))) + 
         Power(s1,3)*(s2 - t1)*
          (3*Power(s2,4)*(t1 - t2) + 
            Power(s2,3)*(-13*Power(t1,2) + 12*t1*t2 + Power(t2,2)) - 
            3*Power(t1,2)*t2*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*(8*Power(t1,3) + 3*Power(t1,2)*t2 - 
               18*t1*Power(t2,2) + 6*Power(t2,3)) + 
            s2*t1*(2*Power(t1,3) - 3*Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
               6*Power(t2,3))) + 
         s1*t2*(Power(s2,6)*(2*t1 - t2) + 
            Power(s2,4)*t1*(-14*Power(t1,2) + 13*t1*t2 - 
               3*Power(t2,2)) + 
            Power(s2,5)*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*t1*(-16*Power(t1,4) + 22*Power(t1,3)*t2 - 
               24*Power(t1,2)*Power(t2,2) + 26*t1*Power(t2,3) - 
               11*Power(t2,4)) - 
            2*Power(t1,3)*(Power(t1,4) - 3*Power(t1,3)*t2 + 
               4*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,3)*(22*Power(t1,4) - 18*Power(t1,3)*t2 + 
               9*Power(t1,2)*Power(t2,2) - 11*t1*Power(t2,3) + 
               5*Power(t2,4)) + 
            s2*Power(t1,2)*(7*Power(t1,4) - 16*Power(t1,3)*t2 + 
               20*Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) + 
               10*Power(t2,4))) + 
         Power(s1,2)*(Power(s2,5)*
             (-6*Power(t1,2) + t1*t2 + 5*Power(t2,2)) + 
            Power(s2,4)*(14*Power(t1,3) + 15*Power(t1,2)*t2 - 
               33*t1*Power(t2,2) + 4*Power(t2,3)) + 
            s2*Power(t1,3)*(-8*Power(t1,3) + 20*Power(t1,2)*t2 - 
               20*t1*Power(t2,2) + 9*Power(t2,3)) + 
            Power(s2,3)*(-14*Power(t1,4) - 26*Power(t1,3)*t2 + 
               35*Power(t1,2)*Power(t2,2) + 18*t1*Power(t2,3) - 
               10*Power(t2,4)) + 
            Power(t1,3)*(2*Power(t1,4) - 8*Power(t1,3)*t2 + 
               8*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 
               2*Power(t2,4)) + 
            Power(s2,2)*t1*(12*Power(t1,4) - 2*Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - 34*t1*Power(t2,3) + 
               13*Power(t2,4))) + 
         Power(s,3)*(t1 - t2)*
          (Power(s2,3)*(7*Power(t1,2) + 6*t1*t2 - 10*Power(t2,2)) - 
            4*Power(t1,2)*t2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*t1*(-13*Power(t1,2) - 18*t1*t2 + 
               25*Power(t2,2)) + 
            s2*t1*(8*Power(t1,3) + 17*Power(t1,2)*t2 - 
               26*t1*Power(t2,2) + 4*Power(t2,3)) + 
            s1*(Power(s2,2)*t2*(-14*t1 + t2) + 4*Power(s2,3)*(t1 + t2) + 
               s2*(-10*Power(t1,3) + 10*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - 3*Power(t2,3)) + 
               t1*(8*Power(t1,3) - 2*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
                  3*Power(t2,3)))) + 
         Power(s,2)*(2*Power(t1,2)*Power(t1 - t2,2)*
             (Power(t1,3) - 2*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
               Power(t2,3)) - 
            Power(s2,4)*(4*Power(t1,3) + 8*Power(t1,2)*t2 - 
               25*t1*Power(t2,2) + 12*Power(t2,3)) + 
            2*s2*t1*t2*(17*Power(t1,4) - 37*Power(t1,3)*t2 + 
               25*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) - 
               Power(t2,4)) + 
            Power(s2,3)*(10*Power(t1,4) + 35*Power(t1,3)*t2 - 
               86*Power(t1,2)*Power(t2,2) + 37*t1*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,2)*(-8*Power(t1,5) - 49*Power(t1,4)*t2 + 
               112*Power(t1,3)*Power(t2,2) - 
               58*Power(t1,2)*Power(t2,3) + 3*t1*Power(t2,4) + 
               Power(t2,5)) + 
            Power(s1,2)*(3*Power(s2,4)*(t1 - t2) + 
               Power(s2,3)*(-11*Power(t1,2) + 4*t1*t2 + 
                  8*Power(t2,2)) + 
               Power(s2,2)*(5*Power(t1,3) + 20*Power(t1,2)*t2 - 
                  36*t1*Power(t2,2) + 8*Power(t2,3)) + 
               s2*(14*Power(t1,4) - 32*Power(t1,3)*t2 + 
                  20*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               t1*(-12*Power(t1,4) + 14*Power(t1,3)*t2 + 
                  5*Power(t1,2)*Power(t2,2) - 11*t1*Power(t2,3) + 
                  3*Power(t2,4))) + 
            s1*(Power(s2,4)*(-8*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(10*Power(t1,3) + 20*Power(t1,2)*t2 - 
                  25*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*(4*Power(t1,4) - 27*Power(t1,3)*t2 - 
                  10*Power(t1,2)*Power(t2,2) + 45*t1*Power(t2,3) - 
                  13*Power(t2,4)) + 
               t1*t2*(20*Power(t1,4) - 43*Power(t1,3)*t2 + 
                  29*Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               s2*(-8*Power(t1,5) - 9*Power(t1,4)*t2 + 
                  65*Power(t1,3)*Power(t2,2) - 
                  65*Power(t1,2)*Power(t2,3) + 14*t1*Power(t2,4) + 
                  3*Power(t2,5)))) - 
         s*(Power(s1,3)*(s2 - t1)*
             (8*Power(t1,4) + 3*Power(s2,3)*(t1 - t2) - 
               10*Power(t1,3)*t2 + 4*t1*Power(t2,3) + 
               Power(s2,2)*(-6*Power(t1,2) + 5*t1*t2 + Power(t2,2)) - 
               2*s2*(Power(t1,3) - 5*Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
                  2*Power(t2,3))) + 
            t2*(-2*Power(t1,3)*Power(t1 - t2,2)*
                (Power(t1,2) + Power(t2,2)) - 
               2*Power(s2,5)*(3*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,4)*(27*Power(t1,3) - 52*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*t1*
                (31*Power(t1,4) - 57*Power(t1,3)*t2 + 
                  28*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,3)*(-43*Power(t1,4) + 76*Power(t1,3)*t2 - 
                  26*Power(t1,2)*Power(t2,2) - 7*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*Power(t1,2)*
                (-7*Power(t1,4) + 13*Power(t1,3)*t2 - 
                  4*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,2)*(3*Power(s2,5)*(t1 - t2) + 
               Power(s2,4)*(-22*Power(t1,2) + 10*t1*t2 + 
                  12*Power(t2,2)) + 
               2*Power(t1,2)*t2*
                (9*Power(t1,3) - 18*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) - Power(t2,3)) + 
               2*Power(s2,3)*
                (17*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  32*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*(-13*Power(t1,4) - 36*Power(t1,3)*t2 + 
                  42*Power(t1,2)*Power(t2,2) + 24*t1*Power(t2,3) - 
                  13*Power(t2,4)) + 
               s2*t1*(-2*Power(t1,4) - 7*Power(t1,3)*t2 + 
                  45*Power(t1,2)*Power(t2,2) - 52*t1*Power(t2,3) + 
                  14*Power(t2,4))) + 
            s1*(-4*Power(s2,5)*t1*(t1 - t2) + 
               Power(s2,4)*(8*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) - 2*Power(t2,3)) - 
               2*Power(s2,3)*(3*Power(t1,4) + 10*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) - 19*t1*Power(t2,3) + 
                  5*Power(t2,4)) + 
               2*s2*t1*(-4*Power(t1,5) + 19*Power(t1,4)*t2 - 
                  33*Power(t1,3)*Power(t2,2) + 
                  20*Power(t1,2)*Power(t2,3) + 4*t1*Power(t2,4) - 
                  6*Power(t2,5)) + 
               Power(t1,2)*(4*Power(t1,5) - 16*Power(t1,4)*t2 + 
                  20*Power(t1,3)*Power(t2,2) - 
                  7*Power(t1,2)*Power(t2,3) - 7*t1*Power(t2,4) + 
                  6*Power(t2,5)) + 
               Power(s2,2)*(6*Power(t1,5) - 15*Power(t1,4)*t2 + 
                  65*Power(t1,3)*Power(t2,2) - 
                  67*Power(t1,2)*Power(t2,3) + 5*t1*Power(t2,4) + 
                  8*Power(t2,5)))))*Log(-t1))/
     (s1*s2*Power(s2 - t1,2)*(-s + s2 - t1)*t1*(-s + s1 - t2)*
       Power(t1 - t2,2)*t2) + (8*
       (Power(s1,4)*(s2 + t1) + Power(s1,3)*(s2 - t1)*(2*s2 + t2) + 
         s2*t1*Power(t2,2)*(2*s2 - t1 + t2) - 
         s2*t1*t2*(Power(s2,2) + s2*t2 - 2*t1*t2) + 
         s1*t1*t2*(-3*s2*t1 + Power(t1,2) - t1*t2 + 2*Power(t2,2)) + 
         2*Power(s,2)*(s2*Power(t1,2) + s1*Power(t2,2)) - 
         Power(s,3)*(s1*(t1 - t2) + 2*t1*(t1 + t2)) + 
         Power(s1,2)*(2*Power(s2,3) - Power(t1,2)*t2 + 
            Power(s2,2)*(-t1 + t2) + s2*Power(t1 + t2,2)) + 
         Power(s1,3)*(2*Power(s2,2) - s2*(t1 + 2*t2) + t1*(t1 + 3*t2)) - 
         Power(s1,2)*t2*(2*Power(s2,2) - s2*t2 + t1*(t1 + 6*t2)) + 
         s1*(s2*t1*(t1 - 3*t2)*t2 + 2*Power(t1,2)*Power(t2,2) + 
            Power(s2,3)*(t1 + 2*t2) + 
            Power(s2,2)*(Power(t1,2) + 3*Power(t2,2))) + 
         Power(s,2)*(Power(s1,2)*(s2 + 3*t1 - 2*t2) + 
            s1*(5*Power(t1,2) + 4*t1*t2 + t2*(-s2 + t2)) + 
            t1*(2*s2*(t1 + t2) - t2*(5*t1 + t2))) + 
         s*(Power(s1,2)*(-2*Power(s2,2) + 2*s2*t1 + (t1 - t2)*t2) + 
            s2*t1*(s2*(-t1 + t2) + t2*(3*t1 + t2)) + 
            s1*(-2*Power(s2,2)*t2 + t1*t2*(t1 + 3*t2) + 
               s2*(-3*Power(t1,2) + 2*t1*t2 - 5*Power(t2,2)))) - 
         s*(Power(s1,3)*(2*s2 + 3*t1 - t2) + 
            t1*t2*(Power(t1,2) + Power(t2,2) + s2*(-4*t1 + t2)) + 
            Power(s1,2)*(2*Power(s2,2) + 4*Power(t1,2) + 5*t1*t2 + 
               Power(t2,2) - s2*(t1 + 3*t2)) + 
            s1*(-(t1*t2*(6*t1 + 5*t2)) + 
               s2*(2*Power(t1,2) + 2*t1*t2 + Power(t2,2)))) - 
         (Power(s1,4)*Power(s2 - t1,2)*(s2 + t1) + 
            Power(s,5)*(s1*(-t1 + t2) + 2*t1*(t1 + t2)) + 
            s2*t1*Power(t2,2)*
             (-2*Power(s2,3) + Power(t1,2)*(t1 - t2) + 
               Power(s2,2)*(6*t1 - t2) + s2*t1*(-3*t1 + 2*t2)) + 
            Power(s1,3)*(2*Power(s2,4) - Power(s2,2)*t1*(t1 - 2*t2) - 
               2*Power(s2,3)*t2 - Power(t1,3)*(t1 + 7*t2) + 
               s2*Power(t1,2)*(4*t1 + 7*t2)) + 
            Power(s,4)*(Power(s1,2)*(s2 + 3*t1 - 2*t2) + 
               s1*(3*s2*t1 - 7*Power(t1,2) - 4*s2*t2 - 3*t1*t2) + 
               t1*(-4*s2*t1 + 4*Power(t1,2) - 6*s2*t2 + 9*t1*t2 + 
                  Power(t2,2))) + 
            Power(s1,2)*(-4*Power(s2,4)*t2 - 
               2*s2*Power(t1,2)*t2*(3*t1 + 4*t2) + 
               Power(t1,3)*t2*(t1 + 8*t2) + 
               Power(s2,3)*(2*Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*t1*(2*Power(t1,2) + 3*t1*t2 + 2*Power(t2,2))\
) - s1*t2*(-2*Power(s2,4)*(t1 + t2) + Power(s2,3)*t1*(6*t1 + t2) - 
               2*s2*Power(t1,2)*(Power(t1,2) + t1*t2 + Power(t2,2)) + 
               Power(t1,3)*(Power(t1,2) - t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*t1*(Power(t1,2) + t1*t2 + 3*Power(t2,2))) + 
            Power(s,3)*(Power(s1,3)*(-2*s2 - 3*t1 + t2) + 
               Power(s1,2)*(-3*Power(s2,2) - 7*s2*t1 + 
                  10*Power(t1,2) + 9*s2*t2 + 3*t1*t2) - 
               s1*(11*Power(t1,3) + Power(s2,2)*(4*t1 - 5*t2) + 
                  18*Power(t1,2)*t2 + t1*Power(t2,2) + Power(t2,3) - 
                  2*s2*(6*Power(t1,2) + 3*t1*t2 - Power(t2,2))) + 
               t1*(2*Power(t1,3) + 13*Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) + Power(t2,3) + 
                  2*Power(s2,2)*(t1 + 3*t2) - 
                  s2*(6*Power(t1,2) + 21*t1*t2 + 2*Power(t2,2)))) + 
            s*(-2*Power(s1,4)*(Power(s2,2) - Power(t1,2)) + 
               Power(s1,3)*(-3*Power(s2,3) + 6*Power(s2,2)*t2 + 
                  2*s2*t1*(5*t1 + t2) - 5*Power(t1,2)*(t1 + 2*t2)) + 
               t1*t2*(4*Power(s2,3)*t2 + 
                  s2*t1*(-7*Power(t1,2) + 2*t1*t2 - 4*Power(t2,2)) + 
                  Power(t1,2)*(Power(t1,2) + Power(t2,2)) + 
                  Power(s2,2)*
                   (8*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2))) + 
               Power(s1,2)*(-2*Power(s2,4) + 7*Power(s2,3)*t2 + 
                  Power(s2,2)*
                   (5*Power(t1,2) + t1*t2 - 3*Power(t2,2)) - 
                  3*s2*t1*(3*Power(t1,2) + 7*t1*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*
                   (4*Power(t1,2) + 15*t1*t2 + 10*Power(t2,2))) + 
               s1*(2*Power(s2,4)*t2 - 
                  2*Power(s2,3)*
                   (Power(t1,2) + 2*t1*t2 + 2*Power(t2,2)) - 
                  Power(t1,2)*t2*
                   (8*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2)) - 
                  Power(s2,2)*
                   (4*Power(t1,3) + Power(t1,2)*t2 + 
                     4*t1*Power(t2,2) + Power(t2,3)) + 
                  2*s2*t1*(Power(t1,3) + 7*Power(t1,2)*t2 + 
                     5*t1*Power(t2,2) + 2*Power(t2,3)))) + 
            Power(s,2)*(Power(s1,4)*(s2 + t1) + 
               Power(s1,3)*(5*Power(s2,2) + 4*s2*t1 - 7*Power(t1,2) - 
                  5*s2*t2 - 2*t1*t2) + 
               Power(s1,2)*(2*Power(s2,3) + 
                  Power(s2,2)*(5*t1 - 12*t2) + 
                  s2*(-17*Power(t1,2) - 6*t1*t2 + 2*Power(t2,2)) + 
                  t1*(11*Power(t1,2) + 19*t1*t2 + 2*Power(t2,2))) - 
               s1*(4*Power(s2,3)*t2 + 
                  Power(s2,2)*
                   (6*Power(t1,2) + 3*t1*t2 - 4*Power(t2,2)) + 
                  t1*(5*Power(t1,3) + 21*Power(t1,2)*t2 + 
                     7*t1*Power(t2,2) + Power(t2,3)) - 
                  s2*(11*Power(t1,3) + 22*Power(t1,2)*t2 + 
                     6*t1*Power(t2,2) + 2*Power(t2,3))) - 
               t1*(2*Power(s2,3)*t2 + 
                  Power(s2,2)*
                   (-2*Power(t1,2) - 12*t1*t2 + Power(t2,2)) - 
                  t1*t2*(7*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
                  s2*(2*Power(t1,3) + 22*Power(t1,2)*t2 + 
                     t1*Power(t2,2) + 3*Power(t2,3)))))/Power(s + t1,2) + 
         (-2*Power(s,4)*(t1 - t2)*(s2*t1 + s1*t2) - 
            2*Power(s1,5)*(2*s2*t1 - s2*t2 + t1*t2) + 
            s2*t1*t2*(Power(s2,4) + Power(s2,2)*t1*(t1 - 4*t2) + 
               Power(s2,3)*(-2*t1 + t2) + 2*Power(t1,2)*t2*(-t1 + t2) + 
               s2*t2*(5*Power(t1,2) - 2*t1*t2 - 2*Power(t2,2))) + 
            Power(s1,4)*(-2*Power(s2,3) - 2*t1*(t1 - 2*t2)*t2 + 
               Power(s2,2)*(6*t1 + t2) + 
               s2*(-8*Power(t1,2) + 13*t1*t2 - 2*Power(t2,2))) + 
            Power(s,3)*(2*Power(s1,2)*(s2*t1 + 2*t1*t2 - Power(t2,2)) + 
               s2*t1*(2*s2*t1 - Power(t1,2) - 4*s2*t2 - 3*t1*t2 + 
                  4*Power(t2,2)) + 
               s1*(8*s2*Power(t1,2) - 2*Power(s2,2)*t2 - 
                  4*Power(t1,2)*t2 - 8*s2*Power(t2,2) + 
                  3*t1*Power(t2,2) + Power(t2,3))) - 
            Power(s1,3)*(3*Power(s2,3)*t1 - 
               t1*t2*(Power(t1,2) + 2*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,2)*(-8*Power(t1,2) + 4*t1*t2 + 5*Power(t2,2)) + 
               s2*(5*Power(t1,3) - 15*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(-(Power(s2,5)*(t1 - 2*t2)) + 
               2*Power(t1,3)*Power(t2,2)*(-t1 + t2) + 
               Power(s2,4)*(Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) + 
               s2*t1*t2*(5*Power(t1,3) + Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,3)*(Power(t1,3) + 9*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(-Power(t1,4) - 11*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) + 6*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s1,2)*(2*Power(s2,5) + Power(t1,3)*(3*t1 - 4*t2)*t2 + 
               Power(s2,4)*(-6*t1 + 3*t2) + 
               Power(s2,3)*(4*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,3) + Power(t1,2)*t2 - 
                  11*t1*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(-2*Power(t1,4) + Power(t1,2)*Power(t2,2) + 
                  3*t1*Power(t2,3) + 2*Power(t2,4))) - 
            Power(s,2)*(Power(s1,3)*
                (2*Power(s2,2) + 6*s2*t1 - 4*s2*t2 + 7*t1*t2 - 
                  3*Power(t2,2)) + 
               s2*t1*(Power(t1,3) + Power(s2,2)*(3*t1 - 5*t2) - 
                  2*Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
                  6*Power(t2,3) + 
                  s2*(-4*Power(t1,2) + 6*t1*t2 + Power(t2,2))) + 
               Power(s1,2)*(-2*Power(s2,3) + 
                  s2*(13*Power(t1,2) - 17*t1*t2 + Power(t2,2)) + 
                  t2*(3*Power(t1,2) - 10*t1*t2 + 4*Power(t2,2))) + 
               s1*(Power(s2,3)*(4*t1 - 6*t2) - 
                  Power(s2,2)*
                   (Power(t1,2) - 15*t1*t2 + 13*Power(t2,2)) - 
                  t2*(-8*Power(t1,3) + 11*Power(t1,2)*t2 - 
                     4*t1*Power(t2,2) + Power(t2,3)) + 
                  s2*(-Power(t1,3) - 36*Power(t1,2)*t2 + 
                     36*t1*Power(t2,2) + Power(t2,3)))) + 
            s*(Power(s1,4)*(2*Power(s2,2) + 8*s2*t1 - 5*s2*t2 + 
                  7*t1*t2 - 2*Power(t2,2)) + 
               Power(s1,3)*(2*Power(s2,3) - Power(s2,2)*(7*t1 + 2*t2) + 
                  t2*(9*Power(t1,2) - 16*t1*t2 + 4*Power(t2,2)) + 
                  s2*(15*Power(t1,2) - 26*t1*t2 + 6*Power(t2,2))) + 
               Power(s1,2)*(-4*Power(s2,4) - 
                  13*Power(s2,2)*t1*(t1 - t2) + 
                  3*Power(s2,3)*(4*t1 - t2) + 
                  (7*t1 - 2*t2)*Power(t1 - t2,2)*t2 + 
                  s2*(5*Power(t1,3) - 35*Power(t1,2)*t2 + 
                     34*t1*Power(t2,2) + Power(t2,3))) + 
               s2*t1*(Power(s2,3)*(t1 - 4*t2) + 
                  Power(s2,2)*
                   (-2*Power(t1,2) + 5*t1*t2 - 4*Power(t2,2)) + 
                  s2*(Power(t1,3) + 5*t1*Power(t2,2) - 6*Power(t2,3)) + 
                  t2*(-Power(t1,3) - Power(t1,2)*t2 + 2*Power(t2,3))) + 
               s1*(3*Power(s2,4)*(t1 - 2*t2) + 
                  Power(s2,3)*
                   (Power(t1,2) + 12*t1*t2 - 10*Power(t2,2)) - 
                  Power(s2,2)*
                   (7*Power(t1,3) + 13*Power(t1,2)*t2 - 
                     20*t1*Power(t2,2) + Power(t2,3)) - 
                  t1*t2*(3*Power(t1,3) + Power(t1,2)*t2 - 
                     6*t1*Power(t2,2) + 2*Power(t2,3)) + 
                  s2*(3*Power(t1,4) + 10*Power(t1,3)*t2 + 
                     5*Power(t1,2)*Power(t2,2) - 16*t1*Power(t2,3) - 
                     2*Power(t2,4)))))/Power(s1 - s2 + t1 - t2,2))*
       Log(s - s2 + t1))/(s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) - 
    (8*(-(Power(s1,6)*t1*(s2 + t1)*(t1 - 2*t2)) - 
         Power(s,4)*Power(t1 - t2,2)*(t1 + t2)*
          (3*Power(s1,2) - 4*s1*t2 + 2*Power(t2,2)) + 
         Power(s1,5)*(-3*Power(s2,3)*(t1 - t2) + 
            Power(s2,2)*(5*Power(t1,2) + t1*t2 - 6*Power(t2,2)) + 
            Power(t1,2)*(2*Power(t1,2) + t1*t2 - 5*Power(t2,2)) + 
            s2*t1*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
         Power(s1,4)*(Power(s2,3)*
             (Power(t1,2) + 15*t1*t2 - 16*Power(t2,2)) + 
            s2*t1*t2*(-3*Power(t1,2) + 13*t1*t2 - 14*Power(t2,2)) + 
            Power(t1,2)*(Power(t1,3) - 6*Power(t1,2)*t2 + 
               2*t1*Power(t2,2) + 3*Power(t2,3)) + 
            Power(s2,2)*(4*Power(t1,3) - 33*Power(t1,2)*t2 + 
               15*t1*Power(t2,2) + 14*Power(t2,3))) + 
         s2*Power(t2,3)*(Power(s2,3)*
             (-3*Power(t1,2) + 4*t1*t2 - 2*Power(t2,2)) + 
            3*Power(s2,2)*t1*(3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) - 
            2*t1*(2*Power(t1,4) - 4*Power(t1,3)*t2 + 
               4*Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) + 
               Power(t2,4)) + 
            s2*(-2*Power(t1,4) + Power(t1,3)*t2 + 
               8*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
               2*Power(t2,4))) + 
         s1*Power(t2,2)*(Power(s2,4)*
             (3*Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*t2*(9*Power(t1,3) - 20*Power(t1,2)*t2 + 
               20*t1*Power(t2,2) - 8*Power(t2,3)) + 
            2*Power(t1,2)*t2*
             (-Power(t1,3) + 2*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
               Power(t2,3)) - 
            Power(s2,3)*(15*Power(t1,3) - 19*Power(t1,2)*t2 + 
               3*t1*Power(t2,2) + 2*Power(t2,3)) + 
            s2*t1*(10*Power(t1,4) - 21*Power(t1,3)*t2 + 
               20*Power(t1,2)*Power(t2,2) - 16*t1*Power(t2,3) + 
               7*Power(t2,4))) + 
         Power(s1,2)*t2*(Power(s2,4)*
             (3*Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) - 
            2*Power(s2,3)*t2*
             (-7*Power(t1,2) + 3*t1*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*t2*(4*Power(t1,3) - 6*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - 3*Power(t2,3)) + 
            s2*t1*(-11*Power(t1,4) + 26*Power(t1,3)*t2 - 
               24*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) - 
               16*Power(t2,4)) + 
            Power(s2,2)*(13*Power(t1,4) - 34*Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - 2*t1*Power(t2,3) + 
               12*Power(t2,4))) + 
         Power(s1,3)*(Power(s2,4)*
             (-3*Power(t1,2) + 4*t1*t2 - 2*Power(t2,2)) + 
            Power(t1,2)*t2*(-3*Power(t1,3) + 6*Power(t1,2)*t2 - 
               2*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s2,3)*(6*Power(t1,3) - 19*Power(t1,2)*t2 - 
               9*t1*Power(t2,2) + 21*Power(t2,3)) + 
            Power(s2,2)*(-10*Power(t1,4) + 18*Power(t1,3)*t2 + 
               35*Power(t1,2)*Power(t2,2) - 26*t1*Power(t2,3) - 
               14*Power(t2,4)) + 
            s2*t1*(5*Power(t1,4) - 11*Power(t1,3)*t2 + 
               9*Power(t1,2)*Power(t2,2) - 18*t1*Power(t2,3) + 
               22*Power(t2,4))) - 
         Power(s,3)*(t1 - t2)*
          (Power(s1,3)*(-10*Power(t1,2) + 6*t1*t2 + 7*Power(t2,2) + 
               4*s2*(t1 + t2)) + 
            Power(s1,2)*(s2*t1*(t1 - 14*t2) + 
               t2*(25*Power(t1,2) - 18*t1*t2 - 13*Power(t2,2))) + 
            t2*(-4*t1*t2*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
               s2*(3*Power(t1,3) - 6*Power(t1,2)*t2 - 
                  2*t1*Power(t2,2) + 8*Power(t2,3))) + 
            s1*(s2*(-3*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) - 10*Power(t2,3)) + 
               t2*(4*Power(t1,3) - 26*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + 8*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,4)*
             (-12*Power(t1,3) - 3*Power(s2,2)*(t1 - t2) + 
               25*Power(t1,2)*t2 - 8*t1*Power(t2,2) - 4*Power(t2,3) + 
               s2*(5*Power(t1,2) + 2*t1*t2 - 8*Power(t2,2))) + 
            Power(s1,3)*(2*Power(t1,4) + 37*Power(t1,3)*t2 - 
               86*Power(t1,2)*Power(t2,2) + 35*t1*Power(t2,3) + 
               10*Power(t2,4) + 
               Power(s2,2)*(8*Power(t1,2) + 4*t1*t2 - 11*Power(t2,2)) + 
               s2*(-3*Power(t1,3) - 25*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s1,2)*(Power(t1,5) + 3*Power(t1,4)*t2 - 
               58*Power(t1,3)*Power(t2,2) + 
               112*Power(t1,2)*Power(t2,3) - 49*t1*Power(t2,4) - 
               8*Power(t2,5) + 
               Power(s2,2)*(8*Power(t1,3) - 36*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(-13*Power(t1,4) + 45*Power(t1,3)*t2 - 
                  10*Power(t1,2)*Power(t2,2) - 27*t1*Power(t2,3) + 
                  4*Power(t2,4))) + 
            t2*(2*Power(t1 - t2,2)*t2*
                (Power(t1,3) + 2*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,2)*(3*Power(t1,4) - 11*Power(t1,3)*t2 + 
                  5*Power(t1,2)*Power(t2,2) + 14*t1*Power(t2,3) - 
                  12*Power(t2,4)) + 
               s2*t1*(-3*Power(t1,4) - 3*Power(t1,3)*t2 + 
                  29*Power(t1,2)*Power(t2,2) - 43*t1*Power(t2,3) + 
                  20*Power(t2,4))) + 
            s1*(-2*t1*t2*(Power(t1,4) + 4*Power(t1,3)*t2 - 
                  25*Power(t1,2)*Power(t2,2) + 37*t1*Power(t2,3) - 
                  17*Power(t2,4)) + 
               Power(s2,2)*(-3*Power(t1,4) + 4*Power(t1,3)*t2 + 
                  20*Power(t1,2)*Power(t2,2) - 32*t1*Power(t2,3) + 
                  14*Power(t2,4)) + 
               s2*(3*Power(t1,5) + 14*Power(t1,4)*t2 - 
                  65*Power(t1,3)*Power(t2,2) + 
                  65*Power(t1,2)*Power(t2,3) - 9*t1*Power(t2,4) - 
                  8*Power(t2,5)))) + 
         s*(Power(s1,5)*(3*Power(s2,2)*(t1 - t2) + 4*s2*t2*(-t1 + t2) + 
               2*t1*(3*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,4)*(3*Power(s2,3)*(t1 - t2) - 
               2*Power(s2,2)*
                (6*Power(t1,2) + 5*t1*t2 - 11*Power(t2,2)) + 
               s2*(2*Power(t1,3) + 13*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) - 8*Power(t2,3)) - 
               t1*(4*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  52*t1*Power(t2,2) + 27*Power(t2,3))) + 
            Power(s1,3)*(-(Power(s2,3)*
                  (Power(t1,2) + 8*t1*t2 - 9*Power(t2,2))) - 
               2*Power(s2,2)*
                (5*Power(t1,3) - 32*Power(t1,2)*t2 + 9*t1*Power(t2,2) + 
                  17*Power(t2,3)) + 
               2*s2*(5*Power(t1,4) - 19*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               t1*(-2*Power(t1,4) + 7*Power(t1,3)*t2 + 
                  26*Power(t1,2)*Power(t2,2) - 76*t1*Power(t2,3) + 
                  43*Power(t2,4))) + 
            Power(t2,2)*(2*t1*Power(t1 - t2,2)*t2*
                (Power(t1,2) + Power(t2,2)) + 
               2*Power(s2,2)*t1*
                (Power(t1,3) - 10*Power(t1,2)*t2 + 18*t1*Power(t2,2) - 
                  9*Power(t2,3)) + 
               2*Power(s2,3)*
                (2*Power(t1,3) - 5*t1*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-6*Power(t1,5) + 7*Power(t1,4)*t2 + 
                  7*Power(t1,3)*Power(t2,2) - 
                  20*Power(t1,2)*Power(t2,3) + 16*t1*Power(t2,4) - 
                  4*Power(t2,5))) + 
            s1*t2*(-2*Power(s2,3)*t2*
                (7*Power(t1,2) - 10*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(-14*Power(t1,4) + 52*Power(t1,3)*t2 - 
                  45*Power(t1,2)*Power(t2,2) + 7*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               t1*t2*(-6*Power(t1,4) + 8*Power(t1,3)*t2 + 
                  4*Power(t1,2)*Power(t2,2) - 13*t1*Power(t2,3) + 
                  7*Power(t2,4)) + 
               2*s2*(6*Power(t1,5) - 4*Power(t1,4)*t2 - 
                  20*Power(t1,3)*Power(t2,2) + 
                  33*Power(t1,2)*Power(t2,3) - 19*t1*Power(t2,4) + 
                  4*Power(t2,5))) - 
            Power(s1,2)*(Power(s2,3)*
                (4*Power(t1,3) - 15*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s2,2)*(-13*Power(t1,4) + 24*Power(t1,3)*t2 + 
                  42*Power(t1,2)*Power(t2,2) - 36*t1*Power(t2,3) - 
                  13*Power(t2,4)) + 
               t1*t2*(-5*Power(t1,4) + 3*Power(t1,3)*t2 + 
                  28*Power(t1,2)*Power(t2,2) - 57*t1*Power(t2,3) + 
                  31*Power(t2,4)) + 
               s2*(8*Power(t1,5) + 5*Power(t1,4)*t2 - 
                  67*Power(t1,3)*Power(t2,2) + 
                  65*Power(t1,2)*Power(t2,3) - 15*t1*Power(t2,4) + 
                  6*Power(t2,5)))))*Log(-t2))/
     (s1*s2*(-s + s2 - t1)*t1*Power(s1 - t2,2)*(-s + s1 - t2)*
       Power(t1 - t2,2)*t2) + (8*
       (-(s2*t1*t2*(Power(s2,2) + s2*t2 - 2*t1*t2)) + 
         2*Power(s1,2)*(Power(s2,3) - Power(s2,2)*t1 + Power(t1,2)*t2) + 
         2*Power(s,2)*(s2*Power(t1,2) + s1*Power(t2,2)) + 
         Power(s,3)*(s2*(t1 - t2) - 2*t2*(t1 + t2)) + 
         s1*(Power(s2,3)*(t1 - 2*t2) + 2*Power(t1,2)*Power(t2,2) + 
            s2*t1*t2*(-3*t1 + t2) + Power(s2,2)*Power(t1 + t2,2)) + 
         Power(s1,3)*(2*Power(s2,2) - t1*t2 + s2*(2*t1 + t2)) + 
         s1*(Power(s2,4) + Power(s2,2)*Power(t1,2) + 
            Power(t1,2)*(t1 - t2)*t2 - 3*s2*t1*Power(t2,2) - 
            Power(s2,3)*(2*t1 + t2)) + 
         Power(s1,2)*(2*Power(s2,3) + Power(s2,2)*(t1 - t2) - 
            Power(t1,2)*t2 + s2*(3*Power(t1,2) + Power(t2,2))) + 
         s2*t2*(Power(s2,3) + Power(s2,2)*(3*t1 + t2) - 
            s2*t1*(6*t1 + t2) + t1*(2*Power(t1,2) - t1*t2 + Power(t2,2))) \
+ Power(s,2)*(Power(s2,2)*(-2*t1 + 3*t2) - t1*t2*(t1 + 5*t2) + 
            s2*(Power(t1,2) + 4*t1*t2 + 5*Power(t2,2)) + 
            s1*(Power(s2,2) - s2*t1 + 2*t2*(t1 + t2))) + 
         s*(-(Power(s1,2)*(2*Power(s2,2) + 2*s2*t1 + t2*(-t1 + t2))) + 
            s2*t1*(s2*(-t1 + t2) + t2*(3*t1 + t2)) + 
            s1*(2*Power(s2,2)*t2 + t1*t2*(t1 + 3*t2) + 
               s2*(-5*Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)))) - 
         s*(2*Power(s1,2)*Power(s2,2) - Power(s2,3)*(t1 - 3*t2) - 
            s2*t1*t2*(5*t1 + 6*t2) + t1*t2*(Power(t1,2) + Power(t2,2)) + 
            Power(s2,2)*(Power(t1,2) + 5*t1*t2 + 4*Power(t2,2)) + 
            s1*(2*Power(s2,3) + t1*(t1 - 4*t2)*t2 - 
               Power(s2,2)*(3*t1 + t2) + 
               s2*(Power(t1,2) + 2*t1*t2 + 2*Power(t2,2)))) - 
         (2*Power(s1,4)*(s2 - t1)*(Power(s2,2) - s2*t1 + t1*t2) + 
            Power(s,5)*(s2*(t1 - t2) + 2*t2*(t1 + t2)) + 
            s2*Power(t2,3)*(Power(s2,3) - Power(s2,2)*(7*t1 + t2) + 
               s2*t1*(8*t1 + t2) - 
               t1*(2*Power(t1,2) - t1*t2 + Power(t2,2))) + 
            s1*Power(t2,2)*(-Power(s2,4) + Power(t1,2)*t2*(-t1 + t2) - 
               2*Power(s2,2)*t1*(4*t1 + 3*t2) + 
               Power(s2,3)*(7*t1 + 4*t2) + 
               2*s2*t1*(Power(t1,2) + t1*t2 + Power(t2,2))) + 
            Power(s1,3)*(Power(s2,4) - 2*Power(s2,3)*t1 - 
               Power(t1,2)*(t1 - 6*t2)*t2 - s2*t1*t2*(t1 + 6*t2) + 
               Power(s2,2)*(Power(t1,2) + 2*t1*t2 + 2*Power(t2,2))) - 
            Power(s1,2)*t2*(Power(s2,4) + Power(s2,3)*(-2*t1 + t2) + 
               Power(t1,2)*t2*(-2*t1 + 3*t2) + 
               s2*t1*(3*Power(t1,2) + t1*t2 + Power(t2,2)) - 
               Power(s2,2)*(2*Power(t1,2) + 3*t1*t2 + 2*Power(t2,2))) + 
            Power(s,4)*(Power(s2,2)*(-2*t1 + 3*t2) - 
               s2*t2*(3*t1 + 7*t2) + 
               s1*(Power(s2,2) - 4*s2*t1 + 3*s2*t2 - 6*t1*t2 - 
                  4*Power(t2,2)) + 
               t2*(Power(t1,2) + 9*t1*t2 + 4*Power(t2,2))) + 
            Power(s,3)*(Power(s2,3)*(t1 - 3*t2) + 
               Power(s2,2)*t2*(3*t1 + 10*t2) + 
               t2*(Power(t1,3) + 2*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 2*Power(t2,3)) - 
               s2*(Power(t1,3) + Power(t1,2)*t2 + 18*t1*Power(t2,2) + 
                  11*Power(t2,3)) + 
               Power(s1,2)*(-3*Power(s2,2) + s2*(5*t1 - 4*t2) + 
                  2*t2*(3*t1 + t2)) - 
               s1*(2*Power(s2,3) + Power(s2,2)*(-9*t1 + 7*t2) + 
                  2*s2*(Power(t1,2) - 3*t1*t2 - 6*Power(t2,2)) + 
                  t2*(2*Power(t1,2) + 21*t1*t2 + 6*Power(t2,2)))) + 
            s*(-2*Power(s1,4)*s2*(s2 - t1) - 
               Power(s1,3)*(3*Power(s2,3) - 7*Power(s2,2)*t1 - 
                  4*Power(t1,2)*t2 + 
                  2*s2*(2*Power(t1,2) + 2*t1*t2 + Power(t2,2))) + 
               Power(t2,2)*(2*Power(s2,4) - 
                  5*Power(s2,3)*(2*t1 + t2) + 
                  t1*t2*(Power(t1,2) + Power(t2,2)) + 
                  Power(s2,2)*
                   (10*Power(t1,2) + 15*t1*t2 + 4*Power(t2,2)) - 
                  s2*t1*(2*Power(t1,2) + 5*t1*t2 + 8*Power(t2,2))) + 
               s1*t2*(2*Power(s2,3)*(t1 + 5*t2) + 
                  t1*t2*(-4*Power(t1,2) + 2*t1*t2 - 7*Power(t2,2)) - 
                  3*Power(s2,2)*
                   (2*Power(t1,2) + 7*t1*t2 + 3*Power(t2,2)) + 
                  2*s2*(2*Power(t1,3) + 5*Power(t1,2)*t2 + 
                     7*t1*Power(t2,2) + Power(t2,3))) - 
               Power(s1,2)*(2*Power(s2,4) - 6*Power(s2,3)*t1 + 
                  t1*t2*(-3*Power(t1,2) + 7*t1*t2 - 8*Power(t2,2)) + 
                  Power(s2,2)*(3*Power(t1,2) - t1*t2 - 5*Power(t2,2)) + 
                  s2*(Power(t1,3) + 4*Power(t1,2)*t2 + t1*Power(t2,2) + 
                     4*Power(t2,3)))) + 
            Power(s,2)*(2*Power(s1,3)*(Power(s2,2) - 2*s2*t1 - t1*t2) + 
               Power(s1,2)*(5*Power(s2,3) + 
                  Power(s2,2)*(-12*t1 + 5*t2) + 
                  s2*(4*Power(t1,2) - 3*t1*t2 - 6*Power(t2,2)) + 
                  t2*(-Power(t1,2) + 12*t1*t2 + 2*Power(t2,2))) + 
               t2*(Power(s2,4) - Power(s2,3)*(2*t1 + 7*t2) + 
                  t1*t2*(2*Power(t1,2) + t1*t2 + 7*Power(t2,2)) + 
                  Power(s2,2)*
                   (2*Power(t1,2) + 19*t1*t2 + 11*Power(t2,2)) - 
                  s2*(Power(t1,3) + 7*Power(t1,2)*t2 + 
                     21*t1*Power(t2,2) + 5*Power(t2,3))) + 
               s1*(Power(s2,4) + Power(s2,3)*(-5*t1 + 4*t2) + 
                  Power(s2,2)*
                   (2*Power(t1,2) - 6*t1*t2 - 17*Power(t2,2)) - 
                  t2*(3*Power(t1,3) + Power(t1,2)*t2 + 
                     22*t1*Power(t2,2) + 2*Power(t2,3)) + 
                  s2*(2*Power(t1,3) + 6*Power(t1,2)*t2 + 
                     22*t1*Power(t2,2) + 11*Power(t2,3)))))/
          Power(s + t2,2) + (2*Power(s,4)*(t1 - t2)*(s2*t1 + s1*t2) + 
            Power(s1,5)*(2*Power(s2,2) + 2*s2*t1 - s2*t2 + t1*t2) + 
            Power(s1,4)*(3*Power(s2,2)*(t1 - 2*t2) + t1*(t1 - 2*t2)*t2 + 
               s2*(3*Power(t1,2) - 5*t1*t2 + Power(t2,2))) - 
            s2*t1*(s2 - t2)*t2*
             (2*Power(s2,3) - 4*Power(s2,2)*(t1 - t2) + 
               2*t1*(t1 - t2)*t2 + 
               s2*(2*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2))) + 
            Power(s,3)*(s2*t1*
                (-2*s2*t1 + Power(t1,2) + 4*s2*t2 + 3*t1*t2 - 
                  4*Power(t2,2)) - 
               2*Power(s1,2)*(s2*t1 + 2*t1*t2 - Power(t2,2)) + 
               s1*(-8*s2*Power(t1,2) + 2*Power(s2,2)*t2 + 
                  4*Power(t1,2)*t2 + 8*s2*Power(t2,2) - 
                  3*t1*Power(t2,2) - Power(t2,3))) + 
            Power(s1,3)*(-2*Power(s2,4) - 3*Power(s2,3)*t2 + 
               t1*Power(t2,2)*(-4*t1 + t2) + 
               Power(s2,2)*(3*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)) + 
               s2*(Power(t1,3) - 5*Power(t1,2)*t2 + 9*t1*Power(t2,2) + 
                  Power(t2,3))) + 
            s1*(2*Power(s2,5)*(t1 - 2*t2) + 
               2*Power(t1,2)*(t1 - t2)*Power(t2,3) + 
               Power(s2,4)*(-2*Power(t1,2) + 13*t1*t2 - 8*Power(t2,2)) - 
               Power(s2,3)*(2*Power(t1,3) + 10*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 5*Power(t2,3)) + 
               s2*t1*t2*(-2*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*(2*Power(t1,4) + 3*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 2*Power(t2,4))) + 
            Power(s1,2)*(Power(s2,4)*(t1 + 6*t2) + 
               Power(t1,2)*t2*
                (-2*Power(t1,2) - 2*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(-5*Power(t1,2) - 4*t1*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*(3*Power(t1,3) - 11*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(Power(t1,4) + 6*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) - 11*t1*Power(t2,3) - 
                  Power(t2,4))) + 
            Power(s,2)*(Power(s1,3)*
                (2*Power(s2,2) + 6*s2*t1 - 4*s2*t2 + 5*t1*t2 - 
                  3*Power(t2,2)) + 
               s2*t1*(Power(t1,3) + Power(s2,2)*(3*t1 - 7*t2) - 
                  4*Power(t1,2)*t2 + 11*t1*Power(t2,2) - 
                  8*Power(t2,3) + 
                  s2*(-4*Power(t1,2) + 10*t1*t2 - 3*Power(t2,2))) + 
               Power(s1,2)*(-2*Power(s2,3) + 
                  s2*(13*Power(t1,2) - 15*t1*t2 + Power(t2,2)) + 
                  t2*(-Power(t1,2) - 6*t1*t2 + 4*Power(t2,2))) + 
               s1*(Power(s2,3)*(4*t1 - 6*t2) - 
                  Power(s2,2)*
                   (Power(t1,2) - 17*t1*t2 + 13*Power(t2,2)) - 
                  t2*(-6*Power(t1,3) + 7*Power(t1,2)*t2 - 
                     2*t1*Power(t2,2) + Power(t2,3)) + 
                  s2*(-Power(t1,3) - 36*Power(t1,2)*t2 + 
                     36*t1*Power(t2,2) + Power(t2,3)))) + 
            s*(Power(s1,4)*(-4*Power(s2,2) - 6*s2*t1 + 3*s2*t2 - 
                  4*t1*t2 + Power(t2,2)) + 
               Power(s1,3)*(2*Power(s2,3) - 3*Power(s2,2)*(t1 - 4*t2) + 
                  t2*(-4*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) + 
                  s2*(-10*Power(t1,2) + 12*t1*t2 + Power(t2,2))) - 
               s2*t1*(Power(s2,3)*(2*t1 - 7*t2) + 
                  s2*(2*t1 - 7*t2)*Power(t1 - t2,2) + 
                  Power(s2,2)*
                   (-4*Power(t1,2) + 16*t1*t2 - 9*Power(t2,2)) + 
                  t2*(2*Power(t1,3) - 6*Power(t1,2)*t2 + 
                     t1*Power(t2,2) + 3*Power(t2,3))) + 
               Power(s1,2)*(2*Power(s2,4) + 
                  13*Power(s2,2)*(t1 - t2)*t2 - 
                  Power(s2,3)*(2*t1 + 7*t2) + 
                  t2*(-6*Power(t1,3) + 5*Power(t1,2)*t2 + Power(t2,3)) - 
                  s2*(Power(t1,3) - 20*Power(t1,2)*t2 + 
                     13*t1*Power(t2,2) + 7*Power(t2,3))) + 
               s1*(Power(s2,4)*(-5*t1 + 8*t2) + 
                  Power(s2,3)*
                   (6*Power(t1,2) - 26*t1*t2 + 15*Power(t2,2)) + 
                  t1*t2*(2*Power(t1,3) - t1*Power(t2,2) - Power(t2,3)) + 
                  Power(s2,2)*
                   (Power(t1,3) + 34*Power(t1,2)*t2 - 
                     35*t1*Power(t2,2) + 5*Power(t2,3)) + 
                  s2*(-2*Power(t1,4) - 16*Power(t1,3)*t2 + 
                     5*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) + 
                     3*Power(t2,4)))))/Power(s1 - s2 + t1 - t2,2))*
       Log(s - s1 + t2))/(s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2));
       
       return a;
};
