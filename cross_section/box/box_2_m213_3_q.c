#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m213_3_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(8*Power(s,7)*s1*(-1 + t2)*Power(t2,2)*
          (s1 + Power(s1,2) - 3*s1*t2 + t2*(-1 + 2*t2)) + 
         Power(s,6)*t2*(2*Power(-1 + t2,2)*Power(t2,2) - 
            8*Power(s1,4)*(1 - 5*t2 + 4*Power(t2,2)) + 
            Power(s1,3)*(-8 + (92 + 16*s2 - 25*t1)*t2 + 
               (-183 - 16*s2 + 13*t1)*Power(t2,2) + 111*Power(t2,3)) + 
            Power(s1,2)*t2*(34 - 185*t2 + 242*Power(t2,2) - 
               115*Power(t2,3) + t1*(-19 + 88*t2 - 45*Power(t2,2)) + 
               8*s2*(1 - 6*t2 + 5*Power(t2,2))) + 
            s1*t2*(10 - 8*(5 + s2)*t2 + (93 + 32*s2)*Power(t2,2) - 
               3*(27 + 8*s2)*Power(t2,3) + 30*Power(t2,4) + 
               t1*(-6 + 25*t2 - 57*Power(t2,2) + 26*Power(t2,3)))) + 
         Power(s,5)*t2*(2*Power(-1 + t2,2)*Power(t2,2)*(-7 + 2*t2) + 
            24*Power(s1,5)*(1 - 3*t2 + 2*Power(t2,2)) + 
            Power(s1,4)*(42 - 194*t2 + 322*Power(t2,2) - 
               194*Power(t2,3) + t1*(-26 + 109*t2 - 47*Power(t2,2)) + 
               s2*(16 - 81*t2 + 53*Power(t2,2))) + 
            Power(s1,3)*(16 - 178*t2 + 8*Power(s2,2)*(-1 + t2)*t2 + 
               455*Power(t2,2) - 544*Power(t2,3) + 275*Power(t2,4) + 
               Power(t1,2)*(-2 - 30*t2 + 8*Power(t2,2)) + 
               s2*(8 + (-139 + 38*t1)*t2 + (281 - 14*t1)*Power(t2,2) - 
                  162*Power(t2,3)) + 
               t1*(-23 + 269*t2 - 440*Power(t2,2) + 182*Power(t2,3))) + 
            s1*t2*(-50 + (42 + 52*s2)*t2 + 
               (9 - 179*s2 - 8*Power(s2,2))*Power(t2,2) + 
               (42 + 131*s2 + 8*Power(s2,2))*Power(t2,3) - 
               5*(17 + 8*s2)*Power(t2,4) + 18*Power(t2,5) + 
               2*Power(t1,2)*
                (-5 + 13*t2 - 28*Power(t2,2) + 8*Power(t2,3)) + 
               t1*(61 - 2*(79 + 6*s2)*t2 + (258 + 62*s2)*Power(t2,2) - 
                  13*(11 + 2*s2)*Power(t2,3) + 42*Power(t2,4))) - 
            Power(s1,2)*(-10 + 6*(7 + 10*s2)*t2 - 
               (221 + 302*s2 + 16*Power(s2,2))*Power(t2,2) + 
               (391 + 331*s2 + 16*Power(s2,2))*Power(t2,3) - 
               (361 + 149*s2)*Power(t2,4) + 135*Power(t2,5) + 
               2*Power(t1,2)*
                (1 + 6*t2 - 49*Power(t2,2) + 18*Power(t2,3)) + 
               t1*(7 - (133 + 12*s2)*t2 + (561 + 100*s2)*Power(t2,2) - 
                  2*(261 + 20*s2)*Power(t2,3) + 171*Power(t2,4)))) + 
         Power(s,3)*(8*Power(s1,7)*Power(-1 + t2,2)*t2 - 
            2*Power(-1 + t2,2)*Power(t2,3)*(21 - 20*t2 + 4*Power(t2,2)) + 
            Power(s1,6)*(t1*(3 - 69*t2 + 100*Power(t2,2) - 
                  22*Power(t2,3)) + 
               s2*(-1 + 49*t2 - 76*Power(t2,2) + 16*Power(t2,3)) + 
               6*(1 - 9*t2 + 11*Power(t2,2) + 6*Power(t2,3) - 
                  9*Power(t2,4))) + 
            Power(s1,2)*t2*(56 + (528 - 301*s2)*t2 + 
               (-1846 + 885*s2 + 60*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,2) - 
               (-1967 + 801*s2 + 101*Power(s2,2) + 31*Power(s2,3))*
                Power(t2,3) + 
               (-739 + 510*s2 + 26*Power(s2,2) - 9*Power(s2,3))*
                Power(t2,4) + 
               (29 - 229*s2 - 9*Power(s2,2))*Power(t2,5) + 
               (15 + 68*s2)*Power(t2,6) - 10*Power(t2,7) + 
               Power(t1,3)*(-5 + 38*t2 - 144*Power(t2,2) + 
                  157*Power(t2,3) - 34*Power(t2,4)) + 
               Power(t1,2)*(28 - 19*(12 + s2)*t2 + 
                  (841 + 169*s2)*Power(t2,2) - 
                  (911 + 238*s2)*Power(t2,3) + 
                  (387 + 28*s2)*Power(t2,4) - 45*Power(t2,5)) + 
               t1*(-134 + 7*(41 + 32*s2)*t2 - 
                  3*(108 + 359*s2 + 16*Power(s2,2))*Power(t2,2) + 
                  (179 + 1304*s2 + 113*Power(s2,2))*Power(t2,3) + 
                  (-344 - 577*s2 + 19*Power(s2,2))*Power(t2,4) + 
                  (263 + 78*s2)*Power(t2,5) - 59*Power(t2,6))) + 
            s1*Power(t2,2)*(-63 + (-412 + 205*s2)*t2 - 
               (-1283 + 514*s2 + 17*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,2) + 
               (-1070 + 428*s2 + 3*Power(s2,2) + 13*Power(s2,3))*
                Power(t2,3) + 
               (240 - 213*s2 - 11*Power(s2,2) + Power(s2,3))*
                Power(t2,4) + (30 + 66*s2 + Power(s2,2))*Power(t2,5) - 
               8*(1 + s2)*Power(t2,6) + 
               Power(t1,3)*(10 - 25*t2 + 53*Power(t2,2) - 
                  32*Power(t2,3) + 6*Power(t2,4)) + 
               Power(t1,2)*(-75 + (168 + 17*s2)*t2 - 
                  (269 + 88*s2)*Power(t2,2) + 
                  4*(51 + 16*s2)*Power(t2,3) - (76 + 5*s2)*Power(t2,4)) \
+ t1*(213 - (211 + 166*s2)*t2 + 
                  (-185 + 556*s2 + 19*Power(s2,2))*Power(t2,2) + 
                  (160 - 463*s2 - 25*Power(s2,2))*Power(t2,3) + 
                  (121 + 152*s2 - 6*Power(s2,2))*Power(t2,4) - 
                  (68 + 7*s2)*Power(t2,5) + 6*Power(t2,6))) + 
            Power(s1,3)*(1 + (-174 + 95*s2)*t2 - 
               (-790 + 428*s2 + 69*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,2) + 
               (-1446 + 451*s2 + 223*Power(s2,2) + 23*Power(s2,3))*
                Power(t2,3) + 
               (1175 - 399*s2 - 87*Power(s2,2) + 15*Power(s2,3))*
                Power(t2,4) + 
               (-398 + 308*s2 + 5*Power(s2,2))*Power(t2,5) - 
               (14 + 195*s2)*Power(t2,6) + 66*Power(t2,7) + 
               Power(t1,3)*(1 - 4*t2 + 76*Power(t2,2) - 
                  187*Power(t2,3) + 54*Power(t2,4)) + 
               Power(t1,2)*(5 + (38 + 3*s2)*t2 - 
                  2*(298 + 49*s2)*Power(t2,2) + 
                  (1173 + 284*s2)*Power(t2,3) - 
                  (851 + 33*s2)*Power(t2,4) + 159*Power(t2,5)) + 
               t1*(5 - (163 + 58*s2)*t2 + 
                  (687 + 644*s2 + 39*Power(s2,2))*Power(t2,2) - 
                  (729 + 1337*s2 + 151*Power(s2,2))*Power(t2,3) + 
                  (658 + 914*s2 - 20*Power(s2,2))*Power(t2,4) - 
                  (498 + 163*s2)*Power(t2,5) + 208*Power(t2,6))) + 
            Power(s1,5)*(11 + (-122 + 31*s2 + 30*Power(s2,2))*t2 + 
               (310 - 76*s2 - 68*Power(s2,2))*Power(t2,2) + 
               (-283 + 172*s2 - 10*Power(s2,2))*Power(t2,3) - 
               (57 + 115*s2)*Power(t2,4) + 141*Power(t2,5) + 
               2*Power(t1,2)*
                (3 + 37*t2 - 92*Power(t2,2) + 16*Power(t2,3)) + 
               t1*(15 - 124*t2 + 298*Power(t2,2) - 327*Power(t2,3) + 
                  126*Power(t2,4) - 
                  2*s2*(1 + 55*t2 - 124*Power(t2,2) + 8*Power(t2,3)))) + 
            Power(s1,4)*(6 - 138*t2 + 597*Power(t2,2) - 852*Power(t2,3) + 
               488*Power(t2,4) + 50*Power(t2,5) - 151*Power(t2,6) - 
               Power(s2,3)*Power(t2,2)*(5 + 7*t2) + 
               Power(t1,3)*(3 - 9*t2 + 50*Power(t2,2) - 8*Power(t2,3)) + 
               Power(t1,2)*(10 + 90*t2 - 504*Power(t2,2) + 
                  694*Power(t2,3) - 170*Power(t2,4)) + 
               t1*(15 - 211*t2 + 499*Power(t2,2) - 724*Power(t2,3) + 
                  614*Power(t2,4) - 265*Power(t2,5)) + 
               Power(s2,2)*t2*
                (26 - 155*t2 + 140*Power(t2,2) + 13*Power(t2,3) + 
                  t1*(-10 + 63*t2 + 7*Power(t2,2))) + 
               s2*(1 + 57*t2 - 108*Power(t2,2) + 129*Power(t2,3) - 
                  241*Power(t2,4) + 234*Power(t2,5) + 
                  Power(t1,2)*
                   (-1 + 17*t2 - 110*Power(t2,2) + 10*Power(t2,3)) + 
                  t1*t2*(-121 + 606*t2 - 737*Power(t2,2) + 
                     108*Power(t2,3))))) - 
         Power(s,4)*(-2*Power(-1 + t2,2)*Power(t2,3)*
             (18 - 11*t2 + Power(t2,2)) + 
            8*Power(s1,6)*t2*(3 - 7*t2 + 4*Power(t2,2)) + 
            Power(s1,5)*(2 + (7 + 50*s2)*t2 - 
               3*(27 + 44*s2)*Power(t2,2) + (216 + 58*s2)*Power(t2,3) - 
               156*Power(t2,4) + 
               t1*(1 - 75*t2 + 168*Power(t2,2) - 58*Power(t2,3))) + 
            Power(s1,4)*(2 + (-12 + 67*s2 + 8*Power(s2,2))*t2 + 
               (40 - 305*s2 - 46*Power(s2,2))*Power(t2,2) + 
               (133 + 417*s2 + 14*Power(s2,2))*Power(t2,3) - 
               (408 + 227*s2)*Power(t2,4) + 293*Power(t2,5) + 
               2*Power(t1,2)*
                (1 + 11*t2 - 62*Power(t2,2) + 14*Power(t2,3)) + 
               t1*(4 - (157 + 36*s2)*t2 + 6*(95 + 28*s2)*Power(t2,2) - 
                  (673 + 36*s2)*Power(t2,3) + 256*Power(t2,4))) - 
            s1*Power(t2,2)*(89 + (139 - 143*s2)*t2 + 
               (-609 + 403*s2 + 34*Power(s2,2))*Power(t2,2) + 
               (436 - 306*s2 - 31*Power(s2,2))*Power(t2,3) + 
               (-8 + 139*s2 + 9*Power(s2,2))*Power(t2,4) - 
               (40 + 21*s2)*Power(t2,5) + 5*Power(t2,6) + 
               Power(t1,3)*(-5 + 10*t2 - 22*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(50 - (127 + 2*s2)*t2 + 
                  (224 + 34*s2)*Power(t2,2) - 
                  2*(53 + 4*s2)*Power(t2,3) + 19*Power(t2,4)) + 
               t1*(-180 + (334 + 76*s2)*t2 - 
                  (317 + 303*s2 + 13*Power(s2,2))*Power(t2,2) + 
                  (198 + 188*s2 + Power(s2,2))*Power(t2,3) - 
                  3*(46 + 11*s2)*Power(t2,4) + 19*Power(t2,5))) + 
            Power(s1,2)*t2*(41 - 3*(-43 + 62*s2)*t2 + 
               (-554 + 713*s2 + 76*Power(s2,2))*Power(t2,2) + 
               (574 - 789*s2 - 108*Power(s2,2))*Power(t2,3) + 
               (-23 + 455*s2 + 32*Power(s2,2))*Power(t2,4) - 
               (179 + 145*s2)*Power(t2,5) + 60*Power(t2,6) + 
               Power(t1,3)*t2*(3 - 40*t2 + 13*Power(t2,2)) + 
               Power(t1,2)*t2*
                (-112 + (521 + 60*s2)*t2 - 2*(229 + 6*s2)*Power(t2,2) + 
                  97*Power(t2,3)) + 
               t1*(-61 + 11*(35 + 8*s2)*t2 - 
                  (1125 + 538*s2 + 26*Power(s2,2))*Power(t2,2) + 
                  (1135 + 528*s2 + 2*Power(s2,2))*Power(t2,3) - 
                  2*(295 + 63*s2)*Power(t2,4) + 160*Power(t2,5))) + 
            Power(s1,3)*(Power(t1,3)*
                (1 + t2 + 12*Power(t2,2) - 2*Power(t2,3)) + 
               Power(t1,2)*(2 + t2 - 2*s2*t2 - 
                  (241 + 26*s2)*Power(t2,2) + (458 + 4*s2)*Power(t2,3) - 
                  136*Power(t2,4)) + 
               t1*(1 - (103 + 12*s2)*t2 + 
                  (766 + 271*s2 + 13*Power(s2,2))*Power(t2,2) - 
                  (1378 + 508*s2 + Power(s2,2))*Power(t2,3) + 
                  3*(345 + 43*s2)*Power(t2,4) - 345*Power(t2,5)) - 
               t2*(26 - 133*t2 + 253*Power(t2,2) + 77*Power(t2,3) - 
                  369*Power(t2,4) + 218*Power(t2,5) + 
                  Power(s2,2)*t2*(50 - 123*t2 + 37*Power(t2,2)) + 
                  s2*(-43 + 377*t2 - 738*Power(t2,2) + 601*Power(t2,3) - 
                     293*Power(t2,4))))) + 
         Power(-1 + s1,2)*s1*(s1 - t2)*(-1 + t2)*t2*
          (-5 + 15*t1 - 15*Power(t1,2) + 5*Power(t1,3) + 
            4*Power(s1,4)*Power(s2 - t1,2)*(-1 + t2) + 41*t2 - 11*s2*t2 - 
            92*t1*t2 + 22*s2*t1*t2 + 61*Power(t1,2)*t2 - 
            11*s2*Power(t1,2)*t2 - 10*Power(t1,3)*t2 - 64*Power(t2,2) + 
            63*s2*Power(t2,2) - 54*Power(s2,2)*Power(t2,2) + 
            4*Power(s2,3)*Power(t2,2) + 94*t1*Power(t2,2) + 
            15*s2*t1*Power(t2,2) + 14*Power(s2,2)*t1*Power(t2,2) - 
            83*Power(t1,2)*Power(t2,2) - 10*s2*Power(t1,2)*Power(t2,2) + 
            21*Power(t1,3)*Power(t2,2) + 28*Power(t2,3) - 
            93*s2*Power(t2,3) + 63*Power(s2,2)*Power(t2,3) - 
            12*Power(s2,3)*Power(t2,3) + 22*t1*Power(t2,3) - 
            6*s2*t1*Power(t2,3) + 6*Power(s2,2)*t1*Power(t2,3) + 
            Power(t1,2)*Power(t2,3) - 3*s2*Power(t1,2)*Power(t2,3) - 
            6*Power(t1,3)*Power(t2,3) - 3*Power(t2,4) + 
            41*s2*Power(t2,4) - 6*Power(s2,2)*Power(t2,4) - 
            2*Power(s2,3)*Power(t2,4) - 33*t1*Power(t2,4) - 
            37*s2*t1*Power(t2,4) + 10*Power(s2,2)*t1*Power(t2,4) + 
            36*Power(t1,2)*Power(t2,4) - 6*s2*Power(t1,2)*Power(t2,4) + 
            3*Power(t2,5) - 3*Power(s2,2)*Power(t2,5) - 
            6*t1*Power(t2,5) + 6*s2*t1*Power(t2,5) + 
            Power(s1,3)*(2*Power(t1,3)*(-6 + t2) - 4*Power(-1 + t2,2) + 
               17*t1*Power(-1 + t2,2) + Power(s2,3)*(4 + 6*t2) - 
               2*Power(t1,2)*(5 - 14*t2 + 9*Power(t2,2)) + 
               Power(s2,2)*(3 + 2*t2 - 5*Power(t2,2) - 10*t1*(2 + t2)) + 
               s2*(-9*Power(-1 + t2,2) + 2*Power(t1,2)*(14 + t2) + 
                  t1*(3 - 22*t2 + 19*Power(t2,2)))) + 
            Power(s1,2)*(Power(-1 + t2,2)*(1 + 7*t2) - 
               2*t1*Power(-1 + t2,2)*(25 + 16*t2) + 
               Power(t1,3)*(7 + 36*t2 - 13*Power(t2,2)) - 
               2*Power(s2,3)*(-2 + 10*t2 + 7*Power(t2,2)) + 
               2*Power(t1,2)*
                (5 - 23*t2 + 8*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,2)*(-54 + 57*t2 + 2*Power(t2,2) - 
                  5*Power(t2,3) + 2*t1*(7 + 23*t2 + 15*Power(t2,2))) - 
               s2*(-2*Power(-1 + t2,2)*(22 + 9*t2) + 
                  Power(t1,2)*(29 + 54*t2 + 7*Power(t2,2)) + 
                  t1*(-53 + 22*t2 + 23*Power(t2,2) + 8*Power(t2,3)))) + 
            s1*(2*Power(t1,3)*t2*(-14 - 4*t2 + 3*Power(t2,2)) + 
               2*Power(s2,3)*t2*(-4 + 14*t2 + 5*Power(t2,2)) - 
               Power(-1 + t2,2)*(31 - t2 + 6*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(62 + 80*t2 + 21*Power(t2,2)) + 
               Power(t1,2)*(-31 + 73*t2 + 25*Power(t2,2) - 
                  61*Power(t2,3) - 6*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-2*t1*(14 + 16*t2 + 15*Power(t2,2)) + 
                  3*(36 - 41*t2 + 2*Power(t2,2) + 3*Power(t2,3))) + 
               s2*(-(Power(-1 + t2,2)*(-11 + 85*t2 + 9*Power(t2,2))) + 
                  Power(t1,2)*
                   (11 + 39*t2 + 29*Power(t2,2) + 11*Power(t2,3)) + 
                  t1*(-22 - 68*t2 + 25*Power(t2,2) + 74*Power(t2,3) - 
                     9*Power(t2,4))))) + 
         Power(s,2)*(2*Power(-1 + t2,2)*Power(t2,3)*
             (11 - 14*t2 + 4*Power(t2,2)) + 
            Power(s1,7)*(-1 + t2)*
             (6 - 27*t2 + 16*Power(t2,2) + 5*Power(t2,3) + 
               t1*(3 - 14*t2 - 7*Power(t2,2)) + 
               2*s2*(-1 + 5*t2 + 5*Power(t2,2))) + 
            s1*Power(t2,2)*(4 + (419 - 154*s2)*t2 + 
               (-1119 + 411*s2 - 84*Power(s2,2) + 8*Power(s2,3))*
                Power(t2,2) + 
               (955 - 436*s2 + 158*Power(s2,2) - 42*Power(s2,3))*
                Power(t2,3) + 
               (-251 + 269*s2 - 64*Power(s2,2) + 8*Power(s2,3))*
                Power(t2,4) + 
               (-6 - 106*s2 - Power(s2,2) + 2*Power(s2,3))*Power(t2,5) + 
               (-2 + 16*s2 + 3*Power(s2,2))*Power(t2,6) + 
               Power(t1,3)*t2*
                (5 - 9*t2 + 22*Power(t2,2) - 6*Power(t2,3)) + 
               Power(t1,2)*(25 + (17 - 39*s2)*t2 + 
                  (-54 + 75*s2)*Power(t2,2) - 
                  (70 + 97*s2)*Power(t2,3) + (130 + 7*s2)*Power(t2,4) + 
                  6*(-6 + s2)*Power(t2,5)) + 
               t1*(-85 + 6*(-30 + 29*s2)*t2 + 
                  (760 - 458*s2 + 15*Power(s2,2))*Power(t2,2) + 
                  (-544 + 445*s2 + 39*Power(s2,2))*Power(t2,3) + 
                  (-51 - 236*s2 + 16*Power(s2,2))*Power(t2,4) + 
                  (106 + 57*s2 - 10*Power(s2,2))*Power(t2,5) - 
                  6*(1 + s2)*Power(t2,6))) - 
            Power(s1,2)*t2*(24 + (642 - 254*s2)*t2 + 
               (-1828 + 661*s2 - 180*Power(s2,2) + 18*Power(s2,3))*
                Power(t2,2) - 
               2*(-833 + 387*s2 - 216*Power(s2,2) + 56*Power(s2,3))*
                Power(t2,3) + 
               (-493 + 642*s2 - 290*Power(s2,2) + 30*Power(s2,3))*
                Power(t2,4) + 
               2*(-20 - 156*s2 + 25*Power(s2,2) + 8*Power(s2,3))*
                Power(t2,5) + 
               (15 + 53*s2 + 12*Power(s2,2))*Power(t2,6) - 
               2*(-7 + 8*s2)*Power(t2,7) + 
               Power(t1,3)*(-5 + 22*t2 - 59*Power(t2,2) + 
                  114*Power(t2,3) - 90*Power(t2,4) + 18*Power(t2,5)) + 
               Power(t1,2)*(33 + (9 - 60*s2)*t2 + 
                  (57 + 145*s2)*Power(t2,2) - 
                  (326 + 259*s2)*Power(t2,3) + 
                  (400 + 119*s2)*Power(t2,4) + 
                  (-143 + 7*s2)*Power(t2,5) - 6*Power(t2,6)) + 
               t1*(-98 + 3*(-113 + 92*s2)*t2 + 
                  (1209 - 809*s2 + 24*Power(s2,2))*Power(t2,2) + 
                  (-959 + 888*s2 + 124*Power(s2,2))*Power(t2,3) + 
                  (84 - 630*s2)*Power(t2,4) + 
                  (128 + 256*s2 - 52*Power(s2,2))*Power(t2,5) - 
                  (37 + 29*s2)*Power(t2,6) + 12*Power(t2,7))) + 
            Power(s1,3)*(-2 + (285 - 98*s2)*t2 + 
               (-845 + 247*s2 - 108*Power(s2,2) + 12*Power(s2,3))*
                Power(t2,2) + 
               (1109 - 336*s2 + 363*Power(s2,2) - 102*Power(s2,3))*
                Power(t2,3) + 
               (-769 + 407*s2 - 411*Power(s2,2) + 44*Power(s2,3))*
                Power(t2,4) + 
               (244 - 215*s2 + 131*Power(s2,2) + 46*Power(s2,3))*
                Power(t2,5) + (-83 + 68*s2 + Power(s2,2))*Power(t2,6) + 
               (56 - 73*s2)*Power(t2,7) + 5*Power(t2,8) + 
               Power(t1,3)*(1 + 21*t2 - 112*Power(t2,2) + 
                  235*Power(t2,3) - 289*Power(t2,4) + 72*Power(t2,5)) + 
               Power(t1,2)*(-4 - (34 + 23*s2)*t2 + 
                  99*(3 + s2)*Power(t2,2) - 
                  (575 + 303*s2)*Power(t2,3) + 
                  7*(84 + 55*s2)*Power(t2,4) - 
                  (303 + 14*s2)*Power(t2,5) + 7*Power(t2,6)) + 
               t1*(-7 + 3*(-33 + 34*s2)*t2 + 
                  3*(103 - 144*s2 + Power(s2,2))*Power(t2,2) + 
                  3*(-244 + 240*s2 + 57*Power(s2,2))*Power(t2,3) + 
                  (877 - 766*s2 - 145*Power(s2,2))*Power(t2,4) + 
                  (-358 + 482*s2 - 101*Power(s2,2))*Power(t2,5) - 
                  (51 + 58*s2)*Power(t2,6) + 61*Power(t2,7))) + 
            Power(s1,6)*(2*Power(s2,2)*t2*
                (-16 + 11*t2 + 17*Power(t2,2)) - 
               2*Power(t1,2)*
                (3 + 35*t2 - 54*Power(t2,2) + 4*Power(t2,3)) - 
               2*Power(-1 + t2,2)*
                (4 - 11*t2 + 44*Power(t2,2) + 12*Power(t2,3)) + 
               t1*(-14 - 15*t2 + 61*Power(t2,2) - 45*Power(t2,3) + 
                  13*Power(t2,4)) + 
               s2*(2 + 61*t2 - 107*Power(t2,2) + 47*Power(t2,3) - 
                  3*Power(t2,4) - 
                  4*t1*(-1 - 26*t2 + 32*Power(t2,2) + 7*Power(t2,3)))) + 
            Power(s1,4)*(-2*Power(s2,3)*t2*
                (1 - 18*t2 + 15*Power(t2,2) + 26*Power(t2,3)) + 
               Power(t1,3)*(2 + 29*t2 - 164*Power(t2,2) + 
                  313*Power(t2,3) - 84*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (8 - 72*t2 + 247*Power(t2,2) - 5*Power(t2,3) + 
                  119*Power(t2,4) + 24*Power(t2,5)) - 
               Power(t1,2)*(4 + 78*t2 - 344*Power(t2,2) + 
                  603*Power(t2,3) - 490*Power(t2,4) + 53*Power(t2,5)) + 
               t1*(-18 + 58*t2 + 370*Power(t2,2) - 785*Power(t2,3) + 
                  422*Power(t2,4) + 57*Power(t2,5) - 104*Power(t2,6)) + 
               2*Power(s2,2)*t2*
                (6 - 31*t2 + 88*Power(t2,2) - 43*Power(t2,3) + 
                  28*Power(t2,4) + 
                  t1*(3 - 63*t2 + 113*Power(t2,2) + 43*Power(t2,3))) + 
               s2*(-2 + 5*t2 - 104*Power(t2,2) + 229*Power(t2,3) - 
                  249*Power(t2,4) + 6*Power(t2,5) + 115*Power(t2,6) + 
                  Power(t1,2)*
                   (2 - 31*t2 + 209*Power(t2,2) - 441*Power(t2,3) + 
                     21*Power(t2,4)) + 
                  t1*t2*(77 - 344*t2 + 562*Power(t2,2) - 
                     512*Power(t2,3) + 25*Power(t2,4)))) + 
            Power(s1,5)*(4*Power(s2,3)*t2*(-1 + 2*t2 + 5*Power(t2,2)) + 
               3*Power(t1,3)*(-1 + 11*t2 - 26*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-8 + 49*t2 - 14*Power(t2,2) + 134*Power(t2,3) + 
                  38*Power(t2,4)) + 
               Power(t1,2)*(-6 - 31*t2 + 259*Power(t2,2) - 
                  360*Power(t2,3) + 54*Power(t2,4)) + 
               t1*(-14 - 51*t2 + 163*Power(t2,2) - 150*Power(t2,3) + 
                  3*Power(t2,4) + 49*Power(t2,5)) - 
               Power(s2,2)*t2*
                (27 - 41*t2 + 16*Power(t2,2) + 82*Power(t2,3) + 
                  t1*(-40 + 97*t2 + 27*Power(t2,2))) + 
               s2*(-2 + 100*t2 - 326*Power(t2,2) + 377*Power(t2,3) - 
                  84*Power(t2,4) - 65*Power(t2,5) + 
                  Power(t1,2)*
                   (2 - 68*t2 + 168*Power(t2,2) - 6*Power(t2,3)) + 
                  t1*(4 + 63*t2 - 294*Power(t2,2) + 357*Power(t2,3) + 
                     38*Power(t2,4))))) - 
         s*(2*(-2 + t2)*Power(-1 + t2,3)*Power(t2,3) + 
            Power(s1,8)*Power(-1 + t2,2)*
             (-2 + s2 - t1 + 2*t2 + 5*s2*t2 - 5*t1*t2) + 
            s1*Power(t2,2)*(-15 + (206 - 59*s2)*t2 + 
               (-462 + 215*s2 - 129*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,2) + 
               (340 - 331*s2 + 255*Power(s2,2) - 45*Power(s2,3))*
                Power(t2,3) + 
               (-51 + 261*s2 - 135*Power(s2,2) + 19*Power(s2,3))*
                Power(t2,4) + 
               (-10 - 94*s2 + 3*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,5) + (-8 + 8*s2 + 6*Power(s2,2))*Power(t2,6) + 
               Power(t1,3)*(10 - 25*t2 + 53*Power(t2,2) - 
                  32*Power(t2,3) + 6*Power(t2,4)) + 
               Power(t1,2)*(-25 - 5*(-32 + 7*s2)*t2 + 
                  (-299 + 22*s2)*Power(t2,2) + 
                  (128 - 34*s2)*Power(t2,3) - (-108 + s2)*Power(t2,4) + 
                  12*(-6 + s2)*Power(t2,5)) + 
               t1*(18 + (-297 + 94*s2)*t2 + 
                  5*(129 - 30*s2 + 7*Power(s2,2))*Power(t2,2) + 
                  (-375 + 123*s2 + 7*Power(s2,2))*Power(t2,3) + 
                  (-81 - 148*s2 + 14*Power(s2,2))*Power(t2,4) + 
                  (84 + 93*s2 - 20*Power(s2,2))*Power(t2,5) + 
                  (6 - 12*s2)*Power(t2,6))) - 
            Power(s1,2)*t2*(-6 + (322 - 109*s2)*t2 + 
               (-639 + 343*s2 - 330*Power(s2,2) + 26*Power(s2,3))*
                Power(t2,2) - 
               3*(-70 + 200*s2 - 229*Power(s2,2) + 43*Power(s2,3))*
                Power(t2,3) + 
               (172 + 697*s2 - 458*Power(s2,2) + 39*Power(s2,3))*
                Power(t2,4) + 
               (10 - 405*s2 + 91*Power(s2,2) + 8*Power(s2,3))*
                Power(t2,5) + 
               (-71 + 58*s2 + 16*Power(s2,2) - 4*Power(s2,3))*
                Power(t2,6) + (2 + 16*s2 - 6*Power(s2,2))*Power(t2,7) + 
               Power(t1,3)*(5 - 18*t2 + 114*Power(t2,2) - 
                  87*Power(t2,3) + 58*Power(t2,4) - 12*Power(t2,5)) - 
               Power(t1,2)*(8 + (-214 + 63*s2)*t2 + 
                  13*(41 + s2)*Power(t2,2) + 
                  (-315 + 74*s2)*Power(t2,3) + 
                  (-135 + 68*s2)*Power(t2,4) + 
                  (183 - 50*s2)*Power(t2,5) + 12*(-5 + s2)*Power(t2,6)) + 
               t1*(-3 + 2*(-235 + 86*s2)*t2 + 
                  (1015 - 135*s2 + 88*Power(s2,2))*Power(t2,2) + 
                  (-259 - 104*s2 + 61*Power(s2,2))*Power(t2,3) + 
                  (-641 + 3*s2 + 87*Power(s2,2))*Power(t2,4) + 
                  (343 + 154*s2 - 76*Power(s2,2))*Power(t2,5) + 
                  (39 - 102*s2 + 20*Power(s2,2))*Power(t2,6) + 
                  12*(-2 + s2)*Power(t2,7))) - 
            Power(s1,3)*(1 + 7*(-22 + 7*s2)*t2 + 
               (91 - 75*s2 + 273*Power(s2,2) - 22*Power(s2,3))*
                Power(t2,2) + 
               (257 + 461*s2 - 711*Power(s2,2) + 133*Power(s2,3))*
                Power(t2,3) + 
               (80 - 1191*s2 + 794*Power(s2,2) - 35*Power(s2,3))*
                Power(t2,4) + 
               (-517 + 988*s2 - 364*Power(s2,2) + 17*Power(s2,3))*
                Power(t2,5) + 
               (230 - 174*s2 - 15*Power(s2,2) + 27*Power(s2,3))*
                Power(t2,6) + (10 - 50*s2 + 23*Power(s2,2))*Power(t2,7) + 
               (2 - 8*s2)*Power(t2,8) + 
               Power(t1,3)*(-1 + 14*t2 - 98*Power(t2,2) + 
                  80*Power(t2,3) - 105*Power(t2,4) - 28*Power(t2,5) + 
                  18*Power(t2,6)) + 
               Power(t1,2)*(1 + (-126 + 29*s2)*t2 + 
                  (347 + 62*s2)*Power(t2,2) + 
                  (-389 + 55*s2)*Power(t2,3) + 
                  (218 + 167*s2)*Power(t2,4) + 
                  (115 + 18*s2)*Power(t2,5) + 
                  (-154 + 29*s2)*Power(t2,6) - 12*Power(t2,7)) + 
               t1*(3 + (246 - 78*s2)*t2 - 
                  (377 + 118*s2 + 71*Power(s2,2))*Power(t2,2) + 
                  (-312 + 579*s2 - 93*Power(s2,2))*Power(t2,3) + 
                  (699 - 570*s2 - 169*Power(s2,2))*Power(t2,4) + 
                  (-124 - 92*s2 + 59*Power(s2,2))*Power(t2,5) + 
                  (-207 + 316*s2 - 86*Power(s2,2))*Power(t2,6) + 
                  (66 - 37*s2)*Power(t2,7) + 6*Power(t2,8))) + 
            Power(s1,7)*(-1 + t2)*
             (2*Power(s2,2)*t2*(3 + 11*t2) - 
               Power(-1 + t2,2)*(1 + 15*t2) + 
               2*Power(t1,2)*(1 + 9*t2 + 4*Power(t2,2)) + 
               t1*(3 + 8*t2 - 31*Power(t2,2) + 20*Power(t2,3)) - 
               s2*(-2 + 11*t2 - 22*Power(t2,2) + 13*Power(t2,3) + 
                  t1*(2 + 24*t2 + 30*Power(t2,2)))) + 
            Power(s1,6)*(2 + (-46 + 28*s2 + 28*Power(s2,2) - 
                  8*Power(s2,3))*t2 + 
               (95 - 110*s2 + 5*Power(s2,2) + Power(s2,3))*Power(t2,2) + 
               (-33 + 137*s2 + 24*Power(s2,2) + 19*Power(s2,3))*
                Power(t2,3) - (45 + 56*s2 + 57*Power(s2,2))*Power(t2,4) + 
               (27 + s2)*Power(t2,5) + 
               Power(t1,3)*(-1 + 35*t2 - 54*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(2 + 58*t2 - 42*Power(t2,2) + 
                  20*Power(t2,3) - 38*Power(t2,4) + 
                  s2*(1 - 77*t2 + 110*Power(t2,2) + 2*Power(t2,3))) + 
               t1*(Power(s2,2)*t2*(50 - 57*t2 - 29*Power(t2,2)) - 
                  Power(-1 + t2,2)*
                   (-4 - 13*t2 - 32*Power(t2,2) + 16*Power(t2,3)) + 
                  s2*(-4 - 73*t2 + 6*Power(t2,2) - 13*Power(t2,3) + 
                     84*Power(t2,4)))) + 
            Power(s1,4)*(Power(-1 + t2,2)*t2*
                (-134 - 283*t2 + 288*Power(t2,2) + 66*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s2,3)*t2*
                (-6 + 59*t2 - 37*Power(t2,2) + 43*Power(t2,3) + 
                  61*Power(t2,4)) + 
               Power(t1,2)*t2*
                (22 - 206*t2 + 335*Power(t2,2) + 35*Power(t2,3) - 
                  141*Power(t2,4) - 45*Power(t2,5)) + 
               t1*Power(-1 + t2,2)*
                (1 + 83*t2 - 24*Power(t2,2) - 110*Power(t2,3) + 
                  89*Power(t2,4) + 21*Power(t2,5)) + 
               Power(t1,3)*(-3 - 10*t2 + 32*Power(t2,2) - 
                  14*Power(t2,3) - 187*Power(t2,4) + 62*Power(t2,5)) + 
               Power(s2,2)*t2*
                (72 - 381*t2 + 782*Power(t2,2) - 474*Power(t2,3) - 
                  14*Power(t2,4) + 15*Power(t2,5) - 
                  t1*(18 + 31*t2 + 83*Power(t2,2) + 87*Power(t2,3) + 
                     141*Power(t2,4))) + 
               s2*(-(t1*t2*(105 - 446*t2 + 672*Power(t2,2) + 
                       28*Power(t2,3) - 373*Power(t2,4) + 14*Power(t2,5))\
) + Power(t1,2)*(1 + 29*t2 - 23*Power(t2,2) + 89*Power(t2,3) + 
                     240*Power(t2,4) + 24*Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (1 - 49*t2 - 497*Power(t2,2) + 334*Power(t2,3) + 
                     117*Power(t2,4) + 26*Power(t2,5)))) + 
            Power(s1,5)*(-(Power(-1 + t2,2)*t2*
                  (-150 + 44*t2 + 71*Power(t2,2) + 19*Power(t2,3))) - 
               Power(s2,3)*t2*
                (10 - 30*t2 + 23*Power(t2,2) + 57*Power(t2,3)) + 
               Power(t1,3)*(3 - 6*t2 - 104*Power(t2,2) + 
                  225*Power(t2,3) - 58*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (-2 + 7*t2 - 10*Power(t2,2) + 55*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(t1,2)*(1 + 2*t2 - 56*Power(t2,2) - 45*Power(t2,3) + 
                  35*Power(t2,4) + 63*Power(t2,5)) + 
               Power(s2,2)*t2*
                (102 - 339*t2 + 199*Power(t2,2) + Power(t2,3) + 
                  37*Power(t2,4) + 
                  t1*(-8 - 63*t2 + 147*Power(t2,2) + 104*Power(t2,3))) - 
               s2*(Power(t1,2)*
                   (2 - 37*t2 - 88*Power(t2,2) + 294*Power(t2,3) + 
                     9*Power(t2,4)) - 
                  Power(-1 + t2,2)*
                   (2 - 202*t2 + 92*Power(t2,2) + 111*Power(t2,3) + 
                     25*Power(t2,4)) + 
                  t1*(-2 + 90*t2 - 324*Power(t2,2) + 31*Power(t2,3) + 
                     140*Power(t2,4) + 65*Power(t2,5)))))))/
     (s*(-1 + s1)*s1*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       (-1 + s2)*(-s + s2 - t1)*Power(s1 - t2,2)*Power(-1 + t2,2)*t2*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) + 
    (8*(-2*Power(s,7)*Power(t2,2)*((-5 + t2)*t2 + t1*(1 + t2)) + 
         (s1 - t2)*(-1 + t2)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,
           2)*(-3 + 3*t1 + 2*Power(s1,3)*(-1 + t2) + 18*t2 - s2*t2 - 
            7*t1*t2 - 15*Power(t2,2) + 7*s2*Power(t2,2) - 
            2*t1*Power(t2,2) + 
            Power(s1,2)*(t1*(-7 + t2) - 2*(-1 + t2)*t2 + 
               3*s2*(1 + t2)) + 
            s1*(s2*(1 - 10*t2 - 3*Power(t2,2)) - 
               3*(5 - 6*t2 + Power(t2,2)) + 2*t1*(2 + 3*t2 + Power(t2,2))\
)) - 2*Power(s,6)*t2*(Power(t1,2)*(-1 + t2) + 
            t2*(1 + 6*(5 + s2)*t2 - 2*(9 + s2)*Power(t2,2) + 
               Power(t2,3)) + 
            t1*t2*(-2 - 5*t2 + 5*Power(t2,2) - s2*(1 + t2)) + 
            s1*(t2*(-16 + s2 + 23*t2 + s2*t2 - 3*Power(t2,2)) - 
               2*t1*(-1 + t2 + 2*Power(t2,2)))) - 
         Power(s,5)*(Power(t1,3)*Power(-1 + t2,2) + 
            Power(t1,2)*t2*(6 + s2 - 13*t2 - s2*t2 - 4*Power(t2,2) + 
               3*Power(t2,3)) + 
            t1*t2*(-4 + (-13 + 9*s2)*t2 + 6*(5 + 3*s2)*Power(t2,2) - 
               (41 + 11*s2)*Power(t2,3) + 8*Power(t2,4)) + 
            Power(t2,2)*(-6 - 136*t2 + 2*Power(s2,2)*(-3 + t2)*t2 + 
               133*Power(t2,2) - 32*Power(t2,3) + Power(t2,4) + 
               s2*(2 - 58*t2 + 55*Power(t2,2) - 3*Power(t2,3))) + 
            s1*(Power(t1,2)*(-2 + 15*t2 - 9*Power(t2,2)) + 
               t2*(4 - 2*(-74 - 17*s2 + Power(s2,2))*t2 - 
                  2*(133 + 26*s2 + Power(s2,2))*Power(t2,2) + 
                  (107 + 10*s2)*Power(t2,3) - 5*Power(t2,4)) + 
               t1*(-1 + t2)*t2*((27 - 29*t2)*t2 + 4*s2*(2 + t2))) + 
            2*Power(s1,2)*(t1*(1 - 5*t2 + 6*Power(t2,3)) + 
               t2*(-17 + 51*t2 - 37*Power(t2,2) + 3*Power(t2,3) - 
                  s2*(-2 + t2 + 3*Power(t2,2))))) + 
         Power(s,4)*(-(Power(t1,3)*
               (-2 + 10*t2 - 9*Power(t2,2) + Power(t2,4))) + 
            Power(t1,2)*(-3 + t2 + 5*s2*t2 - 7*(2 + s2)*Power(t2,2) - 
               (17 + 2*s2)*Power(t2,3) + (27 + 2*s2)*Power(t2,4)) + 
            t2*(2 + 2*(3 - s2 + Power(s2,2))*t2 - 
               (149 + 106*s2 + 30*Power(s2,2))*Power(t2,2) + 
               2*(98 + 69*s2 + 14*Power(s2,2))*Power(t2,3) - 
               4*(24 + 11*s2)*Power(t2,4) + (21 + 2*s2)*Power(t2,5)) + 
            2*Power(s1,3)*(-1 + t2)*
             (-6 + 29*t2 - 24*Power(t2,2) + Power(t2,3) + 
               s2*(1 - 2*t2 - 3*Power(t2,2)) + 
               2*t1*(-1 + t2 + 2*Power(t2,2))) + 
            t1*t2*(-11 - 26*t2 + 36*Power(t2,2) - 22*Power(t2,3) + 
               8*Power(t2,4) - 3*Power(t2,5) + 
               Power(s2,2)*(t2 + 4*Power(t2,2) - Power(t2,3)) + 
               s2*(1 + 5*t2 + 58*Power(t2,2) - 72*Power(t2,3) + 
                  6*Power(t2,4))) - 
            Power(s1,2)*(2 + (114 + 40*s2 - 6*Power(s2,2))*t2 - 
               (373 + 93*s2)*Power(t2,2) + 
               2*(182 + 29*s2 + Power(s2,2))*Power(t2,3) - 
               (109 + 9*s2)*Power(t2,4) + 4*Power(t2,5) + 
               Power(t1,2)*(11 - 34*t2 + 17*Power(t2,2)) + 
               t1*(4 - 54*t2 + 107*Power(t2,2) - 76*Power(t2,3) + 
                  27*Power(t2,4) - 2*s2*(3 - 14*t2 + 5*Power(t2,2)))) + 
            s1*(Power(t1,3)*(9 - 14*t2 + 5*Power(t2,2)) + 
               t2*(4 + (245 + 173*s2 + 7*Power(s2,2))*t2 - 
                  (513 + 254*s2 + 28*Power(s2,2))*Power(t2,2) + 
                  (336 + 111*s2 + Power(s2,2))*Power(t2,3) - 
                  2*(38 + 3*s2)*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(4 + (23 - 19*s2 - 2*Power(s2,2))*t2 - 
                  (157 + 9*s2)*Power(t2,2) - 
                  2*(-86 - 36*s2 + Power(s2,2))*Power(t2,3) - 
                  2*(40 + 7*s2)*Power(t2,4) + 16*Power(t2,5)) - 
               Power(t1,2)*(1 - 29*t2 + 15*Power(t2,2) + 
                  29*Power(t2,3) - 8*Power(t2,4) + 
                  s2*(4 - 11*t2 + 5*Power(t2,2))))) + 
         Power(s,3)*(Power(t1,3)*t2*
             (9 - 20*t2 + 4*Power(t2,2) + 5*Power(t2,3)) + 
            Power(t1,2)*(3 + (3 - 9*s2)*t2 + 
               (-37 + 25*s2)*Power(t2,2) + (42 + 4*s2)*Power(t2,3) - 
               (51 + 13*s2)*Power(t2,4) - (-10 + s2)*Power(t2,5)) + 
            t1*(-3 + (-9 + 8*s2)*t2 + 
               (15 + 14*s2 - 3*Power(s2,2))*Power(t2,2) + 
               (67 - 99*s2 - 14*Power(s2,2))*Power(t2,3) + 
               (-168 + 137*s2 + 15*Power(s2,2))*Power(t2,4) + 
               (63 - 31*s2 + 2*Power(s2,2))*Power(t2,5) + 
               (-7 + 3*s2)*Power(t2,6)) + 
            2*Power(s1,4)*Power(-1 + t2,2)*
             (6*(-1 + t2) + s2*(1 + t2) - t1*(1 + t2)) + 
            t2*(-5 - 39*t2 + 109*Power(t2,2) - 165*Power(t2,3) + 
               148*Power(t2,4) - 60*Power(t2,5) + 12*Power(t2,6) - 
               Power(s2,3)*Power(t2,2)*(2 + t2 + Power(t2,2)) - 
               Power(s2,2)*t2*
                (8 - 78*t2 + 84*Power(t2,2) - 13*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(2 + 30*t2 + 23*Power(t2,2) - 51*Power(t2,3) + 
                  62*Power(t2,4) - 24*Power(t2,5))) + 
            Power(s1,3)*(Power(t1,2)*(16 - 31*t2 + 15*Power(t2,2)) + 
               Power(s2,2)*(4 - 9*t2 + 3*Power(t2,2) - 2*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-26 + 97*t2 - 40*Power(t2,2) + Power(t2,3)) + 
               t1*(29 - 92*t2 + 76*Power(t2,2) - 16*Power(t2,3) + 
                  7*Power(t2,4)) + 
               2*s2*(-9 + 32*t2 - 29*Power(t2,2) + 6*Power(t2,3) - 
                  2*Power(t2,4) + 
                  t1*(-9 + 19*t2 - 10*Power(t2,2) + 2*Power(t2,3)))) - 
            Power(s1,2)*(2 - 125*t2 + 419*Power(t2,2) - 
               511*Power(t2,3) + 274*Power(t2,4) - 60*Power(t2,5) + 
               Power(t2,6) + Power(s2,3)*t2*(1 + t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(29 - 37*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(10 - 21*t2 + 61*Power(t2,2) - 
                  46*Power(t2,3) + 6*Power(t2,4)) + 
               t1*(-5 + 170*t2 - 301*Power(t2,2) + 139*Power(t2,3) - 
                  5*Power(t2,4) + 8*Power(t2,5)) - 
               s2*(2 + 159*t2 - 342*Power(t2,2) + 237*Power(t2,3) - 
                  55*Power(t2,4) + 5*Power(t2,5) + 
                  Power(t1,2)*(27 - 36*t2 + 11*Power(t2,2)) + 
                  t1*t2*(5 + 48*t2 - 39*Power(t2,2) - 6*Power(t2,3))) - 
               Power(s2,2)*(t1*
                   (-5 + 10*t2 - 5*Power(t2,2) + 4*Power(t2,3)) + 
                  t2*(1 - 17*t2 + 14*Power(t2,2) + 4*Power(t2,3)))) + 
            s1*(2 + (23 - 15*s2 + 5*Power(s2,2))*t2 + 
               (-185 - 213*s2 - 76*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,2) + 
               (421 + 362*s2 + 105*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) + 
               (-410 - 244*s2 - 29*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,4) + (192 + 69*s2 - Power(s2,2))*Power(t2,5) - 
               (43 + 3*s2)*Power(t2,6) + 
               Power(t1,3)*(-11 + 50*t2 - 34*Power(t2,2) - 
                  4*Power(t2,3) + 3*Power(t2,4)) + 
               Power(t1,2)*(12 + 40*t2 - 103*Power(t2,2) + 
                  150*Power(t2,3) - 58*Power(t2,4) - Power(t2,5) + 
                  s2*(4 - 44*t2 + 22*Power(t2,2) + 14*Power(t2,3) - 
                     4*Power(t2,4))) - 
               t1*(1 - 4*t2 - 68*Power(t2,2) + 11*Power(t2,3) + 
                  43*Power(t2,4) - 23*Power(t2,5) - 4*Power(t2,6) + 
                  Power(s2,2)*t2*
                   (-11 + 3*t2 + 5*Power(t2,2) + 7*Power(t2,3)) + 
                  s2*(5 + 39*t2 - 171*Power(t2,2) + 270*Power(t2,3) - 
                     100*Power(t2,4) + Power(t2,5))))) + 
         Power(s,2)*(-1 + 3*Power(t1,2) - 2*Power(t1,3) - 9*t2 + 
            6*s2*t2 + 24*t1*t2 - 13*s2*t1*t2 - 22*Power(t1,2)*t2 + 
            7*s2*Power(t1,2)*t2 + 7*Power(t1,3)*t2 + 45*Power(t2,2) - 
            14*s2*Power(t2,2) - Power(s2,2)*Power(t2,2) - 
            108*t1*Power(t2,2) + 40*s2*t1*Power(t2,2) + 
            3*Power(s2,2)*t1*Power(t2,2) + 82*Power(t1,2)*Power(t2,2) - 
            40*s2*Power(t1,2)*Power(t2,2) + Power(t1,3)*Power(t2,2) - 
            124*Power(t2,3) + 214*s2*Power(t2,3) - 
            124*Power(s2,2)*Power(t2,3) + 6*Power(s2,3)*Power(t2,3) - 
            122*t1*Power(t2,3) + 41*s2*t1*Power(t2,3) + 
            20*Power(s2,2)*t1*Power(t2,3) - 79*Power(t1,2)*Power(t2,3) + 
            25*s2*Power(t1,2)*Power(t2,3) - 7*Power(t1,3)*Power(t2,3) + 
            201*Power(t2,4) - 255*s2*Power(t2,4) + 
            138*Power(s2,2)*Power(t2,4) - 3*Power(s2,3)*Power(t2,4) + 
            335*t1*Power(t2,4) - 57*s2*t1*Power(t2,4) - 
            41*Power(s2,2)*t1*Power(t2,4) + 8*Power(t1,2)*Power(t2,4) + 
            21*s2*Power(t1,2)*Power(t2,4) - 3*Power(t1,3)*Power(t2,4) - 
            181*Power(t2,5) + 48*s2*Power(t2,5) - 
            41*Power(s2,2)*Power(t2,5) + 3*Power(s2,3)*Power(t2,5) - 
            158*t1*Power(t2,5) + 42*s2*t1*Power(t2,5) + 
            2*Power(s2,2)*t1*Power(t2,5) - 20*Power(t1,2)*Power(t2,5) + 
            s2*Power(t1,2)*Power(t2,5) + 95*Power(t2,6) + 
            9*s2*Power(t2,6) + 4*Power(s2,2)*Power(t2,6) + 
            33*t1*Power(t2,6) - 5*s2*t1*Power(t2,6) + 
            4*Power(t1,2)*Power(t2,6) - 30*Power(t2,7) - 
            8*s2*Power(t2,7) - 4*t1*Power(t2,7) + 4*Power(t2,8) + 
            Power(s1,4)*(Power(t1,2)*(-5 + 6*t2 - 3*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(-13 - 20*t2 + Power(t2,2)) + 
               2*Power(-1 + t2,2)*(5 - 6*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-5 + 8*t2 - 7*Power(t2,2) + 
                  2*Power(t2,3)) + 
               s2*(Power(-1 + t2,2)*(11 + 8*t2 + Power(t2,2)) - 
                  2*t1*(-6 + 9*t2 - 6*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,3)*(2*Power(t1,3)*(21 - 24*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(57 - 124*t2 + 102*Power(t2,2) - 
                  25*Power(t2,3)) + 
               Power(s2,3)*(-2 + t2 - 3*Power(t2,2) + 2*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-13 + 54*t2 - 49*Power(t2,2) + 16*Power(t2,3)) + 
               t1*(-33 + 127*t2 - 60*Power(t2,2) - 91*Power(t2,3) + 
                  57*Power(t2,4)) + 
               Power(s2,2)*(5 + 15*t2 - 15*Power(t2,2) + 
                  8*Power(t2,3) - 3*Power(t2,4) + 
                  t1*(23 - 20*t2 + 7*Power(t2,2) - 2*Power(t2,3))) + 
               s2*(39 - 145*t2 + 132*Power(t2,2) - 25*Power(t2,3) + 
                  Power(t2,4) - 2*Power(t2,5) + 
                  Power(t1,2)*(-59 + 59*t2 - 10*Power(t2,2)) + 
                  t1*(-35 + 21*t2 + 15*Power(t2,2) - 31*Power(t2,3) + 
                     10*Power(t2,4)))) + 
            Power(s1,2)*(Power(s2,3)*t2*
                (10 - 5*t2 + 9*Power(t2,2) - 4*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (-10 + 28*t2 - 67*Power(t2,2) + 22*Power(t2,3)) + 
               Power(t1,3)*(10 - 73*t2 + 45*Power(t2,2) + 
                  9*Power(t2,3) - 3*Power(t2,4)) + 
               Power(t1,2)*(4 - 238*t2 + 375*Power(t2,2) - 
                  213*Power(t2,3) + 32*Power(t2,4) + 2*Power(t2,5)) - 
               t1*(4 + 121*t2 - 214*Power(t2,2) + 222*Power(t2,3) - 
                  187*Power(t2,4) + 53*Power(t2,5) + Power(t2,6)) + 
               Power(s2,2)*(t2*
                   (-108 + 87*t2 - 21*Power(t2,2) + 4*Power(t2,3)) + 
                  t1*(2 - 52*t2 + 35*Power(t2,2) - 26*Power(t2,3) + 
                     9*Power(t2,4))) + 
               s2*(-3 - 44*t2 + 233*Power(t2,2) - 237*Power(t2,3) + 
                  85*Power(t2,4) - 35*Power(t2,5) + Power(t2,6) + 
                  Power(t1,2)*
                   (-5 + 88*t2 - 38*Power(t2,2) - 13*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  t1*(-6 + 346*t2 - 425*Power(t2,2) + 163*Power(t2,3) + 
                     5*Power(t2,4) - 7*Power(t2,5)))) + 
            s1*(Power(s2,3)*Power(t2,2)*
                (-14 + 7*t2 - 9*Power(t2,2) + 2*Power(t2,3)) - 
               2*Power(t1,3)*
                (1 + 7*t2 - 20*Power(t2,2) + Power(t2,3) + 
                  5*Power(t2,4)) - 
               Power(-1 + t2,2)*t2*
                (31 - 79*t2 + 100*Power(t2,2) - 76*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(t1,2)*(3 - 56*t2 + 228*Power(t2,2) - 
                  215*Power(t2,3) + 102*Power(t2,4) - 8*Power(t2,5)) + 
               t1*(-1 + 61*t2 + 334*Power(t2,2) - 717*Power(t2,3) + 
                  453*Power(t2,4) - 148*Power(t2,5) + 18*Power(t2,6)) + 
               Power(s2,2)*t2*
                (1 + 227*t2 - 235*Power(t2,2) + 69*Power(t2,3) - 
                  9*Power(t2,4) + Power(t2,5) + 
                  t1*(-5 + 9*t2 + 26*Power(t2,2) + 17*Power(t2,3) - 
                     7*Power(t2,4))) + 
               s2*(-1 + 4*t2 - 199*Power(t2,2) + 158*Power(t2,3) + 
                  64*Power(t2,4) - 62*Power(t2,5) + 36*Power(t2,6) + 
                  Power(t1,2)*
                   (-2 + 32*t2 - 44*Power(t2,2) - 40*Power(t2,3) + 
                     15*Power(t2,4) + Power(t2,5)) - 
                  t1*(-3 + 8*t2 + 372*Power(t2,2) - 445*Power(t2,3) + 
                     188*Power(t2,4) - 13*Power(t2,5) + Power(t2,6))))) + 
         s*(-1 + 3*t1 - 3*Power(t1,2) + Power(t1,3) + 
            4*Power(s1,5)*(s2*(1 + t1) - t1*(3 + t1 - 2*t2))*
             Power(-1 + t2,2) + 9*t2 - 2*s2*t2 - 29*t1*t2 + 4*s2*t1*t2 + 
            31*Power(t1,2)*t2 - 2*s2*Power(t1,2)*t2 - 11*Power(t1,3)*t2 - 
            50*Power(t2,2) + 20*s2*Power(t2,2) + 
            Power(s2,2)*Power(t2,2) + 136*t1*Power(t2,2) - 
            48*s2*t1*Power(t2,2) - Power(s2,2)*t1*Power(t2,2) - 
            107*Power(t1,2)*Power(t2,2) + 28*s2*Power(t1,2)*Power(t2,2) + 
            21*Power(t1,3)*Power(t2,2) + 146*Power(t2,3) - 
            201*s2*Power(t2,3) + 49*Power(s2,2)*Power(t2,3) - 
            2*Power(s2,3)*Power(t2,3) - 115*t1*Power(t2,3) + 
            164*s2*t1*Power(t2,3) - 21*Power(s2,2)*t1*Power(t2,3) + 
            75*Power(t1,2)*Power(t2,3) - 49*s2*Power(t1,2)*Power(t2,3) - 
            6*Power(t1,3)*Power(t2,3) - 219*Power(t2,4) + 
            369*s2*Power(t2,4) - 149*Power(s2,2)*Power(t2,4) + 
            12*Power(s2,3)*Power(t2,4) - 92*t1*Power(t2,4) - 
            106*s2*t1*Power(t2,4) + 37*Power(s2,2)*t1*Power(t2,4) + 
            8*Power(t1,2)*Power(t2,4) + 19*s2*Power(t1,2)*Power(t2,4) - 
            7*Power(t1,3)*Power(t2,4) + 173*Power(t2,5) - 
            207*s2*Power(t2,5) + 99*Power(s2,2)*Power(t2,5) - 
            9*Power(s2,3)*Power(t2,5) + 116*t1*Power(t2,5) - 
            10*s2*t1*Power(t2,5) - 22*Power(s2,2)*t1*Power(t2,5) + 
            2*Power(t1,2)*Power(t2,5) + 10*s2*Power(t1,2)*Power(t2,5) - 
            76*Power(t2,6) + 21*s2*Power(t2,6) + 
            Power(s2,3)*Power(t2,6) - 23*t1*Power(t2,6) - 
            6*s2*t1*Power(t2,6) + Power(s2,2)*t1*Power(t2,6) - 
            6*Power(t1,2)*Power(t2,6) + 24*Power(t2,7) + 
            2*s2*Power(t2,7) + 4*t1*Power(t2,7) + 2*s2*t1*Power(t2,7) - 
            6*Power(t2,8) - 2*s2*Power(t2,8) + 
            Power(s1,4)*(Power(s2,3)*(5 - 3*t2) - 
               4*Power(-1 + t2,3)*(-1 + 2*t2) + 
               Power(t1,3)*(-28 + 31*t2 - 5*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(-9 - 46*t2 + 23*Power(t2,2)) + 
               Power(t1,2)*(-42 + 76*t2 - 43*Power(t2,2) + 
                  8*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(1 - 12*t2 + 3*Power(t2,2) + 8*Power(t2,3) + 
                  3*t1*(-10 + 7*t2 + Power(t2,2))) + 
               s2*(-(Power(-1 + t2,2)*(9 + 22*t2 + Power(t2,2))) + 
                  Power(t1,2)*(53 - 49*t2 + 2*Power(t2,2)) - 
                  t1*(-21 + 4*t2 + 20*Power(t2,2) - 4*Power(t2,3) + 
                     Power(t2,4)))) - 
            Power(s1,3)*(-3*Power(1 - 3*t2,2)*Power(-1 + t2,3) + 
               Power(s2,3)*(-2 + 27*t2 - 18*Power(t2,2) + Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (-77 + 50*t2 - 75*Power(t2,2) + 22*Power(t2,3)) - 
               Power(t1,3)*(17 + 26*t2 - 30*Power(t2,2) - 
                  6*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(37 - 248*t2 + 288*Power(t2,2) - 
                  81*Power(t2,3) + 3*Power(t2,4) + Power(t2,5)) + 
               Power(s2,2)*(35 - 99*t2 + 14*Power(t2,2) + 
                  27*Power(t2,3) + 23*Power(t2,4) + 
                  t1*(-7 - 100*t2 + 90*Power(t2,2) - 8*Power(t2,3) + 
                     Power(t2,4))) - 
               s2*(Power(-1 + t2,2)*
                   (25 + 26*t2 + 23*Power(t2,2) + 6*Power(t2,3)) + 
                  Power(t1,2)*
                   (-34 - 75*t2 + 78*Power(t2,2) + 7*Power(t2,3)) + 
                  t1*(95 - 378*t2 + 256*Power(t2,2) + 40*Power(t2,3) - 
                     15*Power(t2,4) + 2*Power(t2,5)))) + 
            Power(s1,2)*(3*Power(s2,3)*t2*
                (-2 + 17*t2 - 12*Power(t2,2) + Power(t2,3)) - 
               Power(-1 + t2,3)*
                (-2 + 34*t2 - 37*Power(t2,2) + 35*Power(t2,3)) + 
               Power(t1,3)*(3 - 28*t2 + 6*Power(t2,3) + 7*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (-22 - 319*t2 + 168*Power(t2,2) - 54*Power(t2,3) + 
                  9*Power(t2,4)) - 
               Power(t1,2)*(23 - 13*t2 + 196*Power(t2,2) - 
                  245*Power(t2,3) + 37*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,2)*(1 + 119*t2 - 350*Power(t2,2) + 
                  163*Power(t2,3) + 45*Power(t2,4) + 22*Power(t2,5) + 
                  t1*(-1 - 35*t2 - 73*Power(t2,2) + 95*Power(t2,3) - 
                     24*Power(t2,4) + 2*Power(t2,5))) - 
               s2*(Power(t1,2)*
                   (-3 - 69*t2 + 10*Power(t2,2) + 19*Power(t2,3) + 
                     7*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (5 + 211*t2 - 7*Power(t2,2) - 2*Power(t2,3) + 
                     11*Power(t2,4)) + 
                  t1*(-2 + 126*t2 - 627*Power(t2,2) + 486*Power(t2,3) + 
                     20*Power(t2,4) - 4*Power(t2,5) + Power(t2,6)))) - 
            s1*(Power(s2,3)*Power(t2,2)*
                (-6 + 41*t2 - 30*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,3)*(-7 + 18*t2 - 14*Power(t2,2) - 
                  6*Power(t2,3) + Power(t2,4)) - 
               Power(-1 + t2,3)*
                (5 - 31*t2 + 72*Power(t2,2) - 30*Power(t2,3) + 
                  22*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (17 - 106*t2 - 321*Power(t2,2) + 118*Power(t2,3) - 
                  14*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,2)*(19 - 112*t2 + 42*Power(t2,2) + 
                  9*Power(t2,3) + 49*Power(t2,4) - 9*Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s2,2)*t2*
                (2 + 133*t2 - 399*Power(t2,2) + 236*Power(t2,3) + 
                  21*Power(t2,4) + 7*Power(t2,5) + 
                  t1*(-2 - 49*t2 + 34*Power(t2,2) + 4*Power(t2,3) - 
                     12*Power(t2,4) + Power(t2,5))) + 
               s2*(Power(t1,2)*
                   (-2 + 31*t2 - 14*Power(t2,2) - 13*Power(t2,3) + 
                     20*Power(t2,4) + 2*Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (2 - 11*t2 + 353*Power(t2,2) - 43*Power(t2,3) - 
                     5*Power(t2,4) + 8*Power(t2,5)) - 
                  t1*(-4 + 46*t2 - 133*Power(t2,2) - 164*Power(t2,3) + 
                     240*Power(t2,4) + 14*Power(t2,5) + Power(t2,6))))))*
       B1(s,t2,s1))/
     (s*(-1 + s1)*(-1 + s2)*(-s + s2 - t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) + 
    (8*(-2*Power(s1,12)*Power(s2 - t1,2)*(-1 + t2) + 
         2*Power(-1 + s,3)*s*(1 - 3*s + Power(s,2))*Power(t2,4)*
          (-2 + s + t2)*Power(-1 + s + t2,2) - 
         (-1 + s)*s*s1*Power(t2,3)*(-1 + s + t2)*
          (4*Power(s,7)*t2 - Power(s,6)*
             (-8 + t1 + (19 + 4*s2)*t2 - 8*Power(t2,2)) + 
            Power(s,5)*(-65 + (28 + 33*s2)*t2 - 
               (35 + 8*s2)*Power(t2,2) + 4*Power(t2,3) + 3*t1*(4 + t2)) \
- 2*(-1 + t2)*(9 + (36 - 11*s2)*t2 + (-17 + 5*s2)*Power(t2,2) + 
               (1 + s2)*Power(t2,3) - t1*(1 + Power(t2,2))) + 
            Power(s,4)*(211 + (7 - 115*s2)*t2 + 
               (39 + 55*s2)*Power(t2,2) - 2*(7 + 2*s2)*Power(t2,3) + 
               t1*(-46 - 9*t2 + 6*Power(t2,2))) + 
            Power(s,2)*(299 + (242 - 219*s2)*t2 + 
               3*(-111 + 71*s2)*Power(t2,2) + 
               (42 - 40*s2)*Power(t2,3) - 2*(3 + s2)*Power(t2,4) + 
               t1*(-63 + 15*t2 + 20*Power(t2,2) - 2*Power(t2,3))) - 
            2*s*(62 + (100 - 56*s2)*t2 + (-181 + 69*s2)*Power(t2,2) + 
               (49 - 16*s2)*Power(t2,3) - (3 + 2*s2)*Power(t2,4) + 
               t1*(-11 + 6*t2 + 2*Power(t2,2) + Power(t2,3))) + 
            Power(s,3)*(-347 + (-116 + 215*s2)*t2 + 
               (85 - 154*s2)*Power(t2,2) + 10*(1 + 2*s2)*Power(t2,3) + 
               2*Power(t2,4) + 
               t1*(78 + t2 - 20*Power(t2,2) + 2*Power(t2,3)))) - 
         Power(s1,2)*Power(t2,2)*
          (Power(s,9)*(6 + 21*t2 - 41*Power(t2,2) + t1*(-5 + 11*t2)) - 
            Power(s,8)*(29 + 12*Power(t1,2) + 7*(25 + 2*s2)*t2 - 
               (275 + 37*s2)*Power(t2,2) + 92*Power(t2,3) + 
               2*t1*(-31 + (26 + 6*s2)*t2 - 7*Power(t2,2))) + 
            Power(-1 + t1 + t2 - s2*t2,2)*
             (3 + (-16 + s2)*t2 + (13 - 5*s2)*Power(t2,2) + 
               t1*(-3 + 5*t2 + 2*Power(t2,2))) + 
            Power(s,7)*(3 - 3*Power(t1,3) + 5*(125 + 24*s2)*t2 - 
               4*(179 + 84*s2)*Power(t2,2) + 
               10*(44 + 7*s2)*Power(t2,3) - 67*Power(t2,4) - 
               Power(t1,2)*(-96 + (13 + 2*s2)*t2 + 6*Power(t2,2)) + 
               t1*(-275 + t2 + 112*s2*t2 - 4*(10 + 3*s2)*Power(t2,2) + 
                  7*Power(t2,3))) + 
            Power(s,6)*(256 - 4*(281 + 112*s2)*t2 + 
               2*(320 + 649*s2)*Power(t2,2) + 
               (-614 - 499*s2 + 12*Power(s2,2))*Power(t2,3) + 
               (224 + 45*s2)*Power(t2,4) - 14*Power(t2,5) + 
               Power(t1,3)*(15 - 5*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(-297 + (99 + 13*s2)*t2 + 43*Power(t2,2)) - 
               t1*(-614 + 6*(-85 + 73*s2)*t2 + 
                  (215 - 116*s2 + 4*Power(s2,2))*Power(t2,2) + 
                  (57 + 8*s2)*Power(t2,3) + 2*Power(t2,4))) + 
            Power(s,5)*(-720 + (816 + 916*s2)*t2 + 
               2*(585 - 1369*s2 + Power(s2,2))*Power(t2,2) - 
               (564 - 1592*s2 + 89*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) + 
               (-107 - 219*s2 + 10*Power(s2,2))*Power(t2,4) + 
               6*(4 + s2)*Power(t2,5) + 
               Power(t1,3)*(-27 + 20*t2 + 8*Power(t2,2)) - 
               Power(t1,2)*(-456 + 14*(17 + 3*s2)*t2 + 
                  (94 + 3*s2)*Power(t2,2) + 4*(-2 + s2)*Power(t2,3)) + 
               t1*(-750 + (-1499 + 920*s2)*t2 + 
                  (1221 - 484*s2 + 31*Power(s2,2))*Power(t2,2) + 
                  2*(-5 + 34*s2)*Power(t2,3) + (-34 + 4*s2)*Power(t2,4))\
) + Power(s,4)*(942 + (392 - 1093*s2)*t2 + 
               (-3976 + 3217*s2 + 15*Power(s2,2))*Power(t2,2) + 
               (3055 - 2745*s2 + 199*Power(s2,2) + 15*Power(s2,3))*
                Power(t2,3) + 
               (-606 + 560*s2 - 79*Power(s2,2))*Power(t2,4) + 
               (22 + 4*Power(s2,2))*Power(t2,5) + 2*Power(t2,6) - 
               5*Power(t1,3)*(-3 + 5*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-345 + 5*(38 + 17*s2)*t2 + 
                  (85 - 3*s2)*Power(t2,2) + 4*(-7 + 3*s2)*Power(t2,3)) \
+ t1*(461 + (2171 - 1134*s2)*t2 + 
                  (-2355 + 1052*s2 - 89*Power(s2,2))*Power(t2,2) + 
                  3*(169 - 78*s2 + 3*Power(s2,2))*Power(t2,3) + 
                  (38 + 4*s2 - 2*Power(s2,2))*Power(t2,4) - 
                  12*Power(t2,5))) - 
            s*(48 + (54 - 22*s2)*t2 + 
               (-162 - 143*s2 + 36*Power(s2,2))*Power(t2,2) + 
               2*(-45 + 143*s2 - 73*Power(s2,2) + 6*Power(s2,3))*
                Power(t2,3) + 
               (212 - 145*s2 + 66*Power(s2,2) - 17*Power(s2,3))*
                Power(t2,4) + 
               2*(-32 + 9*s2 + Power(s2,2))*Power(t2,5) + 
               (2 + 6*s2)*Power(t2,6) + 
               Power(t1,3)*(-15 + 20*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(48 + 2*(-78 + 19*s2)*t2 + 
                  (58 - 57*s2)*Power(t2,2) + (8 - 12*s2)*Power(t2,3)) + 
               t1*(-57 + (342 - 104*s2)*t2 + 
                  (-451 + 356*s2 - 35*Power(s2,2))*Power(t2,2) + 
                  2*(104 - 90*s2 + 27*Power(s2,2))*Power(t2,3) + 
                  (-50 + 20*s2 + 4*Power(s2,2))*Power(t2,4) - 
                  8*(-1 + s2)*Power(t2,5))) + 
            Power(s,3)*(-673 + 15*Power(t1,3) + 6*(-176 + 125*s2)*t2 + 
               (4377 - 1901*s2 - 62*Power(s2,2))*Power(t2,2) - 
               (3828 - 2272*s2 + 89*Power(s2,2) + 34*Power(s2,3))*
                Power(t2,3) + 
               (1196 - 776*s2 + 144*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,4) + 
               (-94 + 20*s2 - 22*Power(s2,2))*Power(t2,5) + 
               2*(-3 + s2)*Power(t2,6) + 
               Power(t1,2)*(72 + (95 - 110*s2)*t2 + 
                  (-50 + 42*s2)*Power(t2,2) - 8*(-4 + s2)*Power(t2,3)) \
+ t1*(-51 + (-1899 + 848*s2)*t2 + 
                  2*(1168 - 646*s2 + 63*Power(s2,2))*Power(t2,2) + 
                  (-921 + 392*s2 - 42*Power(s2,2))*Power(t2,3) + 
                  4*(26 - 12*s2 + Power(s2,2))*Power(t2,4) + 
                  8*s2*Power(t2,5))) + 
            Power(s,2)*(260 + (577 - 260*s2)*t2 + 
               (-1939 + 325*s2 + 76*Power(s2,2))*Power(t2,2) + 
               (1539 - 473*s2 - 151*Power(s2,2) + 32*Power(s2,3))*
                Power(t2,3) - 
               (509 - 324*s2 + 32*Power(s2,2) + 19*Power(s2,3))*
                Power(t2,4) + 
               (66 - 40*s2 + 20*Power(s2,2))*Power(t2,5) + 
               (6 + 4*s2)*Power(t2,6) + 
               Power(t1,3)*(-27 + 25*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(69 + (-257 + 87*s2)*t2 + 
                  (61 - 78*s2)*Power(t2,2) - 8*(1 + s2)*Power(t2,3)) + 
               t1*(-104 + (1050 - 386*s2)*t2 + 
                  (-1343 + 916*s2 - 94*Power(s2,2))*Power(t2,2) + 
                  (655 - 356*s2 + 72*Power(s2,2))*Power(t2,3) + 
                  2*(-79 + 32*s2)*Power(t2,4) - 4*(-5 + 4*s2)*Power(t2,5)\
))) + Power(s1,3)*t2*(6 - 18*t1 + 18*Power(t1,2) - 6*Power(t1,3) - 
            48*t2 + 4*Power(s,10)*t2 + 21*s2*t2 + 93*t1*t2 - 
            42*s2*t1*t2 - 42*Power(t1,2)*t2 + 21*s2*Power(t1,2)*t2 - 
            3*Power(t1,3)*t2 + 72*Power(t2,2) - 141*s2*Power(t2,2) + 
            20*Power(s2,2)*Power(t2,2) - 11*t1*Power(t2,2) + 
            162*s2*t1*Power(t2,2) - 20*Power(s2,2)*t1*Power(t2,2) - 
            94*Power(t1,2)*Power(t2,2) - 21*s2*Power(t1,2)*Power(t2,2) + 
            33*Power(t1,3)*Power(t2,2) + 12*Power(t2,3) + 
            129*s2*Power(t2,3) - 112*Power(s2,2)*Power(t2,3) + 
            5*Power(s2,3)*Power(t2,3) - 177*t1*Power(t2,3) + 
            74*s2*t1*Power(t2,3) + 47*Power(s2,2)*t1*Power(t2,3) + 
            98*Power(t1,2)*Power(t2,3) - 88*s2*Power(t1,2)*Power(t2,3) + 
            12*Power(t1,3)*Power(t2,3) - 78*Power(t2,4) + 
            81*s2*Power(t2,4) + 20*Power(s2,2)*Power(t2,4) - 
            23*Power(s2,3)*Power(t2,4) + 105*t1*Power(t2,4) - 
            178*s2*t1*Power(t2,4) + 73*Power(s2,2)*t1*Power(t2,4) + 
            20*Power(t1,2)*Power(t2,4) - 20*s2*Power(t1,2)*Power(t2,4) + 
            36*Power(t2,5) - 90*s2*Power(t2,5) + 
            72*Power(s2,2)*Power(t2,5) - 18*Power(s2,3)*Power(t2,5) + 
            8*t1*Power(t2,5) - 16*s2*t1*Power(t2,5) + 
            8*Power(s2,2)*t1*Power(t2,5) - 
            Power(s,9)*t2*(4*s2 - 17*t1 + 5*(4 + t2)) + 
            Power(s,8)*(-12 + 7*(3 + 5*s2)*t2 + 8*Power(t1,2)*t2 + 
               3*(75 + 2*s2)*Power(t2,2) - 165*Power(t2,3) + 
               2*t1*(5 - 6*(14 + s2)*t2 + 47*Power(t2,2))) + 
            Power(s,7)*(70 + (185 - 123*s2)*t2 - 
               3*(375 + 58*s2)*Power(t2,2) + 
               (962 + 103*s2)*Power(t2,3) - 268*Power(t2,4) + 
               5*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(24 - 143*t2 + 3*Power(t2,2)) + 
               t1*(-117 + (647 + 136*s2)*t2 - 64*(5 + s2)*Power(t2,2) + 
                  98*Power(t2,3))) + 
            Power(s,6)*(-147 + 3*(-300 + 71*s2)*t2 + 
               (2059 + 1040*s2)*Power(t2,2) + 
               (-1754 - 817*s2 + 32*Power(s2,2))*Power(t2,3) + 
               (843 + 127*s2)*Power(t2,4) - 136*Power(t2,5) + 
               Power(t1,3)*(-14 - 33*t2 + 5*Power(t2,2)) - 
               Power(t1,2)*(153 + (-689 + 6*s2)*t2 + 
                  (84 + 5*s2)*Power(t2,2) + 25*Power(t2,3)) + 
               t1*(472 - 4*(219 + 160*s2)*t2 - 
                  2*(120 - 251*s2 + 6*Power(s2,2))*Power(t2,2) - 
                  15*(7 + 4*s2)*Power(t2,3) + 37*Power(t2,4))) - 
            Power(s,5)*(-105 + (-1643 + 173*s2)*t2 + 
               (473 + 2842*s2 - 6*Power(s2,2))*Power(t2,2) + 
               (410 - 2930*s2 + 277*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,3) + 
               (286 + 604*s2 - 53*Power(s2,2))*Power(t2,4) - 
               5*(41 + 10*s2)*Power(t2,5) + 16*Power(t2,6) + 
               Power(t1,3)*(-1 - 73*t2 + 32*Power(t2,2) + 
                  8*Power(t2,3)) - 
               Power(t1,2)*(381 + (-1282 + 3*s2)*t2 + 
                  (442 - 24*s2)*Power(t2,2) + 
                  (125 - 9*s2)*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(915 + (622 - 1566*s2)*t2 + 
                  (-2989 + 1682*s2 - 103*Power(s2,2))*Power(t2,2) + 
                  (1117 - 460*s2 + 15*Power(s2,2))*Power(t2,3) + 
                  6*(19 + 4*s2)*Power(t2,4) + 8*Power(t2,5))) + 
            Power(s,4)*(76 + 4*(-299 + 6*s2)*t2 + 
               (-3573 + 3741*s2 + 49*Power(s2,2))*Power(t2,2) + 
               (6189 - 5972*s2 + 807*Power(s2,2) + 34*Power(s2,3))*
                Power(t2,3) + 
               (-2322 + 1929*s2 - 436*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,4) + 
               (149 - 47*s2 + 15*Power(s2,2))*Power(t2,5) + 
               28*Power(t2,6) + 
               Power(t1,3)*(30 - 65*t2 + 31*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(-456 + 23*(40 + 3*s2)*t2 + 
                  (-538 + 77*s2)*Power(t2,2) - 
                  4*(12 + s2)*Power(t2,3) - 20*(-1 + s2)*Power(t2,4)) + 
               t1*(912 + (3295 - 2206*s2)*t2 + 
                  (-6005 + 2934*s2 - 306*Power(s2,2))*Power(t2,2) + 
                  (3303 - 1430*s2 + 121*Power(s2,2))*Power(t2,3) + 
                  (-271 + 194*s2 + Power(s2,2))*Power(t2,4) + 
                  8*(-11 + 2*s2)*Power(t2,5))) + 
            Power(s,3)*(-192 + (64 + 37*s2)*t2 + 
               (4704 - 1882*s2 - 209*Power(s2,2))*Power(t2,2) - 
               (8265 - 5761*s2 + 593*Power(s2,2) + 105*Power(s2,3))*
                Power(t2,3) + 
               (4391 - 3570*s2 + 913*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,4) + 
               (-532 + 446*s2 - 187*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,5) + 
               2*(-19 + 14*s2 + Power(s2,2))*Power(t2,6) + 
               14*Power(t2,7) + 
               Power(t1,3)*(-25 + 15*t2 + 16*Power(t2,2)) + 
               Power(t1,2)*(234 + (61 - 186*s2)*t2 + 
                  (91 - 16*s2)*Power(t2,2) + 
                  2*(-27 + 5*s2)*Power(t2,3) + 
                  8*(-1 + 2*s2)*Power(t2,4)) + 
               t1*(-403 + (-4091 + 1876*s2)*t2 + 
                  4*(1394 - 775*s2 + 110*Power(s2,2))*Power(t2,2) - 
                  8*(382 - 209*s2 + 31*Power(s2,2))*Power(t2,3) + 
                  (730 - 516*s2 + 38*Power(s2,2))*Power(t2,4) - 
                  4*(-12 + s2)*s2*Power(t2,5) - 24*Power(t2,6))) + 
            Power(s,2)*(141 + 9*(23 + 3*s2)*t2 + 
               (-1593 - 490*s2 + 267*Power(s2,2))*Power(t2,2) + 
               3*(980 - 490*s2 - 145*Power(s2,2) + 43*Power(s2,3))*
                Power(t2,3) - 
               (2266 - 1967*s2 + 326*Power(s2,2) + 64*Power(s2,3))*
                Power(t2,4) + 
               (523 - 788*s2 + 217*Power(s2,2) - 8*Power(s2,3))*
                Power(t2,5) + 
               (36 + 84*s2 - 20*Power(s2,2))*Power(t2,6) - 
               2*(-6 + s2)*Power(t2,7) + 
               Power(t1,3)*(-10 + 5*t2 + 11*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(15 + (-439 + 204*s2)*t2 - 
                  (28 + 99*s2)*Power(t2,2) - 
                  5*(5 + 12*s2)*Power(t2,3) + 8*(-5 + s2)*Power(t2,4)) \
+ t1*(-16 + (2416 - 972*s2)*t2 + 
                  (-2570 + 2162*s2 - 334*Power(s2,2))*Power(t2,2) + 
                  (839 - 616*s2 + 240*Power(s2,2))*Power(t2,3) + 
                  (-29 + 320*s2 - 30*Power(s2,2))*Power(t2,4) + 
                  (60 - 96*s2)*Power(t2,5) + 4*(-7 + 4*s2)*Power(t2,6))) \
+ s*(-47 + (40 - 57*s2)*t2 + (-243 + 742*s2 - 133*Power(s2,2))*
                Power(t2,2) + 
               (591 - 808*s2 + 578*Power(s2,2) - 59*Power(s2,3))*
                Power(t2,3) + 
               2*(-172 + 89*s2 - 88*Power(s2,2) + 37*Power(s2,3))*
                Power(t2,4) + 
               (-25 - 21*s2 + 47*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,5) - 
               2*(-11 + 14*s2 + 5*Power(s2,2))*Power(t2,6) - 
               6*(-1 + s2)*Power(t2,7) + 
               Power(t1,3)*(19 + 3*t2 - 64*Power(t2,2) - 
                  24*Power(t2,3)) + 
               Power(t1,2)*(-63 - 3*(-76 + 35*s2)*t2 + 
                  8*(26 + 11*s2)*Power(t2,2) + 
                  (-71 + 151*s2)*Power(t2,3) + 4*(1 + 4*s2)*Power(t2,4)) \
+ t1*(75 + 3*(-237 + 98*s2)*t2 + 
                  (487 - 914*s2 + 129*Power(s2,2))*Power(t2,2) + 
                  (311 - 100*s2 - 145*Power(s2,2))*Power(t2,3) - 
                  2*(121 - 54*s2 + 41*Power(s2,2))*Power(t2,4) - 
                  4*(-27 + 4*s2 + Power(s2,2))*Power(t2,5) + 
                  4*(-7 + 4*s2)*Power(t2,6)))) + 
         Power(s1,4)*(-3 + 9*t1 - 9*Power(t1,2) + 3*Power(t1,3) + 
            30*t2 - 21*s2*t2 - 39*t1*t2 + 42*s2*t1*t2 - 
            12*Power(t1,2)*t2 - 21*s2*Power(t1,2)*t2 + 
            21*Power(t1,3)*t2 + 2*Power(s,9)*(4 - 9*t2)*t2 + 
            30*Power(t2,2) + 153*s2*Power(t2,2) - 
            30*Power(s2,2)*Power(t2,2) - 255*t1*Power(t2,2) - 
            126*s2*t1*Power(t2,2) + 30*Power(s2,2)*t1*Power(t2,2) + 
            270*Power(t1,2)*Power(t2,2) - 
            27*s2*Power(t1,2)*Power(t2,2) - 45*Power(t1,3)*Power(t2,2) - 
            208*Power(t2,3) + 87*s2*Power(t2,3) + 
            168*Power(s2,2)*Power(t2,3) - 10*Power(s2,3)*Power(t2,3) + 
            413*t1*Power(t2,3) - 542*s2*t1*Power(t2,3) - 
            38*Power(s2,2)*t1*Power(t2,3) - 2*Power(t1,2)*Power(t2,3) + 
            225*s2*Power(t1,2)*Power(t2,3) - 
            93*Power(t1,3)*Power(t2,3) + 189*Power(t2,4) - 
            461*s2*Power(t2,4) + 180*Power(s2,2)*Power(t2,4) + 
            40*Power(s2,3)*Power(t2,4) + 38*t1*Power(t2,4) + 
            306*s2*t1*Power(t2,4) - 270*Power(s2,2)*t1*Power(t2,4) - 
            207*Power(t1,2)*Power(t2,4) + 
            215*s2*Power(t1,2)*Power(t2,4) - 
            30*Power(t1,3)*Power(t2,4) - 6*Power(t2,5) + 
            154*s2*Power(t2,5) - 238*Power(s2,2)*Power(t2,5) + 
            90*Power(s2,3)*Power(t2,5) - 154*t1*Power(t2,5) + 
            296*s2*t1*Power(t2,5) - 142*Power(s2,2)*t1*Power(t2,5) - 
            40*Power(t1,2)*Power(t2,5) + 40*s2*Power(t1,2)*Power(t2,5) - 
            32*Power(t2,6) + 88*s2*Power(t2,6) - 
            80*Power(s2,2)*Power(t2,6) + 24*Power(s2,3)*Power(t2,6) - 
            12*t1*Power(t2,6) + 24*s2*t1*Power(t2,6) - 
            12*Power(s2,2)*t1*Power(t2,6) + 
            Power(s,8)*t2*(-32 + 4*Power(t1,2) + t1*(34 - 98*t2) - 
               2*t2 + 35*Power(t2,2) + s2*(-8 + 19*t2)) + 
            Power(s,7)*(-6 + Power(t1,3)*(-2 + t2) + (45 + 62*s2)*t2 + 
               (480 - 84*s2)*Power(t2,2) + (-829 + 4*s2)*Power(t2,3) + 
               331*Power(t2,4) - Power(t1,2)*t2*(2*s2 + 37*t2) + 
               t1*(5 - (251 + 24*s2)*t2 + (651 + 44*s2)*Power(t2,2) - 
                  306*Power(t2,3))) + 
            Power(s,6)*(33 - 5*(-23 + 42*s2)*t2 - 
               (1609 + 79*s2)*Power(t2,2) + 
               (2676 + 289*s2 - 24*Power(s2,2))*Power(t2,3) - 
               (1462 + 81*s2)*Power(t2,4) + 366*Power(t2,5) + 
               Power(t1,3)*(11 - 37*t2 - 30*Power(t2,2)) + 
               Power(t1,2)*(6 + (-256 + 27*s2)*t2 + 
                  (448 - 6*s2)*Power(t2,2) - 9*Power(t2,3)) + 
               t1*(-54 + (772 + 214*s2)*t2 + 
                  (-1145 - 440*s2 + 12*Power(s2,2))*Power(t2,2) + 
                  (501 + 124*s2)*Power(t2,3) - 264*Power(t2,4))) + 
            Power(s,5)*(-71 + (-678 + 406*s2)*t2 + 
               (1619 + 1114*s2 - 6*Power(s2,2))*Power(t2,2) + 
               (-1847 - 1970*s2 + 293*Power(s2,2))*Power(t2,3) + 
               (1212 + 490*s2 - 81*Power(s2,2))*Power(t2,4) - 
               (519 + 92*s2)*Power(t2,5) + 114*Power(t2,6) + 
               Power(t1,3)*(-16 + 35*t2 + 65*Power(t2,2) - 
                  28*Power(t2,3)) + 
               Power(t1,2)*(-45 + (935 - 76*s2)*t2 + 
                  3*(-525 + 46*s2)*Power(t2,2) + 
                  2*(85 + 3*s2)*Power(t2,3) + 35*Power(t2,4)) - 
               t1*(-182 + (715 + 724*s2)*t2 + 
                  3*(422 - 586*s2 + 41*Power(s2,2))*Power(t2,2) - 
                  5*(401 - 190*s2 + 9*Power(s2,2))*Power(t2,3) - 
                  6*(-19 + 20*s2)*Power(t2,4) + 74*Power(t2,5))) + 
            Power(s,4)*(71 + (1189 - 487*s2)*t2 + 
               (1318 - 2288*s2 - 57*Power(s2,2))*Power(t2,2) - 
               (4842 - 6005*s2 + 1235*Power(s2,2) + 8*Power(s2,3))*
                Power(t2,3) + 
               (2900 - 2590*s2 + 819*Power(s2,2) - 14*Power(s2,3))*
                Power(t2,4) + (-557 + 3*Power(s2,2))*Power(t2,5) - 
               (33 + 38*s2)*Power(t2,6) + 4*Power(t2,7) + 
               Power(t1,3)*(-3 + 55*t2 - 69*Power(t2,2) + 
                  39*Power(t2,3) + 10*Power(t2,4)) + 
               Power(t1,2)*(111 + (-1092 + 53*s2)*t2 + 
                  (1658 - 279*s2)*Power(t2,2) + 
                  (-802 + 197*s2)*Power(t2,3) + 
                  (-163 + 49*s2)*Power(t2,4) - 16*Power(t2,5)) + 
               t1*(-279 + (-1123 + 1278*s2)*t2 + 
                  (5407 - 3022*s2 + 384*Power(s2,2))*Power(t2,2) + 
                  (-6811 + 3102*s2 - 356*Power(s2,2))*Power(t2,3) + 
                  (2276 - 810*s2)*Power(t2,4) + 
                  (78 + 20*s2)*Power(t2,5) + 12*Power(t2,6))) + 
            Power(s,3)*(-24 + 24*(-39 + 16*s2)*t2 + 
               (-3481 + 1466*s2 + 255*Power(s2,2))*Power(t2,2) + 
               2*(4735 - 3806*s2 + 675*Power(s2,2) + 50*Power(s2,3))*
                Power(t2,3) + 
               (-6796 + 7072*s2 - 2306*Power(s2,2) + 63*Power(s2,3))*
                Power(t2,4) + 
               (1359 - 1360*s2 + 537*Power(s2,2) - 36*Power(s2,3))*
                Power(t2,5) + 
               (-14 - 113*s2 + 43*Power(s2,2))*Power(t2,6) - 
               2*(33 + 2*s2)*Power(t2,7) + 
               Power(t1,3)*(22 - 41*t2 + 34*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(-114 + (202 + 78*s2)*t2 + 
                  3*(-157 + 48*s2)*Power(t2,2) + 
                  (202 - 148*s2)*Power(t2,3) + 
                  2*(-83 + 21*s2)*Power(t2,4) + 
                  8*(-6 + 5*s2)*Power(t2,5)) + 
               t1*(201 + (2853 - 1312*s2)*t2 + 
                  (-4901 + 2760*s2 - 564*Power(s2,2))*Power(t2,2) + 
                  (5560 - 3336*s2 + 558*Power(s2,2))*Power(t2,3) + 
                  (-3138 + 2180*s2 - 228*Power(s2,2))*Power(t2,4) - 
                  4*(-97 + 71*s2 + 4*Power(s2,2))*Power(t2,5) - 
                  4*(-31 + 6*s2)*Power(t2,6))) + 
            Power(s,2)*(-13 + (362 - 218*s2)*t2 + 
               (1337 + 575*s2 - 345*Power(s2,2))*Power(t2,2) + 
               (-4672 + 3221*s2 + 293*Power(s2,2) - 198*Power(s2,3))*
                Power(t2,3) + 
               (4040 - 4657*s2 + 1089*Power(s2,2) + 56*Power(s2,3))*
                Power(t2,4) + 
               2*(-470 + 1531*s2 - 517*Power(s2,2) + 44*Power(s2,3))*
                Power(t2,5) - 
               (72 + 391*s2 - 128*Power(s2,2) + 11*Power(s2,3))*
                Power(t2,6) + 
               4*(-7 - 2*s2 + 2*Power(s2,2))*Power(t2,7) - 
               14*Power(t2,8) + 
               Power(t1,3)*(-11 - 23*t2 - 16*Power(t2,2) + 
                  30*Power(t2,3) + 4*Power(t2,4)) + 
               Power(t1,2)*(36 - 31*(-12 + 5*s2)*t2 + 
                  232*Power(t2,2) + (37 + 2*s2)*Power(t2,3) + 
                  2*(-67 + 16*s2)*Power(t2,4) + 
                  24*(-1 + s2)*Power(t2,5)) + 
               t1*(-44 + (-2108 + 802*s2)*t2 + 
                  (951 - 1676*s2 + 438*Power(s2,2))*Power(t2,2) + 
                  (-217 + 756*s2 - 290*Power(s2,2))*Power(t2,3) + 
                  2*(96 - 496*s2 + 49*Power(s2,2))*Power(t2,4) + 
                  (-458 + 532*s2 - 68*Power(s2,2))*Power(t2,5) - 
                  80*(-1 + s2)*Power(t2,6) + 20*Power(t2,7))) + 
            s*(13 + (-103 + 92*s2)*t2 + 
               (262 - 876*s2 + 183*Power(s2,2))*Power(t2,2) + 
               (-209 + 436*s2 - 893*Power(s2,2) + 116*Power(s2,3))*
                Power(t2,3) - 
               (10 + 23*s2 - 127*Power(s2,2) + 129*Power(s2,3))*
                Power(t2,4) + 
               2*(2 + 103*s2 - 153*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,5) + 
               (65 + 143*s2 - 93*Power(s2,2) + 13*Power(s2,3))*
                Power(t2,6) + 
               2*(-6 + 10*s2 + 7*Power(s2,2))*Power(t2,7) + 
               2*(-5 + s2)*Power(t2,8) + 
               Power(t1,3)*(-4 - 11*t2 + 61*Power(t2,2) + 
                  36*Power(t2,3) + 16*Power(t2,4)) + 
               Power(t1,2)*(15 + 3*(-51 + 32*s2)*t2 + 
                  15*(-35 + 2*s2)*Power(t2,2) - 
                  2*(38 + 141*s2)*Power(t2,3) - 
                  5*(33 + 10*s2)*Power(t2,4) + 8*(-8 + 3*s2)*Power(t2,5)\
) + t1*(-20 + (577 - 276*s2)*t2 + 
                  (560 + 702*s2 - 177*Power(s2,2))*Power(t2,2) + 
                  (-1249 + 1134*s2 + 81*Power(s2,2))*Power(t2,3) + 
                  2*(113 + 78*s2 + 104*Power(s2,2))*Power(t2,4) + 
                  (-86 + 260*s2 - 38*Power(s2,2))*Power(t2,5) - 
                  4*(8 + 8*s2 + Power(s2,2))*Power(t2,6) - 
                  8*(-3 + s2)*Power(t2,7)))) + 
         Power(s1,11)*(2*Power(s,2)*Power(-1 + t2,2) + 
            2*s*(-Power(-1 + t2,2) + Power(s2,2)*(-2 + 3*t2) + 
               Power(t1,2)*(-4 + 5*t2) - 
               2*t1*(2 - 3*t2 + Power(t2,2)) + 
               s2*(9 + t1*(6 - 8*t2) - 16*t2 + 7*Power(t2,2))) - 
            (s2 - t1)*(-(Power(t1,2)*(-5 + t2)) + 4*Power(-1 + t2,2) + 
               Power(s2,2)*(1 + 3*t2) + 
               2*t1*(-5 + 2*t2 + 3*Power(t2,2)) - 
               2*s2*(-3 - 2*t2 + 5*Power(t2,2) + t1*(3 + t2)))) + 
         Power(s1,10)*(2 - 16*t1 + 43*Power(t1,2) - 27*Power(t1,3) - 
            6*t2 + 20*t1*t2 - 18*Power(t1,2)*t2 - 9*Power(t1,3)*t2 + 
            6*Power(t2,2) + 8*t1*Power(t2,2) - 
            19*Power(t1,2)*Power(t2,2) - 2*Power(t2,3) - 
            12*t1*Power(t2,3) - 6*Power(t1,2)*Power(t2,3) + 
            Power(s,3)*(-2 + 8*t2 - 6*Power(t2,2)) + 
            Power(s2,3)*(2 + 19*t2 + 15*Power(t2,2)) - 
            2*Power(s,2)*(-12 + 2*Power(s2,2) + 31*t2 - 
               25*Power(t2,2) + 6*Power(t2,3) + 
               Power(t1,2)*(-3 + 7*t2) - 
               2*t1*(7 - 15*t2 + 7*Power(t2,2)) + 
               2*s2*(8 - (23 + 3*t1)*t2 + 14*Power(t2,2))) - 
            Power(s2,2)*(-21 - 12*t2 + 13*Power(t2,2) + 
               20*Power(t2,3) + t1*(23 + 63*t2 + 22*Power(t2,2))) + 
            s2*(4*Power(-1 + t2,2)*(3 + 4*t2) + 
               Power(t1,2)*(48 + 53*t2 + 7*Power(t2,2)) + 
               t1*(-62 + 38*Power(t2,2) + 24*Power(t2,3))) + 
            s*(2*Power(-1 + t2,2)*(1 + 5*t2) + 
               Power(t1,3)*(-32 + 7*t2) + Power(s2,3)*(7 + 10*t2) + 
               Power(t1,2)*(2 - 3*t2 - 25*Power(t2,2)) + 
               4*t1*(4 + 5*t2 - 12*Power(t2,2) + 3*Power(t2,3)) - 
               Power(s2,2)*(-22 + 39*t2 + 9*Power(t2,2) + 
                  t1*(38 + 21*t2)) + 
               s2*(Power(t1,2)*(63 + 4*t2) + 
                  2*t1*(5 - 13*t2 + 34*Power(t2,2)) - 
                  2*(21 + 9*t2 - 61*Power(t2,2) + 31*Power(t2,3))))) + 
         Power(s1,8)*(19 - 131*t1 + 195*Power(t1,2) - 55*Power(t1,3) - 
            42*t2 + 125*t1*t2 + 142*Power(t1,2)*t2 - 
            169*Power(t1,3)*t2 + 18*Power(t2,2) + 107*t1*Power(t2,2) - 
            238*Power(t1,2)*Power(t2,2) - 87*Power(t1,3)*Power(t2,2) + 
            8*Power(t2,3) - 69*t1*Power(t2,3) - 
            86*Power(t1,2)*Power(t2,3) - 23*Power(t1,3)*Power(t2,3) + 
            3*Power(t2,4) - 28*t1*Power(t2,4) - 
            13*Power(t1,2)*Power(t2,4) - 2*Power(t1,3)*Power(t2,4) - 
            6*Power(t2,5) - 4*t1*Power(t2,5) + 
            2*Power(s,5)*(6 - 20*t2 + 13*Power(t2,2)) + 
            2*Power(s2,3)*(-1 + 9*t2 + 70*Power(t2,2) + 
               75*Power(t2,3) + 15*Power(t2,4)) - 
            2*Power(s2,2)*(-15 - 109*t2 - 15*Power(t2,2) + 
               100*Power(t2,3) + 34*Power(t2,4) + 5*Power(t2,5) + 
               t1*(-1 + 99*t2 + 255*Power(t2,2) + 135*Power(t2,3) + 
                  16*Power(t2,4))) + 
            s2*(Power(-1 + t2,2)*
                (78 + 151*t2 + 63*Power(t2,2) + 16*Power(t2,3)) + 
               Power(t1,2)*(40 + 385*t2 + 439*Power(t2,2) + 
                  131*Power(t2,3) + 13*Power(t2,4)) + 
               2*t1*(-88 - 237*t2 + 131*Power(t2,2) + 159*Power(t2,3) + 
                  33*Power(t2,4) + 2*Power(t2,5))) + 
            Power(s,4)*(-60 + 49*t2 + 90*Power(t2,2) - 65*Power(t2,3) + 
               10*Power(t1,2)*(-5 + 7*t2) + 
               2*Power(s2,2)*(-7 + 15*t2) + 
               t1*(-60 + 50*t2 + 38*Power(t2,2)) - 
               s2*(-49 + 88*t2 + Power(t2,2) + 20*t1*(-3 + 5*t2))) + 
            Power(s,3)*(81 + 13*Power(s2,3) - 127*t2 - 
               393*Power(t2,2) + 532*Power(t2,3) - 103*Power(t2,4) + 
               5*Power(t1,3)*(-26 + 7*t2) + 
               Power(t1,2)*(-196 + 130*t2 - 3*Power(t2,2)) + 
               t1*(-141 + 229*t2 - 223*Power(t2,2) + 76*Power(t2,3)) - 
               Power(s2,2)*(6 + 155*t2 - 146*Power(t2,2) + 
                  2*t1*(44 + 5*t2)) + 
               s2*(-80 - 10*Power(t1,2)*(-19 + t2) + 348*t2 + 
                  3*Power(t2,2) - 284*Power(t2,3) - 
                  4*t1*(-32 - 5*t2 + 7*Power(t2,2)))) + 
            Power(s,2)*(66 + 186*t2 - 353*Power(t2,2) - 
               86*Power(t2,3) + 213*Power(t2,4) - 26*Power(t2,5) + 
               Power(s2,3)*(-20 + 130*t2 + 23*Power(t2,2)) - 
               Power(t1,3)*(119 + 155*t2 + 30*Power(t2,2)) + 
               Power(t1,2)*(76 - 728*t2 + 242*Power(t2,2) - 
                  33*Power(t2,3)) + 
               t1*(212 - 730*t2 + 461*Power(t2,2) - 211*Power(t2,3) + 
                  36*Power(t2,4)) - 
               Power(s2,2)*(-147 + 344*t2 + 443*Power(t2,2) - 
                  257*Power(t2,3) + 2*t1*(4 + 203*t2 + 66*Power(t2,2))) \
+ s2*(-204 + 164*t2 + 376*Power(t2,2) + 271*Power(t2,3) - 
                  375*Power(t2,4) + 
                  Power(t1,2)*(92 + 585*t2 + 40*Power(t2,2)) + 
                  2*t1*(-138 + 613*t2 - 144*Power(t2,2) + 
                     82*Power(t2,3)))) + 
            s*(-(Power(-1 + t2,2)*
                  (98 - 7*t2 - 7*Power(t2,2) + 8*Power(t2,3))) - 
               Power(t1,3)*(84 + 233*t2 + 97*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(s2,3)*(-3 - 2*t2 + 271*Power(t2,2) + 
                  56*Power(t2,3)) - 
               Power(t1,2)*(-119 + 387*t2 + 331*Power(t2,2) + 
                  4*Power(t2,3) + 13*Power(t2,4)) + 
               t1*(222 + 215*t2 - 534*Power(t2,2) + 99*Power(t2,3) - 
                  6*Power(t2,4) + 4*Power(t2,5)) - 
               Power(s2,2)*(-29 - 356*t2 + 812*Power(t2,2) + 
                  279*Power(t2,3) - 90*Power(t2,4) + 
                  t1*(26 + 312*t2 + 589*Power(t2,2) + 151*Power(t2,3))) \
+ s2*(67 - 518*t2 + 169*Power(t2,2) + 296*Power(t2,3) + 78*Power(t2,4) - 
                  92*Power(t2,5) + 
                  Power(t1,2)*
                   (62 + 600*t2 + 462*Power(t2,2) + 66*Power(t2,3)) + 
                  2*t1*(-104 + 86*t2 + 591*Power(t2,2) + 
                     11*Power(t2,3) + 32*Power(t2,4))))) + 
         Power(s1,9)*(-6 + 55*t1 - 126*Power(t1,2) + 57*Power(t1,3) + 
            12*t2 - 69*t1*t2 + 22*Power(t1,2)*t2 + 69*Power(t1,3)*t2 - 
            15*t1*Power(t2,2) + 80*Power(t1,2)*Power(t2,2) + 
            15*Power(t1,3)*Power(t2,2) - 12*Power(t2,3) + 
            17*t1*Power(t2,3) + 22*Power(t1,2)*Power(t2,3) + 
            3*Power(t1,3)*Power(t2,3) + 6*Power(t2,4) + 
            12*t1*Power(t2,4) + 2*Power(t1,2)*Power(t2,4) - 
            2*Power(s,4)*(4 - 6*t2 + Power(t2,2)) - 
            2*Power(s2,3)*t2*(17 + 40*t2 + 15*Power(t2,2)) - 
            Power(s,3)*(32 - 140*t2 + 158*Power(t2,2) - 
               50*Power(t2,3) + 4*Power(s2,2)*(-4 + 5*t2) + 
               2*Power(t1,2)*(-10 + 7*t2) + 
               s2*(11 - 40*t1*(-1 + t2) + 50*t2 - 71*Power(t2,2)) + 
               t1*(15 - 86*t2 + 67*Power(t2,2))) + 
            2*Power(s2,2)*(-21 - 40*t2 + 25*Power(t2,2) + 
               26*Power(t2,3) + 10*Power(t2,4) + 
               t1*(11 + 91*t2 + 95*Power(t2,2) + 19*Power(t2,3))) - 
            s2*(3*Power(-1 + t2,2)*(13 + 15*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(75 + 229*t2 + 113*Power(t2,2) + 
                  15*Power(t2,3)) + 
               2*t1*(-77 - 47*t2 + 77*Power(t2,2) + 39*Power(t2,3) + 
                  8*Power(t2,4))) - 
            Power(s,2)*(5*Power(s2,3)*(3 + 2*t2) + 
               3*Power(t1,3)*(-29 + 7*t2) + 
               Power(t1,2)*(-94 + 45*t2 - 32*Power(t2,2)) + 
               t1*(-16 + 76*t2 - 163*Power(t2,2) + 59*Power(t2,3)) - 
               2*(-38 + 17*t2 + 95*Power(t2,2) - 87*Power(t2,3) + 
                  13*Power(t2,4)) + 
               Power(s2,2)*(18 - 143*t2 + 64*Power(t2,2) - 
                  2*t1*(41 + 15*t2)) + 
               s2*(-72 + 66*t2 + 283*Power(t2,2) - 233*Power(t2,3) + 
                  3*Power(t1,2)*(50 + t2) + 
                  2*t1*(56 - 27*t2 + 42*Power(t2,2)))) + 
            s*(Power(s2,3)*(7 - 82*t2 - 39*Power(t2,2)) - 
               4*Power(-1 + t2,2)*(-3 + 2*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(100 + 57*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-59 + 217*t2 - 14*Power(t2,2) + 
                  24*Power(t2,3)) + 
               2*t1*(-53 + 31*t2 + Power(t2,2) + 27*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,2)*(-107 + 115*t2 + 190*Power(t2,2) - 
                  30*Power(t2,3) + t1*(58 + 241*t2 + 91*Power(t2,2))) - 
               s2*(Power(t1,2)*(137 + 272*t2 + 29*Power(t2,2)) + 
                  2*t1*(-61 + 177*t2 + 52*Power(t2,3)) - 
                  2*(26 + 63*t2 - 60*Power(t2,2) - 83*Power(t2,3) + 
                     54*Power(t2,4))))) + 
         Power(s1,7)*(-38 + 154*t1 - 142*Power(t1,2) + 15*Power(t1,3) + 
            66*t2 + 52*t1*t2 - 438*Power(t1,2)*t2 + 195*Power(t1,3)*t2 + 
            18*Power(t2,2) - 449*t1*Power(t2,2) + 
            244*Power(t1,2)*Power(t2,2) + 207*Power(t1,3)*Power(t2,2) - 
            72*Power(t2,3) + 139*t1*Power(t2,3) + 
            278*Power(t1,2)*Power(t2,3) + 75*Power(t1,3)*Power(t2,3) + 
            18*Power(t2,4) + 91*t1*Power(t2,4) + 
            54*Power(t1,2)*Power(t2,4) + 12*Power(t1,3)*Power(t2,4) + 
            6*Power(t2,5) + 13*t1*Power(t2,5) + 
            4*Power(t1,2)*Power(t2,5) + 2*Power(t2,6) + 
            Power(s,6)*(2 + 20*t2 - 30*Power(t2,2)) - 
            Power(s2,3)*(-1 - 5*t2 + 90*Power(t2,2) + 260*Power(t2,3) + 
               145*Power(t2,4) + 15*Power(t2,5)) + 
            Power(s2,2)*(2*t2*
                (-96 - 190*t2 + 130*Power(t2,2) + 135*Power(t2,3) + 
                  20*Power(t2,4) + Power(t2,5)) + 
               t1*(-13 + 57*t2 + 580*Power(t2,2) + 680*Power(t2,3) + 
                  195*Power(t2,4) + 13*Power(t2,5))) - 
            s2*(Power(-1 + t2,2)*
                (60 + 322*t2 + 219*Power(t2,2) + 39*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(t1,2)*(-15 + 275*t2 + 745*Power(t2,2) + 
                  423*Power(t2,3) + 80*Power(t2,4) + 4*Power(t2,5)) + 
               t1*(-68 - 732*t2 - 250*Power(t2,2) + 754*Power(t2,3) + 
                  270*Power(t2,4) + 26*Power(t2,5))) + 
            Power(s,5)*(118 + Power(t1,2)*(48 - 98*t2) + 
               Power(s2,2)*(4 - 18*t2) - 395*t2 + 270*Power(t2,2) - 
               5*Power(t2,3) + 5*t1*(22 - 56*t2 + 23*Power(t2,2)) + 
               s2*(-31 + 132*t2 - 80*Power(t2,2) + 12*t1*(-3 + 8*t2))) + 
            Power(s,4)*(-119 + 545*t2 - 282*Power(t2,2) - 
               372*Power(t2,3) + 111*Power(t2,4) + 
               Power(s2,3)*(-4 + 5*t2) - 5*Power(t1,3)*(-23 + 7*t2) + 
               Power(s2,2)*(8 + t1*(47 - 15*t2) + 57*t2 - 
                  114*Power(t2,2)) - 
               2*Power(t1,2)*(-77 + 75*t2 + 5*Power(t2,2)) + 
               t1*(66 - 90*t2 - 66*Power(t2,2) + 96*Power(t2,3)) + 
               s2*(39 - 436*t2 + 508*Power(t2,2) + 52*Power(t2,3) + 
                  5*Power(t1,2)*(-27 + 5*t2) + 
                  4*t1*(-13 - 25*t2 + 35*Power(t2,2)))) + 
            Power(s,3)*(-80 + 85*t2 + 278*Power(t2,2) + 
               360*Power(t2,3) - 641*Power(t2,4) + 84*Power(t2,5) + 
               Power(s2,3)*(11 - 90*t2 + 15*Power(t2,2)) + 
               Power(t1,3)*(16 + 235*t2 + 75*Power(t2,2)) + 
               Power(t1,2)*(-230 + 1246*t2 - 543*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(-310 + 1649*t2 - 1679*Power(t2,2) + 
                  566*Power(t2,3) + 18*Power(t2,4)) + 
               Power(s2,2)*(-105 + 449*t2 + 261*Power(t2,2) - 
                  367*Power(t2,3) + t1*(-66 + 402*t2 + 58*Power(t2,2))) \
+ s2*(126 - 290*t2 - 761*Power(t2,2) + 282*Power(t2,3) + 
                  431*Power(t2,4) - 
                  2*Power(t1,2)*(-33 + 340*t2 + 5*Power(t2,2)) - 
                  4*t1*(-76 + 386*t2 - 129*Power(t2,2) + 28*Power(t2,3))\
)) + Power(s,2)*(24 - 517*t2 + 438*Power(t2,2) + 263*Power(t2,3) - 
               172*Power(t2,4) - 48*Power(t2,5) + 12*Power(t2,6) + 
               Power(s2,3)*(3 + 82*t2 - 364*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,3)*(-26 + 292*t2 + 217*Power(t2,2) + 
                  55*Power(t2,3)) + 
               Power(t1,2)*(40 + 225*t2 + 965*Power(t2,2) - 
                  116*Power(t2,3) + 31*Power(t2,4)) + 
               t1*(14 - 324*t2 + 2044*Power(t2,2) - 1090*Power(t2,3) + 
                  123*Power(t2,4) - 7*Power(t2,5)) + 
               Power(s2,2)*(106 - 744*t2 + 1643*Power(t2,2) + 
                  443*Power(t2,3) - 395*Power(t2,4) + 
                  2*t1*(1 + 8*t2 + 397*Power(t2,2) + 108*Power(t2,3))) \
- s2*(239 - 1582*t2 + 2167*Power(t2,2) + 202*Power(t2,3) + 
                  21*Power(t2,4) - 287*Power(t2,5) + 
                  Power(t1,2)*
                   (-16 + 366*t2 + 828*Power(t2,2) + 111*Power(t2,3)) + 
                  2*t1*(80 - 344*t2 + 1576*Power(t2,2) - 
                     267*Power(t2,3) + 54*Power(t2,4)))) + 
            s*(Power(t1,3)*(-24 + 241*t2 + 281*Power(t2,2) + 
                  96*Power(t2,3) + 8*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (147 + 273*t2 - 142*Power(t2,2) + 26*Power(t2,3) + 
                  26*Power(t2,4)) - 
               Power(s2,3)*(11 - 18*t2 + 59*Power(t2,2) + 
                  404*Power(t2,3) + 34*Power(t2,4)) + 
               Power(t1,2)*(82 + 81*t2 + 880*Power(t2,2) + 
                  256*Power(t2,3) + 69*Power(t2,4) + 4*Power(t2,5)) - 
               t1*(394 + 471*t2 - 557*Power(t2,2) - 535*Power(t2,3) + 
                  203*Power(t2,4) + 24*Power(t2,5)) + 
               Power(s2,2)*(35 - 82*t2 - 273*Power(t2,2) + 
                  1649*Power(t2,3) + 133*Power(t2,4) - 90*Power(t2,5) + 
                  t1*(30 + 92*t2 + 638*Power(t2,2) + 705*Power(t2,3) + 
                     117*Power(t2,4))) - 
               s2*(23 - 98*t2 - 1086*Power(t2,2) + 962*Power(t2,3) + 
                  257*Power(t2,4) - 20*Power(t2,5) - 38*Power(t2,6) + 
                  Power(t1,2)*
                   (-6 + 304*t2 + 927*Power(t2,2) + 408*Power(t2,3) + 
                     61*Power(t2,4)) + 
                  2*t1*(-18 - 132*t2 + 781*Power(t2,2) + 
                     745*Power(t2,3) - 8*Power(t2,4) + 4*Power(t2,5))))) \
- Power(s1,6)*(-30 - 6*s2 + 5*Power(s2,2) + 70*t1 - 18*s2*t1 - 
            5*Power(s2,2)*t1 - 25*Power(t1,2) + 24*s2*Power(t1,2) - 
            15*Power(t1,3) - 18*t2 - 258*s2*t2 - 28*Power(s2,2)*t2 + 
            5*Power(s2,3)*t2 + 330*t1*t2 + 404*s2*t1*t2 - 
            37*Power(s2,2)*t1*t2 - 466*Power(t1,2)*t2 - 
            31*s2*Power(t1,2)*t2 + 99*Power(t1,3)*t2 + 195*Power(t2,2) + 
            36*s2*Power(t2,2) - 445*Power(s2,2)*Power(t2,2) - 
            5*Power(s2,3)*Power(t2,2) - 531*t1*Power(t2,2) + 
            974*s2*t1*Power(t2,2) + 250*Power(s2,2)*t1*Power(t2,2) - 
            124*Power(t1,2)*Power(t2,2) - 
            605*s2*Power(t1,2)*Power(t2,2) + 
            255*Power(t1,3)*Power(t2,2) - 180*Power(t2,3) + 
            585*s2*Power(t2,3) - 180*Power(s2,2)*Power(t2,3) - 
            180*Power(s2,3)*Power(t2,3) - 165*t1*Power(t2,3) - 
            570*s2*t1*Power(t2,3) + 780*Power(s2,2)*t1*Power(t2,3) + 
            450*Power(t1,2)*Power(t2,3) - 
            675*s2*Power(t1,2)*Power(t2,3) + 
            135*Power(t1,3)*Power(t2,3) - 225*s2*Power(t2,4) + 
            475*Power(s2,2)*Power(t2,4) - 250*Power(s2,3)*Power(t2,4) + 
            255*t1*Power(t2,4) - 680*s2*t1*Power(t2,4) + 
            455*Power(s2,2)*t1*Power(t2,4) + 
            145*Power(t1,2)*Power(t2,4) - 
            205*s2*Power(t1,2)*Power(t2,4) + 
            30*Power(t1,3)*Power(t2,4) + 30*Power(t2,5) - 
            123*s2*Power(t2,5) + 164*Power(s2,2)*Power(t2,5) - 
            71*Power(s2,3)*Power(t2,5) + 39*t1*Power(t2,5) - 
            106*s2*t1*Power(t2,5) + 67*Power(s2,2)*t1*Power(t2,5) + 
            20*Power(t1,2)*Power(t2,5) - 20*s2*Power(t1,2)*Power(t2,5) + 
            3*Power(t2,6) - 9*s2*Power(t2,6) + 
            9*Power(s2,2)*Power(t2,6) - 3*Power(s2,3)*Power(t2,6) + 
            2*t1*Power(t2,6) - 4*s2*t1*Power(t2,6) + 
            2*Power(s2,2)*t1*Power(t2,6) + 
            2*Power(s,7)*(5 - 12*t2 + Power(t2,2)) + 
            Power(s,6)*(36 + Power(t1,2)*(22 - 70*t2) - 337*t2 - 
               4*Power(s2,2)*t2 + 458*Power(t2,2) - 90*Power(t2,3) + 
               8*t1*(9 - 41*t2 + 32*Power(t2,2)) + 
               s2*(-11 + 76*t2 - 86*Power(t2,2) + t1*(-8 + 44*t2))) + 
            Power(s,5)*(-139 + Power(t1,3)*(60 - 21*t2) + 958*t2 + 
               2*Power(s2,3)*t2 - 1577*Power(t2,2) + 648*Power(t2,3) - 
               91*Power(t2,4) + 
               Power(t1,2)*(46 - 81*t2 + 31*Power(t2,2)) + 
               t1*(-128 + 486*t2 - 695*Power(t2,2) + 395*Power(t2,3)) + 
               Power(s2,2)*((2 - 31*t2)*t2 - 5*t1*(-2 + 3*t2)) + 
               s2*(12 - 196*t2 + 493*Power(t2,2) - 142*Power(t2,3) + 
                  3*Power(t1,2)*(-17 + 8*t2) + 
                  t1*(-22 - 30*t2 + 84*Power(t2,2)))) + 
            Power(s,4)*(75 - 344*t2 + 871*Power(t2,2) - 
               315*Power(t2,3) - 436*Power(t2,4) + 32*Power(t2,5) + 
               Power(s2,3)*t2*(-23 + 18*t2) + 
               Power(t1,2)*(-259 + 1294*t2 - 709*Power(t2,2)) + 
               Power(t1,3)*(-71 + 215*t2 + 100*Power(t2,2)) + 
               2*t1*(-57 + 635*t2 - 945*Power(t2,2) + 
                  317*Power(t2,3) + 103*Power(t2,4)) + 
               Power(s2,2)*(4 + 234*t2 - 112*Power(t2,2) - 
                  159*Power(t2,3) + t1*(-39 + 235*t2 - 38*Power(t2,2))) \
+ s2*(73 - 294*t2 - 553*Power(t2,2) + 810*Power(t2,3) + 
                  139*Power(t2,4) + 
                  5*Power(t1,2)*(20 - 91*t2 + 5*Power(t2,2)) - 
                  2*t1*(-83 + 520*t2 - 263*Power(t2,2) + 30*Power(t2,3))\
)) + Power(s,3)*(262 - 1061*t2 + 1754*Power(t2,2) - 772*Power(t2,3) + 
               343*Power(t2,4) - 263*Power(t2,5) + 29*Power(t2,6) + 
               Power(s2,3)*t2*(30 - 203*t2 + 52*Power(t2,2)) + 
               Power(t1,3)*(-60 + 154*t2 + 195*Power(t2,2) + 
                  80*Power(t2,3)) + 
               Power(t1,2)*(234 - 820*t2 + 1742*Power(t2,2) - 
                  197*Power(t2,3) + 28*Power(t2,4)) + 
               t1*(474 - 2204*t2 + 4715*Power(t2,2) - 
                  3025*Power(t2,3) + 488*Power(t2,4) + 19*Power(t2,5)) \
+ Power(s2,2)*(-23 - 641*t2 + 1792*Power(t2,2) - 108*Power(t2,3) - 
                  423*Power(t2,4) + 
                  2*t1*(31 - 160*t2 + 352*Power(t2,2) + 55*Power(t2,3))\
) + s2*(-173 + 1490*t2 - 2952*Power(t2,2) + 66*Power(t2,3) + 
                  436*Power(t2,4) + 298*Power(t2,5) - 
                  2*Power(t1,2)*
                   (17 - 114*t2 + 424*Power(t2,2) + 43*Power(t2,3)) - 
                  4*t1*(53 - 400*t2 + 975*Power(t2,2) - 
                     276*Power(t2,3) + 31*Power(t2,4)))) + 
            Power(s,2)*(-407 + 854*t2 - 1466*Power(t2,2) + 
               1385*Power(t2,3) - 314*Power(t2,4) - 141*Power(t2,5) + 
               87*Power(t2,6) + 2*Power(t2,7) + 
               Power(s2,3)*t2*
                (42 + 119*t2 - 470*Power(t2,2) + 44*Power(t2,3)) + 
               Power(t1,3)*(-50 + 78*t2 + 269*Power(t2,2) + 
                  133*Power(t2,3) + 10*Power(t2,4)) + 
               Power(t1,2)*(134 - 274*t2 + 629*Power(t2,2) + 
                  709*Power(t2,3) + 141*Power(t2,4) + 16*Power(t2,5)) + 
               t1*(184 + 674*t2 - 216*Power(t2,2) + 1786*Power(t2,3) - 
                  801*Power(t2,4) - 45*Power(t2,5) - 2*Power(t2,6)) + 
               Power(s2,2)*(39 + 336*t2 - 1580*Power(t2,2) + 
                  2842*Power(t2,3) + 21*Power(t2,4) - 
                  283*Power(t2,5) + 
                  2*t1*(-26 + 15*t2 - 15*Power(t2,2) + 
                     383*Power(t2,3) + 78*Power(t2,4))) + 
               s2*(68 - 1354*t2 + 4151*Power(t2,2) - 5192*Power(t2,3) + 
                  535*Power(t2,4) + 111*Power(t2,5) + 101*Power(t2,6) - 
                  Power(t1,2)*
                   (4 - 78*t2 + 472*Power(t2,2) + 533*Power(t2,3) + 
                     114*Power(t2,4)) + 
                  2*t1*(18 - 408*t2 + 514*Power(t2,2) - 
                     1723*Power(t2,3) + 234*Power(t2,4)))) + 
            s*(-(Power(s2,3)*t2*
                  (56 - 59*t2 + 116*Power(t2,2) + 301*Power(t2,3) + 
                    6*Power(t2,4))) + 
               Power(t1,3)*(-56 + 19*t2 + 269*Power(t2,2) + 
                  164*Power(t2,3) + 24*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (129 + 372*t2 + 134*Power(t2,2) - 184*Power(t2,3) + 
                  61*Power(t2,4) + 18*Power(t2,5)) + 
               Power(t1,2)*(232 + 193*t2 + 511*Power(t2,2) + 
                  724*Power(t2,3) + 232*Power(t2,4) + 40*Power(t2,5)) + 
               t1*(-494 - 604*t2 + 289*Power(t2,2) + 884*Power(t2,3) + 
                  49*Power(t2,4) - 106*Power(t2,5) - 18*Power(t2,6)) + 
               Power(s2,2)*(-25 + 245*t2 - 50*Power(t2,2) + 
                  300*Power(t2,3) + 1545*Power(t2,4) - 44*Power(t2,5) - 
                  39*Power(t2,6) + 
                  t1*(24 + 107*t2 + 50*Power(t2,2) + 618*Power(t2,3) + 
                     421*Power(t2,4) + 40*Power(t2,5))) + 
               s2*(37 + 116*t2 + 523*Power(t2,2) + 616*Power(t2,3) - 
                  1228*Power(t2,4) - 96*Power(t2,5) + 26*Power(t2,6) + 
                  6*Power(t2,7) - 
                  Power(t1,2)*
                   (35 - 28*t2 + 404*Power(t2,2) + 626*Power(t2,3) + 
                     203*Power(t2,4) + 20*Power(t2,5)) + 
                  t1*(58 - 322*t2 - 528*Power(t2,2) - 2324*Power(t2,3) - 
                     800*Power(t2,4) + 48*Power(t2,5) + 4*Power(t2,6))))) \
+ Power(s1,5)*(-4 + 7*s2 - 5*t1 - 14*s2*t1 + 22*Power(t1,2) + 
            7*s2*Power(t1,2) - 13*Power(t1,3) - 84*t2 - 63*s2*t2 + 
            20*Power(s2,2)*t2 + 267*t1*t2 + 6*s2*t1*t2 - 
            20*Power(s2,2)*t1*t2 - 182*Power(t1,2)*t2 + 
            57*s2*Power(t1,2)*t2 - Power(t1,3)*t2 + 174*Power(t2,2) - 
            345*s2*Power(t2,2) - 112*Power(s2,2)*Power(t2,2) + 
            10*Power(s2,3)*Power(t2,2) - 33*t1*Power(t2,2) + 
            762*s2*t1*Power(t2,2) - 18*Power(s2,2)*t1*Power(t2,2) - 
            416*Power(t1,2)*Power(t2,2) - 
            187*s2*Power(t1,2)*Power(t2,2) + 
            165*Power(t1,3)*Power(t2,2) + 12*Power(t2,3) + 
            509*s2*Power(t2,3) - 460*Power(s2,2)*Power(t2,3) - 
            30*Power(s2,3)*Power(t2,3) - 593*t1*Power(t2,3) + 
            286*s2*t1*Power(t2,3) + 390*Power(s2,2)*t1*Power(t2,3) + 
            306*Power(t1,2)*Power(t2,3) - 
            565*s2*Power(t1,2)*Power(t2,3) + 
            145*Power(t1,3)*Power(t2,3) - 180*Power(t2,4) + 
            200*s2*Power(t2,4) + 190*Power(s2,2)*Power(t2,4) - 
            180*Power(s2,3)*Power(t2,4) + 250*t1*Power(t2,4) - 
            780*s2*t1*Power(t2,4) + 510*Power(s2,2)*t1*Power(t2,4) + 
            230*Power(t1,2)*Power(t2,4) - 
            280*s2*Power(t1,2)*Power(t2,4) + 40*Power(t1,3)*Power(t2,4) + 
            72*Power(t2,5) - 274*s2*Power(t2,5) + 
            324*Power(s2,2)*Power(t2,5) - 122*Power(s2,3)*Power(t2,5) + 
            106*t1*Power(t2,5) - 244*s2*t1*Power(t2,5) + 
            138*Power(s2,2)*t1*Power(t2,5) + 40*Power(t1,2)*Power(t2,5) - 
            40*s2*Power(t1,2)*Power(t2,5) + 10*Power(t2,6) - 
            34*s2*Power(t2,6) + 38*Power(s2,2)*Power(t2,6) - 
            14*Power(s2,3)*Power(t2,6) + 8*t1*Power(t2,6) - 
            16*s2*t1*Power(t2,6) + 8*Power(s2,2)*t1*Power(t2,6) + 
            Power(s,8)*(4 - 28*t2 + 26*Power(t2,2)) + 
            Power(s,7)*(-12 + Power(t1,2)*(4 - 26*t2) - 41*t2 + 
               230*Power(t2,2) - 88*Power(t2,3) + 
               s2*(-4 + (30 + 8*t1)*t2 - 49*Power(t2,2)) + 
               t1*(17 - 170*t2 + 227*Power(t2,2))) + 
            Power(s,6)*(11 + Power(t1,3)*(17 - 7*t2) + 467*t2 - 
               4*Power(s2,2)*t1*t2 - 1569*Power(t2,2) + 
               1277*Power(t2,3) - 328*Power(t2,4) + 
               Power(t1,2)*(2 - 17*t2 + 60*Power(t2,2)) + 
               t1*(-94 + 652*t2 - 1013*Power(t2,2) + 493*Power(t2,3)) + 
               s2*(27 - 94*t2 + 196*Power(t2,2) - 91*Power(t2,3) + 
                  Power(t1,2)*(-8 + 11*t2) - 
                  2*t1*(6 - 23*t2 + 14*Power(t2,2)))) + 
            Power(s,5)*(54 - 834*t2 + 2383*Power(t2,2) + 
               4*Power(s2,3)*Power(t2,2) - 2321*Power(t2,3) + 
               752*Power(t2,4) - 204*Power(t2,5) + 
               Power(s2,2)*t2*
                (2 + t1*(61 - 45*t2) - 107*t2 + 25*Power(t2,2)) + 
               Power(t1,3)*(-52 + 119*t2 + 75*Power(t2,2)) + 
               Power(t1,2)*(-95 + 809*t2 - 692*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(224 - 386*t2 - 122*Power(t2,2) + 3*Power(t2,3) + 
                  344*Power(t2,4)) + 
               s2*(-81 - 92*t2 + 174*Power(t2,2) + 278*Power(t2,3) - 
                  14*Power(t2,4) + 
                  Power(t1,2)*(31 - 168*t2 + 23*Power(t2,2)) + 
                  t1*(78 - 582*t2 + 564*Power(t2,2) - 104*Power(t2,3)))) \
+ Power(s,4)*(-225 - 11*t2 + 891*Power(t2,2) - 764*Power(t2,3) + 
               420*Power(t2,4) - 165*Power(t2,5) - 16*Power(t2,6) + 
               6*Power(s2,3)*Power(t2,2)*(-5 + 4*t2) + 
               Power(t1,3)*(9 + 5*t2 + 25*Power(t2,2) + 
                  65*Power(t2,3)) - 
               2*Power(t1,2)*
                (-139 + 731*t2 - 1005*Power(t2,2) + 99*Power(t2,3) + 
                  4*Power(t2,4)) + 
               t1*(-27 - 1783*t2 + 5215*Power(t2,2) - 
                  3801*Power(t2,3) + 540*Power(t2,4) + 66*Power(t2,5)) + 
               Power(s2,2)*t2*
                (27 + 853*t2 - 617*Power(t2,2) - 89*Power(t2,3) + 
                  t1*(-206 + 432*t2 - 26*Power(t2,2))) + 
               s2*(135 + 685*t2 - 2612*Power(t2,2) + 1003*Power(t2,3) + 
                  435*Power(t2,4) + 126*Power(t2,5) - 
                  Power(t1,2)*
                   (37 - 299*t2 + 501*Power(t2,2) + 29*Power(t2,3)) - 
                  2*t1*(103 - 653*t2 + 1447*Power(t2,2) - 
                     593*Power(t2,3) + 68*Power(t2,4)))) + 
            Power(s,3)*(327 + 1617*t2 - 5007*Power(t2,2) + 
               5077*Power(t2,3) - 1818*Power(t2,4) + 278*Power(t2,5) + 
               48*Power(t2,6) + 4*Power(t2,7) + 
               2*Power(s2,3)*Power(t2,2)*(-5 - 96*t2 + 33*Power(t2,2)) + 
               Power(t1,3)*(32 - 50*t2 + 86*Power(t2,2) + 
                  48*Power(t2,3)) + 
               2*Power(t1,2)*
                (-87 + 195*t2 - 311*Power(t2,2) + 499*Power(t2,3) + 
                  83*Power(t2,4) + 12*Power(t2,5)) - 
               t1*(541 - 2302*t2 + 5399*Power(t2,2) - 
                  5581*Power(t2,3) + 1882*Power(t2,4) + 
                  29*Power(t2,5) + 8*Power(t2,6)) + 
               Power(s2,2)*t2*
                (-131 - 1382*t2 + 2886*Power(t2,2) - 602*Power(t2,3) - 
                  227*Power(t2,4) + 
                  t1*(312 - 606*t2 + 584*Power(t2,2) + 82*Power(t2,3))) \
- 2*s2*(68 + 403*t2 - 2512*Power(t2,2) + 3400*Power(t2,3) - 
                  715*Power(t2,4) - 144*Power(t2,5) - 42*Power(t2,6) + 
                  Power(t1,2)*
                   (1 + 60*t2 - 146*Power(t2,2) + 208*Power(t2,3) + 
                     53*Power(t2,4)) - 
                  2*t1*(71 - 291*t2 + 838*Power(t2,2) - 
                     1049*Power(t2,3) + 213*Power(t2,4)))) + 
            s*(Power(s2,3)*Power(t2,2)*
                (-114 + 116*t2 - 79*Power(t2,2) - 106*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,3)*(-12 - 61*t2 + 31*Power(t2,2) + 
                  96*Power(t2,3) + 16*Power(t2,4)) + 
               Power(t1,2)*(81 + 491*t2 + 250*Power(t2,2) + 
                  472*Power(t2,3) + 354*Power(t2,4) + 88*Power(t2,5)) + 
               Power(-1 + t2,2)*
                (55 + 214*t2 + 192*Power(t2,2) - 88*Power(t2,3) - 
                  35*Power(t2,4) + 44*Power(t2,5) + 4*Power(t2,6)) - 
               2*t1*(102 + 547*t2 - 262*Power(t2,2) - 301*Power(t2,3) - 
                  94*Power(t2,4) - 15*Power(t2,5) + 20*Power(t2,6) + 
                  3*Power(t2,7)) + 
               Power(s2,2)*t2*
                (-111 + 671*t2 - 14*Power(t2,2) + 581*Power(t2,3) + 
                  674*Power(t2,4) - 59*Power(t2,5) - 6*Power(t2,6) + 
                  t1*(107 + 87*t2 - 146*Power(t2,2) + 276*Power(t2,3) + 
                     106*Power(t2,4) + 4*Power(t2,5))) - 
               s2*(35 - 368*t2 - 185*Power(t2,2) - 354*Power(t2,3) + 
                  205*Power(t2,4) + 640*Power(t2,5) + 29*Power(t2,6) - 
                  2*Power(t2,7) + 
                  Power(t1,2)*
                   (29 + 96*t2 - 165*Power(t2,2) + 128*Power(t2,3) + 
                     186*Power(t2,4) + 48*Power(t2,5)) + 
                  2*t1*(-43 + 43*t2 + 606*Power(t2,2) + 
                     434*Power(t2,3) + 646*Power(t2,4) + 
                     66*Power(t2,5) - 16*Power(t2,6)))) + 
            Power(s,2)*(-210 - 1238*t2 + 3451*Power(t2,2) - 
               3124*Power(t2,3) + 1288*Power(t2,4) - 234*Power(t2,5) + 
               3*Power(t2,6) + 64*Power(t2,7) + 
               Power(s2,3)*Power(t2,2)*
                (140 + 46*t2 - 301*Power(t2,2) + 40*Power(t2,3)) + 
               Power(t1,3)*(19 - 5*t2 + 66*Power(t2,2) + 
                  62*Power(t2,3) + 8*Power(t2,4)) + 
               Power(t1,2)*(-118 - 99*t2 - 334*Power(t2,2) + 
                  592*Power(t2,3) + 330*Power(t2,4) + 72*Power(t2,5)) + 
               t1*(630 + 564*t2 + 689*Power(t2,2) - 511*Power(t2,3) + 
                  946*Power(t2,4) - 194*Power(t2,5) - 80*Power(t2,6)) + 
               Power(s2,2)*t2*
                (193 + 221*t2 - 1778*Power(t2,2) + 2398*Power(t2,3) - 
                  226*Power(t2,4) - 89*Power(t2,5) + 
                  2*t1*(-125 + 75*t2 - 53*Power(t2,2) + 
                     182*Power(t2,3) + 21*Power(t2,4))) + 
               s2*(87 - 28*t2 - 3121*Power(t2,2) + 5755*Power(t2,3) - 
                  5581*Power(t2,4) + 770*Power(t2,5) + 62*Power(t2,6) + 
                  12*Power(t2,7) + 
                  Power(t1,2)*
                   (38 + 17*t2 + 112*Power(t2,2) - 238*Power(t2,3) - 
                     164*Power(t2,4) - 40*Power(t2,5)) + 
                  2*t1*(-108 + 233*t2 - 576*Power(t2,2) + 
                     676*Power(t2,3) - 930*Power(t2,4) + 
                     116*Power(t2,5) + 8*Power(t2,6))))))*R1q(s1))/
     (s*(-1 + s1)*s1*Power(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2),2)*(-1 + s2)*(-s + s2 - t1)*Power(s1 - t2,3)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) - 
    (8*(8*Power(s,6)*(-1 + t2)*Power(t2,4)*
          (Power(s1,3) - 3*Power(s1,2)*t2 + 
            t2*(-1 + 2*t2 - 2*Power(t2,2)) + 
            s1*(1 - 2*t2 + 4*Power(t2,2))) - 
         Power(s1 - t2,2)*Power(-1 + t2,2)*Power(t2,2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          (-3 + 3*t1 + 2*Power(s1,3)*(-1 + t2) + 18*t2 - s2*t2 - 
            7*t1*t2 - 15*Power(t2,2) + 7*s2*Power(t2,2) - 
            2*t1*Power(t2,2) + 
            Power(s1,2)*(t1*(-7 + t2) - 2*(-1 + t2)*t2 + 
               3*s2*(1 + t2)) + 
            s1*(s2*(1 - 10*t2 - 3*Power(t2,2)) - 
               3*(5 - 6*t2 + Power(t2,2)) + 2*t1*(2 + 3*t2 + Power(t2,2))\
)) - Power(s,5)*Power(t2,3)*(4*Power(s1,4)*(5 - 12*t2 + 7*Power(t2,2)) + 
            Power(s1,3)*(8 - (97 + 16*s2)*t2 + 
               2*(91 + 8*s2)*Power(t2,2) - 105*Power(t2,3) + 
               t1*(9 + 10*t2 - 7*Power(t2,2))) + 
            Power(s1,2)*(20 - 96*t2 + 3*(89 + 16*s2)*Power(t2,2) - 
               2*(157 + 24*s2)*Power(t2,3) + 159*Power(t2,4) + 
               t1*(8 - 51*t2 - 6*Power(t2,2) + 13*Power(t2,3))) + 
            t2*(-12 + (55 + 8*s2)*t2 - 6*(19 + 4*s2)*Power(t2,2) + 
               5*(29 + 8*s2)*Power(t2,3) - 24*(4 + s2)*Power(t2,4) + 
               34*Power(t2,5) + 
               t1*(5 - 22*t2 + 27*Power(t2,2) - 36*Power(t2,3) + 
                  14*Power(t2,4))) + 
            s1*(t1*(1 + 2*t2 + 15*Power(t2,2) + 44*Power(t2,3) - 
                  26*Power(t2,4)) + 
               t2*(-45 + 190*t2 - 359*Power(t2,2) + 300*Power(t2,3) - 
                  122*Power(t2,4) + 
                  8*s2*(-1 + 3*t2 - 9*Power(t2,2) + 7*Power(t2,3))))) - 
         Power(s,4)*Power(t2,2)*
          (-4*Power(s1,5)*Power(-1 + t2,2)*(-4 + 9*t2) + 
            Power(s1,4)*(14 - (131 + 31*s2)*t2 + 
               (340 + 82*s2)*Power(t2,2) - (385 + 39*s2)*Power(t2,3) + 
               162*Power(t2,4) + 
               t1*(17 - t2 - 47*Power(t2,2) + 19*Power(t2,3))) + 
            Power(s1,3)*(Power(t1,2)*
                (2 + 8*t2 + 18*Power(t2,2) - 4*Power(t2,3)) - 
               4*t1*(-3 + 3*(10 + s2)*t2 + (-14 + 5*s2)*Power(t2,2) - 
                  2*(13 + s2)*Power(t2,3) + 13*Power(t2,4)) - 
               (-1 + t2)*(14 - (107 + 4*s2)*t2 + 
                  (351 + 138*s2 + 8*Power(s2,2))*Power(t2,2) - 
                  (401 + 146*s2)*Power(t2,3) + 287*Power(t2,4))) + 
            s1*t2*(-27 + 6*(19 + 11*s2)*t2 - 
               4*(79 + 60*s2)*Power(t2,2) + 
               (634 + 466*s2 + 24*Power(s2,2))*Power(t2,3) - 
               (685 + 352*s2 + 24*Power(s2,2))*Power(t2,4) + 
               12*(35 + 13*s2)*Power(t2,5) - 140*Power(t2,6) + 
               Power(t1,2)*(8 - 18*t2 + 48*Power(t2,2) + 
                  46*Power(t2,3) - 12*Power(t2,4)) - 
               4*t1*(-1 + 19*t2 + (-38 + 9*s2)*Power(t2,2) + 
                  3*(28 + 5*s2)*Power(t2,3) - 
                  3*(19 + 2*s2)*Power(t2,4) + 17*Power(t2,5))) + 
            Power(t2,2)*(57 - (145 + 47*s2)*t2 + 
               40*(3 + 4*s2)*Power(t2,2) - 
               (77 + 235*s2 + 8*Power(s2,2))*Power(t2,3) + 
               (97 + 130*s2 + 8*Power(s2,2))*Power(t2,4) - 
               2*(39 + 22*s2)*Power(t2,5) + 26*Power(t2,6) + 
               2*Power(t1,2)*
                (3 - 10*t2 + 5*Power(t2,2) - 12*Power(t2,3) + 
                  2*Power(t2,4)) + 
               t1*(-47 + (161 + 6*s2)*t2 - (191 + 6*s2)*Power(t2,2) + 
                  (159 + 38*s2)*Power(t2,3) - 
                  2*(32 + 7*s2)*Power(t2,4) + 18*Power(t2,5))) + 
            Power(s1,2)*(-2*Power(t1,2)*
                (1 - 6*t2 + 33*Power(t2,2) + 8*Power(t2,3)) + 
               t1*(1 - (7 + 6*s2)*t2 + 2*(65 + 27*s2)*Power(t2,2) + 
                  (38 + 42*s2)*Power(t2,3) - 
                  (167 + 18*s2)*Power(t2,4) + 77*Power(t2,5)) + 
               t2*(-49 + 315*t2 - 840*Power(t2,2) + 
                  24*Power(s2,2)*(-1 + t2)*Power(t2,2) + 
                  1026*Power(t2,3) - 727*Power(t2,4) + 275*Power(t2,5) + 
                  s2*(-19 + 84*t2 - 342*Power(t2,2) + 424*Power(t2,3) - 
                     219*Power(t2,4))))) + 
         s*(s1 - t2)*(-1 + t2)*
          (Power(s1,6)*Power(-1 + t2,2)*
             (2 + t1 - 8*t2 - 5*t1*t2 + 6*Power(t2,2) - 
               2*t1*Power(t2,2) + s2*(-1 + t2 + 6*Power(t2,2))) + 
            s1*Power(t2,2)*(-12 + (-197 + 82*s2)*t2 + 
               (882 - 470*s2 + 51*Power(s2,2))*Power(t2,2) - 
               (1298 - 699*s2 + 197*Power(s2,2) + 17*Power(s2,3))*
                Power(t2,3) + 
               (724 - 300*s2 + 202*Power(s2,2) - 64*Power(s2,3))*
                Power(t2,4) - 
               (-3 + 24*s2 + 65*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,5) + 
               (-130 + 10*s2 + 9*Power(s2,2))*Power(t2,6) + 
               (28 + 3*s2)*Power(t2,7) + 
               Power(t1,3)*(3 - 21*t2 + 56*Power(t2,2) + 
                  40*Power(t2,3) + 9*Power(t2,4) + Power(t2,5)) - 
               Power(t1,2)*(20 - (41 + 22*s2)*t2 + 
                  2*(55 + 23*s2)*Power(t2,2) + 
                  (-143 + 191*s2)*Power(t2,3) + 
                  14*(5 + 3*s2)*Power(t2,4) + 
                  (-18 + 7*s2)*Power(t2,5) + 2*Power(t2,6)) + 
               t1*(41 - (109 + 38*s2)*t2 + 
                  (265 + 176*s2 - 24*Power(s2,2))*Power(t2,2) + 
                  (-323 - 118*s2 + 174*Power(s2,2))*Power(t2,3) + 
                  (53 - 48*s2 + 108*Power(s2,2))*Power(t2,4) + 
                  (103 + 32*s2 + 6*Power(s2,2))*Power(t2,5) - 
                  (31 + 4*s2)*Power(t2,6) + Power(t2,7))) + 
            Power(s1,3)*(1 + (-30 + s2)*t2 + 
               (182 - 66*s2 + 15*Power(s2,2))*Power(t2,2) - 
               (372 - 42*s2 + 49*Power(s2,2) + 19*Power(s2,3))*
                Power(t2,3) + 
               (313 + 222*s2 + 22*Power(s2,2) - 40*Power(s2,3))*
                Power(t2,4) - 
               (98 + 313*s2 + 47*Power(s2,2) + 29*Power(s2,3))*
                Power(t2,5) + 
               (8 + 116*s2 + 59*Power(s2,2))*Power(t2,6) - 
               2*(2 + s2)*Power(t2,7) + 
               Power(t1,3)*(-1 + 3*t2 + 4*Power(t2,2) + 
                  60*Power(t2,3) + 17*Power(t2,4) + 5*Power(t2,5)) + 
               Power(t1,2)*(1 + (-28 + 5*s2)*t2 + 
                  (133 - 30*s2)*Power(t2,2) - 
                  (217 + 87*s2)*Power(t2,3) + 
                  (59 - 146*s2)*Power(t2,4) + 
                  (45 - 6*s2)*Power(t2,5) + 7*Power(t2,6)) + 
               t1*(3 + (-67 + 16*s2)*t2 + 
                  2*(187 - 57*s2 + 5*Power(s2,2))*Power(t2,2) + 
                  (-644 + 202*s2 + 56*Power(s2,2))*Power(t2,3) + 
                  (343 + 12*s2 + 170*Power(s2,2))*Power(t2,4) + 
                  (77 - 74*s2 + 28*Power(s2,2))*Power(t2,5) - 
                  6*(16 + 7*s2)*Power(t2,6) + 10*Power(t2,7))) + 
            Power(s1,5)*(-1 + t2)*
             (-3 + (19 - 5*s2 + 4*Power(s2,2))*t2 + 
               (-53 - 11*s2 + 10*Power(s2,2))*Power(t2,2) + 
               (61 + 33*s2 + 16*Power(s2,2))*Power(t2,3) - 
               (24 + 17*s2)*Power(t2,4) + 
               2*Power(t1,2)*
                (-1 + 7*t2 + 8*Power(t2,2) + Power(t2,3)) + 
               t1*(-5 + 30*t2 - 36*Power(t2,2) + 6*Power(t2,3) + 
                  5*Power(t2,4) - 
                  2*s2*(-1 + 5*t2 + 21*Power(t2,2) + 5*Power(t2,3)))) - 
            Power(t2,3)*(Power(t1,3)*
                (-3 + 4*t2 + 9*Power(t2,2) + 11*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*(6 + (-41 + 14*s2)*t2 - 
                  32*(-1 + s2)*Power(t2,2) + 
                  (18 - 40*s2)*Power(t2,3) - (16 + 9*s2)*Power(t2,4) + 
                  (1 + s2)*Power(t2,5)) + 
               t1*(1 + (70 - 36*s2)*t2 + 
                  (-174 + 160*s2 - 13*Power(s2,2))*Power(t2,2) + 
                  (175 - 178*s2 + 63*Power(s2,2))*Power(t2,3) + 
                  2*(-61 + 27*s2 + 9*Power(s2,2))*Power(t2,4) + 
                  (59 + 2*s2 - 2*Power(s2,2))*Power(t2,5) - 
                  (9 + 2*s2)*Power(t2,6)) + 
               t2*(Power(s2,3)*Power(t2,2)*(-4 - 19*t2 + Power(t2,2)) + 
                  Power(-1 + t2,3)*
                   (125 - 85*t2 - 9*Power(t2,2) + 7*Power(t2,3)) + 
                  2*Power(s2,2)*t2*
                   (10 - 37*t2 + 36*Power(t2,2) - 10*Power(t2,3) + 
                     Power(t2,4)) + 
                  s2*Power(-1 + t2,2)*
                   (44 - 142*t2 + 22*Power(t2,2) + Power(t2,3) + 
                     Power(t2,4)))) + 
            Power(s1,4)*(Power(s2,3)*Power(t2,2)*
                (5 + 7*t2 + 10*Power(t2,2)) + 
               Power(-1 + t2,3)*
                (-2 + 30*t2 - 23*Power(t2,2) + 33*Power(t2,3)) + 
               Power(t1,3)*(1 - 9*t2 + 5*Power(t2,2) - 
                  22*Power(t2,3) + 3*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (-5 + 61*t2 - 71*Power(t2,2) - 17*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(t1,2)*(2 - 26*t2 + 66*Power(t2,2) + 
                  19*Power(t2,3) - 58*Power(t2,4) - 3*Power(t2,5)) - 
               Power(s2,2)*t2*
                (2 - 6*t2 - 27*Power(t2,2) - 20*Power(t2,3) + 
                  51*Power(t2,4) + 
                  t1*(4 + 4*t2 + 49*Power(t2,2) + 9*Power(t2,3))) + 
               s2*(Power(t1,2)*
                   (-1 + 9*t2 + 6*Power(t2,2) + 52*Power(t2,3)) + 
                  6*t1*t2*(3 - 6*t2 - 19*Power(t2,2) + 
                     16*Power(t2,3) + 6*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (1 + 15*t2 + 20*Power(t2,2) - 75*Power(t2,3) + 
                     15*Power(t2,4)))) - 
            Power(s1,2)*t2*(Power(t1,3)*
                (-6 + 12*t2 + 20*Power(t2,2) + 72*Power(t2,3) + 
                  36*Power(t2,4) - 2*Power(t2,5)) + 
               Power(t1,2)*(8 + (-43 + 12*s2)*t2 + 
                  (33 - 35*s2)*Power(t2,2) - 
                  8*(-3 + 29*s2)*Power(t2,3) - 
                  (77 + 127*s2)*Power(t2,4) + 
                  (51 - 14*s2)*Power(t2,5) + 4*Power(t2,6)) + 
               t1*(10 + 2*(-71 + 7*s2)*t2 + 
                  (681 - 82*s2 - 5*Power(s2,2))*Power(t2,2) + 
                  (-1044 + 238*s2 + 163*Power(s2,2))*Power(t2,3) + 
                  (490 - 172*s2 + 211*Power(s2,2))*Power(t2,4) + 
                  (84 + 20*s2 + 27*Power(s2,2))*Power(t2,5) - 
                  (85 + 18*s2)*Power(t2,6) + 6*Power(t2,7)) + 
               t2*(-3*Power(s2,3)*Power(t2,2)*
                   (9 + 26*t2 + 9*Power(t2,2)) + 
                  Power(-1 + t2,3)*
                   (133 - 194*t2 - 53*Power(t2,2) + 32*Power(t2,3)) + 
                  s2*Power(-1 + t2,2)*
                   (40 - 213*t2 - 80*Power(t2,2) + 69*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  Power(s2,2)*t2*
                   (44 - 170*t2 + 173*Power(t2,2) - 78*Power(t2,3) + 
                     31*Power(t2,4))))) + 
         Power(s,2)*(4*Power(s1,7)*Power(-1 + t2,4)*t2 - 
            Power(s1,6)*Power(-1 + t2,2)*
             (-2 - (-3 + s2)*t2 + (-2 + 20*s2)*Power(t2,2) + 
               (-23 + 5*s2)*Power(t2,3) + 24*Power(t2,4) + 
               t1*(-1 - t2 - 23*Power(t2,2) + Power(t2,3))) + 
            Power(s1,5)*(-1 + t2)*
             (-2 + (19 - 4*s2)*t2 + 
               (-49 + 34*s2 - 4*Power(s2,2))*Power(t2,2) - 
               2*(-54 + 37*s2 + 15*Power(s2,2))*Power(t2,3) - 
               2*(38 - 21*s2 + 9*Power(s2,2))*Power(t2,4) + 
               (-59 + 2*s2)*Power(t2,5) + 59*Power(t2,6) + 
               2*Power(t1,2)*
                (-1 + 8*t2 - 9*Power(t2,2) - 27*Power(t2,3) + 
                  3*Power(t2,4)) + 
               4*t1*(-1 - 2*(-3 + s2)*t2 - 21*Power(t2,2) + 
                  4*(9 + 7*s2)*Power(t2,3) - 23*Power(t2,4) + 
                  3*Power(t2,5))) + 
            Power(s1,3)*t2*(1 + (116 - 42*s2)*t2 + 
               (-653 + 320*s2 - 17*Power(s2,2))*Power(t2,2) + 
               (1271 - 641*s2 + 30*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,3) + 
               (-1139 + 274*s2 + 140*Power(s2,2) + 16*Power(s2,3))*
                Power(t2,4) + 
               (496 + 206*s2 - 86*Power(s2,2) + 22*Power(s2,3))*
                Power(t2,5) - 
               (33 + 18*s2 + 67*Power(s2,2))*Power(t2,6) - 
               3*(41 + 33*s2)*Power(t2,7) + 64*Power(t2,8) - 
               2*Power(t1,3)*
                (2 - 14*t2 - 9*Power(t2,2) + 32*Power(t2,3) + 
                  11*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,2)*(-11 + (143 - 19*s2)*t2 - 
                  10*(48 + 5*s2)*Power(t2,2) + 
                  (562 + 54*s2)*Power(t2,3) + 
                  (17 + 162*s2)*Power(t2,4) - 
                  (233 + 3*s2)*Power(t2,5) + 2*Power(t2,6)) + 
               2*t1*(-2 - 6*(-17 + 5*s2)*t2 + 
                  (-494 + 159*s2 + 13*Power(s2,2))*Power(t2,2) + 
                  2*(472 - 103*s2 + Power(s2,2))*Power(t2,3) - 
                  (734 + 144*s2 + 81*Power(s2,2))*Power(t2,4) - 
                  6*(-19 - 34*s2 + Power(s2,2))*Power(t2,5) + 
                  (70 + 17*s2)*Power(t2,6))) - 
            Power(t2,4)*(54 + (58 - 120*s2)*t2 + 
               (-782 + 490*s2 - 5*Power(s2,2))*Power(t2,2) + 
               (1491 - 754*s2 + 39*Power(s2,2) + Power(s2,3))*
                Power(t2,3) + 
               (-1155 + 525*s2 - 52*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,4) + 
               (334 - 153*s2 + 13*Power(s2,2) + Power(s2,3))*
                Power(t2,5) + 
               (18 + 17*s2 + 5*Power(s2,2))*Power(t2,6) - 
               (19 + 5*s2)*Power(t2,7) + Power(t2,8) - 
               Power(t1,3)*(3 - 13*t2 + 12*Power(t2,2) + 
                  2*Power(t2,3) + 7*Power(t2,4) + Power(t2,5)) + 
               Power(t1,2)*(21 - (76 + 7*s2)*t2 + 
                  (125 + s2)*Power(t2,2) + 
                  3*(-35 + 6*s2)*Power(t2,3) + 
                  (41 + 21*s2)*Power(t2,4) + (-7 + 3*s2)*Power(t2,5) + 
                  Power(t2,6)) - 
               t1*(78 - 16*(16 + 3*s2)*t2 + 
                  (258 + 200*s2 - 11*Power(s2,2))*Power(t2,2) + 
                  (12 - 266*s2 + 23*Power(s2,2))*Power(t2,3) + 
                  (-197 + 144*s2 + 21*Power(s2,2))*Power(t2,4) + 
                  (139 - 38*s2 + 3*Power(s2,2))*Power(t2,5) + 
                  (-35 + 8*s2)*Power(t2,6) + Power(t2,7))) + 
            s1*Power(t2,3)*(98 + (198 - 272*s2)*t2 + 
               (-1905 + 1184*s2 - 19*Power(s2,2))*Power(t2,2) + 
               (3599 - 1885*s2 + 110*Power(s2,2) + 6*Power(s2,3))*
                Power(t2,3) + 
               (-2743 + 1262*s2 - 118*Power(s2,2) + 32*Power(s2,3))*
                Power(t2,4) + 
               2*(324 - 140*s2 + 9*Power(s2,2) + 5*Power(s2,3))*
                Power(t2,5) + 
               (197 + 26*s2 + 9*Power(s2,2))*Power(t2,6) - 
               (101 + 35*s2)*Power(t2,7) + 9*Power(t2,8) + 
               Power(t1,3)*(-6 + 24*t2 + 6*Power(t2,2) - 
                  36*Power(t2,3) - 40*Power(t2,4) + 4*Power(t2,5)) + 
               Power(t1,2)*(20 - (63 + 11*s2)*t2 + 
                  (161 - 58*s2)*Power(t2,2) + 
                  2*(-140 + 57*s2)*Power(t2,3) + 
                  (266 + 90*s2)*Power(t2,4) + 
                  3*(-35 + 3*s2)*Power(t2,5) + Power(t2,6)) + 
               2*t1*(-67 + (333 + 16*s2)*t2 + 
                  (-688 - 83*s2 + 19*Power(s2,2))*Power(t2,2) + 
                  (703 + 114*s2 - 30*Power(s2,2))*Power(t2,3) - 
                  (287 + 84*s2 + 55*Power(s2,2))*Power(t2,4) + 
                  (-43 + 42*s2 - 6*Power(s2,2))*Power(t2,5) + 
                  (46 - 5*s2)*Power(t2,6) + 3*Power(t2,7))) - 
            Power(s1,2)*Power(t2,2)*
             (39 + (259 - 193*s2)*t2 - 
               3*(558 - 322*s2 + 9*Power(s2,2))*Power(t2,2) + 
               (3089 - 1634*s2 + 100*Power(s2,2) + 12*Power(s2,3))*
                Power(t2,3) + 
               (-2331 + 952*s2 - 16*Power(s2,2) + 36*Power(s2,3))*
                Power(t2,4) + 
               (463 + 11*s2 - 36*Power(s2,2) + 24*Power(s2,3))*
                Power(t2,5) + 
               (286 - 14*s2 - 21*Power(s2,2))*Power(t2,6) - 
               (163 + 88*s2)*Power(t2,7) + 32*Power(t2,8) + 
               2*Power(t1,3)*t2*
                (4 + 31*t2 - 48*Power(t2,2) - 25*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,2)*(-28 + (181 - 16*s2)*t2 - 
                  6*(57 + 17*s2)*Power(t2,2) + 
                  2*(73 + 75*s2)*Power(t2,3) + 
                  2*(137 + 89*s2)*Power(t2,4) + 
                  3*(-77 + 2*s2)*Power(t2,5)) + 
               t1*(-41 + (529 - 70*s2)*t2 + 
                  6*(-314 + 47*s2 + 8*Power(s2,2))*Power(t2,2) + 
                  (2882 - 368*s2 - 42*Power(s2,2))*Power(t2,3) - 
                  3*(617 + 20*s2 + 68*Power(s2,2))*Power(t2,4) + 
                  (199 + 198*s2 - 18*Power(s2,2))*Power(t2,5) + 
                  2*(80 + 9*s2)*Power(t2,6) + 6*Power(t2,7))) + 
            Power(s1,4)*(Power(t1,3)*
                (1 - 13*t2 + 14*Power(t2,2) - 10*Power(t2,3) + 
                  23*Power(t2,4) - 3*Power(t2,5)) + 
               Power(t1,2)*(2 + (-31 + 7*s2)*t2 + 
                  (156 + 7*s2)*Power(t2,2) - 239*Power(t2,3) - 
                  (58 + 53*s2)*Power(t2,4) + (178 + 3*s2)*Power(t2,5) - 
                  8*Power(t2,6)) + 
               t1*(1 + (-35 + 6*s2)*t2 + 
                  (213 - 78*s2 - 5*Power(s2,2))*Power(t2,2) - 
                  9*(63 - 10*s2 + Power(s2,2))*Power(t2,3) + 
                  (648 + 364*s2 + 47*Power(s2,2))*Power(t2,4) + 
                  (-290 - 368*s2 + 3*Power(s2,2))*Power(t2,5) - 
                  14*(-3 + s2)*Power(t2,6) - 12*Power(t2,7)) + 
               t2*(-(Power(s2,3)*Power(t2,2)*
                     (3 + 2*t2 + 7*Power(t2,2))) + 
                  s2*Power(-1 + t2,2)*
                   (1 - 50*t2 + 74*Power(t2,2) + 53*Power(t2,3) + 
                     44*Power(t2,4)) + 
                  Power(s2,2)*t2*
                   (4 - 5*t2 - 116*Power(t2,2) + 57*Power(t2,3) + 
                     60*Power(t2,4)) - 
                  Power(-1 + t2,3)*
                   (-17 + 82*t2 - 42*Power(t2,2) + 132*Power(t2,3) + 
                     79*Power(t2,4))))) + 
         Power(s,3)*t2*(-4*Power(s1,6)*Power(-1 + t2,3)*(-1 + 5*t2) + 
            Power(s1,5)*(-1 + t2)*
             (4 - (49 + 15*s2)*t2 + (139 + 69*s2)*Power(t2,2) - 
               (199 + 24*s2)*Power(t2,3) + 105*Power(t2,4) + 
               t1*(7 - 4*t2 - 48*Power(t2,2) + 15*Power(t2,3))) - 
            Power(t2,3)*(-92 + (129 + 110*s2)*t2 + 
               (283 - 413*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (-667 + 568*s2 + 17*Power(s2,2))*Power(t2,3) + 
               (412 - 333*s2 - 16*Power(s2,2))*Power(t2,4) + 
               (-41 + 98*s2 + 9*Power(s2,2))*Power(t2,5) - 
               (31 + 30*s2)*Power(t2,6) + 7*Power(t2,7) + 
               Power(t1,3)*(3 - 7*t2 - 2*Power(t2,2) - 7*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(t1,2)*(24 - 87*t2 + (89 - 10*s2)*Power(t2,2) - 
                  8*(9 + 2*s2)*Power(t2,3) + (33 + 2*s2)*Power(t2,4) + 
                  Power(t2,5)) + 
               t1*(110 - (367 + 32*s2)*t2 + 
                  (442 + 74*s2 - 3*Power(s2,2))*Power(t2,2) - 
                  2*(116 + 68*s2 + 5*Power(s2,2))*Power(t2,3) + 
                  (31 + 82*s2 + Power(s2,2))*Power(t2,4) + 
                  (7 - 12*s2)*Power(t2,5) + 9*Power(t2,6))) + 
            s1*Power(t2,2)*(-97 + (88 + 197*s2)*t2 + 
               (478 - 802*s2 + 6*Power(s2,2))*Power(t2,2) + 
               (-825 + 1247*s2 + 59*Power(s2,2))*Power(t2,3) + 
               (248 - 838*s2 - 84*Power(s2,2))*Power(t2,4) + 
               (269 + 332*s2 + 31*Power(s2,2))*Power(t2,5) - 
               17*(13 + 8*s2)*Power(t2,6) + 60*Power(t2,7) + 
               Power(t1,3)*t2*
                (5 - 30*t2 - 15*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(20 - 2*(13 + 2*s2)*t2 + 
                  (55 + 42*s2)*Power(t2,2) + 
                  2*(59 + 18*s2)*Power(t2,3) - 
                  (169 + 2*s2)*Power(t2,4) + 14*Power(t2,5)) + 
               t1*(79 - (445 + 12*s2)*t2 + 
                  (947 - 68*s2 - 9*Power(s2,2))*Power(t2,2) - 
                  6*(180 + 19*s2 + 5*Power(s2,2))*Power(t2,3) + 
                  (576 + 200*s2 + 3*Power(s2,2))*Power(t2,4) - 
                  3*(41 + 10*s2)*Power(t2,5) + 46*Power(t2,6))) + 
            Power(s1,4)*t2*(-4*Power(s2,2)*t2*(2 - 9*t2 + Power(t2,2)) - 
               2*Power(t1,2)*
                (10 + 3*t2 - 30*Power(t2,2) + 5*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-31 + 150*t2 - 134*Power(t2,2) + 219*Power(t2,3)) + 
               t1*(46 - 19*t2 - 176*Power(t2,2) + 207*Power(t2,3) - 
                  58*Power(t2,4)) + 
               s2*(8 - 126*t2 + 365*Power(t2,2) - 366*Power(t2,3) + 
                  119*Power(t2,4) + 
                  2*t1*(9 + 9*t2 - 49*Power(t2,2) + 7*Power(t2,3)))) + 
            Power(s1,2)*t2*(17 + (26 - 100*s2)*t2 + 
               (-171 + 481*s2 - 6*Power(s2,2))*Power(t2,2) - 
               (65 + 994*s2 + 75*Power(s2,2))*Power(t2,3) + 
               2*(300 + 455*s2 + 78*Power(s2,2))*Power(t2,4) - 
               (721 + 550*s2 + 39*Power(s2,2))*Power(t2,5) + 
               (482 + 253*s2)*Power(t2,6) - 168*Power(t2,7) + 
               Power(t1,3)*(-6 + 7*t2 + 30*Power(t2,2) + 
                  3*Power(t2,3) + 2*Power(t2,4)) + 
               Power(t1,2)*(-24 + 2*(51 + 2*s2)*t2 - 
                  21*(15 + 2*s2)*Power(t2,2) - 
                  6*(-19 + 6*s2)*Power(t2,3) + 
                  (177 + 2*s2)*Power(t2,4) - 18*Power(t2,5)) + 
               t1*t2*(147 - 507*t2 + 929*Power(t2,2) - 707*Power(t2,3) + 
                  208*Power(t2,4) - 70*Power(t2,5) - 
                  3*Power(s2,2)*t2*(-3 - 10*t2 + Power(t2,2)) + 
                  s2*(-32 + 256*t2 - 42*Power(t2,2) - 292*Power(t2,3) + 
                     38*Power(t2,4)))) + 
            Power(s1,3)*(Power(t1,3)*
                (3 - 7*t2 - 2*Power(t2,2) - 7*Power(t2,3) + Power(t2,4)) \
+ Power(t1,2)*(4 - 31*t2 + (179 + 10*s2)*Power(t2,2) + 
                  2*(-53 + 8*s2)*Power(t2,3) - 
                  (113 + 2*s2)*Power(t2,4) + 7*Power(t2,5)) + 
               t1*(1 + 3*(-7 + 4*s2)*t2 - 
                  (13 + 132*s2 + 3*Power(s2,2))*Power(t2,2) + 
                  (-133 + 2*s2 - 10*Power(s2,2))*Power(t2,3) + 
                  (300 + 272*s2 + Power(s2,2))*Power(t2,4) - 
                  2*(105 + 17*s2)*Power(t2,5) + 76*Power(t2,6)) + 
               t2*(Power(s2,2)*t2*
                   (2 + 41*t2 - 124*Power(t2,2) + 21*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (-3 - 69*t2 + 275*Power(t2,2) - 126*Power(t2,3) + 
                     249*Power(t2,4)) + 
                  s2*(13 - 100*t2 + 426*Power(t2,2) - 686*Power(t2,3) + 
                     589*Power(t2,4) - 242*Power(t2,5))))))*R1q(t2))/
     (s*(-1 + s1)*(-1 + s2)*(-s + s2 - t1)*Power(s1 - t2,3)*
       Power(-1 + t2,3)*t2*Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) - 
    (8*(12*Power(s,9)*Power(t2,2) + 
         2*Power(s,8)*t2*(2*Power(t1,2) + s1*(12 - 36*t2) + 8*t1*t2 + 
            t2*(-37 - 9*s2 + 10*t2)) + 
         Power(s,7)*(Power(t1,3)*(-2 + t2) + 
            Power(t1,2)*t2*(-24 - 2*s2 + 17*t2) + 
            Power(t2,2)*(146 + 132*s2 + 6*Power(s2,2) - 84*t2 - 
               25*s2*t2 + 11*Power(t2,2)) + 
            12*Power(s1,2)*(1 - 10*t2 + 15*Power(t2,2)) + 
            t1*t2*(8 - 110*t2 - 17*s2*t2 + 19*Power(t2,2)) + 
            s1*(Power(t1,2)*(4 - 26*t2) + t1*(24 + 8*s2 - 95*t2)*t2 + 
               t2*(-124 - 36*s2 + 320*t2 + 80*s2*t2 - 103*Power(t2,2)))) \
+ Power(s,6)*(Power(t1,3)*(10 - 9*t2 + 5*Power(t2,2)) - 
            48*Power(s1,3)*(1 - 5*t2 + 5*Power(t2,2)) + 
            Power(t1,2)*(-6 + (41 + 16*s2)*t2 - 
               (103 + 9*s2)*Power(t2,2) + 7*Power(t2,3)) + 
            t2*(4 + (24 - 347*s2 - 56*Power(s2,2))*t2 + 
               (59 + 147*s2 + 2*Power(s2,2))*Power(t2,2) - 
               (19 + 15*s2)*Power(t2,3) + Power(t2,4)) + 
            t1*t2*(-46 + 298*t2 + Power(s2,2)*t2 - 111*Power(t2,2) + 
               11*Power(t2,3) + s2*(2 + 129*t2 - 10*Power(t2,2))) + 
            Power(s1,2)*(-50 + 364*t2 + 4*Power(s2,2)*t2 - 
               522*Power(t2,2) + 215*Power(t2,3) + 
               Power(t1,2)*(-22 + 70*t2) + 
               s2*(-18 + t1*(8 - 44*t2) + 116*t2 - 135*Power(t2,2)) + 
               2*t1*(4 - 61*t2 + 117*Power(t2,2))) + 
            s1*(Power(t1,3)*(17 - 7*t2) + 
               t1*(8 - 2*(93 + 34*s2 + 2*Power(s2,2))*t2 + 
                  (442 + 93*s2)*Power(t2,2) - 99*Power(t2,3)) + 
               Power(t1,2)*(-14 + 119*t2 - 94*Power(t2,2) + 
                  s2*(-8 + 11*t2)) + 
               t2*(160 + Power(s2,2)*(12 - 5*t2) - 359*t2 + 
                  210*Power(t2,2) - 48*Power(t2,3) + 
                  s2*(236 - 434*t2 + 94*Power(t2,2))))) + 
         Power(s,5)*(12*Power(s1,4)*(6 - 20*t2 + 15*Power(t2,2)) + 
            Power(t1,3)*(-18 + 25*t2 - 24*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,2)*(24 + (20 - 54*s2)*t2 + 
               8*(25 + 7*s2)*Power(t2,2) + (-52 + s2)*Power(t2,3)) + 
            t1*(-6 + (63 + 10*s2)*t2 - 
               (360 + 339*s2 + 14*Power(s2,2))*Power(t2,2) + 
               (275 + 90*s2 - 9*Power(s2,2))*Power(t2,3) - 
               (52 + 7*s2)*Power(t2,4) + 2*Power(t2,5)) + 
            t2*(-22 - 443*t2 + 278*Power(t2,2) - 62*Power(t2,3) + 
               8*Power(t2,4) + 3*Power(s2,3)*t2*(2 + t2) + 
               Power(s2,2)*t2*(161 - 32*t2 + 3*Power(t2,2)) + 
               s2*(4 + 308*t2 - 307*Power(t2,2) + 62*Power(t2,3) - 
                  2*Power(t2,4))) + 
            Power(s1,3)*(88 + Power(t1,2)*(48 - 98*t2) + 
               Power(s2,2)*(4 - 18*t2) - 301*t2 + 360*Power(t2,2) - 
               230*Power(t2,3) - 5*t1*(7 - 50*t2 + 61*Power(t2,2)) + 
               s2*(36 - 118*t2 + 105*Power(t2,2) + 12*t1*(-3 + 8*t2))) + 
            Power(s1,2)*(26 - 92*t2 - 2*Power(s2,3)*t2 + 
               125*Power(t2,2) - 59*Power(t2,3) + 82*Power(t2,4) + 
               3*Power(t1,3)*(-20 + 7*t2) + 
               Power(t1,2)*(28 - 231*t2 + 207*Power(t2,2)) + 
               t1*(-82 + 498*t2 - 684*Power(t2,2) + 210*Power(t2,3)) + 
               Power(s2,2)*(6 - 8*t2 - 27*Power(t2,2) + 
                  5*t1*(-2 + 3*t2)) + 
               s2*(104 + Power(t1,2)*(51 - 24*t2) - 479*t2 + 
                  451*Power(t2,2) - 138*Power(t2,3) + 
                  t1*(-31 + 246*t2 - 201*Power(t2,2)))) + 
            s1*(4 + (242 - 504*s2 - 98*Power(s2,2))*t2 + 
               (-567 + 848*s2 + 72*Power(s2,2) - 9*Power(s2,3))*
                Power(t2,2) + 
               (173 - 301*s2 + 15*Power(s2,2))*Power(t2,3) + 
               (-24 + 53*s2)*Power(t2,4) - 4*Power(t2,5) + 
               Power(t1,3)*(-52 + 56*t2 - 30*Power(t2,2)) + 
               Power(t1,2)*(39 - 128*t2 + 354*Power(t2,2) - 
                  27*Power(t2,3) + s2*(32 - 87*t2 + 41*Power(t2,2))) + 
               t1*(-26 + 486*t2 - 717*Power(t2,2) + 310*Power(t2,3) - 
                  51*Power(t2,4) + 2*Power(s2,2)*t2*(16 + t2) + 
                  2*s2*(-5 + 94*t2 - 234*Power(t2,2) + 23*Power(t2,3))))) \
+ Power(s,4)*(-2 + 21*t2 + 487*Power(t2,2) + 176*s2*Power(t2,2) - 
            157*Power(s2,2)*Power(t2,2) - 26*Power(s2,3)*Power(t2,2) - 
            560*Power(t2,3) + 187*s2*Power(t2,3) + 
            89*Power(s2,2)*Power(t2,3) - 7*Power(s2,3)*Power(t2,3) + 
            218*Power(t2,4) - 92*s2*Power(t2,4) - 
            15*Power(s2,2)*Power(t2,4) + Power(s2,3)*Power(t2,4) - 
            35*Power(t2,5) + 4*s2*Power(t2,5) + 
            Power(s2,2)*Power(t2,5) - 
            24*Power(s1,5)*(2 - 5*t2 + 3*Power(t2,2)) + 
            Power(t1,3)*(10 - 25*t2 + 37*Power(t2,2) - 6*Power(t2,3)) + 
            Power(t1,2)*(-30 + (-179 + 100*s2)*t2 - 
               (64 + 133*s2)*Power(t2,2) + (127 + s2)*Power(t2,3) + 
               2*(-6 + s2)*Power(t2,4)) + 
            t1*(18 + (79 - 68*s2)*t2 + 
               (49 + 381*s2 + 43*Power(s2,2))*Power(t2,2) + 
               (-351 - 256*s2 + 31*Power(s2,2))*Power(t2,3) + 
               (111 + 43*s2 - 4*Power(s2,2))*Power(t2,4) - 
               2*(3 + s2)*Power(t2,5)) + 
            Power(s1,4)*(10*Power(t1,2)*(-5 + 7*t2) + 
               2*Power(s2,2)*(-7 + 15*t2) + 
               20*t1*(3 - 13*t2 + 11*Power(t2,2)) + 
               t2*(-57 - 30*t2 + 130*Power(t2,2)) + 
               s2*(-9 + 32*t2 - 40*Power(t2,2) - 20*t1*(-3 + 5*t2))) + 
            Power(s1,3)*(57 - 159*t2 + 326*Power(t2,2) - 
               259*Power(t2,3) - 68*Power(t2,4) + 
               Power(s2,3)*(-4 + 5*t2) - 5*Power(t1,3)*(-23 + 7*t2) + 
               Power(t1,2)*(24 + 210*t2 - 220*Power(t2,2)) + 
               Power(s2,2)*(7 + t1*(47 - 15*t2) - 29*t2 + 
                  41*Power(t2,2)) + 
               t1*(153 - 454*t2 + 509*Power(t2,2) - 230*Power(t2,3)) + 
               s2*(-112 + 155*t2 - 56*Power(t2,2) + 112*Power(t2,3) + 
                  5*Power(t1,2)*(-27 + 5*t2) + 
                  t1*(39 - 310*t2 + 205*Power(t2,2)))) + 
            Power(s1,2)*(2*Power(s2,3)*t2*(-2 + 13*t2) + 
               Power(t1,3)*(86 - 145*t2 + 75*Power(t2,2)) + 
               Power(t1,2)*(-27 + 28*t2 - 421*Power(t2,2) + 
                  30*Power(t2,3)) + 
               2*t1*(67 - 298*t2 + 245*Power(t2,2) - 121*Power(t2,3) + 
                  47*Power(t2,4)) + 
               2*(79 - 484*t2 + 611*Power(t2,2) - 216*Power(t2,3) + 
                  84*Power(t2,4) + 3*Power(t2,5)) - 
               Power(s2,2)*(48 + 17*t2 - 77*Power(t2,2) + 
                  40*Power(t2,3) + t1*(-33 + 95*t2 + 32*Power(t2,2))) - 
               s2*(155 - 701*t2 + 651*Power(t2,2) - 71*Power(t2,3) + 
                  78*Power(t2,4) + 
                  Power(t1,2)*(99 - 215*t2 + 70*Power(t2,2)) + 
                  t1*(-83 + 214*t2 - 633*Power(t2,2) + 80*Power(t2,3)))) \
- s1*(12 + 655*t2 - 1530*Power(t2,2) + 916*Power(t2,3) - 
               222*Power(t2,4) + 32*Power(t2,5) + 
               Power(s2,3)*t2*(-12 - 23*t2 + 18*Power(t2,2)) + 
               Power(t1,3)*(-45 + 99*t2 - 80*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,2)*(7 + 73*t2 + 219*Power(t2,2) - 
                  139*Power(t2,3) - 2*Power(t2,4)) + 
               t1*(-25 + 307*t2 - 428*Power(t2,2) + 345*Power(t2,3) - 
                  93*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-242 + 152*t2 + 16*Power(t2,2) + 7*Power(t2,3) + 
                  t1*(94 - 25*t2 - 45*Power(t2,2))) + 
               s2*(2 - 107*t2 + 516*Power(t2,2) - 379*Power(t2,3) + 
                  69*Power(t2,4) - 8*Power(t2,5) + 
                  Power(t1,2)*
                   (52 - 212*t2 + 153*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(-36 + 286*t2 - 537*Power(t2,2) + 252*Power(t2,3) - 
                     31*Power(t2,4))))) - 
         Power(-1 + s1,4)*(-2 + 6*t1 - 6*Power(t1,2) + 2*Power(t1,3) + 
            2*Power(s1,4)*Power(s2 - t1,2)*(-1 + t2) + 19*t2 - 4*s2*t2 - 
            43*t1*t2 + 8*s2*t1*t2 + 29*Power(t1,2)*t2 - 
            4*s2*Power(t1,2)*t2 - 5*Power(t1,3)*t2 - 30*Power(t2,2) + 
            22*s2*Power(t2,2) - 23*Power(s2,2)*Power(t2,2) + 
            2*Power(s2,3)*Power(t2,2) + 51*t1*Power(t2,2) + 
            13*s2*t1*Power(t2,2) + 3*Power(s2,2)*t1*Power(t2,2) - 
            46*Power(t1,2)*Power(t2,2) - s2*Power(t1,2)*Power(t2,2) + 
            9*Power(t1,3)*Power(t2,2) + 12*Power(t2,3) - 
            32*s2*Power(t2,3) + 27*Power(s2,2)*Power(t2,3) - 
            5*Power(s2,3)*Power(t2,3) + t1*Power(t2,3) - 
            12*s2*t1*Power(t2,3) + 5*Power(s2,2)*t1*Power(t2,3) + 
            11*Power(t1,2)*Power(t2,3) - 5*s2*Power(t1,2)*Power(t2,3) - 
            2*Power(t1,3)*Power(t2,3) + 14*s2*Power(t2,4) - 
            3*Power(s2,2)*Power(t2,4) - Power(s2,3)*Power(t2,4) - 
            13*t1*Power(t2,4) - 11*s2*t1*Power(t2,4) + 
            4*Power(s2,2)*t1*Power(t2,4) + 12*Power(t1,2)*Power(t2,4) - 
            2*s2*Power(t1,2)*Power(t2,4) + Power(t2,5) - 
            Power(s2,2)*Power(t2,5) - 2*t1*Power(t2,5) + 
            2*s2*t1*Power(t2,5) + 
            Power(s1,3)*(Power(t1,3)*(-5 + t2) - 2*Power(-1 + t2,2) + 
               7*t1*Power(-1 + t2,2) + Power(s2,3)*(1 + 3*t2) - 
               4*Power(t1,2)*(1 - 3*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(1 + 2*t2 - 3*Power(t2,2) - t1*(7 + 5*t2)) + 
               s2*(-3*Power(-1 + t2,2) + Power(t1,2)*(11 + t2) + 
                  t1*(1 - 10*t2 + 9*Power(t2,2)))) - 
            Power(s1,2)*(6*t1*Power(-1 + t2,2)*(3 + 2*t2) - 
               Power(-1 + t2,2)*(1 + 3*t2) + 
               Power(t1,3)*(-2 - 15*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(-2 + 7*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(1 + 10*t2 - 3*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(s2,2)*(23 - 25*t2 + Power(t2,2) + Power(t2,3) - 
                  t1*(3 + 19*t2 + 14*Power(t2,2))) + 
               s2*(-2*Power(-1 + t2,2)*(7 + 3*t2) + 
                  Power(t1,2)*(9 + 23*t2 + 4*Power(t2,2)) + 
                  t1*(-29 + 22*t2 + 3*Power(t2,2) + 4*Power(t2,3)))) + 
            s1*(-(Power(-1 + t2,2)*(15 + t2 + 2*Power(t2,2))) + 
               Power(s2,3)*t2*(-4 + 11*t2 + 5*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(31 + 29*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(1 - 11*t2 - 4*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,2)*(17 - 47*t2 + 9*Power(t2,2) + 
                  19*Power(t2,3) + 2*Power(t2,4)) + 
               Power(s2,2)*t2*
                (46 - 53*t2 + 4*Power(t2,2) + 3*Power(t2,3) - 
                  t1*(6 + 17*t2 + 13*Power(t2,2))) + 
               s2*(-(Power(-1 + t2,2)*(-4 + 28*t2 + 3*Power(t2,2))) + 
                  Power(t1,2)*
                   (4 + 10*t2 + 17*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(-8 - 42*t2 + 33*Power(t2,2) + 20*Power(t2,3) - 
                     3*Power(t2,4))))) + 
         s*Power(-1 + s1,2)*(-4 + 18*t1 - 24*Power(t1,2) + 
            10*Power(t1,3) + 82*t2 - 24*s2*t2 - 201*t1*t2 + 
            50*s2*t1*t2 + 140*Power(t1,2)*t2 - 26*s2*Power(t1,2)*t2 - 
            21*Power(t1,3)*t2 - 94*Power(t2,2) + 58*s2*Power(t2,2) - 
            119*Power(s2,2)*Power(t2,2) + 14*Power(s2,3)*Power(t2,2) + 
            244*t1*Power(t2,2) + 71*s2*t1*Power(t2,2) + 
            2*Power(s2,2)*t1*Power(t2,2) - 226*Power(t1,2)*Power(t2,2) + 
            16*s2*Power(t1,2)*Power(t2,2) + 32*Power(t1,3)*Power(t2,2) - 
            2*Power(t2,3) - 74*s2*Power(t2,3) + 
            88*Power(s2,2)*Power(t2,3) - 17*Power(s2,3)*Power(t2,3) - 
            3*t1*Power(t2,3) + 10*s2*t1*Power(t2,3) + 
            11*Power(s2,2)*t1*Power(t2,3) + 20*Power(t1,2)*Power(t2,3) - 
            19*s2*Power(t1,2)*Power(t2,3) - 6*Power(t1,3)*Power(t2,3) + 
            10*Power(t2,4) + 42*s2*Power(t2,4) - 
            7*Power(s2,2)*Power(t2,4) - 4*Power(s2,3)*Power(t2,4) - 
            52*t1*Power(t2,4) - 55*s2*t1*Power(t2,4) + 
            16*Power(s2,2)*t1*Power(t2,4) + 48*Power(t1,2)*Power(t2,4) - 
            8*s2*Power(t1,2)*Power(t2,4) + 8*Power(t2,5) - 
            2*s2*Power(t2,5) - 4*Power(s2,2)*Power(t2,5) - 
            6*t1*Power(t2,5) + 8*s2*t1*Power(t2,5) + 
            Power(s1,5)*(-8*Power(-1 + t2,2) + 
               2*Power(t1,2)*(-4 + 5*t2) + Power(s2,2)*(-4 + 6*t2) + 
               t1*(-3 + 2*t2 + Power(t2,2)) + 
               s2*(9 - 14*t2 + 5*Power(t2,2) - 4*t1*(-3 + 4*t2))) + 
            Power(s1,3)*(-56 + 77*t2 - 10*Power(t2,2) + Power(t2,3) - 
               12*Power(t2,4) + 
               Power(t1,3)*(12 + 78*t2 - 30*Power(t2,2)) + 
               Power(s2,3)*(11 - 35*t2 - 20*Power(t2,2)) + 
               Power(t1,2)*(-13 + 44*t2 + 20*Power(t2,2) + 
                  33*Power(t2,3)) + 
               t1*(-37 + 40*t2 + 54*Power(t2,2) - 50*Power(t2,3) - 
                  7*Power(t2,4)) + 
               Power(s2,2)*(-103 + 187*t2 + 20*Power(t2,2) - 
                  20*Power(t2,3) + 2*t1*(6 + 37*t2 + 31*Power(t2,2))) - 
               s2*(-110 + 150*t2 + 23*Power(t2,2) - 54*Power(t2,3) - 
                  9*Power(t2,4) + 
                  Power(t1,2)*(30 + 123*t2 + 11*Power(t2,2)) + 
                  2*t1*(-48 + 104*t2 + 23*Power(t2,2) + 5*Power(t2,3)))) \
+ Power(s1,4)*(13 - 6*t2 - 23*Power(t2,2) + 16*Power(t2,3) + 
               Power(t1,3)*(-32 + 7*t2) + Power(s2,3)*(7 + 10*t2) + 
               Power(t1,2)*(-28 + 41*t2 - 31*Power(t2,2)) + 
               2*t1*(12 - 21*t2 + 7*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(6 - 25*t2 + Power(t2,2) - 
                  t1*(38 + 21*t2)) + 
               s2*(Power(t1,2)*(63 + 4*t2) + 
                  t1*(33 - 34*t2 + 37*Power(t2,2)) - 
                  2*(23 - 35*t2 + 6*Power(t2,2) + 6*Power(t2,3)))) + 
            Power(s1,2)*(2*Power(t1,3)*
                (1 - 11*t2 - 10*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,3)*(10 - 27*t2 + 33*Power(t2,2) + 
                  14*Power(t2,3)) + 
               t2*(125 - 184*t2 + 41*Power(t2,2) + 14*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(t1,2)*(74 - 137*t2 + 149*Power(t2,2) + 
                  70*Power(t2,3) + 8*Power(t2,4)) + 
               t1*(60 - 111*t2 - 6*Power(t2,2) + 17*Power(t2,3) + 
                  38*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,2)*(-71 + 182*t2 - 315*Power(t2,2) + 
                  23*Power(t2,3) + 17*Power(t2,4) - 
                  t1*(6 - 7*t2 + 34*Power(t2,2) + 57*Power(t2,3))) + 
               s2*(-19 - 159*t2 + 329*Power(t2,2) - 117*Power(t2,3) - 
                  32*Power(t2,4) - 2*Power(t2,5) + 
                  Power(t1,2)*
                   (-1 + 8*t2 + 62*Power(t2,2) + 21*Power(t2,3)) + 
                  t1*(97 - 136*t2 + 300*Power(t2,2) + 86*Power(t2,3) - 
                     19*Power(t2,4)))) - 
            s1*(61 - 46*t2 + 13*Power(t2,2) - 60*Power(t2,3) + 
               28*Power(t2,4) + 4*Power(t2,5) + 
               2*Power(t1,3)*
                (-4 + 21*t2 - 9*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,3)*t2*
                (24 - 33*t2 + Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(93 - 268*t2 + 94*Power(t2,2) - 
                  17*Power(t2,3) - 40*Power(t2,4)) + 
               t1*(-146 + 216*t2 - 45*Power(t2,2) - 64*Power(t2,3) + 
                  27*Power(t2,4) + 12*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-190 + 167*t2 - 129*Power(t2,2) + 10*Power(t2,3) + 
                  4*Power(t2,4) - 
                  2*t1*(2 - 15*t2 - 9*Power(t2,2) + 8*Power(t2,3))) + 
               s2*(-14 + 15*t2 - 111*Power(t2,2) + 183*Power(t2,3) - 
                  69*Power(t2,4) - 4*Power(t2,5) + 
                  Power(t1,2)*
                   (-16 - 9*t2 - 29*Power(t2,2) + 2*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  t1*(30 + 216*t2 - 54*Power(t2,2) + 54*Power(t2,3) + 
                     38*Power(t2,4) - 8*Power(t2,5))))) + 
         Power(s,3)*(4 - 12*t1 + 10*Power(t1,3) + 
            12*Power(s1,6)*Power(-1 + t2,2) + 52*t2 - 28*s2*t2 - 
            318*t1*t2 + 132*s2*t1*t2 + 312*Power(t1,2)*t2 - 
            110*s2*Power(t1,2)*t2 - 5*Power(t1,3)*t2 - 109*Power(t2,2) - 
            426*s2*Power(t2,2) - 48*Power(s2,2)*Power(t2,2) + 
            44*Power(s2,3)*Power(t2,2) + 386*t1*Power(t2,2) - 
            131*s2*t1*Power(t2,2) - 52*Power(s2,2)*t1*Power(t2,2) - 
            279*Power(t1,2)*Power(t2,2) + 
            152*s2*Power(t1,2)*Power(t2,2) - 8*Power(t1,3)*Power(t2,2) + 
            228*Power(t2,3) + 110*s2*Power(t2,3) - 
            56*Power(s2,2)*Power(t2,3) - 2*Power(s2,3)*Power(t2,3) + 
            221*t1*Power(t2,3) + 316*s2*t1*Power(t2,3) - 
            34*Power(s2,2)*t1*Power(t2,3) - 
            128*Power(t1,2)*Power(t2,3) - 
            14*s2*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) - 
            195*Power(t2,4) + 48*s2*Power(t2,4) + 
            20*Power(s2,2)*Power(t2,4) - 4*Power(s2,3)*Power(t2,4) - 
            136*t1*Power(t2,4) - 98*s2*t1*Power(t2,4) + 
            16*Power(s2,2)*t1*Power(t2,4) + 48*Power(t1,2)*Power(t2,4) - 
            8*s2*Power(t1,2)*Power(t2,4) + 48*Power(t2,5) - 
            4*s2*Power(t2,5) - 4*Power(s2,2)*Power(t2,5) + 
            4*t1*Power(t2,5) + 8*s2*t1*Power(t2,5) - 
            Power(s1,5)*(72 - 197*t2 + 96*Power(t2,2) + 
               35*Power(t2,3) + 4*Power(s2,2)*(-4 + 5*t2) + 
               2*Power(t1,2)*(-10 + 7*t2) + 
               s2*(9 - 40*t1*(-1 + t2) + 8*t2 - 18*Power(t2,2)) + 
               t1*(50 - 140*t2 + 81*Power(t2,2))) + 
            Power(s1,4)*(13*Power(s2,3) + 5*Power(t1,3)*(-26 + 7*t2) + 
               Power(t1,2)*(-96 - 70*t2 + 95*Power(t2,2)) + 
               t1*(-76 + 100*t2 - 186*Power(t2,2) + 135*Power(t2,3)) + 
               3*(3 + 4*t2 - 108*Power(t2,2) + 97*Power(t2,3) + 
                  9*Power(t2,4)) - 
               Power(s2,2)*(36 - 9*t2 + 4*Power(t2,2) + 
                  2*t1*(44 + 5*t2)) + 
               s2*(-26 - 10*Power(t1,2)*(-19 + t2) + 269*t2 - 
                  181*Power(t2,2) - 73*Power(t2,3) + 
                  t1*(54 + 140*t2 - 75*Power(t2,2)))) + 
            Power(s1,3)*(-256 + 886*t2 - 629*Power(t2,2) + 
               160*Power(t2,3) - 200*Power(t2,4) - 4*Power(t2,5) + 
               Power(s2,3)*(1 - 9*t2 - 31*Power(t2,2)) - 
               4*Power(t1,3)*(6 - 50*t2 + 25*Power(t2,2)) + 
               2*Power(t1,2)*
                (-5 + 114*t2 + 102*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(-34 + 236*t2 + 50*Power(t2,2) - 36*Power(t2,3) - 
                  86*Power(t2,4)) + 
               Power(s2,2)*(-49 + 361*t2 - 150*Power(t2,2) + 
                  9*Power(t2,3) + 4*t1*(-10 + 33*t2 + 22*Power(t2,2))) \
+ s2*(134 - 698*t2 + 220*Power(t2,2) + 239*Power(t2,3) + 
                  66*Power(t2,4) + 
                  Power(t1,2)*(52 - 310*t2 + 50*Power(t2,2)) + 
                  4*t1*(4 - 72*t2 - 105*Power(t2,2) + 15*Power(t2,3)))) \
+ Power(s1,2)*(-182 + 861*t2 - 1308*Power(t2,2) + 658*Power(t2,3) - 
               68*Power(t2,4) + 44*Power(t2,5) + 
               2*Power(t1,3)*
                (-8 + 41*t2 - 40*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,3)*(6 + 21*t2 - 37*Power(t2,2) + 
                  31*Power(t2,3)) - 
               2*Power(t1,2)*
                (54 - 153*t2 + 122*Power(t2,2) + 78*Power(t2,3) + 
                  4*Power(t2,4)) + 
               2*t1*(-8 - 147*t2 + 28*Power(t2,2) + 58*Power(t2,3) + 
                  4*Power(t2,4) + 6*Power(t2,5)) + 
               Power(s2,2)*(89 - 22*t2 - 227*Power(t2,2) + 
                  103*Power(t2,3) + 16*Power(t2,4) - 
                  2*t1*(24 - 53*t2 + 10*Power(t2,2) + 47*Power(t2,3))) \
+ s2*(-113 + 98*t2 + 504*Power(t2,2) - 370*Power(t2,3) - 
                  52*Power(t2,4) - 12*Power(t2,5) + 
                  2*Power(t1,2)*
                   (31 - 110*t2 + 78*Power(t2,2) + 13*Power(t2,3)) - 
                  2*t1*(11 + 56*t2 - 63*Power(t2,2) - 134*Power(t2,3) + 
                     27*Power(t2,4)))) + 
            s1*(1 + 244*t2 - 782*Power(t2,2) + 702*Power(t2,3) - 
               196*Power(t2,4) + 32*Power(t2,5) + 
               4*Power(t1,3)*t2*(10 - 9*t2 + 2*Power(t2,2)) - 
               Power(s2,3)*t2*
                (40 + 20*t2 - 31*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(92 - 264*t2 - 17*Power(t2,2) + 108*Power(t2,3) - 
                  58*Power(t2,4)) + 
               2*Power(t1,2)*
                (-63 + 155*t2 - 120*Power(t2,2) - 47*Power(t2,3) + 
                  20*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-140 + 113*t2 + 20*Power(t2,2) + 4*Power(t2,3) - 
                  4*Power(t2,4) + 
                  4*t1*(31 - 8*t2 - 16*Power(t2,2) + 4*Power(t2,3))) + 
               s2*(10 + 667*t2 - 347*Power(t2,2) - 94*Power(t2,3) + 
                  58*Power(t2,4) + 
                  Power(t1,2)*
                   (48 - 198*t2 + 138*Power(t2,2) + 4*Power(t2,3) - 
                     8*Power(t2,4)) + 
                  4*t1*(-14 + 18*t2 - 11*Power(t2,2) + 47*Power(t2,3) - 
                     22*Power(t2,4) + 2*Power(t2,5))))) + 
         Power(s,2)*(-12*t1 + 30*Power(t1,2) - 18*Power(t1,3) - 118*t2 + 
            44*s2*t2 + 372*t1*t2 - 118*s2*t1*t2 - 285*Power(t1,2)*t2 + 
            72*s2*Power(t1,2)*t2 + 29*Power(t1,3)*t2 + 21*Power(t2,2) + 
            139*s2*Power(t2,2) + 190*Power(s2,2)*Power(t2,2) - 
            36*Power(s2,3)*Power(t2,2) - 472*t1*Power(t2,2) - 
            81*s2*t1*Power(t2,2) + 23*Power(s2,2)*t1*Power(t2,2) + 
            409*Power(t1,2)*Power(t2,2) - 83*s2*Power(t1,2)*Power(t2,2) - 
            33*Power(t1,3)*Power(t2,2) + 97*Power(t2,3) - 
            70*s2*Power(t2,3) - 64*Power(s2,2)*Power(t2,3) + 
            18*Power(s2,3)*Power(t2,3) - 49*t1*Power(t2,3) - 
            162*s2*t1*Power(t2,3) + 6*Power(s2,2)*t1*Power(t2,3) + 
            37*Power(t1,2)*Power(t2,3) + 26*s2*Power(t1,2)*Power(t2,3) + 
            4*Power(t1,3)*Power(t2,3) + 13*Power(t2,4) - 
            31*s2*Power(t2,4) - 4*Power(s2,2)*Power(t2,4) + 
            6*Power(s2,3)*Power(t2,4) + 105*t1*Power(t2,4) + 
            106*s2*t1*Power(t2,4) - 24*Power(s2,2)*t1*Power(t2,4) - 
            72*Power(t1,2)*Power(t2,4) + 12*s2*Power(t1,2)*Power(t2,4) - 
            29*Power(t2,5) + 4*s2*Power(t2,5) + 
            6*Power(s2,2)*Power(t2,5) + 4*t1*Power(t2,5) - 
            12*s2*t1*Power(t2,5) + 
            Power(s1,6)*(42 - 4*Power(s2,2) + Power(t1,2)*(6 - 14*t2) - 
               95*t2 + 50*Power(t2,2) + 3*Power(t2,3) + 
               s2*(-9 + 4*(7 + 3*t1)*t2 - 15*Power(t2,2)) + 
               2*t1*(10 - 17*t2 + 5*Power(t2,2))) + 
            Power(s1,5)*(-91 + Power(t1,3)*(87 - 21*t2) + 121*t2 + 
               91*Power(t2,2) - 115*Power(t2,3) - 4*Power(t2,4) - 
               5*Power(s2,3)*(3 + 2*t2) + 
               Power(t1,2)*(74 - 21*t2 + 18*Power(t2,2)) + 
               t1*(-26 + 66*t2 + 24*Power(t2,2) - 39*Power(t2,3)) + 
               Power(s2,2)*(18 + 47*t2 - 15*Power(t2,2) + 
                  t1*(82 + 30*t2)) - 
               s2*(-95 + 273*t2 - 107*Power(t2,2) - 42*Power(t2,3) + 
                  3*Power(t1,2)*(50 + t2) + t1*(86 + 33*Power(t2,2)))) + 
            Power(s1,4)*(197 - 356*t2 - 11*Power(t2,2) + 
               74*Power(t2,3) + 87*Power(t2,4) + Power(t2,5) + 
               Power(s2,3)*t2*(49 + 27*t2) + 
               Power(t1,3)*(-74 - 155*t2 + 75*Power(t2,2)) - 
               Power(t1,2)*(28 + 187*t2 + 81*Power(t2,2) + 
                  45*Power(t2,3)) + 
               t1*(32 - 126*t2 - 170*Power(t2,2) + 125*Power(t2,3) + 
                  39*Power(t2,4)) + 
               Power(s2,2)*(129 - 460*t2 + 50*Power(t2,2) + 
                  33*Power(t2,3) - t1*(50 + 146*t2 + 107*Power(t2,2))) - 
               s2*(300 - 743*t2 + 62*Power(t2,2) + 228*Power(t2,3) + 
                  35*Power(t2,4) + 
                  Power(t1,2)*(-118 - 270*t2 + 5*Power(t2,2)) + 
                  t1*(82 - 462*t2 - 207*Power(t2,2) + 10*Power(t2,3)))) - 
            Power(s1,3)*(-28 + 143*t2 - 439*Power(t2,2) + 
               172*Power(t2,3) + 100*Power(t2,4) + 24*Power(t2,5) + 
               Power(t1,3)*(-6 - 98*t2 + 20*Power(t2,3)) + 
               Power(s2,3)*(-13 + 28*t2 + 26*Power(t2,2) + 
                  25*Power(t2,3)) - 
               2*Power(t1,2)*
                (85 - 96*t2 + 207*Power(t2,2) + 77*Power(t2,3) + 
                  6*Power(t2,4)) + 
               2*t1*(74 - 247*t2 - 51*Power(t2,2) + 52*Power(t2,3) + 
                  47*Power(t2,4) + 4*Power(t2,5)) - 
               Power(s2,2)*(16 + 35*t2 + 441*Power(t2,2) - 
                  110*Power(t2,3) - 26*Power(t2,4) + 
                  2*t1*(-1 + 19*t2 + 47*Power(t2,2) + 51*Power(t2,3))) + 
               s2*(-45 + 38*t2 + 900*Power(t2,2) - 473*Power(t2,3) - 
                  106*Power(t2,4) - 8*Power(t2,5) + 
                  2*Power(t1,2)*
                   (13 + 44*t2 + 55*Power(t2,2) + 17*Power(t2,3)) + 
                  t1*(166 - 80*t2 + 654*Power(t2,2) + 176*Power(t2,3) - 
                     46*Power(t2,4)))) + 
            s1*(53 + 236*t2 - 208*Power(t2,2) - 15*Power(t2,3) - 
               36*Power(t2,4) + 20*Power(t2,5) + 
               Power(s2,3)*t2*
                (48 + 4*t2 - 21*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(-5 + 19*t2 - 8*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(156 - 259*t2 + 16*Power(t2,2) - 
                  58*Power(t2,3) - 36*Power(t2,4)) + 
               t1*(-202 + 200*t2 + 226*Power(t2,2) - Power(t2,3) - 
                  34*Power(t2,4) + 16*Power(t2,5)) + 
               2*Power(s2,2)*t2*
                (-80 - 30*t2 - 26*Power(t2,2) + 3*Power(t2,3) + 
                  2*Power(t2,4) + 
                  t1*(-34 + 5*t2 + 13*Power(t2,2) - 8*Power(t2,3))) + 
               s2*(-18 - 523*t2 + 319*Power(t2,2) - 61*Power(t2,3) - 
                  30*Power(t2,4) + 
                  Power(t1,2)*
                   (-32 + 27*t2 - 2*Power(t2,2) - 6*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  t1*(52 + 328*t2 - 49*Power(t2,2) + 160*Power(t2,3) + 
                     26*Power(t2,4) - 8*Power(t2,5)))) + 
            Power(s1,2)*(-101 - 13*t2 - 30*Power(t2,2) + 72*Power(t2,4) + 
               16*Power(t2,5) + 
               2*Power(t1,3)*
                (2 + 15*t2 - 17*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,3)*(-14 - 27*t2 + 31*Power(t2,2) - 
                  4*Power(t2,3) + 6*Power(t2,4)) - 
               2*Power(t1,2)*(-36 + 161*t2 - 92*Power(t2,2) + 
                  44*Power(t2,3) + 32*Power(t2,4)) + 
               4*t1*(-20 + 21*t2 - 106*Power(t2,2) + Power(t2,3) + 
                  20*Power(t2,4) + 5*Power(t2,5)) + 
               Power(s2,2)*(1 + 10*t2 + 2*Power(t2,2) - 63*Power(t2,3) + 
                  24*Power(t2,4) + 6*Power(t2,5) - 
                  2*t1*(-17 - 9*t2 + 10*Power(t2,2) + 3*Power(t2,3) + 
                     12*Power(t2,4))) + 
               s2*(299 - 141*t2 + 220*Power(t2,2) + 260*Power(t2,3) - 
                  186*Power(t2,4) - 12*Power(t2,5) + 
                  2*Power(t1,2)*
                   (-3 - 11*t2 + 4*Power(t2,2) + 7*Power(t2,3) + 
                     6*Power(t2,4)) - 
                  2*t1*(67 - 178*t2 + 111*Power(t2,2) - 62*Power(t2,3) - 
                     23*Power(t2,4) + 6*Power(t2,5))))))*R2q(s))/
     (s*(-1 + s1)*Power(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2),
        2)*(-1 + s2)*(-s + s2 - t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) - 
    (8*(2*Power(s,12)*(t1 - 3*t2)*Power(t2,2) + 
         Power(-1 + s1,6)*(s1 - t2)*(-1 + t2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) - 
         2*Power(s,11)*t2*(Power(t1,2) + t1*(7 + s2 - t2)*t2 + 
            t2*(-1 - 26*t2 - 4*s2*t2 + 7*Power(t2,2)) + 
            s1*(t1*(-2 + 9*t2) - t2*(-10 + s2 + 24*t2))) + 
         Power(s,10)*(-(Power(t1,3)*(-1 + t2)) + 
            Power(t1,2)*t2*(16 + s2 - 3*t2 - 3*Power(t2,2)) + 
            t1*t2*(-4 + (23 + 19*s2)*t2 - (1 + s2)*Power(t2,2) + 
               4*Power(t2,3)) - 
            Power(t2,2)*(16 + 152*t2 + 2*Power(s2,2)*t2 - 
               105*Power(t2,2) + 13*Power(t2,3) + 
               s2*(-2 + 80*t2 - 15*Power(t2,2))) + 
            2*Power(s1,2)*(t2*
                (-11 + 2*s2 + 70*t2 - 8*s2*t2 - 84*Power(t2,2)) + 
               t1*(1 - 16*t2 + 36*Power(t2,2))) + 
            s1*(Power(t1,2)*(-2 + 19*t2) + 
               t2*(4 - 2*(-69 - 6*s2 + Power(s2,2))*t2 - 
                  6*(59 + 7*s2)*Power(t2,2) + 99*Power(t2,3)) + 
               t1*t2*(-20 + 85*t2 - 17*Power(t2,2) + 2*s2*(-4 + 7*t2)))) \
+ Power(s,9)*(-(Power(t1,3)*(7 - 11*t2 + Power(t2,2) + Power(t2,3))) + 
            Power(t1,2)*(3 - 2*(24 + 5*s2)*t2 + 19*Power(t2,2) + 
               2*(12 + s2)*Power(t2,3)) + 
            t2*(-2 - 2*(-21 + 4*s2 + Power(s2,2))*t2 + 
               (39 + 324*s2 + 26*Power(s2,2))*Power(t2,2) - 
               (238 + 135*s2)*Power(t2,3) + 2*(44 + 7*s2)*Power(t2,4) - 
               4*Power(t2,5)) + 
            2*Power(s1,3)*(-4 + 66*t2 - 210*Power(t2,2) + 
               168*Power(t2,3) - 7*t1*(1 - 8*t2 + 12*Power(t2,2)) + 
               s2*(1 - 14*t2 + 28*Power(t2,2))) + 
            t1*t2*(31 + 70*t2 - 86*Power(t2,2) - 15*Power(t2,3) + 
               Power(t2,4) - Power(s2,2)*t2*(1 + t2) - 
               s2*(1 + 71*t2 - 9*Power(t2,2) + 6*Power(t2,3))) + 
            s1*(2*Power(t1,3)*(-6 + 5*t2) + 
               Power(t1,2)*(11 + s2*(4 - 10*t2) - 107*t2 + 
                  14*Power(t2,2) + 23*Power(t2,3)) + 
               t1*(-4 + (25 + 59*s2 + 2*Power(s2,2))*t2 + 
                  (40 - 107*s2 + 2*Power(s2,2))*Power(t2,2) + 
                  (-2 + 3*s2)*Power(t2,3) - 28*Power(t2,4)) + 
               t2*(-24 + (-275 - 233*s2 + 15*Power(s2,2))*t2 + 
                  (832 + 371*s2 - 5*Power(s2,2))*Power(t2,2) - 
                  (562 + 69*s2)*Power(t2,3) + 79*Power(t2,4))) + 
            Power(s1,2)*(2 + Power(t1,2)*(17 - 78*t2) + 
               (118 + 8*s2 - 6*Power(s2,2))*t2 + 
               (-749 - 41*s2 + 12*Power(s2,2))*Power(t2,2) + 
               (1023 + 71*s2)*Power(t2,3) - 301*Power(t2,4) + 
               t1*(-6 + 82*t2 - 202*Power(t2,2) + 64*Power(t2,3) + 
                  s2*(-6 + 58*t2 - 40*Power(t2,2))))) + 
         Power(s,7)*(1 - 35*t2 + 4*s2*t2 - 295*Power(t2,2) + 
            172*s2*Power(t2,2) - 59*Power(s2,2)*Power(t2,2) - 
            1800*Power(t2,3) - 23*s2*Power(t2,3) + 
            411*Power(s2,2)*Power(t2,3) + 16*Power(s2,3)*Power(t2,3) + 
            1548*Power(t2,4) - 667*s2*Power(t2,4) - 
            109*Power(s2,2)*Power(t2,4) + 6*Power(s2,3)*Power(t2,4) - 
            203*Power(t2,5) + 367*s2*Power(t2,5) + 
            15*Power(s2,2)*Power(t2,5) - 18*Power(t2,6) - 
            36*s2*Power(t2,6) - 
            Power(t1,3)*(28 - 90*t2 + 35*Power(t2,2) + 24*Power(t2,3)) + 
            Power(t1,2)*(42 - 7*(-5 + 16*s2)*t2 + 
               (-62 + 45*s2)*Power(t2,2) + (101 + 74*s2)*Power(t2,3) + 
               4*(4 + s2)*Power(t2,4)) + 
            t1*(-15 + (60 + 43*s2)*t2 + 
               (777 - 71*s2 - 28*Power(s2,2))*Power(t2,2) - 
               (866 + 15*s2 + 32*Power(s2,2))*Power(t2,3) + 
               (407 - 185*s2 - 16*Power(s2,2))*Power(t2,4) + 
               18*(-5 + s2)*Power(t2,5) + 4*Power(t2,6)) + 
            2*Power(s1,5)*(-40 + 220*t2 - 350*Power(t2,2) + 
               168*Power(t2,3) + 5*s2*(3 - 14*t2 + 14*Power(t2,2)) - 
               7*t1*(5 - 20*t2 + 18*Power(t2,2))) + 
            Power(s1,4)*(-92 + 679*t2 - 1549*Power(t2,2) + 
               1473*Power(t2,3) - 525*Power(t2,4) - 
               7*Power(t1,2)*(-17 + 36*t2) + 
               Power(s2,2)*(21 - 82*t2 + 28*Power(t2,2)) + 
               2*t1*(27 - 88*t2 - 7*Power(t2,2) + 98*Power(t2,3)) + 
               s2*(-19 + 25*t2 + 159*Power(t2,2) - 189*Power(t2,3) - 
                  2*t1*(55 - 147*t2 + 14*Power(t2,2)))) - 
            Power(s1,3)*(21 - 143*t2 + 522*Power(t2,2) - 
               1315*Power(t2,3) + 1298*Power(t2,4) - 275*Power(t2,5) - 
               3*Power(t1,3)*(-61 + 40*t2) + 
               2*Power(s2,3)*(-1 + t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(-23 + 205*t2 + 91*Power(t2,2) - 
                  140*Power(t2,3)) + 
               t1*(-113 + 1094*t2 - 1627*Power(t2,2) + 
                  141*Power(t2,3) + 140*Power(t2,4)) + 
               Power(s2,2)*(-19 + 222*t2 - 389*Power(t2,2) + 
                  161*Power(t2,3) + t1*(38 - 22*t2 - 42*Power(t2,2))) + 
               s2*(79 - 799*t2 + 1291*Power(t2,2) - 301*Power(t2,3) - 
                  3*Power(t2,4) + Power(t1,2)*(-164 + 115*t2) + 
                  t1*(95 - 569*t2 + 374*Power(t2,2) + 42*Power(t2,3)))) \
+ Power(s1,2)*(6 - 442*t2 + 2093*Power(t2,2) - 1704*Power(t2,3) - 
               665*Power(t2,4) + 608*Power(t2,5) - 40*Power(t2,6) + 
               Power(s2,3)*t2*(-15 + 16*t2 - 7*Power(t2,2)) - 
               Power(t1,3)*(210 - 286*t2 + 15*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,2)*(70 + 169*t2 - 270*Power(t2,2) + 
                  277*Power(t2,3) + 7*Power(t2,4)) + 
               t1*(46 - 1077*t2 + 2858*Power(t2,2) - 
                  1230*Power(t2,3) - 107*Power(t2,4) + 9*Power(t2,5)) + 
               Power(s2,2)*(t1*
                   (-27 + 110*t2 + 47*Power(t2,2) - 38*Power(t2,3)) + 
                  t2*(-4 + 346*t2 - 468*Power(t2,2) + 113*Power(t2,3))) \
+ s2*(13 + 811*t2 - 2981*Power(t2,2) + 2299*Power(t2,3) - 
                  583*Power(t2,4) + 76*Power(t2,5) + 
                  Power(t1,2)*
                   (152 - 290*t2 - 39*Power(t2,2) + 42*Power(t2,3)) + 
                  t1*(-69 + 139*t2 - 433*Power(t2,2) + 
                     227*Power(t2,3) - 50*Power(t2,4)))) + 
            s1*(10 + 65*t2 + 2076*Power(t2,2) - 4167*Power(t2,3) + 
               1333*Power(t2,4) + 269*Power(t2,5) - 82*Power(t2,6) + 
               Power(s2,3)*Power(t2,2)*(17 - 28*t2 + 7*Power(t2,2)) - 
               Power(t1,3)*(123 - 240*t2 + 46*Power(t2,2) + 
                  44*Power(t2,3)) + 
               Power(t1,2)*(96 + 133*t2 - 309*Power(t2,2) + 
                  226*Power(t2,3) + 23*Power(t2,4)) - 
               t1*(53 + 447*t2 - 2246*Power(t2,2) + 1703*Power(t2,3) - 
                  343*Power(t2,4) + 41*Power(t2,5)) + 
               Power(s2,2)*t2*
                (24 - 359*t2 + 61*Power(t2,2) + 143*Power(t2,3) - 
                  10*Power(t2,4) - 
                  t1*(-80 + 7*t2 + 46*Power(t2,2) + 5*Power(t2,3))) + 
               s2*(1 - 72*t2 - 1288*Power(t2,2) + 2447*Power(t2,3) - 
                  1454*Power(t2,4) + 355*Power(t2,5) - 12*Power(t2,6) + 
                  t1*(-28 + 69*t2 - 10*Power(t2,2) + 250*Power(t2,3) - 
                     259*Power(t2,4)) + 
                  Power(t1,2)*
                   (62 - 259*t2 + 6*Power(t2,2) + 100*Power(t2,3) + 
                     6*Power(t2,4))))) + 
         Power(s,8)*(Power(t1,3)*
             (20 - 45*t2 + 10*Power(t2,2) + 8*Power(t2,3)) - 
            Power(t1,2)*(18 - 2*(29 + 22*s2)*t2 + 
               (29 + 6*s2)*Power(t2,2) + (74 + 20*s2)*Power(t2,3) + 
               (2 + s2)*Power(t2,4)) + 
            t1*(3 - (83 + 3*s2)*t2 + 
               2*(-185 + 60*s2 + 4*Power(s2,2))*Power(t2,2) + 
               (421 - 19*s2 + 8*Power(s2,2))*Power(t2,3) + 
               (-55 + 58*s2 + 2*Power(s2,2))*Power(t2,4) - 
               (-6 + s2)*Power(t2,5)) + 
            2*Power(s1,4)*(s2*(-6 + 42*t2 - 56*Power(t2,2)) + 
               7*t1*(3 - 16*t2 + 18*Power(t2,2)) - 
               5*(-4 + 33*t2 - 70*Power(t2,2) + 42*Power(t2,3))) + 
            t2*(15 + 8*t2 + 776*Power(t2,2) - 139*Power(t2,3) - 
               157*Power(t2,4) + 24*Power(t2,5) - 
               Power(s2,3)*Power(t2,2)*(2 + t2) - 
               Power(s2,2)*t2*
                (-18 + 142*t2 - 12*Power(t2,2) + Power(t2,3)) + 
               s2*(-2 - 22*t2 - 573*Power(t2,2) + 476*Power(t2,3) - 
                  118*Power(t2,4) + 4*Power(t2,5))) + 
            Power(s1,3)*(32 - 497*t2 + 1564*Power(t2,2) - 
               1614*Power(t2,3) + 511*Power(t2,4) + 
               Power(t1,2)*(-61 + 180*t2) + 
               Power(s2,2)*(-4 + 35*t2 - 28*Power(t2,2)) + 
               t1*(7 - 71*t2 + 210*Power(t2,2) - 140*Power(t2,3)) + 
               s2*(4 - 26*t2 + 5*Power(t2,2) + 7*Power(t2,3) + 
                  2*t1*(20 - 89*t2 + 28*Power(t2,2)))) + 
            Power(s1,2)*(-8 + Power(t1,3)*(62 - 45*t2) - 147*t2 + 
               924*Power(t2,2) - 1682*Power(t2,3) + 1211*Power(t2,4) - 
               201*Power(t2,5) + Power(s2,3)*t2*(1 + 2*t2) - 
               Power(t1,2)*(48 - 260*t2 + 7*Power(t2,2) + 
                  76*Power(t2,3)) + 
               t1*(7 + 224*t2 - 607*Power(t2,2) + 40*Power(t2,3) + 
                  84*Power(t2,4)) + 
               Power(s2,2)*(t1*(5 - 11*t2 - 14*Power(t2,2)) + 
                  t2*(41 - 125*t2 + 63*Power(t2,2))) + 
               s2*(-2 - 237*t2 + 889*Power(t2,2) - 606*Power(t2,3) + 
                  101*Power(t2,4) + Power(t1,2)*(-39 + 44*t2) + 
                  t1*(30 - 292*t2 + 261*Power(t2,2) + 6*Power(t2,3)))) + 
            s1*(-2 + (41 + 15*s2 - 5*Power(s2,2))*t2 + 
               (-295 + 963*s2 + 8*Power(s2,2) - 3*Power(s2,3))*
                Power(t2,2) + 
               (138 - 1401*s2 + 29*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,3) + 
               (786 + 492*s2 - 25*Power(s2,2))*Power(t2,4) - 
               (374 + 57*s2)*Power(t2,5) + 20*Power(t2,6) + 
               Power(t1,3)*(62 - 86*t2 + 6*Power(t2,2) + 
                  8*Power(t2,3)) - 
               Power(t1,2)*(46 - 156*t2 + 12*Power(t2,2) + 
                  128*Power(t2,3) + Power(t2,4) + 
                  2*s2*(12 - 41*t2 - 3*Power(t2,2) + 7*Power(t2,3))) + 
               t1*(21 + 67*t2 - 950*Power(t2,2) + 507*Power(t2,3) + 
                  66*Power(t2,4) - 5*Power(t2,5) + 
                  Power(s2,2)*t2*(-21 - 7*t2 + 10*Power(t2,2)) + 
                  s2*(5 - 128*t2 + 265*Power(t2,2) - 65*Power(t2,3) + 
                     29*Power(t2,4))))) + 
         s*Power(-1 + s1,4)*(-1 + 3*t1 - 3*Power(t1,2) + Power(t1,3) + 
            2*Power(s1,6)*Power(s2 - t1,2)*(-1 + t2) + 15*t2 - 2*s2*t2 - 
            41*t1*t2 + 4*s2*t1*t2 + 37*Power(t1,2)*t2 - 
            2*s2*Power(t1,2)*t2 - 11*Power(t1,3)*t2 - 40*Power(t2,2) + 
            21*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) + 
            89*t1*Power(t2,2) - 48*s2*t1*Power(t2,2) - 
            Power(s2,2)*t1*Power(t2,2) - 59*Power(t1,2)*Power(t2,2) + 
            27*s2*Power(t1,2)*Power(t2,2) + 10*Power(t1,3)*Power(t2,2) + 
            42*Power(t2,3) - 46*s2*Power(t2,3) + 
            5*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) - 
            60*t1*Power(t2,3) + 86*s2*t1*Power(t2,3) - 
            17*Power(s2,2)*t1*Power(t2,3) + 13*Power(t1,2)*Power(t2,3) - 
            28*s2*Power(t1,2)*Power(t2,3) + 3*Power(t1,3)*Power(t2,3) - 
            21*Power(t2,4) + 41*s2*Power(t2,4) - 
            21*Power(s2,2)*Power(t2,4) + Power(s2,3)*Power(t2,4) + 
            2*t1*Power(t2,4) - 32*s2*t1*Power(t2,4) + 
            24*Power(s2,2)*t1*Power(t2,4) + 12*Power(t1,2)*Power(t2,4) - 
            6*s2*Power(t1,2)*Power(t2,4) + 7*Power(t2,5) - 
            18*s2*Power(t2,5) + 17*Power(s2,2)*Power(t2,5) - 
            6*Power(s2,3)*Power(t2,5) + 7*t1*Power(t2,5) - 
            10*s2*t1*Power(t2,5) + 3*Power(s2,2)*t1*Power(t2,5) - 
            2*Power(t2,6) + 4*s2*Power(t2,6) - 
            2*Power(s2,2)*Power(t2,6) + 
            Power(s1,5)*(-3*Power(s2,3)*(-2 + t2) - 
               Power(s2,2)*(-3 + t1*(25 - 16*t2) + t2 + 
                  2*Power(t2,2)) + 
               t1*(Power(-1 + t2,2)*(1 + t2) + 
                  Power(t1,2)*(-13 + 10*t2) - 
                  t1*(4 - 12*t2 + 7*Power(t2,2) + Power(t2,3))) + 
               s2*(Power(t1,2)*(32 - 23*t2) - 
                  Power(-1 + t2,2)*(1 + t2) + 
                  t1*(1 - 11*t2 + 9*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,4)*(2*t1*(-5 + t2)*Power(-1 + t2,2) - 
               Power(-1 + t2,3)*(3 + t2) + 
               Power(s2,3)*(6 - 34*t2 + 13*Power(t2,2)) - 
               Power(t1,3)*(-29 + 7*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-16 + 37*t2 - 31*Power(t2,2) + 
                  9*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(-31 + 47*t2 - 7*Power(t2,2) - 
                  9*Power(t2,3) + 
                  t1*(13 + 71*t2 - 40*Power(t2,2) + Power(t2,3))) + 
               s2*(2*Power(-1 + t2,2)*(-1 + 4*t2 + Power(t2,2)) + 
                  Power(t1,2)*(-48 - 30*t2 + 33*Power(t2,2)) - 
                  2*t1*(-28 + 55*t2 - 31*Power(t2,2) + 3*Power(t2,3) + 
                     Power(t2,4)))) + 
            Power(s1,2)*(-(Power(-1 + t2,3)*t2*(11 + 3*t2)) - 
               t1*Power(-1 + t2,2)*
                (15 + 5*t2 + 14*Power(t2,2) + Power(t2,3)) + 
               2*Power(t1,3)*
                (-7 + 13*t2 + 6*Power(t2,2) + 3*Power(t2,3)) + 
               3*Power(s2,3)*t2*
                (2 + 7*t2 - 24*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(29 - 68*t2 + 34*Power(t2,2) - 
                  6*Power(t2,3) + 11*Power(t2,4)) + 
               s2*(Power(-1 + t2,2)*
                   (6 - 44*t2 + 59*Power(t2,2) + 14*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (-6 - 4*t2 + 42*Power(t2,2) + 10*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  2*t1*(-9 + 42*t2 - 21*Power(t2,2) - 22*Power(t2,3) + 
                     10*Power(t2,4))) + 
               Power(s2,2)*(1 + 7*t2 - 122*Power(t2,2) + 
                  182*Power(t2,3) - 57*Power(t2,4) - 11*Power(t2,5) + 
                  t1*(-1 - 43*t2 + 77*Power(t2,2) + 56*Power(t2,3) + 
                     Power(t2,5)))) - 
            Power(s1,3)*(Power(t1,3)*(10 + 28*t2 - 8*Power(t2,2)) - 
               Power(-1 + t2,3)*(1 + 5*t2 + Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (8 - 35*t2 + 5*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(2 + 19*t2 - 72*Power(t2,2) + 
                  21*Power(t2,3)) + 
               Power(t1,2)*(-21 + 32*t2 - 14*Power(t2,2) + 
                  2*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(1 - 97*t2 + 159*Power(t2,2) - 
                  45*Power(t2,3) - 18*Power(t2,4) + 
                  t1*(-13 + 46*t2 + 83*Power(t2,2) - 28*Power(t2,3) + 
                     2*Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (-25 + 28*t2 + 17*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-2 - 86*t2 - 6*Power(t2,2) + 4*Power(t2,3)) - 
                  t1*(-39 - 29*t2 + 150*Power(t2,2) - 84*Power(t2,3) + 
                     Power(t2,4) + Power(t2,5)))) + 
            s1*(Power(t1,3)*(7 + 10*t2 - 24*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (11 - t2 + 11*Power(t2,2) + 2*Power(t2,3)) - 
               Power(s2,3)*Power(t2,2)*
                (6 + 9*t2 - 34*Power(t2,2) + 4*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (-29 - 2*t2 - 3*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(-25 + 12*t2 + 49*Power(t2,2) - 
                  13*Power(t2,3) - 23*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-2 - 11*t2 + 77*Power(t2,2) - 90*Power(t2,3) + 
                  24*Power(t2,4) + 2*Power(t2,5) + 
                  t1*(2 + 47*t2 - 68*Power(t2,2) - 22*Power(t2,3) - 
                     4*Power(t2,4))) + 
               s2*(-(Power(-1 + t2,2)*
                     (-2 + 23*t2 - 29*Power(t2,2) + 33*Power(t2,3) + 
                       4*Power(t2,4))) + 
                  Power(t1,2)*
                   (2 - 39*t2 + 18*Power(t2,2) + 52*Power(t2,3) + 
                     12*Power(t2,4)) + 
                  t1*(-4 + 66*t2 - 131*Power(t2,2) + 47*Power(t2,3) + 
                     13*Power(t2,4) + 9*Power(t2,5))))) - 
         Power(s,6)*(4 - 27*t1 + 42*Power(t1,2) - 14*Power(t1,3) - 
            10*t2 - 12*s2*t2 - 166*t1*t2 + 139*s2*t1*t2 + 
            255*Power(t1,2)*t2 - 182*s2*Power(t1,2)*t2 + 
            84*Power(t1,3)*t2 - 773*Power(t2,2) + 396*s2*Power(t2,2) - 
            94*Power(s2,2)*Power(t2,2) + 1095*t1*Power(t2,2) + 
            20*s2*t1*Power(t2,2) - 56*Power(s2,2)*t1*Power(t2,2) - 
            319*Power(t1,2)*Power(t2,2) + 
            147*s2*Power(t1,2)*Power(t2,2) - 
            55*Power(t1,3)*Power(t2,2) - 1297*Power(t2,3) - 
            1873*s2*Power(t2,3) + 649*Power(s2,2)*Power(t2,3) + 
            52*Power(s2,3)*Power(t2,3) - 894*t1*Power(t2,3) - 
            53*s2*t1*Power(t2,3) - 77*Power(s2,2)*t1*Power(t2,3) + 
            11*Power(t1,2)*Power(t2,3) + 
            127*s2*Power(t1,2)*Power(t2,3) - 
            32*Power(t1,3)*Power(t2,3) + 2568*Power(t2,4) + 
            314*s2*Power(t2,4) - 374*Power(s2,2)*Power(t2,4) + 
            11*Power(s2,3)*Power(t2,4) + 890*t1*Power(t2,4) - 
            233*s2*t1*Power(t2,4) - 41*Power(s2,2)*t1*Power(t2,4) + 
            54*Power(t1,2)*Power(t2,4) - s2*Power(t1,2)*Power(t2,4) - 
            1084*Power(t2,5) + 434*s2*Power(t2,5) + 
            80*Power(s2,2)*Power(t2,5) - Power(s2,3)*Power(t2,5) - 
            324*t1*Power(t2,5) + 59*s2*t1*Power(t2,5) + 
            3*Power(s2,2)*t1*Power(t2,5) + 126*Power(t2,6) - 
            102*s2*Power(t2,6) - 4*Power(s2,2)*Power(t2,6) + 
            26*t1*Power(t2,6) - 2*s2*t1*Power(t2,6) - 6*Power(t2,7) + 
            2*s2*Power(t2,7) + 
            2*Power(s1,6)*(-40 + 165*t2 - 210*Power(t2,2) + 
               84*Power(t2,3) - 7*t1*(5 - 16*t2 + 12*Power(t2,2)) + 
               s2*(20 - 70*t2 + 56*Power(t2,2))) + 
            Power(s1,5)*(-46 + Power(s2,2)*(43 - 93*t2) + 240*t2 - 
               588*Power(t2,2) + 732*Power(t2,3) - 329*Power(t2,4) - 
               7*Power(t1,2)*(-19 + 30*t2) + 
               t1*(165 - 475*t2 + 196*Power(t2,2) + 182*Power(t2,3)) + 
               s2*(-41 + 15*t2 + 275*Power(t2,2) - 287*Power(t2,3) + 
                  2*t1*(-78 + 133*t2 + 14*Power(t2,2)))) - 
            Power(s1,4)*(-71 + 645*t2 - 1191*Power(t2,2) + 
               247*Power(t2,3) + 645*Power(t2,4) - 215*Power(t2,5) - 
               7*Power(t1,3)*(-49 + 30*t2) + 
               Power(s2,3)*(-11 - 7*t2 + 30*Power(t2,2)) + 
               Power(t1,2)*(216 - 254*t2 + 287*Power(t2,2) - 
                  154*Power(t2,3)) + 
               t1*(-311 + 1778*t2 - 2039*Power(t2,2) + 
                  244*Power(t2,3) + 140*Power(t2,4)) + 
               Power(s2,2)*(-67 + 470*t2 - 593*Power(t2,2) + 
                  175*Power(t2,3) + t1*(125 - 16*t2 - 70*Power(t2,2))) \
+ s2*(213 - 1048*t2 + 916*Power(t2,2) + 201*Power(t2,3) - 
                  165*Power(t2,4) + 7*Power(t1,2)*(-56 + 29*t2) + 
                  t1*(32 - 535*t2 + 361*Power(t2,2) + 84*Power(t2,3)))) \
+ Power(s1,3)*(166 - 1907*t2 + 4924*Power(t2,2) - 3999*Power(t2,3) + 
               458*Power(t2,4) + 454*Power(t2,5) - 40*Power(t2,6) + 
               2*Power(s2,3)*
                (6 - 35*t2 + 9*Power(t2,2) + 5*Power(t2,3)) - 
               7*Power(t1,3)*
                (47 - 74*t2 + 3*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(-170 + 1324*t2 - 853*Power(t2,2) + 
                  318*Power(t2,3) + 21*Power(t2,4)) + 
               t1*(344 - 1854*t2 + 3454*Power(t2,2) - 
                  1525*Power(t2,3) - 70*Power(t2,4) + 5*Power(t2,5)) - 
               2*Power(s2,2)*
                (t2*(90 - 615*t2 + 522*Power(t2,2) - 95*Power(t2,3)) + 
                  t1*(59 - 147*t2 - 50*Power(t2,2) + 37*Power(t2,3))) + 
               s2*(-225 + 1782*t2 - 3659*Power(t2,2) + 
                  1827*Power(t2,3) - 168*Power(t2,4) + 15*Power(t2,5) + 
                  2*Power(t1,2)*
                   (170 - 299*t2 - 51*Power(t2,2) + 35*Power(t2,3)) + 
                  t1*(44 - 699*t2 - 447*Power(t2,2) + 465*Power(t2,3) - 
                     27*Power(t2,4)))) + 
            Power(s1,2)*(-52 - 1683*t2 + 7050*Power(t2,2) - 
               8678*Power(t2,3) + 3320*Power(t2,4) - 123*Power(t2,5) - 
               112*Power(t2,6) + 
               2*Power(s2,3)*t2*
                (-27 + 78*t2 - 44*Power(t2,2) + 4*Power(t2,3)) - 
               3*Power(t1,3)*
                (77 - 150*t2 + 31*Power(t2,2) + 32*Power(t2,3)) + 
               Power(t1,2)*(-74 + 1269*t2 - 1344*Power(t2,2) + 
                  193*Power(t2,3) + 80*Power(t2,4)) + 
               t1*(187 - 1744*t2 + 3154*Power(t2,2) - 
                  2488*Power(t2,3) + 807*Power(t2,4) - 102*Power(t2,5)) \
+ Power(s2,2)*(1 + 391*t2 - 249*Power(t2,2) - 954*Power(t2,3) + 
                  596*Power(t2,4) - 45*Power(t2,5) + 
                  t1*(-61 + 269*t2 - 70*Power(t2,2) - 
                     103*Power(t2,3) + 3*Power(t2,4))) + 
               s2*(33 + 792*t2 - 2836*Power(t2,2) + 3860*Power(t2,3) - 
                  1677*Power(t2,4) + 366*Power(t2,5) - 8*Power(t2,6) + 
                  Power(t1,2)*
                   (224 - 553*t2 + 15*Power(t2,2) + 211*Power(t2,3) + 
                     15*Power(t2,4)) - 
                  t1*(37 + 849*t2 - 701*Power(t2,2) - 835*Power(t2,3) + 
                     520*Power(t2,4) + 14*Power(t2,5)))) + 
            s1*(18 + 474*t2 + 3058*Power(t2,2) - 7651*Power(t2,3) + 
               5209*Power(t2,4) - 1111*Power(t2,5) + 42*Power(t2,6) + 
               4*Power(t2,7) + 
               2*Power(s2,3)*Power(t2,2)*(11 - 56*t2 + 24*Power(t2,2)) - 
               Power(t1,3)*(107 - 274*t2 + 87*Power(t2,2) + 
                  72*Power(t2,3)) + 
               Power(t1,2)*(37 + 724*t2 - 989*Power(t2,2) + 
                  124*Power(t2,3) + 109*Power(t2,4)) + 
               t1*(-28 - 1123*t2 + 2348*Power(t2,2) - 
                  2159*Power(t2,3) + 1375*Power(t2,4) - 
                  351*Power(t2,5) + 16*Power(t2,6)) + 
               Power(s2,2)*t2*
                (43 - 1138*t2 + 794*Power(t2,2) + 127*Power(t2,3) - 
                  52*Power(t2,4) + 4*Power(t2,5) - 
                  t1*(-157 + 28*t2 + 66*Power(t2,2) + 26*Power(t2,3) + 
                     3*Power(t2,4))) + 
               s2*(7 - 194*t2 + 950*Power(t2,2) + 498*Power(t2,3) - 
                  1829*Power(t2,4) + 772*Power(t2,5) - 90*Power(t2,6) + 
                  2*Power(t1,2)*
                   (46 - 208*t2 + 42*Power(t2,2) + 100*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  t1*(-69 - 140*t2 + 749*Power(t2,2) + 301*Power(t2,3) - 
                     618*Power(t2,4) + 71*Power(t2,5))))) + 
         Power(s,2)*Power(-1 + s1,2)*
          (4 - 15*t1 + 18*Power(t1,2) - 7*Power(t1,3) - 70*t2 + 
            16*s2*t2 + 190*t1*t2 - 33*s2*t1*t2 - 165*Power(t1,2)*t2 + 
            17*s2*Power(t1,2)*t2 + 45*Power(t1,3)*t2 + 185*Power(t2,2) - 
            72*s2*Power(t2,2) - 6*Power(s2,2)*Power(t2,2) - 
            364*t1*Power(t2,2) + 161*s2*t1*Power(t2,2) + 
            8*Power(s2,2)*t1*Power(t2,2) + 232*Power(t1,2)*Power(t2,2) - 
            105*s2*Power(t1,2)*Power(t2,2) - 
            35*Power(t1,3)*Power(t2,2) - 251*Power(t2,3) + 
            201*s2*Power(t2,3) + 35*Power(s2,2)*Power(t2,3) - 
            16*Power(s2,3)*Power(t2,3) + 171*t1*Power(t2,3) - 
            276*s2*t1*Power(t2,3) + 47*Power(s2,2)*t1*Power(t2,3) + 
            6*Power(t1,2)*Power(t2,3) + 91*s2*Power(t1,2)*Power(t2,3) - 
            16*Power(t1,3)*Power(t2,3) + 219*Power(t2,4) - 
            253*s2*Power(t2,4) + 46*Power(s2,2)*Power(t2,4) + 
            9*Power(s2,3)*Power(t2,4) + 40*t1*Power(t2,4) + 
            33*s2*t1*Power(t2,4) - 69*Power(s2,2)*t1*Power(t2,4) - 
            58*Power(t1,2)*Power(t2,4) + 31*s2*Power(t1,2)*Power(t2,4) - 
            101*Power(t2,5) + 134*s2*Power(t2,5) - 
            54*Power(s2,2)*Power(t2,5) + 15*Power(s2,3)*Power(t2,5) - 
            24*t1*Power(t2,5) + 51*s2*t1*Power(t2,5) - 
            15*Power(s2,2)*t1*Power(t2,5) + 16*Power(t2,6) - 
            22*s2*Power(t2,6) + 12*Power(s2,2)*Power(t2,6) + 
            2*t1*Power(t2,6) - 2*s2*t1*Power(t2,6) - 2*Power(t2,7) - 
            4*s2*Power(t2,7) + 
            Power(s1,7)*(Power(t1,2)*(11 - 13*t2) + 
               2*Power(-1 + t2,3) - t1*(-1 + t2)*Power(1 + t2,2) + 
               Power(s2,2)*(9 - 13*t2 + 2*Power(t2,2)) - 
               s2*(-5 + 15*t2 - 11*Power(t2,2) + Power(t2,3) + 
                  2*t1*(10 - 13*t2 + Power(t2,2)))) + 
            Power(s1,6)*(Power(t1,3)*(68 - 45*t2) - 
               Power(-1 + t2,2)*(11 - 7*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(-17 - 3*t2 + 2*Power(t2,2)) - 
               t1*(13 - 8*t2 - 6*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(54 - 96*t2 + 56*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(s2,2)*(1 + 2*t2 + 25*Power(t2,2) - 
                  9*Power(t2,3) + t1*(93 - 32*t2 - 2*Power(t2,2))) + 
               s2*(27 - 66*t2 + 70*Power(t2,2) - 33*Power(t2,3) + 
                  2*Power(t2,4) + 16*Power(t1,2)*(-9 + 5*t2) - 
                  t1*(28 - 33*t2 + 40*Power(t2,2) + 3*Power(t2,3)))) + 
            Power(s1,4)*(Power(t1,3)*(21 + 143*t2 + Power(t2,2)) + 
               Power(t1,2)*(-30 + 235*t2 + 4*Power(t2,2) + 
                  68*Power(t2,3) - 2*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (11 - 12*t2 - 98*Power(t2,2) - 5*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,3)*(3 + t2 - 138*Power(t2,2) - 
                  16*Power(t2,3) + 10*Power(t2,4)) + 
               t1*(-36 - 122*t2 + 280*Power(t2,2) - 127*Power(t2,3) + 
                  Power(t2,4) + 4*Power(t2,5)) - 
               s2*(-38 - 318*t2 + 626*Power(t2,2) - 181*Power(t2,3) - 
                  135*Power(t2,4) + 46*Power(t2,5) + 
                  t1*(-57 + 70*t2 + 695*Power(t2,2) - 
                     120*Power(t2,3) - 38*Power(t2,4)) + 
                  Power(t1,2)*
                   (16 + 295*t2 + 129*Power(t2,2) + 29*Power(t2,3) + 
                     Power(t2,4))) + 
               Power(s2,2)*(-18 - 173*t2 + 688*Power(t2,2) - 
                  199*Power(t2,3) - 18*Power(t2,4) - 5*Power(t2,5) + 
                  t1*(-8 + 123*t2 + 336*Power(t2,2) - 11*Power(t2,3) + 
                     5*Power(t2,4)))) + 
            Power(s1,5)*(Power(s2,3)*
                (-10 + 92*t2 + 8*Power(t2,2) - 8*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (18 - 63*t2 + 2*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(-129 + 4*t2 + 15*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(5 - 171*t2 + 127*Power(t2,2) - 
                  52*Power(t2,3) - 7*Power(t2,4)) + 
               t1*(73 - 141*t2 + 103*Power(t2,2) - 38*Power(t2,3) + 
                  2*Power(t2,4) + Power(t2,5)) + 
               Power(s2,2)*(127 - 301*t2 + 64*Power(t2,2) + 
                  12*Power(t2,4) - 
                  2*t1*(26 + 138*t2 - 32*Power(t2,2) + Power(t2,3))) + 
               s2*(-84 + 79*t2 + 108*Power(t2,2) - 154*Power(t2,3) + 
                  52*Power(t2,4) - Power(t2,5) - 
                  2*Power(t1,2)*
                   (-102 - 75*t2 + 33*Power(t2,2) + Power(t2,3)) + 
                  t1*(-178 + 539*t2 - 177*Power(t2,2) + 7*Power(t2,3) + 
                     5*Power(t2,4)))) - 
            Power(s1,3)*(Power(t1,3)*
                (-38 + 48*t2 + 58*Power(t2,2) + 32*Power(t2,3)) + 
               Power(t1,2)*(95 - 89*t2 + 318*Power(t2,2) + 
                  60*Power(t2,3) + 46*Power(t2,4)) + 
               t1*(-92 + 45*t2 + 9*Power(t2,2) + 17*Power(t2,3) + 
                  55*Power(t2,4) - 34*Power(t2,5)) + 
               Power(-1 + t2,2)*
                (-22 + 15*t2 - 100*Power(t2,2) - 77*Power(t2,3) + 
                  2*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,3)*(-8 - 22*t2 + 28*Power(t2,2) - 
                  82*Power(t2,3) - 20*Power(t2,4) + 4*Power(t2,5)) - 
               s2*(-156 + 86*t2 - 219*Power(t2,2) + 647*Power(t2,3) - 
                  345*Power(t2,4) - 33*Power(t2,5) + 20*Power(t2,6) + 
                  4*Power(t1,2)*
                   (-4 - 12*t2 + 54*Power(t2,2) + 30*Power(t2,3) + 
                     7*Power(t2,4)) + 
                  t1*(151 - 27*t2 + 306*Power(t2,2) + 
                     392*Power(t2,3) + 69*Power(t2,4) - 31*Power(t2,5))\
) + Power(s2,2)*(2 + 131*t2 - 92*Power(t2,2) + 526*Power(t2,3) - 
                  131*Power(t2,4) - 6*Power(t2,5) + 
                  t1*(40 - 79*t2 + 104*Power(t2,2) + 202*Power(t2,3) + 
                     32*Power(t2,4) + Power(t2,5)))) + 
            Power(s1,2)*(Power(t1,3)*
                (30 - 79*t2 + 18*Power(t2,2) + 16*Power(t2,3)) - 
               Power(s2,3)*t2*
                (32 + 44*t2 - 108*Power(t2,2) + 33*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(t1,2)*(-58 + 194*t2 + 68*Power(t2,2) + 
                  105*Power(t2,3) + 68*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (26 - 159*t2 - 19*Power(t2,2) - 78*Power(t2,3) - 
                  32*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(20 - 108*t2 + 18*Power(t2,2) + 29*Power(t2,3) + 
                  19*Power(t2,4) + 28*Power(t2,5) - 6*Power(t2,6)) + 
               Power(s2,2)*(-5 + 45*t2 + 333*Power(t2,2) - 
                  191*Power(t2,3) + 190*Power(t2,4) + 5*Power(t2,5) + 
                  t1*(7 + 121*t2 - 174*Power(t2,2) - 4*Power(t2,3) + 
                     44*Power(t2,4) + 11*Power(t2,5))) - 
               s2*(-5 - 418*t2 + 612*Power(t2,2) - 223*Power(t2,3) + 
                  134*Power(t2,4) - 118*Power(t2,5) + 14*Power(t2,6) + 
                  4*Power(t2,7) + 
                  2*Power(t1,2)*
                   (8 + t2 - 57*Power(t2,2) + 27*Power(t2,3) + 
                     11*Power(t2,4)) + 
                  t1*(5 + 358*t2 - 50*Power(t2,2) + 217*Power(t2,3) + 
                     183*Power(t2,4) + 47*Power(t2,5) - 6*Power(t2,6)))) \
+ s1*(2*Power(s2,3)*Power(t2,2)*
                (20 + 5*t2 - 43*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,3)*(-21 - 20*t2 + 59*Power(t2,2) + 
                  24*Power(t2,3)) + 
               Power(t1,2)*(95 - 73*t2 - 169*Power(t2,2) - 
                  72*Power(t2,3) + 45*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (48 - 64*t2 + 204*Power(t2,2) + 3*Power(t2,3) + 
                  18*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(-122 + 217*t2 - 33*Power(t2,2) - 16*Power(t2,3) - 
                  7*Power(t2,4) - 43*Power(t2,5) + 4*Power(t2,6)) + 
               Power(s2,2)*t2*
                (11 - 78*t2 - 230*Power(t2,2) + 199*Power(t2,3) - 
                  64*Power(t2,4) - 12*Power(t2,5) + 
                  t1*(-15 - 128*t2 + 172*Power(t2,2) + 52*Power(t2,3) + 
                     5*Power(t2,4))) + 
               s2*(-11 + 44*t2 - 420*Power(t2,2) + 696*Power(t2,3) - 
                  337*Power(t2,4) + 4*Power(t2,5) + 16*Power(t2,6) + 
                  8*Power(t2,7) - 
                  2*Power(t1,2)*
                   (6 - 49*t2 + 15*Power(t2,2) + 63*Power(t2,3) + 
                     18*Power(t2,4)) + 
                  t1*(23 - 110*t2 + 397*Power(t2,2) - 23*Power(t2,3) + 
                     38*Power(t2,4) + 27*Power(t2,5) - 4*Power(t2,6))))) \
- Power(s,4)*(15*t1 - 42*Power(t1,2) + 28*Power(t1,3) + 178*t2 - 
            70*s2*t2 - 624*t1*t2 + 209*s2*t1*t2 + 547*Power(t1,2)*t2 - 
            140*s2*Power(t1,2)*t2 - 84*Power(t1,3)*t2 - 
            918*Power(t2,2) + 357*s2*Power(t2,2) - 
            22*Power(s2,2)*Power(t2,2) + 1195*t1*Power(t2,2) - 
            226*s2*t1*Power(t2,2) - 56*Power(s2,2)*t1*Power(t2,2) - 
            691*Power(t1,2)*Power(t2,2) + 
            315*s2*Power(t1,2)*Power(t2,2) + 
            29*Power(t1,3)*Power(t2,2) + 1722*Power(t2,3) - 
            2570*s2*Power(t2,3) + 135*Power(s2,2)*Power(t2,3) + 
            90*Power(s2,3)*Power(t2,3) - 234*t1*Power(t2,3) + 
            385*s2*t1*Power(t2,3) - 121*Power(s2,2)*t1*Power(t2,3) - 
            213*Power(t1,2)*Power(t2,3) - 
            53*s2*Power(t1,2)*Power(t2,3) + 24*Power(t1,3)*Power(t2,3) - 
            947*Power(t2,4) + 2611*s2*Power(t2,4) - 
            490*Power(s2,2)*Power(t2,4) - 20*Power(s2,3)*Power(t2,4) + 
            344*t1*Power(t2,4) + 122*s2*t1*Power(t2,4) + 
            35*Power(s2,2)*t1*Power(t2,4) + 
            142*Power(t1,2)*Power(t2,4) - 
            65*s2*Power(t1,2)*Power(t2,4) - 256*Power(t2,5) - 
            783*s2*Power(t2,5) + 230*Power(s2,2)*Power(t2,5) - 
            15*Power(s2,3)*Power(t2,5) - 366*t1*Power(t2,5) - 
            25*s2*t1*Power(t2,5) + 30*Power(s2,2)*t1*Power(t2,5) + 
            230*Power(t2,6) + 12*s2*Power(t2,6) - 
            40*Power(s2,2)*Power(t2,6) + 56*t1*Power(t2,6) - 
            12*Power(t2,7) + 10*s2*Power(t2,7) + 
            2*Power(s1,8)*(-1 + t2)*
             (4 + t1*(7 - 9*t2) - 7*t2 + 3*Power(t2,2) + s2*(-6 + 8*t2)) \
+ Power(s1,7)*(76 - 247*t2 + 236*Power(t2,2) - 42*Power(t2,3) - 
               21*Power(t2,4) + Power(t1,2)*(7 + 12*t2) + 
               Power(s2,2)*(10 + 19*t2 - 28*Power(t2,2)) + 
               t1*(99 - 223*t2 + 98*Power(t2,2) + 44*Power(t2,3)) + 
               s2*(-46 + 88*t2 + 9*Power(t2,2) - 69*Power(t2,3) + 
                  2*t1*(-8 - 21*t2 + 20*Power(t2,2)))) + 
            Power(s1,6)*(130 - 657*t2 + 1094*Power(t2,2) - 
               700*Power(t2,3) + 103*Power(t2,4) + 19*Power(t2,5) + 
               21*Power(t1,3)*(-17 + 10*t2) - 
               5*Power(s2,3)*(-7 - 8*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(-504 + 688*t2 - 371*Power(t2,2) + 
                  28*Power(t2,3)) + 
               t1*(57 - 412*t2 + 425*Power(t2,2) - 132*Power(t2,3) - 
                  28*Power(t2,4)) + 
               Power(s2,2)*(12 - 261*t2 + 135*Power(t2,2) + 
                  35*Power(t2,3) + 2*t1*(-135 + t2 + 21*Power(t2,2))) + 
               s2*(-88 + 407*t2 - 385*Power(t2,2) + 76*Power(t2,3) + 
                  81*Power(t2,4) - 7*Power(t1,2)*(-82 + 35*t2) + 
                  t1*(298 + 34*t2 - 67*Power(t2,2) - 42*Power(t2,3)))) + 
            Power(s1,5)*(162 - 773*t2 + 1815*Power(t2,2) - 
               1770*Power(t2,3) + 606*Power(t2,4) + 2*Power(t2,5) - 
               4*Power(t2,6) + 
               Power(t1,3)*(213 + 266*t2 - 21*Power(t2,2) - 
                  56*Power(t2,3)) + 
               Power(s2,3)*(38 - 178*t2 - 85*Power(t2,2) + 
                  65*Power(t2,3)) + 
               Power(t1,2)*(-125 + 1490*t2 - 741*Power(t2,2) + 
                  164*Power(t2,3) + 35*Power(t2,4)) - 
               t1*(147 - 781*t2 + 122*Power(t2,2) + 149*Power(t2,3) - 
                  10*Power(t2,4) + 9*Power(t2,5)) + 
               Power(s2,2)*(-107 + 319*t2 + 749*Power(t2,2) - 
                  429*Power(t2,3) + 5*Power(t2,4) + 
                  t1*(-13 + 717*t2 + 34*Power(t2,2) - 46*Power(t2,3))) \
+ s2*(-20 + 25*t2 - 703*Power(t2,2) + 531*Power(t2,3) - 
                  140*Power(t2,4) - 41*Power(t2,5) + 
                  6*Power(t1,2)*
                   (-44 - 110*t2 - 10*Power(t2,2) + 7*Power(t2,3)) + 
                  t1*(469 - 2222*t2 + 31*Power(t2,2) + 
                     379*Power(t2,3) + 29*Power(t2,4)))) - 
            Power(s1,4)*(-308 + 1839*t2 - 2732*Power(t2,2) + 
               2354*Power(t2,3) - 1387*Power(t2,4) + 295*Power(t2,5) + 
               52*Power(t2,6) + 
               Power(t1,3)*(-42 + 240*t2 + 95*Power(t2,2) + 
                  40*Power(t2,3)) + 
               Power(t1,2)*(346 - 267*t2 + 1041*Power(t2,2) + 
                  253*Power(t2,3) - 90*Power(t2,4)) + 
               Power(s2,3)*(-51 + 221*t2 - 334*Power(t2,2) - 
                  68*Power(t2,3) + 47*Power(t2,4)) + 
               t1*(190 - 436*t2 + 1411*Power(t2,2) + 130*Power(t2,3) - 
                  535*Power(t2,4) + 66*Power(t2,5)) + 
               Power(s2,2)*(164 - 451*t2 + 472*Power(t2,2) + 
                  1101*Power(t2,3) - 446*Power(t2,4) + 
                  25*Power(t2,5) + 
                  t1*(43 - 158*t2 + 698*Power(t2,2) + 97*Power(t2,3) - 
                     20*Power(t2,4))) - 
               s2*(53 + 447*t2 - 627*Power(t2,2) + 1130*Power(t2,3) - 
                  426*Power(t2,4) + 168*Power(t2,5) + 12*Power(t2,6) + 
                  Power(t1,2)*
                   (-12 + 214*t2 + 411*Power(t2,2) + 195*Power(t2,3) + 
                     15*Power(t2,4)) + 
                  t1*(408 - 765*t2 + 2248*Power(t2,2) + 
                     733*Power(t2,3) - 514*Power(t2,4) - 35*Power(t2,5))\
)) + Power(s1,3)*(299 - 763*t2 + 3060*Power(t2,2) - 4032*Power(t2,3) + 
               2186*Power(t2,4) - 714*Power(t2,5) + 140*Power(t2,6) + 
               18*Power(t2,7) + 
               Power(t1,3)*(26 - 76*t2 + 62*Power(t2,2) + 
                  48*Power(t2,3)) + 
               Power(t1,2)*(-323 + 652*t2 - 174*Power(t2,2) + 
                  140*Power(t2,3) + 178*Power(t2,4)) + 
               2*Power(s2,3)*
                (10 - 128*t2 + 261*Power(t2,2) - 195*Power(t2,3) - 
                  4*Power(t2,4) + 6*Power(t2,5)) + 
               t1*(-74 + 631*t2 - 768*Power(t2,2) + 250*Power(t2,3) + 
                  873*Power(t2,4) - 390*Power(t2,5) + 16*Power(t2,6)) + 
               Power(s2,2)*(-257 + 1157*t2 - 1996*Power(t2,2) + 
                  890*Power(t2,3) + 713*Power(t2,4) - 
                  184*Power(t2,5) + 12*Power(t2,6) + 
                  t1*(-3 + 108*t2 - 190*Power(t2,2) + 
                     232*Power(t2,3) + 67*Power(t2,4) - 14*Power(t2,5))\
) + s2*(422 - 1846*t2 + 1697*Power(t2,2) + 693*Power(t2,3) - 
                  1476*Power(t2,4) + 210*Power(t2,5) - 90*Power(t2,6) - 
                  4*Power(t1,2)*
                   (2 - t2 - 3*Power(t2,2) + 32*Power(t2,3) + 
                     11*Power(t2,4)) + 
                  2*t1*(177 - 330*t2 + 255*Power(t2,2) - 
                     259*Power(t2,3) - 323*Power(t2,4) + 97*Power(t2,5))\
)) + Power(s1,2)*(-268 + 62*t2 + 480*Power(t2,2) - 1970*Power(t2,3) + 
               2579*Power(t2,4) - 1214*Power(t2,5) + 194*Power(t2,6) - 
               24*Power(t2,7) + 
               Power(t1,3)*(15 - 46*t2 + 18*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(s2,3)*t2*
                (-9 + 412*t2 - 490*Power(t2,2) + 209*Power(t2,3) - 
                  7*Power(t2,4)) + 
               Power(t1,2)*(-308 + 746*t2 - 465*Power(t2,2) - 
                  18*Power(t2,3) + 80*Power(t2,4)) + 
               t1*(364 + 400*t2 - 423*Power(t2,2) + 248*Power(t2,3) + 
                  485*Power(t2,4) - 448*Power(t2,5) + 48*Power(t2,6)) + 
               Power(s2,2)*(10 + 896*t2 - 2429*Power(t2,2) + 
                  2447*Power(t2,3) - 818*Power(t2,4) - 
                  151*Power(t2,5) + 24*Power(t2,6) - 
                  t1*(55 + 88*t2 + 88*Power(t2,2) - 66*Power(t2,3) - 
                     9*Power(t2,4) + 14*Power(t2,5))) + 
               s2*(5 - 3090*t2 + 5383*Power(t2,2) - 4068*Power(t2,3) + 
                  316*Power(t2,4) + 593*Power(t2,5) - 36*Power(t2,6) + 
                  18*Power(t2,7) + 
                  Power(t1,2)*
                   (30 + 3*t2 + 66*Power(t2,2) - 38*Power(t2,3) - 
                     22*Power(t2,4)) + 
                  t1*(214 - 566*t2 + 325*Power(t2,2) + 60*Power(t2,3) - 
                     144*Power(t2,4) + 156*Power(t2,5) - 24*Power(t2,6))\
)) - s1*(75 - 961*t2 + 1855*Power(t2,2) - 692*Power(t2,3) - 
               939*Power(t2,4) + 854*Power(t2,5) - 260*Power(t2,6) + 
               14*Power(t2,7) - 
               Power(t1,3)*(33 - 30*t2 + 7*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (97 + 207*t2 - 202*Power(t2,2) + 38*Power(t2,3)) + 
               Power(t1,2)*(279 - 718*t2 + 357*Power(t2,2) - 
                  152*Power(t2,3) - 115*Power(t2,4)) + 
               t1*(-306 + 1085*t2 + 160*Power(t2,2) + 281*Power(t2,3) - 
                  437*Power(t2,4) + 321*Power(t2,5) - 40*Power(t2,6)) + 
               Power(s2,2)*t2*
                (5 + 769*t2 - 2023*Power(t2,2) + 1184*Power(t2,3) - 
                  210*Power(t2,4) - 4*Power(t2,5) + 
                  t1*(-127 - 188*t2 + 34*Power(t2,2) + 3*Power(t2,3) + 
                     2*Power(t2,4))) + 
               s2*(-30 + 205*t2 - 5101*Power(t2,2) + 6283*Power(t2,3) - 
                  2794*Power(t2,4) + 195*Power(t2,5) + 26*Power(t2,6) - 
                  4*Power(t2,7) + 
                  2*Power(t1,2)*
                   (-32 + 100*t2 - 12*Power(t2,2) + 9*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  t1*(95 + 276*t2 + 13*Power(t2,2) + 101*Power(t2,3) + 
                     223*Power(t2,4) - 30*Power(t2,5) + 8*Power(t2,6))))) \
+ Power(s,5)*(5 - 15*t1 + 14*Power(t1,3) + 91*t2 - 50*s2*t2 - 
            494*t1*t2 + 225*s2*t1*t2 + 487*Power(t1,2)*t2 - 
            196*s2*Power(t1,2)*t2 - 1068*Power(t2,2) + 
            481*s2*Power(t2,2) - 75*Power(s2,2)*Power(t2,2) + 
            1261*t1*Power(t2,2) - 45*s2*t1*Power(t2,2) - 
            70*Power(s2,2)*t1*Power(t2,2) - 
            608*Power(t1,2)*Power(t2,2) + 
            273*s2*Power(t1,2)*Power(t2,2) - 
            29*Power(t1,3)*Power(t2,2) + 717*Power(t2,3) - 
            3272*s2*Power(t2,3) + 521*Power(s2,2)*Power(t2,3) + 
            90*Power(s2,3)*Power(t2,3) - 486*t1*Power(t2,3) + 
            97*s2*t1*Power(t2,3) - 118*Power(s2,2)*t1*Power(t2,3) - 
            157*Power(t1,2)*Power(t2,3) + 
            82*s2*Power(t1,2)*Power(t2,3) - 10*Power(t1,3)*Power(t2,3) + 
            1157*Power(t2,4) + 2174*s2*Power(t2,4) - 
            607*Power(s2,2)*Power(t2,4) + Power(s2,3)*Power(t2,4) + 
            879*t1*Power(t2,4) - 52*s2*t1*Power(t2,4) - 
            32*Power(s2,2)*t1*Power(t2,4) + 
            108*Power(t1,2)*Power(t2,4) - 
            30*s2*Power(t1,2)*Power(t2,4) - 1285*Power(t2,5) - 
            150*s2*Power(t2,5) + 193*Power(s2,2)*Power(t2,5) - 
            6*Power(s2,3)*Power(t2,5) - 512*t1*Power(t2,5) + 
            58*s2*t1*Power(t2,5) + 15*Power(s2,2)*t1*Power(t2,5) + 
            316*Power(t2,6) - 104*s2*Power(t2,6) - 
            22*Power(s2,2)*Power(t2,6) + 60*t1*Power(t2,6) - 
            4*s2*t1*Power(t2,6) - 16*Power(t2,7) + 6*s2*Power(t2,7) + 
            2*Power(s1,7)*(-20 + 66*t2 - 70*Power(t2,2) + 
               24*Power(t2,3) + t1*(-21 + 56*t2 - 36*Power(t2,2)) + 
               s2*(15 - 42*t2 + 28*Power(t2,2))) + 
            Power(s1,6)*(66 + Power(t1,2)*(77 - 84*t2) - 244*t2 + 
               177*Power(t2,2) + 129*Power(t2,3) - 119*Power(t2,4) - 
               4*Power(s2,2)*(-10 + 10*t2 + 7*Power(t2,2)) + 
               2*t1*(95 - 235*t2 + 105*Power(t2,2) + 56*Power(t2,3)) + 
               s2*(-54 + 50*t2 + 173*Power(t2,2) - 203*Power(t2,3) + 
                  2*t1*(-55 + 49*t2 + 28*Power(t2,2)))) - 
            Power(s1,5)*(-202 + 1134*t2 - 1941*Power(t2,2) + 
               1106*Power(t2,3) + 30*Power(t2,4) - 93*Power(t2,5) - 
               7*Power(t1,3)*(-61 + 36*t2) + 
               Power(s2,3)*(-26 - 29*t2 + 40*Power(t2,2)) + 
               Power(t1,2)*(515 - 719*t2 + 427*Power(t2,2) - 
                  98*Power(t2,3)) + 
               t1*(-257 + 1304*t2 - 1337*Power(t2,2) + 
                  239*Power(t2,3) + 84*Power(t2,4)) + 
               Power(s2,2)*(-71 + 489*t2 - 455*Power(t2,2) + 
                  63*Power(t2,3) + t1*(233 + 2*t2 - 70*Power(t2,2))) + 
               s2*(193 - 753*t2 + 490*Power(t2,2) + 200*Power(t2,3) - 
                  185*Power(t2,4) + 7*Power(t1,2)*(-84 + 37*t2) + 
                  t1*(-207 - 230*t2 + 234*Power(t2,2) + 84*Power(t2,3)))\
) + Power(s1,4)*(409 - 2288*t2 + 4646*Power(t2,2) - 3915*Power(t2,3) + 
               1071*Power(t2,4) + 136*Power(t2,5) - 20*Power(t2,6) + 
               Power(s2,3)*(40 - 140*t2 - 31*Power(t2,2) + 
                  50*Power(t2,3)) - 
               Power(t1,3)*(166 - 532*t2 + 21*Power(t2,2) + 
                  70*Power(t2,3)) + 
               Power(t1,2)*(-405 + 2143*t2 - 1128*Power(t2,2) + 
                  235*Power(t2,3) + 35*Power(t2,4)) - 
               t1*(-226 + 549*t2 - 1604*Power(t2,2) + 
                  932*Power(t2,3) + 5*Power(t2,4) + 5*Power(t2,5)) + 
               Power(s2,2)*(-17 - 195*t2 + 1527*Power(t2,2) - 
                  995*Power(t2,3) + 130*Power(t2,4) + 
                  t1*(-167 + 551*t2 + 101*Power(t2,2) - 80*Power(t2,3))\
) + s2*(-268 + 1250*t2 - 2292*Power(t2,2) + 902*Power(t2,3) + 
                  45*Power(t2,4) - 50*Power(t2,5) + 
                  Power(t1,2)*
                   (240 - 792*t2 - 129*Power(t2,2) + 70*Power(t2,3)) + 
                  t1*(422 - 1937*t2 - 271*Power(t2,2) + 
                     565*Power(t2,3) + 20*Power(t2,4)))) - 
            Power(s1,3)*(-437 + 3310*t2 - 7803*Power(t2,2) + 
               7649*Power(t2,3) - 3284*Power(t2,4) + 481*Power(t2,5) + 
               88*Power(t2,6) + 
               2*Power(t1,3)*
                (53 - 126*t2 + 58*Power(t2,2) + 50*Power(t2,3)) + 
               Power(t1,2)*(503 - 1818*t2 + 1874*Power(t2,2) + 
                  64*Power(t2,3) - 125*Power(t2,4)) + 
               Power(s2,3)*(-26 + 217*t2 - 338*Power(t2,2) + 
                  49*Power(t2,3) + 18*Power(t2,4)) + 
               t1*(-242 + 685*t2 - 487*Power(t2,2) + 
                  1432*Power(t2,3) - 925*Power(t2,4) + 119*Power(t2,5)) \
+ Power(s2,2)*(135 - 527*t2 - 135*Power(t2,2) + 1785*Power(t2,3) - 
                  798*Power(t2,4) + 60*Power(t2,5) + 
                  t1*(117 - 410*t2 + 361*Power(t2,2) + 
                     124*Power(t2,3) - 20*Power(t2,4))) - 
               s2*(-129 + 738*t2 - 2266*Power(t2,2) + 
                  3185*Power(t2,3) - 1017*Power(t2,4) + 
                  195*Power(t2,5) + 8*Power(t2,6) + 
                  2*Power(t1,2)*
                   (91 - 225*t2 + 87*Power(t2,2) + 126*Power(t2,3) + 
                     10*Power(t2,4)) + 
                  t1*(443 - 1827*t2 + 1688*Power(t2,2) + 
                     1182*Power(t2,3) - 641*Power(t2,4) - 35*Power(t2,5)\
))) + Power(s1,2)*(-184 - 1659*t2 + 6487*Power(t2,2) - 
               9499*Power(t2,3) + 5941*Power(t2,4) - 1513*Power(t2,5) + 
               152*Power(t2,6) + 14*Power(t2,7) - 
               2*Power(t1,3)*
                (32 - 78*t2 + 27*Power(t2,2) + 24*Power(t2,3)) + 
               Power(s2,3)*t2*
                (-68 + 377*t2 - 366*Power(t2,2) + 65*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(t1,2)*(-376 + 1662*t2 - 1528*Power(t2,2) + 
                  26*Power(t2,3) + 217*Power(t2,4)) + 
               t1*(407 - 985*t2 + 701*Power(t2,2) - 1170*Power(t2,3) + 
                  1682*Power(t2,4) - 525*Power(t2,5) + 24*Power(t2,6)) \
+ Power(s2,2)*(5 + 971*t2 - 1852*Power(t2,2) + 576*Power(t2,3) + 
                  723*Power(t2,4) - 203*Power(t2,5) + 12*Power(t2,6) + 
                  t1*(-75 + 205*t2 - 199*Power(t2,2) + 
                     30*Power(t2,3) + 18*Power(t2,4) - 11*Power(t2,5))) \
+ s2*(38 - 1234*t2 + 2114*Power(t2,2) + 613*Power(t2,3) - 
                  2205*Power(t2,4) + 598*Power(t2,5) - 98*Power(t2,6) - 
                  2*Power(t1,2)*
                   (-74 + 182*t2 - 60*Power(t2,2) - 68*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  2*t1*(57 - 706*t2 + 698*Power(t2,2) + 
                     207*Power(t2,3) - 443*Power(t2,4) + 74*Power(t2,5))\
)) - s1*(11 - 984*t2 - 818*Power(t2,2) + 4845*Power(t2,3) - 
               5228*Power(t2,4) + 2170*Power(t2,5) - 344*Power(t2,6) + 
               14*Power(t2,7) + 
               Power(s2,3)*Power(t2,2)*
                (28 + 213*t2 - 140*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,3)*(19 - 88*t2 + 36*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,2)*(166 - 1063*t2 + 1155*Power(t2,2) + 
                  10*Power(t2,3) - 187*Power(t2,4)) + 
               t1*(-143 + 1513*t2 - 1080*Power(t2,2) + 
                  1165*Power(t2,3) - 1499*Power(t2,4) + 
                  695*Power(t2,5) - 60*Power(t2,6)) + 
               Power(s2,2)*t2*
                (t1*(-180 - 43*t2 + 28*Power(t2,2) + 22*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  2*(-15 + 733*t2 - 965*Power(t2,2) + 300*Power(t2,3) + 
                     11*Power(t2,4) - 5*Power(t2,5))) + 
               s2*(-20 + 291*t2 - 4648*Power(t2,2) + 4553*Power(t2,3) - 
                  422*Power(t2,4) - 635*Power(t2,5) + 126*Power(t2,6) - 
                  10*Power(t2,7) + 
                  Power(t1,2)*
                   (-90 + 371*t2 - 138*Power(t2,2) - 100*Power(t2,3) + 
                     16*Power(t2,4)) + 
                  t1*(100 + 305*t2 - 818*Power(t2,2) - 130*Power(t2,3) + 
                     505*Power(t2,4) - 149*Power(t2,5) + 12*Power(t2,6)))\
)) + Power(s,3)*(-5 + 27*t1 - 42*Power(t1,2) + 20*Power(t1,3) + 
            2*Power(s1,9)*(s2 - t1)*Power(-1 + t2,2) + 155*t2 - 
            48*s2*t2 - 452*t1*t2 + 113*s2*t1*t2 + 387*Power(t1,2)*t2 - 
            64*s2*Power(t1,2)*t2 - 90*Power(t1,3)*t2 - 513*Power(t2,2) + 
            182*s2*Power(t2,2) + 7*Power(s2,2)*Power(t2,2) + 
            825*t1*Power(t2,2) - 275*s2*t1*Power(t2,2) - 
            28*Power(s2,2)*t1*Power(t2,2) - 506*Power(t1,2)*Power(t2,2) + 
            231*s2*Power(t1,2)*Power(t2,2) + 55*Power(t1,3)*Power(t2,2) + 
            1014*Power(t2,3) - 991*s2*Power(t2,3) - 
            67*Power(s2,2)*Power(t2,3) + 52*Power(s2,3)*Power(t2,3) - 
            232*t1*Power(t2,3) + 463*s2*t1*Power(t2,3) - 
            88*Power(s2,2)*t1*Power(t2,3) - 109*Power(t1,2)*Power(t2,3) - 
            130*s2*Power(t1,2)*Power(t2,3) + 32*Power(t1,3)*Power(t2,3) - 
            1000*Power(t2,4) + 1283*s2*Power(t2,4) - 
            191*Power(s2,2)*Power(t2,4) - 24*Power(s2,3)*Power(t2,4) - 
            25*t1*Power(t2,4) + 67*s2*t1*Power(t2,4) + 
            88*Power(s2,2)*t1*Power(t2,4) + 120*Power(t1,2)*Power(t2,4) - 
            64*s2*Power(t1,2)*Power(t2,4) + 349*Power(t2,5) - 
            581*s2*Power(t2,5) + 143*Power(s2,2)*Power(t2,5) - 
            20*Power(s2,3)*Power(t2,5) - 78*t1*Power(t2,5) - 
            82*s2*t1*Power(t2,5) + 30*Power(s2,2)*t1*Power(t2,5) + 
            64*s2*Power(t2,6) - 32*Power(s2,2)*Power(t2,6) + 
            16*t1*Power(t2,6) + 4*s2*t1*Power(t2,6) + 10*s2*Power(t2,7) - 
            Power(s1,8)*(-24 + Power(t1,2)*(19 - 30*t2) + 73*t2 - 
               71*Power(t2,2) + 21*Power(t2,3) + Power(t2,4) + 
               Power(s2,2)*(11 - 30*t2 + 12*Power(t2,2)) - 
               2*t1*(9 - 22*t2 + 10*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(23 - 61*t2 + 35*Power(t2,2) + 7*Power(t2,3) - 
                  2*t1*(15 - 31*t2 + 7*Power(t2,2)))) + 
            Power(s1,7)*(35 - 157*t2 + 248*Power(t2,2) - 
               161*Power(t2,3) + 34*Power(t2,4) + Power(t2,5) + 
               Power(t1,3)*(-197 + 120*t2) - 
               6*Power(s2,3)*(-5 - 4*t2 + 2*Power(t2,2)) - 
               Power(t1,2)*(227 - 325*t2 + 193*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(9 - 42*t2 + 37*Power(t2,2) - 35*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,2)*(-5 - 80*t2 - 25*Power(t2,2) + 
                  37*Power(t2,3) + 2*t1*(-100 + 13*t2 + 7*Power(t2,2))) \
+ s2*(-43 + Power(t1,2)*(364 - 169*t2) + 175*t2 - 227*Power(t2,2) + 
                  121*Power(t2,3) + 9*Power(t2,4) + 
                  t1*(127 + 17*t2 + 34*Power(t2,2) - 6*Power(t2,3)))) + 
            Power(s1,6)*(-50 - 28*t2 + 345*Power(t2,2) - 
               372*Power(t2,3) + 105*Power(t2,4) - 
               Power(t1,3)*(-398 + 14*t2 + 21*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(s2,3)*(-2 - 167*t2 - 62*Power(t2,2) + 
                  37*Power(t2,3)) + 
               Power(t1,2)*(184 + 391*t2 - 218*Power(t2,2) + 
                  123*Power(t2,3) + 21*Power(t2,4)) - 
               t1*(182 - 585*t2 + 330*Power(t2,2) - 106*Power(t2,3) + 
                  Power(t2,4) + 5*Power(t2,5)) + 
               Power(s2,2)*(-172 + 604*t2 + 80*Power(t2,2) - 
                  74*Power(t2,3) - 35*Power(t2,4) + 
                  t1*(209 + 564*t2 - 43*Power(t2,2) - 10*Power(t2,3))) + 
               s2*(173 - 473*t2 - 3*Power(t2,2) + 315*Power(t2,3) - 
                  177*Power(t2,4) - 8*Power(t2,5) + 
                  Power(t1,2)*
                   (-632 - 282*t2 + 39*Power(t2,2) + 14*Power(t2,3)) + 
                  t1*(227 - 1415*t2 + 173*Power(t2,2) + 
                     105*Power(t2,3) + 6*Power(t2,4)))) - 
            Power(s1,5)*(-230 + 229*t2 + 210*Power(t2,2) + 
               41*Power(t2,3) - 313*Power(t2,4) + 41*Power(t2,5) + 
               22*Power(t2,6) + 
               Power(t1,3)*(159 + 360*t2 + 30*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,2)*(54 + 883*t2 + 57*Power(t2,2) + 
                  278*Power(t2,3) - 17*Power(t2,4)) + 
               Power(s2,3)*(-8 - 38*t2 - 249*Power(t2,2) - 
                  74*Power(t2,3) + 37*Power(t2,4)) + 
               t1*(-95 + 209*t2 + 676*Power(t2,2) - 109*Power(t2,3) - 
                  135*Power(t2,4) + 15*Power(t2,5)) + 
               Power(s2,2)*(8 + 126*t2 + 957*Power(t2,2) + 
                  53*Power(t2,3) - 113*Power(t2,4) - 6*Power(t2,5) + 
                  t1*(50 + 472*t2 + 637*Power(t2,2) + 46*Power(t2,3) - 
                     3*Power(t2,4))) - 
               s2*(-213 + 444*t2 + 808*Power(t2,2) - 511*Power(t2,3) - 
                  118*Power(t2,4) + 147*Power(t2,5) + 4*Power(t2,6) + 
                  Power(t1,2)*
                   (234 + 737*t2 + 330*Power(t2,2) + 100*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  t1*(-102 + 973*t2 + 1558*Power(t2,2) + 
                     94*Power(t2,3) - 229*Power(t2,4) - 14*Power(t2,5)))) \
+ Power(s1,4)*(-287 + 32*t2 + 754*Power(t2,2) - 197*Power(t2,3) - 
               161*Power(t2,4) - 205*Power(t2,5) + 54*Power(t2,6) + 
               10*Power(t2,7) + 
               Power(t1,3)*(-24 + 178*t2 + 129*Power(t2,2) + 
                  72*Power(t2,3)) + 
               Power(t1,2)*(163 + 139*t2 + 770*Power(t2,2) + 
                  301*Power(t2,3) + 82*Power(t2,4)) + 
               2*Power(s2,3)*
                (15 - 53*t2 + 62*Power(t2,2) - 119*Power(t2,3) - 
                  21*Power(t2,4) + 6*Power(t2,5)) + 
               t1*(151 + 126*t2 + 757*Power(t2,2) + 62*Power(t2,3) + 
                  211*Power(t2,4) - 156*Power(t2,5) + 4*Power(t2,6)) - 
               s2*(-199 + 879*t2 + 793*Power(t2,2) - 378*Power(t2,3) - 
                  65*Power(t2,4) + 59*Power(t2,5) + 66*Power(t2,6) + 
                  Power(t1,2)*
                   (-24 + 204*t2 + 423*Power(t2,2) + 286*Power(t2,3) + 
                     56*Power(t2,4)) + 
                  t1*(236 + 103*t2 + 1251*Power(t2,2) + 
                     955*Power(t2,3) + 189*Power(t2,4) - 134*Power(t2,5)\
)) + Power(s2,2)*(-119 + 906*t2 - 537*Power(t2,2) + 777*Power(t2,3) + 
                  167*Power(t2,4) - 53*Power(t2,5) + 4*Power(t2,6) + 
                  t1*(-8 - 12*t2 + 426*Power(t2,2) + 340*Power(t2,3) + 
                     70*Power(t2,4) - 6*Power(t2,5)))) + 
            Power(s1,3)*(-77 + 853*t2 - 1398*Power(t2,2) + 
               27*Power(t2,3) + 552*Power(t2,4) + 35*Power(t2,5) + 
               30*Power(t2,6) - 22*Power(t2,7) - 
               Power(t1,3)*(19 - 56*t2 + 52*Power(t2,2) + 
                  40*Power(t2,3)) - 
               Power(t1,2)*(-81 + 185*t2 + 35*Power(t2,2) + 
                  168*Power(t2,3) + 134*Power(t2,4)) + 
               2*Power(s2,3)*
                (-1 - 77*t2 + 89*Power(t2,2) - 110*Power(t2,3) + 
                  81*Power(t2,4) + 3*Power(t2,5)) + 
               t1*(-303 - 504*t2 - 115*Power(t2,2) - 329*Power(t2,3) - 
                  204*Power(t2,4) - 34*Power(t2,5) + 20*Power(t2,6)) + 
               Power(s2,2)*(-143 + 670*t2 - 1571*Power(t2,2) + 
                  1611*Power(t2,3) - 700*Power(t2,4) - 
                  102*Power(t2,5) + 8*Power(t2,6) - 
                  2*t1*(-37 - 35*t2 - 28*Power(t2,2) + 70*Power(t2,3) + 
                     34*Power(t2,4) + 6*Power(t2,5))) + 
               s2*(617 - 737*t2 + 2619*Power(t2,2) - 1163*Power(t2,3) - 
                  261*Power(t2,4) + 318*Power(t2,5) + 62*Power(t2,6) + 
                  14*Power(t2,7) + 
                  Power(t1,2)*
                   (-8 - 55*t2 + 12*Power(t2,2) + 120*Power(t2,3) + 
                     36*Power(t2,4)) + 
                  t1*(-141 + 227*t2 - 158*Power(t2,2) + 
                     334*Power(t2,3) + 390*Power(t2,4) + 
                     36*Power(t2,5) - 20*Power(t2,6)))) - 
            s1*(88 - 409*t2 + 1344*Power(t2,2) - 1527*Power(t2,3) + 
               663*Power(t2,4) - 249*Power(t2,5) + 88*Power(t2,6) + 
               2*Power(t2,7) + 
               Power(s2,3)*Power(t2,2)*
                (95 + 110*t2 - 175*Power(t2,2) + 26*Power(t2,3)) + 
               Power(t1,3)*(-23 - 56*t2 + 78*Power(t2,2) + 
                  36*Power(t2,3)) + 
               Power(t1,2)*(184 - 39*t2 - 509*Power(t2,2) - 
                  290*Power(t2,3) + 75*Power(t2,4)) + 
               t1*(-249 + 257*t2 + 716*Power(t2,2) - 127*Power(t2,3) - 
                  41*Power(t2,4) - 129*Power(t2,5) + 36*Power(t2,6)) + 
               Power(s2,2)*t2*
                (20 + 31*t2 - 1045*Power(t2,2) + 649*Power(t2,3) - 
                  196*Power(t2,4) - 16*Power(t2,5) + 
                  t1*(-56 - 231*t2 + 102*Power(t2,2) + 95*Power(t2,3) + 
                     20*Power(t2,4))) + 
               s2*(-25 + 50*t2 - 2230*Power(t2,2) + 2935*Power(t2,3) - 
                  1406*Power(t2,4) + 245*Power(t2,5) - 38*Power(t2,6) + 
                  6*Power(t2,7) + 
                  Power(t1,2)*
                   (-34 + 97*t2 + 150*Power(t2,2) - 164*Power(t2,3) - 
                     70*Power(t2,4)) + 
                  t1*(60 + 97*t2 + 362*Power(t2,2) + 422*Power(t2,3) + 
                     177*Power(t2,4) + 22*Power(t2,5) - 4*Power(t2,6)))) \
+ Power(s1,2)*(-102 + 638*t2 - 1153*Power(t2,2) + 1424*Power(t2,3) - 
               779*Power(t2,4) - 68*Power(t2,5) + 26*Power(t2,6) + 
               14*Power(t2,7) - 
               3*Power(t1,3)*(14 - 18*t2 + Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,3)*t2*
                (45 + 258*t2 - 235*Power(t2,2) + 86*Power(t2,3) - 
                  36*Power(t2,4)) - 
               Power(t1,2)*(-98 + 243*t2 + 270*Power(t2,2) + 
                  155*Power(t2,3) + 31*Power(t2,4)) + 
               t1*(-62 + 793*t2 + 200*Power(t2,2) + 182*Power(t2,3) - 
                  153*Power(t2,4) + 159*Power(t2,5) - 4*Power(t2,6)) + 
               Power(s2,2)*(10 + 256*t2 - 1434*Power(t2,2) + 
                  1204*Power(t2,3) - 945*Power(t2,4) + 258*Power(t2,5) + 
                  4*Power(t2,6) + 
                  t1*(-25 - 232*t2 - 19*Power(t2,2) + 46*Power(t2,3) + 
                     2*Power(t2,4) + 8*Power(t2,5))) + 
               s2*(-33 - 2009*t2 + 2257*Power(t2,2) - 2247*Power(t2,3) + 
                  1313*Power(t2,4) - 276*Power(t2,5) - 102*Power(t2,6) - 
                  18*Power(t2,7) + 
                  Power(t1,2)*
                   (-16 + 134*t2 - 39*Power(t2,2) + 18*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  t1*(155 + 347*t2 + 267*Power(t2,2) + 387*Power(t2,3) + 
                     132*Power(t2,4) - 52*Power(t2,5) + 12*Power(t2,6)))))\
)*T2q(s1,s))/(Power(s,2)*(-1 + s1)*
       Power(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2),2)*
       (-1 + s2)*(-s + s2 - t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) - 
    (8*(4*Power(s,8)*(1 + s1)*(s1 - t2)*Power(t2,3) - 
         (-1 + s1)*Power(s1 - t2,4)*(-1 + t2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) - 
         2*Power(s,7)*Power(t2,2)*
          (-(Power(s1,3)*(6 + t1 - 7*t2)) + 
            2*s1*t2*(7 + s2 - 3*t1 - 10*t2 - s2*t2 + 2*Power(t2,2)) - 
            Power(s1,2)*(6 + (-18 - 2*s2 + t1)*t2 + 13*Power(t2,2)) + 
            t2*(2 - t2 - 2*s2*t2 + 6*Power(t2,2) + Power(t2,3) + 
               t1*(-1 + 2*t2 + Power(t2,2)))) - 
         2*Power(s,6)*t2*(-(Power(s1,4)*
               (6 + t1*(2 - 4*t2) + (-16 + s2)*t2 + 9*Power(t2,2))) + 
            Power(s1,2)*t2*(24 - 51*t2 + 68*Power(t2,2) - 
               22*Power(t2,3) - 3*Power(t1,2)*(1 + t2) + 
               t1*(-18 + 25*t2 + Power(t2,2)) + 
               s2*(6 - 22*t2 + 3*Power(t2,2))) + 
            s1*t2*(6 - (5 + 19*s2)*t2 + Power(t1,2)*(-6 + t2)*t2 + 
               (23 + 19*s2)*Power(t2,2) + (-36 + s2)*Power(t2,3) + 
               3*Power(t2,4) - 
               3*t1*(1 - (14 + s2)*t2 + 6*Power(t2,2) + 3*Power(t2,3))) \
+ Power(t2,2)*(-11 - 2*Power(t1,2) + (-32 + 13*s2)*t2 + 
               (4 - 3*s2)*Power(t2,2) - 2*(-3 + s2)*Power(t2,3) + 
               Power(t2,4) + 
               t1*(10 - 3*(-1 + s2)*t2 - (-2 + s2)*Power(t2,2) + 
                  5*Power(t2,3))) + 
            Power(s1,3)*(-6 + Power(t1,2) + (29 + 6*s2)*t2 - 
               (53 + s2)*Power(t2,2) + 27*Power(t2,3) + 
               t1*t2*(s2 - 2*(2 + t2)))) + 
         Power(s,4)*(2*Power(s1,6)*(-1 + t2)*
             (2 + t1*(2 - 4*t2) - 3*t2 + Power(t2,2) + s2*(-1 + 3*t2)) + 
            Power(s1,2)*t2*(42 - 6*(-57 + 34*s2)*t2 - 
               2*(555 - 196*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (644 - 333*s2 + 139*Power(s2,2))*Power(t2,3) + 
               (248 + 54*s2 - 23*Power(s2,2))*Power(t2,4) + 
               (-238 + 23*s2)*Power(t2,5) + 10*Power(t2,6) + 
               Power(t1,3)*(6 - 9*t2 - 20*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(3 + 21*(-11 + s2)*t2 + 
                  (235 + s2)*Power(t2,2) - (7 + 6*s2)*Power(t2,3) + 
                  12*Power(t2,4)) + 
               t1*(-48 + 9*(17 + 13*s2)*t2 + 
                  (-164 - 155*s2 + 7*Power(s2,2))*Power(t2,2) - 
                  (-99 + 106*s2 + Power(s2,2))*Power(t2,3) + 
                  (4 + 8*s2)*Power(t2,4) - 10*Power(t2,5))) - 
            Power(t2,3)*(-44 + 4*(-71 + 26*s2)*t2 + 
               2*(235 - 38*s2 + Power(s2,2))*Power(t2,2) - 
               (103 + 10*s2 + 20*Power(s2,2))*Power(t2,3) + 
               (-49 + 12*s2)*Power(t2,4) + (13 - 2*s2)*Power(t2,5) + 
               Power(t1,3)*(2 - 6*t2 + Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-27 + (36 + 5*s2)*t2 + 14*Power(t2,2) - 
                  (21 + 2*s2)*Power(t2,3)) + 
               t1*(68 + (167 - 87*s2)*t2 + 
                  (-253 + 36*s2 + Power(s2,2))*Power(t2,2) + 
                  (23 + 52*s2 + Power(s2,2))*Power(t2,3) + 
                  (11 - 6*s2)*Power(t2,4) + 3*Power(t2,5))) + 
            Power(s1,3)*(-4 + (-48 + 58*s2)*t2 + 
               (418 - 226*s2 + 4*Power(s2,2))*Power(t2,2) + 
               (-386 + 307*s2 - 109*Power(s2,2))*Power(t2,3) + 
               (-262 - 42*s2 + 27*Power(s2,2))*Power(t2,4) + 
               (336 - 47*s2)*Power(t2,5) - 32*Power(t2,6) - 
               Power(t1,3)*(2 - 27*t2 + 10*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(t1,2)*(3 + (53 - 17*s2)*t2 + 
                  (-89 + 9*s2)*Power(t2,2) + (49 + 4*s2)*Power(t2,3) - 
                  34*Power(t2,4)) + 
               t1*(2 - (113 + 19*s2)*t2 + 
                  (318 + 39*s2 - 7*Power(s2,2))*Power(t2,2) - 
                  3*(87 - 32*s2 + Power(s2,2))*Power(t2,3) + 
                  2*(7 + s2)*Power(t2,4) + 8*Power(t2,5))) + 
            s1*Power(t2,2)*(-84 + (-580 + 254*s2)*t2 + 
               2*(616 - 145*s2 + 4*Power(s2,2))*Power(t2,2) + 
               (-438 + 107*s2 - 85*Power(s2,2))*Power(t2,3) + 
               (-165 + s2 + 7*Power(s2,2))*Power(t2,4) + 
               (83 - 8*s2)*Power(t2,5) + 
               Power(t1,3)*t2*(-11 + 20*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-39 - 3*(-65 + s2)*t2 - 
                  (106 + 5*s2)*Power(t2,2) - 52*Power(t2,3) + 
                  2*Power(t2,4)) + 
               t1*(120 + (137 - 185*s2)*t2 - 
                  (336 - 139*s2 + Power(s2,2))*Power(t2,2) + 
                  (53 + 114*s2 + 3*Power(s2,2))*Power(t2,3) + 
                  (11 - 14*s2)*Power(t2,4) + 13*Power(t2,5))) + 
            Power(s1,5)*(2 + Power(t1,2)*(7 - 13*t2) + 24*t2 + 
               2*Power(s2,2)*(-3 + t2)*t2 - 81*Power(t2,2) + 
               73*Power(t2,3) - 16*Power(t2,4) + 
               t1*(12 - 24*t2 - 21*Power(t2,2) + 31*Power(t2,3)) + 
               s2*(-4 - 2*t2 + 37*Power(t2,2) - 29*Power(t2,3) + 
                  6*t1*(-1 + 3*t2))) + 
            Power(s1,4)*(-4 - 66*t2 + 59*Power(t2,2) + 201*Power(t2,3) - 
               233*Power(t2,4) + 36*Power(t2,5) + 
               Power(t1,3)*(-7 + 5*t2) + 
               Power(t1,2)*(1 - 15*t2 + 8*Power(t2,2) + 
                  20*Power(t2,3)) + 
               t1*(8 - 101*t2 + 154*Power(t2,2) - 9*Power(t2,3) - 
                  31*Power(t2,4)) + 
               Power(s2,2)*t2*((41 - 13*t2)*t2 + 2*t1*(1 + t2)) - 
               s2*(4 - 52*t2 + 91*Power(t2,2) + 30*Power(t2,3) - 
                  53*Power(t2,4) + Power(t1,2)*(-4 + 5*t2) + 
                  t1*t2*(-19 + 70*t2 + 2*Power(t2,2))))) + 
         Power(s,5)*(2*Power(s1,5)*
             (2 + (-11 + 2*s2)*t2 + (14 - 3*s2)*Power(t2,2) - 
               5*Power(t2,3) + t1*(1 - 6*t2 + 6*Power(t2,2))) + 
            Power(s1,4)*(4 - 12*(2 + s2)*t2 - 
               2*(-51 + s2 + Power(s2,2))*Power(t2,2) + 
               6*(-22 + 3*s2)*Power(t2,3) + 47*Power(t2,4) + 
               Power(t1,2)*(-2 + 9*t2) - 
               t1*t2*(-18 - 4*s2*(-2 + t2) + t2 + 25*Power(t2,2))) - 
            Power(t2,3)*(46 + Power(t1,3)*(-1 + t2) + 
               (204 - 72*s2)*t2 - 
               2*(104 - 19*s2 + Power(s2,2))*Power(t2,2) + 
               2*(-4 + 11*s2 + Power(s2,2))*Power(t2,3) - 
               3*(-7 + s2)*Power(t2,4) + Power(t2,5) + 
               Power(t1,2)*(20 - (8 + s2)*t2 - 5*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(-58 + (-74 + 38*s2)*t2 + (45 + s2)*Power(t2,2) - 
                  (13 + 11*s2)*Power(t2,3) + 8*Power(t2,4))) + 
            s1*Power(t2,2)*(54 - 6*(-38 + 23*s2)*t2 - 
               2*(189 - 78*s2 + 2*Power(s2,2))*Power(t2,2) + 
               2*(-18 + 5*s2 + 5*Power(s2,2))*Power(t2,3) - 
               7*(-15 + s2)*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,3)*(3 + 7*t2) + 
               Power(t1,2)*(12 + (-82 + s2)*t2 + 
                  2*(12 + s2)*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(-54 + 56*(2 + s2)*t2 - (89 + 19*s2)*Power(t2,2) - 
                  (28 + 19*s2)*Power(t2,3) + 29*Power(t2,4))) + 
            Power(s1,2)*t2*(-12 + 6*(-3 + 13*s2)*t2 + 
               2*(80 - 96*s2 + Power(s2,2))*Power(t2,2) + 
               (106 + 52*s2 - 16*Power(s2,2))*Power(t2,3) + 
               (-233 + 11*s2)*Power(t2,4) + 34*Power(t2,5) + 
               Power(t1,3)*(-3 + 3*t2 + 2*Power(t2,2)) + 
               t1*(6 - 6*(29 + 3*s2)*t2 + (277 + 13*s2)*Power(t2,2) + 
                  (-22 + 9*s2)*Power(t2,3) - 25*Power(t2,4)) + 
               Power(t1,2)*t2*
                (30 - 42*t2 + 17*Power(t2,2) - s2*(3 + 2*t2))) + 
            Power(s1,3)*(-(Power(t1,3)*(-1 + t2)) + 
               Power(t1,2)*t2*(12 + s2 - 12*t2 - 19*Power(t2,2)) + 
               t1*t2*(32 + (-137 + 15*s2)*t2 + 
                  (54 - 5*s2)*Power(t2,2) + 17*Power(t2,3)) - 
               t2*(28 - 18*t2 + 160*Power(t2,2) - 
                  10*Power(s2,2)*Power(t2,2) - 253*Power(t2,3) + 
                  68*Power(t2,4) + 
                  s2*(12 - 86*t2 + 42*Power(t2,2) + 19*Power(t2,3))))) - 
         Power(s,2)*Power(s1 - t2,2)*
          (Power(s1,5)*(Power(t1,2)*(1 - 3*t2) - 2*Power(-1 + t2,3) - 
               t1*(-1 + t2)*Power(1 + t2,2) + 
               Power(s2,2)*(-1 - 3*t2 + 2*Power(t2,2)) + 
               s2*(1 + (-3 + 6*t1)*t2 - (1 + 2*t1)*Power(t2,2) + 
                  3*Power(t2,3))) + 
            t2*(3 + 6*(-13 + 4*s2)*t2 + 
               (151 - 18*s2 - 3*Power(s2,2))*Power(t2,2) + 
               (-79 + 4*s2 + 23*Power(s2,2) - 6*Power(s2,3))*
                Power(t2,3) - 
               (-2 + 25*s2 - 19*Power(s2,2) + Power(s2,3))*
                Power(t2,4) + 
               (-1 + 17*s2 - 6*Power(s2,2))*Power(t2,5) - 
               2*(-2 + s2)*Power(t2,6) - 2*Power(t2,7) - 
               Power(t1,3)*(2 - 5*t2 + Power(t2,3)) + 
               Power(t1,2)*(7 + (-33 + 7*s2)*t2 + 
                  (29 - 15*s2)*Power(t2,2) - 4*(-7 + s2)*Power(t2,3) + 
                  (4 + s2)*Power(t2,4) - 2*Power(t2,5)) + 
               t1*(-8 + (106 - 31*s2)*t2 + 
                  (-206 + 51*s2 + 3*Power(s2,2))*Power(t2,2) + 
                  (84 - 40*s2 + 7*Power(s2,2))*Power(t2,3) + 
                  (43 - 55*s2 + 6*Power(s2,2))*Power(t2,4) + 
                  (-23 + 9*s2)*Power(t2,5) + 4*Power(t2,6))) - 
            s1*(-3 + (-118 + 41*s2)*t2 + 
               (304 - 17*s2 - 4*Power(s2,2))*Power(t2,2) + 
               (-253 + 66*s2 + 55*Power(s2,2) - 20*Power(s2,3))*
                Power(t2,3) + 
               (87 - 193*s2 + 82*Power(s2,2) - 6*Power(s2,3))*
                Power(t2,4) + 
               (-29 + 113*s2 - 28*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,5) + 
               (18 - 10*s2 + 3*Power(s2,2))*Power(t2,6) - 
               6*Power(t2,7) + 
               Power(t1,3)*(4 - 7*t2 + 6*Power(t2,2) + 
                  11*Power(t2,3)) + 
               Power(t1,2)*(-11 + (6 + 7*s2)*t2 + 
                  (24 - 13*s2)*Power(t2,2) + 
                  (37 - 40*s2)*Power(t2,3) + (58 - 9*s2)*Power(t2,4) + 
                  3*(-2 + s2)*Power(t2,5)) + 
               t1*(10 + (119 - 48*s2)*t2 + 
                  4*(-103 + 21*s2 + Power(s2,2))*Power(t2,2) + 
                  (320 - 157*s2 + 43*Power(s2,2))*Power(t2,3) + 
                  (22 - 97*s2 + 16*Power(s2,2))*Power(t2,4) - 
                  (73 - 3*s2 + Power(s2,2))*Power(t2,5) - 
                  (-14 + s2)*Power(t2,6))) + 
            Power(s1,4)*(-2*Power(t1,3)*(-9 + 5*t2) + 
               Power(s2,3)*(-2 - 3*t2 + 2*Power(t2,2)) + 
               Power(-1 + t2,2)*(-13 - t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(25 - 30*t2 + 18*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-11 + 23*t2 - 21*Power(t2,2) + 8*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(-5 + 23*t2 - 3*Power(t2,3) + 
                  t1*(13 + 3*t2 - 2*Power(t2,2))) + 
               s2*(23 - 57*t2 + 45*Power(t2,2) - 2*Power(t2,3) - 
                  9*Power(t2,4) + Power(t1,2)*(-29 + 10*t2) + 
                  t1*(-5 - 18*t2 - 13*Power(t2,2) + 6*Power(t2,3)))) + 
            Power(s1,3)*(2*Power(s2,3)*t2*(6 + 5*t2 - 3*Power(t2,2)) + 
               Power(t1,3)*(-2 - 33*t2 + 4*Power(t2,2) + 
                  5*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (18 - 59*t2 + 4*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(10 - 89*t2 + 38*Power(t2,2) - 
                  27*Power(t2,3) + 2*Power(t2,4)) + 
               t1*(36 - 62*t2 + Power(t2,2) + 55*Power(t2,3) - 
                  31*Power(t2,4) + Power(t2,5)) + 
               Power(s2,2)*(-2 + t2 - 87*Power(t2,2) + 
                  25*Power(t2,3) - 3*Power(t2,4) + 
                  t1*(2 - 55*t2 - 10*Power(t2,2) + 5*Power(t2,3))) + 
               s2*(3 - 124*t2 + 256*Power(t2,2) - 157*Power(t2,3) + 
                  13*Power(t2,4) + 9*Power(t2,5) + 
                  Power(t1,2)*(5 + 70*t2 - 7*Power(t2,2)) + 
                  t1*(-26 + 127*t2 + 19*Power(t2,2) + 17*Power(t2,3) - 
                     5*Power(t2,4)))) + 
            Power(s1,2)*(6*Power(s2,3)*Power(t2,2)*
                (-4 - 2*t2 + Power(t2,2)) + 
               Power(t1,3)*(-12 + 38*t2 - 3*Power(t2,2) + 
                  6*Power(t2,3) + Power(t2,4)) - 
               Power(-1 + t2,2)*
                (40 - 61*t2 + 64*Power(t2,2) - 9*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,2)*(39 - 105*t2 + 163*Power(t2,2) + 
                  45*Power(t2,3) - 10*Power(t2,4) - 4*Power(t2,5)) - 
               t1*(-13 + 152*t2 - 219*Power(t2,2) + 46*Power(t2,3) + 
                  67*Power(t2,4) - 34*Power(t2,5) + Power(t2,6)) + 
               Power(s2,2)*t2*
                (1 + 36*t2 + 128*Power(t2,2) - 44*Power(t2,3) + 
                  7*Power(t2,4) + 
                  t1*(-1 + 78*t2 + 17*Power(t2,2) - 4*Power(t2,3))) + 
               s2*(17 - 2*t2 + 163*Power(t2,2) - 368*Power(t2,3) + 
                  211*Power(t2,4) - 18*Power(t2,5) - 3*Power(t2,6) + 
                  Power(t1,2)*t2*
                   (-3 - 77*t2 - 13*Power(t2,2) + 3*Power(t2,3)) - 
                  t1*(17 - 59*t2 + 239*Power(t2,2) + 43*Power(t2,3) + 
                     16*Power(t2,4))))) - 
         Power(s,3)*(s1 - t2)*
          (2*Power(s1,6)*(s2 - t1)*Power(-1 + t2,2) + 
            Power(t2,2)*(-16 + (-215 + 82*s2)*t2 + 
               (410 - 40*s2 - 8*Power(s2,2))*Power(t2,2) - 
               (177 - 47*s2 + 28*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) - 
               (20 + 8*s2 - 12*Power(s2,2) + Power(s2,3))*Power(t2,4) - 
               (-18 + Power(s2,2))*Power(t2,5) + 
               Power(t1,3)*t2*(-5 + 5*t2 + 3*Power(t2,2)) - 
               Power(t1,2)*(7 - (14 + 9*s2)*t2 + 
                  (3 + 6*s2)*Power(t2,2) + 5*(5 + 2*s2)*Power(t2,3) + 
                  (-6 + s2)*Power(t2,4)) + 
               t1*(23 + (174 - 82*s2)*t2 + 
                  (-393 + 92*s2 + 3*Power(s2,2))*Power(t2,2) + 
                  (78 + 59*s2 + 3*Power(s2,2))*Power(t2,3) + 
                  2*(25 - 16*s2 + Power(s2,2))*Power(t2,4) + 
                  (-13 + 3*s2)*Power(t2,5))) - 
            Power(s1,2)*(10 + (197 - 98*s2)*t2 + 
               2*(-344 + 67*s2 + 3*Power(s2,2))*Power(t2,2) + 
               (772 - 534*s2 + 175*Power(s2,2) + 9*Power(s2,3))*
                Power(t2,3) + 
               (-218 + 385*s2 - 120*Power(s2,2) + 9*Power(s2,3))*
                Power(t2,4) + 
               (-131 - 82*s2 + 13*Power(s2,2))*Power(t2,5) + 
               (59 - 17*s2)*Power(t2,6) - Power(t2,7) + 
               Power(t1,3)*t2*(11 - 8*t2 + 21*Power(t2,2)) + 
               Power(t1,2)*(1 + (-140 + 11*s2)*t2 + 
                  (371 - 51*s2)*Power(t2,2) + (-56 + s2)*Power(t2,3) + 
                  (-38 + 5*s2)*Power(t2,4) - 12*Power(t2,5)) + 
               t1*(-11 + 4*(7 + 15*s2)*t2 + 
                  (280 - 278*s2 + 24*Power(s2,2))*Power(t2,2) - 
                  (95 + 52*s2 + 24*Power(s2,2))*Power(t2,3) + 
                  (-68 + 57*s2 - 8*Power(s2,2))*Power(t2,4) + 
                  (82 + 13*s2)*Power(t2,5) - 4*Power(t2,6))) + 
            s1*t2*(32 + (400 - 166*s2)*t2 + 
               (-955 + 121*s2 + 15*Power(s2,2))*Power(t2,2) + 
               (632 - 302*s2 + 115*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,3) + 
               (-49 + 162*s2 - 64*Power(s2,2) + 5*Power(s2,3))*
                Power(t2,4) + 
               (-69 - 32*s2 + 7*Power(s2,2))*Power(t2,5) - 
               3*(-3 + s2)*Power(t2,6) + 
               Power(t1,3)*(-6 + 13*t2 - 13*Power(t2,2) + 
                  7*Power(t2,3) + 3*Power(t2,4)) + 
               Power(t1,2)*(26 - 2*(56 + s2)*t2 - 
                  10*(-16 + s2)*Power(t2,2) + 
                  (53 + 8*s2)*Power(t2,3) + (-43 + 4*s2)*Power(t2,4) - 
                  3*Power(t2,5)) + 
               t1*(-52 + (-205 + 141*s2)*t2 + 
                  (755 - 268*s2 + 5*Power(s2,2))*Power(t2,2) - 
                  (241 + 89*s2 + 16*Power(s2,2))*Power(t2,3) + 
                  (-90 + 63*s2 - 5*Power(s2,2))*Power(t2,4) - 
                  (-51 + s2)*Power(t2,5) + 2*Power(t2,6))) - 
            Power(s1,5)*(2 - 17*t2 + 31*Power(t2,2) - 17*Power(t2,3) + 
               Power(t2,4) + Power(t1,2)*(-6 + 5*t2) + 
               Power(s2,2)*(-4 + 5*t2 + 2*Power(t2,2)) + 
               t1*(-11 + 9*t2 + 9*Power(t2,2) - 11*Power(t2,3)) + 
               2*s2*(1 + 4*t2 - 10*Power(t2,2) + 7*Power(t2,3) + 
                  t1*(5 - 4*t2 - 2*Power(t2,2)))) + 
            Power(s1,4)*(22 - 79*t2 + 20*Power(t2,2) + 
               109*Power(t2,3) - 75*Power(t2,4) + 3*Power(t2,5) - 
               Power(s2,3)*t2*(1 + 2*t2) + Power(t1,3)*(-17 + 10*t2) + 
               Power(t1,2)*(-18 + 15*t2 - 14*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(9 - 79*t2 + 68*Power(t2,2) - 13*Power(t2,3) - 
                  8*Power(t2,4)) + 
               Power(s2,2)*(t2*(-37 + 38*t2 + 2*Power(t2,2)) + 
                  t1*(-5 + t2 + 4*Power(t2,2))) + 
               s2*(-6 + Power(t1,2)*(19 - 9*t2) + 81*t2 - 
                  75*Power(t2,2) - 10*Power(t2,3) + 33*Power(t2,4) + 
                  t1*(6 + 59*t2 - 43*Power(t2,2) - 14*Power(t2,3)))) + 
            Power(s1,3)*(22 - 197*t2 + 408*Power(t2,2) - 
               180*Power(t2,3) - 158*Power(t2,4) + 108*Power(t2,5) - 
               3*Power(t2,6) + Power(s2,3)*Power(t2,2)*(5 + 7*t2) - 
               Power(t1,3)*(7 - 49*t2 + 9*Power(t2,2) + 9*Power(t2,3)) + 
               Power(t1,2)*(-12 + 136*t2 - 75*Power(t2,2) + 
                  36*Power(t2,3) - 15*Power(t2,4)) + 
               t1*(29 + 5*t2 + 106*Power(t2,2) - 103*Power(t2,3) + 
                  62*Power(t2,4) - 7*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-1 + 121*t2 - 101*Power(t2,2) + 7*Power(t2,3) - 
                  3*t1*(-7 + 4*t2 + 3*Power(t2,2))) + 
               s2*(-14 + 59*t2 - 358*Power(t2,2) + 312*Power(t2,3) - 
                  56*Power(t2,4) - 35*Power(t2,5) + 
                  2*Power(t1,2)*
                   (2 - 27*t2 + 6*Power(t2,2) + Power(t2,3)) + 
                  t1*(1 - 108*t2 - 71*Power(t2,2) + 61*Power(t2,3) + 
                     21*Power(t2,4))))) - 
         s*Power(s1 - t2,3)*(1 - 3*t1 + 3*Power(t1,2) - Power(t1,3) + 
            2*Power(s1,5)*Power(s2 - t1,2)*(-1 + t2) - 10*t2 + 2*s2*t2 + 
            26*t1*t2 - 4*s2*t1*t2 - 22*Power(t1,2)*t2 + 
            2*s2*Power(t1,2)*t2 + 6*Power(t1,3)*t2 + 20*Power(t2,2) - 
            6*s2*Power(t2,2) - Power(s2,2)*Power(t2,2) - 
            44*t1*Power(t2,2) + 18*s2*t1*Power(t2,2) + 
            Power(s2,2)*t1*Power(t2,2) + 29*Power(t1,2)*Power(t2,2) - 
            12*s2*Power(t1,2)*Power(t2,2) - 5*Power(t1,3)*Power(t2,2) - 
            12*Power(t2,3) + s2*Power(t2,3) + 
            10*Power(s2,2)*Power(t2,3) - 2*Power(s2,3)*Power(t2,3) + 
            15*t1*Power(t2,3) - 26*s2*t1*Power(t2,3) + 
            2*Power(s2,2)*t1*Power(t2,3) + 2*Power(t1,2)*Power(t2,3) + 
            13*s2*Power(t1,2)*Power(t2,3) - 3*Power(t1,3)*Power(t2,3) + 
            Power(t2,4) + 4*s2*Power(t2,4) - 9*Power(s2,2)*Power(t2,4) + 
            4*Power(s2,3)*Power(t2,4) + 13*t1*Power(t2,4) + 
            2*s2*t1*Power(t2,4) - 9*Power(s2,2)*t1*Power(t2,4) - 
            12*Power(t1,2)*Power(t2,4) + 6*s2*Power(t1,2)*Power(t2,4) - 
            2*Power(t2,5) + 3*s2*Power(t2,5) - 
            2*Power(s2,2)*Power(t2,5) + Power(s2,3)*Power(t2,5) - 
            7*t1*Power(t2,5) + 10*s2*t1*Power(t2,5) - 
            3*Power(s2,2)*t1*Power(t2,5) + 2*Power(t2,6) - 
            4*s2*Power(t2,6) + 2*Power(s2,2)*Power(t2,6) + 
            Power(s1,4)*(Power(s2,3)*(1 + 2*t2) + 
               Power(s2,2)*(1 + t1*(-10 + t2) + t2 - 2*Power(t2,2)) + 
               t1*(Power(-1 + t2,2)*(1 + t2) + 
                  Power(t1,2)*(-8 + 5*t2) - 
                  t1*(6 - 14*t2 + 7*Power(t2,2) + Power(t2,3))) + 
               s2*(Power(t1,2)*(17 - 8*t2) - Power(-1 + t2,2)*(1 + t2) + 
                  t1*(5 - 15*t2 + 9*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,3)*(3*t1*(-3 + t2)*Power(-1 + t2,2) - 
               Power(-1 + t2,3)*(3 + t2) + 
               Power(s2,3)*(2 - 7*t2 - 7*Power(t2,2)) - 
               Power(t1,3)*(-11 - 3*t2 + Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-7 + 21*t2 - 23*Power(t2,2) + 
                  8*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(-15 + 18*t2 + 6*Power(t2,2) - 
                  9*Power(t2,3) + 
                  t1*(3 + 27*t2 + 5*Power(t2,2) + Power(t2,3))) + 
               s2*(Power(-1 + t2,2)*(-3 + 7*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*(-16 - 23*t2 + 3*Power(t2,2)) + 
                  t1*(31 - 65*t2 + 41*Power(t2,2) - 5*Power(t2,3) - 
                     2*Power(t2,4)))) - 
            s1*(Power(-1 + t2,3)*(7 + 2*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*Power(t2,2)*(-6 + 13*t2 + 5*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (17 - 3*t2 + 4*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,3)*(-3 + t2 + 9*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(-13 + 19*t2 + 5*Power(t2,2) - 
                  11*Power(t2,4)) - 
               Power(s2,2)*t2*
                (2 - 35*t2 + 38*Power(t2,2) + 6*Power(t2,3) - 
                  11*Power(t2,4) + 
                  t1*(-2 - t2 + 25*Power(t2,2) + 13*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(-(Power(-1 + t2,2)*
                     (-2 - 5*t2 + 14*Power(t2,2) + 13*Power(t2,3))) + 
                  Power(t1,2)*
                   (2 - 11*t2 + 15*Power(t2,2) + 24*Power(t2,3) + 
                     6*Power(t2,4)) - 
                  t1*(4 - 10*t2 + 29*Power(t2,2) - 13*Power(t2,3) - 
                     11*Power(t2,4) + Power(t2,5)))) + 
            Power(s1,2)*(Power(-1 + t2,3)*(-2 + 4*t2 + Power(t2,2)) + 
               3*Power(s2,3)*t2*(-2 + 5*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-1 + 4*t2 + 6*Power(t2,2) - 9*Power(t2,3)) - 
               Power(t1,3)*(-1 + 15*t2 + 3*Power(t2,2) + Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (2 - 23*t2 + 5*Power(t2,2) + Power(t2,3)) - 
               Power(s2,2)*(1 - 40*t2 + 48*Power(t2,2) + 9*Power(t2,3) - 
                  18*Power(t2,4) + 
                  t1*(-1 + 4*t2 + 33*Power(t2,2) + 16*Power(t2,3) + 
                     2*Power(t2,4))) + 
               s2*(-(Power(-1 + t2,2)*
                     (-7 + 6*t2 + 15*Power(t2,2) + Power(t2,3))) + 
                  Power(t1,2)*
                   (1 + 18*t2 + 24*Power(t2,2) + 11*Power(t2,3)) + 
                  t1*(-8 - 34*t2 + 71*Power(t2,2) - 29*Power(t2,3) - 
                     Power(t2,4) + Power(t2,5))))))*T3q(t2,s1))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*(-s + s2 - t1)*Power(s1 - t2,2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) + 
    (8*(8*Power(s,8)*(-1 + t2)*Power(t2,3) + 
         (-1 + s1)*(s1 - t2)*Power(-1 + t2,4)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         2*Power(s,7)*Power(t2,2)*
          (-4*s1*(3 - 7*t2 + 4*Power(t2,2)) + 
            t1*(-1 - 8*t2 + 2*Power(t2,2) + Power(t2,3)) + 
            t2*(9 + 8*s2 - 14*t2 - 8*s2*t2 + 10*Power(t2,2) + 
               Power(t2,3))) + 
         2*Power(s,6)*t2*(12*Power(s1,2)*Power(-1 + t2,2)*(-1 + 2*t2) + 
            Power(t1,2)*(1 - 3*t2 - 11*Power(t2,2) + Power(t2,3)) + 
            t2*(-1 + (9 - 24*s2 - 4*Power(s2,2))*t2 + 
               (-16 + 31*s2 + 4*Power(s2,2))*Power(t2,2) - 
               (5 + 23*s2)*Power(t2,3) - 2*(-6 + s2)*Power(t2,4) + 
               Power(t2,5)) + 
            t1*t2*(2 + 27*t2 - 13*Power(t2,2) - 3*Power(t2,3) + 
               5*Power(t2,4) - 
               s2*(-1 - 13*t2 + Power(t2,2) + Power(t2,3))) + 
            s1*(-(t1*(2 + 23*t2 - 38*Power(t2,2) + 3*Power(t2,3) + 
                    4*Power(t2,4))) + 
               t2*(16 - 40*t2 + 56*Power(t2,2) - 29*Power(t2,3) - 
                  3*Power(t2,4) + 
                  s2*(23 - 52*t2 + 22*Power(t2,2) + Power(t2,3))))) + 
         Power(s,5)*(-8*Power(s1,3)*Power(-1 + t2,3)*(-1 + 4*t2) + 
            Power(t1,3)*(-1 + 4*t2 - 6*Power(t2,2) - 10*Power(t2,3) + 
               Power(t2,4)) + 
            t1*t2*(4 + (5 - 9*s2)*t2 - 
               2*(30 + 36*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (22 + 52*s2 - 2*Power(s2,2))*Power(t2,3) + 
               2*(17 + 8*s2)*Power(t2,4) - (13 + 11*s2)*Power(t2,5) + 
               8*Power(t2,6)) + 
            Power(t1,2)*t2*(-6 + 21*t2 + 52*Power(t2,2) - 
               50*Power(t2,3) - 8*Power(t2,4) + 3*Power(t2,5) + 
               s2*(-1 + 3*t2 + 21*Power(t2,2) + Power(t2,3))) + 
            Power(t2,2)*(2*Power(s2,2)*t2*
                (17 - 24*t2 + 12*Power(t2,2) + Power(t2,3)) + 
               Power(-1 + t2,2)*
                (6 - 54*t2 + 41*Power(t2,2) + 30*Power(t2,3) + 
                  Power(t2,4)) - 
               s2*(2 + 10*t2 - 33*Power(t2,2) - 39*Power(t2,3) + 
                  57*Power(t2,4) + 3*Power(t2,5))) + 
            2*Power(s1,2)*(-1 + t2)*
             (t1*(1 + 25*t2 - 53*Power(t2,2) + 6*Power(t2,4)) + 
               t2*(-5 + 3*t2 - 33*Power(t2,2) + 32*Power(t2,3) + 
                  3*Power(t2,4) - 
                  s2*(22 - 62*t2 + 16*Power(t2,2) + 3*Power(t2,3)))) + 
            s1*(Power(t1,2)*(2 - 15*t2 - 51*Power(t2,2) + 
                  91*Power(t2,3) - 3*Power(t2,4)) - 
               t2*(4 + (-78 + 94*s2 + 22*Power(s2,2))*t2 + 
                  (246 - 208*s2 - 50*Power(s2,2))*Power(t2,2) + 
                  (-205 + 220*s2 + 2*Power(s2,2))*Power(t2,3) + 
                  (-31 - 96*s2 + 2*Power(s2,2))*Power(t2,4) + 
                  (59 - 10*s2)*Power(t2,5) + 5*Power(t2,6)) + 
               t1*t2*(t2*(133 - 176*t2 + 20*Power(t2,2) + 
                     52*Power(t2,3) - 29*Power(t2,4)) + 
                  4*s2*(2 + 17*t2 - 29*Power(t2,2) - 3*Power(t2,3) + 
                     Power(t2,4))))) + 
         Power(s,4)*(8*Power(s1,4)*Power(-1 + t2,4) + 
            Power(t1,3)*(2 - 12*t2 + 25*Power(t2,2) - 2*Power(t2,3) - 
               24*Power(t2,4) - 2*Power(t2,5) + Power(t2,6)) - 
            Power(t1,2)*(3 - (7 + 5*s2)*t2 + (7 + 15*s2)*Power(t2,2) + 
               (38 + 23*s2)*Power(t2,3) - (63 + 59*s2)*Power(t2,4) + 
               (7 - 12*s2)*Power(t2,5) + (15 + 2*s2)*Power(t2,6)) + 
            t2*(2 + 2*(1 - s2 + Power(s2,2))*t2 - 
               (25 - 158*s2 + 20*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,2) + 
               2*(11 - 198*s2 + 30*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,3) + 
               (19 + 268*s2 - 74*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,4) + 
               (-11 + 28*s2 + 32*Power(s2,2))*Power(t2,5) - 
               6*(4 + 9*s2)*Power(t2,6) + (15 - 2*s2)*Power(t2,7)) - 
            2*Power(s1,3)*Power(-1 + t2,2)*
             (2 - 22*t2 + 6*Power(t2,2) + 13*Power(t2,3) + 
               Power(t2,4) + 
               s2*(-7 + 32*t2 + 5*Power(t2,2) - 3*Power(t2,3)) + 
               t1*(9 - 32*t2 - 8*Power(t2,2) + 4*Power(t2,3))) + 
            t1*t2*(Power(s2,2)*t2*
                (1 + 18*t2 - 42*Power(t2,2) - 14*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(-1 + t2,2)*
                (-11 - 34*t2 - 6*Power(t2,2) + 22*Power(t2,3) + 
                  20*Power(t2,4) + 3*Power(t2,5)) + 
               s2*(1 + 3*t2 + 19*Power(t2,2) - 23*Power(t2,3) - 
                  22*Power(t2,4) + 28*Power(t2,5) - 6*Power(t2,6))) + 
            Power(s1,2)*(-1 + t2)*
             (2 + 2*(-39 + 26*s2 + 9*Power(s2,2))*t2 + 
               (291 - 91*s2 - 58*Power(s2,2))*Power(t2,2) + 
               (-295 + 107*s2 - 38*Power(s2,2))*Power(t2,3) + 
               (23 - 59*s2 + 2*Power(s2,2))*Power(t2,4) + 
               (53 - 9*s2)*Power(t2,5) + 4*Power(t2,6) + 
               Power(t1,2)*(7 + 57*t2 - 135*Power(t2,2) - 
                  5*Power(t2,3)) + 
               t1*(4 - 98*t2 + 79*Power(t2,2) + 57*Power(t2,3) - 
                  69*Power(t2,4) + 27*Power(t2,5) + 
                  2*s2*(-3 - 33*t2 + 87*Power(t2,2) + 25*Power(t2,3)))) \
- s1*(Power(t1,3)*(-7 + 26*t2 + 6*Power(t2,2) - 42*Power(t2,3) + 
                  5*Power(t2,4)) + 
               t2*(-4 + (103 + 107*s2 - 67*Power(s2,2))*t2 + 
                  (-389 - 330*s2 + 172*Power(s2,2) + 2*Power(s2,3))*
                   Power(t2,2) + 
                  (529 + 212*s2 - 112*Power(s2,2) + 10*Power(s2,3))*
                   Power(t2,3) + 
                  3*(-75 + 44*s2 + 2*Power(s2,2))*Power(t2,4) + 
                  (-70 - 115*s2 + Power(s2,2))*Power(t2,5) - 
                  6*(-9 + s2)*Power(t2,6) + 2*Power(t2,7)) + 
               t1*(-4 + (-23 + 19*s2 + 2*Power(s2,2))*t2 + 
                  (91 + 125*s2 + 26*Power(s2,2))*Power(t2,2) + 
                  (33 - 307*s2 - 44*Power(s2,2))*Power(t2,3) + 
                  (-229 + 89*s2 - 18*Power(s2,2))*Power(t2,4) - 
                  2*(-75 - 44*s2 + Power(s2,2))*Power(t2,5) - 
                  2*(17 + 7*s2)*Power(t2,6) + 16*Power(t2,7)) + 
               Power(t1,2)*(1 - 15*t2 - 106*Power(t2,2) + 
                  250*Power(t2,3) - 89*Power(t2,4) - 49*Power(t2,5) + 
                  8*Power(t2,6) + 
                  s2*(4 - 17*t2 - 45*Power(t2,2) + 87*Power(t2,3) + 
                     7*Power(t2,4))))) + 
         s*Power(-1 + t2,3)*(1 - 3*t1 + 3*Power(t1,2) - Power(t1,3) + 
            2*Power(s1,5)*Power(s2 - t1,2)*(-1 + t2) - 10*t2 + 2*s2*t2 + 
            26*t1*t2 - 4*s2*t1*t2 - 22*Power(t1,2)*t2 + 
            2*s2*Power(t1,2)*t2 + 6*Power(t1,3)*t2 + 20*Power(t2,2) - 
            6*s2*Power(t2,2) - Power(s2,2)*Power(t2,2) - 
            44*t1*Power(t2,2) + 18*s2*t1*Power(t2,2) + 
            Power(s2,2)*t1*Power(t2,2) + 29*Power(t1,2)*Power(t2,2) - 
            12*s2*Power(t1,2)*Power(t2,2) - 5*Power(t1,3)*Power(t2,2) - 
            12*Power(t2,3) + s2*Power(t2,3) + 
            10*Power(s2,2)*Power(t2,3) - 2*Power(s2,3)*Power(t2,3) + 
            15*t1*Power(t2,3) - 26*s2*t1*Power(t2,3) + 
            2*Power(s2,2)*t1*Power(t2,3) + 2*Power(t1,2)*Power(t2,3) + 
            13*s2*Power(t1,2)*Power(t2,3) - 3*Power(t1,3)*Power(t2,3) + 
            Power(t2,4) + 4*s2*Power(t2,4) - 9*Power(s2,2)*Power(t2,4) + 
            4*Power(s2,3)*Power(t2,4) + 13*t1*Power(t2,4) + 
            2*s2*t1*Power(t2,4) - 9*Power(s2,2)*t1*Power(t2,4) - 
            12*Power(t1,2)*Power(t2,4) + 6*s2*Power(t1,2)*Power(t2,4) - 
            2*Power(t2,5) + 3*s2*Power(t2,5) - 
            2*Power(s2,2)*Power(t2,5) + Power(s2,3)*Power(t2,5) - 
            7*t1*Power(t2,5) + 10*s2*t1*Power(t2,5) - 
            3*Power(s2,2)*t1*Power(t2,5) + 2*Power(t2,6) - 
            4*s2*Power(t2,6) + 2*Power(s2,2)*Power(t2,6) + 
            Power(s1,4)*(Power(s2,3)*(1 + 2*t2) + 
               Power(s2,2)*(1 + t1*(-10 + t2) + t2 - 2*Power(t2,2)) + 
               t1*(Power(-1 + t2,2)*(1 + t2) + 
                  Power(t1,2)*(-8 + 5*t2) - 
                  t1*(6 - 14*t2 + 7*Power(t2,2) + Power(t2,3))) + 
               s2*(Power(t1,2)*(17 - 8*t2) - 
                  Power(-1 + t2,2)*(1 + t2) + 
                  t1*(5 - 15*t2 + 9*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,3)*(3*t1*(-3 + t2)*Power(-1 + t2,2) - 
               Power(-1 + t2,3)*(3 + t2) + 
               Power(s2,3)*(2 - 7*t2 - 7*Power(t2,2)) - 
               Power(t1,3)*(-11 - 3*t2 + Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-7 + 21*t2 - 23*Power(t2,2) + 
                  8*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(-15 + 18*t2 + 6*Power(t2,2) - 
                  9*Power(t2,3) + 
                  t1*(3 + 27*t2 + 5*Power(t2,2) + Power(t2,3))) + 
               s2*(Power(-1 + t2,2)*(-3 + 7*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*(-16 - 23*t2 + 3*Power(t2,2)) + 
                  t1*(31 - 65*t2 + 41*Power(t2,2) - 5*Power(t2,3) - 
                     2*Power(t2,4)))) - 
            s1*(Power(-1 + t2,3)*(7 + 2*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*Power(t2,2)*(-6 + 13*t2 + 5*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (17 - 3*t2 + 4*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,3)*(-3 + t2 + 9*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(-13 + 19*t2 + 5*Power(t2,2) - 
                  11*Power(t2,4)) - 
               Power(s2,2)*t2*
                (2 - 35*t2 + 38*Power(t2,2) + 6*Power(t2,3) - 
                  11*Power(t2,4) + 
                  t1*(-2 - t2 + 25*Power(t2,2) + 13*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(-(Power(-1 + t2,2)*
                     (-2 - 5*t2 + 14*Power(t2,2) + 13*Power(t2,3))) + 
                  Power(t1,2)*
                   (2 - 11*t2 + 15*Power(t2,2) + 24*Power(t2,3) + 
                     6*Power(t2,4)) - 
                  t1*(4 - 10*t2 + 29*Power(t2,2) - 13*Power(t2,3) - 
                     11*Power(t2,4) + Power(t2,5)))) + 
            Power(s1,2)*(Power(-1 + t2,3)*(-2 + 4*t2 + Power(t2,2)) + 
               3*Power(s2,3)*t2*(-2 + 5*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-1 + 4*t2 + 6*Power(t2,2) - 9*Power(t2,3)) - 
               Power(t1,3)*(-1 + 15*t2 + 3*Power(t2,2) + Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (2 - 23*t2 + 5*Power(t2,2) + Power(t2,3)) - 
               Power(s2,2)*(1 - 40*t2 + 48*Power(t2,2) + 
                  9*Power(t2,3) - 18*Power(t2,4) + 
                  t1*(-1 + 4*t2 + 33*Power(t2,2) + 16*Power(t2,3) + 
                     2*Power(t2,4))) + 
               s2*(-(Power(-1 + t2,2)*
                     (-7 + 6*t2 + 15*Power(t2,2) + Power(t2,3))) + 
                  Power(t1,2)*
                   (1 + 18*t2 + 24*Power(t2,2) + 11*Power(t2,3)) + 
                  t1*(-8 - 34*t2 + 71*Power(t2,2) - 29*Power(t2,3) - 
                     Power(t2,4) + Power(t2,5))))) + 
         Power(s,3)*(-1 + t2)*
          (-(Power(t1,3)*t2*(5 - 15*t2 + 24*Power(t2,2) + 
                 17*Power(t2,3) + 3*Power(t2,4))) + 
            Power(t1,2)*(-3 + (-6 + 9*s2)*t2 - 
               6*(-7 + 4*s2)*Power(t2,2) + (-17 + 31*s2)*Power(t2,3) + 
               (-9 + 71*s2)*Power(t2,4) + (-3 + 14*s2)*Power(t2,5) + 
               (-4 + s2)*Power(t2,6)) - 
            t1*(-3 + (-6 + 8*s2)*t2 + 
               (46 + 4*s2 - 3*Power(s2,2))*Power(t2,2) + 
               (-76 + 79*s2 + 3*Power(s2,2))*Power(t2,3) + 
               (63 - 100*s2 + 77*Power(s2,2))*Power(t2,4) + 
               (-49 + 6*s2 + 23*Power(s2,2))*Power(t2,5) + 
               2*(20 + Power(s2,2))*Power(t2,6) + 
               3*(-5 + s2)*Power(t2,7)) + 
            2*Power(s1,4)*Power(-1 + t2,2)*
             (-8 + 7*t2 + Power(t2,2) + s2*(6 + 10*t2 - Power(t2,2)) + 
               t1*(-7 - 9*t2 + Power(t2,2))) + 
            t2*(Power(s2,3)*Power(t2,2)*
                (-10 + 37*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               Power(-1 + t2,3)*
                (-5 - 53*t2 + 30*Power(t2,2) + 12*Power(t2,3)) - 
               s2*Power(-1 + t2,2)*
                (2 + 32*t2 - 105*Power(t2,2) + 52*Power(t2,3) + 
                  28*Power(t2,4)) + 
               Power(s2,2)*t2*
                (8 + 52*t2 - 48*Power(t2,2) - 43*Power(t2,3) + 
                  30*Power(t2,4) + Power(t2,5))) - 
            Power(s1,3)*(-1 + t2)*
             (Power(t1,2)*(22 - 93*t2 - 13*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-20 + 55*t2 + 20*Power(t2,2) + Power(t2,3)) - 
               Power(s2,2)*(-4 + 31*t2 + 55*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-15 - 42*t2 + 96*Power(t2,2) - 46*Power(t2,3) + 
                  7*Power(t2,4)) + 
               2*s2*(3 + 10*t2 - 11*Power(t2,2) - 2*Power(t2,4) + 
                  t1*(-11 + 59*t2 + 34*Power(t2,2) + 2*Power(t2,3)))) + 
            Power(s1,2)*(Power(s2,3)*t2*
                (1 + 6*t2 + 25*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(17 - 2*t2 - 59*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (-2 - 29*t2 + 57*Power(t2,2) + 32*Power(t2,3) + 
                  Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (-13 - 55*t2 + 166*Power(t2,2) + 5*Power(t2,3) + 
                  8*Power(t2,4)) + 
               3*Power(t1,2)*
                (4 - 33*t2 + 68*Power(t2,2) - 17*Power(t2,3) - 
                  24*Power(t2,4) + 2*Power(t2,5)) - 
               Power(s2,2)*(t1*
                   (-5 - 19*t2 + 81*Power(t2,2) + 41*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  t2*(37 - 118*t2 + 27*Power(t2,2) + 50*Power(t2,3) + 
                     4*Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (-19 - 25*t2 + 137*Power(t2,2) + 9*Power(t2,3)) + 
                  3*t1*t2*(27 - 79*t2 + 11*Power(t2,2) + 
                     39*Power(t2,3) + 2*Power(t2,4)) - 
                  Power(-1 + t2,2)*
                   (2 - 127*t2 + 137*Power(t2,2) + 94*Power(t2,3) + 
                     5*Power(t2,4)))) + 
            s1*(-2 - 5*(5 - 3*s2 + Power(s2,2))*t2 + 
               (126 - 290*s2 - 25*Power(s2,2) + 9*Power(s2,3))*
                Power(t2,2) - 
               (180 - 689*s2 + 33*Power(s2,2) + 43*Power(s2,3))*
                Power(t2,3) + 
               (77 - 470*s2 + 96*Power(s2,2) - 31*Power(s2,3))*
                Power(t2,4) - 
               (-6 + 39*s2 + 34*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,5) + (11 + 92*s2 + Power(s2,2))*Power(t2,6) + 
               (-13 + 3*s2)*Power(t2,7) + 
               Power(t1,3)*(7 - 35*t2 + 32*Power(t2,2) + 
                  62*Power(t2,3) + 5*Power(t2,4) - 3*Power(t2,5)) + 
               Power(t1,2)*(-6 - 32*t2 + 85*Power(t2,2) - 
                  73*Power(t2,3) - 16*Power(t2,4) + 41*Power(t2,5) + 
                  Power(t2,6) + 
                  2*s2*(-2 + 15*t2 - 3*Power(t2,2) - 92*Power(t2,3) - 
                     22*Power(t2,4) + 2*Power(t2,5))) + 
               t1*(Power(s2,2)*t2*
                   (-11 - 10*t2 + 156*Power(t2,2) + 62*Power(t2,3) + 
                     7*Power(t2,4)) - 
                  Power(-1 + t2,2)*
                   (-1 - 27*t2 - 69*Power(t2,2) + 54*Power(t2,3) + 
                     49*Power(t2,4) + 4*Power(t2,5)) + 
                  s2*(5 + 32*t2 + 6*Power(t2,2) - 55*Power(t2,3) + 
                     72*Power(t2,4) - 61*Power(t2,5) + Power(t2,6))))) - 
         Power(s,2)*Power(-1 + t2,2)*
          (1 - 3*Power(t1,2) + 2*Power(t1,3) + 
            6*Power(s1,5)*(s2 - t1)*Power(-1 + t2,2) + 9*t2 - 6*s2*t2 - 
            30*t1*t2 + 13*s2*t1*t2 + 28*Power(t1,2)*t2 - 
            7*s2*Power(t1,2)*t2 - 7*Power(t1,3)*t2 - 55*Power(t2,2) + 
            18*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) + 
            84*t1*Power(t2,2) - 24*s2*t1*Power(t2,2) - 
            3*Power(s2,2)*t1*Power(t2,2) - 52*Power(t1,2)*Power(t2,2) + 
            22*s2*Power(t1,2)*Power(t2,2) + 5*Power(t1,3)*Power(t2,2) + 
            108*Power(t2,3) - 32*s2*Power(t2,3) - 
            50*Power(s2,2)*Power(t2,3) + 6*Power(s2,3)*Power(t2,3) - 
            78*t1*Power(t2,3) + 59*s2*t1*Power(t2,3) + 
            18*Power(s2,2)*t1*Power(t2,3) + 9*Power(t1,2)*Power(t2,3) - 
            55*s2*Power(t1,2)*Power(t2,3) + 23*Power(t1,3)*Power(t2,3) - 
            95*Power(t2,4) + 49*s2*Power(t2,4) + 
            56*Power(s2,2)*Power(t2,4) - 31*Power(s2,3)*Power(t2,4) + 
            27*t1*Power(t2,4) - 41*s2*t1*Power(t2,4) + 
            61*Power(s2,2)*t1*Power(t2,4) + 16*Power(t1,2)*Power(t2,4) - 
            47*s2*Power(t1,2)*Power(t2,4) + 7*Power(t1,3)*Power(t2,4) + 
            29*Power(t2,5) - 38*s2*Power(t2,5) + 
            7*Power(s2,2)*Power(t2,5) - 5*Power(s2,3)*Power(t2,5) - 
            10*t1*Power(t2,5) - 10*s2*t1*Power(t2,5) + 
            14*Power(s2,2)*t1*Power(t2,5) - 
            3*s2*Power(t1,2)*Power(t2,5) + 11*Power(t2,6) + 
            3*s2*Power(t2,6) - 14*Power(s2,2)*Power(t2,6) + 
            11*t1*Power(t2,6) + 3*s2*t1*Power(t2,6) + 
            2*Power(t1,2)*Power(t2,6) - 10*Power(t2,7) + 
            6*s2*Power(t2,7) - 4*t1*Power(t2,7) + 2*Power(t2,8) + 
            Power(s1,4)*(-1 + t2)*
             (-2*Power(-1 + t2,2)*t2 + Power(t1,2)*(25 + 9*t2) + 
               Power(s2,2)*(7 + 25*t2 + 2*Power(t2,2)) + 
               t1*(25 - 39*t2 + 13*Power(t2,2) + Power(t2,3)) + 
               s2*(-13 + 17*t2 - 5*Power(t2,2) + Power(t2,3) - 
                  2*t1*(16 + 17*t2 + Power(t2,2)))) + 
            Power(s1,3)*(Power(-1 + t2,3)*(5 - t2 + 2*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(-37 + 67*t2 + 3*Power(t2,2)) + 
               2*Power(t1,3)*(-2 - 18*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-19 + 50*t2 + 10*Power(t2,2) - 
                  41*Power(t2,3)) + 
               Power(s2,3)*(2 + 7*t2 + 19*Power(t2,2) + 2*Power(t2,3)) - 
               Power(s2,2)*(9 - 29*t2 - 19*Power(t2,2) + 
                  36*Power(t2,3) + 3*Power(t2,4) + 
                  t1*(3 + 58*t2 + 27*Power(t2,2) + 2*Power(t2,3))) + 
               s2*(Power(t1,2)*(5 + 87*t2 - 2*Power(t2,2)) - 
                  Power(-1 + t2,2)*
                   (-41 + 43*t2 + 29*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(19 - 59*t2 - 35*Power(t2,2) + 65*Power(t2,3) + 
                     10*Power(t2,4)))) + 
            Power(s1,2)*(3*Power(t1,3)*t2*
                (15 + 15*t2 + Power(t2,2) - Power(t2,3)) + 
               Power(-1 + t2,3)*
                (14 - 34*t2 - 7*Power(t2,2) + 2*Power(t2,3)) - 
               Power(s2,3)*t2*
                (-2 + 45*t2 + 43*Power(t2,2) + 4*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (-6 - 59*t2 + 22*Power(t2,2) + 27*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*(-10 + 24*t2 - 11*Power(t2,2) - 
                  39*Power(t2,3) + 34*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,2)*(t2*
                   (-40 + 33*t2 + 23*Power(t2,2) - 16*Power(t2,3)) + 
                  t1*(-2 + 32*t2 + 163*Power(t2,2) + 68*Power(t2,3) + 
                     9*Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (3 - 120*t2 + 56*Power(t2,2) + 45*Power(t2,3) + 
                     Power(t2,4)) + 
                  Power(t1,2)*
                   (-3 - 68*t2 - 166*Power(t2,2) - 35*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  t1*(16 + 36*t2 - 113*Power(t2,2) + 109*Power(t2,3) - 
                     41*Power(t2,4) - 7*Power(t2,5)))) + 
            s1*(Power(s2,3)*Power(t2,2)*
                (-10 + 69*t2 + 29*Power(t2,2) + 2*Power(t2,3)) - 
               Power(-1 + t2,3)*t2*
                (45 - 36*t2 - 14*Power(t2,2) + 4*Power(t2,3)) - 
               2*Power(t1,3)*(-1 + t2 + 30*Power(t2,2) + 
                  13*Power(t2,3) + 2*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (7 - 25*t2 - Power(t2,2) - 22*Power(t2,3) + 
                  16*Power(t2,4)) - 
               Power(t1,2)*(9 - 32*t2 + 14*Power(t2,2) - 3*Power(t2,3) + 
                  8*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-1 + 99*t2 - 111*Power(t2,2) - 31*Power(t2,3) + 
                  43*Power(t2,4) + Power(t2,5) - 
                  t1*(-5 + 47*t2 + 166*Power(t2,2) + 55*Power(t2,3) + 
                     7*Power(t2,4))) + 
               s2*(-(Power(-1 + t2,2)*
                     (-1 + 6*t2 - 92*Power(t2,2) + 38*Power(t2,3) + 
                       24*Power(t2,4))) + 
                  Power(t1,2)*
                   (2 - 6*t2 + 106*Power(t2,2) + 134*Power(t2,3) + 
                     33*Power(t2,4) + Power(t2,5)) - 
                  t1*(3 + 18*t2 + 90*Power(t2,2) - 165*Power(t2,3) + 
                     52*Power(t2,4) + Power(t2,5) + Power(t2,6))))))*
       T4q(-1 + t2))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*(-s + s2 - t1)*Power(-1 + t2,2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) + 
    (8*(2*Power(s,7)*Power(t2,2)*(t1 + t2) + 
         2*Power(s,6)*t2*(-Power(t1,2) + t1*t2*(-2 - s2 + 5*t2) + 
            s1*(t1*(2 - 4*t2) + (2 + s2 - 3*t2)*t2) + 
            t2*(1 + t2 - 2*s2*t2 + Power(t2,2))) + 
         Power(s,5)*(-(Power(t1,3)*(-1 + t2)) + 
            Power(t1,2)*t2*(6 + s2 - 3*t2 + 3*Power(t2,2)) + 
            t1*t2*(-4 + (-17 + 9*s2)*t2 - 11*(1 + s2)*Power(t2,2) + 
               8*Power(t2,3)) + 
            Power(t2,2)*(-6 - 34*t2 + 2*Power(s2,2)*t2 + 
               27*Power(t2,2) + Power(t2,3) + 
               s2*(2 + 12*t2 - 3*Power(t2,2))) + 
            2*Power(s1,2)*(t2*
                (1 + 2*s2 - 4*t2 - 3*s2*t2 + 3*Power(t2,2)) + 
               t1*(1 - 6*t2 + 6*Power(t2,2))) + 
            s1*(Power(t1,2)*(-2 + 9*t2) + 
               t1*t2*(4*s2*(-2 + t2) - 29*(-1 + t2)*t2) + 
               t2*(4 - 2*(-6 + 7*s2 + Power(s2,2))*t2 + 
                  2*(-7 + 5*s2)*Power(t2,2) - 5*Power(t2,3)))) + 
         Power(s,4)*(Power(t1,3)*
             (-2 + 6*t2 - Power(t2,2) + Power(t2,3)) + 
            Power(t1,2)*(3 + (2 - 5*s2)*t2 + 4*Power(t2,2) - 
               (3 + 2*s2)*Power(t2,3)) - 
            t2*(2 + 2*(4 - s2 + Power(s2,2))*t2 + 
               (-89 - 26*s2 + 12*Power(s2,2))*Power(t2,2) + 
               (87 + 20*s2)*Power(t2,3) + (-31 + 2*s2)*Power(t2,4)) - 
            2*Power(s1,3)*(-1 + t2)*
             (s2 - 3*s2*t2 + (-1 + t2)*t2 + t1*(-2 + 4*t2)) + 
            t1*t2*(11 + 45*t2 + Power(s2,2)*(-1 + t2)*t2 - 
               125*Power(t2,2) + 29*Power(t2,3) + 3*Power(t2,4) - 
               s2*(1 + 6*t2 - 44*Power(t2,2) + 6*Power(t2,3))) + 
            s1*(Power(t1,3)*(-7 + 5*t2) + 
               Power(t1,2)*(1 + s2*(4 - 5*t2) - 12*t2 + 
                  17*Power(t2,2) - 8*Power(t2,3)) + 
               t1*(-4 + (-35 + 19*s2 + 2*Power(s2,2))*t2 + 
                  2*(32 - 26*s2 + Power(s2,2))*Power(t2,2) + 
                  2*(4 + 7*s2)*Power(t2,3) - 16*Power(t2,4)) - 
               t2*(4 + (77 + 13*s2 - 17*Power(s2,2))*t2 + 
                  (-146 + 5*s2 + Power(s2,2))*Power(t2,2) + 
                  (64 - 6*s2)*Power(t2,3) + 2*Power(t2,4))) + 
            Power(s1,2)*(2 + Power(t1,2)*(7 - 13*t2) - 
               2*(-8 + 4*s2 + 3*Power(s2,2))*t2 + 
               (-45 + 19*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (25 - 9*s2)*Power(t2,3) + 4*Power(t2,4) + 
               t1*(4 + 6*t2 - 39*Power(t2,2) + 27*Power(t2,3) + 
                  6*s2*(-1 + 3*t2)))) + 
         Power(s,3)*(-2*Power(s1,4)*(s2 - t1)*Power(-1 + t2,2) + 
            Power(t1,3)*t2*(-5 + 5*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(-3 + 3*(-4 + 3*s2)*t2 + 
               (21 - 6*s2)*Power(t2,2) + (-43 + 2*s2)*Power(t2,3) + 
               (16 + s2)*Power(t2,4)) + 
            t1*(3 + (12 - 8*s2)*t2 + 
               (-25 - 20*s2 + 3*Power(s2,2))*Power(t2,2) + 
               (326 + 5*s2 - 13*Power(s2,2))*Power(t2,3) - 
               2*(119 - 8*s2 + Power(s2,2))*Power(t2,4) + 
               (35 - 3*s2)*Power(t2,5)) + 
            t2*(5 + 48*t2 - 107*Power(t2,2) + 88*Power(t2,3) - 
               48*Power(t2,4) + 14*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(2 + t2) + 
               Power(s2,2)*t2*
                (8 + 20*t2 + 2*Power(t2,2) + Power(t2,3)) - 
               s2*(2 + 32*t2 + 151*Power(t2,2) - 108*Power(t2,3) + 
                  36*Power(t2,4))) + 
            Power(s1,3)*(6 - 27*t2 + 37*Power(t2,2) - 15*Power(t2,3) - 
               Power(t2,4) + Power(t1,2)*(-6 + 5*t2) + 
               Power(s2,2)*(-4 + 5*t2 + 2*Power(t2,2)) + 
               t1*(-5 + t2 + 7*Power(t2,2) - 7*Power(t2,3)) - 
               2*s2*(-1 + t2 - 2*Power(t2,3) + 
                  t1*(-5 + 4*t2 + 2*Power(t2,2)))) + 
            Power(s1,2)*(2 + Power(t1,3)*(17 - 10*t2) - 51*t2 + 
               148*Power(t2,2) - 143*Power(t2,3) + 43*Power(t2,4) + 
               Power(t2,5) + Power(s2,3)*t2*(1 + 2*t2) + 
               3*Power(t1,2)*(4 + t2 - 4*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-13 + 107*t2 - 110*Power(t2,2) + 23*Power(t2,3) + 
                  8*Power(t2,4)) + 
               s2*(-2 - 53*t2 + 73*Power(t2,2) - 28*Power(t2,3) - 
                  5*Power(t2,4) + Power(t1,2)*(-19 + 9*t2) + 
                  3*t1*t2*(-19 + 13*t2 + 2*Power(t2,2))) - 
               Power(s2,2)*(t1*(-5 + t2 + 4*Power(t2,2)) + 
                  t2*(-23 + 16*t2 + 4*Power(t2,2)))) + 
            s1*(-2 + (-29 + 15*s2 - 5*Power(s2,2))*t2 + 
               (148 + 154*s2 - 29*Power(s2,2) - 3*Power(s2,3))*
                Power(t2,2) - 
               (231 + 168*s2 - 8*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,3) + (155 + 60*s2 + Power(s2,2))*Power(t2,4) + 
               (-41 + 3*s2)*Power(t2,5) + 
               Power(t1,3)*(7 - 21*t2 + 7*Power(t2,2) - 3*Power(t2,3)) + 
               Power(t1,2)*(-6 - 44*t2 + 15*Power(t2,2) - 
                  13*Power(t2,3) + Power(t2,4) + 
                  s2*(-4 + 22*t2 - 6*Power(t2,2) + 4*Power(t2,3))) + 
               t1*(1 + 27*t2 - 393*Power(t2,2) + 374*Power(t2,3) - 
                  69*Power(t2,4) - 4*Power(t2,5) + 
                  Power(s2,2)*t2*(-11 + 4*t2 + 7*Power(t2,2)) + 
                  s2*(5 + 42*t2 + 85*Power(t2,2) - 61*Power(t2,3) + 
                     Power(t2,4))))) - 
         (s1 - t2)*(-1 + t2)*(-1 + Power(s1,4)*Power(s2 - t1,3) + 3*t1 - 
            3*Power(t1,2) + Power(t1,3) + 3*t2 - 3*s2*t2 - 6*t1*t2 + 
            6*s2*t1*t2 + 3*Power(t1,2)*t2 - 3*s2*Power(t1,2)*t2 - 
            Power(t2,2) - 20*s2*Power(t2,2) + 7*Power(s2,2)*Power(t2,2) + 
            25*t1*Power(t2,2) + 4*s2*t1*Power(t2,2) - 
            7*Power(s2,2)*t1*Power(t2,2) - 18*Power(t1,2)*Power(t2,2) + 
            16*s2*Power(t1,2)*Power(t2,2) - 6*Power(t1,3)*Power(t2,2) - 
            5*Power(t2,3) + 49*s2*Power(t2,3) - 
            9*Power(s2,2)*Power(t2,3) + Power(s2,3)*Power(t2,3) - 
            40*t1*Power(t2,3) - 36*s2*t1*Power(t2,3) + 
            8*Power(s2,2)*t1*Power(t2,3) + 42*Power(t1,2)*Power(t2,3) - 
            14*s2*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) + 
            6*Power(t2,4) - 26*s2*Power(t2,4) + 14*t1*Power(t2,4) + 
            30*s2*t1*Power(t2,4) - 4*Power(s2,2)*t1*Power(t2,4) - 
            24*Power(t1,2)*Power(t2,4) + 4*s2*Power(t1,2)*Power(t2,4) - 
            2*Power(t2,5) + 2*Power(s2,2)*Power(t2,5) + 
            4*t1*Power(t2,5) - 4*s2*t1*Power(t2,5) - 
            Power(s1,3)*(s2 - t1)*
             (4*Power(t1,2) - 6*Power(-1 + t2,2) + 
               Power(s2,2)*(1 + 3*t2) + t1*(1 - 5*t2 + 4*Power(t2,2)) + 
               s2*(5 - 7*t2 + 2*Power(t2,2) - t1*(5 + 3*t2))) + 
            Power(s1,2)*(-2*Power(-1 + t2,3) + 
               6*Power(t1,3)*(-2 + t2)*t2 + 3*Power(s2,3)*t2*(1 + t2) + 
               t1*Power(-1 + t2,2)*(25 + 16*t2) - 
               Power(t1,2)*(27 - 55*t2 + 20*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,2)*(7 + t2 - 14*Power(t2,2) + 6*Power(t2,3) - 
                  t1*(7 + 4*t2 + 7*Power(t2,2))) + 
               s2*(-(Power(-1 + t2,2)*(29 + 12*t2)) + 
                  Power(t1,2)*(7 + 13*t2 - 2*Power(t2,2)) + 
                  t1*(22 - 62*t2 + 40*Power(t2,2)))) + 
            s1*(-(Power(s2,3)*Power(t2,2)*(3 + t2)) + 
               Power(-1 + t2,3)*(-1 + 4*t2) - 
               2*t1*Power(-1 + t2,2)*(3 + 22*t2 + 7*Power(t2,2)) - 
               4*Power(t1,3)*(1 - 3*t2 + Power(t2,3)) + 
               Power(t1,2)*(9 + 27*t2 - 86*Power(t2,2) + 
                  46*Power(t2,3) + 4*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-14 + 13*t2 + 7*Power(t2,2) - 6*Power(t2,3) + 
                  2*t1*(7 - 5*t2 + 4*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(3 + 55*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*
                   (3 - 23*t2 + 10*Power(t2,2) - 2*Power(t2,3)) + 
                  t1*(-6 - 26*t2 + 94*Power(t2,2) - 68*Power(t2,3) + 
                     6*Power(t2,4))))) - 
         Power(s,2)*(-1 + 3*Power(t1,2) - 2*Power(t1,3) - 10*t2 + 
            6*s2*t2 + 30*t1*t2 - 13*s2*t1*t2 - 25*Power(t1,2)*t2 + 
            7*s2*Power(t1,2)*t2 + 5*Power(t1,3)*t2 + 45*Power(t2,2) - 
            12*s2*Power(t2,2) - Power(s2,2)*Power(t2,2) - 
            54*t1*Power(t2,2) + 11*s2*t1*Power(t2,2) + 
            3*Power(s2,2)*t1*Power(t2,2) + 27*Power(t1,2)*Power(t2,2) - 
            15*s2*Power(t1,2)*Power(t2,2) - 57*Power(t2,3) - 
            164*s2*Power(t2,3) - 11*Power(s2,2)*Power(t2,3) + 
            6*Power(s2,3)*Power(t2,3) + 262*t1*Power(t2,3) + 
            148*s2*t1*Power(t2,3) - 31*Power(s2,2)*t1*Power(t2,3) - 
            122*Power(t1,2)*Power(t2,3) + 16*s2*Power(t1,2)*Power(t2,3) + 
            5*Power(t1,3)*Power(t2,3) + 24*Power(t2,4) + 
            241*s2*Power(t2,4) - 13*Power(s2,2)*Power(t2,4) + 
            Power(s2,3)*Power(t2,4) - 377*t1*Power(t2,4) - 
            81*s2*t1*Power(t2,4) + 12*Power(s2,2)*t1*Power(t2,4) + 
            90*Power(t1,2)*Power(t2,4) + 3*s2*Power(t1,2)*Power(t2,4) - 
            10*Power(t1,3)*Power(t2,4) - 3*Power(t2,5) - 
            95*s2*Power(t2,5) - 8*Power(s2,2)*Power(t2,5) + 
            157*t1*Power(t2,5) + s2*t1*Power(t2,5) - 
            6*Power(t1,2)*Power(t2,5) + 4*Power(t2,6) + 
            24*s2*Power(t2,6) - 18*t1*Power(t2,6) - 2*Power(t2,7) + 
            Power(s1,4)*(Power(t1,2)*(1 - 3*t2) - 2*Power(-1 + t2,3) + 
               Power(s2,2)*(-1 - 3*t2 + 2*Power(t2,2)) + 
               t1*(-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               s2*(3 + (-9 + 6*t1)*t2 + (5 - 2*t1)*Power(t2,2) + 
                  Power(t2,3))) + 
            Power(s1,3)*(-2*Power(t1,3)*(-9 + 5*t2) + 
               Power(s2,3)*(-2 - 3*t2 + 2*Power(t2,2)) + 
               Power(-1 + t2,2)*(5 - 17*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(21 - 23*t2 + 15*Power(t2,2)) + 
               t1*(-27 + 66*t2 - 56*Power(t2,2) + 17*Power(t2,3)) + 
               Power(s2,2)*(-3 + 16*t2 + 3*Power(t2,2) - 
                  3*Power(t2,3) + t1*(13 + 3*t2 - 2*Power(t2,2))) + 
               s2*(23 - 66*t2 + 66*Power(t2,2) - 21*Power(t2,3) - 
                  2*Power(t2,4) + Power(t1,2)*(-29 + 10*t2) + 
                  t1*(-5 - 12*t2 - 19*Power(t2,2) + 10*Power(t2,3)))) - 
            Power(s1,2)*(3*Power(t1,3)*t2*(9 - 4*t2 + Power(t2,2)) + 
               Power(s2,3)*t2*(-10 - 7*t2 + 4*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-14 + 34*t2 - 23*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,2)*(-10 + 104*t2 - 75*Power(t2,2) + 
                  36*Power(t2,3) - 2*Power(t2,4)) + 
               t1*(6 - 247*t2 + 483*Power(t2,2) - 275*Power(t2,3) + 
                  32*Power(t2,4) + Power(t2,5)) + 
               Power(s2,2)*(t2*(26 + 17*t2 + 10*Power(t2,2)) + 
                  t1*(-2 + 36*t2 + 19*Power(t2,2) - 9*Power(t2,3))) - 
               s2*(-3 - 111*t2 + 250*Power(t2,2) - 181*Power(t2,3) + 
                  44*Power(t2,4) + Power(t2,5) + 
                  Power(t1,2)*
                   (3 + 47*t2 - 3*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(-16 + 86*t2 + 43*Power(t2,2) - 7*Power(t2,4)))) + 
            s1*(Power(s2,3)*Power(t2,2)*(-14 - 5*t2 + 2*Power(t2,2)) + 
               2*Power(t1,3)*(-1 + 7*Power(t2,3)) + 
               Power(-1 + t2,2)*t2*
                (-45 + 30*t2 - 6*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(9 - 23*t2 + 219*Power(t2,2) - 
                  162*Power(t2,3) + 32*Power(t2,4)) + 
               t1*(-7 + 32*t2 - 498*Power(t2,2) + 815*Power(t2,3) - 
                  378*Power(t2,4) + 36*Power(t2,5)) + 
               Power(s2,2)*t2*
                (1 + 40*t2 + 15*Power(t2,2) + 18*Power(t2,3) + 
                  Power(t2,4) + 
                  t1*(-5 + 54*t2 + 4*Power(t2,2) - 7*Power(t2,3))) + 
               s2*(-1 + 7*t2 + 256*Power(t2,2) - 432*Power(t2,3) + 
                  222*Power(t2,4) - 52*Power(t2,5) + 
                  Power(t1,2)*
                   (-2 + 4*t2 - 30*Power(t2,2) - 14*Power(t2,3) + 
                     Power(t2,4)) + 
                  t1*(3 + 21*t2 - 237*Power(t2,2) + 58*Power(t2,3) + 
                     6*Power(t2,4) - Power(t2,5))))) - 
         s*(-1 + 3*t1 - 3*Power(t1,2) + Power(t1,3) + 
            2*Power(s1,5)*Power(s2 - t1,2)*(-1 + t2) + 10*t2 - 2*s2*t2 - 
            26*t1*t2 + 4*s2*t1*t2 + 22*Power(t1,2)*t2 - 
            2*s2*Power(t1,2)*t2 - 6*Power(t1,3)*t2 - 20*Power(t2,2) + 
            6*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) + 
            44*t1*Power(t2,2) - 18*s2*t1*Power(t2,2) - 
            Power(s2,2)*t1*Power(t2,2) - 29*Power(t1,2)*Power(t2,2) + 
            12*s2*Power(t1,2)*Power(t2,2) + 5*Power(t1,3)*Power(t2,2) + 
            6*Power(t2,3) + 91*s2*Power(t2,3) - 
            6*Power(s2,2)*Power(t2,3) - 2*Power(s2,3)*Power(t2,3) - 
            117*t1*Power(t2,3) - 64*s2*t1*Power(t2,3) + 
            20*Power(s2,2)*t1*Power(t2,3) + 80*Power(t1,2)*Power(t2,3) - 
            29*s2*Power(t1,2)*Power(t2,3) + Power(t1,3)*Power(t2,3) + 
            19*Power(t2,4) - 208*s2*Power(t2,4) + 
            9*Power(s2,2)*Power(t2,4) + 4*Power(s2,3)*Power(t2,4) + 
            219*t1*Power(t2,4) + 162*s2*t1*Power(t2,4) - 
            39*Power(s2,2)*t1*Power(t2,4) - 140*Power(t1,2)*Power(t2,4) + 
            34*s2*Power(t1,2)*Power(t2,4) - 22*Power(t2,5) + 
            135*s2*Power(t2,5) + 2*Power(s2,2)*Power(t2,5) + 
            Power(s2,3)*Power(t2,5) - 155*t1*Power(t2,5) - 
            92*s2*t1*Power(t2,5) + 11*Power(s2,2)*t1*Power(t2,5) + 
            70*Power(t1,2)*Power(t2,5) - 6*s2*Power(t1,2)*Power(t2,5) - 
            4*Power(t1,3)*Power(t2,5) + 10*Power(t2,6) - 
            28*s2*Power(t2,6) - 6*Power(s2,2)*Power(t2,6) + 
            36*t1*Power(t2,6) + 8*s2*t1*Power(t2,6) - 2*Power(t2,7) + 
            6*s2*Power(t2,7) - 4*t1*Power(t2,7) + 
            Power(s1,4)*(Power(s2,3)*(1 + 2*t2) + 
               Power(s2,2)*(1 + t1*(-10 + t2) + t2 - 2*Power(t2,2)) + 
               s2*(Power(t1,2)*(17 - 8*t2) + 
                  (-3 + t2)*Power(-1 + t2,2) - 
                  t1*(-7 + 21*t2 - 15*Power(t2,2) + Power(t2,3))) + 
               t1*(Power(-1 + t2,2)*(-1 + 3*t2) + 
                  Power(t1,2)*(-8 + 5*t2) + 
                  t1*(-8 + 20*t2 - 13*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,3)*(3*Power(-1 + t2,3)*(1 + t2) + 
               Power(s2,3)*(2 - 7*t2 - 7*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(-45 + 25*t2 + 8*Power(t2,2)) + 
               Power(t1,3)*(9 + 9*t2 - 7*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(21 - 53*t2 + 47*Power(t2,2) - 
                  16*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(-19 + 36*t2 - 18*Power(t2,2) + Power(t2,3) + 
                  t1*(5 + 21*t2 + 11*Power(t2,2) - Power(t2,3))) + 
               s2*(Power(t1,2)*(-16 - 23*t2 + 3*Power(t2,2)) - 
                  Power(-1 + t2,2)*(11 - 7*t2 + 8*Power(t2,2)) + 
                  t1*(29 - 57*t2 + 35*Power(t2,2) - 9*Power(t2,3) + 
                     2*Power(t2,4)))) + 
            Power(s1,2)*(3*Power(s2,3)*t2*(-2 + 5*t2 + 3*Power(t2,2)) - 
               Power(-1 + t2,3)*(-2 - 2*t2 + 7*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (2 - 197*t2 + 77*Power(t2,2) + Power(t2,3)) - 
               Power(t1,3)*(1 + 27*t2 - 27*Power(t2,2) + 
                  17*Power(t2,3)) + 
               Power(t1,2)*(1 + 134*t2 - 264*Power(t2,2) + 
                  133*Power(t2,3) - 4*Power(t2,4)) + 
               Power(s2,2)*(1 + 32*t2 - 66*Power(t2,2) + 
                  43*Power(t2,3) - 10*Power(t2,4) + 
                  t1*(-1 + 10*t2 - 51*Power(t2,2) - 14*Power(t2,3) + 
                     2*Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (-7 + 126*t2 - 21*Power(t2,2) + 19*Power(t2,3)) + 
                  Power(t1,2)*
                   (-1 + 30*t2 + 6*Power(t2,2) + 19*Power(t2,3)) - 
                  t1*(-8 + 176*t2 - 301*Power(t2,2) + 129*Power(t2,3) + 
                     3*Power(t2,4) + Power(t2,5)))) - 
            s1*(Power(s2,3)*Power(t2,2)*(-6 + 13*t2 + 5*Power(t2,2)) - 
               Power(-1 + t2,3)*
                (7 + 2*t2 - 10*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,3)*(-3 + t2 - 15*Power(t2,2) + 25*Power(t2,3) - 
                  20*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (17 - 3*t2 + 244*Power(t2,2) - 76*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(t1,2)*(13 - 19*t2 + 187*Power(t2,2) - 
                  350*Power(t2,3) + 165*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s2,2)*t2*
                (2 + 7*t2 - 20*Power(t2,2) + 26*Power(t2,3) - 
                  15*Power(t2,4) + 
                  t1*(-2 + 35*t2 - 79*Power(t2,2) + 9*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (-2 + 11*t2 - 15*Power(t2,2) + 34*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (-2 - 5*t2 + 212*Power(t2,2) - 33*Power(t2,3) + 
                     18*Power(t2,4)) + 
                  t1*(4 - 10*t2 - 211*Power(t2,2) + 413*Power(t2,3) - 
                     203*Power(t2,4) + 7*Power(t2,5))))))*T5q(s))/
     (s*(-1 + s1)*(-1 + s2)*(-s + s2 - t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)));
   return a;
};
