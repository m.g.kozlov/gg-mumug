#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m321_6_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(Power(s2,5)*t1*(-14 - Power(s1,2) + (15 + t1)*t2 + 
            2*Power(t2,2) + s1*(1 + t1 + 3*t2)) - 
         Power(t1,2)*(1 + t1 - t2)*
          ((-3 + s1)*Power(t1,2) - t1*(-9 + t2) + 2*t2 + 
            Power(s,2)*(-2 + t1 + t2) + 
            s*(3 + Power(t1,2) - 3*t2 + t1*(-9 + s1 + t2))) + 
         Power(s2,4)*(Power(t1,3)*(-5 + s + s1 - 2*t2) - 
            2*(-1 + t2 + Power(t2,2)) + 
            Power(t1,2)*(24 + s - 6*s1 - 3*s*s1 + 7*Power(s1,2) - 
               39*t2 + 3*s*t2 - 8*s1*t2 + 4*Power(t2,2)) + 
            t1*(-5 + s*(14 + s1*(5 - 3*t2) - 20*t2) + 8*t2 - 
               21*Power(t2,2) + 7*Power(t2,3) + Power(s1,2)*(1 + 4*t2) + 
               s1*(6 - 15*t2 + Power(t2,2)))) + 
         s2*t1*(2*(-1 + s1)*Power(t1,4)*(-9 + 4*t2) + 
            4*(-2 + 6*t2 - 5*Power(t2,2) + Power(t2,3)) + 
            t1*(-5 + 3*t2 + 35*Power(t2,2) - 27*Power(t2,3) + 
               8*s1*Power(-1 + t2,2)*(1 + t2)) + 
            Power(t1,3)*(-36 - 6*t2 + 8*Power(t2,2) + 
               Power(s1,2)*(-21 + 8*t2) + s1*(70 - 7*t2 - 8*Power(t2,2))) \
- Power(s,2)*(Power(t1,3) + Power(t1,2)*(1 - 2*t2) + 
               t1*(-17 + 20*t2 - 3*Power(t2,2)) - 
               5*(2 - 3*t2 + Power(t2,2))) + 
            Power(t1,2)*(53 - 4*Power(s1,2)*(-2 + t2) + 2*t2 + 
               3*Power(t2,2) + s1*(-21 - 22*t2 + 7*Power(t2,2))) + 
            s*(3*Power(-1 + t2,2)*(3 + 2*t2) + 
               Power(t1,3)*(-13 + 4*s1 + 3*t2) + 
               Power(t1,2)*(-9 + 2*s1*(-1 + t2) - 3*t2 + 2*Power(t2,2)) - 
               t1*(-1 + t2)*(16 + 27*t2 + Power(t2,2) - 2*s1*(1 + 2*t2)))) \
- Power(s2,3)*(Power(t1,4)*(-10 + 2*s + 5*s1 - t2) + 
            Power(t1,3)*(-20 + 2*Power(s,2) + s*(8 - 6*s1) + 
               11*Power(s1,2) + s1*(9 - 15*t2) - 9*t2 + 6*Power(t2,2)) + 
            2*(2 - 3*t2 - Power(t2,2) + Power(t2,3)) + 
            Power(t1,2)*(29 + 23*Power(s1,2) - 9*t2 - 15*Power(t2,2) + 
               Power(t2,3) - s*(-1 + t2)*(10 + 6*s1 + 3*t2) + 
               Power(s,2)*(-5 + 4*t2) + s1*(-56 - 23*t2 + 10*Power(t2,2))\
) + t1*(-11 + 4*Power(s1,2)*(-2 + t2) - 20*t2 - 49*Power(t2,2) + 
               34*Power(t2,3) + 
               Power(s,2)*(-14 + 11*t2 + 2*Power(t2,2)) + 
               s1*(15 + 28*t2 + Power(t2,2) - 8*Power(t2,3)) + 
               s*(1 - 36*t2 + 22*Power(t2,2) + 7*Power(t2,3) - 
                  2*s1*(-4 + t2 + 2*Power(t2,2))))) + 
         Power(s2,2)*((-5 + s + 3*s1)*Power(t1,5) + 
            Power(t1,4)*(-48 + 2*Power(s,2) + 5*Power(s1,2) + 
               s1*(32 - 18*t2) + s*(7 - 3*s1 - 3*t2) + 23*t2) + 
            2*(1 - 2*t2 + Power(t2,3)) - 
            Power(t1,3)*(-61 + 2*Power(s,2)*(-1 + t2) - 7*t2 + 
               20*Power(t2,2) + Power(s1,2)*(-43 + 12*t2) + 
               s1*(131 + t2 - 17*Power(t2,2)) + 
               s*(-10 - 10*t2 + 3*Power(t2,2) + 3*s1*(1 + t2))) + 
            t1*(24 - 35*t2 - 18*Power(t2,2) + 23*Power(t2,3) - 
               8*s1*Power(-1 + t2,2)*(1 + t2) + 
               Power(s,2)*(-24 + 26*t2 - 3*Power(t2,2)) + 
               s*(-1 + t2)*(22 + 26*t2 + Power(t2,2) - s1*(3 + 4*t2))) - 
            Power(t1,2)*(54 - 8*Power(s1,2)*(-2 + t2) + 36*t2 + 
               32*Power(t2,2) - 28*Power(t2,3) + 
               Power(s,2)*(24 - 27*t2 + 4*Power(t2,2)) + 
               s1*(-37 - 49*t2 + 6*Power(t2,2) + 8*Power(t2,3)) + 
               s*(4 + 27*t2 - 26*Power(t2,2) - Power(t2,3) + 
                  s1*(-11 + 4*t2 + 4*Power(t2,2)))))))/
     ((-1 + s1)*(-1 + s2)*s2*(-1 + t1)*t1*Power(-s2 + t1,2)*
       (1 - s2 + t1 - t2)*(-1 + t2)) - 
    (8*(4*(-1 + s)*Power(t1,5) - 
         Power(s2,6)*(Power(s1,2)*(1 + t1) - s1*(-4 + t1)*t2 + 
            3*Power(t2,2)) - Power(1 + (-1 + s)*t2,2)*
          (-1 + 4*t2 - 4*Power(t2,2) + Power(t2,3)) + 
         Power(t1,3)*(23 - 18*t2 + 8*Power(t2,2) - 3*Power(t2,3) + 
            Power(s,2)*(-5 + 4*t2) + 
            Power(s1,2)*(-9 + 16*t2 - 2*Power(t2,2)) - 
            s1*(-22 + 37*t2 + 10*Power(t2,2) + 2*Power(t2,3)) + 
            s*(-2 + (42 - 22*s1)*t2 + (5 + 3*s1)*Power(t2,2) + 
               3*Power(t2,3))) + 
         Power(t1,4)*(26 - 4*Power(s1,2) - Power(s,2)*(-2 + t2) - t2 + 
            Power(t2,2) + s1*(8 + 9*t2 + Power(t2,2)) + 
            s*(-32 - 6*t2 - Power(t2,2) + s1*(6 + t2))) + 
         Power(t1,2)*(Power(s,2)*
             (1 + 6*t2 - 10*Power(t2,2) + 2*Power(t2,3)) - 
            s*(-2 - 16*t2 + 8*Power(t2,2) + 6*Power(t2,3) + 
               3*Power(t2,4) + 
               s1*(-8 + 29*t2 - 20*Power(t2,2) + Power(t2,3))) + 
            (-1 + t2)*(-13 + 26*t2 - 13*Power(t2,2) + 3*Power(t2,3) + 
               Power(s1,2)*(-3 + 2*t2 - 2*Power(t2,2)) + 
               s1*(14 - 11*t2 + 15*Power(t2,2) + Power(t2,3)))) + 
         t1*(-11 - 2*(-16 + 6*s + Power(s,2))*t2 + 
            (-35 + 18*s + 3*Power(s,2))*Power(t2,2) + 
            (17 - 8*s)*Power(t2,3) + (-2 + s)*Power(t2,4) + 
            (-1 + s)*Power(t2,5) - 
            s1*(-1 + t2)*(-(Power(-1 + t2,2)*(-4 + 3*t2)) + 
               s*(-2 + 10*t2 - 9*Power(t2,2) + 3*Power(t2,3)))) + 
         Power(s2,5)*(Power(s1,2)*
             (-2 + 3*Power(t1,2) - 2*t1*(-2 + t2) - 3*t2 + Power(t2,2)) \
+ t2*(4 + 5*t2 + 5*Power(t2,2) + Power(t2,3) + 
               t1*(5 + 8*t2 - Power(t2,2)) + 
               s*(2 - 4*t1 - Power(t1,2) + 8*t2 + Power(t2,2))) - 
            s1*(-2 - 13*t2 + (12 + s)*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(1 + 2*s + 2*t2) + 
               t1*(-6 - 13*t2 - 3*Power(t2,2) + s*(2 + t2)))) - 
         Power(s2,4)*(1 + s*Power(t1,3)*(-1 + s - t2) + 13*t2 - 2*s*t2 - 
            19*Power(t2,2) + 16*s*Power(t2,2) + Power(s,2)*Power(t2,2) - 
            6*Power(t2,3) - 8*s*Power(t2,3) + Power(s,2)*Power(t2,3) - 
            17*Power(t2,4) - s*Power(t2,4) - 2*Power(t2,5) + 
            t1*(5 + (23 + 6*s - 2*Power(s,2))*t2 + 
               (9 + 17*s - Power(s,2))*Power(t2,2) - 
               (-10 + s)*Power(t2,3) + 3*Power(t2,4)) + 
            Power(s1,2)*(3*Power(t1,3) - 5*Power(t1,2)*(-1 + t2) - 
               2*(-4 + t2)*Power(t2,2) + t1*(-15 - 4*t2 + Power(t2,2))) \
+ Power(t1,2)*(2 - Power(s,2)*(-1 + t2) + 14*t2 + 3*Power(t2,2) - 
               Power(t2,3) + 3*s*(-2 - 4*t2 + Power(t2,2))) + 
            s1*(-4 - 9*t2 - 9*(4 + s)*Power(t2,2) + 
               (1 + s)*Power(t2,3) + 4*Power(t2,4) - 
               Power(t1,3)*(2 + 4*s + t2) + 
               Power(t1,2)*(19 + 20*t2 + 3*Power(t2,2) + 
                  s*(-9 + 4*t2)) + 
               t1*(21 + 6*t2 - 23*Power(t2,2) - 4*Power(t2,3) + 
                  s*(2 + 2*t2 + 3*Power(t2,2))))) + 
         Power(s2,3)*(-2 + (-1 + s)*s*Power(t1,4) - 6*t2 - 4*s*t2 - 
            9*Power(t2,2) - 12*s*Power(t2,2) - 
            2*Power(s,2)*Power(t2,2) + 8*Power(t2,3) - 
            47*s*Power(t2,3) - 13*Power(t2,4) - 3*s*Power(t2,4) - 
            2*Power(s,2)*Power(t2,4) + 11*Power(t2,5) - s*Power(t2,5) + 
            Power(t2,6) + t1*
             (17 + 2*(-3 + 9*s + 2*Power(s,2))*t2 + 
               (-74 + 28*s + 3*Power(s,2))*Power(t2,2) + 
               (-32 + 3*Power(s,2))*Power(t2,3) + 
               (-19 + 4*s)*Power(t2,4) - 2*Power(t2,5)) + 
            Power(t1,3)*(6 - 3*Power(s,2)*(-1 + t2) + 8*t2 - 
               2*Power(t2,2) + 2*s*(-8 - 2*t2 + Power(t2,2))) + 
            Power(t1,2)*(21 + 17*t2 + 11*Power(t2,2) + 14*Power(t2,3) + 
               Power(t2,4) + Power(s,2)*(-2 - 6*t2 + Power(t2,2)) + 
               s*(-10 + 23*t2 + 8*Power(t2,2) - 5*Power(t2,3))) + 
            Power(s1,2)*(2 + Power(t1,4) + Power(t1,3)*(2 - 4*t2) + 
               t2 - 9*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(-31 + Power(t2,2)) + 
               t1*(2 + 15*t2 + 4*Power(t2,2) + Power(t2,3))) - 
            s1*((1 + 2*s)*Power(t1,4) + 
               t2*(17 - 3*(-3 + s)*t2 - 3*(13 + 5*s)*Power(t2,2) - 
                  (16 + s)*Power(t2,3) + 2*Power(t2,4)) - 
               Power(t1,3)*(23 + 11*t2 + s*(-8 + 7*t2)) + 
               Power(t1,2)*(-32 + 19*t2 + 20*Power(t2,2) - 
                  Power(t2,3) + s*(-17 - 13*t2 + Power(t2,2))) + 
               t1*(33 + 85*t2 + 29*Power(t2,2) + 4*Power(t2,3) - 
                  Power(t2,4) + 
                  s*(-4 + 16*t2 + 8*Power(t2,2) + 5*Power(t2,3))))) + 
         s2*(2 + 2*s*t2 - 9*Power(t2,2) + 4*s*Power(t2,2) + 
            2*Power(s,2)*Power(t2,2) + 12*Power(t2,3) - 
            6*s*Power(t2,3) - 2*Power(s,2)*Power(t2,3) - 6*Power(t2,4) - 
            Power(s,2)*Power(t2,4) + s*Power(t2,5) + Power(t2,6) - 
            s*Power(t2,6) + t1*
             (-15 + (11 + 2*s - 4*Power(s,2))*t2 + 
               (45 - 64*s + 3*Power(s,2))*Power(t2,2) + 
               (-62 + 50*s - 3*Power(s,2))*Power(t2,3) + 
               2*(13 + 3*s + Power(s,2))*Power(t2,4) + 
               (-5 + 4*s)*Power(t2,5)) + 
            Power(t1,4)*(2*Power(s,2)*(-4 + t2) + t2*(16 + t2) - 
               s*(-8 + 10*t2 + Power(t2,2))) + 
            Power(s1,2)*t1*(-4 + 3*Power(t1,3)*(-3 + t2) + 9*t2 - 
               11*Power(t2,2) + 5*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(13 + 4*t2 + Power(t2,2)) - 
               t1*(-21 + 27*t2 + 8*Power(t2,2) + Power(t2,3))) - 
            Power(t1,3)*(29 + 67*t2 + 7*Power(t2,2) + 5*Power(t2,3) + 
               Power(s,2)*(1 - 11*t2 + 2*Power(t2,2)) - 
               2*s*(26 + 21*t2 + 10*Power(t2,2) + 2*Power(t2,3))) + 
            Power(t1,2)*(-42 + 6*t2 + Power(t2,2) - 3*Power(t2,3) + 
               8*Power(t2,4) + 
               Power(s,2)*(2 + Power(t2,2) - 2*Power(t2,3)) - 
               s*(-10 + 82*t2 + 68*Power(t2,2) + 17*Power(t2,3) + 
                  6*Power(t2,4))) + 
            s1*(Power(t1,4)*(1 + s*(17 - 5*t2) - 5*t2 + Power(t2,2)) + 
               (-1 + t2)*(2 - 6*t2 + (5 + 2*s)*Power(t2,2) - 
                  s*Power(t2,3) + (-1 + s)*Power(t2,4)) + 
               Power(t1,3)*(-77 + 4*t2 - 22*Power(t2,2) - 
                  3*Power(t2,3) + s*(6 - 31*t2 + 2*Power(t2,2))) + 
               Power(t1,2)*(-47 + 23*t2 + 79*Power(t2,2) + 
                  23*Power(t2,3) + 3*Power(t2,4) + 
                  s*(-15 + 65*t2 + 3*Power(t2,2) + 4*Power(t2,3))) + 
               t1*(23 - 48*t2 + 44*Power(t2,2) + Power(t2,3) - 
                  19*Power(t2,4) - Power(t2,5) + 
                  s*(-2 + t2 + 26*Power(t2,2) - 19*Power(t2,3) - 
                     2*Power(t2,4))))) + 
         Power(s2,2)*(Power(t1,4)*
             (-4 + s*(9 - 2*t2) + Power(s,2)*(-1 + t2) + t2) + 
            t1*(18 + (25 + 18*s)*t2 + 
               (-11 + 107*s + 2*Power(s,2))*Power(t2,2) + 
               9*(2 + 5*s)*Power(t2,3) + 
               (-14 + 9*s + 2*Power(s,2))*Power(t2,4) + 
               3*(-2 + s)*Power(t2,5)) - 
            Power(t1,3)*(16 + t2 + 23*Power(t2,2) + 2*Power(t2,3) + 
               2*Power(s,2)*(-4 - 2*t2 + Power(t2,2)) - 
               s*(5 - 25*t2 + 13*Power(t2,2) + Power(t2,3))) + 
            Power(s1,2)*(1 - 2*t2 + Power(t1,4)*t2 + Power(t2,2) + 
               3*Power(t2,3) - 3*Power(t2,4) - 
               Power(t1,3)*(-27 + 4*t2 + Power(t2,2)) - 
               Power(t1,2)*(13 + 13*t2 + 3*Power(t2,2) + Power(t2,3)) + 
               t1*(-12 + 2*t2 + 22*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4))) - 
            Power(t1,2)*(Power(s,2)*t2*(13 + 4*t2) - 
               2*(-10 + 52*t2 + 26*Power(t2,2) + 13*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s*(16 + 64*t2 + 25*Power(t2,2) + 16*Power(t2,3) + 
                  3*Power(t2,4))) + 
            t2*(17 - 30*t2 + 21*Power(t2,3) - 10*Power(t2,4) + 
               2*Power(t2,5) + 
               Power(s,2)*Power(t2,2)*(3 + t2 - Power(t2,2)) - 
               s*(4 - 18*t2 - 12*Power(t2,2) + 20*Power(t2,3) + 
                  4*Power(t2,4) + Power(t2,5))) - 
            s1*(4 + t2 + (-16 + s)*Power(t2,2) + (23 + 9*s)*Power(t2,3) - 
               (3 + 7*s)*Power(t2,4) - (9 + s)*Power(t2,5) + 
               Power(t1,4)*(10 + s*(-1 + 2*t2)) + 
               Power(t1,3)*(16 - 23*t2 - 2*Power(t2,2) + Power(t2,3) + 
                  s*(34 + 2*t2 - 3*Power(t2,2))) - 
               Power(t1,2)*(101 + 55*t2 + 20*Power(t2,2) + 
                  8*Power(t2,3) + 2*Power(t2,4) + 
                  s*(-9 + 27*t2 + 14*Power(t2,2) + Power(t2,3))) + 
               t1*(-13 - 68*t2 + 96*Power(t2,2) + 54*Power(t2,3) + 
                  11*Power(t2,4) + Power(t2,5) + 
                  s*(-4 + 10*t2 + 44*Power(t2,2) + 4*Power(t2,3) + 
                     3*Power(t2,4))))))*B1(1 - s2 + t1 - t2,s2,t1))/
     ((-1 + s1)*(-1 + t1)*(1 - s2 + t1 - t2)*(-1 + t2)*Power(t1 - s2*t2,2)) \
- (8*(Power(s2,9)*t2*(-2 + s1 + t2) + 
         Power(s2,8)*(2*Power(s1,2)*(1 + t1) - 
            s1*(t1 + 3*t1*t2 + (-7 + s - 6*t2)*t2) + 
            t1*(2 + (8 + s)*t2 - 2*Power(t2,2)) + 
            t2*(-11 + 2*s - 6*t2 + 4*Power(t2,2))) + 
         Power(t1,3)*(1 + t1 - t2)*
          ((-3 + s1)*Power(t1,2) - t1*(-9 + t2) + 2*t2 + 
            Power(s,2)*(-2 + t1 + t2) + 
            s*(3 + Power(t1,2) - 3*t2 + t1*(-9 + s1 + t2))) - 
         Power(s2,7)*(2*Power(s1,2)*
             (4*Power(t1,2) + t1*(7 - 3*t2) - 3*t2) + 
            Power(t1,2)*(9 + s + 14*t2 + 2*s*t2) + 
            t1*(-11 - 51*t2 + Power(s,2)*t2 + Power(t2,2) - 
               Power(t2,3) + s*(2 + 20*t2 - 5*Power(t2,2))) + 
            t2*(-17 + Power(s,2)*(-2 + t2) + 5*t2 + 36*Power(t2,2) - 
               3*Power(t2,3) + s*(-9 + 14*t2 + 3*Power(t2,2))) + 
            s1*(4 + 50*t2 + 4*s*t2 + s*Power(t2,2) - 13*Power(t2,3) - 
               Power(t1,2)*(3 + 4*s + 6*t2) + 
               t1*(11 - 5*s + 42*t2 + 19*Power(t2,2)))) + 
         Power(s2,6)*(2 + 54*t2 - 4*s*t2 + 4*Power(s,2)*t2 + 
            12*Power(t2,2) + 55*s*Power(t2,2) - 
            4*Power(s,2)*Power(t2,2) + 79*Power(t2,3) - 
            22*s*Power(t2,3) - Power(s,2)*Power(t2,3) - 36*Power(t2,4) - 
            3*s*Power(t2,4) + 
            2*Power(t1,3)*(8 + s + Power(s,2) + 7*t2 + s*t2 + 
               Power(t2,2)) + 
            2*Power(s1,2)*(6*Power(t1,3) - Power(-1 + t2,2) - 
               2*Power(t1,2)*(-8 + 5*t2) + 
               t1*(-4 - 15*t2 + 3*Power(t2,2))) - 
            Power(t1,2)*(45 + Power(s,2)*(-3 + t2) + 76*t2 - 
               4*Power(t2,2) + 8*Power(t2,3) + 
               s*(-16 - 27*t2 + 6*Power(t2,2))) + 
            t1*(-15 - 46*t2 + 94*Power(t2,2) + 34*Power(t2,3) + 
               7*Power(t2,4) + Power(s,2)*(-2 + 3*t2 - 4*Power(t2,2)) + 
               s*(-13 - 29*t2 + 21*Power(t2,2) + 4*Power(t2,3))) + 
            s1*(-2*Power(t1,3)*(3 + 6*s + 5*t2) + 
               t1*(78 + 4*(52 + 7*s)*t2 + (-38 + s)*Power(t2,2) - 
                  32*Power(t2,3)) + 
               Power(t1,2)*(52 + 76*t2 + 27*Power(t2,2) + 
                  2*s*(-14 + 5*t2)) + 
               t2*(30 - 46*t2 - 15*Power(t2,2) + 8*Power(t2,3) + 
                  s*(2 + 5*t2)))) + 
         s2*Power(t1,2)*(-3 + t2 + Power(t2,2) + Power(t2,3) - 
            2*Power(t1,3)*(15 + Power(s1,2) + 7*t2 + s1*(-7 + t2)*t2 - 
               2*Power(t2,2)) + 
            Power(t1,4)*(25 - 6*t2 + s1*(-11 + 2*t2)) + 
            t1*(-45 + 7*t2 + 33*Power(t2,2) - 3*Power(t2,3) - 
               5*s1*(-1 + Power(t2,2))) + 
            Power(t1,2)*(-109 + 2*Power(s1,2)*(-1 + t2) + 45*t2 - 
               16*Power(t2,2) + 2*Power(t2,3) - 
               s1*(-17 + 6*t2 + Power(t2,2))) + 
            Power(s,2)*(8 + Power(t1,2)*(-2 + t2) - 10*t2 + 
               Power(t2,2) + Power(t2,3) + Power(t1,3)*(-7 + 2*t2) + 
               t1*(16 - 17*t2 + 9*Power(t2,2) - 2*Power(t2,3))) + 
            s*(-2*Power(-1 + t2,2)*(4 + t2) + Power(t1,4)*(-7 + 2*t2) + 
               Power(t1,3)*(46 - 11*t2 + s1*(-9 + 2*t2)) + 
               t1*(-1 + t2)*(-11 - 23*t2 + 5*Power(t2,2) + 
                  s1*(5 + 3*t2)) + 
               Power(t1,2)*(85 - 50*t2 + 17*Power(t2,2) - 
                  2*Power(t2,3) + s1*(-15 + 8*t2 - 2*Power(t2,2))))) + 
         Power(s2,4)*(-2 + (9 + 34*s + 20*Power(s,2) + 3*s1 - s*s1)*t2 - 
            (-28 + 24*Power(s,2) + 8*s1 + s*(19 + s1))*Power(t2,2) + 
            (5*Power(s,2) + 2*s*(-10 + s1) - 3*(7 + s1))*Power(t2,3) + 
            (-12 + 5*s + 8*s1)*Power(t2,4) + 
            Power(t1,5)*(2*Power(s,2) + 2*Power(s1,2) + 2*(3 + t2) - 
               3*s1*(3 + t2) + s*(2 - 4*s1 + t2)) + 
            t1*(60 + 262*t2 - 96*Power(t2,2) + 22*Power(t2,3) - 
               56*Power(t2,4) + 
               Power(s1,2)*(2 + 4*t2 - 6*Power(t2,2)) + 
               Power(s,2)*(20 - 18*t2 - 19*Power(t2,2) + 
                  8*Power(t2,3)) + 
               s*(34 - 7*t2 + (71 + 15*s1)*Power(t2,2) - 
                  (71 + 6*s1)*Power(t2,3) + 7*Power(t2,4)) + 
               s1*(-21 - 5*t2 - 42*Power(t2,2) + 12*Power(t2,3) + 
                  16*Power(t2,4))) + 
            Power(t1,4)*(-68 + Power(s,2)*(19 - 5*t2) - 25*t2 - 
               2*Power(t2,3) - 2*Power(s1,2)*(-7 + 6*t2) + 
               s1*(41 + 15*t2 + 19*Power(t2,2)) + 
               s*(13 + 13*t2 - 6*Power(t2,2) + s1*(-44 + 15*t2))) - 
            Power(t1,3)*(Power(s,2)*(-19 + 5*t2 + 4*Power(t2,2)) + 
               s*(182 + 53*t2 - 34*Power(t2,2) + 5*Power(t2,3) + 
                  s1*(-13 - 42*t2 + 5*Power(t2,2))) - 
               2*(-41 - 41*t2 + 49*Power(t2,2) + 6*Power(t2,3) + 
                  Power(t2,4) + 
                  3*Power(s1,2)*(-11 - 4*t2 + 3*Power(t2,2)) + 
                  s1*(220 + 93*t2 - 48*Power(t2,2) - 9*Power(t2,3)))) + 
            Power(t1,2)*(312 + 455*t2 + 101*Power(t2,2) + 
               3*Power(t2,3) - 
               4*Power(s1,2)*(4 - 9*t2 + 3*Power(t2,2)) + 
               Power(s,2)*(6 - 23*t2 - 4*Power(t2,2) + 3*Power(t2,3)) + 
               s1*(54 - 93*t2 - 197*Power(t2,2) + 35*Power(t2,3)) + 
               s*(-123 - 164*t2 + 118*Power(t2,2) - 28*Power(t2,3) + 
                  6*Power(t2,4) + 
                  s1*(49 + 38*t2 + 15*Power(t2,2) - 8*Power(t2,3))))) + 
         Power(s2,2)*t1*(-3 + 18*t2 - 20*Power(t2,2) + 4*Power(t2,3) + 
            Power(t2,4) + Power(t1,5)*(-5 - 2*t2 + s1*(-9 + 6*t2)) + 
            2*Power(t1,4)*(-56 + 9*t2 + 4*Power(t2,2) + 
               Power(s1,2)*(-7 + 3*t2) - 4*s1*(-14 + 2*t2 + Power(t2,2))\
) - 2*Power(t1,2)*(-157 + 35*t2 + 9*Power(t2,2) - 4*Power(t2,3) + 
               Power(t2,4) + Power(s1,2)*(-3 + 2*t2 + Power(t2,2)) + 
               s1*(33 - 14*t2 + 4*Power(t2,2) - 3*Power(t2,3))) + 
            Power(t1,3)*(235 + 61*t2 + 9*Power(t2,2) - 4*Power(t2,3) + 
               Power(s1,2)*(2 + 8*t2 - 2*Power(t2,2)) + 
               s1*(7 - 95*t2 + 5*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s,2)*(6 + Power(t1,3)*(18 - 7*t2) + 
               Power(t1,4)*(13 - 4*t2) - 17*t2 + 15*Power(t2,2) - 
               4*Power(t2,3) + t1*(-13 + 4*t2 + Power(t2,3)) + 
               Power(t1,2)*(-27 + 30*t2 - 19*Power(t2,2) + 
                  4*Power(t2,3))) + 
            t1*(45 + 55*t2 - 53*Power(t2,2) - 35*Power(t2,3) + 
               s1*(-2 - 13*t2 + 2*Power(t2,2) + 13*Power(t2,3))) - 
            s*(Power(t1,5)*(-7 + 2*t2) + 
               Power(-1 + t2,2)*(-11 - 13*t2 + Power(t2,2)) + 
               Power(t1,4)*(26 + s1 - 11*t2 - 2*s1*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(247 - 72*t2 + 15*Power(t2,2) - 
                  2*Power(t2,3) + s1*(-51 + 8*t2)) + 
               t1*(-1 + t2)*(27 - 30*t2 + 4*Power(t2,2) + 
                  s1*(3 + 5*t2 + 2*Power(t2,2))) + 
               Power(t1,2)*(135 - 17*t2 - 49*Power(t2,2) + 
                  15*Power(t2,3) - 2*Power(t2,4) + 
                  s1*(-35 + 16*t2 - 5*Power(t2,2) + 2*Power(t2,3))))) + 
         Power(s2,3)*(-((1 + s - 3*s1)*Power(t1,6)) - 
            (-1 + t2)*t2*(3 + 3*Power(s,2)*(-2 + t2) - 12*t2 + 
               7*Power(t2,2) + s*(-11 + 6*t2 + 5*Power(t2,2))) + 
            t1*(5 - 111*t2 + 63*Power(t2,2) + 11*Power(t2,3) + 
               24*Power(t2,4) + 
               s*(-1 + t2)*(38 + s1*(-1 + t2) + 22*t2 + 
                  8*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(-20 + 35*t2 - 15*Power(t2,2) + 
                  2*Power(t2,3)) + 
               s1*(-3 + 10*t2 + 11*Power(t2,2) - 10*Power(t2,3) - 
                  8*Power(t2,4))) + 
            Power(t1,5)*(Power(s,2)*(-9 + 2*t2) - 
               s*(14 + t2 - 2*Power(t2,2) + s1*(-13 + 4*t2)) + 
               2*(17 + Power(s1,2)*(-1 + t2) + 2*Power(t2,2) + 
                  s1*(4 - 5*t2 - 3*Power(t2,2)))) + 
            Power(t1,4)*(147 + 39*t2 - 49*Power(t2,2) - 4*Power(t2,3) + 
               Power(s,2)*(-30 + 11*t2) + 
               Power(s1,2)*(50 - 6*t2 - 6*Power(t2,2)) + 
               s1*(-331 - 19*t2 + 41*Power(t2,2) + 6*Power(t2,3)) + 
               s*(157 - 40*t2 + 5*Power(t2,2) + 
                  s1*(-5 - 14*t2 + 2*Power(t2,2)))) - 
            Power(t1,3)*(460 + 222*t2 + 72*Power(t2,2) - 
               10*Power(t2,3) - 
               4*Power(s1,2)*(2 - 7*t2 + 2*Power(t2,2)) + 
               Power(s,2)*(-5 + 16*t2 - 15*Power(t2,2) + 
                  2*Power(t2,3)) + 
               4*s1*(9 - 42*t2 - 18*Power(t2,2) + 5*Power(t2,3)) + 
               s*(-321 - 47*t2 + 63*Power(t2,2) - 15*Power(t2,3) + 
                  2*Power(t2,4) + 
                  s1*(85 + 8*t2 + 3*Power(t2,2) - 2*Power(t2,3)))) + 
            Power(t1,2)*(-271 - 177*t2 + 113*Power(t2,2) + 
               21*Power(t2,3) + 6*Power(t2,4) + 
               6*Power(s1,2)*(-1 + Power(t2,2)) + 
               s1*(65 - 9*t2 + 35*Power(t2,2) - 31*Power(t2,3)) + 
               Power(s,2)*(2 + 25*t2 - 5*Power(t2,3)) + 
               s*(18 + 82*t2 - 98*Power(t2,2) + 30*Power(t2,3) - 
                  6*Power(t2,4) + 
                  s1*(-21 + 4*t2 - 15*Power(t2,2) + 10*Power(t2,3))))) - 
         Power(s2,5)*(Power(t1,4)*
             (14 + 4*Power(s,2) + 8*t2 + Power(t2,2) + 2*s*(1 + t2)) + 
            Power(t1,3)*(-73 + Power(s,2)*(13 - 5*t2) - 63*t2 + 
               Power(t2,2) - 5*Power(t2,3) + 
               s*(20 + 19*t2 - 5*Power(t2,2))) + 
            2*Power(s1,2)*(4*Power(t1,4) - (-1 + t2)*t2 - 
               4*Power(t1,3)*(-4 + 3*t2) + 
               t1*(-5 + 10*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(-19 - 24*t2 + 9*Power(t2,2))) + 
            t2*(62 - 17*t2 + 53*Power(t2,2) - 52*Power(t2,3) + 
               Power(s,2)*(20 - 20*t2 + Power(t2,2)) + 
               s*(30 + 39*t2 - 46*Power(t2,2) - 3*Power(t2,3))) - 
            Power(t1,2)*(43 + 36*t2 - 110*Power(t2,2) - 30*Power(t2,3) - 
               6*Power(t2,4) + Power(s,2)*(-6 + 5*t2 + 3*Power(t2,2)) + 
               s*(71 + 100*t2 - 44*Power(t2,2) + 4*Power(t2,3))) + 
            t1*(68 + 286*t2 + 126*Power(t2,2) + 40*Power(t2,3) - 
               24*Power(t2,4) + 
               Power(s,2)*(4 + 2*t2 - 23*Power(t2,2) + 6*Power(t2,3)) + 
               s*(-8 - 48*t2 + 95*Power(t2,2) - 45*Power(t2,3) + 
                  7*Power(t2,4))) + 
            s1*(-4 + (7 - 4*s)*t2 + (-16 + 3*s)*Power(t2,2) + 
               (-13 + 2*s)*Power(t2,3) + 16*Power(t2,4) - 
               Power(t1,4)*(10 + 12*s + 9*t2) + 
               Power(t1,3)*(81 + 52*t2 + 27*Power(t2,2) + 
                  s*(-54 + 20*t2)) + 
               Power(t1,2)*(284 + 311*t2 - 101*Power(t2,2) - 
                  31*Power(t2,3) + s*(7 + 54*t2 - 3*Power(t2,2))) + 
               t1*(s*(6 + 26*t2 + 17*Power(t2,2) - 6*Power(t2,3)) + 
                  2*(13 + 12*t2 - 84*Power(t2,2) + Power(t2,3) + 
                     4*Power(t2,4))))))*R1q(s2))/
     ((-1 + s1)*Power(-1 + s2,2)*s2*(-1 + t1)*Power(-s2 + t1,3)*
       (1 - s2 + t1 - t2)*(-1 + t2)*(t1 - s2*t2)) + 
    (8*(-2*Power(s2,7)*t1*t2*(s1*(-3 + 2*t1 + t2) + 
            (-1 + t2)*(-2 + 4*t1 + t2)) - 
         Power(s2,6)*(2*t2*(-1 + t2 + Power(t2,2)) + 
            2*Power(t1,3)*(4 + Power(s1,2) + 2*(5 + 2*s)*t2 - 
               15*Power(t2,2) - 2*s1*(1 + 5*t2)) + 
            Power(t1,2)*(-4 - 2*(5 + 3*s)*t2 + 
               (-19 + 2*s)*Power(t2,2) + 25*Power(t2,3) + 
               2*Power(s1,2)*(1 + t2) + 
               s1*(6 + (15 - 4*s)*t2 + 9*Power(t2,2))) + 
            t1*t2*(-2 + 23*t2 - 29*Power(t2,2) + 6*Power(t2,3) - 
               2*s*(-4 + 3*t2 + Power(t2,2)) + 
               s1*(5 - 17*t2 + 10*Power(t2,2)))) + 
         Power(s2,5)*(-2*t2*(1 - 4*t2 + 2*Power(t2,2) + 3*Power(t2,3)) - 
            t1*(2 + (5 - 4*s + s1)*t2 + 
               (-29 + 21*s + 8*s1)*Power(t2,2) + 
               (27 - 23*s - 15*s1)*Power(t2,3) + 
               (-55 - 6*s + 18*s1)*Power(t2,4) + 6*Power(t2,5)) + 
            Power(t1,2)*(-2 + 41*t2 + 8*Power(s,2)*(-2 + t2)*t2 + 
               15*Power(t2,2) + 23*Power(t2,3) - 31*Power(t2,4) - 
               2*Power(s1,2)*(1 + 2*Power(t2,2)) + 
               s*(8 + (17 + 4*s1)*t2 + 4*(1 + s1)*Power(t2,2) - 
                  3*Power(t2,3)) - 
               s1*(-9 - 5*t2 + 17*Power(t2,2) + Power(t2,3))) + 
            Power(t1,3)*(-16 - 94*t2 + 8*Power(s,2)*t2 - 
               26*Power(t2,2) + 82*Power(t2,3) + 
               2*Power(s1,2)*(7 + 2*t2) + 
               s1*(21 + 32*t2 + 51*Power(t2,2)) - 
               s*(6 - 9*t2 + 15*Power(t2,2) + 4*s1*(2 + 5*t2))) + 
            Power(t1,4)*(s*(8 - 4*s1 + 30*t2) + 
               2*(14 + 4*Power(s1,2) + t2 - 21*Power(t2,2) - 
                  2*s1*(5 + 11*t2)))) - 
         Power(s2,4)*(2*Power(t2,2)*
             (2 - 5*t2 + Power(t2,2) + 3*Power(t2,3)) + 
            t1*(-2 + (9 - 4*s)*t2 + 5*(5 + s)*Power(t2,2) + 
               (-45 + 22*s - s1)*Power(t2,3) - 
               (29*s + 3*s1)*Power(t2,4) + 
               (-47 - 6*s + 14*s1)*Power(t2,5) + 2*Power(t2,6)) + 
            2*Power(t1,5)*(16 + Power(s,2) + 6*Power(s1,2) - 17*t2 - 
               13*Power(t2,2) - 2*s1*(11 + 13*t2) + 
               s*(15 - 6*s1 + 22*t2)) - 
            Power(t1,2)*(5 - 85*t2 - 45*Power(t2,2) - 107*Power(t2,3) + 
               5*Power(t2,4) - 19*Power(t2,5) + 
               2*Power(s1,2)*t2*(1 + t2 - Power(t2,2)) + 
               2*Power(s,2)*t2*(2 - 14*t2 + 11*Power(t2,2)) + 
               s*(-4 + (6 - 16*s1)*t2 + (30 + 8*s1)*Power(t2,2) - 
                  (29 + 8*s1)*Power(t2,3) + 5*Power(t2,4)) + 
               s1*(5 + 8*t2 + 13*Power(t2,2) + 31*Power(t2,3) + 
                  9*Power(t2,4))) + 
            Power(t1,4)*(-77 + 32*Power(s1,2) - 263*t2 + 
               79*Power(t2,2) + 101*Power(t2,3) + 
               2*Power(s,2)*(5 + 12*t2) + 
               s1*(39 + 74*t2 + 87*Power(t2,2)) + 
               s*(3 + 23*t2 - 46*Power(t2,2) - 4*s1*(11 + 9*t2))) + 
            Power(t1,3)*(20 + 131*t2 + 135*Power(t2,2) + 
               37*Power(t2,3) - 75*Power(t2,4) - 
               2*Power(s,2)*(8 + 11*t2) - 
               2*Power(s1,2)*(8 + 3*t2 + 5*Power(t2,2)) + 
               s1*(46 + 48*t2 - 28*Power(t2,2) - 54*Power(t2,3)) + 
               s*(19 + 13*t2 - 19*Power(t2,2) + 21*Power(t2,3) + 
                  4*s1*(1 + 6*t2 + 5*Power(t2,2))))) + 
         Power(s2,2)*t1*((-1 + t2)*Power(t2,2)*
             (-2 - (3 + 4*s)*t2 + (4 + 3*s)*Power(t2,2) + 
               (7 + s)*Power(t2,3)) + 
            t1*(-8 - 2*(7 + 12*s)*t2 + 
               (33 + 52*s + 24*Power(s,2) - s1)*Power(t2,2) + 
               (129 - 46*Power(s,2) + 4*s*(-7 + s1) - 14*s1)*
                Power(t2,3) + 
               (-107 - 6*s + 12*Power(s,2) + s1)*Power(t2,4) + 
               (-43 + s + 6*Power(s,2) + 14*s1 - 4*s*s1)*Power(t2,5) + 
               (-2 + 5*s)*Power(t2,6)) - 
            2*Power(t1,6)*(-4 + Power(s,2) + Power(s1,2) - 5*t2 - 
               4*s1*(4 + t2) + s*(15 - 2*s1 + 4*t2)) + 
            Power(t1,2)*(-39 - 297*t2 + 189*Power(t2,2) + 
               382*Power(t2,3) + 57*Power(t2,4) - 25*Power(t2,5) + 
               Power(t2,6) - 
               2*Power(s,2)*(6 - 41*t2 - 5*Power(t2,2) + 
                  12*Power(t2,3) + 8*Power(t2,4)) + 
               s*(-26 + (87 - 24*s1)*t2 + (55 + 12*s1)*Power(t2,2) + 
                  2*(-65 + 2*s1)*Power(t2,3) + 
                  5*(-3 + 4*s1)*Power(t2,4) - 13*Power(t2,5)) + 
               2*s1*(2 + 41*t2 - 33*Power(t2,2) - 30*Power(t2,3) - 
                  18*Power(t2,4) + 8*Power(t2,5))) - 
            Power(t1,5)*(-289 - 2*Power(s1,2)*(-7 + t2) - 101*t2 + 
               138*Power(t2,2) + 12*Power(t2,3) + 
               2*Power(s,2)*(22 + 5*t2) + 
               s1*(27 + 71*t2 + 36*Power(t2,2)) - 
               s*(-37 + 17*t2 + 32*Power(t2,2) + s1*(68 + 8*t2))) + 
            Power(t1,4)*(-131 - 183*t2 - 258*Power(t2,2) + 
               56*Power(t2,3) + 38*Power(t2,4) + 
               4*Power(s1,2)*(20 - 3*t2 + Power(t2,2)) + 
               2*Power(s,2)*(6 - t2 + 16*Power(t2,2)) + 
               s1*(-212 - 53*t2 + 173*Power(t2,2) + 30*Power(t2,3)) - 
               2*s*(-93 - 93*t2 + 41*Power(t2,2) + 5*Power(t2,3) + 
                  s1*(26 + 6*t2 + 20*Power(t2,2)))) + 
            Power(t1,3)*(-339 - 471*t2 + 12*Power(s1,2)*(3 - 2*t2)*t2 + 
               78*Power(t2,2) + 14*Power(t2,3) + 49*Power(t2,4) - 
               17*Power(t2,5) + 
               2*Power(s,2)*(3 - 12*t2 - 11*Power(t2,2) + 
                  9*Power(t2,3)) + 
               s1*(101 + 8*t2 + 126*Power(t2,2) - 46*Power(t2,3) - 
                  33*Power(t2,4)) + 
               s*(97 + 462*t2 - 123*Power(t2,2) + 22*Power(t2,3) + 
                  22*Power(t2,4) - 
                  4*s1*(25 + 12*t2 - 10*Power(t2,2) + 3*Power(t2,3))))) + 
         Power(s2,3)*(-2*Power(t2,3)*(1 - 2*t2 + Power(t2,3)) + 
            t1*t2*(12 + 2*(-7 + 4*s)*t2 + 
               (-35 - 16*s + s1)*Power(t2,2) + 
               (15 - 7*s + 4*s1)*Power(t2,3) + 
               (15 + 13*s - s1)*Power(t2,4) + 
               (15 + 2*s - 4*s1)*Power(t2,5)) + 
            2*Power(t1,6)*(4 + 2*Power(s,2) + 4*Power(s1,2) - 17*t2 - 
               3*Power(t2,2) - 2*s1*(13 + 8*t2) + s*(22 - 6*s1 + 15*t2)) \
+ Power(t1,2)*(-1 + (84 - 8*s1)*t2 + (44 - 15*s1)*Power(t2,2) + 
               (-153 + 11*s1)*Power(t2,3) + 
               (-152 + 47*s1)*Power(t2,4) + (-9 + 5*s1)*Power(t2,5) - 
               5*Power(t2,6) + 
               2*Power(s,2)*t2*
                (6 - 18*t2 - 3*Power(t2,2) + 10*Power(t2,3)) + 
               s*(-4 + 27*t2 + (-21 + 4*s1)*Power(t2,2) + 
                  4*(2 + s1)*Power(t2,3) - (31 + 12*s1)*Power(t2,4) + 
                  11*Power(t2,5))) + 
            Power(t1,5)*(-212 - 4*Power(s1,2)*(-8 + t2) - 292*t2 + 
               186*Power(t2,2) + 58*Power(t2,3) + 
               Power(s,2)*(36 + 26*t2) + 
               s1*(47 + 98*t2 + 75*Power(t2,2)) + 
               s*(26 + 3*t2 - 53*Power(t2,2) - 28*s1*(3 + t2))) - 
            Power(t1,4)*(-95 - 237*t2 - 239*Power(t2,2) + 
               3*Power(t2,3) + 76*Power(t2,4) + 
               Power(s1,2)*(52 + 8*t2 + 8*Power(t2,2)) + 
               2*Power(s,2)*(14 + 9*t2 + 10*Power(t2,2)) + 
               s1*(-131 - 121*t2 + 135*Power(t2,2) + 69*Power(t2,3)) - 
               s*(-17 - 154*t2 + 29*Power(t2,2) + 30*Power(t2,3) + 
                  4*s1*(7 + 9*t2 + 10*Power(t2,2)))) + 
            Power(t1,3)*(70 + 383*t2 + 105*Power(t2,2) + 
               25*Power(t2,3) - 23*Power(t2,4) + 24*Power(t2,5) + 
               4*Power(s1,2)*t2*(-4 + t2 + Power(t2,2)) + 
               Power(s,2)*(-4 + 24*t2 + 12*Power(t2,2) - 
                  22*Power(t2,3)) + 
               s1*(-32 - 29*t2 - 67*Power(t2,2) - 51*Power(t2,3) + 
                  35*Power(t2,4)) + 
               s*(15 - 158*t2 - 77*Power(t2,2) + 30*Power(t2,3) - 
                  26*Power(t2,4) + 
                  4*s1*(5 + 17*t2 - 8*Power(t2,2) + 4*Power(t2,3))))) + 
         Power(t1,3)*(4*Power(t1,5)*
             (11 + s1 - 5*t2 - 2*s1*t2 + s*(-3 + 2*t2)) - 
            (-1 + t2)*Power(t2,2)*
             (9 + 6*Power(s,2)*(-2 + t2) - 12*t2 + Power(t2,2) + 
               s*(-2 - 3*t2 + 5*Power(t2,2))) + 
            t1*(-36 + 84*t2 - (23 + 9*s1)*Power(t2,2) + 
               (1 + 4*s1)*Power(t2,3) + (-43 + 9*s1)*Power(t2,4) + 
               (15 - 4*s1)*Power(t2,5) + 
               s*(8 + 4*t2 + (-49 + 4*s1)*Power(t2,2) + 
                  10*Power(t2,3) + (30 - 4*s1)*Power(t2,4) - 
                  3*Power(t2,5)) + 
               2*Power(s,2)*(24 - 36*t2 + 3*Power(t2,2) + 
                  10*Power(t2,3) - 4*Power(t2,4) + Power(t2,5))) - 
            Power(t1,3)*(160 - 28*t2 - 23*Power(t2,2) - 
               25*Power(t2,3) + 6*Power(t2,4) - 
               2*Power(s1,2)*t2*(5 - 5*t2 + Power(t2,2)) + 
               2*Power(s,2)*(4 + 3*t2 - 4*Power(t2,2) + Power(t2,3)) + 
               s1*(-44 + 14*t2 + Power(t2,2) + 11*Power(t2,3)) + 
               s*(-212 + 114*t2 - 17*Power(t2,2) - 5*Power(t2,3) + 
                  4*s1*(14 - 9*t2 + Power(t2,2)))) + 
            2*Power(t1,4)*(16 - 47*t2 + 6*Power(t2,2) + 3*Power(t2,3) + 
               Power(s1,2)*(8 - 5*t2 + Power(t2,2)) + 
               Power(s,2)*(-8 + 3*t2 + Power(t2,2)) + 
               2*s1*(-13 + 10*t2 + Power(t2,2)) - 
               s*(-38 + 8*t2 + Power(t2,2) + 
                  2*s1*(2 - t2 + Power(t2,2)))) + 
            Power(t1,2)*(-120 + 34*t2 + 203*Power(t2,2) - 
               65*Power(t2,3) - 10*Power(t2,4) - 
               2*Power(s,2)*(-36 + 40*t2 - 18*Power(t2,2) + 
                  3*Power(t2,3) + Power(t2,4)) + 
               s1*(36 - 14*t2 - 64*Power(t2,2) + 29*Power(t2,3) + 
                  3*Power(t2,4)) + 
               2*s*(34 + 21*t2 - 79*Power(t2,2) + 15*Power(t2,3) - 
                  4*Power(t2,4) + 
                  2*s1*(-4 + 7*Power(t2,2) - 3*Power(t2,3) + Power(t2,4))\
))) + s2*Power(t1,2)*((-4 + 8*s - 8*s1)*Power(t1,6) + 
            2*Power(t1,5)*(-93 + 9*Power(s,2) + Power(s1,2) + 18*t2 + 
               16*Power(t2,2) + 4*s1*t2*(4 + t2) - 
               2*s*(-8 + 5*s1 + 5*t2 + 2*Power(t2,2))) + 
            (-1 + t2)*Power(t2,2)*
             (5 + 7*t2 + 6*Power(s,2)*(-2 + t2)*t2 - 19*Power(t2,2) + 
               Power(t2,3) + s*(4 - 5*t2 - 4*Power(t2,2) + 5*Power(t2,3))\
) + t1*(20 + 26*t2 + 2*(-92 + 5*s1)*Power(t2,2) + 
               9*(7 + s1)*Power(t2,3) + (65 - 14*s1)*Power(t2,4) + 
               (31 - 9*s1)*Power(t2,5) + (-13 + 4*s1)*Power(t2,6) - 
               2*Power(s,2)*t2*
                (36 - 60*t2 + 14*Power(t2,2) + 5*Power(t2,3) + 
                  Power(t2,4)) - 
               s*(-1 + t2)*(16 - 24*t2 - (17 + 4*s1)*Power(t2,2) + 
                  (77 - 8*s1)*Power(t2,3) - 4*(-3 + s1)*Power(t2,4) + 
                  Power(t2,5))) - 
            2*Power(t1,4)*(-10 - 76*t2 - 57*Power(t2,2) + 
               25*Power(t2,3) + 3*Power(t2,4) + 
               Power(s1,2)*(29 - 12*t2 + 2*Power(t2,2)) + 
               Power(s,2)*(-11 + 2*t2 + 8*Power(t2,2)) + 
               s1*(-85 + 30*t2 + 35*Power(t2,2) + 2*Power(t2,3)) - 
               s*(-117 - 6*t2 + 13*Power(t2,2) + Power(t2,3) + 
                  2*s1*(9 - 2*t2 + 5*Power(t2,2)))) + 
            Power(t1,3)*(450 + 40*t2 + 4*Power(t2,2) - 96*Power(t2,3) - 
               8*Power(t2,4) + 6*Power(t2,5) - 
               4*Power(s1,2)*t2*(8 - 7*t2 + Power(t2,2)) + 
               2*Power(s,2)*(-12 + 40*t2 - 18*Power(t2,2) + 
                  Power(t2,3)) + 
               s1*(-118 + 28*t2 - 63*Power(t2,2) + 62*Power(t2,3) + 
                  7*Power(t2,4)) + 
               s*(-326 - 188*t2 + 174*Power(t2,2) - 63*Power(t2,3) - 
                  Power(t2,4) + 
                  4*s1*(34 - 10*t2 - 3*Power(t2,2) + Power(t2,3)))) + 
            Power(t1,2)*(164 + 166*t2 - 389*Power(t2,2) - 
               205*Power(t2,3) + 81*Power(t2,4) + 11*Power(t2,5) + 
               2*Power(s,2)*(-24 - 34*t2 + 40*Power(t2,2) - 
                  15*Power(t2,3) + 8*Power(t2,4)) - 
               s1*(40 + 60*t2 - 145*Power(t2,2) - 19*Power(t2,3) + 
                  17*Power(t2,4) + 7*Power(t2,5)) + 
               s*(-20 - 208*t2 + 153*Power(t2,2) + 150*Power(t2,3) - 
                  17*Power(t2,4) + 8*Power(t2,5) - 
                  4*s1*(-4 - 6*t2 + 11*Power(t2,2) - Power(t2,3) + 
                     3*Power(t2,4))))))*R1q(t1))/
     ((-1 + s1)*(-1 + t1)*t1*Power(-s2 + t1,3)*(1 - s2 + t1 - t2)*(-1 + t2)*
       (t1 - s2*t2)*(-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) - 
    (8*(4*Power(t1,4)*(14 + 2*s*(-2 + t2) - 2*s1*(-1 + t2) - 5*t2) - 
         3*Power(s2,5)*t2*(-2 + s1 + t2) + 
         2*t2*Power(1 + (-1 + s)*t2,2)*(1 - 3*t2 + Power(t2,2)) - 
         t1*(8 + 24*t2 - 89*Power(t2,2) - 2*Power(s1,2)*Power(t2,2) + 
            34*Power(t2,3) + 7*Power(t2,4) + 
            Power(s,2)*t2*(12 - 22*t2 + Power(t2,2) + 2*Power(t2,3)) - 
            2*s1*(-2 + 18*t2 - 42*Power(t2,2) + 16*Power(t2,3) + 
               Power(t2,4)) + 
            s*(20 - 4*(9 + s1)*t2 + (51 - 12*s1)*Power(t2,2) + 
               16*(-1 + s1)*Power(t2,3) + (3 - 4*s1)*Power(t2,4))) - 
         Power(s2,4)*(2*Power(s1,2)*(1 + t1 + t2) + 
            t1*(6 + 14*t2 + 3*s*t2 - 9*Power(t2,2)) + 
            t2*(-5 + 6*s - 24*t2 + 11*Power(t2,2)) + 
            s1*(-(t1*(3 + 8*t2)) + t2*(9 - 3*s + 11*t2))) - 
         Power(t1,2)*(-2*Power(s1,2)*
             (-14 + 11*t2 - 6*Power(t2,2) + Power(t2,3)) + 
            Power(s,2)*(-12 + 18*t2 - 7*Power(t2,2) + 2*Power(t2,3)) + 
            s1*(-192 + 86*t2 + 3*Power(t2,2) + 11*Power(t2,3)) + 
            2*(124 - 65*t2 - 7*Power(t2,2) - 12*Power(t2,3) + 
               3*Power(t2,4)) + 
            s*(-92 + 50*t2 + 8*Power(t2,2) - 6*Power(t2,3) + 
               s1*(32 - 32*t2 + Power(t2,2)))) + 
         Power(t1,3)*(8 - 88*t2 + 7*Power(t2,2) + 6*Power(t2,3) + 
            2*Power(s1,2)*(14 - 7*t2 + Power(t2,2)) + 
            2*Power(s,2)*(-4 + t2 + Power(t2,2)) + 
            s1*(-20 + 30*t2 + 7*Power(t2,2)) - 
            s*(-88 + 10*t2 + 5*Power(t2,2) + 
               4*s1*(7 - 3*t2 + Power(t2,2)))) + 
         Power(s2,3)*(2*Power(s1,2)*
             (-1 + 2*Power(t1,2) - t1*(-5 + t2) - 3*t2 - 2*Power(t2,2)) \
+ Power(t1,2)*(17 - 4*t2 - 6*Power(t2,2) + s*(3 + 9*t2)) + 
            t1*(-5 - 60*t2 + 3*Power(s,2)*t2 - 27*Power(t2,2) + 
               24*Power(t2,3) + s*(6 + 14*t2 - 8*Power(t2,2))) + 
            t2*(-29 + 3*Power(s,2)*(-2 + t2) + 46*t2 + 39*Power(t2,2) - 
               13*Power(t2,3) + s*(17 - 20*t2 + 3*Power(t2,2))) + 
            s1*(4 + (54 + 4*s)*t2 + 6*(-5 + s)*Power(t2,2) - 
               19*Power(t2,3) - Power(t1,2)*(8 + 4*s + 11*t2) + 
               t1*(13 + 24*t2 + 23*Power(t2,2) - 7*s*(1 + t2)))) - 
         Power(s2,2)*(2 + 40*t2 + 16*s*t2 - 8*Power(s,2)*t2 + 
            98*Power(t2,2) - 40*s*Power(t2,2) + 
            18*Power(s,2)*Power(t2,2) - 63*Power(t2,3) + 
            26*s*Power(t2,3) - 8*Power(s,2)*Power(t2,3) - 
            30*Power(t2,4) - 6*s*Power(t2,4) + 5*Power(t2,5) + 
            Power(t1,3)*(5 + 2*Power(s,2) - 10*t2 + s*(9 + 8*t2)) + 
            2*Power(s1,2)*(Power(t1,3) + Power(t1,2)*(4 - 3*t2) + 
               t2*(1 + 3*t2 + Power(t2,2)) - t1*(9 + 7*t2 + Power(t2,2))\
) - t1*(27 - 88*t2 - 111*Power(t2,2) - 24*Power(t2,3) + 
               21*Power(t2,4) + Power(s,2)*(6 + 5*t2 + 4*Power(t2,2)) + 
               s*(-13 + 82*t2 + 15*Power(t2,2) - 7*Power(t2,3))) + 
            Power(t1,2)*(Power(s,2)*(5 + 6*t2) + 
               s*(10 + 6*t2 - 14*Power(t2,2)) + 
               2*(-18 - 71*t2 + 21*Power(t2,2) + 6*Power(t2,3))) + 
            s1*(-4 + 8*(1 + s)*t2 - 2*(53 + 4*s)*Power(t2,2) - 
               3*(-11 + s)*Power(t2,3) + 17*Power(t2,4) - 
               Power(t1,3)*(11 + 4*s + 8*t2) + 
               Power(t1,2)*(21 - 23*s + 29*t2 + 18*Power(t2,2)) + 
               t1*(70 - 32*t2 - 43*Power(t2,2) - 22*Power(t2,3) + 
                  2*s*(2 + 13*t2 + 5*Power(t2,2))))) + 
         s2*(-2 + (-4 + 8*s - 8*s1)*Power(t1,4) + 4*(5 + 4*s)*t2 + 
            2*(5*Power(s,2) - 8*s1 - 2*s*(5 + 2*s1))*Power(t2,2) + 
            (-67 - 18*Power(s,2) + 52*s1 - 2*Power(s1,2) + 
               s*(39 + 4*s1))*Power(t2,3) + 
            (24 - 16*s + 7*Power(s,2) - 12*s1)*Power(t2,4) + 
            3*(3 + s - 2*s1)*Power(t2,5) + 
            2*Power(t1,3)*(-52 + 7*Power(s,2) - Power(s1,2) - 19*t2 + 
               16*Power(t2,2) + s*(5 - 6*s1 + t2 - 4*Power(t2,2)) + 
               s1*(5 + 5*t2 + 4*Power(t2,2))) + 
            Power(t1,2)*(46 + 62*t2 + 125*Power(t2,2) - 28*Power(t2,3) - 
               6*Power(t2,4) + Power(s,2)*(-6 + 2*t2 - 8*Power(t2,2)) + 
               Power(s1,2)*(-34 - 8*t2 + 4*Power(t2,2)) - 
               s1*(-18 + 12*t2 + 52*Power(t2,2) + 7*Power(t2,3)) + 
               s*(-78 - 94*t2 + 17*Power(t2,2) + 5*Power(t2,3) + 
                  2*s1*(14 + 9*t2 + 2*Power(t2,2)))) + 
            t1*(48 + 336*t2 - 189*Power(t2,2) - 44*Power(t2,3) - 
               19*Power(t2,4) + 6*Power(t2,5) - 
               Power(s,2)*(8 - 8*t2 + Power(t2,3)) + 
               2*Power(s1,2)*(2 + 12*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               s1*(-16 - 250*t2 + 75*Power(t2,2) + 40*Power(t2,3) + 
                  7*Power(t2,4)) + 
               s*(s1*(12 + 8*t2 - 23*Power(t2,2) + Power(t2,3)) - 
                  2*(-8 + 70*t2 - 48*Power(t2,2) + 7*Power(t2,3) + 
                     Power(t2,4))))))*R2q(1 - s2 + t1 - t2))/
     ((-1 + s1)*(-1 + t1)*(1 - s2 + t1 - t2)*(-1 + t2)*(t1 - s2*t2)*
       (-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) + 
    (8*(-8*Power(t1,6) - Power(t2,3)*Power(1 + (-1 + s)*t2,2)*
          (1 - 3*t2 + Power(t2,2)) + 
         t1*t2*(6 + 6*(-5 + 2*s)*t2 + 
            2*(29 + 3*Power(s,2) + s*(-24 + s1) - 2*s1)*Power(t2,2) + 
            (-47 - 16*Power(s,2) + s*(58 - 10*s1) + 11*s1)*Power(t2,3) + 
            (16 + 4*Power(s,2) - 10*s1 + 3*s*(-4 + 3*s1))*Power(t2,4) + 
            (-2 + Power(s,2) + 3*s1 - 3*s*s1)*Power(t2,5) + 
            (-1 + s)*Power(t2,6)) + 
         Power(s2,7)*(Power(s1,2)*(1 + t1) + Power(t2,2)*(-1 + 2*t2) + 
            s1*t2*(4 - t1 + 2*t2)) + 
         2*Power(t1,5)*(-2 + 2*Power(s,2)*(-3 + t2) + 
            2*Power(s1,2)*(-3 + t2) + 19*t2 - 6*Power(t2,2) + 
            s1*(8 - 7*t2 - 2*Power(t2,2)) - 
            s*(4 + 4*s1*(-3 + t2) + t2 - 2*Power(t2,2))) + 
         Power(t1,2)*(-4 - 88*t2 + 158*Power(t2,2) - 61*Power(t2,3) + 
            17*Power(t2,4) - 4*Power(t2,5) + 2*Power(t2,6) + 
            Power(s1,2)*Power(t2,2)*
             (-4 + t2 + 6*Power(t2,2) - 2*Power(t2,3)) + 
            Power(s,2)*t2*(-12 + 6*t2 + 11*Power(t2,2) - 
               11*Power(t2,3) + Power(t2,4)) + 
            s*(-20 + (34 - 4*s1)*t2 + (-98 + 62*s1)*Power(t2,2) + 
               (22 - 68*s1)*Power(t2,3) + (8 + 23*s1)*Power(t2,4) - 
               10*Power(t2,5) - 2*Power(t2,6)) + 
            s1*(-4 + 50*t2 - 76*Power(t2,2) + 46*Power(t2,3) - 
               32*Power(t2,4) + 6*Power(t2,5) + Power(t2,6))) + 
         2*Power(t1,4)*(-2 - 20*t2 - 21*Power(t2,2) + 5*Power(t2,3) + 
            2*Power(t2,4) + Power(s,2)*(-8 - 2*t2 + 3*Power(t2,2)) + 
            Power(s1,2)*(28 - 20*t2 + 5*Power(t2,2)) + 
            s1*(-74 + 37*t2 - 3*Power(t2,2) + 5*Power(t2,3)) + 
            s*(38 + 5*t2 - 4*Power(t2,2) - 3*Power(t2,3) - 
               2*s1*(2 - 7*t2 + 4*Power(t2,2)))) - 
         Power(t1,3)*(172 - 52*t2 + 34*Power(t2,2) - 38*Power(t2,3) - 
            3*Power(t2,4) + 5*Power(t2,5) + 
            2*Power(s1,2)*(2 - 4*t2 + 16*Power(t2,2) - 7*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s,2)*(-36 + 32*t2 - 34*Power(t2,2) + 6*Power(t2,3) + 
               Power(t2,4)) + 
            s1*(-56 + 54*t2 - 138*Power(t2,2) + 46*Power(t2,3) + 
               3*Power(t2,4) + Power(t2,5)) - 
            s*(64 - 2*t2 - 74*Power(t2,2) + 26*Power(t2,3) + 
               10*Power(t2,4) + Power(t2,5) + 
               s1*(-64 + 104*t2 - 50*Power(t2,2) + 3*Power(t2,4)))) + 
         Power(s2,6)*(Power(s1,2)*
             (2 - 2*Power(t1,2) + 5*t2 + Power(t2,2) + t1*(-3 + 4*t2)) + 
            s1*(-2 - 13*t2 - (-22 + s)*Power(t2,2) + 10*Power(t2,3) + 
               Power(t1,2)*(1 + 2*s + t2) + 
               t1*(-6 - 13*t2 - 9*Power(t2,2) + s*(2 + t2))) + 
            t2*(-4 - 7*t2 - 15*Power(t2,2) + 9*Power(t2,3) + 
               t1*(3 + 5*t2 - 5*Power(t2,2)) + 
               s*(-2 + Power(t1,2) - 4*t2 - Power(t2,2) + 2*t1*(2 + t2)))\
) + Power(s2,5)*(1 + (-1 + s)*s*Power(t1,3) + 13*t2 - 2*s*t2 - 
            6*Power(t2,2) - 2*s*Power(t2,2) + 5*Power(s,2)*Power(t2,2) - 
            27*Power(t2,3) - 6*s*Power(t2,3) - Power(s,2)*Power(t2,3) - 
            44*Power(t2,4) - 5*s*Power(t2,4) + 16*Power(t2,5) + 
            t1*(5 + (23 - 4*s - 2*Power(s,2))*t2 + 
               (35 + 5*s - 3*Power(s,2))*Power(t2,2) + 
               2*(7 + 3*s)*Power(t2,3) - 20*Power(t2,4)) - 
            Power(t1,2)*(2 + Power(s,2)*(-1 + t2) + 17*t2 - 
               4*Power(t2,3) + s*(6 + 12*t2 + Power(t2,2))) + 
            Power(s1,2)*(1 + Power(t1,3) + 5*t2 + 13*Power(t2,2) + 
               4*Power(t2,3) - Power(t1,2)*(3 + 8*t2) + 
               t1*(-17 - 11*t2 + 3*Power(t2,2))) - 
            s1*(4 + (1 + 2*s)*Power(t1,3) + 9*t2 + 
               (66 + 7*s)*Power(t2,2) + (-43 + 3*s)*Power(t2,3) - 
               20*Power(t2,4) - 
               Power(t1,2)*(15 + 26*t2 + 9*Power(t2,2) + 
                  s*(-7 + 9*t2)) + 
               t1*(-19 + 44*t2 + 56*Power(t2,2) + 23*Power(t2,3) - 
                  2*s*(1 + 5*t2 + 5*Power(t2,2))))) + 
         Power(s2,4)*(2 + 4*t2 + 2*s*t2 + 36*Power(t2,2) - 
            2*Power(s,2)*Power(t2,2) + 17*Power(t2,3) - 
            2*s*Power(t2,3) + 16*Power(s,2)*Power(t2,3) - 
            41*Power(t2,4) + 8*s*Power(t2,4) - 
            4*Power(s,2)*Power(t2,4) - 51*Power(t2,5) - 
            10*s*Power(t2,5) + 14*Power(t2,6) + 
            t1*(-16 + (8 + 22*s - 12*Power(s,2))*t2 + 
               (125 - 14*s - 4*Power(s,2))*Power(t2,2) + 
               (153 + s - 8*Power(s,2))*Power(t2,3) + 
               (13 + 8*s)*Power(t2,4) - 30*Power(t2,5)) + 
            Power(t1,3)*(10 + 15*t2 - 8*Power(t2,2) + 
               Power(s,2)*(-2 + 4*t2) + s*(12 + t2 + 4*Power(t2,2))) + 
            Power(t1,2)*(-18 - 33*t2 - 100*Power(t2,2) + 
               32*Power(t2,3) + 12*Power(t2,4) + 
               2*Power(s,2)*(1 + 5*t2) - 
               2*s*(-7 + 13*t2 + 12*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,2)*(Power(t1,3)*(11 + 4*t2) - 
               Power(t1,2)*(-33 + t2 + 10*Power(t2,2)) - 
               t1*(16 + 54*t2 + 19*Power(t2,2) + 4*Power(t2,3)) + 
               t2*(3 + t2 + 19*Power(t2,2) + 6*Power(t2,3))) - 
            s1*(2*(1 + 3*t2 + (9 - 2*s)*Power(t2,2) + 
                  (62 + 11*s)*Power(t2,3) + (-18 + s)*Power(t2,4) - 
                  10*Power(t2,5)) + 
               Power(t1,3)*(19 + 21*t2 + 4*Power(t2,2) + s*(9 + 8*t2)) - 
               Power(t1,2)*(20 + 116*t2 + 77*Power(t2,2) + 
                  20*Power(t2,3) + s*(-25 - 38*t2 + 10*Power(t2,2))) + 
               t1*(-33 - 178*t2 + 93*Power(t2,2) + 96*Power(t2,3) + 
                  26*Power(t2,4) - 
                  s*(-2 + 19*t2 + 19*Power(t2,2) + 22*Power(t2,3))))) + 
         Power(s2,2)*(4*(s - s1)*Power(t1,5) + 
            t2*(3 + (-25 - 14*s + 2*s1)*t2 - 
               (8 + 9*Power(s,2) - 12*s1 + Power(s1,2) - 
                  4*s*(1 + 2*s1))*Power(t2,2) + 
               (37 + 3*Power(s,2) + 8*s*(-3 + s1) + 8*s1 - 
                  3*Power(s1,2))*Power(t2,3) + 
               (22 + 5*Power(s,2) - 31*s1 + 4*Power(s1,2) - 
                  10*s*(1 + s1))*Power(t2,4) + 
               (-8 - 4*Power(s,2) - 2*s1 + Power(s1,2) + 3*s*(4 + s1))*
                Power(t2,5) + (-2 - 5*s + 2*s1)*Power(t2,6) + 
               Power(t2,7)) - 
            2*Power(t1,4)*(24 + 4*Power(s,2)*(-2 + t2) + 58*t2 - 
               14*Power(t2,2) + Power(s1,2)*(2 + 4*t2) + 
               s*(24 + s1*(6 - 8*t2) + t2 + 8*Power(t2,2)) - 
               s1*(29 + 23*t2 + 8*Power(t2,2))) - 
            t1*(12 + (86 + 24*s - 16*Power(s,2))*t2 - 
               3*(-67 + 18*s + 10*Power(s,2))*Power(t2,2) + 
               2*(71 - 6*s + 26*Power(s,2))*Power(t2,3) - 
               3*(23 - 22*s + 5*Power(s,2))*Power(t2,4) - 
               (77 + 26*s)*Power(t2,5) - 6*(-1 + s)*Power(t2,6) + 
               5*Power(t2,7) + 
               2*Power(s1,2)*t2*
                (1 - 7*t2 + 23*Power(t2,2) + 5*Power(t2,3)) + 
               s1*(-12 + 2*(6 + 11*s)*t2 + 4*(5 + 12*s)*Power(t2,2) - 
                  14*(23 + 6*s)*Power(t2,3) + 2*(4 + 7*s)*Power(t2,4) - 
                  (-31 + s)*Power(t2,5) + 5*Power(t2,6))) - 
            Power(t1,3)*(-100 - 106*t2 - 247*Power(t2,2) + 
               16*Power(t2,3) + 36*Power(t2,4) + 
               Power(s1,2)*(104 + 32*t2 - 9*Power(t2,2)) + 
               Power(s,2)*(14 + 44*t2 + 11*Power(t2,2)) + 
               2*s1*(-37 + 51*t2 + 29*Power(t2,2) + 24*Power(t2,3) + 
                  2*Power(t2,4)) - 
               2*s*(-45 - 45*t2 - 11*Power(t2,2) + 10*Power(t2,3) + 
                  2*Power(t2,4) + s1*(43 + 58*t2 + Power(t2,2)))) + 
            Power(t1,2)*(96 + 181*t2 - 205*Power(t2,2) - 
               279*Power(t2,3) - 88*Power(t2,4) + 50*Power(t2,5) + 
               4*Power(t2,6) + 
               Power(s1,2)*(34 + 95*t2 + 25*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(s,2)*(-4 + 45*t2 - 7*Power(t2,2) + 
                  19*Power(t2,3) + 4*Power(t2,4)) + 
               s1*(-136 - 538*t2 + 4*Power(t2,2) + 116*Power(t2,3) + 
                  37*Power(t2,4) + 7*Power(t2,5)) + 
               s*(2 - 14*t2 + 106*Power(t2,2) + 50*Power(t2,3) - 
                  22*Power(t2,4) - 5*Power(t2,5) - 
                  2*s1*(-9 + 26*t2 - Power(t2,2) + 25*Power(t2,3) + 
                     2*Power(t2,4))))) + 
         Power(s2,3)*(1 + (1 + 2*s - 6*s1)*t2 + 
            (-7*Power(s,2) + Power(6 + s1,2) + s*(4 + 8*s1))*
             Power(t2,2) - (-59 + 3*Power(s,2) + s*(2 - 10*s1) + 
               8*s1 + 5*Power(s1,2))*Power(t2,3) + 
            (17*Power(s,2) - 2*s*(5 + 12*s1) + 
               2*(22 - 51*s1 + 7*Power(s1,2)))*Power(t2,4) + 
            (-29 - 6*Power(s,2) + 10*s1 + 4*Power(s1,2) + 
               2*s*(10 + s1))*Power(t2,5) - 
            2*(12 + 5*s - 5*s1)*Power(t2,6) + 6*Power(t2,7) - 
            2*Power(t1,4)*(5 + 3*Power(s,2) - 6*s1 - 6*s*s1 + 
               3*Power(s1,2) - 2*t2 + 4*s*t2 - 4*s1*t2) + 
            Power(t1,3)*(14 + 125*t2 + 50*Power(t2,2) - 
               36*Power(t2,3) + 
               Power(s,2)*(-12 - 7*t2 + 4*Power(t2,2)) + 
               Power(s1,2)*(-20 + 19*t2 + 4*Power(t2,2)) - 
               4*s1*(19 + 27*t2 + 13*Power(t2,2) + 2*Power(t2,3)) + 
               2*s*(11 + 31*t2 + 6*Power(t2,2) + 4*Power(t2,3) + 
                  s1*(28 - 6*t2 - 4*Power(t2,2)))) + 
            Power(t1,2)*(-1 - 195*t2 - 185*Power(t2,2) - 
               178*Power(t2,3) + 72*Power(t2,4) + 12*Power(t2,5) + 
               Power(s1,2)*(79 + 63*t2 + 15*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(s,2)*(5 + 25*t2 + 27*Power(t2,2) + 
                  4*Power(t2,3)) + 
               2*s1*(-79 + 6*t2 + 77*Power(t2,2) + 40*Power(t2,3) + 
                  9*Power(t2,4)) - 
               2*s*(2 - 41*t2 - 27*Power(t2,2) + 9*Power(t2,3) + 
                  5*Power(t2,4) + s1*(6 + 36*t2 + 38*Power(t2,2)))) - 
            t1*(16 + (109 + 2*s - 6*Power(s,2))*t2 + 
               (96 - 4*s + 46*Power(s,2))*Power(t2,2) + 
               (-161 + 60*s - 5*Power(s,2))*Power(t2,3) + 
               2*(-96 - 7*s + 3*Power(s,2))*Power(t2,4) - 
               (1 + 8*s)*Power(t2,5) + 20*Power(t2,6) + 
               Power(s1,2)*(6 + 14*t2 + 70*Power(t2,2) + 
                  19*Power(t2,3) + 5*Power(t2,4)) + 
               s1*(-28 - 78*t2 - 420*Power(t2,2) + 70*Power(t2,3) + 
                  80*Power(t2,4) + 15*Power(t2,5) - 
                  2*s*(-1 - 4*t2 + 31*Power(t2,2) + 4*Power(t2,3) + 
                     8*Power(t2,4))))) + 
         s2*(-(Power(t2,2)*(3 + (-13 + 6*s - 2*s1)*t2 + 
                 (25 - 22*s + 3*Power(s,2) + 6*s1)*Power(t2,2) - 
                 (18 + 7*Power(s,2) + 2*s*(-14 + s1) + 5*s1)*
                  Power(t2,3) + (4 + 2*Power(s,2) + s*s1)*Power(t2,4) + 
                 (Power(s,2) + s1 - s*(2 + s1))*Power(t2,5) + 
                 (-1 + s)*Power(t2,6))) + 
            t1*(-6 + (50 + 28*s - 4*s1)*t2 + 
               2*(22 + 9*Power(s,2) - 14*s1 - s*(4 + 5*s1))*
                Power(t2,2) + 
               (-135 + s*(82 - 44*s1) + 2*s1 + 12*Power(s1,2))*
                Power(t2,3) + 
               (5 - 14*Power(s,2) + 43*s1 - 13*Power(s1,2) + 
                  s*(2 + 48*s1))*Power(t2,4) + 
               (9*Power(s,2) - 2*(-5 + s1)*s1 - 2*s*(8 + 7*s1))*
                Power(t2,5) + 
               (5 + Power(s,2) + s*(13 - 2*s1) - 4*s1 + Power(s1,2))*
                Power(t2,6) + (-3 + 2*s - s1)*Power(t2,7)) + 
            2*Power(t1,5)*(27 + 2*Power(s,2) + 2*Power(s1,2) + 2*t2 - 
               s1*(7 + 4*t2) + s*(-1 - 4*s1 + 4*t2)) - 
            2*Power(t1,4)*(16 + 47*t2 + 32*Power(t2,2) - 
               18*Power(t2,3) + Power(s,2)*(-16 - 5*t2 + Power(t2,2)) + 
               Power(s1,2)*(-30 + 3*t2 + Power(t2,2)) - 
               s1*(7 - 6*t2 + 22*Power(t2,2) + 4*Power(t2,3)) + 
               s*(-27 - 20*t2 + 4*Power(t2,2) + 4*Power(t2,3) + 
                  s1*(54 + 2*t2 - 2*Power(t2,2)))) - 
            Power(t1,3)*(120 - 94*t2 - 178*Power(t2,2) - 
               115*Power(t2,3) + 40*Power(t2,4) + 8*Power(t2,5) + 
               Power(s1,2)*(96 + 8*t2 + 2*Power(t2,2) + Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s,2)*(8 - 4*t2 + 10*Power(t2,2) + 7*Power(t2,3) + 
                  Power(t2,4)) + 
               s1*(-354 - 100*t2 + 104*Power(t2,2) + 20*Power(t2,3) + 
                  17*Power(t2,4)) + 
               s*(18 + 84*t2 + 86*Power(t2,2) - 26*Power(t2,3) - 
                  11*Power(t2,4) - 
                  2*s1*(-4 - 6*t2 + 18*Power(t2,2) + 4*Power(t2,3) + 
                     Power(t2,4)))) + 
            Power(t1,2)*(88 + 258*t2 + 85*Power(t2,2) - 19*Power(t2,3) - 
               89*Power(t2,4) - 3*Power(t2,5) + 10*Power(t2,6) + 
               Power(s1,2)*(4 - 22*t2 + 57*Power(t2,2) + 9*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s,2)*(-8 - 66*t2 + 59*Power(t2,2) - 
                  25*Power(t2,3) + 2*Power(t2,4) + Power(t2,5)) + 
               s1*(-42 + 76*t2 - 286*Power(t2,2) - 72*Power(t2,3) + 
                  49*Power(t2,4) + 10*Power(t2,5) + Power(t2,6)) - 
               s*(-6 + 96*t2 - 12*Power(t2,2) - 90*Power(t2,3) + 
                  30*Power(t2,4) + 18*Power(t2,5) + Power(t2,6) + 
                  s1*(-20 - 72*t2 + 124*Power(t2,2) - 48*Power(t2,3) + 
                     5*Power(t2,4) + Power(t2,5))))))*
       T2q(t1,1 - s2 + t1 - t2))/
     ((-1 + s1)*(-1 + t1)*(1 - s2 + t1 - t2)*(-1 + t2)*Power(t1 - s2*t2,2)*
       (-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) - 
    (8*(-(Power(s2,7)*(Power(s1,2)*(1 + t1) - s1*(-4 + t1)*t2 + 
              3*Power(t2,2))) + 
         Power(s2,5)*(-1 - 13*t2 + 2*s*t2 + 8*Power(t2,2) - 
            6*s*Power(t2,2) - Power(s,2)*Power(t2,2) - 16*Power(t2,3) + 
            2*s*Power(t2,3) - Power(s,2)*Power(t2,3) + 13*Power(t2,4) + 
            Power(t2,5) + s*Power(t1,3)*(1 - s + 3*t2) - 
            t1*(5 + (15 + 10*s - 2*Power(s,2))*t2 - 
               (1 - 33*s + Power(s,2))*Power(t2,2) + 
               (23 + s)*Power(t2,3) + 2*Power(t2,4)) + 
            Power(s1,2)*(-1 - 10*Power(t1,3) + 
               5*Power(t1,2)*(-3 + t2) + t2 - 4*Power(t2,2) + 
               Power(t2,3) + t1*(17 + 4*t2 - 2*Power(t2,2))) + 
            Power(t1,2)*(-2 + Power(s,2)*(-1 + t2) - 24*t2 - 
               22*Power(t2,2) + 3*Power(t2,3) + 
               s*(6 + 20*t2 - 2*Power(t2,2))) + 
            s1*(4 + 3*t2 + 3*(3 + s)*Power(t2,2) + 5*Power(t2,3) - 
               2*Power(t2,4) + Power(t1,3)*(4 + 8*s + 6*t2) + 
               Power(t1,2)*(-31 + 13*s - 48*t2 - 6*Power(t2,2)) + 
               t1*(-25 - 2*s - 41*t2 + 31*Power(t2,2) + 8*Power(t2,3)))) \
+ Power(s2,6)*(Power(s1,2)*(-2 + 5*Power(t1,2) - t1*(-6 + t2) - 2*t2 + 
               Power(t2,2)) + 
            t2*(4 - 3*t2 + 8*Power(t2,2) + Power(t2,3) + 
               t1*(5 + 14*t2 - Power(t2,2)) + 
               s*(2 - 4*t1 - Power(t1,2) + 8*t2 + Power(t2,2))) - 
            s1*(-2 - 13*t2 + (8 + s)*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(1 + 2*s + 4*t2) + 
               t1*(-6 - 21*t2 - 2*Power(t2,2) + s*(2 + t2)))) + 
         Power(t1,2)*(-2*Power(-1 + t2,3) + 
            t1*Power(-1 + t2,2)*
             (17 + 11*t2 + Power(t2,2) - 4*s1*(1 + t2)) + 
            Power(t1,2)*(-1 + t2)*
             (-8 - 2*Power(s1,2) - 27*t2 + 13*Power(t2,2) + 
               Power(t2,3) + s1*(6 + 19*t2 - 7*Power(t2,2))) + 
            Power(t1,4)*(-10 + 2*Power(s1,2)*(-3 + t2) + 3*t2 + 
               Power(t2,2) + s1*(6 - 5*t2 + Power(t2,2))) + 
            Power(t1,3)*(3 + 13*t2 - 6*Power(t2,2) - 2*Power(t2,3) + 
               Power(s1,2)*(5 - 4*t2 + 2*Power(t2,2)) - 
               s1*(24 - 6*t2 + Power(t2,3))) + 
            Power(s,2)*(Power(t1,4)*(-4 + t2) + 
               Power(t1,3)*(3 + 5*t2 - Power(t2,2)) - 
               2*(2 - 3*t2 + Power(t2,2)) - 
               Power(t1,2)*(2 + 2*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               t1*(-18 + 24*t2 - 5*Power(t2,2) - 3*Power(t2,3) + 
                  Power(t2,4))) - 
            s*(2*Power(-1 + t2,2)*(1 + t2) + 
               2*t1*(-1 + t2)*
                (-3 + s1 - 8*t2 + s1*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               Power(t1,4)*(-6 + Power(t2,2) + s1*(-10 + 3*t2)) + 
               Power(t1,3)*(12 + 4*t2 - 2*Power(t2,3) + 
                  s1*(-6 + 7*t2)) + 
               Power(t1,2)*(22 - 14*t2 + 4*Power(t2,2) - 2*Power(t2,3) + 
                  Power(t2,4) + 
                  s1*(6 - 16*t2 + 11*Power(t2,2) - 3*Power(t2,3))))) + 
         Power(s2,4)*(-2 + 3*s*Power(t1,4)*(-1 + s - t2) - t2 - 2*s*t2 + 
            11*Power(t2,2) + 10*s*Power(t2,2) + 
            6*Power(s,2)*Power(t2,2) + 13*Power(t2,3) - 
            6*s*Power(t2,3) - 5*Power(s,2)*Power(t2,3) - 
            24*Power(t2,4) - 12*s*Power(t2,4) - Power(s,2)*Power(t2,4) + 
            3*Power(t2,5) - s*Power(t2,5) + 
            t1*(19 + 2*(21 - 6*s + 2*Power(s,2))*t2 + 
               (-4 + 16*s + Power(s,2))*Power(t2,2) + 
               4*(5 - 3*s + Power(s,2))*Power(t2,3) + 
               2*(-19 + s)*Power(t2,4) + Power(t2,5)) + 
            Power(t1,3)*(10 + Power(s,2)*(5 - 4*t2) + 41*t2 + 
               12*Power(t2,2) - 3*Power(t2,3) + 
               s*(-28 - 34*t2 + 6*Power(t2,2))) + 
            Power(s1,2)*(10*Power(t1,4) + 2*Power(t1,2)*(-26 + t2) - 
               10*Power(t1,3)*(-2 + t2) - 2*(-1 + t2)*Power(t2,2) + 
               t1*(7 + 8*Power(t2,2))) + 
            Power(t1,2)*(23 + 18*t2 + 4*Power(t2,2) + 21*Power(t2,3) + 
               4*Power(t2,4) - Power(s,2)*(2 + 9*t2 + 2*Power(t2,2)) + 
               s*(-10 + 34*t2 + 49*Power(t2,2) - 2*Power(t2,3))) - 
            s1*(-2 + 6*t2 + (1 + 4*s)*Power(t2,2) + 
               (6 - 5*s)*Power(t2,3) - (11 + s)*Power(t2,4) + 
               2*Power(t1,4)*(3 + 6*s + 2*t2) - 
               2*Power(t1,3)*
                (33 + 29*t2 + 3*Power(t2,2) + s*(-15 + 4*t2)) - 
               Power(t1,2)*(81 + 42*t2 - 54*Power(t2,2) - 
                  9*Power(t2,3) + s*(17 + 3*t2 + 5*Power(t2,2))) + 
               t1*(33 + 16*t2 + 38*Power(t2,2) + 15*Power(t2,3) - 
                  7*Power(t2,4) + 
                  s*(-2 + 5*t2 + 6*Power(t2,2) + 4*Power(t2,3))))) + 
         Power(s2,3)*(-1 + 5*t2 - 2*s*t2 - 18*Power(t2,2) + 
            6*s*Power(t2,2) + 3*Power(s,2)*Power(t2,2) + 
            4*s1*Power(t2,2) - 2*s*s1*Power(t2,2) + 5*Power(t2,3) + 
            12*s*Power(t2,3) + Power(s,2)*Power(t2,3) - 
            4*s1*Power(t2,3) + 29*Power(t2,4) - 10*s*Power(t2,4) - 
            3*Power(s,2)*Power(t2,4) - 4*s1*Power(t2,4) + 
            2*s*s1*Power(t2,4) - 20*Power(t2,5) - 6*s*Power(t2,5) + 
            4*s1*Power(t2,5) + 
            Power(t1,5)*(-3*Power(s,2) + s1*(4 - 5*s1 + t2) + 
               s*(3 + 8*s1 + t2)) + 
            Power(t1,4)*(-18 - 29*t2 + Power(t2,2) + Power(t2,3) + 
               5*Power(s1,2)*(-3 + 2*t2) + Power(s,2)*(-9 + 6*t2) + 
               s*(48 + s1*(32 - 14*t2) + 22*t2 - 6*Power(t2,2)) - 
               2*s1*(36 + 18*t2 + Power(t2,2))) + 
            Power(t1,3)*(-37 + 11*t2 + 5*Power(t2,2) - 15*Power(t2,3) - 
               4*Power(t2,4) + 2*Power(s,2)*(5 + 7*t2) + 
               2*Power(s1,2)*(40 - 6*t2 + Power(t2,2)) + 
               s1*(-121 + t2 + 49*Power(t2,2) + Power(t2,3)) - 
               s*(-28 + 66*t2 + 32*Power(t2,2) - 6*Power(t2,3) + 
                  s1*(49 + 6*t2 + 5*Power(t2,2)))) + 
            Power(t1,2)*(-66 - 47*t2 - 10*Power(t2,2) + 
               14*Power(t2,3) + 28*Power(t2,4) + Power(t2,5) - 
               2*Power(s1,2)*
                (11 - 2*t2 + 4*Power(t2,2) + 2*Power(t2,3)) - 
               Power(s,2)*(1 + 13*t2 + 8*Power(t2,3)) + 
               s1*(101 + 16*t2 + 72*Power(t2,2) + 8*Power(t2,3) - 
                  7*Power(t2,4)) + 
               2*s*(1 + 20*t2 - 2*Power(t2,2) + 5*Power(t2,3) - 
                  2*Power(t2,4) + 
                  s1*(-5 + 8*t2 + 3*Power(t2,2) + 5*Power(t2,3)))) + 
            t1*(16 - 2*(15 + 6*s + 7*Power(s,2))*t2 + 
               (-59 + 18*s + 14*Power(s,2))*Power(t2,2) + 
               (51 + 24*s + 2*Power(s,2))*Power(t2,3) + 
               (31 + 15*s + Power(s,2))*Power(t2,4) - 
               (9 + s)*Power(t2,5) + 
               4*Power(s1,2)*t2*(-1 + Power(t2,2)) + 
               s1*(-10 + 41*t2 + 3*Power(t2,2) - 13*Power(t2,3) - 
                  25*Power(t2,4) + 4*Power(t2,5) + 
                  s*(2 + 2*t2 + Power(t2,2) - 12*Power(t2,3) - 
                     Power(t2,4))))) + 
         s2*t1*(Power(t1,5)*(-4 - 9*s1 + Power(s,2)*(-2 + t2) + 
               Power(s1,2)*(-1 + t2) + t2 + 
               s*(10 + 3*s1 - 2*t2 - 2*s1*t2)) + 
            4*(-1 + t2)*t2*(Power(s,2)*(-2 + t2) + Power(-1 + t2,2) + 
               s*(-1 + Power(t2,2))) + 
            Power(t1,4)*(Power(s1,2)*(31 - 8*t2) - 
               Power(s,2)*(-14 + Power(t2,2)) - 
               s1*(38 - 24*t2 + Power(t2,3)) - 
               2*(-6 - 10*t2 + 5*Power(t2,2) + Power(t2,3)) + 
               s*(-2 - 26*t2 + 7*Power(t2,2) + Power(t2,3) + 
                  s1*(-41 + 6*t2 + Power(t2,2)))) + 
            t1*(2*s*(-1 + t2)*
                (1 + s1 - 12*t2 + 3*s1*t2 - 30*Power(t2,2) + 
                  2*s1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s,2)*(4 + 34*t2 - 53*Power(t2,2) + 
                  21*Power(t2,3) - 3*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (-13 - 43*t2 - 31*Power(t2,2) + 
                  4*s1*(1 + 3*t2 + 2*Power(t2,2)))) - 
            Power(t1,3)*(49 - 14*t2 + 30*Power(t2,2) - 22*Power(t2,3) - 
               3*Power(t2,4) + 
               Power(s,2)*(1 + 25*t2 - 3*Power(t2,2) + Power(t2,3)) + 
               Power(s1,2)*(21 - 15*t2 + 8*Power(t2,2) + Power(t2,3)) - 
               s1*(95 - 19*t2 + 23*Power(t2,2) - 5*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(-(s1*(-18 + 20*t2 + 3*Power(t2,2) + 2*Power(t2,3))) + 
                  2*(-11 - 22*t2 + 4*Power(t2,2) + Power(t2,3) + 
                     Power(t2,4)))) + 
            Power(t1,2)*(Power(s,2)*
                (-2 + 14*t2 - 6*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4)) - 
               (-1 + t2)*(-4*Power(s1,2)*(1 + t2) + 
                  s1*(2 + 69*t2 + 10*Power(t2,2) - 9*Power(t2,3)) + 
                  t2*(-84 - 25*t2 + 24*Power(t2,2) + Power(t2,3))) + 
               s*(6 + 54*t2 - 24*Power(t2,2) + 10*Power(t2,3) - 
                  3*Power(t2,4) + Power(t2,5) - 
                  s1*(-16 + 34*t2 - 15*Power(t2,2) + 4*Power(t2,3) + 
                     Power(t2,4))))) + 
         Power(s2,2)*((Power(s,2) + (-1 + s1)*s1 - s*(1 + 2*s1))*
             Power(t1,6) + t1*
             (3 + (5 - 8*Power(s,2) - 8*s1 + 2*s*(5 + 2*s1))*t2 + 
               (-11*Power(s,2) + 2*s*(-35 + s1) + 4*(10 + s1))*
                Power(t2,2) + 
               (-93 + 19*Power(s,2) - 4*s*(-9 + s1) + 12*s1)*
                Power(t2,3) + 
               (31 - 3*Power(s,2) - 2*s*(-13 + s1) - 4*s1)*Power(t2,4) - 
               2*(-7 + s + 2*s1)*Power(t2,5)) - 
            2*(-1 + t2)*Power(t2,2)*
             (Power(s,2)*(-2 + t2) + Power(-1 + t2,2) + 
               s*(-1 + Power(t2,2))) + 
            Power(t1,5)*(14 + Power(s1,2)*(6 - 5*t2) + 
               Power(s,2)*(7 - 4*t2) + 6*t2 - 2*Power(t2,2) + 
               s1*(40 + 9*t2) + 
               s*(-36 - 16*s1 - 2*t2 + 9*s1*t2 + 2*Power(t2,2))) + 
            Power(t1,4)*(17 - 41*t2 + 2*Power(t2,2) + 11*Power(t2,3) + 
               Power(t2,4) - Power(s1,2)*(68 - 14*t2 + Power(t2,2)) + 
               2*Power(s,2)*(-9 - 4*t2 + Power(t2,2)) + 
               s*(-22 + 65*s1 + 66*t2 + 2*Power(t2,2) - 5*Power(t2,3)) + 
               s1*(95 - 34*t2 - 19*Power(t2,2) + 3*Power(t2,3))) + 
            Power(t1,3)*(92 + t2 + 22*Power(t2,2) - 18*Power(t2,3) - 
               16*Power(t2,4) - Power(t2,5) + 
               2*Power(s1,2)*
                (16 - 8*t2 + 5*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s,2)*(1 + 23*t2 + 4*Power(t2,2) + 4*Power(t2,3)) + 
               s1*(-143 + 10*t2 - 66*Power(t2,2) + 8*Power(t2,3) + 
                  Power(t2,4)) - 
               2*s*(6 + 35*t2 - Power(t2,2) - 2*Power(t2,4) + 
                  s1*(-10 + 12*t2 + 3*Power(t2,2) + 4*Power(t2,3)))) - 
            Power(t1,2)*(Power(s,2)*
                (-8 + 8*t2 + 10*Power(t2,2) - 4*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s*(-14 + 48*t2 + 4*Power(t2,2) + 34*Power(t2,3) - 
                  7*Power(t2,4) + Power(t2,5) + 
                  s1*(12 - 16*t2 + Power(t2,2) - 8*Power(t2,3) - 
                     Power(t2,4))) + 
               (-1 + t2)*(-28 + 98*t2 + 67*Power(t2,2) + 2*Power(t2,3) - 
                  13*Power(t2,4) + 
                  2*Power(s1,2)*(1 + 4*t2 + Power(t2,2)) + 
                  s1*(12 - 77*t2 - 46*Power(t2,2) - Power(t2,3) + 
                     4*Power(t2,4))))))*T3q(s2,t1))/
     ((-1 + s1)*(-1 + t1)*Power(-s2 + t1,2)*(1 - s2 + t1 - t2)*(-1 + t2)*
       Power(t1 - s2*t2,2)) + (8*
       (1 - 10*t1 + 27*Power(t1,2) + 6*s*Power(t1,2) + 
         Power(s,2)*Power(t1,2) + 38*Power(t1,3) + 6*s*Power(t1,3) - 
         4*Power(s,2)*Power(t1,3) - 20*Power(t1,4) - 5*t2 + 2*s*t2 + 
         17*t1*t2 - 10*s*t1*t2 - 2*Power(s,2)*t1*t2 - 45*Power(t1,2)*t2 - 
         10*s*Power(t1,2)*t2 + 5*Power(s,2)*Power(t1,2)*t2 + 
         15*Power(t1,3)*t2 + Power(s,2)*Power(t1,3)*t2 + 
         8*Power(t1,4)*t2 + 8*Power(t2,2) - 8*s*Power(t2,2) + 
         Power(s,2)*Power(t2,2) + 3*Power(s2,6)*Power(t2,2) - 
         10*t1*Power(t2,2) + 2*Power(s,2)*t1*Power(t2,2) + 
         2*s*Power(t1,2)*Power(t2,2) - 
         Power(s,2)*Power(t1,2)*Power(t2,2) - 7*Power(t1,3)*Power(t2,2) - 
         s*Power(t1,3)*Power(t2,2) - 5*Power(t2,3) + 8*s*Power(t2,3) - 
         3*Power(s,2)*Power(t2,3) + 2*t1*Power(t2,3) - 
         Power(s,2)*t1*Power(t2,3) - 2*Power(t1,2)*Power(t2,3) + 
         2*s*Power(t1,2)*Power(t2,3) + Power(t2,4) - 2*s*Power(t2,4) + 
         Power(s,2)*Power(t2,4) + t1*Power(t2,4) - s*t1*Power(t2,4) - 
         Power(s2,5)*t2*(4 + 11*t2 + 2*Power(t2,2) + Power(t2,3) + 
            t1*(5 + 5*t2 - Power(t2,2)) + 
            s*(2 - 4*t1 - Power(t1,2) + 8*t2 + Power(t2,2))) + 
         Power(s2,4)*(1 + (-1 + s)*s*Power(t1,3) + (21 + 2*s)*t2 + 
            (11 + 30*s + Power(s,2))*Power(t2,2) + 
            (16 - 4*s + Power(s,2))*Power(t2,3) - 3*Power(t2,4) - 
            Power(t2,5) + t1*
             (5 + (29 - 4*s - 2*Power(s,2))*t2 + 
               (8 + 11*s - Power(s,2))*Power(t2,2) + 
               (3 - 2*s)*Power(t2,3) + Power(t2,4)) + 
            Power(t1,2)*(2 - Power(s,2)*(-1 + t2) + 9*t2 - 
               2*Power(t2,2) + 2*s*(-3 - 5*t2 + Power(t2,2)))) + 
         Power(s2,2)*(-2 + (6 - 4*s)*t2 - 
            2*(-9 + s + Power(s,2))*Power(t2,2) - 
            3*(13 + 4*s)*Power(t2,3) - 
            (4 - 2*s + Power(s,2))*Power(t2,4) + (1 - 2*s)*Power(t2,5) + 
            t1*(27 + 2*(47 + 7*s + 2*Power(s,2))*t2 + 
               (30 + s + Power(s,2))*Power(t2,2) + 
               (-9 - 2*s + Power(s,2))*Power(t2,3) + 
               3*(-4 + s)*Power(t2,4)) + 
            Power(t1,3)*(8 - Power(s,2)*(-1 + t2) - 7*t2 + 
               Power(t2,2) - s*(17 - 4*t2 + Power(t2,2))) + 
            Power(t1,2)*(55 - 22*t2 + 16*Power(t2,2) + 10*Power(t2,3) - 
               4*s*(5 - 7*t2 + Power(t2,2)) + 
               Power(s,2)*(-2 - 2*t2 + Power(t2,2)))) + 
         Power(s1,2)*(Power(s2,6)*(1 + t1) - 
            Power(s2,5)*(2*Power(t1,2) - t1*(-5 + t2) + (-2 + t2)*t2) + 
            Power(t1,2)*(7 + t1*(16 - 6*t2) - 10*t2 + 2*Power(t2,2)) - 
            s2*t1*(4 + 3*t2 - 10*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(-5 + 3*t2) + 
               t1*(11 + 30*t2 - 12*Power(t2,2))) + 
            Power(s2,4)*(-2 + Power(t1,3) + Power(t1,2)*(7 - 2*t2) - 
               5*t2 + 8*Power(t2,2) - Power(t2,3) - t1*(4 + Power(t2,2))\
) + Power(s2,3)*(Power(t1,3)*(-3 + t2) + Power(t1,2)*(5 + 2*t2) + 
               t2*(4 - 19*t2 + 6*Power(t2,2)) - 
               t1*(-15 + 10*t2 + 2*Power(t2,2) + Power(t2,3))) - 
            Power(s2,2)*(-1 + 3*Power(t1,3) + t2 - 4*Power(t2,2) + 
               5*Power(t2,3) - 2*Power(t1,2)*(-7 + 4*t2 + Power(t2,2)) + 
               t1*(3 - 28*t2 - 9*Power(t2,2) + 6*Power(t2,3)))) + 
         Power(s2,3)*(Power(t1,3)*
             (-4 - 2*s*(-6 + t2) + Power(s,2)*(-4 + t2) + t2) - 
            t1*(26 + 2*(29 + 7*s)*t2 + 
               (5 + 20*s - 2*Power(s,2))*Power(t2,2) + 
               (15 - 4*s + Power(s,2))*Power(t2,3) + 
               (-1 + 2*s)*Power(t2,4)) + 
            Power(t1,2)*(-20 - 13*t2 - Power(s,2)*(-5 + t2)*t2 + 
               Power(t2,2) - 2*Power(t2,3) + s*(22 - 3*t2 + Power(t2,3))\
) + t2*(-29 - 18*t2 - 13*Power(t2,2) + 
               Power(s,2)*(-3 + t2)*Power(t2,2) + 17*Power(t2,3) + 
               Power(t2,4) + s*
                (4 - 32*t2 + 19*Power(t2,2) - 2*Power(t2,3) + 
                  Power(t2,4)))) + 
         s2*(4*Power(t1,4) + Power(t1,3)*
             (6 - Power(s,2)*(-6 + t2) + 23*t2 + 2*s*(-1 + t2)*t2 - 
               18*Power(t2,2)) + 
            t1*(4 + (-61 + 10*s)*t2 + 
               (94 + 8*s - 4*Power(s,2))*Power(t2,2) + 
               (2 + Power(s,2))*Power(t2,3) + Power(t2,4)) + 
            Power(t1,2)*(-72 - 57*t2 + Power(s,2)*(-7 + t2)*t2 - 
               23*Power(t2,2) + 18*Power(t2,3) - 
               s*(2 + 6*t2 + 3*Power(t2,3))) + 
            t2*(11 - 19*t2 + 11*Power(t2,2) - 
               Power(s,2)*(-5 + t2)*Power(t2,2) - 2*Power(t2,3) - 
               Power(t2,4) + s*
                (-2 + 20*t2 - 10*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4)))) + 
         s1*(-(Power(s2,6)*(-4 + t1)*t2) + 
            Power(s2,5)*(-2 - 21*t2 + (10 + s)*Power(t2,2) + 
               2*Power(t2,3) + t1*(-3 + s - 2*t2)*(2 + t2) + 
               Power(t1,2)*(1 + 2*s + t2)) - 
            t1*(4*Power(t1,3)*(-5 + 2*t2) + 
               Power(-1 + t2,2)*(-4 + 3*t2) + 
               Power(t1,2)*(60 + 9*t2 - 9*Power(t2,2)) + 
               t1*(48 - 60*t2 + 2*Power(t2,2) + Power(t2,3)) + 
               s*(2 - 10*t2 + 9*Power(t2,2) - 3*Power(t2,3) + 
                  Power(t1,2)*(-8 + 3*t2) + t1*(-2 + 3*t2))) - 
            Power(s2,4)*((1 + 2*s)*Power(t1,3) + 
               t2*(-27 + (47 + 7*s)*t2 + 7*Power(t2,2) - 
                  2*Power(t2,3)) - 
               Power(t1,2)*(11 + 7*t2 + s*(-11 + 3*t2)) + 
               t1*(-31 - 15*t2 + 11*Power(t2,2) + 
                  s*(2 + 2*t2 - 3*Power(t2,2)))) + 
            s2*(-2 - 4*Power(t1,4) + 6*t2 - (5 + 2*s)*Power(t2,2) + 
               s*Power(t2,3) - (-1 + s)*Power(t2,4) + 
               Power(t1,3)*(-13 - 18*t2 + 14*Power(t2,2) + 
                  s*(-15 + 4*t2)) + 
               Power(t1,2)*(92 + 99*t2 + 7*Power(t2,2) - 
                  15*Power(t2,3) + s*(-15 + 9*t2 + Power(t2,2))) + 
               t1*(13 + 73*t2 - 107*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4) + 
                  s*(2 - 11*t2 + 13*Power(t2,2) - 4*Power(t2,3)))) + 
            Power(s2,2)*(t2*(-15 - (21 + s)*t2 + 45*Power(t2,2) + 
                  2*s*Power(t2,3)) + 
               Power(t1,3)*(17 + 3*t2 + Power(t2,2) + s*(2 + t2)) - 
               Power(t1,2)*(-1 + 9*t2 + 12*Power(t2,2) + 
                  7*Power(t2,3) + s*(-17 + 2*Power(t2,2))) - 
               t1*(19 + 139*t2 + 51*Power(t2,2) - 17*Power(t2,3) - 
                  6*Power(t2,4) + 
                  s*(-4 + 8*t2 + 2*Power(t2,2) + Power(t2,3)))) - 
            Power(s2,3)*(-4 + t2 - (79 + 9*s)*Power(t2,2) + 
               (8 + s)*Power(t2,3) + (11 + s)*Power(t2,4) + 
               Power(t1,3)*(7 + s*(-7 + 2*t2)) + 
               Power(t1,2)*(41 - 2*t2 + Power(t2,2) + Power(t2,3) - 
                  s*(5 - 9*t2 + Power(t2,2))) - 
               t1*(-23 + 38*t2 + 33*Power(t2,2) + 8*Power(t2,3) + 
                  Power(t2,4) + 
                  s*(-4 + 10*t2 - 5*Power(t2,2) + 2*Power(t2,3))))))*
       T4q(-1 + s2))/
     ((-1 + s1)*(-1 + s2)*(-1 + t1)*(1 - s2 + t1 - t2)*(-1 + t2)*
       Power(t1 - s2*t2,2)) + (8*
       (-(Power(s2,5)*(Power(s1,2)*(1 + t1) - s1*(-4 + t1)*t2 + 
              3*Power(t2,2))) - 
         Power(1 + (-1 + s)*t2,2)*
          (-1 + 4*t2 - 4*Power(t2,2) + Power(t2,3)) + 
         Power(t1,4)*(-6 + Power(s,2)*(-4 + t2) + 
            2*Power(s1,2)*(-3 + t2) + 3*t2 + Power(t2,2) + 
            s1*(6 - 5*t2 + Power(t2,2)) - 
            s*(-6 + Power(t2,2) + s1*(-10 + 3*t2))) - 
         Power(t1,3)*(-21 + Power(s1,2)*(1 - 2*t2) + 2*Power(t2,2) + 
            3*Power(t2,3) + Power(s,2)*(3 - 10*t2 + 2*Power(t2,2)) + 
            s1*(22 - 7*t2 - 4*Power(t2,2) + 2*Power(t2,3)) - 
            s*(-8 - 12*t2 + Power(t2,2) + 3*Power(t2,3) + 
               3*s1*(6 - 6*t2 + Power(t2,2)))) + 
         t1*(-9 - 2*(-11 + 4*s + Power(s,2))*t2 + 
            (-19 + 2*s + 5*Power(s,2))*Power(t2,2) + 
            (7 + 8*s - 6*Power(s,2))*Power(t2,3) + 
            s*(-3 + 2*s)*Power(t2,4) + (-1 + s)*Power(t2,5) - 
            s1*(-1 + t2)*(-(Power(-1 + t2,2)*(-4 + 3*t2)) + 
               s*(-2 + 10*t2 - 9*Power(t2,2) + 3*Power(t2,3)))) + 
         Power(t1,2)*(Power(s,2)*(1 + 2*t2 - 4*Power(t2,2)) + 
            (-1 + t2)*(Power(s1,2)*(-7 + 6*t2 - 2*Power(t2,2)) + 
               s1*(32 - 7*t2 - Power(t2,2) + Power(t2,3)) + 
               3*(-3 - 2*t2 + Power(t2,2) + Power(t2,3))) + 
            s*(14 - 14*t2 + 8*Power(t2,2) - 3*Power(t2,4) + 
               s1*(-8 + 5*t2 - 4*Power(t2,2) + 3*Power(t2,3)))) + 
         Power(s2,4)*(Power(s1,2)*
             (-1 + 3*Power(t1,2) + t1*(5 - 2*t2) - 3*t2 + Power(t2,2)) + 
            t2*(4 + 8*t2 - Power(t2,2) + Power(t2,3) + 
               t1*(5 + 8*t2 - Power(t2,2)) + 
               s*(2 - 4*t1 - Power(t1,2) + 8*t2 + Power(t2,2))) - 
            s1*(-2 - 17*t2 + (14 + s)*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(1 + 2*s + 2*t2) + 
               t1*(s*(2 + t2) - 3*(2 + 4*t2 + Power(t2,2))))) + 
         Power(s2,3)*(-1 - 17*t2 + 9*Power(t2,2) - 20*s*Power(t2,2) - 
            Power(s,2)*Power(t2,2) - 17*Power(t2,3) + 13*s*Power(t2,3) - 
            Power(s,2)*Power(t2,3) + 8*Power(t2,4) + s*Power(t2,4) + 
            2*Power(t2,5) + s*Power(t1,3)*(1 - s + t2) + 
            t1*(-5 + 2*(-14 - s + Power(s,2))*t2 + 
               (1 - 23*s + Power(s,2))*Power(t2,2) + 
               (1 + s)*Power(t2,3) - 3*Power(t2,4)) + 
            Power(t1,2)*(-2 + Power(s,2)*(-1 + t2) - 14*t2 - 
               3*Power(t2,2) + Power(t2,3) + 
               s*(6 + 13*t2 - 3*Power(t2,2))) + 
            Power(s1,2)*(1 - 3*Power(t1,3) + t2 - 5*Power(t2,2) + 
               2*Power(t2,3) + Power(t1,2)*(-8 + 5*t2) + 
               t1*(10 + 4*t2 - Power(t2,2))) + 
            s1*(2 - 8*t2 + (34 + 6*s)*Power(t2,2) - 
               (11 + s)*Power(t2,3) - 4*Power(t2,4) + 
               Power(t1,3)*(2 + 4*s + t2) + 
               t1*(-27 - (14 + s)*t2 + (26 - 3*s)*Power(t2,2) + 
                  4*Power(t2,3)) - 
               Power(t1,2)*(s*(-11 + 4*t2) + 3*(6 + 6*t2 + Power(t2,2))))) \
+ Power(s2,2)*(-1 + (-1 + s)*s*Power(t1,4) + 11*t2 - 4*s*t2 - 
            14*Power(t2,2) + 20*s*Power(t2,2) - Power(s,2)*Power(t2,2) + 
            15*Power(t2,3) - 16*s*Power(t2,3) + Power(s,2)*Power(t2,3) - 
            17*Power(t2,4) + 6*s*Power(t2,4) - 2*Power(s,2)*Power(t2,4) + 
            5*Power(t2,5) - s*Power(t2,5) + Power(t2,6) + 
            t1*(22 + 2*(13 + 6*s + Power(s,2))*t2 + 
               (7 - s + 2*Power(s,2))*Power(t2,2) + 
               (7 - 19*s + 3*Power(s,2))*Power(t2,3) + 
               4*(-3 + s)*Power(t2,4) - 2*Power(t2,5)) + 
            Power(t1,3)*(6 + Power(s,2)*(4 - 3*t2) + 8*t2 - 
               2*Power(t2,2) + s*(-17 - 5*t2 + 2*Power(t2,2))) + 
            Power(t1,2)*(23 + 13*t2 - 16*Power(t2,2) + 9*Power(t2,3) + 
               Power(t2,4) + Power(s,2)*(-1 - 7*t2 + Power(t2,2)) + 
               s*(-16 + 22*t2 + 19*Power(t2,2) - 5*Power(t2,3))) + 
            Power(s1,2)*(1 + Power(t1,4) + Power(t1,3)*(5 - 4*t2) - 
               2*t2 + 5*Power(t2,2) - 5*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(-21 - t2 + Power(t2,2)) + 
               t1*(-6 + 11*t2 - 3*Power(t2,2) + Power(t2,3))) + 
            s1*(-2 - (1 + 2*s)*Power(t1,4) - 5*t2 - 
               11*(1 + s)*Power(t2,2) + 6*(3 + s)*Power(t2,3) + 
               (2 + s)*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,3)*(21 + 10*t2 + s*(-12 + 7*t2)) + 
               Power(t1,2)*(48 - 13*t2 - 21*Power(t2,2) + Power(t2,3) + 
                  s*(6 + 13*t2 - Power(t2,2))) + 
               t1*(-6 - 35*t2 - 15*Power(t2,2) + 16*Power(t2,3) + 
                  Power(t2,4) + 
                  s*(4 - 11*t2 + 5*Power(t2,2) - 5*Power(t2,3))))) + 
         s2*(1 + 4*t2 - 12*Power(t2,2) + 10*s*Power(t2,2) + 
            Power(s,2)*Power(t2,2) + 9*Power(t2,3) - 6*s*Power(t2,3) - 
            2*Power(t2,4) - 6*s*Power(t2,4) + Power(s,2)*Power(t2,4) - 
            Power(t2,5) + 3*s*Power(t2,5) - Power(s,2)*Power(t2,5) + 
            Power(t2,6) - s*Power(t2,6) + 
            Power(t1,4)*(-4 - 2*s*(-5 + t2) + Power(s,2)*(-2 + t2) + t2) + 
            t1*(-4 - (11 + 14*s + 2*Power(s,2))*t2 + 
               (4 - 2*s + 4*Power(s,2))*Power(t2,2) + 
               (19 + 6*s - Power(s,2))*Power(t2,3) + 
               (-4 - 3*s + 2*Power(s,2))*Power(t2,4) + 
               (-4 + 3*s)*Power(t2,5)) + 
            Power(s1,2)*t1*(-4 + Power(t1,3)*(-1 + t2) + t2 + 
               5*Power(t2,2) - 3*Power(t2,3) + Power(t2,4) - 
               Power(t1,2)*(-18 + 2*t2 + Power(t2,2)) - 
               t1*(-4 + 8*t2 - 2*Power(t2,2) + Power(t2,3))) + 
            Power(t1,3)*(-16 + 21*t2 - 9*Power(t2,2) - 2*Power(t2,3) + 
               Power(s,2)*(4 + 5*t2 - 2*Power(t2,2)) + 
               s*(16 - 36*t2 + 7*Power(t2,2) + Power(t2,3))) + 
            Power(t1,2)*(-45 - t2 - 20*Power(t2,2) + 13*Power(t2,3) + 
               5*Power(t2,4) + Power(s,2)*(1 - 8*t2 - 3*Power(t2,2)) + 
               s*(4 + 4*t2 + 26*Power(t2,2) - 5*Power(t2,3) - 
                  3*Power(t2,4))) + 
            s1*(Power(t1,4)*(-9 + s*(3 - 2*t2)) + 
               (-1 + t2)*(2 - 6*t2 + (5 + 2*s)*Power(t2,2) - 
                  s*Power(t2,3) + (-1 + s)*Power(t2,4)) + 
               t1*(15 + (23 + 17*s)*t2 - (37 + 7*s)*Power(t2,2) + 
                  (-6 + s)*Power(t2,3) - 3*(-2 + s)*Power(t2,4) - 
                  Power(t2,5)) - 
               Power(t1,3)*(31 - 21*t2 - 2*Power(t2,2) + Power(t2,3) + 
                  s*(18 + 5*t2 - 3*Power(t2,2))) + 
               Power(t1,2)*(33 + 16*t2 - 5*Power(t2,2) - 7*Power(t2,3) + 
                  2*Power(t2,4) + 
                  s*(-15 + 10*t2 + 3*Power(t2,2) + Power(t2,3))))))*
       T5q(1 - s2 + t1 - t2))/
     ((-1 + s1)*(-1 + t1)*(1 - s2 + t1 - t2)*(-1 + t2)*Power(t1 - s2*t2,2)));
   return a;
};
