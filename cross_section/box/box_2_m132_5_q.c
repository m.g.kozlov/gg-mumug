#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_2_m132_5_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(-24*Power(s2,2) - 16*Power(s2,3) + 8*Power(s2,4) + 48*s2*t1 + 
         104*Power(s2,2)*t1 + 8*Power(s2,3)*t1 - 16*Power(s2,4)*t1 - 
         24*Power(t1,2) - 160*s2*Power(t1,2) - 
         112*Power(s2,2)*Power(t1,2) + 32*Power(s2,3)*Power(t1,2) + 
         8*Power(s2,4)*Power(t1,2) + 72*Power(t1,3) + 168*s2*Power(t1,3) + 
         8*Power(s2,2)*Power(t1,3) - 24*Power(s2,3)*Power(t1,3) - 
         72*Power(t1,4) - 48*s2*Power(t1,4) + 24*Power(s2,2)*Power(t1,4) + 
         24*Power(t1,5) - 8*s2*Power(t1,5) + 
         Power(s1,5)*(19*Power(s2,2) + 3*Power(s2,3) + 
            s2*(2 - 11*t1 - 5*Power(t1,2)) + 
            2*(-7 + 8*t1 - 2*Power(t1,2) + Power(t1,3))) + 24*t2 - 
         44*s2*t2 - 21*Power(s2,2)*t2 + 57*Power(s2,3)*t2 + 
         8*Power(s2,4)*t2 - 4*Power(s2,5)*t2 + 8*t1*t2 + 129*s2*t1*t2 - 
         124*Power(s2,2)*t1*t2 - 124*Power(s2,3)*t1*t2 + 
         4*Power(s2,4)*t1*t2 - 108*Power(t1,2)*t2 + 43*s2*Power(t1,2)*t2 + 
         277*Power(s2,2)*Power(t1,2)*t2 + 15*Power(s2,3)*Power(t1,2)*t2 + 
         36*Power(t1,3)*t2 - 231*s2*Power(t1,3)*t2 - 
         48*Power(s2,2)*Power(t1,3)*t2 + 8*Power(s2,3)*Power(t1,3)*t2 + 
         70*Power(t1,4)*t2 + 63*s2*Power(t1,4)*t2 - 
         16*Power(s2,2)*Power(t1,4)*t2 - 30*Power(t1,5)*t2 + 
         8*s2*Power(t1,5)*t2 - 44*Power(t2,2) + 42*s2*Power(t2,2) + 
         81*Power(s2,2)*Power(t2,2) - 6*Power(s2,3)*Power(t2,2) - 
         3*Power(s2,4)*Power(t2,2) + 103*t1*Power(t2,2) - 
         242*s2*t1*Power(t2,2) - 143*Power(s2,2)*t1*Power(t2,2) + 
         99*Power(s2,3)*t1*Power(t2,2) + 25*Power(s2,4)*t1*Power(t2,2) + 
         95*Power(t1,2)*Power(t2,2) + 269*s2*Power(t1,2)*Power(t2,2) - 
         193*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         59*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         8*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         163*Power(t1,3)*Power(t2,2) + 102*s2*Power(t1,3)*Power(t2,2) + 
         51*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         16*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         3*Power(t1,4)*Power(t2,2) - 25*s2*Power(t1,4)*Power(t2,2) - 
         8*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         8*Power(t1,5)*Power(t2,2) - 19*Power(t2,3) + 6*s2*Power(t2,3) + 
         8*Power(s2,2)*Power(t2,3) - 51*Power(s2,3)*Power(t2,3) - 
         32*Power(s2,4)*Power(t2,3) + 4*Power(s2,5)*Power(t2,3) - 
         117*t1*Power(t2,3) + 59*s2*t1*Power(t2,3) + 
         271*Power(s2,2)*t1*Power(t2,3) + 75*Power(s2,3)*t1*Power(t2,3) - 
         9*Power(s2,4)*t1*Power(t2,3) + 57*Power(t1,2)*Power(t2,3) - 
         289*s2*Power(t1,2)*Power(t2,3) - 
         55*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         2*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         91*Power(t1,3)*Power(t2,3) + 20*s2*Power(t1,3)*Power(t2,3) + 
         7*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         16*Power(t1,4)*Power(t2,3) + 46*Power(t2,4) - 39*s2*Power(t2,4) - 
         113*Power(s2,2)*Power(t2,4) - 19*Power(s2,3)*Power(t2,4) + 
         11*Power(s2,4)*Power(t2,4) + 31*t1*Power(t2,4) + 
         139*s2*t1*Power(t2,4) + 11*Power(s2,2)*t1*Power(t2,4) - 
         13*Power(s2,3)*t1*Power(t2,4) - 83*Power(t1,2)*Power(t2,4) + 
         9*s2*Power(t1,2)*Power(t2,4) + 
         2*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         8*Power(t1,3)*Power(t2,4) - 19*Power(t2,5) - 7*s2*Power(t2,5) + 
         13*Power(s2,2)*Power(t2,5) + Power(s2,3)*Power(t2,5) + 
         19*t1*Power(t2,5) - 12*s2*t1*Power(t2,5) - 
         Power(s2,2)*t1*Power(t2,5) + 
         Power(s,5)*(-12*t1 + 8*Power(t1,2) - 3*Power(t1,3) + 
            2*s1*(-((-9 + 4*t1)*(t1 - t2)) + 4*s2*(-1 + t2)) + 12*t2 - 
            16*t1*t2 + 5*Power(t1,2)*t2 + 8*Power(t2,2) - 
            t1*Power(t2,2) - Power(t2,3) + Power(s1,2)*(4 - 5*t1 + t2) + 
            Power(s2,2)*(4 - 7*t1 + 3*t2) + 
            s2*(6*t1 + 4*Power(t1,2) - 2*t2*(3 + 2*t2))) + 
         Power(s1,4)*(-38 + 5*Power(s2,4) + 2*Power(t1,4) + 
            t1*(52 - 63*t2) + Power(s2,3)*(-27 + 4*t1 - 19*t2) + 54*t2 - 
            2*Power(t1,3)*(1 + 3*t2) + 3*Power(t1,2)*(-4 + 5*t2) - 
            s2*(11 + 2*Power(t1,3) + t1*(2 - 53*t2) + 
               Power(t1,2)*(17 - 10*t2) + 12*t2) + 
            Power(s2,2)*(-3 - 9*Power(t1,2) - 85*t2 + t1*(58 + 15*t2))) + 
         Power(s1,3)*(-8 - 8*Power(s2,5) + 
            Power(s2,4)*(24 + 25*t1 - 30*t2) + Power(t1,4)*(14 - 4*t2) + 
            75*t2 - 59*Power(t2,2) + 
            Power(t1,2)*(-12 + 110*t2 - 21*Power(t2,2)) + 
            Power(t1,3)*(-52 - 6*t2 + 6*Power(t2,2)) + 
            t1*(62 - 183*t2 + 74*Power(t2,2)) + 
            Power(s2,3)*(10 - 19*Power(t1,2) + 96*t2 + 38*Power(t2,2) + 
               t1*(-65 + 7*t2)) + 
            Power(s2,2)*(2 + 3*Power(t1,3) + 117*t2 + 128*Power(t2,2) + 
               3*Power(t1,2)*(25 + 9*t2) - 
               t1*(119 + 198*t2 + 44*Power(t2,2))) - 
            s2*(-14 + 40*Power(t1,3) + Power(t1,4) - 74*t2 - 
               31*Power(t2,2) - 7*Power(t1,2)*(22 + 9*t2) + 
               t1*(67 + 132*t2 + 81*Power(t2,2)))) + 
         Power(s1,2)*(-8 + 6*Power(t1,5) - 15*t2 + 10*Power(t2,2) - 
            7*Power(t2,3) + 4*Power(s2,5)*(-1 + 5*t2) + 
            Power(t1,4)*(12 - 41*t2 + 2*Power(t2,2)) + 
            t1*(22 - 216*t2 + 241*Power(t2,2) - 4*Power(t2,3)) - 
            2*Power(t1,3)*(74 - 93*t2 - 13*Power(t2,2) + Power(t2,3)) + 
            Power(t1,2)*(118 + 74*t2 - 267*Power(t2,2) + 
               13*Power(t2,3)) - 
            Power(s2,4)*(1 + 8*Power(t1,2) + 84*t2 - 56*Power(t2,2) + 
               t1*(-39 + 59*t2)) + 
            Power(s2,3)*(-17 + 16*Power(t1,3) + 
               36*Power(t1,2)*(-2 + t2) - 63*t2 - 130*Power(t2,2) - 
               30*Power(t2,3) + t1*(67 + 209*t2 - 39*Power(t2,2))) + 
            s2*(63 + 2*Power(t1,4)*(-9 + t2) - 23*t2 - 154*Power(t2,2) - 
               41*Power(t2,3) + 
               Power(t1,3)*(41 + 90*t2 + 6*Power(t2,2)) - 
               Power(t1,2)*(-245 + 554*t2 + 66*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(-221 + 165*t2 + 409*Power(t2,2) + 35*Power(t2,3))) + 
            Power(s2,2)*(49 - 8*Power(t1,4) + 27*t2 - 338*Power(t2,2) - 
               64*Power(t2,3) + Power(t1,3)*(49 + t2) - 
               Power(t1,2)*(111 + 198*t2 + 25*Power(t2,2)) + 
               t1*(-111 + 467*t2 + 233*Power(t2,2) + 42*Power(t2,3)))) - 
         s1*(24 - 52*t2 - 42*Power(t2,2) + 93*Power(t2,3) - 
            45*Power(t2,4) + 2*Power(t1,5)*(-15 + 7*t2) + 
            Power(t1,4)*(70 + 15*t2 - 43*Power(t2,2)) + 
            4*Power(s2,5)*(-1 - t2 + 4*Power(t2,2)) + 
            Power(t1,3)*(36 - 311*t2 + 225*Power(t2,2) + 
               26*Power(t2,3)) + 
            Power(t1,2)*(-108 + 213*t2 + 119*Power(t2,2) - 
               252*Power(t2,3) + 3*Power(t2,4)) + 
            t1*(8 + 125*t2 - 271*Power(t2,2) + 141*Power(t2,3) + 
               42*Power(t2,4)) + 
            Power(s2,4)*(8 - 4*t2 - 16*Power(t1,2)*t2 - 92*Power(t2,2) + 
               42*Power(t2,3) + t1*(4 + 64*t2 - 43*Power(t2,2))) + 
            Power(s2,3)*(57 - 23*t2 - 104*Power(t2,2) - 80*Power(t2,3) - 
               7*Power(t2,4) + 8*Power(t1,3)*(1 + 4*t2) + 
               Power(t1,2)*(15 - 131*t2 + 15*Power(t2,2)) + 
               t1*(-124 + 166*t2 + 219*Power(t2,2) - 41*Power(t2,3))) + 
            s2*(-44 + 8*Power(t1,5) + 105*t2 - 3*Power(t2,2) - 
               130*Power(t2,3) - 27*Power(t2,4) + 
               Power(t1,4)*(63 - 43*t2 + Power(t2,2)) + 
               Power(t1,3)*(-231 + 143*t2 + 70*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(129 - 463*t2 + 157*Power(t2,2) + 414*Power(t2,3) - 
                  16*Power(t2,4)) + 
               Power(t1,2)*(43 + 514*t2 - 689*Power(t2,2) - 
                  11*Power(t2,3) - 5*Power(t2,4))) + 
            Power(s2,2)*(-21 + 130*t2 + 37*Power(t2,2) - 
               337*Power(t2,3) + 11*Power(t2,4) - 
               16*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(-48 + 100*t2 + 11*Power(t2,2)) - 
               Power(t1,2)*(-277 + 304*t2 + 178*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(-124 - 254*t2 + 619*Power(t2,2) + 104*Power(t2,3) + 
                  12*Power(t2,4)))) + 
         Power(s,4)*(-12 + 60*t1 - 42*Power(t1,2) + 22*Power(t1,3) - 
            7*Power(t1,4) - 60*t2 + 24*t1*t2 - 30*Power(t1,2)*t2 + 
            7*Power(t1,3)*t2 + 18*Power(t2,2) - 6*t1*Power(t2,2) + 
            5*Power(t1,2)*Power(t2,2) + 14*Power(t2,3) - 
            3*t1*Power(t2,3) - 2*Power(t2,4) + 
            Power(s1,3)*(s2 + 11*t1 - 2*(4 + t2)) + 
            Power(s2,3)*(25*t1 - 2*(9 + t2)) + 
            Power(s2,2)*(-1 - 46*Power(t1,2) - 22*t2 + 25*Power(t2,2) + 
               t1*(38 + 5*t2)) + 
            Power(s1,2)*(-19 + 4*Power(s2,2) + 5*Power(t1,2) + 
               s2*(4 + 30*t1 - 25*t2) + 38*t2 + 2*Power(t2,2) - 
               t1*(22 + 23*t2)) + 
            s2*(6 + 28*Power(t1,3) + 29*t2 - 62*Power(t2,2) - 
               5*Power(t2,3) - Power(t1,2)*(24 + 29*t2) + 
               t1*(-29 + 86*t2 + 6*Power(t2,2))) + 
            s1*(30 - 5*Power(s2,3) - 13*Power(t1,3) + 
               Power(s2,2)*(34 + 6*t1 - 43*t2) + 
               Power(t1,2)*(38 - 4*t2) + 19*t2 - 44*Power(t2,2) + 
               2*Power(t2,3) + t1*(-19 + 6*t2 + 15*Power(t2,2)) + 
               s2*(-16 + 37*Power(t1,2) + 68*t2 + 29*Power(t2,2) - 
                  2*t1*(50 + 17*t2)))) + 
         Power(s,3)*(48 - 115*t1 + 99*Power(t1,2) - 85*Power(t1,3) + 
            11*Power(t1,4) - 4*Power(t1,5) + 
            Power(s2,4)*(18 - 26*t1 - 5*t2) + 67*t2 + 38*t1*t2 + 
            65*Power(t1,2)*t2 + 32*Power(t1,3)*t2 - 6*Power(t1,4)*t2 - 
            137*Power(t2,2) + 5*t1*Power(t2,2) - 
            94*Power(t1,2)*Power(t2,2) + 19*Power(t1,3)*Power(t2,2) + 
            15*Power(t2,3) + 48*t1*Power(t2,3) - 
            5*Power(t1,2)*Power(t2,3) + 3*Power(t2,4) - 
            3*t1*Power(t2,4) - Power(t2,5) + 
            Power(s1,4)*(4 - 2*s2 - 7*t1 + t2) + 
            Power(s2,3)*(33 + 85*Power(t1,2) + 44*t2 - 30*Power(t2,2) + 
               t1*(-111 + 16*t2)) - 
            Power(s1,3)*(12*Power(s2,2) - 17*Power(t1,2) + 
               s2*(11 + 55*t1 - 28*t2) + t1*(23 - 14*t2) + 
               2*(-31 + 8*t2 + Power(t2,2))) + 
            Power(s2,2)*(5 - 88*Power(t1,3) + 13*t2 + 16*Power(t2,2) + 
               38*Power(t2,3) + Power(t1,2)*(114 + 11*t2) + 
               t1*(-47 - 102*t2 + 15*Power(t2,2))) + 
            s2*(-17 + 33*Power(t1,4) + 42*t2 + 157*Power(t2,2) - 
               115*Power(t2,3) + 2*Power(t2,4) - 
               8*Power(t1,3)*(4 + t2) + 
               Power(t1,2)*(93 + 33*t2 - 33*Power(t2,2)) + 
               2*t1*(-7 - 125*t2 + 57*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s1,2)*(11 + Power(s2,3) + 31*Power(t1,3) - 154*t2 + 
               23*Power(t2,2) - Power(t1,2)*(119 + 45*t2) + 
               Power(s2,2)*(-18 - 45*t1 + 106*t2) + 
               t1*(52 + 124*t2 - 10*Power(t2,2)) + 
               s2*(9 - 31*Power(t1,2) - 82*t2 - 48*Power(t2,2) + 
                  t1*(121 + 92*t2))) + 
            s1*(-79 + 13*Power(s2,4) + 3*Power(t1,4) + 120*t2 + 
               77*Power(t2,2) - 14*Power(t2,3) + 2*Power(t2,4) + 
               Power(s2,3)*(-41 - 40*t1 + 43*t2) - 
               Power(t1,3)*(21 + 44*t2) + 
               Power(t1,2)*(-47 + 184*t2 + 33*Power(t2,2)) + 
               t1*(-4 - 30*t2 - 149*Power(t2,2) + 6*Power(t2,3)) + 
               s2*(-16 + 15*Power(t1,3) - 211*t2 + 208*Power(t2,2) + 
                  20*Power(t2,3) + 8*Power(t1,2)*(-10 + 7*t2) + 
                  t1*(203 - 184*t2 - 43*Power(t2,2))) + 
               Power(s2,2)*(Power(t1,2) + 3*t1*(37 + 6*t2) + 
                  4*(-2 + t2 - 33*Power(t2,2))))) + 
         Power(s,2)*(-60 + 107*t1 - 118*Power(t1,2) + 169*Power(t1,3) - 
            62*Power(t1,4) - 4*Power(t1,5) + Power(s1,5)*(s2 + t1) + 
            37*t2 - 101*t1*t2 - 109*Power(t1,2)*t2 - 72*Power(t1,3)*t2 + 
            55*Power(t1,4)*t2 - 8*Power(t1,5)*t2 + 147*Power(t2,2) + 
            59*t1*Power(t2,2) + 242*Power(t1,2)*Power(t2,2) - 
            70*Power(t1,3)*Power(t2,2) + 9*Power(t1,4)*Power(t2,2) - 
            119*Power(t2,3) - 140*t1*Power(t2,3) - 
            12*Power(t1,2)*Power(t2,3) + 5*Power(t1,3)*Power(t2,3) + 
            32*Power(t2,4) + 34*t1*Power(t2,4) - 
            5*Power(t1,2)*Power(t2,4) - 3*Power(t2,5) - t1*Power(t2,5) + 
            4*Power(s2,5)*(-1 + 2*t1 + t2) + 
            Power(s2,4)*(-30 - 40*Power(t1,2) + t1*(66 - 29*t2) - 
               12*t2 + Power(t2,2)) + 
            Power(s1,4)*(-57 + 12*Power(s2,2) - 17*Power(t1,2) + 
               s2*(32 + 28*t1 - 11*t2) - 4*t2 + t1*(30 + t2)) + 
            Power(s2,3)*(-34 + 64*Power(t1,3) - 44*t2 + 
               100*Power(t2,2) - 53*Power(t2,3) + 
               2*Power(t1,2)*(-65 + 36*t2) + 
               t1*(130 - 45*t2 - 8*Power(t2,2))) + 
            Power(s1,3)*(-136 + 16*Power(s2,3) - 9*Power(t1,3) + 
               Power(s2,2)*(-27 + 68*t1 - 95*t2) + 184*t2 + 
               15*Power(t2,2) + Power(t1,2)*(48 + 50*t2) + 
               t1*(63 - 130*t2 - 8*Power(t2,2)) + 
               s2*(25 + t1 - 38*Power(t1,2) - 69*t2 - 44*t1*t2 + 
                  24*Power(t2,2))) + 
            Power(s2,2)*(-36 - 40*Power(t1,4) + 
               Power(t1,3)*(58 - 73*t2) + 9*t2 - 97*Power(t2,2) + 
               114*Power(t2,3) + 13*Power(t2,4) + 
               Power(t1,2)*(-145 + 98*t2 + 66*Power(t2,2)) + 
               t1*(123 + 118*t2 - 242*Power(t2,2) + 18*Power(t2,3))) + 
            s2*(7 + 8*Power(t1,5) - 177*t2 + 32*Power(t2,2) + 
               243*Power(t2,3) - 69*Power(t2,4) + 3*Power(t2,5) + 
               2*Power(t1,4)*(7 + 17*t2) + 
               Power(t1,3)*(107 - 112*t2 - 52*Power(t2,2)) + 
               Power(t1,2)*(-276 + 67*t2 + 197*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(118 + 292*t2 - 417*Power(t2,2) - 30*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,2)*(43 - 21*Power(s2,4) + 17*Power(t1,4) + 
               Power(s2,3)*(67 + 57*t1 - 121*t2) + 189*t2 - 
               165*Power(t2,2) - 21*Power(t2,3) + 
               Power(t1,3)*(-86 + 13*t2) + 
               Power(t1,2)*(256 - 69*t2 - 54*Power(t2,2)) + 
               t1*(-35 - 311*t2 + 204*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(-43 + 31*Power(t1,2) + 135*t2 + 
                  167*Power(t2,2) - 3*t1*(55 + 26*t2)) + 
               s2*(76 - 68*Power(t1,3) + 243*t2 - 27*Power(t2,2) - 
                  16*Power(t2,3) + Power(t1,2)*(217 + 87*t2) + 
                  t1*(-371 - 83*t2 + 6*Power(t2,2)))) - 
            s1*(-29 + 8*Power(s2,5) - 8*Power(t1,5) + 208*t2 - 
               66*Power(t2,2) - 6*Power(t2,3) - 13*Power(t2,4) + 
               24*Power(t1,4)*(2 + t2) - 
               Power(s2,4)*(4 + 45*t1 + 16*t2) + 
               Power(t1,3)*(-86 - 140*t2 + 9*Power(t2,2)) - 
               Power(t1,2)*(108 - 480*t2 + 33*Power(t2,2) + 
                  26*Power(t2,3)) + 
               t1*(21 + 6*t2 - 388*Power(t2,2) + 138*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,3)*(-33 + 93*Power(t1,2) + 175*t2 - 
                  158*Power(t2,2) + t1*(-49 + 43*t2)) + 
               Power(s2,2)*(62 - 83*Power(t1,3) - 177*t2 + 
                  222*Power(t2,2) + 97*Power(t2,3) + 
                  Power(t1,2)*(51 + 95*t2) + 
                  t1*(94 - 380*t2 + 8*Power(t2,2))) + 
               s2*(-173 + 35*Power(t1,4) + 78*t2 + 511*Power(t2,2) - 
                  133*Power(t2,3) + Power(t2,4) - 
                  2*Power(t1,3)*(31 + 57*t2) + 
                  Power(t1,2)*(64 + 363*t2 + 54*Power(t2,2)) - 
                  t1*(-208 + 703*t2 + 112*Power(t2,2) + 8*Power(t2,3))))) \
- s*(-24 + 40*t1 - 76*Power(t1,2) + 172*Power(t1,3) - 126*Power(t1,4) + 
            14*Power(t1,5) + Power(s1,5)*
             (-14 + 4*Power(s2,2) + 3*t1 - 3*Power(t1,2) + s2*(17 + 3*t1)) \
+ 80*t2 - 58*t1*t2 - 177*Power(t1,2)*t2 + 2*Power(t1,3)*t2 + 
            123*Power(t1,4)*t2 - 4*Power(t1,5)*t2 - 10*Power(t2,2) + 
            167*t1*Power(t2,2) + 223*Power(t1,2)*Power(t2,2) - 
            200*Power(t1,3)*Power(t2,2) - 28*Power(t1,4)*Power(t2,2) + 
            4*Power(t1,5)*Power(t2,2) - 114*Power(t2,3) - 
            176*t1*Power(t2,3) + 12*Power(t1,2)*Power(t2,3) + 
            72*Power(t1,3)*Power(t2,3) - 8*Power(t1,4)*Power(t2,3) + 
            77*Power(t2,4) + 74*t1*Power(t2,4) - 
            44*Power(t1,2)*Power(t2,4) + 4*Power(t1,3)*Power(t2,4) - 
            23*Power(t2,5) + 4*t1*Power(t2,5) + 
            Power(s2,5)*(-4 - 8*t1*(-1 + t2) + 4*t2 - 8*Power(t2,2)) + 
            Power(s2,4)*(t2 + 62*Power(t2,2) - 17*Power(t2,3) + 
               16*Power(t1,2)*(-2 + 3*t2) + 
               t1*(20 - 59*t2 + 12*Power(t2,2))) + 
            Power(s2,3)*(-33 + Power(t1,3)*(40 - 80*t2) + 80*t2 + 
               80*Power(t2,2) - 19*Power(t2,3) + 24*Power(t2,4) + 
               Power(t1,2)*(1 + 93*t2 + 15*Power(t2,2)) + 
               t1*(36 - 173*t2 - 93*Power(t2,2) + 12*Power(t2,3))) + 
            Power(s2,2)*(-51 - 21*t2 + 60*Power(t2,2) + 192*Power(t2,3) - 
               85*Power(t2,4) + 3*Power(t2,5) + 
               16*Power(t1,4)*(-1 + 3*t2) - 
               Power(t1,3)*(40 + 13*t2 + 22*Power(t2,2)) - 
               Power(t1,2)*(165 - 410*t2 + 25*Power(t2,2) + 
                  11*Power(t2,3)) + 
               t1*(204 - 172*t2 - 412*Power(t2,2) + 123*Power(t2,3) - 
                  14*Power(t2,4))) + 
            s2*(-4 - 145*t2 - 8*Power(t1,5)*t2 + 162*Power(t2,2) + 
               99*Power(t2,3) - 116*Power(t2,4) + 10*Power(t2,5) - 
               Power(t1,4)*(-9 + 21*t2 + Power(t2,2)) + 
               Power(t1,3)*(255 - 369*t2 + 108*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(-355 + 159*t2 + 443*Power(t2,2) - 
                  181*Power(t2,3) - 5*Power(t2,4)) + 
               t1*(127 + 260*t2 - 549*Power(t2,2) + 33*Power(t2,3) + 
                  84*Power(t2,4) - 2*Power(t2,5))) + 
            Power(s1,4)*(-93 + 15*Power(s2,3) + 8*Power(t1,3) + 
               Power(s2,2)*(12 + 22*t1 - 29*t2) - 6*t1*(-14 + t2) + 
               51*t2 + Power(t1,2)*(-29 + 6*t2) + 
               s2*(12 - 33*Power(t1,2) - 79*t2 + t1*(25 + 6*t2))) - 
            Power(s1,3)*(86 + 3*Power(s2,4) - 15*Power(t1,4) - 249*t2 + 
               46*Power(t2,2) - 7*Power(t1,2)*(13 + 20*t2) + 
               Power(t1,3)*(87 + 30*t2) + 
               Power(s2,3)*(19 - 46*t1 + 99*t2) + 
               t1*(-137 + 362*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(23 + 23*Power(t1,2) + 4*t2 - 
                  60*Power(t2,2) + 2*t1*(-5 + 4*t2)) + 
               s2*(1 + 27*Power(t1,3) - 88*t2 - 125*Power(t2,2) - 
                  4*Power(t1,2)*(32 + 23*t2) + 
                  t1*(175 + 138*t2 + 34*Power(t2,2)))) + 
            Power(s1,2)*(29 - 16*Power(s2,5) + 4*Power(t1,5) + 
               Power(s2,4)*(46 + 44*t1 - 19*t2) + 47*t2 - 
               142*Power(t2,2) - 28*Power(t2,3) - 
               Power(t1,4)*(23 + 34*t2) + 
               Power(t1,3)*(-147 + 226*t2 + 40*Power(t2,2)) - 
               Power(t1,2)*(-267 + 161*t2 + 237*Power(t2,2) + 
                  6*Power(t2,3)) + 
               2*t1*(-5 - 214*t2 + 273*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,3)*(28 - 27*Power(t1,2) + 7*t2 + 
                  177*Power(t2,2) - t1*(79 + 68*t2)) + 
               Power(s2,2)*(17 - 2*Power(t1,3) + 270*t2 - 
                  113*Power(t2,2) - 46*Power(t2,3) + 
                  Power(t1,2)*(42 + 39*t2) + 
                  t1*(-236 + 63*t2 - 64*Power(t2,2))) + 
               s2*(178 - 3*Power(t1,4) + 133*t2 - 328*Power(t2,2) - 
                  71*Power(t2,3) + Power(t1,3)*(38 + 58*t2) + 
                  Power(t1,2)*(311 - 365*t2 - 90*Power(t2,2)) + 
                  t1*(-473 + 299*t2 + 285*Power(t2,2) + 36*Power(t2,3)))) \
+ s1*(-44 + Power(t1,5)*(2 - 8*t2) - 31*t2 + 153*Power(t2,2) - 
               91*Power(t2,3) + 60*Power(t2,4) + 
               8*Power(s2,5)*(-1 + t1 + 3*t2) + 
               3*Power(t1,4)*(-38 + 18*t2 + 9*Power(t2,2)) + 
               Power(s2,4)*(1 - 48*Power(t1,2) + t1*(73 - 56*t2) - 
                  112*t2 + 39*Power(t2,2)) + 
               Power(t1,3)*(13 + 338*t2 - 211*Power(t2,2) - 
                  22*Power(t2,3)) + 
               Power(t1,2)*(200 - 497*t2 + 58*Power(t2,2) + 
                  170*Power(t2,3) + 3*Power(t2,4)) - 
               t1*(23 + 132*t2 - 467*Power(t2,2) + 342*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(s2,3)*(-91 + 80*Power(t1,3) - 100*t2 + 
                  31*Power(t2,2) - 117*Power(t2,3) + 
                  2*Power(t1,2)*(-53 + 6*t2) + 
                  t1*(141 + 176*t2 + 10*Power(t2,2))) + 
               Power(s2,2)*(-11 - 48*Power(t1,4) - 54*t2 - 
                  439*Power(t2,2) + 190*Power(t2,3) + 8*Power(t2,4) + 
                  Power(t1,3)*(11 + 24*t2) - 
                  Power(t1,2)*(328 + 10*t2 + 5*Power(t2,2)) + 
                  2*t1*(102 + 303*t2 - 98*Power(t2,2) + 32*Power(t2,3))) + 
               s2*(166 + 8*Power(t1,5) - 341*t2 - 231*Power(t2,2) + 
                  344*Power(t2,3) - 2*Power(t2,4) + 
                  4*Power(t1,4)*(7 + t2) + 
                  Power(t1,3)*(308 - 156*t2 - 47*Power(t2,2)) + 
                  Power(t1,2)*
                   (-183 - 711*t2 + 418*Power(t2,2) + 36*Power(t2,3)) - 
                  t1*(239 - 994*t2 + 157*Power(t2,2) + 256*Power(t2,3) + 
                     9*Power(t2,4)))))))/
     ((-1 + s2)*(-s + s2 - t1)*(1 - s + s2 - t1)*(-1 + t1)*
       (-1 + s1 + t1 - t2)*(-1 + s - s1 + t2)*(s - s1 + t2)*
       Power(-s1 + s2 - t1 + t2,2)) - 
    (8*(6 - 12*s2 - 2*Power(s2,2) + 2*t1 + 54*s2*t1 + 12*Power(s2,2)*t1 - 
         48*Power(t1,2) - 88*s2*Power(t1,2) - 
         24*Power(s2,2)*Power(t1,2) + 72*Power(t1,3) + 
         60*s2*Power(t1,3) + 20*Power(s2,2)*Power(t1,3) - 
         38*Power(t1,4) - 12*s2*Power(t1,4) - 6*Power(s2,2)*Power(t1,4) + 
         6*Power(t1,5) - 2*s2*Power(t1,5) + 
         Power(s1,6)*(-(Power(s2,2)*(-3 + t1)) - (-2 + t1)*Power(t1,2) + 
            s2*t1*(3 + 2*t1)) + 
         Power(s,6)*(t1 - t2)*(-1 + s1 + t1 - t2)*(1 - s2 + t1 - t2) - 
         18*s2*t2 - 31*Power(s2,2)*t2 + 53*t1*t2 + 121*s2*t1*t2 + 
         96*Power(s2,2)*t1*t2 + 12*Power(s2,3)*t1*t2 - 
         122*Power(t1,2)*t2 - 178*s2*Power(t1,2)*t2 - 
         94*Power(s2,2)*Power(t1,2)*t2 - 24*Power(s2,3)*Power(t1,2)*t2 + 
         80*Power(t1,3)*t2 + 64*s2*Power(t1,3)*t2 + 
         24*Power(s2,2)*Power(t1,3)*t2 + 12*Power(s2,3)*Power(t1,3)*t2 - 
         6*Power(t1,4)*t2 + 12*s2*Power(t1,4)*t2 + 
         5*Power(s2,2)*Power(t1,4)*t2 - 5*Power(t1,5)*t2 - 
         s2*Power(t1,5)*t2 - 37*Power(t2,2) - 62*s2*Power(t2,2) - 
         58*Power(s2,2)*Power(t2,2) - 36*Power(s2,3)*Power(t2,2) + 
         6*Power(s2,4)*Power(t2,2) + 119*t1*Power(t2,2) + 
         188*s2*t1*Power(t2,2) + 178*Power(s2,2)*t1*Power(t2,2) + 
         52*Power(s2,3)*t1*Power(t2,2) - 96*Power(t1,2)*Power(t2,2) - 
         132*s2*Power(t1,2)*Power(t2,2) - 
         84*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         12*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         6*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         4*Power(t1,3)*Power(t2,2) - 4*s2*Power(t1,3)*Power(t2,2) - 
         38*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         4*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         9*Power(t1,4)*Power(t2,2) + 10*s2*Power(t1,4)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         Power(t1,5)*Power(t2,2) - 29*Power(t2,3) - 55*s2*Power(t2,3) - 
         99*Power(s2,2)*Power(t2,3) - 27*Power(s2,3)*Power(t2,3) - 
         17*Power(s2,4)*Power(t2,3) + 4*Power(s2,5)*Power(t2,3) + 
         21*t1*Power(t2,3) + 99*s2*t1*Power(t2,3) + 
         60*Power(s2,2)*t1*Power(t2,3) + 55*Power(s2,3)*t1*Power(t2,3) + 
         18*Power(t1,2)*Power(t2,3) + 23*s2*Power(t1,2)*Power(t2,3) + 
         37*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         21*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         17*Power(t1,3)*Power(t2,3) - 21*s2*Power(t1,3)*Power(t2,3) - 
         6*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         Power(s2,3)*Power(t1,3)*Power(t2,3) - Power(t1,4)*Power(t2,3) - 
         2*s2*Power(t1,4)*Power(t2,3) + 13*Power(t2,4) - 
         33*s2*Power(t2,4) - 8*Power(s2,2)*Power(t2,4) - 
         25*Power(s2,3)*Power(t2,4) - Power(s2,4)*Power(t2,4) - 
         14*t1*Power(t2,4) - 24*s2*t1*Power(t2,4) - 
         31*Power(s2,2)*t1*Power(t2,4) - 26*Power(s2,3)*t1*Power(t2,4) - 
         Power(s2,4)*t1*Power(t2,4) + 13*Power(t1,2)*Power(t2,4) + 
         24*s2*Power(t1,2)*Power(t2,4) + 
         12*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         3*s2*Power(t1,3)*Power(t2,4) + 
         Power(s2,2)*Power(t1,3)*Power(t2,4) - 3*Power(t2,5) + 
         11*s2*Power(t2,5) + 11*Power(s2,2)*Power(t2,5) + 
         12*Power(s2,3)*Power(t2,5) - 4*t1*Power(t2,5) - 
         16*s2*t1*Power(t2,5) - 9*Power(s2,2)*t1*Power(t2,5) + 
         Power(t1,2)*Power(t2,5) - s2*Power(t1,2)*Power(t2,5) - 
         2*Power(s2,2)*Power(t1,2)*Power(t2,5) + 2*Power(t2,6) + 
         5*s2*Power(t2,6) + Power(s2,2)*Power(t2,6) - t1*Power(t2,6) + 
         Power(s2,2)*t1*Power(t2,6) - 
         Power(s1,5)*(2*Power(s2,3)*(5 + t1) + 
            Power(s2,2)*(5 - 3*Power(t1,2) + 16*t2 - 2*t1*(1 + 2*t2)) + 
            t1*(3 + Power(t1,3) + 4*t2 - Power(t1,2)*(1 + 4*t2) + 
               t1*(-9 + 6*t2)) + 
            s2*(3*(2 + t2) + Power(t1,2)*(-17 + 8*t2) + t1*(5 + 19*t2))) \
+ Power(s1,4)*(3 - Power(s2,4)*(1 + t1) + 3*Power(t1,4)*(-1 + t2) + 
            3*t2 + 2*Power(t2,2) + 
            4*Power(t1,2)*(-6 - 8*t2 + Power(t2,2)) + 
            3*t1*(2 - t2 + 5*Power(t2,2)) - 
            Power(t1,3)*(-30 + t2 + 6*Power(t2,2)) + 
            Power(s2,3)*(-24 + 52*t2 + t1*(-26 + 8*t2)) + 
            Power(s2,2)*(4 + 3*Power(t1,3) + Power(t1,2)*(4 - 12*t2) + 
               33*t2 + 35*Power(t2,2) - t1*(37 + 21*t2 + 5*Power(t2,2))) \
+ s2*(10 - 2*Power(t1,4) + 39*t2 + 17*Power(t2,2) + 
               Power(t1,3)*(18 + t2) + 
               Power(t1,2)*(5 - 67*t2 + 12*Power(t2,2)) + 
               t1*(-61 - 3*t2 + 46*Power(t2,2)))) - 
         Power(s1,3)*(5 + 4*Power(s2,5) + Power(t1,5) + 
            Power(s2,4)*(1 + t1)*(-9 + t1 - 4*t2) + 23*t2 + 
            6*Power(t2,2) + 8*Power(t2,3) + 
            Power(t1,4)*(-20 - 7*t2 + 3*Power(t2,2)) + 
            Power(t1,3)*(-10 + 71*t2 + 3*Power(t2,2) - 4*Power(t2,3)) - 
            Power(t1,2)*(-75 + 26*t2 + 41*Power(t2,2) + 4*Power(t2,3)) + 
            t1*(-59 - 13*t2 - 31*Power(t2,2) + 20*Power(t2,3)) + 
            Power(s2,3)*(-42 - 2*Power(t1,3) - 97*t2 + 
               108*Power(t2,2) + Power(t1,2)*(40 + t2) + 
               4*t1*(13 - 26*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(Power(t1,4) + Power(t1,3)*(-22 + 8*t2) + 
               Power(t1,2)*(61 + 26*t2 - 20*Power(t2,2)) - 
               4*t1*(-4 + 35*t2 + 15*Power(t2,2)) + 
               2*(-32 + t2 + 40*Power(t2,2) + 20*Power(t2,3))) + 
            s2*(-22 + Power(t1,4)*(3 - 5*t2) + 9*t2 + 92*Power(t2,2) + 
               38*Power(t2,3) + 
               Power(t1,3)*(-33 + 50*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(129 + 62*t2 - 100*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-33 - 236*t2 - 55*Power(t2,2) + 54*Power(t2,3)))) + 
         Power(s1,2)*(12*Power(s2,5)*t2 + 2*Power(t1,5)*t2 + 
            Power(t1,4)*(46 - 34*t2 - 5*Power(t2,2) + Power(t2,3)) + 
            Power(t1,2)*(102 + 158*t2 + 33*Power(t2,2) - 
               21*Power(t2,3) - 6*Power(t2,4)) - 
            Power(t1,3)*(138 + 45*t2 - 52*Power(t2,2) - 5*Power(t2,3) + 
               Power(t2,4)) + 
            2*(-8 - 12*t2 + 25*Power(t2,2) + 6*Power(t2,4)) + 
            t1*(6 - 81*t2 - 58*Power(t2,2) - 45*Power(t2,3) + 
               10*Power(t2,4)) + 
            Power(s2,4)*(6 + 3*Power(t1,2)*(-2 + t2) - 35*t2 - 
               6*Power(t2,2) - 2*t1*t2*(8 + 3*t2)) + 
            Power(s2,3)*(-30 - 115*t2 - 147*Power(t2,2) + 
               112*Power(t2,3) - Power(t1,3)*(2 + 5*t2) + 
               Power(t1,2)*(-10 + 97*t2 + 3*Power(t2,2)) + 
               t1*(42 + 167*t2 - 156*Power(t2,2) + 8*Power(t2,3))) - 
            s2*(56 + Power(t1,5) + 105*t2 + 45*Power(t2,2) - 
               102*Power(t2,3) - 42*Power(t2,4) + 
               Power(t1,4)*(-22 + 4*Power(t2,2)) + 
               Power(t1,3)*(64 + 89*t2 - 49*Power(t2,2) - 
                  3*Power(t2,3)) + 
               t1*(-137 - 35*t2 + 313*Power(t2,2) + 97*Power(t2,3) - 
                  31*Power(t2,4)) + 
               Power(t1,2)*(38 - 291*t2 - 133*Power(t2,2) + 
                  68*Power(t2,3) - 2*Power(t2,4))) + 
            Power(s2,2)*(-75 - 221*t2 - 16*Power(t2,2) + 
               92*Power(t2,3) + 25*Power(t2,4) + 
               Power(t1,4)*(1 + 2*t2) + 
               Power(t1,3)*(-50 - 40*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-74 + 145*t2 + 52*Power(t2,2) - 
                  18*Power(t2,3)) + 
               t1*(198 + 90*t2 - 200*Power(t2,2) - 74*Power(t2,3) + 
                  5*Power(t2,4)))) + 
         s1*(16 + 55*t2 + 58*Power(t2,2) - 12*Power(s2,5)*Power(t2,2) - 
            43*Power(t2,3) + 6*Power(t2,4) - 8*Power(t2,5) - 
            Power(t1,5)*(-11 + t2 + Power(t2,2)) + 
            Power(t1,4)*(-18 - 49*t2 + 15*Power(t2,2) + Power(t2,3)) + 
            Power(t1,3)*(-60 + 114*t2 + 52*Power(t2,2) - 
               11*Power(t2,3) - 2*Power(t2,4)) + 
            t1*(-95 - 137*t2 + Power(t2,2) + 53*Power(t2,3) + 
               24*Power(t2,4)) + 
            Power(t1,2)*(146 + 18*t2 - 101*Power(t2,2) - 
               48*Power(t2,3) + 2*Power(t2,4) + 2*Power(t2,5)) + 
            Power(s2,4)*t2*(-12 - 3*Power(t1,2)*(-4 + t2) + 43*t2 + 
               4*Power(t2,2) + 4*t1*t2*(2 + t2)) + 
            Power(s2,3)*(2*Power(t1,3)*(-6 + 3*t2 + 2*Power(t2,2)) + 
               t2*(66 + 100*t2 + 99*Power(t2,2) - 58*Power(t2,3)) + 
               Power(t1,2)*(24 + 22*t2 - 78*Power(t2,2) - 
                  3*Power(t2,3)) - 
               2*t1*(6 + 47*t2 + 85*Power(t2,2) - 52*Power(t2,3) + 
                  Power(t2,4))) - 
            Power(s2,2)*(-33 - 129*t2 - 256*Power(t2,2) - 
               22*Power(t2,3) + 51*Power(t2,4) + 8*Power(t2,5) + 
               Power(t1,4)*(3 + 3*t2 + Power(t2,2)) + 
               4*Power(t1,3)*
                (8 - 23*t2 - 6*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-106 - 146*t2 + 121*Power(t2,2) + 
                  42*Power(t2,3) - 9*Power(t2,4)) + 
               2*t1*(52 + 182*t2 + 67*Power(t2,2) - 64*Power(t2,3) - 
                  21*Power(t2,4) + 2*Power(t2,5))) + 
            s2*(28 + Power(t1,5)*(-1 + t2) + 124*t2 + 138*Power(t2,2) + 
               77*Power(t2,3) - 54*Power(t2,4) - 23*Power(t2,5) + 
               Power(t1,4)*(-10 - 34*t2 + 5*Power(t2,2) + Power(t2,3)) - 
               Power(t1,3)*(68 - 68*t2 - 77*Power(t2,2) + 
                  20*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(198 + 182*t2 - 185*Power(t2,2) - 
                  100*Power(t2,3) + 19*Power(t2,4)) - 
               t1*(147 + 341*t2 + 167*Power(t2,2) - 162*Power(t2,3) - 
                  66*Power(t2,4) + 7*Power(t2,5)))) + 
         Power(s,5)*(1 + 4*t1 - 3*Power(t1,2) - 5*Power(t1,3) + 
            Power(t1,4) - 8*t2 + 5*t1*t2 + 16*Power(t1,2)*t2 - 
            2*Power(t2,2) - 17*t1*Power(t2,2) - 
            6*Power(t1,2)*Power(t2,2) + 6*Power(t2,3) + 
            8*t1*Power(t2,3) - 3*Power(t2,4) + 
            Power(s2,2)*(2 + 4*Power(t1,2) + 5*t2 + 3*Power(t2,2) - 
               t1*(6 + 7*t2)) + 
            Power(s1,2)*(Power(s2,2) - 5*Power(t1,2) + 
               s2*(-1 + 5*t1 - 2*t2) - 3*(-1 + t2)*t2 + t1*(-6 + 8*t2)) \
+ s2*(-3 - 4*Power(t1,3) + 4*t2 + 10*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(8 + 9*t2) + t1*(1 - 18*t2 - 6*Power(t2,2))) \
+ s1*(-1 - 4*Power(t1,3) + Power(s2,2)*(-3 + 5*t1 - 4*t2) + 6*t2 - 
               9*Power(t2,2) + 6*Power(t2,3) + 
               Power(t1,2)*(-5 + 14*t2) + 
               t1*(1 + 14*t2 - 16*Power(t2,2)) + 
               s2*(Power(t1,2) + Power(-2 + t2,2) - 2*t1*(2 + t2)))) - 
         Power(s,4)*(3 + 17*t1 - 23*Power(t1,2) + 3*Power(t1,3) + 
            2*Power(t1,4) - 32*t2 + 57*t1*t2 - 2*Power(t1,2)*t2 + 
            6*Power(t1,3)*t2 - 3*Power(t1,4)*t2 - 16*Power(t2,2) - 
            13*t1*Power(t2,2) - 34*Power(t1,2)*Power(t2,2) + 
            6*Power(t1,3)*Power(t2,2) + 12*Power(t2,3) + 
            42*t1*Power(t2,3) - 16*Power(t2,4) - 6*t1*Power(t2,4) + 
            3*Power(t2,5) + Power(s1,3)*
             (3*Power(s2,2) - 10*Power(t1,2) + s2*(-3 + 10*t1) + 
               12*t1*(-1 + t2) - 3*(-1 + t2)*t2) + 
            Power(s2,3)*(4 + 5*Power(t1,2) + 7*t2 + 3*Power(t2,2) - 
               t1*(9 + 8*t2)) + 
            Power(s2,2)*(-6*Power(t1,3) + Power(t1,2)*(27 + 5*t2) + 
               t2*(28 + 27*t2 - 3*Power(t2,2)) + 
               t1*(-15 - 50*t2 + 4*Power(t2,2))) + 
            Power(s1,2)*(1 + 2*Power(s2,3) - 5*Power(t1,3) + 
               Power(s2,2)*(-4 + 18*t1 - 11*t2) + 14*t2 - 
               22*Power(t2,2) + 9*Power(t2,3) + 
               Power(t1,2)*(-17 + 26*t2) + 
               t1*(-13 + 55*t2 - 30*Power(t2,2)) - 
               s2*(-1 + 8*Power(t1,2) - 3*t1*(-2 + t2) + 5*t2 + 
                  8*Power(t2,2))) + 
            s2*(-7 + 2*Power(t1,4) + 4*Power(t1,3)*(-5 + t2) + t2 + 
               19*Power(t2,2) - 7*Power(t2,3) - 8*Power(t2,4) + 
               Power(t1,2)*(15 + 52*t2 - 22*Power(t2,2)) + 
               t1*(2 - 55*t2 - 25*Power(t2,2) + 24*Power(t2,3))) + 
            s1*(-4 + 5*Power(t1,4) + Power(s2,3)*(-6 + 7*t1 - 5*t2) + 
               21*t2 - 26*Power(t2,2) + 35*Power(t2,3) - 9*Power(t2,4) - 
               2*Power(t1,3)*(9 + 2*t2) + 
               Power(t1,2)*(-15 + 68*t2 - 16*Power(t2,2)) + 
               t1*(2 + 8*t2 - 85*Power(t2,2) + 24*Power(t2,3)) + 
               Power(s2,2)*(1 + 9*Power(t1,2) - 15*t2 + 
                  11*Power(t2,2) - t1*(5 + 24*t2)) + 
               s2*(9 - 16*Power(t1,3) - 6*t2 + 15*Power(t2,2) + 
                  16*Power(t2,3) + Power(t1,2)*(17 + 37*t2) + 
                  t1*(4 + 2*t2 - 37*Power(t2,2))))) + 
         Power(s,3)*(4 + 32*t1 - 78*Power(t1,2) + 49*Power(t1,3) - 
            12*Power(t1,4) + Power(t1,5) - 67*t2 + 157*t1*t2 - 
            116*Power(t1,2)*t2 + 42*Power(t1,3)*t2 - 8*Power(t1,4)*t2 - 
            35*Power(t2,2) + 10*t1*Power(t2,2) - 
            52*Power(t1,2)*Power(t2,2) + 10*Power(t1,3)*Power(t2,2) + 
            3*Power(t1,4)*Power(t2,2) + 25*Power(t2,3) + 
            48*t1*Power(t2,3) + 14*Power(t1,2)*Power(t2,3) - 
            8*Power(t1,3)*Power(t2,3) - 26*Power(t2,4) - 
            31*t1*Power(t2,4) + 6*Power(t1,2)*Power(t2,4) + 
            14*Power(t2,5) - Power(t2,6) + 
            Power(s1,4)*(3*Power(s2,2) - 10*t1 - 10*Power(t1,2) + t2 + 
               8*t1*t2 - Power(t2,2) + s2*(-3 + 10*t1 + 2*t2)) + 
            Power(s2,4)*(2 + 2*Power(t1,2) + 3*t2 + Power(t2,2) - 
               t1*(4 + 3*t2)) + 
            Power(s1,3)*(5 + 5*Power(s2,3) + 
               Power(s2,2)*(1 + 25*t1 - 8*t2) + 6*t2 - 17*Power(t2,2) + 
               4*Power(t2,3) + Power(t1,2)*(-23 + 24*t2) + 
               t1*(-27 + 74*t2 - 24*Power(t2,2)) + 
               s2*(-11 - 22*Power(t1,2) + t1*(-6 + t2) + 12*t2 - 
                  15*Power(t2,2))) + 
            Power(s2,3)*(7 - 3*Power(t1,3) + Power(t1,2)*(25 - 6*t2) + 
               42*t2 + 24*Power(t2,2) - 5*Power(t2,3) + 
               t1*(-25 - 40*t2 + 14*Power(t2,2))) + 
            Power(s2,2)*(7 + Power(t1,4) + 51*t2 + 7*Power(t2,2) - 
               60*Power(t2,3) - 5*Power(t2,4) + 
               2*Power(t1,3)*(-7 + 6*t2) + 
               Power(t1,2)*(46 + 3*t2 - 26*Power(t2,2)) + 
               t1*(-52 - 120*t2 + 65*Power(t2,2) + 18*Power(t2,3))) + 
            s2*(-20 + Power(t1,4)*(1 - 5*t2) - 52*t2 - 25*Power(t2,2) - 
               46*Power(t2,3) - 26*Power(t2,4) + 9*Power(t2,5) + 
               Power(t1,3)*(-7 + 31*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-23 + 7*t2 - 127*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(61 + 73*t2 + 80*Power(t2,2) + 121*Power(t2,3) - 
                  20*Power(t2,4))) + 
            Power(s1,2)*(-2 + Power(s2,4) + 10*Power(t1,4) + 
               Power(s2,3)*(-2 + 20*t1 - 15*t2) + 32*t2 - 
               38*Power(t2,2) + 45*Power(t2,3) - 6*Power(t2,4) - 
               Power(t1,3)*(25 + 16*t2) + 
               Power(t1,2)*(-12 + 95*t2 - 12*Power(t2,2)) + 
               Power(s2,2)*(1 + t1*(21 - 32*t2) - 60*t2 + 
                  2*Power(t2,2)) + 
               t1*(-23 + 54*t2 - 149*Power(t2,2) + 24*Power(t2,3)) + 
               s2*(2 - 24*Power(t1,3) - 9*t2 - 41*Power(t2,2) + 
                  33*Power(t2,3) + Power(t1,2)*(-12 + 65*t2) + 
                  t1*(10 + 103*t2 - 52*Power(t2,2)))) + 
            s1*(-9 + Power(t1,4)*(9 - 12*t2) + 
               Power(s2,4)*(-3 + 3*t1 - 2*t2) + 38*t2 - 62*Power(t2,2) + 
               58*Power(t2,3) - 43*Power(t2,4) + 4*Power(t2,5) + 
               Power(t1,3)*(3 + 4*t2 + 24*Power(t2,2)) - 
               Power(t1,2)*(25 - 76*t2 + 86*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(22 + 6*t2 - 75*Power(t2,2) + 116*Power(t2,3) - 
                  8*Power(t2,4)) + 
               Power(s2,3)*(-8 + 12*Power(t1,2) - 18*t2 + 
                  15*Power(t2,2) - 2*t1*(4 + 17*t2)) + 
               Power(s2,2)*(-18 - 21*Power(t1,3) + 6*t2 + 
                  119*Power(t2,2) + 8*Power(t2,3) + 
                  Power(t1,2)*(88 + 30*t2) + 
                  t1*(29 - 116*t2 - 11*Power(t2,2))) + 
               s2*(38 + 8*Power(t1,4) + 38*t2 + 66*Power(t2,2) + 
                  58*Power(t2,3) - 29*Power(t2,4) + 
                  Power(t1,3)*(-78 + 11*t2) + 
                  Power(t1,2)*(32 + 182*t2 - 51*Power(t2,2)) + 
                  t1*(-78 - 131*t2 - 218*Power(t2,2) + 61*Power(t2,3))))) \
- Power(s,2)*(5 + 28*t1 - 64*Power(t1,2) + 60*Power(t1,3) - 
            29*Power(t1,4) - 78*t2 + 150*t1*t2 - 88*Power(t1,2)*t2 + 
            60*Power(t1,3)*t2 + 14*Power(t1,4)*t2 - 2*Power(t1,5)*t2 - 
            5*Power(t2,2) - 97*t1*Power(t2,2) + 
            35*Power(t1,2)*Power(t2,2) - 59*Power(t1,3)*Power(t2,2) + 
            10*Power(t1,4)*Power(t2,2) + 73*Power(t2,3) - 
            15*t1*Power(t2,3) + 75*Power(t1,2)*Power(t2,3) - 
            18*Power(t1,3)*Power(t2,3) - Power(t1,4)*Power(t2,3) - 
            23*Power(t2,4) - 52*t1*Power(t2,4) + 
            10*Power(t1,2)*Power(t2,4) + 3*Power(t1,3)*Power(t2,4) + 
            22*Power(t2,5) + 4*t1*Power(t2,5) - 
            3*Power(t1,2)*Power(t2,5) - 4*Power(t2,6) + t1*Power(t2,6) + 
            Power(s1,5)*(Power(s2,2) + s2*(-1 + 5*t1 + t2) + 
               t1*(-3 - 5*t1 + 2*t2)) + 
            Power(s2,4)*(8 + Power(t1,2)*(8 - 5*t2) + 23*t2 + 
               11*Power(t2,2) - 2*Power(t2,3) + 
               t1*(-16 - 10*t2 + 7*Power(t2,2))) + 
            Power(s2,3)*(5 + 20*t2 - 46*Power(t2,2) - 54*Power(t2,3) + 
               Power(t2,4) + Power(t1,3)*(1 + 7*t2) + 
               Power(t1,2)*(19 - 38*t2 - 4*Power(t2,2)) - 
               t1*(25 + 49*t2 - 76*Power(t2,2) + 4*Power(t2,3))) - 
            Power(s2,2)*(-6 + 43*t2 + 155*Power(t2,2) + 
               94*Power(t2,3) - 37*Power(t2,4) - 7*Power(t2,5) + 
               2*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(-4 - 15*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(32 + 53*t2 + 42*Power(t2,2) - 
                  23*Power(t2,3)) + 
               t1*(-24 - 203*t2 - 246*Power(t2,2) + 4*Power(t2,3) + 
                  21*Power(t2,4))) + 
            s2*(-41 + Power(t1,5) - 84*t2 - 49*Power(t2,2) - 
               65*Power(t2,3) + 10*Power(t2,4) + 32*Power(t2,5) - 
               3*Power(t2,6) + Power(t1,4)*(-6 + 4*Power(t2,2)) - 
               3*Power(t1,3)*
                (-31 - 11*t2 + 5*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(-157 - 296*t2 - 91*Power(t2,2) + 
                  87*Power(t2,3) + 9*Power(t2,4)) + 
               t1*(110 + 223*t2 + 205*Power(t2,2) + 28*Power(t2,3) - 
                  105*Power(t2,4) + 2*Power(t2,5))) + 
            Power(s1,4)*(3 + 4*Power(s2,3) + 5*Power(t1,3) - t2 - 
               4*Power(t2,2) + Power(s2,2)*(-1 + 17*t1 + t2) + 
               Power(t1,2)*(-17 + 11*t2) + 
               t1*(-16 + 39*t2 - 7*Power(t2,2)) - 
               s2*(9 + 23*Power(t1,2) - 18*t2 + 7*Power(t2,2) + 
                  t1*(8 + 5*t2))) + 
            Power(s1,3)*(4 + 2*Power(s2,4) + 10*Power(t1,4) + 
               Power(s2,3)*(19 + 21*t1 - 13*t2) + 2*t2 + 
               56*Power(t1,2)*t2 - 19*Power(t2,2) + 16*Power(t2,3) - 
               Power(t1,3)*(17 + 24*t2) + 
               t1*(-49 + 91*t2 - 103*Power(t2,2) + 8*Power(t2,3)) - 
               Power(s2,2)*(-13 + 14*Power(t1,2) + 44*t2 + 
                  16*Power(t2,2) + 6*t1*(-5 + 4*t2)) + 
               s2*(-12 - 16*Power(t1,3) + 7*t2 - 80*Power(t2,2) + 
                  18*Power(t2,3) + Power(t1,2)*(-60 + 63*t2) + 
                  t1*(36 + 144*t2 - 17*Power(t2,2)))) + 
            Power(s1,2)*(-3 + Power(s2,4)*(3 + 7*t1 - 6*t2) + 91*t2 - 
               36*Power(t2,2) + 63*Power(t2,3) - 24*Power(t2,4) - 
               3*Power(t1,4)*(-5 + 6*t2) + 
               Power(s2,3)*(-9 + 9*Power(t1,2) + t1*(26 - 46*t2) - 
                  88*t2 + 15*Power(t2,2)) + 
               Power(t1,3)*(-33 - 9*t2 + 36*Power(t2,2)) + 
               Power(t1,2)*(6 + 138*t2 - 51*Power(t2,2) - 
                  16*Power(t2,3)) + 
               t1*(-53 + 8*t2 - 186*Power(t2,2) + 105*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,2)*(-81 - 27*Power(t1,3) - 116*t2 + 
                  128*Power(t2,2) + 32*Power(t2,3) + 
                  Power(t1,2)*(88 + 57*t2) - 
                  6*t1*(-17 + 15*t2 + 4*Power(t2,2))) + 
               s2*(30 + 12*Power(t1,4) - 5*t2 + 23*Power(t2,2) + 
                  142*Power(t2,3) - 22*Power(t2,4) + 
                  3*Power(t1,3)*(-38 + 3*t2) + 
                  Power(t1,2)*(70 + 267*t2 - 48*Power(t2,2)) + 
                  t1*(-24 - 109*t2 - 369*Power(t2,2) + 31*Power(t2,3)))) \
+ s1*(-13 + 3*Power(t1,5) + t2 - 168*Power(t2,2) + 54*Power(t2,3) - 
               65*Power(t2,4) + 16*Power(t2,5) + 
               Power(t1,4)*(-44 - 23*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(73 + 101*t2 + 44*Power(t2,2) - 
                  20*Power(t2,3)) + 
               Power(t1,2)*(-167 - 68*t2 - 213*Power(t2,2) + 
                  2*Power(t2,3) + 13*Power(t2,4)) + 
               t1*(92 + 173*t2 + 56*Power(t2,2) + 163*Power(t2,3) - 
                  42*Power(t2,4) - 2*Power(t2,5)) + 
               Power(s2,4)*(-7 + 5*Power(t1,2) - 14*t2 + 
                  6*Power(t2,2) - 2*t1*(3 + 7*t2)) + 
               Power(s2,3)*(-33 - 8*Power(t1,3) + 
                  Power(t1,2)*(83 - 5*t2) + 63*t2 + 123*Power(t2,2) - 
                  7*Power(t2,3) + t1*(18 - 110*t2 + 29*Power(t2,2))) + 
               Power(s2,2)*(68 + 3*Power(t1,4) + 244*t2 + 
                  197*Power(t2,2) - 120*Power(t2,3) - 25*Power(t2,4) + 
                  Power(t1,3)*(-53 + 32*t2) + 
                  Power(t1,2)*(71 - 14*t2 - 66*Power(t2,2)) + 
                  t1*(-209 - 386*t2 + 64*Power(t2,2) + 52*Power(t2,3))) \
+ s2*(39 + Power(t1,4)*(5 - 15*t2) + 21*t2 + 82*Power(t2,2) - 
                  31*Power(t2,3) - 111*Power(t2,4) + 13*Power(t2,5) + 
                  Power(t1,3)*(7 + 106*t2 + 19*Power(t2,2)) - 
                  Power(t1,2)*
                   (-180 - 50*t2 + 294*Power(t2,2) + Power(t2,3)) + 
                  t1*(-107 - 190*t2 + 45*Power(t2,2) + 338*Power(t2,3) - 
                     16*Power(t2,4))))) + 
         s*(-4 - 3*t1 + Power(s1,6)*(s2 - t1)*t1 + 28*Power(t1,2) - 
            38*Power(t1,3) + 24*Power(t1,4) - 7*Power(t1,5) - 34*t2 + 
            5*t1*t2 + 90*Power(t1,2)*t2 - 98*Power(t1,3)*t2 + 
            36*Power(t1,4)*t2 + Power(t1,5)*t2 + 53*Power(t2,2) + 
            4*Power(s2,5)*Power(t2,2) - 203*t1*Power(t2,2) + 
            190*Power(t1,2)*Power(t2,2) - 98*Power(t1,3)*Power(t2,2) - 
            3*Power(t1,4)*Power(t2,2) + Power(t1,5)*Power(t2,2) + 
            82*Power(t2,3) - 67*t1*Power(t2,3) + 
            65*Power(t1,2)*Power(t2,3) + 14*Power(t1,3)*Power(t2,3) - 
            4*Power(t1,4)*Power(t2,3) - 22*Power(t2,4) - 
            22*t1*Power(t2,4) - 17*Power(t1,2)*Power(t2,4) + 
            7*Power(t1,3)*Power(t2,4) + 14*Power(t2,5) + 
            10*t1*Power(t2,5) - 6*Power(t1,2)*Power(t2,5) - 
            5*Power(t2,6) + 2*t1*Power(t2,6) + 
            Power(s2,4)*t2*(2 - 38*t2 - 15*Power(t2,2) + Power(t2,3) + 
               2*Power(t1,2)*(-7 + 2*t2) + 
               t1*(12 + 14*t2 - 5*Power(t2,2))) + 
            Power(s2,3)*(-2 - 49*t2 - 48*Power(t2,2) - 13*Power(t2,3) + 
               35*Power(t2,4) + Power(t2,5) + 
               Power(t1,3)*(14 - 5*t2 - 5*Power(t2,2)) + 
               Power(t1,2)*(-30 - 23*t2 + 34*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(18 + 77*t2 + 107*Power(t2,2) - 53*Power(t2,3) - 
                  2*Power(t2,4))) + 
            Power(s2,2)*(-37 - 86*t2 - 95*Power(t2,2) + 88*Power(t2,3) + 
               78*Power(t2,4) - 8*Power(t2,5) - 2*Power(t2,6) + 
               Power(t1,4)*(1 + 4*t2 + Power(t2,2)) + 
               Power(t1,3)*(14 - 40*t2 - 7*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,2)*(-68 - 58*t2 + 62*Power(t2,2) + 
                  24*Power(t2,3) - 8*Power(t2,4)) + 
               t1*(90 + 180*t2 - 73*Power(t2,2) - 168*Power(t2,3) - 
                  14*Power(t2,4) + 7*Power(t2,5))) + 
            s2*(2 - Power(t1,5)*(-1 + t2) - 27*t2 + 15*Power(t2,2) + 
               31*Power(t2,3) + 81*Power(t2,4) + 20*Power(t2,5) - 
               10*Power(t2,6) - 
               Power(t1,4)*(-2 - 10*t2 + 3*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(38 - 57*t2 - 43*Power(t2,2) + 
                  7*Power(t2,3) + 4*Power(t2,4)) - 
               Power(t1,2)*(84 + 3*t2 - 262*Power(t2,2) - 
                  97*Power(t2,3) + 21*Power(t2,4) + 5*Power(t2,5)) + 
               t1*(41 + 78*t2 - 99*Power(t2,2) - 234*Power(t2,3) - 
                  76*Power(t2,4) + 28*Power(t2,5) + 2*Power(t2,6))) + 
            Power(s1,5)*(Power(s2,3) + 2*Power(s2,2)*(-3 + 3*t1 + t2) + 
               2*t1*(-1 + 2*Power(t1,2) + t1*(-4 + t2) + 3*t2) - 
               s2*(1 + 11*Power(t1,2) - 6*t2 + t1*(8 + 3*t2))) + 
            Power(s1,4)*(1 + Power(s2,4) + 5*Power(t1,4) + 
               Power(s2,3)*(25 + 10*t1 - 3*t2) - 5*t2 - 5*Power(t2,2) - 
               2*Power(t1,3)*(3 + 8*t2) + 
               t1*(-21 + 44*t2 - 22*Power(t2,2)) + 
               Power(t1,2)*(-9 + 19*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-12*Power(t1,2) + t1*(8 - 13*t2) + 
                  2*(8 + 6*t2 - 5*Power(t2,2))) + 
               s2*(-4*Power(t1,3) + (9 - 34*t2)*t2 + 
                  Power(t1,2)*(-56 + 34*t2) + 
                  t1*(34 + 80*t2 + 4*Power(t2,2)))) + 
            Power(s1,3)*(6 + Power(t1,4)*(11 - 12*t2) + 
               Power(s2,4)*(7 + 5*t1 - 4*t2) + 18*t2 + Power(t2,2) + 
               20*Power(t2,3) + 
               Power(s2,3)*(23 + 2*Power(t1,2) + t1*(51 - 28*t2) - 
                  110*t2 + 2*Power(t2,2)) + 
               3*Power(t1,3)*(-21 - 2*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(48 + 92*t2 - 3*Power(t2,2) - 
                  8*Power(t2,3)) + 
               2*t1*(-38 + 26*t2 - 65*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,2)*(-55 - 15*Power(t1,3) - 134*t2 + 
                  8*Power(t2,2) + 20*Power(t2,3) + 
                  Power(t1,2)*(23 + 44*t2) + 
                  t1*(93 - 2*t2 - 4*Power(t2,2))) + 
               s2*(-33 + 8*Power(t1,4) + Power(t1,3)*(-74 + t2) - 
                  64*t2 - 41*Power(t2,2) + 76*Power(t2,3) + 
                  Power(t1,2)*(48 + 204*t2 - 31*Power(t2,2)) - 
                  t1*(-103 + 51*t2 + 220*Power(t2,2) + 6*Power(t2,3)))) + 
            Power(s1,2)*(16 + 4*Power(s2,5) + 3*Power(t1,5) + 88*t2 - 
               61*Power(t2,2) + 27*Power(t2,3) - 30*Power(t2,4) + 
               Power(t1,4)*(-52 - 22*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(38 + 130*t2 + 37*Power(t2,2) - 
                  16*Power(t2,3)) + 
               t1*(-21 + 31*t2 - 63*Power(t2,2) + 146*Power(t2,3) - 
                  12*Power(t2,4)) + 
               Power(t1,2)*(-44 + 11*t2 - 174*Power(t2,2) - 
                  25*Power(t2,3) + 7*Power(t2,4)) + 
               Power(s2,4)*(-14 + 4*Power(t1,2) - 29*t2 + 
                  6*Power(t2,2) - 5*t1*(2 + 3*t2)) + 
               Power(s2,3)*(-92 - 7*Power(t1,3) - 51*t2 + 
                  180*Power(t2,2) + 2*Power(t2,3) + 
                  2*Power(t1,2)*(49 + t2) + 
                  t1*(89 - 163*t2 + 24*Power(t2,2))) + 
               Power(s2,2)*(-11 + 3*Power(t1,4) + 192*t2 + 
                  298*Power(t2,2) - 36*Power(t2,3) - 20*Power(t2,4) + 
                  Power(t1,3)*(-61 + 28*t2) + 
                  Power(t1,2)*(80 + 12*t2 - 60*Power(t2,2)) + 
                  t1*(-123 - 378*t2 - 34*Power(t2,2) + 30*Power(t2,3))) \
+ s2*(55 + Power(t1,4)*(7 - 15*t2) + 123*t2 + 209*Power(t2,2) + 
                  79*Power(t2,3) - 84*Power(t2,4) + 
                  Power(t1,3)*(-19 + 125*t2 + 14*Power(t2,2)) - 
                  Power(t1,2)*
                   (-312 - 41*t2 + 261*Power(t2,2) + Power(t2,3)) + 
                  t1*(-223 - 478*t2 - 76*Power(t2,2) + 260*Power(t2,3) + 
                     9*Power(t2,4)))) - 
            s1*(25 + 76*t2 + 8*Power(s2,5)*t2 + 4*Power(t1,5)*t2 + 
               176*Power(t2,2) - 64*Power(t2,3) + 37*Power(t2,4) - 
               20*Power(t2,5) + 
               Power(t1,4)*(59 - 48*t2 - 15*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,3)*(-180 - 92*t2 + 81*Power(t2,2) + 
                  32*Power(t2,3) - 4*Power(t2,4)) + 
               2*t1*(-76 - 128*t2 - 56*Power(t2,2) - 27*Power(t2,3) + 
                  34*Power(t2,4) + Power(t2,5)) + 
               Power(t1,2)*(248 + 196*t2 + 124*Power(t2,2) - 
                  108*Power(t2,3) - 23*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,4)*(2 - 52*t2 - 37*Power(t2,2) + 4*Power(t2,3) + 
                  2*Power(t1,2)*(-7 + 4*t2) + 
                  t1*(12 + 4*t2 - 15*Power(t2,2))) + 
               Power(s2,3)*(-51 - 144*t2 - 41*Power(t2,2) + 
                  130*Power(t2,3) + 3*Power(t2,4) - 
                  3*Power(t1,3)*(1 + 4*t2) + 
                  Power(t1,2)*(-29 + 128*t2 + 10*Power(t2,2)) + 
                  t1*(83 + 204*t2 - 165*Power(t2,2) + 4*Power(t2,3))) + 
               Power(s2,2)*(-83 - 112*t2 + 225*Power(t2,2) + 
                  258*Power(t2,3) - 30*Power(t2,4) - 10*Power(t2,5) + 
                  Power(t1,4)*(3 + 4*t2) + 
                  Power(t1,3)*(-48 - 58*t2 + 15*Power(t2,2)) + 
                  Power(t1,2)*
                   (-36 + 116*t2 + 59*Power(t2,2) - 36*Power(t2,3)) + 
                  t1*(164 - 174*t2 - 453*Power(t2,2) - 42*Power(t2,3) + 
                     26*Power(t2,4))) + 
               s2*(9 - 2*Power(t1,5) + 80*t2 + 121*Power(t2,2) + 
                  226*Power(t2,3) + 66*Power(t2,4) - 46*Power(t2,5) + 
                  Power(t1,4)*(28 - 8*Power(t2,2)) + 
                  Power(t1,3)*
                   (-141 - 56*t2 + 58*Power(t2,2) + 15*Power(t2,3)) + 
                  Power(t1,2)*
                   (151 + 584*t2 + 186*Power(t2,2) - 134*Power(t2,3) - 
                     14*Power(t2,4)) + 
                  t1*(-45 - 344*t2 - 609*Power(t2,2) - 169*Power(t2,3) + 
                     140*Power(t2,4) + 7*Power(t2,5))))))*
       B1(1 - s1 - t1 + t2,1 - s + s1 - t2,1 - s + s2 - t1))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s1 + t1 - t2)*
       Power(1 - s + s*s2 - s1*s2 - t1 + s2*t2,2)) - 
    (8*(-72 - 114*s2 + 73*Power(s2,2) + 162*Power(s2,3) + 
         40*Power(s2,4) + 28*Power(s2,5) + 41*Power(s2,6) - 
         6*Power(s2,8) + 342*t1 + 288*s2*t1 - 626*Power(s2,2)*t1 - 
         855*Power(s2,3)*t1 - 509*Power(s2,4)*t1 - 301*Power(s2,5)*t1 - 
         63*Power(s2,6)*t1 + 32*Power(s2,7)*t1 + 12*Power(s2,8)*t1 - 
         565*Power(t1,2) + 154*s2*Power(t1,2) + 
         2132*Power(s2,2)*Power(t1,2) + 2637*Power(s2,3)*Power(t1,2) + 
         1698*Power(s2,4)*Power(t1,2) + 471*Power(s2,5)*Power(t1,2) - 
         69*Power(s2,6)*Power(t1,2) - 64*Power(s2,7)*Power(t1,2) - 
         6*Power(s2,8)*Power(t1,2) + 286*Power(t1,3) - 
         1479*s2*Power(t1,3) - 4441*Power(s2,2)*Power(t1,3) - 
         4728*Power(s2,3)*Power(t1,3) - 2038*Power(s2,4)*Power(t1,3) - 
         67*Power(s2,5)*Power(t1,3) + 163*Power(s2,6)*Power(t1,3) + 
         32*Power(s2,7)*Power(t1,3) + 258*Power(t1,4) + 
         2777*s2*Power(t1,4) + 5778*Power(s2,2)*Power(t1,4) + 
         4262*Power(s2,3)*Power(t1,4) + 768*Power(s2,4)*Power(t1,4) - 
         215*Power(s2,5)*Power(t1,4) - 72*Power(s2,6)*Power(t1,4) - 
         504*Power(t1,5) - 2911*s2*Power(t1,5) - 
         4163*Power(s2,2)*Power(t1,5) - 1573*Power(s2,3)*Power(t1,5) + 
         91*Power(s2,4)*Power(t1,5) + 84*Power(s2,5)*Power(t1,5) + 
         411*Power(t1,6) + 1731*s2*Power(t1,6) + 
         1341*Power(s2,2)*Power(t1,6) + 83*Power(s2,3)*Power(t1,6) - 
         50*Power(s2,4)*Power(t1,6) - 200*Power(t1,7) - 
         470*s2*Power(t1,7) - 94*Power(s2,2)*Power(t1,7) + 
         12*Power(s2,3)*Power(t1,7) + 44*Power(t1,8) + 
         24*s2*Power(t1,8) + 2*Power(s1,7)*s2*
          (-2 + 5*Power(s2,3) + Power(s2,2)*(2 - 6*t1) + 5*t1 - 
            5*Power(t1,2) + 2*Power(t1,3) - s2*(5 - 5*t1 + Power(t1,2))) \
- 42*t2 - 85*s2*t2 + 150*Power(s2,2)*t2 + 662*Power(s2,3)*t2 + 
         692*Power(s2,4)*t2 + 233*Power(s2,5)*t2 + 50*Power(s2,6)*t2 + 
         36*Power(s2,7)*t2 - 10*Power(s2,8)*t2 - 6*Power(s2,9)*t2 - 
         111*t1*t2 - 534*s2*t1*t2 - 2327*Power(s2,2)*t1*t2 - 
         4325*Power(s2,3)*t1*t2 - 3236*Power(s2,4)*t1*t2 - 
         1109*Power(s2,5)*t1*t2 - 250*Power(s2,6)*t1*t2 + 
         52*Power(s2,7)*t1*t2 + 42*Power(s2,8)*t1*t2 + 
         6*Power(s2,9)*t1*t2 + 978*Power(t1,2)*t2 + 
         3605*s2*Power(t1,2)*t2 + 8886*Power(s2,2)*Power(t1,2)*t2 + 
         10860*Power(s2,3)*Power(t1,2)*t2 + 
         6160*Power(s2,4)*Power(t1,2)*t2 + 
         1703*Power(s2,5)*Power(t1,2)*t2 - 
         26*Power(s2,6)*Power(t1,2)*t2 - 160*Power(s2,7)*Power(t1,2)*t2 - 
         32*Power(s2,8)*Power(t1,2)*t2 - 2148*Power(t1,3)*t2 - 
         8129*s2*Power(t1,3)*t2 - 15288*Power(s2,2)*Power(t1,3)*t2 - 
         13274*Power(s2,3)*Power(t1,3)*t2 - 
         5408*Power(s2,4)*Power(t1,3)*t2 - 
         629*Power(s2,5)*Power(t1,3)*t2 + 
         310*Power(s2,6)*Power(t1,3)*t2 + 72*Power(s2,7)*Power(t1,3)*t2 + 
         2534*Power(t1,4)*t2 + 9492*s2*Power(t1,4)*t2 + 
         13258*Power(s2,2)*Power(t1,4)*t2 + 
         7912*Power(s2,3)*Power(t1,4)*t2 + 
         1812*Power(s2,4)*Power(t1,4)*t2 - 
         248*Power(s2,5)*Power(t1,4)*t2 - 84*Power(s2,6)*Power(t1,4)*t2 - 
         1905*Power(t1,5)*t2 - 5899*s2*Power(t1,5)*t2 - 
         5489*Power(s2,2)*Power(t1,5)*t2 - 
         1945*Power(s2,3)*Power(t1,5)*t2 - 8*Power(s2,4)*Power(t1,5)*t2 + 
         50*Power(s2,5)*Power(t1,5)*t2 + 860*Power(t1,6)*t2 + 
         1646*s2*Power(t1,6)*t2 + 850*Power(s2,2)*Power(t1,6)*t2 + 
         110*Power(s2,3)*Power(t1,6)*t2 - 12*Power(s2,4)*Power(t1,6)*t2 - 
         158*Power(t1,7)*t2 - 96*s2*Power(t1,7)*t2 - 
         40*Power(s2,2)*Power(t1,7)*t2 - 8*Power(t1,8)*t2 + 
         208*Power(t2,2) + 514*s2*Power(t2,2) + 
         852*Power(s2,2)*Power(t2,2) + 1329*Power(s2,3)*Power(t2,2) + 
         1374*Power(s2,4)*Power(t2,2) + 788*Power(s2,5)*Power(t2,2) + 
         189*Power(s2,6)*Power(t2,2) - Power(s2,7)*Power(t2,2) - 
         5*Power(s2,8)*Power(t2,2) - 10*Power(s2,9)*Power(t2,2) - 
         1094*t1*Power(t2,2) - 3492*s2*t1*Power(t2,2) - 
         6220*Power(s2,2)*t1*Power(t2,2) - 
         7382*Power(s2,3)*t1*Power(t2,2) - 
         5711*Power(s2,4)*t1*Power(t2,2) - 
         2449*Power(s2,5)*t1*Power(t2,2) - 
         359*Power(s2,6)*t1*Power(t2,2) + 64*Power(s2,7)*t1*Power(t2,2) + 
         69*Power(s2,8)*t1*Power(t2,2) + 2438*Power(t1,2)*Power(t2,2) + 
         9103*s2*Power(t1,2)*Power(t2,2) + 
         15075*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         14515*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         8535*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         2312*Power(s2,5)*Power(t1,2)*Power(t2,2) - 
         37*Power(s2,6)*Power(t1,2)*Power(t2,2) - 
         179*Power(s2,7)*Power(t1,2)*Power(t2,2) - 
         3216*Power(t1,3)*Power(t2,2) - 
         11846*s2*Power(t1,3)*Power(t2,2) - 
         16495*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         12788*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         5084*Power(s2,4)*Power(t1,3)*Power(t2,2) - 
         392*Power(s2,5)*Power(t1,3)*Power(t2,2) + 
         207*Power(s2,6)*Power(t1,3)*Power(t2,2) + 
         2695*Power(t1,4)*Power(t2,2) + 7773*s2*Power(t1,4)*Power(t2,2) + 
         8487*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         4804*Power(s2,3)*Power(t1,4)*Power(t2,2) + 
         798*Power(s2,4)*Power(t1,4)*Power(t2,2) - 
         87*Power(s2,5)*Power(t1,4)*Power(t2,2) - 
         1218*Power(t1,5)*Power(t2,2) - 2117*s2*Power(t1,5)*Power(t2,2) - 
         1757*Power(s2,2)*Power(t1,5)*Power(t2,2) - 
         538*Power(s2,3)*Power(t1,5)*Power(t2,2) - 
         16*Power(s2,4)*Power(t1,5)*Power(t2,2) + 
         141*Power(t1,6)*Power(t2,2) + 51*s2*Power(t1,6)*Power(t2,2) + 
         98*Power(s2,2)*Power(t1,6)*Power(t2,2) + 
         16*Power(s2,3)*Power(t1,6)*Power(t2,2) + 
         46*Power(t1,7)*Power(t2,2) + 12*s2*Power(t1,7)*Power(t2,2) + 
         142*Power(t2,3) + 908*s2*Power(t2,3) + 
         1693*Power(s2,2)*Power(t2,3) + 1707*Power(s2,3)*Power(t2,3) + 
         1411*Power(s2,4)*Power(t2,3) + 865*Power(s2,5)*Power(t2,3) + 
         251*Power(s2,6)*Power(t2,3) - 6*Power(s2,7)*Power(t2,3) - 
         19*Power(s2,8)*Power(t2,3) - 590*t1*Power(t2,3) - 
         3787*s2*t1*Power(t2,3) - 6832*Power(s2,2)*t1*Power(t2,3) - 
         6856*Power(s2,3)*t1*Power(t2,3) - 
         4732*Power(s2,4)*t1*Power(t2,3) - 
         1822*Power(s2,5)*t1*Power(t2,3) - 
         181*Power(s2,6)*t1*Power(t2,3) + 64*Power(s2,7)*t1*Power(t2,3) + 
         1228*Power(t1,2)*Power(t2,3) + 6076*s2*Power(t1,2)*Power(t2,3) + 
         9887*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         9173*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         4488*Power(s2,4)*Power(t1,2)*Power(t2,3) + 
         757*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         29*Power(s2,6)*Power(t1,2)*Power(t2,3) - 
         1339*Power(t1,3)*Power(t2,3) - 4288*s2*Power(t1,3)*Power(t2,3) - 
         6225*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         4332*Power(s2,3)*Power(t1,3)*Power(t2,3) - 
         1038*Power(s2,4)*Power(t1,3)*Power(t2,3) - 
         116*Power(s2,5)*Power(t1,3)*Power(t2,3) + 
         532*Power(t1,4)*Power(t2,3) + 946*s2*Power(t1,4)*Power(t2,3) + 
         1262*Power(s2,2)*Power(t1,4)*Power(t2,3) + 
         444*Power(s2,3)*Power(t1,4)*Power(t2,3) + 
         150*Power(s2,4)*Power(t1,4)*Power(t2,3) + 
         127*Power(t1,5)*Power(t2,3) + 211*s2*Power(t1,5)*Power(t2,3) + 
         78*Power(s2,2)*Power(t1,5)*Power(t2,3) - 
         46*Power(s2,3)*Power(t1,5)*Power(t2,3) - 
         100*Power(t1,6)*Power(t2,3) - 58*s2*Power(t1,6)*Power(t2,3) - 
         4*Power(s2,2)*Power(t1,6)*Power(t2,3) - 6*Power(t2,4) + 
         286*s2*Power(t2,4) + 1016*Power(s2,2)*Power(t2,4) + 
         1348*Power(s2,3)*Power(t2,4) + 888*Power(s2,4)*Power(t2,4) + 
         321*Power(s2,5)*Power(t2,4) + 68*Power(s2,6)*Power(t2,4) + 
         11*Power(s2,7)*Power(t2,4) - 98*t1*Power(t2,4) - 
         727*s2*t1*Power(t2,4) - 2446*Power(s2,2)*t1*Power(t2,4) - 
         3069*Power(s2,3)*t1*Power(t2,4) - 
         1462*Power(s2,4)*t1*Power(t2,4) - 
         279*Power(s2,5)*t1*Power(t2,4) - 75*Power(s2,6)*t1*Power(t2,4) + 
         147*Power(t1,2)*Power(t2,4) + 482*s2*Power(t1,2)*Power(t2,4) + 
         1875*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         1351*Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         265*Power(s2,4)*Power(t1,2)*Power(t2,4) + 
         188*Power(s2,5)*Power(t1,2)*Power(t2,4) + 
         132*Power(t1,3)*Power(t2,4) + 233*s2*Power(t1,3)*Power(t2,4) + 
         128*Power(s2,2)*Power(t1,3)*Power(t2,4) + 
         236*Power(s2,3)*Power(t1,3)*Power(t2,4) - 
         169*Power(s2,4)*Power(t1,3)*Power(t2,4) - 
         275*Power(t1,4)*Power(t2,4) - 398*s2*Power(t1,4)*Power(t2,4) - 
         388*Power(s2,2)*Power(t1,4)*Power(t2,4) + 
         33*Power(s2,3)*Power(t1,4)*Power(t2,4) + 
         100*Power(t1,5)*Power(t2,4) + 112*s2*Power(t1,5)*Power(t2,4) + 
         12*Power(s2,2)*Power(t1,5)*Power(t2,4) + 20*Power(t2,5) - 
         77*s2*Power(t2,5) + 21*Power(s2,2)*Power(t2,5) + 
         293*Power(s2,3)*Power(t2,5) + 176*Power(s2,4)*Power(t2,5) + 
         14*Power(s2,5)*Power(t2,5) + 9*Power(s2,6)*Power(t2,5) + 
         13*t1*Power(t2,5) + 149*s2*t1*Power(t2,5) + 
         87*Power(s2,2)*t1*Power(t2,5) + 200*Power(s2,3)*t1*Power(t2,5) + 
         113*Power(s2,4)*t1*Power(t2,5) - 58*Power(s2,5)*t1*Power(t2,5) - 
         132*Power(t1,2)*Power(t2,5) - 249*s2*Power(t1,2)*Power(t2,5) - 
         609*Power(s2,2)*Power(t1,2)*Power(t2,5) - 
         415*Power(s2,3)*Power(t1,2)*Power(t2,5) + 
         59*Power(s2,4)*Power(t1,2)*Power(t2,5) + 
         143*Power(t1,3)*Power(t2,5) + 291*s2*Power(t1,3)*Power(t2,5) + 
         376*Power(s2,2)*Power(t1,3)*Power(t2,5) + 
         2*Power(s2,3)*Power(t1,3)*Power(t2,5) - 
         44*Power(t1,4)*Power(t2,5) - 106*s2*Power(t1,4)*Power(t2,5) - 
         12*Power(s2,2)*Power(t1,4)*Power(t2,5) - 10*Power(t2,6) + 
         30*s2*Power(t2,6) - 59*Power(s2,2)*Power(t2,6) - 
         167*Power(s2,3)*Power(t2,6) - 55*Power(s2,4)*Power(t2,6) + 
         11*Power(s2,5)*Power(t2,6) + 26*t1*Power(t2,6) + 
         25*s2*t1*Power(t2,6) + 226*Power(s2,2)*t1*Power(t2,6) + 
         141*Power(s2,3)*t1*Power(t2,6) - 8*Power(s2,4)*t1*Power(t2,6) - 
         22*Power(t1,2)*Power(t2,6) - 105*s2*Power(t1,2)*Power(t2,6) - 
         124*Power(s2,2)*Power(t1,2)*Power(t2,6) - 
         7*Power(s2,3)*Power(t1,2)*Power(t2,6) + 
         6*Power(t1,3)*Power(t2,6) + 48*s2*Power(t1,3)*Power(t2,6) + 
         4*Power(s2,2)*Power(t1,3)*Power(t2,6) - 14*s2*Power(t2,7) - 
         6*Power(s2,2)*Power(t2,7) + 6*Power(s2,3)*Power(t2,7) - 
         2*Power(s2,4)*Power(t2,7) + 22*s2*t1*Power(t2,7) + 
         2*Power(s2,3)*t1*Power(t2,7) - 8*s2*Power(t1,2)*Power(t2,7) - 
         2*Power(s,7)*(t1 - t2)*
          (-6 + 2*t1 + 2*Power(t1,2) + Power(t1,3) + 
            Power(s2,2)*(-7 + 6*t1 - 6*t2) - 2*t2 - 4*t1*t2 - 
            3*Power(t1,2)*t2 + 2*Power(t2,2) + 3*t1*Power(t2,2) - 
            Power(t2,3) - Power(s1,2)*(-1 + s2 - t1 + t2) + 
            s2*(13 - 7*t1 - 4*Power(t1,2) + 7*t2 + 8*t1*t2 - 
               4*Power(t2,2)) + 
            s1*(5 + 7*Power(s2,2) + 3*t1 + 2*Power(t1,2) - 3*t2 - 
               4*t1*t2 + 2*Power(t2,2) + s2*(-12 - 5*t1 + 5*t2))) + 
         Power(s1,6)*(-7*Power(s2,5) - 
            2*Power(-1 + t1,2)*
             (-2 + 3*t1 - 4*Power(t1,2) + Power(t1,3)) + 
            Power(s2,4)*(2 + 71*t1 - 78*t2) + 
            Power(s2,2)*(15*Power(t1,3) + t1*(10 - 28*t2) - 
               4*Power(t1,2)*(-4 + 5*t2) + 6*(-5 + 9*t2)) + 
            s2*(4 + 16*Power(t1,4) + 18*t2 + 
               11*Power(t1,2)*(5 + 4*t2) - Power(t1,3)*(55 + 16*t2) - 
               2*t1*(11 + 23*t2)) + 
            Power(s2,3)*(-23 - 93*Power(t1,2) - 42*t2 + t1*(41 + 114*t2))\
) - Power(s1,5)*(Power(s2,6) + Power(s2,5)*(-3 + 19*t1 - 28*t2) + 
            (-1 + t1)*(6 + 4*Power(t1,5) - 14*t2 - 
               2*Power(t1,4)*(11 + 4*t2) + t1*(2 + 28*t2) + 
               Power(t1,3)*(39 + 32*t2) - Power(t1,2)*(27 + 38*t2)) - 
            Power(s2,4)*(17 + 153*Power(t1,2) + 43*t2 + 
               242*Power(t2,2) - t1*(66 + 409*t2)) + 
            s2*(-18*Power(t1,5) + Power(t1,4)*(17 + 46*t2) + 
               t1*(103 + 41*t2 - 58*Power(t2,2)) + 
               Power(t1,3)*(111 - 113*t2 - 20*Power(t2,2)) + 
               2*(-18 + 5*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-169 + 4*t2 + 62*Power(t2,2))) + 
            Power(s2,2)*(-8 - 64*Power(t1,4) - 291*t2 + 
               114*Power(t2,2) + Power(t1,3)*(232 + 195*t2) + 
               2*t1*(163 + 226*t2 + 5*Power(t2,2)) - 
               Power(t1,2)*(563 + 254*t2 + 130*Power(t2,2))) + 
            Power(s2,3)*(13 + 211*Power(t1,3) - 314*t2 - 
               144*Power(t2,2) - 2*Power(t1,2)*(155 + 307*t2) + 
               t1*(346 + 460*t2 + 392*Power(t2,2)))) + 
         Power(s1,4)*(17*Power(s2,7) + Power(s2,6)*(75 - 62*t1 + 5*t2) + 
            Power(s2,5)*(150 + 39*Power(t1,2) - 54*t2 - 
               31*Power(t2,2) + 3*t1*(-65 + 22*t2)) - 
            (-1 + t1)*(-10 + 2*Power(t1,6) - 28*t2 + 6*Power(t2,2) - 
               4*Power(t1,5)*(2 + 3*t2) + 
               4*Power(t1,4)*(-7 + 10*t2 + 3*Power(t2,2)) + 
               t1*(83 + 73*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(-197 - 55*t2 + 6*Power(t2,2)) - 
               2*Power(t1,3)*(-83 + 14*t2 + 14*Power(t2,2))) + 
            Power(s2,4)*(79 + 161*Power(t1,3) + 14*t2 - 
               247*Power(t2,2) - 390*Power(t2,3) - 
               Power(t1,2)*(12 + 707*t2) + 
               t1*(-234 + 569*t2 + 918*Power(t2,2))) + 
            s2*(57 - 2*Power(t1,6) + Power(t1,5)*(157 - 22*t2) - 
               265*t2 + 30*Power(t2,2) - 50*Power(t2,3) + 
               Power(t1,4)*(-719 - 314*t2 + 24*Power(t2,2)) + 
               Power(t1,3)*(1305 + 1259*t2 + 146*Power(t2,2)) - 
               Power(t1,2)*(1114 + 1389*t2 + 639*Power(t2,2)) + 
               t1*(304 + 771*t2 + 409*Power(t2,2) + 50*Power(t2,3))) + 
            Power(s2,2)*(70 + 115*Power(t1,5) - 33*t2 - 
               923*Power(t2,2) + 110*Power(t2,3) - 
               Power(t1,4)*(649 + 438*t2) + 
               Power(t1,3)*(1603 + 1820*t2 + 634*Power(t2,2)) + 
               t1*(401 + 1695*t2 + 1934*Power(t2,2) + 
                  120*Power(t2,3)) - 
               Power(t1,2)*(1387 + 3489*t2 + 1300*Power(t2,2) + 
                  280*Power(t2,3))) + 
            Power(s2,3)*(-10 - 266*Power(t1,4) + 297*t2 - 
               1193*Power(t2,2) - 230*Power(t2,3) + 
               2*Power(t1,3)*(315 + 542*t2) - 
               Power(t1,2)*(738 + 2083*t2 + 1533*Power(t2,2)) + 
               t1*(65 + 1894*t2 + 1571*Power(t2,2) + 670*Power(t2,3)))) - 
         Power(s1,3)*(9*Power(s2,8) + Power(s2,7)*(57 - 95*t1 + 90*t2) + 
            Power(s2,6)*(188 + 303*Power(t1,2) + 359*t2 + 
               18*Power(t2,2) - t1*(446 + 383*t2)) + 
            Power(s2,5)*(420 - 407*Power(t1,3) + 699*t2 - 
               130*Power(t2,2) + 16*Power(t2,3) + 
               Power(t1,2)*(1016 + 499*t2) + 
               t1*(-1073 - 986*t2 + 26*Power(t2,2))) - 
            (-1 + t1)*(4*Power(t1,6)*(-3 + t2) + 
               Power(t1,5)*(133 + 20*t2 - 12*Power(t2,2)) + 
               2*Power(t1,4)*
                (-232 - 133*t2 - 6*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(603 + 739*t2 + 194*Power(t2,2) + 
                  8*Power(t2,3)) - 
               4*(-13 + 7*t2 + 7*Power(t2,2) + 9*Power(t2,3)) - 
               Power(t1,2)*(351 + 592*t2 + 426*Power(t2,2) + 
                  76*Power(t2,3)) + 
               t1*(27 + 155*t2 + 264*Power(t2,2) + 96*Power(t2,3))) + 
            Power(s2,4)*(490 + 192*Power(t1,4) + 803*t2 + 
               320*Power(t2,2) - 518*Power(t2,3) - 350*Power(t2,4) + 
               Power(t1,3)*(-787 + 208*t2) + 
               Power(t1,2)*(1717 + 137*t2 - 1144*Power(t2,2)) + 
               t1*(-1633 - 1588*t2 + 1424*Power(t2,2) + 
                  1002*Power(t2,3))) + 
            s2*(33 + 8*Power(t1,7) + 565*t2 - 656*Power(t2,2) + 
               100*Power(t2,3) - 140*Power(t2,4) - 
               15*Power(t1,6)*(15 + 2*t2) + 
               Power(t1,5)*(1040 + 843*t2 + 42*Power(t2,2)) - 
               Power(t1,4)*(2158 + 3177*t2 + 1150*Power(t2,2) + 
                  44*Power(t2,3)) + 
               Power(t1,3)*(2163 + 4702*t2 + 3402*Power(t2,2) + 
                  614*Power(t2,3) + 20*Power(t2,4)) - 
               2*Power(t1,2)*
                (443 + 1457*t2 + 1701*Power(t2,2) + 748*Power(t2,3) + 
                  45*Power(t2,4)) + 
               t1*(33 - 37*t2 + 1844*Power(t2,2) + 786*Power(t2,3) + 
                  210*Power(t2,4))) + 
            Power(s2,2)*(-101 - 60*Power(t1,6) + 1326*t2 - 
               30*Power(t2,2) - 1382*Power(t2,3) + 30*Power(t2,4) + 
               Power(t1,5)*(611 + 427*t2) - 
               Power(t1,4)*(1559 + 2867*t2 + 942*Power(t2,2)) + 
               Power(t1,3)*(1089 + 6127*t2 + 4444*Power(t2,2) + 
                  886*Power(t2,3)) + 
               t1*(-256 - 1091*t2 + 3216*Power(t2,2) + 
                  3416*Power(t2,3) + 170*Power(t2,4)) - 
               Power(t1,2)*(-147 + 3266*t2 + 7698*Power(t2,2) + 
                  2340*Power(t2,3) + 290*Power(t2,4))) + 
            Power(s2,3)*(50*Power(t1,5) - Power(t1,4)*(242 + 807*t2) + 
               2*Power(t1,3)*(-109 + 1296*t2 + 994*Power(t2,2)) - 
               Power(t1,2)*(-1729 + 2055*t2 + 4804*Power(t2,2) + 
                  1852*Power(t2,3)) + 
               2*(62 + 553*t2 + 553*Power(t2,2) - 1046*Power(t2,3) - 
                  90*Power(t2,4)) + 
               t1*(-1273 - 1922*t2 + 3806*Power(t2,2) + 
                  2504*Power(t2,3) + 620*Power(t2,4)))) + 
         Power(s1,2)*(-10*Power(s2,9) + Power(s2,8)*(15 + 49*t1 - t2) - 
            2*Power(s2,7)*(-46 + 71*t1 + 33*Power(t1,2) - 68*t2 + 
               77*t1*t2 - 70*Power(t2,2)) + 
            Power(s2,6)*(174 - 48*Power(t1,3) + 729*t2 + 
               561*Power(t2,2) + 34*Power(t2,3) + 
               Power(t1,2)*(670 + 707*t2) - 
               t1*(796 + 1305*t2 + 655*Power(t2,2))) + 
            Power(s2,5)*(419 + 206*Power(t1,4) + 1745*t2 + 
               1269*Power(t2,2) - 96*Power(t2,3) + 59*Power(t2,4) - 
               Power(t1,3)*(1497 + 1168*t2) + 
               Power(t1,2)*(2838 + 3361*t2 + 1069*Power(t2,2)) - 
               2*t1*(897 + 2171*t2 + 833*Power(t2,2) + 64*Power(t2,3))) \
- (-1 + t1)*(-26 + 8*Power(t1,7) + 318*t2 - 32*Power(t2,2) + 
               24*Power(t2,3) - 64*Power(t2,4) + 
               2*Power(t1,6)*(-57 - 19*t2 + Power(t2,2)) + 
               Power(t1,5)*(363 + 396*t2 + 64*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(t1,2)*(88 + 260*t2 - 550*Power(t2,2) - 
                  678*Power(t2,3) - 94*Power(t2,4)) + 
               Power(t1,4)*(-389 - 995*t2 - 548*Power(t2,2) - 
                  56*Power(t2,3) + 2*Power(t2,4)) + 
               Power(t1,3)*(121 + 627*t2 + 1155*Power(t2,2) + 
                  360*Power(t2,3) + 22*Power(t2,4)) + 
               t1*(-43 - 604*t2 - 43*Power(t2,2) + 334*Power(t2,3) + 
                  134*Power(t2,4))) + 
            Power(s2,4)*(742 - 195*Power(t1,5) + 2163*t2 + 
               2257*Power(t2,2) + 706*Power(t2,3) - 532*Power(t2,4) - 
               170*Power(t2,5) + Power(t1,4)*(1605 + 748*t2) - 
               Power(t1,3)*(4535 + 3196*t2 + 236*Power(t2,2)) + 
               Power(t1,2)*(5248 + 8164*t2 + 575*Power(t2,2) - 
                  720*Power(t2,3)) + 
               t1*(-2969 - 7642*t2 - 3936*Power(t2,2) + 
                  1518*Power(t2,3) + 523*Power(t2,4))) - 
            Power(s2,2)*(187 + 6*Power(t1,7) - 1469*t2 - 
               3458*Power(t2,2) - 28*Power(t2,3) + 1068*Power(t2,4) + 
               30*Power(t2,5) + 4*Power(t1,6)*(4 + 27*t2) + 
               Power(t1,5)*(222 - 1382*t2 - 521*Power(t2,2)) + 
               Power(t1,4)*(-1793 + 2442*t2 + 4175*Power(t2,2) + 
                  838*Power(t2,3)) + 
               Power(t1,3)*(4140 + 2731*t2 - 7573*Power(t2,2) - 
                  4732*Power(t2,3) - 579*Power(t2,4)) + 
               t1*(972 + 6874*t2 + 5831*Power(t2,2) - 
                  2738*Power(t2,3) - 3014*Power(t2,4) - 100*Power(t2,5)\
) + Power(t1,2)*(-3790 - 8905*t2 + 496*Power(t2,2) + 7790*Power(t2,3) + 
                  2000*Power(t2,4) + 148*Power(t2,5))) + 
            s2*(-251 + 1128*t2 + 1245*Power(t2,2) - 738*Power(t2,3) + 
               160*Power(t2,4) - 146*Power(t2,5) + 
               2*Power(t1,7)*(39 + 8*t2) - 
               Power(t1,6)*(515 + 582*t2 + 54*Power(t2,2)) + 
               Power(t1,5)*(1064 + 2565*t2 + 1327*Power(t2,2) + 
                  78*Power(t2,3)) - 
               Power(t1,4)*(268 + 3864*t2 + 4595*Power(t2,2) + 
                  1396*Power(t2,3) + 56*Power(t2,4)) + 
               Power(t1,3)*(-1259 + 406*t2 + 5722*Power(t2,2) + 
                  3762*Power(t2,3) + 661*Power(t2,4) + 16*Power(t2,5)) \
- Power(t1,2)*(-985 - 4478*t2 + 2004*Power(t2,2) + 3562*Power(t2,3) + 
                  1439*Power(t2,4) + 100*Power(t2,5)) + 
               t1*(164 - 4123*t2 - 1713*Power(t2,2) + 
                  1936*Power(t2,3) + 644*Power(t2,4) + 230*Power(t2,5))) \
+ Power(s2,3)*(470 + 70*Power(t1,6) + 1657*t2 + 3590*Power(t2,2) + 
               1666*Power(t2,3) - 1913*Power(t2,4) - 54*Power(t2,5) - 
               5*Power(t1,5)*(141 + 8*t2) + 
               Power(t1,4)*(3028 + 154*t2 - 783*Power(t2,2)) + 
               2*Power(t1,3)*
                (-2996 - 2191*t2 + 1765*Power(t2,2) + 785*Power(t2,3)) + 
               Power(t1,2)*(5962 + 11327*t2 - 545*Power(t2,2) - 
                  5014*Power(t2,3) - 1103*Power(t2,4)) + 
               t1*(-2877 - 8286*t2 - 7108*Power(t2,2) + 
                  3514*Power(t2,3) + 2071*Power(t2,4) + 302*Power(t2,5)))\
) - s1*(Power(s2,9)*(-6 + 6*t1 - 20*t2) + 
            Power(s2,8)*(-10 - 32*Power(t1,2) + 10*t2 - 
               29*Power(t2,2) + 2*t1*(21 + 59*t2)) + 
            Power(s2,7)*(56 + 72*Power(t1,3) + 95*t2 + 73*Power(t2,2) + 
               78*Power(t2,3) - Power(t1,2)*(140 + 241*t2) + 
               t1*(12 - 86*t2 + 5*Power(t2,2))) + 
            Power(s2,6)*(111 - 84*Power(t1,4) + 389*t2 + 
               792*Power(t2,2) + 345*Power(t2,3) + 29*Power(t2,4) + 
               Power(t1,3)*(193 + 141*t2) + 
               Power(t1,2)*(269 + 695*t2 + 375*Power(t2,2)) - 
               t1*(489 + 1225*t2 + 1040*Power(t2,2) + 409*Power(t2,3))) \
+ Power(s2,5)*(157 + 50*Power(t1,5) + 1263*t2 + 2190*Power(t2,2) + 
               1041*Power(t2,3) - 3*Power(t2,4) + 44*Power(t2,5) + 
               Power(t1,4)*(25 + 151*t2) - 
               Power(t1,3)*(1448 + 2057*t2 + 877*Power(t2,2)) + 
               Power(t1,2)*(2446 + 5446*t2 + 3102*Power(t2,2) + 
                  797*Power(t2,3)) - 
               t1*(1230 + 4459*t2 + 5091*Power(t2,2) + 
                  1154*Power(t2,3) + 165*Power(t2,4))) - 
            (-1 + t1)*(-150 + 218*t2 + 408*Power(t2,2) - 
               20*Power(t2,3) + 50*Power(t2,4) - 42*Power(t2,5) + 
               4*Power(t1,7)*(7 + 2*t2) - 
               2*Power(t1,6)*(22 + 80*t2 + 13*Power(t2,2)) + 
               Power(t1,5)*(-121 + 202*t2 + 363*Power(t2,2) + 
                  36*Power(t2,3)) + 
               Power(t1,4)*(420 + 606*t2 - 558*Power(t2,2) - 
                  410*Power(t2,3) - 26*Power(t2,4)) + 
               Power(t1,2)*(18 + 1848*t2 + 1391*Power(t2,2) - 
                  112*Power(t2,3) - 433*Power(t2,4) - 42*Power(t2,5)) + 
               Power(t1,3)*(-468 - 1627*t2 - 535*Power(t2,2) + 
                  757*Power(t2,3) + 221*Power(t2,4) + 8*Power(t2,5)) + 
               t1*(315 - 1079*t2 - 1079*Power(t2,2) - 219*Power(t2,3) + 
                  178*Power(t2,4) + 76*Power(t2,5))) + 
            Power(s2,4)*(410 - 12*Power(t1,6) + 2102*t2 + 
               3084*Power(t2,2) + 2421*Power(t2,3) + 593*Power(t2,4) - 
               271*Power(t2,5) - 38*Power(t2,6) - 
               Power(t1,5)*(333 + 239*t2) + 
               Power(t1,4)*(2884 + 2621*t2 + 706*Power(t2,2)) - 
               Power(t1,3)*(6040 + 10131*t2 + 3447*Power(t2,2) + 
                  452*Power(t2,3)) + 
               Power(t1,2)*(5226 + 14251*t2 + 10935*Power(t2,2) + 
                  691*Power(t2,3) - 71*Power(t2,4)) + 
               t1*(-2135 - 8812*t2 - 10741*Power(t2,2) - 
                  4044*Power(t2,3) + 710*Power(t2,4) + 93*Power(t2,5))) \
+ Power(s2,2)*(15 + 601*t2 + 3263*Power(t2,2) + 3218*Power(t2,3) + 
               54*Power(t2,4) - 407*Power(t2,5) - 26*Power(t2,6) - 
               2*Power(t1,7)*(53 + 4*t2) + 
               Power(t1,6)*(949 + 130*t2 - 52*Power(t2,2)) + 
               Power(t1,5)*(-3780 - 2137*t2 + 849*Power(t2,2) + 
                  221*Power(t2,3)) + 
               Power(t1,4)*(7564 + 10194*t2 + 379*Power(t2,2) - 
                  2345*Power(t2,3) - 282*Power(t2,4)) + 
               Power(t1,3)*(-8237 - 19847*t2 - 10045*Power(t2,2) + 
                  3177*Power(t2,3) + 2252*Power(t2,4) + 151*Power(t2,5)\
) + Power(t1,2)*(4796 + 17887*t2 + 18645*Power(t2,2) + 
                  3258*Power(t2,3) - 3627*Power(t2,4) - 
                  814*Power(t2,5) - 30*Power(t2,6)) + 
               t1*(-1201 - 6740*t2 - 13450*Power(t2,2) - 
                  6785*Power(t2,3) + 978*Power(t2,4) + 
                  1316*Power(t2,5) + 22*Power(t2,6))) + 
            Power(s2,3)*(470 + 1669*t2 + 3240*Power(t2,2) + 
               3822*Power(t2,3) + 1137*Power(t2,4) - 890*Power(t2,5) + 
               8*Power(t2,6) + Power(t1,6)*(317 + 98*t2) - 
               Power(t1,5)*(2592 + 1389*t2 + 136*Power(t2,2)) + 
               Power(t1,4)*(7160 + 8250*t2 + 840*Power(t2,2) - 
                  209*Power(t2,3)) + 
               Power(t1,3)*(-9354 - 19072*t2 - 8496*Power(t2,2) + 
                  1804*Power(t2,3) + 457*Power(t2,4)) + 
               Power(t1,2)*(6637 + 20201*t2 + 18771*Power(t2,2) + 
                  2123*Power(t2,3) - 2398*Power(t2,4) - 270*Power(t2,5)\
) + t1*(-2638 - 9845*t2 - 13869*Power(t2,2) - 8190*Power(t2,3) + 
                  1456*Power(t2,4) + 860*Power(t2,5) + 64*Power(t2,6))) \
+ s2*(-301 + 8*Power(t1,8) + 317*t2 + 2003*Power(t2,2) + 
               1023*Power(t2,3) - 388*Power(t2,4) + 114*Power(t2,5) - 
               72*Power(t2,6) + 
               4*Power(t1,7)*(-9 + 21*t2 + 2*Power(t2,2)) - 
               Power(t1,6)*(-567 + 442*t2 + 415*Power(t2,2) + 
                  26*Power(t2,3)) + 
               Power(t1,5)*(-2360 - 927*t2 + 1736*Power(t2,2) + 
                  753*Power(t2,3) + 32*Power(t2,4)) - 
               Power(t1,4)*(-4251 - 7019*t2 + 760*Power(t2,2) + 
                  2535*Power(t2,3) + 683*Power(t2,4) + 18*Power(t2,5)) + 
               Power(t1,2)*(1057 + 9986*t2 + 11440*Power(t2,2) + 
                  278*Power(t2,3) - 1629*Power(t2,4) - 
                  636*Power(t2,5) - 46*Power(t2,6)) + 
               Power(t1,3)*(-3690 - 12587*t2 - 6045*Power(t2,2) + 
                  2558*Power(t2,3) + 1799*Power(t2,4) + 
                  299*Power(t2,5) + 4*Power(t2,6)) + 
               t1*(504 - 3454*t2 - 7943*Power(t2,2) - 2099*Power(t2,3) + 
                  909*Power(t2,4) + 229*Power(t2,5) + 114*Power(t2,6)))) \
+ Power(s,6)*(12 - 48*t1 + 57*Power(t1,2) + 3*Power(t1,3) - 
            14*Power(t1,4) - 6*Power(t1,5) + 48*t2 - 30*t1*t2 - 
            37*Power(t1,2)*t2 + 28*Power(t1,3)*t2 + 20*Power(t1,4)*t2 - 
            27*Power(t2,2) + 65*t1*Power(t2,2) - 
            20*Power(t1,3)*Power(t2,2) - 31*Power(t2,3) - 
            28*t1*Power(t2,3) + 14*Power(t2,4) + 10*t1*Power(t2,4) - 
            4*Power(t2,5) - 2*Power(s1,3)*(t1 - t2)*
             (-2 + s2 - 4*t1 + 2*t2) + Power(s2,4)*(6 - 6*t1 + 6*t2) + 
            Power(s2,3)*(-6 + 75*Power(t1,2) + 81*t2 + 79*Power(t2,2) - 
               t1*(81 + 154*t2)) + 
            Power(s2,2)*(6 - 91*Power(t1,3) - 112*t2 - 97*Power(t2,2) + 
               19*Power(t2,3) + Power(t1,2)*(1 + 201*t2) + 
               t1*(112 + 96*t2 - 129*Power(t2,2))) + 
            Power(s1,2)*(18 + 6*Power(s2,3) + 10*Power(t1,3) + 
               Power(t1,2)*(8 - 32*t2) + 
               Power(s2,2)*(6 + 31*t1 - 31*t2) - 43*t2 + 
               22*Power(t2,2) - 12*Power(t2,3) - 
               2*s2*(15 + 3*Power(t1,2) + t1*(40 - 7*t2) - 40*t2 + 
                  4*Power(t2,2)) + t1*(43 - 30*t2 + 34*Power(t2,2))) + 
            s2*(-18 + 40*Power(t1,4) + Power(t1,3)*(52 - 116*t2) - 
               21*t2 + 67*Power(t2,2) + 46*Power(t2,3) - 
               4*Power(t2,4) + 
               Power(t1,2)*(-115 - 58*t2 + 108*Power(t2,2)) + 
               t1*(21 + 48*t2 - 40*Power(t2,2) - 28*Power(t2,3))) + 
            s1*(-30 - 6*Power(s2,4) - 10*Power(t1,3) - 4*Power(t1,4) + 
               97*Power(s2,3)*(t1 - t2) - 3*t2 + 74*Power(t2,2) - 
               32*Power(t2,3) + 12*Power(t2,4) + 
               4*Power(t1,2)*(1 - 3*t2 + 6*Power(t2,2)) + 
               t1*(3 - 78*t2 + 54*Power(t2,2) - 32*Power(t2,3)) + 
               Power(s2,2)*(-12 - 74*Power(t1,2) + 155*t2 + 
                  12*Power(t2,2) + 31*t1*(-5 + 2*t2)) + 
               s2*(48 + 36*Power(t1,3) + Power(t1,2)*(42 - 62*t2) - 
                  61*t2 - 126*Power(t2,2) + 10*Power(t2,3) + 
                  t1*(61 + 84*t2 + 16*Power(t2,2))))) - 
         Power(s,5)*(36 - 15*t1 + 38*Power(t1,2) - 78*Power(t1,3) - 
            27*Power(t1,4) + 34*Power(t1,5) + 6*Power(t1,6) - 
            30*Power(s2,5)*(-1 + t1 - t2) - 57*t2 + 208*t1*t2 - 
            107*Power(t1,2)*t2 + 89*Power(t1,3)*t2 - 78*Power(t1,4)*t2 - 
            12*Power(t1,5)*t2 - 246*Power(t2,2) + 196*t1*Power(t2,2) - 
            21*Power(t1,2)*Power(t2,2) + 52*Power(t1,3)*Power(t2,2) - 
            10*Power(t1,4)*Power(t2,2) - 11*Power(t2,3) - 
            117*t1*Power(t2,3) - 28*Power(t1,2)*Power(t2,3) + 
            40*Power(t1,3)*Power(t2,3) + 76*Power(t2,4) + 
            42*t1*Power(t2,4) - 30*Power(t1,2)*Power(t2,4) - 
            22*Power(t2,5) + 4*t1*Power(t2,5) + 2*Power(t2,6) + 
            2*Power(s1,4)*(2 + Power(s2,2) + 6*Power(t1,2) + 
               s2*(-2 + 5*t1 - 5*t2) + 3*t2 + Power(t2,2) - 
               t1*(1 + 6*t2)) + 
            Power(s2,4)*(-32 + 214*Power(t1,2) + 173*t2 + 
               199*Power(t2,2) - 5*t1*(42 + 83*t2)) + 
            Power(s2,3)*(23 - 352*Power(t1,3) - 139*t2 - 
               254*Power(t2,2) - 52*Power(t2,3) + 
               Power(t1,2)*(222 + 680*t2) + 
               t1*(187 + 36*t2 - 276*Power(t2,2))) + 
            Power(s2,2)*(-52 + 232*Power(t1,4) + 
               Power(t1,3)*(107 - 481*t2) - 363*t2 + 8*Power(t2,2) + 
               228*Power(t2,3) - 35*Power(t2,4) + 
               Power(t1,2)*(-685 - 292*t2 + 231*Power(t2,2)) + 
               t1*(316 + 677*t2 - 43*Power(t2,2) + 53*Power(t2,3))) - 
            s2*(3 + 64*Power(t1,5) - 368*t2 - 353*Power(t2,2) + 
               115*Power(t2,3) + 81*Power(t2,4) + 16*Power(t2,5) - 
               7*Power(t1,4)*(-21 + 20*t2) + 
               Power(t1,3)*(-269 - 240*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(-237 + 107*t2 + 120*Power(t2,2) + 
                  140*Power(t2,3)) + 
               t1*(256 + 590*t2 + 47*Power(t2,2) - 108*Power(t2,3) - 
                  100*Power(t2,4))) + 
            Power(s1,3)*(34 + 19*Power(s2,3) + 
               Power(s2,2)*(20 + 44*t1 - 35*t2) + 
               Power(t1,2)*(4 - 24*t2) - 94*t2 + 4*Power(t2,2) - 
               8*Power(t2,3) + t1*(101 - 12*t2 + 32*Power(t2,2)) + 
               s2*(-81 + 50*Power(t1,2) + 129*t2 + 46*Power(t2,2) - 
                  t1*(161 + 100*t2))) + 
            Power(s1,2)*(-31 + 9*Power(s2,4) - 30*Power(t1,4) + 
               Power(s2,3)*(29 + 208*t1 - 258*t2) - 169*t2 + 
               252*Power(t2,2) - 48*Power(t2,3) + 12*Power(t2,4) + 
               Power(t1,3)*(6 + 60*t2) + 
               Power(t1,2)*(7 - 30*t2 - 18*Power(t2,2)) + 
               t1*(58 - 259*t2 + 72*Power(t2,2) - 24*Power(t2,3)) + 
               Power(s2,2)*(-176 - 166*Power(t1,2) + 302*t2 + 
                  29*Power(t2,2) + t1*(-295 + 137*t2)) + 
               s2*(181 + 132*Power(t1,3) + Power(t1,2)*(41 - 324*t2) + 
                  149*t2 - 327*Power(t2,2) - 78*Power(t2,3) + 
                  t1*(29 + 298*t2 + 270*Power(t2,2)))) + 
            s1*(-45 - 30*Power(s2,5) - 12*Power(t1,5) + 
               Power(s2,4)*(21 + 277*t1 - 236*t2) + 319*t2 + 
               146*Power(t2,2) - 238*Power(t2,3) + 60*Power(t2,4) - 
               8*Power(t2,5) + Power(t1,4)*(34 + 60*t2) + 
               Power(t1,3)*(9 - 48*t2 - 100*Power(t2,2)) + 
               2*Power(t1,2)*
                (63 - 23*t2 + 27*Power(t2,2) + 30*Power(t2,3)) - 
               t1*(143 + 272*t2 - 275*Power(t2,2) + 100*Power(t2,3)) - 
               Power(s2,3)*(63 + 259*Power(t1,2) - 401*t2 - 
                  291*Power(t2,2) + t1*(415 + 28*t2)) + 
               Power(s2,2)*(194 + 66*Power(t1,3) + 162*t2 - 
                  550*Power(t2,2) + 39*Power(t2,3) + 
                  Power(t1,2)*(470 + 129*t2) + 
                  t1*(-107 + 68*t2 - 234*Power(t2,2))) + 
               s2*(-85 + 28*Power(t1,4) - 678*t2 + 47*Power(t2,2) + 
                  283*Power(t2,3) + 58*Power(t2,4) - 
                  Power(t1,3)*(159 + 220*t2) + 
                  Power(t1,2)*(-311 + 121*t2 + 414*Power(t2,2)) + 
                  t1*(404 + 264*t2 - 245*Power(t2,2) - 280*Power(t2,3))))\
) + Power(s,4)*(-48 + 350*t1 - 607*Power(t1,2) + 202*Power(t1,3) + 
            114*Power(t1,4) + 45*Power(t1,5) - 50*Power(t1,6) - 
            2*Power(t1,7) - 60*Power(s2,6)*(-1 + t1 - t2) - 530*t2 + 
            1285*t1*t2 - 782*Power(t1,2)*t2 - 85*Power(t1,3)*t2 - 
            81*Power(t1,4)*t2 + 130*Power(t1,5)*t2 - 4*Power(t1,6)*t2 - 
            498*Power(t2,2) + 258*t1*Power(t2,2) + 
            421*Power(t1,2)*Power(t2,2) - 88*Power(t1,3)*Power(t2,2) - 
            128*Power(t1,4)*Power(t2,2) + 30*Power(t1,5)*Power(t2,2) + 
            322*Power(t2,3) - 623*t1*Power(t2,3) + 
            160*Power(t1,2)*Power(t2,3) + 92*Power(t1,3)*Power(t2,3) - 
            40*Power(t1,4)*Power(t2,3) + 173*Power(t2,4) + 
            43*t1*Power(t2,4) - 58*Power(t1,2)*Power(t2,4) + 
            10*Power(t1,3)*Power(t2,4) - 79*Power(t2,5) + 
            2*t1*Power(t2,5) + 12*Power(t1,2)*Power(t2,5) + 
            12*Power(t2,6) - 6*t1*Power(t2,6) + 
            2*Power(s1,5)*(6 + 6*Power(s2,2) + 4*Power(t1,2) + 
               s2*(-12 + 11*t1 - 9*t2) + 4*t2 - 2*t1*(2 + t2)) + 
            2*Power(s2,5)*(-35 + 172*Power(t1,2) + 77*t2 + 
               118*Power(t2,2) - t1*(153 + 296*t2)) - 
            Power(s2,4)*(-14 + 668*Power(t1,3) + 10*t2 + 
               365*Power(t2,2) + 259*Power(t2,3) - 
               Power(t1,2)*(561 + 1126*t2) + 
               t1*(-205 + 83*t2 + 189*Power(t2,2))) + 
            Power(s2,3)*(-3 + 579*Power(t1,4) - 883*t2 - 
               569*Power(t2,2) + 243*Power(t2,3) - 64*Power(t2,4) - 
               Power(t1,3)*(23 + 862*t2) - 
               Power(t1,2)*(1452 + 952*t2 + 157*Power(t2,2)) + 
               t1*(747 + 1883*t2 + 712*Power(t2,2) + 504*Power(t2,3))) + 
            Power(s2,2)*(-169 - 233*Power(t1,5) + 408*t2 + 
               1115*Power(t2,2) + Power(t2,3) - 458*Power(t2,4) - 
               37*Power(t2,5) + Power(t1,4)*(-524 + 253*t2) + 
               Power(t1,3)*(1771 + 1440*t2 + 436*Power(t2,2)) - 
               Power(t1,2)*(607 + 1792*t2 + 1216*Power(t2,2) + 
                  736*Power(t2,3)) + 
               t1*(-138 - 363*t2 + 20*Power(t2,2) + 758*Power(t2,3) + 
                  317*Power(t2,4))) + 
            s2*(226 + 40*Power(t1,6) + 779*t2 - 41*Power(t2,2) - 
               581*Power(t2,3) + 53*Power(t2,4) + 28*Power(t2,5) + 
               12*Power(t2,6) - 6*Power(t1,5)*(-51 + 2*t2) - 
               Power(t1,4)*(517 + 750*t2 + 220*Power(t2,2)) + 
               Power(t1,3)*(-832 + 669*t2 + 816*Power(t2,2) + 
                  320*Power(t2,3)) + 
               Power(t1,2)*(1505 + 1348*t2 - 644*Power(t2,2) - 
                  578*Power(t2,3) - 120*Power(t2,4)) + 
               t1*(-760 - 1754*t2 + 65*Power(t2,2) + 439*Power(t2,3) + 
                  178*Power(t2,4) - 20*Power(t2,5))) + 
            Power(s1,4)*(6 + 11*Power(s2,3) + 28*Power(t1,2) - 
               20*Power(t1,3) + Power(s2,2)*(37 + 114*t1 - 111*t2) - 
               83*t2 - 20*Power(t2,2) + 
               t1*(103 - 26*t2 + 10*Power(t2,2)) + 
               s2*(-44 + 78*Power(t1,2) + 224*t2 + 84*Power(t2,2) - 
                  t1*(247 + 162*t2))) + 
            Power(s1,3)*(152 + 73*Power(s2,4) - 40*Power(t1,4) + 
               Power(s2,3)*(107 + 110*t1 - 189*t2) - 295*t2 + 
               256*Power(t2,2) + 4*Power(t1,3)*(18 + 25*t2) - 
               Power(t1,2)*(113 + 176*t2 + 60*Power(t2,2)) + 
               t1*(111 - 190*t2 + 124*Power(t2,2)) + 
               Power(s2,2)*(-375 + 62*Power(t1,2) + 271*t2 + 
                  298*Power(t2,2) - t1*(401 + 417*t2)) + 
               s2*(3 + 72*Power(t1,3) + Power(t1,2)*(9 - 270*t2) + 
                  205*t2 - 556*Power(t2,2) - 156*Power(t2,3) + 
                  t1*(148 + 683*t2 + 374*Power(t2,2)))) + 
            Power(s1,2)*(-418 - 36*Power(s2,5) + 10*Power(t1,5) + 
               Power(s2,4)*(38 + 593*t1 - 697*t2) - 110*t2 + 
               745*Power(t2,2) - 390*Power(t2,3) + 40*Power(t2,4) + 
               8*Power(t1,4)*(6 + 5*t2) - 
               2*Power(t1,3)*(61 + 81*t2 + 65*Power(t2,2)) + 
               Power(t1,2)*(143 + 398*t2 + 210*Power(t2,2) + 
                  100*Power(t2,3)) + 
               t1*(255 - 603*t2 + 114*Power(t2,2) - 136*Power(t2,3) - 
                  20*Power(t2,4)) + 
               Power(s2,3)*(-364 - 809*Power(t1,2) + 207*t2 + 
                  281*Power(t2,2) + t1*(-217 + 684*t2)) + 
               Power(s2,2)*(304 + 587*Power(t1,3) + 1365*t2 - 
                  1111*Power(t2,2) - 348*Power(t2,3) - 
                  8*Power(t1,2)*(-26 + 131*t2) + 
                  t1*(-474 + 838*t2 + 809*Power(t2,2))) - 
               s2*(-536 + 172*Power(t1,4) + Power(t1,3)*(31 - 212*t2) + 
                  821*t2 + 225*Power(t2,2) - 564*Power(t2,3) - 
                  144*Power(t2,4) + 
                  Power(t1,2)*(64 + 146*t2 - 186*Power(t2,2)) + 
                  t1*(21 + 115*t2 + 447*Power(t2,2) + 370*Power(t2,3)))) \
- s1*(-290 + 60*Power(s2,6) - 20*Power(t1,6) - 1048*t2 + 
               364*Power(t2,2) + 629*Power(t2,3) - 284*Power(t2,4) + 
               40*Power(t2,5) + Power(t1,5)*(38 + 60*t2) + 
               Power(s2,5)*(-94 - 408*t1 + 232*t2) + 
               Power(t1,4)*(83 - 40*t2 - 40*Power(t2,2)) + 
               Power(t1,3)*(88 - 314*t2 + 2*Power(t2,2) - 
                  40*Power(t2,3)) + 
               Power(t1,2)*(-785 + 398*t2 + 445*Power(t2,2) + 
                  4*Power(t2,3) + 60*Power(t2,4)) + 
               t1*(827 + 851*t2 - 1115*Power(t2,2) + 70*Power(t2,3) - 
                  44*Power(t2,4) - 20*Power(t2,5)) + 
               Power(s2,4)*(83 + 418*Power(t1,2) - 591*t2 - 
                  883*Power(t2,2) + t1*(756 + 556*t2)) + 
               Power(s2,3)*(-201 + 89*Power(t1,3) - 1043*t2 + 
                  557*Power(t2,2) + 39*Power(t2,3) - 
                  2*Power(t1,2)*(884 + 703*t2) + 
                  t1*(762 + 1197*t2 + 1298*Power(t2,2))) + 
               Power(s2,2)*(-203 - 324*Power(t1,4) + 1891*t2 + 
                  991*Power(t2,2) - 1261*Power(t2,3) - 
                  186*Power(t2,4) + Power(t1,3)*(1328 + 1409*t2) - 
                  2*Power(t1,2)*(-119 + 661*t2 + 861*Power(t2,2)) + 
                  t1*(-893 - 1098*t2 + 1195*Power(t2,2) + 
                     823*Power(t2,3))) + 
               s2*(685 + 148*Power(t1,5) + 493*t2 - 1399*Power(t2,2) - 
                  11*Power(t2,3) + 236*Power(t2,4) + 66*Power(t2,5) - 
                  Power(t1,4)*(393 + 536*t2) + 
                  Power(t1,3)*(-497 + 705*t2 + 604*Power(t2,2)) + 
                  Power(t1,2)*
                   (1191 + 36*t2 - 715*Power(t2,2) - 126*Power(t2,3)) + 
                  t1*(-918 - 442*t2 + 472*Power(t2,2) + 
                     167*Power(t2,3) - 156*Power(t2,4))))) + 
         Power(s,3)*(312 - 1182*t1 + 1884*Power(t1,2) - 
            1379*Power(t1,3) + 126*Power(t1,4) + 262*Power(t1,5) + 
            11*Power(t1,6) - 34*Power(t1,7) - 
            2*Power(s1,6)*(4 + 9*Power(s2,2) - 2*t1 + Power(t1,2) + 
               2*s2*(-7 + 4*t1 - 2*t2)) + 
            60*Power(s2,7)*(-1 + t1 - t2) + 990*t2 - 2392*t1*t2 + 
            1731*Power(t1,2)*t2 + 374*Power(t1,3)*t2 - 
            921*Power(t1,4)*t2 + 85*Power(t1,5)*t2 + 70*Power(t1,6)*t2 - 
            4*Power(t1,7)*t2 + 148*Power(t2,2) + 809*t1*Power(t2,2) - 
            2202*Power(t1,2)*Power(t2,2) + 
            1776*Power(t1,3)*Power(t2,2) - 314*Power(t1,4)*Power(t2,2) - 
            12*Power(t1,5)*Power(t2,2) + 10*Power(t1,6)*Power(t2,2) - 
            921*Power(t2,3) + 1858*t1*Power(t2,3) - 
            1292*Power(t1,2)*Power(t2,3) + 208*Power(t1,3)*Power(t2,3) - 
            72*Power(t1,4)*Power(t2,3) - 156*Power(t2,4) + 
            50*t1*Power(t2,4) + 105*Power(t1,2)*Power(t2,4) + 
            98*Power(t1,3)*Power(t2,4) - 20*Power(t1,4)*Power(t2,4) + 
            125*Power(t2,5) - 69*t1*Power(t2,5) - 
            78*Power(t1,2)*Power(t2,5) + 20*Power(t1,3)*Power(t2,5) - 
            26*Power(t2,6) + 28*t1*Power(t2,6) - 
            6*Power(t1,2)*Power(t2,6) - 
            2*Power(s2,6)*(-39 + 159*Power(t1,2) + 9*t2 + 
               57*Power(t2,2) - t1*(129 + 230*t2)) + 
            Power(s2,5)*(70 + 676*Power(t1,3) - 40*t2 + 
               332*Power(t2,2) + 414*Power(t2,3) - 
               5*Power(t1,2)*(109 + 182*t2) - 
               t1*(277 + 127*t2 + 228*Power(t2,2))) + 
            Power(s2,4)*(-145 - 700*Power(t1,4) + 949*t2 + 
               1272*Power(t2,2) + 79*Power(t2,3) + 39*Power(t2,4) + 
               Power(t1,3)*(95 + 597*t2) + 
               Power(t1,2)*(1744 + 1925*t2 + 1171*Power(t2,2)) - 
               t1*(866 + 2602*t2 + 1897*Power(t2,2) + 1087*Power(t2,3))) \
+ Power(s2,3)*(325 + 360*Power(t1,5) + 399*t2 - 745*Power(t2,2) - 
               175*Power(t2,3) + 544*Power(t2,4) + 40*Power(t2,5) + 
               15*Power(t1,4)*(61 + 4*t2) - 
               3*Power(t1,3)*(1275 + 1239*t2 + 532*Power(t2,2)) + 
               Power(t1,2)*(2522 + 5621*t2 + 3409*Power(t2,2) + 
                  1492*Power(t2,3)) - 
               t1*(405 + 2099*t2 + 1813*Power(t2,2) + 
                  1191*Power(t2,3) + 356*Power(t2,4))) - 
            Power(s2,2)*(158 + 86*Power(t1,6) + 1786*t2 + 
               1291*Power(t2,2) - 1246*Power(t2,3) - 504*Power(t2,4) + 
               374*Power(t2,5) + 41*Power(t2,6) + 
               Power(t1,5)*(932 + 187*t2) - 
               Power(t1,4)*(2753 + 2652*t2 + 869*Power(t2,2)) + 
               Power(t1,3)*(330 + 4063*t2 + 2450*Power(t2,2) + 
                  764*Power(t2,3)) + 
               Power(t1,2)*(2229 + 1448*t2 - 2677*Power(t2,2) - 
                  908*Power(t2,3) - 58*Power(t2,4)) - 
               t1*(1028 + 4080*t2 + 762*Power(t2,2) - 
                  1871*Power(t2,3) + 196*Power(t2,4) + 151*Power(t2,5))) \
+ s2*(-402 + 8*Power(t1,7) - 148*t2 + 1066*Power(t2,2) - 
               65*Power(t2,3) - 827*Power(t2,4) + 45*Power(t2,5) - 
               25*Power(t2,6) + Power(t1,6)*(303 + 52*t2) - 
               2*Power(t1,5)*(254 + 361*t2 + 90*Power(t2,2)) + 
               Power(t1,4)*(-1496 + 653*t2 + 527*Power(t2,2) + 
                  120*Power(t2,3)) + 
               Power(t1,3)*(2971 + 2749*t2 - 302*Power(t2,2) + 
                  116*Power(t2,3) + 80*Power(t2,4)) - 
               Power(t1,2)*(1960 + 3143*t2 + 1517*Power(t2,2) + 
                  188*Power(t2,3) + 465*Power(t2,4) + 108*Power(t2,5)) \
+ t1*(1076 + 940*t2 - 163*Power(t2,2) + 1091*Power(t2,3) + 
                  300*Power(t2,4) + 266*Power(t2,5) + 28*Power(t2,6))) + 
            Power(s1,5)*(-46 - 15*Power(s2,3) + 20*Power(t1,3) + 
               22*t2 - 2*Power(t1,2)*(23 + 2*t2) + t1*(3 + 32*t2) + 
               Power(s2,2)*(6 - 144*t1 + 175*t2) + 
               s2*(37 - 24*Power(t1,2) - 189*t2 - 40*Power(t2,2) + 
                  t1*(133 + 68*t2))) - 
            Power(s1,4)*(-23 + 43*Power(s2,4) - 10*Power(t1,4) + 
               2*Power(s2,3)*(81 + 77*t1 - 85*t2) - 329*t2 + 
               34*Power(t2,2) + Power(t1,3)*(38 + 60*t2) - 
               Power(t1,2)*(253 + 146*t2 + 30*Power(t2,2)) + 
               t1*(430 + 81*t2 + 140*Power(t2,2)) + 
               Power(s2,2)*(-127 + 186*Power(t1,2) + 402*t2 + 
                  561*Power(t2,2) - t1*(572 + 799*t2)) + 
               s2*(-127 - 92*Power(t1,3) + 323*t2 - 451*Power(t2,2) - 
                  80*Power(t2,3) + Power(t1,2)*(393 + 88*t2) + 
                  2*t1*(-128 + 81*t2 + 42*Power(t2,2)))) + 
            Power(s1,3)*(-203 - 96*Power(s2,5) - 40*Power(t1,5) + 
               301*t2 - 836*Power(t2,2) + 76*Power(t2,3) + 
               4*Power(t1,4)*(27 + 10*t2) + 
               Power(s2,4)*(-175 - 147*t1 + 446*t2) + 
               4*Power(t1,3)*(13 - 36*t2 + 10*Power(t2,2)) - 
               4*Power(t1,2)*
                (109 + 135*t2 + 21*Power(t2,2) + 10*Power(t2,3)) + 
               t1*(441 + 884*t2 + 294*Power(t2,2) + 160*Power(t2,3)) + 
               Power(s2,3)*(550 + 190*Power(t1,2) + 316*t2 - 
                  460*Power(t2,2) + t1*(147 + 148*t2)) + 
               Power(s2,2)*(566 - 400*Power(t1,3) - 1247*t2 + 
                  1544*Power(t2,2) + 854*Power(t2,3) + 
                  12*Power(t1,2)*(33 + 91*t2) - 
                  2*t1*(171 + 1114*t2 + 842*Power(t2,2))) + 
               s2*(-790 + 268*Power(t1,4) + 448*t2 + 702*Power(t2,2) - 
                  474*Power(t2,3) - 80*Power(t2,4) - 
                  4*Power(t1,3)*(191 + 160*t2) + 
                  12*Power(t1,2)*(93 + 170*t2 + 43*Power(t2,2)) - 
                  t1*(355 + 1224*t2 + 578*Power(t2,2) + 24*Power(t2,3)))\
) + Power(s1,2)*(922 + 112*Power(s2,6) - 20*Power(t1,6) - 593*t2 - 
               827*Power(t2,2) + 994*Power(t2,3) - 124*Power(t2,4) + 
               2*Power(t1,5)*(81 + 50*t2) + 
               Power(s2,5)*(-3 - 926*t1 + 884*t2) - 
               2*Power(t1,4)*(106 + 239*t2 + 65*Power(t2,2)) + 
               4*Power(t1,3)*
                (29 + 84*t2 + 125*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,2)*(205 - 292*t2 + 426*Power(t2,2) - 
                  164*Power(t2,3) + 10*Power(t2,4)) - 
               t1*(1065 - 892*t2 + 428*Power(t2,2) + 426*Power(t2,3) + 
                  20*Power(t2,4)) + 
               Power(s2,4)*(391 + 1665*Power(t1,2) + 245*t2 - 
                  724*Power(t2,2) - t1*(305 + 1249*t2)) - 
               Power(s2,3)*(99 + 1318*Power(t1,3) + 
                  Power(t1,2)*(247 - 1242*t2) + 2471*t2 - 
                  398*Power(t2,2) - 510*Power(t2,3) + 
                  t1*(-1041 + 25*t2 + 190*Power(t2,2))) + 
               Power(s2,2)*(-1457 + 366*Power(t1,4) + 54*t2 + 
                  2617*Power(t2,2) - 2280*Power(t2,3) - 
                  676*Power(t2,4) + 18*Power(t1,3)*(51 + 10*t2) - 
                  2*Power(t1,2)*(721 + 822*t2 + 784*Power(t2,2)) + 
                  t1*(1202 + 545*t2 + 2936*Power(t2,2) + 
                     1698*Power(t2,3))) + 
               s2*(296 + 42*Power(t1,5) + 2245*t2 - 2104*Power(t2,2) - 
                  538*Power(t2,3) + 186*Power(t2,4) + 40*Power(t2,5) - 
                  2*Power(t1,4)*(412 + 261*t2) + 
                  2*Power(t1,3)*(607 + 1254*t2 + 542*Power(t2,2)) - 
                  Power(t1,2)*
                   (347 + 3092*t2 + 3366*Power(t2,2) + 780*Power(t2,3)) \
+ t1*(-383 + 869*t2 + 1980*Power(t2,2) + 1376*Power(t2,3) + 
                     136*Power(t2,4)))) + 
            s1*(-990 + 60*Power(s2,7) + 32*Power(t1,6) + 8*Power(t1,7) - 
               2*Power(s2,6)*(81 + 158*t1 - 10*t2) - 1148*t2 + 
               1717*Power(t2,2) + 659*Power(t2,3) - 566*Power(t2,4) + 
               94*Power(t2,5) - 
               Power(t1,5)*(221 + 220*t2 + 60*Power(t2,2)) + 
               2*Power(t1,4)*
                (125 + 344*t2 + 221*Power(t2,2) + 50*Power(t2,3)) - 
               Power(t1,3)*(-693 + 1628*t2 + 596*Power(t2,2) + 
                  416*Power(t2,3) + 60*Power(t2,4)) + 
               t1*(2284 + 706*t2 - 3191*Power(t2,2) - 76*Power(t2,3) + 
                  279*Power(t2,4) - 64*Power(t2,5)) + 
               Power(t1,2)*(-1989 + 1279*t2 + 2020*Power(t2,2) - 
                  244*Power(t2,3) + 226*Power(t2,4) + 12*Power(t2,5)) + 
               Power(s2,5)*(-59 + 246*Power(t1,2) - 555*t2 - 
                  1202*Power(t2,2) + 2*t1*(521 + 652*t2)) + 
               Power(s2,4)*(54 + 529*Power(t1,3) - 1875*t2 - 
                  149*Power(t2,2) + 282*Power(t2,3) - 
                  Power(t1,2)*(3265 + 3368*t2) + 
                  t1*(1557 + 3074*t2 + 2483*Power(t2,2))) + 
               Power(s2,3)*(-607 - 967*Power(t1,4) + 1536*t2 + 
                  2096*Power(t2,2) - 1096*Power(t2,3) - 
                  245*Power(t2,4) + Power(t1,3)*(3779 + 3544*t2) - 
                  2*Power(t1,2)*(749 + 1918*t2 + 1462*Power(t2,2)) + 
                  t1*(-755 + 16*t2 + 1069*Power(t2,2) + 552*Power(t2,3))\
) + Power(s2,2)*(926 + 550*Power(t1,5) + 3106*t2 - 1866*Power(t2,2) - 
                  2001*Power(t2,3) + 1506*Power(t2,4) + 
                  267*Power(t2,5) - Power(t1,4)*(1614 + 1585*t2) + 
                  4*Power(t1,3)*(-200 + 341*t2 + 246*Power(t2,2)) + 
                  Power(t1,2)*
                   (3750 + 919*t2 + 340*Power(t2,2) + 604*Power(t2,3)) \
- 4*t1*(538 + 978*t2 - 417*Power(t2,2) + 369*Power(t2,3) + 
                     205*Power(t2,4))) + 
               s2*(688 - 118*Power(t1,6) - 1788*t2 - 1390*Power(t2,2) + 
                  2356*Power(t2,3) + 77*Power(t2,4) + 23*Power(t2,5) - 
                  8*Power(t2,6) + Power(t1,5)*(95 + 232*t2) + 
                  Power(t1,4)*(1199 + 593*t2 + 134*Power(t2,2)) - 
                  Power(t1,3)*
                   (2301 + 2128*t2 + 1860*Power(t2,2) + 616*Power(t2,3)) \
+ Power(t1,2)*(956 + 2370*t2 + 2164*Power(t2,2) + 2184*Power(t2,3) + 
                     484*Power(t2,4)) - 
                  t1*(884 - 1284*t2 + 1605*Power(t2,2) + 
                     1312*Power(t2,3) + 1035*Power(t2,4) + 
                     108*Power(t2,5))))) + 
         Power(s,2)*(-468 + 1764*t1 - 2621*Power(t1,2) + 
            2019*Power(t1,3) - 578*Power(t1,4) - 449*Power(t1,5) + 
            375*Power(t1,6) - 34*Power(t1,7) - 8*Power(t1,8) + 
            4*Power(s1,7)*s2*(-2 + 2*s2 + t1) - 
            30*Power(s2,8)*(-1 + t1 - t2) - 828*t2 + 1752*t1*t2 - 
            545*Power(t1,2)*t2 - 1823*Power(t1,3)*t2 + 
            2783*Power(t1,4)*t2 - 1525*Power(t1,5)*t2 + 
            222*Power(t1,6)*t2 - 6*Power(t1,7)*t2 + 581*Power(t2,2) - 
            2939*t1*Power(t2,2) + 5361*Power(t1,2)*Power(t2,2) - 
            5212*Power(t1,3)*Power(t2,2) + 
            2461*Power(t1,4)*Power(t2,2) - 433*Power(t1,5)*Power(t2,2) + 
            80*Power(t1,6)*Power(t2,2) - 2*Power(t1,7)*Power(t2,2) + 
            1105*Power(t2,3) - 2815*t1*Power(t2,3) + 
            3132*Power(t1,2)*Power(t2,3) - 
            1404*Power(t1,3)*Power(t2,3) + 259*Power(t1,4)*Power(t2,3) - 
            144*Power(t1,5)*Power(t2,3) + 8*Power(t1,6)*Power(t2,3) + 
            35*Power(t2,4) - 195*t1*Power(t2,4) - 
            84*Power(t1,2)*Power(t2,4) + 27*Power(t1,3)*Power(t2,4) + 
            132*Power(t1,4)*Power(t2,4) - 12*Power(t1,5)*Power(t2,4) - 
            59*Power(t2,5) + 163*t1*Power(t2,5) - 
            5*Power(t1,2)*Power(t2,5) - 74*Power(t1,3)*Power(t2,5) + 
            8*Power(t1,4)*Power(t2,5) + 14*Power(t2,6) - 
            36*t1*Power(t2,6) + 20*Power(t1,2)*Power(t2,6) - 
            2*Power(t1,3)*Power(t2,6) + 
            Power(s1,6)*(20 + 35*Power(s2,3) - 30*t1 + 20*Power(t1,2) - 
               6*Power(t1,3) + Power(s2,2)*(-27 + 51*t1 - 82*t2) + 
               s2*(-48 + 27*t1 - 16*Power(t1,2) + 38*t2)) + 
            Power(s2,7)*(-44 + 157*Power(t1,2) - 61*t2 - 
               17*Power(t2,2) - t1*(117 + 172*t2)) + 
            Power(s2,6)*(-126 - 349*Power(t1,3) - 52*t2 - 
               189*Power(t2,2) - 325*Power(t2,3) + 
               Power(t1,2)*(200 + 269*t2) + 
               t1*(295 + 377*t2 + 489*Power(t2,2))) + 
            Power(s2,5)*(139 + 399*Power(t1,4) - 530*t2 - 
               1111*Power(t2,2) - 229*Power(t2,3) + 17*Power(t2,4) + 
               Power(t1,3)*(125 + 72*t2) - 
               Power(t1,2)*(1429 + 2100*t2 + 1588*Power(t2,2)) + 
               t1*(726 + 2083*t2 + 1796*Power(t2,2) + 1028*Power(t2,3))) \
- Power(s2,4)*(132 + 235*Power(t1,5) + 936*t2 + 291*Power(t2,2) - 
               385*Power(t2,3) + 30*Power(t2,4) + 4*Power(t2,5) + 
               25*Power(t1,4)*(38 + 21*t2) - 
               Power(t1,3)*(3867 + 4354*t2 + 2079*Power(t2,2)) + 
               Power(t1,2)*(2942 + 6619*t2 + 3516*Power(t2,2) + 
                  1323*Power(t2,3)) - 
               t1*(432 + 3408*t2 + 2759*Power(t2,2) + 350*Power(t2,3) + 
                  28*Power(t2,4))) + 
            Power(s2,3)*(-241 + 64*Power(t1,6) + 1250*t2 + 
               1731*Power(t2,2) - 504*Power(t2,3) - 205*Power(t2,4) + 
               584*Power(t2,5) + 65*Power(t2,6) + 
               2*Power(t1,5)*(602 + 227*t2) - 
               Power(t1,4)*(4443 + 3746*t2 + 1204*Power(t2,2)) + 
               Power(t1,3)*(3058 + 6900*t2 + 2151*Power(t2,2) + 
                  524*Power(t2,3)) + 
               Power(t1,2)*(493 - 1895*t2 - 2420*Power(t2,2) + 
                  1133*Power(t2,3) + 393*Power(t2,4)) - 
               t1*(155 + 2232*t2 + 1039*Power(t2,2) - 30*Power(t2,3) + 
                  1366*Power(t2,4) + 296*Power(t2,5))) + 
            s2*(90 - 705*t2 - 518*Power(t2,2) + 2129*Power(t2,3) + 
               1448*Power(t2,4) - 330*Power(t2,5) + 68*Power(t2,6) - 
               2*Power(t2,7) + 2*Power(t1,7)*(57 + 8*t2) - 
               Power(t1,6)*(53 + 110*t2 + 16*Power(t2,2)) - 
               Power(t1,5)*(2167 + 741*t2 + 444*Power(t2,2) + 
                  68*Power(t2,3)) + 
               Power(t1,4)*(4498 + 6116*t2 + 2019*Power(t2,2) + 
                  1056*Power(t2,3) + 140*Power(t2,4)) - 
               Power(t1,3)*(3271 + 9412*t2 + 7451*Power(t2,2) + 
                  2212*Power(t2,3) + 938*Power(t2,4) + 92*Power(t2,5)) \
+ Power(t1,2)*(1346 + 4237*t2 + 9504*Power(t2,2) + 5107*Power(t2,3) + 
                  1118*Power(t2,4) + 348*Power(t2,5) + 20*Power(t2,6)) \
- t1*(557 - 600*t2 + 3041*Power(t2,2) + 6348*Power(t2,3) + 
                  1275*Power(t2,4) + 199*Power(t2,5) + 24*Power(t2,6))) \
- Power(s2,2)*(-658 + 6*Power(t1,7) - 1598*t2 - 434*Power(t2,2) + 
               241*Power(t2,3) - 1145*Power(t2,4) - 751*Power(t2,5) + 
               59*Power(t2,6) + 2*Power(t2,7) + 
               2*Power(t1,6)*(299 + 72*t2) - 
               Power(t1,5)*(1841 + 1268*t2 + 271*Power(t2,2)) - 
               Power(t1,4)*(1064 - 1780*t2 + 258*Power(t2,2) + 
                  99*Power(t2,3)) + 
               Power(t1,3)*(5311 + 4775*t2 + 1389*Power(t2,2) + 
                  2372*Power(t2,3) + 463*Power(t2,4)) - 
               Power(t1,2)*(4352 + 9519*t2 + 4233*Power(t2,2) + 
                  3179*Power(t2,3) + 2243*Power(t2,4) + 299*Power(t2,5)\
) + t1*(1996 + 6046*t2 + 3387*Power(t2,2) + 1467*Power(t2,3) + 
                  2602*Power(t2,4) + 740*Power(t2,5) + 54*Power(t2,6))) \
+ Power(s1,5)*(60 - 13*Power(s2,4) - 31*Power(t1,2) + 12*Power(t1,4) + 
               Power(s2,3)*(43 + 267*t1 - 298*t2) - 82*t2 + 
               6*Power(t1,3)*(-7 + 2*t2) + t1*(4 + 54*t2) - 
               Power(s2,2)*(45 + 38*Power(t1,2) - 244*t2 - 
                  292*Power(t2,2) + t1*(242 + 229*t2)) + 
               s2*(53 - 96*Power(t1,3) + 378*t2 - 68*Power(t2,2) + 
                  3*Power(t1,2)*(145 + 48*t2) - 
                  t1*(357 + 373*t2 + 60*Power(t2,2)))) + 
            Power(s1,4)*(-56 + 80*Power(s2,5) + 30*Power(t1,5) + 
               Power(s2,4)*(279 - 92*t1 - 18*t2) - 403*t2 + 
               142*Power(t2,2) - 2*Power(t1,4)*(71 + 30*t2) + 
               Power(t1,3)*(351 + 294*t2 + 10*Power(t2,2)) + 
               t1*(642 + 335*t2 + 48*Power(t2,2)) - 
               Power(t1,2)*(862 + 145*t2 + 180*Power(t2,2)) + 
               Power(s2,3)*(-12 + 433*Power(t1,2) + 200*t2 + 
                  907*Power(t2,2) - 2*t1*(328 + 693*t2)) + 
               Power(s2,2)*(-475 - 227*Power(t1,3) + 1007*t2 - 
                  765*Power(t2,2) - 510*Power(t2,3) + 
                  Power(t1,2)*(569 + 503*t2) + 
                  t1*(-713 + 416*t2 + 352*Power(t2,2))) + 
               s2*(-102 - 92*Power(t1,4) - 422*t2 - 964*Power(t2,2) + 
                  50*Power(t2,3) + Power(t1,3)*(378 + 328*t2) - 
                  4*Power(t1,2)*(120 + 440*t2 + 99*Power(t2,2)) + 
                  t1*(807 + 1393*t2 + 1198*Power(t2,2) + 
                     160*Power(t2,3)))) + 
            Power(s1,3)*(34 + 40*Power(s2,6) + 
               Power(s2,5)*(50 + 253*t1 - 571*t2) - 53*t2 + 
               908*Power(t2,2) - 148*Power(t2,3) - 
               12*Power(t1,5)*(4 + 5*t2) + 
               4*Power(t1,4)*(96 + 81*t2 + 25*Power(t2,2)) - 
               2*Power(t1,3)*
                (739 + 510*t2 + 278*Power(t2,2) + 20*Power(t2,3)) - 
               t1*(804 + 1401*t2 + 1192*Power(t2,2) + 
                  132*Power(t2,3)) + 
               Power(t1,2)*(1815 + 2418*t2 + 626*Power(t2,2) + 
                  320*Power(t2,3)) + 
               Power(s2,4)*(-422 - 783*Power(t1,2) - 1303*t2 + 
                  136*Power(t2,2) + t1*(677 + 1118*t2)) + 
               Power(s2,3)*(-1002 + 1144*Power(t1,3) + 695*t2 - 
                  1442*Power(t2,2) - 1348*Power(t2,3) - 
                  Power(t1,2)*(1643 + 2730*t2) + 
                  4*t1*(284 + 955*t2 + 713*Power(t2,2))) + 
               Power(s2,2)*(640 - 712*Power(t1,4) + 840*t2 - 
                  3502*Power(t2,2) + 1160*Power(t2,3) + 
                  480*Power(t2,4) + 2*Power(t1,3)*(877 + 894*t2) - 
                  2*Power(t1,2)*(1375 + 2071*t2 + 790*Power(t2,2)) + 
                  t1*(979 + 3883*t2 + 944*Power(t2,2) - 
                     138*Power(t2,3))) + 
               s2*(122*Power(t1,5) - 2*Power(t1,4)*(188 + 25*t2) - 
                  2*Power(t1,3)*(-471 + 84*t2 + 158*Power(t2,2)) + 
                  Power(t1,2)*
                   (-109 + 334*t2 + 2322*Power(t2,2) + 464*Power(t2,3)) \
+ 2*(612 - 739*t2 + 639*Power(t2,2) + 518*Power(t2,3)) - 
                  t1*(1139 + 780*t2 + 1838*Power(t2,2) + 
                     1602*Power(t2,3) + 180*Power(t2,4)))) - 
            Power(s1,2)*(830 + 120*Power(s2,7) + 12*Power(t1,7) - 
               1361*t2 - 309*Power(t2,2) + 906*Power(t2,3) - 
               112*Power(t2,4) - 24*Power(t1,6)*(4 + t2) + 
               Power(s2,6)*(7 - 787*t1 + 543*t2) + 
               Power(t1,5)*(29 + 150*t2 - 18*Power(t2,2)) + 
               Power(t1,4)*(516 + 305*t2 + 90*Power(t2,2) + 
                  60*Power(t2,3)) - 
               t1*(1393 - 1813*t2 + 681*Power(t2,2) + 
                  1526*Power(t2,3) + 18*Power(t2,4)) - 
               Power(t1,3)*(769 + 1512*t2 + 1014*Power(t2,2) + 
                  324*Power(t2,3) + 30*Power(t2,4)) + 
               2*Power(t1,2)*
                (481 + 147*t2 + 1167*Power(t2,2) + 349*Power(t2,3) + 
                  90*Power(t2,4)) + 
               Power(s2,5)*(178 + 1585*Power(t1,2) + 169*t2 - 
                  919*Power(t2,2) - t1*(365 + 728*t2)) + 
               Power(s2,4)*(143 - 1283*Power(t1,3) - 2341*t2 - 
                  1739*Power(t2,2) + 170*Power(t2,3) - 
                  Power(t1,2)*(545 + 429*t2) + 
                  2*t1*(579 + 1274*t2 + 966*Power(t2,2))) + 
               Power(s2,3)*(-1426 + 169*Power(t1,4) - 1956*t2 + 
                  1559*Power(t2,2) - 2696*Power(t2,3) - 
                  1057*Power(t2,4) + Power(t1,3)*(2605 + 2326*t2) - 
                  Power(t1,2)*(4415 + 7095*t2 + 4554*Power(t2,2)) + 
                  t1*(2569 + 4922*t2 + 7038*Power(t2,2) + 
                     2910*Power(t2,3))) + 
               Power(s2,2)*(-789 + 286*Power(t1,5) + 2559*t2 - 
                  890*Power(t2,2) - 4914*Power(t2,3) + 
                  925*Power(t2,4) + 238*Power(t2,5) - 
                  Power(t1,4)*(2801 + 1953*t2) + 
                  Power(t1,3)*(5530 + 7768*t2 + 3358*Power(t2,2)) - 
                  Power(t1,2)*
                   (3431 + 10419*t2 + 8820*Power(t2,2) + 
                     2102*Power(t2,3)) + 
                  t1*(1621 + 2485*t2 + 8229*Power(t2,2) + 
                     2908*Power(t2,3) + 173*Power(t2,4))) + 
               s2*(1593 - 114*Power(t1,6) + 721*t2 - 4710*Power(t2,2) + 
                  1832*Power(t2,3) + 384*Power(t2,4) + 22*Power(t2,5) + 
                  Power(t1,5)*(1019 + 450*t2) - 
                  2*Power(t1,4)*(1157 + 1235*t2 + 258*Power(t2,2)) + 
                  Power(t1,3)*
                   (1829 + 4760*t2 + 1736*Power(t2,2) + 60*Power(t2,3)) \
+ Power(t1,2)*(-188 - 4293*t2 - 1890*Power(t2,2) + 756*Power(t2,3) + 
                     216*Power(t2,4)) - 
                  t1*(2045 - 2552*t2 - 2136*Power(t2,2) + 
                     726*Power(t2,3) + 943*Power(t2,4) + 96*Power(t2,5))\
)) + s1*(1230 - 30*Power(s2,8) + 165*t2 - 2500*Power(t2,2) - 
               235*Power(t2,3) + 400*Power(t2,4) - 58*Power(t2,5) + 
               4*Power(t1,7)*(14 + 3*t2) + 
               Power(s2,7)*(132 + 109*t1 + 133*t2) - 
               Power(t1,6)*(221 + 228*t2 + 32*Power(t2,2)) + 
               Power(t1,5)*(419 + 600*t2 + 342*Power(t2,2) + 
                  24*Power(t2,3)) - 
               Power(t1,4)*(223 + 1757*t2 + 338*Power(t2,2) + 
                  224*Power(t2,3)) + 
               Power(t1,2)*(2532 - 4027*t2 - 4653*Power(t2,2) + 
                  862*Power(t2,3) + 253*Power(t2,4)) + 
               Power(t1,3)*(-744 + 3793*t2 + 1370*Power(t2,2) - 
                  372*Power(t2,3) + 54*Power(t2,4) - 4*Power(t2,5)) + 
               t1*(-3079 + 1636*t2 + 5432*Power(t2,2) + 
                  273*Power(t2,3) - 836*Power(t2,4) + 78*Power(t2,5)) + 
               Power(s2,6)*(185 + 68*Power(t1,2) + 300*t2 + 
                  828*Power(t2,2) - t1*(887 + 1360*t2)) - 
               Power(s2,5)*(163 + 759*Power(t1,3) - 1439*t2 - 
                  348*Power(t2,2) + 445*Power(t2,3) - 
                  Power(t1,2)*(3110 + 3501*t2) + 
                  t1*(1633 + 2679*t2 + 2009*Power(t2,2))) + 
               Power(s2,4)*(429 + 1212*Power(t1,4) - 68*t2 - 
                  2304*Power(t2,2) - 685*Power(t2,3) + 69*Power(t2,4) - 
                  Power(t1,3)*(4583 + 3832*t2) + 
                  3*Power(t1,2)*(935 + 1169*t2 + 559*Power(t2,2)) + 
                  t1*(375 - 1125*t2 + 1521*Power(t2,2) + 
                     878*Power(t2,3))) + 
               Power(s2,3)*(-241 - 808*Power(t1,5) - 3785*t2 - 
                  450*Power(t2,2) + 1081*Power(t2,3) - 
                  2081*Power(t2,4) - 418*Power(t2,5) + 
                  Power(t1,4)*(2558 + 1705*t2) + 
                  Power(t1,3)*(418 + 672*t2 + 658*Power(t2,2)) - 
                  Power(t1,2)*
                   (4537 + 4469*t2 + 6585*Power(t2,2) + 
                     2650*Power(t2,3)) + 
                  t1*(1919 + 6140*t2 + 3756*Power(t2,2) + 
                     5240*Power(t2,3) + 1473*Power(t2,4))) + 
               Power(s2,2)*(-1548 + 232*Power(t1,6) - 815*t2 + 
                  2160*Power(t2,2) - 2400*Power(t2,3) - 
                  3125*Power(t2,4) + 372*Power(t2,5) + 52*Power(t2,6) - 
                  Power(t1,5)*(21 + 109*t2) - 
                  Power(t1,4)*(3567 + 3657*t2 + 1340*Power(t2,2)) + 
                  Power(t1,3)*
                   (7507 + 9653*t2 + 8386*Power(t2,2) + 
                     2260*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (2768 + 5058*t2 + 5424*Power(t2,2) + 
                     3745*Power(t2,3) + 643*Power(t2,4)) + 
                  t1*(3285 + 5044*t2 + 2973*Power(t2,2) + 
                     7661*Power(t2,3) + 2530*Power(t2,4) + 
                     191*Power(t2,5))) - 
               s2*(-400 + 24*Power(t1,7) - 2507*t2 + 2632*Power(t2,2) + 
                  4578*Power(t2,3) - 1253*Power(t2,4) + 86*Power(t2,5) - 
                  12*Power(t2,6) + Power(t1,6)*(341 + 74*t2) - 
                  Power(t1,5)*(2054 + 1773*t2 + 396*Power(t2,2)) + 
                  Power(t1,4)*
                   (4208 + 5411*t2 + 3150*Power(t2,2) + 514*Power(t2,3)) \
- Power(t1,3)*(3215 + 9442*t2 + 6030*Power(t2,2) + 2464*Power(t2,3) + 
                     236*Power(t2,4)) + 
                  Power(t1,2)*
                   (590 + 7826*t2 + 9291*Power(t2,2) + 
                     2862*Power(t2,3) + 589*Power(t2,4)) + 
                  t1*(507 + 684*t2 - 10039*Power(t2,2) - 
                     3384*Power(t2,3) - 275*Power(t2,4) + 
                     169*Power(t2,5) + 20*Power(t2,6))))) - 
         s*(-300 + 1253*t1 - 1894*Power(t1,2) + 1205*Power(t1,3) - 
            49*Power(t1,4) - 602*Power(t1,5) + 601*Power(t1,6) - 
            234*Power(t1,7) + 20*Power(t1,8) + 
            2*Power(s1,7)*s2*(-6 + 9*Power(s2,2) + 9*t1 - 
               4*Power(t1,2) - 3*s2*(1 + t1)) - 317*t2 + 304*t1*t2 + 
            1464*Power(t1,2)*t2 - 3699*Power(t1,3)*t2 + 
            4229*Power(t1,4)*t2 - 2847*Power(t1,5)*t2 + 
            948*Power(t1,6)*t2 - 92*Power(t1,7)*t2 + 8*Power(t1,8)*t2 + 
            654*Power(t2,2) - 3109*t1*Power(t2,2) + 
            6019*Power(t1,2)*Power(t2,2) - 6751*Power(t1,3)*Power(t2,2) + 
            4466*Power(t1,4)*Power(t2,2) - 1356*Power(t1,5)*Power(t2,2) + 
            113*Power(t1,6)*Power(t2,2) - 28*Power(t1,7)*Power(t2,2) + 
            632*Power(t2,3) - 2073*t1*Power(t2,3) + 
            3261*Power(t1,2)*Power(t2,3) - 2448*Power(t1,3)*Power(t2,3) + 
            539*Power(t1,4)*Power(t2,3) + 37*Power(t1,5)*Power(t2,3) + 
            40*Power(t1,6)*Power(t2,3) - 18*Power(t2,4) - 
            234*t1*Power(t2,4) + 137*Power(t1,2)*Power(t2,4) + 
            296*Power(t1,3)*Power(t2,4) - 141*Power(t1,4)*Power(t2,4) - 
            32*Power(t1,5)*Power(t2,4) + 25*Power(t2,5) + 
            103*t1*Power(t2,5) - 205*Power(t1,2)*Power(t2,5) + 
            59*Power(t1,3)*Power(t2,5) + 16*Power(t1,4)*Power(t2,5) - 
            12*Power(t2,6) + 12*t1*Power(t2,6) + 
            4*Power(t1,2)*Power(t2,6) - 4*Power(t1,3)*Power(t2,6) + 
            Power(s2,9)*(6 - 6*t1 + 6*t2) + 
            Power(s2,8)*(-10 + 32*Power(t1,2) - 37*t2 - 37*Power(t2,2) - 
               t1*(22 + 13*t2)) - 
            Power(s2,7)*(65 + 72*Power(t1,3) + 59*t2 + 56*Power(t2,2) + 
               126*Power(t2,3) + Power(t1,2)*(-3 + 50*t2) - 
               t1*(134 + 241*t2 + 312*Power(t2,2))) + 
            Power(s2,6)*(25 + 84*Power(t1,4) - 98*t2 - 354*Power(t2,2) - 
               91*Power(t2,3) + 32*Power(t2,4) + 
               2*Power(t1,3)*(85 + 126*t2) - 
               Power(t1,2)*(635 + 1041*t2 + 888*Power(t2,2)) + 
               t1*(356 + 795*t2 + 666*Power(t2,2) + 436*Power(t2,3))) + 
            Power(s2,5)*(64 - 50*Power(t1,5) - 419*t2 - 
               112*Power(t2,2) + 550*Power(t2,3) + 219*Power(t2,4) + 
               20*Power(t2,5) - 2*Power(t1,4)*(251 + 206*t2) + 
               Power(t1,3)*(1758 + 2157*t2 + 1120*Power(t2,2)) - 
               2*Power(t1,2)*
                (687 + 1383*t2 + 606*Power(t2,2) + 227*Power(t2,3)) + 
               t1*(104 + 1308*t2 + 701*Power(t2,2) - 408*Power(t2,3) - 
                  176*Power(t2,4))) + 
            Power(s2,4)*(-165 + 12*Power(t1,6) + 439*t2 + 
               1359*Power(t2,2) + 1075*Power(t2,3) + 599*Power(t2,4) + 
               274*Power(t2,5) + 45*Power(t2,6) + 
               Power(t1,5)*(644 + 305*t2) - 
               Power(t1,4)*(2510 + 1968*t2 + 595*Power(t2,2)) + 
               Power(t1,3)*(2127 + 2919*t2 - 101*Power(t2,2) - 
                  105*Power(t2,3)) + 
               Power(t1,2)*(55 - 598*t2 + 2047*Power(t2,2) + 
                  2365*Power(t2,3) + 555*Power(t2,4)) - 
               t1*(163 + 877*t2 + 2761*Power(t2,2) + 3217*Power(t2,3) + 
                  1327*Power(t2,4) + 227*Power(t2,5))) + 
            Power(s2,3)*(214 + 1612*t2 + 1982*Power(t2,2) + 
               1197*Power(t2,3) + 1559*Power(t2,4) + 875*Power(t2,5) - 
               127*Power(t2,6) - 4*Power(t2,7) - 
               Power(t1,6)*(395 + 96*t2) + 
               2*Power(t1,5)*(778 + 295*t2 + 24*Power(t2,2)) + 
               Power(t1,4)*(109 + 329*t2 + 1792*Power(t2,2) + 
                  412*Power(t2,3)) - 
               Power(t1,3)*(3762 + 6128*t2 + 6923*Power(t2,2) + 
                  3661*Power(t2,3) + 588*Power(t2,4)) + 
               Power(t1,2)*(3294 + 10152*t2 + 10422*Power(t2,2) + 
                  8237*Power(t2,3) + 2174*Power(t2,4) + 268*Power(t2,5)) \
- t1*(1016 + 6503*t2 + 7577*Power(t2,2) + 5739*Power(t2,3) + 
                  4026*Power(t2,4) + 353*Power(t2,5) + 40*Power(t2,6))) + 
            Power(s2,2)*(462 + 607*t2 + 985*Power(t2,2) + 
               2483*Power(t2,3) + 2230*Power(t2,4) + 348*Power(t2,5) - 
               151*Power(t2,6) + 4*Power(t2,7) + 
               8*Power(t1,7)*(13 + t2) + 
               Power(t1,6)*(-177 + 122*t2 + 48*Power(t2,2)) - 
               Power(t1,5)*(2660 + 2026*t2 + 1354*Power(t2,2) + 
                  195*Power(t2,3)) + 
               Power(t1,4)*(7479 + 10581*t2 + 6176*Power(t2,2) + 
                  2116*Power(t2,3) + 227*Power(t2,4)) - 
               Power(t1,3)*(7728 + 19084*t2 + 15188*Power(t2,2) + 
                  6943*Power(t2,3) + 1161*Power(t2,4) + 99*Power(t2,5)) \
+ Power(t1,2)*(4442 + 14821*t2 + 18243*Power(t2,2) + 
                  11208*Power(t2,3) + 3563*Power(t2,4) + 
                  2*Power(t2,5) + 9*Power(t2,6)) + 
               t1*(-1922 - 5105*t2 - 8654*Power(t2,2) - 
                  9168*Power(t2,3) - 4380*Power(t2,4) - 
                  442*Power(t2,5) + 167*Power(t2,6) + 2*Power(t2,7))) + 
            s2*(-215 - 8*Power(t1,8) - 522*t2 + 749*Power(t2,2) + 
               2544*Power(t2,3) + 1039*Power(t2,4) - 314*Power(t2,5) + 
               87*Power(t2,6) - 16*Power(t2,7) - 
               8*Power(t1,7)*(17 + 9*t2 + Power(t2,2)) + 
               Power(t1,6)*(1741 + 876*t2 + 317*Power(t2,2) + 
                  28*Power(t2,3)) - 
               2*Power(t1,5)*
                (2266 + 2961*t2 + 847*Power(t2,2) + 229*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(t1,4)*(5045 + 13681*t2 + 8117*Power(t2,2) + 
                  1459*Power(t2,3) + 264*Power(t2,4) + 20*Power(t2,5)) + 
               Power(t1,2)*(707 + 5964*t2 + 16453*Power(t2,2) + 
                  10353*Power(t2,3) + 905*Power(t2,4) + 7*Power(t2,5) - 
                  49*Power(t2,6)) - 
               Power(t1,3)*(2900 + 13829*t2 + 17447*Power(t2,2) + 
                  5131*Power(t2,3) + 503*Power(t2,4) + 4*Power(t2,5) + 
                  4*Power(t2,6)) + 
               t1*(298 - 138*t2 - 6584*Power(t2,2) - 8739*Power(t2,3) - 
                  1613*Power(t2,4) + 203*Power(t2,5) + 7*Power(t2,6) + 
                  10*Power(t2,7))) + 
            Power(s1,6)*(16 + 10*Power(s2,4) - 40*t1 + 46*Power(t1,2) - 
               28*Power(t1,3) + 6*Power(t1,4) + 
               Power(s2,3)*(9 + 100*t1 - 152*t2) + 
               s2*(-14 - 16*Power(t1,3) + 64*t2 + 
                  8*Power(t1,2)*(7 + 3*t2) - t1*(13 + 82*t2)) + 
               Power(s2,2)*(-79 - 84*Power(t1,2) + 8*t2 + 
                  t1*(43 + 102*t2))) + 
            Power(s1,5)*(32 - 17*Power(s2,5) + 4*Power(t1,5) + 
               Power(s2,4)*(22 + 132*t1 - 113*t2) - 66*t2 - 
               2*Power(t1,4)*(11 + 10*t2) - 2*Power(t1,2)*(46 + 51*t2) + 
               Power(t1,3)*(83 + 64*t2) + t1*(-3 + 124*t2) + 
               Power(s2,3)*(27 + 55*Power(t1,2) + 76*t2 + 
                  494*Power(t2,2) - t1*(139 + 544*t2)) + 
               s2*(98 + 32*Power(t1,4) + 149*t2 - 124*Power(t2,2) - 
                  3*Power(t1,3)*(87 + 4*t2) + 
                  Power(t1,2)*(511 + 71*t2) + 
                  2*t1*(-158 - 157*t2 + 65*Power(t2,2))) + 
               Power(s2,2)*(-6 - 182*Power(t1,3) + 722*t2 + 
                  46*Power(t2,2) + Power(t1,2)*(588 + 599*t2) - 
                  2*t1*(349 + 321*t2 + 211*Power(t2,2)))) + 
            Power(s1,4)*(-41 + 63*Power(s2,6) + 68*Power(t1,5) - 
               10*Power(t1,6) - 191*t2 + 92*Power(t2,2) + 
               Power(s2,5)*(235 - 206*t1 + 56*t2) + 
               t1*(408 + 331*t2 - 84*Power(t2,2)) + 
               Power(t1,4)*(-225 - 76*t2 + 20*Power(t2,2)) + 
               Power(t1,3)*(680 + 79*t2 + 20*Power(t2,2)) - 
               Power(t1,2)*(872 + 153*t2 + 48*Power(t2,2)) + 
               Power(s2,4)*(213 + 383*Power(t1,2) - 14*t2 + 
                  397*Power(t2,2) - t1*(557 + 695*t2)) - 
               Power(s2,3)*(291 + 122*Power(t1,3) - 585*t2 + 
                  521*Power(t2,2) + 820*Power(t2,3) + 
                  Power(t1,2)*(-324 + 98*t2) + 
                  t1*(546 - 603*t2 - 1136*Power(t2,2))) + 
               s2*(44 + 98*Power(t1,5) - 756*t2 - 369*Power(t2,2) + 
                  80*Power(t2,3) - Power(t1,4)*(677 + 222*t2) + 
                  Power(t1,3)*(1549 + 1688*t2 + 204*Power(t2,2)) + 
                  t1*(1113 + 1891*t2 + 1393*Power(t2,2) - 
                     50*Power(t2,3)) - 
                  Power(t1,2)*
                   (2055 + 2945*t2 + 893*Power(t2,2) + 80*Power(t2,3))) \
+ Power(s2,2)*(-251 - 190*Power(t1,4) + 396*t2 - 2249*Power(t2,2) - 
                  140*Power(t2,3) + Power(t1,3)*(815 + 845*t2) - 
                  Power(t1,2)*(978 + 3058*t2 + 1547*Power(t2,2)) + 
                  t1*(624 + 2902*t2 + 2305*Power(t2,2) + 790*Power(t2,3))\
)) - Power(s1,3)*(103 + 11*Power(s2,7) + 8*Power(t1,7) - 71*t2 - 
               356*Power(t2,2) + 28*Power(t2,3) - 
               8*Power(t1,6)*(11 + 4*t2) + 
               Power(s2,6)*(95 - 265*t1 + 367*t2) + 
               Power(t1,5)*(465 + 292*t2 + 24*Power(t2,2)) - 
               2*Power(t1,4)*(787 + 525*t2 + 172*Power(t2,2)) + 
               2*t1*(162 + 441*t2 + 539*Power(t2,2) + 52*Power(t2,3)) + 
               Power(t1,3)*(2393 + 2484*t2 + 794*Power(t2,2) + 
                  160*Power(t2,3)) - 
               Power(t1,2)*(1643 + 2473*t2 + 1216*Power(t2,2) + 
                  292*Power(t2,3)) + 
               2*Power(s2,5)*
                (177 + 442*Power(t1,2) + 607*t2 + 43*Power(t2,2) - 
                  3*t1*(174 + 221*t2)) + 
               Power(s2,4)*(841 - 1285*Power(t1,3) + 944*t2 + 
                  364*Power(t2,2) + 658*Power(t2,3) + 
                  3*Power(t1,2)*(773 + 828*t2) - 
                  t1*(1851 + 3390*t2 + 1520*Power(t2,2))) + 
               s2*(-485 - 34*Power(t1,6) + 1525*t2 - 1994*Power(t2,2) - 
                  266*Power(t2,3) - 60*Power(t2,4) + 
                  Power(t1,5)*(461 + 252*t2) - 
                  Power(t1,4)*(1237 + 2239*t2 + 454*Power(t2,2)) + 
                  Power(t1,3)*
                   (2327 + 5060*t2 + 3494*Power(t2,2) + 
                     376*Power(t2,3)) + 
                  2*t1*(611 + 602*t2 + 1990*Power(t2,2) + 
                     1086*Power(t2,3) + 45*Power(t2,4)) - 
                  2*Power(t1,2)*
                   (1115 + 2765*t2 + 2881*Power(t2,2) + 
                     871*Power(t2,3) + 60*Power(t2,4))) + 
               Power(s2,3)*(823*Power(t1,4) - 
                  3*Power(t1,3)*(667 + 500*t2) + 
                  Power(t1,2)*(3258 + 3032*t2 + 304*Power(t2,2)) + 
                  t1*(-1973 - 4210*t2 + 622*Power(t2,2) + 
                     1104*Power(t2,3)) - 
                  2*(-144 + 61*t2 - 1396*Power(t2,2) + 
                     572*Power(t2,3) + 375*Power(t2,4))) + 
               Power(s2,2)*(-919 - 146*Power(t1,5) + 
                  Power(t1,4)*(146 - 183*t2) + 1391*t2 + 
                  1500*Power(t2,2) - 3356*Power(t2,3) - 
                  150*Power(t2,4) + 
                  2*Power(t1,3)*(-567 + 932*t2 + 672*Power(t2,2)) - 
                  Power(t1,2)*
                   (-1556 + 1057*t2 + 5644*Power(t2,2) + 
                     1878*Power(t2,3)) + 
                  t1*(47 - 1460*t2 + 4076*Power(t2,2) + 
                     3660*Power(t2,3) + 770*Power(t2,4)))) + 
            Power(s1,2)*(-303 - 57*Power(s2,8) + 
               4*Power(s2,7)*(5 + 82*t1 - 33*t2) + 1104*t2 - 
               37*Power(t2,2) - 242*Power(t2,3) - 48*Power(t2,4) + 
               2*Power(t1,7)*(5 + 6*t2) - 
               Power(t1,6)*(233 + 142*t2 + 34*Power(t2,2)) + 
               Power(t1,5)*(950 + 979*t2 + 348*Power(t2,2) + 
                  32*Power(t2,3)) - 
               Power(t1,4)*(1350 + 2663*t2 + 1566*Power(t2,2) + 
                  356*Power(t2,3) + 10*Power(t2,4)) + 
               Power(t1,3)*(877 + 2198*t2 + 3224*Power(t2,2) + 
                  1078*Power(t2,3) + 140*Power(t2,4)) + 
               t1*(549 - 2213*t2 + 306*Power(t2,2) + 1278*Power(t2,3) + 
                  176*Power(t2,4)) - 
               Power(t1,2)*(492 - 689*t2 + 2193*Power(t2,2) + 
                  1810*Power(t2,3) + 258*Power(t2,4)) + 
               Power(s2,6)*(87 - 637*Power(t1,2) + 199*t2 + 
                  577*Power(t2,2) - 2*t1*(43 + 55*t2)) + 
               Power(s2,5)*(55 + 372*Power(t1,3) + 1776*t2 + 
                  1942*Power(t2,2) + 92*Power(t2,3) + 
                  Power(t1,2)*(1153 + 1640*t2) - 
                  t1*(1351 + 3396*t2 + 2210*Power(t2,2))) + 
               Power(s2,4)*(843 + 310*Power(t1,4) + 3045*t2 + 
                  1848*Power(t2,2) + 956*Power(t2,3) + 
                  572*Power(t2,4) - Power(t1,3)*(3263 + 3319*t2) + 
                  Power(t1,2)*(5516 + 8939*t2 + 4374*Power(t2,2)) - 
                  t1*(2925 + 8555*t2 + 6436*Power(t2,2) + 
                     1710*Power(t2,3))) + 
               Power(s2,3)*(1297 - 498*Power(t1,5) + 1081*t2 + 
                  2188*Power(t2,2) + 4596*Power(t2,3) - 
                  1201*Power(t2,4) - 368*Power(t2,5) + 
                  Power(t1,4)*(3645 + 2612*t2) - 
                  Power(t1,3)*(8469 + 9371*t2 + 3222*Power(t2,2)) + 
                  Power(t1,2)*
                   (7414 + 15979*t2 + 7266*Power(t2,2) + 
                     950*Power(t2,3)) + 
                  t1*(-3993 - 8981*t2 - 10808*Power(t2,2) - 
                     362*Power(t2,3) + 436*Power(t2,4))) + 
               s2*(-1223 - 24*Power(t1,7) + 1760*t2 + 
                  3957*Power(t2,2) - 2426*Power(t2,3) + 
                  136*Power(t2,4) - 128*Power(t2,5) + 
                  Power(t1,6)*(187 + 8*t2) + 
                  2*Power(t1,5)*(-221 + 194*t2 + 87*Power(t2,2)) - 
                  Power(t1,4)*
                   (-434 + 1141*t2 + 2183*Power(t2,2) + 
                     350*Power(t2,3)) + 
                  Power(t1,3)*
                   (-1177 + 391*t2 + 4970*Power(t2,2) + 
                     2964*Power(t2,3) + 264*Power(t2,4)) - 
                  Power(t1,2)*
                   (-446 - 4897*t2 + 3990*Power(t2,2) + 
                     4726*Power(t2,3) + 1418*Power(t2,4) + 
                     72*Power(t2,5)) + 
                  t1*(1714 - 6199*t2 - 2544*Power(t2,2) + 
                     3754*Power(t2,3) + 1493*Power(t2,4) + 
                     122*Power(t2,5))) + 
               Power(s2,2)*(-377 + 206*Power(t1,6) - 25*t2 + 
                  5765*Power(t2,2) + 2184*Power(t2,3) - 
                  2609*Power(t2,4) - 64*Power(t2,5) - 
                  Power(t1,5)*(1642 + 723*t2) + 
                  Power(t1,4)*(5005 + 3062*t2 + 431*Power(t2,2)) + 
                  Power(t1,3)*
                   (-6464 - 9247*t2 + 122*Power(t2,2) + 782*Power(t2,3)) \
+ Power(t1,2)*(4822 + 12332*t2 + 4383*Power(t2,2) - 4464*Power(t2,3) - 
                     1082*Power(t2,4)) + 
                  t1*(-1234 - 6826*t2 - 9172*Power(t2,2) + 
                     1796*Power(t2,3) + 2925*Power(t2,4) + 
                     386*Power(t2,5)))) + 
            s1*(695 - 6*Power(s2,9) - 16*Power(t1,8) - 465*t2 - 
               1633*Power(t2,2) + 25*Power(t2,3) + 20*Power(t2,4) + 
               46*Power(t2,5) + Power(s2,8)*(49 + t1 + 94*t2) - 
               4*Power(t1,7)*(-8 - 8*t2 + Power(t2,2)) + 
               2*Power(t1,6)*(-57 + 38*t2 + 7*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,5)*(473 + 320*t2 - 551*Power(t2,2) - 
                  92*Power(t2,3) - 12*Power(t2,4)) + 
               t1*(-1951 + 2994*t2 + 4610*Power(t2,2) + 
                  402*Power(t2,3) - 631*Power(t2,4) - 84*Power(t2,5)) + 
               Power(t1,3)*(288 + 5864*t2 + 2643*Power(t2,2) - 
                  1716*Power(t2,3) - 505*Power(t2,4) - 32*Power(t2,5)) + 
               Power(t1,4)*(-902 - 2826*t2 + 550*Power(t2,2) + 
                  882*Power(t2,3) + 94*Power(t2,4) + 4*Power(t2,5)) + 
               Power(t1,2)*(1497 - 6011*t2 - 5593*Power(t2,2) + 
                  455*Power(t2,3) + 1044*Power(t2,4) + 66*Power(t2,5)) + 
               Power(s2,7)*(104 + 123*Power(t1,2) + 56*t2 + 
                  269*Power(t2,2) - t1*(359 + 660*t2)) - 
               Power(s2,6)*(110 + 425*Power(t1,3) - 299*t2 + 
                  13*Power(t2,2) + 305*Power(t2,3) - 
                  Power(t1,2)*(1319 + 1601*t2) + 
                  t1*(692 + 688*t2 + 591*Power(t2,2))) + 
               Power(s2,5)*(1 + 617*Power(t1,4) - 119*t2 - 
                  1972*Power(t2,2) - 1182*Power(t2,3) - 65*Power(t2,4) - 
                  18*Power(t1,3)*(121 + 89*t2) + 
                  Power(t1,2)*(1187 + 127*t2 - 302*Power(t2,2)) + 
                  t1*(505 + 868*t2 + 2760*Power(t2,2) + 1266*Power(t2,3))\
) + Power(s2,4)*(-52 - 432*Power(t1,5) - 2562*t2 - 3279*Power(t2,2) - 
                  1716*Power(t2,3) - 874*Power(t2,4) - 253*Power(t2,5) + 
                  Power(t1,4)*(1292 + 359*t2) + 
                  Power(t1,3)*(1278 + 3652*t2 + 2139*Power(t2,2)) - 
                  Power(t1,2)*
                   (4061 + 8857*t2 + 8985*Power(t2,2) + 
                     2828*Power(t2,3)) + 
                  t1*(1755 + 6978*t2 + 9921*Power(t2,2) + 
                     4930*Power(t2,3) + 980*Power(t2,4))) + 
               Power(s2,3)*(-1018 + 134*Power(t1,6) - 3163*t2 - 
                  1990*Power(t2,2) - 3578*Power(t2,3) - 
                  3291*Power(t2,4) + 620*Power(t2,5) + 82*Power(t2,6) + 
                  Power(t1,5)*(335 + 428*t2) - 
                  Power(t1,4)*(4910 + 5973*t2 + 2201*Power(t2,2)) + 
                  Power(t1,3)*
                   (10080 + 17450*t2 + 11031*Power(t2,2) + 
                     2432*Power(t2,3)) - 
                  Power(t1,2)*
                   (8085 + 20016*t2 + 20958*Power(t2,2) + 
                     6732*Power(t2,3) + 871*Power(t2,4)) + 
                  t1*(3508 + 12134*t2 + 12747*Power(t2,2) + 
                     11170*Power(t2,3) + 873*Power(t2,4) + 16*Power(t2,5)\
)) + s2*(837 + 502*t2 - 4789*Power(t2,2) - 3515*Power(t2,3) + 
                  1404*Power(t2,4) - 255*Power(t2,5) + 76*Power(t2,6) + 
                  32*Power(t1,7)*(6 + t2) - 
                  2*Power(t1,6)*(552 + 310*t2 + 35*Power(t2,2)) + 
                  Power(t1,5)*
                   (3327 + 2542*t2 + 531*Power(t2,2) + 16*Power(t2,3)) + 
                  Power(t1,4)*
                   (-5645 - 8415*t2 - 1555*Power(t2,2) + 
                     357*Power(t2,3) + 66*Power(t2,4)) + 
                  Power(t1,3)*
                   (5062 + 17048*t2 + 7067*Power(t2,2) - 
                     956*Power(t2,3) - 893*Power(t2,4) - 60*Power(t2,5)) \
+ Power(t1,2)*(-1411 - 15171*t2 - 17480*Power(t2,2) - 390*Power(t2,3) + 
                     1391*Power(t2,4) + 491*Power(t2,5) + 16*Power(t2,6)\
) - 2*t1*(648 - 2132*t2 - 8080*Power(t2,2) - 2124*Power(t2,3) + 
                     776*Power(t2,4) + 197*Power(t2,5) + 29*Power(t2,6))) \
- Power(s2,2)*(640 + 12*Power(t1,7) + 178*t2 + 3377*Power(t2,2) + 
                  6353*Power(t2,3) + 1422*Power(t2,4) - 
                  1010*Power(t2,5) - 2*Power(t2,6) + 
                  Power(t1,6)*(634 + 252*t2) - 
                  Power(t1,5)*(4129 + 3366*t2 + 772*Power(t2,2)) + 
                  Power(t1,4)*
                   (9676 + 12557*t2 + 5032*Power(t2,2) + 651*Power(t2,3)) \
+ Power(t1,3)*(-11003 - 22624*t2 - 15056*Power(t2,2) - 
                     2088*Power(t2,3) + 2*Power(t2,4)) + 
                  Power(t1,2)*
                   (7151 + 21977*t2 + 21984*Power(t2,2) + 
                     8025*Power(t2,3) - 1288*Power(t2,4) - 
                     227*Power(t2,5)) + 
                  t1*(-3057 - 8402*t2 - 16041*Power(t2,2) - 
                     11468*Power(t2,3) - 518*Power(t2,4) + 
                     1138*Power(t2,5) + 82*Power(t2,6))))))*
       R1(1 - s + s2 - t1))/
     ((-1 + s2)*(-s + s2 - t1)*(1 - s + s2 - t1)*(-1 + t1)*
       (-1 + s1 + t1 - t2)*Power(-s1 + s2 - t1 + t2,3)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) - 
    (8*(-24*Power(s2,3) + 8*Power(s2,4) + 72*Power(s2,2)*t1 + 
         48*Power(s2,3)*t1 - 24*Power(s2,4)*t1 - 72*s2*Power(t1,2) - 
         192*Power(s2,2)*Power(t1,2) + 24*Power(s2,4)*Power(t1,2) + 
         24*Power(t1,3) + 208*s2*Power(t1,3) + 
         144*Power(s2,2)*Power(t1,3) - 48*Power(s2,3)*Power(t1,3) - 
         8*Power(s2,4)*Power(t1,3) - 72*Power(t1,4) - 
         192*s2*Power(t1,4) + 24*Power(s2,3)*Power(t1,4) + 
         72*Power(t1,5) + 48*s2*Power(t1,5) - 
         24*Power(s2,2)*Power(t1,5) - 24*Power(t1,6) + 8*s2*Power(t1,6) + 
         Power(s1,9)*s2*(-2 + s2 + t1) - 72*Power(s2,2)*t2 + 
         24*Power(s2,3)*t2 - 36*Power(s2,4)*t2 + 8*Power(s2,5)*t2 + 
         144*s2*t1*t2 + 168*Power(s2,2)*t1*t2 + 8*Power(s2,3)*t1*t2 + 
         64*Power(s2,4)*t1*t2 - 16*Power(s2,5)*t1*t2 - 
         72*Power(t1,2)*t2 - 408*s2*Power(t1,2)*t2 - 
         96*Power(s2,2)*Power(t1,2)*t2 - 112*Power(s2,3)*Power(t1,2)*t2 - 
         20*Power(s2,4)*Power(t1,2)*t2 + 8*Power(s2,5)*Power(t1,2)*t2 + 
         216*Power(t1,3)*t2 + 312*s2*Power(t1,3)*t2 + 
         16*Power(s2,2)*Power(t1,3)*t2 + 104*Power(s2,3)*Power(t1,3)*t2 - 
         8*Power(s2,4)*Power(t1,3)*t2 - 188*Power(t1,4)*t2 + 
         8*s2*Power(t1,4)*t2 - 56*Power(s2,2)*Power(t1,4)*t2 - 
         24*Power(s2,3)*Power(t1,4)*t2 + 16*Power(t1,5)*t2 - 
         40*s2*Power(t1,5)*t2 + 40*Power(s2,2)*Power(t1,5)*t2 + 
         28*Power(t1,6)*t2 - 16*s2*Power(t1,6)*t2 + 24*Power(t2,2) - 
         86*s2*Power(t2,2) + 6*Power(s2,2)*Power(t2,2) - 
         88*Power(s2,3)*Power(t2,2) - 4*Power(s2,5)*Power(t2,2) + 
         26*t1*Power(t2,2) + 244*s2*t1*Power(t2,2) + 
         66*Power(s2,2)*t1*Power(t2,2) + 272*Power(s2,3)*t1*Power(t2,2) - 
         44*Power(s2,4)*t1*Power(t2,2) + 16*Power(s2,5)*t1*Power(t2,2) - 
         214*Power(t1,2)*Power(t2,2) - 112*s2*Power(t1,2)*Power(t2,2) - 
         368*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         108*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         16*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         12*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         146*Power(t1,3)*Power(t2,2) - 24*s2*Power(t1,3)*Power(t2,2) + 
         302*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         64*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         28*Power(s2,4)*Power(t1,3)*Power(t2,2) + 
         108*Power(t1,4)*Power(t2,2) - 58*s2*Power(t1,4)*Power(t2,2) + 
         6*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         12*Power(s2,3)*Power(t1,4)*Power(t2,2) - 
         88*Power(t1,5)*Power(t2,2) + 28*s2*Power(t1,5)*Power(t2,2) - 
         12*Power(s2,2)*Power(t1,5)*Power(t2,2) - 
         2*Power(t1,6)*Power(t2,2) + 8*s2*Power(t1,6)*Power(t2,2) - 
         62*Power(t2,3) + 31*s2*Power(t2,3) - 
         90*Power(s2,2)*Power(t2,3) - 86*Power(s2,3)*Power(t2,3) + 
         40*Power(s2,4)*Power(t2,3) - 32*Power(s2,5)*Power(t2,3) + 
         8*Power(s2,6)*Power(t2,3) + 227*t1*Power(t2,3) - 
         100*s2*t1*Power(t2,3) + 418*Power(s2,2)*t1*Power(t2,3) - 
         156*Power(s2,3)*t1*Power(t2,3) + 
         106*Power(s2,4)*t1*Power(t2,3) - 16*Power(s2,5)*t1*Power(t2,3) - 
         4*Power(s2,6)*t1*Power(t2,3) - 84*Power(t1,2)*Power(t2,3) - 
         128*s2*Power(t1,2)*Power(t2,3) + 
         82*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         66*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         38*Power(s2,4)*Power(t1,2)*Power(t2,3) + 
         16*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         190*Power(t1,3)*Power(t2,3) - 24*s2*Power(t1,3)*Power(t2,3) - 
         396*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         64*Power(s2,3)*Power(t1,3)*Power(t2,3) - 
         24*Power(s2,4)*Power(t1,3)*Power(t2,3) + 
         130*Power(t1,4)*Power(t2,3) + 273*s2*Power(t1,4)*Power(t2,3) + 
         34*Power(s2,2)*Power(t1,4)*Power(t2,3) + 
         16*Power(s2,3)*Power(t1,4)*Power(t2,3) - 
         25*Power(t1,5)*Power(t2,3) - 52*s2*Power(t1,5)*Power(t2,3) - 
         4*Power(s2,2)*Power(t1,5)*Power(t2,3) - 49*Power(t2,4) - 
         38*s2*Power(t2,4) - 43*Power(s2,2)*Power(t2,4) + 
         142*Power(s2,3)*Power(t2,4) - 114*Power(s2,4)*Power(t2,4) + 
         60*Power(s2,5)*Power(t2,4) - 8*Power(s2,6)*Power(t2,4) - 
         102*t1*Power(t2,4) + 263*s2*t1*Power(t2,4) - 
         565*Power(s2,2)*t1*Power(t2,4) + 90*Power(s2,3)*t1*Power(t2,4) - 
         162*Power(s2,4)*t1*Power(t2,4) + 6*Power(s2,5)*t1*Power(t2,4) + 
         4*Power(s2,6)*t1*Power(t2,4) + 264*Power(t1,2)*Power(t2,4) + 
         431*s2*Power(t1,2)*Power(t2,4) + 
         485*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         236*Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         44*Power(s2,4)*Power(t1,2)*Power(t2,4) - 
         12*Power(s2,5)*Power(t1,2)*Power(t2,4) - 
         212*Power(t1,3)*Power(t2,4) - 685*s2*Power(t1,3)*Power(t2,4) - 
         191*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         94*Power(s2,3)*Power(t1,3)*Power(t2,4) + 
         12*Power(s2,4)*Power(t1,3)*Power(t2,4) + 
         109*Power(t1,4)*Power(t2,4) + 73*s2*Power(t1,4)*Power(t2,4) + 
         48*Power(s2,2)*Power(t1,4)*Power(t2,4) - 
         4*Power(s2,3)*Power(t1,4)*Power(t2,4) + 
         8*Power(t1,5)*Power(t2,4) + 4*s2*Power(t1,5)*Power(t2,4) + 
         96*Power(t2,5) - 76*s2*Power(t2,5) + 
         215*Power(s2,2)*Power(t2,5) - 61*Power(s2,3)*Power(t2,5) + 
         158*Power(s2,4)*Power(t2,5) - 24*Power(s2,5)*Power(t2,5) - 
         159*t1*Power(t2,5) - 586*s2*t1*Power(t2,5) - 
         57*Power(s2,2)*t1*Power(t2,5) - 500*Power(s2,3)*t1*Power(t2,5) + 
         6*Power(s2,5)*t1*Power(t2,5) + 182*Power(t1,2)*Power(t2,5) + 
         584*s2*Power(t1,2)*Power(t2,5) + 
         477*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
         65*Power(s2,3)*Power(t1,2)*Power(t2,5) - 
         8*Power(s2,4)*Power(t1,2)*Power(t2,5) - 
         123*Power(t1,3)*Power(t2,5) - 58*s2*Power(t1,3)*Power(t2,5) - 
         45*Power(s2,2)*Power(t1,3)*Power(t2,5) + 
         6*Power(s2,3)*Power(t1,3)*Power(t2,5) - 
         28*Power(t1,4)*Power(t2,5) - 20*s2*Power(t1,4)*Power(t2,5) - 
         4*Power(s2,2)*Power(t1,4)*Power(t2,5) - 3*Power(t2,6) + 
         247*s2*Power(t2,6) - 61*Power(s2,2)*Power(t2,6) + 
         210*Power(s2,3)*Power(t2,6) - 9*Power(s2,4)*Power(t2,6) - 
         44*t1*Power(t2,6) - 146*s2*t1*Power(t2,6) - 
         488*Power(s2,2)*t1*Power(t2,6) + 37*Power(s2,3)*t1*Power(t2,6) + 
         3*Power(s2,4)*t1*Power(t2,6) + 43*Power(t1,2)*Power(t2,6) + 
         55*s2*Power(t1,2)*Power(t2,6) - 
         49*Power(s2,2)*Power(t1,2)*Power(t2,6) - 
         7*Power(s2,3)*Power(t1,2)*Power(t2,6) + 
         32*Power(t1,3)*Power(t2,6) + 40*s2*Power(t1,3)*Power(t2,6) + 
         12*Power(s2,2)*Power(t1,3)*Power(t2,6) + 4*Power(t2,7) - 
         38*s2*Power(t2,7) + 180*Power(s2,2)*Power(t2,7) - 
         28*Power(s2,3)*Power(t2,7) - 5*Power(s2,4)*Power(t2,7) - 
         4*t1*Power(t2,7) - 42*s2*t1*Power(t2,7) + 
         87*Power(s2,2)*t1*Power(t2,7) + 4*Power(s2,3)*t1*Power(t2,7) - 
         12*Power(t1,2)*Power(t2,7) - 38*s2*Power(t1,2)*Power(t2,7) - 
         12*Power(s2,2)*Power(t1,2)*Power(t2,7) + 2*Power(t2,8) + 
         16*s2*Power(t2,8) - 37*Power(s2,2)*Power(t2,8) + 
         Power(s2,3)*Power(t2,8) + 16*s2*t1*Power(t2,8) + 
         4*Power(s2,2)*t1*Power(t2,8) - 2*s2*Power(t2,9) + 
         Power(s1,8)*(2 + Power(s2,3) - 3*t1 + 5*Power(t1,2) - 
            2*Power(t1,3) + Power(s2,2)*(7 + 8*t1 - 10*t2) + 
            s2*(-16 + 3*Power(t1,2) + t1*(5 - 7*t2) + 15*t2)) - 
         2*Power(s,7)*(t1 - t2)*
          (-6 + 2*t1 + 2*Power(t1,2) + Power(t1,3) + 
            Power(s2,2)*(-7 + 6*t1 - 6*t2) - 2*t2 - 4*t1*t2 - 
            3*Power(t1,2)*t2 + 2*Power(t2,2) + 3*t1*Power(t2,2) - 
            Power(t2,3) - Power(s1,2)*(-1 + s2 - t1 + t2) + 
            s2*(13 - 7*t1 - 4*Power(t1,2) + 7*t2 + 8*t1*t2 - 
               4*Power(t2,2)) + 
            s1*(5 + 7*Power(s2,2) + 3*t1 + 2*Power(t1,2) - 3*t2 - 
               4*t1*t2 + 2*Power(t2,2) + s2*(-12 - 5*t1 + 5*t2))) + 
         Power(s1,7)*(15 + 2*Power(s2,4) - 4*Power(t1,4) + 
            Power(t1,2)*(13 - 26*t2) + Power(s2,3)*(-3 + 2*t1 - 12*t2) - 
            13*t2 + t1*(-19 + 11*t2) + Power(t1,3)*(7 + 12*t2) + 
            Power(s2,2)*(7 + 13*Power(t1,2) - 24*t2 + 42*Power(t2,2) - 
               4*t1*(7 + 15*t2)) + 
            s2*(-22 + 3*Power(t1,3) + Power(t1,2)*(58 - 16*t2) + 
               119*t2 - 47*Power(t2,2) + 21*t1*(-4 - 3*t2 + Power(t2,2)))\
) - Power(s1,6)*(-15 + 2*Power(t1,5) + Power(t1,4)*(3 - 20*t2) + 95*t2 - 
            38*Power(t2,2) + Power(s2,4)*(-8 + 19*t2) + 
            Power(t1,2)*(135 + 105*t2 - 51*Power(t2,2)) + 
            t1*(-68 - 121*t2 + 3*Power(t2,2)) + 
            5*Power(t1,3)*(-17 + 5*t2 + 6*Power(t2,2)) + 
            Power(s2,3)*(-57 + 9*Power(t1,2) + 21*t2 - 52*Power(t2,2) + 
               t1*(-30 + 7*t2)) + 
            Power(s2,2)*(29 - 24*Power(t1,3) - 108*t2 + 
               40*Power(t2,2) + 98*Power(t2,3) + 
               Power(t1,2)*(101 + 96*t2) - 
               7*t1*(-6 + 39*t2 + 28*Power(t2,2))) + 
            s2*(-41 + 5*Power(t1,4) - 128*t2 + 362*Power(t2,2) - 
               77*Power(t2,3) + Power(t1,3)*(-103 + 7*t2) + 
               Power(t1,2)*(153 + 423*t2 - 33*Power(t2,2)) + 
               t1*(40 - 484*t2 - 289*Power(t2,2) + 35*Power(t2,3)))) + 
         Power(s1,5)*(-49 - 4*Power(s2,6) + 
            2*Power(s2,5)*(5 + 9*t1 - 2*t2) - 96*t2 + 246*Power(t2,2) - 
            67*Power(t2,3) + Power(t1,5)*(-9 + 8*t2) + 
            Power(t1,4)*(123 + 24*t2 - 40*Power(t2,2)) + 
            Power(t1,2)*(6 + 619*t2 + 342*Power(t2,2) - 
               40*Power(t2,3)) + 
            Power(t1,3)*(-165 - 457*t2 + 20*Power(t2,2) + 
               40*Power(t2,3)) - 
            t1*(-126 + 266*t2 + 316*Power(t2,2) + 45*Power(t2,3)) + 
            Power(s2,4)*(-78 - 32*Power(t1,2) - 39*t2 + 
               70*Power(t2,2) + t1*(10 + 13*t2)) + 
            Power(s2,3)*(35 + 12*Power(t1,3) - 466*t2 + 
               178*Power(t2,2) - 116*Power(t2,3) + 
               Power(t1,2)*(-1 + 33*t2) + 
               t1*(206 - 205*t2 + Power(t2,2))) + 
            Power(s2,2)*(-186 + 12*Power(t1,4) + 213*t2 - 
               825*Power(t2,2) + 352*Power(t2,3) + 140*Power(t2,4) - 
               2*Power(t1,3)*(29 + 64*t2) + 
               Power(t1,2)*(-100 + 607*t2 + 297*Power(t2,2)) + 
               t1*(84 + 618*t2 - 1032*Power(t2,2) - 364*Power(t2,3))) - 
            s2*(-81 + 6*Power(t1,5) + 448*t2 + 272*Power(t2,2) - 
               569*Power(t2,3) + 63*Power(t2,4) - 
               6*Power(t1,4)*(12 + 5*t2) + 
               Power(t1,3)*(161 + 591*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(165 - 727*t2 - 1283*Power(t2,2) + 
                  30*Power(t2,3)) + 
               t1*(-179 - 376*t2 + 1118*Power(t2,2) + 691*Power(t2,3) - 
                  35*Power(t2,4)))) + 
         Power(s1,4)*(-49 - 4*Power(t1,6) + 294*t2 + 231*Power(t2,2) - 
            330*Power(t2,3) + 80*Power(t2,4) + 
            8*Power(s2,6)*(-1 + 2*t2) + 
            Power(t1,5)*(66 + 37*t2 - 12*Power(t2,2)) + 
            2*Power(s2,5)*(23 + 2*Power(t1,2) + t1*(6 - 35*t2) - 
               32*t2 + 8*Power(t2,2)) + 
            Power(t1,4)*(-26 - 519*t2 - 66*Power(t2,2) + 
               40*Power(t2,3)) + 
            Power(t1,3)*(-243 + 532*t2 + 1010*Power(t2,2) + 
               30*Power(t2,3) - 30*Power(t2,4)) + 
            Power(t1,2)*(399 + 183*t2 - 1083*Power(t2,2) - 
               590*Power(t2,3) - 5*Power(t2,4)) + 
            t1*(-125 - 687*t2 + 340*Power(t2,2) + 430*Power(t2,3) + 
               95*Power(t2,4)) - 
            Power(s2,4)*(78 + 12*Power(t1,3) - 462*t2 - 
               67*Power(t2,2) + 135*Power(t2,3) - 
               2*Power(t1,2)*(20 + 67*t2) + 
               t1*(138 + 38*t2 + 49*Power(t2,2))) + 
            Power(s2,3)*(144 + 12*Power(t1,4) - 175*t2 + 
               1504*Power(t2,2) - 470*Power(t2,3) + 150*Power(t2,4) - 
               10*Power(t1,3)*(11 + 6*t2) + 
               Power(t1,2)*(230 + 63*t2 - 49*Power(t2,2)) + 
               t1*(-10 - 1326*t2 + 557*Power(t2,2) + 30*Power(t2,3))) - 
            Power(s2,2)*(42 + 4*Power(t1,5) - 964*t2 + 
               623*Power(t2,2) - 2260*Power(t2,3) + 810*Power(t2,4) + 
               126*Power(t2,5) + Power(t1,4)*(-64 + 42*t2) - 
               2*Power(t1,3)*(-62 + 97*t2 + 142*Power(t2,2)) + 
               Power(t1,2)*(-376 - 888*t2 + 1467*Power(t2,2) + 
                  500*Power(t2,3)) + 
               t1*(436 + 434*t2 + 2540*Power(t2,2) - 2045*Power(t2,3) - 
                  420*Power(t2,4))) + 
            s2*(101 - 414*t2 + 1629*Power(t2,2) + 210*Power(t2,3) - 
               460*Power(t2,4) + 7*Power(t2,5) + 
               Power(t1,5)*(6 + 22*t2) - 
               2*Power(t1,4)*(28 + 156*t2 + 35*Power(t2,2)) + 
               Power(t1,3)*(-341 + 584*t2 + 1374*Power(t2,2) + 
                  50*Power(t2,3)) + 
               Power(t1,2)*(375 + 1230*t2 - 1323*Power(t2,2) - 
                  2100*Power(t2,3) + 5*Power(t2,4)) - 
               t1*(69 + 1262*t2 + 1250*Power(t2,2) - 1270*Power(t2,3) - 
                  975*Power(t2,4) + 21*Power(t2,5)))) + 
         Power(s1,3)*(26 + 208*t2 - 684*Power(t2,2) - 264*Power(t2,3) + 
            235*Power(t2,4) - 67*Power(t2,5) + 
            6*Power(t1,6)*(1 + 2*t2) - 
            4*Power(s2,6)*(1 + (-8 + t1)*t2 + 6*Power(t2,2)) + 
            Power(t1,5)*(43 - 204*t2 - 57*Power(t2,2) + 8*Power(t2,3)) + 
            Power(t1,4)*(-270 - 39*t2 + 847*Power(t2,2) + 
               84*Power(t2,3) - 20*Power(t2,4)) + 
            Power(t1,3)*(386 + 951*t2 - 483*Power(t2,2) - 
               1170*Power(t2,3) - 65*Power(t2,4) + 12*Power(t2,5)) + 
            Power(t1,2)*(-50 - 1449*t2 - 767*Power(t2,2) + 
               842*Power(t2,3) + 585*Power(t2,4) + 30*Power(t2,5)) - 
            t1*(137 - 449*t2 - 1464*Power(t2,2) + 60*Power(t2,3) + 
               315*Power(t2,4) + 87*Power(t2,5)) - 
            2*Power(s2,5)*(-16 + 2*Power(t1,2) + 97*t2 - 
               78*Power(t2,2) + 12*Power(t2,3) + 
               t1*(-2 + 23*t2 - 48*Power(t2,2))) + 
            2*Power(s2,4)*(-21 + 175*t2 - 538*Power(t2,2) - 
               19*Power(t2,3) + 75*Power(t2,4) + 
               4*Power(t1,3)*(2 + 3*t2) + 
               Power(t1,2)*(26 - 75*t2 - 101*Power(t2,2)) + 
               t1*(-55 + 280*t2 + 27*Power(t2,2) + 33*Power(t2,3))) - 
            Power(s2,3)*(-62 + 574*t2 - 376*Power(t2,2) + 
               2496*Power(t2,3) - 625*Power(t2,4) + 116*Power(t2,5) + 
               8*Power(t1,4)*(3 + 4*t2) - 
               2*Power(t1,3)*(-39 + 203*t2 + 51*Power(t2,2)) + 
               Power(t1,2)*(46 + 906*t2 + 248*Power(t2,2) - 
                  46*Power(t2,3)) + 
               2*t1*(-91 + 31*t2 - 1621*Power(t2,2) + 389*Power(t2,3) + 
                  30*Power(t2,4))) + 
            Power(s2,2)*(20 + 153*t2 - 1991*Power(t2,2) + 
               942*Power(t2,3) - 3195*Power(t2,4) + 968*Power(t2,5) + 
               70*Power(t2,6) + 4*Power(t1,5)*(4 + 3*t2) + 
               2*Power(t1,4)*(-7 - 115*t2 + 29*Power(t2,2)) + 
               Power(t1,3)*(392 + 557*t2 - 189*Power(t2,2) - 
                  336*Power(t2,3)) + 
               Power(t1,2)*(-288 - 1631*t2 - 2541*Power(t2,2) + 
                  1818*Power(t2,3) + 495*Power(t2,4)) + 
               t1*(-170 + 1903*t2 + 855*Power(t2,2) + 
                  4820*Power(t2,3) - 2340*Power(t2,4) - 308*Power(t2,5))\
) + s2*(-25 - 4*Power(t1,6) - 273*t2 + 832*Power(t2,2) - 
               2856*Power(t2,3) + 90*Power(t2,4) + 121*Power(t2,5) + 
               35*Power(t2,6) + 
               Power(t1,5)*(34 - 24*t2 - 30*Power(t2,2)) + 
               Power(t1,4)*(-303 + 91*t2 + 524*Power(t2,2) + 
                  80*Power(t2,3)) + 
               Power(t1,3)*(358 + 1734*t2 - 728*Power(t2,2) - 
                  1646*Power(t2,3) - 65*Power(t2,4)) + 
               2*Power(t1,2)*
                (-146 - 804*t2 - 1642*Power(t2,2) + 541*Power(t2,3) + 
                  1000*Power(t2,4) + 6*Power(t2,5)) + 
               t1*(232 - 16*t2 + 3298*Power(t2,2) + 2040*Power(t2,3) - 
                  640*Power(t2,4) - 845*Power(t2,5) + 7*Power(t2,6)))) + 
         Power(s1,2)*(24 - 114*t2 - 318*Power(t2,2) + 778*Power(t2,3) + 
            141*Power(t2,4) - 75*Power(t2,5) + 38*Power(t2,6) - 
            6*Power(t1,6)*(-1 + 2*t2 + 2*Power(t2,2)) + 
            Power(t1,5)*(-120 - 111*t2 + 218*Power(t2,2) + 
               39*Power(t2,3) - 2*Power(t2,4)) + 
            Power(t1,4)*(148 + 670*t2 + 265*Power(t2,2) - 
               657*Power(t2,3) - 51*Power(t2,4) + 4*Power(t2,5)) + 
            Power(t1,2)*(-214 + 16*t2 + 1965*Power(t2,2) + 
               1143*Power(t2,3) - 193*Power(t2,4) - 333*Power(t2,5) - 
               19*Power(t2,6)) + 
            Power(t1,3)*(130 - 962*t2 - 1385*Power(t2,2) - 
               93*Power(t2,3) + 745*Power(t2,4) + 43*Power(t2,5) - 
               2*Power(t2,6)) + 
            t1*(26 + 501*t2 - 625*Power(t2,2) - 1530*Power(t2,3) - 
               220*Power(t2,4) + 109*Power(t2,5) + 39*Power(t2,6)) + 
            4*Power(s2,6)*t2*
             (t1*(-1 + 3*t2) + 4*(1 - 3*t2 + Power(t2,2))) - 
            2*Power(s2,5)*(4 + 48*t2 - 155*Power(t2,2) + 
               92*Power(t2,3) - 8*Power(t2,4) + 
               4*Power(t1,2)*(2 - 3*t2 + 3*Power(t2,2)) + 
               t1*(-12 + 12*t2 - 31*Power(t2,2) + 24*Power(t2,3))) - 
            Power(s2,4)*(Power(t1,3)*(-44 + 56*t2) - 
               2*Power(t1,2)*
                (-8 - 71*t2 + 112*Power(t2,2) + 61*Power(t2,3)) + 
               t1*(28 - 326*t2 + 868*Power(t2,2) + 34*Power(t2,3) + 
                  34*Power(t2,4)) + 
               t2*(-124 + 580*t2 - 1236*Power(t2,2) + 18*Power(t2,3) + 
                  97*Power(t2,4))) + 
            Power(s2,3)*(-72 - 210*t2 + 858*Power(t2,2) - 
               428*Power(t2,3) + 2269*Power(t2,4) - 457*Power(t2,5) + 
               52*Power(t2,6) + 
               4*Power(t1,4)*(-9 + 16*t2 + 6*Power(t2,2)) - 
               2*Power(t1,3)*
                (12 - 110*t2 + 288*Power(t2,2) + 33*Power(t2,3)) + 
               Power(t1,2)*(-100 + 158*t2 + 1358*Power(t2,2) + 
                  376*Power(t2,3) - 39*Power(t2,4)) + 
               t1*(232 - 520*t2 + 244*Power(t2,2) - 3830*Power(t2,3) + 
                  592*Power(t2,4) + 53*Power(t2,5))) - 
            Power(s2,2)*(-6 + 130*t2 + 223*Power(t2,2) - 
               2049*Power(t2,3) + 783*Power(t2,4) - 2508*Power(t2,5) + 
               656*Power(t2,6) + 22*Power(t2,7) + 
               4*Power(t1,5)*(-1 + 9*t2 + 3*Power(t2,2)) + 
               Power(t1,4)*(2 - 62*t2 - 316*Power(t2,2) + 
                  42*Power(t2,3)) + 
               Power(t1,3)*(-222 + 1180*t2 + 933*Power(t2,2) + 
                  17*Power(t2,3) - 224*Power(t2,4)) + 
               Power(t1,2)*(248 - 658*t2 - 2619*Power(t2,2) - 
                  3295*Power(t2,3) + 1207*Power(t2,4) + 288*Power(t2,5)\
) + t1*(-18 - 758*t2 + 3063*Power(t2,2) + 801*Power(t2,3) + 
                  4770*Power(t2,4) - 1563*Power(t2,5) - 140*Power(t2,6))\
) + s2*(-86 + 81*t2 + 205*Power(t2,2) - 822*Power(t2,3) + 
               2659*Power(t2,4) - 268*Power(t2,5) + 86*Power(t2,6) - 
               33*Power(t2,7) + Power(t1,6)*(4 + 8*t2) + 
               2*Power(t1,5)*
                (6 - 60*t2 + 17*Power(t2,2) + 9*Power(t2,3)) + 
               Power(t1,4)*(34 + 879*t2 + 59*Power(t2,2) - 
                  420*Power(t2,3) - 45*Power(t2,4)) + 
               Power(t1,3)*(-144 - 740*t2 - 3130*Power(t2,2) + 
                  290*Power(t2,3) + 1059*Power(t2,4) + 37*Power(t2,5)) + 
               Power(t1,2)*(-64 + 456*t2 + 2522*Power(t2,2) + 
                  4122*Power(t2,3) - 283*Power(t2,4) - 
                  1103*Power(t2,5) - 9*Power(t2,6)) - 
               t1*(-244 + 564*t2 - 502*Power(t2,2) + 4112*Power(t2,3) + 
                  1780*Power(t2,4) + 16*Power(t2,5) - 443*Power(t2,6) + 
                  Power(t2,7)))) + 
         s1*(Power(t1,6)*(-28 - 4*t2 + 6*Power(t2,2) + 4*Power(t2,3)) + 
            Power(t1,5)*(-16 + 208*t2 + 93*Power(t2,2) - 
               88*Power(t2,3) - 10*Power(t2,4)) + 
            Power(t1,4)*(188 - 256*t2 - 530*Power(t2,2) - 
               309*Power(t2,3) + 234*Power(t2,4) + 12*Power(t2,5)) + 
            Power(t1,3)*(-216 - 276*t2 + 766*Power(t2,2) + 
               889*Power(t2,3) + 332*Power(t2,4) - 245*Power(t2,5) - 
               10*Power(t2,6)) + 
            t1*t2*(-52 - 591*t2 + 403*Power(t2,2) + 786*Power(t2,3) + 
               182*Power(t2,4) - 6*Power(t2,5) - 7*Power(t2,6)) - 
            t2*(48 - 150*t2 - 208*Power(t2,2) + 435*Power(t2,3) + 
               24*Power(t2,4) + 13*Power(t2,6)) + 
            Power(t1,2)*(72 + 428*t2 + 118*Power(t2,2) - 
               1179*Power(t2,3) - 747*Power(t2,4) - 93*Power(t2,5) + 
               100*Power(t2,6) + 4*Power(t2,7)) - 
            4*Power(s2,6)*Power(t2,2)*
             (5 - 8*t2 + Power(t2,2) + t1*(-2 + 3*t2)) + 
            2*Power(s2,5)*(-4 + 6*t2 + 48*Power(t2,2) - 
               111*Power(t2,3) + 53*Power(t2,4) - 2*Power(t2,5) + 
               2*Power(t1,2)*
                (-2 + 7*t2 - 9*Power(t2,2) + 8*Power(t2,3)) - 
               t1*(-8 + 20*t2 - 18*Power(t2,2) + 17*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s2,4)*(36 - 122*Power(t2,2) + 422*Power(t2,3) - 
               702*Power(t2,4) + 29*Power(t2,5) + 34*Power(t2,6) - 
               8*Power(t1,3)*
                (-1 + 9*t2 - 8*Power(t2,2) + 3*Power(t2,3)) - 
               2*Power(t1,2)*
                (-10 - 64*Power(t2,2) + 79*Power(t2,3) + 7*Power(t2,4)) \
+ t1*(-64 + 72*t2 - 322*Power(t2,2) + 608*Power(t2,3) + 8*Power(t2,4) + 
                  Power(t2,5))) + 
            Power(s2,3)*(-24 + 160*t2 + 234*Power(t2,2) - 
               570*Power(t2,3) + 253*Power(t2,4) - 1078*Power(t2,5) + 
               176*Power(t2,6) - 12*Power(t2,7) - 
               8*Power(t1,4)*(-3 - 6*t2 + 7*Power(t2,2)) + 
               2*Power(t1,3)*
                (-52 + 44*t2 - 103*Power(t2,2) + 187*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(t1,2)*(112 + 208*t2 - 178*Power(t2,2) - 
                  918*Power(t2,3) - 255*Power(t2,4) + 25*Power(t2,5)) - 
               t1*(8 + 504*t2 - 494*Power(t2,2) + 262*Power(t2,3) - 
                  2208*Power(t2,4) + 233*Power(t2,5) + 23*Power(t2,6))) \
+ s2*(-4*Power(t1,6)*(-4 + 3*t2 + Power(t2,2)) - 
               2*Power(t1,5)*
                (-20 + 20*t2 - 69*Power(t2,2) + 10*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,4)*(-8 + 24*t2 - 849*Power(t2,2) - 
                  167*Power(t2,3) + 156*Power(t2,4) + 10*Power(t2,5)) + 
               Power(t1,3)*(-312 + 168*t2 + 406*Power(t2,2) + 
                  2422*Power(t2,3) + 73*Power(t2,4) - 
                  339*Power(t2,5) - 8*Power(t2,6)) + 
               t1*(-144 - 488*t2 + 432*Power(t2,2) - 680*Power(t2,3) + 
                  2483*Power(t2,4) + 800*Power(t2,5) + 
                  146*Power(t2,6) - 129*Power(t2,7)) + 
               Power(t1,2)*(408 + 176*t2 - 36*Power(t2,2) - 
                  1720*Power(t2,3) - 2487*Power(t2,4) - 
                  105*Power(t2,5) + 323*Power(t2,6) + 2*Power(t2,7)) + 
               t2*(172 - 87*t2 + 5*Power(t2,2) + 399*Power(t2,3) - 
                  1272*Power(t2,4) + 172*Power(t2,5) - 73*Power(t2,6) + 
                  13*Power(t2,7))) + 
            Power(s2,2)*(72 - 12*t2 + 200*Power(t2,2) + 
               155*Power(t2,3) - 1051*Power(t2,4) + 341*Power(t2,5) - 
               1043*Power(t2,6) + 240*Power(t2,7) + 3*Power(t2,8) + 
               4*Power(t1,5)*
                (-10 + 2*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               2*Power(t1,4)*
                (28 - 2*t2 - 41*Power(t2,2) - 99*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(t1,3)*(-16 - 524*t2 + 1184*Power(t2,2) + 
                  691*Power(t2,3) + 115*Power(t2,4) - 80*Power(t2,5)) + 
               Power(t1,2)*(96 + 616*t2 - 452*Power(t2,2) - 
                  1849*Power(t2,3) - 2019*Power(t2,4) + 
                  399*Power(t2,5) + 91*Power(t2,6)) - 
               t1*(168 + 84*t2 + 1006*Power(t2,2) - 2161*Power(t2,3) - 
                  353*Power(t2,4) - 2402*Power(t2,5) + 568*Power(t2,6) + 
                  36*Power(t2,7)))) + 
         Power(s,6)*(12 - 72*t1 + 49*Power(t1,2) + 15*Power(t1,3) - 
            4*Power(t1,4) - 2*Power(t1,5) + 
            2*Power(s1,3)*(4 - 3*s2 + 6*t1 - 4*t2)*(t1 - t2) + 72*t2 - 
            14*t1*t2 - 73*Power(t1,2)*t2 - 12*Power(t1,3)*t2 - 
            35*Power(t2,2) + 101*t1*Power(t2,2) + 
            60*Power(t1,2)*Power(t2,2) + 20*Power(t1,3)*Power(t2,2) - 
            43*Power(t2,3) - 68*t1*Power(t2,3) - 
            40*Power(t1,2)*Power(t2,3) + 24*Power(t2,4) + 
            30*t1*Power(t2,4) - 8*Power(t2,5) + 
            Power(s2,4)*(6 - 6*t1 + 6*t2) + 
            Power(s2,3)*(-6 + 33*Power(t1,2) + 33*t2 + 37*Power(t2,2) - 
               t1*(33 + 70*t2)) + 
            Power(s2,2)*(6 - 43*Power(t1,3) + 4*t2 - 47*Power(t2,2) - 
               29*Power(t2,3) + Power(t1,2)*(51 + 57*t2) + 
               t1*(-4 - 4*t2 + 15*Power(t2,2))) + 
            Power(s1,2)*(18 + 6*Power(s2,3) + 22*Power(t1,3) + 
               Power(t1,2)*(26 - 68*t2) + 
               Power(s2,2)*(6 + 59*t1 - 59*t2) - 59*t2 + 
               40*Power(t2,2) - 24*Power(t2,3) - 
               2*s2*(15 + 18*Power(t1,2) + t1*(62 - 37*t2) - 62*t2 + 
                  19*Power(t2,2)) + t1*(59 - 66*t2 + 70*Power(t2,2))) + 
            s2*(-18 + 18*Power(t1,4) + Power(t1,3)*(4 - 28*t2) - 
               113*t2 + 63*Power(t2,2) + 94*Power(t2,3) - 
               26*Power(t2,4) + 
               Power(t1,2)*(-119 + 86*t2 - 24*Power(t2,2)) + 
               t1*(113 + 56*t2 - 184*Power(t2,2) + 60*Power(t2,3))) + 
            s1*(-30 - 6*Power(s2,4) + 8*Power(t1,4) + 
               Power(t1,3)*(14 - 48*t2) + 49*Power(s2,3)*(t1 - t2) - 
               7*t2 + 102*Power(t2,2) - 56*Power(t2,3) + 
               24*Power(t2,4) + 
               4*Power(t1,2)*(8 - 21*t2 + 24*Power(t2,2)) + 
               t1*(7 - 134*t2 + 126*Power(t2,2) - 80*Power(t2,3)) + 
               Power(s2,2)*(-12 + 2*Power(t1,2) + 67*t2 + 
                  88*Power(t2,2) - t1*(67 + 90*t2)) + 
               s2*(48 - 12*Power(t1,3) - 17*t2 - 218*Power(t2,2) + 
                  58*Power(t2,3) + Power(t1,2)*(-50 + 82*t2) + 
                  t1*(17 + 268*t2 - 128*Power(t2,2))))) - 
         Power(s,5)*(60 - 179*t1 + 144*Power(t1,2) + 5*Power(t1,3) - 
            42*Power(t1,4) + 10*Power(t1,5) - 
            10*Power(s2,5)*(-1 + t1 - t2) + 107*t2 + 140*t1*t2 - 
            308*Power(t1,2)*t2 + 79*Power(t1,3)*t2 - 16*Power(t1,4)*t2 + 
            8*Power(t1,5)*t2 - 284*Power(t2,2) + 349*t1*Power(t2,2) + 
            99*Power(t1,2)*Power(t2,2) + 44*Power(t1,3)*Power(t2,2) - 
            20*Power(t1,4)*Power(t2,2) - 46*Power(t2,3) - 
            267*t1*Power(t2,3) - 136*Power(t1,2)*Power(t2,3) + 
            131*Power(t2,4) + 154*t1*Power(t2,4) + 
            40*Power(t1,2)*Power(t2,4) - 56*Power(t2,5) - 
            40*t1*Power(t2,5) + 12*Power(t2,6) + 
            Power(s1,4)*(2*Power(s2,2) + 30*Power(t1,2) + 
               t1*(13 - 40*t2) - s2*(t1 - 3*t2) + t2*(-11 + 12*t2)) + 
            Power(s2,4)*(4 + 40*Power(t1,2) + 13*t2 + 15*Power(t2,2) - 
               3*t1*(16 + 19*t2)) - 
            Power(s2,3)*(13 + 66*Power(t1,3) - 112*t2 + 
               31*Power(t2,2) + 124*Power(t2,3) - 
               Power(t1,2)*(149 + 36*t2) + 
               t1*(66 + 114*t2 - 154*Power(t2,2))) + 
            Power(s1,3)*(62 + 27*Power(s2,3) + 48*Power(t1,3) + 
               Power(t1,2)*(61 - 160*t2) + 
               Power(s2,2)*(16 + 116*t1 - 113*t2) - 130*t2 + 
               89*Power(t2,2) - 48*Power(t2,3) - 
               s2*(113 + 35*Power(t1,2) + t1*(287 - 66*t2) - 259*t2 + 
                  35*Power(t2,2)) + t1*(139 - 154*t2 + 160*Power(t2,2))) \
+ Power(s2,2)*(20 + 44*Power(t1,4) - 150*t2 - 61*Power(t2,2) + 
               160*Power(t2,3) + 11*Power(t2,4) + 
               Power(t1,3)*(-113 + 37*t2) + 
               Power(t1,2)*(-52 + 80*t2 - 195*Power(t2,2)) + 
               t1*(103 + 113*t2 - 127*Power(t2,2) + 103*Power(t2,3))) - 
            s2*(79 + 8*Power(t1,5) + 84*t2 - 387*Power(t2,2) + 
               8*Power(t2,3) + 222*Power(t2,4) - 26*Power(t2,5) + 
               Power(t1,4)*(8 + 42*t2) - 
               4*Power(t1,3)*(45 - 9*t2 + 37*Power(t2,2)) + 
               Power(t1,2)*(281 - 178*t2 + 126*Power(t2,2) + 
                  112*Power(t2,3)) + 
               2*t1*(-98 + 53*t2 + 175*Power(t2,2) - 196*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,2)*(-35 - 19*Power(s2,4) + 6*Power(t1,4) + 
               Power(s2,3)*(33 + 185*t1 - 229*t2) + 
               Power(t1,3)*(51 - 108*t2) - 260*t2 + 391*Power(t2,2) - 
               201*Power(t2,3) + 72*Power(t2,4) + 
               Power(t1,2)*(56 - 273*t2 + 270*Power(t2,2)) + 
               t1*(149 - 447*t2 + 423*Power(t2,2) - 240*Power(t2,3)) - 
               Power(s2,2)*(80 + 90*Power(t1,2) - 177*t2 - 
                  231*Power(t2,2) + t1*(170 + 141*t2)) + 
               s2*(113 + 51*Power(t1,3) + 3*Power(t1,2)*(-36 + t2) + 
                  312*t2 - 740*Power(t2,2) + 87*Power(t2,3) + 
                  t1*(-140 + 860*t2 - 141*Power(t2,2)))) + 
            s1*(-89 - 10*Power(s2,5) - 12*Power(t1,5) + 
               13*Power(s2,4)*(1 + 3*t1) + 295*t2 + 244*Power(t2,2) - 
               392*Power(t2,3) + 179*Power(t2,4) - 48*Power(t2,5) + 
               Power(t1,4)*(13 + 20*t2) + 
               Power(t1,3)*(1 - 104*t2 + 60*Power(t2,2)) - 
               4*Power(t1,2)*
                (-50 + 46*t2 - 87*Power(t2,2) + 45*Power(t2,3)) + 
               t1*(-119 - 444*t2 + 575*Power(t2,2) - 436*Power(t2,3) + 
                  160*Power(t2,4)) + 
               Power(s2,3)*(-39 + 32*Power(t1,2) + 17*t2 + 
                  326*Power(t2,2) - t1*(35 + 354*t2)) + 
               Power(s2,2)*(30 - 118*Power(t1,3) + 208*t2 - 
                  353*Power(t2,2) - 131*Power(t2,3) + 
                  Power(t1,2)*(151 + 327*t2) + 
                  t1*(-147 + 190*t2 - 78*Power(t2,2))) + 
               s2*(87 + 77*Power(t1,4) - 536*t2 - 191*Power(t2,2) + 
                  703*Power(t2,3) - 81*Power(t2,4) - 
                  3*Power(t1,3)*(13 + 76*t2) + 
                  Power(t1,2)*(-297 + 301*t2 + 144*Power(t2,2)) + 
                  t1*(262 + 488*t2 - 965*Power(t2,2) + 88*Power(t2,3))))) \
+ Power(s,4)*(108 - 236*t1 + 159*Power(t1,2) - 5*Power(t1,3) - 
            65*Power(t1,4) + 39*Power(t1,5) - 6*Power(t1,6) - 64*t2 + 
            565*t1*t2 - 695*Power(t1,2)*t2 + 251*Power(t1,3)*t2 + 
            Power(t1,4)*t2 - 10*Power(t1,5)*t2 - 544*Power(t2,2) + 
            345*t1*Power(t2,2) + 367*Power(t1,2)*Power(t2,2) - 
            188*Power(t1,3)*Power(t2,2) + 44*Power(t1,4)*Power(t2,2) - 
            12*Power(t1,5)*Power(t2,2) + 355*Power(t2,3) - 
            807*t1*Power(t2,3) + 28*Power(t1,2)*Power(t2,3) - 
            76*Power(t1,3)*Power(t2,3) + 40*Power(t1,4)*Power(t2,3) + 
            254*Power(t2,4) + 309*t1*Power(t2,4) + 
            154*Power(t1,2)*Power(t2,4) - 40*Power(t1,3)*Power(t2,4) - 
            189*Power(t2,5) - 170*t1*Power(t2,5) + 64*Power(t2,6) + 
            20*t1*Power(t2,6) - 8*Power(t2,7) + 
            Power(s2,6)*(4 - 4*t1 + 4*t2) + 
            Power(s1,5)*(2 + 9*Power(s2,2) + 40*Power(t1,2) + 
               t1*(10 - 40*t2) + s2*(-3 + 17*t1 - 8*t2) - 5*t2 + 
               8*Power(t2,2)) + 
            2*Power(s2,5)*(13 + 10*Power(t1,2) + t2 - 11*Power(t2,2) - 
               t1*(23 + 2*t2)) - 
            Power(s2,4)*(32 + 36*Power(t1,3) - 50*t2 - 17*Power(t2,2) + 
               93*Power(t2,3) + 8*Power(t1,2)*(-14 + 5*t2) + 
               t1*(52 + 38*t2 - 179*Power(t2,2))) + 
            Power(s2,3)*(10 + 28*Power(t1,4) + 39*t2 - 
               350*Power(t2,2) - 96*Power(t2,3) + 155*Power(t2,4) + 
               2*Power(t1,3)*(-50 + 73*t2) + 
               Power(t1,2)*(56 - 333*t2 - 301*Power(t2,2)) + 
               t1*(32 + 168*t2 + 509*Power(t2,2) - 28*Power(t2,3))) + 
            Power(s2,2)*(4 - 8*Power(t1,5) - 298*t2 + 294*Power(t2,2) + 
               205*Power(t2,3) - 421*Power(t2,4) + 18*Power(t2,5) - 
               2*Power(t1,4)*(1 + 68*t2) + 
               Power(t1,3)*(150 + 467*t2 + 198*Power(t2,2)) + 
               Power(t1,2)*(-350 - 122*t2 - 799*Power(t2,2) + 
                  62*Power(t2,3)) + 
               t1*(172 + 201*t2 - 233*Power(t2,2) + 755*Power(t2,3) - 
                  134*Power(t2,4))) + 
            s2*(-118 + 198*t2 + 383*Power(t2,2) - 681*Power(t2,3) - 
               150*Power(t2,4) + 238*Power(t2,5) - 2*Power(t2,6) + 
               Power(t1,5)*(38 + 30*t2) - 
               Power(t1,4)*(219 + 112*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(403 - 46*t2 + 300*Power(t2,2) - 
                  172*Power(t2,3)) + 
               Power(t1,2)*(-291 + 158*t2 - 311*Power(t2,2) - 
                  178*Power(t2,3) + 228*Power(t2,4)) + 
               t1*(209 - 382*t2 + 120*Power(t2,2) + 726*Power(t2,3) - 
                  286*Power(t2,4) - 82*Power(t2,5))) + 
            Power(s1,4)*(89 + 49*Power(s2,3) + 50*Power(t1,3) + 
               Power(t1,2)*(99 - 200*t2) + 
               Power(s2,2)*(25 + 158*t1 - 154*t2) - 146*t2 + 
               84*Power(t2,2) - 40*Power(t2,3) + 
               t1*(168 - 199*t2 + 180*Power(t2,2)) + 
               s2*(-193 + 23*Power(t1,2) + 295*t2 + 30*Power(t2,2) - 
                  t1*(371 + 75*t2))) - 
            Power(s1,3)*(-84 + 14*Power(s2,4) + 20*Power(t1,4) + 
               657*t2 - 615*Power(t2,2) + 286*Power(t2,3) - 
               80*Power(t2,4) + Power(t1,3)*(-131 + 100*t2) + 
               Power(s2,3)*(-86 - 268*t1 + 432*t2) + 
               Power(t1,2)*(9 + 532*t2 - 360*Power(t2,2)) + 
               t1*(-378 + 665*t2 - 707*Power(t2,2) + 320*Power(t2,3)) + 
               Power(s2,2)*(134 + 143*Power(t1,2) - 293*t2 - 
                  390*Power(t2,2) + t1*(294 + 268*t2)) + 
               s2*(-18 - 167*Power(t1,3) - 958*t2 + 1105*Power(t2,2) + 
                  40*Power(t2,3) + 4*Power(t1,2)*(23 + 78*t2) + 
                  t1*(454 - 1349*t2 - 205*Power(t2,2)))) - 
            Power(s1,2)*(281 + 40*Power(s2,5) + 30*Power(t1,5) - 
               77*t2 - 1301*Power(t2,2) + 989*Power(t2,3) - 
               404*Power(t2,4) + 80*Power(t2,5) + 
               Power(s2,4)*(-60 - 192*t1 + 97*t2) - 
               Power(t1,4)*(69 + 100*t2) + 
               Power(t1,3)*(95 + 367*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(-369 + 51*t2 - 921*Power(t2,2) + 
                  280*Power(t2,3)) + 
               t1*(-76 + 1385*t2 - 1135*Power(t2,2) + 
                  1027*Power(t2,3) - 280*Power(t2,4)) + 
               Power(s2,3)*(119 + 163*Power(t1,2) + 233*t2 - 
                  872*Power(t2,2) + t1*(-96 + 569*t2)) + 
               Power(s2,2)*(73 + 42*Power(t1,3) - 688*t2 + 
                  1082*Power(t2,2) + 336*Power(t2,3) - 
                  5*Power(t1,2)*(-7 + 90*t2) + 
                  t1*(238 - 992*t2 + 72*Power(t2,2))) - 
               s2*(433 + 123*Power(t1,4) + Power(t1,3)*(15 - 591*t2) - 
                  761*t2 - 1487*Power(t2,2) + 1575*Power(t2,3) + 
                  20*Power(t2,4) + 
                  Power(t1,2)*(-503 + 221*t2 + 783*Power(t2,2)) + 
                  t1*(180 + 1622*t2 - 1871*Power(t2,2) - 
                     335*Power(t2,3)))) + 
            s1*(-14 - 4*Power(s2,6) + 837*t2 - 516*Power(t2,2) - 
               987*Power(t2,3) + 707*Power(t2,4) - 261*Power(t2,5) + 
               40*Power(t2,6) + Power(t1,5)*(21 + 40*t2) + 
               Power(s2,5)*(8 - 6*t1 + 62*t2) - 
               Power(t1,4)*(91 + 109*t2 + 120*Power(t2,2)) + 
               Power(t1,3)*(-45 + 299*t2 + 312*Power(t2,2) + 
                  80*Power(t2,3)) + 
               Power(t1,2)*(497 - 782*t2 + 32*Power(t2,2) - 
                  642*Power(t2,3) + 80*Power(t2,4)) - 
               t1*(404 + 411*t2 - 1814*Power(t2,2) + 947*Power(t2,3) - 
                  679*Power(t2,4) + 120*Power(t2,5)) + 
               Power(s2,4)*(-2 + 80*Power(t1,2) - 73*t2 + 
                  204*Power(t2,2) - t1*(34 + 383*t2)) + 
               Power(s2,3)*(-14 - 204*Power(t1,3) + 520*t2 + 
                  243*Power(t2,2) - 644*Power(t2,3) + 
                  Power(t1,2)*(500 + 499*t2) + 
                  t1*(-354 - 665*t2 + 329*Power(t2,2))) + 
               Power(s2,2)*(121 + 172*Power(t1,4) - 191*t2 - 
                  759*Power(t2,2) + 1185*Power(t2,3) + 73*Power(t2,4) - 
                  2*Power(t1,3)*(299 + 96*t2) + 
                  Power(t1,2)*(241 + 926*t2 - 369*Power(t2,2)) + 
                  t1*(20 + 351*t2 - 1453*Power(t2,2) + 316*Power(t2,3))) \
+ s2*(-95 - 38*Power(t1,5) + Power(t1,4)*(127 - 106*t2) - 904*t2 + 
                  1424*Power(t2,2) + 872*Power(t2,3) - 
                  1000*Power(t2,4) + 
                  Power(t1,3)*(209 - 355*t2 + 596*Power(t2,2)) + 
                  Power(t1,2)*
                   (-610 + 813*t2 + 49*Power(t2,2) - 722*Power(t2,3)) + 
                  t1*(517 - 164*t2 - 1894*Power(t2,2) + 
                     1179*Power(t2,3) + 270*Power(t2,4))))) + 
         Power(s,3)*(-84 + 163*t1 - 62*Power(t1,2) - 20*Power(t1,3) + 
            16*Power(t1,4) - 7*Power(t1,5) - 2*Power(t1,6) + 269*t2 - 
            808*t1*t2 + 679*Power(t1,2)*t2 - 101*Power(t1,3)*t2 - 
            182*Power(t1,4)*t2 + 129*Power(t1,5)*t2 - 
            18*Power(t1,6)*t2 + 270*Power(t2,2) + 444*t1*Power(t2,2) - 
            1193*Power(t1,2)*Power(t2,2) + 824*Power(t1,3)*Power(t2,2) - 
            267*Power(t1,4)*Power(t2,2) + 30*Power(t1,5)*Power(t2,2) - 
            863*Power(t2,3) + 1225*t1*Power(t2,3) - 
            340*Power(t1,2)*Power(t2,3) + 52*Power(t1,3)*Power(t2,3) - 
            4*Power(t1,4)*Power(t2,3) - 8*Power(t1,5)*Power(t2,3) + 
            53*Power(t2,4) - 609*t1*Power(t2,4) + 
            46*Power(t1,2)*Power(t2,4) - 24*Power(t1,3)*Power(t2,4) + 
            30*Power(t1,4)*Power(t2,4) + 314*Power(t2,5) + 
            187*t1*Power(t2,5) + 66*Power(t1,2)*Power(t2,5) - 
            40*Power(t1,3)*Power(t2,5) - 145*Power(t2,6) - 
            86*t1*Power(t2,6) + 20*Power(t1,2)*Power(t2,6) + 
            36*Power(t2,7) - 2*Power(t2,8) - 
            Power(s1,6)*(6 + 16*Power(s2,2) + 30*Power(t1,2) + 
               t1*(2 - 20*t2) + s2*(-11 + 28*t1 - 12*t2) + t2 + 
               2*Power(t2,2)) - 
            4*Power(s2,6)*(3 + t2 - 3*Power(t2,2) + t1*(-3 + 2*t2)) + 
            2*Power(s2,5)*(8 + 32*t2 + 3*Power(t2,2) - 3*Power(t2,3) + 
               4*Power(t1,2)*(-5 + 6*t2) + 
               t1*(12 - 20*t2 - 33*Power(t2,2))) - 
            2*Power(s2,4)*(-21 - 5*t2 - 124*Power(t2,2) - 
               58*Power(t2,3) + 67*Power(t2,4) + 
               4*Power(t1,3)*(-5 + 12*t2) - 
               2*Power(t1,2)*(13 + 47*t2 + 28*Power(t2,2)) + 
               t1*(65 + 148*t2 + 105*Power(t2,2) - 69*Power(t2,3))) + 
            2*Power(s2,3)*(-16 + 65*t2 + 40*Power(t1,4)*t2 + 
               174*Power(t2,2) - 115*Power(t2,3) - 114*Power(t2,4) + 
               46*Power(t2,5) + 
               Power(t1,3)*(-92 - 145*t2 + 24*Power(t2,2)) + 
               Power(t1,2)*(142 + 431*t2 - 128*Power(t2,2) - 
                  182*Power(t2,3)) + 
               t1*(-42 - 245*t2 - 306*Power(t2,2) + 367*Power(t2,3) + 
                  72*Power(t2,4))) + 
            Power(s2,2)*(28 + 135*t2 - 627*Power(t2,2) + 
               249*Power(t2,3) + 527*Power(t2,4) - 624*Power(t2,5) + 
               10*Power(t2,6) - 4*Power(t1,5)*(5 + 6*t2) + 
               Power(t1,4)*(198 + 82*t2 - 148*Power(t2,2)) + 
               Power(t1,3)*(-406 - 363*t2 + 678*Power(t2,2) + 
                  398*Power(t2,3)) - 
               3*Power(t1,2)*
                (-108 + 33*t2 - 187*Power(t2,2) + 512*Power(t2,3) + 
                  82*Power(t2,4)) + 
               t1*(-100 - t2 + 486*Power(t2,2) - 923*Power(t2,3) + 
                  1420*Power(t2,4) + 10*Power(t2,5))) + 
            s2*(71 + 8*Power(t1,6) - 332*t2 + 162*Power(t2,2) + 
               652*Power(t2,3) - 823*Power(t2,4) - 206*Power(t2,5) + 
               106*Power(t2,6) + 10*Power(t2,7) + 
               Power(t1,5)*(-76 + 82*t2 + 42*Power(t2,2)) - 
               Power(t1,4)*(-243 + 372*t2 + 344*Power(t2,2) + 
                  78*Power(t2,3)) + 
               Power(t1,3)*(-268 + 556*t2 + 436*Power(t2,2) + 
                  724*Power(t2,3) - 28*Power(t2,4)) + 
               2*Power(t1,2)*
                (79 - 154*t2 - 252*Power(t2,2) - 284*Power(t2,3) - 
                  339*Power(t2,4) + 66*Power(t2,5)) + 
               t1*(-152 + 518*t2 - 476*Power(t2,2) + 528*Power(t2,3) + 
                  786*Power(t2,4) + 102*Power(t2,5) - 78*Power(t2,6))) - 
            Power(s1,5)*(81 + 46*Power(s2,3) + 20*Power(t1,3) + 
               2*Power(s2,2)*(22 + 83*t1 - 87*t2) - 100*t2 + 
               31*Power(t2,2) - 12*Power(t2,3) - 
               2*Power(t1,2)*(-53 + 70*t2) + 
               t1*(103 - 147*t2 + 100*Power(t2,2)) + 
               s2*(-209 + 72*Power(t1,2) + 227*t2 + 70*Power(t2,2) - 
                  2*t1*(135 + 94*t2))) + 
            Power(s1,4)*(-201 - 14*Power(s2,4) - 174*Power(t1,3) + 
               50*Power(t1,4) + 735*t2 - 485*Power(t2,2) + 
               170*Power(t2,3) - 30*Power(t2,4) + 
               Power(s2,3)*(-67 - 186*t1 + 424*t2) + 
               Power(t1,2)*(110 + 614*t2 - 240*Power(t2,2)) + 
               t1*(-475 + 503*t2 - 654*Power(t2,2) + 200*Power(t2,3)) + 
               Power(s2,2)*(57 + 36*Power(t1,2) - 233*t2 - 
                  526*Power(t2,2) + 6*t1*(59 + 91*t2)) - 
               2*s2*(-106 + 99*Power(t1,3) + 
                  Power(t1,2)*(60 - 255*t2) + 676*t2 - 
                  452*Power(t2,2) - 85*Power(t2,3) + 
                  t1*(-368 + 531*t2 + 275*Power(t2,2)))) + 
            Power(s1,3)*(238 + 60*Power(s2,5) + 40*Power(t1,5) - 
               2*Power(s2,4)*(76 + 141*t1 - 125*t2) + 755*t2 - 
               2033*Power(t2,2) + 1060*Power(t2,3) - 350*Power(t2,4) + 
               40*Power(t2,5) - 2*Power(t1,4)*(43 + 100*t2) + 
               2*Power(t1,3)*(48 + 281*t2 + 80*Power(t2,2)) + 
               8*Power(t1,2)*
                (-29 - 28*t2 - 159*Power(t2,2) + 20*Power(t2,3)) + 
               t1*(-424 + 1781*t2 - 1078*Power(t2,2) + 
                  1186*Power(t2,3) - 200*Power(t2,4)) + 
               Power(s2,3)*(122 + 360*Power(t1,2) + 431*t2 - 
                  1088*Power(t2,2) + t1*(-252 + 338*t2)) + 
               Power(s2,2)*(216 - 176*Power(t1,3) - 917*t2 + 
                  1587*Power(t2,2) + 684*Power(t2,3) + 
                  Power(t1,2)*(598 + 90*t2) + 
                  t1*(152 - 2049*t2 - 652*Power(t2,2))) + 
               s2*(-592 - 82*Power(t1,4) + 95*t2 + 3008*Power(t2,2) - 
                  1566*Power(t2,3) - 220*Power(t2,4) + 
                  Power(t1,3)*(-382 + 692*t2) + 
                  Power(t1,2)*(794 + 812*t2 - 1230*Power(t2,2)) + 
                  8*t1*(10 - 376*t2 + 183*Power(t2,2) + 110*Power(t2,3))\
)) + Power(s1,2)*(331 + 16*Power(s2,6) + Power(t1,5)*(6 - 80*t2) - 
               1339*t2 - 854*Power(t2,2) + 2499*Power(t2,3) - 
               1180*Power(t2,4) + 355*Power(t2,5) - 30*Power(t2,6) - 
               2*Power(s2,5)*(-6 + 30*t1 + 61*t2) + 
               Power(t1,4)*(-62 + 147*t2 + 280*Power(t2,2)) - 
               2*Power(t1,3)*
                (-205 + 84*t2 + 313*Power(t2,2) + 140*Power(t2,3)) + 
               Power(t1,2)*(-927 + 227*t2 + 164*Power(t2,2) + 
                  1192*Power(t2,3) + 10*Power(t2,4)) + 
               t1*(258 + 2043*t2 - 2746*Power(t2,2) + 
                  1246*Power(t2,3) - 1074*Power(t2,4) + 100*Power(t2,5)\
) + 2*Power(s2,4)*(33 + 16*Power(t1,2) + 215*t2 - 296*Power(t2,2) + 
                  t1*(-52 + 361*t2)) + 
               Power(s2,3)*(165 + 204*Power(t1,3) - 636*t2 - 
                  889*Power(t2,2) + 1180*Power(t2,3) - 
                  Power(t1,2)*(707 + 1170*t2) + 
                  2*t1*(131 + 691*t2 + 13*Power(t2,2))) - 
               Power(s2,2)*(135 + 264*Power(t1,4) + 298*t2 - 
                  2190*Power(t2,2) + 2923*Power(t2,3) + 
                  396*Power(t2,4) - 2*Power(t1,3)*(579 + 427*t2) + 
                  Power(t1,2)*(349 + 3001*t2 + 534*Power(t2,2)) - 
                  t1*(40 - 805*t2 + 4456*Power(t2,2) + 340*Power(t2,3))\
) + s2*(-268 + 72*Power(t1,5) + 2044*t2 - 1649*Power(t2,2) - 
                  3002*Power(t2,3) + 1379*Power(t2,4) + 
                  160*Power(t2,5) + Power(t1,4)*(-461 + 36*t2) + 
                  Power(t1,3)*(250 + 1624*t2 - 818*Power(t2,2)) + 
                  Power(t1,2)*
                   (411 - 2194*t2 - 1942*Power(t2,2) + 
                     1350*Power(t2,3)) + 
                  t1*(-352 + 8*t2 + 4594*Power(t2,2) - 
                     720*Power(t2,3) - 800*Power(t2,4)))) + 
            s1*(-167 + 22*Power(t1,6) + 4*Power(s2,6)*(3*t1 - 7*t2) - 
               631*t2 + 1964*Power(t2,2) + 247*Power(t2,3) - 
               1434*Power(t2,4) + 656*Power(t2,5) - 179*Power(t2,6) + 
               12*Power(t2,7) + 
               Power(t1,5)*(-163 - 37*t2 + 48*Power(t2,2)) + 
               Power(t1,4)*(301 + 338*t2 - 57*Power(t2,2) - 
                  160*Power(t2,3)) + 
               Power(t1,3)*(-88 - 1261*t2 + 20*Power(t2,2) + 
                  262*Power(t2,3) + 180*Power(t2,4)) - 
               Power(t1,2)*(468 - 2125*t2 - 345*Power(t2,2) + 
                  96*Power(t2,3) + 494*Power(t2,4) + 60*Power(t2,5)) + 
               t1*(587 - 654*t2 - 2844*Power(t2,2) + 2049*Power(t2,3) - 
                  755*Power(t2,4) + 483*Power(t2,5) - 20*Power(t2,6)) + 
               Power(s2,5)*(-76 - 64*Power(t1,2) - 22*t2 + 
                  68*Power(t2,2) + 2*t1*(34 + 65*t2)) + 
               2*Power(s2,4)*
                (60*Power(t1,3) - Power(t1,2)*(123 + 79*t2) + 
                  t1*(156 + 170*t2 - 289*Power(t2,2)) + 
                  t2*(-161 - 197*t2 + 245*Power(t2,2))) + 
               Power(s2,3)*(-110 - 96*Power(t1,4) + 
                  Power(t1,3)*(328 - 234*t2) - 541*t2 + 
                  744*Power(t2,2) + 753*Power(t2,3) - 562*Power(t2,4) + 
                  Power(t1,2)*(-818 + 915*t2 + 1174*Power(t2,2)) + 
                  t1*(436 + 392*t2 - 1864*Power(t2,2) - 322*Power(t2,3))\
) + Power(s2,2)*(-34 + 28*Power(t1,5) + 787*t2 - 167*Power(t2,2) - 
                  1857*Power(t2,3) + 2237*Power(t2,4) + 
                  70*Power(t2,5) + 6*Power(t1,4)*(-12 + 67*t2) + 
                  Power(t1,3)*(224 - 1803*t2 - 1076*Power(t2,2)) + 
                  Power(t1,2)*
                   (340 - 241*t2 + 3939*Power(t2,2) + 654*Power(t2,3)) \
- t1*(264 + 521*t2 - 1576*Power(t2,2) + 4181*Power(t2,3) + 
                     78*Power(t2,4))) + 
               s2*(265 + 160*t2 - 2104*Power(t2,2) + 2165*Power(t2,3) + 
                  1343*Power(t2,4) - 607*Power(t2,5) - 62*Power(t2,6) - 
                  4*Power(t1,5)*(25 + 28*t2) + 
                  Power(t1,4)*(497 + 799*t2 + 124*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (-457 - 350*t2 - 983*Power(t2,2) + 176*Power(t2,3)) + 
                  Power(t1,2)*
                   (742 + 185*t2 + 1968*Power(t2,2) + 
                     1928*Power(t2,3) - 690*Power(t2,4)) + 
                  t1*(-602 + 684*t2 - 616*Power(t2,2) - 
                     3108*Power(t2,3) - 54*Power(t2,4) + 388*Power(t2,5))\
))) + Power(s,2)*(24 - 46*t1 + 2*Power(t1,2) + 34*Power(t1,3) - 
            4*Power(t1,4) - 32*Power(t1,5) + 22*Power(t1,6) - 206*t2 + 
            481*t1*t2 - 280*Power(t1,2)*t2 - 82*Power(t1,3)*t2 + 
            250*Power(t1,4)*t2 - 155*Power(t1,5)*t2 - 4*Power(t1,6)*t2 + 
            165*Power(t2,2) - 922*t1*Power(t2,2) + 
            1173*Power(t1,2)*Power(t2,2) - 751*Power(t1,3)*Power(t2,2) + 
            160*Power(t1,4)*Power(t2,2) + 149*Power(t1,5)*Power(t2,2) - 
            18*Power(t1,6)*Power(t2,2) + 568*Power(t2,3) - 
            366*t1*Power(t2,3) - 307*Power(t1,2)*Power(t2,3) + 
            552*Power(t1,3)*Power(t2,3) - 397*Power(t1,4)*Power(t2,3) + 
            50*Power(t1,5)*Power(t2,3) - 541*Power(t2,4) + 
            987*t1*Power(t2,4) - 671*Power(t1,2)*Power(t2,4) + 
            309*Power(t1,3)*Power(t2,4) - 56*Power(t1,4)*Power(t2,4) - 
            2*Power(t1,5)*Power(t2,4) - 147*Power(t2,5) - 
            59*t1*Power(t2,5) - 59*Power(t1,2)*Power(t2,5) + 
            40*Power(t1,3)*Power(t2,5) + 8*Power(t1,4)*Power(t2,5) + 
            151*Power(t2,6) + 58*t1*Power(t2,6) - 
            14*Power(t1,2)*Power(t2,6) - 12*Power(t1,3)*Power(t2,6) - 
            56*Power(t2,7) - 10*t1*Power(t2,7) + 
            8*Power(t1,2)*Power(t2,7) + 8*Power(t2,8) - 
            2*t1*Power(t2,8) + 
            Power(s1,7)*(6 + 14*Power(s2,2) + 12*Power(t1,2) + 
               s2*(-15 + 20*t1 - 6*t2) + t2 - 2*t1*(1 + 2*t2)) + 
            4*Power(s2,6)*(2 - 3*t2 - 7*Power(t2,2) + 3*Power(t2,3) + 
               t1*(-2 + 5*t2)) + 
            2*Power(s2,5)*(-22 + 5*t2 + 64*Power(t2,2) - 
               29*Power(t2,3) + 7*Power(t2,4) + 
               2*Power(t1,2)*(1 - 16*t2 + 6*Power(t2,2)) - 
               t1*(-20 + t2 - 32*Power(t2,2) + 37*Power(t2,3))) + 
            2*Power(s2,4)*(21 - 8*t2 - 22*Power(t2,2) + 
               260*Power(t2,3) + 45*Power(t2,4) - 42*Power(t2,5) + 
               Power(t1,3)*(22 + 28*t2 - 36*Power(t2,2)) + 
               t1*(32 + 25*t2 - 323*Power(t2,2) - 57*Power(t2,3)) + 
               Power(t1,2)*(-75 + 33*t2 + 42*Power(t2,2) + 
                  88*Power(t2,3))) + 
            Power(s2,3)*(4*Power(t1,4)*(-19 + 4*t2 + 18*Power(t2,2)) - 
               2*Power(t1,3)*
                (-57 + 70*t2 + 187*Power(t2,2) + 36*Power(t2,3)) + 
               Power(t1,2)*(88 + 84*t2 + 1684*Power(t2,2) - 
                  44*Power(t2,3) - 141*Power(t2,4)) + 
               t2*(68 + 224*t2 + 448*Power(t2,2) + 410*Power(t2,3) - 
                  237*Power(t2,4) + 31*Power(t2,5)) + 
               t1*(-126 - 244*t2 - 516*Power(t2,2) - 1984*Power(t2,3) + 
                  599*Power(t2,4) + 110*Power(t2,5))) + 
            Power(s2,2)*(-18 + 14*t2 + 537*Power(t2,2) - 
               229*Power(t2,3) + 15*Power(t2,4) + 855*Power(t2,5) - 
               511*Power(t2,6) - 5*Power(t2,7) - 
               4*Power(t1,5)*(-11 + 11*t2 + 6*Power(t2,2)) + 
               Power(t1,4)*(36 + 218*t2 + 218*Power(t2,2) - 
                  68*Power(t2,3)) + 
               Power(t1,3)*(-174 - 948*t2 - 1079*Power(t2,2) + 
                  362*Power(t2,3) + 281*Power(t2,4)) + 
               Power(t1,2)*(46 + 1414*t2 + 777*Power(t2,2) + 
                  2021*Power(t2,3) - 1299*Power(t2,4) - 267*Power(t2,5)\
) + t1*(66 - 502*t2 - 1367*Power(t2,2) + 320*Power(t2,3) - 
                  2059*Power(t2,4) + 1274*Power(t2,5) + 83*Power(t2,6))) \
+ s2*(-14 + 165*t2 - 372*Power(t2,2) + 168*Power(t2,3) + 
               904*Power(t2,4) - 664*Power(t2,5) - 71*Power(t2,6) - 
               2*Power(t2,7) + 4*Power(t2,8) + 
               8*Power(t1,6)*(-1 + 2*t2) + 
               2*Power(t1,5)*
                (-35 - 63*t2 + 27*Power(t2,2) + 13*Power(t2,3)) + 
               Power(t1,4)*(98 + 951*t2 - 164*Power(t2,2) - 
                  340*Power(t2,3) - 72*Power(t2,4)) + 
               2*Power(t1,3)*
                (15 - 632*t2 - 445*Power(t2,2) + 186*Power(t2,3) + 
                  356*Power(t2,4) + 28*Power(t2,5)) + 
               Power(t1,2)*(-88 + 494*t2 + 1584*Power(t2,2) + 
                  138*Power(t2,3) - 325*Power(t2,4) - 
                  702*Power(t2,5) + 4*Power(t2,6)) + 
               t1*(52 - 284*t2 + 170*Power(t2,2) - 1632*Power(t2,3) + 
                  535*Power(t2,4) + 322*Power(t2,5) + 262*Power(t2,6) - 
                  18*Power(t2,7))) + 
            Power(s1,6)*(55 + 24*Power(s2,3) - 6*Power(t1,3) + 
               Power(s2,2)*(54 + 119*t1 - 137*t2) + 
               Power(t1,2)*(70 - 52*t2) - 47*t2 + 2*Power(t2,2) + 
               t1*(20 - 55*t2 + 22*Power(t2,2)) + 
               s2*(-157 + 58*Power(t1,2) + 144*t2 + 40*Power(t2,2) - 
                  2*t1*(47 + 72*t2))) + 
            Power(s1,5)*(159 + 26*Power(s2,4) - 48*Power(t1,4) + 
               5*Power(s2,3)*(1 + 13*t1 - 47*t2) - 449*t2 + 
               201*Power(t2,2) - 33*Power(t2,3) + 
               24*Power(t1,3)*(5 + 3*t2) + 
               Power(t1,2)*(-92 - 402*t2 + 72*Power(t2,2)) + 
               t1*(302 - 158*t2 + 315*Power(t2,2) - 48*Power(t2,3)) + 
               Power(s2,2)*(33 + 72*Power(t1,2) + 24*t2 + 
                  480*Power(t2,2) - t1*(283 + 606*t2)) + 
               s2*(-272 + 114*Power(t1,3) + 
                  Power(t1,2)*(308 - 354*t2) + 1108*t2 - 
                  493*Power(t2,2) - 114*Power(t2,3) + 
                  t1*(-695 + 290*t2 + 438*Power(t2,2)))) + 
            Power(s1,4)*(-26 - 40*Power(s2,5) - 30*Power(t1,5) + 
               2*Power(s2,4)*(77 + 87*t1 - 129*t2) - 968*t2 + 
               1397*Power(t2,2) - 510*Power(t2,3) + 100*Power(t2,4) + 
               Power(t1,4)*(34 + 200*t2) + 
               Power(t1,3)*(107 - 404*t2 - 240*Power(t2,2)) + 
               Power(s2,3)*(54 - 277*Power(t1,2) + t1*(271 - 42*t2) - 
                  301*t2 + 731*Power(t2,2)) + 
               Power(t1,2)*(-143 + 165*t2 + 894*Power(t2,2)) + 
               2*t1*(242 - 534*t2 + 245*Power(t2,2) - 
                  340*Power(t2,3) + 25*Power(t2,4)) + 
               Power(s2,2)*(-214 + 245*Power(t1,3) + 754*t2 - 
                  1147*Power(t2,2) - 835*Power(t2,3) - 
                  3*Power(t1,2)*(281 + 201*t2) + 
                  t1*(-168 + 2183*t2 + 1317*Power(t2,2))) + 
               s2*(406 + 8*Power(t1,4) + Power(t1,3)*(646 - 390*t2) + 
                  630*t2 - 2933*Power(t2,2) + 830*Power(t2,3) + 
                  180*Power(t2,4) + 
                  3*Power(t1,2)*(-295 - 632*t2 + 280*Power(t2,2)) + 
                  t1*(-211 + 3180*t2 + 42*Power(t2,2) - 730*Power(t2,3))\
)) + Power(s1,3)*(-393 - 24*Power(s2,6) + 559*t2 + 2097*Power(t2,2) - 
               2198*Power(t2,3) + 760*Power(t2,4) - 145*Power(t2,5) + 
               2*Power(s2,5)*(1 + 58*t1 + 47*t2) + 
               Power(t1,5)*(-44 + 80*t2) + 
               Power(t1,4)*(352 - 7*t2 - 320*Power(t2,2)) + 
               Power(t1,3)*(-670 - 622*t2 + 452*Power(t2,2) + 
                  360*Power(t2,3)) + 
               Power(t1,2)*(781 + 1019*t2 + 116*Power(t2,2) - 
                  956*Power(t2,3) - 100*Power(t2,4)) + 
               t1*(74 - 2379*t2 + 1451*Power(t2,2) - 780*Power(t2,3) + 
                  740*Power(t2,4) - 20*Power(t2,5)) - 
               2*Power(s2,4)*
                (103 + 88*Power(t1,2) + 296*t2 - 351*Power(t2,2) + 
                  t1*(-64 + 255*t2)) - 
               Power(s2,3)*(223 + 48*Power(t1,3) + 372*t2 - 
                  1110*Power(t2,2) + 1054*Power(t2,3) - 
                  Power(t1,2)*(509 + 1020*t2) + 
                  2*t1*(-235 + 769*t2 + 187*Power(t2,2))) + 
               Power(s2,2)*(-193 + 200*Power(t1,4) + 774*t2 - 
                  3315*Power(t2,2) + 3268*Power(t2,3) + 
                  790*Power(t2,4) - 8*Power(t1,3)*(129 + 139*t2) + 
                  Power(t1,2)*(-185 + 4143*t2 + 1644*Power(t2,2)) + 
                  t1*(88 + 1999*t2 - 6125*Power(t2,2) - 
                     1588*Power(t2,3))) + 
               s2*(496 - 68*Power(t1,5) - 2270*t2 + 406*Power(t2,2) + 
                  3792*Power(t2,3) - 745*Power(t2,4) - 
                  170*Power(t2,5) + Power(t1,4)*(581 + 108*t2) + 
                  86*Power(t1,3)*(-8 - 33*t2 + 5*Power(t2,2)) + 
                  Power(t1,2)*
                   (-273 + 3076*t2 + 4542*Power(t2,2) - 
                     980*Power(t2,3)) + 
                  2*t1*(189 + 220*t2 - 2846*Power(t2,2) - 
                     594*Power(t2,3) + 360*Power(t2,4)))) - 
            Power(s1,2)*(39 + 30*Power(t1,6) + 
               4*Power(s2,6)*(5 + 3*t1 - 15*t2) - 1416*t2 + 
               1581*Power(t2,2) + 2073*Power(t2,3) - 1877*Power(t2,4) + 
               651*Power(t2,5) - 114*Power(t2,6) - 
               2*Power(s2,5)*
                (67 + 36*Power(t1,2) + t1*(7 - 159*t2) - 27*t2 - 
                  27*Power(t2,2)) + 
               Power(t1,5)*(-275 - 141*t2 + 72*Power(t2,2)) + 
               Power(t1,4)*(273 + 1118*t2 + 144*Power(t2,2) - 
                  240*Power(t2,3)) + 
               Power(t1,3)*(148 - 1941*t2 - 1232*Power(t2,2) + 
                  132*Power(t2,3) + 270*Power(t2,4)) - 
               2*Power(t1,2)*
                (329 - 927*t2 - 1140*Power(t2,2) - 209*Power(t2,3) + 
                  237*Power(t2,4) + 54*Power(t2,5)) + 
               t1*(455 + 634*t2 - 4293*Power(t2,2) + 965*Power(t2,3) - 
                  680*Power(t2,4) + 423*Power(t2,5) + 6*Power(t2,6)) + 
               2*Power(s2,4)*
                (8 + 72*Power(t1,3) - 470*t2 - 406*Power(t2,2) + 
                  409*Power(t2,3) - Power(t1,2)*(98 + 285*t2) + 
                  t1*(321 + 210*t2 - 249*Power(t2,2))) - 
               2*Power(s2,3)*
                (112 + 60*Power(t1,4) + 488*t2 + 496*Power(t2,2) - 
                  787*Power(t2,3) + 383*Power(t2,4) - 
                  Power(t1,3)*(233 + 15*t2) + 
                  Power(t1,2)*(804 - 486*t2 - 675*Power(t2,2)) + 
                  t1*(-314 - 1505*t2 + 1431*Power(t2,2) + 
                     362*Power(t2,3))) + 
               Power(s2,2)*(-228 + 36*Power(t1,5) - 112*t2 + 
                  891*Power(t2,2) - 5091*Power(t2,3) + 
                  3972*Power(t2,4) + 399*Power(t2,5) + 
                  Power(t1,4)*(-214 + 438*t2) + 
                  Power(t1,3)*(790 - 2367*t2 - 1770*Power(t2,2)) + 
                  6*Power(t1,2)*
                   (-59 - 410*t2 + 1176*Power(t2,2) + 339*Power(t2,3)) \
+ t1*(624 - 93*t2 + 5553*Power(t2,2) - 8107*Power(t2,3) - 
                     1137*Power(t2,4))) - 
               s2*(-147 - 946*t2 + 4226*Power(t2,2) - 
                  2278*Power(t2,3) - 2503*Power(t2,4) + 
                  340*Power(t2,5) + 96*Power(t2,6) + 
                  4*Power(t1,5)*(23 + 39*t2) - 
                  3*Power(t1,4)*(171 + 498*t2 + 104*Power(t2,2)) + 
                  Power(t1,3)*
                   (170 + 1774*t2 + 4450*Power(t2,2) - 90*Power(t2,3)) \
+ Power(t1,2)*(282 + 486*t2 - 3822*Power(t2,2) - 5330*Power(t2,3) + 
                     570*Power(t2,4)) + 
                  t1*(342 - 2060*t2 + 288*Power(t2,2) + 
                     4946*Power(t2,3) + 1822*Power(t2,4) - 
                     420*Power(t2,5)))) + 
            s1*(170 - 114*t2 - 1591*Power(t2,2) + 1589*Power(t2,3) + 
               932*Power(t2,4) - 833*Power(t2,5) + 297*Power(t2,6) - 
               47*Power(t2,7) + 2*Power(t1,6)*(5 + 24*t2) + 
               4*Power(s2,6)*
                (4 + 3*t1*(-2 + t2) + 12*t2 - 12*Power(t2,2)) + 
               Power(t1,5)*(137 - 422*t2 - 147*Power(t2,2) + 
                  24*Power(t2,3)) + 
               Power(t1,4)*(-246 + 105*t2 + 1163*Power(t2,2) + 
                  173*Power(t2,3) - 80*Power(t2,4)) + 
               Power(t1,3)*(122 + 909*t2 - 1823*Power(t2,2) - 
                  1026*Power(t2,3) - 76*Power(t2,4) + 96*Power(t2,5)) + 
               Power(t1,2)*(194 - 1819*t2 + 1380*Power(t2,2) + 
                  2075*Power(t2,3) + 288*Power(t2,4) - 66*Power(t2,5) - 
                  48*Power(t2,6)) + 
               t1*(-391 + 1349*t2 + 926*Power(t2,2) - 
                  3385*Power(t2,3) + 339*Power(t2,4) - 
                  310*Power(t2,5) + 115*Power(t2,6) + 8*Power(t2,7)) - 
               2*Power(s2,5)*
                (5 + 129*t2 - 55*Power(t2,2) + 7*Power(t2,3) + 
                  Power(t1,2)*(-38 + 48*t2) + 
                  t1*(5 + 41*t2 - 138*Power(t2,2))) + 
               2*Power(s2,4)*
                (-6 + 31*t2 - 627*Power(t2,2) - 232*Power(t2,3) + 
                  216*Power(t2,4) + 4*Power(t1,3)*(-8 + 27*t2) - 
                  Power(t1,2)*(35 + 133*t2 + 285*Power(t2,2)) + 
                  t1*(-5 + 636*t2 + 203*Power(t2,2) - 81*Power(t2,3))) - 
               Power(s2,3)*(56 + 448*t2 + 1201*Power(t2,2) + 
                  1084*Power(t2,3) - 997*Power(t2,4) + 
                  263*Power(t2,5) + 24*Power(t1,4)*(1 + 8*t2) - 
                  6*Power(t1,3)*(30 + 137*t2 + 25*Power(t2,2)) + 
                  Power(t1,2)*
                   (184 + 3272*t2 - 507*Power(t2,2) - 748*Power(t2,3)) \
+ t1*(-300 - 1142*t2 - 4524*Power(t2,2) + 2194*Power(t2,3) + 
                     483*Power(t2,4))) + 
               Power(s2,2)*(-36 - 781*t2 + 310*Power(t2,2) + 
                  316*Power(t2,3) - 3418*Power(t2,4) + 
                  2284*Power(t2,5) + 92*Power(t2,6) + 
                  Power(t1,5)*(56 + 60*t2) + 
                  Power(t1,4)*(-252 - 422*t2 + 306*Power(t2,2)) + 
                  Power(t1,3)*
                   (1016 + 1863*t2 - 1697*Power(t2,2) - 
                     1184*Power(t2,3)) + 
                  3*Power(t1,2)*
                   (-486 - 383*t2 - 1432*Power(t2,2) + 
                     1685*Power(t2,3) + 396*Power(t2,4)) + 
                  t1*(522 + 2021*t2 - 501*Power(t2,2) + 
                     5781*Power(t2,3) - 5156*Power(t2,4) - 
                     462*Power(t2,5))) - 
               s2*(159 + 20*Power(t1,6) - 511*t2 - 282*Power(t2,2) + 
                  3266*Power(t2,3) - 2178*Power(t2,4) - 
                  764*Power(t2,5) + 59*Power(t2,6) + 30*Power(t2,7) + 
                  2*Power(t1,5)*(-63 + 74*t2 + 57*Power(t2,2)) - 
                  Power(t1,4)*
                   (-941 + 673*t2 + 1253*Power(t2,2) + 268*Power(t2,3)) \
+ 2*Power(t1,3)*(-644 - 373*t2 + 729*Power(t2,2) + 1485*Power(t2,3) + 
                     60*Power(t2,4)) + 
                  Power(t1,2)*
                   (566 + 1918*t2 + 351*Power(t2,2) - 
                     1956*Power(t2,3) - 3078*Power(t2,4) + 
                     138*Power(t2,5)) + 
                  t1*(-320 + 472*t2 - 3314*Power(t2,2) + 
                     1052*Power(t2,3) + 2061*Power(t2,4) + 
                     1134*Power(t2,5) - 134*Power(t2,6))))) + 
         s*(-40*Power(t1,3) + 92*Power(t1,4) - 64*Power(t1,5) + 
            12*Power(t1,6) + 48*t2 - 92*t1*t2 + 124*Power(t1,2)*t2 - 
            228*Power(t1,3)*t2 + 168*Power(t1,4)*t2 - 40*Power(t1,5)*t2 + 
            20*Power(t1,6)*t2 - 160*Power(t2,2) + 353*t1*Power(t2,2) - 
            38*Power(t1,2)*Power(t2,2) - 188*Power(t1,3)*Power(t2,2) + 
            204*Power(t1,4)*Power(t2,2) - 173*Power(t1,5)*Power(t2,2) - 
            2*Power(t1,6)*Power(t2,2) - 5*Power(t2,3) - 
            516*t1*Power(t2,3) + 781*Power(t1,2)*Power(t2,3) - 
            707*Power(t1,3)*Power(t2,3) + 386*Power(t1,4)*Power(t2,3) + 
            67*Power(t1,5)*Power(t2,3) - 6*Power(t1,6)*Power(t2,3) + 
            390*Power(t2,4) - 503*t1*Power(t2,4) + 
            437*Power(t1,2)*Power(t2,4) - 139*Power(t1,3)*Power(t2,4) - 
            199*Power(t1,4)*Power(t2,4) + 20*Power(t1,5)*Power(t2,4) - 
            122*Power(t2,5) + 147*t1*Power(t2,5) - 
            180*Power(t1,2)*Power(t2,5) + 195*Power(t1,3)*Power(t2,5) - 
            28*Power(t1,4)*Power(t2,5) - 53*Power(t2,6) + 
            62*t1*Power(t2,6) - 59*Power(t1,2)*Power(t2,6) + 
            24*Power(t1,3)*Power(t2,6) + 24*Power(t2,7) + 
            6*t1*Power(t2,7) - 14*Power(t1,2)*Power(t2,7) - 
            8*Power(t2,8) + 4*t1*Power(t2,8) + 
            Power(s1,8)*(-2 - 6*Power(s2,2) + t1 - 2*Power(t1,2) + 
               s2*(9 - 7*t1 + t2)) + 
            4*Power(s2,6)*t2*(2 + 2*t2 - 7*Power(t2,2) + Power(t2,3) + 
               t1*(-2 + t2 + 2*Power(t2,2))) - 
            2*Power(s2,5)*(-8 + 20*t2 + 19*Power(t2,2) - 
               75*Power(t2,3) + 38*Power(t2,4) - 4*Power(t2,5) + 
               4*Power(t1,2)*(-2 + t2 + Power(t2,2) + 2*Power(t2,3)) + 
               t1*(16 - 24*t2 + 21*Power(t2,2) - 32*Power(t2,3) + 
                  8*Power(t2,4))) + 
            Power(s2,4)*(-60 + 58*t2 + 14*Power(t2,2) - 
               200*Power(t2,3) + 484*Power(t2,4) + Power(t2,5) - 
               27*Power(t2,6) - 8*Power(t1,3)*(5 - 9*t2 + Power(t2,2)) + 
               Power(t1,2)*(20 - 70*t2 - 24*Power(t2,2) + 
                  52*Power(t2,3) + 56*Power(t2,4)) + 
               t1*(80 - 60*t2 + 254*Power(t2,2) - 564*Power(t2,3) + 
                  10*Power(t2,4) - 19*Power(t2,5))) + 
            Power(s2,3)*(40 - 160*t2 - 2*Power(t2,2) + 294*Power(t2,3) + 
               65*Power(t2,4) + 606*Power(t2,5) - 131*Power(t2,6) + 
               8*Power(t2,7) + 
               8*Power(t1,4)*
                (3 - 11*t2 + 4*Power(t2,2) + 2*Power(t2,3)) - 
               2*Power(t1,3)*
                (-60 + 47*t2 - 54*Power(t2,2) + 139*Power(t2,3) + 
                  17*Power(t2,4)) + 
               Power(t1,2)*(-272 + 68*t2 + 10*Power(t2,2) + 
                  1114*Power(t2,3) + 93*Power(t2,4) - 16*Power(t2,5)) + 
               2*t1*(44 + 137*t2 - 222*Power(t2,2) + 24*Power(t2,3) - 
                  885*Power(t2,4) + 132*Power(t2,5) + 13*Power(t2,6))) + 
            s2*(8*Power(t1,6)*(-1 + Power(t2,2)) + 
               2*Power(t1,5)*
                (20 - 41*t2 - 51*Power(t2,2) + 7*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(t1,4)*(64 - 192*t2 + 1061*Power(t2,2) + 
                  62*Power(t2,3) - 136*Power(t2,4) - 20*Power(t2,5)) + 
               2*Power(t1,3)*
                (-108 + 91*t2 - 366*Power(t2,2) - 904*Power(t2,3) + 
                  6*Power(t2,4) + 148*Power(t2,5) + 12*Power(t2,6)) + 
               Power(t1,2)*(120 + 256*t2 - 176*Power(t2,2) + 
                  1880*Power(t2,3) + 1143*Power(t2,4) + 
                  46*Power(t2,5) - 294*Power(t2,6) - 12*Power(t2,7)) + 
               2*t1*t2*(-68 - 164*t2 + 190*Power(t2,2) - 
                  956*Power(t2,3) - 10*Power(t2,4) - 24*Power(t2,5) + 
                  63*Power(t2,6) + Power(t2,7)) - 
               t2*(28 - 237*t2 + 220*Power(t2,2) + 7*Power(t2,3) - 
                  788*Power(t2,4) + 286*Power(t2,5) - 30*Power(t2,6) + 
                  14*Power(t2,7))) + 
            Power(s2,2)*(-4*Power(t1,5)*
                (-2 - 8*t2 + 7*Power(t2,2) + 2*Power(t2,3)) - 
               2*Power(t1,4)*
                (80 - 85*t2 - 27*Power(t2,2) - 91*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(t1,3)*(176 + 264*t2 - 1130*Power(t2,2) - 
                  757*Power(t2,3) - 7*Power(t2,4) + 87*Power(t2,5)) + 
               Power(t1,2)*(96 - 706*t2 + 1148*Power(t2,2) + 
                  1139*Power(t2,3) + 1763*Power(t2,4) - 
                  480*Power(t2,5) - 97*Power(t2,6)) + 
               t2*(84 - 176*t2 + 299*Power(t2,2) + 367*Power(t2,3) - 
                  145*Power(t2,4) + 656*Power(t2,5) - 218*Power(t2,6) - 
                  3*Power(t2,7)) + 
               t1*(-120 + 156*t2 + 304*Power(t2,2) - 1823*Power(t2,3) + 
                  49*Power(t2,4) - 1748*Power(t2,5) + 551*Power(t2,6) + 
                  37*Power(t2,7))) + 
            Power(s1,7)*(-7*Power(s2,3) + 8*Power(t1,3) + 
               3*(-7 + 4*t2) + t1*(8 + 7*t2) + 
               Power(t1,2)*(-27 + 8*t2) + 
               Power(s2,2)*(-32 - 48*t1 + 59*t2) + 
               s2*(74 - 21*Power(t1,2) - 68*t2 - 7*Power(t2,2) + 
                  t1*(3 + 50*t2))) - 
            Power(s1,6)*(62 + 13*Power(s2,4) - 22*Power(t1,4) + 
               Power(s2,3)*(-12 + 13*t1 - 75*t2) - 143*t2 + 
               38*Power(t2,2) + Power(t1,3)*(43 + 52*t2) + 
               Power(t1,2)*(-6 - 145*t2 + 6*Power(t2,2)) + 
               t1*(56 + 12*t2 + 59*Power(t2,2)) + 
               Power(s2,2)*(31 + 58*Power(t1,2) - 64*t2 + 
                  231*Power(t2,2) - t1*(134 + 313*t2)) + 
               s2*(-129 + 31*Power(t1,3) + Power(t1,2)*(226 - 117*t2) + 
                  530*t2 - 205*Power(t2,2) - 21*Power(t2,3) + 
                  t1*(-366 - 106*t2 + 151*Power(t2,2)))) + 
            Power(s1,5)*(-31 + 10*Power(s2,5) + 12*Power(t1,5) + 
               Power(t1,4)*(3 - 100*t2) + 438*t2 - 424*Power(t2,2) + 
               88*Power(t2,3) + Power(s2,4)*(-63 - 39*t1 + 118*t2) + 
               Power(t1,3)*(-207 + 142*t2 + 140*Power(t2,2)) + 
               Power(t1,2)*(292 + 104*t2 - 306*Power(t2,2) - 
                  20*Power(t2,3)) + 
               t1*(-267 + 137*t2 - 66*Power(t2,2) + 151*Power(t2,3)) + 
               Power(s2,3)*(-147 + 88*Power(t1,2) + 108*t2 - 
                  278*Power(t2,2) - t1*(147 + 4*t2)) + 
               Power(s2,2)*(124 - 126*Power(t1,3) - 431*t2 + 
                  378*Power(t2,2) + 483*Power(t2,3) + 
                  13*Power(t1,2)*(37 + 33*t2) + 
                  t1*(153 - 1205*t2 - 882*Power(t2,2))) + 
               s2*(-191 + 15*Power(t1,4) - 499*t2 + 1510*Power(t2,2) - 
                  306*Power(t2,3) - 35*Power(t2,4) + 
                  Power(t1,3)*(-425 + 96*t2) + 
                  Power(t1,2)*(569 + 1501*t2 - 258*Power(t2,2)) + 
                  t1*(148 - 1858*t2 - 701*Power(t2,2) + 248*Power(t2,3)))\
) + Power(s1,4)*(185 + 16*Power(s2,6) + Power(t1,5)*(36 - 40*t2) + 
               74*t2 - 1185*Power(t2,2) + 710*Power(t2,3) - 
               150*Power(t2,4) - 2*Power(s2,5)*(11 + 39*t1 + 10*t2) + 
               Power(t1,4)*(-364 - 71*t2 + 180*Power(t2,2)) + 
               Power(t1,3)*(475 + 1027*t2 - 114*Power(t2,2) - 
                  200*Power(t2,3)) + 
               t1*(-153 + 1145*t2 + 74*Power(t2,2) + 230*Power(t2,3) - 
                  185*Power(t2,4)) + 
               Power(t1,2)*(-293 - 1311*t2 - 535*Power(t2,2) + 
                  300*Power(t2,3) + 50*Power(t2,4)) + 
               Power(s2,4)*(224 + 136*Power(t1,2) + 287*t2 - 
                  369*Power(t2,2) + t1*(-48 + 101*t2)) + 
               Power(s2,3)*(24 - 30*Power(t1,3) + 1076*t2 - 
                  683*Power(t2,2) + 510*Power(t2,3) - 
                  2*Power(t1,2)*(76 + 173*t2) + 
                  2*t1*(-325 + 456*t2 + 86*Power(t2,2))) + 
               s2*(-349 + 32*Power(t1,5) + 1576*t2 + 420*Power(t2,2) - 
                  2190*Power(t2,3) + 205*Power(t2,4) + 35*Power(t2,5) - 
                  Power(t1,4)*(327 + 110*t2) + 
                  Power(t1,3)*(570 + 2124*t2 - 50*Power(t2,2)) + 
                  6*Power(t1,2)*
                   (66 - 384*t2 - 673*Power(t2,2) + 45*Power(t2,3)) + 
                  t1*(-510 - 760*t2 + 3724*Power(t2,2) + 
                     1810*Power(t2,3) - 235*Power(t2,4))) + 
               Power(s2,2)*(-76*Power(t1,4) + 
                  Power(t1,3)*(417 + 615*t2) + 
                  Power(t1,2)*(341 - 2595*t2 - 1233*Power(t2,2)) + 
                  t1*(-161 - 2018*t2 + 4031*Power(t2,2) + 
                     1395*Power(t2,3)) - 
                  5*(-89 + 142*t2 - 538*Power(t2,2) + 346*Power(t2,3) + 
                     119*Power(t2,4)))) + 
            Power(s1,3)*(107 + 18*Power(t1,6) + 
               4*Power(s2,6)*(6 + t1 - 13*t2) - 979*t2 + 
               86*Power(t2,2) + 1600*Power(t2,3) - 725*Power(t2,4) + 
               172*Power(t2,5) - 
               2*Power(s2,5)*
                (65 + 16*Power(t1,2) + t1*(24 - 131*t2) - 69*t2 + 
                  4*Power(t2,2)) + 
               Power(t1,5)*(-217 - 131*t2 + 48*Power(t2,2)) + 
               Power(t1,4)*(63 + 1298*t2 + 223*Power(t2,2) - 
                  160*Power(t2,3)) + 
               Power(t1,3)*(324 - 1303*t2 - 2034*Power(t2,2) - 
                  104*Power(t2,3) + 160*Power(t2,4)) + 
               Power(t1,2)*(-612 + 397*t2 + 2361*Power(t2,2) + 
                  980*Power(t2,3) - 95*Power(t2,4) - 48*Power(t2,5)) + 
               t1*(293 + 1058*t2 - 1980*Power(t2,2) - 546*Power(t2,3) - 
                  300*Power(t2,4) + 109*Power(t2,5)) + 
               2*Power(s2,4)*
                (63 + 36*Power(t1,3) - 574*t2 - 242*Power(t2,2) + 
                  278*Power(t2,3) - Power(t1,2)*(51 + 253*t2) + 
                  t1*(260 + 78*t2 - 25*Power(t2,2))) + 
               Power(s2,3)*(-316 - 64*Power(t1,4) - 217*t2 - 
                  2952*Power(t2,2) + 1412*Power(t2,3) - 
                  515*Power(t2,4) + 2*Power(t1,3)*(174 + 89*t2) + 
                  Power(t1,2)*(-1076 + 327*t2 + 526*Power(t2,2)) + 
                  t1*(218 + 3766*t2 - 2118*Power(t2,2) - 
                     388*Power(t2,3))) + 
               Power(s2,2)*(-92 + 20*Power(t1,5) - 1687*t2 + 
                  1531*Power(t2,2) - 5830*Power(t2,3) + 
                  2980*Power(t2,4) + 441*Power(t2,5) + 
                  Power(t1,4)*(-204 + 214*t2) + 
                  Power(t1,3)*(540 - 1225*t2 - 1176*Power(t2,2)) + 
                  Power(t1,2)*
                   (-848 - 2837*t2 + 5379*Power(t2,2) + 
                     1802*Power(t2,3)) + 
                  t1*(1216 + 521*t2 + 6884*Power(t2,2) - 
                     6754*Power(t2,3) - 1340*Power(t2,4))) + 
               s2*(-77 + 1136*t2 - 4370*Power(t2,2) + 730*Power(t2,3) + 
                  1670*Power(t2,4) + 16*Power(t2,5) - 21*Power(t2,6) - 
                  12*Power(t1,5)*(3 + 8*t2) + 
                  Power(t1,4)*(291 + 1119*t2 + 260*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (-381 + 866*t2 + 2059*Power(t2,2) + 70*Power(t2,3)) + 
                  Power(t1,2)*
                   (-956 - 2211*t2 + 3452*Power(t2,2) + 
                     5662*Power(t2,3) - 105*Power(t2,4)) + 
                  t1*(-136 + 3218*t2 + 1412*Power(t2,2) - 
                     3636*Power(t2,3) - 2455*Power(t2,4) + 
                     122*Power(t2,5)))) + 
            Power(s1,2)*(-88 - 243*t2 + 1793*Power(t2,2) - 
               392*Power(t2,3) - 1140*Power(t2,4) + 451*Power(t2,5) - 
               122*Power(t2,6) - 14*Power(t1,6)*(1 + 3*t2) + 
               Power(t1,5)*(-173 + 497*t2 + 174*Power(t2,2) - 
                  24*Power(t2,3)) + 
               Power(t1,4)*(340 + 276*t2 - 1703*Power(t2,2) - 
                  273*Power(t2,3) + 70*Power(t2,4)) + 
               Power(t1,3)*(-424 - 1375*t2 + 1042*Power(t2,2) + 
                  2010*Power(t2,3) + 221*Power(t2,4) - 68*Power(t2,5)) + 
               t1*(173 - 1046*t2 - 2160*Power(t2,2) + 
                  1740*Power(t2,3) + 664*Power(t2,4) + 
                  192*Power(t2,5) - 17*Power(t2,6)) + 
               Power(t1,2)*(182 + 1981*t2 + 522*Power(t2,2) - 
                  2137*Power(t2,3) - 860*Power(t2,4) - 63*Power(t2,5) + 
                  22*Power(t2,6)) + 
               4*Power(s2,6)*(3*t1 + t2*(-19 + 15*t2)) + 
               2*Power(s2,5)*
                (-19 + 201*t2 - 143*Power(t2,2) + 22*Power(t2,3) + 
                  8*Power(t1,2)*(-2 + 3*t2) - 
                  3*t1*(3 - 28*t2 + 51*Power(t2,2))) + 
               Power(s2,4)*(44 + Power(t1,3)*(8 - 144*t2) - 456*t2 + 
                  2108*Power(t2,2) + 360*Power(t2,3) - 
                  439*Power(t2,4) + 
                  Power(t1,2)*(-34 + 228*t2 + 660*Power(t2,2)) - 
                  2*t1*(-109 + 786*t2 + 79*Power(t2,2) + 33*Power(t2,3))\
) + Power(s2,3)*(10 + 926*t2 + 427*Power(t2,2) + 3870*Power(t2,3) - 
                  1398*Power(t2,4) + 287*Power(t2,5) + 
                  48*Power(t1,4)*(1 + 3*t2) + 
                  Power(t1,3)*(82 - 938*t2 - 300*Power(t2,2)) + 
                  Power(t1,2)*
                   (90 + 3226*t2 - 105*Power(t2,2) - 382*Power(t2,3)) + 
                  t1*(-526 - 384*t2 - 7352*Power(t2,2) + 
                     2352*Power(t2,3) + 367*Power(t2,4))) + 
               Power(s2,2)*(-84 + 515*t2 + 2406*Power(t2,2) - 
                  1573*Power(t2,3) + 6125*Power(t2,4) - 
                  2628*Power(t2,5) - 189*Power(t2,6) - 
                  4*Power(t1,5)*(13 + 12*t2) + 
                  Power(t1,4)*(68 + 570*t2 - 216*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1194 - 1825*t2 + 1192*Power(t2,2) + 
                     1098*Power(t2,3)) + 
                  Power(t1,2)*
                   (1398 + 2871*t2 + 6414*Power(t2,2) - 
                     5377*Power(t2,3) - 1428*Power(t2,4)) + 
                  t1*(36 - 4315*t2 - 510*Power(t2,2) - 
                     10074*Power(t2,3) + 6116*Power(t2,4) + 
                     783*Power(t2,5))) + 
               s2*(225 + 16*Power(t1,6) - 50*t2 - 1232*Power(t2,2) + 
                  5564*Power(t2,3) - 1655*Power(t2,4) - 
                  574*Power(t2,5) - 117*Power(t2,6) + 7*Power(t2,7) + 
                  6*Power(t1,5)*(-14 + 15*t2 + 17*Power(t2,2)) - 
                  Power(t1,4)*
                   (-1081 + 512*t2 + 1393*Power(t2,2) + 270*Power(t2,3)) \
+ Power(t1,3)*(-1090 - 3384*t2 + 1766*Power(t2,2) + 3860*Power(t2,3) + 
                     225*Power(t2,4)) + 
                  Power(t1,2)*
                   (316 + 3896*t2 + 4377*Power(t2,2) - 
                     2222*Power(t2,3) - 4378*Power(t2,4) - 
                     39*Power(t2,5)) + 
                  t1*(-496 + 572*t2 - 6818*Power(t2,2) - 
                     1156*Power(t2,3) + 1654*Power(t2,4) + 
                     1862*Power(t2,5) - 25*Power(t2,6)))) - 
            s1*(48 - 248*t2 - 141*Power(t2,2) + 1389*Power(t2,3) - 
               385*Power(t2,4) - 402*Power(t2,5) + 158*Power(t2,6) - 
               48*Power(t2,7) - 
               2*Power(t1,6)*(-14 + 8*t2 + 15*Power(t2,2)) + 
               Power(t1,5)*(-72 - 346*t2 + 347*Power(t2,2) + 
                  99*Power(t2,3) - 4*Power(t2,4)) + 
               Power(t1,4)*(208 + 544*t2 + 725*Power(t2,2) - 
                  968*Power(t2,3) - 146*Power(t2,4) + 12*Power(t2,5)) + 
               Power(t1,3)*(-244 - 612*t2 - 1758*Power(t2,2) + 
                  75*Power(t2,3) + 991*Power(t2,4) + 126*Power(t2,5) - 
                  12*Power(t2,6)) + 
               Power(t1,2)*(124 + 144*t2 + 2150*Power(t2,2) + 
                  1063*Power(t2,3) - 975*Power(t2,4) - 364*Power(t2,5) - 
                  60*Power(t2,6) + 4*Power(t2,7)) + 
               t1*(-92 + 526*t2 - 1269*Power(t2,2) - 1758*Power(t2,3) + 
                  785*Power(t2,4) + 335*Power(t2,5) + 58*Power(t2,6) + 
                  11*Power(t2,7)) + 
               4*Power(s2,6)*(2 + 2*t2 - 20*Power(t2,2) + 
                  7*Power(t2,3) + t1*(-2 + 4*t2 + 3*Power(t2,2))) - 
               2*Power(s2,5)*(22 + 38*t2 - 211*Power(t2,2) + 
                  123*Power(t2,3) - 17*Power(t2,4) + 
                  Power(t1,2)*(6 + 20*t2) + 
                  t1*(-28 + 30*t2 - 92*Power(t2,2) + 69*Power(t2,3))) + 
               Power(s2,4)*(58 + 58*t2 - 530*Power(t2,2) + 
                  1668*Power(t2,3) + 101*Power(t2,4) - 174*Power(t2,5) + 
                  Power(t1,3)*(88 - 72*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (-51 - 29*t2 + 89*Power(t2,2) + 173*Power(t2,3)) - 
                  t1*(44 - 472*t2 + 1616*Power(t2,2) + 40*Power(t2,3) + 
                     73*Power(t2,4))) + 
               Power(s2,3)*(-144 + 8*t2 + 904*Power(t2,2) + 
                  299*Power(t2,3) + 2453*Power(t2,4) - 680*Power(t2,5) + 
                  80*Power(t2,6) + 
                  16*Power(t1,4)*(-7 + 5*t2 + 6*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (27 - 95*t2 + 434*Power(t2,2) + 93*Power(t2,3)) + 
                  Power(t1,2)*
                   (76 + 100*t2 + 3264*Power(t2,2) + 163*Power(t2,3) - 
                     130*Power(t2,4)) + 
                  t1*(234 - 970*t2 - 118*Power(t2,2) - 
                     6006*Power(t2,3) + 1263*Power(t2,4) + 
                     160*Power(t2,5))) + 
               Power(s2,2)*(84 - 260*t2 + 722*Power(t2,2) + 
                  1531*Power(t2,3) - 773*Power(t2,4) + 
                  3179*Power(t2,5) - 1186*Power(t2,6) - 41*Power(t2,7) - 
                  4*Power(t1,5)*(-12 + 20*t2 + 9*Power(t2,2)) + 
                  Power(t1,4)*
                   (162 + 122*t2 + 548*Power(t2,2) - 94*Power(t2,3)) + 
                  Power(t1,3)*
                   (184 - 2324*t2 - 2042*Power(t2,2) + 
                     377*Power(t2,3) + 498*Power(t2,4)) + 
                  Power(t1,2)*
                   (-586 + 2546*t2 + 3162*Power(t2,2) + 
                     5681*Power(t2,3) - 2592*Power(t2,4) - 
                     585*Power(t2,5)) + 
                  t1*(108 + 340*t2 - 4922*Power(t2,2) - 
                     101*Power(t2,3) - 6803*Power(t2,4) + 
                     2873*Power(t2,5) + 258*Power(t2,6))) + 
               s2*(-28 + 462*t2 - 347*Power(t2,2) - 452*Power(t2,3) + 
                  3367*Power(t2,4) - 1161*Power(t2,5) - 10*Power(t2,6) - 
                  70*Power(t2,7) + Power(t2,8) + 
                  4*Power(t1,6)*(-1 + 6*t2) + 
                  2*Power(t1,5)*
                   (-49 - 93*t2 + 34*Power(t2,2) + 22*Power(t2,3)) - 
                  Power(t1,4)*
                   (100 - 2142*t2 + 159*Power(t2,2) + 737*Power(t2,3) + 
                     125*Power(t2,4)) + 
                  Power(t1,3)*
                   (62 - 1822*t2 - 4430*Power(t2,2) + 616*Power(t2,3) + 
                     1737*Power(t2,4) + 124*Power(t2,5)) + 
                  Power(t1,2)*
                   (304 + 140*t2 + 4820*Power(t2,2) + 3705*Power(t2,3) - 
                     459*Power(t2,4) - 1773*Power(t2,5) - 48*Power(t2,6)) \
+ t1*(-136 - 824*t2 + 816*Power(t2,2) - 6022*Power(t2,3) - 
                     376*Power(t2,4) + 202*Power(t2,5) + 
                     751*Power(t2,6) + 4*Power(t2,7))))))*
       R1(1 - s + s1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s1 + t1 - t2)*
       (-1 + s - s1 + t2)*Power(s - s1 + t2,2)*Power(-s1 + s2 - t1 + t2,3)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)) + 
    (8*(66 + 25*s2 - 4*Power(s2,2) + 9*Power(s2,3) - 4*Power(s2,4) - 
         266*t1 - 164*s2*t1 + 8*Power(s2,2)*t1 - 12*Power(s2,3)*t1 + 
         10*Power(s2,4)*t1 + 410*Power(t1,2) + 243*s2*Power(t1,2) + 
         18*Power(s2,2)*Power(t1,2) - 9*Power(s2,3)*Power(t1,2) - 
         6*Power(s2,4)*Power(t1,2) - 270*Power(t1,3) - 
         128*s2*Power(t1,3) - 22*Power(s2,2)*Power(t1,3) + 
         12*Power(s2,3)*Power(t1,3) + 60*Power(t1,4) + 
         24*s2*Power(t1,4) + 3*Power(s1,5)*s2*(-2 + s2 + t1) + 108*t2 + 
         177*s2*t2 + 38*Power(s2,2)*t2 + Power(s2,3)*t2 + 
         4*Power(s2,4)*t2 - 4*Power(s2,5)*t2 - 312*t1*t2 - 543*s2*t1*t2 - 
         184*Power(s2,2)*t1*t2 + 7*Power(s2,3)*t1*t2 + 
         2*Power(s2,4)*t1*t2 + 6*Power(s2,5)*t1*t2 + 300*Power(t1,2)*t2 + 
         486*s2*Power(t1,2)*t2 + 152*Power(s2,2)*Power(t1,2)*t2 + 
         14*Power(s2,3)*Power(t1,2)*t2 - 12*Power(s2,4)*Power(t1,2)*t2 - 
         88*Power(t1,3)*t2 - 104*s2*Power(t1,3)*t2 - 
         40*Power(s2,2)*Power(t1,3)*t2 - 8*Power(t1,4)*t2 + 
         32*Power(t2,2) + 179*s2*Power(t2,2) + 
         135*Power(s2,2)*Power(t2,2) + 11*Power(s2,3)*Power(t2,2) + 
         5*Power(s2,4)*Power(t2,2) - 5*Power(s2,5)*Power(t2,2) - 
         34*t1*Power(t2,2) - 298*s2*t1*Power(t2,2) - 
         262*Power(s2,2)*t1*Power(t2,2) - 34*Power(s2,3)*t1*Power(t2,2) + 
         8*Power(s2,4)*t1*Power(t2,2) - 24*Power(t1,2)*Power(t2,2) + 
         103*s2*Power(t1,2)*Power(t2,2) + 
         62*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         16*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         30*Power(t1,3)*Power(t2,2) + 12*s2*Power(t1,3)*Power(t2,2) - 
         12*Power(t2,3) + 29*s2*Power(t2,3) + 
         80*Power(s2,2)*Power(t2,3) + 14*Power(s2,3)*Power(t2,3) + 
         2*Power(s2,4)*Power(t2,3) + 32*t1*Power(t2,3) + 
         7*s2*t1*Power(t2,3) - 14*Power(s2,2)*t1*Power(t2,3) - 
         18*Power(s2,3)*t1*Power(t2,3) - 26*Power(t1,2)*Power(t2,3) - 
         34*s2*Power(t1,2)*Power(t2,3) - 
         4*Power(s2,2)*Power(t1,2)*Power(t2,3) - 2*Power(t2,4) - 
         4*s2*Power(t2,4) - 9*Power(s2,2)*Power(t2,4) + 
         3*Power(s2,3)*Power(t2,4) + 4*t1*Power(t2,4) + 
         26*s2*t1*Power(t2,4) + 4*Power(s2,2)*t1*Power(t2,4) - 
         6*s2*Power(t2,5) - 2*Power(s,5)*
          (-1 + Power(t1,2) - 2*t1*(-1 + t2) - t2 + Power(t2,2) + 
            s2*(1 - 2*t1 + t2)) + 
         Power(s,4)*(Power(s2,2)*(12 - 19*t1 + 7*t2) + 
            s2*(-22 + 16*Power(t1,2) + t1*(21 - 10*t2) - 13*t2 + 
               2*Power(t2,2)) - 
            2*(-4 + 5*Power(t1,2) + Power(t1,3) - 3*t2 + 
               t1*(4 - 3*t2)*t2 - 5*Power(t2,2) + 2*Power(t2,3)) + 
            s1*(-8 + 11*t1 + 8*Power(t1,2) - 9*t2 - 12*t1*t2 + 
               4*Power(t2,2) + s2*(10 - 11*t1 + 7*t2))) - 
         Power(s1,4)*(-6 + Power(s2,3) + 9*t1 - 7*Power(t1,2) + 
            2*Power(t1,3) + Power(s2,2)*(10 - 21*t1 + 18*t2) + 
            s2*(6 - 6*Power(t1,2) - 21*t2 + t1*(-2 + 9*t2))) + 
         Power(s1,3)*(3 - 7*Power(s2,4) + Power(t1,2)*(1 - 10*t2) + 
            Power(s2,3)*(-6 + 15*t1 - 4*t2) - 15*t2 + 
            4*Power(t1,3)*(1 + t2) + t1*(-2 + 13*t2) + 
            Power(s2,2)*(1 + 36*Power(t1,2) + 53*t2 + 36*Power(t2,2) - 
               t1*(22 + 81*t2)) + 
            s2*(60 - 8*Power(t1,3) + Power(t1,2)*(63 - 6*t2) + 34*t2 - 
               21*Power(t2,2) + t1*(-130 - 52*t2 + 9*Power(t2,2)))) + 
         Power(s,3)*(Power(s2,3)*(-22 + 32*t1 - 9*t2) + 
            Power(s2,2)*(59 - 38*Power(t1,2) + 36*t2 + 9*Power(t2,2) - 
               t1*(45 + 7*t2)) + 
            Power(s1,2)*(8 + 3*Power(s2,2) - 12*Power(t1,2) + 
               s2*(-21 + 6*t1 - 5*t2) + 4*t2 - 2*Power(t2,2) + 
               t1*(-7 + 12*t2)) + 
            s2*(-55 + 8*Power(t1,3) - 52*t2 - 28*Power(t2,2) + 
               10*Power(t2,3) + Power(t1,2)*(49 + 24*t2) + 
               t1*(24 + 13*t2 - 30*Power(t2,2))) - 
            2*(-9 - 5*t2 + 2*Power(t2,2) - 8*Power(t2,3) + 
               Power(t2,4) + Power(t1,3)*(9 + 2*t2) - 
               Power(t1,2)*(1 + 2*t2 + 3*Power(t2,2)) + 
               t1*(7 - 4*t2 + 7*Power(t2,2))) - 
            s1*(3 + Power(s2,3) - 19*Power(t1,2) - 8*Power(t1,3) - 
               4*t2 + 20*Power(t2,2) - 4*Power(t2,3) + 
               Power(s2,2)*(37 - 46*t1 + 26*t2) + 
               s2*(-51 + 54*Power(t1,2) + t1*(28 - 45*t2) - 48*t2 + 
                  5*Power(t2,2)) + t1*(24 - 13*t2 + 12*Power(t2,2)))) + 
         Power(s1,2)*(-3*Power(s2,5) - 8*Power(t1,4) + 
            Power(s2,4)*(14 - 9*t1 + 18*t2) + 
            Power(t1,3)*(60 + 2*t2 - 2*Power(t2,2)) - 
            Power(t1,2)*(147 + 42*t2 + Power(t2,2)) + 
            2*(-25 - 8*t2 + 5*Power(t2,2)) + 
            t1*(149 + 38*t2 + 5*Power(t2,2)) + 
            Power(s2,3)*(31 + 42*Power(t1,2) + 37*t2 + 14*Power(t2,2) - 
               13*t1*(4 + 5*t2)) + 
            Power(s2,2)*(38 - 6*Power(t1,3) + 
               Power(t1,2)*(51 - 68*t2) + 74*t2 - 85*Power(t2,2) - 
               30*Power(t2,3) + t1*(-188 + 32*t2 + 103*Power(t2,2))) + 
            s2*(25 - 108*t2 - 54*Power(t2,2) - 3*Power(t2,3) + 
               2*Power(t1,3)*(29 + 8*t2) - 
               Power(t1,2)*(79 + 180*t2 + 6*Power(t2,2)) + 
               t1*(10 + 302*t2 + 124*Power(t2,2) - 3*Power(t2,3)))) + 
         s1*(-25 + 15*t2 + 25*Power(t2,2) + Power(t2,3) + 
            4*Power(t1,4)*(7 + 2*t2) + Power(s2,5)*(4 - 6*t1 + 8*t2) + 
            Power(s2,4)*(-6 + 12*Power(t1,2) - 19*t2 + t1*t2 - 
               13*Power(t2,2)) - 
            2*Power(t1,3)*(22 + 43*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(5 + 158*t2 + 67*Power(t2,2) + 4*Power(t2,3)) - 
            t1*(-36 + 103*t2 + 68*Power(t2,2) + 13*Power(t2,3)) - 
            Power(s2,3)*(10 + 48*t2 + 45*Power(t2,2) + 12*Power(t2,3) + 
               31*Power(t1,2)*(1 + 2*t2) - 
               t1*(19 + 96*t2 + 68*Power(t2,2))) + 
            Power(s2,2)*(-49 - 174*t2 - 155*Power(t2,2) + 
               51*Power(t2,3) + 9*Power(t2,4) + 
               Power(t1,3)*(70 + 8*t2) + 
               Power(t1,2)*(-195 - 124*t2 + 36*Power(t2,2)) + 
               t1*(208 + 460*t2 + 4*Power(t2,2) - 47*Power(t2,3))) + 
            s2*(-98 - 8*Power(t1,4) - 202*t2 + 19*Power(t2,2) + 
               30*Power(t2,3) + 15*Power(t2,4) + 
               Power(t1,3)*(94 - 68*t2 - 8*Power(t2,2)) + 
               t1*(399 + 280*t2 - 179*Power(t2,2) - 100*Power(t2,3)) + 
               Power(t1,2)*(-403 - 20*t2 + 151*Power(t2,2) + 
                  6*Power(t2,3)))) + 
         Power(s,2)*(Power(s2,4)*(16 - 23*t1 + 5*t2) + 
            Power(s1,3)*(4 - 3*Power(s2,2) + 8*Power(t1,2) + 
               s2*(6 + 8*t1 - 3*t2) + 3*t2 - t1*(3 + 4*t2)) + 
            Power(s2,3)*(-56 + 36*Power(t1,2) - 41*t2 - 
               21*Power(t2,2) + t1*(41 + 32*t2)) + 
            Power(s2,2)*(56 - 6*Power(t1,3) + 65*t2 + 31*Power(t2,2) - 
               7*Power(t2,3) - 8*Power(t1,2)*(13 + 9*t2) + 
               t1*(5 + 18*t2 + 49*Power(t2,2))) + 
            s2*(7 - 76*t2 - 78*Power(t2,2) - 25*Power(t2,3) + 
               6*Power(t2,4) + 2*Power(t1,3)*(31 + 8*t2) - 
               3*Power(t1,2)*(9 + 10*t2) + 
               t1*(21 + 141*t2 + 27*Power(t2,2) - 14*Power(t2,3))) - 
            2*(4*Power(t1,4) + Power(t1,3)*(2 + 5*t2 + Power(t2,2)) - 
               Power(t1,2)*(1 + 8*t2 + 4*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-7 + 46*t2 - 7*Power(t2,2) + 3*Power(t2,3) + 
                  Power(t2,4)) - 
               2*(-8 + 8*t2 + 9*Power(t2,2) - 6*Power(t2,3) + 
                  2*Power(t2,4))) - 
            Power(s1,2)*(48 + 7*Power(s2,3) + Power(t1,2) + 
               12*Power(t1,3) + Power(s2,2)*(-36 + 14*t1 - 17*t2) + 
               26*t2 - 2*Power(t2,2) + 
               t1*(-39 + 14*t2 - 6*Power(t2,2)) + 
               s2*(-30 - 66*Power(t1,2) + 8*t2 - 12*Power(t2,2) + 
                  t1*(33 + 69*t2))) + 
            s1*(55 + 2*Power(s2,4) - 2*t2 + 46*Power(t2,2) - 
               13*Power(t2,3) + 4*Power(t1,3)*(10 + 3*t2) + 
               Power(s2,3)*(42 - 63*t1 + 37*t2) - 
               Power(t1,2)*(43 + 26*t2 + 12*Power(t2,2)) + 
               t1*(35 - 23*t2 + 23*Power(t2,2)) + 
               Power(s2,2)*(-42 + 112*Power(t1,2) - 66*t2 - 
                  7*Power(t2,2) - t1*(9 + 59*t2)) + 
               s2*(-66 - 24*Power(t1,3) + 29*t2 + 27*Power(t2,2) - 
                  15*Power(t2,3) - Power(t1,2)*(59 + 54*t2) + 
                  t1*(16 + 32*t2 + 75*Power(t2,2))))) - 
         s*(Power(s1,4)*(6 + 3*Power(s2,2) - 3*t1 + 2*Power(t1,2) + 
               s2*(-13 + 10*t1 - 3*t2)) + Power(s2,5)*(4 - 6*t1 + t2) + 
            Power(s2,4)*(-17 + 12*Power(t1,2) - 20*t2 - 17*Power(t2,2) + 
               t1*(13 + 25*t2)) + 
            Power(s2,3)*(11 + 27*t2 + 16*Power(t2,2) + Power(t2,3) - 
               Power(t1,2)*(67 + 60*t2) + 
               t1*(28 + 23*t2 + 34*Power(t2,2))) + 
            Power(s2,2)*(-7 - 126*t2 - 78*Power(t2,2) + 18*Power(t2,3) + 
               7*Power(t2,4) + Power(t1,3)*(68 + 8*t2) + 
               2*Power(t1,2)*(-41 + t2 + 16*Power(t2,2)) + 
               t1*(77 + 201*t2 - 53*Power(t2,2) - 35*Power(t2,3))) + 
            2*(31 + 80*t2 + 35*Power(t2,2) - 12*Power(t2,3) + 
               2*Power(t2,4) + 2*Power(t1,4)*(5 + 2*t2) - 
               Power(t1,3)*(53 + 25*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(141 + 119*t2 + 6*Power(t2,2) + 
                  3*Power(t2,3)) - 
               t1*(121 + 182*t2 + 22*Power(t2,2) - 7*Power(t2,3) + 
                  2*Power(t2,4))) - 
            s2*(73 + 8*Power(t1,4) + 38*t2 - 63*Power(t2,2) - 
               40*Power(t2,3) - 20*Power(t2,4) + 
               Power(t1,3)*(6 + 56*t2 + 8*Power(t2,2)) - 
               Power(t1,2)*(-113 + 60*t2 + 107*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-150 + 39*t2 + 162*Power(t2,2) + 71*Power(t2,3) + 
                  2*Power(t2,4))) - 
            Power(s1,3)*(-7 + 9*Power(s2,3) - 15*Power(t1,2) + 
               8*Power(t1,3) + 12*t2 + t1*(2 + 9*t2) + 
               Power(s2,2)*(7 - 34*t1 + 20*t2) - 
               s2*(49 + 34*Power(t1,2) + 48*t2 + 9*Power(t2,2) - 
                  t1*(38 + 43*t2))) + 
            Power(s1,2)*(-92 - 7*Power(s2,4) - 38*t2 + 10*Power(t2,2) + 
               2*Power(t1,3)*(13 + 6*t2) + 
               Power(s2,3)*(29 - 16*t1 + 28*t2) - 
               2*Power(t1,2)*(32 + 16*t2 + 3*Power(t2,2)) + 
               t1*(165 + 26*t2 + 23*Power(t2,2)) + 
               Power(s2,2)*(22 + 110*Power(t1,2) + 58*t2 + 
                  38*Power(t2,2) - t1*(94 + 147*t2)) - 
               s2*(-6 + 24*Power(t1,3) + 69*t2 + 37*Power(t2,2) + 
                  9*Power(t2,3) + Power(t1,2)*(-53 + 36*t2) + 
                  t1*(150 - 5*t2 - 54*Power(t2,2)))) + 
            s1*(17 + Power(s2,5) - 16*Power(t1,4) + 12*t2 + 
               55*Power(t2,2) - 8*Power(t2,3) + 
               Power(s2,4)*(19 - 34*t1 + 26*t2) - 
               4*Power(t1,3)*(-8 + 2*t2 + Power(t2,2)) + 
               Power(t1,2)*(-45 + 26*t2 + 11*Power(t2,2) + 
                  4*Power(t2,3)) - 
               t1*(-2 + 93*t2 + 38*Power(t2,2) + 7*Power(t2,3)) - 
               Power(s2,2)*(-12 + 12*Power(t1,3) - 42*t2 + 
                  69*Power(t2,2) + 28*Power(t2,3) + 
                  5*Power(t1,2)*(13 + 28*t2) + 
                  t1*(88 - 171*t2 - 148*Power(t2,2))) + 
               Power(s2,3)*(78*Power(t1,2) - t1*(20 + 27*t2) - 
                  4*(3 + 11*t2 + 5*Power(t2,2))) + 
               s2*(-11 - 78*t2 - 20*Power(t2,2) - 18*Power(t2,3) + 
                  3*Power(t2,4) + 8*Power(t1,3)*(15 + 4*t2) - 
                  2*Power(t1,2)*(119 + 93*t2 + 3*Power(t2,2)) + 
                  t1*(260 + 339*t2 + 104*Power(t2,2) - 19*Power(t2,3))))))*
       R2(1 - s1 - t1 + t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s1 + t1 - t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) - 
    (8*(30 + 116*s2 + 95*Power(s2,2) - 13*Power(s2,3) - Power(s2,4) - 
         3*Power(s2,5) - 235*t1 - 654*s2*t1 - 281*Power(s2,2)*t1 + 
         23*Power(s2,3)*t1 + 10*Power(s2,4)*t1 + 9*Power(s2,5)*t1 + 
         627*Power(t1,2) + 1184*s2*Power(t1,2) + 
         349*Power(s2,2)*Power(t1,2) + Power(s2,3)*Power(t1,2) - 
         24*Power(s2,4)*Power(t1,2) - 9*Power(s2,5)*Power(t1,2) - 
         715*Power(t1,3) - 876*s2*Power(t1,3) - 
         255*Power(s2,2)*Power(t1,3) - 19*Power(s2,3)*Power(t1,3) + 
         22*Power(s2,4)*Power(t1,3) + 3*Power(s2,5)*Power(t1,3) + 
         323*Power(t1,4) + 236*s2*Power(t1,4) + 
         112*Power(s2,2)*Power(t1,4) + 8*Power(s2,3)*Power(t1,4) - 
         7*Power(s2,4)*Power(t1,4) - 14*Power(t1,5) - 6*s2*Power(t1,5) - 
         20*Power(s2,2)*Power(t1,5) - 16*Power(t1,6) + 
         Power(s1,7)*(-2*Power(s2,3) - Power(s2,2)*(-1 + t1) + 
            (-2 + t1)*Power(t1,2) - s2*t1*(3 + 2*t1)) - 
         Power(s,8)*(t1 - t2)*(1 - s2 + t1 - t2) + 125*t2 + 335*s2*t2 + 
         363*Power(s2,2)*t2 + 202*Power(s2,3)*t2 - 14*Power(s2,4)*t2 - 
         9*Power(s2,5)*t2 - 6*Power(s2,6)*t2 - 496*t1*t2 - 
         1245*s2*t1*t2 - 1432*Power(s2,2)*t1*t2 - 455*Power(s2,3)*t1*t2 + 
         10*Power(s2,4)*t1*t2 + 28*Power(s2,5)*t1*t2 + 
         12*Power(s2,6)*t1*t2 + 573*Power(t1,2)*t2 + 
         1531*s2*Power(t1,2)*t2 + 1675*Power(s2,2)*Power(t1,2)*t2 + 
         450*Power(s2,3)*Power(t1,2)*t2 + 18*Power(s2,4)*Power(t1,2)*t2 - 
         33*Power(s2,5)*Power(t1,2)*t2 - 6*Power(s2,6)*Power(t1,2)*t2 - 
         44*Power(t1,3)*t2 - 559*s2*Power(t1,3)*t2 - 
         642*Power(s2,2)*Power(t1,3)*t2 - 
         255*Power(s2,3)*Power(t1,3)*t2 - 14*Power(s2,4)*Power(t1,3)*t2 + 
         14*Power(s2,5)*Power(t1,3)*t2 - 260*Power(t1,4)*t2 - 
         126*s2*Power(t1,4)*t2 + 36*Power(s2,2)*Power(t1,4)*t2 + 
         58*Power(s2,3)*Power(t1,4)*t2 + 102*Power(t1,5)*t2 + 
         64*s2*Power(t1,5)*t2 + 117*Power(t2,2) + 371*s2*Power(t2,2) + 
         594*Power(s2,2)*Power(t2,2) + 365*Power(s2,3)*Power(t2,2) + 
         122*Power(s2,4)*Power(t2,2) + 2*Power(s2,5)*Power(t2,2) - 
         10*Power(s2,6)*Power(t2,2) - 3*Power(s2,7)*Power(t2,2) - 
         156*t1*Power(t2,2) - 694*s2*t1*Power(t2,2) - 
         1246*Power(s2,2)*t1*Power(t2,2) - 
         901*Power(s2,3)*t1*Power(t2,2) - 
         228*Power(s2,4)*t1*Power(t2,2) + Power(s2,5)*t1*Power(t2,2) + 
         13*Power(s2,6)*t1*Power(t2,2) + 3*Power(s2,7)*t1*Power(t2,2) - 
         219*Power(t1,2)*Power(t2,2) - 108*s2*Power(t1,2)*Power(t2,2) + 
         401*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         594*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         174*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         5*Power(s2,5)*Power(t1,2)*Power(t2,2) - 
         7*Power(s2,6)*Power(t1,2)*Power(t2,2) + 
         398*Power(t1,3)*Power(t2,2) + 729*s2*Power(t1,3)*Power(t2,2) + 
         311*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         66*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         56*Power(s2,4)*Power(t1,3)*Power(t2,2) - 
         132*Power(t1,4)*Power(t2,2) - 266*s2*Power(t1,4)*Power(t2,2) - 
         96*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         12*Power(t1,5)*Power(t2,2) - 48*Power(t2,3) + 
         95*s2*Power(t2,3) + 275*Power(s2,2)*Power(t2,3) + 
         290*Power(s2,3)*Power(t2,3) + 121*Power(s2,4)*Power(t2,3) + 
         33*Power(s2,5)*Power(t2,3) - Power(s2,6)*Power(t2,3) - 
         2*Power(s2,7)*Power(t2,3) + 217*t1*Power(t2,3) + 
         272*s2*t1*Power(t2,3) + 247*Power(s2,2)*t1*Power(t2,3) - 
         198*Power(s2,3)*t1*Power(t2,3) - 
         205*Power(s2,4)*t1*Power(t2,3) - 33*Power(s2,5)*t1*Power(t2,3) + 
         Power(s2,6)*t1*Power(t2,3) - 227*Power(t1,2)*Power(t2,3) - 
         776*s2*Power(t1,2)*Power(t2,3) - 
         776*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         192*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         50*Power(s2,4)*Power(t1,2)*Power(t2,3) + 
         18*Power(s2,5)*Power(t1,2)*Power(t2,3) + 
         44*Power(t1,3)*Power(t2,3) + 303*s2*Power(t1,3)*Power(t2,3) + 
         240*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         64*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         30*Power(t1,4)*Power(t2,3) + 30*s2*Power(t1,4)*Power(t2,3) - 
         44*Power(t2,4) - 114*s2*Power(t2,4) - 
         137*Power(s2,2)*Power(t2,4) - 33*Power(s2,3)*Power(t2,4) + 
         26*Power(s2,4)*Power(t2,4) + 16*Power(s2,5)*Power(t2,4) + 
         2*Power(s2,6)*Power(t2,4) + 39*t1*Power(t2,4) + 
         336*s2*t1*Power(t2,4) + 465*Power(s2,2)*t1*Power(t2,4) + 
         335*Power(s2,3)*t1*Power(t2,4) + 19*Power(s2,4)*t1*Power(t2,4) - 
         14*Power(s2,5)*t1*Power(t2,4) + 4*Power(t1,2)*Power(t2,4) - 
         97*s2*Power(t1,2)*Power(t2,4) - 
         198*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         90*Power(s2,3)*Power(t1,2)*Power(t2,4) - 
         16*Power(s2,4)*Power(t1,2)*Power(t2,4) - 
         23*Power(t1,3)*Power(t2,4) - 67*s2*Power(t1,3)*Power(t2,4) - 
         24*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         Power(t1,4)*Power(t2,4) + 17*Power(t2,5) - 59*s2*Power(t2,5) - 
         95*Power(s2,2)*Power(t2,5) - 104*Power(s2,3)*Power(t2,5) - 
         26*Power(s2,4)*Power(t2,5) + 2*Power(s2,5)*Power(t2,5) - 
         8*t1*Power(t2,5) + s2*t1*Power(t2,5) + 
         46*Power(s2,2)*t1*Power(t2,5) + 29*Power(s2,3)*t1*Power(t2,5) + 
         14*Power(s2,4)*t1*Power(t2,5) + 10*Power(t1,2)*Power(t2,5) + 
         51*s2*Power(t1,2)*Power(t2,5) + 
         42*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
         6*Power(s2,3)*Power(t1,2)*Power(t2,5) + 
         2*s2*Power(t1,3)*Power(t2,5) - 7*Power(t2,6) + 
         3*s2*Power(t2,6) - 4*Power(s2,2)*Power(t2,6) + 
         9*Power(s2,3)*Power(t2,6) - 2*Power(s2,4)*Power(t2,6) - 
         16*s2*t1*Power(t2,6) - 28*Power(s2,2)*t1*Power(t2,6) - 
         5*Power(s2,3)*t1*Power(t2,6) - s2*Power(t1,2)*Power(t2,6) - 
         Power(s2,2)*Power(t1,2)*Power(t2,6) + 2*Power(t2,7) + 
         5*s2*Power(t2,7) + 5*Power(s2,2)*Power(t2,7) - t1*Power(t2,7) + 
         Power(s2,2)*t1*Power(t2,7) - 
         Power(s,7)*(-1 + Power(t1,2) + Power(t1,3) + 
            Power(s2,2)*(-2 + s1 + 7*t1 - 6*t2) + 4*t2 + 5*t1*t2 + 
            3*Power(t1,2)*t2 - 6*Power(t2,2) - 9*t1*Power(t2,2) + 
            5*Power(t2,3) + s2*
             (3 - 6*t1 - 7*Power(t1,2) + s1*(-1 + 7*t1 - 4*t2) + t2 + 
               9*t1*t2 - 2*Power(t2,2)) + 
            s1*(-7*Power(t1,2) - 5*(-1 + t2)*t2 + 4*t1*(-2 + 3*t2))) + 
         Power(s1,6)*(-2*Power(s2,4) + Power(s2,3)*(3 - 11*t1 + 16*t2) + 
            s2*(-2 + 5*Power(t1,3) + 3*t2 + 11*t1*(1 + 2*t2) + 
               2*Power(t1,2)*(-14 + 5*t2)) + 
            Power(s2,2)*(5 - 12*Power(t1,2) - 3*t2 + t1*(-6 + 5*t2)) + 
            t1*(3 + Power(t1,2)*(3 - 5*t2) + 4*t2 + t1*(-13 + 8*t2))) + 
         Power(s1,5)*(1 + 4*Power(s2,5) + 7*Power(t1,4) - 3*t2 - 
            2*Power(t2,2) + Power(s2,4)*(10 - 18*t1 + 20*t2) + 
            t1*(-10 + 8*t2 - 19*Power(t2,2)) + 
            Power(t1,2)*(17 + 49*t2 - 10*Power(t2,2)) + 
            2*Power(t1,3)*(-17 - 6*t2 + 5*Power(t2,2)) + 
            Power(s2,3)*(32 - 36*Power(t1,2) - 28*t2 - 50*Power(t2,2) + 
               t1*(11 + 64*t2)) + 
            Power(s2,2)*(-5 + 10*Power(t1,3) - 41*t2 - 5*Power(t2,2) + 
               Power(t1,2)*(-100 + 49*t2) + 
               t1*(114 + 82*t2 - 11*Power(t2,2))) - 
            s2*(4 + 3*t2 + 20*Power(t2,2) + 3*Power(t1,3)*(4 + 7*t2) + 
               Power(t1,2)*(62 - 115*t2 + 20*Power(t2,2)) + 
               t1*(-83 + 7*t2 + 65*Power(t2,2)))) + 
         Power(s1,4)*(1 + 6*Power(s2,6) + Power(t1,4)*(5 - 22*t2) + 
            6*t2 + 5*Power(t2,2) + 10*Power(t2,3) - 
            2*Power(s2,5)*(2 + 2*t1 + 5*t2) + 
            Power(t1,2)*(157 - 15*t2 - 66*Power(t2,2)) + 
            Power(t1,3)*(-111 + 102*t2 + 18*Power(t2,2) - 
               10*Power(t2,3)) + 
            t1*(-77 + 24*t2 - 62*Power(t2,2) + 35*Power(t2,3)) - 
            Power(s2,4)*(26 + 50*Power(t1,2) + 84*t2 + 62*Power(t2,2) - 
               25*t1*(3 + 4*t2)) + 
            Power(s2,3)*(-25 + 8*Power(t1,3) - 282*t2 + 
               91*Power(t2,2) + 80*Power(t2,3) + 
               2*Power(t1,2)*(-69 + 67*t2) + 
               t1*(297 + 47*t2 - 151*Power(t2,2))) + 
            Power(s2,2)*(-53 - 81*t2 + 110*Power(t2,2) + 
               35*Power(t2,3) - 3*Power(t1,3)*(29 + 12*t2) + 
               Power(t1,2)*(56 + 412*t2 - 77*Power(t2,2)) + 
               t1*(168 - 374*t2 - 296*Power(t2,2) + 15*Power(t2,3))) + 
            s2*(1 + 22*Power(t1,4) - 37*t2 + 35*Power(t2,2) + 
               55*Power(t2,3) + 
               Power(t1,3)*(-220 + 35*t2 + 34*Power(t2,2)) + 
               Power(t1,2)*(350 + 319*t2 - 181*Power(t2,2) + 
                  20*Power(t2,3)) + 
               t1*(-95 - 342*t2 - 98*Power(t2,2) + 100*Power(t2,3)))) + 
         Power(s1,3)*(1 + 2*Power(s2,7) + 12*Power(t1,5) + 62*t2 - 
            41*Power(t2,2) + 10*Power(t2,3) - 20*Power(t2,4) + 
            4*Power(t1,4)*(-38 - 3*t2 + 6*Power(t2,2)) + 
            Power(t1,3)*(247 + 296*t2 - 102*Power(t2,2) - 
               12*Power(t2,3) + 5*Power(t2,4)) + 
            Power(t1,2)*(-116 - 340*t2 - 67*Power(t2,2) + 
               34*Power(t2,3) + 10*Power(t2,4)) - 
            2*t1*(4 - 47*t2 + 2*Power(t2,2) - 54*Power(t2,3) + 
               15*Power(t2,4)) + Power(s2,6)*(7*t1 - 5*(3 + 4*t2)) + 
            Power(s2,4)*(-77 + Power(t1,3) + 42*t2 + 218*Power(t2,2) + 
               88*Power(t2,3) + 2*Power(t1,2)*(-22 + 81*t2) + 
               t1*(74 - 206*t2 - 206*Power(t2,2))) + 
            Power(s2,5)*(-30*Power(t1,2) + 34*t1*(2 + t2) + 
               4*(-6 - 5*t2 + Power(t2,2))) + 
            Power(s2,2)*(-18 + 18*Power(t1,4) + 374*t2 + 
               368*Power(t2,2) - 130*Power(t2,3) - 65*Power(t2,4) + 
               Power(t1,3)*(-346 + 254*t2 + 48*Power(t2,2)) + 
               Power(t1,2)*(931 + 184*t2 - 678*Power(t2,2) + 
                  58*Power(t2,3)) + 
               t1*(-611 - 1162*t2 + 392*Power(t2,2) + 484*Power(t2,3) - 
                  15*Power(t2,4))) - 
            Power(s2,3)*(Power(t1,3)*(139 + 24*t2) + 
               2*Power(t1,2)*(-207 - 259*t2 + 96*Power(t2,2)) + 
               t1*(43 + 1322*t2 + 236*Power(t2,2) - 184*Power(t2,3)) + 
               2*(50 - 83*t2 - 379*Power(t2,2) + 72*Power(t2,3) + 
                  35*Power(t2,4))) - 
            s2*(Power(t1,4)*(64 + 58*t2) + 
               Power(t1,3)*(46 - 646*t2 + 35*Power(t2,2) + 
                  26*Power(t2,3)) + 
               2*Power(t1,2)*
                (-272 + 386*t2 + 318*Power(t2,2) - 67*Power(t2,3) + 
                  5*Power(t2,4)) + 
               2*(-9 - 73*t2 - 97*Power(t2,2) + 35*Power(t2,3) + 
                  40*Power(t2,4)) + 
               t1*(376 + 194*t2 - 527*Power(t2,2) - 242*Power(t2,3) + 
                  85*Power(t2,4)))) + 
         Power(s1,2)*(5 + 3*Power(s2,7)*(-1 + t1 - 2*t2) - 37*t2 - 
            171*Power(t2,2) + 77*Power(t2,3) - 30*Power(t2,4) + 
            20*Power(t2,5) - 2*Power(t1,5)*(11 + 10*t2) + 
            Power(s2,6)*(-5 - 6*Power(t1,2) + t1*(7 - 13*t2) + 29*t2 + 
               24*Power(t2,2)) - 
            2*Power(t1,4)*(73 - 134*t2 - 4*Power(t2,2) + 
               5*Power(t2,3)) - 
            Power(t1,3)*(-577 + 301*t2 + 282*Power(t2,2) - 
               34*Power(t2,3) - 3*Power(t2,4) + Power(t2,5)) - 
            Power(t1,2)*(615 + 98*t2 - 213*Power(t2,2) - 
               121*Power(t2,3) + Power(t2,4) + 8*Power(t2,5)) + 
            t1*(197 + 236*t2 + 82*Power(t2,2) - 32*Power(t2,3) - 
               77*Power(t2,4) + 10*Power(t2,5)) + 
            Power(s2,5)*(17 - Power(t1,3) + 83*t2 + 68*Power(t2,2) + 
               8*Power(t2,3) + Power(t1,2)*(26 + 80*t2) - 
               t1*(34 + 173*t2 + 70*Power(t2,2))) + 
            Power(s2,4)*(125 + 315*t2 + 20*Power(t2,2) - 
               250*Power(t2,3) - 62*Power(t2,4) - 
               Power(t1,3)*(80 + 3*t2) + 
               Power(t1,2)*(225 + 164*t2 - 190*Power(t2,2)) + 
               2*t1*(-129 - 209*t2 + 103*Power(t2,2) + 99*Power(t2,3))) \
+ Power(s2,3)*(282 + 2*Power(t1,4) + 488*t2 - 290*Power(t2,2) - 
               902*Power(t2,3) + 121*Power(t2,4) + 32*Power(t2,5) + 
               Power(t1,3)*(-77 + 327*t2 + 24*Power(t2,2)) + 
               Power(t1,2)*(487 - 944*t2 - 712*Power(t2,2) + 
                  132*Power(t2,3)) + 
               t1*(-702 - 171*t2 + 2088*Power(t2,2) + 316*Power(t2,3) - 
                  121*Power(t2,4))) - 
            Power(s2,2)*(-292 - 230*t2 + 726*Power(t2,2) + 
               568*Power(t2,3) - 65*Power(t2,4) - 59*Power(t2,5) + 
               2*Power(t1,4)*(72 + 17*t2) + 
               Power(t1,3)*(-544 - 920*t2 + 271*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,2)*(147 + 2749*t2 + 734*Power(t2,2) - 
                  562*Power(t2,3) + 22*Power(t2,4)) + 
               t1*(581 - 1671*t2 - 2285*Power(t2,2) + 72*Power(t2,3) + 
                  406*Power(t2,4) - 11*Power(t2,5))) + 
            s2*(71 + 12*Power(t1,5) + 25*t2 - 409*Power(t2,2) - 
               320*Power(t2,3) + 60*Power(t2,4) + 65*Power(t2,5) + 
               2*Power(t1,4)*(-166 + 72*t2 + 25*Power(t2,2)) + 
               Power(t1,3)*(1109 + 546*t2 - 699*Power(t2,2) + 
                  15*Power(t2,3) + 9*Power(t2,4)) + 
               Power(t1,2)*(-1046 - 2142*t2 + 397*Power(t2,2) + 
                  614*Power(t2,3) - 46*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(218 + 1199*t2 + 1009*Power(t2,2) - 359*Power(t2,3) - 
                  233*Power(t2,4) + 38*Power(t2,5)))) - 
         s1*(38 + 137*t2 - 84*Power(t2,2) - 152*Power(t2,3) + 
            60*Power(t2,4) - 25*Power(t2,5) + 10*Power(t2,6) - 
            6*Power(s2,7)*t2*(1 - t1 + t2) - 
            2*Power(t1,5)*(-72 + 13*t2 + 4*Power(t2,2)) - 
            2*Power(t1,3)*(-238 - 475*t2 + 5*Power(t2,2) + 
               60*Power(t2,3)) - 
            Power(t1,4)*(479 + 292*t2 - 146*Power(t2,2) + Power(t2,4)) + 
            Power(t1,2)*(-65 - 759*t2 - 441*Power(t2,2) + 
               34*Power(t2,3) + 66*Power(t2,4) + 3*Power(t2,5) - 
               2*Power(t2,6)) - 
            t1*(114 + 18*t2 - 445*Power(t2,2) - 138*Power(t2,3) + 
               30*Power(t2,4) + 20*Power(t2,5) + Power(t2,6)) + 
            Power(s2,6)*(-6 - 15*t2 + 13*Power(t2,2) + 12*Power(t2,3) - 
               Power(t1,2)*(6 + 13*t2) + t1*(12 + 20*t2 - 5*Power(t2,2))\
) + Power(s2,5)*(-4 - Power(t1,3)*(-13 + t2) + 15*t2 + 92*Power(t2,2) + 
               60*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,2)*(-26 + 27*t2 + 68*Power(t2,2)) - 
               t1*(-17 + 25*t2 + 138*Power(t2,2) + 54*Power(t2,3))) + 
            Power(s2,4)*(-11 + Power(t1,4) + 265*t2 + 359*Power(t2,2) + 
               62*Power(t2,3) - 132*Power(t2,4) - 20*Power(t2,5) - 
               2*Power(t1,3)*(12 + 67*t2 + Power(t2,2)) + 
               Power(t1,2)*(38 + 413*t2 + 170*Power(t2,2) - 
                  94*Power(t2,3)) + 
               t1*(-4 - 520*t2 - 549*Power(t2,2) + 94*Power(t2,3) + 
                  88*Power(t2,4))) + 
            Power(s2,3)*(202 + 658*t2 + 678*Power(t2,2) - 
               182*Power(t2,3) - 498*Power(t2,4) + 52*Power(t2,5) + 
               6*Power(t2,6) + 2*Power(t1,4)*(33 + t2) + 
               Power(t1,3)*(-287 - 186*t2 + 252*Power(t2,2) + 
                  8*Power(t2,3)) + 
               2*Power(t1,2)*
                (245 + 589*t2 - 361*Power(t2,2) - 211*Power(t2,3) + 
                  22*Power(t2,4)) - 
               t1*(471 + 1668*t2 + 412*Power(t2,2) - 1398*Power(t2,3) - 
                  167*Power(t2,4) + 40*Power(t2,5))) + 
            Power(s2,2)*(317 + 855*t2 + 487*Power(t2,2) - 
               542*Power(t2,3) - 381*Power(t2,4) + 5*Power(t2,5) + 
               27*Power(t2,6) - 
               4*Power(t1,4)*(-17 + 54*t2 + 4*Power(t2,2)) - 
               2*Power(t1,3)*
                (326 - 415*t2 - 407*Power(t2,2) + 64*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(t1,2)*(1553 + 201*t2 - 2594*Power(t2,2) - 
                  692*Power(t2,3) + 238*Power(t2,4) - 5*Power(t2,5)) + 
               t1*(-1286 - 1742*t2 + 1307*Power(t2,2) + 
                  1756*Power(t2,3) + 106*Power(t2,4) - 
                  170*Power(t2,5) + 5*Power(t2,6))) + 
            s2*(200 + 437*t2 + 138*Power(t2,2) - 376*Power(t2,3) - 
               226*Power(t2,4) + 23*Power(t2,5) + 28*Power(t2,6) + 
               Power(t1,5)*(58 + 8*t2) + 
               2*Power(t1,4)*
                (-112 - 310*t2 + 55*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,3)*(-154 + 1937*t2 + 803*Power(t2,2) - 
                  340*Power(t2,3) + 5*Power(t2,4) + Power(t2,5)) - 
               Power(t1,2)*(-904 + 1275*t2 + 2374*Power(t2,2) + 
                  122*Power(t2,3) - 286*Power(t2,4) + 7*Power(t2,5)) + 
               t1*(-784 - 423*t2 + 1095*Power(t2,2) + 1056*Power(t2,3) - 
                  90*Power(t2,4) - 101*Power(t2,5) + 7*Power(t2,6)))) + 
         Power(s,6)*(-2 + 6*t1 + 6*Power(t1,2) - 9*Power(t1,3) + 
            5*Power(s2,3)*(-2 + 4*t1 - 3*t2) + 11*t2 - 24*t1*t2 + 
            21*Power(t1,2)*t2 - 5*Power(t1,3)*t2 - 8*Power(t2,2) - 
            36*t1*Power(t2,2) + 24*Power(t2,3) + 15*t1*Power(t2,3) - 
            10*Power(t2,4) + Power(s2,2)*
             (15 - 23*Power(t1,2) + t2 + 8*Power(t2,2) + 
               t1*(-10 + 9*t2)) + 
            s2*(-3 + 5*Power(t1,3) + t2 - 7*Power(t2,2) + 
               21*Power(t2,3) + 2*Power(t1,2)*(7 + 10*t2) + 
               t1*(-13 + 24*t2 - 46*Power(t2,2))) + 
            Power(s1,2)*(5*Power(s2,2) - 21*Power(t1,2) - 
               10*(-1 + t2)*t2 + 5*t1*(-5 + 6*t2) + 
               s2*(21*t1 - 5*(1 + t2))) + 
            s1*(-5 + 5*Power(s2,3) + 7*Power(t1,3) + 
               Power(s2,2)*(-17 + 39*t1 - 27*t2) + 19*t2 - 
               34*Power(t2,2) + 20*Power(t2,3) + 
               Power(t1,2)*(1 + 18*t2) + 
               t1*(-8 + 54*t2 - 45*Power(t2,2)) + 
               s2*(17 - 44*Power(t1,2) + 5*t2 - 16*Power(t2,2) + 
                  t1*(-30 + 41*t2)))) + 
         Power(s,5)*(2 - 28*t1 + 24*Power(t1,2) + 14*Power(t1,3) - 
            7*Power(t1,4) + 3*t2 + 18*t1*t2 - 44*Power(t1,2)*t2 - 
            9*Power(t1,3)*t2 + 35*Power(t2,2) - 19*t1*Power(t2,2) + 
            54*Power(t1,2)*Power(t2,2) - 10*Power(t1,3)*Power(t2,2) - 
            23*Power(t2,3) - 88*t1*Power(t2,3) + 
            10*Power(t1,2)*Power(t2,3) + 50*Power(t2,4) + 
            10*t1*Power(t2,4) - 10*Power(t2,5) - 
            5*Power(s1,3)*(2*Power(s2,2) - 7*Power(t1,2) + 
               s2*(-2 + 7*t1) + 8*t1*(-1 + t2) - 2*(-1 + t2)*t2) + 
            Power(s2,4)*(-30*t1 + 20*(1 + t2)) + 
            Power(s2,3)*(-34 + 42*Power(t1,2) - 23*t2 - 
               35*Power(t2,2) + t1*(2 + 19*t2)) + 
            Power(s1,2)*(6 - 23*Power(s2,3) - 21*Power(t1,3) + 
               Power(t1,2)*(12 - 45*t2) - 36*t2 + 70*Power(t2,2) - 
               30*Power(t2,3) + Power(s2,2)*(50 - 89*t1 + 44*t2) + 
               s2*(-33 + 117*Power(t1,2) + t1*(56 - 70*t2) - 20*t2 + 
                  44*Power(t2,2)) + 2*t1*(23 - 86*t2 + 45*Power(t2,2))) \
+ Power(s2,2)*(11 - 10*Power(t1,3) + 21*t2 - 29*Power(t2,2) - 
               26*Power(t2,3) - 4*Power(t1,2)*(13 + 18*t2) + 
               t1*(62 + t2 + 93*Power(t2,2))) + 
            s2*(1 - 50*t2 - 52*Power(t2,2) - 63*Power(t2,3) + 
               44*Power(t2,4) + 7*Power(t1,3)*(7 + 3*t2) + 
               Power(t1,2)*(-80 - 68*t2 + 7*Power(t2,2)) + 
               t1*(14 + 120*t2 + 163*Power(t2,2) - 72*Power(t2,3))) + 
            s1*(15 - 10*Power(s2,4) - 61*t2 + 59*Power(t2,2) - 
               110*Power(t2,3) + 30*Power(t2,4) + 
               6*Power(t1,3)*(7 + 5*t2) + 
               Power(s2,3)*(69 - 91*t1 + 74*t2) - 
               Power(t1,2)*(24 + 89*t2) + 
               Power(s2,2)*(-82 + 127*Power(t1,2) + t1*(5 - 38*t2) + 
                  2*t2 - 8*Power(t2,2)) + 
               t1*(-15 + 26*t2 + 220*Power(t2,2) - 60*Power(t2,3)) + 
               s2*(8 - 30*Power(t1,3) + 36*t2 + 73*Power(t2,2) - 
                  88*Power(t2,3) - 2*Power(t1,2)*(17 + 55*t2) + 
                  3*t1*(21 - 66*t2 + 59*Power(t2,2))))) + 
         Power(s,4)*(-5 + 88*t1 - 130*Power(t1,2) + 95*Power(t1,3) - 
            19*Power(t1,4) + 5*Power(s2,5)*(-4 + 5*t1 - 3*t2) - 119*t2 + 
            134*t1*t2 - 111*Power(t1,2)*t2 + 90*Power(t1,3)*t2 - 
            22*Power(t1,4)*t2 - 97*Power(t2,2) + 152*t1*Power(t2,2) - 
            230*Power(t1,2)*Power(t2,2) + 30*Power(t1,3)*Power(t2,2) + 
            29*Power(t2,3) + 121*t1*Power(t2,3) + 
            22*Power(t1,2)*Power(t2,3) - 10*Power(t1,3)*Power(t2,3) - 
            72*Power(t2,4) - 81*t1*Power(t2,4) + 
            15*Power(t1,2)*Power(t2,4) + 51*Power(t2,5) - 
            5*Power(t2,6) + Power(s2,4)*
             (42 - 43*Power(t1,2) + t1*(9 - 56*t2) + 67*t2 + 
               55*Power(t2,2)) + 
            5*Power(s1,4)*(2*Power(s2,2) - 7*t1 - 7*Power(t1,2) + t2 + 
               6*t1*t2 - Power(t2,2) + s2*(-2 + 7*t1 + t2)) + 
            Power(s2,3)*(-16 + 8*Power(t1,3) - 36*t2 + 61*Power(t2,2) - 
               6*Power(t2,3) + Power(t1,2)*(99 + 142*t2) - 
               t1*(138 + 132*t2 + 89*Power(t2,2))) - 
            Power(s2,2)*(-22 - 94*t2 - 110*Power(t2,2) + 
               11*Power(t2,3) + 68*Power(t2,4) + 
               3*Power(t1,3)*(35 + 12*t2) + 
               Power(t1,2)*(-230 - 66*t2 + 71*Power(t2,2)) + 
               t1*(68 + 170*t2 + 145*Power(t2,2) - 155*Power(t2,3))) + 
            s2*(-12 + 22*Power(t1,4) + 108*t2 - 26*Power(t2,2) - 
               123*Power(t2,3) - 159*Power(t2,4) + 41*Power(t2,5) + 
               Power(t1,3)*(-56 + 89*t2 + 34*Power(t2,2)) - 
               Power(t1,2)*(46 + 51*t2 + 218*Power(t2,2) + 
                  32*Power(t2,3)) + 
               t1*(18 - 34*t2 + 245*Power(t2,2) + 381*Power(t2,3) - 
                  43*Power(t2,4))) + 
            Power(s1,3)*(6 + 40*Power(s2,3) + 35*Power(t1,3) + 
               Power(s2,2)*(-67 + 105*t1 - 26*t2) + 30*t2 - 
               66*Power(t2,2) + 20*Power(t2,3) + 
               20*Power(t1,2)*(-2 + 3*t2) + 
               t1*(-92 + 248*t2 - 90*Power(t2,2)) + 
               s2*(14 - 170*Power(t1,2) + 46*t2 - 56*Power(t2,2) + 
                  t1*(-47 + 50*t2))) + 
            Power(s1,2)*(42*Power(s2,4) + 
               Power(s2,3)*(-150 + 153*t1 - 130*t2) - 
               75*Power(t1,3)*(1 + t2) + Power(t1,2)*(47 + 154*t2) + 
               t1*(-29 + 156*t2 - 472*Power(t2,2) + 90*Power(t2,3)) - 
               2*(30 - 49*t2 + 66*Power(t2,2) - 84*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(s2,2)*(115 - 290*Power(t1,2) - 9*t2 - 
                  46*Power(t2,2) + t1*(96 + 67*t2)) + 
               s2*(78 + 75*Power(t1,3) - 81*t2 - 221*Power(t2,2) + 
                  138*Power(t2,3) + Power(t1,2)*(-32 + 250*t2) + 
                  t1*(-127 + 504*t2 - 248*Power(t2,2)))) + 
            s1*(13 + 10*Power(s2,5) + 35*Power(t1,4) + 
               2*Power(s2,4)*(-62 + 57*t1 - 53*t2) + 155*t2 - 
               133*Power(t2,2) + 174*Power(t2,3) - 158*Power(t2,4) + 
               20*Power(t2,5) + 
               Power(t1,3)*(-90 + 24*t2 + 50*Power(t2,2)) + 
               Power(s2,3)*(156 - 204*Power(t1,2) + t1*(122 - 28*t2) + 
                  52*t2 + 96*Power(t2,2)) - 
               Power(t1,2)*(35 - 241*t2 + 136*Power(t2,2) + 
                  40*Power(t2,3)) + 
               t1*(60 - 142*t2 - 185*Power(t2,2) + 340*Power(t2,3) - 
                  30*Power(t2,4)) + 
               Power(s2,2)*(-26 + 50*Power(t1,3) - 139*t2 + 
                  87*Power(t2,2) + 130*Power(t2,3) + 
                  Power(t1,2)*(128 + 337*t2) + 
                  t1*(-230 + 32*t2 - 327*Power(t2,2))) + 
               s2*(-58 + t2 + 190*Power(t2,2) + 344*Power(t2,3) - 
                  128*Power(t2,4) - Power(t1,3)*(208 + 105*t2) + 
                  Power(t1,2)*(338 + 317*t2 - 48*Power(t2,2)) + 
                  t1*(-59 - 293*t2 - 838*Power(t2,2) + 206*Power(t2,3))))\
) - Power(s,3)*(-8 - 14*t1 - 41*Power(t1,2) + 103*Power(t1,3) - 
            92*Power(t1,4) + 12*Power(t1,5) + 
            Power(s2,6)*(-10 + 11*t1 - 6*t2) - 182*t2 + 129*t1*t2 - 
            35*Power(t1,2)*t2 + 66*Power(t1,3)*t2 + 64*Power(t2,2) - 
            47*t1*Power(t2,2) - 25*Power(t1,2)*Power(t2,2) - 
            74*Power(t1,3)*Power(t2,2) + 24*Power(t1,4)*Power(t2,2) + 
            141*Power(t2,3) - 100*t1*Power(t2,3) + 
            258*Power(t1,2)*Power(t2,3) - 54*Power(t1,3)*Power(t2,3) - 
            44*Power(t2,4) - 184*t1*Power(t2,4) + 
            33*Power(t1,2)*Power(t2,4) + 5*Power(t1,3)*Power(t2,4) + 
            88*Power(t2,5) + 21*t1*Power(t2,5) - 
            9*Power(t1,2)*Power(t2,5) - 24*Power(t2,6) + 
            3*t1*Power(t2,6) + Power(t2,7) + 
            Power(s2,5)*(27 - 23*Power(t1,2) + t1*(8 - 57*t2) + 79*t2 + 
               44*Power(t2,2)) + 
            Power(s1,5)*(5*Power(s2,2) - 21*Power(t1,2) - 
               (-1 + t2)*t2 + 4*t1*(-4 + 3*t2) + s2*(-5 + 21*t1 + 4*t2)) \
+ Power(s2,4)*(3 + Power(t1,3) - 36*t2 + 22*Power(t2,2) - 
               41*Power(t2,3) + Power(t1,2)*(98 + 145*t2) - 
               t1*(156 + 215*t2 + 29*Power(t2,2))) + 
            Power(s1,4)*(15 + 30*Power(s2,3) + 35*Power(t1,3) + 8*t2 - 
               28*Power(t2,2) + 5*Power(t2,3) + 
               Power(s2,2)*(-40 + 65*t1 + 6*t2) + 
               5*Power(t1,2)*(-11 + 9*t2) + 
               t1*(-80 + 177*t2 - 45*Power(t2,2)) - 
               s2*(27 + 145*Power(t1,2) + t1*(18 - 5*t2) - 53*t2 + 
                  34*Power(t2,2))) - 
            Power(s2,3)*(-20 - 58*t2 + 56*Power(t2,2) + 
               164*Power(t2,3) + 42*Power(t2,4) + 
               Power(t1,3)*(119 + 24*t2) + 
               Power(t1,2)*(-301 + 31*t2 + 180*Power(t2,2)) - 
               2*t1*(-41 + 21*t2 + 96*Power(t2,2) + 93*Power(t2,3))) + 
            Power(s2,2)*(23 + 18*Power(t1,4) + 52*t2 - 
               172*Power(t2,2) - 370*Power(t2,3) - 89*Power(t2,4) + 
               58*Power(t2,5) + 
               8*Power(t1,3)*(-1 + 31*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(-177 - 688*t2 - 387*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(-27 + 416*t2 + 916*Power(t2,2) + 440*Power(t2,3) - 
                  103*Power(t2,4))) - 
            s2*(Power(t1,4)*(68 + 58*t2) + 
               Power(t1,3)*(-402 - 348*t2 + 7*Power(t2,2) + 
                  26*Power(t2,3)) + 
               Power(t1,2)*(541 + 636*t2 + 489*Power(t2,2) - 
                  155*Power(t2,3) - 43*Power(t2,4)) - 
               t1*(397 + 481*t2 + 556*Power(t2,2) + 166*Power(t2,3) - 
                  347*Power(t2,4) + Power(t2,5)) + 
               2*(29 + 48*t2 + 205*Power(t2,2) + 99*Power(t2,3) + 
                  Power(t2,4) - 81*Power(t2,5) + 9*Power(t2,6))) + 
            Power(s1,2)*(-90 + 38*Power(s2,5) + 70*Power(t1,4) + 
               330*t2 - 149*Power(t2,2) + 288*Power(t2,3) - 
               148*Power(t2,4) + 10*Power(t2,5) + 
               2*Power(t1,3)*(-110 + 3*t2 + 50*Power(t2,2)) + 
               Power(t1,2)*(48 + 396*t2 - 94*Power(t2,2) - 
                  60*Power(t2,3)) + 
               t1*(70 - 98*t2 - 648*Power(t2,2) + 466*Power(t2,3) - 
                  30*Power(t2,4)) + 
               Power(s2,4)*(144*t1 - 31*(7 + 6*t2)) + 
               Power(s2,3)*(119 - 396*Power(t1,2) - 73*t2 + 
                  28*Power(t2,2) + t1*(429 + 94*t2)) + 
               Power(s2,2)*(125 + 100*Power(t1,3) - 286*t2 + 
                  59*Power(t2,2) + 232*Power(t2,3) + 
                  4*Power(t1,2)*(-7 + 157*t2) - 
                  2*t1*(61 - 49*t2 + 217*Power(t2,2))) - 
               s2*(66 + 306*t2 - 55*Power(t2,2) - 605*Power(t2,3) + 
                  124*Power(t2,4) + 6*Power(t1,3)*(57 + 35*t2) + 
                  2*Power(t1,2)*(-284 - 329*t2 + 61*Power(t2,2)) + 
                  t1*(240 + 100*t2 + 1465*Power(t2,2) - 186*Power(t2,3))\
)) + Power(s1,3)*(64*Power(s2,4) + 
               Power(s2,3)*(-125 + 102*t1 - 80*t2) - 
               2*Power(s2,2)*
                (-12 + 175*Power(t1,2) + 7*t2 + 53*Power(t2,2) - 
                  2*t1*(53 + 17*t2)) - 
               2*(21 - 15*t2 + 56*Power(t2,2) - 51*Power(t2,3) + 
                  5*Power(t2,4) + 10*Power(t1,3)*(3 + 5*t2) - 
                  Power(t1,2)*(22 + 73*t2) + 
                  t1*(66 - 176*t2 + 228*Power(t2,2) - 30*Power(t2,3))) \
+ s2*(100*Power(t1,3) + 4*Power(t1,2)*(-47 + 75*t2) - 
                  2*t1*(39 - 293*t2 + 71*Power(t2,2)) + 
                  3*(52 + 4*t2 - 97*Power(t2,2) + 32*Power(t2,3)))) + 
            s1*(100 + 5*Power(s2,6) + 
               Power(s2,5)*(-113 + 81*t1 - 84*t2) - 11*t2 - 
               429*Power(t2,2) + 148*Power(t2,3) - 272*Power(t2,4) + 
               97*Power(t2,5) - 5*Power(t2,6) - 
               4*Power(t1,4)*(13 + 22*t2) + 
               Power(s2,4)*(158 - 179*Power(t1,2) + t1*(190 - 96*t2) + 
                  167*t2 + 163*Power(t2,2)) + 
               Power(t1,3)*(218 + 292*t2 + 108*Power(t2,2) - 
                  40*Power(t2,3)) + 
               t1*(204 + 90*t2 + 330*Power(t2,2) + 560*Power(t2,3) - 
                  192*Power(t2,4)) + 
               Power(t1,2)*(-395 - 116*t2 - 698*Power(t2,2) - 
                  30*Power(t2,3) + 45*Power(t2,4)) + 
               Power(s2,3)*(-44 + 32*Power(t1,3) + 16*t2 + 
                  362*Power(t2,2) + 64*Power(t2,3) + 
                  Power(t1,2)*(213 + 560*t2) - 
                  2*t1*(209 + 316*t2 + 191*Power(t2,2))) + 
               Power(s2,2)*(31 + 98*t2 + 632*Power(t2,2) + 
                  84*Power(t2,3) - 195*Power(t2,4) - 
                  6*Power(t1,3)*(67 + 24*t2) + 
                  Power(t1,2)*(1006 + 490*t2 - 290*Power(t2,2)) + 
                  2*t1*(-205 - 503*t2 - 375*Power(t2,2) + 
                     202*Power(t2,3))) + 
               s2*(-98 + 88*Power(t1,4) + 446*t2 + 348*Power(t2,2) - 
                  38*Power(t2,3) - 524*Power(t2,4) + 76*Power(t2,5) + 
                  2*Power(t1,3)*(-162 + 151*t2 + 68*Power(t2,2)) + 
                  Power(t1,2)*
                   (250 + 60*t2 - 625*Power(t2,2) - 76*Power(t2,3)) + 
                  t1*(-141 - 324*t2 + 12*Power(t2,2) + 
                     1244*Power(t2,3) - 71*Power(t2,4))))) + 
         Power(s,2)*(25 - 384*t1 + 716*Power(t1,2) - 477*Power(t1,3) + 
            142*Power(t1,4) - 6*Power(t1,5) + 
            Power(s2,7)*(-2 + 2*t1 - t2) + 74*t2 - 474*t1*t2 + 
            671*Power(t1,2)*t2 - 433*Power(t1,3)*t2 + 
            170*Power(t1,4)*t2 - 20*Power(t1,5)*t2 + 398*Power(t2,2) - 
            600*t1*Power(t2,2) + 413*Power(t1,2)*Power(t2,2) - 
            324*Power(t1,3)*Power(t2,2) + 36*Power(t1,4)*Power(t2,2) + 
            156*Power(t2,3) - 195*t1*Power(t2,3) + 
            345*Power(t1,2)*Power(t2,3) - 26*Power(t1,3)*Power(t2,3) - 
            10*Power(t1,4)*Power(t2,3) - 75*Power(t2,4) - 
            128*t1*Power(t2,4) - 72*Power(t1,2)*Power(t2,4) + 
            27*Power(t1,3)*Power(t2,4) + 74*Power(t2,5) + 
            68*t1*Power(t2,5) - 27*Power(t1,2)*Power(t2,5) - 
            Power(t1,3)*Power(t2,5) - 40*Power(t2,6) + 
            6*t1*Power(t2,6) + 2*Power(t1,2)*Power(t2,6) + 
            4*Power(t2,7) - t1*Power(t2,7) + 
            Power(s2,6)*(7 - 5*Power(t1,2) + t1*(2 - 27*t2) + 43*t2 + 
               18*Power(t2,2)) + 
            Power(s1,6)*(Power(s2,2) + s2*(-1 + 7*t1 + t2) + 
               t1*(-3 - 7*t1 + 2*t2)) - 
            Power(s2,5)*(-19 + Power(t1,3) + 29*t2 + 27*Power(t2,2) + 
               37*Power(t2,3) - Power(t1,2)*(43 + 70*t2) + 
               t1*(81 + 128*t2 - 16*Power(t2,2))) - 
            Power(s2,4)*(28 + 39*t2 + 196*Power(t2,2) + 
               152*Power(t2,3) + 4*Power(t2,4) + 
               3*Power(t1,3)*(22 + t2) + 
               Power(t1,2)*(-178 + 87*t2 + 174*Power(t2,2)) - 
               t1*(-28 + 250*t2 + 334*Power(t2,2) + 117*Power(t2,3))) + 
            Power(s2,3)*(103 + 2*Power(t1,4) - 147*t2 - 
               332*Power(t2,2) - 230*Power(t2,3) + 123*Power(t2,4) + 
               37*Power(t2,5) + 
               3*Power(t1,3)*(25 + 95*t2 + 8*Power(t2,2)) + 
               2*Power(t1,2)*
                (-148 - 556*t2 - 143*Power(t2,2) + 54*Power(t2,3)) + 
               2*t1*(6 + 420*t2 + 555*Power(t2,2) + 7*Power(t2,3) - 
                  67*Power(t2,4))) + 
            Power(s2,2)*(-239 - 261*t2 - 394*Power(t2,2) + 
               38*Power(t2,3) + 311*Power(t2,4) + 109*Power(t2,5) - 
               20*Power(t2,6) - 2*Power(t1,4)*(58 + 17*t2) + 
               Power(t1,3)*(640 + 420*t2 - 213*Power(t2,2) - 
                  28*Power(t2,3)) + 
               Power(t1,2)*(-808 - 1025*t2 + 116*Power(t2,2) + 
                  422*Power(t2,3) + 15*Power(t2,4)) + 
               t1*(639 + 1067*t2 + 100*Power(t2,2) - 696*Power(t2,3) - 
                  427*Power(t2,4) + 27*Power(t2,5))) + 
            s2*(17 + 12*Power(t1,5) - 159*t2 - 86*Power(t2,2) + 
               387*Power(t2,3) + 307*Power(t2,4) + 117*Power(t2,5) - 
               70*Power(t2,6) + 3*Power(t2,7) + 
               2*Power(t1,4)*(-81 + 44*t2 + 25*Power(t2,2)) + 
               Power(t1,3)*(365 - 444*t2 - 403*Power(t2,2) - 
                  55*Power(t2,3) + 9*Power(t2,4)) + 
               Power(t1,2)*(-620 + 578*t2 + 1158*Power(t2,2) + 
                  634*Power(t2,3) + Power(t2,4) - 20*Power(t2,5)) + 
               t1*(320 - 133*t2 - 666*Power(t2,2) - 910*Power(t2,3) - 
                  406*Power(t2,4) + 119*Power(t2,5) + 8*Power(t2,6))) + 
            Power(s1,5)*(7 + 5*Power(s2,3) + 21*Power(t1,3) - t2 - 
               4*Power(t2,2) + 3*Power(t1,2)*(-13 + 6*t2) + 
               Power(s2,2)*(-5 + 17*t1 + 13*t2) + 
               t1*(-28 + 58*t2 - 9*Power(t2,2)) - 
               s2*(31 + 72*Power(t1,2) - 29*t2 + 8*Power(t2,2) + 
                  t1*(8 + 11*t2))) + 
            Power(s1,4)*(22 + 40*Power(s2,4) - 23*t2 + 
               89*Power(t1,2)*t2 - 36*Power(t2,2) + 20*Power(t2,3) - 
               15*Power(t1,3)*(1 + 5*t2) + 
               Power(s2,3)*(-23 - 2*t1 + 25*t2) + 
               t1*(-128 + 256*t2 - 196*Power(t2,2) + 15*Power(t2,3)) - 
               Power(s2,2)*(35 + 235*Power(t1,2) + 17*t2 + 
                  82*Power(t2,2) - t1*(160 + 47*t2)) + 
               s2*(61 + 75*Power(t1,3) + 113*t2 - 176*Power(t2,2) + 
                  25*Power(t2,3) + Power(t1,2)*(-242 + 200*t2) + 
                  t1*(49 + 354*t2 - 18*Power(t2,2)))) + 
            Power(s1,3)*(-108 + 50*Power(s2,5) + 70*Power(t1,4) + 
               6*Power(s2,4)*(-21 + 8*t1 - 19*t2) + 108*t2 - 
               47*Power(t2,2) + 154*Power(t2,3) - 40*Power(t2,4) + 
               4*Power(t1,3)*(-65 - 9*t2 + 25*Power(t2,2)) + 
               Power(t1,2)*(186 + 294*t2 - 6*Power(t2,2) - 
                  40*Power(t2,3)) - 
               2*t1*(29 - 106*t2 + 334*Power(t2,2) - 132*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,2)*(125 + 100*Power(t1,3) - 280*t2 - 
                  28*Power(t2,2) + 178*Power(t2,3) + 
                  Power(t1,2)*(-332 + 582*t2) + 
                  t1*(352 + 178*t2 - 270*Power(t2,2))) - 
               Power(s2,3)*(384*Power(t1,2) - 5*t1*(99 + 52*t2) + 
                  2*(8 + 125*t2 + 71*Power(t2,2))) - 
               s2*(-90 + 336*t2 + 270*Power(t2,2) - 434*Power(t2,3) + 
                  40*Power(t2,4) + Power(t1,3)*(268 + 210*t2) + 
                  4*Power(t1,2)*(-95 - 188*t2 + 37*Power(t2,2)) + 
                  t1*(274 - 88*t2 + 1133*Power(t2,2) - 42*Power(t2,3)))) \
+ Power(s1,2)*(-28 + 17*Power(s2,6) + 
               Power(s2,5)*(-162 + 83*t1 - 139*t2) + 444*t2 - 
               357*Power(t2,2) + 209*Power(t2,3) - 236*Power(t2,4) + 
               40*Power(t2,5) - 6*Power(t1,4)*(7 + 22*t2) + 
               Power(t1,3)*(136 + 416*t2 + 144*Power(t2,2) - 
                  60*Power(t2,3)) + 
               t1*(249 - 196*t2 - 168*Power(t2,2) + 748*Power(t2,3) - 
                  151*Power(t2,4)) + 
               3*Power(t1,2)*
                (-121 + 22*t2 - 220*Power(t2,2) - 42*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(s2,4)*(40 - 279*Power(t1,2) - 4*t2 + 
                  104*Power(t2,2) + t1*(452 + 76*t2)) + 
               Power(s2,3)*(-12 + 48*Power(t1,3) - 80*t2 + 
                  692*Power(t2,2) + 226*Power(t2,3) + 
                  9*Power(t1,2)*(-1 + 92*t2) - 
                  t1*(13 + 891*t2 + 648*Power(t2,2))) + 
               Power(s2,2)*(10 + 45*t2 + 976*Power(t2,2) + 
                  236*Power(t2,3) - 187*Power(t2,4) - 
                  72*Power(t1,3)*(8 + 3*t2) + 
                  Power(t1,2)*(1474 + 1194*t2 - 444*Power(t2,2)) + 
                  t1*(-736 - 1944*t2 - 1263*Power(t2,2) + 
                     358*Power(t2,3))) + 
               s2*(-92 + 132*Power(t1,4) + 233*t2 + 796*Power(t2,2) + 
                  442*Power(t2,3) - 521*Power(t2,4) + 35*Power(t2,5) + 
                  4*Power(t1,3)*(-175 + 93*t2 + 51*Power(t2,2)) + 
                  Power(t1,2)*
                   (1040 + 336*t2 - 777*Power(t2,2) - 36*Power(t2,3)) - 
                  t1*(597 + 666*t2 + 729*Power(t2,2) - 
                     1355*Power(t2,3) + 13*Power(t2,4)))) + 
            s1*(82 + Power(s2,7) + 36*Power(t1,5) + 
               Power(s2,6)*(-51 + 31*t1 - 35*t2) - 427*t2 - 
               492*Power(t2,2) + 302*Power(t2,3) - 220*Power(t2,4) + 
               159*Power(t2,5) - 20*Power(t2,6) + 
               4*Power(t1,4)*(-68 - 3*t2 + 18*Power(t2,2)) + 
               Power(s2,5)*(91 - 76*Power(t1,2) + t1*(96 - 95*t2) + 
                  181*t2 + 126*Power(t2,2)) + 
               Power(t1,3)*(589 + 312*t2 - 130*Power(t2,2) - 
                  120*Power(t2,3) + 15*Power(t2,4)) - 
               Power(t1,2)*(695 + 278*t2 + 597*Power(t2,2) - 
                  438*Power(t2,3) - 109*Power(t2,4) + 18*Power(t2,5)) + 
               t1*(252 + 534*t2 + 449*Power(t2,2) + 212*Power(t2,3) - 
                  376*Power(t2,4) + 22*Power(t2,5) + 3*Power(t2,6)) + 
               Power(s2,4)*(-5 + 3*Power(t1,3) + 197*t2 + 
                  282*Power(t2,2) - 26*Power(t2,3) + 
                  Power(t1,2)*(183 + 452*t2) - 
                  t1*(378 + 806*t2 + 241*Power(t2,2))) + 
               Power(s2,3)*(145 + 364*t2 + 326*Power(t2,2) - 
                  542*Power(t2,3) - 151*Power(t2,4) - 
                  Power(t1,3)*(377 + 72*t2) + 
                  Power(t1,2)*(1168 + 354*t2 - 552*Power(t2,2)) + 
                  2*t1*(-329 - 608*t2 + 191*Power(t2,2) + 
                     262*Power(t2,3))) + 
               Power(s2,2)*(125 + 54*Power(t1,4) + 313*t2 - 
                  208*Power(t2,2) - 972*Power(t2,3) - 295*Power(t2,4) + 
                  97*Power(t2,5) + 
                  2*Power(t1,3)*(-137 + 375*t2 + 72*Power(t2,2)) + 
                  Power(t1,2)*
                   (491 - 1544*t2 - 1284*Power(t2,2) + 82*Power(t2,3)) \
+ t1*(-719 + 740*t2 + 2288*Power(t2,2) + 1352*Power(t2,3) - 
                     179*Power(t2,4))) - 
               s2*(119 - 159*t2 + 710*Power(t2,2) + 828*Power(t2,3) + 
                  371*Power(t2,4) - 305*Power(t2,5) + 16*Power(t2,6) + 
                  2*Power(t1,4)*(100 + 87*t2) + 
                  Power(t1,3)*
                   (-922 - 1118*t2 + 49*Power(t2,2) + 78*Power(t2,3)) + 
                  Power(t1,2)*
                   (1274 + 2338*t2 + 1350*Power(t2,2) - 
                     266*Power(t2,3) - 76*Power(t2,4)) + 
                  t1*(-829 - 1379*t2 - 1850*Power(t2,2) - 
                     998*Power(t2,3) + 687*Power(t2,4) + 15*Power(t2,5)))\
)) + s*(-59 + 540*t1 - 1258*Power(t1,2) + 1148*Power(t1,3) - 
            459*Power(t1,4) + 88*Power(t1,5) + 
            Power(s1,7)*t1*(-s2 + t1) - 273*t2 + 980*t1*t2 - 
            1076*Power(t1,2)*t2 + 445*Power(t1,3)*t2 - 
            94*Power(t1,4)*t2 - 10*Power(t1,5)*t2 - 386*Power(t2,2) + 
            591*t1*Power(t2,2) - 115*Power(t1,2)*Power(t2,2) - 
            50*Power(t1,3)*Power(t2,2) + 68*Power(t1,4)*Power(t2,2) - 
            8*Power(t1,5)*Power(t2,2) + 9*Power(t2,3) - 
            154*t1*Power(t2,3) + 68*Power(t1,2)*Power(t2,3) - 
            106*Power(t1,3)*Power(t2,3) + 16*Power(t1,4)*Power(t2,3) + 
            105*Power(t2,4) - 38*t1*Power(t2,4) + 
            115*Power(t1,2)*Power(t2,4) - 24*Power(t1,3)*Power(t2,4) - 
            Power(t1,4)*Power(t2,4) - 43*Power(t2,5) - 
            48*t1*Power(t2,5) + 6*Power(t1,2)*Power(t2,5) + 
            3*Power(t1,3)*Power(t2,5) + 28*Power(t2,6) - t1*Power(t2,6) - 
            4*Power(t1,2)*Power(t2,6) - 5*Power(t2,7) + 
            2*t1*Power(t2,7) + Power(s2,7)*t2*(5*t1 - 3*(3 + t2)) + 
            Power(s2,6)*(-9 + 11*t2 + 21*Power(t2,2) + 14*Power(t2,3) - 
               Power(t1,2)*(5 + 12*t2) + t1*(14 + 25*t2 - 15*Power(t2,2))\
) + Power(s2,5)*(27 - Power(t1,3)*(-12 + t2) + 48*t2 + 100*Power(t2,2) + 
               39*Power(t2,3) - 6*Power(t2,4) + 
               Power(t1,2)*(-33 + 43*t2 + 65*Power(t2,2)) - 
               t1*(6 + 146*t2 + 137*Power(t2,2) + 30*Power(t2,3))) + 
            Power(s2,4)*(-31 + Power(t1,4) + 133*t2 + 121*Power(t2,2) - 
               47*Power(t2,3) - 111*Power(t2,4) - 12*Power(t2,5) - 
               Power(t1,3)*(30 + 123*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(154 + 480*t2 + 58*Power(t2,2) - 
                  88*Power(t2,3)) + 
               t1*(-94 - 406*t2 - 347*Power(t2,2) + 152*Power(t2,3) + 
                  76*Power(t2,4))) + 
            Power(s2,3)*(127 + 228*t2 - 5*Power(t2,2) - 
               394*Power(t2,3) - 307*Power(t2,4) + 42*Power(t2,5) + 
               9*Power(t2,6) + 2*Power(t1,4)*(29 + t2) + 
               Power(t1,3)*(-353 - 76*t2 + 230*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(379 + 447*t2 - 748*Power(t2,2) - 
                  308*Power(t2,3) + 34*Power(t2,4)) + 
               t1*(-211 - 669*t2 + 588*Power(t2,2) + 1094*Power(t2,3) + 
                  101*Power(t2,4) - 41*Power(t2,5))) - 
            Power(s2,2)*(-135 + 31*t2 + 440*Power(t2,2) + 
               740*Power(t2,3) + 261*Power(t2,4) - 69*Power(t2,5) - 
               42*Power(t2,6) + 2*Power(t2,7) + 
               2*Power(t1,4)*(-23 + 92*t2 + 8*Power(t2,2)) + 
               2*Power(t1,3)*
                (57 - 458*t2 - 270*Power(t2,2) + 47*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(t1,2)*(-597 + 1410*t2 + 2133*Power(t2,2) + 
                  372*Power(t2,3) - 195*Power(t2,4) - 4*Power(t2,5)) + 
               t1*(664 - 693*t2 - 2157*Power(t2,2) - 1188*Power(t2,3) + 
                  78*Power(t2,4) + 171*Power(t2,5) - 3*Power(t2,6))) + 
            s2*(-174 - 335*t2 - 603*Power(t2,2) - 528*Power(t2,3) - 
               72*Power(t2,4) + 81*Power(t2,5) + 57*Power(t2,6) - 
               10*Power(t2,7) + Power(t1,5)*(42 + 8*t2) + 
               2*Power(t1,4)*
                (-163 - 196*t2 + 35*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,3)*(581 + 1755*t2 + 337*Power(t2,2) - 
                  218*Power(t2,3) - 20*Power(t2,4) + Power(t2,5)) + 
               Power(t1,2)*(-786 - 2520*t2 - 1582*Power(t2,2) + 
                  155*Power(t2,3) + 265*Power(t2,4) + 19*Power(t2,5) - 
                  3*Power(t2,6)) + 
               t1*(663 + 1548*t2 + 1644*Power(t2,2) + 642*Power(t2,3) - 
                  215*Power(t2,4) - 164*Power(t2,5) + 8*Power(t2,6) + 
                  2*Power(t2,7))) + 
            Power(s1,6)*(5*Power(s2,3) + 
               t1*(2 - 7*Power(t1,2) + t1*(14 - 3*t2) - 6*t2) + 
               Power(s2,2)*(t1 - 4*(1 + t2)) + 
               s2*(9 + 19*Power(t1,2) - 6*t2 + 4*t1*(2 + t2))) + 
            Power(s1,5)*(-9 - 6*Power(s2,4) + 
               Power(s2,3)*(-14 + 33*t1 - 50*t2) + 
               Power(t1,2)*(28 - 37*t2) + 5*t2 + 5*Power(t2,2) + 
               6*Power(t1,3)*(-1 + 5*t2) + 
               t1*(31 - 66*t2 + 28*Power(t2,2)) + 
               Power(s2,2)*(6 + 83*Power(t1,2) + 12*t2 + 
                  22*Power(t2,2) - 11*t1*(3 + 2*t2)) - 
               s2*(-14 + 30*Power(t1,3) + 60*t2 - 40*Power(t2,2) + 
                  2*Power(t1,2)*(-67 + 35*t2) + 
                  t1*(61 + 120*t2 + 7*Power(t2,2)))) + 
            Power(s1,4)*(-10 - 26*Power(s2,5) - 35*Power(t1,4) + 
               Power(s2,4)*(3 + 30*t1 - 6*t2) + 25*t2 + 8*Power(t2,2) - 
               25*Power(t2,3) + 
               Power(t1,3)*(150 + 39*t2 - 50*Power(t2,2)) + 
               t1*(146 - 178*t2 + 243*Power(t2,2) - 50*Power(t2,3)) + 
               2*Power(t1,2)*
                (-72 - 72*t2 + 2*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,3)*(-19 + 186*Power(t1,2) + 176*t2 + 
                  159*Power(t2,2) - t1*(201 + 221*t2)) - 
               Power(s2,2)*(10 + 50*Power(t1,3) - 161*t2 - 
                  34*Power(t2,2) + 50*Power(t2,3) + 
                  4*Power(t1,2)*(-82 + 67*t2) + 
                  t1*(420 + 195*t2 - 81*Power(t2,2))) + 
               s2*(-69 + 54*t2 + 207*Power(t2,2) - 110*Power(t2,3) + 
                  Power(t1,3)*(97 + 105*t2) + 
                  Power(t1,2)*(-8 - 458*t2 + 87*Power(t2,2)) + 
                  2*t1*(-10 - 4*t2 + 204*Power(t2,2) + 5*Power(t2,3)))) + 
            Power(s1,3)*(58 - 18*Power(s2,6) - 152*t2 + 22*Power(t2,2) - 
               82*Power(t2,3) + 50*Power(t2,4) + 
               Power(s2,5)*(73 - 23*t1 + 80*t2) + 
               Power(t1,4)*(4 + 88*t2) + 
               Power(t1,3)*(98 - 316*t2 - 84*Power(t2,2) + 
                  40*Power(t2,3)) + 
               Power(t1,2)*(5 + 8*t2 + 258*Power(t2,2) + 
                  74*Power(t2,3) - 15*Power(t2,4)) + 
               2*t1*(-15 - 53*t2 + 198*Power(t2,2) - 176*Power(t2,3) + 
                  20*Power(t2,4)) + 
               Power(s2,4)*(80 + 193*Power(t1,2) + 194*t2 + 
                  66*Power(t2,2) - 2*t1*(173 + 108*t2)) + 
               Power(s2,3)*(105 - 32*Power(t1,3) + 
                  Power(t1,2)*(261 - 544*t2) + 418*t2 - 
                  486*Power(t2,2) - 236*Power(t2,3) + 
                  t1*(-572 + 344*t2 + 506*Power(t2,2))) + 
               2*Power(s2,2)*
                (34 + 58*t2 - 294*Power(t2,2) - 88*Power(t2,3) + 
                  30*Power(t2,4) + 3*Power(t1,3)*(61 + 24*t2) + 
                  Power(t1,2)*(-377 - 591*t2 + 151*Power(t2,2)) + 
                  t1*(87 + 769*t2 + 477*Power(t2,2) - 62*Power(t2,3))) - 
               s2*(-37 + 88*Power(t1,4) - 118*t2 + 327*Power(t2,2) + 
                  408*Power(t2,3) - 160*Power(t2,4) + 
                  2*Power(t1,3)*(-326 + 97*t2 + 68*Power(t2,2)) + 
                  Power(t1,2)*
                   (1158 + 544*t2 - 551*Power(t2,2) + 28*Power(t2,3)) + 
                  t1*(-527 - 670*t2 - 554*Power(t2,2) + 
                     592*Power(t2,3) + 15*Power(t2,4)))) - 
            Power(s1,2)*(13 + 3*Power(s2,7) + 36*Power(t1,5) + 
               Power(s2,6)*(-53 + 27*t1 - 50*t2) + 146*t2 - 
               439*Power(t2,2) + 126*Power(t2,3) - 148*Power(t2,4) + 
               50*Power(t2,5) + 
               4*Power(t1,4)*(-83 - 6*t2 + 18*Power(t2,2)) + 
               Power(s2,5)*(-2 - 83*Power(t1,2) + t1*(157 - 4*t2) + 
                  79*t2 + 88*Power(t2,2)) + 
               Power(t1,3)*(717 + 446*t2 - 158*Power(t2,2) - 
                  78*Power(t2,3) + 15*Power(t2,4)) + 
               2*t1*(190 + 52*t2 + 132*Power(t2,2) + 215*Power(t2,3) - 
                  114*Power(t2,4) + 5*Power(t2,5)) - 
               Power(t1,2)*(906 + 235*t2 + 531*Power(t2,2) - 
                  190*Power(t2,3) - 86*Power(t2,4) + 9*Power(t2,5)) + 
               Power(s2,4)*(-2 + 3*Power(t1,3) + 252*t2 + 
                  508*Power(t2,2) + 102*Power(t2,3) + 
                  Power(t1,2)*(41 + 469*t2) - 
                  2*t1*(56 + 410*t2 + 209*Power(t2,2))) - 
               Power(s2,3)*(-68 - 779*t2 - 1086*Power(t2,2) + 
                  542*Power(t2,3) + 179*Power(t2,4) + 
                  Power(t1,3)*(397 + 72*t2) + 
                  3*Power(t1,2)*(-435 - 301*t2 + 188*Power(t2,2)) + 
                  t1*(729 + 2582*t2 + 16*Power(t2,2) - 522*Power(t2,3))) \
+ Power(s2,2)*(308 + 54*Power(t1,4) + 830*t2 + 463*Power(t2,2) - 
                  738*Power(t2,3) - 264*Power(t2,4) + 40*Power(t2,5) + 
                  36*Power(t1,3)*(-17 + 21*t2 + 4*Power(t2,2)) + 
                  Power(t1,2)*
                   (1595 - 792*t2 - 1575*Power(t2,2) + 128*Power(t2,3)) \
+ t1*(-1649 - 1004*t2 + 1894*Power(t2,2) + 1362*Power(t2,3) - 
                     91*Power(t2,4))) + 
               s2*(76 + 497*t2 + 101*Power(t2,2) - 517*Power(t2,3) - 
                  447*Power(t2,4) + 130*Power(t2,5) - 
                  2*Power(t1,4)*(98 + 87*t2) + 
                  Power(t1,3)*
                   (570 + 1416*t2 - 77*Power(t2,2) - 78*Power(t2,3)) + 
                  Power(t1,2)*
                   (-9 - 2562*t2 - 1377*Power(t2,2) + 245*Power(t2,3) + 
                     23*Power(t2,4)) + 
                  t1*(-271 + 682*t2 + 1495*Power(t2,2) + 
                     1004*Power(t2,3) - 408*Power(t2,4) - 16*Power(t2,5))\
)) + s1*(33 + 445*t2 + 79*Power(t2,2) - 382*Power(t2,3) + 
               131*Power(t2,4) - 107*Power(t2,5) + 25*Power(t2,6) + 
               Power(s2,7)*(9 - 5*t1 + 6*t2) + 
               4*Power(t1,5)*(7 + 10*t2) + 
               Power(s2,6)*(-24 - 11*t1 + 11*Power(t1,2) - 74*t2 + 
                  42*t1*t2 - 46*Power(t2,2)) + 
               Power(t1,4)*(-52 - 374*t2 - 44*Power(t2,2) + 
                  20*Power(t2,3)) + 
               2*Power(t1,3)*(16 + 337*t2 + 227*Power(t2,2) + 
                  16*Power(t2,3) - 15*Power(t2,4) + Power(t2,5)) + 
               t1*(-213 - 364*t2 + 288*Power(t2,2) + 262*Power(t2,3) + 
                  229*Power(t2,4) - 54*Power(t2,5) - 4*Power(t2,6)) + 
               Power(t1,2)*(200 - 613*t2 - 308*Power(t2,2) - 
                  510*Power(t2,3) + 42*Power(t2,4) + 35*Power(t2,5) - 
                  2*Power(t2,6)) + 
               Power(s2,5)*(-22 + 2*Power(t1,3) - 112*t2 - 
                  33*Power(t2,2) + 40*Power(t2,3) - 
                  Power(t1,2)*(71 + 150*t2) + 
                  t1*(147 + 306*t2 + 49*Power(t2,2))) + 
               Power(s2,4)*(-111 - 129*t2 + 219*Power(t2,2) + 
                  422*Power(t2,3) + 60*Power(t2,4) + 
                  2*Power(t1,3)*(73 + 3*t2) + 
                  Power(t1,2)*(-416 - 49*t2 + 364*Power(t2,2)) + 
                  t1*(297 + 272*t2 - 626*Power(t2,2) - 308*Power(t2,3))) \
- Power(s2,3)*(4*Power(t1,4) + 
                  Power(t1,3)*(22 + 612*t2 + 48*Power(t2,2)) + 
                  Power(t1,2)*
                   (251 - 2106*t2 - 950*Power(t2,2) + 240*Power(t2,3)) + 
                  t1*(-555 + 1452*t2 + 3104*Power(t2,2) + 
                     260*Power(t2,3) - 245*Power(t2,4)) + 
                  2*(105 - 70*t2 - 534*Power(t2,2) - 497*Power(t2,3) + 
                     130*Power(t2,4) + 33*Power(t2,5))) + 
               Power(s2,2)*(4*Power(t1,4)*(65 + 17*t2) + 
                  4*Power(t1,3)*
                   (-290 - 309*t2 + 121*Power(t2,2) + 14*Power(t2,3)) + 
                  Power(t1,2)*
                   (1711 + 3914*t2 + 334*Power(t2,2) - 
                     916*Power(t2,3) + 7*Power(t2,4)) - 
                  t1*(983 + 3902*t2 + 2366*Power(t2,2) - 
                     854*Power(t2,3) - 807*Power(t2,4) + 30*Power(t2,5)) \
+ 2*(94 + 372*t2 + 751*Power(t2,2) + 309*Power(t2,3) - 193*Power(t2,4) - 
                     86*Power(t2,5) + 7*Power(t2,6))) - 
               s2*(-259 + 24*Power(t1,5) - 660*t2 - 988*Power(t2,2) - 
                  124*Power(t2,3) + 339*Power(t2,4) + 252*Power(t2,5) - 
                  56*Power(t2,6) + 
                  2*Power(t1,4)*(-215 + 116*t2 + 50*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (859 - 85*t2 - 491*Power(t2,2) - 20*Power(t2,3) + 
                     9*Power(t2,4)) + 
                  Power(t1,2)*
                   (-2460 - 1554*t2 + 1559*Power(t2,2) + 
                     1090*Power(t2,3) + Power(t2,4) - 18*Power(t2,5)) + 
                  t1*(1471 + 1848*t2 + 487*Power(t2,2) - 
                     1060*Power(t2,3) - 683*Power(t2,4) + 
                     120*Power(t2,5) + 9*Power(t2,6))))))*
       T2(1 - s + s2 - t1,1 - s1 - t1 + t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s1 + t1 - t2)*
       Power(1 - s + s*s2 - s1*s2 - t1 + s2*t2,2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) - 
    (8*(24 - 14*s2 - 28*Power(s2,2) + 30*Power(s2,3) + 6*Power(s2,4) - 
         Power(s2,5) - 70*t1 + 86*s2*t1 + 16*Power(s2,2)*t1 - 
         85*Power(s2,3)*t1 - 18*Power(s2,4)*t1 + 3*Power(s2,5)*t1 + 
         38*Power(t1,2) - 94*s2*Power(t1,2) + 
         41*Power(s2,2)*Power(t1,2) + 85*Power(s2,3)*Power(t1,2) + 
         18*Power(s2,4)*Power(t1,2) - 3*Power(s2,5)*Power(t1,2) + 
         24*Power(t1,3) + 17*s2*Power(t1,3) + 5*Power(s2,2)*Power(t1,3) - 
         37*Power(s2,3)*Power(t1,3) - 6*Power(s2,4)*Power(t1,3) + 
         Power(s2,5)*Power(t1,3) - 3*Power(t1,4) - 50*s2*Power(t1,4) - 
         57*Power(s2,2)*Power(t1,4) + 9*Power(s2,3)*Power(t1,4) - 
         9*Power(t1,5) + 80*s2*Power(t1,5) + 23*Power(s2,2)*Power(t1,5) - 
         2*Power(s2,3)*Power(t1,5) - 7*Power(t1,6) - 26*s2*Power(t1,6) + 
         3*Power(t1,7) + s2*Power(t1,7) + 
         Power(s1,7)*(-(Power(s2,2)*(-3 + t1)) - (-2 + t1)*Power(t1,2) + 
            s2*t1*(3 + 2*t1)) - 2*t2 + 6*s2*t2 + 32*Power(s2,2)*t2 - 
         33*Power(s2,3)*t2 + 69*Power(s2,4)*t2 + 15*Power(s2,5)*t2 - 
         2*Power(s2,6)*t2 + 108*t1*t2 - 136*s2*t1*t2 + 
         10*Power(s2,2)*t1*t2 - 60*Power(s2,3)*t1*t2 - 
         139*Power(s2,4)*t1*t2 - 26*Power(s2,5)*t1*t2 + 
         4*Power(s2,6)*t1*t2 - 168*Power(t1,2)*t2 + 
         143*s2*Power(t1,2)*t2 - 51*Power(s2,2)*Power(t1,2)*t2 + 
         89*Power(s2,3)*Power(t1,2)*t2 + 79*Power(s2,4)*Power(t1,2)*t2 + 
         11*Power(s2,5)*Power(t1,2)*t2 - 2*Power(s2,6)*Power(t1,2)*t2 + 
         24*Power(t1,3)*t2 + 126*s2*Power(t1,3)*t2 + 
         122*Power(s2,2)*Power(t1,3)*t2 + 46*Power(s2,3)*Power(t1,3)*t2 - 
         13*Power(s2,4)*Power(t1,3)*t2 + 6*Power(t1,4)*t2 - 
         216*s2*Power(t1,4)*t2 - 157*Power(s2,2)*Power(t1,4)*t2 - 
         42*Power(s2,3)*Power(t1,4)*t2 + 4*Power(s2,4)*Power(t1,4)*t2 + 
         47*Power(t1,5)*t2 + 82*s2*Power(t1,5)*t2 + 
         46*Power(s2,2)*Power(t1,5)*t2 - 14*Power(t1,6)*t2 - 
         5*s2*Power(t1,6)*t2 - 2*Power(s2,2)*Power(t1,6)*t2 - 
         Power(t1,7)*t2 - 70*Power(t2,2) + 84*s2*Power(t2,2) - 
         35*Power(s2,2)*Power(t2,2) + 89*Power(s2,3)*Power(t2,2) + 
         10*Power(s2,4)*Power(t2,2) + 42*Power(s2,5)*Power(t2,2) + 
         7*Power(s2,6)*Power(t2,2) - Power(s2,7)*Power(t2,2) + 
         72*t1*Power(t2,2) + 79*s2*t1*Power(t2,2) - 
         117*Power(s2,2)*t1*Power(t2,2) - 35*Power(s2,3)*t1*Power(t2,2) - 
         43*Power(s2,4)*t1*Power(t2,2) - 47*Power(s2,5)*t1*Power(t2,2) - 
         3*Power(s2,6)*t1*Power(t2,2) + Power(s2,7)*t1*Power(t2,2) + 
         70*Power(t1,2)*Power(t2,2) - 328*s2*Power(t1,2)*Power(t2,2) - 
         90*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         170*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         6*Power(t1,3)*Power(t2,2) + 296*s2*Power(t1,3)*Power(t2,2) + 
         422*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         143*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         21*Power(s2,4)*Power(t1,3)*Power(t2,2) - 
         2*Power(s2,5)*Power(t1,3)*Power(t2,2) - 
         113*Power(t1,4)*Power(t2,2) - 189*s2*Power(t1,4)*Power(t2,2) - 
         146*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         20*Power(s2,3)*Power(t1,4)*Power(t2,2) + 
         36*Power(t1,5)*Power(t2,2) + 24*s2*Power(t1,5)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,5)*Power(t2,2) + 
         Power(s2,3)*Power(t1,5)*Power(t2,2) + 
         3*Power(t1,6)*Power(t2,2) + 2*s2*Power(t1,6)*Power(t2,2) + 
         40*Power(t2,3) - 159*s2*Power(t2,3) + 
         119*Power(s2,2)*Power(t2,3) - 33*Power(s2,3)*Power(t2,3) + 
         24*Power(s2,4)*Power(t2,3) + 7*Power(s2,5)*Power(t2,3) + 
         7*Power(s2,6)*Power(t2,3) - 2*Power(s2,7)*Power(t2,3) - 
         92*t1*Power(t2,3) + 208*s2*t1*Power(t2,3) + 
         194*Power(s2,2)*t1*Power(t2,3) + 94*Power(s2,3)*t1*Power(t2,3) + 
         51*Power(s2,4)*t1*Power(t2,3) + 9*Power(s2,5)*t1*Power(t2,3) + 
         5*Power(s2,6)*t1*Power(t2,3) - 44*Power(t1,2)*Power(t2,3) - 
         172*s2*Power(t1,2)*Power(t2,3) - 
         546*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         233*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         65*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         2*Power(s2,5)*Power(t1,2)*Power(t2,3) + 
         140*Power(t1,3)*Power(t2,3) + 290*s2*Power(t1,3)*Power(t2,3) + 
         224*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         68*Power(s2,3)*Power(t1,3)*Power(t2,3) - 
         57*Power(t1,4)*Power(t2,3) - 60*s2*Power(t1,4)*Power(t2,3) - 
         4*Power(s2,2)*Power(t1,4)*Power(t2,3) - 
         3*Power(t1,5)*Power(t2,3) - 7*s2*Power(t1,5)*Power(t2,3) - 
         Power(s2,2)*Power(t1,5)*Power(t2,3) + 5*Power(t2,4) + 
         32*s2*Power(t2,4) - 157*Power(s2,2)*Power(t2,4) + 
         6*Power(s2,3)*Power(t2,4) - 34*Power(s2,4)*Power(t2,4) - 
         8*Power(s2,5)*Power(t2,4) - 4*Power(s2,6)*Power(t2,4) + 
         57*t1*Power(t2,4) + 18*s2*t1*Power(t2,4) + 
         278*Power(s2,2)*t1*Power(t2,4) + 
         234*Power(s2,3)*t1*Power(t2,4) + 55*Power(s2,4)*t1*Power(t2,4) + 
         4*Power(s2,5)*t1*Power(t2,4) - 89*Power(t1,2)*Power(t2,4) - 
         247*s2*Power(t1,2)*Power(t2,4) - 
         215*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         88*Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         2*Power(s2,4)*Power(t1,2)*Power(t2,4) + 
         52*Power(t1,3)*Power(t2,4) + 82*s2*Power(t1,3)*Power(t2,4) + 
         18*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         6*Power(s2,3)*Power(t1,3)*Power(t2,4) + 
         9*s2*Power(t1,4)*Power(t2,4) + 
         4*Power(s2,2)*Power(t1,4)*Power(t2,4) - 16*Power(t2,5) - 
         6*s2*Power(t2,5) - 20*Power(s2,2)*Power(t2,5) - 
         98*Power(s2,3)*Power(t2,5) - 16*Power(s2,4)*Power(t2,5) + 
         21*t1*Power(t2,5) + 112*s2*t1*Power(t2,5) + 
         124*Power(s2,2)*t1*Power(t2,5) + 45*Power(s2,3)*t1*Power(t2,5) - 
         2*Power(s2,4)*t1*Power(t2,5) - 27*Power(t1,2)*Power(t2,5) - 
         66*s2*Power(t1,2)*Power(t2,5) - 
         23*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
         8*Power(s2,3)*Power(t1,2)*Power(t2,5) + 
         3*Power(t1,3)*Power(t2,5) - 5*s2*Power(t1,3)*Power(t2,5) - 
         6*Power(s2,2)*Power(t1,3)*Power(t2,5) + Power(t2,6) - 
         22*s2*Power(t2,6) - 33*Power(s2,2)*Power(t2,6) - 
         5*Power(s2,3)*Power(t2,6) + 9*t1*Power(t2,6) + 
         29*s2*t1*Power(t2,6) + 10*Power(s2,2)*t1*Power(t2,6) - 
         3*Power(s2,3)*t1*Power(t2,6) - 3*Power(t1,2)*Power(t2,6) + 
         s2*Power(t1,2)*Power(t2,6) + 
         4*Power(s2,2)*Power(t1,2)*Power(t2,6) - 2*Power(t2,7) - 
         5*s2*Power(t2,7) - Power(s2,2)*Power(t2,7) + t1*Power(t2,7) - 
         Power(s2,2)*t1*Power(t2,7) + 
         Power(s1,6)*(Power(s2,3)*(-19 + t1) + 
            Power(s2,2)*(-5*Power(t1,2) - 19*t2 + 5*t1*t2) + 
            t1*(-3 - 3*Power(t1,3) + t1*(7 - 8*t2) - 4*t2 + 
               Power(t1,2)*(6 + 5*t2)) + 
            s2*(7*Power(t1,3) - 3*(2 + t2) - 5*Power(t1,2)*(-3 + 2*t2) - 
               2*t1*(4 + 11*t2))) - 
         Power(s,5)*(t1 - t2)*
          (-4 - Power(s2,4) + Power(t1,3) + Power(t1,4) - 
            3*Power(t1,2)*t2 - 4*Power(t1,3)*t2 + 3*t1*Power(t2,2) + 
            6*Power(t1,2)*Power(t2,2) - Power(t2,3) - 4*t1*Power(t2,3) + 
            Power(t2,4) - Power(s1,3)*(-1 + s2 - t1 + t2) + 
            Power(s2,3)*(9 - 4*t1 + 4*t2) + 
            3*Power(s2,2)*(-6 + t1 + 2*Power(t1,2) - t2 - 4*t1*t2 + 
               2*Power(t2,2)) + 
            3*Power(s1,2)*(Power(s2,2) + t1 + Power(t1,2) - 2*t1*t2 + 
               (-1 + t2)*t2 + s2*(-1 - 2*t1 + 2*t2)) + 
            s2*(14 - 4*Power(t1,3) + 6*t1*(1 - 2*t2)*t2 - 
               3*Power(t2,2) + 4*Power(t2,3) + 3*Power(t1,2)*(-1 + 4*t2)\
) + s1*(2 - 5*Power(s2,3) + 3*Power(t1,3) + Power(t1,2)*(3 - 9*t2) + 
               9*Power(s2,2)*(1 + t1 - t2) + 3*Power(t2,2) - 
               3*Power(t2,3) + 3*t1*t2*(-2 + 3*t2) - 
               3*s2*(2 + 3*Power(t1,2) + t1*(2 - 6*t2) - 2*t2 + 
                  3*Power(t2,2)))) + 
         Power(s1,5)*(3 - 2*Power(s2,5) - 3*Power(t1,5) + 
            2*Power(s2,4)*(11 + t1) + 3*t2 + 2*Power(t2,2) + 
            Power(t1,4)*(5 + 12*t2) - 
            2*Power(t1,3)*(-19 + 9*t2 + 5*Power(t2,2)) + 
            Power(t1,2)*(-33 - 43*t2 + 10*Power(t2,2)) + 
            t1*(9 + 4*t2 + 19*Power(t2,2)) + 
            Power(s2,3)*(4 - 2*Power(t1,2) + 108*t2 - t1*(47 + 2*t2)) + 
            Power(s2,2)*(36 - Power(t1,3) + 23*t2 + 51*Power(t2,2) + 
               Power(t1,2)*(-33 + 17*t2) + t1*(1 - 4*t2 - 9*Power(t2,2))\
) + s2*(6*Power(t1,4) + Power(t1,3)*(35 - 27*t2) + 4*t2*(12 + 5*t2) + 
               Power(t1,2)*(-42 - 96*t2 + 20*Power(t2,2)) + 
               t1*(-34 + 27*t2 + 65*Power(t2,2)))) - 
         Power(s1,4)*(Power(t1,6) + Power(t1,5)*(1 - 9*t2) + 
            2*Power(s2,5)*(5 + 4*t1 - 6*t2) + 
            Power(t1,2)*(9 - 76*t2 - 99*Power(t2,2)) + 
            2*Power(t1,4)*(-37 + 4*t2 + 9*Power(t2,2)) + 
            t2*(29 + 11*t2 + 10*Power(t2,2)) + 
            Power(t1,3)*(72 + 147*t2 - 12*Power(t2,2) - 
               10*Power(t2,3)) + 
            t1*(-34 - 4*t2 - 23*Power(t2,2) + 35*Power(t2,3)) + 
            Power(s2,4)*(-2 - 14*Power(t1,2) + 102*t2 + 
               t1*(-45 + 16*t2)) + 
            Power(s2,3)*(-2 + 15*Power(t1,3) + 
               Power(t1,2)*(40 - 18*t2) + 100*t2 + 247*Power(t2,2) + 
               t1*(-53 - 237*t2 + 5*Power(t2,2))) + 
            Power(s2,2)*(24 - 11*Power(t1,4) + 192*t2 + 
               125*Power(t2,2) + 75*Power(t2,3) + 
               Power(t1,3)*(55 + 8*t2) + 
               Power(t1,2)*(31 - 123*t2 + 14*Power(t2,2)) - 
               t1*(145 + 120*t2 + 26*Power(t2,2) + 5*Power(t2,3))) + 
            s2*(15 + Power(t1,5) + 12*t2 + 154*Power(t2,2) + 
               55*Power(t2,3) + Power(t1,4)*(-41 + 15*t2) + 
               Power(t1,3)*(100 + 171*t2 - 38*Power(t2,2)) + 
               5*Power(t1,2)*
                (6 - 21*t2 - 47*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(1 - 268*t2 - Power(t2,2) + 100*Power(t2,3)))) + 
         Power(s1,3)*(-2 + 2*Power(s2,7) - 11*t2 + 85*Power(t2,2) + 
            14*Power(t2,3) + 20*Power(t2,4) + Power(t1,6)*(-3 + 2*t2) + 
            Power(s2,6)*(1 - 5*t1 + 4*t2) + 
            Power(t1,5)*(64 + 7*t2 - 9*Power(t2,2)) + 
            Power(t1,4)*(-45 - 200*t2 - 6*Power(t2,2) + 
               12*Power(t2,3)) + 
            Power(t1,2)*(95 + 147*t2 - 3*Power(t2,2) - 
               106*Power(t2,3) - 10*Power(t2,4)) + 
            Power(t1,3)*(-92 + 107*t2 + 210*Power(t2,2) + 
               12*Power(t2,3) - 5*Power(t2,4)) + 
            t1*(-1 - 152*t2 - 87*Power(t2,2) - 72*Power(t2,3) + 
               30*Power(t2,4)) + 
            2*Power(s2,5)*(-10 + Power(t1,2) + 23*t2 - 12*Power(t2,2) + 
               2*t1*(-7 + 5*t2)) + 
            Power(s2,4)*(2*Power(t1,3) + Power(t1,2)*(63 - 40*t2) + 
               t1*(23 - 196*t2 + 38*Power(t2,2)) + 
               2*(-9 + 3*t2 + 95*Power(t2,2))) + 
            Power(s2,3)*(50 - 6*Power(t1,4) - 32*t2 + 374*Power(t2,2) + 
               288*Power(t2,3) + Power(t1,3)*(-41 + 43*t2) + 
               Power(t1,2)*(84 + 200*t2 - 50*Power(t2,2)) + 
               t1*(-79 - 333*t2 - 474*Power(t2,2) + 20*Power(t2,3))) + 
            Power(s2,2)*(-104 + 7*Power(t1,5) + 243*t2 + 
               380*Power(t2,2) + 270*Power(t2,3) + 65*Power(t2,4) - 
               Power(t1,4)*(21 + 34*t2) + 
               Power(t1,3)*(-56 + 159*t2 + 36*Power(t2,2)) + 
               Power(t1,2)*(296 + 315*t2 - 148*Power(t2,2) - 
                  14*Power(t2,3)) + 
               t1*(-68 - 757*t2 - 490*Power(t2,2) - 64*Power(t2,3) + 
                  5*Power(t2,4))) + 
            s2*(36 - 2*Power(t1,6) + 56*t2 + 42*Power(t2,2) + 
               256*Power(t2,3) + 80*Power(t2,4) + 
               Power(t1,5)*(21 + 5*t2) + 
               Power(t1,4)*(-105 - 142*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(-49 + 171*t2 + 308*Power(t2,2) - 
                  22*Power(t2,3)) + 
               Power(t1,2)*(93 + 456*t2 + 3*Power(t2,2) - 
                  280*Power(t2,3) + 10*Power(t2,4)) + 
               t1*(-94 - 122*t2 - 712*Power(t2,2) - 114*Power(t2,3) + 
                  85*Power(t2,4)))) + 
         Power(s1,2)*(-16 - Power(t1,7) + Power(s2,7)*(-1 + t1 - 6*t2) + 
            28*t2 + 27*Power(t2,2) - 111*Power(t2,3) - 6*Power(t2,4) - 
            20*Power(t2,5) + Power(t1,6)*(23 + 6*t2 - Power(t2,2)) + 
            Power(t1,5)*(10 - 112*t2 - 11*Power(t2,2) + 3*Power(t2,3)) + 
            Power(t1,4)*(-132 - 10*t2 + 178*Power(t2,2) + 
               16*Power(t2,3) - 3*Power(t2,4)) + 
            t1*(-4 - 60*t2 + 259*Power(t2,2) + 147*Power(t2,3) + 
               83*Power(t2,4) - 10*Power(t2,5)) + 
            Power(t1,3)*(87 + 351*t2 + 54*Power(t2,2) - 
               128*Power(t2,3) - 18*Power(t2,4) + Power(t2,5)) + 
            Power(t1,2)*(37 - 251*t2 - 356*Power(t2,2) - 
               117*Power(t2,3) + 49*Power(t2,4) + 8*Power(t2,5)) + 
            Power(s2,6)*(2 - Power(t1,2) + 5*t2 - 12*Power(t2,2) + 
               3*t1*(1 + 5*t2)) + 
            Power(s2,5)*(28 + 2*Power(t1,3) + 45*t2 - 70*Power(t2,2) + 
               20*Power(t2,3) - Power(t1,2)*(27 + 8*t2) + 
               t1*(-11 + 69*t2 - 12*Power(t2,2))) + 
            Power(s2,4)*(12 - 6*Power(t1,4) + 54*t2 - 52*Power(t2,2) - 
               178*Power(t2,3) + 3*Power(t1,3)*(22 + t2) + 
               5*Power(t1,2)*(-14 - 39*t2 + 8*Power(t2,2)) + 
               t1*(-14 + 8*t2 + 312*Power(t2,2) - 38*Power(t2,3))) + 
            Power(s2,3)*(52 + 5*Power(t1,5) - 115*t2 + 64*Power(t2,2) - 
               562*Power(t2,3) - 177*Power(t2,4) + 
               Power(t1,4)*(-58 + 3*t2) + 
               Power(t1,3)*(158 + 144*t2 - 47*Power(t2,2)) + 
               Power(t1,2)*(-99 - 392*t2 - 368*Power(t2,2) + 
                  62*Power(t2,3)) + 
               t1*(-50 + 240*t2 + 741*Power(t2,2) + 470*Power(t2,3) - 
                  25*Power(t2,4))) + 
            s2*(114 - 241*t2 - 35*Power(t2,2) - 54*Power(t2,3) - 
               234*Power(t2,4) - 65*Power(t2,5) + 
               Power(t1,6)*(2 + 3*t2) - 
               Power(t1,5)*(39 + 51*t2 + 7*Power(t2,2)) + 
               Power(t1,4)*(-96 + 103*t2 + 170*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,3)*(303 + 494*t2 + 40*Power(t2,2) - 
                  248*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(-5 + 434*t2 + 265*Power(t2,2) + 868*Power(t2,3) + 
                  186*Power(t2,4) - 38*Power(t2,5)) - 
               Power(t1,2)*(311 + 442*t2 + 1069*Power(t2,2) + 
                  219*Power(t2,3) - 165*Power(t2,4) + 2*Power(t2,5))) - 
            Power(s2,2)*(74 + Power(t1,6) - 342*t2 + 571*Power(t2,2) + 
               348*Power(t2,3) + 290*Power(t2,4) + 33*Power(t2,5) + 
               2*Power(t1,5)*(-8 + 5*t2) + 
               Power(t1,4)*(79 - 46*t2 - 39*Power(t2,2)) + 
               Power(t1,3)*(-239 - 354*t2 + 135*Power(t2,2) + 
                  50*Power(t2,3)) + 
               Power(t1,2)*(91 + 1155*t2 + 752*Power(t2,2) - 
                  36*Power(t2,3) - 31*Power(t2,4)) + 
               t1*(-26 - 301*t2 - 1357*Power(t2,2) - 740*Power(t2,3) - 
                  76*Power(t2,4) + 9*Power(t2,5)))) + 
         s1*(-34 + 98*t2 - 66*Power(t2,2) - 21*Power(t2,3) + 
            68*Power(t2,4) - Power(t2,5) + 10*Power(t2,6) + 
            Power(t1,7)*(2 + t2) + 2*Power(s2,7)*t2*(1 - t1 + 3*t2) + 
            Power(t1,6)*(14 - 21*t2 - 3*Power(t2,2)) + 
            Power(t1,5)*(-55 - 55*t2 + 51*Power(t2,2) + 5*Power(t2,3)) + 
            Power(t1,4)*(18 + 242*t2 + 112*Power(t2,2) - 
               52*Power(t2,3) - 7*Power(t2,4)) + 
            Power(t1,3)*(1 - 96*t2 - 399*Power(t2,2) - 
               141*Power(t2,3) + 24*Power(t2,4) + 6*Power(t2,5)) + 
            Power(t1,2)*(36 - 69*t2 + 200*Power(t2,2) + 
               307*Power(t2,3) + 104*Power(t2,4) - 3*Power(t2,5) - 
               2*Power(t2,6)) - 
            t1*(-18 + 108*t2 - 153*Power(t2,2) + 198*Power(t2,3) + 
               94*Power(t2,4) + 44*Power(t2,5) + Power(t2,6)) + 
            Power(s2,6)*(2 - 9*t2 - 13*Power(t2,2) + 12*Power(t2,3) + 
               Power(t1,2)*(2 + t2) - t1*(4 + 15*Power(t2,2))) - 
            Power(s2,5)*(10 + Power(t1,3) + 74*t2 + 32*Power(t2,2) - 
               42*Power(t2,3) + 6*Power(t2,4) - 
               4*Power(t1,2)*(-1 + 6*t2 + 2*Power(t2,2)) + 
               t1*(-15 - 66*t2 + 50*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,4)*(-59 - 16*t2 + 6*Power(t1,4)*t2 - 
               60*Power(t2,2) + 78*Power(t2,3) + 84*Power(t2,4) - 
               Power(t1,3)*(13 + 73*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-29 + 48*t2 + 197*Power(t2,2) - 
                  16*Power(t2,3)) + 
               t1*(101 + 59*t2 - 82*Power(t2,2) - 216*Power(t2,3) + 
                  16*Power(t2,4))) + 
            Power(s2,3)*(36 - 132*t2 + 98*Power(t2,2) - 
               40*Power(t2,3) + 382*Power(t2,4) + 52*Power(t2,5) - 
               6*Power(t1,5)*(1 + t2) + 
               Power(t1,4)*(77 + 60*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(-126 - 288*t2 - 171*Power(t2,2) + 
                  25*Power(t2,3)) + 
               Power(t1,2)*(-7 + 306*t2 + 541*Power(t2,2) + 
                  296*Power(t2,3) - 36*Power(t2,4)) + 
               t1*(26 + 44*t2 - 255*Power(t2,2) - 695*Power(t2,3) - 
                  231*Power(t2,4) + 14*Power(t2,5))) - 
            s2*(Power(t1,7) + Power(t1,6)*(-7 + 6*t2 + Power(t2,2)) + 
               Power(t1,5)*(89 + 2*t2 - 37*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(t1,4)*(-215 - 324*t2 - 62*Power(t2,2) + 
                  78*Power(t2,3) + 3*Power(t2,4)) + 
               Power(t1,3)*(152 + 582*t2 + 735*Power(t2,2) + 
                  193*Power(t2,3) - 81*Power(t2,4) - Power(t2,5)) - 
               2*t2*(-103 + 182*t2 - 19*Power(t2,2) + 15*Power(t2,3) + 
                  56*Power(t2,4) + 14*Power(t2,5)) + 
               Power(t1,2)*(58 - 558*t2 - 521*Power(t2,2) - 
                  890*Power(t2,3) - 219*Power(t2,4) + 40*Power(t2,5)) + 
               t1*(-78 + 22*t2 + 548*Power(t2,2) + 160*Power(t2,3) + 
                  502*Power(t2,4) + 121*Power(t2,5) - 7*Power(t2,6))) + 
            Power(s2,2)*(-10 + 91*t2 - 357*Power(t2,2) + 
               509*Power(t2,3) + 144*Power(t2,4) + 155*Power(t2,5) + 
               9*Power(t2,6) + Power(t1,6)*(6 + t2) + 
               Power(t1,5)*(-65 - 8*t2 + 4*Power(t2,2)) + 
               Power(t1,4)*(205 + 238*t2 - 21*Power(t2,2) - 
                  20*Power(t2,3)) + 
               Power(t1,3)*(-174 - 726*t2 - 522*Power(t2,2) + 
                  13*Power(t2,3) + 29*Power(t2,4)) + 
               Power(t1,2)*(89 + 214*t2 + 1405*Power(t2,2) + 
                  683*Power(t2,3) + 45*Power(t2,4) - 19*Power(t2,5)) + 
               t1*(-51 + 118*t2 - 427*Power(t2,2) - 1023*Power(t2,3) - 
                  495*Power(t2,4) - 44*Power(t2,5) + 5*Power(t2,6)))) + 
         Power(s,4)*(4 - 36*t1 + 20*Power(t1,2) + 3*Power(t1,3) + 
            3*Power(t1,4) + 2*Power(t1,5) - Power(t1,6) + 36*t2 - 
            20*t1*t2 - 7*Power(t1,2)*t2 - 16*Power(t1,3)*t2 - 
            14*Power(t1,4)*t2 + 3*Power(t1,5)*t2 + 5*t1*Power(t2,2) + 
            30*Power(t1,2)*Power(t2,2) + 36*Power(t1,3)*Power(t2,2) - 
            Power(t2,3) - 24*t1*Power(t2,3) - 
            44*Power(t1,2)*Power(t2,3) - 10*Power(t1,3)*Power(t2,3) + 
            7*Power(t2,4) + 26*t1*Power(t2,4) + 
            15*Power(t1,2)*Power(t2,4) - 6*Power(t2,5) - 
            9*t1*Power(t2,5) + 2*Power(t2,6) - Power(s2,5)*(2 + t2) + 
            Power(s1,4)*(-Power(s2,2) + 4*Power(t1,2) + t1*(5 - 6*t2) + 
               2*(-1 + t2)*t2 + s2*(1 - 4*t1 + t2)) + 
            Power(s2,4)*(1 - 10*Power(t1,2) - 15*t2 - 19*Power(t2,2) + 
               t1*(16 + 29*t2)) + 
            Power(s2,3)*(1 + 23*Power(t1,3) - 12*t2 + 5*Power(t2,2) - 
               13*Power(t2,3) - Power(t1,2)*(38 + 59*t2) + 
               t1*(10 + 33*t2 + 49*Power(t2,2))) + 
            Power(s2,2)*(6 - 19*Power(t1,4) + 93*t2 - 10*Power(t2,2) - 
               35*Power(t2,3) - 3*Power(t2,4) + 
               2*Power(t1,3)*(7 + 30*t2) + 
               Power(t1,2)*(80 - 63*t2 - 66*Power(t2,2)) + 
               7*t1*(-13 - 10*t2 + 12*Power(t2,2) + 4*Power(t2,3))) + 
            s2*(-10 + 7*Power(t1,5) - 102*t2 + 13*Power(t2,2) + 
               24*Power(t2,3) - 23*Power(t2,4) + 4*Power(t2,5) - 
               6*Power(t1,4)*(1 + 4*t2) + 
               Power(t1,3)*(-18 + 41*t2 + 26*Power(t2,2)) - 
               Power(t1,2)*(61 - 60*t2 + 87*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(102 + 48*t2 - 66*Power(t2,2) + 75*Power(t2,3) - 
                  9*Power(t2,4))) + 
            Power(s1,3)*(1 + 3*Power(s2,3) + 11*Power(t1,3) + 
               Power(t1,2)*(17 - 30*t2) - 7*t2 + 12*Power(t2,2) - 
               8*Power(t2,3) + Power(s2,2)*(-1 + 5*t1 + 3*t2) + 
               t1*(3 - 29*t2 + 27*Power(t2,2)) - 
               s2*(3 + 20*Power(t1,2) - 8*t2 + 7*Power(t2,2) - 
                  3*t1*(-4 + 9*t2))) - 
            Power(s1,2)*(-4 + 5*Power(s2,4) - 9*Power(t1,4) + t2 - 
               21*Power(t2,2) + 24*Power(t2,3) - 12*Power(t2,4) + 
               Power(s2,3)*(1 + 2*t1 + 6*t2) + 
               3*Power(t1,3)*(-7 + 13*t2) + 
               Power(t1,2)*(-9 + 66*t2 - 63*Power(t2,2)) + 
               t1*(-3 + 30*t2 - 69*Power(t2,2) + 45*Power(t2,3)) + 
               Power(s2,2)*(-15 - 9*Power(t1,2) + 31*t2 + 
                  6*Power(t2,2) + t1*(-31 + 3*t2)) + 
               s2*(13 + 21*Power(t1,3) + Power(t1,2)*(33 - 57*t2) - 
                  32*t2 + 42*Power(t2,2) - 15*Power(t2,3) + 
                  t1*(26 - 75*t2 + 51*Power(t2,2)))) + 
            s1*(-10 + 3*Power(s2,5) + Power(t1,5) + 
               Power(t1,4)*(11 - 12*t2) - 18*t2 + Power(t2,2) - 
               21*Power(t2,3) + 20*Power(t2,4) - 8*Power(t2,5) + 
               Power(s2,4)*(-1 - 23*t1 + 27*t2) + 
               Power(s2,3)*(7 + 23*Power(t1,2) + t1*(20 - 39*t2) - 
                  20*t2 + 16*Power(t2,2)) + 
               Power(t1,3)*(9 - 53*t2 + 38*Power(t2,2)) - 
               Power(t1,2)*(5 + 39*t2 - 93*Power(t2,2) + 
                  52*Power(t2,3)) + 
               t1*(18 + 4*t2 + 51*Power(t2,2) - 71*Power(t2,3) + 
                  33*Power(t2,4)) + 
               Power(s2,2)*(-29 - 16*Power(t1,3) - 13*t2 + 
                  67*Power(t2,2) + 7*Power(t2,3) + 
                  Power(t1,2)*(16 + 39*t2) + 
                  t1*(13 - 83*t2 - 30*Power(t2,2))) + 
               s2*(30 + 2*Power(t1,4) + 28*t2 - 53*Power(t2,2) + 
                  56*Power(t2,3) - 13*Power(t2,4) + 
                  Power(t1,3)*(-26 + 7*t2) + 
                  Power(t1,2)*(-11 + 108*t2 - 33*Power(t2,2)) + 
                  t1*(-32 + 64*t2 - 138*Power(t2,2) + 37*Power(t2,3))))) \
+ Power(s,3)*(-32 + 116*t1 - 104*Power(t1,2) + 29*Power(t1,3) - 
            6*Power(t1,4) + 5*Power(t1,5) - 100*t2 + 66*t1*t2 - 
            13*Power(t1,2)*t2 + 50*Power(t1,3)*t2 - 24*Power(t1,4)*t2 + 
            3*Power(t1,5)*t2 - 2*Power(t1,6)*t2 + 38*Power(t2,2) - 
            21*t1*Power(t2,2) - 108*Power(t1,2)*Power(t2,2) + 
            32*Power(t1,3)*Power(t2,2) - 21*Power(t1,4)*Power(t2,2) + 
            9*Power(t1,5)*Power(t2,2) + 5*Power(t2,3) + 
            90*t1*Power(t2,3) - 2*Power(t1,2)*Power(t2,3) + 
            54*Power(t1,3)*Power(t2,3) - 15*Power(t1,4)*Power(t2,3) - 
            26*Power(t2,4) - 21*t1*Power(t2,4) - 
            66*Power(t1,2)*Power(t2,4) + 10*Power(t1,3)*Power(t2,4) + 
            10*Power(t2,5) + 39*t1*Power(t2,5) - 9*Power(t2,6) - 
            3*t1*Power(t2,6) + Power(t2,7) + 
            Power(s2,6)*(4 - 3*t1 + 5*t2) + 
            Power(s1,5)*(2*Power(s2,2) - 7*t1 - 6*Power(t1,2) + t2 + 
               6*t1*t2 - Power(t2,2) + s2*(-2 + 6*t1 + t2)) + 
            Power(s2,5)*(-8 + 11*Power(t1,2) + 21*Power(t2,2) - 
               t1*(3 + 35*t2)) - 
            Power(s1,4)*(-2 + 4*Power(s2,3) + 14*Power(t1,3) + 
               Power(t1,2)*(26 - 36*t2) - 6*t2 + 13*Power(t2,2) - 
               5*Power(t2,3) + Power(s2,2)*(-5 + t1 + 13*t2) - 
               s2*(-3 + 22*Power(t1,2) + t1*(14 - 22*t2) + t2 - 
                  7*Power(t2,2)) + 3*t1*(4 - 17*t2 + 9*Power(t2,2))) + 
            Power(s2,4)*(19 - 25*Power(t1,3) + 65*t2 + 22*Power(t2,2) - 
               7*Power(t2,3) + Power(t1,2)*(57 + 66*t2) - 
               t1*(57 + 77*t2 + 34*Power(t2,2))) + 
            Power(s2,3)*(-19 + 27*Power(t1,4) - 69*t2 - 
               21*Power(t2,2) + 87*Power(t2,3) - 14*Power(t2,4) - 
               Power(t1,3)*(89 + 61*t2) + 
               Power(t1,2)*(20 + 183*t2 + 27*Power(t2,2)) + 
               t1*(61 - 3*t2 - 181*Power(t2,2) + 21*Power(t2,3))) + 
            Power(s2,2)*(-36 - 12*Power(t1,5) - 111*t2 + 
               147*Power(t2,2) + 39*Power(t2,3) + 5*Power(t2,4) - 
               15*Power(t2,5) + 3*Power(t1,4)*(13 + 7*t2) + 
               2*Power(t1,3)*(43 - 85*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(-193 + 41*t2 + 228*Power(t2,2) - 
                  78*Power(t2,3)) + 
               t1*(137 + 52*t2 - 166*Power(t2,2) - 102*Power(t2,3) + 
                  60*Power(t2,4))) + 
            s2*(72 + 2*Power(t1,6) + 2*Power(t1,5)*(-4 + t2) + 212*t2 - 
               185*Power(t2,2) - 85*Power(t2,3) + 41*Power(t2,4) - 
               21*Power(t2,5) - 3*Power(t2,6) + 
               Power(t1,4)*(-24 + 45*t2 - 31*Power(t2,2)) + 
               Power(t1,3)*(-39 + 55*t2 - 66*Power(t2,2) + 
                  64*Power(t2,3)) + 
               Power(t1,2)*(227 - 159*t2 + 3*Power(t2,2) + 
                  8*Power(t2,3) - 56*Power(t2,4)) + 
               t1*(-254 - 42*t2 + 283*Power(t2,2) - 75*Power(t2,3) + 
                  42*Power(t2,4) + 22*Power(t2,5))) + 
            Power(s1,3)*(-9 + 6*Power(s2,4) - 6*Power(t1,4) + 18*t2 - 
               28*Power(t2,2) + 42*Power(t2,3) - 10*Power(t2,4) + 
               4*Power(t1,3)*(-9 + 10*t2) + 
               Power(s2,3)*(-1 - 4*t1 + 12*t2) + 
               Power(t1,2)*(-31 + 144*t2 - 72*Power(t2,2)) + 
               3*t1*(-2 + 15*t2 - 50*Power(t2,2) + 16*Power(t2,3)) + 
               Power(s2,2)*(-25 + 21*Power(t1,2) - 14*t2 + 
                  42*Power(t2,2) - 3*t1*(6 + 13*t2)) + 
               s2*(29 + 2*Power(t1,3) + Power(t1,2)*(40 - 28*t2) - 
                  12*t2 + 30*Power(t2,2) + 18*Power(t2,3) + 
                  t1*(34 - 90*t2 + 8*Power(t2,2)))) + 
            Power(s1,2)*(-26 + 2*Power(s2,5) + 6*Power(t1,5) + 
               Power(s2,4)*(1 + 32*t1 - 38*t2) + 29*t2 - 
               68*Power(t2,2) + 48*Power(t2,3) - 58*Power(t2,4) + 
               10*Power(t2,5) - 2*Power(t1,4)*(11 + 3*t2) + 
               Power(t1,3)*(-21 + 142*t2 - 28*Power(t2,2)) - 
               Power(s2,3)*(1 + 53*Power(t1,2) + t1*(37 - 53*t2) - 
                  79*t2 + 26*Power(t2,2)) + 
               4*Power(t1,2)*
                (-8 + 12*t2 - 69*Power(t2,2) + 15*Power(t2,3)) + 
               t1*(-17 + 106*t2 - 75*Power(t2,2) + 214*Power(t2,3) - 
                  42*Power(t2,4)) + 
               Power(s2,2)*(-25 + 71*Power(t1,3) + 
                  Power(t1,2)*(34 - 144*t2) + 133*t2 + 
                  18*Power(t2,2) - 68*Power(t2,3) + 
                  t1*(-115 - 46*t2 + 141*Power(t2,2))) + 
               s2*(49 - 36*Power(t1,4) - 199*t2 + 74*Power(t2,2) - 
                  76*Power(t2,3) - 22*Power(t2,4) + 
                  Power(t1,3)*(22 + 76*t2) - 
                  3*Power(t1,2)*(-15 + 42*t2 + 22*Power(t2,2)) + 
                  t1*(123 - 95*t2 + 180*Power(t2,2) + 48*Power(t2,3)))) \
- s1*(-72 + 6*Power(s2,6) - 4*Power(t1,6) - 10*t2 + 25*Power(t2,2) - 
               74*Power(t2,3) + 36*Power(t2,4) - 37*Power(t2,5) + 
               5*Power(t2,6) + Power(t1,5)*(5 + 18*t2) + 
               Power(s2,5)*(-9 - 28*t1 + 25*t2) - 
               3*Power(t1,4)*(1 + 17*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(50 + 15*t2 + 160*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-37 - 166*t2 + 15*Power(t2,2) - 
                  224*Power(t2,3) + 18*Power(t2,4)) + 
               t1*(48 + 12*t2 + 190*Power(t2,2) - 63*Power(t2,3) + 
                  147*Power(t2,4) - 18*Power(t2,5)) + 
               Power(s2,4)*(11 + 21*Power(t1,2) + 13*t2 - 
                  39*Power(t2,2) + t1*(19 + 5*t2)) + 
               Power(s2,3)*(25 + 16*Power(t1,3) - 72*t2 + 
                  165*Power(t2,2) - 32*Power(t2,3) - 
                  3*Power(t1,2)*(7 + 18*t2) + 
                  2*t1*(17 - 72*t2 + 35*Power(t2,2))) + 
               Power(s2,2)*(-139 - 35*Power(t1,4) + 186*t2 + 
                  147*Power(t2,2) + 14*Power(t2,3) - 52*Power(t2,4) + 
                  Power(t1,3)*(-36 + 127*t2) + 
                  Power(t1,2)*(152 + 188*t2 - 201*Power(t2,2)) + 
                  t1*(-98 - 293*t2 - 166*Power(t2,2) + 161*Power(t2,3))) \
+ s2*(178 + 20*Power(t1,5) + Power(t1,4)*(10 - 83*t2) - 136*t2 - 
                  255*Power(t2,2) + 100*Power(t2,3) - 68*Power(t2,4) - 
                  13*Power(t2,5) + 
                  2*Power(t1,3)*(-22 - 5*t2 + 71*Power(t2,2)) - 
                  Power(t1,2)*
                   (83 - 80*t2 + 78*Power(t2,2) + 128*Power(t2,3)) + 
                  2*t1*(7 + 175*t2 - 68*Power(t2,2) + 73*Power(t2,3) + 
                     31*Power(t2,4))))) + 
         s*(-72 + 188*t1 + Power(s1,7)*(s2 - t1)*t1 - 120*Power(t1,2) + 
            49*Power(t1,3) - 73*Power(t1,4) + 41*Power(t1,5) - 
            11*Power(t1,6) - 2*Power(t1,7) - 36*t2 - 154*t1*t2 + 
            147*Power(t1,2)*t2 + 130*Power(t1,3)*t2 - 
            129*Power(t1,4)*t2 + 58*Power(t1,5)*t2 + 13*Power(t1,6)*t2 - 
            Power(t1,7)*t2 + Power(s2,7)*(1 + 3*t1 - 5*t2)*t2 + 
            178*Power(t2,2) - 109*t1*Power(t2,2) - 
            214*Power(t1,2)*Power(t2,2) + 165*Power(t1,3)*Power(t2,2) - 
            93*Power(t1,4)*Power(t2,2) - 45*Power(t1,5)*Power(t2,2) + 
            6*Power(t1,6)*Power(t2,2) - 71*Power(t2,3) + 
            192*t1*Power(t2,3) - 61*Power(t1,2)*Power(t2,3) + 
            50*Power(t1,3)*Power(t2,3) + 85*Power(t1,4)*Power(t2,3) - 
            16*Power(t1,5)*Power(t2,3) - 35*Power(t2,4) - 
            42*t1*Power(t2,4) - 5*Power(t1,2)*Power(t2,4) - 
            90*Power(t1,3)*Power(t2,4) + 24*Power(t1,4)*Power(t2,4) + 
            26*Power(t2,5) + 8*t1*Power(t2,5) + 
            57*Power(t1,2)*Power(t2,5) - 21*Power(t1,3)*Power(t2,5) - 
            7*Power(t2,6) - 23*t1*Power(t2,6) + 
            10*Power(t1,2)*Power(t2,6) + 5*Power(t2,7) - 
            2*t1*Power(t2,7) + 
            Power(s2,6)*(1 + 8*t2 + 31*Power(t2,2) - 10*Power(t2,3) - 
               Power(t1,2)*(3 + 5*t2) + t1*(2 - 27*t2 + 19*Power(t2,2))) \
+ Power(s2,5)*(Power(t1,3)*(5 + 6*t2) + 
               Power(t1,2)*(26 + 34*t2 - 32*Power(t2,2)) + 
               t2*(51 - 52*t2 - 2*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(-31 - 35*t2 - 36*Power(t2,2) + 21*Power(t2,3))) - 
            Power(s2,4)*(-39 + 173*t2 + 52*Power(t2,2) + 
               121*Power(t2,3) + 4*Power(t2,4) - 2*Power(t2,5) + 
               2*Power(t1,4)*(3 + 5*t2) + 
               Power(t1,3)*(43 - 33*t2 - 31*Power(t2,2)) + 
               Power(t1,2)*(-15 + 94*t2 + 37*Power(t2,2) + 
                  18*Power(t2,3)) + 
               t1*(5 - 160*t2 - 272*Power(t2,2) - 12*Power(t2,3) + 
                  5*Power(t2,4))) + 
            Power(s2,3)*(-115 + 52*t2 - 219*Power(t2,2) - 
               61*Power(t2,3) - 266*Power(t2,4) + 47*Power(t2,5) + 
               5*Power(t2,6) + Power(t1,5)*(10 + 7*t2) - 
               Power(t1,4)*(17 + 47*t2 + 12*Power(t2,2)) + 
               Power(t1,3)*(214 + 152*t2 + 55*Power(t2,2) - 
                  13*Power(t2,3)) + 
               Power(t1,2)*(-252 - 516*t2 - 468*Power(t2,2) + 
                  Power(t2,3) + 39*Power(t2,4)) + 
               t1*(160 + 420*t2 + 323*Power(t2,2) + 603*Power(t2,3) - 
                  66*Power(t2,4) - 26*Power(t2,5))) + 
            Power(s2,2)*(40 - 69*t2 + 114*Power(t2,2) - 
               491*Power(t2,3) + 36*Power(t2,4) - 52*Power(t2,5) + 
               35*Power(t2,6) + 2*Power(t2,7) - Power(t1,6)*(7 + t2) + 
               Power(t1,5)*(27 + 3*t2 - 2*Power(t2,2)) + 
               Power(t1,4)*(-213 - 37*t2 + 36*Power(t2,2) + 
                  22*Power(t2,3)) - 
               2*Power(t1,3)*
                (-94 - 342*t2 - 75*Power(t2,2) + 56*Power(t2,3) + 
                  23*Power(t2,4)) + 
               Power(t1,2)*(-142 - 470*t2 - 969*Power(t2,2) - 
                  277*Power(t2,3) + 174*Power(t2,4) + 41*Power(t2,5)) + 
               t1*(107 - 94*t2 + 811*Power(t2,2) + 464*Power(t2,3) + 
                  189*Power(t2,4) - 129*Power(t2,5) - 16*Power(t2,6))) + 
            s2*(88 + Power(t1,7) + 10*t2 - 409*Power(t2,2) + 
               252*Power(t2,3) - 21*Power(t2,4) - 56*Power(t2,5) + 
               10*Power(t2,6) + 10*Power(t2,7) + 
               Power(t1,6)*(7 + 4*t2 + Power(t2,2)) - 
               Power(t1,5)*(-25 + 27*t2 + 31*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,4)*(4 - 281*t2 + 53*Power(t2,2) + 
                  85*Power(t2,3) + 14*Power(t2,4)) + 
               Power(t1,3)*(28 + 284*t2 + 507*Power(t2,2) - 
                  53*Power(t2,3) - 134*Power(t2,4) - 16*Power(t2,5)) + 
               Power(t1,2)*(139 - 556*t2 - 197*Power(t2,2) - 
                  401*Power(t2,3) + 30*Power(t2,4) + 121*Power(t2,5) + 
                  9*Power(t2,6)) - 
               2*t1*(146 - 251*t2 - 115*Power(t2,2) + 35*Power(t2,3) - 
                  103*Power(t2,4) + 10*Power(t2,5) + 28*Power(t2,6) + 
                  Power(t2,7))) + 
            Power(s1,6)*(Power(s2,3) + Power(s2,2)*(-3 + 2*t1 + 2*t2) + 
               t1*(-2 + Power(t1,2) + 6*t2 + t1*(-5 + 3*t2)) - 
               s2*(1 + 4*Power(t1,2) - 6*t2 + t1*(6 + 4*t2))) + 
            Power(s1,5)*(1 + 9*Power(t1,4) - 5*t2 - 5*Power(t2,2) - 
               5*Power(s2,3)*(-7 + 2*t2) - 3*Power(t1,3)*(5 + 4*t2) + 
               Power(t1,2)*(-5 + 31*t2) + 
               t1*(-22 + 36*t2 - 28*Power(t2,2)) + 
               Power(s2,2)*(-13 + 22*Power(t1,2) + t1*(17 - 2*t2) - 
                  14*t2 - 12*Power(t2,2)) + 
               s2*(11 - 27*Power(t1,3) + t2 - 40*Power(t2,2) + 
                  Power(t1,2)*(-34 + 25*t2) + 
                  t1*(35 + 66*t2 + 7*Power(t2,2)))) + 
            Power(s1,4)*(-8 + 6*Power(s2,5) + 11*Power(t1,5) + 
               Power(s2,4)*(-38 + t1 - 4*t2) + 25*t2 + 13*Power(t2,2) + 
               25*Power(t2,3) - 13*Power(t1,4)*(1 + 3*t2) + 
               Power(t1,3)*(-29 + 39*t2 + 38*Power(t2,2)) - 
               Power(t1,2)*(31 - 127*t2 + 64*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(-28 + 46*t2 - 147*Power(t2,2) + 50*Power(t2,3)) - 
               Power(s2,3)*(22 + Power(t1,2) + 121*t2 - 
                  35*Power(t2,2) + t1*(-62 + 22*t2)) + 
               Power(s2,2)*(-84 + 23*Power(t1,3) + 
                  Power(t1,2)*(128 - 49*t2) + 40*t2 + 
                  121*Power(t2,2) + 30*Power(t2,3) - 
                  t1*(104 + 193*t2 + 28*Power(t2,2))) + 
               s2*(21 - 32*Power(t1,4) - 64*t2 + 16*Power(t2,2) + 
                  110*Power(t2,3) + Power(t1,3)*(-86 + 96*t2) + 
                  Power(t1,2)*(116 + 259*t2 - 51*Power(t2,2)) - 
                  2*t1*(-59 + 109*t2 + 130*Power(t2,2) + 5*Power(t2,3)))\
) + Power(s1,3)*(-9 - 2*Power(s2,6) + 4*Power(t1,6) + 
               Power(s2,5)*(8 + 26*t1 - 34*t2) + 
               Power(t1,5)*(1 - 30*t2) + 69*t2 - 107*Power(t2,2) - 
               2*Power(t2,3) - 50*Power(t2,4) + 
               Power(s2,4)*(58 - 38*t1 - 45*Power(t1,2) + 106*t2 + 
                  28*t1*t2 + 10*Power(t2,2)) + 
               Power(t1,4)*(-79 - t2 + 63*Power(t2,2)) + 
               Power(t1,3)*(26 + 259*t2 - 6*Power(t2,2) - 
                  52*Power(t2,3)) + 
               t1*(-23 + 131*t2 - 14*Power(t2,2) + 268*Power(t2,3) - 
                  40*Power(t2,4)) + 
               Power(t1,2)*(-67 + 14*t2 - 408*Power(t2,2) + 
                  46*Power(t2,3) + 15*Power(t2,4)) + 
               Power(s2,3)*(-35 + 50*Power(t1,3) + 350*t2 + 
                  106*Power(t2,2) - 60*Power(t2,3) - 
                  Power(t1,2)*(1 + 66*t2) + 
                  2*t1*(-81 - 74*t2 + 46*Power(t2,2))) + 
               Power(s2,2)*(165 - 25*Power(t1,4) + 256*t2 + 
                  10*Power(t2,2) - 284*Power(t2,3) - 40*Power(t2,4) + 
                  Power(t1,3)*(194 + 7*t2) - 
                  2*Power(t1,2)*(63 + 282*t2 + 13*Power(t2,2)) + 
                  t1*(-226 + 137*t2 + 606*Power(t2,2) + 92*Power(t2,3))\
) + s2*(35 - 4*Power(t1,5) - 131*t2 + 182*Power(t2,2) - 
                  54*Power(t2,3) - 160*Power(t2,4) + 
                  13*Power(t1,4)*(-8 + 5*t2) + 
                  Power(t1,3)*(216 + 430*t2 - 110*Power(t2,2)) + 
                  Power(t1,2)*
                   (147 - 528*t2 - 694*Power(t2,2) + 34*Power(t2,3)) + 
                  t1*(12 - 382*t2 + 464*Power(t2,2) + 500*Power(t2,3) + 
                     15*Power(t2,4)))) + 
            Power(s1,2)*(14 - 5*Power(s2,7) + 
               3*Power(s2,6)*(5 + 5*t1 - 2*t2) - 
               6*Power(t1,6)*(-1 + t2) - 11*t2 - 149*Power(t2,2) + 
               161*Power(t2,3) - 22*Power(t2,4) + 50*Power(t2,5) + 
               Power(t1,5)*(-83 - 26*t2 + 27*Power(t2,2)) + 
               Power(t1,4)*(38 + 294*t2 + 65*Power(t2,2) - 
                  45*Power(t2,3)) + 
               Power(t1,3)*(-21 - 60*t2 - 521*Power(t2,2) - 
                  66*Power(t2,3) + 33*Power(t2,4)) + 
               Power(t1,2)*(-97 + 92*t2 + 60*Power(t2,2) + 
                  512*Power(t2,3) + 11*Power(t2,4) - 9*Power(t2,5)) + 
               t1*(39 + 190*t2 - 220*Power(t2,2) - 14*Power(t2,3) - 
                  252*Power(t2,4) + 10*Power(t2,5)) - 
               Power(s2,5)*(11 + 11*Power(t1,2) + 30*t2 - 
                  54*Power(t2,2) + t1*(-2 + 35*t2)) - 
               Power(s2,4)*(7 + 13*Power(t1,3) + 
                  Power(t1,2)*(18 - 85*t2) + 190*t2 + 
                  102*Power(t2,2) + 6*Power(t2,3) + 
                  4*t1*(-3 - 23*t2 + 16*Power(t2,2))) + 
               Power(s2,3)*(-203 + 34*Power(t1,4) + 37*t2 - 
                  900*Power(t2,2) + 58*Power(t2,3) + 55*Power(t2,4) - 
                  Power(t1,3)*(37 + 133*t2) + 
                  Power(t1,2)*(-6 + t2 + 174*Power(t2,2)) + 
                  t1*(203 + 825*t2 + 44*Power(t2,2) - 144*Power(t2,3))) \
+ Power(s2,2)*(255 - 26*Power(t1,5) - 878*t2 - 224*Power(t2,2) - 
                  140*Power(t2,3) + 311*Power(t2,4) + 30*Power(t2,5) + 
                  Power(t1,4)*(111 + 91*t2) - 
                  3*Power(t1,3)*(56 + 164*t2 + 43*Power(t2,2)) + 
                  Power(t1,2)*
                   (-597 + 15*t2 + 918*Power(t2,2) + 152*Power(t2,3)) \
+ t1*(369 + 970*t2 + 227*Power(t2,2) - 830*Power(t2,3) - 
                     118*Power(t2,4))) + 
               s2*(-263 + 6*Power(t1,6) + 134*t2 + 178*Power(t2,2) - 
                  272*Power(t2,3) + 71*Power(t2,4) + 130*Power(t2,5) - 
                  Power(t1,5)*(55 + 8*t2) + 
                  Power(t1,4)*(228 + 303*t2 - 20*Power(t2,2)) + 
                  Power(t1,3)*
                   (147 - 579*t2 - 736*Power(t2,2) + 24*Power(t2,3)) + 
                  Power(t1,2)*
                   (59 - 615*t2 + 738*Power(t2,2) + 868*Power(t2,3) + 
                     14*Power(t2,4)) - 
                  2*t1*(-34 + 24*t2 - 308*Power(t2,2) + 
                     217*Power(t2,3) + 255*Power(t2,4) + 8*Power(t2,5)))\
) - s1*(-120 - 2*Power(t1,7) + Power(s2,7)*(1 + 3*t1 - 10*t2) + 216*t2 - 
               91*Power(t2,2) - 123*Power(t2,3) + 106*Power(t2,4) - 
               23*Power(t2,5) + 25*Power(t2,6) - 
               2*Power(t1,6)*(-16 - 6*t2 + Power(t2,2)) - 
               Power(s2,6)*(5 + 6*Power(t1,2) + t1*(13 - 34*t2) - 
                  46*t2 + 18*Power(t2,2)) + 
               Power(t1,5)*(18 - 139*t2 - 41*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,4)*(-94 - 53*t2 + 300*Power(t2,2) + 
                  75*Power(t2,3) - 12*Power(t2,4)) + 
               Power(t1,3)*(117 + 183*t2 + 16*Power(t2,2) - 
                  381*Power(t2,3) - 69*Power(t2,4) + 8*Power(t2,5)) + 
               t1*(70 - 116*t2 + 359*Power(t2,2) - 159*Power(t2,3) + 
                  4*Power(t2,4) - 120*Power(t2,5) - 4*Power(t2,6)) + 
               Power(t1,2)*(7 - 319*t2 - 36*Power(t2,2) + 
                  38*Power(t2,3) + 283*Power(t2,4) + 29*Power(t2,5) - 
                  2*Power(t2,6)) + 
               Power(s2,5)*(54 + 10*Power(t1,3) - 73*t2 - 
                  24*Power(t2,2) + 30*Power(t2,3) - 
                  Power(t1,2)*(23 + 45*t2) + 
                  t1*(15 - 22*t2 + 12*Power(t2,2))) + 
               Power(s2,4)*(-128 - 16*Power(t1,4) - 67*t2 - 
                  253*Power(t2,2) - 38*Power(t2,3) + 2*Power(t2,4) + 
                  Power(t1,3)*(119 + 25*t2) + 
                  Power(t1,2)*(-115 - 101*t2 + 22*Power(t2,2)) + 
                  t1*(56 + 331*t2 + 66*Power(t2,2) - 40*Power(t2,3))) + 
               Power(s2,3)*(41 + 11*Power(t1,5) - 363*t2 - 
                  59*Power(t2,2) - 838*Power(t2,3) + 125*Power(t2,4) + 
                  26*Power(t2,5) + Power(t1,4)*(-102 + 13*t2) + 
                  Power(t1,3)*(18 + 84*t2 - 96*Power(t2,2)) + 
                  Power(t1,2)*
                   (-134 - 504*t2 + Power(t2,2) + 146*Power(t2,3)) - 
                  2*t1*(-117 - 220*t2 - 633*Power(t2,2) + 
                     54*Power(t2,3) + 50*Power(t2,4))) - 
               Power(s2,2)*(51 + 2*Power(t1,6) - 357*t2 + 
                  1204*Power(t2,2) + 16*Power(t2,3) + 155*Power(t2,4) - 
                  166*Power(t2,5) - 12*Power(t2,6) + 
                  Power(t1,5)*(-12 + 23*t2) - 
                  Power(t1,4)*(164 + 105*t2 + 88*Power(t2,2)) + 
                  Power(t1,3)*
                   (-216 + 52*t2 + 410*Power(t2,2) + 145*Power(t2,3)) + 
                  Power(t1,2)*
                   (266 + 1388*t2 + 388*Power(t2,2) - 
                     656*Power(t2,3) - 140*Power(t2,4)) + 
                  t1*(57 - 1085*t2 - 1208*Power(t2,2) - 
                     449*Power(t2,3) + 529*Power(t2,4) + 70*Power(t2,5))\
) + s2*(110 - 734*t2 + 421*Power(t2,2) + 47*Power(t2,3) - 
                  199*Power(t2,4) + 43*Power(t2,5) + 56*Power(t2,6) + 
                  Power(t1,6)*(8 + 6*t2) - 
                  Power(t1,5)*(129 + 76*t2 + 18*Power(t2,2)) + 
                  Power(t1,4)*
                   (-90 + 319*t2 + 284*Power(t2,2) + 27*Power(t2,3)) + 
                  Power(t1,3)*
                   (222 + 604*t2 - 416*Power(t2,2) - 526*Power(t2,3) - 
                     33*Power(t2,4)) + 
                  Power(t1,2)*
                   (-377 - 273*t2 - 869*Power(t2,2) + 356*Power(t2,3) + 
                     520*Power(t2,4) + 27*Power(t2,5)) + 
                  t1*(192 + 498*t2 - 106*Power(t2,2) + 558*Power(t2,3) - 
                     173*Power(t2,4) - 266*Power(t2,5) - 9*Power(t2,6))))\
) + Power(s,2)*(76 - 202*t1 + 170*Power(t1,2) - 102*Power(t1,3) + 
            56*Power(t1,4) - 22*Power(t1,5) + 9*Power(t1,6) - 
            Power(t1,7) + Power(s2,7)*(-2 + 2*t1 - 3*t2) + 106*t2 + 
            20*Power(t1,2)*t2 - 154*Power(t1,3)*t2 + 118*Power(t1,4)*t2 - 
            52*Power(t1,5)*t2 + 6*Power(t1,6)*t2 - 146*Power(t2,2) + 
            56*t1*Power(t2,2) + 232*Power(t1,2)*Power(t2,2) - 
            200*Power(t1,3)*Power(t2,2) + 113*Power(t1,4)*Power(t2,2) - 
            15*Power(t1,5)*Power(t2,2) - Power(t1,6)*Power(t2,2) + 
            26*Power(t2,3) - 186*t1*Power(t2,3) + 
            118*Power(t1,2)*Power(t2,3) - 130*Power(t1,3)*Power(t2,3) + 
            16*Power(t1,4)*Power(t2,3) + 5*Power(t1,5)*Power(t2,3) + 
            52*Power(t2,4) + 2*t1*Power(t2,4) + 
            97*Power(t1,2)*Power(t2,4) + Power(t1,3)*Power(t2,4) - 
            10*Power(t1,4)*Power(t2,4) - 16*Power(t2,5) - 
            50*t1*Power(t2,5) - 18*Power(t1,2)*Power(t2,5) + 
            10*Power(t1,3)*Power(t2,5) + 13*Power(t2,6) + 
            15*t1*Power(t2,6) - 5*Power(t1,2)*Power(t2,6) - 
            4*Power(t2,7) + t1*Power(t2,7) - 
            Power(s2,6)*(-3 + 5*Power(t1,2) + t1*(2 - 11*t2) - 5*t2 + 
               Power(t2,2)) - 
            Power(s1,6)*(Power(s2,2) + s2*(-1 + 4*t1 + t2) + 
               t1*(-3 - 4*t1 + 2*t2)) + 
            Power(s2,5)*(9 + 8*Power(t1,3) - 53*t2 - 46*Power(t2,2) + 
               26*Power(t2,3) - Power(t1,2)*(9 + 19*t2) + 
               t1*(12 + 55*t2 - 18*Power(t2,2))) + 
            Power(s2,4)*(-70 - 10*Power(t1,4) + 2*t2 + 21*Power(t2,2) - 
               17*Power(t2,3) + 13*Power(t2,4) + 
               6*Power(t1,3)*(8 + t2) + 
               Power(t1,2)*(-110 - 83*t2 + 56*Power(t2,2)) + 
               t1*(86 + 106*t2 + 52*Power(t2,2) - 65*Power(t2,3))) + 
            Power(s2,3)*(98 + 6*Power(t1,5) + 50*t2 + 12*Power(t2,2) - 
               104*Power(t2,3) + 99*Power(t2,4) + 8*Power(t2,5) + 
               Power(t1,4)*(-31 + 15*t2) + 
               Power(t1,3)*(23 + 14*t2 - 91*Power(t2,2)) + 
               Power(t1,2)*(112 - 87*t2 + 86*Power(t2,2) + 
                  121*Power(t2,3)) - 
               t1*(104 + 158*t2 - 168*Power(t2,2) + 168*Power(t2,3) + 
                  59*Power(t2,4))) - 
            Power(s2,2)*(-22 + Power(t1,6) - 60*t2 + 232*Power(t2,2) - 
               260*Power(t2,3) - 77*Power(t2,4) - 58*Power(t2,5) + 
               4*Power(t2,6) + Power(t1,5)*(9 + 13*t2) - 
               2*Power(t1,4)*(49 + 27*t2 + 29*Power(t2,2)) + 
               2*Power(t1,3)*
                (97 + 67*t2 + 107*Power(t2,2) + 41*Power(t2,3)) - 
               Power(t1,2)*(142 + 186*t2 + 213*Power(t2,2) + 
                  360*Power(t2,3) + 43*Power(t2,4)) + 
               t1*(174 - 136*t2 + 246*Power(t2,2) + 254*Power(t2,3) + 
                  249*Power(t2,4) + Power(t2,5))) + 
            s2*(-136 - 140*t2 + 494*Power(t2,2) - 10*Power(t2,3) - 
               47*Power(t2,4) + 61*Power(t2,5) + 7*Power(t2,6) - 
               3*Power(t2,7) + 3*Power(t1,6)*(2 + t2) - 
               Power(t1,5)*(35 + 35*t2 + 11*Power(t2,2)) + 
               Power(t1,4)*(11 + 154*t2 + 117*Power(t2,2) + 
                  11*Power(t2,3)) + 
               Power(t1,3)*(80 - 284*t2 - 277*Power(t2,2) - 
                  208*Power(t2,3) + 6*Power(t2,4)) + 
               Power(t1,2)*(-222 + 446*t2 + 336*Power(t2,2) + 
                  293*Power(t2,3) + 182*Power(t2,4) - 19*Power(t2,5)) + 
               t1*(364 - 338*t2 - 516*Power(t2,2) - 16*Power(t2,3) - 
                  196*Power(t2,4) - 69*Power(t2,5) + 13*Power(t2,6))) + 
            Power(s1,5)*(-3 + 6*Power(t1,3) + t2 + 4*Power(t2,2) + 
               Power(s2,2)*(-4 - 2*t1 + 5*t2) - 
               3*Power(t1,2)*(-5 + 6*t2) + 
               t1*(11 - 34*t2 + 9*Power(t2,2)) + 
               s2*(7 - 6*Power(t1,2) - 12*t2 + 8*Power(t2,2) + 
                  t1*(-2 + 11*t2))) - 
            Power(s1,4)*(-8 + Power(s2,4) + 6*Power(t1,4) + 
               Power(s2,3)*(17 - 10*t2) + 5*t2 - 9*Power(t2,2) + 
               20*Power(t2,3) + 3*Power(t1,3)*(-9 + 2*t2) + 
               Power(t1,2)*(-20 + 110*t2 - 27*Power(t2,2)) + 
               Power(s2,2)*(-30 + 38*Power(t1,2) + t1*(22 - 21*t2) - 
                  64*t2 + 14*Power(t2,2)) + 
               t1*(-18 + 50*t2 - 121*Power(t2,2) + 15*Power(t2,3)) + 
               s2*(20 - 30*Power(t1,3) + 8*t2 - 45*Power(t2,2) + 
                  25*Power(t2,3) + Power(t1,2)*(-6 + 17*t2) + 
                  t1*(32 + 19*t2 - 9*Power(t2,2)))) + 
            Power(s1,3)*(20 - 9*Power(s2,5) - 14*Power(t1,5) - 78*t2 + 
               49*Power(t2,2) - 46*Power(t2,3) + 40*Power(t2,4) + 
               20*Power(t1,4)*(1 + 2*t2) + 
               Power(s2,4)*(29 - 13*t1 + 12*t2) + 
               Power(t1,3)*(3 - 114*t2 - 28*Power(t2,2)) + 
               Power(t1,2)*(44 - 109*t2 + 258*Power(t2,2) - 
                  8*Power(t2,3)) + 
               t1*(44 - 65*t2 + 134*Power(t2,2) - 204*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,3)*(-17 + 29*Power(t1,2) - 20*t2 - 
                  38*Power(t2,2) + t1*(3 + 23*t2)) + 
               Power(s2,2)*(93 - 71*Power(t1,3) - 220*t2 - 
                  226*Power(t2,2) + 26*Power(t2,3) + 
                  7*Power(t1,2)*(-21 + 17*t2) + 
                  t1*(177 + 303*t2 - 50*Power(t2,2))) + 
               s2*(-116 + 56*Power(t1,4) + Power(t1,3)*(52 - 136*t2) + 
                  142*t2 - 79*Power(t2,2) - 80*Power(t2,3) + 
                  40*Power(t2,4) + 
                  2*Power(t1,2)*(-31 - 73*t2 + 53*Power(t2,2)) + 
                  t1*(-117 + 229*t2 + 138*Power(t2,2) - 66*Power(t2,3)))) \
+ Power(s1,2)*(24 + 8*Power(s2,6) - 6*Power(t1,6) - 48*t2 + 
               184*Power(t2,2) - 87*Power(t2,3) + 74*Power(t2,4) - 
               40*Power(t2,5) + Power(t1,5)*(3 + 36*t2) + 
               Power(s2,5)*(-26 - 46*t1 + 54*t2) + 
               Power(s2,4)*(-3 + 62*Power(t1,2) + t1*(29 - 52*t2) - 
                  61*t2 - 8*Power(t2,2)) - 
               Power(t1,4)*(1 + 28*t2 + 72*Power(t2,2)) + 
               4*Power(t1,3)*
                (-3 - 29*t2 + 37*Power(t2,2) + 15*Power(t2,3)) + 
               Power(t1,2)*(92 + 27*t2 + 255*Power(t2,2) - 
                  264*Power(t2,3) - 18*Power(t2,4)) + 
               t1*(-18 - 260*t2 + 78*Power(t2,2) - 212*Power(t2,3) + 
                  181*Power(t2,4)) + 
               Power(s2,3)*(55 - 42*Power(t1,3) - 139*t2 + 
                  190*Power(t2,2) + 54*Power(t2,3) + 
                  7*Power(t1,2)*(8 + 9*t2) + 
                  t1*(112 - 148*t2 - 105*Power(t2,2))) + 
               Power(s2,2)*(-168 - 2*Power(t1,4) + 61*t2 + 
                  427*Power(t2,2) + 334*Power(t2,3) - 29*Power(t2,4) + 
                  Power(t1,3)*(-199 + 68*t2) + 
                  Power(t1,2)*(119 + 672*t2 - 81*Power(t2,2)) + 
                  t1*(-30 - 582*t2 - 789*Power(t2,2) + 44*Power(t2,3))) \
+ s2*(110 + 18*Power(t1,5) + Power(t1,4)*(79 - 109*t2) + 334*t2 - 
                  271*Power(t2,2) + 215*Power(t2,3) + 75*Power(t2,4) - 
                  35*Power(t2,5) + 
                  Power(t1,3)*(-47 - 330*t2 + 188*Power(t2,2)) + 
                  Power(t1,2)*
                   (1 + 426*t2 + 456*Power(t2,2) - 156*Power(t2,3)) + 
                  2*t1*(-84 + 60*t2 - 279*Power(t2,2) - 
                     140*Power(t2,3) + 47*Power(t2,4)))) + 
            s1*(-148 + 3*Power(s2,7) + 124*t2 + 2*Power(t2,2) - 
               166*Power(t2,3) + 62*Power(t2,4) - 51*Power(t2,5) + 
               20*Power(t2,6) + Power(t1,6)*(-3 + 6*t2) - 
               Power(s2,6)*(5 + 7*t1 + 7*t2) + 
               Power(t1,5)*(14 + 16*t2 - 27*Power(t2,2)) + 
               Power(t1,4)*(-77 - 110*t2 - 8*Power(t2,2) + 
                  48*Power(t2,3)) + 
               Power(t1,3)*(156 + 237*t2 + 243*Power(t2,2) - 
                  62*Power(t2,3) - 42*Power(t2,4)) + 
               Power(t1,2)*(-50 - 392*t2 - 189*Power(t2,2) - 
                  263*Power(t2,3) + 119*Power(t2,4) + 18*Power(t2,5)) + 
               t1*(84 - 6*t2 + 402*Power(t2,2) - 33*Power(t2,3) + 
                  167*Power(t2,4) - 82*Power(t2,5) - 3*Power(t2,6)) + 
               Power(s2,5)*(4 - 2*Power(t1,2) + 72*t2 - 71*Power(t2,2) + 
                  t1*(-9 + 68*t2)) + 
               Power(s2,4)*(30 + 36*Power(t1,3) - 65*t2 + 
                  49*Power(t2,2) - 16*Power(t2,3) - 
                  27*Power(t1,2)*(3 + 5*t2) + 
                  t1*(60 - 37*t2 + 130*Power(t2,2))) + 
               Power(s2,3)*(6 - 55*Power(t1,4) - 38*t2 + 
                  260*Power(t2,2) - 252*Power(t2,3) - 34*Power(t2,4) + 
                  Power(t1,3)*(179 + 161*t2) + 
                  Power(t1,2)*(11 - 240*t2 - 213*Power(t2,2)) + 
                  t1*(-119 - 199*t2 + 313*Power(t2,2) + 141*Power(t2,3))) \
+ Power(s2,2)*(-148 + 31*Power(t1,5) + 476*t2 - 414*Power(t2,2) - 
                  314*Power(t2,3) - 226*Power(t2,4) + 17*Power(t2,5) - 
                  Power(t1,4)*(139 + 78*t2) + 
                  Power(t1,3)*(-62 + 493*t2 + 85*Power(t2,2)) - 
                  Power(t1,2)*
                   (-259 + 301*t2 + 885*Power(t2,2) + 43*Power(t2,3)) + 
                  t1*(-74 + 71*t2 + 659*Power(t2,2) + 757*Power(t2,3) - 
                     12*Power(t2,4))) + 
               s2*(-6*Power(t1,6) + Power(t1,5)*(42 + t2) + 
                  Power(t1,4)*(1 - 226*t2 + 42*Power(t2,2)) + 
                  Power(t1,3)*
                   (81 + 295*t2 + 486*Power(t2,2) - 88*Power(t2,3)) + 
                  Power(t1,2)*
                   (-410 - 302*t2 - 657*Power(t2,2) - 498*Power(t2,3) + 
                     92*Power(t2,4)) + 
                  t1*(140 + 796*t2 + 13*Power(t2,2) + 557*Power(t2,3) + 
                     232*Power(t2,4) - 57*Power(t2,5)) + 
                  2*(129 - 340*t2 - 104*Power(t2,2) + 98*Power(t2,3) - 
                     98*Power(t2,4) - 18*Power(t2,5) + 8*Power(t2,6))))))*
       T3(1 - s + s1 - t2,1 - s + s2 - t1))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s1 + t1 - t2)*
       Power(-s1 + s2 - t1 + t2,2)*
       Power(1 - s + s*s2 - s1*s2 - t1 + s2*t2,2)) + 
    (8*(24 - 8*s2 - 96*t1 + 32*s2*t1 + 144*Power(t1,2) - 
         48*s2*Power(t1,2) - 96*Power(t1,3) + 32*s2*Power(t1,3) + 
         24*Power(t1,4) - 8*s2*Power(t1,4) + 
         Power(s1,6)*(-(Power(s2,2)*(-3 + t1)) - (-2 + t1)*Power(t1,2) + 
            s2*t1*(3 + 2*t1)) - Power(s,7)*(t1 - t2)*(1 - s2 + t1 - t2) + 
         2*t2 + 28*s2*t2 - 16*Power(s2,2)*t2 - 88*s2*t1*t2 + 
         48*Power(s2,2)*t1*t2 - 12*Power(t1,2)*t2 + 
         96*s2*Power(t1,2)*t2 - 48*Power(s2,2)*Power(t1,2)*t2 + 
         16*Power(t1,3)*t2 - 40*s2*Power(t1,3)*t2 + 
         16*Power(s2,2)*Power(t1,3)*t2 - 6*Power(t1,4)*t2 + 
         4*s2*Power(t1,4)*t2 + 6*Power(t2,2) + 6*s2*Power(t2,2) - 
         15*Power(s2,2)*Power(t2,2) - 8*Power(s2,3)*Power(t2,2) - 
         33*t1*Power(t2,2) - 11*s2*t1*Power(t2,2) + 
         21*Power(s2,2)*t1*Power(t2,2) + 16*Power(s2,3)*t1*Power(t2,2) + 
         55*Power(t1,2)*Power(t2,2) + 5*s2*Power(t1,2)*Power(t2,2) + 
         3*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         8*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         35*Power(t1,3)*Power(t2,2) - s2*Power(t1,3)*Power(t2,2) - 
         9*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         7*Power(t1,4)*Power(t2,2) + s2*Power(t1,4)*Power(t2,2) - 
         13*Power(t2,3) + 16*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) - 
         18*Power(s2,3)*Power(t2,3) + 7*t1*Power(t2,3) - 
         61*s2*t1*Power(t2,3) - 16*Power(s2,2)*t1*Power(t2,3) + 
         12*Power(s2,3)*t1*Power(t2,3) + 12*Power(t1,2)*Power(t2,3) + 
         58*s2*Power(t1,2)*Power(t2,3) + 
         17*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         6*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         5*Power(t1,3)*Power(t2,3) - 13*s2*Power(t1,3)*Power(t2,3) - 
         2*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         Power(t1,4)*Power(t2,3) + 16*Power(t2,4) - 7*s2*Power(t2,4) + 
         20*Power(s2,2)*Power(t2,4) - 3*Power(s2,3)*Power(t2,4) + 
         Power(s2,4)*Power(t2,4) - 24*t1*Power(t2,4) - 
         27*s2*t1*Power(t2,4) - 30*Power(s2,2)*t1*Power(t2,4) - 
         10*Power(s2,3)*t1*Power(t2,4) - Power(s2,4)*t1*Power(t2,4) + 
         12*Power(t1,2)*Power(t2,4) + 12*s2*Power(t1,2)*Power(t2,4) + 
         6*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         2*s2*Power(t1,3)*Power(t2,4) - Power(t2,5) + 16*s2*Power(t2,5) + 
         10*Power(s2,2)*Power(t2,5) + 6*Power(s2,3)*Power(t2,5) - 
         3*t1*Power(t2,5) - 11*s2*t1*Power(t2,5) - 
         7*Power(s2,2)*t1*Power(t2,5) - s2*Power(t1,2)*Power(t2,5) - 
         Power(s2,2)*Power(t1,2)*Power(t2,5) + 2*Power(t2,6) + 
         5*s2*Power(t2,6) + Power(s2,2)*Power(t2,6) - t1*Power(t2,6) + 
         Power(s2,2)*t1*Power(t2,6) + 
         Power(s,6)*(1 - Power(t1,3) - 
            Power(s2,2)*(-2 + s1 + 4*t1 - 3*t2) + 
            Power(t1,2)*(2 + 6*s1 - 2*t2) - 7*t2 - 4*s1*t2 + 
            8*Power(t2,2) + 4*s1*Power(t2,2) - 4*Power(t2,3) + 
            s2*(-3 + s1 - 6*s1*t1 + 4*Power(t1,2) + 5*t2 + 3*s1*t2 - 
               4*t1*t2) + t1*(3 + 7*s1 - 10*t2 - 10*s1*t2 + 
               7*Power(t2,2))) - 
         Power(s1,5)*(2*Power(s2,3)*(2 + t1) - 
            2*Power(s2,2)*(-4 + t1 + 2*Power(t1,2) - 8*t2 + 2*t1*t2) + 
            t1*(3 + 4*t2 - 4*Power(t1,2)*t2 + t1*(-7 + 6*t2)) + 
            s2*(2*Power(t1,3) + 3*(2 + t2) + 4*Power(t1,2)*(-3 + 2*t2) + 
               t1*(8 + 19*t2))) + 
         Power(s1,4)*(3 - Power(s2,4)*(-1 + t1) - Power(t1,4) + 3*t2 + 
            2*Power(t2,2) + 2*Power(s2,3)*
             (1 + Power(t1,2) + 4*t1*(-2 + t2) + 11*t2) + 
            Power(t1,3)*(17 - 6*Power(t2,2)) + 
            4*Power(t1,2)*(-6 - 6*t2 + Power(t2,2)) + 
            t1*(9 + t2 + 15*Power(t2,2)) - 
            Power(s2,2)*(-16 + Power(t1,3) - 44*t2 - 35*Power(t2,2) + 
               3*Power(t1,2)*(-4 + 5*t2) + 
               t1*(31 + 19*t2 + 5*Power(t2,2))) + 
            s2*(16 + 42*t2 + 17*Power(t2,2) + Power(t1,3)*(2 + 7*t2) + 
               Power(t1,2)*(7 - 45*t2 + 12*Power(t2,2)) + 
               t1*(-45 + 16*t2 + 46*Power(t2,2)))) + 
         Power(s1,3)*(4*Power(s2,4)*(-1 + t1)*t2 + 
            Power(t1,4)*(2 + 3*t2) + 
            t1*(43 + 4*t2 + 18*Power(t2,2) - 20*Power(t2,3)) + 
            Power(t1,3)*(11 - 46*t2 + 4*Power(t2,3)) - 
            2*(4 + 13*t2 + 4*Power(t2,2) + 4*Power(t2,3)) + 
            Power(t1,2)*(-48 + 49*t2 + 30*Power(t2,2) + 4*Power(t2,3)) - 
            Power(s2,3)*(Power(t1,2)*(6 + 7*t2) + 
               2*t1*(6 - 29*t2 + 6*Power(t2,2)) + 
               3*(-6 + t2 + 16*Power(t2,2))) + 
            Power(s2,2)*(3*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(-24 - 38*t2 + 22*Power(t2,2)) + 
               t1*(27 + 115*t2 + 52*Power(t2,2)) - 
               2*(3 + 32*t2 + 47*Power(t2,2) + 20*Power(t2,3))) - 
            s2*(20 + Power(t1,4) + 47*t2 + 106*Power(t2,2) + 
               38*Power(t2,3) + 
               Power(t1,3)*(-20 + 10*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(73 + 35*t2 - 64*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-74 - 172*t2 - 11*Power(t2,2) + 54*Power(t2,3)))) + 
         Power(s1,2)*(8 + 3*t2 + 59*Power(t2,2) - 
            6*Power(s2,4)*(-1 + t1)*Power(t2,2) + 6*Power(t2,3) + 
            12*Power(t2,4) + Power(t1,4)*(9 - 5*t2 - 3*Power(t2,2)) + 
            Power(t1,2)*(67 + 108*t2 - 14*Power(t2,2) - 
               16*Power(t2,3) - 6*Power(t2,4)) - 
            Power(t1,3)*(43 + 27*t2 - 41*Power(t2,2) + Power(t2,4)) + 
            t1*(-41 - 79*t2 - 59*Power(t2,2) - 30*Power(t2,3) + 
               10*Power(t2,4)) + 
            Power(s2,3)*(-8 - 54*t2 - 3*Power(t2,2) + 52*Power(t2,3) + 
               Power(t1,2)*(-8 + 18*t2 + 9*Power(t2,2)) + 
               2*t1*(8 + 18*t2 - 39*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,2)*(-15 + 13*t2 + 100*Power(t2,2) + 
               98*Power(t2,3) + 25*Power(t2,4) - 
               Power(t1,3)*(9 + 8*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(3 + 65*t2 + 46*Power(t2,2) - 
                  16*Power(t2,3)) + 
               t1*(21 - 70*t2 - 167*Power(t2,2) - 62*Power(t2,3) + 
                  5*Power(t2,4))) + 
            s2*(6 + 56*t2 + 39*Power(t2,2) + 126*Power(t2,3) + 
               42*Power(t2,4) + Power(t1,4)*(1 + 2*t2) + 
               Power(t1,3)*(-1 - 53*t2 + 16*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(5 + 204*t2 + 61*Power(t2,2) - 
                  42*Power(t2,3) + 2*Power(t2,4)) + 
               t1*(-11 - 209*t2 - 236*Power(t2,2) - 49*Power(t2,3) + 
                  31*Power(t2,4)))) + 
         s1*(4*Power(s2,4)*(-1 + t1)*Power(t2,3) + 
            Power(t1,3)*(-16 + 78*t2 + 21*Power(t2,2) - 
               12*Power(t2,3)) + 
            Power(t1,4)*(6 - 16*t2 + 4*Power(t2,2) + Power(t2,3)) + 
            t1*t2*(74 + 29*t2 + 70*Power(t2,2) + 17*Power(t2,3)) + 
            Power(t1,2)*(12 - 122*t2 - 72*Power(t2,2) - 
               23*Power(t2,3) + 3*Power(t2,4) + 2*Power(t2,5)) - 
            2*(1 + 7*t2 - 9*Power(t2,2) + 26*Power(t2,3) + 
               4*Power(t2,5)) + 
            Power(s2,3)*t2*(16 + 54*t2 + 7*Power(t2,2) - 
               28*Power(t2,3) + 
               Power(t1,2)*(16 - 18*t2 - 5*Power(t2,2)) - 
               2*t1*(16 + 18*t2 - 23*Power(t2,2) + Power(t2,3))) + 
            Power(s2,2)*(Power(t1,3)*
                (-16 + 18*t2 + 7*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(48 - 6*t2 - 58*Power(t2,2) - 
                  26*Power(t2,3) + 6*Power(t2,4)) + 
               t1*(-48 - 42*t2 + 59*Power(t2,2) + 113*Power(t2,3) + 
                  34*Power(t2,4) - 4*Power(t2,5)) - 
               2*(-8 - 15*t2 + 4*Power(t2,2) + 36*Power(t2,3) + 
                  25*Power(t2,4) + 4*Power(t2,5))) - 
            s2*(28 + 12*t2 + 52*Power(t2,2) + Power(t2,3) + 
               72*Power(t2,4) + 23*Power(t2,5) + 
               Power(t1,4)*(4 + 2*t2 + Power(t2,2)) + 
               Power(t1,2)*(96 + 10*t2 + 189*Power(t2,2) + 
                  45*Power(t2,3) - 12*Power(t2,4)) + 
               Power(t1,3)*(-40 - 2*t2 - 46*Power(t2,2) + 
                  10*Power(t2,3) + Power(t2,4)) + 
               t1*(-88 - 22*t2 - 196*Power(t2,2) - 136*Power(t2,3) - 
                  41*Power(t2,4) + 7*Power(t2,5)))) + 
         Power(s,5)*(-9 + Power(t1,3)*(6*s1 - 4*t2) + 
            Power(s2,3)*(-4 + 2*s1 + 5*t1 - 3*t2) + 28*t2 + 20*s1*t2 + 
            6*Power(s1,2)*t2 - 24*Power(t2,2) - 28*s1*Power(t2,2) - 
            6*Power(s1,2)*Power(t2,2) + 22*Power(t2,3) + 
            12*s1*Power(t2,3) - 6*Power(t2,4) + 
            Power(s2,2)*(-4 + 4*Power(s1,2) - 6*Power(t1,2) + 
               s1*(-5 + 19*t1 - 12*t2) + t1*(13 - 5*t2) - 10*t2 + 
               6*Power(t2,2)) + 
            Power(t1,2)*(5 - 15*Power(s1,2) + 7*t2 + 2*Power(t2,2) + 
               s1*(-9 + 10*t2)) + 
            t1*(-4 - 3*t2 - 29*Power(t2,2) + 8*Power(t2,3) + 
               2*Power(s1,2)*(-9 + 10*t2) + 
               s1*(-18 + 55*t2 - 28*Power(t2,2))) + 
            s2*(17 + 2*Power(t1,3) - 17*t2 + 4*Power(t2,2) + 
               9*Power(t2,3) + 2*Power(t1,2)*(-4 + 5*t2) + 
               t1*(-11 + 30*t2 - 21*Power(t2,2)) - 
               s1*(-3 + t1 + 22*Power(t1,2) + 10*t2 - 13*t1*t2 + 
                  7*Power(t2,2)) + Power(s1,2)*(15*t1 - 2*(2 + t2)))) + 
         Power(s,4)*(30 - 20*t1 - 2*Power(t1,2) + 9*Power(t1,3) - 
            Power(t1,4) - 72*t2 + 32*t1*t2 - 15*Power(t1,2)*t2 + 
            3*Power(t1,3)*t2 + 69*Power(t2,2) - 4*t1*Power(t2,2) + 
            5*Power(t1,2)*Power(t2,2) - 6*Power(t1,3)*Power(t2,2) - 
            40*Power(t2,3) - 35*t1*Power(t2,3) + 
            8*Power(t1,2)*Power(t2,3) + 28*Power(t2,4) + 
            2*t1*Power(t2,4) - 4*Power(t2,5) + 
            Power(s2,4)*(2 - 2*t1 + t2) + 
            Power(s2,3)*(11 + 3*Power(t1,2) + t2 - 8*Power(t2,2) + 
               2*t1*(-9 + 7*t2)) - 
            2*Power(s1,3)*(3*Power(s2,2) - 10*Power(t1,2) - 
               2*(-1 + t2)*t2 + s2*(-3 + 10*t1 + t2) + t1*(-11 + 10*t2)) \
- Power(s2,2)*(-1 + Power(t1,3) + 2*t2 + 39*Power(t2,2) + 
               2*Power(t2,3) + Power(t1,2)*(-9 + 19*t2) + 
               t1*(1 - 20*t2 - 12*Power(t2,2))) + 
            s2*(-44 + 49*t2 - 47*Power(t2,2) - 26*Power(t2,3) + 
               17*Power(t2,4) + Power(t1,3)*(3 + 7*t2) + 
               Power(t1,2)*(-34 - 27*t2 + 3*Power(t2,2)) + 
               t1*(55 - 7*t2 + 105*Power(t2,2) - 27*Power(t2,3))) + 
            Power(s1,2)*(-7*Power(s2,3) - 15*Power(t1,3) + 
               Power(t1,2)*(18 - 20*t2) + 
               Power(s2,2)*(3 - 37*t1 + 16*t2) + 
               s2*(10 + 50*Power(t1,2) + t1*(6 - 12*t2) - 6*t2 + 
                  21*Power(t2,2)) + t1*(38 - 111*t2 + 42*Power(t2,2)) - 
               6*(1 + 3*t2 - 6*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(-Power(s2,4) + 20*Power(t1,3)*t2 + 
               Power(s2,3)*(9 - 20*t1 + 15*t2) - 
               Power(t1,2)*(23 + 19*t2 + 8*Power(t2,2)) + 
               t1*(41 - 32*t2 + 124*Power(t2,2) - 24*Power(t2,3)) + 
               2*(7 - 32*t2 + 29*Power(t2,2) - 30*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,2)*(11 + 28*Power(t1,2) + 44*t2 - 
                  8*Power(t2,2) + t1*(-50 + 21*t2)) + 
               s2*(-33 - 10*Power(t1,3) + Power(t1,2)*(43 - 48*t2) + 
                  42*t2 + 26*Power(t2,2) - 36*Power(t2,3) + 
                  t1*(6 - 129*t2 + 59*Power(t2,2))))) + 
         Power(s,3)*(-43 + 81*t1 - 51*Power(t1,2) + 15*Power(t1,3) - 
            2*Power(t1,4) + 113*t2 - 76*t1*t2 + Power(t1,2)*t2 + 
            25*Power(t1,3)*t2 - 3*Power(t1,4)*t2 - 149*Power(t2,2) + 
            95*t1*Power(t2,2) - 63*Power(t1,2)*Power(t2,2) + 
            9*Power(t1,3)*Power(t2,2) + 65*Power(t2,3) + 
            21*t1*Power(t2,3) - 7*Power(t1,2)*Power(t2,3) - 
            4*Power(t1,3)*Power(t2,3) - 41*Power(t2,4) - 
            16*t1*Power(t2,4) + 7*Power(t1,2)*Power(t2,4) + 
            17*Power(t2,5) - 2*t1*Power(t2,5) - Power(t2,6) + 
            Power(s2,4)*(-8 + t1*(8 - 7*t2) + 3*t2 + 3*Power(t2,2)) + 
            Power(s1,4)*(4*Power(s2,2) - 13*t1 - 15*Power(t1,2) + t2 + 
               10*t1*t2 - Power(t2,2) + s2*(-4 + 15*t1 + 3*t2)) + 
            Power(s2,3)*(11 + 24*t2 + 22*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,2)*(-1 + 10*t2) + 
               2*t1*(-5 - 23*t2 + 6*Power(t2,2))) - 
            Power(s2,2)*(-14 - 80*t2 - 38*Power(t2,2) + 
               45*Power(t2,3) + 12*Power(t2,4) + 
               Power(t1,3)*(4 + 3*t2) + 
               Power(t1,2)*(-46 - 28*t2 + 22*Power(t2,2)) + 
               t1*(56 + 81*t2 + 9*Power(t2,2) - 27*Power(t2,3))) + 
            s2*(33 + Power(t1,4) - 147*t2 + 98*Power(t2,2) - 
               50*Power(t2,3) - 54*Power(t2,4) + 12*Power(t2,5) + 
               Power(t1,3)*(-17 + 10*t2 + 9*Power(t2,2)) - 
               Power(t1,2)*(-44 + 65*t2 + 37*Power(t2,2) + 
                  11*Power(t2,3)) + 
               t1*(-61 + 118*t2 + 140*Power(t2,3) - 10*Power(t2,4))) + 
            Power(s1,3)*(9*Power(s2,3) + 20*Power(t1,3) + 
               Power(s2,2)*(-2 + 38*t1 - 6*t2) + 
               Power(t1,2)*(-22 + 20*t2) + 
               t1*(-36 + 103*t2 - 28*Power(t2,2)) + 
               4*(2 + t2 - 5*Power(t2,2) + Power(t2,3)) - 
               s2*(18 + 60*Power(t1,2) - 28*t2 + 21*Power(t2,2) + 
                  t1*(15 + 2*t2))) + 
            Power(s1,2)*(-4 + 3*Power(s2,4) + 
               Power(s2,3)*(-2 + 32*t1 - 24*t2) + 39*t2 - 
               40*Power(t1,3)*t2 - 49*Power(t2,2) + 54*Power(t2,3) - 
               6*Power(t2,4) + 
               Power(t1,2)*(32 + 21*t2 + 12*Power(t2,2)) + 
               t1*(-88 + 107*t2 - 183*Power(t2,2) + 24*Power(t2,3)) - 
               Power(s2,2)*(-6 + 52*Power(t1,2) + 61*t2 + 
                  12*Power(t2,2) + t1*(-69 + 37*t2)) + 
               s2*(10 + 20*Power(t1,3) - 33*t2 - 98*Power(t2,2) + 
                  45*Power(t2,3) + Power(t1,2)*(-93 + 92*t2) + 
                  t1*(57 + 222*t2 - 51*Power(t2,2)))) + 
            s1*(4*Power(t1,4) + Power(s2,4)*(-3 + 7*t1 - 6*t2) + 
               Power(t1,3)*(-44 - 9*t2 + 24*Power(t2,2)) + 
               Power(t1,2)*(55 + 42*t2 + 8*Power(t2,2) - 
                  24*Power(t2,3)) - 
               t1*(15 + 21*t2 + 92*Power(t2,2) - 109*Power(t2,3) + 
                  4*Power(t2,4)) + 
               2*(-30 + 78*t2 - 56*Power(t2,2) + 43*Power(t2,3) - 
                  26*Power(t2,4) + 2*Power(t2,5)) - 
               Power(s2,3)*(37 + 11*Power(t1,2) + 20*t2 - 
                  21*Power(t2,2) + t1*(-60 + 44*t2)) + 
               Power(s2,2)*(-59 + 4*Power(t1,3) - 54*t2 + 
                  108*Power(t2,2) + 26*Power(t2,3) + 
                  Power(t1,2)*(-41 + 72*t2) - 
                  4*t1*(-18 + 12*t2 + 7*Power(t2,2))) - 
               s2*(-142 + 100*t2 - 101*Power(t2,2) - 128*Power(t2,3) + 
                  39*Power(t2,4) + Power(t1,3)*(11 + 28*t2) + 
                  Power(t1,2)*(-98 - 120*t2 + 21*Power(t2,2)) + 
                  t1*(145 + 56*t2 + 347*Power(t2,2) - 48*Power(t2,3))))) \
+ Power(s,2)*(60 - 129*t1 + 91*Power(t1,2) - 35*Power(t1,3) + 
            13*Power(t1,4) - 91*t2 + 191*t1*t2 - 102*Power(t1,2)*t2 + 
            7*Power(t1,3)*t2 - 5*Power(t1,4)*t2 + 134*Power(t2,2) - 
            116*t1*Power(t2,2) + 38*Power(t1,2)*Power(t2,2) + 
            23*Power(t1,3)*Power(t2,2) - 3*Power(t1,4)*Power(t2,2) - 
            113*Power(t2,3) + 69*t1*Power(t2,3) - 
            61*Power(t1,2)*Power(t2,3) + 9*Power(t1,3)*Power(t2,3) + 
            32*Power(t2,4) + 26*t1*Power(t2,4) - 
            11*Power(t1,2)*Power(t2,4) - Power(t1,3)*Power(t2,4) - 
            23*Power(t2,5) + t1*Power(t2,5) + 
            2*Power(t1,2)*Power(t2,5) + 4*Power(t2,6) - t1*Power(t2,6) + 
            Power(s2,4)*t2*(-16 + t1*(16 - 9*t2) + t2 + 3*Power(t2,2)) - 
            Power(s1,5)*(Power(s2,2) + s2*(-1 + 6*t1 + t2) + 
               t1*(-3 - 6*t1 + 2*t2)) + 
            Power(s2,3)*(-24 - 12*t2 + 12*Power(t2,2) + 
               31*Power(t2,3) + 
               4*Power(t1,2)*(-6 + t2 + 3*Power(t2,2)) + 
               2*t1*(24 + 4*t2 - 24*Power(t2,2) + Power(t2,3))) + 
            Power(s2,2)*(49 + 61*t2 + 161*Power(t2,2) + 
               76*Power(t2,3) - 22*Power(t2,4) - 9*Power(t2,5) + 
               Power(t1,3)*(7 - 10*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*(35 + 77*t2 + 35*Power(t2,2) - 
                  12*Power(t2,3)) + 
               t1*(-91 - 128*t2 - 173*Power(t2,2) - 33*Power(t2,3) + 
                  19*Power(t2,4))) + 
            s2*(-68 + 98*t2 - 115*Power(t2,2) + 133*Power(t2,3) - 
               7*Power(t2,4) - 39*Power(t2,5) + 3*Power(t2,6) + 
               Power(t1,4)*(1 + 2*t2) + 
               Power(t1,3)*(-47 - 47*t2 + 13*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(23 + 250*t2 - 16*Power(t2,2) - 
                  26*Power(t2,3) - 11*Power(t2,4)) + 
               t1*(91 - 303*t2 - 10*Power(t2,2) - 34*Power(t2,3) + 
                  85*Power(t2,4) + 3*Power(t2,5))) + 
            Power(s1,4)*(-3 - 5*Power(s2,3) - 15*Power(t1,3) + 
               Power(s2,2)*(8 - 22*t1 - 3*t2) + t2 + 4*Power(t2,2) - 
               2*Power(t1,2)*(-9 + 5*t2) + 
               t1*(15 - 43*t2 + 7*Power(t2,2)) + 
               s2*(9 + 40*Power(t1,2) - 23*t2 + 7*Power(t2,2) + 
                  t1*(19 + 8*t2))) - 
            Power(s1,3)*(2 + 3*Power(s2,4) + 
               Power(s2,3)*(11 + 26*t1 - 15*t2) - 2*t2 - 
               40*Power(t1,3)*t2 - 20*Power(t2,2) + 16*Power(t2,3) + 
               Power(t1,2)*(8 + 19*t2 + 8*Power(t2,2)) + 
               2*t1*(-35 + 55*t2 - 55*Power(t2,2) + 4*Power(t2,3)) - 
               Power(s2,2)*(-37 + 48*Power(t1,2) + 14*t2 + 
                  24*Power(t2,2) + t1*(-38 + 35*t2)) + 
               s2*(-5 + 20*Power(t1,3) - 5*t2 - 102*Power(t2,2) + 
                  18*Power(t2,3) + Power(t1,2)*(-101 + 88*t2) + 
                  t1*(96 + 196*t2 - 9*Power(t2,2)))) + 
            Power(s1,2)*(9 - 6*Power(t1,4) - 111*t2 + 37*Power(t2,2) - 
               66*Power(t2,3) + 24*Power(t2,4) + 
               Power(s2,4)*(1 - 9*t1 + 9*t2) + 
               Power(t1,3)*(78 + 9*t2 - 36*Power(t2,2)) + 
               Power(t1,2)*(-116 - 63*t2 - 27*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(111 - 51*t2 + 201*Power(t2,2) - 102*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,3)*(43 + 15*Power(t1,2) + 53*t2 - 
                  15*Power(t2,2) + t1*(-82 + 54*t2)) + 
               Power(s2,2)*(115 - 6*Power(t1,3) + 
                  Power(t1,2)*(67 - 102*t2) + 172*t2 - 
                  74*Power(t2,2) - 44*Power(t2,3) + 
                  3*t1*(-52 + 5*t2 + 5*Power(t2,2))) + 
               s2*(-82 + 109*t2 - 44*Power(t2,2) - 178*Power(t2,3) + 
                  22*Power(t2,4) + 3*Power(t1,3)*(5 + 14*t2) + 
                  3*Power(t1,2)*(-29 - 68*t2 + 15*Power(t2,2)) + 
                  t1*(26 + 151*t2 + 420*Power(t2,2) - 15*Power(t2,3)))) \
+ s1*(76 - 144*t2 + 226*Power(t2,2) - 68*Power(t2,3) + 68*Power(t2,4) - 
               16*Power(t2,5) + Power(t1,4)*(6 + 9*t2) + 
               Power(t1,3)*(-7 - 96*t2 - 18*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(84 + 67*t2 + 132*Power(t2,2) + 
                  39*Power(t2,3) - 14*Power(t2,4)) + 
               t1*(-159 + 12*t2 - 88*Power(t2,2) - 132*Power(t2,3) + 
                  31*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,4)*(16 - 2*t2 - 9*Power(t2,2) + 
                  2*t1*(-8 + 9*t2)) - 
               Power(s2,3)*(-12 + 55*t2 + 73*Power(t2,2) - 
                  5*Power(t2,3) + Power(t1,2)*(4 + 27*t2) + 
                  2*t1*(4 - 65*t2 + 15*Power(t2,2))) + 
               Power(s2,2)*(-66 - 272*t2 - 211*Power(t2,2) + 
                  74*Power(t2,3) + 33*Power(t2,4) + 
                  Power(t1,3)*(11 + 9*t2) + 
                  Power(t1,2)*(-84 - 98*t2 + 66*Power(t2,2)) + 
                  t1*(139 + 321*t2 + 56*Power(t2,2) - 47*Power(t2,3))) + 
               s2*(-102 - 3*Power(t1,4) + 191*t2 - 247*Power(t2,2) + 
                  37*Power(t2,3) + 137*Power(t2,4) - 13*Power(t2,5) - 
                  3*Power(t1,3)*(-18 + 10*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*
                   (-265 + 101*t2 + 129*Power(t2,2) + 14*Power(t2,3)) + 
                  t1*(316 - 6*t2 - 21*Power(t2,2) - 328*Power(t2,3) + 
                     Power(t2,4))))) + 
         s*(-62 + 176*t1 + Power(s1,6)*(s2 - t1)*t1 - 156*Power(t1,2) + 
            32*Power(t1,3) + 10*Power(t1,4) + 26*t2 - 130*t1*t2 + 
            202*Power(t1,2)*t2 - 118*Power(t1,3)*t2 + 20*Power(t1,4)*t2 - 
            45*Power(t2,2) + 53*t1*Power(t2,2) + 
            9*Power(t1,2)*Power(t2,2) - 13*Power(t1,3)*Power(t2,2) - 
            4*Power(t1,4)*Power(t2,2) + 83*Power(t2,3) - 
            100*t1*Power(t2,3) + 47*Power(t1,2)*Power(t2,3) + 
            7*Power(t1,3)*Power(t2,3) - Power(t1,4)*Power(t2,3) - 
            28*Power(t2,4) + 7*t1*Power(t2,4) - 
            18*Power(t1,2)*Power(t2,4) + 3*Power(t1,3)*Power(t2,4) + 
            11*Power(t2,5) + 6*t1*Power(t2,5) - 
            4*Power(t1,2)*Power(t2,5) - 5*Power(t2,6) + 
            2*t1*Power(t2,6) + 
            Power(s2,4)*Power(t2,2)*
             (-8 + t1*(8 - 5*t2) + t2 + Power(t2,2)) + 
            Power(s2,3)*t2*(-32 - 41*t2 - 4*Power(t2,2) + 
               20*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(-32 + 11*t2 + 6*Power(t2,2)) - 
               t1*(-64 - 30*t2 + 30*Power(t2,2) + Power(t2,3))) - 
            Power(s2,2)*(24 - 18*t2 - 40*Power(t2,2) - 102*Power(t2,3) - 
               50*Power(t2,4) + 3*Power(t2,5) + 2*Power(t2,6) + 
               Power(t1,3)*(-24 + 2*t2 + 8*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(72 - 22*t2 - 48*Power(t2,2) - 
                  22*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(-72 + 38*t2 + 80*Power(t2,2) + 123*Power(t2,3) + 
                  24*Power(t2,4) - 6*Power(t2,5))) + 
            s2*(60 - 30*t2 + 81*Power(t2,2) - 27*Power(t2,3) + 
               83*Power(t2,4) + 15*Power(t2,5) - 10*Power(t2,6) + 
               Power(t1,4)*(-4 + 2*t2 + Power(t2,2)) + 
               Power(t1,3)*(-48 - 24*t2 - 43*Power(t2,2) + 
                  8*Power(t2,3) + Power(t2,4)) - 
               3*Power(t1,2)*
                (-56 - 4*t2 - 80*Power(t2,2) - 9*Power(t2,3) + 
                  3*Power(t2,4) + Power(t2,5)) + 
               t1*(-176 + 40*t2 - 279*Power(t2,2) - 92*Power(t2,3) - 
                  41*Power(t2,4) + 20*Power(t2,5) + 2*Power(t2,6))) + 
            Power(s1,5)*(Power(s2,3) + Power(s2,2)*(-9 + 7*t1 + 2*t2) - 
               s2*(1 + 14*Power(t1,2) - 6*t2 + 3*t1*(4 + t2)) + 
               t1*(-2 + 6*Power(t1,2) + 6*t2 + t1*(-9 + 2*t2))) + 
            Power(s1,4)*(1 + Power(s2,4) + 
               Power(s2,3)*(12 + 11*t1 - 3*t2) - 5*t2 - 
               20*Power(t1,3)*t2 - 5*Power(t2,2) + 
               Power(s2,2)*(32 - 22*Power(t1,2) + t1*(4 - 18*t2) + 
                  29*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(-13 + 16*t2 + 2*Power(t2,2)) - 
               2*t1*(8 - 21*t2 + 11*Power(t2,2)) + 
               s2*(7 + 10*Power(t1,3) + 6*t2 - 34*Power(t2,2) + 
                  Power(t1,2)*(-55 + 42*t2) + 
                  4*t1*(13 + 23*t2 + Power(t2,2)))) + 
            Power(s1,3)*(2 + 4*Power(t1,4) + 
               Power(s2,4)*(-1 + 5*t1 - 4*t2) + 20*t2 + 4*Power(t2,2) + 
               20*Power(t2,3) + 
               3*Power(t1,3)*(-20 - t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(87 + 60*t2 + 10*Power(t2,2) - 
                  8*Power(t2,3)) + 
               t1*(-69 + 43*t2 - 120*Power(t2,2) + 28*Power(t2,3)) - 
               Power(s2,3)*(19 + 9*Power(t1,2) + 56*t2 - 
                  2*Power(t2,2) + 8*t1*(-7 + 4*t2)) + 
               Power(s2,2)*(-73 + 4*Power(t1,3) - 160*t2 - 
                  30*Power(t2,2) + 20*Power(t2,3) + 
                  Power(t1,2)*(-47 + 64*t2) + 
                  2*t1*(58 + 16*t2 + 3*Power(t2,2))) - 
               s2*(24 + 100*t2 + 27*Power(t2,2) - 76*Power(t2,3) + 
                  Power(t1,3)*(9 + 28*t2) + 
                  Power(t1,2)*(-16 - 156*t2 + 39*Power(t2,2)) + 
                  t1*(-101 + 104*t2 + 224*Power(t2,2) + 6*Power(t2,3)))) \
+ Power(s1,2)*(-9 + 81*t2 - 71*Power(t2,2) + 18*Power(t2,3) - 
               30*Power(t2,4) - 3*Power(t1,4)*(2 + 3*t2) + 
               Power(s2,4)*(-8 + t1*(8 - 15*t2) + 3*t2 + 
                  6*Power(t2,2)) + 
               Power(t1,3)*(-19 + 117*t2 + 9*Power(t2,2) - 
                  12*Power(t2,3)) + 
               t1*(-29 + 24*t2 - 31*Power(t2,2) + 128*Power(t2,3) - 
                  12*Power(t2,4)) + 
               Power(t1,2)*(63 - 105*t2 - 99*Power(t2,2) - 
                  36*Power(t2,3) + 7*Power(t2,4)) + 
               Power(s2,3)*(-41 + 34*t2 + 96*Power(t2,2) + 
                  2*Power(t2,3) + Power(t1,2)*(11 + 24*t2) + 
                  2*t1*(15 - 71*t2 + 15*Power(t2,2))) + 
               Power(s2,2)*(50 + 240*t2 + 274*Power(t2,2) + 
                  6*Power(t2,3) - 20*Power(t2,4) - 
                  Power(t1,3)*(10 + 9*t2) + 
                  Power(t1,2)*(62 + 108*t2 - 66*Power(t2,2)) + 
                  t1*(-102 - 339*t2 - 100*Power(t2,2) + 20*Power(t2,3))) \
+ s2*(89 + 3*Power(t1,4) + 33*t2 + 262*Power(t2,2) + 55*Power(t2,3) - 
                  84*Power(t2,4) + 
                  3*Power(t1,3)*(-19 + 10*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*
                   (270 - t2 - 156*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(-305 - 314*t2 + 11*Power(t2,2) + 240*Power(t2,3) + 
                     9*Power(t2,4)))) + 
            s1*(-28 + 54*t2 - 166*Power(t2,2) + 78*Power(t2,3) - 
               28*Power(t2,4) + 20*Power(t2,5) + 
               2*Power(t1,4)*(-11 + 5*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(126 + 32*t2 - 64*Power(t2,2) - 
                  9*Power(t2,3) + 2*Power(t2,4)) - 
               Power(t1,2)*(214 + 72*t2 + 29*Power(t2,2) - 
                  70*Power(t2,3) - 23*Power(t2,4) + 2*Power(t2,5)) - 
               t1*(-138 + 24*t2 - 145*Power(t2,2) + 3*Power(t2,3) + 
                  54*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,4)*t2*
                (16 - 3*t2 - 4*Power(t2,2) + t1*(-16 + 15*t2)) - 
               Power(s2,3)*(-32 - 82*t2 + 11*Power(t2,2) + 
                  72*Power(t2,3) + 3*Power(t2,4) + 
                  Power(t1,2)*(-32 + 22*t2 + 21*Power(t2,2)) + 
                  4*t1*(16 + 15*t2 - 29*Power(t2,2) + 2*Power(t2,3))) + 
               Power(s2,2)*(-18 - 90*t2 - 269*Power(t2,2) - 
                  196*Power(t2,3) + 7*Power(t2,4) + 10*Power(t2,5) + 
                  2*Power(t1,3)*(1 + 9*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (-22 - 110*t2 - 83*Power(t2,2) + 28*Power(t2,3)) + 
                  t1*(38 + 182*t2 + 346*Power(t2,2) + 88*Power(t2,3) - 
                     21*Power(t2,4))) - 
               s2*(-30 + 170*t2 - 18*Power(t2,2) + 252*Power(t2,3) + 
                  48*Power(t2,4) - 46*Power(t2,5) + 
                  Power(t1,4)*(2 + 4*t2) + 
                  Power(t1,3)*
                   (-24 - 100*t2 + 29*Power(t2,2) + 10*Power(t2,3)) + 
                  Power(t1,2)*
                   (12 + 510*t2 + 42*Power(t2,2) - 64*Power(t2,3) - 
                     9*Power(t2,4)) + 
                  t1*(40 - 584*t2 - 305*Power(t2,2) - 82*Power(t2,3) + 
                     116*Power(t2,4) + 7*Power(t2,5))))))*T4(-s + s1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s1 + t1 - t2)*(s - s1 + t2)*
       Power(1 - s + s*s2 - s1*s2 - t1 + s2*t2,2)) - 
    (8*(4 - 8*s2 + Power(s2,2) - 15*t1 + 19*s2*t1 - 4*Power(s2,2)*t1 + 
         24*Power(t1,2) - 8*s2*Power(t1,2) + 6*Power(s2,2)*Power(t1,2) - 
         22*Power(t1,3) - 10*s2*Power(t1,3) - 4*Power(s2,2)*Power(t1,3) + 
         12*Power(t1,4) + 8*s2*Power(t1,4) + Power(s2,2)*Power(t1,4) - 
         3*Power(t1,5) - s2*Power(t1,5) + 
         Power(s1,5)*(Power(s2,2)*(-3 + t1) + (-2 + t1)*Power(t1,2) - 
            s2*t1*(3 + 2*t1)) + 
         Power(s,5)*(t1 - t2)*(-1 + s1 + t1 - t2)*(1 - s2 + t1 - t2) - 
         9*t2 + 20*s2*t2 - 24*Power(s2,2)*t2 + 2*Power(s2,3)*t2 + t1*t2 - 
         50*s2*t1*t2 + 38*Power(s2,2)*t1*t2 - 6*Power(s2,3)*t1*t2 + 
         34*Power(t1,2)*t2 + 46*s2*Power(t1,2)*t2 - 
         2*Power(s2,2)*Power(t1,2)*t2 + 6*Power(s2,3)*Power(t1,2)*t2 - 
         34*Power(t1,3)*t2 - 22*s2*Power(t1,3)*t2 - 
         14*Power(s2,2)*Power(t1,3)*t2 - 2*Power(s2,3)*Power(t1,3)*t2 + 
         7*Power(t1,4)*t2 + 6*s2*Power(t1,4)*t2 + 
         2*Power(s2,2)*Power(t1,4)*t2 + Power(t1,5)*t2 + 3*Power(t2,2) + 
         5*s2*Power(t2,2) + 29*Power(s2,2)*Power(t2,2) - 
         19*Power(s2,3)*Power(t2,2) + Power(s2,4)*Power(t2,2) - 
         33*t1*Power(t2,2) - 57*s2*t1*Power(t2,2) - 
         42*Power(s2,2)*t1*Power(t2,2) + 13*Power(s2,3)*t1*Power(t2,2) - 
         2*Power(s2,4)*t1*Power(t2,2) + 48*Power(t1,2)*Power(t2,2) + 
         69*s2*Power(t1,2)*Power(t2,2) + 
         17*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         7*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         17*Power(t1,3)*Power(t2,2) - 15*s2*Power(t1,3)*Power(t2,2) - 
         4*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         Power(s2,3)*Power(t1,3)*Power(t2,2) - Power(t1,4)*Power(t2,2) - 
         2*s2*Power(t1,4)*Power(t2,2) + 15*Power(t2,3) + 
         9*s2*Power(t2,3) + 28*Power(s2,2)*Power(t2,3) + 
         9*Power(s2,3)*Power(t2,3) - 3*Power(s2,4)*Power(t2,3) - 
         26*t1*Power(t2,3) - 54*s2*t1*Power(t2,3) - 
         37*Power(s2,2)*t1*Power(t2,3) - 6*Power(s2,3)*t1*Power(t2,3) - 
         Power(s2,4)*t1*Power(t2,3) + 15*Power(t1,2)*Power(t2,3) + 
         22*s2*Power(t1,2)*Power(t2,3) + 
         8*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         3*s2*Power(t1,3)*Power(t2,3) + 
         Power(s2,2)*Power(t1,3)*Power(t2,3) + Power(t2,4) + 
         21*s2*Power(t2,4) + 11*Power(s2,2)*Power(t2,4) + 
         6*Power(s2,3)*Power(t2,4) - 6*t1*Power(t2,4) - 
         16*s2*t1*Power(t2,4) - 7*Power(s2,2)*t1*Power(t2,4) + 
         Power(t1,2)*Power(t2,4) - s2*Power(t1,2)*Power(t2,4) - 
         2*Power(s2,2)*Power(t1,2)*Power(t2,4) + 2*Power(t2,5) + 
         5*s2*Power(t2,5) + Power(s2,2)*Power(t2,5) - t1*Power(t2,5) + 
         Power(s2,2)*t1*Power(t2,5) + 
         Power(s1,4)*(2*Power(s2,3)*(2 + t1) + 
            Power(s2,2)*(11 - 3*Power(t1,2) + 13*t2 - 3*t1*(2 + t2)) + 
            t1*(3 + Power(t1,3) + 4*t2 - 3*Power(t1,2)*(1 + t2) + 
               t1*(-5 + 4*t2)) + 
            s2*(3*(2 + t2) + Power(t1,2)*(-13 + 6*t2) + t1*(11 + 16*t2))) \
+ Power(s,4)*(1 + 4*t1 - Power(t1,2) - 3*Power(t1,3) + Power(t1,4) - 
            8*t2 + 2*t1*t2 + 10*Power(t1,2)*t2 - Power(t1,3)*t2 - 
            Power(t2,2) - 11*t1*Power(t2,2) - 3*Power(t1,2)*Power(t2,2) + 
            4*Power(t2,3) + 5*t1*Power(t2,3) - 2*Power(t2,4) + 
            Power(s1,2)*(Power(s2,2) - 4*Power(t1,2) + 
               s2*(-1 + 4*t1 - t2) - 2*(-1 + t2)*t2 + t1*(-5 + 6*t2)) + 
            Power(s2,2)*(2 + 4*Power(t1,2) + 3*t2 + 3*Power(t2,2) - 
               t1*(4 + 7*t2)) + 
            s1*(-1 - 3*Power(t1,3) + Power(s2,2)*(-3 + 5*t1 - 4*t2) + 
               5*t2 - 6*Power(t2,2) + 4*Power(t2,3) + 
               Power(t1,2)*(-3 + 10*t2) + 
               t1*(2 + 9*t2 - 11*Power(t2,2)) + 
               s2*(4 + t1*(-5 + t2) - 3*t2 - Power(t2,2))) - 
            s2*(3 + 4*Power(t1,3) - 6*t2 - 7*Power(t2,2) - 
               2*Power(t2,3) - 2*Power(t1,2)*(2 + 5*t2) + 
               t1*(1 + 11*t2 + 8*Power(t2,2)))) + 
         Power(s1,3)*(-3 + Power(s2,4)*(3 + t1) + 
            Power(t1,4)*(1 - 2*t2) - 3*t2 - 2*Power(t2,2) + 
            Power(t1,2)*(34 + 19*t2) + 
            Power(t1,3)*(-24 + 4*t2 + 3*Power(t2,2)) - 
            t1*(12 + 2*t2 + 11*Power(t2,2)) - 
            2*Power(s2,3)*(4 + 9*t2 + t1*(-2 + 3*t2)) + 
            s2*(-22 + 2*Power(t1,4) - 39*t2 - 14*Power(t2,2) - 
               Power(t1,3)*(16 + t2) + 
               t1*(43 - 18*t2 - 30*Power(t2,2)) + 
               Power(t1,2)*(13 + 42*t2 - 6*Power(t2,2))) + 
            Power(s2,2)*(-3*Power(t1,3) + 9*Power(t1,2)*t2 + 
               t1*(15 + 29*t2 + 2*Power(t2,2)) - 
               2*(6 + 23*t2 + 11*Power(t2,2)))) + 
         Power(s1,2)*(11 + Power(t1,5) + 26*t2 + 7*Power(t2,2) + 
            6*Power(t2,3) + Power(t1,4)*(-20 - 2*t2 + Power(t2,2)) + 
            Power(t1,3)*(30 + 39*t2 + Power(t2,2) - Power(t2,3)) - 
            Power(t1,2)*(-15 + 30*t2 + 22*Power(t2,2) + 4*Power(t2,3)) + 
            t1*(-37 - 21*t2 - 11*Power(t2,2) + 9*Power(t2,3)) + 
            Power(s2,4)*(1 + Power(t1,2) - 9*t2 - t1*(2 + 3*t2)) + 
            Power(s2,3)*(-14 - 2*Power(t1,3) + 25*t2 + 30*Power(t2,2) + 
               Power(t1,2)*(14 + t2) + 2*t1*(1 - 7*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(20 + Power(t1,4) + 5*Power(t1,3)*(-2 + t2) + 
               50*t2 + 70*Power(t2,2) + 18*Power(t2,3) + 
               Power(t1,2)*(17 + 10*t2 - 11*Power(t2,2)) + 
               t1*(-28 - 65*t2 - 47*Power(t2,2) + 2*Power(t2,3))) + 
            s2*(12 + 61*t2 + 81*Power(t2,2) + 24*Power(t2,3) - 
               Power(t1,4)*(1 + 3*t2) + 
               Power(t1,3)*(-5 + 32*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(53 + 7*t2 - 46*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-59 - 157*t2 - 13*Power(t2,2) + 24*Power(t2,3)))) - 
         s1*(4 + 15*t2 + 38*Power(t2,2) + 5*Power(t2,3) + 6*Power(t2,4) + 
            Power(t1,5)*(2 + t2) - 
            Power(t1,4)*(-8 + 16*t2 + Power(t2,2)) + 
            Power(t1,3)*(-56 - 3*t2 + 15*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,2)*(84 + 81*t2 + 19*Power(t2,2) - 7*Power(t2,3) - 
               2*Power(t2,4)) + 
            t1*(-42 - 78*t2 - 59*Power(t2,2) - 16*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,4)*t2*(2 + 2*Power(t1,2) - 9*t2 - t1*(4 + 3*t2)) + 
            Power(s2,3)*(2 - 33*t2 + 26*Power(t2,2) + 22*Power(t2,3) - 
               Power(t1,3)*(2 + 3*t2) + 
               Power(t1,2)*(6 + 21*t2 + 2*Power(t2,2)) + 
               t1*(-6 + 15*t2 - 16*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s2,2)*(-19 + 45*t2 + 66*Power(t2,2) + 46*Power(t2,3) + 
               7*Power(t2,4) + Power(t1,4)*(3 + t2) + 
               Power(t1,3)*(-22 - 10*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(16 + 22*t2 + 18*Power(t2,2) - 
                  7*Power(t2,3)) + 
               t1*(22 - 58*t2 - 87*Power(t2,2) - 31*Power(t2,3) + 
                  3*Power(t2,4))) + 
            s2*(16 - Power(t1,5) + 23*t2 + 48*Power(t2,2) + 
               69*Power(t2,3) + 18*Power(t2,4) - 
               Power(t1,4)*(-10 + 5*t2 + Power(t2,2)) + 
               Power(t1,2)*(38 + 134*t2 + 42*Power(t2,2) - 
                  18*Power(t2,3)) + 
               Power(t1,3)*(-24 - 20*t2 + 19*Power(t2,2) + Power(t2,3)) + 
               t1*(-39 - 132*t2 - 168*Power(t2,2) - 36*Power(t2,3) + 
                  7*Power(t2,4)))) + 
         Power(s,3)*(5 - 9*t1 + 21*Power(t1,2) - 5*Power(t1,3) + 15*t2 - 
            51*t1*t2 + 17*Power(t1,2)*t2 - 5*Power(t1,3)*t2 + 
            2*Power(t1,4)*t2 + 16*Power(t2,2) - 11*t1*Power(t2,2) + 
            18*Power(t1,2)*Power(t2,2) - 5*Power(t1,3)*Power(t2,2) - 
            Power(t2,3) - 21*t1*Power(t2,3) + 3*Power(t1,2)*Power(t2,3) + 
            8*Power(t2,4) + t1*Power(t2,4) - Power(t2,5) - 
            Power(s1,3)*(2*Power(s2,2) - 7*t1 - 6*Power(t1,2) + t2 + 
               6*t1*t2 - Power(t2,2) + s2*(-2 + 6*t1 + t2)) - 
            Power(s2,3)*(8 + 5*Power(t1,2) + t2 + 3*Power(t2,2) - 
               t1*(9 + 8*t2)) + 
            Power(s2,2)*(6*Power(t1,3) - Power(t1,2)*(11 + 9*t2) - 
               18*(-1 + 2*t2 + Power(t2,2)) + 
               t1*(7 + 26*t2 + 3*Power(t2,2))) + 
            Power(s1,2)*(-2 - 2*Power(s2,3) + 2*Power(t1,3) + 
               Power(t1,2)*(6 - 12*t2) - 5*t2 + 10*Power(t2,2) - 
               3*Power(t2,3) + Power(s2,2)*(3 - 13*t1 + 6*t2) + 
               s2*(1 + 8*Power(t1,2) + t1*(9 - 6*t2) + t2 + 
                  8*Power(t2,2)) + t1*(5 - 29*t2 + 13*Power(t2,2))) + 
            s2*(-15 + 10*Power(t1,3) - 2*Power(t1,4) + 20*t2 - 
               13*Power(t2,2) + 6*Power(t2,4) + 
               Power(t1,2)*(-17 - 36*t2 + 12*Power(t2,2)) + 
               t1*(-4 + 46*t2 + 26*Power(t2,2) - 16*Power(t2,3))) + 
            s1*(3 - 4*Power(t1,4) - 18*t2 + 6*Power(t2,2) - 
               17*Power(t2,3) + 3*Power(t2,4) + 
               Power(s2,3)*(4 - 7*t1 + 5*t2) + Power(t1,3)*(9 + 6*t2) + 
               t1*t2*(18 + 43*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(2 - 35*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(-1 - 5*Power(t1,2) + 13*t2 - 4*Power(t2,2) + 
                  t1*(5 + 12*t2)) + 
               s2*(-6 + 12*Power(t1,3) + 2*t2 - 3*Power(t2,2) - 
                  13*Power(t2,3) - Power(t1,2)*(5 + 27*t2) + 
                  t1*(-3 - 18*t2 + 28*Power(t2,2))))) + 
         Power(s,2)*(-2 + 30*t1 - 58*Power(t1,2) + 39*Power(t1,3) - 
            10*Power(t1,4) + Power(t1,5) - 24*t2 + 112*t1*t2 - 
            101*Power(t1,2)*t2 + 37*Power(t1,3)*t2 - 4*Power(t1,4)*t2 - 
            44*Power(t2,2) + 41*t1*Power(t2,2) - 
            39*Power(t1,2)*Power(t2,2) + 5*Power(t1,3)*Power(t2,2) + 
            Power(t1,4)*Power(t2,2) + 3*Power(t2,3) + 21*t1*Power(t2,3) + 
            2*Power(t1,2)*Power(t2,3) - 3*Power(t1,3)*Power(t2,3) - 
            9*Power(t2,4) - 8*t1*Power(t2,4) + 
            3*Power(t1,2)*Power(t2,4) + 4*Power(t2,5) - t1*Power(t2,5) + 
            Power(s1,4)*(Power(s2,2) + s2*(-1 + 4*t1 + t2) + 
               t1*(-3 - 4*t1 + 2*t2)) + 
            Power(s2,4)*(2 + 2*Power(t1,2) - t2 + Power(t2,2) - 
               t1*(4 + 3*t2)) - 
            Power(s2,3)*(5 + 3*Power(t1,3) + Power(t1,2)*(-9 + t2) - 
               20*t2 - 19*Power(t2,2) + 2*Power(t2,3) + 
               t1*(1 + 7*t2 - 6*Power(t2,2))) + 
            Power(s1,3)*(3 + 3*Power(s2,3) + 12*Power(s2,2)*t1 + 
               2*Power(t1,3) - t2 - 4*Power(t2,2) + 
               Power(t1,2)*(-5 + 6*t2) + 
               s2*(-6 - 14*Power(t1,2) + t1*(-9 + t2) + 9*t2 - 
                  6*Power(t2,2)) + t1*(-8 + 26*t2 - 5*Power(t2,2))) + 
            Power(s2,2)*(21 + Power(t1,4) + 41*t2 + 6*Power(t1,3)*t2 - 
               37*Power(t2,2) - 20*Power(t2,3) - 5*Power(t2,4) + 
               Power(t1,2)*(-2 + 8*t2 - 17*Power(t2,2)) + 
               t1*(-20 - 47*t2 + 9*Power(t2,2) + 15*Power(t2,3))) + 
            s2*(-16 - 61*t2 + 13*Power(t2,2) - 41*Power(t2,3) - 
               16*Power(t2,4) + 3*Power(t2,5) - 3*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(1 + 17*t2 + 8*Power(t2,2)) - 
               Power(t1,2)*(-7 + 32*t2 + 61*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(11 + 43*t2 + 90*Power(t2,2) + 63*Power(t2,3) - 
                  4*Power(t2,4))) + 
            Power(s1,2)*(-3 + Power(s2,4) + 6*Power(t1,4) + 
               Power(s2,3)*(13*t1 - 8*t2) + 12*t2 - 7*Power(t2,2) + 
               12*Power(t2,3) - 12*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(8 + 30*t2 + 3*Power(t2,2)) - 
               Power(s2,2)*(-4 + 5*Power(t1,2) + 26*t2 + 7*t1*t2 + 
                  8*Power(t2,2)) + 
               t1*(-15 + 5*t2 - 51*Power(t2,2) + 3*Power(t2,3)) - 
               s2*(2 + 12*Power(t1,3) + Power(t1,2)*(13 - 30*t2) + 
                  22*t2 + 31*Power(t2,2) - 12*Power(t2,3) + 
                  t1*(-25 - 76*t2 + 18*Power(t2,2)))) + 
            s1*(-2 + Power(t1,4)*(1 - 6*t2) + 
               Power(s2,4)*(1 + 3*t1 - 2*t2) + 46*t2 - 18*Power(t2,2) + 
               17*Power(t2,3) - 12*Power(t2,4) + 
               Power(t1,3)*(4 + 2*t2 + 13*Power(t2,2)) - 
               Power(t1,2)*(6 - 33*t2 + 27*Power(t2,2) + 8*Power(t2,3)) + 
               t1*(-17 - 23*t2 - 18*Power(t2,2) + 36*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,3)*(-6 + 7*Power(t1,2) - 19*t2 + 7*Power(t2,2) - 
                  t1*(13 + 19*t2)) + 
               Power(s2,2)*(-26 - 15*Power(t1,3) + 41*t2 + 
                  46*Power(t2,2) + 12*Power(t2,3) - t1*t2*(21 + 20*t2) + 
                  Power(t1,2)*(33 + 26*t2)) + 
               s2*(33 + 6*Power(t1,4) - 6*t2 + 69*Power(t2,2) + 
                  39*Power(t2,3) - 10*Power(t2,4) - 
                  Power(t1,3)*(36 + t2) + 
                  Power(t1,2)*(41 + 97*t2 - 12*Power(t2,2)) + 
                  t1*(-8 - 138*t2 - 130*Power(t2,2) + 17*Power(t2,3))))) - 
         s*(7 + Power(s1,5)*(s2 - t1)*t1 - 36*Power(t1,2) + 
            42*Power(t1,3) - 11*Power(t1,4) - 2*Power(t1,5) - 24*t2 + 
            66*t1*t2 - 80*Power(t1,2)*t2 + 29*Power(t1,3)*t2 + 
            10*Power(t1,4)*t2 - Power(t1,5)*t2 - 25*Power(t2,2) + 
            13*t1*Power(t2,2) + 8*Power(t1,2)*Power(t2,2) - 
            28*Power(t1,3)*Power(t2,2) + 4*Power(t1,4)*Power(t2,2) + 
            19*Power(t2,3) - 14*t1*Power(t2,3) + 
            30*Power(t1,2)*Power(t2,3) - 7*Power(t1,3)*Power(t2,3) - 
            2*Power(t2,4) - 15*t1*Power(t2,4) + 
            6*Power(t1,2)*Power(t2,4) + 5*Power(t2,5) - 2*t1*Power(t2,5) + 
            Power(s2,4)*t2*(1 - 3*Power(t1,2) + 8*t2 - Power(t2,2) + 
               t1*(2 + 4*t2)) + 
            Power(s2,3)*(1 + 25*t2 - 28*Power(t2,2) - 15*Power(t2,3) - 
               Power(t2,4) + Power(t1,3)*(3 + 4*t2) - 
               Power(t1,2)*(5 + 7*t2 + 5*Power(t2,2)) + 
               t1*(1 - 22*t2 + Power(t2,2) + 2*Power(t2,3))) - 
            Power(s2,2)*(-4 + 88*t2 + 78*Power(t2,2) + 13*Power(t2,3) - 
               7*Power(t2,4) - 2*Power(t2,5) + Power(t1,4)*(4 + t2) + 
               Power(t1,2)*t2*(7 + 6*t2 - 6*Power(t2,2)) + 
               Power(t1,3)*(-8 - t2 + Power(t2,2)) + 
               t1*(8 - 95*t2 - 89*Power(t2,2) - 3*Power(t2,3) + 
                  6*Power(t2,4))) + 
            s2*(-29 + Power(t1,5) - 14*t2 - 18*Power(t2,2) - 
               56*Power(t2,3) + 5*Power(t2,4) + 10*Power(t2,5) + 
               Power(t1,4)*(2 + 3*t2 + Power(t2,2)) + 
               Power(t1,3)*(17 + 8*t2 - 12*Power(t2,2) - 4*Power(t2,3)) + 
               Power(t1,2)*(-73 - 135*t2 - 27*Power(t2,2) + 
                  30*Power(t2,3) + 5*Power(t2,4)) - 
               2*t1*(-41 - 69*t2 - 54*Power(t2,2) - 2*Power(t2,3) + 
                  16*Power(t2,4) + Power(t2,5))) + 
            Power(s1,4)*(Power(s2,3) + Power(s2,2)*(-3 + 5*t1 + 2*t2) + 
               t1*(-2 + 3*Power(t1,2) + t1*(-4 + t2) + 6*t2) - 
               s2*(1 + 9*Power(t1,2) - 6*t2 + t1*(7 + 2*t2))) + 
            Power(s1,3)*(1 + Power(s2,4) + 4*Power(t1,4) + 
               Power(s2,3)*(5 + 8*t1 - 2*t2) - 5*t2 - 5*Power(t2,2) - 
               Power(t1,3)*(9 + 10*t2) + 
               Power(t1,2)*(4 + 9*t2 + 3*Power(t2,2)) - 
               2*t1*(10 - 13*t2 + 8*Power(t2,2)) - 
               Power(s2,2)*(-23 + 9*Power(t1,2) + 2*t2 + 8*Power(t2,2) + 
                  t1*(6 + 5*t2)) + 
               s2*(-4 - 4*Power(t1,3) - 7*t2 - 28*Power(t2,2) + 
                  Power(t1,2)*(-27 + 19*t2) + 
                  t1*(41 + 61*t2 + 2*Power(t2,2)))) + 
            Power(s1,2)*(7 + Power(t1,4)*(2 - 6*t2) + 
               Power(s2,4)*(8 + 4*t1 - 3*t2) + 32*t2 + 8*Power(t2,2) + 
               15*Power(t2,3) + Power(t1,3)*(-25 + t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(16 + 59*t2 - 5*Power(t2,3)) + 
               t1*(-28 - 18*t2 - 61*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s2,3)*(-29 + 2*Power(t1,2) - 25*t2 - 
                  t1*(5 + 14*t2)) + 
               Power(s2,2)*(-27 - 12*Power(t1,3) - 65*t2 + 
                  20*Power(t2,2) + 12*Power(t2,3) + 
                  Power(t1,2)*(19 + 26*t2) + 
                  t1*(24 + 19*t2 - 11*Power(t2,2))) + 
               s2*(-19 + 6*Power(t1,4) - 31*t2 + 22*Power(t2,2) + 
                  48*Power(t2,3) - 2*Power(t1,3)*(21 + t2) + 
                  Power(t1,2)*(65 + 97*t2 - 6*Power(t2,2)) - 
                  t1*(-42 + 102*t2 + 133*Power(t2,2) + 4*Power(t2,3)))) + 
            s1*(-5 + 2*Power(t1,5) + 15*t2 - 52*Power(t2,2) - 
               Power(t2,3) - 15*Power(t2,4) + 
               2*Power(t1,4)*(-15 - 3*t2 + Power(t2,2)) + 
               Power(t1,3)*(42 + 64*t2 + 15*Power(t2,2) - 4*Power(t2,3)) + 
               t1*(26 + 32*t2 + 52*Power(t2,2) + 52*Power(t2,3)) + 
               Power(t1,2)*(-35 - 49*t2 - 93*Power(t2,2) - 
                  11*Power(t2,3) + 2*Power(t2,4)) + 
               Power(s2,4)*(-1 + 3*Power(t1,2) - 16*t2 + 3*Power(t2,2) - 
                  2*t1*(1 + 4*t2)) + 
               Power(s2,3)*(-12 - 5*Power(t1,3) + 57*t2 + 
                  35*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t1,2)*(22 + 3*t2) + t1*(-5 + 4*t2 + 4*Power(t2,2))\
) + Power(s2,2)*(57 + 2*Power(t1,4) + 115*t2 + 55*Power(t2,2) - 
                  22*Power(t2,3) - 8*Power(t2,4) + 
                  Power(t1,3)*(-13 + 11*t2) + 
                  Power(t1,2)*(-3 + t2 - 23*Power(t2,2)) + 
                  t1*(-43 - 135*t2 - 16*Power(t2,2) + 17*Power(t2,3))) + 
               s2*(15 + 29*t2 + 91*Power(t2,2) - 19*Power(t2,3) - 
                  36*Power(t2,4) - 2*Power(t1,4)*(2 + 3*t2) + 
                  Power(t1,3)*(26 + 43*t2 + 10*Power(t2,2)) - 
                  Power(t1,2)*
                   (-71 + 27*t2 + 100*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(-108 - 143*t2 + 57*Power(t2,2) + 111*Power(t2,3) + 
                     5*Power(t2,4))))))*T5(1 - s1 - t1 + t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s1 + t1 - t2)*
       Power(1 - s + s*s2 - s1*s2 - t1 + s2*t2,2)));
   return a;
};
