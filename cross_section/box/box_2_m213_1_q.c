#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m213_1_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(8*Power(s,3)*s1*t2*(s1 + Power(s1,2) - 3*s1*t2 + 
            t2*(-1 + 2*t2)) - s1*Power(s1 - t2,2)*(-1 + t2)*t2*
          (11 + 2*Power(s1,3) - 2*s2 + 5*t1 + 
            Power(s1,2)*(6 - 9*s2 + 11*t1 - 10*t2) - 9*t2 - 7*s2*t2 + 
            6*t1*t2 - 18*Power(t2,2) + 
            s1*(1 + 11*t2 + 6*Power(t2,2) - 2*t1*(8 + 3*t2) + 
               s2*(11 + 7*t2))) - 
         Power(s,2)*t2*((-1 + t2)*t2 + 
            Power(s1,4)*(2 + 4*s2 - 4*t1 + 6*t2) - 
            Power(s1,3)*(-11 + t1*(3 - 15*t2) + 4*(8 + 3*s2)*t2 + 
               11*Power(t2,2)) + 
            s1*(-5 + (-11 + 4*s2)*t2 + (45 - 8*s2)*Power(t2,2) - 
               45*Power(t2,3) + t1*(6 - 7*t2 + 5*Power(t2,2))) + 
            Power(s1,2)*(16 + t1 - 50*t2 - 8*t1*t2 + 69*Power(t2,2) - 
               5*t1*Power(t2,2) + 5*Power(t2,3) + 
               s2*(-4 + 8*t2 + 8*Power(t2,2)))) + 
         s*(s1 - t2)*(Power(-1 + t2,2)*t2 + 
            4*Power(s1,4)*t2*(-2 + s2 - t1 + 2*t2) + 
            Power(s1,2)*(2 + (-7*s2 + 11*(-2 + t1))*t2 + 
               (-27 + 12*s2 - 20*t1)*Power(t2,2) + 
               (36 + 15*s2 - 11*t1)*Power(t2,3) + 11*Power(t2,4)) + 
            Power(s1,3)*(2 + (7 + 3*s2)*t2 + (10 - 19*s2)*Power(t2,2) - 
               19*Power(t2,3) + t1*(1 - 9*t2 + 24*Power(t2,2))) + 
            s1*(t2*(-2 + (11 + 7*s2)*t2 + (38 - 15*s2)*Power(t2,2) - 
                  47*Power(t2,3)) + 
               t1*(-1 + 2*t2 - 4*Power(t2,2) + 11*Power(t2,3))))))/
     (s*Power(-1 + s1,2)*s1*Power(s1 - t2,2)*(-s + s1 - t2)*(-1 + t2)*t2) + 
    (8*(2 - 2*t1 - 4*Power(s1,4)*(-1 + t2) + 39*t2 - 6*s2*t2 - 9*t1*t2 + 
         8*Power(s,3)*(-2 + t2)*t2 - 2*Power(t2,2) + 9*s2*Power(t2,2) + 
         9*t1*Power(t2,2) - 27*Power(t2,3) - 5*s2*Power(t2,3) + 
         4*t1*Power(t2,3) - 12*Power(t2,4) + 
         Power(s1,3)*(s2*(-7 + 11*t2 - 2*Power(t2,2)) + 
            20*(-1 + Power(t2,2)) + t1*(7 - 11*t2 + 2*Power(t2,2))) + 
         Power(s1,2)*(31 + 14*t2 - 23*Power(t2,2) - 22*Power(t2,3) + 
            t1*(-16 + 9*t2 + 13*Power(t2,2)) + 
            s2*(1 + 5*t2 - 14*Power(t2,2) + 2*Power(t2,3))) + 
         s1*(-41 - 33*t2 + 37*Power(t2,2) + 33*Power(t2,3) + 
            4*Power(t2,4) + t1*
             (11 + 11*t2 - 24*Power(t2,2) - 4*Power(t2,3)) + 
            s2*(6 - 10*t2 + 7*Power(t2,2) + 3*Power(t2,3))) + 
         2*Power(s,2)*(t1*(-1 + t2 - Power(t2,2)) + 
            t2*(31 + 2*s2*(-2 + t2) - 23*t2 + 5*Power(t2,2)) - 
            s1*(8 - 21*t2 - 4*s2*t2 + Power(t2,2) + 2*s2*Power(t2,2) + 
               Power(t2,3) + t1*(-1 + t2 - Power(t2,2)))) + 
         s*(-2 - 84*t2 + 4*Power(s1,3)*t2 + 14*s2*t2 + 33*Power(t2,2) - 
            18*s2*Power(t2,2) - 37*Power(t2,3) + 2*s2*Power(t2,3) + 
            t1*(4 + 7*t2 + 9*Power(t2,2)) + 
            Power(s1,2)*(16 - 31*t2 - 21*Power(t2,2) + 2*Power(t2,3) + 
               t1*(-5 + 13*t2 - 4*Power(t2,2)) + 
               2*s2*(5 - 9*t2 + 3*Power(t2,2))) + 
            s1*(50 + t1 - 57*t2 - 20*t1*t2 + 52*Power(t2,2) - 
               5*t1*Power(t2,2) + 11*Power(t2,3) - 
               2*s2*(5 - 2*t2 - 6*Power(t2,2) + Power(t2,3)))))*B1(s,t2,s1)\
)/(Power(-1 + s1,2)*(-s + s1 - t2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) \
- (16*(Power(s1,7)*(-3 + s + 5*s2 - 5*t1 - 3*t2) - 
         Power(-1 + s,2)*Power(t2,2)*(-1 + s + t2) + 
         Power(s1,6)*(4 - 2*Power(s,2) + 15*t1 + 18*t2 + t1*t2 + 
            6*Power(t2,2) - 4*s2*(2 + 3*t2) + 
            s*(21 - 8*s2 + 15*t1 + 5*t2)) + 
         Power(s1,5)*(-14 + Power(s,3) - 14*t1 + 
            Power(s,2)*(-19 + 5*s2 - 15*t1 - t2) - 9*t2 - 3*t1*t2 - 
            20*Power(t2,2) + 4*t1*Power(t2,2) - 3*Power(t2,3) + 
            s2*(6 + 14*t2 + 9*Power(t2,2)) - 
            s*(38 + 42*t2 + 12*Power(t2,2) - 12*s2*(1 + t2) + 
               t1*(10 + t2))) - 
         Power(s1,2)*(-2 + t1 - 18*t2 + 2*s2*t2 + 3*t1*t2 - 
            2*Power(t2,2) + 4*t1*Power(t2,2) + 42*Power(t2,3) - 
            6*s2*Power(t2,3) + 
            Power(s,3)*(-3 + 5*t1 + 7*t2 - (5 + 2*s2)*Power(t2,2)) + 
            s*(1 + (33 - 4*s2)*t2 + 12*Power(t2,2) + 
               4*(-3 + s2)*Power(t2,3) + 2*Power(t2,4) + 
               t1*(3 - 2*t2 - 8*Power(t2,2))) - 
            Power(s,2)*(-4 - 2*(-9 + s2)*t2 + (-11 + 2*s2)*Power(t2,2) + 
               (3 + 2*s2)*Power(t2,3) + t1*(9 + t2 - 4*Power(t2,2)))) + 
         Power(s1,3)*(-11 + 4*Power(s,4) + s2 + 3*t1 - 37*t2 + 6*s2*t2 + 
            2*t1*t2 + 38*Power(t2,2) - 6*s2*Power(t2,2) + 
            12*t1*Power(t2,2) + 26*Power(t2,3) - 2*s2*Power(t2,3) - 
            2*Power(t2,4) + Power(s,3)*(-13 + 2*s2 + (-8 + t1)*t2) + 
            s*(-1 + 12*t2 + 8*s2*Power(t2,2) + 10*Power(t2,3) + 
               2*t1*(5 + 2*t2)) + 
            Power(s,2)*(1 + 14*t2 - 3*Power(t2,3) + 
               s2*(-3 + 2*t2 - 7*Power(t2,2)) + 
               t1*(11 - 3*t2 + 4*Power(t2,2)))) - 
         Power(s1,4)*(-22 + 4*s2 - 2*t1 - 16*t2 + 6*s2*t2 - 2*t1*t2 + 
            21*Power(t2,2) + 4*s2*Power(t2,2) + 12*t1*Power(t2,2) - 
            7*Power(t2,3) + 2*s2*Power(t2,3) + 
            Power(s,3)*(-1 + 2*s2 - 5*t1 + t2) + 
            Power(s,2)*(8 + 2*s2 - 28*t2 - 6*Power(t2,2) + 
               t1*(5 + t2)) + 
            s*(-22 - 40*t2 - 7*Power(t2,2) - 6*Power(t2,3) + 
               4*s2*(1 + 4*t2 + Power(t2,2)) + 
               2*t1*(6 + t2 + 4*Power(t2,2)))) + 
         s1*t2*(-3 + t1 - 6*t2 - 4*Power(s,4)*t2 + s2*t2 + 
            13*Power(t2,2) - 2*s2*Power(t2,2) + 2*Power(t2,3) - 
            Power(s,3)*(-4 + t1 + 2*(-10 + s2)*t2 + 4*Power(t2,2)) + 
            Power(s,2)*(-11 + 3*t1 + 5*(-6 + s2)*t2 + 
               (17 - 2*s2)*Power(t2,2)) + 
            s*(-3*t1 - 2*(-5 + 2*(-5 + s2)*t2 + (11 - 2*s2)*Power(t2,2) + 
                  Power(t2,3)))))*R1q(s1))/
     (Power(-1 + s1,2)*s1*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2))*Power(s1 - t2,3)*(-s + s1 - t2)) + 
    (16*(Power(s1,3)*(-2 + (14 + 8*s + s2 + t1)*t2 + 
            (-64 + 8*Power(s,2) + 15*s2 + 4*s*(-4 + s2 - 3*t1) - 39*t1)*
             Power(t2,2) + (22 + 19*s2 + 4*s*(10 + 3*s2 - t1) - 5*t1)*
             Power(t2,3) + (18 + 8*s + 21*s2 - 13*t1)*Power(t2,4) + 
            12*Power(t2,5)) + 
         2*Power(s1,5)*t2*(-2 + t2 + 2*s2*t2 + Power(t2,2) - 
            t1*(1 + t2)) + Power(s1,4)*
          (-2 + (13 + 4*s - 2*s2)*t2 - 
            (-7 + 10*s + 8*s2 + 4*s*s2)*Power(t2,2) - 
            (9 + 2*s + 14*s2)*Power(t2,3) - 9*Power(t2,4) + 
            t1*(1 + t2)*(-1 + 2*(4 + s)*t2 + 5*Power(t2,2))) + 
         Power(t2,2)*(-1 - t2 + s2*t2 + 21*Power(t2,2) - 
            7*s2*Power(t2,2) - 23*Power(t2,3) + 3*s2*Power(t2,3) + 
            4*Power(t2,4) - 5*s2*Power(t2,4) - 
            8*Power(s,2)*t2*(1 - 2*t2 + 2*Power(t2,2)) + 
            2*t1*(1 - 3*t2 + 5*Power(t2,2) + Power(t2,3)) + 
            s*(6 + (7 - 4*s2)*t2 + (-45 + 8*s2)*Power(t2,2) + 
               (35 - 8*s2)*Power(t2,3) - 19*Power(t2,4) + 
               t1*(-5 + 11*t2 - 5*Power(t2,2) + 3*Power(t2,3)))) + 
         s1*t2*(1 + (3 - 23*s + 8*Power(s,2) - 2*s2 + 4*s*s2)*t2 + 
            (-26 + 58*s - 16*Power(s,2) + 15*s2 - 4*s*s2)*Power(t2,2) + 
            (-18 - 20*s + 32*Power(s,2) + 5*s2 + 8*s*s2)*Power(t2,3) + 
            (43 + 38*s + 13*s2 + 8*s*s2)*Power(t2,4) + 
            (-3 + 3*s + 5*s2)*Power(t2,5) - 
            t1*(s*(1 - 2*t2 + 8*Power(t2,2) + 6*Power(t2,3) + 
                  3*Power(t2,4)) + 
               3*(1 - 3*t2 + 9*Power(t2,2) + Power(t2,3) + 4*Power(t2,4))\
)) + Power(s1,2)*(t1*(1 - (3 + s)*t2 + (17 + 13*s)*Power(t2,2) + 
               (33 + s)*Power(t2,3) + (6 + 11*s)*Power(t2,4) + 
               10*Power(t2,5)) - 
            t2*(24*Power(s,2)*Power(t2,2) + 
               t2*(7 - 92*t2 + 72*Power(t2,2) + 8*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s*(-4 + (21 + 4*s2)*t2 + (-9 + 4*s2)*Power(t2,2) + 
                  (55 + 16*s2)*Power(t2,3) + 9*Power(t2,4)) + 
               s2*(-1 + 9*t2 + 21*Power(t2,2) + 19*Power(t2,3) + 
                  16*Power(t2,4)))))*R1q(t2))/
     (Power(-1 + s1,2)*Power(s1 - t2,3)*(-s + s1 - t2)*Power(-1 + t2,2)*t2) \
+ (16*(12*Power(s,4) + Power(s,3)*
          (-24 + 2*Power(s1,2) + 6*s2 - 5*t1 + 21*t2 - 
            s1*(22 + 6*s2 - 5*t1 + 5*t2)) + 
         Power(s,2)*(13 - 5*Power(s1,3) - 9*s2 + 7*t1 - 31*t2 + 
            3*s2*t2 - 2*t1*t2 + 6*Power(t2,2) + 
            Power(s1,2)*(12*s2 - 15*t1 + 14*(1 + t2)) - 
            s1*(42 + 31*t2 + 2*Power(t2,2) + 3*s2*(1 + t2) - 
               2*t1*(4 + t2))) - 
         Power(-1 + s1,2)*(5 + Power(s1,3) - s2 + 3*t1 + 
            Power(s1,2)*(2 - 4*s2 + 5*t1 - 4*t2) - 7*t2 - 3*s2*t2 + 
            2*t1*t2 - 6*Power(t2,2) + 
            s1*(4 + 3*t2 + 2*Power(t2,2) - 2*t1*(4 + t2) + s2*(5 + 3*t2))\
) + s*(8 + 4*Power(s1,4) + t1 + Power(s1,3)*(-10*s2 + 15*t1 - 13*t2) + 
            s2*(2 - 6*t2) + 7*t2 + 4*t1*t2 - 12*Power(t2,2) + 
            s1*(20 - 2*s2 + 5*t1 - 55*t2 - 8*Power(t2,2)) + 
            Power(s1,2)*(16 - 21*t1 + 29*t2 - 4*t1*t2 + 4*Power(t2,2) + 
               2*s2*(5 + 3*t2))))*R2q(s))/
     (s*Power(-1 + s1,2)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2))*(-s + s1 - t2)) - 
    (8*(8*Power(s,6)*t2 + Power(-1 + s1,4)*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2)*
          (-2 + t2 + s1*(-1 + 2*t2)) + 
         2*Power(s,5)*(t1 - t1*t2 + t2*(-21 + 2*s2 + 5*t2) + 
            s1*(4 + t1*(-1 + t2) - 11*t2 - 2*s2*t2 - Power(t2,2))) + 
         Power(s,4)*(2 + 66*t2 - 14*s2*t2 - 53*Power(t2,2) + 
            2*s2*Power(t2,2) + t1*(-10 + 13*t2) + 
            Power(s1,2)*(-12 + 9*t1 + 15*t2 - 10*t1*t2 + 
               8*Power(t2,2) + 2*s2*(-3 + 7*t2)) + 
            s1*(-38 + t1 + 39*t2 - 3*t1*t2 - 11*Power(t2,2) - 
               2*s2*(-3 + Power(t2,2)))) + 
         Power(s,3)*(-8 + 20*t1 - 31*t2 + 12*s2*t2 - 28*t1*t2 + 
            70*Power(t2,2) - 5*s2*Power(t2,2) + 4*t1*Power(t2,2) - 
            12*Power(t2,3) + Power(s1,2)*
             (-11 + s2*Power(1 - 2*t2,2) + 31*t2 - 7*Power(t2,2) + 
               4*t1*(-2 + 3*t2)) + 
            Power(s1,3)*(3*(5 - 4*t2)*t2 - 5*s2*(-3 + 4*t2) + 
               4*t1*(-4 + 5*t2)) + 
            s1*(43 + 89*t2 + 21*Power(t2,2) + 4*Power(t2,3) - 
               4*t1*(-1 + t2 + Power(t2,2)) + 
               s2*(-16 + 12*t2 + Power(t2,2)))) - 
         s*Power(-1 + s1,2)*(8 - 21*t2 + 8*s2*t2 - 5*s2*Power(t2,2) + 
            16*Power(t2,3) - 2*t1*(5 - 5*t2 + 2*Power(t2,2)) + 
            Power(s1,3)*(8 + t1*(6 - 10*t2) - 11*t2 + 2*Power(t2,2) + 
               s2*(-5 + 8*t2)) + 
            Power(s1,2)*(9 - 2*t1*(-3 + t2) - 19*t2 + 15*Power(t2,2) + 
               s2*(5 - 4*t2 - 4*Power(t2,2))) + 
            s1*(11 - 5*t2 - 13*Power(t2,2) + 
               2*t1*(-1 + t2 + 2*Power(t2,2)) + 
               s2*(-4 - 4*t2 + 5*Power(t2,2)))) + 
         Power(s,2)*(12 - 19*t2 + 4*s2*t2 - 36*Power(t2,2) - 
            s2*Power(t2,2) + 20*Power(t2,3) + 
            t1*(-20 + 26*t2 - 8*Power(t2,2)) + 
            Power(s1,4)*(12 + t1*(14 - 20*t2) - 27*t2 + 8*Power(t2,2) + 
               s2*(-13 + 16*t2)) + 
            s1*(-19 + t1*(6 - 10*t2) - 78*t2 + 125*Power(t2,2) + 
               16*Power(t2,3) + 2*s2*(4 - 11*t2 + Power(t2,2))) + 
            Power(s1,3)*(17 - 22*t2 + 17*Power(t2,2) + 2*t1*(1 + t2) - 
               2*s2*(-3 + 7*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(s2*(-1 + 16*t2 + 3*Power(t2,2)) + 
               2*(69 - 71*t2 - 9*Power(t2,2) - 2*Power(t2,3) + 
                  t1*(-1 + t2 + 4*Power(t2,2))))))*T2q(s1,s))/
     (s*Power(-1 + s1,2)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2))*(-s + s1 - t2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))\
) + (8*(8*Power(s,4)*(1 + s1)*(s1 - t2)*t2 + 
         (-1 + s1)*Power(s1 - t2,3)*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2)*
          (-2 + t2 + s1*(-1 + 2*t2)) - 
         4*Power(s,3)*(Power(s1,3)*(-2 + s2*t2 - t1*t2) + 
            s1*t2*(7 - s2 + 2*t1 - 3*t2 - t1*t2 + 2*Power(t2,2)) - 
            Power(s1,2)*(2 + 2*(-3 + t1)*t2 + (3 + s2)*Power(t2,2)) + 
            t2*(-1 + t1 - 8*t2 + s2*t2 + t1*t2 + 2*Power(t2,2) + 
               2*Power(t2,3))) - 
         s*Power(s1 - t2,2)*(-4 + 4*t1 + 2*t2 - 4*s2*t2 - 8*t1*t2 + 
            8*Power(t2,2) + 2*s2*Power(t2,2) + 3*t1*Power(t2,2) - 
            3*Power(t2,3) + 2*s2*Power(t2,3) + 
            Power(s1,3)*(12 + t1*(7 - 8*t2) + 6*s2*(-1 + t2) - 11*t2 - 
               2*Power(t2,2)) + 
            Power(s1,2)*(2 - 5*t2 + 6*Power(t2,2) + 2*Power(t2,3) + 
               t1*(5 - 4*Power(t2,2)) + s2*(2 + 2*t2 - 4*Power(t2,2))) + 
            s1*(22 - 34*t2 - 12*Power(t2,2) + 17*Power(t2,3) + 
               t1*(-16 + 16*t2 + Power(t2,2)) - 
               2*s2*(-2 + 2*t2 - Power(t2,2) + Power(t2,3)))) - 
         2*Power(s,2)*(s1 - t2)*
          (-2 - 12*t2 + 2*s2*t2 + 10*Power(t2,2) - 4*s2*Power(t2,2) + 
            5*Power(t2,3) - 2*s2*Power(t2,3) - 5*Power(t2,4) + 
            t1*(2 + 2*t2 - 3*Power(t2,2) + Power(t2,3)) + 
            Power(s1,3)*(-4 + s2*(2 - 4*t2) + 9*t2 + Power(t2,2) + 
               t1*(-3 + 5*t2)) + 
            Power(s1,2)*((-5 + 2*s2 - 2*t2)*(-1 + t2)*t2 + 
               t1*(-3 + 7*t2 + 2*Power(t2,2))) + 
            s1*(6 + 10*t2 - 12*Power(t2,2) - 7*Power(t2,3) + 
               Power(t2,4) + t1*(4 - 14*t2 + Power(t2,2) - Power(t2,3)) + 
               2*s2*(-1 + 2*t2 + Power(t2,2) + Power(t2,3)))))*T3q(t2,s1))/
     (s*Power(-1 + s1,2)*Power(s1 - t2,2)*(-s + s1 - t2)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) + 
    (8*(-16*Power(s,4)*t2 + (-1 + s1)*Power(-1 + t2,2)*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2)*
          (-2 + t2 + s1*(-1 + 2*t2)) + 
         8*Power(s,3)*(-(t2*(-1 + s2 - t1 + 4*t2 + Power(t2,2))) + 
            s1*(-2 + (3 + s2 - t1)*t2 + Power(t2,2))) + 
         s*(-1 + t2)*(-2 + 4*t1 + 56*t2 - 18*s2*t2 + 7*t1*t2 - 
            35*Power(t2,2) + 2*s2*Power(t2,2) + 7*t1*Power(t2,2) - 
            19*Power(t2,3) - 2*s2*Power(t2,3) + 
            8*Power(s1,3)*(-1 + s2 - t1 + t2) - 
            Power(s1,2)*(-24 + 11*t2 + 11*Power(t2,2) + 2*Power(t2,3) - 
               t1*(19 + 11*t2 + 4*Power(t2,2)) + 
               s2*(22 + 6*t2 + 6*Power(t2,2))) + 
            s1*(-46 - 5*t2 + 46*Power(t2,2) + 5*Power(t2,3) - 
               t1*(15 + 18*t2 + 11*Power(t2,2)) + 
               2*s2*(7 + 12*t2 + 2*Power(t2,2) + Power(t2,3)))) - 
         2*Power(s,2)*(-(t1*(1 + 6*Power(t2,2) + Power(t2,3))) - 
            4*Power(s1,2)*(1 + s2 + t2 - 2*s2*t2 - 2*Power(t2,2) + 
               t1*(-1 + 2*t2)) + 
            t2*(-29 + 12*t2 + 12*Power(t2,2) + 5*Power(t2,3) + 
               2*s2*(2 + t2 + Power(t2,2))) - 
            s1*(-4 - 21*t2 + 16*Power(t2,2) + 8*Power(t2,3) + 
               Power(t2,4) + 2*s2*
                (-2 + 6*t2 + Power(t2,2) + Power(t2,3)) - 
               t1*(-3 + 8*t2 + 6*Power(t2,2) + Power(t2,3)))))*T4q(-1 + t2))/
     (s*Power(-1 + s1,2)*(-s + s1 - t2)*(-1 + t2)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) - 
    (8*(8*Power(s,4)*t2 + 2*Power(s,3)*
          (-(t1*(1 + t2)) + t2*(3 + 2*s2 + 5*t2) + 
            s1*(4 + t1 - 3*t2 - 2*s2*t2 + t1*t2 - Power(t2,2))) - 
         4*(s1 - t2)*(-1 + t2)*
          (-1 + t1 - 5*t2 + s2*t2 - 2*t1*t2 + 6*Power(t2,2) + 
            Power(s1,2)*(-2 + s2 - t1 + 2*t2) - 
            s1*(-7 + s2 + 5*t2 + s2*t2 - 2*t1*t2 + 2*Power(t2,2))) + 
         Power(s,2)*(-2 + 4*t1 - 36*t2 + 2*s2*t2 - 9*t1*t2 + 
            45*Power(t2,2) + 2*s2*Power(t2,2) + 
            Power(s1,2)*(t1 - 4*t1*t2 + t2*(-5 + 2*t2) + 
               s2*(-2 + 6*t2)) - 
            s1*(-18 + t1*(5 - 13*t2) + 31*t2 + 7*Power(t2,2) + 
               2*s2*(-1 + 4*t2 + Power(t2,2)))) + 
         s*(2 + 19*t2 - 6*s2*t2 - 77*Power(t2,2) + 9*s2*Power(t2,2) + 
            56*Power(t2,3) + Power(s1,3)*
             (-4 - 3*t1 + s2*(3 - 2*t2) + 4*t2 + 2*t1*t2) + 
            t1*(-2 + 15*t2 - 16*Power(t2,2)) + 
            Power(s1,2)*(-11 + t1*(12 - 13*t2) + t2 + 10*Power(t2,2) + 
               s2*(-9 + 8*t2 + 2*Power(t2,2))) - 
            s1*(27 - 96*t2 + 53*Power(t2,2) + 16*Power(t2,3) + 
               t1*(7 + 4*t2 - 16*Power(t2,2)) + s2*(-6 + 11*Power(t2,2)))))*
       T5q(s))/(s*Power(-1 + s1,2)*(-s + s1 - t2)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))));
   return a;
};
