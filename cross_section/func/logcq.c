int
logq_c(const __float128 zr, const __float128 zi,  __float128 * lnr, __float128 * theta)
{
    if(zr != 0.0 || zi != 0.0) {
    const __float128 ax = fabsq(zr);
    const __float128 ay = fabsq(zi);
    const __float128 min = fminq(ax, ay);
    const __float128 max = fmaxq(ax, ay);
    
    (* lnr) = logq(max) + 0.5Q * logq(1.0Q + (min/max)*(min/max));
    (* theta) = atan2q(zi, zr);
    
    return 1;
  }
  else {
    return 0;
  }
}



