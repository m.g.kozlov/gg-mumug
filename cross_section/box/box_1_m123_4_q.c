#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m123_4_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(2*Power(s,6)*(-1 + s1)*s1 + 
         Power(s,5)*s1*(10 + 2*Power(s1,2) - 13*t1 + 4*Power(t1,2) + 
            3*t2 - 8*t1*t2 + 4*Power(t2,2) + s2*(8 - 4*t1 + 4*t2) - 
            s1*(12 + 8*s2 - 17*t1 + 7*t2)) + 
         Power(s,4)*(-12*Power(s1,4) + (2 + t2)*(-1 + s2 - t1 + t2) + 
            Power(s1,3)*(18 + 13*s2 - 33*t1 + 50*t2) + 
            Power(s1,2)*(30 + 6*Power(s2,2) + 9*Power(t1,2) + 
               s2*(6 - 15*t1 - 20*t2) - 50*t2 - 51*Power(t2,2) + 
               t1*(-17 + 62*t2)) + 
            s1*(-26 + 4*Power(t1,3) + Power(s2,2)*(2 + 4*t1 - 4*t2) - 
               t2 + 26*Power(t2,2) + 16*Power(t2,3) + 
               Power(t1,2)*(-13 + 8*t2) + 
               t1*(52 - 33*t2 - 28*Power(t2,2)) + 
               s2*(-37 - 8*Power(t1,2) + t1*(11 - 4*t2) + 23*t2 + 
                  12*Power(t2,2)))) + 
         Power(s,3)*(10*Power(s1,5) + 
            Power(s1,4)*(18 - 2*s2 + 15*t1 - 71*t2) + 
            2*(-1 + s2 - t1 + t2)*(-5 + 2*t2 + 2*Power(t2,2)) - 
            Power(s1,3)*(58 + 13*Power(s2,2) + 31*Power(t1,2) - 4*t2 - 
               130*Power(t2,2) + 11*s2*(4 - 4*t1 + t2) + t1*(-78 + 51*t2)\
) + Power(s1,2)*(-40 - 4*Power(t1,3) + 151*t2 - 67*Power(t2,2) - 
               93*Power(t2,3) + Power(s2,2)*(-4*t1 + 25*t2) + 
               Power(t1,2)*(1 + 45*t2) + 
               t1*(-26 - 82*t2 + 72*Power(t2,2)) + 
               s2*(14 + 8*Power(t1,2) + 37*t2 + 2*Power(t2,2) - 
                  t1*(1 + 70*t2))) + 
            s1*(36 - 8*Power(s2,3) - 14*t2 - 75*Power(t2,2) + 
               45*Power(t2,3) + 24*Power(t2,4) + 
               8*Power(t1,3)*(-1 + 2*t2) + 
               Power(t1,2)*(18 - 25*t2 - 8*Power(t2,2)) - 
               t1*(101 - 161*t2 + 32*Power(t2,2) + 32*Power(t2,3)) + 
               Power(s2,2)*(5 + 3*t2 - 16*Power(t2,2) + 
                  8*t1*(1 + 2*t2)) + 
               s2*(82 + Power(t1,2)*(8 - 32*t2) - 114*t2 + 
                  26*Power(t2,2) + 8*Power(t2,3) + 
                  t1*(-23 + 22*t2 + 24*Power(t2,2))))) - 
         s*(4*Power(s1,6)*(-1 + t2) - 
            2*(-1 + s2 - t1 + t2)*
             (-7 + 17*t2 - 10*Power(t2,2) - 2*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s1,5)*(Power(s2,2) + Power(t1,2) + 
               s2*(-2*t1 + 17*(-1 + t2)) - 13*t1*(-1 + t2) - 
               12*(1 - 3*t2 + 2*Power(t2,2))) + 
            Power(s1,4)*(-15 - 4*Power(t1,3) + 
               Power(t1,2)*(35 - 11*t2) + 38*t2 - 75*Power(t2,2) + 
               52*Power(t2,3) + t1*(-37 + 2*t2 + 35*Power(t2,2)) + 
               Power(s2,2)*(-4*t1 + 3*(7 + t2)) + 
               4*s2*(2*Power(t1,2) + 2*t1*(-7 + t2) + 
                  7*(1 + t2 - 2*Power(t2,2)))) + 
            Power(s1,3)*(-8 - 8*Power(s2,3) + 22*t2 - 4*Power(t2,2) + 
               42*Power(t2,3) - 52*Power(t2,4) + 
               8*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(29 - 116*t2 + 45*Power(t2,2)) - 
               t1*(100 - 175*t2 + 36*Power(t2,2) + 39*Power(t2,3)) + 
               Power(s2,2)*(11 - 60*t2 + 7*Power(t2,2) + 
                  8*t1*(3 + t2)) - 
               s2*(-110 + 173*t2 + 10*Power(t2,2) - 73*Power(t2,3) + 
                  8*Power(t1,2)*(3 + 2*t2) + 
                  4*t1*(10 - 44*t2 + 13*Power(t2,2)))) + 
            Power(s1,2)*(25 + 8*Power(s2,3) - 45*t2 + 52*Power(t2,2) - 
               77*Power(t2,3) + 21*Power(t2,4) + 24*Power(t2,5) + 
               4*Power(t1,3)*(4 - 10*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-74 + 36*t2 + 117*Power(t2,2) - 
                  63*Power(t2,3)) + 
               Power(s2,2)*(-85 + (88 - 40*t1)*t2 + 
                  4*(10 + 3*t1)*Power(t2,2) - 27*Power(t2,3)) + 
               5*t1*(-1 + 20*t2 - 22*Power(t2,2) - 2*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s2*(24 - 169*t2 + 174*Power(t2,2) + 17*Power(t2,3) - 
                  46*Power(t2,4) - 
                  8*Power(t1,2)*(3 - 10*t2 + 3*Power(t2,2)) + 
                  t1*(159 - 124*t2 - 157*Power(t2,2) + 90*Power(t2,3)))) \
+ s1*(8*Power(s2,3)*(5 - 8*t2 + 3*Power(t2,2)) - 
               8*Power(t1,3)*(4 - 3*t2 - 3*Power(t2,2) + 2*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-4 + 43*t2 - 15*Power(t2,2) + 28*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(t1,2)*(-47 + 151*t2 - 102*Power(t2,2) - 
                  29*Power(t2,3) + 28*Power(t2,4)) + 
               t1*(75 - 170*t2 + 119*Power(t2,2) - 43*Power(t2,3) + 
                  27*Power(t2,4) - 8*Power(t2,5)) + 
               Power(s2,2)*(-52 + 173*t2 - 143*Power(t2,2) + 
                  7*Power(t2,3) + 16*Power(t2,4) - 
                  8*t1*(14 - 19*t2 + 3*Power(t2,2) + 2*Power(t2,3))) + 
               s2*(-71 + 139*t2 - 48*Power(t2,2) - 18*Power(t2,3) - 
                  14*Power(t2,4) + 12*Power(t2,5) + 
                  8*Power(t1,2)*
                   (13 - 14*t2 - 3*Power(t2,2) + 4*Power(t2,3)) + 
                  t1*(99 - 324*t2 + 245*Power(t2,2) + 22*Power(t2,3) - 
                     44*Power(t2,4))))) - 
         Power(s,2)*(2*Power(s1,6) + 
            Power(s1,5)*(26 + 3*s2 - t1 - 32*t2) - 
            (-1 + s2 - t1 + t2)*(18 - 25*t2 + 6*Power(t2,3)) + 
            Power(s1,4)*(4 - 8*Power(s2,2) - 19*Power(t1,2) + 
               s2*(-13 + 27*t1 - 44*t2) - 81*t2 + 99*Power(t2,2) + 
               t1*(35 + 16*t2)) + 
            Power(s1,3)*(-45 + 4*Power(t1,3) + 88*t2 + 49*Power(t2,2) - 
               126*Power(t2,3) + Power(s2,2)*(-24 + 4*t1 + 23*t2) + 
               Power(t1,2)*(-52 + 69*t2) - 
               9*t1*(-11 + 15*t2 + Power(t2,2)) + 
               s2*(-89 - 8*Power(t1,2) + t1*(76 - 92*t2) + 61*t2 + 
                  79*Power(t2,2))) + 
            Power(s1,2)*(-29 + 119*t2 - 188*Power(t2,2) + 
               41*Power(t2,3) + 73*Power(t2,4) + 
               4*Power(t1,3)*(-5 + 3*t2) + 
               Power(t1,2)*(51 + 57*t2 - 81*Power(t2,2)) - 
               2*t1*(10 + 29*t2 - 42*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(53 + 20*t2 - 39*Power(t2,2) + 
                  4*t1*(-5 + 3*t2)) - 
               s2*(9 - 96*t2 + 35*Power(t2,2) + 46*Power(t2,3) + 
                  8*Power(t1,2)*(-5 + 3*t2) + 
                  t1*(104 + 77*t2 - 120*Power(t2,2)))) + 
            s1*(16 + 13*t2 - 93*Power(t2,2) + 119*Power(t2,3) - 
               39*Power(t2,4) - 16*Power(t2,5) + 
               8*Power(s2,3)*(-4 + 3*t2) + 
               Power(t1,3)*(12 + 24*t2 - 24*Power(t2,2)) + 
               Power(t1,2)*(32 - 78*t2 - 3*Power(t2,2) + 
                  32*Power(t2,3)) + 
               t1*(-123 + 250*t2 - 159*Power(t2,2) + 28*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s2,2)*(35 - 79*t2 + 3*Power(t2,2) + 
                  24*Power(t2,3) - 4*t1*(-19 + 6*t2 + 6*Power(t2,2))) + 
               s2*(8*Power(t1,2)*(-7 - 3*t2 + 6*Power(t2,2)) + 
                  t1*(-67 + 157*t2 - 56*Power(t2,3)) + 
                  2*(55 - 95*t2 + 44*Power(t2,2) - 10*Power(t2,3) + 
                     4*Power(t2,4))))) - 
         (-1 + t2)*(2*Power(s1,6)*(-1 + t2) - 
            (-1 + s2 - t1 + t2)*
             (-4 + 10*t2 - 6*Power(t2,2) - Power(t2,3) + Power(t2,4)) - 
            Power(s1,5)*(2 + 5*Power(s2,2) + 5*Power(t1,2) - 10*t2 + 
               8*Power(t2,2) - 2*s2*(-1 + 5*t1 + t2)) + 
            Power(s1,4)*(-11 - 4*Power(t1,3) + 12*t2 - 13*Power(t2,2) + 
               12*Power(t2,3) + Power(t1,2)*(22 + 8*t2) + 
               Power(s2,2)*(19 - 4*t1 + 11*t2) + 
               t1*(-5 + t2 + 4*Power(t2,2)) + 
               s2*(8*Power(t1,2) - t1*(41 + 19*t2) + 
                  5*(1 + t2 - 2*Power(t2,2)))) - 
            Power(s1,3)*(9 + 8*Power(s2,3) - 35*t2 + 17*Power(t2,2) + 
               Power(t2,3) + 8*Power(t2,4) - 4*Power(t1,3)*(3 + t2) + 
               Power(t1,2)*(-14 + 57*t2 - 7*Power(t2,2)) + 
               t1*(34 - 37*t2 - 9*Power(t2,2) + 12*Power(t2,3)) + 
               s2*(-40 + 49*t2 + 9*Power(t2,2) - 18*Power(t2,3) + 
                  8*Power(t1,2)*(4 + t2) + 4*t1*(5 - 24*t2 + Power(t2,2))\
) + Power(s2,2)*(-4*t1*(7 + t2) + 3*(-2 + 13*t2 + Power(t2,2)))) + 
            Power(s1,2)*(10 + 8*Power(s2,3) - 7*t2 + 
               4*Power(t1,3)*(-4 + t2)*t2 - 17*Power(t2,2) + 
               Power(t2,3) + 11*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,2)*(-39 + 26*t2 + 41*Power(t2,2) - 
                  18*Power(t2,3)) + 
               t1*(-11 + 59*t2 - 43*Power(t2,2) - 17*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,2)*(-44 + 48*t2 + 13*Power(t2,2) - 
                  7*Power(t2,3) + 4*t1*(-4 - 4*t2 + Power(t2,2))) + 
               s2*(15 - 79*t2 + 71*Power(t2,2) + 7*Power(t2,3) - 
                  14*Power(t2,4) - 
                  8*Power(t1,2)*(-1 - 4*t2 + Power(t2,2)) + 
                  t1*(83 - 74*t2 - 54*Power(t2,2) + 25*Power(t2,3)))) + 
            s1*(8*Power(s2,3)*(2 - 3*t2 + Power(t2,2)) - 
               4*Power(t1,3)*(4 - 4*t2 - Power(t2,2) + Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-2 + 14*t2 + 3*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(-24 + 71*t2 - 48*Power(t2,2) - 
                  6*Power(t2,3) + 8*Power(t2,4)) + 
               t1*(14 - 15*t2 - 16*Power(t2,2) + 14*Power(t2,3) + 
                  7*Power(t2,4) - 4*Power(t2,5)) + 
               Power(s2,2)*(-2 + t2)*
                (12 - 32*t2 + 15*Power(t2,2) + 4*Power(t2,3) - 
                  4*t1*(-6 + 5*t2 + Power(t2,2))) + 
               s2*(-14 + 11*t2 + 30*Power(t2,2) - 30*Power(t2,3) - 
                  Power(t2,4) + 4*Power(t2,5) + 
                  8*Power(t1,2)*(6 - 7*t2 + Power(t2,3)) - 
                  t1*(-48 + 147*t2 - 110*Power(t2,2) + Power(t2,3) + 
                     12*Power(t2,4)))))))/
     ((-1 + s1)*s1*(s - s2 + t1)*Power(-1 + s + t2,2)*(-1 + s - s1 + t2)*
       Power(s - s1 + t2,2)*(-1 + s2 - t1 + t2)) - 
    (8*(-24 + 4*Power(s,4)*(-1 + s1) + 32*s2 + 16*Power(s2,2) + 
         2*Power(s1,5)*(s2 - t1) - 32*t1 - 32*s2*t1 + 16*Power(t1,2) + 
         75*t2 - 18*s2*t2 - 16*Power(s2,2)*t2 + 15*t1*t2 + 22*s2*t1*t2 - 
         6*Power(t1,2)*t2 - 52*Power(t2,2) - 11*s2*Power(t2,2) + 
         2*Power(s2,2)*Power(t2,2) + 11*t1*Power(t2,2) + 
         6*s2*t1*Power(t2,2) - 8*Power(t1,2)*Power(t2,2) + Power(t2,3) - 
         s2*Power(t2,3) + 4*t1*Power(t2,3) + 
         Power(s,3)*(-4 - 7*t1 - 9*t2 + s1*(4 - 4*s2 + 15*t1 + t2) + 
            Power(s1,2)*(4 - 2*t1 + 2*t2)) + 
         2*Power(s1,4)*(-1 + Power(s2,2) - Power(t1,2) + t2 - 
            s2*(3 + 2*t2) + t1*(2 + 3*t2)) + 
         Power(s,2)*(-11 - 6*t1 - 10*Power(t1,2) + 
            s2*(16 + 10*t1 - 5*t2) + 
            2*Power(s1,3)*(-8 + s2 + t1 - 2*t2) + 4*t2 - 6*t1*t2 - 
            8*Power(t2,2) + Power(s1,2)*
             (-4 - 10*t1 - 6*Power(t1,2) + s2*(-9 + 6*t1 - 4*t2) + 
               21*t2 + 4*t1*t2 + 2*Power(t2,2)) + 
            s1*(41 + 20*t1 + 18*Power(t1,2) - 17*t2 + 14*t1*t2 - 
               8*Power(t2,2) + s2*(-19 - 18*t1 + t2))) + 
         Power(s1,3)*(6 + 4*Power(t1,3) + 
            Power(s2,2)*(-6 + 4*t1 - 4*t2) - 6*Power(t2,2) - 
            2*Power(t1,2)*(2 + t2) + t1*(15 - 3*t2 - 4*Power(t2,2)) + 
            s2*(-21 - 8*Power(t1,2) + 11*t2 + 2*Power(t2,2) + 
               2*t1*(5 + 3*t2))) + 
         Power(s1,2)*(19 + 8*Power(s2,3) - 26*t2 + 3*Power(t2,2) + 
            4*Power(t2,3) - 4*Power(t1,3)*(4 + t2) + 
            t1*(16 + 13*t2 - 5*Power(t2,2)) + 
            4*Power(t1,2)*(-3 + 5*t2 + Power(t2,2)) + 
            2*Power(s2,2)*(-9 + 12*t2 + Power(t2,2) - 2*t1*(8 + t2)) + 
            s2*(-15 - 7*t2 - 2*Power(t2,2) + 8*Power(t1,2)*(5 + t2) + 
               t1*(30 - 44*t2 - 6*Power(t2,2)))) + 
         s1*(21 - 16*Power(s2,3) - 15*t2 - 13*Power(t2,2) + 
            7*Power(t2,3) + 8*Power(t1,3)*(2 + t2) - 
            2*Power(t1,2)*(25 + 2*t2) - 
            t1*(39 - 93*t2 + 24*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s2,2)*(8*t1*(6 + t2) - 2*(20 + 4*t2 + Power(t2,2))) + 
            s2*(42 - 94*t2 + 21*Power(t2,2) + 5*Power(t2,3) - 
               16*Power(t1,2)*(3 + t2) + 2*t1*(45 + 6*t2 + Power(t2,2)))) \
+ s*(43 - 50*s2 - 16*Power(s2,2) + 47*t1 + 22*s2*t1 - 6*Power(t1,2) - 
            79*t2 + 5*s2*t2 + 2*Power(s2,2)*t2 + 5*t1*t2 + 16*s2*t1*t2 - 
            18*Power(t1,2)*t2 + 9*Power(t2,2) - 6*s2*Power(t2,2) + 
            5*t1*Power(t2,2) - 3*Power(t2,3) + 
            2*Power(s1,4)*(4 - 2*s2 + t1 + t2) - 
            Power(s1,3)*(2*Power(s2,2) - 8*Power(t1,2) + 
               s2*(-19 + 6*t1 - 8*t2) + t1*(9 + 10*t2) + 
               2*(-7 + 10*t2 + Power(t2,2))) + 
            Power(s1,2)*(-11 - 4*Power(t1,3) + 20*t2 + 11*Power(t2,2) - 
               2*Power(t1,2)*(11 + t2) + 
               Power(s2,2)*(-2 - 4*t1 + 2*t2) + 
               s2*(32 + 24*t1 + 8*Power(t1,2) - 39*t2 - 4*Power(t2,2)) + 
               t1*(-20 + 21*t2 + 6*Power(t2,2))) + 
            s1*(-64 + 8*Power(t1,3) + Power(s2,2)*(42 + 8*t1 - 2*t2) + 
               73*t2 + 4*Power(t2,2) - 5*Power(t2,3) + 
               2*Power(t1,2)*(23 + 9*t2) - 
               t1*(2 + 56*t2 + 5*Power(t2,2)) - 
               s2*(9 + 16*Power(t1,2) - 54*t2 - 10*Power(t2,2) + 
                  8*t1*(11 + 2*t2)))))*
       B1(1 - s2 + t1 - t2,s1,1 - s + s1 - t2))/
     ((s - s2 + t1)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       Power(s - s1 + t2,2)) - 
    (16*(2*Power(s,5)*s1*(-2 + s1 + Power(s1,2) + 2*s1*t1 - t2 - s1*t2) + 
         Power(s,4)*(2 - Power(s1,4) - 
            Power(s1,3)*(9 + 2*s2 + 4*t1 - 13*t2) + t2 + 
            s1*(3 - 15*t2 - 8*Power(t2,2) - 2*t1*(1 + t2) + 
               2*s2*(2 + t2)) + 
            Power(s1,2)*(13 + 4*Power(t1,2) - 2*s2*(5 + 2*t1 - t2) + 
               5*t2 - 8*Power(t2,2) + 2*t1*(1 + 7*t2))) + 
         Power(s,3)*(-10 - 4*Power(s1,5) + 
            Power(s1,4)*(6 + 5*t1 - 17*t2) + 4*t2 + 4*Power(t2,2) + 
            Power(s1,3)*(26 - 4*Power(t1,2) + 4*s2*(2 + t1 - 3*t2) - 
               17*t2 + 28*Power(t2,2) - t1*(1 + 8*t2)) + 
            s1*(42 + t2 - 28*Power(t2,2) - 12*Power(t2,3) + 
               t1*(17 - 8*Power(t2,2)) + 8*s2*(-3 + t2 + Power(t2,2))) + 
            Power(s1,2)*(-76 + 8*Power(s2,2) + 61*t2 + 4*Power(t2,2) - 
               12*Power(t2,3) + 4*Power(t1,2)*(-1 + 4*t2) - 
               4*s2*(-6 + t1 + 7*t2 + 4*t1*t2 - 2*Power(t2,2)) + 
               t1*(-21 + 8*t2 + 16*Power(t2,2)))) + 
         Power(s,2)*(18 + 3*Power(s1,6) - 25*t2 + 6*Power(t2,3) + 
            Power(s1,5)*(-3 + 3*s2 - 6*t1 + 10*t2) + 
            Power(s1,4)*(-13 - 6*t1 + 4*Power(t1,2) + 2*t2 - 
               34*Power(t2,2) + s2*(5 - 4*t1 + 11*t2)) + 
            s1*(-102 + 157*t2 - 4*Power(t2,2) - 34*Power(t2,3) - 
               8*Power(t2,4) - 
               4*t1*(11 - 10*t2 - 3*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(52 - 61*t2 + 12*Power(t2,3))) + 
            Power(s1,3)*(-7 + Power(t1,2)*(4 - 12*t2) + 53*t2 - 
               8*Power(t2,2) + 26*Power(t2,3) + t1*(22 + 4*t2) + 
               s2*(-27 + 13*t2 - 24*Power(t2,2) + 4*t1*(-1 + 3*t2))) + 
            Power(s1,2)*(104 + 24*Power(s2,2)*(-1 + t2) - 229*t2 + 
               94*Power(t2,2) + 2*Power(t2,3) - 8*Power(t2,4) + 
               4*Power(t1,2)*(-2 - 3*t2 + 6*Power(t2,2)) + 
               2*t1*(9 - 22*t2 + 6*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-9 + 37*t2 - 24*Power(t2,2) + 12*Power(t2,3) - 
                  4*t1*(-8 + 3*t2 + 6*Power(t2,2))))) + 
         (-1 + t2)*(-4 + Power(s1,6)*(1 + 5*s2 - 5*t1 - t2) + 10*t2 - 
            6*Power(t2,2) - Power(t2,3) + Power(t2,4) + 
            Power(s1,5)*(4 - 4*Power(t1,2) + s2*(-23 + 4*t1 - 12*t2) - 
               7*t2 + 3*Power(t2,2) + t1*(22 + 13*t2)) + 
            s1*(26 - 79*t2 + 64*Power(t2,2) - 4*Power(t2,3) - 
               7*Power(t2,4) + 
               t1*(16 - 29*t2 + 10*Power(t2,2) + 4*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s2*(-16 + 32*t2 - 15*Power(t2,2) - 2*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,4)*(4 + 8*Power(s2,2) - 6*t2 + 5*Power(t2,2) - 
               3*Power(t2,3) + 4*Power(t1,2)*(4 + t2) - 
               2*t1*(5 + 23*t2 + 5*Power(t2,2)) + 
               s2*(13 + 42*t2 + 11*Power(t2,2) - 4*t1*(6 + t2))) + 
            Power(s1,3)*(-20 - 16*Power(s2,2) + 18*t2 + Power(t2,2) + 
               Power(t2,4) - 4*Power(t1,2)*(6 + Power(t2,2)) + 
               2*t1*(-12 + 28*t2 + 5*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(21 - 52*t2 - 9*Power(t2,2) - 6*Power(t2,3) + 
                  4*t1*(10 + Power(t2,2)))) + 
            Power(s1,2)*(-3 + 45*t2 - 55*Power(t2,2) + 12*Power(t2,3) + 
               Power(t2,4) + 8*Power(s2,2)*(2 - 2*t2 + Power(t2,2)) + 
               4*Power(t1,2)*(4 - 2*t2 + Power(t2,3)) - 
               t1*(-13 + 18*t2 + 2*Power(t2,2) + 2*Power(t2,4)) + 
               s2*(-16 + 26*t2 - 7*Power(t2,2) + 4*Power(t2,3) + 
                  2*Power(t2,4) - 
                  4*t1*(8 - 6*t2 + 2*Power(t2,2) + Power(t2,3))))) + 
         s*(-14 + Power(s1,6)*(4 - s2 + t1 - 4*t2) + 34*t2 - 
            20*Power(t2,2) - 4*Power(t2,3) + 4*Power(t2,4) + 
            Power(s1,5)*(-1 - 4*Power(t1,2) + s2*(-14 + 4*t1 - 9*t2) - 
               16*t2 + 17*Power(t2,2) + t1*(16 + 7*t2)) + 
            s1*(87 - 245*t2 + 183*Power(t2,2) + Power(t2,3) - 
               24*Power(t2,4) - 2*Power(t2,5) + 
               t1*(45 - 83*t2 + 29*Power(t2,2) + 16*Power(t2,3) - 
                  8*Power(t2,4)) + 
               s2*(-48 + 99*t2 - 50*Power(t2,2) - 8*Power(t2,3) + 
                  8*Power(t2,4))) + 
            Power(s1,4)*(12 + 8*Power(s2,2) + 5*t2 + 4*Power(t2,2) - 
               21*Power(t2,3) + 4*Power(t1,2)*(3 + 2*t2) - 
               3*t1*(3 + 14*t2 + 5*Power(t2,2)) + 
               s2*(8 + 36*t2 + 22*Power(t2,2) - 4*t1*(5 + 2*t2))) - 
            Power(s1,3)*(20 + 16*Power(s2,2) + 17*t2 - 28*Power(t2,2) + 
               Power(t2,3) - 10*Power(t2,4) + 
               4*Power(t1,2)*(6 - 2*t2 + 3*Power(t2,2)) - 
               t1*(-29 + 68*t2 + 11*Power(t2,2) + 8*Power(t2,3)) - 
               2*s2*(15 - 35*t2 + Power(t2,2) - 10*Power(t2,3) + 
                  t1*(20 - 4*t2 + 6*Power(t2,2)))) + 
            Power(s1,2)*(-52 + 215*t2 - 220*Power(t2,2) + 
               57*Power(t2,3) + 2*Power(t2,4) - 2*Power(t2,5) + 
               8*Power(s2,2)*(4 - 6*t2 + 3*Power(t2,2)) + 
               4*Power(t1,2)*(6 - 4*t2 - 3*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(4 + 2*t2 - 25*Power(t2,2) + 8*Power(t2,3) - 
                  4*Power(t2,4)) + 
               s2*(-15 + 24*t2 + 2*Power(t2,2) - 4*Power(t2,3) + 
                  8*Power(t2,4) - 
                  4*t1*(14 - 16*t2 + 3*Power(t2,2) + 4*Power(t2,3))))))*
       R1q(s1))/(Power(-1 + s1,2)*s1*(s - s2 + t1)*Power(-1 + s + t2,3)*
       Power(s - s1 + t2,2)) + 
    (16*(Power(s,7)*(2 + 3*s1 + s2 - 6*t1 + 6*t2) - 
         Power(s,6)*(-9 + 15*Power(s1,2) + 2*Power(s2,2) - 43*t1 + 
            10*Power(t1,2) - 12*s2*(-2 + t1 - t2) + 15*t2 + 15*t1*t2 - 
            25*Power(t2,2) + s1*(12 + 12*s2 - 35*t1 + 25*t2)) + 
         Power(s,5)*(30*Power(s1,3) + Power(s2,3) + 
            Power(s2,2)*(28 - 8*t1 + 6*t2) + 
            Power(s1,2)*(26 + 39*s2 - 81*t1 + 44*t2) + 
            s2*(50 + 13*Power(t1,2) - 38*t2 - 60*Power(t2,2) + 
               t1*(-82 + 26*t2)) + 
            s1*(-77 + 13*Power(s2,2) - 198*t1 + 49*Power(t1,2) + 
               86*t2 + 59*t1*t2 - 111*Power(t2,2) + 
               s2*(130 - 62*t1 + 43*t2)) - 
            2*(13 + 3*Power(t1,3) - 34*t2 + 27*Power(t2,2) - 
               20*Power(t2,3) + Power(t1,2)*(-27 + 16*t2) + 
               t1*(41 - 63*t2 + Power(t2,2)))) - 
         Power(s,4)*(38 + 30*Power(s1,4) - 2*Power(s2,3)*(-3 + t1) + 
            28*t1 + 139*Power(t1,2) - 23*Power(t1,3) + 2*Power(t1,4) - 
            6*t2 + 138*t1*t2 - 153*Power(t1,2)*t2 + 19*Power(t1,3)*t2 - 
            72*Power(t2,2) - 150*t1*Power(t2,2) + 
            31*Power(t1,2)*Power(t2,2) + 46*Power(t2,3) - 
            22*t1*Power(t2,3) - 30*Power(t2,4) + 
            Power(s1,3)*(22 + 56*s2 - 94*t1 + 46*t2) + 
            Power(s2,2)*(115 + 6*Power(t1,2) - 57*t2 - 45*Power(t2,2) + 
               t1*(-35 + 19*t2)) - 
            2*s2*(33 + 3*Power(t1,3) + t2 + 11*Power(t2,2) - 
               50*Power(t2,3) + Power(t1,2)*(-26 + 19*t2) + 
               t1*(127 - 105*t2 - 7*Power(t2,2))) + 
            Power(s1,2)*(29*Power(s2,2) + 93*Power(t1,2) + 
               t1*(-341 + 71*t2) + s2*(249 - 122*t1 + 75*t2) - 
               4*(43 - 46*t2 + 51*Power(t2,2))) + 
            s1*(-193 + 4*Power(s2,3) - 29*Power(t1,3) + 
               Power(t1,2)*(206 - 107*t2) + 409*t2 - 249*Power(t2,2) + 
               155*Power(t2,3) + Power(s2,2)*(120 - 37*t1 + 23*t2) + 
               t1*(-286 + 423*t2 + 41*Power(t2,2)) + 
               s2*(198 + 62*Power(t1,2) - 169*t2 - 231*Power(t2,2) + 
                  t1*(-326 + 84*t2)))) + 
         Power(s,3)*(15*Power(s1,5) - 4*Power(s2,4) + 
            Power(s1,4)*(39*s2 - 56*t1 + 34*t2) + 
            Power(s2,3)*(17 - 20*t2 - 10*Power(t2,2) + 
               2*t1*(5 + 4*t2)) + 
            Power(s1,3)*(-153 + 29*Power(s2,2) + 85*Power(t1,2) + 
               196*t2 - 197*Power(t2,2) + t1*(-262 + 4*t2) + 
               s2*(204 - 114*t1 + 92*t2)) - 
            2*Power(s2,2)*(-117 + 155*t2 + 8*Power(t2,2) - 
               40*Power(t2,3) + 3*Power(t1,2)*(1 + 4*t2) + 
               t1*(36 - 55*t2 - 2*Power(t2,2))) + 
            s2*(-341 + 636*t2 - 243*Power(t2,2) + 32*Power(t2,3) - 
               75*Power(t2,4) + Power(t1,3)*(-2 + 24*t2) + 
               Power(t1,2)*(93 - 160*t2 + 22*Power(t2,2)) - 
               2*t1*(232 - 337*t2 + 55*Power(t2,2) + 38*Power(t2,3))) - 
            2*(-91 + 256*t2 - 211*Power(t2,2) + 48*Power(t2,3) - 
               Power(t2,4) - 5*Power(t2,5) + Power(t1,4)*(-1 + 4*t2) + 
               Power(t1,3)*(19 - 35*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-115 + 182*t2 - 63*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-152 + 240*t2 + Power(t2,2) - 75*Power(t2,3) - 
                  9*Power(t2,4))) + 
            s1*(4 + 8*Power(t1,4) + 296*t2 - 439*Power(t2,2) + 
               172*Power(t2,3) - 94*Power(t2,4) + 
               Power(s2,3)*(12 - 8*t1 + 5*t2) + 
               Power(t1,3)*(-82 + 71*t2) + 
               Power(t1,2)*(416 - 384*t2 + 25*Power(t2,2)) + 
               t1*(124 + 301*t2 - 346*Power(t2,2) - 125*Power(t2,3)) + 
               Power(s2,2)*(380 + 24*Power(t1,2) - 180*t2 - 
                  159*Power(t2,2) + t1*(-106 + 61*t2)) + 
               s2*(-216 - 24*Power(t1,3) + Power(t1,2)*(176 - 137*t2) + 
                  15*t2 - 60*Power(t2,2) + 317*Power(t2,3) + 
                  2*t1*(-398 + 282*t2 + 67*Power(t2,2)))) + 
            Power(s1,2)*(-318 + 6*Power(s2,3) - 49*Power(t1,3) + 
               Power(t1,2)*(282 - 118*t2) + 624*t2 - 380*Power(t2,2) + 
               232*Power(t2,3) + Power(s2,2)*(176 - 61*t1 + 52*t2) + 
               t1*(-333 + 466*t2 + 165*Power(t2,2)) + 
               2*s2*(52*Power(t1,2) + t1*(-229 + 33*t2) - 
                  2*(-62 + 51*t2 + 93*Power(t2,2))))) + 
         Power(s,2)*(-235 - 3*Power(s1,6) - 443*t1 - 241*Power(t1,2) + 
            39*Power(t1,3) + 6*Power(t1,4) + 
            Power(s1,5)*(10 - 12*s2 + 15*t1 - 17*t2) - 
            12*Power(s2,4)*(-1 + t2) + 979*t2 + 1289*t1*t2 + 
            521*Power(t1,2)*t2 - 119*Power(t1,3)*t2 + 6*Power(t1,4)*t2 - 
            1444*Power(t2,2) - 1051*t1*Power(t2,2) - 
            279*Power(t1,2)*Power(t2,2) + 72*Power(t1,3)*Power(t2,2) - 
            12*Power(t1,4)*Power(t2,2) + 879*Power(t2,3) + 
            35*t1*Power(t2,3) + 6*Power(t1,3)*Power(t2,3) - 
            188*Power(t2,4) + 165*t1*Power(t2,4) + 
            4*Power(t1,2)*Power(t2,4) + 9*Power(t2,5) + t1*Power(t2,5) + 
            Power(t2,6) + Power(s1,4)*
             (54 - 13*Power(s2,2) + 73*t1 - 37*Power(t1,2) + 
               s2*(-56 + 50*t1 - 76*t2) - 111*t2 + 49*t1*t2 + 
               102*Power(t2,2)) + 
            2*Power(s2,3)*(-14 + 37*t2 - 12*Power(t2,2) - 
               10*Power(t2,3) + 3*t1*(-7 + 5*t2 + 2*Power(t2,2))) + 
            Power(s2,2)*(-257 + 555*t2 - 243*Power(t2,2) - 
               110*Power(t2,3) + 60*Power(t2,4) - 
               18*Power(t1,2)*(-3 + t2 + 2*Power(t2,2)) + 
               t1*(95 - 267*t2 + 120*Power(t2,2) + 46*Power(t2,3))) + 
            2*s2*(233 - 708*t2 + 651*Power(t2,2) - 134*Power(t2,3) - 
               28*Power(t2,4) - 12*Power(t2,5) + 
               3*Power(t1,3)*(-5 - t2 + 6*Power(t2,2)) - 
               Power(t1,2)*(53 - 156*t2 + 84*Power(t2,2) + 
                  16*Power(t2,3)) + 
               t1*(249 - 538*t2 + 261*Power(t2,2) + 55*Power(t2,3) - 
                  32*Power(t2,4))) + 
            Power(s1,3)*(176 - 4*Power(s2,3) + 35*Power(t1,3) + 
               Power(s2,2)*(-94 + 43*t1 - 69*t2) - 329*t2 + 
               252*Power(t2,2) - 163*Power(t2,3) + 
               Power(t1,2)*(-156 + 37*t2) + 
               t1*(155 - 162*t2 - 221*Power(t2,2)) + 
               s2*(-122 - 74*Power(t1,2) + 50*t2 + 312*Power(t2,2) + 
                  t1*(250 + 32*t2))) + 
            Power(s1,2)*(36 - 10*Power(t1,4) + 
               Power(t1,3)*(100 - 84*t2) + 
               5*Power(s2,3)*(-1 + 2*t1 - 3*t2) - 349*t2 + 
               421*Power(t2,2) - 175*Power(t2,3) + 104*Power(t2,4) + 
               Power(t1,2)*(-406 + 247*t2 + 75*Power(t2,2)) + 
               Power(s2,2)*(-410 - 30*Power(t1,2) + t1*(110 - 54*t2) + 
                  119*t2 + 231*Power(t2,2)) + 
               2*t1*(-99 - 87*t2 + 125*Power(t2,2) + 110*Power(t2,3)) + 
               s2*(277 + 30*Power(t1,3) - 85*t2 + 79*Power(t2,2) - 
                  375*Power(t2,3) + Power(t1,2)*(-205 + 153*t2) - 
                  6*t1*(-136 + 61*t2 + 51*Power(t2,2)))) + 
            s1*(-347 + 12*Power(s2,4) + 850*t2 - 550*Power(t2,2) + 
               51*Power(t2,3) + 11*Power(t2,4) - 24*Power(t2,5) + 
               6*Power(t1,4)*(-1 + 4*t2) + 
               3*Power(t1,3)*(27 - 59*t2 + 13*Power(t2,2)) - 
               Power(t1,2)*(547 - 770*t2 + 87*Power(t2,2) + 
                  77*Power(t2,3)) - 
               t1*(717 - 1199*t2 + 100*Power(t2,2) + 321*Power(t2,3) + 
                  64*Power(t2,4)) + 
               Power(s2,3)*(-14 + 21*t2 + 39*Power(t2,2) - 
                  6*t1*(5 + 4*t2)) + 
               Power(s2,2)*(-595 + 796*t2 + 63*Power(t2,2) - 
                  209*Power(t2,3) + 18*Power(t1,2)*(1 + 4*t2) + 
                  t1*(109 - 219*t2 - 39*Power(t2,2))) + 
               s2*(800 + Power(t1,3)*(6 - 72*t2) - 1523*t2 + 
                  587*Power(t2,2) - 35*Power(t2,3) + 175*Power(t2,4) + 
                  Power(t1,2)*(-176 + 375*t2 - 39*Power(t2,2)) + 
                  2*t1*(571 - 783*t2 + 12*Power(t2,2) + 143*Power(t2,3)))\
)) - (-1 + t2)*(Power(s1,6)*(1 + 5*s2 - 5*t1 - t2) + 
            Power(s1,5)*(5 + 10*Power(s2,2) + 6*Power(t1,2) - 9*t2 + 
               4*Power(t2,2) - s2*(14 + 16*t1 + 21*t2) + t1*(13 + 22*t2)\
) + Power(s1,4)*(12 + 5*Power(s2,3) + 3*Power(t1,3) + 
               Power(s2,2)*(7 - 7*t1 - 39*t2) + 
               Power(t1,2)*(11 - 31*t2) - 25*t2 + 19*Power(t2,2) - 
               6*Power(t2,3) - 
               s2*(25 + Power(t1,2) + t1*(18 - 70*t2) - 56*t2 - 
                  35*Power(t2,2)) + t1*(28 - 60*t2 - 34*Power(t2,2))) + 
            Power(s1,3)*(-37 + 9*Power(t1,3) - 4*Power(t1,4) + 
               Power(s2,3)*(10 + 4*t1 - 19*t2) + 41*t2 + 
               9*Power(t2,2) - 17*Power(t2,3) + 4*Power(t2,4) - 
               Power(s2,2)*(85 + 12*Power(t1,2) + t1*(11 - 38*t2) + 
                  17*t2 - 58*Power(t2,2)) + 
               Power(t1,2)*(-59 - 49*t2 + 52*Power(t2,2)) + 
               t1*(-93 - 10*t2 + 129*Power(t2,2) + 20*Power(t2,3)) + 
               s2*(108 + 12*Power(t1,3) - 29*t2 - 96*Power(t2,2) - 
                  29*Power(t2,3) - Power(t1,2)*(8 + 19*t2) + 
                  t1*(144 + 66*t2 - 110*Power(t2,2)))) + 
            Power(s1,2)*(-123 + 8*Power(s2,4) + 340*t2 - 
               276*Power(t2,2) + 48*Power(t2,3) + 12*Power(t2,4) - 
               Power(t2,5) + 2*Power(t1,4)*(1 + 5*t2) + 
               Power(t1,3)*(11 - 32*t2 - 14*Power(t2,2)) + 
               Power(t1,2)*(-157 + 138*t2 + 87*Power(t2,2) - 
                  32*Power(t2,3)) - 
               t1*(263 - 478*t2 + 63*Power(t2,2) + 160*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,3)*(2 - 2*t2 + 27*Power(t2,2) - 
                  2*t1*(13 + 5*t2)) + 
               Power(s2,2)*(-181 + 210*t2 + 43*Power(t2,2) - 
                  40*Power(t2,3) + 30*Power(t1,2)*(1 + t2) + 
                  t1*(7 - 28*t2 - 68*Power(t2,2))) + 
               s2*(280 - 546*t2 + 157*Power(t2,2) + 106*Power(t2,3) + 
                  12*Power(t2,4) - 2*Power(t1,3)*(7 + 15*t2) + 
                  Power(t1,2)*(-20 + 62*t2 + 55*Power(t2,2)) + 
                  2*t1*(169 - 174*t2 - 65*Power(t2,2) + 36*Power(t2,3)))\
) + (-1 + t2)*(32 + 4*Power(s2,4)*(-1 + t2) - 142*t2 + 
               219*Power(t2,2) - 129*Power(t2,3) + 15*Power(t2,4) + 
               4*Power(t2,5) + 2*Power(t1,4)*(-2 + t2 + Power(t2,2)) - 
               Power(t1,3)*(8 - 26*t2 + 11*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(68 - 208*t2 + 175*Power(t2,2) - 5*Power(t2,3) - 
                  26*Power(t2,4)) + 
               Power(t1,2)*(32 - 50*t2 - 5*Power(t2,2) + 
                  15*Power(t2,3) + 3*Power(t2,4)) - 
               2*Power(s2,3)*
                (-4 + 12*t2 - 5*Power(t2,2) - 2*Power(t2,3) + 
                  t1*(-8 + 7*t2 + Power(t2,2))) + 
               Power(s2,2)*(32 - 56*t2 + 3*Power(t2,2) + 
                  17*Power(t2,3) - Power(t2,4) + 
                  6*Power(t1,2)*(-4 + 3*t2 + Power(t2,2)) - 
                  t1*(24 - 74*t2 + 31*Power(t2,2) + 13*Power(t2,3))) - 
               2*s2*(34 - 107*t2 + 96*Power(t2,2) - 10*Power(t2,3) - 
                  11*Power(t2,4) + 
                  Power(t1,3)*(-8 + 5*t2 + 3*Power(t2,2)) - 
                  Power(t1,2)*
                   (12 - 38*t2 + 16*Power(t2,2) + 7*Power(t2,3)) + 
                  t1*(32 - 53*t2 - Power(t2,2) + 16*Power(t2,3) + 
                     Power(t2,4)))) - 
            s1*(110 + 12*Power(s2,4)*(-1 + t2) - 452*t2 + 
               651*Power(t2,2) - 367*Power(t2,3) + 48*Power(t2,4) + 
               10*Power(t2,5) + 
               2*Power(t1,4)*(-5 + t2 + 4*Power(t2,2)) - 
               Power(t1,3)*(14 - 60*t2 + 29*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(126 - 234*t2 + 47*Power(t2,2) + 
                  61*Power(t2,3) - 2*Power(t2,4)) + 
               t1*(228 - 686*t2 + 581*Power(t2,2) - 20*Power(t2,3) - 
                  104*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,3)*(12 - 44*t2 + 14*Power(t2,2) + 
                  17*Power(t2,3) + t1*(46 - 38*t2 - 8*Power(t2,2))) + 
               Power(s2,2)*(132 - 272*t2 + 99*Power(t2,2) + 
                  51*Power(t2,3) - 12*Power(t2,4) + 
                  6*Power(t1,2)*(-11 + 7*t2 + 4*Power(t2,2)) - 
                  t1*(38 - 148*t2 + 57*Power(t2,2) + 50*Power(t2,3))) + 
               s2*(-234 + 726*t2 - 666*Power(t2,2) + 97*Power(t2,3) + 
                  74*Power(t2,4) + 2*Power(t2,5) - 
                  6*Power(t1,3)*(-7 + 3*t2 + 4*Power(t2,2)) + 
                  Power(t1,2)*
                   (40 - 164*t2 + 72*Power(t2,2) + 49*Power(t2,3)) + 
                  2*t1*(-129 + 253*t2 - 73*Power(t2,2) - 
                     56*Power(t2,3) + 7*Power(t2,4))))) + 
         s*(Power(s1,6)*(-4 + s2 - t1 + 4*t2) + 
            Power(s1,5)*(-6 + 2*Power(s2,2) + 8*t1 + 6*Power(t1,2) + 
               30*t2 - 31*t1*t2 - 24*Power(t2,2) + 
               s2*(-10 - 8*t1 + 33*t2)) - 
            Power(s1,3)*(-4*Power(t1,4) + Power(t1,3)*(44 - 35*t2) + 
               Power(s2,3)*(6 + 4*t1 - 15*t2) + 
               2*Power(t1,2)*(-59 - 13*t2 + 50*Power(t2,2)) + 
               t1*(-70 - 105*t2 + 82*Power(t2,2) + 151*Power(t2,3)) + 
               2*(9 - 49*t2 + 56*Power(t2,2) - 39*Power(t2,3) + 
                  23*Power(t2,4)) - 
               Power(s2,2)*(138 + 12*Power(t1,2) + 50*t2 - 
                  156*Power(t2,2) + t1*(-32 + 5*t2)) + 
               s2*(98 + 12*Power(t1,3) + 19*t2 + 18*Power(t2,2) - 
                  193*Power(t2,3) + Power(t1,2)*(-82 + 55*t2) + 
                  t1*(256 + 76*t2 - 256*Power(t2,2)))) + 
            Power(s1,4)*(Power(s2,3) - 9*Power(t1,3) - 
               11*Power(s2,2)*(t1 - 4*t2) + 4*Power(t1,2)*(5 + 3*t2) + 
               t1*(-39 - 16*t2 + 121*Power(t2,2)) + 
               10*(-3 + 6*t2 - 8*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(19*Power(t1,2) - 4*t1*(5 + 14*t2) + 
                  6*(6 + 5*t2 - 22*Power(t2,2)))) - 
            Power(s1,2)*(-222 + 8*Power(s2,4) + 530*t2 - 
               356*Power(t2,2) + 48*Power(t2,3) + 18*Power(t2,4) - 
               18*Power(t2,5) + 4*Power(t1,4)*(-2 + 5*t2) + 
               Power(t1,3)*(56 - 118*t2 + 21*Power(t2,2)) - 
               2*Power(t1,2)*
                (190 - 203*t2 - 77*Power(t2,2) + 66*Power(t2,3)) - 
               t1*(534 - 876*t2 + 7*Power(t2,2) + 284*Power(t2,3) + 
                  66*Power(t2,4)) + 
               Power(s2,3)*(9 - 24*t2 + 48*Power(t2,2) - 
                  4*t1*(4 + 5*t2)) + 
               Power(s2,2)*(-450 + 544*t2 + 60*Power(t1,2)*t2 + 
                  140*Power(t2,2) - 190*Power(t2,3) + 
                  t1*(38 - 70*t2 - 75*Power(t2,2))) + 
               s2*(595 + Power(t1,3)*(16 - 60*t2) - 1104*t2 + 
                  335*Power(t2,2) + 60*Power(t2,3) + 129*Power(t2,4) + 
                  Power(t1,2)*(-103 + 212*t2 + 6*Power(t2,2)) + 
                  t1*(830 - 950*t2 - 294*Power(t2,2) + 322*Power(t2,3)))) \
+ s1*(24*Power(s2,4)*(-1 + t2) + 
               12*Power(t1,4)*(-1 - t2 + 2*Power(t2,2)) - 
               Power(t1,3)*(42 - 170*t2 + 108*Power(t2,2) + 
                  19*Power(t2,3)) + 
               Power(t1,2)*(414 - 864*t2 + 340*Power(t2,2) + 
                  154*Power(t2,3) - 46*Power(t2,4)) - 
               t1*(-698 + 2050*t2 - 1692*Power(t2,2) + 31*Power(t2,3) + 
                  306*Power(t2,4) + 2*Power(t2,5)) - 
               2*(-173 + 680*t2 - 945*Power(t2,2) + 535*Power(t2,3) - 
                  98*Power(t2,4) + Power(t2,6)) + 
               Power(s2,3)*(18 - 72*t2 + 6*Power(t2,2) + 
                  47*Power(t2,3) - 12*t1*(-7 + 5*t2 + 2*Power(t2,2))) + 
               Power(s2,2)*(454 - 1008*t2 + 464*Power(t2,2) + 
                  186*Power(t2,3) - 98*Power(t2,4) + 
                  36*Power(t1,2)*(-3 + t2 + 2*Power(t2,2)) - 
                  t1*(78 - 314*t2 + 120*Power(t2,2) + 113*Power(t2,3))) \
+ s2*(-738 + 2256*t2 - 2088*Power(t2,2) + 397*Power(t2,3) + 
                  136*Power(t2,4) + 36*Power(t2,5) + 
                  12*Power(t1,3)*(5 + t2 - 6*Power(t2,2)) + 
                  Power(t1,2)*
                   (102 - 412*t2 + 222*Power(t2,2) + 85*Power(t2,3)) + 
                  4*t1*(-217 + 468*t2 - 201*Power(t2,2) - 
                     85*Power(t2,3) + 36*Power(t2,4)))) - 
            (-1 + t2)*(12*Power(s2,4)*(-1 + t2) + 
               Power(s2,3)*(24 - 70*t2 + 27*Power(t2,2) + 
                  15*Power(t2,3) + t1*(46 - 38*t2 - 8*Power(t2,2))) + 
               2*Power(s2,2)*(72 - 142*t2 + 41*Power(t2,2) + 
                  33*Power(t2,3) - 9*Power(t2,4) + 
                  3*Power(t1,2)*(-11 + 7*t2 + 4*Power(t2,2)) - 
                  t1*(37 - 112*t2 + 47*Power(t2,2) + 22*Power(t2,3))) + 
               s2*(-286 + 892*t2 - 817*Power(t2,2) + 125*Power(t2,3) + 
                  76*Power(t2,4) + 2*Power(t2,5) - 
                  6*Power(t1,3)*(-7 + 3*t2 + 4*Power(t2,2)) + 
                  Power(t1,2)*
                   (76 - 238*t2 + 107*Power(t2,2) + 43*Power(t2,3)) + 
                  2*t1*(-141 + 269*t2 - 72*Power(t2,2) - 
                     53*Power(t2,3) + 7*Power(t2,4))) + 
               2*(69 - 300*t2 + 457*Power(t2,2) - 276*Power(t2,3) + 
                  46*Power(t2,4) + 3*Power(t2,5) + 
                  Power(t1,4)*(-5 + t2 + 4*Power(t2,2)) - 
                  Power(t1,3)*
                   (13 - 42*t2 + 20*Power(t2,2) + 7*Power(t2,3)) + 
                  Power(t1,2)*
                   (69 - 127*t2 + 31*Power(t2,2) + 20*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  t1*(140 - 423*t2 + 359*Power(t2,2) - 20*Power(t2,3) - 
                     53*Power(t2,4) + Power(t2,5))))))*R1q(1 - s + s1 - t2))/
     ((s - s2 + t1)*Power(-1 + s + t2,3)*(-1 + s - s1 + t2)*
       Power(s - s1 + t2,2)*(-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2) + 2*s2 - 2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 
         2*s*t1 - 2*s1*t1 - 2*s2*t1 + Power(t1,2) + 4*t2)) + 
    (16*(-4 + Power(s,5) + Power(s1,5) + 76*s2 - 80*Power(s2,2) - 
         4*Power(s2,3) + 12*Power(s2,4) - 76*t1 + 160*s2*t1 + 
         12*Power(s2,2)*t1 - 48*Power(s2,3)*t1 - 80*Power(t1,2) - 
         12*s2*Power(t1,2) + 72*Power(s2,2)*Power(t1,2) + 4*Power(t1,3) - 
         48*s2*Power(t1,3) + 12*Power(t1,4) + 
         Power(s1,4)*(-1 + 5*s2 - 4*t1 - 3*t2) + 55*t2 - 204*s2*t2 + 
         71*Power(s2,2)*t2 + 30*Power(s2,3)*t2 + 201*t1*t2 - 
         130*s2*t1*t2 - 93*Power(s2,2)*t1*t2 - 6*Power(s2,3)*t1*t2 + 
         59*Power(t1,2)*t2 + 96*s2*Power(t1,2)*t2 + 
         18*Power(s2,2)*Power(t1,2)*t2 - 33*Power(t1,3)*t2 - 
         18*s2*Power(t1,3)*t2 + 6*Power(t1,4)*t2 - 86*Power(t2,2) + 
         89*s2*Power(t2,2) + 30*Power(s2,2)*Power(t2,2) + 
         3*Power(s2,3)*Power(t2,2) - 85*t1*Power(t2,2) - 
         70*s2*t1*Power(t2,2) - 15*Power(s2,2)*t1*Power(t2,2) + 
         40*Power(t1,2)*Power(t2,2) + 21*s2*Power(t1,2)*Power(t2,2) - 
         9*Power(t1,3)*Power(t2,2) + 23*Power(t2,3) + 30*s2*Power(t2,3) - 
         Power(s2,2)*Power(t2,3) - 30*t1*Power(t2,3) - 
         2*s2*t1*Power(t2,3) + 3*Power(t1,2)*Power(t2,3) + 
         4*Power(t2,4) - Power(s,4)*(1 + s1 + 4*s2 - 8*t1 + 4*t2) + 
         Power(s1,3)*(10 + 7*Power(s2,2) + 3*Power(t1,2) + t2 + 
            3*Power(t2,2) - 2*s2*(10 + 5*t1 + 6*t2) + t1*(19 + 9*t2)) + 
         Power(s,3)*(-4*Power(s1,2) + 5*Power(s2,2) + t1 + 
            15*Power(t1,2) - 11*t2 - 3*t1*t2 - 6*Power(t2,2) + 
            s2*(6 - 20*t1 + 9*t2) + s1*(12 + 3*s2 - 16*t1 + 17*t2)) + 
         Power(s,2)*(29 + 8*Power(s1,3) - 2*Power(s2,3) + 36*t1 + 
            9*Power(t1,2) + 14*Power(t1,3) + 
            Power(s1,2)*(-22 + 11*s2 + 4*t1 - 25*t2) + 
            Power(s2,2)*(5 + 18*t1 - 6*t2) - 35*t2 - 17*t1*t2 - 
            16*Power(t2,2) - 9*t1*Power(t2,2) - Power(t2,3) + 
            s2*(-44 - 14*t1 - 30*Power(t1,2) + 34*t2 + 6*t1*t2 + 
               15*Power(t2,2)) + 
            s1*(12 - Power(s2,2) + 21*t1 - 25*Power(t1,2) + 
               s2*(-36 + 26*t1 - 34*t2) + 35*t2 + 19*t1*t2 + 
               15*Power(t2,2))) + 
         Power(s1,2)*(-27 + 3*Power(s2,3) + 6*Power(t1,3) + 
            Power(t1,2)*(1 - 15*t2) + 2*t2 - 2*Power(t2,2) - 
            Power(t2,3) - 3*Power(s2,2)*(3 + 5*t2) - 
            t1*(20 + 43*t2 + 3*Power(t2,2)) + 
            s2*(21 - 9*Power(t1,2) + 42*t2 + 9*Power(t2,2) + 
               t1*(8 + 30*t2))) + 
         s1*(-43 - 6*Power(t1,4) + Power(s2,3)*(2 + 6*t1 - 6*t2) + 
            101*t2 - 31*Power(t2,2) - 2*Power(t2,3) + 
            Power(t1,3)*(1 + 3*t2) + 
            Power(t1,2)*(-71 - 13*t2 + 9*Power(t2,2)) + 
            t1*(-121 + 109*t2 + 46*Power(t2,2) - 2*Power(t2,3)) + 
            Power(s2,2)*(-83 - 18*Power(t1,2) + 7*t2 + 9*Power(t2,2) + 
               3*t1*(-1 + 5*t2)) + 
            2*s2*(62 + 9*Power(t1,3) - 57*t2 - 6*Power(t1,2)*t2 - 
               22*Power(t2,2) - Power(t2,3) + 
               t1*(77 + 3*t2 - 9*Power(t2,2)))) + 
         s*(-21 - 5*Power(s1,4) + 41*t1 + 71*Power(t1,2) + 
            15*Power(t1,3) + 6*Power(t1,4) + 23*t2 - 61*t1*t2 - 
            23*Power(t1,2)*t2 + 5*Power(t1,3)*t2 - 8*Power(t2,2) - 
            12*Power(t1,2)*Power(t2,2) - 14*Power(t2,3) + 
            2*t1*Power(t2,3) + Power(s2,3)*(-18 - 6*t1 + t2) + 
            Power(s1,3)*(12 - 15*s2 + 8*t1 + 15*t2) + 
            Power(s2,2)*(83 + 18*Power(t1,2) - 37*t2 - 12*Power(t2,2) + 
               3*t1*(17 + t2)) - 
            Power(s1,2)*(22 + 11*Power(s2,2) - 7*Power(t1,2) + 25*t2 + 
               12*Power(t2,2) + t1*(41 + 25*t2) - s2*(50 + 4*t1 + 37*t2)) \
+ s2*(-44 - 18*Power(t1,3) + 57*t2 + 10*Power(t2,2) + 2*Power(t2,3) - 
               3*Power(t1,2)*(16 + 3*t2) + 
               2*t1*(-77 + 30*t2 + 12*Power(t2,2))) - 
            s1*(14 + Power(s2,3) + 20*Power(t1,3) + 
               Power(s2,2)*(-8 + 18*t1 - 23*t2) + 
               Power(t1,2)*(6 - 17*t2) - 43*t2 - 26*Power(t2,2) - 
               2*Power(t2,3) - 4*t1*(-6 + 14*t2 + 3*Power(t2,2)) + 
               s2*(-31 - 39*Power(t1,2) + 72*t2 + 24*Power(t2,2) + 
                  t1*(2 + 40*t2)))))*R2q(1 - s2 + t1 - t2))/
     ((s - s2 + t1)*Power(s - s1 + t2,2)*(-1 + s2 - t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) + 
    (8*(144 - 240*s2 + 24*Power(s2,2) + 88*Power(s2,3) - 8*Power(s2,4) - 
         8*Power(s2,5) + 2*Power(s1,7)*(s2 - t1) + 240*t1 - 48*s2*t1 - 
         264*Power(s2,2)*t1 + 32*Power(s2,3)*t1 + 40*Power(s2,4)*t1 + 
         24*Power(t1,2) + 264*s2*Power(t1,2) - 
         48*Power(s2,2)*Power(t1,2) - 80*Power(s2,3)*Power(t1,2) - 
         88*Power(t1,3) + 32*s2*Power(t1,3) + 
         80*Power(s2,2)*Power(t1,3) - 8*Power(t1,4) - 40*s2*Power(t1,4) + 
         8*Power(t1,5) + Power(s,6)*(-1 + 2*s1)*(t1 - t2) - 563*t2 + 
         615*s2*t2 + 137*Power(s2,2)*t2 - 179*Power(s2,3)*t2 - 
         18*Power(s2,4)*t2 + 8*Power(s2,5)*t2 - 612*t1*t2 - 
         261*s2*t1*t2 + 508*Power(s2,2)*t1*t2 + 79*Power(s2,3)*t1*t2 - 
         34*Power(s2,4)*t1*t2 + 124*Power(t1,2)*t2 - 
         479*s2*Power(t1,2)*t2 - 129*Power(s2,2)*Power(t1,2)*t2 + 
         56*Power(s2,3)*Power(t1,2)*t2 + 150*Power(t1,3)*t2 + 
         93*s2*Power(t1,3)*t2 - 44*Power(s2,2)*Power(t1,3)*t2 - 
         25*Power(t1,4)*t2 + 16*s2*Power(t1,4)*t2 - 2*Power(t1,5)*t2 + 
         772*Power(t2,2) - 381*s2*Power(t2,2) - 
         311*Power(s2,2)*Power(t2,2) + 63*Power(s2,3)*Power(t2,2) + 
         19*Power(s2,4)*Power(t2,2) - 2*Power(s2,5)*Power(t2,2) + 
         377*t1*Power(t2,2) + 568*s2*t1*Power(t2,2) - 
         127*Power(s2,2)*t1*Power(t2,2) - 74*Power(s2,3)*t1*Power(t2,2) + 
         4*Power(s2,4)*t1*Power(t2,2) - 257*Power(t1,2)*Power(t2,2) + 
         65*s2*Power(t1,2)*Power(t2,2) + 
         108*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         4*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         Power(t1,3)*Power(t2,2) - 70*s2*Power(t1,3)*Power(t2,2) - 
         16*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         17*Power(t1,4)*Power(t2,2) + 14*s2*Power(t1,4)*Power(t2,2) - 
         4*Power(t1,5)*Power(t2,2) - 401*Power(t2,3) - 
         84*s2*Power(t2,3) + 126*Power(s2,2)*Power(t2,3) + 
         16*Power(s2,3)*Power(t2,3) - Power(s2,4)*Power(t2,3) + 
         81*t1*Power(t2,3) - 187*s2*t1*Power(t2,3) - 
         77*Power(s2,2)*t1*Power(t2,3) - 5*Power(s2,3)*t1*Power(t2,3) + 
         61*Power(t1,2)*Power(t2,3) + 106*s2*Power(t1,2)*Power(t2,3) + 
         21*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         45*Power(t1,3)*Power(t2,3) - 23*s2*Power(t1,3)*Power(t2,3) + 
         8*Power(t1,4)*Power(t2,3) + 24*Power(t2,4) + 80*s2*Power(t2,4) + 
         16*Power(s2,2)*Power(t2,4) - 76*t1*Power(t2,4) - 
         56*s2*t1*Power(t2,4) - 4*Power(s2,2)*t1*Power(t2,4) + 
         40*Power(t1,2)*Power(t2,4) + 8*s2*Power(t1,2)*Power(t2,4) - 
         4*Power(t1,3)*Power(t2,4) + 24*Power(t2,5) + 8*s2*Power(t2,5) - 
         8*t1*Power(t2,5) + 2*Power(s1,6)*
          (-1 + 4*Power(s2,2) + 2*Power(t1,2) + t2 - 
            s2*(3 + 6*t1 + 2*t2) + t1*(2 + 3*t2)) + 
         Power(s1,5)*(6 + 12*Power(s2,3) - 6*Power(t2,2) - 
            2*Power(t1,2)*(3 + 8*t2) - 
            2*Power(s2,2)*(9 + 12*t1 + 8*t2) + 
            t1*(7 - 17*t2 - 4*Power(t2,2)) + 
            s2*(-5 + 12*Power(t1,2) + 17*t2 + 2*Power(t2,2) + 
               8*t1*(3 + 4*t2))) - 
         Power(s,5)*(-7 - 7*t1 + 5*Power(t1,2) + 
            2*Power(s1,2)*(s2 + 4*t1 - 5*t2) + 5*t2 - 5*t1*t2 + 
            s2*(8 - 5*t1 + 6*t2) + 
            s1*(-8*Power(t1,2) + s2*(-11 + 8*t1 - 10*t2) + 
               t1*(3 + 6*t2) + 2*(4 + t2 + Power(t2,2)))) + 
         Power(s1,4)*(-3 + 8*Power(s2,4) - 8*Power(t1,4) - 12*t2 + 
            11*Power(t2,2) + 4*Power(t2,3) - 
            8*Power(s2,3)*(1 + 2*t1 + 3*t2) + 
            4*Power(t1,3)*(-3 + 5*t2) + 
            Power(t1,2)*(-43 + 39*t2 + 8*Power(t2,2)) + 
            t1*(-48 + 21*t2 + 25*Power(t2,2)) + 
            Power(s2,2)*(-53 + 57*t2 + 8*Power(t2,2) + t1*(4 + 68*t2)) + 
            s2*(59 + 16*Power(t1,3) + Power(t1,2)*(16 - 64*t2) - 41*t2 - 
               16*Power(t2,2) - 16*t1*(-6 + 6*t2 + Power(t2,2)))) + 
         Power(s1,3)*(-41 + 2*Power(s2,5) + 10*Power(t1,5) + 
            2*Power(s2,4)*(5 + t1 - 8*t2) + 81*t2 - 31*Power(t2,2) - 
            9*Power(t2,3) - 6*Power(t1,4)*(-3 + 2*t2) + 
            Power(t1,3)*(71 - 3*t2 - 12*Power(t2,2)) + 
            Power(t1,2)*(85 - 12*t2 - 61*Power(t2,2) + 4*Power(t2,3)) - 
            t1*(19 - 86*t2 + 41*Power(t2,2) + 8*Power(t2,3)) + 
            Power(s2,3)*(-99 - 28*Power(t1,2) + 55*t2 + 
               12*Power(t2,2) + 12*t1*(-4 + 5*t2)) + 
            Power(s2,2)*(116 + 52*Power(t1,3) - 
               84*Power(t1,2)*(-1 + t2) - 52*t2 - 40*Power(t2,2) + 
               t1*(269 - 113*t2 - 36*Power(t2,2))) + 
            s2*(12 - 38*Power(t1,4) - 83*t2 + 48*Power(t2,2) + 
               5*Power(t2,3) + Power(t1,3)*(-64 + 52*t2) + 
               Power(t1,2)*(-241 + 61*t2 + 36*Power(t2,2)) + 
               t1*(-201 + 64*t2 + 101*Power(t2,2) - 4*Power(t2,3)))) + 
         Power(s1,2)*(29 - 4*Power(t1,6) + 
            2*Power(s2,5)*(7 + 2*t1 - 2*t2) - 27*t2 - 15*Power(t2,2) + 
            13*Power(t2,3) - 2*Power(t1,5)*(6 + t2) + 
            Power(t1,4)*(-31 + 3*t2 + 16*Power(t2,2)) - 
            Power(t1,3)*(-154 + 25*t2 + 5*Power(t2,2) + 8*Power(t2,3)) + 
            2*Power(t1,2)*(221 - 244*t2 + 5*Power(t2,2) + 
               26*Power(t2,3)) + 
            t1*(294 - 525*t2 + 188*Power(t2,2) + 45*Power(t2,3) - 
               12*Power(t2,4)) + 
            Power(s2,4)*(-69 - 20*Power(t1,2) + 41*t2 + 8*Power(t2,2) + 
               2*t1*(-34 + 7*t2)) + 
            Power(s2,3)*(-121 + 40*Power(t1,3) - 21*t2 + 
               2*Power(t2,2) - 4*Power(t1,2)*(-33 + 4*t2) - 
               2*t1*(-119 + 63*t2 + 20*Power(t2,2))) + 
            Power(s2,2)*(458 - 40*Power(t1,4) + 
               4*Power(t1,3)*(-32 + t2) - 533*t2 + 46*Power(t2,2) + 
               33*Power(t2,3) + 
               12*Power(t1,2)*(-25 + 11*t2 + 6*Power(t2,2)) + 
               t1*(396 + 17*t2 - 9*Power(t2,2) - 8*Power(t2,3))) + 
            s2*(-311 + 20*Power(t1,5) + 580*t2 - 249*Power(t2,2) - 
               18*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,4)*(62 + 4*t2) - 
               2*Power(t1,3)*(-81 + 25*t2 + 28*Power(t2,2)) + 
               Power(t1,2)*(-429 + 29*t2 + 12*Power(t2,2) + 
                  16*Power(t2,3)) - 
               t1*(900 - 1021*t2 + 56*Power(t2,2) + 85*Power(t2,3)))) + 
         s1*(203 + 8*Power(s2,6) - 601*t2 + 581*Power(t2,2) - 
            183*Power(t2,3) - 8*Power(t2,4) + 8*Power(t2,5) + 
            4*Power(t1,6)*(2 + t2) - 
            2*Power(t1,5)*(7 + 12*t2 + 4*Power(t2,2)) + 
            Power(t1,4)*(-95 + 78*t2 + 51*Power(t2,2) + 4*Power(t2,3)) - 
            7*Power(t1,3)*(2 - 25*t2 + 15*Power(t2,2) + 8*Power(t2,3)) + 
            t1*(508 - 1159*t2 + 735*Power(t2,2) - 22*Power(t2,3) - 
               64*Power(t2,4)) + 
            Power(t1,2)*(364 - 489*t2 + 6*Power(t2,2) + 
               93*Power(t2,3) + 16*Power(t2,4)) + 
            2*Power(s2,5)*(4 + 14*t2 + Power(t2,2) - 2*t1*(12 + t2)) + 
            2*Power(s2,4)*(-51 + 57*t2 + 15*Power(t2,2) + 
               10*Power(t1,2)*(6 + t2) - t1*(23 + 68*t2 + 8*Power(t2,2))\
) + Power(s2,3)*(43 - 270*t2 + 208*Power(t2,2) + 31*Power(t2,3) - 
               40*Power(t1,3)*(4 + t2) + 
               4*Power(t1,2)*(26 + 66*t2 + 11*Power(t2,2)) - 
               t1*(-401 + 420*t2 + 141*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,2)*(351 - 451*t2 - 45*Power(t2,2) + 
               131*Power(t2,3) + 8*Power(t2,4) + 
               40*Power(t1,4)*(3 + t2) - 
               4*Power(t1,3)*(29 + 64*t2 + 14*Power(t2,2)) + 
               3*Power(t1,2)*
                (-197 + 192*t2 + 81*Power(t2,2) + 4*Power(t2,3)) - 
               t1*(100 - 715*t2 + 521*Power(t2,2) + 118*Power(t2,3))) + 
            s2*(-511 + 1180*t2 - 780*Power(t2,2) + 65*Power(t2,3) + 
               48*Power(t2,4) - 4*Power(t1,5)*(12 + 5*t2) + 
               2*Power(t1,4)*(32 + 62*t2 + 17*Power(t2,2)) - 
               3*Power(t1,3)*
                (-129 + 116*t2 + 61*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(71 - 620*t2 + 418*Power(t2,2) + 
                  143*Power(t2,3)) + 
               t1*(-715 + 940*t2 + 39*Power(t2,2) - 224*Power(t2,3) - 
                  24*Power(t2,4)))) + 
         Power(s,4)*(-18 - 15*t1 - 4*Power(t1,2) - 13*Power(t1,3) + 
            10*Power(s1,3)*(s2 + t1 - 2*t2) + 34*t2 + 31*t1*t2 + 
            15*Power(t1,2)*t2 - 17*Power(t2,2) - t1*Power(t2,2) - 
            Power(t2,3) + Power(s2,2)*(12 - 13*t1 + 14*t2) + 
            s2*(9 + 26*Power(t1,2) - 20*t2 - 2*Power(t2,2) - 
               t1*(8 + 29*t2)) + 
            Power(s1,2)*(30 + 8*Power(s2,2) - 28*Power(t1,2) + 
               s2*(-50 + 20*t1 - 44*t2) + 8*Power(t2,2) + 
               t1*(26 + 30*t2)) + 
            s1*(-16 + 16*Power(t1,3) + 
               Power(s2,2)*(-27 + 16*t1 - 20*t2) - t2 + Power(t2,2) - 
               2*Power(t1,2)*(-5 + 6*t2) - 
               t1*(35 + 36*t2 + 4*Power(t2,2)) + 
               s2*(42 - 32*Power(t1,2) + 39*t2 + 10*Power(t2,2) + 
                  t1*(17 + 32*t2)))) + 
         Power(s,3)*(-12 - 55*t1 - 27*Power(t1,2) + Power(t1,3) - 
            15*Power(t1,4) + Power(s2,3)*(15*t1 - 16*t2) - 
            20*Power(s1,4)*(s2 - t2) - 9*t2 - 8*t1*t2 - 
            28*Power(t1,2)*t2 + 9*Power(t1,3)*t2 + 41*Power(t2,2) + 
            75*t1*Power(t2,2) + 17*Power(t1,2)*Power(t2,2) - 
            28*Power(t2,3) - 11*t1*Power(t2,3) + 
            Power(s2,2)*(-65 + t1 - 45*Power(t1,2) + 38*t2 + 41*t1*t2 + 
               8*Power(t2,2)) - 
            2*Power(s1,3)*(20 + 16*Power(s2,2) - 16*Power(t1,2) + 
               6*Power(t2,2) + 3*t1*(9 + 10*t2) - s2*(45 + 38*t2)) + 
            s2*(77 + 45*Power(t1,3) - 54*t2 - 20*Power(t2,2) + 
               4*Power(t2,3) - 2*Power(t1,2)*(1 + 17*t2) + 
               t1*(92 - 10*t2 - 25*Power(t2,2))) - 
            Power(s1,2)*(20 + 12*Power(s2,3) + 48*Power(t1,3) + 
               Power(s2,2)*(-95 + 24*t1 - 76*t2) - 57*t2 + 
               Power(t2,2) - 2*Power(t1,2)*(1 + 26*t2) - 
               t1*(32 + 103*t2 + 16*Power(t2,2)) + 
               s2*(49 - 84*Power(t1,2) + 106*t2 + 32*Power(t2,2) + 
                  t1*(97 + 128*t2))) + 
            s1*(111 + 20*Power(t1,4) + Power(t1,3)*(47 - 16*t2) - 
               5*Power(s2,3)*(-3 + 4*t1 - 4*t2) - 173*t2 + 
               65*Power(t2,2) + Power(t2,3) + 
               Power(s2,2)*(37 + 60*Power(t1,2) + t1*(17 - 56*t2) - 
                  101*t2 - 20*Power(t2,2)) + 
               Power(t1,2)*(73 - 77*t2 - 8*Power(t2,2)) + 
               t1*(181 - 202*t2 - 21*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-175 - 60*Power(t1,3) + 194*t2 + 15*Power(t2,2) + 
                  Power(t1,2)*(-79 + 52*t2) + 
                  2*t1*(-55 + 89*t2 + 14*Power(t2,2))))) + 
         Power(s,2)*(202 + 388*t1 + 152*Power(t1,2) - 18*Power(t1,3) + 
            10*Power(t1,4) - 6*Power(t1,5) - 
            3*Power(s2,4)*(4 + 2*t1 - 3*t2) + 
            10*Power(s1,5)*(2*s2 - t1 - t2) - 400*t2 - 429*t1*t2 - 
            12*Power(t1,2)*t2 - 35*Power(t1,3)*t2 - 12*Power(t1,4)*t2 + 
            193*Power(t2,2) - 18*t1*Power(t2,2) - 
            6*Power(t1,2)*Power(t2,2) + 33*Power(t1,3)*Power(t2,2) + 
            22*Power(t2,3) + 75*t1*Power(t2,3) - 
            11*Power(t1,2)*Power(t2,3) - 24*Power(t2,4) - 
            4*t1*Power(t2,4) + 
            Power(s2,3)*(59 + 24*Power(t1,2) + t1*(26 - 15*t2) - 
               28*t2 - 12*Power(t2,2)) + 
            Power(s1,4)*(20 + 48*Power(s2,2) - 8*Power(t1,2) + 5*t2 + 
               8*Power(t2,2) - 8*s2*(10 + 5*t1 + 8*t2) + t1*(51 + 60*t2)\
) - Power(s2,2)*(-156 + 36*Power(t1,3) + 44*t2 - 30*Power(t2,2) + 
               6*Power(t2,3) + Power(t1,2)*(6 + 9*t2) + 
               t1*(136 - 21*t2 - 57*Power(t2,2))) + 
            s2*(-405 + 24*Power(t1,4) + 499*t2 - 91*Power(t2,2) - 
               8*Power(t2,3) + 9*Power(t1,3)*(-2 + 3*t2) + 
               Power(t1,2)*(95 + 42*t2 - 78*Power(t2,2)) + 
               t1*(-308 + 56*t2 - 24*Power(t2,2) + 17*Power(t2,3))) + 
            Power(s1,3)*(66 + 36*Power(s2,3) + 48*Power(t1,3) - 91*t2 - 
               7*Power(t2,2) - 4*Power(t1,2)*(5 + 21*t2) - 
               Power(s2,2)*(127 + 24*t1 + 108*t2) + 
               t1*(20 - 135*t2 - 24*Power(t2,2)) + 
               s2*(-1 - 60*Power(t1,2) + 136*t2 + 36*Power(t2,2) + 
                  3*t1*(49 + 64*t2))) + 
            Power(s1,2)*(-183 + 8*Power(s2,4) - 48*Power(t1,4) + 
               Power(s2,3)*(-34 + 24*t1 - 64*t2) + 220*t2 - 
               40*Power(t2,2) + Power(t2,3) + 
               Power(t1,3)*(-71 + 52*t2) + 
               Power(t1,2)*(-197 + 152*t2 + 24*Power(t2,2)) + 
               t1*(-393 + 324*t2 + 74*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s2,2)*(-183 - 120*Power(t1,2) + 221*t2 + 
                  48*Power(t2,2) + 3*t1*(-1 + 60*t2)) + 
               s2*(410 + 136*Power(t1,3) - 361*t2 - 44*Power(t2,2) - 
                  12*Power(t1,2)*(-9 + 14*t2) + 
                  t1*(380 - 373*t2 - 72*Power(t2,2)))) + 
            s1*(-187 + 14*Power(t1,5) + 
               Power(s2,4)*(15 + 14*t1 - 10*t2) + 
               Power(t1,4)*(52 - 6*t2) + 490*t2 - 352*Power(t2,2) + 
               59*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,3)*(95 - 46*t2 - 20*Power(t2,2)) + 
               Power(t1,2)*(25 - 33*t2 - 42*Power(t2,2) + 
                  12*Power(t2,3)) - 
               t1*(219 - 447*t2 + 177*Power(t2,2) + 22*Power(t2,3)) + 
               Power(s2,3)*(-136 - 56*Power(t1,2) + 121*t2 + 
                  20*Power(t2,2) + t1*(-97 + 36*t2)) + 
               Power(s2,2)*(143 + 84*Power(t1,3) + 
                  Power(t1,2)*(201 - 48*t2) - 223*t2 - 3*Power(t2,2) + 
                  t1*(367 - 288*t2 - 60*Power(t2,2))) + 
               s2*(165 - 56*Power(t1,4) - 311*t2 + 70*Power(t2,2) + 
                  29*Power(t2,3) + Power(t1,3)*(-171 + 28*t2) + 
                  Power(t1,2)*(-326 + 213*t2 + 60*Power(t2,2)) + 
                  t1*(-168 + 256*t2 + 45*Power(t2,2) - 12*Power(t2,3))))) \
- s*(323 + 564*t1 + 140*Power(t1,2) - 118*Power(t1,3) - 15*Power(t1,4) + 
            2*Power(t1,5) + 2*Power(s1,6)*(5*s2 - 4*t1 - t2) + 
            2*Power(s2,5)*(-4 + t2) - 950*t2 - 1029*t1*t2 + 
            57*Power(t1,2)*t2 + 99*Power(t1,3)*t2 - 27*Power(t1,4)*t2 + 
            10*Power(t1,5)*t2 + 877*Power(t2,2) + 325*t1*Power(t2,2) - 
            156*Power(t1,2)*Power(t2,2) + 81*Power(t1,3)*Power(t2,2) - 
            11*Power(t1,4)*Power(t2,2) - 216*Power(t2,3) + 
            141*t1*Power(t2,3) - 58*Power(t1,2)*Power(t2,3) - 
            7*Power(t1,3)*Power(t2,3) - 40*Power(t2,4) - 
            16*t1*Power(t2,4) + 8*Power(t1,2)*Power(t2,4) + 
            8*Power(t2,5) + Power(s2,4)*
             (-22 - 7*t2 - 8*Power(t2,2) + 2*t1*(17 + t2)) + 
            Power(s1,5)*(32*Power(s2,2) + 8*Power(t1,2) + 
               2*t2*(3 + t2) - s2*(35 + 40*t1 + 26*t2) + t1*(23 + 30*t2)) \
+ Power(s2,3)*(147 - 202*t2 + 12*Power(t2,2) - 4*Power(t2,3) - 
               28*Power(t1,2)*(2 + t2) + t1*(81 + 48*t2 + 35*Power(t2,2))\
) + Power(s2,2)*(127 + 107*t2 - 227*Power(t2,2) - 20*Power(t2,3) + 
               Power(t1,3)*(44 + 52*t2) - 
               3*Power(t1,2)*(37 + 34*t2 + 19*Power(t2,2)) + 
               t1*(-412 + 503*t2 + 57*Power(t2,2) + Power(t2,3))) - 
            s2*(567 - 1050*t2 + 370*Power(t2,2) + 92*Power(t2,3) + 
               8*Power(t2,4) + 2*Power(t1,4)*(8 + 19*t2) - 
               Power(t1,3)*(67 + 88*t2 + 41*Power(t2,2)) + 
               Power(t1,2)*(-383 + 400*t2 + 150*Power(t2,2) - 
                  10*Power(t2,3)) + 
               t1*(267 + 164*t2 - 383*Power(t2,2) - 78*Power(t2,3) + 
                  8*Power(t2,4))) + 
            Power(s1,4)*(43 + 36*Power(s2,3) + 16*Power(t1,3) - 40*t2 - 
               13*Power(t2,2) - Power(t1,2)*(19 + 60*t2) - 
               Power(s2,2)*(77 + 56*t1 + 68*t2) + 
               t1*(31 - 80*t2 - 16*Power(t2,2)) + 
               s2*(-21 + 4*Power(t1,2) + 80*t2 + 16*Power(t2,2) + 
                  32*t1*(3 + 4*t2))) + 
            Power(s1,3)*(-93 + 16*Power(s2,4) - 36*Power(t1,4) + 69*t2 + 
               19*Power(t2,2) + 5*Power(t2,3) + 
               7*Power(t1,3)*(-7 + 8*t2) - 
               Power(s2,3)*(27 + 12*t1 + 68*t2) + 
               3*Power(t1,2)*(-57 + 43*t2 + 8*Power(t2,2)) + 
               t1*(-275 + 174*t2 + 77*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,2)*(-187 - 60*Power(t1,2) + 191*t2 + 
                  36*Power(t2,2) + t1*(5 + 192*t2)) + 
               s2*(303 + 92*Power(t1,3) + Power(t1,2)*(71 - 180*t2) - 
                  228*t2 - 47*Power(t2,2) + 
                  t1*(358 - 320*t2 - 60*Power(t2,2)))) + 
            Power(s1,2)*(-264 + 2*Power(s2,5) + 24*Power(t1,5) + 
               Power(s2,4)*(25 + 16*t1 - 26*t2) + 
               Power(t1,4)*(55 - 18*t2) + 614*t2 - 358*Power(t2,2) + 
               10*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,3)*(175 - 36*t2 - 32*Power(t2,2)) + 
               Power(t1,2)*(75 - 77*t2 - 94*Power(t2,2) + 
                  16*Power(t2,3)) - 
               t1*(333 - 569*t2 + 135*Power(t2,2) + 37*Power(t2,3)) + 
               Power(s2,3)*(-243 - 84*Power(t1,2) + 156*t2 + 
                  32*Power(t2,2) + 2*t1*(-65 + 48*t2)) + 
               Power(s2,2)*(186 + 136*Power(t1,3) + 
                  Power(t1,2)*(240 - 132*t2) - 241*t2 - 
                  43*Power(t2,2) + t1*(661 - 348*t2 - 96*Power(t2,2))) + 
               s2*(294 - 94*Power(t1,4) - 492*t2 + 90*Power(t2,2) + 
                  34*Power(t2,3) + 10*Power(t1,3)*(-19 + 8*t2) + 
                  Power(t1,2)*(-593 + 228*t2 + 96*Power(t2,2)) + 
                  t1*(-261 + 318*t2 + 137*Power(t2,2) - 16*Power(t2,3)))) \
+ s1*(111 - 4*Power(t1,6) + Power(s2,5)*(22 + 4*t1 - 2*t2) - 57*t2 - 
               175*Power(t2,2) + 105*Power(t2,3) + 8*Power(t2,4) - 
               2*Power(t1,5)*(13 + 3*t2) + 
               Power(s2,4)*(-65 - 20*Power(t1,2) + 2*t1*(-57 + t2) + 
                  85*t2 + 10*Power(t2,2)) + 
               Power(t1,4)*(-5 + 27*t2 + 22*Power(t2,2)) - 
               Power(t1,3)*(-208 + 142*t2 + 31*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(538 - 610*t2 + 171*Power(t2,2) + 
                  79*Power(t2,3)) + 
               t1*(458 - 572*t2 + 145*Power(t2,2) - 8*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(s2,3)*(-134 + 40*Power(t1,3) + 44*t2 + 
                  43*Power(t2,2) + 4*Power(t1,2)*(59 + 3*t2) + 
                  t1*(200 - 282*t2 - 52*Power(t2,2))) - 
               Power(s2,2)*(-558 + 40*Power(t1,4) + 698*t2 - 
                  263*Power(t2,2) - 61*Power(t2,3) + 
                  4*Power(t1,3)*(61 + 7*t2) - 
                  6*Power(t1,2)*(-35 + 56*t2 + 16*Power(t2,2)) + 
                  t1*(-476 + 230*t2 + 117*Power(t2,2) + 12*Power(t2,3))) \
+ s2*(-492 + 20*Power(t1,5) + 700*t2 - 325*Power(t2,2) + 
                  110*Power(t2,3) + 16*Power(t2,4) + 
                  2*Power(t1,4)*(63 + 11*t2) - 
                  2*Power(t1,3)*(-40 + 83*t2 + 38*Power(t2,2)) + 
                  Power(t1,2)*
                   (-550 + 328*t2 + 105*Power(t2,2) + 24*Power(t2,3)) - 
                  2*t1*(548 - 654*t2 + 217*Power(t2,2) + 70*Power(t2,3))))\
))*T2q(1 - s + s1 - t2,1 - s2 + t1 - t2))/
     ((s - s2 + t1)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       Power(s - s1 + t2,2)*(-1 + s2 - t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) - 
    (8*(Power(s,6)*(-1 + 2*s1)*(t1 - t2) + 
         Power(s,5)*(11 - 12*s2 + 13*t1 + 6*s2*t1 - 6*Power(t1,2) - 
            11*t2 - 7*s2*t2 + 7*t1*t2 - Power(t2,2) + 
            2*Power(s1,2)*(-2 + s2 - 3*t1 + 4*t2) + 
            s1*(-8 + 6*Power(t1,2) + 9*t2 - 4*Power(t2,2) - 
               2*t1*(7 + t2) + s2*(11 - 6*t1 + 8*t2))) + 
         Power(s,4)*(-48 - 40*t1 + 28*Power(t1,2) - 4*Power(t1,3) + 
            Power(s1,3)*(8 - 4*s2 + 6*t1 - 10*t2) + 79*t2 + t1*t2 - 
            16*Power(t1,2)*t2 - 19*Power(t2,2) + 34*t1*Power(t2,2) - 
            14*Power(t2,3) + Power(s2,2)*(8 - 4*t1 + 6*t2) + 
            s2*(46 + 8*Power(t1,2) - 16*t2 - 23*Power(t2,2) + 
               2*t1*(-18 + 5*t2)) + 
            Power(s1,2)*(38 - 2*Power(s2,2) - 16*Power(t1,2) + 
               s2*(-33 + 18*t1 - 22*t2) - 46*t2 + 10*Power(t2,2) + 
               t1*(37 + 16*t2)) + 
            s1*(-7 + 8*Power(t1,3) + 2*Power(s2,2)*(-9 + 4*t1 - 5*t2) + 
               4*Power(t1,2)*(-4 + t2) - 19*t2 + 28*Power(t2,2) - 
               t1*(5 + 37*t2 + 12*Power(t2,2)) + 
               s2*(-16*Power(t1,2) + t1*(34 + 6*t2) + 
                  4*(3 + 5*t2 + 5*Power(t2,2))))) + 
         2*Power(s,3)*(49 + 4*Power(s2,3) + 26*t1 - 36*Power(t1,2) + 
            4*Power(t1,3) - 110*t2 - 9*t1*t2 + 37*Power(t1,2)*t2 - 
            8*Power(t1,3)*t2 + 68*Power(t2,2) - 37*t1*Power(t2,2) - 
            2*Power(t1,2)*Power(t2,2) + 6*Power(t2,3) + 
            23*t1*Power(t2,3) - 13*Power(t2,4) + 
            Power(s1,4)*(-2 + s2 - t1 + 2*t2) + 
            Power(s2,2)*(-28 + 13*t2 + 12*Power(t2,2) - 
               4*t1*(1 + 2*t2)) + 
            Power(s1,3)*(Power(s2,2) + 5*Power(t1,2) - 
               2*t1*(4 + 5*t2) + s2*(8 - 6*t1 + 10*t2) - 
               3*(5 - 6*t2 + Power(t2,2))) + 
            s2*(-31 + 29*t2 + 10*Power(t2,2) - 11*Power(t2,3) + 
               4*Power(t1,2)*(-1 + 4*t2) - 
               2*t1*(-32 + 25*t2 + 5*Power(t2,2))) + 
            Power(s1,2)*(-25 - 10*Power(t1,3) + 61*t2 - 
               33*Power(t2,2) - 3*Power(t2,3) + 
               5*Power(t1,2)*(6 + t2) + 
               Power(s2,2)*(28 - 10*t1 + 13*t2) + 
               t1*(-5 + 5*t2 + 18*Power(t2,2)) + 
               s2*(1 + 20*Power(t1,2) + t2 - 20*Power(t2,2) - 
                  2*t1*(29 + 9*t2))) + 
            s1*(30 + 2*Power(t1,4) - 2*Power(s2,3)*(t1 - t2) - 41*t2 - 
               14*Power(t2,2) + 23*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,3)*(-6 + 8*t2) + 
               Power(t1,2)*(9 - 8*t2 - 10*Power(t2,2)) - 
               t1*(-37 + 3*t2 + 34*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(29 + 6*Power(t1,2) - 24*t2 - 
                  14*Power(t2,2) + t1*(-6 + 4*t2)) + 
               s2*(-50 - 6*Power(t1,3) + 37*t2 + 9*Power(t2,2) + 
                  6*Power(t2,3) - 2*Power(t1,2)*(-6 + 7*t2) + 
                  t1*(-38 + 32*t2 + 24*Power(t2,2))))) + 
         Power(s,2)*(-116 - 23*t1 + 128*Power(t1,2) + 12*Power(t1,3) - 
            6*Power(s1,4)*(s2 - t1)*(-1 + t2) + 313*t2 - 68*t1*t2 - 
            226*Power(t1,2)*t2 + 24*Power(t1,3)*t2 - 276*Power(t2,2) + 
            170*t1*Power(t2,2) + 54*Power(t1,2)*Power(t2,2) - 
            24*Power(t1,3)*Power(t2,2) + 58*Power(t2,3) - 
            98*t1*Power(t2,3) + 24*Power(t1,2)*Power(t2,3) + 
            40*Power(t2,4) + 19*t1*Power(t2,4) - 19*Power(t2,5) + 
            8*Power(s2,3)*(-4 + 3*t2) + 
            2*Power(s1,3)*(6*Power(t1,3) + 
               Power(s2,2)*(-17 + 6*t1 - 15*t2) + 
               Power(-1 + t2,2)*(11 + 3*t2) - 
               Power(t1,2)*(17 + 15*t2) - 
               6*t1*(2 - 3*t2 + Power(t2,2)) + 
               2*s2*(6 + 17*t1 - 6*Power(t1,2) - 9*t2 + 15*t1*t2 + 
                  3*Power(t2,2))) + 
            Power(s2,2)*(132 - 218*t2 + 30*Power(t2,2) + 
               36*Power(t2,3) - 4*t1*(-19 + 6*t2 + 6*Power(t2,2))) + 
            2*s2*(13 + 21*t2 - 53*Power(t2,2) + 18*Power(t2,3) + 
               Power(t2,4) + 
               4*Power(t1,2)*(-7 - 3*t2 + 6*Power(t2,2)) - 
               2*t1*(65 - 111*t2 + 21*Power(t2,2) + 15*Power(t2,3))) + 
            2*Power(s1,2)*(-4*Power(t1,4) - 4*Power(t1,3)*(-5 + 3*t2) - 
               Power(-1 + t2,2)*(-13 + 36*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(-46 + 32*t1 - 12*Power(t1,2) + 27*t2 + 
                  27*Power(t2,2)) + 
               Power(t1,2)*(-30 + 5*t2 + 33*Power(t2,2)) - 
               2*t1*(8 - 2*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(4*t1 - 6*(1 + t2)) + 
               2*s2*(14 + 6*Power(t1,3) - 16*t2 + 3*Power(t2,2) - 
                  Power(t2,3) + Power(t1,2)*(-23 + 9*t2) - 
                  2*t1*(-19 + 8*t2 + 15*Power(t2,2)))) + 
            2*s1*(4*Power(s2,4) - 6*Power(t1,3)*(-1 + t2) + 
               Power(t1,4)*(-2 + 6*t2) + 
               Power(t1,2)*(-3 + 25*t2 - 12*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-47 + 36*t2 + 26*Power(t2,2) + Power(t2,3)) + 
               t1*(-61 + 105*t2 - 8*Power(t2,2) - 41*Power(t2,3) + 
                  5*Power(t2,4)) + 
               2*Power(s2,3)*
                (-8 + 3*t2 + 3*Power(t2,2) - t1*(5 + 3*t2)) + 
               Power(s2,2)*(-19 + 65*t2 - 24*Power(t2,2) - 
                  12*Power(t2,3) + 6*Power(t1,2)*(1 + 3*t2) - 
                  2*t1*(-19 + 9*t2 + 6*Power(t2,2))) + 
               s2*(75 + Power(t1,3)*(2 - 18*t2) - 160*t2 + 
                  79*Power(t2,2) + 8*Power(t2,3) - 2*Power(t2,4) + 
                  2*Power(t1,2)*(-14 + 9*t2 + 3*Power(t2,2)) + 
                  t1*(22 - 90*t2 + 24*Power(t2,2) + 24*Power(t2,3))))) + 
         (-1 + t2)*(-2*Power(s1,4)*(s2 - t1)*
             (2*Power(s2,2) - 4*s2*t1 + 2*Power(t1,2) - 
               Power(-1 + t2,2)) - 
            2*Power(s1,3)*(-2*Power(t1,4) + 
               2*Power(s2,3)*(-1 + t1 - 3*t2) - Power(-1 + t2,3) - 
               3*t1*Power(-1 + t2,2)*(1 + t2) + 
               Power(t1,3)*(2 + 6*t2) + 
               Power(t1,2)*(-1 + Power(t2,2)) - 
               Power(s2,2)*(3 + 6*Power(t1,2) - 4*t2 + Power(t2,2) - 
                  6*t1*(1 + 3*t2)) + 
               2*s2*(2 + 3*Power(t1,3) - 2*t1*(-1 + t2) - 3*t2 + 
                  Power(t2,3) - 3*Power(t1,2)*(1 + 3*t2))) - 
            Power(s1,2)*(8*Power(s2,4) + 8*Power(t1,4)*t2 + 
               2*Power(-1 + t2,3)*(4 + 3*t2) + 
               t1*Power(-1 + t2,3)*(13 + 4*t2) + 
               4*Power(t1,3)*(8 + t2 - 4*Power(t2,2)) + 
               2*Power(t1,2)*(10 - t2 - 10*Power(t2,2) + Power(t2,3)) - 
               4*Power(s2,3)*
                (11 - 3*t2 - 3*Power(t2,2) + 2*t1*(3 + t2)) + 
               2*Power(s2,2)*
                (11 - 2*t2 - 11*Power(t2,2) + 2*Power(t2,3) + 
                  12*Power(t1,2)*(1 + t2) - 
                  10*t1*(-6 + t2 + 2*Power(t2,2))) - 
               s2*(Power(-1 + t2,3)*(17 + 2*t2) + 
                  8*Power(t1,3)*(1 + 3*t2) - 
                  4*Power(t1,2)*(-27 + t2 + 11*Power(t2,2)) + 
                  6*t1*(7 - t2 - 7*Power(t2,2) + Power(t2,3)))) + 
            (-1 + t2)*(8*Power(s2,3)*(-2 + t2) - 
               4*Power(t1,3)*(-4 + Power(t2,2)) + 
               Power(-1 + t2,2)*(-16 - 9*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(48 - 78*t2 + 6*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(16 - 81*t2 + 76*Power(t2,2) - 7*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,2)*(48 - 84*t2 + 14*Power(t2,2) + 
                  6*Power(t2,3) - 4*t1*(-12 + 4*t2 + Power(t2,2))) + 
               s2*(-16 + 82*t2 - 77*Power(t2,2) + 6*Power(t2,3) + 
                  5*Power(t2,4) + 
                  8*Power(t1,2)*(-6 + t2 + Power(t2,2)) - 
                  2*t1*(48 - 81*t2 + 10*Power(t2,2) + 7*Power(t2,3)))) + 
            s1*(8*Power(s2,4)*(-2 + t2) + 
               4*Power(t1,4)*(-4 + Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(1 - 6*t2 + Power(t2,2)) + 
               Power(-1 + t2,3)*(-15 + 13*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(-48 + 68*t2 + 4*Power(t2,2) - 
                  8*Power(t2,3)) + 
               2*Power(t1,2)*
                (-27 + 60*t2 - 29*Power(t2,2) - 6*Power(t2,3) + 
                  2*Power(t2,4)) + 
               4*Power(s2,3)*
                (14 - 23*t2 + 4*Power(t2,2) + Power(t2,3) - 
                  t1*(-16 + 6*t2 + Power(t2,2))) + 
               2*Power(s2,2)*
                (-30 + 68*t2 - 37*Power(t2,2) - 2*Power(t2,3) + 
                  Power(t2,4) + 
                  6*Power(t1,2)*(-8 + 2*t2 + Power(t2,2)) - 
                  2*t1*(40 - 63*t2 + 7*Power(t2,2) + 4*Power(t2,3))) - 
               2*s2*(Power(-1 + t2,2)*(-1 + t2 + 2*Power(t2,2)) + 
                  Power(t1,3)*(-32 + 4*t2 + 6*Power(t2,2)) - 
                  2*Power(t1,2)*
                   (38 - 57*t2 + 2*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(-57 + 128*t2 - 66*Power(t2,2) - 8*Power(t2,3) + 
                     3*Power(t2,4))))) + 
         s*(6*Power(s1,4)*(s2 - t1)*(-1 + t2)*(-1 + 2*s2 - 2*t1 + t2) - 
            2*Power(s1,3)*(14*Power(t1,3) - 2*Power(t1,4) + 
               2*Power(s2,3)*(-4 + t1 - 3*t2) - 
               Power(-1 + t2,3)*(7 + t2) - 
               2*t1*Power(-1 + t2,2)*(1 + 5*t2) + 
               Power(t1,2)*(5 - 26*t2 + 21*Power(t2,2)) + 
               Power(s2,2)*(-1 - 6*Power(t1,2) - 14*t2 + 
                  15*Power(t2,2) + 6*t1*(5 + 2*t2)) + 
               2*s2*(3*Power(t1,3) - 3*Power(t1,2)*(6 + t2) + 
                  2*Power(-1 + t2,2)*(1 + 2*t2) - 
                  2*t1*(1 - 10*t2 + 9*Power(t2,2)))) + 
            (-1 + t2)*(8*Power(s2,3)*(-5 + 3*t2) + 
               8*Power(t1,3)*(4 + t2 - 2*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (71 - 8*t2 - 8*Power(t2,2) + 5*Power(t2,3)) + 
               2*Power(t1,2)*
                (63 - 106*t2 + 12*Power(t2,2) + 13*Power(t2,3)) + 
               t1*(17 - 166*t2 + 194*Power(t2,2) - 40*Power(t2,3) - 
                  5*Power(t2,4)) + 
               2*Power(s2,2)*
                (66 - 115*t2 + 19*Power(t2,2) + 12*Power(t2,3) - 
                  4*t1*(-14 + 5*t2 + 2*Power(t2,2))) + 
               s2*(-18 + 165*t2 - 181*Power(t2,2) + 21*Power(t2,3) + 
                  13*Power(t2,4) + 
                  8*Power(t1,2)*(-13 + t2 + 4*Power(t2,2)) - 
                  2*t1*(129 - 221*t2 + 31*Power(t2,2) + 25*Power(t2,3)))) \
- 2*Power(s1,2)*(4*Power(s2,4) + Power(t1,4)*(-4 + 8*t2) + 
               Power(-1 + t2,3)*(5 + 20*t2 + Power(t2,2)) - 
               2*Power(t1,3)*(-10 + 5*t2 + 3*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(-30 - 7*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(-16 + 21*t2 + 14*Power(t2,2) - 
                  19*Power(t2,3)) - 
               2*Power(s2,3)*
                (11 - 3*t2 - 6*Power(t2,2) + 4*t1*(1 + t2)) + 
               Power(s2,2)*(-26 + 49*t2 + 24*Power(t1,2)*t2 - 
                  12*Power(t2,2) - 11*Power(t2,3) + 
                  t1*(64 - 22*t2 - 30*Power(t2,2))) + 
               s2*(Power(t1,3)*(8 - 24*t2) - 
                  Power(-1 + t2,2)*(-40 + 7*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*(-62 + 26*t2 + 24*Power(t2,2)) + 
                  t1*(42 - 70*t2 - 2*Power(t2,2) + 30*Power(t2,3)))) + 
            s1*(8*Power(s2,4)*(-3 + 2*t2) + 
               4*Power(t1,4)*(-4 - 2*t2 + 3*Power(t2,2)) + 
               Power(-1 + t2,3)*(-68 + 59*t2 + 25*Power(t2,2)) + 
               2*t1*Power(-1 + t2,2)*
                (38 - 32*t2 - 13*Power(t2,2) + 3*Power(t2,3)) - 
               4*Power(t1,3)*
                (13 - 19*t2 - 3*Power(t2,2) + 4*Power(t2,3)) - 
               2*Power(t1,2)*(22 - 38*t2 + 7*Power(t2,2) + 
                  8*Power(t2,3) + Power(t2,4)) + 
               4*Power(s2,3)*(21 - 35*t2 + 6*Power(t2,2) + 
                  3*Power(t2,3) + t1*(22 - 10*t2 - 3*Power(t2,2))) + 
               2*Power(s2,2)*(-25 + 38*t2 + Power(t2,2) - 
                  12*Power(t2,3) - 2*Power(t2,4) + 
                  6*Power(t1,2)*(-10 + 2*t2 + 3*Power(t2,2)) - 
                  2*t1*(55 - 89*t2 + 9*Power(t2,2) + 10*Power(t2,3))) + 
               s2*(Power(t1,3)*(72 + 8*t2 - 36*Power(t2,2)) - 
                  Power(-1 + t2,2)*
                   (83 - 100*t2 + 5*Power(t2,2) + 4*Power(t2,3)) + 
                  4*Power(t1,2)*(47 - 73*t2 + 11*Power(t2,3)) + 
                  2*t1*(47 - 76*t2 + 6*Power(t2,2) + 20*Power(t2,3) + 
                     3*Power(t2,4))))))*T3q(1 - s + s1 - t2,s1))/
     ((s - s2 + t1)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       Power(-1 + s + t2,2)*Power(s - s1 + t2,2)*(-1 + s2 - t1 + t2)) - 
    (8*(16 + 16*s2 - 48*Power(s2,2) + 16*Power(s2,3) + 
         2*Power(s1,6)*(s2 - t1) - 16*t1 + 96*s2*t1 - 48*Power(s2,2)*t1 - 
         48*Power(t1,2) + 48*s2*Power(t1,2) - 16*Power(t1,3) - t2 - 
         106*s2*t2 + 88*Power(s2,2)*t2 - 16*Power(s2,3)*t2 + 95*t1*t2 - 
         154*s2*t1*t2 + 40*Power(s2,2)*t1*t2 + 66*Power(t1,2)*t2 - 
         32*s2*Power(t1,2)*t2 + 8*Power(t1,3)*t2 - 40*Power(t2,2) + 
         93*s2*Power(t2,2) - 30*Power(s2,2)*Power(t2,2) - 
         69*t1*Power(t2,2) + 30*s2*t1*Power(t2,2) + 
         8*Power(s2,2)*t1*Power(t2,2) - 16*s2*Power(t1,2)*Power(t2,2) + 
         8*Power(t1,3)*Power(t2,2) + 13*Power(t2,3) + 7*s2*Power(t2,3) - 
         8*Power(s2,2)*Power(t2,3) - 20*t1*Power(t2,3) + 
         24*s2*t1*Power(t2,3) - 16*Power(t1,2)*Power(t2,3) + 
         12*Power(t2,4) - 8*s2*Power(t2,4) + 8*t1*Power(t2,4) + 
         2*Power(s1,5)*(-1 + Power(s2,2) + t1 - Power(t1,2) + t2 + 
            3*t1*t2 - 2*s2*(1 + t2)) - 
         Power(s,3)*(-1 + s1)*
          (-8 + t1 + 8*Power(t1,2) + s1*(8 - 8*s2 + 5*t1 - 5*t2) + 
            2*Power(s1,2)*(t1 - t2) - t2 - 16*t1*t2 + 8*Power(t2,2) + 
            s2*(8 - 8*t1 + 8*t2)) + 
         Power(s1,4)*(4 + 4*Power(t1,3) + 4*Power(s2,2)*(-2 + t1 - t2) + 
            2*t2 - 6*Power(t2,2) - 2*Power(t1,2)*(1 + t2) - 
            t1*(1 + t2 + 4*Power(t2,2)) + 
            s2*(1 - 8*Power(t1,2) + 3*t2 + 2*Power(t2,2) + 
               2*t1*(5 + 3*t2))) + 
         Power(s1,2)*(-8 + 8*Power(t1,4) - 8*Power(s2,3)*(t1 - 2*t2) - 
            12*Power(t1,3)*(-2 + t2) + 23*t2 - 26*Power(t2,2) + 
            11*Power(t2,3) + Power(t1,2)*(-22 - 60*t2 + 4*Power(t2,2)) + 
            2*Power(s2,2)*(-13 + 12*Power(t1,2) + t1*(12 - 22*t2) - 
               28*t2 + 6*Power(t2,2)) + 
            t1*(-27 + 22*t2 + 15*Power(t2,2)) + 
            s2*(23 - 24*Power(t1,3) - 17*t2 - 17*Power(t2,2) + 
               Power(t2,3) + 8*Power(t1,2)*(-6 + 5*t2) + 
               t1*(48 + 116*t2 - 16*Power(t2,2)))) + 
         Power(s1,3)*(-3 - 2*t2 + Power(t2,2) + 4*Power(t2,3) - 
            4*Power(t1,3)*(3 + t2) + t1*(-13 + 14*t2 - 9*Power(t2,2)) + 
            Power(t1,2)*(-4 + 22*t2 + 4*Power(t2,2)) + 
            2*Power(s2,2)*(-2*t1*(3 + t2) + t2*(10 + t2)) + 
            2*s2*(4*Power(t1,2)*(3 + t2) + 
               t1*(2 - 21*t2 - 3*Power(t2,2)) + 
               4*(2 - 2*t2 + Power(t2,2)))) + 
         s1*(-23 - 16*Power(s2,4) + 16*t2 + 47*Power(t2,2) - 
            36*Power(t2,3) - 4*Power(t2,4) - 8*Power(t1,4)*(2 + t2) + 
            8*Power(t1,3)*(-5 + t2 + 2*Power(t2,2)) + 
            t1*(1 - 40*t2 + 51*Power(t2,2) - 12*Power(t2,3)) + 
            Power(t1,2)*(6 + 38*t2 + 24*Power(t2,2) - 8*Power(t2,3)) + 
            8*Power(s2,3)*(6 - 5*t2 - Power(t2,2) + t1*(8 + t2)) - 
            8*Power(s2,2)*(2 - 9*t2 - Power(t2,2) + Power(t2,3) + 
               3*Power(t1,2)*(4 + t2) + t1*(17 - 11*t2 - 4*Power(t2,2))) \
+ 2*s2*(5 + 10*t2 - 23*Power(t2,2) + 8*Power(t2,3) + 
               4*Power(t1,3)*(8 + 3*t2) - 
               4*Power(t1,2)*(-16 + 7*t2 + 5*Power(t2,2)) + 
               t1*(5 - 55*t2 - 16*Power(t2,2) + 8*Power(t2,3)))) + 
         Power(s,2)*(9 + 18*t1 + 22*Power(t1,2) + 8*Power(t1,3) + 
            2*Power(s1,4)*(s2 + t1 - 2*t2) + 
            8*Power(s2,2)*(1 + t1 - t2) - 40*t2 - 38*t1*t2 + 
            16*Power(t2,2) - 24*t1*Power(t2,2) + 16*Power(t2,3) + 
            Power(s1,3)*(8 - 6*Power(t1,2) + s2*(-15 + 6*t1 - 4*t2) + 
               t2 + 2*Power(t2,2) + 4*t1*(1 + t2)) + 
            s2*(-16 - 16*Power(t1,2) + 51*t2 + 8*Power(t2,2) + 
               t1*(-30 + 8*t2)) + 
            Power(s1,2)*(-31 - 16*Power(s2,2) + 8*Power(t1,2) + 12*t2 + 
               10*Power(t2,2) - 2*t1*(19 + 9*t2) + s2*(56 + 8*t1 + 9*t2)\
) + s1*(14 - 16*Power(t1,3) + 39*t2 - 44*Power(t2,2) - 8*Power(t2,3) + 
               8*Power(t1,2)*(-5 + 3*t2) + 
               Power(s2,2)*(8 - 16*t1 + 16*t2) + t1*(6 + 84*t2) + 
               s2*(-27 + 32*Power(t1,2) - 72*t2 + 8*Power(t2,2) - 
                  8*t1*(-4 + 5*t2)))) - 
         s*(17 + 16*Power(s2,3) + t1 - 18*Power(t1,2) - 8*Power(t1,3) - 
            17*t2 + 3*t1*t2 - 22*Power(t1,2)*t2 - 16*Power(t1,3)*t2 + 
            35*Power(t2,2) + 59*t1*Power(t2,2) + 
            24*Power(t1,2)*Power(t2,2) - 29*Power(t2,3) - 8*Power(t2,4) + 
            Power(s1,4)*(-2 + 2*Power(s2,2) + 3*t1 - 8*Power(t1,2) + 
               s2*(-11 + 6*t1 - 8*t2) + 6*t2 + 10*t1*t2 + 2*Power(t2,2)) \
+ Power(s1,5)*(4*s2 - 2*(t1 + t2)) + 
            2*Power(s2,2)*(-20 + 11*t2 + 8*Power(t2,2) - 
               4*t1*(5 + 2*t2)) + 
            s2*(10 - 29*t2 - 50*Power(t2,2) + 8*Power(t2,3) + 
               32*Power(t1,2)*(1 + t2) + t1*(58 - 40*Power(t2,2))) + 
            Power(s1,3)*(13 + 4*Power(t1,3) + 
               2*Power(s2,2)*(-8 + 2*t1 - t2) - 16*t2 - Power(t2,2) + 
               2*Power(t1,2)*(3 + t2) - 
               3*t1*(1 + 5*t2 + 2*Power(t2,2)) + 
               s2*(9 + 10*t1 - 8*Power(t1,2) + 15*t2 + 4*Power(t2,2))) + 
            Power(s1,2)*(3 - 8*Power(s2,3) - 20*Power(t1,3) - 
               4*Power(s2,2)*(-10 + t1 - 7*t2) + 39*t2 - 
               47*Power(t2,2) + Power(t2,3) + 
               4*Power(t1,2)*(-1 + 9*t2) + 
               t1*(18 + 79*t2 - 17*Power(t2,2)) + 
               s2*(-27 + 32*Power(t1,2) - 71*t2 + 22*Power(t2,2) - 
                  4*t1*(9 + 16*t2))) - 
            s1*(15 - 56*Power(t1,3) - 8*Power(t1,4) + 
               8*Power(s2,3)*(3 + t1 - t2) + 58*t2 - 51*Power(t2,2) - 
               28*Power(t2,3) - 
               2*Power(s2,2)*(31 + 12*Power(t1,2) + t1*(52 - 8*t2) - 
                  56*t2 - 4*Power(t2,2)) + 
               8*Power(t1,2)*(-10 + 10*t2 + 3*Power(t2,2)) + 
               t1*(-31 + 173*t2 + 4*Power(t2,2) - 16*Power(t2,3)) + 
               s2*(33 + 24*Power(t1,3) - 8*Power(t1,2)*(-17 + t2) - 
                  197*t2 + 24*Power(t2,2) + 16*Power(t2,3) - 
                  2*t1*(-71 + 96*t2 + 16*Power(t2,2))))))*T4q(s1))/
     ((-1 + s1)*(s - s2 + t1)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       Power(s - s1 + t2,2)*(-1 + s2 - t1 + t2)) + 
    (8*(-16*s2 + 40*Power(s2,2) - 8*Power(s2,3) + 16*t1 - 80*s2*t1 + 
         24*Power(s2,2)*t1 + 40*Power(t1,2) - 24*s2*Power(t1,2) + 
         8*Power(t1,3) - 9*t2 + 79*s2*t2 - 66*Power(s2,2)*t2 + 
         8*Power(s2,3)*t2 - 78*t1*t2 + 125*s2*t1*t2 - 
         22*Power(s2,2)*t1*t2 - 59*Power(t1,2)*t2 + 20*s2*Power(t1,2)*t2 - 
         6*Power(t1,3)*t2 + 21*Power(t2,2) - 77*s2*Power(t2,2) + 
         21*Power(s2,2)*Power(t2,2) + 2*Power(s2,3)*Power(t2,2) + 
         76*t1*Power(t2,2) - 34*s2*t1*Power(t2,2) - 
         8*Power(s2,2)*t1*Power(t2,2) + 13*Power(t1,2)*Power(t2,2) + 
         10*s2*Power(t1,2)*Power(t2,2) - 4*Power(t1,3)*Power(t2,2) - 
         15*Power(t2,3) + 9*s2*Power(t2,3) + 7*Power(s2,2)*Power(t2,3) - 
         10*t1*Power(t2,3) - 15*s2*t1*Power(t2,3) + 
         8*Power(t1,2)*Power(t2,3) + 3*Power(t2,4) + 5*s2*Power(t2,4) - 
         4*t1*Power(t2,4) + 2*Power(s1,4)*(s2 - t1)*(-1 + s2 - t1 + t2) + 
         Power(s,3)*(8*Power(s1,2) + s2*(-4 + 3*t1 - 3*t2) - 
            3*(-4 + t1 + Power(t1,2) - t2 - 2*t1*t2 + Power(t2,2)) + 
            2*s1*(-10 + t1 + Power(t1,2) - t2 - 2*t1*t2 + Power(t2,2) + 
               s2*(2 - t1 + t2))) + 
         2*Power(s1,3)*(Power(s2,3) + Power(t1,3) + Power(-1 + t2,2) - 
            4*Power(t1,2)*(1 + t2) - Power(s2,2)*(7 + t1 + t2) + 
            t1*(-2 - t2 + 3*Power(t2,2)) - 
            s2*(-3 + Power(t1,2) + t2 + 2*Power(t2,2) - t1*(11 + 5*t2))) + 
         Power(s1,2)*(-4*Power(t1,4) + Power(s2,3)*(-2 + 4*t1 - 4*t2) - 
            2*Power(-1 + t2,2)*(4 + 3*t2) + Power(t1,3)*(-8 + 6*t2) + 
            Power(t1,2)*(7 + 19*t2 + 2*Power(t2,2)) + 
            t1*(-25 + 28*t2 + Power(t2,2) - 4*Power(t2,3)) + 
            Power(s2,2)*(5 - 12*Power(t1,2) + 25*t2 - 2*Power(t2,2) + 
               2*t1*(-2 + 7*t2)) + 
            s2*(29 + 12*Power(t1,3) - 38*t2 + 7*Power(t2,2) + 
               2*Power(t2,3) - 2*Power(t1,2)*(-7 + 8*t2) - 
               4*t1*(3 + 11*t2))) + 
         s1*(8*Power(s2,4) + 4*Power(t1,4)*(2 + t2) - 
            2*Power(t1,3)*(-19 + 6*t2 + 4*Power(t2,2)) + 
            Power(-1 + t2,2)*(-15 + 13*t2 + 4*Power(t2,2)) + 
            t1*(6 + 5*t2 - 6*Power(t2,2) - 5*Power(t2,3)) + 
            Power(t1,2)*(11 - 52*t2 + 5*Power(t2,2) + 4*Power(t2,3)) + 
            2*Power(s2,3)*(-20 + 12*t2 + Power(t2,2) - 2*t1*(8 + t2)) + 
            2*Power(s2,2)*(9 - 29*t2 + 3*Power(t2,2) + Power(t2,3) + 
               6*Power(t1,2)*(4 + t2) + t1*(59 - 30*t2 - 6*Power(t2,2))) \
- s2*(7 + 8*t2 - 15*Power(t2,2) + 4*Power(t1,3)*(8 + 3*t2) - 
               2*Power(t1,2)*(-58 + 24*t2 + 9*Power(t2,2)) + 
               t1*(29 - 110*t2 + 11*Power(t2,2) + 6*Power(t2,3)))) + 
         Power(s,2)*(-19 - 8*Power(s1,3) - 17*t1 - 4*Power(t1,2) - 
            2*Power(t1,3) + 51*t2 - 4*Power(t1,2)*t2 + 4*Power(t2,2) + 
            14*t1*Power(t2,2) - 8*Power(t2,3) + 
            Power(s2,2)*(-2*t1 + t2) + 
            2*Power(s1,2)*(22 + Power(s2,2) - Power(t1,2) + 2*t2 - 
               2*Power(t2,2) + 3*t1*(3 + t2) - s2*(13 + t2)) + 
            s2*(23 + 4*Power(t1,2) - 17*t2 - 7*Power(t2,2) + 
               t1*(4 + 3*t2)) + 
            s1*(-20 + 6*Power(t1,3) + Power(s2,2)*(-5 + 6*t1 - 4*t2) - 
               59*t2 + 13*Power(t2,2) + 2*Power(t2,3) - 
               2*Power(t1,2)*(-9 + 5*t2) + 
               t1*(4 - 31*t2 + 2*Power(t2,2)) + 
               s2*(9 - 12*Power(t1,2) + 26*t2 - 2*Power(t2,2) + 
                  t1*(-13 + 14*t2)))) - 
         s*(-7 - 2*t1 + 35*Power(t1,2) + 6*Power(t1,3) + 38*t2 - 
            35*t1*t2 - 9*Power(t1,2)*t2 + 6*Power(t1,3)*t2 - 
            32*Power(t2,2) + 7*t1*Power(t2,2) - 
            7*Power(t1,2)*Power(t2,2) - 4*Power(t2,3) - 4*t1*Power(t2,3) + 
            5*Power(t2,4) - 2*Power(s2,3)*(4 + t2) + 
            2*Power(s1,3)*(4 + 2*Power(s2,2) + 11*t1 + Power(t1,2) - 
               3*t2 - Power(t2,2) + s2*(-12 - 3*t1 + t2)) + 
            Power(s2,2)*(42 - 21*t2 - 8*Power(t2,2) + 2*t1*(11 + 5*t2)) + 
            s2*(1 + 30*t2 + 4*Power(t2,2) - Power(t2,3) - 
               2*Power(t1,2)*(10 + 7*t2) + 
               t1*(-77 + 30*t2 + 15*Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,3) + 8*Power(t1,3) + 
               Power(t1,2)*(7 - 18*t2) + 
               Power(s2,2)*(-19 + 4*t1 - 6*t2) + 
               t1*(-43 - 47*t2 + 8*Power(t2,2)) + 
               2*(9 - 20*t2 + 10*Power(t2,2) + Power(t2,3)) + 
               s2*(51 - 14*Power(t1,2) + 37*t2 - 6*Power(t2,2) + 
                  12*t1*(1 + 2*t2))) + 
            s1*(-51 - 4*Power(t1,4) + Power(s2,3)*(6 + 4*t1 - 2*t2) + 
               2*Power(t1,3)*(-9 + t2) + 67*t2 - 3*Power(t2,2) - 
               13*Power(t2,3) + 
               Power(s2,2)*(-59 - 12*Power(t1,2) + 6*t1*(-5 + t2) + 
                  41*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-61 + 19*t2 + 8*Power(t2,2)) - 
               2*t1*(13 - 57*t2 - 6*Power(t2,2) + 3*Power(t2,3)) + 
               2*s2*(18 + 6*Power(t1,3) - 3*Power(t1,2)*(-7 + t2) - 
                  69*t2 + 2*Power(t2,2) + 2*Power(t2,3) - 
                  5*t1*(-12 + 6*t2 + Power(t2,2))))))*T5q(1 - s2 + t1 - t2))/
     ((s - s2 + t1)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*Power(s - s1 + t2,2)*
       (-1 + s2 - t1 + t2)));
   return a;
};
