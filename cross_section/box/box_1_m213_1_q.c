#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m213_1_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(2*Power(s1,6)*(1 + (2 + s2 + t1)*t2) + 
         Power(s1,4)*(-4 + (Power(s,2)*t1 + 
               s*(-11 - 25*s2 + 42*t1 - 2*s2*t1 + 8*Power(t1,2)) + 
               2*(-13 - 6*s2 + 5*Power(s2,2) + 26*t1 - 22*s2*t1 + 
                  14*Power(t1,2)))*t2 + 
            (-56 + Power(s,2) + 52*s2 - 10*Power(s2,2) + 
               s*(-51 + 7*s2 - 16*t1) - 28*t1 + 8*Power(t1,2))*
             Power(t2,2) - 2*(-15 + s - 4*s2 + 13*t1)*Power(t2,3)) + 
         2*(-1 + t1 - t2)*Power(t2,2)*
          (1 + Power(s,2) - 3*t2 + Power(t2,2) + s*(-2 + 3*t2)) + 
         Power(s1,5)*(-2 + (34 + 8*s - 20*s2 + s*s2 + 2*Power(s2,2))*t2 - 
            4*Power(t1,2)*t2 + 2*(-11 + s - 4*s2)*Power(t2,2) + 
            t1*(2 - 3*(6 + s - 2*s2)*t2 + 10*Power(t2,2))) + 
         Power(s1,2)*(-2 + 2*(-4 + s + 12*Power(s,2) + 4*s*s2)*t2 + 
            2*(8 + 16*s + 3*Power(s,2) - 14*s2 - 24*s*s2 + 
               4*Power(s2,2))*Power(t2,2) - 
            (-32 + 45*s + 20*Power(s,2) + 60*s2 + 31*s*s2 - 
               14*Power(s2,2))*Power(t2,3) - 
            (22 + 51*s + 3*Power(s,2) - 8*s2 - 9*s*s2 + 6*Power(s2,2))*
             Power(t2,4) + 2*(-1 + s - s2)*Power(t2,5) + 
            t1*(2 - 2*(-1 + 5*Power(s,2) + s*(5 + 4*s2))*t2 + 
               (38 - 8*Power(s,2) - 44*s2 + 6*s*(3 + 2*s2))*
                Power(t2,2) + 
               (24 + 3*Power(s,2) - 20*s2 + s*(-26 + 6*s2))*
                Power(t2,3) - 2*(32 + s - 6*s2)*Power(t2,4)) + 
            4*Power(t1,2)*t2*(Power(s,2)*(1 + t2) + t2*(4 + 7*t2) + 
               s*(3 + 6*t2 + Power(t2,2)))) - 
         Power(s1,3)*(Power(s,2)*t2*(14 - 5*t2 - 2*Power(t2,2)) + 
            t1*(4 + (18 + 25*s + Power(s,2) - 22*s2 - 2*s*s2)*t2 + 
               (94 + 18*s - 8*Power(s,2) - 76*s2 + 4*s*s2)*Power(t2,2) + 
               (-110 - 21*s + 18*s2)*Power(t2,3) - 14*Power(t2,4)) + 
            4*Power(t1,2)*t2*(2 + Power(s,2) + 14*t2 + Power(t2,2) + 
               s*(5 + 3*t2)) - 
            2*(3 + 13*Power(t2,2) + 11*Power(t2,3) - 5*Power(t2,4) + 
               s2*t2*(7 + 24*t2 - 21*Power(t2,2)) + 
               Power(s2,2)*t2*(-2 - 11*t2 + 7*Power(t2,2))) + 
            s*t2*(-5 - 5*t2 - 96*Power(t2,2) + 2*Power(t2,3) + 
               s2*(-16 - 57*t2 + 17*Power(t2,2)))) - 
         s1*t2*(Power(s,2)*(10 + 10*t2 + 4*Power(t1,2)*t2 - 
               20*Power(t2,2) - 3*Power(t2,3) + 
               t1*(-10 + 2*t2 + 3*Power(t2,2))) + 
            s*(4 + 31*Power(t2,2) - 51*Power(t2,3) + 2*Power(t2,4) + 
               4*Power(t1,2)*t2*(3 + t2) + 
               s2*t2*(8 - 32*t2 + Power(t2,2)) - 
               t1*(4 + (4 + 8*s2)*t2 + (7 - 14*s2)*Power(t2,2) + 
                  2*Power(t2,3))) + 
            2*(-2 + t2 + (8 - 7*s2 + 2*Power(s2,2))*Power(t2,2) + 
               4*Power(t1,2)*Power(t2,2) + 
               (14 - 12*s2 + Power(s2,2))*Power(t2,3) - 
               (10 + s2)*Power(t2,4) + 
               t1*(2 - 4*t2 - 11*(-1 + s2)*Power(t2,2) + 
                  (-8 + 6*s2)*Power(t2,3))))))/
     (Power(-1 + s1,2)*s1*Power(s1 - t2,2)*(-s + s1 - t2)*
       (-1 + s1 + t1 - t2)*(-1 + t2)*t2) + 
    (8*(Power(s1,7)*(-Power(s2,2) + 2*s2*t1 + Power(t1,2)) - 
         (-1 + s)*Power(t1,4)*(-2 + 2*s + 3*t2) - 
         t1*(-2 + (-14 + 7*Power(s,2) + s*(-4 + s2) + 3*s2)*t2 + 
            (22 + Power(s,2) + 3*s2 + 2*Power(s2,2) - 5*s*(1 + 3*s2))*
             Power(t2,2) + (44 - 47*s + 3*Power(s,2) + 26*s2 + 
               Power(s2,2))*Power(t2,3) + 2*(9 + 3*s + s2)*Power(t2,4)) + 
         Power(t1,3)*(6 + (-14 + 3*s2)*t2 - 3*(2 + s2)*Power(t2,2) + 
            Power(s,2)*(2 + t2) + s*(-8 - 3*(-2 + s2)*t2 + 4*Power(t2,2))\
) + t2*(-11 - t2 + 7*Power(t2,2) + 13*Power(t2,3) - 
            Power(s,2)*t2*(1 + 5*t2) + 
            Power(s2,2)*t2*(3 + t2 + 2*Power(t2,2)) + 
            s2*(3 - 7*t2 + 19*Power(t2,2) + 15*Power(t2,3) + 
               2*Power(t2,4)) + 
            s*(-7 - (13 + 6*s2)*t2 - (23 + 12*s2)*Power(t2,2) + 
               (-31 + 2*s2)*Power(t2,3) + 2*Power(t2,4))) + 
         Power(t1,2)*(-6 + (8 - 3*s2)*t2 + 
            (29 + 13*s2 - Power(s2,2))*Power(t2,2) + 
            3*(7 + s2)*Power(t2,3) + 4*Power(s,2)*t2*(1 + t2) + 
            s*(4 - 26*Power(t2,2) + 3*Power(t2,3) + s2*t2*(4 + t2))) - 
         Power(s1,6)*(t1*(2 + s*(-2 + t1) - 3*t1 - 2*Power(t1,2) + 
               2*t2 + 3*t1*t2) + Power(s2,2)*(3*t1 - 2*(1 + t2)) + 
            s2*(-5*Power(t1,2) + t1*(11 + s + 5*t2) + 2*(-1 + t2 + s*t2))\
) + Power(s1,5)*(-1 + Power(t1,4) + Power(t1,3)*(11 - 2*s - 3*t2) + 
            2*t2 + Power(t2,2) - Power(s,2)*Power(t2,2) + 
            Power(t1,2)*(-1 - 11*t2 + Power(t2,2) + 4*s*(1 + t2)) + 
            Power(s2,2)*(4 - 3*Power(t1,2) - 3*t2 + 5*t1*(1 + t2)) + 
            t1*(11 - t2 + Power(s,2)*t2 + 6*Power(t2,2) - 
               s*(5 + 5*t2 + 3*Power(t2,2))) + 
            s2*(-4 + 4*Power(t1,3) + 7*(1 + s)*t2 + 
               (5 + 2*s)*Power(t2,2) - Power(t1,2)*(23 + 3*s + 11*t2) + 
               t1*(s*(2 - 5*t2) + 3*(2 + 9*t2 + Power(t2,2))))) - 
         Power(s1,4)*(-2 + (-13 + s)*Power(t1,4) + 9*t2 + s*t2 + 
            2*Power(t2,2) - 4*s*Power(t2,2) - 3*Power(s,2)*Power(t2,2) + 
            3*Power(t2,3) - 3*s*Power(t2,3) + 
            Power(t1,3)*(15 + s + 21*t2 - 4*s*t2 + Power(t2,2)) + 
            Power(s2,2)*(2 + Power(t1,3) + 17*t2 - Power(t2,2) + 
               2*Power(t2,3) - Power(t1,2)*(3 + 4*t2) + 
               t1*(4 + t2 + Power(t2,2))) - 
            Power(t1,2)*(Power(s,2)*t2 + 
               t2*(-9 + 18*t2 + Power(t2,2)) - 
               s*(27 + 15*t2 + 5*Power(t2,2))) + 
            t1*(3 + 35*t2 - 13*Power(t2,2) + 2*Power(t2,3) + 
               Power(s,2)*t2*(3 + t2) - 
               s*(-6 + 16*t2 + 15*Power(t2,2) + 2*Power(t2,3))) - 
            s2*(-8 + Power(t1,4) + 2*(3 + s)*t2 - 
               (32 + 5*s)*Power(t2,2) + (-3 + 2*s)*Power(t2,3) - 
               Power(t1,3)*(12 + 3*s + 8*t2) + 
               Power(t1,2)*(39 + s*(7 - 3*t2) + 39*t2 + 5*Power(t2,2)) + 
               t1*(21 + 29*t2 - 21*Power(t2,2) + Power(t2,3) + 
                  s*(8 + 15*t2 + 3*Power(t2,2))))) + 
         s1*(-3 + 3*(-1 + s)*Power(t1,5) + 
            (3 + 2*s2 - 3*Power(s2,2) + s*(-2 + 13*s2))*t2 + 
            (-40 + 7*Power(s,2) + s*(44 - 12*s2) - 17*s2 - 
               7*Power(s2,2))*Power(t2,2) + 
            (-2 + 13*Power(s,2) + s*(52 - 8*s2) - 84*s2 - 
               3*Power(s2,2))*Power(t2,3) - 
            (3 + 5*Power(s,2) + 14*s2 + 4*Power(s2,2) - s*(7 + 9*s2))*
             Power(t2,4) + (-7 - 3*s + 3*s2)*Power(t2,5) + 
            Power(t1,4)*(13 + 2*Power(s,2) + s2 + 8*t2 + 3*s2*t2 - 
               s*(8 + s2 + t2)) - 
            Power(t1,3)*(2 + 37*t2 + Power(s2,2)*t2 + 38*Power(t2,2) + 
               2*Power(s,2)*(2 + t2) + s2*(9 + 9*t2 + 2*Power(t2,2)) + 
               s*(2 + s2*(-4 + t2) - 19*t2 + 9*Power(t2,2))) + 
            t1*(21 + 9*t2 + 60*Power(t2,2) - 6*Power(t2,3) + 
               14*Power(t2,4) + 
               Power(s2,2)*t2*(-3 + 7*t2 + Power(t2,2)) + 
               Power(s,2)*t2*(13 - 6*t2 + 12*Power(t2,2)) + 
               s2*(-7 + 21*t2 + 4*Power(t2,2) + Power(t2,3) - 
                  7*Power(t2,4)) + 
               s*(1 + (37 - 19*s2)*t2 + (-2 + 21*s2)*Power(t2,2) + 
                  (40 - 18*s2)*Power(t2,3) + 4*Power(t2,4))) + 
            Power(t1,2)*(-26 + 17*t2 + 7*Power(s2,2)*t2 + 
               66*Power(t2,2) + 26*Power(t2,3) + 
               Power(s,2)*(4 - 3*t2 - 7*Power(t2,2)) + 
               s2*(15 - 17*t2 + 27*Power(t2,2) + 3*Power(t2,3)) + 
               s*(6 + 7*t2 - 58*Power(t2,2) + 6*Power(t2,3) + 
                  s2*(-3 - 13*t2 + 11*Power(t2,2))))) + 
         Power(s1,3)*(4 + 5*Power(t1,5) - 3*t2 + 2*s*t2 + 
            36*Power(t2,2) - 10*s*Power(t2,2) + 
            2*Power(s,2)*Power(t2,2) - 4*Power(t2,3) - 
            21*s*Power(t2,3) - Power(s,2)*Power(t2,3) + Power(t2,4) - 
            2*s*Power(t2,4) + Power(s,2)*Power(t2,4) - 
            Power(t1,4)*(11 + 6*s + 15*t2) + 
            Power(t1,3)*(31 + 10*t2 + 16*Power(t2,2) + 
               s*(-9 - 10*t2 + Power(t2,2))) + 
            Power(t1,2)*(13 + 10*t2 + 16*Power(t2,2) - 4*Power(t2,3) + 
               Power(s,2)*(4 - t2 + Power(t2,2)) + 
               s*(40 + 34*t2 + 27*Power(t2,2) - 2*Power(t2,3))) + 
            Power(s2,2)*(-3 + 8*t2 + Power(t1,3)*t2 + 16*Power(t2,2) - 
               Power(t2,3) + Power(t2,4) - 
               Power(t1,2)*(-2 + t2 + Power(t2,2)) - 
               t1*(-1 - 2*t2 + 9*Power(t2,2) + Power(t2,3))) + 
            t1*(2*Power(s,2)*t2*(1 + t2 - Power(t2,2)) + 
               s*(4 + 68*t2 - 2*Power(t2,2) - 9*Power(t2,3) + 
                  Power(t2,4)) - 
               2*(13 - 6*t2 - 14*Power(t2,2) + 14*Power(t2,3) + 
                  Power(t2,4))) - 
            s2*(-4 + (-3 + 20*s)*t2 + 2*(9 + 10*s)*Power(t2,2) + 
               (-36 + s)*Power(t2,3) + (1 + 2*s)*Power(t2,4) + 
               Power(t1,4)*(s + 2*t2) - 
               Power(t1,3)*(15 + 7*s + 15*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(18 + s*(8 - 10*t2) + 16*t2 + 
                  13*Power(t2,2) - Power(t2,3)) + 
               t1*(-3 + 136*t2 + 69*Power(t2,2) - 9*Power(t2,3) + 
                  Power(t2,4) + 
                  s*(2 + 36*t2 - 4*Power(t2,2) - 3*Power(t2,3))))) + 
         Power(s1,2)*(-2 - (2 + 3*s)*Power(t1,5) + 14*t2 + 8*s*t2 - 
            6*Power(t2,2) - 33*s*Power(t2,2) - 
            10*Power(s,2)*Power(t2,2) - 32*Power(t2,3) + 
            17*s*Power(t2,3) - 3*Power(s,2)*Power(t2,3) + 
            13*Power(t2,4) + 16*s*Power(t2,4) + Power(t2,5) - 
            s*Power(t2,5) + Power(t1,4)*(10 + s + 12*t2 + 6*s*t2) + 
            Power(t1,3)*(1 - 11*t2 + 7*Power(t2,2) + 
               Power(s,2)*(-2 + 5*t2) + s*(2 + 21*t2)) + 
            Power(s2,2)*(Power(t1,3) - 
               2*Power(t1,2)*(3 + t2 + Power(t2,2)) + 
               t2*(9 - 5*t2 + Power(t2,2) + Power(t2,3)) + 
               t1*(5 - 7*t2 + Power(t2,2) + 5*Power(t2,3))) - 
            Power(t1,2)*(8 + 102*t2 + 30*Power(t2,2) + 30*Power(t2,3) + 
               Power(s,2)*(8 - 11*t2 + 10*Power(t2,2)) + 
               s*(34 - 38*t2 - 2*Power(t2,2) + 7*Power(t2,3))) + 
            t1*(1 + 39*t2 + 5*Power(t2,2) + 8*Power(t2,3) + 
               12*Power(t2,4) + 
               Power(s,2)*t2*(-6 - 6*t2 + 5*Power(t2,2)) + 
               s*(4 - 104*t2 - 89*Power(t2,2) - 40*Power(t2,3) + 
                  5*Power(t2,4))) + 
            s2*(6 + Power(t1,4)*(-2 + 2*s - t2) - 11*t2 + 
               (73 + 49*s)*Power(t2,2) + 3*(10 + s)*Power(t2,3) - 
               (16 + s)*Power(t2,4) + Power(t2,5) + 
               Power(t1,3)*(8 - (5 + 4*s)*t2 + Power(t2,2)) + 
               Power(t1,2)*(10 - 30*t2 - 32*Power(t2,2) + 
                  3*Power(t2,3) + 3*s*(5 - 10*t2 + 4*Power(t2,2))) + 
               t1*(-22 + 35*t2 + 120*Power(t2,2) + 35*Power(t2,3) - 
                  4*Power(t2,4) - 
                  s*(7 - 30*t2 + 3*Power(t2,2) + 9*Power(t2,3))))))*
       B1(1 - s1 - t1 + t2,s1,t2))/
     ((-1 + s1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*
       (-1 + t2)) - (16*(Power(s1,10)*t1 - 
         (-1 + t1 - t2)*Power(t2,3)*
          (1 + Power(s,2) - 3*t2 + Power(t2,2) + s*(-2 + 3*t2)) - 
         Power(s1,9)*(Power(s2,2) - 5*s2*t1 - Power(t1,2) + t2 + 
            t1*(3 + s + 3*t2)) + 
         Power(s1,8)*(-3*Power(t1,2) + t2*(3 + s + 3*t2) + 
            Power(s2,2)*(4 - t1 + 3*t2) + 
            t1*(1 + 6*t2 + 3*Power(t2,2) + 3*s*(1 + t2)) + 
            s2*(2 + 5*Power(t1,2) - 5*t2 - 2*s*t2 - 2*t1*(9 + s + 10*t2))\
) + s1*Power(t2,2)*(-2 + 2*t2 + 2*s2*t2 + 23*Power(t2,2) + 
            2*s2*Power(t2,2) + 6*Power(t2,3) + 3*s2*Power(t2,3) - 
            8*Power(t2,4) + 3*s2*Power(t2,4) + 
            Power(t1,2)*(1 + 3*t2 + 2*Power(t2,2)) - 
            t1*(-1 + (9 + 2*s2)*t2 + 20*Power(t2,2) + 
               3*(-2 + s2)*Power(t2,3)) + 
            Power(s,2)*(-4 - 9*t2 - Power(t2,2) + 3*Power(t2,3) + 
               Power(t1,2)*(1 + t2) + t1*(3 + 2*t2 - 4*Power(t2,2))) - 
            s*(-6 + 2*(-3 + s2)*t2 + 3*(10 + s2)*Power(t2,2) + 
               (26 + s2)*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(2 + 4*t2 + 3*Power(t2,2)) - 
               t1*(-4 + 2*(4 + s2)*t2 + (32 + s2)*Power(t2,2) + 
                  4*Power(t2,3)))) - 
         Power(s1,2)*t2*(-1 - 2*t2 + 4*s2*t2 + 37*Power(t2,2) + 
            14*s2*Power(t2,2) + 73*Power(t2,3) + 9*s2*Power(t2,3) + 
            4*Power(s2,2)*Power(t2,3) - 11*Power(t2,4) + 
            13*s2*Power(t2,4) + 2*Power(s2,2)*Power(t2,4) - 
            22*Power(t2,5) + s2*Power(t2,5) + Power(t1,3)*t2*(4 + t2) - 
            Power(t1,2)*(-2 + (-5 + 2*s2)*t2 - 
               4*(-1 + s2)*Power(t2,2) + (1 + 3*s2)*Power(t2,3)) + 
            t1*(-1 - (13 + 2*s2)*t2 - 2*(22 + 5*s2)*Power(t2,2) - 
               2*(11 + 5*s2 + Power(s2,2))*Power(t2,3) + 
               2*(11 + s2)*Power(t2,4)) + 
            Power(s,2)*(3 - 10*t2 - Power(t1,3)*t2 - 19*Power(t2,2) + 
               4*Power(t2,3) + 5*Power(t2,4) + 
               Power(t1,2)*(4 + 7*t2 + Power(t2,2)) - 
               t1*(7 + 3*Power(t2,2) + 5*Power(t2,3))) + 
            s*(4 + 19*t2 - (12 + 13*s2)*Power(t2,2) - 
               (103 + 13*s2)*Power(t2,3) - (51 + 5*s2)*Power(t2,4) + 
               Power(t2,5) - 3*Power(t1,3)*t2*(1 + t2) + 
               Power(t1,2)*(-6 + (-3 + 2*s2)*t2 - 
                  3*(-3 + s2)*Power(t2,2) + Power(t2,3)) + 
               t1*(2 + (5 - 2*s2)*t2 + (34 + 8*s2)*Power(t2,2) + 
                  (53 + 8*s2)*Power(t2,3) + Power(t2,4)))) + 
         Power(s1,7)*(-1 + (3 + s)*Power(t1,3) - t2 - s*t2 - 
            7*Power(t2,2) - 3*s*Power(t2,2) - Power(s,2)*Power(t2,2) - 
            3*Power(t2,3) + Power(s2,2)*
             (-3 + 7*t1 - 13*t2 - 2*Power(t2,2)) + 
            t1*(13 + 13*t2 - 13*Power(t2,2) - Power(t2,3) + 
               Power(s,2)*(5 + t2) + s*(4 - 15*t2 - 7*Power(t2,2))) - 
            Power(t1,2)*(Power(s,2) + s*(-7 + 3*t2) + 
               3*(-2 + t2 + Power(t2,2))) + 
            s2*(-(Power(t1,2)*(21 + 2*s + 11*t2)) + 
               4*(-2 + 3*(1 + s)*t2 + (5 + s)*Power(t2,2)) + 
               t1*(13 + 67*t2 + 28*Power(t2,2) + s*(-3 + 6*t2)))) + 
         Power(s1,6)*(4 - 10*t2 - 14*s*t2 - 5*Power(s,2)*t2 - 
            10*Power(t2,2) + 11*s*Power(t2,2) + 
            5*Power(s,2)*Power(t2,2) + 13*Power(t2,3) + 
            7*s*Power(t2,3) + Power(s,2)*Power(t2,3) + Power(t2,4) - 
            Power(t1,3)*(4 + 2*s + Power(s,2) + 5*t2) - 
            2*Power(s2,2)*(t1*(1 + 10*t2 - 3*Power(t2,2)) + 
               t2*(-1 - 8*t2 + Power(t2,2))) + 
            Power(t1,2)*(19 - 12*t2 + 7*Power(t2,2) + 2*Power(t2,3) + 
               4*Power(s,2)*(1 + t2) - s*(8 + 19*t2 + 2*Power(t2,2))) + 
            t1*(-11 - 100*t2 + 9*Power(t2,2) + 27*Power(t2,3) + 
               Power(s,2)*(-14 - 12*t2 + Power(t2,2)) + 
               s*(-9 - 13*t2 + 66*Power(t2,2) + 7*Power(t2,3))) + 
            s2*(6 + (15 - 11*s)*t2 - 2*(34 + 15*s)*Power(t2,2) - 
               28*Power(t2,3) + 
               Power(t1,2)*(10 + 56*t2 + 5*Power(t2,2) + s*(3 + 2*t2)) - 
               2*t1*(6 + 5*t2 + 50*Power(t2,2) + 7*Power(t2,3) + 
                  s*(-1 - 11*t2 + 5*Power(t2,2))))) + 
         Power(s1,5)*(-3 - 17*Power(t1,2) + 13*Power(t1,3) - 3*t2 + 
            61*t1*t2 - 74*Power(t1,2)*t2 + 92*Power(t2,2) + 
            162*t1*Power(t2,2) + 35*Power(t1,2)*Power(t2,2) + 
            Power(t1,3)*Power(t2,2) - 6*Power(t2,3) - 
            75*t1*Power(t2,3) + 7*Power(t1,2)*Power(t2,3) - 
            24*Power(t2,4) - 24*t1*Power(t2,4) + 
            Power(s2,2)*t2*(4 + 14*t2 - 10*Power(t2,2) + 
               3*Power(t2,3) + t1*(4 + 18*t2 - 8*Power(t2,2))) + 
            Power(s,2)*(2*Power(t1,3) + 
               2*Power(t1,2)*(-3 - 5*t2 + 2*Power(t2,2)) + 
               t2*(14 + t2 - 9*Power(t2,2) + Power(t2,3)) - 
               t1*(-16 - 15*t2 + Power(t2,2) + 3*Power(t2,3))) + 
            s2*(Power(t1,2)*(-4 - 21*t2 - 50*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t2*(-6 - t2 + 115*Power(t2,2) + 14*Power(t2,3)) + 
               t1*(6 + 11*t2 - 30*Power(t2,2) + 74*Power(t2,3) - 
                  Power(t2,4))) + 
            s*(-(Power(t1,3)*(-4 + 9*t2 + Power(t2,2))) + 
               Power(t1,2)*(-2 + 31*t2 + 59*Power(t2,2) + 
                  3*Power(t2,3) + s2*(-7 + 5*t2 - 2*Power(t2,2))) + 
               t2*(23 + 30*t2 - 63*Power(t2,2) - 7*Power(t2,3) + 
                  s2*(4 + 12*t2 + 22*Power(t2,2) - 4*Power(t2,3))) + 
               t1*(11 + 29*t2 - 69*Power(t2,2) - 80*Power(t2,3) + 
                  s2*(5 - 26*t2 - 24*Power(t2,2) + 10*Power(t2,3))))) + 
         Power(s1,4)*(Power(t1,3)*
             (-4 - 27*t2 + 12*Power(t2,2) + Power(t2,3) + 
               Power(s,2)*(-1 + Power(t2,2)) + 
               s*(-3 + 9*t2 + 14*Power(t2,2))) + 
            Power(t1,2)*(1 + 42*t2 + 92*Power(t2,2) - 58*Power(t2,3) - 
               8*Power(t2,4) - 
               Power(s,2)*(-6 + 2*t2 + 7*Power(t2,2) + Power(t2,3)) + 
               s2*(2 + 4*t2 + 15*Power(t2,2) + 16*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s*(11 - 17*t2 - 77*Power(t2,2) - 49*Power(t2,3) + 
                  2*Power(t2,4) + 
                  s2*(2 + 5*t2 - 7*Power(t2,2) + 2*Power(t2,3)))) + 
            t1*(4 + t2 - 55*Power(t2,2) - 98*Power(t2,3) + 
               66*Power(t2,4) + 7*Power(t2,5) + 
               Power(s2,2)*Power(t2,3)*(-4 + 3*t2) + 
               Power(s,2)*(-10 + 5*t2 + 17*Power(t2,2) - 
                  2*Power(t2,3) + Power(t2,4)) + 
               2*s2*(-1 - t2 + 9*Power(t2,2) + 18*Power(t2,3) - 
                  13*Power(t2,4) + Power(t2,5)) + 
               s*(-12 - 22*t2 + 30*Power(t2,2) + 74*Power(t2,3) + 
                  27*Power(t2,4) - 2*Power(t2,5) + 
                  2*s2*(-1 + 4*t2 + 7*Power(t2,2) + Power(t2,3) - 
                     2*Power(t2,4)))) - 
            t2*(-9 + 64*t2 + 155*Power(t2,2) - 68*Power(t2,3) - 
               22*Power(t2,4) + 
               Power(s2,2)*t2*
                (12 + 24*t2 - 4*Power(t2,2) + Power(t2,3)) + 
               s2*(10 + 9*t2 + 18*Power(t2,2) + 85*Power(t2,3) - 
                  Power(t2,4)) + 
               Power(s,2)*(16 + 9*t2 - 13*Power(t2,2) - Power(t2,3) + 
                  Power(t2,4)) + 
               s*(17 + 53*t2 - 75*Power(t2,2) - 86*Power(t2,3) + 
                  s2*(5 - 15*t2 - 10*Power(t2,2) + 2*Power(t2,3) - 
                     2*Power(t2,4))))) - 
         Power(s1,3)*(Power(t1,3)*t2*
             (-8 + (-15 + 16*s + 2*Power(s,2))*t2 + 
               (8 + 3*s)*Power(t2,2)) + 
            Power(t1,2)*(-1 + (-1 + 4*s2)*t2 + (31 - 4*s2)*Power(t2,2) + 
               (38 + 7*s2)*Power(t2,3) + (-29 + s2)*Power(t2,4) - 
               Power(s,2)*(-3 + 12*t2 + 9*Power(t2,2) + Power(t2,3)) + 
               s*(4 + 10*t2 + (-31 + s2)*Power(t2,2) + 
                  (-55 + s2)*Power(t2,3) - 2*Power(t2,4))) + 
            t1*(1 + (11 - 2*s2)*t2 + 2*(12 + 7*s2)*Power(t2,2) + 
               (23 + 24*s2 + 4*Power(s2,2))*Power(t2,3) + 
               (-45 + 7*s2 + Power(s2,2))*Power(t2,4) + 
               (14 - 3*s2)*Power(t2,5) + 
               Power(s,2)*(-3 + 16*t2 + 20*Power(t2,2) - Power(t2,3) + 
                  2*Power(t2,4)) + 
               s*(-4 + 2*(-6 + s2)*t2 + (-13 + 6*s2)*Power(t2,2) + 
                  (1 - 18*s2)*Power(t2,3) - (5 + 3*s2)*Power(t2,4) + 
                  Power(t2,5))) - 
            t2*(-2 + 10*t2 + 129*Power(t2,2) + 71*Power(t2,3) - 
               73*Power(t2,4) - 7*Power(t2,5) + 
               Power(s2,2)*Power(t2,2)*(12 + 13*t2 - Power(t2,2)) + 
               Power(s,2)*(10 - 2*t2 - 16*Power(t2,2) + 2*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s2*(2 + 22*t2 + 15*Power(t2,2) + 22*Power(t2,3) + 
                  25*Power(t2,4) - 2*Power(t2,5)) - 
               s*(-12 - 32*t2 + 47*Power(t2,2) + 141*Power(t2,3) + 
                  32*Power(t2,4) - 2*Power(t2,5) + 
                  s2*(-2 + 5*t2 + 31*Power(t2,2) + 16*Power(t2,3) + 
                     2*Power(t2,4))))))*R1q(s1))/
     (Power(-1 + s1,3)*s1*Power(s1 - t2,3)*(-s + s1 - t2)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*(-1 + t2)) - 
    (16*(-(Power(s1,8)*t1*(-1 + (2 - s2 + t1)*t2 + Power(t2,2))) + 
         Power(s1,7)*(-3*Power(t1,3)*t2 + 
            Power(t1,2)*(3 + (-5 + s + 3*s2)*t2 + Power(t2,2)) + 
            t2*(-1 + (2 - s2 + Power(s2,2))*t2 + Power(t2,2)) + 
            t1*t2*(-4 - s + 8*t2 - 8*s2*t2 + 3*Power(t2,2))) + 
         Power(s1,5)*(-(Power(t1,5)*t2) + 
            Power(t1,4)*(1 + (1 + 3*s + s2)*t2 + 9*Power(t2,2)) - 
            Power(t1,3)*(-2 + t2 + 3*s*t2 + 
               (2 + 15*s + 16*s2)*Power(t2,2) + 14*Power(t2,3)) + 
            t2*(2 + (9 + 3*s + 5*s2)*t2 + 
               (-26 + 3*s2 + 8*Power(s2,2) - s*(13 + 10*s2))*
                Power(t2,2) + 
               (Power(s,2) + 2*(-11 + s2)*s2 - s*(3 + 4*s2))*
                Power(t2,3) + 3*Power(t2,4)) + 
            Power(t1,2)*(-5 + (-19 - 7*s + s2)*t2 + 
               (-4 + 11*s + Power(s,2) + 15*s2 + 6*s*s2 + Power(s2,2))*
                Power(t2,2) + (19 + 25*s + 41*s2)*Power(t2,3) + 
               7*Power(t2,4)) + 
            t1*(1 + 2*(5 + 3*s - s2)*t2 + 
               (16 - 7*s - 9*Power(s,2) + 7*s2 + 6*s*s2 - 
                  9*Power(s2,2))*Power(t2,2) - 
               (93 + 27*s + 3*Power(s,2) - 7*s2 + 2*s*s2 + 
                  3*Power(s2,2))*Power(t2,3) - 
               (9 + 5*s + 26*s2)*Power(t2,4) + Power(t2,5))) + 
         Power(s1,6)*(-3*Power(t1,4)*t2 + 
            Power(t1,3)*(3 + 3*(-1 + s + s2)*t2 + 8*Power(t2,2)) - 
            Power(t1,2)*(-1 + (8 + 3*s)*t2 + 
               5*(-4 + s + 4*s2)*Power(t2,2) + 2*Power(t2,3)) + 
            t1*(-2 + (-11 - 3*s + s2)*t2 + 
               (31 + 2*Power(s2,2) + 2*s*(5 + s2))*Power(t2,2) + 
               (-1 + 3*s + 22*s2)*Power(t2,3) - 3*Power(t2,4)) + 
            Power(t2,2)*(4 + s - 7*t2 + 2*s*s2*t2 - 3*Power(t2,2) - 
               3*Power(s2,2)*(1 + t2) + s2*(-2 + 8*t2))) + 
         Power(s1,4)*(Power(t1,5)*t2*(1 + s + 3*t2) - 
            Power(t1,4)*(-1 + (-7 + s)*t2 + 
               (21 + 15*s + 4*s2)*Power(t2,2) + 12*Power(t2,3)) + 
            Power(t1,3)*(-4 - 2*(6 + 2*s + s2)*t2 + 
               (-39 + 4*Power(s,2) + 18*s2 + 2*s*(7 + 3*s2))*
                Power(t2,2) + (75 + 43*s + 23*s2)*Power(t2,3) + 
               18*Power(t2,4)) + 
            t2*(-1 - 2*(7 + 3*s - s2)*t2 + 
               (-11 + 18*s + 9*Power(s,2) - 27*s2 + 14*Power(s2,2))*
                Power(t2,2) + 
               (76 - 2*Power(s,2) + 9*s2 - 8*Power(s2,2) + 
                  4*s*(9 + 5*s2))*Power(t2,3) + 
               (11 + 5*s - Power(s,2) + 26*s2 + 2*Power(s2,2))*
                Power(t2,4) - Power(t2,5)) - 
            Power(t1,2)*(-2 - (9 + 10*s)*t2 + 
               (-60 - 22*s + 14*Power(s,2) + 19*s2 + 6*Power(s2,2))*
                Power(t2,2) + 
               2*(8*Power(s,2) + 15*(-1 + s2) + s*(23 + 3*s2))*
                Power(t2,3) + (91 + 28*s + 33*s2)*Power(t2,4) + 
               7*Power(t2,5)) + 
            t1*t2*(11 + (-4 - s2 + 8*Power(s2,2))*t2 + 
               (78 - 65*s2 + 24*Power(s2,2))*Power(t2,2) - 
               2*(7 + 9*s2 + 2*Power(s2,2))*Power(t2,3) + 
               11*(-1 + s2)*Power(t2,4) + 
               Power(s,2)*t2*(15 + 25*t2 + 6*Power(t2,2)) + 
               s*(-2 + (11 - 12*s2)*t2 + (13 - 48*s2)*Power(t2,2) - 
                  36*Power(t2,3) + 3*Power(t2,4)))) - 
         Power(s1,3)*(Power(t1,5)*t2*
             (-4 + (7 + 5*s)*t2 + 3*Power(t2,2)) - 
            Power(t1,4)*(-1 - (3 + 2*s2)*t2 + 
               (5*Power(s,2) + 4*(-7 + s2) + s*(19 + 2*s2))*
                Power(t2,2) + (63 + 26*s + 4*s2)*Power(t2,3) + 
               9*Power(t2,4)) + 
            Power(t1,3)*(-1 - (5 + 6*s + 2*s2)*t2 + 
               (-5 + 6*Power(s,2) + 6*s2 + 2*s*(-3 + 5*s2))*
                Power(t2,2) + 
               (-102 + 24*Power(s,2) + 43*s2 + s*(53 + 2*s2))*
                Power(t2,3) + (153 + 35*s + 7*s2)*Power(t2,4) + 
               11*Power(t2,5)) + 
            t1*t2*(6 + (21 + 16*s + 6*Power(s,2) + 8*s2)*t2 + 
               (94 + s*(43 - 58*s2) - 57*s2 + 20*Power(s2,2))*
                Power(t2,2) + 
               (138 - 121*s2 + 22*Power(s2,2) - 5*s*(39 + 14*s2))*
                Power(t2,3) + 
               (-275 + 4*Power(s,2) - 14*s2 - 10*Power(s2,2) + 
                  s*(-93 + 4*s2))*Power(t2,4) + 
               (-25 + 3*s - 2*s2)*Power(t2,5)) - 
            Power(t1,2)*t2*(4 + (7 + 18*s2)*t2 + 
               (2 + 8*s2 + 20*Power(s2,2))*Power(t2,2) + 
               (-174 + 23*s2 - 6*Power(s2,2))*Power(t2,3) + 
               6*(12 + s2)*Power(t2,4) + 2*Power(t2,5) + 
               Power(s,2)*t2*(20 + 33*t2 + 13*Power(t2,2)) + 
               s*(-4 + 8*(-3 + s2)*t2 - 2*(17 + 24*s2)*Power(t2,2) - 
                  43*Power(t2,3) + 8*Power(t2,4))) + 
            Power(t2,2)*(6 + 2*(-15 + 5*s2 + 4*Power(s2,2))*t2 + 
               (69 - 69*s2 + 44*Power(s2,2))*Power(t2,2) + 
               (13 + 27*s2 - 8*Power(s2,2))*Power(t2,3) + 
               (-4 + 11*s2 + 3*Power(s2,2))*Power(t2,4) + 
               Power(s,2)*t2*(15 + 19*t2 + Power(t2,3)) + 
               s*(-2 - 2*(-5 + 6*s2)*t2 + (45 - 38*s2)*Power(t2,2) + 
                  (-11 + 8*s2)*Power(t2,3) + (3 - 4*s2)*Power(t2,4)))) + 
         Power(t2,3)*(Power(t1,4)*t2*(-6 + 5*t2 + s*(10 + t2)) + 
            Power(t1,3)*(1 + (5 + 2*s2)*t2 + 11*Power(t2,2) + 
               (-25 + s2)*Power(t2,3) + 
               Power(s,2)*(-6 + t2 + 3*Power(t2,2)) + 
               s*(2 - 2*(5 + 2*s2)*t2 - 2*(17 + s2)*Power(t2,2) + 
                  Power(t2,3))) + 
            Power(t1,2)*(-1 + 7*Power(t2,2) - 14*Power(t2,3) - 
               2*Power(s2,2)*Power(t2,3) + 24*Power(t2,4) + 
               Power(s,2)*(6 + 7*t2 - 2*Power(t2,2) - 3*Power(t2,3)) + 
               s2*t2*(-2 + 2*t2 + 7*Power(t2,2) - 3*Power(t2,3)) + 
               s*(-2 + (-2 + 4*s2)*t2 + (-13 + 2*s2)*Power(t2,2) + 
                  (7 + 4*s2)*Power(t2,3))) - 
            2*t1*t2*(2 + 6*(1 + s2)*t2 + 
               (19 - s2 + 2*Power(s2,2))*Power(t2,2) + 
               (-39 + 3*s2 - 2*Power(s2,2))*Power(t2,3) - 
               (-2 + s2)*Power(t2,4) - 
               Power(s,2)*(12 - 2*t2 - 9*Power(t2,2) + Power(t2,3)) + 
               s*(4 - 2*(11 + 4*s2)*t2 - (70 + 9*s2)*Power(t2,2) + 
                  (-4 + 3*s2)*Power(t2,3) + Power(t2,4))) - 
            2*t2*(-2 - 25*Power(t2,2) + 10*Power(t2,3) + 
               29*Power(t2,4) + 
               Power(s2,2)*Power(t2,2)*(-4 - t2 + Power(t2,2)) + 
               s2*t2*(-4 + 7*t2 + 3*Power(t2,2)) + 
               Power(s,2)*(12 + 14*t2 - 8*Power(t2,2) - 7*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(-4 + (-4 + 8*s2)*t2 + (53 + 12*s2)*Power(t2,2) + 
                  (43 + 6*s2)*Power(t2,3) - 2*(1 + s2)*Power(t2,4)))) - 
         s1*Power(t2,2)*(Power(t1,5)*t2*
             (-4 + 2*Power(s,2) + 5*t2 + s*(6 + t2)) + 
            Power(t1,3)*(1 + (13 + 2*s2)*t2 + 
               5*(7 + 2*s2)*Power(t2,2) + (-83 + 9*s2)*Power(t2,3) - 
               3*(-7 + s2)*Power(t2,4) - 
               2*s*t2*(8 + (34 + 5*s2)*t2 + (5 + s2)*Power(t2,2)) + 
               2*Power(s,2)*(3 + 4*t2 - 5*Power(t2,2) + Power(t2,3))) + 
            Power(t1,4)*(1 + (-11 + 2*s2)*t2 + 
               (21 - 4*s2)*Power(t2,2) + (-24 + s2)*Power(t2,3) - 
               Power(s,2)*(6 + t2 + 2*Power(t2,2)) + 
               s*(2 - 4*(-2 + s2)*t2 + (-23 + 2*s2)*Power(t2,2) + 
                  Power(t2,3))) - 
            t1*t2*(2 + 4*(7 + 4*s2)*t2 + 
               (-4 + 45*s2 + 4*Power(s2,2))*Power(t2,2) + 
               (-134 + 28*s2 - 9*Power(s2,2))*Power(t2,3) + 
               (86 - 5*s2 + Power(s2,2))*Power(t2,4) + 
               Power(s,2)*(36 + 46*t2 - 2*Power(t2,2) - 
                  5*Power(t2,3) + Power(t2,4)) - 
               s*(4 - 8*(-10 + s2)*t2 + (57 + 50*s2)*Power(t2,2) - 
                  2*(13 + 6*s2)*Power(t2,3) + (1 + 2*s2)*Power(t2,4))) + 
            2*t2*(4 + (4 + 8*s2)*t2 + 
               (46 - 11*s2 + 12*Power(s2,2))*Power(t2,2) + 
               (12 - 21*s2 + 10*Power(s2,2))*Power(t2,3) + 
               (-94 + 4*s2 - 4*Power(s2,2))*Power(t2,4) - 
               (4 + s2)*Power(t2,5) + 
               Power(s,2)*t2*
                (-14 - 6*t2 + 11*Power(t2,2) + Power(t2,3)) + 
               s*(4 - 8*(-1 + s2)*t2 - (47 + 40*s2)*Power(t2,2) - 
                  (118 + 29*s2)*Power(t2,3) + 3*(-5 + s2)*Power(t2,4) + 
                  Power(t2,5))) - 
            Power(t1,2)*(2 + 4*(2 + s2)*t2 + 2*(-2 + 7*s2)*Power(t2,2) + 
               (97 - 41*s2 + 12*Power(s2,2))*Power(t2,3) + 
               (-191 + 14*s2 - 3*Power(s2,2))*Power(t2,4) - 
               2*(-1 + s2)*Power(t2,5) + 
               Power(s,2)*t2*
                (-43 - 11*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               s*(2 - 4*(-4 + s2)*t2 - 16*(1 + 2*s2)*Power(t2,2) - 
                  5*(39 + 4*s2)*Power(t2,3) + 2*(-7 + s2)*Power(t2,4) + 
                  2*Power(t2,5)))) + 
         Power(s1,2)*t2*(-(Power(t1,2)*
               (1 + 2*(7 + 5*s + 6*Power(s,2) + s2)*t2 + 
                 (14 + 7*Power(s,2) + 34*s2 - s*(43 + 22*s2))*
                  Power(t2,2) - 
                 (-127 + 26*Power(s,2) + 44*s2 - 24*Power(s2,2) + 
                    s*(207 + 64*s2))*Power(t2,3) + 
                 (-320 + 5*Power(s,2) + 19*s2 - 8*Power(s2,2) + 
                    s*(-95 + 2*s2))*Power(t2,4) + 
                 (17 + 3*s - 5*s2)*Power(t2,5))) + 
            t2*(4 + (15 + 6*s + 6*Power(s,2) + 8*s2)*t2 + 
               (11*Power(s,2) + s*(28 - 68*s2) + 24*(1 + Power(s2,2)))*
                Power(t2,2) + 
               (115 + 12*Power(s,2) - 83*s2 + 48*Power(s2,2) - 
                  42*s*(3 + 2*s2))*Power(t2,3) + 
               (-170 - 69*s + 6*Power(s,2) + 25*s2 - 11*Power(s2,2))*
                Power(t2,4) + 
               (-18 + Power(s,2) + s*(3 - 2*s2) - 2*s2 + Power(s2,2))*
                Power(t2,5)) + 
            Power(t1,5)*t2*(-8 + 2*Power(s,2) + 11*t2 + Power(t2,2) + 
               s*(6 + 5*t2)) - 
            Power(t1,4)*(-2 + (3 - 4*s2)*t2 + 
               4*(-9 + 2*s2)*Power(t2,2) + 67*Power(t2,3) + 
               3*Power(t2,4) + Power(s,2)*t2*(1 + 13*t2) + 
               s*(-2 + (2 + 4*s2)*t2 + 42*Power(t2,2) + 13*Power(t2,3))) \
+ Power(t1,3)*(-1 + (7 - 2*s2)*t2 + (29 + 18*s2)*Power(t2,2) + 
               3*(-41 + 11*s2)*Power(t2,3) + (104 - 6*s2)*Power(t2,4) + 
               2*Power(t2,5) + 
               Power(s,2)*t2*(19 + 5*t2 + 10*Power(t2,2)) + 
               s*(-2 + 4*(-3 + s2)*t2 + 2*(-18 + s2)*Power(t2,2) + 
                  (31 - 6*s2)*Power(t2,3) + 4*Power(t2,4))) + 
            t1*t2*(8 + (-7 + 4*s2)*t2 + 
               (132 - 101*s2 + 12*Power(s2,2))*Power(t2,2) + 
               (111 - 86*s2 + 12*Power(s2,2))*Power(t2,3) - 
               2*(139 + 3*Power(s2,2))*Power(t2,4) - 
               2*(5 + s2)*Power(t2,5) - 
               Power(s,2)*t2*(48 + 55*t2 + Power(t2,2)) + 
               s*(12 - 6*(-9 + 4*s2)*t2 - (57 + 14*s2)*Power(t2,2) - 
                  2*(116 + 17*s2)*Power(t2,3) + 6*(-6 + s2)*Power(t2,4) + 
                  2*Power(t2,5)))))*R1q(t2))/
     ((-1 + s1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       Power(s1 - t2,3)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*(s1*t1 - t2)*
       (-1 + t2)*t2) - (8*(-2*Power(s1,6)*t1 + 
         Power(t1,4)*(-4 + 8*s - 4*Power(s,2) + 6*t2) + 
         Power(s1,5)*(2*Power(s2,2) - 11*s2*t1 - 3*Power(t1,2) + 2*t2 + 
            2*t1*(4 + s + t2)) - 
         Power(t1,3)*(-8 - 8*Power(s,2)*t2 + (8 + 7*s2)*t2 + 
            32*Power(t2,2) + s*(8 + (20 + 11*s2)*t2)) + 
         Power(t1,2)*(-4 + Power(s2,2)*(6 - 7*t2)*t2 + 
            4*Power(s,2)*(7 - 2*t2)*t2 - 2*Power(t2,2) + 
            46*Power(t2,3) + s2*t2*(11 + 37*t2 - 8*Power(t2,2)) + 
            s*t2*(-32 + 6*s2 + 6*t2 + 13*s2*t2 + 2*Power(t2,2))) - 
         4*t2*(4 - t2 - 2*Power(t2,2) + 23*Power(t2,3) + 
            Power(s2,2)*t2*(-2 + Power(t2,2)) + 
            Power(s,2)*t2*(2 - 7*t2 + Power(t2,2)) + 
            4*s2*(-1 + 4*t2 + 6*Power(t2,2)) + 
            s*(4 + 3*(1 + 8*s2)*t2 + (39 + 5*s2)*Power(t2,2) - 
               2*(1 + s2)*Power(t2,3))) + 
         Power(s1,4)*(-2*t2*(4 + s + t2) + Power(t1,2)*(7 + s + 4*t2) + 
            Power(s2,2)*(7*t1 - 2*(3 + t2)) + 
            t1*(31 + 6*t2 + s*(4 + t2)) + 
            s2*(-4 - 31*Power(t1,2) + 11*t2 + 4*s*t2 + 
               t1*(22 + 4*s + 19*t2))) - 
         2*t1*t2*(-1 - 20*t2 - 23*Power(t2,2) + 4*Power(t2,3) + 
            Power(s2,2)*(4 + t2 - 5*Power(t2,2)) + 
            Power(s,2)*(4 + 23*t2 - 3*Power(t2,2)) - 
            s2*(-1 + 5*t2 + 4*Power(t2,2) + 2*Power(t2,3)) + 
            s*(-19 - 65*t2 - 10*Power(t2,2) + 2*Power(t2,3) + 
               s2*(-8 - 30*t2 + 8*Power(t2,2)))) + 
         Power(s1,3)*(2 + Power(t1,4) - 2*Power(t1,3)*(8 + 2*s - t2) - 
            31*t2 - 8*s*t2 - 3*Power(t2,2) - s*Power(t2,2) + 
            2*Power(s,2)*Power(t2,2) + 
            2*Power(t1,2)*(28 + Power(s,2) + 3*t2 + 2*Power(t2,2) + 
               s*(8 + t2)) + Power(s2,2)*
             (8*Power(t1,2) + t2 - 2*Power(t2,2) - t1*(18 + t2)) - 
            t1*(24 + 73*t2 + 2*Power(t2,2) + 2*Power(s,2)*(5 + t2) + 
               s*(16 + 29*t2 - 4*Power(t2,2))) + 
            s2*(12 - 29*Power(t1,3) - 2*(9 + 10*s)*t2 - 19*Power(t2,2) + 
               Power(t1,2)*(59 + 11*s + 38*t2) + 
               t1*(-15 + 20*t2 - 6*Power(t2,2) + s*(2 + 7*t2)))) + 
         s1*(-6*Power(t1,5) + 
            Power(t1,4)*(10 + 2*Power(s,2) + 15*s2 + s*(8 + 3*s2) + 
               32*t2) - Power(t1,3)*
             (-6 - 3*Power(s2,2)*(-2 + t2) - 21*t2 + 46*Power(t2,2) + 
               2*Power(s,2)*(9 + t2) + 
               s2*(27 + 32*t2 - 8*Power(t2,2)) + 
               s*(-24 + s2*(-2 + t2) - 5*t2 + 2*Power(t2,2))) + 
            2*t1*(13 + 3*t2 - 20*Power(t2,2) + 70*Power(t2,3) + 
               Power(s,2)*t2*(13 - 2*t2 + Power(t2,2)) + 
               Power(s2,2)*t2*(3 + t2 + Power(t2,2)) - 
               s2*(8 - 69*t2 - 78*Power(t2,2) + Power(t2,3)) - 
               s*(-8 + (2 - 36*s2)*t2 + (-77 + s2)*Power(t2,2) + 
                  (5 + 2*s2)*Power(t2,3))) - 
            2*t2*(-15 + 12*t2 - 21*Power(t2,2) - 8*Power(t2,3) + 
               Power(s2,2)*(4 + 15*t2 - 5*Power(t2,2)) + 
               Power(s,2)*(4 + 3*t2 + 5*Power(t2,2)) - 
               s2*(-19 + 53*t2 + 2*Power(t2,2) + 2*Power(t2,3)) + 
               s*(3 + 5*t2 - 24*Power(t2,2) + 2*Power(t2,3) - 
                  8*s2*(1 + 5*t2))) + 
            Power(t1,2)*(-20 - 75*t2 - 93*Power(t2,2) + 4*Power(t2,3) + 
               2*Power(s,2)*(4 + 9*t2) + 
               Power(s2,2)*(8 + 7*t2 - 8*Power(t2,2)) - 
               s2*(-10 + 74*t2 + 23*Power(t2,2) + 4*Power(t2,3)) + 
               s*(-18 - 130*t2 - 25*Power(t2,2) + 4*Power(t2,3) + 
                  2*s2*(-8 - 31*t2 + 4*Power(t2,2))))) + 
         Power(s1,2)*(-6 - 3*(7 + s + 3*s2)*Power(t1,4) + 
            (22 + 10*Power(s,2) - s2 + 28*Power(s2,2) + 2*s*(16 + 5*s2))*
             t2 + (66 - 8*Power(s,2) + s*(28 - 3*s2) + 15*s2 + 
               Power(s2,2))*Power(t2,2) + 
            2*(-1 + Power(s,2) + 3*s2 + Power(s2,2) - 2*s*(1 + s2))*
             Power(t2,3) + Power(t1,3)*
             (35 + 4*Power(s,2) + 3*Power(s2,2) + 46*t2 + 
               4*Power(t2,2) + s*(20 + 10*s2 + t2) + s2*(52 + 19*t2)) - 
            Power(t1,2)*(Power(s2,2)*(18 - 4*t2) + 
               4*Power(s,2)*(6 + t2) + s2*(38 + 31*t2 - 2*Power(t2,2)) + 
               10*(1 - 5*t2 + 5*Power(t2,2)) - 
               2*s*((-1 + t2)*t2 + s2*(2 + t2))) + 
            t1*(Power(s2,2)*(8 + 4*t2 - 10*Power(t2,2)) + 
               2*Power(s,2)*(4 + 9*t2 + Power(t2,2)) - 
               s2*(-18 + 165*t2 + 46*Power(t2,2) + 4*Power(t2,3)) - 
               2*(11 + 17*t2 + 24*Power(t2,2) + 10*Power(t2,3)) + 
               s*(-6 - 10*t2 - 46*Power(t2,2) + 4*Power(t2,3) + 
                  s2*(-16 - 75*t2 + 8*Power(t2,2))))))*R2q(1 - s1 - t1 + t2)\
)/((-1 + s1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*(-s + s1 - t2)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*(-1 + t2)) - 
    (8*(Power(s1,8)*(-Power(s2,2) + 2*s2*t1 + Power(t1,2)) + 
         (-1 + s)*Power(t1,6)*(-2 + 2*s + 3*t2) + 
         Power(t1,4)*(2 - (7 + 19*Power(s,2) + s*(-21 + s2))*t2 + 
            (-3 + 3*Power(s,2) - 15*s2 + Power(s2,2) - 4*s*(4 + s2))*
             Power(t2,2) + (6*s - 2*(16 + s2))*Power(t2,3)) - 
         2*Power(t1,2)*t2*(6 - 
            (-5 + s + 31*Power(s,2) + 4*s2 + 3*s*s2 - 2*Power(s2,2))*
             t2 + (-51 + 13*Power(s,2) - 17*s2 + 5*Power(s2,2) - 
               s*(25 + 18*s2))*Power(t2,2) - 
            (49 + Power(s,2) - 4*s2 + 2*Power(s2,2) - 3*s*(1 + s2))*
             Power(t2,3) + (5 + s - s2)*Power(t2,4)) + 
         Power(t1,5)*(-4 - 3*Power(s,2)*t2 - 3*(-3 + s2)*t2 + 
            3*(3 + s2)*Power(t2,2) + s*(4 + t2 + 3*s2*t2 - 7*Power(t2,2))\
) + Power(t1,3)*t2*(13 - 54*t2 + Power(s2,2)*(7 - 4*t2)*t2 + 
            Power(s,2)*(25 - 2*t2)*t2 + 3*Power(t2,2) + 34*Power(t2,3) + 
            s*(-31 + (2 - 32*s2)*t2 + (11 + 6*s2)*Power(t2,2)) + 
            s2*(3 + 22*t2 + 19*Power(t2,2) - 2*Power(t2,3))) + 
         8*Power(t2,2)*(-2 + 4*t2 - Power(s2,2)*(-4 + t2)*t2 + 
            3*Power(s,2)*(-2 + t2)*t2 - 7*Power(t2,2) + 6*Power(t2,3) + 
            3*Power(t2,4) - s*
             (2 + (11 + 6*s2)*t2 + (21 + 2*s2)*Power(t2,2) + 
               2*Power(t2,3)) + 
            s2*(2 - 7*t2 + 15*Power(t2,2) + 6*Power(t2,3))) - 
         Power(s1,7)*(Power(s2,2)*(-2 + 5*t1 - t2) + 
            t1*(2 + s*(-2 + t1) - 3*t1 - 4*Power(t1,2) + 2*t2 + 
               2*t1*t2) + s2*(-13*Power(t1,2) + t1*(11 + s + 3*t2) + 
               2*(-1 + t2 + s*t2))) + 
         Power(s1,6)*(-1 + 6*Power(t1,4) + 2*t2 + Power(t2,2) - 
            Power(s,2)*Power(t2,2) - 2*Power(t1,3)*(-9 + 2*s + 3*t2) - 
            Power(t1,2)*(20 + 25*t2 + Power(t2,2) - s*(4 + 3*t2)) + 
            Power(s2,2)*(3 - 10*Power(t1,2) + 4*t2 + Power(t2,2) + 
               t1*(8 + 5*t2)) + 
            s2*(-4 + 32*Power(t1,3) + 9*t2 + 7*s*t2 + 3*Power(t2,2) - 
               Power(t1,2)*(47 + 5*s + 22*t2) - 
               2*t1*(-6 - s + 3*t2 + 5*s*t2)) + 
            t1*(11 - 3*t2 + Power(s,2)*t2 + 4*Power(t2,2) - 
               s*(5 + 3*t2 + 3*Power(t2,2)))) - 
         2*t1*Power(t2,2)*(-15 - 25*t2 + 95*Power(t2,2) + 
            49*Power(t2,3) + Power(s2,2)*
             (4 + 7*t2 - 6*Power(t2,2) + Power(t2,3)) + 
            Power(s,2)*(4 + 23*t2 - 6*Power(t2,2) + Power(t2,3)) + 
            s2*(3 + 34*t2 + 45*Power(t2,2) + 2*Power(t2,3)) - 
            s*(29 + 10*t2 + 11*Power(t2,2) + 2*Power(t2,3) + 
               2*s2*(4 + 15*t2 - 6*Power(t2,2) + Power(t2,3)))) + 
         Power(s1,4)*(3 + Power(t1,6) + 
            Power(t1,5)*(45 - 4*s + 22*s2 - 2*t2) + 
            (6 + s*(2 - 13*s2) + 22*s2 - 15*Power(s2,2))*t2 + 
            (1 + Power(s,2) - 58*s2 + 10*Power(s2,2) - 3*s*(5 + 17*s2))*
             Power(t2,2) + (-17 + 7*Power(s,2) - 12*s2 - 
               6*Power(s2,2) - s*(17 + s2))*Power(t2,3) + 
            (-1 + s - s2)*Power(t2,4) - 
            Power(t1,4)*(88 + 5*Power(s2,2) + s*(31 + 10*s2 - 9*t2) + 
               133*t2 + 3*Power(t2,2) + s2*(74 + 36*t2)) + 
            Power(t1,3)*(-47 + Power(s2,2)*(17 - 2*t2) - 30*t2 + 
               3*Power(s,2)*t2 + 95*Power(t2,2) + 
               s*(-47 + s2*(7 - 5*t2) + 9*t2 - 6*Power(t2,2)) + 
               s2*(62 + 8*t2 + 4*Power(t2,2))) - 
            t1*(12 + 8*t2 + 50*Power(t2,2) + 61*Power(t2,3) + 
               Power(s,2)*t2*(-3 + t2 + 3*Power(t2,2)) + 
               Power(s2,2)*t2*(49 + 14*t2 + 3*Power(t2,2)) + 
               s2*(11 + 91*t2 + 148*Power(t2,2) + 6*Power(t2,3)) - 
               s*(-1 + (72 - 22*s2)*t2 + 7*(1 + 5*s2)*Power(t2,2) + 
                  6*(4 + s2)*Power(t2,3))) + 
            Power(t1,2)*(27 + 140*t2 + 188*Power(t2,2) + 
               12*Power(t2,3) - 2*Power(s,2)*(-6 + 7*t2) + 
               Power(s2,2)*t2*(15 + 7*t2) + 
               s2*(2 + 259*t2 + 100*Power(t2,2) + 3*Power(t2,3)) + 
               s*(33 + 59*t2 + 14*Power(t2,2) - 3*Power(t2,3) + 
                  s2*(4 + 69*t2 - 7*Power(t2,2))))) - 
         Power(s1,5)*(-2 - 4*Power(t1,5) + 10*t2 + s*t2 - 
            4*s*Power(t2,2) - 3*Power(s,2)*Power(t2,2) + 2*Power(t2,3) - 
            3*s*Power(t2,3) + Power(s,2)*Power(t2,3) + 
            Power(t1,4)*(-41 + 6*s + 6*t2) + 
            Power(t1,3)*(67 + s*(8 - 9*t2) + 85*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*(10*Power(t1,3) - 3*Power(t1,2)*(5 + 2*t2) + 
               t2*(21 + 6*t2 + Power(t2,2)) - 
               t1*(4 + 14*t2 + 3*Power(t2,2))) + 
            Power(t1,2)*(5 - 2*t2 - 3*Power(s,2)*t2 - 39*Power(t2,2) + 
               2*s*(16 + t2 + 4*Power(t2,2))) + 
            t1*(7 - 20*t2 - 38*Power(t2,2) - 2*Power(t2,3) + 
               Power(s,2)*t2*(3 + 2*t2) + 
               s*(4 - 9*t2 - 12*Power(t2,2) + Power(t2,3))) - 
            s2*(-6 + 38*Power(t1,4) - 10*t2 + 3*(-3 + 4*s)*Power(t2,2) + 
               2*s*Power(t2,3) - 2*Power(t1,3)*(41 + 5*s + 23*t2) + 
               Power(t1,2)*(41 + s*(6 - 15*t2) - 7*t2 + 4*Power(t2,2)) + 
               t1*(4 + 107*t2 + 36*Power(t2,2) + Power(t2,3) + 
                  s*(7 + 34*t2 - Power(t2,2))))) + 
         Power(s1,2)*(5*Power(t1,7) - 
            Power(t1,6)*(s*(17 + s2) + s2*(7 - 2*t2) + 30*t2) + 
            2*t2*(-9 + (13 + 8*Power(s,2) - 19*s2 + 3*Power(s2,2) + 
                  s*(20 + 29*s2))*t2 + 
               (-30 - 7*Power(s,2) + 55*s2 - 21*Power(s2,2) + 
                  s*(31 + 52*s2))*Power(t2,2) + 
               (19 - 6*Power(s,2) - 21*s2 + 5*Power(s2,2) + 
                  s*(46 + s2))*Power(t2,3) + 
               (3 - 3*s + 3*s2)*Power(t2,4)) - 
            Power(t1,2)*(31 + 31*t2 + 472*Power(t2,2) + 
               363*Power(t2,3) - 45*Power(t2,4) + 
               Power(s,2)*t2*(91 - 56*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*t2*(-17 + 11*t2 + 20*Power(t2,2)) + 
               s*(16 + (195 - 26*s2)*t2 + (365 + 101*s2)*Power(t2,2) - 
                  (7 + 25*s2)*Power(t2,3) - 3*Power(t2,4)) + 
               s2*(-16 + 46*t2 + 107*Power(t2,2) + 42*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(t1,5)*(-34 - 3*Power(s2,2)*(-1 + t2) - 51*t2 + 
               35*Power(t2,2) + s2*(6 + 6*t2 - 4*Power(t2,2)) + 
               s*(4 + 18*t2 + Power(t2,2) + 3*s2*(1 + t2))) + 
            Power(t1,4)*(-12 + 185*t2 + 291*Power(t2,2) - 
               8*Power(t2,3) + Power(s,2)*(26 - 14*t2 + Power(t2,2)) + 
               Power(s2,2)*(1 + t2 + 4*Power(t2,2)) + 
               s2*(16 + 88*t2 + 11*Power(t2,2) + Power(t2,3)) - 
               s*(-21 - 130*t2 + 12*Power(t2,2) + Power(t2,3) + 
                  s2*(16 - 28*t2 + 5*Power(t2,2)))) - 
            Power(t1,3)*(-28 - 149*t2 + 139*Power(t2,2) + 
               285*Power(t2,3) + 
               s2*(3 + 134*t2 + 193*Power(t2,2) - 10*Power(t2,3)) + 
               Power(s2,2)*(8 + 31*t2 - 22*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(8 + 4*t2 - 5*Power(t2,2) + Power(t2,3)) - 
               s*(7 + 71*t2 - 62*Power(t2,2) + 4*Power(t2,3) + 
                  s2*(16 - 17*t2 - 23*Power(t2,2) + 2*Power(t2,3)))) + 
            t1*t2*(41 - 84*t2 + 63*Power(t2,2) + 340*Power(t2,3) + 
               s2*(-15 + 314*t2 + 521*Power(t2,2) - 6*Power(t2,3)) + 
               Power(s2,2)*(16 + 27*t2 + 6*Power(t2,3)) + 
               Power(s,2)*(16 - 31*t2 + 6*Power(t2,2) + 6*Power(t2,3)) - 
               s*(-27 + 278*t2 - 53*Power(t2,2) + 16*Power(t2,3) + 
                  2*s2*(16 - 58*t2 + 11*Power(t2,2) + 6*Power(t2,3))))) - 
         Power(s1,3)*((-24 + s - 5*s2)*Power(t1,6) + 
            t1*(-7 + (39 + 25*Power(s,2) + 26*s2 - 9*Power(s2,2) + 
                  s*(71 + 40*s2))*t2 + 
               (16 - 34*Power(s,2) + 279*s2 - 9*Power(s2,2) + 
                  s*(121 + 131*s2))*Power(t2,2) + 
               (147 - 3*Power(s,2) + s*(109 - 11*s2) + 18*s2 + 
                  14*Power(s2,2))*Power(t2,3) + 
               (15 - 9*s + 9*s2)*Power(t2,4)) + 
            Power(t1,5)*(42 + Power(s2,2) + s*(35 + 5*s2 - 3*t2) + 
               101*t2 + Power(t2,2) + 7*s2*(5 + t2)) - 
            t2*(-1 + 12*t2 + 29*Power(t2,2) + 28*Power(t2,3) + 
               Power(s,2)*t2*(-7 - 16*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*t2*(65 + 4*t2 + 6*Power(t2,2)) + 
               s2*(33 + 22*t2 + 129*Power(t2,2) + 2*Power(t2,3)) + 
               s*(7 + 2*(-9 + 7*s2)*t2 + (1 - 12*s2)*Power(t2,2) - 
                  4*(4 + 3*s2)*Power(t2,3))) - 
            Power(t1,4)*(-57 + Power(s2,2)*(11 - 7*t2) - 72*t2 + 
               Power(s,2)*t2 + 95*Power(t2,2) + 
               s2*(38 + 20*t2 - 4*Power(t2,2)) + 
               s*(-17 + 19*t2 + 5*s2*(1 + t2))) - 
            Power(t1,3)*(23 + 317*t2 + 400*Power(t2,2) - 
               6*Power(t2,3) + Power(s2,2)*t2*(5 + 9*t2) + 
               2*Power(s,2)*(15 - 11*t2 + Power(t2,2)) + 
               s2*(2 + 229*t2 + 78*Power(t2,2) + 3*Power(t2,3)) + 
               s*(59 + 148*t2 - 4*Power(t2,2) - 3*Power(t2,3) + 
                  s2*(-16 + 67*t2 - 11*Power(t2,2)))) + 
            Power(t1,2)*(t2*(-117 + 89*t2 + 299*Power(t2,2)) + 
               Power(s2,2)*(8 + 45*t2 - 6*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s,2)*(8 - 8*t2 + 3*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(-3 + 232*t2 + 327*Power(t2,2) + 6*Power(t2,3)) - 
               s*(7*(-1 + 27*t2 + 2*Power(t2,3)) + 
                  s2*(16 - 49*t2 + 11*Power(t2,2) + 6*Power(t2,3))))) + 
         s1*(-3*(-1 + s)*Power(t1,7) + 
            Power(t1,3)*(3 + (47 - 73*Power(s,2) - 62*s2 + 
                  5*Power(s2,2) + s*(-45 + 28*s2))*t2 + 
               (-246 + 42*Power(s,2) - 77*s2 - Power(s2,2) - 
                  s*(155 + 49*s2))*Power(t2,2) + 
               (-289 - 5*Power(s,2) + 2*s2 - 8*Power(s2,2) + 
                  s*(17 + 13*s2))*Power(t2,3) + 
               (19 + 3*s - 3*s2)*Power(t2,4)) + 
            Power(t1,6)*(-8 - s2 - 14*t2 - 3*s2*t2 + s*(1 + s2 + 7*t2)) + 
            Power(t1,5)*(-4 + Power(s,2)*(10 - 3*t2) + 3*t2 + 
               Power(s2,2)*t2 + 62*Power(t2,2) + 4*s2*(2 + 5*t2) + 
               s*(-5 + 3*s2*(-1 + t2) + 35*t2 - 6*Power(t2,2))) + 
            Power(t1,4)*(6 + 83*t2 + 4*Power(s,2)*(-3 + t2)*t2 + 
               25*Power(t2,2) - 69*Power(t2,3) + 
               2*Power(s2,2)*t2*(-7 + 4*t2) - 
               s*(-13 + (16 - 13*s2)*t2 + (30 + 11*s2)*Power(t2,2) + 
                  Power(t2,3)) + 
               s2*(-7 - 6*t2 - 30*Power(t2,2) + 6*Power(t2,3))) + 
            2*t1*t2*(31 - 26*t2 + 151*Power(t2,2) + 22*Power(t2,3) - 
               30*Power(t2,4) + 
               Power(s,2)*t2*(49 - 32*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*t2*(-29 + 22*t2 + 5*Power(t2,2)) - 
               s2*(16 - 85*t2 + 92*Power(t2,2) + 21*Power(t2,3)) + 
               s*(16 + (117 + 4*s2)*t2 + 18*(12 + s2)*Power(t2,2) - 
                  (1 + 10*s2)*Power(t2,3))) - 
            2*Power(t2,2)*(5 - 5*t2 - 9*Power(t2,2) + 65*Power(t2,3) + 
               s2*(9 + 44*t2 + 123*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s,2)*(4 - 19*t2 - 6*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*(4 + 17*t2 - 2*Power(t2,2) + 3*Power(t2,3)) + 
               s*(21 - 52*t2 + 31*Power(t2,2) - 4*Power(t2,3) + 
                  s2*(-8 + 42*t2 - 6*Power(t2,3)))) + 
            Power(t1,2)*t2*(-93 - 134*t2 + 353*Power(t2,2) + 
               290*Power(t2,3) + 
               s2*(27 + 114*t2 + 243*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*(16 + 53*t2 - 32*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s,2)*(16 + 29*t2 - 28*Power(t2,2) + 4*Power(t2,3)) - 
               s*(35 + 50*t2 - 31*Power(t2,2) + 8*Power(t2,3) + 
                  s2*(32 + 26*t2 - 52*Power(t2,2) + 8*Power(t2,3))))))*
       T2q(t2,1 - s1 - t1 + t2))/
     ((-1 + s1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*(-s + s1 - t2)*
       (-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)) + 
    (8*(Power(s1,8)*(-Power(s2,2) + 2*s2*t1 + Power(t1,2)) - 
         Power(s1,7)*(2*Power(s2,2)*(-1 + t1 - 2*t2) + 
            t1*(2 + s*(-2 + t1) - 3*t1 - Power(t1,2) + 2*t2 + 5*t1*t2) + 
            s2*(-3*Power(t1,2) + t1*(11 + s + 9*t2) + 2*(-1 + t2 + s*t2))\
) + Power(s1,6)*(-1 + 2*t2 + Power(t2,2) - Power(s,2)*Power(t2,2) - 
            Power(t1,3)*(-5 + s + 3*t2) + 
            2*Power(t1,2)*(-7 + s - 5*t2 + 3*s*t2 + 4*Power(t2,2)) - 
            Power(s2,2)*(-3 + Power(t1,2) + 8*t2 + 5*Power(t2,2) - 
               2*t1*(1 + 4*t2)) + 
            t1*(11 + 3*t2 + Power(s,2)*t2 + 10*Power(t2,2) - 
               s*(5 + 9*t2 + 3*Power(t2,2))) + 
            s2*(-4 + Power(t1,3) + (3 + 7*s)*t2 + 
               (9 + 6*s)*Power(t2,2) - 2*Power(t1,2)*(7 + s + 7*t2) + 
               t1*(6 - s*(-2 + t2) + 53*t2 + 15*Power(t2,2)))) + 
         Power(s1,4)*(3 + (-6 + s*(2 - 13*s2) + 16*s2 + 3*Power(s2,2))*
             t2 + (40 + Power(s,2) - 22*s2 + 40*Power(s2,2) - 
               s*(8 + 9*s2))*Power(t2,2) + 
            (7 - 8*Power(s,2) + 114*s2 - 8*Power(s2,2) + 
               s*(-29 + 20*s2))*Power(t2,3) + 
            (8 + 10*s2 + 5*Power(s2,2) - 4*s*(2 + s2))*Power(t2,4) - 
            Power(t1,4)*(9 + s + 7*t2 + 4*s*t2 + 4*Power(t2,2)) + 
            Power(t1,3)*(8 + 2*Power(s,2) + 85*t2 + 15*Power(t2,2) + 
               7*Power(t2,3) + 
               s*(9 + s2 - 5*t2 + 5*s2*t2 + 7*Power(t2,2)) + 
               s2*(5 + 18*t2 + 11*Power(t2,2))) + 
            t1*(-18 + 28*t2 + 3*Power(t2,2) - 27*Power(t2,3) + 
               8*Power(t2,4) + Power(s,2)*t2*(3 + 5*t2) + 
               Power(s2,2)*t2*(21 - 4*t2 + 8*Power(t2,2)) + 
               s2*(7 - 93*t2 - 41*Power(t2,2) + 94*Power(t2,3)) - 
               s*(1 + (-33 + 35*s2)*t2 + (61 + 8*s2)*Power(t2,2) + 
                  (50 + 8*s2)*Power(t2,3) + 6*Power(t2,4))) - 
            Power(t1,2)*(-16 - 20*t2 + 109*Power(t2,2) + 
               27*Power(t2,3) + Power(t2,4) + 
               2*Power(s2,2)*t2*(2 + 3*t2) + Power(s,2)*(-4 + 11*t2) + 
               s2*(12 + 41*t2 + 73*Power(t2,2) + 18*Power(t2,3)) - 
               s*(23 + 3*t2 + 35*Power(t2,2) + 7*Power(t2,3) + 
                  s2*(-7 + t2 - 3*Power(t2,2))))) + 
         Power(s1,3)*(t1*(-2 + 
               (37 - 9*Power(s,2) - 13*s2 + 3*s*(-18 + 7*s2))*t2 + 
               (-37 + 16*Power(s,2) + 186*s2 - 34*Power(s2,2) + 
                  s*(-39 + 53*s2))*Power(t2,2) + 
               (109 + 3*Power(s,2) + s*(55 - 2*s2) + 79*s2 + 
                  16*Power(s2,2))*Power(t2,3) + 
               (57 + 2*Power(s,2) - 47*s2 - 2*Power(s2,2) + 
                  s*(48 + s2))*Power(t2,4) + (2 + 3*s2)*Power(t2,5)) + 
            Power(t1,4)*(2*Power(s,2)*(1 + t2) + 
               3*s*(2 + t2 + 2*Power(t2,2)) + 
               t2*(21 + 11*t2 + 2*Power(t2,2))) + 
            t2*(2 - 6*t2 - 63*Power(t2,2) + 7*Power(t2,3) - 
               4*Power(t2,4) + 
               s2*(-3 + 8*t2 + 25*Power(t2,2) - 128*Power(t2,3)) - 
               Power(s,2)*t2*(7 + t2 - 6*Power(t2,2) + 2*Power(t2,3)) - 
               2*Power(s2,2)*t2*
                (6 + 20*t2 - Power(t2,2) + 2*Power(t2,3)) + 
               s*(7 + (-18 + 55*s2)*t2 + (47 + 23*s2)*Power(t2,2) + 
                  (65 - 4*s2)*Power(t2,3) + 6*(1 + s2)*Power(t2,4))) - 
            Power(t1,3)*(2 + (9 + 11*s2)*t2 + 
               (101 + 24*s2)*Power(t2,2) + (17 + 5*s2)*Power(t2,3) + 
               4*Power(t2,4) + 6*Power(s,2)*t2*(2 + t2) + 
               s*(-8 + 52*t2 - 27*Power(t2,2) + 11*Power(t2,3) + 
                  s2*(4 + t2 + 3*Power(t2,2)))) + 
            Power(t1,2)*(4 + 3*(-17 + 9*s2)*t2 + 
               (-107 + 30*s2 + 6*Power(s2,2))*Power(t2,2) + 
               (89 + 41*s2 + 4*Power(s2,2))*Power(t2,3) + 
               5*(2 + s2)*Power(t2,4) + Power(t2,5) + 
               Power(s,2)*(4 + 9*t2 + 9*Power(t2,2)) - 
               s*(14 + 24*t2 - 18*Power(t2,2) + 73*Power(t2,3) + 
                  s2*(-4 - 4*t2 + 17*Power(t2,2) + Power(t2,3))))) - 
         Power(t2,2)*(Power(s,2)*
             (4 + 16*t2 - 2*Power(t1,3)*t2 - 6*Power(t2,2) - 
               7*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,2)*t2*(2 + 5*t2) - 
               t1*(4 + 10*t2 - 9*Power(t2,2) + 5*Power(t2,3))) + 
            s*t2*(-10 + Power(t1,3)*(4 - 3*t2) + 13*t2 + 
               18*Power(t2,2) + 7*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,2)*(2 + 7*t2 + 9*Power(t2,2)) - 
               t1*(-4 + 47*t2 + 12*Power(t2,2) + 8*Power(t2,3)) + 
               s2*(4 + 10*t2 - 3*Power(t1,2)*t2 + 6*Power(t2,2) - 
                  4*Power(t2,3) + t1*(-4 - t2 + 4*Power(t2,2)))) + 
            t2*(Power(t1,3)*(-2 + 3*t2) + 
               Power(t1,2)*(4 + 3*(-1 + s2)*t2 + 
                  (1 - 3*s2)*Power(t2,2)) + 
               t1*(-2 - (5 + 4*s2)*t2 - 
                  (29 - 3*s2 + Power(s2,2))*Power(t2,2) + 
                  2*(-5 + 2*s2)*Power(t2,3)) + 
               t2*(5 + 4*t2 + 23*Power(t2,2) + 8*Power(t2,3) + 
                  Power(s2,2)*t2*(-3 + 2*t2) - 
                  s2*(-1 - 20*t2 + Power(t2,2) + 2*Power(t2,3))))) + 
         Power(s1,5)*(2 - 7*t2 - s*t2 - 6*Power(t2,2) + 
            4*s*Power(t2,2) + 3*Power(s,2)*Power(t2,2) - 5*Power(t2,3) + 
            3*s*Power(t2,3) + 2*Power(s,2)*Power(t2,3) + 
            Power(t1,4)*(1 + 2*t2) - 
            Power(t1,3)*(21 + s*(2 - 3*t2) + 11*t2 + Power(t2,2)) - 
            Power(t1,2)*(2 - 55*t2 - 23*Power(t2,2) + 4*Power(t2,3) + 
               s*(5 + 13*t2 + 11*Power(t2,2))) + 
            Power(s2,2)*(6*t2*(-3 + 2*t2) + Power(t1,2)*(1 + 4*t2) - 
               t1*(5 + 4*t2 + 12*Power(t2,2))) - 
            t1*(4 + 31*t2 - 2*Power(t2,2) + 16*Power(t2,3) + 
               Power(s,2)*t2*(3 + 2*t2) - 
               s*(-4 + 28*t2 + 27*Power(t2,2) + 8*Power(t2,3))) + 
            s2*(-6 + 14*t2 - (46 + 21*s)*Power(t2,2) - 
               (15 + 4*s)*Power(t2,3) - Power(t1,3)*(5 + s + 7*t2) + 
               Power(t1,2)*(15 + 55*t2 + 24*Power(t2,2) + 
                  s*(2 + 5*t2)) + 
               t1*(s*(7 + t2 + 8*Power(t2,2)) - 
                  2*(-8 + 2*t2 + 50*Power(t2,2) + 5*Power(t2,3))))) + 
         s1*t2*(Power(s,2)*(2*Power(t1,3)*t2*(-1 + 2*t2) - 
               Power(t1,2)*(8 + 18*t2 - 19*Power(t2,2) + 
                  7*Power(t2,3)) - 
               t2*(-4 + 2*t2 + 3*Power(t2,2) - 5*Power(t2,3) + 
                  Power(t2,4)) + 
               t1*(8 + 34*t2 - 3*Power(t2,2) - 18*Power(t2,3) + 
                  4*Power(t2,4))) + 
            s*t2*(-10 - 3*t2 - 3*Power(t1,4)*t2 + 16*Power(t2,2) + 
               50*Power(t2,3) + 23*Power(t2,4) - Power(t2,5) + 
               Power(t1,3)*(24 + 8*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(2 - 84*t2 + 15*Power(t2,2) - 
                  10*Power(t2,3)) + 
               t1*(-16 - 38*t2 + 7*Power(t2,2) - 43*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s2*(4 + 20*t2 + Power(t1,3)*t2 + 45*Power(t2,2) - 
                  3*Power(t2,3) + Power(t2,4) + 
                  Power(t1,2)*(-8 - 16*t2 + 3*Power(t2,2)) + 
                  t1*(4 + 19*t2 - 7*Power(t2,3)))) + 
            t2*(3*Power(t1,4)*t2 - 
               Power(t1,3)*(6 + (-13 + s2)*t2 + 
                  3*(-2 + s2)*Power(t2,2)) + 
               Power(t1,2)*(12 + (-25 + 9*s2)*t2 + 
                  (-71 - 13*s2 + Power(s2,2))*Power(t2,2) + 
                  4*(-5 + s2)*Power(t2,3)) + 
               t1*(-6 - (9 + 7*s2)*t2 + 
                  (-51 + 54*s2 - 9*Power(s2,2))*Power(t2,2) + 
                  (46 + 17*s2 + 4*Power(s2,2))*Power(t2,3) + 
                  (11 - 2*s2)*Power(t2,4)) + 
               t2*(18 - 4*t2 + 2*Power(s2,2)*(-6 + t2)*t2 + 
                  38*Power(t2,2) + 27*Power(t2,3) + Power(t2,4) + 
                  s2*(-1 + 62*t2 + 5*Power(t2,2) - 20*Power(t2,3) + 
                     Power(t2,4))))) - 
         Power(s1,2)*(Power(s,2)*
             (2*Power(t1,4)*t2*(1 + t2) + 
               Power(t1,2)*(4 + 20*t2 - 9*Power(t2,2) - 
                  21*Power(t2,3)) - 
               2*Power(t1,3)*(2 + 6*t2 - 3*Power(t2,2) + Power(t2,3)) - 
               Power(t2,2)*(8 + 7*t2 - 3*Power(t2,2) + Power(t2,4)) + 
               t1*t2*(8 + 15*t2 + 18*Power(t2,2) + 9*Power(t2,3) + 
                  Power(t2,4))) + 
            s*t2*(Power(t1,4)*(6 - t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(28 - 32*t2 + 31*Power(t2,2) - 
                  2*Power(t2,3) + s2*(-4 + t2 + Power(t2,2))) - 
               Power(t1,2)*(14 + 92*t2 - 22*Power(t2,2) + 
                  59*Power(t2,3) - Power(t2,4) + 
                  s2*(4 + 16*t2 + 11*Power(t2,2) + Power(t2,3))) - 
               t1*(20 + 46*t2 - 9*Power(t2,2) + 18*Power(t2,3) - 
                  21*Power(t2,4) + Power(t2,5) + 
                  s2*(-8 - 41*t2 - 21*Power(t2,2) + 14*Power(t2,3) + 
                     Power(t2,4))) + 
               t2*(-9 - 18*t2 + 81*Power(t2,2) + 61*Power(t2,3) + 
                  s2*(10 + 81*t2 + 15*Power(t2,2) + 3*Power(t2,3) + 
                     2*Power(t2,4)))) + 
            t2*(5*Power(t1,4)*t2*(3 + t2) - 
               Power(t1,3)*(6 + (-9 + 7*s2)*t2 + 
                  (31 + 14*s2)*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(12 + 3*(-19 + 7*s2)*t2 + 
                  (-161 - 6*s2 + 4*Power(s2,2))*Power(t2,2) + 
                  (1 + 13*s2 + Power(s2,2))*Power(t2,3) - Power(t2,4)) + 
               t1*(-6 + (15 - 9*s2)*t2 + 
                  (-35 + 160*s2 - 26*Power(s2,2))*Power(t2,2) + 
                  (148 + 53*s2 + 14*Power(s2,2))*Power(t2,3) + 
                  (44 - 13*s2)*Power(t2,4) + (2 + s2)*Power(t2,5)) + 
               t2*(18 - 18*t2 - 16*Power(t2,2) + 29*Power(t2,3) + 
                  Power(t2,4) - 
                  Power(s2,2)*t2*(18 + 15*t2 + Power(t2,3)) + 
                  s2*(-5 + 60*t2 + 19*Power(t2,2) - 73*Power(t2,3) + 
                     3*Power(t2,4))))))*T3q(t2,s1))/
     ((-1 + s1)*Power(s1 - t2,2)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       Power(s1*t1 - t2,2)*(-1 + t2)) - 
    (8*(Power(s1,7)*(Power(s2,2) - 2*s2*t1 - Power(t1,2)) + 
         (-1 + s)*Power(t1,3)*(-2 + 2*s + 3*t2) + 
         t1*(2 - (-5 + 7*Power(s,2) + s*(3 + s2))*t2 + 
            (-29 + 3*Power(s,2) + 17*s2 + Power(s2,2) - 2*s*(9 + 4*s2))*
             Power(t2,2) + 2*(-4 + 5*s - 5*s2)*Power(t2,3)) + 
         Power(t1,2)*(-4 - 3*Power(s,2)*t2 - 3*(-3 + s2)*t2 + 
            (5 + 3*s2)*Power(t2,2) + s*(4 + t2 + 3*s2*t2 - 7*Power(t2,2))\
) + t2*(-11 - 20*t2 - 5*Power(s2,2)*t2 + Power(s,2)*(7 - 2*t2)*t2 + 
            15*Power(t2,2) + 8*Power(t2,3) + 
            s*(-7 + 6*(2 + s2)*t2 + (23 + 2*s2)*Power(t2,2) - 
               6*Power(t2,3)) + 
            s2*(3 + 8*t2 - 7*Power(t2,2) + 6*Power(t2,3))) + 
         Power(s1,6)*(Power(s2,2)*(-4 + 2*t1 - t2) + 
            t1*(2 + s*(-2 + t1) + 7*t1 - Power(t1,2) + 2*t2 + 2*t1*t2) + 
            s2*(-3*Power(t1,2) + t1*(15 + s + 3*t2) + 2*(-1 + t2 + s*t2))\
) + Power(s1,5)*(1 + (11 + s)*Power(t1,3) - 2*t2 - Power(t2,2) + 
            Power(s,2)*Power(t2,2) + 
            Power(s2,2)*(2 + Power(t1,2) + 4*t2 - Power(t2,2) - 
               2*t1*(3 + t2)) + 
            Power(t1,2)*(-17 - 7*t2 + Power(t2,2) - s*(8 + 3*t2)) - 
            s2*(-8 + Power(t1,3) + 13*t2 + 11*s*t2 + 3*Power(t2,2) - 
               Power(t1,2)*(22 + 2*s + 5*t2) + 
               t1*(30 + 4*s + 26*t2 - 4*s*t2)) - 
            t1*(15 + 17*t2 + Power(s,2)*t2 + 4*Power(t2,2) - 
               3*s*(3 + t2 + Power(t2,2)))) + 
         s1*(-3 - 3*(-1 + s)*Power(t1,4) + 
            (22 - 4*s2 - 3*Power(s2,2) + s*(12 + 13*s2))*t2 + 
            (-15 - 15*Power(s,2) + 14*s2 + 4*Power(s2,2) + 
               s*(-41 + 21*s2))*Power(t2,2) + 
            (-61 + 3*Power(s,2) + 40*s2 - 6*Power(s2,2) + 
               s*(-7 + 3*s2))*Power(t2,3) + 
            (17 + 7*s - 7*s2)*Power(t2,4) + 
            Power(t1,3)*(-12 - 4*Power(s,2) + s*(9 + s2 + t2) - 
               s2*(1 + 3*t2)) - 
            t1*(-14 - 38*t2 + 2*Power(s2,2)*(-5 + t2)*t2 - 
               44*Power(t2,2) + 41*Power(t2,3) + 
               Power(s,2)*t2*(-11 + 2*t2) + 
               s2*(7 + 11*t2 + 5*Power(t2,2) - 2*Power(t2,3)) + 
               s*(-1 + (15 + 28*s2)*t2 + (51 + s2)*Power(t2,2) + 
                  5*Power(t2,3))) + 
            Power(t1,2)*(-2 + 28*t2 + Power(s2,2)*t2 + 16*Power(t2,2) + 
               Power(s,2)*(4 + 3*t2) + 
               2*s2*(4 - 19*t2 + 5*Power(t2,2)) + 
               s*(-1 + 46*t2 + s2*(-3 + 5*t2)))) + 
         Power(s1,3)*(2 + (13 - 3*s)*Power(t1,4) - 22*t2 - 4*s*t2 - 
            50*Power(t2,2) + 15*s*Power(t2,2) + 
            6*Power(s,2)*Power(t2,2) - 9*Power(t2,3) + 
            23*s*Power(t2,3) - 3*Power(s,2)*Power(t2,3) + Power(t2,4) - 
            s*Power(t2,4) + Power(t1,3)*(23 - 42*t2 + s*(44 + 3*t2)) + 
            Power(t1,2)*(22 + Power(s,2)*(4 - 3*t2) + 3*t2 + 
               13*Power(t2,2) - s*(33 + 71*t2)) - 
            Power(s2,2)*(3 + 19*t2 + 5*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(-17 + 7*t2) - 4*t1*(-3 - 2*t2 + Power(t2,2))\
) + t1*(-1 - 101*t2 - 28*Power(t2,2) + 19*Power(t2,3) + 
               2*Power(s,2)*t2*(-5 + 3*t2) + 
               s*(-2 - 56*t2 + 14*Power(t2,2) + Power(t2,3))) + 
            s2*(-8 + (-3 + 6*s)*t2 + (-47 + 9*s)*Power(t2,2) + 
               (-14 + 5*s)*Power(t2,3) + Power(t2,4) + 
               Power(t1,3)*(-48 - 3*s + t2) + 
               Power(t1,2)*(22 + 75*t2 - 4*Power(t2,2) + 
                  s*(-17 + 7*t2)) + 
               t1*(19 + 85*t2 + 27*Power(t2,2) + 
                  s*(12 + 36*t2 - 9*Power(t2,2))))) + 
         Power(s1,4)*(-4 + 5*Power(t1,4) + 14*t2 + s*t2 + 
            10*Power(t2,2) - 4*s*Power(t2,2) - 
            5*Power(s,2)*Power(t2,2) + 2*Power(t2,3) - 3*s*Power(t2,3) + 
            Power(s,2)*Power(t2,3) + Power(t1,3)*(-8 - 8*s + 2*t2) + 
            Power(s2,2)*(4 + 4*t2 + 2*Power(t2,2) + Power(t2,3) - 
               Power(t1,2)*(1 + t2) + t1*(11 + 2*t2)) + 
            Power(t1,2)*(17 - 11*t2 - 9*Power(t2,2) + 
               s*(42 + 23*t2 - Power(t2,2))) + 
            t1*(28 + 66*t2 - Power(s,2)*(-5 + t2)*t2 + 17*Power(t2,2) - 
               2*Power(t2,3) + 
               s*(-8 - 11*t2 - 18*Power(t2,2) + Power(t2,3))) + 
            s2*(-4 + Power(t1,2)*(-62 + s*(-8 + t2) - 35*t2) + 
               2*(9 + 8*s)*t2 + 31*Power(t2,2) - 2*s*Power(t2,3) + 
               Power(t1,3)*(7 + s + 2*t2) + 
               t1*(7 + 25*t2 + 7*Power(t2,2) - Power(t2,3) + 
                  s*(-2 - 15*t2 + Power(t2,2))))) - 
         Power(s1,2)*(-4 + (5 - 6*s)*Power(t1,4) + t2 + 2*s*t2 - 
            84*Power(t2,2) - 18*s*Power(t2,2) - 
            6*Power(s,2)*Power(t2,2) - 29*Power(t2,3) + 
            44*s*Power(t2,3) - Power(s,2)*Power(t2,3) + 10*Power(t2,4) + 
            Power(t1,3)*(-1 - 2*Power(s,2) + 21*t2 + s*(34 + 7*t2)) + 
            Power(t1,2)*(14 + Power(s,2)*(8 - 3*t2) + 80*t2 - 
               70*Power(t2,2) + s*(5 + 20*t2 - 8*Power(t2,2))) + 
            Power(s2,2)*(Power(t1,2)*(9 + t2) + 
               t1*(-5 + 18*t2 - 13*Power(t2,2)) + 
               t2*(-15 - 13*t2 + Power(t2,2))) + 
            t1*(30 + 9*t2 - 64*Power(t2,2) + 32*Power(t2,3) + 
               2*Power(s,2)*t2*(-1 + 3*t2) + 
               s*(-2 - 82*t2 - 94*Power(t2,2) + 7*Power(t2,3))) + 
            s2*(-6 + (-27 + 7*s)*Power(t1,3) + (3 + 26*s)*t2 + 
               (19 + 36*s)*Power(t2,2) - 13*Power(t2,3) + 
               Power(t1,2)*(3 - 60*t2 + 9*Power(t2,2) - 
                  2*s*(13 + 4*t2)) + 
               t1*(2 + 44*t2 + 126*Power(t2,2) - 9*Power(t2,3) + 
                  s*(7 - 4*t2 + 7*Power(t2,2))))))*T4q(s1))/
     (Power(-1 + s1,2)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       Power(s1*t1 - t2,2)*(-1 + t2)) - 
    (8*(Power(s1,6)*(-Power(s2,2) + 2*s2*t1 + Power(t1,2)) - 
         (-1 + s)*Power(t1,4)*(-2 + 2*s + 3*t2) + 
         t1*(2 - (7*Power(s,2) + 3*(-6 + s2) + s*(-4 + s2))*t2 + 
            (4 - 5*Power(s,2) + 5*s2 - 2*Power(s2,2) + s*(-5 + 7*s2))*
             Power(t2,2) + (-24 + 5*Power(s,2) + s*(9 - 6*s2) - 4*s2 + 
               3*Power(s2,2))*Power(t2,3) + (8*s - 4*(1 + s2))*Power(t2,4)\
) + Power(t1,3)*(6 + (-10 + 3*s2)*t2 - (8 + 3*s2)*Power(t2,2) + 
            Power(s,2)*(2 + 5*t2) - s*(8 + (2 + 3*s2)*t2 - 10*Power(t2,2))\
) + Power(t1,2)*(-6 + 2*Power(s,2)*(2 - 3*t2)*t2 + 21*Power(t2,2) - 
            Power(s2,2)*Power(t2,2) + 13*Power(t2,3) + 
            s2*t2*(-3 + 5*t2 + 5*Power(t2,2)) + 
            s*(4 + 4*(2 + s2)*t2 + (-2 + 5*s2)*Power(t2,2) - 
               13*Power(t2,3))) - 
         t2*(11 + 17*t2 - 11*Power(t2,2) + 3*Power(t2,3) + 
            4*Power(t2,4) + Power(s2,2)*t2*(-3 - t2 + 2*Power(t2,2)) + 
            Power(s,2)*t2*(-3 + t2 + 2*Power(t2,2)) + 
            s2*(-3 + 7*t2 + 9*Power(t2,2) - 3*Power(t2,3) - 
               2*Power(t2,4)) + 
            s*(7 + (-11 + 18*s2)*t2 + 17*Power(t2,2) + 
               (9 - 4*s2)*Power(t2,3) + 2*Power(t2,4))) - 
         Power(s1,5)*(Power(s2,2)*(-3 + 3*t1 - 2*t2) + 
            t1*(2 + s*(-2 + t1) + 2*t1 - 2*Power(t1,2) + 2*t2 + 
               3*t1*t2) + s2*(-5*Power(t1,2) + t1*(13 + s + 5*t2) + 
               2*(-1 + t2 + s*t2))) + 
         Power(s1,4)*(-1 + Power(t1,4) + 2*t2 + Power(t2,2) - 
            Power(s,2)*Power(t2,2) - Power(t1,3)*(11 + 2*s + 3*t2) + 
            Power(t1,2)*(-7 + 4*t2 + Power(t2,2) + s*(5 + 4*t2)) + 
            Power(s2,2)*(1 - 3*Power(t1,2) - 5*t2 + t1*(6 + 5*t2)) + 
            t1*(13 + 9*t2 + Power(s,2)*t2 + 6*Power(t2,2) - 
               s*(7 + 5*t2 + 3*Power(t2,2))) + 
            s2*(-6 + 4*Power(t1,3) + 9*(1 + s)*t2 + 
               (5 + 2*s)*Power(t2,2) + 
               t1*(19 + s*(3 - 5*t2) + 32*t2 + 3*Power(t2,2)) - 
               Power(t1,2)*(3*s + 11*(2 + t2)))) - 
         Power(s1,3)*(-3 + (10 + s)*Power(t1,4) + 11*t2 + s*t2 + 
            7*Power(t2,2) - 4*s*Power(t2,2) - 4*Power(s,2)*Power(t2,2) + 
            3*Power(t2,3) - 3*s*Power(t2,3) + 
            Power(s2,2)*(3 + Power(t1,3) + 10*t2 - Power(t2,2) + 
               2*Power(t2,3) - 4*Power(t1,2)*(1 + t2) + 
               t1*Power(2 + t2,2)) + 
            Power(t1,3)*(14 - 22*t2 + Power(t2,2) - s*(15 + 4*t2)) - 
            Power(t1,2)*(37 + 39*t2 + 5*Power(t2,2) + Power(t2,3) + 
               Power(s,2)*(4 + t2) - s*(12 + 21*t2 + 5*Power(t2,2))) + 
            t1*(16 + 28*t2 + 17*Power(t2,2) + 2*Power(t2,3) + 
               Power(s,2)*t2*(4 + t2) - 
               s*(1 + 21*t2 + 18*Power(t2,2) + 2*Power(t2,3))) - 
            s2*(-2 + Power(t1,4) - (3 + 7*s)*t2 - 
               (37 + 7*s)*Power(t2,2) + (-3 + 2*s)*Power(t2,3) - 
               Power(t1,3)*(12 + 3*s + 8*t2) + 
               Power(t1,2)*(15 + s*(2 - 3*t2) + 42*t2 + 5*Power(t2,2)) + 
               t1*(6 - 15*t2 - 24*Power(t2,2) + Power(t2,3) + 
                  s*(5 + 16*t2 + 3*Power(t2,2))))) + 
         s1*(-3 + 3*(-1 + s)*Power(t1,5) + 
            (8 - s2 - 3*Power(s2,2) + s*(5 + 13*s2))*t2 - 
            (11 + 4*Power(s,2) + s*(3 - 34*s2) - 6*s2 + 10*Power(s2,2))*
             Power(t2,2) + (-31 - 17*s2 - 2*Power(s2,2) + s*(39 + 6*s2))*
             Power(t2,3) + (-Power(s,2) - 15*s2 + s*(18 + s2))*
             Power(t2,4) + (1 - s + s2)*Power(t2,5) + 
            Power(t1,4)*(11 + s2 + 9*t2 + 3*s2*t2 - s*(4 + s2 + 10*t2)) + 
            t1*(19 + 33*t2 + 30*Power(t2,2) + 50*Power(t2,3) + 
               6*Power(t2,4) + 
               Power(s2,2)*t2*(-3 + t2 + 4*Power(t2,2)) + 
               Power(s,2)*t2*(12 + 11*t2 + 5*Power(t2,2)) + 
               s2*(-7 + 16*t2 + 41*Power(t2,2) + 23*Power(t2,3) - 
                  3*Power(t2,4)) + 
               s*(1 + 3*(-9 + 2*s2)*t2 + (1 - 24*s2)*Power(t2,2) - 
                  (33 + 8*s2)*Power(t2,3) + 4*Power(t2,4))) + 
            Power(t1,3)*(3*Power(s,2)*(-2 + t2) - Power(s2,2)*t2 - 
               t2*(13 + 20*t2) - s2*(9 + 4*t2 + 3*Power(t2,2)) + 
               s*(-2 - 15*t2 + 13*Power(t2,2) + 2*s2*(2 + t2))) + 
            Power(t1,2)*(-24 - 37*t2 + Power(s2,2)*(7 - 3*t2)*t2 + 
               Power(t2,2) + 7*Power(t2,3) + 
               Power(s,2)*(4 - 5*t2 - 7*Power(t2,2)) + 
               s2*(15 - 14*t2 - 20*Power(t2,2) + 2*Power(t2,3)) + 
               s*(2 + 19*t2 + 34*Power(t2,2) - 9*Power(t2,3) + 
                  s2*(-3 - 9*t2 + 6*Power(t2,2))))) + 
         Power(s1,2)*(1 - Power(t1,5) + 8*t2 + 3*s*t2 + 35*Power(t2,2) - 
            14*s*Power(t2,2) - 2*Power(s,2)*Power(t2,2) + 11*Power(t2,3) - 
            24*s*Power(t2,3) - Power(s,2)*Power(t2,3) + Power(t2,4) - 
            2*s*Power(t2,4) + Power(s,2)*Power(t2,4) + 
            Power(t1,4)*(-7 + 17*s + 7*t2) + 
            Power(t1,3)*(29 + 6*Power(s,2) + 32*t2 - 3*Power(t2,2) + 
               s*(-6 - 38*t2 + Power(t2,2))) + 
            Power(t1,2)*(-10 - 27*t2 - 69*Power(t2,2) - Power(t2,3) + 
               s*t2*(-3 + 34*t2 - 2*Power(t2,2)) + 
               Power(s,2)*(-8 - 6*t2 + Power(t2,2))) + 
            Power(s2,2)*(Power(t1,3)*(1 + t2) - 
               Power(t1,2)*(6 + t2 + Power(t2,2)) + 
               t2*(12 + 13*t2 + Power(t2,2) + Power(t2,3)) - 
               t1*(-5 + 6*Power(t2,2) + Power(t2,3))) + 
            t1*(-12 - 20*t2 + Power(t2,2) - 2*Power(t2,3) - 
               2*Power(t2,4) + Power(s,2)*t2*(-2 + t2 - 2*Power(t2,2)) + 
               s*(3 + 11*t2 - 16*Power(t2,2) - 11*Power(t2,3) + 
                  Power(t2,4))) - 
            s2*(-6 + (-2 + 13*s)*t2 + (-25 + 9*s)*Power(t2,2) + 
               3*(-13 + s)*Power(t2,3) + (1 + 2*s)*Power(t2,4) + 
               Power(t1,4)*(3 + s + 2*t2) + 
               Power(t1,3)*(-9 + 4*s - 21*t2 - 2*Power(t2,2)) - 
               Power(t1,2)*(3 - 12*t2 - 16*Power(t2,2) + Power(t2,3) + 
                  s*(6 + 19*t2)) + 
               t1*(15 + 33*t2 + 29*Power(t2,2) - 8*Power(t2,3) + 
                  Power(t2,4) - 
                  s*(-7 - 20*t2 + Power(t2,2) + 3*Power(t2,3))))))*
       T5q(1 - s1 - t1 + t2))/
     ((-1 + s1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*
       (-1 + t2)));
   return a;
};
