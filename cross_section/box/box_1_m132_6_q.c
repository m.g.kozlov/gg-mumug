#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_1_m132_6_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(-24*s2 - 8*Power(s2,2) - 8*Power(s2,3) - 38*Power(s2,4) - 
         14*Power(s2,5) + 24*t1 + 52*s2*t1 - 15*Power(s2,2)*t1 + 
         75*Power(s2,3)*t1 + 54*Power(s2,4)*t1 - 44*Power(t1,2) + 
         42*s2*Power(t1,2) + 10*Power(s2,2)*Power(t1,2) - 
         59*Power(s2,3)*Power(t1,2) - 19*Power(t1,3) - 93*s2*Power(t1,3) - 
         7*Power(s2,2)*Power(t1,3) + 46*Power(t1,4) + 45*s2*Power(t1,4) - 
         19*Power(t1,5) - 4*Power(s1,5)*
          (2*Power(s2,3) + Power(s2,2)*(1 - 5*t1) + t1 - Power(t1,3) + 
            s2*(-1 - t1 + 4*Power(t1,2))) - 8*s2*t2 + 22*Power(s2,2)*t2 + 
         62*Power(s2,3)*t2 + 52*Power(s2,4)*t2 + 16*Power(s2,5)*t2 + 
         8*t1*t2 - 125*s2*t1*t2 - 216*Power(s2,2)*t1*t2 - 
         183*Power(s2,3)*t1*t2 - 63*Power(s2,4)*t1*t2 + 
         103*Power(t1,2)*t2 + 271*s2*Power(t1,2)*t2 + 
         241*Power(s2,2)*Power(t1,2)*t2 + 74*Power(s2,3)*Power(t1,2)*t2 - 
         117*Power(t1,3)*t2 - 141*s2*Power(t1,3)*t2 - 
         4*Power(s2,2)*Power(t1,3)*t2 + 31*Power(t1,4)*t2 - 
         42*s2*Power(t1,4)*t2 + 19*Power(t1,5)*t2 - 24*Power(t2,2) + 
         108*s2*Power(t2,2) + 118*Power(s2,2)*Power(t2,2) - 
         12*Power(s2,3)*Power(t2,2) - 12*Power(s2,4)*Power(t2,2) - 
         4*Power(s2,5)*Power(t2,2) - 108*t1*Power(t2,2) - 
         213*s2*t1*Power(t2,2) + 74*Power(s2,2)*t1*Power(t2,2) + 
         110*Power(s2,3)*t1*Power(t2,2) + 15*Power(s2,4)*t1*Power(t2,2) + 
         95*Power(t1,2)*Power(t2,2) - 119*s2*Power(t1,2)*Power(t2,2) - 
         267*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         21*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         57*Power(t1,3)*Power(t2,2) + 252*s2*Power(t1,3)*Power(t2,2) + 
         13*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         83*Power(t1,4)*Power(t2,2) - 3*s2*Power(t1,4)*Power(t2,2) + 
         72*Power(t2,3) - 36*s2*Power(t2,3) - 
         148*Power(s2,2)*Power(t2,3) - 52*Power(s2,3)*Power(t2,3) - 
         2*Power(s2,4)*Power(t2,3) + 2*Power(s2,5)*Power(t2,3) + 
         36*t1*Power(t2,3) + 311*s2*t1*Power(t2,3) + 
         186*Power(s2,2)*t1*Power(t2,3) - 6*Power(s2,3)*t1*Power(t2,3) - 
         6*Power(s2,4)*t1*Power(t2,3) - 163*Power(t1,2)*Power(t2,3) - 
         225*s2*Power(t1,2)*Power(t2,3) + 
         26*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         6*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         91*Power(t1,3)*Power(t2,3) - 26*s2*Power(t1,3)*Power(t2,3) - 
         2*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         8*Power(t1,4)*Power(t2,3) - 72*Power(t2,4) - 70*s2*Power(t2,4) + 
         12*Power(s2,2)*Power(t2,4) + 14*Power(s2,3)*Power(t2,4) + 
         2*Power(s2,4)*Power(t2,4) + 70*t1*Power(t2,4) - 
         15*s2*t1*Power(t2,4) - 41*Power(s2,2)*t1*Power(t2,4) - 
         4*Power(s2,3)*t1*Power(t2,4) + 3*Power(t1,2)*Power(t2,4) + 
         43*s2*Power(t1,2)*Power(t2,4) + 
         2*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         16*Power(t1,3)*Power(t2,4) + 24*Power(t2,5) + 30*s2*Power(t2,5) + 
         6*Power(s2,2)*Power(t2,5) - 30*t1*Power(t2,5) - 
         14*s2*t1*Power(t2,5) + 8*Power(t1,2)*Power(t2,5) + 
         Power(s,5)*(12*t1 + 8*Power(t1,2) - Power(t1,3) + 
            Power(s1,2)*(4 + 3*t1 - 7*t2) + Power(s2,2)*(4 + t1 - 5*t2) - 
            12*t2 - 16*t1*t2 - Power(t1,2)*t2 + 8*Power(t2,2) + 
            5*t1*Power(t2,2) - 3*Power(t2,3) + 
            2*s2*(t1 - t2)*(-9 + 4*t2) + 
            s1*(8*s2*(-1 + t1) - 6*t1 - 4*Power(t1,2) + 6*t2 + 
               4*Power(t2,2))) + 
         Power(s1,4)*(5*Power(s2,4) + 11*Power(t1,4) + 
            8*Power(-1 + t2,2) + 4*t1*(2 + t2) - 
            Power(t1,3)*(32 + 9*t2) + Power(s2,3)*(24 - 30*t1 + 25*t2) + 
            Power(t1,2)*(-3 + 25*t2 - 8*Power(t2,2)) + 
            Power(s2,2)*(-1 + 56*Power(t1,2) + 39*t2 - 8*Power(t2,2) - 
               t1*(84 + 59*t2)) + 
            s2*(-42*Power(t1,3) - 4*(2 + t2) + Power(t1,2)*(92 + 43*t2) + 
               4*t1*(1 - 16*t2 + 4*Power(t2,2)))) + 
         Power(s,4)*(-12 - 60*t1 + 18*Power(t1,2) + 14*Power(t1,3) - 
            2*Power(t1,4) - Power(s1,3)*(18 + 5*s2 + 2*t1 - 25*t2) + 
            60*t2 + 24*t1*t2 - 6*Power(t1,2)*t2 - 3*Power(t1,3)*t2 - 
            42*Power(t2,2) - 30*t1*Power(t2,2) + 
            5*Power(t1,2)*Power(t2,2) + 22*Power(t2,3) + 
            7*t1*Power(t2,3) - 7*Power(t2,4) + 
            Power(s2,3)*(-8 - 2*t1 + 11*t2) + 
            Power(s2,2)*(-19 + 2*Power(t1,2) + t1*(38 - 23*t2) - 22*t2 + 
               5*Power(t2,2)) + 
            Power(s1,2)*(-1 + 4*Power(s2,2) + 25*Power(t1,2) + 38*t2 - 
               46*Power(t2,2) + t1*(-22 + 5*t2) + s2*(34 - 43*t1 + 6*t2)) \
+ s2*(30 + 2*Power(t1,3) - 19*t2 + 38*Power(t2,2) - 13*Power(t2,3) + 
               Power(t1,2)*(-44 + 15*t2) + t1*(19 + 6*t2 - 4*Power(t2,2))\
) + s1*(6 + Power(s2,3) - 5*Power(t1,3) - 29*t2 - 24*Power(t2,2) + 
               28*Power(t2,3) + Power(t1,2)*(-62 + 6*t2) + 
               Power(s2,2)*(4 - 25*t1 + 30*t2) + 
               t1*(29 + 86*t2 - 29*Power(t2,2)) + 
               s2*(-16 + 29*Power(t1,2) - 34*t1*(-2 + t2) - 100*t2 + 
                  37*Power(t2,2)))) + 
         Power(s1,3)*(3*Power(s2,5) + Power(t1,5) - 
            8*Power(-1 + t2,2)*(2 + 3*t2) + 
            Power(s2,4)*(-27 - 19*t1 + 4*t2) - Power(t1,4)*(19 + 13*t2) + 
            Power(t1,3)*(-51 + 75*t2 - 2*Power(t2,2)) + 
            t1*(57 - 124*t2 + 15*Power(t2,2) + 8*Power(t2,3)) + 
            Power(t1,2)*(-6 + 99*t2 - 59*Power(t2,2) + 16*Power(t2,3)) + 
            Power(s2,3)*(10 + 38*Power(t1,2) - 65*t2 - 19*Power(t2,2) + 
               t1*(96 + 7*t2)) - 
            Power(s2,2)*(17 + 30*Power(t1,3) - 67*t2 + 72*Power(t2,2) - 
               16*Power(t2,3) + 13*Power(t1,2)*(10 + 3*t2) + 
               t1*(63 - 209*t2 - 36*Power(t2,2))) + 
            s2*(-57 + 7*Power(t1,4) + 124*t2 - 15*Power(t2,2) - 
               8*Power(t2,3) + Power(t1,3)*(80 + 41*t2) + 
               Power(t1,2)*(104 - 219*t2 - 15*Power(t2,2)) + 
               t1*(23 - 166*t2 + 131*Power(t2,2) - 32*Power(t2,3)))) - 
         s1*(Power(t1,5)*(7 + 12*t2) + 
            Power(t1,4)*(39 - 139*t2 - 9*Power(t2,2)) + 
            8*Power(-1 + t2,2)*t2*(-6 + 8*t2 + Power(t2,2)) + 
            Power(s2,5)*(-2 + 11*t2 + 5*Power(t2,2)) - 
            Power(t1,3)*(6 + 59*t2 - 289*Power(t2,2) + 20*Power(t2,3)) + 
            Power(t1,2)*(-42 + 242*t2 - 269*Power(t2,2) - 
               102*Power(t2,3) + 25*Power(t2,4)) - 
            t1*(-44 + 129*t2 + 43*Power(t2,2) - 231*Power(t2,3) + 
               63*Power(t2,4) + 8*Power(t2,5)) + 
            Power(s2,3)*(-14 + 67*t2 - 154*Power(t2,2) + 
               40*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(-31 + 81*t2) + 
               t1*(-74 + 132*t2 - 63*Power(t2,2))) + 
            Power(s2,4)*(11 + 2*t2 + 17*Power(t2,2) + 2*Power(t2,3) + 
               t1*(12 - 53*t2 - 10*Power(t2,2))) + 
            Power(s2,2)*(-63 + 221*t2 - 245*Power(t2,2) - 
               41*Power(t2,3) + 18*Power(t2,4) + 
               Power(t1,3)*(41 - 35*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(154 - 409*t2 + 66*Power(t2,2) - 
                  6*Power(t2,3)) + 
               t1*(23 - 165*t2 + 554*Power(t2,2) - 90*Power(t2,3) - 
                  2*Power(t2,4))) + 
            s2*(-44 + 129*t2 + 43*Power(t2,2) - 231*Power(t2,3) + 
               63*Power(t2,4) + 8*Power(t2,5) - 
               Power(t1,4)*(27 + 16*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(-130 + 414*t2 - 11*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(105 - 463*t2 + 514*Power(t2,2) + 143*Power(t2,3) - 
                  43*Power(t2,4)) + 
               Power(t1,2)*(-3 + 157*t2 - 689*Power(t2,2) + 
                  70*Power(t2,3) + Power(t2,4)))) + 
         Power(s1,2)*(19*Power(s2,5) - Power(t1,5)*(-13 + t2) + 
            Power(t1,4)*(-113 + 11*t2 + 2*Power(t2,2)) + 
            8*Power(-1 + t2,2)*(-3 + 7*t2 + 3*Power(t2,2)) + 
            Power(t1,3)*(8 + 271*t2 - 55*Power(t2,2) + 7*Power(t2,3)) + 
            Power(t1,2)*(81 - 143*t2 - 193*Power(t2,2) + 
               51*Power(t2,3) - 8*Power(t2,4)) - 
            t1*(21 + 124*t2 - 277*Power(t2,2) + 48*Power(t2,3) + 
               16*Power(t2,4)) + 
            Power(s2,4)*(-3 + 58*t2 - 9*Power(t2,2) + 
               5*t1*(-17 + 3*t2)) + 
            Power(s2,3)*(2 + Power(t1,2)*(128 - 44*t2) - 119*t2 + 
               75*Power(t2,2) + 3*Power(t2,3) + 
               9*t1*(13 - 22*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(49 - 111*t2 - 111*Power(t2,2) + 
               49*Power(t2,3) - 8*Power(t2,4) + 
               Power(t1,3)*(-64 + 42*t2) + 
               Power(t1,2)*(-338 + 233*t2 - 25*Power(t2,2)) + 
               t1*(27 + 467*t2 - 198*Power(t2,2) + Power(t2,3))) + 
            s2*(21 + 124*t2 - 277*Power(t2,2) + 48*Power(t2,3) + 
               16*Power(t2,4) - Power(t1,4)*(11 + 12*t2) + 
               Power(t1,3)*(337 - 104*t2 + 5*Power(t2,2)) - 
               Power(t1,2)*(37 + 619*t2 - 178*Power(t2,2) + 
                  11*Power(t2,3)) + 
               2*t1*(-65 + 127*t2 + 152*Power(t2,2) - 50*Power(t2,3) + 
                  8*Power(t2,4)))) + 
         Power(s,3)*(48 + 67*t1 - 137*Power(t1,2) + 15*Power(t1,3) + 
            3*Power(t1,4) - Power(t1,5) + 
            Power(s1,4)*(18 + 13*s2 - 5*t1 - 26*t2) + 
            Power(s2,4)*(4 + t1 - 7*t2) - 115*t2 + 38*t1*t2 + 
            5*Power(t1,2)*t2 + 48*Power(t1,3)*t2 - 3*Power(t1,4)*t2 + 
            99*Power(t2,2) + 65*t1*Power(t2,2) - 
            94*Power(t1,2)*Power(t2,2) - 5*Power(t1,3)*Power(t2,2) - 
            85*Power(t2,3) + 32*t1*Power(t2,3) + 
            19*Power(t1,2)*Power(t2,3) + 11*Power(t2,4) - 
            6*t1*Power(t2,4) - 4*Power(t2,5) + 
            Power(s1,3)*(33 + Power(s2,2) - 30*Power(t1,2) + 
               s2*(-41 + 43*t1 - 40*t2) - 111*t2 + 85*Power(t2,2) + 
               4*t1*(11 + 4*t2)) + 
            Power(s2,3)*(62 - 2*Power(t1,2) - 23*t2 + 17*Power(t2,2) + 
               2*t1*(-8 + 7*t2)) + 
            Power(s2,2)*(11 + Power(t1,2)*(23 - 10*t2) + 52*t2 - 
               119*Power(t2,2) + 31*Power(t2,3) + 
               t1*(-154 + 124*t2 - 45*Power(t2,2))) + 
            s2*(-79 + 2*Power(t1,4) - 4*t2 - 47*Power(t2,2) - 
               21*Power(t2,3) + 3*Power(t2,4) + 
               2*Power(t1,3)*(-7 + 3*t2) + 
               Power(t1,2)*(77 - 149*t2 + 33*Power(t2,2)) - 
               2*t1*(-60 + 15*t2 - 92*Power(t2,2) + 22*Power(t2,3))) + 
            Power(s1,2)*(5 - 12*Power(s2,3) + 38*Power(t1,3) - 47*t2 + 
               114*Power(t2,2) - 88*Power(t2,3) + 
               Power(t1,2)*(16 + 15*t2) + 
               t1*(13 - 102*t2 + 11*Power(t2,2)) + 
               Power(s2,2)*(106*t1 - 9*(2 + 5*t2)) + 
               s2*(-8 - 132*Power(t1,2) + 111*t2 + Power(t2,2) + 
                  2*t1*(2 + 9*t2))) + 
            s1*(-17 - 2*Power(s2,4) + 2*Power(t1,4) + 
               Power(s2,3)*(-11 + 28*t1 - 55*t2) - 14*t2 + 
               93*Power(t2,2) - 32*Power(t2,3) + 33*Power(t2,4) + 
               Power(t1,3)*(-115 + 6*t2) + 
               Power(t1,2)*(157 + 114*t2 - 33*Power(t2,2)) + 
               t1*(42 - 250*t2 + 33*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s2,2)*(9 - 48*Power(t1,2) + 121*t2 - 
                  31*Power(t2,2) + t1*(-82 + 92*t2)) + 
               s2*(-16 + 20*Power(t1,3) + Power(t1,2)*(208 - 43*t2) + 
                  203*t2 - 80*Power(t2,2) + 15*Power(t2,3) + 
                  t1*(-211 - 184*t2 + 56*Power(t2,2))))) + 
         Power(s,2)*(-60 + 37*t1 + 147*Power(t1,2) - 119*Power(t1,3) + 
            32*Power(t1,4) - 3*Power(t1,5) + 107*t2 + Power(s2,5)*t2 - 
            101*t1*t2 + 59*Power(t1,2)*t2 - 140*Power(t1,3)*t2 + 
            34*Power(t1,4)*t2 - Power(t1,5)*t2 - 118*Power(t2,2) - 
            109*t1*Power(t2,2) + 242*Power(t1,2)*Power(t2,2) - 
            12*Power(t1,3)*Power(t2,2) - 5*Power(t1,4)*Power(t2,2) + 
            169*Power(t2,3) - 72*t1*Power(t2,3) - 
            70*Power(t1,2)*Power(t2,3) + 5*Power(t1,3)*Power(t2,3) - 
            62*Power(t2,4) + 55*t1*Power(t2,4) + 
            9*Power(t1,2)*Power(t2,4) - 4*Power(t2,5) - 
            8*t1*Power(t2,5) + Power(s1,5)*(-4 - 8*s2 + 4*t1 + 8*t2) + 
            Power(s2,4)*(-57 + t1*(-4 + t2) + 30*t2 - 17*Power(t2,2)) + 
            Power(s1,4)*(-30 - 21*Power(s2,2) + Power(t1,2) + 66*t2 - 
               40*Power(t2,2) - t1*(12 + 29*t2) + s2*(4 + 16*t1 + 45*t2)) \
+ Power(s2,3)*(-136 + Power(t1,2)*(15 - 8*t2) + 63*t2 + 48*Power(t2,2) - 
               9*Power(t2,3) + 2*t1*(92 - 65*t2 + 25*Power(t2,2))) + 
            Power(s2,2)*(43 - 35*t2 + 256*Power(t2,2) - 86*Power(t2,3) + 
               17*Power(t2,4) + Power(t1,3)*(-21 + 8*t2) - 
               3*Power(t1,2)*(55 - 68*t2 + 18*Power(t2,2)) + 
               t1*(189 - 311*t2 - 69*Power(t2,2) + 13*Power(t2,3))) + 
            s2*(29 - Power(t1,4)*(-13 + t2) - 21*t2 + 108*Power(t2,2) + 
               86*Power(t2,3) - 48*Power(t2,4) + 8*Power(t2,5) + 
               2*Power(t1,3)*(3 - 69*t2 + 13*Power(t2,2)) + 
               Power(t1,2)*(66 + 388*t2 + 33*Power(t2,2) - 
                  9*Power(t2,3)) - 
               2*t1*(104 + 3*t2 + 240*Power(t2,2) - 70*Power(t2,3) + 
                  12*Power(t2,4))) + 
            Power(s1,3)*(16*Power(s2,3) - 53*Power(t1,3) + 
               Power(t1,2)*(100 - 8*t2) + 
               Power(s2,2)*(67 - 121*t1 + 57*t2) + 
               t1*(-44 - 45*t2 + 72*Power(t2,2)) + 
               2*(-17 + 65*t2 - 65*Power(t2,2) + 32*Power(t2,3)) + 
               s2*(33 + 158*Power(t1,2) + 49*t2 - 93*Power(t2,2) - 
                  t1*(175 + 43*t2))) + 
            Power(s1,2)*(-36 + 12*Power(s2,4) + 13*Power(t1,4) + 
               123*t2 - 145*Power(t2,2) + 58*Power(t2,3) - 
               40*Power(t2,4) + 6*Power(t1,3)*(19 + 3*t2) + 
               Power(s2,3)*(-27 - 95*t1 + 68*t2) + 
               Power(s2,2)*(-43 + 167*Power(t1,2) + t1*(135 - 78*t2) - 
                  165*t2 + 31*Power(t2,2)) + 
               Power(t1,2)*(-97 - 242*t2 + 66*Power(t2,2)) + 
               t1*(9 + 118*t2 + 98*Power(t2,2) - 73*Power(t2,3)) - 
               s2*(62 + 97*Power(t1,3) + 94*t2 + 51*Power(t2,2) - 
                  83*Power(t2,3) + Power(t1,2)*(222 + 8*t2) + 
                  t1*(-177 - 380*t2 + 95*Power(t2,2)))) + 
            s1*(7 + Power(s2,5) + 3*Power(t1,5) + 118*t2 - 
               276*Power(t2,2) + 107*Power(t2,3) + 14*Power(t2,4) + 
               8*Power(t2,5) + Power(t1,4)*(-69 + 2*t2) + 
               Power(s2,4)*(32 - 11*t1 + 28*t2) + 
               Power(t1,3)*(243 - 30*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(32 - 417*t2 + 197*Power(t2,2) - 
                  52*Power(t2,3)) + 
               t1*(-177 + 292*t2 + 67*Power(t2,2) - 112*Power(t2,3) + 
                  34*Power(t2,4)) + 
               Power(s2,3)*(25 + 24*Power(t1,2) + t2 - 38*Power(t2,2) - 
                  t1*(69 + 44*t2)) + 
               Power(s2,2)*(76 - 16*Power(t1,3) - 371*t2 + 
                  217*Power(t2,2) - 68*Power(t2,3) + 
                  3*Power(t1,2)*(-9 + 2*t2) + 
                  t1*(243 - 83*t2 + 87*Power(t2,2))) - 
               s2*(-173 + Power(t1,4) + 208*t2 + 64*Power(t2,2) - 
                  62*Power(t2,3) + 35*Power(t2,4) - 
                  Power(t1,3)*(133 + 8*t2) + 
                  Power(t1,2)*(511 - 112*t2 + 54*Power(t2,2)) + 
                  t1*(78 - 703*t2 + 363*Power(t2,2) - 114*Power(t2,3))))) \
+ s*(24 - 80*t1 + 10*Power(t1,2) + 114*Power(t1,3) - 77*Power(t1,4) + 
            23*Power(t1,5) - 40*t2 + 58*t1*t2 - 167*Power(t1,2)*t2 + 
            176*Power(t1,3)*t2 - 74*Power(t1,4)*t2 - 4*Power(t1,5)*t2 + 
            76*Power(t2,2) + 177*t1*Power(t2,2) - 
            223*Power(t1,2)*Power(t2,2) - 12*Power(t1,3)*Power(t2,2) + 
            44*Power(t1,4)*Power(t2,2) - 172*Power(t2,3) - 
            2*t1*Power(t2,3) + 200*Power(t1,2)*Power(t2,3) - 
            72*Power(t1,3)*Power(t2,3) - 4*Power(t1,4)*Power(t2,3) + 
            126*Power(t2,4) - 123*t1*Power(t2,4) + 
            28*Power(t1,2)*Power(t2,4) + 8*Power(t1,3)*Power(t2,4) - 
            14*Power(t2,5) + 4*t1*Power(t2,5) - 
            4*Power(t1,2)*Power(t2,5) + 
            Power(s2,5)*(14 - 3*t2 + 3*Power(t2,2)) + 
            4*Power(s1,5)*(1 + 4*Power(s2,2) + 2*Power(t1,2) - 2*t2 - 
               2*s2*(-1 + 3*t1 + t2) + t1*(-1 + 2*t2)) + 
            Power(s2,4)*(93 - 84*t2 + 29*Power(t2,2) - 8*Power(t2,3) + 
               t1*(-51 + 6*t2 - 6*Power(t2,2))) + 
            Power(s2,3)*(86 - 137*t2 - 91*Power(t2,2) + 87*Power(t2,3) - 
               15*Power(t2,4) + Power(t1,2)*(46 + 4*t2) + 
               t1*(-249 + 362*t2 - 140*Power(t2,2) + 30*Power(t2,3))) + 
            Power(s2,2)*(-29 + 10*t2 - 267*Power(t2,2) + 
               147*Power(t2,3) + 23*Power(t2,4) - 4*Power(t2,5) + 
               2*Power(t1,3)*(14 - 9*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(142 - 546*t2 + 237*Power(t2,2) - 
                  40*Power(t2,3)) + 
               t1*(-47 + 428*t2 + 161*Power(t2,2) - 226*Power(t2,3) + 
                  34*Power(t2,4))) + 
            s2*(44 + 23*t2 - 200*Power(t2,2) - 13*Power(t2,3) + 
               114*Power(t2,4) - 2*Power(t2,5) - 
               3*Power(t1,4)*(20 - 5*t2 + Power(t2,2)) + 
               Power(t1,3)*(91 + 342*t2 - 170*Power(t2,2) + 
                  22*Power(t2,3)) - 
               Power(t1,2)*(153 + 467*t2 + 58*Power(t2,2) - 
                  211*Power(t2,3) + 27*Power(t2,4)) + 
               t1*(31 + 132*t2 + 497*Power(t2,2) - 338*Power(t2,3) - 
                  54*Power(t2,4) + 8*Power(t2,5))) + 
            Power(s1,4)*(3*Power(s2,3) + 17*Power(t1,3) + 
               Power(s2,2)*(-46 + 19*t1 - 44*t2) - 
               2*Power(t1,2)*(31 + 6*t2) + 4*t2*(-5 + 8*t2) + 
               t1*(-1 + 59*t2 - 48*Power(t2,2)) + 
               s2*(-1 - 39*Power(t1,2) - 73*t2 + 48*Power(t2,2) + 
                  56*t1*(2 + t2))) - 
            Power(s1,3)*(-33 + 15*Power(s2,4) + 24*Power(t1,4) + 36*t2 + 
               Power(t2,2) + 40*Power(t2,3) + Power(t1,3)*(-19 + 12*t2) + 
               Power(s2,3)*(-19 - 99*t1 + 46*t2) + 
               Power(s2,2)*(28 + 177*Power(t1,2) + t1*(7 - 68*t2) - 
                  79*t2 - 27*Power(t2,2)) + 
               Power(t1,2)*(80 - 93*t2 + 15*Power(t2,2)) + 
               t1*(80 - 173*t2 + 93*Power(t2,2) - 80*Power(t2,3)) + 
               s2*(-91 - 117*Power(t1,3) + 141*t2 - 106*Power(t2,2) + 
                  80*Power(t2,3) + Power(t1,2)*(31 + 10*t2) + 
                  4*t1*(-25 + 44*t2 + 3*Power(t2,2)))) + 
            Power(s1,2)*(51 - 4*Power(s2,5) - 3*Power(t1,5) + 
               Power(s2,4)*(-12 + 29*t1 - 22*t2) - 204*t2 + 
               165*Power(t2,2) + 40*Power(t2,3) + 16*Power(t2,4) + 
               Power(t1,4)*(85 + 14*t2) + 
               Power(t1,3)*(-192 - 123*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(-60 + 412*t2 + 25*Power(t2,2) + 
                  22*Power(t2,3)) + 
               t1*(21 + 172*t2 - 410*Power(t2,2) + 13*Power(t2,3) - 
                  48*Power(t2,4)) + 
               Power(s2,3)*(23 - 60*Power(t1,2) - 10*t2 + 
                  23*Power(t2,2) + t1*(4 + 8*t2)) + 
               Power(s2,2)*(-17 + 46*Power(t1,3) + 236*t2 - 
                  42*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t1,2)*(113 + 64*t2) - 
                  3*t1*(90 + 21*t2 + 13*Power(t2,2))) + 
               s2*(11 - 8*Power(t1,4) - 204*t2 + 328*Power(t2,2) - 
                  11*Power(t2,3) + 48*Power(t2,4) - 
                  2*Power(t1,3)*(95 + 32*t2) + 
                  Power(t1,2)*(439 + 196*t2 + 5*Power(t2,2)) + 
                  t1*(54 - 606*t2 + 10*Power(t2,2) - 24*Power(t2,3)))) + 
            s1*(4 + 2*Power(t1,5)*(-5 + t2) - 127*t2 + 355*Power(t2,2) - 
               255*Power(t2,3) - 9*Power(t2,4) - Power(s2,5)*(17 + 3*t2) + 
               Power(t1,4)*(116 - 84*t2 + 5*Power(t2,2)) + 
               Power(s2,4)*(-12 + t1*(79 - 6*t2) - 25*t2 + 
                  33*Power(t2,2)) - 
               Power(t1,3)*(99 + 33*t2 - 181*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(-162 + 549*t2 - 443*Power(t2,2) - 
                  108*Power(t2,3) + Power(t2,4)) + 
               t1*(145 - 260*t2 - 159*Power(t2,2) + 369*Power(t2,3) + 
                  21*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,3)*(1 + 175*t2 - 128*Power(t2,2) + 
                  27*Power(t2,3) + Power(t1,2)*(-125 + 34*t2) - 
                  2*t1*(44 - 69*t2 + 46*Power(t2,2))) - 
               Power(s2,2)*(178 - 473*t2 + 311*Power(t2,2) + 
                  38*Power(t2,3) - 3*Power(t2,4) + 
                  Power(t1,3)*(-71 + 36*t2) + 
                  Power(t1,2)*(-328 + 285*t2 - 90*Power(t2,2)) + 
                  t1*(133 + 299*t2 - 365*Power(t2,2) + 58*Power(t2,3))) + 
               s2*(-166 + 239*t2 + 183*Power(t2,2) - 308*Power(t2,3) - 
                  28*Power(t2,4) - 8*Power(t2,5) + 
                  Power(t1,4)*(2 + 9*t2) - 
                  4*Power(t1,3)*(86 - 64*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*
                   (231 + 157*t2 - 418*Power(t2,2) + 47*Power(t2,3)) + 
                  t1*(341 - 994*t2 + 711*Power(t2,2) + 156*Power(t2,3) - 
                     4*Power(t2,4)))))))/
     ((-1 + s1)*(-1 + s - s2 + t1)*(s - s2 + t1)*(-s + s1 - t2)*
       (1 - s + s1 - t2)*Power(s1 - s2 + t1 - t2,2)*(-1 + t2)*
       (-1 + s2 - t1 + t2)) - (8*
       (6 + 16*s2 - 16*Power(s2,2) - 5*Power(s2,3) + 3*Power(s2,4) - 
         4*Power(s1,5)*Power(s2 - t1,3) + 55*s2*t1 - 24*Power(s2,2)*t1 - 
         23*Power(s2,3)*t1 + 3*Power(s2,4)*t1 - 37*Power(t1,2) + 
         58*s2*Power(t1,2) + 50*Power(s2,2)*Power(t1,2) - 
         6*Power(s2,3)*Power(t1,2) + 2*Power(s2,4)*Power(t1,2) - 
         29*Power(t1,3) - 43*s2*Power(t1,3) - 8*Power(s2,3)*Power(t1,3) + 
         13*Power(t1,4) + 6*s2*Power(t1,4) + 12*Power(s2,2)*Power(t1,4) - 
         3*Power(t1,5) - 8*s2*Power(t1,5) + 2*Power(t1,6) - 
         Power(s,6)*(t1 - t2)*(-1 + s1 + t1 - t2)*(1 - s2 + t1 - t2) + 
         2*t2 - 95*s2*t2 + 6*Power(s2,2)*t2 + 59*Power(s2,3)*t2 + 
         6*Power(s2,4)*t2 - 3*Power(s2,5)*t2 + 53*t1*t2 - 137*s2*t1*t2 - 
         81*Power(s2,2)*t1*t2 + 13*Power(s2,3)*t1*t2 - 
         3*Power(s2,4)*t1*t2 - 4*Power(s2,5)*t1*t2 + 119*Power(t1,2)*t2 + 
         s2*Power(t1,2)*t2 - 58*Power(s2,2)*Power(t1,2)*t2 + 
         31*Power(s2,3)*Power(t1,2)*t2 + 15*Power(s2,4)*Power(t1,2)*t2 + 
         21*Power(t1,3)*t2 + 53*s2*Power(t1,3)*t2 - 
         45*Power(s2,2)*Power(t1,3)*t2 - 20*Power(s2,3)*Power(t1,3)*t2 - 
         14*Power(t1,4)*t2 + 24*s2*Power(t1,4)*t2 + 
         10*Power(s2,2)*Power(t1,4)*t2 - 4*Power(t1,5)*t2 - 
         Power(t1,6)*t2 - 48*Power(t2,2) + 146*s2*Power(t2,2) + 
         102*Power(s2,2)*Power(t2,2) - 75*Power(s2,3)*Power(t2,2) - 
         24*Power(s2,4)*Power(t2,2) + 9*Power(s2,5)*Power(t2,2) + 
         2*Power(s2,6)*Power(t2,2) - 122*t1*Power(t2,2) + 
         18*s2*t1*Power(t2,2) + 158*Power(s2,2)*t1*Power(t2,2) + 
         26*Power(s2,3)*t1*Power(t2,2) - 32*Power(s2,4)*t1*Power(t2,2) - 
         6*Power(s2,5)*t1*Power(t2,2) - 96*Power(t1,2)*Power(t2,2) - 
         101*s2*Power(t1,2)*Power(t2,2) + 
         33*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         41*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         4*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         18*Power(t1,3)*Power(t2,2) - 48*s2*Power(t1,3)*Power(t2,2) - 
         21*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         4*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         13*Power(t1,4)*Power(t2,2) + 2*s2*Power(t1,4)*Power(t2,2) - 
         6*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         Power(t1,5)*Power(t2,2) + 2*s2*Power(t1,5)*Power(t2,2) + 
         72*Power(t2,3) - 60*s2*Power(t2,3) - 
         138*Power(s2,2)*Power(t2,3) + 10*Power(s2,3)*Power(t2,3) + 
         30*Power(s2,4)*Power(t2,3) + Power(s2,5)*Power(t2,3) - 
         Power(s2,6)*Power(t2,3) + 80*t1*Power(t2,3) + 
         114*s2*t1*Power(t2,3) - 45*Power(s2,2)*t1*Power(t2,3) - 
         71*Power(s2,3)*t1*Power(t2,3) - Power(s2,4)*t1*Power(t2,3) + 
         4*Power(s2,5)*t1*Power(t2,3) + 4*Power(t1,2)*Power(t2,3) + 
         52*s2*Power(t1,2)*Power(t2,3) + 
         52*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         3*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         6*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         17*Power(t1,3)*Power(t2,3) - 11*s2*Power(t1,3)*Power(t2,3) + 
         5*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         4*Power(s2,3)*Power(t1,3)*Power(t2,3) - 
         2*s2*Power(t1,4)*Power(t2,3) - 
         Power(s2,2)*Power(t1,4)*Power(t2,3) - 38*Power(t2,4) - 
         18*s2*Power(t2,4) + 46*Power(s2,2)*Power(t2,4) + 
         20*Power(s2,3)*Power(t2,4) - 3*Power(s2,4)*Power(t2,4) - 
         Power(s2,5)*Power(t2,4) - 6*t1*Power(t2,4) - 
         49*s2*t1*Power(t2,4) - 34*Power(s2,2)*t1*Power(t2,4) + 
         7*Power(s2,3)*t1*Power(t2,4) + 3*Power(s2,4)*t1*Power(t2,4) + 
         9*Power(t1,2)*Power(t2,4) + 15*s2*Power(t1,2)*Power(t2,4) - 
         5*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         3*Power(s2,3)*Power(t1,2)*Power(t2,4) - 
         Power(t1,3)*Power(t2,4) + s2*Power(t1,3)*Power(t2,4) + 
         Power(s2,2)*Power(t1,3)*Power(t2,4) + 6*Power(t2,5) + 
         11*s2*Power(t2,5) - Power(s2,3)*Power(t2,5) - 5*t1*Power(t2,5) - 
         s2*t1*Power(t2,5) + 2*Power(s2,2)*t1*Power(t2,5) + 
         Power(t1,2)*Power(t2,5) - s2*Power(t1,2)*Power(t2,5) - 
         Power(s1,4)*Power(s2 - t1,2)*
          (Power(s2,2)*(1 + t2) + Power(t1,2)*(1 + t2) - 
            s2*(9 + 2*t1 - t2)*(1 + t2) - t1*(-17 + Power(t2,2)) + 
            6*(-1 + Power(t2,2))) - 
         Power(s1,3)*(s2 - t1)*
          (12*Power(t1,4) + 12*Power(-1 + t2,2)*t2 + 
            2*Power(s2,4)*(5 + t2) + 
            Power(t1,3)*(-25 - 26*t2 + Power(t2,2)) - 
            Power(t1,2)*(27 - 55*t2 - 21*Power(t2,2) + Power(t2,3)) - 
            4*t1*(9 - 13*t2 + 3*Power(t2,2) + Power(t2,3)) + 
            Power(s2,3)*(24 + 26*t2 - 6*t1*(7 + t2)) + 
            Power(s2,2)*(-42 + 52*t2 + 40*Power(t2,2) - 2*Power(t2,3) + 
               6*Power(t1,2)*(11 + t2) + t1*(-73 - 78*t2 + Power(t2,2))) \
+ s2*(-2*Power(t1,3)*(23 + t2) + 
               Power(t1,2)*(74 + 78*t2 - 2*Power(t2,2)) + 
               2*(15 - 21*t2 + 5*Power(t2,2) + Power(t2,3)) + 
               t1*(73 - 115*t2 - 57*Power(t2,2) + 3*Power(t2,3)))) + 
         Power(s1,2)*(-(Power(s2,6)*(-3 + t2)) + Power(t1,6)*(1 + t2) - 
            2*Power(-1 + t2,3)*(-1 + 3*t2) + 
            Power(t1,5)*(11 - 9*t2 - 2*Power(t2,2)) + 
            Power(s2,5)*(-5 + 4*t1*(-4 + t2) + 2*t2 + 3*Power(t2,2)) + 
            t1*Power(-1 + t2,2)*(-31 + 34*t2 + 5*Power(t2,2)) + 
            Power(t1,3)*(-99 + 60*t2 + 37*Power(t2,2) - 6*Power(t2,3)) + 
            Power(t1,4)*(-8 - 31*t2 + 12*Power(t2,2) + Power(t2,3)) + 
            2*Power(t1,2)*(-29 + 89*t2 - 42*Power(t2,2) - 
               19*Power(t2,3) + Power(t2,4)) + 
            Power(s2,4)*(4 - 5*Power(t1,2)*(-7 + t2) - 37*t2 + 
               4*Power(t2,2) + 3*Power(t2,3) - 
               3*t1*(-11 + 7*t2 + 4*Power(t2,2))) - 
            Power(s2,3)*(-64 + 40*Power(t1,3) + 16*t2 + 
               61*Power(t2,2) - 22*Power(t2,3) + Power(t2,4) - 
               20*Power(t1,2)*(-4 + 3*t2 + Power(t2,2)) + 
               2*t1*(1 - 70*t2 + 13*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,2)*(-75 + 198*t2 - 74*Power(t2,2) - 
               50*Power(t2,3) + Power(t2,4) + 5*Power(t1,4)*(5 + t2) - 
               2*Power(t1,3)*(-46 + 37*t2 + 9*Power(t2,2)) + 
               4*Power(t1,2)*
                (-4 - 50*t2 + 13*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-221 + 90*t2 + 145*Power(t2,2) - 40*Power(t2,3) + 
                  2*Power(t2,4))) - 
            s2*(4*Power(t1,5)*(2 + t2) + 
               Power(t1,4)*(51 - 42*t2 - 9*Power(t2,2)) + 
               Power(-1 + t2,2)*(-33 + 38*t2 + 3*Power(t2,2)) + 
               2*Power(t1,3)*
                (-11 - 64*t2 + 21*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(-256 + 134*t2 + 121*Power(t2,2) - 
                  24*Power(t2,3) + Power(t2,4)) + 
               t1*(-129 + 364*t2 - 146*Power(t2,2) - 92*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         s1*(5*Power(t1,6) + Power(s2,6)*t2*(3 + 2*t2) - 
            2*Power(-1 + t2,3)*(-6 + 9*t2 + Power(t2,2)) - 
            Power(t1,5)*(-11 + 16*t2 + Power(t2,2)) - 
            t1*Power(-1 + t2,2)*
             (18 - 85*t2 - 10*Power(t2,2) + Power(t2,3)) + 
            3*Power(t1,4)*(-11 - 8*t2 + 8*Power(t2,2) + Power(t2,3)) + 
            Power(t1,3)*(-55 + 99*t2 + 23*Power(t2,2) - 
               21*Power(t2,3) - 2*Power(t2,4)) + 
            2*Power(t1,2)*(-31 + 94*t2 - 66*Power(t2,2) - 
               2*Power(t2,3) + 5*Power(t2,4)) - 
            Power(s2,5)*(6 + 5*t2 - 17*Power(t2,2) + 
               t1*(3 + 19*t2 + 8*Power(t2,2))) + 
            Power(s2,4)*(10 - 61*t2 + 5*Power(t2,2) + 18*Power(t2,3) - 
               2*Power(t2,4) + 
               Power(t1,2)*(17 + 46*t2 + 12*Power(t2,2)) + 
               t1*(39 - 3*t2 - 67*Power(t2,2) + Power(t2,3))) - 
            Power(s2,3)*(-22 - 33*t2 + 129*Power(t2,2) - 
               33*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,3)*(38 + 54*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(92 - 55*t2 - 100*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(9 - 236*t2 + 62*Power(t2,2) + 50*Power(t2,3) - 
                  5*Power(t2,4))) + 
            Power(s2,2)*(-56 + 137*t2 - 38*Power(t2,2) - 
               64*Power(t2,3) + 22*Power(t2,4) - Power(t2,5) + 
               Power(t1,4)*(42 + 31*t2 + 2*Power(t2,2)) + 
               t1*(-105 + 35*t2 + 291*Power(t2,2) - 89*Power(t2,3)) + 
               Power(t1,3)*(102 - 97*t2 - 68*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(-45 - 313*t2 + 133*Power(t2,2) + 
                  49*Power(t2,3) - 4*Power(t2,4))) + 
            s2*(-(Power(t1,5)*(23 + 7*t2)) - 
               Power(t1,4)*(54 - 66*t2 - 19*Power(t2,2) + Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-28 + 91*t2 + 12*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(77 + 162*t2 - 100*Power(t2,2) - 
                  20*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(138 - 167*t2 - 185*Power(t2,2) + 
                  77*Power(t2,3) + 5*Power(t2,4)) + 
               t1*(124 - 341*t2 + 182*Power(t2,2) + 68*Power(t2,3) - 
                  34*Power(t2,4) + Power(t2,5)))) + 
         Power(s,5)*(1 - 8*t1 - 2*Power(t1,2) + 6*Power(t1,3) - 
            3*Power(t1,4) + 4*t2 + 5*t1*t2 - 17*Power(t1,2)*t2 + 
            8*Power(t1,3)*t2 - 3*Power(t2,2) + 16*t1*Power(t2,2) - 
            6*Power(t1,2)*Power(t2,2) - 5*Power(t2,3) + Power(t2,4) + 
            Power(s1,2)*(2 + Power(s2,2) + 3*Power(t1,2) + 
               t1*(5 - 7*t2) - 6*t2 + 4*Power(t2,2) + 
               s2*(-3 - 4*t1 + 5*t2)) + 
            Power(s2,2)*(-3*Power(t1,2) - t2*(6 + 5*t2) + 
               t1*(3 + 8*t2)) + 
            s2*(-1 + 6*Power(t1,3) + t2 - 5*Power(t2,2) - 
               4*Power(t2,3) - Power(t1,2)*(9 + 16*t2) + 
               2*t1*(3 + 7*t2 + 7*Power(t2,2))) + 
            s1*(-3 + Power(t1,3) + Power(t1,2)*(10 - 6*t2) + t2 + 
               8*Power(t2,2) - 4*Power(t2,3) + 
               Power(s2,2)*(-1 - 2*t1 + 5*t2) + 
               t1*(4 - 18*t2 + 9*Power(t2,2)) + 
               s2*(Power(t1,2) + Power(-2 + t2,2) - 2*t1*(2 + t2)))) - 
         Power(s,4)*(3 - 32*t1 - 16*Power(t1,2) + 12*Power(t1,3) - 
            16*Power(t1,4) + 3*Power(t1,5) + 17*t2 + 57*t1*t2 - 
            13*Power(t1,2)*t2 + 42*Power(t1,3)*t2 - 6*Power(t1,4)*t2 - 
            23*Power(t2,2) - 2*t1*Power(t2,2) - 
            34*Power(t1,2)*Power(t2,2) + 3*Power(t2,3) + 
            6*t1*Power(t2,3) + 6*Power(t1,2)*Power(t2,3) + 
            2*Power(t2,4) - 3*t1*Power(t2,4) + 
            Power(s2,3)*(-3*Power(t1,2) + 3*t1*(1 + 4*t2) - 
               2*t2*(6 + 5*t2)) + 
            Power(s1,3)*(4 + 2*Power(s2,2) + 3*Power(t1,2) + 
               t1*(7 - 8*t2) - 9*t2 + 5*Power(t2,2) + 
               s2*(-6 - 5*t1 + 7*t2)) + 
            Power(s2,2)*(1 + 9*Power(t1,3) - 13*t2 - 17*Power(t2,2) - 
               5*Power(t2,3) - 2*Power(t1,2)*(11 + 15*t2) + 
               t1*(14 + 55*t2 + 26*Power(t2,2))) + 
            s2*(-4 - 9*Power(t1,4) + 2*t2 - 15*Power(t2,2) - 
               18*Power(t2,3) + 5*Power(t2,4) + 
               Power(t1,3)*(35 + 24*t2) - 
               Power(t1,2)*(26 + 85*t2 + 16*Power(t2,2)) + 
               t1*(21 + 8*t2 + 68*Power(t2,2) - 4*Power(t2,3))) + 
            Power(s1,2)*(3*Power(s2,3) - 3*Power(t1,3) + 
               Power(t1,2)*(27 + 4*t2) + 
               Power(s2,2)*(-4 - 11*t1 + 18*t2) - 
               3*t2*(5 - 9*t2 + 2*Power(t2,2)) + 
               t1*(28 - 50*t2 + 5*Power(t2,2)) + 
               s2*(1 + 11*Power(t1,2) - 5*t2 + 9*Power(t2,2) - 
                  3*t1*(5 + 8*t2))) + 
            s1*(-7 - 8*Power(t1,4) + 2*t2 + 15*Power(t2,2) - 
               20*Power(t2,3) + 2*Power(t2,4) + 
               Power(s2,3)*(-3 + 10*t2) + Power(t1,3)*(-7 + 24*t2) + 
               Power(t1,2)*(19 - 25*t2 - 22*Power(t2,2)) - 
               Power(s2,2)*(-1 + 8*Power(t1,2) + t1*(5 - 3*t2) + 6*t2 + 
                  8*Power(t2,2)) + 
               t1*(1 - 55*t2 + 52*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(9 + 16*Power(t1,3) + Power(t1,2)*(15 - 37*t2) + 
                  4*t2 + 17*Power(t2,2) - 16*Power(t2,3) + 
                  t1*(-6 + 2*t2 + 37*Power(t2,2))))) + 
         Power(s,3)*(4 - 67*t1 - 35*Power(t1,2) + 25*Power(t1,3) - 
            26*Power(t1,4) + 14*Power(t1,5) - Power(t1,6) + 32*t2 + 
            157*t1*t2 + 10*Power(t1,2)*t2 + 48*Power(t1,3)*t2 - 
            31*Power(t1,4)*t2 - 78*Power(t2,2) - 116*t1*Power(t2,2) - 
            52*Power(t1,2)*Power(t2,2) + 14*Power(t1,3)*Power(t2,2) + 
            6*Power(t1,4)*Power(t2,2) + 49*Power(t2,3) + 
            42*t1*Power(t2,3) + 10*Power(t1,2)*Power(t2,3) - 
            8*Power(t1,3)*Power(t2,3) - 12*Power(t2,4) - 
            8*t1*Power(t2,4) + 3*Power(t1,2)*Power(t2,4) + Power(t2,5) + 
            Power(s2,4)*(t1 - Power(t1,2) + 8*t1*t2 - 10*t2*(1 + t2)) + 
            Power(s1,4)*(Power(s2,2) + Power(t1,2) - 3*t1*(-1 + t2) + 
               2*Power(-1 + t2,2) + s2*(-3 - 2*t1 + 3*t2)) + 
            Power(s2,3)*(5 + 4*Power(t1,3) - 27*t2 - 23*Power(t2,2) - 
               Power(t1,2)*(17 + 24*t2) + 
               t1*(6 + 74*t2 + 24*Power(t2,2))) - 
            Power(s2,2)*(2 + 6*Power(t1,4) + 23*t2 + 12*Power(t2,2) + 
               25*Power(t2,3) - 10*Power(t2,4) - 
               3*Power(t1,3)*(15 + 8*t2) + 
               Power(t1,2)*(38 + 149*t2 + 12*Power(t2,2)) + 
               t1*(-32 - 54*t2 - 95*Power(t2,2) + 16*Power(t2,3))) + 
            s2*(-9 + 4*Power(t1,5) + 22*t2 - 25*Power(t2,2) + 
               3*Power(t2,3) + 9*Power(t2,4) - 
               Power(t1,4)*(43 + 8*t2) + 
               Power(t1,3)*(58 + 116*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(-62 - 75*t2 - 86*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(38 + 6*t2 + 76*Power(t2,2) + 4*Power(t2,3) - 
                  12*Power(t2,4))) + 
            Power(s1,3)*(7 + 5*Power(s2,3) - 5*Power(t1,3) - 25*t2 + 
               25*Power(t2,2) - 3*Power(t2,3) + 
               2*Power(t1,2)*(12 + 7*t2) + 
               Power(s2,2)*(-2 - 15*t1 + 20*t2) + 
               t1*(42 - 40*t2 - 6*Power(t2,2)) + 
               s2*(-8 + 15*Power(t1,2) - 8*t2 + 12*Power(t2,2) - 
                  2*t1*(9 + 17*t2))) + 
            Power(s1,2)*(7 + 3*Power(s2,4) - 5*Power(t1,4) - 52*t2 + 
               46*Power(t2,2) - 14*Power(t2,3) + Power(t2,4) + 
               6*Power(t1,3)*(-10 + 3*t2) + 
               Power(s2,3)*(1 - 8*t1 + 25*t2) + 
               Power(t1,2)*(7 + 65*t2 - 26*Power(t2,2)) + 
               3*t1*(17 - 40*t2 + Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(1 + 2*Power(t1,2) + 21*t2 - 
                  4*t1*(15 + 8*t2)) + 
               s2*(-18 + 8*Power(t1,3) + Power(t1,2)*(119 - 11*t2) + 
                  29*t2 + 88*Power(t2,2) - 21*Power(t2,3) + 
                  2*t1*(3 - 58*t2 + 15*Power(t2,2)))) + 
            s1*(-20 + 9*Power(t1,5) + 61*t2 - 23*Power(t2,2) - 
               7*Power(t2,3) + Power(t2,4) - 
               2*Power(t1,4)*(13 + 10*t2) + 
               Power(s2,4)*(-3 + 2*t1 + 10*t2) + 
               Power(t1,3)*(-46 + 121*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-25 + 80*t2 - 127*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-52 + 73*t2 + 7*Power(t2,2) + 31*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,3)*(-11 - 15*Power(t1,2) - 6*t2 - 
                  22*Power(t2,2) + t1*(12 + t2)) + 
               Power(s2,2)*(2 + 33*Power(t1,3) + 10*t2 - 
                  12*Power(t2,2) - 24*Power(t2,3) - 
                  Power(t1,2)*(41 + 52*t2) + 
                  t1*(-9 + 103*t2 + 65*Power(t2,2))) + 
               s2*(38 - 29*Power(t1,4) - 78*t2 + 32*Power(t2,2) - 
                  78*Power(t2,3) + 8*Power(t2,4) + 
                  Power(t1,3)*(58 + 61*t2) + 
                  Power(t1,2)*(66 - 218*t2 - 51*Power(t2,2)) + 
                  t1*(38 - 131*t2 + 182*Power(t2,2) + 11*Power(t2,3))))) \
+ s*(-4 + 4*Power(s1,5)*Power(s2 - t1,2) - 34*t1 + 53*Power(t1,2) + 
            82*Power(t1,3) - 22*Power(t1,4) + 14*Power(t1,5) - 
            5*Power(t1,6) - 3*t2 + 5*t1*t2 - 203*Power(t1,2)*t2 - 
            67*Power(t1,3)*t2 - 22*Power(t1,4)*t2 + 10*Power(t1,5)*t2 + 
            2*Power(t1,6)*t2 + 28*Power(t2,2) - 
            Power(s2,6)*Power(t2,2) + 90*t1*Power(t2,2) + 
            190*Power(t1,2)*Power(t2,2) + 65*Power(t1,3)*Power(t2,2) - 
            17*Power(t1,4)*Power(t2,2) - 6*Power(t1,5)*Power(t2,2) - 
            38*Power(t2,3) - 98*t1*Power(t2,3) - 
            98*Power(t1,2)*Power(t2,3) + 14*Power(t1,3)*Power(t2,3) + 
            7*Power(t1,4)*Power(t2,3) + 24*Power(t2,4) + 
            36*t1*Power(t2,4) - 3*Power(t1,2)*Power(t2,4) - 
            4*Power(t1,3)*Power(t2,4) - 7*Power(t2,5) + t1*Power(t2,5) + 
            Power(t1,2)*Power(t2,5) + 
            2*Power(s2,5)*t2*(-1 - 4*t2 + 2*Power(t2,2) + t1*(3 + t2)) + 
            Power(s2,4)*(1 - 21*t2 - 9*Power(t2,2) - 6*Power(t2,3) + 
               5*Power(t2,4) + 
               Power(t1,2)*(-5 - 22*t2 + 2*Power(t2,2)) + 
               t1*(-5 + 44*t2 + 19*Power(t2,2) - 16*Power(t2,3))) + 
            Power(s2,3)*(6 - 76*t2 + 48*Power(t2,2) - 63*Power(t2,3) + 
               11*Power(t2,4) + 
               Power(t1,3)*(20 + 28*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(1 - 130*t2 - 3*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(18 + 52*t2 + 92*Power(t2,2) - 6*Power(t2,3) - 
                  12*Power(t2,4))) + 
            Power(s2,2)*(16 - 21*t2 - 44*Power(t2,2) + 38*Power(t2,3) - 
               52*Power(t2,4) + 3*Power(t2,5) + 
               Power(t1,4)*(-30 - 12*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(27 + 146*t2 - 25*Power(t2,2) - 
                  16*Power(t2,3)) + 
               t1*(88 + 31*t2 + 11*Power(t2,2) + 130*Power(t2,3) - 
                  22*Power(t2,4)) + 
               Power(t1,2)*(-61 - 63*t2 - 174*Power(t2,2) + 
                  37*Power(t2,3) + 9*Power(t2,4))) - 
            s2*(25 - 152*t2 + 248*Power(t2,2) - 180*Power(t2,3) + 
               59*Power(t2,4) + 
               2*Power(t1,5)*(-10 + t2 + Power(t2,2)) + 
               Power(t1,4)*(37 + 68*t2 - 23*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(t1,2)*(176 - 112*t2 + 124*Power(t2,2) + 
                  81*Power(t2,3) - 15*Power(t2,4)) + 
               2*Power(t1,3)*
                (-32 - 27*t2 - 54*Power(t2,2) + 16*Power(t2,3) + 
                  Power(t2,4)) + 
               4*t1*(19 - 64*t2 + 49*Power(t2,2) - 23*Power(t2,3) - 
                  12*Power(t2,4) + Power(t2,5))) + 
            Power(s1,4)*(s2 - t1)*
             (Power(s2,3) - Power(t1,3) + 5*Power(t1,2)*(3 + t2) + 
               Power(s2,2)*(7 - 3*t1 + 5*t2) - 
               2*t1*(-19 + 7*t2 + 2*Power(t2,2)) + 
               2*(-1 - 6*t2 + 7*Power(t2,2)) + 
               s2*(3*Power(t1,2) - 2*t1*(11 + 5*t2) + 
                  2*(-7 - 5*t2 + 2*Power(t2,2)))) + 
            Power(s1,3)*(Power(s2,5) + Power(t1,5) + 
               Power(t1,4)*(35 - 2*t2) + 
               2*Power(-1 + t2,2)*(-1 + 7*t2) + 
               Power(s2,4)*(25 - 3*t1 + 10*t2) + 
               Power(t1,3)*(-13 - 53*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(-48 + 107*t2 + 34*Power(t2,2) - 
                  5*Power(t2,3)) - 
               t1*(49 - 77*t2 + 23*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,3)*(23 + 2*Power(t1,2) + 51*t2 + 
                  2*Power(t2,2) - 2*t1*(55 + 14*t2)) + 
               Power(s2,2)*(-92 + 2*Power(t1,3) + 89*t2 + 
                  98*Power(t2,2) - 7*Power(t2,3) + 
                  12*Power(t1,2)*(15 + 2*t2) + 
                  t1*(-51 - 163*t2 + 2*Power(t2,2))) + 
               s2*(51 - 3*Power(t1,4) - 83*t2 + 29*Power(t2,2) + 
                  3*Power(t2,3) - 2*Power(t1,3)*(65 + 2*t2) + 
                  Power(t1,2)*(41 + 165*t2 - 10*Power(t2,2)) + 
                  4*t1*(36 - 51*t2 - 32*Power(t2,2) + 3*Power(t2,3)))) + 
            s1*(2*Power(t1,6)*(-5 + t2) + Power(s2,6)*t2 + 
               Power(t1,5)*(20 + 28*t2 - 5*Power(t2,2)) - 
               Power(s2,5)*(1 + 3*t1*(-2 + t2) + 8*t2 + 
                  11*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (2 + 45*t2 + 4*Power(t2,2) + Power(t2,3)) + 
               Power(t1,4)*(81 - 76*t2 - 21*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(15 - 99*t2 + 262*Power(t2,2) - 
                  43*Power(t2,3) - 3*Power(t2,4)) + 
               Power(t1,3)*(31 - 234*t2 + 97*Power(t2,2) + 
                  7*Power(t2,3) - Power(t2,4)) - 
               t1*(27 - 78*t2 + 3*Power(t2,2) + 57*Power(t2,3) - 
                  10*Power(t2,4) + Power(t2,5)) + 
               Power(s2,4)*(Power(t1,2)*(-34 + 4*t2) - 
                  2*t2*(-17 + 28*t2 + 2*Power(t2,2)) + 
                  t1*(9 + 80*t2 + 34*Power(t2,2))) + 
               Power(s2,3)*(-33 + Power(t1,3)*(76 - 6*t2) + 103*t2 + 
                  48*Power(t2,2) - 74*Power(t2,3) + 8*Power(t2,4) - 
                  Power(t1,2)*(41 + 220*t2 + 31*Power(t2,2)) + 
                  t1*(-64 - 51*t2 + 204*Power(t2,2) + Power(t2,3))) + 
               Power(s2,2)*(55 - 223*t2 + 312*Power(t2,2) - 
                  19*Power(t2,3) + 7*Power(t2,4) + 
                  Power(t1,4)*(-84 + 9*t2) + 
                  Power(t1,3)*(79 + 260*t2 - Power(t2,2)) + 
                  Power(t1,2)*
                   (209 - 76*t2 - 261*Power(t2,2) + 14*Power(t2,3)) + 
                  t1*(123 - 478*t2 + 41*Power(t2,2) + 
                     125*Power(t2,3) - 15*Power(t2,4))) - 
               s2*(9 - 45*t2 + 151*Power(t2,2) - 141*Power(t2,3) + 
                  28*Power(t2,4) - 2*Power(t2,5) + 
                  Power(t1,5)*(-46 + 7*t2) + 
                  Power(t1,4)*(66 + 140*t2 - 14*Power(t2,2)) - 
                  8*t1*(-10 + 43*t2 - 73*Power(t2,2) + 7*Power(t2,3)) + 
                  Power(t1,3)*
                   (226 - 169*t2 - 134*Power(t2,2) + 15*Power(t2,3)) + 
                  Power(t1,2)*
                   (121 - 609*t2 + 186*Power(t2,2) + 58*Power(t2,3) - 
                     8*Power(t2,4)))) + 
            Power(s1,2)*(-2*Power(t1,6) + 
               2*Power(s2,5)*(-3 + t1 + 3*t2) + 
               Power(t1,5)*(-8 + 7*t2) + 
               Power(s2,4)*(16 - 10*Power(t1,2) + t1*(12 - 13*t2) + 
                  8*t2 - 12*Power(t2,2)) + 
               Power(-1 + t2,2)*(-37 + 16*t2 + Power(t2,2)) - 
               2*Power(t1,4)*(-39 + 7*t2 + 4*Power(t2,2)) + 
               2*Power(t1,3)*
                (44 - 84*t2 + 12*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-95 - 73*t2 + 62*Power(t2,2) - 
                  7*Power(t2,3) + Power(t2,4)) + 
               2*t1*(-43 + 90*t2 - 29*Power(t2,2) - 20*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,3)*(-55 + 20*Power(t1,3) - 
                  4*Power(t1,2)*(-2 + t2) + 93*t2 + 23*Power(t2,2) - 
                  15*Power(t2,3) + 2*t1*(-67 - t2 + 22*Power(t2,2))) + 
               Power(s2,2)*(-11 - 20*Power(t1,4) - 123*t2 + 
                  80*Power(t2,2) - 61*Power(t2,3) + 3*Power(t2,4) + 
                  6*Power(t1,3)*(-6 + 5*t2) + 
                  Power(t1,2)*(298 - 34*t2 - 60*Power(t2,2)) + 
                  2*t1*(96 - 189*t2 + 6*Power(t2,2) + 14*Power(t2,3))) + 
               s2*(83 + 10*Power(t1,5) + Power(t1,4)*(30 - 26*t2) - 
                  164*t2 + 36*Power(t2,2) + 48*Power(t2,3) - 
                  3*Power(t2,4) + 
                  6*Power(t1,3)*(-43 + 7*t2 + 6*Power(t2,2)) - 
                  Power(t1,2)*
                   (225 - 453*t2 + 59*Power(t2,2) + 15*Power(t2,3)) - 
                  2*t1*(-56 - 87*t2 + 58*Power(t2,2) - 29*Power(t2,3) + 
                     2*Power(t2,4))))) - 
         Power(s,2)*(5 - 78*t1 - 5*Power(t1,2) + 73*Power(t1,3) - 
            23*Power(t1,4) + 22*Power(t1,5) - 4*Power(t1,6) + 28*t2 + 
            150*t1*t2 - 97*Power(t1,2)*t2 - 15*Power(t1,3)*t2 - 
            52*Power(t1,4)*t2 + 4*Power(t1,5)*t2 + Power(t1,6)*t2 + 
            Power(s2,5)*(-3 + 2*t1 - 5*t2)*t2 - 64*Power(t2,2) - 
            88*t1*Power(t2,2) + 35*Power(t1,2)*Power(t2,2) + 
            75*Power(t1,3)*Power(t2,2) + 10*Power(t1,4)*Power(t2,2) - 
            3*Power(t1,5)*Power(t2,2) + 60*Power(t2,3) + 
            60*t1*Power(t2,3) - 59*Power(t1,2)*Power(t2,3) - 
            18*Power(t1,3)*Power(t2,3) + 3*Power(t1,4)*Power(t2,3) - 
            29*Power(t2,4) + 14*t1*Power(t2,4) + 
            10*Power(t1,2)*Power(t2,4) - Power(t1,3)*Power(t2,4) - 
            2*t1*Power(t2,5) - 
            Power(s2,4)*(-3 + 16*t2 + 17*Power(t2,2) - 5*Power(t2,3) + 
               Power(t1,2)*(4 + 7*t2) + t1*(1 - 39*t2 - 11*Power(t2,2))) \
+ Power(s2,3)*(4 - 49*t2 - 17*Power(t2,3) + 10*Power(t2,4) + 
               8*Power(t1,3)*(2 + t2) - Power(t1,2)*(19 + 103*t2) + 
               t1*(2 + 91*t2 + 56*Power(t2,2) - 24*Power(t2,3))) + 
            s2*(-13 - 2*Power(t1,5)*(-8 + t2) + 92*t2 - 
               167*Power(t2,2) + 73*Power(t2,3) - 44*Power(t2,4) + 
               3*Power(t2,5) + 
               Power(t1,4)*(-65 - 42*t2 + 13*Power(t2,2)) + 
               Power(t1,3)*(54 + 163*t2 + 2*Power(t2,2) - 
                  20*Power(t2,3)) + 
               t1*(1 + 173*t2 - 68*Power(t2,2) + 101*Power(t2,3) - 
                  23*Power(t2,4)) + 
               Power(t1,2)*(-168 + 56*t2 - 213*Power(t2,2) + 
                  44*Power(t2,3) + 9*Power(t2,4))) - 
            Power(s2,2)*(3 + 53*t2 - 6*Power(t2,2) + 33*Power(t2,3) - 
               15*Power(t2,4) + 2*Power(t1,4)*(12 + t2) + 
               Power(t1,3)*(-63 - 105*t2 + 16*Power(t2,2)) + 
               Power(t1,2)*(36 + 186*t2 + 51*Power(t2,2) - 
                  36*Power(t2,3)) + 
               t1*(-91 - 8*t2 - 138*Power(t2,2) + 9*Power(t2,3) + 
                  18*Power(t2,4))) + 
            Power(s1,4)*(2*Power(s2,3) - 2*Power(t1,3) + 
               8*Power(-1 + t2,2) + Power(t1,2)*(11 + 7*t2) + 
               Power(s2,2)*(3 - 6*t1 + 7*t2) + 
               t1*(23 - 10*t2 - 5*Power(t2,2)) + 
               s2*(-7 + 6*Power(t1,2) - 6*t2 + 5*Power(t2,2) - 
                  14*t1*(1 + t2))) + 
            Power(s1,3)*(5 + 4*Power(s2,4) + Power(t1,4) - 25*t2 + 
               19*Power(t2,2) + Power(t2,3) - 
               2*Power(t1,3)*(27 + 2*t2) + 
               Power(s2,3)*(19 - 13*t1 + 21*t2) + 
               Power(t1,2)*(-46 + 76*t2 - 4*Power(t2,2)) + 
               t1*(20 - 49*t2 - 38*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(-9 + 15*Power(t1,2) + 26*t2 + 
                  9*Power(t2,2) - 2*t1*(44 + 23*t2)) + 
               s2*(-33 - 7*Power(t1,3) + 18*t2 + 83*Power(t2,2) - 
                  8*Power(t2,3) + Power(t1,2)*(123 + 29*t2) + 
                  t1*(63 - 110*t2 - 5*Power(t2,2)))) + 
            Power(s1,2)*(6 + Power(s2,5) + 7*Power(t1,5) + 
               Power(t1,4)*(37 - 21*t2) + 24*t2 - 32*Power(t2,2) + 
               4*Power(t2,3) - 2*Power(t2,4) + 
               Power(s2,4)*(-1 + t1 + 17*t2) + 
               Power(t1,3)*(-94 - 4*t2 + 23*Power(t2,2)) - 
               Power(t1,2)*(155 - 246*t2 + 42*Power(t2,2) + 
                  7*Power(t2,3)) + 
               t1*(-43 + 203*t2 - 53*Power(t2,2) + 15*Power(t2,3) - 
                  2*Power(t2,4)) - 
               Power(s2,3)*(-13 + 16*Power(t1,2) - 30*t2 + 
                  14*Power(t2,2) + 4*t1*(11 + 6*t2)) + 
               Power(s2,2)*(-81 + 32*Power(t1,3) + 102*t2 + 
                  88*Power(t2,2) - 27*Power(t2,3) - 
                  8*Power(t1,2)*(-16 + 3*t2) + 
                  t1*(-116 - 90*t2 + 57*Power(t2,2))) + 
               s2*(68 - 25*Power(t1,4) - 209*t2 + 71*Power(t2,2) - 
                  53*Power(t2,3) + 3*Power(t2,4) + 
                  4*Power(t1,3)*(-30 + 13*t2) + 
                  Power(t1,2)*(197 + 64*t2 - 66*Power(t2,2)) + 
                  2*t1*(122 - 193*t2 - 7*Power(t2,2) + 16*Power(t2,3)))) \
+ s1*(-41 - 3*Power(t1,6) + 110*t2 - 157*Power(t2,2) + 93*Power(t2,3) - 
               6*Power(t2,4) + Power(t2,5) + 2*Power(t1,5)*(16 + t2) + 
               Power(s2,5)*(-1 + t1 + 5*t2) + 
               Power(t1,4)*(10 - 105*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(-65 + 28*t2 + 87*Power(t2,2) - 
                  12*Power(t2,3)) + 
               t1*(-84 + 223*t2 - 296*Power(t2,2) + 33*Power(t2,3)) + 
               Power(t1,2)*(-49 + 205*t2 - 91*Power(t2,2) - 
                  15*Power(t2,3) + 4*Power(t2,4)) - 
               Power(s2,4)*(9 + 7*Power(t1,2) + 8*t2 + 23*Power(t2,2) + 
                  t1*(-18 + 5*t2)) + 
               Power(s2,3)*(18*Power(t1,3) - Power(t1,2)*(80 + 17*t2) + 
                  t1*(7 + 144*t2 + 63*Power(t2,2)) - 
                  4*(3 - 9*t2 + 15*Power(t2,2) + 4*Power(t2,3))) + 
               s2*(39 + 13*Power(t1,5) - 107*t2 + 180*Power(t2,2) + 
                  7*Power(t2,3) + 5*Power(t2,4) - 
                  Power(t1,4)*(111 + 16*t2) - 
                  Power(t1,3)*(31 - 338*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (82 + 45*t2 - 294*Power(t2,2) + 19*Power(t2,3)) + 
                  t1*(21 - 190*t2 + 50*Power(t2,2) + 106*Power(t2,3) - 
                     15*Power(t2,4))) + 
               Power(s2,2)*(-22*Power(t1,4) + Power(t1,3)*(142 + 31*t2) + 
                  Power(t1,2)*(23 - 369*t2 - 48*Power(t2,2)) + 
                  t1*(-5 - 109*t2 + 267*Power(t2,2) + 9*Power(t2,3)) + 
                  2*(15 - 12*t2 + 35*Power(t2,2) - 57*Power(t2,3) + 
                     6*Power(t2,4))))))*
       B1(1 - s2 + t1 - t2,1 - s + s2 - t1,1 - s + s1 - t2))/
     ((-1 + s1)*(-s + s1 - t2)*Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,2)*
       (-1 + t2)*(-1 + s2 - t1 + t2)) - 
    (8*(24*Power(s2,2) + 26*Power(s2,3) - 49*Power(s2,4) - 
         49*Power(s2,5) + 15*Power(s2,6) + 15*Power(s2,7) + 
         2*Power(s2,8) - 48*s2*t1 - 114*Power(s2,2)*t1 + 
         208*Power(s2,3)*t1 + 294*Power(s2,4)*t1 - 96*Power(s2,5)*t1 - 
         95*Power(s2,6)*t1 - 13*Power(s2,7)*t1 + 24*Power(t1,2) + 
         150*s2*Power(t1,2) - 318*Power(s2,2)*Power(t1,2) - 
         684*Power(s2,3)*Power(t1,2) + 231*Power(s2,4)*Power(t1,2) + 
         246*Power(s2,5)*Power(t1,2) + 38*Power(s2,6)*Power(t1,2) - 
         62*Power(t1,3) + 208*s2*Power(t1,3) + 
         778*Power(s2,2)*Power(t1,3) - 264*Power(s2,3)*Power(t1,3) - 
         330*Power(s2,4)*Power(t1,3) - 67*Power(s2,5)*Power(t1,3) - 
         49*Power(t1,4) - 435*s2*Power(t1,4) + 
         141*Power(s2,2)*Power(t1,4) + 235*Power(s2,3)*Power(t1,4) + 
         80*Power(s2,4)*Power(t1,4) + 96*Power(t1,5) - 
         24*s2*Power(t1,5) - 75*Power(s2,2)*Power(t1,5) - 
         67*Power(s2,3)*Power(t1,5) - 3*Power(t1,6) + 
         38*Power(s2,2)*Power(t1,6) + 4*Power(t1,7) - 13*s2*Power(t1,7) + 
         2*Power(t1,8) - 4*Power(s1,6)*Power(s2 - t1,2)*(1 + s2 - t1)*
          (s2 + Power(s2,2) - s2*t1 + t1*(-2 + t2)) + 26*Power(s2,2)*t2 - 
         137*Power(s2,3)*t2 - 125*Power(s2,4)*t2 + 126*Power(s2,5)*t2 + 
         68*Power(s2,6)*t2 - 19*Power(s2,7)*t2 - 3*Power(s2,8)*t2 - 
         52*s2*t1*t2 + 501*Power(s2,2)*t1*t2 + 449*Power(s2,3)*t1*t2 - 
         687*Power(s2,4)*t1*t2 - 266*Power(s2,5)*t1*t2 + 
         121*Power(s2,6)*t1*t2 + 11*Power(s2,7)*t1*t2 + 
         26*Power(t1,2)*t2 - 591*s2*Power(t1,2)*t2 - 
         625*Power(s2,2)*Power(t1,2)*t2 + 
         1464*Power(s2,3)*Power(t1,2)*t2 + 
         340*Power(s2,4)*Power(t1,2)*t2 - 
         316*Power(s2,5)*Power(t1,2)*t2 - 3*Power(s2,6)*Power(t1,2)*t2 + 
         227*Power(t1,3)*t2 + 403*s2*Power(t1,3)*t2 - 
         1530*Power(s2,2)*Power(t1,3)*t2 - 
         60*Power(s2,3)*Power(t1,3)*t2 + 430*Power(s2,4)*Power(t1,3)*t2 - 
         45*Power(s2,5)*Power(t1,3)*t2 - 102*Power(t1,4)*t2 + 
         786*s2*Power(t1,4)*t2 - 220*Power(s2,2)*Power(t1,4)*t2 - 
         315*Power(s2,3)*Power(t1,4)*t2 + 95*Power(s2,4)*Power(t1,4)*t2 - 
         159*Power(t1,5)*t2 + 182*s2*Power(t1,5)*t2 + 
         109*Power(s2,2)*Power(t1,5)*t2 - 87*Power(s2,3)*Power(t1,5)*t2 - 
         44*Power(t1,6)*t2 - 6*s2*Power(t1,6)*t2 + 
         39*Power(s2,2)*Power(t1,6)*t2 - 4*Power(t1,7)*t2 - 
         7*s2*Power(t1,7)*t2 + 72*s2*Power(t2,2) - 
         214*Power(s2,2)*Power(t2,2) - 50*Power(s2,3)*Power(t2,2) + 
         399*Power(s2,4)*Power(t2,2) + 6*Power(s2,5)*Power(t2,2) - 
         135*Power(s2,6)*Power(t2,2) + 13*Power(s2,7)*Power(t2,2) + 
         5*Power(s2,8)*Power(t2,2) - 72*t1*Power(t2,2) + 
         428*s2*t1*Power(t2,2) + 16*Power(s2,2)*t1*Power(t2,2) - 
         1449*Power(s2,3)*t1*Power(t2,2) + 
         183*Power(s2,4)*t1*Power(t2,2) + 
         619*Power(s2,5)*t1*Power(t2,2) - 
         105*Power(s2,6)*t1*Power(t2,2) - 26*Power(s2,7)*t1*Power(t2,2) - 
         214*Power(t1,2)*Power(t2,2) + 118*s2*Power(t1,2)*Power(t2,2) + 
         1965*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         767*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         1083*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         342*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         51*Power(s2,6)*Power(t1,2)*Power(t2,2) - 
         84*Power(t1,3)*Power(t2,2) - 1179*s2*Power(t1,3)*Power(t2,2) + 
         1143*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         842*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         590*Power(s2,4)*Power(t1,3)*Power(t2,2) - 
         40*Power(s2,5)*Power(t1,3)*Power(t2,2) + 
         264*Power(t1,4)*Power(t2,2) - 747*s2*Power(t1,4)*Power(t2,2) - 
         193*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         585*Power(s2,3)*Power(t1,4)*Power(t2,2) - 
         5*Power(s2,4)*Power(t1,4)*Power(t2,2) + 
         182*Power(t1,5)*Power(t2,2) - 93*s2*Power(t1,5)*Power(t2,2) - 
         333*Power(s2,2)*Power(t1,5)*Power(t2,2) + 
         30*Power(s2,3)*Power(t1,5)*Power(t2,2) + 
         43*Power(t1,6)*Power(t2,2) + 100*s2*Power(t1,6)*Power(t2,2) - 
         19*Power(s2,2)*Power(t1,6)*Power(t2,2) - 
         12*Power(t1,7)*Power(t2,2) + 4*s2*Power(t1,7)*Power(t2,2) + 
         24*Power(t2,3) - 216*s2*Power(t2,3) + 
         130*Power(s2,2)*Power(t2,3) + 386*Power(s2,3)*Power(t2,3) - 
         243*Power(s2,4)*Power(t2,3) - 165*Power(s2,5)*Power(t2,3) + 
         85*Power(s2,6)*Power(t2,3) + 7*Power(s2,7)*Power(t2,3) - 
         2*Power(s2,8)*Power(t2,3) + 216*t1*Power(t2,3) - 
         276*s2*t1*Power(t2,3) - 962*Power(s2,2)*t1*Power(t2,3) + 
         951*Power(s2,3)*t1*Power(t2,3) + 
         532*Power(s2,4)*t1*Power(t2,3) - 
         457*Power(s2,5)*t1*Power(t2,3) - 25*Power(s2,6)*t1*Power(t2,3) + 
         12*Power(s2,7)*t1*Power(t2,3) + 146*Power(t1,2)*Power(t2,3) + 
         766*s2*Power(t1,2)*Power(t2,3) - 
         1385*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         483*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         1010*Power(s2,4)*Power(t1,2)*Power(t2,3) + 
         20*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         30*Power(s2,6)*Power(t1,2)*Power(t2,3) - 
         190*Power(t1,3)*Power(t2,3) + 889*s2*Power(t1,3)*Power(t2,3) - 
         93*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         1170*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         30*Power(s2,4)*Power(t1,3)*Power(t2,3) + 
         40*Power(s2,5)*Power(t1,3)*Power(t2,3) - 
         212*Power(t1,4)*Power(t2,3) + 332*s2*Power(t1,4)*Power(t2,3) + 
         745*Power(s2,2)*Power(t1,4)*Power(t2,3) - 
         65*Power(s2,3)*Power(t1,4)*Power(t2,3) - 
         30*Power(s2,4)*Power(t1,4)*Power(t2,3) - 
         123*Power(t1,5)*Power(t2,3) - 245*s2*Power(t1,5)*Power(t2,3) + 
         43*Power(s2,2)*Power(t1,5)*Power(t2,3) + 
         12*Power(s2,3)*Power(t1,5)*Power(t2,3) + 
         32*Power(t1,6)*Power(t2,3) - 10*s2*Power(t1,6)*Power(t2,3) - 
         2*Power(s2,2)*Power(t1,6)*Power(t2,3) - 72*Power(t2,4) + 
         188*s2*Power(t2,4) + 148*Power(s2,2)*Power(t2,4) - 
         270*Power(s2,3)*Power(t2,4) - 26*Power(s2,4)*Power(t2,4) + 
         123*Power(s2,5)*Power(t2,4) - 3*Power(s2,6)*Power(t2,4) - 
         4*Power(s2,7)*Power(t2,4) - 188*t1*Power(t2,4) - 
         256*s2*t1*Power(t2,4) + 670*Power(s2,2)*t1*Power(t2,4) - 
         39*Power(s2,3)*t1*Power(t2,4) - 519*Power(s2,4)*t1*Power(t2,4) + 
         24*Power(s2,5)*t1*Power(t2,4) + 20*Power(s2,6)*t1*Power(t2,4) + 
         108*Power(t1,2)*Power(t2,4) - 530*s2*Power(t1,2)*Power(t2,4) + 
         265*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         847*Power(s2,3)*Power(t1,2)*Power(t2,4) - 
         66*Power(s2,4)*Power(t1,2)*Power(t2,4) - 
         40*Power(s2,5)*Power(t1,2)*Power(t2,4) + 
         130*Power(t1,3)*Power(t2,4) - 309*s2*Power(t1,3)*Power(t2,4) - 
         657*Power(s2,2)*Power(t1,3)*Power(t2,4) + 
         84*Power(s2,3)*Power(t1,3)*Power(t2,4) + 
         40*Power(s2,4)*Power(t1,3)*Power(t2,4) + 
         109*Power(t1,4)*Power(t2,4) + 234*s2*Power(t1,4)*Power(t2,4) - 
         51*Power(s2,2)*Power(t1,4)*Power(t2,4) - 
         20*Power(s2,3)*Power(t1,4)*Power(t2,4) - 
         28*Power(t1,5)*Power(t2,4) + 12*s2*Power(t1,5)*Power(t2,4) + 
         4*Power(s2,2)*Power(t1,5)*Power(t2,4) + 72*Power(t2,5) - 
         16*s2*Power(t2,5) - 120*Power(s2,2)*Power(t2,5) + 
         43*Power(s2,3)*Power(t2,5) + 66*Power(s2,4)*Power(t2,5) - 
         9*Power(s2,5)*Power(t2,5) - 2*Power(s2,6)*Power(t2,5) + 
         16*t1*Power(t2,5) + 208*s2*t1*Power(t2,5) - 
         111*Power(s2,2)*t1*Power(t2,5) - 
         204*Power(s2,3)*t1*Power(t2,5) + 37*Power(s2,4)*t1*Power(t2,5) + 
         8*Power(s2,5)*t1*Power(t2,5) - 88*Power(t1,2)*Power(t2,5) + 
         93*s2*Power(t1,2)*Power(t2,5) + 
         218*Power(s2,2)*Power(t1,2)*Power(t2,5) - 
         57*Power(s2,3)*Power(t1,2)*Power(t2,5) - 
         12*Power(s2,4)*Power(t1,2)*Power(t2,5) - 
         25*Power(t1,3)*Power(t2,5) - 88*s2*Power(t1,3)*Power(t2,5) + 
         39*Power(s2,2)*Power(t1,3)*Power(t2,5) + 
         8*Power(s2,3)*Power(t1,3)*Power(t2,5) + 
         8*Power(t1,4)*Power(t2,5) - 10*s2*Power(t1,4)*Power(t2,5) - 
         2*Power(s2,2)*Power(t1,4)*Power(t2,5) - 24*Power(t2,6) - 
         28*s2*Power(t2,6) + 6*Power(s2,2)*Power(t2,6) + 
         6*Power(s2,3)*Power(t2,6) - 4*Power(s2,4)*Power(t2,6) + 
         28*t1*Power(t2,6) - 4*s2*t1*Power(t2,6) - 
         12*Power(s2,2)*t1*Power(t2,6) + 12*Power(s2,3)*t1*Power(t2,6) - 
         2*Power(t1,2)*Power(t2,6) + 6*s2*Power(t1,2)*Power(t2,6) - 
         12*Power(s2,2)*Power(t1,2)*Power(t2,6) + 
         4*s2*Power(t1,3)*Power(t2,6) - 
         2*Power(s,7)*(t1 - t2)*
          (6 + 2*t1 - 2*Power(t1,2) + Power(t1,3) + 
            Power(s1,2)*(7 - 7*s2 + 6*t1 - 6*t2) + 
            Power(s2,2)*(-1 + t1 - t2) - 2*t2 + 4*t1*t2 - 
            3*Power(t1,2)*t2 - 2*Power(t2,2) + 3*t1*Power(t2,2) - 
            Power(t2,3) - s2*
             (5 - 3*t1 + 2*Power(t1,2) + 3*t2 - 4*t1*t2 + 2*Power(t2,2)) \
+ s1*(-13 + Power(s2,2) - 7*t1 + 4*Power(t1,2) + 7*t2 - 8*t1*t2 + 
               4*Power(t2,2) + s2*(12 - 5*t1 + 5*t2))) - 
         2*Power(s1,5)*(s2 - t1)*
          (Power(s2,4)*(-5 + 2*t1 - 9*t2) + 3*Power(t1,4)*(-4 + t2) + 
            4*Power(-1 + t2,2) + 
            Power(t1,3)*(30 + 3*t2 - 6*Power(t2,2)) + 
            t1*(-2 + 8*t2 - 6*Power(t2,2)) + 
            8*Power(t1,2)*(-2 - t2 + Power(t2,2)) - 
            Power(s2,3)*(23 + 6*Power(t1,2) + 6*t2 + 2*Power(t2,2) - 
               t1*(27 + 26*t2)) + 
            Power(s2,2)*(6*Power(t1,3) - Power(t1,2)*(51 + 22*t2) + 
               t1*(74 + 17*t2 - 2*Power(t2,2)) + 
               2*(-8 - t2 + Power(t2,2))) + 
            s2*(4 - 2*Power(t1,4) - 12*t2 + 8*Power(t2,2) + 
               Power(t1,3)*(41 + 2*t2) + 
               t1*(32 + 10*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(-81 - 14*t2 + 10*Power(t2,2)))) + 
         Power(s1,4)*(2*Power(s2,7) + Power(s2,6)*(8 - 19*t1) - 
            5*Power(t1,7) + 3*Power(t1,6)*(-3 + t2) - 
            8*Power(-1 + t2,3) - 4*t1*Power(-1 + t2,2)*(9 + 2*t2) + 
            Power(s2,5)*(-78 + 70*Power(t1,2) + 13*t1*(-3 + t2) + 
               10*t2 - 32*Power(t2,2)) + 
            Power(t1,5)*(158 - 8*Power(t2,2)) + 
            4*Power(t1,2)*t2*(-11 + 4*t2 + 7*Power(t2,2)) + 
            Power(t1,3)*(40 + 106*t2 - 38*Power(t2,2) - 
               24*Power(t2,3)) + 
            2*Power(t1,4)*(-57 - 81*t2 + 22*Power(t2,2) + 
               6*Power(t2,3)) - 
            Power(s2,4)*(135*Power(t1,3) + Power(t1,2)*(-67 + 49*t2) - 
               2*t1*(231 - 19*t2 + 67*Power(t2,2)) + 
               2*(39 + 69*t2 - 20*Power(t2,2) + 6*Power(t2,3))) + 
            2*Power(s2,3)*(-21 + 75*Power(t1,4) - 55*t2 + 
               26*Power(t2,2) + 8*Power(t2,3) + 
               Power(t1,3)*(-19 + 33*t2) + 
               Power(t1,2)*(-538 + 27*t2 - 101*Power(t2,2)) + 
               t1*(175 + 280*t2 - 75*Power(t2,2) + 12*Power(t2,3))) - 
            Power(s2,2)*(97*Power(t1,5) + 2*Power(t1,4)*(9 + 17*t2) + 
               Power(t1,2)*(580 + 868*t2 - 224*Power(t2,2)) + 
               4*t2*(7 + 4*t2 - 11*Power(t2,2)) - 
               2*Power(t1,3)*(618 - 17*t2 + 61*Power(t2,2)) + 
               2*t1*(-62 - 163*t2 + 71*Power(t2,2) + 28*Power(t2,3))) + 
            s2*(34*Power(t1,6) + Power(t1,5)*(29 + t2) + 
               4*Power(-1 + t2,2)*(9 + 2*t2) - 
               72*t1*t2*(-1 + Power(t2,2)) - 
               2*Power(t1,4)*(351 - 4*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(422 + 608*t2 - 158*Power(t2,2) - 
                  24*Power(t2,3)) + 
               2*Power(t1,2)*(-61 - 161*t2 + 64*Power(t2,2) + 
                  32*Power(t2,3)))) + 
         Power(s1,3)*(Power(s2,8) + Power(t1,8) + 
            4*Power(t1,7)*(-7 + t2) + 24*Power(-1 + t2,3)*(1 + t2) + 
            Power(s2,7)*(-3 - 12*t1 + 2*t2) + 
            Power(t1,6)*(210 + 37*t2 - 7*Power(t2,2)) - 
            8*t1*Power(-1 + t2,2)*(-3 - 7*t2 + 3*Power(t2,2)) + 
            Power(t1,5)*(-61 - 500*t2 + 65*Power(t2,2) + 
               6*Power(t2,3)) + 
            Power(t1,4)*(142 + 90*t2 + 236*Power(t2,2) - 
               94*Power(t2,3) - 4*Power(t2,4)) - 
            4*Power(t1,2)*(22 - 68*t2 + 27*Power(t2,2) + 
               16*Power(t2,3) + 3*Power(t2,4)) + 
            2*Power(t1,3)*(-43 - 78*t2 + 33*Power(t2,2) + 
               32*Power(t2,3) + 8*Power(t2,4)) + 
            Power(s2,6)*(57 + 52*Power(t1,2) + 30*t2 - 9*Power(t2,2) - 
               7*t1*(3 + t2)) + 
            Power(s2,5)*(35 - 116*Power(t1,3) + 206*t2 - Power(t2,2) + 
               12*Power(t2,3) + Power(t1,2)*(178 + t2) + 
               t1*(-466 - 205*t2 + 33*Power(t2,2))) + 
            Power(s2,4)*(150*Power(t1,4) + 
               10*Power(t1,3)*(-47 + 3*t2) + 
               Power(t1,2)*(1504 + 557*t2 - 49*Power(t2,2)) - 
               t1*(175 + 1326*t2 - 63*Power(t2,2) + 60*Power(t2,3)) + 
               2*(72 - 5*t2 + 115*Power(t2,2) - 55*Power(t2,3) + 
                  6*Power(t2,4))) + 
            s2*(-12*Power(t1,7) + Power(t1,6)*(176 - 23*t2) + 
               8*Power(-1 + t2,2)*(-3 - 7*t2 + 3*Power(t2,2)) + 
               Power(t1,5)*(-1078 - 233*t2 + 25*Power(t2,2)) + 
               Power(t1,4)*(253 + 2208*t2 - 255*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,3)*(-570 - 262*t2 - 918*Power(t2,2) + 
                  374*Power(t2,3)) + 
               8*t1*(20 - 63*t2 + 26*Power(t2,2) + 11*Power(t2,3) + 
                  6*Power(t2,4)) - 
               2*Power(t1,2)*
                (-117 - 247*t2 + 89*Power(t2,2) + 103*Power(t2,3) + 
                  28*Power(t2,4))) - 
            Power(s2,3)*(-62 + 116*Power(t1,5) - 182*t2 + 
               46*Power(t2,2) + 78*Power(t2,3) + 24*Power(t2,4) + 
               Power(t1,4)*(-625 + 60*t2) + 
               Power(t1,3)*(2496 + 778*t2 - 46*Power(t2,2)) - 
               2*Power(t1,2)*
                (188 + 1621*t2 - 124*Power(t2,2) + 51*Power(t2,3)) + 
               t1*(574 + 62*t2 + 906*Power(t2,2) - 406*Power(t2,3) + 
                  32*Power(t2,4))) + 
            Power(s2,2)*(52*Power(t1,6) + Power(t1,5)*(-457 + 53*t2) + 
               Power(t1,4)*(2269 + 592*t2 - 39*Power(t2,2)) - 
               2*Power(t1,3)*
                (214 + 1915*t2 - 188*Power(t2,2) + 33*Power(t2,3)) - 
               4*(18 - 58*t2 + 25*Power(t2,2) + 6*Power(t2,3) + 
                  9*Power(t2,4)) + 
               2*Power(t1,2)*
                (429 + 122*t2 + 679*Power(t2,2) - 288*Power(t2,3) + 
                  12*Power(t2,4)) + 
               2*t1*(-105 - 260*t2 + 79*Power(t2,2) + 110*Power(t2,3) + 
                  32*Power(t2,4)))) + 
         Power(s1,2)*(Power(s2,9) - 24*Power(-1 + t2,3)*t2*(3 + t2) + 
            Power(t1,8)*(-37 + 4*t2) + Power(s2,8)*(7 - 10*t1 + 8*t2) + 
            Power(t1,7)*(180 + 87*t2 - 12*Power(t2,2)) + 
            8*t1*Power(-1 + t2,2)*
             (-9 + 3*t2 + 3*Power(t2,2) + 5*Power(t2,3)) + 
            Power(t1,6)*(-61 - 488*t2 - 49*Power(t2,2) + 
               12*Power(t2,3)) + 
            Power(t1,5)*(215 - 57*t2 + 477*Power(t2,2) - 
               45*Power(t2,3) - 4*Power(t2,4)) + 
            Power(t1,4)*(-43 - 565*t2 + 485*Power(t2,2) - 
               191*Power(t2,3) + 48*Power(t2,4)) + 
            Power(t1,2)*(6 + 66*t2 - 368*Power(t2,2) + 
               302*Power(t2,3) + 6*Power(t2,4) - 12*Power(t2,5)) + 
            Power(t1,3)*(-90 + 418*t2 + 82*Power(t2,2) - 
               396*Power(t2,3) + 34*Power(t2,4) - 4*Power(t2,5)) + 
            Power(s2,7)*(7 + 42*Power(t1,2) - 28*t2 + 13*Power(t2,2) - 
               12*t1*(2 + 5*t2)) - 
            Power(s2,6)*(29 + 98*Power(t1,3) + 
               Power(t1,2)*(40 - 196*t2) + 42*t2 + 101*Power(t2,2) - 
               24*Power(t2,3) + 3*t1*(-36 - 91*t2 + 32*Power(t2,2))) + 
            Power(s2,5)*(140*Power(t1,4) - 
               4*Power(t1,3)*(-88 + 91*t2) + 
               3*Power(t1,2)*(-275 - 344*t2 + 99*Power(t2,2)) + 
               t1*(213 + 618*t2 + 607*Power(t2,2) - 128*Power(t2,3)) + 
               2*(-93 + 42*t2 - 50*Power(t2,2) - 29*Power(t2,3) + 
                  6*Power(t2,4))) - 
            Power(s2,4)*(42 + 126*Power(t1,5) + 
               Power(t1,4)*(810 - 420*t2) + 436*t2 - 376*Power(t2,2) + 
               124*Power(t2,3) - 64*Power(t2,4) + 4*Power(t2,5) + 
               5*Power(t1,3)*(-452 - 409*t2 + 100*Power(t2,2)) + 
               Power(t1,2)*(623 + 2540*t2 + 1467*Power(t2,2) - 
                  284*Power(t2,3)) + 
               2*t1*(-482 + 217*t2 - 444*Power(t2,2) - 97*Power(t2,3) + 
                  21*Power(t2,4))) + 
            Power(s2,3)*(70*Power(t1,6) - 44*Power(t1,5)*(-22 + 7*t2) + 
               45*Power(t1,4)*(-71 - 52*t2 + 11*Power(t2,2)) + 
               Power(t1,3)*(942 + 4820*t2 + 1818*Power(t2,2) - 
                  336*Power(t2,3)) + 
               Power(t1,2)*(-1991 + 855*t2 - 2541*Power(t2,2) - 
                  189*Power(t2,3) + 58*Power(t2,4)) + 
               2*(10 - 85*t2 - 144*Power(t2,2) + 196*Power(t2,3) - 
                  7*Power(t2,4) + 8*Power(t2,5)) + 
               t1*(153 + 1903*t2 - 1631*Power(t2,2) + 557*Power(t2,3) - 
                  230*Power(t2,4) + 12*Power(t2,5))) + 
            s2*(3*Power(t1,8) + Power(t1,7)*(240 - 36*t2) + 
               Power(t1,6)*(-1043 - 568*t2 + 91*Power(t2,2)) + 
               Power(t1,5)*(341 + 2402*t2 + 399*Power(t2,2) - 
                  80*Power(t2,3)) - 
               8*Power(-1 + t2,2)*
                (-9 + 3*t2 + 3*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,4)*(-1051 + 353*t2 - 2019*Power(t2,2) + 
                  115*Power(t2,3) + 18*Power(t2,4)) + 
               4*t1*(-3 - 21*t2 + 154*Power(t2,2) - 131*Power(t2,3) - 
                  Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,3)*(155 + 2161*t2 - 1849*Power(t2,2) + 
                  691*Power(t2,3) - 198*Power(t2,4) + 4*Power(t2,5)) + 
               2*Power(t1,2)*
                (100 - 503*t2 - 226*Power(t2,2) + 592*Power(t2,3) - 
                  41*Power(t2,4) + 12*Power(t2,5))) - 
            Power(s2,2)*(22*Power(t1,7) + Power(t1,6)*(656 - 140*t2) + 
               3*Power(t1,5)*(-836 - 521*t2 + 96*Power(t2,2)) + 
               Power(t1,4)*(783 + 4770*t2 + 1207*Power(t2,2) - 
                  224*Power(t2,3)) + 
               Power(t1,3)*(-2049 + 801*t2 - 3295*Power(t2,2) + 
                  17*Power(t2,3) + 42*Power(t2,4)) + 
               2*(-3 - 9*t2 + 124*Power(t2,2) - 111*Power(t2,3) + 
                  Power(t2,4) - 2*Power(t2,5)) + 
               Power(t1,2)*(223 + 3063*t2 - 2619*Power(t2,2) + 
                  933*Power(t2,3) - 316*Power(t2,4) + 12*Power(t2,5)) + 
               2*t1*(65 - 379*t2 - 329*Power(t2,2) + 590*Power(t2,3) - 
                  31*Power(t2,4) + 18*Power(t2,5)))) + 
         s1*(-2*Power(t1,9) + Power(s2,9)*(-2 + t2) + 
            16*Power(t1,8)*(1 + t2) + 
            8*Power(-1 + t2,3)*Power(t2,2)*(9 + t2) + 
            Power(s2,8)*(-16 + t1*(15 - 7*t2) + 5*t2 + 3*Power(t2,2)) - 
            2*Power(t1,7)*(19 + 21*t2 + 19*Power(t2,2)) - 
            8*t1*Power(-1 + t2,2)*t2*
             (-18 + 15*t2 + 9*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,6)*(247 - 146*t2 + 55*Power(t2,2) + 
               40*Power(t2,3)) - 
            2*Power(t1,5)*(38 + 293*t2 - 292*Power(t2,2) + 
               29*Power(t2,3) + 10*Power(t2,4)) + 
            Power(t1,4)*(-38 + 263*t2 + 431*Power(t2,2) - 
               685*Power(t2,3) + 73*Power(t2,4) + 4*Power(t2,5)) - 
            Power(t1,3)*(-31 + 100*t2 + 128*Power(t2,2) + 
               24*Power(t2,3) - 273*Power(t2,4) + 52*Power(t2,5)) + 
            2*Power(t1,2)*(-43 + 122*t2 - 56*Power(t2,2) - 
               12*Power(t2,3) - 29*Power(t2,4) + 14*Power(t2,5) + 
               4*Power(t2,6)) + 
            Power(s2,7)*(-22 - 84*t2 + 58*Power(t2,2) + 3*Power(t2,3) + 
               Power(t1,2)*(-47 + 21*t2) + 
               t1*(119 - 63*t2 - 16*Power(t2,2))) + 
            Power(s2,6)*(41 + Power(t1,3)*(77 - 35*t2) - 40*t2 - 
               153*Power(t2,2) + 103*Power(t2,3) - 5*Power(t2,4) + 
               Power(t1,2)*(-362 + 289*t2 + 33*Power(t2,2)) + 
               t1*(128 + 484*t2 - 423*Power(t2,2) - 7*Power(t2,3))) + 
            Power(s2,5)*(81 + 179*t2 - 165*Power(t2,2) - 
               161*Power(t2,3) + 72*Power(t2,4) - 6*Power(t2,5) + 
               7*Power(t1,4)*(-9 + 5*t2) + 
               Power(t1,3)*(569 - 691*t2 - 30*Power(t2,2)) - 
               Power(t1,2)*(272 + 1118*t2 - 1283*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(-448 + 376*t2 + 727*Power(t2,2) - 591*Power(t2,3) + 
                  30*Power(t2,4))) + 
            Power(s2,4)*(101 + Power(t1,5)*(7 - 21*t2) - 69*t2 + 
               375*Power(t2,2) - 341*Power(t2,3) - 56*Power(t2,4) + 
               6*Power(t2,5) + 
               5*Power(t1,4)*(-92 + 195*t2 + Power(t2,2)) + 
               10*Power(t1,3)*
                (21 + 127*t2 - 210*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(1629 - 1250*t2 - 1323*Power(t2,2) + 
                  1374*Power(t2,3) - 70*Power(t2,4)) + 
               2*t1*(-207 - 631*t2 + 615*Power(t2,2) + 
                  292*Power(t2,3) - 156*Power(t2,4) + 11*Power(t2,5))) + 
            Power(s2,3)*(-25 + 232*t2 - 292*Power(t2,2) + 
               358*Power(t2,3) - 303*Power(t2,4) + 34*Power(t2,5) - 
               4*Power(t2,6) + 7*Power(t1,6)*(5 + t2) + 
               Power(t1,5)*(121 - 845*t2 + 12*Power(t2,2)) + 
               Power(t1,4)*(90 - 640*t2 + 2000*Power(t2,2) - 
                  65*Power(t2,3)) + 
               2*Power(t1,3)*
                (-1428 + 1020*t2 + 541*Power(t2,2) - 823*Power(t2,3) + 
                  40*Power(t2,4)) + 
               Power(t1,2)*(832 + 3298*t2 - 3284*Power(t2,2) - 
                  728*Power(t2,3) + 524*Power(t2,4) - 30*Power(t2,5)) - 
               t1*(273 + 16*t2 + 1608*Power(t2,2) - 1734*Power(t2,3) - 
                  91*Power(t2,4) + 24*Power(t2,5))) + 
            s2*(13*Power(t1,8) + 
               Power(t1,7)*(-73 - 129*t2 + 2*Power(t2,2)) + 
               Power(t1,6)*(172 + 146*t2 + 323*Power(t2,2) - 
                  8*Power(t2,3)) + 
               8*Power(-1 + t2,2)*t2*
                (-18 + 15*t2 + 9*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,5)*(-1272 + 800*t2 - 105*Power(t2,2) - 
                  339*Power(t2,3) + 10*Power(t2,4)) + 
               Power(t1,4)*(399 + 2483*t2 - 2487*Power(t2,2) + 
                  73*Power(t2,3) + 156*Power(t2,4) - 4*Power(t2,5)) - 
               Power(t1,3)*(-5 + 680*t2 + 1720*Power(t2,2) - 
                  2422*Power(t2,3) + 167*Power(t2,4) + 20*Power(t2,5)) \
+ Power(t1,2)*(-87 + 432*t2 - 36*Power(t2,2) + 406*Power(t2,3) - 
                  849*Power(t2,4) + 138*Power(t2,5) - 4*Power(t2,6)) - 
               4*t1*(-43 + 122*t2 - 44*Power(t2,2) - 42*Power(t2,3) - 
                  6*Power(t2,4) + 10*Power(t2,5) + 3*Power(t2,6))) + 
            Power(s2,2)*(-(Power(t1,7)*(33 + t2)) + 
               Power(t1,6)*(86 + 443*t2 - 9*Power(t2,2)) + 
               Power(t1,5)*(-268 - 16*t2 - 1103*Power(t2,2) + 
                  37*Power(t2,3)) + 
               Power(t1,4)*(2659 - 1780*t2 - 283*Power(t2,2) + 
                  1059*Power(t2,3) - 45*Power(t2,4)) + 
               2*Power(t1,3)*
                (-411 - 2056*t2 + 2061*Power(t2,2) + 145*Power(t2,3) - 
                  210*Power(t2,4) + 9*Power(t2,5)) + 
               Power(t1,2)*(205 + 502*t2 + 2522*Power(t2,2) - 
                  3130*Power(t2,3) + 59*Power(t2,4) + 34*Power(t2,5)) + 
               2*(-43 + 122*t2 - 32*Power(t2,2) - 72*Power(t2,3) + 
                  17*Power(t2,4) + 6*Power(t2,5) + 2*Power(t2,6)) + 
               t1*(81 - 564*t2 + 456*Power(t2,2) - 740*Power(t2,3) + 
                  879*Power(t2,4) - 120*Power(t2,5) + 8*Power(t2,6)))) + 
         Power(s,6)*(12 + 72*t1 - 35*Power(t1,2) - 43*Power(t1,3) + 
            24*Power(t1,4) - 8*Power(t1,5) - 72*t2 - 14*t1*t2 + 
            101*Power(t1,2)*t2 - 68*Power(t1,3)*t2 + 30*Power(t1,4)*t2 + 
            49*Power(t2,2) - 73*t1*Power(t2,2) + 
            60*Power(t1,2)*Power(t2,2) - 40*Power(t1,3)*Power(t2,2) + 
            15*Power(t2,3) - 12*t1*Power(t2,3) + 
            20*Power(t1,2)*Power(t2,3) - 4*Power(t2,4) - 2*Power(t2,5) - 
            6*Power(s1,4)*(-1 + s2 - t1 + t2) + 
            Power(s1,3)*(-6 + 6*Power(s2,2) + 37*Power(t1,2) + 
               t1*(33 - 70*t2) - 49*s2*(t1 - t2) - 33*t2 + 
               33*Power(t2,2)) + 
            4*Power(s2,3)*(2*Power(t1,2) + t2*(2 + 3*t2) - 
               t1*(2 + 5*t2)) + 
            Power(s1,2)*(6 - 29*Power(t1,3) - 4*t2 + 51*Power(t2,2) - 
               43*Power(t2,3) + Power(t1,2)*(-47 + 15*t2) + 
               Power(s2,2)*(6 - 59*t1 + 59*t2) + 
               s2*(-12 + 88*Power(t1,2) + t1*(67 - 90*t2) - 67*t2 + 
                  2*Power(t2,2)) + t1*(4 - 4*t2 + 57*Power(t2,2))) + 
            Power(s2,2)*(18 - 24*Power(t1,3) + 59*t2 + 26*Power(t2,2) + 
               22*Power(t2,3) + 10*Power(t1,2)*(4 + 7*t2) - 
               t1*(59 + 66*t2 + 68*Power(t2,2))) + 
            s2*(-30 + 24*Power(t1,4) + 7*t2 + 32*Power(t2,2) + 
               14*Power(t2,3) + 8*Power(t2,4) - 
               8*Power(t1,3)*(7 + 10*t2) + 
               6*Power(t1,2)*(17 + 21*t2 + 16*Power(t2,2)) - 
               t1*(7 + 134*t2 + 84*Power(t2,2) + 48*Power(t2,3))) + 
            s1*(-18 - 26*Power(t1,4) + 6*Power(s2,3)*(t1 - t2) + 
               113*t2 - 119*Power(t2,2) + 4*Power(t2,3) + 
               18*Power(t2,4) + Power(t1,3)*(94 + 60*t2) + 
               Power(t1,2)*(63 - 184*t2 - 24*Power(t2,2)) + 
               t1*(-113 + 56*t2 + 86*Power(t2,2) - 28*Power(t2,3)) - 
               2*Power(s2,2)*
                (15 + 19*Power(t1,2) + 62*t2 + 18*Power(t2,2) - 
                  t1*(62 + 37*t2)) + 
               s2*(48 + 58*Power(t1,3) + 17*t2 - 50*Power(t2,2) - 
                  12*Power(t2,3) - 2*Power(t1,2)*(109 + 64*t2) + 
                  t1*(-17 + 268*t2 + 82*Power(t2,2))))) + 
         Power(s,5)*(-60 - 107*t1 + 284*Power(t1,2) + 46*Power(t1,3) - 
            131*Power(t1,4) + 56*Power(t1,5) - 12*Power(t1,6) + 179*t2 - 
            140*t1*t2 - 349*Power(t1,2)*t2 + 267*Power(t1,3)*t2 - 
            154*Power(t1,4)*t2 + 40*Power(t1,5)*t2 - 144*Power(t2,2) + 
            308*t1*Power(t2,2) - 99*Power(t1,2)*Power(t2,2) + 
            136*Power(t1,3)*Power(t2,2) - 40*Power(t1,4)*Power(t2,2) - 
            5*Power(t2,3) - 79*t1*Power(t2,3) - 
            44*Power(t1,2)*Power(t2,3) + 42*Power(t2,4) + 
            16*t1*Power(t2,4) + 20*Power(t1,2)*Power(t2,4) - 
            10*Power(t2,5) - 8*t1*Power(t2,5) + 
            10*Power(s1,5)*(-1 + s2 - t1 + t2) + 
            Power(s2,4)*(-12*Power(t1,2) - t2*(13 + 30*t2) + 
               t1*(11 + 40*t2)) + 
            Power(s1,4)*(-4 + 19*Power(s2,2) - 15*Power(t1,2) + 48*t2 - 
               40*Power(t2,2) - 13*s2*(1 + 3*t2) + t1*(-13 + 57*t2)) + 
            Power(s2,3)*(-62 + 48*Power(t1,3) - 139*t2 - 
               61*Power(t2,2) - 48*Power(t2,3) - 
               Power(t1,2)*(89 + 160*t2) + 
               2*t1*(65 + 77*t2 + 80*Power(t2,2))) + 
            Power(s2,2)*(35 - 72*Power(t1,4) - 149*t2 - 
               56*Power(t2,2) - 51*Power(t2,3) - 6*Power(t2,4) + 
               3*Power(t1,3)*(67 + 80*t2) - 
               Power(t1,2)*(391 + 423*t2 + 270*Power(t2,2)) + 
               t1*(260 + 447*t2 + 273*Power(t2,2) + 108*Power(t2,3))) + 
            s2*(89 + 48*Power(t1,5) + 119*t2 - 200*Power(t2,2) - 
               Power(t2,3) - 13*Power(t2,4) + 12*Power(t2,5) - 
               Power(t1,4)*(179 + 160*t2) + 
               4*Power(t1,3)*(98 + 109*t2 + 45*Power(t2,2)) - 
               Power(t1,2)*(244 + 575*t2 + 348*Power(t2,2) + 
                  60*Power(t2,3)) + 
               t1*(-295 + 444*t2 + 184*Power(t2,2) + 104*Power(t2,3) - 
                  20*Power(t2,4))) + 
            Power(s1,3)*(13 - 27*Power(s2,3) + 124*Power(t1,3) + 
               Power(s2,2)*(-33 + 229*t1 - 185*t2) + 
               Power(t1,2)*(31 - 154*t2) + 66*t2 - 149*Power(t2,2) + 
               66*Power(t2,3) - 2*t1*(56 - 57*t2 + 18*Power(t2,2)) + 
               s2*(39 - 326*Power(t1,2) + 35*t2 - 32*Power(t2,2) + 
                  t1*(-17 + 354*t2))) - 
            Power(s1,2)*(20 + 2*Power(s2,4) + 11*Power(t1,4) + 103*t2 - 
               52*Power(t2,2) - 113*Power(t2,3) + 44*Power(t2,4) + 
               Power(t1,3)*(160 + 103*t2) + 
               Power(s2,3)*(16 - 113*t1 + 116*t2) - 
               Power(t1,2)*(61 + 127*t2 + 195*Power(t2,2)) + 
               t1*(-150 + 113*t2 + 80*Power(t2,2) + 37*Power(t2,3)) + 
               Power(s2,2)*(231*Power(t1,2) - 3*t1*(-59 + 47*t2) - 
                  10*(8 + 17*t2 + 9*Power(t2,2))) + 
               s2*(30 - 131*Power(t1,3) - 147*t2 + 151*Power(t2,2) - 
                  118*Power(t2,3) - Power(t1,2)*(353 + 78*t2) + 
                  t1*(208 + 190*t2 + 327*Power(t2,2)))) + 
            s1*(79 - 26*Power(t1,5) - 196*t2 + 281*Power(t2,2) - 
               180*Power(t2,3) + 8*Power(t2,4) + 8*Power(t2,5) + 
               Power(s2,4)*(-3*t1 + t2) + 6*Power(t1,4)*(37 + 2*t2) + 
               8*Power(t1,3)*(1 - 49*t2 + 14*Power(t2,2)) + 
               Power(t1,2)*(-387 + 350*t2 + 126*Power(t2,2) - 
                  148*Power(t2,3)) + 
               2*t1*(42 + 53*t2 - 89*Power(t2,2) + 18*Power(t2,3) + 
                  21*Power(t2,4)) + 
               Power(s2,3)*(113 + 35*Power(t1,2) + 287*t2 + 
                  35*Power(t2,2) - t1*(259 + 66*t2)) - 
               Power(s2,2)*(113 + 87*Power(t1,3) - 140*t2 - 
                  108*Power(t2,2) + 51*Power(t2,3) - 
                  Power(t1,2)*(740 + 141*t2) + 
                  t1*(312 + 860*t2 + 3*Power(t2,2))) + 
               s2*(-87 + 81*Power(t1,4) - 262*t2 + 297*Power(t2,2) + 
                  39*Power(t2,3) - 77*Power(t2,4) - 
                  Power(t1,3)*(703 + 88*t2) + 
                  Power(t1,2)*(191 + 965*t2 - 144*Power(t2,2)) + 
                  t1*(536 - 488*t2 - 301*Power(t2,2) + 228*Power(t2,3))))\
) + Power(s,4)*(108 - 64*t1 - 544*Power(t1,2) + 355*Power(t1,3) + 
            254*Power(t1,4) - 189*Power(t1,5) + 64*Power(t1,6) - 
            8*Power(t1,7) - 236*t2 + 565*t1*t2 + 345*Power(t1,2)*t2 - 
            807*Power(t1,3)*t2 + 309*Power(t1,4)*t2 - 
            170*Power(t1,5)*t2 + 20*Power(t1,6)*t2 + 159*Power(t2,2) - 
            695*t1*Power(t2,2) + 367*Power(t1,2)*Power(t2,2) + 
            28*Power(t1,3)*Power(t2,2) + 154*Power(t1,4)*Power(t2,2) - 
            5*Power(t2,3) + 251*t1*Power(t2,3) - 
            188*Power(t1,2)*Power(t2,3) - 76*Power(t1,3)*Power(t2,3) - 
            40*Power(t1,4)*Power(t2,3) - 65*Power(t2,4) + 
            t1*Power(t2,4) + 44*Power(t1,2)*Power(t2,4) + 
            40*Power(t1,3)*Power(t2,4) + 39*Power(t2,5) - 
            10*t1*Power(t2,5) - 12*Power(t1,2)*Power(t2,5) - 
            6*Power(t2,6) - 4*Power(s1,6)*(-1 + s2 - t1 + t2) + 
            Power(s1,5)*(26 - 40*Power(s2,2) - 22*Power(t1,2) + 
               s2*(8 + 62*t1 - 6*t2) + t1*(2 - 4*t2) - 46*t2 + 
               20*Power(t2,2)) + 
            Power(s2,5)*(2 + 8*Power(t1,2) + 10*t2 + 40*Power(t2,2) - 
               5*t1*(1 + 8*t2)) + 
            Power(s2,4)*(89 - 40*Power(t1,3) + 168*t2 + 
               99*Power(t2,2) + 50*Power(t2,3) + 
               12*Power(t1,2)*(7 + 15*t2) - 
               t1*(146 + 199*t2 + 200*Power(t2,2))) + 
            Power(s2,3)*(84 + 80*Power(t1,4) + 378*t2 - 9*Power(t2,2) + 
               131*Power(t2,3) - 20*Power(t2,4) - 
               2*Power(t1,3)*(143 + 160*t2) + 
               Power(t1,2)*(615 + 707*t2 + 360*Power(t2,2)) - 
               t1*(657 + 665*t2 + 532*Power(t2,2) + 100*Power(t2,3))) + 
            Power(s2,2)*(-281 - 80*Power(t1,5) + 76*t2 + 
               369*Power(t2,2) - 95*Power(t2,3) + 69*Power(t2,4) - 
               30*Power(t2,5) + 4*Power(t1,4)*(101 + 70*t2) - 
               Power(t1,3)*(989 + 1027*t2 + 280*Power(t2,2)) + 
               Power(t1,2)*(1301 + 1135*t2 + 921*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(77 - 1385*t2 - 51*Power(t2,2) - 367*Power(t2,3) + 
                  100*Power(t2,4))) + 
            s2*(-14 + 40*Power(t1,6) - 404*t2 + 497*Power(t2,2) - 
               45*Power(t2,3) - 91*Power(t2,4) + 21*Power(t2,5) - 
               3*Power(t1,5)*(87 + 40*t2) + 
               Power(t1,4)*(707 + 679*t2 + 80*Power(t2,2)) + 
               Power(t1,3)*(-987 - 947*t2 - 642*Power(t2,2) + 
                  80*Power(t2,3)) + 
               Power(t1,2)*(-516 + 1814*t2 + 32*Power(t2,2) + 
                  312*Power(t2,3) - 120*Power(t2,4)) + 
               t1*(837 - 411*t2 - 782*Power(t2,2) + 299*Power(t2,3) - 
                  109*Power(t2,4) + 40*Power(t2,5))) - 
            Power(s1,4)*(14*Power(s2,3) + 93*Power(t1,3) + 
               Power(s2,2)*(-60 + 97*t1 - 192*t2) - 
               Power(t1,2)*(17 + 179*t2) + 
               t1*(-50 + 38*t2 + 40*Power(t2,2)) + 
               4*(8 + 13*t2 - 28*Power(t2,2) + 9*Power(t2,3)) + 
               s2*(2 - 204*Power(t1,2) + 34*t2 - 80*Power(t2,2) + 
                  t1*(73 + 383*t2))) + 
            Power(s1,2)*(9*Power(s2,5) + 18*Power(t1,5) - 
               Power(t1,4)*(421 + 134*t2) + 
               Power(s2,4)*(25 - 154*t1 + 158*t2) + 
               Power(s2,3)*(-134 + 390*Power(t1,2) + 
                  t1*(293 - 268*t2) - 294*t2 - 143*Power(t2,2)) + 
               Power(t1,3)*(205 + 755*t2 + 62*Power(t2,2)) + 
               Power(t1,2)*(294 - 233*t2 - 799*Power(t2,2) + 
                  198*Power(t2,3)) + 
               t1*(-298 + 201*t2 - 122*Power(t2,2) + 467*Power(t2,3) - 
                  136*Power(t2,4)) - 
               2*(-2 - 86*t2 + 175*Power(t2,2) - 75*Power(t2,3) + 
                  Power(t2,4) + 4*Power(t2,5)) - 
               Power(s2,2)*(73 + 336*Power(t1,3) + 238*t2 + 
                  35*Power(t2,2) + 42*Power(t2,3) + 
                  2*Power(t1,2)*(541 + 36*t2) - 
                  2*t1*(344 + 496*t2 + 225*Power(t2,2))) + 
               s2*(121 + 73*Power(t1,4) + 20*t2 + 241*Power(t2,2) - 
                  598*Power(t2,3) + 172*Power(t2,4) + 
                  79*Power(t1,3)*(15 + 4*t2) - 
                  Power(t1,2)*(759 + 1453*t2 + 369*Power(t2,2)) + 
                  t1*(-191 + 351*t2 + 926*Power(t2,2) - 192*Power(t2,3))\
)) + Power(s1,3)*(49*Power(s2,4) + 155*Power(t1,4) - 
               4*Power(t1,3)*(24 + 7*t2) + 
               Power(s2,3)*(86 - 432*t1 + 268*t2) + 
               Power(t1,2)*(-350 + 509*t2 - 301*Power(t2,2)) + 
               t1*(39 + 168*t2 - 333*Power(t2,2) + 146*Power(t2,3)) + 
               2*(5 + 16*t2 + 28*Power(t2,2) - 50*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,2)*(-119 + 872*Power(t1,2) + 96*t2 - 
                  163*Power(t2,2) - t1*(233 + 569*t2)) + 
               s2*(-644*Power(t1,3) + Power(t1,2)*(243 + 329*t2) + 
                  t1*(520 - 665*t2 + 499*Power(t2,2)) - 
                  2*(7 + 177*t2 - 250*Power(t2,2) + 102*Power(t2,3)))) + 
            s1*(-118 - 2*Power(t1,6) + Power(t1,5)*(238 - 82*t2) + 
               209*t2 - 291*Power(t2,2) + 403*Power(t2,3) - 
               219*Power(t2,4) + 38*Power(t2,5) + 
               Power(s2,5)*(-3 - 8*t1 + 17*t2) + 
               Power(s2,4)*(-193 + 30*Power(t1,2) + t1*(295 - 75*t2) - 
                  371*t2 + 23*Power(t2,2)) + 
               2*Power(t1,4)*(-75 - 143*t2 + 114*Power(t2,2)) - 
               Power(t1,3)*(681 - 726*t2 + 178*Power(t2,2) + 
                  172*Power(t2,3)) + 
               Power(t1,2)*(383 + 120*t2 - 311*Power(t2,2) + 
                  300*Power(t2,3) - 2*Power(t2,4)) + 
               2*t1*(99 - 191*t2 + 79*Power(t2,2) - 23*Power(t2,3) - 
                  56*Power(t2,4) + 15*Power(t2,5)) + 
               Power(s2,3)*(18 - 40*Power(t1,3) - 454*t2 - 
                  92*Power(t2,2) + 167*Power(t2,3) + 
                  5*Power(t1,2)*(-221 + 41*t2) + 
                  t1*(958 + 1349*t2 - 312*Power(t2,2))) + 
               Power(s2,2)*(433 + 20*Power(t1,4) + 180*t2 - 
                  503*Power(t2,2) + 15*Power(t2,3) + 123*Power(t2,4) - 
                  5*Power(t1,3)*(-315 + 67*t2) + 
                  Power(t1,2)*(-1487 - 1871*t2 + 783*Power(t2,2)) + 
                  t1*(-761 + 1622*t2 + 221*Power(t2,2) - 
                     591*Power(t2,3))) + 
               s2*(-95 + 517*t2 - 610*Power(t2,2) + 209*Power(t2,3) + 
                  127*Power(t2,4) - 38*Power(t2,5) + 
                  10*Power(t1,4)*(-100 + 27*t2) + 
                  Power(t1,3)*(872 + 1179*t2 - 722*Power(t2,2)) + 
                  Power(t1,2)*
                   (1424 - 1894*t2 + 49*Power(t2,2) + 596*Power(t2,3)) - 
                  t1*(904 + 164*t2 - 813*Power(t2,2) + 355*Power(t2,3) + 
                     106*Power(t2,4))))) + 
         Power(s,3)*(-84 + 269*t1 + 270*Power(t1,2) - 863*Power(t1,3) + 
            53*Power(t1,4) + 314*Power(t1,5) - 145*Power(t1,6) + 
            36*Power(t1,7) - 2*Power(t1,8) + 163*t2 - 808*t1*t2 + 
            444*Power(t1,2)*t2 + 1225*Power(t1,3)*t2 - 
            609*Power(t1,4)*t2 + 187*Power(t1,5)*t2 - 
            86*Power(t1,6)*t2 - 62*Power(t2,2) + 679*t1*Power(t2,2) - 
            1193*Power(t1,2)*Power(t2,2) - 340*Power(t1,3)*Power(t2,2) + 
            46*Power(t1,4)*Power(t2,2) + 66*Power(t1,5)*Power(t2,2) + 
            20*Power(t1,6)*Power(t2,2) - 20*Power(t2,3) - 
            101*t1*Power(t2,3) + 824*Power(t1,2)*Power(t2,3) + 
            52*Power(t1,3)*Power(t2,3) - 24*Power(t1,4)*Power(t2,3) - 
            40*Power(t1,5)*Power(t2,3) + 16*Power(t2,4) - 
            182*t1*Power(t2,4) - 267*Power(t1,2)*Power(t2,4) - 
            4*Power(t1,3)*Power(t2,4) + 30*Power(t1,4)*Power(t2,4) - 
            7*Power(t2,5) + 129*t1*Power(t2,5) + 
            30*Power(t1,2)*Power(t2,5) - 8*Power(t1,3)*Power(t2,5) - 
            2*Power(t2,6) - 18*t1*Power(t2,6) + 
            4*Power(s1,6)*(-3 + 4*Power(s2,2) - t1 - 7*s2*t1 + 
               3*Power(t1,2) + 3*t2 + 3*s2*t2 - 2*t1*t2) - 
            Power(s2,6)*(6 + t1 + 2*Power(t1,2) + 2*t2 - 20*t1*t2 + 
               30*Power(t2,2)) + 
            Power(s2,5)*(-81 + 12*Power(t1,3) - 103*t2 - 
               106*Power(t2,2) - 20*Power(t2,3) - 
               Power(t1,2)*(31 + 100*t2) + 
               t1*(100 + 147*t2 + 140*Power(t2,2))) + 
            Power(s2,4)*(-201 - 30*Power(t1,4) - 475*t2 + 
               110*Power(t2,2) - 174*Power(t2,3) + 50*Power(t2,4) + 
               10*Power(t1,3)*(17 + 20*t2) - 
               Power(t1,2)*(485 + 654*t2 + 240*Power(t2,2)) + 
               t1*(735 + 503*t2 + 614*Power(t2,2))) + 
            Power(s2,3)*(238 + 40*Power(t1,5) - 424*t2 - 
               232*Power(t2,2) + 96*Power(t2,3) - 86*Power(t2,4) + 
               40*Power(t2,5) - 50*Power(t1,4)*(7 + 4*t2) + 
               2*Power(t1,3)*(530 + 593*t2 + 80*Power(t2,2)) + 
               Power(t1,2)*(-2033 - 1078*t2 - 1272*Power(t2,2) + 
                  160*Power(t2,3)) + 
               t1*(755 + 1781*t2 - 224*Power(t2,2) + 562*Power(t2,3) - 
                  200*Power(t2,4))) + 
            Power(s2,2)*(331 - 30*Power(t1,6) + 258*t2 - 
               927*Power(t2,2) + 410*Power(t2,3) - 62*Power(t2,4) + 
               6*Power(t2,5) + 5*Power(t1,5)*(71 + 20*t2) + 
               2*Power(t1,4)*(-590 - 537*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(2499 + 1246*t2 + 1192*Power(t2,2) - 
                  280*Power(t2,3)) + 
               2*Power(t1,2)*
                (-427 - 1373*t2 + 82*Power(t2,2) - 313*Power(t2,3) + 
                  140*Power(t2,4)) + 
               t1*(-1339 + 2043*t2 + 227*Power(t2,2) - 
                  168*Power(t2,3) + 147*Power(t2,4) - 80*Power(t2,5))) + 
            s2*(-167 + 12*Power(t1,7) + 587*t2 - 468*Power(t2,2) - 
               88*Power(t2,3) + 301*Power(t2,4) - 163*Power(t2,5) + 
               22*Power(t2,6) - Power(t1,6)*(179 + 20*t2) + 
               Power(t1,5)*(656 + 483*t2 - 60*Power(t2,2)) + 
               Power(t1,4)*(-1434 - 755*t2 - 494*Power(t2,2) + 
                  180*Power(t2,3)) + 
               Power(t1,3)*(247 + 2049*t2 - 96*Power(t2,2) + 
                  262*Power(t2,3) - 160*Power(t2,4)) - 
               t1*(631 + 654*t2 - 2125*Power(t2,2) + 
                  1261*Power(t2,3) - 338*Power(t2,4) + 37*Power(t2,5)) \
+ Power(t1,2)*(1964 - 2844*t2 + 345*Power(t2,2) + 20*Power(t2,3) - 
                  57*Power(t2,4) + 48*Power(t2,5))) + 
            2*Power(s1,5)*(8 + 30*Power(s2,3) - 3*Power(t1,3) + 
               Power(t1,2)*(3 - 33*t2) + 
               Power(s2,2)*(6 - 61*t1 - 30*t2) + 12*t2 - 
               20*Power(t2,2) + 4*t1*(8 - 5*t2 + 6*Power(t2,2)) + 
               s2*(-38 + 34*Power(t1,2) + 34*t2 - 32*Power(t2,2) + 
                  t1*(-11 + 65*t2))) - 
            2*Power(s1,4)*(-21 + 7*Power(s2,4) + 67*Power(t1,4) + 
               65*t2 - 26*Power(t2,2) - 20*Power(t2,3) - 
               Power(t1,3)*(58 + 69*t2) + 
               Power(s2,3)*(76 - 125*t1 + 141*t2) + 
               Power(t1,2)*(-124 + 105*t2 - 56*Power(t2,2)) + 
               t1*(-5 + 148*t2 - 94*Power(t2,2) + 48*Power(t2,3)) + 
               Power(s2,2)*(-33 + 296*Power(t1,2) + 52*t2 - 
                  16*Power(t2,2) - t1*(215 + 361*t2)) + 
               s2*(-245*Power(t1,3) + Power(t1,2)*(197 + 289*t2) - 
                  3*t2*(52 - 41*t2 + 20*Power(t2,2)) + 
                  t1*(161 - 170*t2 + 79*Power(t2,2)))) - 
            Power(s1,2)*(16*Power(s2,6) - 10*Power(t1,6) + 
               Power(t1,5)*(624 - 10*t2) + 
               Power(s2,5)*(44 - 174*t1 + 166*t2) + 
               Power(s2,4)*(-57 + 233*t1 + 526*Power(t1,2) - 354*t2 - 
                  546*t1*t2 - 36*Power(t2,2)) + 
               Power(t1,4)*(-527 - 1420*t2 + 246*Power(t2,2)) + 
               Power(t1,3)*(-249 + 923*t2 + 1536*Power(t2,2) - 
                  398*Power(t2,3)) + 
               Power(t1,2)*(627 - 486*t2 - 561*Power(t2,2) - 
                  678*Power(t2,3) + 148*Power(t2,4)) + 
               2*(-14 + 50*t2 - 162*Power(t2,2) + 203*Power(t2,3) - 
                  99*Power(t2,4) + 10*Power(t2,5)) + 
               t1*(-135 + t2 + 99*Power(t2,2) + 363*Power(t2,3) - 
                  82*Power(t2,4) + 24*Power(t2,5)) - 
               Power(s2,3)*(216 + 684*Power(t1,3) + 
                  Power(t1,2)*(1587 - 652*t2) + 152*t2 + 
                  598*Power(t2,2) - 176*Power(t2,3) + 
                  t1*(-917 - 2049*t2 + 90*Power(t2,2))) + 
               Power(s2,2)*(135 + 396*Power(t1,4) + 
                  Power(t1,3)*(2923 - 340*t2) - 40*t2 + 
                  349*Power(t2,2) - 1158*Power(t2,3) + 
                  264*Power(t2,4) + 
                  Power(t1,2)*(-2190 - 4456*t2 + 534*Power(t2,2)) + 
                  t1*(298 + 805*t2 + 3001*Power(t2,2) - 
                     854*Power(t2,3))) + 
               s2*(34 - 70*Power(t1,5) + 264*t2 - 340*Power(t2,2) - 
                  224*Power(t2,3) + 72*Power(t2,4) - 28*Power(t2,5) + 
                  Power(t1,4)*(-2237 + 78*t2) + 
                  Power(t1,3)*(1857 + 4181*t2 - 654*Power(t2,2)) + 
                  Power(t1,2)*
                   (167 - 1576*t2 - 3939*Power(t2,2) + 
                     1076*Power(t2,3)) + 
                  t1*(-787 + 521*t2 + 241*Power(t2,2) + 
                     1803*Power(t2,3) - 402*Power(t2,4)))) + 
            Power(s1,3)*(-46*Power(s2,5) + 
               Power(s2,4)*(-67 + 424*t1 - 186*t2) + 
               Power(s2,3)*(122 - 1088*Power(t1,2) - 252*t2 + 
                  360*Power(t2,2) + t1*(431 + 338*t2)) + 
               Power(s2,2)*(165 + 1180*Power(t1,3) + 262*t2 - 
                  707*Power(t2,2) + 204*Power(t2,3) + 
                  Power(t1,2)*(-889 + 26*t2) - 
                  2*t1*(318 - 691*t2 + 585*Power(t2,2))) + 
               s2*(-110 - 562*Power(t1,4) + 
                  Power(t1,3)*(753 - 322*t2) + 436*t2 - 
                  818*Power(t2,2) + 328*Power(t2,3) - 96*Power(t2,4) + 
                  2*Power(t1,2)*(372 - 932*t2 + 587*Power(t2,2)) + 
                  t1*(-541 + 392*t2 + 915*Power(t2,2) - 
                     234*Power(t2,3))) + 
               2*(46*Power(t1,5) + 6*Power(t1,4)*(-19 + 12*t2) + 
                  Power(t1,3)*(-115 + 367*t2 - 182*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (87 - 153*t2 - 64*Power(t2,2) + 12*Power(t2,3)) - 
                  2*(8 + 21*t2 - 71*Power(t2,2) + 46*Power(t2,3)) + 
                  t1*(65 - 245*t2 + 431*Power(t2,2) - 145*Power(t2,3) + 
                     40*Power(t2,4)))) + 
            s1*(71 + 10*Power(t1,7) + Power(t1,6)*(106 - 78*t2) + 
               Power(s2,6)*(11 + 12*t1 - 28*t2) - 152*t2 + 
               158*Power(t2,2) - 268*Power(t2,3) + 243*Power(t2,4) - 
               76*Power(t2,5) + 8*Power(t2,6) + 
               2*Power(t1,5)*(-103 + 51*t2 + 66*Power(t2,2)) - 
               Power(t1,4)*(823 - 786*t2 + 678*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,3)*(652 + 528*t2 - 568*Power(t2,2) + 
                  724*Power(t2,3) - 78*Power(t2,4)) + 
               2*Power(t1,2)*
                (81 - 238*t2 - 252*Power(t2,2) + 218*Power(t2,3) - 
                  172*Power(t2,4) + 21*Power(t2,5)) + 
               t1*(-332 + 518*t2 - 308*Power(t2,2) + 556*Power(t2,3) - 
                  372*Power(t2,4) + 82*Power(t2,5)) + 
               Power(s2,5)*(209 - 70*Power(t1,2) + 270*t2 - 
                  72*Power(t2,2) + t1*(-227 + 188*t2)) + 
               2*Power(s2,4)*
                (106 + 85*Power(t1,3) + Power(t1,2)*(452 - 275*t2) + 
                  368*t2 - 60*Power(t2,2) - 99*Power(t2,3) + 
                  t1*(-676 - 531*t2 + 255*Power(t2,2))) + 
               Power(s2,3)*(-592 - 220*Power(t1,4) + 80*t2 + 
                  794*Power(t2,2) - 382*Power(t2,3) - 82*Power(t2,4) + 
                  2*Power(t1,3)*(-783 + 440*t2) + 
                  Power(t1,2)*(3008 + 1464*t2 - 1230*Power(t2,2)) + 
                  t1*(95 - 3008*t2 + 812*Power(t2,2) + 692*Power(t2,3))) \
+ Power(s2,2)*(-268 + 160*Power(t1,5) + Power(t1,4)*(1379 - 800*t2) - 
                  352*t2 + 411*Power(t2,2) + 250*Power(t2,3) - 
                  461*Power(t2,4) + 72*Power(t2,5) + 
                  2*Power(t1,3)*(-1501 - 360*t2 + 675*Power(t2,2)) - 
                  Power(t1,2)*
                   (1649 - 4594*t2 + 1942*Power(t2,2) + 
                     818*Power(t2,3)) + 
                  2*t1*(1022 + 4*t2 - 1097*Power(t2,2) + 
                     812*Power(t2,3) + 18*Power(t2,4))) + 
               s2*(265 - 62*Power(t1,6) - 602*t2 + 742*Power(t2,2) - 
                  914*Power(t2,3) + 497*Power(t2,4) - 100*Power(t2,5) + 
                  Power(t1,5)*(-607 + 388*t2) + 
                  Power(t1,4)*(1343 - 54*t2 - 690*Power(t2,2)) + 
                  Power(t1,3)*
                   (2165 - 3108*t2 + 1928*Power(t2,2) + 352*Power(t2,3)) \
+ 2*Power(t1,2)*(-1052 - 308*t2 + 984*Power(t2,2) - 983*Power(t2,3) + 
                     62*Power(t2,4)) + 
                  t1*(160 + 684*t2 + 185*Power(t2,2) - 700*Power(t2,3) + 
                     799*Power(t2,4) - 112*Power(t2,5))))) + 
         Power(s,2)*(24 - 206*t1 + 165*Power(t1,2) + 568*Power(t1,3) - 
            541*Power(t1,4) - 147*Power(t1,5) + 151*Power(t1,6) - 
            56*Power(t1,7) + 8*Power(t1,8) - 46*t2 + 481*t1*t2 - 
            922*Power(t1,2)*t2 - 366*Power(t1,3)*t2 + 
            987*Power(t1,4)*t2 - 59*Power(t1,5)*t2 + 58*Power(t1,6)*t2 - 
            10*Power(t1,7)*t2 - 2*Power(t1,8)*t2 + 2*Power(t2,2) - 
            280*t1*Power(t2,2) + 1173*Power(t1,2)*Power(t2,2) - 
            307*Power(t1,3)*Power(t2,2) - 671*Power(t1,4)*Power(t2,2) - 
            59*Power(t1,5)*Power(t2,2) - 14*Power(t1,6)*Power(t2,2) + 
            8*Power(t1,7)*Power(t2,2) + 34*Power(t2,3) - 
            82*t1*Power(t2,3) - 751*Power(t1,2)*Power(t2,3) + 
            552*Power(t1,3)*Power(t2,3) + 309*Power(t1,4)*Power(t2,3) + 
            40*Power(t1,5)*Power(t2,3) - 12*Power(t1,6)*Power(t2,3) - 
            4*Power(t2,4) + 250*t1*Power(t2,4) + 
            160*Power(t1,2)*Power(t2,4) - 397*Power(t1,3)*Power(t2,4) - 
            56*Power(t1,4)*Power(t2,4) + 8*Power(t1,5)*Power(t2,4) - 
            32*Power(t2,5) - 155*t1*Power(t2,5) + 
            149*Power(t1,2)*Power(t2,5) + 50*Power(t1,3)*Power(t2,5) - 
            2*Power(t1,4)*Power(t2,5) + 22*Power(t2,6) - 
            4*t1*Power(t2,6) - 18*Power(t1,2)*Power(t2,6) + 
            Power(s2,7)*(6 + t1 - 2*t2 - 4*t1*t2 + 12*Power(t2,2)) + 
            Power(s2,6)*(55 + 20*t2 + 70*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,2)*(2 + 22*t2) - 
               t1*(47 + 55*t2 + 52*Power(t2,2))) + 
            Power(s2,5)*(159 + 302*t2 - 92*Power(t2,2) + 
               120*Power(t2,3) - 48*Power(t2,4) - 
               3*Power(t1,3)*(11 + 16*t2) + 
               3*Power(t1,2)*(67 + 105*t2 + 24*Power(t2,2)) + 
               t1*(-449 - 158*t2 - 402*Power(t2,2) + 72*Power(t2,3))) + 
            Power(s2,4)*(-26 + 484*t2 - 143*Power(t2,2) + 
               107*Power(t2,3) + 34*Power(t2,4) - 30*Power(t2,5) + 
               50*Power(t1,4)*(2 + t2) - 170*Power(t1,3)*(3 + 4*t2) + 
               Power(t1,2)*(1397 + 490*t2 + 894*Power(t2,2) - 
                  240*Power(t2,3)) + 
               t1*(-968 - 1068*t2 + 165*Power(t2,2) - 404*Power(t2,3) + 
                  200*Power(t2,4))) - 
            Power(s2,2)*(39 + 6*Power(t1,6)*(-19 + t2) + 455*t2 - 
               658*Power(t2,2) + 148*Power(t2,3) + 273*Power(t2,4) - 
               275*Power(t2,5) + 30*Power(t2,6) + 
               Power(t1,5)*(651 + 423*t2 - 108*Power(t2,2)) + 
               Power(t1,4)*(-1877 - 680*t2 - 474*Power(t2,2) + 
                  270*Power(t2,3)) + 
               Power(t1,3)*(2073 + 965*t2 + 418*Power(t2,2) + 
                  132*Power(t2,3) - 240*Power(t2,4)) + 
               t1*(-1416 + 634*t2 + 1854*Power(t2,2) - 
                  1941*Power(t2,3) + 1118*Power(t2,4) - 141*Power(t2,5)\
) + Power(t1,2)*(1581 - 4293*t2 + 2280*Power(t2,2) - 1232*Power(t2,3) + 
                  144*Power(t2,4) + 72*Power(t2,5))) + 
            Power(s2,3)*(-393 + 74*t2 + 781*Power(t2,2) - 
               670*Power(t2,3) + 352*Power(t2,4) - 44*Power(t2,5) - 
               5*Power(t1,5)*(29 + 4*t2) - 
               20*Power(t1,4)*(-38 - 37*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(-2198 - 780*t2 - 956*Power(t2,2) + 
                  360*Power(t2,3)) + 
               Power(t1,2)*(2097 + 1451*t2 + 116*Power(t2,2) + 
                  452*Power(t2,3) - 320*Power(t2,4)) + 
               t1*(559 - 2379*t2 + 1019*Power(t2,2) - 622*Power(t2,3) - 
                  7*Power(t2,4) + 80*Power(t2,5))) + 
            s2*(170 - 391*t2 + 194*Power(t2,2) + 122*Power(t2,3) - 
               246*Power(t2,4) + 137*Power(t2,5) + 10*Power(t2,6) + 
               Power(t1,7)*(-47 + 8*t2) + 
               Power(t1,6)*(297 + 115*t2 - 48*Power(t2,2)) + 
               Power(t1,5)*(-833 - 310*t2 - 66*Power(t2,2) + 
                  96*Power(t2,3)) + 
               Power(t1,4)*(932 + 339*t2 + 288*Power(t2,2) - 
                  76*Power(t2,3) - 80*Power(t2,4)) + 
               Power(t1,2)*(-1591 + 926*t2 + 1380*Power(t2,2) - 
                  1823*Power(t2,3) + 1163*Power(t2,4) - 147*Power(t2,5)\
) + Power(t1,3)*(1589 - 3385*t2 + 2075*Power(t2,2) - 
                  1026*Power(t2,3) + 173*Power(t2,4) + 24*Power(t2,5)) \
+ t1*(-114 + 1349*t2 - 1819*Power(t2,2) + 909*Power(t2,3) + 
                  105*Power(t2,4) - 422*Power(t2,5) + 48*Power(t2,6))) - 
            4*Power(s1,6)*(-2 + 6*Power(s2,3) + 3*t1 + 7*Power(t1,2) - 
               3*Power(t1,3) + 2*t2 - 5*t1*t2 + 
               Power(s2,2)*(5 - 15*t1 + 3*t2) + 
               s2*(-4 + 12*Power(t1,2) + 6*t2 - 3*t1*(4 + t2))) - 
            2*Power(s1,5)*(20*Power(s2,4) - 7*Power(t1,4) + 
               Power(t1,3)*(29 + 37*t2) - 
               Power(s2,3)*(1 + 47*t1 + 58*t2) - 
               2*(-11 + 10*t2 + Power(t2,2)) - 
               4*Power(t1,2)*(16 + 8*t2 + 3*Power(t2,2)) + 
               t1*(-5 + t2 + 32*Power(t2,2)) + 
               Power(s2,2)*(-67 + 27*Power(t1,2) - 7*t2 - 
                  36*Power(t2,2) + 3*t1*(9 + 53*t2)) + 
               s2*(5 + 7*Power(t1,3) + 5*t2 - 38*Power(t2,2) - 
                  Power(t1,2)*(55 + 138*t2) + 
                  t1*(129 + 41*t2 + 48*Power(t2,2)))) + 
            2*Power(s1,4)*(21 + 13*Power(s2,5) + 45*Power(t1,4) - 
               42*Power(t1,5) + 32*t2 - 75*Power(t2,2) + 
               22*Power(t2,3) + Power(s2,4)*(77 - 129*t1 + 87*t2) + 
               Power(t1,3)*(260 - 57*t2 + 88*Power(t2,2)) + 
               t1*(-8 + 25*t2 + 33*Power(t2,2) + 28*Power(t2,3)) - 
               Power(t1,2)*(22 + 323*t2 - 42*Power(t2,2) + 
                  36*Power(t2,3)) + 
               Power(s2,3)*(-103 + 351*Power(t1,2) + 64*t2 - 
                  88*Power(t2,2) - t1*(296 + 255*t2)) + 
               Power(s2,2)*(-8 - 409*Power(t1,3) - 321*t2 + 
                  98*Power(t2,2) - 72*Power(t2,3) + 
                  Power(t1,2)*(406 + 249*t2) + 
                  5*t1*(94 - 42*t2 + 57*Power(t2,2))) + 
               s2*(-6 + 216*Power(t1,4) - 5*t2 - 35*Power(t2,2) - 
                  32*Power(t2,3) - Power(t1,3)*(232 + 81*t2) + 
                  Power(t1,2)*(-627 + 203*t2 - 285*Power(t2,2)) + 
                  t1*(31 + 636*t2 - 133*Power(t2,2) + 108*Power(t2,3)))) \
+ Power(s1,3)*(24*Power(s2,6) + 31*Power(t1,6) + 
               Power(s2,5)*(5 - 235*t1 + 65*t2) + 
               Power(t1,5)*(-237 + 110*t2) + 
               Power(t1,4)*(410 + 599*t2 - 141*Power(t2,2)) + 
               2*t2*(-63 + 44*t2 + 57*Power(t2,2) - 38*Power(t2,3)) - 
               4*Power(t1,3)*
                (-112 + 496*t2 + 11*Power(t2,2) + 18*Power(t2,3)) + 
               4*t1*(17 - 61*t2 + 21*Power(t2,2) - 35*Power(t2,3) + 
                  4*Power(t2,4)) + 
               2*Power(t1,2)*
                (112 - 258*t2 + 842*Power(t2,2) - 187*Power(t2,3) + 
                  36*Power(t2,4)) + 
               Power(s2,4)*(54 + 731*Power(t1,2) + 271*t2 - 
                  277*Power(t2,2) - 7*t1*(43 + 6*t2)) - 
               Power(s2,3)*(223 + 1054*Power(t1,3) - 470*t2 - 
                  509*Power(t2,2) + 48*Power(t2,3) + 
                  2*Power(t1,2)*(-555 + 187*t2) + 
                  t1*(372 + 1538*t2 - 1020*Power(t2,2))) + 
               2*Power(s2,2)*
                (112 + 383*Power(t1,4) - 314*t2 + 804*Power(t2,2) - 
                  233*Power(t2,3) + 60*Power(t2,4) + 
                  Power(t1,3)*(-787 + 362*t2) + 
                  Power(t1,2)*(496 + 1431*t2 - 675*Power(t2,2)) - 
                  t1*(-488 + 1505*t2 + 486*Power(t2,2) + 
                     15*Power(t2,3))) - 
               s2*(263*Power(t1,5) + Power(t1,4)*(-997 + 483*t2) + 
                  Power(t1,3)*(1084 + 2194*t2 - 748*Power(t2,2)) - 
                  Power(t1,2)*
                   (-1201 + 4524*t2 + 507*Power(t2,2) + 
                     150*Power(t2,3)) + 
                  4*(14 - 75*t2 + 46*Power(t2,2) - 45*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  2*t1*(224 - 571*t2 + 1636*Power(t2,2) - 
                     411*Power(t2,3) + 96*Power(t2,4)))) + 
            Power(s1,2)*(14*Power(s2,7) - 5*Power(t1,7) + 
               Power(t1,6)*(-511 + 83*t2) + 
               Power(s2,6)*(54 - 137*t1 + 119*t2) + 
               Power(t1,5)*(855 + 1274*t2 - 267*Power(t2,2)) + 
               Power(s2,5)*(33 + 480*Power(t1,2) + t1*(24 - 606*t2) - 
                  283*t2 + 72*Power(t2,2)) + 
               2*Power(-1 + t2,2)*
                (-9 + 15*t2 + 62*Power(t2,2) + 22*Power(t2,3)) + 
               Power(t1,4)*(15 - 2059*t2 - 1299*Power(t2,2) + 
                  281*Power(t2,3)) + 
               Power(t1,3)*(-229 + 320*t2 + 2021*Power(t2,2) + 
                  362*Power(t2,3) - 68*Power(t2,4)) + 
               Power(t1,2)*(537 - 1367*t2 + 777*Power(t2,2) - 
                  1079*Power(t2,3) + 218*Power(t2,4) - 24*Power(t2,5)) \
- 2*t1*(-7 + 251*t2 - 707*Power(t2,2) + 474*Power(t2,3) - 
                  109*Power(t2,4) + 22*Power(t2,5)) - 
               Power(s2,4)*(214 + 835*Power(t1,3) + 
                  Power(t1,2)*(1147 - 1317*t2) + 168*t2 + 
                  843*Power(t2,2) - 245*Power(t2,3) + 
                  t1*(-754 - 2183*t2 + 603*Power(t2,2))) + 
               Power(s2,3)*(-193 + 790*Power(t1,4) + 88*t2 - 
                  185*Power(t2,2) - 1032*Power(t2,3) + 
                  200*Power(t2,4) - 4*Power(t1,3)*(-817 + 397*t2) + 
                  Power(t1,2)*(-3315 - 6125*t2 + 1644*Power(t2,2)) + 
                  t1*(774 + 1999*t2 + 4143*Power(t2,2) - 
                     1112*Power(t2,3))) + 
               Power(s2,2)*(228 - 399*Power(t1,5) - 624*t2 + 
                  354*Power(t2,2) - 790*Power(t2,3) + 
                  214*Power(t2,4) - 36*Power(t2,5) + 
                  3*Power(t1,4)*(-1324 + 379*t2) + 
                  Power(t1,3)*(5091 + 8107*t2 - 2034*Power(t2,2)) + 
                  3*Power(t1,2)*
                   (-297 - 1851*t2 - 2352*Power(t2,2) + 
                     590*Power(t2,3)) + 
                  t1*(112 + 93*t2 + 2460*Power(t2,2) + 
                     2367*Power(t2,3) - 438*Power(t2,4))) + 
               s2*(92*Power(t1,6) + Power(t1,5)*(2284 - 462*t2) + 
                  2*Power(t1,4)*(-1709 - 2578*t2 + 594*Power(t2,2)) + 
                  Power(t1,3)*
                   (316 + 5781*t2 + 5055*Power(t2,2) - 
                     1184*Power(t2,3)) + 
                  Power(t1,2)*
                   (310 - 501*t2 - 4296*Power(t2,2) - 
                     1697*Power(t2,3) + 306*Power(t2,4)) + 
                  2*(-18 + 261*t2 - 729*Power(t2,2) + 
                     508*Power(t2,3) - 126*Power(t2,4) + 28*Power(t2,5)\
) + t1*(-781 + 2021*t2 - 1149*Power(t2,2) + 1863*Power(t2,3) - 
                     422*Power(t2,4) + 60*Power(t2,5)))) + 
            s1*(-14 + 4*Power(t1,8) + 52*t2 - 88*Power(t2,2) + 
               30*Power(t2,3) + 98*Power(t2,4) - 70*Power(t2,5) - 
               8*Power(t2,6) - 2*Power(t1,7)*(1 + 9*t2) + 
               Power(s2,7)*(-15 - 6*t1 + 20*t2) + 
               Power(t1,6)*(-71 + 262*t2 + 4*Power(t2,2)) + 
               Power(s2,6)*(-157 + 40*Power(t1,2) - 144*t1*(-1 + t2) - 
                  94*t2 + 58*Power(t2,2)) + 
               Power(t1,5)*(-664 + 322*t2 - 702*Power(t2,2) + 
                  56*Power(t2,3)) + 
               Power(t1,4)*(904 + 535*t2 - 325*Power(t2,2) + 
                  712*Power(t2,3) - 72*Power(t2,4)) + 
               2*Power(t1,3)*
                (84 - 816*t2 + 69*Power(t2,2) + 186*Power(t2,3) - 
                  170*Power(t2,4) + 13*Power(t2,5)) + 
               2*Power(t1,2)*
                (-186 + 85*t2 + 792*Power(t2,2) - 445*Power(t2,3) - 
                  82*Power(t2,4) + 27*Power(t2,5)) + 
               t1*(165 - 284*t2 + 494*Power(t2,2) - 1264*Power(t2,3) + 
                  951*Power(t2,4) - 126*Power(t2,5) + 16*Power(t2,6)) + 
               Power(s2,5)*(-272 - 114*Power(t1,3) - 695*t2 + 
                  308*Power(t2,2) + 114*Power(t2,3) + 
                  Power(t1,2)*(-493 + 438*t2) + 
                  t1*(1108 + 290*t2 - 354*Power(t2,2))) + 
               Power(s2,4)*(406 + 180*Power(t1,4) + 
                  Power(t1,3)*(830 - 730*t2) - 211*t2 - 
                  885*Power(t2,2) + 646*Power(t2,3) + 8*Power(t2,4) + 
                  7*Power(t1,2)*(-419 + 6*t2 + 120*Power(t2,2)) - 
                  6*t1*(-105 - 530*t2 + 316*Power(t2,2) + 
                     65*Power(t2,3))) + 
               Power(s2,3)*(496 - 170*Power(t1,5) + 378*t2 - 
                  273*Power(t2,2) - 688*Power(t2,3) + 581*Power(t2,4) - 
                  68*Power(t2,5) + Power(t1,4)*(-745 + 720*t2) - 
                  4*Power(t1,3)*(-948 + 297*t2 + 245*Power(t2,2)) + 
                  Power(t1,2)*
                   (406 - 5692*t2 + 4542*Power(t2,2) + 430*Power(t2,3)) \
+ 2*t1*(-1135 + 220*t2 + 1538*Power(t2,2) - 1419*Power(t2,3) + 
                     54*Power(t2,4))) + 
               Power(s2,2)*(-147 + 96*Power(t1,6) + 
                  Power(t1,5)*(340 - 420*t2) + 342*t2 + 
                  282*Power(t2,2) + 170*Power(t2,3) - 513*Power(t2,4) + 
                  92*Power(t2,5) + 
                  Power(t1,4)*(-2503 + 1822*t2 + 570*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (1139 - 2473*t2 + 2665*Power(t2,2) + 45*Power(t2,3)) \
+ Power(t1,2)*(4226 + 288*t2 - 3822*Power(t2,2) + 4450*Power(t2,3) - 
                     312*Power(t2,4)) + 
                  2*t1*(-473 - 1030*t2 + 243*Power(t2,2) + 
                     887*Power(t2,3) - 747*Power(t2,4) + 78*Power(t2,5))\
) - s2*(159 + 30*Power(t1,7) + Power(t1,6)*(59 - 134*t2) - 320*t2 + 
                  566*Power(t2,2) - 1288*Power(t2,3) + 941*Power(t2,4) - 
                  126*Power(t2,5) + 20*Power(t2,6) + 
                  2*Power(t1,5)*(-382 + 567*t2 + 69*Power(t2,2)) + 
                  3*Power(t1,4)*
                   (-726 + 687*t2 - 1026*Power(t2,2) + 40*Power(t2,3)) + 
                  Power(t1,3)*
                   (3266 + 1052*t2 - 1956*Power(t2,2) + 
                     2970*Power(t2,3) - 268*Power(t2,4)) + 
                  Power(t1,2)*
                   (-282 - 3314*t2 + 351*Power(t2,2) + 
                     1458*Power(t2,3) - 1253*Power(t2,4) + 
                     114*Power(t2,5)) + 
                  t1*(-511 + 472*t2 + 1918*Power(t2,2) - 
                     746*Power(t2,3) - 673*Power(t2,4) + 148*Power(t2,5))\
))) + s*(48*t1 - 160*Power(t1,2) - 5*Power(t1,3) + 390*Power(t1,4) - 
            122*Power(t1,5) - 53*Power(t1,6) + 24*Power(t1,7) - 
            8*Power(t1,8) - 92*t1*t2 + 353*Power(t1,2)*t2 - 
            516*Power(t1,3)*t2 - 503*Power(t1,4)*t2 + 
            147*Power(t1,5)*t2 + 62*Power(t1,6)*t2 + 6*Power(t1,7)*t2 + 
            4*Power(t1,8)*t2 + 124*t1*Power(t2,2) - 
            38*Power(t1,2)*Power(t2,2) + 781*Power(t1,3)*Power(t2,2) + 
            437*Power(t1,4)*Power(t2,2) - 180*Power(t1,5)*Power(t2,2) - 
            59*Power(t1,6)*Power(t2,2) - 14*Power(t1,7)*Power(t2,2) - 
            40*Power(t2,3) - 228*t1*Power(t2,3) - 
            188*Power(t1,2)*Power(t2,3) - 707*Power(t1,3)*Power(t2,3) - 
            139*Power(t1,4)*Power(t2,3) + 195*Power(t1,5)*Power(t2,3) + 
            24*Power(t1,6)*Power(t2,3) + 92*Power(t2,4) + 
            168*t1*Power(t2,4) + 204*Power(t1,2)*Power(t2,4) + 
            386*Power(t1,3)*Power(t2,4) - 199*Power(t1,4)*Power(t2,4) - 
            28*Power(t1,5)*Power(t2,4) - 64*Power(t2,5) - 
            40*t1*Power(t2,5) - 173*Power(t1,2)*Power(t2,5) + 
            67*Power(t1,3)*Power(t2,5) + 20*Power(t1,4)*Power(t2,5) + 
            12*Power(t2,6) + 20*t1*Power(t2,6) - 
            2*Power(t1,2)*Power(t2,6) - 6*Power(t1,3)*Power(t2,6) + 
            Power(s2,8)*(-2 + t2 - 2*Power(t2,2)) + 
            4*Power(s1,6)*(s2 - t1)*
             (4*Power(s2,3) - Power(t1,3) + Power(t1,2)*(7 - 2*t2) + 
               2*(-1 + t2) - t1*(2 + t2) + Power(s2,2)*(6 - 9*t1 + t2) + 
               s2*(6*Power(t1,2) + t1*(-13 + t2) + 3*t2)) + 
            Power(s2,7)*(-21 + 8*t2 - 27*Power(t2,2) + 8*Power(t2,3) + 
               t1*(12 + 7*t2 + 8*Power(t2,2))) - 
            Power(s2,6)*(62 + 56*t2 - 6*Power(t2,2) + 43*Power(t2,3) - 
               22*Power(t2,4) + 
               Power(t1,2)*(38 + 59*t2 + 6*Power(t2,2)) + 
               t1*(-143 + 12*t2 - 145*Power(t2,2) + 52*Power(t2,3))) + 
            Power(s2,5)*(-31 - 267*t2 + 292*Power(t2,2) - 
               207*Power(t2,3) + 3*Power(t2,4) + 12*Power(t2,5) + 
               Power(t1,3)*(88 + 151*t2 - 20*Power(t2,2)) + 
               2*Power(t1,2)*
                (-212 - 33*t2 - 153*Power(t2,2) + 70*Power(t2,3)) + 
               t1*(438 + 137*t2 + 104*Power(t2,2) + 142*Power(t2,3) - 
                  100*Power(t2,4))) + 
            Power(s2,4)*(185 - 153*t2 - 293*Power(t2,2) + 
               475*Power(t2,3) - 364*Power(t2,4) + 36*Power(t2,5) + 
               5*Power(t1,4)*(-30 - 37*t2 + 10*Power(t2,2)) + 
               10*Power(t1,3)*
                (71 + 23*t2 + 30*Power(t2,2) - 20*Power(t2,3)) + 
               Power(t1,2)*(-1185 + 74*t2 - 535*Power(t2,2) - 
                  114*Power(t2,3) + 180*Power(t2,4)) + 
               t1*(74 + 1145*t2 - 1311*Power(t2,2) + 1027*Power(t2,3) - 
                  71*Power(t2,4) - 40*Power(t2,5))) + 
            Power(s2,3)*(107 + 293*t2 - 612*Power(t2,2) + 
               324*Power(t2,3) + 63*Power(t2,4) - 217*Power(t2,5) + 
               18*Power(t2,6) + 
               Power(t1,5)*(172 + 109*t2 - 48*Power(t2,2)) + 
               5*Power(t1,4)*
                (-145 - 60*t2 - 19*Power(t2,2) + 32*Power(t2,3)) - 
               2*Power(t1,3)*
                (-800 + 273*t2 - 490*Power(t2,2) + 52*Power(t2,3) + 
                  80*Power(t2,4)) + 
               t1*(-979 + 1058*t2 + 397*Power(t2,2) - 
                  1303*Power(t2,3) + 1298*Power(t2,4) - 131*Power(t2,5)) \
+ Power(t1,2)*(86 - 1980*t2 + 2361*Power(t2,2) - 2034*Power(t2,3) + 
                  223*Power(t2,4) + 48*Power(t2,5))) + 
            Power(s2,2)*(-88 + 173*t2 + 182*Power(t2,2) - 
               424*Power(t2,3) + 340*Power(t2,4) - 173*Power(t2,5) - 
               14*Power(t2,6) + 
               Power(t1,6)*(-122 - 17*t2 + 22*Power(t2,2)) + 
               Power(t1,5)*(451 + 192*t2 - 63*Power(t2,2) - 
                  68*Power(t2,3)) + 
               Power(t1,4)*(-1140 + 664*t2 - 860*Power(t2,2) + 
                  221*Power(t2,3) + 70*Power(t2,4)) - 
               Power(t1,3)*(392 - 1740*t2 + 2137*Power(t2,2) - 
                  2010*Power(t2,3) + 273*Power(t2,4) + 24*Power(t2,5)) + 
               Power(t1,2)*(1793 - 2160*t2 + 522*Power(t2,2) + 
                  1042*Power(t2,3) - 1703*Power(t2,4) + 174*Power(t2,5)) \
+ t1*(-243 - 1046*t2 + 1981*Power(t2,2) - 1375*Power(t2,3) + 
                  276*Power(t2,4) + 497*Power(t2,5) - 42*Power(t2,6))) + 
            s2*(Power(t1,7)*(48 - 11*t2 - 4*Power(t2,2)) + 
               2*Power(t1,6)*
                (-79 - 29*t2 + 30*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,5)*(402 - 335*t2 + 364*Power(t2,2) - 
                  126*Power(t2,3) - 12*Power(t2,4)) + 
               Power(t1,4)*(385 - 785*t2 + 975*Power(t2,2) - 
                  991*Power(t2,3) + 146*Power(t2,4) + 4*Power(t2,5)) - 
               Power(t1,3)*(1389 - 1758*t2 + 1063*Power(t2,2) + 
                  75*Power(t2,3) - 968*Power(t2,4) + 99*Power(t2,5)) - 
               4*(12 - 23*t2 + 31*Power(t2,2) - 61*Power(t2,3) + 
                  52*Power(t2,4) - 18*Power(t2,5) + 7*Power(t2,6)) + 
               2*t1*(124 - 263*t2 - 72*Power(t2,2) + 306*Power(t2,3) - 
                  272*Power(t2,4) + 173*Power(t2,5) + 8*Power(t2,6)) + 
               Power(t1,2)*(141 + 1269*t2 - 2150*Power(t2,2) + 
                  1758*Power(t2,3) - 725*Power(t2,4) - 347*Power(t2,5) + 
                  30*Power(t2,6))) + 
            2*Power(s1,5)*(5*Power(s2,5) + 4*Power(t1,5) + 
               8*Power(-1 + t2,2) - 2*Power(t1,4)*(19 + 4*t2) - 
               Power(s2,4)*(11 + 10*t1 + 39*t2) + 
               Power(t1,3)*(75 + 32*t2 - 8*Power(t2,2)) - 
               4*t1*(5 - 6*t2 + Power(t2,2)) - 
               Power(t1,2)*(19 + 21*t2 + 4*Power(t2,2)) - 
               Power(s2,3)*(65 + 4*Power(t1,2) + 24*t2 + 
                  16*Power(t2,2) - t1*(69 + 131*t2)) + 
               Power(s2,2)*(-19 + 22*Power(t1,3) - 9*t2 - 
                  16*Power(t2,2) - Power(t1,2)*(143 + 153*t2) + 
                  3*t1*(67 + 28*t2 + 8*Power(t2,2))) + 
               s2*(22 - 17*Power(t1,4) - 28*t2 + 6*Power(t2,2) + 
                  3*Power(t1,3)*(41 + 23*t2) - 
                  Power(t1,2)*(211 + 92*t2) + 
                  t1*(38 + 30*t2 + 20*Power(t2,2)))) + 
            Power(s1,4)*(-13*Power(s2,6) - 27*Power(t1,6) + 
               Power(s2,5)*(-63 + 118*t1 - 39*t2) + 
               Power(t1,5)*(1 - 19*t2) - 
               20*Power(-1 + t2,2)*(3 + 2*t2) + 
               4*Power(t1,3)*(-50 - 141*t2 + 13*Power(t2,2)) + 
               2*Power(t1,4)*(242 + 5*t2 + 28*Power(t2,2)) + 
               Power(t1,2)*(14 + 254*t2 - 24*Power(t2,2) - 
                  8*Power(t2,3)) + 
               t1*(58 - 60*t2 - 70*Power(t2,2) + 72*Power(t2,3)) + 
               2*Power(s2,3)*
                (63 + 278*Power(t1,3) + 260*t2 - 51*Power(t2,2) + 
                  36*Power(t2,3) - Power(t1,2)*(242 + 25*t2) + 
                  t1*(-574 + 78*t2 - 253*Power(t2,2))) + 
               Power(s2,4)*(-369*Power(t1,2) + t1*(287 + 101*t2) + 
                  8*(28 - 6*t2 + 17*Power(t2,2))) + 
               Power(s2,2)*(44 - 439*Power(t1,4) + 
                  Power(t1,3)*(360 - 66*t2) + 218*t2 - 34*Power(t2,2) + 
                  8*Power(t2,3) + 
                  2*Power(t1,2)*(1054 - 79*t2 + 330*Power(t2,2)) - 
                  12*t1*(38 + 131*t2 - 19*Power(t2,2) + 12*Power(t2,3))) \
+ s2*(-58 + 174*Power(t1,5) + 44*t2 + 102*Power(t2,2) - 88*Power(t2,3) + 
                  Power(t1,4)*(-101 + 73*t2) + 
                  t1*(-58 - 472*t2 + 58*Power(t2,2)) - 
                  2*Power(t1,3)*(834 - 20*t2 + 173*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (265 + 808*t2 - 89*Power(t2,2) + 36*Power(t2,3)))) + 
            Power(s1,3)*(-7*Power(s2,7) + 8*Power(t1,7) + 
               Power(s2,6)*(12 + 75*t1 - 13*t2) + 
               Power(t1,6)*(-131 + 26*t2) - 
               Power(s2,5)*(147 + 278*Power(t1,2) + 4*t1*(-27 + t2) + 
                  147*t2 - 88*Power(t2,2)) + 
               Power(t1,5)*(606 + 264*t2 - 16*Power(t2,2)) + 
               8*Power(-1 + t2,2)*(5 + 21*t2 + 3*Power(t2,2)) + 
               Power(t1,4)*(65 - 1770*t2 + 93*Power(t2,2) - 
                  34*Power(t2,3)) + 
               2*Power(t1,3)*
                (147 + 24*t2 + 557*Power(t2,2) - 139*Power(t2,3) + 
                  8*Power(t2,4)) + 
               2*Power(t1,2)*
                (-1 - 222*t2 + 5*Power(t2,2) + 54*Power(t2,3) + 
                  16*Power(t2,4)) - 
               2*t1*(80 - 137*t2 - 34*Power(t2,2) + 47*Power(t2,3) + 
                  44*Power(t2,4)) + 
               Power(s2,4)*(510*Power(t1,3) + 
                  Power(t1,2)*(-683 + 172*t2) + 
                  t1*(1076 + 912*t2 - 346*Power(t2,2)) - 
                  2*(-12 + 325*t2 + 76*Power(t2,2) + 15*Power(t2,3))) + 
               Power(s2,3)*(-316 - 515*Power(t1,4) + 218*t2 - 
                  1076*Power(t2,2) + 348*Power(t2,3) - 64*Power(t2,4) - 
                  4*Power(t1,3)*(-353 + 97*t2) + 
                  Power(t1,2)*(-2952 - 2118*t2 + 526*Power(t2,2)) + 
                  t1*(-217 + 3766*t2 + 327*Power(t2,2) + 
                     178*Power(t2,3))) - 
               s2*(80*Power(t1,6) + 40*Power(t1,5)*(-17 + 4*t2) + 
                  Power(t1,4)*(2453 + 1263*t2 - 130*Power(t2,2)) + 
                  Power(t1,3)*
                   (299 - 6006*t2 + 163*Power(t2,2) - 186*Power(t2,3)) \
+ 2*t1*(4 - 485*t2 + 50*Power(t2,2) + 95*Power(t2,3) + 
                     40*Power(t2,4)) + 
                  2*Power(t1,2)*
                   (452 - 59*t2 + 1632*Power(t2,2) - 434*Power(t2,3) + 
                     48*Power(t2,4)) - 
                  2*(72 - 117*t2 - 38*Power(t2,2) + 27*Power(t2,3) + 
                     56*Power(t2,4))) + 
               Power(s2,2)*(10 + 287*Power(t1,5) - 526*t2 + 
                  90*Power(t2,2) + 82*Power(t2,3) + 48*Power(t2,4) + 
                  Power(t1,4)*(-1398 + 367*t2) + 
                  Power(t1,3)*(3870 + 2352*t2 - 382*Power(t2,2)) - 
                  Power(t1,2)*
                   (-427 + 7352*t2 + 105*Power(t2,2) + 300*Power(t2,3)) \
+ 2*t1*(463 - 192*t2 + 1613*Power(t2,2) - 469*Power(t2,3) + 
                     72*Power(t2,4)))) + 
            Power(s1,2)*(-6*Power(s2,8) - 3*Power(t1,8) + 
               Power(s2,7)*(-32 + 59*t1 - 48*t2) + 
               Power(t1,7)*(-218 + 37*t2) + 
               Power(t1,6)*(656 + 551*t2 - 97*Power(t2,2)) + 
               8*Power(-1 + t2,2)*t2*(-15 - 18*t2 + Power(t2,2)) + 
               Power(t1,5)*(-145 - 1748*t2 - 480*Power(t2,2) + 
                  87*Power(t2,3)) + 
               Power(t1,4)*(367 + 49*t2 + 1763*Power(t2,2) - 
                  7*Power(t2,3) - 16*Power(t2,4)) + 
               Power(t1,3)*(299 - 1823*t2 + 1139*Power(t2,2) - 
                  757*Power(t2,3) + 182*Power(t2,4) - 8*Power(t2,5)) - 
               2*Power(t1,2)*
                (88 - 152*t2 - 574*Power(t2,2) + 565*Power(t2,3) - 
                  27*Power(t2,4) + 14*Power(t2,5)) + 
               2*t1*(42 + 78*t2 - 353*Power(t2,2) + 132*Power(t2,3) + 
                  85*Power(t2,4) + 16*Power(t2,5)) + 
               Power(s2,6)*(-31 - 231*Power(t1,2) + 134*t2 - 
                  58*Power(t2,2) + t1*(64 + 313*t2)) + 
               Power(s2,5)*(124 + 483*Power(t1,3) + 153*t2 + 
                  481*Power(t2,2) - 126*Power(t2,3) - 
                  126*Power(t1,2)*(-3 + 7*t2) + 
                  t1*(-431 - 1205*t2 + 429*Power(t2,2))) + 
               Power(s2,4)*(445 - 595*Power(t1,4) - 161*t2 + 
                  341*Power(t2,2) + 417*Power(t2,3) - 76*Power(t2,4) + 
                  5*Power(t1,3)*(-346 + 279*t2) + 
                  Power(t1,2)*(2690 + 4031*t2 - 1233*Power(t2,2)) + 
                  t1*(-710 - 2018*t2 - 2595*Power(t2,2) + 
                     615*Power(t2,3))) + 
               Power(s2,2)*(-84 - 189*Power(t1,6) + 36*t2 + 
                  1398*Power(t2,2) - 1194*Power(t2,3) + 
                  68*Power(t2,4) - 52*Power(t2,5) + 
                  9*Power(t1,5)*(-292 + 87*t2) + 
                  Power(t1,4)*(6125 + 6116*t2 - 1428*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1573 - 10074*t2 - 5377*Power(t2,2) + 
                     1098*Power(t2,3)) + 
                  Power(t1,2)*
                   (2406 - 510*t2 + 6414*Power(t2,2) + 
                     1192*Power(t2,3) - 216*Power(t2,4)) + 
                  t1*(515 - 4315*t2 + 2871*Power(t2,2) - 
                     1825*Power(t2,3) + 570*Power(t2,4) - 48*Power(t2,5)\
)) + Power(s2,3)*(441*Power(t1,5) - 20*Power(t1,4)*(-149 + 67*t2) + 
                  2*Power(t1,3)*(-2915 - 3377*t2 + 901*Power(t2,2)) + 
                  Power(t1,2)*
                   (1531 + 6884*t2 + 5379*Power(t2,2) - 
                     1176*Power(t2,3)) + 
                  t1*(-1687 + 521*t2 - 2837*Power(t2,2) - 
                     1225*Power(t2,3) + 214*Power(t2,4)) + 
                  4*(-23 + 304*t2 - 212*Power(t2,2) + 135*Power(t2,3) - 
                     51*Power(t2,4) + 5*Power(t2,5))) + 
               s2*(41*Power(t1,7) + Power(t1,6)*(1186 - 258*t2) + 
                  Power(t1,5)*(-3179 - 2873*t2 + 585*Power(t2,2)) + 
                  Power(t1,4)*
                   (773 + 6803*t2 + 2592*Power(t2,2) - 498*Power(t2,3)) \
+ Power(t1,3)*(-1531 + 101*t2 - 5681*Power(t2,2) - 377*Power(t2,3) + 
                     94*Power(t2,4)) - 
                  2*(42 + 54*t2 - 293*Power(t2,2) + 92*Power(t2,3) + 
                     81*Power(t2,4) + 24*Power(t2,5)) + 
                  Power(t1,2)*
                   (-722 + 4922*t2 - 3162*Power(t2,2) + 
                     2042*Power(t2,3) - 548*Power(t2,4) + 36*Power(t2,5)\
) + 2*t1*(130 - 170*t2 - 1273*Power(t2,2) + 1162*Power(t2,3) - 
                     61*Power(t2,4) + 40*Power(t2,5)))) + 
            s1*(Power(s2,8)*(9 + t1 - 7*t2) + 2*Power(t1,8)*(-7 + t2) - 
               8*Power(-1 + t2,2)*Power(t2,2)*
                (-15 - 3*t2 + Power(t2,2)) - 
               6*Power(t1,7)*(-5 - 21*t2 + 2*Power(t2,2)) + 
               Power(t1,6)*(-286 - 48*t2 - 294*Power(t2,2) + 
                  24*Power(t2,3)) + 
               Power(t1,5)*(788 - 20*t2 + 46*Power(t2,2) + 
                  296*Power(t2,3) - 20*Power(t2,4)) + 
               Power(t1,4)*(-7 - 1912*t2 + 1143*Power(t2,2) + 
                  12*Power(t2,3) - 136*Power(t2,4) + 6*Power(t2,5)) + 
               2*Power(t1,3)*(-110 + 190*t2 + 940*Power(t2,2) - 
                  904*Power(t2,3) + 31*Power(t2,4) + 7*Power(t2,5)) - 
               2*t1*(14 + 68*t2 - 128*Power(t2,2) - 91*Power(t2,3) + 
                  96*Power(t2,4) + 41*Power(t2,5)) + 
               Power(t1,2)*(237 - 328*t2 - 176*Power(t2,2) - 
                  732*Power(t2,3) + 1061*Power(t2,4) - 102*Power(t2,5) + 
                  8*Power(t2,6)) + 
               Power(s2,7)*(74 - 7*Power(t1,2) + 3*t2 - 21*Power(t2,2) + 
                  t1*(-68 + 50*t2)) + 
               Power(s2,6)*(129 + 21*Power(t1,3) + 
                  Power(t1,2)*(205 - 151*t2) + 366*t2 - 
                  226*Power(t2,2) - 31*Power(t2,3) + 
                  t1*(-530 + 106*t2 + 117*Power(t2,2))) + 
               Power(s2,5)*(-191 - 35*Power(t1,4) + 148*t2 + 
                  569*Power(t2,2) - 425*Power(t2,3) + 15*Power(t2,4) + 
                  Power(t1,3)*(-306 + 248*t2) + 
                  Power(t1,2)*(1510 - 701*t2 - 258*Power(t2,2)) + 
                  t1*(-499 - 1858*t2 + 1501*Power(t2,2) + 96*Power(t2,3))\
) + Power(s2,4)*(-349 + 35*Power(t1,5) - 510*t2 + 396*Power(t2,2) + 
                  570*Power(t2,3) - 327*Power(t2,4) + 32*Power(t2,5) - 
                  5*Power(t1,4)*(-41 + 47*t2) + 
                  10*Power(t1,3)*(-219 + 181*t2 + 27*Power(t2,2)) + 
                  Power(t1,2)*
                   (420 + 3724*t2 - 4038*Power(t2,2) - 50*Power(t2,3)) - 
                  2*t1*(-788 + 380*t2 + 1152*Power(t2,2) - 
                     1062*Power(t2,3) + 55*Power(t2,4))) + 
               Power(s2,3)*(-77 - 21*Power(t1,6) - 136*t2 - 
                  956*Power(t2,2) + 762*Power(t2,3) + 291*Power(t2,4) - 
                  36*Power(t2,5) + 2*Power(t1,5)*(8 + 61*t2) - 
                  5*Power(t1,4)*(-334 + 491*t2 + 21*Power(t2,2)) + 
                  Power(t1,3)*
                   (730 - 3636*t2 + 5662*Power(t2,2) - 140*Power(t2,3)) \
+ 2*Power(t1,2)*(-2185 + 706*t2 + 1726*Power(t2,2) - 2059*Power(t2,3) + 
                     130*Power(t2,4)) + 
                  t1*(1136 + 3218*t2 - 2211*Power(t2,2) - 
                     1732*Power(t2,3) + 1119*Power(t2,4) - 96*Power(t2,5)\
)) + Power(s2,2)*(225 + 7*Power(t1,7) - 496*t2 + 316*Power(t2,2) - 
                  1090*Power(t2,3) + 1081*Power(t2,4) - 84*Power(t2,5) + 
                  16*Power(t2,6) - Power(t1,6)*(117 + 25*t2) + 
                  Power(t1,5)*(-574 + 1862*t2 - 39*Power(t2,2)) + 
                  Power(t1,4)*
                   (-1655 + 1654*t2 - 4378*Power(t2,2) + 
                     225*Power(t2,3)) - 
                  2*Power(t1,3)*
                   (-2782 + 578*t2 + 1111*Power(t2,2) - 
                     1930*Power(t2,3) + 135*Power(t2,4)) + 
                  t1*(-50 + 572*t2 + 3896*Power(t2,2) - 
                     3384*Power(t2,3) - 512*Power(t2,4) + 90*Power(t2,5)\
) + Power(t1,2)*(-1232 - 6818*t2 + 4377*Power(t2,2) + 1766*Power(t2,3) - 
                     1393*Power(t2,4) + 102*Power(t2,5))) + 
               s2*(-Power(t1,8) + Power(t1,7)*(70 - 4*t2) + 
                  Power(t1,6)*(10 - 751*t2 + 48*Power(t2,2)) + 
                  Power(t1,5)*
                   (1161 - 202*t2 + 1773*Power(t2,2) - 124*Power(t2,3)) + 
                  Power(t1,4)*
                   (-3367 + 376*t2 + 459*Power(t2,2) - 
                     1737*Power(t2,3) + 125*Power(t2,4)) + 
                  Power(t1,2)*
                   (347 - 816*t2 - 4820*Power(t2,2) + 4430*Power(t2,3) + 
                     159*Power(t2,4) - 68*Power(t2,5)) + 
                  Power(t1,3)*
                   (452 + 6022*t2 - 3705*Power(t2,2) - 616*Power(t2,3) + 
                     737*Power(t2,4) - 44*Power(t2,5)) + 
                  2*(14 + 68*t2 - 152*Power(t2,2) - 31*Power(t2,3) + 
                     50*Power(t2,4) + 49*Power(t2,5) + 2*Power(t2,6)) - 
                  2*t1*(231 - 412*t2 + 70*Power(t2,2) - 911*Power(t2,3) + 
                     1071*Power(t2,4) - 93*Power(t2,5) + 12*Power(t2,6))))\
))*R1(1 - s + s2 - t1))/
     ((-1 + s1)*(-1 + s - s2 + t1)*Power(s - s2 + t1,2)*(-s + s1 - t2)*
       Power(s1 - s2 + t1 - t2,3)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       (-1 + t2)*(-1 + s2 - t1 + t2)) - 
    (8*(-2*Power(s1,9)*(s2 - t1)*(-3 + 5*s2 - 5*t1 + 3*t2) - 
         2*Power(s,7)*(t1 - t2)*
          (6 + 2*t1 - 2*Power(t1,2) + Power(t1,3) + 
            Power(s1,2)*(7 - 7*s2 + 6*t1 - 6*t2) + 
            Power(s2,2)*(-1 + t1 - t2) - 2*t2 + 4*t1*t2 - 
            3*Power(t1,2)*t2 - 2*Power(t2,2) + 3*t1*Power(t2,2) - 
            Power(t2,3) - s2*
             (5 - 3*t1 + 2*Power(t1,2) + 3*t2 - 4*t1*t2 + 2*Power(t2,2)) \
+ s1*(-13 + Power(s2,2) - 7*t1 + 4*Power(t1,2) + 7*t2 - 8*t1*t2 + 
               4*Power(t2,2) + s2*(12 - 5*t1 + 5*t2))) - 
         Power(s1,8)*(9*Power(s2,3) + 19*Power(t1,3) + 
            Power(t1,2)*(5 - 69*t2) + Power(s2,2)*(-15 + t1 - 49*t2) + 
            6*Power(-1 + t2,2) + 2*t1*(5 - 21*t2 + 16*Power(t2,2)) + 
            s2*(-10 - 29*Power(t1,2) + 42*t2 - 32*Power(t2,2) + 
               2*t1*(5 + 59*t2))) + 
         Power(s1,7)*(17*Power(s2,4) + 11*Power(t1,4) + 
            32*Power(-1 + t2,2)*t2 + Power(t1,3)*(-6 + 64*t2) + 
            Power(s2,3)*(-57 - 90*t1 + 95*t2) + 
            Power(t1,2)*(-1 + 64*t2 - 179*Power(t2,2)) + 
            2*Power(s2,2)*(46 + 70*Power(t1,2) + t1*(68 - 77*t2) - 
               71*t2 - 33*Power(t2,2)) + 
            4*t1*(9 + 13*t2 - 40*Power(t2,2) + 18*Power(t2,3)) - 
            s2*(78*Power(t1,3) + Power(t1,2)*(73 + 5*t2) + 
               t1*(95 - 86*t2 - 241*Power(t2,2)) + 
               4*(14 + 3*t2 - 35*Power(t2,2) + 18*Power(t2,3)))) - 
         Power(s1,6)*(Power(s2,5) - 9*Power(t1,5) + 
            Power(s2,4)*(-75 - 5*t1 + 62*t2) + 
            Power(t1,4)*(-68 + 75*t2) + 
            Power(t1,3)*(-251 + 181*t2 + 29*Power(t2,2)) + 
            Power(-1 + t2,2)*(-41 - 19*t2 + 72*Power(t2,2)) + 
            Power(s2,3)*(188 + 18*Power(t1,2) + t1*(359 - 383*t2) - 
               446*t2 + 303*Power(t2,2)) + 
            Power(t1,2)*(-189 + 359*t2 + 37*Power(t2,2) - 
               207*Power(t2,3)) + 
            t1*(-50 + 250*t2 + 26*Power(t2,2) - 310*Power(t2,3) + 
               84*Power(t2,4)) - 
            Power(s2,2)*(174 + 34*Power(t1,3) + 
               Power(t1,2)*(561 - 655*t2) - 796*t2 + 670*Power(t2,2) - 
               48*Power(t2,3) + t1*(729 - 1305*t2 + 707*Power(t2,2))) + 
            s2*(111 + 29*Power(t1,4) + Power(t1,3)*(345 - 409*t2) - 
               489*t2 + 269*Power(t2,2) + 193*Power(t2,3) - 
               84*Power(t2,4) + 
               Power(t1,2)*(792 - 1040*t2 + 375*Power(t2,2)) + 
               t1*(389 - 1225*t2 + 695*Power(t2,2) + 141*Power(t2,3)))) - 
         Power(s1,5)*(7*Power(s2,6) - 11*Power(t1,6) + 
            Power(s2,5)*(-3 - 28*t1 + 19*t2) + 
            2*Power(t1,5)*(-7 + 29*t2) + 
            Power(t1,4)*(-321 + 279*t2 - 188*Power(t2,2)) + 
            Power(s2,4)*(-150 + 54*t1 + 31*Power(t1,2) + 195*t2 - 
               66*t1*t2 - 39*Power(t2,2)) - 
            Power(-1 + t2,2)*
             (28 - 245*t2 - 47*Power(t2,2) + 84*Power(t2,3)) + 
            Power(t1,3)*(-865 + 1822*t2 - 757*Power(t2,2) + 
               116*Power(t2,3)) + 
            Power(t1,2)*(-788 + 2449*t2 - 2312*Power(t2,2) + 
               392*Power(t2,3) + 87*Power(t2,4)) + 
            t1*(-233 + 1109*t2 - 1703*Power(t2,2) + 629*Power(t2,3) + 
               248*Power(t2,4) - 50*Power(t2,5)) + 
            Power(s2,3)*(420 + 16*Power(t1,3) + 
               26*Power(t1,2)*(-5 + t2) - 1073*t2 + 1016*Power(t2,2) - 
               407*Power(t2,3) + t1*(699 - 986*t2 + 499*Power(t2,2))) + 
            Power(s2,2)*(-419 - 59*Power(t1,4) + 1794*t2 - 
               2838*Power(t2,2) + 1497*Power(t2,3) - 206*Power(t2,4) + 
               32*Power(t1,3)*(3 + 4*t2) + 
               Power(t1,2)*(-1269 + 1666*t2 - 1069*Power(t2,2)) + 
               t1*(-1745 + 4342*t2 - 3361*Power(t2,2) + 
                  1168*Power(t2,3))) + 
            s2*(157 + 44*Power(t1,5) - 1230*t2 + 2446*Power(t2,2) - 
               1448*Power(t2,3) + 25*Power(t2,4) + 50*Power(t2,5) - 
               3*Power(t1,4)*(1 + 55*t2) + 
               Power(t1,3)*(1041 - 1154*t2 + 797*Power(t2,2)) + 
               Power(t1,2)*(2190 - 5091*t2 + 3102*Power(t2,2) - 
                  877*Power(t2,3)) + 
               t1*(1263 - 4459*t2 + 5446*Power(t2,2) - 
                  2057*Power(t2,3) + 151*Power(t2,4)))) + 
         Power(s1,4)*(10*Power(s2,7) - 2*Power(t1,7) - 
            Power(t1,6)*(55 + 8*t2) + Power(s2,6)*(2 - 78*t1 + 71*t2) + 
            Power(t1,5)*(176 + 113*t2 + 59*Power(t2,2)) + 
            Power(s2,5)*(17 + 242*Power(t1,2) + t1*(43 - 409*t2) - 
               66*t2 + 153*Power(t2,2)) + 
            Power(t1,4)*(888 - 1462*t2 + 265*Power(t2,2) - 
               169*Power(t2,3)) - 
            Power(-1 + t2,2)*
             (-40 + 429*t2 - 800*Power(t2,2) + 9*Power(t2,3) + 
               50*Power(t2,4)) + 
            Power(t1,3)*(1411 - 4732*t2 + 4488*Power(t2,2) - 
               1038*Power(t2,3) + 150*Power(t2,4)) + 
            Power(t1,2)*(1374 - 5711*t2 + 8535*Power(t2,2) - 
               5084*Power(t2,3) + 798*Power(t2,4) - 16*Power(t2,5)) - 
            4*t1*(-173 + 809*t2 - 1540*Power(t2,2) + 1352*Power(t2,3) - 
               453*Power(t2,4) + 2*Power(t2,5) + 3*Power(t2,6)) + 
            Power(s2,4)*(79 - 390*Power(t1,3) - 234*t2 - 
               12*Power(t2,2) + 161*Power(t2,3) + 
               Power(t1,2)*(-247 + 918*t2) + 
               t1*(14 + 569*t2 - 707*Power(t2,2))) + 
            Power(s2,3)*(-490 + 350*Power(t1,4) + 
               Power(t1,3)*(518 - 1002*t2) + 1633*t2 - 
               1717*Power(t2,2) + 787*Power(t2,3) - 192*Power(t2,4) + 
               8*Power(t1,2)*(-40 - 178*t2 + 143*Power(t2,2)) - 
               t1*(803 - 1588*t2 + 137*Power(t2,2) + 208*Power(t2,3))) + 
            Power(s2,2)*(742 - 170*Power(t1,5) - 2969*t2 + 
               5248*Power(t2,2) - 4535*Power(t2,3) + 1605*Power(t2,4) - 
               195*Power(t2,5) + Power(t1,4)*(-532 + 523*t2) + 
               Power(t1,3)*(706 + 1518*t2 - 720*Power(t2,2)) + 
               Power(t1,2)*(2257 - 3936*t2 + 575*Power(t2,2) - 
                  236*Power(t2,3)) + 
               t1*(2163 - 7642*t2 + 8164*Power(t2,2) - 
                  3196*Power(t2,3) + 748*Power(t2,4))) + 
            s2*(-410 + 38*Power(t1,6) + Power(t1,5)*(271 - 93*t2) + 
               2135*t2 - 5226*Power(t2,2) + 6040*Power(t2,3) - 
               2884*Power(t2,4) + 333*Power(t2,5) + 12*Power(t2,6) + 
               Power(t1,4)*(-593 - 710*t2 + 71*Power(t2,2)) + 
               Power(t1,3)*(-2421 + 4044*t2 - 691*Power(t2,2) + 
                  452*Power(t2,3)) + 
               Power(t1,2)*(-3084 + 10741*t2 - 10935*Power(t2,2) + 
                  3447*Power(t2,3) - 706*Power(t2,4)) + 
               t1*(-2102 + 8812*t2 - 14251*Power(t2,2) + 
                  10131*Power(t2,3) - 2621*Power(t2,4) + 239*Power(t2,5))\
)) + Power(s1,3)*(Power(s2,7)*(4 - 12*t2) + 2*Power(t1,7)*(3 + t2) + 
            Power(t1,6)*(-167 + 141*t2 - 7*Power(t2,2)) + 
            Power(t1,5)*(293 + 200*t2 - 415*Power(t2,2) + 
               2*Power(t2,3)) + 
            Power(t1,4)*(1348 - 3069*t2 + 1351*Power(t2,2) + 
               236*Power(t2,3) + 33*Power(t2,4)) + 
            Power(t1,3)*(1707 - 6856*t2 + 9173*Power(t2,2) - 
               4332*Power(t2,3) + 444*Power(t2,4) - 46*Power(t2,5)) + 
            Power(-1 + t2,2)*
             (162 - 531*t2 + 1413*Power(t2,2) - 1371*Power(t2,3) + 
               107*Power(t2,4) + 12*Power(t2,5)) + 
            Power(t1,2)*(1329 - 7382*t2 + 14515*Power(t2,2) - 
               12788*Power(t2,3) + 4804*Power(t2,4) - 538*Power(t2,5) + 
               16*Power(t2,6)) + 
            t1*(662 - 4325*t2 + 10860*Power(t2,2) - 13274*Power(t2,3) + 
               7912*Power(t2,4) - 1945*Power(t2,5) + 110*Power(t2,6)) + 
            Power(s2,6)*(-23 + 41*t2 - 93*Power(t2,2) + 
               6*t1*(-7 + 19*t2)) + 
            Power(s2,5)*(-13 - 346*t2 + 310*Power(t2,2) - 
               211*Power(t2,3) - 8*Power(t1,2)*(-18 + 49*t2) + 
               t1*(314 - 460*t2 + 614*Power(t2,2))) + 
            Power(s2,4)*(-10 + 65*t2 - 738*Power(t2,2) + 
               630*Power(t2,3) - 266*Power(t2,4) + 
               10*Power(t1,3)*(-23 + 67*t2) + 
               Power(t1,2)*(-1193 + 1571*t2 - 1533*Power(t2,2)) + 
               t1*(297 + 1894*t2 - 2083*Power(t2,2) + 1084*Power(t2,3))) \
+ Power(s2,3)*(-124 + 1273*t2 - 1729*Power(t2,2) + 218*Power(t2,3) + 
               242*Power(t2,4) - 50*Power(t2,5) - 
               20*Power(t1,4)*(-9 + 31*t2) + 
               4*Power(t1,3)*(523 - 626*t2 + 463*Power(t2,2)) - 
               2*Power(t1,2)*
                (553 + 1903*t2 - 2402*Power(t2,2) + 994*Power(t2,3)) + 
               t1*(-1106 + 1922*t2 + 2055*Power(t2,2) - 
                  2592*Power(t2,3) + 807*Power(t2,4))) + 
            Power(s2,2)*(470 - 2877*t2 + 5962*Power(t2,2) - 
               5992*Power(t2,3) + 3028*Power(t2,4) - 705*Power(t2,5) + 
               70*Power(t2,6) + Power(t1,5)*(-54 + 302*t2) + 
               Power(t1,4)*(-1913 + 2071*t2 - 1103*Power(t2,2)) + 
               2*Power(t1,3)*
                (833 + 1757*t2 - 2507*Power(t2,2) + 785*Power(t2,3)) + 
               Power(t1,2)*(3590 - 7108*t2 - 545*Power(t2,2) + 
                  3530*Power(t2,3) - 783*Power(t2,4)) + 
               t1*(1657 - 8286*t2 + 11327*Power(t2,2) - 
                  4382*Power(t2,3) + 154*Power(t2,4) - 40*Power(t2,5))) \
- s2*(470 - 2638*t2 + 6637*Power(t2,2) - 9354*Power(t2,3) + 
               7160*Power(t2,4) - 2592*Power(t2,5) + 317*Power(t2,6) + 
               8*Power(t1,6)*(1 + 8*t2) - 
               10*Power(t1,5)*(89 - 86*t2 + 27*Power(t2,2)) + 
               Power(t1,4)*(1137 + 1456*t2 - 2398*Power(t2,2) + 
                  457*Power(t2,3)) + 
               Power(t1,3)*(3822 - 8190*t2 + 2123*Power(t2,2) + 
                  1804*Power(t2,3) - 209*Power(t2,4)) + 
               Power(t1,2)*(3240 - 13869*t2 + 18771*Power(t2,2) - 
                  8496*Power(t2,3) + 840*Power(t2,4) - 136*Power(t2,5)) \
+ t1*(1669 - 9845*t2 + 20201*Power(t2,2) - 19072*Power(t2,3) + 
                  8250*Power(t2,4) - 1389*Power(t2,5) + 98*Power(t2,6)))) \
- (-1 + t2)*(-72 + 270*t2 - 295*Power(t2,2) - 9*Power(t2,3) + 
            249*Power(t2,4) - 255*Power(t2,5) + 156*Power(t2,6) - 
            44*Power(t2,7) - 2*Power(t1,6)*(5 - 8*t2 + 3*Power(t2,2)) + 
            Power(t1,5)*(20 + 33*t2 - 99*Power(t2,2) + 44*Power(t2,3)) + 
            Power(t1,4)*(-6 - 104*t2 + 43*Power(t2,2) + 
               175*Power(t2,3) - 100*Power(t2,4)) + 
            2*Power(s2,6)*(2 - 5*t2 + 7*Power(t2,2) - 5*Power(t2,3) + 
               Power(t2,4)) + 
            Power(t1,3)*(142 - 448*t2 + 780*Power(t2,2) - 
               559*Power(t2,3) - 27*Power(t2,4) + 100*Power(t2,5)) + 
            Power(t1,2)*(208 - 886*t2 + 1552*Power(t2,2) - 
               1664*Power(t2,3) + 1031*Power(t2,4) - 187*Power(t2,5) - 
               46*Power(t2,6)) + 
            t1*(-42 - 153*t2 + 825*Power(t2,2) - 1323*Power(t2,3) + 
               1211*Power(t2,4) - 694*Power(t2,5) + 166*Power(t2,6) + 
               8*Power(t2,7)) + 
            Power(s2,5)*(6 + 2*t2 - 27*Power(t2,2) + 39*Power(t2,3) - 
               22*Power(t2,4) + 4*Power(t2,5) - 
               2*t1*(7 - 14*t2 + 19*Power(t2,2) - 16*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s2,4)*(-10 + 83*t2 - 197*Power(t2,2) + 
               166*Power(t2,3) - 28*Power(t2,4) - 8*Power(t2,5) + 
               2*Power(t2,6) + 
               2*Power(t1,2)*
                (3 + 2*t2 + 3*Power(t2,2) - 14*Power(t2,3) + 
                  6*Power(t2,4)) - 
               t1*(28 - 73*t2 + 55*Power(t2,2) + 28*Power(t2,3) - 
                  40*Power(t2,4) + 12*Power(t2,5))) + 
            Power(s2,3)*(-52 - 27*t2 + 351*Power(t2,2) - 
               603*Power(t2,3) + 464*Power(t2,4) - 133*Power(t2,5) + 
               12*Power(t2,6) - 
               4*Power(t1,3)*Power(-1 + t2,2)*
                (-9 + 6*t2 + 2*Power(t2,2)) + 
               2*Power(t1,2)*
                (14 - 132*t2 + 213*Power(t2,2) - 97*Power(t2,3) + 
                  6*Power(t2,4) + 6*Power(t2,5)) + 
               t1*(28 - 155*t2 + 592*Power(t2,2) - 739*Power(t2,3) + 
                  266*Power(t2,4) - 20*Power(t2,5) - 4*Power(t2,6))) + 
            Power(s2,2)*(-26 - 43*t2 + 88*Power(t2,2) + 
               121*Power(t2,3) - 389*Power(t2,4) + 363*Power(t2,5) - 
               114*Power(t2,6) + 8*Power(t2,7) + 
               2*Power(t1,4)*
                (-32 + 67*t2 - 47*Power(t2,2) + 11*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,3)*(24 + 334*t2 - 678*Power(t2,2) + 
                  360*Power(t2,3) - 56*Power(t2,4) - 4*Power(t2,5)) + 
               t1*(318 - 604*t2 + 260*Power(t2,2) + 627*Power(t2,3) - 
                  995*Power(t2,4) + 396*Power(t2,5) - 38*Power(t2,6)) + 
               Power(t1,2)*(-32 - 43*t2 - 550*Power(t2,2) + 
                  1155*Power(t2,3) - 548*Power(t2,4) + 64*Power(t2,5) + 
                  2*Power(t2,6))) + 
            s2*(150 - 315*t2 - 18*Power(t2,2) + 468*Power(t2,3) - 
               420*Power(t2,4) + 121*Power(t2,5) + 44*Power(t2,6) - 
               28*Power(t2,7) + 
               Power(t1,5)*(42 - 76*t2 + 42*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,4)*(-50 - 178*t2 + 433*Power(t2,2) - 
                  221*Power(t2,3) + 26*Power(t2,4)) + 
               Power(t1,3)*(20 + 219*t2 + 112*Power(t2,2) - 
                  757*Power(t2,3) + 410*Power(t2,4) - 36*Power(t2,5)) + 
               Power(t1,2)*(-408 + 1079*t2 - 1391*Power(t2,2) + 
                  535*Power(t2,3) + 558*Power(t2,4) - 363*Power(t2,5) + 
                  26*Power(t2,6)) - 
               t1*(218 - 1079*t2 + 1848*Power(t2,2) - 1627*Power(t2,3) + 
                  606*Power(t2,4) + 202*Power(t2,5) - 160*Power(t2,6) + 
                  8*Power(t2,7)))) + 
         Power(s1,2)*(-6*Power(t1,7) - 
            2*Power(s2,7)*(5 - 5*t2 + Power(t2,2)) + 
            Power(t1,6)*(-59 + 226*t2 - 124*Power(t2,2) + 
               4*Power(t2,3)) + 
            Power(t1,5)*(21 + 87*t2 - 609*Power(t2,2) + 
               376*Power(t2,3) - 12*Power(t2,4)) + 
            Power(t1,4)*(1016 - 2446*t2 + 1875*Power(t2,2) + 
               128*Power(t2,3) - 388*Power(t2,4) + 12*Power(t2,5)) - 
            Power(-1 + t2,2)*
             (-73 + 480*t2 - 1099*Power(t2,2) + 1763*Power(t2,3) - 
               1153*Power(t2,4) + 94*Power(t2,5)) + 
            Power(t1,3)*(1693 - 6832*t2 + 9887*Power(t2,2) - 
               6225*Power(t2,3) + 1262*Power(t2,4) + 78*Power(t2,5) - 
               4*Power(t2,6)) + 
            Power(t1,2)*(852 - 6220*t2 + 15075*Power(t2,2) - 
               16495*Power(t2,3) + 8487*Power(t2,4) - 
               1757*Power(t2,5) + 98*Power(t2,6)) + 
            t1*(150 - 2327*t2 + 8886*Power(t2,2) - 15288*Power(t2,3) + 
               13258*Power(t2,4) - 5489*Power(t2,5) + 850*Power(t2,6) - 
               40*Power(t2,7)) + 
            Power(s2,6)*(-30 + 10*t2 + 16*Power(t2,2) + 
               15*Power(t2,3) + t1*(54 - 28*t2 - 20*Power(t2,2))) + 
            Power(s2,5)*(8 - 326*t2 + 563*Power(t2,2) - 
               232*Power(t2,3) + 64*Power(t2,4) + 
               2*Power(t1,2)*(-57 - 5*t2 + 65*Power(t2,2)) + 
               t1*(291 - 452*t2 + 254*Power(t2,2) - 195*Power(t2,3))) + 
            Power(s2,4)*(70 + 401*t2 - 1387*Power(t2,2) + 
               1603*Power(t2,3) - 649*Power(t2,4) + 115*Power(t2,5) + 
               Power(t1,3)*(110 + 120*t2 - 280*Power(t2,2)) + 
               Power(t1,2)*(-923 + 1934*t2 - 1300*Power(t2,2) + 
                  634*Power(t2,3)) + 
               t1*(-33 + 1695*t2 - 3489*Power(t2,2) + 
                  1820*Power(t2,3) - 438*Power(t2,4))) + 
            Power(s2,3)*(101 + 256*t2 - 147*Power(t2,2) - 
               1089*Power(t2,3) + 1559*Power(t2,4) - 611*Power(t2,5) + 
               60*Power(t2,6) + 
               10*Power(t1,4)*(-3 - 17*t2 + 29*Power(t2,2)) + 
               Power(t1,3)*(1382 - 3416*t2 + 2340*Power(t2,2) - 
                  886*Power(t2,3)) + 
               Power(t1,2)*(30 - 3216*t2 + 7698*Power(t2,2) - 
                  4444*Power(t2,3) + 942*Power(t2,4)) + 
               t1*(-1326 + 1091*t2 + 3266*Power(t2,2) - 
                  6127*Power(t2,3) + 2867*Power(t2,4) - 427*Power(t2,5))\
) - Power(s2,2)*(187 + 972*t2 - 3790*Power(t2,2) + 4140*Power(t2,3) - 
               1793*Power(t2,4) + 222*Power(t2,5) + 16*Power(t2,6) + 
               6*Power(t2,7) + 
               2*Power(t1,5)*(15 - 50*t2 + 74*Power(t2,2)) + 
               Power(t1,4)*(1068 - 3014*t2 + 2000*Power(t2,2) - 
                  579*Power(t2,3)) + 
               Power(t1,3)*(-28 - 2738*t2 + 7790*Power(t2,2) - 
                  4732*Power(t2,3) + 838*Power(t2,4)) + 
               Power(t1,2)*(-3458 + 5831*t2 + 496*Power(t2,2) - 
                  7573*Power(t2,3) + 4175*Power(t2,4) - 521*Power(t2,5)\
) + t1*(-1469 + 6874*t2 - 8905*Power(t2,2) + 2731*Power(t2,3) + 
                  2442*Power(t2,4) - 1382*Power(t2,5) + 108*Power(t2,6))\
) + s2*(-15 + 1201*t2 - 4796*Power(t2,2) + 8237*Power(t2,3) - 
               7564*Power(t2,4) + 3780*Power(t2,5) - 949*Power(t2,6) + 
               106*Power(t2,7) + 
               Power(t1,6)*(26 - 22*t2 + 30*Power(t2,2)) + 
               Power(t1,5)*(407 - 1316*t2 + 814*Power(t2,2) - 
                  151*Power(t2,3)) + 
               Power(t1,4)*(-54 - 978*t2 + 3627*Power(t2,2) - 
                  2252*Power(t2,3) + 282*Power(t2,4)) - 
               Power(t1,3)*(3218 - 6785*t2 + 3258*Power(t2,2) + 
                  3177*Power(t2,3) - 2345*Power(t2,4) + 221*Power(t2,5)) \
+ Power(t1,2)*(-3263 + 13450*t2 - 18645*Power(t2,2) + 
                  10045*Power(t2,3) - 379*Power(t2,4) - 
                  849*Power(t2,5) + 52*Power(t2,6)) + 
               t1*(-601 + 6740*t2 - 17887*Power(t2,2) + 
                  19847*Power(t2,3) - 10194*Power(t2,4) + 
                  2137*Power(t2,5) - 130*Power(t2,6) + 8*Power(t2,7)))) + 
         s1*(-2*Power(t1,7)*(7 - 11*t2 + 4*Power(t2,2)) + 
            2*Power(s2,7)*(-2 + 5*t2 - 5*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,6)*(30 + 25*t2 - 105*Power(t2,2) + 
               48*Power(t2,3)) + 
            Power(t1,5)*(-77 + 149*t2 - 249*Power(t2,2) + 
               291*Power(t2,3) - 106*Power(t2,4)) + 
            Power(t1,4)*(286 - 727*t2 + 482*Power(t2,2) + 
               233*Power(t2,3) - 398*Power(t2,4) + 112*Power(t2,5)) + 
            Power(t1,3)*(908 - 3787*t2 + 6076*Power(t2,2) - 
               4288*Power(t2,3) + 946*Power(t2,4) + 211*Power(t2,5) - 
               58*Power(t2,6)) + 
            Power(-1 + t2,2)*
             (-114 + 60*t2 + 388*Power(t2,2) - 763*Power(t2,3) + 
               863*Power(t2,4) - 422*Power(t2,5) + 24*Power(t2,6)) + 
            Power(t1,2)*(514 - 3492*t2 + 9103*Power(t2,2) - 
               11846*Power(t2,3) + 7773*Power(t2,4) - 
               2117*Power(t2,5) + 51*Power(t2,6) + 12*Power(t2,7)) - 
            t1*(85 + 534*t2 - 3605*Power(t2,2) + 8129*Power(t2,3) - 
               9492*Power(t2,4) + 5899*Power(t2,5) - 1646*Power(t2,6) + 
               96*Power(t2,7)) + 
            Power(s2,6)*(4 - 22*t2 + 55*Power(t2,2) - 55*Power(t2,3) + 
               16*Power(t2,4) - 
               2*t1*(-9 + 23*t2 - 22*Power(t2,2) + 8*Power(t2,3))) + 
            Power(s2,5)*(36 - 103*t2 + 169*Power(t2,2) - 
               111*Power(t2,3) - 17*Power(t2,4) + 18*Power(t2,5) + 
               2*Power(t1,2)*
                (-8 + 29*t2 - 31*Power(t2,2) + 10*Power(t2,3)) - 
               t1*(10 + 41*t2 + 4*Power(t2,2) - 113*Power(t2,3) + 
                  46*Power(t2,4))) + 
            Power(s2,4)*(57 + 50*Power(t1,3)*(-1 + t2) + 304*t2 - 
               1114*Power(t2,2) + 1305*Power(t2,3) - 719*Power(t2,4) + 
               157*Power(t2,5) - 2*Power(t2,6) + 
               Power(t1,2)*(30 + 409*t2 - 639*Power(t2,2) + 
                  146*Power(t2,3) + 24*Power(t2,4)) - 
               t1*(265 - 771*t2 + 1389*Power(t2,2) - 1259*Power(t2,3) + 
                  314*Power(t2,4) + 22*Power(t2,5))) - 
            Power(s2,3)*(33 + 33*t2 - 886*Power(t2,2) + 
               2163*Power(t2,3) - 2158*Power(t2,4) + 1040*Power(t2,5) - 
               225*Power(t2,6) + 8*Power(t2,7) + 
               10*Power(t1,4)*
                (-14 + 21*t2 - 9*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(100 + 786*t2 - 1496*Power(t2,2) + 
                  614*Power(t2,3) - 44*Power(t2,4)) + 
               2*Power(t1,2)*
                (-328 + 922*t2 - 1701*Power(t2,2) + 1701*Power(t2,3) - 
                  575*Power(t2,4) + 21*Power(t2,5)) + 
               t1*(565 - 37*t2 - 2914*Power(t2,2) + 4702*Power(t2,3) - 
                  3177*Power(t2,4) + 843*Power(t2,5) - 30*Power(t2,6))) \
+ Power(s2,2)*(-251 + 164*t2 + 985*Power(t2,2) - 1259*Power(t2,3) - 
               268*Power(t2,4) + 1064*Power(t2,5) - 515*Power(t2,6) + 
               78*Power(t2,7) + 
               2*Power(t1,5)*
                (-73 + 115*t2 - 50*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,4)*(160 + 644*t2 - 1439*Power(t2,2) + 
                  661*Power(t2,3) - 56*Power(t2,4)) + 
               2*Power(t1,3)*
                (-369 + 968*t2 - 1781*Power(t2,2) + 1881*Power(t2,3) - 
                  698*Power(t2,4) + 39*Power(t2,5)) + 
               Power(t1,2)*(1245 - 1713*t2 - 2004*Power(t2,2) + 
                  5722*Power(t2,3) - 4595*Power(t2,4) + 
                  1327*Power(t2,5) - 54*Power(t2,6)) + 
               t1*(1128 - 4123*t2 + 4478*Power(t2,2) + 
                  406*Power(t2,3) - 3864*Power(t2,4) + 
                  2565*Power(t2,5) - 582*Power(t2,6) + 16*Power(t2,7))) \
- s2*(-301 + 504*t2 + 1057*Power(t2,2) - 3690*Power(t2,3) + 
               4251*Power(t2,4) - 2360*Power(t2,5) + 567*Power(t2,6) - 
               36*Power(t2,7) + 8*Power(t2,8) + 
               2*Power(t1,6)*
                (-36 + 57*t2 - 23*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,5)*(114 + 229*t2 - 636*Power(t2,2) + 
                  299*Power(t2,3) - 18*Power(t2,4)) + 
               Power(t1,4)*(-388 + 909*t2 - 1629*Power(t2,2) + 
                  1799*Power(t2,3) - 683*Power(t2,4) + 32*Power(t2,5)) + 
               Power(t1,3)*(1023 - 2099*t2 + 278*Power(t2,2) + 
                  2558*Power(t2,3) - 2535*Power(t2,4) + 
                  753*Power(t2,5) - 26*Power(t2,6)) + 
               Power(t1,2)*(2003 - 7943*t2 + 11440*Power(t2,2) - 
                  6045*Power(t2,3) - 760*Power(t2,4) + 
                  1736*Power(t2,5) - 415*Power(t2,6) + 8*Power(t2,7)) + 
               t1*(317 - 3454*t2 + 9986*Power(t2,2) - 
                  12587*Power(t2,3) + 7019*Power(t2,4) - 
                  927*Power(t2,5) - 442*Power(t2,6) + 84*Power(t2,7)))) + 
         Power(s,6)*(12 + 48*t1 - 27*Power(t1,2) - 31*Power(t1,3) + 
            14*Power(t1,4) - 4*Power(t1,5) - 48*t2 - 30*t1*t2 + 
            65*Power(t1,2)*t2 - 28*Power(t1,3)*t2 + 10*Power(t1,4)*t2 + 
            57*Power(t2,2) - 37*t1*Power(t2,2) + 3*Power(t2,3) + 
            28*t1*Power(t2,3) - 20*Power(t1,2)*Power(t2,3) - 
            14*Power(t2,4) + 20*t1*Power(t2,4) - 6*Power(t2,5) - 
            6*Power(s1,4)*(-1 + s2 - t1 + t2) + 
            4*Power(s2,3)*(-t1 + Power(t1,2) + t2 - 3*t1*t2 + 
               2*Power(t2,2)) + 
            Power(s1,3)*(-6 + 6*Power(s2,2) + 79*Power(t1,2) + 
               t1*(81 - 154*t2) - 97*s2*(t1 - t2) - 81*t2 + 
               75*Power(t2,2)) + 
            s2*(-30 + 12*Power(t1,4) + 3*t2 + 4*Power(t2,2) - 
               10*Power(t2,3) - 4*Power(t2,4) - 
               32*Power(t1,3)*(1 + t2) - 
               3*t1*(1 + 26*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(74 + 54*t2 + 24*Power(t2,2))) + 
            Power(s2,2)*(18 - 12*Power(t1,3) + 43*t2 + 8*Power(t2,2) + 
               10*Power(t2,3) + Power(t1,2)*(22 + 34*t2) - 
               t1*(43 + 30*t2 + 32*Power(t2,2))) + 
            Power(s1,2)*(6 + 19*Power(t1,3) + 112*t2 + Power(t2,2) - 
               91*Power(t2,3) + Power(s2,2)*(6 - 31*t1 + 31*t2) - 
               Power(t1,2)*(97 + 129*t2) + 
               t1*(-112 + 96*t2 + 201*Power(t2,2)) + 
               s2*(-12 + 12*Power(t1,2) - 155*t2 - 74*Power(t2,2) + 
                  31*t1*(5 + 2*t2))) + 
            s1*(-18 - 4*Power(t1,4) + Power(t1,3)*(46 - 28*t2) + 
               2*Power(s2,3)*(t1 - t2) + 21*t2 - 115*Power(t2,2) + 
               52*Power(t2,3) + 40*Power(t2,4) + 
               Power(t1,2)*(67 - 40*t2 + 108*Power(t2,2)) - 
               t1*(21 - 48*t2 + 58*Power(t2,2) + 116*Power(t2,3)) - 
               2*Power(s2,2)*
                (15 + 4*Power(t1,2) + 40*t2 + 3*Power(t2,2) - 
                  t1*(40 + 7*t2)) + 
               s2*(48 + 10*Power(t1,3) + 61*t2 + 42*Power(t2,2) + 
                  36*Power(t2,3) + 2*Power(t1,2)*(-63 + 8*t2) + 
                  t1*(-61 + 84*t2 - 62*Power(t2,2))))) + 
         Power(s,5)*(-36 + 57*t1 + 246*Power(t1,2) + 11*Power(t1,3) - 
            76*Power(t1,4) + 22*Power(t1,5) - 2*Power(t1,6) + 15*t2 - 
            208*t1*t2 - 196*Power(t1,2)*t2 + 117*Power(t1,3)*t2 - 
            42*Power(t1,4)*t2 - 4*Power(t1,5)*t2 - 38*Power(t2,2) + 
            107*t1*Power(t2,2) + 21*Power(t1,2)*Power(t2,2) + 
            28*Power(t1,3)*Power(t2,2) + 30*Power(t1,4)*Power(t2,2) + 
            78*Power(t2,3) - 89*t1*Power(t2,3) - 
            52*Power(t1,2)*Power(t2,3) - 40*Power(t1,3)*Power(t2,3) + 
            27*Power(t2,4) + 78*t1*Power(t2,4) + 
            10*Power(t1,2)*Power(t2,4) - 34*Power(t2,5) + 
            12*t1*Power(t2,5) - 6*Power(t2,6) + 
            30*Power(s1,5)*(-1 + s2 - t1 + t2) - 
            2*Power(s2,4)*(2 + Power(t1,2) + t1*(3 - 6*t2) - t2 + 
               6*Power(t2,2)) - 
            Power(s1,4)*(-32 + 9*Power(s2,2) + 199*Power(t1,2) + 
               t1*(173 - 415*t2) - 210*t2 + 214*Power(t2,2) + 
               s2*(21 - 236*t1 + 277*t2)) + 
            Power(s2,3)*(-34 + 8*Power(t1,3) - 101*t2 - 4*Power(t2,2) - 
               4*Power(t1,2)*(1 + 8*t2) + 
               2*t1*(47 + 6*t2 + 12*Power(t2,2))) + 
            Power(s2,2)*(31 - 12*Power(t1,4) - 58*t2 - 7*Power(t2,2) - 
               6*Power(t2,3) + 30*Power(t2,4) + 
               24*Power(t1,3)*(2 + t2) + 
               18*Power(t1,2)*(-14 - 4*t2 + Power(t2,2)) + 
               t1*(169 + 259*t2 + 30*Power(t2,2) - 60*Power(t2,3))) + 
            s2*(45 - 60*Power(t1,4) + 8*Power(t1,5) + 143*t2 - 
               126*Power(t2,2) - 9*Power(t2,3) - 34*Power(t2,4) + 
               12*Power(t2,5) + 
               Power(t1,3)*(238 + 100*t2 - 60*Power(t2,2)) + 
               Power(t1,2)*(-146 - 275*t2 - 54*Power(t2,2) + 
                  100*Power(t2,3)) + 
               t1*(-319 + 272*t2 + 46*Power(t2,2) + 48*Power(t2,3) - 
                  60*Power(t2,4))) + 
            Power(s1,3)*(-23 - 19*Power(s2,3) + 52*Power(t1,3) + 
               Power(s2,2)*(-29 + 258*t1 - 208*t2) - 187*t2 - 
               222*Power(t2,2) + 352*Power(t2,3) + 
               Power(t1,2)*(254 + 276*t2) + 
               t1*(139 - 36*t2 - 680*Power(t2,2)) + 
               s2*(63 - 291*Power(t1,2) + 415*t2 + 259*Power(t2,2) + 
                  t1*(-401 + 28*t2))) - 
            Power(s1,2)*(-52 + 2*Power(s2,4) - 35*Power(t1,4) + 
               316*t2 - 685*Power(t2,2) + 107*Power(t2,3) + 
               232*Power(t2,4) + Power(s2,3)*(20 - 35*t1 + 44*t2) + 
               Power(t1,3)*(228 + 53*t2) + 
               Power(t1,2)*(8 - 43*t2 + 231*Power(t2,2)) - 
               t1*(363 - 677*t2 + 292*Power(t2,2) + 481*Power(t2,3)) + 
               Power(s2,2)*(-176 + 29*Power(t1,2) - 295*t2 - 
                  166*Power(t2,2) + t1*(302 + 137*t2)) + 
               s2*(194 + 39*Power(t1,3) - 107*t2 + 470*Power(t2,2) + 
                  66*Power(t2,3) - 2*Power(t1,2)*(275 + 117*t2) + 
                  t1*(162 + 68*t2 + 129*Power(t2,2)))) + 
            s1*(3 + 16*Power(t1,5) + Power(t1,4)*(81 - 100*t2) + 
               2*Power(s2,4)*(2 + 5*t1 - 5*t2) + 256*t2 - 
               237*Power(t2,2) - 269*Power(t2,3) + 147*Power(t2,4) + 
               64*Power(t2,5) + 
               Power(t1,3)*(115 - 108*t2 + 140*Power(t2,2)) + 
               Power(t1,2)*(-353 + 47*t2 + 120*Power(t2,2) + 
                  20*Power(t2,3)) + 
               t1*(-368 + 590*t2 + 107*Power(t2,2) - 240*Power(t2,3) - 
                  140*Power(t2,4)) + 
               Power(s2,3)*(81 - 46*Power(t1,2) + 161*t2 - 
                  50*Power(t2,2) + t1*(-129 + 100*t2)) + 
               Power(s2,2)*(-181 + 78*Power(t1,3) + 
                  Power(t1,2)*(327 - 270*t2) - 29*t2 - 41*Power(t2,2) - 
                  132*Power(t2,3) + t1*(-149 - 298*t2 + 324*Power(t2,2))\
) + s2*(85 - 58*Power(t1,4) - 404*t2 + 311*Power(t2,2) + 
                  159*Power(t2,3) - 28*Power(t2,4) + 
                  Power(t1,3)*(-283 + 280*t2) + 
                  Power(t1,2)*(-47 + 245*t2 - 414*Power(t2,2)) + 
                  t1*(678 - 264*t2 - 121*Power(t2,2) + 220*Power(t2,3))))\
) + Power(s,4)*(-48 - 530*t1 - 498*Power(t1,2) + 322*Power(t1,3) + 
            173*Power(t1,4) - 79*Power(t1,5) + 12*Power(t1,6) + 350*t2 + 
            1285*t1*t2 + 258*Power(t1,2)*t2 - 623*Power(t1,3)*t2 + 
            43*Power(t1,4)*t2 + 2*Power(t1,5)*t2 - 6*Power(t1,6)*t2 - 
            607*Power(t2,2) - 782*t1*Power(t2,2) + 
            421*Power(t1,2)*Power(t2,2) + 160*Power(t1,3)*Power(t2,2) - 
            58*Power(t1,4)*Power(t2,2) + 12*Power(t1,5)*Power(t2,2) + 
            202*Power(t2,3) - 85*t1*Power(t2,3) - 
            88*Power(t1,2)*Power(t2,3) + 92*Power(t1,3)*Power(t2,3) + 
            10*Power(t1,4)*Power(t2,3) + 114*Power(t2,4) - 
            81*t1*Power(t2,4) - 128*Power(t1,2)*Power(t2,4) - 
            40*Power(t1,3)*Power(t2,4) + 45*Power(t2,5) + 
            130*t1*Power(t2,5) + 30*Power(t1,2)*Power(t2,5) - 
            50*Power(t2,6) - 4*t1*Power(t2,6) - 2*Power(t2,7) - 
            60*Power(s1,6)*(-1 + s2 - t1 + t2) + 
            Power(s2,5)*(12 - 4*t1*(-2 + t2) - 8*t2 + 8*Power(t2,2)) + 
            Power(s2,4)*(6 + 10*Power(t1,2)*(-2 + t2) + 103*t2 + 
               28*Power(t2,2) - 20*Power(t2,3) - t1*(83 + 26*t2)) + 
            Power(s1,5)*(-70 - 36*Power(s2,2) + 236*Power(t1,2) + 
               t1*(154 - 592*t2) - 306*t2 + 344*Power(t2,2) + 
               s2*(94 - 232*t1 + 408*t2)) + 
            Power(s1,4)*(14 + 73*Power(s2,3) - 259*Power(t1,3) + 
               205*t2 + 561*Power(t2,2) - 668*Power(t2,3) - 
               Power(t1,2)*(365 + 189*t2) + 
               Power(s2,2)*(38 - 697*t1 + 593*t2) + 
               s2*(-83 + 883*Power(t1,2) + t1*(591 - 556*t2) - 
                  756*t2 - 418*Power(t2,2)) + 
               t1*(-10 - 83*t2 + 1126*Power(t2,2))) + 
            Power(s2,3)*(152 + 111*t2 - 113*Power(t2,2) + 
               72*Power(t2,3) - 40*Power(t2,4) + 
               Power(t1,2)*(256 + 124*t2 - 60*Power(t2,2)) + 
               t1*(-295 - 190*t2 - 176*Power(t2,2) + 100*Power(t2,3))) + 
            Power(s2,2)*(-418 - 20*Power(t1,4)*(-2 + t2) + 255*t2 + 
               143*Power(t2,2) - 122*Power(t2,3) + 48*Power(t2,4) + 
               10*Power(t2,5) + 
               2*Power(t1,3)*(-195 - 68*t2 + 50*Power(t2,2)) + 
               Power(t1,2)*(745 + 114*t2 + 210*Power(t2,2) - 
                  130*Power(t2,3)) + 
               t1*(-110 - 603*t2 + 398*Power(t2,2) - 162*Power(t2,3) + 
                  40*Power(t2,4))) + 
            s2*(290 + 20*Power(t1,5)*(-2 + t2) - 827*t2 + 
               785*Power(t2,2) - 88*Power(t2,3) - 83*Power(t2,4) - 
               38*Power(t2,5) + 20*Power(t2,6) + 
               Power(t1,4)*(284 + 44*t2 - 60*Power(t2,2)) + 
               Power(t1,3)*(-629 - 70*t2 - 4*Power(t2,2) + 
                  40*Power(t2,3)) + 
               Power(t1,2)*(-364 + 1115*t2 - 445*Power(t2,2) - 
                  2*Power(t2,3) + 40*Power(t2,4)) + 
               t1*(1048 - 851*t2 - 398*Power(t2,2) + 314*Power(t2,3) + 
                  40*Power(t2,4) - 60*Power(t2,5))) + 
            Power(s1,3)*(-3 + 11*Power(s2,4) - 64*Power(t1,4) + 
               747*t2 - 1452*Power(t2,2) - 23*Power(t2,3) + 
               579*Power(t2,4) + 9*Power(t1,3)*(27 + 56*t2) + 
               Power(s2,3)*(107 - 189*t1 + 110*t2) + 
               Power(t1,2)*(-569 + 712*t2 - 157*Power(t2,2)) - 
               t1*(883 - 1883*t2 + 952*Power(t2,2) + 862*Power(t2,3)) + 
               Power(s2,2)*(-364 + 281*Power(t1,2) - 217*t2 - 
                  809*Power(t2,2) + 9*t1*(23 + 76*t2)) - 
               s2*(-201 + 39*Power(t1,3) + 762*t2 - 1768*Power(t2,2) + 
                  89*Power(t2,3) + Power(t1,2)*(557 + 1298*t2) + 
                  t1*(-1043 + 1197*t2 - 1406*Power(t2,2)))) + 
            Power(s1,2)*(-169 + 12*Power(s2,5) - 37*Power(t1,5) - 
               138*t2 - 607*Power(t2,2) + 1771*Power(t2,3) - 
               524*Power(t2,4) - 233*Power(t2,5) + 
               Power(s2,4)*(37 - 111*t1 + 114*t2) + 
               Power(t1,4)*(-458 + 317*t2) + 
               Power(t1,3)*(1 + 758*t2 - 736*Power(t2,2)) + 
               Power(s2,3)*(-375 + 298*Power(t1,2) + 
                  t1*(271 - 417*t2) - 401*t2 + 62*Power(t2,2)) + 
               Power(t1,2)*(1115 + 20*t2 - 1216*Power(t2,2) + 
                  436*Power(t2,3)) + 
               t1*(408 - 363*t2 - 1792*Power(t2,2) + 
                  1440*Power(t2,3) + 253*Power(t2,4)) + 
               Power(s2,2)*(304 - 348*Power(t1,3) - 474*t2 + 
                  208*Power(t2,2) + 587*Power(t2,3) + 
                  Power(t1,2)*(-1111 + 809*t2) + 
                  t1*(1365 + 838*t2 - 1048*Power(t2,2))) + 
               s2*(203 + 186*Power(t1,4) + 
                  Power(t1,3)*(1261 - 823*t2) + 893*t2 - 
                  238*Power(t2,2) - 1328*Power(t2,3) + 
                  324*Power(t2,4) + 
                  Power(t1,2)*(-991 - 1195*t2 + 1722*Power(t2,2)) + 
                  t1*(-1891 + 1098*t2 + 1322*Power(t2,2) - 
                     1409*Power(t2,3)))) + 
            s1*(226 + 12*Power(t1,6) - 
               2*Power(s2,5)*(12 + 9*t1 - 11*t2) - 760*t2 + 
               1505*Power(t2,2) - 832*Power(t2,3) - 517*Power(t2,4) + 
               306*Power(t2,5) + 40*Power(t2,6) - 
               4*Power(t1,5)*(-7 + 5*t2) + 
               Power(t1,4)*(53 + 178*t2 - 120*Power(t2,2)) + 
               Power(s2,4)*(-44 + 84*Power(t1,2) + t1*(224 - 162*t2) - 
                  247*t2 + 78*Power(t2,2)) + 
               Power(t1,3)*(-581 + 439*t2 - 578*Power(t2,2) + 
                  320*Power(t2,3)) + 
               Power(t1,2)*(-41 + 65*t2 - 644*Power(t2,2) + 
                  816*Power(t2,3) - 220*Power(t2,4)) + 
               t1*(779 - 1754*t2 + 1348*Power(t2,2) + 669*Power(t2,3) - 
                  750*Power(t2,4) - 12*Power(t2,5)) + 
               Power(s2,3)*(3 - 156*Power(t1,3) + 148*t2 + 
                  9*Power(t2,2) + 72*Power(t2,3) + 
                  Power(t1,2)*(-556 + 374*t2) + 
                  t1*(205 + 683*t2 - 270*Power(t2,2))) + 
               Power(s2,2)*(536 + 144*Power(t1,4) + 
                  Power(t1,3)*(564 - 370*t2) - 21*t2 - 64*Power(t2,2) - 
                  31*Power(t2,3) - 172*Power(t2,4) + 
                  3*Power(t1,2)*(-75 - 149*t2 + 62*Power(t2,2)) + 
                  t1*(-821 - 115*t2 - 146*Power(t2,2) + 212*Power(t2,3))\
) + s2*(-685 - 66*Power(t1,5) + 918*t2 - 1191*Power(t2,2) + 
                  497*Power(t2,3) + 393*Power(t2,4) - 148*Power(t2,5) + 
                  4*Power(t1,4)*(-59 + 39*t2) + 
                  Power(t1,3)*(11 - 167*t2 + 126*Power(t2,2)) + 
                  Power(t1,2)*
                   (1399 - 472*t2 + 715*Power(t2,2) - 604*Power(t2,3)) + 
                  t1*(-493 + 442*t2 - 36*Power(t2,2) - 705*Power(t2,3) + 
                     536*Power(t2,4))))) + 
         Power(s,3)*(312 + 990*t1 + 148*Power(t1,2) - 921*Power(t1,3) - 
            156*Power(t1,4) + 125*Power(t1,5) - 26*Power(t1,6) - 
            1182*t2 - 2392*t1*t2 + 809*Power(t1,2)*t2 + 
            1858*Power(t1,3)*t2 + 50*Power(t1,4)*t2 - 
            69*Power(t1,5)*t2 + 28*Power(t1,6)*t2 + 1884*Power(t2,2) + 
            1731*t1*Power(t2,2) - 2202*Power(t1,2)*Power(t2,2) - 
            1292*Power(t1,3)*Power(t2,2) + 105*Power(t1,4)*Power(t2,2) - 
            78*Power(t1,5)*Power(t2,2) - 6*Power(t1,6)*Power(t2,2) - 
            1379*Power(t2,3) + 374*t1*Power(t2,3) + 
            1776*Power(t1,2)*Power(t2,3) + 208*Power(t1,3)*Power(t2,3) + 
            98*Power(t1,4)*Power(t2,3) + 20*Power(t1,5)*Power(t2,3) + 
            126*Power(t2,4) - 921*t1*Power(t2,4) - 
            314*Power(t1,2)*Power(t2,4) - 72*Power(t1,3)*Power(t2,4) - 
            20*Power(t1,4)*Power(t2,4) + 262*Power(t2,5) + 
            85*t1*Power(t2,5) - 12*Power(t1,2)*Power(t2,5) + 
            11*Power(t2,6) + 70*t1*Power(t2,6) + 
            10*Power(t1,2)*Power(t2,6) - 34*Power(t2,7) - 
            4*t1*Power(t2,7) + 60*Power(s1,7)*(-1 + s2 - t1 + t2) - 
            2*Power(s2,6)*(4 - 2*t2 + Power(t2,2)) + 
            2*Power(s1,6)*(39 + 56*Power(s2,2) - 57*Power(t1,2) + 
               s2*(-81 + 10*t1 - 158*t2) + 129*t2 - 159*Power(t2,2) + 
               t1*(-9 + 230*t2)) + 
            Power(s2,5)*(-46 + 3*t2 - 46*Power(t2,2) + 20*Power(t2,3) + 
               t1*(22 + 32*t2 - 4*Power(t2,2))) - 
            Power(s1,5)*(-70 + 96*Power(s2,3) - 414*Power(t1,3) + 
               277*t2 + 545*Power(t2,2) - 676*Power(t2,3) + 
               4*Power(t1,2)*(-83 + 57*t2) + 
               Power(s2,2)*(3 - 884*t1 + 926*t2) + 
               s2*(59 + 1202*Power(t1,2) + t1*(555 - 1304*t2) - 
                  1042*t2 - 246*Power(t2,2)) + 
               t1*(40 + 127*t2 + 910*Power(t2,2))) + 
            Power(s2,4)*(23 - 430*t2 + 253*Power(t2,2) - 
               38*Power(t2,3) + 10*Power(t2,4) + 
               2*Power(t1,2)*(-17 - 70*t2 + 15*Power(t2,2)) + 
               t1*(329 - 81*t2 + 146*Power(t2,2) - 60*Power(t2,3))) + 
            Power(s2,3)*(-203 + 441*t2 - 436*Power(t2,2) + 
               52*Power(t2,3) + 108*Power(t2,4) - 40*Power(t2,5) + 
               Power(t1,3)*(76 + 160*t2 - 40*Power(t2,2)) + 
               Power(t1,2)*(-836 + 294*t2 - 84*Power(t2,2) + 
                  40*Power(t2,3)) + 
               t1*(301 + 884*t2 - 540*Power(t2,2) - 144*Power(t2,3) + 
                  40*Power(t2,4))) + 
            s2*(-990 + 2284*t2 - 1989*Power(t2,2) + 693*Power(t2,3) + 
               250*Power(t2,4) - 221*Power(t2,5) + 32*Power(t2,6) + 
               8*Power(t2,7) + 
               2*Power(t1,5)*(47 - 32*t2 + 6*Power(t2,2)) + 
               Power(t1,4)*(-566 + 279*t2 + 226*Power(t2,2) - 
                  60*Power(t2,3)) + 
               Power(t1,3)*(659 - 76*t2 - 244*Power(t2,2) - 
                  416*Power(t2,3) + 100*Power(t2,4)) + 
               t1*(-1148 + 706*t2 + 1279*Power(t2,2) - 
                  1628*Power(t2,3) + 688*Power(t2,4) - 220*Power(t2,5)) \
+ Power(t1,2)*(1717 - 3191*t2 + 2020*Power(t2,2) - 596*Power(t2,3) + 
                  442*Power(t2,4) - 60*Power(t2,5))) + 
            Power(s2,2)*(922 - 1065*t2 + 205*Power(t2,2) + 
               116*Power(t2,3) - 212*Power(t2,4) + 162*Power(t2,5) - 
               20*Power(t2,6) + 
               2*Power(t1,4)*(-62 - 10*t2 + 5*Power(t2,2)) + 
               2*Power(t1,3)*
                (497 - 213*t2 - 82*Power(t2,2) + 20*Power(t2,3)) + 
               Power(t1,2)*(-827 - 428*t2 + 426*Power(t2,2) + 
                  500*Power(t2,3) - 130*Power(t2,4)) + 
               t1*(-593 + 892*t2 - 292*Power(t2,2) + 336*Power(t2,3) - 
                  478*Power(t2,4) + 100*Power(t2,5))) + 
            Power(s1,4)*(-145 - 43*Power(s2,4) + 39*Power(t1,4) + 
               Power(t1,3)*(79 - 1087*t2) - 866*t2 + 1744*Power(t2,2) + 
               95*Power(t2,3) - 700*Power(t2,4) + 
               Power(t1,2)*(1272 - 1897*t2 + 1171*Power(t2,2)) + 
               Power(s2,2)*(391 - 724*Power(t1,2) + 
                  t1*(245 - 1249*t2) - 305*t2 + 1665*Power(t2,2)) + 
               t1*(949 - 2602*t2 + 1925*Power(t2,2) + 
                  597*Power(t2,3)) + 
               Power(s2,3)*(446*t1 - 7*(25 + 21*t2)) + 
               s2*(54 + 282*Power(t1,3) + 1557*t2 - 3265*Power(t2,2) + 
                  529*Power(t2,3) + Power(t1,2)*(-149 + 2483*t2) + 
                  t1*(-1875 + 3074*t2 - 3368*Power(t2,2)))) + 
            Power(s1,3)*(325 - 15*Power(s2,5) + 40*Power(t1,5) + 
               Power(t1,4)*(544 - 356*t2) + 
               2*Power(s2,4)*(-81 + 85*t1 - 77*t2) - 405*t2 + 
               2522*Power(t2,2) - 3825*Power(t2,3) + 915*Power(t2,4) + 
               360*Power(t2,5) + 
               Power(t1,3)*(-175 - 1191*t2 + 1492*Power(t2,2)) - 
               Power(t1,2)*(745 + 1813*t2 - 3409*Power(t2,2) + 
                  1596*Power(t2,3)) + 
               t1*(399 - 2099*t2 + 5621*Power(t2,2) - 
                  3717*Power(t2,3) + 60*Power(t2,4)) + 
               Power(s2,3)*(550 - 460*Power(t1,2) + 147*t2 + 
                  190*Power(t2,2) + 4*t1*(79 + 37*t2)) + 
               Power(s2,2)*(-99 + 510*Power(t1,3) + 
                  Power(t1,2)*(398 - 190*t2) + 1041*t2 - 
                  247*Power(t2,2) - 1318*Power(t2,3) + 
                  t1*(-2471 - 25*t2 + 1242*Power(t2,2))) + 
               s2*(-607 - 245*Power(t1,4) - 755*t2 - 1498*Power(t2,2) + 
                  3779*Power(t2,3) - 967*Power(t2,4) + 
                  8*Power(t1,3)*(-137 + 69*t2) + 
                  Power(t1,2)*(2096 + 1069*t2 - 2924*Power(t2,2)) + 
                  4*t1*(384 + 4*t2 - 959*Power(t2,2) + 886*Power(t2,3)))\
) + Power(s1,2)*(-158 - 18*Power(s2,6) - 41*Power(t1,6) + 
               Power(s2,5)*(6 + 175*t1 - 144*t2) + 1028*t2 - 
               2229*Power(t2,2) - 330*Power(t2,3) + 2753*Power(t2,4) - 
               932*Power(t2,5) - 86*Power(t2,6) + 
               Power(t1,5)*(-374 + 151*t2) + 
               2*Power(t1,4)*(252 + 98*t2 + 29*Power(t2,2)) + 
               Power(t1,3)*(1246 - 1871*t2 + 908*Power(t2,2) - 
                  764*Power(t2,3)) + 
               Power(t1,2)*(-1291 + 762*t2 + 2677*Power(t2,2) - 
                  2450*Power(t2,3) + 869*Power(t2,4)) - 
               t1*(1786 - 4080*t2 + 1448*Power(t2,2) + 
                  4063*Power(t2,3) - 2652*Power(t2,4) + 187*Power(t2,5)\
) + Power(s2,4)*(127 - 561*Power(t1,2) + 572*t2 - 186*Power(t2,2) + 
                  t1*(-402 + 799*t2)) + 
               Power(s2,3)*(566 + 854*Power(t1,3) - 342*t2 + 
                  396*Power(t2,2) - 400*Power(t2,3) - 
                  4*Power(t1,2)*(-386 + 421*t2) + 
                  t1*(-1247 - 2228*t2 + 1092*Power(t2,2))) + 
               Power(s2,2)*(-1457 - 676*Power(t1,4) + 1202*t2 - 
                  1442*Power(t2,2) + 918*Power(t2,3) + 
                  366*Power(t2,4) + 6*Power(t1,3)*(-380 + 283*t2) + 
                  Power(t1,2)*(2617 + 2936*t2 - 1568*Power(t2,2)) + 
                  t1*(54 + 545*t2 - 1644*Power(t2,2) + 180*Power(t2,3))\
) + s2*(926 + 267*Power(t1,5) + Power(t1,4)*(1506 - 820*t2) - 2152*t2 + 
                  3750*Power(t2,2) - 800*Power(t2,3) - 
                  1614*Power(t2,4) + 550*Power(t2,5) + 
                  Power(t1,3)*(-2001 - 1476*t2 + 604*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (-933 + 834*t2 + 170*Power(t2,2) + 492*Power(t2,3)) \
+ t1*(3106 - 3912*t2 + 919*Power(t2,2) + 1364*Power(t2,3) - 
                     1585*Power(t2,4)))) + 
            s1*(-402 + 4*Power(s2,6)*(7 + 2*t1 - 4*t2) + 1076*t2 - 
               1960*Power(t2,2) + 2971*Power(t2,3) - 1496*Power(t2,4) - 
               508*Power(t2,5) + 303*Power(t2,6) + 8*Power(t2,7) + 
               Power(t1,6)*(-25 + 28*t2) + 
               Power(t1,5)*(45 + 266*t2 - 108*Power(t2,2)) + 
               Power(t1,4)*(-827 + 300*t2 - 465*Power(t2,2) + 
                  80*Power(t2,3)) + 
               Power(t1,3)*(-65 + 1091*t2 - 188*Power(t2,2) + 
                  116*Power(t2,3) + 120*Power(t2,4)) - 
               Power(t1,2)*(-1066 + 163*t2 + 1517*Power(t2,2) + 
                  302*Power(t2,3) - 527*Power(t2,4) + 180*Power(t2,5)) + 
               t1*(-148 + 940*t2 - 3143*Power(t2,2) + 
                  2749*Power(t2,3) + 653*Power(t2,4) - 
                  722*Power(t2,5) + 52*Power(t2,6)) + 
               Power(s2,5)*(37 - 40*Power(t1,2) + 133*t2 - 
                  24*Power(t2,2) + t1*(-189 + 68*t2)) + 
               Power(s2,4)*(127 + 80*Power(t1,3) + 
                  Power(t1,2)*(451 - 84*t2) + 256*t2 - 
                  393*Power(t2,2) + 92*Power(t2,3) - 
                  t1*(323 + 162*t2 + 88*Power(t2,2))) - 
               Power(s2,3)*(790 + 80*Power(t1,4) + 355*t2 - 
                  1116*Power(t2,2) + 764*Power(t2,3) - 
                  268*Power(t2,4) + 6*Power(t1,3)*(79 + 4*t2) + 
                  Power(t1,2)*(-702 + 578*t2 - 516*Power(t2,2)) + 
                  8*t1*(-56 + 153*t2 - 255*Power(t2,2) + 80*Power(t2,3))\
) + Power(s2,2)*(296 + 40*Power(t1,5) - 383*t2 - 347*Power(t2,2) + 
                  1214*Power(t2,3) - 824*Power(t2,4) + 42*Power(t2,5) + 
                  2*Power(t1,4)*(93 + 68*t2) - 
                  2*Power(t1,3)*(269 - 688*t2 + 390*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (-1052 + 990*t2 - 1683*Power(t2,2) + 
                     542*Power(t2,3)) + 
                  t1*(2245 + 869*t2 - 3092*Power(t2,2) + 
                     2508*Power(t2,3) - 522*Power(t2,4))) + 
               s2*(688 - 8*Power(t1,6) + Power(t1,5)*(23 - 108*t2) - 
                  884*t2 + 956*Power(t2,2) - 2301*Power(t2,3) + 
                  1199*Power(t2,4) + 95*Power(t2,5) - 118*Power(t2,6) + 
                  Power(t1,4)*(77 - 1035*t2 + 484*Power(t2,2)) - 
                  4*Power(t1,3)*
                   (-589 + 328*t2 - 546*Power(t2,2) + 154*Power(t2,3)) + 
                  Power(t1,2)*
                   (-1390 - 1605*t2 + 2164*Power(t2,2) - 
                     1860*Power(t2,3) + 134*Power(t2,4)) + 
                  t1*(-1788 + 1284*t2 + 2370*Power(t2,2) - 
                     2128*Power(t2,3) + 593*Power(t2,4) + 232*Power(t2,5)\
)))) - Power(s,2)*(468 + 828*t1 - 581*Power(t1,2) - 1105*Power(t1,3) - 
            35*Power(t1,4) + 59*Power(t1,5) - 14*Power(t1,6) - 1764*t2 - 
            1752*t1*t2 + 2939*Power(t1,2)*t2 + 2815*Power(t1,3)*t2 + 
            195*Power(t1,4)*t2 - 163*Power(t1,5)*t2 + 
            36*Power(t1,6)*t2 + 2621*Power(t2,2) + 545*t1*Power(t2,2) - 
            5361*Power(t1,2)*Power(t2,2) - 
            3132*Power(t1,3)*Power(t2,2) + 84*Power(t1,4)*Power(t2,2) + 
            5*Power(t1,5)*Power(t2,2) - 20*Power(t1,6)*Power(t2,2) - 
            2019*Power(t2,3) + 1823*t1*Power(t2,3) + 
            5212*Power(t1,2)*Power(t2,3) + 
            1404*Power(t1,3)*Power(t2,3) - 27*Power(t1,4)*Power(t2,3) + 
            74*Power(t1,5)*Power(t2,3) + 2*Power(t1,6)*Power(t2,3) + 
            578*Power(t2,4) - 2783*t1*Power(t2,4) - 
            2461*Power(t1,2)*Power(t2,4) - 259*Power(t1,3)*Power(t2,4) - 
            132*Power(t1,4)*Power(t2,4) - 8*Power(t1,5)*Power(t2,4) + 
            449*Power(t2,5) + 1525*t1*Power(t2,5) + 
            433*Power(t1,2)*Power(t2,5) + 144*Power(t1,3)*Power(t2,5) + 
            12*Power(t1,4)*Power(t2,5) - 375*Power(t2,6) - 
            222*t1*Power(t2,6) - 80*Power(t1,2)*Power(t2,6) - 
            8*Power(t1,3)*Power(t2,6) + 34*Power(t2,7) + 
            6*t1*Power(t2,7) + 2*Power(t1,2)*Power(t2,7) + 
            8*Power(t2,8) + 30*Power(s1,8)*(-1 + s2 - t1 + t2) + 
            Power(s2,6)*(-20 + 30*t2 - 20*Power(t2,2) + 6*Power(t2,3)) + 
            Power(s1,7)*(44 + 120*Power(s2,2) + 17*Power(t1,2) + 
               117*t2 - 157*Power(t2,2) - s2*(132 + 133*t1 + 109*t2) + 
               t1*(61 + 172*t2)) - 
            Power(s2,5)*(60 + 4*t2 - 31*Power(t2,2) - 42*Power(t2,3) + 
               12*Power(t2,4) + 2*t1*(-41 + 27*t2 + 6*Power(t2,3))) + 
            Power(s2,4)*(56 - 642*t2 + 862*Power(t2,2) - 
               351*Power(t2,3) + 142*Power(t2,4) - 30*Power(t2,5) - 
               2*Power(t1,2)*
                (71 + 24*t2 - 90*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(403 - 335*t2 + 145*Power(t2,2) - 294*Power(t2,3) + 
                  60*Power(t2,4))) + 
            Power(s2,3)*(-34 + 804*t2 - 1815*Power(t2,2) + 
               1478*Power(t2,3) - 384*Power(t2,4) + 48*Power(t2,5) + 
               4*Power(t1,3)*
                (37 + 33*t2 - 80*Power(t2,2) + 10*Power(t2,3)) - 
               2*Power(t1,2)*
                (454 - 596*t2 + 313*Power(t2,2) - 278*Power(t2,3) + 
                  50*Power(t2,4)) + 
               t1*(53 + 1401*t2 - 2418*Power(t2,2) + 1020*Power(t2,3) - 
                  324*Power(t2,4) + 60*Power(t2,5))) + 
            Power(s2,2)*(830 - 1393*t2 + 962*Power(t2,2) - 
               769*Power(t2,3) + 516*Power(t2,4) + 29*Power(t2,5) - 
               96*Power(t2,6) + 12*Power(t2,7) - 
               2*Power(t1,4)*
                (56 + 9*t2 - 90*Power(t2,2) + 15*Power(t2,3)) + 
               2*Power(t1,3)*
                (453 - 763*t2 + 349*Power(t2,2) - 162*Power(t2,3) + 
                  30*Power(t2,4)) - 
               3*Power(t1,2)*
                (103 + 227*t2 - 778*Power(t2,2) + 338*Power(t2,3) - 
                  30*Power(t2,4) + 6*Power(t2,5)) + 
               t1*(-1361 + 1813*t2 + 294*Power(t2,2) - 
                  1512*Power(t2,3) + 305*Power(t2,4) + 
                  150*Power(t2,5) - 24*Power(t2,6))) + 
            s2*(-1230 + 3079*t2 - 2532*Power(t2,2) + 744*Power(t2,3) + 
               223*Power(t2,4) - 419*Power(t2,5) + 221*Power(t2,6) - 
               56*Power(t2,7) + 
               Power(t1,5)*(58 - 78*t2 + 4*Power(t2,3)) - 
               Power(t1,4)*(400 - 836*t2 + 253*Power(t2,2) + 
                  54*Power(t2,3)) + 
               Power(t1,3)*(235 - 273*t2 - 862*Power(t2,2) + 
                  372*Power(t2,3) + 224*Power(t2,4) - 24*Power(t2,5)) + 
               Power(t1,2)*(2500 - 5432*t2 + 4653*Power(t2,2) - 
                  1370*Power(t2,3) + 338*Power(t2,4) - 
                  342*Power(t2,5) + 32*Power(t2,6)) - 
               t1*(165 + 1636*t2 - 4027*Power(t2,2) + 
                  3793*Power(t2,3) - 1757*Power(t2,4) + 
                  600*Power(t2,5) - 228*Power(t2,6) + 12*Power(t2,7))) - 
            Power(s1,6)*(-126 + 40*Power(s2,3) - 325*Power(t1,3) + 
               295*t2 + 200*Power(t2,2) - 349*Power(t2,3) + 
               3*Power(t1,2)*(-63 + 163*t2) + 
               Power(s2,2)*(-7 - 543*t1 + 787*t2) + 
               t1*(-52 + 377*t2 + 269*Power(t2,2)) + 
               s2*(185 + 828*Power(t1,2) - 887*t2 + 68*Power(t2,2) - 
                  20*t1*(-15 + 68*t2))) - 
            Power(s1,5)*(139 + 80*Power(s2,4) + 17*Power(t1,4) + 
               726*t2 - 1429*Power(t2,2) + 125*Power(t2,3) + 
               399*Power(t2,4) + Power(s2,3)*(50 - 571*t1 + 253*t2) + 
               Power(t1,3)*(-229 + 1028*t2) + 
               Power(t1,2)*(-1111 + 1796*t2 - 1588*Power(t2,2)) + 
               t1*(-530 + 2083*t2 - 2100*Power(t2,2) + 
                  72*Power(t2,3)) + 
               Power(s2,2)*(-178 + 919*Power(t1,2) + 365*t2 - 
                  1585*Power(t2,2) + 13*t1*(-13 + 56*t2)) - 
               s2*(163 + 445*Power(t1,3) + 1633*t2 - 3110*Power(t2,2) + 
                  759*Power(t2,3) + Power(t1,2)*(-348 + 2009*t2) + 
                  t1*(-1439 + 2679*t2 - 3501*Power(t2,2)))) + 
            Power(s1,4)*(132 + 13*Power(s2,5) + 4*Power(t1,5) + 
               Power(t1,4)*(30 - 28*t2) - 432*t2 + 2942*Power(t2,2) - 
               3867*Power(t2,3) + 950*Power(t2,4) + 235*Power(t2,5) + 
               Power(s2,4)*(-279 + 18*t1 + 92*t2) + 
               7*Power(t1,3)*(-55 - 50*t2 + 189*Power(t2,2)) + 
               Power(s2,3)*(422 - 136*Power(t1,2) + 
                  t1*(1303 - 1118*t2) - 677*t2 + 783*Power(t2,2)) + 
               Power(t1,2)*(291 - 2759*t2 + 3516*Power(t2,2) - 
                  2079*Power(t2,3)) + 
               t1*(936 - 3408*t2 + 6619*Power(t2,2) - 
                  4354*Power(t2,3) + 525*Power(t2,4)) + 
               Power(s2,2)*(143 + 170*Power(t1,3) + 1158*t2 - 
                  545*Power(t2,2) - 1283*Power(t2,3) + 
                  Power(t1,2)*(-1739 + 1932*t2) + 
                  t1*(-2341 + 2548*t2 - 429*Power(t2,2))) - 
               s2*(429 + 69*Power(t1,4) + 375*t2 + 2805*Power(t2,2) - 
                  4583*Power(t2,3) + 1212*Power(t2,4) + 
                  Power(t1,3)*(-685 + 878*t2) + 
                  3*Power(t1,2)*(-768 + 507*t2 + 559*Power(t2,2)) - 
                  t1*(68 + 1125*t2 - 3507*Power(t2,2) + 
                     3832*Power(t2,3)))) - 
            Power(s1,3)*(-241 + 35*Power(s2,6) + 65*Power(t1,6) - 
               155*t2 + 493*Power(t2,2) + 3058*Power(t2,3) - 
               4443*Power(t2,4) + 1204*Power(t2,5) + 64*Power(t2,6) - 
               8*Power(t1,5)*(-73 + 37*t2) + 
               Power(s2,5)*(43 - 298*t1 + 267*t2) + 
               Power(t1,4)*(-205 - 1366*t2 + 393*Power(t2,2)) + 
               Power(s2,4)*(-12 + 907*Power(t1,2) + 
                  t1*(200 - 1386*t2) - 656*t2 + 433*Power(t2,2)) + 
               Power(t1,3)*(-504 + 30*t2 + 1133*Power(t2,2) + 
                  524*Power(t2,3)) + 
               Power(t1,2)*(1731 - 1039*t2 - 2420*Power(t2,2) + 
                  2151*Power(t2,3) - 1204*Power(t2,4)) + 
               t1*(1250 - 2232*t2 - 1895*Power(t2,2) + 
                  6900*Power(t2,3) - 3746*Power(t2,4) + 454*Power(t2,5)\
) + Power(s2,3)*(-1002 - 1348*Power(t1,3) + 1136*t2 - 
                  1643*Power(t2,2) + 1144*Power(t2,3) + 
                  2*Power(t1,2)*(-721 + 1426*t2) + 
                  t1*(695 + 3820*t2 - 2730*Power(t2,2))) + 
               Power(s2,2)*(1426 + 1057*Power(t1,4) + 
                  Power(t1,3)*(2696 - 2910*t2) - 2569*t2 + 
                  4415*Power(t2,2) - 2605*Power(t2,3) - 
                  169*Power(t2,4) + 
                  Power(t1,2)*(-1559 - 7038*t2 + 4554*Power(t2,2)) + 
                  t1*(1956 - 4922*t2 + 7095*Power(t2,2) - 
                     2326*Power(t2,3))) + 
               s2*(-241 - 418*Power(t1,5) + 1919*t2 - 
                  4537*Power(t2,2) + 418*Power(t2,3) + 
                  2558*Power(t2,4) - 808*Power(t2,5) + 
                  Power(t1,4)*(-2081 + 1473*t2) + 
                  Power(t1,3)*(1081 + 5240*t2 - 2650*Power(t2,2)) + 
                  Power(t1,2)*
                   (-450 + 3756*t2 - 6585*Power(t2,2) + 
                     658*Power(t2,3)) + 
                  t1*(-3785 + 6140*t2 - 4469*Power(t2,2) + 
                     672*Power(t2,3) + 1705*Power(t2,4)))) + 
            Power(s1,2)*(-658 - 8*Power(s2,7) + 2*Power(t1,7) + 
               Power(s2,6)*(27 + 82*t1 - 51*t2) + 1996*t2 - 
               4352*Power(t2,2) + 5311*Power(t2,3) - 1064*Power(t2,4) - 
               1841*Power(t2,5) + 598*Power(t2,6) + 6*Power(t2,7) + 
               Power(t1,6)*(59 + 54*t2) + 
               Power(t1,5)*(-751 + 740*t2 - 299*Power(t2,2)) + 
               Power(t1,4)*(-1145 + 2602*t2 - 2243*Power(t2,2) + 
                  463*Power(t2,3)) + 
               Power(t1,3)*(241 + 1467*t2 - 3179*Power(t2,2) + 
                  2372*Power(t2,3) - 99*Power(t2,4)) - 
               Power(t1,2)*(434 - 3387*t2 + 4233*Power(t2,2) - 
                  1389*Power(t2,3) + 258*Power(t2,4) + 271*Power(t2,5)) \
+ t1*(-1598 + 6046*t2 - 9519*Power(t2,2) + 4775*Power(t2,3) + 
                  1780*Power(t2,4) - 1268*Power(t2,5) + 144*Power(t2,6)\
) + Power(s2,5)*(45 - 292*Power(t1,2) + 242*t2 + 38*Power(t2,2) + 
                  t1*(-244 + 229*t2)) + 
               Power(s2,4)*(475 + 510*Power(t1,3) + 
                  Power(t1,2)*(765 - 352*t2) + 713*t2 - 
                  569*Power(t2,2) + 227*Power(t2,3) - 
                  t1*(1007 + 416*t2 + 503*Power(t2,2))) - 
               Power(s2,3)*(640 + 480*Power(t1,4) + 979*t2 - 
                  2750*Power(t2,2) + 1754*Power(t2,3) - 
                  712*Power(t2,4) - 2*Power(t1,3)*(-580 + 69*t2) - 
                  2*Power(t1,2)*(1751 - 472*t2 + 790*Power(t2,2)) + 
                  t1*(840 + 3883*t2 - 4142*Power(t2,2) + 
                     1788*Power(t2,3))) + 
               Power(s2,2)*(-789 + 238*Power(t1,5) + 1621*t2 - 
                  3431*Power(t2,2) + 5530*Power(t2,3) - 
                  2801*Power(t2,4) + 286*Power(t2,5) + 
                  Power(t1,4)*(925 + 173*t2) + 
                  Power(t1,3)*(-4914 + 2908*t2 - 2102*Power(t2,2)) + 
                  Power(t1,2)*
                   (-890 + 8229*t2 - 8820*Power(t2,2) + 
                     3358*Power(t2,3)) + 
                  t1*(2559 + 2485*t2 - 10419*Power(t2,2) + 
                     7768*Power(t2,3) - 1953*Power(t2,4))) + 
               s2*(1548 - 52*Power(t1,6) - 3285*t2 + 5536*Power(t2,2) - 
                  7507*Power(t2,3) + 3567*Power(t2,4) + 
                  21*Power(t2,5) - 232*Power(t2,6) - 
                  Power(t1,5)*(372 + 191*t2) + 
                  Power(t1,4)*(3125 - 2530*t2 + 1286*Power(t2,2)) + 
                  Power(t1,3)*
                   (2400 - 7661*t2 + 7490*Power(t2,2) - 
                     2260*Power(t2,3)) + 
                  Power(t1,2)*
                   (-2160 - 2973*t2 + 10848*Power(t2,2) - 
                     8386*Power(t2,3) + 1340*Power(t2,4)) + 
                  t1*(815 - 5044*t2 + 10116*Power(t2,2) - 
                     9653*Power(t2,3) + 3657*Power(t2,4) + 
                     109*Power(t2,5)))) + 
            s1*(-90 + 2*Power(t1,7) - 4*Power(s2,7)*(-2 + t2) + 557*t2 - 
               1346*Power(t2,2) + 3271*Power(t2,3) - 4498*Power(t2,4) + 
               2167*Power(t2,5) + 53*Power(t2,6) - 114*Power(t2,7) - 
               4*Power(t1,6)*(17 - 6*t2 + 5*Power(t2,2)) + 
               Power(s2,6)*(48 - 38*t1 - 27*t2 + 16*Power(t2,2)) + 
               Power(t1,5)*(330 + 199*t2 - 348*Power(t2,2) + 
                  92*Power(t2,3)) + 
               Power(t1,4)*(-1448 + 1275*t2 - 1118*Power(t2,2) + 
                  938*Power(t2,3) - 140*Power(t2,4)) + 
               Power(t1,3)*(-2129 + 6348*t2 - 5107*Power(t2,2) + 
                  2212*Power(t2,3) - 1056*Power(t2,4) + 68*Power(t2,5)) \
+ Power(t1,2)*(518 + 3041*t2 - 9504*Power(t2,2) + 7451*Power(t2,3) - 
                  2019*Power(t2,4) + 444*Power(t2,5) + 16*Power(t2,6)) + 
               t1*(705 - 600*t2 - 4237*Power(t2,2) + 9412*Power(t2,3) - 
                  6116*Power(t2,4) + 741*Power(t2,5) + 
                  110*Power(t2,6) - 16*Power(t2,7)) + 
               Power(s2,5)*(-53 + 357*t2 - 435*Power(t2,2) + 
                  96*Power(t2,3) + Power(t1,2)*(68 + 60*t2) + 
                  t1*(-378 + 373*t2 - 144*Power(t2,2))) + 
               Power(s2,4)*(102 - 807*t2 + 480*Power(t2,2) - 
                  378*Power(t2,3) + 92*Power(t2,4) - 
                  10*Power(t1,3)*(5 + 16*t2) + 
                  2*Power(t1,2)*(482 - 599*t2 + 198*Power(t2,2)) + 
                  t1*(422 - 1393*t2 + 1760*Power(t2,2) - 
                     328*Power(t2,3))) + 
               Power(s2,3)*(-1224 + 1139*t2 + 180*Power(t1,4)*t2 + 
                  109*Power(t2,2) - 942*Power(t2,3) + 376*Power(t2,4) - 
                  122*Power(t2,5) - 
                  2*Power(t1,3)*(518 - 801*t2 + 232*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (-639 + 919*t2 - 1161*Power(t2,2) + 158*Power(t2,3)) \
+ 2*t1*(739 + 390*t2 - 167*Power(t2,2) + 84*Power(t2,3) + 
                     25*Power(t2,4))) + 
               Power(s2,2)*(1593 + Power(t1,5)*(22 - 96*t2) - 2045*t2 - 
                  188*Power(t2,2) + 1829*Power(t2,3) - 
                  2314*Power(t2,4) + 1019*Power(t2,5) - 
                  114*Power(t2,6) + 
                  Power(t1,4)*(384 - 943*t2 + 216*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (916 - 363*t2 + 378*Power(t2,2) + 30*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (2355 - 1068*t2 + 945*Power(t2,2) - 
                     868*Power(t2,3) + 258*Power(t2,4)) + 
                  t1*(721 + 2552*t2 - 4293*Power(t2,2) + 
                     4760*Power(t2,3) - 2470*Power(t2,4) + 
                     450*Power(t2,5))) + 
               s2*(-400 + 507*t2 + 590*Power(t2,2) - 3215*Power(t2,3) + 
                  4208*Power(t2,4) - 2054*Power(t2,5) + 
                  341*Power(t2,6) + 24*Power(t2,7) + 
                  4*Power(t1,6)*(-3 + 5*t2) + 
                  Power(t1,5)*(86 + 169*t2) - 
                  Power(t1,4)*
                   (1253 + 275*t2 - 589*Power(t2,2) + 236*Power(t2,3)) + 
                  Power(t1,3)*
                   (4578 - 3384*t2 + 2862*Power(t2,2) - 
                     2464*Power(t2,3) + 514*Power(t2,4)) + 
                  Power(t1,2)*
                   (2632 - 10039*t2 + 9291*Power(t2,2) - 
                     6030*Power(t2,3) + 3150*Power(t2,4) - 
                     396*Power(t2,5)) + 
                  t1*(-2507 + 684*t2 + 7826*Power(t2,2) - 
                     9442*Power(t2,3) + 5411*Power(t2,4) - 
                     1773*Power(t2,5) + 74*Power(t2,6))))) + 
         s*(300 + 317*t1 - 654*Power(t1,2) - 632*Power(t1,3) + 
            18*Power(t1,4) - 25*Power(t1,5) + 12*Power(t1,6) - 1253*t2 - 
            304*t1*t2 + 3109*Power(t1,2)*t2 + 2073*Power(t1,3)*t2 + 
            234*Power(t1,4)*t2 - 103*Power(t1,5)*t2 - 12*Power(t1,6)*t2 + 
            1894*Power(t2,2) - 1464*t1*Power(t2,2) - 
            6019*Power(t1,2)*Power(t2,2) - 3261*Power(t1,3)*Power(t2,2) - 
            137*Power(t1,4)*Power(t2,2) + 205*Power(t1,5)*Power(t2,2) - 
            4*Power(t1,6)*Power(t2,2) - 1205*Power(t2,3) + 
            3699*t1*Power(t2,3) + 6751*Power(t1,2)*Power(t2,3) + 
            2448*Power(t1,3)*Power(t2,3) - 296*Power(t1,4)*Power(t2,3) - 
            59*Power(t1,5)*Power(t2,3) + 4*Power(t1,6)*Power(t2,3) + 
            49*Power(t2,4) - 4229*t1*Power(t2,4) - 
            4466*Power(t1,2)*Power(t2,4) - 539*Power(t1,3)*Power(t2,4) + 
            141*Power(t1,4)*Power(t2,4) - 16*Power(t1,5)*Power(t2,4) + 
            602*Power(t2,5) + 2847*t1*Power(t2,5) + 
            1356*Power(t1,2)*Power(t2,5) - 37*Power(t1,3)*Power(t2,5) + 
            32*Power(t1,4)*Power(t2,5) - 601*Power(t2,6) - 
            948*t1*Power(t2,6) - 113*Power(t1,2)*Power(t2,6) - 
            40*Power(t1,3)*Power(t2,6) + 234*Power(t2,7) + 
            92*t1*Power(t2,7) + 28*Power(t1,2)*Power(t2,7) - 
            20*Power(t2,8) - 8*t1*Power(t2,8) + 
            6*Power(s1,9)*(-1 + s2 - t1 + t2) - 
            2*Power(s2,6)*(8 - 20*t2 + 23*Power(t2,2) - 14*Power(t2,3) + 
               3*Power(t2,4)) + 
            Power(s1,8)*(10 + 57*Power(s2,2) + 37*Power(t1,2) + 22*t2 - 
               32*Power(t2,2) - s2*(49 + 94*t1 + t2) + t1*(37 + 13*t2)) + 
            Power(s1,7)*(65 + 11*Power(s2,3) + 126*Power(t1,3) + 
               4*Power(s2,2)*(-5 + 33*t1 - 82*t2) - 134*t2 - 
               3*Power(t2,2) + 72*Power(t2,3) - 
               8*Power(t1,2)*(-7 + 39*t2) + 
               t1*(59 - 241*t2 + 50*Power(t2,2)) - 
               s2*(104 + 269*Power(t1,2) + t1*(56 - 660*t2) - 359*t2 + 
                  123*Power(t2,2))) + 
            Power(s2,5)*(-32 + 3*t2 + 92*Power(t2,2) - 83*Power(t2,3) + 
               22*Power(t2,4) - 4*Power(t2,5) + 
               2*t1*(33 - 62*t2 + 51*Power(t2,2) - 32*Power(t2,3) + 
                  10*Power(t2,4))) + 
            Power(s2,4)*(41 - 408*t2 + 872*Power(t2,2) - 
               680*Power(t2,3) + 225*Power(t2,4) - 68*Power(t2,5) + 
               10*Power(t2,6) - 
               4*Power(t1,2)*
                (23 - 21*t2 - 12*Power(t2,2) + 5*Power(t2,3) + 
                  5*Power(t2,4)) + 
               t1*(191 - 331*t2 + 153*Power(t2,2) - 79*Power(t2,3) + 
                  76*Power(t2,4))) + 
            Power(s2,3)*(103 + 324*t2 - 1643*Power(t2,2) + 
               2393*Power(t2,3) - 1574*Power(t2,4) + 465*Power(t2,5) - 
               88*Power(t2,6) + 8*Power(t2,7) + 
               4*Power(t1,3)*Power(-1 + t2,2)*(7 + 40*t2) + 
               2*Power(t1,2)*
                (-178 + 539*t2 - 608*Power(t2,2) + 397*Power(t2,3) - 
                  172*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(-71 + 882*t2 - 2473*Power(t2,2) + 2484*Power(t2,3) - 
                  1050*Power(t2,4) + 292*Power(t2,5) - 32*Power(t2,6))) + 
            Power(s2,2)*(303 - 549*t2 + 492*Power(t2,2) - 
               877*Power(t2,3) + 1350*Power(t2,4) - 950*Power(t2,5) + 
               233*Power(t2,6) - 10*Power(t2,7) + 
               2*Power(t1,4)*
                (24 - 88*t2 + 129*Power(t2,2) - 70*Power(t2,3) + 
                  5*Power(t2,4)) - 
               2*Power(t1,3)*
                (-121 + 639*t2 - 905*Power(t2,2) + 539*Power(t2,3) - 
                  178*Power(t2,4) + 16*Power(t2,5)) + 
               Power(t1,2)*(37 - 306*t2 + 2193*Power(t2,2) - 
                  3224*Power(t2,3) + 1566*Power(t2,4) - 
                  348*Power(t2,5) + 34*Power(t2,6)) - 
               t1*(1104 - 2213*t2 + 689*Power(t2,2) + 2198*Power(t2,3) - 
                  2663*Power(t2,4) + 979*Power(t2,5) - 142*Power(t2,6) + 
                  12*Power(t2,7))) - 
            s2*(695 - 1951*t2 + 1497*Power(t2,2) + 288*Power(t2,3) - 
               902*Power(t2,4) + 473*Power(t2,5) - 114*Power(t2,6) + 
               32*Power(t2,7) - 16*Power(t2,8) + 
               Power(t1,5)*(46 - 84*t2 + 66*Power(t2,2) - 
                  32*Power(t2,3) + 4*Power(t2,4)) + 
               Power(t1,4)*(20 - 631*t2 + 1044*Power(t2,2) - 
                  505*Power(t2,3) + 94*Power(t2,4) - 12*Power(t2,5)) + 
               Power(t1,3)*(25 + 402*t2 + 455*Power(t2,2) - 
                  1716*Power(t2,3) + 882*Power(t2,4) - 92*Power(t2,5) + 
                  12*Power(t2,6)) + 
               Power(t1,2)*(-1633 + 4610*t2 - 5593*Power(t2,2) + 
                  2643*Power(t2,3) + 550*Power(t2,4) - 
                  551*Power(t2,5) + 14*Power(t2,6) - 4*Power(t2,7)) + 
               t1*(-465 + 2994*t2 - 6011*Power(t2,2) + 
                  5864*Power(t2,3) - 2826*Power(t2,4) + 
                  320*Power(t2,5) + 76*Power(t2,6) + 32*Power(t2,7))) + 
            Power(s1,6)*(-25 - 63*Power(s2,4) - 32*Power(t1,4) + 
               Power(t1,3)*(91 - 436*t2) + 
               Power(s2,3)*(95 + 367*t1 - 265*t2) - 356*t2 + 
               635*Power(t2,2) - 170*Power(t2,3) - 84*Power(t2,4) + 
               6*Power(t1,2)*(59 - 111*t2 + 148*Power(t2,2)) + 
               t1*(98 - 795*t2 + 1041*Power(t2,2) - 252*Power(t2,3)) + 
               Power(s2,2)*(-87 - 577*Power(t1,2) + 86*t2 + 
                  637*Power(t2,2) + t1*(-199 + 110*t2)) + 
               s2*(110 + 305*Power(t1,3) + 692*t2 - 1319*Power(t2,2) + 
                  425*Power(t2,3) + Power(t1,2)*(13 + 591*t2) + 
                  t1*(-299 + 688*t2 - 1601*Power(t2,2)))) + 
            Power(s1,5)*(17*Power(s2,5) - 20*Power(t1,5) + 
               Power(t1,4)*(-219 + 176*t2) + 
               Power(s2,4)*(-235 - 56*t1 + 206*t2) + 
               2*Power(s2,3)*
                (177 + 43*Power(t1,2) + t1*(607 - 663*t2) - 522*t2 + 
                  442*Power(t2,2)) + 
               Power(t1,3)*(-550 + 408*t2 + 454*Power(t2,2)) + 
               Power(t1,2)*(112 - 701*t2 + 1212*Power(t2,2) - 
                  1120*Power(t2,3)) + 
               t1*(419 - 1308*t2 + 2766*Power(t2,2) - 
                  2157*Power(t2,3) + 412*Power(t2,4)) + 
               2*(-32 - 52*t2 + 687*Power(t2,2) - 879*Power(t2,3) + 
                  251*Power(t2,4) + 25*Power(t2,5)) - 
               Power(s2,2)*(55 + 92*Power(t1,3) + 
                  Power(t1,2)*(1942 - 2210*t2) - 1351*t2 + 
                  1153*Power(t2,2) + 372*Power(t2,3) + 
                  4*t1*(444 - 849*t2 + 410*Power(t2,2))) + 
               s2*(-1 + 65*Power(t1,4) - 505*t2 - 1187*Power(t2,2) + 
                  2178*Power(t2,3) - 617*Power(t2,4) - 
                  6*Power(t1,3)*(-197 + 211*t2) + 
                  2*Power(t1,2)*(986 - 1380*t2 + 151*Power(t2,2)) + 
                  t1*(119 - 868*t2 - 127*Power(t2,2) + 1602*Power(t2,3)))\
) - Power(s1,4)*(-165 + 10*Power(s2,6) + 45*Power(t1,6) + 
               Power(t1,5)*(274 - 227*t2) - 163*t2 + 55*Power(t2,2) + 
               2127*Power(t2,3) - 2510*Power(t2,4) + 644*Power(t2,5) + 
               12*Power(t2,6) + Power(s2,5)*(22 - 113*t1 + 132*t2) + 
               Power(t1,4)*(599 - 1327*t2 + 555*Power(t2,2)) + 
               Power(t1,3)*(1075 - 3217*t2 + 2365*Power(t2,2) - 
                  105*Power(t2,3)) + 
               Power(t1,2)*(1359 - 2761*t2 + 2047*Power(t2,2) - 
                  101*Power(t2,3) - 595*Power(t2,4)) + 
               t1*(439 - 877*t2 - 598*Power(t2,2) + 2919*Power(t2,3) - 
                  1968*Power(t2,4) + 305*Power(t2,5)) + 
               Power(s2,4)*(213 + 397*Power(t1,2) - 557*t2 + 
                  383*Power(t2,2) - t1*(14 + 695*t2)) - 
               Power(s2,3)*(841 + 658*Power(t1,3) - 1851*t2 + 
                  2319*Power(t2,2) - 1285*Power(t2,3) - 
                  4*Power(t1,2)*(-91 + 380*t2) + 
                  t1*(944 - 3390*t2 + 2484*Power(t2,2))) + 
               Power(s2,2)*(843 + 572*Power(t1,4) + 
                  Power(t1,3)*(956 - 1710*t2) - 2925*t2 + 
                  5516*Power(t2,2) - 3263*Power(t2,3) + 
                  310*Power(t2,4) + 
                  2*Power(t1,2)*(924 - 3218*t2 + 2187*Power(t2,2)) + 
                  t1*(3045 - 8555*t2 + 8939*Power(t2,2) - 
                     3319*Power(t2,3))) + 
               s2*(-52 - 253*Power(t1,5) + 1755*t2 - 4061*Power(t2,2) + 
                  1278*Power(t2,3) + 1292*Power(t2,4) - 
                  432*Power(t2,5) + Power(t1,4)*(-874 + 980*t2) - 
                  2*Power(t1,3)*(858 - 2465*t2 + 1414*Power(t2,2)) + 
                  3*Power(t1,2)*
                   (-1093 + 3307*t2 - 2995*Power(t2,2) + 
                     713*Power(t2,3)) + 
                  t1*(-2562 + 6978*t2 - 8857*Power(t2,2) + 
                     3652*Power(t2,3) + 359*Power(t2,4)))) - 
            Power(s1,3)*(214 + 18*Power(s2,7) - 4*Power(t1,7) - 
               1016*t2 + 3294*Power(t2,2) - 3762*Power(t2,3) + 
               109*Power(t2,4) + 1556*Power(t2,5) - 395*Power(t2,6) - 
               Power(t1,6)*(127 + 40*t2) + 
               Power(s2,6)*(9 - 152*t1 + 100*t2) + 
               Power(s2,5)*(27 + 494*Power(t1,2) + t1*(76 - 544*t2) - 
                  139*t2 + 55*Power(t2,2)) + 
               Power(t1,5)*(875 - 353*t2 + 268*Power(t2,2)) + 
               Power(t1,4)*(1559 - 4026*t2 + 2174*Power(t2,2) - 
                  588*Power(t2,3)) + 
               Power(t1,3)*(1197 - 5739*t2 + 8237*Power(t2,2) - 
                  3661*Power(t2,3) + 412*Power(t2,4)) + 
               Power(t1,2)*(1982 - 7577*t2 + 10422*Power(t2,2) - 
                  6923*Power(t2,3) + 1792*Power(t2,4) + 48*Power(t2,5)) \
+ t1*(1612 - 6503*t2 + 10152*Power(t2,2) - 6128*Power(t2,3) + 
                  329*Power(t2,4) + 590*Power(t2,5) - 96*Power(t2,6)) - 
               Power(s2,4)*(291 + 820*Power(t1,3) + 
                  Power(t1,2)*(521 - 1136*t2) + 546*t2 - 
                  324*Power(t2,2) + 122*Power(t2,3) + 
                  t1*(-585 - 603*t2 + 98*Power(t2,2))) + 
               Power(s2,3)*(-288 + 750*Power(t1,4) + 1973*t2 - 
                  3258*Power(t2,2) + 2001*Power(t2,3) - 
                  823*Power(t2,4) - 8*Power(t1,3)*(-143 + 138*t2) - 
                  2*Power(t1,2)*(1396 + 311*t2 + 152*Power(t2,2)) + 
                  2*t1*(61 + 2105*t2 - 1516*Power(t2,2) + 
                     750*Power(t2,3))) + 
               Power(s2,2)*(1297 - 368*Power(t1,5) - 3993*t2 + 
                  7414*Power(t2,2) - 8469*Power(t2,3) + 
                  3645*Power(t2,4) - 498*Power(t2,5) + 
                  Power(t1,4)*(-1201 + 436*t2) + 
                  Power(t1,3)*(4596 - 362*t2 + 950*Power(t2,2)) + 
                  Power(t1,2)*
                   (2188 - 10808*t2 + 7266*Power(t2,2) - 
                     3222*Power(t2,3)) + 
                  t1*(1081 - 8981*t2 + 15979*Power(t2,2) - 
                     9371*Power(t2,3) + 2612*Power(t2,4))) + 
               s2*(-1018 + 82*Power(t1,6) + 3508*t2 - 8085*Power(t2,2) + 
                  10080*Power(t2,3) - 4910*Power(t2,4) + 
                  335*Power(t2,5) + 134*Power(t2,6) + 
                  4*Power(t1,5)*(155 + 4*t2) + 
                  Power(t1,4)*(-3291 + 873*t2 - 871*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (-1789 + 5585*t2 - 3366*Power(t2,2) + 
                     1216*Power(t2,3)) + 
                  Power(t1,2)*
                   (-1990 + 12747*t2 - 20958*Power(t2,2) + 
                     11031*Power(t2,3) - 2201*Power(t2,4)) + 
                  t1*(-3163 + 12134*t2 - 20016*Power(t2,2) + 
                     17450*Power(t2,3) - 5973*Power(t2,4) + 
                     428*Power(t2,5)))) + 
            Power(s1,2)*(-462 + 1922*t2 - 4442*Power(t2,2) + 
               7728*Power(t2,3) - 7479*Power(t2,4) + 2660*Power(t2,5) + 
               177*Power(t2,6) - 104*Power(t2,7) + 
               6*Power(s2,7)*(1 + t2) - 2*Power(t1,7)*(2 + t2) + 
               Power(t1,6)*(151 - 167*t2 - 9*Power(t2,2)) + 
               Power(t1,5)*(-348 + 442*t2 - 2*Power(t2,2) + 
                  99*Power(t2,3)) + 
               Power(t1,4)*(-2230 + 4380*t2 - 3563*Power(t2,2) + 
                  1161*Power(t2,3) - 227*Power(t2,4)) + 
               Power(t1,3)*(-2483 + 9168*t2 - 11208*Power(t2,2) + 
                  6943*Power(t2,3) - 2116*Power(t2,4) + 195*Power(t2,5)) \
+ Power(t1,2)*(-985 + 8654*t2 - 18243*Power(t2,2) + 15188*Power(t2,3) - 
                  6176*Power(t2,4) + 1354*Power(t2,5) - 48*Power(t2,6)) \
- t1*(607 - 5105*t2 + 14821*Power(t2,2) - 19084*Power(t2,3) + 
                  10581*Power(t2,4) - 2026*Power(t2,5) + 
                  122*Power(t2,6) + 8*Power(t2,7)) + 
               Power(s2,6)*(79 - 43*t2 + 84*Power(t2,2) - 
                  2*t1*(4 + 51*t2)) + 
               Power(s2,5)*(6 + 698*t2 - 588*Power(t2,2) + 
                  182*Power(t2,3) + Power(t1,2)*(-46 + 422*t2) + 
                  t1*(-722 + 642*t2 - 599*Power(t2,2))) + 
               Power(s2,4)*(251 + Power(t1,3)*(140 - 790*t2) - 624*t2 + 
                  978*Power(t2,2) - 815*Power(t2,3) + 190*Power(t2,4) + 
                  Power(t1,2)*(2249 - 2305*t2 + 1547*Power(t2,2)) - 
                  t1*(396 + 2902*t2 - 3058*Power(t2,2) + 
                     845*Power(t2,3))) + 
               Power(s2,3)*(-919 + 47*t2 + 1556*Power(t2,2) - 
                  1134*Power(t2,3) + 146*Power(t2,4) - 
                  146*Power(t2,5) + 10*Power(t1,4)*(-15 + 77*t2) - 
                  2*Power(t1,3)*(1678 - 1830*t2 + 939*Power(t2,2)) + 
                  4*Power(t1,2)*
                   (375 + 1019*t2 - 1411*Power(t2,2) + 336*Power(t2,3)) \
+ t1*(1391 - 1460*t2 - 1057*Power(t2,2) + 1864*Power(t2,3) - 
                     183*Power(t2,4))) + 
               Power(s2,2)*(377 + Power(t1,5)*(64 - 386*t2) + 1234*t2 - 
                  4822*Power(t2,2) + 6464*Power(t2,3) - 
                  5005*Power(t2,4) + 1642*Power(t2,5) - 
                  206*Power(t2,6) + 
                  Power(t1,4)*(2609 - 2925*t2 + 1082*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (1092 + 898*t2 - 2232*Power(t2,2) + 391*Power(t2,3)) \
- Power(t1,2)*(5765 - 9172*t2 + 4383*Power(t2,2) + 122*Power(t2,3) + 
                     431*Power(t2,4)) + 
                  t1*(25 + 6826*t2 - 12332*Power(t2,2) + 
                     9247*Power(t2,3) - 3062*Power(t2,4) + 
                     723*Power(t2,5))) + 
               s2*(640 - 3057*t2 + 7151*Power(t2,2) - 
                  11003*Power(t2,3) + 9676*Power(t2,4) - 
                  4129*Power(t2,5) + 634*Power(t2,6) + 12*Power(t2,7) + 
                  Power(t1,6)*(-2 + 82*t2) + 
                  Power(t1,5)*(-1010 + 1138*t2 - 227*Power(t2,2)) + 
                  2*Power(t1,4)*
                   (711 - 259*t2 - 644*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,3)*
                   (6353 - 11468*t2 + 8025*Power(t2,2) - 
                     2088*Power(t2,3) + 651*Power(t2,4)) + 
                  Power(t1,2)*
                   (3377 - 16041*t2 + 21984*Power(t2,2) - 
                     15056*Power(t2,3) + 5032*Power(t2,4) - 
                     772*Power(t2,5)) + 
                  t1*(178 - 8402*t2 + 21977*Power(t2,2) - 
                     22624*Power(t2,3) + 12557*Power(t2,4) - 
                     3366*Power(t2,5) + 252*Power(t2,6)))) + 
            s1*(215 - 298*t2 - 707*Power(t2,2) + 2900*Power(t2,3) - 
               5045*Power(t2,4) + 4532*Power(t2,5) - 1741*Power(t2,6) + 
               136*Power(t2,7) + 8*Power(t2,8) - 
               2*Power(t1,7)*(-8 + 5*t2) + 
               2*Power(s2,7)*(6 - 9*t2 + 4*Power(t2,2)) + 
               Power(t1,6)*(-87 - 7*t2 + 49*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,5)*(314 - 203*t2 - 7*Power(t2,2) + 
                  4*Power(t2,3) - 20*Power(t2,4)) + 
               Power(t1,4)*(-1039 + 1613*t2 - 905*Power(t2,2) + 
                  503*Power(t2,3) - 264*Power(t2,4) + 36*Power(t2,5)) + 
               Power(t1,3)*(-2544 + 8739*t2 - 10353*Power(t2,2) + 
                  5131*Power(t2,3) - 1459*Power(t2,4) + 
                  458*Power(t2,5) - 28*Power(t2,6)) + 
               Power(t1,2)*(-749 + 6584*t2 - 16453*Power(t2,2) + 
                  17447*Power(t2,3) - 8117*Power(t2,4) + 
                  1694*Power(t2,5) - 317*Power(t2,6) + 8*Power(t2,7)) + 
               t1*(522 + 138*t2 - 5964*Power(t2,2) + 13829*Power(t2,3) - 
                  13681*Power(t2,4) + 5922*Power(t2,5) - 
                  876*Power(t2,6) + 72*Power(t2,7)) + 
               Power(s2,6)*(14 + 13*t2 - 56*Power(t2,2) + 
                  16*Power(t2,3) + t1*(-64 + 82*t2 - 24*Power(t2,2))) + 
               Power(s2,5)*(-98 + 316*t2 - 511*Power(t2,2) + 
                  261*Power(t2,3) - 32*Power(t2,4) - 
                  2*Power(t1,2)*(-62 + 65*t2) + 
                  t1*(-149 + 314*t2 - 71*Power(t2,2) + 12*Power(t2,3))) + 
               Power(s2,4)*(-44 - 1113*t2 + 2055*Power(t2,2) - 
                  1549*Power(t2,3) + 677*Power(t2,4) - 98*Power(t2,5) + 
                  10*Power(t1,3)*(-8 + 5*t2 + 8*Power(t2,2)) + 
                  Power(t1,2)*
                   (369 - 1393*t2 + 893*Power(t2,2) - 204*Power(t2,3)) + 
                  t1*(756 - 1891*t2 + 2945*Power(t2,2) - 
                     1688*Power(t2,3) + 222*Power(t2,4))) + 
               Power(s2,3)*(-485 + 1222*t2 - 2230*Power(t2,2) + 
                  2327*Power(t2,3) - 1237*Power(t2,4) + 
                  461*Power(t2,5) - 34*Power(t2,6) - 
                  30*Power(t1,4)*(2 - 3*t2 + 4*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (-133 + 1086*t2 - 871*Power(t2,2) + 188*Power(t2,3)) \
- 2*Power(t1,2)*(997 - 1990*t2 + 2881*Power(t2,2) - 1747*Power(t2,3) + 
                     227*Power(t2,4)) + 
                  t1*(1525 + 1204*t2 - 5530*Power(t2,2) + 
                     5060*Power(t2,3) - 2239*Power(t2,4) + 
                     252*Power(t2,5))) + 
               Power(s2,2)*(1223 - 1714*t2 - 446*Power(t2,2) + 
                  1177*Power(t2,3) - 434*Power(t2,4) + 442*Power(t2,5) - 
                  187*Power(t2,6) + 24*Power(t2,7) + 
                  2*Power(t1,5)*(64 - 61*t2 + 36*Power(t2,2)) - 
                  Power(t1,4)*
                   (136 + 1493*t2 - 1418*Power(t2,2) + 264*Power(t2,3)) \
+ Power(t1,3)*(2426 - 3754*t2 + 4726*Power(t2,2) - 2964*Power(t2,3) + 
                     350*Power(t2,4)) + 
                  Power(t1,2)*
                   (-3957 + 2544*t2 + 3990*Power(t2,2) - 
                     4970*Power(t2,3) + 2183*Power(t2,4) - 
                     174*Power(t2,5)) - 
                  t1*(1760 - 6199*t2 + 4897*Power(t2,2) + 
                     391*Power(t2,3) - 1141*Power(t2,4) + 
                     388*Power(t2,5) + 8*Power(t2,6))) + 
               s2*(-837 + 1296*t2 + 1411*Power(t2,2) - 5062*Power(t2,3) + 
                  5645*Power(t2,4) - 3327*Power(t2,5) + 
                  1104*Power(t2,6) - 192*Power(t2,7) - 
                  2*Power(t1,6)*(38 - 29*t2 + 8*Power(t2,2)) + 
                  Power(t1,5)*
                   (255 + 394*t2 - 491*Power(t2,2) + 60*Power(t2,3)) + 
                  Power(t1,4)*
                   (-1404 + 1552*t2 - 1391*Power(t2,2) + 
                     893*Power(t2,3) - 66*Power(t2,4)) + 
                  Power(t1,3)*
                   (3515 - 4248*t2 + 390*Power(t2,2) + 956*Power(t2,3) - 
                     357*Power(t2,4) - 16*Power(t2,5)) + 
                  Power(t1,2)*
                   (4789 - 16160*t2 + 17480*Power(t2,2) - 
                     7067*Power(t2,3) + 1555*Power(t2,4) - 
                     531*Power(t2,5) + 70*Power(t2,6)) - 
                  t1*(502 + 4264*t2 - 15171*Power(t2,2) + 
                     17048*Power(t2,3) - 8415*Power(t2,4) + 
                     2542*Power(t2,5) - 620*Power(t2,6) + 32*Power(t2,7)))\
)))*R1(1 - s + s1 - t2))/
     ((-1 + s1)*(-s + s1 - t2)*(1 - s + s1 - t2)*Power(s1 - s2 + t1 - t2,3)*
       (1 - s + s*s1 - s1*s2 + s1*t1 - t2)*(-1 + t2)*(-1 + s2 - t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) + 
    (8*(66 - 25*s2 - 50*Power(s2,2) + 3*Power(s2,3) + 6*Power(s2,4) + 
         108*t1 + 15*s2*t1 - 16*Power(s2,2)*t1 - 15*Power(s2,3)*t1 + 
         32*Power(t1,2) + 25*s2*Power(t1,2) + 
         10*Power(s2,2)*Power(t1,2) - 12*Power(t1,3) + s2*Power(t1,3) - 
         2*Power(t1,4) - 266*t2 + 36*s2*t2 + 149*Power(s2,2)*t2 - 
         2*Power(s2,3)*t2 - 9*Power(s2,4)*t2 - 312*t1*t2 - 103*s2*t1*t2 + 
         38*Power(s2,2)*t1*t2 + 13*Power(s2,3)*t1*t2 - 
         34*Power(t1,2)*t2 - 68*s2*Power(t1,2)*t2 + 
         5*Power(s2,2)*Power(t1,2)*t2 + 32*Power(t1,3)*t2 - 
         13*s2*Power(t1,3)*t2 + 4*Power(t1,4)*t2 + 410*Power(t2,2) + 
         5*s2*Power(t2,2) - 147*Power(s2,2)*Power(t2,2) + 
         Power(s2,3)*Power(t2,2) + 7*Power(s2,4)*Power(t2,2) + 
         300*t1*Power(t2,2) + 158*s2*t1*Power(t2,2) - 
         42*Power(s2,2)*t1*Power(t2,2) - 10*Power(s2,3)*t1*Power(t2,2) - 
         24*Power(t1,2)*Power(t2,2) + 67*s2*Power(t1,2)*Power(t2,2) - 
         Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         26*Power(t1,3)*Power(t2,2) + 4*s2*Power(t1,3)*Power(t2,2) - 
         270*Power(t2,3) - 44*s2*Power(t2,3) + 
         60*Power(s2,2)*Power(t2,3) + 4*Power(s2,3)*Power(t2,3) - 
         2*Power(s2,4)*Power(t2,3) - 88*t1*Power(t2,3) - 
         86*s2*t1*Power(t2,3) + 2*Power(s2,2)*t1*Power(t2,3) + 
         4*Power(s2,3)*t1*Power(t2,3) + 30*Power(t1,2)*Power(t2,3) - 
         6*s2*Power(t1,2)*Power(t2,3) - 
         2*Power(s2,2)*Power(t1,2)*Power(t2,3) + 60*Power(t2,4) + 
         28*s2*Power(t2,4) - 8*Power(s2,2)*Power(t2,4) - 
         8*t1*Power(t2,4) + 8*s2*t1*Power(t2,4) - 
         Power(s1,5)*(s2 - t1)*(-4 + 3*s2 - 5*t1 + 6*t2) - 
         2*Power(s,5)*(-1 + Power(t1,2) + s1*(1 + t1 - 2*t2) + 2*t2 + 
            Power(t2,2) - t1*(1 + 2*t2)) + 
         Power(s1,4)*(-4 - 7*Power(s2,3) + 2*Power(t1,3) + 
            Power(s2,2)*(14 + 18*t1 - 9*t2) + 10*t2 - 6*Power(t2,2) + 
            Power(t1,2)*(5 + 8*t2) + 2*t1*(2 + t2 - 6*Power(t2,2)) + 
            s2*(-6 - 13*Power(t1,2) + t1*(-19 + t2) + 12*Power(t2,2))) + 
         Power(s,4)*(Power(s1,2)*(12 + 7*t1 - 19*t2) + 
            s1*(-22 - 13*t1 + 2*Power(t1,2) + s2*(10 + 7*t1 - 11*t2) + 
               21*t2 - 10*t1*t2 + 16*Power(t2,2)) - 
            2*(-4 + 2*Power(t1,3) + 5*Power(t2,2) + Power(t2,3) - 
               Power(t1,2)*(5 + 3*t2) + t1*(-3 + 4*t2)) + 
            s2*(-8 + 4*Power(t1,2) + 11*t2 + 8*Power(t2,2) - 
               3*t1*(3 + 4*t2))) + 
         Power(s1,3)*(-Power(s2,4) + 3*Power(t1,4) - 
            2*Power(t1,3)*(-7 + 9*t2) + 
            Power(s2,3)*(-6 - 4*t1 + 15*t2) + 
            t1*(1 + 7*t2 + 14*Power(t2,2)) + 
            Power(t1,2)*(11 - 34*t2 + 16*Power(t2,2)) + 
            Power(s2,2)*(31 + 14*Power(t1,2) + t1*(37 - 65*t2) - 
               52*t2 + 42*Power(t2,2)) + 
            3*(3 - 4*t2 - 3*Power(t2,2) + 4*Power(t2,3)) - 
            s2*(10 + 12*Power(t1,3) + Power(t1,2)*(45 - 68*t2) - 19*t2 + 
               31*Power(t2,2) + t1*(48 - 96*t2 + 62*Power(t2,2)))) + 
         Power(s1,2)*(-4 + 3*Power(s2,5) + 8*t2 + 18*Power(t2,2) - 
            22*Power(t2,3) + Power(t1,4)*(-9 + 4*t2) + 
            Power(s2,4)*(-10 - 18*t1 + 21*t2) - 
            2*Power(t1,3)*(-40 + 7*t2 + 2*Power(t2,2)) + 
            Power(s2,3)*(1 + 36*Power(t1,2) + t1*(53 - 81*t2) - 22*t2 + 
               36*Power(t2,2)) + 
            Power(t1,2)*(135 - 262*t2 + 62*Power(t2,2)) - 
            2*t1*(-19 + 92*t2 - 76*Power(t2,2) + 20*Power(t2,3)) + 
            Power(s2,2)*(38 - 30*Power(t1,3) - 188*t2 + 
               51*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,2)*(-85 + 103*t2) + 
               t1*(74 + 32*t2 - 68*Power(t2,2))) + 
            s2*(-49 + 9*Power(t1,4) + Power(t1,3)*(51 - 47*t2) + 
               208*t2 - 195*Power(t2,2) + 70*Power(t2,3) + 
               Power(t1,2)*(-155 + 4*t2 + 36*Power(t2,2)) + 
               2*t1*(-87 + 230*t2 - 62*Power(t2,2) + 4*Power(t2,3)))) + 
         s1*(25 - 6*Power(t1,5) + 3*Power(s2,5)*(-2 + t2) - 164*t2 + 
            243*Power(t2,2) - 128*Power(t2,3) + 24*Power(t2,4) + 
            Power(t1,4)*(-4 + 26*t2) + 
            Power(t1,3)*(29 + 7*t2 - 34*Power(t2,2)) + 
            Power(s2,4)*(-6 + 21*t1 + 2*t2 - 9*t1*t2 + 6*Power(t2,2)) + 
            t1*(177 - 543*t2 + 486*Power(t2,2) - 104*Power(t2,3)) + 
            Power(t1,2)*(179 - 298*t2 + 103*Power(t2,2) + 
               12*Power(t2,3)) + 
            Power(s2,3)*(60 - 130*t2 + 63*Power(t2,2) - 8*Power(t2,3) + 
               3*Power(t1,2)*(-7 + 3*t2) + 
               t1*(34 - 52*t2 - 6*Power(t2,2))) + 
            Power(s2,2)*(25 + 10*t2 - 79*Power(t2,2) + 58*Power(t2,3) - 
               3*Power(t1,3)*(1 + t2) - 
               2*Power(t1,2)*(27 - 62*t2 + 3*Power(t2,2)) + 
               2*t1*(-54 + 151*t2 - 90*Power(t2,2) + 8*Power(t2,3))) + 
            s2*(-98 + 15*Power(t1,4) + 399*t2 - 403*Power(t2,2) + 
               94*Power(t2,3) - 8*Power(t2,4) + 
               2*Power(t1,3)*(15 - 50*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(19 - 179*t2 + 151*Power(t2,2) - 
                  8*Power(t2,3)) - 
               2*t1*(101 - 140*t2 + 10*Power(t2,2) + 34*Power(t2,3)))) + 
         Power(s,3)*(-(Power(s1,3)*(22 + s2 + 9*t1 - 32*t2)) + 
            Power(s2,2)*(8 - 2*Power(t1,2) - 7*t2 - 12*Power(t2,2) + 
               4*t1*(1 + 3*t2)) + 
            s2*(-3 + 4*Power(t1,3) - 24*t2 + 19*Power(t2,2) + 
               8*Power(t2,3) - 4*Power(t1,2)*(5 + 3*t2) + t1*(4 + 13*t2)\
) + Power(s1,2)*(59 + 3*Power(s2,2) + 9*Power(t1,2) + t1*(36 - 7*t2) - 
               45*t2 - 38*Power(t2,2) + s2*(-37 - 26*t1 + 46*t2)) - 
            2*(-9 - 8*Power(t1,3) + Power(t1,4) + 7*t2 - Power(t2,2) + 
               9*Power(t2,3) + Power(t1,2)*(2 + 7*t2 - 3*Power(t2,2)) + 
               t1*(-5 - 4*t2 - 2*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(-55 + 10*Power(t1,3) + 24*t2 + 49*Power(t2,2) + 
               8*Power(t2,3) + Power(s2,2)*(-21 - 5*t1 + 6*t2) - 
               2*Power(t1,2)*(14 + 15*t2) + 
               t1*(-52 + 13*t2 + 24*Power(t2,2)) + 
               s2*(51 - 5*Power(t1,2) - 28*t2 - 54*Power(t2,2) + 
                  t1*(48 + 45*t2)))) + 
         Power(s,2)*(Power(s1,4)*(16 + 2*s2 + 5*t1 - 23*t2) + 
            Power(s2,3)*(4 + t1*(3 - 4*t2) - 3*t2 + 8*Power(t2,2)) + 
            Power(s2,2)*(-48 + 39*t2 - Power(t2,2) - 12*Power(t2,3) + 
               Power(t1,2)*(2 + 6*t2) - 2*t1*(13 + 7*t2)) + 
            Power(s1,3)*(-56 - 7*Power(s2,2) - 21*Power(t1,2) + 
               s2*(42 + 37*t1 - 63*t2) + 41*t2 + 36*Power(t2,2) + 
               t1*(-41 + 32*t2)) - 
            2*(16 + Power(t1,4)*(-4 + t2) - 7*t2 - Power(t2,2) + 
               2*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,3)*(12 + 3*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(-18 - 7*t2 - 4*Power(t2,2) + Power(t2,3)) + 
               t1*(-16 + 46*t2 - 8*Power(t2,2) + 5*Power(t2,3))) + 
            s2*(55 - 13*Power(t1,3) + 35*t2 - 43*Power(t2,2) + 
               40*Power(t2,3) + 
               Power(t1,2)*(46 + 23*t2 - 12*Power(t2,2)) + 
               t1*(-2 - 23*t2 - 26*Power(t2,2) + 12*Power(t2,3))) - 
            Power(s1,2)*(-56 + 3*Power(s2,3) + 7*Power(t1,3) - 5*t2 + 
               104*Power(t2,2) + 6*Power(t2,3) + 
               Power(s2,2)*(-36 - 17*t1 + 14*t2) - 
               Power(t1,2)*(31 + 49*t2) + 
               t1*(-65 - 18*t2 + 72*Power(t2,2)) + 
               s2*(42 + 7*Power(t1,2) + 9*t2 - 112*Power(t2,2) + 
                  t1*(66 + 59*t2))) + 
            s1*(7 + 6*Power(t1,4) + 21*t2 - 27*Power(t2,2) + 
               62*Power(t2,3) + Power(s2,3)*(6 - 3*t1 + 8*t2) + 
               3*Power(t1,2)*(-26 + 9*t2) - Power(t1,3)*(25 + 14*t2) + 
               t1*(-76 + 141*t2 - 30*Power(t2,2) + 16*Power(t2,3)) + 
               Power(s2,2)*(30 + 12*Power(t1,2) - 33*t2 + 
                  66*Power(t2,2) - t1*(8 + 69*t2)) + 
               s2*(-66 - 15*Power(t1,3) + 16*t2 - 59*Power(t2,2) - 
                  24*Power(t2,3) + 3*Power(t1,2)*(9 + 25*t2) + 
                  t1*(29 + 32*t2 - 54*Power(t2,2))))) - 
         s*(Power(s1,5)*(4 + s2 + t1 - 6*t2) + 
            Power(s2,4)*(6 - 3*t2 + 2*Power(t2,2)) - 
            Power(s2,3)*(-7 + 2*t2 - 15*Power(t2,2) + 8*Power(t2,3) + 
               3*t1*(4 + 3*t2)) + 
            Power(s1,4)*(-17 - 7*Power(s2,2) - 17*Power(t1,2) + 
               s2*(19 + 26*t1 - 34*t2) + 13*t2 + 12*Power(t2,2) + 
               5*t1*(-4 + 5*t2)) + 
            s2*(17 + 2*t2 - 45*Power(t2,2) + 32*Power(t2,3) - 
               16*Power(t2,4) + 
               Power(t1,3)*(-8 - 7*t2 + 4*Power(t2,2)) + 
               t1*(12 - 93*t2 + 26*Power(t2,2) - 8*Power(t2,3)) + 
               Power(t1,2)*(55 - 38*t2 + 11*Power(t2,2) - 4*Power(t2,3))) \
+ Power(s2,2)*(-92 + 165*t2 - 64*Power(t2,2) + 26*Power(t2,3) + 
               Power(t1,2)*(10 + 23*t2 - 6*Power(t2,2)) + 
               2*t1*(-19 + 13*t2 - 16*Power(t2,2) + 6*Power(t2,3))) + 
            2*(31 - 2*Power(t1,4)*(-1 + t2) - 121*t2 + 141*Power(t2,2) - 
               53*Power(t2,3) + 10*Power(t2,4) + 
               Power(t1,3)*(-12 + 7*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(35 - 22*t2 + 6*Power(t2,2) - 
                  4*Power(t2,3)) + 
               t1*(80 - 182*t2 + 119*Power(t2,2) - 25*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s1,3)*(11 - 9*Power(s2,3) + Power(t1,3) + 
               Power(s2,2)*(29 + 28*t1 - 16*t2) + 28*t2 - 
               67*Power(t2,2) + 2*Power(t1,2)*(8 + 17*t2) + 
               t1*(27 + 23*t2 - 60*Power(t2,2)) - 
               s2*(12 + 20*Power(t1,2) + 20*t2 - 78*Power(t2,2) + 
                  t1*(44 + 27*t2))) + 
            Power(s1,2)*(-7 + 3*Power(s2,4) + 7*Power(t1,4) + 
               Power(t1,3)*(18 - 35*t2) + 77*t2 - 82*Power(t2,2) + 
               68*Power(t2,3) + Power(s2,3)*(-7 - 20*t1 + 34*t2) + 
               Power(t1,2)*(-78 - 53*t2 + 32*Power(t2,2)) + 
               Power(s2,2)*(22 + 38*Power(t1,2) + t1*(58 - 147*t2) - 
                  94*t2 + 110*Power(t2,2)) + 
               t1*(-126 + 201*t2 + 2*Power(t2,2) + 8*Power(t2,3)) - 
               s2*(-12 + 28*Power(t1,3) + Power(t1,2)*(69 - 148*t2) + 
                  88*t2 + 65*Power(t2,2) + 12*Power(t2,3) + 
                  t1*(-42 - 171*t2 + 140*Power(t2,2)))) - 
            s1*(73 + Power(s2,4)*(13 + 3*t1 - 10*t2) + 
               2*Power(t1,4)*(-10 + t2) - 150*t2 + 113*Power(t2,2) + 
               6*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,3)*(-40 + 71*t2 - 8*Power(t2,2)) - 
               Power(s2,3)*(49 + 9*Power(t1,2) + t1*(48 - 43*t2) - 
                  38*t2 + 34*Power(t2,2)) + 
               Power(t1,2)*(-63 + 162*t2 - 107*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(38 + 39*t2 - 60*Power(t2,2) + 56*Power(t2,3)) + 
               Power(s2,2)*(-6 + 9*Power(t1,3) + 
                  Power(t1,2)*(37 - 54*t2) + 150*t2 - 53*Power(t2,2) + 
                  24*Power(t2,3) + t1*(69 - 5*t2 + 36*Power(t2,2))) + 
               s2*(11 - 3*Power(t1,4) - 260*t2 + 238*Power(t2,2) - 
                  120*Power(t2,3) + Power(t1,3)*(18 + 19*t2) + 
                  2*Power(t1,2)*(10 - 52*t2 + 3*Power(t2,2)) + 
                  t1*(78 - 339*t2 + 186*Power(t2,2) - 32*Power(t2,3))))))*
       R2(1 - s2 + t1 - t2))/
     ((-1 + s1)*(-s + s1 - t2)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       (-1 + t2)*(-1 + s2 - t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) - 
    (8*(30 - 38*s2 + 5*Power(s2,2) + Power(s2,3) + Power(s2,4) + 
         Power(s2,5) + 125*t1 - 137*s2*t1 - 37*Power(s2,2)*t1 + 
         62*Power(s2,3)*t1 + 6*Power(s2,4)*t1 - 3*Power(s2,5)*t1 + 
         117*Power(t1,2) + 84*s2*Power(t1,2) - 
         171*Power(s2,2)*Power(t1,2) - 41*Power(s2,3)*Power(t1,2) + 
         5*Power(s2,4)*Power(t1,2) - 2*Power(s2,5)*Power(t1,2) - 
         48*Power(t1,3) + 152*s2*Power(t1,3) + 
         77*Power(s2,2)*Power(t1,3) + 10*Power(s2,3)*Power(t1,3) + 
         10*Power(s2,4)*Power(t1,3) - 44*Power(t1,4) - 
         60*s2*Power(t1,4) - 30*Power(s2,2)*Power(t1,4) - 
         20*Power(s2,3)*Power(t1,4) + 17*Power(t1,5) + 
         25*s2*Power(t1,5) + 20*Power(s2,2)*Power(t1,5) - 7*Power(t1,6) - 
         10*s2*Power(t1,6) + 2*Power(t1,7) - 
         Power(s,8)*(t1 - t2)*(-1 + s1 + t1 - t2) - 235*t2 + 114*s2*t2 + 
         197*Power(s2,2)*t2 - 8*Power(s2,3)*t2 - 77*Power(s2,4)*t2 - 
         10*Power(s2,5)*t2 + 3*Power(s2,6)*t2 - 496*t1*t2 + 18*s2*t1*t2 + 
         236*Power(s2,2)*t1*t2 + 94*Power(s2,3)*t1*t2 + 
         24*Power(s2,4)*t1*t2 + 8*Power(s2,5)*t1*t2 + 
         4*Power(s2,6)*t1*t2 - 156*Power(t1,2)*t2 - 
         445*s2*Power(t1,2)*t2 + 82*Power(s2,2)*Power(t1,2)*t2 - 
         4*Power(s2,3)*Power(t1,2)*t2 - 62*Power(s2,4)*Power(t1,2)*t2 - 
         19*Power(s2,5)*Power(t1,2)*t2 + 217*Power(t1,3)*t2 - 
         138*s2*Power(t1,3)*t2 - 32*Power(s2,2)*Power(t1,3)*t2 + 
         108*Power(s2,3)*Power(t1,3)*t2 + 35*Power(s2,4)*Power(t1,3)*t2 + 
         39*Power(t1,4)*t2 + 30*s2*Power(t1,4)*t2 - 
         77*Power(s2,2)*Power(t1,4)*t2 - 30*Power(s2,3)*Power(t1,4)*t2 - 
         8*Power(t1,5)*t2 + 20*s2*Power(t1,5)*t2 + 
         10*Power(s2,2)*Power(t1,5)*t2 + s2*Power(t1,6)*t2 - 
         Power(t1,7)*t2 + 627*Power(t2,2) + 65*s2*Power(t2,2) - 
         615*Power(s2,2)*Power(t2,2) - 116*Power(s2,3)*Power(t2,2) + 
         157*Power(s2,4)*Power(t2,2) + 17*Power(s2,5)*Power(t2,2) - 
         13*Power(s2,6)*Power(t2,2) - 2*Power(s2,7)*Power(t2,2) + 
         573*t1*Power(t2,2) + 759*s2*t1*Power(t2,2) - 
         98*Power(s2,2)*t1*Power(t2,2) - 340*Power(s2,3)*t1*Power(t2,2) - 
         15*Power(s2,4)*t1*Power(t2,2) + 49*Power(s2,5)*t1*Power(t2,2) + 
         8*Power(s2,6)*t1*Power(t2,2) - 219*Power(t1,2)*Power(t2,2) + 
         441*s2*Power(t1,2)*Power(t2,2) + 
         213*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         67*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         66*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         10*Power(s2,5)*Power(t1,2)*Power(t2,2) - 
         227*Power(t1,3)*Power(t2,2) - 34*s2*Power(t1,3)*Power(t2,2) + 
         121*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         34*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         4*Power(t1,4)*Power(t2,2) - 66*s2*Power(t1,4)*Power(t2,2) - 
         Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         10*Power(s2,3)*Power(t1,4)*Power(t2,2) + 
         10*Power(t1,5)*Power(t2,2) - 3*s2*Power(t1,5)*Power(t2,2) - 
         8*Power(s2,2)*Power(t1,5)*Power(t2,2) + 
         2*s2*Power(t1,6)*Power(t2,2) - 715*Power(t2,3) - 
         476*s2*Power(t2,3) + 577*Power(s2,2)*Power(t2,3) + 
         247*Power(s2,3)*Power(t2,3) - 111*Power(s2,4)*Power(t2,3) - 
         34*Power(s2,5)*Power(t2,3) + 3*Power(s2,6)*Power(t2,3) + 
         Power(s2,7)*Power(t2,3) - 44*t1*Power(t2,3) - 
         950*s2*t1*Power(t2,3) - 301*Power(s2,2)*t1*Power(t2,3) + 
         296*Power(s2,3)*t1*Power(t2,3) + 
         102*Power(s2,4)*t1*Power(t2,3) - 12*Power(s2,5)*t1*Power(t2,3) - 
         5*Power(s2,6)*t1*Power(t2,3) + 398*Power(t1,2)*Power(t2,3) + 
         10*s2*Power(t1,2)*Power(t2,3) - 
         282*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         102*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         18*Power(s2,4)*Power(t1,2)*Power(t2,3) + 
         10*Power(s2,5)*Power(t1,2)*Power(t2,3) + 
         44*Power(t1,3)*Power(t2,3) + 120*s2*Power(t1,3)*Power(t2,3) + 
         34*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         12*Power(s2,3)*Power(t1,3)*Power(t2,3) - 
         10*Power(s2,4)*Power(t1,3)*Power(t2,3) - 
         23*Power(t1,4)*Power(t2,3) + 
         3*Power(s2,2)*Power(t1,4)*Power(t2,3) + 
         5*Power(s2,3)*Power(t1,4)*Power(t2,3) - 
         Power(s2,2)*Power(t1,5)*Power(t2,3) + 323*Power(t2,4) + 
         479*s2*Power(t2,4) - 146*Power(s2,2)*Power(t2,4) - 
         152*Power(s2,3)*Power(t2,4) + 5*Power(s2,4)*Power(t2,4) + 
         7*Power(s2,5)*Power(t2,4) - 260*t1*Power(t2,4) + 
         292*s2*t1*Power(t2,4) + 268*Power(s2,2)*t1*Power(t2,4) - 
         12*Power(s2,3)*t1*Power(t2,4) - 22*Power(s2,4)*t1*Power(t2,4) - 
         132*Power(t1,2)*Power(t2,4) - 146*s2*Power(t1,2)*Power(t2,4) + 
         8*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         24*Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         30*Power(t1,3)*Power(t2,4) - 
         10*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         Power(t1,4)*Power(t2,4) + s2*Power(t1,4)*Power(t2,4) - 
         14*Power(t2,5) - 144*s2*Power(t2,5) - 
         22*Power(s2,2)*Power(t2,5) + 12*Power(s2,3)*Power(t2,5) + 
         102*t1*Power(t2,5) + 26*s2*t1*Power(t2,5) - 
         20*Power(s2,2)*t1*Power(t2,5) - 12*Power(t1,2)*Power(t2,5) + 
         8*s2*Power(t1,2)*Power(t2,5) - 16*Power(t2,6) + 
         Power(s1,7)*Power(s2 - t1,2)*(-3 + 2*s2 - 2*t1 + 3*t2) + 
         Power(s1,6)*(s2 - t1)*
          (6*Power(s2,3) - 2*Power(t1,3) - Power(t1,2)*(-1 + t2) + 
            6*Power(-1 + t2,2) + Power(s2,2)*(-15 - 14*t1 + 7*t2) + 
            s2*(-5 + 10*Power(t1,2) + t1*(14 - 6*t2) + 7*t2 - 
               6*Power(t2,2)) + t1*(10 - 13*t2 + 7*Power(t2,2))) - 
         Power(s,7)*(-1 + 5*Power(t1,3) - 8*s2*t2 + Power(t2,2) - 
            7*s2*Power(t2,2) + Power(t2,3) + 
            Power(s1,2)*(-2 + s2 - 6*t1 + 7*t2) - 
            Power(t1,2)*(6 + 5*s2 + 9*t2) + 
            t1*(4 + 5*s2 + 5*t2 + 12*s2*t2 + 3*Power(t2,2)) + 
            s1*(3 + t1 - 2*Power(t1,2) - 6*t2 + 9*t1*t2 - 
               7*Power(t2,2) + s2*(-1 - 4*t1 + 7*t2))) + 
         Power(s1,5)*(4*Power(s2,5) + 2*Power(t1,5) + 
            3*Power(-1 + t2,3) - 2*Power(s2,4)*(2 + 5*t1 + 2*t2) - 
            2*Power(t1,4)*(-8 + 7*t2) + 
            Power(t1,2)*(2 + t2 + 5*Power(t2,2)) + 
            3*Power(t1,3)*(11 - 11*t2 + 6*Power(t2,2)) + 
            t1*(-9 + 28*t2 - 33*Power(t2,2) + 14*Power(t2,3)) + 
            Power(s2,3)*(-24 + 4*Power(t1,2) + 68*t2 - 30*Power(t2,2) + 
               t1*(-20 + 34*t2)) + 
            Power(s2,2)*(17 + 8*Power(t1,3) + 
               Power(t1,2)*(68 - 70*t2) - 34*t2 + 26*Power(t2,2) - 
               Power(t2,3) + t1*(83 - 173*t2 + 80*Power(t2,2))) + 
            s2*(4 - 8*Power(t1,4) - 17*t2 + 26*Power(t2,2) - 
               13*Power(t2,3) + 6*Power(t1,3)*(-10 + 9*t2) - 
               2*Power(t1,2)*(46 - 69*t2 + 34*Power(t2,2)) + 
               t1*(-15 + 25*t2 - 27*Power(t2,2) + Power(t2,3)))) + 
         Power(s1,4)*(-2*Power(s2,6) - 2*Power(t1,6) + 
            2*Power(s2,5)*(5 + 10*t1 - 9*t2) + 
            2*Power(t1,5)*(-13 + 7*t2) - Power(-1 + t2,3)*(-1 + 7*t2) + 
            Power(t1,4)*(26 + 19*t2 - 16*Power(t2,2)) + 
            Power(t1,3)*(121 - 205*t2 + 50*Power(t2,2)) - 
            Power(s2,4)*(26 + 62*Power(t1,2) + t1*(84 - 100*t2) - 
               75*t2 + 50*Power(t2,2)) - 
            2*t1*(7 - 5*t2 - 9*Power(t2,2) + 7*Power(t2,3)) - 
            2*Power(t1,2)*(-61 + 114*t2 - 87*Power(t2,2) + 
               28*Power(t2,3)) + 
            Power(s2,3)*(-77 + 88*Power(t1,3) + 
               Power(t1,2)*(218 - 206*t2) + 74*t2 - 44*Power(t2,2) + 
               Power(t2,3) + 2*t1*(21 - 103*t2 + 81*Power(t2,2))) + 
            Power(s2,2)*(125 - 62*Power(t1,4) - 258*t2 + 
               225*Power(t2,2) - 80*Power(t2,3) + 
               2*Power(t1,3)*(-125 + 99*t2) + 
               Power(t1,2)*(20 + 206*t2 - 190*Power(t2,2)) + 
               t1*(315 - 418*t2 + 164*Power(t2,2) - 3*Power(t2,3))) + 
            s2*(11 + 20*Power(t1,5) + 4*t2 - 38*Power(t2,2) + 
               24*Power(t2,3) - Power(t2,4) - 
               44*Power(t1,4)*(-3 + 2*t2) + 
               Power(t1,3)*(-62 - 94*t2 + 94*Power(t2,2)) + 
               Power(t1,2)*(-359 + 549*t2 - 170*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-265 + 520*t2 - 413*Power(t2,2) + 134*Power(t2,3)))) + 
         Power(s1,3)*(-2*Power(s2,7) + Power(s2,6)*(3 + 16*t1 - 11*t2) + 
            Power(t1,6)*(9 - 5*t2) + 
            Power(t1,4)*(-33 + 335*t2 - 90*Power(t2,2)) + 
            Power(t1,5)*(-104 + 29*t2 + 6*Power(t2,2)) + 
            Power(-1 + t2,2)*(-13 - 3*t2 + 8*Power(t2,2)) + 
            Power(t1,2)*(365 - 901*t2 + 594*Power(t2,2) - 
               66*Power(t2,3)) + 
            2*Power(t1,3)*(145 - 99*t2 - 96*Power(t2,2) + 
               32*Power(t2,3)) + 
            t1*(202 - 455*t2 + 450*Power(t2,2) - 255*Power(t2,3) + 
               58*Power(t2,4)) + 
            Power(s2,5)*(32 - 50*Power(t1,2) + 11*t2 - 36*Power(t2,2) + 
               4*t1*(-7 + 16*t2)) + 
            Power(s2,4)*(-25 + 80*Power(t1,3) + 
               Power(t1,2)*(91 - 151*t2) + 297*t2 - 138*Power(t2,2) + 
               8*Power(t2,3) + t1*(-282 + 47*t2 + 134*Power(t2,2))) - 
            Power(s2,3)*(100 + 70*Power(t1,4) + 43*t2 - 
               414*Power(t2,2) + 139*Power(t2,3) - 
               8*Power(t1,3)*(-18 + 23*t2) + 
               2*Power(t1,2)*(-379 + 118*t2 + 96*Power(t2,2)) + 
               2*t1*(-83 + 661*t2 - 259*Power(t2,2) + 12*Power(t2,3))) + 
            Power(s2,2)*(282 + 32*Power(t1,5) - 
               121*Power(t1,4)*(-1 + t2) - 702*t2 + 487*Power(t2,2) - 
               77*Power(t2,3) + 2*Power(t2,4) + 
               2*Power(t1,3)*(-451 + 158*t2 + 66*Power(t2,2)) + 
               Power(t1,2)*(-290 + 2088*t2 - 712*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(488 - 171*t2 - 944*Power(t2,2) + 327*Power(t2,3))) - 
            s2*(202 + 6*Power(t1,6) + Power(t1,5)*(52 - 40*t2) - 
               471*t2 + 490*Power(t2,2) - 287*Power(t2,3) + 
               66*Power(t2,4) + 
               Power(t1,4)*(-498 + 167*t2 + 44*Power(t2,2)) + 
               2*Power(t1,3)*
                (-91 + 699*t2 - 211*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(678 - 412*t2 - 722*Power(t2,2) + 
                  252*Power(t2,3)) + 
               2*t1*(329 - 834*t2 + 589*Power(t2,2) - 93*Power(t2,3) + 
                  Power(t2,4)))) - 
         Power(s1,2)*(Power(s2,7)*(-1 + t2) - Power(t1,7)*(5 + t2) + 
            Power(t1,5)*(95 - 46*t2 - 42*Power(t2,2)) + 
            Power(t1,6)*(4 + 28*t2 + Power(t2,2)) + 
            Power(s2,6)*(-5 + t1*(3 - 5*t2) + 6*t2 + 12*Power(t2,2)) + 
            Power(-1 + t2,2)*
             (-95 + 91*t2 - 72*Power(t2,2) + 20*Power(t2,3)) + 
            Power(t1,4)*(137 - 465*t2 + 198*Power(t2,2) + 
               24*Power(t2,3)) - 
            Power(t1,3)*(275 + 247*t2 - 776*Power(t2,2) + 
               240*Power(t2,3)) + 
            t1*(-363 + 1432*t2 - 1675*Power(t2,2) + 642*Power(t2,3) - 
               36*Power(t2,4)) + 
            Power(t1,2)*(-594 + 1246*t2 - 401*Power(t2,2) - 
               311*Power(t2,3) + 96*Power(t2,4)) + 
            Power(s2,5)*(5 - 114*t2 + 100*Power(t2,2) - 
               10*Power(t2,3) + Power(t1,2)*(5 + 11*t2) + 
               t1*(41 - 82*t2 - 49*Power(t2,2))) + 
            Power(s2,4)*(53 - 168*t2 - 56*Power(t2,2) + 
               87*Power(t2,3) - 5*Power(t1,3)*(7 + 3*t2) + 
               Power(t1,2)*(-110 + 296*t2 + 77*Power(t2,2)) + 
               t1*(81 + 374*t2 - 412*Power(t2,2) + 36*Power(t2,3))) + 
            Power(s2,3)*(18 + 611*t2 - 931*Power(t2,2) + 
               346*Power(t2,3) - 18*Power(t2,4) + 
               5*Power(t1,4)*(13 + 3*t2) - 
               2*Power(t1,3)*(-65 + 242*t2 + 29*Power(t2,2)) - 
               2*Power(t1,2)*
                (184 + 196*t2 - 339*Power(t2,2) + 24*Power(t2,3)) - 
               2*t1*(187 - 581*t2 + 92*Power(t2,2) + 127*Power(t2,3))) + 
            s2*(317 - 1286*t2 + 1553*Power(t2,2) - 652*Power(t2,3) + 
               68*Power(t2,4) + Power(t1,6)*(27 + 5*t2) - 
               5*Power(t1,5)*(-1 + 34*t2 + Power(t2,2)) + 
               Power(t1,4)*(-381 + 106*t2 + 238*Power(t2,2) - 
                  6*Power(t2,3)) - 
               2*Power(t1,3)*
                (271 - 878*t2 + 346*Power(t2,2) + 64*Power(t2,3)) + 
               t1*(855 - 1742*t2 + 201*Power(t2,2) + 830*Power(t2,3) - 
                  216*Power(t2,4)) + 
               Power(t1,2)*(487 + 1307*t2 - 2594*Power(t2,2) + 
                  814*Power(t2,3) - 16*Power(t2,4))) + 
            Power(s2,2)*(-292 + 581*t2 + 147*Power(t2,2) - 
               544*Power(t2,3) + 144*Power(t2,4) - 
               Power(t1,5)*(59 + 11*t2) + 
               Power(t1,4)*(-65 + 406*t2 + 22*Power(t2,2)) + 
               Power(t1,3)*(568 + 72*t2 - 562*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,2)*(726 - 2285*t2 + 734*Power(t2,2) + 
                  271*Power(t2,3)) + 
               t1*(-230 - 1671*t2 + 2749*Power(t2,2) - 920*Power(t2,3) + 
                  34*Power(t2,4)))) + 
         s1*(5*Power(t1,7) - Power(s2,7)*t2*(3 + 2*t2) - 
            Power(t1,6)*(-3 + 16*t2 + Power(t2,2)) + 
            Power(t1,5)*(-59 + t2 + 51*Power(t2,2) + 2*Power(t2,3)) - 
            2*Power(-1 + t2,2)*
             (-58 + 211*t2 - 112*Power(t2,2) + 3*Power(t2,3)) - 
            Power(t1,4)*(114 - 336*t2 + 97*Power(t2,2) + 
               67*Power(t2,3)) + 
            Power(t1,2)*(371 - 694*t2 - 108*Power(t2,2) + 
               729*Power(t2,3) - 266*Power(t2,4)) + 
            Power(t1,3)*(95 + 272*t2 - 776*Power(t2,2) + 
               303*Power(t2,3) + 30*Power(t2,4)) + 
            t1*(335 - 1245*t2 + 1531*Power(t2,2) - 559*Power(t2,3) - 
               126*Power(t2,4) + 64*Power(t2,5)) + 
            Power(s2,6)*(-2 + 11*t2 - 28*Power(t2,2) + 5*Power(t2,3) + 
               t1*(3 + 22*t2 + 10*Power(t2,2))) - 
            Power(s2,5)*(4 - 83*t2 + 62*Power(t2,2) + 12*Power(t2,3) + 
               5*Power(t1,2)*(4 + 13*t2 + 4*Power(t2,2)) + 
               t1*(3 + 7*t2 - 115*Power(t2,2) + 21*Power(t2,3))) + 
            Power(s2,4)*(1 - 95*t2 + 350*Power(t2,2) - 
               220*Power(t2,3) + 22*Power(t2,4) + 
               5*Power(t1,3)*(11 + 20*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(35 - 98*t2 - 181*Power(t2,2) + 
                  34*Power(t2,3)) + 
               t1*(-37 - 342*t2 + 319*Power(t2,2) + 35*Power(t2,3))) - 
            Power(s2,3)*(-18 + 376*t2 - 544*Power(t2,2) + 
               46*Power(t2,3) + 64*Power(t2,4) + 
               5*Power(t1,4)*(16 + 17*t2 + 2*Power(t2,2)) + 
               2*Power(t1,3)*
                (35 - 121*t2 - 67*Power(t2,2) + 13*Power(t2,3)) + 
               Power(t1,2)*(-194 - 527*t2 + 636*Power(t2,2) + 
                  35*Power(t2,3)) + 
               2*t1*(-73 + 97*t2 + 386*Power(t2,2) - 323*Power(t2,3) + 
                  29*Power(t2,4))) + 
            Power(s2,2)*(71 + 218*t2 - 1046*Power(t2,2) + 
               1109*Power(t2,3) - 332*Power(t2,4) + 12*Power(t2,5) + 
               Power(t1,5)*(65 + 38*t2 + 2*Power(t2,2)) + 
               Power(t1,4)*(60 - 233*t2 - 46*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(t1,3)*(-320 - 359*t2 + 614*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(t1,2)*(-409 + 1009*t2 + 397*Power(t2,2) - 
                  699*Power(t2,3) + 50*Power(t2,4)) + 
               t1*(25 + 1199*t2 - 2142*Power(t2,2) + 546*Power(t2,3) + 
                  144*Power(t2,4))) - 
            s2*(200 - 784*t2 + 904*Power(t2,2) - 154*Power(t2,3) - 
               224*Power(t2,4) + 58*Power(t2,5) + 
               7*Power(t1,6)*(4 + t2) + 
               Power(t1,5)*(23 - 101*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               Power(t1,4)*(-226 - 90*t2 + 286*Power(t2,2) + 
                  5*Power(t2,3)) + 
               2*Power(t1,3)*
                (-188 + 528*t2 - 61*Power(t2,2) - 170*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(t1,2)*(138 + 1095*t2 - 2374*Power(t2,2) + 
                  803*Power(t2,3) + 110*Power(t2,4)) + 
               t1*(437 - 423*t2 - 1275*Power(t2,2) + 1937*Power(t2,3) - 
                  620*Power(t2,4) + 8*Power(t2,5)))) + 
         Power(s,6)*(-2 + 11*t1 - 8*Power(t1,2) + 24*Power(t1,3) - 
            10*Power(t1,4) + 6*t2 - 24*t1*t2 - 36*Power(t1,2)*t2 + 
            15*Power(t1,3)*t2 + 6*Power(t2,2) + 21*t1*Power(t2,2) - 
            9*Power(t2,3) - 5*t1*Power(t2,3) + 
            5*Power(s1,3)*(-2 + s2 - 3*t1 + 4*t2) + 
            Power(s2,2)*(-10*Power(t1,2) + 10*t1*(1 + 3*t2) - 
               t2*(25 + 21*t2)) + 
            Power(s1,2)*(15 + 5*Power(s2,2) + t1 + 8*Power(t1,2) - 
               10*t2 + 9*t1*t2 - 23*Power(t2,2) + 
               s2*(-17 - 27*t1 + 39*t2)) + 
            s2*(-5 + 20*Power(t1,3) - 8*t2 + Power(t2,2) + 
               7*Power(t2,3) - Power(t1,2)*(34 + 45*t2) + 
               t1*(19 + 54*t2 + 18*Power(t2,2))) + 
            s1*(-3 + 21*Power(t1,3) - 13*t2 + 14*Power(t2,2) + 
               5*Power(t2,3) + Power(s2,2)*(-5 - 5*t1 + 21*t2) - 
               Power(t1,2)*(7 + 46*t2) + 
               t1*(1 + 24*t2 + 20*Power(t2,2)) + 
               s2*(17 - 16*Power(t1,2) - 30*t2 - 44*Power(t2,2) + 
                  t1*(5 + 41*t2)))) + 
         Power(s,5)*(2 + 3*t1 + 35*Power(t1,2) - 23*Power(t1,3) + 
            50*Power(t1,4) - 10*Power(t1,5) - 28*t2 + 18*t1*t2 - 
            19*Power(t1,2)*t2 - 88*Power(t1,3)*t2 + 10*Power(t1,4)*t2 + 
            24*Power(t2,2) - 44*t1*Power(t2,2) + 
            54*Power(t1,2)*Power(t2,2) + 10*Power(t1,3)*Power(t2,2) + 
            14*Power(t2,3) - 9*t1*Power(t2,3) - 
            10*Power(t1,2)*Power(t2,3) - 7*Power(t2,4) - 
            10*Power(s1,4)*(-2 + s2 - 2*t1 + 3*t2) + 
            Power(s1,3)*(-34 - 23*Power(s2,2) - 23*t1 - 
               35*Power(t1,2) + s2*(69 + 74*t1 - 91*t2) + 2*t2 + 
               19*t1*t2 + 42*Power(t2,2)) + 
            5*Power(s2,3)*(2*Power(t1,2) - 2*t1*(1 + 4*t2) + 
               t2*(8 + 7*t2)) + 
            Power(s2,2)*(6 - 30*Power(t1,3) + 46*t2 + 12*Power(t2,2) - 
               21*Power(t2,3) + 10*Power(t1,2)*(7 + 9*t2) - 
               t1*(36 + 172*t2 + 45*Power(t2,2))) + 
            Power(s1,2)*(11 - 10*Power(s2,3) - 26*Power(t1,3) + 
               Power(s2,2)*(50 + 44*t1 - 89*t2) + 62*t2 - 
               52*Power(t2,2) - 10*Power(t2,3) + 
               Power(t1,2)*(-29 + 93*t2) + 
               t1*(21 + t2 - 72*Power(t2,2)) + 
               s2*(-82 - 8*Power(t1,2) + t1*(2 - 38*t2) + 5*t2 + 
                  127*Power(t2,2))) + 
            s2*(30*Power(t1,4) - 10*Power(t1,3)*(11 + 6*t2) + 
               Power(t1,2)*(59 + 220*t2) + 
               3*(5 - 5*t2 - 8*Power(t2,2) + 14*Power(t2,3)) + 
               t1*(-61 + 26*t2 - 89*Power(t2,2) + 30*Power(t2,3))) + 
            s1*(1 + 44*Power(t1,4) + 14*t2 - 80*Power(t2,2) + 
               49*Power(t2,3) - 5*Power(s2,3)*(-2 + 7*t2) - 
               9*Power(t1,3)*(7 + 8*t2) + 
               Power(t1,2)*(-52 + 163*t2 + 7*Power(t2,2)) + 
               t1*(-50 + 120*t2 - 68*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s2,2)*(-33 + 44*Power(t1,2) + 56*t2 + 
                  117*Power(t2,2) - 10*t1*(2 + 7*t2)) + 
               s2*(8 - 88*Power(t1,3) + 63*t2 - 34*Power(t2,2) - 
                  30*Power(t2,3) + Power(t1,2)*(73 + 177*t2) - 
                  2*t1*(-18 + 99*t2 + 55*Power(t2,2))))) + 
         Power(s,4)*(-5 - 119*t1 - 97*Power(t1,2) + 29*Power(t1,3) - 
            72*Power(t1,4) + 51*Power(t1,5) - 5*Power(t1,6) + 88*t2 + 
            134*t1*t2 + 152*Power(t1,2)*t2 + 121*Power(t1,3)*t2 - 
            81*Power(t1,4)*t2 - 130*Power(t2,2) - 111*t1*Power(t2,2) - 
            230*Power(t1,2)*Power(t2,2) + 22*Power(t1,3)*Power(t2,2) + 
            15*Power(t1,4)*Power(t2,2) + 95*Power(t2,3) + 
            90*t1*Power(t2,3) + 30*Power(t1,2)*Power(t2,3) - 
            10*Power(t1,3)*Power(t2,3) - 19*Power(t2,4) - 
            22*t1*Power(t2,4) + 
            5*Power(s1,5)*(-4 + 2*s2 - 3*t1 + 5*t2) + 
            Power(s1,4)*(42 + 42*Power(s2,2) + 55*Power(t1,2) - 
               2*s2*(62 + 53*t1 - 57*t2) + t1*(67 - 56*t2) + 9*t2 - 
               43*Power(t2,2)) - 
            5*Power(s2,4)*(Power(t1,2) + 7*t2*(1 + t2) - 
               t1*(1 + 6*t2)) + 
            Power(s2,3)*(6 + 20*Power(t1,3) - 92*t2 - 40*Power(t2,2) + 
               35*Power(t2,3) - 6*Power(t1,2)*(11 + 15*t2) + 
               t1*(30 + 248*t2 + 60*Power(t2,2))) + 
            Power(s1,3)*(-16 + 40*Power(s2,3) - 6*Power(t1,3) + 
               Power(t1,2)*(61 - 89*t2) - 138*t2 + 99*Power(t2,2) + 
               8*Power(t2,3) + Power(s2,2)*(-150 - 130*t1 + 153*t2) + 
               2*s2*(78 + 48*Power(t1,2) + t1*(26 - 14*t2) + 61*t2 - 
                  102*Power(t2,2)) + 2*t1*(-18 - 66*t2 + 71*Power(t2,2))\
) + Power(s2,2)*(-60 - 30*Power(t1,4) - 29*t2 + 47*Power(t2,2) - 
               75*Power(t2,3) + 6*Power(t1,3)*(28 + 15*t2) - 
               4*Power(t1,2)*(33 + 118*t2) + 
               t1*(98 + 156*t2 + 154*Power(t2,2) - 75*Power(t2,3))) + 
            s2*(13 + 20*Power(t1,5) + 60*t2 - 35*Power(t2,2) - 
               90*Power(t2,3) + 35*Power(t2,4) - 
               2*Power(t1,4)*(79 + 15*t2) + 
               Power(t1,3)*(174 + 340*t2 - 40*Power(t2,2)) + 
               t1*(155 - 142*t2 + 241*Power(t2,2) + 24*Power(t2,3)) + 
               Power(t1,2)*(-133 - 185*t2 - 136*Power(t2,2) + 
                  50*Power(t2,3))) + 
            Power(s1,2)*(22 + 10*Power(s2,4) - 68*Power(t1,4) - 68*t2 + 
               230*Power(t2,2) - 105*Power(t2,3) + 
               Power(s2,3)*(-67 - 26*t1 + 105*t2) + 
               Power(t1,3)*(-11 + 155*t2) + 
               Power(t1,2)*(110 - 145*t2 - 71*Power(t2,2)) + 
               t1*(94 - 170*t2 + 66*Power(t2,2) - 36*Power(t2,3)) + 
               Power(s2,2)*(115 - 46*Power(t1,2) + 96*t2 - 
                  290*Power(t2,2) + t1*(-9 + 67*t2)) + 
               s2*(-26 + 130*Power(t1,3) + Power(t1,2)*(87 - 327*t2) - 
                  230*t2 + 128*Power(t2,2) + 50*Power(t2,3) + 
                  t1*(-139 + 32*t2 + 337*Power(t2,2)))) + 
            s1*(41*Power(t1,5) + 5*Power(s2,4)*(-2 + t1 + 7*t2) - 
               Power(t1,4)*(159 + 43*t2) + 
               Power(t1,3)*(-123 + 381*t2 - 32*Power(t2,2)) + 
               Power(t1,2)*(-26 + 245*t2 - 218*Power(t2,2) + 
                  34*Power(t2,3)) + 
               t1*(108 - 34*t2 - 51*Power(t2,2) + 89*Power(t2,3)) + 
               2*(-6 + 9*t2 - 23*Power(t2,2) - 28*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(s2,3)*(14 - 56*Power(t1,2) - 47*t2 - 
                  170*Power(t2,2) + t1*(46 + 50*t2)) + 
               Power(s2,2)*(78 + 138*Power(t1,3) - 127*t2 - 
                  32*Power(t2,2) + 75*Power(t2,3) - 
                  Power(t1,2)*(221 + 248*t2) + 
                  t1*(-81 + 504*t2 + 250*Power(t2,2))) + 
               s2*(-58 - 128*Power(t1,4) - 59*t2 + 338*Power(t2,2) - 
                  208*Power(t2,3) + Power(t1,3)*(344 + 206*t2) - 
                  2*Power(t1,2)*(-95 + 419*t2 + 24*Power(t2,2)) + 
                  t1*(1 - 293*t2 + 317*Power(t2,2) - 105*Power(t2,3))))) \
- Power(s,3)*(-8 - 182*t1 + 64*Power(t1,2) + 141*Power(t1,3) - 
            44*Power(t1,4) + 88*Power(t1,5) - 24*Power(t1,6) + 
            Power(t1,7) - 14*t2 + 129*t1*t2 - 47*Power(t1,2)*t2 - 
            100*Power(t1,3)*t2 - 184*Power(t1,4)*t2 + 
            21*Power(t1,5)*t2 + 3*Power(t1,6)*t2 - 41*Power(t2,2) - 
            35*t1*Power(t2,2) - 25*Power(t1,2)*Power(t2,2) + 
            258*Power(t1,3)*Power(t2,2) + 33*Power(t1,4)*Power(t2,2) - 
            9*Power(t1,5)*Power(t2,2) + 103*Power(t2,3) + 
            66*t1*Power(t2,3) - 74*Power(t1,2)*Power(t2,3) - 
            54*Power(t1,3)*Power(t2,3) + 5*Power(t1,4)*Power(t2,3) - 
            92*Power(t2,4) + 24*Power(t1,2)*Power(t2,4) + 
            12*Power(t2,5) + Power(s1,6)*(-10 + 5*s2 - 6*t1 + 11*t2) + 
            Power(s2,5)*(t1 - Power(t1,2) + 12*t1*t2 - 
               t2*(16 + 21*t2)) + 
            Power(s1,5)*(27 + 38*Power(s2,2) + 44*Power(t1,2) + 
               t1*(79 - 57*t2) + 8*t2 - 23*Power(t2,2) + 
               s2*(-113 - 84*t1 + 81*t2)) + 
            Power(s1,4)*(3 + 64*Power(s2,3) - 41*Power(t1,3) + 
               Power(t1,2)*(22 - 29*t2) - 156*t2 + 98*Power(t2,2) + 
               Power(t2,3) + Power(s2,2)*(-217 - 186*t1 + 144*t2) + 
               s2*(158 + 163*Power(t1,2) + t1*(167 - 96*t2) + 190*t2 - 
                  179*Power(t2,2)) + t1*(-36 - 215*t2 + 145*Power(t2,2))\
) + Power(s2,4)*(5*Power(t1,3) - Power(t1,2)*(28 + 45*t2) + 
               t1*(8 + 177*t2 + 45*Power(t2,2)) + 
               5*(3 - 16*t2 - 11*Power(t2,2) + 7*Power(t2,3))) - 
            2*Power(s2,3)*(21 + 5*Power(t1,4) + 66*t2 - 
               22*Power(t2,2) + 30*Power(t2,3) - 
               3*Power(t1,3)*(17 + 10*t2) + 
               4*Power(t1,2)*(14 + 57*t2) + 
               t1*(-15 - 176*t2 - 73*Power(t2,2) + 50*Power(t2,3))) + 
            Power(s2,2)*(-90 + 10*Power(t1,5) + 70*t2 + 
               48*Power(t2,2) - 220*Power(t2,3) + 70*Power(t2,4) - 
               2*Power(t1,4)*(74 + 15*t2) + 
               Power(t1,3)*(288 + 466*t2 - 60*Power(t2,2)) + 
               t1*(330 - 98*t2 + 396*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(-149 - 648*t2 - 94*Power(t2,2) + 
                  100*Power(t2,3))) + 
            s2*(100 + 97*Power(t1,5) - 5*Power(t1,6) + 204*t2 - 
               395*Power(t2,2) + 218*Power(t2,3) - 52*Power(t2,4) + 
               Power(t1,4)*(-272 - 192*t2 + 45*Power(t2,2)) + 
               Power(t1,3)*(148 + 560*t2 - 30*Power(t2,2) - 
                  40*Power(t2,3)) + 
               Power(t1,2)*(-429 + 330*t2 - 698*Power(t2,2) + 
                  108*Power(t2,3)) + 
               t1*(-11 + 90*t2 - 116*Power(t2,2) + 292*Power(t2,3) - 
                  88*Power(t2,4))) + 
            Power(s1,3)*(20 + 30*Power(s2,4) - 42*Power(t1,4) - 82*t2 + 
               301*Power(t2,2) - 119*Power(t2,3) + 
               2*Power(t1,3)*(-82 + 93*t2) + 
               Power(s2,3)*(-125 - 80*t1 + 102*t2) - 
               4*Power(t1,2)*(14 - 48*t2 + 45*Power(t2,2)) + 
               t1*(58 + 42*t2 - 31*Power(t2,2) - 24*Power(t2,3)) + 
               Power(s2,2)*(119 + 28*Power(t1,2) + 429*t2 - 
                  396*Power(t2,2) + t1*(-73 + 94*t2)) + 
               s2*(-44 + 64*Power(t1,3) + Power(t1,2)*(362 - 382*t2) - 
                  418*t2 + 213*Power(t2,2) + 32*Power(t2,3) + 
                  8*t1*(2 - 79*t2 + 70*Power(t2,2)))) + 
            Power(s1,2)*(23 + 5*Power(s2,5) + 58*Power(t1,5) - 27*t2 - 
               177*Power(t2,2) - 8*Power(t2,3) + 18*Power(t2,4) + 
               Power(s2,4)*(-40 + 6*t1 + 65*t2) - 
               Power(t1,4)*(89 + 103*t2) + 
               2*Power(t1,3)*(-185 + 220*t2 + 6*Power(t2,2)) - 
               2*Power(s2,3)*
                (-12 + 53*Power(t1,2) + t1*(7 - 34*t2) - 106*t2 + 
                  175*Power(t2,2)) + 
               Power(t1,2)*(-172 + 916*t2 - 387*Power(t2,2) + 
                  48*Power(t2,3)) + 
               4*t1*(13 + 104*t2 - 172*Power(t2,2) + 62*Power(t2,3)) + 
               Power(s2,2)*(125 + 232*Power(t1,3) + 
                  Power(t1,2)*(59 - 434*t2) - 122*t2 - 
                  28*Power(t2,2) + 100*Power(t2,3) + 
                  t1*(-286 + 98*t2 + 628*Power(t2,2))) + 
               s2*(31 - 195*Power(t1,4) - 410*t2 + 1006*Power(t2,2) - 
                  402*Power(t2,3) + Power(t1,3)*(84 + 404*t2) + 
                  Power(t1,2)*(632 - 750*t2 - 290*Power(t2,2)) - 
                  2*t1*(-49 + 503*t2 - 245*Power(t2,2) + 72*Power(t2,3))\
)) + s1*(-58 - 18*Power(t1,6) + 397*t2 - 541*Power(t2,2) + 
               402*Power(t2,3) - 68*Power(t2,4) + 
               Power(t1,5)*(162 + t2) + 
               Power(s2,5)*(-5 + 4*t1 + 21*t2) + 
               Power(t1,4)*(-2 - 347*t2 + 43*Power(t2,2)) + 
               Power(t1,3)*(-198 + 166*t2 + 155*Power(t2,2) - 
                  26*Power(t2,3)) - 
               Power(t1,2)*(410 - 556*t2 + 489*Power(t2,2) + 
                  7*Power(t2,3)) + 
               t1*(-96 + 481*t2 - 636*Power(t2,2) + 348*Power(t2,3) - 
                  58*Power(t2,4)) - 
               Power(s2,4)*(27 + 34*Power(t1,2) + 18*t2 + 
                  145*Power(t2,2) - t1*(53 + 5*t2)) + 
               Power(s2,3)*(96*Power(t1,3) - 
                  Power(t1,2)*(291 + 142*t2) + 
                  2*t1*(6 + 293*t2 + 150*Power(t2,2)) + 
                  2*(78 - 39*t2 - 94*Power(t2,2) + 50*Power(t2,3))) - 
               Power(s2,2)*(66 + 124*Power(t1,4) + 240*t2 - 
                  568*Power(t2,2) + 342*Power(t2,3) - 
                  Power(t1,3)*(605 + 186*t2) + 
                  Power(t1,2)*(-55 + 1465*t2 + 122*Power(t2,2)) + 
                  2*t1*(153 + 50*t2 - 329*Power(t2,2) + 105*Power(t2,3))\
) + s2*(-98 + 76*Power(t1,5) - 141*t2 + 250*Power(t2,2) - 
                  324*Power(t2,3) + 88*Power(t2,4) - 
                  Power(t1,4)*(524 + 71*t2) - 
                  2*Power(t1,3)*(19 - 622*t2 + 38*Power(t2,2)) + 
                  Power(t1,2)*
                   (348 + 12*t2 - 625*Power(t2,2) + 136*Power(t2,3)) + 
                  t1*(446 - 324*t2 + 60*Power(t2,2) + 302*Power(t2,3))))) \
+ s*(-59 - 273*t1 - 386*Power(t1,2) + 9*Power(t1,3) + 105*Power(t1,4) - 
            43*Power(t1,5) + 28*Power(t1,6) - 5*Power(t1,7) + 540*t2 + 
            980*t1*t2 + 591*Power(t1,2)*t2 - 154*Power(t1,3)*t2 - 
            38*Power(t1,4)*t2 - 48*Power(t1,5)*t2 - Power(t1,6)*t2 + 
            2*Power(t1,7)*t2 - 1258*Power(t2,2) + 
            Power(s2,7)*Power(t2,2) - 1076*t1*Power(t2,2) - 
            115*Power(t1,2)*Power(t2,2) + 68*Power(t1,3)*Power(t2,2) + 
            115*Power(t1,4)*Power(t2,2) + 6*Power(t1,5)*Power(t2,2) - 
            4*Power(t1,6)*Power(t2,2) + 1148*Power(t2,3) + 
            445*t1*Power(t2,3) - 50*Power(t1,2)*Power(t2,3) - 
            106*Power(t1,3)*Power(t2,3) - 24*Power(t1,4)*Power(t2,3) + 
            3*Power(t1,5)*Power(t2,3) - 459*Power(t2,4) - 
            94*t1*Power(t2,4) + 68*Power(t1,2)*Power(t2,4) + 
            16*Power(t1,3)*Power(t2,4) - Power(t1,4)*Power(t2,4) + 
            88*Power(t2,5) - 10*t1*Power(t2,5) - 
            8*Power(t1,2)*Power(t2,5) - 
            Power(s1,7)*(s2 - t1)*(-9 + 3*s2 - 3*t1 + 5*t2) + 
            Power(s2,6)*t2*(2 + 14*t2 - 7*Power(t2,2) - 3*t1*(2 + t2)) + 
            Power(s1,6)*(-9 - 18*Power(s2,3) + 14*Power(t1,3) + 
               Power(s2,2)*(53 + 50*t1 - 27*t2) + 14*t2 - 
               5*Power(t2,2) - 3*Power(t1,2)*(-7 + 5*t2) + 
               t1*(11 + 25*t2 - 12*Power(t2,2)) - 
               s2*(24 + 46*Power(t1,2) + t1*(74 - 42*t2) + 11*t2 - 
                  11*Power(t2,2))) + 
            Power(s2,5)*(-9 + 31*t2 + 28*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,2)*(5 + 28*t2) + 
               t1*(5 - 66*t2 - 37*Power(t2,2) + 30*Power(t2,3))) + 
            Power(s2,4)*(-10 + 146*t2 - 144*Power(t2,2) + 
               150*Power(t2,3) - 35*Power(t2,4) + 
               5*Power(t1,3)*(-5 - 10*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(8 + 243*t2 + 4*Power(t2,2) - 
                  50*Power(t2,3)) + 
               t1*(25 - 178*t2 - 144*Power(t2,2) + 39*Power(t2,3))) + 
            Power(s2,2)*(-13 - 380*t2 + 906*Power(t2,2) - 
               717*Power(t2,3) + 332*Power(t2,4) - 36*Power(t2,5) + 
               Power(t1,5)*(-50 - 10*t2 + 9*Power(t2,2)) + 
               Power(t1,4)*(148 + 228*t2 - 86*Power(t2,2) - 
                  15*Power(t2,3)) + 
               2*Power(t1,3)*
                (-63 - 215*t2 - 95*Power(t2,2) + 39*Power(t2,3)) + 
               Power(t1,2)*(439 - 264*t2 + 531*Power(t2,2) + 
                  158*Power(t2,3) - 72*Power(t2,4)) + 
               t1*(-146 - 104*t2 + 235*Power(t2,2) - 446*Power(t2,3) + 
                  24*Power(t2,4))) + 
            Power(s2,3)*(58 - 30*t2 + 5*Power(t2,2) + 98*Power(t2,3) + 
               4*Power(t2,4) + 
               Power(t1,4)*(50 + 40*t2 - 15*Power(t2,2)) + 
               Power(t1,2)*(22 + 396*t2 + 258*Power(t2,2) - 
                  84*Power(t2,3)) + 
               Power(t1,3)*(-82 - 352*t2 + 74*Power(t2,2) + 
                  40*Power(t2,3)) + 
               2*t1*(-76 - 53*t2 + 4*Power(t2,2) - 158*Power(t2,3) + 
                  44*Power(t2,4))) + 
            s2*(33 - 213*t2 + 200*Power(t2,2) + 32*Power(t2,3) - 
               52*Power(t2,4) + 28*Power(t2,5) + 
               Power(t1,6)*(25 - 4*t2 - 2*Power(t2,2)) + 
               Power(t1,4)*(131 + 229*t2 + 42*Power(t2,2) - 
                  30*Power(t2,3)) + 
               Power(t1,5)*(-107 - 54*t2 + 35*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,2)*(79 + 288*t2 - 308*Power(t2,2) + 
                  454*Power(t2,3) - 44*Power(t2,4)) + 
               Power(t1,3)*(-382 + 262*t2 - 510*Power(t2,2) + 
                  32*Power(t2,3) + 20*Power(t2,4)) + 
               t1*(445 - 364*t2 - 613*Power(t2,2) + 674*Power(t2,3) - 
                  374*Power(t2,4) + 40*Power(t2,5))) + 
            Power(s1,5)*(-26*Power(s2,4) - 6*Power(t1,4) + 
               Power(t1,3)*(39 - 30*t2) + 
               Power(s2,3)*(73 + 80*t1 - 23*t2) + 
               Power(t1,2)*(100 - 137*t2 + 65*Power(t2,2)) - 
               t1*(-48 + 146*t2 - 43*Power(t2,2) + Power(t2,3)) + 
               3*(9 - 2*t2 - 11*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(2 - 88*Power(t1,2) - 157*t2 + 
                  83*Power(t2,2) + t1*(-79 + 4*t2)) + 
               s2*(-22 + 40*Power(t1,3) + 147*t2 - 71*Power(t2,2) + 
                  2*Power(t2,3) + Power(t1,2)*(-33 + 49*t2) - 
                  2*t1*(56 - 153*t2 + 75*Power(t2,2)))) + 
            Power(s1,4)*(-31 - 6*Power(s2,5) - 12*Power(t1,5) - 94*t2 + 
               154*Power(t2,2) - 30*Power(t2,3) + Power(t2,4) + 
               Power(s2,4)*(3 - 6*t1 + 30*t2) + 
               Power(t1,4)*(-111 + 76*t2) + 
               Power(t1,3)*(-47 + 152*t2 - 88*Power(t2,2)) + 
               Power(s2,3)*(80 + 66*Power(t1,2) + t1*(194 - 216*t2) - 
                  346*t2 + 193*Power(t2,2)) + 
               t1*(133 - 406*t2 + 480*Power(t2,2) - 123*Power(t2,3)) + 
               Power(t1,2)*(121 - 347*t2 + 58*Power(t2,2) - 
                  2*Power(t2,3)) - 
               Power(s2,2)*(-2 + 102*Power(t1,3) + 
                  Power(t1,2)*(508 - 418*t2) - 112*t2 + 
                  41*Power(t2,2) + 3*Power(t2,3) + 
                  t1*(252 - 820*t2 + 469*Power(t2,2))) + 
               s2*(-111 + 60*Power(t1,4) + Power(t1,3)*(422 - 308*t2) + 
                  297*t2 - 416*Power(t2,2) + 146*Power(t2,3) + 
                  Power(t1,2)*(219 - 626*t2 + 364*Power(t2,2)) + 
                  t1*(-129 + 272*t2 - 49*Power(t2,2) + 6*Power(t2,3)))) \
+ Power(s1,3)*(127 + 5*Power(s2,6) + 9*Power(t1,6) + 
               Power(t1,5)*(42 - 41*t2) - 211*t2 + 379*Power(t2,2) - 
               353*Power(t2,3) + 58*Power(t2,4) + 
               Power(s2,5)*(-14 - 50*t1 + 33*t2) + 
               Power(t1,4)*(-307 + 101*t2 + 34*Power(t2,2)) + 
               Power(s2,4)*(-19 + 159*Power(t1,2) + 
                  t1*(176 - 221*t2) - 201*t2 + 186*Power(t2,2)) + 
               2*Power(t1,3)*
                (-197 + 547*t2 - 154*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(-5 + 588*t2 - 748*Power(t2,2) + 
                  230*Power(t2,3)) + 
               t1*(228 - 669*t2 + 447*Power(t2,2) - 76*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,3)*(105 - 236*Power(t1,3) - 572*t2 + 
                  261*Power(t2,2) - 32*Power(t2,3) + 
                  Power(t1,2)*(-486 + 506*t2) + 
                  t1*(418 + 344*t2 - 544*Power(t2,2))) + 
               Power(s2,2)*(-68 + 179*Power(t1,4) + 
                  Power(t1,3)*(542 - 522*t2) + 729*t2 - 
                  1305*Power(t2,2) + 397*Power(t2,3) + 
                  2*Power(t1,2)*(-543 + 8*t2 + 282*Power(t2,2)) + 
                  t1*(-779 + 2582*t2 - 903*Power(t2,2) + 
                     72*Power(t2,3))) - 
               s2*(210 + 66*Power(t1,5) - 555*t2 + 251*Power(t2,2) + 
                  22*Power(t2,3) + 4*Power(t2,4) - 
                  5*Power(t1,4)*(-52 + 49*t2) + 
                  Power(t1,3)*(-994 + 260*t2 + 240*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (-534 + 1552*t2 - 475*Power(t2,2) + 24*Power(t2,3)) \
+ 2*t1*(-70 + 726*t2 - 1053*Power(t2,2) + 306*Power(t2,3)))) + 
            Power(s1,2)*(135 - 2*Power(t1,7) - 664*t2 + 
               597*Power(t2,2) - 114*Power(t2,3) + 46*Power(t2,4) + 
               3*Power(t1,6)*(14 + t2) + Power(s2,6)*(-4 - 4*t1 + t2) + 
               Power(t1,5)*(69 - 171*t2 + 4*Power(t2,2)) + 
               Power(s2,5)*(6 + 22*Power(t1,2) + t1*(12 - 22*t2) - 
                  33*t2 + 83*Power(t2,2)) - 
               3*Power(t1,4)*
                (87 + 26*t2 - 65*Power(t2,2) + 2*Power(t2,3)) - 
               2*Power(t1,3)*
                (370 - 594*t2 + 186*Power(t2,2) + 47*Power(t2,3)) + 
               t1*(-31 + 693*t2 - 1410*Power(t2,2) + 916*Power(t2,3) - 
                  184*Power(t2,4)) + 
               Power(t1,2)*(-440 + 2157*t2 - 2133*Power(t2,2) + 
                  540*Power(t2,3) - 16*Power(t2,4)) - 
               Power(s2,4)*(10 + 50*Power(t1,3) + 420*t2 - 
                  328*Power(t2,2) + 50*Power(t2,3) - 
                  Power(t1,2)*(34 + 81*t2) + 
                  t1*(-161 + 195*t2 + 268*Power(t2,2))) + 
               2*Power(s2,3)*
                (34 + 30*Power(t1,4) + 87*t2 - 377*Power(t2,2) + 
                  183*Power(t2,3) - 2*Power(t1,3)*(44 + 31*t2) + 
                  Power(t1,2)*(-294 + 477*t2 + 151*Power(t2,2)) + 
                  t1*(58 + 769*t2 - 591*Power(t2,2) + 72*Power(t2,3))) \
- Power(s2,2)*(308 + 40*Power(t1,5) - 1649*t2 + 1595*Power(t2,2) - 
                  612*Power(t2,3) + 54*Power(t2,4) - 
                  Power(t1,4)*(264 + 91*t2) + 
                  2*Power(t1,3)*(-369 + 681*t2 + 64*Power(t2,2)) + 
                  Power(t1,2)*
                   (463 + 1894*t2 - 1575*Power(t2,2) + 
                     144*Power(t2,3)) + 
                  2*t1*(415 - 502*t2 - 396*Power(t2,2) + 
                     378*Power(t2,3))) + 
               s2*(188 + 14*Power(t1,6) - 983*t2 + 1711*Power(t2,2) - 
                  1160*Power(t2,3) + 260*Power(t2,4) - 
                  2*Power(t1,5)*(86 + 15*t2) + 
                  Power(t1,4)*(-386 + 807*t2 + 7*Power(t2,2)) + 
                  Power(t1,3)*
                   (618 + 854*t2 - 916*Power(t2,2) + 56*Power(t2,3)) + 
                  2*Power(t1,2)*
                   (751 - 1183*t2 + 167*Power(t2,2) + 242*Power(t2,3)) \
+ 2*t1*(372 - 1951*t2 + 1957*Power(t2,2) - 618*Power(t2,3) + 
                     34*Power(t2,4)))) + 
            s1*(-174 + 2*Power(t1,7)*(-5 + t2) + 663*t2 - 
               Power(s2,7)*t2 - 786*Power(t2,2) + 581*Power(t2,3) - 
               326*Power(t2,4) + 42*Power(t2,5) + 
               Power(t1,6)*(57 + 8*t2 - 3*Power(t2,2)) + 
               Power(t1,5)*(81 - 164*t2 + 19*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(t1,4)*(72 + 215*t2 - 265*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(t1,3)*(-528 + 642*t2 + 155*Power(t2,2) - 
                  218*Power(t2,3) + 14*Power(t2,4)) + 
               Power(t1,2)*(-603 + 1644*t2 - 1582*Power(t2,2) + 
                  337*Power(t2,3) + 70*Power(t2,4)) + 
               t1*(-335 + 1548*t2 - 2520*Power(t2,2) + 
                  1755*Power(t2,3) - 392*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,6)*(9 + 8*t2 + 19*Power(t2,2) + 
                  t1*(-6 + 4*t2)) - 
               Power(s2,5)*(-14 + 61*t2 - 134*Power(t2,2) + 
                  30*Power(t2,3) + Power(t1,2)*(-40 + 7*t2) + 
                  10*t1*(6 + 12*t2 + 7*Power(t2,2))) + 
               Power(s2,4)*(-69 + 10*Power(t1,3)*(-11 + t2) - 20*t2 - 
                  8*Power(t2,2) + 97*Power(t2,3) + 
                  3*Power(t1,2)*(69 + 136*t2 + 29*Power(t2,2)) + 
                  t1*(54 - 8*t2 - 458*Power(t2,2) + 105*Power(t2,3))) - 
               Power(s2,3)*(-37 - 527*t2 + 1158*Power(t2,2) - 
                  652*Power(t2,3) + 88*Power(t2,4) + 
                  5*Power(t1,4)*(-32 + 3*t2) + 
                  4*Power(t1,3)*(102 + 148*t2 + 7*Power(t2,2)) + 
                  2*t1*(-59 - 335*t2 + 272*Power(t2,2) + 
                     97*Power(t2,3)) + 
                  Power(t1,2)*
                   (327 - 554*t2 - 551*Power(t2,2) + 136*Power(t2,3))) - 
               s2*(-259 + 1471*t2 - 2460*Power(t2,2) + 
                  1718*Power(t2,3) - 430*Power(t2,4) + 24*Power(t2,5) + 
                  Power(t1,6)*(-56 + 9*t2) - 
                  6*Power(t1,5)*(-42 - 20*t2 + 3*Power(t2,2)) + 
                  Power(t1,4)*
                   (339 - 683*t2 + Power(t2,2) + 18*Power(t2,3)) - 
                  2*Power(t1,3)*
                   (62 + 530*t2 - 545*Power(t2,2) + 20*Power(t2,3)) + 
                  Power(t1,2)*
                   (-988 + 487*t2 + 1559*Power(t2,2) - 
                     982*Power(t2,3) + 100*Power(t2,4)) + 
                  2*t1*(-330 + 924*t2 - 777*Power(t2,2) - 
                     85*Power(t2,3) + 116*Power(t2,4))) + 
               Power(s2,2)*(-76 + 271*t2 + 9*Power(t2,2) - 
                  570*Power(t2,3) + 196*Power(t2,4) + 
                  2*Power(t1,5)*(-65 + 8*t2) + 
                  Power(t1,4)*(447 + 408*t2 - 23*Power(t2,2)) + 
                  Power(t1,2)*
                   (-101 - 1495*t2 + 1377*Power(t2,2) + 77*Power(t2,3)) \
+ Power(t1,3)*(517 - 1004*t2 - 245*Power(t2,2) + 78*Power(t2,3)) + 
                  t1*(-497 - 682*t2 + 2562*Power(t2,2) - 
                     1416*Power(t2,3) + 174*Power(t2,4))))) + 
         Power(s,2)*(25 + 74*t1 + 398*Power(t1,2) + 156*Power(t1,3) - 
            75*Power(t1,4) + 74*Power(t1,5) - 40*Power(t1,6) + 
            4*Power(t1,7) - 384*t2 - 474*t1*t2 - 600*Power(t1,2)*t2 - 
            195*Power(t1,3)*t2 - 128*Power(t1,4)*t2 + 68*Power(t1,5)*t2 + 
            6*Power(t1,6)*t2 - Power(t1,7)*t2 + 
            Power(s2,6)*(-3 + 2*t1 - 7*t2)*t2 + 716*Power(t2,2) + 
            671*t1*Power(t2,2) + 413*Power(t1,2)*Power(t2,2) + 
            345*Power(t1,3)*Power(t2,2) - 72*Power(t1,4)*Power(t2,2) - 
            27*Power(t1,5)*Power(t2,2) + 2*Power(t1,6)*Power(t2,2) - 
            477*Power(t2,3) - 433*t1*Power(t2,3) - 
            324*Power(t1,2)*Power(t2,3) - 26*Power(t1,3)*Power(t2,3) + 
            27*Power(t1,4)*Power(t2,3) - Power(t1,5)*Power(t2,3) + 
            142*Power(t2,4) + 170*t1*Power(t2,4) + 
            36*Power(t1,2)*Power(t2,4) - 10*Power(t1,3)*Power(t2,4) - 
            6*Power(t2,5) - 20*t1*Power(t2,5) + 
            Power(s1,7)*(-2 + s2 - t1 + 2*t2) + 
            Power(s1,6)*(7 + 17*Power(s2,2) + 18*Power(t1,2) + 
               t1*(43 - 27*t2) + 2*t2 - 5*Power(t2,2) + 
               s2*(-51 - 35*t1 + 31*t2)) - 
            Power(s2,5)*(-7 + 28*t2 + 39*Power(t2,2) - 21*Power(t2,3) + 
               Power(t1,2)*(4 + 9*t2) + t1*(1 - 58*t2 - 18*Power(t2,2))) \
+ Power(s1,5)*(19 + 50*Power(s2,3) - 37*Power(t1,3) - 81*t2 + 
               43*Power(t2,2) - Power(t2,3) + 
               Power(t1,2)*(-27 + 16*t2) + 
               Power(s2,2)*(-162 - 139*t1 + 83*t2) + 
               s2*(91 + 126*Power(t1,2) + t1*(181 - 95*t2) + 96*t2 - 
                  76*Power(t2,2)) + t1*(-29 - 128*t2 + 70*Power(t2,2))) + 
            Power(s2,4)*(22 - 128*t2 - 15*Power(t2,3) + 
               5*Power(t1,3)*(4 + 3*t2) - 4*Power(t1,2)*(9 + 49*t2) + 
               t1*(-23 + 256*t2 + 89*Power(t2,2) - 75*Power(t2,3))) - 
            Power(s2,3)*(108 + 58*t2 - 186*Power(t2,2) + 
               260*Power(t2,3) - 70*Power(t2,4) + 
               10*Power(t1,4)*(4 + t2) + 
               2*Power(t1,3)*(-77 - 132*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(47 + 668*t2 + 6*Power(t2,2) - 
                  100*Power(t2,3)) + 
               2*t1*(-54 - 106*t2 - 147*Power(t2,2) + 18*Power(t2,3))) + 
            Power(s2,2)*(-28 + 40*Power(t1,5) + 249*t2 - 
               363*Power(t2,2) + 136*Power(t2,3) - 42*Power(t2,4) + 
               Power(t1,4)*(-236 - 151*t2 + 45*Power(t2,2)) + 
               Power(t1,3)*(209 + 748*t2 - 126*Power(t2,2) - 
                  60*Power(t2,3)) + 
               3*Power(t1,2)*
                (-119 - 56*t2 - 220*Power(t2,2) + 48*Power(t2,3)) + 
               t1*(444 - 196*t2 + 66*Power(t2,2) + 416*Power(t2,3) - 
                  132*Power(t2,4))) + 
            s2*(82 + 252*t2 - 695*Power(t2,2) + 589*Power(t2,3) - 
               272*Power(t2,4) + 36*Power(t2,5) + 
               Power(t1,6)*(-20 + 3*t2) + 
               Power(t1,5)*(159 + 22*t2 - 18*Power(t2,2)) + 
               Power(t1,3)*(302 + 212*t2 + 438*Power(t2,2) - 
                  120*Power(t2,3)) + 
               Power(t1,4)*(-220 - 376*t2 + 109*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(-427 + 534*t2 - 278*Power(t2,2) + 312*Power(t2,3) - 
                  12*Power(t2,4)) + 
               Power(t1,2)*(-492 + 449*t2 - 597*Power(t2,2) - 
                  130*Power(t2,3) + 72*Power(t2,4))) + 
            Power(s1,4)*(40*Power(s2,4) - 4*Power(t1,4) - 
               6*Power(s2,3)*(21 + 19*t1 - 8*t2) + 
               Power(t1,3)*(-152 + 117*t2) - 
               2*Power(t1,2)*(98 - 167*t2 + 87*Power(t2,2)) - 
               t1*(39 - 250*t2 + 87*Power(t2,2) + 3*Power(t2,3)) - 
               2*(14 + 14*t2 - 89*Power(t2,2) + 33*Power(t2,3)) + 
               Power(s2,2)*(40 + 104*Power(t1,2) + 452*t2 - 
                  279*Power(t2,2) + t1*(-4 + 76*t2)) + 
               s2*(-5 - 26*Power(t1,3) + Power(t1,2)*(282 - 241*t2) - 
                  378*t2 + 183*Power(t2,2) + 3*Power(t2,3) + 
                  t1*(197 - 806*t2 + 452*Power(t2,2)))) + 
            Power(s1,3)*(103 + 5*Power(s2,5) + 37*Power(t1,5) + 
               Power(t1,4)*(123 - 134*t2) + 
               Power(s2,4)*(-23 + 25*t1 - 2*t2) + 12*t2 - 
               296*Power(t2,2) + 75*Power(t2,3) + 2*Power(t2,4) + 
               2*Power(t1,3)*(-115 + 7*t2 + 54*Power(t2,2)) - 
               Power(s2,3)*(16 + 142*Power(t1,2) + t1*(250 - 260*t2) - 
                  495*t2 + 384*Power(t2,2)) + 
               2*Power(t1,2)*
                (-166 + 555*t2 - 143*Power(t2,2) + 12*Power(t2,3)) + 
               t1*(-147 + 840*t2 - 1112*Power(t2,2) + 285*Power(t2,3)) + 
               Power(s2,2)*(-12 + 226*Power(t1,3) + 
                  Power(t1,2)*(692 - 648*t2) - 13*t2 - 9*Power(t2,2) + 
                  48*Power(t2,3) + t1*(-80 - 891*t2 + 828*Power(t2,2))) \
+ s2*(145 - 151*Power(t1,4) - 658*t2 + 1168*Power(t2,2) - 
                  377*Power(t2,3) + Power(t1,3)*(-542 + 524*t2) + 
                  Power(t1,2)*(326 + 382*t2 - 552*Power(t2,2)) + 
                  t1*(364 - 1216*t2 + 354*Power(t2,2) - 72*Power(t2,3)))) \
+ Power(s1,2)*(-239 + Power(s2,6) - 20*Power(t1,6) + 639*t2 - 
               808*Power(t2,2) + 640*Power(t2,3) - 116*Power(t2,4) + 
               Power(s2,5)*(-5 + 13*t1 + 17*t2) + 
               Power(t1,5)*(109 + 27*t2) + 
               Power(t1,4)*(311 - 427*t2 + 15*Power(t2,2)) + 
               Power(t1,2)*(-394 + 100*t2 + 116*Power(t2,2) - 
                  213*Power(t2,3)) + 
               Power(t1,3)*(38 - 696*t2 + 422*Power(t2,2) - 
                  28*Power(t2,3)) + 
               t1*(-261 + 1067*t2 - 1025*Power(t2,2) + 
                  420*Power(t2,3) - 34*Power(t2,4)) - 
               Power(s2,4)*(82*Power(t1,2) + t1*(17 - 47*t2) + 
                  5*(7 - 32*t2 + 47*Power(t2,2))) + 
               Power(s2,3)*(125 + 178*Power(t1,3) + 352*t2 - 
                  332*Power(t2,2) + 100*Power(t2,3) - 
                  2*Power(t1,2)*(14 + 135*t2) + 
                  2*t1*(-140 + 89*t2 + 291*Power(t2,2))) + 
               Power(s2,2)*(10 - 187*Power(t1,4) - 736*t2 + 
                  1474*Power(t2,2) - 576*Power(t2,3) + 
                  Power(t1,3)*(236 + 358*t2) + 
                  Power(t1,2)*(976 - 1263*t2 - 444*Power(t2,2)) - 
                  3*t1*(-15 + 648*t2 - 398*Power(t2,2) + 72*Power(t2,3))\
) + s2*(125 + 97*Power(t1,5) - 719*t2 + 491*Power(t2,2) - 
                  274*Power(t2,3) + 54*Power(t2,4) - 
                  Power(t1,4)*(295 + 179*t2) + 
                  2*Power(t1,3)*(-486 + 676*t2 + 41*Power(t2,2)) + 
                  4*Power(t1,2)*
                   (-52 + 572*t2 - 321*Power(t2,2) + 36*Power(t2,3)) + 
                  t1*(313 + 740*t2 - 1544*Power(t2,2) + 750*Power(t2,3)))\
) + s1*(17 + 3*Power(t1,7) + 320*t2 - 620*Power(t2,2) + 365*Power(t2,3) - 
               162*Power(t2,4) + 12*Power(t2,5) + 
               Power(s2,6)*(-1 + t1 + 7*t2) + Power(t1,6)*(-70 + 8*t2) + 
               Power(t1,5)*(117 + 119*t2 - 20*Power(t2,2)) + 
               Power(t1,3)*(387 - 910*t2 + 634*Power(t2,2) - 
                  55*Power(t2,3)) + 
               Power(t1,4)*(307 - 406*t2 + Power(t2,2) + 9*Power(t2,3)) + 
               Power(t1,2)*(-86 - 666*t2 + 1158*Power(t2,2) - 
                  403*Power(t2,3) + 50*Power(t2,4)) + 
               t1*(-159 - 133*t2 + 578*Power(t2,2) - 444*Power(t2,3) + 
                  88*Power(t2,4)) - 
               Power(s2,5)*(31 + 8*Power(t1,2) + 8*t2 + 72*Power(t2,2) + 
                  t1*(-29 + 11*t2)) + 
               Power(s2,4)*(61 + 25*Power(t1,3) + 49*t2 - 
                  242*Power(t2,2) + 75*Power(t2,3) - 
                  2*Power(t1,2)*(88 + 9*t2) + 
                  t1*(113 + 354*t2 + 200*Power(t2,2))) - 
               Power(s2,3)*(-90 + 40*Power(t1,4) + 274*t2 - 
                  380*Power(t2,2) + 268*Power(t2,3) - 
                  14*Power(t1,3)*(31 + 3*t2) + 
                  Power(t1,2)*(270 + 1133*t2 + 148*Power(t2,2)) + 
                  t1*(336 - 88*t2 - 752*Power(t2,2) + 210*Power(t2,3))) + 
               Power(s2,2)*(-92 + 35*Power(t1,5) - 597*t2 + 
                  1040*Power(t2,2) - 700*Power(t2,3) + 132*Power(t2,4) - 
                  Power(t1,4)*(521 + 13*t2) + 
                  Power(t1,3)*(442 + 1355*t2 - 36*Power(t2,2)) + 
                  Power(t1,2)*
                   (796 - 729*t2 - 777*Power(t2,2) + 204*Power(t2,3)) + 
                  t1*(233 - 666*t2 + 336*Power(t2,2) + 372*Power(t2,3))) \
- s2*(119 + 16*Power(t1,6) - 829*t2 + 1274*Power(t2,2) - 
                  922*Power(t2,3) + 200*Power(t2,4) + 
                  5*Power(t1,5)*(-61 + 3*t2) + 
                  Power(t1,4)*(371 + 687*t2 - 76*Power(t2,2)) + 
                  Power(t1,2)*
                   (710 - 1850*t2 + 1350*Power(t2,2) + 49*Power(t2,3)) + 
                  Power(t1,3)*
                   (828 - 998*t2 - 266*Power(t2,2) + 78*Power(t2,3)) + 
                  t1*(-159 - 1379*t2 + 2338*Power(t2,2) - 
                     1118*Power(t2,3) + 174*Power(t2,4))))))*
       T2(1 - s + s1 - t2,1 - s2 + t1 - t2))/
     ((-1 + s1)*(-s + s1 - t2)*Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,2)*
       (-1 + t2)*(-1 + s2 - t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) + 
    (8*(-24 + 34*s2 + 16*Power(s2,2) + 2*Power(s2,3) - 3*Power(s2,5) + 
         2*t1 - 98*s2*t1 - 28*Power(s2,2)*t1 + 11*Power(s2,3)*t1 + 
         29*Power(s2,4)*t1 - 3*Power(s2,5)*t1 + 70*Power(t1,2) + 
         66*s2*Power(t1,2) - 27*Power(s2,2)*Power(t1,2) - 
         85*Power(s2,3)*Power(t1,2) + 11*Power(s2,4)*Power(t1,2) - 
         2*Power(s2,5)*Power(t1,2) - 40*Power(t1,3) + 21*s2*Power(t1,3) + 
         111*Power(s2,2)*Power(t1,3) - 14*Power(s2,3)*Power(t1,3) + 
         10*Power(s2,4)*Power(t1,3) - 5*Power(t1,4) - 68*s2*Power(t1,4) + 
         6*Power(s2,2)*Power(t1,4) - 20*Power(s2,3)*Power(t1,4) + 
         16*Power(t1,5) + s2*Power(t1,5) + 20*Power(s2,2)*Power(t1,5) - 
         Power(t1,6) - 10*s2*Power(t1,6) + 2*Power(t1,7) + 70*t2 - 
         18*s2*t2 + 4*Power(s2,2)*t2 + Power(s2,3)*t2 - 
         34*Power(s2,4)*t2 - 9*Power(s2,5)*t2 + 3*Power(s2,6)*t2 - 
         108*t1*t2 + 108*s2*t1*t2 + 60*Power(s2,2)*t1*t2 + 
         152*Power(s2,3)*t1*t2 - 4*Power(s2,4)*t1*t2 - 
         4*Power(s2,5)*t1*t2 + 4*Power(s2,6)*t1*t2 - 72*Power(t1,2)*t2 - 
         153*s2*Power(t1,2)*t2 - 259*Power(s2,2)*Power(t1,2)*t2 + 
         87*Power(s2,3)*Power(t1,2)*t2 - 23*Power(s2,4)*Power(t1,2)*t2 - 
         19*Power(s2,5)*Power(t1,2)*t2 + 92*Power(t1,3)*t2 + 
         198*s2*Power(t1,3)*t2 - 147*Power(s2,2)*Power(t1,3)*t2 + 
         72*Power(s2,3)*Power(t1,3)*t2 + 35*Power(s2,4)*Power(t1,3)*t2 - 
         57*Power(t1,4)*t2 + 94*s2*Power(t1,4)*t2 - 
         83*Power(s2,2)*Power(t1,4)*t2 - 30*Power(s2,3)*Power(t1,4)*t2 - 
         21*Power(t1,5)*t2 + 44*s2*Power(t1,5)*t2 + 
         10*Power(s2,2)*Power(t1,5)*t2 - 9*Power(t1,6)*t2 + 
         s2*Power(t1,6)*t2 - Power(t1,7)*t2 - 38*Power(t2,2) - 
         36*s2*Power(t2,2) - 37*Power(s2,2)*Power(t2,2) - 
         95*Power(s2,3)*Power(t2,2) + 9*Power(s2,4)*Power(t2,2) + 
         33*Power(s2,5)*Power(t2,2) - 7*Power(s2,6)*Power(t2,2) - 
         2*Power(s2,7)*Power(t2,2) + 168*t1*Power(t2,2) + 
         69*s2*t1*Power(t2,2) + 251*Power(s2,2)*t1*Power(t2,2) - 
         147*Power(s2,3)*t1*Power(t2,2) - 76*Power(s2,4)*t1*Power(t2,2) + 
         43*Power(s2,5)*t1*Power(t2,2) + 8*Power(s2,6)*t1*Power(t2,2) - 
         70*Power(t1,2)*Power(t2,2) - 200*s2*Power(t1,2)*Power(t2,2) + 
         356*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         3*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         99*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         10*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         44*Power(t1,3)*Power(t2,2) - 307*s2*Power(t1,3)*Power(t2,2) + 
         117*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         106*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         89*Power(t1,4)*Power(t2,2) - 104*s2*Power(t1,4)*Power(t2,2) - 
         49*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         10*Power(s2,3)*Power(t1,4)*Power(t2,2) + 
         27*Power(t1,5)*Power(t2,2) + 3*s2*Power(t1,5)*Power(t2,2) - 
         8*Power(s2,2)*Power(t1,5)*Power(t2,2) + 
         3*Power(t1,6)*Power(t2,2) + 2*s2*Power(t1,6)*Power(t2,2) - 
         24*Power(t2,3) - s2*Power(t2,3) - 87*Power(s2,2)*Power(t2,3) + 
         92*Power(s2,3)*Power(t2,3) + 72*Power(s2,4)*Power(t2,3) - 
         38*Power(s2,5)*Power(t2,3) - 6*Power(s2,6)*Power(t2,3) + 
         Power(s2,7)*Power(t2,3) - 24*t1*Power(t2,3) + 
         96*s2*t1*Power(t2,3) - 351*Power(s2,2)*t1*Power(t2,3) - 
         107*Power(s2,3)*t1*Power(t2,3) + 
         147*Power(s2,4)*t1*Power(t2,3) + 18*Power(s2,5)*t1*Power(t2,3) - 
         5*Power(s2,6)*t1*Power(t2,3) - 6*Power(t1,2)*Power(t2,3) + 
         399*s2*Power(t1,2)*Power(t2,3) - 
         54*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         210*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         12*Power(s2,4)*Power(t1,2)*Power(t2,3) + 
         10*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         140*Power(t1,3)*Power(t2,3) + 141*s2*Power(t1,3)*Power(t2,3) + 
         128*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         12*Power(s2,3)*Power(t1,3)*Power(t2,3) - 
         10*Power(s2,4)*Power(t1,3)*Power(t2,3) - 
         52*Power(t1,4)*Power(t2,3) - 24*s2*Power(t1,4)*Power(t2,3) + 
         18*Power(s2,2)*Power(t1,4)*Power(t2,3) + 
         5*Power(s2,3)*Power(t1,4)*Power(t2,3) - 
         3*Power(t1,5)*Power(t2,3) - 6*s2*Power(t1,5)*Power(t2,3) - 
         Power(s2,2)*Power(t1,5)*Power(t2,3) + 3*Power(t2,4) - 
         18*s2*Power(t2,4) + 132*Power(s2,2)*Power(t2,4) + 
         45*Power(s2,3)*Power(t2,4) - 74*Power(s2,4)*Power(t2,4) - 
         5*Power(s2,5)*Power(t2,4) + 3*Power(s2,6)*Power(t2,4) - 
         6*t1*Power(t2,4) - 242*s2*t1*Power(t2,4) + 
         10*Power(s2,2)*t1*Power(t2,4) + 200*Power(s2,3)*t1*Power(t2,4) + 
         8*Power(s2,4)*t1*Power(t2,4) - 12*Power(s2,5)*t1*Power(t2,4) + 
         113*Power(t1,2)*Power(t2,4) - 112*s2*Power(t1,2)*Power(t2,4) - 
         178*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         6*Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         18*Power(s2,4)*Power(t1,2)*Power(t2,4) + 
         57*Power(t1,3)*Power(t2,4) + 52*s2*Power(t1,3)*Power(t2,4) - 
         16*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         12*Power(s2,3)*Power(t1,3)*Power(t2,4) + 
         7*s2*Power(t1,4)*Power(t2,4) + 
         3*Power(s2,2)*Power(t1,4)*Power(t2,4) + 9*Power(t2,5) + 
         55*s2*Power(t2,5) - 10*Power(s2,2)*Power(t2,5) - 
         64*Power(s2,3)*Power(t2,5) + Power(s2,4)*Power(t2,5) + 
         3*Power(s2,5)*Power(t2,5) - 47*t1*Power(t2,5) + 
         55*s2*t1*Power(t2,5) + 112*Power(s2,2)*t1*Power(t2,5) - 
         7*Power(s2,3)*t1*Power(t2,5) - 9*Power(s2,4)*t1*Power(t2,5) - 
         36*Power(t1,2)*Power(t2,5) - 51*s2*Power(t1,2)*Power(t2,5) + 
         11*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
         9*Power(s2,3)*Power(t1,2)*Power(t2,5) + 
         3*Power(t1,3)*Power(t2,5) - 5*s2*Power(t1,3)*Power(t2,5) - 
         3*Power(s2,2)*Power(t1,3)*Power(t2,5) + 7*Power(t2,6) - 
         14*s2*Power(t2,6) - 23*Power(s2,2)*Power(t2,6) + 
         3*Power(s2,3)*Power(t2,6) + Power(s2,4)*Power(t2,6) + 
         14*t1*Power(t2,6) + 21*s2*t1*Power(t2,6) - 
         6*Power(s2,2)*t1*Power(t2,6) - 2*Power(s2,3)*t1*Power(t2,6) - 
         3*Power(t1,2)*Power(t2,6) + 3*s2*Power(t1,2)*Power(t2,6) + 
         Power(s2,2)*Power(t1,2)*Power(t2,6) - 3*Power(t2,7) - 
         2*s2*Power(t2,7) + Power(s2,2)*Power(t2,7) + t1*Power(t2,7) - 
         s2*t1*Power(t2,7) - Power(s1,7)*Power(s2 - t1,2)*
          (-1 + 2*s2 - 2*t1 + t2) - 
         Power(s1,6)*(s2 - t1)*
          (4*Power(t1,3) + Power(s2,2)*(1 + 4*t1 - 5*t2) + 
            2*Power(-1 + t2,2) + t1*(-7 + 3*t2) - 
            Power(t1,2)*(7 + 5*t2) + 
            s2*(2 - 8*Power(t1,2) + 3*t2 - Power(t2,2) + 2*t1*(3 + 5*t2))\
) + Power(s1,5)*(2*Power(s2,5) - 4*Power(t1,4)*(-2 + t2) - 
            Power(-1 + t2,3) + Power(s2,4)*(10 - 12*t1 + 8*t2) + 
            t1*(-15 + 26*t2 - 11*Power(t2,2)) + 
            Power(t1,3)*(-7 - 9*t2 + 2*Power(t2,2)) + 
            Power(t1,2)*(-42 + 47*t2 + Power(t2,2) + 2*Power(t2,3)) + 
            Power(s2,3)*(20 + 24*Power(t1,2) + 28*t2 - 2*Power(t2,2) - 
               2*t1*(23 + 10*t2)) + 
            s2*(10 + 6*Power(t1,4) - 15*t2 + 4*Power(t2,2) + 
               Power(t2,3) + Power(t1,3)*(-42 + 4*t2) + 
               t1*(74 - 66*t2 - 24*Power(t2,2)) + 
               Power(t1,2)*(32 + 50*t2 - 8*Power(t2,2))) + 
            Power(s2,2)*(-28 - 20*Power(t1,3) + 11*t2 + 27*Power(t2,2) - 
               2*Power(t2,3) + 2*Power(t1,2)*(35 + 6*t2) + 
               t1*(-45 - 69*t2 + 8*Power(t2,2)))) - 
         Power(s1,4)*(-6*Power(-1 + t2,3) - 2*Power(t1,5)*(8 + t2) + 
            2*Power(s2,5)*(11 + t2) + 
            Power(t1,3)*(24 + 51*t2 - 65*Power(t2,2)) + 
            Power(t1,4)*(-34 + 55*t2 + 2*Power(t2,2)) + 
            Power(t1,2)*(10 - 43*t2 + 21*Power(t2,3)) + 
            t1*(69 - 139*t2 + 79*Power(t2,2) - 13*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(s2,4)*(2 + 45*t2 + 14*Power(t2,2) - 
               2*t1*(51 + 8*t2)) + 
            Power(s2,3)*(-18 + 23*t2 + 63*Power(t2,2) + 2*Power(t2,3) + 
               38*Power(t1,2)*(5 + t2) - 
               2*t1*(-3 + 98*t2 + 20*Power(t2,2))) + 
            Power(s2,2)*(-2*Power(t1,3)*(89 + 19*t2) + 
               4*Power(t1,2)*(-13 + 78*t2 + 10*Power(t2,2)) + 
               t1*(54 + 8*t2 - 195*Power(t2,2) + 3*Power(t2,3)) - 
               2*(-6 + 7*t2 + 35*Power(t2,2) - 33*Power(t2,3) + 
                  3*Power(t2,4))) + 
            s2*(-59 + 101*t2 - 29*Power(t2,2) - 13*Power(t2,3) + 
               4*Power(t1,4)*(21 + 4*t2) - 
               2*Power(t1,3)*(-39 + 108*t2 + 8*Power(t2,2)) - 
               Power(t1,2)*(60 + 82*t2 - 197*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(-16 + 59*t2 + 48*Power(t2,2) - 73*Power(t2,3) + 
                  6*Power(t2,4)))) + 
         Power(s1,3)*(-(Power(s2,6)*(-19 + t2)) + 
            Power(t1,6)*(5 + 3*t2) + 
            Power(t1,5)*(98 - 45*t2 - 8*Power(t2,2)) + 
            Power(s2,5)*(-4 + 2*t1*(-54 + t2) + 47*t2 + 2*Power(t2,2)) + 
            Power(t1,3)*(33 - 94*t2 + 233*Power(t2,2) - 
               68*Power(t2,3)) + 
            Power(-1 + t2,2)*
             (-30 + 25*t2 - 5*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,4)*(-6 - 234*t2 + 88*Power(t2,2) + 6*Power(t2,3)) + 
            t1*(33 + 60*t2 - 89*Power(t2,2) - 46*Power(t2,3) + 
               42*Power(t2,4)) - 
            Power(t1,2)*(89 - 35*t2 - 170*Power(t2,2) + 
               143*Power(t2,3) - 20*Power(t2,4) + Power(t2,5)) + 
            Power(s2,4)*(-2 - 53*t2 + 40*Power(t2,2) + 15*Power(t2,3) + 
               Power(t1,2)*(247 + 5*t2) + 
               t1*(100 - 237*t2 - 18*Power(t2,2))) + 
            Power(s2,3)*(-50 + 79*t2 - 84*Power(t2,2) + 
               41*Power(t2,3) + 6*Power(t2,4) - 
               4*Power(t1,3)*(72 + 5*t2) + 
               Power(t1,2)*(-374 + 474*t2 + 50*Power(t2,2)) + 
               t1*(32 + 333*t2 - 200*Power(t2,2) - 43*Power(t2,3))) + 
            Power(s2,2)*(-52 + 50*t2 + 99*Power(t2,2) - 
               158*Power(t2,3) + 58*Power(t2,4) - 5*Power(t2,5) + 
               Power(t1,4)*(177 + 25*t2) + 
               Power(t1,3)*(562 - 470*t2 - 62*Power(t2,2)) + 
               Power(t1,2)*(-64 - 741*t2 + 368*Power(t2,2) + 
                  47*Power(t2,3)) + 
               t1*(115 - 240*t2 + 392*Power(t2,2) - 144*Power(t2,3) - 
                  3*Power(t2,4))) - 
            s2*(36 + 26*t2 - 7*Power(t2,2) - 126*Power(t2,3) + 
               77*Power(t2,4) - 6*Power(t2,5) + 
               2*Power(t1,5)*(26 + 7*t2) + 
               Power(t1,4)*(382 - 231*t2 - 36*Power(t2,2)) + 
               Power(t1,3)*(-40 - 695*t2 + 296*Power(t2,2) + 
                  25*Power(t2,3)) + 
               Power(t1,2)*(98 - 255*t2 + 541*Power(t2,2) - 
                  171*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(-132 + 44*t2 + 306*Power(t2,2) - 288*Power(t2,3) + 
                  60*Power(t2,4) - 6*Power(t2,5)))) + 
         Power(s1,2)*(Power(s2,7)*(-3 + t2) + Power(t1,7)*(1 + t2) + 
            Power(t1,6)*(33 - 10*t2 - 4*Power(t2,2)) + 
            Power(s2,6)*(t1*(19 - 5*t2) + 5*Power(t2,2)) + 
            Power(t1,5)*(20 - 124*t2 + 23*Power(t2,2) + 6*Power(t2,3)) - 
            Power(-1 + t2,2)*
             (-28 - 40*t2 - 11*Power(t2,2) + 23*Power(t2,3)) + 
            Power(t1,4)*(157 - 278*t2 + 215*Power(t2,2) - 
               18*Power(t2,3) - 4*Power(t2,4)) + 
            Power(t1,2)*(35 + 117*t2 + 90*Power(t2,2) - 
               422*Power(t2,3) + 146*Power(t2,4) - 2*Power(t2,5)) + 
            Power(t1,3)*(-119 - 194*t2 + 546*Power(t2,2) - 
               224*Power(t2,3) + 4*Power(t2,4) + Power(t2,5)) + 
            t1*(-32 - 10*t2 + 51*Power(t2,2) - 122*Power(t2,3) + 
               157*Power(t2,4) - 46*Power(t2,5) + 2*Power(t2,6)) + 
            Power(s2,5)*(-36 - t2 + 33*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(-51 + 9*t2) + 
               t1*(-23 + 4*t2 - 17*Power(t2,2))) + 
            Power(s2,4)*(24 - 5*Power(t1,3)*(-15 + t2) - 145*t2 + 
               31*Power(t2,2) + 55*Power(t2,3) - 11*Power(t2,4) + 
               Power(t1,2)*(125 - 26*t2 + 14*Power(t2,2)) + 
               t1*(192 - 120*t2 - 123*Power(t2,2) + 8*Power(t2,3))) + 
            Power(s2,3)*(104 + 68*t2 - 296*Power(t2,2) + 
               56*Power(t2,3) + 21*Power(t2,4) - 7*Power(t2,5) - 
               5*Power(t1,4)*(13 + t2) + 
               2*Power(t1,3)*(-135 + 32*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(-380 + 490*t2 + 148*Power(t2,2) - 
                  36*Power(t2,3)) + 
               t1*(-243 + 757*t2 - 315*Power(t2,2) - 159*Power(t2,3) + 
                  34*Power(t2,4))) + 
            Power(s2,2)*(74 - 26*t2 + 91*Power(t2,2) - 
               239*Power(t2,3) + 79*Power(t2,4) - 16*Power(t2,5) + 
               Power(t2,6) + Power(t1,5)*(33 + 9*t2) + 
               Power(t1,4)*(290 - 76*t2 - 31*Power(t2,2)) + 
               Power(t1,3)*(348 - 740*t2 - 36*Power(t2,2) + 
                  50*Power(t2,3)) + 
               Power(t1,2)*(571 - 1357*t2 + 752*Power(t2,2) + 
                  135*Power(t2,3) - 39*Power(t2,4)) + 
               t1*(-342 - 301*t2 + 1155*Power(t2,2) - 354*Power(t2,3) - 
                  46*Power(t2,4) + 10*Power(t2,5))) - 
            s2*(-10 - 51*t2 + 89*Power(t2,2) - 174*Power(t2,3) + 
               205*Power(t2,4) - 65*Power(t2,5) + 6*Power(t2,6) + 
               Power(t1,6)*(9 + 5*t2) + 
               Power(t1,5)*(155 - 44*t2 - 19*Power(t2,2)) + 
               Power(t1,4)*(144 - 495*t2 + 45*Power(t2,2) + 
                  29*Power(t2,3)) + 
               Power(t1,3)*(509 - 1023*t2 + 683*Power(t2,2) + 
                  13*Power(t2,3) - 20*Power(t2,4)) + 
               Power(t1,2)*(-357 - 427*t2 + 1405*Power(t2,2) - 
                  522*Power(t2,3) - 21*Power(t2,4) + 4*Power(t2,5)) + 
               t1*(91 + 118*t2 + 214*Power(t2,2) - 726*Power(t2,3) + 
                  238*Power(t2,4) - 8*Power(t2,5) + Power(t2,6)))) + 
         s1*(5*Power(t1,7) - Power(s2,7)*t2*(3 + 2*t2) - 
            Power(t1,6)*(-22 + 29*t2 + Power(t2,2)) + 
            Power(t1,5)*(6 - 112*t2 + 66*Power(t2,2) + 5*Power(t2,3)) - 
            Power(t1,4)*(32 + 18*t2 - 247*Power(t2,2) + 
               82*Power(t2,3) + 9*Power(t2,4)) - 
            Power(-1 + t2,2)*
             (-14 + 58*t2 + 36*Power(t2,2) + 31*Power(t2,3) - 
               24*Power(t2,4) + Power(t2,5)) + 
            Power(t1,3)*(159 - 208*t2 + 172*Power(t2,2) - 
               290*Power(t2,3) + 60*Power(t2,4) + 7*Power(t2,5)) - 
            Power(t1,2)*(84 + 79*t2 - 328*Power(t2,2) + 
               296*Power(t2,3) - 189*Power(t2,4) + 24*Power(t2,5) + 
               2*Power(t2,6)) + 
            t1*(-6 + 136*t2 - 143*Power(t2,2) - 126*Power(t2,3) + 
               216*Power(t2,4) - 82*Power(t2,5) + 5*Power(t2,6)) + 
            Power(s2,6)*(6 + 8*t2 - 15*Power(t2,2) - 7*Power(t2,3) + 
               t1*(3 + 22*t2 + 10*Power(t2,2))) - 
            Power(s2,5)*(5*Power(t1,2)*(4 + 13*t2 + 4*Power(t2,2)) + 
               t2*(-34 - 42*t2 + 35*Power(t2,2) + 6*Power(t2,3)) - 
               3*t1*(-16 - 9*t2 + 32*Power(t2,2) + 9*Power(t2,3))) + 
            Power(s2,4)*(15 + t2 + 30*Power(t2,2) + 100*Power(t2,3) - 
               41*Power(t2,4) + Power(t2,5) + 
               5*Power(t1,3)*(11 + 20*t2 + 4*Power(t2,2)) - 
               Power(t1,2)*(-154 + t2 + 235*Power(t2,2) + 
                  38*Power(t2,3)) + 
               t1*(12 - 268*t2 - 105*Power(t2,2) + 171*Power(t2,3) + 
                  15*Power(t2,4))) - 
            Power(s2,3)*(36 - 94*t2 + 93*Power(t2,2) - 49*Power(t2,3) - 
               105*Power(t2,4) + 21*Power(t2,5) - 2*Power(t2,6) + 
               5*Power(t1,4)*(16 + 17*t2 + 2*Power(t2,2)) - 
               2*Power(t1,3)*
                (-128 + 57*t2 + 140*Power(t2,2) + 11*Power(t2,3)) + 
               Power(t1,2)*(42 - 712*t2 + 3*Power(t2,2) + 
                  308*Power(t2,3) + 9*Power(t2,4)) + 
               t1*(56 - 122*t2 + 456*Power(t2,2) + 171*Power(t2,3) - 
                  142*Power(t2,4) + 5*Power(t2,5))) + 
            Power(s2,2)*(-114 + 5*t2 + 311*Power(t2,2) - 
               303*Power(t2,3) + 96*Power(t2,4) + 39*Power(t2,5) - 
               2*Power(t2,6) + 
               Power(t1,5)*(65 + 38*t2 + 2*Power(t2,2)) - 
               3*Power(t1,4)*
                (-78 + 62*t2 + 55*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(54 - 868*t2 + 219*Power(t2,2) + 
                  248*Power(t2,3) - 3*Power(t2,4)) + 
               Power(t1,2)*(35 - 265*t2 + 1069*Power(t2,2) - 
                  40*Power(t2,3) - 170*Power(t2,4) + 7*Power(t2,5)) + 
               t1*(241 - 434*t2 + 442*Power(t2,2) - 494*Power(t2,3) - 
                  103*Power(t2,4) + 51*Power(t2,5) - 3*Power(t2,6))) + 
            s2*(-7*Power(t1,6)*(4 + t2) - 
               Power(t1,5)*(112 - 121*t2 - 40*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,4)*(-30 + 502*t2 - 219*Power(t2,2) - 
                  81*Power(t2,3) + 3*Power(t2,4)) + 
               Power(t1,3)*(38 + 160*t2 - 890*Power(t2,2) + 
                  193*Power(t2,3) + 78*Power(t2,4) - 3*Power(t2,5)) + 
               Power(t1,2)*(-364 + 548*t2 - 521*Power(t2,2) + 
                  735*Power(t2,3) - 62*Power(t2,4) - 37*Power(t2,5) + 
                  Power(t2,6)) + 
               t2*(-78 + 58*t2 + 152*Power(t2,2) - 215*Power(t2,3) + 
                  89*Power(t2,4) - 7*Power(t2,5) + Power(t2,6)) + 
               2*t1*(103 + 11*t2 - 279*Power(t2,2) + 291*Power(t2,3) - 
                  162*Power(t2,4) + Power(t2,5) + 3*Power(t2,6)))) - 
         Power(s,5)*(t1 - t2)*
          (-4 - Power(s1,4) - Power(t1,3) + Power(t1,4) + 
            Power(s1,3)*(9 - 5*s2 + 4*t1 - 4*t2) + 3*Power(t1,2)*t2 - 
            4*Power(t1,3)*t2 - 3*t1*Power(t2,2) + 
            6*Power(t1,2)*Power(t2,2) + Power(t2,3) - 4*t1*Power(t2,3) + 
            Power(t2,4) + Power(s2,3)*(1 - t1 + t2) + 
            3*Power(s2,2)*(-t1 + Power(t1,2) + t2 - 2*t1*t2 + 
               Power(t2,2)) + 
            3*Power(s1,2)*(-6 + Power(s2,2) + 2*Power(t1,2) + t2 + 
               2*Power(t2,2) + s2*(3 - 3*t1 + 3*t2) - t1*(1 + 4*t2)) + 
            s2*(2 - 3*Power(t1,3) + 3*Power(t2,2) + 3*Power(t2,3) - 
               3*t1*t2*(2 + 3*t2) + Power(t1,2)*(3 + 9*t2)) - 
            s1*(-14 + Power(s2,3) - 4*Power(t1,3) + 3*Power(t2,2) + 
               4*Power(t2,3) - 6*t1*t2*(1 + 2*t2) + 
               3*Power(t1,2)*(1 + 4*t2) + 
               Power(s2,2)*(3 - 6*t1 + 6*t2) + 
               3*s2*(2 + 3*Power(t1,2) + 2*t2 + 3*Power(t2,2) - 
                  2*t1*(1 + 3*t2)))) - 
         Power(s,4)*(4 + Power(s1,5)*(-2 + 3*s2 - t1) + 36*t1 - 
            Power(t1,3) + 7*Power(t1,4) - 6*Power(t1,5) + 
            2*Power(t1,6) - 36*t2 - 20*t1*t2 + 5*Power(t1,2)*t2 - 
            24*Power(t1,3)*t2 + 26*Power(t1,4)*t2 - 9*Power(t1,5)*t2 + 
            20*Power(t2,2) - 7*t1*Power(t2,2) + 
            30*Power(t1,2)*Power(t2,2) - 44*Power(t1,3)*Power(t2,2) + 
            15*Power(t1,4)*Power(t2,2) + 3*Power(t2,3) - 
            16*t1*Power(t2,3) + 36*Power(t1,2)*Power(t2,3) - 
            10*Power(t1,3)*Power(t2,3) + 3*Power(t2,4) - 
            14*t1*Power(t2,4) + 2*Power(t2,5) + 3*t1*Power(t2,5) - 
            Power(t2,6) - Power(s1,4)*
             (-1 + s2 + 5*Power(s2,2) + 15*t1 - 27*s2*t1 + 
               19*Power(t1,2) - 16*t2 + 23*s2*t2 - 29*t1*t2 + 
               10*Power(t2,2)) + 
            Power(s2,4)*(2*Power(t1,2) - 2*t1*(1 + 3*t2) + 
               t2*(5 + 4*t2)) + 
            Power(s2,3)*(1 - 8*Power(t1,3) + 3*t2 + 17*Power(t2,2) + 
               11*Power(t2,3) + 3*Power(t1,2)*(4 + 9*t2) - 
               t1*(7 + 29*t2 + 30*Power(t2,2))) + 
            Power(s2,2)*(4 + 12*Power(t1,4) + 3*t2 + 9*Power(t2,2) + 
               21*Power(t2,3) + 9*Power(t2,4) - 
               3*Power(t1,3)*(8 + 15*t2) + 
               3*Power(t1,2)*(7 + 23*t2 + 21*Power(t2,2)) - 
               t1*(1 + 30*t2 + 66*Power(t2,2) + 39*Power(t2,3))) + 
            s2*(-10 - 8*Power(t1,5) + 18*t2 - 5*Power(t2,2) + 
               9*Power(t2,3) + 11*Power(t2,4) + Power(t2,5) + 
               Power(t1,4)*(20 + 33*t2) - 
               Power(t1,3)*(21 + 71*t2 + 52*Power(t2,2)) + 
               Power(t1,2)*(1 + 51*t2 + 93*Power(t2,2) + 
                  38*Power(t2,3)) - 
               t1*(18 - 4*t2 + 39*Power(t2,2) + 53*Power(t2,3) + 
                  12*Power(t2,4))) + 
            Power(s1,3)*(1 + 3*Power(s2,3) - 13*Power(t1,3) + 10*t2 - 
               38*Power(t2,2) + 23*Power(t2,3) - 
               Power(s2,2)*(1 + 6*t1 + 2*t2) + 
               Power(t1,2)*(5 + 49*t2) + 
               t1*(-12 + 33*t2 - 59*Power(t2,2)) + 
               s2*(7 + 16*Power(t1,2) + 20*t2 + 23*Power(t2,2) - 
                  t1*(20 + 39*t2))) - 
            Power(s1,2)*(-6 + Power(s2,4) + 3*Power(t1,4) + 
               Power(s2,3)*(1 - 3*t1 - 5*t2) + 91*t2 - 80*Power(t2,2) - 
               14*Power(t2,3) + 19*Power(t2,4) - 
               7*Power(t1,3)*(-5 + 4*t2) + 
               2*Power(t1,2)*(5 - 42*t2 + 33*Power(t2,2)) + 
               t1*(-93 + 70*t2 + 63*Power(t2,2) - 60*Power(t2,3)) + 
               Power(s2,2)*(-15 + 6*Power(t1,2) - 31*t2 - 
                  9*Power(t2,2) + t1*(31 + 3*t2)) + 
               s2*(29 - 7*Power(t1,3) - 13*t2 - 16*Power(t2,2) + 
                  16*Power(t2,3) + Power(t1,2)*(-67 + 30*t2) + 
                  t1*(13 + 83*t2 - 39*Power(t2,2)))) + 
            s1*(-10 + 4*Power(t1,5) + Power(s2,4)*(1 + t1 - 4*t2) + 
               102*t2 - 61*Power(t2,2) - 18*Power(t2,3) - 
               6*Power(t2,4) + 7*Power(t2,5) - Power(t1,4)*(23 + 9*t2) + 
               Power(t1,3)*(24 + 75*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(13 - 66*t2 - 87*Power(t2,2) + 
                  26*Power(t2,3)) + 
               t1*(-102 + 48*t2 + 60*Power(t2,2) + 41*Power(t2,3) - 
                  24*Power(t2,4)) - 
               Power(s2,3)*(3 + 7*Power(t1,2) + 12*t2 + 
                  20*Power(t2,2) - t1*(8 + 27*t2)) + 
               Power(s2,2)*(-13 + 15*Power(t1,3) - 26*t2 - 
                  33*Power(t2,2) - 21*Power(t2,3) - 
                  3*Power(t1,2)*(14 + 17*t2) + 
                  t1*(32 + 75*t2 + 57*Power(t2,2))) + 
               s2*(30 - 13*Power(t1,4) - 32*t2 - 11*Power(t2,2) - 
                  26*Power(t2,3) + 2*Power(t2,4) + 
                  Power(t1,3)*(56 + 37*t2) - 
                  Power(t1,2)*(53 + 138*t2 + 33*Power(t2,2)) + 
                  t1*(28 + 64*t2 + 108*Power(t2,2) + 7*Power(t2,3))))) + 
         Power(s,3)*(32 + 100*t1 - 38*Power(t1,2) - 5*Power(t1,3) + 
            26*Power(t1,4) - 10*Power(t1,5) + 9*Power(t1,6) - 
            Power(t1,7) - 116*t2 - 66*t1*t2 + 21*Power(t1,2)*t2 - 
            90*Power(t1,3)*t2 + 21*Power(t1,4)*t2 - 39*Power(t1,5)*t2 + 
            3*Power(t1,6)*t2 + 104*Power(t2,2) + 13*t1*Power(t2,2) + 
            108*Power(t1,2)*Power(t2,2) + 2*Power(t1,3)*Power(t2,2) + 
            66*Power(t1,4)*Power(t2,2) - 29*Power(t2,3) - 
            50*t1*Power(t2,3) - 32*Power(t1,2)*Power(t2,3) - 
            54*Power(t1,3)*Power(t2,3) - 10*Power(t1,4)*Power(t2,3) + 
            6*Power(t2,4) + 24*t1*Power(t2,4) + 
            21*Power(t1,2)*Power(t2,4) + 15*Power(t1,3)*Power(t2,4) - 
            5*Power(t2,5) - 3*t1*Power(t2,5) - 
            9*Power(t1,2)*Power(t2,5) + 2*t1*Power(t2,6) + 
            Power(s1,6)*(-4 + 6*s2 - 5*t1 + 3*t2) + 
            Power(s1,5)*(8 - 2*Power(s2,2) - 21*Power(t1,2) + 
               s2*(-9 + 25*t1 - 28*t2) + 3*t2 + 35*t1*t2 - 
               11*Power(t2,2)) + 
            Power(s2,5)*(Power(t1,2) - t1*(1 + 6*t2) + t2*(7 + 6*t2)) + 
            Power(s2,4)*(-5*Power(t1,3) + Power(t1,2)*(13 + 27*t2) - 
               3*t1*(2 + 17*t2 + 12*Power(t2,2)) + 
               2*(-1 + 6*t2 + 13*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s2,3)*(9 + 10*Power(t1,4) + 6*t2 + 31*Power(t2,2) + 
               36*Power(t2,3) + 6*Power(t2,4) - 
               6*Power(t1,3)*(7 + 8*t2) + 
               2*Power(t1,2)*(14 + 75*t2 + 36*Power(t2,2)) - 
               t1*(18 + 45*t2 + 144*Power(t2,2) + 40*Power(t2,3))) + 
            Power(s2,2)*(26 - 10*Power(t1,5) + 17*t2 + 32*Power(t2,2) + 
               21*Power(t2,3) + 22*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,4)*(58 + 42*t2) - 
               2*Power(t1,3)*(24 + 107*t2 + 30*Power(t2,2)) + 
               Power(t1,2)*(68 + 75*t2 + 276*Power(t2,2) + 
                  28*Power(t2,3)) + 
               t1*(-29 - 106*t2 - 48*Power(t2,2) - 142*Power(t2,3) + 
                  6*Power(t2,4))) + 
            s2*(-72 + 5*Power(t1,6) + 48*t2 - 37*Power(t2,2) + 
               50*Power(t2,3) - 3*Power(t2,4) + 5*Power(t2,5) - 
               4*Power(t2,6) - Power(t1,5)*(37 + 18*t2) + 
               3*Power(t1,4)*(12 + 49*t2 + 6*Power(t2,2)) + 
               Power(t1,3)*(-74 - 63*t2 - 224*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(25 + 190*t2 + 15*Power(t2,2) + 
                  160*Power(t2,3) - 27*Power(t2,4)) + 
               t1*(-10 + 12*t2 - 166*Power(t2,2) + 15*Power(t2,3) - 
                  51*Power(t2,4) + 18*Power(t2,5))) + 
            Power(s1,4)*(-19 - 6*Power(s2,3) + 7*Power(t1,3) + 
               Power(s2,2)*(-1 + 38*t1 - 32*t2) + 57*t2 - 
               57*Power(t2,2) + 25*Power(t2,3) + 
               Power(t1,2)*(-22 + 34*t2) + 
               t1*(-65 + 77*t2 - 66*Power(t2,2)) + 
               s2*(11 - 39*Power(t1,2) + 19*t2 + 21*Power(t2,2) + 
                  t1*(13 + 5*t2))) + 
            Power(s1,3)*(19 + 4*Power(s2,4) + 14*Power(t1,4) - 61*t2 - 
               20*Power(t2,2) + 89*Power(t2,3) - 27*Power(t2,4) + 
               Power(s2,3)*(1 - 12*t1 + 4*t2) - 
               3*Power(t1,3)*(29 + 7*t2) + 
               Power(t1,2)*(21 + 181*t2 - 27*Power(t2,2)) + 
               t1*(69 + 3*t2 - 183*Power(t2,2) + 61*Power(t2,3)) + 
               Power(s2,2)*(1 + 26*Power(t1,2) + 37*t2 + 
                  53*Power(t2,2) - t1*(79 + 53*t2)) + 
               s2*(25 - 32*Power(t1,3) + 34*t2 - 21*Power(t2,2) + 
                  16*Power(t2,3) + 5*Power(t1,2)*(33 + 14*t2) - 
                  18*t1*(4 + 8*t2 + 3*Power(t2,2)))) + 
            Power(s1,2)*(36 - 2*Power(s2,5) + 15*Power(t1,5) - 137*t2 + 
               193*Power(t2,2) - 86*Power(t2,3) - 39*Power(t2,4) + 
               12*Power(t2,5) + Power(s2,4)*(-5 + 13*t1 + t2) - 
               5*Power(t1,4)*(1 + 12*t2) + 
               3*Power(t1,3)*(-13 + 34*t2 + 26*Power(t2,2)) - 
               Power(t1,2)*(147 - 166*t2 + 228*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(111 - 52*t2 - 41*Power(t2,2) + 170*Power(t2,3) - 
                  21*Power(t2,4)) + 
               Power(s2,3)*(25 - 42*Power(t1,2) + 18*t2 - 
                  21*Power(t2,2) + t1*(14 + 39*t2)) + 
               Power(s2,2)*(25 + 68*Power(t1,3) + 115*t2 - 
                  34*Power(t2,2) - 71*Power(t2,3) - 
                  3*Power(t1,2)*(6 + 47*t2) + 
                  t1*(-133 + 46*t2 + 144*Power(t2,2))) + 
               s2*(-139 - 52*Power(t1,4) - 98*t2 + 152*Power(t2,2) - 
                  36*Power(t2,3) - 35*Power(t2,4) + 
                  7*Power(t1,3)*(2 + 23*t2) + 
                  Power(t1,2)*(147 - 166*t2 - 201*Power(t2,2)) + 
                  t1*(186 - 293*t2 + 188*Power(t2,2) + 127*Power(t2,3)))\
) - s1*(72 - 3*Power(t1,6) - 254*t2 + 227*Power(t2,2) - 39*Power(t2,3) - 
               24*Power(t2,4) - 8*Power(t2,5) + 2*Power(t2,6) + 
               Power(s2,5)*(-2 + t1 + 6*t2) + 
               Power(t1,5)*(-21 + 22*t2) + 
               Power(t1,4)*(41 + 42*t2 - 56*Power(t2,2)) + 
               Power(s2,4)*(-3 + t1 - 7*Power(t1,2) + 14*t2 - 
                  22*t1*t2 + 22*Power(t2,2)) + 
               Power(t1,3)*(-85 - 75*t2 + 8*Power(t2,2) + 
                  64*Power(t2,3)) + 
               Power(t1,2)*(-185 + 283*t2 + 3*Power(t2,2) - 
                  66*Power(t2,3) - 31*Power(t2,4)) + 
               t1*(212 - 42*t2 - 159*Power(t2,2) + 55*Power(t2,3) + 
                  45*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,3)*(29 + 18*Power(t1,3) + 34*t2 + 
                  40*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t1,2)*(30 + 8*t2) - 
                  2*t1*(6 + 45*t2 + 14*Power(t2,2))) + 
               Power(s2,2)*(49 - 22*Power(t1,4) + 123*t2 + 
                  45*Power(t2,2) + 22*Power(t2,3) - 36*Power(t2,4) + 
                  Power(t1,3)*(-76 + 48*t2) + 
                  Power(t1,2)*(74 + 180*t2 - 66*Power(t2,2)) + 
                  t1*(-199 - 95*t2 - 126*Power(t2,2) + 76*Power(t2,3))) \
+ s2*(-178 + 13*Power(t1,5) + Power(t1,4)*(68 - 62*t2) - 14*t2 + 
                  83*Power(t2,2) + 44*Power(t2,3) - 10*Power(t2,4) - 
                  20*Power(t2,5) + 
                  2*Power(t1,3)*(-50 - 73*t2 + 64*Power(t2,2)) + 
                  Power(t1,2)*
                   (255 + 136*t2 + 78*Power(t2,2) - 142*Power(t2,3)) + 
                  t1*(136 - 350*t2 - 80*Power(t2,2) + 10*Power(t2,3) + 
                     83*Power(t2,4))))) - 
         Power(s,2)*(76 + 106*t1 - 146*Power(t1,2) + 26*Power(t1,3) + 
            52*Power(t1,4) - 16*Power(t1,5) + 13*Power(t1,6) - 
            4*Power(t1,7) - 202*t2 + 56*Power(t1,2)*t2 - 
            186*Power(t1,3)*t2 + 2*Power(t1,4)*t2 - 50*Power(t1,5)*t2 + 
            15*Power(t1,6)*t2 + Power(t1,7)*t2 + 170*Power(t2,2) + 
            20*t1*Power(t2,2) + 232*Power(t1,2)*Power(t2,2) + 
            118*Power(t1,3)*Power(t2,2) + 97*Power(t1,4)*Power(t2,2) - 
            18*Power(t1,5)*Power(t2,2) - 5*Power(t1,6)*Power(t2,2) - 
            102*Power(t2,3) - 154*t1*Power(t2,3) - 
            200*Power(t1,2)*Power(t2,3) - 130*Power(t1,3)*Power(t2,3) + 
            Power(t1,4)*Power(t2,3) + 10*Power(t1,5)*Power(t2,3) + 
            56*Power(t2,4) + 118*t1*Power(t2,4) + 
            113*Power(t1,2)*Power(t2,4) + 16*Power(t1,3)*Power(t2,4) - 
            10*Power(t1,4)*Power(t2,4) - 22*Power(t2,5) - 
            52*t1*Power(t2,5) - 15*Power(t1,2)*Power(t2,5) + 
            5*Power(t1,3)*Power(t2,5) + 9*Power(t2,6) + 
            6*t1*Power(t2,6) - Power(t1,2)*Power(t2,6) - Power(t2,7) + 
            Power(s1,7)*(-2 + 3*s2 - 3*t1 + 2*t2) + 
            Power(s2,6)*t2*(3 - 2*t1 + 4*t2) + 
            Power(s1,6)*(3 + 8*Power(s2,2) - Power(t1,2) - 2*t2 - 
               5*Power(t2,2) - s2*(5 + 7*t1 + 7*t2) + t1*(5 + 11*t2)) + 
            Power(s2,5)*(-3 + 11*t2 + 15*Power(t2,2) + 6*Power(t2,3) + 
               Power(t1,2)*(4 + 9*t2) + t1*(1 - 34*t2 - 18*Power(t2,2))) \
+ Power(s2,4)*(8 + 18*t2 + 20*Power(t2,2) + 27*Power(t2,3) - 
               6*Power(t2,4) - 5*Power(t1,3)*(4 + 3*t2) + 
               Power(t1,2)*(9 + 121*t2 + 27*Power(t2,2)) - 
               t1*(5 + 50*t2 + 110*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s2,3)*(20 + 44*t2 + 44*Power(t2,2) + 3*Power(t2,3) + 
               20*Power(t2,4) - 14*Power(t2,5) + 
               10*Power(t1,4)*(4 + t2) - 
               2*Power(t1,3)*(23 + 102*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(49 + 134*t2 + 258*Power(t2,2) - 
                  28*Power(t2,3)) + 
               t1*(-78 - 65*t2 - 109*Power(t2,2) - 114*Power(t2,3) + 
                  40*Power(t2,4))) - 
            Power(s2,2)*(-24 + 40*Power(t1,5) + 18*t2 - 
               92*Power(t2,2) + 12*Power(t2,3) + Power(t2,4) - 
               3*Power(t2,5) + 6*Power(t2,6) + 
               Power(t1,4)*(-74 - 181*t2 + 18*Power(t2,2)) + 
               Power(t1,3)*(87 + 212*t2 + 264*Power(t2,2) - 
                  60*Power(t2,3)) + 
               Power(t1,2)*(-184 - 78*t2 - 255*Power(t2,2) - 
                  148*Power(t2,3) + 72*Power(t2,4)) + 
               t1*(48 + 260*t2 - 27*Power(t2,2) + 116*Power(t2,3) + 
                  28*Power(t2,4) - 36*Power(t2,5))) - 
            s2*(148 - 84*t2 + 50*Power(t2,2) - 156*Power(t2,3) + 
               77*Power(t2,4) - 14*Power(t2,5) + 3*Power(t2,6) + 
               Power(t1,6)*(-20 + 3*t2) + 
               Power(t1,5)*(51 + 82*t2 - 18*Power(t2,2)) + 
               Power(t1,4)*(-62 - 167*t2 - 119*Power(t2,2) + 
                  42*Power(t2,3)) + 
               Power(t1,3)*(166 + 33*t2 + 263*Power(t2,2) + 
                  62*Power(t2,3) - 48*Power(t2,4)) + 
               Power(t1,2)*(-2 - 402*t2 + 189*Power(t2,2) - 
                  243*Power(t2,3) + 8*Power(t2,4) + 27*Power(t2,5)) + 
               t1*(-124 + 6*t2 + 392*Power(t2,2) - 237*Power(t2,3) + 
                  110*Power(t2,4) - 16*Power(t2,5) - 6*Power(t2,6))) - 
            Power(s1,5)*(-9 + 9*Power(s2,3) - 26*Power(t1,3) - 12*t2 + 
               9*Power(t2,2) - 8*Power(t2,3) + 
               2*Power(t1,2)*(23 + 9*t2) + 
               Power(s2,2)*(26 - 54*t1 + 46*t2) + 
               t1*(53 - 55*t2 + 19*Power(t2,2)) + 
               s2*(-4 + 71*Power(t1,2) + 9*t2 + 2*Power(t2,2) - 
                  4*t1*(18 + 17*t2))) + 
            Power(s1,4)*(-Power(s2,4) + 13*Power(t1,4) + 
               Power(s2,3)*(29 + 12*t1 - 13*t2) - 
               Power(t1,3)*(17 + 65*t2) + 
               Power(t1,2)*(21 + 52*t2 + 56*Power(t2,2)) + 
               t1*(2 + 106*t2 - 83*Power(t2,2) + 6*Power(t2,3)) - 
               2*(35 - 43*t2 + 55*Power(t2,2) - 24*Power(t2,3) + 
                  5*Power(t2,4)) - 
               Power(s2,2)*(3 + 8*Power(t1,2) - 29*t2 - 
                  62*Power(t2,2) + t1*(61 + 52*t2)) + 
               s2*(30 - 16*Power(t1,3) + 60*t2 - 81*Power(t2,2) + 
                  36*Power(t2,3) + Power(t1,2)*(49 + 130*t2) - 
                  t1*(65 + 37*t2 + 135*Power(t2,2)))) + 
            Power(s1,3)*(98 + 8*Power(t1,5) + 
               Power(s2,4)*(-17 + 10*t1) + Power(t1,4)*(99 - 59*t2) - 
               104*t2 + 112*Power(t2,2) + 23*Power(t2,3) - 
               31*Power(t2,4) + 6*Power(t2,5) + 
               Power(t1,3)*(-104 - 168*t2 + 121*Power(t2,2)) + 
               Power(t1,2)*(12 + 168*t2 + 86*Power(t2,2) - 
                  91*Power(t2,3)) + 
               t1*(50 - 158*t2 - 87*Power(t2,2) + 14*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(s2,3)*(-17 - 38*Power(t1,2) + 3*t2 + 
                  29*Power(t2,2) + t1*(-20 + 23*t2)) + 
               Power(s2,2)*(55 + 54*Power(t1,3) + 112*t2 + 
                  56*Power(t2,2) - 42*Power(t2,3) - 
                  5*Power(t1,2)*(-38 + 21*t2) + 
                  t1*(-139 - 148*t2 + 63*Power(t2,2))) + 
               s2*(6 - 34*Power(t1,4) - 119*t2 + 11*Power(t2,2) + 
                  179*Power(t2,3) - 55*Power(t2,4) + 
                  3*Power(t1,3)*(-84 + 47*t2) + 
                  Power(t1,2)*(260 + 313*t2 - 213*Power(t2,2)) + 
                  t1*(-38 - 199*t2 - 240*Power(t2,2) + 161*Power(t2,3)))\
) - Power(s1,2)*(-22 + Power(s2,6) + 4*Power(t1,6) + 
               Power(t1,5)*(-58 + t2) + 174*t2 - 142*Power(t2,2) + 
               194*Power(t2,3) - 98*Power(t2,4) + 9*Power(t2,5) + 
               Power(t2,6) + Power(s2,5)*(4 - 5*t1 + 2*t2) + 
               Power(t1,4)*(-77 + 249*t2 - 43*Power(t2,2)) + 
               Power(t1,3)*(-260 + 254*t2 - 360*Power(t2,2) + 
                  82*Power(t2,3)) + 
               Power(t1,2)*(232 + 246*t2 - 213*Power(t2,2) + 
                  214*Power(t2,3) - 58*Power(t2,4)) + 
               t1*(-60 - 136*t2 - 186*Power(t2,2) + 134*Power(t2,3) - 
                  54*Power(t2,4) + 13*Power(t2,5)) + 
               Power(s2,4)*(-30 + 14*Power(t1,2) + 22*t2 + 
                  38*Power(t2,2) - t1*(64 + 21*t2)) + 
               Power(s2,3)*(-93 - 26*Power(t1,3) - 177*t2 + 
                  147*Power(t2,2) + 71*Power(t2,3) + 
                  Power(t1,2)*(226 + 50*t2) + 
                  t1*(220 - 303*t2 - 119*Power(t2,2))) + 
               Power(s2,2)*(168 + 29*Power(t1,4) + 30*t2 - 
                  119*Power(t2,2) + 199*Power(t2,3) + 2*Power(t2,4) - 
                  2*Power(t1,3)*(167 + 22*t2) + 
                  Power(t1,2)*(-427 + 789*t2 + 81*Power(t2,2)) - 
                  t1*(61 - 582*t2 + 672*Power(t2,2) + 68*Power(t2,3))) \
+ s2*(148 - 17*Power(t1,5) + 74*t2 - 259*Power(t2,2) + 62*Power(t2,3) + 
                  139*Power(t2,4) - 31*Power(t2,5) + 
                  2*Power(t1,4)*(113 + 6*t2) + 
                  Power(t1,3)*(314 - 757*t2 + 43*Power(t2,2)) + 
                  Power(t1,2)*
                   (414 - 659*t2 + 885*Power(t2,2) - 85*Power(t2,3)) + 
                  t1*(-476 - 71*t2 + 301*Power(t2,2) - 
                     493*Power(t2,3) + 78*Power(t2,4)))) + 
            s1*(-136 - 3*Power(t1,7) + 364*t2 - 222*Power(t2,2) + 
               80*Power(t2,3) + 11*Power(t2,4) - 35*Power(t2,5) + 
               6*Power(t2,6) - Power(s2,6)*(-1 + t1 + 4*t2) + 
               Power(t1,6)*(7 + 13*t2) + 
               Power(t1,5)*(61 - 69*t2 - 19*Power(t2,2)) + 
               Power(t1,4)*(-47 - 196*t2 + 182*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,3)*(-10 - 16*t2 + 293*Power(t2,2) - 
                  208*Power(t2,3) + 11*Power(t2,4)) + 
               Power(t1,2)*(494 - 516*t2 + 336*Power(t2,2) - 
                  277*Power(t2,3) + 117*Power(t2,4) - 11*Power(t2,5)) + 
               t1*(-140 - 338*t2 + 446*Power(t2,2) - 284*Power(t2,3) + 
                  154*Power(t2,4) - 35*Power(t2,5) + 3*Power(t2,6)) + 
               Power(s2,5)*(7 + 8*Power(t1,2) - 2*t2 - 6*Power(t2,2) + 
                  t1*(-12 + 11*t2)) - 
               Power(s2,4)*(20 + 25*Power(t1,3) + 32*t2 - 
                  6*Power(t2,2) - 30*Power(t2,3) - 
                  9*Power(t1,2)*(5 + t2) + 
                  t1*(8 + 19*t2 + 17*Power(t2,2))) + 
               Power(s2,3)*(-116 + 40*Power(t1,4) - 117*t2 - 
                  62*Power(t2,2) + 52*Power(t2,3) + 56*Power(t2,4) - 
                  2*Power(t1,3)*(40 + 33*t2) + 
                  Power(t1,2)*(-79 + 138*t2 + 106*Power(t2,2)) + 
                  t1*(142 + 229*t2 - 146*Power(t2,2) - 136*Power(t2,3))) \
+ Power(s2,2)*(110 - 35*Power(t1,5) - 168*t2 + Power(t2,2) - 
                  47*Power(t2,3) + 79*Power(t2,4) + 18*Power(t2,5) + 
                  Power(t1,4)*(75 + 94*t2) + 
                  Power(t1,3)*(215 - 280*t2 - 156*Power(t2,2)) + 
                  Power(t1,2)*
                   (-271 - 558*t2 + 456*Power(t2,2) + 188*Power(t2,3)) \
+ t1*(334 + 120*t2 + 426*Power(t2,2) - 330*Power(t2,3) - 
                     109*Power(t2,4))) + 
               s2*(258 + 16*Power(t1,6) + 140*t2 - 410*Power(t2,2) + 
                  81*Power(t2,3) + Power(t2,4) + 42*Power(t2,5) - 
                  6*Power(t2,6) - 3*Power(t1,5)*(12 + 19*t2) + 
                  4*Power(t1,4)*(-49 + 58*t2 + 23*Power(t2,2)) + 
                  Power(t1,3)*
                   (196 + 557*t2 - 498*Power(t2,2) - 88*Power(t2,3)) + 
                  Power(t1,2)*
                   (-208 + 13*t2 - 657*Power(t2,2) + 486*Power(t2,3) + 
                     42*Power(t2,4)) + 
                  t1*(-680 + 796*t2 - 302*Power(t2,2) + 
                     295*Power(t2,3) - 226*Power(t2,4) + Power(t2,5))))) \
+ s*(72 + 36*t1 - 178*Power(t1,2) + 71*Power(t1,3) + 35*Power(t1,4) - 
            26*Power(t1,5) + 7*Power(t1,6) - 5*Power(t1,7) - 188*t2 + 
            154*t1*t2 + 109*Power(t1,2)*t2 - 192*Power(t1,3)*t2 + 
            42*Power(t1,4)*t2 - 8*Power(t1,5)*t2 + 23*Power(t1,6)*t2 + 
            2*Power(t1,7)*t2 + 120*Power(t2,2) + 
            Power(s2,7)*Power(t2,2) - 147*t1*Power(t2,2) + 
            214*Power(t1,2)*Power(t2,2) + 61*Power(t1,3)*Power(t2,2) + 
            5*Power(t1,4)*Power(t2,2) - 57*Power(t1,5)*Power(t2,2) - 
            10*Power(t1,6)*Power(t2,2) - 49*Power(t2,3) - 
            130*t1*Power(t2,3) - 165*Power(t1,2)*Power(t2,3) - 
            50*Power(t1,3)*Power(t2,3) + 90*Power(t1,4)*Power(t2,3) + 
            21*Power(t1,5)*Power(t2,3) + 73*Power(t2,4) + 
            129*t1*Power(t2,4) + 93*Power(t1,2)*Power(t2,4) - 
            85*Power(t1,3)*Power(t2,4) - 24*Power(t1,4)*Power(t2,4) - 
            41*Power(t2,5) - 58*t1*Power(t2,5) + 
            45*Power(t1,2)*Power(t2,5) + 16*Power(t1,3)*Power(t2,5) + 
            11*Power(t2,6) - 13*t1*Power(t2,6) - 
            6*Power(t1,2)*Power(t2,6) + 2*Power(t2,7) + t1*Power(t2,7) + 
            Power(s1,7)*(s2 - t1)*(1 + 5*s2 - 5*t1 + 3*t2) - 
            Power(s2,6)*t2*(-2 - 5*t2 + Power(t2,2) + 3*t1*(2 + t2)) + 
            Power(s2,5)*(-1 + 22*t2 + 5*Power(t2,2) + 15*Power(t2,3) - 
               9*Power(t2,4) + Power(t1,2)*(5 + 28*t2) + 
               t1*(5 - 36*t2 - 31*Power(t2,2) + 12*Power(t2,3))) + 
            Power(s2,4)*(8 + 28*t2 + 31*Power(t2,2) + 29*Power(t2,3) + 
               13*Power(t2,4) - 11*Power(t2,5) + 
               5*Power(t1,3)*(-5 - 10*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-13 + 147*t2 + 64*Power(t2,2) - 
                  38*Power(t2,3)) + 
               t1*(-25 - 46*t2 - 127*Power(t2,2) - 39*Power(t2,3) + 
                  39*Power(t2,4))) + 
            Power(s2,3)*(9 + 23*t2 + 67*Power(t2,2) - 26*Power(t2,3) + 
               79*Power(t2,4) - Power(t2,5) - 4*Power(t2,6) + 
               Power(t1,4)*(50 + 40*t2 - 15*Power(t2,2)) + 
               Power(t1,3)*(2 - 268*t2 - 46*Power(t2,2) + 
                  52*Power(t2,3)) + 
               Power(t1,2)*(107 + 14*t2 + 408*Power(t2,2) + 
                  6*Power(t2,3) - 63*Power(t2,4)) + 
               t1*(-69 - 131*t2 - 14*Power(t2,2) - 259*Power(t2,3) + 
                  Power(t2,4) + 30*Power(t2,5))) + 
            Power(s2,2)*(-14 - 39*t2 + 97*Power(t2,2) + 21*Power(t2,3) - 
               38*Power(t2,4) + 83*Power(t2,5) - 6*Power(t2,6) + 
               Power(t1,5)*(-50 - 10*t2 + 9*Power(t2,2)) + 
               Power(t1,4)*(22 + 252*t2 - 11*Power(t2,2) - 
                  33*Power(t2,3)) + 
               Power(t1,3)*(-161 + 14*t2 - 512*Power(t2,2) + 
                  66*Power(t2,3) + 45*Power(t2,4)) + 
               Power(t1,2)*(149 + 220*t2 - 60*Power(t2,2) + 
                  521*Power(t2,3) - 65*Power(t2,4) - 27*Power(t2,5)) + 
               t1*(11 - 190*t2 - 92*Power(t2,2) + 60*Power(t2,3) - 
                  294*Power(t2,4) + 26*Power(t2,5) + 6*Power(t2,6))) + 
            s2*(-120 + 70*t2 + 7*Power(t2,2) + 117*Power(t2,3) - 
               94*Power(t2,4) + 18*Power(t2,5) + 32*Power(t2,6) - 
               2*Power(t2,7) + Power(t1,6)*(25 - 4*t2 - 2*Power(t2,2)) + 
               Power(t1,5)*(-23 - 120*t2 + 29*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,4)*(106 + 4*t2 + 283*Power(t2,2) - 
                  69*Power(t2,3) - 12*Power(t2,4)) + 
               Power(t1,3)*(-123 - 159*t2 + 38*Power(t2,2) - 
                  381*Power(t2,3) + 75*Power(t2,4) + 8*Power(t2,5)) + 
               Power(t1,2)*(-91 + 359*t2 - 36*Power(t2,2) + 
                  16*Power(t2,3) + 300*Power(t2,4) - 41*Power(t2,5) - 
                  2*Power(t2,6)) + 
               t1*(216 - 116*t2 - 319*Power(t2,2) + 183*Power(t2,3) - 
                  53*Power(t2,4) - 139*Power(t2,5) + 12*Power(t2,6))) + 
            Power(s1,6)*(-1 + 2*Power(s2,3) + 10*Power(t1,3) - 2*t2 + 
               3*Power(t2,2) - Power(t1,2)*(31 + 19*t2) + 
               t1*(-8 + 27*t2 + 5*Power(t2,2)) + 
               3*Power(s2,2)*(2*t1 - 5*(1 + t2)) - 
               s2*(5 + 18*Power(t1,2) + 13*t2 + 6*Power(t2,2) - 
                  2*t1*(23 + 17*t2))) + 
            Power(s1,5)*(-6*Power(s2,4) - 4*Power(t1,4) + 
               Power(s2,3)*(-8 + 34*t1 - 26*t2) + 
               Power(t1,3)*(2 - 21*t2) + 
               t2*(31 - 26*t2 - 5*Power(t2,2)) + 
               4*Power(t1,2)*(13 + 9*t2 + 8*Power(t2,2)) - 
               t1*(51 - 35*t2 + 34*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(11 - 54*Power(t1,2) - 2*t2 + 
                  11*Power(t2,2) + 5*t1*(6 + 7*t2)) + 
               s2*(54 + 30*Power(t1,3) + 12*Power(t1,2)*(-2 + t2) + 
                  15*t2 - 23*Power(t2,2) + 10*Power(t2,3) - 
                  t1*(73 + 22*t2 + 45*Power(t2,2)))) + 
            Power(s1,4)*(-39 - 2*Power(t1,5) + 
               Power(s2,4)*(38 + 4*t1 - t2) + 5*t2 - 15*Power(t2,2) + 
               43*Power(t2,3) + 6*Power(t2,4) + Power(t1,4)*(4 + 5*t2) + 
               Power(t1,3)*(121 - 12*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(52 - 272*t2 + 37*Power(t2,2) - 
                  31*Power(t2,3)) + 
               t1*(173 - 160*t2 + 94*Power(t2,2) - 33*Power(t2,3) + 
                  10*Power(t2,4)) - 
               Power(s2,3)*(58 + 10*Power(t1,2) - 38*t2 - 
                  45*Power(t2,2) + 2*t1*(53 + 14*t2)) + 
               Power(s2,2)*(7 + 6*Power(t1,3) - 12*t2 + 
                  18*Power(t2,2) + 13*Power(t2,3) + 
                  2*Power(t1,2)*(51 + 32*t2) + 
                  t1*(190 - 92*t2 - 85*Power(t2,2))) + 
               s2*(-128 + 2*Power(t1,4) + 56*t2 - 115*Power(t2,2) + 
                  119*Power(t2,3) - 16*Power(t2,4) - 
                  2*Power(t1,3)*(19 + 20*t2) + 
                  11*Power(t1,2)*(-23 + 6*t2 + 2*Power(t2,2)) + 
                  t1*(-67 + 331*t2 - 101*Power(t2,2) + 25*Power(t2,3)))) \
+ Power(s1,3)*(115 - Power(s2,6) - 5*Power(t1,6) + 
               5*Power(s2,5)*(-7 + 2*t1) - 160*t2 + 252*Power(t2,2) - 
               214*Power(t2,3) + 17*Power(t2,4) - 10*Power(t2,5) + 
               Power(t1,5)*(-47 + 26*t2) + 
               Power(t1,4)*(266 + 66*t2 - 39*Power(t2,2)) + 
               Power(t1,3)*(61 - 603*t2 - Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(t1,2)*(219 - 323*t2 + 468*Power(t2,2) - 
                  55*Power(t2,3) + 12*Power(t2,4)) - 
               t1*(52 + 420*t2 - 516*Power(t2,2) + 152*Power(t2,3) - 
                  47*Power(t2,4) + 7*Power(t2,5)) + 
               Power(s2,4)*(22 - 35*Power(t1,2) - 62*t2 + Power(t2,2) + 
                  11*t1*(11 + 2*t2)) + 
               Power(s2,3)*(35 + 60*Power(t1,3) + 162*t2 + 
                  Power(t2,2) - 50*Power(t2,3) - 
                  2*Power(t1,2)*(53 + 46*t2) + 
                  2*t1*(-175 + 74*t2 + 33*Power(t2,2))) - 
               Power(s2,2)*(-203 + 55*Power(t1,4) + 
                  Power(t1,3)*(58 - 144*t2) + 203*t2 - 6*Power(t2,2) - 
                  37*Power(t2,3) + 34*Power(t2,4) + 
                  2*Power(t1,2)*(-450 + 22*t2 + 87*Power(t2,2)) + 
                  t1*(37 + 825*t2 + Power(t2,2) - 133*Power(t2,3))) + 
               s2*(41 + 26*Power(t1,5) + 234*t2 - 134*Power(t2,2) + 
                  18*Power(t2,3) - 102*Power(t2,4) + 11*Power(t2,5) - 
                  25*Power(t1,4)*(-5 + 4*t2) + 
                  2*Power(t1,3)*(-419 - 54*t2 + 73*Power(t2,2)) + 
                  Power(t1,2)*
                   (-59 + 1266*t2 + Power(t2,2) - 96*Power(t2,3)) + 
                  t1*(-363 + 440*t2 - 504*Power(t2,2) + 84*Power(t2,3) + 
                     13*Power(t2,4)))) - 
            Power(s1,2)*(40 + 2*Power(t1,7) + Power(t1,6)*(35 - 16*t2) + 
               107*t2 - 142*Power(t2,2) + 188*Power(t2,3) - 
               213*Power(t2,4) + 27*Power(t2,5) - 7*Power(t2,6) + 
               Power(s2,6)*(-3 + 2*t1 + 2*t2) + 
               Power(t1,5)*(-52 - 129*t2 + 41*Power(t2,2)) + 
               Power(t1,4)*(36 + 189*t2 + 174*Power(t2,2) - 
                  46*Power(t2,3)) + 
               Power(t1,3)*(-491 + 464*t2 - 277*Power(t2,2) - 
                  112*Power(t2,3) + 22*Power(t2,4)) + 
               Power(t1,2)*(114 + 811*t2 - 969*Power(t2,2) + 
                  150*Power(t2,3) + 36*Power(t2,4) - 2*Power(t2,5)) - 
               t1*(69 + 94*t2 + 470*Power(t2,2) - 684*Power(t2,3) + 
                  37*Power(t2,4) - 3*Power(t2,5) + Power(t2,6)) - 
               Power(s2,5)*(13 + 12*Power(t1,2) - 17*t2 - 
                  22*Power(t2,2) + 2*t1*(7 + t2)) + 
               Power(s2,4)*(-84 + 30*Power(t1,3) + 
                  Power(t1,2)*(121 - 28*t2) - 104*t2 + 
                  128*Power(t2,2) + 23*Power(t2,3) + 
                  t1*(40 - 193*t2 - 49*Power(t2,2))) + 
               Power(s2,3)*(165 - 40*Power(t1,4) - 226*t2 - 
                  126*Power(t2,2) + 194*Power(t2,3) - 25*Power(t2,4) + 
                  4*Power(t1,3)*(-71 + 23*t2) + 
                  Power(t1,2)*(10 + 606*t2 - 26*Power(t2,2)) + 
                  t1*(256 + 137*t2 - 564*Power(t2,2) + 7*Power(t2,3))) + 
               Power(s2,2)*(255 + 30*Power(t1,5) + 
                  Power(t1,4)*(311 - 118*t2) + 369*t2 - 
                  597*Power(t2,2) - 168*Power(t2,3) + 111*Power(t2,4) - 
                  26*Power(t2,5) + 
                  2*Power(t1,3)*(-70 - 415*t2 + 76*Power(t2,2)) + 
                  Power(t1,2)*
                   (-224 + 227*t2 + 918*Power(t2,2) - 129*Power(t2,3)) \
+ t1*(-878 + 970*t2 + 15*Power(t2,2) - 492*Power(t2,3) + 91*Power(t2,4))\
) + s2*(51 - 12*Power(t1,6) + 57*t2 + 266*Power(t2,2) - 
                  216*Power(t2,3) - 164*Power(t2,4) - 12*Power(t2,5) + 
                  2*Power(t2,6) + 2*Power(t1,5)*(-83 + 35*t2) + 
                  Power(t1,4)*(155 + 529*t2 - 140*Power(t2,2)) + 
                  Power(t1,3)*
                   (16 - 449*t2 - 656*Power(t2,2) + 145*Power(t2,3)) + 
                  Power(t1,2)*
                   (1204 - 1208*t2 + 388*Power(t2,2) + 
                     410*Power(t2,3) - 88*Power(t2,4)) + 
                  t1*(-357 - 1085*t2 + 1388*Power(t2,2) + 
                     52*Power(t2,3) - 105*Power(t2,4) + 23*Power(t2,5)))) \
- s1*(88 - 2*Power(t1,7)*(-5 + t2) - 292*t2 + Power(s2,7)*t2 + 
               139*Power(t2,2) + 28*Power(t2,3) + 4*Power(t2,4) + 
               25*Power(t2,5) + 7*Power(t2,6) + Power(t2,7) + 
               Power(t1,6)*(10 - 56*t2 + 9*Power(t2,2)) - 
               Power(t1,5)*(56 + 20*t2 - 121*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,4)*(-21 + 206*t2 + 30*Power(t2,2) - 
                  134*Power(t2,3) + 14*Power(t2,4)) - 
               Power(t1,3)*(-252 + 70*t2 + 401*Power(t2,2) + 
                  53*Power(t2,3) - 85*Power(t2,4) + 6*Power(t2,5)) + 
               Power(t1,2)*(-409 + 230*t2 - 197*Power(t2,2) + 
                  507*Power(t2,3) + 53*Power(t2,4) - 31*Power(t2,5) + 
                  Power(t2,6)) + 
               t1*(10 + 502*t2 - 556*Power(t2,2) + 284*Power(t2,3) - 
                  281*Power(t2,4) - 27*Power(t2,5) + 4*Power(t2,6)) - 
               Power(s2,6)*(1 + 6*t2 + 4*Power(t2,2) + t1*(-6 + 4*t2)) + 
               Power(s2,5)*(11 + 35*t2 - 34*Power(t2,2) - 
                  27*Power(t2,3) + Power(t1,2)*(-40 + 7*t2) + 
                  t1*(1 + 66*t2 + 25*Power(t2,2))) + 
               Power(s2,4)*(21 - 10*Power(t1,3)*(-11 + t2) + 118*t2 + 
                  116*Power(t2,2) - 86*Power(t2,3) - 32*Power(t2,4) + 
                  Power(t1,2)*(16 - 260*t2 - 51*Power(t2,2)) + 
                  t1*(-64 - 218*t2 + 259*Power(t2,2) + 96*Power(t2,3))) + 
               Power(s2,3)*(35 + 12*t2 + 147*Power(t2,2) + 
                  216*Power(t2,3) - 104*Power(t2,4) - 4*Power(t2,5) + 
                  5*Power(t1,4)*(-32 + 3*t2) + 
                  Power(t1,3)*(-54 + 500*t2 + 34*Power(t2,2)) - 
                  2*Power(t1,2)*
                   (-91 - 232*t2 + 347*Power(t2,2) + 55*Power(t2,3)) + 
                  t1*(-131 - 382*t2 - 528*Power(t2,2) + 
                     430*Power(t2,3) + 65*Power(t2,4))) + 
               Power(s2,2)*(-263 + 68*t2 + 59*Power(t2,2) + 
                  147*Power(t2,3) + 228*Power(t2,4) - 55*Power(t2,5) + 
                  6*Power(t2,6) - 2*Power(t1,5)*(-65 + 8*t2) + 
                  Power(t1,4)*(71 - 510*t2 + 14*Power(t2,2)) + 
                  Power(t1,3)*
                   (-272 - 434*t2 + 868*Power(t2,2) + 24*Power(t2,3)) + 
                  Power(t1,2)*
                   (178 + 616*t2 + 738*Power(t2,2) - 736*Power(t2,3) - 
                     20*Power(t2,4)) - 
                  t1*(-134 + 48*t2 + 615*Power(t2,2) + 579*Power(t2,3) - 
                     303*Power(t2,4) + 8*Power(t2,5))) + 
               s2*(-110 - 192*t2 + 377*Power(t2,2) - 222*Power(t2,3) + 
                  90*Power(t2,4) + 129*Power(t2,5) - 8*Power(t2,6) + 
                  Power(t1,6)*(-56 + 9*t2) + 
                  Power(t1,5)*(-43 + 266*t2 - 27*Power(t2,2)) + 
                  Power(t1,4)*
                   (199 + 173*t2 - 520*Power(t2,2) + 33*Power(t2,3)) - 
                  Power(t1,3)*
                   (47 + 558*t2 + 356*Power(t2,2) - 526*Power(t2,3) + 
                     27*Power(t2,4)) + 
                  Power(t1,2)*
                   (-421 + 106*t2 + 869*Power(t2,2) + 416*Power(t2,3) - 
                     284*Power(t2,4) + 18*Power(t2,5)) + 
                  t1*(734 - 498*t2 + 273*Power(t2,2) - 604*Power(t2,3) - 
                     319*Power(t2,4) + 76*Power(t2,5) - 6*Power(t2,6))))))*
       T3(1 - s + s1 - t2,1 - s + s2 - t1))/
     ((-1 + s1)*(-s + s1 - t2)*Power(s1 - s2 + t1 - t2,2)*
       Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,2)*(-1 + t2)*
       (-1 + s2 - t1 + t2)) - (8*
       (-24 + 2*s2 - 8*Power(s2,2) + 8*Power(s2,3) - 3*Power(s2,4) - 
         2*t1 + 14*s2*t1 - 3*Power(s2,2)*t1 + 26*Power(s2,3)*t1 - 
         3*Power(s2,4)*t1 - 6*Power(t1,2) - 18*s2*Power(t1,2) - 
         59*Power(s2,2)*Power(t1,2) + 8*Power(s2,3)*Power(t1,2) - 
         2*Power(s2,4)*Power(t1,2) + 13*Power(t1,3) + 52*s2*Power(t1,3) - 
         6*Power(s2,2)*Power(t1,3) + 8*Power(s2,3)*Power(t1,3) - 
         16*Power(t1,4) - 12*Power(s2,2)*Power(t1,4) + Power(t1,5) + 
         8*s2*Power(t1,5) - 2*Power(t1,6) + 
         Power(s,7)*(t1 - t2)*(-1 + s1 + t1 - t2) + 
         Power(s1,4)*Power(s2 - t1,4)*(-1 + t2) + 96*t2 + 
         41*Power(s2,2)*t2 - 43*Power(s2,3)*t2 - 9*Power(s2,4)*t2 + 
         3*Power(s2,5)*t2 - 74*s2*t1*t2 + 79*Power(s2,2)*t1*t2 - 
         4*Power(s2,3)*t1*t2 - Power(s2,4)*t1*t2 + 4*Power(s2,5)*t1*t2 + 
         33*Power(t1,2)*t2 - 29*s2*Power(t1,2)*t2 + 
         59*Power(s2,2)*Power(t1,2)*t2 - 18*Power(s2,3)*Power(t1,2)*t2 - 
         15*Power(s2,4)*Power(t1,2)*t2 - 7*Power(t1,3)*t2 - 
         70*s2*Power(t1,3)*t2 + 30*Power(s2,2)*Power(t1,3)*t2 + 
         20*Power(s2,3)*Power(t1,3)*t2 + 24*Power(t1,4)*t2 - 
         17*s2*Power(t1,4)*t2 - 10*Power(s2,2)*Power(t1,4)*t2 + 
         3*Power(t1,5)*t2 + Power(t1,6)*t2 - 144*Power(t2,2) - 
         12*s2*Power(t2,2) - 67*Power(s2,2)*Power(t2,2) + 
         48*Power(s2,3)*Power(t2,2) + 24*Power(s2,4)*Power(t2,2) - 
         7*Power(s2,5)*Power(t2,2) - 2*Power(s2,6)*Power(t2,2) + 
         12*t1*Power(t2,2) + 122*s2*t1*Power(t2,2) - 
         108*Power(s2,2)*t1*Power(t2,2) - 49*Power(s2,3)*t1*Power(t2,2) + 
         24*Power(s2,4)*t1*Power(t2,2) + 6*Power(s2,5)*t1*Power(t2,2) - 
         55*Power(t1,2)*Power(t2,2) + 72*s2*Power(t1,2)*Power(t2,2) + 
         14*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         30*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         4*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         12*Power(t1,3)*Power(t2,2) + 23*s2*Power(t1,3)*Power(t2,2) + 
         16*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         4*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         12*Power(t1,4)*Power(t2,2) - 3*s2*Power(t1,4)*Power(t2,2) + 
         6*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         2*s2*Power(t1,5)*Power(t2,2) + 96*Power(t2,3) + 
         16*s2*Power(t2,3) + 43*Power(s2,2)*Power(t2,3) - 
         11*Power(s2,3)*Power(t2,3) - 17*Power(s2,4)*Power(t2,3) + 
         Power(s2,6)*Power(t2,3) - 16*t1*Power(t2,3) - 
         78*s2*t1*Power(t2,3) + 27*Power(s2,2)*t1*Power(t2,3) + 
         46*Power(s2,3)*t1*Power(t2,3) - 4*Power(s2,5)*t1*Power(t2,3) + 
         35*Power(t1,2)*Power(t2,3) - 21*s2*Power(t1,2)*Power(t2,3) - 
         41*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         6*Power(s2,4)*Power(t1,2)*Power(t2,3) + 
         5*Power(t1,3)*Power(t2,3) + 12*s2*Power(t1,3)*Power(t2,3) - 
         4*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         Power(s2,2)*Power(t1,4)*Power(t2,3) - 24*Power(t2,4) - 
         6*s2*Power(t2,4) - 9*Power(s2,2)*Power(t2,4) - 
         2*Power(s2,3)*Power(t2,4) + Power(s2,4)*Power(t2,4) + 
         6*t1*Power(t2,4) + 16*s2*t1*Power(t2,4) + 
         5*Power(s2,2)*t1*Power(t2,4) - 3*Power(s2,3)*t1*Power(t2,4) - 
         7*Power(t1,2)*Power(t2,4) - 4*s2*Power(t1,2)*Power(t2,4) + 
         3*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         Power(t1,3)*Power(t2,4) - s2*Power(t1,3)*Power(t2,4) + 
         Power(s,6)*(-1 + 4*Power(t1,3) - 3*t2 - 7*s2*t2 - 
            2*Power(t2,2) - 6*s2*Power(t2,2) + Power(t2,3) + 
            Power(s1,2)*(-2 + s2 - 3*t1 + 4*t2) - 
            Power(t1,2)*(8 + 4*s2 + 7*t2) + 
            t1*(7 + 4*s2 + 10*t2 + 10*s2*t2 + 2*Power(t2,2)) - 
            s1*(-3 + s2 + 5*t1 + 3*s2*t1 - 6*s2*t2 - 4*t1*t2 + 
               4*Power(t2,2))) + 
         Power(s1,3)*Power(s2 - t1,2)*
          (-6*Power(t1,3) + 8*Power(-1 + t2,2) + 
            2*Power(s2,3)*(2 + t2) + 
            Power(t1,2)*(3 + 10*t2 - Power(t2,2)) - 
            6*t1*(-3 + 2*t2 + Power(t2,2)) - 
            2*Power(s2,2)*(1 - 8*t2 + Power(t2,2) + t1*(7 + 2*t2)) + 
            s2*(2*Power(t1,2)*(8 + t2) + 6*(-3 + 2*t2 + Power(t2,2)) + 
               t1*(-1 - 26*t2 + 3*Power(t2,2)))) + 
         Power(s,5)*(9 + 6*Power(t1,4) + 
            Power(s1,3)*(4 - 2*s2 + 3*t1 - 5*t2) + 4*t2 + 18*s2*t2 + 
            18*Power(s2,2)*t2 - 5*Power(t2,2) + 9*s2*Power(t2,2) + 
            15*Power(s2,2)*Power(t2,2) - 6*s2*Power(t2,3) - 
            2*Power(t1,3)*(11 + 6*s2 + 4*t2) + 
            Power(t1,2)*(24 + 6*Power(s2,2) + 29*t2 - 2*Power(t2,2) + 
               28*s2*(1 + t2)) + 
            Power(s1,2)*(4 - 4*Power(s2,2) - 6*Power(t1,2) + 
               s2*(5 + 12*t1 - 19*t2) - 13*t2 + 6*Power(t2,2) + 
               5*t1*(2 + t2)) - 
            t1*(28 - 3*t2 + 7*Power(t2,2) - 4*Power(t2,3) + 
               Power(s2,2)*(6 + 20*t2) + 
               5*s2*(4 + 11*t2 + 2*Power(t2,2))) + 
            s1*(-17 - 9*Power(t1,3) + Power(s2,2)*(4 + 2*t1 - 15*t2) + 
               11*t2 + 8*Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,2)*(-4 + 21*t2) + 
               t1*(17 - 30*t2 - 10*Power(t2,2)) + 
               s2*(-3 + 7*Power(t1,2) + t1*(10 - 13*t2) + t2 + 
                  22*Power(t2,2)))) + 
         Power(s1,2)*(s2 - t1)*
          (Power(s2,5)*(-3 + t2) + 16*Power(-1 + t2,3) + 
            Power(t1,5)*(1 + t2) - 3*t1*Power(-1 + t2,2)*(5 + 3*t2) + 
            Power(s2,4)*(8 + 13*t1 - 2*t2 - 3*t1*t2 - 4*Power(t2,2)) - 
            Power(t1,4)*(-10 + 7*t2 + Power(t2,2)) + 
            Power(t1,3)*(20 - 30*t2 + 6*Power(t2,2)) + 
            Power(t1,2)*(1 - 16*t2 + 17*Power(t2,2) - 2*Power(t2,3)) + 
            Power(s2,3)*(-16 + 2*Power(t1,2)*(-11 + t2) + 31*t2 - 
               12*Power(t2,2) + Power(t2,3) + 
               t1*(-36 + 17*t2 + 11*Power(t2,2))) + 
            Power(s2,2)*(2*Power(t1,3)*(9 + t2) + 
               Power(t1,2)*(58 - 35*t2 - 11*Power(t2,2)) - 
               2*t1*(-24 + 42*t2 - 13*Power(t2,2) + Power(t2,3)) - 
               3*(-2 + 9*t2 - 8*Power(t2,2) + Power(t2,3))) + 
            s2*(3*Power(-1 + t2,2)*(5 + 3*t2) - Power(t1,4)*(7 + 3*t2) + 
               Power(t1,3)*(-40 + 27*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-52 + 83*t2 - 20*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-7 + 43*t2 - 41*Power(t2,2) + 5*Power(t2,3)))) + 
         s1*(-5*Power(t1,6) - 4*t1*(-7 + t2)*Power(-1 + t2,3) + 
            8*Power(-1 + t2,4) - Power(s2,6)*t2*(3 + 2*t2) - 
            Power(t1,2)*Power(-1 + t2,2)*(6 + t2 + Power(t2,2)) + 
            Power(t1,5)*(-16 + 11*t2 + Power(t2,2)) + 
            Power(t1,4)*(7 + 27*t2 - 12*Power(t2,2) - 2*Power(t2,3)) + 
            Power(t1,3)*(-16 + 61*t2 - 58*Power(t2,2) + 
               13*Power(t2,3)) + 
            Power(s2,5)*(t1*(3 + 19*t2 + 8*Power(t2,2)) + 
               2*(3 + 4*t2 - 6*Power(t2,2) + Power(t2,3))) - 
            Power(s2,4)*(16 - 45*t2 + 7*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(17 + 46*t2 + 12*Power(t2,2)) + 
               t1*(42 + 16*t2 - 45*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s2,3)*(20 - 74*t2 + 73*Power(t2,2) - 20*Power(t2,3) + 
               Power(t2,4) + Power(t1,3)*(38 + 54*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(106 - 11*t2 - 64*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(47 - 172*t2 + 35*Power(t2,2) + 10*Power(t2,3))) + 
            s2*(4*(-7 + t2)*Power(-1 + t2,3) + 
               Power(t1,5)*(23 + 7*t2) + 
               2*t1*Power(-1 + t2,2)*(6 + t2 + Power(t2,2)) + 
               Power(t1,4)*(72 - 41*t2 - 12*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,3)*(1 - 136*t2 + 45*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,2)*(52 - 196*t2 + 189*Power(t2,2) - 
                  46*Power(t2,3) + Power(t2,4))) - 
            Power(s2,2)*(Power(-1 + t2,2)*(6 + t2 + Power(t2,2)) + 
               Power(t1,4)*(42 + 31*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(126 - 49*t2 - 42*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(39 - 236*t2 + 61*Power(t2,2) + 
                  16*Power(t2,3)) + 
               t1*(56 - 209*t2 + 204*Power(t2,2) - 53*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         Power(s,4)*(-30 + 72*t1 - 69*Power(t1,2) + 40*Power(t1,3) - 
            28*Power(t1,4) + 4*Power(t1,5) + 20*t2 - 32*t1*t2 + 
            4*Power(t1,2)*t2 + 35*Power(t1,3)*t2 - 2*Power(t1,4)*t2 + 
            2*Power(t2,2) + 15*t1*Power(t2,2) - 
            5*Power(t1,2)*Power(t2,2) - 8*Power(t1,3)*Power(t2,2) - 
            9*Power(t2,3) - 3*t1*Power(t2,3) + 
            6*Power(t1,2)*Power(t2,3) + Power(t2,4) + 
            Power(s1,4)*(-2 + s2 - t1 + 2*t2) + 
            Power(s2,3)*(-4*Power(t1,2) + 4*t1*(1 + 5*t2) - 
               2*t2*(11 + 10*t2)) + 
            Power(s1,3)*(-11 + 7*Power(s2,2) + 8*Power(t1,2) + 18*t2 - 
               3*Power(t2,2) - t1*(1 + 14*t2) + s2*(-9 - 15*t1 + 20*t2)) \
+ Power(s2,2)*(6 + 12*Power(t1,3) - 38*t2 - 18*Power(t2,2) + 
               15*Power(t2,3) - 6*Power(t1,2)*(6 + 7*t2) + 
               t1*(18 + 111*t2 + 20*Power(t2,2))) + 
            s2*(-14 - 12*Power(t1,4) - 41*t2 + 23*Power(t2,2) + 
               12*Power(t1,3)*(5 + 2*t2) + 
               2*Power(t1,2)*(-29 - 62*t2 + 4*Power(t2,2)) + 
               t1*(64 + 32*t2 + 19*Power(t2,2) - 20*Power(t2,3))) + 
            Power(s1,2)*(-1 + 6*Power(s2,3) + 2*Power(t1,3) + 
               Power(t1,2)*(39 - 12*t2) + t2 - 9*Power(t2,2) + 
               Power(t2,3) + Power(s2,2)*(-3 - 16*t1 + 37*t2) + 
               t1*(2 - 20*t2 + 19*Power(t2,2)) + 
               s2*(-11 + 8*Power(t1,2) + 50*t2 - 28*Power(t2,2) - 
                  t1*(44 + 21*t2))) + 
            s1*(44 - 17*Power(t1,4) - 55*t2 + 34*Power(t2,2) - 
               3*Power(t2,3) + 2*Power(s2,3)*(-3 + t1 + 10*t2) + 
               Power(t1,3)*(26 + 27*t2) + 
               Power(t1,2)*(47 - 105*t2 - 3*Power(t2,2)) + 
               t1*(-49 + 7*t2 + 27*Power(t2,2) - 7*Power(t2,3)) - 
               Power(s2,2)*(10 + 21*Power(t1,2) + 6*t2 + 
                  50*Power(t2,2) - 6*t1*(1 + 2*t2)) + 
               s2*(33 + 36*Power(t1,3) - 6*t2 - 43*Power(t2,2) + 
                  10*Power(t2,3) - Power(t1,2)*(26 + 59*t2) + 
                  3*t1*(-14 + 43*t2 + 16*Power(t2,2))))) + 
         Power(s,3)*(43 - 113*t1 + 149*Power(t1,2) - 65*Power(t1,3) + 
            41*Power(t1,4) - 17*Power(t1,5) + Power(t1,6) - 81*t2 + 
            76*t1*t2 - 95*Power(t1,2)*t2 - 21*Power(t1,3)*t2 + 
            16*Power(t1,4)*t2 + 2*Power(t1,5)*t2 + 51*Power(t2,2) - 
            t1*Power(t2,2) + 63*Power(t1,2)*Power(t2,2) + 
            7*Power(t1,3)*Power(t2,2) - 7*Power(t1,4)*Power(t2,2) - 
            15*Power(t2,3) - 25*t1*Power(t2,3) - 
            9*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) + 
            2*Power(t2,4) + 3*t1*Power(t2,4) + 
            Power(s1,4)*(8 - 3*Power(s2,2) - 3*Power(t1,2) + 
               s2*(3 + 6*t1 - 7*t2) - 8*t2 + t1*(-3 + 7*t2)) + 
            Power(s2,4)*(Power(t1,2) - t1*(1 + 10*t2) + 
               t2*(13 + 15*t2)) - 
            Power(s2,3)*(8 + 4*Power(t1,3) - 36*t2 - 22*Power(t2,2) + 
               20*Power(t2,3) - 4*Power(t1,2)*(5 + 7*t2) + 
               t1*(4 + 103*t2 + 20*Power(t2,2))) + 
            s2*(60 - 4*Power(t1,5) + 15*t2 - 55*Power(t2,2) + 
               44*Power(t2,3) - 4*Power(t2,4) + 
               4*Power(t1,4)*(13 + t2) + 
               Power(t1,3)*(-86 - 109*t2 + 24*Power(t2,2)) + 
               3*t1*(-52 + 7*t2 - 14*Power(t2,2) + 3*Power(t2,3)) - 
               4*Power(t1,2)*
                (-28 - 23*t2 + 2*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s2,2)*(4 + 6*Power(t1,4) + 88*t2 - 32*Power(t2,2) - 
               6*Power(t1,3)*(9 + 4*t2) + 
               Power(t1,2)*(49 + 183*t2 - 12*Power(t2,2)) + 
               t1*(-39 - 107*t2 - 21*Power(t2,2) + 40*Power(t2,3))) + 
            Power(s1,3)*(-11 - 9*Power(s2,3) + 6*Power(t1,3) + 
               Power(s2,2)*(2 + 24*t1 - 32*t2) + 10*t2 + Power(t2,2) - 
               2*Power(t1,2)*(11 + 6*t2) - 
               2*t1*(12 - 23*t2 + 5*Power(t2,2)) + 
               s2*(37 - 21*Power(t1,2) - 60*t2 + 11*Power(t2,2) + 
                  4*t1*(5 + 11*t2))) + 
            Power(s1,2)*(-14 - 4*Power(s2,4) + 12*Power(t1,4) + 
               Power(s2,3)*(2 + 6*t1 - 38*t2) + 56*t2 - 
               46*Power(t2,2) + 4*Power(t2,3) - 
               9*Power(t1,3)*(-5 + 3*t2) + 
               Power(t1,2)*(-38 + 9*t2 + 22*Power(t2,2)) + 
               t1*(-80 + 81*t2 - 28*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*(-6 + 12*Power(t1,2) - 69*t2 + 
                  52*Power(t2,2) + t1*(61 + 37*t2)) + 
               s2*(59 - 26*Power(t1,3) - 72*t2 + 41*Power(t2,2) - 
                  4*Power(t2,3) + 4*Power(t1,2)*(-27 + 7*t2) + 
                  t1*(54 + 48*t2 - 72*Power(t2,2)))) + 
            s1*(-33 - 12*Power(t1,5) + Power(s2,4)*(4 - 3*t1 - 15*t2) + 
               61*t2 - 44*Power(t2,2) + 17*Power(t2,3) - Power(t2,4) + 
               2*Power(t1,4)*(27 + 5*t2) + 
               Power(t1,3)*(50 - 140*t2 + 11*Power(t2,2)) + 
               t1*(147 - 118*t2 + 65*Power(t2,2) - 10*Power(t2,3)) + 
               Power(t1,2)*(-98 + 37*Power(t2,2) - 9*Power(t2,3)) + 
               Power(s2,3)*(21*Power(t1,2) + 2*t1*(-14 + t2) + 
                  3*(6 + 5*t2 + 20*Power(t2,2))) - 
               Power(s2,2)*(10 + 45*Power(t1,3) + 57*t2 - 
                  93*Power(t2,2) + 20*Power(t2,3) - 
                  Power(t1,2)*(98 + 51*t2) + 
                  t1*(-33 + 222*t2 + 92*Power(t2,2))) + 
               s2*(-142 + 39*Power(t1,4) + 145*t2 - 98*Power(t2,2) + 
                  11*Power(t2,3) - 16*Power(t1,3)*(8 + 3*t2) + 
                  Power(t1,2)*(-101 + 347*t2 + 21*Power(t2,2)) + 
                  4*t1*(25 + 14*t2 - 30*Power(t2,2) + 7*Power(t2,3))))) + 
         Power(s,2)*(-60 + 91*t1 - 134*Power(t1,2) + 113*Power(t1,3) - 
            32*Power(t1,4) + 23*Power(t1,5) - 4*Power(t1,6) + 129*t2 - 
            191*t1*t2 + 116*Power(t1,2)*t2 - 69*Power(t1,3)*t2 - 
            26*Power(t1,4)*t2 - Power(t1,5)*t2 + Power(t1,6)*t2 + 
            Power(s2,5)*(-3 + 2*t1 - 6*t2)*t2 - 91*Power(t2,2) + 
            102*t1*Power(t2,2) - 38*Power(t1,2)*Power(t2,2) + 
            61*Power(t1,3)*Power(t2,2) + 11*Power(t1,4)*Power(t2,2) - 
            2*Power(t1,5)*Power(t2,2) + 35*Power(t2,3) - 
            7*t1*Power(t2,3) - 23*Power(t1,2)*Power(t2,3) - 
            9*Power(t1,3)*Power(t2,3) + Power(t1,4)*Power(t2,3) - 
            13*Power(t2,4) + 5*t1*Power(t2,4) + 
            3*Power(t1,2)*Power(t2,4) + 
            Power(s1,4)*(s2 - t1)*
             (3*Power(s2,2) + t1 + 3*Power(t1,2) + 16*(-1 + t2) - 
               9*t1*t2 + s2*(-1 - 6*t1 + 9*t2)) + 
            Power(s2,3)*(2 - 70*t2 + 8*Power(t2,2) + 
               8*Power(t1,3)*(2 + t2) + 
               2*Power(t1,2)*(-10 - 55*t2 + 4*Power(t2,2)) + 
               t1*(-2 + 110*t2 + 19*Power(t2,2) - 40*Power(t2,3))) - 
            Power(s2,4)*(Power(t1,2)*(4 + 7*t2) + 
               t1*(1 - 43*t2 - 10*Power(t2,2)) + 
               3*(-1 + 5*t2 + 6*Power(t2,2) - 5*Power(t2,3))) + 
            Power(s2,2)*(-9 - 111*t2 + 116*Power(t2,2) - 
               78*Power(t2,3) + 6*Power(t2,4) - 
               2*Power(t1,4)*(12 + t2) + 
               Power(t1,3)*(66 + 102*t2 - 24*Power(t2,2)) + 
               t1*(111 + 51*t2 + 63*Power(t2,2) - 9*Power(t2,3)) + 
               Power(t1,2)*(-37 - 201*t2 + 27*Power(t2,2) + 
                  36*Power(t2,3))) - 
            s2*(76 + 2*Power(t1,5)*(-8 + t2) - 159*t2 + 
               84*Power(t2,2) - 7*Power(t2,3) + 6*Power(t2,4) + 
               Power(t1,4)*(68 + 31*t2 - 14*Power(t2,2)) - 
               2*Power(t1,2)*
                (-113 + 44*t2 - 66*Power(t2,2) + 9*Power(t2,3)) + 
               Power(t1,3)*(-68 - 132*t2 + 39*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(-144 + 12*t2 + 67*Power(t2,2) - 96*Power(t2,3) + 
                  9*Power(t2,4))) + 
            Power(s1,3)*(5*Power(s2,4) + 24*Power(-1 + t2,2) - 
               Power(t1,3)*(31 + 2*t2) + 
               Power(s2,3)*(11 - 15*t1 + 26*t2) - 
               12*Power(t1,2)*(1 - 4*t2 + Power(t2,2)) - 
               4*t1*(-3 + 2*t2 + Power(t2,2)) + 
               Power(s2,2)*(-43 + 15*Power(t1,2) + 82*t2 - 
                  15*Power(t2,2) - t1*(53 + 54*t2)) + 
               s2*(-5*Power(t1,3) + Power(t1,2)*(73 + 30*t2) + 
                  4*(-3 + 2*t2 + Power(t2,2)) + 
                  t1*(55 - 130*t2 + 27*Power(t2,2)))) + 
            Power(s1,2)*(Power(s2,5) + 9*Power(t1,5) + 
               Power(t1,4)*(22 - 19*t2) - 7*Power(-1 + t2,2)*(7 + t2) + 
               Power(s2,4)*(-8 + 3*t1 + 22*t2) + 
               Power(t1,3)*(-76 + 33*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(-161 + 173*t2 - 35*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(-61 + 128*t2 - 77*Power(t2,2) + 10*Power(t2,3)) - 
               Power(s2,3)*(-37 + 24*Power(t1,2) - 38*t2 + 
                  48*Power(t2,2) + 7*t1*(2 + 5*t2)) + 
               Power(s2,2)*(-115 + 44*Power(t1,3) + 
                  Power(t1,2)*(74 - 15*t2) + 156*t2 - 67*Power(t2,2) + 
                  6*Power(t2,3) + t1*(-172 - 15*t2 + 102*Power(t2,2))) \
- s2*(-66 + 33*Power(t1,4) + Power(t1,3)*(74 - 47*t2) + 139*t2 - 
                  84*Power(t2,2) + 11*Power(t2,3) + 
                  Power(t1,2)*(-211 + 56*t2 + 66*Power(t2,2)) + 
                  t1*(-272 + 321*t2 - 98*Power(t2,2) + 9*Power(t2,3)))) \
+ s1*(-3*Power(t1,6) - 3*Power(t1,5)*(-13 + t2) + 
               Power(s2,5)*(-1 + t1 + 6*t2) - 
               Power(-1 + t2,2)*(-68 - 45*t2 + Power(t2,2)) + 
               Power(t1,4)*(7 - 85*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(115 + 10*t2 + 16*Power(t2,2) - 
                  13*Power(t2,3)) + 
               Power(t1,3)*(-133 + 34*t2 + 26*Power(t2,2) - 
                  5*Power(t2,3)) + 
               t1*(-98 + 303*t2 - 250*Power(t2,2) + 47*Power(t2,3) - 
                  2*Power(t2,4)) - 
               Power(s2,4)*(9 + 7*Power(t1,2) + 19*t2 + 
                  40*Power(t2,2) + t1*(-23 + 8*t2)) + 
               Power(s2,3)*(-5 + 18*Power(t1,3) + 96*t2 - 
                  101*Power(t2,2) + 20*Power(t2,3) - 
                  3*Power(t1,2)*(34 + 3*t2) + 
                  t1*(-5 + 196*t2 + 88*Power(t2,2))) + 
               s2*(102 + 13*Power(t1,5) - 316*t2 + 265*Power(t2,2) - 
                  54*Power(t2,3) + 3*Power(t2,4) - 
                  Power(t1,4)*(137 + t2) + 
                  Power(t1,3)*(-37 + 328*t2 - 14*Power(t2,2)) + 
                  Power(t1,2)*
                   (247 + 21*t2 - 129*Power(t2,2) + 27*Power(t2,3)) + 
                  t1*(-191 + 6*t2 - 101*Power(t2,2) + 30*Power(t2,3))) - 
               Power(s2,2)*(-82 + 22*Power(t1,4) + 26*t2 - 
                  87*Power(t2,2) + 15*Power(t2,3) - 
                  Power(t1,3)*(178 + 15*t2) + 
                  Power(t1,2)*(-44 + 420*t2 + 45*Power(t2,2)) + 
                  t1*(109 + 151*t2 - 204*Power(t2,2) + 42*Power(t2,3))))) \
- s*(-62 + 26*t1 - 45*Power(t1,2) + 83*Power(t1,3) - 28*Power(t1,4) + 
            11*Power(t1,5) - 5*Power(t1,6) + 176*t2 - 130*t1*t2 + 
            53*Power(t1,2)*t2 - 100*Power(t1,3)*t2 + 7*Power(t1,4)*t2 + 
            6*Power(t1,5)*t2 + 2*Power(t1,6)*t2 - 156*Power(t2,2) - 
            Power(s2,6)*Power(t2,2) + 202*t1*Power(t2,2) + 
            9*Power(t1,2)*Power(t2,2) + 47*Power(t1,3)*Power(t2,2) - 
            18*Power(t1,4)*Power(t2,2) - 4*Power(t1,5)*Power(t2,2) + 
            32*Power(t2,3) - 118*t1*Power(t2,3) - 
            13*Power(t1,2)*Power(t2,3) + 7*Power(t1,3)*Power(t2,3) + 
            3*Power(t1,4)*Power(t2,3) + 10*Power(t2,4) + 
            20*t1*Power(t2,4) - 4*Power(t1,2)*Power(t2,4) - 
            Power(t1,3)*Power(t2,4) + 
            Power(s2,5)*t2*(-2 - 9*t2 + 6*Power(t2,2) + 2*t1*(3 + t2)) + 
            Power(s1,4)*Power(s2 - t1,2)*
             (-8 + Power(s2,2) + t1 + Power(t1,2) + 8*t2 - 5*t1*t2 + 
               s2*(-1 - 2*t1 + 5*t2)) + 
            Power(s2,4)*(1 - 16*t2 - 13*Power(t2,2) + 
               Power(t1,2)*(-5 - 22*t2 + 2*Power(t2,2)) + 
               t1*(-5 + 42*t2 + 16*Power(t2,2) - 20*Power(t2,3))) + 
            Power(s2,3)*(2 - 69*t2 + 87*Power(t2,2) - 60*Power(t2,3) + 
               4*Power(t2,4) + 
               Power(t1,3)*(20 + 28*t2 - 8*Power(t2,2)) + 
               t1*(20 + 43*t2 + 60*Power(t2,2) - 3*Power(t2,3)) + 
               2*Power(t1,2)*(2 - 60*t2 + 5*Power(t2,2) + 12*Power(t2,3))\
) + Power(s2,2)*(-9 - 29*t2 + 63*Power(t2,2) - 19*Power(t2,3) - 
               6*Power(t2,4) + 
               Power(t1,4)*(-30 - 12*t2 + 7*Power(t2,2)) - 
               2*Power(t1,3)*
                (-9 - 64*t2 + 18*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(-71 - 31*t2 - 99*Power(t2,2) + 
                  9*Power(t2,3)) - 
               3*t1*(-27 - 8*t2 + 35*Power(t2,2) - 39*Power(t2,3) + 
                  3*Power(t2,4))) - 
            s2*(2*Power(t1,5)*(-10 + t2 + Power(t2,2)) + 
               2*Power(-1 + t2,2)*(14 - 41*t2 + 11*Power(t2,2)) + 
               Power(t1,4)*(28 + 54*t2 - 23*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(t1,3)*(-78 + 3*t2 - 70*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(t1,2)*(166 - 145*t2 + 29*Power(t2,2) + 
                  64*Power(t2,3) - 6*Power(t2,4)) - 
               2*t1*(27 - 12*t2 - 36*Power(t2,2) + 16*Power(t2,3) + 
                  5*Power(t2,4))) + 
            Power(s1,3)*(s2 - t1)*
             (Power(s2,4) - Power(t1,4) + Power(t1,3)*(-20 + t2) + 
               32*Power(-1 + t2,2) + Power(s2,3)*(12 - 2*t1 + 11*t2) + 
               t1*(41 - 30*t2 - 11*Power(t2,2)) + 
               Power(t1,2)*(4 + 30*t2 - 6*Power(t2,2)) - 
               Power(s2,2)*(19 - 56*t2 + 9*Power(t2,2) + 
                  t1*(44 + 21*t2)) + 
               s2*(-41 + 2*Power(t1,3) + 30*t2 + 11*Power(t2,2) + 
                  Power(t1,2)*(52 + 9*t2) + 
                  t1*(15 - 86*t2 + 15*Power(t2,2)))) + 
            Power(s1,2)*(-2*Power(t1,6) - 
               2*t1*(-9 + t2)*Power(-1 + t2,2) + 24*Power(-1 + t2,3) + 
               Power(t1,5)*(-3 + 6*t2) + 
               Power(s2,5)*(-9 + 2*t1 + 7*t2) + 
               Power(s2,4)*(32 - 10*Power(t1,2) + t1*(29 - 18*t2) + 
                  4*t2 - 22*Power(t2,2)) + 
               Power(t1,4)*(50 - 24*t2 - 4*Power(t2,2)) - 
               Power(t1,3)*(-102 + 123*t2 - 22*Power(t2,2) + 
                  Power(t2,3)) - 
               8*Power(t1,2)*
                (-5 + 10*t2 - 6*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(-73 + 20*Power(t1,3) + 
                  6*Power(t1,2)*(-5 + t2) + 116*t2 - 47*Power(t2,2) + 
                  4*Power(t2,3) + 32*t1*(-5 + t2 + 2*Power(t2,2))) + 
               Power(s2,2)*(50 - 20*Power(t1,4) - 102*t2 + 
                  62*Power(t2,2) - 10*Power(t2,3) + 
                  Power(t1,3)*(6 + 20*t2) - 
                  2*Power(t1,2)*(-137 + 50*t2 + 33*Power(t2,2)) - 
                  3*t1*(-80 + 113*t2 - 36*Power(t2,2) + 3*Power(t2,3))) \
+ s2*(10*Power(t1,5) + Power(t1,4)*(7 - 21*t2) + 
                  2*(-9 + t2)*Power(-1 + t2,2) + 
                  4*Power(t1,3)*(-49 + 22*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*
                   (-269 + 346*t2 - 83*Power(t2,2) + 6*Power(t2,3)) + 
                  2*t1*(-45 + 91*t2 - 55*Power(t2,2) + 9*Power(t2,3)))) + 
            s1*(2*Power(t1,6)*(-5 + t2) + Power(s2,6)*t2 - 
               4*Power(-1 + t2,3)*(15 + t2) + 
               Power(t1,5)*(15 + 20*t2 - 3*Power(t2,2)) + 
               2*t1*Power(-1 + t2,2)*(-15 - 10*t2 + Power(t2,2)) - 
               Power(s2,5)*(1 + 3*t1*(-2 + t2) + 12*t2 + 
                  14*Power(t2,2)) + 
               Power(t1,4)*(83 - 41*t2 - 9*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(-27 - 92*t2 + 27*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(81 - 279*t2 + 240*Power(t2,2) - 
                  43*Power(t2,3) + Power(t2,4)) + 
               Power(s2,4)*(7 + 52*t2 - 55*Power(t2,2) + 
                  10*Power(t2,3) + Power(t1,2)*(-34 + 4*t2) + 
                  t1*(6 + 92*t2 + 42*Power(t2,2))) - 
               Power(s2,3)*(24 - 101*t2 - 16*Power(t2,2) + 
                  9*Power(t2,3) + Power(t1,3)*(-76 + 6*t2) + 
                  Power(t1,2)*(27 + 224*t2 + 39*Power(t2,2)) + 
                  4*t1*(25 + 26*t2 - 39*Power(t2,2) + 7*Power(t2,3))) + 
               Power(s2,2)*(89 - 305*t2 + 270*Power(t2,2) - 
                  57*Power(t2,3) + 3*Power(t2,4) + 
                  Power(t1,4)*(-84 + 9*t2) + 
                  5*Power(t1,3)*(11 + 48*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (262 + 11*t2 - 156*Power(t2,2) + 27*Power(t2,3)) + 
                  t1*(33 - 314*t2 - Power(t2,2) + 30*Power(t2,3))) - 
               s2*(Power(t1,5)*(-46 + 7*t2) + 
                  Power(t1,4)*(48 + 116*t2 - 9*Power(t2,2)) + 
                  2*Power(-1 + t2,2)*(-15 - 10*t2 + Power(t2,2)) + 
                  2*Power(t1,3)*
                   (126 - 41*t2 - 32*Power(t2,2) + 5*Power(t2,3)) + 
                  Power(t1,2)*
                   (-18 - 305*t2 + 42*Power(t2,2) + 29*Power(t2,3)) + 
                  2*t1*(85 - 292*t2 + 255*Power(t2,2) - 50*Power(t2,3) + 
                     2*Power(t2,4))))))*T4(1 - s + s2 - t1))/
     ((-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*
       Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,2)*(-1 + t2)*
       (-1 + s2 - t1 + t2)) - (8*
       (4 - 4*s2 + 11*Power(s2,2) - 3*Power(s2,3) - 9*t1 - 15*s2*t1 + 
         26*Power(s2,2)*t1 - 3*Power(s2,3)*t1 + 3*Power(t1,2) - 
         38*s2*Power(t1,2) + 7*Power(s2,2)*Power(t1,2) - 
         2*Power(s2,3)*Power(t1,2) + 15*Power(t1,3) - 5*s2*Power(t1,3) + 
         6*Power(s2,2)*Power(t1,3) + Power(t1,4) - 6*s2*Power(t1,4) + 
         2*Power(t1,5) - Power(s,5)*(t1 - t2)*(-1 + s1 + t1 - t2)*
          (1 - s2 + t1 - t2) - 15*t2 + 42*s2*t2 - 37*Power(s2,2)*t2 - 
         12*Power(s2,3)*t2 + 3*Power(s2,4)*t2 + t1*t2 + 78*s2*t1*t2 - 
         21*Power(s2,2)*t1*t2 - 2*Power(s2,3)*t1*t2 + 
         4*Power(s2,4)*t1*t2 - 33*Power(t1,2)*t2 + 59*s2*Power(t1,2)*t2 - 
         11*Power(s2,2)*Power(t1,2)*t2 - 11*Power(s2,3)*Power(t1,2)*t2 - 
         26*Power(t1,3)*t2 + 16*s2*Power(t1,3)*t2 + 
         9*Power(s2,2)*Power(t1,3)*t2 - 6*Power(t1,4)*t2 - 
         s2*Power(t1,4)*t2 - Power(t1,5)*t2 + 24*Power(t2,2) - 
         84*s2*Power(t2,2) + 15*Power(s2,2)*Power(t2,2) + 
         34*Power(s2,3)*Power(t2,2) - 5*Power(s2,4)*Power(t2,2) - 
         2*Power(s2,5)*Power(t2,2) + 34*t1*Power(t2,2) - 
         81*s2*t1*Power(t2,2) - 30*Power(s2,2)*t1*Power(t2,2) + 
         19*Power(s2,3)*t1*Power(t2,2) + 4*Power(s2,4)*t1*Power(t2,2) + 
         48*Power(t1,2)*Power(t2,2) - 19*s2*Power(t1,2)*Power(t2,2) - 
         22*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         15*Power(t1,3)*Power(t2,2) + 7*s2*Power(t1,3)*Power(t2,2) - 
         4*Power(s2,2)*Power(t1,3)*Power(t2,2) + Power(t1,4)*Power(t2,2) + 
         2*s2*Power(t1,4)*Power(t2,2) - 22*Power(t2,3) + 
         56*s2*Power(t2,3) + 30*Power(s2,2)*Power(t2,3) - 
         24*Power(s2,3)*Power(t2,3) - 3*Power(s2,4)*Power(t2,3) + 
         Power(s2,5)*Power(t2,3) - 34*t1*Power(t2,3) + 
         3*s2*t1*Power(t2,3) + 39*Power(s2,2)*t1*Power(t2,3) + 
         4*Power(s2,3)*t1*Power(t2,3) - 3*Power(s2,4)*t1*Power(t2,3) - 
         17*Power(t1,2)*Power(t2,3) - 15*s2*Power(t1,2)*Power(t2,3) + 
         Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         3*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         2*s2*Power(t1,3)*Power(t2,3) - 
         Power(s2,2)*Power(t1,3)*Power(t2,3) + 12*Power(t2,4) - 
         8*s2*Power(t2,4) - 20*Power(s2,2)*Power(t2,4) + 
         Power(s2,3)*Power(t2,4) + Power(s2,4)*Power(t2,4) + 
         7*t1*Power(t2,4) + 16*s2*t1*Power(t2,4) - 
         2*Power(s2,2)*t1*Power(t2,4) - 2*Power(s2,3)*t1*Power(t2,4) - 
         Power(t1,2)*Power(t2,4) + s2*Power(t1,2)*Power(t2,4) + 
         Power(s2,2)*Power(t1,2)*Power(t2,4) - 3*Power(t2,5) - 
         2*s2*Power(t2,5) + Power(s2,2)*Power(t2,5) + t1*Power(t2,5) - 
         s2*t1*Power(t2,5) + Power(s1,4)*Power(s2 - t1,2)*
          (Power(-1 + t2,2) + s2*(3 + t2) - t1*(3 + t2)) + 
         Power(s,4)*(1 - 8*t1 - Power(t1,2) + 4*Power(t1,3) - 
            2*Power(t1,4) + 4*t2 + 2*t1*t2 - 11*Power(t1,2)*t2 + 
            5*Power(t1,3)*t2 - Power(t2,2) + 10*t1*Power(t2,2) - 
            3*Power(t1,2)*Power(t2,2) - 3*Power(t2,3) - t1*Power(t2,3) + 
            Power(t2,4) + Power(s1,2)*
             (2 + Power(s2,2) + 3*Power(t1,2) + t1*(3 - 7*t2) - 4*t2 + 
               4*Power(t2,2) + s2*(-3 - 4*t1 + 5*t2)) + 
            Power(s2,2)*(-2*Power(t1,2) - t2*(5 + 4*t2) + 
               t1*(2 + 6*t2)) - 
            s1*(3 - 2*Power(t1,3) + Power(s2,2)*(1 + t1 - 4*t2) + t2 - 
               4*Power(t2,2) + 4*Power(t2,3) + 
               s2*(-4 + Power(t1,2) - t1*(-3 + t2) + 5*t2) + 
               Power(t1,2)*(-7 + 8*t2) + t1*(-6 + 11*t2 - 10*Power(t2,2))\
) + s2*(-1 + 4*Power(t1,3) + 2*t2 - 3*Power(t2,2) - 3*Power(t2,3) - 
               Power(t1,2)*(6 + 11*t2) + t1*(5 + 9*t2 + 10*Power(t2,2)))) \
+ Power(s1,3)*(s2 - t1)*(-6*Power(t1,3) - Power(t1,2)*Power(-3 + t2,2) + 
            2*Power(-1 + t2,3) + 2*Power(s2,3)*(2 + t2) + 
            t1*(19 - 13*t2 - 7*Power(t2,2) + Power(t2,3)) - 
            2*Power(s2,2)*(4 - 2*t2 + t1*(7 + 2*t2)) + 
            s2*(2*Power(t1,2)*(8 + t2) + t1*(17 - 10*t2 + Power(t2,2)) - 
               2*(7 - t2 - 7*Power(t2,2) + Power(t2,3)))) + 
         Power(s1,2)*(Power(s2,5)*(-3 + t2) + Power(-1 + t2,4) + 
            Power(t1,5)*(1 + t2) + 
            Power(s2,4)*(11 + t1*(13 - 3*t2) - 6*t2 - 3*Power(t2,2)) + 
            Power(t1,4)*(11 - 7*t2 - 2*Power(t2,2)) + 
            2*t1*Power(-1 + t2,2)*(-12 - 5*t2 + Power(t2,2)) + 
            Power(t1,2)*(29 - 42*t2 + 17*Power(t2,2) - 4*Power(t2,3)) + 
            Power(t1,3)*(28 - 37*t2 + 8*Power(t2,2) + Power(t2,3)) + 
            Power(s2,3)*(2*Power(t1,2)*(-11 + t2) + 
               t1*(-46 + 29*t2 + 9*Power(t2,2)) - 
               3*(4 - 5*t2 + Power(t2,3))) + 
            Power(s2,2)*(20 - 28*t2 + 17*Power(t2,2) - 10*Power(t2,3) + 
               Power(t2,4) + 2*Power(t1,3)*(9 + t2) + 
               Power(t1,2)*(70 - 47*t2 - 11*Power(t2,2)) + 
               5*t1*(10 - 13*t2 + 2*Power(t2,2) + Power(t2,3))) - 
            s2*(Power(t1,4)*(7 + 3*t2) + 
               Power(t1,3)*(46 - 31*t2 - 7*Power(t2,2)) + 
               Power(-1 + t2,2)*(-19 - 16*t2 + 3*Power(t2,2)) + 
               3*Power(t1,2)*(22 - 29*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               t1*(45 - 58*t2 + 22*Power(t2,2) - 10*Power(t2,3) + 
                  Power(t2,4)))) - 
         s1*(-5*Power(t1,5) + Power(s2,5)*t2*(3 + 2*t2) + 
            Power(-1 + t2,3)*(-8 - 5*t2 + Power(t2,2)) + 
            Power(t1,4)*(-21 + 16*t2 + Power(t2,2)) - 
            2*t1*Power(-1 + t2,2)*(10 - 5*t2 + 3*Power(t2,2)) - 
            Power(t1,3)*(9 - 54*t2 + 22*Power(t2,2) + 3*Power(t2,3)) + 
            Power(t1,2)*(-5 + 57*t2 - 69*Power(t2,2) + 15*Power(t2,3) + 
               2*Power(t2,4)) - 
            Power(s2,4)*(6 + 11*t2 - 13*Power(t2,2) + 
               t1*(3 + 16*t2 + 6*Power(t2,2))) + 
            Power(s2,3)*(22 - 43*t2 - 13*Power(t2,2) + 16*Power(t2,3) - 
               2*Power(t2,4) + 
               2*Power(t1,2)*(7 + 15*t2 + 3*Power(t2,2)) + 
               t1*(39 + 18*t2 - 42*Power(t2,2) + Power(t2,3))) + 
            s2*(Power(t1,4)*(18 + 7*t2) + 
               Power(t1,3)*(69 - 36*t2 - 18*Power(t2,2) + Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-16 + 7*t2 - 8*Power(t2,2) + Power(t2,3)) + 
               t1*(23 - 132*t2 + 134*Power(t2,2) - 20*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(t1,2)*(48 - 168*t2 + 42*Power(t2,2) + 
                  19*Power(t2,3) - Power(t2,4))) + 
            Power(s2,2)*(-12 + 59*t2 - 53*Power(t2,2) + 5*Power(t2,3) + 
               Power(t2,4) - 2*Power(t1,3)*(12 + 12*t2 + Power(t2,2)) + 
               Power(t1,2)*(-81 + 13*t2 + 46*Power(t2,2) - 
                  2*Power(t2,3)) + 
               t1*(-61 + 157*t2 - 7*Power(t2,2) - 32*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(s,3)*(5 + 15*t1 + 16*Power(t1,2) - Power(t1,3) + 
            8*Power(t1,4) - Power(t1,5) - 9*t2 - 51*t1*t2 - 
            11*Power(t1,2)*t2 - 21*Power(t1,3)*t2 + Power(t1,4)*t2 + 
            21*Power(t2,2) + 17*t1*Power(t2,2) + 
            18*Power(t1,2)*Power(t2,2) + 3*Power(t1,3)*Power(t2,2) - 
            5*Power(t2,3) - 5*t1*Power(t2,3) - 
            5*Power(t1,2)*Power(t2,3) + 2*t1*Power(t2,4) + 
            Power(s2,3)*(Power(t1,2) - t1*(1 + 6*t2) + t2*(7 + 6*t2)) - 
            Power(s1,3)*(8 + 2*Power(s2,2) + t1 + 3*Power(t1,2) - 9*t2 - 
               8*t1*t2 + 5*Power(t2,2) + s2*(-4 - 5*t1 + 7*t2)) + 
            Power(s2,2)*(-2 - 3*Power(t1,3) + 5*t2 + 6*Power(t2,2) + 
               2*Power(t2,3) + Power(t1,2)*(10 + 13*t2) - 
               t1*(5 + 29*t2 + 12*Power(t2,2))) + 
            s2*(3 + 3*Power(t1,4) + 2*Power(t2,2) + 9*Power(t2,3) - 
               4*Power(t2,4) - Power(t1,3)*(17 + 8*t2) + 
               Power(t1,2)*(6 + 43*t2 + 3*Power(t2,2)) + 
               t1*(-18 + 18*t2 - 35*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s1,2)*(18 - 2*Power(s2,3) + 
               Power(s2,2)*(3 + 6*t1 - 13*t2) + 
               3*Power(t1,2)*(-6 + t2) + 7*t2 - 11*Power(t2,2) + 
               6*Power(t2,3) + t1*(-36 + 26*t2 - 9*Power(t2,2)) + 
               s2*(-1 - 4*Power(t1,2) + 5*t2 - 5*Power(t2,2) + 
                  t1*(13 + 12*t2))) + 
            s1*(-15 + 6*Power(t1,4) - 4*t2 - 16*Power(t1,3)*t2 - 
               17*Power(t2,2) + 10*Power(t2,3) - 2*Power(t2,4) - 
               Power(s2,3)*(-2 + t1 + 6*t2) + 
               t1*(20 + 46*t2 - 36*Power(t2,2)) + 
               Power(s2,2)*(1 + t1 + 8*Power(t1,2) + 9*t2 - 6*t1*t2 + 
                  8*Power(t2,2)) + 
               Power(t1,2)*(-13 + 26*t2 + 12*Power(t2,2)) - 
               s2*(6 + 13*Power(t1,3) + Power(t1,2)*(3 - 28*t2) + 3*t2 + 
                  5*Power(t2,2) - 12*Power(t2,3) + 
                  t1*(-2 + 18*t2 + 27*Power(t2,2))))) + 
         Power(s,2)*(-2 - 24*t1 - 44*Power(t1,2) + 3*Power(t1,3) - 
            9*Power(t1,4) + 4*Power(t1,5) + 30*t2 + 112*t1*t2 + 
            41*Power(t1,2)*t2 + 21*Power(t1,3)*t2 - 8*Power(t1,4)*t2 - 
            Power(t1,5)*t2 + Power(s2,4)*(-3 + 2*t1 - 4*t2)*t2 - 
            58*Power(t2,2) - 101*t1*Power(t2,2) - 
            39*Power(t1,2)*Power(t2,2) + 2*Power(t1,3)*Power(t2,2) + 
            3*Power(t1,4)*Power(t2,2) + 39*Power(t2,3) + 
            37*t1*Power(t2,3) + 5*Power(t1,2)*Power(t2,3) - 
            3*Power(t1,3)*Power(t2,3) - 10*Power(t2,4) - 
            4*t1*Power(t2,4) + Power(t1,2)*Power(t2,4) + Power(t2,5) + 
            Power(s1,4)*(Power(s2,2) + Power(t1,2) + 
               2*Power(-1 + t2,2) - t1*(1 + 3*t2) + s2*(1 - 2*t1 + 3*t2)) \
- Power(s2,3)*(-3 + 8*t2 + 5*Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,2)*(4 + 5*t2) + t1*(1 - 26*t2 - 6*Power(t2,2))) + 
            Power(s2,2)*(-3 - 15*t2 + 8*Power(t2,2) - 12*Power(t2,3) + 
               6*Power(t2,4) + 3*Power(t1,3)*(4 + t2) + 
               Power(t1,2)*(-7 - 51*t2 + 3*Power(t2,2)) + 
               t1*(12 + 5*t2 + 30*Power(t2,2) - 12*Power(t2,3))) + 
            s2*(-2 + Power(t1,4)*(-12 + t2) - 17*t2 - 6*Power(t2,2) + 
               4*Power(t2,3) + Power(t2,4) + 
               Power(t1,3)*(17 + 36*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(-18 - 18*t2 - 27*Power(t2,2) + 
                  13*Power(t2,3)) + 
               t1*(46 - 23*t2 + 33*Power(t2,2) + 2*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s1,3)*(-5 + 3*Power(s2,3) - 2*Power(t1,3) - t2 + 
               9*Power(t2,2) - 3*Power(t2,3) + Power(t1,2)*(19 + 6*t2) + 
               Power(s2,2)*(-8*t1 + 13*t2) - 
               t1*(-20 + 7*t2 + Power(t2,2)) + 
               s2*(-6 + 7*Power(t1,2) - 13*t2 + 7*Power(t2,2) - 
                  19*t1*(1 + t2))) + 
            Power(s1,2)*(21 + Power(s2,4) - 5*Power(t1,4) - 20*t2 + 
               12*Power(s2,3)*t2 - 2*Power(t2,2) + Power(t2,4) + 
               5*Power(t1,3)*(-4 + 3*t2) + 
               Power(t1,2)*(-37 + 9*t2 - 17*Power(t2,2)) + 
               t1*(41 - 47*t2 + 8*Power(t2,2) + 6*Power(t2,3)) - 
               Power(s2,2)*(-4 + 8*Power(t1,2) + 5*Power(t2,2) + 
                  t1*(26 + 7*t2)) + 
               s2*(-26 + 12*Power(t1,3) + Power(t1,2)*(46 - 20*t2) + 
                  33*Power(t2,2) - 15*Power(t2,3) + 
                  t1*(41 - 21*t2 + 26*Power(t2,2)))) + 
            s1*(-16 + 3*Power(t1,5) + 11*t2 + 7*Power(t2,2) + 
               Power(t2,3) - 3*Power(t2,4) - 4*Power(t1,4)*(4 + t2) + 
               Power(s2,4)*(-1 + t1 + 4*t2) + 
               Power(t1,3)*(-41 + 63*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(13 + 90*t2 - 61*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-61 + 43*t2 - 32*Power(t2,2) + 17*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,3)*(-6 - 6*Power(t1,2) - 9*t2 - 14*Power(t2,2) + 
                  t1*(9 + t2)) + 
               Power(s2,2)*(-2 + 12*Power(t1,3) + 25*t2 - 
                  13*Power(t2,2) - 12*Power(t2,3) - 
                  Power(t1,2)*(31 + 18*t2) + 
                  t1*(-22 + 76*t2 + 30*Power(t2,2))) - 
               s2*(-33 + 10*Power(t1,4) + 8*t2 - 41*Power(t2,2) + 
                  36*Power(t2,3) - 6*Power(t2,4) - 
                  Power(t1,3)*(39 + 17*t2) + 
                  Power(t1,2)*(-69 + 130*t2 + 12*Power(t2,2)) + 
                  t1*(6 + 138*t2 - 97*Power(t2,2) + Power(t2,3))))) - 
         s*(7 - 24*t1 - 25*Power(t1,2) + 19*Power(t1,3) - 2*Power(t1,4) + 
            5*Power(t1,5) + 66*t1*t2 + 13*Power(t1,2)*t2 - 
            14*Power(t1,3)*t2 - 15*Power(t1,4)*t2 - 2*Power(t1,5)*t2 - 
            36*Power(t2,2) - Power(s2,5)*Power(t2,2) - 80*t1*Power(t2,2) + 
            8*Power(t1,2)*Power(t2,2) + 30*Power(t1,3)*Power(t2,2) + 
            6*Power(t1,4)*Power(t2,2) + 42*Power(t2,3) + 
            29*t1*Power(t2,3) - 28*Power(t1,2)*Power(t2,3) - 
            7*Power(t1,3)*Power(t2,3) - 11*Power(t2,4) + 
            10*t1*Power(t2,4) + 4*Power(t1,2)*Power(t2,4) - 
            2*Power(t2,5) - t1*Power(t2,5) + 
            Power(s2,4)*t2*(-2 - 4*t2 + 3*Power(t2,2) + t1*(6 + t2)) + 
            Power(s1,4)*(s2 - t1)*
             (-1 + Power(s2,2) + Power(t1,2) - 2*t2 + 3*Power(t2,2) - 
               4*t1*(2 + t2) + s2*(8 - 2*t1 + 4*t2)) + 
            Power(s2,3)*(1 - 20*t2 + 4*Power(t2,2) - 9*Power(t2,3) + 
               4*Power(t2,4) + Power(t1,2)*(-5 - 16*t2 + 3*Power(t2,2)) + 
               t1*(-5 + 26*t2 + 9*Power(t2,2) - 10*Power(t2,3))) + 
            Power(s2,2)*(7 - 28*t2 + 16*Power(t2,2) - 25*Power(t2,3) + 
               2*Power(t2,4) + Power(t1,3)*(15 + 12*t2 - 5*Power(t2,2)) + 
               Power(t1,2)*(8 - 61*t2 + 11*Power(t2,3)) + 
               t1*(32 - 18*t2 + 59*Power(t2,2) + Power(t2,3) - 
                  6*Power(t2,4))) + 
            s2*(-5 + 26*t2 - 35*Power(t2,2) + 42*Power(t2,3) - 
               30*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,4)*(-15 + 2*Power(t2,2)) - 
               Power(t1,3)*(1 - 52*t2 + 11*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(15 + 32*t2 - 49*Power(t2,2) + 64*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(t1,2)*(-52 + 52*t2 - 93*Power(t2,2) + 
                  15*Power(t2,3) + 2*Power(t2,4))) + 
            Power(s1,3)*(Power(s2,4) - Power(t1,4) + 
               Power(t1,3)*(-15 + 2*t2) + Power(-1 + t2,2)*(1 + 3*t2) + 
               Power(s2,3)*(5 - 2*t1 + 8*t2) + 
               Power(t1,2)*(-28 + t2 - 5*Power(t2,2)) + 
               t1*(25 - 22*t2 - 7*Power(t2,2) + 4*Power(t2,3)) - 
               Power(s2,2)*(29 + 5*t2 - 2*Power(t2,2) + 
                  t1*(25 + 14*t2)) + 
               s2*(-12 + 2*Power(t1,3) - 5*t2 + 22*Power(t2,2) - 
                  5*Power(t2,3) + Power(t1,2)*(35 + 4*t2) + 
                  t1*(57 + 4*t2 + 3*Power(t2,2)))) + 
            Power(s1,2)*(2*Power(t1,5) + Power(t1,4)*(7 - 6*t2) - 
               4*Power(-1 + t2,3)*(1 + t2) + 
               Power(s2,4)*(-3 + 2*t1 + 5*t2) + 
               Power(t1,3)*(-13 + 3*t2 + 6*Power(t2,2)) - 
               Power(t1,2)*(78 - 89*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               t1*(-88 + 95*t2 - 7*Power(t2,2) + Power(t2,3) - 
                  Power(t2,4)) - 
               Power(s2,3)*(-23 + 8*Power(t1,2) + 6*t2 + 9*Power(t2,2) + 
                  t1*(2 + 5*t2)) + 
               Power(s2,2)*(-27 + 12*Power(t1,3) + 
                  Power(t1,2)*(20 - 11*t2) + 24*t2 + 19*Power(t2,2) - 
                  12*Power(t2,3) + t1*(-65 + 19*t2 + 26*Power(t2,2))) + 
               s2*(57 - 8*Power(t1,4) - 43*t2 - 3*Power(t2,2) - 
                  13*Power(t2,3) + 2*Power(t2,4) + 
                  Power(t1,3)*(-22 + 17*t2) + 
                  Power(t1,2)*(55 - 16*t2 - 23*Power(t2,2)) + 
                  t1*(115 - 135*t2 + Power(t2,2) + 11*Power(t2,3)))) + 
            s1*(-2*Power(t1,5)*(-5 + t2) + Power(s2,5)*t2 + 
               Power(-1 + t2,3)*(29 + 5*t2 + Power(t2,2)) + 
               Power(t1,4)*(5 - 32*t2 + 5*Power(t2,2)) - 
               Power(s2,4)*(1 + 2*t1*(-3 + t2) + 7*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(-56 + 4*t2 + 30*Power(t2,2) - 4*Power(t2,3)) + 
               Power(t1,2)*(-18 + 108*t2 - 27*Power(t2,2) - 
                  12*Power(t2,3) + Power(t2,4)) + 
               t1*(-14 + 138*t2 - 135*Power(t2,2) + 8*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,3)*(-4 + 2*Power(t1,2)*(-14 + t2) + 41*t2 - 
                  27*Power(t2,2) - 4*Power(t2,3) + 
                  t1*(-7 + 61*t2 + 19*Power(t2,2))) - 
               Power(s2,2)*(19 + 4*Power(t1,3)*(-12 + t2) - 42*t2 - 
                  65*Power(t2,2) + 42*Power(t2,3) - 6*Power(t2,4) + 
                  Power(t1,2)*(-22 + 133*t2 + 6*Power(t2,2)) + 
                  t1*(31 + 102*t2 - 97*Power(t2,2) + 2*Power(t2,3))) + 
               s2*(15 - 108*t2 + 71*Power(t2,2) + 26*Power(t2,3) - 
                  4*Power(t2,4) + Power(t1,4)*(-36 + 5*t2) + 
                  Power(t1,3)*(-19 + 111*t2 - 9*Power(t2,2)) + 
                  Power(t1,2)*
                   (91 + 57*t2 - 100*Power(t2,2) + 10*Power(t2,3)) + 
                  t1*(29 - 143*t2 - 27*Power(t2,2) + 43*Power(t2,3) - 
                     6*Power(t2,4))))))*T5(1 - s2 + t1 - t2))/
     ((-1 + s1)*(-s + s1 - t2)*Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,2)*
       (-1 + t2)*(-1 + s2 - t1 + t2)));
   return a;
};
