#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_monte_miser.h>
#include <gsl/gsl_monte_vegas.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_pow_int.h>

#include "./h/nlo_functions.h"
#include "./h/born.h"
#include "./h/nlo.h"
#include "./h/ur.h"

#define Pi M_PI
#define Power gsl_pow_int


struct mc_pars 
{
    int N;
    double s;
    double mf;
    double om_min;
    double th_particles_min;
    double th_max;
    double th_gmu;
    double si_min;
    double om_s; //omega*
};

double mc_born(double *inv, size_t dim, void *params);
int mc_cuts(double *, struct mc_pars *);


double PhaseVol(double, double, double, double);

double t2_func(double, double, double, double, double);

double Gk(double, double, double, double, double, double);
double f_lambda(double, double, double);



int mc_integrate(struct mc_pars *pars, double (*myf)(double *,size_t, void *), double *res, double *err)
{
    double xl[4], xu[4];
    double si_min, si_max, t1_min, t1_max;
    double s = (pars->s);
    size_t N = (pars->N);
    size_t Nwarm = N/10;
    
    
    
    
       
    
    si_min = (pars->si_min);
    si_max = Power(sqrt(s)-1.,2);
    t1_min = (2.-s-sqrt(s*(s-2.)))/2.;
    t1_max = (2.-s+sqrt(s*(s-2.)))/2.;
    
    // s1
    xl[0] = si_min;
    xu[0] = si_max;
    // s2
    xl[1] = si_min;
    xu[1] = si_max;
    // t1
    xl[2] = t1_min;
    xu[2] = t1_max;
    // lambda
    xl[3] = 0.;
    xu[3] = 2.*Pi;
    
    // initialisation of random numbers generator
    const gsl_rng_type *T;
    gsl_rng *r;
    gsl_rng_env_setup ();
    T = gsl_rng_default;
    r = gsl_rng_alloc (T);
    
    gsl_monte_function func;
    gsl_monte_vegas_state *state;
    
    func.dim = 4;
    func.params = pars;
    
    func.f = myf;
    gsl_rng_set(r,time(NULL));
    
    state = gsl_monte_vegas_alloc(4);
    gsl_monte_vegas_integrate (&func, xl, xu, 4, Nwarm, r, state, res, err);
    gsl_monte_vegas_integrate (&func, xl, xu, 4, N, r, state, res, err);
    
    gsl_monte_vegas_free(state);
    
    gsl_rng_free (r);
    
    return 1;
};



int mc_integrate_wb(struct mc_pars *pars, double (*myf)(double *,size_t, void *), double *res, double *err)
{
    double xl[4], xu[4];
    double si_min, si_max, t1_min, t1_max;
    double s = (pars->s);
    size_t N = (pars->N);
    //size_t Nwarm = N/10;
    
    
    
    
       
    
    si_min = (pars->si_min);
    si_max = Power(sqrt(s)-1.,2);
    t1_min = (2.-s-sqrt(s*(s-2.)))/2.;
    t1_max = (2.-s+sqrt(s*(s-2.)))/2.;
    
    // s1
    xl[0] = si_min;
    xu[0] = si_max;
    // s2
    xl[1] = si_min;
    xu[1] = si_max;
    // t1
    xl[2] = t1_min;
    xu[2] = t1_max;
    // lambda
    xl[3] = 0.;
    xu[3] = 2.*Pi;
    
    // initialisation of random numbers generator
    const gsl_rng_type *T;
    gsl_rng *r;
    gsl_rng_env_setup ();
    T = gsl_rng_default;
    r = gsl_rng_alloc (T);
    
    gsl_monte_function func;
    gsl_monte_vegas_state *state;
    
    func.dim = 4;
    func.params = pars;
    
    
    gsl_rng_set(r,time(NULL));
    
    state = gsl_monte_vegas_alloc(4);
    func.f = mc_born;
    gsl_monte_vegas_integrate (&func, xl, xu, 4, N, r, state, res, err);
    func.f = myf;
    gsl_monte_vegas_integrate (&func, xl, xu, 4, N, r, state, res, err);
    
    gsl_monte_vegas_free(state);
    
    gsl_rng_free (r);
    
    return 1;
};




double mc_born(double *inv, size_t dim, void *params)
{
    (void)(dim);
    //double *pars = (double *)params;
    
    struct mc_pars *pars = (struct mc_pars *)params;
    
    double vars[6];
    double s1,s2,t1,lam; // variables
    double t2; // temporary variables
    double s, mf;
    double result;
    
        
    s1 = inv[0];
    s2 = inv[1];
    t1 = inv[2];
    lam = inv[3];
    
    s = pars->s;
    mf = pars->mf;   
           
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        
        vars[0] = s;
        vars[1] = s1;
        vars[2] = s2;
        vars[3] = t1;
        vars[4] = t2;
        vars[5] = mf;    
        
        if( mc_cuts(vars,pars) )//if( cuts(s,s1,s2,t1,t2) )
        {
            result = born(vars)*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );        
        }
        else result=0.;
    }
    else  result = 0.;
    
    return result;
};


double mc_ur_born(double *inv, size_t dim, void *params)
{
    (void)(dim);
    //double *pars = (double *)params;
    
    struct mc_pars *pars = (struct mc_pars *)params;
    
    double vars[6];
    double s1,s2,t1,lam; // variables
    double t2; // temporary variables
    double s, mf;
    double result;
    
        
    s1 = inv[0];
    s2 = inv[1];
    t1 = inv[2];
    lam = inv[3];
    
    s = pars->s;
    mf = pars->mf;   
           
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        
        vars[0] = s;
        vars[1] = s1;
        vars[2] = s2;
        vars[3] = t1;
        vars[4] = t2;
        vars[5] = mf;    
        
        if( mc_cuts(vars,pars) )//if( cuts(s,s1,s2,t1,t2) )
        {
            result = ur_born(vars)*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );        
        }
        else result=0.;
    }
    else  result = 0.;
    
    return result;
};

double mc_nlo1(double *inv, size_t dim, void *params)
{
    (void)(dim);
    //double *pars = (double *)params;
    
    struct mc_pars *pars = (struct mc_pars *)params;
    
    double vars[6];
    double s1,s2,t1,lam; // variables
    double t2; // temporary variables
    double s, mf;
    double result;
    
        
    s1 = inv[0];
    s2 = inv[1];
    t1 = inv[2];
    lam = inv[3];
    
    s = pars->s;
    mf = pars->mf;   
           
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        
        vars[0] = s;
        vars[1] = s1;
        vars[2] = s2;
        vars[3] = t1;
        vars[4] = t2;
        vars[5] = pars->om_s;    
        vars[6] = mf;    
        
        if( mc_cuts(vars,pars) )//if( cuts(s,s1,s2,t1,t2) )
        {
            result = (nlo1(vars)+nlo1_2(vars)+nlo3(vars))*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );        
        }
        else result=0.;
    }
    else  result = 0.;
    
    return result;
};


double mc_ur_nlo1(double *inv, size_t dim, void *params)
{
    (void)(dim);
    //double *pars = (double *)params;
    
    struct mc_pars *pars = (struct mc_pars *)params;
    
    double vars[6];
    double s1,s2,t1,lam; // variables
    double t2; // temporary variables
    double s;
    double result;
    
        
    s1 = inv[0];
    s2 = inv[1];
    t1 = inv[2];
    lam = inv[3];
    
    s = pars->s;
     
           
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        
        vars[0] = s;
        vars[1] = s1;
        vars[2] = s2;
        vars[3] = t1;
        vars[4] = t2;
        vars[5] = pars->om_s;    
        vars[6] = pars->mf; 
        
        if( mc_cuts(vars,pars) )//if( cuts(s,s1,s2,t1,t2) )
        {
            result = (ur_nlo1(vars))*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );        
        }
        else result=0.;
    }
    else  result = 0.;
    
    return result;
};


double mc_ur_nlo1_m0(double *inv, size_t dim, void *params)
{
    (void)(dim);
    //double *pars = (double *)params;
    
    struct mc_pars *pars = (struct mc_pars *)params;
    
    double vars[6];
    double s1,s2,t1,lam; // variables
    double t2; // temporary variables
    double s;
    double result;
    
        
    s1 = inv[0];
    s2 = inv[1];
    t1 = inv[2];
    lam = inv[3];
    
    s = pars->s;
     
           
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        
        vars[0] = s;
        vars[1] = s1;
        vars[2] = s2;
        vars[3] = t1;
        vars[4] = t2;
        vars[5] = pars->om_s;    
        vars[6] = pars->th_max; 
        
        if( mc_cuts(vars,pars) )//if( cuts(s,s1,s2,t1,t2) )
        {
            result = (ur_nlo1_m0(vars))*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );        
        }
        else result=0.;
    }
    else  result = 0.;
    
    return result;
};


double mc_nlo2(double *inv, size_t dim, void *params)
{
    (void)(dim);
    //double *pars = (double *)params;
    
    struct mc_pars *pars = (struct mc_pars *)params;
    
    double vars[6];
    double s1,s2,t1,lam; // variables
    double t2; // temporary variables
    double s, mf;
    double result;
    
        
    s1 = inv[0];
    s2 = inv[1];
    t1 = inv[2];
    lam = inv[3];
    
    s = pars->s;
    mf = pars->mf;   
           
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        
        vars[0] = s;
        vars[1] = s1;
        vars[2] = s2;
        vars[3] = t1;
        vars[4] = t2;
        vars[5] = mf;    
        
        if( mc_cuts(vars,pars) )
        {
            result = nlo2(vars)*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );        
        }
        else result=0.;
    }
    else  result = 0.;
    
    return result;
};



double PhaseVol(double s,double s1, double s2, double t1)
{
    return Pi/( 8.*sqrt(f_lambda(s,0.,0.)*f_lambda(s,s2,1.)) );
};



double t2_func(double s,double s1,double s2,double t1,double lam)
{
	return 1.+(2*sqrt(Gk(s,t1,s2,0.,0.,1.)*Gk(s1,s2,s,0.,1.,1.))*cos(lam) + (-1 + s1)*(-1 + s2)*(s2 - t1) + Power(s,2)*(-1 + t1) - s*(-1 + (2 + s1)*t1 + s2*(-4 + s1 + t1)) )/f_lambda(s,s2,1.)  ;
};


double Gk(double x,double y,double z,double u,double v,double w)
{
    return Power(v,2)*w + Power(u,2)*z + u*((w - x)*(-v + y) - (v + w + x + y)*z + Power(z,2)) + x*(y*(x + y - z) + w*(-y + z)) + v*(Power(w,2) + y*(-x + z) - w*(x + y + z)) ;
};


double f_lambda(double x, double y, double z)
{
    return Power(x-y-z,2)-4*y*z;
};







int mc_cuts(double *vars, struct mc_pars *p)
{
    double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];//, mf=vars[6];
    double Em  = p->om_min;                      //minimum energy of photon
    double THc = (p->th_particles_min)*(Pi/180.);    //minimum angle of photons and muons
    double TH1 = (p->th_gmu)*(Pi/180.);              //minimum angle between muons and photon
    //double E=sqrt(s)/2;                       //energy of inital photons
    double E1=(s-s2+1.)/2./sqrt(s); 
    double E2=(s-s1+1.)/2./sqrt(s);             //energy of muons
    double omega=(s1+s2-2.)/2./sqrt(s);         // energy of final photon
    
    return (omega > Em && fabs((s1-s2+2*(t1-t2))/(s1+s2-2))<cos(THc) && fabs((s-s1-1+2*t2)/sqrtl(pow(s-s1+1,2)-4*s))<cos(THc) && fabs((s-s2-1+2*t1)/sqrtl(pow(s-s2+1,2)-4*s))<cos(THc) && (2*E1*omega+1.-s1)/(2.*omega*sqrt(E1*E1-1.))<cos(TH1) && (2*E2*omega+1.-s2)/(2.*omega*sqrt(E2*E2-1.))<cos(TH1) );
};



