#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m213_5_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(Power(s1,5)*t2*(-14 - Power(s2,2) + 2*Power(t1,2) + 
            t1*(15 + t2) + s2*(1 + 3*t1 + t2)) + 
         (-1 + t1 - t2)*Power(t2,2)*
          (-(t1*(-2 + t2)) + Power(s,2)*(-2 + t1 + t2) + 
            t2*(9 + (-3 + s2)*t2) + 
            s*(3 + t1*(-3 + t2) + (-9 + s2)*t2 + Power(t2,2))) + 
         Power(s1,4)*(2 + (-5 + 14*s + 6*s2 + 5*s*s2 + Power(s2,2))*t2 + 
            7*Power(t1,3)*t2 + 
            (24 + s - 6*s2 - 3*s*s2 + 7*Power(s2,2))*Power(t2,2) + 
            (-5 + s + s2)*Power(t2,3) + 
            Power(t1,2)*(-2 + (-21 + s2)*t2 + 4*Power(t2,2)) - 
            t1*(2 + (-8 + 20*s + 15*s2 + 3*s*s2 - 4*Power(s2,2))*t2 + 
               (39 - 3*s + 8*s2)*Power(t2,2) + 2*Power(t2,3))) - 
         Power(s1,3)*(4 + (-11 + s - 14*Power(s,2) + 15*s2 + 8*s*s2 - 
               8*Power(s2,2))*t2 + 
            (29 + 10*s - 5*Power(s,2) - 56*s2 + 6*s*s2 + 23*Power(s2,2))*
             Power(t2,2) + (-20 + 8*s + 2*Power(s,2) + 9*s2 - 6*s*s2 + 
               11*Power(s2,2))*Power(t2,3) + 
            (-10 + 2*s + 5*s2)*Power(t2,4) + 
            Power(t1,3)*(2 + (34 + 7*s - 8*s2)*t2 + Power(t2,2)) + 
            Power(t1,2)*(-2 + 
               (-49 + 22*s + 2*Power(s,2) + s2 - 4*s*s2)*t2 + 
               (-15 - 3*s + 10*s2)*Power(t2,2) + 6*Power(t2,3)) - 
            t1*(6 + (-11*Power(s,2) + 2*s*(18 + s2) - 
                  4*(-5 + 7*s2 + Power(s2,2)))*t2 + 
               (9 - 4*Power(s,2) + 23*s2 + s*(7 + 6*s2))*Power(t2,2) + 
               3*(3 + 5*s2)*Power(t2,3) + Power(t2,4))) + 
         s1*t2*(-8 - 5*t2 + 8*s2*t2 + 53*Power(t2,2) - 
            21*s2*Power(t2,2) + 8*Power(s2,2)*Power(t2,2) - 
            36*Power(t2,3) + 70*s2*Power(t2,3) - 
            21*Power(s2,2)*Power(t2,3) + 18*Power(t2,4) - 
            18*s2*Power(t2,4) + Power(t1,3)*(4 + (-27 + 8*s2)*t2) + 
            Power(t1,2)*(-20 + (35 - 8*s2)*t2 + (3 + 7*s2)*Power(t2,2) - 
               8*(-1 + s2)*Power(t2,3)) + 
            t1*(24 + (3 - 8*s2)*t2 + 
               (2 - 22*s2 - 4*Power(s2,2))*Power(t2,2) + 
               (-6 - 7*s2 + 8*Power(s2,2))*Power(t2,3) + 
               8*(-1 + s2)*Power(t2,4)) + 
            Power(s,2)*(10 + 17*t2 - Power(t2,2) - Power(t2,3) + 
               Power(t1,2)*(5 + 3*t2) + t1*(-15 - 20*t2 + 2*Power(t2,2))) \
- s*(-9 + Power(t1,3)*(-6 + t2) + 2*(-8 + s2)*t2 + 
               (9 + 2*s2)*Power(t2,2) + (13 - 4*s2)*Power(t2,3) + 
               Power(t1,2)*(3 + (26 - 4*s2)*t2 - 2*Power(t2,2)) + 
               t1*(12 + (-11 + 2*s2)*t2 + (3 - 2*s2)*Power(t2,2) - 
                  3*Power(t2,3)))) + 
         Power(s1,2)*(2 + (24 - 22*s - 24*Power(s,2) - 8*s2 + 3*s*s2)*t2 - 
            (54 + 24*Power(s,2) + s*(4 - 11*s2) - 37*s2 + 16*Power(s2,2))*
             Power(t2,2) + (61 + 2*Power(s,2) + s*(10 - 3*s2) - 131*s2 + 
               43*Power(s2,2))*Power(t2,3) + 
            (-48 + 2*Power(s,2) + s*(7 - 3*s2) + 32*s2 + 5*Power(s2,2))*
             Power(t2,4) + (-5 + s + 3*s2)*Power(t2,5) + 
            Power(t1,3)*(2 + (23 + s - 8*s2)*t2 + 
               (28 + s - 8*s2)*Power(t2,2)) - 
            t1*(4 - (-35 + 26*Power(s,2) + s*(-4 + s2) + 8*s2)*t2 + 
               (36 - 27*Power(s,2) - 49*s2 - 8*Power(s2,2) + 
                  s*(27 + 4*s2))*Power(t2,2) + 
               (-7 + 2*Power(s,2) + s2 + 12*Power(s2,2) + 
                  s*(-10 + 3*s2))*Power(t2,3) + 
               (-23 + 3*s + 18*s2)*Power(t2,4)) - 
            Power(t1,2)*t2*(18 + 32*t2 + 20*Power(t2,2) + 
               Power(s,2)*(3 + 4*t2) + s2*(-8 + 6*t2 - 17*Power(t2,2)) + 
               s*(-25 - 26*t2 + 3*Power(t2,2) + 4*s2*(1 + t2))))))/
     ((-1 + s1)*s1*(-1 + s2)*(-1 + t1)*(-1 + s1 + t1 - t2)*(-1 + t2)*t2*
       Power(-s1 + t2,2)) + (8*
       (1 - (-1 + s)*Power(t1,5)*(-1 + s - t2) - 11*t2 + 4*s2*t2 - 
         2*s*s2*t2 + 13*Power(t2,2) + 2*s*Power(t2,2) + 
         Power(s,2)*Power(t2,2) - 14*s2*Power(t2,2) + 
         8*s*s2*Power(t2,2) + 3*Power(s2,2)*Power(t2,2) + 
         23*Power(t2,3) - 2*s*Power(t2,3) - 5*Power(s,2)*Power(t2,3) + 
         22*s2*Power(t2,3) - 9*Power(s2,2)*Power(t2,3) + 26*Power(t2,4) - 
         32*s*Power(t2,4) + 2*Power(s,2)*Power(t2,4) + 8*s2*Power(t2,4) + 
         6*s*s2*Power(t2,4) - 4*Power(s2,2)*Power(t2,4) - 4*Power(t2,5) + 
         4*s*Power(t2,5) - Power(s1,6)*
          (3*Power(t1,2) - s2*t1*(-4 + t2) + Power(s2,2)*(1 + t2)) + 
         Power(t1,4)*(6 + 4*Power(s,2) + (-2 + 3*s2)*t2 + 
            (3 + s2)*Power(t2,2) + s*(-10 + t2 - 3*s2*t2 - 3*Power(t2,2))\
) + Power(t1,3)*(-13 + (17 - 13*s2)*t2 - 
            2*(8 - 7*s2 + Power(s2,2))*Power(t2,2) - 
            (3 + 2*s2)*Power(t2,3) + 2*Power(s,2)*(-2 + Power(t2,2)) + 
            s*(16 + 4*(-2 + 3*s2)*t2 - (6 + s2)*Power(t2,2) + 
               3*Power(t2,3))) + 
         Power(t1,2)*(13 + 7*(-5 + 3*s2)*t2 + 
            (39 - 26*s2 + 4*Power(s2,2))*Power(t2,2) - 
            2*(-4 + 5*s2 + Power(s2,2))*Power(t2,3) + 
            (1 + s2)*Power(t2,4) + 
            Power(s,2)*(1 + 3*t2 - 10*Power(t2,2)) - 
            s*(10 + (-18 + 19*s2)*t2 + (8 - 20*s2)*Power(t2,2) - 
               (5 + 3*s2)*Power(t2,3) + Power(t2,4))) - 
         t1*(6 + (-32 + 15*s2)*t2 + 
            (39 - 25*s2 + 5*Power(s2,2))*Power(t2,2) + 
            (18 + 37*s2 - 16*Power(s2,2))*Power(t2,3) + 
            (1 - 9*s2)*Power(t2,4) + 
            Power(s,2)*t2*(2 - 6*t2 - 4*Power(t2,2) + Power(t2,3)) - 
            s*(2 + 12*(-1 + s2)*t2 + (16 - 29*s2)*Power(t2,2) + 
               (42 - 22*s2)*Power(t2,3) + (-6 + s2)*Power(t2,4))) + 
         Power(s1,5)*(Power(s2,2)*
             (-2 + Power(t1,2) + 4*t2 + 3*Power(t2,2) - t1*(3 + 2*t2)) + 
            t1*(4 + Power(t1,3) - Power(t1,2)*(-5 + t2) + 5*t2 + 
               t1*(5 + 8*t2) + 
               s*(2 + 8*t1 + Power(t1,2) - 4*t2 - Power(t2,2))) - 
            s2*(-2 + 2*Power(t1,3) + Power(t1,2)*(12 + s - 3*t2) + 
               2*(-3 + s)*t2 + (1 + 2*s)*Power(t2,2) + 
               t1*(-13 + (-13 + s)*t2 + 2*Power(t2,2)))) + 
         Power(s1,4)*(-1 + 2*Power(t1,5) + Power(t1,4)*(17 + s - 3*t2) - 
            5*t2 - 2*Power(t2,2) + 6*s*Power(t2,2) - 
            Power(s,2)*Power(t2,2) + s*Power(t2,3) - 
            Power(s,2)*Power(t2,3) + 
            Power(t1,3)*(6 - Power(s,2) - 10*t2 + Power(t2,2) + 
               s*(8 + t2)) + Power(s2,2)*
             (2*Power(t1,3) - Power(t1,2)*(8 + t2) + t1*t2*(4 + 5*t2) + 
               t2*(15 - 5*t2 - 3*Power(t2,2))) + 
            Power(t1,2)*(19 + Power(s,2)*(-1 + t2) - 9*t2 - 
               3*Power(t2,2) - s*(16 + 17*t2 + 3*Power(t2,2))) - 
            s2*(-4 + 4*Power(t1,4) + Power(t1,3)*(1 + s - 4*t2) + 
               (21 + 2*s)*t2 + (19 - 9*s)*Power(t2,2) - 
               2*(1 + 2*s)*Power(t2,3) + 
               Power(t1,2)*(-36 + 3*s*(-3 + t2) - 23*t2 + 
                  3*Power(t2,2)) + 
               t1*(-9 + 2*(3 + s)*t2 + 4*(5 + s)*Power(t2,2) - 
                  Power(t2,3))) + 
            t1*(-13 - 23*t2 - 14*Power(t2,2) + Power(s,2)*t2*(2 + t2) + 
               s*(2 - 6*t2 + 12*Power(t2,2) + Power(t2,3)))) + 
         s1*(2 - 9*Power(t1,2) + 12*Power(t1,3) - 6*Power(t1,4) + 
            Power(t1,6) - 15*t2 + 11*t1*t2 + 45*Power(t1,2)*t2 - 
            62*Power(t1,3)*t2 + 26*Power(t1,4)*t2 - 5*Power(t1,5)*t2 - 
            42*Power(t2,2) + 6*t1*Power(t2,2) + 
            Power(t1,2)*Power(t2,2) - 3*Power(t1,3)*Power(t2,2) + 
            8*Power(t1,4)*Power(t2,2) - 29*Power(t2,3) - 
            67*t1*Power(t2,3) - 7*Power(t1,2)*Power(t2,3) - 
            5*Power(t1,3)*Power(t2,3) + 16*t1*Power(t2,4) + 
            Power(t1,2)*Power(t2,4) + 
            Power(s,2)*Power(t1 - t2,2)*
             (2 - t2 - 8*Power(t2,2) + Power(t1,2)*(-1 + 2*t2) + 
               t1*(-2 - 5*t2 + 2*Power(t2,2))) + 
            Power(s2,2)*t2*(-4 + Power(t1,4) - Power(t1,3)*(-5 + t2) + 
               21*t2 + 13*Power(t2,2) - 9*Power(t2,3) + 
               Power(t1,2)*(-11 - 8*t2 + Power(t2,2)) + 
               t1*(9 - 27*t2 + 4*Power(t2,2) + 3*Power(t2,3))) + 
            s*(-Power(t1,6) - 6*Power(t1,4)*(-1 + t2)*t2 + 
               Power(t1,5)*(1 + 4*t2) + 
               2*Power(t2,2)*(5 + 26*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(-6 + 50*t2 - 17*Power(t2,2) + 
                  4*Power(t2,3)) + 
               2*t1*(1 + t2 - 41*Power(t2,2) + 21*Power(t2,3) - 
                  5*Power(t2,4)) - 
               Power(t1,2)*(-4 + 64*t2 + 68*Power(t2,2) - 
                  20*Power(t2,3) + Power(t2,4))) + 
            s2*(-2 + Power(t1,5)*(-1 + s - t2) + (23 - 2*s)*t2 - 
               (47 + 15*s)*Power(t2,2) + (-77 + 6*s)*Power(t2,3) + 
               (1 + 17*s)*Power(t2,4) + 
               t1*(8 + (-48 + s)*t2 + (23 + 65*s)*Power(t2,2) + 
                  (4 - 31*s)*Power(t2,3) - 5*(1 + s)*Power(t2,4)) + 
               Power(t1,4)*(1 - 19*t2 + 3*Power(t2,2) - 2*s*(1 + t2)) + 
               Power(t1,3)*(5 + t2 + 23*Power(t2,2) - 3*Power(t2,3) + 
                  s*(3 - 19*t2 + 4*Power(t2,2))) + 
               Power(t1,2)*(-11 + 44*t2 + 79*Power(t2,2) - 
                  22*Power(t2,3) + Power(t2,4) + 
                  s*(-2 + 26*t2 + 3*Power(t2,2) + 2*Power(t2,3))))) + 
         Power(s1,3)*(-2 + Power(t1,6) + 17*t2 + 21*Power(t2,2) - 
            10*s*Power(t2,2) - 2*Power(s,2)*Power(t2,2) + 
            6*Power(t2,3) - 16*s*Power(t2,3) + 
            3*Power(s,2)*Power(t2,3) - s*Power(t2,4) + 
            Power(s,2)*Power(t2,4) - Power(t1,5)*(-11 + s + 2*t2) + 
            Power(t1,4)*(-13 - 2*Power(s,2) - 19*t2 + Power(t2,2) + 
               s*(-3 + 4*t2)) + 
            Power(t1,3)*(8 - 32*t2 + 3*Power(s,2)*t2 + 14*Power(t2,2) - 
               s*(47 + 5*Power(t2,2))) + 
            Power(s2,2)*(2 + Power(t1,4) + Power(t1,3)*(-9 + t2) + 
               2*t2 - 31*Power(t2,2) + 2*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*t2*(4 + t2) + t1*(1 + 15*t2 - 4*Power(t2,3))) \
+ t1*(-6 - 6*t2 + 17*Power(t2,2) + 8*Power(t2,3) + 
               Power(s,2)*t2*(4 - 6*t2 - 3*Power(t2,2)) + 
               s*(-4 + 18*t2 + 23*Power(t2,2) - 4*Power(t2,3))) + 
            Power(t1,2)*(-9 - 74*t2 + 11*Power(t2,2) - 2*Power(t2,3) + 
               Power(s,2)*(-2 + 3*t2 + Power(t2,2)) + 
               2*s*(-6 + 14*t2 + 4*Power(t2,2) + Power(t2,3))) - 
            s2*(2*Power(t1,5) - Power(t1,4)*(16 + s + t2) + 
               Power(t1,3)*(-39 + 5*s*(-3 + t2) + 4*t2 - Power(t2,2)) + 
               t1*(17 + (85 + 16*s)*t2 + (19 - 13*s)*Power(t2,2) - 
                  (11 + 7*s)*Power(t2,3)) + 
               Power(t1,2)*(9 + 29*t2 + 20*Power(t2,2) + 
                  s*(-3 + 8*t2 + Power(t2,2))) + 
               t2*(33 - 32*t2 - 23*Power(t2,2) + Power(t2,3) + 
                  s*(-4 - 17*t2 + 8*Power(t2,2) + 2*Power(t2,3))))) + 
         Power(s1,2)*(-((-2 + s)*Power(t1,6)) - 
            Power(t1,5)*(10 + Power(s,2) + s*(4 - 3*t2) + 6*t2) - 
            t2*(-18 + 4*(5 + 4*s)*t2 + 
               (16 - 5*s - 8*Power(s,2))*Power(t2,2) + 
               (4 - 9*s + Power(s,2))*Power(t2,3)) + 
            Power(t1,4)*(21 - 14*t2 + 6*Power(t2,2) + 
               Power(s,2)*(1 + 2*t2) + s*(-20 + 9*t2 - 3*Power(t2,2))) + 
            Power(t1,3)*(3*Power(s,2) - 
               2*t2*(-9 - 13*t2 + Power(t2,2)) + 
               s*(12 + 45*t2 - 16*Power(t2,2) + Power(t2,3))) + 
            Power(t1,2)*(-30 - 11*t2 + 52*Power(t2,2) - 23*Power(t2,3) - 
               2*Power(s,2)*t2*(-1 + 2*t2 + Power(t2,2)) + 
               s*(18 + 107*t2 - 25*Power(t2,2) + 13*Power(t2,3))) + 
            Power(s2,2)*(1 + Power(t1,4)*(-3 + t2) - 12*t2 - 
               13*Power(t2,2) + 27*Power(t2,3) + 
               Power(t1,3)*(3 + 2*t2 - Power(t2,2)) - 
               Power(t1,2)*(-1 - 22*t2 + 3*Power(t2,2) + Power(t2,3)) + 
               t1*(-2 + 2*t2 - 13*Power(t2,2) - 4*Power(t2,3) + 
                  Power(t2,4))) + 
            t1*(17 + 25*t2 + 104*Power(t2,2) - Power(t2,3) + 
               Power(t2,4) + Power(s,2)*Power(t2,2)*
                (-13 + 4*t2 + Power(t2,2)) - 
               s*(4 - 18*t2 + 64*Power(t2,2) + 25*Power(t2,3) + 
                  2*Power(t2,4))) + 
            s2*(-4 + Power(t1,5)*(9 + s - t2) + (13 + 4*s)*t2 + 
               (101 - 9*s)*Power(t2,2) - 2*(8 + 17*s)*Power(t2,3) + 
               (-10 + s)*Power(t2,4) + 
               Power(t1,4)*(3 + s*(7 - 3*t2) - 11*t2 + 2*Power(t2,2)) + 
               t1*(-1 + (68 - 10*s)*t2 + (55 + 27*s)*Power(t2,2) + 
                  (23 - 2*s)*Power(t2,3) - 2*s*Power(t2,4)) - 
               Power(t1,3)*(23 + 54*t2 - 8*Power(t2,2) + Power(t2,3) + 
                  s*(9 + 4*t2 - Power(t2,2))) + 
               Power(t1,2)*(2*
                   (8 - 48*t2 + 10*Power(t2,2) + Power(t2,3)) + 
                  s*(-1 - 44*t2 + 14*Power(t2,2) + 3*Power(t2,3))))))*
       B1(1 - s1 - t1 + t2,s1,t2))/
     ((-1 + s2)*(-1 + t1)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)) \
+ (8*(-(Power(s1,9)*t1*(-2 + s2 + t1)) + 
         (-1 + t1 - t2)*Power(t2,3)*
          (-(t1*(-2 + t2)) + Power(s,2)*(-2 + t1 + t2) + 
            t2*(9 + (-3 + s2)*t2) + 
            s*(3 + t1*(-3 + t2) + (-9 + s2)*t2 + Power(t2,2))) - 
         Power(s1,8)*(4*Power(t1,3) + 2*t2 + 2*Power(s2,2)*(1 + t2) - 
            2*Power(t1,2)*(3 + t2) + t1*(-11 + 8*t2 + s*(2 + t2)) + 
            s2*(6*Power(t1,2) - t2 - t1*(-7 + s + 3*t2))) + 
         Power(s1,7)*(-3*Power(t1,4) + Power(t1,3)*(36 + 3*s - t2) + 
            Power(t1,2)*(5 + Power(s,2) + s*(14 - 5*t2) + t2) + 
            t2*(-11 + 9*t2 + s*(2 + t2)) + 
            Power(s2,2)*(-6*t1*(1 + t2) + 2*t2*(7 + 4*t2)) + 
            s2*(4 - 13*Power(t1,3) + (11 - 5*s)*t2 - 
               (3 + 4*s)*Power(t2,2) + Power(t1,2)*(s + 19*t2) + 
               t1*(50 + 4*s + 42*t2 - 6*Power(t2,2))) + 
            t1*(-17 + Power(s,2)*(-2 + t2) - 51*t2 + 14*Power(t2,2) + 
               s*(-9 + 20*t2 + 2*Power(t2,2)))) - 
         s1*Power(t2,2)*(-3 - 45*t2 + 5*s2*t2 - 109*Power(t2,2) + 
            17*s2*Power(t2,2) - 2*Power(s2,2)*Power(t2,2) - 
            30*Power(t2,3) - 2*Power(s2,2)*Power(t2,3) + 
            25*Power(t2,4) - 11*s2*Power(t2,4) + 
            Power(t1,3)*(1 - 3*t2 + 2*Power(t2,2)) - 
            Power(t1,2)*(-1 + (-33 + 5*s2)*t2 + (16 + s2)*Power(t2,2) + 
               2*(-2 + s2)*Power(t2,3)) + 
            t1*(1 + 7*t2 + (45 - 6*s2 + 2*Power(s2,2))*Power(t2,2) + 
               14*(-1 + s2)*Power(t2,3) + 2*(-3 + s2)*Power(t2,4)) + 
            Power(s,2)*(8 + Power(t1,3)*(1 - 2*t2) + 16*t2 - 
               2*Power(t2,2) - 7*Power(t2,3) + Power(t1,2)*(1 + 9*t2) + 
               t1*(-10 - 17*t2 + Power(t2,2) + 2*Power(t2,3))) + 
            s*(-8 + (11 - 5*s2)*t2 + (85 - 15*s2)*Power(t2,2) + 
               (46 - 9*s2)*Power(t2,3) - 7*Power(t2,4) + 
               Power(t1,3)*(-2 + 5*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(-4 + (-28 + 3*s2)*t2 + 
                  (17 - 2*s2)*Power(t2,2)) + 
               t1*(14 + 2*(6 + s2)*t2 + (-50 + 8*s2)*Power(t2,2) + 
                  (-11 + 2*s2)*Power(t2,3) + 2*Power(t2,4)))) + 
         Power(s1,2)*t2*(3 - 45*t2 + 2*s2*t2 - 314*Power(t2,2) + 
            66*s2*Power(t2,2) - 6*Power(s2,2)*Power(t2,2) - 
            235*Power(t2,3) - 7*s2*Power(t2,3) - 
            2*Power(s2,2)*Power(t2,3) + 112*Power(t2,4) - 
            112*s2*Power(t2,4) + 14*Power(s2,2)*Power(t2,4) + 
            5*Power(t2,5) + 9*s2*Power(t2,5) + 
            Power(t1,4)*(-1 + 2*Power(t2,2)) - 
            Power(t1,3)*(4 + (-35 + 13*s2)*t2 + 
               (8 + 6*s2)*Power(t2,2) + 2*(-2 + s2)*Power(t2,3)) + 
            Power(t1,2)*(20 + (53 - 2*s2)*t2 + 
               2*(9 + 4*s2 + Power(s2,2))*Power(t2,2) + 
               (-9 - 5*s2 + 2*Power(s2,2))*Power(t2,3) + 
               8*(-1 + s2)*Power(t2,4)) - 
            t1*(18 + (55 - 13*s2)*t2 + 
               (-70 + 28*s2 - 4*Power(s2,2))*Power(t2,2) + 
               (61 - 95*s2 + 8*Power(s2,2))*Power(t2,3) + 
               2*(9 - 8*s2 + 3*Power(s2,2))*Power(t2,4) + 
               (-2 + 6*s2)*Power(t2,5)) - 
            Power(s,2)*(6 - 13*t2 - 27*Power(t2,2) + 18*Power(t2,3) + 
               13*Power(t2,4) + Power(t1,2)*(15 - 19*Power(t2,2)) + 
               Power(t1,3)*(-4 + t2 + 4*Power(t2,2)) + 
               t1*(-17 + 4*t2 + 30*Power(t2,2) - 7*Power(t2,3) - 
                  4*Power(t2,4))) + 
            s*(-11 - 3*(9 + s2)*t2 - 5*(-27 + 7*s2)*Power(t2,2) + 
               (247 - 51*s2)*Power(t2,3) + (26 + s2)*Power(t2,4) - 
               7*Power(t2,5) + Power(t1,4)*(1 - 2*Power(t2,2)) + 
               Power(t1,3)*(-15 + 2*(2 + s2)*t2 + 
                  (15 + 2*s2)*Power(t2,2) - 2*Power(t2,3)) + 
               Power(t1,2)*(16 + (-34 + 3*s2)*t2 - 
                  (49 + 5*s2)*Power(t2,2) + 15*Power(t2,3) + 
                  2*Power(t2,4)) + 
               t1*(9 + (57 - 2*s2)*t2 + (-17 + 16*s2)*Power(t2,2) + 
                  8*(-9 + s2)*Power(t2,3) - (11 + 2*s2)*Power(t2,4) + 
                  2*Power(t2,5)))) - 
         Power(s1,6)*(2 - 15*t2 - 13*s*t2 - 2*Power(s,2)*t2 - 
            45*Power(t2,2) + 16*s*Power(t2,2) + 
            3*Power(s,2)*Power(t2,2) + 16*Power(t2,3) + 
            2*s*Power(t2,3) + 2*Power(s,2)*Power(t2,3) + 
            Power(t1,4)*(-36 - 3*s + 7*t2) - 
            Power(t1,3)*(-79 + Power(s,2) + s*(22 - 4*t2) - 34*t2 + 
               8*Power(t2,2)) + 
            2*Power(s2,2)*(-1 - 4*t2 + 16*Power(t2,2) + 6*Power(t2,3) + 
               Power(t1,2)*(-1 + 3*t2) + t1*(2 - 15*t2 - 10*Power(t2,2))\
) + Power(t1,2)*(-4*Power(s,2)*(1 + t2) + 
               s*(55 + 21*t2 - 6*Power(t2,2)) + 
               2*(6 + 47*t2 + 2*Power(t2,2) + Power(t2,3))) + 
            t1*(54 - 46*t2 - 76*Power(t2,2) + 14*Power(t2,3) + 
               Power(s,2)*(4 + 3*t2 - Power(t2,2)) + 
               s*(-4 - 29*t2 + 27*Power(t2,2) + 2*Power(t2,3))) + 
            s2*(8*Power(t1,4) - Power(t1,3)*(15 + 32*t2) - 
               2*t2*(-39 + 2*(-13 + 7*s)*t2 + (3 + 6*s)*Power(t2,2)) + 
               2*t1*(15 + s + 104*t2 + 14*s*t2 + 38*Power(t2,2) + 
                  5*s*Power(t2,2) - 5*Power(t2,3)) + 
               Power(t1,2)*(-46 - 38*t2 + 27*Power(t2,2) + s*(5 + t2)))) \
+ Power(s1,5)*(Power(t1,4)*(-52 - 24*t2 + 6*Power(t2,2) + 
               s*(-3 + 7*t2)) + 
            Power(t1,3)*(53 + 40*t2 + 30*Power(t2,2) - 5*Power(t2,3) + 
               Power(s,2)*(1 + 6*t2) - s*(46 + 45*t2 + 4*Power(t2,2))) + 
            Power(t1,2)*(-17 + 126*t2 + 110*Power(t2,2) + Power(t2,3) + 
               Power(t2,4) - Power(s,2)*(20 + 23*t2 + 3*Power(t2,2)) + 
               s*(39 + 95*t2 + 44*Power(t2,2) - 5*Power(t2,3))) + 
            t2*(68 - 43*t2 - 73*Power(t2,2) + 14*Power(t2,3) + 
               s*(-8 - 71*t2 + 20*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s,2)*(4 + 6*t2 + 13*Power(t2,2) + 4*Power(t2,3))) + 
            2*Power(s2,2)*(Power(t1,2)*(-1 - 4*t2 + 9*Power(t2,2)) + 
               t1*(1 + 10*t2 - 24*Power(t2,2) - 12*Power(t2,3)) + 
               t2*(-5 - 19*t2 + 16*Power(t2,2) + 4*Power(t2,3))) + 
            t1*(62 + 286*t2 - 36*Power(t2,2) - 63*Power(t2,3) + 
               8*Power(t2,4) + 
               Power(s,2)*(20 + 2*t2 - 5*Power(t2,2) - 5*Power(t2,3)) + 
               s*(30 - 48*t2 - 100*Power(t2,2) + 19*Power(t2,3) + 
                  2*Power(t2,4))) + 
            s2*(-4 + (26 + 6*s)*t2 + (284 + 7*s)*Power(t2,2) + 
               (81 - 54*s)*Power(t2,3) - 2*(5 + 6*s)*Power(t2,4) + 
               8*Power(t1,4)*(2 + t2) + 
               Power(t1,3)*(-13 + s*(2 - 6*t2) + 2*t2 - 
                  31*Power(t2,2)) + 
               Power(t1,2)*(-16 - 168*t2 - 101*Power(t2,2) + 
                  27*Power(t2,3) + s*(3 + 17*t2 - 3*Power(t2,2))) + 
               t1*(7 + 24*t2 + 311*Power(t2,2) + 52*Power(t2,3) - 
                  9*Power(t2,4) + 
                  s*(-4 + 26*t2 + 54*Power(t2,2) + 20*Power(t2,3))))) + 
         Power(s1,3)*(Power(t1,4)*
             (7 + 8*(-3 + s2)*t2 - 6*Power(t2,2) + 
               s*(5 - t2 + 6*Power(t2,2) + 2*Power(t2,3))) + 
            Power(t1,3)*(-19 + (-11 + 10*s2)*t2 + 
               (-21 + 31*s2)*Power(t2,2) + 10*(-1 + 2*s2)*Power(t2,3) + 
               (4 - 6*s2)*Power(t2,4) + 
               Power(s,2)*(3 - 2*t2 + 5*Power(t2,2) + 2*Power(t2,3)) - 
               s*(-1 + 7*t2 + 10*(3 + s2)*Power(t2,2) + 
                  (15 + 2*s2)*Power(t2,3))) - 
            Power(t1,2)*(-15 + (63 + 11*s2)*t2 + 
               (113 + 35*s2 + 6*Power(s2,2))*Power(t2,2) + 
               8*(-9 + 9*s2 + Power(s2,2))*Power(t2,3) + 
               (-49 + 41*s2 - 6*Power(s2,2))*Power(t2,4) + 
               (4 - 6*s2)*Power(t2,5) + 
               3*Power(s,2)*(3 - 5*t2 + 5*Power(t2,3)) + 
               s*(17 + (14 + s2)*t2 - (98 + 15*s2)*Power(t2,2) - 
                  3*(21 + s2)*Power(t2,3) + (5 + 2*s2)*Power(t2,4) + 
                  2*Power(t2,5))) + 
            t1*(-3 + (111 - 10*s2)*t2 + 3*(59 + 3*s2)*Power(t2,2) + 
               2*(111 - 84*s2 + 14*Power(s2,2))*Power(t2,3) + 
               (-39 + 19*s2 + 6*Power(s2,2))*Power(t2,4) - 
               2*(-5 + s2)*s2*Power(t2,5) - 
               Power(s,2)*(-6 + 35*t2 + 25*Power(t2,2) - 
                  16*Power(t2,3) + 11*Power(t2,4) + 2*Power(t2,5)) + 
               s*(11 + 2*(-8 + s2)*t2 - 2*(41 + 2*s2)*Power(t2,2) + 
                  (-47 + 8*s2)*Power(t2,3) + 
                  2*(20 + 7*s2)*Power(t2,4) + (1 + 4*s2)*Power(t2,5))) + 
            t2*(-5 + 271*t2 + 460*Power(t2,2) - 147*Power(t2,3) - 
               34*Power(t2,4) + Power(t2,5) + 
               2*Power(s2,2)*t2*
                (3 - 4*t2 - 25*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(20 - 2*t2 - 5*Power(t2,2) + 30*Power(t2,3) + 
                  9*Power(t2,4)) + 
               s2*(3 - 65*t2 + 36*Power(t2,2) + 331*Power(t2,3) - 
                  8*Power(t2,4) - 3*Power(t2,5)) + 
               s*(38 - 18*t2 - 321*Power(t2,2) - 157*Power(t2,3) + 
                  14*Power(t2,4) + Power(t2,5) + 
                  s2*(-1 + 21*t2 + 85*Power(t2,2) + 5*Power(t2,3) - 
                     13*Power(t2,4))))) - 
         Power(s1,4)*(-2 + (60 + 34*s + 20*Power(s,2) - 21*s2 + 
               2*Power(s2,2))*t2 + 
            (312 + 6*Power(s,2) + 54*s2 - 16*Power(s2,2) + 
               s*(-123 + 49*s2))*Power(t2,2) + 
            (-82 + 19*Power(s,2) + 13*s*(-14 + s2) + 440*s2 - 
               66*Power(s2,2))*Power(t2,3) + 
            (-68 + 19*Power(s,2) + s*(13 - 44*s2) + 41*s2 + 
               14*Power(s2,2))*Power(t2,4) + 
            (6 + 2*Power(s,2) + s*(2 - 4*s2) - 9*s2 + 2*Power(s2,2))*
             Power(t2,5) + Power(t1,4)*
             (s*(5 + 7*t2 + 6*Power(t2,2)) + 
               2*(-6 - 28*t2 + Power(t2,3) + s2*(4 + 8*t2))) + 
            Power(t1,3)*(-21 + 22*t2 + 3*Power(t2,2) + 12*Power(t2,3) - 
               2*Power(t2,4) + Power(s,2)*(5 + 8*t2 + 3*Power(t2,2)) + 
               s2*(-3 + 12*t2 + 35*Power(t2,2) - 18*Power(t2,3)) - 
               s*(20 + 71*t2 + 28*Power(t2,2) + 5*Power(t2,3) + 
                  s2*(-2 + 6*t2 + 8*Power(t2,2)))) - 
            Power(t1,2)*(-28 + 96*t2 - 101*Power(t2,2) - 
               98*Power(t2,3) + 
               6*Power(s2,2)*t2*(1 + 2*t2 - 3*Power(t2,2)) + 
               Power(s,2)*(24 + 19*t2 + 4*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(8 + 42*t2 + 197*Power(t2,2) + 96*Power(t2,3) - 
                  19*Power(t2,4)) + 
               s*(19 - 71*t2 - 118*Power(t2,2) - 34*Power(t2,3) + 
                  6*Power(t2,4) + 
                  s2*(1 - 15*t2 - 15*Power(t2,2) + 5*Power(t2,3)))) + 
            t1*(9 + 262*t2 + 455*Power(t2,2) - 82*Power(t2,3) - 
               25*Power(t2,4) + 2*Power(t2,5) - 
               4*Power(s2,2)*t2*
                (-1 - 9*t2 + 6*Power(t2,2) + 3*Power(t2,3)) - 
               Power(s,2)*(-20 + 18*t2 + 23*Power(t2,2) + 
                  5*Power(t2,3) + 5*Power(t2,4)) + 
               s2*(3 - 5*t2 - 93*Power(t2,2) + 186*Power(t2,3) + 
                  15*Power(t2,4) - 3*Power(t2,5)) + 
               s*(34 - 7*t2 - 164*Power(t2,2) - 53*Power(t2,3) + 
                  13*Power(t2,4) + Power(t2,5) + 
                  s2*(-1 + 38*Power(t2,2) + 42*Power(t2,3) + 
                     15*Power(t2,4))))))*R1q(s1))/
     (Power(-1 + s1,2)*s1*(-1 + s2)*(-1 + t1)*(-1 + s1 + t1 - t2)*
       (s1*t1 - t2)*(-1 + t2)*Power(-s1 + t2,3)) - 
    (8*(-2*Power(s1,7)*t1*t2*(s2*(-3 + t1 + 2*t2) + 
            (-1 + t1)*(-2 + t1 + 4*t2)) + 
         Power(s1,5)*(-6*Power(t1,5)*t2 + 
            Power(t1,4)*(-6 + (55 + 6*s - 18*s2)*t2 - 31*Power(t2,2)) + 
            Power(t1,3)*(-4 + (-27 + 23*s + 15*s2)*t2 - 
               (-23 + 3*s + s2)*Power(t2,2) + 82*Power(t2,3)) + 
            t2*(-2 + (-2 + 8*s + 9*s2 - 2*Power(s2,2))*t2 + 
               (-16 + 21*s2 + 14*Power(s2,2) - 2*s*(3 + 4*s2))*
                Power(t2,2) - 
               4*(-7 + s*(-2 + s2) + 5*s2 - 2*Power(s2,2))*Power(t2,3)) \
+ Power(t1,2)*(8 + (29 - 21*s - 8*s2)*t2 + 
               (15 + 8*Power(s,2) - 17*s2 - 4*Power(s2,2) + 
                  4*s*(1 + s2))*Power(t2,2) + 
               (-26 - 15*s + 51*s2)*Power(t2,3) - 42*Power(t2,4)) + 
            t1*(-2 + (-5 + 4*s - s2)*t2 + 
               (41 + 17*s - 16*Power(s,2) + 5*s2 + 4*s*s2)*Power(t2,2) + 
               (-94 + 9*s + 8*Power(s,2) + 32*s2 - 20*s*s2 + 
                  4*Power(s2,2))*Power(t2,3) + 
               (2 + 30*s - 44*s2)*Power(t2,4))) - 
         Power(s1,6)*(6*Power(t1,4)*t2 + 
            Power(t1,3)*(2 + (-29 - 2*s + 10*s2)*t2 + 25*Power(t2,2)) + 
            Power(t1,2)*(2 + (23 - 6*s - 17*s2)*t2 + 
               (-19 + 2*s + 9*s2)*Power(t2,2) - 30*Power(t2,3)) + 
            t1*(-2 + (-2 + 8*s + 5*s2)*t2 + 
               (-10 - 6*s + 15*s2 - 4*s*s2 + 2*Power(s2,2))*
                Power(t2,2) + 4*(5 + 2*s - 5*s2)*Power(t2,3)) + 
            2*Power(t2,2)*(-2 + s2*(3 - 2*t2) + 4*t2 + 
               Power(s2,2)*(1 + t2))) - 
         Power(s1,4)*(2*Power(t1,6)*t2 + 
            Power(t1,5)*(6 + (-47 - 6*s + 14*s2)*t2 + 19*Power(t2,2)) + 
            Power(t1,3)*(-10 + (-45 + 22*s - s2)*t2 + 
               (107 - 22*Power(s,2) - 31*s2 + 2*Power(s2,2) + 
                  s*(29 + 8*s2))*Power(t2,2) + 
               (37 + 21*s - 54*s2)*Power(t2,3) + 101*Power(t2,4)) + 
            t2*(-2 + (4*s - 5*(1 + s2))*t2 + 
               (20 - 16*Power(s,2) + 46*s2 - 16*Power(s2,2) + 
                  s*(19 + 4*s2))*Power(t2,2) + 
               (-77 + 10*Power(s,2) + s*(3 - 44*s2) + 39*s2 + 
                  32*Power(s2,2))*Power(t2,3) + 
               2*(16 + Power(s,2) + s*(15 - 6*s2) - 22*s2 + 
                  6*Power(s2,2))*Power(t2,4)) + 
            Power(t1,2)*(4 + 5*(5 + s)*t2 + 
               (45 + 28*Power(s,2) - 13*s2 - 2*Power(s2,2) - 
                  2*s*(15 + 4*s2))*Power(t2,2) + 
               (135 - 28*s2 - 10*Power(s2,2) + s*(-19 + 20*s2))*
                Power(t2,3) + (79 - 46*s + 87*s2)*Power(t2,4) - 
               26*Power(t2,5)) - 
            Power(t1,4)*(-2 + 5*Power(t2,2) + 75*Power(t2,3) + 
               3*s2*t2*(1 + 3*t2) + s*t2*(29 + 5*t2)) + 
            t1*t2*(9 + (85 - 8*s2 - 2*Power(s2,2))*t2 + 
               (131 + 48*s2 - 6*Power(s2,2))*Power(t2,2) + 
               (-263 + 74*s2)*Power(t2,3) - 2*(17 + 26*s2)*Power(t2,4) + 
               2*Power(s,2)*t2*(-2 - 11*t2 + 12*Power(t2,2)) + 
               s*(-4 + 2*(-3 + 8*s2)*t2 + (13 + 24*s2)*Power(t2,2) + 
                  (23 - 36*s2)*Power(t2,3) + 44*Power(t2,4)))) + 
         Power(s1,3)*(Power(t1,6)*
             (-2 + (15 + 2*s - 4*s2)*t2 - 5*Power(t2,2)) + 
            Power(t1,4)*(4 + (15 - 7*s + 4*s2)*t2 + 
               (-152 + 20*Power(s,2) + 47*s2 - s*(31 + 12*s2))*
                Power(t2,2) + (-23 - 26*s + 35*s2)*Power(t2,3) - 
               76*Power(t2,4)) + 
            Power(t1,3)*(-2 + (-35 - 16*s + s2)*t2 + 
               (-153 - 6*Power(s,2) + 11*s2 + 4*s*(2 + s2))*
                Power(t2,2) + 
               (25 - 22*Power(s,2) - 51*s2 + 4*Power(s2,2) + 
                  2*s*(15 + 8*s2))*Power(t2,3) + 
               (-3 + 30*s - 69*s2)*Power(t2,4) + 58*Power(t2,5)) + 
            t1*t2*(12 + (84 + 27*s + 12*Power(s,2) - 8*s2)*t2 + 
               (383 + 24*Power(s,2) - 29*s2 - 16*Power(s2,2) + 
                  2*s*(-79 + 34*s2))*Power(t2,2) + 
               (237 - 18*Power(s,2) + 121*s2 - 8*Power(s2,2) + 
                  2*s*(-77 + 18*s2))*Power(t2,3) + 
               (-292 + 26*Power(s,2) + s*(3 - 28*s2) + 98*s2 - 
                  4*Power(s2,2))*Power(t2,4) + 
               (-34 + 30*s - 32*s2)*Power(t2,5)) + 
            Power(t1,5)*t2*(15 - s2 - 9*t2 + 5*s2*t2 + 24*Power(t2,2) + 
               s*(13 + 11*t2)) + 
            Power(t1,2)*t2*(-14 + (44 - 15*s2)*t2 + 
               (105 - 67*s2 + 4*Power(s2,2))*Power(t2,2) + 
               (239 - 135*s2 - 8*Power(s2,2))*Power(t2,3) + 
               3*(62 + 25*s2)*Power(t2,4) - 6*Power(t2,5) - 
               4*Power(s,2)*t2*(9 - 3*t2 + 5*Power(t2,2)) + 
               s*(8 + (-21 + 4*s2)*t2 - (77 + 32*s2)*Power(t2,2) + 
                  (29 + 40*s2)*Power(t2,3) - 53*Power(t2,4))) + 
            Power(t2,2)*(-1 + (70 - 32*s2)*t2 + 
               (95 + 131*s2 - 52*Power(s2,2))*Power(t2,2) + 
               (-212 + 47*s2 + 32*Power(s2,2))*Power(t2,3) + 
               (8 - 52*s2 + 8*Power(s2,2))*Power(t2,4) + 
               4*Power(s,2)*t2*
                (-1 - 7*t2 + 9*Power(t2,2) + Power(t2,3)) + 
               s*(-4 + 5*(3 + 4*s2)*t2 + (-17 + 28*s2)*Power(t2,2) + 
                  (26 - 84*s2)*Power(t2,3) + (44 - 12*s2)*Power(t2,4)))) \
+ Power(t2,3)*(Power(t1,5)*(-1 + 2*Power(s,2)*t2 + (15 - 4*s2)*t2 - 
               s*(5 + 3*t2)) - 
            Power(t1,4)*(-13 + (43 - 9*s2)*t2 + 
               (10 - 3*s2)*Power(t2,2) + 6*Power(t2,3) + 
               2*Power(s,2)*(3 + 4*t2 + Power(t2,2)) + 
               s*(-8 + (-30 + 4*s2)*t2 - 4*(-2 + s2)*Power(t2,2))) + 
            Power(t1,3)*(-21 + t2 + 4*s2*t2 + 
               (-65 + 29*s2)*Power(t2,2) + 
               (25 - 11*s2 + 2*Power(s2,2))*Power(t2,3) + 
               6*Power(t2,4) - 
               2*Power(s,2)*
                (-9 - 10*t2 + 3*Power(t2,2) + Power(t2,3)) + 
               s*(-1 + 10*t2 - 6*(-5 + 2*s2)*Power(t2,2) + 
                  5*Power(t2,3))) - 
            4*t2*(9 + (30 - 9*s2)*t2 + (40 - 11*s2)*Power(t2,2) + 
               (-8 + 13*s2 - 4*Power(s2,2))*Power(t2,3) - 
               (11 + s2)*Power(t2,4) + 
               2*Power(s,2)*(-6 - 9*t2 + Power(t2,2) + 2*Power(t2,3)) + 
               s*(-2 + (-17 + 4*s2)*t2 + (-53 + 14*s2)*Power(t2,2) + 
                  (-19 + 2*s2)*Power(t2,3) + 3*Power(t2,4))) + 
            2*t1*t2*(42 + (17 - 7*s2)*t2 + 
               (14 - 7*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (-47 + 20*s2 - 5*Power(s2,2))*Power(t2,3) - 
               2*(5 + 2*s2)*Power(t2,4) + 
               Power(s,2)*(-36 - 40*t2 - 3*Power(t2,2) + 
                  3*Power(t2,3)) + 
               s*(2 + 21*t2 + 3*(-19 + 6*s2)*Power(t2,2) + 
                  2*(-4 + s2)*Power(t2,3) + 4*Power(t2,4))) + 
            Power(t1,2)*(9 - (23 + 9*s2)*t2 + 
               (203 - 64*s2)*Power(t2,2) - 
               (-23 + s2 + 10*Power(s2,2))*Power(t2,3) + 
               2*(6 + 2*s2 + Power(s2,2))*Power(t2,4) + 
               2*Power(s,2)*(-6 + 3*t2 + 18*Power(t2,2) + 
                  4*Power(t2,3) + Power(t2,4)) - 
               s*(2 + (49 - 4*s2)*t2 + (158 - 28*s2)*Power(t2,2) + 
                  (-17 + 4*s2)*Power(t2,3) + (2 + 4*s2)*Power(t2,4)))) + 
         Power(s1,2)*t2*(Power(t1,6)*
             (7 + s - 2*t2 + 5*s*t2 + Power(t2,2)) + 
            Power(t1,2)*(2 + (33 + 52*s + 24*Power(s,2) - s2)*t2 + 
               (189 + 10*Power(s,2) - 66*s2 + s*(55 + 12*s2))*
                Power(t2,2) + 
               (78 - 22*Power(s,2) + 126*s2 - 24*Power(s2,2) + 
                  s*(-123 + 40*s2))*Power(t2,3) + 
               (-258 + 32*Power(s,2) + 173*s2 + 4*Power(s2,2) - 
                  2*s*(41 + 20*s2))*Power(t2,4) + 
               2*(-69 + 16*s - 18*s2)*Power(t2,5)) - 
            t2*(8 + (39 + 26*s + 12*Power(s,2) - 4*s2)*t2 + 
               (339 - 6*Power(s,2) - 101*s2 + s*(-97 + 100*s2))*
                Power(t2,2) + 
               (131 - 12*Power(s,2) + 212*s2 - 80*Power(s2,2) + 
                  2*s*(-93 + 26*s2))*Power(t2,3) + 
               (-289 + 44*Power(s,2) + s*(37 - 68*s2) + 27*s2 + 
                  14*Power(s2,2))*Power(t2,4) + 
               2*(-4 + Power(s,2) + s*(15 - 2*s2) - 16*s2 + 
                  Power(s2,2))*Power(t2,5)) + 
            Power(t1,5)*(-3 + 6*Power(s,2)*t2 + (-43 + 14*s2)*t2 + 
               (-25 + 16*s2)*Power(t2,2) - 17*Power(t2,3) + 
               s*(2 + t2 - 4*s2*t2 - 13*Power(t2,2))) + 
            Power(t1,4)*(-7 + (-107 + s2)*t2 + 
               (57 - 36*s2)*Power(t2,2) + (49 - 33*s2)*Power(t2,3) + 
               38*Power(t2,4) - 4*Power(s,2)*t2*(-3 + 4*t2) + 
               s*(-7 - 6*t2 + 5*(-3 + 4*s2)*Power(t2,2) + 
                  22*Power(t2,3))) + 
            Power(t1,3)*(1 + (129 - 14*s2)*t2 + 
               (382 - 60*s2)*Power(t2,2) + (14 - 46*s2)*Power(t2,3) + 
               (56 + 30*s2)*Power(t2,4) - 12*Power(t2,5) + 
               2*Power(s,2)*t2*(-23 - 12*t2 + 9*Power(t2,2)) - 
               2*s*(-2 - 2*(-7 + s2)*t2 + (65 - 2*s2)*Power(t2,2) + 
                  (-11 + 6*s2)*Power(t2,3) + 5*Power(t2,4))) - 
            t1*t2*(14 + (297 - 82*s2)*t2 + 
               (471 - 8*s2 - 36*Power(s2,2))*Power(t2,2) + 
               (183 + 53*s2 + 12*Power(s2,2))*Power(t2,3) + 
               (-101 + 71*s2 - 2*Power(s2,2))*Power(t2,4) - 
               2*(5 + 4*s2)*Power(t2,5) + 
               2*Power(s,2)*t2*
                (-41 + 12*t2 + Power(t2,2) + 5*Power(t2,3)) + 
               s*(24 + 3*(-29 + 8*s2)*t2 + 6*(-77 + 8*s2)*Power(t2,2) + 
                  6*(-31 + 2*s2)*Power(t2,3) - (17 + 8*s2)*Power(t2,4) + 
                  8*Power(t2,5)))) - 
         s1*Power(t2,2)*(Power(t1,6)*
             (-1 + s*(-5 + t2) + (13 - 4*s2)*t2) + 
            Power(t1,5)*(20 + 2*Power(s,2)*(-3 + t2) + (-31 + 9*s2)*t2 + 
               (-11 + 7*s2)*Power(t2,2) - 6*Power(t2,3) + 
               s*(9 + (11 - 4*s2)*t2 - 8*Power(t2,2))) + 
            Power(t1,4)*(-26 + (-65 + 14*s2)*t2 + 
               (-81 + 17*s2)*Power(t2,2) + (8 - 7*s2)*Power(t2,3) + 
               6*Power(t2,4) - 
               2*Power(s,2)*(-9 - 5*t2 + 8*Power(t2,2)) + 
               s*(1 + (65 - 4*s2)*t2 + (17 + 12*s2)*Power(t2,2) + 
                  Power(t2,3))) - 
            Power(t1,3)*(-2 + 9*(7 + s2)*t2 + 
               (-205 + 19*s2)*Power(t2,2) + 
               (-96 + 62*s2 - 4*Power(s2,2))*Power(t2,3) - 
               2*(25 + 2*s2)*Power(t2,4) + 
               2*Power(s,2)*(6 - 14*t2 - 15*Power(t2,2) + Power(t2,3)) + 
               s*(9 + (94 - 4*s2)*t2 + 2*(75 + 2*s2)*Power(t2,2) + 
                  (-63 + 4*s2)*Power(t2,3) + 2*Power(t2,4))) + 
            2*t1*t2*(-13 + (-83 + 30*s2)*t2 + 
               2*(-10 - 7*s2 + 8*Power(s2,2))*Power(t2,2) - 
               2*(38 - 15*s2 + 6*Power(s2,2))*Power(t2,3) - 
               2*(9 + 8*s2)*Power(t2,4) + 
               2*Power(s,2)*(18 + 17*t2 - 20*Power(t2,2) + 
                  Power(t2,3)) + 
               2*s*(10 + (52 - 6*s2)*t2 + (47 + 10*s2)*Power(t2,2) + 
                  (3 + 2*s2)*Power(t2,3) + 5*Power(t2,4))) - 
            2*t2*(10 + (82 - 20*s2)*t2 + (225 - 59*s2)*Power(t2,2) + 
               (10 + 85*s2 - 29*Power(s2,2))*Power(t2,3) + 
               (-93 + Power(s2,2))*Power(t2,4) - 
               2*(1 + 2*s2)*Power(t2,5) + 
               Power(s,2)*t2*
                (-24 - 12*t2 + 11*Power(t2,2) + 9*Power(t2,3)) + 
               s*(8 + 2*(-5 + 4*s2)*t2 + (-163 + 68*s2)*Power(t2,2) + 
                  9*(-13 + 2*s2)*Power(t2,3) - 
                  2*(-8 + 5*s2)*Power(t2,4) + 4*Power(t2,5))) + 
            Power(t1,2)*(5 - 2*(-92 + 5*s2)*t2 + 
               (389 - 145*s2)*Power(t2,2) + 
               (-4 + 63*s2 - 28*Power(s2,2))*Power(t2,3) + 
               2*(-57 + 35*s2 + 2*Power(s2,2))*Power(t2,4) - 
               8*(4 + s2)*Power(t2,5) + 
               4*Power(s,2)*t2*
                (-30 - 20*t2 + 9*Power(t2,2) + 4*Power(t2,3)) + 
               s*(4 + (-7 + 4*s2)*t2 + (-153 + 44*s2)*Power(t2,2) + 
                  6*(-29 + 2*s2)*Power(t2,3) - 
                  2*(13 + 10*s2)*Power(t2,4) + 8*Power(t2,5)))))*R1q(t2))/
     ((-1 + s2)*(-1 + t1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*(-1 + t2)*t2*Power(-s1 + t2,3)) + 
    (8*(2*Power(-1 + s,2)*Power(t1,5) - 3*Power(s1,5)*t1*(-2 + s2 + t1) + 
         2*t1*(1 - 2*(6 + 3*Power(s,2) - 9*s2 - s*(9 + s2))*t2 + 
            (65 - 9*Power(s,2) - 43*s2 + 11*Power(s2,2) + 
               s*(-25 + 16*s2))*Power(t2,2) + 
            (-44 + Power(s,2) + 15*s2 - 7*Power(s2,2) + s*(-5 + 6*s2))*
             Power(t2,3) + 2*(-5 + 2*s - 2*s2)*Power(t2,4)) - 
         Power(t1,4)*(10 + (7 - 2*s2)*t2 + 6*Power(t2,2) + 
            2*Power(s,2)*(3 + t2) + s*(-16 + (3 - 4*s2)*t2)) + 
         Power(t1,3)*(16 + (-34 + 32*s2)*t2 + 
            (24 - 11*s2 + 2*Power(s2,2))*Power(t2,2) + 6*Power(t2,3) - 
            Power(s,2)*(-2 + t2 + 2*Power(t2,2)) + 
            2*s*(-8 - 8*(-1 + s2)*t2 + 3*Power(t2,2))) - 
         4*t2*(-7*Power(s2,2)*(-1 + t2)*t2 + Power(s,2)*t2*(-3 + 2*t2) + 
            s2*(1 - 48*t2 + 5*Power(t2,2) - 2*Power(t2,3)) + 
            s*(5 + (-23 + 8*s2)*t2 + (-22 + 7*s2)*Power(t2,2) + 
               4*Power(t2,3)) - 
            2*(-1 - 31*t2 + Power(t2,2) + 7*Power(t2,3))) + 
         Power(t1,2)*(-10 + (89 - 84*s2 + 2*Power(s2,2))*t2 + 
            (14 - 3*s2 - 12*Power(s2,2))*Power(t2,2) + 
            (7 + 7*s2 + 2*Power(s2,2))*Power(t2,3) + 
            Power(s,2)*t2*(22 + 7*t2 + 2*Power(t2,2)) - 
            s*(-4 + (51 - 12*s2)*t2 + (8 + s2)*Power(t2,2) + 
               (5 + 4*s2)*Power(t2,3))) - 
         Power(s1,4)*(11*Power(t1,3) + 
            s2*(11*Power(t1,2) + t1*(9 - 3*s - 8*t2) - 3*t2) + 6*t2 + 
            2*Power(s2,2)*(1 + t1 + t2) - 3*Power(t1,2)*(8 + 3*t2) + 
            t1*(-5 + 14*t2 + 3*s*(2 + t2))) + 
         Power(s1,3)*(-13*Power(t1,4) + 3*Power(t1,3)*(13 + s + 8*t2) + 
            t2*(-5 + 17*t2 + 3*s*(2 + t2)) - 
            2*Power(s2,2)*(1 + 2*Power(t1,2) - 5*t2 - 2*Power(t2,2) + 
               t1*(3 + t2)) + 
            Power(t1,2)*(46 + 3*Power(s,2) - 27*t2 - 6*Power(t2,2) - 
               4*s*(5 + 2*t2)) + 
            s2*(4 - 19*Power(t1,3) + (13 - 7*s)*t2 - 
               4*(2 + s)*Power(t2,2) + 
               Power(t1,2)*(-30 + 6*s + 23*t2) + 
               t1*(54 + s*(4 - 7*t2) + 24*t2 - 11*Power(t2,2))) + 
            t1*(-29 + 3*Power(s,2)*(-2 + t2) - 60*t2 - 4*Power(t2,2) + 
               s*(17 + 14*t2 + 9*Power(t2,2)))) - 
         Power(s1,2)*(2 + 5*Power(t1,5) - 27*t2 + 13*s*t2 - 
            6*Power(s,2)*t2 - 36*Power(t2,2) + 10*s*Power(t2,2) + 
            5*Power(s,2)*Power(t2,2) + 5*Power(t2,3) + 9*s*Power(t2,3) + 
            2*Power(s,2)*Power(t2,3) - 3*Power(t1,4)*(10 + 2*s + 7*t2) + 
            Power(t1,3)*(-63 + 26*s - 8*Power(s,2) + 24*t2 + 7*s*t2 + 
               12*Power(t2,2)) + 
            2*Power(s2,2)*(Power(t1,3) - Power(t1,2)*(-3 + t2) + 
               t1*(1 - 7*t2 - 3*Power(t2,2)) + 
               t2*(-9 + 4*t2 + Power(t2,2))) + 
            Power(t1,2)*(98 + Power(s,2)*(18 - 4*t2) + 111*t2 + 
               42*Power(t2,2) - s*(40 + 15*t2 + 14*Power(t2,2))) + 
            t1*(40 + 88*t2 - 142*Power(t2,2) - 10*Power(t2,3) + 
               Power(s,2)*(-8 - 5*t2 + 6*Power(t2,2)) + 
               2*s*(8 - 41*t2 + 3*Power(t2,2) + 4*Power(t2,3))) + 
            s2*(-4 + 17*Power(t1,4) + Power(t1,3)*(33 - 3*s - 22*t2) + 
               (70 + 4*s)*t2 + (21 - 23*s)*Power(t2,2) - 
               (11 + 4*s)*Power(t2,3) + 
               Power(t1,2)*(-106 - 43*t2 + 18*Power(t2,2) + 
                  2*s*(-4 + 5*t2)) + 
               t1*(8 - 32*t2 + 29*Power(t2,2) - 8*Power(t2,3) + 
                  s*(8 + 26*t2)))) + 
         s1*(3*Power(t1,5)*(3 + s - 2*s2 + 2*t2) + 
            2*(-1 + (24 + 8*s - 4*Power(s,2) - 8*s2 + 6*s*s2 + 
                  2*Power(s2,2))*t2 + 
               (23 - 3*Power(s,2) + 9*s2 - 17*Power(s2,2) + 
                  s*(-39 + 14*s2))*Power(t2,2) + 
               (-52 + 7*Power(s,2) + s*(5 - 6*s2) + 5*s2 - Power(s2,2))*
                Power(t2,3) + (-2 + 4*s - 4*s2)*Power(t2,4)) + 
            Power(t1,4)*(24 + 7*Power(s,2) - 19*t2 - 6*Power(t2,2) - 
               2*s*(8 + t2) + s2*(-12 + 7*t2)) + 
            2*t1*(10 + (168 - 125*s2 + 12*Power(s2,2))*t2 + 
               (31 - 6*s2 - 4*Power(s2,2))*Power(t2,2) + 
               (-19 + 5*s2)*Power(t2,3) + Power(s,2)*t2*(4 + t2) + 
               s*(8 + (-70 + 4*s2)*t2 + (-47 + 9*s2)*Power(t2,2) + 
                  Power(t2,3))) + 
            Power(t1,3)*(-67 + 2*Power(s2,2)*(-1 + t2) - 44*t2 - 
               28*Power(t2,2) - Power(s,2)*(18 + t2) + 
               s2*(52 + 40*t2 - 7*Power(t2,2)) + 
               s*(39 - 14*t2 + 5*Power(t2,2) + s2*(4 + t2))) + 
            Power(t1,2)*(4*Power(s2,2)*(-1 + t2)*t2 + 
               Power(s,2)*(10 - 8*Power(t2,2)) + 
               t2*(-189 + 125*t2 + 32*Power(t2,2)) + 
               s2*(-16 + 75*t2 - 52*Power(t2,2) + 8*Power(t2,3)) + 
               s*(-20 + 96*t2 + 17*Power(t2,2) - 8*Power(t2,3) + 
                  s2*(-8 - 23*t2 + 4*Power(t2,2))))))*R2q(1 - s1 - t1 + t2))/
     ((-1 + s2)*(-1 + t1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*(-1 + t2)) - 
    (8*((-1 + s)*Power(t1,7)*(-1 + s - t2) + 
         Power(t1,3)*(1 - 2*(29 + 3*Power(s,2) + s*(-24 + s2) - 2*s2)*
             t2 - (-61 + 11*Power(s,2) + s*(22 - 68*s2) + 46*s2 + 
               Power(s2,2))*Power(t2,2) + 
            2*(-19 - 13*s + 3*Power(s,2) + 23*s2 - 7*Power(s2,2))*
             Power(t2,3) + 2*(3*s - 5*(1 + s2))*Power(t2,4)) - 
         2*t1*t2*(3 + (-44 - 6*Power(s,2) + s*(17 - 2*s2) + 25*s2)*t2 - 
            (-26 + s + 16*Power(s,2) + 27*s2 - 52*s*s2 - 4*Power(s2,2))*
             Power(t2,2) + (-20 - 2*Power(s,2) + 37*s2 - 
               20*Power(s2,2) + s*(5 + 14*s2))*Power(t2,3) + 
            (19 + 2*Power(s,2) - 7*s2 + 2*Power(s2,2) - s*(1 + 4*s2))*
             Power(t2,4)) - Power(s1,7)*
          (Power(t1,2)*(-1 + 2*t1) + s2*t1*(4 + 2*t1 - t2) + 
            Power(s2,2)*(1 + t2)) - 
         Power(t1,6)*(5 + (-2 + 3*s2)*t2 + (2 + s2)*Power(t2,2) + 
            Power(s,2)*(3 + t2) - s*(8 + 3*s2*t2 + 2*Power(t2,2))) + 
         Power(t1,5)*(8 + 2*(-8 + 5*s2)*t2 + 
            2*(2 - 3*s2 + Power(s2,2))*Power(t2,2) + 
            (5 + s2)*Power(t2,3) - 
            Power(s,2)*(-1 + 4*t2 + Power(t2,2)) - 
            s*(8 + 3*(-4 + 3*s2)*t2 - 10*Power(t2,2) + Power(t2,3))) + 
         4*Power(t2,2)*(1 + 43*t2 + Power(t2,2) + Power(t2,3) + 
            2*Power(t2,4) + Power(s2,2)*t2*(1 - 14*t2 + 3*Power(t2,2)) + 
            Power(s,2)*t2*(-9 + 4*t2 + 3*Power(t2,2)) + 
            s2*(1 - 14*t2 + 37*Power(t2,2) - 4*Power(t2,3)) + 
            s*(5 + 16*(-1 + s2)*t2 + (-19 + 2*s2)*Power(t2,2) + 
               (2 - 6*s2)*Power(t2,3))) + 
         Power(t1,4)*(-5 + (47 - 11*s2)*t2 + 
            (-17 + 32*s2 - 6*Power(s2,2))*Power(t2,2) + 
            (-3 + 3*s2 + 2*Power(s2,2))*Power(t2,3) - 4*Power(t2,4) + 
            Power(s,2)*t2*(16 + 11*t2 + Power(t2,2)) - 
            s*(-2 + (58 - 10*s2)*t2 + (8 + 23*s2)*Power(t2,2) + 
               (10 + 3*s2)*Power(t2,3))) + 
         2*Power(t1,2)*t2*(15 + (-79 + 38*s2 + 2*Power(s2,2))*t2 + 
            (17 - 69*s2 + 16*Power(s2,2))*Power(t2,2) + 
            (21 + 3*s2 - 5*Power(s2,2))*Power(t2,3) + 
            2*(3 + s2)*Power(t2,4) - 
            Power(s,2)*t2*(3 + 17*t2 + 3*Power(t2,2)) + 
            s*(-6 + (49 - 31*s2)*t2 + (37 + 25*s2)*Power(t2,2) + 
               (4 + 8*s2)*Power(t2,3) - 2*Power(t2,4))) - 
         Power(s1,6)*(Power(s2,2)*
             (2 + Power(t1,2) - 3*t2 - 2*Power(t2,2) + t1*(5 + 4*t2)) + 
            t1*(-4 + 9*Power(t1,3) + 3*t2 - 5*Power(t1,2)*(3 + t2) + 
               t1*(-7 + 5*t2) + 
               s*(-2 - Power(t1,2) + 2*t1*(-2 + t2) + 4*t2 + 
                  Power(t2,2))) + 
            s2*(-2 + 10*Power(t1,3) + 2*(-3 + s)*t2 + 
               (1 + 2*s)*Power(t2,2) - Power(t1,2)*(-22 + s + 9*t2) + 
               t1*(-13 + (-13 + s)*t2 + Power(t2,2)))) + 
         Power(s1,5)*(-1 - 16*Power(t1,5) - 5*t2 + 2*Power(t2,2) + 
            6*s*Power(t2,2) - Power(s,2)*Power(t2,2) + s*Power(t2,3) - 
            Power(s,2)*Power(t2,3) + Power(t1,4)*(44 + 5*s + 20*t2) + 
            Power(t1,3)*(27 + Power(s,2) - 6*s*(-1 + t2) - 14*t2 - 
               4*Power(t2,2)) - 
            Power(s2,2)*(1 + 4*Power(t1,3) - 17*t2 - 3*Power(t2,2) + 
               Power(t2,3) + Power(t1,2)*(13 + 3*t2) + 
               t1*(5 - 11*t2 - 8*Power(t2,2))) + 
            Power(t1,2)*(6 - 35*t2 + Power(s,2)*(-5 + 3*t2) + 
               s*(2 - 5*t2 + Power(t2,2))) + 
            t1*(-13 - 23*t2 + 17*Power(t2,2) + Power(s,2)*t2*(2 + t2) + 
               2*s*(1 + 2*t2 + 6*Power(t2,2))) + 
            s2*(4 - 20*Power(t1,4) - (19 + 2*s)*t2 + 
               (-15 + 7*s)*Power(t2,2) + (1 + 2*s)*Power(t2,3) + 
               Power(t1,3)*(-43 + 3*s + 23*t2) + 
               Power(t1,2)*(66 + s*(7 - 10*t2) + 56*t2 - 
                  9*Power(t2,2)) + 
               t1*(9 + (44 - 10*s)*t2 - (26 + 9*s)*Power(t2,2)))) + 
         Power(s1,2)*(-Power(t1,8) + 
            Power(t1,7)*(2 + 5*s - 2*s2 + 5*t2) + 
            t1*(-3 + 2*(43 - 8*Power(s,2) + 6*s2 + Power(s2,2) + 
                  s*(12 + 11*s2))*t2 + 
               (-181 - 45*Power(s,2) + 538*s2 - 95*Power(s2,2) + 
                  2*s*(7 + 26*s2))*Power(t2,2) + 
               2*(-53 + 22*Power(s,2) + s*(45 - 58*s2) + 51*s2 + 
                  16*Power(s2,2))*Power(t2,3) + 
               2*(58 + s + 4*Power(s,2) - 23*s2 - 8*s*s2 + 
                  4*Power(s2,2))*Power(t2,4)) + 
            Power(t1,6)*(8 + 4*Power(s,2) - Power(s2,2) + 6*t2 - 
               4*Power(t2,2) - 3*s*(4 + s2 + 2*t2) + s2*(2 + 5*t2)) - 
            Power(t1,5)*(22 + 5*Power(s,2) + 4*Power(s2,2) + 77*t2 + 
               50*Power(t2,2) + 
               s*(-10 + s2*(-10 + t2) + 26*t2 - 5*Power(t2,2)) + 
               s2*(-31 - 31*t2 + 7*Power(t2,2))) + 
            2*t2*(6 + (-48 - s + 2*Power(s,2))*t2 + 
               (-50 + 45*s + 7*Power(s,2))*Power(t2,2) - 
               8*(-3 - 3*s + Power(s,2))*Power(t2,3) - 
               2*s*Power(t2,4) + 
               Power(s2,2)*t2*(-17 + 52*t2 + 2*Power(t2,2)) + 
               s2*(-6 + (68 - 9*s)*t2 - (37 + 43*s)*Power(t2,2) + 
                  (-29 + 6*s)*Power(t2,3) + 2*Power(t2,4))) + 
            Power(t1,2)*(25 + 201*t2 + 205*Power(t2,2) - 
               247*Power(t2,3) - 28*Power(t2,4) - 
               Power(s2,2)*t2*(14 + 25*t2 + 9*Power(t2,2)) + 
               Power(s,2)*t2*(-30 + 7*t2 + 11*Power(t2,2)) - 
               2*s2*(1 - 10*t2 + 2*Power(t2,2) - 29*Power(t2,3) + 
                  8*Power(t2,4)) + 
               2*s*(7 + 3*(-9 + 8*s2)*t2 - (53 + s2)*Power(t2,2) - 
                  (-11 + s2)*Power(t2,3) + 8*Power(t2,4))) + 
            Power(t1,3)*(8 + 142*t2 + 279*Power(t2,2) + 
               16*Power(t2,3) + 
               Power(s,2)*(9 + 52*t2 - 19*Power(t2,2)) + 
               Power(s2,2)*(1 + 46*t2 - 13*Power(t2,2)) + 
               2*s2*(-6 - 161*t2 - 58*Power(t2,2) + 24*Power(t2,3)) - 
               2*s*(2 + 6*t2 + 25*Power(t2,2) + 10*Power(t2,3) + 
                  s2*(4 + 42*t2 - 25*Power(t2,2)))) + 
            Power(t1,4)*(-37 - 69*t2 + 88*Power(t2,2) + 36*Power(t2,3) + 
               Power(s2,2)*(3 + 10*t2) - 
               Power(s,2)*(3 + 15*t2 + 4*Power(t2,2)) + 
               s2*(-8 + 8*t2 - 37*Power(t2,2) + 4*Power(t2,3)) + 
               2*s*(12 + 33*t2 + 11*Power(t2,2) - 2*Power(t2,3) + 
                  s2*(-4 + 7*t2 + 2*Power(t2,2))))) + 
         s1*((-1 + s)*Power(t1,8) + 
            Power(t1,2)*(3 + 
               (-44 - 18*Power(s,2) + 28*s2 + 2*s*(4 + 5*s2))*t2 - 
               (85 + 59*Power(s,2) - 286*s2 + 57*Power(s2,2) - 
                  4*s*(-3 + 31*s2))*Power(t2,2) + 
               2*(-89 + 5*Power(s,2) + s*(43 - 18*s2) + 52*s2 + 
                  Power(s2,2))*Power(t2,3) + 
               2*(32 + Power(s,2) - 2*s*(-2 + s2) - 22*s2 + 
                  Power(s2,2))*Power(t2,4)) - 
            2*t2*(-3 + (44 - 4*Power(s,2) - 21*s2 + 2*Power(s2,2) + 
                  s*(3 + 10*s2))*t2 - 
               (60 + 4*Power(s,2) - 177*s2 + 48*Power(s2,2) + 
                  s*(9 + 4*s2))*Power(t2,2) + 
               (-16 + 16*Power(s,2) + s*(27 - 54*s2) + 7*s2 + 
                  30*Power(s2,2))*Power(t2,3) + 
               (27 + 2*Power(s,2) - 7*s2 + 2*Power(s2,2) - 
                  s*(1 + 4*s2))*Power(t2,4)) + 
            Power(t1,7)*(Power(s,2) + s2 + 3*t2 + s2*t2 - 
               s*(2 + s2 + 2*t2)) + 
            Power(t1,6)*(4 - Power(s,2)*(-2 + t2) - 
               (5 - 4*s2 + Power(s2,2))*t2 - (10 + s2)*Power(t2,2) + 
               s*(s2 + 2*s2*t2 + (-13 + t2)*t2)) + 
            Power(t1,4)*(25 - 5*t2 + 89*Power(t2,2) + 40*Power(t2,3) + 
               Power(s2,2)*t2*(13 + 2*t2 + Power(t2,2)) + 
               Power(s,2)*(3 + 14*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               s2*(6 - 43*t2 - 49*Power(t2,2) + 17*Power(t2,3)) - 
               s*(22 + (2 + 48*s2)*t2 - 5*(6 + s2)*Power(t2,2) + 
                  (11 + 2*s2)*Power(t2,3))) - 
            2*t1*t2*(25 + 129*t2 + 47*Power(t2,2) - 47*Power(t2,3) + 
               2*Power(t2,4) - 
               Power(s2,2)*t2*(11 + 4*t2 + 3*Power(t2,2)) + 
               Power(s,2)*t2*(-33 + 2*t2 + 5*Power(t2,2)) + 
               s2*(-2 + 38*t2 + 50*Power(t2,2) - 6*Power(t2,3) - 
                  4*Power(t2,4)) + 
               2*s*(7 + 6*(-4 + 3*s2)*t2 - 3*(7 + s2)*Power(t2,2) - 
                  (-10 + s2)*Power(t2,3) + 2*Power(t2,4))) + 
            Power(t1,3)*(-13 + 135*t2 + 19*Power(t2,2) - 
               115*Power(t2,3) - 36*Power(t2,4) + 
               Power(s,2)*Power(t2,2)*(25 + 7*t2) + 
               Power(s2,2)*t2*(-12 - 9*t2 + Power(t2,2)) - 
               2*s2*(1 + t2 - 36*Power(t2,2) - 10*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s*(6 + (-82 + 44*s2)*t2 - 6*(15 + 8*s2)*Power(t2,2) - 
                  2*(13 + 4*s2)*Power(t2,3) + 8*Power(t2,4))) + 
            Power(t1,5)*(-18 + 2*Power(s2,2)*t2 + 3*Power(t2,2) + 
               8*Power(t2,3) - Power(s,2)*(7 + 9*t2 + Power(t2,2)) - 
               5*s2*(1 + 2*t2 + 2*Power(t2,2)) + 
               s*(s2*(-2 + 14*t2 + Power(t2,2)) + 
                  2*(14 + 8*t2 + 9*Power(t2,2))))) - 
         Power(s1,4)*(14*Power(t1,6) - Power(t1,5)*(51 + 10*s + 30*t2) - 
            2*(-1 + 8*t2 - (-9 + 7*s + Power(s,2))*Power(t2,2) + 
               (-5 - 6*s + Power(s,2))*Power(t2,3)) + 
            Power(t1,4)*(-41 - 4*Power(s,2) + 13*t2 + 12*Power(t2,2) + 
               8*s*(1 + t2)) + 
            Power(t1,3)*(17 - 8*Power(s,2)*(-2 + t2) + 153*t2 + 
               32*Power(t2,2) + s*(-2 + t2 - 8*Power(t2,2))) - 
            Power(t1,2)*(-36 - 125*t2 + 100*Power(t2,2) + 
               8*Power(t2,3) + Power(s,2)*(2 + 4*t2) + 
               2*s*t2*(7 + 12*t2 - 2*Power(t2,2))) + 
            t1*(4 + 8*t2 - 33*Power(t2,2) + 15*Power(t2,3) + 
               2*Power(s,2)*t2*(-6 + 5*t2 + 2*Power(t2,2)) + 
               s*(2 + 22*t2 - 26*Power(t2,2) + Power(t2,3))) + 
            Power(s2,2)*(6*Power(t1,4) + Power(t1,3)*(19 - 4*t2) + 
               Power(t1,2)*(1 - 19*t2 - 10*Power(t2,2)) + 
               t2*(-16 + 33*t2 + 11*Power(t2,2)) + 
               t1*(3 - 54*t2 - Power(t2,2) + 4*Power(t2,3))) + 
            s2*(-2 + 20*Power(t1,5) + (33 - 2*s)*t2 - 
               5*(-4 + 5*s)*Power(t2,2) - (19 + 9*s)*Power(t2,3) - 
               2*Power(t1,4)*(-18 + s + 13*t2) + 
               2*Power(t1,3)*
                (-62 + 11*s*(-1 + t2) - 48*t2 + 10*Power(t2,2)) + 
               t1*(-6 + (178 + 19*s)*t2 + (116 - 38*s)*Power(t2,2) - 
                  (21 + 8*s)*Power(t2,3)) + 
               Power(t1,2)*(-18 - 93*t2 + 77*Power(t2,2) - 
                  4*Power(t2,3) + s*(4 + 19*t2 + 10*Power(t2,2))))) - 
         Power(s1,3)*(1 + 6*Power(t1,7) - 
            2*(8 + (-14 + s)*s2 + 3*Power(s2,2))*t2 + 
            (-1 + 5*Power(s,2) - 158*s2 + 79*Power(s2,2) - 
               4*s*(1 + 3*s2))*Power(t2,2) - 
            2*(-7 + 6*Power(s,2) + 38*s2 + 10*Power(s2,2) - 
               s*(11 + 28*s2))*Power(t2,3) - 
            2*(5 + 3*Power(s,2) - 6*s2 - 6*s*s2 + 3*Power(s2,2))*
             Power(t2,4) - 2*Power(t1,6)*(12 + 5*s - 5*s2 + 10*t2) + 
            Power(t1,5)*(-29 - 6*Power(s,2) + 4*Power(s2,2) + t2 + 
               12*Power(t2,2) - 5*s2*(-2 + 3*t2) + 2*s*(10 + s2 + 4*t2)) \
+ t1*(1 - 109*t2 - 195*Power(t2,2) + 125*Power(t2,3) + 4*Power(t2,4) + 
               Power(s,2)*t2*(6 + 25*t2 - 7*Power(t2,2)) + 
               Power(s2,2)*t2*(-14 + 63*t2 + 19*Power(t2,2)) + 
               2*s2*(-3 + 39*t2 + 6*Power(t2,2) - 54*Power(t2,3) + 
                  4*Power(t2,4)) - 
               2*s*(-1 + t2 + 4*s2*t2 + (-41 + 36*s2)*Power(t2,2) + 
                  (-31 + 6*s2)*Power(t2,3) + 4*Power(t2,4))) + 
            Power(t1,4)*(44 + Power(s,2)*(17 - 6*t2) + 
               Power(s2,2)*(14 - 5*t2) + 192*t2 + 72*Power(t2,2) + 
               2*s2*(-51 - 40*t2 + 9*Power(t2,2)) + 
               2*s*(-5 + 7*t2 - 5*Power(t2,2) + 4*s2*(-3 + 2*t2))) + 
            Power(t1,3)*(59 + 161*t2 - 178*Power(t2,2) - 
               36*Power(t2,3) + Power(s,2)*(-3 + 5*t2 + 4*Power(t2,2)) - 
               Power(s2,2)*(5 + 19*t2 + 4*Power(t2,2)) - 
               2*s2*(4 + 35*t2 - 40*Power(t2,2) + 4*Power(t2,3)) + 
               2*s*(-1 - 30*t2 - 9*Power(t2,2) + 4*Power(t2,3) + 
                  s2*(5 + 4*t2))) + 
            Power(t1,2)*(36 - 96*t2 - 185*Power(t2,2) + 50*Power(t2,3) + 
               s2*(12 + 420*t2 + 154*Power(t2,2) - 52*Power(t2,3)) + 
               Power(s2,2)*(1 - 70*t2 + 15*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s,2)*(-7 - 46*t2 + 27*Power(t2,2) + 4*Power(t2,3)) + 
               s*(4 + 4*t2 + 54*Power(t2,2) + 12*Power(t2,3) + 
                  s2*(8 + 62*t2 - 76*Power(t2,2) - 8*Power(t2,3))))))*
       T2q(t2,1 - s1 - t1 + t2))/
     ((-1 + s2)*(-1 + t1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       (-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)) - 
    (8*(Power(s1,7)*(3*Power(t1,2) - s2*t1*(-4 + t2) + 
            Power(s2,2)*(1 + t2)) + 
         Power(s1,6)*(Power(s2,2)*
             (2 - Power(t1,2) - 6*t2 - 5*Power(t2,2) + t1*(2 + t2)) - 
            t1*(4 + Power(t1,3) - Power(t1,2)*(-8 + t2) + 5*t2 + 
               t1*(-3 + 14*t2) + 
               s*(2 + 8*t1 + Power(t1,2) - 4*t2 - Power(t2,2))) + 
            s2*(-2 + 2*Power(t1,3) + Power(t1,2)*(8 + s - 2*t2) + 
               2*(-3 + s)*t2 + (1 + 2*s)*Power(t2,2) + 
               t1*(-13 + (-21 + s)*t2 + 4*Power(t2,2)))) + 
         Power(s1,5)*(1 - Power(t1,5) + 5*t2 + 2*Power(t2,2) - 
            6*s*Power(t2,2) + Power(s,2)*Power(t2,2) - s*Power(t2,3) + 
            Power(s,2)*Power(t2,3) + Power(t1,4)*(-13 + 2*t2) + 
            Power(t1,3)*(16 + Power(s,2) + s*(-2 + t2) + 23*t2 - 
               3*Power(t2,2)) + 
            Power(t1,2)*(-8 - Power(s,2)*(-1 + t2) - t2 + 
               22*Power(t2,2) + s*(6 + 33*t2 + 2*Power(t2,2))) - 
            Power(s2,2)*(-1 + Power(t1,3) + 17*t2 - 15*Power(t2,2) - 
               10*Power(t2,3) - 2*Power(t1,2)*(2 + t2) + 
               t1*(1 + 4*t2 + 5*Power(t2,2))) + 
            s2*(-4 + 2*Power(t1,4) + (25 + 2*s)*t2 + 
               (31 - 13*s)*Power(t2,2) - 4*(1 + 2*s)*Power(t2,3) - 
               Power(t1,3)*(5 + 8*t2) - 
               Power(t1,2)*(9 + 3*s + 31*t2 - 6*Power(t2,2)) + 
               t1*(-3 + 41*t2 + 48*Power(t2,2) - 6*Power(t2,3))) - 
            t1*(-13 - 15*t2 - 24*Power(t2,2) + Power(s,2)*t2*(2 + t2) + 
               s*(2 - 10*t2 + 20*Power(t2,2) + 3*Power(t2,3)))) + 
         Power(t2,2)*(-2 - 17*t2 + 4*s2*t2 - 8*Power(t2,2) + 
            6*s2*Power(t2,2) - 2*Power(s2,2)*Power(t2,2) - 
            3*Power(t2,3) + 24*s2*Power(t2,3) - 
            5*Power(s2,2)*Power(t2,3) + 10*Power(t2,4) - 
            6*s2*Power(t2,4) + 6*Power(s2,2)*Power(t2,4) - 
            Power(t1,4)*t2*(1 + t2) + 
            Power(t1,3)*(2 + (-9 + 4*s2)*t2 + 
               (-12 + 7*s2)*Power(t2,2) + (2 + s2)*Power(t2,3)) - 
            Power(t1,2)*(6 + 4*(-1 + s2)*t2 + 
               (-40 + 26*s2)*Power(t2,2) + 
               2*(-3 + Power(s2,2))*Power(t2,3) + (1 + s2)*Power(t2,4)) \
+ t1*(6 + (23 - 4*s2)*t2 + (-19 + 13*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (-13 - 6*s2 + 4*Power(s2,2))*Power(t2,3) + 
               (-3 + 5*s2 - 2*Power(s2,2))*Power(t2,4)) + 
            Power(s,2)*(4 + 18*t2 - Power(t1,4)*t2 + 2*Power(t2,2) - 
               3*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,3)*t2*(3 + t2) + 
               Power(t1,2)*(2 + 5*t2 - 2*Power(t2,2) + Power(t2,3)) - 
               t1*(6 + 24*t2 - 2*Power(t2,2) + 5*Power(t2,3) + 
                  Power(t2,4))) + 
            s*(2 - 2*(-3 + s2)*t2 + (22 + 6*s2)*Power(t2,2) - 
               6*(-2 + s2)*Power(t2,3) - 2*(3 + 5*s2)*Power(t2,4) + 
               Power(t1,4)*t2*(2 + t2) - 
               Power(t1,3)*(-2 + 6*t2 + (2 + 3*s2)*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,2)*(-2 + 2*(-6 + s2)*t2 + 
                  (4 + 11*s2)*Power(t2,2) + Power(t2,4)) + 
               t1*(-2 + 10*t2 - 2*(7 + 8*s2)*Power(t2,2) + 
                  (4 + 7*s2)*Power(t2,3) + 3*s2*Power(t2,4)))) + 
         Power(s1,4)*(2 + Power(t1,5)*(-3 + s - t2) - 19*t2 - 
            23*Power(t2,2) + 10*s*Power(t2,2) + 
            2*Power(s,2)*Power(t2,2) - 10*Power(t2,3) + 
            28*s*Power(t2,3) - 5*Power(s,2)*Power(t2,3) + 
            3*s*Power(t2,4) - 3*Power(s,2)*Power(t2,4) + 
            Power(t1,4)*(24 + Power(s,2) - 2*s*(-6 + t2) + 38*t2 - 
               4*Power(t2,2)) + 
            Power(t1,3)*(-13 + Power(s,2)*(5 - 4*t2) - 20*t2 - 
               21*Power(t2,2) + 3*Power(t2,3) + 
               2*s*(3 + 6*t2 + Power(t2,2))) + 
            Power(t1,2)*(-11 + 4*t2 - 4*Power(t2,2) - 12*Power(t2,3) + 
               Power(s,2)*(-6 - t2 + 2*Power(t2,2)) - 
               s*(10 + 16*t2 + 49*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s2,2)*(2*Power(t1,3) - 2*Power(t1,2)*(1 + 4*t2) + 
               2*t1*Power(t2,2)*(-1 + 5*t2) - 
               t2*(7 - 52*t2 + 20*Power(t2,2) + 10*Power(t2,3))) + 
            t1*(1 - 42*t2 - 18*Power(t2,2) - 41*Power(t2,3) + 
               Power(s,2)*t2*(-4 + 9*t2 + 4*Power(t2,2)) + 
               s*(2 + 12*t2 - 34*Power(t2,2) + 34*Power(t2,3) + 
                  3*Power(t2,4))) + 
            s2*(-2 + (33 - 2*s)*t2 - (81 + 17*s)*Power(t2,2) + 
               6*(-11 + 5*s)*Power(t2,3) + 6*(1 + 2*s)*Power(t2,4) - 
               Power(t1,4)*(11 + s + 7*t2) + 
               t1*(6 + (16 + 5*s)*t2 - 3*(14 + s)*Power(t2,2) - 
                  2*(29 + 4*s)*Power(t2,3) + 4*Power(t2,4)) + 
               Power(t1,3)*(6 + 15*t2 + 9*Power(t2,2) + s*(-5 + 4*t2)) + 
               Power(t1,2)*(1 + 38*t2 + 54*Power(t2,2) - 6*Power(t2,3) + 
                  s*(4 + 6*t2 - 5*Power(t2,2))))) + 
         Power(s1,3)*(1 - 2*(8 + (-5 + s)*s2)*t2 + 
            (66 + Power(s,2) - 101*s2 + 22*Power(s2,2) + 
               2*s*(-1 + 5*s2))*Power(t2,2) + 
            (37 - 10*Power(s,2) + 121*s2 - 80*Power(s2,2) + 
               7*s*(-4 + 7*s2))*Power(t2,3) + 
            (9*Power(s,2) - 16*s*(3 + 2*s2) + 
               3*(6 + 24*s2 + 5*Power(s2,2)))*Power(t2,4) + 
            (3*Power(s,2) + s2*(-4 + 5*s2) - s*(3 + 8*s2))*Power(t2,5) + 
            Power(t1,5)*(20 + 9*t2 - Power(t2,2) - 4*s2*(1 + t2) + 
               s*(6 + t2)) + Power(t1,4)*
             (-29 - Power(s,2)*(-3 + t2) - 31*t2 - 28*Power(t2,2) + 
               4*Power(t2,3) + 
               s*(10 + s2*(-2 + t2) - 15*t2 + 4*Power(t2,2)) + 
               s2*(4 + 25*t2 + 7*Power(t2,2))) + 
            Power(t1,3)*(-5 - 51*t2 + 4*Power(s2,2)*(-1 + t2)*t2 - 
               14*Power(t2,2) + 15*Power(t2,3) - Power(t2,4) + 
               Power(s,2)*(-1 - 2*t2 + 8*Power(t2,2)) - 
               s2*(-4 - 13*t2 + 8*Power(t2,2) + Power(t2,3)) - 
               2*s*(6 - 6*(-2 + s2)*t2 + 5*(1 + s2)*Power(t2,2) + 
                  3*Power(t2,3))) - 
            t1*(5 + (-30 + 41*s2 - 4*Power(s2,2))*t2 + 
               (-47 + 16*s2 + 4*Power(s2,2))*Power(t2,2) + 
               (11 + s2 - 12*Power(s2,2))*Power(t2,3) + 
               (-29 - 36*s2 + 10*Power(s2,2))*Power(t2,4) + 
               s2*Power(t2,5) + 
               Power(s,2)*t2*
                (-14 - 13*t2 + 14*Power(t2,2) + 6*Power(t2,3)) + 
               s*(-2 + 2*(-6 + s2)*t2 + 8*(5 + 2*s2)*Power(t2,2) - 
                  6*(11 + s2)*Power(t2,3) + (22 - 14*s2)*Power(t2,4) + 
                  Power(t2,5))) - 
            Power(t1,2)*(-18 - 59*t2 - 10*Power(t2,2) + 
               2*Power(s2,2)*(-4 + t2)*Power(t2,2) + 5*Power(t2,3) + 
               Power(t2,4) + Power(s,2)*(3 + 14*t2) + 
               s2*(4 + 3*t2 + 72*Power(t2,2) + 49*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s*(6 + 18*t2 - 4*Power(t2,2) - 32*Power(t2,3) - 
                  6*Power(t2,4) + 
                  s2*(-2 + t2 + 6*Power(t2,2) - 5*Power(t2,3))))) - 
         s1*t2*((-1 + s)*Power(t1,5)*Power(t2,2) + 
            Power(t1,4)*(4 + (-31 + 8*s2)*t2 + 
               Power(s,2)*(-3 + t2)*t2 + (-23 + 9*s2)*Power(t2,2) + 
               (3 + s2)*Power(t2,3) - 
               s*(-4 - 10*t2 + (3 + s2)*Power(t2,2) + 2*Power(t2,3))) - 
            Power(t1,3)*(12 + (-19 + 4*s2)*t2 + 
               (-49 + 19*s2)*Power(t2,2) + 
               (-22 + 5*s2 + Power(s2,2))*Power(t2,3) + 
               (2 + s2)*Power(t2,4) + 
               Power(s,2)*(-4 - 21*t2 - 2*Power(t2,2) + Power(t2,3)) - 
               s*(-4 + (-70 + 4*s2)*t2 + (10 - 4*s2)*Power(t2,2) + 
                  2*(-1 + s2)*Power(t2,3) + Power(t2,4))) + 
            Power(t1,2)*(12 - 6*(-7 + 2*s2)*t2 + 
               (59 - 59*s2 + 4*Power(s2,2))*Power(t2,2) + 
               (-30 + 23*s2 - 8*Power(s2,2))*Power(t2,3) - 
               10*Power(t2,4) - 
               Power(s,2)*(12 + 53*t2 + 6*Power(t2,2) - 
                  3*Power(t2,3) + Power(t2,4)) + 
               s*(-4 + 2*(18 + s2)*t2 + 3*(-8 + 5*s2)*Power(t2,2) + 
                  (-8 + 3*s2)*Power(t2,3) + (7 + s2)*Power(t2,4))) + 
            t1*(-4 + (-17 + 4*s2)*t2 + (-84 + 67*s2)*Power(t2,2) + 
               (14 - 19*s2 + 15*Power(s2,2))*Power(t2,3) + 
               (20 + 24*s2 - 8*Power(s2,2))*Power(t2,4) + 
               (1 + Power(s2,2))*Power(t2,5) + 
               Power(s,2)*(8 + 34*t2 + 14*Power(t2,2) - 
                  25*Power(t2,3) + Power(t2,5)) - 
               2*s*(-2 + (-13 + 2*s2)*t2 + (-27 + 17*s2)*Power(t2,2) - 
                  2*(11 + 5*s2)*Power(t2,3) + (13 - 3*s2)*Power(t2,4) + 
                  (1 + s2)*Power(t2,5))) - 
            t2*(13 + 49*Power(t2,2) - 12*Power(t2,3) + 4*Power(t2,4) + 
               Power(s2,2)*t2*
                (4 + 21*t2 - 31*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(-4 + 2*t2 + Power(t2,2) - 14*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*(-4 - 2*t2 - 95*Power(t2,2) + 38*Power(t2,3) + 
                  9*Power(t2,4)) + 
               s*(2*(1 - 3*t2 - 11*Power(t2,2) + Power(t2,3) - 
                     5*Power(t2,4)) + 
                  s2*(2 - 16*t2 + 18*Power(t2,2) + 41*Power(t2,3) - 
                     3*Power(t2,4))))) + 
         Power(s1,2)*(-(t2*(3 + 
                 2*(-14 + 4*Power(s,2) + s*(7 - 6*s2) + 6*s2 + 
                    Power(s2,2))*t2 + 
                 (92 + Power(s,2) - 143*s2 + 32*Power(s2,2) + 
                    4*s*(-3 + 5*s2))*Power(t2,2) + 
                 (17 - 18*Power(s,2) + 95*s2 - 68*Power(s2,2) + 
                    s*(-22 + 65*s2))*Power(t2,3) + 
                 (14 + 7*Power(s,2) + 40*s2 + 6*Power(s2,2) - 
                    4*s*(9 + 4*s2))*Power(t2,4) + 
                 (Power(s,2) + (-1 + s2)*s2 - s*(1 + 2*s2))*Power(t2,5))) \
+ Power(t1,5)*(2 + 2*(-7 + 2*s2)*t2 + (-13 + 4*s2)*Power(t2,2) + 
               Power(t2,3) + s*(2 + 2*t2 + Power(t2,2))) + 
            Power(t1,4)*(-6 + (-31 + 4*s2)*t2 - 
               5*(-3 + s2)*Power(t2,2) - (-16 + s2)*Power(t2,3) - 
               Power(t2,4) + Power(s,2)*(2 + 3*t2 + 3*Power(t2,2)) - 
               s*(2 - 2*(-13 + s2)*t2 + (7 + s2)*Power(t2,2) + 
                  4*Power(t2,3))) - 
            Power(t1,3)*(-6 + 3*(-31 + 4*s2)*t2 + 
               (-65 + 45*s2 - 2*Power(s2,2))*Power(t2,2) + 
               2*(-9 + 4*s2 + 2*Power(s2,2))*Power(t2,3) + 
               (11 + 3*s2)*Power(t2,4) + 
               Power(s,2)*(6 + 19*t2 + 4*Power(t2,2) + 4*Power(t2,3)) + 
               s*(2 - 4*(-9 + s2)*t2 + (-34 + 8*s2)*Power(t2,2) - 
                  8*s2*Power(t2,3) - 5*Power(t2,4))) + 
            Power(t1,2)*(-2 - 4*(10 + s2)*t2 + 
               (31 - 31*s2 + 6*Power(s2,2))*Power(t2,2) - 
               2*(11 - 33*s2 + 5*Power(s2,2))*Power(t2,3) + 
               (-2 + 19*s2 + Power(s2,2))*Power(t2,4) + 2*Power(t2,5) + 
               Power(s,2)*(4 + 11*t2 + 10*Power(t2,2) - 4*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s*(2 - 2*(-35 + s2)*t2 + (4 + s2)*Power(t2,2) + 
                  (-2 + 6*s2)*Power(t2,3) - 2*Power(t2,4) - 2*Power(t2,5)\
)) + t1*t2*(-5 - 126*t2 - Power(t2,2) + 41*Power(t2,3) - 6*Power(t2,4) + 
               Power(s2,2)*t2*
                (-6 + 16*t2 - 14*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(8 + 89*t2 - 10*Power(t2,2) + 34*Power(t2,3) - 
                  9*Power(t2,4)) + 
               Power(s,2)*(8 + 8*t2 - 23*Power(t2,2) + 8*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s*(2*(-5 + 24*t2 + 35*Power(t2,2) - 33*Power(t2,3) + 
                     Power(t2,4)) - 
                  s2*(4 + 16*t2 - 24*Power(t2,2) + 9*Power(t2,4))))))*
       T3q(t2,s1))/
     ((-1 + s2)*(-1 + t1)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)*
       Power(-s1 + t2,2)) - (8*
       (1 + (-1 + s)*Power(t1,4)*(-1 + s - t2) - 10*t2 + 4*s2*t2 - 
         2*s*s2*t2 + 27*Power(t2,2) + 6*s*Power(t2,2) + 
         Power(s,2)*Power(t2,2) - 48*s2*Power(t2,2) + 
         2*s*s2*Power(t2,2) + 7*Power(s2,2)*Power(t2,2) + 
         38*Power(t2,3) + 6*s*Power(t2,3) - 4*Power(s,2)*Power(t2,3) - 
         60*s2*Power(t2,3) + 8*s*s2*Power(t2,3) + 
         16*Power(s2,2)*Power(t2,3) - 20*Power(t2,4) + 
         20*s2*Power(t2,4) + Power(s1,6)*
          (3*Power(t1,2) - s2*t1*(-4 + t2) + Power(s2,2)*(1 + t2)) - 
         Power(t1,3)*(5 + (-2 + 3*s2)*t2 + (2 + s2)*Power(t2,2) + 
            Power(s,2)*(3 + t2) - s*(8 + 3*s2*t2 + 2*Power(t2,2))) + 
         Power(t1,2)*(8 + 10*(-1 + s2)*t2 + 2*(-1 + s2)*s2*Power(t2,2) + 
            (-7 + 9*s2)*Power(t2,3) + 
            Power(s,2)*(1 + 2*t2 - Power(t2,2)) - 
            s*(8 + 9*s2*t2 - 2*Power(t2,2) + Power(t2,3))) + 
         t1*(-5 + (17 - 11*s2)*t2 - 
            5*(9 - 12*s2 + 2*Power(s2,2))*Power(t2,2) - 
            3*(-5 + 3*s2 + 2*Power(s2,2))*Power(t2,3) - 
            8*(-1 + s2)*Power(t2,4) + 
            Power(s,2)*t2*(-2 + 5*t2 + Power(t2,2)) - 
            s*(-2 - 10*(-1 + s2)*t2 + (10 + 3*s2)*Power(t2,2) + 
               3*s2*Power(t2,3))) + 
         Power(s1,5)*(-(Power(s2,2)*
               (Power(t1,2) - t1*(2 + t2) + t2*(5 + 2*t2))) - 
            t1*(4 + Power(t1,3) - Power(t1,2)*(-2 + t2) + 5*t2 + 
               t1*(11 + 5*t2) + 
               s*(2 + 8*t1 + Power(t1,2) - 4*t2 - Power(t2,2))) + 
            s2*(-2 + 2*Power(t1,3) + Power(t1,2)*(10 + s - 2*t2) + 
               2*(-3 + s)*t2 + (1 + 2*s)*Power(t2,2) + 
               t1*(-21 + (-7 + s)*t2 + Power(t2,2)))) - 
         Power(s1,3)*(-((1 + s)*Power(t1,5)) + 
            2*t2*(13 + (10 - 11*s)*t2 + 
               2*(1 - 3*s + Power(s,2))*Power(t2,2)) - 
            Power(t1,4)*(17 + Power(s,2) + t2 - 2*s*(1 + t2)) + 
            Power(t1,2)*(18 + 5*t2 + Power(s,2)*(-2 + t2)*t2 - 
               Power(t2,2) + 4*s*(8 + 5*t2)) + 
            Power(t1,3)*(13 + 15*t2 + 2*Power(t2,2) + 
               Power(s,2)*(3 + t2) - s*(19 + 4*t2 + Power(t2,2))) + 
            Power(s2,2)*(Power(t1,3)*(-6 + t2) + 
               Power(t1,2)*(19 + 2*t2) + 
               t2*(-15 - 5*t2 + 3*Power(t2,2)) - 
               t1*(4 - 10*t2 + 2*Power(t2,2) + Power(t2,3))) + 
            t1*(29 + 58*t2 + 13*Power(t2,2) - Power(t2,3) - 
               Power(s,2)*Power(t2,2)*(5 + t2) + 
               s*(-4 + 14*t2 + 3*Power(t2,2) + 2*Power(t2,3))) + 
            s2*(-4 + Power(t1,4)*(11 + s - t2) + (23 + 4*s)*t2 + 
               (41 - 5*s)*Power(t2,2) - 7*(-1 + s)*Power(t2,3) + 
               Power(t1,3)*(8 + s - 8*t2 - 2*s*t2 + Power(t2,2)) + 
               t1*(1 - 2*(19 + 5*s)*t2 + (-2 + 9*s)*Power(t2,2) + 
                  2*s*Power(t2,3)) + 
               Power(t1,2)*(-79 - 33*t2 + Power(t2,2) - 
                  s*(9 - 5*t2 + Power(t2,2))))) + 
         s1*((-1 + s)*Power(t1,5) + 
            Power(t1,4)*(-2 + 2*s - Power(s,2) + t2) + 
            2*t2*(2 - (36 + s)*t2 + 3*(1 + Power(s,2))*Power(t2,2) + 
               2*Power(t2,3)) - 
            Power(s2,2)*t2*(4 + Power(t1,3) + 11*t2 - 5*Power(t2,2) - 
               2*Power(t1,2)*(5 + 6*t2) + 3*t1*(1 + 10*t2 + Power(t2,2))\
) + Power(t1,3)*(11 + 2*t2 + 18*Power(t2,2) + Power(s,2)*(5 + t2) - 
               s*(10 + 3*Power(t2,2))) + 
            Power(t1,2)*(-19 + 94*t2 + Power(s,2)*(-4 + t2)*t2 - 
               23*Power(t2,2) - 18*Power(t2,3) + 
               2*s*(10 + 4*t2 + Power(t2,3))) - 
            t1*(-11 + 61*t2 + 57*Power(t2,2) - 23*Power(t2,3) + 
               Power(s,2)*Power(t2,2)*(7 + t2) + 
               2*s*(1 - 5*t2 + 3*Power(t2,2) + Power(t2,3))) + 
            s2*(-2 + (13 + 2*s)*t2 + (92 - 15*s)*Power(t2,2) - 
               (13 + 15*s)*Power(t2,3) - 4*Power(t2,4) + 
               Power(t1,4)*(1 - s + t2) + 
               Power(t1,3)*(s - 4*s*t2 + (2 - 15*t2)*t2) + 
               t1*(6 + (73 - 11*s)*t2 + 9*(11 + s)*Power(t2,2) + 
                  2*(-9 + 2*s)*Power(t2,3)) + 
               Power(t1,2)*(-5 - 107*t2 + 7*Power(t2,2) + 
                  14*Power(t2,3) + s*(-2 + 13*t2 + Power(t2,2))))) + 
         Power(s1,4)*(1 - Power(t1,5) + Power(t1,4)*(-3 + t2) + 5*t2 + 
            2*Power(t2,2) - 6*s*Power(t2,2) + Power(s,2)*Power(t2,2) - 
            s*Power(t2,3) + Power(s,2)*Power(t2,3) + 
            Power(t1,3)*(16 + Power(s,2) + 3*t2 - 2*s*(2 + t2)) - 
            Power(s2,2)*(2 + Power(t1,3) + Power(t1,2)*(-8 + t2) + 
               4*t2 - 7*Power(t2,2) - Power(t2,3) + 
               t1*(5 + 2*Power(t2,2))) + 
            Power(t1,2)*(11 - Power(s,2)*(-1 + t2) + 8*t2 - 
               2*Power(t2,2) + s*(30 + 11*t2 + 2*Power(t2,2))) + 
            t1*(21 + 29*t2 + 9*Power(t2,2) - Power(s,2)*t2*(2 + t2) - 
               2*s*(-1 + 2*t2 + 5*Power(t2,2))) + 
            s2*(-7*Power(t1,3) + 2*Power(t1,4) + 
               t1*(27 + (15 - 2*s)*t2 + (7 + 3*s)*Power(t2,2)) + 
               Power(t1,2)*(-47 - 11*t2 + s*(-7 + 3*t2)) - 
               t2*(-31 - 11*t2 + Power(t2,2) + 
                  s*(2 + 11*t2 + 2*Power(t2,2))))) + 
         Power(s1,2)*(-2 + (1 - 2*s)*Power(t1,5) + 27*t2 + 
            55*Power(t2,2) - 20*s*Power(t2,2) - 
            2*Power(s,2)*Power(t2,2) + 8*Power(t2,3) - 17*s*Power(t2,3) + 
            Power(s,2)*Power(t2,3) + 
            Power(t1,3)*(-39 - 9*t2 + Power(s,2)*t2 + 10*Power(t2,2) - 
               2*s*(6 + t2)) - 
            Power(t1,4)*(4 + Power(s,2) + 12*t2 - s*(2 + 3*t2)) - 
            Power(s2,2)*(-1 + 3*t2 + 14*Power(t2,2) + 3*Power(t2,3) + 
               Power(t1,3)*(5 + 6*t2) + t1*(1 - 28*t2 - 8*Power(t2,2)) - 
               Power(t1,2)*(4 + 9*t2 + 2*Power(t2,2))) + 
            Power(t1,2)*(18 + 30*t2 + 16*Power(t2,2) + Power(t2,3) + 
               Power(s,2)*(-2 + t2 + Power(t2,2)) - 
               s*(2 - t2 + 4*Power(t2,2) + Power(t2,3))) + 
            t1*(6 + 94*t2 - 22*Power(t2,2) - 7*Power(t2,3) - 
               Power(s,2)*t2*(-4 + 2*t2 + Power(t2,2)) + 
               2*s*(-2 + 7*t2 + 14*Power(t2,2) + 2*Power(t2,3))) + 
            s2*(2*Power(t1,4)*(s + 3*t2) + 
               Power(t1,3)*(45 - (-17 + s)*t2 - 7*Power(t2,2)) - 
               Power(t1,2)*(21 + s + 51*t2 + 2*s*t2 + 12*Power(t2,2) + 
                  2*s*Power(t2,2) - Power(t2,3)) + 
               t1*(-15 - (139 + 8*s)*t2 - 9*Power(t2,2) + 
                  (3 + s)*Power(t2,3)) + 
               t2*(-19 + t2 + 17*Power(t2,2) + 
                  s*(4 + 17*t2 + 2*Power(t2,2))))))*T4q(s1))/
     ((-1 + s1)*(-1 + s2)*(-1 + t1)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*
       (-1 + t2)) - (8*(1 - (-1 + s)*Power(t1,5)*(-1 + s - t2) - 9*t2 + 
         4*s2*t2 - 2*s*s2*t2 + 9*Power(t2,2) + 14*s*Power(t2,2) + 
         Power(s,2)*Power(t2,2) - 32*s2*Power(t2,2) - 8*s*s2*Power(t2,2) + 
         7*Power(s2,2)*Power(t2,2) + 21*Power(t2,3) - 8*s*Power(t2,3) - 
         3*Power(s,2)*Power(t2,3) - 22*s2*Power(t2,3) + 
         18*s*s2*Power(t2,3) - Power(s2,2)*Power(t2,3) - 6*Power(t2,4) + 
         6*s*Power(t2,4) - 4*Power(s,2)*Power(t2,4) + 6*s2*Power(t2,4) + 
         10*s*s2*Power(t2,4) - 6*Power(s2,2)*Power(t2,4) - 
         Power(s1,5)*(3*Power(t1,2) - s2*t1*(-4 + t2) + 
            Power(s2,2)*(1 + t2)) + 
         Power(t1,4)*(2*Power(s,2)*(2 + t2) + s2*t2*(3 + t2) + 
            3*(2 + Power(t2,2)) - s*(10 + 3*(1 + s2)*t2 + 3*Power(t2,2))) \
- Power(t1,3)*(13 + (-7 + 13*s2)*t2 + 2*s2*(1 + s2)*Power(t2,2) + 
            (3 + 2*s2)*Power(t2,3) + Power(s,2)*(4 + 6*t2) - 
            s*(16 + 4*(2 + 3*s2)*t2 + 3*s2*Power(t2,2) + 3*Power(t2,3))) + 
         Power(t1,2)*(13 + (-19 + 21*s2)*t2 + 
            (-9 - 6*s2 + 8*Power(s2,2))*Power(t2,2) + 
            (-2 + 4*s2)*Power(t2,3) + (1 + s2)*Power(t2,4) + 
            Power(s,2)*(1 + 5*t2 - 4*Power(t2,2) - 2*Power(t2,3)) + 
            s*(-10 + (2 - 19*s2)*t2 - 4*(-2 + s2)*Power(t2,2) + 
               (1 + 3*s2)*Power(t2,3) - Power(t2,4))) + 
         t1*(-6 + (22 - 15*s2)*t2 + 
            (-3 + 39*s2 - 13*Power(s2,2))*Power(t2,2) + 
            s2*(7 + 2*s2)*Power(t2,3) + 
            (3 - 5*s2 + 2*Power(s2,2))*Power(t2,4) + 
            Power(s,2)*t2*(-2 + 2*t2 + 10*Power(t2,2) + Power(t2,3)) - 
            s*(-2 + (8 - 12*s2)*t2 + (14 - 5*s2)*Power(t2,2) + 
               6*(2 + 3*s2)*Power(t2,3) + 3*s2*Power(t2,4))) + 
         Power(s1,4)*(Power(s2,2)*
             (-1 + Power(t1,2) + 5*t2 + 3*Power(t2,2) - t1*(3 + 2*t2)) + 
            t1*(4 + Power(t1,3) + 5*t2 + 8*t1*(1 + t2) - 
               Power(t1,2)*(1 + t2) + 
               s*(2 + 8*t1 + Power(t1,2) - 4*t2 - Power(t2,2))) - 
            s2*(-2 + 2*Power(t1,3) + Power(t1,2)*(14 + s - 3*t2) + 
               2*(-3 + s)*t2 + (1 + 2*s)*Power(t2,2) + 
               t1*(-17 + (-12 + s)*t2 + 2*Power(t2,2)))) + 
         Power(s1,3)*(-1 + 2*Power(t1,5) + Power(t1,4)*(8 + s - 3*t2) - 
            5*t2 - 2*Power(t2,2) + 6*s*Power(t2,2) - 
            Power(s,2)*Power(t2,2) + s*Power(t2,3) - 
            Power(s,2)*Power(t2,3) + 
            t1*(-17 + 2*(-14 - s + Power(s,2))*t2 + 
               (-14 + 13*s + Power(s,2))*Power(t2,2) + s*Power(t2,3)) + 
            Power(t1,3)*(-17 - Power(s,2) + t2 + Power(t2,2) + 
               s*(13 + t2)) + 
            Power(t1,2)*(9 + Power(s,2)*(-1 + t2) + t2 - 3*Power(t2,2) - 
               s*(20 + 23*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(1 + 2*Power(t1,3) + 10*t2 - 8*Power(t2,2) - 
               3*Power(t2,3) - Power(t1,2)*(5 + t2) + 
               t1*(1 + 4*t2 + 5*Power(t2,2))) - 
            s2*(-2 + 4*Power(t1,4) + Power(t1,3)*(11 + s - 4*t2) + 
               27*t2 + (18 - 11*s)*Power(t2,2) - 
               2*(1 + 2*s)*Power(t2,3) + 
               Power(t1,2)*(-34 + 3*s*(-2 + t2) - 26*t2 + 
                  3*Power(t2,2)) + 
               t1*(8 + (14 + s)*t2 + 2*(9 + 2*s)*Power(t2,2) - 
                  Power(t2,3)))) + 
         Power(s1,2)*(-1 + Power(t1,6) + 22*t2 + 23*Power(t2,2) - 
            16*s*Power(t2,2) - Power(s,2)*Power(t2,2) + 6*Power(t2,3) - 
            17*s*Power(t2,3) + 4*Power(s,2)*Power(t2,3) - s*Power(t2,4) + 
            Power(s,2)*Power(t2,4) - Power(t1,5)*(-5 + s + 2*t2) + 
            Power(t1,4)*(-17 - 2*Power(s,2) - 12*t2 + Power(t2,2) + 
               s*(6 + 4*t2)) + 
            Power(t1,3)*(15 + 7*t2 + 9*Power(t2,2) + 
               Power(s,2)*(1 + 3*t2) - s*(16 + 19*t2 + 5*Power(t2,2))) + 
            t1*(11 + 26*t2 + 13*Power(t2,2) + 8*Power(t2,3) + 
               Power(s,2)*t2*(2 - 7*t2 - 3*Power(t2,2)) + 
               s*(-4 + 12*t2 + 22*Power(t2,2) - 5*Power(t2,3))) + 
            Power(t1,2)*(-14 + 7*t2 - 16*Power(t2,2) - 2*Power(t2,3) + 
               Power(s,2)*(-1 + 2*t2 + Power(t2,2)) + 
               s*(20 - t2 + 19*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s2,2)*(1 + Power(t1,4) + Power(t1,3)*(-5 + t2) - 
               6*t2 - 21*Power(t2,2) + 5*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(5 - 3*t2 + Power(t2,2)) - 
               t1*(2 - 11*t2 + Power(t2,2) + 4*Power(t2,3))) + 
            s2*(-2 - 2*Power(t1,5) + (-6 + 4*s)*t2 + 
               6*(8 + s)*Power(t2,2) - 3*(-7 + 4*s)*Power(t2,3) - 
               (1 + 2*s)*Power(t2,4) + Power(t1,4)*(2 + s + t2) + 
               Power(t1,3)*(18 + s*(6 - 5*t2) + 16*t2 + Power(t2,2)) + 
               t1*(-5 - (35 + 11*s)*t2 + 13*(-1 + s)*Power(t2,2) + 
                  (10 + 7*s)*Power(t2,3)) - 
               Power(t1,2)*(11 + 15*t2 + 21*Power(t2,2) + 
                  s*(11 - 5*t2 + Power(t2,2))))) + 
         s1*(1 - (-1 + s)*Power(t1,6) - 4*t2 - 45*Power(t2,2) + 
            4*s*Power(t2,2) + Power(s,2)*Power(t2,2) - 16*Power(t2,3) + 
            16*s*Power(t2,3) + 4*Power(s,2)*Power(t2,3) - 4*Power(t2,4) + 
            10*s*Power(t2,4) - 2*Power(s,2)*Power(t2,4) + 
            t1*(4 - (11 + 14*s + 2*Power(s,2))*t2 + 
               (-1 + 4*s - 8*Power(s,2))*Power(t2,2) + 
               (21 - 36*s + 5*Power(s,2))*Power(t2,3) + 
               Power(-1 + s,2)*Power(t2,4)) - 
            Power(t1,5)*(1 + Power(s,2) + 4*t2 - 3*s*(1 + t2)) + 
            Power(t1,4)*(-2 - 4*t2 + 5*Power(t2,2) + 
               Power(s,2)*(1 + 2*t2) - 3*s*(2 + t2 + Power(t2,2))) + 
            Power(t1,3)*(9 + 19*t2 - Power(s,2)*t2 + 13*Power(t2,2) - 
               2*Power(t2,3) + s*(-6 + 6*t2 - 5*Power(t2,2) + Power(t2,3))\
) + Power(s2,2)*t2*(-4 + Power(t1,4) + 4*t2 + 18*Power(t2,2) - 
               Power(t2,3) - Power(t1,3)*(3 + t2) + 
               Power(t1,2)*(5 + 2*t2 - Power(t2,2)) + 
               t1*(1 - 8*t2 - 2*Power(t2,2) + Power(t2,3))) + 
            Power(t1,2)*(-12 + 4*t2 - 20*Power(t2,2) - 9*Power(t2,3) + 
               Power(s,2)*(1 + 4*t2 - 3*Power(t2,2) - 2*Power(t2,3)) + 
               s*(10 - 2*t2 + 26*Power(t2,2) + 7*Power(t2,3))) + 
            s2*(-2 + Power(t1,5)*(-1 + s - t2) + 15*t2 + 
               (33 - 15*s)*Power(t2,2) - (31 + 18*s)*Power(t2,3) + 
               3*(-3 + s)*Power(t2,4) + 
               t1*(8 + (23 + 17*s)*t2 + 2*(8 + 5*s)*Power(t2,2) + 
                  (21 - 5*s)*Power(t2,3) - 2*s*Power(t2,4)) + 
               Power(t1,4)*(1 + 6*t2 + 2*Power(t2,2) - s*(2 + 3*t2)) + 
               Power(t1,3)*(5 - 6*t2 - 7*Power(t2,2) - Power(t2,3) + 
                  s*(3 + t2 + Power(t2,2))) + 
               Power(t1,2)*(-11 - 37*t2 - 5*Power(t2,2) + 2*Power(t2,3) + 
                  s*(-2 - 7*t2 + 3*Power(t2,2) + 3*Power(t2,3))))))*
       T5q(1 - s1 - t1 + t2))/
     ((-1 + s2)*(-1 + t1)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)));
   return a;
};
