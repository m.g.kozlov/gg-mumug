#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_2_m231_3_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(2*(-1 + s2 + Power(s2,2))*(-1 + t2)*Power(t2,2)*(-1 + s2 + t2) + 
         2*Power(t1,5)*(1 + s1 - s1*t2 + (7 + 10*s - 4*s2 - 4*s*s2)*t2 - 
            2*(-2 + s)*Power(t2,2)) + 
         Power(t1,4)*(5 - 42*t2 - 17*s2*t2 + 8*Power(s2,2)*t2 + 
            Power(s,2)*(21 - 8*s2 - 5*t2)*t2 + 5*Power(t2,2) - 
            15*s2*Power(t2,2) + 8*Power(s2,2)*Power(t2,2) - 
            16*Power(t2,3) + Power(s1,2)*(1 + t2 - 2*Power(t2,2)) + 
            s1*(-1 + t2)*(-4 + 5*t2 + s2*(2 + t2)) + 
            s*(-1 + (17 - 27*s2 + 8*Power(s2,2))*t2 + 
               (-40 + 19*s2)*Power(t2,2) + 8*Power(t2,3) + 
               s1*(1 - 4*t2 + 3*Power(t2,2)))) + 
         t1*t2*(-11 - 2*t2 - 5*s*t2 - 8*Power(s2,4)*(-1 + t2)*t2 + 
            48*Power(t2,2) - 4*s*Power(t2,2) - 8*Power(s,2)*Power(t2,2) - 
            16*Power(t2,3) + 5*s*Power(t2,3) - Power(s,2)*Power(t2,3) - 
            19*Power(t2,4) + 4*s*Power(t2,4) + Power(s,2)*Power(t2,4) - 
            Power(s2,3)*(10 + (26 + 8*s)*t2 - (37 + 8*s)*Power(t2,2) + 
               9*Power(t2,3)) + 
            Power(s2,2)*(18 + 4*(-6 + s)*t2 - (23 + 9*s)*Power(t2,2) + 
               13*(4 + s)*Power(t2,3) + Power(t2,4)) + 
            s2*(3 + (51 + 9*s)*t2 + 
               (-45 - 17*s + 4*Power(s,2))*Power(t2,2) - 
               (21 + 6*s + 4*Power(s,2))*Power(t2,3) - 
               2*(-6 + s)*Power(t2,4)) + 
            Power(s1,2)*(-1 + t2)*
             (10 - 14*t2 + Power(s2,2)*(5 + 2*t2) + s2*(-15 + 11*t2)) - 
            s1*(-1 + t2)*(29 - 3*(6 + s)*t2 + (-47 + 5*s)*Power(t2,2) + 
               3*Power(s2,3)*(2 + t2) + 
               Power(s2,2)*(7 + (33 + 4*s)*t2 + 7*Power(t2,2)) - 
               s2*(42 + (20 + s)*t2 + (-40 + 3*s)*Power(t2,2)))) + 
         Power(t1,3)*(4 - s2 - 2*Power(s2,2) + 13*t2 + 3*s2*t2 - 
            19*Power(s2,2)*t2 + 8*Power(s2,3)*t2 + 58*Power(t2,2) + 
            42*s2*Power(t2,2) + 20*Power(s2,2)*Power(t2,2) - 
            16*Power(s2,3)*Power(t2,2) - 59*Power(t2,3) + 
            44*s2*Power(t2,3) - 7*Power(s2,2)*Power(t2,3) + 
            8*Power(t2,4) + Power(s1,2)*(-1 + t2)*
             (1 + 2*s2*t2 + 2*Power(t2,2)) + 
            s1*(-1 + t2)*(-3 + s2 + 40*t2 - 42*s2*t2 + 
               6*Power(s2,2)*t2 - 6*Power(t2,2) + 4*s2*Power(t2,2)) + 
            Power(s,2)*t2*(-8 - 43*t2 + 11*Power(t2,2) + 
               4*s2*(1 + 3*t2)) + 
            s*(-1 - 6*t2 - 29*Power(t2,2) + 24*Power(t2,3) - 
               4*Power(t2,4) - Power(s2,2)*t2*(5 + 3*t2) + 
               s1*(-1 + t2)*(-1 + s2 - 3*t2 + 3*s2*t2 - 6*Power(t2,2)) + 
               s2*(1 - 9*t2 + 48*Power(t2,2) - 16*Power(t2,3)))) + 
         Power(t1,2)*(1 - 6*t2 + 6*s*t2 + 8*Power(s2,4)*(-1 + t2)*t2 - 
            52*Power(t2,2) + 11*s*Power(t2,2) + 
            16*Power(s,2)*Power(t2,2) - 2*Power(t2,3) + 7*s*Power(t2,3) + 
            23*Power(s,2)*Power(t2,3) + 59*Power(t2,4) - 8*s*Power(t2,4) - 
            7*Power(s,2)*Power(t2,4) + 
            Power(s2,3)*t2*(30 - 8*s*(-1 + t2) - 33*t2 + 19*Power(t2,2)) + 
            s2*(-1 - 2*(16 + 5*s)*t2 + 
               (34 + 26*s - 8*Power(s,2))*Power(t2,2) - 
               (16 + 15*s)*Power(t2,3) + (-33 + 7*s)*Power(t2,4)) + 
            Power(s1,2)*(-1 + t2)*
             (2 + 19*t2 - 5*Power(t2,2) + Power(s2,2)*(1 + 4*t2) + 
               s2*(-3 - 23*t2 + 4*Power(t2,2))) - 
            2*Power(s2,2)*t2*(-5 - 11*t2 + 31*Power(t2,2) + Power(t2,3) + 
               s*(2 - 7*t2 + 9*Power(t2,2))) - 
            s1*(-1 + t2)*(1 + (33 + 2*s)*t2 + 3*Power(s2,3)*t2 + 
               (71 - 9*s)*Power(t2,2) - 3*(1 + s)*Power(t2,3) - 
               Power(s2,2)*(1 + (27 + 4*s)*t2 + Power(t2,2)) + 
               s2*t2*(-9 - 68*t2 + 5*Power(t2,2) + s*(2 + 6*t2))))))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*t1*Power(t1 - t2,2)*(-1 + t2)*t2*
       (-1 + s2 - t1 + t2)) + (8*
       (8*Power(s2,6)*Power(t2,2) - 
         (-1 + t2)*Power(1 + t2,3)*Power(1 + (-1 + s)*t2,2) + 
         Power(t1,5)*(-((-1 + s1*(-3 + t2) - t2)*(2 + t2)) + 
            s*(-2 - t2 + Power(t2,2))) + 
         Power(t1,3)*(15 - 43*t2 - 13*Power(t2,2) + 21*Power(t2,3) + 
            Power(s,2)*(-9 + 13*t2 + 27*Power(t2,2) + 2*Power(t2,3) - 
               3*Power(t2,4)) + 
            s1*(22 - 57*t2 + 6*Power(t2,2) + 15*Power(t2,3) - 
               2*Power(t2,4)) - 
            Power(s1,2)*(5 + t2 - 8*Power(t2,2) - 3*Power(t2,3) + 
               Power(t2,4)) + 
            s*(10 + 6*(12 + s1)*t2 - (45 + 34*s1)*Power(t2,2) - 
               2*(15 + 4*s1)*Power(t2,3) + (7 + 4*s1)*Power(t2,4))) - 
         t1*(1 + t2)*(Power(s,2)*t2*
             (4 + 8*t2 - 10*Power(t2,2) - 5*Power(t2,3) + Power(t2,4)) + 
            Power(-1 + t2,2)*
             (9 + 7*t2 - 7*Power(t2,2) + Power(t2,3) - 
               2*s1*Power(1 + t2,2)) + 
            s*(2 + 9*t2 - 20*Power(t2,2) - 5*Power(t2,3) + 
               16*Power(t2,4) - 2*Power(t2,5) + 
               2*s1*Power(-1 + Power(t2,2),2))) + 
         Power(t1,4)*(26 - 6*t2 - 11*Power(t2,2) - Power(t2,3) + 
            Power(s,2)*(-4 - 9*t2 + Power(t2,3)) + 
            s1*(12 - 9*t2 - 10*Power(t2,2) + Power(t2,3)) + 
            Power(s1,2)*(2 - 8*t2 - Power(t2,2) + Power(t2,3)) + 
            s*(-24 + 26*t2 + 12*Power(t2,2) - 4*Power(t2,3) + 
               s1*(6 + 17*t2 + Power(t2,2) - 2*Power(t2,3)))) + 
         Power(t1,2)*(Power(s,2)*
             (3 + 21*t2 - 13*Power(t2,2) - 31*Power(t2,3) - 
               5*Power(t2,4) + 3*Power(t2,5)) - 
            (-1 + t2)*(13 - 55*t2 - 6*Power(t2,2) + 17*Power(t2,3) - 
               Power(t2,4) + Power(s1,2)*Power(1 + t2,3) + 
               s1*(-14 - 17*t2 + 8*Power(t2,2) + 9*Power(t2,3) - 
                  2*Power(t2,4))) + 
            s*(-2 + 2*t2 - 71*Power(t2,2) + 30*Power(t2,3) + 
               31*Power(t2,4) - 6*Power(t2,5) + 
               s1*(8 - 15*t2 - 9*Power(t2,2) + 17*Power(t2,3) + 
                  9*Power(t2,4) - 2*Power(t2,5)))) - 
         Power(s2,5)*(Power(s1,2)*(1 + Power(t2,2)) + 
            s1*t2*(-2 + t1 + 2*t2 + t1*t2 - 2*Power(t2,2) - 
               s*(1 + t2)) + t2*
             (t1*(16 + 19*t2 - Power(t2,2)) + 
               t2*(-1 - 20*t2 + Power(t2,2) + s*(5 + t2)))) + 
         Power(s2,4)*(Power(s1,2)*
             (4 + (-1 + 2*t1)*t2 + (1 + 2*t1)*Power(t2,2) - 
               2*Power(t2,3)) - 
            Power(t1,2)*(-8 + (-38 + s)*t2 + (-12 + s)*Power(t2,2) + 
               2*Power(t2,3)) + 
            t1*t2*(-1 - 45*t2 - 28*Power(t2,2) + 4*Power(t2,3) + 
               Power(s,2)*(1 + t2) + s*(10 + 11*t2 + 3*Power(t2,2))) + 
            t2*(-2 - 13*t2 + Power(s,2)*(-3 + t2)*t2 + 15*Power(t2,2) + 
               18*Power(t2,3) - 2*Power(t2,4) - 
               s*(-1 + 8*t2 + 8*Power(t2,2) + Power(t2,3))) + 
            s1*(2 - (7 + 2*s)*t2 + (22 + 7*s)*Power(t2,2) + 
               (-11 + s)*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,2)*(1 + 4*t2 + 3*Power(t2,2)) - 
               t1*(2 - 7*t2 + Power(t2,2) + 6*Power(t2,3) + 
                  s*(3 + 2*t2 + 3*Power(t2,2))))) + 
         Power(s2,3)*(-1 + 7*t2 - 8*s*t2 - 12*Power(t2,2) + 
            21*s*Power(t2,2) + 3*Power(s,2)*Power(t2,2) + 
            7*Power(t2,3) - 27*s*Power(t2,3) - 
            9*Power(s,2)*Power(t2,3) + 16*Power(t2,4) - s*Power(t2,4) + 
            2*Power(s,2)*Power(t2,4) + 4*Power(t2,5) + s*Power(t2,5) - 
            Power(t2,6) + Power(t1,3)*
             (-19 + s - 27*t2 + 3*s*t2 + Power(t2,2) + 
               2*s*Power(t2,2) + Power(t2,3)) - 
            Power(s1,2)*(4 + 2*Power(t1,2)*(-1 + t2) + 2*t2 - 
               3*Power(t2,2) + Power(t2,4) - 3*t1*t2*(-1 + Power(t2,2))) \
- Power(t1,2)*(Power(s,2)*(2 + t2 + Power(t2,2)) + 
               4*t2*(-7 - 14*t2 - Power(t2,2) + Power(t2,3)) + 
               s*(5 + 17*t2 + 5*Power(t2,2) + 7*Power(t2,3))) + 
            t1*(2 + 23*t2 - 19*Power(t2,2) - 61*Power(t2,3) - 
               5*Power(t2,4) + 4*Power(t2,5) + 
               Power(s,2)*t2*(5 + 2*t2 + Power(t2,2)) + 
               s*(1 + 21*t2 + 26*Power(t2,2) + 6*Power(t2,4))) - 
            s1*(8 - (17 + 3*s)*t2 + (4 + 9*s)*Power(t2,2) - 
               (23 + 15*s)*Power(t2,3) + (14 + s)*Power(t2,4) - 
               2*Power(t2,5) + 3*Power(t1,3)*Power(1 + t2,2) - 
               Power(t1,2)*(-5 - 15*t2 + Power(t2,2) + 7*Power(t2,3) + 
                  s*(-1 + 4*t2 + Power(t2,2))) + 
               t1*(-7 + 52*t2 - 6*Power(t2,2) - 22*Power(t2,3) + 
                  7*Power(t2,4) + 
                  s*(-12 + 19*t2 + 4*Power(t2,2) + 5*Power(t2,3))))) + 
         Power(s2,2)*(4 - 16*t2 + 13*s*t2 + 7*Power(t2,2) - 
            11*s*Power(t2,2) + Power(s,2)*Power(t2,2) - 23*Power(t2,3) + 
            5*s*Power(t2,3) + 35*Power(t2,4) - 32*s*Power(t2,4) - 
            8*Power(s,2)*Power(t2,4) - 5*Power(t2,5) + 8*s*Power(t2,5) + 
            Power(s,2)*Power(t2,5) - 2*Power(t2,6) + s*Power(t2,6) - 
            Power(t1,4)*(2*(-7 - 2*t2 + Power(t2,2)) + 
               s*(2 + 3*t2 + Power(t2,2))) + 
            Power(t1,3)*(-3 - 24*t2 - 16*Power(t2,2) + 10*Power(t2,3) + 
               Power(t2,4) + Power(s,2)*(-2 + t2 - Power(t2,2)) + 
               s*(7 - 6*t2 + 11*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s1,2)*(-2*Power(t1,3)*t2*(1 + t2) - 
               (-1 + t2)*Power(1 + t2,3) + 
               Power(t1,2)*(-10 + t2 - 4*Power(t2,2) + Power(t2,3)) + 
               t1*(3 + 3*t2 + 2*Power(t2,2) + 3*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(t1,2)*(Power(s,2)*
                (4 - 8*t2 - 3*Power(t2,2) + Power(t2,3)) - 
               s*(11 + 28*t2 + 5*Power(t2,2) - 5*Power(t2,3) + 
                  11*Power(t2,4)) - 
               2*(5 - 2*t2 - 46*Power(t2,2) - Power(t2,3) + 
                  7*Power(t2,4) + Power(t2,5))) + 
            t1*(-7 + 32*t2 - 30*Power(t2,2) - 67*Power(t2,3) + 
               2*Power(t2,4) + 9*Power(t2,5) + Power(t2,6) - 
               Power(s,2)*t2*
                (11 - 22*t2 - 4*Power(t2,2) + Power(t2,3)) + 
               s*(-2 - 49*t2 + 67*Power(t2,2) + 46*Power(t2,3) - 
                  25*Power(t2,4) + 5*Power(t2,5))) + 
            s1*(8 - 2*(4 + s)*t2 - (21 + s)*Power(t2,2) + 
               (13 + 3*s)*Power(t2,3) + (13 + 9*s)*Power(t2,4) - 
               (5 + s)*Power(t2,5) + 
               Power(t1,4)*(3 + 4*t2 + Power(t2,2)) + 
               Power(t1,3)*(14 + 18*t2 - 4*Power(t2,3) + 
                  s*(3 + 2*t2 + 3*Power(t2,2))) + 
               Power(t1,2)*(24 + 50*t2 - 50*Power(t2,2) - 
                  16*Power(t2,3) + 4*Power(t2,4) + 
                  s*(20 + 3*t2 + 14*Power(t2,2) - Power(t2,3))) - 
               t1*(15 - 27*t2 + 64*Power(t2,2) + 15*Power(t2,3) - 
                  21*Power(t2,4) + 2*Power(t2,5) + 
                  s*(19 - 26*t2 + 44*Power(t2,2) + 8*Power(t2,3) + 
                     3*Power(t2,4))))) + 
         s2*(Power(t1,5)*(-3 + s + t2 + s*t2) + 
            (1 + t2)*(-4 + (14 - 8*s)*t2 + 
               (-4 + 11*s - 2*Power(s,2))*Power(t2,2) + 
               (-22 + 3*Power(s,2))*Power(t2,3) + 
               (20 - 9*s - 3*Power(s,2))*Power(t2,4) + 
               (-4 + 6*s)*Power(t2,5)) + 
            Power(t1,4)*(-3 + 6*t2 - 9*Power(t2,2) - 2*Power(t2,3) + 
               Power(s,2)*t2*(3 + t2) - 
               s*(-8 + 2*t2 + 5*Power(t2,2) + Power(t2,3))) + 
            Power(s1,2)*t1*(2*(-1 + t2)*Power(1 + t2,3) + 
               Power(t1,3)*(-1 + 2*t2 + Power(t2,2)) + 
               Power(t1,2)*(4 + 11*t2 + 4*Power(t2,2) - 3*Power(t2,3)) + 
               t1*(6 - 13*Power(t2,2) - 6*Power(t2,3) + Power(t2,4))) + 
            Power(t1,3)*(-4*Power(s,2)*
                (-4 - t2 + Power(t2,2) + Power(t2,3)) + 
               t2*(-75 + 11*t2 + 21*Power(t2,2) + 3*Power(t2,3)) + 
               s*(6 + 26*t2 - 35*Power(t2,2) + Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(t1,2)*(-17 + 8*t2 + 91*Power(t2,2) + 7*Power(t2,3) - 
               28*Power(t2,4) - Power(t2,5) + 
               Power(s,2)*(-5 - 27*t2 - 13*Power(t2,2) + 
                  5*Power(t2,4)) + 
               s*(20 - 28*t2 - 101*Power(t2,2) + 55*Power(t2,3) + 
                  17*Power(t2,4) - 5*Power(t2,5))) + 
            t1*(15 - 32*t2 + 94*Power(t2,2) - 72*Power(t2,3) - 
               21*Power(t2,4) + 16*Power(t2,5) + 
               Power(s,2)*t2*
                (9 + 2*t2 + 15*Power(t2,2) + 4*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s*(3 + 25*t2 - 43*Power(t2,2) + 79*Power(t2,3) - 
                  14*Power(t2,4) - 20*Power(t2,5) + 2*Power(t2,6))) - 
            s1*(Power(t1,5)*(1 + t2) + 
               2*Power(-1 + t2,2)*Power(1 + t2,3) - 
               Power(t1,4)*(-13 + s - 13*t2 - 5*s*t2 + 3*Power(t2,2) - 
                  2*s*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(45 - 18*t2 - 32*Power(t2,2) - 
                  4*Power(t2,3) + Power(t2,4) + 
                  s*(22 + 31*t2 + 2*Power(t2,2) - 7*Power(t2,3))) + 
               Power(t1,2)*(-1 - 13*t2 - 62*Power(t2,2) + 
                  23*Power(t2,3) + 5*Power(t2,4) + 
                  s*(29 - 65*t2 - 27*Power(t2,2) - 13*Power(t2,3) + 
                     4*Power(t2,4))) + 
               t1*(-(Power(1 + t2,2)*
                     (6 - t2 - 8*Power(t2,2) + 3*Power(t2,3))) + 
                  s*(-12 - t2 + 10*Power(t2,2) + 16*Power(t2,3) + 
                     2*Power(t2,4) + Power(t2,5))))))*
       B1(1 - s2 + t1 - t2,t2,t1))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s2 - t1 + t2)*
       Power(-t1 + s2*t2,2)) - 
    (8*(4*Power(s2,7)*t1*t2*(Power(t1,2) - Power(t2,2)) - 
         Power(s2,6)*(Power(t1,3)*(20 + 4*s + 3*s1 - 19*t2)*t2 + 
            2*Power(t2,3) + 2*Power(t1,4)*(2 + 7*t2) + 
            t1*Power(t2,2)*(-8 + s1 - 13*t2 - 4*s*t2 + 2*s1*t2 + 
               14*Power(t2,2)) + 
            Power(t1,2)*t2*(-6 - 17*t2 - 3*Power(t2,2) + s1*(5 + t2))) + 
         Power(s2,5)*(-6*Power(t2,4) + 2*Power(t1,5)*(7 + 11*t2) + 
            Power(t1,4)*(20 + 3*s1 + 2*Power(s1,2) - 2*t2 - 
               73*Power(t2,2) + s*(4 + 11*t2)) + 
            Power(t1,3)*(-6 + 5*(-1 + s)*t2 - 2*Power(s1,2)*t2 - 
               4*(19 + 6*s)*Power(t2,2) + 56*Power(t2,3) + 
               s1*(5 + (19 + 4*s)*t2 + 5*Power(t2,2))) - 
            t1*Power(t2,2)*(1 - (33 + s)*t2 - (55 + 14*s)*Power(t2,2) + 
               18*Power(t2,3) + s1*(2 + 13*t2 + 6*Power(t2,2))) + 
            Power(t1,2)*t2*(-23 - 2*(12 + 5*s)*t2 - 
               (-45 + s)*Power(t2,2) + Power(t2,3) + 
               6*Power(s1,2)*(1 + t2) - 
               s1*(3 + (21 + 4*s)*t2 + 11*Power(t2,2)))) + 
         Power(s2,3)*(12*Power(t1,7) - 
            2*Power(t2,3)*(1 - 5*t2 + 2*Power(t2,2) + Power(t2,3)) + 
            Power(t1,6)*(-27 + 2*Power(s,2) - 2*Power(s1,2) - 138*t2 - 
               16*Power(t2,2) + s*(15 + 8*t2) - s1*(21 + 10*t2)) + 
            Power(t1,5)*(-100 + 2*Power(s1,2)*(-3 + t2) - 257*t2 - 
               4*Power(s,2)*t2 + 188*Power(t2,2) + 70*Power(t2,3) + 
               s*(-17 + 4*s1*(-3 + t2) - 98*t2 - 54*Power(t2,2)) + 
               s1*(6 + 23*t2 + 70*Power(t2,2))) - 
            Power(t1,4)*(5 - 34*t2 - 456*Power(t2,2) + 14*Power(t2,3) + 
               93*Power(t2,4) - 
               2*Power(s1,2)*(10 - 15*t2 + 9*Power(t2,2)) + 
               s*(4 + (27 - 4*s1)*t2 + 2*(-53 + 6*s1)*Power(t2,2) - 
                  121*Power(t2,3)) + 
               2*s1*(9 + 75*t2 + 35*Power(t2,2) + 67*Power(t2,3))) - 
            t1*Power(t2,2)*(-5 + (46 + s)*t2 + (-8 + s)*Power(t2,2) + 
               (19 + 15*s)*Power(t2,3) - (61 + 10*s)*Power(t2,4) + 
               2*Power(t2,5) + 
               s1*(4 - 16*t2 - 22*Power(t2,2) + 23*Power(t2,3) + 
                  2*Power(t2,4))) - 
            Power(t1,2)*t2*(-19 - (73 + 10*s)*t2 + 
               (236 + 19*s)*Power(t2,2) + 
               (87 + 27*s + 2*Power(s,2))*Power(t2,3) + 
               (31 - 17*s)*Power(t2,4) + 5*Power(t2,5) + 
               Power(s1,2)*(-12 + 46*t2 + 6*Power(t2,2) - 
                  22*Power(t2,3)) + 
               s1*(33 - 4*(23 + s)*t2 - 4*(22 + s)*Power(t2,2) + 
                  (3 + 8*s)*Power(t2,3) + 45*Power(t2,4))) + 
            Power(t1,3)*(-4 + (142 - 5*s)*t2 + 
               4*(67 + 16*s)*Power(t2,2) + 
               (-226 + 19*s + 4*Power(s,2))*Power(t2,3) - 
               6*(9 + 17*s)*Power(t2,4) + 46*Power(t2,5) - 
               2*Power(s1,2)*
                (-9 + 14*t2 + 12*Power(t2,2) + 11*Power(t2,3)) + 
               s1*(-35 - 2*(21 + 2*s)*t2 + 2*(65 + 2*s)*Power(t2,2) + 
                  2*(35 + 8*s)*Power(t2,3) + 121*Power(t2,4)))) - 
         Power(s2,4)*(2*Power(t1,6)*(11 + 6*t2) + 
            2*Power(t2,3)*(-2 + t2 + 3*Power(t2,2)) + 
            Power(t1,5)*(17 + 2*Power(s1,2) - 100*t2 - 21*s1*t2 - 
               80*Power(t2,2) + s*(11 - 4*s1 + 15*t2)) + 
            t1*Power(t2,2)*(14 + (-6 + 4*s)*t2 + 
               (-26 + 3*s)*Power(t2,2) - 3*(29 + 6*s)*Power(t2,3) + 
               10*Power(t2,4) + 
               s1*(-7 - 7*t2 + 29*Power(t2,2) + 6*Power(t2,3))) + 
            Power(t1,2)*t2*(-7 + (140 + s)*t2 + 
               (106 + 35*s)*Power(t2,2) - 7*(3 + s)*Power(t2,3) + 
               7*Power(t2,4) - 
               2*Power(s1,2)*(-9 + 6*t2 + 10*Power(t2,2)) + 
               s1*(-37 - 14*t2 + (25 + 12*s)*Power(t2,2) + 
                  37*Power(t2,3))) + 
            Power(t1,3)*(-15 - 2*(9 + 5*s)*t2 - 
               4*(-31 + 3*s)*Power(t2,2) + 5*(26 + 15*s)*Power(t2,3) - 
               83*Power(t2,4) + 
               2*Power(s1,2)*(3 + 5*t2 + 8*Power(t2,2)) - 
               s1*(4 + (61 + 4*s)*t2 + (67 + 20*s)*Power(t2,2) + 
                  70*Power(t2,3))) + 
            Power(t1,4)*(8 + Power(s1,2)*(8 - 16*t2) - 173*t2 - 
               22*Power(t2,2) + 140*Power(t2,3) + 
               s1*(22 + 7*t2 + 54*Power(t2,2)) + 
               s*(5 - 37*t2 - 65*Power(t2,2) + 4*s1*(1 + 3*t2)))) + 
         s2*(-8*(3 + s - s1)*Power(t1,8) + 2*(-1 + t2)*Power(t2,5) + 
            t1*Power(t2,3)*(12 - (5 + 4*s1)*t2 + 
               (2 + s - 4*s1)*Power(t2,2) + 
               (-7 + 5*s + 8*s1)*Power(t2,3) - 6*(1 + s)*Power(t2,4)) + 
            2*Power(t1,7)*(-37 - 5*Power(s,2) + 3*Power(s1,2) + 68*t2 + 
               6*Power(t2,2) + s1*(16 + 6*t2 - 4*Power(t2,2)) + 
               2*s*(s1 + 2*Power(t2,2))) - 
            Power(t1,5)*(-142 - 184*t2 + 320*Power(t2,2) - 
               289*Power(t2,3) - 112*Power(t2,4) + 
               4*Power(s,2)*t2*(8 + 3*t2 + Power(t2,2)) + 
               Power(s1,2)*(80 - 80*t2 + 2*Power(t2,2) - 
                  26*Power(t2,3)) + 
               s*(-14 + 20*(-5 + 2*s1)*t2 + (7 + 12*s1)*Power(t2,2) + 
                  (94 + 28*s1)*Power(t2,3) - 36*Power(t2,4)) + 
               s1*(10 + 20*t2 + 374*Power(t2,2) - 61*Power(t2,3) + 
                  32*Power(t2,4))) + 
            Power(t1,2)*Power(t2,2)*
             (-38 + (91 + 8*s)*t2 + 
               (-29 + 4*s + 2*Power(s,2))*Power(t2,2) + 
               (63 + 11*s)*Power(t2,3) + 
               (-1 + 15*s - 2*Power(s,2))*Power(t2,4) - 8*Power(t2,5) + 
               4*Power(s1,2)*t2*(3 + t2 - 4*Power(t2,2)) + 
               s1*(24 - 51*t2 - 2*(11 + 8*s)*Power(t2,2) + 
                  (3 + 4*s)*Power(t2,3) + (22 + 4*s)*Power(t2,4))) - 
            Power(t1,3)*t2*(86 + (226 + 58*s)*t2 + 
               (-372 - 17*s + 16*Power(s,2))*Power(t2,2) + 
               (164 + 52*s - 6*Power(s,2))*Power(t2,3) + 
               (-21 + 20*s - 4*Power(s,2))*Power(t2,4) - 
               4*(12 + s)*Power(t2,5) - 
               2*Power(s1,2)*
                (-36 + 41*t2 + 12*Power(t2,2) + 11*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s1*(-184 + (83 + 24*s)*t2 + (118 - 68*s)*Power(t2,2) + 
                  (59 + 24*s)*Power(t2,3) + (69 + 20*s)*Power(t2,4) + 
                  4*Power(t2,5))) + 
            Power(t1,4)*(16 + 18*(-7 + 2*s)*t2 + 
               (-553 - 80*s + 36*Power(s,2))*Power(t2,2) + 
               (354 + 47*s - 8*Power(s,2))*Power(t2,3) + 
               6*(-22 + 9*s)*Power(t2,4) - 2*(53 + 10*s)*Power(t2,5) - 
               2*Power(s1,2)*
                (36 + 34*t2 + 12*Power(t2,2) + 9*Power(t2,3) + 
                  12*Power(t2,4)) + 
               s1*(140 + 8*(38 + 3*s)*t2 + (10 - 48*s)*Power(t2,2) + 
                  (290 + 36*s)*Power(t2,3) + (47 + 36*s)*Power(t2,4) + 
                  18*Power(t2,5))) + 
            Power(t1,6)*(4 + 160*t2 - 283*Power(t2,2) - 58*Power(t2,3) + 
               2*Power(s,2)*(5 + 12*t2 + Power(t2,2)) - 
               2*Power(s1,2)*(3 + 2*t2 + 5*Power(t2,2)) + 
               s1*(-14 + 124*t2 - 81*Power(t2,2) + 26*Power(t2,3)) + 
               s*(-42 - 4*t2 + 59*Power(t2,2) - 28*Power(t2,3) + 
                  4*s1*(9 - 2*t2 + 2*Power(t2,2))))) + 
         Power(s2,2)*(2*Power(t1,7)*
             (29 + Power(s,2) + Power(s1,2) + s1*(5 - 4*t2) - 
               2*s*(2 + s1 - 2*t2) + 20*t2) - 
            2*Power(t2,4)*(2 - 4*t2 + Power(t2,2)) + 
            t1*Power(t2,2)*(2 - 2*(3 + 4*s1)*t2 + 
               (-22 + 5*s1)*Power(t2,2) + 
               (-6 + 8*s + 21*s1)*Power(t2,3) - 
               (26 + 17*s + 6*s1)*Power(t2,4) + 2*(8 + s)*Power(t2,5)) + 
            Power(t1,2)*t2*(-9 + (104 + s)*t2 + 
               (60 + 11*s)*Power(t2,2) + 
               (-84 - 33*s + 2*Power(s,2))*Power(t2,3) + 
               (-24 + 13*s - 4*Power(s,2))*Power(t2,4) + 
               (-32 + 9*s)*Power(t2,5) + 
               4*Power(s1,2)*t2*
                (6 - 9*t2 - 7*Power(t2,2) + 2*Power(t2,3)) + 
               s1*(4 - 100*t2 + (41 + 4*s)*Power(t2,2) + 
                  2*(53 + 4*s)*Power(t2,3) + 4*(7 + s)*Power(t2,4) - 
                  18*Power(t2,5))) + 
            Power(t1,3)*(-5 - (73 + 6*s)*t2 + 
               (390 + 58*s)*Power(t2,2) + 
               (187 + 115*s + 4*Power(s,2))*Power(t2,3) + 
               2*(-38 + 2*s + 5*Power(s,2))*Power(t2,4) + 
               (72 - 43*s)*Power(t2,5) + 4*Power(t2,6) + 
               2*Power(s1,2)*
                (-6 + 60*t2 + 5*Power(t2,2) + 6*Power(t2,3)) + 
               s1*(26 - (219 + 4*s)*t2 + 3*(-85 + 4*s)*Power(t2,2) - 
                  (71 + 32*s)*Power(t2,3) - (47 + 20*s)*Power(t2,4) + 
                  55*Power(t2,5))) + 
            Power(t1,6)*(179 + Power(s1,2)*(8 - 16*t2) + 26*t2 - 
               170*Power(t2,2) - 4*Power(t2,3) - 
               2*Power(s,2)*(5 + 2*t2) + 
               s1*(35 - 106*t2 + 16*Power(t2,2)) + 
               s*(33 + 58*t2 - 20*Power(t2,2) + 4*s1*(-1 + 5*t2))) + 
            Power(t1,5)*(57 - 333*t2 - 203*Power(t2,2) + 
               246*Power(t2,3) + 12*Power(t2,4) + 
               4*Power(s,2)*t2*(7 + t2) + 
               4*Power(s1,2)*(9 - 9*t2 + 8*Power(t2,2)) + 
               s1*(74 - 6*t2 + 138*Power(t2,2) + 17*Power(t2,3)) + 
               s*(40 + 11*t2 - 113*Power(t2,2) - 3*Power(t2,3) - 
                  4*s1*(-7 + 3*t2 + 10*Power(t2,2)))) - 
            Power(t1,4)*(45 + 349*t2 - 185*Power(t2,2) - 
               215*Power(t2,3) + 172*Power(t2,4) + 12*Power(t2,5) + 
               8*Power(s,2)*Power(t2,2)*(3 + t2) + 
               Power(s1,2)*(-6 - 80*t2 + 22*Power(t2,2) + 
                  20*Power(t2,3)) + 
               s1*(-9 + 69*t2 - 47*Power(t2,2) + 29*Power(t2,3) + 
                  62*Power(t2,4)) - 
               s*(5 - 109*t2 - 134*Power(t2,2) + 63*Power(t2,3) + 
                  47*Power(t2,4) + 
                  s1*(4 - 44*t2 + 40*Power(t2,2) + 40*Power(t2,3))))) + 
         t1*(-2*(-1 + t2)*Power(t2,4) + 
            t1*Power(t2,2)*(-8 + (-5 + 4*s1)*t2 + 
               (2 - 5*s + 4*s1)*Power(t2,2) - 
               (-9 + 5*s + 2*Power(s,2) + 8*s1)*Power(t2,3) - 
               2*(-2 - 5*s + Power(s,2))*Power(t2,4)) - 
            4*Power(t1,7)*(9 + s1*(7 - 2*t2) + 2*t2 + s*(-5 + 2*t2)) - 
            Power(t1,2)*t2*(-36 + (57 + 4*s)*t2 - 
               (23 + 12*s)*Power(t2,2) + 
               (43 + 2*s - 16*Power(s,2))*Power(t2,3) + 
               (3 + 41*s - 14*Power(s,2))*Power(t2,4) + 
               2*(-3 - 2*s + Power(s,2))*Power(t2,5) + 
               4*Power(s1,2)*t2*(3 + t2 - 4*Power(t2,2)) + 
               s1*(16 - 50*t2 + (3 - 20*s)*Power(t2,2) + 
                  (1 + 4*s)*Power(t2,3) + 2*(9 + 4*s)*Power(t2,4))) + 
            Power(t1,3)*(20 + 8*(20 + 3*s)*t2 - 
               (241 + 17*s)*Power(t2,2) + 
               (127 + 65*s - 52*Power(s,2))*Power(t2,3) + 
               (-5 + 59*s - 32*Power(s,2))*Power(t2,4) + 
               4*(-11 - 4*s + 2*Power(s,2))*Power(t2,5) - 
               2*Power(s1,2)*
                (-24 + 24*t2 - 3*Power(t2,2) + 14*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s1*(-104 + 4*(1 + 4*s)*t2 + (63 - 100*s)*Power(t2,2) + 
                  (45 + 28*s)*Power(t2,3) + (63 + 44*s)*Power(t2,4) - 
                  4*(-2 + s)*Power(t2,5))) - 
            2*Power(t1,6)*(12 - 65*t2 - 23*Power(t2,2) + 
               Power(s1,2)*(8 - 9*t2 + Power(t2,2)) + 
               Power(s,2)*(-8 - t2 + Power(t2,2)) + 
               s1*(54 - 32*t2 + 13*Power(t2,2)) - 
               2*s*(s1*(-2 - 5*t2 + Power(t2,2)) + 
                  7*(-1 - 2*t2 + Power(t2,2)))) + 
            Power(t1,5)*(-136 + 156*t2 - 163*Power(t2,2) - 
               100*Power(t2,3) + 
               2*Power(s,2)*t2*(-29 - 7*t2 + 4*Power(t2,2)) + 
               Power(s1,2)*(-8 + 22*t2 - 44*Power(t2,2) + 
                  4*Power(t2,3)) + 
               3*s1*(-4 + 86*t2 - 5*Power(t2,2) + 12*Power(t2,3)) + 
               s*(-4 + 102*t2 + 59*Power(t2,2) - 40*Power(t2,3) - 
                  4*s1*(14 - 9*t2 - 17*Power(t2,2) + 3*Power(t2,3)))) - 
            Power(t1,4)*(64 - 342*t2 + 225*Power(t2,2) - 73*Power(t2,3) - 
               100*Power(t2,4) + 
               4*Power(s,2)*Power(t2,2)*(-20 - 8*t2 + 3*Power(t2,2)) + 
               2*Power(s1,2)*(-36 + 12*t2 - 6*Power(t2,2) - 
                  18*Power(t2,3) + Power(t2,4)) + 
               2*s1*(50 - 7*t2 + 99*Power(t2,2) + 33*Power(t2,3) + 
                  13*Power(t2,4)) + 
               s*(20 - 14*t2 + 132*Power(t2,2) + 51*Power(t2,3) - 
                  32*Power(t2,4) - 
                  4*s1*(-4 + 34*t2 - 13*Power(t2,2) - 21*Power(t2,3) + 
                     3*Power(t2,4))))))*R1(t1))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*t1*Power(t1 - t2,3)*
       (-1 + s2 - t1 + t2)*(-t1 + s2*t2)*
       (Power(s2,2) - 4*t1 + 2*s2*t2 + Power(t2,2))) + 
    (8*(2*Power(t1,6)*(-1 + (12 + s)*t2 + 
            (1 + 10*s - 4*s2 - 4*s*s2)*Power(t2,2) - 3*s*Power(t2,3) + 
            s1*(-1 + t2)*(1 + (-7 + 2*s2)*t2 + 2*Power(t2,2))) + 
         Power(t1,5)*(-5 + 59*t2 - 20*s2*t2 - 4*Power(s2,2)*t2 - 
            127*Power(t2,2) - 68*s2*Power(t2,2) + 
            24*Power(s2,2)*Power(t2,2) - 7*Power(t2,3) + 
            36*s2*Power(t2,3) + 4*Power(s2,2)*Power(t2,3) + 
            8*Power(t2,4) + 4*s2*Power(t2,4) - 
            Power(s1,2)*Power(-1 + t2,2)*
             (1 + (-5 + 2*s2)*t2 + 2*Power(t2,2)) - 
            2*Power(s,2)*t2*(-1 + (-7 + 3*s2)*t2 + 
               (-1 + s2)*Power(t2,2) + Power(t2,3)) - 
            2*s1*(-1 + t2)*(-2 + 20*t2 - 31*Power(t2,2) + 
               5*Power(t2,3) + 2*Power(s2,2)*t2*(1 + t2) + 
               s2*(1 - 11*t2 + 4*Power(t2,2) + 2*Power(t2,3))) + 
            s*(1 + 7*t2 + (19 - 36*s2 + 8*Power(s2,2))*Power(t2,2) + 
               (-79 + 14*s2 + 8*Power(s2,2))*Power(t2,3) + 
               (20 + 6*s2)*Power(t2,4) + 
               s1*(-1 + t2)*(1 + 2*(-4 + s2)*t2 + 
                  (-9 + 4*s2)*Power(t2,2) + 4*Power(t2,3)))) + 
         t1*Power(t2,2)*(8*Power(s2,5)*Power(-1 + t2,2)*t2 + 
            Power(s2,4)*t2*(-11 - 8*s*Power(-1 + t2,2) + 47*t2 - 
               37*Power(t2,2) + 25*Power(t2,3)) + 
            Power(s2,3)*(-15 - 2*(18 + 5*s)*t2 + 
               (46 - 8*s)*Power(t2,2) + 8*(-5 + 2*s)*Power(t2,3) - 
               (23 + 14*s)*Power(t2,4) + 20*Power(t2,5)) + 
            Power(s2,2)*(21 + (-61 + 12*s)*t2 + 
               (14 - 29*s + 6*Power(s,2))*Power(t2,2) + 
               (52 + 141*s - 8*Power(s,2))*Power(t2,3) - 
               (83 + 103*s + 6*Power(s,2))*Power(t2,4) - 
               (19 + 5*s)*Power(t2,5) + 4*Power(t2,6)) - 
            2*(7 + (-16 + s)*t2 + (12 + 8*s + Power(s,2))*Power(t2,2) + 
               (-18 + 6*s + 5*Power(s,2))*Power(t2,3) + 
               (29 - 30*s - 4*Power(s,2))*Power(t2,4) + 
               (-14 + 17*s - 7*Power(s,2))*Power(t2,5) + 
               (-2 + s)*s*Power(t2,6)) + 
            2*s2*(4 + (37 + 4*s)*t2 + 
               (-112 + 3*s - 2*Power(s,2))*Power(t2,2) + 
               (148 - 35*s + 10*Power(s,2))*Power(t2,3) + 
               (-92 + 62*s + 15*Power(s,2))*Power(t2,4) + 
               (7 - 19*s - 3*Power(s,2))*Power(t2,5) + 
               (8 + s)*Power(t2,6)) + 
            Power(s1,2)*Power(-1 + t2,2)*
             (Power(s2,3)*(4 + 6*t2) + 2*(-3 + 4*t2 + Power(t2,2)) + 
               Power(s2,2)*(-15 - 15*t2 + 4*Power(t2,2)) + 
               s2*(17 - t2 - Power(t2,2) + Power(t2,3))) - 
            s1*(-1 + t2)*(23 - (42 + s)*t2 - (8 + s)*Power(t2,2) + 
               (26 + 5*s)*Power(t2,3) + (1 + 5*s)*Power(t2,4) + 
               Power(s2,4)*(-1 + Power(t2,2)) + 
               Power(s2,3)*(7 + 17*t2 + (-37 + 6*s)*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(s2,2)*(14 - (-9 + s)*t2 + 
                  (76 - 16*s)*Power(t2,2) + (-91 + s)*Power(t2,3)) + 
               s2*(-43 + 2*(10 + s)*t2 + 2*(4 + s)*Power(t2,2) + 
                  4*(5 + 7*s)*Power(t2,3) + 11*Power(t2,4)))) + 
         Power(t2,3)*(-8*Power(s2,5)*Power(-1 + t2,2)*t2 - 
            2*t2*Power(1 + (-1 + s)*t2,2)*(-1 + Power(t2,2)) + 
            Power(s2,4)*(12 + (5 + 8*s)*t2 - (59 + 16*s)*Power(t2,2) + 
               (47 + 8*s)*Power(t2,3) - 13*Power(t2,4) - 
               s1*Power(-1 + t2,2)*(5 + 3*t2)) + 
            Power(s2,3)*(-15 - (-53 + s)*t2 + (-6 + 17*s)*Power(t2,2) - 
               (44 + 21*s)*Power(t2,3) + (41 + 13*s)*Power(t2,4) - 
               5*Power(t2,5) + Power(s1,2)*Power(-1 + t2,2)*(3 + t2) - 
               s1*(-1 + t2)*(-7 - 2*(11 + s)*t2 + 27*Power(t2,2) + 
                  6*Power(t2,3))) + 
            Power(s2,2)*(-11 - (13 + 9*s)*t2 - 
               2*(-27 - 6*s + Power(s,2))*Power(t2,2) + 
               2*(-18 - 20*s + Power(s,2))*Power(t2,3) + 
               (-11 + 16*s)*Power(t2,4) + (17 + 5*s)*Power(t2,5) + 
               Power(s1,2)*Power(-1 + t2,2)*(-9 + 6*t2 + Power(t2,2)) - 
               s1*(-1 + t2)*(35 + (-16 + s)*t2 + 
                  (-62 + 4*s)*Power(t2,2) - (-32 + s)*Power(t2,3) + 
                  3*Power(t2,4))) + 
            s2*(-2*Power(s1,2)*Power(-1 + t2,2)*
                (-3 + 4*t2 + Power(t2,2)) + 
               s1*(-1 + t2)*(23 - (38 + s)*t2 + 
                  3*(-4 + s)*Power(t2,2) + (22 + 5*s)*Power(t2,3) + 
                  (5 + s)*Power(t2,4)) - 
               2*(-7 - (-21 + s)*t2 - 
                  (23 - 4*s + Power(s,2))*Power(t2,2) + 
                  2*(10 - 10*s + Power(s,2))*Power(t2,3) + 
                  (-19 + 20*s + 3*Power(s,2))*Power(t2,4) + 
                  (7 - 3*s)*Power(t2,5) + Power(t2,6)))) + 
         Power(t1,2)*t2*(3 - 45*t2 + s*t2 + 134*Power(t2,2) + 
            34*s*Power(t2,2) + 6*Power(s,2)*Power(t2,2) - 
            252*Power(t2,3) + 46*s*Power(t2,3) + 
            16*Power(s,2)*Power(t2,3) + 227*Power(t2,4) - 
            180*s*Power(t2,4) - 38*Power(s,2)*Power(t2,4) - 
            55*Power(t2,5) + 97*s*Power(t2,5) - 
            32*Power(s,2)*Power(t2,5) - 12*Power(t2,6) - 
            14*s*Power(t2,6) + 8*Power(s,2)*Power(t2,6) - 
            2*Power(s2,4)*t2*
             (6 - 5*t2 + 4*Power(t2,2) + 7*Power(t2,3)) + 
            Power(s2,2)*(2 - (-47 + s)*t2 + 
               (-48 + 28*s - 6*Power(s,2))*Power(t2,2) + 
               4*(25 - 38*s + 3*Power(s,2))*Power(t2,3) + 
               18*(7 + 10*s + Power(s,2))*Power(t2,4) - 
               23*(1 + s)*Power(t2,5) - 12*Power(t2,6)) - 
            s2*(5 + (29 + 11*s)*t2 + (-162 + 13*s)*Power(t2,2) + 
               (426 + 19*s + 36*Power(s,2))*Power(t2,3) + 
               (-413 + 89*s + 48*Power(s,2))*Power(t2,4) + 
               (5 - 88*s - 20*Power(s,2))*Power(t2,5) + 
               2*(19 + 6*s)*Power(t2,6)) - 
            Power(s2,3)*(-1 + t2)*t2*
             (35 - 19*t2 + 75*Power(t2,2) + 29*Power(t2,3) + 
               s*(11 + 19*Power(t2,2))) - 
            Power(s1,2)*Power(-1 + t2,2)*
             (8 + 3*t2 + 3*Power(t2,3) + Power(s2,3)*(1 + 3*t2) + 
               Power(s2,2)*(1 + 2*t2 + 3*Power(t2,2)) - 
               s2*(10 + 16*t2 - 3*Power(t2,2) + Power(t2,3))) + 
            s1*(-1 + t2)*(-8 + (-16 + 3*s)*t2 - 
               2*Power(s2,4)*(-1 + t2)*t2 - 6*(5 + 3*s)*Power(t2,2) + 
               (68 + 31*s)*Power(t2,3) + 2*(-5 + 12*s)*Power(t2,4) - 
               4*(-1 + s)*Power(t2,5) + 
               Power(s2,2)*(2 - 3*(1 + s)*t2 + 
                  (77 - 18*s)*Power(t2,2) - (109 + 3*s)*Power(t2,3) + 
                  17*Power(t2,4)) + 
               s2*(6 + 2*(18 + s)*t2 + (-19 + 6*s)*Power(t2,2) + 
                  2*(9 + 22*s)*Power(t2,3) - 5*(3 + 2*s)*Power(t2,4) + 
                  2*Power(t2,5)) + 
               Power(s2,3)*t2*
                (-3 - 16*t2 + 19*Power(t2,2) + s*(-2 + 8*t2)))) + 
         Power(t1,3)*(-1 + 27*t2 - 148*Power(t2,2) - 27*s*Power(t2,2) - 
            6*Power(s,2)*Power(t2,2) + 414*Power(t2,3) - 
            37*s*Power(t2,3) - 8*Power(s,2)*Power(t2,3) - 
            391*Power(t2,4) + 219*s*Power(t2,4) + 
            66*Power(s,2)*Power(t2,4) + 43*Power(t2,5) - 
            147*s*Power(t2,5) + 32*Power(s,2)*Power(t2,5) + 
            32*Power(t2,6) + 24*s*Power(t2,6) - 
            12*Power(s,2)*Power(t2,6) - 
            4*Power(s2,4)*Power(t2,2)*(1 - 4*t2 + Power(t2,2)) + 
            2*Power(s2,3)*t2*
             (1 + (8 + s)*t2 - (26 + 7*s)*Power(t2,2) + 
               2*(18 + 7*s)*Power(t2,3) + 5*Power(t2,4)) + 
            s2*(1 + 2*(-5 + s)*t2 + 
               2*(-1 + 8*s + 2*Power(s,2))*Power(t2,2) + 
               4*(40 + 25*s + 7*Power(s,2))*Power(t2,3) + 
               (-407 - 70*s + 24*Power(s,2))*Power(t2,4) - 
               2*(-5 + 44*s + 12*Power(s,2))*Power(t2,5) + 
               8*(5 + 3*s)*Power(t2,6)) + 
            Power(s1,2)*Power(-1 + t2,2)*
             (2 - 12*t2 + 2*Power(s2,3)*t2 + Power(t2,2) + 
               9*Power(t2,3) - 2*Power(t2,4) + 
               Power(s2,2)*(1 - 7*t2 + 4*Power(t2,2)) - 
               s2*(3 - 11*t2 + 5*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s2,2)*t2*(-17 + 8*t2 - 144*Power(t2,2) - 
               72*Power(t2,3) + 69*Power(t2,4) + 12*Power(t2,5) - 
               2*Power(s,2)*t2*(-1 + 4*t2 + 9*Power(t2,2)) + 
               s*(-2 - 9*t2 + 37*Power(t2,2) - 107*Power(t2,3) + 
                  49*Power(t2,4))) - 
            s1*(-1 + t2)*(-1 + (17 + 5*s)*t2 - 
               2*(41 + 15*s)*Power(t2,2) + (127 + 55*s)*Power(t2,3) + 
               (-55 + 42*s)*Power(t2,4) - 2*(-5 + 6*s)*Power(t2,5) + 
               2*Power(s2,3)*t2*(1 + (-3 + s)*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(1 - (8 + 3*s)*t2 + (13 - 8*s)*Power(t2,2) - 
                  (54 + 5*s)*Power(t2,3) + 32*Power(t2,4)) + 
               s2*t2*(1 + 53*t2 - 19*Power(t2,2) - 51*Power(t2,3) + 
                  8*Power(t2,4) + 
                  s*(-2 + 14*t2 + 22*Power(t2,2) - 20*Power(t2,3))))) + 
         Power(t1,4)*(-4 + s2 + 2*Power(s2,2) + 59*t2 - 12*s2*t2 - 
            10*Power(s2,2)*t2 + 4*Power(s2,3)*t2 - 267*Power(t2,2) + 
            36*s2*Power(t2,2) + 74*Power(s2,2)*Power(t2,2) - 
            12*Power(s2,3)*Power(t2,2) + 321*Power(t2,3) + 
            220*s2*Power(t2,3) - 14*Power(s2,2)*Power(t2,3) - 
            20*Power(s2,3)*Power(t2,3) - 9*Power(t2,4) - 
            33*s2*Power(t2,4) - 48*Power(s2,2)*Power(t2,4) + 
            4*Power(s2,3)*Power(t2,4) - 28*Power(t2,5) - 
            20*s2*Power(t2,5) - 4*Power(s2,2)*Power(t2,5) + 
            Power(s1,2)*Power(-1 + t2,2)*
             (1 - (-4 + s2)*t2 + (-11 + 5*s2)*Power(t2,2) + 
               4*Power(t2,3)) + 
            2*Power(s,2)*t2*(1 - t2 - 25*Power(t2,2) - 7*Power(t2,3) + 
               4*Power(t2,4) + Power(s2,2)*t2*(1 + 3*t2) + 
               s2*(-1 - 4*t2 + 3*Power(t2,2) + 6*Power(t2,3))) + 
            s1*(-1 + t2)*(3 - 34*t2 + 111*Power(t2,2) + 
               4*Power(s2,3)*Power(t2,2) - 92*Power(t2,3) + 
               12*Power(t2,4) + 
               2*Power(s2,2)*t2*(-1 - 6*t2 + 11*Power(t2,2)) + 
               s2*(-1 + 22*t2 - 37*Power(t2,2) - 26*Power(t2,3) + 
                  10*Power(t2,4))) - 
            s*(-1 - 4*t2 + 116*Power(t2,3) + 8*Power(s2,3)*Power(t2,3) - 
               139*Power(t2,4) + 28*Power(t2,5) + 
               2*Power(s2,2)*t2*
                (1 - 7*t2 - 3*Power(t2,2) + 17*Power(t2,3)) + 
               s2*(1 + t2 + 51*Power(t2,2) - 111*Power(t2,3) - 
                  26*Power(t2,4) + 20*Power(t2,5)) + 
               s1*(-1 + t2)*(-1 + 14*t2 - 37*Power(t2,2) - 
                  32*Power(t2,3) + 12*Power(t2,4) + 
                  2*Power(s2,2)*t2*(1 + t2) + 
                  s2*(1 - 7*t2 + Power(t2,2) + 15*Power(t2,3))))))*R1(t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*Power(t1 - t2,3)*Power(-1 + t2,2)*
       t2*(-1 + s2 - t1 + t2)*(-t1 + s2*t2)) + 
    (8*(-6*Power(s2,6)*t2 - 2*t2*(1 + t2)*Power(1 + (-1 + s)*t2,2) - 
         4*Power(t1,4)*(13 + s1*(7 - 2*t2) + 2*t2 + s*(-5 + 2*t2)) + 
         Power(s2,5)*(2*Power(s1,2) - 7*s1*t2 + (-8 + 6*s - 19*t2)*t2 + 
            6*t1*(1 + 3*t2)) + 
         t1*(2*Power(s1,2)*t2*(-4 + 3*t2) + 
            s1*(20 - 12*t2 + 21*Power(t2,2) - 11*Power(t2,3)) - 
            2*Power(s,2)*t2*(-2 - 9*t2 - 5*Power(t2,2) + Power(t2,3)) + 
            4*(-8 + 9*t2 - 7*Power(t2,2) + Power(t2,3) + Power(t2,4)) + 
            s*(4 + 4*(-1 + 3*s1)*t2 + (26 - 4*s1)*Power(t2,2) - 
               (26 + 7*s1)*Power(t2,3) + 4*Power(t2,4))) + 
         Power(s2,4)*(-6*Power(t1,2)*(3 + 2*t2) + 
            Power(s1,2)*(-6 - 2*t1 + 7*t2) + 
            t1*(8 + 34*t2 + 40*Power(t2,2) - 3*s*(2 + 5*t2)) + 
            s1*(-4 + 10*t2 - 23*Power(t2,2) + t1*(7 + 4*s + 19*t2)) + 
            t2*(59 - 25*t2 - 20*Power(t2,2) + s*(16 + 21*t2))) - 
         2*Power(t1,3)*(Power(s1,2)*(4 - 7*t2 + Power(t2,2)) + 
            Power(s,2)*(-14 + t2 + Power(t2,2)) - 
            15*(-4 + 2*t2 + Power(t2,2)) + 
            s1*(26 - 3*t2 + 3*Power(t2,2)) + 
            s*(48 + 3*t2 - 4*Power(t2,2) - 
               2*s1*(-7 - 3*t2 + Power(t2,2)))) - 
         Power(s2,3)*(-2 - 12*Power(t1,3) + 28*t2 + 52*s*t2 + 
            2*Power(s,2)*t2 - 114*Power(t2,2) - 42*s*Power(t2,2) + 
            2*Power(s,2)*Power(t2,2) + 32*Power(t2,3) - 
            24*s*Power(t2,3) + 7*Power(t2,4) + 
            Power(s1,2)*(-2 + t1 + 2*Power(t1,2) + 18*t2 + t1*t2 - 
               8*Power(t2,2)) + 
            Power(t1,2)*(15 - 2*Power(s,2) + 74*t2 + 16*Power(t2,2) - 
               s*(15 + 4*t2)) + 
            t1*(59 - 7*t2 - 2*Power(s,2)*t2 - 80*Power(t2,2) - 
               26*Power(t2,3) + s*(20 + 21*t2 + 28*Power(t2,2))) + 
            s1*(-12 + (3 - 4*s)*t2 - (35 + 3*s)*Power(t2,2) + 
               25*Power(t2,3) + Power(t1,2)*(19 + 6*t2) - 
               t1*(-6 + s*(-16 + t2) + 8*t2 + 38*Power(t2,2)))) + 
         Power(t1,2)*(12 + 42*t2 + 12*Power(t2,2) - 26*Power(t2,3) + 
            Power(s1,2)*(12 - 6*t2 - 5*Power(t2,2)) + 
            Power(s,2)*(-28 - 34*t2 - 8*Power(t2,2) + 4*Power(t2,3)) + 
            s1*(-84 + 34*t2 + 17*Power(t2,2) + 4*Power(t2,3)) + 
            s*(-104 + 74*t2 + 18*Power(t2,2) - 6*Power(t2,3) + 
               s1*(-32 + 28*t2 + 23*Power(t2,2) - 4*Power(t2,3)))) + 
         Power(s2,2)*(-6 - 30*t2 + 24*s*t2 + 4*Power(t2,2) - 
            90*s*Power(t2,2) - 6*Power(s,2)*Power(t2,2) + 
            59*Power(t2,3) + 36*s*Power(t2,3) - 
            4*Power(s,2)*Power(t2,3) - 21*Power(t2,4) + 
            9*s*Power(t2,4) + 
            2*Power(t1,2)*(9 + 2*Power(s,2)*(-3 + t2) - 72*t2 + 
               s*(13 - 2*t2)*t2 - 43*Power(t2,2) - 2*Power(t2,3)) + 
            2*Power(t1,3)*(17 + Power(s,2) + 20*t2 + s*(-2 + 4*t2)) + 
            Power(s1,2)*(2*Power(t1,3) + Power(t1,2)*(7 - 8*t2) + 
               t2*(10 - 18*t2 + 3*Power(t2,2)) + t1*(22 + 4*Power(t2,2))\
) + t1*(2*Power(s,2)*Power(-1 + t2,2) - 
               s*(-68 + 112*t2 + 46*Power(t2,2) + 11*Power(t2,3)) + 
               2*(13 - 97*t2 - 8*Power(t2,2) + 37*Power(t2,3) + 
                  2*Power(t2,4))) - 
            s1*(4 + 8*(-1 + s)*t2 - 8*(-3 + s)*Power(t2,2) - 
               2*(20 + 3*s)*Power(t2,3) + 9*Power(t2,4) + 
               Power(t1,3)*(-6 + 4*s + 8*t2) + 
               Power(t1,2)*(-19 + s + 60*t2 - 4*s*t2) + 
               t1*(9 + 113*t2 + 15*Power(t2,2) - 19*Power(t2,3) + 
                  s*(-12 + 23*t2 + 10*Power(t2,2))))) + 
         s2*(2 - 8*(3 + s - s1)*Power(t1,4) - 4*(-9 + 4*s1)*t2 - 
            2*(23 + Power(s,2) - 4*s1 - 4*Power(s1,2) + 2*s*(-5 + 2*s1))*
             Power(t2,2) - (-26 + 6*Power(s,2) + s*(34 - 4*s1) + 
               25*s1 + 6*Power(s1,2))*Power(t2,3) + 
            (4 - 2*Power(s,2) + 15*s1 + s*(10 + 3*s1))*Power(t2,4) - 
            6*Power(t2,5) + 2*Power(t1,3)*
             (42 - 7*Power(s,2) + Power(s1,2) + 56*t2 + 6*Power(t2,2) + 
               s1*(9 + 14*t2 - 4*Power(t2,2)) + 
               s*(3 + 6*s1 - 8*t2 + 4*Power(t2,2))) + 
            Power(t1,2)*(78 + 170*t2 - 127*Power(t2,2) - 
               34*Power(t2,3) - 2*Power(s1,2)*(9 - t2 + 3*Power(t2,2)) + 
               Power(s,2)*(22 - 8*t2 + 6*Power(t2,2)) + 
               s1*(66 + 104*t2 - 25*Power(t2,2) + 6*Power(t2,3)) + 
               s*(70 + 102*t2 + 17*Power(t2,2) - 8*Power(t2,3) + 
                  2*s1*(16 + 9*t2))) + 
            t1*(36 - 10*t2 - 107*Power(t2,2) + 9*Power(t2,3) + 
               26*Power(t2,4) - 
               2*Power(s,2)*t2*(-12 - 7*t2 + Power(t2,2)) + 
               Power(s1,2)*(-12 + 8*t2 + 5*Power(t2,2) + 3*Power(t2,3)) - 
               2*s1*(8 - 64*t2 + 35*Power(t2,2) + 12*Power(t2,3)) + 
               s*(-40 + 182*t2 - 90*Power(t2,2) - 15*Power(t2,3) + 
                  2*Power(t2,4) + 
                  s1*(4 + 8*t2 - 26*Power(t2,2) - 7*Power(t2,3))))))*
       R2(1 - s2 + t1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s2 - t1 + t2)*(-t1 + s2*t2)*
       (Power(s2,2) - 4*t1 + 2*s2*t2 + Power(t2,2))) + 
    (8*(16*Power(t1,6) + 4*Power(s2,8)*Power(t2,2) + 
         Power(t2,3)*Power(1 + t2,2)*Power(1 + (-1 + s)*t2,2) + 
         2*Power(t1,5)*(-10 + 2*Power(s,2)*(-3 + t2) + 
            2*Power(s1,2)*(-3 + t2) - 39*t2 - 6*Power(t2,2) + 
            s*(44 - 4*s1*(-3 + t2) + t2 - 4*Power(t2,2)) + 
            s1*(-16 - 9*t2 + 4*Power(t2,2))) + 
         t1*t2*(Power(s,2)*Power(t2,2)*
             (-6 - 16*t2 - 17*Power(t2,2) - 3*Power(t2,3) + Power(t2,4)) \
+ s*t2*(-12 - 2*(7 + s1)*t2 + (3 - 2*s1)*Power(t2,2) + 
               (17 + 2*s1)*Power(t2,3) + 2*(6 + s1)*Power(t2,4) - 
               2*Power(t2,5)) + 
            (-1 + t2)*(6 + 6*t2 - 2*(-1 + s1)*Power(t2,2) + 
               (3 - 4*s1)*Power(t2,3) - 2*(3 + s1)*Power(t2,4) + 
               Power(t2,5))) + 
         Power(t1,2)*(-28 + 48*t2 + 8*Power(t2,2) - 99*Power(t2,3) + 
            21*Power(t2,4) + 10*Power(t2,5) + 
            Power(s1,2)*t2*(-8 - 4*t2 + 5*Power(t2,2) + 2*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s,2)*t2*(4 + 34*t2 + 79*Power(t2,2) + 
               33*Power(t2,3) - 3*Power(t2,4) - 2*Power(t2,5)) + 
            s1*(20 - 10*t2 + 4*Power(t2,2) + 12*Power(t2,3) - 
               5*Power(t2,4) + 9*Power(t2,5) - 2*Power(t2,6)) + 
            s*(4 + 2*(7 + 10*s1)*t2 + 6*(5 + 3*s1)*Power(t2,2) - 
               4*(5 + 3*s1)*Power(t2,3) - (83 + 25*s1)*Power(t2,4) - 
               (9 + 7*s1)*Power(t2,5) + 2*(2 + s1)*Power(t2,6))) - 
         Power(s2,7)*(Power(s1,2)*(1 + t2) - s1*t2*(2 + s - t1 + 4*t2) + 
            t2*((-1 + 5*s - 17*t2)*t2 + t1*(8 + 11*t2))) - 
         Power(t1,4)*(100 + 48*t2 - 108*Power(t2,2) - 26*Power(t2,3) - 
            Power(t2,4) + 2*Power(s1,2)*
             (8 - 16*t2 - 8*Power(t2,2) + 3*Power(t2,3)) + 
            Power(s,2)*(-56 - 60*t2 + 4*Power(t2,2) + 6*Power(t2,3)) + 
            s1*(28 + 2*t2 - 22*Power(t2,2) + Power(t2,4)) + 
            s*(28 + 218*t2 + 52*Power(t2,2) - 16*Power(t2,3) - 
               Power(t2,4) + 4*s1*
                (2 + 27*t2 + 3*Power(t2,2) - 3*Power(t2,3)))) + 
         Power(t1,3)*(-60 + 180*t2 + 80*Power(t2,2) - 56*Power(t2,3) - 
            27*Power(t2,4) + s1*
             (-72 - 26*t2 + 58*Power(t2,2) - 28*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(s1,2)*(36 - 8*t2 - 14*Power(t2,2) - 12*Power(t2,3) - 
               2*Power(t2,4) + Power(t2,5)) + 
            Power(s,2)*(-4 - 96*t2 - 104*Power(t2,2) - 20*Power(t2,3) + 
               11*Power(t2,4) + Power(t2,5)) - 
            s*(-16 + 62*t2 - 142*Power(t2,2) - 120*Power(t2,3) + 
               9*Power(t2,4) + 3*Power(t2,5) + 
               s1*(64 + 8*t2 - 86*Power(t2,2) - 56*Power(t2,3) + 
                  9*Power(t2,4) + 2*Power(t2,5)))) + 
         Power(s2,6)*(Power(s1,2)*
             (3 + t1 - 2*t2 + t1*t2 - 4*Power(t2,2)) + 
            Power(t1,2)*(4 - (-22 + s)*t2 + 7*Power(t2,2)) + 
            t2*(-2 + s - 34*t2 - 10*s*t2 + Power(s,2)*t2 + 
               5*Power(t2,2) - 21*s*Power(t2,2) + 28*Power(t2,3)) + 
            t1*t2*(-1 + Power(s,2) - 48*t2 - 37*Power(t2,2) + 
               2*s*(5 + 6*t2)) + 
            s1*(2 - (3 + s)*t2 + (7 + 3*s)*Power(t2,2) + 
               16*Power(t2,3) + Power(t1,2)*(1 + 2*t2) - 
               t1*(2 + 6*t2 + 14*Power(t2,2) + s*(3 + 2*t2)))) + 
         Power(s2,5)*(-1 + 4*t2 - 7*s*t2 - 24*Power(t2,2) + 
            37*s*Power(t2,2) + 4*Power(s,2)*Power(t2,2) - 
            97*Power(t2,3) - 38*s*Power(t2,3) + 
            4*Power(s,2)*Power(t2,3) + 15*Power(t2,4) - 
            34*s*Power(t2,4) + 22*Power(t2,5) + 
            Power(t1,3)*(-11 + s - 14*t2 + s*t2) + 
            Power(s1,2)*(-1 + 7*t2 + 5*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,2)*(1 + t2) + t1*(4 + 9*t2)) + 
            Power(t1,2)*(-2*Power(s,2) - 
               s*(5 + 23*t2 + 5*Power(t2,2)) + 
               t2*(43 + 109*t2 + 18*Power(t2,2))) + 
            t1*(2 + 66*t2 - 2*Power(s,2)*t2 + 50*Power(t2,2) - 
               111*Power(t2,3) - 46*Power(t2,4) + 
               s*(1 + 27*t2 + 62*Power(t2,2) + 34*Power(t2,3))) + 
            s1*(-(Power(t1,3)*(2 + t2)) + 
               Power(t1,2)*(2 - (-24 + s)*t2 + 8*Power(t2,2)) + 
               2*(-3 + (7 + s)*t2 - (6 + 5*s)*Power(t2,2) + 
                  (2 + s)*Power(t2,3) + 12*Power(t2,4)) + 
               t1*(3 - 28*t2 - 31*Power(t2,2) - 34*Power(t2,3) + 
                  s*(9 - 14*t2 + Power(t2,2))))) - 
         Power(s2,4)*(-3 + (-7 + s)*Power(t1,4) + 12*t2 - 6*s*t2 - 
            63*Power(t2,2) + 8*s*Power(t2,2) + 
            3*Power(s,2)*Power(t2,2) + 73*Power(t2,3) - 
            100*s*Power(t2,3) - 14*Power(s,2)*Power(t2,3) + 
            91*Power(t2,4) + 60*s*Power(t2,4) - 
            6*Power(s,2)*Power(t2,4) - 25*Power(t2,5) + 
            26*s*Power(t2,5) - 8*Power(t2,6) + 
            Power(t1,3)*(Power(s,2)*(2 + t2) + 
               s*(-11 - 7*t2 + 2*Power(t2,2)) + 
               2*(6 + 53*t2 + 28*Power(t2,2))) + 
            Power(t1,2)*(32 + 2*Power(s,2)*(-3 + t2) + 111*t2 - 
               138*Power(t2,2) - 178*Power(t2,3) - 16*Power(t2,4) + 
               s*(15 + 59*t2 + 88*Power(t2,2))) + 
            Power(s1,2)*(Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(11 - 2*t2 - 4*Power(t2,2)) + 
               t2*(3 - 3*t2 - 17*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(16 + 14*t2 - 15*Power(t2,2) + 6*Power(t2,3))) + 
            t1*(4 - 61*t2 - 276*Power(t2,2) - 65*Power(t2,3) + 
               135*Power(t2,4) + 26*Power(t2,5) + 
               Power(s,2)*t2*(13 + 10*t2 + 5*Power(t2,2)) + 
               s*(1 + 79*t2 - 88*Power(t2,2) - 130*Power(t2,3) - 
                  28*Power(t2,4))) - 
            s1*(Power(t1,4) - 
               2*(-1 + 8*t2 - 4*(1 + s)*Power(t2,2) + 
                  4*(2 + 3*s)*Power(t2,3) + (5 + s)*Power(t2,4) - 
                  8*Power(t2,5)) + 
               Power(t1,2)*(17 + 25*t2 + 80*Power(t2,2) + 
                  6*Power(t2,3) + s*(23 - 5*t2 - 4*Power(t2,2))) + 
               Power(t1,3)*(s*(3 + 2*t2) + 
                  2*(-5 - 9*t2 + Power(t2,2))) + 
               2*t1*(-9 + 24*t2 + 4*Power(t2,2) - 23*Power(t2,3) - 
                  14*Power(t2,4) + 
                  s*(-5 + 24*t2 - 7*Power(t2,2) + 8*Power(t2,3))))) + 
         Power(s2,2)*(-20*Power(t1,5) + 
            t2*(-3 + (-37 - 2*s + 14*s1)*t2 + 
               (34 + Power(s,2) + 2*s1 - 7*Power(s1,2) + 
                  s*(-22 + 8*s1))*Power(t2,2) + 
               (19 + Power(s,2) + 14*s1 - 2*Power(s1,2) + 
                  s*(-2 + 4*s1))*Power(t2,3) + 
               (-68 + 13*Power(s,2) + s*(33 - 7*s1) + 3*s1 + 
                  5*Power(s1,2))*Power(t2,4) + 
               (9 + Power(s,2) - 5*s1 - s*(26 + s1))*Power(t2,5) - 
               (-6 + s)*Power(t2,6)) - 
            t1*(18 - 2*(12 + 19*s)*t2 + 
               (179 - 54*s - 14*Power(s,2))*Power(t2,2) + 
               (-352 + 256*s + 70*Power(s,2))*Power(t2,3) + 
               (-95 - 164*s + 19*Power(s,2))*Power(t2,4) + 
               (73 - 62*s - 3*Power(s,2))*Power(t2,5) + 
               (25 + 8*s)*Power(t2,6) + Power(t2,7) + 
               Power(s1,2)*t2*
                (-18 - 30*t2 + 46*Power(t2,2) + 4*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s1*(12 + 2*(9 + 5*s)*t2 + 16*(5 + 3*s)*Power(t2,2) + 
                  (32 - 62*s)*Power(t2,3) - 19*(4 + s)*Power(t2,4) + 
                  (8 - 10*s)*Power(t2,5) - 2*Power(t2,6))) - 
            2*Power(t1,4)*(1 + Power(s,2)*(-5 + t2) + 
               Power(s1,2)*(-3 + t2) - 82*t2 - 48*Power(t2,2) + 
               s1*(-1 - 8*t2 + 8*Power(t2,2)) - 
               2*s*(s1*(-4 + t2) + 4*(-2 + t2 + Power(t2,2)))) + 
            Power(t1,3)*(94 + 232*t2 - 321*Power(t2,2) - 
               264*Power(t2,3) - 24*Power(t2,4) + 
               Power(s1,2)*(34 - 10*t2 - 11*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s,2)*(-32 - 2*t2 + 9*Power(t2,2) + 
                  4*Power(t2,3)) + 
               s1*(26 + 8*t2 - 66*Power(t2,2) + 6*Power(t2,4)) - 
               2*s*(-7 - 64*t2 - 59*Power(t2,2) + 17*Power(t2,3) + 
                  3*Power(t2,4) + 
                  s1*(25 - 18*t2 - Power(t2,2) + 4*Power(t2,3)))) + 
            Power(t1,2)*(82 - 97*t2 - 383*Power(t2,2) - 55*Power(t2,3) + 
               252*Power(t2,4) + 42*Power(t2,5) + Power(t2,6) + 
               Power(s1,2)*(6 + 59*t2 - 7*Power(t2,2) + 
                  27*Power(t2,3)) + 
               Power(s,2)*(-4 + 57*t2 + 25*Power(t2,2) + 
                  15*Power(t2,3) - 10*Power(t2,4)) + 
               s1*(28 - 64*t2 - 148*Power(t2,2) + 24*Power(t2,3) + 
                  9*Power(t2,4) - 4*Power(t2,5)) + 
               s*(22 + 268*t2 - 142*Power(t2,2) - 314*Power(t2,3) - 
                  8*Power(t2,4) + 13*Power(t2,5) + 
                  2*s1*(31 - 62*t2 + Power(t2,2) - 38*Power(t2,3) + 
                     5*Power(t2,4))))) + 
         Power(s2,3)*(-1 + (9 - 2*s + 6*s1)*t2 - 
            (-2 + Power(s,2) + s*(18 - 8*s1) - 6*s1 + 9*Power(s1,2))*
             Power(t2,2) + (67 - 5*Power(s,2) + 8*s1 - 3*Power(s1,2) + 
               2*s*(-1 + 5*s1))*Power(t2,3) + 
            (-104 + 96*s + 19*Power(s,2) - 6*s1 - 22*s*s1 + 
               16*Power(s1,2))*Power(t2,4) + 
            (-23 + 4*Power(s,2) - 14*s1 - Power(s1,2) - s*(52 + 3*s1))*
             Power(t2,5) + (-9*s + 4*(5 + s1))*Power(t2,6) + 
            Power(t2,7) + 2*Power(t1,4)*
             (17 - s1*(-5 + t2) + s*(-1 + t2) + 29*t2) - 
            Power(t1,3)*(-56 + Power(s,2)*(-14 + t2) + 53*t2 + 
               258*Power(t2,2) + 64*Power(t2,3) + 
               Power(s1,2)*(6 + 7*t2) + 
               s1*(10 + 46*t2 + 20*Power(t2,2) - 8*Power(t2,3)) + 
               2*s*(-8 - 2*(21 + 2*s1)*t2 + Power(t2,2) + 4*Power(t2,3))\
) + Power(t1,2)*(-35 - 273*t2 - 249*Power(t2,2) + 312*Power(t2,3) + 
               132*Power(t2,4) + 6*Power(t2,5) + 
               Power(s,2)*(1 + 9*t2 + 13*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s1,2)*(11 - 25*t2 + 19*Power(t2,2) + 
                  4*Power(t2,3)) - 
               2*s1*(12 + 15*t2 + 5*Power(t2,2) - 33*Power(t2,3) + 
                  2*Power(t2,4)) - 
               2*s*(-15 + 30*t2 + 99*Power(t2,2) + 41*Power(t2,3) - 
                  7*Power(t2,4) + s1*(34 - 24*t2 + 25*Power(t2,2)))) + 
            t1*(14 - 151*t2 + 158*Power(t2,2) + 303*Power(t2,3) - 
               40*Power(t2,4) - 89*Power(t2,5) - 7*Power(t2,6) - 
               Power(s,2)*t2*
                (-12 + 46*t2 + 19*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s1,2)*(6 - 52*Power(t2,2) + 5*Power(t2,3) - 
                  8*Power(t2,4)) + 
               s1*(34 - 40*t2 + 46*Power(t2,2) + 88*Power(t2,3) - 
                  26*Power(t2,4) - 5*Power(t2,5)) + 
               2*s*(1 + 8*t2 - 146*Power(t2,2) + 78*Power(t2,3) + 
                  64*Power(t2,4) + 
                  s1*(1 - 22*t2 + 42*Power(t2,2) + 4*Power(t2,3) + 
                     11*Power(t2,4))))) + 
         s2*(Power(t2,2)*(3 + (-3 + 6*s - 2*s1)*t2 + 
               (3 + 3*Power(s,2) - 2*s1)*Power(t2,2) + 
               (7 - 3*s + 5*Power(s,2) + 2*s1)*Power(t2,3) + 
               (-s + 5*Power(s,2) + 2*(-7 + s1))*Power(t2,4) + 
               (4 - 6*s)*Power(t2,5)) - 
            t1*(-6 + (-74 - 4*s + 28*s1)*t2 + 
               2*(45 + Power(s,2) - 7*s1 - 8*Power(s1,2) + 
                  s*(-19 + 11*s1))*Power(t2,2) + 
               (45 + 14*Power(s,2) + 32*s1 - 6*Power(s1,2) + 
                  8*s*(1 + s1))*Power(t2,3) + 
               (54*Power(s,2) + s*(43 - 19*s1) + 
                  3*(-58 + 9*s1 + 4*Power(s1,2)))*Power(t2,4) + 
               (21 + 11*Power(s,2) - 20*s1 + 2*Power(s1,2) - 
                  s*(81 + 10*s1))*Power(t2,5) - 
               (4*Power(s,2) - 3*(6 + s1) + s*(10 + s1))*Power(t2,6) + 
               2*s*Power(t2,7)) + 
            2*Power(t1,5)*(-19 + 2*Power(s,2) + 2*Power(s1,2) - 32*t2 + 
               s1*(-1 + 4*t2) - s*(7 + 4*s1 + 4*t2)) - 
            2*Power(t1,4)*(32 - 71*t2 - 118*Power(t2,2) - 
               15*Power(t2,3) + Power(s1,2)*(2 - 5*t2 + 4*Power(t2,2)) + 
               Power(s,2)*(20 + 3*t2 + 4*Power(t2,2)) + 
               s1*(-9 - 28*t2 - 11*Power(t2,2) + 7*Power(t2,3)) + 
               s*(7 + 74*t2 - 9*Power(t2,2) - 7*Power(t2,3) - 
                  2*s1*(7 - t2 + 4*Power(t2,2)))) + 
            Power(t1,3)*(8 + 274*t2 + 120*Power(t2,2) - 
               279*Power(t2,3) - 61*Power(t2,4) - 2*Power(t2,5) + 
               Power(s1,2)*(-32 + 4*t2 - 44*Power(t2,2) - 
                  7*Power(t2,3) + 4*Power(t2,4)) + 
               Power(s,2)*(8 - 8*t2 - 32*Power(t2,2) + 19*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s1*(-2 + 100*t2 + 6*Power(t2,2) - 26*Power(t2,3) + 
                  4*Power(t2,4) + Power(t2,5)) - 
               s*(62 - 44*t2 - 408*Power(t2,2) - 60*Power(t2,3) + 
                  29*Power(t2,4) + Power(t2,5) + 
                  4*s1*(-26 + 3*t2 - 29*Power(t2,2) + 3*Power(t2,3) + 
                     2*Power(t2,4)))) - 
            Power(t1,2)*(24 - 190*t2 + 429*Power(t2,2) + 
               161*Power(t2,3) - 109*Power(t2,4) - 51*Power(t2,5) - 
               Power(t2,6) + Power(s,2)*t2*
                (22 - 95*t2 - 63*Power(t2,2) + Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s1,2)*(12 + 66*t2 - 45*Power(t2,2) - 
                  25*Power(t2,3) - 10*Power(t2,4) + Power(t2,5)) + 
               2*s1*(3 - 80*t2 - 50*Power(t2,2) + 67*Power(t2,3) - 
                  7*Power(t2,4) + Power(t2,5)) + 
               s*(46 + 108*t2 - 234*Power(t2,2) + 208*Power(t2,3) + 
                  159*Power(t2,4) - 13*Power(t2,5) - 3*Power(t2,6) + 
                  s1*(4 - 72*t2 + 52*Power(t2,2) + 72*Power(t2,3) + 
                     38*Power(t2,4) - 9*Power(t2,5))))))*
       T2(t1,1 - s2 + t1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s2 - t1 + t2)*
       Power(-t1 + s2*t2,2)*(Power(s2,2) - 4*t1 + 2*s2*t2 + Power(t2,2))) - 
    (8*(-(Power(t1,7)*(2 + s2 + t2 + s*(-2 + s2 + t2) - 
              s1*(-2 + s2 + t2))) + 
         Power(t2,2)*(-4*Power(s2,6)*t2 + 
            t2*Power(1 + t2,2)*Power(1 + (-1 + s)*t2,2) - 
            2*Power(s2,5)*(-2 + s1 - (13 + 2*s)*t2 + 2*s1*t2 + 
               4*Power(t2,2)) + 
            s2*t2*(-3 + (9 - 6*s)*t2 + 
               (7 + 3*s - Power(s,2))*Power(t2,2) + 
               (-17 + 5*s + 2*Power(s,2))*Power(t2,3) + 
               (4 - 6*s)*Power(t2,4) + 2*s1*(-1 + t2)*Power(1 + t2,2)) + 
            Power(s2,4)*(-6 - 2*(4 + s)*t2 + 2*(22 + 5*s)*Power(t2,2) + 
               (-3 + s)*Power(t2,3) + Power(t2,4) + 
               Power(s1,2)*(2 + 3*t2 + Power(t2,2)) - 
               s1*(2 + 2*(9 + s)*t2 + (14 + s)*Power(t2,2) + 
                  2*Power(t2,3))) + 
            Power(s2,3)*(-2 - 2*(15 + 2*s)*t2 + 
               (-16 - s + 2*Power(s,2))*Power(t2,2) + 
               (18 + 4*s - Power(s,2))*Power(t2,3) + 4*Power(t2,4) + 
               Power(t2,5) + 
               Power(s1,2)*(-6 - t2 + 5*Power(t2,2) + Power(t2,3)) - 
               s1*(-14 - 24*t2 + (13 + 5*s)*Power(t2,2) + 
                  7*Power(t2,3) + 2*Power(t2,4))) + 
            Power(s2,2)*(4 + (17 + 2*s)*t2 - 
               (24 + 3*s + 2*Power(s,2))*Power(t2,2) + 
               2*(-17 + s + 2*Power(s,2))*Power(t2,3) - 
               (5 + 8*s + Power(s,2))*Power(t2,4) - 
               (-2 + s)*Power(t2,5) + 
               Power(s1,2)*(4 - 3*t2 - 6*Power(t2,2) + Power(t2,3)) + 
               s1*(-10 + 2*(1 + s)*t2 + 4*(6 + s)*Power(t2,2) + 
                  (7 - 3*s)*Power(t2,3) + (5 + s)*Power(t2,4)))) + 
         Power(t1,2)*(4 + 19*t2 + 2*s*t2 + 4*Power(t2,2) + 
            2*s*Power(t2,2) - 2*Power(s,2)*Power(t2,2) + 
            4*Power(s2,6)*Power(t2,2) - 32*Power(t2,3) + 
            33*s*Power(t2,3) + 22*Power(s,2)*Power(t2,3) - 
            68*Power(t2,4) - 32*s*Power(t2,4) + 
            52*Power(s,2)*Power(t2,4) + 36*Power(t2,5) - 
            63*s*Power(t2,5) + 15*Power(s,2)*Power(t2,5) - 
            3*Power(t2,6) + 10*s*Power(t2,6) - 
            5*Power(s,2)*Power(t2,6) + 
            4*Power(s2,5)*t2*(-2 - (5 + s)*t2 + 3*Power(t2,2)) + 
            Power(s2,3)*(4 + 46*t2 + 
               (124 + 45*s + 2*Power(s,2))*Power(t2,2) + 
               (-65 + 6*s + 4*Power(s,2))*Power(t2,3) + 
               5*s*Power(t2,4) + 10*Power(t2,5)) + 
            Power(s2,2)*(-6 - (41 + 10*s)*t2 + 
               6*(9 + 5*s + Power(s,2))*Power(t2,2) + 
               4*(3 + 17*s + 2*Power(s,2))*Power(t2,3) - 
               (3 + 49*s)*Power(t2,4) + (32 + 13*s)*Power(t2,5) + 
               4*Power(t2,6)) + 
            s2*(-2 - 39*t2 - (91 + 65*s + 6*Power(s,2))*Power(t2,2) - 
               2*(25 - 9*s + 2*Power(s,2))*Power(t2,3) - 
               2*(68 + 17*s + Power(s,2))*Power(t2,4) + 
               (66 - 73*s - 5*Power(s,2))*Power(t2,5) + 
               (1 + 9*s)*Power(t2,6)) + 
            Power(s2,4)*t2*(20 - 36*t2 - 47*Power(t2,2) + 
               8*Power(t2,3) + s*(8 + 4*t2 - 13*Power(t2,2))) + 
            Power(s1,2)*(4 - 4*t2 - 8*Power(t2,2) + Power(t2,3) + 
               2*Power(t2,4) + Power(t2,5) + 
               3*Power(s2,4)*t2*(1 + t2) + 
               Power(s2,3)*t2*(-21 - 4*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*(2 + 53*t2 + 10*Power(t2,2) + 
                  2*Power(t2,4)) - 
               s2*(6 + 34*t2 - 8*Power(t2,2) - 13*Power(t2,3) - 
                  9*Power(t2,4) + Power(t2,5))) - 
            s1*(10 - 2*(-2 + s)*t2 - 2*(13 + 6*s)*Power(t2,2) - 
               2*(2 + 5*s)*Power(t2,3) + (1 + 17*s)*Power(t2,4) + 
               (-15 + 13*s)*Power(t2,5) - 2*(-1 + s)*Power(t2,6) + 
               Power(s2,4)*t2*(-8 + (-12 + s)*t2 + Power(t2,2)) + 
               Power(s2,3)*(2 + (30 + 4*s)*t2 + 
                  (11 + 8*s)*Power(t2,2) + 2*(-13 + 5*s)*Power(t2,3) + 
                  17*Power(t2,4)) + 
               Power(s2,2)*(2 + 2*(26 + s)*t2 - (-95 + s)*Power(t2,2) + 
                  (31 + 6*s)*Power(t2,3) + (-57 + 5*s)*Power(t2,4) + 
                  8*Power(t2,5)) - 
               s2*(14 + 4*(21 + s)*t2 - 4*(-5 + 4*s)*Power(t2,2) + 
                  (30 - 16*s)*Power(t2,3) + (7 - 3*s)*Power(t2,4) + 
                  13*Power(t2,5)))) - 
         t1*t2*(4*Power(s2,6)*(-1 + t2)*t2 + 
            t2*(3 + 2*(4 - s1 + s*(4 + s1))*t2 + 
               (-5 + 7*Power(s,2) - 2*s1 + s*(15 + 2*s1))*Power(t2,2) + 
               (-15 + 17*Power(s,2) + 2*s1 - s*(11 + 2*s1))*
                Power(t2,3) + 
               2*(5 + 3*Power(s,2) + s1 - s*(9 + s1))*Power(t2,4) - 
               Power(-1 + s,2)*Power(t2,5)) + 
            2*Power(s2,5)*t2*
             (2 - 2*s*(-1 + t2) - 16*t2 + 3*Power(t2,2) + s1*(2 + t2)) + 
            Power(s2,4)*(8 + (62 + 6*s)*t2 + Power(s1,2)*(-3 + t2)*t2 + 
               (38 + 22*s)*Power(t2,2) - 5*(7 + s)*Power(t2,3) + 
               4*Power(t2,4) - 
               s1*(4 + 2*(9 + s)*t2 + (2 + s)*Power(t2,2) + Power(t2,3))\
) + Power(s2,3)*(-12 - 2*(17 + 4*s)*t2 + 
               (112 + 21*s + 4*Power(s,2))*Power(t2,2) + 
               (15 + 16*s)*Power(t2,3) + 2*(3 + s)*Power(t2,4) + 
               5*Power(t2,5) + 
               Power(s1,2)*(4 + 19*t2 + 2*Power(t2,2) + 
                  4*Power(t2,3)) - 
               s1*(4 + 4*(17 + s)*t2 + (53 + 12*s)*Power(t2,2) + 
                  (-5 + 4*s)*Power(t2,3) + 9*Power(t2,4))) + 
            Power(s2,2)*(-4 - 3*(19 + 2*s)*t2 - 90*Power(t2,2) + 
               (-20 + 27*s + 8*Power(s,2))*Power(t2,3) - 
               2*(3 + 17*s + Power(s,2))*Power(t2,4) + 
               (13 + s)*Power(t2,5) + Power(t2,6) + 
               Power(s1,2)*(-12 - 11*t2 + 14*Power(t2,2) + 
                  Power(t2,3) + Power(t2,4)) + 
               s1*(28 + 2*(51 + s)*t2 + (-23 + s)*Power(t2,2) - 
                  3*(9 + 2*s)*Power(t2,3) + 30*Power(t2,4) - 
                  2*Power(t2,5))) + 
            s2*(8 + (23 + 4*s)*t2 - 
               (17 + 39*s + 4*Power(s,2))*Power(t2,2) + 
               5*(-8 + s)*Power(t2,3) + 
               (-79 + 6*s + 4*Power(s,2))*Power(t2,4) - 
               (-25 + 34*s + Power(s,2))*Power(t2,5) + 2*s*Power(t2,6) + 
               2*Power(s1,2)*
                (4 - 4*t2 - 7*Power(t2,2) + 2*Power(t2,3) + Power(t2,4)) \
+ s1*(-20 + 2*(-7 + 2*s)*t2 + 2*(26 + s)*Power(t2,2) - 
                  5*(-7 + s)*Power(t2,3) - (-3 + s)*Power(t2,5)))) - 
         Power(t1,6)*(4 + 8*s2 - 5*Power(s2,2) - 15*t2 - 8*s2*t2 - 
            2*Power(s2,2)*t2 - 3*Power(t2,2) - 2*s2*Power(t2,2) + 
            Power(s,2)*(2 + t2)*(-3 + s2 + t2) + 
            Power(s1,2)*(-4 + s2 - 2*t2 + s2*t2 + Power(t2,2)) + 
            s1*(4 - 14*t2 + 3*Power(t2,2) + Power(s2,2)*(2 + t2) + 
               s2*(-6 + 3*t2 + Power(t2,2))) - 
            s*(-12 - 19*t2 + 6*Power(t2,2) + Power(s2,2)*(1 + t2) + 
               s2*(-1 + 5*t2 + Power(t2,2)) + 
               s1*(-10 - 3*t2 + 2*Power(t2,2) + s2*(3 + 2*t2)))) - 
         Power(t1,5)*(5 + 2*s2 - 32*Power(s2,2) + 8*Power(s2,3) - 
            41*t2 - 13*s2*t2 - 8*Power(s2,2)*t2 + 10*Power(s2,3)*t2 + 
            45*Power(t2,2) + 33*s2*Power(t2,2) + 
            11*Power(s2,2)*Power(t2,2) + Power(s2,3)*Power(t2,2) + 
            3*Power(t2,3) + 7*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) - 
            Power(s1,2)*(-3 - 14*t2 - 7*Power(t2,2) + 3*Power(t2,3) + 
               Power(s2,2)*(1 + t2) + s2*(-5 + 4*Power(t2,2))) + 
            Power(s,2)*(5 + 2*Power(s2,2) + 31*t2 + 6*Power(t2,2) - 
               5*Power(t2,3) - s2*(4 + 8*t2 + 5*Power(t2,2))) - 
            s1*(10 + 11*t2 - 39*Power(t2,2) + 5*Power(t2,3) + 
               Power(s2,3)*(1 + 2*t2) + 
               Power(s2,2)*(-2 + t2 + 5*Power(t2,2)) + 
               s2*(-7 - 21*t2 - Power(t2,2) + 3*Power(t2,3))) + 
            s*(6 - 49*t2 + Power(s2,3)*t2 - 66*Power(t2,2) + 
               16*Power(t2,3) + 
               Power(s2,2)*(-7 + 2*t2 + 7*Power(t2,2)) + 
               s2*(11 - 19*t2 + 2*Power(t2,2) + 6*Power(t2,3)) + 
               s1*(6 - 41*t2 + Power(s2,2)*t2 - 16*Power(t2,2) + 
                  8*Power(t2,3) + s2*(-7 + 6*t2 + 9*Power(t2,2))))) + 
         Power(t1,4)*(-12 + 14*s2 + 30*Power(s2,2) - 28*Power(s2,3) + 
            4*Power(s2,4) - 4*t2 + 3*s2*t2 - 50*Power(s2,2)*t2 - 
            53*Power(s2,3)*t2 + 16*Power(s2,4)*t2 - 104*Power(t2,2) - 
            45*s2*Power(t2,2) - 19*Power(s2,2)*Power(t2,2) + 
            10*Power(s2,3)*Power(t2,2) + 5*Power(s2,4)*Power(t2,2) + 
            72*Power(t2,3) + 74*s2*Power(t2,3) + 
            28*Power(s2,2)*Power(t2,3) + 5*Power(s2,3)*Power(t2,3) + 
            9*s2*Power(t2,4) + 4*Power(s2,2)*Power(t2,4) + 
            Power(s1,2)*(2 + t2 + 18*Power(t2,2) + 9*Power(t2,3) - 
               3*Power(t2,4) + Power(s2,3)*(1 + t2) - 
               Power(s2,2)*(2 + 3*t2 + 2*Power(t2,2)) + 
               s2*(2 + 25*t2 + 8*Power(t2,2) - 6*Power(t2,3))) + 
            Power(s,2)*(-2 + 21*t2 + Power(s2,3)*t2 + 68*Power(t2,2) + 
               15*Power(t2,3) - 10*Power(t2,4) + 
               Power(s2,2)*t2*(8 + t2) - 
               s2*(-2 + 15*t2 + 14*Power(t2,2) + 10*Power(t2,3))) - 
            s1*(-2 + 14*t2 + Power(s2,4)*t2 + 17*Power(t2,2) - 
               55*Power(t2,3) + 7*Power(t2,4) + 
               Power(s2,3)*(2 + t2 + 7*Power(t2,2)) + 
               Power(s2,2)*(-17 - 22*t2 - 10*Power(t2,2) + 
                  11*Power(t2,3)) + 
               s2*(28 + 2*t2 - 43*Power(t2,2) - 14*Power(t2,3) + 
                  3*Power(t2,4))) + 
            s*(-10 + 21*t2 - 78*Power(t2,2) - 116*Power(t2,3) + 
               24*Power(t2,4) + 
               Power(s2,3)*(-4 - 14*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(13 + 6*t2 - 4*Power(t2,2) + 
                  19*Power(t2,3)) + 
               s2*(3 + 39*t2 - 66*Power(t2,2) - 30*Power(t2,3) + 
                  14*Power(t2,4)) - 
               s1*(-6 - 18*t2 + 65*Power(t2,2) + 32*Power(t2,3) - 
                  12*Power(t2,4) + Power(s2,3)*(3 + 2*t2) + 
                  Power(s2,2)*(-11 + 3*t2) + 
                  s2*(16 + 20*t2 - 14*Power(t2,3))))) + 
         Power(t1,3)*(-1 + 21*s2 - 11*Power(s2,2) - 12*Power(s2,3) + 
            4*Power(s2,4) + 20*t2 + 25*s2*t2 - 92*Power(s2,2)*t2 - 
            26*Power(s2,3)*t2 + 52*Power(s2,4)*t2 - 8*Power(s2,5)*t2 + 
            38*Power(t2,2) - 2*s2*Power(t2,2) + 
            34*Power(s2,2)*Power(t2,2) + 105*Power(s2,3)*Power(t2,2) + 
            9*Power(s2,4)*Power(t2,2) - 8*Power(s2,5)*Power(t2,2) + 
            120*Power(t2,3) + 114*s2*Power(t2,3) + 
            8*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) - 
            10*Power(s2,4)*Power(t2,3) - 67*Power(t2,4) - 
            93*s2*Power(t2,4) - 40*Power(s2,2)*Power(t2,4) - 
            10*Power(s2,3)*Power(t2,4) + 3*Power(t2,5) - 
            5*s2*Power(t2,5) - 6*Power(s2,2)*Power(t2,5) + 
            2*Power(s,2)*t2*(2 - 16*t2 - 2*Power(s2,3)*t2 - 
               40*Power(t2,2) - 10*Power(t2,3) + 5*Power(t2,4) - 
               Power(s2,2)*(2 + 5*t2 + Power(t2,2)) + 
               s2*t2*(8 + 6*t2 + 5*Power(t2,2))) - 
            Power(s1,2)*(-18 - 2*t2 + Power(t2,2) + 10*Power(t2,3) + 
               5*Power(t2,4) - Power(t2,5) + Power(s2,4)*(1 + t2) + 
               Power(s2,2)*(-5 - 6*t2 + 4*Power(t2,2)) + 
               Power(s2,3)*(-3 + 2*t2 + 4*Power(t2,2)) + 
               s2*(24 + 14*t2 + 23*Power(t2,2) + 14*Power(t2,3) - 
                  4*Power(t2,4))) + 
            s1*(-32 - 24*t2 + 2*Power(t2,2) + 13*Power(t2,3) - 
               41*Power(t2,4) + 6*Power(t2,5) + 
               Power(s2,4)*t2*(2 + 3*t2) + 
               Power(s2,3)*(-2 - 27*t2 - 18*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(s2,2)*(4 + 39*t2 - 11*Power(t2,2) - 
                  41*Power(t2,3) + 13*Power(t2,4)) + 
               s2*(28 + 48*t2 - 37*Power(t2,3) - 21*Power(t2,4) + 
                  Power(t2,5))) + 
            s*(-2 + 14*t2 - 35*Power(t2,2) + 64*Power(t2,3) + 
               114*Power(t2,4) - 21*Power(t2,5) + 
               Power(s2,4)*t2*(8 + 7*t2) - 
               Power(s2,3)*(4 + 19*t2 - 20*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*(2 - 40*t2 - 56*Power(t2,2) + 
                  28*Power(t2,3) - 24*Power(t2,4)) + 
               s2*(4 + 29*t2 - 44*Power(t2,2) + 83*Power(t2,3) + 
                  73*Power(t2,4) - 16*Power(t2,5)) + 
               s1*(-2 - 16*t2 + Power(s2,4)*t2 - 20*Power(t2,2) + 
                  49*Power(t2,3) + 30*Power(t2,4) - 8*Power(t2,5) + 
                  4*Power(s2,3)*t2*(1 + 2*t2) + 
                  2*s2*t2*(17 + 12*t2 + 3*Power(t2,2) - 4*Power(t2,3)) + 
                  Power(s2,2)*(2 - 15*t2 + 6*Power(t2,2) + 5*Power(t2,3)))\
)))*T3(t2,t1))/((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*Power(t1 - t2,2)*
       (-1 + s2 - t1 + t2)*Power(-t1 + s2*t2,2)) + 
    (8*(4*Power(s2,5)*Power(t2,2)*(1 + t2) - 
         Power(1 + (-1 + s)*t2,2)*Power(-1 + Power(t2,2),2) - 
         Power(t1,4)*(24 - (5 + 3*s1)*t2 + 4*(1 + s1)*Power(t2,2) - 
            (-1 + s1)*Power(t2,3) + 
            s*(20 - t2 - 4*Power(t2,2) + Power(t2,3))) - 
         t1*Power(-1 + t2,2)*(Power(s,2)*t2*
             (-4 - 11*t2 - 3*Power(t2,2) + Power(t2,3)) + 
            (-1 + t2)*(8 + 9*t2 - 6*Power(t2,2) + Power(t2,3) - 
               2*s1*Power(1 + t2,2)) + 
            s*(-2 - 9*t2 + 5*Power(t2,2) + 12*Power(t2,3) - 
               2*Power(t2,4) + 2*s1*(-1 + t2)*Power(1 + t2,2))) - 
         Power(t1,3)*(Power(s,2)*
             (16 + 5*t2 - 3*Power(t2,2) - 3*Power(t2,3) + Power(t2,4)) + 
            (-1 + t2)*(-24 + 25*t2 - 9*Power(t2,2) + 
               s1*(2 + 6*t2 - 8*Power(t2,2)) + 
               Power(s1,2)*(4 - 2*t2 - 3*Power(t2,2) + Power(t2,3))) + 
            s*(36 - 27*t2 - 9*Power(t2,2) + 19*Power(t2,3) - 
               3*Power(t2,4) + 
               s1*(8 - 15*t2 + 2*Power(t2,2) + 7*Power(t2,3) - 
                  2*Power(t2,4)))) + 
         Power(t1,2)*(Power(s,2)*
             (-7 + 11*t2 + 14*Power(t2,2) - 5*Power(t2,3) - 
               7*Power(t2,4) + 2*Power(t2,5)) - 
            Power(-1 + t2,2)*
             (-5 - 15*t2 + 12*Power(t2,2) + 
               Power(s1,2)*Power(1 + t2,2) - 
               s1*(12 + 7*t2 - 9*Power(t2,2) + 2*Power(t2,3))) - 
            s*(-1 + t2)*(-30 + 33*t2 + 10*Power(t2,2) - 25*Power(t2,3) + 
               4*Power(t2,4) + 
               s1*(-2 + 13*t2 - 4*Power(t2,2) - 9*Power(t2,3) + 
                  2*Power(t2,4)))) - 
         Power(s2,4)*(Power(s1,2)*Power(-1 + t2,2)*(1 + t2) - 
            s1*(-1 + t2)*t2*(-2 + t1 + s*(-1 + t2) - 2*t2 - t1*t2 + 
               2*Power(t2,2)) + 
            t2*(t1*(8 + (15 + 8*s)*t2 + 10*Power(t2,2) - Power(t2,3)) + 
               t2*(39 - 11*t2 - 5*Power(t2,2) + Power(t2,3) + 
                  s*(9 - 2*t2 + Power(t2,2))))) + 
         Power(s2,3)*(Power(s1,2)*Power(-1 + t2,2)*
             (3 + t1 + t2 + t1*t2 - Power(t2,2)) + 
            Power(t1,2)*(4 + 3*(6 + 5*s)*t2 + (23 + 10*s)*Power(t2,2) - 
               (-4 + s)*Power(t2,3) - Power(t2,4)) + 
            t1*t2*(79 + 38*t2 - 23*Power(t2,2) + 2*Power(t2,4) + 
               Power(s,2)*(1 + 6*t2 + Power(t2,2)) + 
               2*s*(9 + 14*t2 - 4*Power(t2,2) + Power(t2,3))) + 
            t2*(-2 + 30*t2 - 46*Power(t2,2) + 17*Power(t2,3) + 
               2*Power(t2,4) - Power(t2,5) + 
               Power(s,2)*t2*(5 - 6*t2 + Power(t2,2)) + 
               s*(1 + 44*t2 - 35*Power(t2,2) + 6*Power(t2,3))) + 
            s1*(-1 + t2)*(-2 + (5 + s)*t2 + (8 + s)*Power(t2,2) - 
               13*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,2)*(-1 - t2 + 2*Power(t2,2)) + 
               t1*(2 + 4*t2 + 5*Power(t2,2) - 3*Power(t2,3) - 
                  s*(-3 + t2 + 2*Power(t2,2))))) + 
         s2*(Power(-1 + t2,2)*
             (3 + (-9 + 6*s)*t2 + (-7 - 3*s + Power(s,2))*Power(t2,2) + 
               (17 - 5*s - 2*Power(s,2))*Power(t2,3) + 
               (-4 + 6*s)*Power(t2,4)) + 
            Power(t1,4)*(3 + 6*t2 - Power(t2,2) + 
               s*(7 + 2*t2 - Power(t2,2))) + 
            Power(s1,2)*t1*Power(-1 + t2,2)*
             (-(Power(t1,2)*(1 + t2)) + 2*Power(1 + t2,2) + 
               t1*(-5 - 3*t2 + Power(t2,2))) + 
            Power(t1,3)*(64 + 29*t2 - 8*Power(t2,2) + 9*Power(t2,3) + 
               2*Power(t2,4) + Power(s,2)*(6 + 3*t2 - Power(t2,3)) + 
               s*(31 + 38*t2 - 14*Power(t2,2) + Power(t2,4))) + 
            Power(t1,2)*(28 - 7*t2 - 61*Power(t2,2) + 54*Power(t2,3) - 
               13*Power(t2,4) - Power(t2,5) + 
               2*Power(s,2)*(5 + 15*t2 - 4*Power(t2,2) - Power(t2,3) + 
                  Power(t2,4)) + 
               s*(41 + 40*t2 - 60*Power(t2,2) + 17*Power(t2,3) + 
                  13*Power(t2,4) - 3*Power(t2,5))) - 
            t1*(-(Power(-1 + t2,2)*
                  (-4 - 13*t2 - 12*Power(t2,2) + 13*Power(t2,3))) + 
               Power(s,2)*t2*
                (-3 + 28*t2 - 10*Power(t2,2) + Power(t2,4)) + 
               s*(1 - 58*t2 + 106*Power(t2,2) - 42*Power(t2,3) - 
                  25*Power(t2,4) + 20*Power(t2,5) - 2*Power(t2,6))) + 
            s1*(-1 + t2)*(Power(t1,4)*(-1 + t2) - 
               2*Power(-1 + Power(t2,2),2) - 
               t1*(-1 + Power(t2,2))*
                (6 + 9*t2 - 3*Power(t2,2) + s*(10 - t2 + Power(t2,2))) + 
               Power(t1,3)*(t2*(8 + t2 - Power(t2,2)) + 
                  s*(-3 + t2 + 2*Power(t2,2))) + 
               Power(t1,2)*(7 + 12*t2 - 15*Power(t2,2) - 4*Power(t2,3) - 
                  3*s*(1 - 2*t2 - 2*Power(t2,2) + Power(t2,3))))) + 
         Power(s2,2)*(-1 + 6*t2 - 7*s*t2 + Power(t2,2) - 
            18*s*Power(t2,2) - 4*Power(s,2)*Power(t2,2) - 
            19*Power(t2,3) + 57*s*Power(t2,3) + 
            19*Power(s,2)*Power(t2,3) + 14*Power(t2,4) - 
            39*s*Power(t2,4) - 8*Power(s,2)*Power(t2,4) + Power(t2,5) + 
            6*s*Power(t2,5) + Power(s,2)*Power(t2,5) - 2*Power(t2,6) + 
            s*Power(t2,6) + Power(s1,2)*Power(-1 + t2,2)*
             (Power(t1,2)*(1 + t2) - Power(1 + t2,2) + 
               t1*(-2 + Power(t2,2))) + 
            Power(t1,3)*(-7 - 16*t2 - 11*Power(t2,2) + 2*Power(t2,3) + 
               s*(-7 - 17*t2 - Power(t2,2) + Power(t2,3))) - 
            Power(t1,2)*(40 + 113*t2 - 13*Power(t2,2) - 4*Power(t2,3) + 
               7*Power(t2,4) + Power(t2,5) + 
               2*Power(s,2)*(1 + 6*t2 + Power(t2,2)) + 
               s*(9 + 61*t2 + 9*Power(t2,2) - 11*Power(t2,3) + 
                  4*Power(t2,4))) + 
            t1*(2 - 58*t2 + 77*Power(t2,2) - 5*Power(t2,3) - 
               22*Power(t2,4) + 5*Power(t2,5) + Power(t2,6) + 
               Power(s,2)*t2*
                (-10 - 9*t2 + 2*Power(t2,2) + Power(t2,3)) + 
               s*(1 - 87*t2 + 27*Power(t2,2) + 31*Power(t2,3) - 
                  24*Power(t2,4) + 4*Power(t2,5))) - 
            s1*(-1 + t2)*(Power(t1,3)*(-2 + t2 + Power(t2,2)) + 
               Power(t1,2)*(2 - (-7 + s)*t2 + (5 + s)*Power(t2,2) - 
                  2*Power(t2,3)) + 
               (-1 + t2)*(6 - 2*(4 + s)*t2 - 5*(3 + s)*Power(t2,2) + 
                  (5 + s)*Power(t2,3)) + 
               t1*(3 + 11*t2 + Power(t2,2) - 17*Power(t2,3) + 
                  2*Power(t2,4) + 
                  s*(9 - 4*t2 - 2*Power(t2,2) + 3*Power(t2,3))))))*
       T4(-1 + t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + t2)*(-1 + s2 - t1 + t2)*
       Power(-t1 + s2*t2,2)) - 
    (8*(Power(t1,5)*(2 + s*(-2 + t2) - s1*(-2 + t2) + t2) - 
         (-1 + t2)*Power(1 + t2,2)*Power(1 + (-1 + s)*t2,2) - 
         Power(s2,5)*(Power(s1,2)*(1 + t2) + 
            Power(t2,2)*(-1 + s - t1 + t2) - s1*t2*(2 + s - t1 + 2*t2)) - 
         t1*(Power(s,2)*t2*(4 + 6*t2 - 10*Power(t2,2) - 5*Power(t2,3) + 
               Power(t2,4)) + 
            Power(-1 + t2,2)*(7 + 7*t2 - 7*Power(t2,2) + Power(t2,3) - 
               2*s1*Power(1 + t2,2)) + 
            s*(2 + 5*t2 - 16*Power(t2,2) - 5*Power(t2,3) + 
               16*Power(t2,4) - 2*Power(t2,5) + 
               2*s1*Power(-1 + Power(t2,2),2))) + 
         Power(t1,4)*(8 - 10*t2 - Power(t2,2) + 
            s1*(4 - 11*t2 + Power(t2,2)) + 
            Power(s1,2)*(-4 - 2*t2 + Power(t2,2)) + 
            Power(s,2)*(-6 - t2 + Power(t2,2)) + 
            s*(s1*(10 + 3*t2 - 2*Power(t2,2)) - 
               4*(-3 - 4*t2 + Power(t2,2)))) - 
         Power(t1,3)*(-21 + 38*t2 - 21*Power(t2,2) + 
            Power(s1,2)*(3 - 4*t2 - 4*Power(t2,2) + Power(t2,3)) + 
            s1*(10 + 13*t2 - 17*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s,2)*(1 - 18*t2 - 5*Power(t2,2) + 3*Power(t2,3)) + 
            s*(-12 + 12*t2 + 37*Power(t2,2) - 7*Power(t2,3) - 
               2*s1*(9 - 9*t2 - 6*Power(t2,2) + 2*Power(t2,3)))) + 
         Power(t1,2)*(Power(s,2)*
             (7 + 4*t2 - 21*Power(t2,2) - 8*Power(t2,3) + 3*Power(t2,4)) \
+ (-1 + t2)*(9 + 24*t2 - 18*Power(t2,2) + Power(t2,3) - 
               Power(s1,2)*Power(1 + t2,2) + 
               s1*(10 + 3*t2 - 11*Power(t2,2) + 2*Power(t2,3))) - 
            s*(-24 + 26*t2 + 5*Power(t2,2) - 37*Power(t2,3) + 
               6*Power(t2,4) + 
               s1*(8 + 15*t2 - 6*Power(t2,2) - 11*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         Power(s2,4)*(Power(s1,2)*
             (4 + t2 - 2*Power(t2,2) + 2*t1*(1 + t2)) + 
            s1*(2 - (5 + 2*s)*t2 + (-11 + s)*Power(t2,2) + 
               4*Power(t2,3) + Power(t1,2)*(1 + 3*t2) - 
               t1*(2 + 3*t2 + 6*Power(t2,2) + 3*s*(1 + t2))) + 
            t2*(-2 + 11*t2 + 2*Power(t2,2) - 2*Power(t2,3) - 
               2*Power(t1,2)*(1 + t2) + Power(s,2)*(t1 + t2) + 
               t1*(-1 + 4*Power(t2,2)) - 
               s*(-1 + Power(t1,2) - 7*t2 + Power(t2,2) - t1*(2 + 3*t2)))) \
+ Power(s2,3)*(-1 + 6*t2 - 8*s*t2 - 4*Power(t2,2) - 27*s*Power(t2,2) - 
            5*Power(s,2)*Power(t2,2) - 3*Power(t2,3) + 12*s*Power(t2,3) + 
            2*Power(s,2)*Power(t2,3) - Power(t2,4) + s*Power(t2,4) - 
            Power(t2,5) + Power(t1,3)*
             (1 + s + 4*t2 + 2*s*t2 + Power(t2,2)) - 
            Power(s1,2)*(4 - Power(t2,2) + Power(t2,3) + 
               t1*(6 + t2 - 3*Power(t2,2))) - 
            Power(t1,2)*(Power(s,2)*(2 + t2) + 4*Power(t2,2)*(2 + t2) + 
               s*(1 + 4*t2 + 7*Power(t2,2))) + 
            t1*(2 - 23*t2 + Power(s,2)*(-3 + t2)*t2 - 18*Power(t2,2) + 
               11*Power(t2,3) + 4*Power(t2,4) + 
               s*(1 - 8*t2 - 12*Power(t2,2) + 6*Power(t2,3))) + 
            s1*(-8 + (13 + 3*s)*t2 + (19 + 6*s)*Power(t2,2) - 
               (18 + s)*Power(t2,3) + 2*Power(t2,4) - 
               3*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(1 + 7*Power(t2,2) + s*(3 + t2)) + 
               t1*(3 + 17*t2 + 23*Power(t2,2) - 7*Power(t2,3) + 
                  s*(12 + t2 - 5*Power(t2,2))))) + 
         Power(s2,2)*(4 - 14*t2 + 13*s*t2 - 9*Power(t2,2) + 
            18*s*Power(t2,2) + 5*Power(s,2)*Power(t2,2) + 
            26*Power(t2,3) - 21*s*Power(t2,3) - 
            5*Power(s,2)*Power(t2,3) - 5*Power(t2,4) + 13*s*Power(t2,4) + 
            Power(s,2)*Power(t2,4) - 2*Power(t2,5) + s*Power(t2,5) + 
            Power(t1,3)*(1 + s + 7*t2 + 5*s*t2 - Power(s,2)*t2 + 
               9*Power(t2,2) + 6*s*Power(t2,2) + Power(t2,3)) - 
            Power(t1,4)*(2*(1 + t2) + s*(2 + t2)) + 
            Power(s1,2)*(-2*Power(t1,3)*(1 + t2) - 
               (-1 + t2)*Power(1 + t2,2) + 
               Power(t1,2)*(-4 - 3*t2 + Power(t2,2)) + 
               t1*(5 + 4*t2 + 2*Power(t2,2) + Power(t2,3))) + 
            Power(t1,2)*(Power(s,2)*(8 + 2*t2 + Power(t2,2)) + 
               s*(1 + 21*t2 + 18*Power(t2,2) - 11*Power(t2,3)) - 
               2*(-6 - 21*t2 + 11*Power(t2,2) + 6*Power(t2,3) + 
                  Power(t2,4))) + 
            t1*(-5 + 11*t2 - 5*Power(t2,2) + 2*Power(t2,3) + 
               8*Power(t2,4) + Power(t2,5) - 
               Power(s,2)*t2*(-5 + 3*t2 + Power(t2,2)) + 
               s*(-2 + 49*t2 - 38*Power(t2,3) + 5*Power(t2,4))) + 
            s1*(8 - 2*(6 + s)*t2 - (9 + 11*s)*Power(t2,2) + 
               6*(3 + s)*Power(t2,3) - (5 + s)*Power(t2,4) + 
               Power(t1,4)*(3 + t2) + 
               Power(t1,3)*(6 - 4*Power(t2,2) + 3*s*(1 + t2)) - 
               Power(t1,2)*(10 + 24*t2 + 18*Power(t2,2) - 
                  4*Power(t2,3) + s*(4 - 3*t2 + Power(t2,2))) - 
               t1*(3 + 26*t2 + 18*Power(t2,2) - 25*Power(t2,3) + 
                  2*Power(t2,4) + 
                  s*(19 + 7*t2 - 5*Power(t2,2) + 3*Power(t2,3))))) + 
         s2*(-4 + (1 + s)*Power(t1,5) + 12*t2 - 8*s*t2 + 7*s*Power(t2,2) - 
            2*Power(s,2)*Power(t2,2) - 24*Power(t2,3) + 4*s*Power(t2,3) + 
            Power(s,2)*Power(t2,3) + 20*Power(t2,4) - 9*s*Power(t2,4) - 
            3*Power(s,2)*Power(t2,4) - 4*Power(t2,5) + 6*s*Power(t2,5) + 
            Power(t1,4)*(-3 - 7*t2 - 2*Power(t2,2) + 
               Power(s,2)*(2 + t2) - s*(-2 + 4*t2 + Power(t2,2))) + 
            Power(s1,2)*t1*(Power(t1,3)*(1 + t2) + 
               2*(-1 + t2)*Power(1 + t2,2) + 
               Power(t1,2)*(10 + 5*t2 - 3*Power(t2,2)) + 
               t1*(2 - 8*t2 - 7*Power(t2,2) + Power(t2,3))) + 
            Power(t1,3)*(-26 + 5*t2 + 18*Power(t2,2) + 3*Power(t2,3) - 
               2*Power(s,2)*(-1 + t2 + 2*Power(t2,2)) + 
               s*(-12 - 36*t2 - 3*Power(t2,2) + 4*Power(t2,3))) + 
            Power(t1,2)*(-5 - 15*t2 + 36*Power(t2,2) - 27*Power(t2,3) - 
               Power(t2,4) + Power(s,2)*
                (-13 - 8*t2 - Power(t2,2) + 5*Power(t2,3)) + 
               s*(-20 - 4*t2 + 45*Power(t2,2) + 22*Power(t2,3) - 
                  5*Power(t2,4))) + 
            t1*(9 + 11*t2 - Power(t2,2) - 35*Power(t2,3) + 
               16*Power(t2,4) + 
               Power(s,2)*(t2 + 11*Power(t2,2) + 4*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s*(3 - 46*t2 + 15*Power(t2,2) - 22*Power(t2,4) + 
                  2*Power(t2,5))) - 
            s1*(Power(t1,5) + 2*Power(-1 + Power(t2,2),2) + 
               Power(t1,4)*(7 - 2*t2 - Power(t2,2) + s*(3 + 2*t2)) + 
               Power(t1,3)*(-1 - 23*t2 - 5*Power(t2,2) + Power(t2,3) + 
                  s*(18 + 5*t2 - 7*Power(t2,2))) + 
               Power(t1,2)*(-21 - 26*t2 + 18*Power(t2,2) + 
                  5*Power(t2,3) + 
                  s*(-5 - 10*t2 - 13*Power(t2,2) + 4*Power(t2,3))) + 
               t1*(-2 - 5*t2 + 5*Power(t2,2) + 5*Power(t2,3) - 
                  3*Power(t2,4) + 
                  s*(-12 - 17*t2 + 11*Power(t2,2) + Power(t2,3) + 
                     Power(t2,4))))))*T5(1 - s2 + t1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s2 - t1 + t2)*
       Power(-t1 + s2*t2,2)));
   return a;
};
