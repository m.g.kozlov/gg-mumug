#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_1_m132_1_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(2*Power(s1,6)*(s2 + Power(s2,2) - 2*s2*t1 + (-1 + t1)*t1) + 
         Power(s1,5)*(s2 + Power(s2,2) - 2*s2*t1 + (-1 + t1)*t1)*
          (s2 + t1 - 8*t2) - 2*Power(s,6)*Power(s1 - s2 + t1 - t2,2) + 
         Power(s,5)*(6 + 10*Power(s1,3) + 4*Power(s2,3) + 2*t1 + 
            3*Power(t1,2) - 4*Power(t1,3) + 
            Power(s1,2)*(8 - 20*s2 + 17*t1 - 27*t2) - 2*t2 - 6*t1*t2 + 
            2*Power(t1,2)*t2 + 3*Power(t2,2) + 8*t1*Power(t2,2) - 
            6*Power(t2,3) + 2*Power(s2,2)*(3 - 6*t1 + t2) + 
            s2*(-8 - 9*t1 + 12*Power(t1,2) + 9*t2 - 4*t1*t2 - 
               8*Power(t2,2)) + 
            s1*(-10 + 5*t1 + 3*Power(t1,2) - 5*t2 - 26*t1*t2 + 
               23*Power(t2,2) + s2*(4 - 3*t1 + 23*t2))) + 
         Power(s1,4)*(3*Power(s2,4) + Power(s2,3)*(5 - 10*t1) + 
            Power(t1,4) + 8*Power(-1 + t2,2) - Power(t1,3)*(7 + 5*t2) - 
            2*t1*(5 - t2 + 10*Power(t2,2)) + 
            Power(t1,2)*(4 + 21*t2 + 12*Power(t2,2)) + 
            Power(s2,2)*(2 + 12*Power(t1,2) + 15*t2 + 12*Power(t2,2) - 
               t1*(17 + 5*t2)) + 
            s2*(10 - 6*Power(t1,3) - 2*t2 + 20*Power(t2,2) + 
               Power(t1,2)*(19 + 10*t2) - 6*t1*(1 + 6*t2 + 4*Power(t2,2)))\
) + Power(s1,3)*(2*Power(t1,5) + Power(s2,4)*(-6 + 2*t1 - 9*t2) - 
            8*Power(-1 + t2,2)*(1 + 3*t2) - 2*Power(t1,4)*(8 + 3*t2) + 
            Power(t1,3)*(9 + 53*t2 + 9*Power(t2,2)) - 
            Power(t1,2)*(47 - 29*t2 + 83*Power(t2,2) + 8*Power(t2,3)) + 
            t1*(40 - 42*t2 + 26*Power(t2,2) + 32*Power(t2,3)) - 
            Power(s2,3)*(8*Power(t1,2) - t1*(34 + 33*t2) + 
               6*(3 + 5*t2 + Power(t2,2))) + 
            Power(s2,2)*(-56 + 12*Power(t1,3) + 43*t2 - 76*Power(t2,2) - 
               8*Power(t2,3) - 3*Power(t1,2)*(22 + 15*t2) + 
               t1*(45 + 113*t2 + 21*Power(t2,2))) - 
            s2*(40 + 8*Power(t1,4) - 42*t2 + 26*Power(t2,2) + 
               32*Power(t2,3) - 27*Power(t1,3)*(2 + t2) + 
               4*Power(t1,2)*(9 + 34*t2 + 6*Power(t2,2)) - 
               t1*(103 - 72*t2 + 159*Power(t2,2) + 16*Power(t2,3)))) + 
         (-1 + t2)*(16*Power(-1 + t2,2)*Power(t2,2) + 
            2*Power(s2,4)*(8 - 5*t2 + 2*Power(t2,2)) + 
            Power(t1,4)*(16 - t2 + 2*Power(t2,2)) - 
            4*Power(t1,3)*(10 - t2 + 4*Power(t2,2) + Power(t2,3)) - 
            t1*t2*(36 - 45*t2 + 9*Power(t2,2) + 16*Power(t2,3)) + 
            Power(t1,2)*(32 + 9*t2 - 21*Power(t2,2) + 33*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,3)*(40 - 5*t2 + 7*Power(t2,2) + 5*Power(t2,3) + 
               t1*(-64 + 31*t2 - 14*Power(t2,2))) + 
            Power(s2,2)*(32 + 21*t2 - 40*Power(t2,2) + 39*Power(t2,3) + 
               Power(t2,4) + 3*Power(t1,2)*
                (32 - 11*t2 + 6*Power(t2,2)) - 
               2*t1*(60 - 7*t2 + 15*Power(t2,2) + 7*Power(t2,3))) + 
            s2*(Power(t1,3)*(-64 + 13*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(120 - 13*t2 + 39*Power(t2,2) + 
                  13*Power(t2,3)) + 
               t2*(36 - 45*t2 + 9*Power(t2,2) + 16*Power(t2,3)) - 
               t1*(64 + 30*t2 - 61*Power(t2,2) + 72*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(s1,2)*(4*Power(s2,5) - 2*Power(t1,5)*(1 + 2*t2) + 
            8*Power(-1 + t2,2)*(-2 + 4*t2 + 3*Power(t2,2)) + 
            Power(t1,4)*(-11 + 39*t2 + 9*Power(t2,2)) - 
            Power(t1,3)*(58 - 56*t2 + 105*Power(t2,2) + 7*Power(t2,3)) + 
            t1*(15 - 110*t2 + 145*Power(t2,2) - 74*Power(t2,3) - 
               26*Power(t2,4)) + 
            Power(t1,2)*(36 + 33*t2 - 47*Power(t2,2) + 109*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,3)*(52 - 33*t2 + 66*Power(t2,2) + 8*Power(t2,3) + 
               16*Power(t1,2)*(2 + t2) + 
               t1*(47 - 111*t2 - 36*Power(t2,2))) + 
            Power(s2,4)*(-2*t1*(9 + 2*t2) + 
               3*(-4 + 8*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(43 + 38*t2 - 62*Power(t2,2) + 104*Power(t2,3) + 
               2*Power(t2,4) - 4*Power(t1,3)*(7 + 6*t2) + 
               3*Power(t1,2)*(-23 + 63*t2 + 18*Power(t2,2)) - 
               t1*(162 - 122*t2 + 237*Power(t2,2) + 23*Power(t2,3))) + 
            s2*(-15 + 110*t2 - 145*Power(t2,2) + 74*Power(t2,3) + 
               26*Power(t2,4) + 4*Power(t1,4)*(3 + 4*t2) - 
               3*Power(t1,3)*(-15 + 47*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(168 - 145*t2 + 276*Power(t2,2) + 
                  22*Power(t2,3)) - 
               t1*(79 + 71*t2 - 109*Power(t2,2) + 213*Power(t2,3) + 
                  4*Power(t2,4)))) - 
         s1*(4*Power(s2,5)*(-1 + t2) - 
            2*Power(t1,5)*(-2 + t2 + Power(t2,2)) + 
            8*Power(-1 + t2,2)*t2*(-4 + 5*t2 + Power(t2,2)) + 
            Power(t1,4)*(9 - 22*t2 + 25*Power(t2,2) + 4*Power(t2,3)) + 
            Power(t1,3)*(36 - 94*t2 + 69*Power(t2,2) - 63*Power(t2,3) - 
               2*Power(t2,4)) + 
            Power(t1,2)*(-57 + 130*t2 - 84*Power(t2,2) + 
               17*Power(t2,3) + 48*Power(t2,4)) - 
            2*t1*(-18 + 33*t2 + 8*Power(t2,2) - 50*Power(t2,3) + 
               31*Power(t2,4) + 4*Power(t2,5)) + 
            Power(s2,4)*(18 - 34*t2 + 22*Power(t2,2) + 3*Power(t2,3) - 
               2*t1*(-10 + 9*t2 + Power(t2,2))) + 
            Power(s2,3)*(-35 + 96*t2 - 65*Power(t2,2) + 46*Power(t2,3) + 
               3*Power(t2,4) + 8*Power(t1,2)*(-5 + 4*t2 + Power(t2,2)) - 
               t1*(63 - 124*t2 + 91*Power(t2,2) + 13*Power(t2,3))) + 
            Power(s2,2)*(-69 + 168*t2 - 113*Power(t2,2) + 
               21*Power(t2,3) + 45*Power(t2,4) - 
               4*Power(t1,3)*(-10 + 7*t2 + 3*Power(t2,2)) + 
               3*Power(t1,2)*
                (27 - 56*t2 + 47*Power(t2,2) + 7*Power(t2,3)) + 
               t1*(106 - 286*t2 + 199*Power(t2,2) - 155*Power(t2,3) - 
                  8*Power(t2,4))) + 
            s2*(4*Power(t1,4)*(-5 + 3*t2 + 2*Power(t2,2)) - 
               Power(t1,3)*(45 - 100*t2 + 97*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(126 - 298*t2 + 197*Power(t2,2) - 38*Power(t2,3) - 
                  93*Power(t2,4)) + 
               Power(t1,2)*(-107 + 284*t2 - 203*Power(t2,2) + 
                  172*Power(t2,3) + 7*Power(t2,4)) + 
               2*(-18 + 33*t2 + 8*Power(t2,2) - 50*Power(t2,3) + 
                  31*Power(t2,4) + 4*Power(t2,5)))) - 
         Power(s,4)*(24 + 12*Power(s1,4) + 2*Power(s2,4) - 19*t1 + 
            3*Power(t1,2) - 12*Power(t1,3) + 2*Power(t1,4) + 
            Power(s1,3)*(20 - 21*s2 + 13*t1 - 50*t2) + 
            Power(s2,3)*(17 - 8*t1 - 8*t2) - 11*t2 - 12*t1*t2 + 
            22*Power(t1,2)*t2 + 8*Power(t1,3)*t2 + 9*Power(t2,2) - 
            8*t1*Power(t2,2) - 16*Power(t1,2)*Power(t2,2) - 
            2*Power(t2,3) + 6*Power(t2,4) + 
            Power(s1,2)*(32 - 31*Power(s2,2) - 17*Power(t1,2) + 
               t1*(5 - 33*t2) - 52*t2 + 70*Power(t2,2) + 
               2*s2*(6 + 24*t1 + 17*t2)) + 
            Power(s2,2)*(9 + 12*Power(t1,2) + 17*t2 - 16*Power(t2,2) + 
               t1*(-46 + 24*t2)) - 
            s2*(5 + 8*Power(t1,3) - 48*t2 + 2*Power(t2,2) + 
               Power(t1,2)*(-41 + 24*t2) + 
               t1*(12 + 39*t2 - 32*Power(t2,2))) + 
            s1*(-37 + 8*Power(s2,3) - 16*Power(t1,3) - 17*t2 + 
               34*Power(t2,2) - 38*Power(t2,3) + 
               Power(s2,2)*(17 - 32*t1 + 33*t2) + 
               Power(t1,2)*(3 + 34*t2) + 
               t1*(71 - 37*t2 + 20*Power(t2,2)) + 
               s2*(-59 + 40*Power(t1,2) + 6*t2 - 13*Power(t2,2) - 
                  t1*(20 + 67*t2)))) + 
         Power(s,3)*(30 + 2*Power(s1,5) - 78*t1 + 77*Power(t1,2) - 
            21*Power(t1,3) + 9*Power(t1,4) + Power(s2,4)*(11 - 6*t2) - 
            18*t2 - 70*t1*t2 + 31*Power(t1,2)*t2 + 4*Power(t1,3)*t2 - 
            6*Power(t1,4)*t2 + 53*Power(t2,2) + 5*t1*Power(t2,2) - 
            40*Power(t1,2)*Power(t2,2) - 15*Power(t2,3) + 
            32*t1*Power(t2,3) + 16*Power(t1,2)*Power(t2,3) - 
            5*Power(t2,4) - 8*t1*Power(t2,4) - 2*Power(t2,5) - 
            Power(s1,4)*(-20 + 2*s2 + 5*t1 + 23*t2) + 
            Power(s1,3)*(14 - 54*Power(s2,2) + 6*t1 + 85*s2*t1 - 
               31*Power(t1,2) - 80*t2 - 11*s2*t2 + 20*t1*t2 + 
               59*Power(t2,2)) + 
            2*Power(s2,3)*(22 - 9*t2 + 3*t1*(-7 + 4*t2)) + 
            Power(s2,2)*(63 + Power(t1,2)*(60 - 36*t2) + 53*t2 - 
               44*Power(t2,2) + 16*Power(t2,3) + t1*(-109 + 40*t2)) + 
            2*s2*(24 + Power(t1,2)*(43 - 13*t2) + 76*t2 - 
               33*Power(t2,2) - 10*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,3)*(-19 + 12*t2) - 
               2*t1*(35 + 21*t2 - 21*Power(t2,2) + 8*Power(t2,3))) + 
            Power(s1,2)*(89 - 10*Power(s2,3) - 13*Power(t1,3) - 75*t2 + 
               103*Power(t2,2) - 59*Power(t2,3) + 
               Power(t1,2)*(3 + 85*t2) + 
               Power(s2,2)*(23 + 7*t1 + 95*t2) + 
               t1*(-73 + 2*t2 - 33*Power(t2,2)) + 
               2*s2*(56 + 8*Power(t1,2) - 25*t2 + 18*Power(t2,2) - 
                  t1*(13 + 90*t2))) + 
            s1*(-42 + 4*Power(s2,4) + 11*Power(t1,4) - 112*t2 + 
               76*Power(t2,2) - 38*Power(t2,3) + 23*Power(t2,4) + 
               Power(s2,3)*(30 - 23*t1 + t2) + 
               2*Power(t1,3)*(-17 + 5*t2) - 
               2*Power(t1,2)*(66 - 52*t2 + 35*Power(t2,2)) + 
               t1*(268 - 60*t2 - 32*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s2,2)*(-92 + 45*Power(t1,2) + 40*t2 - 
                  57*Power(t2,2) + t1*(-94 + 8*t2)) + 
               s2*(-290 - 37*Power(t1,3) + Power(t1,2)*(98 - 19*t2) + 
                  52*t2 + 62*Power(t2,2) - 31*Power(t2,3) + 
                  t1*(224 - 144*t2 + 127*Power(t2,2))))) + 
         Power(s,2)*(-12 + 2*Power(s1,6) + 85*t1 - 186*Power(t1,2) + 
            81*Power(t1,3) - 8*Power(t1,4) + 
            Power(s1,5)*(-6 + s2 + t1 - 4*t2) + 5*t2 + 134*t1*t2 + 
            19*Power(t1,2)*t2 - 34*Power(t1,3)*t2 + 20*Power(t1,4)*t2 - 
            92*Power(t2,2) - 143*t1*Power(t2,2) + 
            72*Power(t1,2)*Power(t2,2) - 32*Power(t1,3)*Power(t2,2) - 
            6*Power(t1,4)*Power(t2,2) + 103*Power(t2,3) - 
            14*t1*Power(t2,3) + 8*Power(t1,3)*Power(t2,3) - 
            16*Power(t2,4) + 16*t1*Power(t2,4) + 
            2*Power(t1,2)*Power(t2,4) - 4*Power(t2,5) - 
            4*t1*Power(t2,5) + 
            Power(s2,4)*(-19 + 26*t2 - 6*Power(t2,2)) + 
            Power(s1,4)*(12 + 27*Power(s2,2) + 11*Power(t1,2) + 35*t2 - 
               4*Power(t2,2) + s2*(5 - 38*t1 + 14*t2) - t1*(1 + 19*t2)) + 
            Power(s2,3)*(-100 + 70*t2 + 20*Power(t2,2) - 8*Power(t2,3) + 
               t1*(65 - 98*t2 + 24*Power(t2,2))) - 
            Power(s2,2)*(157 + 61*t2 - 141*Power(t2,2) + 
               12*Power(t2,3) - 2*Power(t2,4) + 
               3*Power(t1,2)*(27 - 46*t2 + 12*Power(t2,2)) + 
               t1*(-281 + 174*t2 + 72*Power(t2,2) - 24*Power(t2,3))) + 
            s2*(-73 - 195*t2 + 226*Power(t2,2) - 24*Power(t2,3) - 
               10*Power(t2,4) + 4*Power(t2,5) + 
               Power(t1,3)*(43 - 86*t2 + 24*Power(t2,2)) + 
               Power(t1,2)*(-262 + 138*t2 + 84*Power(t2,2) - 
                  24*Power(t2,3)) + 
               t1*(343 + 42*t2 - 213*Power(t2,2) + 12*Power(t2,3) - 
                  4*Power(t2,4))) + 
            Power(s1,3)*(-22 + 29*Power(s2,3) - 5*Power(t1,3) + 
               Power(s2,2)*(1 - 63*t1 - 65*t2) + 
               Power(t1,2)*(10 - 44*t2) - 18*t2 - 65*Power(t2,2) + 
               16*Power(t2,3) + t1*(86 - 47*t2 + 55*Power(t2,2)) + 
               s2*(-109 + 39*Power(t1,2) + 56*t2 - 52*Power(t2,2) + 
                  t1*(-11 + 109*t2))) + 
            Power(s1,2)*(-126 + Power(s2,4) - 15*Power(t1,4) + 
               Power(s2,3)*(-19 + 12*t1 - 44*t2) + 190*t2 - 
               32*Power(t2,2) + 45*Power(t2,3) - 14*Power(t2,4) + 
               Power(t1,3)*(37 + 23*t2) + 
               Power(t1,2)*(30 - 97*t2 + 57*Power(t2,2)) + 
               t1*(92 - 250*t2 + 137*Power(t2,2) - 61*Power(t2,3)) - 
               Power(s2,2)*(34 + 42*Power(t1,2) + t2 - 51*Power(t2,2) - 
                  3*t1*(25 + 37*t2)) + 
               s2*(-103 + 44*Power(t1,3) + 303*t2 - 161*Power(t2,2) + 
                  62*Power(t2,3) - 3*Power(t1,2)*(31 + 30*t2) + 
                  t1*(4 + 98*t2 - 108*Power(t2,2)))) + 
            s1*(19 + 2*Power(t1,5) + 206*t2 - 271*Power(t2,2) + 
               54*Power(t2,3) - 5*Power(t2,4) + 4*Power(t2,5) + 
               Power(s2,4)*(-17 + 2*t1 + 5*t2) + 
               2*Power(t1,4)*(-14 + 9*t2) + 
               Power(t1,3)*(-28 + 27*t2 - 26*Power(t2,2)) + 
               Power(t1,2)*(333 - 312*t2 + 111*Power(t2,2) - 
                  26*Power(t2,3)) + 
               t1*(-336 + 184*t2 + 162*Power(t2,2) - 105*Power(t2,3) + 
                  28*Power(t2,4)) - 
               Power(s2,3)*(12 + 8*Power(t1,2) + 6*t2 - 23*Power(t2,2) + 
                  t1*(-79 + 33*t2)) + 
               Power(s2,2)*(339 + 12*Power(t1,3) - 272*t2 + 
                  36*Power(t2,2) - 15*Power(t2,3) + 
                  3*Power(t1,2)*(-45 + 23*t2) + 
                  t1*(-4 + 39*t2 - 72*Power(t2,2))) + 
               s2*(373 - 8*Power(t1,4) + Power(t1,3)*(101 - 59*t2) - 
                  244*t2 - 154*Power(t2,2) + 110*Power(t2,3) - 
                  29*Power(t2,4) + 
                  Power(t1,2)*(44 - 60*t2 + 75*Power(t2,2)) + 
                  t1*(-672 + 584*t2 - 147*Power(t2,2) + 41*Power(t2,3))))) \
+ s*(-28*t1 + 143*Power(t1,2) - 108*Power(t1,3) + 17*Power(t1,4) + 
            Power(s1,6)*(-2 - 4*s2 + 4*t1) + 4*t2 - 116*t1*t2 - 
            60*Power(t1,2)*t2 + 101*Power(t1,3)*t2 - 11*Power(t1,4)*t2 + 
            63*Power(t2,2) + 202*t1*Power(t2,2) - 
            112*Power(t1,2)*Power(t2,2) - 25*Power(t1,3)*Power(t2,2) + 
            13*Power(t1,4)*Power(t2,2) - 130*Power(t2,3) - 
            47*t1*Power(t2,3) + 69*Power(t1,2)*Power(t2,3) - 
            28*Power(t1,3)*Power(t2,3) - 2*Power(t1,4)*Power(t2,3) + 
            71*Power(t2,4) - 25*t1*Power(t2,4) + 
            17*Power(t1,2)*Power(t2,4) + 4*Power(t1,3)*Power(t2,4) - 
            8*Power(t2,5) - 2*t1*Power(t2,5) - 2*Power(t1,2)*Power(t2,5) + 
            Power(s1,5)*(4 + s2 - 4*Power(s2,2) - 3*t1 + 4*s2*t1 + 4*t2 + 
               12*s2*t2 - 12*t1*t2) + 
            Power(s2,4)*(26 - 33*t2 + 19*Power(t2,2) - 2*Power(t2,3)) + 
            Power(s2,3)*(109 - 112*t2 + 28*Power(t2,2) + 26*Power(t2,3) - 
               4*Power(t2,4) + 
               t1*(-95 + 110*t2 - 70*Power(t2,2) + 8*Power(t2,3))) + 
            Power(s2,2)*(131 - 203*Power(t2,2) + 117*Power(t2,3) + 
               10*Power(t2,4) - 2*Power(t2,5) - 
               3*Power(t1,2)*(-43 + 44*t2 - 32*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-326 + 325*t2 - 81*Power(t2,2) - 80*Power(t2,3) + 
                  12*Power(t2,4))) + 
            s2*(28 + 128*t2 - 233*Power(t2,2) + 72*Power(t2,3) + 
               18*Power(t2,4) + 3*Power(t2,5) + 
               Power(t1,3)*(-77 + 66*t2 - 58*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(325 - 314*t2 + 78*Power(t2,2) + 
                  82*Power(t2,3) - 12*Power(t2,4)) + 
               t1*(-274 + 60*t2 + 315*Power(t2,2) - 186*Power(t2,3) - 
                  27*Power(t2,4) + 4*Power(t2,5))) - 
            Power(s1,4)*(30 + 16*Power(s2,3) - 5*Power(t1,3) + 
               Power(s2,2)*(14 - 37*t1 - 9*t2) - 6*t2 + 4*Power(t2,2) + 
               Power(t1,2)*(12 + t2) - 4*t1*(5 + 6*t2 + 2*Power(t2,2)) + 
               2*s2*(9 + 13*Power(t1,2) + 9*t2 + 4*Power(t2,2) + 
                  t1*(-13 + 4*t2))) + 
            Power(s1,3)*(24 - 6*Power(s2,4) + 5*Power(t1,4) + 66*t2 - 
               42*Power(t2,2) + 8*Power(t2,3) - 
               4*Power(t1,3)*(2 + 5*t2) + 
               Power(s2,3)*(1 + 13*t1 + 35*t2) + 
               Power(t1,2)*(61 + 14*t2 + 5*Power(t2,2)) + 
               t1*(-137 + 75*t2 - 76*Power(t2,2) + 8*Power(t2,3)) - 
               Power(s2,2)*(-93 + 3*Power(t1,2) + 18*t2 + Power(t2,2) + 
                  10*t1*(1 + 9*t2)) + 
               s2*(146 - 9*Power(t1,3) - 89*t2 + 69*Power(t2,2) - 
                  8*Power(t2,3) + Power(t1,2)*(17 + 75*t2) - 
                  2*t1*(77 - 2*t2 + 2*Power(t2,2)))) + 
            Power(s1,2)*(79 - 4*Power(t1,5) + Power(t1,4)*(35 - 6*t2) - 
               198*t2 + 37*Power(t2,2) + 42*Power(t2,3) - 
               10*Power(t2,4) + Power(s2,4)*(12 - 4*t1 + 10*t2) + 
               Power(t1,3)*(16 - 72*t2 + 29*Power(t2,2)) + 
               Power(t1,2)*(-131 + 37*t2 + 25*Power(t2,2) - 
                  9*Power(t2,3)) + 
               t1*(-58 + 391*t2 - 283*Power(t2,2) + 90*Power(t2,3) - 
                  12*Power(t2,4)) + 
               Power(s2,3)*(10 + 16*Power(t1,2) + 39*t2 - 
                  26*Power(t2,2) - t1*(71 + 24*t2)) - 
               Power(s2,2)*(114 + 24*Power(t1,3) + 39*t2 - 
                  88*Power(t2,2) + 11*Power(t2,3) - 
                  3*Power(t1,2)*(47 + 4*t2) + 
                  t1*(4 + 150*t2 - 81*Power(t2,2))) + 
               s2*(51 + 16*Power(t1,4) - 396*t2 + 298*Power(t2,2) - 
                  85*Power(t2,3) + 12*Power(t2,4) + 
                  Power(t1,3)*(-117 + 8*t2) + 
                  Power(t1,2)*(-22 + 183*t2 - 84*Power(t2,2)) + 
                  t1*(245 + 2*t2 - 113*Power(t2,2) + 20*Power(t2,3)))) + 
            s1*(-4*Power(s2,5) + Power(t1,5)*(2 + 4*t2) + 
               Power(t1,4)*(7 - 53*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(65 - 77*t2 + 124*Power(t2,2) - 
                  18*Power(t2,3)) + 
               Power(t1,2)*(-250 + 450*t2 - 207*Power(t2,2) - 
                  44*Power(t2,3) + 7*Power(t2,4)) + 
               2*(-2 - 71*t2 + 152*Power(t2,2) - 72*Power(t2,3) - 
                  Power(t2,4) + 2*Power(t2,5)) + 
               t1*(180 - 188*t2 - 199*Power(t2,2) + 213*Power(t2,3) - 
                  33*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s2,4)*(19 - 39*t2 - 2*Power(t2,2) + 
                  2*t1*(9 + 2*t2)) + 
               Power(s2,3)*(-40 + 33*t2 - 82*Power(t2,2) + 
                  11*Power(t2,3) - 16*Power(t1,2)*(2 + t2) + 
                  t1*(-64 + 170*t2 + 3*Power(t2,2))) + 
               Power(s2,2)*(-286 + 512*t2 - 211*Power(t2,2) - 
                  66*Power(t2,3) + 9*Power(t2,4) + 
                  4*Power(t1,3)*(7 + 6*t2) + 
                  3*Power(t1,2)*(26 - 92*t2 + Power(t2,2)) + 
                  t1*(145 - 143*t2 + 288*Power(t2,2) - 40*Power(t2,3))) - 
               s2*(192 - 226*t2 - 170*Power(t2,2) + 209*Power(t2,3) - 
                  30*Power(t2,4) + 4*Power(t2,5) + 
                  4*Power(t1,4)*(3 + 4*t2) + 
                  Power(t1,3)*(40 - 198*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*
                   (170 - 187*t2 + 330*Power(t2,2) - 47*Power(t2,3)) + 
                  2*t1*(-268 + 481*t2 - 209*Power(t2,2) - 55*Power(t2,3) + 
                     8*Power(t2,4)))))))/
     ((-1 + s1)*(-1 + s - s2 + t1)*(s - s2 + t1)*Power(-s + s1 - t2,2)*
       (1 - s + s1 - t2)*Power(s1 - s2 + t1 - t2,2)*(-1 + s2 - t1 + t2)) + 
    (8*(24 + 88*s2 + 16*Power(s2,2) - 8*Power(s2,3) + 
         4*Power(s1,4)*(s2 - t1) - 88*t1 - 32*s2*t1 + 24*Power(s2,2)*t1 + 
         16*Power(t1,2) - 24*s2*Power(t1,2) + 8*Power(t1,3) + 
         2*Power(s,5)*(t1 - t2) + 30*t2 - 100*s2*t2 - 37*Power(s2,2)*t2 + 
         6*Power(s2,3)*t2 + 90*t1*t2 + 70*s2*t1*t2 - 
         13*Power(s2,2)*t1*t2 + 2*Power(s2,3)*t1*t2 - 33*Power(t1,2)*t2 + 
         8*s2*Power(t1,2)*t2 - 6*Power(s2,2)*Power(t1,2)*t2 - 
         Power(t1,3)*t2 + 6*s2*Power(t1,3)*t2 - 2*Power(t1,4)*t2 - 
         46*Power(t2,2) + 9*s2*Power(t2,2) + 19*Power(s2,2)*Power(t2,2) - 
         3*Power(s2,3)*Power(t2,2) - 2*Power(s2,4)*Power(t2,2) - 
         7*t1*Power(t2,2) - 35*s2*t1*Power(t2,2) + 
         4*Power(s2,2)*t1*Power(t2,2) + 6*Power(s2,3)*t1*Power(t2,2) + 
         16*Power(t1,2)*Power(t2,2) + s2*Power(t1,2)*Power(t2,2) - 
         6*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         2*Power(t1,3)*Power(t2,2) + 2*s2*Power(t1,3)*Power(t2,2) - 
         4*Power(t2,3) + 5*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) + 
         3*t1*Power(t2,3) - s2*t1*Power(t2,3) - 4*Power(t2,4) - 
         2*Power(s,4)*(-3 + 6*t1 - 3*Power(t1,2) + 
            s2*(-2 + 3*t1 - 4*t2) + s1*(4 + s2 + t1 - 2*t2) - 4*t2 + 
            2*t1*t2 + Power(t2,2)) + 
         Power(s1,3)*(-4 - 2*Power(s2,3) + 22*t1 - 9*Power(t1,2) + 
            2*Power(t1,3) + Power(s2,2)*(-9 + 6*t1) + 4*t2 + 12*t1*t2 - 
            2*s2*(11 - 9*t1 + 3*Power(t1,2) + 6*t2)) + 
         Power(s1,2)*(-2*Power(s2,4) - 2*Power(t1,4) - 
            Power(t1,3)*(3 + 2*t2) + Power(s2,3)*(-5 + 8*t1 + 4*t2) + 
            Power(s2,2)*(-26 - 12*Power(t1,2) + t1*(7 - 10*t2) + 
               17*t2) + Power(t1,2)*(-11 + 18*t2) - 
            2*(-11 + 5*t2 + 6*Power(t2,2)) - 
            t1*(33 + 13*t2 + 12*Power(t2,2)) + 
            s2*(31 + 8*Power(t1,3) + t1*(37 - 35*t2) + 15*t2 + 
               12*Power(t2,2) + Power(t1,2)*(1 + 8*t2))) + 
         Power(s,3)*(-28 + 26*t1 - 25*Power(t1,2) + 6*Power(t1,3) + 
            2*Power(s2,2)*(-4 + 3*t1 - 6*t2) + 
            2*Power(s1,2)*(8 + 2*s2 - t1 - t2) + 6*t2 + 9*Power(t2,2) - 
            6*t1*Power(t2,2) + 
            s2*(-16 + 33*t1 - 12*Power(t1,2) - 15*t2 + 12*t1*t2 + 
               8*Power(t2,2)) + 
            s1*(14 + 6*Power(s2,2) + 3*t1 - 4*Power(t1,2) - 25*t2 + 
               10*t1*t2 + 2*Power(t2,2) - 2*s2*(-6 + t1 + 8*t2))) + 
         s1*(Power(t1,3)*(9 - 11*t2) + 4*Power(s2,4)*(2 + t2) + 
            2*Power(t1,4)*(5 + t2) - 
            Power(t1,2)*(87 + 5*t2 + Power(t2,2)) + 
            2*t1*(3 + 48*t2 - 6*Power(t2,2) + 2*Power(t2,3)) + 
            2*(-11 + 5*Power(t2,2) + 6*Power(t2,3)) - 
            2*Power(s2,3)*(7 - 12*t2 + Power(t2,2) + t1*(17 + 7*t2)) - 
            2*s2*(-2 + 48*t2 - Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,3)*(19 + 5*t2) + t1*(-85 + t2 - Power(t2,2)) + 
               Power(t1,2)*(16 - 23*t2 + Power(t2,2))) + 
            Power(s2,2)*(-83 + 7*t2 - Power(t2,2) + 
               18*Power(t1,2)*(3 + t2) + t1*(37 - 59*t2 + 4*Power(t2,2)))\
) + Power(s,2)*(56 - 2*Power(s1,3)*(2 + s2 - t1) - 47*t1 + 
            11*Power(t1,2) - 15*Power(t1,3) + 2*Power(t1,4) - 45*t2 + 
            62*t1*t2 - 23*Power(t1,2)*t2 + 4*Power(t1,3)*t2 - 
            13*Power(t2,2) + 13*t1*Power(t2,2) - 
            6*Power(t1,2)*Power(t2,2) + Power(t2,3) + 
            Power(s2,3)*(4 - 2*t1 + 8*t2) + 
            Power(s1,2)*(-40 - 10*Power(s2,2) - 6*Power(t1,2) + 
               t1*(26 - 6*t2) + 9*t2 + s2*(-33 + 16*t1 + 8*t2)) + 
            Power(s2,2)*(4 + 6*Power(t1,2) + 6*t2 - 12*Power(t2,2) - 
               t1*(23 + 12*t2)) + 
            s2*(53 + 34*Power(t1,2) - 6*Power(t1,3) - 39*t2 - 
               21*Power(t2,2) + t1*(-15 + 17*t2 + 18*Power(t2,2))) + 
            s1*(13 - 6*Power(s2,3) - 2*Power(t1,3) + 21*t2 + 
               2*Power(t2,2) + 5*Power(t1,2)*(7 + 2*t2) + 
               Power(s2,2)*(3 + 10*t1 + 24*t2) + 
               t1*(11 - 55*t2 + 4*Power(t2,2)) - 
               2*s2*(19 + Power(t1,2) - 34*t2 + 3*Power(t2,2) + 
                  t1*(19 + 17*t2)))) + 
         s*(-58 - 4*Power(s1,4) + 122*t1 - 9*Power(t1,2) - Power(t1,3) - 
            2*Power(t1,4) - 6*t2 - 2*Power(s2,4)*t2 - 78*t1*t2 + 
            27*Power(t1,2)*t2 - 17*Power(t1,3)*t2 + 2*Power(t1,4)*t2 - 
            13*Power(t2,2) + 39*t1*Power(t2,2) + 
            2*Power(t1,2)*Power(t2,2) - 2*Power(t1,3)*Power(t2,2) - 
            17*Power(t2,3) + t1*Power(t2,3) + 
            Power(s1,3)*(28 + 4*Power(s2,2) + s2*(13 - 8*t1) - 13*t1 + 
               4*Power(t1,2) + 12*t2) + 
            Power(s2,3)*(6 + t2 + 8*Power(t2,2) + t1*(2 + 4*t2)) + 
            Power(s1,2)*(-45 + 8*Power(s2,3) - 6*Power(t1,3) + 
               Power(t1,2)*(7 - 6*t2) - 41*t2 - 12*Power(t2,2) - 
               2*Power(s2,2)*(-11 + 11*t1 + 5*t2) + t1*(-35 + 27*t2) + 
               s2*(52 - 29*t1 + 20*Power(t1,2) - 26*t2 + 16*t1*t2)) - 
            Power(s2,2)*(13 + 6*Power(t1,2) - 23*t2 - 15*Power(t2,2) + 
               t1*(13 + 19*t2 + 18*Power(t2,2))) + 
            s2*(Power(t1,3)*(6 - 4*t2) + 
               t1*(22 - 50*t2 - 17*Power(t2,2)) + 
               Power(t1,2)*(8 + 35*t2 + 12*Power(t2,2)) - 
               2*(66 - 43*t2 + 9*Power(t2,2) + Power(t2,3))) + 
            s1*(2*Power(s2,4) + Power(t1,3)*(34 + 6*t2) - 
               Power(s2,3)*(15 + 6*t1 + 16*t2) + 
               t1*(-80 - 32*t2 + Power(t2,2)) + 
               Power(t1,2)*(24 - 41*t2 + 2*Power(t2,2)) + 
               2*(5 + 56*t2 + 15*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(46 + 6*Power(t1,2) - 67*t2 + 6*Power(t2,2) + 
                  t1*(64 + 38*t2)) - 
               s2*(-76 + 2*Power(t1,3) + Power(t2,2) + 
                  Power(t1,2)*(83 + 28*t2) + 
                  2*t1*(35 - 54*t2 + 4*Power(t2,2))))))*
       B1(1 - s2 + t1 - t2,1 - s + s2 - t1,1 - s + s1 - t2))/
     ((-1 + s1)*Power(-s + s1 - t2,2)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)) + 
    (16*(32*Power(s2,3) + 16*Power(s2,4) - 32*Power(s2,5) - 
         12*Power(s2,6) - 96*Power(s2,2)*t1 - 64*Power(s2,3)*t1 + 
         160*Power(s2,4)*t1 + 72*Power(s2,5)*t1 + 96*s2*Power(t1,2) + 
         96*Power(s2,2)*Power(t1,2) - 320*Power(s2,3)*Power(t1,2) - 
         180*Power(s2,4)*Power(t1,2) - 32*Power(t1,3) - 
         64*s2*Power(t1,3) + 320*Power(s2,2)*Power(t1,3) + 
         240*Power(s2,3)*Power(t1,3) + 16*Power(t1,4) - 
         160*s2*Power(t1,4) - 180*Power(s2,2)*Power(t1,4) + 
         32*Power(t1,5) + 72*s2*Power(t1,5) - 12*Power(t1,6) - 
         2*Power(s1,5)*(1 + t1)*
          (s2 + Power(s2,2) - 2*s2*t1 + (-1 + t1)*t1) + 
         52*Power(s2,2)*t2 - 40*Power(s2,3)*t2 - 129*Power(s2,4)*t2 - 
         21*Power(s2,5)*t2 + 4*Power(s2,6)*t2 - 104*s2*t1*t2 + 
         132*Power(s2,2)*t1*t2 + 536*Power(s2,3)*t1*t2 + 
         118*Power(s2,4)*t1*t2 - 17*Power(s2,5)*t1*t2 + 
         52*Power(t1,2)*t2 - 144*s2*Power(t1,2)*t2 - 
         834*Power(s2,2)*Power(t1,2)*t2 - 
         262*Power(s2,3)*Power(t1,2)*t2 + 25*Power(s2,4)*Power(t1,2)*t2 + 
         52*Power(t1,3)*t2 + 576*s2*Power(t1,3)*t2 + 
         288*Power(s2,2)*Power(t1,3)*t2 - 10*Power(s2,3)*Power(t1,3)*t2 - 
         149*Power(t1,4)*t2 - 157*s2*Power(t1,4)*t2 - 
         10*Power(s2,2)*Power(t1,4)*t2 + 34*Power(t1,5)*t2 + 
         11*s2*Power(t1,5)*t2 - 3*Power(t1,6)*t2 + 48*s2*Power(t2,2) - 
         62*Power(s2,2)*Power(t2,2) - 131*Power(s2,3)*Power(t2,2) + 
         5*Power(s2,5)*Power(t2,2) - 3*Power(s2,6)*Power(t2,2) - 
         48*t1*Power(t2,2) + 124*s2*t1*Power(t2,2) + 
         383*Power(s2,2)*t1*Power(t2,2) - 3*Power(s2,3)*t1*Power(t2,2) - 
         17*Power(s2,4)*t1*Power(t2,2) + 14*Power(s2,5)*t1*Power(t2,2) - 
         62*Power(t1,2)*Power(t2,2) - 373*s2*Power(t1,2)*Power(t2,2) + 
         9*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         18*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         25*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         121*Power(t1,3)*Power(t2,2) - 9*s2*Power(t1,3)*Power(t2,2) - 
         2*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         20*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         3*Power(t1,4)*Power(t2,2) - 7*s2*Power(t1,4)*Power(t2,2) - 
         5*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         3*Power(t1,5)*Power(t2,2) - 2*s2*Power(t1,5)*Power(t2,2) + 
         Power(t1,6)*Power(t2,2) + 16*Power(t2,3) - 40*s2*Power(t2,3) - 
         48*Power(s2,2)*Power(t2,3) + 23*Power(s2,3)*Power(t2,3) + 
         6*Power(s2,4)*Power(t2,3) - 3*Power(s2,5)*Power(t2,3) + 
         40*t1*Power(t2,3) + 96*s2*t1*Power(t2,3) - 
         57*Power(s2,2)*t1*Power(t2,3) - 13*Power(s2,3)*t1*Power(t2,3) + 
         10*Power(s2,4)*t1*Power(t2,3) - 48*Power(t1,2)*Power(t2,3) + 
         45*s2*Power(t1,2)*Power(t2,3) + 
         3*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         10*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         11*Power(t1,3)*Power(t2,3) + 9*s2*Power(t1,3)*Power(t2,3) - 
         5*Power(t1,4)*Power(t2,3) + 5*s2*Power(t1,4)*Power(t2,3) - 
         2*Power(t1,5)*Power(t2,3) - 16*Power(t2,4) - 20*s2*Power(t2,4) - 
         4*Power(s2,2)*Power(t2,4) - Power(s2,3)*Power(t2,4) + 
         20*t1*Power(t2,4) + 12*s2*t1*Power(t2,4) + 
         7*Power(s2,2)*t1*Power(t2,4) - Power(s2,3)*t1*Power(t2,4) - 
         8*Power(t1,2)*Power(t2,4) - 11*s2*Power(t1,2)*Power(t2,4) + 
         3*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         5*Power(t1,3)*Power(t2,4) - 3*s2*Power(t1,3)*Power(t2,4) + 
         Power(t1,4)*Power(t2,4) + 
         Power(s,6)*(3*Power(s1,2) - Power(s2,2) + 
            s1*(4*s2 + 5*(-2 + t1 - t2)) + s2*(-2 - t1 + t2) + 
            2*(3 + Power(t1,2) + 2*t2 + Power(t2,2) - 2*t1*(1 + t2))) + 
         Power(s,5)*(-24 - 4*Power(s1,3) + 3*Power(s2,3) + 53*t1 - 
            17*Power(t1,2) + 10*Power(t1,3) + 
            Power(s2,2)*(11 + 4*t1 - 11*t2) - 17*t2 + 6*t1*t2 - 
            19*Power(t1,2)*t2 + 11*Power(t2,2) + 8*t1*Power(t2,2) + 
            Power(t2,3) + Power(s1,2)*(-3 - 28*s2 + 12*t1 + 9*t2) - 
            s1*(-41 + 7*Power(s2,2) + 56*t1 - 26*Power(t1,2) + 
               s2*(-40 + 19*t1 - 41*t2) + 8*t2 + 20*t1*t2 + 
               6*Power(t2,2)) - 
            s2*(29 + 17*Power(t1,2) + 14*t2 + 13*Power(t2,2) - 
               6*t1*(1 + 5*t2))) + 
         Power(s1,4)*(8 + 4*Power(s2,4) + Power(s2,3)*(7 - 9*t1) - 
            3*Power(t1,4) + Power(t1,2)*(4 - 8*t2) - 8*t2 + 
            4*t1*(-3 + 2*t2) + Power(t1,3)*(5 + 8*t2) + 
            Power(s2,2)*(8 + 3*Power(t1,2) + t1*(-9 + 8*t2)) + 
            s2*(12 + 5*Power(t1,3) - 8*t2 + 4*t1*(-3 + 2*t2) - 
               Power(t1,2)*(3 + 16*t2))) + 
         Power(s1,3)*(5*Power(s2,5) - 5*Power(t1,5) + 
            Power(s2,4)*(31 - 25*t1 - 12*t2) + Power(t1,4)*(41 + 8*t2) + 
            8*(-2 - t2 + 3*Power(t2,2)) - 
            4*Power(t1,3)*(5 + 11*t2 + 3*Power(t2,2)) - 
            4*t1*(-8 + 2*t2 + 9*Power(t2,2)) + 
            Power(t1,2)*(-22 + 44*t2 + 24*Power(t2,2)) + 
            2*Power(s2,3)*(7 + 25*Power(t1,2) + 2*t2 + 
               t1*(-67 + 14*t2)) - 
            2*Power(s2,2)*(11 + 25*Power(t1,3) + 
               6*Power(t1,2)*(-18 + t2) - 14*t2 - 6*Power(t2,2) + 
               t1*(24 + 26*t2 + 6*Power(t2,2))) + 
            s2*(25*Power(t1,4) - 2*Power(t1,3)*(77 + 6*t2) + 
               4*(-8 + 2*t2 + 9*Power(t2,2)) - 
               4*t1*(-11 + 18*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(54 + 92*t2 + 24*Power(t2,2)))) - 
         Power(s1,2)*(3*Power(s2,6) + 4*Power(t1,6) - 
            Power(t1,5)*(61 + 8*t2) + Power(s2,5)*(36 - 19*t1 + 13*t2) + 
            24*t2*(-2 + t2 + Power(t2,2)) + 
            Power(t1,4)*(62 + 119*t2 + 6*Power(t2,2)) - 
            8*t1*(-6 - 3*t2 + 9*Power(t2,2) + 5*Power(t2,3)) - 
            Power(t1,3)*(49 + 93*t2 + 78*Power(t2,2) + 8*Power(t2,3)) + 
            2*Power(t1,2)*(-11 + 18*t2 + 54*Power(t2,2) + 
               12*Power(t2,3)) + 
            Power(s2,4)*(41 + 50*Power(t1,2) + 88*t2 - 12*Power(t2,2) - 
               5*t1*(41 + 12*t2)) + 
            Power(s2,3)*(47 - 70*Power(t1,3) + 69*t2 + 30*Power(t2,2) + 
               10*Power(t1,2)*(46 + 11*t2) + 
               t1*(-185 - 383*t2 + 30*Power(t2,2))) + 
            Power(s2,2)*(-22 + 55*Power(t1,4) + 36*t2 + 
               84*Power(t2,2) + 16*Power(t2,3) - 
               10*Power(t1,3)*(51 + 10*t2) + 
               Power(t1,2)*(309 + 621*t2 - 18*Power(t2,2)) - 
               t1*(143 + 231*t2 + 138*Power(t2,2) + 8*Power(t2,3))) + 
            s2*(-23*Power(t1,5) + 5*Power(t1,4)*(56 + 9*t2) - 
               Power(t1,3)*(227 + 445*t2 + 6*Power(t2,2)) + 
               8*(-6 - 3*t2 + 9*Power(t2,2) + 5*Power(t2,3)) - 
               4*t1*(-11 + 18*t2 + 48*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,2)*(145 + 255*t2 + 186*Power(t2,2) + 
                  16*Power(t2,3)))) + 
         s1*(6*Power(s2,6)*(2 + t2) + Power(t1,6)*(19 + 3*t2) + 
            8*Power(t2,2)*(-6 + 5*t2 + Power(t2,2)) - 
            Power(t1,5)*(34 + 80*t2 + Power(t2,2)) + 
            Power(t1,4)*(85 + 91*t2 + 83*Power(t2,2)) - 
            2*t1*t2*(-48 + 24*t2 + 36*Power(t2,2) + 7*Power(t2,3)) - 
            2*Power(t1,3)*(2 + 93*t2 + 31*Power(t2,2) + 
               22*Power(t2,3) + Power(t2,4)) + 
            2*Power(t1,2)*(-26 + 20*t2 + 53*Power(t2,2) + 
               34*Power(t2,3) + 4*Power(t2,4)) + 
            Power(s2,5)*(21 + 47*t2 + 11*Power(t2,2) - 
               t1*(79 + 33*t2)) + 
            Power(s2,4)*(65 + 73*t2 + 51*Power(t2,2) - 4*Power(t2,3) + 
               5*Power(t1,2)*(43 + 15*t2) - 
               t1*(118 + 268*t2 + 45*Power(t2,2))) + 
            Power(s2,3)*(-8 + 194*t2 + 32*Power(t2,2) + 
               20*Power(t2,3) - 10*Power(t1,3)*(31 + 9*t2) + 
               Power(t1,2)*(262 + 602*t2 + 70*Power(t2,2)) + 
               2*t1*(-140 - 155*t2 - 118*Power(t2,2) + 6*Power(t2,3))) + 
            2*Power(s2,2)*(-26 + 20*t2 + 53*Power(t2,2) + 
               26*Power(t2,3) + 3*Power(t2,4) + 
               5*Power(t1,4)*(25 + 6*t2) - 
               Power(t1,3)*(144 + 334*t2 + 25*Power(t2,2)) + 
               3*Power(t1,2)*
                (75 + 82*t2 + 67*Power(t2,2) - 2*Power(t2,3)) - 
               t1*(-6 + 287*t2 + 63*Power(t2,2) + 42*Power(t2,3) + 
                  Power(t2,4))) + 
            s2*(-(Power(t1,5)*(107 + 21*t2)) + 
               Power(t1,4)*(157 + 367*t2 + 15*Power(t2,2)) + 
               2*Power(t1,2)*t2*
                (283 + 78*t2 + 54*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(-320 - 346*t2 - 300*Power(t2,2) + 
                  4*Power(t2,3)) + 
               2*t2*(-48 + 24*t2 + 36*Power(t2,2) + 7*Power(t2,3)) - 
               2*t1*(-52 + 40*t2 + 106*Power(t2,2) + 60*Power(t2,3) + 
                  7*Power(t2,4)))) + 
         Power(s,4)*(30 - Power(s1,4) - 3*Power(s2,4) - 145*t1 + 
            115*Power(t1,2) - 28*Power(t1,3) + 18*Power(t1,4) + 25*t2 + 
            43*t1*t2 - 14*Power(t1,2)*t2 - 28*Power(t1,3)*t2 - 
            68*Power(t2,2) + 32*t1*Power(t2,2) + 
            Power(t1,2)*Power(t2,2) + 10*Power(t2,3) + 
            10*t1*Power(t2,3) - Power(t2,4) + 
            Power(s1,3)*(18 + 37*s2 - 29*t1 + 4*t2) + 
            Power(s2,3)*(-23 - 9*t1 + 30*t2) + 
            Power(s2,2)*(34 + 45*Power(t1,2) + t1*(18 - 88*t2) + 
               24*t2 + 23*Power(t2,2)) + 
            Power(s1,2)*(-31 + 61*Power(s2,2) - 2*Power(t1,2) + 
               s2*(16 - 59*t1 - 85*t2) - 34*t2 - 6*Power(t2,2) + 
               2*t1*(9 + 34*t2)) - 
            s2*(-115 + 51*Power(t1,3) + 4*t2 + 33*Power(t2,2) + 
               11*Power(t2,3) - Power(t1,2)*(33 + 86*t2) + 
               t1*(149 + 10*t2 + 24*Power(t2,2))) + 
            s1*(-55 - 4*Power(s2,3) + 44*Power(t1,3) + 
               Power(s2,2)*(-71 + 52*t1 - 84*t2) + 
               Power(t1,2)*(-118 + t2) + 99*t2 + 6*Power(t2,2) + 
               4*Power(t2,3) + t1*(114 - 58*t2 - 49*Power(t2,2)) + 
               s2*(-123 - 92*Power(t1,2) + 25*t2 + 59*Power(t2,2) + 
                  t1*(189 + 83*t2)))) + 
         Power(s,3)*(-12 + 2*Power(s1,5) + Power(s2,5) + 136*t1 - 
            251*Power(t1,2) + 69*Power(t1,3) - 24*Power(t1,4) + 
            14*Power(t1,5) + Power(s2,4)*(25 + 10*t1 - 34*t2) + 
            Power(s1,4)*(3 - 9*s2 + 10*t1 - 8*t2) - 16*t2 - 82*t1*t2 + 
            275*Power(t1,2)*t2 - 33*Power(t1,3)*t2 - 10*Power(t1,4)*t2 + 
            93*Power(t2,2) - 195*t1*Power(t2,2) + 
            21*Power(t1,2)*Power(t2,2) - 24*Power(t1,3)*Power(t2,2) - 
            29*Power(t2,3) + 33*t1*Power(t2,3) + 
            22*Power(t1,2)*Power(t2,3) + 3*Power(t2,4) - 
            2*t1*Power(t2,4) - 
            Power(s2,3)*(-30 + 50*Power(t1,2) + t1*(51 - 112*t2) + 
               24*t2 + 8*Power(t2,2)) - 
            Power(s1,3)*(22 + 80*Power(s2,2) + 44*Power(t1,2) + 
               s2*(105 - 124*t1 - 24*t2) + 12*t2 - 12*Power(t2,2) + 
               t1*(-87 + 28*t2)) + 
            Power(s2,2)*(-171 + 80*Power(t1,3) + 
               Power(t1,2)*(3 - 132*t2) + 149*t2 + 40*Power(t2,2) + 
               30*Power(t2,3) + t1*(9 + 15*t2 - 8*Power(t2,2))) + 
            s2*(-124 - 55*Power(t1,4) + 42*t2 + 210*Power(t2,2) - 
               28*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,3)*(47 + 64*t2) + 
               2*Power(t1,2)*(-54 + 21*t2 + 20*Power(t2,2)) - 
               t1*(-422 + 424*t2 + 61*Power(t2,2) + 52*Power(t2,3))) + 
            Power(s1,2)*(57 - 50*Power(s2,3) - 36*Power(t1,3) + 31*t2 + 
               18*Power(t2,2) - 8*Power(t2,3) + 
               11*Power(t1,2)*(11 + 10*t2) + 
               2*Power(s2,2)*(13 + 32*t1 + 95*t2) + 
               t1*(-161 - 173*t2 + 24*Power(t2,2)) + 
               s2*(140 + 22*Power(t1,2) + 214*t2 - 18*Power(t2,2) - 
                  3*t1*(49 + 100*t2))) + 
            s1*(16*Power(s2,4) + 30*Power(t1,4) + 
               Power(s2,3)*(51 - 78*t1 + 58*t2) + 
               Power(t1,3)*(-107 + 60*t2) - 
               2*Power(t1,2)*(-25 + 87*t2 + 44*Power(t2,2)) + 
               t1*(-74 + 372*t2 + 53*Power(t2,2) - 4*Power(t2,3)) + 
               2*(14 - 75*t2 + 10*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(92 + 138*Power(t1,2) - 98*t2 - 
                  140*Power(t2,2) - t1*(209 + 56*t2)) + 
               s2*(102 - 106*Power(t1,3) + Power(t1,2)*(265 - 62*t2) - 
                  366*t2 - 81*Power(t2,2) + 
                  2*t1*(-71 + 136*t2 + 114*Power(t2,2))))) + 
         Power(s,2)*(-40*t1 + 190*Power(t1,2) - 143*Power(t1,3) - 
            25*Power(t1,4) - 12*Power(t1,5) + 4*Power(t1,6) + 
            Power(s1,5)*(-8 - 4*s2 + 2*t1) + 4*t2 + 28*t1*t2 - 
            332*Power(t1,2)*t2 + 339*Power(t1,3)*t2 - 
            23*Power(t1,4)*t2 + 8*Power(t1,5)*t2 - 38*Power(t2,2) + 
            211*t1*Power(t2,2) - 111*Power(t1,2)*Power(t2,2) - 
            7*Power(t1,3)*Power(t2,2) - 28*Power(t1,4)*Power(t2,2) + 
            24*Power(t2,3) - 117*t1*Power(t2,3) + 
            31*Power(t1,2)*Power(t2,3) + 16*Power(t1,3)*Power(t2,3) + 
            4*Power(t2,4) + 11*t1*Power(t2,4) + 
            Power(s2,5)*(-15 - 4*t1 + 17*t2) + 
            2*Power(s2,4)*(-40 + 10*Power(t1,2) + 8*t2 - 
               7*Power(t2,2) - 6*t1*(-4 + 5*t2)) + 
            Power(s1,4)*(20 + 25*Power(s2,2) - 21*t1 + 20*Power(t1,2) + 
               24*t2 - 8*t1*t2 + s2*(25 - 45*t1 + 16*t2)) + 
            Power(s1,3)*(2 + 70*Power(s2,3) - 22*Power(t1,3) + 
               Power(s2,2)*(151 - 162*t1 - 72*t2) + 
               Power(t1,2)*(113 - 60*t2) - 64*t2 - 24*Power(t2,2) + 
               2*s2*(1 + 57*Power(t1,2) + 66*t1*(-2 + t2) - 22*t2 - 
                  12*Power(t2,2)) + 4*t1*(2 + 7*t2 + 3*Power(t2,2))) - 
            Power(s2,3)*(-73 + 40*Power(t1,3) + 
               Power(t1,2)*(42 - 70*t2) + 207*t2 + 20*Power(t2,2) + 
               34*Power(t2,3) - 5*t1*(53 - 5*t2 + 14*Power(t2,2))) + 
            Power(s2,2)*(166 + 40*Power(t1,4) - 232*t2 - 
               168*Power(t2,2) + 32*Power(t2,3) - 3*Power(t2,4) - 
               4*Power(t1,3)*(3 + 5*t2) - 
               21*Power(t1,2)*(15 + t2 + 6*Power(t2,2)) + 
               t1*(-289 + 753*t2 + 33*Power(t2,2) + 84*Power(t2,3))) + 
            s2*(40 - 20*Power(t1,5) + Power(t1,4)*(33 - 15*t2) - 
               16*t2 - 221*Power(t2,2) + 113*Power(t2,3) - 
               7*Power(t2,4) + 
               Power(t1,3)*(155 + 53*t2 + 98*Power(t2,2)) - 
               Power(t1,2)*(-359 + 885*t2 + 6*Power(t2,2) + 
                  66*Power(t2,3)) + 
               t1*(-356 + 564*t2 + 279*Power(t2,2) - 63*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s1,2)*(-26 + 11*Power(s2,4) - 37*Power(t1,4) + 
               2*Power(s2,3)*(-50 + 2*t1 - 87*t2) + 12*t2 + 
               72*Power(t2,2) + 8*Power(t2,3) + 
               5*Power(t1,3)*(41 + 12*t2) + 
               Power(t1,2)*(-191 - 267*t2 + 60*Power(t2,2)) + 
               t1*(163 - 85*t2 + 18*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s2,2)*(-152 - 78*Power(t1,2) - 342*t2 + 
                  66*Power(t2,2) + t1*(405 + 408*t2)) + 
               s2*(-161 + 100*Power(t1,3) + 61*t2 + 6*Power(t2,2) + 
                  16*Power(t2,3) - 6*Power(t1,2)*(85 + 49*t2) - 
                  7*t1*(-49 - 87*t2 + 18*Power(t2,2)))) + 
            s1*(-4 - 12*Power(s2,5) + 7*Power(t1,5) + 64*t2 - 
               38*Power(t2,2) - 32*Power(t2,3) + 
               Power(s2,4)*(7 + 55*t1 + 3*t2) + 
               Power(t1,4)*(-23 + 65*t2) - 
               2*Power(t1,3)*(27 + 127*t2 + 27*Power(t2,2)) + 
               Power(t1,2)*(78 + 350*t2 + 123*Power(t2,2) - 
                  20*Power(t2,3)) + 
               2*t1*(10 - 191*t2 + 97*Power(t2,2) - 18*Power(t2,3) + 
                  Power(t2,4)) - 
               2*Power(s2,3)*
                (50*Power(t1,2) + t1*(-1 + 37*t2) - t2*(88 + 69*t2)) + 
               Power(s2,2)*(2 + 90*Power(t1,3) + 368*t2 + 
                  159*Power(t2,2) - 16*Power(t2,3) + 
                  12*Power(t1,2)*(-4 + 17*t2) - 
                  6*t1*(9 + 101*t2 + 55*Power(t2,2))) - 
               2*s2*(16 + 20*Power(t1,4) - 195*t2 + 88*Power(t2,2) - 
                  10*Power(t2,3) + 2*Power(t2,4) + 
                  Power(t1,3)*(-31 + 99*t2) - 
                  3*Power(t1,2)*(18 + 114*t2 + 41*Power(t2,2)) + 
                  t1*(40 + 359*t2 + 141*Power(t2,2) - 18*Power(t2,3))))) \
+ s*(-44*Power(t1,2) + 116*Power(t1,3) + 11*Power(t1,4) - 
            38*Power(t1,5) - 3*Power(t1,6) + 
            2*Power(s1,5)*(3 + 5*s2 + Power(s2,2) - 4*t1 - Power(t1,2)) + 
            Power(s2,6)*(4 - 3*t2) + 8*t1*t2 + 32*Power(t1,2)*t2 - 
            342*Power(t1,3)*t2 + 158*Power(t1,4)*t2 - 9*Power(t1,5)*t2 + 
            5*Power(t1,6)*t2 - 4*t1*Power(t2,2) + 
            191*Power(t1,2)*Power(t2,2) + 19*Power(t1,3)*Power(t2,2) - 
            4*Power(t1,4)*Power(t2,2) - 8*Power(t1,5)*Power(t2,2) - 
            24*Power(t2,3) + 8*t1*Power(t2,3) - 
            99*Power(t1,2)*Power(t2,3) + 3*Power(t1,3)*Power(t2,3) + 
            Power(t1,4)*Power(t2,3) + 12*Power(t2,4) - 4*t1*Power(t2,4) + 
            13*Power(t1,2)*Power(t2,4) + 2*Power(t1,3)*Power(t2,4) + 
            Power(s2,5)*(51 - 10*t2 + 13*Power(t2,2) + 
               t1*(-17 + 10*t2)) - 
            Power(s1,4)*(28 + 19*Power(s2,3) - 6*Power(t1,3) + 
               Power(t1,2)*(19 - 8*t2) + 8*t2 - 8*t1*(5 + 2*t2) + 
               Power(s2,2)*(35 - 44*t1 + 8*t2) + 
               s2*(44 - 54*t1 + 31*Power(t1,2) + 24*t2)) + 
            Power(s2,4)*(31 - 5*Power(t1,2)*(-5 + t2) + 100*t2 - 
               3*Power(t2,2) + 17*Power(t2,3) + 
               t1*(-242 + 31*t2 - 60*Power(t2,2))) + 
            Power(s2,2)*(-44 + 8*t2 + 211*Power(t2,2) - 
               107*Power(t2,3) + 5*Power(t2,4) + 
               5*Power(t1,4)*(-2 + 7*t2) - 
               2*Power(t1,3)*(216 + 7*t2 + 50*Power(t2,2)) + 
               3*Power(t1,2)*
                (42 + 258*t2 - 7*Power(t2,2) + 18*Power(t2,3)) + 
               t1*(324 - 866*t2 - 33*Power(t2,2) + 43*Power(t2,3))) + 
            Power(s2,3)*(-104 + 262*t2 + 26*Power(t2,2) - 
               20*Power(t2,3) + Power(t2,4) - 
               10*Power(t1,3)*(1 + 2*t2) + 
               2*Power(t1,2)*(229 - 12*t2 + 55*Power(t2,2)) - 
               t1*(104 + 458*t2 - 13*Power(t2,2) + 52*Power(t2,3))) - 
            s2*(11*Power(t1,5)*(-1 + 2*t2) + 
               4*t2*(2 - t2 + 2*Power(t2,2)) - 
               Power(t1,4)*(203 + 26*t2 + 45*Power(t2,2)) + 
               Power(t1,3)*(64 + 574*t2 - 15*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(t1,2)*(336 - 946*t2 + 12*Power(t2,2) + 
                  26*Power(t2,3) + 3*Power(t2,4)) + 
               2*t1*(-44 + 20*t2 + 201*Power(t2,2) - 103*Power(t2,3) + 
                  9*Power(t2,4))) + 
            Power(s1,3)*(-28*Power(s2,4) - 8*Power(t1,4) + 
               Power(t1,3)*(85 - 20*t2) - 4*t1*(19 + 17*t2) + 
               Power(s2,3)*(-95 + 92*t1 + 56*t2) - 
               12*(-2 - 6*t2 + Power(t2,2)) - 
               2*Power(t1,2)*(-17 + 2*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(30 - 108*Power(t1,2) + t1*(275 - 132*t2) + 
                  52*t2 + 12*Power(t2,2)) + 
               s2*(76 + 52*Power(t1,3) + 84*t2 + 12*Power(t2,2) - 
                  16*t1*(4 + 3*t2) + Power(t1,2)*(-265 + 96*t2))) + 
            Power(s1,2)*(6*Power(s2,5) - 16*Power(t1,5) + 
               Power(t1,4)*(166 + 17*t2) + 
               Power(s2,4)*(97 - 40*t1 + 73*t2) + 
               24*t2*(-3 - 2*t2 + Power(t2,2)) + 
               Power(t1,3)*(-107 - 247*t2 + 24*Power(t2,2)) - 
               4*t1*(-5 - 36*t2 - 3*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(83 - 71*t2 + 78*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,3)*(68 + 100*Power(t1,2) + 250*t2 - 
                  54*Power(t2,2) - t1*(457 + 236*t2)) + 
               Power(s2,2)*(79 - 120*Power(t1,3) - 71*t2 + 
                  6*Power(t2,2) - 8*Power(t2,3) + 
                  3*Power(t1,2)*(263 + 90*t2) + 
                  3*t1*(-81 - 249*t2 + 44*Power(t2,2))) + 
               s2*(70*Power(t1,4) - Power(t1,3)*(595 + 124*t2) + 
                  Power(t1,2)*(282 + 744*t2 - 102*Power(t2,2)) - 
                  2*t1*(81 - 71*t2 + 42*Power(t2,2)) + 
                  4*(-5 - 36*t2 - 9*Power(t2,2) + 2*Power(t2,3)))) + 
            s1*(3*Power(s2,6) - Power(s2,5)*(29 + 15*t1 + 19*t2) + 
               Power(t1,5)*(31 + 24*t2) - 
               2*Power(t2,2)*(-36 + 4*t2 + 5*Power(t2,2)) - 
               Power(t1,4)*(61 + 210*t2 + 10*Power(t2,2)) + 
               Power(t1,3)*(142 + 152*t2 + 159*Power(t2,2) - 
                  12*Power(t2,3)) - 
               2*Power(t1,2)*(-14 + 145*t2 - 68*Power(t2,2) + 
                  34*Power(t2,3) + Power(t2,4)) + 
               4*t1*(-2 - 4*t2 - 19*Power(t2,2) + 5*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,4)*(-27 + 30*Power(t1,2) - 142*t2 - 
                  62*Power(t2,2) + t1*(147 + 100*t2)) - 
               Power(s2,3)*(74 + 30*Power(t1,3) + 158*t2 + 
                  135*Power(t2,2) - 16*Power(t2,3) + 
                  Power(t1,2)*(298 + 210*t2) - 
                  2*t1*(71 + 318*t2 + 98*Power(t2,2))) + 
               s2*(8 - 3*Power(t1,5) + 16*t2 + 76*Power(t2,2) - 
                  4*Power(t2,3) - 6*Power(t2,4) - 
                  Power(t1,4)*(153 + 115*t2) + 
                  Power(t1,3)*(210 + 772*t2 + 92*Power(t2,2)) + 
                  4*t1*(-20 + 149*t2 - 71*Power(t2,2) + 
                     24*Power(t2,3)) + 
                  Power(t1,2)*
                   (-358 - 462*t2 - 453*Power(t2,2) + 40*Power(t2,3))) + 
               Power(s2,2)*(15*Power(t1,4) + Power(t1,3)*(302 + 220*t2) - 
                  24*Power(t1,2)*(11 + 44*t2 + 9*Power(t2,2)) + 
                  t1*(290 + 468*t2 + 429*Power(t2,2) - 44*Power(t2,3)) + 
                  2*(26 - 153*t2 + 74*Power(t2,2) - 14*Power(t2,3) + 
                     Power(t2,4))))))*R1(1 - s + s2 - t1))/
     ((-1 + s1)*(-1 + s - s2 + t1)*Power(s - s2 + t1,2)*
       Power(-s + s1 - t2,2)*Power(s1 - s2 + t1 - t2,3)) - 
    (16*(-(Power(s1,7)*(5 + 9*s2 - 9*t1 + t2)) + 
         Power(s1,6)*(17 - 15*Power(s2,2) + 3*Power(t1,2) + 
            t1*(22 - 46*t2) + 6*t2 + 5*Power(t2,2) + 
            s2*(-19 + 12*t1 + 39*t2)) + 
         Power(s,5)*(12 + 15*Power(s1,3) - 2*Power(s2,3) - 37*t1 + 
            7*Power(t1,2) - 6*Power(t1,3) + 
            Power(s1,2)*(-37 + 35*s2 + 12*t1 - 33*t2) + t2 + 14*t1*t2 + 
            7*Power(t1,2)*t2 - 21*Power(t2,2) + 4*t1*Power(t2,2) - 
            5*Power(t2,3) + Power(s2,2)*(-7 - 2*t1 + 9*t2) + 
            s2*(25 + 10*Power(t1,2) + 8*t2 - 16*t1*t2 + 6*Power(t2,2)) + 
            s1*(-1 + 18*t1 - 9*Power(t1,2) + s2*(-40 + 9*t1 - 31*t2) + 
               46*t2 - 14*t1*t2 + 23*Power(t2,2))) + 
         Power(s1,5)*(50 - 3*Power(s2,3) - 5*Power(t1,3) - 
            9*Power(t1,2)*(-2 + t2) - 126*t2 + 40*Power(t2,2) - 
            10*Power(t2,3) + Power(s2,2)*(-35 + t1 + 68*t2) + 
            s2*(40 + 7*Power(t1,2) + t1*(17 - 59*t2) + 45*t2 - 
               64*Power(t2,2)) + t1*(-3 - 98*t2 + 96*Power(t2,2))) + 
         Power(s,6)*(-3*Power(s1,2) + Power(s2,2) + s2*(2 + t1 - t2) - 
            2*(3 + Power(t1,2) + 2*t2 + Power(t2,2) - 2*t1*(1 + t2)) + 
            s1*(-4*s2 + 5*(2 - t1 + t2))) + 
         Power(s1,4)*(-55 + 3*Power(s2,4) - 3*Power(t1,4) - 97*t2 + 
            286*Power(t2,2) - 120*Power(t2,3) + 10*Power(t2,4) + 
            Power(s2,3)*(7 - 6*t1 + 15*t2) + Power(t1,3)*(-36 + 26*t2) + 
            Power(t1,2)*(-37 - 16*t2 + 3*Power(t2,2)) + 
            t1*(-53 + 61*t2 + 160*Power(t2,2) - 104*Power(t2,3)) - 
            2*Power(s2,2)*(32 - 70*t2 + 61*Power(t2,2) + 
               t1*(25 + 2*t2)) + 
            s2*(118 + 6*Power(t1,3) + Power(t1,2)*(79 - 37*t2) - 
               263*t2 + 11*Power(t2,2) + 46*Power(t2,3) + 
               t1*(101 - 124*t2 + 119*Power(t2,2)))) - 
         Power(s1,3)*(175 + 4*Power(t1,5) + Power(t1,4)*(5 - 20*t2) - 
            529*t2 + 245*Power(t2,2) + 229*Power(t2,3) - 
            139*Power(t2,4) + 5*Power(t2,5) + 
            Power(s2,4)*(-8 + 4*t1 + 13*t2) + 
            2*Power(t1,3)*(49 - 53*t2 + 29*Power(t2,2)) + 
            Power(s2,3)*(-71 - 16*Power(t1,2) - 19*t1*(-1 + t2) + 
               5*t2 + 32*Power(t2,2)) + 
            Power(t1,2)*(192 - 315*t2 + 47*Power(t2,2) - 
               18*Power(t2,3)) + 
            t1*(206 - 480*t2 + 302*Power(t2,2) + 122*Power(t2,3) - 
               61*Power(t2,4)) + 
            Power(s2,2)*(141 + 24*Power(t1,3) - 314*t2 + 
               194*Power(t2,2) - 107*Power(t2,3) - 
               3*Power(t1,2)*(3 + 7*t2) - 
               2*t1*(-120 + 58*t2 + 3*Power(t2,2))) + 
            s2*(-201 - 16*Power(t1,4) + 637*t2 - 657*Power(t2,2) + 
               107*Power(t2,3) + 9*Power(t2,4) + 
               Power(t1,3)*(-7 + 47*t2) + 
               Power(t1,2)*(-267 + 217*t2 - 84*Power(t2,2)) + 
               t1*(-333 + 629*t2 - 241*Power(t2,2) + 125*Power(t2,3)))) - 
         Power(s1,2)*(104 + 4*Power(s2,5) + Power(t1,5)*(2 - 10*t2) - 
            729*t2 + 1373*Power(t2,2) - 821*Power(t2,3) + 
            12*Power(t2,4) + 82*Power(t2,5) - Power(t2,6) + 
            Power(t1,4)*(11 - 16*t2 + 38*Power(t2,2)) + 
            Power(t1,3)*(79 - 191*t2 + 100*Power(t2,2) - 
               66*Power(t2,3)) + 
            Power(t1,2)*(319 - 729*t2 + 544*Power(t2,2) - 
               56*Power(t2,3) + 27*Power(t2,4)) + 
            t1*(359 - 1193*t2 + 1299*Power(t2,2) - 499*Power(t2,3) - 
               56*Power(t2,4) + 18*Power(t2,5)) - 
            Power(s2,4)*(2 - 42*t2 + 21*Power(t2,2) + 2*t1*(7 + 5*t2)) + 
            Power(s2,3)*(-83 + 136*t2 + 49*Power(t2,2) - 
               36*Power(t2,3) + 8*Power(t1,2)*(2 + 5*t2) + 
               5*t1*(-1 - 22*t2 + 5*Power(t2,2))) + 
            Power(s2,2)*(258 - 567*t2 + 456*Power(t2,2) - 
               84*Power(t2,3) + 44*Power(t2,4) - 
               4*Power(t1,3)*(1 + 15*t2) + 
               3*Power(t1,2)*(9 + 26*t2 + 17*Power(t2,2)) + 
               t1*(245 - 463*t2 + 2*Power(t2,2) + 6*Power(t2,3))) + 
            s2*(-287 + 1026*t2 - 1324*Power(t2,2) + 746*Power(t2,3) - 
               90*Power(t2,4) + 5*Power(t2,5) + 
               Power(t1,4)*(-4 + 40*t2) + 
               Power(t1,3)*(-31 + 6*t2 - 93*Power(t2,2)) + 
               Power(t1,2)*(-241 + 518*t2 - 151*Power(t2,2) + 
                  96*Power(t2,3)) + 
               t1*(-577 + 1296*t2 - 1000*Power(t2,2) + 140*Power(t2,3) - 
                  71*Power(t2,4)))) - 
         (-1 + t2)*(4*Power(s2,5) - 
            2*Power(t1,5)*(2 - t2 + Power(t2,2)) + 
            Power(t1,4)*(20 - 7*Power(t2,2) + 7*Power(t2,3)) - 
            Power(t1,3)*(8 + 54*t2 - 31*Power(t2,2) + 2*Power(t2,3) + 
               8*Power(t2,4)) + 
            Power(t1,2)*(-80 + 238*t2 - 143*Power(t2,2) + 
               12*Power(t2,3) + 13*Power(t2,4) + 3*Power(t2,5)) + 
            t2*(12 - 136*t2 + 287*Power(t2,2) - 197*Power(t2,3) + 
               35*Power(t2,4) + 4*Power(t2,5)) - 
            t1*(48 - 294*t2 + 559*Power(t2,2) - 368*Power(t2,3) + 
               68*Power(t2,4) + 10*Power(t2,5)) - 
            2*Power(s2,4)*(-10 + 3*t2 - 5*Power(t2,2) + 2*Power(t2,3) + 
               t1*(10 - t2 + Power(t2,2))) + 
            Power(s2,3)*(8 + 44*t2 - 27*Power(t2,2) + 21*Power(t2,3) - 
               5*Power(t2,4) + 8*Power(t1,2)*(5 - t2 + Power(t2,2)) + 
               t1*(-80 + 18*t2 - 23*Power(t2,2) + 5*Power(t2,3))) - 
            Power(s2,2)*(80 - 220*t2 + 122*Power(t2,2) - 
               7*Power(t2,3) - 19*Power(t2,4) + Power(t2,5) + 
               4*Power(t1,3)*(10 - 3*t2 + 3*Power(t2,2)) - 
               3*Power(t1,2)*
                (40 - 6*t2 + 3*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(24 + 142*t2 - 85*Power(t2,2) + 44*Power(t2,3) - 
                  2*Power(t2,4))) + 
            s2*(48 - 258*t2 + 463*Power(t2,2) - 295*Power(t2,3) + 
               59*Power(t2,4) + 6*Power(t2,5) + 
               4*Power(t1,4)*(5 - 2*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(-80 + 6*t2 + 11*Power(t2,2) - 
                  17*Power(t2,3)) + 
               Power(t1,2)*(24 + 152*t2 - 89*Power(t2,2) + 
                  25*Power(t2,3) + 11*Power(t2,4)) - 
               t1*(-160 + 458*t2 - 265*Power(t2,2) + 19*Power(t2,3) + 
                  32*Power(t2,4) + 2*Power(t2,5)))) + 
         s1*(-12 + 252*t2 + 4*Power(s2,5)*t2 - 977*Power(t2,2) + 
            1383*Power(t2,3) - 761*Power(t2,4) + 95*Power(t2,5) + 
            26*Power(t2,6) + Power(t1,5)*(-2 + 6*t2 - 8*Power(t2,2)) + 
            Power(t1,4)*(12 + 6*t2 - 25*Power(t2,2) + 28*Power(t2,3)) + 
            Power(t1,3)*(-22 + 26*t2 - 48*Power(t2,2) + 
               36*Power(t2,3) - 37*Power(t2,4)) + 
            Power(t1,2)*(-258 + 752*t2 - 720*Power(t2,2) + 
               261*Power(t2,3) - Power(t2,4) + 15*Power(t2,5)) + 
            t1*(-238 + 1156*t2 - 1922*Power(t2,2) + 1316*Power(t2,3) - 
               313*Power(t2,4) - 28*Power(t2,5) + 2*Power(t2,6)) - 
            Power(s2,4)*(2*t1*(1 + 5*t2 + 4*Power(t2,2)) + 
               3*(-6 + 10*t2 - 16*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s2,3)*(32 - 44*t2 + 5*Power(t2,2) + 73*Power(t2,3) - 
               21*Power(t2,4) + 8*Power(t1,2)*(1 + 4*Power(t2,2)) + 
               t1*(-66 + 84*t2 - 119*Power(t2,2) + 17*Power(t2,3))) + 
            Power(s2,2)*(-240 + 652*t2 - 583*Power(t2,2) + 
               190*Power(t2,3) + 25*Power(t2,4) + 5*Power(t2,5) - 
               4*Power(t1,3)*(3 - 5*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(90 - 72*t2 + 69*Power(t2,2) + 
                  39*Power(t2,3)) + 
               t1*(-86 + 114*t2 - 58*Power(t2,2) - 110*Power(t2,3) + 
                  5*Power(t2,4))) + 
            s2*(202 - 952*t2 + 1591*Power(t2,2) - 1167*Power(t2,3) + 
               365*Power(t2,4) - 14*Power(t2,5) + 2*Power(t2,6) + 
               4*Power(t1,4)*(2 - 5*t2 + 8*Power(t2,2)) - 
               3*Power(t1,3)*
                (18 - 4*t2 - 9*Power(t2,2) + 23*Power(t2,3)) + 
               Power(t1,2)*(76 - 96*t2 + 101*Power(t2,2) + 
                  Power(t2,3) + 53*Power(t2,4)) - 
               t1*(-498 + 1404*t2 - 1303*Power(t2,2) + 451*Power(t2,3) + 
                  24*Power(t2,4) + 20*Power(t2,5)))) + 
         Power(s,4)*(36 - 30*Power(s1,4) + Power(s2,4) + 53*t1 - 
            51*Power(t1,2) + 18*Power(t1,3) - 2*Power(t1,4) + 7*t2 - 
            83*t1*t2 - 10*Power(t1,2)*t2 - 16*Power(t1,3)*t2 + 
            44*Power(t2,2) + 46*t1*Power(t2,2) + 
            35*Power(t1,2)*Power(t2,2) - 54*Power(t2,3) - 
            14*t1*Power(t2,3) - 3*Power(t2,4) - 
            Power(s2,3)*(-10 + t1 + 15*t2) + 
            Power(s1,3)*(45 - 109*s2 + 11*t1 + 81*t2) - 
            Power(s2,2)*(9 + 3*Power(t1,2) + t1*(2 - 14*t2) + 24*t2 - 
               9*Power(t2,2)) - 
            Power(s1,2)*(-104 + 28*Power(s2,2) + 70*t1 - 
               56*Power(t1,2) + s2*(-91 + 28*t1 - 191*t2) + 134*t2 + 
               40*t1*t2 + 76*Power(t2,2)) + 
            s2*(-89 + 5*Power(t1,3) + 65*t2 + 2*Power(t2,2) + 
               22*Power(t2,3) + Power(t1,2)*(-26 + 17*t2) + 
               t1*(60 + 34*t2 - 44*Power(t2,2))) + 
            s1*(-103 + 10*Power(s2,3) + 13*Power(t1,3) + 
               Power(t1,2)*(59 - 83*t2) - 94*t2 + 149*Power(t2,2) + 
               28*Power(t2,3) + Power(s2,2)*(46 - 7*t1 + 8*t2) + 
               t1*(89 - 38*t2 + 42*Power(t2,2)) - 
               s2*(-25 + 16*Power(t1,2) + 85*t2 + 109*Power(t2,2) - 
                  15*t1*(-7 + 5*t2)))) + 
         Power(s,3)*(-120 + 30*Power(s1,5) + 75*t1 + 223*Power(t1,2) + 
            Power(t1,3) + 3*Power(t1,4) + 2*Power(t1,5) + 
            2*Power(s1,4)*(-4 + 83*s2 - 32*t1 - 47*t2) + 69*t2 - 
            258*t1*t2 - 236*Power(t1,2)*t2 + 52*Power(t1,3)*t2 - 
            13*Power(t1,4)*t2 + 155*Power(t2,2) + 99*t1*Power(t2,2) - 
            68*Power(t1,2)*Power(t2,2) - 4*Power(t1,3)*Power(t2,2) + 
            16*Power(t2,3) + 88*t1*Power(t2,3) + 
            40*Power(t1,2)*Power(t2,3) - 75*Power(t2,4) - 
            26*t1*Power(t2,4) + Power(t2,5) + 
            Power(s2,4)*(-9 + 2*t1 + 7*t2) - 
            Power(s2,3)*(43 + 8*Power(t1,2) + 8*t1*(-3 + t2) - 24*t2 + 
               28*Power(t2,2)) + 
            Power(s1,3)*(-271 + 89*Power(s2,2) - 97*Power(t1,2) + 
               8*s2*(-4 + t1 - 52*t2) + 168*t2 + 105*Power(t2,2) + 
               t1*(38 + 210*t2)) + 
            Power(s2,2)*(151 + 12*Power(t1,3) - 152*t2 - 
               20*Power(t2,2) - 16*Power(t2,3) - 
               18*Power(t1,2)*(1 + t2) + t1*(87 + 4*t2 + 52*Power(t2,2))\
) - s2*(-45 + 8*Power(t1,4) - 42*t2 - 32*Power(t1,3)*t2 + 
               93*Power(t2,2) + 8*Power(t2,3) - 20*Power(t2,4) + 
               5*Power(t1,2)*(9 + 16*t2 + 4*Power(t2,2)) + 
               t1*(374 - 388*t2 - 88*Power(t2,2) + 24*Power(t2,3))) - 
            Power(s1,2)*(-129 + 15*Power(s2,3) + 
               Power(t1,2)*(239 - 211*t2) - 535*t2 + 397*Power(t2,2) + 
               47*Power(t2,3) + Power(s2,2)*(63 - 30*t1 + 155*t2) + 
               2*t1*(50 - 95*t2 + 127*Power(t2,2)) + 
               s2*(193 + 15*Power(t1,2) - 70*t2 - 373*Power(t2,2) + 
                  t1*(-302 + 56*t2))) + 
            s1*(159 - 6*Power(s2,4) + 5*Power(t1,4) - 368*t2 - 
               304*Power(t2,2) + 312*Power(t2,3) + 5*Power(t2,4) + 
               2*Power(t1,3)*(-9 + 7*t2) + 
               Power(s2,3)*(-32 + 13*t1 + 45*t2) + 
               Power(t1,2)*(52 + 248*t2 - 158*Power(t2,2)) + 
               Power(s2,2)*(-33 - 3*Power(t1,2) + t1*(46 - 76*t2) + 
                  96*t2 + 83*Power(t2,2)) + 
               2*t1*(-34 + 121*t2 - 151*Power(t2,2) + 67*Power(t2,3)) + 
               s2*(56 - 9*Power(t1,3) + 129*t2 - 20*Power(t2,2) - 
                  143*Power(t2,3) + Power(t1,2)*(4 + 17*t2) + 
                  t1*(-19 - 344*t2 + 75*Power(t2,2))))) - 
         Power(s,2)*(-114 + 15*Power(s1,6) + 229*t1 + 391*Power(t1,2) + 
            39*Power(t1,3) - 19*Power(t1,4) + 4*Power(t1,5) + 
            Power(s1,5)*(26 + 134*s2 - 81*t1 - 51*t2) + 131*t2 - 
            1039*t1*t2 - 813*Power(t1,2)*t2 + 75*Power(t1,3)*t2 - 
            20*Power(t1,4)*t2 - 6*Power(t1,5)*t2 + 432*Power(t2,2) + 
            1263*t1*Power(t2,2) + 282*Power(t1,2)*Power(t2,2) - 
            44*Power(t1,3)*Power(t2,2) + 27*Power(t1,4)*Power(t2,2) - 
            609*Power(t2,3) - 343*t1*Power(t2,3) + 
            88*Power(t1,2)*Power(t2,3) - 24*Power(t1,3)*Power(t2,3) + 
            95*Power(t2,4) - 84*t1*Power(t2,4) - 
            10*Power(t1,2)*Power(t2,4) + 56*Power(t2,5) + 
            14*t1*Power(t2,5) - Power(t2,6) + 
            Power(s2,4)*(-34 + t1*(4 - 6*t2) + 32*t2 - 15*Power(t2,2)) + 
            Power(s1,4)*(-281 + 114*Power(s2,2) - 75*Power(t1,2) + 
               s2*(78 - 39*t1 - 427*t2) + 92*t2 + 59*Power(t2,2) + 
               t1*(-64 + 310*t2)) + 
            Power(s2,3)*(-71 + 6*t2 + 8*Power(t2,2) + 14*Power(t2,3) + 
               8*Power(t1,2)*(-2 + 3*t2) + 
               t1*(121 - 76*t2 + 18*Power(t2,2))) + 
            Power(s2,2)*(332 + Power(t1,3)*(24 - 36*t2) - 661*t2 + 
               237*Power(t2,2) + 16*Power(t2,3) + 27*Power(t2,4) + 
               3*Power(t1,2)*(-53 + 12*t2 + 12*Power(t2,2)) + 
               t1*(181 + 63*t2 - 60*Power(t2,2) - 52*Power(t2,3))) + 
            s2*(-115 + 620*t2 - 863*Power(t2,2) + 322*Power(t2,3) + 
               6*Power(t2,4) - 3*Power(t2,5) + 
               8*Power(t1,4)*(-2 + 3*t2) + 
               Power(t1,3)*(91 + 28*t2 - 66*Power(t2,2)) + 
               Power(t1,2)*(-149 - 144*t2 + 96*Power(t2,2) + 
                  62*Power(t2,3)) - 
               t1*(723 - 1474*t2 + 519*Power(t2,2) + 104*Power(t2,3) + 
                  17*Power(t2,4))) - 
            Power(s1,3)*(63 + 5*Power(s2,3) - 20*Power(t1,3) - 940*t2 + 
               481*Power(t2,2) + 20*Power(t2,3) - 
               3*Power(t1,2)*(-97 + 70*t2) + 
               Power(s2,2)*(-19 - 30*t1 + 318*t2) + 
               t1*(84 - 424*t2 + 462*Power(t2,2)) + 
               s2*(245 + 45*Power(t1,2) + 132*t2 - 507*Power(t2,2) - 
                  4*t1*(68 + 27*t2))) - 
            Power(s1,2)*(-447 + 12*Power(s2,4) - 7*Power(t1,4) + 
               Power(s2,3)*(45 - 29*t1 - 30*t2) + 436*t2 + 
               979*Power(t2,2) - 630*Power(t2,3) + 9*Power(t2,4) + 
               14*Power(t1,3)*(-4 + 3*t2) + 
               Power(t1,2)*(38 - 478*t2 + 213*Power(t2,2)) - 
               2*t1*(3 + 168*t2 - 340*Power(t2,2) + 166*Power(t2,3)) + 
               Power(s2,2)*(55 + 15*Power(t1,2) + 28*t2 - 
                  324*Power(t2,2) + 2*t1*(-73 + 51*t2)) + 
               s2*(197 + 9*Power(t1,3) + Power(t1,2)*(157 - 114*t2) - 
                  526*t2 - 37*Power(t2,2) + 272*Power(t2,3) + 
                  3*t1*(-31 + 150*t2 + 37*Power(t2,2)))) + 
            s1*(67 + 8*Power(t1,5) + Power(t1,4)*(15 - 38*t2) - 933*t2 + 
               1078*Power(t2,2) + 225*Power(t2,3) - 323*Power(t2,4) + 
               7*Power(t2,5) + Power(s2,4)*(-18 + 8*t1 + 27*t2) + 
               4*Power(t1,3)*(13 - 5*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(522 - 393*t2 - 258*Power(t2,2) + 
                  88*Power(t2,3)) + 
               t1*(424 - 910*t2 + 126*Power(t2,2) + 396*Power(t2,3) - 
                  113*Power(t2,4)) - 
               Power(s2,3)*(105 + 32*Power(t1,2) - 71*t2 + 
                  39*Power(t2,2) + t1*(-39 + 43*t2)) + 
               Power(s2,2)*(331 + 48*Power(t1,3) - 244*t2 + 
                  21*Power(t2,2) - 147*Power(t2,3) - 
                  3*Power(t1,2)*(3 + 11*t2) + 
                  2*t1*(131 - 81*t2 + 63*Power(t2,2))) + 
               s2*(-203 - 32*Power(t1,4) + 755*t2 - 608*Power(t2,2) + 
                  19*Power(t2,3) + 61*Power(t2,4) + 
                  3*Power(t1,3)*(-9 + 29*t2) + 
                  Power(t1,2)*(-209 + 111*t2 - 135*Power(t2,2)) + 
                  t1*(-853 + 637*t2 + 237*Power(t2,2) + 59*Power(t2,3))))\
) + s*(-36 + 3*Power(s1,7) - 4*Power(s2,5) + 182*t1 + 294*Power(t1,2) + 
            34*Power(t1,3) - 40*Power(t1,4) + 6*Power(t1,5) + 
            Power(s1,6)*(21 + 55*s2 - 44*t1 - 9*t2) + 46*t2 - 
            1058*t1*t2 - 892*Power(t1,2)*t2 + 86*Power(t1,3)*t2 + 
            12*Power(t1,4)*t2 - 8*Power(t1,5)*t2 + 404*Power(t2,2) + 
            1971*t1*Power(t2,2) + 705*Power(t1,2)*Power(t2,2) - 
            109*Power(t1,3)*Power(t2,2) + 31*Power(t1,4)*Power(t2,2) + 
            6*Power(t1,5)*Power(t2,2) - 969*Power(t2,3) - 
            1368*t1*Power(t2,3) - 96*Power(t1,2)*Power(t2,3) + 
            4*Power(t1,3)*Power(t2,3) - 23*Power(t1,4)*Power(t2,3) + 
            677*Power(t2,4) + 256*t1*Power(t2,4) - 
            47*Power(t1,2)*Power(t2,4) + 26*Power(t1,3)*Power(t2,4) - 
            105*Power(t2,5) + 42*t1*Power(t2,5) - 
            7*Power(t1,2)*Power(t2,5) - 22*Power(t2,6) - 
            2*t1*Power(t2,6) + 
            Power(s2,4)*(-46 + 50*t2 - 37*Power(t2,2) + 13*Power(t2,3) + 
               t1*(22 - 8*t2 + 6*Power(t2,2))) - 
            Power(s2,3)*(44 + 40*t2 - 85*Power(t2,2) + 48*Power(t2,3) - 
               6*Power(t2,4) + 
               8*Power(t1,2)*(6 - 4*t2 + 3*Power(t2,2)) + 
               2*t1*(-89 + 81*t2 - 40*Power(t2,2) + 8*Power(t2,3))) + 
            Power(s2,2)*(276 - 794*t2 + 599*Power(t2,2) - 
               82*Power(t2,3) - 33*Power(t2,4) - 9*Power(t2,5) + 
               4*Power(t1,3)*(13 - 12*t2 + 9*Power(t2,2)) - 
               6*Power(t1,2)*
                (43 - 31*t2 + 3*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(122 + 166*t2 - 279*Power(t2,2) + 100*Power(t2,3) + 
                  14*Power(t2,4))) - 
            s2*(146 - 812*t2 + 1503*Power(t2,2) - 1066*Power(t2,3) + 
               242*Power(t2,4) + 8*Power(t2,5) + 2*Power(t2,6) + 
               4*Power(t1,4)*(7 - 8*t2 + 6*Power(t2,2)) + 
               Power(t1,3)*(-166 + 86*t2 + 56*Power(t2,2) - 
                  56*Power(t2,3)) + 
               Power(t1,2)*(112 + 212*t2 - 303*Power(t2,2) + 
                  56*Power(t2,3) + 46*Power(t2,4)) - 
               2*t1*(-285 + 843*t2 - 652*Power(t2,2) + 89*Power(t2,3) + 
                  40*Power(t2,4) + 8*Power(t2,5))) + 
            Power(s1,4)*(-151 + 5*Power(s2,3) + 18*Power(t1,3) + 
               Power(s2,2)*(78 + 8*t1 - 248*t2) + 624*t2 - 
               252*Power(t2,2) + 14*Power(t2,3) + 
               4*Power(t1,2)*(-34 + 21*t2) + 
               t1*(-33 + 356*t2 - 350*Power(t2,2)) + 
               s2*(-142 - 31*Power(t1,2) - 170*t2 + 301*Power(t2,2) + 
                  2*t1*(29 + 82*t2))) + 
            Power(s1,5)*(67*Power(s2,2) + s2*(76 - 41*t1 - 209*t2) + 
               2*(-62 - 13*Power(t1,2) + 5*t2 + 2*Power(t2,2) + 
                  t1*(-38 + 98*t2))) - 
            Power(s1,3)*(-303 + 10*Power(s2,4) - 7*Power(t1,4) - 36*t2 + 
               1019*Power(t2,2) - 496*Power(t2,3) + 21*Power(t2,4) + 
               Power(t1,3)*(-92 + 66*t2) + 
               Power(t1,2)*(2 - 256*t2 + 93*Power(t2,2)) - 
               2*t1*(35 + 61*t2 - 293*Power(t2,2) + 158*Power(t2,3)) + 
               Power(s2,3)*(-23*t1 + 15*(2 + t2)) + 
               Power(s2,2)*(-51 + 9*Power(t1,2) + 240*t2 - 
                  354*Power(t2,2) + 4*t1*(-38 + 9*t2)) + 
               s2*(278 + 11*Power(t1,3) + Power(t1,2)*(214 - 117*t2) - 
                  589*t2 - 46*Power(t2,2) + 197*Power(t2,3) + 
                  t1*(49 + 16*t2 + 261*Power(t2,2)))) + 
            Power(s1,2)*(358 + 10*Power(t1,5) + 
               Power(t1,4)*(17 - 45*t2) - 1473*t2 + 1028*Power(t2,2) + 
               541*Power(t2,3) - 403*Power(t2,4) + 11*Power(t2,5) + 
               Power(s2,4)*(-17 + 10*t1 + 33*t2) + 
               9*Power(t1,3)*(17 - 20*t2 + 12*Power(t2,2)) + 
               Power(s2,3)*(-137 - 40*Power(t1,2) + t1*(34 - 54*t2) + 
                  56*t2 + 21*Power(t2,2)) + 
               Power(t1,2)*(471 - 458*t2 - 137*Power(t2,2) + 
                  31*Power(t2,3)) + 
               t1*(527 - 1026*t2 + 237*Power(t2,2) + 442*Power(t2,3) - 
                  148*Power(t2,4)) + 
               Power(s2,2)*(301 + 60*Power(t1,3) - 392*t2 - 
                  36*Power(t1,2)*t2 + 245*Power(t2,2) - 
                  241*Power(t2,3) + t1*(427 - 292*t2 + 66*Power(t2,2))) \
+ s2*(-421 - 40*Power(t1,4) + 1238*t2 - 1066*Power(t2,2) + 
                  114*Power(t2,3) + 50*Power(t2,4) + 
                  34*Power(t1,3)*(-1 + 3*t2) + 
                  Power(t1,2)*(-443 + 416*t2 - 195*Power(t2,2)) + 
                  2*t1*(-386 + 425*t2 - 54*Power(t2,2) + 105*Power(t2,3))\
)) + s1*(14 + 4*Power(s2,5) + Power(t1,5)*(6 - 16*t2) - 774*t2 + 
               2127*Power(t2,2) - 1590*Power(t2,3) + 83*Power(t2,4) + 
               150*Power(t2,5) - 2*Power(t2,6) + 
               Power(t1,4)*t2*(-40 + 61*t2) + 
               Power(t1,3)*(78 - 96*t2 + 74*Power(t2,2) - 
                  86*Power(t2,3)) + 
               Power(t1,2)*(678 - 1326*t2 + 606*Power(t2,2) + 
                  68*Power(t2,3) + 11*Power(t2,4)) + 
               2*t1*(314 - 1132*t2 + 1177*Power(t2,2) - 
                  299*Power(t2,3) - 89*Power(t2,4) + 16*Power(t2,5)) - 
               2*Power(s2,4)*(14 - 33*t2 + 18*Power(t2,2) + 
                  t1*(5 + 8*t2)) + 
               Power(s2,3)*(-114 + 118*t2 + 64*Power(t1,2)*t2 + 
                  34*Power(t2,2) - 17*Power(t2,3) + 
                  t1*(84 - 158*t2 + 47*Power(t2,2))) + 
               Power(s2,2)*(558 + Power(t1,3)*(20 - 96*t2) - 1016*t2 + 
                  479*Power(t2,2) - 46*Power(t2,3) + 77*Power(t2,4) + 
                  Power(t1,2)*(-84 + 78*t2 + 75*Power(t2,2)) + 
                  t1*(306 - 332*t2 + 6*Power(t2,2) - 52*Power(t2,3))) + 
               s2*(-442 + 1702*t2 - 2044*Power(t2,2) + 877*Power(t2,3) - 
                  58*Power(t2,4) + 2*Power(t2,5) + 
                  4*Power(t1,4)*(-5 + 16*t2) + 
                  Power(t1,3)*(28 + 54*t2 - 147*Power(t2,2)) + 
                  Power(t1,2)*
                   (-270 + 310*t2 - 114*Power(t2,2) + 155*Power(t2,3)) - 
                  t1*(1236 - 2342*t2 + 1085*Power(t2,2) + 
                     22*Power(t2,3) + 88*Power(t2,4))))))*
       R1(1 - s + s1 - t2))/
     ((-1 + s1)*Power(-s + s1 - t2,2)*(1 - s + s1 - t2)*
       Power(s1 - s2 + t1 - t2,3)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) + 
    (16*(80 + Power(s,5) + Power(s1,5) - 76*s2 - 8*Power(s2,2) + 
         4*Power(s2,3) + 76*t1 + 16*s2*t1 - 12*Power(s2,2)*t1 - 
         8*Power(t1,2) + 12*s2*Power(t1,2) - 4*Power(t1,3) + 
         Power(s,4)*(1 - 5*s1 - 2*s2 + 4*t1) + 
         Power(s1,4)*(-3 + 5*s2 - 4*t1 - t2) - 189*t2 + 98*s2*t2 + 
         15*Power(s2,2)*t2 - 4*Power(s2,3)*t2 - 101*t1*t2 - 20*s2*t1*t2 + 
         5*Power(s2,2)*t1*t2 + 5*Power(t1,2)*t2 + 2*s2*Power(t1,2)*t2 - 
         3*Power(t1,3)*t2 + 132*Power(t2,2) - 31*s2*Power(t2,2) - 
         12*Power(s2,2)*Power(t2,2) + 3*Power(s2,3)*Power(t2,2) + 
         35*t1*Power(t2,2) + 10*s2*t1*Power(t2,2) - 
         3*Power(s2,2)*t1*Power(t2,2) + 2*Power(t1,2)*Power(t2,2) - 
         3*s2*Power(t1,2)*Power(t2,2) + 3*Power(t1,3)*Power(t2,2) - 
         27*Power(t2,3) - 2*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) + 
         2*t1*Power(t2,3) + 2*s2*t1*Power(t2,3) - 
         3*Power(t1,2)*Power(t2,3) - 4*Power(t2,4) + 
         Power(s,3)*(16 + 8*Power(s1,2) + Power(s2,2) + 7*t1 - 
            10*s2*t1 + 9*Power(t1,2) + s1*(-8 + 5*s2 - 12*t1 - t2) - 
            7*t2 + 3*s2*t2 - 3*t1*t2) + 
         Power(s1,3)*(10 + 7*Power(s2,2) + Power(t1,2) + 5*t2 - 
            Power(t2,2) - 2*s2*(5 + 4*t1 + 4*t2) + t1*(5 + 9*t2)) + 
         Power(s,2)*(11 - 4*Power(s1,3) + 24*t1 - Power(t1,2) + 
            6*Power(t1,3) + Power(s2,2)*(-1 + 6*t1 - 6*t2) + 17*t2 + 
            13*t1*t2 + 6*Power(t1,2)*t2 - 18*Power(t2,2) - 
            9*t1*Power(t2,2) + Power(t2,3) + 
            Power(s1,2)*(10 + s2 + 8*t1 + t2) + 
            s1*(-24 + 3*Power(s2,2) - 13*t1 - 19*Power(t1,2) + 
               2*s2*(-3 + 8*t1 - 5*t2) + 7*t2 + 11*t1*t2 - Power(t2,2)) \
+ s2*(-30 + 2*t1 - 12*Power(t1,2) + 8*t2 + 3*Power(t2,2))) + 
         Power(s1,2)*(-49 + 3*Power(s2,3) + 2*Power(t1,3) + 36*t2 - 
            16*Power(t2,2) + Power(t2,3) - Power(t1,2)*(5 + 3*t2) - 
            Power(s2,2)*(7 + 4*t1 + 13*t2) + 
            t1*(-36 + 7*t2 - 7*Power(t2,2)) + 
            s2*(33 - Power(t1,2) + 8*t2 + Power(t2,2) + 4*t1*(3 + 4*t2))) \
+ s1*(25 + Power(t1,3)*(11 - 5*t2) - 7*t2 + Power(t2,2) + 
            6*Power(t2,3) - 2*Power(s2,3)*(2 + 3*t2) + 
            Power(t1,2)*(31 - 25*t2 + 5*Power(t2,2)) + 
            t1*(45 - 51*t2 + 18*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s2,2)*(21 - 9*t2 + 5*Power(t2,2) + t1*(19 + 7*t2)) + 
            2*s2*(-21 + 25*t2 - 14*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(-13 + 2*t2) + 
               t1*(-26 + 17*t2 - 5*Power(t2,2)))) + 
         s*(-113 - Power(s1,4) - 117*t1 - 7*Power(t1,2) - 3*Power(t1,3) + 
            151*t2 + 71*t1*t2 + Power(t1,2)*t2 + 9*Power(t1,3)*t2 - 
            30*Power(t2,2) + 8*t1*Power(t2,2) - 
            6*Power(t1,2)*Power(t2,2) - 14*Power(t2,3) - 
            2*t1*Power(t2,3) + Power(s1,3)*(-9*s2 + 4*t1 + t2) + 
            Power(s2,3)*(-4 + 3*t2) + 
            Power(s2,2)*(3 - 13*t2 - 6*Power(t2,2) + t1*(5 + 3*t2)) + 
            Power(s1,2)*(-2 - 11*Power(s2,2) + t1 + 9*Power(t1,2) - 
               5*t2 - 17*t1*t2 + 2*Power(t2,2) + s2*(16 + 2*t1 + 15*t2)) \
+ s2*(114 + Power(t1,2)*(2 - 15*t2) - 73*t2 + 6*Power(t2,2) - 
               2*Power(t2,3) + 4*t1*(1 + 3*t2 + 3*Power(t2,2))) - 
            s1*(-50 + 3*Power(s2,3) + 8*Power(t1,3) + 
               Power(s2,2)*(-4 + 2*t1 - 17*t2) + 63*t2 - 26*Power(t2,2) + 
               2*Power(t2,3) + Power(t1,2)*(-2 + 5*t2) - 
               4*t1*(5 - 4*t2 + 4*Power(t2,2)) + 
               s2*(11 - 13*Power(t1,2) + 20*t2 + 4*Power(t2,2) + 
                  6*t1*(1 + 2*t2)))))*R2(1 - s2 + t1 - t2))/
     ((-1 + s1)*Power(-s + s1 - t2,2)*(-1 + s2 - t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) - 
    (8*(168 - 256*s2 - 8*Power(s2,2) + 104*Power(s2,3) - 8*Power(s2,5) + 
         256*t1 + 16*s2*t1 - 312*Power(s2,2)*t1 + 40*Power(s2,4)*t1 - 
         8*Power(t1,2) + 312*s2*Power(t1,2) - 
         80*Power(s2,3)*Power(t1,2) - 104*Power(t1,3) + 
         80*Power(s2,2)*Power(t1,3) - 40*s2*Power(t1,4) + 8*Power(t1,5) + 
         Power(s1,6)*(-2*Power(s2,2) + t1 - 2*Power(t1,2) + 
            s2*(-1 + 4*t1)) + 2*Power(s,7)*(t1 - t2) - 636*t2 + 
         609*s2*t2 + 227*Power(s2,2)*t2 - 179*Power(s2,3)*t2 - 
         27*Power(s2,4)*t2 + 6*Power(s2,5)*t2 - 621*t1*t2 - 
         419*s2*t1*t2 + 514*Power(s2,2)*t1*t2 + 99*Power(s2,3)*t1*t2 - 
         23*Power(s2,4)*t1*t2 + 2*Power(s2,5)*t1*t2 + 
         192*Power(t1,2)*t2 - 491*s2*Power(t1,2)*t2 - 
         135*Power(s2,2)*Power(t1,2)*t2 + 32*Power(s2,3)*Power(t1,2)*t2 - 
         10*Power(s2,4)*Power(t1,2)*t2 + 156*Power(t1,3)*t2 + 
         81*s2*Power(t1,3)*t2 - 18*Power(s2,2)*Power(t1,3)*t2 + 
         20*Power(s2,3)*Power(t1,3)*t2 - 18*Power(t1,4)*t2 + 
         2*s2*Power(t1,4)*t2 - 20*Power(s2,2)*Power(t1,4)*t2 + 
         Power(t1,5)*t2 + 10*s2*Power(t1,5)*t2 - 2*Power(t1,6)*t2 + 
         857*Power(t2,2) - 338*s2*Power(t2,2) - 
         381*Power(s2,2)*Power(t2,2) + 63*Power(s2,3)*Power(t2,2) + 
         30*Power(s2,4)*Power(t2,2) - 5*Power(s2,5)*Power(t2,2) - 
         2*Power(s2,6)*Power(t2,2) + 375*t1*Power(t2,2) + 
         672*s2*t1*Power(t2,2) - 151*Power(s2,2)*t1*Power(t2,2) - 
         96*Power(s2,3)*t1*Power(t2,2) + 18*Power(s2,4)*t1*Power(t2,2) + 
         10*Power(s2,5)*t1*Power(t2,2) - 291*Power(t1,2)*Power(t2,2) + 
         113*s2*Power(t1,2)*Power(t2,2) + 
         108*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         22*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         20*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         25*Power(t1,3)*Power(t2,2) - 48*s2*Power(t1,3)*Power(t2,2) + 
         8*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         20*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         6*Power(t1,4)*Power(t2,2) + 3*s2*Power(t1,4)*Power(t2,2) - 
         10*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         2*Power(t1,5)*Power(t2,2) + 2*s2*Power(t1,5)*Power(t2,2) - 
         457*Power(t2,3) - 94*s2*Power(t2,3) + 
         160*Power(s2,2)*Power(t2,3) + 10*Power(s2,3)*Power(t2,3) - 
         11*Power(s2,4)*Power(t2,3) + 57*t1*Power(t2,3) - 
         249*s2*t1*Power(t2,3) - 41*Power(s2,2)*t1*Power(t2,3) + 
         29*Power(s2,3)*t1*Power(t2,3) + 89*Power(t1,2)*Power(t2,3) + 
         52*s2*Power(t1,2)*Power(t2,3) - 
         21*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         21*Power(t1,3)*Power(t2,3) - s2*Power(t1,3)*Power(t2,3) + 
         4*Power(t1,4)*Power(t2,3) + 44*Power(t2,4) + 72*s2*Power(t2,4) + 
         4*Power(s2,2)*Power(t2,4) - 60*t1*Power(t2,4) - 
         24*s2*t1*Power(t2,4) - 4*Power(s2,2)*t1*Power(t2,4) + 
         20*Power(t1,2)*Power(t2,4) + 8*s2*Power(t1,2)*Power(t2,4) - 
         4*Power(t1,3)*Power(t2,4) + 24*Power(t2,5) + 8*s2*Power(t2,5) - 
         8*t1*Power(t2,5) - 2*Power(s,6)*
          (1 + t1 + 4*s1*t1 - 5*Power(t1,2) + 
            s2*(-2 + s1 + 5*t1 - 6*t2) + t2 - 5*s1*t2 + 4*t1*t2 + 
            Power(t2,2)) + Power(s,5)*
          (6 + 20*Power(t1,3) + 
            2*Power(s2,2)*(-8 + 5*s1 + 10*t1 - 15*t2) + 4*t2 + 
            15*s1*t2 - 20*Power(s1,2)*t2 - 13*Power(t2,2) + 
            8*s1*Power(t2,2) - Power(t1,2)*(17 + 36*s1 + 10*t2) + 
            t1*(-16 + 5*s1 + 10*Power(s1,2) + 6*t2 + 40*s1*t2 - 
               10*Power(t2,2)) + 
            s2*(4 + 10*Power(s1,2) + 33*t1 - 40*Power(t1,2) + 
               2*s1*(-7 + 13*t1 - 27*t2) + 7*t2 + 40*t1*t2 + 
               12*Power(t2,2))) - 
         Power(s1,5)*(-1 + 8*Power(s2,3) - 4*Power(t1,3) + 
            Power(t1,2)*(4 - 6*t2) + t1*(10 - 4*t2) + t2 - 
            4*Power(s2,2)*(-1 + 5*t1 + t2) + 
            s2*(-11 + 16*Power(t1,2) + 5*t2 + 2*t1*(-4 + 5*t2))) - 
         Power(s1,4)*(9 + 12*Power(s2,4) + 4*Power(t1,4) - 11*t2 + 
            2*Power(t2,2) + 2*Power(t1,3)*(5 + 4*t2) - 
            4*Power(s2,3)*(-1 + 10*t1 + 4*t2) + 
            Power(t1,2)*(9 - 17*t2 + 8*Power(t2,2)) + 
            t1*(18 - 52*t2 + 21*Power(t2,2)) + 
            Power(s2,2)*(-11 + 48*Power(t1,2) + t2 + 2*Power(t2,2) + 
               t1*(2 + 40*t2)) - 
            s2*(17 + 24*Power(t1,3) - 53*t2 + 23*Power(t2,2) + 
               16*Power(t1,2)*(1 + 2*t2) + 
               2*t1*(-1 - 8*t2 + 5*Power(t2,2)))) + 
         Power(s1,3)*(-13 - 8*Power(s2,5) + 30*Power(t1,4) + 
            4*Power(t1,5) + 59*t2 - 61*Power(t2,2) + 15*Power(t2,3) + 
            4*Power(s2,4)*(1 + 9*t1 + 6*t2) + 
            Power(t1,3)*(67 - 29*t2 + 12*Power(t2,2)) + 
            Power(t1,2)*(37 - 82*t2 + 15*Power(t2,2) + 4*Power(t2,3)) + 
            t1*(-17 + 40*t2 - 55*Power(t2,2) + 20*Power(t2,3)) - 
            Power(s2,3)*(41 + 64*Power(t1,2) - 25*t2 + 8*Power(t2,2) + 
               6*t1*(7 + 12*t2)) + 
            Power(s2,2)*(62 + 56*Power(t1,3) - 154*t2 + 
               54*Power(t2,2) + 6*Power(t1,2)*(17 + 12*t2) + 
               t1*(149 - 79*t2 + 28*Power(t2,2))) - 
            s2*(4 + 24*Power(t1,4) - 7*t2 - 26*Power(t2,2) + 
               17*Power(t2,3) + Power(t1,3)*(94 + 24*t2) + 
               Power(t1,2)*(175 - 83*t2 + 32*Power(t2,2)) + 
               t1*(99 - 236*t2 + 69*Power(t2,2) + 4*Power(t2,3)))) - 
         Power(s1,2)*(-1 + 2*Power(s2,6) + 27*Power(t1,5) + 
            2*Power(t1,6) + Power(t1,4)*(43 - 21*t2) + 7*t2 + 
            23*Power(t2,2) - 41*Power(t2,3) + 12*Power(t2,4) - 
            Power(s2,5)*(13 + 12*t1 + 16*t2) + 
            Power(t1,2)*(-408 + 446*t2 - 98*Power(t2,2) + 
               4*Power(t2,3)) + 
            Power(t1,3)*(-152 + 19*t2 + 17*Power(t2,2) + 
               8*Power(t2,3)) + 
            t1*(-239 + 485*t2 - 256*Power(t2,2) - 5*Power(t2,3) + 
               12*Power(t2,4)) + 
            Power(s2,4)*(59 + 30*Power(t1,2) - 53*t2 + 12*Power(t2,2) + 
               t1*(79 + 64*t2)) - 
            Power(s2,3)*(-89 + 40*Power(t1,3) - 107*t2 + 
               70*Power(t2,2) + 6*Power(t1,2)*(31 + 16*t2) + 
               4*t1*(55 - 45*t2 + 9*Power(t2,2))) + 
            Power(s2,2)*(-382 + 30*Power(t1,4) + 433*t2 - 
               132*Power(t2,2) + 17*Power(t2,3) + 
               Power(t1,3)*(214 + 64*t2) + 
               6*Power(t1,2)*(51 - 37*t2 + 6*Power(t2,2)) + 
               t1*(-330 - 195*t2 + 157*Power(t2,2) + 8*Power(t2,3))) - 
            s2*(-246 + 12*Power(t1,5) + 546*t2 - 359*Power(t2,2) + 
               48*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,4)*(121 + 16*t2) + 
               4*Power(t1,3)*(47 - 29*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-393 - 69*t2 + 104*Power(t2,2) + 
                  16*Power(t2,3)) + 
               t1*(-790 + 879*t2 - 230*Power(t2,2) + 21*Power(t2,3)))) + 
         s1*(188 - 546*t2 + 541*Power(t2,2) - 175*Power(t2,3) - 
            16*Power(t2,4) + 8*Power(t2,5) + 4*Power(s2,6)*(2 + t2) + 
            2*Power(t1,6)*(5 + t2) - 
            Power(t1,5)*(9 + 11*t2 + 4*Power(t2,2)) + 
            Power(t1,4)*(-126 + 85*t2 + 25*Power(t2,2) + 
               4*Power(t2,3)) - 
            Power(t1,3)*(52 - 249*t2 + 131*Power(t2,2) + 
               20*Power(t2,3)) + 
            t1*(509 - 1166*t2 + 797*Power(t2,2) - 106*Power(t2,3) - 
               32*Power(t2,4)) + 
            Power(t1,2)*(376 - 469*t2 - 32*Power(t2,2) + 
               77*Power(t2,3) + 16*Power(t2,4)) + 
            Power(s2,5)*(2 + 32*t2 - 8*Power(t2,2) - 
               2*t1*(25 + 11*t2)) + 
            Power(s2,4)*(-117 + 77*t2 + 34*Power(t2,2) + 
               10*Power(t1,2)*(13 + 5*t2) + 
               t1*(-17 - 139*t2 + 28*Power(t2,2))) + 
            Power(s2,2)*(341 - 353*t2 - 141*Power(t2,2) + 
               111*Power(t2,3) + 8*Power(t2,4) + 
               20*Power(t1,4)*(7 + 2*t2) + 
               2*Power(t1,3)*(-31 - 97*t2 + 4*Power(t2,2)) + 
               t1*(-202 + 949*t2 - 615*Power(t2,2) + 2*Power(t2,3)) + 
               3*Power(t1,2)*
                (-243 + 162*t2 + 59*Power(t2,2) + 4*Power(t2,3))) - 
            Power(s2,3)*(-75 + 350*t2 - 242*Power(t2,2) + 
               11*Power(t2,3) + 60*Power(t1,3)*(3 + t2) + 
               4*Power(t1,2)*(-12 - 59*t2 + 8*Power(t2,2)) + 
               t1*(-477 + 316*t2 + 127*Power(t2,2) + 4*Power(t2,3))) + 
            s2*(-497 + 1136*t2 - 800*Power(t2,2) + 151*Power(t2,3) + 
               8*Power(t2,4) - 2*Power(t1,5)*(29 + 7*t2) + 
               Power(t1,4)*(38 + 76*t2 + 8*Power(t2,2)) - 
               Power(t1,3)*(-495 + 332*t2 + 109*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(179 - 848*t2 + 504*Power(t2,2) + 
                  29*Power(t2,3)) + 
               t1*(-717 + 822*t2 + 173*Power(t2,2) - 188*Power(t2,3) - 
                  24*Power(t2,4)))) + 
         Power(s,4)*(-26 - 15*t1 - 48*Power(t1,2) - 45*Power(t1,3) + 
            20*Power(t1,4) - 20*Power(s1,3)*(s2 - t2) + 57*t2 + 
            30*t1*t2 + 29*Power(t1,2)*t2 - 12*Power(t2,2) - 
            33*t1*Power(t2,2) - 20*Power(t1,2)*Power(t2,2) - 
            11*Power(t2,3) + Power(s2,3)*(24 - 20*t1 + 40*t2) + 
            Power(s1,2)*(20 - 42*Power(s2,2) + t1 + 42*Power(t1,2) - 
               40*t2 - 80*t1*t2 - 12*Power(t2,2) + 3*s2*(5 + 32*t2)) + 
            Power(s2,2)*(2 + 60*Power(t1,2) - 8*t2 - 30*Power(t2,2) - 
               t1*(93 + 80*t2)) + 
            s2*(3 - 60*Power(t1,3) - 40*t2 + 47*Power(t2,2) + 
               2*Power(t1,2)*(57 + 20*t2) + 
               t1*(46 - 21*t2 + 50*Power(t2,2))) + 
            s1*(3 - 20*Power(s2,3) - 60*Power(t1,3) - 66*t2 + 
               67*Power(t2,2) + Power(t1,2)*(64 + 50*t2) + 
               Power(s2,2)*(51 - 20*t1 + 120*t2) + 
               t1*(64 - 31*t2 + 40*Power(t2,2)) + 
               s2*(-10 + 100*Power(t1,2) - 27*t2 - 40*Power(t2,2) - 
                  5*t1*(23 + 34*t2)))) + 
         Power(s,3)*(6 - 41*t1 - 19*Power(t1,2) - 31*Power(t1,3) - 
            49*Power(t1,4) + 10*Power(t1,5) + 
            2*Power(s2,4)*(-8 + 5*t1 - 15*t2) + 
            10*Power(s1,4)*(2*s2 - t1 - t2) - 71*t2 + 4*t1*t2 - 
            48*Power(t1,2)*t2 + 13*Power(t1,3)*t2 + 10*Power(t1,4)*t2 + 
            123*Power(t2,2) + 85*t1*Power(t2,2) + 
            Power(t1,2)*Power(t2,2) - 20*Power(t1,3)*Power(t2,2) - 
            46*Power(t2,3) - 45*t1*Power(t2,3) + 
            2*Power(s1,3)*(-20 + 34*Power(s2,2) - 4*Power(t1,2) + 
               25*t2 + 4*Power(t2,2) - 6*s2*(5*t1 + 7*t2) + 
               t1*(-7 + 40*t2)) + 
            Power(s2,2)*(-51 + 60*Power(t1,3) + 70*t2 - 
               58*Power(t2,2) - 15*Power(t1,2)*(13 + 4*t2) + 
               t1*(-11 + 9*t2 - 100*Power(t2,2))) + 
            Power(s2,3)*(-40*Power(t1,2) + t1*(97 + 80*t2) + 
               2*(-5 + t2 + 20*Power(t2,2))) + 
            s2*(71 + 163*Power(t1,3) - 40*Power(t1,4) - 84*t2 - 
               46*Power(t2,2) + 44*Power(t2,3) + 
               4*Power(t1,2)*(13 - 6*t2 + 20*Power(t2,2)) + 
               t1*(70 - 22*t2 + 57*Power(t2,2))) + 
            Power(s1,2)*(-26 + 68*Power(s2,3) + 56*Power(t1,3) + 
               151*t2 - 119*Power(t2,2) - 2*Power(t1,2)*(41 + 48*t2) - 
               Power(s2,2)*(49 + 80*t1 + 184*t2) + 
               t1*(-62 + 45*t2 - 60*Power(t2,2)) + 
               s2*(-29 - 44*Power(t1,2) + 52*t2 + 48*Power(t2,2) + 
                  t1*(131 + 280*t2))) + 
            s1*(93 + 20*Power(s2,4) - 44*Power(t1,4) - 189*t2 + 
               27*Power(t2,2) + 43*Power(t2,3) + 
               Power(t1,3)*(157 + 12*t2) - 
               Power(s2,3)*(69 + 16*t1 + 140*t2) + 
               Power(t1,2)*(251 - 133*t2 + 68*Power(t2,2)) + 
               t1*(167 - 286*t2 + 133*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(67 - 72*Power(t1,2) - 9*t2 + 
                  80*Power(t2,2) + t1*(295 + 292*t2)) + 
               s2*(-123 + 112*Power(t1,3) + 318*t2 - 187*Power(t2,2) - 
                  Power(t1,2)*(383 + 164*t2) - 
                  2*t1*(159 - 71*t2 + 74*Power(t2,2))))) - 
         Power(s,2)*(-228 - 453*t1 - 202*Power(t1,2) - 4*Power(t1,3) - 
            4*Power(t1,4) + 21*Power(t1,5) - 2*Power(t1,6) + 
            2*Power(s2,5)*(-2 + t1 - 6*t2) + 
            2*Power(s1,5)*(5*s2 - 4*t1 - t2) + 435*t2 + 543*t1*t2 + 
            64*Power(t1,2)*t2 + 65*Power(t1,3)*t2 + 29*Power(t1,4)*t2 - 
            8*Power(t1,5)*t2 - 161*Power(t2,2) - 104*t1*Power(t2,2) - 
            36*Power(t1,2)*Power(t2,2) - 47*Power(t1,3)*Power(t2,2) + 
            10*Power(t1,4)*Power(t2,2) - 88*Power(t2,3) - 
            31*t1*Power(t2,3) + 53*Power(t1,2)*Power(t2,3) + 
            36*Power(t2,4) + 4*t1*Power(t2,4) + 
            2*Power(s1,4)*(-15 + 26*Power(s2,2) + 9*Power(t1,2) + 
               s2*(5 - 35*t1 - 18*t2) + 15*t2 + Power(t2,2) + 
               4*t1*(-2 + 5*t2)) + 
            Power(s2,4)*(-10*Power(t1,2) + 2*t2*(-1 + 15*t2) + 
               t1*(37 + 40*t2)) + 
            Power(s2,3)*(-61 + 20*Power(t1,3) + 70*t2 - 
               22*Power(t2,2) - 4*Power(t1,2)*(27 + 10*t2) + 
               t1*(4 - 23*t2 - 100*Power(t2,2))) + 
            Power(s2,2)*(-158 + 142*Power(t1,3) - 20*Power(t1,4) + 
               30*t2 - 78*Power(t2,2) + 66*Power(t2,3) + 
               t1*(118 - 75*t2 - 3*Power(t2,2)) + 
               3*Power(t1,2)*(-4 + 27*t2 + 40*Power(t2,2))) + 
            s2*(451 + 10*Power(t1,5) - 589*t2 + 207*Power(t2,2) - 
               22*Power(t2,3) + 4*Power(t1,4)*(-22 + 5*t2) + 
               Power(t1,3)*(12 - 85*t2 - 60*Power(t2,2)) + 
               Power(t1,2)*(-53 - 60*t2 + 72*Power(t2,2)) + 
               t1*(360 - 94*t2 + 114*Power(t2,2) - 119*Power(t2,3))) + 
            Power(s1,3)*(-20 + 84*Power(s2,3) + 8*Power(t1,3) + 
               121*t2 - 89*Power(t2,2) - 4*Power(t1,2)*(9 + 22*t2) - 
               Power(s2,2)*(5 + 160*t1 + 132*t2) + 
               t1*(14 + 17*t2 - 40*Power(t2,2)) + 
               s2*(-83 + 68*Power(t1,2) + 56*t2 + 24*Power(t2,2) + 
                  t1*(41 + 220*t2))) + 
            Power(s1,2)*(105 + 52*Power(s2,4) - 24*Power(t1,4) - 
               230*t2 + 48*Power(t2,2) + 49*Power(t2,3) + 
               Power(t1,3)*(185 + 32*t2) - 
               2*Power(s2,3)*(29 + 66*t1 + 88*t2) + 
               Power(t1,2)*(347 - 192*t2 + 84*Power(t2,2)) + 
               t1*(279 - 542*t2 + 192*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(109 + 84*Power(t1,2) - 37*t2 + 
                  72*Power(t2,2) + t1*(301 + 384*t2)) + 
               s2*(-226 + 20*Power(t1,3) + 577*t2 - 260*Power(t2,2) - 
                  4*Power(t1,2)*(107 + 60*t2) + 
                  t1*(-456 + 229*t2 - 156*Power(t2,2)))) + 
            s1*(255 + 10*Power(s2,5) + 12*Power(t1,5) - 620*t2 + 
               514*Power(t2,2) - 119*Power(t2,3) - 8*Power(t2,4) + 
               2*Power(t1,4)*(-72 + 7*t2) - 
               Power(s2,4)*(49 + 28*t1 + 90*t2) + 
               Power(t1,3)*(-211 + 92*t2 - 44*Power(t2,2)) + 
               t1*(333 - 453*t2 + 315*Power(t2,2) - 98*Power(t2,3)) + 
               Power(t1,2)*(23 + 27*t2 - 42*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(s2,3)*(118 + 12*Power(t1,2) - 71*t2 + 
                  80*Power(t2,2) + t1*(291 + 256*t2)) + 
               Power(s2,2)*(-97 + 32*Power(t1,3) + 393*t2 - 
                  207*Power(t2,2) - 3*Power(t1,2)*(193 + 76*t2) - 
                  3*t1*(149 - 78*t2 + 68*Power(t2,2))) + 
               s2*(-237 - 38*Power(t1,4) + 201*t2 - 180*Power(t2,2) + 
                  97*Power(t2,3) + Power(t1,3)*(481 + 48*t2) + 
                  3*Power(t1,2)*(180 - 85*t2 + 56*Power(t2,2)) + 
                  t1*(74 - 420*t2 + 249*Power(t2,2) + 12*Power(t2,3))))) \
+ s*(-380 + 2*Power(s1,6)*(s2 - t1) - 637*t1 - 120*Power(t1,2) + 
            156*Power(t1,3) + 22*Power(t1,4) + Power(t1,5) - 
            2*Power(t1,6) + 1093*t2 - 2*Power(s2,6)*t2 + 1140*t1*t2 - 
            89*Power(t1,2)*t2 - 101*Power(t1,3)*t2 + 10*Power(t1,4)*t2 - 
            23*Power(t1,5)*t2 + 2*Power(t1,6)*t2 - 1002*Power(t2,2) - 
            445*t1*Power(t2,2) + 124*Power(t1,2)*Power(t2,2) - 
            55*Power(t1,3)*Power(t2,2) + 24*Power(t1,4)*Power(t2,2) - 
            2*Power(t1,5)*Power(t2,2) + 250*Power(t2,3) - 
            15*t1*Power(t2,3) + 56*Power(t1,2)*Power(t2,3) - 
            15*Power(t1,3)*Power(t2,3) + 48*Power(t2,4) - 
            16*t1*Power(t2,4) - 8*Power(t1,2)*Power(t2,4) - 
            8*Power(t2,5) + Power(s1,5)*
             (-8 + 18*Power(s2,2) + 12*Power(t1,2) + 7*t2 - 
               6*s2*(-1 + 5*t1 + t2) + t1*(-7 + 8*t2)) + 
            Power(s2,5)*(6 - t2 + 12*Power(t2,2) + t1*(2 + 8*t2)) - 
            Power(s2,4)*(-13 - 30*t2 - 7*Power(t2,2) + 
               10*Power(t1,2)*(1 + t2) + t1*(23 + 19*t2 + 50*Power(t2,2))\
) + Power(s2,3)*(-179 + 20*Power(t1,3) + 204*t2 - 50*Power(t2,2) + 
               44*Power(t2,3) - t1*(61 + 100*t2 + 45*Power(t2,2)) + 
               Power(t1,2)*(32 + 86*t2 + 80*Power(t2,2))) + 
            Power(s2,2)*(-85 + 10*Power(t1,4)*(-2 + t2) - 223*t2 + 
               261*Power(t2,2) + 14*Power(t2,3) - 
               2*Power(t1,3)*(9 + 67*t2 + 30*Power(t2,2)) + 
               3*Power(t1,2)*(35 + 40*t2 + 31*Power(t2,2)) + 
               t1*(514 - 509*t2 + 45*Power(t2,2) - 103*Power(t2,3))) + 
            s2*(625 + Power(t1,5)*(10 - 8*t2) - 1101*t2 + 
               424*Power(t2,2) - 8*Power(t2,3) + 32*Power(t2,4) + 
               Power(t1,4)*(2 + 91*t2 + 20*Power(t2,2)) - 
               Power(t1,3)*(79 + 60*t2 + 79*Power(t2,2)) + 
               Power(t1,2)*(-491 + 406*t2 + 60*Power(t2,2) + 
                  74*Power(t2,3)) + 
               t1*(205 + 312*t2 - 385*Power(t2,2) - 70*Power(t2,3) + 
                  8*Power(t2,4))) + 
            Power(s1,4)*(-4 + 44*Power(s2,3) - 12*Power(t1,3) + 
               Power(s2,2)*(13 - 100*t1 - 42*t2) + 
               Power(t1,2)*(3 - 38*t2) + 33*t2 - 24*Power(t2,2) + 
               t1*(38 - 7*t2 - 10*Power(t2,2)) + 
               s2*(-59 + 68*Power(t1,2) + 29*t2 + 4*Power(t2,2) + 
                  16*t1*(-1 + 5*t2))) + 
            Power(s1,3)*(47 + 44*Power(s2,4) + 4*Power(t1,4) - 109*t2 + 
               35*Power(t2,2) + 17*Power(t2,3) + 
               Power(t1,3)*(83 + 28*t2) - 
               Power(s2,3)*(9 + 136*t1 + 92*t2) + 
               Power(t1,2)*(153 - 105*t2 + 44*Power(t2,2)) + 
               t1*(145 - 338*t2 + 113*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(29 + 144*Power(t1,2) - 19*t2 + 
                  24*Power(t2,2) + t1*(101 + 212*t2)) - 
               s2*(123 + 56*Power(t1,3) - 352*t2 + 143*Power(t2,2) + 
                  Power(t1,2)*(175 + 148*t2) + 
                  2*t1*(91 - 62*t2 + 34*Power(t2,2)))) + 
            Power(s1,2)*(238 + 18*Power(s2,5) - 2*Power(t1,5) - 556*t2 + 
               436*Power(t2,2) - 100*Power(t2,3) - 8*Power(t2,4) + 
               Power(t1,4)*(-125 + 4*t2) - 
               Power(s2,4)*(37 + 74*t1 + 84*t2) + 
               Power(t1,3)*(-239 + 112*t2 - 36*Power(t2,2)) + 
               t1*(351 - 453*t2 + 293*Power(t2,2) - 69*Power(t2,3)) - 
               Power(t1,2)*(3 - 153*t2 + 66*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(s2,3)*(161 + 116*Power(t1,2) - 102*t2 + 
                  48*Power(t2,2) + 4*t1*(59 + 62*t2)) - 
               Power(s2,2)*(116 + 84*Power(t1,3) - 473*t2 + 
                  211*Power(t2,2) + 6*Power(t1,2)*(81 + 40*t2) + 
                  t1*(561 - 316*t2 + 132*Power(t2,2))) + 
               s2*(26*Power(t1,4) + Power(t1,3)*(412 + 72*t2) + 
                  Power(t1,2)*(639 - 326*t2 + 120*Power(t2,2)) + 
                  6*(-44 + 39*t2 - 28*Power(t2,2) + 11*Power(t2,3)) + 
                  t1*(119 - 626*t2 + 277*Power(t2,2) + 16*Power(t2,3)))) \
+ s1*(-37 + 2*Power(s2,6) - 4*Power(t1,5)*(-14 + t2) - 84*t2 + 
               275*Power(t2,2) - 191*Power(t2,3) + 32*Power(t2,4) - 
               5*Power(s2,5)*(5 + 2*t1 + 6*t2) + 
               Power(t1,4)*(47 - 16*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(-276 + 142*t2 + Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(-650 + 764*t2 - 249*Power(t2,2) + 
                  35*Power(t2,3)) + 
               t1*(-420 + 670*t2 - 387*Power(t2,2) + 20*Power(t2,3) + 
                  24*Power(t2,4)) + 
               Power(s2,4)*(67 + 20*Power(t1,2) - 82*t2 + 
                  40*Power(t2,2) + 4*t1*(39 + 29*t2)) - 
               Power(s2,3)*(-148 + 20*Power(t1,3) - 104*t2 + 
                  121*Power(t2,2) + 2*Power(t1,2)*(187 + 82*t2) + 
                  2*t1*(124 - 131*t2 + 62*Power(t2,2))) + 
               Power(s2,2)*(-580 + 10*Power(t1,4) + 748*t2 - 
                  369*Power(t2,2) + 65*Power(t2,3) + 
                  Power(t1,3)*(436 + 96*t2) + 
                  6*Power(t1,2)*(57 - 49*t2 + 22*Power(t2,2)) + 
                  t1*(-572 - 66*t2 + 243*Power(t2,2) + 12*Power(t2,3))) - 
               s2*(-425 + 2*Power(t1,5) + 792*t2 - 631*Power(t2,2) + 
                  150*Power(t2,3) + 16*Power(t2,4) + 
                  Power(t1,4)*(249 + 14*t2) + 
                  26*Power(t1,3)*(8 - 5*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*
                   (-700 + 180*t2 + 123*Power(t2,2) + 24*Power(t2,3)) + 
                  2*t1*(-615 + 756*t2 - 309*Power(t2,2) + 50*Power(t2,3)))\
)))*T2(1 - s + s1 - t2,1 - s2 + t1 - t2))/
     ((-1 + s1)*Power(-s + s1 - t2,2)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       (-1 + s2 - t1 + t2)*(-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2) + 2*s2 - 2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 
         2*s*t1 - 2*s1*t1 - 2*s2*t1 + Power(t1,2) + 4*t2)) - 
    (8*(32*s2 - 8*Power(s2,2) - 8*Power(s2,3) - 24*Power(s2,4) + 
         8*Power(s2,5) - 32*t1 + 16*s2*t1 + 24*Power(s2,2)*t1 + 
         96*Power(s2,3)*t1 - 40*Power(s2,4)*t1 - 8*Power(t1,2) - 
         24*s2*Power(t1,2) - 144*Power(s2,2)*Power(t1,2) + 
         80*Power(s2,3)*Power(t1,2) + 8*Power(t1,3) + 96*s2*Power(t1,3) - 
         80*Power(s2,2)*Power(t1,3) - 24*Power(t1,4) + 
         40*s2*Power(t1,4) - 8*Power(t1,5) + 
         Power(s1,6)*(s2 + 2*Power(s2,2) - 4*s2*t1 + t1*(-1 + 2*t1)) + 
         8*t2 - 68*s2*t2 + 4*Power(s2,2)*t2 - 70*Power(s2,3)*t2 + 
         65*Power(s2,4)*t2 - 6*Power(s2,5)*t2 + 92*t1*t2 - 28*s2*t1*t2 + 
         210*Power(s2,2)*t1*t2 - 258*Power(s2,3)*t1*t2 + 
         29*Power(s2,4)*t1*t2 - 2*Power(s2,5)*t1*t2 + 24*Power(t1,2)*t2 - 
         210*s2*Power(t1,2)*t2 + 384*Power(s2,2)*Power(t1,2)*t2 - 
         56*Power(s2,3)*Power(t1,2)*t2 + 10*Power(s2,4)*Power(t1,2)*t2 + 
         70*Power(t1,3)*t2 - 254*s2*Power(t1,3)*t2 + 
         54*Power(s2,2)*Power(t1,3)*t2 - 20*Power(s2,3)*Power(t1,3)*t2 + 
         63*Power(t1,4)*t2 - 26*s2*Power(t1,4)*t2 + 
         20*Power(s2,2)*Power(t1,4)*t2 + 5*Power(t1,5)*t2 - 
         10*s2*Power(t1,5)*t2 + 2*Power(t1,6)*t2 - 36*Power(t2,2) + 
         64*s2*Power(t2,2) - 86*Power(s2,2)*Power(t2,2) + 
         154*Power(s2,3)*Power(t2,2) - 35*Power(s2,4)*Power(t2,2) - 
         Power(s2,5)*Power(t2,2) + 2*Power(s2,6)*Power(t2,2) - 
         132*t1*Power(t2,2) + 208*s2*t1*Power(t2,2) - 
         460*Power(s2,2)*t1*Power(t2,2) + 
         136*Power(s2,3)*t1*Power(t2,2) - 10*Power(s2,5)*t1*Power(t2,2) - 
         122*Power(t1,2)*Power(t2,2) + 458*s2*Power(t1,2)*Power(t2,2) - 
         198*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         10*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         20*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         152*Power(t1,3)*Power(t2,2) + 128*s2*Power(t1,3)*Power(t2,2) - 
         20*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         20*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         31*Power(t1,4)*Power(t2,2) + 15*s2*Power(t1,4)*Power(t2,2) + 
         10*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         4*Power(t1,5)*Power(t2,2) - 2*s2*Power(t1,5)*Power(t2,2) + 
         72*Power(t2,3) - 86*s2*Power(t2,3) + 
         156*Power(s2,2)*Power(t2,3) - 70*Power(s2,3)*Power(t2,3) - 
         4*Power(s2,4)*Power(t2,3) + 6*Power(s2,5)*Power(t2,3) + 
         150*t1*Power(t2,3) - 330*s2*t1*Power(t2,3) + 
         208*Power(s2,2)*t1*Power(t2,3) + 12*Power(s2,3)*t1*Power(t2,3) - 
         24*Power(s2,4)*t1*Power(t2,3) + 174*Power(t1,2)*Power(t2,3) - 
         206*s2*Power(t1,2)*Power(t2,3) - 
         12*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         36*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         68*Power(t1,3)*Power(t2,3) + 4*s2*Power(t1,3)*Power(t2,3) - 
         24*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         6*s2*Power(t1,4)*Power(t2,3) - 70*Power(t2,4) + 
         86*s2*Power(t2,4) - 64*Power(s2,2)*Power(t2,4) - 
         6*Power(s2,3)*Power(t2,4) + 6*Power(s2,4)*Power(t2,4) - 
         108*t1*Power(t2,4) + 132*s2*t1*Power(t2,4) + 
         16*Power(s2,2)*t1*Power(t2,4) - 18*Power(s2,3)*t1*Power(t2,4) - 
         68*Power(t1,2)*Power(t2,4) - 14*s2*Power(t1,2)*Power(t2,4) + 
         18*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         4*Power(t1,3)*Power(t2,4) - 6*s2*Power(t1,3)*Power(t2,4) + 
         31*Power(t2,5) - 28*s2*Power(t2,5) - 4*Power(s2,2)*Power(t2,5) + 
         2*Power(s2,3)*Power(t2,5) + 31*t1*Power(t2,5) + 
         6*s2*t1*Power(t2,5) - 4*Power(s2,2)*t1*Power(t2,5) - 
         2*Power(t1,2)*Power(t2,5) + 2*s2*Power(t1,2)*Power(t2,5) - 
         5*Power(t2,6) - s2*Power(t2,6) + 
         Power(s1,5)*(-1 + 4*Power(s2,3) + t1*(15 - 9*t2) - 
            4*Power(t1,2)*(-1 + t2) + t2 - 
            2*Power(s2,2)*(-2 + 4*t1 + t2) + 
            2*s2*(-8 + 2*Power(t1,2) + 5*t2 + t1*(-4 + 3*t2))) + 
         2*Power(s,4)*(2 - Power(t1,4) + Power(s2,3)*(t1 - t2) - 
            3*Power(s2,2)*Power(t1 - t2,2) + 4*Power(t1,3)*t2 - 
            6*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) - Power(t2,4) + 
            Power(s1,3)*(-2 + 2*s2 - t1 + t2) + 
            s2*(-2 + 3*Power(t1,3) - 9*Power(t1,2)*t2 + 
               9*t1*Power(t2,2) - 3*Power(t2,3)) - 
            3*Power(s1,2)*(-2 + Power(t1,2) - 2*t1*t2 + Power(t2,2) + 
               s2*(2 - t1 + t2)) - 
            3*s1*(2 + Power(t1,3) + Power(s2,2)*(t1 - t2) - 
               3*Power(t1,2)*t2 + 3*t1*Power(t2,2) - Power(t2,3) - 
               2*s2*(1 + Power(t1,2) - 2*t1*t2 + Power(t2,2)))) + 
         Power(s1,4)*(-12*Power(t1,2)*(-2 + t2) + 
            2*Power(t1,3)*(-2 + t2) - 14*Power(s2,3)*(-1 + t2) + 
            7*(2 - 3*t2 + Power(t2,2)) + 
            t1*(30 - 77*t2 + 34*Power(t2,2)) + 
            2*Power(s2,2)*(5 + t2 - 4*Power(t2,2) + t1*(-16 + 15*t2)) + 
            s2*(-32 + Power(t1,2)*(22 - 18*t2) + 84*t2 - 
               39*Power(t2,2) + 2*t1*(-17 + 5*t2 + 4*Power(t2,2)))) - 
         2*Power(s1,3)*(-14 + 2*Power(s2,5) - 2*Power(t1,5) + 52*t2 - 
            53*Power(t2,2) + 15*Power(t2,3) + 3*Power(t1,4)*(5 + t2) + 
            Power(t1,3)*(-3 - 29*t2 + Power(t2,2)) + 
            Power(t1,2)*(-24 + 32*t2 + 5*Power(t2,2) - 2*Power(t2,3)) + 
            t1*(-15 + 41*t2 - 47*Power(t2,2) + 15*Power(t2,3)) + 
            Power(s2,4)*(-10*t1 + 3*(4 + t2)) + 
            Power(s2,3)*(7 + 20*Power(t1,2) + 33*t2 - 8*Power(t2,2) - 
               3*t1*(17 + 4*t2)) + 
            Power(s2,2)*(-19 - 20*Power(t1,3) + 15*t2 + 
               17*Power(t2,2) - 8*Power(t2,3) + 
               9*Power(t1,2)*(9 + 2*t2) + 
               t1*(-17 - 95*t2 + 17*Power(t2,2))) + 
            s2*(9 + 10*Power(t1,4) - 33*t2 + 50*Power(t2,2) - 
               20*Power(t2,3) - 3*Power(t1,3)*(19 + 4*t2) + 
               Power(t1,2)*(13 + 91*t2 - 10*Power(t2,2)) + 
               t1*(43 - 47*t2 - 22*Power(t2,2) + 10*Power(t2,3)))) + 
         Power(s1,2)*(2*Power(s2,6) + 2*Power(t1,6) - 
            Power(t1,5)*(27 + 10*t2) + 
            Power(s2,5)*(25 - 12*t1 + 14*t2) + 
            2*Power(t1,4)*(-20 + 49*t2 + 6*Power(t2,2)) + 
            Power(s2,4)*(-42 - 127*t1 + 30*Power(t1,2) + 90*t2 - 
               66*t1*t2 + 18*Power(t2,2)) - 
            2*Power(t1,3)*(7 - 50*t2 + 58*Power(t2,2) + Power(t2,3)) + 
            t1*(48 - 114*t2 + 42*Power(t2,2) + 22*Power(t2,3) - 
               Power(t2,4)) - 
            2*Power(t1,2)*(-13 - 7*t2 + 44*Power(t2,2) - 
               23*Power(t2,3) + Power(t2,4)) + 
            2*(2 - 28*t2 + 76*Power(t2,2) - 65*Power(t2,3) + 
               15*Power(t2,4)) - 
            2*Power(s2,3)*(6 + 20*Power(t1,3) + 35*t2 - 
               56*Power(t2,2) + 2*Power(t2,3) - 
               Power(t1,2)*(129 + 62*t2) + 
               t1*(-83 + 184*t2 + 33*Power(t2,2))) + 
            2*Power(s2,2)*(29 + 15*Power(t1,4) - 18*t2 - 
               37*Power(t2,2) + 25*Power(t2,3) - 5*Power(t2,4) - 
               Power(t1,3)*(131 + 58*t2) + 
               3*Power(t1,2)*(-41 + 94*t2 + 15*Power(t2,2)) + 
               t1*(5 + 120*t2 - 170*Power(t2,2) + 3*Power(t2,3))) + 
            s2*(-44 - 12*Power(t1,5) + 82*t2 - 2*Power(t2,2) - 
               24*Power(t2,3) - 9*Power(t2,4) + 
               Power(t1,4)*(133 + 54*t2) - 
               6*Power(t1,3)*(-27 + 64*t2 + 9*Power(t2,2)) + 
               2*Power(t1,2)*(8 - 135*t2 + 172*Power(t2,2)) + 
               2*t1*(-42 + 11*t2 + 81*Power(t2,2) - 48*Power(t2,3) + 
                  6*Power(t2,4)))) - 
         s1*(8 - 32*t2 + 44*Power(t2,2) - 8*Power(t2,3) - 
            15*Power(t2,4) + 3*Power(t2,5) + 4*Power(s2,6)*(2 + t2) + 
            2*Power(t1,6)*(5 + t2) + 
            Power(t1,5)*(21 - 47*t2 - 6*Power(t2,2)) + 
            Power(t1,4)*(15 - 87*t2 + 76*Power(t2,2) + 6*Power(t2,3)) - 
            2*Power(t1,3)*(-27 + 79*t2 - 95*Power(t2,2) + 
               29*Power(t2,3) + Power(t2,4)) + 
            2*Power(t1,2)*(8 - 60*t2 + 138*Power(t2,2) - 
               106*Power(t2,3) + 13*Power(t2,4)) - 
            t1*(20 - 60*t2 + 14*Power(t2,2) + 102*Power(t2,3) - 
               85*Power(t2,4) + 7*Power(t2,5)) + 
            2*Power(s2,5)*(-11 + 20*t2 + 8*Power(t2,2) - 
               t1*(25 + 11*t2)) + 
            Power(s2,4)*(17 - 93*t2 + 70*Power(t2,2) + 18*Power(t2,3) + 
               10*Power(t1,2)*(13 + 5*t2) + 
               t1*(109 - 207*t2 - 70*Power(t2,2))) - 
            2*Power(s2,3)*(27 - 67*t2 + 85*Power(t2,2) - 
               27*Power(t2,3) - 2*Power(t2,4) + 
               30*Power(t1,3)*(3 + t2) - 
               2*Power(t1,2)*(-54 + 107*t2 + 30*Power(t2,2)) + 
               t1*(33 - 183*t2 + 143*Power(t2,2) + 30*Power(t2,3))) + 
            2*Power(s2,2)*(-2 - 26*t2 + 99*Power(t2,2) - 
               87*Power(t2,3) + 9*Power(t2,4) - Power(t2,5) + 
               10*Power(t1,4)*(7 + 2*t2) + 
               Power(t1,3)*(107 - 221*t2 - 50*Power(t2,2)) + 
               3*Power(t1,2)*
                (16 - 90*t2 + 73*Power(t2,2) + 12*Power(t2,3)) + 
               t1*(81 - 213*t2 + 265*Power(t2,2) - 83*Power(t2,3) - 
                  5*Power(t2,4))) - 
            2*s2*(-22 + 62*t2 - 29*Power(t2,2) - 51*Power(t2,3) + 
               42*Power(t2,4) - Power(t2,5) + Power(t1,5)*(29 + 7*t2) + 
               Power(t1,4)*(53 - 114*t2 - 20*Power(t2,2)) + 
               Power(t1,3)*(31 - 177*t2 + 149*Power(t2,2) + 
                  18*Power(t2,3)) + 
               Power(t1,2)*(81 - 225*t2 + 275*Power(t2,2) - 
                  85*Power(t2,3) - 4*Power(t2,4)) - 
               t1*(-6 + 86*t2 - 237*Power(t2,2) + 193*Power(t2,3) - 
                  22*Power(t2,4) + Power(t2,5)))) - 
         2*Power(s,3)*(14 - 4*t1 - 6*Power(t1,2) + Power(t1,3) - 
            4*Power(t1,4) + 2*Power(t1,5) + 
            Power(s2,4)*(-2 + 2*t1 - 3*t2) - 4*t2 + 14*t1*t2 - 
            3*Power(t1,2)*t2 + 14*Power(t1,3)*t2 - 7*Power(t1,4)*t2 - 
            8*Power(t2,2) + 3*t1*Power(t2,2) - 
            18*Power(t1,2)*Power(t2,2) + 8*Power(t1,3)*Power(t2,2) - 
            Power(t2,3) + 10*t1*Power(t2,3) - 
            2*Power(t1,2)*Power(t2,3) - 2*Power(t2,4) - 
            2*t1*Power(t2,4) + Power(t2,5) + 
            Power(s1,4)*(-4 + 5*s2 - 3*t1 + 2*t2) + 
            Power(s1,3)*(-7 + 5*Power(s2,2) - 3*Power(t1,2) + 20*t2 - 
               7*Power(t2,2) - 2*s2*(1 + t1 + 3*t2) + 2*t1*(-4 + 5*t2)) \
- Power(s2,3)*(3 + 8*Power(t1,2) + 8*t2 + 8*Power(t2,2) - 
               2*t1*(5 + 8*t2)) + 
            Power(s2,2)*(2 + 12*Power(t1,3) - 7*t2 - 12*Power(t2,2) - 
               6*Power(t2,3) - 6*Power(t1,2)*(3 + 5*t2) + 
               t1*(7 + 30*t2 + 24*Power(t2,2))) + 
            s2*(-10 - 8*Power(t1,4) + 2*t2 - 5*Power(t2,2) - 
               8*Power(t2,3) + 2*Power(t1,3)*(7 + 12*t2) - 
               Power(t1,2)*(5 + 36*t2 + 24*Power(t2,2)) + 
               2*t1*(2 + 5*t2 + 15*Power(t2,2) + 4*Power(t2,3))) - 
            Power(s1,2)*(-34 + 3*Power(s2,3) - 3*Power(t1,3) + 
               Power(t1,2)*(22 - 3*t2) + 27*t2 + 22*Power(t2,2) - 
               9*Power(t2,3) + Power(s2,2)*(20 - 9*t1 + 9*t2) + 
               t1*(3 - 44*t2 + 15*Power(t2,2)) + 
               s2*(5 + 9*Power(t1,2) + 18*t2 - 3*Power(t2,2) - 
                  6*t1*(7 + t2))) + 
            s1*(-38 + Power(s2,4) + 5*Power(t1,4) + 16*t2 + 
               21*Power(t2,2) + 8*Power(t2,3) - 5*Power(t2,4) - 
               2*Power(t1,3)*(7 + 5*t2) + 3*Power(t1,2)*(5 + 12*t2) + 
               2*t1*(5 - 18*t2 - 15*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,3)*(-8*t1 + 10*(1 + t2)) + 
               Power(s2,2)*(7 + 18*Power(t1,2) + 28*t2 + 
                  12*Power(t2,2) - 2*t1*(17 + 15*t2)) - 
               2*s2*(-8 + 8*Power(t1,3) - 2*t2 - 13*Power(t2,2) + 
                  Power(t2,3) - Power(t1,2)*(19 + 15*t2) + 
                  t1*(11 + 32*t2 + 6*Power(t2,2))))) + 
         Power(s,2)*(48 - 52*t1 - 52*Power(t1,2) + 20*Power(t1,3) - 
            2*Power(t1,4) + 9*Power(t1,5) - 2*Power(t1,6) + 
            2*Power(s2,5)*(-2 + t1 - 3*t2) - 32*t2 + 140*t1*t2 - 
            20*Power(t1,2)*t2 - 2*Power(t1,3)*t2 - 19*Power(t1,4)*t2 + 
            4*Power(t1,5)*t2 - 64*Power(t2,2) - 32*t1*Power(t2,2) + 
            18*Power(t1,2)*Power(t2,2) - 2*Power(t1,3)*Power(t2,2) + 
            4*Power(t1,4)*Power(t2,2) + 32*Power(t2,3) - 
            22*t1*Power(t2,3) + 30*Power(t1,2)*Power(t2,3) - 
            16*Power(t1,3)*Power(t2,3) + 8*Power(t2,4) - 
            23*t1*Power(t2,4) + 14*Power(t1,2)*Power(t2,4) + 
            5*Power(t2,5) - 4*t1*Power(t2,5) + 
            2*Power(s1,5)*(-2 + 4*s2 - 3*t1 + t2) + 
            Power(s1,4)*(-42 + 20*Power(s2,2) + 4*Power(t1,2) + 55*t2 - 
               8*Power(t2,2) - 6*s2*(-2 + 4*t1 + t2) + t1*(-21 + 10*t2)) \
- Power(s2,4)*(2 + 10*Power(t1,2) + 11*t2 + 12*Power(t2,2) - 
               t1*(25 + 28*t2)) + 
            2*Power(s2,3)*(-12 + 10*Power(t1,3) + 7*t2 - 
               2*Power(t2,2) - 2*Power(t1,2)*(15 + 13*t2) + 
               2*t1*(2 + 13*t2 + 8*Power(t2,2))) - 
            2*Power(s2,2)*(8 + 10*Power(t1,4) + 38*t2 - 
               21*Power(t2,2) - 7*Power(t2,3) - 6*Power(t2,4) - 
               Power(t1,3)*(35 + 24*t2) + 
               3*Power(t1,2)*(2 + 15*t2 + 4*Power(t2,2)) + 
               t1*(-34 + 15*t2 - 3*Power(t2,2) + 8*Power(t2,3))) + 
            2*s2*(2 + 5*Power(t1,5) - 10*t2 - 22*Power(t2,2) + 
               17*Power(t2,3) + 8*Power(t2,4) + 3*Power(t2,5) - 
               Power(t1,4)*(20 + 11*t2) + Power(t1,3)*(4 + 34*t2) + 
               Power(t1,2)*(-32 + 9*t2 + 16*Power(t2,3)) + 
               t1*(34 + 48*t2 - 30*Power(t2,2) - 22*Power(t2,3) - 
                  13*Power(t2,4))) + 
            2*Power(s1,3)*(22 + 7*Power(t1,3) + 35*t2 - 
               63*Power(t2,2) + 6*Power(t2,3) - 
               7*Power(t1,2)*(3 + 2*t2) + 
               t1*(-18 + 60*t2 + Power(t2,2)) + 
               Power(s2,2)*(7*t1 - 8*(2 + 3*t2)) - 
               s2*(6 + 14*Power(t1,2) + 25*t2 + 12*Power(t2,2) - 
                  t1*(37 + 38*t2))) - 
            2*Power(s1,2)*(-34 + 4*Power(s2,4) - Power(t1,4) + 116*t2 - 
               31*Power(t2,2) - 50*Power(t2,3) + 4*Power(t2,4) - 
               Power(s2,3)*(2 + 11*t1 + 2*t2) + 
               Power(t1,3)*(-2 + 17*t2) + 
               Power(t1,2)*(24 - 46*t2 - 27*Power(t2,2)) + 
               t1*(-4 - 47*t2 + 98*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(50 + 9*Power(t1,2) - 48*t2 - 
                  30*Power(t2,2) + t1*(2 + 21*t2)) - 
               s2*(28 + Power(t1,3) - 49*t2 + 60*Power(t2,2) + 
                  20*Power(t2,3) + Power(t1,2)*(-2 + 36*t2) + 
                  t1*(74 - 94*t2 - 57*Power(t2,2)))) + 
            2*s1*(-56 + 2*Power(s2,5) - 2*Power(t1,5) + 
               Power(t1,4)*(7 - 3*t2) + 70*t2 + 54*Power(t2,2) - 
               49*Power(t2,3) - 15*Power(t2,4) + Power(t2,5) + 
               Power(s2,4)*(6 - 10*t1 + 9*t2) + 
               2*Power(t1,3)*(-11 + 6*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(58 + 13*t2 - 60*Power(t2,2) - 
                  22*Power(t2,3)) + 
               2*t1*(25 - 77*t2 + 29*Power(t2,2) + 28*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,3)*(6 + 20*Power(t1,2) - 3*t2 - 2*Power(t2,2) - 
                  t1*(25 + 24*t2)) + 
               Power(s2,2)*(70 - 20*Power(t1,3) + 5*t2 - 
                  39*Power(t2,2) - 22*Power(t2,3) + 
                  3*Power(t1,2)*(13 + 6*t2) + 
                  2*t1*(-17 + 9*t2 + 12*Power(t2,2))) + 
               s2*(-38 - 27*Power(t1,3) + 10*Power(t1,4) + 88*t2 - 
                  14*Power(t2,2) - 45*Power(t2,3) - 12*Power(t2,4) + 
                  Power(t1,2)*(50 - 27*t2 - 42*Power(t2,2)) + 
                  t1*(-128 - 18*t2 + 99*Power(t2,2) + 44*Power(t2,3))))) \
+ s*(-24 - 2*Power(s1,6)*(s2 - t1) + 76*t1 + 48*Power(t1,2) - 
            26*Power(t1,3) + 23*Power(t1,4) + 5*Power(t1,5) + 
            2*Power(t1,6) + 20*t2 + 2*Power(s2,6)*t2 - 208*t1*t2 - 
            30*Power(t1,2)*t2 - 52*Power(t1,3)*t2 - 33*Power(t1,4)*t2 + 
            5*Power(t1,5)*t2 - 2*Power(t1,6)*t2 + 76*Power(t2,2) + 
            186*t1*Power(t2,2) + 62*Power(t1,2)*Power(t2,2) + 
            68*Power(t1,3)*Power(t2,2) - 27*Power(t1,4)*Power(t2,2) + 
            8*Power(t1,5)*Power(t2,2) - 114*Power(t2,3) - 
            72*t1*Power(t2,3) - 56*Power(t1,2)*Power(t2,3) + 
            30*Power(t1,3)*Power(t2,3) - 12*Power(t1,4)*Power(t2,3) + 
            39*Power(t2,4) + 15*t1*Power(t2,4) - 
            8*Power(t1,2)*Power(t2,4) + 8*Power(t1,3)*Power(t2,4) + 
            Power(t2,5) - 3*t1*Power(t2,5) - 2*Power(t1,2)*Power(t2,5) + 
            Power(t2,6) - Power(s1,5)*
             (-16 + 12*Power(s2,2) + s2*(5 - 18*t1) + 6*Power(t1,2) + 
               2*t1*(-3 + t2) + 15*t2) - 
            Power(s2,5)*(6 + 5*t2 + t1*(2 + 8*t2)) + 
            Power(s2,4)*(25 - 37*t2 - 19*Power(t2,2) - 12*Power(t2,3) + 
               10*Power(t1,2)*(1 + t2) + t1*(29 + 25*t2 + 8*Power(t2,2))) \
+ Power(s1,4)*(13 - 10*Power(s2,3) - 2*Power(t1,3) - 71*t2 + 
               53*Power(t2,2) + 2*Power(t1,2)*(-7 + 8*t2) + 
               2*Power(s2,2)*(-10 + 9*t1 + 12*t2) + 
               t1*(-17 + 5*t2 - 8*Power(t2,2)) + 
               s2*(38 - 6*Power(t1,2) + t1*(34 - 40*t2) - 29*t2 + 
                  16*Power(t2,2))) - 
            2*Power(s2,3)*(-13 + 10*Power(t1,3) - 25*t2 + 
               31*Power(t2,2) + 13*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,2)*(28 + 25*t2 + 16*Power(t2,2)) - 
               t1*(-49 + 72*t2 + 42*Power(t2,2) + 24*Power(t2,3))) - 
            2*Power(s2,2)*(-14 + 5*Power(t1,4)*(-2 + t2) - 21*t2 - 
               2*Power(t2,2) + 18*Power(t2,3) + 7*Power(t2,4) + 
               3*Power(t2,5) - 
               Power(t1,3)*(27 + 25*t2 + 24*Power(t2,2)) + 
               3*Power(t1,2)*
                (-24 + 35*t2 + 23*Power(t2,2) + 12*Power(t2,3)) + 
               t1*(39 + 76*t2 - 96*Power(t2,2) - 41*Power(t2,3) - 
                  20*Power(t2,4))) + 
            s2*(-52 + 92*t2 - 30*Power(t2,2) + 2*Power(t2,3) - 
               4*Power(t2,4) - Power(t2,5) + 2*Power(t1,5)*(-5 + 4*t2) - 
               Power(t1,4)*(26 + 25*t2 + 32*Power(t2,2)) + 
               2*Power(t1,3)*
                (-47 + 68*t2 + 50*Power(t2,2) + 24*Power(t2,3)) - 
               2*Power(t1,2)*
                (-39 - 77*t2 + 99*Power(t2,2) + 43*Power(t2,3) + 
                  16*Power(t2,4)) + 
               2*t1*(-38 - 6*t2 - 33*Power(t2,2) + 46*Power(t2,3) + 
                  11*Power(t2,4) + 4*Power(t2,5))) + 
            2*Power(s1,3)*(-33 + 5*Power(s2,4) + 5*Power(t1,4) + 51*t2 + 
               18*Power(t2,2) - 31*Power(t2,3) - 
               6*Power(t1,3)*(5 + t2) + 
               Power(s2,3)*(17 - 20*t1 + 20*t2) + 
               Power(t1,2)*(-21 + 61*t2 - 7*Power(t2,2)) + 
               t1*(7 + 16*t2 - 20*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(4 + 30*Power(t1,2) + 45*t2 - 
                  3*Power(t2,2) - 2*t1*(32 + 23*t2)) + 
               s2*(-10 - 20*Power(t1,3) - 39*t2 + 47*Power(t2,2) - 
                  14*Power(t2,3) + Power(t1,2)*(77 + 32*t2) + 
                  t1*(17 - 106*t2 + 10*Power(t2,2)))) + 
            2*Power(s1,2)*(-8 + 2*Power(t1,5) + 95*t2 - 
               156*Power(t2,2) + 51*Power(t2,3) + 13*Power(t2,4) - 
               Power(t1,4)*(20 + 13*t2) + 
               Power(t1,3)*(-36 + 85*t2 + 15*Power(t2,2)) + 
               Power(t1,2)*(-16 + 70*t2 - 97*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-29 + 50*t2 - 43*Power(t2,2) + 19*Power(t2,3) - 
                  5*Power(t2,4)) + Power(s2,4)*(2*t1 - 5*(4 + 3*t2)) + 
               Power(s2,3)*(54 - 8*Power(t1,2) - 77*t2 - 
                  33*Power(t2,2) + t1*(80 + 58*t2)) + 
               Power(s2,2)*(12*Power(t1,3) - 
                  12*Power(t1,2)*(10 + 7*t2) + 
                  t1*(-144 + 239*t2 + 81*Power(t2,2)) - 
                  3*(5 - 18*t2 + 29*Power(t2,2) + 3*Power(t2,3))) + 
               s2*(-3 - 8*Power(t1,4) + 15*t2 + 33*Power(t2,2) - 
                  41*Power(t2,3) + 9*Power(t2,4) + 
                  Power(t1,3)*(80 + 54*t2) + 
                  Power(t1,2)*(126 - 247*t2 - 63*Power(t2,2)) + 
                  t1*(31 - 124*t2 + 184*Power(t2,2) + 8*Power(t2,3)))) + 
            s1*(52 - 2*Power(s2,6) + Power(s2,5)*(11 + 10*t1) - 132*t2 + 
               14*Power(t2,2) + 158*Power(t2,3) - 84*Power(t2,4) - 
               3*Power(t2,5) - 2*Power(t1,5)*(7 + 5*t2) + 
               Power(t1,4)*(-22 + 93*t2 + 28*Power(t2,2)) - 
               2*Power(t1,3)*(-49 + 4*t2 + 78*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(-46 - 70*t2 + 34*Power(t2,2) + 
                  86*Power(t2,3) + 4*Power(t2,4)) + 
               2*t1*(-50 + 142*t2 - 123*Power(t2,2) + 40*Power(t2,3) - 
                  3*Power(t2,4) + Power(t2,5)) - 
               Power(s2,4)*(20 + 20*Power(t1,2) - 77*t2 - 
                  32*Power(t2,2) + 2*t1*(29 + 5*t2)) + 
               2*Power(s2,3)*(-34 + 10*Power(t1,3) - 25*t2 + 
                  75*Power(t2,2) + 26*Power(t2,3) + 
                  Power(t1,2)*(61 + 20*t2) + 
                  t1*(41 - 162*t2 - 62*Power(t2,2))) - 
               2*Power(s2,2)*(57 + 5*Power(t1,4) - 19*t2 + 
                  20*Power(t2,2) - 55*Power(t2,3) - 9*Power(t2,4) + 
                  Power(t1,3)*(64 + 30*t2) - 
                  3*Power(t1,2)*(-21 + 85*t2 + 30*Power(t2,2)) + 
                  t1*(-117 - 46*t2 + 228*Power(t2,2) + 64*Power(t2,3))) + 
               s2*(144 + 2*Power(t1,5) - 304*t2 + 168*Power(t2,2) - 
                  46*Power(t2,3) + 23*Power(t2,4) - 4*Power(t2,5) + 
                  Power(t1,4)*(67 + 40*t2) - 
                  2*Power(t1,3)*(-43 + 178*t2 + 58*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (-132 - 17*t2 + 231*Power(t2,2) + 50*Power(t2,3)) + 
                  t1*(160 + 32*t2 + 6*Power(t2,2) - 196*Power(t2,3) - 
                     22*Power(t2,4))))))*
       T3(1 - s + s1 - t2,1 - s + s2 - t1))/
     ((-1 + s1)*Power(-s + s1 - t2,2)*Power(s1 - s2 + t1 - t2,2)*
       (1 - s + s*s1 - s1*s2 + s1*t1 - t2)*(-1 + s2 - t1 + t2)) + 
    (8*(32 - 8*s2 - 8*Power(s2,2) - 24*Power(s2,3) + 8*Power(s2,4) + 
         Power(s1,3)*(1 + 2*s2 - 2*t1)*Power(s2 - t1,3) + 8*t1 + 
         16*s2*t1 + 72*Power(s2,2)*t1 - 32*Power(s2,3)*t1 - 
         8*Power(t1,2) - 72*s2*Power(t1,2) + 48*Power(s2,2)*Power(t1,2) + 
         24*Power(t1,3) - 32*s2*Power(t1,3) + 8*Power(t1,4) + 
         2*Power(s,6)*(t1 - t2) - 96*t2 + 14*s2*t2 - 14*Power(s2,2)*t2 + 
         41*Power(s2,3)*t2 - 6*Power(s2,4)*t2 - 14*t1*t2 + 26*s2*t1*t2 - 
         121*Power(s2,2)*t1*t2 + 23*Power(s2,3)*t1*t2 - 
         2*Power(s2,4)*t1*t2 - 12*Power(t1,2)*t2 + 
         119*s2*Power(t1,2)*t2 - 33*Power(s2,2)*Power(t1,2)*t2 + 
         8*Power(s2,3)*Power(t1,2)*t2 - 39*Power(t1,3)*t2 + 
         21*s2*Power(t1,3)*t2 - 12*Power(s2,2)*Power(t1,3)*t2 - 
         5*Power(t1,4)*t2 + 8*s2*Power(t1,4)*t2 - 2*Power(t1,5)*t2 + 
         96*Power(t2,2) - 4*s2*Power(t2,2) + 29*Power(s2,2)*Power(t2,2) - 
         17*Power(s2,3)*Power(t2,2) - Power(s2,4)*Power(t2,2) + 
         2*Power(s2,5)*Power(t2,2) + 4*t1*Power(t2,2) - 
         54*s2*t1*Power(t2,2) + 50*Power(s2,2)*t1*Power(t2,2) + 
         5*Power(s2,3)*t1*Power(t2,2) - 8*Power(s2,4)*t1*Power(t2,2) + 
         25*Power(t1,2)*Power(t2,2) - 49*s2*Power(t1,2)*Power(t2,2) - 
         9*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         12*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         16*Power(t1,3)*Power(t2,2) + 7*s2*Power(t1,3)*Power(t2,2) - 
         8*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         2*Power(t1,4)*Power(t2,2) + 2*s2*Power(t1,4)*Power(t2,2) - 
         32*Power(t2,3) - 2*s2*Power(t2,3) - 7*Power(s2,2)*Power(t2,3) - 
         Power(s2,3)*Power(t2,3) + 2*t1*Power(t2,3) + 
         12*s2*t1*Power(t2,3) + 2*Power(s2,2)*t1*Power(t2,3) - 
         5*Power(t1,2)*Power(t2,3) - s2*Power(t1,2)*Power(t2,3) - 
         2*Power(s,5)*(1 - 4*Power(t1,2) + s2*(-2 + s1 + 4*t1 - 5*t2) - 
            2*t2 - 2*s1*t2 + Power(t2,2) + t1*(4 + s1 + 3*t2)) + 
         Power(s1,2)*(s2 - t1)*
          (2*Power(s2,4) + 2*Power(t1,4) + 
            Power(s2,3)*(1 - 8*t1 - 4*t2) + Power(t1,2)*(9 - 2*t2) - 
            27*t1*(-1 + t2) + 16*Power(-1 + t2,2) + 
            Power(t1,3)*(3 + 2*t2) + 
            Power(s2,2)*(8 + t1 + 12*Power(t1,2) - t2 + 10*t1*t2) - 
            s2*(8*Power(t1,3) + t1*(17 - 3*t2) - 27*(-1 + t2) + 
               Power(t1,2)*(5 + 8*t2))) + 
         Power(s,4)*(4 + 4*t1 - 25*Power(t1,2) + 12*Power(t1,3) + 
            4*Power(s2,2)*(-3 + 3*t1 - 5*t2) - 6*t2 - 4*Power(t1,2)*t2 + 
            5*Power(t2,2) - 8*t1*Power(t2,2) + 
            s2*(-2 + 37*t1 - 24*Power(t1,2) - 7*t2 + 24*t1*t2 + 
               10*Power(t2,2)) + Power(s1,2)*(4*s2 - 2*(t1 + t2)) + 
            s1*(2 + 8*Power(s2,2) + 11*t1 - 6*Power(t1,2) - 5*t2 + 
               14*t1*t2 + 2*Power(t2,2) - 2*s2*(2 + t1 + 10*t2))) + 
         s1*(Power(t1,4)*(21 - 11*t2) + 6*t1*Power(-1 + t2,2) + 
            16*Power(-1 + t2,3) - 4*Power(s2,5)*(2 + t2) + 
            2*Power(t1,5)*(5 + t2) - 
            2*Power(t1,2)*(2 - 5*t2 + 3*Power(t2,2)) + 
            Power(t1,3)*(-9 - 23*t2 + 7*Power(t2,2)) + 
            2*Power(s2,4)*(11 - 8*t2 + Power(t2,2) + 3*t1*(7 + 3*t2)) - 
            Power(s2,3)*(-7 - 25*t2 + 7*Power(t2,2) + 
               8*Power(t1,2)*(11 + 4*t2) + 
               t1*(87 - 59*t2 + 6*Power(t2,2))) + 
            Power(s2,2)*(-2 + 6*t2 - 4*Power(t2,2) + 
               4*Power(t1,3)*(23 + 7*t2) + 
               3*Power(t1,2)*(43 - 27*t2 + 2*Power(t2,2)) + 
               t1*(-23 - 73*t2 + 21*Power(t2,2))) - 
            s2*(6*Power(-1 + t2,2) + 12*Power(t1,4)*(4 + t2) + 
               Power(t1,3)*(85 - 49*t2 + 2*Power(t2,2)) - 
               2*t1*(3 - 8*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-25 - 71*t2 + 21*Power(t2,2)))) + 
         Power(s,3)*(-4 - 2*Power(s1,3)*(s2 - t1) - 11*t1 + 
            3*Power(t1,2) - 28*Power(t1,3) + 8*Power(t1,4) + 19*t2 + 
            24*t1*t2 - 23*Power(t1,2)*t2 + 4*Power(t1,3)*t2 - 
            15*Power(t2,2) + 10*t1*Power(t2,2) - 
            12*Power(t1,2)*Power(t2,2) + Power(t2,3) + 
            Power(s2,3)*(12 - 8*t1 + 20*t2) + 
            Power(s1,2)*(8 - 14*Power(s2,2) - 8*Power(t1,2) + 
               t1*(6 - 8*t2) - 7*t2 + s2*(-9 + 22*t1 + 10*t2)) + 
            Power(s2,2)*(24*Power(t1,2) - 4*t1*(13 + 9*t2) - 
               t2*(3 + 20*t2)) + 
            s2*(9 - 24*Power(t1,3) - 19*t2 - 14*Power(t2,2) + 
               4*Power(t1,2)*(17 + 3*t2) + 
               t1*(-3 + 26*t2 + 32*Power(t2,2))) + 
            s1*(-11 - 12*Power(s2,3) - 6*Power(t1,3) + 3*t2 + 
               6*Power(t2,2) + Power(t1,2)*(42 + 20*t2) + 
               Power(s2,2)*(19 + 18*t1 + 40*t2) + 
               t1*(-5 - 24*t2 + 6*Power(t2,2)) + 
               s2*(4 + 29*t2 - 8*Power(t2,2) - t1*(61 + 60*t2)))) + 
         Power(s,2)*(58 + 32*t1 - 46*Power(t1,2) - 8*Power(t1,3) - 
            13*Power(t1,4) + 2*Power(t1,5) + 
            2*Power(s2,4)*(-2 + t1 - 5*t2) - 56*t2 + 37*t1*t2 + 
            43*Power(t1,2)*t2 - 32*Power(t1,3)*t2 + 6*Power(t1,4)*t2 + 
            9*Power(t2,2) + 4*t1*Power(t2,2) + 
            3*Power(t1,2)*Power(t2,2) - 8*Power(t1,3)*Power(t2,2) - 
            11*Power(t2,3) + 2*t1*Power(t2,3) + 
            Power(s1,3)*(-8 + 6*Power(s2,2) + s2*(9 - 12*t1) - 9*t1 + 
               6*Power(t1,2) + 8*t2) + 
            Power(s2,3)*(10 - 8*Power(t1,2) + 11*t2 + 20*Power(t2,2) + 
               t1*(25 + 24*t2)) + 
            Power(s2,2)*(-46 + 12*Power(t1,3) + 38*t2 + 
               12*Power(t2,2) - 3*Power(t1,2)*(17 + 4*t2) - 
               2*t1*(14 + 27*t2 + 24*Power(t2,2))) + 
            s2*(-30 - 8*Power(t1,4) + Power(t1,3)*(43 - 8*t2) - 43*t2 + 
               Power(t2,2) - 3*Power(t2,3) + 
               t1*(92 - 81*t2 - 15*Power(t2,2)) + 
               Power(t1,2)*(26 + 75*t2 + 36*Power(t2,2))) + 
            Power(s1,2)*(31 + 18*Power(s2,3) - 12*Power(t1,3) + 
               Power(s2,2)*(11 - 48*t1 - 18*t2) + 
               Power(t1,2)*(1 - 12*t2) - 23*t2 - 8*Power(t2,2) + 
               t1*(41 + 4*t2) + 
               3*s2*(-14 + 14*Power(t1,2) - t2 + 2*t1*(-2 + 5*t2))) + 
            s1*(8*Power(s2,4) - 2*Power(t1,4) + 
               Power(t1,3)*(61 + 16*t2) - 
               2*Power(s2,3)*(17 + 11*t1 + 20*t2) + 
               34*(-2 + t2 + Power(t2,2)) + 
               Power(t1,2)*(45 - 44*t2 + 6*Power(t2,2)) + 
               t1*(-33 - 97*t2 + 19*Power(t2,2)) + 
               Power(s2,2)*(46 + 18*Power(t1,2) - 59*t2 + 
                  12*Power(t2,2) + 3*t1*(43 + 32*t2)) - 
               s2*(-35 + 2*Power(t1,3) - 95*t2 + 19*Power(t2,2) + 
                  12*Power(t1,2)*(13 + 6*t2) + 
                  t1*(91 - 103*t2 + 18*Power(t2,2))))) - 
         s*(88 + 30*t1 - 60*Power(t1,2) + 7*Power(t1,3) + 5*Power(t1,4) + 
            2*Power(t1,5) - 162*t2 - 2*Power(s2,5)*t2 + 36*t1*t2 + 
            69*Power(t1,2)*t2 - 8*Power(t1,3)*t2 + 15*Power(t1,4)*t2 - 
            2*Power(t1,5)*t2 + 60*Power(t2,2) - 82*t1*Power(t2,2) - 
            35*Power(t1,2)*Power(t2,2) + 4*Power(t1,3)*Power(t2,2) + 
            2*Power(t1,4)*Power(t2,2) + 14*Power(t2,3) + 
            16*t1*Power(t2,3) - Power(t1,2)*Power(t2,3) + 
            2*Power(s1,3)*(s2 - t1)*
             (-4 + 3*Power(s2,2) + s2*(5 - 6*t1) - 5*t1 + 
               3*Power(t1,2) + 4*t2) + 
            Power(s2,4)*(6 + 5*t2 + 10*Power(t2,2) + t1*(2 + 6*t2)) - 
            Power(s2,3)*(9 - 7*t2 - 2*Power(t2,2) + 
               4*Power(t1,2)*(2 + t2) + t1*(23 + 30*t2 + 32*Power(t2,2))) \
+ Power(s2,2)*(-58 + t1*(25 - 22*t2) - 4*Power(t1,3)*(-3 + t2) + 65*t2 - 
               31*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,2)*(33 + 60*t2 + 36*Power(t2,2))) + 
            s2*(Power(t1,4)*(-8 + 6*t2) + 
               Power(t1,2)*(-23 + 23*t2 - 6*Power(t2,2)) - 
               Power(t1,3)*(21 + 50*t2 + 16*Power(t2,2)) + 
               2*t1*(59 - 67*t2 + 33*Power(t2,2) + 2*Power(t2,3)) - 
               2*(15 + 19*t2 - 43*Power(t2,2) + 9*Power(t2,3))) + 
            Power(s1,2)*(10*Power(s2,4) + 8*Power(t1,4) + 
               Power(s2,3)*(3 - 38*t1 - 14*t2) + 24*Power(-1 + t2,2) + 
               8*Power(t1,3)*(1 + t2) - Power(t1,2)*(16 + 13*t2) + 
               4*t1*(3 - 5*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-18 + 54*Power(t1,2) - 11*t2 + 
                  t1*(2 + 36*t2)) - 
               s2*(34*Power(t1,3) - 2*t1*(17 + 12*t2) + 
                  Power(t1,2)*(13 + 30*t2) + 4*(3 - 5*t2 + 2*Power(t2,2))\
)) + s1*(2*Power(s2,5) - 4*t1*(-1 + t2)*t2 - 8*Power(t1,4)*(5 + t2) - 
               2*Power(-1 + t2,2)*(35 + 4*t2) - 
               Power(s2,4)*(27 + 8*t1 + 20*t2) + 
               Power(t1,2)*(15 + 99*t2 - 20*Power(t2,2)) + 
               Power(t1,3)*(-65 + 36*t2 - 2*Power(t2,2)) + 
               Power(s2,3)*(66 + 12*Power(t1,2) - 51*t2 + 
                  8*Power(t2,2) + t1*(121 + 68*t2)) - 
               Power(s2,2)*(-15 + 8*Power(t1,3) - 99*t2 + 
                  20*Power(t2,2) + 3*Power(t1,2)*(67 + 28*t2) + 
                  t1*(197 - 138*t2 + 18*Power(t2,2))) + 
               s2*(2 + 2*Power(t1,4) - 8*t2 + 6*Power(t2,2) + 
                  Power(t1,3)*(147 + 44*t2) + 
                  Power(t1,2)*(196 - 123*t2 + 12*Power(t2,2)) + 
                  2*t1*(-15 - 99*t2 + 20*Power(t2,2))))))*
       T4(1 - s + s2 - t1))/
     ((-1 + s1)*(s - s2 + t1)*Power(-s + s1 - t2,2)*
       (1 - s + s*s1 - s1*s2 + s1*t1 - t2)*(-1 + s2 - t1 + t2)) + 
    (8*(-8 + 16*s2 - 32*Power(s2,2) + 8*Power(s2,3) - 16*t1 + 64*s2*t1 - 
         24*Power(s2,2)*t1 - 32*Power(t1,2) + 24*s2*Power(t1,2) - 
         8*Power(t1,3) - 2*Power(s,4)*(t1 - t2)*(1 - s2 + t1 - t2) + 
         22*t2 - 67*s2*t2 + 55*Power(s2,2)*t2 - 6*Power(s2,3)*t2 + 
         69*t1*t2 - 107*s2*t1*t2 + 19*Power(s2,2)*t1*t2 - 
         2*Power(s2,3)*t1*t2 + 52*Power(t1,2)*t2 - 20*s2*Power(t1,2)*t2 + 
         6*Power(s2,2)*Power(t1,2)*t2 + 7*Power(t1,3)*t2 - 
         6*s2*Power(t1,3)*t2 + 2*Power(t1,4)*t2 - 25*Power(t2,2) + 
         69*s2*Power(t2,2) - 22*Power(s2,2)*Power(t2,2) - 
         3*Power(s2,3)*Power(t2,2) + 2*Power(s2,4)*Power(t2,2) - 
         74*t1*Power(t2,2) + 41*s2*t1*Power(t2,2) + 
         6*Power(s2,2)*t1*Power(t2,2) - 6*Power(s2,3)*t1*Power(t2,2) - 
         19*Power(t1,2)*Power(t2,2) - 3*s2*Power(t1,2)*Power(t2,2) + 
         6*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         2*s2*Power(t1,3)*Power(t2,2) + 16*Power(t2,3) - 
         17*s2*Power(t2,3) - 2*Power(s2,2)*Power(t2,3) + 
         2*Power(s2,3)*Power(t2,3) + 21*t1*Power(t2,3) + 
         4*s2*t1*Power(t2,3) - 4*Power(s2,2)*t1*Power(t2,3) - 
         2*Power(t1,2)*Power(t2,3) + 2*s2*Power(t1,2)*Power(t2,3) - 
         5*Power(t2,4) - s2*Power(t2,4) + 
         Power(s1,3)*(s2 - t1)*
          (-3 + 2*Power(s2,2) - 5*t1 + 2*Power(t1,2) + 3*t2 - 2*t1*t2 + 
            s2*(5 - 4*t1 + 2*t2)) + 
         Power(s1,2)*(2*Power(s2,4) + 5*Power(t1,3) + 2*Power(t1,4) + 
            3*Power(-1 + t2,2) - Power(s2,3)*(1 + 8*t1 + 2*t2) + 
            Power(t1,2)*(2 - 9*t2 - 2*Power(t2,2)) + 
            t1*(30 - 34*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*(3 + 12*Power(t1,2) - 8*t2 - 4*Power(t2,2) + 
               t1*(7 + 4*t2)) - 
            s2*(31 + 8*Power(t1,3) - 36*t2 + 5*Power(t2,2) + 
               Power(t1,2)*(11 + 2*t2) + t1*(5 - 17*t2 - 6*Power(t2,2)))) \
- 2*Power(s,3)*(7 + 4*Power(s1,2) - 3*t1 - 2*Power(t1,2) + 
            2*Power(t1,3) + Power(s2,2)*(-2 + 2*t1 - 3*t2) + t2 + 
            5*t1*t2 - 3*Power(t1,2)*t2 - 3*Power(t2,2) + Power(t2,3) - 
            s2*(1 + 4*Power(t1,2) + t2 + 2*Power(t2,2) - 
               2*t1*(2 + 3*t2)) + 
            s1*(Power(s2,2) - Power(t1,2) - s2*(-3 + t2) + 
               3*t1*(-1 + t2) - 2*(6 - 2*t2 + Power(t2,2)))) - 
         s1*(4*Power(s2,4)*(2 + t2) + 2*Power(t1,4)*(5 + t2) + 
            2*Power(-1 + t2,2)*(-13 + 3*t2) + 
            Power(t1,3)*(31 - 19*t2 - 2*Power(t2,2)) + 
            Power(t1,2)*(12 - 49*t2 + 18*Power(t2,2)) + 
            t1*(5 - 4*t2 + 6*Power(t2,2) - 7*Power(t2,3)) + 
            2*Power(s2,3)*(-15 + 10*t2 + Power(t2,2) - t1*(17 + 7*t2)) + 
            Power(s2,2)*(15 - 51*t2 + 19*Power(t2,2) - 2*Power(t2,3) + 
               18*Power(t1,2)*(3 + t2) + t1*(91 - 59*t2 - 6*Power(t2,2))) \
- s2*(3 + 2*t2 - 5*Power(t2,3) + 2*Power(t1,3)*(19 + 5*t2) + 
               Power(t1,2)*(92 - 58*t2 - 6*Power(t2,2)) + 
               t1*(27 - 100*t2 + 37*Power(t2,2) - 2*Power(t2,3)))) + 
         Power(s,2)*(16 + 8*Power(s1,3) + 10*t1 + 7*Power(t1,2) + 
            7*Power(t1,3) - 2*Power(t1,4) + 
            2*Power(s2,3)*(-2 + t1 - 3*t2) - 52*t2 + 2*t1*t2 - 
            3*Power(t1,2)*t2 + 3*Power(t2,2) - 9*t1*Power(t2,2) + 
            6*Power(t1,2)*Power(t2,2) + 5*Power(t2,3) - 
            4*t1*Power(t2,3) + 
            2*Power(s1,2)*(-22 + 2*Power(s2,2) - 9*t1 + Power(t1,2) + 
               t2 - Power(t2,2) + s2*(10 - 3*t1 + t2)) + 
            Power(s2,2)*(2 - 6*Power(t1,2) + 3*t2 + 3*t1*(5 + 4*t2)) + 
            s2*(-18 + 6*Power(t1,3) + 17*t2 + 6*Power(t2,3) - 
               6*Power(t1,2)*(3 + t2) - 3*t1*(3 + 2*Power(t2,2))) + 
            s1*(22 + 4*Power(s2,3) + 2*Power(t1,3) + 57*t2 - 
               17*Power(t2,2) + 2*Power(t2,3) - 
               2*Power(s2,2)*(2 + 3*t1 + 4*t2) - 
               Power(t1,2)*(11 + 8*t2) + 
               t1*(-3 + 22*t2 + 4*Power(t2,2)) + 
               s2*(-6 - 15*t2 - 10*Power(t2,2) + t1*(15 + 16*t2)))) + 
         s*(6 + 5*t1 + 28*Power(t1,2) + 7*Power(t1,3) + 2*Power(t1,4) + 
            23*t2 + 2*Power(s2,4)*t2 - 40*t1*t2 - 12*Power(t1,2)*t2 + 
            7*Power(t1,3)*t2 - 2*Power(t1,4)*t2 - 30*Power(t2,2) + 
            17*t1*Power(t2,2) - 9*Power(t1,2)*Power(t2,2) + 
            4*Power(t1,3)*Power(t2,2) - t1*Power(t2,3) - 
            2*Power(t1,2)*Power(t2,3) + Power(t2,4) - 
            2*Power(s1,3)*(Power(s2,2) + Power(t1,2) + 2*(-1 + t2) - 
               t1*(7 + t2) + s2*(7 - 2*t1 + t2)) - 
            Power(s2,3)*(6 + 7*t2 + 4*Power(t2,2) + t1*(2 + 4*t2)) + 
            Power(s1,2)*(32 - 6*Power(s2,3) + 2*Power(t1,2) + 
               4*Power(t1,3) + Power(s2,2)*(-5 + 16*t1) - 43*t2 + 
               11*Power(t2,2) - t1*(30 + 21*t2 + 4*Power(t2,2)) + 
               s2*(31 + 3*t1 - 14*Power(t1,2) + 18*t2 + 6*Power(t2,2))) + 
            Power(s2,2)*(31 + 6*Power(t1,2) - 20*t2 - 3*Power(t2,2) - 
               6*Power(t2,3) + t1*(19 + 21*t2 + 12*Power(t2,2))) + 
            s2*(-3 + 27*t2 - 2*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,3)*(-6 + 4*t2) - 
               Power(t1,2)*(20 + 21*t2 + 12*Power(t2,2)) + 
               t1*(-59 + 32*t2 + 12*Power(t2,2) + 8*Power(t2,3))) + 
            s1*(-67 - 2*Power(s2,4) + 76*t2 - 9*Power(t2,2) - 
               4*Power(t1,3)*(5 + t2) + Power(s2,3)*(13 + 6*t1 + 10*t2) + 
               Power(t1,2)*(-49 + 28*t2 + 2*Power(t2,2)) + 
               2*t1*(-12 + 47*t2 - 7*Power(t2,2) + Power(t2,3)) - 
               Power(s2,2)*(45 + 6*Power(t1,2) - 28*t2 - 8*Power(t2,2) + 
                  t1*(46 + 24*t2)) + 
               s2*(33 + 2*Power(t1,3) - 108*t2 + 21*Power(t2,2) - 
                  4*Power(t2,3) + Power(t1,2)*(53 + 18*t2) - 
                  2*t1*(-47 + 28*t2 + 5*Power(t2,2))))))*
       T5(1 - s2 + t1 - t2))/
     ((-1 + s1)*Power(-s + s1 - t2,2)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       (-1 + s2 - t1 + t2)));
   return a;
};
