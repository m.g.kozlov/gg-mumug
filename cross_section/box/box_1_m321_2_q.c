#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m321_2_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(2*Power(s,2)*s2*t1*(-2*s2*
             (2 + t1 - 10*Power(t1,2) + 3*Power(t1,3) - 3*t2 + 7*t1*t2 + 
               Power(t2,2) - t1*Power(t2,2)) + 
            Power(s2,2)*(2 + 3*Power(t1,2) + 5*t2 - t1*(9 + t2)) + 
            t1*(3*Power(t1,3) + Power(t1,2)*(-11 + t2) + 
               t1*(9 - 2*t2)*t2 + 2*(2 - 3*t2 + Power(t2,2)))) + 
         s*(-2*Power(t1,2)*(1 - 2*t1 + Power(t1,3)) + 
            Power(s2,4)*(2 - 5*Power(t1,3) + 
               Power(t1,2)*(25 + 6*s1 - 7*t2) + t1*(-22 + 2*s1 - t2)) - 
            Power(s2,2)*(7*Power(t1,5) + 2*Power(t2,2) - 
               2*t1*(-1 + t2)*(-19 + 4*s1 + t2) + 
               Power(t1,4)*(-41 - 22*s1 + 23*t2) + 
               Power(t1,3)*(64 + 38*s1 + 3*t2 + 4*s1*t2 - 
                  12*Power(t2,2)) + 
               2*Power(t1,2)*(4 - 45*t2 - 6*s1*t2 + 14*Power(t2,2))) + 
            s2*t1*(Power(t1,5) - 2*Power(t1,4)*(5 + 4*s1 - 5*t2) + 
               Power(t1,3)*(-1 - 2*s1*(-12 + t2) + 27*t2 - 
                  7*Power(t2,2)) - 2*(5 - 12*t2 + 5*Power(t2,2)) + 
               t1*(-30 + 8*s1 + 14*t2 - 8*s1*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(50 + 2*s1*(-4 + t2) - 91*t2 + 15*Power(t2,2))\
) + Power(s2,3)*(11*Power(t1,4) - 4*Power(t1,3)*(14 + 5*s1 - 5*t2) + 
               2*t2 + t1*(-32 + s1*(8 - 14*t2) - 23*t2 + 
                  25*Power(t2,2)) + 
               Power(t1,2)*(77 + t2 - 17*Power(t2,2) + 6*s1*(2 + t2)))) - 
         s2*(s2 - t1)*t1*(-((-1 + s1)*Power(t1,4)) + 
            Power(s2,3)*(-1 + t1)*(4 + 5*s1 - 9*t2) + 8*Power(-1 + t2,2) + 
            Power(t1,3)*(-47 + 2*Power(s1,2) + s1*(8 - 7*t2) + 44*t2) - 
            t1*(29 + 4*Power(s1,2) + 18*s1*(-3 + t2) + 8*t2 - 
               5*Power(t2,2)) + 
            Power(t1,2)*(67 + 10*Power(s1,2) - 20*t2 - 5*Power(t2,2) + 
               s1*(-61 + 9*t2)) + 
            Power(s2,2)*(-7 + Power(s1,2)*(2 + 6*t1) - 8*t2 + 
               21*Power(t2,2) + Power(t1,2)*(-15 + 22*t2) + 
               t1*(22 - 14*t2 - 13*Power(t2,2)) + 
               s1*(11 - 7*Power(t1,2) - 19*t2 + t1*(-4 + 3*t2))) + 
            s2*(5 - 4*Power(s1,2)*(-1 + 3*t1 + 2*Power(t1,2)) + 
               Power(t1,3)*(10 - 13*t2) + 56*t2 - 29*Power(t2,2) + 
               4*t1*(-9 - 5*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(21 - 23*t2 + 5*Power(t2,2)) + 
               s1*(3*Power(t1,3) + 18*(-3 + t2) + 10*t1*(5 + t2) + 
                  Power(t1,2)*(1 + 4*t2))))))/
     (s*(-1 + s2)*s2*Power(s2 - t1,2)*Power(-1 + t1,2)*t1*(-1 + t2)) - 
    (8*(4*(-1 + t1)*(Power(s2,2)*t1 + t1*(-1 + 2*t1) - 
            s2*(-1 + 2*t1 + Power(t1,2)))*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) + 
         Power(s,5)*(-1 + t1)*t1*(Power(t1,2) - 2*t2 - t1*(2 + t2)) + 
         Power(s,4)*(Power(t1,5) - 2*Power(t1,4)*(9 + s1 + s2 - t2) + 
            2*(s2 - t2)*t2 + t1*
             (2 + s2*(2 + 2*s1 - 7*t2) + (-6 + s1)*t2 + Power(t2,2)) + 
            Power(t1,2)*(1 + s1 - 9*s2 - 3*s1*s2 + 10*t2 - 2*s1*t2 + 
               8*s2*t2 + 2*Power(t2,2)) + 
            Power(t1,3)*(36 - 4*t2 - Power(t2,2) + s1*(1 + s2 + t2) + 
               s2*(11 + 3*t2))) - 
         Power(s,3)*(-Power(t1,6) + Power(t1,5)*(18 + s1 - 3*t2) + 
            2*(2 - 3*t2)*t2 - 
            Power(t1,4)*(92 + 14*s1 + Power(s1,2) - 23*t2 - s1*t2) + 
            Power(t1,2)*(31 + Power(s1,2) + s1*(4 - 14*t2) + 5*t2 + 
               3*Power(t2,2)) + t1*(8 - 5*s1 + 10*t2 + 12*Power(t2,2)) + 
            Power(s2,2)*(-Power(t1,4) + 
               s1*(-2 + 9*t1 - 11*Power(t1,2) + 2*Power(t1,3)) + 
               t1*(4 - 16*t2) + 4*t2 + Power(t1,3)*(10 + 3*t2) + 
               3*Power(t1,2)*(-3 + 5*t2)) + 
            Power(t1,3)*(76 - 33*t2 - 5*Power(t2,2) + s1*(28 + 5*t2)) - 
            s2*(2 + Power(t1,4)*(23 + 5*s1 - 5*t2) - 3*s1*t2 + 
               10*Power(t2,2) - 
               Power(t1,3)*(97 + Power(s1,2) - 37*t2 - 3*Power(t2,2) + 
                  2*s1*(11 + t2)) + 
               Power(t1,2)*(57 - 58*t2 - 9*Power(t2,2) + 
                  s1*(34 + 4*t2)) + 
               t1*(11 + Power(s1,2) + 22*t2 - s1*(5 + 7*t2)))) + 
         Power(s,2)*(-2 + 4*Power(t1,5)*(16 + 3*s1 - 5*t2) - 
            Power(t1,6)*(5 + s1 - 2*t2) + 8*t2 - 6*Power(t2,2) + 
            t1*(-7 + 8*t2 + 25*Power(t2,2) - 3*s1*(1 + t2)) + 
            Power(t1,4)*(-107 + 53*t2 + Power(t2,2) - s1*(74 + t2)) + 
            Power(s2,3)*(s1*
                (-4 + 9*t1 - 10*Power(t1,2) + Power(t1,3)) + 4*t2 + 
               10*Power(t1,2)*t2 + Power(t1,3)*(2 + t2) - t1*(2 + 11*t2)\
) - Power(t1,2)*(-14 + 3*Power(s1,2) + 57*t2 + 14*Power(t2,2) + 
               s1*(-53 + 21*t2)) - 
            Power(t1,3)*(-43 + Power(s1,2) + 38*t2 + 2*Power(t2,2) - 
               s1*(57 + 25*t2)) + 
            s2*(2*Power(s1,2)*t1*
                (2 + 2*t1 + Power(t1,2) - Power(t1,3)) + 
               Power(t1,5)*(1 - 2*t2) + 
               Power(t1,3)*(156 - 142*t2 - 7*Power(t2,2)) - 
               Power(t1,4)*(98 - 49*t2 + Power(t2,2)) + 
               Power(t1,2)*(-20 + 73*t2 + 3*Power(t2,2)) - 
               2*(1 + 4*t2 + 8*Power(t2,2)) + 
               t1*(-37 + 82*t2 + 21*Power(t2,2)) + 
               s1*(1 + Power(t1,5) + Power(t1,3)*(166 - 11*t2) + 
                  4*Power(t1,4)*(-7 + t2) + 5*t2 - t1*(41 + t2) - 
                  Power(t1,2)*(151 + 5*t2))) + 
            Power(s2,2)*(8 + Power(s1,2)*
                (-1 - 3*t1 - 2*Power(t1,2) + 2*Power(t1,3)) - 3*t2 - 
               14*Power(t2,2) + Power(t1,4)*(-2 + 3*t2) + 
               Power(t1,3)*(48 - 49*t2 - 3*Power(t2,2)) + 
               t1*(8 - 69*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(-62 + 106*t2 + 15*Power(t2,2)) + 
               s1*(-Power(t1,4) + 10*t2 + 2*Power(t1,3)*(10 + t2) + 
                  t1*(74 + 4*t2) - Power(t1,2)*(81 + 8*t2)))) + 
         s*(2*Power(t1,6)*(3 + s1 - 2*t2) + 
            2*Power(s2,4)*(-1 + Power(t1,2))*(s1 - t2) + 
            2*t1*(-2 + s1 - 9*t2)*(-1 + t2) + 2*Power(-1 + t2,2) + 
            Power(t1,5)*(-26 + Power(s1,2) + 31*t2 - s1*(49 + t2)) + 
            Power(t1,2)*(8 + 4*Power(s1,2) + 5*t2 + 27*Power(t2,2) + 
               s1*(-45 + 17*t2)) + 
            Power(t1,4)*(56 + 2*Power(s1,2) - 33*t2 - 3*Power(t2,2) + 
               s1*(47 + 23*t2)) - 
            Power(t1,3)*(50 + 11*Power(s1,2) + 9*t2 + 12*Power(t2,2) + 
               s1*(-47 + 33*t2)) + 
            Power(s2,2)*(-7 + 
               Power(s1,2)*(4 - 17*t1 - 2*Power(t1,2) + 3*Power(t1,3)) + 
               53*t2 - 6*Power(t2,2) + 
               t1*(26 - 35*t2 - 28*Power(t2,2)) + 
               Power(t1,4)*(7 - 16*t2 + Power(t2,2)) + 
               Power(t1,3)*(-2 + 111*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-24 - 113*t2 + 19*Power(t2,2)) - 
               s1*(27 + 12*Power(t1,4) + t1*(83 - 61*t2) + 
                  Power(t1,3)*(121 - 3*t2) + t2 + 
                  3*Power(t1,2)*(-81 + 13*t2))) + 
            Power(s2,3)*(-6 + Power(s1,2)*(3 + 2*t1 - Power(t1,2)) - 
               21*t2 + 10*Power(t2,2) + 
               Power(t1,2)*(6 - 47*t2 - 12*Power(t2,2)) + 
               Power(t1,3)*(-6 + 13*t2 + Power(t2,2)) + 
               t1*(6 + 55*t2 + 5*Power(t2,2)) + 
               s1*(31 - Power(t1,3)*(-5 + t2) - 13*t2 - t1*(57 + 7*t2) + 
                  Power(t1,2)*(21 + 13*t2))) + 
            s2*(-(Power(s1,2)*t1*
                  (8 - 25*t1 + 2*Power(t1,2) + 3*Power(t1,3))) + 
               5*Power(t1,5)*(-1 + t2) + 
               t1*(13 - 86*t2 - 7*Power(t2,2)) + 
               Power(t1,3)*(-24 + 61*t2 - 7*Power(t2,2)) + 
               Power(t1,4)*(14 - 79*t2 + 2*Power(t2,2)) + 
               2*(-6 + t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(14 + 97*t2 + 14*Power(t2,2)) + 
               s1*(2 + Power(t1,2)*(5 - 15*t2) + 
                  Power(t1,4)*(149 - 15*t2) - 2*t2 + 
                  Power(t1,5)*(3 + t2) - 8*t1*(-9 + 2*t2) + 
                  Power(t1,3)*(-231 + 23*t2)))))*B1(s,t1,s2))/
     (s*(-1 + s2)*(-1 + t1)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*
       (-1 + t2)) - (16*(-2*(-1 + s2)*Power(s2,2)*Power(s2 - t1,2)*
          (1 + (-2 + s2)*t1)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) \
+ Power(s,5)*s2*(s2 - t1)*t1*(t1*(2 + Power(t1,2) - t1*(-2 + t2)) + 
            Power(s2,2)*(-2 + t1 + t2) - 
            2*s2*(-1 + Power(t1,2) + t1*(-1 + t2) + 3*t2 - Power(t2,2))) \
+ Power(s,4)*(Power(t1,3)*(-1 + t1 + Power(t1,2)) + 
            Power(s2,5)*(-2 + t1*(3 + s1 - 5*t2) + t2) + 
            s2*Power(t1,2)*(4 - Power(t1,4) + Power(t1,2)*(2 - 4*t2) - 
               2*t2 + Power(t1,3)*(-2 + s1 + t2) + t1*(11 - 2*s1 + 2*t2)\
) + Power(s2,4)*(2 + 2*Power(t1,3) - 6*t2 + 
               Power(t1,2)*(-14 - 2*s1 + 13*t2) + 
               2*t1*(10 + s1*(-1 + t2) - 2*t2 - 5*Power(t2,2))) + 
            Power(s2,2)*t1*(3 + 4*Power(t1,4) - 10*t2 + 6*Power(t2,2) - 
               Power(t1,3)*(2 + 2*s1 + t2) + 
               Power(t1,2)*(-2 - 2*s1*(-1 + t2) + 10*t2 - 
                  3*Power(t2,2)) + 
               t1*(4 + (-42 + 4*s1)*t2 + 5*Power(t2,2))) + 
            Power(s2,3)*t1*(-18 - 5*Power(t1,3) + 
               Power(t1,2)*(15 - 8*t2) + 2*s1*(1 + Power(t1,2) - 2*t2) + 
               46*t2 - 5*Power(t2,2) + t1*(-25 + 9*t2 + 7*Power(t2,2)))) \
+ Power(s,3)*(Power(t1,3)*(3 - 4*t1 - 2*Power(t1,2) + Power(t1,3)) + 
            Power(s2,6)*(s1 + 5*t1 + s1*t1 - 2*Power(t1,2) - 4*t2 + 
               9*t1*t2) + s2*Power(t1,2)*
             (-13 + (-1 + s1)*Power(t1,4) + t1*(-13 + 6*s1 - 8*t2) + 
               Power(t1,3)*(11 - 4*s1 - 3*t2) + 6*t2 + 
               Power(t1,2)*(20 - 6*s1 + 8*t2)) - 
            Power(s2,5)*(-16 - 5*Power(t1,3) + 2*t2 + Power(t2,2) + 
               2*Power(t1,2)*(4 + 9*t2) + 
               s1*(2 + t1 + 6*Power(t1,2) + 2*t2 + 6*t1*t2) + 
               t1*(28 - 45*t2 - 20*Power(t2,2))) - 
            Power(s2,2)*t1*(5 + Power(t1,5) - 28*t2 + 18*Power(t2,2) + 
               Power(t1,4)*(-8 + 4*s1 + t2) + 
               Power(t1,3)*(21 + 2*s1*(-7 + t2) - 2*t2 + 
                  2*Power(t2,2)) + 
               t1*(22 - 92*t2 - Power(t2,2) + 2*s1*(1 + 6*t2)) + 
               Power(t1,2)*(25 + 74*t2 - 8*Power(t2,2) - 
                  2*s1*(6 + 7*t2))) + 
            Power(s2,4)*(-16 - 5*Power(t1,4) + 30*t2 + 3*Power(t2,2) + 
               5*Power(t1,3)*(1 + 2*t2) - t1*(51 + 16*t2) - 
               4*Power(t1,2)*(-8 + 21*t2 + 3*Power(t2,2)) + 
               s1*(2 + 7*Power(t1,3) - 4*t2 + Power(t1,2)*(13 + 2*t2) + 
                  2*t1*(2 + 9*t2))) + 
            Power(s2,3)*(3 + (-9 + s1)*Power(t1,4) + 3*Power(t1,5) - 
               10*t2 + 6*Power(t2,2) + 
               Power(t1,3)*(17 + 20*t2 + 6*Power(t2,2) + 
                  s1*(-23 + 6*t2)) + 
               t1*(43 - 90*t2 - 16*Power(t2,2) + 2*s1*(-3 + 8*t2)) + 
               Power(t1,2)*(54 + 60*t2 + 5*Power(t2,2) - 
                  2*s1*(4 + 15*t2)))) + 
         s*(s2 - t1)*(-(Power(t1,2)*(1 - 2*t1 + Power(t1,3))) + 
            Power(s2,7)*(-1 + t1)*(2 + 3*s1 + t1 + 2*t2) - 
            s2*(-1 + t1)*t1*(4 + (-11 + 2*s1)*Power(t1,3) + 
               Power(t1,2)*(-17 + 4*s1 - t2) - 2*t2 + 
               t1*(7 - 2*s1 + 2*t2)) + 
            Power(s2,6)*(14 + 5*Power(s1,2)*t1 - 2*Power(t1,3) + 
               Power(t1,2)*(-6 + t2) - 8*t2 - Power(t2,2) - 
               s1*(-6 + t1 + 9*Power(t1,2) + 4*t2 + 10*t1*t2) + 
               t1*(-6 + 11*t2 + 10*Power(t2,2))) + 
            Power(s2,2)*(3 - 10*t2 + 6*Power(t2,2) + 
               Power(t1,5)*(-17 + 2*s1 + 2*t2) + 
               Power(t1,4)*(21 - 35*s1 + 5*Power(s1,2) - 3*t2 - 
                  2*s1*t2) + t1*(3 + (-22 + 4*s1)*t2 - Power(t2,2)) - 
               2*Power(t1,2)*(5*(-6 + t2)*t2 + s1*(-1 + 9*t2)) + 
               Power(t1,3)*(-10 - 4*Power(s1,2) - 15*t2 + 
                  2*Power(t2,2) + s1*(19 + 18*t2))) + 
            Power(s2,3)*(-22 - 
               Power(s1,2)*Power(t1,2)*(-16 + 23*t1 + Power(t1,2)) + 
               Power(t1,5)*(5 - 2*t2) + 46*t2 - 13*Power(t2,2) + 
               Power(t1,4)*(14 + 3*t2) - 
               Power(t1,3)*(91 + 13*t2 + 4*Power(t2,2)) + 
               t1*(-27 + t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(121 - 75*t2 + 10*Power(t2,2)) + 
               s1*(2 + 2*Power(t1,5) - 2*Power(t1,4)*(-3 + t2) - 4*t2 + 
                  Power(t1,3)*(117 + 16*t2) + t1*(-6 + 20*t2) - 
                  Power(t1,2)*(81 + 22*t2))) + 
            Power(s2,5)*(-26 + Power(t1,4) + 
               Power(s1,2)*(8 - 13*t1 - 11*Power(t1,2)) - 
               4*Power(t1,3)*(-2 + t2) + 44*t2 + 9*Power(t2,2) + 
               t1*(9 - 27*t2 - 31*Power(t2,2)) + 
               Power(t1,2)*(8 - 37*t2 - 2*Power(t2,2)) + 
               s1*(11*Power(t1,3) - 5*(5 + 2*t2) + 
                  Power(t1,2)*(25 + 14*t2) + t1*(13 + 36*t2))) + 
            Power(s2,4)*(47 + 
               Power(s1,2)*t1*(-20 + 31*t1 + 7*Power(t1,2)) - 50*t2 - 
               Power(t2,2) + Power(t1,4)*(-8 + 3*t2) + 
               Power(t1,3)*(13 + 3*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(2 + 119*t2 + 2*Power(t2,2)) + 
               3*t1*(-18 - 9*t2 + 5*Power(t2,2)) - 
               s1*(7*Power(t1,4) + 2*(1 + t2) + 
                  Power(t1,3)*(29 + 2*t2) - t1*(89 + 14*t2) + 
                  Power(t1,2)*(99 + 46*t2)))) + 
         Power(s,2)*(Power(t1,3)*
             (-3 + 5*t1 + Power(t1,2) - 2*Power(t1,3)) + 
            Power(s2,7)*(4 + s1*(2 - 5*t1) + 5*t2 - 7*t1*(1 + t2)) + 
            s2*Power(t1,2)*(14 - 3*(-3 + s1)*Power(t1,4) - 6*t2 + 
               Power(t1,3)*(-5 + s1 + 3*t2) + t1*(5 - 6*s1 + 10*t2) + 
               4*Power(t1,2)*(3*s1 - 2*(5 + t2))) + 
            Power(s2,6)*(-Power(t1,3) - Power(s1,2)*(2 + 3*t1) + 
               Power(t1,2)*(23 + 9*t2) + 2*(-5 + 10*t2 + Power(t2,2)) - 
               2*t1*(-5 + 24*t2 + 10*Power(t2,2)) + 
               s1*(4 + 20*Power(t1,2) + 6*t2 + t1*(-3 + 10*t2))) + 
            Power(s2,2)*t1*(1 + 
               Power(t1,4)*(-20 + Power(s1,2) + s1*(9 - 2*t2) - 6*t2) + 
               Power(t1,5)*(7 + s1 - 2*t2) - 26*t2 + 18*Power(t2,2) + 
               t1*(20 - 78*t2 - 7*Power(t2,2) + 4*s1*(1 + 3*t2)) + 
               Power(t1,3)*(-21 - 6*Power(s1,2) - 17*t2 + 
                  4*Power(t2,2) + s1*(29 + 12*t2)) + 
               Power(t1,2)*(35 + 122*t2 - 11*Power(t2,2) - 
                  2*s1*(11 + 13*t2))) + 
            Power(s2,5)*(-24 + 4*Power(t1,4) + t2 + 16*s1*t2 - 
               6*Power(t2,2) + Power(t1,3)*(-33 - 29*s1 + 2*t2) + 
               Power(t1,2)*(16 + 8*Power(s1,2) + 109*t2 + 
                  15*Power(t2,2) - 7*s1*(3 + 2*t2)) + 
               t1*(-4 + 12*Power(s1,2) - t2 + 25*Power(t2,2) - 
                  2*s1*(14 + 23*t2))) - 
            Power(s2,4)*(-22 + 5*Power(t1,5) + 
               6*Power(s1,2)*Power(t1,2)*(4 + t1) + 26*t2 + 
               16*Power(t2,2) + Power(t1,4)*(-34 + 7*t2) + 
               t1*(-103 + 30*t2 - 38*Power(t2,2)) + 
               Power(t1,3)*(93 + 52*t2 + 5*Power(t2,2)) + 
               3*Power(t1,2)*(14 + 17*t2 + 9*Power(t2,2)) - 
               s1*(-4 + 42*Power(t1,3) + 19*Power(t1,4) + 8*t2 - 
                  2*t1*(5 + 23*t2) + Power(t1,2)*(87 + 74*t2))) + 
            Power(s2,3)*(-6 + 2*Power(t1,6) + 20*t2 - 12*Power(t2,2) + 
               Power(t1,5)*(-24 - 6*s1 + 5*t2) + 
               t1*(-64 + s1*(6 - 20*t2) + 118*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(-75 - 85*t2 - 21*Power(t2,2) + 
                  4*s1*(5 + 14*t2)) + 
               Power(t1,3)*(96 + 20*Power(s1,2) + 22*t2 + 
                  8*Power(t2,2) - s1*(93 + 46*t2)) + 
               Power(t1,4)*(s1*(-26 + 6*t2) + 4*(17 + t2 + Power(t2,2)))))\
)*R1q(s2))/(s*(-1 + s2)*s2*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + 
         Power(s2,2))*Power(s2 - t1,3)*(-1 + t1)*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*(-1 + t2)) - 
    (16*(2*Power(s2 - t1,2)*(-1 + t1)*Power(t1,2)*(1 + (-2 + s2)*t1)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) + 
         Power(s,3)*(s2 - t1)*Power(t1,2)*
          (2*Power(s2,2)*(1 - t1 - 3*Power(t1,2) + Power(t1,3) + 2*t2) - 
            s2*(4*Power(t1,4) + Power(t1,3)*(-15 + t2) + (-3 + t2)*t2 + 
               t1*(1 + 15*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(2 - 5*t2 + Power(t2,2))) + 
            t1*(-4 + 2*Power(t1,4) + 3*t2 - Power(t2,2) + 
               Power(t1,3)*(-11 + 3*t2) - 
               Power(t1,2)*(-4 + 3*t2 + Power(t2,2)) + 
               t1*(5 + t2 + 2*Power(t2,2)))) + 
         Power(s,2)*t1*(-(Power(s2,4)*
               (-1 - (13 + 2*s1)*Power(t1,3) + 2*Power(t1,4) - 4*t2 + 
                 Power(t1,2)*(11 + 2*s1 + 2*t2) + t1*(1 - 4*s1 + 6*t2))) \
+ Power(s2,3)*(8*Power(t1,5) + t2*(2 + t2) + 
               Power(t1,4)*(-46 - 9*s1 + 5*t2) + 
               Power(t1,2)*(10 + s1*(-12 + t2) + 22*t2 - 
                  6*Power(t2,2)) + 
               Power(t1,3)*(35 - 2*s1*(-8 + t2) - 4*t2 + 
                  4*Power(t2,2)) + 
               t1*(-7 + s1 - 21*t2 - 3*s1*t2 + 5*Power(t2,2))) + 
            s2*t1*(10*Power(t1,6) + 2*(1 - 2*t2)*t2 + 
               Power(t1,5)*(-58 - 7*s1 + 11*t2) - 
               Power(t1,2)*(28 - 7*s1 + 43*t2 + 5*s1*t2) + 
               Power(t1,3)*(-37 + 4*s1 + 66*t2 - 5*s1*t2 + 
                  Power(t2,2)) + 
               Power(t1,4)*(90 + 20*s1 - 50*t2 - 2*s1*t2 + 
                  3*Power(t2,2)) + 
               t1*(23 - 4*s1 - 6*t2 + 12*Power(t2,2))) - 
            Power(t1,2)*(6 + 3*Power(t1,6) + 
               Power(t1,4)*(28 - 2*s1*(-3 + t2) - 11*t2) - 
               Power(t1,5)*(20 + s1 - 4*t2) - 10*t2 + 3*Power(t2,2) + 
               t1*(8 + 2*s1*(-2 + t2) + 10*t2 - 5*Power(t2,2)) + 
               Power(t1,2)*(-35 + s1*(9 - 7*t2) + 2*t2 + 
                  4*Power(t2,2)) + 
               Power(t1,3)*(10 - 3*t2 + 2*Power(t2,2) + s1*(-2 + 3*t2))) \
+ Power(s2,2)*(-13*Power(t1,6) + Power(t1,5)*(71 + 13*s1 - 12*t2) + 
               Power(t2,2) + Power(t1,4)*
                (-80 + 2*s1*(-14 + t2) + 33*t2 - Power(t2,2)) + 
               Power(t1,2)*(-1 + s1 + 62*t2 + s1*t2 - Power(t2,2)) - 
               t1*(3 - 2*(-5 + s1)*t2 + 6*Power(t2,2)) + 
               Power(t1,3)*(26 - 61*t2 - 5*Power(t2,2) + s1*(2 + 7*t2)))) \
- s*(s2 - t1)*(Power(s2,4)*(-1 + t1)*
             (-1 + (5 + 2*s1)*Power(t1,3) + t1*(3 + 4*s1 - 2*t2) - 
               Power(t1,2)*(7 + 2*s1 + 2*t2)) + 
            Power(s2,3)*(Power(t1,6) + t2 + 
               Power(t1,5)*(-14 - 7*s1 + 4*t2) - 
               t1*(-2 + s1 + 2*t2 + s1*t2 + Power(t2,2)) + 
               Power(t1,4)*(26 + 3*Power(s1,2) + s1*(21 - 6*t2) - 
                  4*t2 + 5*Power(t2,2)) - 
               Power(t1,3)*(12 + Power(s1,2) - s1*(-22 + t2) + 4*t2 + 
                  6*Power(t2,2)) + 
               Power(t1,2)*(-3 + 2*Power(s1,2) + s1*(9 - 2*t2) + 5*t2 + 
                  6*Power(t2,2))) - 
            Power(s2,2)*(2*Power(t1,7) + 
               Power(t1,5)*(36 + 5*Power(s1,2) + s1*(39 - 8*t2) - 
                  14*t2) + Power(t2,2) + 
               Power(t1,6)*(-12 - 11*s1 + 5*t2) + 
               t1*(1 + (-3 + 2*s1)*t2 - 8*Power(t2,2)) + 
               Power(t1,3)*(37 + 2*Power(s1,2) - 29*t2 - 
                  15*Power(t2,2) + s1*(-1 + 2*t2)) + 
               Power(t1,2)*(-11 + 29*t2 + 16*Power(t2,2) - 
                  s1*(3 + 5*t2)) + 
               Power(t1,4)*(-53 + 5*Power(s1,2) + 12*t2 + 
                  18*Power(t2,2) - 3*s1*(8 + 5*t2))) + 
            s2*t1*(Power(t1,7) + 2*t2*(-1 + 2*t2) + 
               Power(t1,6)*(-8 - 9*s1 + 4*t2) + 
               t1*(-13 + 4*s1 + 5*t2 - 15*Power(t2,2)) + 
               Power(t1,5)*(54 + Power(s1,2) + s1*(35 - 2*t2) - 40*t2 + 
                  5*Power(t2,2)) + 
               Power(t1,3)*(34 - 2*Power(s1,2) + s1*(11 - 14*t2) - 
                  87*t2 + 23*Power(t2,2)) + 
               Power(t1,2)*(23 + 40*t2 + 4*Power(t2,2) + 
                  s1*(-19 + 9*t2)) + 
               Power(t1,4)*(-91 + 13*Power(s1,2) + 80*t2 - 
                  9*Power(t2,2) - s1*(22 + 17*t2))) + 
            Power(t1,2)*(6 + Power(s1,2)*Power(t1,3)*
                (2 - 7*t1 + Power(t1,2)) + Power(t1,6)*(5 - 3*t2) - 
               10*t2 + t1*(19 - 7*t2)*t2 + 3*Power(t2,2) + 
               2*Power(t1,5)*(-11 + 6*t2) + 
               Power(t1,3)*(46 - 21*t2 - 8*Power(t2,2)) + 
               Power(t1,4)*(6 + 4*t2 - Power(t2,2)) + 
               Power(t1,2)*(-41 - t2 + 9*Power(t2,2)) + 
               s1*t1*(-13*Power(t1,4) + 3*Power(t1,5) + t1*(17 - 13*t2) + 
                  2*(-2 + t2) + Power(t1,3)*(14 + t2) + 
                  Power(t1,2)*(-17 + 18*t2)))))*R1q(t1))/
     (s*(-1 + s2)*Power(s2 - t1,3)*Power(-1 + t1,3)*t1*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*(-1 + t2)) + 
    (8*(3*Power(s,5)*t1*(-2 + t1 + t2) + 
         Power(s,4)*(4*Power(t1,3) + Power(t1,2)*(-9 + t2) - 
            4*Power(t2,2) + t1*
             (33 + 6*s1 - 27*t2 - 3*s1*t2 + 5*Power(t2,2)) - 
            s2*(6 + Power(t1,2) - 3*t2 + t1*(-13 - 3*s1 + 10*t2))) + 
         Power(s,3)*(-Power(t1,4) + 8*t2*(-1 + 2*t2) + 
            Power(t1,3)*(-3*s1 + 2*t2) + 
            Power(t1,2)*(22 - 3*Power(s1,2) - 38*t2 + 3*Power(t2,2) + 
               4*s1*(2 + t2)) + 
            t1*(-70 + 6*Power(s1,2) + 41*t2 - 14*Power(t2,2) + 
               s1*(-15 + 2*t2)) + 
            Power(s2,2)*(4 - 7*Power(t1,2) + s1*(3 + t1) - 7*t2 + 
               t1*(6 + 8*t2)) - 
            s2*(-27 + 3*Power(s1,2)*t1 + Power(t1,3) + 
               Power(t1,2)*(13 - 12*t2) + 16*t2 - 19*Power(t2,2) + 
               s1*(-6 + 4*Power(t1,2) + t1*(16 - 17*t2) + 11*t2) + 
               t1*(15 - 58*t2 + 20*Power(t2,2)))) - 
         Power(-1 + s2,2)*(-((-1 + s1)*Power(t1,4)) + 
            2*Power(s2,3)*(-1 + t1)*(1 + s1 - 2*t2) + 
            4*Power(-1 + t2,2) + 
            Power(t1,3)*(-21 + Power(s1,2) + s1*(7 - 3*t2) + 16*t2) + 
            t1*(-11 - 2*Power(s1,2) + s1*(25 - 7*t2) - 6*t2 + 
               Power(t2,2)) + 
            Power(t1,2)*(27 + 5*Power(s1,2) - 2*t2 - Power(t2,2) + 
               s1*(-31 + 2*t2)) + 
            Power(s2,2)*(-2 + Power(s1,2)*(1 + 3*t1) + 
               s1*(1 + 2*t1 - 3*Power(t1,2) - 8*t2) - t2 + 
               9*Power(t2,2) + Power(t1,2)*(-6 + 9*t2) + 
               t1*(8 - 8*t2 - 5*Power(t2,2))) + 
            s2*(-1 + Power(s1,2)*(2 - 6*t1 - 4*Power(t1,2)) + 
               Power(t1,3)*(3 - 5*t2) + 30*t2 - 13*Power(t2,2) + 
               Power(t1,2)*(11 - 4*t2 + Power(t2,2)) + 
               t1*(-13 - 21*t2 + 4*Power(t2,2)) + 
               s1*(-25 + 2*Power(t1,3) + 7*t2 + 6*t1*(5 + t2) + 
                  Power(t1,2)*(-7 + 3*t2)))) + 
         Power(s,2)*((1 + s1)*Power(t1,4) - 
            Power(t1,3)*(15 + s1 + Power(s1,2) + 16*t2 - 3*s1*t2) - 
            4*(1 - 6*t2 + 6*Power(t2,2)) + 
            Power(s2,3)*(8 + s1*(4 - 13*t1) + 5*Power(t1,2) + t2 + 
               3*t1*(-7 + 2*t2)) + 
            Power(t1,2)*(-76 + 9*Power(s1,2) + 79*t2 - 5*Power(t2,2) - 
               5*s1*(3 + 2*t2)) + 
            t1*(26 - 10*Power(s1,2) - 7*t2 + 12*Power(t2,2) + 
               3*s1*(1 + 4*t2)) + 
            Power(s2,2)*(12 + Power(s1,2)*(-7 + t1) - 4*Power(t1,3) + 
               Power(t1,2)*(47 - 36*t2) + 17*t2 - 35*Power(t2,2) + 
               s1*(1 + 15*Power(t1,2) + t1*(18 - 25*t2) + 34*t2) + 
               t1*(-39 - 23*t2 + 30*Power(t2,2))) + 
            s2*(-35 + 2*Power(t1,4) + 
               Power(s1,2)*(6 - 2*t1 + 4*Power(t1,2)) + 13*t2 - 
               17*Power(t2,2) + Power(t1,3)*(-9 + 5*t2) + 
               Power(t1,2)*(-12 + 62*t2 - 7*Power(t2,2)) + 
               t1*(34 + 51*t2 + 14*Power(t2,2)) - 
               s1*(17 + 2*Power(t1,3) - 15*t2 + t1*(85 + 6*t2) + 
                  Power(t1,2)*(-11 + 7*t2)))) + 
         s*((1 - 2*s1)*Power(t1,4) + 
            Power(s2,4)*(-8 + s1*(-9 + 11*t1) + t1*(10 - 11*t2) + 7*t2) + 
            Power(t1,3)*(-34 + 2*Power(s1,2) + s1*(15 - 6*t2) + 30*t2) + 
            Power(t1,2)*(35 - Power(s1,2) + 8*s1*(-1 + t2) - 44*t2 + 
               Power(t2,2)) + 8*(1 - 3*t2 + 2*Power(t2,2)) + 
            t1*(2*Power(s1,2) + s1*(31 - 18*t2) - 
               2*(-3 + 8*t2 + Power(t2,2))) - 
            Power(s2,2)*(-16 + Power(t1,4) + 
               Power(s1,2)*(-4 + 14*t1 + 5*Power(t1,2)) - 73*t2 + 
               22*Power(t2,2) + 2*Power(t1,3)*(-5 + 6*t2) + 
               t1*(80 + 93*t2 - 14*Power(t2,2)) + 
               Power(t1,2)*(-77 + 54*t2 - 5*Power(t2,2)) + 
               s1*(99 - 7*Power(t1,3) + Power(t1,2)*(4 - 6*t2) - 24*t2 + 
                  2*t1*(-77 + 3*t2))) + 
            s2*(-27 + 4*Power(t1,4) + 
               Power(s1,2)*(-4 + 3*t1 + 2*Power(t1,2) + 2*Power(t1,3)) + 
               46*t2 - 23*Power(t2,2) + Power(t1,3)*(-49 + 26*t2) + 
               Power(t1,2)*(43 + 78*t2 - 6*Power(t2,2)) + 
               t1*(-5 - 28*t2 + 8*Power(t2,2)) + 
               s1*(2 - 2*Power(t1,4) + Power(t1,3)*(14 - 6*t2) + 3*t2 + 
                  2*Power(t1,2)*(-61 + 3*t2) + t1*(26 + 9*t2))) + 
            Power(s2,3)*(-1 + Power(t1,3) + Power(s1,2)*(4 + 5*t1) - 
               2*t2 + 29*Power(t2,2) + Power(t1,2)*(-35 + 32*t2) + 
               t1*(29 - 8*t2 - 20*Power(t2,2)) + 
               s1*(18 - 14*Power(t1,2) - 31*t2 + t1*(-18 + 11*t2)))))*R2q(s)\
)/(s*(-1 + s2)*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2))*
       (-1 + t1)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*(-1 + t2)) - 
    (8*(Power(s,7)*t1*(3*Power(t1,2) + t1*(-2 + t2) + 2*t2) + 
         Power(s,6)*(7*Power(t1,4) - 
            Power(t1,3)*(14 + 2*s1 + 11*s2 - 2*t2) + 2*(s2 - t2)*t2 + 
            t1*(2 + s2*(-6 + 2*s1 - 7*t2) + (-12 + s1)*t2 - 
               Power(t2,2)) + 
            Power(t1,2)*(21 + s1 + 11*s2 + 3*s1*s2 - 7*t2 - s1*t2 - 
               6*s2*t2 + Power(t2,2))) + 
         Power(s,5)*(3*Power(t1,5) + 4*t2*(-1 + 3*t2) + 
            Power(t1,4)*(-26 - 7*s1 + 5*t2) + 
            t1*(-14 + s1*(5 - 3*t2) + 10*t2 + Power(t2,2)) + 
            Power(t1,3)*(35 - Power(s1,2) - 18*t2 + s1*(9 + t2)) + 
            Power(t1,2)*(-90 + 3*Power(s1,2) + 20*t2 - 6*Power(t2,2) + 
               s1*(2 + 3*t2)) + 
            Power(s2,2)*(-4 + 14*Power(t1,3) - 
               s1*(-2 + t1 + 11*Power(t1,2)) - 6*t2 + t1*(14 + 3*t2) + 
               Power(t1,2)*(-14 + 15*t2)) + 
            s2*(2 - 21*Power(t1,4) + Power(t1,3)*(32 + 13*s1 - 5*t2) - 
               3*(2 + s1)*t2 + 12*Power(t2,2) - 
               Power(t1,2)*(68 + 3*Power(s1,2) + s1*(5 - 9*t2) - 
                  31*t2 + 6*Power(t2,2)) + 
               t1*(45 + Power(s1,2) + 19*t2 + 7*Power(t2,2) - 
                  11*s1*(1 + t2)))) - 
         Power(-1 + s2,3)*(2*Power(s2,4)*(-1 + t1)*(s1 - t2) - 
            (-1 + s1)*Power(t1,4)*(2 + s1 - t2) - 
            2*t1*(-5 + s1 - 2*t2)*(-1 + t2) - 2*Power(-1 + t2,2) + 
            Power(t1,2)*(28 + 4*Power(s1,2) - 7*t2 - 5*Power(t2,2) + 
               s1*(-23 + 3*t2)) - 
            Power(t1,3)*(18 + 7*Power(s1,2) + 2*t2 + Power(t2,2) - 
               2*s1*(11 + 3*t2)) + 
            Power(s2,3)*(2 + Power(s1,2)*(-1 + 5*t1) + 
               s1*(1 + t1*(2 - 8*t2) + Power(t1,2)*(-3 + t2) - t2) - 
               3*t2 + 2*Power(t2,2) + 
               Power(t1,2)*(2 + t2 - Power(t2,2)) + 
               t1*(-4 + 2*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(-1 + Power(s1,2)*(4 - 5*t1 - 11*Power(t1,2)) + 
               19*t2 - 2*Power(t2,2) + 
               t1*(1 - 14*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(1 - 9*t2 + Power(t2,2)) - 
               Power(t1,3)*(1 - 4*t2 + Power(t2,2)) + 
               s1*(-2*Power(t1,3) + 9*Power(t1,2)*(1 + t2) - 
                  5*(3 + t2) + 4*t1*(2 + 5*t2))) + 
            s2*(Power(s1,2)*t1*(-8 + 13*t1 + 7*Power(t1,2)) + 
               Power(t1,4)*(-3 + t2) + 2*(8 - 9*t2 + Power(t2,2)) + 
               Power(t1,3)*(7 - 6*t2 + 2*Power(t2,2)) + 
               t1*(-31 - 4*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(11 + 27*t2 + 5*Power(t2,2)) - 
               s1*(2 + Power(t1,4)*(-3 + t2) - 2*t2 + 
                  2*Power(t1,3)*(4 + t2) - 2*t1*(19 + t2) + 
                  Power(t1,2)*(31 + 25*t2)))) + 
         Power(s,4)*(-2 + Power(t1,5)*(-3*s1 + 2*(-5 + t2)) + 
            Power(t1,4)*(19 + 28*s1 - 27*t2) + 20*t2 - 30*Power(t2,2) + 
            t1*(21 - 18*s1 + 46*t2 + 10*Power(t2,2)) + 
            Power(t1,2)*(141 - 16*Power(s1,2) - 57*t2 + 9*Power(t2,2) + 
               s1*(16 + t2)) + 
            Power(t1,3)*(-107 + 9*Power(s1,2) + 57*t2 - Power(t2,2) - 
               4*s1*(1 + 2*t2)) + 
            2*Power(s2,3)*(2 - 3*Power(t1,3) + 
               s1*(-1 - 5*t1 + 7*Power(t1,2)) + t2 + 2*t1*(1 + 5*t2) - 
               Power(t1,2)*(3 + 10*t2)) + 
            Power(s2,2)*(22 - 4*Power(t1,3) + 21*Power(t1,4) + 
               Power(s1,2)*(-1 - 13*t1 + 11*Power(t1,2)) - 7*t2 - 
               30*Power(t2,2) + t1*(-79 + 12*t2 - 21*Power(t2,2)) + 
               Power(t1,2)*(28 - 46*t2 + 15*Power(t2,2)) + 
               s1*(-6 - 26*Power(t1,3) + Power(t1,2)*(5 - 27*t2) + 
                  13*t2 + 5*t1*(8 + 9*t2))) + 
            s2*(-8 - 5*Power(t1,5) + 
               Power(s1,2)*t1*(9 + 4*t1 - 3*Power(t1,2)) + 
               Power(t1,4)*(36 - 13*t2) + 2*t2 - 40*Power(t2,2) + 
               Power(t1,3)*(-68 + 46*t2 + Power(t2,2)) + 
               t1*(-128 + 105*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(231 - 54*t2 + 20*Power(t2,2)) + 
               s1*(1 + 24*Power(t1,4) + Power(t1,3)*(-23 + t2) + 14*t2 + 
                  2*t1*(-8 + 5*t2) - Power(t1,2)*(108 + 25*t2)))) + 
         Power(s,2)*(-12 + 40*t2 - 30*Power(t2,2) + 
            Power(t1,5)*(26 - 15*s1 + 10*t2) + 
            Power(t1,3)*(13 + 30*Power(s1,2) + 2*s1*(-62 + t2) + 
               91*t2 - 6*Power(t2,2)) - 
            Power(t1,2)*(34 + 12*Power(s1,2) + 113*t2 + 
               21*Power(t2,2) - 21*s1*(2 + t2)) + 
            Power(t1,4)*(6 - 15*Power(s1,2) - 28*t2 + s1*(27 + 7*t2)) + 
            t1*(-49 + 104*t2 + 35*Power(t2,2) - s1*(8 + 15*t2)) + 
            Power(s2,5)*(Power(t1,3) - s1*(-8 + 14*t1 + Power(t1,2)) - 
               6*(2 + 3*t2) - Power(t1,2)*(13 + 6*t2) + t1*(26 + 33*t2)) \
+ s2*(34 + Power(s1,2)*t1*(43 - 101*t1 + 45*Power(t1,2) + 
                  7*Power(t1,3)) + Power(t1,4)*(47 - 15*t2) - 102*t2 + 
               Power(t1,5)*(-9 + 8*t2) + 
               Power(t1,2)*(71 - 152*t2 - 24*Power(t2,2)) + 
               2*Power(t1,3)*(-22 + 12*t2 + Power(t2,2)) + 
               t1*(27 + 144*t2 + 38*Power(t2,2)) - 
               s1*(3 + 8*Power(t1,5) + Power(t1,3)*(-49 + t2) - 24*t2 + 
                  2*Power(t1,4)*(19 + t2) - Power(t1,2)*(173 + 15*t2) + 
                  2*t1*(67 + 17*t2))) + 
            Power(s2,3)*(5 + Power(t1,5) + 
               Power(s1,2)*(-11 + 17*t1 + 65*Power(t1,2) - 
                  17*Power(t1,3)) + Power(t1,4)*(-16 + t2) - 84*t2 + 
               4*Power(t1,2)*(30 + 13*t2) + 
               Power(t1,3)*(25 + 2*t2 + 6*Power(t2,2)) + 
               t1*(-87 + 209*t2 + 58*Power(t2,2)) + 
               s1*(-17 + 8*Power(t1,4) + t1*(78 - 84*t2) + 16*t2 + 
                  11*Power(t1,3)*(3 + t2) - 3*Power(t1,2)*(98 + 19*t2))) \
+ Power(s2,4)*(8 + Power(s1,2)*(2 - 38*t1 + 9*Power(t1,2)) + 
               Power(t1,3)*(26 - 10*t2) - 10*t2 - 30*Power(t2,2) + 
               t1*(21 - 28*t2 - 31*Power(t2,2)) + 
               Power(t1,2)*(-69 + 2*t2 + 15*Power(t2,2)) - 
               s1*(27 + 4*Power(t1,3) - 14*t2 + 
                  Power(t1,2)*(1 + 27*t2) - 2*t1*(40 + 43*t2))) + 
            Power(s2,2)*(33 + 
               Power(s1,2)*(-23 + 58*t1 - 25*Power(t1,2) - 
                  42*Power(t1,3) + 8*Power(t1,4)) + 
               Power(t1,4)*(19 - 22*t2) - 2*Power(t1,5)*(-3 + t2) - 
               82*t2 - 4*Power(t2,2) + 
               Power(t1,2)*(165 - 167*t2 - 34*Power(t2,2)) + 
               Power(t1,3)*(-245 + 21*t2 - 2*Power(t2,2)) + 
               t1*(-90 + 98*t2 + 28*Power(t2,2)) + 
               s1*(63 - Power(t1,5) + t1*(70 - 49*t2) + 10*t2 - 
                  5*Power(t1,4)*(1 + t2) + Power(t1,3)*(254 + 20*t2) + 
                  Power(t1,2)*(-191 + 48*t2)))) + 
         Power(s,3)*(Power(t1,5)*(2 + 13*s1 - 10*t2) + 
            8*(1 - 5*t2 + 5*Power(t2,2)) + 
            Power(t1,2)*(-47 + 26*Power(s1,2) + 113*t2 + 
               4*Power(t2,2) - 7*s1*(9 + 2*t2)) + 
            Power(t1,4)*(13 + 3*Power(s1,2) + 42*t2 - s1*(25 + 3*t2)) + 
            t1*(11 - 114*t2 - 30*Power(t2,2) + 2*s1*(11 + 5*t2)) + 
            Power(t1,3)*(152 - 30*Power(s1,2) - 101*t2 + 
               4*Power(t2,2) + 2*s1*(25 + 6*t2)) - 
            Power(s2,4)*(Power(t1,3) + 
               2*s1*(2 - 9*t1 + 3*Power(t1,2)) - 4*(2 + 3*t2) + 
               8*t1*(4 + 5*t2) - Power(t1,2)*(22 + 15*t2)) + 
            s2*(2 + Power(s1,2)*t1*
                (-41 + 43*t1 + 7*Power(t1,2) - 4*Power(t1,3)) + 
               Power(t1,3)*(212 - 77*t2) + Power(t1,5)*(3 - 2*t2) + 
               40*t2 + 40*Power(t2,2) + Power(t1,4)*(-26 + 45*t2) + 
               Power(t1,2)*(-257 + 162*t2 + 4*Power(t2,2)) - 
               3*t1*(-54 + 87*t2 + 14*Power(t2,2)) + 
               s1*(-1 + 5*Power(t1,5) + Power(t1,3)*(-141 + t2) - 
                  5*Power(t1,2)*(-23 + t2) - 26*t2 + 
                  Power(t1,4)*(-41 + 3*t2) + 4*t1*(27 + 7*t2))) + 
            Power(s2,3)*(Power(s1,2)*(34 - 15*t1)*t1 - 7*Power(t1,4) + 
               2*Power(t1,3)*(-18 + 5*t2) + 
               Power(t1,2)*(74 + 23*t2 - 20*Power(t2,2)) + 
               8*(-2 + 4*t2 + 5*Power(t2,2)) + 
               t1*(13 - 16*t2 + 34*Power(t2,2)) + 
               s1*(15 + 20*Power(t1,3) - 20*t2 - 3*t1*(26 + 29*t2) + 
                  Power(t1,2)*(2 + 38*t2))) + 
            Power(s2,2)*(-49 + Power(t1,5) + 
               Power(s1,2)*(11 - 9*t1 - 40*Power(t1,2) + 
                  15*Power(t1,3)) + 80*t2 + 40*Power(t2,2) + 
               Power(t1,4)*(5 + 9*t2) + 
               t1*(152 - 249*t2 - 26*Power(t2,2)) + 
               Power(t1,2)*(-218 + 11*t2 - 20*Power(t2,2)) + 
               Power(t1,3)*(9 - 36*t2 - 4*Power(t2,2)) - 
               s1*(26*Power(t1,4) + t1*(96 - 13*t2) + 6*(3 + 5*t2) + 
                  Power(t1,3)*(1 + 9*t2) - Power(t1,2)*(292 + 57*t2)))) + 
         s*(8 + 39*t1 - 3*s1*t1 - 17*Power(t1,2) + 25*s1*Power(t1,2) - 
            5*Power(s1,2)*Power(t1,2) - 64*Power(t1,3) + 
            49*s1*Power(t1,3) - Power(s1,2)*Power(t1,3) + 
            31*Power(t1,4) - 38*s1*Power(t1,4) + 
            13*Power(s1,2)*Power(t1,4) + 3*Power(t1,5) + s1*Power(t1,5) - 
            20*t2 - 42*t1*t2 + 9*s1*t1*t2 + 50*Power(t1,2)*t2 - 
            13*s1*Power(t1,2)*t2 - 29*Power(t1,3)*t2 - 
            13*s1*Power(t1,3)*t2 + 9*Power(t1,4)*t2 - 
            5*s1*Power(t1,4)*t2 - 2*Power(t1,5)*t2 + 12*Power(t2,2) - 
            19*t1*Power(t2,2) + 18*Power(t1,2)*Power(t2,2) + 
            4*Power(t1,3)*Power(t2,2) + 
            Power(s2,6)*(4 + s1*(-6 + 7*t1 + Power(t1,2)) + 10*t2 + 
               Power(t1,2)*(2 + t2) - t1*(6 + 13*t2)) - 
            Power(s2,5)*(6 + Power(s1,2)*(2 - 21*t1 + 2*Power(t1,2)) + 
               Power(t1,3)*(4 - 3*t2) + 18*t2 - 12*Power(t2,2) + 
               2*Power(t1,2)*(-8 + t2 + 3*Power(t2,2)) - 
               3*t1*(-2 + 11*t2 + 5*Power(t2,2)) + 
               s1*(Power(t1,3) + Power(t1,2)*(5 - 9*t2) + 5*(-5 + t2) + 
                  7*t1*(5 + 6*t2))) + 
            Power(s2,2)*(85 + 
               Power(s1,2)*(9 - 55*t1 + 48*Power(t1,2) - 
                  21*Power(t1,3) - 7*Power(t1,4)) + 
               Power(t1,3)*(10 - 33*t2) + Power(t1,5)*(13 - 6*t2) - 
               70*t2 + 8*Power(t2,2) - 3*Power(t1,4)*(14 + t2) + 
               t1*(-23 + 8*t2 + 26*Power(t2,2)) - 
               Power(t1,2)*(43 + 134*t2 + 28*Power(t2,2)) + 
               s1*(-Power(t1,5) + t1*(131 - 17*t2) + 
                  Power(t1,4)*(49 + t2) + 6*(-8 + 3*t2) + 
                  Power(t1,2)*(28 + 5*t2) + Power(t1,3)*(79 + 13*t2))) + 
            s2*(-52 + Power(s1,2)*t1*
                (-4 + 56*t1 - 77*Power(t1,2) + 14*Power(t1,3)) + 
               Power(t1,4)*(35 - 9*t2) + 94*t2 - 20*Power(t2,2) + 
               Power(t1,5)*(-13 + 6*t2) + 
               Power(t1,2)*(209 - 29*t2 - 14*Power(t2,2)) + 
               Power(t1,3)*(-80 + 60*t2 - 8*Power(t2,2)) + 
               t1*(-99 + 22*t2 + 3*Power(t2,2)) + 
               s1*(5 + Power(t1,5) - t1*(-19 + t2) + 
                  3*Power(t1,4)*(-19 + t2) - 11*t2 + 
                  8*Power(t1,2)*(-31 + 5*t2) + Power(t1,3)*(136 + 19*t2))\
) + Power(s2,3)*(-28 + Power(s1,2)*t1*
                (7 + 42*t1 + 29*Power(t1,2) - 4*Power(t1,3)) - 68*t2 + 
               8*Power(t2,2) + Power(t1,5)*(-3 + 2*t2) + 
               Power(t1,4)*(-9 + 5*t2) + 
               Power(t1,3)*(72 - 11*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-29 + 155*t2 + 20*Power(t2,2)) + 
               t1*(-3 + 109*t2 + 30*Power(t2,2)) - 
               s1*(-58 + Power(t1,5) - Power(t1,4)*(13 + t2) + 
                  5*Power(t1,3)*(23 + 3*t2) + t1*(20 + 57*t2) + 
                  Power(t1,2)*(127 + 69*t2))) + 
            Power(s2,4)*(5 + Power(s1,2)*
                (9 - 33*t1 - 43*Power(t1,2) + 6*Power(t1,3)) + 
               Power(t1,4)*(1 - 2*t2) + 72*t2 - 20*Power(t2,2) + 
               t1*(34 - 117*t2 - 55*Power(t2,2)) + 
               Power(t1,3)*(2 + 10*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(-42 - 41*t2 + 10*Power(t2,2)) + 
               s1*(Power(t1,4) - 4*Power(t1,3)*(5 + t2) - 2*(33 + t2) + 
                  2*Power(t1,2)*(67 + 14*t2) + t1*(29 + 108*t2)))))*
       T2q(s2,s))/
     (s*(-1 + s2)*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2))*
       (-1 + t1)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(-1 + t2)) - 
    (8*(Power(s,4)*(s2 - t1)*t1*
          (Power(t1,4) + 2*Power(s2,2)*t2 - 
            Power(t1,3)*(-2 + 2*s2 + t2) + 
            Power(t1,2)*(-2 + Power(s2,2) + 4*t2 + s2*(-6 + 4*t2)) - 
            t1*(Power(s2,2)*(-2 + t2) + 2*(2 - 3*t2 + Power(t2,2)) + 
               2*s2*(2 - t2 + Power(t2,2)))) - 
         Power(s,3)*(Power(s2,4)*
             (2*Power(t1,3) - 2*t2 - Power(t1,2)*(1 + s1 + 3*t2) + 
               t1*(-2 - 2*s1 + 5*t2)) + 
            Power(t1,2)*(Power(t1,5) + 4*Power(-1 + t2,2) + 
               Power(t1,4)*(-7 - 2*s1 + 2*t2) + 
               Power(t1,2)*(4 - s1*(-2 + t2) + 4*t2 - 5*Power(t2,2)) + 
               Power(t1,3)*(-19 + s1*(-3 + t2) + 18*t2 - Power(t2,2)) + 
               t1*(34 + 4*s1*(-1 + t2) - 40*t2 + 4*Power(t2,2))) + 
            s2*Power(t1,2)*(-30 - 5*Power(t1,4) + 
               s1*(4 + 7*Power(t1,3) + Power(t1,2)*(13 - 5*t2) + 
                  t1*(-4 + t2) - 4*t2) + 28*t2 + 8*Power(t2,2) + 
               Power(t1,3)*(16 + 3*t2) + 
               Power(t1,2)*(21 - 23*t2 - 3*Power(t2,2)) + 
               t1*(-14 + 4*t2 + 11*Power(t2,2))) + 
            Power(s2,2)*t1*(9*Power(t1,4) - 
               3*Power(t1,3)*(4 + 3*s1 + 5*t2) - 
               2*(-4 + 6*t2 + Power(t2,2)) + 
               Power(t1,2)*(-21 + 3*s1*(-5 + t2) + 33*t2 + 
                  7*Power(t2,2)) + 
               t1*(-8 + 18*t2 - 23*Power(t2,2) + s1*(2 + t2))) + 
            Power(s2,3)*(-7*Power(t1,4) + 2*Power(t2,2) + 
               Power(t1,3)*(4 + 5*s1 + 13*t2) + 
               t1*(6 - s1*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(17 - 25*t2 - 7*Power(t2,2) + s1*(7 + t2)))) + 
         s*Power(s2 - t1,2)*(Power(t1,6)*(3 + s1 - 2*t2) - 
            4*t1*(-6 + s1 - t2)*(-1 + t2) - 4*Power(-1 + t2,2) + 
            Power(t1,5)*(-5 - 7*s1 + 2*t2) + 
            Power(t1,2)*(3 + s1 - 34*t2 + 5*s1*t2 + Power(t2,2)) + 
            Power(t1,3)*(35 + 3*Power(s1,2) + 19*t2 - 5*Power(t2,2) - 
               2*s1*(10 + t2)) + 
            Power(t1,4)*(-8 + 2*Power(s1,2) + 21*t2 + Power(t2,2) - 
               s1*(13 + t2)) + 
            Power(s2,4)*(s1*(2 - 5*t1 + Power(t1,2)) - 2*t2 + 
               Power(t1,2)*(2 + t2) + t1*(-2 + 3*t2)) + 
            Power(s2,3)*(-6 - 4*Power(t1,3) + 
               Power(s1,2)*(-1 - 6*t1 + 2*Power(t1,2)) + 11*t2 - 
               4*Power(t2,2) + 
               Power(t1,2)*(-8 + 5*t2 + 5*Power(t2,2)) - 
               2*t1*(-9 + 14*t2 + 5*Power(t2,2)) - 
               2*s1*(1 + Power(t1,3) + t1*(3 - 7*t2) - 2*t2 + 
                  Power(t1,2)*(-11 + 2*t2))) + 
            Power(s2,2)*(-8 + 
               Power(s1,2)*t1*(5 + 14*t1 - 4*Power(t1,2)) - 
               Power(t1,4)*(-1 + t2) - 26*t2 + 4*Power(t2,2) + 
               t1*(-11 + 55*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(-11 - 3*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(29 + 27*t2 + 4*Power(t2,2)) + 
               s1*(-1 + 2*Power(t1,4) + t1*(6 - 32*t2) + 
                  2*Power(t1,3)*(-15 + t2) + 7*t2 - 
                  Power(t1,2)*(29 + 7*t2))) + 
            s2*(Power(s1,2)*Power(t1,2)*(-7 - 10*t1 + 2*Power(t1,2)) + 
               2*Power(t1,5)*(-1 + t2) + 
               Power(t1,3)*(-51 + 6*t2 - 7*Power(t2,2)) + 
               4*(3 + t2 - 4*Power(t2,2)) + 
               Power(t1,4)*(38 - 31*t2 + 3*Power(t2,2)) - 
               Power(t1,2)*(50 + 21*t2 + 26*Power(t2,2)) + 
               t1*(53 - 36*t2 + 43*Power(t2,2)) - 
               2*s1*(2 + Power(t1,5) - 2*t2 + 6*t1*t2 - 
                  Power(t1,4)*(10 + t2) + Power(t1,3)*(-23 + 3*t2) - 
                  Power(t1,2)*(8 + 15*t2)))) + 
         Power(s2 - t1,3)*(2*Power(s2,4)*(-1 + t1)*(s1 - t2) - 
            (-1 + s1)*Power(t1,4)*(2 + s1 - t2) - 
            2*t1*(-5 + s1 - 2*t2)*(-1 + t2) - 2*Power(-1 + t2,2) + 
            Power(t1,2)*(28 + 4*Power(s1,2) - 7*t2 - 5*Power(t2,2) + 
               s1*(-23 + 3*t2)) - 
            Power(t1,3)*(18 + 7*Power(s1,2) + 2*t2 + Power(t2,2) - 
               2*s1*(11 + 3*t2)) + 
            Power(s2,3)*(2 + Power(s1,2)*(-1 + 5*t1) + 
               s1*(1 + t1*(2 - 8*t2) + Power(t1,2)*(-3 + t2) - t2) - 
               3*t2 + 2*Power(t2,2) + 
               Power(t1,2)*(2 + t2 - Power(t2,2)) + 
               t1*(-4 + 2*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(-1 + Power(s1,2)*(4 - 5*t1 - 11*Power(t1,2)) + 
               19*t2 - 2*Power(t2,2) + 
               t1*(1 - 14*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(1 - 9*t2 + Power(t2,2)) - 
               Power(t1,3)*(1 - 4*t2 + Power(t2,2)) + 
               s1*(-2*Power(t1,3) + 9*Power(t1,2)*(1 + t2) - 
                  5*(3 + t2) + 4*t1*(2 + 5*t2))) + 
            s2*(Power(s1,2)*t1*(-8 + 13*t1 + 7*Power(t1,2)) + 
               Power(t1,4)*(-3 + t2) + 2*(8 - 9*t2 + Power(t2,2)) + 
               Power(t1,3)*(7 - 6*t2 + 2*Power(t2,2)) + 
               t1*(-31 - 4*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(11 + 27*t2 + 5*Power(t2,2)) - 
               s1*(2 + Power(t1,4)*(-3 + t2) - 2*t2 + 
                  2*Power(t1,3)*(4 + t2) - 2*t1*(19 + t2) + 
                  Power(t1,2)*(31 + 25*t2)))) + 
         Power(s,2)*(s2 - t1)*
          (Power(s2,4)*(Power(t1,3) + s1*(2 + t1 - 2*Power(t1,2)) - 
               2*t2 + 2*t1*(1 + t2) - Power(t1,2)*(5 + 3*t2)) + 
            Power(s2,2)*(2*Power(t1,5) - 
               Power(t1,4)*(30 + 13*s1 + 2*t2) + 
               2*(-2 + t2 + 2*Power(t2,2)) + 
               t1*(-10 + s1 - 56*t2 + 10*s1*t2 + 10*Power(t2,2)) - 
               Power(t1,2)*(11 + 3*Power(s1,2) + 19*t2 + 
                  29*Power(t2,2) + s1*(-51 + 2*t2)) + 
               Power(t1,3)*(17 + 3*Power(s1,2) + 28*t2 + s1*(16 + 3*t2))) \
+ Power(s2,3)*(-2 - 2*Power(t1,4) - 3*(-2 + s1)*t2 + 4*Power(t2,2) + 
               Power(t1,3)*(17 + 9*s1 + 7*t2) - 
               Power(t1,2)*(-12 + Power(s1,2) + s1*(13 - 4*t2) + 
                  32*t2 + 9*Power(t2,2)) + 
               t1*(-13 + Power(s1,2) + 32*t2 + 12*Power(t2,2) - 
                  s1*(13 + 8*t2))) + 
            s2*t1*(-3*Power(s1,2)*(-1 + t1)*Power(t1,2) - 
               2*Power(t1,5) + Power(t1,4)*(29 - 5*t2) + 
               Power(t1,3)*(-32 + 16*t2 - 3*Power(t2,2)) - 
               4*(-9 + 3*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(-9 + 34*t2 + 8*Power(t2,2)) + 
               2*t1*(27 + t2 + 16*Power(t2,2)) + 
               s1*(7*Power(t1,4) + t1*(2 - 15*t2) + 8*(-1 + t2) - 
                  3*Power(t1,3)*(3 + 2*t2) + Power(t1,2)*(-55 + 12*t2))) \
+ t1*(Power(t1,6) - Power(t1,5)*(11 + s1 - 3*t2) - 8*Power(-1 + t2,2) + 
               Power(t1,4)*(-11 + Power(s1,2) - s1*(-5 + t2) + 10*t2) + 
               t1*(-52 - 8*s1*(-1 + t2) + 50*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(10 - 56*t2 + 6*Power(t2,2) + 
                  s1*(-3 + 8*t2)) - 
               Power(t1,3)*(Power(s1,2) + s1*(-15 + 2*t2) + 
                  3*(-7 + 7*t2 + Power(t2,2))))))*T3q(s2,t1))/
     (s*(-1 + s2)*Power(s2 - t1,2)*(-1 + t1)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(-1 + t2)) + 
    (8*(Power(s,4)*t1*(Power(t1,4) - Power(t1,3)*(-6 + t2) + 2*t2 + 
            t1*(10 + t2) + Power(t1,2)*(-25 + 6*t2)) + 
         Power(s,3)*(Power(t1,6) + 2*(s2 - t2)*t2 + 
            Power(t1,5)*(3 - 2*s1 - 2*s2 + 2*t2) + 
            Power(t1,3)*(85 + s2*(66 - 21*t2) + 
               s1*(24 + 10*s2 - 3*t2) - 20*t2 + 3*Power(t2,2)) + 
            Power(t1,4)*(-54 + 12*t2 - Power(t2,2) + 
               s1*(-3 + s2 + t2) + s2*(-7 + 3*t2)) - 
            Power(t1,2)*(37 - 5*s2*(-15 + t2) + 4*t2 - 5*Power(t2,2) + 
               s1*(3 + 5*s2 + 7*t2)) + 
            t1*(2 + (-6 + s1)*t2 + 3*Power(t2,2) + 
               s2*(2*s1 + 3*(6 + t2)))) - 
         Power(s,2)*(-Power(t1,7) + Power(t1,6)*(7 + s1 - 3*t2) + 
            2*(2 - 3*t2)*t2 + 
            Power(t1,5)*(16 + s1 - Power(s1,2) + s1*t2) + 
            t1*(8 - 5*s1 + 6*t2 + 14*Power(t2,2)) + 
            Power(s2,2)*(-1 + t1)*
             (-Power(t1,4) + 
               s1*(2 - 5*t1 + 21*Power(t1,2) + 2*Power(t1,3)) + 
               Power(t1,2)*(45 - 23*t2) + 3*Power(t1,3)*t2 + 
               4*(2 + t2) - 4*t1*(13 + t2)) + 
            Power(t1,2)*(-33 + 9*Power(s1,2) - 5*t2 - 3*Power(t2,2) - 
               s1*(39 + 16*t2)) + 
            Power(t1,3)*(69 - 17*Power(s1,2) - 26*t2 - 10*Power(t2,2) + 
               s1*(92 + 29*t2)) + 
            Power(t1,4)*(Power(s1,2) + 2*s1*(-25 + t2) - 
               3*(22 - 8*t2 + Power(t2,2))) + 
            s2*(-2 + 3*s1*t2 - 6*Power(t2,2) + 
               Power(t1,5)*(-1 - 5*s1 + 5*t2) + 
               Power(t1,3)*(182 + 7*Power(s1,2) + s1*(62 - 16*t2) - 
                  41*t2 + 12*Power(t2,2)) + 
               Power(t1,4)*(-70 + Power(s1,2) + 14*t2 - 3*Power(t2,2) + 
                  s1*(-9 + 2*t2)) + 
               t1*(43 - Power(s1,2) + 18*t2 + s1*(13 + 10*t2)) + 
               Power(t1,2)*(-152 + Power(s1,2) + 4*t2 + 5*Power(t2,2) - 
                  s1*(61 + 15*t2)))) + 
         Power(-1 + t1,2)*(-2*Power(s2,4)*(-1 + t1)*(s1 - t2) + 
            (-1 + s1)*Power(t1,4)*(2 + s1 - t2) + 
            2*t1*(-5 + s1 - 2*t2)*(-1 + t2) + 2*Power(-1 + t2,2) + 
            Power(t1,2)*(-28 - 4*Power(s1,2) + s1*(23 - 3*t2) + 7*t2 + 
               5*Power(t2,2)) + 
            Power(t1,3)*(18 + 7*Power(s1,2) + 2*t2 + Power(t2,2) - 
               2*s1*(11 + 3*t2)) - 
            Power(s2,3)*(2 + Power(s1,2)*(-1 + 5*t1) + 
               s1*(1 + t1*(2 - 8*t2) + Power(t1,2)*(-3 + t2) - t2) - 
               3*t2 + 2*Power(t2,2) + 
               Power(t1,2)*(2 + t2 - Power(t2,2)) + 
               t1*(-4 + 2*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(1 + Power(s1,2)*(-4 + 5*t1 + 11*Power(t1,2)) - 
               19*t2 + 2*Power(t2,2) - 
               Power(t1,2)*(1 - 9*t2 + Power(t2,2)) + 
               Power(t1,3)*(1 - 4*t2 + Power(t2,2)) + 
               t1*(-1 + 14*t2 + 10*Power(t2,2)) + 
               s1*(2*Power(t1,3) - 9*Power(t1,2)*(1 + t2) + 
                  5*(3 + t2) - 4*t1*(2 + 5*t2))) - 
            s2*(Power(s1,2)*t1*(-8 + 13*t1 + 7*Power(t1,2)) + 
               Power(t1,4)*(-3 + t2) + 2*(8 - 9*t2 + Power(t2,2)) + 
               Power(t1,3)*(7 - 6*t2 + 2*Power(t2,2)) + 
               t1*(-31 - 4*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(11 + 27*t2 + 5*Power(t2,2)) - 
               s1*(2 + Power(t1,4)*(-3 + t2) - 2*t2 + 
                  2*Power(t1,3)*(4 + t2) - 2*t1*(19 + t2) + 
                  Power(t1,2)*(31 + 25*t2)))) + 
         s*(-1 + t1)*(2 + 2*Power(t1,5)*(5 + 2*s1 - 3*t2) - 
            Power(t1,6)*(3 + s1 - 2*t2) - 8*t2 + 6*Power(t2,2) + 
            Power(t1,4)*(-13 + 2*Power(s1,2) + s1*(32 - 3*t2) - 11*t2 + 
               Power(t2,2)) + 
            t1*(7 - 16*t2 - 13*Power(t2,2) + 3*s1*(1 + t2)) + 
            Power(s2,3)*(-1 + t1)*
             (t1*(10 + 13*s1 - 11*t2) - 4*(3 + t2) + 
               Power(t1,2)*(2 + s1 + t2)) + 
            Power(t1,2)*(-22 - 13*Power(s1,2) + 17*t2 + 10*Power(t2,2) + 
               s1*(53 + 5*t2)) + 
            Power(t1,3)*(19 + 27*Power(s1,2) + 22*t2 + 12*Power(t2,2) - 
               s1*(91 + 37*t2)) + 
            Power(s2,2)*(8 + Power(s1,2)*
                (1 + t1 + 12*Power(t1,2) + 2*Power(t1,3)) + 11*t2 + 
               2*Power(t2,2) + Power(t1,4)*(-4 + 3*t2) + 
               Power(t1,3)*(-16 + 3*t2 - 3*Power(t2,2)) + 
               t1*(-40 - 11*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(52 - 6*t2 + 11*Power(t2,2)) - 
               s1*(-4 + Power(t1,4) - 2*Power(t1,3)*(-6 + t2) + 
                  12*t1*(2 + t2) + 11*Power(t1,2)*(-3 + 2*t2))) - 
            s2*(2*Power(s1,2)*t1*
                (-6 + 14*t1 + 7*Power(t1,2) + Power(t1,3)) + 
               Power(t1,5)*(-1 + 2*t2) + 
               Power(t1,4)*(-12 - 5*t2 + Power(t2,2)) - 
               2*(1 + 8*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(30 - 16*t2 + 5*Power(t2,2)) + 
               t1*(7 + 6*t2 + 13*Power(t2,2)) + 
               Power(t1,2)*(-22 + 29*t2 + 17*Power(t2,2)) + 
               s1*(1 - Power(t1,5) + Power(t1,4)*(10 - 4*t2) - 
                  9*Power(t1,3)*(-4 + t2) + 5*t2 + t1*(57 + 5*t2) - 
                  Power(t1,2)*(103 + 61*t2)))))*T4q(t1))/
     (s*(-1 + s2)*Power(-1 + t1,2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(-1 + t2)) + 
    (8*(Power(s,5)*t1*(Power(t1,2) - t1*(-2 + t2) + 2*t2) - 
         2*Power(s2 - t1,2)*Power(-1 + t1,2)*
          (-7 + 6*t1 + Power(t1,2) + 4*t2 - Power(s2,2)*t2 - 12*t1*t2 + 
            3*Power(t2,2) + 3*s2*
             (1 + t1*(-1 + t2) + 2*t2 - Power(t2,2)) + 
            s1*(4 + Power(s2,2) - Power(t1,2) + 3*s2*(-3 + t2) - 4*t2 + 
               t1*(5 + t2))) + 
         Power(s,4)*(Power(t1,4) - Power(t1,3)*(7 + 2*s1 + 2*s2 - 2*t2) + 
            2*(s2 - t2)*t2 + t1*
             (2 + s2*(2 + 2*s1 - 5*t2) + (-6 + s1)*t2 - Power(t2,2)) + 
            Power(t1,2)*(-3 + s2 + 6*t2 + 3*s2*t2 - Power(t2,2) + 
               s1*(-3 + s2 + t2))) + 
         Power(s,3)*(Power(t1,5) + 
            Power(t1,3)*(29 + Power(s1,2) - s1*(-5 + t2) - 8*t2) - 
            Power(t1,4)*(13 + s1 - 3*t2) + 2*t2*(-2 + 3*t2) - 
            Power(t1,2)*(23 + Power(s1,2) - 3*t2 + 3*Power(t2,2) + 
               s1*(-15 + 2*t2)) + 
            Power(s2,2)*(Power(t1,3) + s1*(2 + t1 - 2*Power(t1,2)) - 
               2*t2 + 2*t1*(1 + t2) - Power(t1,2)*(5 + 3*t2)) + 
            t1*(5*s1 - 2*(4 + 7*t2 + Power(t2,2))) + 
            s2*(2 + Power(t1,3)*(11 + 5*s1 - 5*t2) - 3*s1*t2 + 
               6*Power(t2,2) - 
               Power(t1,2)*(20 + Power(s1,2) - 6*t2 - 3*Power(t2,2) + 
                  s1*(5 + 2*t2)) + 
               t1*(5 + Power(s1,2) + 14*t2 - s1*(13 + 4*t2)))) + 
         Power(s,2)*(-2 + Power(t1,4)*(47 + 9*s1 - 10*t2) - 
            Power(t1,5)*(7 + s1 - 2*t2) + 8*t2 - 6*Power(t2,2) + 
            t1*(-9 + 24*t2 + 7*Power(t2,2) - 3*s1*(1 + t2)) + 
            Power(s2,3)*(s1*(2 - 5*t1 + Power(t1,2)) - 2*t2 + 
               Power(t1,2)*(2 + t2) + t1*(-2 + 3*t2)) - 
            Power(t1,3)*(78 + 2*Power(s1,2) - 45*t2 + 5*Power(t2,2) + 
               s1*(-9 + 7*t2)) + 
            Power(t1,2)*(49 - 3*Power(s1,2) - 87*t2 + Power(t2,2) + 
               2*s1*(2 + 9*t2)) + 
            Power(s2,2)*(6 + Power(s1,2)*(-1 - 6*t1 + 2*Power(t1,2)) - 
               t2 + 3*Power(t1,3)*t2 - 8*Power(t2,2) + 
               Power(t1,2)*(18 - 22*t2 - 3*Power(t2,2)) + 
               2*t1*(-12 + 6*t2 + Power(t2,2)) + 
               s1*(-4 - Power(t1,3) + 6*t2 + Power(t1,2)*(9 + 2*t2) + 
                  t1*(4 + 6*t2))) + 
            s2*(-2*Power(s1,2)*t1*(-2 - 4*t1 + Power(t1,2)) + 
               Power(t1,4)*(1 - 2*t2) - 
               Power(t1,3)*(49 - 19*t2 + Power(t2,2)) - 
               2*(1 + 8*t2 + 2*Power(t2,2)) + 
               t1*(-31 + 66*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(81 - 43*t2 + 12*Power(t2,2)) + 
               s1*(1 + Power(t1,4) + Power(t1,2)*(3 - 17*t2) + 5*t2 + 
                  Power(t1,3)*(-19 + 4*t2) - 2*t1*(5 + 7*t2)))) + 
         s*(2*(7 + 2*s1)*Power(t1,5) - 2*Power(t1,6) + 
            2*Power(s2,4)*(-1 + t1)*(s1 - t2) + 
            2*t1*(-5 + s1 - 2*t2)*(-1 + t2) + 2*Power(-1 + t2,2) + 
            Power(t1,2)*(-60 + 4*Power(s1,2) + s1*(5 - 25*t2) + 71*t2 + 
               9*Power(t2,2)) - 
            Power(t1,4)*(56 + Power(s1,2) - 61*t2 + 2*Power(t2,2) + 
               7*s1*(3 + t2)) + 
            Power(t1,3)*(92 - 7*Power(s1,2) - 122*t2 - 9*Power(t2,2) + 
               2*s1*(7 + 19*t2)) + 
            Power(s2,2)*(-3 + 2*Power(t1,4) + 
               Power(s1,2)*(4 - 5*t1 - 11*Power(t1,2)) + 29*t2 - 
               6*Power(t2,2) - 
               s1*(19 - 52*t1 + 6*Power(t1,3) + 
                  Power(t1,2)*(27 - 25*t2) + t2) + 
               Power(t1,2)*(-23 + 41*t2 - 19*Power(t2,2)) + 
               Power(t1,3)*(7 + 4*t2 + Power(t2,2)) + 
               t1*(17 - 74*t2 + 12*Power(t2,2))) + 
            Power(s2,3)*(-6 - 2*Power(t1,3) + Power(s1,2)*(-1 + 5*t1) + 
               5*t2 + 4*Power(t2,2) + 
               Power(t1,2)*(-2 + 5*t2 + Power(t2,2)) - 
               t1*(-10 + 10*t2 + Power(t2,2)) - 
               s1*(Power(t1,2)*(-3 + t2) + 3*(-1 + t2) + t1*(6 + 4*t2))) + 
            s2*(2*Power(t1,5) + 
               Power(s1,2)*t1*(-8 + 13*t1 + 7*Power(t1,2)) - 
               Power(t1,4)*(17 + 11*t2) + t1*(67 - 108*t2 + Power(t2,2)) - 
               3*Power(t1,2)*(33 - 61*t2 + Power(t2,2)) - 
               2*(8 - 9*t2 + Power(t2,2)) + 
               Power(t1,3)*(63 - 82*t2 + 16*Power(t2,2)) + 
               s1*(2 - 14*Power(t1,3)*(-4 + t2) + Power(t1,4)*(-3 + t2) - 
                  2*t2 + 2*t1*(7 + 13*t2) - Power(t1,2)*(69 + 35*t2)))))*
       T5q(s))/(s*(-1 + s2)*(-1 + t1)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(-1 + t2)));
   return a;
};
