#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m312_6_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(-2*Power(s,10)*s2*t1*(t1 + t2) + 
         Power(s,9)*(-11*s2*Power(t1,3) + 
            Power(t1,2)*(-2 + 12*Power(s2,2) + s2*(13 + s1 - 9*t2)) - 
            s2*t2*(2*s2 + Power(t2,2)) + 
            t1*(-2 - 2*Power(s2,2)*(1 + s1 - 7*t2) + 
               s2*(2 + s1*(-14 + t2) + 25*t2 + Power(t2,2)))) + 
         Power(s,8)*(2*t1*(7 + 2*t1 - 5*Power(t1,2)) - 
            2*Power(s2,3)*(s1 - 6*s1*t1 + 15*Power(t1,2) - 6*t2 + 
               t1*(-5 + 21*t2)) + 
            Power(s2,2)*(2 + Power(s1,2)*t1 + 59*Power(t1,3) + 23*t2 + 
               4*Power(t2,2) + 8*Power(t2,3) + 
               Power(t1,2)*(-72 + 50*t2) + 
               t1*(5 - 171*t2 - 11*Power(t2,2)) + 
               s1*(-14 - 7*Power(t1,2) + t2 - 3*Power(t2,2) + 
                  t1*(106 + 3*t2))) + 
            s2*(-2 - 25*Power(t1,4) + Power(t1,3)*(65 + 9*s1 - 21*t2) - 
               3*Power(t2,2) + 7*Power(t2,3) - 
               Power(t1,2)*(38 + Power(s1,2) - 4*s1*(-25 + t2) - 
                  161*t2 - 5*Power(t2,2)) + 
               t1*(33 - 143*t2 + 17*Power(t2,2) - 11*Power(t2,3) + 
                  2*Power(s1,2)*(7 + t2) + s1*(73 - 31*t2 + Power(t2,2))))\
) - Power(-1 + s2,2)*s2*(-1 + t1)*
          (s2 + Power(s2,2) - 2*s2*t1 + (-1 + t1)*t1)*
          (3*(-1 + s1)*Power(t1,5)*(1 + s1 - 2*t2) + 
            Power(s2,4)*(-4 + 5*s1 + 4*t1 - 5*t2)*Power(s1 - t2,2) - 
            2*t1*(11*s1 + 7*(-4 + t2))*Power(-1 + t2,2) + 
            10*Power(-1 + t2,3) + 
            Power(t1,3)*(15 - 7*Power(s1,3) - 102*t2 + 127*Power(t2,2) + 
               6*Power(t2,3) + Power(s1,2)*(34 + 26*t2) + 
               s1*(52 - 106*t2 - 45*Power(t2,2))) + 
            Power(t1,4)*(15 - 2*Power(s1,3) + 9*t2 - 36*Power(t2,2) - 
               2*Power(s1,2)*(3 + t2) + s1*(-37 + 53*t2 + 6*Power(t2,2))) \
+ Power(t1,2)*(-73 + 4*Power(s1,3) + 183*t2 - 145*Power(t2,2) + 
               3*Power(t2,3) - Power(s1,2)*(31 + 9*t2) + 
               s1*(7 + 15*t2 + 46*Power(t2,2))) + 
            Power(s2,2)*(-1 + t1)*
             (-7 + Power(s1,3)*(-4 + t1) - 27*t2 + 73*Power(t2,2) - 
               7*Power(t2,3) + Power(t1,2)*(1 + 16*t2 - 4*Power(t2,2)) + 
               t1*(6 + 11*t2 - 56*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s1,2)*(31 + 13*Power(t1,2) + 9*t2 - 
                  t1*(31 + 5*t2)) - 
               s1*(-45 + 119*t2 - 6*Power(t2,2) + 
                  2*Power(t1,2)*(9 + 4*t2) + 
                  t1*(27 - 101*t2 + 5*Power(t2,2)))) - 
            Power(s2,3)*(4 + Power(s1,3)*(1 + 9*t1) + t2 + 
               9*Power(t2,2) - 4*Power(t2,3) + 
               Power(t1,2)*(4 + t2 + 6*Power(t2,2)) - 
               t1*(8 + 2*t2 + 15*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s1,2)*(14 + 11*Power(t1,2) - 6*t2 - 
                  t1*(25 + 24*t2)) + 
               s1*(-9 - 19*t2 + 9*Power(t2,2) - 
                  Power(t1,2)*(9 + 13*t2) + 
                  t1*(18 + 32*t2 + 21*Power(t2,2)))) + 
            s2*(Power(s1,3)*t1*(-8 + 13*t1 + 5*Power(t1,2)) - 
               2*Power(-1 + t2,2)*(13 + 8*t2) + 
               3*Power(t1,4)*(2 - 7*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(12 + 111*t2 - 217*Power(t2,2) - 
                  8*Power(t2,3)) + 
               4*t1*(9 - 30*t2 + 32*Power(t2,2) + 5*Power(t2,3)) - 
               Power(t1,3)*(28 + 6*t2 - 77*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s1,2)*t1*
                (62 - 9*Power(t1,3) + Power(t1,2)*(29 - 2*t2) + 18*t2 - 
                  2*t1*(41 + 23*t2)) + 
               s1*(22*Power(-1 + t2,2) + 9*Power(t1,4)*(1 + t2) + 
                  Power(t1,3)*(64 - 138*t2 + 5*Power(t2,2)) - 
                  2*t1*(-19 + 67*t2 + 20*Power(t2,2)) + 
                  Power(t1,2)*(-133 + 307*t2 + 43*Power(t2,2))))) + 
         Power(s,7)*(2*t1*(-19 + 13*t1 + 22*Power(t1,2) - 
               10*Power(t1,3)) + 
            10*Power(s2,4)*(s1 - 3*s1*t1 + 4*Power(t1,2) - 3*t2 + 
               t1*(-2 + 7*t2)) + 
            Power(s2,3)*(-130*Power(t1,3) + 
               Power(s1,2)*(1 + 3*t1 - 3*t2) - 
               3*Power(t1,2)*(-55 + 37*t2) + 
               t1*(-47 + 497*t2 + 53*Power(t2,2)) - 
               4*(1 + 33*t2 + 7*Power(t2,2) + 7*Power(t2,3)) + 
               s1*(89 + 16*Power(t1,2) + 10*t2 + 21*Power(t2,2) - 
                  5*t1*(65 + 11*t2))) + 
            Power(s2,2)*(35 + 2*Power(s1,3)*t1 + 118*Power(t1,4) - 
               114*t2 + 21*Power(t2,2) - 49*Power(t2,3) + 
               Power(t1,3)*(-281 + 85*t2) + 
               Power(t1,2)*(262 - 891*t2 - 53*Power(t2,2)) + 
               t1*(-200 + 893*t2 - 113*Power(t2,2) + 70*Power(t2,3)) + 
               Power(s1,2)*(15*Power(t1,2) + 2*(7 + t2) - 
                  t1*(121 + 23*t2)) + 
               s1*(59 - 28*Power(t1,3) - 36*t2 + 16*Power(t2,2) + 
                  Power(t1,2)*(585 + 14*t2) + 
                  t1*(-499 + 231*t2 - 18*Power(t2,2)))) + 
            s2*(12 - 30*Power(t1,5) + 
               2*Power(t1,4)*(67 + 15*s1 - 20*t2) - 3*t2 + 
               18*Power(t2,2) - 19*Power(t2,3) + 
               Power(t1,3)*(-207 - 8*Power(s1,2) + 470*t2 + 
                  10*Power(t2,2) + s1*(-306 + 11*t2)) + 
               Power(t1,2)*(269 + 2*Power(s1,3) - 902*t2 + 
                  136*Power(t2,2) - 40*Power(t2,3) + 
                  Power(s1,2)*(85 + 12*t2) - 
                  s1*(-479 + 192*t2 + Power(t2,2))) + 
               t1*(-303 + 14*Power(s1,3) + 553*t2 - 228*Power(t2,2) + 
                  44*Power(t2,3) - Power(s1,2)*(124 + 45*t2) + 
                  s1*(-122 + 199*t2 + 52*Power(t2,2))))) + 
         s*(2*Power(-1 + t1,3)*Power(t1,3)*(1 + t1) + 
            Power(s2,9)*Power(s1 - t2,2)*(-8 + s1 + 8*t1 - t2) + 
            Power(s2,8)*(8 + Power(s1,3)*(-31 + 32*t1) - 6*t2 - 
               6*Power(t1,3)*t2 + 47*Power(t2,2) + 25*Power(t2,3) + 
               Power(t1,2)*(8 + 6*t2 - 9*Power(t2,2)) - 
               2*t1*(8 - 3*t2 + 19*Power(t2,2) + 13*Power(t2,3)) + 
               Power(s1,2)*(57 + Power(t1,2) + 87*t2 - 
                  2*t1*(29 + 45*t2)) + 
               s1*(-10 + 6*Power(t1,3) - 96*t2 - 81*Power(t2,2) + 
                  2*Power(t1,2)*(-11 + 8*t2) + 
                  t1*(26 + 80*t2 + 84*Power(t2,2)))) + 
            Power(s2,6)*(25 + 
               Power(s1,3)*(5 + 51*t1 - 135*Power(t1,2) + 
                  104*Power(t1,3)) + Power(t1,5)*(7 - 21*t2) - 122*t2 + 
               169*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,4)*(88 + 11*t2 - 32*Power(t2,2)) + 
               Power(t1,2)*(75 - 715*t2 + 2059*Power(t2,2) - 
                  92*Power(t2,3)) + 
               Power(t1,3)*(-171 + 322*t2 - 897*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-24 + 525*t2 - 1299*Power(t2,2) + 61*Power(t2,3)) + 
               Power(s1,2)*(52 + 204*Power(t1,4) - 9*t2 - 
                  t1*(637 + 160*t2) - Power(t1,3)*(941 + 325*t2) + 
                  Power(t1,2)*(1322 + 419*t2)) + 
               s1*(39 + 20*Power(t1,5) - 158*t2 - 3*Power(t2,2) - 
                  Power(t1,4)*(153 + 130*t2) + 
                  Power(t1,2)*(760 - 3562*t2 - 119*Power(t2,2)) + 
                  t1*(-495 + 1953*t2 + 19*Power(t2,2)) + 
                  Power(t1,3)*(-171 + 1897*t2 + 178*Power(t2,2)))) - 
            Power(s2,7)*(-8 + 
               Power(s1,3)*(8 - 101*t1 + 108*Power(t1,2)) + 
               Power(t1,4)*(3 - 21*t2) + 44*t2 - 179*Power(t2,2) + 
               22*Power(t2,3) + 
               Power(t1,3)*(43 + 13*t2 + 6*Power(t2,2)) + 
               t1*(65 - 117*t2 + 484*Power(t2,2) + 25*Power(t2,3)) - 
               Power(t1,2)*(103 - 81*t2 + 311*Power(t2,2) + 
                  62*Power(t2,3)) + 
               Power(s1,2)*(88*Power(t1,3) - 13*(10 + t2) - 
                  3*Power(t1,2)*(142 + 99*t2) + t1*(468 + 265*t2)) + 
               s1*(-49 + 20*Power(t1,4) + 328*t2 - 25*Power(t2,2) - 
                  Power(t1,3)*(101 + 53*t2) + 
                  t1*(37 - 949*t2 - 173*Power(t2,2)) + 
                  Power(t1,2)*(93 + 674*t2 + 243*Power(t2,2)))) + 
            Power(s2,5)*(136 + 
               Power(s1,3)*(9 - 31*t1 - 123*Power(t1,2) + 
                  135*Power(t1,3) + 5*Power(t1,4)) - 209*t2 + 
               Power(t2,2) + 28*Power(t2,3) - 3*Power(t1,6)*(1 + t2) + 
               Power(t1,5)*(-90 - 13*t2 + 90*Power(t2,2)) + 
               Power(t1,4)*(21 - 534*t2 + 1443*Power(t2,2) - 
                  103*Power(t2,3)) + 
               Power(t1,3)*(-21 + 1713*t2 - 3977*Power(t2,2) + 
                  67*Power(t2,3)) + 
               2*Power(t1,2)*
                (245 - 983*t2 + 1453*Power(t2,2) + 81*Power(t2,3)) - 
               t1*(533 - 1012*t2 + 463*Power(t2,2) + 149*Power(t2,3)) + 
               Power(s1,2)*(-220*Power(t1,5) - 4*(17 + t2) + 
                  6*t1*(8 + 3*t2) + Power(t1,4)*(983 + 142*t2) + 
                  3*Power(t1,2)*(337 + 165*t2) - 
                  2*Power(t1,3)*(877 + 318*t2)) + 
               s1*(-92 + 260*t2 - 65*Power(t2,2) + 
                  Power(t1,5)*(81 + 164*t2) + 
                  Power(t1,2)*(636 - 2924*t2 - 681*Power(t2,2)) + 
                  Power(t1,4)*(812 - 2768*t2 + 3*Power(t2,2)) + 
                  t1*(307 - 508*t2 + 307*Power(t2,2)) + 
                  Power(t1,3)*(-1744 + 5776*t2 + 421*Power(t2,2)))) + 
            s2*(-1 + t1)*(-2*Power(-1 + t2,3) + 
               2*Power(t1,7)*
                (-2 + 3*Power(s1,2) + s1*(2 - 6*t2) + 3*t2) - 
               t1*Power(-1 + t2,2)*(-133 + 34*s1 + 37*t2) + 
               Power(t1,5)*(22 - 434*t2 + 540*Power(t2,2) + 
                  6*Power(t2,3) + Power(s1,2)*(135 + 68*t2) + 
                  s1*(405 - 617*t2 - 131*Power(t2,2))) + 
               Power(t1,6)*(9 - 4*Power(s1,3) + 42*t2 - 
                  72*Power(t2,2) - 4*Power(s1,2)*(9 + t2) + 
                  s1*(-94 + 147*t2 + 12*Power(t2,2))) + 
               Power(t1,4)*(-359 + 58*Power(s1,3) + 1341*t2 - 
                  1351*Power(t2,2) - 34*Power(t2,3) - 
                  73*Power(s1,2)*(5 + 3*t2) + 
                  5*s1*(-83 + 183*t2 + 87*Power(t2,2))) + 
               Power(t1,2)*(-576 + 8*Power(s1,3) + 1249*t2 - 
                  806*Power(t2,2) + 63*Power(t2,3) - 
                  2*Power(s1,2)*(33 + 7*t2) + 
                  s1*(195 - 288*t2 + 229*Power(t2,2))) - 
               Power(t1,3)*(-773 + 52*Power(s1,3) + 1895*t2 - 
                  1476*Power(t2,2) + 6*Power(t2,3) - 
                  Power(s1,2)*(326 + 139*t2) + 
                  s1*(61 + 213*t2 + 481*Power(t2,2)))) + 
            Power(s2,3)*(38 + 
               Power(s1,3)*(-8 + 92*t1 - 111*Power(t1,2) - 
                  87*Power(t1,3) + 43*Power(t1,4) + 60*Power(t1,5) + 
                  30*Power(t1,6)) + Power(t1,8)*(2 - 6*t2) - 73*t2 + 
               56*Power(t2,2) + 45*Power(t2,3) + 
               3*Power(t1,7)*(3 - 18*t2 + 4*Power(t2,2)) + 
               Power(t1,5)*(305 + 144*t2 - 1423*Power(t2,2) - 
                  100*Power(t2,3)) + 
               t1*(195 + 232*t2 - 1008*Power(t2,2) - 49*Power(t2,3)) + 
               Power(t1,6)*(-278 + 139*t2 + 518*Power(t2,2) - 
                  18*Power(t2,3)) + 
               Power(t1,4)*(-40 - 615*t2 + 1416*Power(t2,2) + 
                  15*Power(t2,3)) - 
               Power(t1,2)*(722 + 401*t2 - 2364*Power(t2,2) + 
                  168*Power(t2,3)) + 
               Power(t1,3)*(491 + 634*t2 - 1935*Power(t2,2) + 
                  256*Power(t2,3)) + 
               Power(s1,2)*(66 - 44*Power(t1,7) + 14*t2 + 
                  4*Power(t1,4)*(83 + 15*t2) + 
                  Power(t1,6)*(133 + 16*t2) - 
                  Power(t1,3)*(472 + 35*t2) - t1*(622 + 245*t2) - 
                  Power(t1,5)*(446 + 313*t2) + 
                  Power(t1,2)*(1053 + 446*t2)) + 
               s1*(-35 + 4*Power(t1,8) - 32*t2 - 69*Power(t2,2) + 
                  Power(t1,7)*(6 + 71*t2) + 
                  Power(t1,4)*(196 - 1096*t2 - 313*Power(t2,2)) + 
                  Power(t1,3)*(-1813 + 3184*t2 - 195*Power(t2,2)) + 
                  Power(t1,6)*(303 - 788*t2 - 43*Power(t2,2)) + 
                  Power(t1,2)*(2220 - 4710*t2 + 17*Power(t2,2)) + 
                  t1*(-682 + 1921*t2 + 162*Power(t2,2)) + 
                  Power(t1,5)*(-199 + 1450*t2 + 498*Power(t2,2)))) - 
            Power(s2,2)*(Power(s1,3)*t1*
                (-16 + 136*t1 - 225*Power(t1,2) + 54*Power(t1,3) + 
                  50*Power(t1,4) + 2*Power(t1,5) + 4*Power(t1,6)) + 
               Power(t1,8)*(10 - 24*t2) + 
               Power(-1 + t2,2)*(-131 + 35*t2) + 
               3*Power(t1,7)*(-37 + 47*t2 + 20*Power(t2,2)) + 
               t1*(715 - 1535*t2 + 979*Power(t2,2) - 25*Power(t2,3)) - 
               Power(t1,6)*(-218 + 245*t2 + 213*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,4)*(365 + 1593*t2 - 3109*Power(t2,2) + 
                  53*Power(t2,3)) - 
               Power(t1,5)*(377 + 168*t2 - 987*Power(t2,2) + 
                  58*Power(t2,3)) - 
               Power(t1,2)*(1166 - 3145*t2 + 2809*Power(t2,2) + 
                  104*Power(t2,3)) + 
               Power(t1,3)*(477 - 3204*t2 + 4306*Power(t2,2) + 
                  106*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-6*Power(t1,7) + Power(t1,6)*(22 + 4*t2) + 
                  4*(33 + 7*t2) + Power(t1,4)*(359 + 31*t2) - 
                  Power(t1,5)*(143 + 40*t2) - 
                  2*Power(t1,3)*(531 + 224*t2) - t1*(899 + 352*t2) + 
                  Power(t1,2)*(1597 + 762*t2)) + 
               s1*(34*Power(-1 + t2,2) + 4*Power(t1,8)*(2 + 3*t2) + 
                  Power(t1,3)*(1961 - 4725*t2 - 1059*Power(t2,2)) + 
                  Power(t1,5)*(1011 - 1815*t2 - 26*Power(t2,2)) - 
                  2*Power(t1,7)*(-5 + 57*t2 + 6*Power(t2,2)) + 
                  Power(t1,6)*(-67 + 422*t2 + 70*Power(t2,2)) - 
                  4*t1*(66 - 81*t2 + 83*Power(t2,2)) + 
                  Power(t1,4)*(-2510 + 4664*t2 + 420*Power(t2,2)) + 
                  Power(t1,2)*(-183 + 1300*t2 + 920*Power(t2,2)))) + 
            Power(s2,4)*(-96 - 
               Power(s1,3)*(16 + 13*t1 - 109*Power(t1,2) - 
                  83*Power(t1,3) + 124*Power(t1,4) + 60*Power(t1,5)) + 
               9*t2 + 241*Power(t2,2) - 44*Power(t2,3) + 
               3*Power(t1,7)*(-1 + 5*t2) + 
               Power(t1,6)*(38 + 39*t2 - 63*Power(t2,2)) + 
               Power(t1,2)*(719 - 1562*t2 + 867*Power(t2,2) + 
                  32*Power(t2,3)) + 
               Power(t1,5)*(234 + 283*t2 - 1269*Power(t2,2) + 
                  82*Power(t2,3)) + 
               Power(t1,4)*(-97 - 1534*t2 + 3637*Power(t2,2) + 
                  113*Power(t2,3)) + 
               t1*(21 + 372*t2 - 616*Power(t2,2) + 128*Power(t2,3)) - 
               Power(t1,3)*(816 - 2378*t2 + 2797*Power(t2,2) + 
                  290*Power(t2,3)) + 
               Power(s1,2)*(133*Power(t1,6) + 23*(5 + 2*t2) - 
                  3*Power(t1,5)*(173 + 11*t2) - 
                  5*Power(t1,2)*(38 + 27*t2) - t1*(79 + 38*t2) + 
                  12*Power(t1,4)*(99 + 56*t2) - 
                  Power(t1,3)*(648 + 449*t2)) + 
               s1*(243 - 10*Power(t1,7) - 546*t2 + 48*Power(t2,2) - 
                  Power(t1,6)*(5 + 146*t2) + 
                  Power(t1,4)*(1261 - 4254*t2 - 834*Power(t2,2)) + 
                  Power(t1,2)*(-223 + 614*t2 - 255*Power(t2,2)) + 
                  t1*(-521 + 853*t2 - 95*Power(t2,2)) + 
                  Power(t1,5)*(-871 + 2135*t2 + 6*Power(t2,2)) + 
                  Power(t1,3)*(126 + 1344*t2 + 1067*Power(t2,2))))) - 
         Power(s,5)*(2*t1*(19 - 72*t1 + 30*Power(t1,2) + 
               69*Power(t1,3) - 47*Power(t1,4) + 5*Power(t1,5)) + 
            2*Power(s2,6)*(-6*Power(t1,2) + 5*s1*(-2 + 3*t1) + 
               t1*(5 - 21*t2) + 15*t2) + 
            Power(s2,5)*(-32 - 5*Power(s1,3) + 95*Power(t1,3) + 416*t2 + 
               160*Power(t2,2) + 70*Power(t2,3) + 
               5*Power(t1,2)*(-27 + 11*t2) + 
               Power(s1,2)*(46 - 110*t1 + 45*t2) - 
               5*t1*(-24 + 151*t2 + 47*Power(t2,2)) + 
               s1*(-294 + 15*Power(t1,2) - 220*t2 - 105*Power(t2,2) + 
                  t1*(496 + 405*t2))) + 
            Power(s2,3)*(-656 + 193*Power(t1,5) + 
               Power(s1,3)*(69 - 233*t1 + 33*Power(t1,2)) + 1252*t2 - 
               999*Power(t2,2) + 160*Power(t2,3) + 
               Power(t1,4)*(-601 + 107*t2) + 
               Power(t1,3)*(1747 - 3851*t2 - 382*Power(t2,2)) + 
               t1*(2313 - 5639*t2 + 3519*Power(t2,2) - 
                  467*Power(t2,3)) + 
               Power(t1,2)*(-2581 + 7647*t2 - 2007*Power(t2,2) + 
                  474*Power(t2,3)) + 
               Power(s1,2)*(-549 + 219*Power(t1,3) - 233*t2 - 
                  Power(t1,2)*(1813 + 369*t2) + t1*(2353 + 883*t2)) + 
               s1*(-265 - 41*Power(t1,4) + 1154*t2 + 138*Power(t2,2) + 
                  Power(t1,3)*(2438 + 153*t2) + 
                  t1*(2118 - 4927*t2 - 417*Power(t2,2)) + 
                  Power(t1,2)*(-4363 + 3534*t2 - 71*Power(t2,2)))) - 
            Power(s2,4)*(232 + 216*Power(t1,4) + 
               Power(s1,3)*(-19 + 80*t1) - 844*t2 + 277*Power(t2,2) - 
               205*Power(t2,3) - 2*Power(t1,3)*(221 + 10*t2) + 
               Power(t1,2)*(814 - 2528*t2 - 471*Power(t2,2)) + 
               t1*(-734 + 3072*t2 - 408*Power(t2,2) + 250*Power(t2,3)) + 
               Power(s1,2)*(305 + 15*Power(t1,2) + 55*t2 - 
                  4*t1*(181 + 60*t2)) + 
               s1*(450 + 122*Power(t1,3) - 560*t2 + 139*Power(t2,2) + 
                  4*Power(t1,2)*(395 + 143*t2) - 
                  2*t1*(935 - 511*t2 + 40*Power(t2,2)))) + 
            Power(s2,2)*(-693 - 68*Power(t1,6) + 
               Power(s1,3)*(46 - 281*t1 + 268*Power(t1,2) + 
                  52*Power(t1,3)) + Power(t1,5)*(374 - 188*t2) + 
               1107*t2 - 744*Power(t2,2) + 47*Power(t2,3) + 
               2*Power(t1,4)*(-726 + 1384*t2 + 61*Power(t2,2)) + 
               Power(t1,3)*(2938 - 8070*t2 + 2028*Power(t2,2) - 
                  332*Power(t2,3)) + 
               t1*(3357 - 6333*t2 + 4473*Power(t2,2) - 
                  192*Power(t2,3)) + 
               Power(t1,2)*(-4646 + 10812*t2 - 6079*Power(t2,2) + 
                  402*Power(t2,3)) + 
               Power(s1,2)*(-281 - 160*Power(t1,4) - 122*t2 + 
                  2*Power(t1,3)*(668 + 91*t2) + 3*t1*(692 + 299*t2) - 
                  Power(t1,2)*(3167 + 1190*t2)) + 
               s1*(64 + 150*Power(t1,5) + 302*t2 + 220*Power(t2,2) + 
                  2*Power(t1,4)*(-899 + 56*t2) + 
                  Power(t1,3)*(4630 - 3056*t2 - 32*Power(t2,2)) - 
                  5*t1*(-176 + 762*t2 + 217*Power(t2,2)) + 
                  2*Power(t1,2)*(-1922 + 3433*t2 + 551*Power(t2,2)))) + 
            s2*(-30 + 7*Power(t1,7) + 24*t2 - 30*Power(t2,2) + 
               5*Power(t2,3) + Power(t1,6)*(-89 - 45*s1 + 57*t2) + 
               Power(t1,5)*(415 + 40*Power(s1,2) + s1*(498 - 55*t2) - 
                  749*t2 - 5*Power(t2,2)) + 
               Power(t1,3)*(2660 - 76*Power(s1,3) - 6371*t2 + 
                  2870*Power(t2,2) - 148*Power(t2,3) + 
                  6*Power(s1,2)*(199 + 71*t2) + 
                  s1*(2316 - 2625*t2 - 592*Power(t2,2))) - 
               Power(t1,4)*(1095 + 20*Power(s1,3) - 3229*t2 + 
                  620*Power(t2,2) - 65*Power(t2,3) + 
                  4*Power(s1,2)*(81 + 10*t2) + 
                  s1*(1879 - 844*t2 - 50*Power(t2,2))) + 
               t1*(1819 - 100*Power(s1,3) - 2892*t2 + 1752*Power(t2,2) + 
                  25*Power(t2,3) + Power(s1,2)*(604 + 250*t2) - 
                  s1*(342 + 315*t2 + 610*Power(t2,2))) + 
               Power(t1,2)*(-3624 + 228*Power(s1,3) + 6602*t2 - 
                  4009*Power(t2,2) + 32*Power(t2,3) - 
                  2*Power(s1,2)*(806 + 353*t2) + 
                  s1*(-542 + 2320*t2 + 1205*Power(t2,2))))) - 
         Power(s,2)*(2*Power(-1 + t1,2)*Power(t1,2)*
             (2 - 7*t1 - 5*Power(t1,2) + 4*Power(t1,3)) + 
            Power(s2,8)*(4 + 5*Power(s1,3) + 
               Power(s1,2)*(-43 + 48*t1 - 18*t2) - 21*t2 + 
               4*Power(t1,2)*t2 - 48*Power(t2,2) - 8*Power(t2,3) + 
               t1*(-4 + 17*t2 + 53*Power(t2,2)) + 
               s1*(13 - 4*Power(t1,2) + 95*t2 + 21*Power(t2,2) - 
                  3*t1*(3 + 35*t2))) + 
            Power(s2,7)*(39 + 2*Power(t1,4) + 
               Power(s1,3)*(-76 + 92*t1) - 68*t2 + 181*Power(t2,2) + 
               36*Power(t2,3) - 2*Power(t1,3)*(1 + 25*t2) + 
               Power(t1,2)*(65 - 78*t2 - 133*Power(t2,2)) - 
               4*t1*(26 - 49*t2 + 22*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s1,2)*(212 - 46*Power(t1,2) + 202*t2 - 
                  2*t1*(103 + 119*t2)) + 
               2*s1*(3 + 25*Power(t1,3) - 187*t2 - 79*Power(t2,2) + 
                  Power(t1,2)*(-22 + 117*t2) + 
                  t1*(-6 + 110*t2 + 91*Power(t2,2)))) + 
            Power(s2,6)*(59 - 7*Power(t1,5) + 
               Power(s1,3)*(-91 + 293*t1 - 252*Power(t1,2)) - 177*t2 + 
               112*Power(t1,4)*t2 + 762*Power(t2,2) - 7*Power(t2,3) + 
               Power(t1,3)*(-264 + 299*t2 + 184*Power(t2,2)) + 
               t1*(-299 + 734*t2 - 2022*Power(t2,2) + 20*Power(t2,3)) + 
               Power(t1,2)*(511 - 968*t2 + 1137*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(s1,2)*(557 - 204*Power(t1,3) + 261*t2 - 
                  5*t1*(343 + 164*t2) + Power(t1,2)*(1423 + 702*t2)) - 
               s1*(-90 + 109*Power(t1,4) + 1309*t2 + 144*Power(t2,2) + 
                  3*Power(t1,3)*(-38 + 39*t2) + 
                  t1*(377 - 3740*t2 - 441*Power(t2,2)) + 
                  Power(t1,2)*(-282 + 2436*t2 + 433*Power(t2,2)))) + 
            Power(s2,5)*(339 + 9*Power(t1,6) + 
               Power(s1,3)*(-21 + 339*t1 - 488*Power(t1,2) + 
                  153*Power(t1,3)) - 682*t2 + 814*Power(t2,2) + 
               32*Power(t2,3) - Power(t1,5)*(3 + 55*t2) + 
               Power(t1,4)*(513 - 691*t2 - 260*Power(t2,2)) + 
               Power(t1,2)*(1013 - 3167*t2 + 7005*Power(t2,2) - 
                  168*Power(t2,3)) - 
               t1*(888 - 2234*t2 + 4520*Power(t2,2) + 85*Power(t2,3)) + 
               Power(t1,3)*(-983 + 2361*t2 - 2983*Power(t2,2) + 
                  206*Power(t2,3)) + 
               Power(s1,2)*(346 + 468*Power(t1,4) + 105*t2 - 
                  Power(t1,3)*(2682 + 659*t2) - t1*(2523 + 1156*t2) + 
                  Power(t1,2)*(4447 + 1729*t2)) + 
               s1*(6 + 59*Power(t1,5) - 798*t2 - 141*Power(t2,2) - 
                  2*Power(t1,4)*(-51 + 80*t2) + 
                  Power(t1,3)*(-1471 + 5872*t2 + 194*Power(t2,2)) - 
                  2*Power(t1,2)*(-915 + 5612*t2 + 479*Power(t2,2)) + 
                  t1*(-526 + 6198*t2 + 918*Power(t2,2)))) + 
            Power(s2,4)*(235 - 5*Power(t1,7) + 
               Power(s1,3)*(-30 + 108*t1 - 489*Power(t1,2) + 
                  448*Power(t1,3) + 84*Power(t1,4)) + 
               Power(t1,6)*(16 - 58*t2) - 799*t2 + 875*Power(t2,2) + 
               Power(t1,5)*(-491 + 751*t2 + 229*Power(t2,2)) + 
               Power(t1,4)*(653 - 2568*t2 + 3648*Power(t2,2) - 
                  314*Power(t2,3)) + 
               Power(t1,3)*(-1458 + 5718*t2 - 10802*Power(t2,2) + 
                  7*Power(t2,3)) - 
               t1*(1620 - 4170*t2 + 4432*Power(t2,2) + 
                  137*Power(t2,3)) + 
               Power(t1,2)*(2670 - 7214*t2 + 10273*Power(t2,2) + 
                  386*Power(t2,3)) + 
               Power(s1,2)*(274 - 419*Power(t1,5) + 120*t2 + 
                  12*Power(t1,4)*(195 + 23*t2) - 
                  9*Power(t1,3)*(595 + 243*t2) - t1*(1480 + 587*t2) + 
                  Power(t1,2)*(4431 + 2078*t2)) + 
               s1*(409 + 37*Power(t1,6) - 953*t2 - 114*Power(t2,2) + 
                  Power(t1,5)*(-354 + 327*t2) + 
                  Power(t1,4)*(2377 - 6536*t2 + Power(t2,2)) + 
                  t1*(-1021 + 4126*t2 + 840*Power(t2,2)) + 
                  2*Power(t1,3)*(-1557 + 7418*t2 + 951*Power(t2,2)) - 
                  2*Power(t1,2)*(-833 + 5691*t2 + 1196*Power(t2,2)))) + 
            Power(s2,3)*(210 + Power(t1,8) - 
               Power(s1,3)*(47 - 240*t1 + 314*Power(t1,2) - 
                  321*Power(t1,3) + 195*Power(t1,4) + 111*Power(t1,5)) - 
               651*t2 + 698*Power(t2,2) + 43*Power(t2,3) + 
               Power(t1,7)*(-13 + 59*t2) + 
               Power(t1,6)*(184 - 315*t2 - 79*Power(t2,2)) + 
               Power(t1,2)*(2283 - 8824*t2 + 10347*Power(t2,2) + 
                  2*Power(t2,3)) - 
               t1*(895 - 3777*t2 + 4635*Power(t2,2) + 25*Power(t2,3)) + 
               Power(t1,4)*(598 - 4628*t2 + 8134*Power(t2,2) + 
                  121*Power(t2,3)) + 
               Power(t1,5)*(176 + 1026*t2 - 2139*Power(t2,2) + 
                  144*Power(t2,3)) - 
               Power(t1,3)*(2544 - 9556*t2 + 12150*Power(t2,2) + 
                  223*Power(t2,3)) + 
               Power(s1,2)*(305 + 195*Power(t1,6) + 113*t2 - 
                  Power(t1,5)*(1072 + 87*t2) + 
                  3*Power(t1,2)*(1154 + 483*t2) + 
                  2*Power(t1,4)*(1703 + 719*t2) - t1*(1753 + 749*t2) - 
                  Power(t1,3)*(4367 + 1890*t2)) + 
               s1*(62 - 41*Power(t1,7) + Power(t1,6)*(228 - 252*t2) - 
                  492*t2 - 231*Power(t2,2) + 
                  Power(t1,4)*(3026 - 10198*t2 - 1763*Power(t2,2)) + 
                  2*Power(t1,5)*(-805 + 1825*t2 + 47*Power(t2,2)) - 
                  2*Power(t1,2)*(-2216 + 5475*t2 + 899*Power(t2,2)) + 
                  t1*(-1819 + 4880*t2 + 914*Power(t2,2)) + 
                  2*Power(t1,3)*(-2139 + 6505*t2 + 1277*Power(t2,2)))) + 
            s2*(Power(t1,8)*(2 + 3*Power(s1,2) + s1*(8 - 6*t2) - 6*t2) + 
               Power(-1 + t2,2)*(-5 + 11*t2) + 
               Power(t1,4)*(2577 - 249*Power(s1,3) - 7257*t2 + 
                  6202*Power(t2,2) + 121*Power(t2,3) + 
                  2*Power(s1,2)*(885 + 464*t2) + 
                  s1*(1544 - 3728*t2 - 1913*Power(t2,2))) + 
               Power(t1,2)*(2501 - 75*Power(s1,3) - 5181*t2 + 
                  3252*Power(t2,2) - 32*Power(t2,3) + 
                  2*Power(s1,2)*(252 + 83*t2) + 
                  s1*(-740 + 702*t2 - 1043*Power(t2,2))) + 
               Power(t1,6)*(189 + 17*Power(s1,3) - 897*t2 + 
                  626*Power(t2,2) - 6*Power(t2,3) + 
                  6*Power(s1,2)*(40 + 9*t2) + 
                  s1*(772 - 930*t2 - 115*Power(t2,2))) - 
               Power(t1,7)*(2*Power(s1,3) + 2*Power(s1,2)*(23 + t2) - 
                  3*s1*(-42 + 41*t2 + 2*Power(t2,2)) + 
                  2*(8 - 55*t2 + 18*Power(t2,2))) + 
               t1*(-610 + 4*Power(s1,3) + 1222*t2 - 676*Power(t2,2) + 
                  24*Power(t2,3) - 5*Power(s1,2)*(7 + t2) + 
                  s1*(202 - 331*t2 + 197*Power(t2,2))) + 
               Power(t1,5)*(-912 + 48*Power(s1,3) + 3434*t2 - 
                  2992*Power(t2,2) - 22*Power(t2,3) - 
                  Power(s1,2)*(877 + 415*t2) + 
                  s1*(-1868 + 2915*t2 + 761*Power(t2,2))) + 
               Power(t1,3)*(-3726 + 252*Power(s1,3) + 8554*t2 - 
                  6349*Power(t2,2) - 91*Power(t2,3) - 
                  Power(s1,2)*(1559 + 711*t2) + 
                  s1*(208 + 1255*t2 + 2092*Power(t2,2)))) + 
            Power(s2,2)*(501 + 
               Power(s1,3)*(-4 + 122*t1 - 462*Power(t1,2) + 
                  476*Power(t1,3) - 128*Power(t1,4) + Power(t1,5) + 
                  31*Power(t1,6)) + Power(t1,8)*(2 - 12*t2) - 1015*t2 + 
               577*Power(t2,2) - 27*Power(t2,3) + 
               Power(t1,7)*(-5 + 23*t2 + 6*Power(t2,2)) + 
               Power(t1,4)*(1484 - 7494*t2 + 8420*Power(t2,2) - 
                  24*Power(t2,3)) - 
               2*Power(t1,5)*(30 - 1120*t2 + 1538*Power(t2,2) + 
                  9*Power(t2,3)) - 
               Power(t1,6)*(245 + 128*t2 - 505*Power(t2,2) + 
                  18*Power(t2,3)) - 
               t1*(2253 - 4902*t2 + 3398*Power(t2,2) + 69*Power(t2,3)) - 
               Power(t1,3)*(3142 - 11634*t2 + 12366*Power(t2,2) + 
                  75*Power(t2,3)) + 
               Power(t1,2)*(3718 - 10150*t2 + 9283*Power(t2,2) + 
                  206*Power(t2,3)) + 
               Power(s1,2)*(-45*Power(t1,7) + 5*(7 + t2) + 
                  26*Power(t1,6)*(11 + t2) - 
                  13*Power(t1,5)*(95 + 32*t2) - t1*(809 + 279*t2) + 
                  2*Power(t1,2)*(1519 + 670*t2) + 
                  Power(t1,4)*(2779 + 1122*t2) - 
                  Power(t1,3)*(4098 + 1895*t2)) + 
               s1*(-174 + 8*Power(t1,8) + 275*t2 - 169*Power(t2,2) + 
                  Power(t1,7)*(-45 + 79*t2) + 
                  Power(t1,4)*(4976 - 9552*t2 - 1637*Power(t2,2)) + 
                  Power(t1,6)*(547 - 988*t2 - 65*Power(t2,2)) + 
                  2*t1*(279 + 15*t2 + 577*Power(t2,2)) + 
                  Power(t1,5)*(-2151 + 4166*t2 + 643*Power(t2,2)) - 
                  2*Power(t1,2)*(-701 + 2791*t2 + 1346*Power(t2,2)) + 
                  Power(t1,3)*(-5121 + 11670*t2 + 2852*Power(t2,2))))) + 
         Power(s,4)*(-2*t1*(-7 + 49*t1 - 63*Power(t1,2) - 
               28*Power(t1,3) + 68*Power(t1,4) - 22*Power(t1,5) + 
               Power(t1,6)) + 
            2*Power(s2,7)*(t1 - Power(t1,2) + s1*(-5 + 6*t1) + 6*t2 - 
               7*t1*t2) + Power(s2,6)*
             (-38 - 10*Power(s1,3) + 31*Power(t1,3) + 309*t2 + 
               180*Power(t2,2) + 56*Power(t2,3) - 
               6*Power(t1,2)*(8 + t2) + 
               Power(s1,2)*(94 - 155*t1 + 60*t2) + 
               t1*(77 - 425*t2 - 241*Power(t2,2)) + 
               s1*(-212 + 29*Power(t1,2) - 305*t2 - 105*Power(t2,2) + 
                  t1*(268 + 449*t2))) - 
            Power(s2,5)*(194 + 109*Power(t1,4) + 
               Power(s1,3)*(-61 + 145*t1) - 706*t2 + 413*Power(t2,2) - 
               150*Power(t2,3) - Power(t1,3)*(209 + 155*t2) + 
               Power(t1,2)*(600 - 1737*t2 - 575*Power(t2,2)) + 
               t1*(-621 + 2363*t2 - 325*Power(t2,2) + 170*Power(t2,3)) + 
               Power(s1,2)*(452 - 71*Power(t1,2) + 155*t2 - 
                  t1*(746 + 365*t2)) + 
               s1*(382 + 203*Power(t1,3) - 828*t2 + 61*Power(t2,2) + 
                  Power(t1,2)*(932 + 840*t2) + 
                  t1*(-1379 + 899*t2 + 30*Power(t2,2)))) + 
            Power(s2,4)*(-716 + 147*Power(t1,5) + 
               Power(s1,3)*(153 - 403*t1 + 172*Power(t1,2)) + 1411*t2 - 
               1733*Power(t2,2) + 107*Power(t2,3) - 
               4*Power(t1,4)*(94 + 23*t2) + 
               Power(t1,3)*(1646 - 3565*t2 - 634*Power(t2,2)) + 
               t1*(2150 - 5339*t2 + 4927*Power(t2,2) - 
                  481*Power(t2,3)) + 
               Power(t1,2)*(-2564 + 7231*t2 - 2751*Power(t2,2) + 
                  500*Power(t2,3)) + 
               Power(s1,2)*(-1095 + 342*Power(t1,3) - 509*t2 - 
                  Power(t1,2)*(2803 + 750*t2) + t1*(3614 + 1405*t2)) + 
               s1*(-281 + 137*Power(t1,4) + 2415*t2 + 311*Power(t2,2) + 
                  Power(t1,3)*(1982 + 413*t2) + 
                  t1*(2126 - 7755*t2 - 573*Power(t2,2)) + 
                  Power(t1,2)*(-4075 + 5273*t2 + 37*Power(t2,2)))) + 
            Power(s2,3)*(-1047 - 85*Power(t1,6) + 
               Power(s1,3)*(123 - 548*t1 + 598*Power(t1,2) + 
                  57*Power(t1,3)) + Power(t1,5)*(345 - 183*t2) + 
               2081*t2 - 1931*Power(t2,2) + 21*Power(t2,3) + 
               Power(t1,4)*(-1959 + 3867*t2 + 374*Power(t2,2)) + 
               Power(t1,3)*(3932 - 10568*t2 + 4255*Power(t2,2) - 
                  616*Power(t2,3)) - 
               6*t1*(-733 + 1555*t2 - 1473*Power(t2,2) + 
                  29*Power(t2,3)) + 
               Power(t1,2)*(-5943 + 14467*t2 - 11799*Power(t2,2) + 
                  539*Power(t2,3)) + 
               Power(s1,2)*(-827 - 422*Power(t1,4) - 383*t2 + 
                  6*t1*(733 + 325*t2) + Power(t1,3)*(3152 + 487*t2) - 
                  5*Power(t1,2)*(1351 + 524*t2)) + 
               s1*(-242 + 131*Power(t1,5) + 1674*t2 + 471*Power(t2,2) + 
                  2*Power(t1,4)*(-1169 + 80*t2) + 
                  t1*(2100 - 9845*t2 - 1844*Power(t2,2)) + 
                  Power(t1,3)*(6224 - 7149*t2 + 2*Power(t2,2)) + 
                  2*Power(t1,2)*(-2781 + 7790*t2 + 985*Power(t2,2)))) + 
            s2*(-16 - Power(t1,8) + Power(t1,7)*(29 + 21*s1 - 29*t2) + 
               6*t2 + 15*Power(t2,2) - 19*Power(t2,3) + 
               Power(t1,6)*(-213 - 35*Power(s1,2) + 401*t2 + 
                  Power(t2,2) + 4*s1*(-69 + 14*t2)) + 
               Power(t1,3)*(5115 - 326*Power(s1,3) - 11095*t2 + 
                  7284*Power(t2,2) + 20*Power(t2,3) + 
                  6*Power(s1,2)*(437 + 199*t2) + 
                  s1*(2095 - 4881*t2 - 2066*Power(t2,2))) + 
               t1*(2039 - 80*Power(s1,3) - 3533*t2 + 2007*Power(t2,2) + 
                  95*Power(t2,3) + 4*Power(s1,2)*(124 + 45*t2) + 
                  s1*(-581 + 259*t2 - 725*Power(t2,2))) + 
               Power(t1,5)*(776 + 20*Power(s1,3) - 2390*t2 + 
                  505*Power(t2,2) - 31*Power(t2,3) + 
                  Power(s1,2)*(286 + 30*t2) + 
                  s1*(1518 - 801*t2 - 55*Power(t2,2))) + 
               Power(t1,4)*(-2498 + 34*Power(s1,3) + 6857*t2 - 
                  3321*Power(t2,2) + 107*Power(t2,3) - 
                  Power(s1,2)*(1243 + 424*t2) + 
                  s1*(-3128 + 3346*t2 + 658*Power(t2,2))) + 
               Power(t1,2)*(-5267 + 342*Power(s1,3) + 9792*t2 - 
                  6447*Power(t2,2) - 178*Power(t2,3) - 
                  Power(s1,2)*(2125 + 944*t2) + 
                  2*s1*(205 + 968*t2 + 1086*Power(t2,2)))) + 
            Power(s2,2)*(-1034 + 19*Power(t1,7) + 
               Power(s1,3)*(54 - 411*t1 + 703*Power(t1,2) - 
                  296*Power(t1,3) - 98*Power(t1,4)) + 1815*t2 - 
               1101*Power(t2,2) - 42*Power(t2,3) + 
               2*Power(t1,6)*(-79 + 83*t2) + 
               Power(t1,5)*(1024 - 1989*t2 - 83*Power(t2,2)) + 
               Power(t1,3)*(6221 - 16167*t2 + 10509*Power(t2,2) - 
                  328*Power(t2,3)) + 
               Power(t1,2)*(-8052 + 17813*t2 - 13917*Power(t2,2) + 
                  67*Power(t2,3)) + 
               t1*(4761 - 9313*t2 + 7068*Power(t2,2) + 109*Power(t2,3)) + 
               Power(t1,4)*(-2626 + 7575*t2 - 2492*Power(t2,2) + 
                  268*Power(t2,3)) + 
               Power(s1,2)*(-323 + 195*Power(t1,5) - 128*t2 - 
                  2*Power(t1,4)*(751 + 84*t2) + 
                  2*Power(t1,3)*(2414 + 891*t2) + t1*(2692 + 1179*t2) - 
                  Power(t1,2)*(5799 + 2551*t2)) + 
               s1*(266 - 127*Power(t1,6) + Power(t1,5)*(1288 - 223*t2) + 
                  25*t2 + 405*Power(t2,2) + 
                  t1*(328 - 4083*t2 - 2075*Power(t2,2)) + 
                  Power(t1,3)*(6900 - 11981*t2 - 1962*Power(t2,2)) + 
                  Power(t1,4)*(-4771 + 3897*t2 + 123*Power(t2,2)) + 
                  Power(t1,2)*(-4094 + 12451*t2 + 3347*Power(t2,2))))) + 
         Power(s,3)*(2*t1*(-1 + 16*t1 - 41*Power(t1,2) + 
               12*Power(t1,3) + 38*Power(t1,4) - 28*Power(t1,5) + 
               4*Power(t1,6)) - 2*Power(s2,8)*(-1 + t1)*(s1 - t2) + 
            Power(s2,7)*(10*Power(s1,3) - 4*Power(t1,3) + 
               Power(s1,2)*(-91 + 119*t1 - 45*t2) + 
               Power(t1,2)*(7 + 15*t2) + 
               t1*(-27 + 131*t2 + 151*Power(t2,2)) - 
               4*(-5 + 31*t2 + 31*Power(t2,2) + 7*Power(t2,3)) + 
               s1*(-18*Power(t1,2) - t1*(77 + 293*t2) + 
                  9*(9 + 26*t2 + 7*Power(t2,2)))) + 
            Power(s2,6)*(101 + 26*Power(t1,4) + 
               2*Power(s1,3)*(-47 + 75*t1) - 308*t2 + 359*Power(t2,2) - 
               27*Power(t2,3) - Power(t1,3)*(45 + 139*t2) + 
               Power(t1,2)*(262 - 619*t2 - 395*Power(t2,2)) + 
               t1*(-328 + 1005*t2 - 177*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s1,2)*(-103*Power(t1,2) + 80*(5 + 3*t2) - 
                  t1*(481 + 367*t2)) + 
               s1*(149 + 148*Power(t1,3) - 732*t2 - 106*Power(t2,2) + 
                  Power(t1,2)*(201 + 646*t2) + 
                  t1*(-473 + 507*t2 + 174*Power(t2,2)))) - 
            Power(s2,5)*(-360 + 53*Power(t1,5) + 
               Power(s1,3)*(175 - 444*t1 + 297*Power(t1,2)) + 709*t2 - 
               1556*Power(t2,2) + 15*Power(t2,3) - 
               2*Power(t1,4)*(50 + 99*t2) + 
               Power(t1,3)*(883 - 1668*t2 - 526*Power(t2,2)) + 
               t1*(1063 - 2631*t2 + 4125*Power(t2,2) - 
                  234*Power(t2,3)) + 
               Power(t1,2)*(-1483 + 3722*t2 - 2274*Power(t2,2) + 
                  220*Power(t2,3)) + 
               Power(s1,2)*(-1093 + 321*Power(t1,3) - 546*t2 - 
                  2*Power(t1,2)*(1288 + 465*t2) + t1*(3251 + 1375*t2)) + 
               s1*(-114 + 207*Power(t1,4) + 2446*t2 + 347*Power(t2,2) + 
                  Power(t1,3)*(609 + 407*t2) + 
                  t1*(1131 - 7101*t2 - 614*Power(t2,2)) + 
                  Power(t1,2)*(-1895 + 4664*t2 + 323*Power(t2,2)))) + 
            Power(s2,4)*(780 + 46*Power(t1,6) + 
               Power(s1,3)*(-103 + 594*t1 - 741*Power(t1,2) + 
                  58*Power(t1,3)) - 1673*t2 + 1931*Power(t2,2) + 
               25*Power(t2,3) + Power(t1,5)*(-127 + 21*t2) + 
               Power(t1,4)*(1380 - 2559*t2 - 478*Power(t2,2)) + 
               Power(t1,2)*(3613 - 9089*t2 + 12116*Power(t2,2) - 
                  354*Power(t2,3)) - 
               t1*(2919 - 6378*t2 + 8656*Power(t2,2) + 65*Power(t2,3)) + 
               Power(t1,3)*(-2661 + 6872*t2 - 4803*Power(t2,2) + 
                  544*Power(t2,3)) + 
               Power(s1,2)*(853 + 590*Power(t1,4) + 375*t2 - 
                  Power(t1,3)*(3893 + 737*t2) - t1*(4661 + 2147*t2) + 
                  Power(t1,2)*(7439 + 2959*t2)) + 
               s1*(327 + 8*Power(t1,5) + Power(t1,4)*(1271 - 150*t2) - 
                  2104*t2 - 377*Power(t2,2) + 
                  Power(t1,2)*(3686 - 17824*t2 - 1939*Power(t2,2)) + 
                  Power(t1,3)*(-4139 + 8787*t2 + 54*Power(t2,2)) + 
                  t1*(-1351 + 11013*t2 + 1830*Power(t2,2)))) + 
            Power(s2,3)*(724 - 17*Power(t1,7) + 
               Power(s1,3)*(-95 + 457*t1 - 826*Power(t1,2) + 
                  523*Power(t1,3) + 151*Power(t1,4)) + 
               Power(t1,6)*(92 - 164*t2) - 1791*t2 + 1669*Power(t2,2) + 
               51*Power(t2,3) + 
               3*Power(t1,5)*(-332 + 650*t2 + 75*Power(t2,2)) + 
               Power(t1,4)*(1984 - 6291*t2 + 4243*Power(t2,2) - 
                  424*Power(t2,3)) - 
               t1*(3611 - 9072*t2 + 9164*Power(t2,2) + 
                  128*Power(t2,3)) + 
               Power(t1,2)*(6522 - 16690*t2 + 17783*Power(t2,2) + 
                  170*Power(t2,3)) + 
               Power(t1,3)*(-4798 + 13948*t2 - 15003*Power(t2,2) + 
                  173*Power(t2,3)) + 
               Power(s1,2)*(624 - 400*Power(t1,5) + 264*t2 - 
                  6*t1*(593 + 263*t2) + Power(t1,4)*(2669 + 294*t2) - 
                  Power(t1,3)*(7417 + 2900*t2) + 
                  Power(t1,2)*(7680 + 3413*t2)) + 
               s1*(225 + 119*Power(t1,6) - 1160*t2 - 458*Power(t2,2) + 
                  3*Power(t1,5)*(-389 + 123*t2) + 
                  Power(t1,2)*(5223 - 19424*t2 - 3798*Power(t2,2)) + 
                  Power(t1,4)*(4486 - 7184*t2 - 81*Power(t2,2)) + 
                  t1*(-2321 + 8611*t2 + 2027*Power(t2,2)) + 
                  Power(t1,3)*(-6383 + 19305*t2 + 2781*Power(t2,2)))) + 
            s2*(Power(t1,8)*(4 + 4*s1 - 6*t2) + 
               t2*(21 - 42*t2 + 23*Power(t2,2)) + 
               Power(t1,7)*(-48 - 16*Power(s1,2) + 100*t2 + 
                  s1*(-78 + 29*t2)) + 
               Power(t1,4)*(3361 - 208*Power(s1,3) - 8979*t2 + 
                  6567*Power(t2,2) + 37*Power(t2,3) + 
                  Power(s1,2)*(2119 + 998*t2) + 
                  s1*(2949 - 5224*t2 - 1764*Power(t2,2))) - 
               Power(t1,5)*(1146 + 14*Power(s1,3) - 3756*t2 + 
                  2046*Power(t2,2) - 40*Power(t2,3) + 
                  13*Power(s1,2)*(56 + 17*t2) + 
                  s1*(2220 - 2409*t2 - 392*Power(t2,2))) + 
               Power(t1,6)*(248 + 10*Power(s1,3) - 879*t2 + 
                  212*Power(t2,2) - 6*Power(t2,3) + 
                  3*Power(s1,2)*(51 + 4*t2) + 
                  s1*(658 - 440*t2 - 29*Power(t2,2))) + 
               t1*(-1445 + 30*Power(s1,3) + 2697*t2 - 
                  1444*Power(t2,2) - 54*Power(t2,3) - 
                  Power(s1,2)*(208 + 57*t2) + 
                  s1*(480 - 559*t2 + 504*Power(t2,2))) + 
               Power(t1,2)*(4679 - 238*Power(s1,3) - 9223*t2 + 
                  5946*Power(t2,2) + 172*Power(t2,3) + 
                  Power(s1,2)*(1469 + 596*t2) - 
                  s1*(1009 + 148*t2 + 2043*Power(t2,2))) + 
               Power(t1,3)*(-5653 + 436*Power(s1,3) + 12511*t2 - 
                  9244*Power(t2,2) - 217*Power(t2,3) - 
                  5*Power(s1,2)*(568 + 273*t2) + 
                  s1*(-782 + 4035*t2 + 2966*Power(t2,2)))) + 
            Power(s2,2)*(941 + 2*Power(t1,8) - 
               Power(s1,3)*(26 - 317*t1 + 766*Power(t1,2) - 
                  599*Power(t1,3) + 114*Power(t1,4) + 82*Power(t1,5)) - 
               1784*t2 + 990*Power(t2,2) + 35*Power(t2,3) + 
               Power(t1,7)*(-31 + 73*t2) + 
               Power(t1,6)*(306 - 659*t2 - 33*Power(t2,2)) + 
               Power(t1,4)*(3117 - 10569*t2 + 8577*Power(t2,2) - 
                  97*Power(t2,3)) - 
               Power(t1,3)*(6776 - 18971*t2 + 16868*Power(t2,2) + 
                  48*Power(t2,3)) + 
               Power(t1,5)*(-711 + 3072*t2 - 1599*Power(t2,2) + 
                  110*Power(t2,3)) - 
               t1*(4106 - 8673*t2 + 6381*Power(t2,2) + 231*Power(t2,3)) + 
               Power(t1,2)*(7290 - 17786*t2 + 15457*Power(t2,2) + 
                  274*Power(t2,3)) + 
               Power(s1,2)*(173 + 131*Power(t1,6) + 52*t2 - 
                  Power(t1,5)*(929 + 87*t2) + 
                  6*Power(t1,4)*(593 + 212*t2) - 
                  7*Power(t1,3)*(889 + 390*t2) - t1*(2001 + 820*t2) + 
                  3*Power(t1,2)*(1825 + 826*t2)) + 
               s1*(-321 - 52*Power(t1,7) + Power(t1,6)*(457 - 194*t2) + 
                  326*t2 - 362*Power(t2,2) + 
                  Power(t1,5)*(-2507 + 2759*t2 + 134*Power(t2,2)) - 
                  2*Power(t1,4)*(-2965 + 5172*t2 + 826*Power(t2,2)) - 
                  2*Power(t1,2)*(-1597 + 5592*t2 + 2072*Power(t2,2)) + 
                  t1*(353 + 1935*t2 + 2089*Power(t2,2)) + 
                  Power(t1,3)*(-7113 + 16415*t2 + 3795*Power(t2,2))))) + 
         Power(s,6)*(-2*t1*(-26 + 51*t1 + 19*Power(t1,2) - 
               48*Power(t1,3) + 10*Power(t1,4)) + 
            10*Power(s2,5)*(-3*Power(t1,2) + s1*(-2 + 4*t1) + 
               t1*(2 - 7*t2) + 4*t2) - 
            Power(s2,3)*(141 + 224*Power(t1,4) + 
               Power(s1,3)*(-2 + 22*t1) - 498*t2 + 103*Power(t2,2) - 
               140*Power(t2,3) + 4*Power(t1,3)*(-123 + 26*t2) + 
               Power(t1,2)*(641 - 2056*t2 - 219*Power(t2,2)) + 
               2*t1*(-261 + 1137*t2 - 149*Power(t2,2) + 92*Power(t2,3)) + 
               2*Power(s1,2)*(20*Power(t1,2) + 6*(9 + t2) - 
                  5*t1*(41 + 10*t2)) + 
               2*s1*(130 + 2*Power(t1,3) - 107*t2 + 40*Power(t2,2) + 
                  Power(t1,2)*(678 + 93*t2) + 
                  t1*(-676 + 337*t2 - 33*Power(t2,2)))) - 
            Power(s2,4)*(8 + Power(s1,3) - 150*Power(t1,3) + 
               Power(s1,2)*(-7 + 38*t1 - 18*t2) - 319*t2 - 
               88*Power(t2,2) - 56*Power(t2,3) - 
               40*Power(t1,2)*(-5 + 3*t2) + 
               t1*(-106 + 795*t2 + 143*Power(t2,2)) + 
               s1*(10*Power(t1,2) - t1*(529 + 211*t2) + 
                  9*(25 + 9*t2 + 7*Power(t2,2)))) - 
            Power(s2,2)*(257 - 122*Power(t1,5) + 
               Power(s1,3)*(-14 + 83*t1 + 7*Power(t1,2)) + 
               Power(t1,4)*(456 - 132*t2) - 421*t2 + 252*Power(t2,2) - 
               93*Power(t2,3) + 
               Power(t1,3)*(-976 + 2113*t2 + 108*Power(t2,2)) + 
               Power(t1,2)*(1353 - 4156*t2 + 807*Power(t2,2) - 
                  218*Power(t2,3)) + 
               t1*(-1269 + 2870*t2 - 1383*Power(t2,2) + 
                  226*Power(t2,3)) + 
               Power(s1,2)*(110 - 71*Power(t1,3) + 43*t2 + 
                  2*Power(t1,2)*(313 + 51*t2) - 2*t1*(414 + 155*t2)) + 
               s1*(66 + 90*Power(t1,4) - 193*t2 - 29*Power(t2,2) + 
                  Power(t1,3)*(-1393 + 5*t2) + 
                  Power(t1,2)*(2314 - 1286*t2 + 25*Power(t2,2)) + 
                  t1*(-932 + 1630*t2 + 215*Power(t2,2)))) + 
            s2*(-27 - 20*Power(t1,6) + 
               2*Power(t1,5)*(73 + 25*s1 - 30*t2) + 15*t2 - 
               39*Power(t2,2) + 23*Power(t2,3) + 
               Power(t1,4)*(-415 - 25*Power(s1,2) + 776*t2 + 
                  10*Power(t2,2) + s1*(-512 + 30*t2)) + 
               Power(t1,3)*(783 + 10*Power(s1,3) - 2361*t2 + 
                  410*Power(t2,2) - 70*Power(t2,3) + 
                  6*Power(s1,2)*(37 + 5*t2) + 
                  s1*(1303 - 533*t2 - 20*Power(t2,2))) - 
               t1*(60*Power(s1,3) - Power(s1,2)*(391 + 165*t2) + 
                  s1*(10 + 447*t2 + 279*Power(t2,2)) + 
                  2*(-502 + 767*t2 - 447*Power(t2,2) + 25*Power(t2,3))) + 
               Power(t1,2)*(55*Power(s1,3) - 
                  2*Power(s1,2)*(301 + 109*t2) + 
                  s1*(-856 + 1110*t2 + 275*Power(t2,2)) + 
                  2*(-705 + 1484*t2 - 633*Power(t2,2) + 56*Power(t2,3))))))\
)/(s*(-1 + s1)*(-1 + s2)*s2*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + 
         Power(s2,2))*Power(-s + s2 - t1,2)*(1 - s + s2 - t1)*
       Power(-1 + s + t1,2)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*
       (-1 + t2)) + (8*(Power(s,8)*
          (2*Power(t1,3) + Power(t1,2)*t2 + Power(t2,3)) + 
         Power(s,7)*(6*Power(t1,4) - 
            2*Power(t1,3)*(1 + 2*s1 + 4*s2 - 3*t2) + 
            Power(t1,2)*(4 + s2*(2 - 6*t2) + s1*(-3 + 4*s2 - t2) - 
               2*t2) + (3 + s2*(-3 + 3*s1 - 7*t2) - 8*t2)*Power(t2,2) + 
            t1*t2*(3 + s2*(-1 + t2) + 4*Power(t2,2) + 
               s1*(-3 + 3*s2 + t2))) + 
         (s2 - t1)*(-1 + t1)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,
           2)*(t1*(23 - 2*s1 - 11*t2) + 
            Power(s2,3)*(-2 + 3*s1 + 2*t1 - 3*t2) + 6*(-1 + t2) + 
            Power(t1,2)*(-17 + 5*s1 + 2*t2) - 
            s2*(11 + s1*(-2 + 4*t1 + Power(t1,2)) + t2 - 
               6*t1*(2 + t2) + Power(t1,2)*(1 + 2*t2)) - 
            Power(s2,2)*(1 + s1 + 2*s1*t1 + 2*Power(t1,2) + 2*t2 - 
               t1*(3 + 5*t2))) + 
         Power(s,6)*(6*Power(t1,5) + 
            Power(t1,4)*(5 - 12*s1 - 18*s2 + 12*t2) + 
            Power(t1,3)*(2*Power(s1,2) + 12*Power(s2,2) + 
               2*s1*(5 + 13*s2 - 3*t2) - 7*(5 + 2*t2) - s2*(3 + 28*t2)) \
+ t2*(3*(1 - 7*t2 + 8*Power(t2,2)) + 
               s2*(-3 + s1*(3 - 17*t2) + 3*t2 + 43*Power(t2,2)) + 
               Power(s2,2)*(1 + 3*Power(s1,2) + 16*t2 + 
                  21*Power(t2,2) - 3*s1*(1 + 6*t2))) + 
            Power(t1,2)*(-22 + Power(s1,2)*(1 - 5*s2) + 2*t2 + 
               3*Power(t2,2) + 6*Power(t2,3) + 
               Power(s2,2)*(-7 + 15*t2) + 
               s2*(7 + 9*t2 + 4*Power(t2,2)) + 
               s1*(16 - 15*Power(s2,2) - 11*t2 + 4*Power(t2,2) + 
                  s2*(-12 + 19*t2))) + 
            t1*(3 + Power(s1,2)*s2*(-3 + 3*s2 - t2) - 21*t2 + 
               7*Power(t2,2) - 30*Power(t2,3) + 
               Power(s2,2)*(1 - 6*Power(t2,2)) + 
               s2*(2 + 6*t2 - 23*Power(t2,2) - 24*Power(t2,3)) + 
               s1*(-3 + Power(s2,2)*(2 - 13*t2) + 20*t2 - 
                  3*Power(t2,2) + s2*(3 - t2 + 7*Power(t2,2))))) + 
         Power(s,5)*(1 + 2*Power(t1,6) + 
            Power(t1,4)*(-152 + 6*Power(s1,2) + s1*(38 - 12*t2) - 
               27*t2) - 18*t2 + 51*Power(t2,2) - 30*Power(t2,3) + 
            Power(t1,5)*(24 - 12*s1 + 10*t2) + 
            Power(t1,2)*(33 - 42*t2 - 18*Power(t2,2) - 41*Power(t2,3) + 
               Power(s1,2)*(-13 + 2*t2) + 
               s1*(-15 + 89*t2 - 12*Power(t2,2))) + 
            t1*(-18 + 50*t2 - 39*Power(t2,2) + 78*Power(t2,3) + 
               s1*(16 - 37*t2 - 9*Power(t2,2))) + 
            Power(t1,3)*(153 - 10*Power(s1,2) - 26*t2 + 
               12*Power(t2,2) + 4*Power(t2,3) + 
               s1*(24 - 2*t2 + 6*Power(t2,2))) + 
            Power(s2,3)*(Power(s1,3) - 8*Power(t1,3) + 
               Power(t1,2)*(9 - 20*t2) - Power(s1,2)*(11*t1 + 15*t2) + 
               t1*(-3 + 10*t2 + 15*Power(t2,2)) - 
               t2*(6 + 35*t2 + 35*Power(t2,2)) + 
               s1*(1 + 20*Power(t1,2) + 14*t2 + 45*Power(t2,2) + 
                  t1*(-6 + 20*t2))) + 
            Power(s2,2)*(1 + 18*Power(t1,4) + 
               (14 - 13*Power(s1,2))*t2 + (-46 + 77*s1)*Power(t2,2) - 
               95*Power(t2,3) + Power(t1,3)*(27 - 50*s1 + 52*t2) + 
               Power(t1,2)*(-59 + 25*Power(s1,2) + s1*(51 - 57*t2) - 
                  34*t2 - 20*Power(t2,2)) + 
               t1*(18 - 2*Power(s1,3) - 18*t2 + 101*Power(t2,2) + 
                  60*Power(t2,3) + Power(s1,2)*(-3 + 16*t2) + 
                  s1*(-16 + 9*t2 - 50*Power(t2,2)))) + 
            s2*(-12*Power(t1,5) + Power(t1,4)*(-53 + 42*s1 - 42*t2) + 
               t2*(3 + 40*t2 - 95*Power(t2,2) + s1*(-13 + 31*t2)) + 
               Power(t1,3)*(222 - 20*Power(s1,2) + 50*t2 + 
                  6*Power(t2,2) + s1*(-74 + 46*t2)) + 
               Power(t1,2)*(-102 + Power(s1,3) + 
                  Power(s1,2)*(19 - 5*t2) + 49*t2 - 73*Power(t2,2) - 
                  30*Power(t2,3) + s1*(-20 - 21*t2 + 2*Power(t2,2))) + 
               t1*(-11 - 5*t2 + 92*Power(t2,2) + 136*Power(t2,3) + 
                  13*Power(s1,2)*(1 + t2) - 
                  s1*(3 + 74*t2 + 52*Power(t2,2))))) + 
         Power(s,4)*(-5 + Power(t1,5)*
             (-205 + 6*Power(s1,2) + s1*(36 - 10*t2) - 24*t2) + 33*t2 - 
            39*Power(t2,2) + 3*Power(t2,3) + 
            Power(t1,6)*(29 - 4*s1 + 3*t2) + 
            Power(t1,3)*(-361 + 2*Power(s1,3) + 57*t2 - 
               75*Power(t2,2) - 24*Power(t2,3) + 
               Power(s1,2)*(-15 + 4*t2) + 
               s1*(-37 + 132*t2 - 16*Power(t2,2))) + 
            Power(t1,4)*(471 - 31*Power(s1,2) - 57*t2 + 
               19*Power(t2,2) + Power(t2,3) + 
               4*s1*(5 + 6*t2 + Power(t2,2))) + 
            Power(t1,2)*(30 + Power(s1,2)*(44 - 10*t2) + 77*t2 + 
               59*Power(t2,2) + 87*Power(t2,3) - 
               s1*(57 + 142*t2 + 32*Power(t2,2))) + 
            t1*(29 - 22*t2 + 37*Power(t2,2) - 69*Power(t2,3) + 
               s1*(-19 - 16*t2 + 51*Power(t2,2))) + 
            Power(s2,4)*(-4*Power(s1,3) + 2*Power(t1,3) + 
               5*Power(t1,2)*(-1 + 3*t2) + 
               Power(s1,2)*(1 + 14*t1 + 30*t2) + 
               t1*(3 - 20*t2 - 20*Power(t2,2)) + 
               t2*(12 + 40*t2 + 35*Power(t2,2)) - 
               s1*(3 + 10*Power(t1,2) + 26*t2 + 60*Power(t2,2) + 
                  2*t1*(-3 + 5*t2))) + 
            s2*(-4 + 2*Power(s1,3)*(-5 + t1)*Power(t1,2) - 
               2*Power(t1,6) + 23*t2 - 101*Power(t2,2) + 
               85*Power(t2,3) - Power(t1,5)*(91 + 24*t2) + 
               Power(t1,4)*(512 + 71*t2 + 4*Power(t2,2)) - 
               Power(t1,3)*(792 - 124*t2 + 111*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(358 - 80*t2 + 336*Power(t2,2) + 
                  151*Power(t2,3)) - 
               t1*(-10 + 44*t2 + 117*Power(t2,2) + 244*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-24*Power(t1,3) - 8*Power(t1,2)*(-11 + t2) - 
                  2*(9 + 19*t2) + t1*(25 + 48*t2)) + 
               s1*(1 + 22*Power(t1,5) + 48*Power(t1,4)*(-2 + t2) + 
                  16*t2 - 17*Power(t2,2) + 
                  Power(t1,2)*(97 - 315*t2 - 48*Power(t2,2)) - 
                  2*Power(t1,3)*(38 + 42*t2 + 3*Power(t2,2)) + 
                  t1*(-18 + 232*t2 + 103*Power(t2,2)))) + 
            Power(s2,3)*(-6*Power(t1,4) + 2*Power(s1,3)*(-2 + 5*t1) - 
               Power(t1,3)*(41 + 48*t2) + 
               Power(s1,2)*(-1 - 40*Power(t1,2) + t1*(16 - 54*t2) + 
                  48*t2) + Power(t1,2)*(92 + 69*t2 + 40*Power(t2,2)) + 
               5*(1 - 2*t2 + 19*Power(t2,2) + 22*Power(t2,3)) - 
               t1*(51 + 19*t2 + 174*Power(t2,2) + 80*Power(t2,3)) + 
               s1*(1 + 34*Power(t1,3) - 28*t2 - 138*Power(t2,2) + 
                  Power(t1,2)*(-49 + 58*t2) + 
                  t1*(22 + 29*t2 + 110*Power(t2,2)))) + 
            Power(s2,2)*(2 + 4*Power(s1,3)*(3 - 2*t1)*t1 + 
               6*Power(t1,5) - 40*t2 + 38*Power(t2,2) + 
               148*Power(t2,3) + 2*Power(t1,4)*(55 + 27*t2) - 
               Power(t1,3)*(421 + 91*t2 + 24*Power(t2,2)) + 
               Power(t1,2)*(405 - 62*t2 + 231*Power(t2,2) + 
                  60*Power(t2,3)) - 
               t1*(116 - 79*t2 + 366*Power(t2,2) + 247*Power(t2,3)) + 
               Power(s1,2)*(44*Power(t1,3) + 18*t2 + 
                  Power(t1,2)*(-74 + 32*t2) - t1*(23 + 90*t2)) + 
               s1*(-1 - 42*Power(t1,4) + Power(t1,3)*(107 - 86*t2) + 
                  6*t2 - 103*Power(t2,2) + 
                  Power(t1,2)*(5 + 69*t2 - 48*Power(t2,2)) + 
                  t1*(-3 + 175*t2 + 224*Power(t2,2))))) + 
         Power(s,3)*(16*Power(t1,7) + 
            Power(t1,6)*(-128 + 2*Power(s1,2) + s1*(11 - 3*t2) - 
               13*t2) + 6*(1 - t2 - 5*Power(t2,2) + 5*Power(t2,3)) + 
            Power(t1,4)*(-731 + 6*Power(s1,3) + 149*t2 - 
               87*Power(t2,2) - 5*Power(t2,3) + Power(s1,2)*(4 + t2) + 
               s1*(-72 + 85*t2 - 8*Power(t2,2))) + 
            Power(t1,5)*(425 - 30*Power(s1,2) - 33*t2 + 
               14*Power(t2,2) + s1*(25 + 27*t2 + Power(t2,2))) + 
            t1*(17 - 128*t2 + 126*Power(t2,2) - 33*Power(t2,3) - 
               4*s1*(6 - 29*t2 + 21*Power(t2,2))) - 
            Power(t1,3)*(-579 + 10*Power(s1,3) + 77*t2 - 
               193*Power(t2,2) - 36*Power(t2,3) + 
               2*Power(s1,2)*(-67 + 13*t2) + 
               s1*(189 + 189*t2 + 46*Power(t2,2))) + 
            Power(t1,2)*(-184 + 184*t2 - 205*Power(t2,2) - 
               37*Power(t2,3) + 2*Power(s1,2)*(-26 + 9*t2) + 
               s1*(173 - 105*t2 + 157*Power(t2,2))) + 
            Power(s2,5)*(6*Power(s1,3) + Power(t1,2)*(1 - 6*t2) - 
               3*Power(s1,2)*(1 + 2*t1 + 10*t2) + 
               t1*(-1 + 15*t2 + 15*Power(t2,2)) - 
               t2*(10 + 25*t2 + 21*Power(t2,2)) + 
               s1*(3 + 24*t2 + 45*Power(t2,2) - t1*(2 + 5*t2))) + 
            Power(s2,3)*(-31 + 
               3*Power(s1,3)*(2 - 13*t1 + 6*Power(t1,2)) + 24*t2 - 
               140*Power(t2,2) - 117*Power(t2,3) - 
               10*Power(t1,4)*(8 + 3*t2) + 
               Power(t1,3)*(316 + 94*t2 + 36*Power(t2,2)) - 
               Power(t1,2)*(407 + 56*t2 + 287*Power(t2,2) + 
                  60*Power(t2,3)) + 
               t1*(202 - 9*t2 + 482*Power(t2,2) + 228*Power(t2,3)) + 
               Power(s1,2)*(-7 - 26*Power(t1,3) + 
                  Power(t1,2)*(57 - 66*t2) - 50*t2 + t1*(38 + 191*t2)) \
+ s1*(-10 + 6*Power(t1,4) + 67*t2 + 136*Power(t2,2) + 
                  Power(t1,3)*(-34 + 42*t2) + 
                  t1*(28 - 290*t2 - 339*Power(t2,2)) + 
                  Power(t1,2)*(-13 + 28*t2 + 92*Power(t2,2)))) + 
            s2*(8 + Power(t1,6)*(-59 + 2*s1 - 4*t2) - 
               2*(13 + 6*s1)*t2 + (53 + 4*s1)*Power(t2,2) - 
               17*Power(t2,3) + 
               Power(t1,5)*(423 - 8*Power(s1,2) + 49*t2 + 
                  Power(t2,2) + s1*(-40 + 19*t2)) + 
               t1*(44 + 84*t2 + 86*Power(t2,2) + 138*Power(t2,3) + 
                  4*Power(s1,2)*(4 + 7*t2) + 
                  s1*(5 - 238*t2 - 67*Power(t2,2))) + 
               Power(t1,3)*(1306 - 29*Power(s1,3) - 169*t2 + 
                  460*Power(t2,2) + 68*Power(t2,3) + 
                  Power(s1,2)*(16 + 75*t2) + 
                  s1*(283 - 436*t2 - 10*Power(t2,2))) - 
               Power(t1,4)*(1106 + 4*Power(s1,2)*(-25 + t2) - 87*t2 + 
                  84*Power(t2,2) + 3*Power(t2,3) + 
                  s1*(122 + 104*t2 + 5*Power(t2,2))) + 
               Power(t1,2)*(-616 + 30*Power(s1,3) - 91*t2 - 
                  573*Power(t2,2) - 200*Power(t2,3) - 
                  Power(s1,2)*(169 + 112*t2) + 
                  s1*(-58 + 873*t2 + 104*Power(t2,2)))) - 
            Power(s2,4)*(9 + Power(s1,3)*(-11 + 18*t1) + 11*t2 + 
               84*Power(t2,2) + 70*Power(t2,3) - 
               Power(t1,3)*(23 + 22*t2) + 
               Power(s1,2)*(4 - 20*Power(t1,2) + 66*t2 - 76*t1*t2) + 
               Power(t1,2)*(55 + 63*t2 + 40*Power(t2,2)) - 
               t1*(41 + 55*t2 + 146*Power(t2,2) + 60*Power(t2,3)) + 
               s1*(2*Power(t1,3) + Power(t1,2)*(-9 + 7*t2) - 
                  t2*(55 + 122*t2) + t1*(10 + 86*t2 + 115*Power(t2,2)))) \
+ Power(s2,2)*(-12 + Power(s1,3)*t1*(-26 + 51*t1 - 6*Power(t1,2)) + 
               16*t2 - 29*Power(t2,2) - 85*Power(t2,3) + 
               9*Power(t1,5)*(11 + 2*t2) - 
               Power(t1,4)*(559 + 78*t2 + 12*Power(t2,2)) + 
               Power(t1,3)*(1078 - 71*t2 + 233*Power(t2,2) + 
                  24*Power(t2,3)) - 
               Power(t1,2)*(878 - 158*t2 + 785*Power(t2,2) + 
                  215*Power(t2,3)) + 
               t1*(272 - 74*t2 + 536*Power(t2,2) + 279*Power(t2,3)) + 
               Power(s1,2)*(18*Power(t1,4) - 10*t2 + 
                  4*Power(t1,3)*(-31 + 6*t2) - 
                  5*Power(t1,2)*(14 + 37*t2) + 2*t1*(47 + 68*t2)) + 
               s1*(-4 - 6*Power(t1,5) + Power(t1,4)*(56 - 46*t2) + 
                  47*t2 + 32*Power(t2,2) + 
                  t1*(57 - 519*t2 - 226*Power(t2,2)) + 
                  Power(t1,3)*(101 + 127*t2 - 18*Power(t2,2)) + 
                  Power(t1,2)*(-173 + 530*t2 + 225*Power(t2,2))))) + 
         Power(s,2)*(4*Power(t1,8) - 2*Power(t1,7)*(21 + s1 + 2*t2) - 
            4*Power(-1 + t2,2)*(-1 + 7*t2) + 
            Power(t1,6)*(153 - 12*Power(s1,2) + 3*t2 + 4*Power(t2,2) + 
               s1*(21 + 11*t2)) + 
            t1*(-1 + t2)*(86 - 185*t2 + 93*Power(t2,2) + 
               s1*(-52 + 60*t2)) + 
            Power(t1,2)*(307 - 599*t2 + 440*Power(t2,2) - 
               76*Power(t2,3) - 2*Power(s1,2)*(-9 + 7*t2) + 
               s1*(-208 + 362*t2 - 204*Power(t2,2))) + 
            Power(t1,5)*(-427 + 6*Power(s1,3) + 
               Power(s1,2)*(7 - 2*t2) + 74*t2 - 40*Power(t2,2) - 
               s1*(55 - 26*t2 + Power(t2,2))) - 
            Power(t1,4)*(-690 + 26*Power(s1,3) + 119*t2 - 
               150*Power(t2,2) - 3*Power(t2,3) + 
               11*Power(s1,2)*(-17 + 2*t2) + 
               s1*(236 + 112*t2 + 33*Power(t2,2))) + 
            Power(t1,3)*(-603 + 14*Power(s1,3) + 410*t2 - 
               314*Power(t2,2) + 9*Power(t2,3) + 
               Power(s1,2)*(-178 + 51*t2) + 
               s1*(428 - 219*t2 + 170*Power(t2,2))) + 
            Power(s2,6)*(-4*Power(s1,3) + 
               Power(s1,2)*(3 - t1 + 15*t2) + 
               s1*(-1 + Power(t1,2) - 11*t2 + 7*t1*t2 - 
                  18*Power(t2,2)) + 
               t2*(3 + Power(t1,2) + 8*t2 + 7*Power(t2,2) - 
                  2*t1*(2 + 3*t2))) + 
            Power(s2,5)*(3 + 2*Power(s1,3)*(-5 + 7*t1) + 11*t2 + 
               34*Power(t2,2) + 23*Power(t2,3) - 
               4*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(11 + 22*t2 + 20*Power(t2,2)) - 
               t1*(10 + 29*t2 + 59*Power(t2,2) + 24*Power(t2,3)) + 
               Power(s1,2)*(11 + 5*Power(t1,2) + 40*t2 - 
                  7*t1*(3 + 7*t2)) + 
               s1*(-2 - 4*Power(t1,3) + Power(t1,2)*(3 - 21*t2) - 
                  41*t2 - 53*Power(t2,2) + 
                  t1*(3 + 72*t2 + 59*Power(t2,2)))) + 
            s2*(13 - 16*Power(t1,7) - 
               Power(s1,3)*Power(t1,2)*
                (34 - 81*t1 + 27*Power(t1,2) + 2*Power(t1,3)) - 46*t2 + 
               47*Power(t2,2) - 14*Power(t2,3) + 
               2*Power(t1,6)*(79 + 10*t2) + 
               t1*(-129 + 130*t2 - 145*Power(t2,2)) - 
               2*Power(t1,5)*(263 + 10*t2 + 14*Power(t2,2)) + 
               Power(t1,4)*(1048 - 13*t2 + 250*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,2)*(625 + 124*t2 + 394*Power(t2,2) + 
                  41*Power(t2,3)) - 
               Power(t1,3)*(1173 + 195*t2 + 577*Power(t2,2) + 
                  50*Power(t2,3)) + 
               Power(s1,2)*t1*
                (Power(t1,5) + Power(t1,4)*(41 + t2) + 
                  3*t1*(99 + 2*t2) + 4*(-5 + 3*t2) + 
                  Power(t1,3)*(17 + 58*t2) - Power(t1,2)*(395 + 126*t2)\
) + s1*(Power(t1,6)*t2 - Power(t1,5)*(86 + 51*t2 + Power(t2,2)) + 
                  Power(t1,4)*(232 - 252*t2 + 5*Power(t2,2)) - 
                  4*(3 - 8*t2 + 5*Power(t2,2)) + 
                  Power(t1,3)*(84 + 1003*t2 + 16*Power(t2,2)) - 
                  Power(t1,2)*(293 + 577*t2 + 19*Power(t2,2)) + 
                  t1*(75 - 38*t2 + 63*Power(t2,2)))) + 
            Power(s2,2)*(16 + 
               Power(s1,3)*t1*
                (26 - 95*t1 + 59*Power(t1,2) + 4*Power(t1,3)) + 13*t2 + 
               35*Power(t2,2) + 8*Power(t2,3) + Power(t1,6)*(30 + t2) - 
               Power(t1,5)*(262 + 33*t2 + 2*Power(t2,2)) + 
               Power(t1,4)*(758 + 33*t2 + 102*Power(t2,2) + 
                  3*Power(t2,3)) - 
               t1*(278 + 98*t2 + 427*Power(t2,2) + 71*Power(t2,3)) - 
               Power(t1,3)*(1096 + 9*t2 + 640*Power(t2,2) + 
                  71*Power(t2,3)) + 
               Power(t1,2)*(832 + 93*t2 + 956*Power(t2,2) + 
                  144*Power(t2,3)) + 
               Power(s1,2)*(-5*Power(t1,5) + Power(t1,4)*(-53 + t2) + 
                  2*(1 + t2) - 3*t1*(52 + 23*t2) - 
                  Power(t1,3)*(137 + 163*t2) + 
                  Power(t1,2)*(373 + 254*t2)) + 
               s1*(13 + Power(t1,6) + Power(t1,5)*(5 - 3*t2) - 84*t2 + 
                  21*Power(t2,2) + 
                  Power(t1,2)*(112 - 1235*t2 - 140*Power(t2,2)) + 
                  t1*(-13 + 665*t2 - 11*Power(t2,2)) + 
                  Power(t1,4)*(78 + 88*t2 - 2*Power(t2,2)) + 
                  Power(t1,3)*(-196 + 521*t2 + 100*Power(t2,2)))) + 
            Power(s2,3)*(50 + 
               Power(s1,3)*(-6 + 51*t1 - 71*Power(t1,2) + 
                  4*Power(t1,3)) - 14*t2 + 121*Power(t2,2) + 
               31*Power(t2,3) - 4*Power(t1,5)*(8 + t2) + 
               Power(t1,4)*(221 + 44*t2 + 12*Power(t2,2)) + 
               t1*(-266 + t2 - 554*Power(t2,2) - 142*Power(t2,3)) - 
               Power(t1,3)*(512 + 83*t2 + 185*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(539 + 56*t2 + 646*Power(t2,2) + 
                  149*Power(t2,3)) + 
               Power(s1,2)*(37 + 10*Power(t1,4) + 12*t2 - 
                  8*Power(t1,3)*(-2 + 3*t2) - 27*t1*(7 + 6*t2) + 
                  2*Power(t1,2)*(83 + 120*t2)) + 
               s1*(2 - 4*Power(t1,5) + Power(t1,4)*(1 - 4*t2) - 
                  117*t2 - 16*Power(t2,2) + 
                  Power(t1,2)*(29 - 525*t2 - 285*Power(t2,2)) + 
                  Power(t1,3)*(-17 + 19*t2 + 30*Power(t2,2)) + 
                  t1*(-11 + 547*t2 + 205*Power(t2,2)))) + 
            Power(s2,4)*(21 + 
               Power(s1,3)*(-11 + 43*t1 - 16*Power(t1,2)) + 20*t2 + 
               113*Power(t2,2) + 53*Power(t2,3) + 
               6*Power(t1,4)*(3 + t2) - 
               Power(t1,3)*(86 + 45*t2 + 24*Power(t2,2)) + 
               Power(t1,2)*(139 + 89*t2 + 160*Power(t2,2) + 
                  30*Power(t2,3)) - 
               t1*(92 + 70*t2 + 273*Power(t2,2) + 112*Power(t2,3)) + 
               Power(s1,2)*(-10*Power(t1,3) + 8*(3 + 7*t2) + 
                  Power(t1,2)*(26 + 56*t2) - t1*(64 + 173*t2)) + 
               s1*(3 + 6*Power(t1,4) - 107*t2 - 96*Power(t2,2) + 
                  Power(t1,3)*(-7 + 20*t2) - 
                  2*Power(t1,2)*(-1 + 64*t2 + 34*Power(t2,2)) + 
                  t1*(-4 + 263*t2 + 238*Power(t2,2))))) + 
         s*(-2*(3 + s1)*Power(t1,8) + 
            Power(s2,7)*Power(s1 - t2,2)*(-1 + s1 + t1 - t2) + 
            8*Power(-1 + t2,3) - 
            t1*Power(-1 + t2,2)*(-67 + 16*s1 + 49*t2) + 
            Power(t1,6)*(-79 + 2*Power(s1,3) + 4*s1*(-4 + t2) - 
               Power(s1,2)*(-1 + t2) + 7*t2 - 6*Power(t2,2)) + 
            Power(t1,7)*(-2*Power(s1,2) + 2*s1*(4 + t2) + 4*(5 + t2)) - 
            Power(s2,6)*(s1 - t2)*
             (-1 + Power(s1,2)*(-3 + 4*t1) - 5*t2 - 3*Power(t2,2) - 
               Power(t1,2)*(1 + 4*t2) + 
               s1*(6 - 11*t1 + 5*Power(t1,2) + 6*t2 - 8*t1*t2) + 
               t1*(2 + 9*t2 + 4*Power(t2,2))) - 
            Power(t1,5)*(-240 + 21*Power(s1,3) + 
               8*Power(s1,2)*(-15 + t2) + 26*t2 - 34*Power(t2,2) + 
               s1*(157 + 20*t2 + 10*Power(t2,2))) + 
            Power(t1,2)*(-1 + t2)*
             (4*Power(s1,2) + 101*s1*(-1 + t2) + 
               6*(36 - 49*t2 + 13*Power(t2,2))) - 
            Power(t1,3)*(-381 + 4*Power(s1,3) + 567*t2 - 
               322*Power(t2,2) + 42*Power(t2,3) + 
               Power(s1,2)*(-79 + 45*t2) + 
               s1*(327 - 392*t2 + 149*Power(t2,2))) + 
            Power(t1,4)*(21*Power(s1,3) + 2*Power(s1,2)*(-97 + 28*t2) + 
               s1*(409 - 208*t2 + 68*Power(t2,2)) + 
               7*(-57 + 33*t2 - 17*Power(t2,2) + Power(t2,3))) + 
            s2*(-(Power(s1,3)*Power(t1,2)*
                  (-12 + 66*t1 - 70*Power(t1,2) + 7*Power(t1,3) + 
                    Power(t1,4))) + 2*Power(t1,7)*(12 + t2) + 
               2*t1*Power(-1 + t2,2)*(77 + t2) + 
               Power(-1 + t2,2)*(-29 + 11*t2) - 
               2*Power(t1,6)*(43 + 10*t2 + Power(t2,2)) + 
               Power(t1,5)*(233 + 62*t2 + 39*Power(t2,2)) + 
               Power(t1,3)*(601 + 188*t2 + 109*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(t1,4)*(-490 - 236*t2 - 149*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(t1,2)*(407 - 241*t2 + 96*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(s1,2)*t1*
                (8 + Power(t1,2)*(506 - 58*t2) - 8*t2 + 
                  Power(t1,5)*(7 + t2) - 4*Power(t1,3)*(84 + 17*t2) + 
                  Power(t1,4)*(6 + 20*t2) + t1*(-191 + 89*t2)) + 
               s1*(6*Power(t1,7) + 16*Power(-1 + t2,2) - 
                  100*t1*Power(-1 + t2,2) - Power(t1,6)*(32 + 11*t2) + 
                  Power(t1,4)*(256 + 439*t2 - 23*Power(t2,2)) + 
                  Power(t1,5)*(59 - 54*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*(454 - 262*t2 + 60*Power(t2,2)) + 
                  Power(t1,3)*(-659 - 280*t2 + 69*Power(t2,2)))) + 
            Power(s2,3)*(-29 + 
               Power(s1,3)*(4 - 30*t1 + 66*Power(t1,2) - 
                  38*Power(t1,3) - 5*Power(t1,4)) - 15*t2 - 
               56*Power(t2,2) + 6*Power(t2,3) + 
               Power(t1,5)*(44 + 8*t2 + Power(t2,2)) + 
               t1*(119 + 52*t2 + 283*Power(t2,2) - 6*Power(t2,3)) - 
               Power(t1,4)*(164 + 66*t2 + 39*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(t1,2)*(227 + 109*t2 + 458*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(t1,3)*(257 + 130*t2 + 269*Power(t2,2) + 
                  34*Power(t2,3)) + 
               Power(s1,2)*(-33 + 5*Power(t1,5) + 
                  3*Power(t1,4)*(-5 + t2) - t2 + 10*t1*(19 + 5*t2) + 
                  6*Power(t1,3)*(27 + 20*t2) - 
                  Power(t1,2)*(309 + 163*t2)) + 
               s1*(18 + Power(t1,5)*(4 - 6*t2) + 86*t2 - 
                  20*Power(t2,2) + 
                  3*Power(t1,4)*(10 + 2*t2 + Power(t2,2)) + 
                  t1*(-61 - 416*t2 + 19*Power(t2,2)) + 
                  2*Power(t1,2)*(53 + 307*t2 + 47*Power(t2,2)) - 
                  Power(t1,3)*(97 + 284*t2 + 105*Power(t2,2)))) + 
            Power(s2,2)*(Power(s1,3)*t1*
                (-12 + 72*t1 - 92*Power(t1,2) + 19*Power(t1,3) + 
                  4*Power(t1,4)) - 2*Power(-1 + t2,2)*(5 + 4*t2) - 
               Power(t1,6)*(44 + 5*t2) + 
               Power(t1,5)*(167 + 42*t2 + 14*Power(t2,2)) + 
               t1*(123 + 137*t2 + 34*Power(t2,2) - 12*Power(t2,3)) + 
               Power(t1,3)*(438 + 313*t2 + 439*Power(t2,2) + 
                  6*Power(t2,3)) - 
               Power(t1,4)*(330 + 129*t2 + 169*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(t1,2)*(-344 - 370*t2 - 324*Power(t2,2) + 
                  30*Power(t2,3)) - 
               Power(s1,2)*(4 + Power(t1,6) - 4*t2 + 
                  Power(t1,5)*(5 + 4*t2) + t1*(-145 + 43*t2) + 
                  Power(t1,2)*(466 + 50*t2) + 
                  Power(t1,4)*(78 + 63*t2) - Power(t1,3)*(409 + 183*t2)) \
+ s1*(Power(t1,6)*(-7 + t2) - Power(-1 + t2,2) + 
                  Power(t1,5)*(20 + 26*t2) + 
                  Power(t1,2)*(302 + 842*t2 - 148*Power(t2,2)) - 
                  Power(t1,3)*(159 + 822*t2 + 11*Power(t2,2)) + 
                  Power(t1,4)*(-10 + 167*t2 + 24*Power(t2,2)) + 
                  t1*(-145 - 216*t2 + 109*Power(t2,2)))) + 
            Power(s2,4)*(-9 + Power(s1,3)*(3 - 31*t1 + 40*Power(t1,2)) - 
               5*t2 - 45*Power(t2,2) + 3*Power(t2,3) - 
               2*Power(t1,4)*(11 + 5*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(75 + 60*t2 + 55*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(49 + 50*t2 + 184*Power(t2,2) + 33*Power(t2,3)) - 
               Power(t1,2)*(93 + 95*t2 + 190*Power(t2,2) + 
                  52*Power(t2,3)) + 
               Power(s1,2)*(-10*Power(t1,4) + 2*(-18 + t2) + 
                  4*Power(t1,3)*(9 + 2*t2) - 
                  5*Power(t1,2)*(25 + 27*t2) + t1*(135 + 89*t2)) + 
               s1*(9 + 62*t2 + 14*Power(t1,4)*t2 - 8*Power(t2,2) - 
                  2*Power(t1,3)*(17 + 36*t2 + 6*Power(t2,2)) - 
                  t1*(52 + 262*t2 + 91*Power(t2,2)) + 
                  Power(t1,2)*(77 + 258*t2 + 147*Power(t2,2)))) + 
            Power(s2,5)*(Power(s1,3)*(8 - 19*t1 + 5*Power(t1,2)) + 
               Power(s1,2)*(-19 + 10*Power(t1,3) - 33*t2 - 
                  Power(t1,2)*(31 + 17*t2) + t1*(40 + 68*t2)) + 
               s1*(6 + 51*t2 + 41*Power(t2,2) - 
                  2*Power(t1,3)*(1 + 8*t2) + 
                  Power(t1,2)*(10 + 67*t2 + 18*Power(t2,2)) - 
                  t1*(14 + 102*t2 + 77*Power(t2,2))) + 
               2*(-2 - 5*t2 - 16*Power(t2,2) - 8*Power(t2,3) + 
                  Power(t1,3)*(2 + 3*t2 + 3*Power(t2,2)) - 
                  Power(t1,2)*
                   (6 + 11*t2 + 18*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(6 + 13*t2 + 31*Power(t2,2) + 14*Power(t2,3))))))*
       B1(s,1 - s + s2 - t1,s2))/
     (s*(-1 + s1)*(-1 + s2)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*
       (-1 + t2)) + (8*(-2*Power(-1 + s,3)*s*(1 - 3*s + Power(s,2))*
          Power(t1,2)*(1 + t1)*Power(-1 + s + t1,4) + 
         Power(s2,10)*(s*(-1 + t1)*(-12 + 5*s1 + 12*t1 - 5*t2) - 
            Power(-1 + t1,2)*(-2 + 3*s1 + 2*t1 - 3*t2) + 
            2*Power(s,2)*(-1 + s1 + t1 - t2))*Power(s1 - t2,2) + 
         (-1 + s)*s*s2*t1*Power(-1 + s + t1,3)*
          (4*Power(s,6)*(-1 + t1)*t1 + 
            Power(s,5)*t1*(32 - 31*t1 + 4*Power(t1,2) - 4*s1*(1 + t1) + 
               3*t2) + 2*(-2 + (1 + s1)*Power(t1,4) + 
               t1*(-24 + 7*s1 - 2*t2) + 2*Power(t1,2)*(5 + t2) - 
               Power(t1,3)*(-15 + 7*s1 + t2)) - 
            2*s*(-10 + (3 + 2*s1)*Power(t1,4) + 
               t1*(-106 + 39*s1 - 17*t2) - 
               Power(t1,3)*(-35 + 27*s1 + t2) + 
               Power(t1,2)*(35 + 7*s1 + 8*t2)) + 
            Power(s,2)*(-32 + 2*(3 + s1)*Power(t1,4) + 
               t1*(-319 + 142*s1 - 75*t2) + 
               Power(t1,3)*(-6 - 66*s1 + 2*t2) + 
               Power(t1,2)*(109 + 55*s1 + 20*t2)) - 
            Power(s,4)*(4 - (99 + 29*s1)*Power(t1,2) + 
               4*(6 + s1)*Power(t1,3) + t1*(113 - 36*s1 + 25*t2)) + 
            Power(s,3)*(20 - 2*Power(t1,4) + 
               Power(t1,3)*(46 + 30*s1 - 2*t2) - 
               Power(t1,2)*(151 + 66*s1 + 8*t2) + 
               t1*(240 - 110*s1 + 67*t2))) + 
         Power(s2,9)*(Power(s1,3)*
             (-10*Power(s,3) + Power(s,2)*(15 - 28*t1) + 
               2*Power(-1 + t1,2)*(5 + 4*t1) + 
               s*(39 - 59*t1 + 20*Power(t1,2))) - 
            Power(s1,2)*(4*Power(s,3)*(-2 + t1 - 9*t2) - 
               Power(-1 + t1,2)*
                (1 + 6*Power(t1,2) - 33*t2 - 7*t1*(1 + 3*t2)) + 
               Power(s,2)*(52 + 65*Power(t1,2) + 60*t2 - 
                  9*t1*(13 + 11*t2)) + 
               s*(-1 + t1)*(-71 + Power(t1,2) - 105*t2 + 
                  t1*(70 + 48*t2))) + 
            s1*(-2*Power(s,3)*
                (2 + 13*t2 + 21*Power(t2,2) - t1*(2 + 9*t2)) - 
               2*Power(-1 + t1,2)*
                (2 - t2 - 18*Power(t2,2) + Power(t1,2)*(2 + 4*t2) - 
                  t1*(4 + 3*t2 + 9*Power(t2,2))) + 
               s*(-1 + t1)*(2 + 8*Power(t1,3) - 124*t2 - 
                  93*Power(t2,2) + 2*Power(t1,2)*(-7 + 10*t2) + 
                  4*t1*(1 + 26*t2 + 9*Power(t2,2))) + 
               Power(s,2)*(22 + 8*Power(t1,3) + 128*t2 + 
                  75*Power(t2,2) + 2*Power(t1,2)*(3 + 77*t2) - 
                  6*t1*(6 + 47*t2 + 19*Power(t2,2)))) + 
            t2*(2*Power(s,3)*
                (4 + 2*Power(t1,2) + 9*t2 + 8*Power(t2,2) - 
                  t1*(6 + 7*t2)) + 
               Power(-1 + t1,2)*
                (4 - 3*t2 - 13*Power(t2,2) + 2*Power(t1,2)*(2 + t2) + 
                  t1*(-8 + t2 - 5*Power(t2,2))) - 
               s*(-1 + t1)*(6 + 4*Power(t1,3) - 53*t2 - 
                  27*Power(t2,2) + Power(t1,2)*(-2 + 19*t2) + 
                  t1*(-8 + 34*t2 + 8*Power(t2,2))) - 
               Power(s,2)*(30 + 76*t2 + 30*Power(t2,2) + 
                  Power(t1,2)*(30 + 89*t2) - 
                  t1*(60 + 165*t2 + 43*Power(t2,2))))) - 
         Power(s2,3)*(-1 + s + t1)*
          (Power(s,8)*(4 - 35*Power(t1,3) + 
               Power(t1,2)*(98 + 73*s1 - 82*t2) + 
               6*(-1 + s1)*Power(t2,2) - 16*Power(t2,3) + 
               2*t1*(-24 + 3*s1*(-4 + t2) + 10*t2 + Power(t2,2))) + 
            Power(s,6)*(145 - 24*Power(s1,3)*Power(t1,2) - 
               66*Power(t1,5) + Power(t1,4)*(343 - 81*t2) + 28*t2 + 
               80*Power(t2,2) - 197*Power(t2,3) + 
               Power(t1,3)*(-1328 + 624*t2 + 17*Power(t2,2)) - 
               Power(t1,2)*(-1858 + 451*t2 + 31*Power(t2,2) + 
                  37*Power(t2,3)) + 
               2*t1*(-479 + 38*t2 + 61*Power(t2,2) + 110*Power(t2,3)) + 
               2*Power(s1,2)*t1*
                (-109 - 35*Power(t1,2) + 11*t2 + t1*(233 + 19*t2)) + 
               s1*(-40 + 90*Power(t1,4) - 26*t2 + 74*Power(t2,2) + 
                  Power(t1,3)*(-743 + 116*t2) + 
                  t1*(-157 + 96*t2 - 67*Power(t2,2)) + 
                  Power(t1,2)*(748 - 538*t2 - 47*Power(t2,2)))) + 
            Power(s,7)*(-36 - 85*Power(t1,4) + 
               Power(t1,3)*(353 - 155*t2) - 9*t2 + 92*Power(t2,3) - 
               2*Power(s1,2)*t1*(-9 + 19*t1 + t2) + 
               Power(t1,2)*(-641 + 415*t2 + 15*Power(t2,2)) - 
               t1*(-325 + 122*t2 + 40*Power(t2,2) + 45*Power(t2,3)) + 
               s1*(4 + 147*Power(t1,3) + 6*t2 - 34*Power(t2,2) + 
                  Power(t1,2)*(-450 + 58*t2) + 
                  2*t1*(80 - 8*t2 + 3*Power(t2,2)))) + 
            (-1 + t1)*(2*Power(s1,3)*Power(t1,3)*(4 + 5*t1) + 
               Power(s1,2)*t1*
                (Power(t1,2)*(25 - 53*t2) + t1*(47 - 17*t2) + 
                  8*Power(t1,3)*(-8 + t2) + 8*(-1 + t2)) + 
               Power(t1,4)*(-44 + 8*t2) + 
               Power(-1 + t2,2)*(-1 + 19*t2) + 
               Power(t1,3)*(133 - 165*t2 + 20*Power(t2,2)) + 
               t1*(47 - 191*t2 + 193*Power(t2,2) - 49*Power(t2,3)) + 
               3*Power(t1,2)*
                (-45 + 109*t2 - 58*Power(t2,2) + 4*Power(t2,3)) - 
               2*s1*(4*Power(-1 + t2,2) + Power(t1,4)*(-49 + 8*t2) + 
                  Power(t1,2)*(-15 + 91*t2 - 46*Power(t2,2)) + 
                  t1*(-23 + 18*t2 + 5*Power(t2,2)) + 
                  Power(t1,3)*(83 - 109*t2 + 10*Power(t2,2)))) + 
            Power(s,4)*(575 - 62*Power(t1,6) + 
               Power(s1,3)*Power(t1,2)*
                (-224 + 165*t1 - 19*Power(t1,2)) + 159*t2 + 
               5*Power(t2,2) - 65*Power(t2,3) + 
               Power(t1,5)*(-11 + 140*t2) + 
               Power(t1,4)*(942 + 79*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(3022 + 3058*t2 + 361*Power(t2,2) - 
                  107*Power(t2,3)) + 
               4*Power(t1,3)*
                (-617 - 464*t2 - 60*Power(t2,2) + 3*Power(t2,3)) + 
               2*t1*(-851 - 936*t2 + 59*Power(t2,2) + 90*Power(t2,3)) + 
               Power(s1,2)*t1*
                (5*Power(t1,4) + Power(t1,3)*(295 + 7*t2) - 
                  4*(375 + 11*t2) - 6*Power(t1,2)*(359 + 38*t2) + 
                  t1*(3534 + 431*t2)) + 
               s1*(-252 - 52*t2 + 50*Power(t2,2) + 
                  Power(t1,5)*(-155 + 16*t2) + 
                  t1*(2588 + 1402*t2 - 108*Power(t2,2)) + 
                  Power(t1,4)*(431 - 498*t2 - 20*Power(t2,2)) + 
                  2*Power(t1,3)*(903 + 1176*t2 + 86*Power(t2,2)) - 
                  2*Power(t1,2)*(2049 + 1853*t2 + 98*Power(t2,2)))) + 
            Power(s,3)*(-494 - 14*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (60 - 137*t1 + 53*Power(t1,2) - 3*Power(t1,3)) - 
               289*t2 + 106*Power(t2,2) + 36*Power(t2,3) + 
               4*Power(t1,6)*(7 + 6*t2) + Power(t1,5)*(58 + 24*t2) - 
               Power(t1,4)*(353 + 630*t2 + 8*Power(t2,2)) + 
               Power(t1,3)*(1504 + 1723*t2 + 110*Power(t2,2)) + 
               t1*(863 + 1800*t2 - 28*Power(t2,2) - 15*Power(t2,3)) + 
               Power(t1,2)*(-1592 - 2451*t2 - 165*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s1,2)*t1*
                (2*Power(t1,5) + Power(t1,4)*(9 - 4*t2) - 
                  Power(t1,3)*(597 + 26*t2) + 2*(561 + 83*t2) - 
                  3*t1*(869 + 86*t2) + Power(t1,2)*(2036 + 222*t2)) + 
               s1*(200 - 14*Power(t1,6) + 78*t2 - 46*Power(t2,2) - 
                  8*Power(t1,5)*(-30 + 13*t2) + 
                  2*Power(t1,4)*(319 + 294*t2 + 8*Power(t2,2)) - 
                  2*Power(t1,3)*(1582 + 1008*t2 + 41*Power(t2,2)) - 
                  2*t1*(1141 + 722*t2 + 44*Power(t2,2)) + 
                  Power(t1,2)*(4181 + 2918*t2 + 98*Power(t2,2)))) - 
            Power(s,5)*(363 + 16*Power(t1,6) + 
               2*Power(s1,3)*Power(t1,2)*(-71 + 20*t1) + 52*t2 + 
               139*Power(t2,2) - 184*Power(t2,3) + 
               Power(t1,5)*(-41 + 8*t2) + 
               Power(t1,4)*(769 - 364*t2 - 4*Power(t2,2)) + 
               Power(t1,3)*(-2504 + 77*t2 - 39*Power(t2,2) + 
                  8*Power(t2,3)) - 
               4*Power(t1,2)*
                (-762 - 275*t2 - 40*Power(t2,2) + 31*Power(t2,3)) + 
               t1*(-1628 - 741*t2 + 126*Power(t2,2) + 
                  353*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-880 + 29*Power(t1,3) + 36*t2 + 8*t1*(247 + 30*t2) - 
                  Power(t1,2)*(734 + 51*t2)) - 
               s1*(148 + 16*Power(t1,5) + 44*t2 - 78*Power(t2,2) + 
                  Power(t1,4)*(-473 + 80*t2) + 
                  Power(t1,3)*(669 - 952*t2 - 67*Power(t2,2)) + 
                  t1*(-939 - 610*t2 + 165*Power(t2,2)) + 
                  Power(t1,2)*(894 + 2090*t2 + 177*Power(t2,2)))) + 
            Power(s,2)*(179 - 12*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (104 - 69*t1 - 7*Power(t1,2) + 8*Power(t1,3)) + 
               Power(t1,5)*(91 - 80*t2) + 202*t2 + 10*Power(t2,2) - 
               87*Power(t2,3) + Power(t1,6)*(30 + 28*t2) + 
               Power(t1,4)*(451 + 53*t2 - 40*Power(t2,2)) + 
               Power(t1,2)*(-46 + 165*t2 + 319*Power(t2,2) - 
                  67*Power(t2,3)) + 
               Power(t1,3)*(-785 + 156*t2 + 65*Power(t2,2) + 
                  8*Power(t2,3)) + 
               2*t1*(46 - 262*t2 - 205*Power(t2,2) + 70*Power(t2,3)) - 
               Power(s1,2)*t1*
                (274 + 37*Power(t1,4) + 4*Power(t1,5) + 
                  Power(t1,2)*(234 - 72*t2) + 162*t2 + 
                  2*Power(t1,3)*(-67 + 3*t2) - t1*(359 + 18*t2)) + 
               s1*(-52 + 2*Power(t1,7) + Power(t1,6)*(52 - 16*t2) - 
                  98*t2 + 54*Power(t2,2) + 2*Power(t1,5)*(13 + 8*t2) + 
                  Power(t1,4)*(-659 - 40*t2 + 8*Power(t2,2)) + 
                  Power(t1,3)*(1159 + 92*t2 + 28*Power(t2,2)) + 
                  t1*(495 + 748*t2 + 141*Power(t2,2)) - 
                  Power(t1,2)*(1023 + 590*t2 + 183*Power(t2,2)))) + 
            s*(-11 - 6*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (-58 + 89*t1 - 25*Power(t1,2) + Power(t1,3)) + 
               Power(t1,5)*(187 - 4*t2) - 18*t2 - 95*Power(t2,2) + 
               72*Power(t2,3) + 4*Power(t1,6)*(13 + 7*t2) + 
               4*Power(t1,4)*(-157 + 43*t2 + Power(t2,2)) + 
               Power(t1,3)*(343 - 811*t2 + 203*Power(t2,2) - 
                  24*Power(t2,3)) + 
               Power(t1,2)*(215 + 964*t2 - 706*Power(t2,2) + 
                  140*Power(t2,3)) - 
               t1*(152 + 331*t2 - 594*Power(t2,2) + 195*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-36 + 6*Power(t1,5) + t1*(317 - 14*t2) + 64*t2 - 
                  Power(t1,4)*(13 + 4*t2) + Power(t1,3)*(60 + 86*t2) - 
                  Power(t1,2)*(334 + 153*t2)) + 
               s1*(-16 + 6*Power(t1,7) + 64*t2 - 34*Power(t2,2) - 
                  2*Power(t1,6)*(17 + 8*t2) + 
                  Power(t1,5)*(-79 + 24*t2) + 
                  Power(t1,3)*(-22 + 808*t2 - 163*Power(t2,2)) + 
                  Power(t1,4)*(273 - 268*t2 + 16*Power(t2,2)) - 
                  3*t1*(-71 + 78*t2 + 17*Power(t2,2)) + 
                  Power(t1,2)*(-341 - 378*t2 + 253*Power(t2,2))))) - 
         Power(s2,7)*(-(Power(-1 + t1,2)*
               (-5 + 2*Power(t1,4) + 
                 6*Power(s1,3)*(1 + 4*t1 + 2*Power(t1,2)) + 31*t2 - 
                 43*Power(t2,2) - 9*Power(t2,3) + 
                 Power(t1,3)*(-1 + 9*t2 - 4*Power(t2,2)) + 
                 t1*(13 - 53*t2 + 45*Power(t2,2) - 21*Power(t2,3)) + 
                 Power(t1,2)*
                  (-9 + 13*t2 + 2*Power(t2,2) - 12*Power(t2,3)) + 
                 Power(s1,2)*
                  (-4 + 2*Power(t1,4) + Power(t1,2)*(5 - 35*t2) + 
                    Power(t1,3)*(11 - 7*t2) - 34*t2 - 2*t1*(7 + 25*t2)) \
- 2*s1*(9 + 2*Power(t1,4) - 18*t2 - 20*Power(t2,2) + 
                    Power(t1,2)*(-7 + 17*t2 - 22*Power(t2,2)) + 
                    Power(t1,3)*(5 + t2 - 2*Power(t2,2)) - 
                    t1*(9 + 19*Power(t2,2))))) + 
            Power(s,5)*(20*Power(s1,3) + Power(t1,3) - 
               8*Power(s1,2)*(1 + 5*t1 + 15*t2) - 
               6*Power(t1,2)*(-3 + 23*t2) + 
               t1*(-31 + 220*t2 + 70*Power(t2,2)) - 
               2*(-6 + 50*t2 + 65*Power(t2,2) + 56*Power(t2,3)) + 
               s1*(77*Power(t1,2) + 2*t1*(-52 + 5*t2) + 
                  5*(9 + 20*t2 + 42*Power(t2,2)))) + 
            s*(-1 + t1)*(-21 - 12*Power(t1,5) - 
               Power(s1,3)*(13 - 37*t1 + 18*Power(t1,2) + 
                  35*Power(t1,3)) + 69*t2 + 56*Power(t2,2) + 
               49*Power(t2,3) + 
               Power(t1,4)*(21 + 42*t2 + 4*Power(t2,2)) + 
               t1*(78 + 82*t2 - 125*Power(t2,2) - 52*Power(t2,3)) + 
               Power(t1,3)*(30 + 136*t2 + 147*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-96 - 329*t2 - 82*Power(t2,2) + 
                  24*Power(t2,3)) + 
               Power(s1,2)*(-15*Power(t1,4) + 15*(3 + 8*t2) + 
                  Power(t1,2)*(-20 + 33*t2) + 
                  Power(t1,3)*(139 + 114*t2) - t1*(149 + 180*t2)) + 
               s1*(-115 + 4*Power(t1,5) - 76*t2 - 160*Power(t2,2) + 
                  2*Power(t1,4)*(-21 + 8*t2) + 
                  Power(t1,2)*(223 + 192*t2 - 51*Power(t2,2)) - 
                  Power(t1,3)*(114 + 326*t2 + 83*Power(t2,2)) + 
                  t1*(44 + 194*t2 + 207*Power(t2,2)))) + 
            Power(s,3)*(67 + 5*Power(t1,5) + 
               Power(s1,3)*(-53 + 192*t1 - 123*Power(t1,2)) + 133*t2 + 
               50*Power(t2,2) - 15*Power(t2,3) - 
               2*Power(t1,4)*(19 + 30*t2) + 
               5*Power(t1,3)*(27 + 116*t2 + 86*Power(t2,2)) + 
               t1*(-60 - 93*t2 + 104*Power(t2,2) + 115*Power(t2,3)) - 
               Power(t1,2)*(109 + 560*t2 + 608*Power(t2,2) + 
                  162*Power(t2,3)) + 
               Power(s1,2)*(137 + 139*Power(t1,3) + 156*t2 + 
                  Power(t1,2)*(85 + 232*t2) - t1*(385 + 482*t2)) + 
               s1*(-43*Power(t1,4) - 2*Power(t1,3)*(117 + 209*t2) + 
                  5*Power(t1,2)*(34 + 24*t2 + 7*Power(t2,2)) - 
                  2*(70 + 144*t2 + 53*Power(t2,2)) + 
                  t1*(247 + 634*t2 + 211*Power(t2,2)))) - 
            Power(s,2)*(108 - 2*Power(t1,6) + 
               Power(s1,3)*(-61 + 182*t1 - 296*Power(t1,2) + 
                  162*Power(t1,3)) - 3*Power(t1,5)*(-17 + t2) + 50*t2 + 
               193*Power(t2,2) + 41*Power(t2,3) - 
               3*Power(t1,4)*(54 + 65*t2 + 31*Power(t2,2)) - 
               Power(t1,3)*(-92 + 128*t2 + 325*Power(t2,2) + 
                  17*Power(t2,3)) + 
               Power(t1,2)*(216 + 903*t2 + 965*Power(t2,2) + 
                  113*Power(t2,3)) - 
               t1*(303 + 627*t2 + 740*Power(t2,2) + 124*Power(t2,3)) + 
               Power(s1,2)*(14*Power(t1,4) + 6*(27 + 23*t2) - 
                  4*t1*(185 + 134*t2) - Power(t1,3)*(601 + 439*t2) + 
                  Power(t1,2)*(1165 + 876*t2)) + 
               s1*(18*Power(t1,5) + 4*Power(t1,4)*(28 + 3*t2) - 
                  3*(5 + 134*t2 + 42*Power(t2,2)) + 
                  2*Power(t1,3)*(126 + 587*t2 + 151*Power(t2,2)) - 
                  3*Power(t1,2)*(309 + 824*t2 + 239*Power(t2,2)) + 
                  2*t1*(280 + 844*t2 + 251*Power(t2,2)))) + 
            Power(s,4)*(3 + 4*Power(t1,4) + 7*Power(s1,3)*(-5 + 12*t1) + 
               Power(t1,3)*(19 - 201*t2) + 213*t2 + 334*Power(t2,2) + 
               208*Power(t2,3) + 
               Power(t1,2)*(-14 + 791*t2 + 495*Power(t2,2)) - 
               t1*(12 + 802*t2 + 930*Power(t2,2) + 343*Power(t2,3)) + 
               Power(s1,2)*(184*Power(t1,2) - 5*t1*(77 + 80*t2) + 
                  2*(61 + 94*t2)) + 
               s1*(-149 + 56*Power(t1,3) - 350*t2 - 350*Power(t2,2) - 
                  4*Power(t1,2)*(105 + 137*t2) + 
                  2*t1*(256 + 539*t2 + 324*Power(t2,2))))) + 
         Power(s2,8)*(Power(s,4)*
             (2 + 20*Power(s1,3) + Power(t1,2)*(4 - 37*t2) - 45*t2 - 
               66*Power(t2,2) - 56*Power(t2,3) - 
               2*Power(s1,2)*(6 + 5*t1 + 45*t2) + 
               t1*(-6 + 80*t2 + 42*Power(t2,2)) + 
               s1*(19 + 13*Power(t1,2) + 70*t2 + 126*Power(t2,2) - 
                  6*t1*(5 + 4*t2))) - 
            Power(-1 + t1,2)*
             (-2 + 6*Power(s1,3)*(2 + 4*t1 + Power(t1,2)) + 11*t2 - 
               6*Power(t2,2) - 19*Power(t2,3) + 
               Power(t1,3)*(2 + 4*t2) + 
               t1*(6 - 18*t2 + 7*Power(t2,2) - 21*Power(t2,3)) - 
               Power(t1,2)*(6 - 3*t2 + Power(t2,2) + 2*Power(t2,3)) + 
               Power(s1,2)*(6 + 6*Power(t1,3) - 46*t2 - 
                  Power(t1,2)*(1 + 17*t2) - t1*(11 + 63*t2)) + 
               s1*(-7 - 2*t2 + 53*Power(t2,2) - 
                  4*Power(t1,3)*(2 + t2) + 
                  Power(t1,2)*(9 - 4*t2 + 13*Power(t2,2)) + 
                  2*t1*(3 + 5*t2 + 30*Power(t2,2)))) - 
            Power(s,2)*(6 + Power(s1,3)*
                (70 - 160*t1 + 63*Power(t1,2)) - 90*t2 - 
               141*Power(t2,2) - 53*Power(t2,3) - 
               Power(t1,4)*(2 + 9*t2) - Power(t1,3)*t2*(92 + 157*t2) + 
               Power(t1,2)*(12 + 121*t2 + 112*Power(t2,2) + 
                  27*Power(t2,3)) + 
               t1*(-16 + 70*t2 + 186*Power(t2,2) + 53*Power(t2,3)) - 
               Power(s1,2)*(179 + 39*Power(t1,3) + 220*t2 + 
                  18*Power(t1,2)*(9 + 7*t2) - t1*(380 + 427*t2)) + 
               s1*(108 + 45*Power(t1,4) + 338*t2 + 203*Power(t2,2) + 
                  2*Power(t1,3)*(-17 + 89*t2) - 
                  20*t1*(8 + 31*t2 + 16*Power(t2,2)) + 
                  Power(t1,2)*(41 + 104*t2 + 36*Power(t2,2)))) - 
            s*(-1 + t1)*(-4 + 
               5*Power(s1,3)*(-9 - 5*t1 + 11*Power(t1,2)) + 
               Power(t1,4)*(2 - 4*t2) - 67*t2 + 19*Power(t2,2) + 
               23*Power(t2,3) - 
               Power(t1,3)*(2 + 18*t2 + 13*Power(t2,2)) + 
               2*t1*(5 + 54*t2 + 63*Power(t2,2) + 6*Power(t2,3)) - 
               Power(t1,2)*(6 + 19*t2 + 132*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(s1,2)*(39 + 27*Power(t1,3) + 98*t2 + 
                  2*t1*(63 + 46*t2) - Power(t1,2)*(192 + 145*t2)) + 
               s1*(73 + 10*Power(t1,4) - 56*t2 - 76*Power(t2,2) - 
                  2*Power(t1,3)*(3 + 8*t2) + 
                  55*Power(t1,2)*(1 + 6*t2 + 2*Power(t2,2)) - 
                  t1*(132 + 258*t2 + 79*Power(t2,2)))) + 
            Power(s,3)*(6 + Power(s1,3)*(-25 + 66*t1) + 
               Power(t1,3)*(8 - 32*t2) + 117*t2 + 206*Power(t2,2) + 
               93*Power(t2,3) + 
               Power(s1,2)*(106 - 277*t1 + 146*Power(t1,2) + 122*t2 - 
                  272*t1*t2) + 
               2*Power(t1,2)*(-5 + 120*t2 + 141*Power(t2,2)) - 
               t1*(4 + 325*t2 + 513*Power(t2,2) + 161*Power(t2,3)) - 
               s1*(22*Power(t1,3) + Power(t1,2)*(90 + 406*t2) + 
                  5*(15 + 58*t2 + 38*Power(t2,2)) - 
                  t1*(187 + 746*t2 + 367*Power(t2,2))))) + 
         Power(s2,4)*(Power(s,8)*
             (18 - 30*Power(t1,3) + Power(t1,2)*(136 - 219*t2) - 
               13*t2 - 38*Power(t2,2) - 56*Power(t2,3) - 
               6*Power(s1,2)*(t1 + t2) + 
               t1*(-109 + 112*t2 + 14*Power(t2,2)) + 
               s1*(185*Power(t1,2) + 2*t1*(-55 + 16*t2) + 
                  6*(2 + t2 + 7*Power(t2,2)))) + 
            Power(-1 + t1,2)*
             (20 + 6*Power(s1,3)*Power(t1,2)*
                (1 + 4*t1 + 2*Power(t1,2)) - 31*t2 - 22*Power(t2,2) + 
               33*Power(t2,3) + 4*Power(t1,4)*(-11 + 3*t2) + 
               2*Power(t1,3)*(56 - 117*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(-72 + 401*t2 - 317*Power(t2,2) + 
                  30*Power(t2,3)) - 
               t1*(16 + 148*t2 - 299*Power(t2,2) + 105*Power(t2,3)) + 
               Power(s1,2)*(Power(t1,2)*(121 - 91*t2) + 4*(-1 + t2) + 
                  4*Power(t1,4)*(-17 + 3*t2) + t1*(19 + 11*t2) - 
                  2*Power(t1,3)*(34 + 31*t2)) - 
               s1*(3 - 34*t2 + 31*Power(t2,2) + 
                  4*Power(t1,4)*(-25 + 6*t2) + 
                  Power(t1,2)*(167 + 56*t2 - 145*Power(t2,2)) + 
                  4*Power(t1,3)*(17 - 74*t2 + 10*Power(t2,2)) - 
                  2*t1*(69 - 125*t2 + 26*Power(t2,2)))) + 
            Power(s,7)*(-109 + 4*Power(s1,3)*t1 - 90*Power(t1,4) + 
               Power(t1,3)*(411 - 570*t2) + 35*t2 + 106*Power(t2,2) + 
               275*Power(t2,3) + 
               Power(t1,2)*(-825 + 1238*t2 + 114*Power(t2,2)) - 
               t1*(-566 + 613*t2 + 285*Power(t2,2) + 203*Power(t2,3)) - 
               2*Power(s1,2)*
                (6 + 13*Power(t1,2) - 16*t2 + t1*(-19 + 17*t2)) + 
               s1*(-79 + 480*Power(t1,3) + 10*t2 - 200*Power(t2,2) + 
                  Power(t1,2)*(-1205 + 106*t2) + 
                  t1*(713 - 50*t2 + 129*Power(t2,2)))) + 
            Power(s,6)*(255 + 24*Power(s1,3)*(1 - 4*t1)*t1 - 
               94*Power(t1,5) + Power(t1,4)*(255 - 501*t2) + 22*t2 - 
               75*Power(t2,2) - 501*Power(t2,3) + 
               Power(t1,3)*(-1384 + 2230*t2 + 189*Power(t2,2)) - 
               Power(t1,2)*(-2342 + 2089*t2 + 566*Power(t2,2) + 
                  237*Power(t2,3)) + 
               t1*(-1337 + 628*t2 + 770*Power(t2,2) + 
                  805*Power(t2,3)) + 
               Power(s1,2)*(122 - 60*Power(t1,3) - 72*t2 + 
                  t1*(-550 + 133*t2) + Power(t1,2)*(721 + 148*t2)) + 
               s1*(104 + 429*Power(t1,4) - 126*t2 + 359*Power(t2,2) + 
                  Power(t1,3)*(-2180 + 238*t2) - 
                  2*Power(t1,2)*(-1274 + 522*t2 + 23*Power(t2,2)) - 
                  4*t1*(274 - 91*t2 + 131*Power(t2,2)))) - 
            Power(s,2)*(199 + 14*Power(t1,8) + 
               Power(s1,3)*t1*
                (108 + 20*t1 - 208*Power(t1,2) + 200*Power(t1,3) - 
                  124*Power(t1,4) + 11*Power(t1,5)) - 114*t2 + 
               31*Power(t2,2) - 105*Power(t2,3) - 
               4*Power(t1,7)*(-8 + 5*t2) - 16*Power(t1,6)*(20 + 9*t2) - 
               6*Power(t1,5)*(-31 - 59*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(1954 + 1278*t2 - 429*Power(t2,2) - 
                  14*Power(t2,3)) + 
               Power(t1,4)*(-281 - 775*t2 + 170*Power(t2,2) + 
                  4*Power(t2,3)) - 
               Power(t1,2)*(2300 + 921*t2 - 812*Power(t2,2) + 
                  25*Power(t2,3)) + 
               t1*(516 + 342*t2 - 560*Power(t2,2) + 133*Power(t2,3)) + 
               Power(s1,2)*(-38*Power(t1,6) + 8*Power(t1,7) + 
                  t1*(336 - 223*t2) - 4*Power(t1,5)*(-155 + t2) + 
                  Power(t1,4)*(-1431 + 14*t2) + 
                  4*Power(t1,2)*(-211 + 25*t2) - 2*(73 + 38*t2) + 
                  Power(t1,3)*(1495 + 168*t2)) + 
               s1*(282 - 12*Power(t1,7) + 190*t2 + 135*Power(t2,2) + 
                  Power(t1,6)*(79 + 88*t2) + 
                  Power(t1,2)*(2560 + 784*t2 - 498*Power(t2,2)) + 
                  Power(t1,4)*(2113 + 312*t2 - 84*Power(t2,2)) + 
                  4*Power(t1,5)*(-124 - 73*t2 + 6*Power(t2,2)) + 
                  8*t1*(-131 - 43*t2 + 29*Power(t2,2)) + 
                  Power(t1,3)*(-3478 - 738*t2 + 212*Power(t2,2)))) + 
            Power(s,3)*(75 - 138*Power(t1,7) + 
               Power(s1,3)*t1*
                (-228 + 814*t1 - 982*Power(t1,2) + 547*Power(t1,3) - 
                  72*Power(t1,4)) - 723*t2 + 184*Power(t2,2) - 
               39*Power(t2,3) + 2*Power(t1,6)*(99 + 92*t2) + 
               28*Power(t1,3)*(124 + 201*t2 + 18*Power(t2,2)) + 
               Power(t1,5)*(883 + 436*t2 + 48*Power(t2,2)) - 
               Power(t1,4)*(2173 + 2942*t2 + 338*Power(t2,2)) - 
               t1*(-1727 - 2251*t2 + 123*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-4044 - 4834*t2 - 278*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s1,2)*(-580 - 23*Power(t1,6) - 76*t2 + 
                  26*t1*(116 + 13*t2) + Power(t1,5)*(431 + 40*t2) - 
                  2*Power(t1,4)*(1615 + 193*t2) - 
                  3*Power(t1,2)*(2089 + 304*t2) + 
                  Power(t1,3)*(6650 + 806*t2)) + 
               s1*(1081 + 4*Power(t1,7) + 554*t2 + 68*Power(t2,2) + 
                  Power(t1,6)*(-73 + 24*t2) - 
                  4*Power(t1,5)*(82 + 183*t2 + 10*Power(t2,2)) - 
                  t1*(4517 + 3118*t2 + 13*Power(t2,2)) - 
                  2*Power(t1,3)*(4535 + 2922*t2 + 166*Power(t2,2)) + 
                  Power(t1,4)*(3494 + 3336*t2 + 202*Power(t2,2)) + 
                  Power(t1,2)*(9409 + 5786*t2 + 258*Power(t2,2)))) + 
            s*(-1 + t1)*(-61 - 10*Power(t1,7) + 
               Power(s1,3)*t1*
                (-86 + 45*t1 + 28*Power(t1,2) + 33*Power(t1,3) + 
                  9*Power(t1,4)) - 123*t2 - 19*Power(t2,2) + 
               99*Power(t2,3) + 2*Power(t1,6)*(5 + 12*t2) + 
               Power(t1,5)*(209 + 84*t2) + 
               Power(t1,4)*(-359 - 26*t2 + 64*Power(t2,2)) - 
               Power(t1,3)*(66 + 634*t2 + 63*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(322 + 767*t2 - 658*Power(t2,2) + 
                  108*Power(t2,3)) - 
               t1*(45 + 92*t2 - 676*Power(t2,2) + 220*Power(t2,3)) - 
               Power(s1,2)*(16 + 6*Power(t1,6) + 
                  Power(t1,4)*(73 - 38*t2) + Power(t1,5)*(39 - 4*t2) - 
                  30*t2 + Power(t1,2)*(86 + 139*t2) - 
                  t1*(269 + 208*t2) + Power(t1,3)*(49 + 228*t2)) + 
               s1*(40 + 2*Power(t1,7) + 44*t2 - 106*Power(t2,2) - 
                  2*Power(t1,6)*(1 + 4*t2) - Power(t1,5)*(87 + 32*t2) + 
                  Power(t1,4)*(227 - 28*t2 - 24*Power(t2,2)) - 
                  2*Power(t1,3)*(59 - 390*t2 + 13*Power(t2,2)) - 
                  t1*(3 + 638*t2 + 57*Power(t2,2)) + 
                  Power(t1,2)*(-59 - 118*t2 + 300*Power(t2,2)))) + 
            Power(s,5)*(-315 - 38*Power(t1,6) + 
               Power(s1,3)*t1*(-246 + 597*t1 - 239*Power(t1,2)) - 
               347*t2 + 75*Power(t2,2) + 407*Power(t2,3) - 
               9*Power(t1,5)*(31 + 18*t2) + 
               Power(t1,4)*(-437 + 1568*t2 + 105*Power(t2,2)) + 
               t1*(1995 + 1457*t2 - 457*Power(t2,2) - 
                  995*Power(t2,3)) - 
               Power(t1,3)*(-3220 + 1137*t2 + 221*Power(t2,2) + 
                  100*Power(t2,3)) + 
               Power(t1,2)*(-3956 - 1285*t2 + 294*Power(t2,2) + 
                  640*Power(t2,3)) + 
               Power(s1,2)*(-464 - 64*Power(t1,4) + 74*t2 + 
                  t1*(2607 + 98*t2) + Power(t1,3)*(1709 + 373*t2) - 
                  Power(t1,2)*(4130 + 803*t2)) + 
               s1*(366 + 156*Power(t1,5) + 376*t2 - 294*Power(t2,2) + 
                  18*Power(t1,4)*(-83 + 16*t2) + 
                  Power(t1,3)*(1791 - 2698*t2 - 314*Power(t2,2)) + 
                  5*Power(t1,2)*(104 + 980*t2 + 63*Power(t2,2)) + 
                  t1*(-1415 - 2362*t2 + 493*Power(t2,2)))) + 
            Power(s,4)*(194 - 4*Power(t1,7) + 
               2*Power(s1,3)*t1*
                (234 - 585*t1 + 492*Power(t1,2) - 100*Power(t1,3)) + 
               820*t2 - 218*Power(t2,2) - 125*Power(t2,3) - 
               Power(t1,6)*(383 + 12*t2) + 
               Power(t1,5)*(445 + 622*t2 + 16*Power(t2,2)) + 
               Power(t1,2)*(4514 + 6557*t2 + 851*Power(t2,2) - 
                  381*Power(t2,3)) + 
               Power(t1,4)*(1985 + 531*t2 + 127*Power(t2,2) - 
                  10*Power(t2,3)) + 
               Power(t1,3)*(-4497 - 4810*t2 - 799*Power(t2,2) + 
                  127*Power(t2,3)) + 
               t1*(-2254 - 3376*t2 - 127*Power(t2,2) + 
                  379*Power(t2,3)) + 
               Power(s1,2)*(776 - 39*Power(t1,5) - 2*t2 + 
                  3*Power(t1,4)*(466 + 79*t2) - t1*(4511 + 583*t2) - 
                  Power(t1,3)*(6236 + 1231*t2) + 
                  Power(t1,2)*(8424 + 1429*t2)) + 
               s1*(-1159 + 26*Power(t1,6) - 620*t2 + 85*Power(t2,2) + 
                  2*Power(t1,5)*(-243 + 74*t2) + 
                  t1*(5190 + 4426*t2 + 82*Power(t2,2)) - 
                  Power(t1,4)*(174 + 2328*t2 + 221*Power(t2,2)) - 
                  Power(t1,2)*(8603 + 8922*t2 + 678*Power(t2,2)) + 
                  Power(t1,3)*(4874 + 7634*t2 + 810*Power(t2,2))))) - 
         Power(s2,5)*(Power(s,7)*
             (2*Power(s1,3) - 10*Power(t1,3) - 
               4*Power(s1,2)*(7*t1 + 9*t2) - 
               8*Power(t1,2)*(-13 + 40*t2) + 
               t1*(-125 + 260*t2 + 42*Power(t2,2)) - 
               2*(-16 + 30*t2 + 51*Power(t2,2) + 56*Power(t2,3)) + 
               s1*(43 + 250*Power(t1,2) + 34*t2 + 126*Power(t2,2) + 
                  6*t1*(-34 + 11*t2))) + 
            Power(-1 + t1,2)*
             (28 + 6*Power(s1,3)*Power(t1,2)*(2 + 4*t1 + Power(t1,2)) - 
               98*t2 + 57*Power(t2,2) + 23*Power(t2,3) + 
               2*Power(t1,4)*(-9 + 4*t2) + 
               2*Power(t1,3)*(13 - 73*t2 + 20*Power(t2,2)) + 
               t1*(-66 + 66*t2 + 173*Power(t2,2) - 105*Power(t2,3)) + 
               10*Power(t1,2)*
                (3 + 17*t2 - 27*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s1,2)*(-3 + Power(t1,2)*(60 - 114*t2) + 
                  t1*(83 - 15*t2) + 13*t2 + Power(t1,4)*(-30 + 8*t2) - 
                  2*Power(t1,3)*(55 + 9*t2)) - 
               2*s1*(-17 + 5*t2 + 22*Power(t2,2) + 
                  Power(t1,4)*(-21 + 8*t2) + 
                  t1*(-38 + 171*t2 - 65*Power(t2,2)) + 
                  Power(t1,3)*(-30 - 82*t2 + 20*Power(t2,2)) - 
                  2*Power(t1,2)*(-53 + 51*t2 + 20*Power(t2,2)))) + 
            Power(s,5)*(150 - 6*Power(t1,5) + 
               Power(s1,3)*(-2 + 73*t1 - 165*Power(t1,2)) - 28*t2 - 
               279*Power(t2,2) - 459*Power(t2,3) - 
               20*Power(t1,4)*(11 + 27*t2) + 
               Power(t1,3)*(32 + 2321*t2 + 463*Power(t2,2)) - 
               Power(t1,2)*(-874 + 2299*t2 + 1193*Power(t2,2) + 
                  372*Power(t2,3)) + 
               t1*(-764 + 730*t2 + 1237*Power(t2,2) + 
                  939*Power(t2,3)) + 
               Power(s1,2)*(148 + 132*Power(t1,3) - 149*t2 + 
                  4*Power(t1,2)*(99 + 71*t2) + t1*(-424 + 179*t2)) + 
               s1*(175 + 414*Power(t1,4) - 76*t2 + 451*Power(t2,2) - 
                  3*Power(t1,3)*(657 + 8*t2) + 
                  t1*(-1120 + 220*t2 - 809*Power(t2,2)) + 
                  Power(t1,2)*(2312 - 624*t2 + 26*Power(t2,2)))) + 
            Power(s,6)*(-107 - 20*Power(t1,4) + 
               2*Power(s1,3)*(-5 + 12*t1) + 
               Power(t1,3)*(160 - 730*t2) + 166*t2 + 272*Power(t2,2) + 
               390*Power(t2,3) + 
               Power(t1,2)*(-407 + 1697*t2 + 327*Power(t2,2)) - 
               t1*(-373 + 1132*t2 + 729*Power(t2,2) + 
                  385*Power(t2,3)) + 
               Power(s1,2)*(-6 + 53*Power(t1,2) + 128*t2 - 
                  t1*(64 + 155*t2)) + 
               s1*(-211 + 552*Power(t1,3) - 56*t2 - 415*Power(t2,2) - 
                  Power(t1,2)*(1435 + 34*t2) + 
                  2*t1*(544 + 115*t2 + 205*Power(t2,2)))) - 
            s*(-1 + t1)*(99 + 4*Power(t1,7) + 
               Power(s1,3)*(38 + 85*t1 - 36*Power(t1,2) - 
                  10*Power(t1,3) - 67*Power(t1,4) + 5*Power(t1,5)) + 
               Power(t1,6)*(22 - 6*t2) + 38*t2 - 35*Power(t2,2) - 
               79*Power(t2,3) - Power(t1,5)*(131 + 92*t2) + 
               Power(t1,4)*(79 - 152*t2 - 88*Power(t2,2)) + 
               2*Power(t1,3)*
                (85 + 372*t2 + 57*Power(t2,2) - 8*Power(t2,3)) + 
               t1*(-155 + 64*t2 - 520*Power(t2,2) + 32*Power(t2,3)) + 
               Power(t1,2)*(-88 - 596*t2 + 529*Power(t2,2) + 
                  48*Power(t2,3)) + 
               Power(s1,2)*(-86 + 6*Power(t1,6) + 
                  Power(t1,4)*(209 - 42*t2) - 121*t2 - 
                  Power(t1,5)*(25 + 4*t2) + 
                  2*Power(t1,3)*(-73 + 98*t2) - 3*t1*(45 + 116*t2) + 
                  Power(t1,2)*(177 + 274*t2)) + 
               s1*(47 - 12*Power(t1,6) + 64*t2 + 179*Power(t2,2) + 
                  3*Power(t1,5)*(45 + 8*t2) - 
                  2*Power(t1,3)*(-79 + 386*t2 + 5*Power(t2,2)) + 
                  Power(t1,4)*(-203 + 148*t2 + 48*Power(t2,2)) - 
                  2*Power(t1,2)*(70 - 6*t2 + 205*Power(t2,2)) + 
                  t1*(15 + 524*t2 + 238*Power(t2,2)))) + 
            Power(s,2)*(-74*Power(t1,7) + 
               Power(s1,3)*(2 - 130*t1 + 282*Power(t1,2) - 
                  516*Power(t1,3) + 383*Power(t1,4) - 54*Power(t1,5)) + 
               3*Power(t1,6)*(31 + 34*t2) + 
               Power(t1,5)*(624 + 490*t2 + 72*Power(t2,2)) + 
               Power(t1,3)*(608 + 1208*t2 - 334*Power(t2,2) - 
                  46*Power(t2,3)) + 
               t1*(762 + 74*t2 + 43*Power(t2,2) - 43*Power(t2,3)) + 
               2*Power(t1,4)*
                (-629 - 667*t2 - 53*Power(t2,2) + 4*Power(t2,3)) + 
               2*(-199 + 86*t2 - 65*Power(t2,2) + 30*Power(t2,3)) + 
               Power(t1,2)*(-357 - 712*t2 + 455*Power(t2,2) + 
                  54*Power(t2,3)) + 
               Power(s1,2)*(-78 - 21*Power(t1,6) + 74*t2 + 
                  7*t1*(97 + 3*t2) - 8*Power(t1,3)*(-349 + 29*t2) + 
                  Power(t1,5)*(318 + 62*t2) - 
                  2*Power(t1,4)*(813 + 80*t2) + 
                  Power(t1,2)*(-2064 + 334*t2)) + 
               s1*(79 + 12*Power(t1,7) + 100*t2 - 139*Power(t2,2) + 
                  2*Power(t1,6)*(-25 + 8*t2) + 
                  Power(t1,2)*(3152 - 462*t2 - 302*Power(t2,2)) + 
                  Power(t1,4)*(1855 + 1436*t2 - 20*Power(t2,2)) - 
                  8*Power(t1,5)*(76 + 61*t2 + 5*Power(t2,2)) + 
                  2*t1*(-713 + 111*t2 + 23*Power(t2,2)) + 
                  Power(t1,3)*(-3014 - 824*t2 + 356*Power(t2,2)))) + 
            Power(s,4)*(-255 + 8*Power(t1,6) + 
               Power(s1,3)*(104 - 606*t1 + 866*Power(t1,2) - 
                  354*Power(t1,3)) - 408*t2 + 25*Power(t2,2) + 
               167*Power(t2,3) - 2*Power(t1,5)*(275 + 69*t2) + 
               Power(t1,4)*(910 + 1251*t2 + 208*Power(t2,2)) + 
               t1*(1005 + 2064*t2 + 251*Power(t2,2) - 
                  453*Power(t2,3)) - 
               Power(t1,3)*(-861 + 190*t2 + 199*Power(t2,2) + 
                  105*Power(t2,3)) + 
               Power(t1,2)*(-1979 - 2331*t2 - 231*Power(t2,2) + 
                  433*Power(t2,3)) + 
               Power(s1,2)*(-633 + 63*Power(t1,4) - 71*t2 + 
                  Power(t1,3)*(1412 + 707*t2) + t1*(2944 + 799*t2) - 
                  Power(t1,2)*(3704 + 1387*t2)) + 
               s1*(546 + 152*Power(t1,5) + 574*t2 - 74*Power(t2,2) + 
                  Power(t1,4)*(-1081 + 160*t2) + 
                  Power(t1,3)*(306 - 2750*t2 - 494*Power(t2,2)) - 
                  6*t1*(311 + 603*t2 + 39*Power(t2,2)) + 
                  Power(t1,2)*(1695 + 5498*t2 + 702*Power(t2,2)))) + 
            Power(s,3)*(451 + 4*Power(t1,7) - 
               2*Power(s1,3)*
                (67 - 296*t1 + 613*Power(t1,2) - 496*Power(t1,3) + 
                  111*Power(t1,4)) + 218*t2 + 192*Power(t2,2) + 
               10*Power(t2,3) - 8*Power(t1,6)*(43 + t2) + 
               Power(t1,5)*(693 + 419*t2 + 24*Power(t2,2)) + 
               Power(t1,4)*(797 + 1105*t2 + 338*Power(t2,2)) - 
               Power(t1,3)*(2322 + 4327*t2 + 1292*Power(t2,2) + 
                  16*Power(t2,3)) - 
               t1*(907 + 2444*t2 + 514*Power(t2,2) + 18*Power(t2,3)) + 
               Power(t1,2)*(1628 + 5037*t2 + 1336*Power(t2,2) + 
                  44*Power(t2,3)) + 
               Power(s1,2)*(658 - 9*Power(t1,5) + 162*t2 - 
                  40*Power(t1,3)*(113 + 33*t2) + 
                  2*Power(t1,4)*(577 + 190*t2) - 3*t1*(1081 + 192*t2) + 
                  2*Power(t1,2)*(3022 + 685*t2)) + 
               s1*(-713 + 52*Power(t1,6) - 630*t2 - 84*Power(t2,2) + 
                  Power(t1,5)*(-359 + 112*t2) - 
                  2*Power(t1,4)*(539 + 1104*t2 + 147*Power(t2,2)) + 
                  t1*(3712 + 2934*t2 + 310*Power(t2,2)) - 
                  Power(t1,2)*(5809 + 6856*t2 + 802*Power(t2,2)) + 
                  Power(t1,3)*(4195 + 6480*t2 + 832*Power(t2,2))))) + 
         Power(s2,6)*(Power(s,6)*
             (10*Power(s1,3) + Power(t1,3) + 
               Power(t1,2)*(50 - 275*t2) - 
               2*Power(s1,2)*(1 + 25*t1 + 45*t2) + 
               t1*(-81 + 320*t2 + 70*Power(t2,2)) - 
               2*(-14 + 55*t2 + 75*Power(t2,2) + 70*Power(t2,3)) + 
               s1*(61 + 190*Power(t1,2) + 80*t2 + 210*Power(t2,2) + 
                  4*t1*(-49 + 15*t2))) + 
            Power(-1 + t1,3)*
             (-14 + Power(s1,3)*
                (1 + 9*t1 + 9*Power(t1,2) + Power(t1,3)) + 82*t2 - 
               90*Power(t2,2) - 5*Power(t2,3) + 
               Power(t1,3)*(-5 + 2*t2) + 
               Power(t1,2)*(-4 - 37*t2 + 20*Power(t2,2)) + 
               t1*(23 - 47*t2 - 75*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s1,2)*(-8 - 21*t2 + Power(t1,3)*(-7 + 2*t2) + 
                  3*Power(t1,2)*(-18 + 5*t2) - t1*(76 + 11*t2)) + 
               s1*(-44 + Power(t1,3)*(11 - 4*t2) + 64*t2 + 
                  35*Power(t2,2) + t1*(-16 + 208*t2 - 45*Power(t2,2)) + 
                  Power(t1,2)*(49 + 22*t2 - 20*Power(t2,2)))) + 
            Power(s,4)*(103 + 17*Power(t1,5) + 
               Power(s1,3)*(-21 + 134*t1 - 170*Power(t1,2)) + 30*t2 - 
               212*Power(t2,2) - 223*Power(t2,3) - 
               Power(t1,4)*(193 + 295*t2) + 
               3*Power(t1,3)*(136 + 501*t2 + 200*Power(t2,2)) - 
               Power(t1,2)*(19 + 1465*t2 + 1211*Power(t2,2) + 
                  335*Power(t2,3)) + 
               t1*(-316 + 302*t2 + 852*Power(t2,2) + 565*Power(t2,3)) + 
               Power(s1,2)*(69 + 223*Power(t1,3) - 45*t2 - 
                  10*t1*(25 + 12*t2) + Power(t1,2)*(39 + 315*t2)) + 
               s1*(69 + 151*Power(t1,4) - 52*t2 + 213*Power(t2,2) - 
                  4*Power(t1,3)*(255 + 98*t2) + 
                  t1*(-334 + 250*t2 - 398*Power(t2,2)) + 
                  Power(t1,2)*(1059 + 84*t2 + 85*Power(t2,2)))) + 
            Power(s,2)*(-191 + 2*Power(t1,7) + 
               Power(s1,3)*(20 + 144*t1 - 461*Power(t1,2) + 
                  388*Power(t1,3) - 116*Power(t1,4)) + 262*t2 - 
               256*Power(t2,2) - 42*Power(t2,3) - 
               Power(t1,6)*(117 + 2*t2) + 
               Power(t1,5)*(308 + 189*t2 + 16*Power(t2,2)) + 
               Power(t1,4)*(119 + 668*t2 + 381*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,2)*(541 + 1760*t2 + 578*Power(t2,2) + 
                  14*Power(t2,3)) - 
               Power(t1,3)*(874 + 2124*t2 + 948*Power(t2,2) + 
                  45*Power(t2,3)) + 
               t1*(212 - 753*t2 + 229*Power(t2,2) + 88*Power(t2,3)) + 
               Power(s1,2)*(97 - 3*Power(t1,5) - 166*t2 + 
                  6*Power(t1,2)*(307 + 58*t2) + t1*(-850 + 248*t2) + 
                  Power(t1,4)*(584 + 298*t2) - 
                  Power(t1,3)*(1670 + 653*t2)) + 
               s1*(-270 + 31*Power(t1,6) + 260*t2 + 156*Power(t2,2) + 
                  12*Power(t1,5)*(-19 + 4*t2) + 
                  t1*(590 + 466*t2 - 360*Power(t2,2)) + 
                  6*Power(t1,3)*(281 + 524*t2 + 69*Power(t2,2)) - 
                  Power(t1,2)*(1271 + 2622*t2 + 69*Power(t2,2)) - 
                  2*Power(t1,4)*(269 + 648*t2 + 108*Power(t2,2)))) + 
            s*(-1 + t1)*(-55 - 14*Power(t1,6) - 
               Power(s1,3)*(62 + 7*t1 + 30*Power(t1,2) - 
                  54*Power(t1,3) + 10*Power(t1,4)) + 30*t2 + 
               32*Power(t2,2) + 91*Power(t2,3) + 
               3*Power(t1,5)*(3 + 8*t2) + 
               2*Power(t1,4)*(58 + 111*t2 + 20*Power(t2,2)) + 
               t1*(106 + 118*t2 + 317*Power(t2,2) - 8*Power(t2,3)) + 
               Power(t1,3)*(-207 - 338*t2 + 166*Power(t2,2) + 
                  24*Power(t2,3)) - 
               Power(t1,2)*(-45 + 56*t2 + 555*Power(t2,2) + 
                  52*Power(t2,3)) + 
               Power(s1,2)*(127 - 5*Power(t1,5) + 
                  Power(t1,2)*(90 - 320*t2) + 219*t2 + 
                  12*Power(t1,4)*(5 + 3*t2) + 
                  Power(t1,3)*(-129 + 58*t2) + t1*(-143 + 172*t2)) + 
               s1*(-155 + 6*Power(t1,6) + 4*Power(t1,5)*(-6 + t2) - 
                  60*t2 - 273*Power(t2,2) + 
                  Power(t1,3)*(295 + 14*t2 - 129*Power(t2,2)) + 
                  t1*(185 - 420*t2 - 88*Power(t2,2)) - 
                  2*Power(t1,4)*(93 + 74*t2 + 10*Power(t2,2)) + 
                  Power(t1,2)*(-121 + 610*t2 + 345*Power(t2,2)))) + 
            Power(s,5)*(-41 + 9*Power(t1,4) + 
               Power(s1,3)*(-30 + 61*t1) + Power(t1,3)*(33 - 525*t2) + 
               248*t2 + 370*Power(t2,2) + 345*Power(t2,3) + 
               5*Power(t1,2)*(-16 + 293*t2 + 104*Power(t2,2)) - 
               t1*(-88 + 1198*t2 + 1050*Power(t2,2) + 
                  455*Power(t2,3)) + 
               Power(s1,2)*(62 + 144*Power(t1,2) + 207*t2 - 
                  5*t1*(57 + 67*t2)) + 
               s1*(-224 + 317*Power(t1,3) - 224*t2 - 475*Power(t2,2) - 
                  4*Power(t1,2)*(253 + 90*t2) + 
                  t1*(928 + 818*t2 + 675*Power(t2,2)))) + 
            Power(s,3)*(32 + 11*Power(t1,6) + 
               Power(s1,3)*(152 - 566*t1 + 657*Power(t1,2) - 
                  296*Power(t1,3)) - 510*t2 - 290*Power(t2,2) - 
               46*Power(t2,3) - Power(t1,5)*(279 + 47*t2) + 
               Power(t1,4)*(708 + 639*t2 + 202*Power(t2,2)) + 
               Power(t1,3)*(-92 + 329*t2 + 192*Power(t2,2) - 
                  40*Power(t2,3)) + 
               Power(t1,2)*(-847 - 2159*t2 - 1136*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(467 + 1748*t2 + 1083*Power(t2,2) + 100*Power(t2,3)) + 
               Power(s1,2)*(-587 + 67*Power(t1,4) - 284*t2 - 
                  2*Power(t1,2)*(1205 + 729*t2) + 
                  Power(t1,3)*(972 + 734*t2) + t1*(2009 + 1140*t2)) + 
               s1*(357 - 518*Power(t1,4) + 49*Power(t1,5) + 900*t2 + 
                  234*Power(t2,2) - 
                  2*t1*(642 + 1769*t2 + 437*Power(t2,2)) - 
                  Power(t1,3)*(419 + 2102*t2 + 486*Power(t2,2)) + 
                  Power(t1,2)*(1815 + 4638*t2 + 1021*Power(t2,2))))) - 
         Power(s2,2)*Power(-1 + s + t1,2)*
          (-(Power(-1 + t1 - s1*t1 + t2,2)*
               (t1*(19 - 9*t2) + 4*(-1 + t2) + 
                 Power(t1,2)*(-15 + 3*s1 + 2*t2))) + 
            Power(s,8)*(8*t1 + 19*Power(t1,3) + 2*Power(t2,3) + 
               Power(t1,2)*(-34 - 12*s1 + 13*t2)) + 
            Power(s,6)*(2 + 14*Power(t1,5) + 
               Power(t1,4)*(-19*s1 + 2*(-85 + t2)) + 6*t2 - 
               36*Power(t2,2) + 38*Power(t2,3) + 
               Power(t1,3)*(615 + 12*Power(s1,2) - 73*t2 - 
                  2*s1*(-85 + 8*t2)) + 
               t1*(294 + 20*t2 + 2*Power(t2,2) - 25*Power(t2,3) + 
                  s1*(-86 + 34*t2 - 4*Power(t2,2))) + 
               Power(t1,2)*(-837 + 2*t2 - 11*Power(t2,2) + 
                  2*Power(t2,3) - 2*Power(s1,2)*(60 + t2) + 
                  2*s1*(-31 + 56*t2 + 3*Power(t2,2)))) + 
            Power(s,3)*(2*(-3 + s1)*Power(t1,6) + 
               2*Power(t1,5)*(45 + 7*Power(s1,2) + 4*s1*(-10 + t2)) + 
               2*(-9 - 6*t2 - 15*Power(t2,2) + 19*Power(t2,3)) + 
               Power(t1,4)*(-230 + 11*Power(s1,3) + 
                  Power(s1,2)*(10 - 4*t2) - 4*t2 - 8*s1*(-47 + 9*t2)) + 
               t1*(-995 - 482*t2 + 250*Power(t2,2) - 20*Power(t2,3) + 
                  s1*(408 + 162*t2 - 130*Power(t2,2))) - 
               Power(t1,3)*(148 + 36*Power(s1,3) + 505*t2 + 
                  32*Power(t2,2) + Power(s1,2)*(317 + 42*t2) - 
                  s1*(191 + 488*t2 + 8*Power(t2,2))) + 
               Power(t1,2)*(1062 + 1191*t2 + 40*Power(t2,2) + 
                  4*Power(s1,2)*(128 + 33*t2) + 
                  2*s1*(-534 - 508*t2 + 21*Power(t2,2)))) - 
            s*(12 + (2 + 6*s1)*Power(t1,6) - 42*t2 + 54*Power(t2,2) - 
               22*Power(t2,3) + 
               Power(t1,5)*(60 + 6*Power(s1,2) + 8*t2 - 
                  2*s1*(19 + 4*t2)) + 
               Power(t1,4)*(114 - 13*Power(s1,3) + 
                  Power(s1,2)*(62 - 4*t2) - 4*t2 + s1*(-107 + 20*t2)) + 
               Power(t1,2)*(218 + Power(s1,2)*(20 - 34*t2) - 376*t2 + 
                  124*Power(t2,2) - 8*Power(t2,3) + 
                  s1*(-203 + 334*t2 - 81*Power(t2,2))) + 
               Power(t1,3)*(-399 + 10*Power(s1,3) + 152*t2 - 
                  8*Power(t2,2) + 3*Power(s1,2)*(-37 + 18*t2) + 
                  4*s1*(82 - 53*t2 + 3*Power(t2,2))) + 
               t1*(-7 + 262*t2 - 193*Power(t2,2) + 40*Power(t2,3) + 
                  2*s1*(7 - 44*t2 + 23*Power(t2,2)))) - 
            Power(s,5)*(10 + (40 + 6*s1)*Power(t1,5) + 30*t2 - 
               78*Power(t2,2) + 46*Power(t2,3) + 
               Power(t1,4)*(-341 + 4*Power(s1,2) + 40*t2 + 
                  s1*(-155 + 4*t2)) + 
               Power(t1,3)*(1143 + 114*Power(s1,2) + 2*Power(s1,3) + 
                  10*t2 + 8*Power(t2,2) - 
                  2*s1*(-169 + 66*t2 + 2*Power(t2,2))) + 
               t1*(724 + 120*t2 - 5*Power(t2,2) - 56*Power(t2,3) + 
                  2*s1*(-159 + 26*t2 + 9*Power(t2,2))) + 
               Power(t1,2)*(-1535 - 472*t2 - 68*Power(t2,2) + 
                  8*Power(t2,3) - 2*Power(s1,2)*(220 + 13*t2) + 
                  s1*(464 + 430*t2 + 27*Power(t2,2)))) + 
            Power(s,4)*(2*Power(t1,6) + 
               Power(t1,5)*(6 + 48*s1 - 4*Power(s1,2) - 12*t2) + 
               2*(9 + 24*t2 - 30*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,4)*(-216 - 2*Power(s1,3) + 50*t2 + 
                  Power(s1,2)*(9 + 2*t2) + s1*(-382 + 36*t2)) + 
               Power(t1,3)*(983 + 16*Power(s1,3) + 355*t2 + 
                  28*Power(t2,2) + Power(s1,2)*(336 + 9*t2) - 
                  2*s1*(-49 + 181*t2 + 6*Power(t2,2))) + 
               t1*(1128 + 313*t2 - 85*Power(t2,2) - 45*Power(t2,3) + 
                  4*s1*(-132 - 7*t2 + 20*Power(t2,2))) + 
               Power(t1,2)*(-1718 - 1093*t2 - 125*Power(t2,2) + 
                  10*Power(t2,3) - 8*Power(s1,2)*(89 + 11*t2) + 
                  s1*(1260 + 862*t2 + 27*Power(t2,2)))) + 
            Power(s,2)*(16 + (6 + 4*s1)*Power(t1,6) - 
               2*Power(t1,5)*
                (39 + 2*Power(s1,2) + 8*s1*(-2 + t2) - 10*t2) - 42*t2 + 
               84*Power(t2,2) - 46*Power(t2,3) + 
               Power(t1,4)*(273 + 26*Power(s1,2) - 19*Power(s1,3) - 
                  10*t2 + 4*s1*(-39 + 14*t2)) + 
               Power(t1,3)*(-494 + 32*Power(s1,3) + 327*t2 + 
                  8*Power(t2,2) + Power(s1,2)*(-3 + 72*t2) + 
                  2*s1*(79 - 198*t2 + 4*Power(t2,2))) + 
               t1*(381 + 468*t2 - 320*Power(t2,2) + 61*Power(t2,3) + 
                  2*s1*(-57 - 91*t2 + 54*Power(t2,2))) - 
               Power(t1,2)*(104 + 806*t2 - 115*Power(t2,2) + 
                  10*Power(t2,3) + 2*Power(s1,2)*(58 + 49*t2) + 
                  s1*(-119 - 748*t2 + 108*Power(t2,2)))) + 
            Power(s,7)*(33*Power(t1,4) + 
               Power(t1,2)*(257 + 12*Power(s1,2) + s1*(73 - 12*t2) - 
                  63*t2) + 2*(3 - 7*t2)*Power(t2,2) + 
               Power(t1,3)*(-166 - 25*s1 + 15*t2) + 
               2*t1*(s1*(4 - 3*t2 + Power(t2,2)) + 2*(-18 + Power(t2,3))))\
))*R1q(s2))/(s*(-1 + s1)*(-1 + s2)*s2*
       Power(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2),2)*
       Power(-1 + s + t1,3)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*
       (-1 + t2)) + (8*(-2*Power(s,11)*
          (2*Power(t1,3) - Power(t1,2)*(5 + s1 - 5*t2) + Power(t2,3)) - 
         Power(-1 + t1,2)*Power(s2 + Power(s2,2) - 2*s2*t1 + 
            (-1 + t1)*t1,2)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          (6 + Power(t1,2)*(17 - 5*s1 - 2*t2) - 6*t2 + 
            Power(s2,3)*(2 - 3*s1 - 2*t1 + 3*t2) + 
            t1*(-23 + 2*s1 + 11*t2) + 
            s2*(11 + s1*(-2 + 4*t1 + Power(t1,2)) + t2 - 
               6*t1*(2 + t2) + Power(t1,2)*(1 + 2*t2)) + 
            Power(s2,2)*(1 + s1 + 2*s1*t1 + 2*Power(t1,2) + 2*t2 - 
               t1*(3 + 5*t2))) + 
         Power(s,10)*(-31*Power(t1,4) + 
            Power(t1,3)*(96 + 20*s1 - 81*t2) + 
            2*Power(t2,2)*(-3 + 8*t2) - 
            t1*t2*(6 + 2*s1*(-3 + t2) + 17*Power(t2,2)) + 
            Power(t1,2)*(-76 - 2*Power(s1,2) + 62*t2 + Power(t2,2) + 
               2*s1*(-11 + 5*t2)) + 
            2*s2*(12*Power(t1,3) + 3*Power(t2,2)*(1 - s1 + 3*t2) + 
               Power(t1,2)*(-35 - 12*s1 + 37*t2) - 
               t1*(-10 - 2*s1 + 7*t2 + 3*s1*t2 + Power(t2,2)))) + 
         Power(s,9)*(-104*Power(t1,5) + 
            Power(t1,4)*(374 + 87*s1 - 288*t2) - 
            2*t2*(3 - 21*t2 + 26*Power(t2,2)) + 
            Power(t1,3)*(-567 - 16*Power(s1,2) + 442*t2 + 
               8*Power(t2,2) + 2*s1*(-96 + 41*t2)) + 
            t1*(-6 + 42*t2 - 41*Power(t2,2) + 126*Power(t2,3) + 
               s1*(6 - 40*t2 + 6*Power(t2,2))) + 
            Power(t1,2)*(30*Power(s1,2) + 
               s1*(60 - 30*t2 - 17*Power(t2,2)) - 
               2*(-130 + 84*t2 + 3*Power(t2,2) + 32*Power(t2,3))) - 
            2*Power(s2,2)*(-5 + 30*Power(t1,3) + 5*t2 + 
               22*Power(t2,2) + 36*Power(t2,3) + 
               3*Power(s1,2)*(t1 + t2) + Power(t1,2)*(-104 + 119*t2) + 
               t1*(57 - 48*t2 - 8*Power(t2,2)) - 
               s1*(1 + 51*Power(t1,2) + 3*t2 + 24*Power(t2,2) + 
                  t1*(-16 + 19*t2))) + 
            s2*(166*Power(t1,4) + Power(t1,3)*(-581 - 189*s1 + 535*t2) + 
               Power(t1,2)*(576 + 18*Power(s1,2) - 515*t2 - 
                  26*Power(t2,2) - 4*s1*(-57 + 26*t2)) + 
               t2*(6 + 6*t2 - 123*Power(t2,2) + s1*(-6 + 34*t2)) + 
               t1*(-120 + 100*t2 + 75*Power(t2,2) + 141*Power(t2,3) + 
                  2*Power(s1,2)*(1 + t2) + 
                  s1*(-58 + 6*t2 - 37*Power(t2,2))))) + 
         Power(s,8)*(-2 - 196*Power(t1,6) + 
            Power(t1,5)*(763 + 216*s1 - 588*t2) + 36*t2 - 
            114*Power(t2,2) + 84*Power(t2,3) + 
            Power(t1,4)*(-1745 - 56*Power(s1,2) + 1367*t2 + 
               28*Power(t2,2) + s1*(-729 + 296*t2)) + 
            t1*(36 - 139*t2 + 249*Power(t2,2) - 368*Power(t2,3) + 
               2*s1*(-16 + 43*t2 + 7*Power(t2,2))) + 
            Power(t1,3)*(1722 + 229*Power(s1,2) - 847*t2 - 
               43*Power(t2,2) - 140*Power(t2,3) - 
               8*s1*(-53 + 49*t2 + 8*Power(t2,2))) + 
            Power(t1,2)*(-599 + 6*Power(s1,3) + 313*t2 - 
               100*Power(t2,2) + 433*Power(t2,3) + 
               Power(s1,2)*(-178 + 3*t2) + 
               s1*(118 - 56*t2 + 45*Power(t2,2))) - 
            2*Power(s2,3)*(23 + Power(s1,3) - 40*Power(t1,3) + 
               Power(t1,2)*(170 - 217*t2) - 29*t2 - 70*Power(t2,2) - 
               84*Power(t2,3) - Power(s1,2)*(17*t1 + 21*t2) + 
               t1*(-135 + 141*t2 + 28*Power(t2,2)) + 
               s1*(7 + 110*Power(t1,2) + 20*t2 + 84*Power(t2,2) + 
                  t1*(-52 + 49*t2))) + 
            s2*(490*Power(t1,5) + 
               Power(t1,4)*(-1933 - 644*s1 + 1674*t2) + 
               Power(t1,3)*(3200 + 127*Power(s1,2) + 
                  s1*(1524 - 572*t2) - 2938*t2 - 128*Power(t2,2)) + 
               t2*(6 + s1*(26 - 74*t2) - 167*t2 + 343*Power(t2,2)) + 
               Power(t1,2)*(-2031 + 4*Power(s1,3) + 1354*t2 + 
                  399*Power(t2,2) + 484*Power(t2,3) + 
                  Power(s1,2)*(-206 + 3*t2) + 
                  s1*(-842 + 480*t2 - 78*Power(t2,2))) + 
               t1*(350 + Power(s1,2)*(34 - 26*t2) - 224*t2 - 
                  125*Power(t2,2) - 876*Power(t2,3) + 
                  2*s1*(63 + 31*t2 + 111*Power(t2,2)))) + 
            Power(s2,2)*(4*Power(s1,3)*t1 - 365*Power(t1,4) + 
               Power(t1,3)*(1477 - 1509*t2) + 
               Power(s1,2)*(-2 - 93*Power(t1,2) + t1*(6 - 63*t2) + 
                  26*t2) + 2*Power(t1,2)*
                (-873 + 913*t2 + 84*Power(t2,2)) + 
               2*(-25 + t2 + 57*Power(t2,2) + 209*Power(t2,3)) - 
               t1*(-636 + 572*t2 + 501*Power(t2,2) + 516*Power(t2,3)) + 
               s1*(-24 + 658*Power(t1,3) + 20*t2 - 237*Power(t2,2) + 
                  Power(t1,2)*(-905 + 372*t2) + 
                  t1*(364 - 58*t2 + 330*Power(t2,2))))) + 
         Power(s,7)*(10 - 224*Power(t1,7) + 
            7*Power(t1,6)*(119 + 48*s1 - 108*t2) - 78*t2 + 
            138*Power(t2,2) - 56*Power(t2,3) + 
            Power(t1,5)*(-2781 - 112*Power(s1,2) + 2401*t2 + 
               56*Power(t2,2) + 7*s1*(-225 + 88*t2)) + 
            t1*(-79 + 248*t2 - 519*Power(t2,2) + 504*Power(t2,3) + 
               s1*(50 - 24*t2 - 98*Power(t2,2))) + 
            Power(t1,4)*(4794 + 763*Power(s1,2) - 1919*t2 - 
               133*Power(t2,2) - 196*Power(t2,3) - 
               140*s1*(-9 + 10*t2 + Power(t2,2))) + 
            Power(t1,2)*(1189 - 24*Power(s1,3) + 
               Power(s1,2)*(509 - 47*t2) - 617*t2 + 557*Power(t2,2) - 
               1097*Power(t2,3) + s1*(-945 + 356*t2 + 91*Power(t2,2))) + 
            Power(t1,3)*(-3595 + 47*Power(s1,3) + 701*t2 - 
               79*Power(t2,2) + 847*Power(t2,3) + 
               Power(s1,2)*(-1245 + 23*t2) + 
               s1*(901 + 506*t2 + 147*Power(t2,2))) + 
            2*Power(s2,4)*(6*Power(s1,3) - 30*Power(t1,3) - 
               5*Power(t1,2)*(-33 + 49*t2) - 
               Power(s1,2)*(1 + 39*t1 + 63*t2) + 
               2*t1*(-85 + 115*t2 + 28*Power(t2,2)) - 
               14*(-3 + 5*t2 + 9*Power(t2,2) + 9*Power(t2,3)) + 
               s1*(20 + 135*Power(t1,2) + 57*t2 + 168*Power(t2,2) + 
                  9*t1*(-10 + 7*t2))) + 
            s2*(12 + 798*Power(t1,6) + 
               3*Power(s1,3)*t1*(4 - 21*t1 + 9*Power(t1,2)) - 121*t2 + 
               490*Power(t2,2) - 485*Power(t2,3) + 
               7*Power(t1,5)*(-467 + 422*t2) + 
               Power(t1,4)*(7719 - 7662*t2 - 336*Power(t2,2)) + 
               Power(t1,3)*(-9089 + 5305*t2 + 1161*Power(t2,2) + 
                  952*Power(t2,3)) + 
               t1*(-779 + 327*t2 - 530*Power(t2,2) + 
                  2145*Power(t2,3)) - 
               Power(t1,2)*(-4491 + 1289*t2 + 936*Power(t2,2) + 
                  2682*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-300 + 385*Power(t1,3) + t1*(1351 - 101*t2) + 94*t2 - 
                  Power(t1,2)*(1420 + 37*t2)) - 
               s1*(2 + 1232*Power(t1,5) + 44*t2 - 78*Power(t2,2) + 
                  4*Power(t1,4)*(-1151 + 399*t2) + 
                  Power(t1,2)*(-285 + 818*t2 - 570*Power(t2,2)) + 
                  Power(t1,3)*(3797 - 2902*t2 + 14*Power(t2,2)) + 
                  t1*(-333 + 182*t2 + 527*Power(t2,2)))) + 
            Power(s2,3)*(Power(s1,3)*(8 - 37*t1) + 420*Power(t1,4) + 
               Power(t1,3)*(-2038 + 2355*t2) - 
               5*Power(t1,2)*(-558 + 721*t2 + 106*Power(t2,2)) - 
               2*(-98 + 64*t2 + 241*Power(t2,2) + 413*Power(t2,3)) + 
               t1*(-1360 + 1628*t2 + 1459*Power(t2,2) + 
                  1092*Power(t2,3)) + 
               Power(s1,2)*(6 + 244*Power(t1,2) - 163*t2 + 
                  t1*(-33 + 351*t2)) + 
               s1*(118 - 1147*Power(t1,3) + 
                  Power(t1,2)*(1867 - 584*t2) + 46*t2 + 
                  723*Power(t2,2) - 2*t1*(490 + 60*t2 + 553*Power(t2,2))\
)) + Power(s2,2)*(106 - 930*Power(t1,5) - 4*Power(s1,3)*t1*(3 + t1) + 
               56*t2 + 97*Power(t2,2) - 990*Power(t2,3) - 
               5*Power(t1,4)*(-825 + 812*t2) + 
               Power(t1,3)*(-7360 + 8376*t2 + 694*Power(t2,2)) + 
               2*t1*(-828 + 405*t2 + 764*Power(t2,2) + 
                  1331*Power(t2,3)) - 
               Power(t1,2)*(-5749 + 4872*t2 + 2283*Power(t2,2) + 
                  1594*Power(t2,3)) + 
               Power(s1,2)*(28 - 435*Power(t1,3) + 
                  Power(t1,2)*(674 - 174*t2) - 48*t2 + 
                  t1*(-193 + 304*t2)) + 
               s1*(44 + 1781*Power(t1,4) - 130*t2 + 454*Power(t2,2) + 
                  2*Power(t1,3)*(-2361 + 727*t2) - 
                  2*t1*(447 - 38*t2 + 755*Power(t2,2)) + 
                  Power(t1,2)*(3455 - 1474*t2 + 912*Power(t2,2))))) - 
         s*(s2 - t1)*(-1 + t1)*
          (Power(s2,8)*(-12 + 5*s1 + 12*t1 - 5*t2)*Power(s1 - t2,2) - 
            Power(-1 + t1,2)*
             ((7 + s1)*Power(t1,8) - 10*Power(-1 + t2,3) - 
               Power(t1,7)*(17 + 2*s1*(-2 + t2) + 9*t2) + 
               6*t1*Power(-1 + t2,2)*(-3*s1 + 2*(5 + t2)) + 
               Power(t1,5)*(301 - 41*Power(s1,3) + 
                  Power(s1,2)*(282 - 17*t2) - 2*t2 + 7*Power(t2,2) + 
                  Power(t2,3) + s1*(-487 + 52*t2)) + 
               Power(t1,2)*(-1 + t2)*
                (421 + 24*Power(s1,2) - 305*t2 - 22*Power(t2,2) + 
                  2*s1*(-129 + 65*t2)) + 
               Power(t1,3)*(792 - 4*Power(s1,3) - 1037*t2 + 
                  250*Power(t2,2) + 47*Power(t2,3) - 
                  3*Power(s1,2)*(-65 + 47*t2) + 
                  s1*(-863 + 924*t2 - 163*Power(t2,2))) + 
               Power(t1,6)*(-56 - 19*Power(s1,2) + 2*t2 + 
                  Power(t2,2) + s1*(33 + 8*t2 + Power(t2,2))) + 
               Power(t1,4)*(-676 + 33*Power(s1,3) + 458*t2 - 
                  41*Power(t2,2) - 16*Power(t2,3) + 
                  2*Power(s1,2)*(-217 + 85*t2) + 
                  2*s1*(536 - 315*t2 + 7*Power(t2,2)))) + 
            Power(s2,7)*(Power(s1,3)*(-10 + t1) + 
               Power(s1,2)*(5 - 43*Power(t1,2) + 18*t2 + 
                  t1*(38 + 9*t2)) + 
               s1*(2 + 8*Power(t1,3) + 8*t2 - 6*Power(t2,2) + 
                  2*Power(t1,2)*(-7 + 52*t2) + 
                  t1*(4 - 112*t2 - 21*Power(t2,2))) + 
               t2*(-6 - 4*Power(t1,3) + Power(t1,2)*(2 - 61*t2) - 
                  13*t2 - 2*Power(t2,2) + 
                  t1*(8 + 74*t2 + 11*Power(t2,2)))) + 
            Power(s2,6)*(8 + 
               Power(s1,3)*(-32 + 97*t1 - 73*Power(t1,2)) + 30*t2 + 
               26*Power(t2,2) - 12*Power(t2,3) + 
               Power(t1,4)*(2 + 17*t2) + 
               2*Power(t1,3)*(-7 - 2*t2 + 73*Power(t2,2)) - 
               Power(t1,2)*(-30 + 13*t2 + 197*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-26 - 30*t2 + 25*Power(t2,2) + 24*Power(t2,3)) + 
               Power(s1,2)*(106 + 42*Power(t1,3) + 67*t2 + 
                  Power(t1,2)*(91 + 157*t2) - t1*(239 + 200*t2)) - 
               s1*(58 + 37*Power(t1,4) + 134*t2 + 23*Power(t2,2) + 
                  2*Power(t1,3)*(-46 + 93*t2) - 
                  t1*(134 + 220*t2 + 79*Power(t2,2)) + 
                  Power(t1,2)*(131 - 100*t2 + 80*Power(t2,2)))) + 
            Power(s2,5)*(36 + 
               Power(s1,3)*(-35 + 203*t1 - 325*Power(t1,2) + 
                  179*Power(t1,3)) + Power(t1,5)*(5 - 27*t2) + 126*t2 + 
               63*Power(t2,2) - 44*Power(t2,3) - 
               2*Power(t1,4)*(-5 + 21*t2 + 103*Power(t2,2)) + 
               Power(t1,2)*(178 + 370*t2 + 115*Power(t2,2) - 
                  110*Power(t2,3)) + 
               Power(t1,3)*(-96 - 5*t2 + 292*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-133 - 422*t2 - 264*Power(t2,2) + 130*Power(t2,3)) + 
               Power(s1,2)*(214 + 23*Power(t1,4) + 62*t2 - 
                  3*t1*(312 + 145*t2) + 5*Power(t1,2)*(247 + 150*t2) - 
                  Power(t1,3)*(536 + 443*t2)) + 
               s1*(-149 + 70*Power(t1,5) - 302*t2 + 21*Power(t2,2) + 
                  7*Power(t1,4)*(-29 + 26*t2) + 
                  t1*(607 + 1276*t2 + 90*Power(t2,2)) + 
                  Power(t1,3)*(505 + 272*t2 + 258*Power(t2,2)) - 
                  Power(t1,2)*(830 + 1428*t2 + 303*Power(t2,2)))) + 
            s2*(-1 + t1)*(Power(s1,3)*Power(t1,2)*
                (12 - 132*t1 + 313*Power(t1,2) - 208*Power(t1,3) - 
                  3*Power(t1,4)) - 4*Power(-1 + t2,2)*(17 + t2) + 
               Power(t1,8)*(41 + t2) - 
               2*Power(t1,7)*(71 + 27*t2 + Power(t2,2)) + 
               Power(t1,5)*(1135 + 573*t2 - 6*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,6)*(-79 - 32*t2 + Power(t2,2) + Power(t2,3)) - 
               Power(t1,4)*(2691 + 73*t2 - 400*Power(t2,2) + 
                  49*Power(t2,3)) + 
               t1*(687 - 1101*t2 + 329*Power(t2,2) + 85*Power(t2,3)) + 
               Power(t1,3)*(3308 - 2016*t2 - 415*Power(t2,2) + 
                  281*Power(t2,3)) - 
               Power(t1,2)*(2191 - 2570*t2 + 247*Power(t2,2) + 
                  288*Power(t2,3)) + 
               Power(s1,2)*t1*
                (Power(t1,6) + Power(t1,2)*(2046 - 884*t2) - 
                  48*(-1 + t2) - 5*Power(t1,5)*(23 + 5*t2) + 
                  Power(t1,4)*(1423 + 106*t2) + t1*(-573 + 411*t2) + 
                  Power(t1,3)*(-2830 + 494*t2)) + 
               s1*(6*Power(t1,8) + 18*Power(-1 + t2,2) - 
                  5*Power(t1,7)*(-7 + 2*t2) + 
                  Power(t1,3)*(-5423 + 3478*t2 - 83*Power(t2,2)) + 
                  2*Power(t1,6)*(55 + 69*t2 + 6*Power(t2,2)) - 
                  32*t1*(15 - 22*t2 + 7*Power(t2,2)) + 
                  Power(t1,5)*(-2026 - 548*t2 + 27*Power(t2,2)) - 
                  2*Power(t1,4)*(-2601 + 502*t2 + 137*Power(t2,2)) + 
                  Power(t1,2)*(2558 - 2722*t2 + 470*Power(t2,2)))) - 
            Power(s2,4)*(-59 + 
               Power(s1,3)*(21 - 217*t1 + 593*Power(t1,2) - 
                  575*Power(t1,3) + 181*Power(t1,4)) + 
               Power(t1,6)*(47 - 18*t2) - 258*t2 + 101*Power(t2,2) + 
               39*Power(t2,3) - 
               2*Power(t1,5)*(72 + 79*t2 + 87*Power(t2,2)) + 
               t1*(341 + 1286*t2 + 14*Power(t2,2) - 254*Power(t2,3)) + 
               Power(t1,3)*(495 + 1358*t2 + 430*Power(t2,2) - 
                  234*Power(t2,3)) + 
               Power(t1,4)*(-14 - 21*t2 + 238*Power(t2,2) + 
                  18*Power(t2,3)) + 
               Power(t1,2)*(-666 - 2189*t2 - 609*Power(t2,2) + 
                  428*Power(t2,3)) + 
               Power(s1,2)*(-241 + 72*Power(t1,5) + 33*t2 + 
                  3*t1*(529 + 66*t2) - Power(t1,4)*(921 + 547*t2) - 
                  Power(t1,2)*(3396 + 1105*t2) + 
                  Power(t1,3)*(2899 + 1412*t2)) + 
               s1*(318 + 71*Power(t1,6) + 136*t2 - 100*Power(t2,2) + 
                  2*Power(t1,5)*(-87 + 53*t2) + 
                  Power(t1,2)*(2743 + 4280*t2 - 27*Power(t2,2)) + 
                  3*t1*(-511 - 562*t2 + 108*Power(t2,2)) + 
                  Power(t1,4)*(766 + 768*t2 + 318*Power(t2,2)) - 
                  Power(t1,3)*(2191 + 3604*t2 + 506*Power(t2,2)))) + 
            Power(s2,3)*(166 + 
               Power(s1,3)*(-4 + 104*t1 - 552*Power(t1,2) + 
                  996*Power(t1,3) - 636*Power(t1,4) + 79*Power(t1,5)) - 
               2*Power(t1,7)*(-49 + t2) + 47*t2 - 204*Power(t2,2) + 
               43*Power(t2,3) - 
               Power(t1,6)*(383 + 221*t2 + 83*Power(t2,2)) + 
               Power(t1,2)*(1933 + 3525*t2 - 1132*Power(t2,2) - 
                  342*Power(t2,3)) + 
               Power(t1,4)*(988 + 2379*t2 + 469*Power(t2,2) - 
                  235*Power(t2,3)) - 
               t1*(867 + 1012*t2 - 986*Power(t2,2) + 17*Power(t2,3)) + 
               Power(t1,5)*(233 - 33*t2 + 91*Power(t2,2) + 
                  22*Power(t2,3)) + 
               Power(t1,3)*(-2168 - 4683*t2 - 127*Power(t2,2) + 
                  542*Power(t2,3)) + 
               Power(s1,2)*(135 + 51*Power(t1,6) + 
                  Power(t1,2)*(4718 - 172*t2) - 81*t2 - 
                  Power(t1,5)*(794 + 373*t2) + t1*(-1405 + 409*t2) - 
                  Power(t1,3)*(6627 + 1193*t2) + 
                  Power(t1,4)*(3922 + 1449*t2)) + 
               s1*(-418 + 44*Power(t1,7) + 302*t2 + 14*Power(t2,2) + 
                  4*Power(t1,6)*(-3 + 7*t2) + 
                  Power(t1,3)*(6979 + 6240*t2 - 551*Power(t2,2)) + 
                  t1*(2667 - 520*t2 - 425*Power(t2,2)) - 
                  2*Power(t1,4)*(1738 + 2299*t2 + 185*Power(t2,2)) + 
                  Power(t1,5)*(544 + 860*t2 + 207*Power(t2,2)) + 
                  2*Power(t1,2)*(-3164 - 1156*t2 + 543*Power(t2,2)))) - 
            Power(s2,2)*(-212 + 
               Power(s1,3)*t1*
                (-12 + 186*t1 - 704*Power(t1,2) + 980*Power(t1,3) - 
                  463*Power(t1,4) + 7*Power(t1,5)) + 285*t2 - 
               28*Power(t2,2) - 45*Power(t2,3) + 
               Power(t1,8)*(92 + 3*t2) - 
               Power(t1,7)*(395 + 153*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(-3664 + 688*t2 + 1763*Power(t2,2) - 
                  455*Power(t2,3)) + 
               Power(t1,5)*(1463 + 1975*t2 + 154*Power(t2,2) - 
                  99*Power(t2,3)) + 
               Power(t1,6)*(259 - 20*t2 + 12*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(t1,3)*(5194 + 2341*t2 - 2201*Power(t2,2) + 
                  106*Power(t2,3)) + 
               Power(t1,4)*(-4119 - 3996*t2 + 704*Power(t2,2) + 
                  209*Power(t2,3)) + 
               t1*(1382 - 1123*t2 - 384*Power(t2,2) + 281*Power(t2,3)) + 
               Power(s1,2)*(14*Power(t1,7) + t1*(513 - 351*t2) + 
                  24*(-1 + t2) - 36*Power(t1,3)*(-191 + 33*t2) - 
                  Power(t1,6)*(389 + 143*t2) + 
                  5*Power(t1,2)*(-587 + 239*t2) - 
                  Power(t1,4)*(7297 + 272*t2) + 
                  Power(t1,5)*(3242 + 753*t2)) + 
               s1*(19*Power(t1,8) + Power(t1,7)*(64 - 10*t2) + 
                  Power(t1,4)*(9934 + 3372*t2 - 773*Power(t2,2)) + 
                  Power(t1,2)*(7274 - 3990*t2 - 242*Power(t2,2)) + 
                  t1*(-2059 + 1992*t2 - 239*Power(t2,2)) + 
                  4*(51 - 70*t2 + 19*Power(t2,2)) + 
                  Power(t1,6)*(205 + 512*t2 + 72*Power(t2,2)) - 
                  Power(t1,5)*(3566 + 2914*t2 + 77*Power(t2,2)) + 
                  Power(t1,3)*(-12075 + 1318*t2 + 1165*Power(t2,2))))) + 
         Power(s,6)*(-154*Power(t1,8) + 
            7*Power(t1,7)*(49 + 48*s1 - 90*t2) - 
            2*(8 - 30*t2 + 15*Power(t2,2) + 14*Power(t2,3)) + 
            Power(t1,6)*(-2217 - 140*Power(s1,2) + 2639*t2 + 
               70*Power(t2,2) + 7*s1*(-303 + 116*t2)) + 
            Power(t1,2)*(-1965 + 36*Power(s1,3) + 1162*t2 - 
               776*Power(t2,2) + 1152*Power(t2,3) + 
               4*Power(s1,2)*(-200 + 43*t2) + 
               s1*(2077 - 656*t2 - 499*Power(t2,2))) + 
            Power(t1,5)*(7224 + 1449*Power(s1,2) - 2379*t2 - 
               231*Power(t2,2) - 182*Power(t2,3) - 
               28*s1*(-72 + 93*t2 + 7*Power(t2,2))) + 
            t1*(53 - 103*t2 + 265*Power(t2,2) - 210*Power(t2,3) + 
               2*s1*(4 - 95*t2 + 105*Power(t2,2))) + 
            Power(t1,3)*(6311 - 166*Power(s1,3) + 
               Power(s1,2)*(3157 - 311*t2) - 1087*t2 + 
               559*Power(t2,2) - 1766*Power(t2,3) + 
               s1*(-5630 + 820*t2 + 248*Power(t2,2))) + 
            Power(t1,4)*(-9493 + 161*Power(s1,3) + 305*t2 + 
               86*Power(t2,2) + 1029*Power(t2,3) + 
               7*Power(s1,2)*(-537 + 11*t2) + 
               s1*(3325 + 1968*t2 + 273*Power(t2,2))) - 
            2*Power(s2,5)*(15*Power(s1,3) - 12*Power(t1,3) + 
               Power(t1,2)*(95 - 175*t2) - 
               5*Power(s1,2)*(1 + 9*t1 + 21*t2) + 
               5*t1*(-24 + 45*t2 + 14*Power(t2,2)) - 
               2*(-19 + 45*t2 + 70*Power(t2,2) + 63*Power(t2,3)) + 
               s1*(96*Power(t1,2) + 5*t1*(-18 + 7*t2) + 
                  30*(1 + 3*t2 + 7*Power(t2,2)))) + 
            s2*(-41 + 770*Power(t1,7) - 
               7*Power(t1,6)*(391 + 204*s1 - 458*t2) + 
               (253 + 52*s1)*t2 - (557 + 50*s1)*Power(t2,2) + 
               315*Power(t2,3) + 
               Power(t1,5)*(9093 + 651*Power(s1,2) + 
                  s1*(7884 - 2632*t2) - 11350*t2 - 532*Power(t2,2)) + 
               Power(t1,2)*(-8068 + 272*Power(s1,3) + 204*t2 + 
                  6*Power(t2,2) + 5559*Power(t2,3) + 
                  Power(s1,2)*(-4539 + 553*t2) + 
                  s1*(5836 - 6*t2 - 1539*Power(t2,2))) - 
               Power(t1,3)*(-17752 + 408*Power(s1,3) + 1414*t2 + 
                  2561*Power(t2,2) + 4584*Power(t2,3) + 
                  Power(s1,2)*(-7408 + 46*t2) + 
                  s1*(2120 + 5762*t2 - 670*Power(t2,2))) + 
               Power(t1,4)*(-18680 + 77*Power(s1,3) + 9831*t2 + 
                  2031*Power(t2,2) + 1176*Power(t2,3) - 
                  7*Power(s1,2)*(583 + 25*t2) + 
                  s1*(-8320 + 7672*t2 + 238*Power(t2,2))) + 
               t1*(1597 - 36*Power(s1,3) + Power(s1,2)*(794 - 220*t2) - 
                  884*t2 + 1602*Power(t2,2) - 2496*Power(t2,3) + 
                  2*s1*(-855 + 241*t2 + 346*Power(t2,2)))) + 
            Power(s2,4)*(-294 - 265*Power(t1,4) + 
               Power(s1,3)*(-43 + 130*t1) + 
               Power(t1,3)*(1642 - 2195*t2) + 400*t2 + 
               924*Power(t2,2) + 1050*Power(t2,3) + 
               Power(t1,2)*(-2560 + 4320*t2 + 982*Power(t2,2)) - 
               t1*(-1496 + 2642*t2 + 2429*Power(t2,2) + 
                  1470*Power(t2,3)) + 
               Power(s1,2)*(18 - 303*Power(t1,2) + 440*t2 - 
                  t1*(23 + 895*t2)) + 
               s1*(-254 + 1080*Power(t1,3) - 332*t2 - 
                  1265*Power(t2,2) + Power(t1,2)*(-2214 + 256*t2) + 
                  2*t1*(738 + 444*t2 + 1015*Power(t2,2)))) - 
            Power(s2,2)*(220 + 1275*Power(t1,6) + 
               2*Power(s1,3)*
                (-3 + 41*t1 - 96*Power(t1,2) + 52*Power(t1,3)) + t2 + 
               623*Power(t2,2) - 1193*Power(t2,3) + 
               Power(t1,5)*(-5617 + 5999*t2) + 
               Power(t1,4)*(13479 - 18062*t2 - 1532*Power(t2,2)) + 
               Power(t1,3)*(-18124 + 14879*t2 + 5497*Power(t2,2) + 
                  2760*Power(t2,3)) + 
               t1*(-2982 - 418*t2 + 1133*Power(t2,2) + 
                  5376*Power(t2,3)) - 
               Power(t1,2)*(-11478 + 3135*t2 + 6077*Power(t2,2) + 
                  7128*Power(t2,3)) + 
               Power(s1,2)*(146 + 995*Power(t1,4) + 
                  Power(t1,2)*(4077 - 718*t2) - 55*t2 + 
                  7*Power(t1,3)*(-535 + 6*t2) + 2*t1*(-783 + 266*t2)) + 
               s1*(-169 - 2580*Power(t1,5) + 
                  Power(t1,4)*(11243 - 3040*t2) - 172*t2 + 
                  405*Power(t2,2) + 
                  t1*(730 + 534*t2 - 2580*Power(t2,2)) - 
                  2*Power(t1,3)*(5842 - 3420*t2 + 613*Power(t2,2)) + 
                  Power(t1,2)*(3017 - 3612*t2 + 3774*Power(t2,2)))) + 
            Power(s2,3)*(-364 + 900*Power(t1,5) + 
               Power(s1,3)*(-10 + 94*t1 - 69*Power(t1,2)) - 77*t2 + 
               539*Power(t2,2) + 1641*Power(t2,3) + 
               Power(t1,4)*(-4676 + 5266*t2) + 
               Power(t1,3)*(8925 - 13220*t2 - 1913*Power(t2,2)) + 
               Power(t1,2)*(-8012 + 9865*t2 + 5843*Power(t2,2) + 
                  2981*Power(t2,3)) - 
               t1*(-3103 + 2230*t2 + 4478*Power(t2,2) + 
                  4628*Power(t2,3)) + 
               Power(s1,2)*(-97 + 705*Power(t1,3) + 
                  t1*(414 - 1198*t2) + 248*t2 + 
                  Power(t1,2)*(-1111 + 894*t2)) + 
               s1*(-196 - 2372*Power(t1,4) + 
                  Power(t1,3)*(7506 - 1412*t2) + 150*t2 - 
                  1203*Power(t2,2) - 
                  2*Power(t1,2)*(3399 - 524*t2 + 1438*Power(t2,2)) + 
                  t1*(2268 + 334*t2 + 4124*Power(t2,2))))) - 
         Power(s,2)*(2*Power(s2,9)*Power(s1 - t2,2)*
             (-1 + s1 + t1 - t2) + 
            Power(s2,8)*(Power(s1,3)*(31 - 42*t1) + 
               Power(s1,2)*(-70 + 151*t1 - 81*Power(t1,2) - 108*t2 + 
                  141*t1*t2) + 
               t2*(-15*Power(t1,2)*(2 + 7*t2) - 
                  2*(15 + 47*t2 + 23*Power(t2,2)) + 
                  t1*(60 + 199*t2 + 57*Power(t2,2))) + 
               s1*(22 + 8*Power(t1,3) + 164*t2 + 123*Power(t2,2) + 
                  6*Power(t1,2)*(1 + 31*t2) - 
                  2*t1*(18 + 175*t2 + 78*Power(t2,2)))) - 
            Power(-1 + t1,2)*
             (Power(t1,10) - 4*Power(-1 + t2,3) - 
               Power(t1,9)*(65 + 4*s1 + t2) + 
               t1*Power(-1 + t2,2)*(-71 + 8*s1 + 83*t2) + 
               Power(t1,8)*(158 - 2*Power(s1,2) + 69*t2 + 
                  Power(t2,2) + s1*(-53 + 26*t2)) + 
               Power(t1,2)*(-1 + t2)*
                (293 + 14*Power(s1,2) + 33*t2 - 244*Power(t2,2) + 
                  s1*(-155 + 63*t2)) + 
               Power(t1,6)*(-1845 + 147*Power(s1,3) - 158*t2 - 
                  27*Power(t2,2) + 5*Power(t2,3) + 
                  Power(s1,2)*(-1325 + 63*t2) + 
                  7*s1*(340 - 18*t2 + Power(t2,2))) + 
               Power(t1,7)*(385 + 159*Power(s1,2) - 11*Power(t2,2) - 
                  Power(t2,3) - 2*s1*(91 + 59*t2 + 5*Power(t2,2))) + 
               Power(t1,5)*(3503 - 180*Power(s1,3) + 
                  Power(s1,2)*(2210 - 591*t2) - 1551*t2 + 
                  134*Power(t2,2) + 69*Power(t2,3) + 
                  s1*(-5260 + 2302*t2 + 20*Power(t2,2))) + 
               Power(t1,3)*(2048 - 2*Power(s1,3) - 2727*t2 + 
                  378*Power(t2,2) + 377*Power(t2,3) - 
                  5*Power(s1,2)*(-55 + 43*t2) - 
                  2*s1*(814 - 924*t2 + 177*Power(t2,2))) + 
               Power(t1,4)*(-3825 + 53*Power(s1,3) + 3895*t2 - 
                  527*Power(t2,2) - 303*Power(t2,3) + 
                  Power(s1,2)*(-1303 + 675*t2) + 
                  s1*(4584 - 3698*t2 + 320*Power(t2,2)))) + 
            Power(s2,7)*(6 + 
               Power(s1,3)*(-1 - 74*t1 + 91*Power(t1,2)) - 83*t2 - 
               189*Power(t2,2) - 109*Power(t2,3) + 
               2*Power(t1,4)*(7 + 5*t2) + 
               Power(t1,3)*(-48 + 232*t2 + 569*Power(t2,2)) - 
               Power(t1,2)*(-60 + 577*t2 + 1277*Power(t2,2) + 
                  259*Power(t2,3)) + 
               t1*(-32 + 418*t2 + 897*Power(t2,2) + 352*Power(t2,3)) + 
               Power(s1,2)*(-1 + 325*Power(t1,3) - 80*t2 - 
                  Power(t1,2)*(601 + 414*t2) + t1*(277 + 446*t2)) + 
               s1*(19 - 84*Power(t1,4) + Power(t1,3)*(54 - 876*t2) + 
                  172*t2 + 190*Power(t2,2) - 
                  4*t1*(38 + 280*t2 + 181*Power(t2,2)) + 
                  Power(t1,2)*(163 + 1824*t2 + 582*Power(t2,2)))) + 
            Power(s2,5)*(159 - 14*Power(t1,7) + 
               Power(s1,3)*(-105 + 746*t1 - 1596*Power(t1,2) + 
                  1448*Power(t1,3) - 515*Power(t1,4)) + 
               Power(t1,6)*(17 - 36*t2) + 620*t2 - 57*Power(t2,2) - 
               196*Power(t2,3) + 
               3*Power(t1,5)*(5 + 748*t2 + 642*Power(t2,2)) + 
               Power(t1,2)*(2145 + 3370*t2 - 2104*Power(t2,2) - 
                  2844*Power(t2,3)) - 
               Power(t1,4)*(-543 + 4798*t2 + 5493*Power(t2,2) + 
                  816*Power(t2,3)) + 
               t1*(-1027 - 2824*t2 + 298*Power(t2,2) + 
                  1254*Power(t2,3)) + 
               2*Power(t1,3)*
                (-919 + 712*t2 + 2715*Power(t2,2) + 1312*Power(t2,3)) + 
               Power(s1,2)*(701 + 303*Power(t1,5) + 133*t2 - 
                  5*t1*(859 + 240*t2) + 3*Power(t1,4)*(593 + 325*t2) - 
                  2*Power(t1,3)*(3661 + 1259*t2) + 
                  Power(t1,2)*(8834 + 2676*t2)) - 
               2*s1*(302 + 242*Power(t1,6) + 388*t2 - 63*Power(t2,2) + 
                  3*Power(t1,5)*(-54 + 319*t2) + 
                  Power(t1,2)*(2567 + 4676*t2 - 780*Power(t2,2)) + 
                  Power(t1,4)*(27 - 1066*t2 - 165*Power(t2,2)) + 
                  t1*(-1522 - 2515*t2 + 324*Power(t2,2)) + 
                  Power(t1,3)*(-1454 - 2440*t2 + 717*Power(t2,2)))) + 
            Power(s2,6)*(81 + 3*Power(t1,6) + 
               Power(s1,3)*(-85 + 272*t1 - 276*Power(t1,2) + 
                  92*Power(t1,3)) + 120*t2 - 84*Power(t2,2) - 
               166*Power(t2,3) - Power(t1,5)*(65 + 21*t2) + 
               Power(t1,4)*(233 - 920*t2 - 1396*Power(t2,2)) + 
               Power(t1,3)*(-420 + 2264*t2 + 3581*Power(t2,2) + 
                  582*Power(t2,3)) + 
               t1*(-299 + 121*t2 + 1064*Power(t2,2) + 
                  835*Power(t2,3)) - 
               Power(t1,2)*(-467 + 1564*t2 + 3165*Power(t2,2) + 
                  1254*Power(t2,3)) + 
               Power(s1,2)*(369 - 505*Power(t1,4) + 
                  Power(t1,2)*(951 - 108*t2) + 138*t2 + 
                  7*Power(t1,3)*(59 + 24*t2) - t1*(1228 + 207*t2)) + 
               s1*(288*Power(t1,5) + Power(t1,4)*(-301 + 1784*t2) + 
                  t1*(554 + 626*t2 - 876*Power(t2,2)) + 
                  5*(-41 - 80*t2 + 21*Power(t2,2)) - 
                  2*Power(t1,3)*(59 + 1764*t2 + 417*Power(t2,2)) + 
                  2*Power(t1,2)*(-109 + 759*t2 + 807*Power(t2,2)))) - 
            s2*(-1 + t1)*(-2*Power(t1,10) + 
               Power(s1,3)*Power(t1,2)*
                (-6 + 204*t1 - 993*Power(t1,2) + 1591*Power(t1,3) - 
                  821*Power(t1,4) + 7*Power(t1,5)) + 
               3*Power(t1,9)*(123 + 8*t2) + 
               Power(-1 + t2,2)*(-53 + 65*t2) - 
               2*Power(t1,8)*(641 + 254*t2 + 13*Power(t2,2)) + 
               t1*(-565 + 571*t2 + 389*Power(t2,2) - 395*Power(t2,3)) + 
               Power(t1,6)*(8107 + 3241*t2 - 46*Power(t2,2) - 
                  170*Power(t2,3)) + 
               Power(t1,7)*(-458 + 87*t2 + 85*Power(t2,2) + 
                  22*Power(t2,3)) + 
               Power(t1,5)*(-17728 - 1526*t2 + 916*Power(t2,2) + 
                  95*Power(t2,3)) - 
               2*Power(t1,3)*
                (7293 - 6825*t2 + 121*Power(t2,2) + 878*Power(t2,3)) + 
               Power(t1,4)*(21043 - 9336*t2 - 1158*Power(t2,2) + 
                  975*Power(t2,3)) + 
               Power(t1,2)*(5155 - 6374*t2 + 265*Power(t2,2) + 
                  1182*Power(t2,3)) + 
               Power(s1,2)*t1*
                (25*Power(t1,7) + Power(t1,3)*(13516 - 4016*t2) + 
                  t1*(773 - 593*t2) + 28*(-1 + t2) - 
                  Power(t1,6)*(1030 + 117*t2) + 
                  Power(t1,5)*(7626 + 397*t2) + 
                  Power(t1,4)*(-15715 + 1696*t2) + 
                  Power(t1,2)*(-5167 + 2659*t2)) + 
               s1*(28*Power(t1,9) + Power(t1,8)*(412 - 146*t2) + 
                  8*Power(-1 + t2,2) + 
                  Power(t1,2)*(-4433 + 4958*t2 - 927*Power(t2,2)) + 
                  Power(t1,5)*(28807 - 3290*t2 - 851*Power(t2,2)) - 
                  2*Power(t1,6)*(5317 + 1596*t2 + 27*Power(t2,2)) + 
                  4*t1*(77 - 108*t2 + 31*Power(t2,2)) + 
                  2*Power(t1,7)*(145 + 623*t2 + 48*Power(t2,2)) + 
                  Power(t1,4)*(-32899 + 15444*t2 + 273*Power(t2,2)) + 
                  Power(t1,3)*(18113 - 14572*t2 + 1277*Power(t2,2)))) + 
            Power(s2,4)*(352 + 25*Power(t1,8) + 
               Power(s1,3)*(-39 + 648*t1 - 2632*Power(t1,2) + 
                  4156*Power(t1,3) - 2800*Power(t1,4) + 678*Power(t1,5)\
) + 493*t2 - 364*Power(t2,2) - 55*Power(t2,3) + 
               Power(t1,7)*(369 + 155*t2) - 
               Power(t1,6)*(1747 + 3401*t2 + 1582*Power(t2,2)) + 
               Power(t1,2)*(6591 + 14549*t2 - 1953*Power(t2,2) - 
                  3514*Power(t2,3)) + 
               Power(t1,4)*(4379 + 3999*t2 - 4862*Power(t2,2) - 
                  3373*Power(t2,3)) + 
               Power(t1,5)*(1109 + 5900*t2 + 4868*Power(t2,2) + 
                  752*Power(t2,3)) + 
               t1*(-2345 - 4906*t2 + 1511*Power(t2,2) + 
                  957*Power(t2,3)) + 
               Power(t1,3)*(-8733 - 16789*t2 + 2382*Power(t2,2) + 
                  5222*Power(t2,3)) + 
               Power(s1,2)*(597 + 63*Power(t1,6) - 153*t2 + 
                  t1*(-5779 + 74*t2) + 
                  2*Power(t1,2)*(9439 + 1350*t2) - 
                  Power(t1,5)*(4594 + 1899*t2) - 
                  2*Power(t1,3)*(13713 + 3410*t2) + 
                  Power(t1,4)*(18261 + 6065*t2)) + 
               s1*(-1161 + 460*Power(t1,7) + 238*t2 + 125*Power(t2,2) + 
                  Power(t1,6)*(453 + 1024*t2) + 
                  Power(t1,3)*(21146 + 29064*t2 - 1830*Power(t2,2)) + 
                  t1*(7608 + 2818*t2 - 1116*Power(t2,2)) - 
                  13*Power(t1,4)*(701 + 1458*t2 + 8*Power(t2,2)) + 
                  6*Power(t1,5)*(-97 + 430*t2 + 79*Power(t2,2)) + 
                  Power(t1,2)*(-18811 - 16770*t2 + 2484*Power(t2,2)))) - 
            Power(s2,3)*(-655 + 20*Power(t1,9) + 
               Power(s1,3)*(2 - 174*t1 + 1602*Power(t1,2) - 
                  4830*Power(t1,3) + 6071*Power(t1,4) - 
                  3078*Power(t1,5) + 403*Power(t1,6)) + 582*t2 + 
               119*Power(t2,2) - 122*Power(t2,3) + 
               38*Power(t1,8)*(22 + 5*t2) - 
               5*Power(t1,7)*(766 + 632*t2 + 153*Power(t2,2)) + 
               Power(t1,3)*(24090 + 26342*t2 - 6540*Power(t2,2) - 
                  4062*Power(t2,3)) + 
               Power(t1,5)*(8609 + 10888*t2 - 2306*Power(t2,2) - 
                  2584*Power(t2,3)) + 
               t1*(4599 - 852*t2 - 1915*Power(t2,2) + 
                  358*Power(t2,3)) + 
               Power(t1,6)*(3299 + 4254*t2 + 2461*Power(t2,2) + 
                  437*Power(t2,3)) + 
               Power(t1,2)*(-14053 - 7719*t2 + 5972*Power(t2,2) + 
                  856*Power(t2,3)) + 
               Power(t1,4)*(-22915 - 30525*t2 + 2974*Power(t2,2) + 
                  5121*Power(t2,3)) + 
               Power(s1,2)*(189*Power(t1,7) + 15*(-13 + 9*t2) - 
                  40*t1*(-85 + 33*t2) + Power(t1,3)*(41482 + 480*t2) - 
                  5*Power(t1,4)*(9314 + 1395*t2) + 
                  2*Power(t1,2)*(-8978 + 1455*t2) - 
                  Power(t1,6)*(5041 + 1584*t2) + 
                  Power(t1,5)*(24691 + 6366*t2)) + 
               s1*(893 + 268*Power(t1,8) - 878*t2 + 119*Power(t2,2) + 
                  14*Power(t1,7)*(91 + 4*t2) + 
                  Power(t1,2)*(32432 - 3872*t2 - 2670*Power(t2,2)) + 
                  Power(t1,4)*(48250 + 38186*t2 - 2327*Power(t2,2)) + 
                  2*t1*(-4489 + 2491*t2 + 64*Power(t2,2)) - 
                  2*Power(t1,5)*(8272 + 12989*t2 + 516*Power(t2,2)) + 
                  Power(t1,6)*(-1481 + 5544*t2 + 694*Power(t2,2)) + 
                  2*Power(t1,3)*(-28057 - 9020*t2 + 2538*Power(t2,2)))) \
+ Power(s2,2)*(307 + 5*Power(t1,10) + 
               Power(s1,3)*t1*
                (6 - 288*t1 + 1968*Power(t1,2) - 4883*Power(t1,3) + 
                  5178*Power(t1,4) - 2088*Power(t1,5) + 104*Power(t1,6)) \
- 428*t2 + 17*Power(t2,2) + 104*Power(t2,3) + 
               35*Power(t1,9)*(23 + 3*t2) - 
               3*Power(t1,8)*(1215 + 582*t2 + 68*Power(t2,2)) + 
               Power(t1,3)*(-34309 + 7527*t2 + 6958*Power(t2,2) - 
                  1570*Power(t2,3)) + 
               Power(t1,4)*(44546 + 13508*t2 - 7679*Power(t2,2) - 
                  1198*Power(t2,3)) + 
               Power(t1,6)*(11741 + 9706*t2 - 647*Power(t2,2) - 
                  1072*Power(t2,3)) + 
               Power(t1,7)*(2835 + 1861*t2 + 683*Power(t2,2) + 
                  144*Power(t2,3)) - 
               t1*(3760 - 4349*t2 + 14*Power(t2,2) + 803*Power(t2,3)) + 
               2*Power(t1,2)*
                (7883 - 5868*t2 - 1037*Power(t2,2) + 1029*Power(t2,3)) + 
               Power(t1,5)*(-34291 - 23146*t2 + 2960*Power(t2,2) + 
                  2340*Power(t2,3)) + 
               Power(s1,2)*(105*Power(t1,8) + 
                  Power(t1,4)*(48713 - 4950*t2) + 
                  Power(t1,2)*(6876 - 3300*t2) - 14*(-1 + t2) + 
                  9*t1*(-77 + 57*t2) - Power(t1,7)*(3079 + 666*t2) - 
                  Power(t1,5)*(45071 + 1962*t2) + 
                  Power(t1,6)*(19605 + 3166*t2) + 
                  Power(t1,3)*(-26470 + 7222*t2)) + 
               s1*(104*Power(t1,9) - 3*Power(t1,8)*(-359 + 88*t2) + 
                  Power(t1,5)*(60904 + 20014*t2 - 2172*Power(t2,2)) - 
                  23*(7 - 10*t2 + 3*Power(t2,2)) + 
                  6*Power(t1,7)*(-176 + 684*t2 + 63*Power(t2,2)) - 
                  6*Power(t1,2)*(3728 - 2777*t2 + 185*Power(t2,2)) - 
                  6*Power(t1,3)*(-10064 + 4064*t2 + 275*Power(t2,2)) - 
                  2*Power(t1,6)*(9112 + 8384*t2 + 315*Power(t2,2)) + 
                  t1*(3704 - 4000*t2 + 698*Power(t2,2)) + 
                  Power(t1,4)*(-84364 + 4406*t2 + 4546*Power(t2,2))))) + 
         Power(s,3)*(2*Power(s2,8)*
             (6*Power(s1,3) + Power(s1,2)*(-5 + 3*t1 - 21*t2) - 
               t2*(4 + 2*Power(t1,2) + 10*t2 + 9*Power(t2,2) - 
                  2*t1*(3 + 4*t2)) + 
               s1*(2 + 15*t2 + 24*Power(t2,2) - t1*(2 + 11*t2))) + 
            (-1 + t1)*(4*Power(t1,10) + 
               3*Power(t1,9)*(-81 + 2*s1 - 6*t2) - 
               2*Power(-1 + t2,2)*(-7 + 13*t2) + 
               t1*(-1 + t2)*(191 - 459*t2 + 286*Power(t2,2) + 
                  s1*(-50 + 54*t2)) + 
               Power(t1,8)*(582 - 16*Power(s1,2) + 289*t2 + 
                  8*Power(t2,2) + s1*(-303 + 136*t2)) + 
               Power(t1,2)*(-858 + 345*t2 + 1290*Power(t2,2) - 
                  825*Power(t2,3) + Power(s1,2)*(-113 + 91*t2) + 
                  s1*(621 - 520*t2 - 39*Power(t2,2))) + 
               Power(t1,4)*(-10573 + 241*Power(s1,3) + 7541*t2 - 
                  422*Power(t2,2) - 576*Power(t2,3) + 
                  Power(s1,2)*(-4527 + 1628*t2) + 
                  2*s1*(6695 - 3921*t2 + 10*Power(t2,2))) + 
               Power(t1,7)*(1264 + 593*Power(s1,2) - 68*t2 - 
                  55*Power(t2,2) - 10*Power(t2,3) - 
                  s1*(219 + 656*t2 + 44*Power(t2,2))) + 
               Power(t1,6)*(-6053 + 301*Power(s1,3) - 865*t2 + 
                  83*Power(t2,3) + Power(s1,2)*(-3670 + 133*t2) + 
                  s1*(6064 + 358*t2 + 61*Power(t2,2))) - 
               Power(t1,3)*(-5543 + 31*Power(s1,3) + 5544*t2 + 
                  321*Power(t2,2) - 1076*Power(t2,3) + 
                  184*Power(s1,2)*(-7 + 4*t2) + 
                  2*s1*(2684 - 2197*t2 + 93*Power(t2,2))) + 
               Power(t1,5)*(10511 - 499*Power(s1,3) + 
                  Power(s1,2)*(6445 - 1152*t2) - 2276*t2 + 
                  179*Power(t2,2) - 20*Power(t2,3) + 
                  s1*(-14241 + 4234*t2 + 170*Power(t2,2)))) + 
            Power(s2,7)*(Power(s1,3)*(82 - 139*t1) + 
               Power(t1,3)*(-20 + 49*t2) + 
               Power(t1,2)*(46 - 331*t2 - 398*Power(t2,2)) - 
               2*(-3 + 89*t2 + 171*Power(t2,2) + 99*Power(t2,3)) + 
               t1*(-32 + 460*t2 + 761*Power(t2,2) + 276*Power(t2,3)) + 
               Power(s1,2)*(-162 - 192*Power(t1,2) - 341*t2 + 
                  t1*(375 + 533*t2)) + 
               s1*(110 + 27*Power(t1,3) + 482*t2 + 457*Power(t2,2) + 
                  Power(t1,2)*(111 + 568*t2) - 
                  2*t1*(124 + 546*t2 + 335*Power(t2,2)))) + 
            Power(s2,6)*(80 - 10*Power(t1,5) + 
               Power(s1,3)*(37 - 256*t1 + 276*Power(t1,2)) + 
               Power(t1,4)*(201 - 244*t2) - 288*t2 - 717*Power(t2,2) - 
               494*Power(t2,3) + 
               Power(t1,3)*(-519 + 2032*t2 + 1768*Power(t2,2)) - 
               Power(t1,2)*(-555 + 3388*t2 + 4145*Power(t2,2) + 
                  1114*Power(t2,3)) + 
               t1*(-307 + 1888*t2 + 3091*Power(t2,2) + 
                  1526*Power(t2,3)) + 
               Power(s1,2)*(623*Power(t1,3) - 362*t2 - 
                  2*Power(t1,2)*(544 + 699*t2) + 2*t1*(231 + 782*t2)) + 
               s1*(103 - 181*Power(t1,4) + 512*t2 + 801*Power(t2,2) - 
                  2*Power(t1,3)*(267 + 1079*t2) + 
                  2*Power(t1,2)*(741 + 2281*t2 + 1109*Power(t2,2)) - 
                  2*t1*(435 + 1455*t2 + 1399*Power(t2,2)))) + 
            Power(s2,5)*(279 + 66*Power(t1,6) + 
               Power(s1,3)*(-121 + 325*t1 - 219*Power(t1,2) + 
                  55*Power(t1,3)) + 434*t2 - 491*Power(t2,2) - 
               648*Power(t2,3) + Power(t1,5)*(-579 + 728*t2) - 
               2*Power(t1,4)*(-753 + 3080*t2 + 1769*Power(t2,2)) + 
               Power(t1,3)*(-2379 + 10592*t2 + 9777*Power(t2,2) + 
                  2230*Power(t2,3)) + 
               t1*(-1398 + 94*t2 + 3995*Power(t2,2) + 
                  3216*Power(t2,3)) - 
               Power(t1,2)*(-2505 + 5688*t2 + 9797*Power(t2,2) + 
                  4836*Power(t2,3)) + 
               Power(s1,2)*(709 - 689*Power(t1,4) + 
                  Power(t1,2)*(2934 - 1693*t2) + 89*t2 + 
                  t1*(-2682 + 511*t2) + Power(t1,3)*(-326 + 975*t2)) + 
               2*s1*(-181 + 207*Power(t1,5) - 349*t2 + 
                  275*Power(t2,2) + 2*Power(t1,4)*(441 + 836*t2) + 
                  t1*(283 + 573*t2 - 1824*Power(t2,2)) + 
                  3*Power(t1,2)*(340 + 417*t2 + 1055*Power(t2,2)) - 
                  Power(t1,3)*(2211 + 3093*t2 + 1558*Power(t2,2)))) + 
            s2*(-94 + 10*Power(t1,10) + 
               Power(s1,3)*Power(t1,2)*
                (-89 + 915*t1 - 2856*Power(t1,2) + 3520*Power(t1,3) - 
                  1545*Power(t1,4) + 49*Power(t1,5)) + 347*t2 - 
               430*Power(t2,2) + 177*Power(t2,3) + 
               Power(t1,9)*(1057 + 214*t2) - 
               Power(t1,8)*(3705 + 2258*t2 + 128*Power(t2,2)) + 
               Power(t1,3)*(-34797 + 20670*t2 + 2093*Power(t2,2) - 
                  2102*Power(t2,3)) + 
               Power(t1,6)*(23562 + 7643*t2 - 1000*Power(t2,2) - 
                  1086*Power(t2,3)) + 
               t1*(-1546 + 1117*t2 + 1410*Power(t2,2) - 
                  1077*Power(t2,3)) + 
               3*Power(t1,7)*
                (-503 + 723*t2 + 193*Power(t2,2) + 48*Power(t2,3)) - 
               Power(t1,4)*(-52971 + 8120*t2 + 3142*Power(t2,2) + 
                  775*Power(t2,3)) + 
               2*Power(t1,2)*
                (6164 - 5561*t2 - 824*Power(t2,2) + 1251*Power(t2,3)) + 
               Power(t1,5)*(-48277 - 10660*t2 + 2266*Power(t2,2) + 
                  2223*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-202 + 147*Power(t1,7) + 
                  Power(t1,3)*(37181 - 6928*t2) + 
                  t1*(3351 - 1807*t2) + 158*t2 - 
                  7*Power(t1,6)*(488 + 41*t2) + 
                  Power(t1,5)*(19229 + 949*t2) + 
                  Power(t1,4)*(-39404 + 2090*t2) + 
                  Power(t1,2)*(-16886 + 5843*t2)) + 
               s1*(30 - 24*Power(t1,9) + Power(t1,8)*(2004 - 692*t2) - 
                  64*t2 + 34*Power(t2,2) + 
                  Power(t1,5)*(64238 + 2542*t2 - 1569*Power(t2,2)) + 
                  Power(t1,3)*(47262 - 25954*t2 - 436*Power(t2,2)) + 
                  t1*(1159 - 1094*t2 + 59*Power(t2,2)) - 
                  2*Power(t1,2)*(6560 - 5197*t2 + 252*Power(t2,2)) + 
                  Power(t1,7)*(-2177 + 4970*t2 + 302*Power(t2,2)) - 
                  Power(t1,6)*(20495 + 11878*t2 + 418*Power(t2,2)) + 
                  Power(t1,4)*(-78877 + 21776*t2 + 2514*Power(t2,2)))) + 
            Power(s2,3)*(957 + 180*Power(t1,8) + 
               Power(s1,3)*(-27 + 629*t1 - 3083*Power(t1,2) + 
                  5403*Power(t1,3) - 3654*Power(t1,4) + 723*Power(t1,5)\
) - 300*t2 - 143*Power(t2,2) - 58*Power(t2,3) + 
               Power(t1,7)*(766 + 1369*t2) - 
               Power(t1,6)*(3407 + 11287*t2 + 2336*Power(t2,2)) + 
               Power(t1,4)*(20896 + 8323*t2 - 11630*Power(t2,2) - 
                  7912*Power(t2,3)) + 
               Power(t1,2)*(18510 + 19537*t2 - 6161*Power(t2,2) - 
                  7318*Power(t2,3)) + 
               t1*(-6283 - 3488*t2 + 1941*Power(t2,2) + 
                  1722*Power(t2,3)) + 
               Power(t1,5)*(-2912 + 15675*t2 + 8223*Power(t2,2) + 
                  1818*Power(t2,3)) + 
               Power(t1,3)*(-28707 - 29829*t2 + 10139*Power(t2,2) + 
                  11734*Power(t2,3)) + 
               Power(s1,2)*(623 + 494*Power(t1,6) + 
                  Power(t1,2)*(28664 - 163*t2) - 227*t2 + 
                  t1*(-7600 + 1151*t2) - 
                  Power(t1,5)*(9577 + 2133*t2) - 
                  Power(t1,3)*(46258 + 4665*t2) + 
                  Power(t1,4)*(33687 + 6041*t2)) + 
               s1*(-1732 + 175*Power(t1,7) + 
                  Power(t1,6)*(6177 - 228*t2) + 978*t2 - 
                  32*Power(t2,2) + 
                  Power(t1,3)*(41295 + 36814*t2 - 6572*Power(t2,2)) + 
                  Power(t1,5)*(-10857 + 10676*t2 + 132*Power(t2,2)) - 
                  2*t1*(-6832 + 775*t2 + 597*Power(t2,2)) + 
                  7*Power(t1,2)*(-5265 - 1766*t2 + 730*Power(t2,2)) + 
                  Power(t1,4)*(-11867 - 34394*t2 + 2575*Power(t2,2)))) + 
            Power(s2,2)*(676 - 90*Power(t1,9) + 
               Power(s1,3)*t1*
                (85 - 1143*t1 + 4285*Power(t1,2) - 6182*Power(t1,3) + 
                  3300*Power(t1,4) - 316*Power(t1,5)) - 771*t2 - 
               68*Power(t2,2) + 211*Power(t2,3) - 
               Power(t1,8)*(1593 + 784*t2) + 
               Power(t1,7)*(5914 + 6896*t2 + 778*Power(t2,2)) + 
               Power(t1,4)*(53315 + 32941*t2 - 7226*Power(t2,2) - 
                  7950*Power(t2,3)) + 
               t1*(-6804 + 5199*t2 + 608*Power(t2,2) - 
                  717*Power(t2,3)) - 
               Power(t1,6)*(-1678 + 7808*t2 + 3001*Power(t2,2) + 
                  726*Power(t2,3)) - 
               Power(t1,2)*(-24584 + 5332*t2 + 3809*Power(t2,2) + 
                  753*Power(t2,3)) + 
               Power(t1,5)*(-30248 - 15173*t2 + 4840*Power(t2,2) + 
                  4218*Power(t2,3)) + 
               Power(t1,3)*(-47432 - 15168*t2 + 7855*Power(t2,2) + 
                  5695*Power(t2,3)) + 
               Power(s1,2)*(89 - 433*Power(t1,7) + 
                  Power(t1,2)*(17538 - 4651*t2) - 67*t2 + 
                  6*Power(t1,6)*(1331 + 217*t2) + 
                  5*Power(t1,4)*(12133 + 332*t2) + 
                  t1*(-2573 + 1207*t2) - 
                  Power(t1,5)*(35475 + 4504*t2) + 
                  Power(t1,3)*(-47820 + 4973*t2)) + 
               s1*(-532 + 3*Power(t1,8) + 566*t2 - 96*Power(t2,2) + 
                  2*Power(t1,7)*(-2485 + 601*t2) + 
                  Power(t1,3)*(81738 - 3316*t2 - 5553*Power(t2,2)) + 
                  Power(t1,6)*(7433 - 11318*t2 - 680*Power(t2,2)) + 
                  Power(t1,5)*(24431 + 32028*t2 + 198*Power(t2,2)) + 
                  t1*(8819 - 6370*t2 + 345*Power(t2,2)) + 
                  Power(t1,2)*(-40793 + 15924*t2 + 1557*Power(t2,2)) + 
                  Power(t1,4)*(-76129 - 28670*t2 + 4302*Power(t2,2)))) + 
            Power(s2,4)*(460 - 160*Power(t1,7) + 
               Power(s1,3)*(-129 + 1035*t1 - 2303*Power(t1,2) + 
                  1991*Power(t1,3) - 660*Power(t1,4)) + 1087*t2 - 
               270*Power(t2,2) - 507*Power(t2,3) - 
               5*Power(t1,6)*(-83 + 262*t2) + 
               Power(t1,5)*(-660 + 10789*t2 + 3830*Power(t2,2)) + 
               Power(t1,2)*(8432 + 9124*t2 - 8605*Power(t2,2) - 
                  8637*Power(t2,3)) - 
               Power(t1,4)*(-3917 + 17335*t2 + 12111*Power(t2,2) + 
                  2600*Power(t2,3)) + 
               t1*(-3305 - 6341*t2 + 2405*Power(t2,2) + 
                  3639*Power(t2,3)) + 
               Power(t1,3)*(-9099 + 3986*t2 + 14781*Power(t2,2) + 
                  8195*Power(t2,3)) + 
               Power(s1,2)*(1133 + 60*Power(t1,5) + 21*t2 - 
                  t1*(7762 + 751*t2) + Power(t1,4)*(5447 + 1050*t2) + 
                  Power(t1,2)*(17564 + 2051*t2) - 
                  Power(t1,3)*(16412 + 2149*t2)) - 
               s1*(1379 + 420*Power(t1,6) + 648*t2 - 275*Power(t2,2) + 
                  Power(t1,5)*(4239 + 2150*t2) + 
                  Power(t1,2)*(9772 + 16212*t2 - 6573*Power(t2,2)) + 
                  Power(t1,4)*(-8701 + 850*t2 - 1810*Power(t2,2)) + 
                  t1*(-6741 - 6644*t2 + 2455*Power(t2,2)) + 
                  Power(t1,3)*(-368 - 13156*t2 + 6449*Power(t2,2))))) + 
         Power(s,5)*(4 - 56*Power(t1,9) + 
            7*Power(t1,8)*(-37 + 30*s1 - 48*t2) + 30*t2 - 
            114*Power(t2,2) + 84*Power(t2,3) + 
            Power(t1,7)*(-331 - 112*Power(s1,2) + 1897*t2 + 
               56*Power(t2,2) + 7*s1*(-261 + 100*t2)) + 
            Power(t1,6)*(6280 + 1715*Power(s1,2) - 1751*t2 - 
               245*Power(t2,2) - 112*Power(t2,3) - 
               14*s1*(-130 + 206*t2 + 13*Power(t2,2))) + 
            t1*(94 - 452*t2 + 615*Power(t2,2) - 308*Power(t2,3) - 
               2*s1*(51 - 172*t2 + 119*Power(t2,2))) + 
            Power(t1,5)*(-14135 + 315*Power(s1,3) - 973*t2 + 
               229*Power(t2,2) + 791*Power(t2,3) + 
               7*Power(s1,2)*(-911 + 21*t2) + 
               s1*(6859 + 3086*t2 + 315*Power(t2,2))) + 
            Power(t1,4)*(15297 - 492*Power(s1,3) + 
               Power(s1,2)*(8295 - 879*t2) - 999*t2 + 219*Power(t2,2) - 
               1615*Power(t2,3) + 
               s1*(-15346 + 1856*t2 + 361*Power(t2,2))) + 
            Power(t1,3)*(-9174 + 218*Power(s1,3) + 3848*t2 - 
               351*Power(t2,2) + 1142*Power(t2,3) + 
               Power(s1,2)*(-4282 + 966*t2) - 
               2*s1*(-5355 + 1877*t2 + 491*Power(t2,2))) + 
            Power(t1,2)*(2284 - 24*Power(s1,3) + 
               Power(s1,2)*(732 - 278*t2) - 1268*t2 - 438*Power(t2,2) + 
               8*Power(t2,3) + s1*(-2320 + 710*t2 + 763*Power(t2,2))) + 
            2*Power(s2,6)*(17 + 20*Power(s1,3) - 2*Power(t1,3) + 
               Power(t1,2)*(30 - 77*t2) - 65*t2 - 98*Power(t2,2) - 
               84*Power(t2,3) - 5*Power(s1,2)*(2 + 5*t1 + 21*t2) + 
               t1*(-45 + 132*t2 + 56*Power(t2,2)) + 
               s1*(25 + 37*Power(t1,2) + 85*t2 + 168*Power(t2,2) - 
                  t1*(52 + 7*t2))) + 
            Power(s2,5)*(Power(s1,3)*(97 - 235*t1) + 86*Power(t1,4) + 
               Power(t1,3)*(-769 + 1221*t2) + 
               Power(t1,2)*(1356 - 3197*t2 - 1146*Power(t2,2)) - 
               2*(-103 + 286*t2 + 521*Power(t2,2) + 448*Power(t2,3)) + 
               t1*(-888 + 2564*t2 + 2545*Power(t2,2) + 
                  1302*Power(t2,3)) + 
               Power(s1,2)*(-104 + 102*Power(t1,2) - 665*t2 + 
                  t1*(256 + 1275*t2)) + 
               s1*(316 - 515*Power(t1,3) + 702*t2 + 1395*Power(t2,2) + 
                  2*Power(t1,2)*(769 + 240*t2) - 
                  2*t1*(677 + 893*t2 + 1134*Power(t2,2)))) + 
            s2*(28 + 434*Power(t1,8) + 
               Power(s1,3)*t1*
                (36 - 448*t1 + 1314*Power(t1,2) - 1125*Power(t1,3) + 
                  119*Power(t1,4)) - 102*t2 + 92*Power(t2,2) + 
               37*Power(t2,3) + 91*Power(t1,7)*(-5 + 24*t2) - 
               4*Power(t1,6)*(-970 + 2591*t2 + 133*Power(t2,2)) + 
               Power(t1,2)*(12815 - 2629*t2 + 1064*Power(t2,2) - 
                  4841*Power(t2,3)) + 
               Power(t1,4)*(34205 + 2924*t2 - 3568*Power(t2,2) - 
                  4740*Power(t2,3)) + 
               Power(t1,5)*(-19984 + 9968*t2 + 2211*Power(t2,2) + 
                  938*Power(t2,3)) + 
               t1*(-2545 + 1890*t2 - 1085*Power(t2,2) + 
                  1013*Power(t2,3)) + 
               Power(t1,3)*(-28460 - 3891*t2 + 1974*Power(t2,2) + 
                  7619*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-980 + 665*Power(t1,5) + t1*(7724 - 1596*t2) + 
                  354*t2 - 7*Power(t1,4)*(922 + 51*t2) + 
                  Power(t1,3)*(18131 + 389*t2) + 
                  2*Power(t1,2)*(-9440 + 717*t2)) + 
               s1*(20 - 994*Power(t1,7) + 
                  Power(t1,6)*(8244 - 2716*t2) - 78*t2 + 
                  46*Power(t2,2) + 
                  Power(t1,3)*(26793 + 2202*t2 - 2437*Power(t2,2)) + 
                  t1*(2850 - 1138*t2 - 571*Power(t2,2)) + 
                  2*Power(t1,4)*(-5354 - 7003*t2 + 99*Power(t2,2)) + 
                  2*Power(t1,5)*(-5027 + 5607*t2 + 252*Power(t2,2)) + 
                  Power(t1,2)*(-16109 + 4160*t2 + 2114*Power(t2,2)))) + 
            Power(s2,4)*(478 - 460*Power(t1,5) + 
               Power(s1,3)*(41 - 266*t1 + 252*Power(t1,2)) - 148*t2 - 
               1297*Power(t2,2) - 1718*Power(t2,3) - 
               4*Power(t1,4)*(-753 + 965*t2) + 
               Power(t1,3)*(-6158 + 12404*t2 + 3073*Power(t2,2)) - 
               Power(t1,2)*(-6036 + 11942*t2 + 8495*Power(t2,2) + 
                  3452*Power(t2,3)) + 
               t1*(-2851 + 3810*t2 + 6695*Power(t2,2) + 
                  5044*Power(t2,3)) + 
               Power(s1,2)*(119 - 387*Power(t1,3) + 
                  Power(t1,2)*(649 - 1956*t2) - 536*t2 + 
                  t1*(-318 + 2350*t2)) + 
               s1*(331 + 1505*Power(t1,4) + 194*t2 + 1779*Power(t2,2) - 
                  2*Power(t1,3)*(3279 + 233*t2) + 
                  3*Power(t1,2)*(2467 + 770*t2 + 1537*Power(t2,2)) - 
                  2*t1*(1474 + 1047*t2 + 3074*Power(t2,2)))) - 
            Power(s2,2)*(-514 + 1000*Power(t1,7) + 
               Power(s1,3)*(12 - 269*t1 + 1065*Power(t1,2) - 
                  1224*Power(t1,3) + 316*Power(t1,4)) + 363*t2 - 
               725*Power(t2,2) + 689*Power(t2,3) + 
               15*Power(t1,6)*(-223 + 350*t2) + 
               Power(t1,5)*(10527 - 21860*t2 - 2010*Power(t2,2)) + 
               t1*(4937 + 1019*t2 + 559*Power(t2,2) - 
                  5161*Power(t2,3)) + 
               Power(t1,4)*(-26067 + 22140*t2 + 7691*Power(t2,2) + 
                  2910*Power(t2,3)) - 
               Power(t1,3)*(-31135 + 1249*t2 + 11204*Power(t2,2) + 
                  10346*Power(t2,3)) + 
               Power(t1,2)*(-17836 - 5995*t2 + 5672*Power(t2,2) + 
                  11808*Power(t2,3)) + 
               Power(s1,2)*(-277 + 1287*Power(t1,5) + 
                  t1*(4004 - 765*t2) + 79*t2 + 
                  Power(t1,3)*(17759 + 188*t2) - 
                  2*Power(t1,4)*(4418 + 315*t2) + 
                  Power(t1,2)*(-13781 + 1188*t2)) - 
               s1*(2075*Power(t1,6) + 2*Power(t1,5)*(-7408 + 1875*t2) + 
                  Power(t1,3)*(29 + 16836*t2 - 4530*Power(t2,2)) + 
                  t1*(6289 - 212*t2 - 2125*Power(t2,2)) + 
                  6*(-108 + 25*t2 + 24*Power(t2,2)) + 
                  Power(t1,4)*(19345 - 14566*t2 + 636*Power(t2,2)) + 
                  Power(t1,2)*(-12601 - 5836*t2 + 5736*Power(t2,2)))) + 
            Power(s2,3)*(517 + 1000*Power(t1,6) + 
               Power(s1,3)*(-39 + 206*t1 - 249*Power(t1,2) + 
                  140*Power(t1,3)) + 525*t2 - 41*Power(t2,2) - 
               1627*Power(t2,3) + Power(t1,5)*(-4934 + 6195*t2) + 
               Power(t1,4)*(11869 - 22861*t2 - 3575*Power(t2,2)) + 
               Power(t1,3)*(-17524 + 23404*t2 + 11880*Power(t2,2) + 
                  4403*Power(t2,3)) + 
               t1*(-4637 - 1523*t2 + 4887*Power(t2,2) + 
                  7533*Power(t2,3)) - 
               Power(t1,2)*(-13576 + 6308*t2 + 13544*Power(t2,2) + 
                  10537*Power(t2,3)) + 
               Power(s1,2)*(537 + 1069*Power(t1,4) + 
                  Power(t1,2)*(6436 - 2028*t2) - 144*t2 + 
                  8*t1*(-415 + 147*t2) + Power(t1,3)*(-4990 + 618*t2)) + 
               s1*(-438 - 2355*Power(t1,5) + 
                  Power(t1,4)*(13519 - 1734*t2) - 440*t2 + 
                  883*Power(t2,2) + 
                  t1*(541 + 1890*t2 - 5495*Power(t2,2)) + 
                  Power(t1,3)*(-17200 + 5546*t2 - 3641*Power(t2,2)) + 
                  Power(t1,2)*(6478 - 4576*t2 + 8796*Power(t2,2))))) + 
         Power(s,4)*(14 - 4*Power(t1,10) + 
            Power(t1,9)*(-431 + 72*s1 - 108*t2) - 84*t2 + 
            138*Power(t2,2) - 68*Power(t2,3) + 
            Power(t1,8)*(933 - 56*Power(s1,2) + 917*t2 + 
               28*Power(t2,2) + 7*s1*(-141 + 56*t2)) + 
            Power(t1,4)*(-19074 + 550*Power(s1,3) + 7977*t2 - 
               93*Power(t2,2) + 190*Power(t2,3) + 
               Power(s1,2)*(-9448 + 2250*t2) + 
               s1*(23734 - 9334*t2 - 855*Power(t2,2))) + 
            Power(t1,7)*(3050 + 1295*Power(s1,2) - 853*t2 - 
               161*Power(t2,2) - 44*Power(t2,3) - 
               56*s1*(-15 + 35*t2 + 2*Power(t2,2))) + 
            t1*(-234 + 895*t2 - 1149*Power(t2,2) + 504*Power(t2,3) + 
               2*s1*(56 - 135*t2 + 77*Power(t2,2))) + 
            Power(t1,2)*(-1647 + 6*Power(s1,3) + 423*t2 + 
               2106*Power(t2,2) - 1177*Power(t2,3) + 
               Power(s1,2)*(-390 + 227*t2) - 
               5*s1*(-296 + 120*t2 + 101*Power(t2,2))) + 
            Power(t1,6)*(-12898 + 385*Power(s1,3) - 1523*t2 + 
               184*Power(t2,2) + 371*Power(t2,3) + 
               35*Power(s1,2)*(-189 + 5*t2) + 
               s1*(8451 + 2500*t2 + 231*Power(t2,2))) + 
            Power(t1,5)*(20730 - 810*Power(s1,3) - 994*t2 + 
               67*Power(t2,2) - 772*Power(t2,3) - 
               55*Power(s1,2)*(-217 + 25*t2) + 
               s1*(-23112 + 3420*t2 + 290*Power(t2,2))) + 
            Power(t1,3)*(9561 - 128*Power(s1,3) + 
               Power(s1,2)*(3279 - 1286*t2) - 6650*t2 - 
               1120*Power(t2,2) + 993*Power(t2,3) + 
               2*s1*(-5295 + 2926*t2 + 403*Power(t2,2))) - 
            2*Power(s2,7)*(3 + 15*Power(s1,3) + 
               Power(t1,2)*(4 - 19*t2) - 25*t2 - 42*Power(t2,2) - 
               36*Power(t2,3) - Power(s1,2)*(10 + 3*t1 + 63*t2) + 
               t1*(-7 + 43*t2 + 28*Power(t2,2)) + 
               s1*(11 + 6*Power(t1,2) + 48*t2 + 84*Power(t2,2) - 
                  t1*(16 + 21*t2))) + 
            Power(s2,6)*(-64 - 11*Power(t1,4) + 
               2*Power(s1,3)*(-59 + 120*t1) + 
               Power(t1,3)*(193 - 375*t2) + 440*t2 + 746*Power(t2,2) + 
               518*Power(t2,3) + 
               2*Power(t1,2)*(-193 + 705*t2 + 428*Power(t2,2)) - 
               t1*(-268 + 1472*t2 + 1735*Power(t2,2) + 
                  756*Power(t2,3)) + 
               Power(s1,2)*(186 + 157*Power(t1,2) + 610*t2 - 
                  3*t1*(152 + 359*t2)) + 
               s1*(-244 + 74*Power(t1,3) - 772*t2 - 999*Power(t2,2) - 
                  Power(t1,2)*(597 + 836*t2) + 
                  2*t1*(382 + 927*t2 + 791*Power(t2,2)))) - 
            Power(s2,2)*(791 + 435*Power(t1,8) + 
               2*Power(s1,3)*
                (-3 + 129*t1 - 881*Power(t1,2) + 1899*Power(t1,3) - 
                  1410*Power(t1,4) + 220*Power(t1,5)) - 773*t2 + 
               226*Power(t2,2) - 23*Power(t2,3) + 
               Power(t1,7)*(337 + 2723*t2) - 
               2*Power(t1,6)*(103 + 7903*t2 + 808*Power(t2,2)) + 
               Power(t1,4)*(41904 + 8899*t2 - 10609*Power(t2,2) - 
                  8720*Power(t2,3)) + 
               Power(t1,2)*(23971 + 7086*t2 - 3319*Power(t2,2) - 
                  8267*Power(t2,3)) + 
               t1*(-7063 + 1990*t2 - 333*Power(t2,2) + 
                  1846*Power(t2,3)) + 
               Power(t1,5)*(-16660 + 17739*t2 + 6379*Power(t2,2) + 
                  1892*Power(t2,3)) + 
               Power(t1,3)*(-43509 - 21903*t2 + 9401*Power(t2,2) + 
                  13290*Power(t2,3)) + 
               Power(s1,2)*(232 + 981*Power(t1,6) + 
                  Power(t1,2)*(22120 - 2799*t2) - 105*t2 + 
                  7*Power(t1,3)*(-5996 + 45*t2) - 
                  3*Power(t1,5)*(3725 + 448*t2) + 
                  t1*(-4583 + 1240*t2) + Power(t1,4)*(34542 + 2890*t2)) \
+ s1*(-846 - 802*Power(t1,7) + Power(t1,6)*(11387 - 2788*t2) + 590*t2 - 
                  41*Power(t2,2) + 
                  Power(t1,3)*(46940 + 19846*t2 - 6488*Power(t2,2)) + 
                  2*Power(t1,5)*(-8489 + 8502*t2 + 171*Power(t2,2)) - 
                  2*t1*(-5395 + 2016*t2 + 392*Power(t2,2)) + 
                  2*Power(t1,4)*(-6876 - 16211*t2 + 1150*Power(t2,2)) + 
                  2*Power(t1,2)*(-18347 + 764*t2 + 2274*Power(t2,2)))) + 
            s2*(52 + 126*Power(t1,9) + 
               Power(s1,3)*t1*
                (-12 + 318*t1 - 1654*Power(t1,2) + 2910*Power(t1,3) - 
                  1710*Power(t1,4) + 105*Power(t1,5)) - 248*t2 + 
               407*Power(t2,2) - 227*Power(t2,3) + 
               7*Power(t1,8)*(167 + 130*t2) - 
               6*Power(t1,7)*(409 + 1005*t2 + 56*Power(t2,2)) + 
               Power(t1,5)*(37067 + 8274*t2 - 2679*Power(t2,2) - 
                  2988*Power(t2,3)) + 
               Power(t1,2)*(-15574 + 8952*t2 + 546*Power(t2,2) + 
                  305*Power(t2,3)) + 
               Power(t1,6)*(-10552 + 5870*t2 + 1485*Power(t2,2) + 
                  476*Power(t2,3)) + 
               t1*(2597 - 2026*t2 - 803*Power(t2,2) + 748*Power(t2,3)) - 
               2*Power(t1,3)*
                (-18694 + 2643*t2 + 871*Power(t2,2) + 2030*Power(t2,3)) \
+ Power(t1,4)*(-49819 - 10414*t2 + 3172*Power(t2,2) + 
                  5775*Power(t2,3)) + 
               Power(s1,2)*t1*
                (624 + 413*Power(t1,6) + 
                  Power(t1,2)*(24607 - 4622*t2) - 334*t2 + 
                  40*Power(t1,4)*(611 + 23*t2) - 
                  7*Power(t1,5)*(869 + 59*t2) + 
                  Power(t1,3)*(-36981 + 2165*t2) + t1*(-6970 + 2399*t2)) \
- s1*(40 + 364*Power(t1,8) - 98*t2 + 54*Power(t2,2) + 
                  4*Power(t1,7)*(-1327 + 441*t2) + 
                  Power(t1,6)*(6733 - 9716*t2 - 518*Power(t2,2)) - 
                  2*t1*(-1220 + 763*t2 + 121*Power(t2,2)) + 
                  2*Power(t1,5)*(10282 + 8663*t2 + 183*Power(t2,2)) - 
                  4*Power(t1,3)*(-12934 + 3335*t2 + 784*Power(t2,2)) + 
                  Power(t1,2)*(-19791 + 9904*t2 + 1212*Power(t2,2)) + 
                  Power(t1,4)*(-56776 - 4214*t2 + 2365*Power(t2,2)))) - 
            Power(s2,5)*(294 - 114*Power(t1,5) + 
               Power(s1,3)*(62 - 366*t1 + 375*Power(t1,2)) - 380*t2 - 
               1329*Power(t2,2) - 1166*Power(t2,3) - 
               33*Power(t1,4)*(-33 + 46*t2) + 
               Power(t1,3)*(-2431 + 6858*t2 + 3008*Power(t2,2)) - 
               Power(t1,2)*(-2497 + 8580*t2 + 7575*Power(t2,2) + 
                  2519*Power(t2,3)) + 
               t1*(-1335 + 3692*t2 + 5878*Power(t2,2) + 
                  3526*Power(t2,3)) + 
               Power(s1,2)*(54 + 349*Power(t1,3) - 609*t2 - 
                  2*Power(t1,2)*(281 + 1125*t2) + t1*(173 + 2572*t2)) + 
               s1*(265 + 252*Power(t1,4) + 592*t2 + 1570*Power(t2,2) - 
                  8*Power(t1,3)*(376 + 291*t2) - 
                  2*t1*(1079 + 1804*t2 + 2714*Power(t2,2)) + 
                  Power(t1,2)*(4577 + 5348*t2 + 4233*Power(t2,2)))) + 
            Power(s2,4)*(-513 - 390*Power(t1,6) + 
               Power(s1,3)*(96 - 298*t1 + 193*Power(t1,2) - 
                  80*Power(t1,3)) + Power(t1,5)*(2339 - 3290*t2) - 
               779*t2 + 676*Power(t2,2) + 1331*Power(t2,3) + 
               Power(t1,4)*(-5653 + 16311*t2 + 4710*Power(t2,2)) + 
               t1*(3531 + 982*t2 - 6447*Power(t2,2) - 
                  6374*Power(t2,3)) - 
               Power(t1,3)*(-8770 + 21342*t2 + 14212*Power(t2,2) + 
                  4135*Power(t2,3)) + 
               Power(t1,2)*(-8084 + 8294*t2 + 15495*Power(t2,2) + 
                  9317*Power(t2,3)) + 
               Power(s1,2)*(-826 - 120*Power(t1,4) + 
                  t1*(3811 - 1252*t2) + 83*t2 - 
                  5*Power(t1,3)*(-618 + 251*t2) + 
                  Power(t1,2)*(-5737 + 2753*t2)) + 
               s1*(482 + 620*Power(t1,5) + 726*t2 - 987*Power(t2,2) - 
                  Power(t1,4)*(8069 + 1880*t2) + 
                  Power(t1,2)*(-5879 + 728*t2 - 10326*Power(t2,2)) + 
                  2*Power(t1,3)*(6484 + 1017*t2 + 2385*Power(t2,2)) + 
                  t1*(-298 - 2048*t2 + 6164*Power(t2,2)))) + 
            Power(s2,3)*(-794 + 600*Power(t1,7) + 
               2*Power(s1,3)*(34 - 377*t1 + 1029*Power(t1,2) - 
                  968*Power(t1,3) + 290*Power(t1,4)) - 420*t2 - 
               139*Power(t2,2) + 780*Power(t2,3) + 
               2*Power(t1,6)*(-918 + 2015*t2) + 
               Power(t1,5)*(4909 - 21470*t2 - 3810*Power(t2,2)) + 
               t1*(5828 + 5730*t2 - 2364*Power(t2,2) - 
                  5738*Power(t2,3)) + 
               Power(t1,4)*(-15691 + 26900*t2 + 13347*Power(t2,2) + 
                  3760*Power(t2,3)) - 
               Power(t1,3)*(-24533 + 2864*t2 + 18496*Power(t2,2) + 
                  12408*Power(t2,3)) + 
               Power(t1,2)*(-17549 - 12062*t2 + 11406*Power(t2,2) + 
                  13466*Power(t2,3)) + 
               Power(s1,2)*(-898 + 930*Power(t1,5) + 
                  t1*(7793 - 516*t2) + 133*t2 + 
                  5*Power(t1,2)*(-4137 + 34*t2) - 
                  3*Power(t1,4)*(3201 + 325*t2) + 
                  Power(t1,3)*(22441 + 1004*t2)) - 
               s1*(-1559 + 940*Power(t1,6) + 68*t2 + 260*Power(t2,2) + 
                  2*Power(t1,5)*(-6346 + 535*t2) + 
                  Power(t1,3)*(-2326 + 21168*t2 - 8340*Power(t2,2)) + 
                  t1*(9060 + 3356*t2 - 3292*Power(t2,2)) + 
                  Power(t1,4)*(20218 - 10804*t2 + 2015*Power(t2,2)) + 
                  Power(t1,2)*(-13797 - 14936*t2 + 9049*Power(t2,2))))))*
       R1q(1 - s + s2 - t1))/
     (s*(-1 + s1)*(-1 + s2)*Power(-s + s2 - t1,3)*(1 - s + s2 - t1)*
       Power(-1 + s + t1,3)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*
       (-1 + t2)) - (8*(2*Power(s,8)*
          (2*Power(t1,3) + Power(t1,2)*(-1 + 3*s1 - 5*t2) - Power(t2,3)) \
- Power(s,7)*(-5*Power(t1,4) + 2*(3 - 7*t2)*Power(t2,2) + 
            Power(t1,3)*(44 - 3*s1 + 13*t2) + 
            Power(t1,2)*(-22 + 38*s1 + 6*Power(s1,2) - 50*t2 - 
               11*s1*t2 + Power(t2,2)) + 
            t1*t2*(6 - 6*s1 + 2*s1*t2 + 3*Power(t2,2)) + 
            s2*(19*Power(t1,3) + Power(t1,2)*(-24 + 36*s1 - 61*t2) + 
               2*(-3 + 3*s1 - 8*t2)*Power(t2,2) + 
               2*t1*(2 + 3*s1*(-2 + t2) + 7*t2 + Power(t2,2)))) + 
         Power(s,6)*(Power(t1,5) - Power(t1,4)*(53 + 5*s1 + t2) - 
            2*t2*(3 - 18*t2 + 19*Power(t2,2)) + 
            Power(t1,3)*(157 - 6*Power(s1,2) + 33*t2 - Power(t2,2) + 
               s1*(5 + 14*t2)) - 
            Power(t1,2)*(116 + Power(s1,2)*(-46 + t2) + 48*t2 + 
               Power(t2,2) - Power(t2,3) + 
               s1*(-79 + 61*t2 + 3*Power(t2,2))) + 
            t1*(-6 + 36*t2 + Power(t2,2) + 19*Power(t2,3) + 
               s1*(6 - 34*t2 + 4*Power(t2,2))) + 
            s2*(-20*Power(t1,4) + Power(t1,3)*(188 - 28*s1 + 69*t2) + 
               Power(t1,2)*(-207 + 23*Power(s1,2) + s1*(130 - 61*t2) - 
                  176*t2 + 2*Power(t2,2)) + 
               t2*(6 + 6*t2 - 75*Power(t2,2) + s1*(-6 + 28*t2)) + 
               t1*(52 + 2*Power(s1,2)*(-3 + t2) + 76*t2 + 
                  27*Power(t2,2) + 21*Power(t2,3) + 
                  s1*(-82 + 8*t2 + 3*Power(t2,2)))) + 
            Power(s2,2)*(35*Power(t1,3) - 6*Power(s1,2)*(t1 + t2) - 
               2*Power(t1,2)*(37 + 78*t2) + 
               2*t1*(13 + 38*t2 + 7*Power(t2,2)) - 
               2*(1 + 5*t2 + 19*Power(t2,2) + 28*Power(t2,3)) + 
               s1*(89*Power(t1,2) + 16*t1*(-3 + 2*t2) + 
                  6*(1 + t2 + 7*Power(t2,2))))) + 
         Power(-1 + s2,4)*((-1 + s1)*Power(t1,5)*(1 + s1 - 2*t2) + 
            2*Power(s2,4)*Power(s1 - t2,2)*(-1 + s1 + t1 - t2) + 
            4*Power(-1 + t2,3) - 
            t1*Power(-1 + t2,2)*(-25 + 8*s1 + 7*t2) + 
            Power(t1,3)*(8 - 3*Power(s1,3) - 41*t2 + 53*Power(t2,2) + 
               2*Power(t2,3) + Power(s1,2)*(17 + 11*t2) + 
               s1*(16 - 44*t2 - 19*Power(t2,2))) + 
            Power(t1,4)*(6 - 3*Power(s1,2) - Power(s1,3) + t2 - 
               12*Power(t2,2) + s1*(-12 + 19*t2 + 2*Power(t2,2))) + 
            Power(t1,2)*(-34 + 2*Power(s1,3) + 83*t2 - 68*Power(t2,2) + 
               3*Power(t2,3) - 5*Power(s1,2)*(3 + t2) + 
               s1*(4 + 11*t2 + 19*Power(t2,2))) + 
            Power(s2,2)*(-1 + t1)*
             (-3 - Power(s1,3)*(2 + t1) - 8*t2 + 29*Power(t2,2) - 
               2*Power(t2,3) + Power(t1,2)*(1 + 4*t2) + 
               t1*(2 + 4*t2 - 21*Power(t2,2) + Power(t2,3)) + 
               Power(s1,2)*(-12*t1 + 5*Power(t1,2) + 5*(3 + t2)) + 
               s1*(16 - 51*t2 + Power(t2,2) - 
                  2*Power(t1,2)*(3 + 2*t2) + 
                  t1*(-10 + 39*t2 - 2*Power(t2,2)))) + 
            Power(s2,3)*(-2 - Power(s1,3)*(1 + 3*t1) + t2 - 
               4*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(-2 + t2 - 4*Power(t2,2)) + 
               t1*(4 - 2*t2 + 8*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s1,2)*(-5 - 5*Power(t1,2) + 3*t2 + 
                  t1*(10 + 9*t2)) + 
               s1*(3 + 7*t2 - 3*Power(t2,2) + Power(t1,2)*(3 + 7*t2) - 
                  t1*(6 + 14*t2 + 9*Power(t2,2)))) + 
            s2*(Power(s1,3)*t1*(-4 + 5*t1 + 3*Power(t1,2)) - 
               Power(-1 + t2,2)*(13 + 5*t2) + 
               Power(t1,4)*(2 - 7*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(3 + 40*t2 - 87*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(t1,3)*(-11 + t2 + 27*Power(t2,2) - 2*Power(t2,3)) + 
               t1*(19 - 55*t2 + 61*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-3*Power(t1,3) - 3*Power(t1,2)*(-4 + t2) + 
                  10*(3 + t2) - t1*(39 + 19*t2)) + 
               s1*(8*Power(-1 + t2,2) + 3*Power(t1,4)*(1 + t2) + 
                  Power(t1,3)*(22 - 52*t2 + 3*Power(t2,2)) - 
                  2*t1*(-6 + 31*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*(-45 + 127*t2 + 19*Power(t2,2))))) + 
         Power(s,4)*(8 + Power(t1,5)*
             (45 + Power(s1,2) - 2*s1*(-6 + t2) - 6*t2) - 48*t2 + 
            60*Power(t2,2) - 10*Power(t2,3) + 
            t1*(-35 - 41*t2 + 109*Power(t2,2) + 35*Power(t2,3) + 
               s1*(24 + 28*t2 - 80*Power(t2,2))) + 
            Power(t1,3)*(388 + Power(s1,3) - 423*t2 + 143*Power(t2,2) - 
               6*Power(t2,3) + Power(s1,2)*(-59 + 15*t2) + 
               s1*(193 - 8*t2 - 23*Power(t2,2))) - 
            Power(t1,4)*(180 + 9*Power(s1,2) + Power(s1,3) - 59*t2 + 
               12*Power(t2,2) + s1*(90 - 11*t2 - 2*Power(t2,2))) + 
            Power(t1,2)*(-475 + 26*Power(s1,3) + 
               Power(s1,2)*(145 - 83*t2) + 769*t2 - 334*Power(t2,2) - 
               7*Power(t2,3) + s1*(-414 + 33*t2 + 113*Power(t2,2))) + 
            Power(s2,4)*(10*Power(s1,3) + 10*Power(t1,3) - 
               2*Power(s1,2)*(1 + 25*t1 + 45*t2) - 
               2*Power(t1,2)*(27 + 85*t2) + 
               t1*(49 + 200*t2 + 70*Power(t2,2)) - 
               2*(6 + 40*t2 + 75*Power(t2,2) + 70*Power(t2,3)) + 
               s1*(21 + 80*Power(t1,2) + 80*t2 + 210*Power(t2,2) + 
                  t1*(-56 + 60*t2))) + 
            Power(s2,2)*(-104 + 6*Power(t1,5) - 
               2*Power(s1,3)*t1*(1 + 8*t1) + 28*t2 + 3*Power(t2,2) - 
               98*Power(t2,3) - 2*Power(t1,4)*(102 + 5*t2) - 
               2*Power(t1,3)*(-331 + 93*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-860 + 556*t2 - 211*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(550 - 272*t2 + 412*Power(t2,2) + 115*Power(t2,3)) + 
               Power(s1,2)*(36 - 30*Power(t1,3) - 29*t2 + 
                  5*Power(t1,2)*(3 + 2*t2) + 3*t1*(-3 + 35*t2)) + 
               s1*(37 - 2*Power(t1,4) - 33*t2 + 79*Power(t2,2) + 
                  11*Power(t1,3)*(11 + 8*t2) + 
                  t1*(3 - 246*t2 - 133*Power(t2,2)) + 
                  Power(t1,2)*(-259 + 5*t2 - 40*Power(t2,2)))) - 
            Power(s2,3)*(65 + 20*Power(t1,4) + 
               Power(s1,3)*(-6 + 19*t1) - 37*t2 + 40*Power(t2,2) + 
               145*Power(t2,3) - Power(t1,3)*(251 + 170*t2) + 
               Power(t1,2)*(610 + 7*t2 + 40*Power(t2,2)) - 
               t1*(403 - 10*t2 + 270*Power(t2,2) + 105*Power(t2,3)) - 
               Power(s1,2)*(3 + 49*Power(t1,2) - 53*t2 + 
                  5*t1*(9 + 13*t2)) + 
               s1*(-30 + 92*Power(t1,3) + 29*t2 - 145*Power(t2,2) + 
                  5*Power(t1,2)*(-2 + 27*t2) + 
                  t1*(87 + 102*t2 + 95*Power(t2,2)))) + 
            s2*(10 + Power(s1,3)*t1*(-12 - 13*t1 + 12*Power(t1,2)) - 
               8*Power(t1,5)*(-5 + t2) - 31*t2 + 53*Power(t2,2) - 
               55*Power(t2,3) + 
               Power(t1,4)*(-246 + 53*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(518 - 409*t2 + 115*Power(t2,2) - 
                  10*Power(t2,3)) - 
               Power(t1,2)*(896 - 972*t2 + 483*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(461 - 487*t2 + 395*Power(t2,2) + 81*Power(t2,3)) + 
               Power(s1,2)*t1*
                (Power(t1,3) + t1*(158 - 91*t2) - 
                  3*Power(t1,2)*(8 + t2) + 2*(-99 + 53*t2)) + 
               s1*(-2 + 8*Power(t1,5) - 24*t2 + 32*Power(t2,2) - 
                  Power(t1,4)*(89 + t2) + 
                  t1*(229 - 78*t2 - 148*Power(t2,2)) - 
                  Power(t1,3)*(-353 + 28*t2 + Power(t2,2)) + 
                  Power(t1,2)*(-502 + 157*t2 + 99*Power(t2,2))))) - 
         s*Power(-1 + s2,2)*(2*Power(-1 + t2,2)*(-5 + 11*t2) + 
            2*Power(t1,5)*(s1 + 2*Power(s1,2) + 3*t2 - 4*s1*t2) - 
            t1*(-1 + t2)*(98 - 141*t2 + 31*Power(t2,2) + 
               s1*(-42 + 46*t2)) + 
            Power(t1,3)*(70 - 11*Power(s1,3) - 245*t2 + 
               236*Power(t2,2) + 6*Power(t2,3) + 
               Power(s1,2)*(58 + 45*t2) + 
               s1*(120 - 194*t2 - 77*Power(t2,2))) - 
            Power(t1,4)*(11*Power(s1,2) + 4*Power(s1,3) + 
               s1*(42 - 71*t2 - 8*Power(t2,2)) + 
               4*(3 - 7*t2 + 12*Power(t2,2))) + 
            Power(t1,2)*(-146 + 14*Power(s1,3) + 408*t2 - 
               332*Power(t2,2) + 8*Power(t2,3) - 
               Power(s1,2)*(77 + 38*t2) + 
               s1*(-38 + 95*t2 + 104*Power(t2,2))) + 
            Power(s2,5)*(10*Power(s1,3) + 
               4*Power(s1,2)*(-2 + t1 - 9*t2) - 
               t2*(5 + Power(t1,2) + 18*t2 + 16*Power(t2,2) - 
                  2*t1*(3 + 7*t2)) - 
               s1*(-3 + Power(t1,2) - 26*t2 - 42*Power(t2,2) + 
                  2*t1*(1 + 9*t2))) + 
            Power(s2,3)*(36 + Power(s1,3)*(9 - 19*t1 + 2*Power(t1,2)) - 
               31*t2 - 119*Power(t2,2) + 12*Power(t2,3) - 
               Power(t1,4)*(4 + t2) - 
               Power(t1,3)*(-43 + 22*t2 + Power(t2,2)) + 
               2*Power(t1,2)*
                (-13 + 36*t2 - 58*Power(t2,2) + 3*Power(t2,3)) - 
               t1*(49 + 18*t2 - 220*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s1,2)*(-91 + 18*Power(t1,3) - 12*t2 + 
                  Power(t1,2)*(-94 + 4*t2) + t1*(151 + 28*t2)) - 
               s1*(28 + 5*Power(t1,4) - 232*t2 + 6*Power(t2,2) + 
                  2*Power(t1,3)*(8 + 7*t2) - 
                  3*t1*(38 - 144*t2 + Power(t2,2)) + 
                  Power(t1,2)*(65 - 246*t2 + 13*Power(t2,2)))) - 
            Power(s2,4)*(25 + Power(s1,3)*(1 + 16*t1) - 34*t2 - 
               4*Power(t1,3)*t2 + 10*Power(t2,2) - 8*Power(t2,3) + 
               Power(t1,2)*(29 - 14*t2 + 23*Power(t2,2)) - 
               t1*(54 - 52*t2 + 55*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s1,2)*(14 + 11*Power(t1,2) - 16*t2 - 
                  47*t1*(1 + t2)) + 
               s1*(6 - 4*Power(t1,3) - 23*t2 + 23*Power(t2,2) - 
                  Power(t1,2)*(6 + 29*t2) + 
                  t1*(4 + 96*t2 + 52*Power(t2,2)))) - 
            s2*(57 + Power(s1,3)*t1*
                (24 - 19*t1 + Power(t1,2) + 4*Power(t1,3)) - 
               12*Power(t1,5)*(-1 + t2) - 110*t2 + 37*Power(t2,2) + 
               16*Power(t2,3) + 
               Power(t1,4)*(-84 + 69*t2 + 40*Power(t2,2)) + 
               Power(t1,2)*(69 - 425*t2 + 530*Power(t2,2) - 
                  6*Power(t2,3)) + 
               Power(t1,3)*(62 + 40*t2 - 271*Power(t2,2) - 
                  4*Power(t2,3)) - 
               2*t1*(58 - 219*t2 + 202*Power(t2,2) + 5*Power(t2,3)) - 
               Power(s1,2)*t1*
                (120 - 22*Power(t1,3) + 4*Power(t1,4) + 70*t2 + 
                  Power(t1,2)*(103 + 34*t2) - t1*(137 + 80*t2)) + 
               s1*(-22 + 48*t2 - 26*Power(t2,2) + 
                  Power(t1,5)*(4 + 8*t2) + 
                  Power(t1,2)*(261 - 522*t2 - 91*Power(t2,2)) + 
                  Power(t1,4)*(41 - 94*t2 - 8*Power(t2,2)) + 
                  Power(t1,3)*(-99 + 338*t2 + 62*Power(t2,2)) + 
                  t1*(-185 + 358*t2 + 81*Power(t2,2)))) + 
            Power(s2,2)*(Power(s1,3)*
                (10 - 17*t1 + 25*Power(t1,2) + 8*Power(t1,3)) - 
               2*Power(t1,5)*(-2 + t2) + 
               Power(t1,4)*(-4 - 6*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(98 + 74*t2 - 439*Power(t2,2) - 
                  20*Power(t2,3)) + 
               Power(t1,3)*(-95 + 15*t2 + 134*Power(t2,2) - 
                  10*Power(t2,3)) - 
               2*(22 - 93*t2 + 81*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(41 - 267*t2 + 415*Power(t2,2) + 14*Power(t2,3)) - 
               Power(s1,2)*(43 + 15*Power(t1,4) + 32*t2 + 
                  Power(t1,3)*(-73 + 15*t2) - t1*(170 + 47*t2) + 
                  Power(t1,2)*(229 + 78*t2)) + 
               s1*(-83 + 2*Power(t1,5) + 135*t2 + 41*Power(t2,2) + 
                  Power(t1,4)*(16 + 11*t2) + 
                  t1*(97 - 416*t2 - 80*Power(t2,2)) + 
                  Power(t1,3)*(101 - 254*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*(-133 + 612*t2 + 106*Power(t2,2))))) + 
         Power(s,2)*(-4 + 128*t1 - 70*s1*t1 - 245*Power(t1,2) - 
            307*s1*Power(t1,2) - 112*Power(s1,2)*Power(t1,2) + 
            36*Power(s1,3)*Power(t1,2) + 223*Power(t1,3) + 
            322*s1*Power(t1,3) + 48*Power(s1,2)*Power(t1,3) - 
            14*Power(s1,3)*Power(t1,3) - 105*Power(t1,4) - 
            45*s1*Power(t1,4) - 20*Power(s1,2)*Power(t1,4) - 
            6*Power(s1,3)*Power(t1,4) + 19*Power(t1,5) + 
            12*s1*Power(t1,5) + 6*Power(s1,2)*Power(t1,5) + 42*t2 - 
            386*t1*t2 + 182*s1*t1*t2 + 902*Power(t1,2)*t2 + 
            241*s1*Power(t1,2)*t2 - 103*Power(s1,2)*Power(t1,2)*t2 - 
            593*Power(t1,3)*t2 - 314*s1*Power(t1,3)*t2 + 
            70*Power(s1,2)*Power(t1,3)*t2 + 85*Power(t1,4)*t2 + 
            98*s1*Power(t1,4)*t2 + 4*Power(t1,5)*t2 - 
            12*s1*Power(t1,5)*t2 - 84*Power(t2,2) + 299*t1*Power(t2,2) - 
            108*s1*t1*Power(t2,2) - 653*Power(t1,2)*Power(t2,2) + 
            223*s1*Power(t1,2)*Power(t2,2) + 
            413*Power(t1,3)*Power(t2,2) - 
            118*s1*Power(t1,3)*Power(t2,2) - 
            72*Power(t1,4)*Power(t2,2) + 12*s1*Power(t1,4)*Power(t2,2) + 
            46*Power(t2,3) - 47*t1*Power(t2,3) + 
            3*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) + 
            Power(s2,6)*(20*Power(s1,3) - Power(t1,3) + 
               Power(t1,2)*(2 - 16*t2) - 
               2*Power(s1,2)*(6 + 5*t1 + 45*t2) + 
               t1*(1 + 44*t2 + 42*Power(t2,2)) - 
               2*(1 + 15*t2 + 33*Power(t2,2) + 28*Power(t2,3)) + 
               s1*(9 + Power(t1,2) + 70*t2 + 126*Power(t2,2) - 
                  8*t1*(1 + 3*t2))) + 
            Power(s2,5)*(-61 - Power(s1,3)*(9 + 34*t1) + 132*t2 + 
               46*Power(t2,2) + 51*Power(t2,3) + 
               Power(t1,3)*(13 + 33*t2) - 
               3*Power(t1,2)*(51 - 26*t2 + 18*Power(t2,2)) + 
               t1*(199 - 224*t2 + 135*Power(t2,2) + 63*Power(t2,3)) + 
               Power(s1,2)*(-14 + 5*Power(t1,2) + 82*t2 + 
                  5*t1*(23 + 20*t2)) + 
               s1*(-43 - 36*t2 - 130*Power(t2,2) + 
                  Power(t1,2)*(-19 + 29*t2) + 
                  t1*(47 - 228*t2 - 123*Power(t2,2)))) + 
            Power(s2,4)*(23 + Power(t1,5) + 
               Power(s1,3)*(4 - 9*t1 + 5*Power(t1,2)) - 78*t2 - 
               190*Power(t2,2) + 22*Power(t2,3) - 
               5*Power(t1,4)*(7 + t2) - 
               5*Power(t1,3)*(-42 + 39*t2 + Power(t2,2)) + 
               Power(t1,2)*(-57 + 296*t2 - 231*Power(t2,2) + 
                  15*Power(t2,3)) - 
               t1*(134 + 32*t2 - 317*Power(t2,2) + 55*Power(t2,3)) + 
               Power(s1,2)*(19*Power(t1,3) + 
                  3*Power(t1,2)*(-68 + 5*t2) + 2*t1*(118 + 7*t2) - 
                  3*(39 + 10*t2)) + 
               s1*(36 - 9*Power(t1,4) - 2*Power(t1,3)*(-12 + t2) + 
                  338*t2 + 10*Power(t2,2) + 
                  Power(t1,2)*(-210 + 489*t2 - 35*Power(t2,2)) + 
                  t1*(155 - 638*t2 + 42*Power(t2,2)))) + 
            Power(s2,2)*(-43 + 
               Power(s1,3)*(14 + 27*t1 - 39*Power(t1,2) + 
                  8*Power(t1,3) - 6*Power(t1,4)) + 362*t2 - 
               268*Power(t2,2) - 4*Power(t2,3) + 
               4*Power(t1,5)*(-4 + 5*t2) - 
               8*Power(t1,4)*(-21 + 14*t2 + 8*Power(t2,2)) + 
               2*Power(t1,3)*
                (-43 - 14*t2 + 164*Power(t2,2) + 6*Power(t2,3)) + 
               t1*(-181 - 178*t2 + 614*Power(t2,2) + 6*Power(t2,3)) + 
               2*Power(t1,2)*
                (107 + 89*t2 - 286*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s1,2)*(19 - 24*Power(t1,4) + 6*Power(t1,5) - 
                  56*t2 - 2*Power(t1,2)*(15 + 16*t2) + 
                  t1*(48 + 34*t2) + Power(t1,3)*(67 + 58*t2)) + 
               s1*(-315 + 192*t2 + 56*Power(t2,2) - 
                  4*Power(t1,5)*(1 + 3*t2) + 
                  Power(t1,3)*(36 - 372*t2 - 98*Power(t2,2)) + 
                  t1*(377 - 632*t2 - 94*Power(t2,2)) + 
                  2*Power(t1,4)*(-31 + 67*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*(-398 + 646*t2 + 68*Power(t2,2)))) + 
            s2*(-45 - Power(s1,3)*t1*
                (48 + 8*t1 - 21*Power(t1,2) + 4*Power(t1,3)) + 132*t2 - 
               40*Power(t2,2) - 41*Power(t2,3) + 
               4*Power(t1,5)*(-9 + 4*t2) - 
               2*Power(t1,4)*(-62 + 53*t2 + 18*Power(t2,2)) + 
               Power(t1,3)*(-243 + 167*t2 + 278*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(58 + 400*t2 - 656*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(92 - 802*t2 + 617*Power(t2,2) + 23*Power(t2,3)) + 
               2*Power(s1,2)*t1*
                (31 - 17*Power(t1,3) + 2*Power(t1,4) + 
                  t1*(3 - 51*t2) + 85*t2 + Power(t1,2)*(38 + 13*t2)) + 
               s1*(18 - 50*t2 + 28*Power(t2,2) - 
                  8*Power(t1,5)*(2 + t2) + 
                  t1*(753 - 716*t2 - 93*Power(t2,2)) + 
                  Power(t1,3)*(219 - 448*t2 - 54*Power(t2,2)) + 
                  2*Power(t1,4)*(-13 + 53*t2 + 4*Power(t2,2)) + 
                  Power(t1,2)*(-647 + 773*t2 + 70*Power(t2,2)))) + 
            Power(s2,3)*(Power(s1,3)*
                (-13 + 32*t1 + 6*Power(t1,2) + 17*Power(t1,3)) - 
               8*Power(t1,5)*(-2 + t2) + 
               6*Power(t1,4)*(-4 + 7*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(149 + 146*t2 - 714*Power(t2,2) - 
                  48*Power(t2,3)) + 
               Power(t1,3)*(-308 + 40*t2 + 266*Power(t2,2) - 
                  20*Power(t2,3)) + 
               2*(18 + 56*t2 - 99*Power(t2,2) - 9*Power(t2,3)) + 
               t1*(103 - 438*t2 + 536*Power(t2,2) + 10*Power(t2,3)) - 
               Power(s1,2)*(4 + 18*Power(t1,4) + t1*(19 - 66*t2) + 
                  34*t2 + 2*Power(t1,3)*(-55 + 13*t2) + 
                  Power(t1,2)*(209 + 162*t2)) + 
               s1*(-137 + 8*Power(t1,5) + 222*t2 + 70*Power(t2,2) + 
                  2*Power(t1,4)*(-1 + 7*t2) + 
                  Power(t1,3)*(263 - 464*t2 + 14*Power(t2,2)) - 
                  2*t1*(-93 + 204*t2 + 68*Power(t2,2)) + 
                  2*Power(t1,2)*(-74 + 415*t2 + 125*Power(t2,2))))) + 
         Power(s,3)*(-8 - 4*Power(t1,5)*
             (12 + Power(s1,2) + s1*(5 - 2*t2) - t2) + 12*t2 + 
            30*Power(t2,2) - 38*Power(t2,3) + 
            Power(t1,2)*(377 - 44*Power(s1,3) - 1118*t2 + 
               651*Power(t2,2) + 8*Power(t2,3) + 
               4*Power(s1,2)*(-1 + 33*t2) + 
               s1*(592 - 235*t2 - 232*Power(t2,2))) + 
            Power(t1,4)*(139 + 20*Power(s1,2) + 4*Power(s1,3) - 
               104*t2 + 48*Power(t2,2) + s1*(68 - 58*t2 - 8*Power(t2,2))\
) + t1*(-44 + 276*t2 - 256*Power(t2,2) + 15*Power(t2,3) + 
               2*s1*(16 - 81*t2 + 65*Power(t2,2))) + 
            Power(t1,3)*(-404 + 6*Power(s1,3) + 
               Power(s1,2)*(28 - 50*t2) + 717*t2 - 352*Power(t2,2) + 
               4*Power(t2,3) + s1*(-356 + 204*t2 + 82*Power(t2,2))) + 
            Power(s2,5)*(-20*Power(s1,3) + Power(t1,3) + 
               8*Power(s1,2)*(1 + 5*t1 + 15*t2) + 
               Power(t1,2)*(8 + 75*t2) - 
               t1*(17 + 130*t2 + 70*Power(t2,2)) + 
               2*(4 + 35*t2 + 65*Power(t2,2) + 56*Power(t2,3)) - 
               s1*(26*Power(t1,2) + 2*t1*(-12 + 5*t2) + 
                  5*(3 + 20*t2 + 42*Power(t2,2)))) + 
            Power(s2,3)*(108 - 4*Power(t1,5) + 
               Power(s1,3)*(-1 + 13*t1 + 7*Power(t1,2)) - 58*t2 + 
               130*Power(t2,2) - 8*Power(t2,3) + 
               10*Power(t1,4)*(12 + t2) + 
               2*Power(t1,3)*(-232 + 166*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(425 - 650*t2 + 324*Power(t2,2) - 
                  20*Power(t2,3)) - 
               2*t1*(133 - 206*t2 + 254*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s1,2)*(33 + 7*Power(t1,3) + 
                  Power(t1,2)*(170 - 20*t2) + 32*t2 - t1*(229 + 128*t2)\
) + s1*(2*Power(t1,4) - Power(t1,3)*(127 + 52*t2) - 
                  2*(-7 + 80*t2 + 4*Power(t2,2)) + 
                  Power(t1,2)*(376 - 428*t2 + 50*Power(t2,2)) + 
                  2*t1*(-103 + 346*t2 + 61*Power(t2,2)))) + 
            Power(s2,4)*(79 + 5*Power(t1,4) + 
               9*Power(s1,3)*(-1 + 4*t1) - 128*t2 - 10*Power(t2,2) + 
               30*Power(t2,3) - 5*Power(t1,3)*(19 + 21*t2) + 
               Power(t1,2)*(400 - 122*t2 + 65*Power(t2,2)) - 
               t1*(364 - 250*t2 + 270*Power(t2,2) + 105*Power(t2,3)) + 
               Power(s1,2)*(-36*Power(t1,2) + 4*(4 + 3*t2) - 
                  5*t1*(25 + 22*t2)) + 
               s1*(43*Power(t1,3) + Power(t1,2)*(59 + 45*t2) - 
                  10*(-3 - 3*t2 + Power(t2,2)) + 
                  t1*(-53 + 278*t2 + 150*Power(t2,2)))) + 
            s2*(-5 - 16*Power(t1,5) + 
               Power(s1,3)*t1*
                (40 + 8*t1 - 27*Power(t1,2) + 4*Power(t1,3)) - 60*t2 + 
               72*Power(t2,2) + 
               Power(t1,4)*(-24 - 34*t2 + 40*Power(t2,2)) + 
               2*Power(t1,3)*
                (-72 + 254*t2 - 207*Power(t2,2) + 4*Power(t2,3)) - 
               2*t1*(216 - 571*t2 + 377*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(478 - 1297*t2 + 1032*Power(t2,2) + 
                  12*Power(t2,3)) - 
               Power(s1,2)*t1*
                (-16*Power(t1,3) + 4*Power(t1,4) + t1*(89 - 188*t2) + 
                  8*Power(t1,2)*(7 + 4*t2) + 24*(-7 + 8*t2)) + 
               s1*(-2 + 8*Power(t1,5)*(-2 + t2) + 28*t2 - 
                  18*Power(t2,2) + 
                  Power(t1,2)*(1003 - 788*t2 - 246*Power(t2,2)) - 
                  2*Power(t1,4)*(-61 + 40*t2 + 4*Power(t2,2)) + 
                  Power(t1,3)*(-510 + 460*t2 + 68*Power(t2,2)) + 
                  t1*(-815 + 358*t2 + 202*Power(t2,2)))) - 
            Power(s2,2)*(-142 + 
               Power(s1,3)*(6 + 13*t1 - 25*Power(t1,2) + 
                  23*Power(t1,3)) - 12*Power(t1,5)*(-3 + t2) + 220*t2 - 
               224*Power(t2,2) + 
               8*Power(t1,4)*(-15 + 10*t2 + Power(t2,2)) + 
               Power(t1,3)*(18 - 372*t2 + 252*Power(t2,2) - 
                  20*Power(t2,3)) - 
               4*Power(t1,2)*
                (95 - 250*t2 + 230*Power(t2,2) + 8*Power(t2,3)) + 
               t1*(401 - 914*t2 + 782*Power(t2,2) + 22*Power(t2,3)) - 
               Power(s1,2)*(-77 + 4*Power(t1,4) + t1*(54 - 178*t2) + 
                  44*t2 + Power(t1,3)*(-71 + 18*t2) + 
                  Power(t1,2)*(131 + 164*t2)) + 
               s1*(-153 + 12*Power(t1,5) + 6*Power(t1,4)*(-12 + t2) + 
                  70*t2 + 42*Power(t2,2) + 
                  t1*(642 - 620*t2 - 212*Power(t2,2)) + 
                  Power(t1,3)*(446 - 348*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*(-656 + 930*t2 + 228*Power(t2,2))))) + 
         Power(s,5)*(-2 - 2*Power(t1,5)*(8 + s1 - t2) + 30*t2 - 
            78*Power(t2,2) + 46*Power(t2,3) + 
            Power(t1,4)*(152 + Power(s1,2) - 12*t2 + s1*(42 + t2)) + 
            Power(t1,2)*(327 - 6*Power(s1,3) - 220*t2 + 74*Power(t2,2) + 
               Power(s1,2)*(-131 + 22*t2) + 
               s1*(40 + 95*t2 - 16*Power(t2,2))) - 
            Power(t1,3)*(286 + Power(s1,3) + Power(s1,2)*(-30 + t2) - 
               75*t2 + 20*Power(t2,2) - 2*Power(t2,3) + 
               s1*(63 + 46*t2 - Power(t2,2))) + 
            t1*(30 - 61*t2 - 20*Power(t2,2) - 43*Power(t2,3) + 
               2*s1*(-13 + 26*t2 + 9*Power(t2,2))) + 
            Power(s2,3)*(8 - 2*Power(s1,3) - 30*Power(t1,3) + 45*t2 + 
               102*Power(t2,2) + 112*Power(t2,3) + 
               4*Power(s1,2)*(7*t1 + 9*t2) + Power(t1,2)*(96 + 215*t2) - 
               t1*(55 + 170*t2 + 42*Power(t2,2)) - 
               s1*(18 - 74*t1 + 115*Power(t1,2) + 34*t2 + 66*t1*t2 + 
                  126*Power(t2,2))) + 
            Power(s2,2)*(24 + 4*Power(s1,3)*t1 + 30*Power(t1,4) - 4*t2 + 
               26*Power(t2,2) + 156*Power(t2,3) + 
               Power(s1,2)*(-6 - 41*Power(t1,2) + t1*(8 - 19*t2) + 
                  20*t2) - Power(t1,3)*(313 + 150*t2) + 
               Power(t1,2)*(521 + 192*t2 + 9*Power(t2,2)) - 
               t1*(240 + 148*t2 + 135*Power(t2,2) + 63*Power(t2,3)) + 
               s1*(-32 + 78*Power(t1,3) + 21*t2 - 113*Power(t2,2) + 
                  Power(t1,2)*(-137 + 133*t2) + 
                  t1*(181 - 24*t2 + 24*Power(t2,2)))) + 
            s2*(-4*Power(t1,5) + Power(t1,4)*(168 + 9*s1 + 5*t2) + 
               t2*(6 + s1*(20 - 46*t2) - 71*t2 + 116*Power(t2,2)) + 
               Power(t1,3)*(-523 + 23*Power(s1,2) - 18*t2 + 
                  5*Power(t2,2) - s1*(41 + 58*t2)) + 
               Power(t1,2)*(631 - 102*Power(s1,2) + 7*Power(s1,3) - 
                  107*t2 + 54*Power(t2,2) - 6*Power(t2,3) + 
                  s1*(-48 + 168*t2 + 17*Power(t2,2))) + 
               t1*(Power(s1,2)*(64 - 26*t2) + 
                  s1*(128 + 26*t2 + 33*Power(t2,2)) - 
                  16*(14 + 3*t2 + 8*Power(t2,2) + 5*Power(t2,3))))))*R2q(s))/
     (s*(-1 + s1)*(-1 + s2)*Power(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + 
         Power(s2,2),2)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*
       (-1 + t2)) + (8*(Power(-1 + s2,6)*(s2 - t1)*(-1 + t1)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         Power(s,12)*(2*Power(t1,3) + Power(t1,2)*t2 + Power(t2,3)) + 
         Power(s,11)*(8*Power(t1,4) + 
            (3 + s2*(-3 + 3*s1 - 11*t2) - 11*t2)*Power(t2,2) + 
            Power(t1,3)*(-16 - 16*s2 + t2) + 
            Power(t1,2)*(4 + 2*s2 + s1*(-3 + 4*s2 - t2) - 5*t2 - 
               10*s2*t2) + t1*t2*
             (3 - s2 + s2*t2 + 3*Power(t2,2) + s1*(-3 + 3*s2 + t2))) + 
         Power(s,10)*(10*Power(t1,5) - 
            Power(t1,4)*(57 + 4*s1 + 56*s2 + t2) + 
            Power(t1,3)*(59 - 2*Power(s1,2) + 56*Power(s2,2) + 
               s2*(101 - 11*t2) + s1*(-7 + 10*s2 - t2) + 5*t2) + 
            Power(t1,2)*(-34 + Power(s1,2)*(1 - 5*s2) + 9*t2 + 
               3*Power(t2,2) + 3*Power(t2,3) + 
               15*Power(s2,2)*(-1 + 3*t2) + 
               s2*(-39 + 23*t2 + 3*Power(t2,2)) + 
               s1*(25 - 31*Power(s2,2) - 5*t2 + 20*s2*t2 + 
                  3*Power(t2,2))) + 
            t1*(3 + Power(s1,2)*s2*(-3 + 3*s2 - t2) - 30*t2 + 
               4*Power(t2,2) - 31*Power(t2,3) + 
               Power(s2,2)*(1 + 4*t2 - 10*Power(t2,2)) - 
               s2*(-2 + 3*t2 + 23*Power(t2,2) + 30*Power(t2,3)) + 
               s1*(-3 + Power(s2,2)*(2 - 25*t2) + 29*t2 - 
                  6*Power(t2,2) + s2*(3 + 2*t2))) + 
            t2*(3 - 30*t2 + 52*Power(t2,2) + 
               s2*(-3 + s1*(3 - 26*t2) + 95*Power(t2,2)) + 
               Power(s2,2)*(1 + 3*Power(s1,2) + 28*t2 + 55*Power(t2,2) - 
                  3*s1*(1 + 10*t2)))) + 
         Power(s,9)*(1 + 4*Power(t1,6) - 27*t2 + 126*Power(t2,2) - 
            136*Power(t2,3) - Power(t1,5)*(61 + 8*s1 + t2) + 
            Power(t1,4)*(-4*Power(s1,2) + s1*(29 + t2) + 
               4*(37 + 6*t2)) + 
            Power(t1,2)*(112 + 2*Power(s1,2)*(-8 + t2) - 28*t2 - 
               31*Power(t2,2) - 28*Power(t2,3) + 
               s1*(-72 + 89*t2 - 18*Power(t2,2))) + 
            Power(t1,3)*(-120 + 19*Power(s1,2) - 57*t2 + 
               5*Power(t2,2) + Power(t2,3) + 
               s1*(49 - 15*t2 + 3*Power(t2,2))) + 
            t1*(-27 + 122*t2 - 30*Power(t2,2) + 134*Power(t2,3) + 
               s1*(25 - 109*t2 + 4*Power(t2,2))) + 
            Power(s2,3)*(Power(s1,3) - 112*Power(t1,3) + 
               Power(t1,2)*(49 - 120*t2) - 
               Power(s1,2)*(23*t1 + 27*t2) + 
               t1*(-7 + 4*t2 + 45*Power(t2,2)) - 
               t2*(10 + 117*t2 + 165*Power(t2,2)) + 
               s1*(1 + 104*Power(t1,2) + 26*t2 + 135*Power(t2,2) + 
                  2*t1*(-7 + 45*t2))) + 
            Power(s2,2)*(1 + 168*Power(t1,4) + 
               (23 - 3*s1 - 22*Power(s1,2))*t2 + 
               (-85 + 196*s1)*Power(t2,2) - 360*Power(t2,3) + 
               Power(t1,3)*(-262 - 67*s1 + 52*t2) + 
               Power(t1,2)*(135 + 42*Power(s1,2) + s1*(48 - 117*t2) - 
                  29*t2 - 27*Power(t2,2)) + 
               t1*(-17 - 2*Power(s1,3) - 36*t2 + 17*Power(s1,2)*t2 + 
                  185*Power(t2,2) + 135*Power(t2,3) + 
                  s1*(-22 + 34*t2 - 45*Power(t2,2)))) + 
            s2*(-60*Power(t1,5) + 4*Power(t1,4)*(69 + 7*s1 + t2) - 
               Power(t1,3)*(288 + 3*Power(s1,2) + s1*(5 - 27*t2) + 
                  50*t2 - 3*Power(t2,2)) + 
               t1*(-29 + 57*t2 + 134*Power(t2,2) + 238*Power(t2,3) + 
                  2*Power(s1,2)*(11 + 8*t2) - s1*t2*(139 + 36*t2)) + 
               2*t2*(2*(25 - 84*t2)*t2 + s1*(-11 + 47*t2)) + 
               Power(t1,2)*(195 + Power(s1,3) + 
                  Power(s1,2)*(21 - 4*t2) + 80*t2 - 74*Power(t2,2) - 
                  27*Power(t2,3) - s1*(116 + 20*t2 + 15*Power(t2,2))))) + 
         s*Power(-1 + s2,4)*(Power(s2,7)*Power(s1 - t2,2)*
             (-1 + s1 + t1 - t2) + 
            Power(s2,5)*(-1 + t1)*(s1 - t2)*
             (-1 + Power(s1,2)*(-5 + 2*t1) - 11*t2 - 7*Power(t2,2) - 
               3*Power(t1,2)*(1 + t2) + t1*(4 + 8*t2 + 3*Power(t2,2)) + 
               s1*(5 + 6*Power(t1,2) + 13*t2 - t1*(5 + 6*t2))) - 
            Power(s2,6)*(s1 - t2)*
             (-1 + 3*Power(s1,2)*t1 - 2*t2 - Power(t1,2)*(1 + 3*t2) + 
               t1*(2 + 5*t2 + 3*Power(t2,2)) + 
               s1*(3 + 4*Power(t1,2) - t1*(7 + 6*t2))) + 
            (-1 + t1)*(2*Power(-1 + s1,2)*Power(t1,5) - 
               4*Power(-1 + t2,3) + 
               t1*Power(-1 + t2,2)*(-31 + 8*s1 + 16*t2) - 
               (-1 + s1)*Power(t1,4)*
                (4 + 3*Power(s1,2) - 7*t2 + 3*s1*(1 + t2)) - 
               Power(t1,2)*(-1 + t2)*
                (58 + 2*Power(s1,2) - 55*t2 + 3*Power(t2,2) + 
                  s1*(-39 + 37*t2)) + 
               Power(t1,3)*(-37 - 2*Power(s1,3) + 54*t2 - 
                  12*Power(t2,2) + 3*Power(s1,2)*(-3 + 8*t2) + 
                  6*s1*(6 - 10*t2 + Power(t2,2)))) + 
            Power(s2,4)*(1 + 
               Power(s1,3)*(6 - 27*t1 + 29*Power(t1,2) + 
                  2*Power(t1,3)) - 13*t2 + Power(t2,2) + 
               20*Power(t2,3) - Power(t1,4)*t2*(3 + t2) + 
               t1*(-3 + 39*t2 + 41*Power(t2,2) - 22*Power(t2,3)) + 
               Power(t1,3)*(-1 + 19*t2 + 6*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(-3 + 42*t2 + 47*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(s1,2)*(3*Power(t1,3) - 4*Power(t1,4) + 
                  5*(-5 + 2*t2) + t1*(84 + 31*t2) - 
                  Power(t1,2)*(58 + 71*t2)) + 
               s1*(5 + 31*t2 - 36*Power(t2,2) + 
                  Power(t1,4)*(3 + 5*t2) - 
                  Power(t1,3)*(11 + 16*t2 + 3*Power(t2,2)) + 
                  3*Power(t1,2)*(6 + 42*t2 + 17*Power(t2,2)) + 
                  t1*(-15 - 146*t2 + 18*Power(t2,2)))) + 
            Power(s2,3)*(-(Power(s1,3)*
                  (2 + 19*t1 - 54*Power(t1,2) + 45*Power(t1,3) + 
                    3*Power(t1,4))) + Power(t1,5)*t2 + 
               t2*(4 + 8*t2 - 7*Power(t2,2)) + 
               Power(t1,4)*(-5 - 15*t2 + Power(t2,2)) + 
               Power(t1,3)*(15 + 56*t2 + 38*Power(t2,2)) + 
               t1*(5 + 21*t2 - 57*Power(t2,2) - 4*Power(t2,3)) + 
               Power(t1,2)*(-15 - 67*t2 + 10*Power(t2,2) + 
                  26*Power(t2,3)) + 
               Power(s1,2)*(6 + Power(t1,5) + 9*t2 - 
                  9*t1*(-5 + 2*t2) + Power(t1,4)*(2 + 3*t2) - 
                  Power(t1,2)*(185 + 17*t2) + Power(t1,3)*(131 + 68*t2)\
) - s1*(-13 + 36*t2 - 5*Power(t2,2) + Power(t1,5)*(1 + t2) - 
                  Power(t1,4)*(13 + 4*t2) + 
                  t1*(70 - 71*t2 - 26*Power(t2,2)) + 
                  2*Power(t1,2)*(-56 - 65*t2 + 24*Power(t2,2)) + 
                  Power(t1,3)*(67 + 168*t2 + 28*Power(t2,2)))) - 
            s2*(2*Power(t1,6) + 
               Power(s1,3)*Power(t1,2)*
                (6 + 9*t1 - 21*Power(t1,2) + 8*Power(t1,3)) - 
               5*Power(t1,5)*t2 + Power(-1 + t2,2)*(14 + t2) + 
               Power(t1,4)*(-17 + 23*t2 - 23*Power(t2,2)) + 
               t1*(-37 + 28*t2 + 43*Power(t2,2) - 34*Power(t2,3)) + 
               Power(t1,3)*(17 - 57*t2 + 81*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,2)*(21 + 38*t2 - 113*Power(t2,2) + 
                  39*Power(t2,3)) + 
               Power(s1,2)*t1*
                (2*Power(t1,5) + t1*(16 - 61*t2) + 4*(-1 + t2) + 
                  Power(t1,3)*(28 + 3*t2) + 
                  13*Power(t1,2)*(-1 + 4*t2) - Power(t1,4)*(29 + 4*t2)) \
+ s1*(-4*Power(t1,6) - 8*Power(-1 + t2,2) + 3*Power(t1,5)*(7 + 3*t2) + 
                  Power(t1,3)*(24 - 53*t2 - 52*Power(t2,2)) + 
                  9*Power(t1,2)*(-7 + 12*t2 + Power(t2,2)) + 
                  Power(t1,4)*(-19 + 14*t2 + 12*Power(t2,2)) + 
                  t1*(49 - 94*t2 + 45*Power(t2,2)))) + 
            Power(s2,2)*(7 + Power(s1,3)*t1*
                (6 + 21*t1 - 50*Power(t1,2) + 31*Power(t1,3) + 
                  Power(t1,4)) - 28*t2 + 29*Power(t2,2) - 
               8*Power(t2,3) + Power(t1,5)*(8 + t2) - 
               Power(t1,4)*(21 + 20*t2 + 11*Power(t2,2)) + 
               Power(t1,3)*(8 + 32*t2 - 36*Power(t2,2) - 
                  6*Power(t2,3)) - 
               t1*(24 - 51*t2 + 41*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(22 - 36*t2 + 59*Power(t2,2) + 
                  6*Power(t2,3)) - 
               Power(s1,2)*(2 + t1 + Power(t1,2)*(24 - 33*t2) + 
                  Power(t1,5)*(-2 + t2) - 2*t2 + 44*t1*t2 - 
                  2*Power(t1,3)*(68 + 5*t2) + 3*Power(t1,4)*(37 + 9*t2)) \
+ s1*(2 - 11*Power(t1,5) - 2*t2 + 
                  Power(t1,2)*(52 - 85*t2 - 48*Power(t2,2)) + 
                  Power(t1,4)*(78 + 75*t2 + 6*Power(t2,2)) + 
                  2*Power(t1,3)*(-60 + 2*t2 + 11*Power(t2,2)) + 
                  t1*(-1 + 8*t2 + 47*Power(t2,2))))) - 
         Power(s,2)*Power(-1 + s2,2)*
          (-20 + 190*t1 - 62*s1*t1 - 487*Power(t1,2) + 
            262*s1*Power(t1,2) - 18*Power(s1,2)*Power(t1,2) + 
            552*Power(t1,3) - 348*s1*Power(t1,3) - 
            2*Power(s1,2)*Power(t1,3) + 16*Power(s1,3)*Power(t1,3) - 
            295*Power(t1,4) + 125*s1*Power(t1,4) + 
            57*Power(s1,2)*Power(t1,4) - 14*Power(s1,3)*Power(t1,4) + 
            54*Power(t1,5) + 69*s1*Power(t1,5) - 
            51*Power(s1,2)*Power(t1,5) + 4*Power(t1,6) - 
            50*s1*Power(t1,6) + 10*Power(s1,2)*Power(t1,6) + 
            2*Power(t1,7) + 4*s1*Power(t1,7) + 72*t2 - 500*t1*t2 + 
            128*s1*t1*t2 + 970*Power(t1,2)*t2 - 469*s1*Power(t1,2)*t2 + 
            16*Power(s1,2)*Power(t1,2)*t2 - 753*Power(t1,3)*t2 + 
            574*s1*Power(t1,3)*t2 - 105*Power(s1,2)*Power(t1,3)*t2 + 
            225*Power(t1,4)*t2 - 266*s1*Power(t1,4)*t2 + 
            97*Power(s1,2)*Power(t1,4)*t2 - 12*Power(t1,5)*t2 + 
            39*s1*Power(t1,5)*t2 - 15*Power(s1,2)*Power(t1,5)*t2 - 
            2*Power(t1,6)*t2 + 2*s1*Power(t1,6)*t2 - 84*Power(t2,2) + 
            421*t1*Power(t2,2) - 66*s1*t1*Power(t2,2) - 
            614*Power(t1,2)*Power(t2,2) + 
            234*s1*Power(t1,2)*Power(t2,2) + 
            331*Power(t1,3)*Power(t2,2) - 
            191*s1*Power(t1,3)*Power(t2,2) - 
            58*Power(t1,4)*Power(t2,2) + 31*s1*Power(t1,4)*Power(t2,2) + 
            32*Power(t2,3) - 111*t1*Power(t2,3) + 
            92*Power(t1,2)*Power(t2,3) - 16*Power(t1,3)*Power(t2,3) + 
            Power(s2,8)*(8*Power(s1,3) + 
               Power(s1,2)*(-7 + 5*t1 - 27*t2) + 
               s1*(1 - Power(t1,2) + 19*t2 - 15*t1*t2 + 
                  30*Power(t2,2)) - 
               t2*(3 + Power(t1,2) + 12*t2 + 11*Power(t2,2) - 
                  2*t1*(2 + 5*t2))) + 
            Power(s2,7)*(-3 - Power(s1,3)*(1 + 23*t1) - 
               11*Power(t2,2) + 3*Power(t2,3) + 
               Power(t1,3)*(4 + 3*t2) - 
               Power(t1,2)*(11 + 17*t2 + 27*Power(t2,2)) + 
               t1*(10 + 14*t2 + 51*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s1,2)*(-17 - 21*Power(t1,2) + 8*t2 + 
                  t1*(51 + 73*t2)) + 
               s1*(1 + 3*Power(t1,3) + 24*t2 - 10*Power(t2,2) + 
                  Power(t1,2)*(6 + 44*t2) - 
                  2*t1*(5 + 47*t2 + 40*Power(t2,2)))) + 
            Power(s2,6)*(10 + 
               4*Power(s1,3)*(3 - 6*t1 + 4*Power(t1,2)) - 25*t2 - 
               76*Power(t2,2) - 18*Power(t2,3) - 
               Power(t1,4)*(14 + 3*t2) + 
               Power(t1,3)*(29 + 33*t2 + 24*Power(t2,2)) - 
               Power(t1,2)*(6 + 51*t2 + 100*Power(t2,2) + 
                  27*Power(t2,3)) + 
               t1*(-19 + 46*t2 + 138*Power(t2,2) + 39*Power(t2,3)) + 
               Power(s1,2)*(-46 + 33*Power(t1,3) - 48*t2 - 
                  2*Power(t1,2)*(41 + 30*t2) + t1*(81 + 94*t2)) + 
               s1*(3 - 3*Power(t1,4) + 116*t2 + 52*Power(t2,2) - 
                  Power(t1,3)*(12 + 43*t2) + 
                  t1*(7 - 193*t2 - 105*Power(t2,2)) + 
                  Power(t1,2)*(5 + 148*t2 + 69*Power(t2,2)))) + 
            Power(s2,2)*(1 - 2*Power(t1,7) - 
               2*Power(s1,3)*t1*
                (-16 - 6*t1 + 21*Power(t1,2) - 12*Power(t1,3) + 
                  Power(t1,4)) - 84*t2 + 48*Power(t2,2) - 
               4*Power(t2,3) + 8*Power(t1,5)*(3 + 2*t2) + 
               2*Power(t1,6)*(-7 + 3*t2) + 
               Power(t1,4)*(15 + 78*t2 + 68*Power(t2,2)) + 
               t1*(27 + 130*t2 - 84*Power(t2,2) + 13*Power(t2,3)) + 
               Power(t1,3)*(-7 - 81*t2 - 254*Power(t2,2) + 
                  16*Power(t2,3)) - 
               Power(t1,2)*(44 + 65*t2 - 284*Power(t2,2) + 
                  63*Power(t2,3)) + 
               Power(s1,2)*(-10 + 6*Power(t1,6) + 
                  Power(t1,3)*(257 - 108*t2) + 8*t2 - 
                  4*Power(t1,4)*(49 + 5*t2) + 
                  Power(t1,5)*(67 + 11*t2) + 
                  4*Power(t1,2)*(9 + 34*t2) - t1*(98 + 113*t2)) + 
               s1*(-6 + 4*Power(t1,7) + Power(t1,6)*(14 - 6*t2) + 
                  65*t2 - 32*Power(t2,2) - 5*Power(t1,5)*(47 + 7*t2) + 
                  Power(t1,4)*(268 + 39*t2 - 22*Power(t2,2)) + 
                  Power(t1,2)*(315 - 682*t2 - 15*Power(t2,2)) + 
                  t1*(-128 + 196*t2 + 75*Power(t2,2)) + 
                  Power(t1,3)*(-232 + 299*t2 + 94*Power(t2,2)))) + 
            Power(s2,3)*(23 + 2*Power(t1,6) + 2*Power(t1,7) + 
               Power(s1,3)*(-8 + 2*t1 - 6*Power(t1,2) - 
                  32*Power(t1,3) + 51*Power(t1,4) + 5*Power(t1,5)) + 
               Power(t1,5)*(51 - 2*t2) - 75*t2 + 83*Power(t2,2) - 
               17*Power(t2,3) - 
               Power(t1,4)*(114 + 155*t2 + 46*Power(t2,2)) + 
               Power(t1,3)*(42 + 123*t2 - 176*Power(t2,2) - 
                  32*Power(t2,3)) - 
               t1*(61 - 210*t2 + 311*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(55 - 101*t2 + 297*Power(t2,2) + 
                  46*Power(t2,3)) + 
               Power(s1,2)*(t1*(9 - 102*t2) - Power(t1,5)*(4 + t2) + 
                  4*(5 + 8*t2) - 13*Power(t1,4)*(27 + 8*t2) + 
                  Power(t1,2)*(-127 + 60*t2) + 
                  Power(t1,3)*(300 + 82*t2)) + 
               s1*(75 - 8*Power(t1,6) + 5*Power(t1,5)*(-4 + t2) - 
                  139*t2 + 10*Power(t2,2) + 
                  Power(t1,4)*(510 + 335*t2 + 28*Power(t2,2)) - 
                  4*Power(t1,2)*(-35 + 2*t2 + 39*Power(t2,2)) + 
                  Power(t1,3)*(-611 - 132*t2 + 48*Power(t2,2)) + 
                  t1*(-86 + 245*t2 + 100*Power(t2,2)))) + 
            Power(s2,4)*(-9 - 10*Power(t1,6) - 
               2*Power(s1,3)*
                (5 + t1 - 40*Power(t1,2) + 61*Power(t1,3) + 
                  8*Power(t1,4)) + 28*t2 + 28*Power(t2,2) - 
               15*Power(t2,3) + 4*Power(t1,5)*(4 + t2) - 
               2*Power(t1,4)*(26 + 32*t2 + Power(t2,2)) + 
               Power(t1,3)*(118 + 313*t2 + 195*Power(t2,2)) - 
               t1*(-40 + 48*t2 + 141*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(-103 - 233*t2 + 70*Power(t2,2) + 
                  110*Power(t2,3)) + 
               Power(s1,2)*(6*Power(t1,5) + 7*(7 + t2) + 
                  Power(t1,4)*(-11 + 7*t2) - 
                  4*Power(t1,2)*(147 + 17*t2) + t1*(58 + 23*t2) + 
                  3*Power(t1,3)*(212 + 87*t2)) + 
               s1*(-24 + 4*Power(t1,5) - 72*t2 + 14*Power(t2,2) + 
                  Power(t1,2)*(707 + 423*t2 - 168*Power(t2,2)) + 
                  Power(t1,4)*(32 + 11*t2 - Power(t2,2)) + 
                  t1*(-203 + 128*t2 + 8*Power(t2,2)) - 
                  Power(t1,3)*(516 + 790*t2 + 103*Power(t2,2)))) - 
            s2*(76 + 2*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (40 - 10*t1 - 35*Power(t1,2) + 19*Power(t1,3)) - 
               125*t2 + 13*Power(t2,2) + 36*Power(t2,3) + 
               2*Power(t1,6)*(-9 + 2*t2) + Power(t1,5)*(83 + 7*t2) + 
               Power(t1,4)*(-94 + 57*t2 - 45*Power(t2,2)) + 
               t1*(-207 + 82*t2 + 205*Power(t2,2) - 158*Power(t2,3)) - 
               2*Power(t1,3)*
                (27 + 126*t2 - 89*Power(t2,2) + 12*Power(t2,3)) + 
               Power(t1,2)*(212 + 227*t2 - 350*Power(t2,2) + 
                  125*Power(t2,3)) + 
               Power(s1,2)*t1*
                (16*Power(t1,5) - 4*Power(t1,3)*(-9 + 5*t2) - 
                  Power(t1,4)*(94 + 5*t2) + 4*(-7 + 6*t2) - 
                  2*t1*(40 + 93*t2) + Power(t1,2)*(151 + 138*t2)) + 
               s1*(-42 + 8*Power(t1,7) + 88*t2 - 46*Power(t2,2) - 
                  4*Power(t1,6)*(11 + t2) + Power(t1,5)*(-5 + 9*t2) - 
                  5*Power(t1,3)*(-6 + 35*t2 + 34*Power(t2,2)) + 
                  Power(t1,4)*(58 + 133*t2 + 36*Power(t2,2)) + 
                  2*t1*(73 - 92*t2 + 46*Power(t2,2)) + 
                  Power(t1,2)*(-151 + 131*t2 + 144*Power(t2,2)))) + 
            Power(s2,5)*(-6 + 
               Power(s1,3)*(15 - 65*t1 + 98*Power(t1,2) + 
                  10*Power(t1,3)) - 38*t2 + 37*Power(t2,2) + 
               66*Power(t2,3) + Power(t1,5)*(18 + t2) - 
               Power(t1,4)*(34 + 24*t2 + 7*Power(t2,2)) + 
               t1*(6 + 226*t2 + 121*Power(t2,2) - 118*Power(t2,3)) + 
               2*Power(t1,3)*
                (4 + 55*t2 + 29*Power(t2,2) + 4*Power(t2,3)) - 
               Power(t1,2)*(-8 + 275*t2 + 260*Power(t2,2) + 
                  33*Power(t2,3)) + 
               Power(s1,2)*(-101 - 23*Power(t1,4) + 20*t2 + 
                  Power(t1,3)*(47 + 8*t2) - 
                  10*Power(t1,2)*(40 + 27*t2) + t1*(426 + 49*t2)) + 
               s1*(84 + Power(t1,5) + 75*t2 - 110*Power(t2,2) + 
                  2*Power(t1,4)*(3 + 7*t2) - 
                  Power(t1,3)*(14 + 83*t2 + 18*Power(t2,2)) + 
                  5*Power(t1,2)*(35 + 135*t2 + 36*Power(t2,2)) + 
                  t1*(-252 - 579*t2 + 160*Power(t2,2))))) + 
         Power(s,8)*(-8 - 4*(5 + s1)*Power(t1,6) + 99*t2 - 
            282*Power(t2,2) + 206*Power(t2,3) + 
            Power(t1,5)*(113 - 2*Power(s1,2) + 18*t2 + s1*(55 + t2)) + 
            Power(t1,4)*(-41 + 24*Power(s1,2) - 121*t2 + 
               2*Power(t2,2) + s1*(-85 - 21*t2 + Power(t2,2))) + 
            Power(t1,2)*(-138 + Power(s1,2)*(87 - 16*t2) + 100*t2 + 
               155*Power(t2,2) + 106*Power(t2,3) + 
               3*s1*(11 - 116*t2 + 6*Power(t2,2))) - 
            Power(t1,3)*(-68 + 2*Power(s1,3) + 
               Power(s1,2)*(97 - 6*t2) - 117*t2 + 50*Power(t2,2) + 
               8*Power(t2,3) + s1*(63 - 159*t2 + 16*Power(t2,2))) + 
            t1*(94 - 235*t2 + 62*Power(t2,2) - 309*Power(t2,3) + 
               s1*(-79 + 181*t2 + 64*Power(t2,2))) + 
            Power(s2,4)*(-8*Power(s1,3) + 140*Power(t1,3) + 
               7*Power(t1,2)*(-13 + 30*t2) + 
               Power(s1,2)*(1 + 76*t1 + 108*t2) + 
               t1*(21 - 56*t2 - 120*Power(t2,2)) + 
               6*t2*(7 + 48*t2 + 55*Power(t2,2)) - 
               s1*(7 + 196*Power(t1,2) + 100*t2 + 360*Power(t2,2) + 
                  6*t1*(-7 + 30*t2))) + 
            Power(s2,2)*(-1 - 483*Power(t1,4) + 150*Power(t1,5) - 
               2*Power(s1,3)*t1*(-9 + 5*t1) - 69*t2 - 28*Power(t2,2) + 
               902*Power(t2,3) + 
               Power(t1,3)*(483 + 186*t2 - 24*Power(t2,2)) + 
               t1*(97 + 102*t2 - 904*Power(t2,2) - 789*Power(t2,3)) + 
               Power(t1,2)*(-406 - 495*t2 + 433*Power(t2,2) + 
                  108*Power(t2,3)) + 
               Power(s1,2)*(63*Power(t1,3) + 69*t2 + 
                  Power(t1,2)*(-163 + 36*t2) - t1*(63 + 167*t2)) + 
               s1*(-1 - 81*Power(t1,4) + 43*t2 - 501*Power(t2,2) - 
                  8*Power(t1,3)*(-8 + 19*t2) + 
                  3*Power(t1,2)*(110 + 43*t2 + 4*Power(t2,2)) + 
                  28*t1*(-1 + 18*t2 + 14*Power(t2,2)))) + 
            Power(s2,3)*(-7 - 280*Power(t1,4) + 
               Power(s1,3)*(-7 + 17*t1) - 71*t2 + 341*Power(t2,2) + 
               780*Power(t2,3) - 7*Power(t1,3)*(-49 + 20*t2) + 
               2*Power(t1,2)*(-101 - 10*t2 + 54*Power(t2,2)) + 
               t1*(45 + 117*t2 - 652*Power(t2,2) - 360*Power(t2,3)) + 
               Power(s1,2)*(-1 - 150*Power(t1,2) + 142*t2 - 
                  20*t1*(-2 + 5*t2)) + 
               s1*(2 + 189*Power(t1,3) - 49*t2 - 632*Power(t2,2) + 
                  2*Power(t1,2)*(-61 + 168*t2) + 
                  t1*(59 - 38*t2 + 240*Power(t2,2)))) + 
            s2*(-8 - 20*Power(t1,6) + 
               Power(s1,3)*Power(t1,2)*(-13 + 5*t1) + 71*t2 - 
               374*Power(t2,2) + 614*Power(t2,3) + 
               Power(t1,5)*(214 + 5*t2) + 
               Power(t1,4)*(-423 - 132*t2 + Power(t2,2)) + 
               Power(t1,3)*(348 + 417*t2 - 76*Power(t2,2) - 
                  8*Power(t2,3)) + 
               2*Power(t1,2)*
                (-176 - 124*t2 + 246*Power(t2,2) + 93*Power(t2,3)) - 
               t1*(-120 + 173*t2 + 420*Power(t2,2) + 734*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-69 + 9*Power(t1,3) - 8*Power(t1,2)*(-2 + t2) - 
                  81*t2 + t1*(73 + 39*t2)) + 
               s1*(1 + 34*Power(t1,5) + 67*t2 - 184*Power(t2,2) + 
                  2*Power(t1,4)*(-58 + 5*t2) + 
                  Power(t1,3)*(-197 + 7*t2 - 18*Power(t2,2)) + 
                  Power(t1,2)*(340 - 499*t2 + 30*Power(t2,2)) + 
                  t1*(-58 + 582*t2 + 242*Power(t2,2))))) + 
         Power(s,7)*(25 - 183*t2 + 336*Power(t2,2) - 154*Power(t2,3) + 
            Power(t1,6)*(30 + 20*s1 + 4*t2) - 
            Power(t1,5)*(-139 + 90*t2 + 6*s1*(22 + t2)) + 
            t1*(-140 + 90*t2 + 91*Power(t2,2) + 384*Power(t2,3) + 
               s1*(103 - 27*t2 - 266*Power(t2,2))) + 
            Power(t1,4)*(-682 - 4*Power(s1,3) + 217*t2 - 
               16*Power(t2,2) + Power(s1,2)*(-66 + 7*t2) + 
               s1*(161 + 118*t2 - 4*Power(t2,2))) + 
            Power(t1,3)*(424 + 16*Power(s1,3) + 
               Power(s1,2)*(294 - 50*t2) + 32*t2 + 213*Power(t2,2) + 
               24*Power(t2,3) + s1*(-373 - 497*t2 + 14*Power(t2,2))) + 
            Power(t1,2)*(-182 - 21*t2 - 514*Power(t2,2) - 
               203*Power(t2,3) + 14*Power(s1,2)*(-17 + 4*t2) + 
               s1*(322 + 551*t2 + 129*Power(t2,2))) + 
            7*Power(s2,5)*(4*Power(s1,3) - 16*Power(t1,3) + 
               Power(t1,2)*(15 - 36*t2) - 
               Power(s1,2)*(1 + 20*t1 + 36*t2) + 
               t1*(-5 + 22*t2 + 30*Power(t2,2)) - 
               2*t2*(7 + 33*t2 + 33*Power(t2,2)) + 
               s1*(3 + 32*Power(t1,2) + 32*t2 + 90*Power(t2,2) + 
                  10*t1*(-1 + 3*t2))) + 
            Power(s2,3)*(5 - 200*Power(t1,5) + 
               Power(s1,3)*(22 - 108*t1 + 41*Power(t1,2)) + 173*t2 - 
               331*Power(t2,2) - 1275*Power(t2,3) - 
               4*Power(t1,4)*(-64 + 7*t2) + 
               7*Power(t1,3)*(-17 - 49*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(279 + 957*t2 - 1211*Power(t2,2) - 
                  252*Power(t2,3)) + 
               t1*(-121 - 340*t2 + 2391*Power(t2,2) + 
                  1464*Power(t2,3)) + 
               Power(s1,2)*(-8 - 197*Power(t1,3) - 301*t2 - 
                  20*Power(t1,2)*(-20 + 7*t2) + t1*(135 + 638*t2)) + 
               s1*(-25 + 122*Power(t1,4) + 130*t2 + 1052*Power(t2,2) + 
                  Power(t1,3)*(-3 + 392*t2) + 
                  t1*(211 - 1505*t2 - 1235*Power(t2,2)) + 
                  Power(t1,2)*(-722 - 80*t2 + 84*Power(t2,2)))) + 
            s2*(39 + 38*Power(t1,6) + 
               Power(s1,3)*Power(t1,2)*(52 - 42*t1 + 7*Power(t1,2)) - 
               179*t2 + 510*Power(t2,2) - 585*Power(t2,3) - 
               Power(t1,5)*(117 + 79*t2) + 
               Power(t1,4)*(-444 + 477*t2 - 23*Power(t2,2)) + 
               Power(t1,3)*(507 - 493*t2 + 470*Power(t2,2) + 
                  44*Power(t2,3)) - 
               Power(t1,2)*(64 - 44*t2 + 1605*Power(t2,2) + 
                  466*Power(t2,3)) + 
               t1*(-113 + 63*t2 + 1043*Power(t2,2) + 
                  1080*Power(t2,3)) + 
               Power(s1,2)*t1*
                (11*Power(t1,4) + Power(t1,3)*(7 - 8*t2) + 
                  Power(t1,2)*(165 + 46*t2) + 4*(32 + 49*t2) - 
                  t1*(566 + 147*t2)) + 
               s1*(-3 - 113*Power(t1,5) + 12*Power(t1,6) - 118*t2 + 
                  216*Power(t2,2) + 
                  t1*(88 - 1087*t2 - 655*Power(t2,2)) + 
                  Power(t1,4)*(104 + 24*t2 - 6*Power(t2,2)) + 
                  Power(t1,3)*(321 - 736*t2 + 48*Power(t2,2)) + 
                  Power(t1,2)*(-31 + 1958*t2 + 174*Power(t2,2)))) + 
            Power(s2,2)*(8 + 40*Power(t1,6) + 
               Power(s1,3)*t1*(-70 + 108*t1 - 25*Power(t1,2)) + 8*t2 + 
               170*Power(t2,2) - 1050*Power(t2,3) - 
               Power(t1,5)*(187 + 9*t2) + 
               Power(t1,4)*(141 + 297*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*(222 + 676*t2 - 2022*Power(t2,2) - 
                  519*Power(t2,3)) + 
               Power(t1,3)*(-23 - 986*t2 + 345*Power(t2,2) + 
                  28*Power(t2,3)) + 
               t1*(-173 - 83*t2 + 2173*Power(t2,2) + 
                  1594*Power(t2,3)) + 
               Power(s1,2)*(12*Power(t1,4) - 122*t2 + 
                  Power(t1,3)*(-287 + 48*t2) - 
                  3*Power(t1,2)*(47 + 105*t2) + t1*(276 + 542*t2)) + 
               s1*(-5 - 49*Power(t1,5) + Power(t1,4)*(62 - 76*t2) - 
                  4*t2 + 636*Power(t2,2) + 
                  t1*(281 - 2037*t2 - 1110*Power(t2,2)) + 
                  Power(t1,3)*(689 + 93*t2 + 42*Power(t2,2)) + 
                  Power(t1,2)*(-885 + 1745*t2 + 177*Power(t2,2)))) - 
            Power(s2,4)*(-17 - 280*Power(t1,4) + 
               Power(s1,3)*(-38 + 63*t1) + Power(t1,3)*(196 - 238*t2) - 
               126*t2 + 637*Power(t2,2) + 1050*Power(t2,3) + 
               Power(s1,2)*(3 - 294*Power(t1,2) + t1*(69 - 308*t2) + 
                  378*t2) + Power(t1,2)*
                (-59 - 70*t2 + 252*Power(t2,2)) - 
               2*t1*(-13 - 77*t2 + 658*Power(t2,2) + 315*Power(t2,3)) + 
               s1*(287*Power(t1,3) + Power(t1,2)*(-103 + 546*t2) + 
                  14*t1*(5 + 16*t2 + 45*Power(t2,2)) - 
                  2*(-6 + 93*t2 + 560*Power(t2,2))))) + 
         Power(s,3)*(-37 + 456*t1 - 191*s1*t1 - 1292*Power(t1,2) + 
            767*s1*Power(t1,2) - 82*Power(s1,2)*Power(t1,2) + 
            1714*Power(t1,3) - 1169*s1*Power(t1,3) - 
            2*Power(s1,2)*Power(t1,3) + 52*Power(s1,3)*Power(t1,3) - 
            1168*Power(t1,4) + 519*s1*Power(t1,4) + 
            152*Power(s1,2)*Power(t1,4) - 50*Power(s1,3)*Power(t1,4) + 
            355*Power(t1,5) + 234*s1*Power(t1,5) - 
            122*Power(s1,2)*Power(t1,5) + 10*Power(s1,3)*Power(t1,5) - 
            28*Power(t1,6) - 184*s1*Power(t1,6) + 
            28*Power(s1,2)*Power(t1,6) + 10*s1*Power(t1,7) + 171*t2 - 
            1285*t1*t2 + 434*s1*t1*t2 + 2310*Power(t1,2)*t2 - 
            1162*s1*Power(t1,2)*t2 + 56*Power(s1,2)*Power(t1,2)*t2 - 
            1433*Power(t1,3)*t2 + 1145*s1*Power(t1,3)*t2 - 
            246*Power(s1,2)*Power(t1,3)*t2 + 241*Power(t1,4)*t2 - 
            428*s1*Power(t1,4)*t2 + 185*Power(s1,2)*Power(t1,4)*t2 - 
            6*Power(t1,5)*t2 + 46*s1*Power(t1,5)*t2 - 
            30*Power(s1,2)*Power(t1,5)*t2 + 16*Power(t1,6)*t2 + 
            4*s1*Power(t1,6)*t2 - 243*Power(t2,2) + 
            1113*t1*Power(t2,2) - 239*s1*t1*Power(t2,2) - 
            1510*Power(t1,2)*Power(t2,2) + 
            627*s1*Power(t1,2)*Power(t2,2) + 
            747*Power(t1,3)*Power(t2,2) - 
            414*s1*Power(t1,3)*Power(t2,2) - 
            120*Power(t1,4)*Power(t2,2) + 
            64*s1*Power(t1,4)*Power(t2,2) + 109*Power(t2,3) - 
            291*t1*Power(t2,3) + 199*Power(t1,2)*Power(t2,3) - 
            32*Power(t1,3)*Power(t2,3) + 
            Power(s2,9)*(28*Power(s1,3) + Power(t1,2)*(1 - 10*t2) + 
               t1*(-1 + 31*t2 + 45*Power(t2,2)) - 
               t2*(22 + 63*t2 + 55*Power(t2,2)) + 
               Power(s1,2)*(4*t1 - 3*(7 + 36*t2)) + 
               s1*(7 - 4*Power(t1,2) + 80*t2 + 135*Power(t2,2) - 
                  t1*(2 + 45*t2))) + 
            Power(s2,3)*(-45 - 22*Power(t1,7) + 
               Power(s1,3)*t1*
                (-88 + 187*t1 - 208*Power(t1,2) + 90*Power(t1,3) - 
                  6*Power(t1,4)) - 227*t2 + 219*Power(t2,2) - 
               15*Power(t2,3) + Power(t1,6)*(-6 + 20*t2) + 
               Power(t1,5)*(270 + 34*t2) + 
               2*Power(t1,4)*(-76 + 136*t2 + 67*Power(t2,2)) + 
               Power(t1,2)*(-515 + 607*t2 + 509*Power(t2,2) - 
                  88*Power(t2,3)) + 
               t1*(481 + 38*t2 - 685*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,3)*(-11 - 403*t2 - 280*Power(t2,2) + 
                  40*Power(t2,3)) + 
               Power(s1,2)*(-112 + 12*Power(t1,6) + 
                  Power(t1,3)*(1223 - 132*t2) + t1*(637 - 34*t2) + 
                  45*t2 + 12*Power(t1,5)*(11 + t2) - 
                  2*Power(t1,2)*(626 + 29*t2) + 
                  Power(t1,4)*(-476 + 74*t2)) + 
               s1*(437 + 14*Power(t1,7) + Power(t1,6)*(62 - 20*t2) - 
                  270*t2 + 32*Power(t2,2) - 
                  14*Power(t1,5)*(41 + 6*t2) - 
                  4*Power(t1,4)*(-157 + 42*t2 + 9*Power(t2,2)) + 
                  4*Power(t1,2)*(445 - 97*t2 + 24*Power(t2,2)) + 
                  Power(t1,3)*(-1707 + 496*t2 + 32*Power(t2,2)) + 
                  t1*(-981 + 373*t2 + 49*Power(t2,2)))) + 
            Power(s2,7)*(29 + 
               Power(s1,3)*(20 - 18*t1 + 55*Power(t1,2)) - 143*t2 - 
               217*Power(t2,2) - 17*Power(t2,3) - 
               4*Power(t1,4)*(28 + 5*t2) + 
               Power(t1,3)*(197 + 79*t2 + 84*Power(t2,2)) + 
               t1*(-107 + 156*t2 + 329*Power(t2,2) + 24*Power(t2,3)) - 
               Power(t1,2)*(7 + 85*t2 + 353*Power(t2,2) + 
                  108*Power(t2,3)) + 
               Power(s1,2)*(-116 + 57*Power(t1,3) - 89*t2 - 
                  4*Power(t1,2)*(58 + 49*t2) + t1*(159 + 82*t2)) + 
               s1*(31 - 14*Power(t1,4) + Power(t1,3)*(21 - 48*t2) + 
                  350*t2 + 76*Power(t2,2) + 
                  t1*(99 - 449*t2 - 57*Power(t2,2)) + 
                  4*Power(t1,2)*(-31 + 109*t2 + 57*Power(t2,2)))) + 
            Power(s2,4)*(-84 + 10*Power(t1,7) + 
               Power(s1,3)*(16 - 117*t1 + 132*Power(t1,2) - 
                  18*Power(t1,3) + 88*Power(t1,4) + 6*Power(t1,5)) + 
               Power(t1,5)*(11 - 24*t2) + 87*t2 + 95*Power(t2,2) - 
               6*Power(t2,3) + Power(t1,6)*(62 + 4*t2) - 
               Power(t1,4)*(336 + 411*t2 + 82*Power(t2,2)) + 
               Power(t1,2)*(123 + 221*t2 + 394*Power(t2,2) - 
                  15*Power(t2,3)) + 
               t1*(-16 - 172*t2 - 361*Power(t2,2) + 28*Power(t2,3)) - 
               Power(t1,3)*(-230 + 184*t2 + 563*Power(t2,2) + 
                  72*Power(t2,3)) - 
               Power(s1,2)*(101 + 4*Power(t1,6) + 
                  Power(t1,2)*(710 - 226*t2) + 
                  Power(t1,5)*(22 - 6*t2) - 8*t2 + 
                  2*Power(t1,3)*(-70 + 71*t2) + t1*(-617 + 88*t2) + 
                  Power(t1,4)*(568 + 197*t2)) + 
               s1*(-58*Power(t1,6) + Power(t1,5)*(-6 + 22*t2) + 
                  Power(t1,2)*(563 - 493*t2 - 123*Power(t2,2)) - 
                  2*(-76 + 53*t2 + 8*Power(t2,2)) + 
                  Power(t1,4)*(1255 + 676*t2 + 56*Power(t2,2)) + 
                  t1*(-713 + 471*t2 + 62*Power(t2,2)) + 
                  Power(t1,3)*(-714 + 595*t2 + 166*Power(t2,2)))) + 
            Power(s2,2)*(-24 + 14*Power(t1,7) + 
               Power(s1,3)*t1*
                (38 + 158*t1 - 209*Power(t1,2) + 94*Power(t1,3) - 
                  28*Power(t1,4)) - 236*t2 - 82*Power(t2,2) + 
               78*Power(t2,3) - 2*Power(t1,6)*(73 + 2*t2) + 
               Power(t1,5)*(-53 + 9*t2) + 
               Power(t1,4)*(31 + 395*t2 + 31*Power(t2,2)) + 
               t1*(279 + 407*t2 + 329*Power(t2,2) - 126*Power(t2,3)) + 
               Power(t1,2)*(-800 + 358*t2 + 90*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(t1,3)*(699 - 1006*t2 - 151*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(s1,2)*(-20 - 16*Power(t1,6) + t1*(68 - 140*t2) + 
                  Power(t1,3)*(1601 - 76*t2) + 
                  Power(t1,5)*(156 - 8*t2) + 6*t2 + 
                  4*Power(t1,4)*(-184 + 7*t2) + 
                  Power(t1,2)*(-1061 + 103*t2)) + 
               s1*(-59 - 18*Power(t1,7) + 362*t2 - 124*Power(t2,2) + 
                  2*Power(t1,6)*(53 + 6*t2) + 
                  Power(t1,5)*(-395 + 28*t2) + 
                  Power(t1,3)*(-1873 + 461*t2 - 30*Power(t2,2)) - 
                  4*Power(t1,4)*(-337 + 49*t2 + 2*Power(t2,2)) + 
                  t1*(-921 + 259*t2 + 34*Power(t2,2)) + 
                  Power(t1,2)*(1889 - 1135*t2 + 201*Power(t2,2)))) + 
            Power(s2,8)*(-13 - Power(s1,3)*(18 + 77*t1) + 46*t2 + 
               14*Power(t2,2) + 45*Power(t2,3) + 
               Power(t1,3)*(32 + 25*t2) - 
               Power(t1,2)*(83 + 65*t2 + 108*Power(t2,2)) + 
               t1*(64 + 3*t2 + 212*Power(t2,2) + 135*Power(t2,3)) + 
               Power(s1,2)*(-29 - 30*Power(t1,2) + 98*t2 + 
                  5*t1*(33 + 52*t2)) + 
               s1*(11*Power(t1,3) + 3*Power(t1,2)*(4 + 33*t2) - 
                  2*(9 + 3*t2 + 64*Power(t2,2)) - 
                  t1*(14 + 317*t2 + 315*Power(t2,2)))) + 
            s2*(-157 - 2*Power(t1,7) - 
               Power(s1,3)*Power(t1,2)*
                (90 + 36*t1 - 125*Power(t1,2) + 46*Power(t1,3)) - 
               36*Power(t1,6)*(-5 + t2) + 239*t2 + 109*Power(t2,2) - 
               184*Power(t2,3) - Power(t1,5)*(389 + 9*t2) + 
               Power(t1,4)*(278 - 321*t2 + 75*Power(t2,2)) + 
               Power(t1,3)*(125 + 1757*t2 - 450*Power(t2,2) + 
                  36*Power(t2,3)) - 
               Power(t1,2)*(410 + 2218*t2 - 1069*Power(t2,2) + 
                  258*Power(t2,3)) + 
               t1*(375 + 558*t2 - 840*Power(t2,2) + 464*Power(t2,3)) + 
               Power(s1,2)*t1*
                (84 - 20*Power(t1,5) + Power(t1,2)*(113 - 2*t2) - 
                  44*t2 - 5*Power(t1,3)*(113 + 16*t2) + 
                  Power(t1,4)*(287 + 20*t2) + t1*(152 + 235*t2)) + 
               s1*(89 - 6*Power(t1,7) - 202*t2 + 109*Power(t2,2) + 
                  Power(t1,6)*(78 + 4*t2) - Power(t1,5)*(81 + 26*t2) + 
                  Power(t1,2)*(563 + 1078*t2 - 774*Power(t2,2)) + 
                  Power(t1,4)*(2 + 104*t2 - 70*Power(t2,2)) + 
                  t1*(-182 - 352*t2 + 123*Power(t2,2)) + 
                  Power(t1,3)*(-433 - 620*t2 + 472*Power(t2,2)))) - 
            Power(s2,5)*(-33 + 62*Power(t1,6) + 
               Power(s1,3)*(-4 - 14*t1 - 8*Power(t1,2) + 
                  236*Power(t1,3) + 27*Power(t1,4)) - 105*t2 - 
               16*Power(t2,2) + 81*Power(t2,3) + 
               Power(t1,5)*(5 + 9*t2) + 
               Power(t1,4)*(-14 + 123*t2 + 17*Power(t2,2)) + 
               Power(t1,2)*(205 + 278*t2 - 471*Power(t2,2) - 
                  294*Power(t2,3)) + 
               t1*(48 + 319*t2 + 129*Power(t2,2) - 144*Power(t2,3)) + 
               Power(t1,3)*(-273 - 935*t2 - 422*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(s1,2)*(-61 - 17*Power(t1,5) + 
                  Power(t1,2)*(324 - 163*t2) + t1*(232 - 60*t2) + 
                  8*t2 + Power(t1,4)*(3 + 10*t2) - 
                  Power(t1,3)*(1247 + 550*t2)) + 
               s1*(80 + 4*Power(t1,6) + 214*t2 - 112*Power(t2,2) - 
                  Power(t1,5)*(107 + 14*t2) + 
                  2*Power(t1,4)*(62 - 16*t2 + 3*Power(t2,2)) + 
                  Power(t1,3)*(1377 + 1812*t2 + 184*Power(t2,2)) + 
                  t1*(-158 - 537*t2 + 275*Power(t2,2)) + 
                  Power(t1,2)*(-1009 + 86*t2 + 510*Power(t2,2)))) + 
            Power(s2,6)*(Power(s1,3)*
                (14 - 72*t1 + 190*Power(t1,2) + 15*Power(t1,3)) + 
               Power(t1,5)*(131 + 5*t2) - 
               Power(t1,4)*(155 + 33*t2 + 21*Power(t2,2)) + 
               t1*(117 + 583*t2 - 13*Power(t2,2) - 386*Power(t2,3)) - 
               Power(t1,2)*(12 + 840*t2 + 562*Power(t2,2) + 
                  21*Power(t2,3)) + 
               Power(t1,3)*(-59 + 230*t2 + 191*Power(t2,2) + 
                  28*Power(t2,3)) + 
               2*(-11 - 10*t2 + 76*Power(t2,2) + 63*Power(t2,3)) + 
               Power(s1,2)*(-110 - 44*Power(t1,4) + t1*(738 - 96*t2) + 
                  48*t2 + Power(t1,3)*(101 + 48*t2) - 
                  Power(t1,2)*(941 + 529*t2)) + 
               s1*(145 + 11*Power(t1,5) + 6*t2 - 196*Power(t2,2) - 
                  2*Power(t1,4)*(47 + 10*t2) + 
                  Power(t1,3)*(201 - 217*t2 - 42*Power(t2,2)) + 
                  Power(t1,2)*(585 + 1651*t2 + 255*Power(t2,2)) + 
                  t1*(-773 - 911*t2 + 618*Power(t2,2))))) + 
         Power(s,4)*(24 - 573*t1 + 295*s1*t1 + 1988*Power(t1,2) - 
            1348*s1*Power(t1,2) + 221*Power(s1,2)*Power(t1,2) - 
            2950*Power(t1,3) + 2372*s1*Power(t1,3) - 
            168*Power(s1,2)*Power(t1,3) - 90*Power(s1,3)*Power(t1,3) + 
            2257*Power(t1,4) - 986*s1*Power(t1,4) - 
            96*Power(s1,2)*Power(t1,4) + 75*Power(s1,3)*Power(t1,4) - 
            986*Power(t1,5) - 406*s1*Power(t1,5) + 
            143*Power(s1,2)*Power(t1,5) - 15*Power(s1,3)*Power(t1,5) + 
            218*Power(t1,6) + 212*s1*Power(t1,6) - 
            40*Power(s1,2)*Power(t1,6) + 12*Power(t1,7) - 
            10*s1*Power(t1,7) - 183*t2 + 1848*t1*t2 - 801*s1*t1*t2 - 
            3032*Power(t1,2)*t2 + 1526*s1*Power(t1,2)*t2 - 
            112*Power(s1,2)*Power(t1,2)*t2 + 1210*Power(t1,3)*t2 - 
            1051*s1*Power(t1,3)*t2 + 355*Power(s1,2)*Power(t1,3)*t2 + 
            62*Power(t1,4)*t2 + 237*s1*Power(t1,4)*t2 - 
            205*Power(s1,2)*Power(t1,4)*t2 + 66*Power(t1,5)*t2 - 
            s1*Power(t1,5)*t2 + 30*Power(s1,2)*Power(t1,5)*t2 - 
            56*Power(t1,6)*t2 + 354*Power(t2,2) - 1655*t1*Power(t2,2) + 
            496*s1*t1*Power(t2,2) + 2171*Power(t1,2)*Power(t2,2) - 
            987*s1*Power(t1,2)*Power(t2,2) - 
            1003*Power(t1,3)*Power(t2,2) + 
            497*s1*Power(t1,3)*Power(t2,2) + 
            142*Power(t1,4)*Power(t2,2) - 
            65*s1*Power(t1,4)*Power(t2,2) - 199*Power(t2,3) + 
            396*t1*Power(t2,3) - 197*Power(t1,2)*Power(t2,3) + 
            24*Power(t1,3)*Power(t2,3) + 
            Power(s2,8)*(-56*Power(s1,3) + 2*Power(t1,3) + 
               9*Power(t1,2)*(-1 + 5*t2) + 
               7*Power(s1,2)*(5 + 4*t1 + 36*t2) + 
               t1*(7 - 104*t2 - 120*Power(t2,2)) + 
               t2*(70 + 192*t2 + 165*Power(t2,2)) - 
               s1*(21 + 4*Power(t1,2) + 196*t2 + 360*Power(t2,2) - 
                  2*t1*(7 + 30*t2))) + 
            s2*(205 + 14*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (71 + 113*t1 - 168*Power(t1,2) + 40*Power(t1,3)) + 
               Power(t1,6)*(68 - 40*t2) - 580*t2 + 255*Power(t2,2) + 
               111*Power(t2,3) + 13*Power(t1,5)*(-46 + 3*t2) + 
               Power(t1,4)*(1966 + 593*t2 + 115*Power(t2,2)) + 
               Power(t1,2)*(2588 + 930*t2 + 1509*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,3)*(-2966 - 1701*t2 - 796*Power(t2,2) + 
                  8*Power(t2,3)) - 
               t1*(1256 - 853*t2 + 974*Power(t2,2) + 138*Power(t2,3)) - 
               Power(s1,2)*t1*
                (147 + 4*Power(t1,5) + Power(t1,2)*(1106 - 76*t2) + 
                  11*t2 + 2*Power(t1,4)*(37 + t2) + 
                  Power(t1,3)*(-776 + 13*t2) + 2*t1*(-228 + 61*t2)) + 
               s1*(-95 - 4*Power(t1,7) + 257*t2 - 144*Power(t2,2) + 
                  Power(t1,5)*(-356 + 6*t2) + Power(t1,6)*(66 + 8*t2) - 
                  2*Power(t1,4)*(214 + 79*t2 + 6*Power(t2,2)) + 
                  Power(t1,3)*(2839 + 597*t2 + 22*Power(t2,2)) + 
                  3*Power(t1,2)*(-938 - 421*t2 + 32*Power(t2,2)) + 
                  t1*(550 + 614*t2 + 56*Power(t2,2)))) + 
            Power(s2,3)*(92 - 18*Power(t1,7) + 
               Power(s1,3)*(-31 + 249*t1 - 578*Power(t1,2) + 
                  494*Power(t1,3) - 130*Power(t1,4) + 2*Power(t1,5)) + 
               124*t2 + 377*Power(t2,2) - 14*Power(t2,3) - 
               4*Power(t1,6)*(5 + 4*t2) + 
               2*Power(t1,5)*(-187 + 57*t2) + 
               Power(t1,4)*(1727 + 879*t2 + 178*Power(t2,2)) + 
               Power(t1,2)*(972 + 2306*t2 + 1386*Power(t2,2) - 
                  134*Power(t2,3)) + 
               Power(t1,3)*(-1881 - 2278*t2 - 660*Power(t2,2) + 
                  48*Power(t2,3)) + 
               t1*(-457 - 891*t2 - 1340*Power(t2,2) + 52*Power(t2,3)) + 
               Power(s1,2)*(296 + 12*Power(t1,6) + 21*t2 - 
                  2*Power(t1,5)*(-8 + 7*t2) - 14*t1*(107 + 10*t2) - 
                  4*Power(t1,3)*(468 + 107*t2) + 
                  Power(t1,4)*(503 + 221*t2) + 
                  Power(t1,2)*(2644 + 274*t2)) + 
               s1*(-364 + 130*Power(t1,6) - 397*t2 - 40*Power(t2,2) - 
                  10*Power(t1,5)*(29 + 7*t2) - 
                  6*Power(t1,2)*(426 + 377*t2 + 2*Power(t2,2)) - 
                  2*Power(t1,4)*(317 + 392*t2 + 22*Power(t2,2)) + 
                  Power(t1,3)*(1721 + 1950*t2 + 104*Power(t2,2)) + 
                  t1*(1567 + 1600*t2 + 108*Power(t2,2)))) + 
            Power(s2,2)*(289 + 24*Power(t1,7) + 
               Power(s1,3)*t1*
                (46 - 360*t1 + 510*Power(t1,2) - 249*Power(t1,3) + 
                  21*Power(t1,4)) + Power(t1,6)*(70 - 48*t2) - 308*t2 + 
               640*Power(t2,2) - 15*Power(t2,3) + 
               Power(t1,5)*(-957 + 56*t2) + 
               Power(t1,4)*(1759 + 727*t2 + 80*Power(t2,2)) + 
               Power(t1,2)*(2318 + 1551*t2 + 1545*Power(t2,2) - 
                  54*Power(t2,3)) + 
               2*t1*(-530 - 135*t2 - 835*Power(t2,2) + 
                  11*Power(t2,3)) + 
               Power(t1,3)*(-2463 - 1658*t2 - 650*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(s1,2)*(20 + Power(t1,2)*(2513 - 26*t2) + 
                  32*t1*(-26 + t2) + 27*t2 - 
                  Power(t1,5)*(71 + 14*t2) + 
                  Power(t1,4)*(966 + 75*t2) - 
                  Power(t1,3)*(2505 + 118*t2)) + 
               s1*(-10 - 18*Power(t1,7) - 427*t2 - 16*Power(t2,2) + 
                  4*Power(t1,6)*(7 + 6*t2) + Power(t1,5)*(75 + 36*t2) - 
                  2*Power(t1,4)*(425 + 179*t2 + 11*Power(t2,2)) + 
                  Power(t1,3)*(2994 + 838*t2 + 94*Power(t2,2)) - 
                  Power(t1,2)*(4062 + 1129*t2 + 102*Power(t2,2)) + 
                  t1*(2003 + 882*t2 + 145*Power(t2,2)))) + 
            Power(s2,4)*(-18 + 148*Power(t1,6) + 
               Power(s1,3)*(-57 + 273*t1 - 458*Power(t1,2) + 
                  344*Power(t1,3) + 8*Power(t1,4)) + 66*t2 + 
               434*Power(t2,2) - 20*Power(t2,3) + 
               Power(t1,5)*(-257 + 54*t2) + 
               Power(t1,4)*(599 + 171*t2 + 90*Power(t2,2)) + 
               Power(t1,2)*(821 + 2429*t2 + 1497*Power(t2,2) - 
                  145*Power(t2,3)) - 
               Power(t1,3)*(1228 + 2306*t2 + 1243*Power(t2,2) + 
                  40*Power(t2,3)) + 
               t1*(-127 - 1056*t2 - 1359*Power(t2,2) + 
                  120*Power(t2,3)) + 
               Power(s1,2)*(287 - 30*Power(t1,5) + 71*t2 + 
                  10*Power(t1,4)*(-4 + 5*t2) + 
                  7*Power(t1,2)*(241 + 114*t2) - 3*t1*(365 + 118*t2) - 
                  Power(t1,3)*(1480 + 789*t2)) + 
               s1*(12*Power(t1,6) - Power(t1,5)*(351 + 35*t2) + 
                  Power(t1,4)*(797 + t2 + 15*Power(t2,2)) - 
                  2*(51 + 269*t2 + 20*Power(t2,2)) + 
                  t1*(572 + 2133*t2 + 86*Power(t2,2)) - 
                  Power(t1,2)*(1178 + 3444*t2 + 147*Power(t2,2)) + 
                  Power(t1,3)*(1016 + 3093*t2 + 265*Power(t2,2)))) + 
            Power(s2,7)*(19 - 8*Power(t1,4) + 
               Power(s1,3)*(-25 + 147*t1) - 113*t2 + 95*Power(t2,2) + 
               60*Power(t2,3) - Power(t1,3)*(103 + 92*t2) + 
               2*Power(t1,2)*(125 + 62*t2 + 126*Power(t2,2)) - 
               t1*(159 - 11*t2 + 700*Power(t2,2) + 360*Power(t2,3)) + 
               Power(s1,2)*(65 - 42*Power(t1,2) + 42*t2 - 
                  4*t1*(75 + 133*t2)) + 
               s1*(44 + 7*Power(t1,3) + Power(t1,2)*(22 - 48*t2) - 
                  87*t2 - 56*Power(t2,2) + 
                  t1*(-3 + 766*t2 + 720*Power(t2,2)))) + 
            Power(s2,6)*(7 + 10*Power(t1,5) + 
               Power(s1,3)*(-45 + 175*t1 - 106*Power(t1,2)) + 235*t2 + 
               460*Power(t2,2) + 21*Power(t2,3) + 
               Power(t1,4)*(359 + 56*t2) - 
               Power(t1,3)*(657 + 70*t2 + 168*Power(t2,2)) + 
               Power(t1,2)*(226 + 135*t2 + 1099*Power(t2,2) + 
                  252*Power(t2,3)) - 
               t1*(-51 + 314*t2 + 1284*Power(t2,2) + 378*Power(t2,3)) + 
               Power(s1,2)*(232 + 13*Power(t1,3) + 130*t2 + 
                  Power(t1,2)*(359 + 364*t2) - t1*(531 + 646*t2)) + 
               s1*(-33 + 17*Power(t1,4) - 735*t2 - 64*Power(t2,2) - 
                  2*Power(t1,3)*(115 + 56*t2) + 
                  Power(t1,2)*(472 - 1025*t2 - 420*Power(t2,2)) + 
                  t1*(-262 + 1694*t2 + 721*Power(t2,2)))) + 
            Power(s2,5)*(6 - 4*Power(t1,6) + 
               Power(s1,3)*(-58 + 246*t1 - 393*Power(t1,2) + 
                  5*Power(t1,3)) + 273*t2 + 393*Power(t2,2) - 
               109*Power(t2,3) - Power(t1,5)*(374 + 9*t2) + 
               Power(t1,4)*(669 - 24*t2 + 35*Power(t2,2)) - 
               Power(t1,3)*(426 + 401*t2 + 600*Power(t2,2) + 
                  56*Power(t2,3)) + 
               Power(t1,2)*(382 + 1816*t2 + 2061*Power(t2,2) + 
                  273*Power(t2,3)) + 
               t1*(-218 - 1293*t2 - 1138*Power(t2,2) + 
                  286*Power(t2,3)) + 
               Power(s1,2)*(297 + 19*Power(t1,4) + 97*t2 - 
                  2*Power(t1,3)*(41 + 60*t2) - t1*(1209 + 397*t2) + 
                  2*Power(t1,2)*(817 + 564*t2)) + 
               s1*(-107 - 32*Power(t1,5) - 789*t2 + 80*Power(t2,2) + 
                  2*Power(t1,4)*(218 + 67*t2) + 
                  t1*(688 + 2716*t2 - 284*Power(t2,2)) + 
                  Power(t1,3)*(-927 + 445*t2 + 42*Power(t2,2)) - 
                  Power(t1,2)*(468 + 3907*t2 + 732*Power(t2,2))))) + 
         Power(s,5)*(15 + 348*t1 - 217*s1*t1 - 1865*Power(t1,2) + 
            1483*s1*Power(t1,2) - 366*Power(s1,2)*Power(t1,2) + 
            2895*Power(t1,3) - 2688*s1*Power(t1,3) + 
            446*Power(s1,2)*Power(t1,3) + 90*Power(s1,3)*Power(t1,3) - 
            2577*Power(t1,4) + 846*s1*Power(t1,4) - 
            57*Power(s1,2)*Power(t1,4) - 59*Power(s1,3)*Power(t1,4) + 
            1401*Power(t1,5) + 271*s1*Power(t1,5) - 
            100*Power(s1,2)*Power(t1,5) + 9*Power(s1,3)*Power(t1,5) - 
            236*Power(t1,6) - 64*s1*Power(t1,6) + 
            26*Power(s1,2)*Power(t1,6) - 16*Power(t1,7) + 
            6*s1*Power(t1,7) + 27*t2 - 1528*t1*t2 + 841*s1*t1*t2 + 
            2225*Power(t1,2)*t2 - 921*s1*Power(t1,2)*t2 + 
            140*Power(s1,2)*Power(t1,2)*t2 - 232*Power(t1,3)*t2 + 
            127*s1*Power(t1,3)*t2 - 318*Power(s1,2)*Power(t1,3)*t2 - 
            191*Power(t1,4)*t2 + 139*s1*Power(t1,4)*t2 + 
            133*Power(s1,2)*Power(t1,4)*t2 - 184*Power(t1,5)*t2 - 
            34*s1*Power(t1,5)*t2 - 15*Power(s1,2)*Power(t1,5)*t2 + 
            60*Power(t1,6)*t2 - 4*s1*Power(t1,6)*t2 - 210*Power(t2,2) + 
            1439*t1*Power(t2,2) - 644*s1*t1*Power(t2,2) - 
            1984*Power(t1,2)*Power(t2,2) + 
            945*s1*Power(t1,2)*Power(t2,2) + 
            885*Power(t1,3)*Power(t2,2) - 
            328*s1*Power(t1,3)*Power(t2,2) - 
            108*Power(t1,4)*Power(t2,2) + 
            30*s1*Power(t1,4)*Power(t2,2) + 188*Power(t2,3) - 
            210*t1*Power(t2,3) + 13*Power(t1,2)*Power(t2,3) + 
            10*Power(t1,3)*Power(t2,3) + 
            Power(s2,7)*(70*Power(s1,3) - 16*Power(t1,3) - 
               5*Power(t1,2)*(-7 + 24*t2) - 
               7*Power(s1,2)*(5 + 14*t1 + 54*t2) + 
               7*t1*(-3 + 28*t2 + 30*Power(t2,2)) - 
               6*t2*(21 + 63*t2 + 55*Power(t2,2)) + 
               7*s1*(5 - 6*t1 + 8*Power(t1,2) + 44*t2 + 90*Power(t2,2))) \
+ Power(s2,5)*(-34 - 60*Power(t1,5) + 
               Power(s1,3)*(91 - 311*t1 + 125*Power(t1,2)) - 121*t2 - 
               695*Power(t2,2) - 343*Power(t2,3) - 
               4*Power(t1,4)*(137 + 21*t2) + 
               3*Power(t1,3)*(342 - 35*t2 + 70*Power(t2,2)) - 
               Power(t1,2)*(451 - 171*t2 + 1883*Power(t2,2) + 
                  378*Power(t2,3)) + 
               t1*(61 + 178*t2 + 2741*Power(t2,2) + 1092*Power(t2,3)) - 
               Power(s1,2)*(205 + 183*Power(t1,3) + 358*t2 + 
                  6*Power(t1,2)*(11 + 70*t2) - 2*t1*(328 + 629*t2)) + 
               s1*(-31 + 24*Power(t1,4) + 878*t2 + 468*Power(t2,2) + 
                  Power(t1,3)*(422 + 406*t2) + 
                  t1*(463 - 2954*t2 - 1687*Power(t2,2)) + 
                  Power(t1,2)*(-929 + 1110*t2 + 462*Power(t2,2)))) - 
            Power(s2,6)*(5 - 56*Power(t1,4) + 
               5*Power(s1,3)*(-16 + 35*t1) - 158*t2 + 385*Power(t2,2) + 
               420*Power(t2,3) - 14*Power(t1,3)*(11 + 14*t2) + 
               7*Power(t1,2)*(53 + 14*t2 + 54*Power(t2,2)) - 
               t1*(187 - 42*t2 + 1358*Power(t2,2) + 630*Power(t2,3)) + 
               Power(s1,2)*(65 - 210*Power(t1,2) + 350*t2 - 
                  t1*(253 + 686*t2)) + 
               s1*(51 + 105*Power(t1,3) - 242*t2 - 616*Power(t2,2) + 
                  5*Power(t1,2)*(13 + 42*t2) + 
                  2*t1*(-6 + 511*t2 + 525*Power(t2,2)))) - 
            Power(s2,2)*(184 - 14*Power(t1,7) + 
               Power(s1,3)*t1*
                (148 - 513*t1 + 508*Power(t1,2) - 133*Power(t1,3) + 
                  7*Power(t1,4)) - 413*t2 + 984*Power(t2,2) + 
               64*Power(t2,3) - 8*Power(t1,6)*(-7 + 3*t2) + 
               Power(t1,5)*(-1018 + 267*t2) + 
               Power(t1,4)*(3329 + 424*t2 + 217*Power(t2,2)) - 
               2*Power(t1,3)*
                (1876 + 1445*t2 + 933*Power(t2,2) + 24*Power(t2,3)) - 
               t1*(909 + 121*t2 + 3532*Power(t2,2) + 222*Power(t2,3)) + 
               Power(t1,2)*(2293 + 2595*t2 + 4274*Power(t2,2) + 
                  234*Power(t2,3)) + 
               Power(s1,2)*(10 + 12*Power(t1,6) + 
                  Power(t1,5)*(20 - 11*t2) + 88*t2 + 
                  Power(t1,4)*(75 + 139*t2) - 
                  2*Power(t1,3)*(900 + 227*t2) - t1*(1145 + 441*t2) + 
                  Power(t1,2)*(2748 + 709*t2)) + 
               s1*(2 + 98*Power(t1,6) - 447*t2 - 188*Power(t2,2) - 
                  Power(t1,5)*(365 + 68*t2) + 
                  Power(t1,4)*(444 - 557*t2 - 6*Power(t2,2)) + 
                  Power(t1,3)*(1219 + 3242*t2 + 184*Power(t2,2)) - 
                  2*Power(t1,2)*(1297 + 2500*t2 + 378*Power(t2,2)) + 
                  t1*(1277 + 2816*t2 + 706*Power(t2,2)))) - 
            s2*(76 + 14*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (-27 + 177*t1 - 123*Power(t1,2) + 14*Power(t1,3)) + 
               Power(t1,6)*(136 - 60*t2) - 441*t2 + 368*Power(t2,2) + 
               19*Power(t2,3) + Power(t1,5)*(-1554 + 265*t2) + 
               Power(t1,4)*(3691 + 497*t2 + 187*Power(t2,2)) - 
               Power(t1,3)*(4221 + 1949*t2 + 1570*Power(t2,2) + 
                  28*Power(t2,3)) - 
               t1*(1246 - 1425*t2 + 2405*Power(t2,2) + 
                  88*Power(t2,3)) + 
               Power(t1,2)*(2914 + 358*t2 + 3459*Power(t2,2) + 
                  126*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-2*Power(t1,5) + Power(t1,4)*(71 - 4*t2) + 
                  4*Power(t1,2)*(-407 + 4*t2) + 
                  Power(t1,3)*(346 + 9*t2) - 2*(89 + 78*t2) + 
                  t1*(1295 + 137*t2)) + 
               s1*(-50 - 10*Power(t1,7) + 218*t2 - 146*Power(t2,2) + 
                  6*Power(t1,6)*(9 + 2*t2) + 
                  Power(t1,5)*(-299 + 5*t2) - 
                  Power(t1,4)*(83 + 518*t2 + 16*Power(t2,2)) + 
                  Power(t1,3)*(2999 + 1844*t2 + 216*Power(t2,2)) + 
                  t1*(609 + 855*t2 + 571*Power(t2,2)) - 
                  Power(t1,2)*(3393 + 2308*t2 + 708*Power(t2,2)))) + 
            Power(s2,3)*(4 - 152*Power(t1,6) + 
               Power(s1,3)*(51 - 377*t1 + 686*Power(t1,2) - 
                  365*Power(t1,3) + 23*Power(t1,4)) + 
               Power(t1,5)*(558 - 121*t2) - 138*t2 - 1071*Power(t2,2) - 
               116*Power(t2,3) + 
               Power(t1,4)*(-1709 + 201*t2 - 125*Power(t2,2)) + 
               Power(t1,3)*(2541 + 2080*t2 + 1860*Power(t2,2) + 
                  100*Power(t2,3)) + 
               t1*(300 + 1289*t2 + 3652*Power(t2,2) + 
                  372*Power(t2,3)) - 
               Power(t1,2)*(1444 + 3101*t2 + 4354*Power(t2,2) + 
                  416*Power(t2,3)) + 
               Power(s1,2)*(35*Power(t1,5) + 
                  Power(t1,4)*(194 - 65*t2) - 2*(115 + 99*t2) + 
                  2*t1*(737 + 455*t2) + Power(t1,3)*(769 + 680*t2) - 
                  Power(t1,2)*(2175 + 1387*t2)) - 
               s1*(2 + 8*Power(t1,6) - 1014*t2 - 248*Power(t2,2) - 
                  Power(t1,5)*(391 + 35*t2) + 
                  Power(t1,4)*(1225 + 104*t2 + 20*Power(t2,2)) + 
                  Power(t1,3)*(-553 + 3118*t2 + 168*Power(t2,2)) - 
                  2*Power(t1,2)*(182 + 3267*t2 + 459*Power(t2,2)) + 
                  t1*(292 + 4481*t2 + 838*Power(t2,2)))) + 
            Power(s2,4)*(-4 + 20*Power(t1,6) + 
               Power(s1,3)*(88 - 409*t1 + 481*Power(t1,2) - 
                  40*Power(t1,3)) - 334*t2 - 933*Power(t2,2) - 
               176*Power(t2,3) + 5*Power(t1,5)*(97 + t2) + 
               Power(t1,4)*(-1158 + 195*t2 - 35*Power(t2,2)) + 
               Power(t1,3)*(1163 - 58*t2 + 905*Power(t2,2) + 
                  70*Power(t2,3)) + 
               t1*(230 + 1211*t2 + 3223*Power(t2,2) + 622*Power(t2,3)) - 
               Power(t1,2)*(833 + 1676*t2 + 3828*Power(t2,2) + 
                  651*Power(t2,3)) + 
               Power(s1,2)*(40*Power(t1,4) - 11*(25 + 28*t2) + 
                  4*Power(t1,3)*(-77 + 40*t2) - 
                  3*Power(t1,2)*(448 + 453*t2) + t1*(1228 + 1221*t2)) + 
               s1*(-43 + 30*Power(t1,5) + 1249*t2 + 296*Power(t2,2) - 
                  10*Power(t1,4)*(64 + 23*t2) - 
                  5*Power(t1,3)*(-348 + 53*t2) + 
                  t1*(150 - 4649*t2 - 1032*Power(t2,2)) + 
                  Power(t1,2)*(-536 + 5203*t2 + 1011*Power(t2,2))))) - 
         Power(s,6)*(36 + 2*(-3 + s1)*Power(t1,7) - 153*t2 + 
            126*Power(t2,2) + 28*Power(t2,3) + 
            Power(t1,6)*(-58 + 6*Power(s1,2) - 2*s1*(-13 + t2) + 26*t2) + 
            Power(t1,5)*(894 + 2*Power(s1,3) - 192*t2 - 
               Power(s1,2)*(35 + 3*t2) - s1*(49 + 23*t2)) + 
            t1*(5 - 602*t2 + 675*Power(t2,2) + 168*Power(t2,3) + 
               s1*(-13 + 433*t2 - 532*Power(t2,2))) + 
            Power(t1,4)*(-1854 - 24*Power(s1,3) + 64*t2 - 
               54*Power(t2,2) + Power(s1,2)*(-107 + 47*t2) + 
               s1*(383 + 248*t2 + Power(t2,2))) + 
            Power(t1,3)*(1613 + 52*Power(s1,3) + 
               Power(s1,2)*(500 - 171*t2) + 238*t2 + 533*Power(t2,2) + 
               32*Power(t2,3) - s1*(1597 + 583*t2 + 93*Power(t2,2))) + 
            Power(t1,2)*(-1005 + 771*t2 - 1211*Power(t2,2) - 
               181*Power(t2,3) + Power(s1,2)*(-377 + 112*t2) + 
               s1*(989 + 112*t2 + 525*Power(t2,2))) + 
            7*Power(s2,6)*(8*Power(s1,3) - 8*Power(t1,3) + 
               Power(t1,2)*(11 - 30*t2) - 
               Power(s1,2)*(3 + 22*t1 + 54*t2) - 
               2*t2*(10 + 36*t2 + 33*Power(t2,2)) + 
               t1*(-5 + 32*t2 + 36*Power(t2,2)) + 
               s1*(5 + 22*Power(t1,2) + 46*t2 + 108*Power(t2,2) + 
                  2*t1*(-5 + 9*t2))) + 
            Power(s2,4)*(-10 - 150*Power(t1,5) + 
               4*Power(s1,3)*(19 - 65*t1 + 23*Power(t1,2)) + 109*t2 - 
               684*Power(t2,2) - 973*Power(t2,3) - 
               5*Power(t1,4)*(59 + 14*t2) + 
               2*Power(t1,3)*(340 - 161*t2 + 84*Power(t2,2)) - 
               Power(t1,2)*(214 - 787*t2 + 1939*Power(t2,2) + 
                  378*Power(t2,3)) + 
               2*t1*(-3 - 108*t2 + 1685*Power(t2,2) + 819*Power(t2,3)) + 
               Power(s1,2)*(-77 - 279*Power(t1,3) + 
                  Power(t1,2)*(365 - 308*t2) - 491*t2 + 
                  2*t1*(191 + 610*t2)) + 
               s1*(-64 + 95*Power(t1,4) + 565*t2 + 1058*Power(t2,2) + 
                  Power(t1,3)*(277 + 546*t2) + 
                  Power(t1,2)*(-1053 + 479*t2 + 294*Power(t2,2)) - 
                  3*t1*(-152 + 931*t2 + 651*Power(t2,2)))) + 
            Power(s2,2)*(-11 - 38*Power(t1,6) + 
               2*Power(s1,3)*t1*
                (-72 + 178*t1 - 103*Power(t1,2) + 12*Power(t1,3)) + 
               51*t2 - 350*Power(t2,2) - 525*Power(t2,3) - 
               6*Power(t1,5)*(-52 + 23*t2) + 
               Power(t1,4)*(-1588 + 621*t2 - 80*Power(t2,2)) + 
               Power(t1,3)*(2049 + 202*t2 + 1385*Power(t2,2) + 
                  96*Power(t2,3)) - 
               Power(t1,2)*(992 + 1000*t2 + 4200*Power(t2,2) + 
                  741*Power(t2,3)) + 
               t1*(160 + 74*t2 + 3433*Power(t2,2) + 1242*Power(t2,3)) + 
               Power(s1,2)*(-2 + 26*Power(t1,5) + 
                  Power(t1,4)*(165 - 37*t2) - 133*t2 - 
                  4*Power(t1,2)*(356 + 209*t2) + 
                  Power(t1,3)*(175 + 307*t2) + t1*(725 + 771*t2)) + 
               s1*(-20 + 8*Power(t1,6) + 242*t2 + 438*Power(t2,2) + 
                  2*Power(t1,5)*(43 + 7*t2) + 
                  t1*(56 - 3355*t2 - 1349*Power(t2,2)) - 
                  Power(t1,3)*(-912 + 1893*t2 + Power(t2,2)) - 
                  Power(t1,4)*(549 + 67*t2 + 15*Power(t2,2)) + 
                  Power(t1,2)*(-112 + 5141*t2 + 759*Power(t2,2)))) + 
            s2*(42 + 4*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (82 - 128*t1 + 43*Power(t1,2) - 3*Power(t1,3)) - 16*t2 + 
               113*Power(t2,2) - 233*Power(t2,3) + 
               2*Power(t1,6)*(-3 + 8*t2) - 9*Power(t1,5)*(-85 + 29*t2) + 
               Power(t1,4)*(-2616 + 337*t2 - 109*Power(t2,2)) + 
               Power(t1,3)*(2637 + 775*t2 + 1224*Power(t2,2) + 
                  72*Power(t2,3)) - 
               Power(t1,2)*(1439 + 338*t2 + 3091*Power(t2,2) + 
                  483*Power(t2,3)) + 
               t1*(417 - 669*t2 + 2048*Power(t2,2) + 694*Power(t2,3)) + 
               Power(s1,2)*t1*
                (169 - 4*Power(t1,5) + Power(t1,3)*(12 - 26*t2) + 
                  3*Power(t1,4)*(-4 + t2) + 251*t2 + 
                  Power(t1,2)*(929 + 64*t2) - t1*(1279 + 246*t2)) + 
               s1*(8 + 2*Power(t1,6) - 161*t2 + 176*Power(t2,2) + 
                  Power(t1,5)*(-43 + 13*t2) + 
                  Power(t1,4)*(-76 + 323*t2 - 10*Power(t2,2)) - 
                  Power(t1,3)*(890 + 2015*t2 + 44*Power(t2,2)) + 
                  Power(t1,2)*(1587 + 3017*t2 + 660*Power(t2,2)) - 
                  t1*(197 + 1142*t2 + 888*Power(t2,2)))) + 
            Power(s2,5)*(15 + Power(s1,3)*(81 - 133*t1) + 
               168*Power(t1,4) + 159*t2 - 665*Power(t2,2) - 
               882*Power(t2,3) + 7*Power(t1,3)*(7 + 38*t2) - 
               2*Power(t1,2)*(118 - 7*t2 + 189*Power(t2,2)) + 
               t1*(86 - 105*t2 + 1666*Power(t2,2) + 756*Power(t2,3)) + 
               Power(s1,2)*(-29 + 336*Power(t1,2) - 518*t2 + 
                  t1*(48 + 574*t2)) - 
               s1*(33 + 245*Power(t1,3) - 297*t2 - 1148*Power(t2,2) + 
                  12*Power(t1,2)*(1 + 42*t2) + 
                  t1*(27 + 742*t2 + 1008*Power(t2,2)))) - 
            Power(s2,3)*(-11 - 40*Power(t1,6) + 
               Power(s1,3)*(-42 + 284*t1 - 324*Power(t1,2) + 
                  48*Power(t1,3)) + 5*Power(t1,5)*(-38 + t2) + 56*t2 + 
               604*Power(t2,2) + 793*Power(t2,3) + 
               Power(t1,4)*(772 - 342*t2 + 21*Power(t2,2)) - 
               Power(t1,3)*(928 - 895*t2 + 754*Power(t2,2) + 
                  56*Power(t2,3)) + 
               Power(t1,2)*(487 + 130*t2 + 3821*Power(t2,2) + 
                  777*Power(t2,3)) - 
               2*t1*(-4 + 125*t2 + 1942*Power(t2,2) + 823*Power(t2,3)) + 
               Power(s1,2)*(78 - 51*Power(t1,4) + 
                  Power(t1,3)*(543 - 120*t2) + 336*t2 + 
                  Power(t1,2)*(513 + 914*t2) - t1*(725 + 1248*t2)) + 
               s1*(81 + 15*Power(t1,5) - 645*t2 - 752*Power(t2,2) + 
                  2*Power(t1,4)*(166 + 95*t2) + 
                  Power(t1,2)*(1211 - 3933*t2 - 690*Power(t2,2)) - 
                  Power(t1,3)*(1543 + 65*t2 + 42*Power(t2,2)) + 
                  t1*(-582 + 4203*t2 + 1790*Power(t2,2))))))*T2q(s2,s))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*
       Power(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2),2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*(-1 + t2)) - 
    (8*((-1 + s2)*(s2 - t1)*Power(-1 + t1,4)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         Power(s,10)*(2*Power(t1,3) + Power(t1,2)*t2 + Power(t2,3)) + 
         Power(s,9)*(10*Power(t1,4) - 
            2*Power(t1,3)*(4 + 2*s1 + 3*s2 - 4*t2) + 
            Power(t1,2)*(4 + s2*(2 - 5*t2) + s1*(-3 + 4*s2 - t2) - 
               3*t2) - 3*Power(t2,2)*
             (-1 + s2 - s1*s2 + 3*t2 + 2*s2*t2) + 
            t1*t2*(3 + s2*(-1 + t2) + 6*Power(t2,2) + 
               s1*(-3 + 3*s2 + t2))) + 
         Power(s,8)*(20*Power(t1,5) + 
            Power(t1,3)*(13 + 2*Power(s1,2) + 6*Power(s2,2) + 
               s2*(21 - 33*t2) + 2*s1*(8 + 15*s2 - 4*t2) - 31*t2) - 
            Power(t1,4)*(23 + 20*s1 + 26*s2 - 25*t2) + 
            t2*(3 - 24*t2 + 33*Power(t2,2) + 
               s2*(-3 + s1*(3 - 20*t2) + 9*t2 + 43*Power(t2,2)) + 
               Power(s2,2)*(1 + 3*Power(s1,2) + 13*t2 + 
                  15*Power(t2,2) - 3*s1*(1 + 5*t2))) + 
            Power(t1,2)*(-26 + Power(s1,2)*(1 - 5*s2) + 11*t2 + 
               3*Power(t2,2) + 15*Power(t2,3) + 
               5*Power(s2,2)*(-1 + 2*t2) + 
               3*s2*(-1 + 4*t2 + 2*Power(t2,2)) + 
               s1*(19 - 11*Power(s2,2) - 16*t2 + 6*Power(t2,2) + 
                  s2*(-19 + 24*t2))) + 
            t1*(3 + Power(s1,2)*s2*(-3 + 3*s2 - t2) - 24*t2 + 
               13*Power(t2,2) - 49*Power(t2,3) - 
               Power(s2,2)*(-1 + t2 + 5*Power(t2,2)) + 
               s2*(2 + 10*t2 - 30*Power(t2,2) - 33*Power(t2,3)) + 
               s1*(-3 + Power(s2,2)*(2 - 10*t2) + 23*t2 - 
                  4*Power(t2,2) + s2*(3 - 7*t2 + 14*Power(t2,2))))) - 
         s*Power(-1 + t1,3)*(2*Power(-1 + s1,2)*Power(t1,6) - 
            Power(s2,6)*Power(s1 - t2,2)*(-1 + s1 + t1 - t2) + 
            4*Power(-1 + t2,3) - 
            t1*Power(-1 + t2,2)*(-33 + 8*s1 + 18*t2) + 
            (-1 + s1)*Power(t1,5)*
             (3 + 2*Power(s1,2) + 7*t2 - 3*s1*(5 + t2)) + 
            Power(t1,2)*(-1 + t2)*
             (78 + 2*Power(s1,2) - 86*t2 + 14*Power(t2,2) + 
               s1*(-41 + 39*t2)) + 
            Power(t1,3)*(74 + 2*Power(s1,3) + Power(s1,2)*(5 - 20*t2) - 
               131*t2 + 55*Power(t2,2) - 3*Power(t2,3) + 
               s1*(-48 + 94*t2 - 28*Power(t2,2))) + 
            Power(t1,4)*(-24 - Power(s1,3) + 46*t2 - 12*Power(t2,2) + 
               12*Power(s1,2)*(1 + t2) + s1*(1 - 40*t2 + 6*Power(t2,2))) \
+ Power(s2,5)*(s1 - t2)*(-1 + Power(s1,2)*(-4 + 3*t1) - 3*t2 - 
               4*Power(t2,2) - Power(t1,2)*(1 + 3*t2) + 
               t1*(2 + 6*t2 + 3*Power(t2,2)) + 
               s1*(4 + 4*Power(t1,2) + 8*t2 - 2*t1*(4 + 3*t2))) + 
            s2*(-(Power(s1,3)*Power(t1,2)*
                  (6 - t1 + 11*Power(t1,2) + Power(t1,3))) + 
               (-16 + t2)*Power(-1 + t2,2) - 2*Power(t1,5)*(4 + t2) + 
               Power(t1,4)*(24 + 23*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(-40 + 36*t2 + 34*Power(t2,2) - 
                  15*Power(t2,3)) + 
               Power(t1,3)*(-8 - 32*t2 - 11*Power(t2,2) + 
                  5*Power(t2,3)) + 
               2*t1*(24 - 29*t2 - 8*Power(t2,2) + 13*Power(t2,3)) + 
               Power(s1,2)*t1*
                (4 - 78*Power(t1,2) + Power(t1,4)*(-3 + t2) - 4*t2 + 
                  45*t1*t2 + Power(t1,3)*(77 + 9*t2)) + 
               s1*(8*Power(-1 + t2,2) + Power(t1,5)*(12 + t2) + 
                  t1*(-45 + 86*t2 - 41*Power(t2,2)) + 
                  Power(t1,2)*(17 - 50*t2 - 21*Power(t2,2)) - 
                  Power(t1,4)*(73 + 54*t2 + 6*Power(t2,2)) + 
                  Power(t1,3)*(81 + 33*t2 + 9*Power(t2,2)))) + 
            Power(s2,4)*(Power(s1,3)*(2 + 17*t1 - 2*Power(t1,2)) + 
               Power(s1,2)*(18 - 6*Power(t1,3) - 12*t2 + 
                  Power(t1,2)*(15 + 8*t2) - t1*(27 + 47*t2)) + 
               t2*(2 + 23*t2 - 9*Power(t2,2) - 
                  3*Power(t1,3)*(1 + t2) + 
                  Power(t1,2)*(8 + 14*t2 + 3*Power(t2,2)) - 
                  t1*(7 + 34*t2 + 11*Power(t2,2))) + 
               s1*(-2 - 41*t2 + 19*Power(t2,2) + 
                  Power(t1,3)*(3 + 9*t2) - 
                  Power(t1,2)*(8 + 29*t2 + 9*Power(t2,2)) + 
                  t1*(7 + 61*t2 + 41*Power(t2,2)))) + 
            Power(s2,2)*(2 + 
               Power(s1,3)*t1*
                (6 + 3*t1 + 25*Power(t1,2) + 3*Power(t1,3)) - 22*t2 - 
               Power(t1,5)*t2 + 32*Power(t2,2) - 12*Power(t2,3) + 
               Power(t1,4)*(5 + 18*t2) + 
               t1*(-11 + 5*t2 + 6*Power(t2,2) - 15*Power(t2,3)) + 
               Power(t1,2)*(21 + 72*t2 - 6*Power(t2,2) - 
                  9*Power(t2,3)) - 
               Power(t1,3)*(17 + 72*t2 + 32*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(s1,2)*(2 + Power(t1,5) - 2*t2 + 
                  15*t1*(1 + 2*t2) + Power(t1,4)*(-2 + 3*t2) + 
                  6*Power(t1,2)*(-23 + 8*t2) + 
                  2*Power(t1,3)*(61 + 16*t2)) + 
               s1*(Power(t1,5)*(1 + t2) - Power(t1,4)*(16 + 9*t2) + 
                  2*(2 - 3*t2 + Power(t2,2)) + 
                  3*Power(t1,2)*(-40 - 27*t2 + 10*Power(t2,2)) + 
                  Power(t1,3)*(84 + 151*t2 + 16*Power(t2,2)) + 
                  t1*(47 - 56*t2 + 63*Power(t2,2)))) + 
            Power(s2,3)*(-1 - 
               Power(s1,3)*(2 + 5*t1 + 29*Power(t1,2) + 2*Power(t1,3)) + 
               24*t2 - 29*Power(t2,2) + 11*Power(t2,3) + 
               Power(t1,4)*t2*(3 + t2) - 
               Power(t1,3)*(-1 + 22*t2 + 9*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-3 + 59*t2 + 55*Power(t2,2) + 
                  7*Power(t2,3)) + 
               t1*(3 - 64*t2 - 18*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s1,2)*(-9*Power(t1,3) + 4*Power(t1,4) + 
                  5*(2 + t2) + 6*t1*(-15 + 8*t2) + 
                  Power(t1,2)*(85 + 61*t2)) - 
               s1*(Power(t1,4)*(3 + 5*t2) - 
                  Power(t1,3)*(14 + 25*t2 + 3*Power(t2,2)) + 
                  2*(8 - 6*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*(35 + 161*t2 + 39*Power(t2,2)) + 
                  t1*(-40 - 129*t2 + 64*Power(t2,2))))) + 
         Power(s,2)*Power(-1 + t1,2)*
          (2*Power(t1,8) - Power(s2,6)*(-5 + 3*s1 + 5*t1 - 3*t2)*
             Power(s1 - t2,2) - 12*Power(-1 + t2,2)*(-1 + 2*t2) + 
            2*Power(t1,7)*(s1 - 2*(2 + t2)) + 
            Power(t1,6)*(15 - 16*Power(s1,2) + t2 + 2*Power(t2,2) + 
               s1*(13 + 5*t2)) + 
            t1*(-1 + t2)*(123 - 218*t2 + 86*Power(t2,2) + 
               s1*(-46 + 50*t2)) + 
            Power(t1,2)*(348 - 701*t2 + 472*Power(t2,2) - 
               80*Power(t2,3) - 2*Power(s1,2)*(-7 + 6*t2) + 
               s1*(-199 + 351*t2 - 179*Power(t2,2))) + 
            Power(t1,4)*(251 + 19*Power(s1,3) - 364*t2 + 
               78*Power(t2,2) - Power(t2,3) - 
               9*Power(s1,2)*(16 + 7*t2) + 
               s1*(66 + 250*t2 - 40*Power(t2,2))) + 
            Power(t1,5)*(-68 - 10*Power(s1,3) + 107*t2 - 
               8*Power(t2,2) + Power(s1,2)*(115 + 16*t2) + 
               s1*(-110 - 77*t2 + Power(t2,2))) + 
            Power(t1,3)*(-429 - 12*Power(s1,3) + 668*t2 - 
               300*Power(t2,2) + 22*Power(t2,3) + 
               Power(s1,2)*(31 + 68*t2) + 
               s1*(182 - 433*t2 + 159*Power(t2,2))) + 
            Power(s2,4)*(11 + Power(s1,3)*(9 - 2*t1 + 2*Power(t1,2)) + 
               Power(t1,4)*(4 - 3*t2) + 10*t2 - 12*Power(t2,2) + 
               3*Power(t2,3) - 
               Power(t1,3)*(23 + 7*t2 + 21*Power(t2,2)) + 
               Power(t1,2)*(45 + 33*t2 + 68*Power(t2,2) + 
                  6*Power(t2,3)) - 
               t1*(37 + 33*t2 + 35*Power(t2,2) + 18*Power(t2,3)) - 
               Power(s1,2)*(29 + Power(t1,3) + 19*t2 + 
                  Power(t1,2)*(-11 + 2*t2) + t1*(-19 + 6*t2)) + 
               s1*(-5 + 9*Power(t1,4) + 45*t2 + 7*Power(t2,2) - 
                  Power(t1,2)*t2*(67 + 6*t2) + 
                  2*Power(t1,3)*(-8 + 9*t2) + 
                  2*t1*(6 + 2*t2 + 13*Power(t2,2)))) + 
            Power(s2,5)*(Power(s1,3)*(-7 + 4*t1) + 
               Power(s1,2)*(14 + 10*Power(t1,2) + 24*t2 - 
                  3*t1*(8 + 5*t2)) + 
               t2*(4 + Power(t1,3) + 20*t2 + 10*Power(t2,2) + 
                  2*Power(t1,2)*(1 + 8*t2) - 
                  t1*(7 + 36*t2 + 7*Power(t2,2))) + 
               s1*(-2 - 3*Power(t1,3) + Power(t1,2)*(4 - 26*t2) - 
                  34*t2 - 27*Power(t2,2) + 
                  t1*(1 + 60*t2 + 18*Power(t2,2)))) + 
            Power(s2,3)*(4 + 
               4*Power(s1,3)*(1 - 5*t1 + 10*Power(t1,2)) - 72*t2 + 
               64*Power(t2,2) - 7*Power(t2,3) + 
               Power(t1,5)*(-14 + 3*t2) + 
               Power(t1,4)*(85 + 7*t2 + 14*Power(t2,2)) - 
               Power(t1,3)*(175 + 8*t2 + 58*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(155 - 89*t2 - 2*Power(t2,2) + 
                  11*Power(t2,3)) - 
               t1*(55 - 159*t2 + 18*Power(t2,2) + 25*Power(t2,3)) + 
               Power(s1,2)*(-27 - 7*Power(t1,4) - 13*t2 + 
                  2*Power(t1,3)*(17 + 5*t2) + t1*(214 + 24*t2) - 
                  Power(t1,2)*(214 + 93*t2)) + 
               s1*(37 - 9*Power(t1,5) + 2*t2 + 18*Power(t2,2) + 
                  3*Power(t1,4)*(4 + t2) + 
                  Power(t1,3)*(22 - 45*t2 - 9*Power(t2,2)) + 
                  t1*(-55 - 323*t2 + 15*Power(t2,2)) + 
                  Power(t1,2)*(-7 + 363*t2 + 48*Power(t2,2)))) + 
            s2*(43 - 10*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (28 - 40*t1 + 35*Power(t1,2) + 4*Power(t1,3)) - 92*t2 + 
               46*Power(t2,2) + 3*Power(t2,3) + 
               2*Power(t1,6)*(24 + 7*t2) + 
               Power(t1,4)*(78 - 185*t2 - 24*Power(t2,2)) - 
               Power(t1,5)*(75 + 13*t2 + 6*Power(t2,2)) - 
               Power(t1,3)*(157 - 341*t2 + 12*Power(t2,2) + 
                  15*Power(t2,3)) - 
               2*t1*(85 - 86*t2 + 13*Power(t2,2) + 27*Power(t2,3)) + 
               Power(t1,2)*(243 - 237*t2 + 22*Power(t2,2) + 
                  39*Power(t2,3)) + 
               Power(s1,2)*t1*
                (Power(t1,5) - 3*Power(t1,4)*(-12 + t2) + 
                  4*(-5 + 4*t2) - Power(t1,3)*(388 + 41*t2) + 
                  Power(t1,2)*(500 + 56*t2) - t1*(129 + 109*t2)) + 
               s1*(-26 + Power(t1,6)*(-12 + t2) + 56*t2 - 
                  30*Power(t2,2) - 
                  Power(t1,5)*(15 + 35*t2 + 3*Power(t2,2)) + 
                  Power(t1,4)*(321 + 307*t2 + 43*Power(t2,2)) + 
                  2*t1*(59 - 76*t2 + 44*Power(t2,2)) + 
                  Power(t1,2)*(97 + 88*t2 + 56*Power(t2,2)) - 
                  Power(t1,3)*(483 + 265*t2 + 73*Power(t2,2)))) + 
            Power(s2,2)*(7 - Power(s1,3)*t1*
                (20 - 32*t1 + 56*Power(t1,2) + 7*Power(t1,3)) - 
               Power(t1,6)*(-18 + t2) + 37*t2 - 17*Power(t2,2) + 
               12*Power(t2,3) - 
               2*Power(t1,5)*(51 + 6*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(-160 + 259*t2 + 19*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(t1,4)*(195 - 16*t2 + 30*Power(t2,2) + 
                  Power(t2,3)) + 
               2*Power(t1,2)*
                (30 - 190*t2 + 51*Power(t2,2) + 12*Power(t2,3)) + 
               t1*(-18 + 113*t2 - 130*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s1,2)*(6 + 2*Power(t1,5) + 
                  Power(t1,4)*(-46 + t2) - 4*t2 + 
                  Power(t1,2)*(-541 + 2*t2) + 
                  2*Power(t1,3)*(227 + 50*t2) + t1*(125 + 54*t2)) + 
               s1*(11 + 3*Power(t1,6) + Power(t1,5)*(10 - 6*t2) - 
                  59*t2 + 21*Power(t2,2) + 
                  t1*(-226 + 163*t2 - 143*Power(t2,2)) + 
                  Power(t1,4)*(-21 + 92*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*(427 + 393*t2 + 41*Power(t2,2)) - 
                  Power(t1,3)*(204 + 583*t2 + 81*Power(t2,2))))) + 
         Power(s,7)*(1 + 20*Power(t1,6) + 
            Power(t1,4)*(-59 + 10*Power(s1,2) + s1*(99 - 25*t2) - 
               97*t2) - 21*t2 + 75*Power(t2,2) - 61*Power(t2,3) + 
            Power(t1,5)*(-4 - 40*s1 + 40*t2) + 
            Power(t1,2)*(65 + 2*Power(s1,2)*(-7 + t2) - 84*t2 - 
               4*Power(t2,2) - 109*Power(t2,3) + 
               s1*(-40 + 136*t2 - 21*Power(t2,2))) + 
            t1*(-21 + 80*t2 - 85*Power(t2,2) + 156*Power(t2,3) + 
               s1*(19 - 60*t2 - 5*Power(t2,2))) + 
            Power(t1,3)*(8 - 18*Power(s1,2) + 35*t2 + 14*Power(t2,2) + 
               20*Power(t2,3) + s1*(-9 - 10*t2 + 15*Power(t2,2))) + 
            Power(s2,3)*(Power(s1,3) - 2*Power(t1,3) + 
               Power(t1,2)*(4 - 10*t2) - 4*Power(s1,2)*(2*t1 + 3*t2) + 
               t1*(-2 + 9*t2 + 10*Power(t2,2)) - 
               t2*(5 + 22*t2 + 20*Power(t2,2)) + 
               s1*(1 + 9*Power(t1,2) + 11*t2 + 30*Power(t2,2) + 
                  2*t1*(-2 + 5*t2))) + 
            Power(s2,2)*(1 + 22*Power(t1,4) - 
               2*Power(t1,3)*(8 + 27*s1 - 27*t2) + 
               (10 + 6*s1 - 16*Power(s1,2))*t2 + 
               (-59 + 81*s1)*Power(t2,2) - 85*Power(t2,3) + 
               Power(t1,2)*(-13 + 26*Power(s1,2) + s1*(59 - 62*t2) - 
                  46*t2 - 27*Power(t2,2)) + 
               t1*(7 - 2*Power(s1,3) - 8*t2 + 114*Power(t2,2) + 
                  75*Power(t2,3) + 3*Power(s1,2)*(-3 + 7*t2) - 
                  15*s1*(1 - t2 + 5*Power(t2,2)))) + 
            s2*(-44*Power(t1,5) + Power(t1,4)*(22 + 80*s1 - 84*t2) + 
               Power(t1,3)*(78 - 28*Power(s1,2) + 115*t2 + 
                  17*Power(t2,2) + 3*s1*(-51 + 26*t2)) + 
               t2*(9 + 16*t2 - 124*Power(t2,2) + s1*(-16 + 51*t2)) + 
               Power(t1,2)*(-25 + Power(s1,3) + 
                  Power(s1,2)*(19 - 7*t2) + 29*t2 - 122*Power(t2,2) - 
                  75*Power(t2,3) + 3*s1*(13 - 18*t2 + 8*Power(t2,2))) + 
               t1*(-10 - 36*t2 + 129*Power(t2,2) + 209*Power(t2,3) + 
                  2*Power(s1,2)*(8 + 7*t2) - 
                  s1*(9 + 47*t2 + 92*Power(t2,2))))) + 
         Power(s,6)*(-6 + 10*Power(t1,7) + 
            Power(t1,5)*(-273 + 20*Power(s1,2) + s1*(178 - 40*t2) - 
               142*t2) + 54*t2 - 108*Power(t2,2) + 51*Power(t2,3) + 
            Power(t1,6)*(56 - 40*s1 + 35*t2) - 
            Power(t1,2)*(54 - 215*t2 + 6*Power(t2,2) - 
               290*Power(t2,3) + 2*Power(s1,2)*(-29 + 6*t2) + 
               3*s1*t2*(103 + 11*t2)) + 
            Power(t1,4)*(391 - 90*Power(s1,2) + 48*t2 + 
               28*Power(t2,2) + 15*Power(t2,3) + 
               s1*(-158 + 65*t2 + 20*Power(t2,2))) + 
            Power(s2,4)*(-3*Power(s1,3) + Power(t1,2)*(-1 + 5*t2) + 
               Power(s1,2)*(1 + 6*t1 + 18*t2) + 
               t1*(1 - 11*t2 - 10*Power(t2,2)) + 
               t2*(7 + 18*t2 + 15*Power(t2,2)) - 
               s1*(2 - 2*t1 + Power(t1,2) + 15*t2 + 30*Power(t2,2))) - 
            Power(t1,3)*(176 + 2*Power(s1,3) + 21*t2 + 85*Power(t2,2) + 
               126*Power(t2,3) - 12*Power(s1,2)*(4 + t2) + 
               s1*(8 - 205*t2 + 43*Power(t2,2))) + 
            t1*(52 - 123*t2 + 176*Power(t2,2) - 230*Power(t2,3) + 
               s1*(-38 + 38*t2 + 58*Power(t2,2))) + 
            Power(s2,2)*(1 + Power(s1,3)*(14 - 11*t1)*t1 + 
               30*Power(t1,5) - 50*t2 + 116*Power(t2,2) + 
               192*Power(t2,3) + Power(t1,4)*(30 + 107*t2) - 
               Power(t1,3)*(241 + 228*t2 + 70*Power(t2,2)) + 
               Power(t1,2)*(233 + 65*t2 + 384*Power(t2,2) + 
                  153*Power(t2,3)) - 
               t1*(53 - 83*t2 + 477*Power(t2,2) + 364*Power(t2,3)) + 
               Power(s1,2)*(72*Power(t1,3) + 34*t2 + 
                  Power(t1,2)*(-90 + 61*t2) - t1*(7 + 117*t2)) - 
               s1*(1 + 93*Power(t1,4) + 7*t2 + 169*Power(t2,2) + 
                  2*Power(t1,3)*(-129 + 71*t2) + 
                  3*Power(t1,2)*(59 - 39*t2 + 51*Power(t2,2)) - 
                  4*t1*(9 + 26*t2 + 90*Power(t2,2)))) + 
            Power(s2,3)*(2 - 6*Power(t1,4) + 
               5*Power(s1,3)*(-1 + 2*t1) + Power(t1,3)*(2 - 44*t2) + 
               8*t2 + 97*Power(t2,2) + 90*Power(t2,3) + 
               Power(s1,2)*(-1 - 39*Power(t1,2) + t1*(24 - 66*t2) + 
                  53*t2) + Power(t1,2)*(17 + 84*t2 + 48*Power(t2,2)) - 
               t1*(15 + 52*t2 + 168*Power(t2,2) + 90*Power(t2,3)) + 
               s1*(31*Power(t1,3) + 46*Power(t1,2)*(-1 + t2) - 
                  6*t2*(7 + 22*t2) + t1*(19 + 35*t2 + 140*Power(t2,2)))) \
+ s2*(-3 - 36*Power(t1,6) + Power(s1,3)*Power(t1,2)*(-11 + 4*t1) + 
               2*t2 - 96*Power(t2,2) + 183*Power(t2,3) - 
               Power(t1,5)*(75 + 106*t2) + 
               Power(t1,4)*(471 + 291*t2 + 28*Power(t2,2)) + 
               t1*(8 + 16*t2 - 201*Power(t2,2) - 504*Power(t2,3)) - 
               Power(t1,3)*(524 + 68*t2 + 259*Power(t2,2) + 
                  90*Power(t2,3)) + 
               Power(t1,2)*(159 - 111*t2 + 555*Power(t2,2) + 
                  409*Power(t2,3)) - 
               Power(s1,2)*t1*
                (34 + 61*Power(t1,3) + t1*(12 - 80*t2) + 52*t2 + 
                  Power(t1,2)*(-155 + 17*t2)) + 
               s1*(1 + 100*Power(t1,5) + 32*t2 - 62*Power(t2,2) + 
                  Power(t1,4)*(-374 + 133*t2) + 
                  Power(t1,3)*(279 - 201*t2 + 13*Power(t2,2)) - 
                  Power(t1,2)*(31 + 292*t2 + 165*Power(t2,2)) + 
                  t1*(1 + 253*t2 + 212*Power(t2,2))))) + 
         Power(s,3)*(-1 + t1)*
          (9 + 16*Power(t1,8) + 
            Power(t1,7)*(-78 + 2*Power(s1,2) + s1*(13 - 3*t2) - 19*t2) - 
            3*Power(s2,6)*Power(s1 - t2,2)*(-1 + s1 + t1 - t2) - 63*t2 + 
            111*Power(t2,2) - 57*Power(t2,3) - 
            Power(t1,5)*(525 + 20*Power(s1,3) - 406*t2 + 
               51*Power(t2,2) + 3*Power(t2,3) - 
               Power(s1,2)*(329 + 36*t2) + s1*(226 + 225*t2)) + 
            Power(t1,2)*(647 + Power(s1,2)*(52 - 30*t2) - 1240*t2 + 
               950*Power(t2,2) - 156*Power(t2,3) + 
               s1*(-440 + 688*t2 - 418*Power(t2,2))) + 
            Power(t1,4)*(969 + 45*Power(s1,3) - 1004*t2 + 
               249*Power(t2,2) + 14*Power(t2,3) - 
               Power(s1,2)*(404 + 135*t2) + 
               s1*(145 + 539*t2 - 108*Power(t2,2))) + 
            Power(t1,6)*(215 - 64*Power(s1,2) - 13*t2 + 
               10*Power(t2,2) + s1*(-1 + 41*t2 + Power(t2,2))) + 
            t1*(-171 + 561*t2 - 562*Power(t2,2) + 179*Power(t2,3) + 
               s1*(91 - 226*t2 + 131*Power(t2,2))) + 
            Power(t1,3)*(-1082 - 26*Power(s1,3) + 1372*t2 - 
               707*Power(t2,2) + 24*Power(t2,3) + 
               Power(s1,2)*(85 + 132*t2) + 
               s1*(418 - 814*t2 + 391*Power(t2,2))) + 
            Power(s2,2)*(27 - 
               Power(s1,3)*t1*
                (-8 + 9*t1 + 16*Power(t1,2) + 8*Power(t1,3)) + 25*t2 + 
               68*Power(t2,2) + 31*Power(t2,3) + 
               2*Power(t1,6)*(48 + t2) - 
               Power(t1,5)*(526 + 88*t2 + 31*Power(t2,2)) + 
               Power(t1,4)*(1072 + 154*t2 + 239*Power(t2,2) + 
                  21*Power(t2,3)) - 
               t1*(151 - 228*t2 + 598*Power(t2,2) + 124*Power(t2,3)) - 
               Power(t1,3)*(1047 - 226*t2 + 628*Power(t2,2) + 
                  136*Power(t2,3)) + 
               Power(t1,2)*(529 - 547*t2 + 950*Power(t2,2) + 
                  233*Power(t2,3)) + 
               Power(s1,2)*(6 + 12*Power(t1,5) + t1*(95 - 56*t2) - 
                  2*Power(t1,3)*(-310 + t2) + 
                  Power(t1,4)*(-165 + t2) + 4*t2 + 
                  8*Power(t1,2)*(-71 + 16*t2)) + 
               s1*(26 + 12*Power(t1,6) + Power(t1,5)*(75 - 28*t2) - 
                  160*t2 + 29*Power(t2,2) + 
                  Power(t1,2)*(395 - 122*t2 - 91*Power(t2,2)) + 
                  t1*(-289 + 604*t2 - 43*Power(t2,2)) + 
                  Power(t1,4)*(-291 + 224*t2 + 9*Power(t2,2)) + 
                  Power(t1,3)*(72 - 518*t2 + 21*Power(t2,2)))) + 
            Power(s2,5)*(Power(s1,3)*(-13 + 18*t1) + 
               Power(s1,2)*(20 + 20*Power(t1,2) + 48*t2 - 
                  t1*(40 + 63*t2)) - 
               s1*(10 + 5*Power(t1,3) + 54*t2 + 54*Power(t1,2)*t2 + 
                  57*Power(t2,2) - 3*t1*(5 + 36*t2 + 24*Power(t2,2))) + 
               t2*(16 - Power(t1,3) + 34*t2 + 22*Power(t2,2) + 
                  2*Power(t1,2)*(9 + 17*t2) - 
                  t1*(33 + 68*t2 + 27*Power(t2,2)))) + 
            s2*(40 + Power(s1,3)*Power(t1,2)*
                (28 - 51*t1 + 39*Power(t1,2) + 7*Power(t1,3)) - 97*t2 + 
               36*Power(t2,2) + 14*Power(t2,3) - 
               Power(t1,7)*(65 + 2*t2) + 
               Power(t1,6)*(331 + 68*t2 + 3*Power(t2,2)) - 
               Power(t1,5)*(703 + 69*t2 + 58*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,4)*(899 - 441*t2 + 203*Power(t2,2) + 
                  37*Power(t2,3)) - 
               t1*(246 - 113*t2 + 116*Power(t2,2) + 103*Power(t2,3)) - 
               Power(t1,3)*(854 - 746*t2 + 486*Power(t2,2) + 
                  125*Power(t2,3)) + 
               Power(t1,2)*(598 - 318*t2 + 418*Power(t2,2) + 
                  157*Power(t2,3)) - 
               Power(s1,2)*t1*
                (4*Power(t1,5) + 5*Power(t1,4)*(-32 + t2) - 
                  8*(-5 + t2) - 2*Power(t1,2)*(491 + 7*t2) + 
                  Power(t1,3)*(842 + 36*t2) + t1*(256 + 50*t2)) + 
               s1*(-29 + 74*t2 - 41*Power(t2,2) + 
                  Power(t1,6)*(-65 + 16*t2) - 
                  2*Power(t1,5)*(-47 + 85*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*(311 - 475*t2 + 48*Power(t2,2)) + 
                  Power(t1,4)*(499 + 531*t2 + 88*Power(t2,2)) + 
                  t1*(134 + 48*t2 + 93*Power(t2,2)) - 
                  Power(t1,3)*(944 + 24*t2 + 105*Power(t2,2)))) + 
            Power(s2,4)*(29 + 11*Power(t1,4) + 
               Power(s1,3)*(-14 + 52*t1 - 21*Power(t1,2)) + 53*t2 + 
               112*Power(t2,2) + 50*Power(t2,3) - 
               Power(t1,3)*(62 + 72*t2 + 81*Power(t2,2)) + 
               Power(t1,2)*(120 + 197*t2 + 274*Power(t2,2) + 
                  57*Power(t2,3)) - 
               t1*(98 + 178*t2 + 305*Power(t2,2) + 124*Power(t2,3)) + 
               Power(s1,2)*(38 - 21*Power(t1,3) + 67*t2 + 
                  Power(t1,2)*(80 + 88*t2) - t1*(97 + 206*t2)) + 
               s1*(-37 + 22*Power(t1,4) - 134*t2 - 103*Power(t2,2) + 
                  2*Power(t1,3)*(-5 + 43*t2) - 
                  Power(t1,2)*(83 + 306*t2 + 124*Power(t2,2)) + 
                  2*t1*(54 + 177*t2 + 139*Power(t2,2)))) + 
            Power(s2,3)*(Power(s1,3)*
                (-10 + 29*t1 - 42*Power(t1,2) + 7*Power(t1,3)) + 
               Power(t1,5)*(-58 + t2) + 
               2*Power(t1,4)*(164 + 50*t2 + 39*Power(t2,2)) + 
               3*(6 - 17*t2 + 57*Power(t2,2) + 15*Power(t2,3)) - 
               Power(t1,3)*(654 + 296*t2 + 390*Power(t2,2) + 
                  51*Power(t2,3)) - 
               t1*(208 - 9*t2 + 540*Power(t2,2) + 179*Power(t2,3)) + 
               Power(t1,2)*(574 + 237*t2 + 681*Power(t2,2) + 
                  201*Power(t2,3)) + 
               2*Power(s1,2)*
                (12 + t1 - 3*Power(t1,4) + Power(t1,3)*(13 - 15*t2) + 
                  13*t2 - 62*t1*t2 + Power(t1,2)*(-23 + 88*t2)) + 
               s1*(4 - 29*Power(t1,5) - 103*t2 - 52*Power(t2,2) - 
                  Power(t1,4)*(13 + 23*t2) + 
                  5*Power(t1,3)*(50 + 25*t2 + 13*Power(t2,2)) + 
                  t1*(129 + 213*t2 + 247*Power(t2,2)) - 
                  Power(t1,2)*(341 + 212*t2 + 308*Power(t2,2))))) + 
         Power(s,5)*(12 + 2*Power(t1,8) - 54*t2 + 45*Power(t2,2) + 
            9*Power(t2,3) + Power(t1,7)*(84 - 20*s1 + 16*t2) + 
            Power(t1,6)*(-407 + 20*Power(s1,2) - 113*t2 - 
               7*s1*(-21 + 5*t2)) + 
            t1*(-36 - 11*t2 - 21*Power(t2,2) + 104*Power(t2,3) + 
               s1*(8 + 109*t2 - 145*Power(t2,2))) + 
            Power(t1,5)*(875 - 162*Power(s1,2) + 8*t2 + 
               32*Power(t2,2) + 6*Power(t2,3) + 
               s1*(-233 + 145*t2 + 15*Power(t2,2))) + 
            Power(t1,3)*(665 + 12*Power(s1,3) - 208*t2 + 
               291*Power(t2,2) + 263*Power(t2,3) - 
               4*Power(s1,2)*(14 + 17*t2) - 
               3*s1*(46 + 94*t2 + 31*Power(t2,2))) - 
            Power(t1,4)*(1053 + 10*Power(s1,3) - 352*t2 + 
               158*Power(t2,2) + 79*Power(t2,3) - 
               Power(s1,2)*(306 + 31*t2) + 
               s1*(-45 + 28*t2 + 42*Power(t2,2))) + 
            Power(t1,2)*(-142 + 10*t2 - 189*Power(t2,2) - 
               306*Power(t2,3) + 6*Power(s1,2)*(-18 + 5*t2) + 
               s1*(191 + 91*t2 + 273*Power(t2,2))) + 
            Power(s2,5)*(3*Power(s1,3) - 2*Power(s1,2)*(1 + 6*t2) + 
               s1*(1 - Power(t1,2) + 9*t2 - 5*t1*t2 + 15*Power(t2,2)) - 
               t2*(3 + Power(t1,2) + 7*t2 + 6*Power(t2,2) - 
                  t1*(4 + 5*t2))) + 
            Power(s2,4)*(-3 + Power(s1,3)*(12 - 17*t1) - 25*t2 - 
               71*Power(t2,2) - 55*Power(t2,3) + 
               Power(t1,3)*(1 + 18*t2) - 
               Power(t1,2)*(5 + 63*t2 + 42*Power(t2,2)) + 
               t1*(7 + 70*t2 + 120*Power(t2,2) + 60*Power(t2,3)) + 
               Power(s1,2)*(-6 + 17*Power(t1,2) - 67*t2 + 
                  t1*(-4 + 82*t2)) + 
               s1*(5 + 61*t2 + 110*Power(t2,2) + 
                  Power(t1,2)*(7 + 9*t2) - 
                  t1*(12 + 84*t2 + 125*Power(t2,2)))) + 
            s2*(8 - 14*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*(29 - 24*t1 + 7*Power(t1,2)) - 
               19*t2 + 137*Power(t2,2) - 145*Power(t2,3) - 
               Power(t1,6)*(187 + 69*t2) + 
               Power(t1,5)*(956 + 332*t2 + 27*Power(t2,2)) - 
               2*Power(t1,4)*
                (816 + 116*t2 + 153*Power(t2,2) + 30*Power(t2,3)) + 
               Power(t1,3)*(1274 - 214*t2 + 1011*Power(t2,2) + 
                  403*Power(t2,3)) + 
               t1*(44 + 120*t2 + 136*Power(t2,2) + 579*Power(t2,3)) - 
               Power(t1,2)*(449 - 82*t2 + 1056*Power(t2,2) + 
                  783*Power(t2,3)) + 
               Power(s1,2)*t1*
                (44 - 64*Power(t1,4) + t1*(25 - 228*t2) + 
                  Power(t1,3)*(346 - 19*t2) + 78*t2 + 
                  Power(t1,2)*(-402 + 139*t2)) + 
               s1*(-1 + 60*Power(t1,6) - 38*t2 + 41*Power(t2,2) + 
                  Power(t1,5)*(-406 + 127*t2) + 
                  t1*(7 - 500*t2 - 213*Power(t2,2)) + 
                  Power(t1,4)*(558 - 383*t2 - 13*Power(t2,2)) - 
                  Power(t1,3)*(93 + 315*t2 + 112*Power(t2,2)) + 
                  Power(t1,2)*(-125 + 1211*t2 + 321*Power(t2,2)))) + 
            Power(s2,3)*(-9 - 6*Power(t1,5) + 
               Power(s1,3)*(11 - 46*t1 + 30*Power(t1,2)) - 4*t2 - 
               200*Power(t2,2) - 160*Power(t2,3) - 
               Power(t1,4)*(38 + 63*t2) + 
               Power(t1,3)*(198 + 247*t2 + 113*Power(t2,2)) - 
               Power(t1,2)*(267 + 293*t2 + 486*Power(t2,2) + 
                  162*Power(t2,3)) + 
               t1*(122 + 113*t2 + 588*Power(t2,2) + 334*Power(t2,3)) + 
               Power(s1,2)*(-6 - 58*Power(t1,3) + 
                  Power(t1,2)*(77 - 146*t2) - 93*t2 + t1*(2 + 261*t2)) \
+ s1*(-3 + 24*Power(t1,4) + 96*t2 + 228*Power(t2,2) + 
                  Power(t1,3)*(-133 + 48*t2) + 
                  Power(t1,2)*(175 + 93*t2 + 264*Power(t2,2)) - 
                  t1*(63 + 267*t2 + 521*Power(t2,2)))) + 
            Power(s2,2)*(-16 + 18*Power(t1,6) + 
               Power(s1,3)*t1*(-40 + 72*t1 - 19*Power(t1,2)) + 73*t2 - 
               149*Power(t2,2) - 216*Power(t2,3) + 
               4*Power(t1,5)*(37 + 24*t2) - 
               Power(t1,4)*(791 + 389*t2 + 102*Power(t2,2)) + 
               Power(t1,3)*(1206 + 383*t2 + 672*Power(t2,2) + 
                  160*Power(t2,3)) - 
               Power(t1,2)*(737 - 31*t2 + 1404*Power(t2,2) + 
                  615*Power(t2,3)) + 
               t1*(172 - 194*t2 + 950*Power(t2,2) + 657*Power(t2,3)) + 
               Power(s1,2)*(89*Power(t1,4) + 
                  Power(t1,2)*(50 - 328*t2) - 38*t2 + 
                  Power(t1,3)*(-245 + 79*t2) + t1*(73 + 247*t2)) + 
               s1*(-2 - 62*Power(t1,5) + Power(t1,4)*(399 - 157*t2) + 
                  56*t2 + 162*Power(t2,2) + 
                  Power(t1,3)*(-608 + 257*t2 - 150*Power(t2,2)) - 
                  t1*(51 + 493*t2 + 610*Power(t2,2)) + 
                  Power(t1,2)*(324 + 403*t2 + 639*Power(t2,2))))) - 
         Power(s,4)*(Power(s2,6)*Power(s1 - t2,2)*(-1 + s1 + t1 - t2) + 
            Power(s2,5)*(-2*Power(s1,3)*(-5 + 6*t1) + 
               Power(s1,2)*(-10 - 4*Power(t1,2) - 39*t2 + 
                  t1*(14 + 45*t2)) + 
               t2*(-10 + 3*Power(t1,3) - 24*t2 - 19*Power(t2,2) - 
                  2*Power(t1,2)*(8 + 9*t2) + 
                  t1*(23 + 42*t2 + 21*Power(t2,2))) + 
               s1*(4 + 3*Power(t1,3) + 34*t2 + 48*Power(t2,2) + 
                  Power(t1,2)*(-2 + 22*t2) - 
                  t1*(5 + 56*t2 + 54*Power(t2,2)))) + 
            s2*(-7 + 2*Power(t1,8) + 
               Power(s1,3)*Power(t1,2)*
                (13 - 14*t1 + Power(t1,2) - 8*Power(t1,3)) + 24*t2 + 
               58*Power(t2,2) - 60*Power(t2,3) + 
               Power(t1,7)*(164 + 21*t2) - 
               Power(t1,6)*(894 + 199*t2 + 14*Power(t2,2)) + 
               Power(t1,5)*(1980 + 239*t2 + 197*Power(t2,2) + 
                  21*Power(t2,3)) - 
               Power(t1,4)*(2389 - 392*t2 + 838*Power(t2,2) + 
                  200*Power(t2,3)) + 
               t1*(164 + 110*t2 + 101*Power(t2,2) + 331*Power(t2,3)) + 
               Power(t1,3)*(1732 - 652*t2 + 1514*Power(t2,2) + 
                  555*Power(t2,3)) - 
               Power(t1,2)*(752 - 65*t2 + 1018*Power(t2,2) + 
                  639*Power(t2,3)) + 
               Power(s1,2)*t1*
                (47 + 31*Power(t1,5) + t1*(151 - 190*t2) + 
                  Power(t1,3)*(1000 - 71*t2) + 43*t2 + 
                  Power(t1,4)*(-346 + 11*t2) + 
                  Power(t1,2)*(-883 + 231*t2)) - 
               s1*(-11 + 14*Power(t1,7) + 53*t2 - 32*Power(t2,2) + 
                  3*Power(t1,6)*(-73 + 22*t2) + 
                  Power(t1,2)*(331 - 1552*t2 - 186*Power(t2,2)) + 
                  Power(t1,5)*(453 - 379*t2 - 24*Power(t2,2)) + 
                  Power(t1,4)*(101 + 307*t2 + 40*Power(t2,2)) + 
                  2*t1*(27 + 207*t2 + 62*Power(t2,2)) + 
                  Power(t1,3)*(-723 + 1091*t2 + 102*Power(t2,2)))) + 
            (-1 + t1)*(-6 + Power(t1,7)*(-53 + 4*s1 - 3*t2) - 9*t2 + 
               72*Power(t2,2) - 61*Power(t2,3) + 
               Power(t1,6)*(231 - 10*Power(s1,2) + 52*t2 + 
                  8*s1*(-7 + 2*t2)) + 
               Power(t1,2)*(466 + Power(s1,2)*(103 - 40*t2) - 584*t2 + 
                  534*Power(t2,2) + 58*Power(t2,3) + 
                  s1*(-402 + 339*t2 - 371*Power(t2,2))) + 
               Power(t1,5)*(-545 + 133*Power(s1,2) + 68*t2 - 
                  23*Power(t2,2) - Power(t2,3) - 
                  2*s1*(-33 + 53*t2 + 3*Power(t2,2))) + 
               Power(t1,4)*(895 + 20*Power(s1,3) - 570*t2 + 
                  113*Power(t2,2) + 24*Power(t2,3) - 
                  4*Power(s1,2)*(95 + 11*t2) + 
                  s1*(171 + 203*t2 + 12*Power(t2,2))) + 
               Power(t1,3)*(-907 - 26*Power(s1,3) + 676*t2 - 
                  339*Power(t2,2) - 90*Power(t2,3) + 
                  Power(s1,2)*(149 + 109*t2) + 
                  s1*(150 - 189*t2 + 149*Power(t2,2))) + 
               t1*(-81 + 370*t2 - 362*Power(t2,2) + 83*Power(t2,3) + 
                  s1*(67 - 253*t2 + 184*Power(t2,2)))) + 
            Power(s2,4)*(-21 + 
               Power(s1,3)*(19 - 56*t1 + 35*Power(t1,2)) - 43*t2 - 
               121*Power(t2,2) - 78*Power(t2,3) - 
               Power(t1,4)*(9 + 16*t2) + 
               Power(t1,3)*(48 + 122*t2 + 89*Power(t2,2)) - 
               Power(t1,2)*(90 + 239*t2 + 308*Power(t2,2) + 
                  93*Power(t2,3)) + 
               t1*(72 + 176*t2 + 340*Power(t2,2) + 173*Power(t2,3)) + 
               Power(s1,2)*(3*Power(t1,3) - 3*(7 + 33*t2) - 
                  2*Power(t1,2)*(18 + 73*t2) + t1*(54 + 251*t2)) + 
               s1*(17 - 14*Power(t1,4) + 118*t2 + 158*Power(t2,2) - 
                  2*Power(t1,3)*(3 + 34*t2) - 
                  2*t1*(34 + 161*t2 + 184*Power(t2,2)) + 
                  Power(t1,2)*(71 + 272*t2 + 204*Power(t2,2)))) + 
            Power(s2,2)*(-33 - 4*Power(t1,7) + 
               Power(s1,3)*t1*
                (-50 + 117*t1 - 80*Power(t1,2) + 14*Power(t1,3)) + 
               30*t2 - 141*Power(t2,2) - 121*Power(t2,3) - 
               Power(t1,6)*(185 + 36*t2) + 
               Power(t1,5)*(1048 + 285*t2 + 81*Power(t2,2)) - 
               Power(t1,4)*(2158 + 523*t2 + 621*Power(t2,2) + 
                  87*Power(t2,3)) + 
               Power(t1,3)*(2129 + 149*t2 + 1776*Power(t2,2) + 
                  486*Power(t2,3)) + 
               t1*(263 - 246*t2 + 1098*Power(t2,2) + 528*Power(t2,3)) - 
               Power(t1,2)*(1060 - 341*t2 + 2193*Power(t2,2) + 
                  807*Power(t2,3)) + 
               Power(s1,2)*(-2 - 51*Power(t1,5) + 
                  Power(t1,2)*(107 - 495*t2) + 
                  Power(t1,4)*(296 - 39*t2) - 23*t2 + 
                  6*t1*(11 + 41*t2) + 4*Power(t1,3)*(-104 + 77*t2)) + 
               s1*(-17 + 3*Power(t1,6) + 153*t2 + 50*Power(t2,2) + 
                  Power(t1,5)*(-256 + 90*t2) + 
                  t1*(86 - 911*t2 - 387*Power(t2,2)) + 
                  Power(t1,4)*(755 - 318*t2 + 57*Power(t2,2)) - 
                  Power(t1,3)*(702 + 161*t2 + 455*Power(t2,2)) + 
                  Power(t1,2)*(131 + 1147*t2 + 738*Power(t2,2)))) + 
            Power(s2,3)*(-22 + 2*Power(t1,6) + 
               Power(s1,3)*(15 - 88*t1 + 113*Power(t1,2) - 
                  30*Power(t1,3)) + 3*t2 - 242*Power(t2,2) - 
               133*Power(t2,3) + Power(t1,5)*(80 + 31*t2) - 
               Power(t1,4)*(452 + 267*t2 + 138*Power(t2,2)) + 
               Power(t1,3)*(878 + 604*t2 + 710*Power(t2,2) + 
                  139*Power(t2,3)) + 
               t1*(266 + 157*t2 + 988*Power(t2,2) + 453*Power(t2,3)) - 
               Power(t1,2)*(752 + 528*t2 + 1318*Power(t2,2) + 
                  469*Power(t2,3)) + 
               Power(s1,2)*(30*Power(t1,4) - 7*(5 + 11*t2) + 
                  2*Power(t1,3)*(-29 + 66*t2) + 2*t1*(71 + 195*t2) - 
                  Power(t1,2)*(79 + 475*t2)) + 
               s1*(14 + 18*Power(t1,5) + 150*t2 + 179*Power(t2,2) + 
                  3*Power(t1,4)*(34 + 3*t2) - 
                  3*Power(t1,3)*(140 + 76*t2 + 75*Power(t2,2)) - 
                  t1*(190 + 650*t2 + 707*Power(t2,2)) + 
                  Power(t1,2)*(476 + 719*t2 + 783*Power(t2,2))))))*
       T3q(s2,1 - s + s2 - t1))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*Power(-1 + s + t1,2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*(-1 + t2)) - 
    (8*((-1 + s2)*Power(s2 - t1,4)*(-1 + t1)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         Power(s,10)*(2*Power(t1,3) + Power(t1,2)*t2 + Power(t2,3)) + 
         Power(s,9)*(10*Power(t1,4) + 
            Power(t1,2)*(4 + s2*(2 - 8*t2) + s1*(-3 + 4*s2 - t2)) - 
            2*Power(t1,3)*(1 + 2*s1 + 6*s2 - 4*t2) - 
            3*Power(t2,2)*(-1 + s2 - s1*s2 + 2*t2 + 3*s2*t2) + 
            t1*t2*(3 + s2*(-1 + t2) + 6*Power(t2,2) + 
               s1*(-3 + 3*s2 + t2))) + 
         Power(s,8)*(20*Power(t1,5) + 
            Power(t1,3)*(9 + 2*Power(s1,2) + 30*Power(s2,2) + 
               s2*(9 - 54*t2) + s1*(-4 + 42*s2 - 8*t2) - 10*t2) + 
            Power(t1,4)*(1 - 20*s1 - 50*s2 + 25*t2) + 
            t2*(3*(1 - 5*t2 + 4*Power(t2,2)) + 
               s2*(-3 + s1*(3 - 11*t2) - 9*t2 + 43*Power(t2,2)) + 
               Power(s2,2)*(1 + 3*Power(s1,2) + 22*t2 + 
                  36*Power(t2,2) - 3*s1*(1 + 8*t2))) + 
            Power(t1,2)*(-14 + Power(s1,2)*(1 - 5*s2) + 8*t2 + 
               3*Power(t2,2) + 15*Power(t2,3) + 
               Power(s2,2)*(-11 + 28*t2) + 
               s2*(-9 - 3*t2 + 6*Power(t2,2)) + 
               s1*(10 - 23*Power(s2,2) - 19*t2 + 6*Power(t2,2) + 
                  s2*(2 + 27*t2))) + 
            t1*(3 + Power(s1,2)*s2*(-3 + 3*s2 - t2) - 15*t2 + 
               13*Power(t2,2) - 34*Power(t2,3) + 
               Power(s2,2)*(1 + 2*t2 - 8*Power(t2,2)) - 
               s2*(-2 + 2*t2 + 27*Power(t2,2) + 48*Power(t2,3)) + 
               s1*(-3 + Power(s2,2)*(2 - 19*t2) + 14*t2 - Power(t2,2) + 
                  s2*(3 + 11*t2 + 11*Power(t2,2))))) + 
         s*Power(s2 - t1,3)*(Power(s2,6)*Power(s1 - t2,2)*
             (-1 + s1 + t1 - t2) - 
            Power(s2,5)*(s1 - t2)*
             (-1 + Power(s1,2)*(-1 + 3*t1) - 3*t2 - Power(t2,2) - 
               Power(t1,2)*(1 + 3*t2) + t1*(2 + 6*t2 + 3*Power(t2,2)) + 
               2*s1*(2 + 2*Power(t1,2) + t2 - t1*(4 + 3*t2))) - 
            (-1 + t1)*(2*Power(-1 + s1,2)*Power(t1,5) - 
               7*Power(-1 + t2,3) + 
               t1*Power(-1 + t2,2)*(-35 + 17*s1 + 11*t2) + 
               (-1 + s1)*Power(t1,4)*
                (1 + 2*Power(s1,2) + 7*t2 - s1*(13 + 3*t2)) - 
               Power(t1,2)*(-1 + t2)*
                (52 + 11*Power(s1,2) - 40*t2 + 3*Power(t2,2) + 
                  s1*(-42 + 22*t2)) + 
               Power(t1,3)*(-25 + Power(s1,3) + 39*t2 - 
                  12*Power(t2,2) + Power(s1,2)*(-3 + 9*t2) + 
                  3*s1*(5 - 10*t2 + 2*Power(t2,2)))) + 
            s2*(Power(s1,3)*Power(t1,2)*
                (-3 - 7*t1 + 11*Power(t1,2) + Power(t1,3)) + 
               2*Power(t1,5)*(4 + t2) + 2*Power(-1 + t2,2)*(5 + 7*t2) - 
               Power(t1,4)*(24 + 23*t2 + 11*Power(t2,2)) + 
               t1*(-30 + 4*t2 + 52*Power(t2,2) - 26*Power(t2,3)) + 
               Power(t1,3)*(14 + 32*t2 + 11*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(t1,2)*(22 - 9*t2 - 34*Power(t2,2) + 
                  15*Power(t2,3)) - 
               Power(s1,2)*t1*
                (-96*Power(t1,2) + Power(t1,4)*(-3 + t2) - 
                  22*(-1 + t2) + 18*t1*t2 + Power(t1,3)*(77 + 9*t2)) - 
               s1*(17*Power(-1 + t2,2) + Power(t1,5)*(12 + t2) + 
                  Power(t1,2)*(-10 + 4*t2 - 21*Power(t2,2)) - 
                  5*t1*(9 - 10*t2 + Power(t2,2)) - 
                  Power(t1,4)*(73 + 54*t2 + 6*Power(t2,2)) + 
                  Power(t1,3)*(99 + 33*t2 + 9*Power(t2,2)))) + 
            Power(s2,2)*(1 + 
               Power(s1,3)*t1*
                (3 + 15*t1 - 22*Power(t1,2) - 3*Power(t1,3)) - 5*t2 + 
               Power(t1,5)*t2 + 22*Power(t2,2) - 18*Power(t2,3) - 
               Power(t1,4)*(5 + 18*t2) + 
               Power(t1,3)*(14 + 72*t2 + 32*Power(t2,2) + 
                  Power(t2,3)) + 
               3*Power(t1,2)*
                (-4 - 33*t2 + 2*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(2 + 49*t2 - 60*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s1,2)*(Power(t1,5) - 11*(-1 + t2) + 
                  Power(t1,4)*(-2 + 3*t2) - 6*t1*(-7 + 4*t2) + 
                  3*Power(t1,2)*(-55 + 7*t2) + 
                  Power(t1,3)*(113 + 32*t2)) - 
               s1*(-14 + 48*t2 - 34*Power(t2,2) + 
                  Power(t1,5)*(1 + t2) - Power(t1,4)*(16 + 9*t2) + 
                  15*Power(t1,2)*(-8 - 9*t2 + 2*Power(t2,2)) + 
                  t1*(74 - 56*t2 + 9*Power(t2,2)) + 
                  Power(t1,3)*(75 + 151*t2 + 16*Power(t2,2)))) + 
            Power(s2,3)*(1 + 
               Power(s1,3)*(-1 - 13*t1 + 20*Power(t1,2) + 
                  2*Power(t1,3)) - 15*t2 - 7*Power(t2,2) + 
               19*Power(t2,3) - Power(t1,4)*t2*(3 + t2) + 
               t1*(-3 + 46*t2 + 54*Power(t2,2) - 21*Power(t2,3)) + 
               Power(t1,3)*(-1 + 22*t2 + 9*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(-3 + 50*t2 + 55*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(s1,2)*(-28 + 9*Power(t1,3) - 4*Power(t1,4) + 
                  22*t2 + 6*t1*(15 + t2) - Power(t1,2)*(67 + 52*t2)) + 
               s1*(7 + 42*t2 - 40*Power(t2,2) + 
                  Power(t1,4)*(3 + 5*t2) + 
                  13*Power(t1,2)*(2 + 11*t2 + 3*Power(t2,2)) - 
                  Power(t1,3)*(14 + 25*t2 + 3*Power(t2,2)) + 
                  t1*(-22 - 165*t2 + 28*Power(t2,2)))) + 
            Power(s2,4)*(2*Power(s1,3)*(2 - 4*t1 + Power(t1,2)) + 
               Power(s1,2)*(6*Power(t1,3) - 3*(3 + 5*t2) - 
                  Power(t1,2)*(15 + 8*t2) + t1*(18 + 29*t2)) + 
               t2*(3*Power(t1,3)*(1 + t2) - 
                  2*(1 + 7*t2 + 3*Power(t2,2)) - 
                  Power(t1,2)*(8 + 14*t2 + 3*Power(t2,2)) + 
                  t1*(7 + 25*t2 + 11*Power(t2,2))) + 
               s1*(2 + 23*t2 + 17*Power(t2,2) - 
                  3*Power(t1,3)*(1 + 3*t2) + 
                  Power(t1,2)*(8 + 29*t2 + 9*Power(t2,2)) - 
                  t1*(7 + 43*t2 + 32*Power(t2,2))))) + 
         Power(s,7)*(1 + 20*Power(t1,6) + 
            Power(t1,4)*(-32 + 10*Power(s1,2) + s1*(13 - 25*t2) - 
               41*t2) - 8*Power(t1,5)*(-4 + 5*s1 - 5*t2) - 12*t2 + 
            21*Power(t2,2) - 6*Power(t2,3) + 
            t1*(-12 + 26*t2 - 55*Power(t2,2) + 60*Power(t2,3) + 
               s1*(10 - 9*t2 - 11*Power(t2,2))) + 
            Power(t1,2)*(11 - 68*t2 + 5*Power(t2,2) - 79*Power(t2,3) + 
               Power(s1,2)*(-11 + 2*t2) + 
               s1*(-1 + 91*t2 - 6*Power(t2,2))) + 
            Power(t1,3)*(-29 - 4*Power(s1,2) + 3*t2 + 16*Power(t2,2) + 
               20*Power(t2,3) + s1*(48 - 41*t2 + 15*Power(t2,2))) + 
            Power(s2,3)*(Power(s1,3) - 40*Power(t1,3) + 
               Power(t1,2)*(25 - 56*t2) - Power(s1,2)*(17*t1 + 21*t2) + 
               t1*(-5 + 9*t2 + 28*Power(t2,2)) - 
               2*t2*(4 + 35*t2 + 42*Power(t2,2)) + 
               s1*(1 + 54*Power(t1,2) + 20*t2 + 84*Power(t2,2) + 
                  t1*(-10 + 49*t2))) + 
            s2*(-80*Power(t1,5) + Power(t1,4)*(-47 + 130*s1 - 140*t2) + 
               t2*(-9 + 70*t2 - 67*Power(t2,2) + s1*(-7 + 9*t2)) + 
               Power(t1,3)*(52 - 34*Power(s1,2) + 53*t2 + 
                  15*Power(t2,2) + s1*(-36 + 101*t2)) + 
               t1*(-13 + 33*t2 + 24*Power(t2,2) + 212*Power(t2,3) + 
                  Power(s1,2)*(7 + 11*t2) + 
                  s1*(9 - 92*t2 - 56*Power(t2,2))) + 
               Power(t1,2)*(44 + Power(s1,3) + Power(s1,2)*(1 - 7*t2) + 
                  23*t2 - 116*Power(t2,2) - 105*Power(t2,3) + 
                  s1*(-48 + 57*t2 + 9*Power(t2,2)))) + 
            Power(s2,2)*(1 + 100*Power(t1,4) + 
               (22 - 12*s1 - 7*Power(s1,2))*t2 + 
               (-11 + 69*s1)*Power(t2,2) - 133*Power(t2,3) + 
               Power(t1,3)*(-7 - 144*s1 + 156*t2) + 
               Power(t1,2)*(-25 + 41*Power(s1,2) + s1*(38 - 128*t2) - 
                  10*t2 - 42*Power(t2,2)) + 
               t1*(4 - 2*Power(s1,3) - 23*t2 + 171*Power(t2,2) + 
                  168*Power(t2,3) + 3*Power(s1,2)*(3 + 8*t2) - 
                  3*s1*(6 + 10*t2 + 35*Power(t2,2))))) - 
         Power(s,2)*Power(s2 - t1,2)*
          (-2*Power(t1,8) + t1*(-10 + 7*s1 - 38*t2)*Power(-1 + t2,2) + 
            15*Power(-1 + t2,3) + 2*Power(t1,7)*(1 + s1 + 2*t2) - 
            Power(t1,6)*(-7 + 6*Power(s1,2) + s1*(9 - 7*t2) + 13*t2 + 
               2*Power(t2,2)) + 
            Power(t1,2)*(-1 + t2)*
             (-189 - 15*Power(s1,2) + s1*(111 - 43*t2) + 91*t2 + 
               46*Power(t2,2)) + 
            Power(t1,5)*(-41 - 10*Power(s1,3) + 84*t2 + 
               12*Power(t2,2) + Power(s1,2)*(75 + 16*t2) + 
               s1*(3 - 118*t2 - 3*Power(t2,2))) + 
            Power(t1,4)*(187 + 15*Power(s1,3) - 317*t2 + 
               26*Power(t2,2) + 7*Power(t2,3) - 
               2*Power(s1,2)*(39 + 28*t2) - 
               s1*(93 - 300*t2 + Power(t2,2))) - 
            Power(t1,3)*(317 + 3*Power(s1,3) + 
               Power(s1,2)*(10 - 48*t2) - 495*t2 + 106*Power(t2,2) + 
               33*Power(t2,3) - 3*s1*(67 - 107*t2 + 16*Power(t2,2))) + 
            Power(s2,6)*(6*Power(s1,3) + 
               Power(s1,2)*(-5 + 3*t1 - 21*t2) + 
               s1*(1 - Power(t1,2) + 15*t2 - 11*t1*t2 + 
                  24*Power(t2,2)) - 
               t2*(3 + Power(t1,2) + 10*t2 + 9*Power(t2,2) - 
                  4*t1*(1 + 2*t2))) + 
            s2*(12*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (9 - 53*t1 + 46*Power(t1,2) + 4*Power(t1,3)) - 
               2*Power(t1,6)*(13 + 6*t2) + 
               2*Power(-1 + t2,2)*(17 + 7*t2) + 
               Power(t1,5)*(30 + 38*t2 + 8*Power(t2,2)) + 
               t1*(-181 + 206*t2 + 27*Power(t2,2) - 52*Power(t2,3)) + 
               Power(t1,3)*(-197 + 497*t2 + 127*Power(t2,2) - 
                  16*Power(t2,3)) - 
               Power(t1,4)*(-19 + 257*t2 + 112*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(309 - 418*t2 - 65*Power(t2,2) + 
                  57*Power(t2,3)) - 
               Power(s1,2)*t1*
                (Power(t1,5) - 30*(-1 + t2) + 
                  3*Power(t1,4)*(-1 + t2) + 
                  Power(t1,3)*(314 + 64*t2) + t1*(46 + 68*t2) - 
                  Power(t1,2)*(379 + 92*t2)) + 
               s1*(-7*Power(-1 + t2,2) - Power(t1,6)*(6 + t2) + 
                  Power(t1,5)*(48 - 25*t2 + Power(t2,2)) + 
                  t1*(147 - 158*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*(-189 + 389*t2 + 16*Power(t2,2)) + 
                  Power(t1,4)*(55 + 476*t2 + 31*Power(t2,2)) - 
                  Power(t1,3)*(48 + 677*t2 + 44*Power(t2,2)))) + 
            Power(s2,5)*(-3 + Power(s1,3)*(6 - 20*t1) - 7*t2 - 
               26*Power(t2,2) - 13*Power(t2,3) + 
               4*Power(t1,3)*(1 + t2) - 
               Power(t1,2)*(11 + 22*t2 + 26*Power(t2,2)) + 
               t1*(10 + 25*t2 + 61*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s1,2)*(-15 - 13*Power(t1,2) - 22*t2 + 
                  t1*(37 + 67*t2)) + 
               s1*(2 + 4*Power(t1,3) + 37*t2 + 29*Power(t2,2) + 
                  Power(t1,2)*(1 + 35*t2) - 
                  t1*(7 + 90*t2 + 77*Power(t2,2)))) + 
            Power(s2,2)*(-(Power(s1,3)*t1*
                  (9 - 69*t1 + 84*Power(t1,2) + 10*Power(t1,3))) - 
               Power(t1,6)*(28 + t2) + 
               Power(t1,5)*(76 + 23*t2 + 2*Power(t2,2)) + 
               2*(8 + t2 - 9*Power(t2,3)) - 
               3*Power(t1,4)*
                (30 + 35*t2 + 14*Power(t2,2) + Power(t2,3)) + 
               t1*(-58 + 157*t2 + 9*Power(t2,2) + 9*Power(t2,3)) + 
               Power(t1,2)*(46 - 460*t2 - 249*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,3)*(38 + 384*t2 + 328*Power(t2,2) + 
                  31*Power(t2,3)) + 
               Power(s1,2)*(7*Power(t1,5) + t1*(122 - 8*t2) - 
                  15*(-1 + t2) + 5*Power(t1,4)*(1 + t2) - 
                  7*Power(t1,2)*(80 + 7*t2) + 
                  Power(t1,3)*(459 + 166*t2)) + 
               s1*(-Power(t1,6) + Power(t1,5)*(5 + t2) + 
                  Power(t1,2)*(199 + 913*t2 - 41*Power(t2,2)) + 
                  2*Power(t1,4)*(-35 + 31*t2 + Power(t2,2)) + 
                  4*(-9 + t2 + 8*Power(t2,2)) + 
                  t1*(-38 - 189*t2 + 11*Power(t2,2)) - 
                  Power(t1,3)*(59 + 887*t2 + 100*Power(t2,2)))) + 
            Power(s2,4)*(1 + Power(s1,3)*(8 - 34*t1 + 20*Power(t1,2)) - 
               16*t2 - 83*Power(t2,2) - 22*Power(t2,3) - 
               6*Power(t1,4)*(3 + t2) + 
               Power(t1,3)*(52 + 43*t2 + 30*Power(t2,2)) - 
               Power(t1,2)*(49 + 69*t2 + 144*Power(t2,2) + 
                  36*Power(t2,3)) + 
               t1*(14 + 48*t2 + 203*Power(t2,2) + 71*Power(t2,3)) + 
               Power(s1,2)*(-36 + 22*Power(t1,3) - 33*t2 - 
                  6*Power(t1,2)*(11 + 12*t2) + 2*t1*(43 + 65*t2)) + 
               s1*(1 - 6*Power(t1,4) + 105*t2 + 45*Power(t2,2) - 
                  Power(t1,3)*(3 + 38*t2) + 
                  2*Power(t1,2)*(5 + 84*t2 + 43*Power(t2,2)) - 
                  t1*(2 + 247*t2 + 163*Power(t2,2)))) + 
            Power(s2,3)*(Power(s1,3)*(3 - 39*t1 + 76*Power(t1,2)) + 
               4*Power(t1,5)*(8 + t2) + 
               3*t2*(-12 - 12*t2 + 11*Power(t2,2)) - 
               Power(t1,4)*(93 + 40*t2 + 14*Power(t2,2)) + 
               t1*(2 + 179*t2 + 296*Power(t2,2) - 20*Power(t2,3)) + 
               Power(t1,3)*(92 + 127*t2 + 129*Power(t2,2) + 
                  18*Power(t2,3)) - 
               Power(t1,2)*(33 + 234*t2 + 423*Power(t2,2) + 
                  79*Power(t2,3)) + 
               Power(s1,2)*(-66 - 18*Power(t1,4) + 28*t2 + 
                  8*Power(t1,3)*(4 + 3*t2) + t1*(295 + 46*t2) - 
                  Power(t1,2)*(291 + 226*t2)) + 
               s1*(26 + 4*Power(t1,5) + 121*t2 - 75*Power(t2,2) + 
                  Power(t1,4)*(1 + 14*t2) + 
                  Power(t1,3)*(27 - 137*t2 - 36*Power(t2,2)) + 
                  t1*(-59 - 641*t2 + 41*Power(t2,2)) + 
                  Power(t1,2)*(1 + 739*t2 + 206*Power(t2,2))))) + 
         Power(s,6)*(-3 + 10*Power(t1,7) + 9*t2 + 3*Power(t2,2) - 
            9*Power(t2,3) + Power(t1,6)*(80 - 40*s1 + 35*t2) + 
            t1*(7 + s1 - 6*t2 - 34*s1*t2 + 35*Power(t2,2) + 
               29*s1*Power(t2,2) - 17*Power(t2,3)) + 
            2*Power(t1,5)*(10*Power(s1,2) - 34*(2 + t2) - 
               5*s1*(-3 + 4*t2)) + 
            Power(t1,4)*(25 - 28*Power(s1,2) - 25*t2 + 36*Power(t2,2) + 
               15*Power(t2,3) + s1*(126 - 41*t2 + 20*Power(t2,2))) + 
            Power(t1,2)*(16 + Power(s1,2)*(22 - 6*t2) + 50*t2 - 
               30*Power(t2,2) + 119*Power(t2,3) - 
               s1*(27 + 40*t2 + 60*Power(t2,2))) + 
            Power(t1,3)*(2*Power(s1,3) + Power(s1,2)*(-45 + 4*t2) + 
               s1*(-14 + 258*t2 - 11*Power(t2,2)) - 
               2*(3 + 67*t2 + 33*Power(t2,2) + 48*Power(t2,3))) + 
            Power(s2,4)*(-6*Power(s1,3) + 30*Power(t1,3) + 
               10*Power(t1,2)*(-3 + 7*t2) + 
               Power(s1,2)*(1 + 39*t1 + 63*t2) - 
               2*t1*(-5 + 20*t2 + 28*Power(t2,2)) + 
               t2*(25 + 126*t2 + 126*Power(t2,2)) - 
               s1*(5 + 65*Power(t1,2) + 57*t2 + 168*Power(t2,2) + 
                  t1*(-20 + 63*t2))) - 
            Power(s2,2)*(-4 - 120*Power(t1,5) + 
               2*Power(s1,3)*t1*(-4 + 7*t1) + 5*t2 + 103*Power(t2,2) - 
               156*Power(t2,3) - 5*Power(t1,4)*(42 + 65*t2) + 
               Power(t1,3)*(375 + 179*t2 + 90*Power(t2,2)) - 
               Power(t1,2)*(47 - 121*t2 + 570*Power(t2,2) + 
                  315*Power(t2,3)) + 
               t1*(-19 - 32*t2 + 345*Power(t2,2) + 559*Power(t2,3)) + 
               Power(s1,2)*(-149*Power(t1,3) + 
                  Power(t1,2)*(39 - 79*t2) - 4*t2 + t1*(37 + 96*t2)) + 
               s1*(1 + 305*Power(t1,4) - 14*t2 + 43*Power(t2,2) + 
                  Power(t1,3)*(-173 + 349*t2) + 
                  t1*(30 - 299*t2 - 321*Power(t2,2)) + 
                  3*Power(t1,2)*(-35 + 31*t2 + 60*Power(t2,2)))) + 
            s2*(-6 + 4*Power(s1,3)*(-2 + t1)*Power(t1,2) - 
               60*Power(t1,6) + 47*t2 - 87*Power(t2,2) + 
               27*Power(t2,3) - 18*Power(t1,5)*(13 + 10*t2) + 
               Power(t1,4)*(381 + 190*t2 + 20*Power(t2,2)) + 
               t1*(26 - 102*t2 + 145*Power(t2,2) - 276*Power(t2,3)) - 
               2*Power(t1,3)*
                (11 - 38*t2 + 131*Power(t2,2) + 60*Power(t2,3)) + 
               Power(t1,2)*(-28 + 187*t2 + 315*Power(t2,2) + 
                  418*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-85*Power(t1,3) + Power(t1,2)*(75 - 19*t2) - 
                  4*(1 + 4*t2) + t1*(81 + 56*t2)) + 
               s1*(180*Power(t1,5) + Power(1 + t2,2) + 
                  3*Power(t1,4)*(-38 + 65*t2) + 
                  Power(t1,2)*(45 - 586*t2 - 111*Power(t2,2)) + 
                  Power(t1,3)*(-277 + 89*t2 - 15*Power(t2,2)) + 
                  t1*(-38 + 118*t2 + 77*Power(t2,2)))) + 
            Power(s2,3)*(-1 - 100*Power(t1,4) + 
               2*Power(s1,3)*(-1 + 8*t1) - 55*t2 + 88*Power(t2,2) + 
               231*Power(t2,3) - 2*Power(t1,3)*(13 + 125*t2) + 
               Power(t1,2)*(121 + 96*t2 + 126*Power(t2,2)) - 
               t1*(45 - 29*t2 + 459*Power(t2,2) + 336*Power(t2,3)) - 
               Power(s1,2)*(1 + 123*Power(t1,2) - 38*t2 + 
                  3*t1*(3 + 41*t2)) + 
               s1*(3 + 230*Power(t1,3) + 9*t2 - 183*Power(t2,2) + 
                  Power(t1,2)*(-109 + 257*t2) + 
                  t1*(43 + 110*t2 + 343*Power(t2,2))))) + 
         Power(s,3)*(s2 - t1)*
          (-16*Power(t1,8) + 13*Power(-1 + t2,3) - 
            t1*Power(-1 + t2,2)*(-94 + 27*s1 + 82*t2) + 
            Power(t1,7)*(32 - 2*Power(s1,2) + 17*t2 + 3*s1*(3 + t2)) + 
            Power(t1,2)*(-1 + t2)*
             (6 - 5*Power(s1,2) - 205*t2 + 135*Power(t2,2) + 
               15*s1*(-1 + 5*t2)) + 
            Power(t1,3)*(-335 - 5*Power(s1,3) + 409*t2 + 
               114*Power(t2,2) - 88*Power(t2,3) + 
               6*Power(s1,2)*(1 + 7*t2) + 
               s1*(186 - 239*t2 - 71*Power(t2,2))) + 
            Power(t1,5)*(-108 - 20*Power(s1,3) + 206*t2 + 
               69*Power(t2,2) + 3*Power(t2,3) + 
               Power(s1,2)*(135 + 32*t2) + 
               s1*(146 - 363*t2 - 10*Power(t2,2))) + 
            Power(t1,6)*(5 + 4*Power(s1,2) - 23*t2 - 12*Power(t2,2) - 
               s1*(93 - 23*t2 + Power(t2,2))) + 
            Power(t1,4)*(347 + 29*Power(s1,3) - 603*t2 - 
               69*Power(t2,2) + 10*Power(t2,3) - 
               2*Power(s1,2)*(77 + 45*t2) + 
               s1*(-222 + 637*t2 + 60*Power(t2,2))) + 
            Power(s2,6)*(15*Power(s1,3) + Power(t1,2)*(1 - 8*t2) - 
               Power(s1,2)*(10 + 3*t1 + 63*t2) - 
               2*t2*(8 + 21*t2 + 18*Power(t2,2)) + 
               t1*(-1 + 23*t2 + 28*Power(t2,2)) + 
               s1*(5 - 2*Power(t1,2) + 48*t2 + 84*Power(t2,2) - 
                  t1*(2 + 21*t2))) + 
            Power(s2,2)*(39 - 
               Power(s1,3)*t1*
                (15 - 117*t1 + 176*Power(t1,2) + 5*Power(t1,3)) - 
               11*t2 - 31*Power(t2,2) + 3*Power(t2,3) - 
               2*Power(t1,6)*(103 + 12*t2) + 
               Power(t1,5)*(524 + 140*t2 + 17*Power(t2,2)) + 
               t1*(-128 + 247*t2 + 167*Power(t2,2) + 14*Power(t2,3)) - 
               Power(t1,4)*(449 + 200*t2 + 293*Power(t2,2) + 
                  33*Power(t2,3)) - 
               Power(t1,2)*(-160 + 896*t2 + 879*Power(t2,2) + 
                  134*Power(t2,3)) + 
               Power(t1,3)*(60 + 827*t2 + 1245*Power(t2,2) + 
                  216*Power(t2,3)) + 
               Power(s1,2)*(5 - 15*Power(t1,5) + 
                  Power(t1,4)*(118 - 27*t2) - 5*t2 + 18*t1*(7 + t2) - 
                  3*Power(t1,2)*(270 + 71*t2) + 
                  Power(t1,3)*(841 + 438*t2)) + 
               s1*(6*Power(t1,6) + 5*Power(t1,5)*(5 + 14*t2) + 
                  Power(t1,3)*(395 - 2365*t2 - 307*Power(t2,2)) + 
                  12*(-4 + 3*t2 + Power(t2,2)) + 
                  Power(t1,4)*(-569 + 138*t2 + 17*Power(t2,2)) + 
                  t1*(99 - 507*t2 + 36*Power(t2,2)) + 
                  Power(t1,2)*(9 + 2137*t2 + 44*Power(t2,2)))) + 
            s2*(Power(s1,3)*Power(t1,2)*
                (15 - 97*t1 + 94*Power(t1,2) + 5*Power(t1,3)) + 
               Power(t1,7)*(91 + 4*t2) + 
               2*Power(-1 + t2,2)*(-19 + 13*t2) - 
               Power(t1,6)*(217 + 66*t2 + Power(t2,2)) + 
               Power(t1,5)*(159 + 72*t2 + 70*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,3)*(-493 + 1154*t2 + 509*Power(t2,2) + 
                  43*Power(t2,3)) - 
               Power(t1,4)*(-129 + 671*t2 + 474*Power(t2,2) + 
                  49*Power(t2,3)) + 
               Power(t1,2)*(454 - 563*t2 - 265*Power(t2,2) + 
                  74*Power(t2,3)) - 
               t1*(85 + 44*t2 - 215*Power(t2,2) + 86*Power(t2,3)) + 
               Power(s1,2)*t1*
                (8*Power(t1,5) + Power(t1,4)*(-59 + t2) + 
                  10*(-1 + t2) - 72*t1*(1 + t2) - 
                  3*Power(t1,3)*(187 + 57*t2) + 
                  Power(t1,2)*(609 + 220*t2)) + 
               s1*(-2*Power(t1,7) + 27*Power(-1 + t2,2) - 
                  3*Power(t1,6)*(9 + 8*t2) + 
                  Power(t1,3)*(248 - 1779*t2 - 149*Power(t2,2)) + 
                  t1*(33 + 54*t2 - 87*Power(t2,2)) + 
                  Power(t1,5)*(391 - 72*t2 + 8*Power(t2,2)) + 
                  3*Power(t1,2)*(-102 + 189*t2 + 37*Power(t2,2)) + 
                  Power(t1,4)*(-352 + 1429*t2 + 74*Power(t2,2)))) - 
            Power(s2,3)*(7 + 
               Power(s1,3)*(-5 + 59*t1 - 164*Power(t1,2) + 
                  30*Power(t1,3)) + 45*t2 + 64*Power(t2,2) - 
               16*Power(t2,3) - 7*Power(t1,5)*(33 + 8*t2) + 
               Power(t1,4)*(593 + 213*t2 + 73*Power(t2,2)) - 
               Power(t1,3)*(482 + 271*t2 + 625*Power(t2,2) + 
                  117*Power(t2,3)) - 
               t1*(12 + 280*t2 + 708*Power(t2,2) + 119*Power(t2,3)) + 
               Power(t1,2)*(125 + 419*t2 + 1426*Power(t2,2) + 
                  384*Power(t2,3)) + 
               Power(s1,2)*(-20*Power(t1,4) - 12*(-5 + t2) - 
                  6*Power(t1,3)*(-5 + 23*t2) - t1*(409 + 126*t2) + 
                  Power(t1,2)*(559 + 556*t2)) + 
               s1*(-21 + 4*Power(t1,5) - 179*t2 + 76*Power(t2,2) + 
                  Power(t1,4)*(1 + 75*t2) + 
                  Power(t1,2)*(249 - 1829*t2 - 615*Power(t2,2)) + 
                  t1*(27 + 1179*t2 + 34*Power(t2,2)) + 
                  Power(t1,3)*(-330 + 304*t2 + 173*Power(t2,2)))) + 
            Power(s2,5)*(-11 + Power(s1,3)*(14 - 55*t1) - 8*t2 - 
               91*Power(t2,2) - 63*Power(t2,3) + 
               Power(t1,3)*(26 + 36*t2) + 
               Power(s1,2)*(-20 + 65*t1 + 12*Power(t1,2) - 65*t2 + 
                  213*t1*t2) - 
               Power(t1,2)*(66 + 109*t2 + 98*Power(t2,2)) + 
               3*t1*(17 + 29*t2 + 85*Power(t2,2) + 44*Power(t2,3)) + 
               s1*(-3 + 6*Power(t1,3) + 78*t2 + 111*Power(t2,2) + 
                  Power(t1,2)*(8 + 47*t2) - 
                  t1*(17 + 248*t2 + 287*Power(t2,2)))) + 
            Power(s2,4)*(3 + 2*Power(s1,3)*
                (5 - 38*t1 + 35*Power(t1,2)) - 22*t2 - 182*Power(t2,2) - 
               67*Power(t2,3) - Power(t1,4)*(127 + 64*t2) + 
               Power(t1,3)*(321 + 208*t2 + 127*Power(t2,2)) - 
               Power(t1,2)*(248 + 191*t2 + 603*Power(t2,2) + 
                  183*Power(t2,3)) + 
               t1*(51 + 77*t2 + 665*Power(t2,2) + 281*Power(t2,3)) - 
               Power(s1,2)*(54 + 20*Power(t1,3) + 43*t2 - 
                  2*t1*(82 + 161*t2) + Power(t1,2)*(88 + 262*t2)) + 
               s1*(-8 - 12*Power(t1,3) - 4*Power(t1,4) + 184*t2 + 
                  79*Power(t2,2) + t1*(63 - 608*t2 - 483*Power(t2,2)) + 
                  Power(t1,2)*(-47 + 415*t2 + 352*Power(t2,2))))) + 
         Power(s,5)*(2*Power(t1,8) + 12*Power(-1 + t2,2)*t2 + 
            Power(t1,7)*(90 - 20*s1 + 16*t2) + 
            Power(t1,6)*(-188 + 15*s1 + 20*Power(s1,2) - 62*t2 - 
               35*s1*t2) - t1*(-1 + t2)*
             (21 - 47*t2 + 53*Power(t2,2) + s1*(-22 + 26*t2)) + 
            Power(t1,4)*(-16 + 10*Power(s1,3) - 183*t2 - 
               155*Power(t2,2) - 64*Power(t2,3) - 
               Power(s1,2)*(93 + 7*t2) + 
               s1*(-123 + 456*t2 - 4*Power(t2,2))) + 
            Power(t1,5)*(96 - 50*Power(s1,2) - 45*t2 + 44*Power(t2,2) + 
               6*Power(t2,3) + s1*(211 - 29*t2 + 15*Power(t2,2))) + 
            Power(t1,3)*(23 - 6*Power(s1,3) + 149*t2 + 
               130*Power(t2,2) + 110*Power(t2,3) + 
               2*Power(s1,2)*(45 + t2) - 
               s1*(57 + 210*t2 + 131*Power(t2,2))) + 
            Power(t1,2)*(Power(s1,2)*(-8 + 6*t2) + 
               3*s1*(25 - 67*t2 + 47*Power(t2,2)) - 
               3*(29 - 68*t2 + 40*Power(t2,2) + Power(t2,3))) + 
            Power(s2,5)*(15*Power(s1,3) - 12*Power(t1,3) + 
               Power(t1,2)*(20 - 56*t2) - 
               5*Power(s1,2)*(1 + 9*t1 + 21*t2) + 
               5*t1*(-2 + 13*t2 + 14*Power(t2,2)) - 
               2*t2*(20 + 70*t2 + 63*Power(t2,2)) + 
               5*s1*(2 + 8*Power(t1,2) + 18*t2 + 42*Power(t2,2) + 
                  t1*(-4 + 7*t2))) - 
            Power(s2,2)*(-5 - 60*Power(t1,6) + 
               Power(s1,3)*t1*(10 - 69*t1 + 30*Power(t1,2)) + 76*t2 - 
               110*Power(t2,2) + 45*Power(t2,3) - 
               2*Power(t1,5)*(293 + 160*t2) + 
               Power(t1,4)*(1164 + 427*t2 + 100*Power(t2,2)) - 
               Power(t1,3)*(461 - 123*t2 + 958*Power(t2,2) + 
                  300*Power(t2,3)) - 
               t1*(-4 + 162*t2 + 105*Power(t2,2) + 501*Power(t2,3)) + 
               Power(t1,2)*(16 + 214*t2 + 1431*Power(t2,2) + 
                  909*Power(t2,3)) + 
               Power(s1,2)*(-225*Power(t1,4) + 2*t2 - 
                  5*Power(t1,3)*(-49 + 26*t2) - 2*t1*(32 + 41*t2) + 
                  Power(t1,2)*(283 + 352*t2)) + 
               s1*(8 + 280*Power(t1,5) - 35*t2 + 12*Power(t2,2) + 
                  Power(t1,4)*(-229 + 475*t2) + 
                  Power(t1,2)*(260 - 1651*t2 - 588*Power(t2,2)) + 
                  Power(t1,3)*(-655 + 116*t2 + 150*Power(t2,2)) + 
                  t1*(-65 + 524*t2 + 181*Power(t2,2)))) + 
            s2*(8 - 20*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*(14 - 45*t1 + 5*Power(t1,2)) - 
               16*t2 - 19*Power(t2,2) + 27*Power(t2,3) - 
               8*Power(t1,6)*(47 + 15*t2) + 
               Power(t1,5)*(759 + 257*t2 + 15*Power(t2,2)) + 
               2*t1*(5 + 14*t2 - 43*Power(t2,2) + 30*Power(t2,3)) - 
               Power(t1,4)*(332 - 102*t2 + 327*Power(t2,2) + 
                  75*Power(t2,3)) + 
               Power(t1,3)*(19 + 470*t2 + 826*Power(t2,2) + 
                  416*Power(t2,3)) - 
               Power(t1,2)*(20 + 338*t2 + 213*Power(t2,2) + 
                  429*Power(t2,3)) + 
               Power(s1,2)*t1*
                (8 - 100*Power(t1,4) + Power(t1,3)*(201 - 25*t2) - 
                  4*t2 - t1*(145 + 66*t2) + Power(t1,2)*(281 + 144*t2)) \
+ s1*(2 + 120*Power(t1,6) - 8*t2 + 6*Power(t2,2) + 
                  Power(t1,5)*(-96 + 205*t2) + 
                  t1*(-17 + 66*t2 - 79*Power(t2,2)) + 
                  Power(t1,4)*(-678 + 71*t2 - 35*Power(t2,2)) - 
                  2*Power(t1,3)*(-145 + 752*t2 + 63*Power(t2,2)) + 
                  Power(t1,2)*(-6 + 698*t2 + 225*Power(t2,2)))) + 
            Power(s2,3)*(-7 - 80*Power(t1,5) + 
               Power(s1,3)*(2 - 43*t1 + 60*Power(t1,2)) + 29*t2 - 
               2*Power(t2,2) - 196*Power(t2,3) - 
               16*Power(t1,4)*(24 + 25*t2) + 
               Power(t1,3)*(797 + 392*t2 + 225*Power(t2,2)) - 
               Power(t1,2)*(331 - 65*t2 + 1215*Power(t2,2) + 
                  525*Power(t2,3)) + 
               t1*(11 - 112*t2 + 914*Power(t2,2) + 806*Power(t2,3)) + 
               Power(s1,2)*(-275*Power(t1,3) + 
                  Power(t1,2)*(82 - 290*t2) - 9*(1 + 2*t2) + 
                  5*t1*(19 + 60*t2)) + 
               s1*(-2 + 320*Power(t1,4) + 36*t2 + 87*Power(t2,2) + 
                  10*Power(t1,3)*(-25 + 51*t2) + 
                  t1*(93 - 614*t2 - 733*Power(t2,2)) + 
                  Power(t1,2)*(-144 + 259*t2 + 555*Power(t2,2)))) + 
            Power(s2,4)*(-6 + Power(s1,3)*(9 - 50*t1) + 50*Power(t1,4) + 
               59*t2 - 170*Power(t2,2) - 245*Power(t2,3) + 
               16*Power(t1,3)*(4 + 15*t2) - 
               Power(t1,2)*(194 + 225*t2 + 210*Power(t2,2)) + 
               t1*(101 + 51*t2 + 675*Power(t2,2) + 420*Power(t2,3)) + 
               Power(s1,2)*(175*Power(t1,2) - 85*t2 + 
                  t1*(17 + 290*t2)) - 
               s1*(10 + 180*Power(t1,3) - 31*t2 - 265*Power(t2,2) + 
                  2*Power(t1,2)*(-61 + 120*t2) + 
                  t1*(54 + 275*t2 + 595*Power(t2,2))))) + 
         Power(s,4)*(-4*Power(-1 + t2,3) + 
            Power(t1,8)*(53 - 4*s1 + 3*t2) + 
            t1*Power(-1 + t2,2)*(-30 + 8*s1 + 51*t2) + 
            Power(t1,7)*(-119 + 10*Power(s1,2) - 38*t2 - 
               8*s1*(1 + 2*t2)) - 
            Power(t1,2)*(-1 + t2)*
             (150 + 2*Power(s1,2) - 254*t2 + 119*Power(t2,2) + 
               2*s1*(-49 + 54*t2)) + 
            Power(t1,4)*(-188 - 22*Power(s1,3) + 454*t2 + 
               203*Power(t2,2) + 36*Power(t2,3) + 
               3*Power(s1,2)*(53 + 18*t2) + 
               s1*(107 - 556*t2 - 137*Power(t2,2))) + 
            Power(t1,6)*(60 - 35*Power(s1,2) - 12*t2 + 31*Power(t2,2) + 
               Power(t2,3) + s1*(203 - 29*t2 + 6*Power(t2,2))) + 
            Power(t1,5)*(62 + 20*Power(s1,3) - 231*t2 - 
               151*Power(t2,2) - 22*Power(t2,3) - 
               4*Power(s1,2)*(34 + 7*t2) + 
               s1*(-232 + 530*t2 + 9*Power(t2,2))) + 
            Power(t1,3)*(-2 + 2*Power(s1,3) + 156*t2 - 299*Power(t2,2) + 
               60*Power(t2,3) - Power(s1,2)*(16 + 5*t2) + 
               s1*(41 - 161*t2 + 203*Power(t2,2))) + 
            Power(s2,6)*(-20*Power(s1,3) + 2*Power(t1,3) + 
               7*Power(t1,2)*(-1 + 4*t2) + 
               5*Power(s1,2)*(2 + 5*t1 + 21*t2) + 
               t1*(5 - 54*t2 - 56*Power(t2,2)) + 
               7*t2*(5 + 14*t2 + 12*Power(t2,2)) - 
               s1*(10 + 9*Power(t1,2) + 85*t2 + 168*Power(t2,2) - 
                  t1*(10 + 7*t2))) - 
            Power(s2,2)*(5 - 10*Power(t1,7) + 
               Power(s1,3)*t1*
                (-6 + 87*t1 - 199*Power(t1,2) + 20*Power(t1,3)) + 2*t2 - 
               34*Power(t2,2) + 27*Power(t2,3) - 
               Power(t1,6)*(623 + 150*t2) + 
               2*Power(t1,5)*(713 + 208*t2 + 30*Power(t2,2)) - 
               9*Power(t1,2)*
                (-10 + 96*t2 + 121*Power(t2,2) + 61*Power(t2,3)) + 
               t1*(-47 + 170*t2 + 66*Power(t2,2) + 66*Power(t2,3)) - 
               2*Power(t1,4)*
                (459 + 38*t2 + 420*Power(t2,2) + 75*Power(t2,3)) + 
               Power(t1,3)*(85 + 923*t2 + 2317*Power(t2,2) + 
                  710*Power(t2,3)) + 
               Power(s1,2)*(-145*Power(t1,5) + 2*(-1 + t2) + 
                  9*t1*(6 + t2) - 45*Power(t1,2)*(13 + 6*t2) - 
                  7*Power(t1,4)*(-52 + 15*t2) + 
                  Power(t1,3)*(863 + 627*t2)) + 
               s1*(1 + 105*Power(t1,6) - 12*t2 + 11*Power(t2,2) + 
                  5*Power(t1,5)*(-12 + 65*t2) + 
                  Power(t1,3)*(810 - 3514*t2 - 586*Power(t2,2)) + 
                  t1*(62 - 287*t2 - 24*Power(t2,2)) + 
                  Power(t1,4)*(-1253 + 153*t2 + 60*Power(t2,2)) + 
                  3*Power(t1,2)*(-44 + 775*t2 + 91*Power(t2,2)))) + 
            s2*(-2*Power(t1,8) + 
               Power(s1,3)*Power(t1,2)*(-6 + 73*t1 - 101*Power(t1,2)) - 
               Power(-1 + t2,2)*(-13 + 34*t2) - 
               Power(t1,7)*(287 + 38*t2) + 
               Power(t1,6)*(658 + 185*t2 + 6*Power(t2,2)) - 
               Power(t1,2)*(59 + 27*t2 - 353*Power(t2,2) + 
                  12*Power(t2,3)) - 
               Power(t1,5)*(420 - 18*t2 + 227*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(-117 + 322*t2 - 323*Power(t2,2) + 118*Power(t2,3)) + 
               Power(t1,4)*(-70 + 815*t2 + 962*Power(t2,2) + 
                  215*Power(t2,3)) - 
               Power(t1,3)*(-285 + 1055*t2 + 766*Power(t2,2) + 
                  278*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-55*Power(t1,5) + Power(t1,4)*(199 - 15*t2) + 
                  4*(-1 + t2) + 3*t1*(17 + 4*t2) - 
                  9*Power(t1,2)*(57 + 22*t2) + 
                  4*Power(t1,3)*(141 + 56*t2)) + 
               s1*(34*Power(t1,7) - 8*Power(-1 + t2,2) + 
                  Power(t1,6)*(6 + 113*t2) + 
                  Power(t1,2)*(16 + 5*t2 - 270*Power(t2,2)) + 
                  Power(t1,4)*(677 - 2177*t2 - 113*Power(t2,2)) + 
                  Power(t1,5)*(-846 + 85*t2 - 27*Power(t2,2)) + 
                  t1*(99 - 218*t2 + 119*Power(t2,2)) + 
                  Power(t1,3)*(-198 + 1877*t2 + 315*Power(t2,2)))) + 
            Power(s2,3)*(6 - 20*Power(t1,6) + 
               Power(s1,3)*(-2 + 43*t1 - 191*Power(t1,2) + 
                  80*Power(t1,3)) + 65*t2 - 12*Power(t2,2) + 
               26*Power(t2,3) - 10*Power(t1,5)*(67 + 28*t2) + 
               Power(t1,4)*(1521 + 566*t2 + 200*Power(t2,2)) - 
               Power(t1,3)*(939 + 180*t2 + 1532*Power(t2,2) + 
                  400*Power(t2,3)) - 
               t1*(16 + 229*t2 + 713*Power(t2,2) + 444*Power(t2,3)) + 
               Power(t1,2)*(142 + 328*t2 + 2483*Power(t2,2) + 
                  1040*Power(t2,3)) + 
               Power(s1,2)*(19 - 230*Power(t1,4) + 
                  Power(t1,3)*(238 - 330*t2) + 2*t2 - 
                  3*t1*(89 + 54*t2) + Power(t1,2)*(588 + 794*t2)) + 
               s1*(5 + 160*Power(t1,5) - 131*t2 + 43*Power(t2,2) + 
                  2*Power(t1,4)*(-73 + 225*t2) + 
                  Power(t1,2)*(485 - 2610*t2 - 1137*Power(t2,2)) + 
                  t1*(-51 + 1149*t2 + 196*Power(t2,2)) + 
                  Power(t1,3)*(-759 + 380*t2 + 450*Power(t2,2)))) + 
            Power(s2,5)*(14 - 10*Power(t1,4) + 
               16*Power(s1,3)*(-1 + 5*t1) - 21*t2 + 167*Power(t2,2) + 
               161*Power(t2,3) - Power(t1,3)*(59 + 138*t2) + 
               Power(t1,2)*(157 + 245*t2 + 210*Power(t2,2)) - 
               t1*(103 + 132*t2 + 585*Power(t2,2) + 336*Power(t2,3)) + 
               Power(s1,2)*(10 - 115*Power(t1,2) + 100*t2 - 
                  t1*(53 + 375*t2)) + 
               s1*(11 + 54*Power(t1,3) - 79*t2 - 225*Power(t2,2) + 
                  Power(t1,2)*(-60 + 61*t2) + 
                  t1*(41 + 383*t2 + 609*Power(t2,2)))) + 
            Power(s2,4)*(1 + 20*Power(t1,5) + 
               Power(s1,3)*(-7 + 89*t1 - 120*Power(t1,2)) - 10*t2 + 
               163*Power(t2,2) + 145*Power(t2,3) + 
               Power(t1,4)*(347 + 275*t2) - 
               4*Power(t1,3)*(199 + 122*t2 + 75*Power(t2,2)) + 
               Power(t1,2)*(484 + 195*t2 + 1375*Power(t2,2) + 
                  525*Power(t2,3)) - 
               t1*(62 - 29*t2 + 1141*Power(t2,2) + 685*Power(t2,3)) + 
               Power(s1,2)*(220*Power(t1,3) + 36*(1 + t2) - 
                  t1*(163 + 463*t2) + Power(t1,2)*(5 + 510*t2)) - 
               s1*(-10 + 130*Power(t1,4) + 145*t2 + 101*Power(t2,2) + 
                  2*Power(t1,3)*(-69 + 145*t2) + 
                  t1*(131 - 822*t2 - 880*Power(t2,2)) + 
                  Power(t1,2)*(-118 + 581*t2 + 810*Power(t2,2))))))*
       T4q(1 - s + s2 - t1))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*Power(-s + s2 - t1,2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*(-1 + t2)) + 
    (8*(Power(s,7)*(2*Power(t1,3) + Power(t1,2)*t2 + Power(t2,3)) + 
         Power(s,6)*(4*Power(t1,4) - 
            Power(t1,3)*(2 + 4*s1 + 6*s2 - 5*t2) + 
            Power(t1,2)*(4 + 2*s2 + s1*(-3 + 4*s2 - t2) - 5*s2*t2) - 
            3*Power(t2,2)*(-1 + s2 - s1*s2 + 2*t2 + 2*s2*t2) + 
            t1*t2*(3 - s2 + s2*t2 + 3*Power(t2,2) + s1*(-3 + 3*s2 + t2))) \
+ Power(s,5)*(2*Power(t1,5) + Power(t1,4)*(7 - 8*s1 - 8*s2 + 7*t2) + 
            Power(t1,3)*(-3 + 2*Power(s1,2) + 6*Power(s2,2) + 
               s1*(5 + 18*s2 - 5*t2) - 2*t2 - 3*s2*(1 + 6*t2)) + 
            t2*(3*(1 - 5*t2 + 4*Power(t2,2)) + 
               s2*(-3 + s1*(3 - 11*t2) + 25*Power(t2,2)) + 
               Power(s2,2)*(1 + 3*Power(s1,2) + 13*t2 + 15*Power(t2,2) - 
                  3*s1*(1 + 5*t2))) + 
            Power(t1,2)*(-14 + Power(s1,2)*(1 - 5*s2) - t2 + 
               3*Power(t2,2) + 3*Power(t2,3) + 
               5*Power(s2,2)*(-1 + 2*t2) + 3*s2*(1 + Power(t2,2)) + 
               s1*(10 - 11*Power(s2,2) - 10*t2 + 3*Power(t2,2) + 
                  s2*(-7 + 15*t2))) + 
            t1*(3 + Power(s1,2)*s2*(-3 + 3*s2 - t2) - 15*t2 + 
               4*Power(t2,2) - 16*Power(t2,3) - 
               Power(s2,2)*(-1 + t2 + 5*Power(t2,2)) + 
               s2*(2 + 7*t2 - 18*Power(t2,2) - 15*Power(t2,3)) + 
               s1*(-3 + Power(s2,2)*(2 - 10*t2) + 14*t2 - Power(t2,2) + 
                  s2*(3 + 2*t2 + 5*Power(t2,2))))) - 
         (s2 - t1)*(-1 + t1)*(2*(-1 + s1)*Power(t1,5)*(1 + s1 - 2*t2) + 
            Power(s2,4)*Power(s1 - t2,3) - 
            3*(-1 + s1)*t1*Power(-1 + t2,2) + Power(-1 + t2,3) - 
            Power(t1,2)*(-1 + t2)*
             (-1 + 7*Power(s1,2) + 24*t2 + 6*Power(t2,2) - 
               4*s1*(5 + 4*t2)) + 
            Power(t1,3)*(-5 + Power(s1,3) - 40*t2 + 42*Power(t2,2) + 
               4*Power(t2,3) + Power(s1,2)*(-9 + 8*t2) + 
               s1*(49 - 36*t2 - 14*Power(t2,2))) + 
            Power(t1,4)*(6 + 14*t2 - 4*Power(s1,2)*t2 - 24*Power(t2,2) + 
               s1*(-26 + 30*t2 + 4*Power(t2,2))) - 
            Power(s2,3)*(s1 - t2)*
             (-6 + Power(s1,2)*(1 + 3*t1) + t1*(12 - 5*t2) + t2 + 
               4*Power(t2,2) + Power(t1,2)*(-6 + 4*t2) + 
               s1*(5 + 2*Power(t1,2) - 5*t2 - t1*(7 + 3*t2))) + 
            s2*(-(Power(s1,3)*Power(t1,2)*(3 + t1)) - 
               Power(-1 + t2,2)*(-1 + 4*t2) + 
               Power(t1,2)*(15 + 68*t2 - 86*Power(t2,2)) + 
               2*Power(t1,4)*(2 - 7*t2 + 2*Power(t2,2)) - 
               Power(t1,3)*(13 + 16*t2 - 46*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-7 - 32*t2 + 27*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-6*Power(t1,3) + t1*(13 - 10*t2) + 14*(-1 + t2) + 
                  Power(t1,2)*(7 + 8*t2)) + 
               s1*(3*Power(-1 + t2,2) + 6*Power(t1,4)*(1 + t2) + 
                  t1*(49 - 26*t2 - 23*Power(t2,2)) + 
                  Power(t1,3)*(43 - 68*t2 - 2*Power(t2,2)) + 
                  Power(t1,2)*(-101 + 94*t2 + 10*Power(t2,2)))) + 
            Power(s2,2)*(2 + 3*Power(s1,3)*t1*(1 + t1) + 25*t2 - 
               27*Power(t2,2) - 
               2*Power(t1,3)*(1 - 8*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(6 - 7*t2 - 20*Power(t2,2) + 6*Power(t2,3)) - 
               t1*(6 + 34*t2 - 55*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s1,2)*(7 + t1 + 6*Power(t1,3) - 7*t2 - 4*t1*t2 - 
                  7*Power(t1,2)*(2 + t2)) + 
               s1*(-29 - 12*Power(t1,3) + 22*t2 + 7*Power(t2,2) + 
                  Power(t1,2)*(-5 + 40*t2 - 2*Power(t2,2)) + 
                  t1*(46 - 62*t2 + 13*Power(t2,2))))) + 
         Power(s,2)*(2*Power(t1,7) - 2*Power(t1,6)*(1 + 4*s1 - 5*t2) + 
            12*Power(-1 + t2,2)*t2 + 
            Power(t1,5)*(-23 + 6*Power(s1,2) + s1*(33 - 13*t2) - 65*t2 + 
               6*Power(t2,2)) - 
            t1*(-1 + t2)*(30 - 65*t2 + 26*Power(t2,2) + 
               s1*(-22 + 26*t2)) + 
            Power(t1,3)*(93 - 6*Power(s1,3) - 195*t2 + 130*Power(t2,2) - 
               5*Power(t2,3) + 4*Power(s1,2)*(9 + 2*t2) + 
               s1*(26 - 76*t2 - 33*Power(t2,2))) + 
            Power(t1,4)*(2 + 4*Power(s1,3) + 183*t2 - 100*Power(t2,2) + 
               10*Power(t2,3) - Power(s1,2)*(30 + 7*t2) + 
               s1*(-101 + 132*t2 - 3*Power(t2,2))) + 
            Power(t1,2)*(Power(s1,2)*(-8 + 6*t2) + 
               9*s1*(8 - 11*t2 + 6*Power(t2,2)) + 
               3*(-34 + 50*t2 - 33*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,5)*(3*Power(s1,3) - 2*Power(s1,2)*(1 + 6*t2) + 
               s1*(1 - Power(t1,2) + 9*t2 - 5*t1*t2 + 15*Power(t2,2)) - 
               t2*(3 + Power(t1,2) + 7*t2 + 6*Power(t2,2) - 
                  t1*(4 + 5*t2))) - 
            Power(s2,4)*(3 + Power(s1,3)*(-3 + 8*t1) + 4*t2 + 
               17*Power(t2,2) + 10*Power(t2,3) - 
               Power(t1,3)*(4 + 3*t2) + 
               Power(t1,2)*(11 + 15*t2 + 12*Power(t2,2)) - 
               t1*(10 + 16*t2 + 36*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s1,2)*(3 + Power(t1,2) + 13*t2 - t1*(11 + 28*t2)) + 
               s1*(1 - 3*Power(t1,3) + Power(t1,2)*(2 - 9*t2) - 16*t2 - 
                  20*Power(t2,2) + t1*t2*(39 + 35*t2))) + 
            Power(s2,3)*(5 + Power(s1,3)*(2 - 13*t1 + 6*Power(t1,2)) - 
               2*t2 - 41*Power(t2,2) - 10*Power(t2,3) - 
               Power(t1,4)*(14 + 3*t2) + 
               Power(t1,3)*(49 + 11*t2 + 9*Power(t2,2)) - 
               Power(t1,2)*(51 + 5*t2 + 66*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(11 - t2 + 111*Power(t2,2) + 34*Power(t2,3)) + 
               Power(s1,2)*(-9 + 3*Power(t1,3) - 6*t2 - 
                  10*Power(t1,2)*(1 + 2*t2) + t1*(29 + 48*t2)) + 
               s1*(-5 - 3*Power(t1,4) - 2*Power(t1,3)*(-2 + t2) + 
                  36*t2 + 12*Power(t2,2) + 
                  t1*(18 - 102*t2 - 65*Power(t2,2)) + 
                  2*Power(t1,2)*(-7 + 21*t2 + 12*Power(t2,2)))) + 
            s2*(-1 - 10*Power(t1,6) - 
               Power(s1,3)*Power(t1,2)*(-14 + 15*t1 + Power(t1,2)) + 
               11*t2 - 10*Power(t2,2) - 4*Power(t1,5)*(-6 + 5*t2) + 
               Power(t1,4)*(27 + 131*t2 - 22*Power(t2,2)) + 
               t1*(37 + 13*t2 + 28*Power(t2,2)) + 
               2*Power(t1,2)*
                (-13 + 53*t2 - 93*Power(t2,2) + 6*Power(t2,3)) - 
               Power(t1,3)*(51 + 241*t2 - 203*Power(t2,2) + 
                  14*Power(t2,3)) + 
               Power(s1,2)*t1*
                (8 - 6*Power(t1,3) + Power(t1,4) - 4*t2 - 
                  t1*(79 + 24*t2) + Power(t1,2)*(89 + 30*t2)) + 
               s1*(2 - 8*t2 + 6*Power(t2,2) + Power(t1,5)*(16 + t2) + 
                  Power(t1,2)*(-87 + 320*t2 - 24*Power(t2,2)) - 
                  Power(t1,4)*(73 - 13*t2 + Power(t2,2)) + 
                  t1*(-17 - 42*t2 + 5*Power(t2,2)) + 
                  Power(t1,3)*(159 - 310*t2 + 16*Power(t2,2)))) + 
            Power(s2,2)*(-7 + Power(s1,3)*t1*(-10 + 21*t1) - 7*t2 - 
               25*Power(t2,2) + Power(t1,5)*(18 + t2) - 
               2*Power(t1,4)*(30 - 5*t2 + Power(t2,2)) + 
               Power(t1,2)*(17 + 107*t2 - 213*Power(t2,2) - 
                  24*Power(t2,3)) + 
               Power(t1,3)*(42 - 84*t2 + 58*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(-10 - 27*t2 + 147*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s1,2)*(-3*Power(t1,4) - 2*t2 + 
                  Power(t1,3)*(1 + 4*t2) + t1*(52 + 22*t2) - 
                  Power(t1,2)*(85 + 58*t2)) + 
               s1*(-5 + Power(t1,5) + 41*t2 - 9*Power(t2,2) - 
                  Power(t1,4)*(10 + 3*t2) + 
                  Power(t1,3)*(53 - 12*t2 - 3*Power(t2,2)) + 
                  t1*(26 - 200*t2 + 5*Power(t2,2)) + 
                  Power(t1,2)*(-65 + 244*t2 + 42*Power(t2,2))))) + 
         Power(s,3)*(-3 + 10*Power(t1,6) + 9*t2 + 3*Power(t2,2) - 
            9*Power(t2,3) + t1*
             (4 + s1 + 30*t2 - 34*s1*t2 - 28*Power(t2,2) + 
               29*s1*Power(t2,2) + Power(t2,3)) + 
            Power(t1,5)*(-16 + 2*Power(s1,2) + 11*t2 - 3*s1*(3 + t2)) + 
            Power(t1,3)*(-61 + 2*Power(s1,3) + 2*Power(s1,2)*(-8 + t2) + 
               100*t2 - 65*Power(t2,2) + 3*Power(t2,3) + 
               s1*(-31 + 85*t2 - 7*Power(t2,2))) + 
            Power(t1,4)*(-8*Power(s1,2) + s1*(50 - 6*t2 + Power(t2,2)) + 
               2*(7 - 47*t2 + 8*Power(t2,2))) + 
            Power(t1,2)*(52 + Power(s1,2)*(22 - 6*t2) - 10*t2 + 
               45*Power(t2,2) + 11*Power(t2,3) - 
               s1*(57 + 13*t2 + 27*Power(t2,2))) + 
            Power(s2,4)*(-3*Power(s1,3) + Power(t1,2)*(-1 + 5*t2) + 
               Power(s1,2)*(1 + 6*t1 + 18*t2) + 
               t1*(1 - 11*t2 - 10*Power(t2,2)) + 
               t2*(7 + 18*t2 + 15*Power(t2,2)) - 
               s1*(2 - 2*t1 + Power(t1,2) + 15*t2 + 30*Power(t2,2))) - 
            Power(s2,2)*(-4 + Power(s1,3)*t1*(-8 + 5*t1) + 14*t2 - 
               17*Power(t2,2) - 27*Power(t2,3) - 
               Power(t1,4)*(42 + 13*t2) + 
               Power(t1,3)*(100 + 8*t2 + 9*Power(t2,2)) - 
               Power(t1,2)*(59 - 43*t2 + 93*Power(t2,2) + 
                  18*Power(t2,3)) + 
               t1*(5 - 35*t2 + 144*Power(t2,2) + 64*Power(t2,3)) + 
               Power(s1,2)*(-14*Power(t1,3) + 
                  Power(t1,2)*(21 - 16*t2) - 4*t2 + 2*t1*(8 + 21*t2)) + 
               s1*(1 + 5*Power(t1,4) + 7*t2 + 16*Power(t2,2) + 
                  Power(t1,3)*(-5 + 28*t2) + 
                  t1*(21 - 107*t2 - 72*Power(t2,2)) + 
                  3*Power(t1,2)*(-13 + 2*t2 + 6*Power(t2,2)))) + 
            s2*(-3 + Power(s1,3)*(-8 + t1)*Power(t1,2) + 11*t2 - 
               24*Power(t2,2) + 9*Power(t2,3) - 
               Power(t1,5)*(35 + 4*t2) + 
               Power(t1,4)*(73 - 12*t2 + Power(t2,2)) - 
               Power(t1,3)*(33 - 122*t2 + 56*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(8 - 50*t2 + 156*Power(t2,2) + 
                  31*Power(t2,3)) - 
               t1*(10 + 33*t2 + 50*Power(t2,2) + 39*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-7*Power(t1,3) - 4*Power(t1,2)*(-7 + t2) - 
                  4*(1 + 4*t2) + t1*(27 + 29*t2)) + 
               s1*(2*Power(t1,5) + Power(1 + t2,2) + 
                  3*Power(t1,4)*(3 + 5*t2) + 
                  Power(t1,2)*(63 - 205*t2 - 15*Power(t2,2)) + 
                  Power(t1,3)*(-101 + 5*t2 - 3*Power(t2,2)) + 
                  t1*(-8 + 112*t2 + 17*Power(t2,2)))) + 
            Power(s2,3)*(2 + Power(s1,3)*(-2 + 7*t1) - 7*t2 + 
               31*Power(t2,2) + 30*Power(t2,3) - 
               2*Power(t1,3)*(8 + 7*t2) - 
               Power(s1,2)*(1 + 15*Power(t1,2) - 17*t2 + 30*t1*t2) + 
               Power(t1,2)*(35 + 27*t2 + 18*Power(t2,2)) - 
               t1*(21 + 10*t2 + 72*Power(t2,2) + 30*Power(t2,3)) + 
               s1*(3 + 4*Power(t1,3) - 9*t2 - 42*Power(t2,2) + 
                  Power(t1,2)*(-7 + 16*t2) + 
                  t1*(4 + 32*t2 + 50*Power(t2,2))))) + 
         s*(-(Power(s2,6)*Power(s1 - t2,2)*(-1 + s1 + t1 - t2)) + 
            Power(s2,5)*(s1 - t2)*
             (-1 + Power(s1,2)*(-1 + 3*t1) - 3*t2 - Power(t2,2) - 
               Power(t1,2)*(1 + 3*t2) + t1*(2 + 6*t2 + 3*Power(t2,2)) + 
               2*s1*(2 + 2*Power(t1,2) + t2 - t1*(4 + 3*t2))) - 
            (-1 + t1)*(2*Power(t1,6)*(1 + s1 - 2*t2) - 
               4*Power(-1 + t2,3) + 
               t1*Power(-1 + t2,2)*(-26 + 8*s1 + 11*t2) - 
               2*Power(t1,5)*
                (-5 + s1 + 4*Power(s1,2) - 4*t2 - 6*s1*t2) - 
               Power(t1,2)*(-1 + t2)*
                (43 + 2*Power(s1,2) - 40*t2 + 3*Power(t2,2) + 
                  s1*(-24 + 22*t2)) - 
               Power(t1,4)*(23 + 2*Power(s1,3) + 79*t2 - 
                  74*Power(t2,2) + 4*Power(t2,3) - 
                  Power(s1,2)*(13 + 11*t2) + 
                  6*s1*(-12 + 16*t2 + Power(t2,2))) + 
               Power(t1,3)*(-10 + 2*Power(s1,3) + 107*t2 - 
                  76*Power(t2,2) - 4*Power(t2,3) - 
                  Power(s1,2)*(2 + 13*t2) + 
                  4*s1*(-14 + 11*t2 + 7*Power(t2,2)))) + 
            s2*(-(Power(s1,3)*Power(t1,2)*
                  (6 - 13*t1 + 8*Power(t1,2) + Power(t1,3))) + 
               Power(-1 + t2,2)*(13 + 2*t2) - 
               2*Power(t1,6)*(-5 + 6*t2) + 
               4*Power(t1,5)*(2 + 7*t2 + 2*Power(t2,2)) - 
               2*t1*(21 - 29*t2 + Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,2)*(10 - 201*t2 + 131*Power(t2,2) + 
                  9*Power(t2,3)) - 
               Power(t1,4)*(81 + 177*t2 - 165*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(t1,3)*(82 + 328*t2 - 311*Power(t2,2) + 
                  25*Power(t2,3)) + 
               Power(s1,2)*t1*
                (Power(t1,4)*(-21 + t2) + 4*(-1 + t2) + 
                  9*t1*(3 + 2*t2) - 6*Power(t1,2)*(15 + 7*t2) + 
                  Power(t1,3)*(88 + 25*t2)) + 
               s1*(6*Power(t1,6) - 8*Power(-1 + t2,2) + 
                  5*Power(t1,5)*(2 + 3*t2) + 
                  Power(t1,3)*(-263 + 439*t2 - 5*Power(t2,2)) + 
                  2*Power(t1,4)*(55 - 139*t2 + 4*Power(t2,2)) - 
                  2*Power(t1,2)*(-59 + 71*t2 + 12*Power(t2,2)) + 
                  t1*(27 - 50*t2 + 23*Power(t2,2)))) - 
            Power(s2,4)*(-4 + Power(s1,3)*(1 - 5*t1 + 2*Power(t1,2)) - 
               2*t2 - 14*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,3)*(4 + 3*t2 + 3*Power(t2,2)) - 
               Power(t1,2)*(12 + 8*t2 + 14*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(12 + 7*t2 + 25*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s1,2)*(-9 + 6*Power(t1,3) - 6*t2 - 
                  Power(t1,2)*(15 + 8*t2) + 2*t1*(9 + 10*t2)) + 
               s1*(2 + 23*t2 + 8*Power(t2,2) - 
                  3*Power(t1,3)*(1 + 3*t2) + 
                  Power(t1,2)*(8 + 29*t2 + 9*Power(t2,2)) - 
                  t1*(7 + 43*t2 + 23*Power(t2,2)))) - 
            Power(s2,3)*(5 + Power(s1,3)*
                (2 - 7*t1 + 11*Power(t1,2) + 2*Power(t1,3)) + 17*t2 - 
               12*Power(t2,2) + 7*Power(t2,3) + 
               Power(t1,4)*(-14 + t2 - Power(t2,2)) + 
               Power(t1,3)*(37 - 4*t2 + 11*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(27 - 22*t2 + 64*Power(t2,2) + 
                  7*Power(t2,3)) - 
               t1*(1 + 36*t2 - 66*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s1,2)*(-19 + 15*Power(t1,3) - 4*Power(t1,4) + 
                  4*t2 + 6*t1*(13 + t2) - 2*Power(t1,2)*(35 + 17*t2)) + 
               s1*(9 + 20*t2 - 13*Power(t2,2) + Power(t1,4)*(1 + 5*t2) - 
                  Power(t1,3)*(28 + 15*t2 + 3*Power(t2,2)) + 
                  t1*(-44 - 111*t2 + 10*Power(t2,2)) + 
                  Power(t1,2)*(62 + 101*t2 + 30*Power(t2,2)))) + 
            Power(s2,2)*(1 + Power(s1,3)*t1*
                (6 - 15*t1 + 13*Power(t1,2) + 3*Power(t1,3)) + 4*t2 - 
               5*Power(t2,2) + Power(t1,5)*(-18 + 11*t2) + 
               Power(t1,4)*(25 - 26*t2 - 6*Power(t2,2)) - 
               3*Power(t1,2)*(22 + 68*t2 - 79*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(t1,3)*(32 + 121*t2 - 136*Power(t2,2) + 
                  17*Power(t2,3)) + 
               t1*(26 + 94*t2 - 90*Power(t2,2) + 21*Power(t2,3)) - 
               Power(s1,2)*(Power(t1,5) + 2*(-1 + t2) - 
                  18*Power(t1,2)*(8 + t2) + 3*t1*(14 + t2) + 
                  Power(t1,4)*(-20 + 3*t2) + Power(t1,3)*(123 + 31*t2)) + 
               s1*(5 + Power(t1,5)*(-5 + t2) - 12*t2 + 7*Power(t2,2) - 
                  Power(t1,4)*(36 + t2) + 
                  t1*(-77 + 164*t2 - 39*Power(t2,2)) + 
                  Power(t1,3)*(20 + 235*t2 - 4*Power(t2,2)) + 
                  Power(t1,2)*(93 - 387*t2 + 57*Power(t2,2))))) + 
         Power(s,4)*(1 + Power(t1,4)*
             (4*Power(s1,2) + s1*(5 - 7*t2) + 3*(-9 + t2)) - 12*t2 + 
            21*Power(t2,2) - 6*Power(t2,3) + 
            Power(t1,5)*(17 - 4*s1 + 3*t2) + 
            t1*(-12 + 17*t2 - 10*Power(t2,2) + 24*Power(t2,3) + 
               s1*(10 - 9*t2 - 11*Power(t2,2))) + 
            Power(t1,2)*(2 - 23*t2 - 16*Power(t2,2) - 13*Power(t2,3) + 
               Power(s1,2)*(-11 + 2*t2) + s1*(8 + 49*t2 - 3*Power(t2,2))) \
+ Power(t1,3)*(13 - 7*Power(s1,2) - 43*t2 + 13*Power(t2,2) + 
               Power(t2,3) + s1*(22 - 8*t2 + 3*Power(t2,2))) + 
            Power(s2,3)*(Power(s1,3) - 2*Power(t1,3) + 
               Power(t1,2)*(4 - 10*t2) - 4*Power(s1,2)*(2*t1 + 3*t2) + 
               t1*(-2 + 9*t2 + 10*Power(t2,2)) - 
               t2*(5 + 22*t2 + 20*Power(t2,2)) + 
               s1*(1 + 9*Power(t1,2) + 11*t2 + 30*Power(t2,2) + 
                  2*t1*(-2 + 5*t2))) + 
            s2*(-2*Power(t1,5) + Power(t1,4)*(-35 + 16*s1 - 17*t2) + 
               Power(t1,2)*(-4 + Power(s1,3) + Power(s1,2)*(13 - 4*t2) + 
                  3*s1*(-9 + t2) + 41*t2 - 44*Power(t2,2) - 
                  12*Power(t2,3)) + 
               t2*((25 - 31*t2)*t2 + s1*(-7 + 9*t2)) + 
               Power(t1,3)*(58 - 13*Power(s1,2) + 2*t2 + 3*Power(t2,2) + 
                  2*s1*(-9 + 13*t2)) + 
               t1*(-4 - 3*t2 + 54*Power(t2,2) + 53*Power(t2,3) + 
                  Power(s1,2)*(7 + 11*t2) - s1*t2*(59 + 26*t2))) + 
            Power(s2,2)*(1 + 4*Power(t1,4) + 
               (13 - 3*s1 - 7*Power(s1,2))*t2 + 
               4*(-5 + 9*s1)*Power(t2,2) - 40*Power(t2,3) + 
               Power(t1,3)*(17 - 21*s1 + 24*t2) + 
               Power(t1,2)*(-31 + 17*Power(s1,2) - 13*t2 - 
                  12*Power(t2,2) - 4*s1*(-5 + 8*t2)) + 
               t1*(-2*Power(s1,3) + 12*Power(s1,2)*t2 - 
                  3*s1*(3 + 2*t2 + 10*Power(t2,2)) + 
                  2*(5 - 7*t2 + 30*Power(t2,2) + 15*Power(t2,3))))))*T5q(s))/
     (s*(-1 + s1)*(-1 + s2)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*
       (-1 + t2)));
   return a;
};
