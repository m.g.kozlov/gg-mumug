
struct mc_pars 
{
    int N;
    double s;
    double mf;
    double om_min;
    double th_particles_min;
    double th_max;
    double th_gmu;
    double si_min;
    double om_s; //omega*
};

double mc_born(double *inv, size_t dim, void *params);
double mc_ur_born(double *inv, size_t dim, void *params);
double mc_nlo2(double *inv, size_t dim, void *params);
double mc_nlo1(double *inv, size_t dim, void *params);
double mc_ur_nlo1(double *inv, size_t dim, void *params);
double mc_ur_nlo1_m0(double *inv, size_t dim, void *params);


int mc_integrate(struct mc_pars *, double (*myf)(double *,size_t, void *), double *, double *);
int mc_integrate_wb(struct mc_pars *, double (*myf)(double *,size_t, void *), double *, double *);
int mc_integrate_col_v1(struct mc_pars *, double (*myf)(double *,size_t, void *), double *, double *);