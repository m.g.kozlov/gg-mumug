#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_2_m132_4_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-16*(22*s2 + 9*Power(s2,2) - 12*Power(s2,3) - 2*Power(s2,4) - 
         2*Power(s2,5) - 22*t1 - 54*s2*t1 - 3*Power(s2,2)*t1 + 
         2*Power(s2,3)*t1 + 8*Power(s2,4)*t1 + 45*Power(t1,2) + 
         56*s2*Power(t1,2) + 20*Power(s2,2)*Power(t1,2) - 
         12*Power(s2,3)*Power(t1,2) - 41*Power(t1,3) - 38*s2*Power(t1,3) + 
         8*Power(s2,2)*Power(t1,3) + 18*Power(t1,4) - 2*s2*Power(t1,4) + 
         Power(s1,2)*(-12 - Power(s2,3) + 8*t1 + 3*Power(s2,2)*t1 + 
            4*Power(t1,2) + Power(t1,3) - s2*(12 + 4*t1 + 3*Power(t1,2))) \
- 2*Power(s,4)*(s1 - s2 + t1 - t2) + 32*t2 - 5*s2*t2 - 25*Power(s2,2)*t2 + 
         9*Power(s2,3)*t2 - 3*Power(s2,4)*t2 - 43*t1*t2 + s2*t1*t2 - 
         34*Power(s2,2)*t1*t2 + 7*Power(s2,3)*t1*t2 + 40*Power(t1,2)*t2 + 
         55*s2*Power(t1,2)*t2 - 5*Power(s2,2)*Power(t1,2)*t2 - 
         30*Power(t1,3)*t2 + s2*Power(t1,3)*t2 - 12*Power(t2,2) - 
         5*s2*Power(t2,2) + 9*Power(s2,2)*Power(t2,2) + 
         Power(s2,3)*Power(t2,2) + t1*Power(t2,2) - 21*s2*t1*Power(t2,2) - 
         2*Power(s2,2)*t1*Power(t2,2) + 12*Power(t1,2)*Power(t2,2) + 
         s2*Power(t1,2)*Power(t2,2) + 
         Power(s,3)*(2*Power(s1,2) - 10*Power(s2,2) + 2*t1 + 13*s2*t1 - 
            5*Power(t1,2) - 2*t2 - 7*s2*t2 + 4*t1*t2 + Power(t2,2) + 
            s1*(8*s2 - 3*(t1 + t2))) + 
         Power(s,2)*(-10 + 12*Power(s2,3) - 36*t1 + 21*Power(t1,2) - 
            4*Power(t1,3) + Power(s1,2)*(3 - 5*s2 + 5*t1) + 36*t2 - 
            32*t1*t2 + 2*Power(t1,2)*t2 + 11*Power(t2,2) + 
            2*t1*Power(t2,2) + Power(s2,2)*(-8 - 24*t1 + 5*t2) + 
            s1*(-9*Power(s2,2) + Power(t1,2) + t1*(18 - 7*t2) - 
               14*(2 + t2) + s2*(-1 + 8*t1 + 6*t2)) + 
            s2*(46 + 16*Power(t1,2) + 9*t2 - Power(t2,2) - t1*(13 + 9*t2))\
) + s1*(Power(s2,4) + Power(t1,4) - 2*Power(s2,3)*(3 + 2*t1) + 
            t1*(37 - 9*t2) - Power(t1,3)*(-20 + t2) + 8*(-4 + 3*t2) - 
            Power(t1,2)*(25 + 16*t2) + 
            Power(s2,2)*(36 + 16*t1 + 6*Power(t1,2) - 9*t2 - t1*t2) + 
            s2*(11 - 4*Power(t1,3) + 2*Power(t1,2)*(-15 + t2) + 17*t2 + 
               t1*(-27 + 25*t2))) + 
         s*(10 - 2*Power(s2,4) + 47*t1 - 77*Power(t1,2) + 37*Power(t1,3) - 
            Power(t1,4) + Power(s1,2)*
             (8 + 4*Power(s2,2) + 7*t1 + 4*Power(t1,2) - s2*(3 + 8*t1)) - 
            67*t2 + 76*t1*t2 - 60*Power(t1,2)*t2 + Power(t2,2) + 
            23*t1*Power(t2,2) + Power(t1,2)*Power(t2,2) + 
            Power(s2,3)*(10 + 5*t1 + 3*t2) - 
            Power(s2,2)*(22 + 5*Power(t1,2) + 16*t2 + Power(t2,2) + 
               t1*(-3 + 2*t2)) + 
            s2*(-59 + 3*Power(t1,3) - 3*t2 - 20*Power(t2,2) - 
               Power(t1,2)*(50 + t2) + t1*(101 + 62*t2)) + 
            s1*(61 + 2*Power(s2,3) + 3*Power(t1,3) + 
               Power(t1,2)*(38 - 5*t2) - 9*t2 - 
               Power(s2,2)*(-7 + t1 + 3*t2) - t1*(53 + 30*t2) + 
               s2*(-16 - 4*Power(t1,2) + 23*t2 + t1*(-29 + 8*t2))))))/
     ((-1 + s - s2 + t1)*Power(s - s2 + t1,2)*(-1 + s1 + t1 - t2)*
       (s - s1 + t2)*(-s1 + s2 - t1 + t2)) + 
    (8*(-32 - 20*s2 - 8*Power(s2,2) + 20*t1 + 24*s2*t1 + 
         8*Power(s2,2)*t1 + 16*Power(t1,2) - 4*s2*Power(t1,2) - 
         4*Power(t1,3) + Power(s1,3)*
          (Power(s2,3) + Power(s2,2)*(2 - 3*t1) - 
            (-2 + t1)*Power(t1,2) + s2*(12 - 8*t1 + 3*Power(t1,2))) + 
         Power(s,5)*(t1 - t2) + 44*t2 - 19*s2*t2 - 4*Power(s2,2)*t2 - 
         8*Power(s2,3)*t2 - 49*t1*t2 - 56*s2*t1*t2 - 
         4*Power(s2,2)*t1*t2 + 16*Power(t1,2)*t2 + 19*s2*Power(t1,2)*t2 - 
         3*Power(t1,3)*t2 - 12*Power(t2,2) + 45*s2*Power(t2,2) + 
         12*Power(s2,2)*Power(t2,2) + 8*Power(s2,3)*Power(t2,2) + 
         15*t1*Power(t2,2) - 2*s2*t1*Power(t2,2) - 
         14*Power(s2,2)*t1*Power(t2,2) - 6*Power(t1,2)*Power(t2,2) + 
         s2*Power(t1,2)*Power(t2,2) + Power(t1,3)*Power(t2,2) - 
         10*s2*Power(t2,3) + 3*Power(s2,2)*Power(t2,3) - 
         Power(s2,3)*Power(t2,3) - 2*t1*Power(t2,3) + 
         2*Power(s2,2)*t1*Power(t2,3) + Power(t1,2)*Power(t2,3) - 
         s2*Power(t1,2)*Power(t2,3) - 
         Power(s,4)*(5 + (4 + 3*s1)*t1 - 2*Power(t1,2) + 
            s2*(-6 + s1 + 4*t1 - 3*t2) - 6*t2 - 2*s1*t2 + 2*Power(t2,2)) \
+ Power(s,3)*(31 - 4*t1 - 6*Power(t1,2) + Power(t1,3) + 
            Power(s2,2)*(-14 + 3*t1 - t2) + 
            Power(s1,2)*(2*s2 + 3*t1 - t2) - 22*t2 + 5*t1*t2 + 
            3*Power(t1,2)*t2 + 9*Power(t2,2) - 3*t1*Power(t2,2) - 
            Power(t2,3) + s1*
             (16 + 10*t1 - 6*Power(t1,2) + s2*(-17 + 10*t1 - 8*t2) - 
               9*t2 + 2*Power(t2,2)) + 
            s2*(-18 + 17*t1 - 4*Power(t1,2) + 2*t2 - 6*t1*t2 + 
               6*Power(t2,2))) - 
         Power(s1,2)*(12 + 2*t1*(-9 + t2) - 2*Power(t1,3)*(-1 + t2) + 
            3*Power(t1,2)*(2 + t2) + 
            Power(s2,2)*(-16 + t1*(18 - 8*t2) + t2) + 
            Power(s2,3)*(-8 + 3*t2) + 
            s2*(-42 + t1*(6 - 16*t2) + 34*t2 + Power(t1,2)*(-8 + 7*t2))) \
- Power(s,2)*(53 - 47*t1 - 6*Power(t1,2) + 2*Power(t1,3) + 
            Power(s1,3)*(s2 + t1) + 
            Power(s1,2)*(7 + 8*t1 - 6*Power(t1,2) + 
               s2*(-17 + 8*t1 - 5*t2) - 3*t2) + Power(s2,3)*(-8 + t2) - 
            42*t2 + 43*t1*t2 + 2*Power(t1,2)*t2 - 2*Power(t1,3)*t2 + 
            11*Power(t2,2) - 13*t1*Power(t2,2) - 3*Power(t2,3) + 
            2*t1*Power(t2,3) + 
            Power(s2,2)*(-35 + t1*(19 - 8*t2) + 20*t2 + 2*Power(t2,2)) + 
            s2*(19 + 9*Power(t1,2)*(-1 + t2) + 17*t2 + 6*Power(t2,2) - 
               3*Power(t2,3) - 6*t1*(-2 + 3*t2)) - 
            s1*(-45 + Power(s2,3) - 3*Power(t1,3) + 
               Power(t1,2)*(14 - 6*t2) + 18*t2 - 6*Power(t2,2) + 
               Power(s2,2)*(29 - 9*t1 + 2*t2) + 
               s2*(35 + 11*Power(t1,2) + 8*t1*(-5 + t2) - 11*t2 - 
                  7*Power(t2,2)) + t1*(29 - 5*t2 + 3*Power(t2,2)))) + 
         s1*(-44 + 24*t2 + 4*Power(t1,2)*(-4 + 3*t2) + 
            Power(t1,3)*(-1 + t2 - Power(t2,2)) + 
            Power(s2,3)*(8 - 16*t2 + 3*Power(t2,2)) + 
            t1*(53 - 33*t2 + 4*Power(t2,2)) + 
            s2*(15 - 87*t2 + 32*Power(t2,2) - 
               8*t1*(-6 - t2 + Power(t2,2)) + 
               Power(t1,2)*(-7 - 9*t2 + 5*Power(t2,2))) - 
            Power(s2,2)*(4*(-3 + 7*t2 + Power(t2,2)) + 
               t1*(4 - 32*t2 + 7*Power(t2,2)))) + 
         s*(64 - 25*t1 + 12*Power(t1,2) + 5*Power(t1,3) + 
            2*Power(s1,3)*(s2*(-3 + t1) - (-1 + t1)*t1) - 73*t2 + 
            58*t1*t2 - 24*Power(t1,2)*t2 - Power(t1,3)*t2 + 
            15*Power(t2,2) - 17*t1*Power(t2,2) + 
            5*Power(t1,2)*Power(t2,2) + Power(t1,3)*Power(t2,2) - 
            2*Power(t2,3) + 4*t1*Power(t2,3) - Power(t1,2)*Power(t2,3) - 
            2*Power(s2,3)*(8 - 8*t2 + Power(t2,2)) + 
            Power(s2,2)*(4 + 23*t2 - 3*Power(t2,2) - Power(t2,3) + 
               t1*(20 - 33*t2 + 7*Power(t2,2))) + 
            s2*(13 + 38*t2 - 33*Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,2)*(-5 + 10*t2 - 6*Power(t2,2)) + 
               t1*(-72 + 34*t2 + Power(t2,2) + 2*Power(t2,3))) - 
            Power(s1,2)*(2*Power(s2,3) + 13*t1 - 3*Power(t1,3) + 
               Power(t1,2)*(10 - 3*t2) + 2*(-9 + t2) + 
               Power(s2,2)*(17 - 9*t1 + t2) + 
               s2*(41 + 10*Power(t1,2) - 10*t2 + t1*(-31 + 2*t2))) + 
            s1*(77 + 4*Power(s2,3)*(-4 + t2) - 4*Power(t1,3)*(-1 + t2) - 
               33*t2 + 4*Power(t2,2) + Power(t1,2)*(12 + 5*t2) + 
               t1*(-61 + 30*t2 - 6*Power(t2,2)) + 
               Power(s2,2)*(-35 + t1*(37 - 16*t2) + 20*t2 + 
                  2*Power(t2,2)) + 
               s2*(-43 + 74*t2 - 2*Power(t2,2) + 
                  Power(t1,2)*(-17 + 16*t2) - 
                  2*t1*(3 + 16*t2 + Power(t2,2))))))*
       B1(1 - s1 - t1 + t2,1 - s + s1 - t2,1 - s + s2 - t1))/
     (Power(s - s2 + t1,2)*(s - s1 + t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)) - 
    (16*(-18*s2 - 11*Power(s2,2) + 58*Power(s2,3) + 71*Power(s2,4) + 
         9*Power(s2,5) - 12*Power(s2,6) - Power(s2,7) + 18*t1 + 
         28*s2*t1 - 179*Power(s2,2)*t1 - 331*Power(s2,3)*t1 - 
         103*Power(s2,4)*t1 + 48*Power(s2,5)*t1 + 5*Power(s2,6)*t1 - 
         17*Power(t1,2) + 194*s2*Power(t1,2) + 
         578*Power(s2,2)*Power(t1,2) + 316*Power(s2,3)*Power(t1,2) - 
         69*Power(s2,4)*Power(t1,2) - 10*Power(s2,5)*Power(t1,2) - 
         73*Power(t1,3) - 445*s2*Power(t1,3) - 
         415*Power(s2,2)*Power(t1,3) + 39*Power(s2,3)*Power(t1,3) + 
         10*Power(s2,4)*Power(t1,3) + 127*Power(t1,4) + 
         249*s2*Power(t1,4) - 3*Power(s2,2)*Power(t1,4) - 
         5*Power(s2,3)*Power(t1,4) - 56*Power(t1,5) - 3*s2*Power(t1,5) + 
         Power(s2,2)*Power(t1,5) - 
         Power(s1,4)*(8 + Power(s2,3) + Power(s2,2)*(10 - 3*t1) - 
            16*t1 + 10*Power(t1,2) - Power(t1,3) + 
            s2*(16 - 20*t1 + 3*Power(t1,2))) + 2*Power(s,6)*(t1 - t2) + 
         12*t2 + 79*s2*t2 + 174*Power(s2,2)*t2 + 163*Power(s2,3)*t2 + 
         53*Power(s2,4)*t2 - 3*Power(s2,5)*t2 - 99*t1*t2 - 451*s2*t1*t2 - 
         651*Power(s2,2)*t1*t2 - 319*Power(s2,3)*t1*t2 - 
         12*Power(s2,4)*t1*t2 - Power(s2,5)*t1*t2 + 281*Power(t1,2)*t2 + 
         798*s2*Power(t1,2)*t2 + 593*Power(s2,2)*Power(t1,2)*t2 + 
         47*Power(s2,3)*Power(t1,2)*t2 + 3*Power(s2,4)*Power(t1,2)*t2 - 
         306*Power(t1,3)*t2 - 434*s2*Power(t1,3)*t2 - 
         37*Power(s2,2)*Power(t1,3)*t2 - 3*Power(s2,3)*Power(t1,3)*t2 + 
         107*Power(t1,4)*t2 - 4*s2*Power(t1,4)*t2 + 
         Power(s2,2)*Power(t1,4)*t2 + 9*Power(t1,5)*t2 + 36*Power(t2,2) + 
         148*s2*Power(t2,2) + 197*Power(s2,2)*Power(t2,2) + 
         100*Power(s2,3)*Power(t2,2) + 16*Power(s2,4)*Power(t2,2) + 
         2*Power(s2,5)*Power(t2,2) - 164*t1*Power(t2,2) - 
         411*s2*t1*Power(t2,2) - 285*Power(s2,2)*t1*Power(t2,2) - 
         36*Power(s2,3)*t1*Power(t2,2) - 2*Power(s2,4)*t1*Power(t2,2) + 
         194*Power(t1,2)*Power(t2,2) + 234*s2*Power(t1,2)*Power(t2,2) + 
         3*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         4*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         49*Power(t1,3)*Power(t2,2) + 39*s2*Power(t1,3)*Power(t2,2) + 
         5*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         22*Power(t1,4)*Power(t2,2) - Power(t1,5)*Power(t2,2) + 
         16*Power(t2,3) + 31*s2*Power(t2,3) + 
         18*Power(s2,2)*Power(t2,3) - 3*Power(s2,3)*Power(t2,3) - 
         4*Power(s2,4)*Power(t2,3) - 7*t1*Power(t2,3) + 
         6*s2*t1*Power(t2,3) + 20*Power(s2,2)*t1*Power(t2,3) + 
         7*Power(s2,3)*t1*Power(t2,3) - 24*Power(t1,2)*Power(t2,3) - 
         32*s2*Power(t1,2)*Power(t2,3) + 
         2*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         15*Power(t1,3)*Power(t2,3) - 9*s2*Power(t1,3)*Power(t2,3) + 
         4*Power(t1,4)*Power(t2,3) - 8*Power(t2,4) - 12*s2*Power(t2,4) - 
         2*Power(s2,2)*Power(t2,4) + 3*Power(s2,3)*Power(t2,4) + 
         12*t1*Power(t2,4) + 4*s2*t1*Power(t2,4) - 
         9*Power(s2,2)*t1*Power(t2,4) - 2*Power(t1,2)*Power(t2,4) + 
         9*s2*Power(t1,2)*Power(t2,4) - 3*Power(t1,3)*Power(t2,4) + 
         Power(s1,3)*(-16 - 18*Power(t1,3) + Power(t1,4) - 
            Power(s2,3)*(11 + t1) + t1*(14 - 60*t2) + 32*t2 + 
            Power(t1,2)*(15 + 32*t2) + 
            Power(s2,2)*(-35 + 4*t1 + 3*Power(t1,2) + 32*t2) + 
            s2*(-38 + 25*Power(t1,2) - 3*Power(t1,3) + t1*(20 - 64*t2) + 
               60*t2)) + Power(s,5)*
          (Power(s1,2) + 3*Power(s2,2) + s1*(4 - 2*s2 - 3*t1 + 3*t2) + 
            s2*(4 - 15*t1 + 15*t2) + 
            2*(-5 - t1 + 4*Power(t1,2) + t2 - 2*t1*t2 - 2*Power(t2,2))) + 
         Power(s,4)*(-2*Power(s1,3) - 13*Power(s2,3) - 35*t1 - 
            4*Power(t1,2) + 12*Power(t1,3) + 
            Power(s2,2)*(-24 + 50*t1 - 38*t2) + 
            Power(s1,2)*(-18 + 3*t1 - t2) - 15*t2 - 2*t1*t2 + 
            Power(t1,2)*t2 + 6*Power(t2,2) - 8*t1*Power(t2,2) - 
            5*Power(t2,3) + s1*
             (27 + Power(s2,2) + 8*t1 - 13*Power(t1,2) + 
               2*s2*(-7 + 9*t1 - 11*t2) + 12*t2 + 5*t1*t2 + 
               8*Power(t2,2)) + 
            s2*(43 + 36*t1 - 47*Power(t1,2) - 16*t2 + 25*t1*t2 + 
               22*Power(t2,2))) + 
         Power(s1,2)*(36 - 2*Power(s2,5) + 2*Power(t1,5) + 
            2*Power(t1,4)*(-14 + t2) + 48*t2 - 48*Power(t2,2) + 
            Power(t1,3)*(-37 + 51*t2 - 6*Power(t2,2)) - 
            2*Power(t1,2)*(-88 + 27*t2 + 18*Power(t2,2)) + 
            t1*(-155 - 35*t2 + 84*Power(t2,2)) + 
            2*Power(s2,4)*(5*t1 - 2*(2 + t2)) + 
            Power(s2,3)*(56 - 20*Power(t1,2) + 19*t2 + 6*Power(t2,2) + 
               t1*(28 + 9*t2)) + 
            2*Power(s2,2)*(82 + 10*Power(t1,3) + 44*t2 - 
               18*Power(t2,2) - 2*Power(t1,2)*(15 + t2) + 
               t1*(-96 + 6*t2 - 9*Power(t2,2))) + 
            s2*(139 - 10*Power(t1,4) + Power(t1,3)*(68 - 3*t2) + 
               107*t2 - 84*Power(t2,2) + 
               Power(t1,2)*(173 - 82*t2 + 18*Power(t2,2)) + 
               t1*(-360 - 34*t2 + 72*Power(t2,2)))) - 
         s1*(12 + 4*Power(s2,6) + Power(s2,5)*(7 - 19*t1) + 72*t2 + 
            48*Power(t2,2) - 32*Power(t2,3) + Power(t1,5)*(14 + t2) + 
            Power(t1,4)*(70 - 50*t2 + 7*Power(t2,2)) + 
            Power(t1,2)*(218 + 370*t2 - 63*Power(t2,2) - 
               16*Power(t2,3)) - 
            Power(t1,3)*(229 + 86*t2 - 48*Power(t2,2) + 8*Power(t2,3)) + 
            t1*(-81 - 319*t2 - 28*Power(t2,2) + 52*Power(t2,3)) + 
            4*Power(s2,4)*(9*Power(t1,2) + 2*t1*(-4 + t2) + 
               2*(5 + t2 - Power(t2,2))) + 
            Power(s2,3)*(105 - 34*Power(t1,3) + 
               Power(t1,2)*(45 - 24*t2) + 156*t2 + 5*Power(t2,2) + 
               8*Power(t2,3) + t1*(-220 - 8*t2 + 15*Power(t2,2))) - 
            s2*(-61 + 3*Power(t1,5) - 287*t2 - 100*Power(t2,2) + 
               52*Power(t2,3) + 2*Power(t1,4)*(13 + 5*t2) + 
               Power(t1,3)*(289 - 107*t2 + 15*Power(t2,2)) + 
               t1*(331 + 771*t2 + 8*Power(t2,2) - 32*Power(t2,3)) - 
               Power(t1,2)*(576 + 407*t2 - 89*Power(t2,2) + 
                  24*Power(t2,3))) + 
            Power(s2,2)*(117 + 16*Power(t1,4) + 361*t2 + 
               71*Power(t2,2) - 16*Power(t2,3) + 
               Power(t1,3)*(-8 + 25*t2) + 
               Power(t1,2)*(399 - 57*t2 + Power(t2,2)) - 
               t1*(448 + 477*t2 - 36*Power(t2,2) + 24*Power(t2,3)))) + 
         Power(s,2)*(-80 - 18*Power(s2,5) + 179*t1 + 39*Power(t1,2) - 
            182*Power(t1,3) + 20*Power(t1,4) + 2*Power(t1,5) + 
            Power(s1,4)*(-10 - 3*s2 + 3*t1) + 
            Power(s2,4)*(-76 + 75*t1 - 24*t2) + t2 - 165*t1*t2 + 
            281*Power(t1,2)*t2 - 47*Power(t1,3)*t2 + 5*Power(t1,4)*t2 + 
            126*Power(t2,2) - 226*t1*Power(t2,2) + 
            12*Power(t1,2)*Power(t2,2) + 5*Power(t1,3)*Power(t2,2) + 
            27*Power(t2,3) + 17*t1*Power(t2,3) - 
            3*Power(t1,2)*Power(t2,3) - 2*Power(t2,4) - 
            9*t1*Power(t2,4) + 
            Power(s1,3)*(-48 - 4*Power(s2,2) + 22*t1 - 3*Power(t1,2) + 
               s2*(-50 + 7*t1) + 32*t2) + 
            Power(s2,3)*(96 - 115*Power(t1,2) - 45*t2 + 
               40*Power(t2,2) + t1*(212 + 30*t2)) + 
            Power(s2,2)*(125 + 79*Power(t1,3) + 
               10*Power(t1,2)*(-17 + t2) + 78*t2 + 62*Power(t2,2) - 
               27*Power(t2,3) - t1*(343 + 21*t2 + 50*Power(t2,2))) + 
            s2*(-109 - 23*Power(t1,4) + 71*t2 + 185*Power(t2,2) - 
               5*Power(t2,3) + 9*Power(t2,4) - 
               7*Power(t1,3)*(-2 + 3*t2) + 
               Power(t1,2)*(427 + 113*t2 + 6*Power(t2,2)) + 
               t1*(-152 - 354*t2 - 82*Power(t2,2) + 29*Power(t2,3))) + 
            Power(s1,2)*(94 + 7*Power(t1,3) + 
               Power(s2,2)*(-73 + 11*t1 - 19*t2) + 
               3*Power(t1,2)*(-10 + t2) + 123*t2 - 36*Power(t2,2) - 
               t1*(142 + 27*t2 + 18*Power(t2,2)) + 
               s2*(60 - 18*Power(t1,2) + 95*t2 + 18*Power(t2,2) + 
                  3*t1*(27 + 5*t2))) + 
            s1*(53 - 25*Power(s2,4) - 15*Power(t1,4) + 
               Power(s2,3)*(-31 + 101*t1 - 40*t2) - 220*t2 - 
               102*Power(t2,2) + 16*Power(t2,3) - 
               6*Power(t1,3)*(-3 + 2*t2) + 
               Power(t1,2)*(-104 + 18*t2 + 3*Power(t2,2)) + 
               t1*(-23 + 368*t2 - 12*Power(t2,2) + 24*Power(t2,3)) + 
               Power(s2,2)*(6 - 140*Power(t1,2) + 11*t2 + 
                  50*Power(t2,2) + t1*(84 + 39*t2)) + 
               s2*(93 + 79*Power(t1,3) - 245*t2 - 40*Power(t2,2) - 
                  24*Power(t2,3) + 3*Power(t1,2)*(-25 + 4*t2) + 
                  t1*(67 + t2 - 51*Power(t2,2))))) + 
         Power(s,3)*(60 + Power(s1,4) + 22*Power(s2,4) + 
            5*Power(s1,3)*(4 + s2 - t1) - 29*t1 - 88*Power(t1,2) + 
            8*Power(t1,3) + 8*Power(t1,4) + 29*t2 + 36*t1*t2 - 
            35*Power(t1,2)*t2 + 7*Power(t1,3)*t2 - 48*Power(t2,2) + 
            26*t1*Power(t2,2) - Power(t1,2)*Power(t2,2) + Power(t2,3) - 
            11*t1*Power(t2,3) - 3*Power(t2,4) + 
            Power(s2,3)*(60 - 85*t1 + 44*t2) - 
            Power(s1,2)*(6 + 3*Power(s2,2) - 5*Power(t1,2) + 
               s2*(-59 + 4*t1 - 9*t2) + 39*t2 + 6*Power(t2,2) + 
               t1*(28 + t2)) + 
            Power(s2,2)*(-88 + 107*Power(t1,2) + 41*t2 - 
               44*Power(t2,2) - 3*t1*(44 + 15*t2)) + 
            s2*(-27 - 52*Power(t1,3) + Power(t1,2)*(62 - 3*t2) + 6*t2 - 
               32*Power(t2,2) + 19*Power(t2,3) + 
               2*t1*(83 + 5*t2 + 18*Power(t2,2))) + 
            s1*(-81 + 13*Power(s2,3) - 21*Power(t1,3) - 
               4*Power(t1,2)*(-5 + t2) + 54*t2 + 18*Power(t2,2) + 
               8*Power(t2,3) + Power(s2,2)*(25 - 62*t1 + 47*t2) + 
               t1*(54 + 2*t2 + 17*Power(t2,2)) + 
               s2*(69*Power(t1,2) - t1*(39 + 32*t2) - 
                  3*(20 + 9*t2 + 11*Power(t2,2))))) + 
         s*(30 + 7*Power(s2,6) - 133*t1 + 62*Power(t1,2) + 
            203*Power(t1,3) - 175*Power(t1,4) + 10*Power(t1,5) + 
            Power(s1,4)*(16 + 3*Power(s2,2) + s2*(20 - 6*t1) - 20*t1 + 
               3*Power(t1,2)) - 27*t2 + 234*t1*t2 - 532*Power(t1,2)*t2 + 
            337*Power(t1,3)*t2 - 7*Power(t1,4)*t2 + Power(t1,5)*t2 - 
            116*Power(t2,2) + 368*t1*Power(t2,2) - 
            227*Power(t1,2)*Power(t2,2) - 30*Power(t1,3)*Power(t2,2) + 
            Power(t1,4)*Power(t2,2) - 39*Power(t2,3) + 3*t1*Power(t2,3) + 
            31*Power(t1,2)*Power(t2,3) + 7*Power(t1,3)*Power(t2,3) + 
            12*Power(t2,4) - 4*t1*Power(t2,4) - 
            9*Power(t1,2)*Power(t2,4) + Power(s2,5)*(48 - 32*t1 + 5*t2) + 
            Power(s2,4)*(-50 + 57*Power(t1,2) + 21*t2 - 16*Power(t2,2) - 
               t1*(162 + 5*t2)) + 
            Power(s1,3)*(46 + Power(s2,3) - Power(s2,2)*(-41 + t1) - 
               16*Power(t1,2) + Power(t1,3) - 60*t2 + t1*(-33 + 64*t2) - 
               s2*(-83 + 25*t1 + Power(t1,2) + 64*t2)) + 
            Power(s2,3)*(-169 - 49*Power(t1,3) + 
               Power(t1,2)*(181 - 11*t2) - 122*t2 - 52*Power(t2,2) + 
               17*Power(t2,3) + t1*(315 + 25*t2 + 24*Power(t2,2))) + 
            Power(s2,2)*(-5 + 20*Power(t1,4) - 259*t2 - 
               237*Power(t2,2) + 7*Power(t2,3) - 9*Power(t2,4) + 
               2*Power(t1,3)*(-29 + 9*t2) - 
               Power(t1,2)*(658 + 122*t2 + Power(t2,2)) + 
               t1*(508 + 629*t2 + 92*Power(t2,2) - 25*Power(t2,3))) - 
            s2*(-107 + 3*Power(t1,5) + 155*t2 + 319*Power(t2,2) + 
               45*Power(t2,3) - 4*Power(t2,4) + 
               Power(t1,4)*(19 + 8*t2) + 
               Power(t1,3)*(-568 - 83*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(540 + 851*t2 + 11*Power(t2,2) - 
                  Power(t2,3)) + 
               t1*(63 - 778*t2 - 500*Power(t2,2) + 37*Power(t2,3) - 
                  18*Power(t2,4))) + 
            Power(s1,2)*(-107 + 4*Power(s2,4) + 6*Power(t1,4) - 
               5*Power(s2,3)*(-8 + 4*t1 - 3*t2) - 131*t2 + 
               84*Power(t2,2) + Power(t1,3)*(-48 + 5*t2) + 
               t1*(318 + 69*t2 - 72*Power(t2,2)) + 
               Power(t1,2)*(-173 + 63*t2 - 18*Power(t2,2)) + 
               Power(s2,2)*(-110 + 34*Power(t1,2) - 75*t2 - 
                  18*Power(t2,2) - t1*(82 + 23*t2)) + 
               s2*(-254 - 24*Power(t1,3) - 211*t2 + 72*Power(t2,2) + 
                  3*Power(t1,2)*(30 + t2) + 
                  t1*(326 + 13*t2 + 36*Power(t2,2)))) + 
            s1*(9 + 17*Power(s2,5) - 4*Power(t1,5) + 223*t2 + 
               124*Power(t2,2) - 52*Power(t2,3) - 
               Power(t1,4)*(12 + 7*t2) + 
               Power(s2,4)*(23 - 73*t1 + 12*t2) + 
               Power(t1,3)*(-201 + 78*t2 - 13*Power(t2,2)) + 
               Power(t1,2)*(319 + 400*t2 - 78*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(-117 - 686*t2 - 39*Power(t2,2) + 32*Power(t2,3)) + 
               Power(s2,3)*(67 + 120*Power(t1,2) + 12*t2 - 
                  33*Power(t2,2) - t1*(85 + 4*t2)) + 
               Power(s2,2)*(89 - 93*Power(t1,3) + 
                  Power(t1,2)*(98 - 33*t2) + 347*t2 + 27*Power(t2,2) + 
                  24*Power(t2,3) + t1*(-334 - 10*t2 + 49*Power(t2,2))) + 
               s2*(44 + 33*Power(t1,4) + 573*t2 + 173*Power(t2,2) - 
                  32*Power(t2,3) + 8*Power(t1,3)*(-3 + 4*t2) + 
                  Power(t1,2)*(477 - 79*t2 - 3*Power(t2,2)) - 
                  t1*(385 + 826*t2 - 49*Power(t2,2) + 48*Power(t2,3))))))*
       R1(1 - s + s2 - t1))/
     ((-1 + s - s2 + t1)*Power(s - s2 + t1,2)*(s - s1 + t2)*
       Power(-s1 + s2 - t1 + t2,2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) + 
    (32*(-2*Power(s1,4) + 2*Power(s2,2) - 4*s2*t1 - 2*Power(s2,2)*t1 + 
         2*Power(t1,2) + 4*s2*Power(t1,2) - 2*Power(t1,3) + 
         Power(s,4)*(t1 - t2) + s2*t2 + 5*Power(s2,2)*t2 + 
         2*Power(s2,3)*t2 - t1*t2 - 15*s2*t1*t2 - 4*Power(s2,2)*t1*t2 + 
         10*Power(t1,2)*t2 + 2*s2*Power(t1,2)*t2 + 4*Power(t2,2) + 
         13*s2*Power(t2,2) + 2*Power(s2,2)*Power(t2,2) - 
         17*t1*Power(t2,2) + 2*s2*t1*Power(t2,2) - 
         4*Power(t1,2)*Power(t2,2) + 4*Power(t2,3) - 2*s2*Power(t2,3) + 
         6*t1*Power(t2,3) - s2*t1*Power(t2,3) + Power(t1,2)*Power(t2,3) - 
         2*Power(t2,4) + s2*Power(t2,4) - t1*Power(t2,4) + 
         Power(s1,3)*(-4 + t1*(-4 + t2) - (-8 + s2)*t2) + 
         Power(s,3)*(-5 + Power(s2,2) - 2*t1 + 2*Power(t1,2) + 2*t2 - 
            2*Power(t2,2) + s1*(2 - 2*t1 + 2*t2) + s2*(2 - 3*t1 + 3*t2)) \
- Power(s1,2)*(Power(s2,3) + Power(s2,2)*(1 - 3*t1) - Power(t1,3) - 
            Power(t1,2)*(-6 + t2) + 
            s2*(-10 + 3*Power(t1,2) + t1*(-7 + t2) + 2*t2 - 
               3*Power(t2,2)) + t1*(14 - 14*t2 + 3*Power(t2,2)) + 
            4*(-1 - 3*t2 + 3*Power(t2,2))) + 
         Power(s,2)*(5 - Power(s2,3) - 3*t1 - 5*Power(t1,2) + 
            Power(t1,3) + Power(s1,2)*(-5 + t1 - 2*t2) + 
            Power(s2,2)*(-3 + 3*t1 - t2) - 12*t2 + 4*t1*t2 + 
            2*Power(t1,2)*t2 + Power(t2,2) - t1*Power(t2,2) - 
            2*Power(t2,3) + s1*
             (9 - 2*Power(s2,2) + 2*t1 - 4*Power(t1,2) + 
               s2*(-5 + 6*t1 - 4*t2) + 4*t2 + 4*Power(t2,2)) + 
            s2*(2 + 8*t1 - 3*Power(t1,2) - 2*t2 - t1*t2 + 4*Power(t2,2))) \
+ s1*(Power(s2,3)*(-2 + t2) - Power(t1,3)*t2 - 
            2*Power(t1,2)*(5 - 5*t2 + Power(t2,2)) + 
            4*t2*(-2 - 3*t2 + 2*Power(t2,2)) + 
            t1*(1 + 31*t2 - 16*Power(t2,2) + 3*Power(t2,3)) - 
            Power(s2,2)*(5 + t2 + t1*(-4 + 3*t2)) + 
            s2*(-1 - 23*t2 + 4*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,2)*(-2 + 3*t2) + t1*(15 - 9*t2 + 2*Power(t2,2)))) \
+ s*(3*t1 + 4*Power(t1,2) - Power(t1,3) - Power(s2,3)*(-3 + t2) + 7*t2 - 
            14*t1*t2 - 6*Power(t1,2)*t2 + Power(t1,3)*t2 - 
            5*Power(t2,2) + 9*t1*Power(t2,2) + Power(t1,2)*Power(t2,2) - 
            2*Power(t2,3) - t1*Power(t2,3) - Power(t2,4) + 
            Power(s1,3)*(4 + t2) + 
            Power(s1,2)*(-2 + Power(s2,2) + t1 + 2*Power(t1,2) - 10*t2 - 
               t1*t2 - 3*Power(t2,2) + s2*(6 - 3*t1 + 2*t2)) + 
            Power(s2,2)*(3 + 2*t2 - 2*Power(t2,2) + t1*(-7 + 3*t2)) + 
            s2*(-3 + Power(t1,2)*(5 - 3*t2) + 11*t2 - 3*Power(t2,2) + 
               2*Power(t2,3) + t1*(-7 + 4*t2 + Power(t2,2))) + 
            s1*(-7 + 2*Power(s2,3) - 2*Power(t1,3) + 
               Power(t1,2)*(8 - 3*t2) + 7*t2 + 8*Power(t2,2) + 
               3*Power(t2,3) + Power(s2,2)*(1 - 6*t1 + t2) + 
               t1*(11 - 10*t2 + 2*Power(t2,2)) + 
               s2*(-8 + 6*Power(t1,2) - 3*t2 - 4*Power(t2,2) + 
                  t1*(-9 + 2*t2)))))*R1(1 - s + s1 - t2))/
     (Power(s - s2 + t1,2)*Power(s - s1 + t2,2)*Power(-s1 + s2 - t1 + t2,2)) \
- (16*(4 + Power(s,5) + 4*Power(s1,4) - 39*s2 - 30*Power(s2,2) + 
         9*Power(s2,3) - Power(s2,4) + Power(s2,5) + 27*t1 + 97*s2*t1 + 
         13*Power(s2,2)*t1 + 2*Power(s2,3)*t1 - 3*Power(s2,4)*t1 - 
         55*Power(t1,2) - 42*s2*Power(t1,2) - 
         10*Power(s2,2)*Power(t1,2) + 3*Power(s2,3)*Power(t1,2) + 
         16*Power(t1,3) + 9*s2*Power(t1,3) - Power(s2,2)*Power(t1,3) - 
         Power(s,4)*(-3 + 4*s1 + 5*s2 + t1 - 5*t2) - 68*t2 - 89*s2*t2 - 
         10*Power(s2,2)*t2 - Power(s2,3)*t2 - Power(s2,4)*t2 + 
         153*t1*t2 + 73*s2*t1*t2 + 4*Power(s2,2)*t1*t2 + 
         5*Power(s2,3)*t1*t2 - 51*Power(t1,2)*t2 - 4*s2*Power(t1,2)*t2 - 
         4*Power(s2,2)*Power(t1,2)*t2 - 7*Power(t1,3)*t2 - 
         64*Power(t2,2) - 55*s2*Power(t2,2) + 4*Power(s2,2)*Power(t2,2) - 
         2*Power(s2,3)*Power(t2,2) + 35*t1*Power(t2,2) + 
         7*s2*t1*Power(t2,2) - 3*Power(s2,2)*t1*Power(t2,2) + 
         9*Power(t1,2)*Power(t2,2) + 4*s2*Power(t1,2)*Power(t2,2) + 
         Power(t1,3)*Power(t2,2) + 12*Power(t2,3) - 7*s2*Power(t2,3) + 
         4*Power(s2,2)*Power(t2,3) - 9*t1*Power(t2,3) - 
         s2*t1*Power(t2,3) - 3*Power(t1,2)*Power(t2,3) + 4*Power(t2,4) - 
         2*s2*Power(t2,4) + 2*t1*Power(t2,4) + 
         Power(s,3)*(13 + 5*Power(s1,2) + 8*Power(s2,2) + 3*t1 - 
            4*Power(t1,2) + s1*(-2 + 14*s2 - t1 - 11*t2) + 
            2*s2*(-8 + 2*t1 - 9*t2) + 3*t2 + 4*t1*t2 + 6*Power(t2,2)) - 
         Power(s1,3)*(Power(s2,2) + Power(t1,2) + 2*t1*(-6 + t2) - 
            2*s2*(2 + t1 + t2) + 4*(3 + 4*t2)) + 
         Power(s1,2)*(-2*Power(t1,3) - Power(t1,2)*(-14 + t2) + 
            Power(s2,2)*(1 - 2*t1 + 6*t2) + 
            s2*(-55 + 4*Power(t1,2) - 5*t1*(-1 + t2) - 15*t2 - 
               6*Power(t2,2)) + t1*(35 - 33*t2 + 6*Power(t2,2)) + 
            4*(-16 + 9*t2 + 6*Power(t2,2))) - 
         Power(s,2)*(2*Power(s1,3) + 4*Power(s2,3) + 
            Power(s2,2)*(-22 + 8*t1 - 20*t2) + 
            Power(s1,2)*(-3 + 12*s2 - 5*t1 - 8*t2) + 
            s1*(47 + 14*Power(s2,2) - 4*t1 - 7*Power(t1,2) + 
               s2*(-7 + t1 - 28*t2) + 7*t2 + 9*t1*t2 + 10*Power(t2,2)) + 
            s2*(19 - 11*Power(t1,2) + 11*t2 + 16*Power(t2,2) + 
               t1*(16 + 7*t2)) + 
            2*(-13 + Power(t1,3) - 23*t2 - 2*Power(t2,2) - 
               2*Power(t2,3) + Power(t1,2)*(1 + t2) + 
               t1*(4 + t2 - 2*Power(t2,2)))) + 
         s1*(2*Power(s2,4) + Power(t1,3)*(10 + t2) + 
            Power(s2,3)*(-1 - 7*t1 + 2*t2) + 
            Power(t1,2)*(50 - 23*t2 + 5*Power(t2,2)) + 
            4*(17 + 32*t2 - 9*Power(t2,2) - 4*Power(t2,3)) - 
            2*t1*(77 + 35*t2 - 15*Power(t2,2) + 3*Power(t2,3)) + 
            Power(s2,2)*(10 + 8*Power(t1,2) - 5*t2 - 9*Power(t2,2) + 
               t1*(-2 + 5*t2)) + 
            s2*(-3*Power(t1,3) + Power(t1,2)*(1 - 8*t2) + 
               4*t1*(-18 - 3*t2 + Power(t2,2)) + 
               2*(45 + 55*t2 + 9*Power(t2,2) + 3*Power(t2,3)))) + 
         s*(-41 - Power(s2,4) + 35*t1 + 7*Power(t1,2) - 6*Power(t1,3) + 
            Power(s2,3)*(-8 + 8*t1 - 6*t2) + 25*t2 - 41*t1*t2 + 
            4*Power(t1,2)*t2 - Power(t1,3)*t2 + 71*Power(t2,2) - 
            11*t1*Power(t2,2) - Power(t1,2)*Power(t2,2) + 7*Power(t2,3) + 
            t1*Power(t2,3) + 2*Power(t2,4) + 
            Power(s2,2)*(-3 - 10*Power(t1,2) + t1*(11 - 2*t2) + 9*t2 + 
               12*Power(t2,2)) + Power(s1,3)*(3*s2 - 3*t1 - 2*(2 + t2)) + 
            Power(s1,2)*(71 + 7*Power(s2,2) - 2*Power(t1,2) + 
               7*t1*(-1 + t2) + 15*t2 + 6*Power(t2,2) - 
               s2*(8 + 5*t1 + 14*t2)) + 
            s2*(3*Power(t1,3) + Power(t1,2)*(4 + 6*t2) + 
               t1*(-15 + 2*t2 - 3*Power(t2,2)) - 
               4*(-4 + 7*t2 + 3*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(2*Power(s2,3) + 4*Power(t1,3) + 
               Power(s2,2)*(-4 + 9*t1 - 19*t2) + 3*Power(t1,2)*t2 + 
               t1*(39 + 18*t2 - 5*Power(t2,2)) - 
               2*(13 + 71*t2 + 9*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(29 - 15*Power(t1,2) + 20*t2 + 19*Power(t2,2) + 
                  t1*(-6 + 8*t2)))))*R2(1 - s1 - t1 + t2))/
     (Power(s - s2 + t1,2)*(-1 + s1 + t1 - t2)*(s - s1 + t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) - 
    (8*(36 + 43*s2 - 15*Power(s2,2) - 3*Power(s2,3) + 11*Power(s2,4) - 
         4*Power(s2,5) - 115*t1 - 55*s2*t1 + 21*Power(s2,2)*t1 - 
         49*Power(s2,3)*t1 + 6*Power(s2,4)*t1 + 4*Power(s2,5)*t1 + 
         66*Power(t1,2) - 35*s2*Power(t1,2) + 
         87*Power(s2,2)*Power(t1,2) + 35*Power(s2,3)*Power(t1,2) - 
         17*Power(s2,4)*Power(t1,2) + 97*Power(t1,3) + 
         15*s2*Power(t1,3) - 89*Power(s2,2)*Power(t1,3) + 
         17*Power(s2,3)*Power(t1,3) - 100*Power(t1,4) + 
         28*s2*Power(t1,4) - 4*Power(s2,2)*Power(t1,4) + 16*Power(t1,5) + 
         4*s2*Power(t1,5) - Power(s1,5)*
          (Power(s2,3) - Power(s2,2)*(-2 + t1) + (-2 + t1)*Power(t1,2) - 
            s2*(-12 + 4*t1 + Power(t1,2))) + Power(s,7)*(t1 - t2) + 
         24*t2 + 80*s2*t2 + 27*Power(s2,2)*t2 - 14*Power(s2,3)*t2 + 
         11*Power(s2,4)*t2 + 8*Power(s2,5)*t2 - 4*Power(s2,6)*t2 + 
         20*t1*t2 - 21*s2*t1*t2 + 4*Power(s2,2)*t1*t2 - 
         45*Power(s2,3)*t1*t2 - 50*Power(s2,4)*t1*t2 + 
         16*Power(s2,5)*t1*t2 - 234*Power(t1,2)*t2 - 
         224*s2*Power(t1,2)*t2 - 11*Power(s2,2)*Power(t1,2)*t2 + 
         106*Power(s2,3)*Power(t1,2)*t2 - 13*Power(s2,4)*Power(t1,2)*t2 + 
         274*Power(t1,3)*t2 + 209*s2*Power(t1,3)*t2 - 
         28*Power(s2,2)*Power(t1,3)*t2 + Power(s2,3)*Power(t1,3)*t2 - 
         84*Power(t1,4)*t2 - 52*s2*Power(t1,4)*t2 - 
         4*Power(s2,2)*Power(t1,4)*t2 + 4*Power(t1,5)*t2 - 
         48*Power(t2,2) - 20*s2*Power(t2,2) + 
         30*Power(s2,2)*Power(t2,2) + 3*Power(s2,3)*Power(t2,2) + 
         8*Power(s2,4)*Power(t2,2) + Power(s2,6)*Power(t2,2) + 
         180*t1*Power(t2,2) + 211*s2*t1*Power(t2,2) + 
         107*Power(s2,2)*t1*Power(t2,2) - 12*Power(s2,3)*t1*Power(t2,2) - 
         12*Power(s2,4)*t1*Power(t2,2) - 4*Power(s2,5)*t1*Power(t2,2) - 
         193*Power(t1,2)*Power(t2,2) - 332*s2*Power(t1,2)*Power(t2,2) - 
         137*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         11*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         3*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         46*Power(t1,3)*Power(t2,2) + 137*s2*Power(t1,3)*Power(t2,2) + 
         43*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         4*Power(t1,4)*Power(t2,2) - 4*s2*Power(t1,4)*Power(t2,2) - 
         24*Power(t2,3) - 94*s2*Power(t2,3) - 
         60*Power(s2,2)*Power(t2,3) - 5*Power(s2,3)*Power(t2,3) + 
         5*Power(s2,4)*Power(t2,3) - Power(s2,5)*Power(t2,3) + 
         30*t1*Power(t2,3) + 191*s2*t1*Power(t2,3) + 
         128*Power(s2,2)*t1*Power(t2,3) + 30*Power(s2,3)*t1*Power(t2,3) + 
         8*Power(s2,4)*t1*Power(t2,3) + 13*Power(t1,2)*Power(t2,3) - 
         81*s2*Power(t1,2)*Power(t2,3) - 
         50*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         7*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         10*Power(t1,3)*Power(t2,3) - 9*s2*Power(t1,3)*Power(t2,3) + 
         12*Power(t2,4) - 23*s2*Power(t2,4) - 
         43*Power(s2,2)*Power(t2,4) - 18*Power(s2,3)*Power(t2,4) - 
         Power(s2,4)*Power(t2,4) - 17*t1*Power(t2,4) + 
         4*s2*t1*Power(t2,4) + 20*Power(s2,2)*t1*Power(t2,4) - 
         4*Power(s2,3)*t1*Power(t2,4) + 3*Power(t1,2)*Power(t2,4) + 
         13*s2*Power(t1,2)*Power(t2,4) + 
         5*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         Power(t1,3)*Power(t2,4) + 14*s2*Power(t2,5) + 
         5*Power(s2,2)*Power(t2,5) + Power(s2,3)*Power(t2,5) - 
         2*t1*Power(t2,5) - 10*s2*t1*Power(t2,5) + 
         Power(t1,2)*Power(t2,5) - s2*Power(t1,2)*Power(t2,5) + 
         Power(s,6)*(-1 + t1 - 5*s1*t1 + 2*Power(t1,2) - 
            s2*(-2 + s1 + 7*t1 - 6*t2) + t2 + 4*s1*t2 + 2*t1*t2 - 
            4*Power(t2,2)) - Power(s1,4)*
          (-12 + Power(s2,4) + Power(s2,3)*(-1 + 3*t1 - 5*t2) + 
            Power(t1,3)*(5 - 4*t2) + 2*t1*(9 + t2) + 
            Power(t1,2)*(-10 + 7*t2) + 
            Power(s2,2)*(34 - 9*Power(t1,2) - 13*t2 + t1*(17 + 4*t2)) + 
            s2*(22 + 5*Power(t1,3) - 62*t2 + Power(t1,2)*(-37 + 5*t2) + 
               2*t1*(6 + 13*t2))) + 
         Power(s,5)*(2 - 21*t1 + 12*Power(t1,2) + Power(t1,3) + 
            Power(s2,2)*(-8 + 18*t1 - 13*t2) + 
            2*Power(s1,2)*(2*s2 + 5*t1 - 3*t2) + 15*t2 - 14*t1*t2 + 
            7*Power(t1,2)*t2 + 14*Power(t2,2) - 2*t1*Power(t2,2) - 
            6*Power(t2,3) + s2*
             (3 + 5*t1 - 12*Power(t1,2) - 5*t2 - 13*t1*t2 + 
               19*Power(t2,2)) + 
            s1*(3*Power(s2,2) + s2*(-6 + 28*t1 - 23*t2) + 
               2*(1 + 3*t1 - 5*Power(t1,2) - 7*t2 - 4*t1*t2 + 
                  6*Power(t2,2)))) + 
         Power(s1,3)*(24 + Power(s2,5) - 8*Power(t1,4) - 48*t2 + 
            Power(t1,3)*(32 + 14*t2 - 6*Power(t2,2)) + 
            Power(t1,2)*(-38 - 33*t2 + 8*Power(t2,2)) + 
            t1*(-19 + 71*t2 + 8*Power(t2,2)) + 
            Power(s2,4)*(-9*t1 + 4*(3 + t2)) + 
            Power(s2,3)*(1 + 15*Power(t1,2) + 15*t2 - 10*Power(t2,2) + 
               t1*(-50 + 13*t2)) + 
            Power(s2,2)*(32 - 7*Power(t1,3) + 
               Power(t1,2)*(50 - 32*t2) + 145*t2 - 32*Power(t2,2) + 
               t1*(-109 + 31*t2 + 6*Power(t2,2))) + 
            s2*(83 + 89*t2 - 128*Power(t2,2) + 
               5*Power(t1,3)*(4 + 3*t2) + 
               2*Power(t1,2)*(22 - 62*t2 + 5*Power(t2,2)) + 
               2*t1*(-69 + 16*t2 + 32*Power(t2,2)))) + 
         Power(s1,2)*(Power(s2,6) + Power(s2,5)*(9 - 5*t1 - 3*t2) + 
            4*Power(t1,4)*(-1 + 4*t2) + 24*(-2 - 3*t2 + 3*Power(t2,2)) + 
            t1*(169 + 68*t2 - 105*Power(t2,2) - 12*Power(t2,3)) + 
            Power(t1,2)*(-184 + 89*t2 + 39*Power(t2,2) - 
               2*Power(t2,3)) + 
            2*Power(t1,3)*(28 - 37*t2 - 6*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s2,4)*(-15 + 7*Power(t1,2) - 19*t2 - 6*Power(t2,2) + 
               t1*(-25 + 26*t2)) - 
            Power(s2,3)*(27 + 3*Power(t1,3) + 7*t2 + 51*Power(t2,2) - 
               10*Power(t2,3) + Power(t1,2)*(3 + 37*t2) + 
               t1*(-61 - 130*t2 + 21*Power(t2,2))) + 
            Power(s2,2)*(47 - 124*t2 - 231*Power(t2,2) + 
               38*Power(t2,3) + Power(t1,3)*(47 + 14*t2) + 
               2*Power(t1,2)*(-116 - 75*t2 + 21*Power(t2,2)) + 
               t1*(149 + 346*t2 + 9*Power(t2,2) - 4*Power(t2,3))) - 
            s2*(9 + 12*Power(t1,4) + 260*t2 + 135*Power(t2,2) - 
               132*Power(t2,3) + 
               Power(t1,3)*(-190 + 49*t2 + 15*Power(t2,2)) + 
               Power(t1,2)*(354 + 169*t2 - 150*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(-185 - 467*t2 + 24*Power(t2,2) + 76*Power(t2,3)))) + 
         s1*(-8*Power(t1,5) - 2*Power(s2,6)*(-2 + t2) + 
            Power(t1,4)*(108 - 8*Power(t2,2)) + 
            24*(-1 + 4*t2 + 3*Power(t2,2) - 2*Power(t2,3)) + 
            Power(t1,2)*(240 + 377*t2 - 64*Power(t2,2) - 
               19*Power(t2,3) - 2*Power(t2,4)) - 
            Power(t1,3)*(303 + 102*t2 - 52*Power(t2,2) - 
               2*Power(t2,3) + Power(t2,4)) + 
            t1*(-17 - 349*t2 - 79*Power(t2,2) + 69*Power(t2,3) + 
               8*Power(t2,4)) + 
            3*Power(s2,5)*(-4 - 3*t2 + Power(t2,2) + t1*(-4 + 3*t2)) + 
            Power(s2,4)*(-6 + Power(t1,2)*(6 - 10*t2) + 7*t2 + 
               2*Power(t2,2) + 4*Power(t2,3) + 
               t1*(52 + 37*t2 - 25*Power(t2,2))) + 
            Power(s2,3)*(29 + 24*t2 + 11*Power(t2,2) + 53*Power(t2,3) - 
               5*Power(t2,4) + 3*Power(t1,3)*(2 + t2) + 
               Power(t1,2)*(-97 + 14*t2 + 29*Power(t2,2)) + 
               t1*(14 - 49*t2 - 110*Power(t2,2) + 15*Power(t2,3))) - 
            Power(s2,2)*(28 + 77*t2 - 152*Power(t2,2) - 
               163*Power(t2,3) + 22*Power(t2,4) + 
               Power(t1,3)*(-17 + 90*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(-52 - 369*t2 - 150*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(29 + 256*t2 + 365*Power(t2,2) + 43*Power(t2,3) - 
                  Power(t2,4))) + 
            s2*(-83 + 29*t2 + 271*Power(t2,2) + 91*Power(t2,3) - 
               68*Power(t2,4) + 4*Power(t1,4)*(15 + 4*t2) + 
               Power(t1,3)*(-248 - 327*t2 + 38*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(263 + 686*t2 + 206*Power(t2,2) - 
                  76*Power(t2,3) + 5*Power(t2,4)) + 
               4*t1*(4 - 99*t2 - 130*Power(t2,2) + 11*Power(t2,4)))) - 
         Power(s,4)*(1 + 2*t1 + 40*Power(t1,2) - 19*Power(t1,3) + 
            2*Power(s1,3)*(3*s2 + 5*t1 - 2*t2) + 16*t2 - 6*t1*t2 + 
            5*Power(t1,2)*t2 - 4*Power(t1,3)*t2 - 19*Power(t2,2) + 
            12*t1*Power(t2,2) - 8*Power(t1,2)*Power(t2,2) - 
            28*Power(t2,3) + 8*t1*Power(t2,3) + 4*Power(t2,4) + 
            Power(s1,2)*(4 + 11*Power(s2,2) + 26*t1 - 20*Power(t1,2) + 
               s2*(-4 + 42*t1 - 33*t2) - 28*t2 - 12*t1*t2 + 
               12*Power(t2,2)) + 2*Power(s2,3)*(11*t1 - 6*(1 + t2)) + 
            Power(s2,2)*(-24*Power(t1,2) + t1*(28 - 34*t2) + 
               t2*(-10 + 33*t2)) + 
            s2*(20 + 5*Power(t1,3) + 64*t2 + 62*Power(t2,2) - 
               21*Power(t2,3) + Power(t1,2)*(39 + 34*t2) - 
               t1*(102 + 71*t2 + 3*Power(t2,2))) + 
            s1*(-6 + 2*Power(s2,3) + 5*Power(t1,3) + 
               Power(s2,2)*(-22 + 62*t1 - 44*t2) + 15*t2 + 
               56*Power(t2,2) - 12*Power(t2,3) + 
               Power(t1,2)*(22 + 28*t2) - 
               2*t1*(28 + 19*t2 + 3*Power(t2,2)) + 
               s2*(-49*Power(t1,2) + t1*(60 - 39*t2) + 
                  2*t2*(-29 + 24*t2)))) + 
         Power(s,3)*(-7 + 64*t1 - 88*Power(t1,2) - 8*Power(t1,3) + 
            8*Power(t1,4) + Power(s2,4)*(-8 + 13*t1 - 3*t2) + 
            Power(s1,4)*(4*s2 + 5*t1 - t2) - 44*t2 + 61*t1*t2 - 
            59*Power(t1,2)*t2 + 26*Power(t1,3)*t2 - 65*Power(t2,2) + 
            56*t1*Power(t2,2) - 38*Power(t1,2)*Power(t2,2) + 
            6*Power(t1,3)*Power(t2,2) - 9*Power(t2,3) + 
            26*t1*Power(t2,3) + 2*Power(t1,2)*Power(t2,3) + 
            18*Power(t2,4) - 7*t1*Power(t2,4) - Power(t2,5) - 
            2*Power(s2,3)*(4 + 10*Power(t1,2) + 22*t1*(-1 + t2) + 
               3*t2 - 12*Power(t2,2)) + 
            Power(s1,3)*(15*Power(s2,2) + s2*(8 + 28*t1 - 21*t2) - 
               2*(1 + 10*Power(t1,2) + 4*t1*(-4 + t2) + 9*t2 - 
                  2*Power(t2,2))) + 
            Power(s2,2)*(42 + 7*Power(t1,3) + 80*t2 + 96*Power(t2,2) - 
               28*Power(t2,3) + Power(t1,2)*(37 + 56*t2) + 
               t1*(-164 - 125*t2 + 15*Power(t2,2))) + 
            Power(s1,2)*(-8 + 8*Power(s2,3) + 10*Power(t1,3) + 
               Power(s2,2)*(-23 + 77*t1 - 58*t2) - 5*t2 + 
               54*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,2)*(-8 + 42*t2) - 
               2*t1*(29 + 19*t2 + 3*Power(t2,2)) + 
               s2*(4 - 76*Power(t1,2) + t1*(146 - 39*t2) - 121*t2 + 
                  39*Power(t2,2))) + 
            s2*(10 + 65*t2 - 67*Power(t2,2) - 105*Power(t2,3) + 
               9*Power(t2,4) - Power(t1,3)*(52 + 15*t2) + 
               Power(t1,2)*(182 + t2 - 31*Power(t2,2)) + 
               t1*(-52 + 7*t2 + 76*Power(t2,2) + 17*Power(t2,3))) + 
            s1*(17 - 2*Power(s2,4) + Power(s2,3)*(-34 + 68*t1 - 32*t2) + 
               73*t2 + 16*Power(t2,2) - 54*Power(t2,3) + 4*Power(t2,4) - 
               4*Power(t1,3)*(13 + 4*t2) + 
               Power(t1,2)*(126 + 46*t2 - 24*Power(t2,2)) + 
               Power(s2,2)*(14 - 81*Power(t1,2) + t1*(134 - 92*t2) - 
                  73*t2 + 71*Power(t2,2)) + 
               2*t1*(-22 + t2 - 10*Power(t2,2) + 8*Power(t2,3)) + 
               s2*(-30 + 20*Power(t1,3) + 63*t2 + 218*Power(t2,2) - 
                  31*Power(t2,3) + Power(t1,2)*(56 + 107*t2) - 
                  6*t1*(32 + 37*t2 + Power(t2,2))))) - 
         Power(s,2)*(-62 + 92*t1 - 104*Power(t1,2) + 112*Power(t1,3) - 
            20*Power(t1,4) + Power(s1,5)*(s2 + t1) + 
            Power(s1,4)*(-5 + 9*Power(s2,2) - 10*Power(t1,2) + 
               s2*(14 + 7*t1 - 5*t2) + t1*(15 - 2*t2) - 3*t2) - 97*t2 + 
            14*t1*t2 + 33*Power(t1,2)*t2 + 50*Power(t1,3)*t2 - 
            16*Power(t1,4)*t2 + 24*Power(t2,2) - 69*t1*Power(t2,2) - 
            21*Power(t1,2)*Power(t2,2) + 4*Power(t1,3)*Power(t2,2) + 
            72*Power(t2,3) - 4*t1*Power(t2,3) + 
            12*Power(t1,2)*Power(t2,3) - 4*Power(t1,3)*Power(t2,3) + 
            10*Power(t2,4) - 27*t1*Power(t2,4) + 
            2*Power(t1,2)*Power(t2,4) - 3*Power(t2,5) + 
            2*t1*Power(t2,5) + Power(s2,5)*(-2 + 3*t1 + 2*t2) + 
            Power(s2,4)*(-9 - 6*Power(t1,2) + t1*(29 - 28*t2) + 7*t2 + 
               4*Power(t2,2)) + 
            Power(s1,3)*(-50 + 11*Power(s2,3) + 10*Power(t1,3) + 
               Power(s2,2)*(-2 + 39*t1 - 36*t2) + 5*t2 + 
               12*Power(t2,2) + 4*Power(t1,2)*(-9 + 7*t2) - 
               2*t1*(4 + 9*t2 + Power(t2,2)) + 
               s2*(44 + 128*t1 - 54*Power(t1,2) - 104*t2 - 13*t1*t2 + 
                  10*Power(t2,2))) + 
            Power(s2,3)*(36 + 3*Power(t1,3) + 20*t2 + 62*Power(t2,2) - 
               16*Power(t2,3) + Power(t1,2)*(5 + 38*t2) + 
               t1*(-110 - 109*t2 + 35*Power(t2,2))) - 
            Power(s2,2)*(4 - 51*t2 + 93*Power(t2,2) + 127*Power(t2,3) - 
               9*Power(t2,4) + Power(t1,3)*(39 + 14*t2) + 
               Power(t1,2)*(-229 + 12*t2 + 45*Power(t2,2)) - 
               2*t1*(-41 + 58*t2 + 63*Power(t2,2))) + 
            s2*(51 + 12*Power(t1,4) - 14*t2 - 195*Power(t2,2) - 
               67*Power(t2,3) + 62*Power(t2,4) - Power(t2,5) + 
               Power(t1,3)*(-22 + 81*t2 + 15*Power(t2,2)) + 
               Power(t1,2)*(-198 - 356*t2 - 84*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(86 + 234*t2 + 277*Power(t2,2) + 19*Power(t2,3) - 
                  8*Power(t2,4))) - 
            Power(s1,2)*(-37 + 2*Power(s2,4) - 172*t2 - 
               15*Power(t2,2) + 18*Power(t2,3) + 
               6*Power(t1,3)*(7 + 4*t2) + 
               Power(s2,3)*(43 - 73*t1 + 38*t2) + 
               6*Power(t1,2)*(-23 - 14*t2 + 4*Power(t2,2)) - 
               4*t1*(-49 + 3*t2 - 9*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(-13 + 99*Power(t1,2) + 123*t2 - 
                  54*Power(t2,2) + 3*t1*(-75 + 26*t2)) + 
               s2*(70 - 30*Power(t1,3) + Power(t1,2)*(42 - 117*t2) + 
                  155*t2 - 228*Power(t2,2) + 10*Power(t2,3) + 
                  3*t1*(-6 + 79*t2 + Power(t2,2)))) + 
            s1*(80 - 3*Power(s2,5) + 24*Power(t1,4) - 61*t2 - 
               194*Power(t2,2) - 25*Power(t2,3) + 12*Power(t2,4) + 
               2*Power(t1,3)*(-32 + 19*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(-122 - 117*t2 - 60*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(80 + 265*t2 + 66*Power(t2,3) - 7*Power(t2,4)) + 
               Power(s2,4)*(37*t1 - 2*(15 + t2)) + 
               Power(s2,3)*(48 - 55*Power(t1,2) - 19*t2 + 
                  43*Power(t2,2) - 4*t1*(-31 + 27*t2)) + 
               Power(s2,2)*(-22 + 21*Power(t1,3) + 80*t2 + 
                  252*Power(t2,2) - 36*Power(t2,3) + 
                  48*Power(t1,2)*(1 + 3*t2) + 
                  t1*(-292 - 351*t2 + 39*Power(t2,2))) + 
               s2*(-40 + 265*t2 + 178*Power(t2,2) - 200*Power(t2,3) + 
                  5*Power(t2,4) - Power(t1,3)*(124 + 45*t2) - 
                  18*Power(t1,2)*(-28 - 7*t2 + 4*Power(t2,2)) + 
                  t1*(-214 - 295*t2 + 90*Power(t2,2) + 17*Power(t2,3))))) \
+ s*(-91 + 176*t1 - 12*Power(t1,2) - 73*Power(t1,3) - 12*Power(t1,4) + 
            8*Power(t1,5) + 2*Power(s1,5)*
             (3*s2 + Power(s2,2) + t1 - Power(t1,2)) - 76*t2 + 
            Power(s2,6)*t2 - 65*t1*t2 + 352*Power(t1,2)*t2 - 
            194*Power(t1,3)*t2 + 4*Power(t1,4)*t2 + 108*Power(t2,2) - 
            289*t1*Power(t2,2) + 180*Power(t1,2)*Power(t2,2) - 
            12*Power(t1,3)*Power(t2,2) + 8*Power(t1,4)*Power(t2,2) + 
            78*Power(t2,3) - 59*t1*Power(t2,3) + 
            3*Power(t1,2)*Power(t2,3) - 10*Power(t1,3)*Power(t2,3) - 
            17*Power(t2,4) - 7*t1*Power(t2,4) + 
            10*Power(t1,2)*Power(t2,4) + Power(t1,3)*Power(t2,4) - 
            2*Power(t2,5) + 4*t1*Power(t2,5) - Power(t1,2)*Power(t2,5) + 
            Power(s2,5)*(-3 - 7*t1*(-1 + t2) + 11*t2 - 3*Power(t2,2)) + 
            Power(s1,4)*(6*Power(s2,3) + 5*Power(t1,3) + 
               s2*(53 + 33*t1 - 16*Power(t1,2) - 36*t2) + 
               Power(s2,2)*(9 + 5*t1 - 9*t2) + t1*(15 - 4*t2) - 
               2*(9 + t2) + Power(t1,2)*(-20 + 7*t2)) + 
            Power(s2,4)*(16 - 19*t2 + 14*Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,2)*(-5 + 9*t2) + 
               t1*(-31 - 57*t2 + 23*Power(t2,2))) - 
            Power(s2,3)*(24 + 9*t2 + 53*Power(t2,2) + 55*Power(t2,3) - 
               5*Power(t2,4) + 3*Power(t1,3)*(2 + t2) + 
               Power(t1,2)*(-104 - 5*t2 + 25*Power(t2,2)) + 
               t1*(34 - 153*t2 - 74*Power(t2,2) + 17*Power(t2,3))) + 
            Power(s2,2)*(49 + 36*t2 - 117*Power(t2,2) - 45*Power(t2,3) + 
               58*Power(t2,4) - Power(t2,5) + 
               Power(t1,3)*(-25 + 66*t2 + 7*Power(t2,2)) + 
               3*Power(t1,2)*
                (-51 - 133*t2 - 17*Power(t2,2) + 6*Power(t2,3)) + 
               t1*(89 + 206*t2 + 217*Power(t2,2) - 25*Power(t2,3) + 
                  Power(t2,4))) - 
            s2*(-1 + 104*t2 + 78*Power(t2,2) - 116*Power(t2,3) - 
               77*Power(t2,4) + 12*Power(t2,5) + 
               8*Power(t1,4)*(5 + 2*t2) + 
               Power(t1,3)*(-264 - 151*t2 + 38*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(182 + 131*t2 - 85*Power(t2,2) - 
                  57*Power(t2,3) + Power(t2,4)) + 
               t1*(23 - 86*t2 + 50*Power(t2,2) + 166*Power(t2,3) + 
                  39*Power(t2,4))) + 
            Power(s1,3)*(-67 + Power(s2,4) + 
               Power(s2,3)*(-22 + 30*t1 - 23*t2) + 71*t2 + 
               8*Power(t2,2) - 4*Power(t1,3)*(1 + 4*t2) + 
               Power(t1,2)*(42 + 50*t2 - 8*Power(t2,2)) - 
               2*t1*(-6 + 19*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(37 - 51*Power(t1,2) - 85*t2 + 
                  16*Power(t2,2) - 8*t1*(-17 + 2*t2)) + 
               s2*(-66 + 20*Power(t1,3) - 236*t2 + 84*Power(t2,2) - 
                  20*t1*(-7 + 3*t2) + Power(t1,2)*(-96 + 49*t2))) - 
            Power(s1,2)*(-97 + 4*Power(s2,5) - 24*Power(t1,4) - 212*t2 + 
               105*Power(t2,2) + 12*Power(t2,3) + 
               Power(s2,4)*(33 - 33*t1 + 4*t2) + 
               2*Power(t1,3)*(44 + t2 - 9*Power(t2,2)) + 
               t1*(293 + 83*t2 - 24*Power(t2,2) - 16*Power(t2,3)) + 
               Power(t1,2)*(-260 + 81*t2 + 30*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(s2,3)*(-28 + 50*Power(t1,2) + 11*t2 - 
                  33*Power(t2,2) + t1*(-130 + 77*t2)) + 
               Power(s2,2)*(19 - 21*Power(t1,3) + 
                  Power(t1,2)*(39 - 120*t2) + 119*t2 - 
                  201*Power(t2,2) + 14*Power(t2,3) + 
                  t1*(11 + 297*t2 - 18*Power(t2,2))) + 
               s2*(82 - 248*t2 - 390*Power(t2,2) + 96*Power(t2,3) + 
                  Power(t1,3)*(92 + 45*t2) + 
                  2*t1*(100 + 223*t2 + 9*Power(t2,2)) + 
                  3*Power(t1,2)*(-106 - 83*t2 + 17*Power(t2,2)))) + 
            s1*(79 - Power(s2,6) - 205*t2 - 223*Power(t2,2) + 
               69*Power(t2,3) + 8*Power(t2,4) - 
               16*Power(t1,4)*(1 + 2*t2) + 
               Power(s2,5)*(-16 + 8*t1 + 7*t2) + 
               Power(s2,4)*(44 - 13*Power(t1,2) - 56*t1*(-1 + t2) + 
                  19*t2 + 5*Power(t2,2)) + 
               4*Power(t1,3)*
                (70 + 25*t2 + 4*Power(t2,2) - 2*Power(t2,3)) + 
               t1*(88 + 582*t2 + 130*Power(t2,2) + 6*Power(t2,3) - 
                  14*Power(t2,4)) + 
               2*Power(t1,2)*(-224 - 220*t2 + 18*Power(t2,2) - 
                  5*Power(t2,3) + Power(t2,4)) + 
               Power(s2,3)*(8 + 6*Power(t1,3) + 25*t2 + 88*Power(t2,2) - 
                  21*Power(t2,3) + Power(t1,2)*(8 + 75*t2) + 
                  4*t1*(-52 - 51*t2 + 16*Power(t2,2))) - 
               Power(s2,2)*(78 - 136*t2 - 127*Power(t2,2) + 
                  183*Power(t2,3) - 6*Power(t2,4) + 
                  Power(t1,3)*(86 + 28*t2) + 
                  Power(t1,2)*(-461 - 90*t2 + 87*Power(t2,2)) + 
                  2*t1*(83 + 103*t2 - 93*Power(t2,2) + 4*Power(t2,3))) + 
               s2*(88 + 24*Power(t1,4) + 160*t2 - 298*Power(t2,2) - 
                  284*Power(t2,3) + 54*Power(t2,4) + 
                  2*Power(t1,3)*(-86 + 65*t2 + 15*Power(t2,2)) + 
                  Power(t1,2)*
                   (36 - 403*t2 - 210*Power(t2,2) + 19*Power(t2,3)) + 
                  2*t1*(9 + 125*t2 + 236*Power(t2,2) + 42*Power(t2,3))))))*
       T2(1 - s + s2 - t1,1 - s1 - t1 + t2))/
     (Power(s - s2 + t1,2)*(-1 + s1 + t1 - t2)*(s - s1 + t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) + 
    (8*(12*s2 + 32*Power(s2,2) + 15*Power(s2,3) - 4*Power(s2,4) - 12*t1 - 
         100*s2*t1 - 111*Power(s2,2)*t1 - 6*Power(s2,3)*t1 + 
         4*Power(s2,4)*t1 + 68*Power(t1,2) + 213*s2*Power(t1,2) + 
         78*Power(s2,2)*Power(t1,2) - 9*Power(s2,3)*Power(t1,2) - 
         117*Power(t1,3) - 134*s2*Power(t1,3) + Power(s2,2)*Power(t1,3) + 
         66*Power(t1,4) + 9*s2*Power(t1,4) - 5*Power(t1,5) + 
         Power(s1,4)*(-Power(s2,3) + (-2 + t1)*Power(t1,2) + 
            Power(s2,2)*(-2 + 3*t1) + s2*(4 + 8*t1 - 3*Power(t1,2))) + 
         32*t2 + 54*s2*t2 + 58*Power(s2,2)*t2 + 31*Power(s2,3)*t2 + 
         12*Power(s2,4)*t2 - 4*Power(s2,5)*t2 - 134*t1*t2 - 
         208*s2*t1*t2 - 135*Power(s2,2)*t1*t2 - 66*Power(s2,3)*t1*t2 + 
         8*Power(s2,4)*t1*t2 + 214*Power(t1,2)*t2 + 
         199*s2*Power(t1,2)*t2 + 106*Power(s2,2)*Power(t1,2)*t2 + 
         3*Power(s2,3)*Power(t1,2)*t2 - 111*Power(t1,3)*t2 - 
         46*s2*Power(t1,3)*t2 - 13*Power(s2,2)*Power(t1,3)*t2 - 
         6*Power(t1,4)*t2 + 5*s2*Power(t1,4)*t2 + Power(t1,5)*t2 + 
         36*Power(t2,2) + 71*s2*Power(t2,2) + 
         54*Power(s2,2)*Power(t2,2) + 42*Power(s2,3)*Power(t2,2) - 
         3*Power(s2,4)*Power(t2,2) + Power(s2,5)*Power(t2,2) - 
         91*t1*Power(t2,2) - 132*s2*t1*Power(t2,2) - 
         126*Power(s2,2)*t1*Power(t2,2) + 2*Power(s2,3)*t1*Power(t2,2) - 
         4*Power(s2,4)*t1*Power(t2,2) + 42*Power(t1,2)*Power(t2,2) + 
         83*s2*Power(t1,2)*Power(t2,2) + 
         4*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         21*Power(t1,3)*Power(t2,2) + 2*s2*Power(t1,3)*Power(t2,2) - 
         Power(t1,4)*Power(t2,2) - s2*Power(t1,4)*Power(t2,2) + 
         4*Power(t2,3) + 33*s2*Power(t2,3) + 38*Power(s2,2)*Power(t2,3) - 
         4*Power(s2,3)*Power(t2,3) + 2*Power(s2,4)*Power(t2,3) + 
         3*t1*Power(t2,3) - 50*s2*t1*Power(t2,3) + 
         16*Power(s2,2)*t1*Power(t2,3) - 2*Power(s2,3)*t1*Power(t2,3) - 
         8*Power(t1,2)*Power(t2,3) - 15*s2*Power(t1,2)*Power(t2,3) - 
         2*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         Power(t1,3)*Power(t2,3) + 2*s2*Power(t1,3)*Power(t2,3) + 
         6*s2*Power(t2,4) - 5*Power(s2,2)*Power(t2,4) - 
         Power(s2,3)*Power(t2,4) - 2*t1*Power(t2,4) + 
         8*s2*t1*Power(t2,4) + 2*Power(s2,2)*t1*Power(t2,4) + 
         Power(t1,2)*Power(t2,4) - s2*Power(t1,2)*Power(t2,4) + 
         Power(s,4)*(t1 - t2)*
          (-2 + Power(s1,2) - Power(s2,2) + Power(t1,2) - 2*t1*t2 + 
            Power(t2,2) - 2*s1*(s2 - t1 + t2) + s2*(4 - 2*t1 + 2*t2)) + 
         Power(s1,3)*(-4 + 2*Power(s2,4) + 2*Power(t1,4) + 
            2*t1*(-1 + t2) + 5*Power(t1,2)*(2 + t2) - 
            Power(t1,3)*(4 + 3*t2) + Power(s2,3)*(6 - 8*t1 + 4*t2) + 
            Power(s2,2)*(-28 + 12*Power(t1,2) + 11*t2 - 
               t1*(16 + 11*t2)) - 
            2*s2*(17 + 4*Power(t1,3) + 9*t2 - Power(t1,2)*(9 + 5*t2) + 
               t1*(-19 + 16*t2))) + 
         Power(s1,2)*(Power(s2,5) + Power(t1,5) + 12*(3 + t2) - 
            2*Power(t1,4)*(1 + 2*t2) - Power(s2,4)*(-2 + t1 + 2*t2) + 
            t1*(-81 + 7*t2 - 6*Power(t2,2)) + 
            Power(t1,2)*(32 - 28*t2 - 3*Power(t2,2)) + 
            Power(t1,3)*(21 + 7*t2 + 3*Power(t2,2)) - 
            2*Power(s2,3)*(-16 + 2*Power(t1,2) + t1*(2 - 7*t2) + 8*t2 + 
               3*Power(t2,2)) + 
            Power(s2,2)*(36 + 8*Power(t1,3) + 94*t2 - 
               26*Power(t1,2)*t2 - 21*Power(t2,2) + 
               t1*(-92 + 48*t2 + 15*Power(t2,2))) + 
            s2*(61 - 5*Power(t1,4) + 101*t2 + 30*Power(t2,2) + 
               2*Power(t1,3)*(2 + 9*t2) + 
               Power(t1,2)*(59 - 51*t2 - 12*Power(t2,2)) + 
               2*t1*(-52 - 63*t2 + 24*Power(t2,2)))) - 
         s1*(2*Power(s2,5)*(-2 + t2) + Power(t1,5)*t2 + 
            Power(s2,4)*(16 + t1*(4 - 5*t2) - t2 + 2*Power(t2,2)) - 
            Power(t1,4)*(8 + 3*t2 + 2*Power(t2,2)) + 
            4*(8 + 18*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(182 + 74*t2 - 26*Power(t2,2) + Power(t2,3)) + 
            Power(t1,3)*(-88 + 42*t2 + 2*Power(t2,2) + Power(t2,3)) - 
            2*t1*(61 + 86*t2 - 4*Power(t2,2) + 3*Power(t2,3)) + 
            2*Power(s2,3)*(17 + 7*Power(t1,2) + 37*t2 - 7*Power(t2,2) - 
               2*Power(t2,3) + t1*(-40 - t2 + 2*Power(t2,2))) - 
            2*s2*(-21 - 66*t2 - 50*Power(t2,2) - 11*Power(t2,3) + 
               Power(t1,4)*(-5 + 3*t2) + 
               Power(t1,3)*(22 - 3*t2 - 6*Power(t2,2)) + 
               t1*(82 + 118*t2 + 69*Power(t2,2) - 16*Power(t2,3)) + 
               Power(t1,2)*(-80 - 71*t2 + 24*Power(t2,2) + 
                  3*Power(t2,3))) + 
            Power(s2,2)*(46 + 8*Power(t1,3)*(-3 + t2) + 90*t2 + 
               104*Power(t2,2) - 17*Power(t2,3) - 
               4*Power(t1,2)*(-29 + 4*Power(t2,2)) + 
               t1*(-122 - 218*t2 + 48*Power(t2,2) + 9*Power(t2,3)))) + 
         Power(s,3)*(10 + 12*t1 - 21*Power(t1,2) - 2*Power(t1,3) + 
            2*Power(t1,4) - 12*t2 + 32*t1*t2 + 8*Power(t1,2)*t2 - 
            5*Power(t1,3)*t2 - 11*Power(t2,2) - 10*t1*Power(t2,2) + 
            3*Power(t1,2)*Power(t2,2) + 4*Power(t2,3) + t1*Power(t2,3) - 
            Power(t2,4) + Power(s2,3)*(2 + t2) + 
            Power(s1,3)*(-s2 - 2*t1 + t2) + 
            Power(s1,2)*(-9 + 2*Power(s2,2) - 2*Power(t1,2) + 4*t2 - 
               3*Power(t2,2) + s2*(10 - 2*t1 + 3*t2) + t1*(-2 + 5*t2)) + 
            Power(s2,2)*(17 + 6*Power(t1,2) + 24*t2 + 11*Power(t2,2) - 
               t1*(26 + 17*t2)) + 
            s2*(-28 - 8*Power(t1,3) - 4*t2 + 14*Power(t2,2) + 
               Power(t2,3) + Power(t1,2)*(30 + 17*t2) - 
               2*t1*(-3 + 22*t2 + 5*Power(t2,2))) - 
            s1*(3*Power(s2,3) - 2*Power(t1,3) + Power(t1,2)*(4 + t2) + 
               Power(s2,2)*(16 - 12*t1 + 13*t2) + 
               t2*(-20 + 8*t2 - 3*Power(t2,2)) + 
               2*t1*(11 - 6*t2 + 2*Power(t2,2)) + 
               s2*(-16 + 9*Power(t1,2) + 24*t2 + 3*Power(t2,2) - 
                  12*t1*(2 + t2)))) + 
         Power(s,2)*(-30 - 10*t1 + 91*Power(t1,2) - 44*Power(t1,3) - 
            2*Power(t1,4) + Power(t1,5) + Power(s1,4)*(s2 + t1) + 
            Power(s2,4)*(-2 + t1 - 3*t2) + 40*t2 - 140*t1*t2 + 
            59*Power(t1,2)*t2 + 9*Power(t1,3)*t2 - Power(t1,4)*t2 + 
            49*Power(t2,2) - 4*t1*Power(t2,2) - 
            9*Power(t1,2)*Power(t2,2) - 3*Power(t1,3)*Power(t2,2) - 
            11*Power(t2,3) - t1*Power(t2,3) + 
            5*Power(t1,2)*Power(t2,3) + 3*Power(t2,4) - 
            2*t1*Power(t2,4) - 
            Power(s1,3)*(-7 + 2*Power(s2,2) + 2*Power(t1,2) + 
               t1*(-4 + t2) + 3*t2 + s2*(13 - 6*t1 + 6*t2)) - 
            Power(s2,3)*(26 + 6*Power(t1,2) + 24*t2 + 9*Power(t2,2) - 
               t1*(29 + 18*t2)) + 
            Power(s2,2)*(10*Power(t1,3) - 2*Power(t1,2)*(33 + 10*t2) + 
               t1*(80 + 57*t2 + Power(t2,2)) + 
               3*(-3 - 9*t2 + Power(t2,2) + 3*Power(t2,3))) + 
            s2*(72 - 6*Power(t1,4) + 26*t2 - 28*Power(t2,2) + 
               16*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,3)*(41 + 6*t2) + 
               Power(t1,2)*(-8 - 42*t2 + 9*Power(t2,2)) - 
               t1*(116 - 42*t2 + 15*Power(t2,2) + 12*Power(t2,3))) + 
            Power(s1,2)*(37 + 3*Power(s2,3) - 6*Power(t1,3) - 25*t2 + 
               9*Power(t2,2) + Power(t1,2)*(6 + 9*t2) + 
               Power(s2,2)*(12 - 12*t1 + 13*t2) - 
               3*t1*(2 + 3*t2 + Power(t2,2)) + 
               s2*(-34 + 15*Power(t1,2) + 42*t2 + 12*Power(t2,2) - 
                  3*t1*(7 + 8*t2))) + 
            s1*(-18 + 4*Power(s2,4) - 2*Power(t1,4) - 86*t2 + 
               9*Power(t1,3)*t2 + 29*Power(t2,2) - 9*Power(t2,3) + 
               Power(s2,3)*(21 - 12*t1 + 6*t2) - 
               3*Power(t1,2)*(15 - t2 + 4*Power(t2,2)) + 
               t1*(92 + 10*t2 + 6*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*(-7 + 6*Power(t1,2) - 15*t2 - 
                  20*Power(t2,2) + t1*(-24 + 11*t2)) + 
               s2*(-16 + 4*Power(t1,3) + Power(t1,2)*(9 - 24*t2) + 
                  62*t2 - 45*Power(t2,2) - 10*Power(t2,3) + 
                  6*t1*(-2 + 6*t2 + 5*Power(t2,2))))) + 
         s*(20 + 2*t1 - 133*Power(t1,2) + 145*Power(t1,3) - 
            30*Power(t1,4) - 2*Power(s1,4)*
             (s2*(-3 + t1) - (-1 + t1)*t1) - 62*t2 + Power(s2,5)*t2 + 
            242*t1*t2 - 239*Power(t1,2)*t2 + 23*Power(t1,3)*t2 + 
            2*Power(t1,4)*t2 + Power(t1,5)*t2 - 79*Power(t2,2) + 
            91*t1*Power(t2,2) + 28*Power(t1,2)*Power(t2,2) - 
            3*Power(t1,4)*Power(t2,2) + 3*Power(t2,3) - 
            19*t1*Power(t2,3) - 6*Power(t1,2)*Power(t2,3) + 
            3*Power(t1,3)*Power(t2,3) - 2*Power(t2,4) + 
            4*t1*Power(t2,4) - Power(t1,2)*Power(t2,4) - 
            Power(s2,4)*(-13 - 12*t2 + Power(t2,2) + 3*t1*(3 + t2)) + 
            Power(s1,3)*(-2 + Power(s2,3) + 17*t1 + 2*Power(t1,3) + 
               2*t2 + 2*t1*t2 - 5*Power(t1,2)*t2 + 
               Power(s2,2)*(-5 - 2*t1 + t2) - 
               s2*(-25 + Power(t1,2) + t1*(3 - 4*t2) + 24*t2)) + 
            Power(s2,3)*(25 + Power(t1,2)*(31 - 2*t2) + 13*t2 - 
               2*Power(t2,2) - 11*Power(t2,3) + 
               t1*(-72 - 25*t2 + 16*Power(t2,2))) + 
            Power(s2,2)*(-49 - 25*t2 - 19*Power(t2,2) - 4*Power(t2,3) - 
               Power(t2,4) + 5*Power(t1,3)*(-7 + 2*t2) + 
               Power(t1,2)*(95 - 28*Power(t2,2)) + 
               t1*(13 - 21*t2 + 33*Power(t2,2) + 19*Power(t2,3))) + 
            s2*(-46 + Power(t1,4)*(13 - 7*t2) - 62*t2 - 31*Power(t2,2) - 
               31*Power(t2,3) + 6*Power(t2,4) + 
               Power(t1,3)*(-6 + 11*t2 + 16*Power(t2,2)) - 
               Power(t1,2)*(187 - 9*t2 + 39*Power(t2,2) + 
                  11*Power(t2,3)) + 
               t1*(210 + 122*t2 + 34*Power(t2,2) + 9*Power(t2,3) + 
                  2*Power(t2,4))) - 
            Power(s1,2)*(69 + 6*Power(s2,4) + 2*Power(t1,4) + 
               Power(t1,3)*(-6 + t2) - 7*t2 + 6*Power(t2,2) + 
               Power(s2,3)*(12 - 20*t1 + 13*t2) + 
               t1*(-69 + 53*t2 - 6*Power(t2,2)) - 
               3*Power(t1,2)*(8 - 2*t2 + Power(t2,2)) + 
               s2*(1 - 12*Power(t1,3) + 81*t2 - 36*Power(t2,2) - 
                  5*t1*(2 + 3*t2) + Power(t1,2)*(39 + 9*t2)) + 
               Power(s2,2)*(5 + 24*Power(t1,2) - 6*t2 + 3*Power(t2,2) - 
                  t1*(33 + 23*t2))) + 
            s1*(50 - Power(s2,5) - 2*Power(t1,5) + 148*t2 - 
               8*Power(t2,2) + 6*Power(t2,3) + Power(t1,4)*(4 + 5*t2) + 
               Power(s2,4)*(-13 + 7*t2) - 
               Power(t1,3)*(15 + 6*t2 + 4*Power(t2,2)) + 
               Power(s2,3)*(11 + 10*Power(t1,2) + t1*(9 - 36*t2) + 
                  14*t2 + 23*Power(t2,2)) + 
               Power(t1,2)*(180 - 52*t2 + 12*Power(t2,2) + Power(t2,3)) - 
               t1*(188 + 160*t2 - 55*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,2)*(-18*Power(t1,3) + Power(t1,2)*(33 + 52*t2) - 
                  t1*(33 + 66*t2 + 40*Power(t2,2)) + 
                  3*(10 + 8*t2 + Power(t2,2) + Power(t2,3))) + 
               s2*(28 + 11*Power(t1,4) + 32*t2 + 87*Power(t2,2) - 
                  24*Power(t2,3) - Power(t1,3)*(33 + 28*t2) + 
                  3*Power(t1,2)*(3 + 26*t2 + 7*Power(t2,2)) - 
                  t1*(72 + 44*t2 + 21*Power(t2,2) + 4*Power(t2,3))))))*
       T3(1 - s + s1 - t2,1 - s + s2 - t1))/
     (Power(s - s2 + t1,2)*(-1 + s1 + t1 - t2)*(s - s1 + t2)*
       (-s1 + s2 - t1 + t2)*(1 - s + s*s2 - s1*s2 - t1 + s2*t2)) + 
    (8*(32 - 8*s2 - 72*t1 + 16*s2*t1 + 48*Power(t1,2) - 
         8*s2*Power(t1,2) - 8*Power(t1,3) + 
         Power(s1,3)*(Power(s2,3) - (-2 + t1)*Power(t1,2) - 
            Power(s2,2)*(2 + 3*t1) + s2*(-4 - 4*t1 + 3*Power(t1,2))) + 
         Power(s,5)*(t1 - t2) + 36*t2 + 25*s2*t2 - 4*Power(s2,2)*t2 - 
         45*t1*t2 - 42*s2*t1*t2 + 4*Power(s2,2)*t1*t2 + 
         10*Power(t1,2)*t2 + 17*s2*Power(t1,2)*t2 - Power(t1,3)*t2 + 
         4*Power(t2,2) + 41*s2*Power(t2,2) - 12*Power(s2,2)*Power(t2,2) + 
         4*Power(s2,3)*Power(t2,2) - 5*t1*Power(t2,2) - 
         4*s2*t1*Power(t2,2) - 8*Power(s2,2)*t1*Power(t2,2) - 
         4*Power(t1,2)*Power(t2,2) - s2*Power(t1,2)*Power(t2,2) + 
         Power(t1,3)*Power(t2,2) + 6*s2*Power(t2,3) + 
         3*Power(s2,2)*Power(t2,3) - Power(s2,3)*Power(t2,3) - 
         2*t1*Power(t2,3) + 2*Power(s2,2)*t1*Power(t2,3) + 
         Power(t1,2)*Power(t2,3) - s2*Power(t1,2)*Power(t2,3) - 
         Power(s,4)*(1 + (2 + 3*s1)*t1 - 2*Power(t1,2) + 
            s2*(-2 + s1 + 4*t1 - 3*t2) - 4*t2 - 2*s1*t2 + 2*Power(t2,2)) \
+ Power(s,3)*(1 - 6*t1 - 2*Power(t1,2) + Power(t1,3) + 
            Power(s2,2)*(-2 + 3*t1 - t2) + 
            Power(s1,2)*(2*s2 + 3*t1 - t2) - 4*t2 + 3*t1*t2 + 
            3*Power(t1,2)*t2 + 7*Power(t2,2) - 3*t1*Power(t2,2) - 
            Power(t2,3) - s2*
             (2 - 11*t1 + 4*Power(t1,2) + 6*t2 + 6*t1*t2 - 
               6*Power(t2,2)) + 
            s1*(4 + 6*t1 - 6*Power(t1,2) + s2*(-3 + 10*t1 - 8*t2) - 
               7*t2 + 2*Power(t2,2))) + 
         Power(s1,2)*(4 + Power(s2,3)*(4 - 3*t2) + 2*Power(t1,3)*t2 - 
            2*t1*(1 + t2) - 3*Power(t1,2)*(2 + t2) + 
            Power(s2,2)*(-8 + 7*t2 + 4*t1*(-3 + 2*t2)) + 
            s2*(38 + Power(t1,2)*(4 - 7*t2) + 14*t2 + t1*(-6 + 8*t2))) + 
         s1*(10*Power(t1,2)*(-1 + t2) - 4*(9 + 2*t2) + 
            Power(s2,3)*t2*(-8 + 3*t2) - 
            Power(t1,3)*(-1 + t2 + Power(t2,2)) + 
            t1*(45 + 7*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*(4 + 20*t2 - 8*Power(t2,2) + 
               t1*(-4 + 20*t2 - 7*Power(t2,2))) + 
            s2*(-25 - 79*t2 - 16*Power(t2,2) + 
               t1*(42 + 10*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(-17 - 3*t2 + 5*Power(t2,2)))) + 
         Power(s,2)*(-1 + 3*t1 - 6*Power(t1,2) - Power(s1,3)*(s2 + t1) - 
            4*t2 - Power(s2,3)*t2 - 13*t1*t2 + 2*Power(t1,3)*t2 - 
            5*Power(t2,2) + 9*t1*Power(t2,2) + 3*Power(t2,3) - 
            2*t1*Power(t2,3) + 
            s2*(-5 + Power(t1,2)*(7 - 9*t2) + 9*t2 - 10*Power(t2,2) + 
               3*Power(t2,3) + 6*t1*(1 + t2)) + 
            Power(s1,2)*(3*(-1 - 2*t1 + 2*Power(t1,2) + t2) + 
               s2*(3 - 8*t1 + 5*t2)) + 
            Power(s2,2)*(7 + 2*t2 - 2*Power(t2,2) + t1*(-11 + 8*t2)) + 
            s1*(1 + Power(s2,3) - 3*Power(t1,3) - 
               6*Power(t1,2)*(-1 + t2) + 8*t2 - 6*Power(t2,2) + 
               Power(s2,2)*(3 - 9*t1 + 2*t2) + 
               3*t1*(5 - t2 + Power(t2,2)) + 
               s2*(-5 + 11*Power(t1,2) + 7*t2 - 7*Power(t2,2) + 
                  4*t1*(-5 + 2*t2)))) + 
         s*(-36 + 2*Power(s1,3)*(s2 - t1)*(-1 + t1) + 43*t1 - 
            6*Power(t1,2) - Power(t1,3) - 37*t2 + 6*t1*t2 - 
            10*Power(t1,2)*t2 + Power(t1,3)*t2 - 
            2*Power(s2,3)*(-2 + t2)*t2 - 5*Power(t2,2) - 
            9*t1*Power(t2,2) + 3*Power(t1,2)*Power(t2,2) + 
            Power(t1,3)*Power(t2,2) - 2*Power(t2,3) + 4*t1*Power(t2,3) - 
            Power(t1,2)*Power(t2,3) + 
            Power(s1,2)*(-2*Power(s2,3) - 9*t1 + 3*Power(t1,3) + 
               Power(s2,2)*(1 + 9*t1 - t2) + 3*Power(t1,2)*(-2 + t2) - 
               2*(1 + t2) + s2*
                (3 - 10*Power(t1,2) + t1*(13 - 2*t2) + 2*t2)) - 
            Power(s2,2)*(12 + 13*t2 - 7*Power(t2,2) + Power(t2,3) + 
               t1*(-12 + 19*t2 - 7*Power(t2,2))) + 
            s2*(49 + 44*t2 + 9*Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,2)*(9 + 6*t2 - 6*Power(t2,2)) + 
               t1*(-58 + 18*t2 - 5*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(37 + 4*Power(s2,3)*(-1 + t2) + 7*t2 - 4*Power(t1,3)*t2 + 
               4*Power(t2,2) + 3*Power(t1,2)*(4 + t2) + 
               Power(s2,2)*(9 + t1*(23 - 16*t2) - 8*t2 + 2*Power(t2,2)) - 
               3*t1*(3 - 6*t2 + 2*Power(t2,2)) + 
               s2*(-41 - 12*t2 + 2*Power(t2,2) + 
                  Power(t1,2)*(-11 + 16*t2) - 
                  2*t1*(8 + 4*t2 + Power(t2,2))))))*T4(-s + s1 - t2))/
     (Power(s - s2 + t1,2)*(-1 + s1 + t1 - t2)*(s - s1 + t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)) + 
    (8*(-20 - 17*s2 - 4*Power(s2,2) + 57*t1 + 39*s2*t1 + 
         8*Power(s2,2)*t1 - 51*Power(t1,2) - 27*s2*Power(t1,2) - 
         4*Power(s2,2)*Power(t1,2) + 11*Power(t1,3) + 5*s2*Power(t1,3) + 
         3*Power(t1,4) - Power(s1,3)*
          (Power(s2,3) + Power(s2,2)*(2 - 3*t1) - (-2 + t1)*Power(t1,2) + 
            s2*(4 - 8*t1 + 3*Power(t1,2))) + 
         Power(s,4)*(t1 - t2)*(-1 + s1 + t1 - t2) - 32*t2 - 14*s2*t2 - 
         20*Power(s2,2)*t2 - 4*Power(s2,3)*t2 + 34*t1*t2 + 31*s2*t1*t2 + 
         24*Power(s2,2)*t1*t2 + 4*Power(s2,3)*t1*t2 + 9*Power(t1,2)*t2 - 
         12*s2*Power(t1,2)*t2 - 4*Power(s2,2)*Power(t1,2)*t2 - 
         12*Power(t1,3)*t2 - 5*s2*Power(t1,3)*t2 + Power(t1,4)*t2 + 
         4*Power(t2,2) - 35*s2*Power(t2,2) + 13*Power(s2,2)*Power(t2,2) - 
         7*Power(s2,3)*Power(t2,2) - 5*t1*Power(t2,2) - 
         6*s2*t1*Power(t2,2) + 5*Power(s2,2)*t1*Power(t2,2) - 
         Power(s2,3)*t1*Power(t2,2) + 5*Power(t1,2)*Power(t2,2) + 
         10*s2*Power(t1,2)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         s2*Power(t1,3)*Power(t2,2) + 2*s2*Power(t2,3) + 
         Power(s2,2)*Power(t2,3) + Power(s2,3)*Power(t2,3) + 
         2*t1*Power(t2,3) - 4*s2*t1*Power(t2,3) - 
         2*Power(s2,2)*t1*Power(t2,3) - Power(t1,2)*Power(t2,3) + 
         s2*Power(t1,2)*Power(t2,3) + 
         Power(s,3)*(13 + 8*Power(s2,2) + t1 - 4*Power(t1,2) + 
            2*Power(t1,3) - 3*t2 + 7*t1*t2 - 3*Power(t1,2)*t2 - 
            3*Power(t2,2) + Power(t2,3) + Power(s1,2)*(-s2 - 2*t1 + t2) - 
            s2*(22 - 6*t1 + 4*Power(t1,2) + 5*t2 - 7*t1*t2 + 
               3*Power(t2,2)) + 
            s1*(-5 + 3*t2 + 2*t1*t2 - 2*Power(t2,2) + 
               s2*(7 - 5*t1 + 4*t2))) + 
         Power(s1,2)*(4 + Power(t1,4) - Power(s2,3)*(7 + t1 - 3*t2) + 
            2*t1*(-5 + t2) + 3*Power(t1,2)*(4 + t2) - 
            Power(t1,3)*(3 + 2*t2) + 
            Power(s2,2)*(6 + 3*Power(t1,2) + t1*(11 - 8*t2) + 5*t2) + 
            s2*(-3*Power(t1,3) + 10*(-3 + t2) + 7*Power(t1,2)*(1 + t2) - 
               2*t1*(3 + 10*t2))) + 
         s1*(-8*(-4 + t2) - Power(t1,4)*t2 - Power(t1,2)*(6 + 17*t2) + 
            t1*(-35 + 15*t2 - 4*Power(t2,2)) + 
            Power(s2,3)*(4 + 2*t1*(-2 + t2) + 14*t2 - 3*Power(t2,2)) + 
            Power(t1,3)*(9 + 3*t2 + Power(t2,2)) - 
            Power(s2,2)*(-24 + 19*t2 + 4*Power(t2,2) + 
               Power(t1,2)*(-8 + 5*t2) + t1*(32 + 16*t2 - 7*Power(t2,2))) \
+ s2*(15 + 65*t2 + 4*Power(t1,3)*t2 - 8*Power(t2,2) + 
               Power(t1,2)*(23 - 17*t2 - 5*Power(t2,2)) + 
               2*t1*(-19 + 6*t2 + 8*Power(t2,2)))) + 
         Power(s,2)*(-13 - 8*Power(s2,3) + 43*t1 - 3*Power(t1,3) + 
            Power(t1,4) + Power(s1,3)*(s2 + t1) - 2*t2 - 10*t1*t2 + 
            6*Power(t1,2)*t2 + 4*Power(t2,2) - 
            3*Power(t1,2)*Power(t2,2) - 3*Power(t2,3) + 
            2*t1*Power(t2,3) + 
            Power(s2,2)*(46 + 3*Power(t1,2) + 23*t2 + Power(t2,2) - 
               t1*(5 + 4*t2)) + 
            s2*(-26 - 4*Power(t1,3) - 10*t2 + Power(t2,2) - 
               3*Power(t2,3) + Power(t1,2)*(11 + 2*t2) + 
               t1*(-45 - 9*t2 + 5*Power(t2,2))) + 
            s1*(10 - 3*Power(t1,3) + Power(s2,2)*(-26 + 3*t1 - t2) - 
               11*t2 + 6*Power(t2,2) + 6*Power(t1,2)*(1 + t2) - 
               t1*(11 + 3*t2 + 3*Power(t2,2)) + 
               s2*(19 + 2*Power(t1,2) + t1*(4 - 12*t2) + 9*t2 + 
                  7*Power(t2,2))) + 
            Power(s1,2)*(7 + 3*t1 - 3*Power(t1,2) - 3*t2 + 
               s2*(7*t1 - 5*(2 + t2)))) + 
         s*(25 - 68*t1 + 41*Power(t1,2) + 2*Power(t1,3) - 
            2*Power(s1,3)*(s2*(-3 + t1) - (-1 + t1)*t1) + 42*t2 + 
            7*t1*t2 - 19*Power(t1,2)*t2 + Power(t1,3)*t2 + 
            Power(t1,4)*t2 - 5*Power(t2,2) + 9*t1*Power(t2,2) + 
            3*Power(t1,2)*Power(t2,2) - 2*Power(t1,3)*Power(t2,2) + 
            2*Power(t2,3) - 4*t1*Power(t2,3) + Power(t1,2)*Power(t2,3) + 
            Power(s2,3)*(-8 - t1*(-8 + t2) - 19*t2 + Power(t2,2)) + 
            Power(s2,2)*(-15 + 5*Power(t1,2)*(-3 + t2) + 49*t2 + 
               5*Power(t2,2) + Power(t2,3) + 
               t1*(30 + 22*t2 - 6*Power(t2,2))) - 
            s2*(-25 + 38*t2 - 7*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,3)*(-3 + 5*t2) + 
               Power(t1,2)*(9 + 3*t2 - 7*Power(t2,2)) + 
               t1*(19 + 42*t2 - 5*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,3) + 19*t1 + 2*(-5 + t2) - 
               3*Power(t1,2)*t2 + Power(s2,2)*(13 - 6*t1 + t2) + 
               s2*(11 + 5*Power(t1,2) - 14*t2 + t1*(-9 + 2*t2))) + 
            s1*(-43 - 2*Power(t1,4) + Power(s2,3)*(19 + t1 - 2*t2) - 
               3*Power(t1,2)*(-1 + t2) + 15*t2 - 4*Power(t2,2) + 
               2*Power(t1,3)*(3 + t2) - 
               2*Power(s2,2)*(25 + 3*Power(t1,2) + t1*(10 - 6*t2) + 
                  9*t2 + Power(t2,2)) + t1*(4 - 28*t2 + 6*Power(t2,2)) + 
               s2*(7*Power(t1,3) - 3*Power(t1,2)*(3 + 4*t2) + 
                  2*t1*(32 + 2*t2 + Power(t2,2)) + 
                  2*(13 - 9*t2 + 5*Power(t2,2))))))*T5(1 - s1 - t1 + t2))/
     (Power(s - s2 + t1,2)*(-1 + s1 + t1 - t2)*(s - s1 + t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)));
   return a;
};
