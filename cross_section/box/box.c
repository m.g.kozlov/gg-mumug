#include<stdio.h>
#include<math.h>
#include<quadmath.h>
#include "../h/box.h"
#include "../h/nlo_functions.h"


#define CUT 1000000.0L
#define Q_IF 100.0L
#define PRN 0






int inv_pr(double *vars)
{
    double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    printf("s=%0.6f s1=%0.6f  s2=%0.6f  t1=%0.6f  t2=%0.6f \n ",s,s1,s2,t1,t2);
    return 1;
};





double box_part_2(double *vars)
{
    //double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    //double vars_1[5], vars_2[5], vars_5[5], vars_6[5];
    double a;
    //double R=0;//s/5000.;
    long double b1_m231, b1_m132;
    long double b2_m231, b2_m132;
    //double t2_m123=t2, t2_m231=t2, s2_m321=s2, s1_m123=s1, t1_m231=t1, t1_m132=t1;
    //double s_m123_1=s, s_m321_1=s, s_m123_2=s, s_m132_2=s;
    
    
    b1_m231 = b1_m132 = 0.;
    b2_m231 = b2_m132 = 0.;

    
    b1_m231 = box_1_m231p2(vars);
    b1_m132 = box_1_m132p2(vars);
    b2_m231 = box_2_m231p2(vars);
    b2_m132 = box_2_m132p2(vars);
    
    
    
    
    

    a=b1_m231+b1_m132+b2_m231+b2_m132;
    
    
 
    if( isnanl(a)!=0 )
    { 
        
        printf("box function:\n");
        //vars_1[0]=s;
        //vars_1[4]=t2;
        inv_pr(vars);
        printf("b1_m231=%Le b1_m132=%Le\n", b1_m231, b1_m132);
        printf("b2_m231=%Le b2_m132=%Le\n", b2_m231, b2_m132);
        printf("\n");
    };
    
  
    
    return a;
};

double box_part_1(double *vars)
{
    double a;
    long double b1_m123, b1_m312, b1_m213, b1_m321, b1_m132, b1_m231;
    long double b2_m123, b2_m312, b2_m213, b2_m321, b2_m231, b2_m132;
    
    b1_m123 = b1_m312 = b1_m213 = b1_m321 = b1_m132 = b1_m231 = 0.;
    b2_m123 = b2_m312 = b2_m213 = b2_m321 = b2_m231 = b2_m132 = 0.;
       
    b1_m123 = box_1_m123(vars);
    b1_m132 = box_1_m132p1(vars);
    b1_m231 = box_1_m231p1(vars);
    b1_m312 = box_1_m312(vars);
    b1_m213 = box_1_m213(vars);
    b1_m321 = box_1_m321(vars);
       
    b2_m123 = box_2_m123(vars);
    b2_m312 = box_2_m312(vars);
    b2_m213 = box_2_m213(vars);
    b2_m321 = box_2_m321(vars);
    b2_m231 = box_2_m231p1(vars);
    b2_m132 = box_2_m132p1(vars);
    
    a=b1_m123 + b1_m312 + b1_m213 + b1_m321 + b1_m132 + b1_m231 + b2_m123 + b2_m312 + b2_m213 + b2_m321 + b2_m231 + b2_m132;
    
    if( isnanl(a)!=0 )
    { 
        
        printf("box function:\n");
        inv_pr(vars);
        printf("b1_m123=%Le b1_m312=%Le b1_m213=%Le b1_m321=%Le b1_m132=%Le b1_m231=%Le\n",b1_m123, b1_m312, b1_m213, b1_m321, b1_m132, b1_m231);
        printf("b2_m123=%Le b2_m312=%Le b2_m213=%Le b2_m321=%Le b2_m231=%Le b2_m132=%Le\n",b2_m123, b2_m312, b2_m213, b2_m321, b2_m231, b2_m132);
        printf("\n");
    };
    
    return a;
};


long double box_1_m123(double *vars)
{
    long double a=0.L, a1=0.L, a2=0.L, a3=0.L, a4=0.L, a5=0.L, a6=0.L;

        a1 = box_1_m123_1_q(vars);
        a2 = box_1_m123_2_q(vars);
        a3 = box_1_m123_3_q(vars);
        a4 = box_1_m123_4_q(vars);
        a5 = box_1_m123_5_q(vars);
        a6 = box_1_m123_6_q(vars);
      
    a = a1 + a2 + a3 + a4 + a5 + a6;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_1_m123: %Le %Le %Le %Le %Le %Le\n",a1,a2,a3,a4,a5,a6);
    };
    return a;
};


long double box_1_m231p2(double *vars)
{
    long double a, a1, a2, a3, a4, a5, a6;
    a1 = a2 = a3 = a4 = a5 = a6 = 0.L;
    
    //double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    
    //double R=0.01;
    
    /*
    if( fabs(s1*t1-t2)>R )
    { 
        a1 = box_1_m231_1_q(vars); 
        a3 = box_1_m231_3_q(vars);
        a4 = box_1_m231_4_q(vars);       
    };*/
    
    
    a1 = box_1_m231_1_q(vars); 
    a3 = box_1_m231_3_q(vars);
    a4 = box_1_m231_4_q(vars);
  
    a = a1 + a3 + a4;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_1_m231p2: %Le %Le %Le\n",a1,a3,a4);
    };
    return a;
};


long double box_1_m231p1(double *vars)
{
    long double a, a2, a5, a6;
    a2 = a5 = a6 = 0.L;
    
    a2 = box_1_m231_2_q(vars);
    a5 = box_1_m231_5_q(vars);
    a6 = box_1_m231_6_q(vars);

    a =  a2 + a5 + a6;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_1_m231 part1: %Le %Le %Le \n",a2,a5,a6);
    };
    return a;
};



long double box_1_m312(double *vars)
{
    long double a, a1, a2, a3, a4, a5, a6;
    
    
   
        a1 = box_1_m312_1_q(vars);
        a2 = box_1_m312_2_q(vars);
        a3 = box_1_m312_3_q(vars);
        a4 = box_1_m312_4_q(vars);
        a5 = box_1_m312_5_q(vars);
        a6 = box_1_m312_6_q(vars);
    
    
    
    
    
    a = a1 + a2 + a3 + a4 + a5 + a6;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_1_m312: %Le %Le %Le %Le %Le %Le\n",a1,a2,a3,a4,a5,a6);
    };
    return a;
};

long double box_1_m132p2(double *vars)
{
    long double a,  a2, a5, a6;
    a2 = a5 = a6 = 0.L;
    
    //double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    
    //double R=0.01;
    
    /*double vars_1[5], t1n;
    
    vars_1[0]=s;
    vars_1[1]=s1;
    vars_1[2]=s2;
    vars_1[3]=t1;
    vars_1[4]=t2;*/
    
    /*
    if( fabs(-1.+s-s*s1+s1*s2-s1*t1+t2) > R )
    { 
        
        a2 = box_1_m132_2_q(vars);
        a5 = box_1_m132_5_q(vars);
        a6 = box_1_m132_6_q(vars);        
        
    };*/

    a2 = box_1_m132_2_q(vars);
    a5 = box_1_m132_5_q(vars);
    a6 = box_1_m132_6_q(vars);        
        
    
    a = a2 + a5 + a6;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_1_m132p2: %Le %Le %Le \n",a2,a5,a6);
    };
    return a;
};


long double box_1_m132p1(double *vars)
{
    long double a, a1, a3, a4;
    a1 = a3 = a4 = 0.L;
    
    a1 = box_1_m132_1_q(vars);
    a3 = box_1_m132_3_q(vars);
    a4 = box_1_m132_4_q(vars);
    
    
    
    a = a1 + a3 + a4;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_1_m132 part1: %Le %Le %Le\n",a1,a3,a4);
    };
    return a;
};


long double box_1_m213(double *vars)
{
    long double a, a1, a2, a3, a4, a5, a6;
    
    
   
        a1 = box_1_m213_1_q(vars);
        a2 = box_1_m213_2_q(vars);
        a3 = box_1_m213_3_q(vars);
        a4 = box_1_m213_4_q(vars);
        a5 = box_1_m213_5_q(vars);
        a6 = box_1_m213_6_q(vars);
    

    
    
    a = a1 + a2 + a3 + a4 + a5 + a6;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_1_m213: %Le %Le %Le %Le %Le %Le\n",a1,a2,a3,a4,a5,a6);
    };
    return a;
};


long double box_1_m321(double *vars)
{
    long double a, a1, a2, a3, a4, a5, a6;
    
    
  
        a1 = box_1_m321_1_q(vars);
        a2 = box_1_m321_2_q(vars);
        a3 = box_1_m321_3_q(vars);
        a4 = box_1_m321_4_q(vars);
        a5 = box_1_m321_5_q(vars);
        a6 = box_1_m321_6_q(vars);

    
    
    
    a = a1 + a2 + a3 + a4 + a5 + a6;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_1_m321: %Le %Le %Le %Le %Le %Le\n",a1,a2,a3,a4,a5,a6);
    };
    return a;
};


long double box_2_m123(double *vars)
{
    long double a, a1, a2, a3, a4, a5, a6;
    
        
  
        a1 = box_2_m123_1_q(vars);
        a2 = box_2_m123_2_q(vars);
        a3 = box_2_m123_3_q(vars);
        a4 = box_2_m123_4_q(vars);
        a5 = box_2_m123_5_q(vars);
        a6 = box_2_m123_6_q(vars);

    
    
    
    a = a1 + a2 + a3 + a4 + a5 + a6;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_2_m123: %Le %Le %Le %Le %Le %Le\n",a1,a2,a3,a4,a5,a6);
    };
    return a;
};


long double box_2_m231p1(double *vars)
{
    long double a, a2, a5, a6;
    a2 = a5 = a6 = 0.L;
        
        a2 = box_2_m231_2_q(vars);
        a5 = box_2_m231_5_q(vars);
        a6 = box_2_m231_6_q(vars);
    
    a = a2 + a5 + a6;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_2_m231p1: %Le %Le %Le\n",a2,a5,a6);
    };
    return a;
};

long double box_2_m231p2(double *vars)
{
    long double a, a1, a3, a4;
    a1 = a3 = a4 = 0.L;
      
    //double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    
    //double R=0.01;
    
  
    /*
    if( fabs(s2*t2-t1)>R )
    { 
        a1 = box_2_m231_1_q(vars);
        a3 = box_2_m231_3_q(vars);
        a4 = box_2_m231_4_q(vars);
    };*/
        
    a1 = box_2_m231_1_q(vars);
    a3 = box_2_m231_3_q(vars);
    a4 = box_2_m231_4_q(vars);
    
    
    
    a = a1 + a3 + a4;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_2_m231: %Le %Le %Le\n",a1,a3,a4);
    };
    return a;
};



long double box_2_m312(double *vars)
{
    long double a, a1, a2, a3, a4, a5, a6;
    
    

        a1 = box_2_m312_1_q(vars);
        a2 = box_2_m312_2_q(vars);
        a3 = box_2_m312_3_q(vars);
        a4 = box_2_m312_4_q(vars);
        a5 = box_2_m312_5_q(vars);
        a6 = box_2_m312_6_q(vars);

    
    
    a = a1 + a2 + a3 + a4 + a5 + a6;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_2_m312: %Le %Le %Le %Le %Le %Le\n",a1,a2,a3,a4,a5,a6);
    };
    return a;
};

long double box_2_m132p1(double *vars)
{
    long double a, a1, a3, a4;
    a1 = a3 = a4 = 0.L;
 
        a1 = box_2_m132_1_q(vars);
        a3 = box_2_m132_3_q(vars);
        a4 = box_2_m132_4_q(vars);
        
    a = a1 + a3 + a4;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_2_m132: %Le %Le %Le\n",a1,a3,a4);
    };
    return a;
};

long double box_2_m132p2(double *vars)
{
    long double a, a2, a5, a6;
    a2 = a5 = a6 = 0.L;
    //double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    
    //double R=0.01;
    
    /*
    if( fabs(-1.+s-s*s2+s1*s2-s2*t2+t1) > R )
    {
        a2 = box_2_m132_2_q(vars);
        a5 = box_2_m132_5_q(vars);
        a6 = box_2_m132_6_q(vars);
    };*/
    
    a2 = box_2_m132_2_q(vars);
    a5 = box_2_m132_5_q(vars);
    a6 = box_2_m132_6_q(vars);

    
    
    a = a2 + a5 + a6;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_2_m132: %Le %Le %Le\n",a2,a5,a6);
    };
    return a;
};





long double box_2_m213(double *vars)
{
    long double a, a1, a2, a3, a4, a5, a6;
    
    
    

        a1 = box_2_m213_1_q(vars);
        a2 = box_2_m213_2_q(vars);
        a3 = box_2_m213_3_q(vars);
        a4 = box_2_m213_4_q(vars);
        a5 = box_2_m213_5_q(vars);
        a6 = box_2_m213_6_q(vars);

    
    a = a1 + a2 + a3 + a4 + a5 + a6;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_2_m213: %Le %Le %Le %Le %Le %Le\n",a1,a2,a3,a4,a5,a6);
    };
    return a;
};


long double box_2_m321(double *vars)
{
    long double a, a1, a2, a3, a4, a5, a6;
    
    
    

        a1 = box_2_m321_1_q(vars);
        a2 = box_2_m321_2_q(vars);
        a3 = box_2_m321_3_q(vars);
        a4 = box_2_m321_4_q(vars);
        a5 = box_2_m321_5_q(vars);
        a6 = box_2_m321_6_q(vars);

    
    
    a = a1 + a2 + a3 + a4 + a5 + a6;
    
    if (fabsl(a) > CUT && PRN==1)
    {
        inv_pr(vars);    
        printf("box_2_m321: %Le %Le %Le %Le %Le %Le\n",a1,a2,a3,a4,a5,a6);
    };
    return a;
};

