#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>


#include <gsl/gsl_math.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_monte_miser.h>
#include <gsl/gsl_monte_vegas.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_pow_int.h>

#include "./h/nlo_functions.h"
#include "./h/mc_functions.h"
#include "./h/config.h"
#include "./h/ur.h"
#include "./h/nlo.h"




#define Pi M_PI
#define Power gsl_pow_int






double nlo1(double *);
double nlo1_2(double *);
double nlo2(double *);
double nlo3(double *);
double nlo_qcd(double *);
double born(double *);


double PhaseVol(double, double, double, double);
double t2_func(double,double,double,double,double);
int HevTheta(double);
double Gk(double,double,double,double,double,double);
double f_lambda(double, double, double);
double delta4(double,double,double,double,double);



double C(double s,double s1,double s2,double t1,double t2);


int save(int,double *,char *);




double born_mc(double *, size_t, void *);
double nlo1_mc(double *, size_t, void *);
double nlo1_2_mc(double *, size_t, void *);
double nlo2_mc(double *, size_t, void *);
double nlo3_mc(double *, size_t, void *);
double nlo_qcd_mc(double *, size_t, void *);






int main(int argc, char *argv[])
{
    //default values:
    opt.E=500.;   // MeV
    opt.N=50000;
    opt.omega=30.; // MeV
    strcpy(opt.conf,"1.cfg");
    strcpy(opt.file,"num.txt");
    
    
    //============== get parameters =========================
    
    if ( get_params(argc,argv,&opt) < 0) return(0);
    
   
 
     //default parameters and from command line
     par.E=opt.E;
     par.N=opt.N;
     par.omega=opt.omega;
     par.nlo1=1;
     par.nlo2=1;
     par.nlo3=1;
     par.th_beam=10.;
     par.th_max=10.;
     par.th_gmu=0.;
     par.si_min=1.;
     par.om_s=30.;
     strcpy(par.file,opt.file);
     
     //get from config   
     
     read_my_config(opt.conf,&par);
          
     printf("parameters after read config file:\n \
            \t E=%.2f\n\
            \t N=%i\n\
            \t omega=%.2f\n\
            \t theta_beam=%.1f\n\
            \t theta_gmu=%.1f\n\
            \t theta_max=%.1f\n\
            \t omega_s=%.2f\n\
            \t si_min=%.2f\n\
            \t nlo1=%i\n\
            \t nlo2=%i\n\
            \t nlo3=%i\n\
            \t file=%s\n" , par.E, par.N, par.omega, par.th_beam, par.th_gmu, par.th_max, par.om_s, par.si_min, par.nlo1, par.nlo2, par.nlo3, par.file);
    
    //=============== end get parameters ================================
    
    struct mc_pars P;
    
    time_t Ts, Tf;
    
    //=============== physical constants ============================
    
    double mu, me, Const, mtau, m_up, alpha;
    mu=105.6583745;     // Muon mass (MeV)
    me=0.511/mu;       // Electron mass 
    mtau=1776.84/mu;    // Tau mass
    m_up=2./mu;
    alpha=1./137.02;
    Const=24.*alpha*Power(Pi*me,2)*Power(10.,9)*0.6652; // nb    
    
    
    //==== copy parameters to structure P ===========================    
    
    P.N = par.N;
    P.s = 4.*Power( (par.E)/mu, 2);
    P.mf = me;
    P.om_min = (par.omega)/mu;
    P.th_particles_min = par.th_beam;
    P.th_max = par.th_max;
    P.th_gmu = par.th_gmu;
    P.si_min = par.si_min;
    P.om_s = (par.om_s)/mu;
    
    // ==== results =================================================
    /*
     * results:
     * 
     * {    energy,  
     *      born, err born,
     *      nlo1,  err nlo1,
     *      nlo2e, err nlo2e,
     *      nlo2mu, err nlo2mu,  }
     * 
     * 
     * 
     * 
     * 
     */ 
    
    double results[30];
    double res=0, err=0.;
    int i;
    for(i=0;i<30;i++) results[i]=0.;
    // ==============================================================
    // test nlo 1 functions
    //double me=0.511;
/*
    me=105.6;
    double me2=pow(me,2);
    double om;
    double vars[7], vars2[7];
    double s, s1, s2, t1, t2;
    om=30.;
    s=pow(2.*5000.,2);
    s1=s/5.8;
    s2=s/2.4;
    t1=-s/4.;
    t2=-s/6.;
    
    vars[0]=s/me2;
    vars[1]=s1/me2;
    vars[2]=s2/me2;
    vars[3]=t1/me2;
    vars[4]=t2/me2;
    vars[5]=om/me;
    vars[6]=1.;
    
    
    vars2[0]=s;
    vars2[1]=s1;
    vars2[2]=s2;
    vars2[3]=t1;
    vars2[4]=t2;
    vars2[5]=om;
    vars2[6]=me;
    
    
    printf("----test born----\n \
            born=%.6f\n \
            ur born=%.6f\n ",born(vars),ur_born(vars));
    
    
    printf("----test nlo1----\n \
            nlo1=%.6f\n \
            ur nlo1=%.6f\n ",(nlo1(vars)+nlo1_2(vars)),ur_nlo1(vars));
    
    
    return 1; */
    //==== calc born ================================================
    
    
    mc_integrate(&P, &mc_born, &res, &err);
    
    results[0]=par.E;
    results[1]=res;
    results[2]=err;
    
    
    printf("==========Born============\n");
    printf("E gamma = %.1f\n",par.E);
    printf("%.6f + pm %.6f nb\n",results[1]*Const,results[2]*Const);
    
    double Cb=alpha*4.*Pi/results[1];
    
    //==== calc born  ultra rel ================================================
    
    
    mc_integrate(&P, &mc_ur_born, &res, &err);
    
        
    printf("---ur Born---\n");
    printf("%.6f + pm %.6f nb\n",res*Const,err*Const);
    
    
    printf("diff = %.6f | rel err = %.6f\n",(res-results[1])/results[1],err/res);
    
    // ==== test born ======
    /*
    double vars[6];
    vars[0] = 1000000.;
    vars[1] = 23.;
    vars[2] = 400167.;
    vars[3] = -300013.;
    vars[4] = -500147.;
    vars[5] = 1.;
    
    printf("\t born = %e\n",born(vars));
    printf("\t uborn = %e\n",ur_born(vars));
    */
      
    
    
    //======calc NLO 1 ============================================
    
    if( par.nlo1>0 )
    {
        
        printf("========== NLO 1 ============\n");
      
        P.mf = 1.;
        
        mc_integrate_wb(&P, &mc_ur_nlo1, &res, &err);
    
        
        printf("--- ur nlo1 ---\n");
        printf("delta1 = %.6f + pm %.6f\n",res*Cb,err*Cb);
    
        
        P.mf = 1.;
        
        mc_integrate_wb(&P, &mc_ur_nlo1_m0, &res, &err);
    
        
        printf("--- ur nlo1 with collinear corrections ---\n");
        printf("delta1 = %.6f + pm %.6f\n",res*Cb,err*Cb);
    
        
        
    
        Ts=time(NULL);
        
        printf("--- nlo1 ---\n");
        
        mc_integrate_wb(&P,&mc_nlo1,&res,&err);
        
        results[3]=res*Cb;
        results[4]=err*Cb;
    
    
        
        
        printf("delta1 = %.6f + pm %.6f nb\n",results[3],results[4]);
    
        
        Tf=time(NULL);
        printf("\tCalc time: %f h\n",difftime(Ts,Tf)/3600.);
    };
   
    //========= calc NLO 2=========================================
    
    if( par.nlo2>0 )
    {
        //============= nlo2 for electron =========================
        Ts=time(NULL);
        printf("\t ======= calc NLO2 =======\
                \n\
                \t -- NLO 2 for electron --\n\
                ");
        
        P.mf = me;
        res = 0.;
        err = 0.;
        mc_integrate(&P, &mc_nlo2, &res, &err);
        
        results[5]=res*Cb;
        results[6]=err*Cb;
        Tf=time(NULL);
        
        printf("\t delta2 = %.6f + pm %.6f \n",results[5],results[6]);
        printf("\t Calc time: %f h \n",difftime(Ts,Tf)/3600.);
        
        //============ nlo2 for muon============================
        
        Ts=time(NULL);
        printf("\t --------------------------\
                \n\
                \t -- NLO 2 for muon --\n\
                ");
        P.mf = 1.;
        res = 0.;
        err = 0.;
        mc_integrate(&P, &mc_nlo2, &res, &err);
        
        results[7]=res*Cb;
        results[8]=err*Cb;
        Tf=time(NULL);
        
        printf("\t delta2 = %.6f + pm %.6f \n",results[7],results[8]);
        printf("\t Calc time: %f h \n",difftime(Ts,Tf)/3600.);
        
        
        
    };
    
    
    
    save(19,results,par.file);
    
     
    
    
    
    

    

    
    return 0;
};





int save(int len,double *Mres,char *file_name)
{
    FILE *file;
    int i;
    file=fopen(file_name,"a");
    for(i=0;i<len;i++)
    {
        fprintf(file,"%.10f  ",Mres[i]);
    };
    fprintf(file,"\n");
    
    fclose(file);
    return 1;
};








int HevTheta(double a)
{
    if (a<0) return 0;
    else return 1;
};








double nlo1_mc(double *inv, size_t dim, void *params)
{
    (void)(dim);
    double *pars = (double *)params;
    double vars[7];
    double s1,s2,t1,lam; // variables
    double t2; // temporary variables
    double s, omega;
    double result;
    
    
    s1=inv[0];
    s2=inv[1];
    t1=inv[2];
    lam=inv[3];
    
    s=pars[0];
    omega=pars[1];
    
    
    
        
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        if( cuts(s,s1,s2,t1,t2) )
        {
            vars[0] = s;
            vars[1] = s1;
            vars[2] = s2;
            vars[3] = t1;
            vars[4] = t2;
            vars[5] = omega;
            result = nlo1(vars)*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );   
            if(isnanl(result)!=0) printf("NAN nlo1 func: s1=%e  s2=%e  t1=%e  t2=%e  nlo1=%e\n",s1,s2,t1,t2, nlo1(vars) );
        }
        else result=0.;
        
        
    }
    else
    {
        result = 0.;
    };
    
    return result;

};

double nlo2_mc(double *inv, size_t dim, void *params)
{
    (void)(dim);
    double *pars = (double *)params;
    double vars[6];
    double s1,s2,t1,lam; // variables
    double s, m, t2; // temporary variables
    double result;
    
        
    s1=inv[0];
    s2=inv[1];
    t1=inv[2];
    lam=inv[3];
    
    s=pars[0];
    m=pars[1];
    
        
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        if( cuts(s,s1,s2,t1,t2) )
        {
            vars[0] = s;
            vars[1] = s1;
            vars[2] = s2;
            vars[3] = t1;
            vars[4] = t2;
            vars[5] = m;
            result = nlo2(vars)*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );   
            //if(isnanl(result)!=0) printf("NAN nlo2 func: s1=%e  s2=%e  t1=%e  t2=%e  nlo2=%e\n",s1,s2,t1,t2, nlo2(vars) );
        }
        else result=0.;
    }
    else  result = 0.;
        
    return result;
};

double nlo3_mc(double *inv, size_t dim, void *params)
{
    (void)(dim);
    double *pars = (double *)params;
    double vars[7];
    double s1,s2,t1,lam; // variables
    double t2; // temporary variables
    double s, omega;
    double result;
    
    
    s1=inv[0];
    s2=inv[1];
    t1=inv[2];
    lam=inv[3];
    
    s=pars[0];
    omega=pars[1];
    
    
    
        
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        if( cuts(s,s1,s2,t1,t2) )
        {
            vars[0] = s;
            vars[1] = s1;
            vars[2] = s2;
            vars[3] = t1;
            vars[4] = t2;
            vars[5] = omega;
            result = nlo3(vars)*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );   
            if(isnanl(result)!=0) printf("NAN nlo1 func: s1=%e  s2=%e  t1=%e  t2=%e  nlo1=%e\n",s1,s2,t1,t2, nlo1(vars) );
        }
        else result=0.;
        
        
    }
    else
    {
        result = 0.;
    };
    
    return result;

};

/*
double nlo_qcd_mc(double *inv, size_t dim, void *params)
{
    (void)(dim);
    double *pars = (double *)params;
    double vars[6];
    double s1,s2,t1,lam; // variables
    double s, m, t2; // temporary variables
    double result;
    
        
    s1=inv[0];
    s2=inv[1];
    t1=inv[2];
    lam=inv[3];
    
    s=pars[0];
    m=pars[1];
    
        
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        if( cuts(s,s1,s2,t1,t2) )
        {
            vars[0] = s;
            vars[1] = s1;
            vars[2] = s2;
            vars[3] = t1;
            vars[4] = t2;
            vars[5] = m;
            result = nlo_qcd(vars)*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );   
            //if(isnanl(result)!=0) printf("NAN nlo2 func: s1=%e  s2=%e  t1=%e  t2=%e  nlo2=%e\n",s1,s2,t1,t2, nlo2(vars) );
        }
        else result=0.;
    }
    else  result = 0.;
        
    return result;
};
*/


double nlo1_2_mc(double *inv, size_t dim, void *params)
{
    (void)(dim);
    double *pars = (double *)params;
    double vars[7];
    double s1,s2,t1,lam; // variables
    double t2; // temporary variables
    double s, omega;
    double result;
    
    
    s1=inv[0];
    s2=inv[1];
    t1=inv[2];
    lam=inv[3];
    
    s=pars[0];
    omega=pars[1];
    
    
    
        
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        if( cuts(s,s1,s2,t1,t2) )
        {
            vars[0] = s;
            vars[1] = s1;
            vars[2] = s2;
            vars[3] = t1;
            vars[4] = t2;
            vars[5] = omega;
            result = nlo1_2(vars)*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );   
            if(isnanl(result)!=0) printf("NAN nlo1_2 func: s1=%e  s2=%e  t1=%e  t2=%e\n",s1,s2,t1,t2);
        }
        else result=0.;
        
        
    }
    else
    {
        result = 0.;
    };
    
    return result;

};