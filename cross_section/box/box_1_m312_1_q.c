#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m312_1_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(2*Power(s,10)*t1*(1 + s2*(5 + 2*t1)) - 
         Power(s,9)*(6*(3 - 2*t1)*t1 + 
            2*Power(s2,2)*(-5 + 15*Power(t1,2) + t1*(35 + t2)) + 
            s2*(-2 - 15*Power(t1,3) + Power(t1,2)*(-68 + 4*s1 - t2) + 
               Power(t2,3) + t1*(116 + 4*s1 - 28*t2 + 3*Power(t2,2)))) - 
         Power(s,8)*(-2*t1*(33 - 49*t1 + 15*Power(t1,2)) + 
            2*Power(s2,3)*(32 - 48*Power(t1,2) + 
               t1*(-104 + 3*s1 - 9*t2) + t2) + 
            Power(s2,2)*(108*Power(t1,3) + 3*Power(t1,2)*(130 + 7*t2) + 
               t1*(-648 + 220*t2 - 9*Power(t2,2)) - 
               4*(-26 + 7*t2 + 2*Power(t2,3)) + 
               s1*(4 - 14*Power(t1,2) + 3*Power(t2,2) - t1*(88 + 7*t2))) \
+ s2*(16 - 15*Power(t1,4) + 3*Power(t2,2) - 7*Power(t2,3) + 
               Power(t1,3)*(-227 + 8*s1 + t2) + 
               Power(t1,2)*(724 + 2*Power(s1,2) - 217*t2 + 
                  15*Power(t2,2) + s1*(46 + t2)) - 
               t1*(596 + 2*Power(s1,2)*(-4 + t2) - 305*t2 + 
                  19*Power(t2,2) - 11*Power(t2,3) + 
                  s1*(50 + 30*t2 + Power(t2,2))))) + 
         Power(s,7)*(4*t1*(-32 + 80*t1 - 55*Power(t1,2) + 
               10*Power(t1,3)) - 
            2*Power(s2,4)*(-85 + s1*(3 - 18*t1) + 85*Power(t1,2) - 
               8*t2 + t1*(170 + 33*t2)) + 
            Power(s2,3)*(321*Power(t1,3) + Power(s1,2)*(14*t1 - 3*t2) + 
               Power(t1,2)*(919 + 109*t2) + 
               t1*(-1554 + 697*t2 + 23*Power(t2,2)) - 
               2*(-209 + 99*t2 + 6*Power(t2,2) + 14*Power(t2,3)) + 
               s1*(88 - 25*Power(t1,2) + 13*t2 + 21*Power(t2,2) - 
                  t1*(425 + 76*t2))) - 
            s2*(-50 + 10*Power(t1,5) - 
               4*Power(t1,4)*(108 + 5*s1 - 5*t2) + 3*t2 - 
               18*Power(t2,2) + 19*Power(t2,3) + 
               Power(t1,3)*(1897 + 17*Power(s1,2) - s1*(-241 + t2) - 
                  684*t2 + 30*Power(t2,2)) + 
               t1*(1776 - 14*Power(s1,3) - 1289*t2 + 185*Power(t2,2) - 
                  44*Power(t2,3) + 3*Power(s1,2)*(8 + 15*t2) + 
                  s1*(114 + 101*t2 - 52*Power(t2,2))) + 
               Power(t1,2)*(-3027 - 2*Power(s1,3) + 1839*t2 - 
                  148*Power(t2,2) + 40*Power(t2,3) - 
                  3*Power(s1,2)*(-7 + 4*t2) + 
                  s1*(-463 - 93*t2 + Power(t2,2)))) + 
            Power(s2,2)*(466 + 2*Power(s1,3)*t1 - 122*Power(t1,4) - 
               271*t2 + 19*Power(t2,2) - 49*Power(t2,3) - 
               Power(t1,3)*(1045 + 43*t2) + 
               Power(s1,2)*(-8 + 10*t1 + 35*Power(t1,2) + 2*t2 - 
                  23*t1*t2) + 
               Power(t1,2)*(3117 - 1346*t2 + 39*Power(t2,2)) + 
               t1*(-2685 + 1968*t2 - 108*Power(t2,2) + 70*Power(t2,3)) + 
               s1*(46 - 5*Power(t1,3) + 24*t2 + 16*Power(t2,2) + 
                  Power(t1,2)*(607 + 15*t2) - 
                  t1*(694 + 67*t2 + 18*Power(t2,2))))) - 
         Power(-1 + s2,2)*s2*(-1 + t1)*
          (s2 + Power(s2,2) - 2*s2*t1 + (-1 + t1)*t1)*
          ((-1 + s1)*Power(t1,5)*(-1 + 7*s1 - 6*t2) + 
            Power(s2,4)*(-4 + 5*s1 + 4*t1 - 5*t2)*Power(s1 - t2,2) - 
            2*t1*(11*s1 + 7*(-4 + t2))*Power(-1 + t2,2) + 
            10*Power(-1 + t2,3) + 
            Power(t1,3)*(27 - 7*Power(s1,3) - 110*t2 + 127*Power(t2,2) + 
               6*Power(t2,3) + Power(s1,2)*(38 + 26*t2) + 
               s1*(36 - 98*t2 - 45*Power(t2,2))) + 
            Power(t1,4)*(3 - 2*Power(s1,3) + 13*t2 - 36*Power(t2,2) - 
               2*Power(s1,2)*(7 + t2) + s1*(-17 + 49*t2 + 6*Power(t2,2))) \
+ Power(t1,2)*(-77 + 4*Power(s1,3) + 187*t2 - 145*Power(t2,2) + 
               3*Power(t2,3) - Power(s1,2)*(31 + 9*t2) + 
               s1*(11 + 11*t2 + 46*Power(t2,2))) + 
            Power(s2,2)*(-1 + t1)*
             (-3 + Power(s1,3)*(-4 + t1) - 31*t2 + 73*Power(t2,2) - 
               7*Power(t2,3) + Power(t1,2)*(5 + 24*t2 - 4*Power(t2,2)) + 
               t1*(-2 + 7*t2 - 56*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s1,2)*(31 + 25*Power(t1,2) + 9*t2 - 
                  t1*(43 + 5*t2)) - 
               s1*(-41 + 115*t2 - 6*Power(t2,2) + 
                  2*Power(t1,2)*(17 + 8*t2) + 
                  t1*(7 - 105*t2 + 5*Power(t2,2)))) - 
            Power(s2,3)*(4 + Power(s1,3)*(1 + 9*t1) + 5*t2 + 
               9*Power(t2,2) - 4*Power(t2,3) + 
               Power(t1,2)*(4 + 5*t2 + 6*Power(t2,2)) - 
               t1*(8 + 10*t2 + 15*Power(t2,2) + 6*Power(t2,3)) + 
               3*Power(s1,2)*
                (6 + 5*Power(t1,2) - 2*t2 - t1*(11 + 8*t2)) + 
               s1*(-13 - 23*t2 + 9*Power(t2,2) - 
                  Power(t1,2)*(13 + 17*t2) + 
                  t1*(26 + 40*t2 + 21*Power(t2,2)))) + 
            s2*(Power(s1,3)*t1*(-8 + 13*t1 + 5*Power(t1,2)) - 
               2*Power(-1 + t2,2)*(13 + 8*t2) + 
               Power(t1,4)*(-2 - 25*t2 + 6*Power(t2,2)) + 
               4*t1*(11 - 32*t2 + 32*Power(t2,2) + 5*Power(t2,3)) - 
               Power(t1,3)*(4 + 6*t2 - 77*Power(t2,2) + 6*Power(t2,3)) - 
               Power(t1,2)*(12 - 123*t2 + 217*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s1,2)*t1*
                (62 - 21*Power(t1,3) + Power(t1,2)*(53 - 2*t2) + 18*t2 - 
                  2*t1*(47 + 23*t2)) + 
               s1*(22*Power(-1 + t2,2) + Power(t1,4)*(29 + 13*t2) + 
                  Power(t1,3)*(16 - 138*t2 + 5*Power(t2,2)) - 
                  2*t1*(-15 + 63*t2 + 20*Power(t2,2)) + 
                  Power(t1,2)*(-97 + 295*t2 + 43*Power(t2,2))))) - 
         Power(s,6)*(-2*t1*(71 - 271*t1 + 314*Power(t1,2) - 
               130*Power(t1,3) + 15*Power(t1,4)) - 
            10*Power(s2,5)*(-24 + s1*(3 - 9*t1) + 18*Power(t1,2) - 
               5*t2 + t1*(33 + 13*t2)) + 
            Power(s2,4)*(664 + Power(s1,3) + 510*Power(t1,3) + 
               Power(s1,2)*(-17 + 84*t1 - 18*t2) - 548*t2 - 
               74*Power(t2,2) - 56*Power(t2,3) + 
               Power(t1,2)*(1126 + 259*t2) + 
               t1*(-2012 + 1187*t2 + 159*Power(t2,2)) + 
               s1*(356 - 49*Power(t1,2) + 99*t2 + 63*Power(t2,2) - 
                  3*t1*(313 + 101*t2))) + 
            Power(s2,3)*(1256 - 361*Power(t1,4) + 
               Power(s1,3)*(-2 + 22*t1) - 1291*t2 + 68*Power(t2,2) - 
               140*Power(t2,3) - Power(t1,3)*(1914 + 217*t2) + 
               Power(t1,2)*(5479 - 3457*t2 - 81*Power(t2,2)) + 
               t1*(-4901 + 5168*t2 - 233*Power(t2,2) + 
                  184*Power(t2,3)) + 
               Power(s1,2)*(-1 + 110*Power(t1,2) + 12*t2 - 
                  5*t1*(23 + 20*t2)) + 
               s1*(508 - 109*Power(t1,3) + 17*t2 + 80*Power(t2,2) + 
                  Power(t1,2)*(2247 + 170*t2) - 
                  3*t1*(875 - 14*t2 + 22*Power(t2,2)))) + 
            Power(s2,2)*(1185 + 8*Power(t1,5) + 
               Power(s1,3)*(-14 + 83*t1 + 7*Power(t1,2)) + 
               Power(t1,4)*(1563 - 10*t2) - 1000*t2 + 211*Power(t2,2) - 
               93*Power(t2,3) + 
               Power(t1,3)*(-6182 + 3361*t2 - 60*Power(t2,2)) + 
               Power(t1,2)*(9943 - 8949*t2 + 754*Power(t2,2) - 
                  218*Power(t2,3)) + 
               t1*(-6594 + 6628*t2 - 1199*Power(t2,2) + 
                  226*Power(t2,3)) + 
               Power(s1,2)*(32 - 195*Power(t1,3) + 43*t2 + 
                  Power(t1,2)*(181 + 102*t2) - t1*(313 + 310*t2)) + 
               s1*(71 + 184*Power(t1,4) + 47*t2 - 29*Power(t2,2) + 
                  15*Power(t1,3)*(-125 + 3*t2) + 
                  Power(t1,2)*(3691 - 158*t2 + 25*Power(t2,2)) + 
                  t1*(-1758 + 410*t2 + 215*Power(t2,2)))) + 
            s2*(79 + 30*Power(t1,6) - 15*t2 + 39*Power(t2,2) - 
               23*Power(t2,3) + Power(t1,5)*(-468 - 80*s1 + 50*t2) + 
               Power(t1,4)*(2617 + 55*Power(s1,2) - 20*s1*(-30 + t2) - 
                  1132*t2 + 30*Power(t2,2)) + 
               Power(t1,2)*(7229 - 55*Power(s1,3) - 6334*t2 + 
                  1127*Power(t2,2) - 112*Power(t2,3) + 
                  2*Power(s1,2)*(81 + 109*t2) + 
                  s1*(1193 + 117*t2 - 275*Power(t2,2))) - 
               Power(t1,3)*(6288 + 10*Power(s1,3) - 4474*t2 + 
                  438*Power(t2,2) - 70*Power(t2,3) + 
                  10*Power(s1,2)*(4 + 3*t2) + 
                  s1*(1601 + t2 - 20*Power(t2,2))) + 
               t1*(-3353 + 60*Power(s1,3) + 3082*t2 - 791*Power(t2,2) + 
                  50*Power(t2,3) - Power(s1,2)*(211 + 165*t2) + 
                  s1*(42 - 193*t2 + 279*Power(t2,2))))) + 
         s*(-2*(-2 + t1)*Power(-1 + t1,3)*Power(t1,3) + 
            Power(s2,9)*(s1 - t2)*
             (6 + Power(s1,2) + 6*Power(t1,2) + 2*s1*(-7 + 7*t1 - t2) + 
               14*t2 + Power(t2,2) - 2*t1*(6 + 7*t2)) + 
            Power(s2,8)*(8 + Power(s1,3)*(-31 + 32*t1) - t2 + 
               19*Power(t1,3)*t2 + 55*Power(t2,2) + 25*Power(t2,3) + 
               Power(t1,2)*(8 - 39*t2 - 31*Power(t2,2)) - 
               t1*(16 - 21*t2 + 24*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s1,2)*(73 - 13*Power(t1,2) + 87*t2 - 
                  30*t1*(2 + 3*t2)) + 
               s1*(-13 - 21*Power(t1,3) - 120*t2 - 81*Power(t2,2) + 
                  Power(t1,2)*(29 + 52*t2) + 
                  t1*(5 + 68*t2 + 84*Power(t2,2)))) + 
            Power(s2,6)*(57 + 
               Power(s1,3)*(5 + 51*t1 - 135*Power(t1,2) + 
                  104*Power(t1,3)) - 6*Power(t1,5)*(-8 + t2) - 67*t2 + 
               139*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,4)*(16 + 9*t2 - 38*Power(t2,2)) + 
               Power(t1,2)*(283 - 1016*t2 + 2101*Power(t2,2) - 
                  92*Power(t2,3)) + 
               Power(t1,3)*(-233 + 473*t2 - 939*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-171 + 607*t2 - 1263*Power(t2,2) + 61*Power(t2,3)) + 
               Power(s1,2)*(380*Power(t1,4) - 9*(-6 + t2) - 
                  t1*(659 + 160*t2) - Power(t1,3)*(1341 + 325*t2) + 
                  Power(t1,2)*(1566 + 419*t2)) - 
               s1*(73 + 54*Power(t1,5) + 109*t2 + 3*Power(t2,2) + 
                  Power(t1,4)*(70 + 249*t2) + 
                  Power(t1,3)*(154 - 2165*t2 - 178*Power(t2,2)) + 
                  t1*(310 - 1825*t2 - 19*Power(t2,2)) + 
                  Power(t1,2)*(-661 + 3632*t2 + 119*Power(t2,2)))) + 
            Power(s2,7)*(15 + 
               Power(s1,3)*(-8 + 101*t1 - 108*Power(t1,2)) - 91*t2 + 
               190*Power(t2,2) - 22*Power(t2,3) - 
               14*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(-17 + 33*t2 + 20*Power(t2,2)) - 
               t1*(75 - 187*t2 + 534*Power(t2,2) + 25*Power(t2,3)) + 
               Power(t1,2)*(91 - 115*t2 + 324*Power(t2,2) + 
                  62*Power(t2,3)) + 
               Power(s1,2)*(136 - 125*Power(t1,3) + 13*t2 - 
                  t1*(571 + 265*t2) + Power(t1,2)*(560 + 297*t2)) + 
               s1*(85 + 36*Power(t1,4) - 339*t2 + 25*Power(t2,2) + 
                  Power(t1,3)*(3 + 58*t2) + 
                  t1*(-95 + 1084*t2 + 173*Power(t2,2)) - 
                  Power(t1,2)*(29 + 803*t2 + 243*Power(t2,2)))) + 
            Power(s2,3)*(45 + 
               Power(s1,3)*(-8 + 92*t1 - 111*Power(t1,2) - 
                  87*Power(t1,3) + 43*Power(t1,4) + 60*Power(t1,5) + 
                  30*Power(t1,6)) - 51*t2 + 29*Power(t2,2) + 
               45*Power(t2,3) - 6*Power(t1,8)*(1 + t2) + 
               Power(t1,7)*(-75 - 34*t2 + 12*Power(t2,2)) + 
               t1*(209 + 298*t2 - 912*Power(t2,2) - 49*Power(t2,3)) + 
               2*Power(t1,6)*
                (58 + 71*t2 + 253*Power(t2,2) - 9*Power(t2,3)) + 
               Power(t1,4)*(9 + 413*t2 + 1500*Power(t2,2) + 
                  15*Power(t2,3)) - 
               Power(t1,5)*(191 + 392*t2 + 1453*Power(t2,2) + 
                  100*Power(t2,3)) - 
               Power(t1,2)*(908 + 554*t2 - 2271*Power(t2,2) + 
                  168*Power(t2,3)) + 
               Power(t1,3)*(801 + 184*t2 - 1953*Power(t2,2) + 
                  256*Power(t2,3)) + 
               Power(s1,2)*(66 - 109*Power(t1,7) + 14*t2 + 
                  Power(t1,6)*(245 + 16*t2) - 
                  Power(t1,3)*(846 + 35*t2) + 
                  Power(t1,4)*(846 + 60*t2) - t1*(592 + 245*t2) - 
                  Power(t1,5)*(705 + 313*t2) + 
                  Power(t1,2)*(1095 + 446*t2)) + 
               s1*(-57 + 12*Power(t1,8) - 10*t2 - 69*Power(t2,2) + 
                  Power(t1,7)*(127 + 79*t2) + 
                  Power(t1,2)*(2642 - 4718*t2 + 17*Power(t2,2)) - 
                  5*Power(t1,3)*(340 - 727*t2 + 39*Power(t2,2)) - 
                  Power(t1,6)*(311 + 699*t2 + 43*Power(t2,2)) + 
                  t1*(-803 + 1820*t2 + 162*Power(t2,2)) + 
                  2*Power(t1,5)*(570 + 764*t2 + 249*Power(t2,2)) - 
                  Power(t1,4)*(1050 + 1635*t2 + 313*Power(t2,2)))) + 
            s2*(-1 + t1)*(-2*Power(-1 + t2,3) - 
               t1*Power(-1 + t2,2)*(-145 + 34*s1 + 37*t2) + 
               2*Power(t1,7)*
                (-2 + 7*Power(s1,2) + 3*t2 - 2*s1*(1 + 3*t2)) + 
               Power(t1,5)*(83 - 590*t2 + 536*Power(t2,2) + 
                  6*Power(t2,3) + 2*Power(s1,2)*(89 + 34*t2) + 
                  s1*(396 - 538*t2 - 131*Power(t2,2))) + 
               Power(t1,6)*(3 - 4*Power(s1,3) + 70*t2 - 
                  72*Power(t2,2) - Power(s1,2)*(77 + 4*t2) + 
                  s1*(-67 + 139*t2 + 12*Power(t2,2))) + 
               Power(t1,2)*(-647 + 8*Power(s1,3) + 1347*t2 - 
                  839*Power(t2,2) + 63*Power(t2,3) - 
                  2*Power(s1,2)*(33 + 7*t2) + 
                  s1*(217 - 310*t2 + 229*Power(t2,2))) + 
               Power(t1,4)*(-510 + 58*Power(s1,3) + 1619*t2 - 
                  1357*Power(t2,2) - 34*Power(t2,3) - 
                  73*Power(s1,2)*(5 + 3*t2) + 
                  s1*(-424 + 769*t2 + 435*Power(t2,2))) - 
               Power(t1,3)*(-928 + 52*Power(s1,3) + 2119*t2 - 
                  1507*Power(t2,2) + 6*Power(t2,3) - 
                  Power(s1,2)*(316 + 139*t2) + 
                  s1*(84 + 116*t2 + 481*Power(t2,2)))) + 
            Power(s2,5)*(130 + 
               Power(s1,3)*(9 - 31*t1 - 123*Power(t1,2) + 
                  135*Power(t1,3) + 5*Power(t1,4)) + 
               2*Power(t1,6)*(-30 + t2) - 162*t2 + 23*Power(t2,2) + 
               28*Power(t2,3) + 
               Power(t1,5)*(-60 - 7*t2 + 82*Power(t2,2)) + 
               Power(t1,4)*(405 - 849*t2 + 1450*Power(t2,2) - 
                  103*Power(t2,3)) + 
               Power(t1,3)*(-777 + 2035*t2 - 3937*Power(t2,2) + 
                  67*Power(t2,3)) - 
               t1*(647 - 648*t2 + 447*Power(t2,2) + 149*Power(t2,3)) + 
               Power(t1,2)*(1009 - 1667*t2 + 2829*Power(t2,2) + 
                  162*Power(t2,3)) + 
               Power(s1,2)*(-484*Power(t1,5) - 4*(11 + t2) + 
                  3*t1*(-13 + 6*t2) + Power(t1,4)*(1495 + 142*t2) - 
                  3*Power(t1,3)*(685 + 212*t2) + 
                  Power(t1,2)*(1127 + 495*t2)) + 
               s1*(-144 + 66*Power(t1,6) + 221*t2 - 65*Power(t2,2) + 
                  Power(t1,5)*(122 + 311*t2) + 
                  Power(t1,4)*(336 - 2840*t2 + 3*Power(t2,2)) + 
                  t1*(910 - 530*t2 + 307*Power(t2,2)) + 
                  Power(t1,3)*(-680 + 5439*t2 + 421*Power(t2,2)) - 
                  Power(t1,2)*(610 + 2601*t2 + 681*Power(t2,2)))) - 
            Power(s2,2)*(Power(s1,3)*t1*
                (-16 + 136*t1 - 225*Power(t1,2) + 54*Power(t1,3) + 
                  50*Power(t1,4) + 2*Power(t1,5) + 4*Power(t1,6)) - 
               2*Power(t1,8)*(7 + 12*t2) + 
               Power(-1 + t2,2)*(-143 + 35*t2) + 
               Power(t1,7)*(-26 + 185*t2 + 60*Power(t2,2)) + 
               t1*(803 - 1635*t2 + 997*Power(t2,2) - 25*Power(t2,3)) - 
               Power(t1,6)*(-152 + 458*t2 + 225*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,4)*(345 + 1966*t2 - 3009*Power(t2,2) + 
                  53*Power(t2,3)) - 
               Power(t1,5)*(419 + 19*t2 - 973*Power(t2,2) + 
                  58*Power(t2,3)) - 
               Power(t1,2)*(1388 - 3481*t2 + 2767*Power(t2,2) + 
                  104*Power(t2,3)) + 
               Power(t1,3)*(690 - 3817*t2 + 4184*Power(t2,2) + 
                  106*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-14*Power(t1,7) + 4*Power(t1,6)*(13 + t2) + 
                  4*(33 + 7*t2) + Power(t1,4)*(697 + 31*t2) - 
                  11*t1*(79 + 32*t2) - Power(t1,5)*(317 + 40*t2) - 
                  8*Power(t1,3)*(159 + 56*t2) + 
                  Power(t1,2)*(1591 + 762*t2)) + 
               s1*(34*Power(-1 + t2,2) + 4*Power(t1,8)*(10 + 3*t2) + 
                  Power(t1,3)*(2177 - 4345*t2 - 1059*Power(t2,2)) + 
                  Power(t1,5)*(718 - 1971*t2 - 26*Power(t2,2)) - 
                  Power(t1,7)*(157 + 114*t2 + 12*Power(t2,2)) + 
                  Power(t1,6)*(315 + 533*t2 + 70*Power(t2,2)) - 
                  4*t1*(77 - 92*t2 + 83*Power(t2,2)) + 
                  Power(t1,4)*(-2643 + 4514*t2 + 420*Power(t2,2)) + 
                  Power(t1,2)*(-176 + 1071*t2 + 920*Power(t2,2)))) + 
            Power(s2,4)*(-116 - 
               Power(s1,3)*(16 + 13*t1 - 109*Power(t1,2) - 
                  83*Power(t1,3) + 124*Power(t1,4) + 60*Power(t1,5)) - 
               43*t2 + 251*Power(t2,2) - 44*Power(t2,3) + 
               Power(t1,7)*(32 + 11*t2) + 
               Power(t1,6)*(114 + 2*t2 - 59*Power(t2,2)) + 
               Power(t1,2)*(669 - 864*t2 + 1001*Power(t2,2) + 
                  32*Power(t2,3)) + 
               Power(t1,5)*(-385 + 513*t2 - 1243*Power(t2,2) + 
                  82*Power(t2,3)) + 
               Power(t1,4)*(897 - 1335*t2 + 3615*Power(t2,2) + 
                  113*Power(t2,3)) + 
               t1*(138 + 367*t2 - 704*Power(t2,2) + 128*Power(t2,3)) - 
               Power(t1,3)*(1349 - 1349*t2 + 2861*Power(t2,2) + 
                  290*Power(t2,3)) + 
               Power(s1,2)*(105 + 323*Power(t1,6) + 46*t2 - 
                  11*Power(t1,5)*(77 + 3*t2) - t1*(141 + 38*t2) - 
                  3*Power(t1,2)*(-34 + 45*t2) + 
                  2*Power(t1,4)*(733 + 336*t2) - 
                  Power(t1,3)*(1008 + 449*t2)) - 
               s1*(-326 + 45*Power(t1,7) + 555*t2 - 48*Power(t2,2) + 
                  Power(t1,6)*(159 + 211*t2) + 
                  Power(t1,5)*(6 - 1979*t2 - 6*Power(t2,2)) + 
                  t1*(661 - 1037*t2 + 95*Power(t2,2)) + 
                  Power(t1,2)*(960 - 211*t2 + 255*Power(t2,2)) + 
                  Power(t1,4)*(635 + 3917*t2 + 834*Power(t2,2)) - 
                  Power(t1,3)*(2140 + 1456*t2 + 1067*Power(t2,2))))) + 
         Power(s,4)*(2*t1*(15 - 136*t1 + 352*Power(t1,2) - 
               371*Power(t1,3) + 169*Power(t1,4) - 29*Power(t1,5) + 
               Power(t1,6)) + 
            Power(s2,7)*(-80 + s1*(60 - 90*t1) + 60*t1 + 
               40*Power(t1,2) - 70*t2 + 102*t1*t2) - 
            Power(s2,6)*(210 + 10*Power(s1,3) + 240*Power(t1,3) + 
               2*Power(s1,2)*(-79 + 140*t1 - 30*t2) - 660*t2 - 
               260*Power(t2,2) - 56*Power(t2,3) + 
               Power(t1,2)*(214 + 211*t2) + 
               t1*(-616 + 778*t2 + 397*Power(t2,2)) + 
               s1*(592 - 64*Power(t1,2) + 446*t2 + 105*Power(t2,2) - 
                  t1*(806 + 725*t2))) + 
            Power(s2,5)*(-606 + Power(s1,3)*(61 - 145*t1) + 
               401*Power(t1,4) + 2052*t2 - 299*Power(t2,2) + 
               150*Power(t2,3) + Power(t1,3)*(685 + 321*t2) + 
               Power(t1,2)*(-2422 + 3863*t2 + 775*Power(t2,2)) + 
               t1*(1979 - 5892*t2 + 69*Power(t2,2) - 170*Power(t2,3)) + 
               Power(s1,2)*(-294 + 62*Power(t1,2) - 155*t2 + 
                  t1*(548 + 365*t2)) + 
               s1*(-1339 + 278*Power(t1,3) + 495*t2 - 61*Power(t2,2) - 
                  Power(t1,2)*(3636 + 1093*t2) + 
                  t1*(4446 - 310*t2 - 30*Power(t2,2)))) + 
            Power(s2,4)*(-1873 - 188*Power(t1,5) + 
               Power(s1,3)*(153 - 403*t1 + 172*Power(t1,2)) + 3139*t2 - 
               1628*Power(t2,2) + 107*Power(t2,3) - 
               2*Power(t1,4)*(676 + 139*t2) + 
               Power(t1,3)*(4834 - 7196*t2 - 530*Power(t2,2)) + 
               t1*(6248 - 12850*t2 + 4422*Power(t2,2) - 
                  481*Power(t2,3)) + 
               Power(t1,2)*(-7488 + 17177*t2 - 2311*Power(t2,2) + 
                  500*Power(t2,3)) + 
               Power(s1,2)*(-893 + 967*Power(t1,3) - 509*t2 - 
                  Power(t1,2)*(2647 + 750*t2) + t1*(2961 + 1405*t2)) + 
               s1*(-1028 - 713*Power(t1,4) + 1982*t2 + 311*Power(t2,2) + 
                  Power(t1,3)*(6174 + 67*t2) + 
                  t1*(5952 - 5947*t2 - 573*Power(t2,2)) + 
                  Power(t1,2)*(-10766 + 3746*t2 + 37*Power(t2,2)))) + 
            Power(s2,3)*(-2164 - 60*Power(t1,6) + 
               Power(s1,3)*(123 - 548*t1 + 598*Power(t1,2) + 
                  57*Power(t1,3)) + Power(t1,5)*(1195 - 56*t2) + 
               3594*t2 - 1856*Power(t2,2) + 21*Power(t2,3) + 
               2*Power(t1,4)*(-2393 + 3341*t2 + 82*Power(t2,2)) + 
               Power(t1,3)*(12371 - 22476*t2 + 3862*Power(t2,2) - 
                  616*Power(t2,3)) + 
               t1*(10618 - 17433*t2 + 8492*Power(t2,2) - 
                  174*Power(t2,3)) + 
               Power(t1,2)*(-17473 + 29603*t2 - 11089*Power(t2,2) + 
                  539*Power(t2,3)) + 
               Power(s1,2)*(-598 - 1199*Power(t1,4) - 383*t2 + 
                  Power(t1,3)*(3247 + 487*t2) - 
                  5*Power(t1,2)*(1115 + 524*t2) + t1*(3377 + 1950*t2)) + 
               s1*(-787 + 779*Power(t1,5) + 1180*t2 + 471*Power(t2,2) + 
                  Power(t1,4)*(-5391 + 555*t2) + 
                  2*Power(t1,3)*(6291 - 2549*t2 + Power(t2,2)) - 
                  2*t1*(-2730 + 3827*t2 + 922*Power(t2,2)) + 
                  Power(t1,2)*(-12004 + 11915*t2 + 1970*Power(t2,2)))) - 
            Power(s2,2)*(1983 - 52*Power(t1,7) + 
               Power(s1,3)*(-54 + 411*t1 - 703*Power(t1,2) + 
                  296*Power(t1,3) + 98*Power(t1,4)) + 
               Power(t1,6)*(433 - 143*t2) - 2725*t2 + 1093*Power(t2,2) + 
               42*Power(t2,3) + 
               Power(t1,5)*(-2492 + 2960*t2 + 15*Power(t2,2)) + 
               Power(t1,4)*(9913 - 13994*t2 + 2409*Power(t2,2) - 
                  268*Power(t2,3)) + 
               Power(t1,2)*(19800 - 31237*t2 + 13481*Power(t2,2) - 
                  67*Power(t2,3)) - 
               t1*(9826 - 15096*t2 + 6853*Power(t2,2) + 
                  109*Power(t2,3)) + 
               Power(t1,3)*(-19900 + 29946*t2 - 10173*Power(t2,2) + 
                  328*Power(t2,3)) + 
               Power(s1,2)*(265 - 526*Power(t1,5) + 128*t2 + 
                  3*Power(t1,4)*(563 + 56*t2) - 
                  6*Power(t1,3)*(617 + 297*t2) - t1*(1964 + 1179*t2) + 
                  Power(t1,2)*(4048 + 2551*t2)) + 
               s1*(-387 + 374*Power(t1,6) + 320*t2 - 405*Power(t2,2) + 
                  3*Power(t1,5)*(-830 + 101*t2) + 
                  Power(t1,2)*(8939 - 8194*t2 - 3347*Power(t2,2)) + 
                  Power(t1,4)*(8043 - 2756*t2 - 123*Power(t2,2)) + 
                  Power(t1,3)*(-12712 + 8455*t2 + 1962*Power(t2,2)) + 
                  t1*(-1394 + 1935*t2 + 2075*Power(t2,2)))) + 
            s2*(-30 - 5*Power(t1,8) + Power(t1,7)*(71 + 56*s1 - 29*t2) + 
               6*t2 + 15*Power(t2,2) - 19*Power(t2,3) - 
               Power(t1,6)*(733 + 80*Power(s1,2) + s1*(514 - 55*t2) - 
                  525*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(13583 - 326*Power(s1,3) - 19109*t2 + 
                  7081*Power(t2,2) + 20*Power(t2,3) + 
                  2*Power(s1,2)*(827 + 597*t2) + 
                  s1*(3991 - 1906*t2 - 2066*Power(t2,2))) + 
               t1*(3592 - 80*Power(s1,3) - 4955*t2 + 2051*Power(t2,2) + 
                  95*Power(t2,3) + 6*Power(s1,2)*(71 + 30*t2) + 
                  s1*(-808 + 760*t2 - 725*Power(t2,2))) + 
               Power(t1,5)*(3680 + 20*Power(s1,3) - 3795*t2 + 
                  523*Power(t2,2) - 31*Power(t2,3) + 
                  Power(s1,2)*(356 + 30*t2) + 
                  s1*(2358 - 552*t2 - 55*Power(t2,2))) + 
               Power(t1,4)*(-9329 + 34*Power(s1,3) + 12037*t2 - 
                  3236*Power(t2,2) + 107*Power(t2,3) - 
                  2*Power(s1,2)*(395 + 212*t2) + 
                  s1*(-5149 + 1748*t2 + 658*Power(t2,2))) + 
               Power(t1,2)*(-10863 + 342*Power(s1,3) + 15270*t2 - 
                  6393*Power(t2,2) - 178*Power(t2,3) - 
                  2*Power(s1,2)*(795 + 472*t2) + 
                  s1*(184 - 159*t2 + 2172*Power(t2,2))))) + 
         Power(s,2)*(2*Power(-1 + t1,2)*Power(t1,2)*
             (-4 + 21*t1 - 18*Power(t1,2) + 4*Power(t1,3)) - 
            6*Power(s2,9)*(-1 + t1)*(s1 - t2) + 
            Power(s2,8)*(-6 - 5*Power(s1,3) - 6*Power(t1,3) + 76*t2 + 
               82*Power(t2,2) + 8*Power(t2,3) + 
               Power(t1,2)*(10 + 11*t2) + 
               Power(s1,2)*(73 - 84*t1 + 18*t2) + 
               t1*(2 - 87*t2 - 93*Power(t2,2)) - 
               s1*(72 - 87*t1 + 15*Power(t1,2) + 159*t2 - 181*t1*t2 + 
                  21*Power(t2,2))) + 
            Power(s2,7)*(-50 + Power(s1,3)*(76 - 92*t1) + 
               23*Power(t1,4) + 193*t2 - 194*Power(t2,2) - 
               36*Power(t2,3) - Power(t1,3)*(34 + 41*t2) + 
               Power(t1,2)*(-79 + 439*t2 + 251*Power(t2,2)) + 
               t1*(140 - 591*t2 + 7*Power(t2,2) + 40*Power(t2,3)) + 
               Power(s1,2)*(-239 + 102*Power(t1,2) - 202*t2 + 
                  t1*(201 + 238*t2)) + 
               s1*(-141 + 93*Power(t1,3) + 414*t2 + 158*Power(t2,2) - 
                  Power(t1,2)*(431 + 408*t2) + 
                  t1*(479 - 134*t2 - 182*Power(t2,2)))) + 
            Power(s2,6)*(-116 - 28*Power(t1,5) + 
               Power(s1,3)*(91 - 293*t1 + 252*Power(t1,2)) + 
               Power(t1,4)*(61 - 12*t2) + 322*t2 - 768*Power(t2,2) + 
               7*Power(t2,3) + 
               Power(t1,3)*(282 - 992*t2 - 280*Power(t2,2)) + 
               t1*(448 - 1685*t2 + 2067*Power(t2,2) - 20*Power(t2,3)) - 
               Power(t1,2)*(647 - 2367*t2 + 1110*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(s1,2)*(-583 + 391*Power(t1,3) - 261*t2 - 
                  2*Power(t1,2)*(893 + 351*t2) + t1*(1887 + 820*t2)) + 
               s1*(-203*Power(t1,4) + Power(t1,3)*(980 + 75*t2) + 
                  t1*(1015 - 3806*t2 - 441*Power(t2,2)) + 
                  6*(-15 + 215*t2 + 24*Power(t2,2)) + 
                  Power(t1,2)*(-1702 + 2623*t2 + 433*Power(t2,2)))) + 
            Power(s2,5)*(-534 + 6*Power(t1,6) + 
               Power(s1,3)*(21 - 339*t1 + 488*Power(t1,2) - 
                  153*Power(t1,3)) + 699*t2 - 791*Power(t2,2) - 
               32*Power(t2,3) + Power(t1,5)*(-73 + 46*t2) + 
               2*Power(t1,4)*(-223 + 747*t2 + 123*Power(t2,2)) + 
               Power(t1,3)*(1457 - 5014*t2 + 2952*Power(t2,2) - 
                  206*Power(t2,3)) + 
               t1*(1718 - 2720*t2 + 4419*Power(t2,2) + 85*Power(t2,3)) + 
               Power(t1,2)*(-2128 + 5495*t2 - 6882*Power(t2,2) + 
                  168*Power(t2,3)) + 
               Power(s1,2)*(-391 - 1059*Power(t1,4) - 105*t2 + 
                  34*t1*(81 + 34*t2) - 7*Power(t1,2)*(711 + 247*t2) + 
                  Power(t1,3)*(3617 + 659*t2)) + 
               s1*(240 + 315*Power(t1,5) + 749*t2 + 141*Power(t2,2) + 
                  Power(t1,4)*(-1353 + 535*t2) + 
                  Power(t1,3)*(2995 - 6003*t2 - 194*Power(t2,2)) - 
                  t1*(444 + 5873*t2 + 918*Power(t2,2)) + 
                  Power(t1,2)*(-1753 + 10704*t2 + 958*Power(t2,2)))) - 
            Power(s2,4)*(253 - 10*Power(t1,7) + 
               Power(s1,3)*(-30 + 108*t1 - 489*Power(t1,2) + 
                  448*Power(t1,3) + 84*Power(t1,4)) - 1021*t2 + 
               914*Power(t2,2) - Power(t1,6)*(71 + 29*t2) + 
               Power(t1,5)*(-296 + 1376*t2 + 185*Power(t2,2)) + 
               Power(t1,4)*(1476 - 5135*t2 + 3523*Power(t2,2) - 
                  314*Power(t2,3)) + 
               Power(t1,3)*(-3884 + 8170*t2 - 10612*Power(t2,2) + 
                  7*Power(t2,3)) - 
               t1*(2224 - 4715*t2 + 4556*Power(t2,2) + 
                  137*Power(t2,3)) + 
               Power(t1,2)*(4756 - 8076*t2 + 10307*Power(t2,2) + 
                  386*Power(t2,3)) + 
               Power(s1,2)*(-1054*Power(t1,5) + 120*(2 + t2) - 
                  9*Power(t1,3)*(706 + 243*t2) + 
                  Power(t1,4)*(3421 + 276*t2) - t1*(1612 + 587*t2) + 
                  Power(t1,2)*(5120 + 2078*t2)) + 
               s1*(688 + 306*Power(t1,6) - 974*t2 - 114*Power(t2,2) + 
                  3*Power(t1,5)*(-338 + 213*t2) + 
                  Power(t1,4)*(2325 - 6155*t2 + Power(t2,2)) + 
                  3*t1*(-347 + 1406*t2 + 280*Power(t2,2)) + 
                  2*Power(t1,3)*(-388 + 6938*t2 + 951*Power(t2,2)) - 
                  2*Power(t1,2)*(244 + 5563*t2 + 1196*Power(t2,2)))) + 
            Power(s2,3)*(-226 - 5*Power(t1,8) + 
               Power(s1,3)*(47 - 240*t1 + 314*Power(t1,2) - 
                  321*Power(t1,3) + 195*Power(t1,4) + 111*Power(t1,5)) + 
               719*t2 - 652*Power(t2,2) - 43*Power(t2,3) - 
               3*Power(t1,7)*(19 + 17*t2) + 
               Power(t1,6)*(35 + 573*t2 + 67*Power(t2,2)) + 
               Power(t1,5)*(386 - 2102*t2 + 2057*Power(t2,2) - 
                  144*Power(t2,3)) - 
               Power(t1,2)*(2877 - 10853*t2 + 10463*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(948 - 4738*t2 + 4564*Power(t2,2) + 25*Power(t2,3)) - 
               Power(t1,4)*(2615 - 5823*t2 + 8171*Power(t2,2) + 
                  121*Power(t2,3)) + 
               Power(t1,3)*(4411 - 11077*t2 + 12398*Power(t2,2) + 
                  223*Power(t2,3)) + 
               Power(s1,2)*(-279 - 508*Power(t1,6) - 113*t2 + 
                  Power(t1,5)*(1780 + 87*t2) - 
                  3*Power(t1,2)*(1132 + 483*t2) - 
                  2*Power(t1,4)*(2250 + 719*t2) + t1*(1535 + 749*t2) + 
                  2*Power(t1,3)*(2584 + 945*t2)) + 
               s1*(-111 + 146*Power(t1,7) + 409*t2 + 231*Power(t2,2) + 
                  Power(t1,6)*(-314 + 333*t2) + 
                  t1*(2930 - 4579*t2 - 914*Power(t2,2)) + 
                  Power(t1,5)*(690 - 3309*t2 - 94*Power(t2,2)) - 
                  2*Power(t1,3)*(-1988 + 6601*t2 + 1277*Power(t2,2)) + 
                  Power(t1,4)*(-848 + 9933*t2 + 1763*Power(t2,2)) + 
                  Power(t1,2)*(-6469 + 10815*t2 + 1798*Power(t2,2)))) + 
            s2*(-(Power(-1 + t2,2)*(-5 + 11*t2)) + 
               Power(t1,8)*(6 - 16*s1 - 7*Power(s1,2) + 6*t2 + 
                  6*s1*t2) + Power(t1,5)*
                (1794 - 48*Power(s1,3) - 5007*t2 + 2974*Power(t2,2) + 
                  22*Power(t2,3) + Power(s1,2)*(889 + 415*t2) + 
                  s1*(2542 - 2298*t2 - 761*Power(t2,2))) + 
               t1*(751 - 4*Power(s1,3) - 1412*t2 + 733*Power(t2,2) - 
                  24*Power(t2,3) + 5*Power(s1,2)*(7 + t2) + 
                  s1*(-232 + 361*t2 - 197*Power(t2,2))) - 
               Power(t1,6)*(479 + 17*Power(s1,3) - 1345*t2 + 
                  622*Power(t2,2) - 6*Power(t2,3) + 
                  Power(s1,2)*(373 + 54*t2) + 
                  s1*(1004 - 803*t2 - 115*Power(t2,2))) + 
               Power(t1,7)*(54 + 2*Power(s1,3) - 154*t2 + 
                  36*Power(t2,2) + 2*Power(s1,2)*(50 + t2) + 
                  s1*(159 - 119*t2 - 6*Power(t2,2))) + 
               Power(t1,2)*(-3226 + 75*Power(s1,3) + 6105*t2 - 
                  3425*Power(t2,2) + 32*Power(t2,3) - 
                  2*Power(s1,2)*(239 + 83*t2) + 
                  s1*(873 - 967*t2 + 1043*Power(t2,2))) + 
               Power(t1,4)*(-4106 + 249*Power(s1,3) + 9779*t2 - 
                  6247*Power(t2,2) - 121*Power(t2,3) - 
                  Power(s1,2)*(1575 + 928*t2) + 
                  s1*(-2215 + 2674*t2 + 1913*Power(t2,2))) + 
               Power(t1,3)*(5201 - 252*Power(s1,3) - 10641*t2 + 
                  6524*Power(t2,2) + 91*Power(t2,3) + 
                  Power(s1,2)*(1409 + 711*t2) - 
                  s1*(107 + 460*t2 + 2092*Power(t2,2)))) + 
            Power(s2,2)*(-626 - 
               Power(s1,3)*(-4 + 122*t1 - 462*Power(t1,2) + 
                  476*Power(t1,3) - 128*Power(t1,4) + Power(t1,5) + 
                  31*Power(t1,6)) + 1181*t2 - 622*Power(t2,2) + 
               27*Power(t2,3) + 2*Power(t1,8)*(11 + 6*t2) - 
               3*Power(t1,7)*(32 + 17*t2 + 2*Power(t2,2)) + 
               Power(t1,6)*(96 + 275*t2 - 497*Power(t2,2) + 
                  18*Power(t2,3)) + 
               Power(t1,5)*(890 - 2909*t2 + 3132*Power(t2,2) + 
                  18*Power(t2,3)) + 
               Power(t1,4)*(-2782 + 9947*t2 - 8449*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(2898 - 5774*t2 + 3465*Power(t2,2) + 69*Power(t2,3)) + 
               Power(t1,3)*(4600 - 15417*t2 + 12260*Power(t2,2) + 
                  75*Power(t2,3)) - 
               2*Power(t1,2)*(2501 - 6368*t2 + 4614*Power(t2,2) + 
                  103*Power(t2,3)) + 
               Power(s1,2)*(111*Power(t1,7) - 5*(7 + t2) - 
                  2*Power(t1,6)*(282 + 13*t2) + 
                  8*Power(t1,5)*(231 + 52*t2) - 
                  6*Power(t1,4)*(518 + 187*t2) + t1*(757 + 279*t2) - 
                  4*Power(t1,2)*(676 + 335*t2) + 
                  5*Power(t1,3)*(750 + 379*t2)) + 
               s1*(204 - 24*Power(t1,8) + Power(t1,7)*(27 - 83*t2) - 
                  305*t2 + 169*Power(t2,2) + 
                  Power(t1,3)*(7563 - 10340*t2 - 2852*Power(t2,2)) + 
                  Power(t1,5)*(1963 - 4188*t2 - 643*Power(t2,2)) + 
                  Power(t1,6)*(-224 + 946*t2 + 65*Power(t2,2)) - 
                  2*t1*(321 - 159*t2 + 577*Power(t2,2)) + 
                  Power(t1,4)*(-6532 + 9077*t2 + 1637*Power(t2,2)) + 
                  Power(t1,2)*(-2335 + 4465*t2 + 2692*Power(t2,2))))) + 
         Power(s,3)*(-2*t1*(2 - 37*t1 + 146*Power(t1,2) - 
               222*Power(t1,3) + 151*Power(t1,4) - 44*Power(t1,5) + 
               4*Power(t1,6)) + 
            2*Power(s2,8)*(7 - 3*Power(t1,2) + 3*s1*(-5 + 6*t1) + 
               16*t2 - t1*(4 + 19*t2)) + 
            Power(s2,7)*(48 + 10*Power(s1,3) + 63*Power(t1,3) + 
               Power(s1,2)*(-152 + 210*t1 - 45*t2) - 314*t2 - 
               200*Power(t2,2) - 28*Power(t2,3) + 
               Power(t1,2)*(-1 + 51*t2) + 
               t1*(-114 + 325*t2 + 261*Power(t2,2)) + 
               s1*(296 - 9*Power(t1,2) + 369*t2 + 63*Power(t2,2) - 
                  t1*(341 + 492*t2))) + 
            Power(s2,6)*(140 - 154*Power(t1,4) + 
               2*Power(s1,3)*(-47 + 75*t1) - 939*t2 + 321*Power(t2,2) - 
               27*Power(t2,3) - Power(t1,3)*(39 + 73*t2) + 
               Power(t1,2)*(629 - 1804*t2 - 635*Power(t2,2)) + 
               t1*(-560 + 2717*t2 + 20*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s1,2)*(362 - 171*Power(t1,2) + 240*t2 - 
                  t1*(420 + 367*t2)) + 
               s1*(699 - 199*Power(t1,3) - 633*t2 - 106*Power(t2,2) + 
                  3*Power(t1,2)*(607 + 321*t2) + 
                  t1*(-2258 + 217*t2 + 174*Power(t2,2)))) + 
            Power(s2,5)*(785 + 122*Power(t1,5) + 
               Power(s1,3)*(-175 + 444*t1 - 297*Power(t1,2)) - 1445*t2 + 
               1511*Power(t2,2) - 15*Power(t2,3) + 
               Power(t1,4)*(263 + 170*t2) + 
               Power(t1,3)*(-1629 + 4011*t2 + 610*Power(t2,2)) + 
               Power(t1,2)*(2491 - 9236*t2 + 2026*Power(t2,2) - 
                  220*Power(t2,3)) + 
               t1*(-2088 + 6438*t2 - 3889*Power(t2,2) + 
                  234*Power(t2,3)) + 
               Power(s1,2)*(-797*Power(t1,3) + 6*(180 + 91*t2) + 
                  Power(t1,2)*(2921 + 930*t2) - t1*(3184 + 1375*t2)) + 
               s1*(289 + 517*Power(t1,4) - 2275*t2 - 347*Power(t2,2) - 
                  3*Power(t1,3)*(1278 + 67*t2) + 
                  Power(t1,2)*(6468 - 4232*t2 - 323*Power(t2,2)) + 
                  t1*(-3250 + 6342*t2 + 614*Power(t2,2)))) + 
            Power(s2,4)*(1335 + 6*Power(t1,6) + 
               Power(s1,3)*(-103 + 594*t1 - 741*Power(t1,2) + 
                  58*Power(t1,3)) - 2489*t2 + 1925*Power(t2,2) + 
               25*Power(t2,3) - Power(t1,5)*(391 + 54*t2) + 
               Power(t1,4)*(1909 - 4953*t2 - 360*Power(t2,2)) + 
               Power(t1,2)*(8041 - 17843*t2 + 11558*Power(t2,2) - 
                  354*Power(t2,3)) - 
               t1*(5868 - 10372*t2 + 8481*Power(t2,2) + 
                  65*Power(t2,3)) + 
               Power(t1,3)*(-4920 + 15219*t2 - 4462*Power(t2,2) + 
                  544*Power(t2,3)) + 
               Power(s1,2)*(765 + 1548*Power(t1,4) + 375*t2 + 
                  11*Power(t1,2)*(689 + 269*t2) - 
                  Power(t1,3)*(4806 + 737*t2) - t1*(4552 + 2147*t2)) + 
               s1*(727 - 714*Power(t1,5) + Power(t1,4)*(4344 - 717*t2) - 
                  1954*t2 - 377*Power(t2,2) + 
                  Power(t1,2)*(6411 - 15733*t2 - 1939*Power(t2,2)) + 
                  Power(t1,3)*(-8967 + 7753*t2 + 54*Power(t2,2)) + 
                  t1*(-2301 + 10097*t2 + 1830*Power(t2,2)))) + 
            Power(s2,3)*(1031 - 41*Power(t1,7) + 
               Power(s1,3)*(-95 + 457*t1 - 826*Power(t1,2) + 
                  523*Power(t1,3) + 151*Power(t1,4)) + 
               Power(t1,6)*(174 - 119*t2) - 2442*t2 + 1630*Power(t2,2) + 
               51*Power(t2,3) + 
               Power(t1,5)*(-877 + 3225*t2 + 141*Power(t2,2)) + 
               Power(t1,4)*(4505 - 12477*t2 + 3996*Power(t2,2) - 
                  424*Power(t2,3)) - 
               t1*(5876 - 13237*t2 + 9123*Power(t2,2) + 
                  128*Power(t2,3)) + 
               Power(t1,2)*(12822 - 25618*t2 + 17717*Power(t2,2) + 
                  170*Power(t2,3)) + 
               Power(t1,3)*(-11838 + 23996*t2 - 14680*Power(t2,2) + 
                  173*Power(t2,3)) + 
               Power(s1,2)*(504 - 1092*Power(t1,5) + 264*t2 + 
                  Power(t1,4)*(3641 + 294*t2) - 
                  5*Power(t1,3)*(1559 + 580*t2) - t1*(2945 + 1578*t2) + 
                  Power(t1,2)*(7129 + 3413*t2)) + 
               s1*(551 + 514*Power(t1,6) - 905*t2 - 458*Power(t2,2) + 
                  Power(t1,5)*(-2702 + 643*t2) + 
                  Power(t1,4)*(6993 - 6108*t2 - 81*Power(t2,2)) - 
                  3*Power(t1,2)*(-3067 + 5840*t2 + 1266*Power(t2,2)) + 
                  t1*(-4821 + 7543*t2 + 2027*Power(t2,2)) + 
                  Power(t1,3)*(-9322 + 17092*t2 + 2781*Power(t2,2)))) + 
            s2*(2 + 2*Power(t1,8)*(2 + 6*s1 - 3*t2) + 21*t2 - 
               42*Power(t2,2) + 23*Power(t2,3) + 
               Power(t1,7)*(-92 - 37*Power(s1,2) + 120*t2 + 
                  s1*(-161 + 29*t2)) + 
               Power(t1,2)*(7343 - 238*Power(s1,3) - 12254*t2 + 
                  6140*Power(t2,2) + 172*Power(t2,3) + 
                  Power(s1,2)*(1275 + 596*t2) + 
                  s1*(-1165 + 917*t2 - 2043*Power(t2,2))) + 
               Power(t1,4)*(7715 - 208*Power(s1,3) - 14190*t2 + 
                  6498*Power(t2,2) + 37*Power(t2,3) + 
                  Power(s1,2)*(1615 + 998*t2) + 
                  s1*(4912 - 3295*t2 - 1764*Power(t2,2))) - 
               Power(t1,5)*(3534 + 14*Power(s1,3) - 6015*t2 + 
                  2024*Power(t2,2) - 40*Power(t2,3) + 
                  Power(s1,2)*(733 + 221*t2) + 
                  s1*(3452 - 1749*t2 - 392*Power(t2,2))) + 
               Power(t1,6)*(922 + 10*Power(s1,3) - 1285*t2 + 
                  216*Power(t2,2) - 6*Power(t2,3) + 
                  Power(s1,2)*(271 + 12*t2) + 
                  s1*(1013 - 383*t2 - 29*Power(t2,2))) + 
               t1*(-2074 + 30*Power(s1,3) + 3375*t2 - 
                  1541*Power(t2,2) - 54*Power(t2,3) - 
                  Power(s1,2)*(196 + 57*t2) + 
                  s1*(616 - 745*t2 + 504*Power(t2,2))) + 
               Power(t1,3)*(-10286 + 436*Power(s1,3) + 18202*t2 - 
                  9303*Power(t2,2) - 217*Power(t2,3) - 
                  Power(s1,2)*(2251 + 1365*t2) + 
                  s1*(-1773 + 1840*t2 + 2966*Power(t2,2)))) + 
            Power(s2,2)*(1407 + 10*Power(t1,8) - 
               Power(s1,3)*(26 - 317*t1 + 766*Power(t1,2) - 
                  599*Power(t1,3) + 114*Power(t1,4) + 82*Power(t1,5)) - 
               2296*t2 + 1042*Power(t2,2) + 35*Power(t2,3) + 
               Power(t1,7)*(-2 + 69*t2) + 
               Power(t1,6)*(148 - 940*t2 - 21*Power(t2,2)) + 
               Power(t1,4)*(8790 - 17096*t2 + 8559*Power(t2,2) - 
                  97*Power(t2,3)) - 
               Power(t1,3)*(14074 - 29308*t2 + 16773*Power(t2,2) + 
                  48*Power(t2,3)) + 
               Power(t1,5)*(-2396 + 5261*t2 - 1561*Power(t2,2) + 
                  110*Power(t2,3)) - 
               t1*(6416 - 11763*t2 + 6395*Power(t2,2) + 
                  231*Power(t2,3)) + 
               Power(t1,2)*(12565 - 26016*t2 + 15319*Power(t2,2) + 
                  274*Power(t2,3)) + 
               Power(s1,2)*(161 + 339*Power(t1,6) + 52*t2 - 
                  Power(t1,5)*(1451 + 87*t2) - 
                  14*Power(t1,3)*(376 + 195*t2) + 
                  4*Power(t1,4)*(943 + 318*t2) + 
                  7*Power(t1,2)*(623 + 354*t2) - t1*(1687 + 820*t2)) + 
               s1*(-427 - 157*Power(t1,7) + Power(t1,6)*(903 - 225*t2) + 
                  482*t2 - 362*Power(t2,2) + 
                  Power(t1,2)*(6505 - 8291*t2 - 4144*Power(t2,2)) + 
                  Power(t1,4)*(9015 - 8972*t2 - 1652*Power(t2,2)) + 
                  Power(t1,5)*(-3554 + 2393*t2 + 134*Power(t2,2)) + 
                  t1*(53 + 745*t2 + 2089*Power(t2,2)) + 
                  Power(t1,3)*(-12459 + 13507*t2 + 3795*Power(t2,2))))) + 
         Power(s,5)*(2*t1*(-45 + 257*t1 - 452*Power(t1,2) + 
               316*Power(t1,3) - 85*Power(t1,4) + 6*Power(t1,5)) - 
            2*Power(s2,6)*(-95 + s1*(30 - 60*t1) + 95*t1 + 
               57*Power(t1,2) - 40*t2 + 75*t1*t2) + 
            Power(s2,5)*(5*Power(s1,3) + 465*Power(t1,3) + 
               Power(s1,2)*(-82 + 210*t1 - 45*t2) + 
               Power(t1,2)*(734 + 325*t2) + 
               t1*(-1496 + 1210*t2 + 345*Power(t2,2)) - 
               2*(-258 + 397*t2 + 95*Power(t2,2) + 35*Power(t2,3)) + 
               s1*(634 - 80*Power(t1,2) + 294*t2 + 105*Power(t2,2) - 
                  2*t1*(569 + 310*t2))) + 
            Power(s2,4)*(1314 - 524*Power(t1,4) + 
               Power(s1,3)*(-19 + 80*t1) - 2325*t2 + 173*Power(t2,2) - 
               205*Power(t2,3) - 7*Power(t1,3)*(244 + 57*t2) + 
               Power(t1,2)*(4938 - 4787*t2 - 461*Power(t2,2)) + 
               t1*(-4337 + 7250*t2 - 224*Power(t2,2) + 250*Power(t2,3)) + 
               Power(s1,2)*(109 + 101*Power(t1,2) + 55*t2 - 
                  2*t1*(197 + 120*t2)) + 
               s1*(1256 - 247*Power(t1,3) - 171*t2 + 139*Power(t2,2) + 
                  Power(t1,2)*(3903 + 634*t2) + 
                  t1*(-4653 + 254*t2 - 80*Power(t2,2)))) + 
            Power(s2,3)*(2232 + 112*Power(t1,5) + 
               Power(s1,3)*(-69 + 233*t1 - 33*Power(t1,2)) - 2917*t2 + 
               893*Power(t2,2) - 160*Power(t2,3) + 
               Power(t1,4)*(2173 + 144*t2) + 
               Power(t1,3)*(-7789 + 6816*t2 + 136*Power(t2,2)) + 
               Power(t1,2)*(12595 - 17288*t2 + 1710*Power(t2,2) - 
                  474*Power(t2,3)) + 
               t1*(-9357 + 13234*t2 - 3104*Power(t2,2) + 
                  467*Power(t2,3)) + 
               Power(s1,2)*(306 - 637*Power(t1,3) + 233*t2 + 
                  Power(t1,2)*(1214 + 369*t2) - t1*(1433 + 883*t2)) + 
               s1*(802 + 527*Power(t1,4) - 626*t2 - 138*Power(t2,2) + 
                  6*Power(t1,3)*(-824 + 13*t2) + 
                  Power(t1,2)*(9218 - 1666*t2 + 71*Power(t2,2)) + 
                  t1*(-5227 + 2901*t2 + 417*Power(t2,2)))) + 
            Power(s2,2)*(1882 + 78*Power(t1,6) - 
               Power(s1,3)*(46 - 281*t1 + 268*Power(t1,2) + 
                  52*Power(t1,3)) - 2076*t2 + 682*Power(t2,2) - 
               47*Power(t2,3) + Power(t1,5)*(-1247 + 120*t2) + 
               Power(t1,4)*(5937 - 4330*t2 + 30*Power(t2,2)) + 
               Power(t1,2)*(18280 - 22535*t2 + 5623*Power(t2,2) - 
                  402*Power(t2,3)) + 
               2*t1*(-5042 + 6273*t2 - 2067*Power(t2,2) + 
                  96*Power(t2,3)) + 
               Power(t1,3)*(-14807 + 16214*t2 - 1931*Power(t2,2) + 
                  332*Power(t2,3)) + 
               Power(s1,2)*(179 + 444*Power(t1,4) + 122*t2 - 
                  Power(t1,3)*(927 + 182*t2) - t1*(1225 + 897*t2) + 
                  Power(t1,2)*(1761 + 1190*t2)) + 
               s1*(-101 - 406*Power(t1,5) + Power(t1,4)*(3010 - 202*t2) + 
                  98*t2 - 220*Power(t2,2) + 
                  Power(t1,2)*(7640 - 3475*t2 - 1102*Power(t2,2)) + 
                  Power(t1,3)*(-7887 + 1408*t2 + 32*Power(t2,2)) + 
                  t1*(-2221 + 1622*t2 + 1085*Power(t2,2)))) - 
            s2*(-68 + 21*Power(t1,7) + 24*t2 - 30*Power(t2,2) + 
               5*Power(t2,3) + Power(t1,6)*(-272 - 100*s1 + 55*t2) + 
               Power(t1,5)*(1963 + 90*Power(s1,2) + s1*(770 - 50*t2) - 
                  1048*t2 + 15*Power(t2,2)) + 
               Power(t1,3)*(11751 - 76*Power(s1,3) - 12342*t2 + 
                  2707*Power(t2,2) - 148*Power(t2,3) + 
                  Power(s1,2)*(484 + 426*t2) + 
                  s1*(3709 - 641*t2 - 592*Power(t2,2))) - 
               Power(t1,4)*(6671 + 20*Power(s1,3) - 5610*t2 + 
                  652*Power(t2,2) - 65*Power(t2,3) + 
                  8*Power(s1,2)*(28 + 5*t2) + 
                  s1*(2696 - 338*t2 - 50*Power(t2,2))) + 
               t1*(4203 - 100*Power(s1,3) - 4771*t2 + 1682*Power(t2,2) + 
                  25*Power(t2,3) + Power(s1,2)*(444 + 250*t2) - 
                  10*s1*(50 - 43*t2 + 61*Power(t2,2))) + 
               Power(t1,2)*(228*Power(s1,3) - 
                  2*Power(s1,2)*(456 + 353*t2) + 
                  s1*(-1117 + 100*t2 + 1205*Power(t2,2)) + 
                  2*(-5442 + 6173*t2 - 1879*Power(t2,2) + 16*Power(t2,3))))\
)))/(s*(-1 + s1)*(-1 + s2)*s2*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + 
         Power(s2,2))*Power(-s + s2 - t1,2)*(1 - s + s2 - t1)*
       Power(-1 + s + t1,2)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*
       (-s + s1 - t2)) - (8*(Power(s,8)*
          (2*Power(t1,3) - Power(t1,2)*t2 - Power(t2,3)) - 
         (s2 - t1)*(-1 + t1)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,
           2)*(t1*(23 - 2*s1 - 11*t2) + 
            Power(s2,3)*(-2 + 3*s1 + 2*t1 - 3*t2) + 6*(-1 + t2) + 
            Power(t1,2)*(-17 + 5*s1 + 2*t2) - 
            s2*(11 + s1*(-2 + 4*t1 + Power(t1,2)) + t2 - 
               6*t1*(2 + t2) + Power(t1,2)*(1 + 2*t2)) - 
            Power(s2,2)*(1 + s1 + 2*s1*t1 + 2*Power(t1,2) + 2*t2 - 
               t1*(3 + 5*t2))) + 
         Power(s,7)*(9*Power(t1,4) - 
            Power(t1,3)*(2*s1 + 10*s2 + 3*(5 + t2)) - 
            t1*t2*(3 + s2*(-1 + t2) - 2*t2 + 4*Power(t2,2) + 
               s1*(-3 + 3*s2 + t2)) + 
            Power(t1,2)*(-4 - 3*t2 + s1*(3 - 4*s2 + t2) + 
               2*s2*(5 + 4*t2)) + 
            Power(t2,2)*(-3 + 8*t2 + s2*(3 - 3*s1 + 7*t2))) + 
         Power(s,6)*(16*Power(t1,5) - 
            Power(t1,4)*(55 + 9*s1 + 36*s2 + 3*t2) + 
            Power(t1,3)*(39 + 20*Power(s2,2) - 25*t2 + Power(t2,2) + 
               s1*(26 - 5*s2 + 4*t2) + s2*(97 + 25*t2)) - 
            t2*(3*(1 - 7*t2 + 8*Power(t2,2)) + 
               s2*(-3 + s1*(3 - 17*t2) + t2 + 43*Power(t2,2)) + 
               Power(s2,2)*(1 + 3*Power(s1,2) + 16*t2 + 
                  21*Power(t2,2) - 3*s1*(1 + 6*t2))) + 
            t1*(-3 + 25*t2 - 21*Power(t2,2) + 30*Power(t2,3) + 
               Power(s1,2)*s2*(3 - 3*s2 + t2) + 
               Power(s2,2)*(11 + 4*t2 + 6*Power(t2,2)) + 
               s2*(-2 - 20*t2 + 15*Power(t2,2) + 24*Power(t2,3)) + 
               s1*(3 - 20*t2 + 3*Power(t2,2) + 
                  Power(s2,2)*(-2 + 13*t2) + 
                  s2*(-3 + 5*t2 - 7*Power(t2,2)))) + 
            Power(t1,2)*(21 + Power(s1,2)*(-1 + 5*s2) + 19*t2 + 
               4*Power(t2,2) - 6*Power(t2,3) - 
               Power(s2,2)*(37 + 25*t2) + 
               s2*(-43 + 14*t2 - 4*Power(t2,2)) + 
               s1*(17*Power(s2,2) - 7*s2*(1 + 3*t2) - 
                  4*(5 - 4*t2 + Power(t2,2))))) + 
         Power(s,4)*(5 - 2*(26 + 7*s1)*Power(t1,6) + 6*Power(t1,7) - 
            33*t2 + 39*Power(t2,2) - 3*Power(t2,3) + 
            Power(t1,5)*(128 - 55*t2 + 3*Power(t2,2) + 7*s1*(12 + t2)) + 
            t1*(-39 + 74*t2 - 87*Power(t2,2) + 69*Power(t2,3) + 
               s1*(19 + 16*t2 - 51*Power(t2,2))) - 
            Power(t1,4)*(209 + 10*Power(s1,2) - 205*t2 + 
               15*Power(t2,2) + Power(t2,3) + 
               s1*(211 - 46*t2 + 4*Power(t2,2))) + 
            Power(t1,3)*(122 - 2*Power(s1,3) - 
               4*Power(s1,2)*(-15 + t2) - 30*t2 + 13*Power(t2,2) + 
               24*Power(t2,3) + 2*s1*(85 - 134*t2 + 8*Power(t2,2))) + 
            Power(t1,2)*(22 - 181*t2 + 54*Power(t2,2) - 
               87*Power(t2,3) + 2*Power(s1,2)*(-22 + 5*t2) + 
               2*s1*(3 + 91*t2 + 16*Power(t2,2))) + 
            Power(s2,4)*(-8 + 4*Power(s1,3) + 10*Power(t1,3) - 18*t2 - 
               40*Power(t2,2) - 35*Power(t2,3) - 
               Power(s1,2)*(1 + 14*t1 + 30*t2) - 
               Power(t1,2)*(31 + 35*t2) + 
               t1*(29 + 44*t2 + 20*Power(t2,2)) + 
               s1*(5 + 22*Power(t1,2) + 26*t2 + 60*Power(t2,2) + 
                  2*t1*(-9 + 5*t2))) - 
            Power(s2,3)*(5 + 36*Power(t1,4) + 
               2*Power(s1,3)*(-2 + 5*t1) - 25*t2 + 89*Power(t2,2) + 
               110*Power(t2,3) - Power(t1,3)*(161 + 90*t2) + 
               Power(t1,2)*(200 + 97*t2 + 40*Power(t2,2)) - 
               t1*(81 - 6*t2 + 166*Power(t2,2) + 80*Power(t2,3)) + 
               Power(s1,2)*(-3 - 46*Power(t1,2) + 48*t2 - 
                  6*t1*(-4 + 9*t2)) + 
               s1*(40*Power(t1,3) + 5*Power(t1,2)*(-3 + 14*t2) + 
                  5*t1*(-5 + t2 + 22*Power(t2,2)) - 
                  6*(-2 + 3*t2 + 23*Power(t2,2)))) + 
            Power(s2,2)*(-7 + 48*Power(t1,5) + 
               4*Power(s1,3)*t1*(-3 + 2*t1) + 63*t2 - 19*Power(t2,2) - 
               148*Power(t2,3) - 3*Power(t1,4)*(97 + 25*t2) + 
               2*Power(t1,3)*(261 + 7*t2 + 15*Power(t2,2)) - 
               Power(t1,2)*(341 - 277*t2 + 233*Power(t2,2) + 
                  60*Power(t2,3)) + 
               t1*(82 - 216*t2 + 335*Power(t2,2) + 247*Power(t2,3)) + 
               Power(s1,2)*(-50*Power(t1,3) + 
                  Power(t1,2)*(74 - 32*t2) - 18*t2 + 5*t1*(5 + 18*t2)) \
+ s1*(1 - 17*t2 + 103*Power(t2,2) + Power(t1,3)*(96 + 107*t2) + 
                  t1*(73 - 139*t2 - 224*Power(t2,2)) + 
                  Power(t1,2)*(-235 - 106*t2 + 48*Power(t2,2)))) + 
            s2*(6 - 2*Power(s1,3)*(-5 + t1)*Power(t1,2) - 
               28*Power(t1,6) - 43*t2 + 127*Power(t2,2) - 
               85*Power(t2,3) + Power(t1,5)*(211 + 20*t2) + 
               Power(t1,4)*(-470 + 103*t2 - 13*Power(t2,2)) + 
               Power(t1,3)*(467 - 481*t2 + 113*Power(t2,2) + 
                  16*Power(t2,3)) - 
               Power(t1,2)*(167 - 293*t2 + 261*Power(t2,2) + 
                  151*Power(t2,3)) + 
               t1*(-12 + 42*t2 + 25*Power(t2,2) + 244*Power(t2,3)) + 
               Power(s1,2)*t1*
                (18 + 18*Power(t1,3) + 38*t2 + 
                  Power(t1,2)*(-43 + 8*t2) - 2*t1*(31 + 24*t2)) + 
               s1*(-1 + 32*Power(t1,5) - 16*t2 + 17*Power(t2,2) - 
                  Power(t1,4)*(185 + 54*t2) + 
                  t1*(44 - 238*t2 - 103*Power(t2,2)) + 
                  Power(t1,3)*(441 + 44*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*(-261 + 361*t2 + 48*Power(t2,2))))) - 
         Power(s,5)*(1 - 14*Power(t1,6) - 18*t2 + 51*Power(t2,2) - 
            30*Power(t2,3) + Power(t1,5)*(77 + 16*s1 + t2) - 
            Power(t1,4)*(126 - 58*t2 + 3*Power(t2,2) + 7*s1*(10 + t2)) + 
            Power(t1,2)*(41 - 37*t2 + 29*Power(t2,2) - 41*Power(t2,3) + 
               Power(s1,2)*(-13 + 2*t2) + 
               s1*(-40 + 113*t2 - 12*Power(t2,2))) + 
            t1*(-20 + 74*t2 - 77*Power(t2,2) + 78*Power(t2,3) + 
               s1*(16 - 37*t2 - 9*Power(t2,2))) + 
            Power(t1,3)*(64 + 5*Power(s1,2) - 126*t2 + 4*Power(t2,2) + 
               4*Power(t2,3) + 2*s1*(58 - 19*t2 + 3*Power(t2,2))) + 
            Power(s2,3)*(-4 + Power(s1,3) + 20*Power(t1,3) - 8*t2 - 
               35*Power(t2,2) - 35*Power(t2,3) - 
               Power(s1,2)*(11*t1 + 15*t2) - Power(t1,2)*(51 + 40*t2) + 
               t1*(31 + 26*t2 + 15*Power(t2,2)) + 
               s1*(1 + 28*Power(t1,2) + 14*t2 + 45*Power(t2,2) + 
                  10*t1*(-1 + 2*t2))) + 
            s2*(48*Power(t1,5) - Power(t1,4)*(224 + 14*s1 + 31*t2) + 
               t2*(-1 + 52*t2 - 95*Power(t2,2) + s1*(-13 + 31*t2)) + 
               Power(t1,3)*(293 - 16*Power(s1,2) - 69*t2 + 
                  10*Power(t2,2) + s1*(81 + 49*t2)) + 
               t1*(-5 - 49*t2 + 50*Power(t2,2) + 136*Power(t2,3) + 
                  13*Power(s1,2)*(1 + t2) + 
                  s1*(1 - 64*t2 - 52*Power(t2,2))) + 
               Power(t1,2)*(-86 + Power(s1,3) + 
                  Power(s1,2)*(12 - 5*t2) + 198*t2 - 59*Power(t2,2) - 
                  30*Power(t2,3) + s1*(-110 - 23*t2 + 2*Power(t2,2)))) - 
            Power(s2,2)*(-1 + 54*Power(t1,4) + 
               5*Power(t1,3)*(-39 + 6*s1 - 14*t2) + 
               (-23 + 4*s1 + 13*Power(s1,2))*t2 + 
               (40 - 77*s1)*Power(t2,2) + 95*Power(t2,3) + 
               Power(t1,2)*(172 + s1 - 27*Power(s1,2) + 14*t2 + 
                  65*s1*t2 + 20*Power(t2,2)) + 
               t1*(-37 + 2*Power(s1,3) + Power(s1,2)*(5 - 16*t2) + 
                  57*t2 - 89*Power(t2,2) - 60*Power(t2,3) + 
                  s1*(-8 - 25*t2 + 50*Power(t2,2))))) + 
         s*(2*(3 + s1)*Power(t1,8) - 
            Power(s2,7)*Power(s1 - t2,2)*(-1 + s1 + t1 - t2) - 
            8*Power(-1 + t2,3) + 
            t1*Power(-1 + t2,2)*(-67 + 16*s1 + 49*t2) - 
            2*Power(t1,7)*(12 + s1 + 2*t2 + s1*t2) + 
            Power(t1,6)*(66 - 2*Power(s1,3) + 39*t2 + 6*Power(t2,2) + 
               Power(s1,2)*(14 + t2) - 4*s1*(2 + 7*t2)) - 
            Power(t1,2)*(-1 + t2)*
             (210 + 4*Power(s1,2) - 286*t2 + 78*Power(t2,2) + 
               s1*(-103 + 101*t2)) + 
            Power(s2,6)*(s1 - t2)*
             (-1 + Power(s1,2)*(-3 + 4*t1) - 5*t2 - 3*Power(t2,2) - 
               Power(t1,2)*(1 + 4*t2) + 
               s1*(6 - 11*t1 + 5*Power(t1,2) + 6*t2 - 8*t1*t2) + 
               t1*(2 + 9*t2 + 4*Power(t2,2))) + 
            Power(t1,5)*(21*Power(s1,3) + 2*Power(s1,2)*(-77 + 4*t2) - 
               3*(57 + 43*t2 + 8*Power(t2,2)) + 
               s1*(198 + 89*t2 + 10*Power(t2,2))) - 
            Power(t1,4)*(-310 + 21*Power(s1,3) + 45*t2 - 
               91*Power(t2,2) + 7*Power(t2,3) + 
               Power(s1,2)*(-225 + 56*t2) + 
               s1*(445 - 144*t2 + 68*Power(t2,2))) + 
            Power(t1,3)*(-338 + 4*Power(s1,3) + 476*t2 - 
               296*Power(t2,2) + 42*Power(t2,3) + 
               Power(s1,2)*(-89 + 45*t2) + 
               s1*(342 - 375*t2 + 149*Power(t2,2))) + 
            s2*(Power(s1,3)*Power(t1,2)*
                (-12 + 66*t1 - 70*Power(t1,2) + 7*Power(t1,3) + 
                  Power(t1,4)) - 2*Power(t1,7)*(11 + t2) - 
               Power(-1 + t2,2)*(-29 + 11*t2) + 
               Power(t1,6)*(95 + 16*t2 + 2*Power(t2,2)) - 
               Power(t1,5)*(207 + 166*t2 + 51*Power(t2,2)) + 
               Power(t1,4)*(326 + 609*t2 + 163*Power(t2,2) - 
                  Power(t2,3)) - 
               2*t1*(71 - 139*t2 + 67*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(-385 - 634*t2 - 85*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,2)*(306 - 32*t2 + 54*Power(t2,2) + 
                  20*Power(t2,3)) - 
               Power(s1,2)*t1*
                (8 + Power(t1,2)*(596 - 58*t2) - 8*t2 + 
                  Power(t1,5)*(4 + t2) + Power(t1,4)*(42 + 20*t2) - 
                  Power(t1,3)*(429 + 68*t2) + t1*(-221 + 89*t2)) + 
               s1*(-8*Power(t1,7) - 16*Power(-1 + t2,2) + 
                  Power(t1,6)*(16 + 19*t2) + 
                  Power(t1,3)*(759 + 476*t2 - 69*Power(t2,2)) + 
                  Power(t1,2)*(-491 + 203*t2 - 60*Power(t2,2)) + 
                  Power(t1,5)*(17 + 104*t2 - 2*Power(t2,2)) + 
                  Power(t1,4)*(-381 - 630*t2 + 23*Power(t2,2)) + 
                  4*t1*(26 - 51*t2 + 25*Power(t2,2)))) + 
            Power(s2,2)*(Power(s1,3)*t1*
                (12 - 72*t1 + 92*Power(t1,2) - 19*Power(t1,3) - 
                  4*Power(t1,4)) + Power(t1,6)*(34 + 11*t2) - 
               Power(t1,5)*(154 + 53*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(178 + 708*t2 + 356*Power(t2,2) - 
                  30*Power(t2,3)) + 
               2*(2 + t2 - 7*Power(t2,2) + 4*Power(t2,3)) + 
               2*t1*(-25 - 141*t2 - 14*Power(t2,2) + 6*Power(t2,3)) - 
               Power(t1,3)*(308 + 609*t2 + 487*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,4)*(296 + 223*t2 + 185*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(s1,2)*(4 + Power(t1,6) - 4*t2 + 
                  50*Power(t1,2)*(11 + t2) + Power(t1,5)*(9 + 4*t2) + 
                  t1*(-175 + 43*t2) - 3*Power(t1,3)*(161 + 61*t2) + 
                  Power(t1,4)*(94 + 63*t2)) + 
               s1*(-1 - Power(t1,6)*(-11 + t2) + Power(t2,2) - 
                  Power(t1,5)*(5 + 49*t2) + 
                  t1*(174 + 283*t2 - 109*Power(t2,2)) + 
                  Power(t1,3)*(295 + 974*t2 + 11*Power(t2,2)) - 
                  Power(t1,4)*(76 + 173*t2 + 24*Power(t2,2)) + 
                  2*Power(t1,2)*(-199 - 517*t2 + 74*Power(t2,2)))) + 
            Power(s2,5)*(Power(s1,3)*(-8 + 19*t1 - 5*Power(t1,2)) + 
               Power(s1,2)*(18 - 10*Power(t1,3) + 33*t2 + 
                  Power(t1,2)*(30 + 17*t2) - 2*t1*(19 + 34*t2)) + 
               t2*(13 + 34*t2 + 16*Power(t2,2) - 
                  6*Power(t1,3)*(2 + t2) + 
                  Power(t1,2)*(37 + 38*t2 + 6*Power(t2,2)) - 
                  2*t1*(19 + 33*t2 + 14*Power(t2,2))) + 
               s1*(-5 - 52*t2 - 41*Power(t2,2) + 
                  4*Power(t1,3)*(1 + 4*t2) - 
                  Power(t1,2)*(13 + 68*t2 + 18*Power(t2,2)) + 
                  t1*(14 + 104*t2 + 77*Power(t2,2)))) + 
            Power(s2,3)*(14 + 
               Power(s1,3)*(-4 + 30*t1 - 66*Power(t1,2) + 
                  38*Power(t1,3) + 5*Power(t1,4)) + 42*t2 + 
               66*Power(t2,2) - 6*Power(t2,3) - 
               Power(t1,5)*(26 + 26*t2 + Power(t2,2)) + 
               Power(t1,4)*(115 + 115*t2 + 37*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-79 - 134*t2 - 307*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(179 + 205*t2 + 474*Power(t2,2) + 
                  30*Power(t2,3)) - 
               Power(t1,3)*(203 + 202*t2 + 269*Power(t2,2) + 
                  34*Power(t2,3)) + 
               Power(s1,2)*(43 - 5*Power(t1,5) + 
                  Power(t1,4)*(5 - 3*t2) + t2 - 
                  24*Power(t1,3)*(6 + 5*t2) - 2*t1*(106 + 25*t2) + 
                  Power(t1,2)*(313 + 163*t2)) + 
               s1*(-25 - 111*t2 + 20*Power(t2,2) + 
                  Power(t1,5)*(-4 + 6*t2) + 
                  t1*(97 + 468*t2 - 19*Power(t2,2)) - 
                  3*Power(t1,4)*(13 - 5*t2 + Power(t2,2)) - 
                  2*Power(t1,2)*(83 + 311*t2 + 47*Power(t2,2)) + 
                  Power(t1,3)*(137 + 244*t2 + 105*Power(t2,2)))) + 
            Power(s2,4)*(8 + Power(s1,3)*(-3 + 31*t1 - 40*Power(t1,2)) + 
               9*t2 + 41*Power(t2,2) - 3*Power(t2,3) + 
               4*Power(t1,4)*(2 + 7*t2 + Power(t2,2)) - 
               Power(t1,3)*(32 + 109*t2 + 57*Power(t2,2) + 
                  4*Power(t2,3)) - 
               t1*(32 + 71*t2 + 178*Power(t2,2) + 33*Power(t2,3)) + 
               Power(t1,2)*(48 + 143*t2 + 190*Power(t2,2) + 
                  52*Power(t2,3)) + 
               Power(s1,2)*(33 + 10*Power(t1,4) - 2*t2 - 
                  2*Power(t1,3)*(15 + 4*t2) + 
                  5*Power(t1,2)*(22 + 27*t2) - t1*(123 + 89*t2)) + 
               s1*(-13 - 54*t2 + 8*Power(t2,2) - 
                  2*Power(t1,4)*(2 + 7*t2) + 
                  Power(t1,3)*(41 + 67*t2 + 12*Power(t2,2)) + 
                  t1*(59 + 241*t2 + 91*Power(t2,2)) - 
                  Power(t1,2)*(83 + 240*t2 + 147*Power(t2,2))))) + 
         Power(s,3)*(-6*(3 + s1)*Power(t1,7) + Power(t1,8) + 
            6*(-1 + t2 + 5*Power(t2,2) - 5*Power(t2,3)) + 
            Power(t1,6)*(60 - 21*t2 + Power(t2,2) + s1*(49 + 4*t2)) + 
            Power(t1,2)*(92 + Power(s1,2)*(52 - 18*t2) + 6*t2 + 
               84*Power(t2,2) + 37*Power(t2,3) + 
               s1*(-136 + 79*t2 - 157*Power(t2,2))) - 
            Power(t1,5)*(183 + 9*Power(s1,2) - 124*t2 + 
               12*Power(t2,2) + s1*(157 - 25*t2 + Power(t2,2))) + 
            Power(t1,4)*(313 - 6*Power(s1,3) - Power(s1,2)*(-93 + t2) - 
               9*t2 + 45*Power(t2,2) + 5*Power(t2,3) + 
               s1*(205 - 298*t2 + 8*Power(t2,2))) + 
            t1*(-1 + 80*t2 - 94*Power(t2,2) + 33*Power(t2,3) + 
               4*s1*(6 - 29*t2 + 21*Power(t2,2))) + 
            Power(t1,3)*(-258 + 10*Power(s1,3) - 265*t2 - 
               63*Power(t2,2) - 36*Power(t2,3) + 
               13*Power(s1,2)*(-15 + 2*t2) + 
               2*s1*(50 + 187*t2 + 23*Power(t2,2))) + 
            Power(s2,5)*(4 - 6*Power(s1,3) - 2*Power(t1,3) + 16*t2 + 
               25*Power(t2,2) + 21*Power(t2,3) + 
               Power(t1,2)*(7 + 16*t2) + 
               Power(s1,2)*(3 + 6*t1 + 30*t2) - 
               t1*(9 + 31*t2 + 15*Power(t2,2)) - 
               s1*(7 + 8*Power(t1,2) + 24*t2 + 45*Power(t2,2) - 
                  t1*(14 + 5*t2))) + 
            s2*(-16 - 6*Power(t1,7) + 2*(29 + 6*s1)*t2 - 
               (77 + 4*s1)*Power(t2,2) + 17*Power(t2,3) + 
               Power(t1,6)*(90 + 22*s1 + 7*t2) + 
               Power(t1,5)*(-294 + 8*Power(s1,2) + 50*t2 - 
                  7*Power(t2,2) - 3*s1*(53 + 10*t2)) + 
               Power(t1,2)*(235 - 30*Power(s1,3) + 267*t2 + 
                  453*Power(t2,2) + 200*Power(t2,3) + 
                  16*Power(s1,2)*(16 + 7*t2) + 
                  s1*(193 - 1072*t2 - 104*Power(t2,2))) + 
               Power(t1,4)*(568 + 4*Power(s1,2)*(-10 + t2) - 383*t2 + 
                  94*Power(t2,2) + 3*Power(t2,3) + 
                  s1*(504 + 58*t2 + 5*Power(t2,2))) + 
               Power(t1,3)*(-591 + 29*Power(s1,3) + 280*t2 - 
                  428*Power(t2,2) - 68*Power(t2,3) - 
                  3*Power(s1,2)*(54 + 25*t2) + 
                  s1*(-584 + 667*t2 + 10*Power(t2,2))) + 
               t1*(14 - 206*t2 + 20*Power(t2,2) - 138*Power(t2,3) - 
                  4*Power(s1,2)*(4 + 7*t2) + 
                  s1*(-49 + 264*t2 + 67*Power(t2,2)))) + 
            Power(s2,3)*(15 - 16*Power(t1,5) - 
               3*Power(s1,3)*(2 - 13*t1 + 6*Power(t1,2)) - 47*t2 + 
               138*Power(t2,2) + 117*Power(t2,3) + 
               Power(t1,4)*(137 + 69*t2) - 
               Power(t1,3)*(321 + 147*t2 + 40*Power(t2,2)) + 
               Power(t1,2)*(300 + 15*t2 + 307*Power(t2,2) + 
                  60*Power(t2,3)) - 
               t1*(115 - 84*t2 + 494*Power(t2,2) + 228*Power(t2,3)) + 
               Power(s1,2)*(6 + 42*Power(t1,3) + 50*t2 + 
                  Power(t1,2)*(-83 + 66*t2) - t1*(28 + 191*t2)) - 
               s1*(-26 + 6*Power(t1,4) + 55*t2 + 136*Power(t2,2) + 
                  Power(t1,3)*(45 + 67*t2) + 
                  t1*(131 - 255*t2 - 339*Power(t2,2)) + 
                  Power(t1,2)*(-182 - 19*t2 + 92*Power(t2,2)))) + 
            Power(s2,4)*(7 + 9*Power(t1,4) + 
               Power(s1,3)*(-11 + 18*t1) + 6*t2 + 82*Power(t2,2) + 
               70*Power(t2,3) - 5*Power(t1,3)*(10 + 11*t2) + 
               Power(s1,2)*(-26*Power(t1,2) + t1*(10 - 76*t2) + 
                  66*t2) + 8*Power(t1,2)*(10 + 14*t2 + 5*Power(t2,2)) - 
               2*t1*(23 + 33*t2 + 72*Power(t2,2) + 30*Power(t2,3)) + 
               s1*(12 + 20*Power(t1,3) - 47*t2 - 122*Power(t2,2) + 
                  3*Power(t1,2)*(-7 + 5*t2) + 
                  t1*(-8 + 70*t2 + 115*Power(t2,2)))) + 
            Power(s2,2)*(16 + 14*Power(t1,6) + 
               Power(s1,3)*t1*(26 - 51*t1 + 6*Power(t1,2)) - 22*t2 + 
               12*Power(t2,2) + 85*Power(t2,3) - 
               Power(t1,5)*(166 + 37*t2) + 
               Power(t1,4)*(488 + 33*t2 + 21*Power(t2,2)) - 
               Power(t1,3)*(670 - 314*t2 + 263*Power(t2,2) + 
                  24*Power(t2,3)) + 
               Power(t1,2)*(474 - 433*t2 + 785*Power(t2,2) + 
                  215*Power(t2,3)) - 
               t1*(156 - 179*t2 + 500*Power(t2,2) + 279*Power(t2,3)) + 
               Power(s1,2)*(-30*Power(t1,4) + 
                  Power(t1,3)*(119 - 24*t2) + 10*t2 - 
                  17*t1*(7 + 8*t2) + Power(t1,2)*(113 + 185*t2)) + 
               s1*(9 - 22*Power(t1,5) - 45*t2 - 32*Power(t2,2) + 
                  Power(t1,4)*(162 + 73*t2) + 
                  Power(t1,2)*(430 - 519*t2 - 225*Power(t2,2)) + 
                  2*Power(t1,3)*(-249 - 82*t2 + 9*Power(t2,2)) + 
                  t1*(-115 + 517*t2 + 226*Power(t2,2))))) + 
         Power(s,2)*(-((3 + s1)*Power(t1,8)) + 
            4*Power(-1 + t2,2)*(-1 + 7*t2) - 
            Power(t1,6)*(83 + 3*Power(s1,2) - 2*s1*(-22 + t2) - 19*t2 + 
               3*Power(t2,2)) + Power(t1,7)*(21 - 2*t2 + s1*(14 + t2)) - 
            t1*(-1 + t2)*(78 - 177*t2 + 93*Power(t2,2) + 
               s1*(-52 + 60*t2)) + 
            Power(t1,5)*(225 - 6*Power(s1,3) + 70*t2 + 27*Power(t2,2) + 
               2*Power(s1,2)*(30 + t2) + s1*(64 - 151*t2 + Power(t2,2))) \
+ Power(t1,4)*(-376 + 26*Power(s1,3) - 296*t2 - 83*Power(t2,2) - 
               3*Power(t2,3) + Power(s1,2)*(-278 + 22*t2) + 
               s1*(245 + 320*t2 + 33*Power(t2,2))) - 
            Power(t1,3)*(-392 + 14*Power(s1,3) + 60*t2 - 
               211*Power(t2,2) + 9*Power(t2,3) + 
               Power(s1,2)*(-217 + 51*t2) + 
               s1*(429 - 119*t2 + 170*Power(t2,2))) + 
            Power(t1,2)*(-250 + 488*t2 - 384*Power(t2,2) + 
               76*Power(t2,3) + 2*Power(s1,2)*(-9 + 7*t2) + 
               s1*(203 - 359*t2 + 204*Power(t2,2))) + 
            Power(s2,6)*(4*Power(s1,3) + Power(s1,2)*(-3 + t1 - 15*t2) - 
               t2*(5 + 3*Power(t1,2) + 8*t2 + 7*Power(t2,2) - 
                  2*t1*(4 + 3*t2)) + 
               s1*(3 + Power(t1,2) + 11*t2 + 18*Power(t2,2) - 
                  t1*(4 + 7*t2))) + 
            s2*(-5 + Power(s1,3)*Power(t1,2)*
                (34 - 81*t1 + 27*Power(t1,2) + 2*Power(t1,3)) + 30*t2 - 
               39*Power(t2,2) + 14*Power(t2,3) + Power(t1,7)*(16 + t2) + 
               Power(t1,6)*(-96 + t2 - Power(t2,2)) + 
               Power(t1,5)*(301 - 69*t2 + 30*Power(t2,2)) + 
               t1*(59 + 81*Power(t2,2)) - 
               Power(t1,4)*(549 + 148*t2 + 262*Power(t2,2) + 
                  10*Power(t2,3)) - 
               Power(t1,2)*(274 + 620*t2 + 293*Power(t2,2) + 
                  41*Power(t2,3)) + 
               Power(t1,3)*(548 + 805*t2 + 543*Power(t2,2) + 
                  50*Power(t2,3)) + 
               Power(s1,2)*t1*
                (20 + Power(t1,5) - 12*t2 - Power(t1,4)*(16 + t2) - 
                  2*t1*(191 + 3*t2) - 2*Power(t1,3)*(77 + 29*t2) + 
                  2*Power(t1,2)*(295 + 63*t2)) + 
               s1*(5*Power(t1,7) - 7*Power(t1,6)*(8 + t2) + 
                  t1*(-61 + 28*t2 - 63*Power(t2,2)) + 
                  Power(t1,4)*(-287 + 466*t2 - 5*Power(t2,2)) + 
                  Power(t1,5)*(193 + 51*t2 + Power(t2,2)) + 
                  4*(3 - 8*t2 + 5*Power(t2,2)) - 
                  Power(t1,3)*(94 + 1413*t2 + 16*Power(t2,2)) + 
                  Power(t1,2)*(288 + 789*t2 + 19*Power(t2,2)))) + 
            Power(s2,4)*(-8 + 
               Power(s1,3)*(11 - 43*t1 + 16*Power(t1,2)) - 23*t2 - 
               120*Power(t2,2) - 53*Power(t2,3) - 
               Power(t1,4)*(15 + 22*t2) + 
               Power(t1,3)*(55 + 101*t2 + 25*Power(t2,2)) - 
               Power(t1,2)*(73 + 150*t2 + 173*Power(t2,2) + 
                  30*Power(t2,3)) + 
               t1*(41 + 94*t2 + 292*Power(t2,2) + 112*Power(t2,3)) + 
               Power(s1,2)*(-22 + 2*Power(t1,3) - 56*t2 - 
                  2*Power(t1,2)*(3 + 28*t2) + t1*(50 + 173*t2)) + 
               s1*(-10 + Power(t1,4) + 107*t2 - 11*Power(t1,3)*t2 + 
                  96*Power(t2,2) + t1*(31 - 256*t2 - 238*Power(t2,2)) + 
                  2*Power(t1,2)*(-11 + 56*t2 + 34*Power(t2,2)))) + 
            Power(s2,5)*(-1 - 2*Power(s1,3)*(-5 + 7*t1) - 12*t2 - 
               34*Power(t2,2) - 23*Power(t2,3) + 
               Power(t1,3)*(2 + 13*t2) - 
               Power(t1,2)*(5 + 41*t2 + 20*Power(t2,2)) + 
               t1*(4 + 40*t2 + 59*Power(t2,2) + 24*Power(t2,3)) + 
               Power(s1,2)*(-9 - 3*Power(t1,2) - 40*t2 + 
                  t1*(17 + 49*t2)) + 
               s1*(1 - 3*Power(t1,3) + 39*t2 + 53*Power(t2,2) + 
                  Power(t1,2)*(10 + 19*t2) - 
                  t1*(8 + 68*t2 + 59*Power(t2,2)))) - 
            Power(s2,2)*(3 + Power(s1,3)*t1*
                (26 - 95*t1 + 59*Power(t1,2) + 4*Power(t1,3)) + 32*t2 + 
               27*Power(t2,2) + 8*Power(t2,3) + 
               Power(t1,6)*(34 + 7*t2) - 
               Power(t1,5)*(182 + 39*t2 + 5*Power(t2,2)) + 
               3*Power(t1,4)*
                (152 - 6*t2 + 38*Power(t2,2) + Power(t2,3)) - 
               t1*(117 + 236*t2 + 430*Power(t2,2) + 71*Power(t2,3)) - 
               Power(t1,3)*(613 + 13*t2 + 674*Power(t2,2) + 
                  71*Power(t2,3)) + 
               Power(t1,2)*(419 + 267*t2 + 992*Power(t2,2) + 
                  144*Power(t2,3)) + 
               Power(s1,2)*(3*Power(t1,5) + Power(t1,4)*(-60 + t2) + 
                  2*(1 + t2) - t1*(209 + 69*t2) - 
                  Power(t1,3)*(200 + 163*t2) + 
                  Power(t1,2)*(488 + 254*t2)) + 
               s1*(22 + 9*Power(t1,6) - 91*t2 + 21*Power(t2,2) - 
                  Power(t1,5)*(78 + 17*t2) + 
                  Power(t1,2)*(114 - 1419*t2 - 140*Power(t2,2)) + 
                  t1*(-14 + 777*t2 - 11*Power(t2,2)) + 
                  Power(t1,4)*(247 + 135*t2 - 2*Power(t2,2)) + 
                  Power(t1,3)*(-300 + 567*t2 + 100*Power(t2,2)))) + 
            Power(s2,3)*(-29 + 
               Power(s1,3)*(6 - 51*t1 + 71*Power(t1,2) - 4*Power(t1,3)) + 
               22*t2 - 122*Power(t2,2) - 31*Power(t2,3) + 
               2*Power(t1,5)*(17 + 9*t2) - 
               Power(t1,4)*(157 + 106*t2 + 15*Power(t2,2)) + 
               Power(t1,3)*(305 + 151*t2 + 207*Power(t2,2) + 
                  16*Power(t2,3)) + 
               t1*(151 - 19*t2 + 564*Power(t2,2) + 142*Power(t2,3)) - 
               Power(t1,2)*(304 + 66*t2 + 674*Power(t2,2) + 
                  149*Power(t2,3)) + 
               Power(s1,2)*(2*Power(t1,4) - 4*(11 + 3*t2) + 
                  18*t1*(11 + 9*t2) + Power(t1,3)*(-49 + 24*t2) - 
                  3*Power(t1,2)*(49 + 80*t2)) + 
               s1*(3 + 6*Power(t1,5) + 117*t2 + 16*Power(t2,2) - 
                  6*Power(t1,4)*(7 + 2*t2) + 
                  t1*(21 - 529*t2 - 205*Power(t2,2)) + 
                  Power(t1,3)*(125 + 27*t2 - 30*Power(t2,2)) + 
                  Power(t1,2)*(-113 + 477*t2 + 285*Power(t2,2))))))*
       B1(s,1 - s + s2 - t1,s2))/
     (s*(-1 + s1)*(-1 + s2)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*
       (-s + s1 - t2)) + (8*(2*Power(-1 + s,3)*s*(1 - 3*s + Power(s,2))*
          Power(t1,2)*(-2 + s + t1)*Power(-1 + s + t1,4) + 
         Power(s2,10)*(s1 - t2)*
          (-(Power(-1 + t1,2)*(-2 + 3*s1 + 2*t1 - 3*t2)*(s1 - t2)) + 
            2*Power(s,2)*(-3 + Power(s1,2) - 3*Power(t1,2) + 
               s1*(-1 + t1 - 2*t2) - t1*(-6 + t2) + t2 + Power(t2,2)) + 
            s*(-1 + t1)*(6 + 5*Power(s1,2) + 6*Power(t1,2) + 
               2*s1*(-9 + 9*t1 - 5*t2) + 18*t2 + 5*Power(t2,2) - 
               6*t1*(2 + 3*t2))) - 
         (-1 + s)*s*s2*t1*Power(-1 + s + t1,3)*
          (8 - 4*Power(t1,4) + 2*Power(t1,2)*(-60 + 11*s1 - 3*t2) + 
            2*Power(s,6)*t1*(2 + t1 - t2) + t1*(66 - 16*s1 + 4*t2) + 
            Power(t1,3)*(50 - 8*s1 + 4*t2) + 
            Power(s,4)*(28 - 2*(1 + s1)*Power(t1,3) + 
               t1*(134 - 66*s1 - 3*t2) + 
               Power(t1,2)*(-57 + 26*s1 + 2*t2)) + 
            2*Power(s,5)*(-2 + Power(t1,3) - Power(t1,2)*(s1 + t2) + 
               t1*(-17 + 5*s1 + 4*t2)) + 
            Power(s,3)*(-72 + t1*(-325 + 166*s1 - 29*t2) + 
               2*Power(t1,3)*(-15 + 7*s1 - 2*t2) + 
               Power(t1,2)*(247 - 101*s1 + 16*t2)) + 
            s*(-44 + 8*Power(t1,4) + 3*t1*(-99 + 32*s1 - 9*t2) + 
               2*Power(t1,3)*(-74 + 15*s1 - 6*t2) + 
               Power(t1,2)*(445 - 105*s1 + 26*t2)) + 
            Power(s,2)*(84 - 4*Power(t1,4) + 
               Power(t1,2)*(-497 + 160*s1 - 36*t2) + 
               t1*(452 - 190*s1 + 49*t2) + 
               Power(t1,3)*(-34*s1 + 12*(9 + t2)))) + 
         Power(s2,9)*(2*Power(s,4)*Power(-1 + t1,2) - 
            Power(s,3)*(10 + 10*Power(s1,3) + 2*Power(t1,3) + 
               4*Power(s1,2)*(-2 + t1 - 9*t2) + 21*t2 - 
               18*Power(t2,2) - 16*Power(t2,3) + 
               Power(t1,2)*(6 + 37*t2) + 
               2*t1*(-9 - 29*t2 + 7*Power(t2,2)) + 
               s1*(-25 - 41*Power(t1,2) + t1*(66 - 18*t2) + 26*t2 + 
                  42*Power(t2,2))) + 
            Power(-1 + t1,2)*(s1 - t2)*
             (-4 + 2*Power(s1,2)*(5 + 4*t1) + 3*t2 + 13*Power(t2,2) - 
               2*Power(t1,2)*(2 + t2) + t1*(8 - t2 + 5*Power(t2,2)) + 
               s1*(1 + 6*Power(t1,2) - 23*t2 - t1*(7 + 13*t2))) + 
            s*(-1 + t1)*(-8 + Power(s1,3)*(-39 + 20*t1) + 6*t2 + 
               77*Power(t2,2) + 27*Power(t2,3) + 
               4*Power(t1,3)*(2 + 3*t2) - 
               Power(t1,2)*(24 + 18*t2 + 31*Power(t2,2)) + 
               t1*(24 - 46*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s1,2)*(95 - 13*Power(t1,2) + 105*t2 - 
                  2*t1*(41 + 24*t2)) + 
               s1*(-10 - 8*Power(t1,3) - 172*t2 - 93*Power(t2,2) + 
                  Power(t1,2)*(6 + 44*t2) + 
                  4*t1*(3 + 32*t2 + 9*Power(t2,2)))) - 
            Power(s,2)*(12 + 4*Power(t1,4) + Power(s1,3)*(-15 + 28*t1) + 
               103*t2 + 106*Power(t2,2) + 30*Power(t2,3) - 
               Power(t1,3)*(24 + 29*t2) + 
               Power(t1,2)*(48 + 161*t2 + 131*Power(t2,2)) - 
               t1*(40 + 235*t2 + 237*Power(t2,2) + 43*Power(t2,3)) + 
               Power(s1,2)*(82 + 107*Power(t1,2) + 60*t2 - 
                  9*t1*(21 + 11*t2)) + 
               s1*(-95 + 21*Power(t1,3) - 188*t2 - 75*Power(t2,2) - 
                  Power(t1,2)*(137 + 238*t2) + 
                  t1*(211 + 426*t2 + 114*Power(t2,2))))) + 
         Power(s2,3)*(-1 + s + t1)*
          (2*Power(s,9)*t1*(-6 + 19*t1) + 
            Power(s,8)*(21*Power(t1,3) + 
               Power(t1,2)*(-208 - 94*s1 + 65*t2) + 
               2*(-2 + t2 - 3*(-1 + s1)*Power(t2,2) + 8*Power(t2,3)) - 
               2*t1*(-45 + 13*t2 + Power(t2,2) + s1*(-20 + 3*t2))) + 
            Power(s,7)*(42 - 97*Power(t1,4) - 4*t2 + 4*Power(t2,2) - 
               92*Power(t2,3) + 2*Power(s1,2)*t1*(-9 + 8*t1 + t2) + 
               Power(t1,3)*(-176 + 183*t2) - 
               5*Power(t1,2)*(-109 + 66*t2 + 9*Power(t2,2)) + 
               t1*(-286 + 112*t2 + 28*Power(t2,2) + 45*Power(t2,3)) - 
               s1*(10 + 201*Power(t1,3) + 2*Power(t1,2)*(-284 + t2) + 
                  6*t2 - 34*Power(t2,2) + 
                  t1*(208 - 8*t2 + 6*Power(t2,2)))) - 
            (-1 + t1)*(2*Power(s1,3)*Power(t1,3)*(4 + 5*t1) + 
               Power(s1,2)*t1*
                (Power(t1,2)*(25 - 53*t2) + t1*(47 - 17*t2) + 
                  8*Power(t1,3)*(-8 + t2) + 8*(-1 + t2)) + 
               Power(t1,4)*(-44 + 8*t2) + 
               Power(-1 + t2,2)*(-1 + 19*t2) + 
               Power(t1,3)*(133 - 165*t2 + 20*Power(t2,2)) + 
               t1*(47 - 191*t2 + 193*Power(t2,2) - 49*Power(t2,3)) + 
               3*Power(t1,2)*
                (-45 + 109*t2 - 58*Power(t2,2) + 4*Power(t2,3)) - 
               2*s1*(4*Power(-1 + t2,2) + Power(t1,4)*(-49 + 8*t2) + 
                  Power(t1,2)*(-15 + 91*t2 - 46*Power(t2,2)) + 
                  t1*(-23 + 18*t2 + 5*Power(t2,2)) + 
                  Power(t1,3)*(83 - 109*t2 + 10*Power(t2,2)))) + 
            Power(s,6)*(-204 + 24*Power(s1,3)*Power(t1,2) - 
               122*Power(t1,5) + 19*t2 - 104*Power(t2,2) + 
               197*Power(t2,3) + Power(t1,4)*(79 + 178*t2) + 
               Power(t1,3)*(593 - 506*t2 - 67*Power(t2,2)) + 
               Power(t1,2)*(-913 + 495*t2 + 145*Power(t2,2) + 
                  37*Power(t2,3)) - 
               t1*(-609 + 74*t2 + 82*Power(t2,2) + 220*Power(t2,3)) + 
               2*Power(s1,2)*t1*
                (111 + 36*Power(t1,2) - 11*t2 - t1*(188 + 19*t2)) + 
               s1*(76 - 111*Power(t1,4) + Power(t1,3)*(822 - 64*t2) + 
                  26*t2 - 74*Power(t2,2) + 
                  Power(t1,2)*(-923 + 166*t2 + 47*Power(t2,2)) + 
                  t1*(38 + 8*t2 + 67*Power(t2,2)))) + 
            Power(s,3)*(916 + 2*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (-60 + 137*t1 - 53*Power(t1,2) + 3*Power(t1,3)) + 
               176*t2 - 46*Power(t2,2) - 36*Power(t2,3) + 
               Power(t1,6)*(2 + 20*t2) - 4*Power(t1,5)*(55 + 46*t2) + 
               Power(t1,4)*(2101 + 788*t2 + 16*Power(t2,2)) - 
               Power(t1,3)*(4775 + 1955*t2 + 102*Power(t2,2)) + 
               Power(t1,2)*(4946 + 2596*t2 + 87*Power(t2,2) - 
                  8*Power(t2,3)) + 
               t1*(-2972 - 1430*t2 + 24*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s1,2)*t1*
                (2*Power(t1,5) + Power(t1,4)*(-201 + 4*t2) + 
                  Power(t1,3)*(1293 + 26*t2) - 2*(509 + 83*t2) + 
                  3*t1*(1019 + 86*t2) - 2*Power(t1,2)*(1520 + 111*t2)) \
+ s1*(-266 + 6*Power(t1,7) + Power(t1,6)*(6 - 12*t2) - 78*t2 + 
                  46*Power(t2,2) + 2*Power(t1,5)*(247 + 64*t2) - 
                  2*Power(t1,4)*(1883 + 314*t2 + 8*Power(t2,2)) + 
                  2*t1*(1937 + 526*t2 + 44*Power(t2,2)) + 
                  Power(t1,3)*(8652 + 2048*t2 + 82*Power(t2,2)) - 
                  Power(t1,2)*(9011 + 2582*t2 + 98*Power(t2,2)))) + 
            s*(45 - 6*Power(t1,7) - 
               Power(s1,3)*Power(t1,2)*
                (-58 + 89*t1 - 25*Power(t1,2) + Power(t1,3)) + 10*t2 + 
               99*Power(t2,2) - 72*Power(t2,3) + 
               4*Power(t1,6)*(35 + t2) - 3*Power(t1,5)*(75 + 8*t2) - 
               2*Power(t1,4)*(124 + 47*t2 + 12*Power(t2,2)) + 
               Power(t1,3)*(739 + 629*t2 - 123*Power(t2,2) + 
                  24*Power(t2,3)) - 
               Power(t1,2)*(453 + 890*t2 - 646*Power(t2,2) + 
                  140*Power(t2,3)) + 
               t1*(8 + 365*t2 - 598*Power(t2,2) + 195*Power(t2,3)) + 
               Power(s1,2)*t1*
                (56 - 10*Power(t1,5) - 64*t2 + 7*t1*(-51 + 2*t2) + 
                  Power(t1,4)*(53 + 4*t2) - 
                  2*Power(t1,3)*(46 + 43*t2) + 
                  Power(t1,2)*(350 + 153*t2)) + 
               s1*(14 + 6*Power(t1,7) - 64*t2 + 34*Power(t2,2) + 
                  Power(t1,6)*(-22 + 4*t2) + 
                  Power(t1,5)*(-87 + 16*t2) + 
                  Power(t1,2)*(245 + 422*t2 - 253*Power(t2,2)) + 
                  Power(t1,4)*(193 + 164*t2 - 16*Power(t2,2)) + 
                  t1*(-155 + 218*t2 + 51*Power(t2,2)) + 
                  Power(t1,3)*(-194 - 760*t2 + 163*Power(t2,2)))) + 
            Power(s,5)*(597 - 48*Power(t1,6) + 
               2*Power(s1,3)*Power(t1,2)*(-71 + 20*t1) - 54*t2 + 
               199*Power(t2,2) - 184*Power(t2,3) + 
               Power(t1,5)*(91 + 68*t2) + 
               Power(t1,4)*(419 - 168*t2 - 24*Power(t2,2)) + 
               Power(t1,2)*(2078 + 668*t2 + 40*Power(t2,2) - 
                  124*Power(t2,3)) + 
               Power(t1,3)*(-1300 - 241*t2 + 81*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-1562 - 559*t2 + 82*Power(t2,2) + 353*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-876 + 85*Power(t1,3) + 36*t2 - 
                  3*Power(t1,2)*(310 + 17*t2) + 4*t1*(509 + 60*t2)) + 
               s1*(-234 + 20*Power(t1,5) + Power(t1,4)*(295 - 128*t2) - 
                  44*t2 + 78*Power(t2,2) + 
                  t1*(1841 + 258*t2 - 165*Power(t2,2)) + 
                  Power(t1,3)*(273 + 648*t2 + 67*Power(t2,2)) - 
                  Power(t1,2)*(2102 + 1310*t2 + 177*Power(t2,2)))) - 
            Power(s,2)*(374 + 12*Power(t1,6) + 6*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (104 - 69*t1 - 7*Power(t1,2) + 8*Power(t1,3)) + 
               155*t2 + 34*Power(t2,2) - 87*Power(t2,3) + 
               Power(t1,5)*(-559 + 40*t2) + 
               Power(t1,4)*(1397 - 98*t2 - 56*Power(t2,2)) + 
               Power(t1,2)*(1167 + 427*t2 + 181*Power(t2,2) - 
                  67*Power(t2,3)) + 
               Power(t1,3)*(-1422 - 170*t2 + 179*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-975 - 354*t2 - 418*Power(t2,2) + 140*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-153*Power(t1,4) + 20*Power(t1,5) + 
                  Power(t1,3)*(430 - 6*t2) + 
                  8*Power(t1,2)*(-77 + 9*t2) - 18*(11 + 9*t2) + 
                  t1*(437 + 18*t2)) + 
               s1*(-72 - 4*Power(t1,7) - 98*t2 + 54*Power(t2,2) - 
                  2*Power(t1,6)*(35 + 4*t2) + 
                  Power(t1,5)*(686 + 24*t2) + 
                  2*Power(t1,4)*(-963 - 96*t2 + 4*Power(t2,2)) + 
                  Power(t1,3)*(3238 + 280*t2 + 28*Power(t2,2)) + 
                  3*t1*(362 + 204*t2 + 47*Power(t2,2)) - 
                  Power(t1,2)*(2938 + 458*t2 + 183*Power(t2,2)))) + 
            Power(s,4)*(-1017 - 6*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*(224 - 165*t1 + 19*Power(t1,2)) - 
               15*t2 - 85*Power(t2,2) + 65*Power(t2,3) + 
               Power(t1,6)*(22 + 8*t2) + Power(t1,5)*(177 + 44*t2) - 
               Power(t1,4)*(1046 + 695*t2 + 4*Power(t2,2)) - 
               4*Power(t1,3)*
                (-863 - 529*t2 - 49*Power(t2,2) + 3*Power(t2,3)) - 
               6*t1*(-517 - 245*t2 + 17*Power(t2,2) + 30*Power(t2,3)) + 
               Power(t1,2)*(-4732 - 2695*t2 - 325*Power(t2,2) + 
                  107*Power(t2,3)) + 
               Power(s1,2)*t1*
                (1444 + 31*Power(t1,4) + 44*t2 - 
                  Power(t1,3)*(719 + 7*t2) + 
                  Power(t1,2)*(2954 + 228*t2) - t1*(3994 + 431*t2)) + 
               s1*(356 + 30*Power(t1,6) + Power(t1,5)*(3 - 72*t2) + 
                  52*t2 - 50*Power(t2,2) + 
                  Power(t1,4)*(1227 + 538*t2 + 20*Power(t2,2)) + 
                  2*t1*(-2199 - 437*t2 + 54*Power(t2,2)) - 
                  2*Power(t1,3)*(2983 + 996*t2 + 86*Power(t2,2)) + 
                  Power(t1,2)*(8395 + 2994*t2 + 196*Power(t2,2))))) + 
         Power(s2,8)*(-2*Power(s,5)*(7 - 16*t1 + 9*Power(t1,2)) + 
            Power(s,4)*(34 + 20*Power(s1,3) + Power(t1,3) + 18*t2 - 
               66*Power(t2,2) - 56*Power(t2,3) - 
               2*Power(s1,2)*(6 + 5*t1 + 45*t2) + 
               Power(t1,2)*(58 + 90*t2) + 
               t1*(-93 - 104*t2 + 42*Power(t2,2)) + 
               s1*(-51 - 121*Power(t1,2) - 24*t1*(-7 + t2) + 70*t2 + 
                  126*Power(t2,2))) - 
            Power(-1 + t1,2)*
             (-2 + 6*Power(s1,3)*(2 + 4*t1 + Power(t1,2)) + 11*t2 - 
               6*Power(t2,2) - 19*Power(t2,3) + 
               Power(t1,3)*(2 + 4*t2) + 
               t1*(6 - 18*t2 + 7*Power(t2,2) - 21*Power(t2,3)) - 
               Power(t1,2)*(6 - 3*t2 + Power(t2,2) + 2*Power(t2,3)) + 
               Power(s1,2)*(6 + 6*Power(t1,3) - 46*t2 - 
                  Power(t1,2)*(1 + 17*t2) - t1*(11 + 63*t2)) + 
               s1*(-7 - 2*t2 + 53*Power(t2,2) - 
                  4*Power(t1,3)*(2 + t2) + 
                  Power(t1,2)*(9 - 4*t2 + 13*Power(t2,2)) + 
                  2*t1*(3 + 5*t2 + 30*Power(t2,2)))) - 
            Power(s,2)*(5 - 8*Power(t1,5) + 
               Power(s1,3)*(70 - 160*t1 + 63*Power(t1,2)) - 174*t2 - 
               175*Power(t2,2) - 53*Power(t2,3) + 
               Power(t1,4)*(81 + 79*t2) - 
               Power(t1,3)*(200 + 380*t2 + 237*Power(t2,2)) + 
               Power(t1,2)*(194 + 349*t2 + 178*Power(t2,2) + 
                  27*Power(t2,3)) + 
               t1*(-72 + 126*t2 + 234*Power(t2,2) + 53*Power(t2,3)) - 
               Power(s1,2)*(209 + 107*Power(t1,3) + 220*t2 + 
                  2*Power(t1,2)*(58 + 63*t2) - t1*(432 + 427*t2)) + 
               s1*(239 + 402*t2 + 203*Power(t2,2) + 
                  Power(t1,3)*(78 + 326*t2) - 
                  80*t1*(5 + 9*t2 + 4*Power(t2,2)) + 
                  Power(t1,2)*(83 - 8*t2 + 36*Power(t2,2)))) - 
            s*(-1 + t1)*(-36 + 
               5*Power(s1,3)*(-9 - 5*t1 + 11*Power(t1,2)) - 65*t2 + 
               53*Power(t2,2) + 23*Power(t2,3) + 
               6*Power(t1,4)*(3 + 2*t2) + 
               Power(t1,3)*(-18 + 8*t2 - 23*Power(t2,2)) + 
               6*t1*(15 + 27*t2 + 22*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,2)*(54 + 117*t2 + 162*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(s1,2)*(81 + 25*Power(t1,3) + 98*t2 + 
                  4*t1*(31 + 23*t2) - 5*Power(t1,2)*(46 + 29*t2)) + 
               s1*(89 + 10*Power(t1,4) - 132*t2 - 76*Power(t2,2) - 
                  2*Power(t1,3)*(49 + 2*t2) - 
                  t1*(256 + 262*t2 + 79*Power(t2,2)) + 
                  Power(t1,2)*(255 + 398*t2 + 110*Power(t2,2)))) + 
            Power(s,3)*(103 + 27*Power(t1,4) + 
               Power(s1,3)*(-25 + 66*t1) + 274*t2 + 270*Power(t2,2) + 
               93*Power(t2,3) - Power(t1,3)*(114 + 73*t2) + 
               Power(s1,2)*(166 - 457*t1 + 272*Power(t1,2) + 122*t2 - 
                  272*t1*t2) + 
               Power(t1,2)*(250 + 573*t2 + 412*Power(t2,2)) - 
               t1*(266 + 774*t2 + 701*Power(t2,2) + 161*Power(t2,3)) - 
               s1*(15*Power(t1,3) + Power(t1,2)*(319 + 662*t2) + 
                  2*(98 + 207*t2 + 95*Power(t2,2)) - 
                  t1*(530 + 1114*t2 + 367*Power(t2,2))))) - 
         Power(s2,4)*(2*Power(s,9)*(3 - 32*t1 + 51*Power(t1,2)) + 
            Power(s,8)*(-42 + 163*Power(t1,3) + 14*t2 + 
               38*Power(t2,2) + 56*Power(t2,3) + 
               6*Power(s1,2)*(t1 + t2) + 2*Power(t1,2)*(-283 + 59*t2) - 
               2*t1*(-175 + 36*t2 + 7*Power(t2,2)) - 
               s1*(20 + 159*Power(t1,2) + 6*t2 + 42*Power(t2,2) + 
                  8*t1*(-17 + 4*t2))) - 
            Power(-1 + t1,2)*
             (20 + 6*Power(s1,3)*Power(t1,2)*
                (1 + 4*t1 + 2*Power(t1,2)) - 31*t2 - 22*Power(t2,2) + 
               33*Power(t2,3) + 4*Power(t1,4)*(-11 + 3*t2) + 
               2*Power(t1,3)*(56 - 117*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(-72 + 401*t2 - 317*Power(t2,2) + 
                  30*Power(t2,3)) - 
               t1*(16 + 148*t2 - 299*Power(t2,2) + 105*Power(t2,3)) + 
               Power(s1,2)*(Power(t1,2)*(121 - 91*t2) + 4*(-1 + t2) + 
                  4*Power(t1,4)*(-17 + 3*t2) + t1*(19 + 11*t2) - 
                  2*Power(t1,3)*(34 + 31*t2)) - 
               s1*(3 - 34*t2 + 31*Power(t2,2) + 
                  4*Power(t1,4)*(-25 + 6*t2) + 
                  Power(t1,2)*(167 + 56*t2 - 145*Power(t2,2)) + 
                  4*Power(t1,3)*(17 - 74*t2 + 10*Power(t2,2)) - 
                  2*t1*(69 - 125*t2 + 26*Power(t2,2)))) + 
            s*(-1 + t1)*(89 + 2*Power(t1,7) - 
               Power(s1,3)*t1*
                (-86 + 45*t1 + 28*Power(t1,2) + 33*Power(t1,3) + 
                  9*Power(t1,4)) + Power(t1,5)*(57 - 40*t2) + 109*t2 + 
               33*Power(t2,2) - 99*Power(t2,3) + 
               Power(t1,6)*(74 + 8*t2) + 
               Power(t1,4)*(-519 + 54*t2 - 104*Power(t2,2)) + 
               Power(t1,2)*(324 - 829*t2 + 616*Power(t2,2) - 
                  108*Power(t2,3)) + 
               Power(t1,3)*(356 + 448*t2 + 193*Power(t2,2) + 
                  16*Power(t2,3)) + 
               t1*(-383 + 250*t2 - 738*Power(t2,2) + 220*Power(t2,3)) + 
               Power(s1,2)*(26 - 14*Power(t1,6) + 
                  Power(t1,4)*(45 - 38*t2) + 
                  Power(t1,5)*(135 - 4*t2) - 30*t2 + 
                  Power(t1,2)*(56 + 139*t2) - t1*(259 + 208*t2) + 
                  Power(t1,3)*(11 + 228*t2)) + 
               s1*(-24 + 6*Power(t1,7) + Power(t1,6)*(2 - 4*t2) - 
                  52*t2 + 106*Power(t2,2) + 
                  Power(t1,5)*(-257 + 56*t2) + 
                  Power(t1,2)*(-365 + 286*t2 - 300*Power(t2,2)) + 
                  Power(t1,4)*(157 - 48*t2 + 24*Power(t2,2)) + 
                  Power(t1,3)*(426 - 856*t2 + 26*Power(t2,2)) + 
                  t1*(55 + 618*t2 + 57*Power(t2,2)))) - 
            Power(s,7)*(-102 + 4*Power(s1,3)*t1 + 95*Power(t1,4) + 
               Power(t1,3)*(649 - 469*t2) + 12*t2 + 94*Power(t2,2) + 
               275*Power(t2,3) + 
               Power(t1,2)*(-1403 + 893*t2 + 216*Power(t2,2)) - 
               t1*(-732 + 370*t2 + 297*Power(t2,2) + 203*Power(t2,3)) + 
               2*Power(s1,2)*
                (-6 + 40*Power(t1,2) + 16*t2 - t1*(1 + 17*t2)) + 
               s1*(441*Power(t1,3) - 2*Power(t1,2)*(656 + 39*t2) - 
                  2*(56 - 5*t2 + 100*Power(t2,2)) + 
                  t1*(872 + 14*t2 + 129*Power(t2,2)))) + 
            Power(s,6)*(-50 - 326*Power(t1,5) + 
               24*Power(s1,3)*t1*(-1 + 4*t1) - 80*t2 + 41*Power(t2,2) + 
               501*Power(t2,3) + Power(t1,4)*(42 + 675*t2) + 
               Power(t1,3)*(1344 - 1942*t2 - 421*Power(t2,2)) + 
               Power(t1,2)*(-1274 + 2179*t2 + 980*Power(t2,2) + 
                  237*Power(t2,3)) - 
               t1*(-475 + 684*t2 + 866*Power(t2,2) + 805*Power(t2,3)) - 
               Power(s1,2)*(124 + 164*Power(t1,3) - 72*t2 + 
                  t1*(-334 + 133*t2) + Power(t1,2)*(123 + 148*t2)) + 
               s1*(-115 - 350*Power(t1,4) + 94*t2 - 359*Power(t2,2) + 
                  Power(t1,3)*(2336 + 86*t2) + 
                  2*Power(t1,2)*(-1942 + 6*t2 + 23*Power(t2,2)) + 
                  4*t1*(432 + 37*t2 + 131*Power(t2,2)))) + 
            Power(s,2)*(160 + 
               Power(s1,3)*t1*
                (108 + 20*t1 - 208*Power(t1,2) + 200*Power(t1,3) - 
                  124*Power(t1,4) + 11*Power(t1,5)) - 158*t2 + 
               101*Power(t2,2) - 105*Power(t2,3) + 
               4*Power(t1,7)*(1 + 5*t2) - 4*Power(t1,6)*(-74 + 25*t2) + 
               Power(t1,5)*(238 + 270*t2 - 48*Power(t2,2)) + 
               Power(t1,3)*(5142 + 366*t2 - 277*Power(t2,2) - 
                  14*Power(t2,3)) + 
               Power(t1,4)*(-3250 - 183*t2 + 174*Power(t2,2) + 
                  4*Power(t2,3)) - 
               Power(t1,2)*(2830 + 1041*t2 - 770*Power(t2,2) + 
                  25*Power(t2,3)) + 
               t1*(240 + 826*t2 - 720*Power(t2,2) + 133*Power(t2,3)) - 
               Power(s1,2)*(108 + 94*Power(t1,6) + 8*Power(t1,7) + 
                  Power(t1,4)*(2287 - 14*t2) + 
                  4*Power(t1,5)*(-286 + t2) + 76*t2 - 
                  21*Power(t1,3)*(111 + 8*t2) - 
                  2*Power(t1,2)*(-761 + 50*t2) + t1*(-544 + 223*t2)) + 
               s1*(6*Power(t1,8) - 12*Power(t1,7)*(-3 + t2) + 
                  9*Power(t1,6)*(5 + 8*t2) + 
                  Power(t1,2)*(4216 + 1392*t2 - 498*Power(t2,2)) + 
                  Power(t1,4)*(5820 + 24*t2 - 84*Power(t2,2)) + 
                  8*Power(t1,5)*(-275 - 22*t2 + 3*Power(t2,2)) + 
                  8*t1*(-246 - 65*t2 + 29*Power(t2,2)) + 
                  3*(161 + 42*t2 + 45*Power(t2,2)) + 
                  Power(t1,3)*(-6438 - 906*t2 + 212*Power(t2,2)))) + 
            Power(s,5)*(-17 - 226*Power(t1,6) + 
               Power(s1,3)*t1*(246 - 597*t1 + 239*Power(t1,2)) + 
               375*t2 - 73*Power(t2,2) - 407*Power(t2,3) + 
               Power(t1,5)*(255 + 436*t2) + 
               Power(t1,4)*(817 - 1364*t2 - 275*Power(t2,2)) + 
               Power(t1,3)*(-700 + 1559*t2 + 769*Power(t2,2) + 
                  100*Power(t2,3)) - 
               Power(t1,2)*(-310 + 377*t2 + 822*Power(t2,2) + 
                  640*Power(t2,3)) + 
               t1*(-507 - 295*t2 + 605*Power(t2,2) + 995*Power(t2,3)) - 
               Power(s1,2)*(-462 + 110*Power(t1,4) + 74*t2 + 
                  t1*(2407 + 98*t2) + Power(t1,3)*(973 + 373*t2) - 
                  Power(t1,2)*(3458 + 803*t2)) + 
               s1*(-604 + 26*Power(t1,5) - 240*t2 + 294*Power(t2,2) - 
                  4*Power(t1,4)*(-313 + 56*t2) + 
                  t1*(1145 + 1350*t2 - 493*Power(t2,2)) + 
                  Power(t1,2)*(934 - 2952*t2 - 315*Power(t2,2)) + 
                  Power(t1,3)*(-3173 + 1474*t2 + 314*Power(t2,2)))) + 
            Power(s,3)*(-6*Power(t1,8) + 
               Power(s1,3)*t1*
                (228 - 814*t1 + 982*Power(t1,2) - 547*Power(t1,3) + 
                  72*Power(t1,4)) + 2*Power(t1,7)*(1 + 6*t2) + 
               2*Power(t1,6)*(83 + 48*t2) + 
               Power(t1,5)*(21 - 970*t2 - 72*Power(t2,2)) + 
               2*Power(t1,4)*(787 + 1692*t2 + 193*Power(t2,2)) - 
               Power(t1,3)*(6262 + 4873*t2 + 592*Power(t2,2)) + 
               Power(t1,2)*(6996 + 3819*t2 + 416*Power(t2,2) - 
                  8*Power(t2,3)) + 
               t1*(-2569 - 2222*t2 + 207*Power(t2,2) + Power(t2,3)) + 
               13*(6 + 58*t2 - 24*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s1,2)*(528 - 37*Power(t1,6) + 76*t2 - 
                  Power(t1,5)*(523 + 40*t2) - 2*t1*(1772 + 169*t2) + 
                  Power(t1,4)*(4318 + 386*t2) - 
                  2*Power(t1,3)*(4209 + 403*t2) + 
                  Power(t1,2)*(7709 + 912*t2)) + 
               s1*(56*Power(t1,7) - 3*Power(t1,6)*(5 + 36*t2) + 
                  t1*(7086 + 3254*t2 + 13*Power(t2,2)) - 
                  2*(814 + 189*t2 + 34*Power(t2,2)) + 
                  Power(t1,5)*(750 + 864*t2 + 40*Power(t2,2)) - 
                  2*Power(t1,4)*(3770 + 1668*t2 + 101*Power(t2,2)) - 
                  2*Power(t1,2)*(7503 + 3001*t2 + 129*Power(t2,2)) + 
                  Power(t1,3)*(16297 + 5640*t2 + 332*Power(t2,2)))) + 
            Power(s,4)*(-128 - 62*Power(t1,7) + 
               2*Power(s1,3)*t1*
                (-234 + 585*t1 - 492*Power(t1,2) + 100*Power(t1,3)) - 
               815*t2 + 310*Power(t2,2) + 125*Power(t2,3) + 
               Power(t1,6)*(63 + 124*t2) - 
               7*Power(t1,5)*(-85 + 26*t2 + 8*Power(t2,2)) + 
               t1*(2279 + 2132*t2 + 63*Power(t2,2) - 379*Power(t2,3)) + 
               Power(t1,3)*(1440 + 4106*t2 + 591*Power(t2,2) - 
                  127*Power(t2,3)) + 
               Power(t1,4)*(-621 - 1121*t2 + 13*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,2)*(-3566 - 4314*t2 - 711*Power(t2,2) + 
                  381*Power(t2,3)) + 
               Power(s1,2)*(-61*Power(t1,5) + 2*(-374 + t2) + 
                  11*t1*(437 + 53*t2) - 3*Power(t1,4)*(418 + 79*t2) + 
                  Power(t1,3)*(6648 + 1231*t2) - 
                  Power(t1,2)*(9048 + 1429*t2)) + 
               s1*(1745 + 144*Power(t1,6) + Power(t1,5)*(58 - 296*t2) + 
                  396*t2 - 85*Power(t2,2) - 
                  2*t1*(3516 + 1917*t2 + 41*Power(t2,2)) - 
                  6*Power(t1,3)*(1193 + 1101*t2 + 135*Power(t2,2)) + 
                  Power(t1,4)*(496 + 2168*t2 + 221*Power(t2,2)) + 
                  Power(t1,2)*(11817 + 7618*t2 + 678*Power(t2,2))))) + 
         Power(s2,7)*(2*Power(s,6)*(18 - 50*t1 + 33*Power(t1,2)) + 
            Power(s,5)*(-20*Power(s1,3) + 39*Power(t1,3) + 
               8*Power(s1,2)*(1 + 5*t1 + 15*t2) - 
               Power(t1,2)*(238 + 97*t2) + 
               t1*(263 + 66*t2 - 70*Power(t2,2)) + 
               2*(-38 + 10*t2 + 65*Power(t2,2) + 56*Power(t2,3)) + 
               s1*(43 + 180*Power(t1,2) - 100*t2 - 210*Power(t2,2) - 
                  2*t1*(106 + 5*t2))) + 
            Power(-1 + t1,2)*
             (-5 + 2*Power(t1,4) + 
               6*Power(s1,3)*(1 + 4*t1 + 2*Power(t1,2)) + 31*t2 - 
               43*Power(t2,2) - 9*Power(t2,3) + 
               Power(t1,3)*(-1 + 9*t2 - 4*Power(t2,2)) + 
               t1*(13 - 53*t2 + 45*Power(t2,2) - 21*Power(t2,3)) + 
               Power(t1,2)*(-9 + 13*t2 + 2*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(s1,2)*(-4 + 2*Power(t1,4) + 
                  Power(t1,2)*(5 - 35*t2) + Power(t1,3)*(11 - 7*t2) - 
                  34*t2 - 2*t1*(7 + 25*t2)) - 
               2*s1*(9 + 2*Power(t1,4) - 18*t2 - 20*Power(t2,2) + 
                  Power(t1,2)*(-7 + 17*t2 - 22*Power(t2,2)) + 
                  Power(t1,3)*(5 + t2 - 2*Power(t2,2)) - 
                  t1*(9 + 19*Power(t2,2)))) + 
            s*(-1 + t1)*(-41 + 
               Power(s1,3)*(13 - 37*t1 + 18*Power(t1,2) + 
                  35*Power(t1,3)) - 65*t2 - 36*Power(t2,2) - 
               49*Power(t2,3) + 2*Power(t1,5)*(9 + 4*t2) + 
               Power(t1,4)*(51 + 28*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(174 + 207*t2 + 94*Power(t2,2) - 
                  24*Power(t2,3)) - 
               Power(t1,3)*(220 + 194*t2 + 179*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(18 + 16*t2 + 129*Power(t2,2) + 52*Power(t2,3)) + 
               Power(s1,2)*(11*Power(t1,4) - 
                  Power(t1,2)*(112 + 33*t2) - 3*(3 + 40*t2) + 
                  3*t1*(67 + 60*t2) - Power(t1,3)*(91 + 114*t2)) + 
               s1*(165 + 22*Power(t1,5) + 8*t2 + 160*Power(t2,2) - 
                  4*Power(t1,4)*(34 + 5*t2) + 
                  3*Power(t1,2)*(3 - 48*t2 + 17*Power(t2,2)) + 
                  Power(t1,3)*(238 + 358*t2 + 83*Power(t2,2)) - 
                  t1*(298 + 202*t2 + 207*Power(t2,2)))) + 
            Power(s,3)*(-200 - 53*Power(t1,5) + 
               Power(s1,3)*(53 - 192*t1 + 123*Power(t1,2)) + 77*t2 - 
               22*Power(t2,2) + 15*Power(t2,3) + 
               Power(t1,4)*(265 + 261*t2) - 
               Power(t1,3)*(469 + 1359*t2 + 662*Power(t2,2)) - 
               t1*(-302 + 516*t2 + 248*Power(t2,2) + 115*Power(t2,3)) + 
               Power(t1,2)*(155 + 1537*t2 + 932*Power(t2,2) + 
                  162*Power(t2,3)) + 
               Power(s1,2)*(-29 - 339*Power(t1,3) + 
                  Power(t1,2)*(287 - 232*t2) - 156*t2 + 
                  t1*(81 + 482*t2)) + 
               s1*(122*Power(t1,4) + Power(t1,3)*(91 + 814*t2) - 
                  Power(t1,2)*(95 + 708*t2 + 35*Power(t2,2)) + 
                  2*(-11 + 94*t2 + 53*Power(t2,2)) - 
                  t1*(96 + 294*t2 + 211*Power(t2,2)))) + 
            Power(s,4)*(-27 + Power(s1,3)*(35 - 84*t1) - 
               72*Power(t1,4) - 326*t2 - 406*Power(t2,2) - 
               208*Power(t2,3) + 10*Power(t1,3)*(18 + 17*t2) - 
               Power(t1,2)*(316 + 1135*t2 + 729*Power(t2,2)) + 
               t1*(235 + 1246*t2 + 1206*Power(t2,2) + 
                  343*Power(t2,3)) + 
               Power(s1,2)*(-410*Power(t1,2) - 2*(97 + 94*t2) + 
                  t1*(653 + 400*t2)) + 
               s1*(183 + 139*Power(t1,3) + 482*t2 + 350*Power(t2,2) + 
                  Power(t1,2)*(353 + 996*t2) - 
                  2*t1*(315 + 799*t2 + 324*Power(t2,2)))) + 
            Power(s,2)*(135 - 8*Power(t1,6) + 
               Power(s1,3)*(-61 + 182*t1 - 296*Power(t1,2) + 
                  162*Power(t1,3)) - 11*t2 + 257*Power(t2,2) + 
               41*Power(t2,3) + Power(t1,5)*(93 + 86*t2) - 
               Power(t1,4)*(35 + 276*t2 + 155*Power(t2,2)) - 
               Power(t1,3)*(508 + 322*t2 + 329*Power(t2,2) + 
                  17*Power(t2,3)) + 
               Power(t1,2)*(924 + 1117*t2 + 1115*Power(t2,2) + 
                  113*Power(t2,3)) - 
               t1*(601 + 594*t2 + 888*Power(t2,2) + 124*Power(t2,3)) + 
               Power(s1,2)*(-24*Power(t1,4) + 6*(27 + 23*t2) - 
                  8*t1*(74 + 67*t2) - Power(t1,3)*(485 + 439*t2) + 
                  Power(t1,2)*(939 + 876*t2)) + 
               s1*(123 + 101*Power(t1,5) - 502*t2 - 126*Power(t2,2) + 
                  Power(t1,4)*(-474 + 76*t2) + 
                  2*Power(t1,3)*(669 + 603*t2 + 151*Power(t2,2)) + 
                  t1*(447 + 1832*t2 + 502*Power(t2,2)) - 
                  Power(t1,2)*(1535 + 2612*t2 + 717*Power(t2,2))))) + 
         Power(s2,5)*(2*Power(s,8)*(13 - 70*t1 + 75*Power(t1,2)) + 
            Power(s,7)*(-110 - 2*Power(s1,3) + 210*Power(t1,3) + 
               39*t2 + 102*Power(t2,2) + 112*Power(t2,3) + 
               4*Power(s1,2)*(7*t1 + 9*t2) + 
               Power(t1,2)*(-710 + 99*t2) + 
               t1*(557 - 98*t2 - 42*Power(t2,2)) - 
               s1*(48 + 79*Power(t1,2) + 34*t2 + 126*Power(t2,2) + 
                  6*t1*(-21 + 11*t2))) - 
            Power(-1 + t1,2)*
             (28 + 6*Power(s1,3)*Power(t1,2)*(2 + 4*t1 + Power(t1,2)) - 
               98*t2 + 57*Power(t2,2) + 23*Power(t2,3) + 
               2*Power(t1,4)*(-9 + 4*t2) + 
               2*Power(t1,3)*(13 - 73*t2 + 20*Power(t2,2)) + 
               t1*(-66 + 66*t2 + 173*Power(t2,2) - 105*Power(t2,3)) + 
               10*Power(t1,2)*
                (3 + 17*t2 - 27*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s1,2)*(-3 + Power(t1,2)*(60 - 114*t2) + 
                  t1*(83 - 15*t2) + 13*t2 + Power(t1,4)*(-30 + 8*t2) - 
                  2*Power(t1,3)*(55 + 9*t2)) - 
               2*s1*(-17 + 5*t2 + 22*Power(t2,2) + 
                  Power(t1,4)*(-21 + 8*t2) + 
                  t1*(-38 + 171*t2 - 65*Power(t2,2)) + 
                  Power(t1,3)*(-30 - 82*t2 + 20*Power(t2,2)) - 
                  2*Power(t1,2)*(-53 + 51*t2 + 20*Power(t2,2)))) + 
            s*(-1 + t1)*(39 + 2*Power(t1,7) + 
               Power(s1,3)*(38 + 85*t1 - 36*Power(t1,2) - 
                  10*Power(t1,3) - 67*Power(t1,4) + 5*Power(t1,5)) + 
               Power(t1,5)*(83 - 2*t2) + 46*t2 - 19*Power(t2,2) - 
               79*Power(t2,3) + Power(t1,6)*(34 + 8*t2) - 
               Power(t1,4)*(215 + 130*t2 + 128*Power(t2,2)) - 
               2*Power(t1,3)*
                (131 - 293*t2 - 97*Power(t2,2) + 8*Power(t2,3)) + 
               t1*(-375 + 260*t2 - 636*Power(t2,2) + 32*Power(t2,3)) + 
               Power(t1,2)*(694 - 768*t2 + 589*Power(t2,2) + 
                  48*Power(t2,3)) - 
               Power(s1,2)*(66 + 6*Power(t1,6) + 
                  Power(t1,3)*(338 - 196*t2) + 121*t2 + 
                  21*Power(t1,4)*(-17 + 2*t2) + 
                  Power(t1,5)*(1 + 4*t2) - 
                  Power(t1,2)*(185 + 274*t2) + t1*(131 + 348*t2)) + 
               s1*(91 + 2*Power(t1,7) + Power(t1,6)*(22 - 4*t2) + 
                  36*t2 + 179*Power(t2,2) + 
                  Power(t1,5)*(-129 + 16*t2) - 
                  2*Power(t1,3)*(-432 + 458*t2 + 5*Power(t2,2)) + 
                  Power(t1,4)*(-247 + 144*t2 + 48*Power(t2,2)) - 
                  2*Power(t1,2)*(251 - 84*t2 + 205*Power(t2,2)) + 
                  t1*(-101 + 556*t2 + 238*Power(t2,2)))) + 
            Power(s,2)*(458 - 2*Power(t1,8) + 
               Power(s1,3)*(-2 + 130*t1 - 282*Power(t1,2) + 
                  516*Power(t1,3) - 383*Power(t1,4) + 54*Power(t1,5)) - 
               199*t2 + 228*Power(t2,2) - 60*Power(t2,3) + 
               2*Power(t1,7)*(7 + 4*t2) + 16*Power(t1,6)*(12 + 5*t2) - 
               Power(t1,5)*(124 + 651*t2 + 120*Power(t2,2)) + 
               Power(t1,2)*(1686 + 510*t2 - 629*Power(t2,2) - 
                  54*Power(t2,3)) + 
               Power(t1,4)*(-614 + 1685*t2 + 158*Power(t2,2) - 
                  8*Power(t2,3)) + 
               2*Power(t1,3)*
                (20 - 902*t2 + 259*Power(t2,2) + 23*Power(t2,3)) + 
               t1*(-1650 + 371*t2 - 155*Power(t2,2) + 43*Power(t2,3)) - 
               Power(s1,2)*(-196 + 59*Power(t1,6) + 74*t2 + 
                  t1*(859 + 21*t2) - 8*Power(t1,3)*(-441 + 29*t2) + 
                  2*Power(t1,5)*(9 + 31*t2) - 
                  2*Power(t1,4)*(881 + 80*t2) + 
                  Power(t1,2)*(-2506 + 334*t2)) + 
               s1*(-116 + 42*Power(t1,7) - 260*t2 + 139*Power(t2,2) - 
                  Power(t1,6)*(53 + 60*t2) + 
                  Power(t1,3)*(4852 + 664*t2 - 356*Power(t2,2)) + 
                  t1*(1949 - 318*t2 - 46*Power(t2,2)) + 
                  4*Power(t1,4)*(-341 - 414*t2 + 5*Power(t2,2)) + 
                  Power(t1,5)*(-135 + 568*t2 + 40*Power(t2,2)) + 
                  Power(t1,2)*(-5175 + 1062*t2 + 302*Power(t2,2)))) + 
            Power(s,3)*(-671 - 32*Power(t1,7) + 
               2*Power(s1,3)*
                (67 - 296*t1 + 613*Power(t1,2) - 496*Power(t1,3) + 
                  111*Power(t1,4)) + 101*t2 - 296*Power(t2,2) - 
               10*Power(t2,3) + Power(t1,6)*(45 + 106*t2) + 
               Power(t1,5)*(625 - 76*t2 - 64*Power(t2,2)) - 
               Power(t1,4)*(895 + 1401*t2 + 354*Power(t2,2)) + 
               2*Power(t1,3)*
                (-193 + 2252*t2 + 750*Power(t2,2) + 8*Power(t2,3)) + 
               t1*(1061 + 2000*t2 + 586*Power(t2,2) + 18*Power(t2,3)) - 
               Power(t1,2)*(-253 + 5234*t2 + 1456*Power(t2,2) + 
                  44*Power(t2,3)) - 
               Power(s1,2)*(778 + 183*Power(t1,5) + 162*t2 + 
                  10*Power(t1,4)*(33 + 38*t2) + 
                  10*Power(t1,2)*(638 + 137*t2) - 
                  8*Power(t1,3)*(508 + 165*t2) - t1*(3523 + 576*t2)) + 
               2*s1*(384 + 96*Power(t1,6) + 367*t2 + 42*Power(t2,2) - 
                  2*Power(t1,5)*(115 + 53*t2) + 
                  Power(t1,4)*(218 + 1118*t2 + 147*Power(t2,2)) - 
                  t1*(2354 + 1383*t2 + 155*Power(t2,2)) - 
                  2*Power(t1,3)*(782 + 1657*t2 + 208*Power(t2,2)) + 
                  Power(t1,2)*(3450 + 3402*t2 + 401*Power(t2,2)))) - 
            Power(s,6)*(104*Power(t1,4) + 2*Power(s1,3)*(-5 + 12*t1) + 
               Power(t1,3)*(455 - 521*t2) + 
               Power(t1,2)*(-1255 + 1331*t2 + 533*Power(t2,2)) + 
               3*(-51 + 23*t2 + 90*Power(t2,2) + 130*Power(t2,3)) - 
               t1*(-857 + 787*t2 + 841*Power(t2,2) + 385*Power(t2,3)) + 
               Power(s1,2)*(275*Power(t1,2) - 5*t1*(44 + 31*t2) + 
                  4*(3 + 32*t2)) + 
               s1*(-242 + 201*Power(t1,3) - 72*t2 - 415*Power(t2,2) - 
                  2*Power(t1,2)*(555 + 203*t2) + 
                  t1*(1051 + 454*t2 + 410*Power(t2,2)))) + 
            Power(s,4)*(163 - 158*Power(t1,6) + 
               2*Power(s1,3)*
                (-52 + 303*t1 - 433*Power(t1,2) + 177*Power(t1,3)) - 
               126*t2 - 45*Power(t2,2) - 167*Power(t2,3) + 
               3*Power(t1,5)*(73 + 148*t2) + 
               Power(t1,4)*(695 - 1460*t2 - 428*Power(t2,2)) + 
               Power(t1,3)*(-562 + 1523*t2 + 719*Power(t2,2) + 
                  105*Power(t2,3)) - 
               Power(t1,2)*(1188 - 135*t2 + 173*Power(t2,2) + 
                  433*Power(t2,3)) + 
               t1*(831 - 676*t2 - 91*Power(t2,2) + 453*Power(t2,3)) + 
               Power(s1,2)*(581 - 395*Power(t1,4) + 
                  Power(t1,3)*(12 - 707*t2) + 71*t2 - 
                  t1*(2488 + 799*t2) + Power(t1,2)*(2308 + 1387*t2)) + 
               s1*(-284 + 274*Power(t1,5) - 398*t2 + 74*Power(t2,2) - 
                  2*Power(t1,4)*(13 + 10*t2) + 
                  Power(t1,2)*(1473 - 4054*t2 - 702*Power(t2,2)) + 
                  t1*(586 + 2818*t2 + 234*Power(t2,2)) + 
                  Power(t1,3)*(-1863 + 1654*t2 + 494*Power(t2,2)))) + 
            Power(s,5)*(48 - 292*Power(t1,5) + 
               Power(s1,3)*(2 - 73*t1 + 165*Power(t1,2)) + 202*t2 + 
               319*Power(t2,2) + 459*Power(t2,3) + 
               Power(t1,4)*(263 + 762*t2) + 
               Power(t1,3)*(690 - 2612*t2 - 843*Power(t2,2)) + 
               Power(t1,2)*(-579 + 3357*t2 + 1889*Power(t2,2) + 
                  372*Power(t2,3)) - 
               t1*(226 + 1684*t2 + 1601*Power(t2,2) + 939*Power(t2,3)) - 
               Power(s1,2)*(56 + 556*Power(t1,3) - 149*t2 + 
                  4*Power(t1,2)*(-184 + 71*t2) + t1*(304 + 179*t2)) + 
               s1*(-437 + 6*Power(t1,4) - 88*t2 - 451*Power(t2,2) + 
                  4*Power(t1,3)*(349 + 146*t2) - 
                  2*Power(t1,2)*(1848 + 424*t2 + 13*Power(t2,2)) + 
                  t1*(2690 + 792*t2 + 809*Power(t2,2))))) + 
         Power(s2,6)*(-2*Power(s,7)*(22 - 80*t1 + 65*Power(t1,2)) + 
            Power(s,6)*(10*Power(s1,3) - 138*Power(t1,3) + 
               Power(t1,2)*(534 + 8*t2) - 
               2*Power(s1,2)*(1 + 25*t1 + 45*t2) + 
               t1*(-489 + 40*t2 + 70*Power(t2,2)) - 
               2*(-62 + 25*t2 + 75*Power(t2,2) + 70*Power(t2,3)) + 
               s1*(17 - 100*Power(t1,2) + 80*t2 + 210*Power(t2,2) + 
                  t1*(68 + 60*t2))) + 
            Power(-1 + t1,3)*
             (-14 + Power(s1,3)*
                (1 + 9*t1 + 9*Power(t1,2) + Power(t1,3)) + 82*t2 - 
               90*Power(t2,2) - 5*Power(t2,3) + 
               Power(t1,3)*(-5 + 2*t2) + 
               Power(t1,2)*(-4 - 37*t2 + 20*Power(t2,2)) + 
               t1*(23 - 47*t2 - 75*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s1,2)*(-8 - 21*t2 + Power(t1,3)*(-7 + 2*t2) + 
                  3*Power(t1,2)*(-18 + 5*t2) - t1*(76 + 11*t2)) + 
               s1*(-44 + Power(t1,3)*(11 - 4*t2) + 64*t2 + 
                  35*Power(t2,2) + t1*(-16 + 208*t2 - 45*Power(t2,2)) + 
                  Power(t1,2)*(49 + 22*t2 - 20*Power(t2,2)))) - 
            s*(-1 + t1)*(-39 + 
               Power(s1,3)*(62 + 7*t1 + 30*Power(t1,2) - 
                  54*Power(t1,3) + 10*Power(t1,4)) - 8*t2 - 
               22*Power(t2,2) - 91*Power(t2,3) + 
               2*Power(t1,6)*(5 + t2) + Power(t1,5)*(75 + 26*t2) - 
               2*Power(t1,4)*(58 + 69*t2 + 30*Power(t2,2)) + 
               t1*(-52 + 10*t2 - 387*Power(t2,2) + 8*Power(t2,3)) - 
               Power(t1,3)*(163 - 184*t2 + 176*Power(t2,2) + 
                  24*Power(t2,3)) + 
               Power(t1,2)*(285 - 76*t2 + 645*Power(t2,2) + 
                  52*Power(t2,3)) - 
               Power(s1,2)*(15*Power(t1,5) + 
                  Power(t1,2)*(208 - 320*t2) + 
                  4*Power(t1,4)*(-13 + 9*t2) + 
                  Power(t1,3)*(-71 + 58*t2) + 3*(35 + 73*t2) + 
                  t1*(-205 + 172*t2)) + 
               s1*(209 + 12*Power(t1,6) + 12*t2 + 273*Power(t2,2) - 
                  4*Power(t1,5)*(7 + 3*t2) + 
                  Power(t1,2)*(75 - 566*t2 - 345*Power(t2,2)) + 
                  4*Power(t1,4)*(-32 + 38*t2 + 5*Power(t2,2)) + 
                  t1*(-425 + 464*t2 + 88*Power(t2,2)) + 
                  Power(t1,3)*(285 - 50*t2 + 129*Power(t2,2)))) - 
            Power(s,4)*(239 - 162*Power(t1,5) + 
               Power(s1,3)*(21 - 134*t1 + 170*Power(t1,2)) + 352*t2 + 
               296*Power(t2,2) + 223*Power(t2,3) + 
               22*Power(t1,4)*(18 + 25*t2) - 
               Power(t1,3)*(221 + 2411*t2 + 980*Power(t2,2)) + 
               Power(t1,2)*(-178 + 3080*t2 + 1865*Power(t2,2) + 
                  335*Power(t2,3)) - 
               t1*(74 + 1704*t2 + 1252*Power(t2,2) + 565*Power(t2,3)) + 
               Power(s1,2)*(139 - 603*Power(t1,3) + 
                  Power(t1,2)*(935 - 315*t2) + 45*t2 + 
                  2*t1*(-289 + 60*t2)) + 
               s1*(-583 + 238*Power(t1,4) - 196*t2 - 213*Power(t2,2) + 
                  Power(t1,3)*(314 + 992*t2) - 
                  Power(t1,2)*(1562 + 1348*t2 + 85*Power(t2,2)) + 
                  2*t1*(863 + 365*t2 + 199*Power(t2,2)))) + 
            Power(s,2)*(-229 + 6*Power(t1,7) + 
               Power(s1,3)*(20 + 144*t1 - 461*Power(t1,2) + 
                  388*Power(t1,3) - 116*Power(t1,4)) + 344*t2 - 
               380*Power(t2,2) - 42*Power(t2,3) - 
               2*Power(t1,6)*(27 + 23*t2) + 
               9*Power(t1,5)*(-30 - t2 + 4*Power(t2,2)) + 
               2*Power(t1,2)*
                (-282 + 1092*t2 + 360*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,4)*(983 + 1022*t2 + 459*Power(t2,2) + 
                  10*Power(t2,3)) - 
               Power(t1,3)*(650 + 2320*t2 + 1208*Power(t2,2) + 
                  45*Power(t2,3)) + 
               t1*(778 - 1175*t2 + 373*Power(t2,2) + 88*Power(t2,3)) + 
               Power(s1,2)*(49 + 73*Power(t1,5) - 166*t2 + 
                  4*Power(t1,2)*(451 + 87*t2) + 
                  2*Power(t1,4)*(55 + 149*t2) + t1*(-950 + 248*t2) - 
                  Power(t1,3)*(1086 + 653*t2)) + 
               s1*(-404 - 110*Power(t1,6) + 460*t2 + 156*Power(t2,2) + 
                  8*Power(t1,5)*(59 + 10*t2) + 
                  t1*(706 + 438*t2 - 360*Power(t2,2)) - 
                  Power(t1,2)*(412 + 3070*t2 + 69*Power(t2,2)) - 
                  2*Power(t1,4)*(413 + 692*t2 + 108*Power(t2,2)) + 
                  Power(t1,3)*(574 + 3476*t2 + 414*Power(t2,2)))) + 
            Power(s,5)*(-131 + 104*Power(t1,4) + 
               Power(s1,3)*(-30 + 61*t1) + Power(t1,3)*(21 - 358*t2) + 
               212*t2 + 408*Power(t2,2) + 345*Power(t2,3) + 
               Power(t1,2)*(-333 + 1458*t2 + 790*Power(t2,2)) - 
               t1*(-396 + 1212*t2 + 1290*Power(t2,2) + 
                  455*Power(t2,3)) + 
               Power(s1,2)*(116 + 418*Power(t1,2) + 207*t2 - 
                  t1*(549 + 335*t2)) - 
               s1*(196 + 122*Power(t1,3) + 296*t2 + 475*Power(t2,2) + 
                  Power(t1,2)*(521 + 860*t2) - 
                  t1*(740 + 1258*t2 + 675*Power(t2,2)))) - 
            Power(s,3)*(-352 - 56*Power(t1,6) + 
               Power(s1,3)*(-152 + 566*t1 - 657*Power(t1,2) + 
                  296*Power(t1,3)) + 736*t2 + 322*Power(t2,2) + 
               46*Power(t2,3) + 3*Power(t1,5)*(63 + 88*t2) + 
               Power(t1,4)*(266 - 944*t2 - 362*Power(t2,2)) + 
               2*Power(t1,2)*
                (461 + 620*t2 + 591*Power(t2,2) - 6*Power(t2,3)) + 
               2*Power(t1,3)*
                (-637 + 158*t2 + 10*Power(t2,2) + 20*Power(t2,3)) - 
               t1*(-305 + 1612*t2 + 1243*Power(t2,2) + 
                  100*Power(t2,3)) + 
               Power(s1,2)*(599 - 259*Power(t1,4) + 284*t2 - 
                  2*Power(t1,3)*(44 + 367*t2) + 
                  2*Power(t1,2)*(682 + 729*t2) - t1*(1697 + 1140*t2)) + 
               s1*(-419 + 278*Power(t1,5) - 956*t2 - 234*Power(t2,2) + 
                  Power(t1,4)*(-747 + 136*t2) + 
                  2*Power(t1,3)*(422 + 833*t2 + 243*Power(t2,2)) + 
                  t1*(382 + 3638*t2 + 874*Power(t2,2)) - 
                  Power(t1,2)*(338 + 4322*t2 + 1021*Power(t2,2))))) - 
         Power(s2,2)*Power(-1 + s + t1,2)*
          (6*Power(s,9)*Power(t1,2) - 
            2*Power(s,8)*(4*Power(t1,3) + 
               Power(t1,2)*(23 + 10*s1 - 9*t2) + 2*t1*(-2 + t2) - 
               Power(t2,3)) - 
            Power(-1 + t1 - s1*t1 + t2,2)*
             (t1*(19 - 9*t2) + 4*(-1 + t2) + 
               Power(t1,2)*(-15 + 3*s1 + 2*t2)) - 
            2*Power(s,7)*(1 + 14*Power(t1,4) + 
               Power(t1,3)*(8 + 13*s1 - 17*t2) - 3*Power(t2,2) + 
               7*Power(t2,3) + 
               Power(t1,2)*(-93 - 6*Power(s1,2) + 39*t2 + 
                  2*Power(t2,2) + s1*(-41 + 2*t2)) - 
               t1*(-40 + 13*t2 - 2*Power(t2,2) + 2*Power(t2,3) + 
                  s1*(10 - 3*t2 + Power(t2,2)))) + 
            Power(s,6)*(16 - 16*Power(t1,5) + 6*t2 - 36*Power(t2,2) + 
               38*Power(t2,3) + 3*Power(t1,4)*(13 + 6*t2) + 
               Power(t1,3)*(277 + 28*Power(s1,2) + s1*(44 - 16*t2) - 
                  75*t2 - 4*Power(t2,2)) + 
               t1*(380 - 66*t2 + 26*Power(t2,2) - 25*Power(t2,3) - 
                  2*s1*(79 - 17*t2 + 2*Power(t2,2))) + 
               Power(t1,2)*(-606 + 54*t2 + 7*Power(t2,2) + 
                  2*Power(t2,3) - 2*Power(s1,2)*(61 + t2) + 
                  s1*(129 + 40*t2 + 6*Power(t2,2)))) + 
            Power(s,5)*(-48 - 2*Power(t1,6) - 30*t2 + 78*Power(t2,2) - 
               46*Power(t2,3) + 2*Power(t1,5)*(12 + 4*s1 + t2) + 
               Power(t1,4)*(189 + 20*Power(s1,2) + 4*t2 - 
                  s1*(43 + 16*t2)) + 
               Power(t1,3)*(-1043 - 230*Power(s1,2) - 2*Power(s1,3) - 
                  176*t2 + 4*Power(t2,2) + 
                  4*s1*(137 + 26*t2 + Power(t2,2))) + 
               t1*(-1088 + 52*t2 - 55*Power(t2,2) + 56*Power(t2,3) - 
                  2*s1*(-245 + 26*t2 + 9*Power(t2,2))) + 
               Power(t1,2)*(1813 + 308*t2 + 50*Power(t2,2) - 
                  8*Power(t2,3) + Power(s1,2)*(438 + 26*t2) - 
                  s1*(1350 + 214*t2 + 27*Power(t2,2)))) + 
            Power(s,2)*(30 - 2*(-2 + s1)*Power(t1,6) - 42*t2 + 
               84*Power(t2,2) - 46*Power(t2,3) + 
               2*Power(t1,5)*
                (-37 + 10*Power(s1,2) + 2*t2 - 2*s1*(13 + 3*t2)) - 
               Power(t1,4)*(84 + 46*Power(s1,2) + 19*Power(s1,3) + 
                  38*t2 - s1*(451 + 32*t2)) + 
               Power(t1,3)*(998 + 32*Power(s1,3) + 321*t2 + 
                  20*Power(t2,2) + Power(s1,2)*(5 + 72*t2) + 
                  4*s1*(-227 - 71*t2 + 2*Power(t2,2))) - 
               Power(t1,2)*(1577 + 644*t2 - 61*Power(t2,2) + 
                  10*Power(t2,3) + Power(s1,2)*(78 + 98*t2) + 
                  2*s1*(-325 - 338*t2 + 54*Power(t2,2))) + 
               t1*(703 + 414*t2 - 296*Power(t2,2) + 61*Power(t2,3) + 
                  2*s1*(-77 - 91*t2 + 54*Power(t2,2)))) - 
            Power(s,3)*(56 + 2*(-2 + s1)*Power(t1,6) + 12*t2 + 
               30*Power(t2,2) - 38*Power(t2,3) + 
               2*Power(t1,5)*
                (21 - 26*s1 + 11*Power(s1,2) + 12*t2 - 6*s1*t2) + 
               Power(t1,4)*(-691 - 11*Power(s1,3) - 176*t2 + 
                  Power(s1,2)*(-202 + 4*t2) + s1*(812 + 72*t2)) + 
               Power(t1,3)*(2694 + 36*Power(s1,3) + 702*t2 + 
                  40*Power(t2,2) + Power(s1,2)*(477 + 42*t2) - 
                  2*s1*(1315 + 160*t2 + 4*Power(t2,2))) - 
               Power(t1,2)*(3719 + 896*t2 + 112*Power(t2,2) + 
                  4*Power(s1,2)*(115 + 33*t2) + 
                  s1*(-2447 - 800*t2 + 42*Power(t2,2))) + 
               t1*(1687 + 336*t2 - 190*Power(t2,2) + 20*Power(t2,3) + 
                  2*s1*(-270 - 81*t2 + 65*Power(t2,2)))) + 
            s*(-14 + 2*(-5 + s1)*Power(t1,6) - 
               2*Power(t1,5)*
                (-68 + Power(s1,2) - 2*s1*(-5 + t2) - 3*t2) + 42*t2 - 
               54*Power(t2,2) + 22*Power(t2,3) + 
               Power(t1,4)*(13*Power(s1,3) + s1*(109 - 8*t2) + 
                  Power(s1,2)*(-70 + 4*t2) - 2*(167 + 6*t2)) + 
               Power(t1,3)*(223 - 10*Power(s1,3) + 
                  Power(s1,2)*(131 - 54*t2) - 120*t2 + 4*Power(t2,2) - 
                  4*s1*(55 - 46*t2 + 3*Power(t2,2))) - 
               t1*(49 + 254*t2 - 189*Power(t2,2) + 40*Power(t2,3) + 
                  2*s1*(5 - 44*t2 + 23*Power(t2,2))) + 
               Power(t1,2)*(48 + 338*t2 - 110*Power(t2,2) + 
                  8*Power(t2,3) + Power(s1,2)*(-30 + 34*t2) + 
                  s1*(139 - 326*t2 + 81*Power(t2,2)))) + 
            Power(s,4)*(70 + 2*(2 + s1)*Power(t1,6) + 48*t2 - 
               60*Power(t2,2) + 10*Power(t2,3) + 
               4*Power(t1,5)*(10 + Power(s1,2) + 3*t2 - s1*(5 + t2)) + 
               Power(t1,4)*(-2*Power(s1,3) + Power(s1,2)*(-127 + 2*t2) + 
                  4*s1*(94 + 15*t2) - 2*(294 + 73*t2)) + 
               Power(t1,3)*(2328 + 16*Power(s1,3) + 675*t2 + 
                  20*Power(t2,2) + Power(s1,2)*(568 + 9*t2) - 
                  2*s1*(1071 + 125*t2 + 6*Power(t2,2))) + 
               Power(t1,2)*(-3600 - 800*t2 - 153*Power(t2,2) + 
                  10*Power(t2,3) - 4*Power(s1,2)*(171 + 22*t2) + 
                  s1*(2866 + 558*t2 + 27*Power(t2,2))) + 
               t1*(4*s1*(-184 - 7*t2 + 20*Power(t2,2)) - 
                  5*(-368 - 21*t2 + Power(t2,2) + 9*Power(t2,3))))))*R1q(s2)\
)/(s*(-1 + s1)*(-1 + s2)*s2*Power(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + 
         Power(s2,2),2)*Power(-1 + s + t1,3)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(-s + s1 - t2)) - 
    (8*(4*Power(s,12)*Power(t1,2) + 
         2*Power(s,11)*(s2*(4 - 15*t1)*t1 + 13*Power(t1,3) + 
            Power(t2,3) + Power(t1,2)*(-16 - 3*s1 + 2*t2)) + 
         Power(-1 + t1,2)*Power(s2 + Power(s2,2) - 2*s2*t1 + 
            (-1 + t1)*t1,2)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          (6 + Power(t1,2)*(17 - 5*s1 - 2*t2) - 6*t2 + 
            Power(s2,3)*(2 - 3*s1 - 2*t1 + 3*t2) + 
            t1*(-23 + 2*s1 + 11*t2) + 
            s2*(11 + s1*(-2 + 4*t1 + Power(t1,2)) + t2 - 
               6*t1*(2 + t2) + Power(t1,2)*(1 + 2*t2)) + 
            Power(s2,2)*(1 + s1 + 2*s1*t1 + 2*Power(t1,2) + 2*t2 - 
               t1*(3 + 5*t2))) + 
         Power(s,10)*(61*Power(t1,4) + 
            Power(s2,2)*(4 - 52*t1 + 98*Power(t1,2)) + 
            2*(3 - 8*t2)*Power(t2,2) + 
            Power(t1,3)*(-206 - 39*s1 + 33*t2) + 
            Power(t1,2)*(108 + 2*Power(s1,2) + s1*(48 - 5*t2) - 10*t2 - 
               3*Power(t2,2)) + 
            t1*t2*(6 + 2*s1*(-3 + t2) - 4*t2 + 17*Power(t2,2)) + 
            s2*(-173*Power(t1,3) + Power(t1,2)*(254 + 40*s1 - 33*t2) + 
               6*(-1 + s1 - 3*t2)*Power(t2,2) + 
               2*t1*(-28 + 3*s1*(-2 + t2) + t2 + Power(t2,2)))) - 
         Power(s,9)*(-32*Power(t1,5) + 
            2*Power(s2,3)*(11 - 72*t1 + 91*Power(t1,2)) + 
            3*Power(t1,4)*(179 + 31*s1 - 40*t2) - 
            2*t2*(3 - 21*t2 + 26*Power(t2,2)) + 
            Power(t1,3)*(-671 - 13*Power(s1,2) + 58*t2 + 
               24*Power(t2,2) + s1*(-298 + 44*t2)) + 
            Power(t1,2)*(210 + 30*Power(s1,2) - 16*t2 + 7*Power(t2,2) - 
               64*Power(t2,3) + s1*(121 + 20*t2 - 17*Power(t2,2))) + 
            t1*(-6 + 50*t2 - 69*Power(t2,2) + 126*Power(t2,3) + 
               s1*(6 - 40*t2 + 6*Power(t2,2))) + 
            s2*(347*Power(t1,4) + 
               Power(t1,3)*(-1259 - 226*s1 + 251*t2) + 
               Power(t1,2)*(935 + 9*Power(s1,2) + s1*(356 - 71*t2) - 
                  133*t2 - 43*Power(t2,2)) + 
               t2*(6 + 10*t2 - 123*Power(t2,2) + s1*(-6 + 34*t2)) + 
               t1*(-148 + 55*Power(t2,2) + 141*Power(t2,3) + 
                  2*Power(s1,2)*(1 + t2) + 
                  s1*(-102 + 4*t2 - 37*Power(t2,2)))) + 
            Power(s2,2)*(-490*Power(t1,3) + Power(t1,2)*(852 - 111*t2) - 
               6*Power(s1,2)*(t1 + t2) + 
               2*t1*(-168 + 13*t2 + 8*Power(t2,2)) - 
               4*(-6 + t2 + 11*Power(t2,2) + 18*Power(t2,3)) + 
               s1*(107*Power(t1,2) + t1*(-56 + 38*t2) + 
                  6*(1 + t2 + 8*Power(t2,2))))) + 
         Power(s,8)*(2 - 140*Power(t1,6) + 
            10*Power(s2,4)*(5 - 22*t1 + 21*Power(t1,2)) - 36*t2 + 
            114*Power(t2,2) - 84*Power(t2,3) + 
            Power(t1,5)*(-674 - 60*s1 + 252*t2) + 
            Power(t1,4)*(1781 + 32*Power(s1,2) + s1*(737 - 172*t2) - 
               123*t2 - 84*Power(t2,2)) + 
            t1*(-40 + 187*t2 - 333*Power(t2,2) + 368*Power(t2,3) - 
               2*s1*(-16 + 43*t2 + 7*Power(t2,2))) - 
            Power(t1,2)*(-475 + 6*Power(s1,3) + 
               3*Power(s1,2)*(-64 + t2) + 228*t2 - 231*Power(t2,2) + 
               433*Power(t2,3) + s1*(159 - 254*t2 + 45*Power(t2,2))) + 
            Power(t1,3)*(-1237 - 206*Power(s1,2) - 132*t2 + 
               62*Power(t2,2) + 140*Power(t2,3) + 
               s1*(-618 + 46*t2 + 64*Power(t2,2))) + 
            Power(s2,3)*(114 + 2*Power(s1,3) - 763*Power(t1,3) + 
               Power(t1,2)*(1576 - 191*t2) - 29*t2 - 140*Power(t2,2) - 
               168*Power(t2,3) - 2*Power(s1,2)*(17*t1 + 21*t2) + 
               t1*(-855 + 98*t2 + 56*Power(t2,2)) + 
               2*s1*(11 + 66*Power(t1,2) + 20*t2 + 84*Power(t2,2) + 
                  t1*(-45 + 49*t2))) + 
            Power(s2,2)*(50 - 4*Power(s1,3)*t1 + 801*Power(t1,4) + 
               46*t2 - 96*Power(t2,2) - 418*Power(t2,3) + 
               Power(t1,3)*(-3259 + 773*t2) + 
               Power(t1,2)*(3096 - 699*t2 - 236*Power(t2,2)) + 
               t1*(-872 + 70*t2 + 471*Power(t2,2) + 516*Power(t2,3)) + 
               Power(s1,2)*(2 + 39*Power(t1,2) - 26*t2 + 
                  t1*(8 + 63*t2)) + 
               s1*(42 - 500*Power(t1,3) + Power(t1,2)*(990 - 260*t2) - 
                  23*t2 + 237*Power(t2,2) + 
                  t1*(-495 + 42*t2 - 330*Power(t2,2)))) - 
            s2*(105*Power(t1,5) + 
               Power(t1,4)*(-2586 - 437*s1 + 836*t2) + 
               Power(t1,3)*(4007 + 51*Power(s1,2) + 
                  s1*(1688 - 359*t2) - 784*t2 - 249*Power(t2,2)) + 
               t2*(14 + s1*(26 - 74*t2) - 191*t2 + 343*Power(t2,2)) + 
               Power(t1,2)*(-1740 + 4*Power(s1,3) + 49*t2 + 
                  392*Power(t2,2) + 484*Power(t2,3) + 
                  Power(s1,2)*(-119 + 3*t2) - 
                  2*s1*(673 - 91*t2 + 39*Power(t2,2))) + 
               t1*(214 + Power(s1,2)*(34 - 26*t2) - 4*t2 + 
                  9*Power(t2,2) - 876*Power(t2,3) + 
                  2*s1*(106 + 56*t2 + 111*Power(t2,2))))) + 
         Power(s,7)*(-364*Power(t1,7) - 
            2*Power(s2,5)*(30 - 100*t1 + 77*Power(t1,2)) + 
            14*Power(t1,6)*(-19 + 12*s1 + 24*t2) + 
            Power(t1,5)*(2620 + 28*Power(s1,2) + s1*(847 - 392*t2) - 
               77*t2 - 168*Power(t2,2)) + 
            2*(-5 + 39*t2 - 69*Power(t2,2) + 28*Power(t2,3)) + 
            Power(t1,2)*(-1495 + 24*Power(s1,3) + 1195*t2 - 
               955*Power(t2,2) + 1097*Power(t2,3) + 
               Power(s1,2)*(-573 + 47*t2) + 
               s1*(1438 - 809*t2 - 91*Power(t2,2))) + 
            Power(t1,4)*(-3178 - 602*Power(s1,2) - 827*t2 + 
               294*Power(t2,2) + 196*Power(t2,3) + 
               2*s1*(-589 + 189*t2 + 70*Power(t2,2))) + 
            t1*(99 - 368*t2 + 659*Power(t2,2) - 504*Power(t2,3) + 
               s1*(-50 + 24*t2 + 98*Power(t2,2))) - 
            Power(t1,3)*(-2620 + 47*Power(s1,3) + 229*t2 - 
               302*Power(t2,2) + 847*Power(t2,3) + 
               Power(s1,2)*(-1264 + 23*t2) + 
               s1*(1389 - 701*t2 + 147*Power(t2,2))) + 
            Power(s2,4)*(-12*Power(s1,3) + 700*Power(t1,3) + 
               2*Power(s1,2)*(1 + 39*t1 + 63*t2) + 
               Power(t1,2)*(-1750 + 159*t2) - 
               2*t1*(-599 + 79*t2 + 56*Power(t2,2)) + 
               7*(-32 + 11*t2 + 36*Power(t2,2) + 36*Power(t2,3)) - 
               s1*(25 + 29*Power(t1,2) + 114*t2 + 336*Power(t2,2) + 
                  18*t1*(-1 + 7*t2))) + 
            s2*(-16 + 749*Power(t1,6) - 
               3*Power(s1,3)*t1*(4 - 21*t1 + 9*Power(t1,2)) + 161*t2 - 
               550*Power(t2,2) + 485*Power(t2,3) - 
               3*Power(t1,5)*(-755 + 532*t2) + 
               Power(t1,4)*(-7997 + 2020*t2 + 707*Power(t2,2)) + 
               t1*(635 - 385*t2 + 922*Power(t2,2) - 2145*Power(t2,3)) + 
               Power(t1,3)*(6349 + 97*t2 - 1549*Power(t2,2) - 
                  952*Power(t2,3)) + 
               Power(t1,2)*(-2306 - 266*t2 + 579*Power(t2,2) + 
                  2682*Power(t2,3)) + 
               Power(s1,2)*t1*
                (328 - 105*Power(t1,3) - 94*t2 + 
                  Power(t1,2)*(767 + 37*t2) + t1*(-1142 + 101*t2)) + 
               s1*(2 + 91*Power(t1,5) + 44*t2 - 78*Power(t2,2) + 
                  Power(t1,4)*(-3251 + 1029*t2) + 
                  Power(t1,3)*(4919 - 1199*t2 + 14*Power(t2,2)) - 
                  5*Power(t1,2)*(191 + 73*t2 + 114*Power(t2,2)) + 
                  t1*(-461 + 408*t2 + 527*Power(t2,2)))) + 
            Power(s2,2)*(4*Power(t1,5) + 4*Power(s1,3)*t1*(3 + t1) + 
               Power(t1,4)*(-5041 + 2333*t2) + 
               Power(t1,3)*(9622 - 3627*t2 - 1117*Power(t2,2)) + 
               t1*(918 + 214*t2 - 1334*Power(t2,2) - 
                  2662*Power(t2,3)) + 
               2*(-17 - 65*t2 - 89*Power(t2,2) + 495*Power(t2,3)) + 
               Power(t1,2)*(-5420 + 1308*t2 + 2529*Power(t2,2) + 
                  1594*Power(t2,3)) + 
               2*Power(s1,2)*
                (-14 + 47*Power(t1,3) + t1*(29 - 152*t2) + 24*t2 + 
                  Power(t1,2)*(-73 + 87*t2)) + 
               s1*(-73 - 668*Power(t1,4) + 
                  Power(t1,3)*(3679 - 840*t2) + 122*t2 - 
                  454*Power(t2,2) + 
                  Power(t1,2)*(-4391 + 602*t2 - 912*Power(t2,2)) + 
                  2*t1*(730 + 83*t2 + 755*Power(t2,2)))) + 
            Power(s2,3)*(-225 - 931*Power(t1,4) + 
               Power(s1,3)*(-8 + 37*t1) + Power(t1,3)*(4576 - 1230*t2) - 
               44*t2 + 455*Power(t2,2) + 826*Power(t2,3) + 
               Power(t1,2)*(-5308 + 1829*t2 + 699*Power(t2,2)) - 
               t1*(-2074 + 630*t2 + 1485*Power(t2,2) + 
                  1092*Power(t2,3)) - 
               Power(s1,2)*(1 + 95*Power(t1,2) - 163*t2 + 
                  t1*(41 + 351*t2)) + 
               s1*(-148 + 442*Power(t1,3) - 37*t2 - 723*Power(t2,2) + 
                  Power(t1,2)*(-1331 + 316*t2) + 
                  t1*(963 + 208*t2 + 1106*Power(t2,2))))) + 
         s*(s2 - t1)*(-1 + t1)*
          (Power(s2,8)*(s1 - t2)*
             (6 + 5*Power(s1,2) + 6*Power(t1,2) + 
               2*s1*(-9 + 9*t1 - 5*t2) + 18*t2 + 5*Power(t2,2) - 
               6*t1*(2 + 3*t2)) + 
            Power(s2,7)*(-8 + Power(s1,3)*(-10 + t1) - 26*t2 - 
               21*Power(t2,2) - 2*Power(t2,3) + 
               8*Power(t1,3)*(1 + 4*t2) - 
               3*Power(t1,2)*(8 + 30*t2 + 31*Power(t2,2)) + 
               t1*(24 + 84*t2 + 114*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s1,2)*(-3 - 75*Power(t1,2) + 18*t2 + 
                  t1*(78 + 9*t2)) + 
               s1*(22 - 28*Power(t1,3) + 24*t2 - 6*Power(t2,2) + 
                  6*Power(t1,2)*(13 + 28*t2) - 
                  3*t1*(24 + 64*t2 + 7*Power(t2,2)))) - 
            Power(-1 + t1,2)*
             ((5 + 3*s1)*Power(t1,8) - 10*Power(-1 + t2,3) + 
               Power(t1,7)*(-8 - 3*Power(s1,2) - 4*s1*(-1 + t2) + 
                  3*t2) + Power(t1,5)*
                (164 - 41*Power(s1,3) + Power(s1,2)*(268 - 17*t2) + 
                  71*s1*(-6 + t2) - 20*t2 + 26*Power(t2,2) + 
                  Power(t2,3)) + 
               6*t1*Power(-1 + t2,2)*(-3*s1 + 2*(6 + t2)) + 
               Power(t1,2)*(-1 + t2)*
                (491 + 24*Power(s1,2) - 351*t2 - 22*Power(t2,2) + 
                  2*s1*(-136 + 65*t2)) + 
               Power(t1,3)*(898 - 4*Power(s1,3) + 
                  Power(s1,2)*(197 - 141*t2) - 1235*t2 + 
                  318*Power(t2,2) + 47*Power(t2,3) + 
                  s1*(-904 + 967*t2 - 163*Power(t2,2))) - 
               Power(t1,6)*(-5 + 5*Power(s1,2) + 26*t2 + Power(t2,2) - 
                  s1*(-8 + 6*t2 + Power(t2,2))) + 
               Power(t1,4)*(-655 + 33*Power(s1,3) + 598*t2 - 
                  92*Power(t2,2) - 16*Power(t2,3) + 
                  Power(s1,2)*(-433 + 170*t2) + 
                  s1*(1077 - 674*t2 + 14*Power(t2,2)))) + 
            Power(s2,6)*(2 + 
               Power(s1,3)*(-32 + 97*t1 - 73*Power(t1,2)) - 8*t2 + 
               25*Power(t2,2) - 12*Power(t2,3) - 
               Power(t1,4)*(36 + 79*t2) + 
               Power(t1,3)*(106 + 284*t2 + 219*Power(t2,2)) - 
               Power(t1,2)*(102 + 339*t2 + 314*Power(t2,2) + 
                  4*Power(t2,3)) + 
               2*t1*(15 + 71*t2 + 35*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s1,2)*(97 + 107*Power(t1,3) + 67*t2 - 
                  2*t1*(93 + 100*t2) + Power(t1,2)*(-18 + 157*t2)) + 
               s1*(-34 + 47*Power(t1,4) - 124*t2 - 23*Power(t2,2) - 
                  2*Power(t1,3)*(73 + 162*t2) + 
                  Power(t1,2)*(117 + 326*t2 - 80*Power(t2,2)) + 
                  t1*(16 + 122*t2 + 79*Power(t2,2)))) + 
            s2*(-1 + t1)*(Power(s1,3)*Power(t1,2)*
                (12 - 132*t1 + 313*Power(t1,2) - 208*Power(t1,3) - 
                  3*Power(t1,4)) - 4*Power(-1 + t2,2)*(20 + t2) + 
               Power(t1,8)*(23 + 3*t2) + 
               Power(t1,7)*(-47 + 6*t2 - 4*Power(t2,2)) + 
               Power(t1,4)*(-1947 + 226*t2 + 296*Power(t2,2) - 
                  49*Power(t2,3)) + 
               Power(t1,5)*(377 + 624*t2 + 20*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,6)*(57 - 185*t2 + 9*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(803 - 1285*t2 + 397*Power(t2,2) + 85*Power(t2,3)) + 
               Power(t1,3)*(3275 - 2603*t2 - 253*Power(t2,2) + 
                  281*Power(t2,3)) - 
               Power(t1,2)*(2461 - 3058*t2 + 393*Power(t2,2) + 
                  288*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-9*Power(t1,6) + Power(t1,2)*(2044 - 884*t2) - 
                  48*(-1 + t2) - Power(t1,5)*(27 + 25*t2) + 
                  Power(t1,4)*(1273 + 106*t2) + t1*(-579 + 411*t2) + 
                  Power(t1,3)*(-2750 + 494*t2)) + 
               s1*(22*Power(t1,8) + 18*Power(-1 + t2,2) - 
                  2*Power(t1,7)*(12 + 13*t2) + 
                  Power(t1,4)*(4851 - 1132*t2 - 274*Power(t2,2)) + 
                  Power(t1,3)*(-5465 + 3668*t2 - 83*Power(t2,2)) + 
                  Power(t1,6)*(-16 + 145*t2 + 12*Power(t2,2)) + 
                  Power(t1,5)*(-1553 - 506*t2 + 27*Power(t2,2)) - 
                  4*t1*(127 - 183*t2 + 56*Power(t2,2)) + 
                  5*Power(t1,2)*(535 - 569*t2 + 94*Power(t2,2)))) + 
            Power(s2,5)*(22 + 
               Power(s1,3)*(-35 + 203*t1 - 325*Power(t1,2) + 
                  179*Power(t1,3)) + 81*t2 + 69*Power(t2,2) - 
               44*Power(t2,3) + Power(t1,5)*(67 + 119*t2) - 
               Power(t1,4)*(166 + 515*t2 + 300*Power(t2,2)) + 
               Power(t1,2)*(104 - 240*t2 + 7*Power(t2,2) - 
                  110*Power(t2,3)) + 
               Power(t1,3)*(74 + 695*t2 + 490*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-101 - 140*t2 - 266*Power(t2,2) + 130*Power(t2,3)) + 
               Power(s1,2)*(200 - 35*Power(t1,4) + 62*t2 - 
                  5*t1*(170 + 87*t2) + 3*Power(t1,2)*(349 + 250*t2) - 
                  Power(t1,3)*(362 + 443*t2)) + 
               s1*(-110 - 18*Power(t1,5) - 302*t2 + 21*Power(t2,2) + 
                  2*Power(t1,4)*(5 + 163*t2) + 
                  3*t1*(143 + 408*t2 + 30*Power(t2,2)) + 
                  Power(t1,3)*(243 - 68*t2 + 258*Power(t2,2)) - 
                  Power(t1,2)*(554 + 1180*t2 + 303*Power(t2,2)))) - 
            Power(s2,4)*(-11 + 
               Power(s1,3)*(21 - 217*t1 + 593*Power(t1,2) - 
                  575*Power(t1,3) + 181*Power(t1,4)) - 253*t2 + 
               104*Power(t2,2) + 39*Power(t2,3) + 
               Power(t1,6)*(73 + 116*t2) - 
               Power(t1,5)*(118 + 567*t2 + 250*Power(t2,2)) + 
               t1*(93 + 1109*t2 + 23*Power(t2,2) - 254*Power(t2,3)) + 
               Power(t1,3)*(421 + 356*t2 + 267*Power(t2,2) - 
                  234*Power(t2,3)) + 
               2*Power(t1,4)*
                (-72 + 371*t2 + 228*Power(t2,2) + 9*Power(t2,3)) + 
               Power(t1,2)*(-314 - 1503*t2 - 600*Power(t2,2) + 
                  428*Power(t2,3)) + 
               Power(s1,2)*(-236 + 57*Power(t1,5) + 33*t2 + 
                  t1*(1505 + 198*t2) - Power(t1,4)*(705 + 547*t2) + 
                  2*Power(t1,3)*(1235 + 706*t2) - 
                  Power(t1,2)*(3091 + 1105*t2)) + 
               s1*(282 + 51*Power(t1,6) + 147*t2 - 100*Power(t2,2) + 
                  2*Power(t1,5)*(-143 + 80*t2) + 
                  Power(t1,2)*(2041 + 4228*t2 - 27*Power(t2,2)) + 
                  Power(t1,4)*(848 + 501*t2 + 318*Power(t2,2)) + 
                  t1*(-1229 - 1726*t2 + 324*Power(t2,2)) - 
                  Power(t1,3)*(1707 + 3310*t2 + 506*Power(t2,2)))) + 
            Power(s2,3)*(156 + 
               Power(s1,3)*(-4 + 104*t1 - 552*Power(t1,2) + 
                  996*Power(t1,3) - 636*Power(t1,4) + 79*Power(t1,5)) + 
               43*t2 - 214*Power(t2,2) + 43*Power(t2,3) + 
               Power(t1,7)*(62 + 70*t2) - 
               Power(t1,6)*(76 + 350*t2 + 123*Power(t2,2)) + 
               Power(t1,2)*(908 + 3292*t2 - 1184*Power(t2,2) - 
                  342*Power(t2,3)) + 
               Power(t1,4)*(484 + 1721*t2 + 299*Power(t2,2) - 
                  235*Power(t2,3)) - 
               t1*(606 + 991*t2 - 1030*Power(t2,2) + 17*Power(t2,3)) + 
               Power(t1,5)*(-170 + 244*t2 + 251*Power(t2,2) + 
                  22*Power(t2,3)) + 
               Power(t1,3)*(-758 - 4029*t2 - 59*Power(t2,2) + 
                  542*Power(t2,3)) + 
               Power(s1,2)*(137 + 55*Power(t1,6) + 
                  Power(t1,2)*(4542 - 172*t2) - 81*t2 - 
                  Power(t1,5)*(554 + 373*t2) + t1*(-1393 + 409*t2) - 
                  Power(t1,3)*(6095 + 1193*t2) + 
                  Power(t1,4)*(3308 + 1449*t2)) + 
               s1*(-425 + 88*Power(t1,7) + 311*t2 + 14*Power(t2,2) - 
                  2*Power(t1,6)*(189 + 2*t2) + 
                  Power(t1,3)*(5505 + 6250*t2 - 551*Power(t2,2)) + 
                  t1*(2552 - 523*t2 - 425*Power(t2,2)) + 
                  Power(t1,5)*(857 + 789*t2 + 207*Power(t2,2)) - 
                  Power(t1,4)*(2651 + 4449*t2 + 370*Power(t2,2)) + 
                  2*Power(t1,2)*(-2774 - 1187*t2 + 543*Power(t2,2)))) - 
            Power(s2,2)*(-246 + 
               Power(s1,3)*t1*
                (-12 + 186*t1 - 704*Power(t1,2) + 980*Power(t1,3) - 
                  463*Power(t1,4) + 7*Power(t1,5)) + 329*t2 - 
               38*Power(t2,2) - 45*Power(t2,3) + 
               23*Power(t1,8)*(2 + t2) - 
               Power(t1,7)*(94 + 92*t2 + 33*Power(t2,2)) + 
               Power(t1,2)*(-3427 + 1127*t2 + 1729*Power(t2,2) - 
                  455*Power(t2,3)) + 
               Power(t1,5)*(363 + 2015*t2 + 71*Power(t2,2) - 
                  99*Power(t2,3)) + 
               Power(t1,6)*(48 - 223*t2 + 81*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(t1,3)*(3699 + 1850*t2 - 2179*Power(t2,2) + 
                  106*Power(t2,3)) + 
               Power(t1,4)*(-1889 - 3684*t2 + 721*Power(t2,2) + 
                  209*Power(t2,3)) + 
               t1*(1500 - 1345*t2 - 352*Power(t2,2) + 281*Power(t2,3)) + 
               Power(s1,2)*(7*Power(t1,7) + 
                  Power(t1,3)*(6700 - 1188*t2) + t1*(519 - 351*t2) + 
                  24*(-1 + t2) - 2*Power(t1,4)*(3403 + 136*t2) - 
                  Power(t1,6)*(188 + 143*t2) + 
                  3*Power(t1,5)*(907 + 251*t2) + 
                  Power(t1,2)*(-2929 + 1195*t2)) + 
               s1*(218 + 63*Power(t1,8) - 294*t2 + 76*Power(t2,2) - 
                  2*Power(t1,7)*(103 + 26*t2) + 
                  Power(t1,4)*(8342 + 3306*t2 - 773*Power(t2,2)) + 
                  Power(t1,2)*(7253 - 4173*t2 - 242*Power(t2,2)) + 
                  t1*(-2142 + 2081*t2 - 239*Power(t2,2)) + 
                  Power(t1,6)*(324 + 547*t2 + 72*Power(t2,2)) - 
                  Power(t1,5)*(2591 + 2879*t2 + 77*Power(t2,2)) + 
                  Power(t1,3)*(-11261 + 1464*t2 + 1165*Power(t2,2))))) + 
         Power(s,6)*(16 - 434*Power(t1,8) + 
            2*Power(s2,6)*(20 - 54*t1 + 35*Power(t1,2)) - 60*t2 + 
            30*Power(t2,2) + 28*Power(t2,3) + 
            42*Power(t1,7)*(9 + 11*s1 + 7*t2) + 
            Power(t1,6)*(2287 - 28*Power(s1,2) + 133*t2 - 
               210*Power(t2,2) - 7*s1*(-35 + 82*t2)) + 
            Power(t1,3)*(-7275 + 166*Power(s1,3) + 3779*t2 - 
               1403*Power(t2,2) + 1766*Power(t2,3) + 
               Power(s1,2)*(-3389 + 311*t2) + 
               s1*(8205 - 3190*t2 - 248*Power(t2,2))) + 
            Power(t1,4)*(6425 - 161*Power(s1,3) + 
               Power(s1,2)*(3579 - 77*t2) + 602*t2 + 69*Power(t2,2) - 
               1029*Power(t2,3) - 3*s1*(1656 - 372*t2 + 91*Power(t2,2))) \
+ Power(t1,5)*(-4641 - 966*Power(s1,2) - 2050*t2 + 574*Power(t2,2) + 
               182*Power(t2,3) + 2*s1*(-433 + 469*t2 + 98*Power(t2,2))) \
+ t1*(-93 + 263*t2 - 405*Power(t2,2) + 210*Power(t2,3) - 
               2*s1*(4 - 95*t2 + 105*Power(t2,2))) + 
            Power(t1,2)*(3303 - 36*Power(s1,3) + 
               Power(s1,2)*(916 - 172*t2) - 2892*t2 + 
               1414*Power(t2,2) - 1152*Power(t2,3) + 
               s1*(-3131 + 1339*t2 + 499*Power(t2,2))) + 
            Power(s2,5)*(238 + 30*Power(s1,3) - 371*Power(t1,3) + 
               Power(t1,2)*(1182 - 9*t2) - 92*t2 - 280*Power(t2,2) - 
               252*Power(t2,3) - 10*Power(s1,2)*(1 + 9*t1 + 21*t2) + 
               2*t1*(-501 + 45*t2 + 70*Power(t2,2)) + 
               2*s1*(-3 - 67*Power(t1,2) + 90*t2 + 210*Power(t2,2) + 
                  35*t1*(2 + t2))) + 
            s2*(57 + 1505*Power(t1,7) - (333 + 52*s1)*t2 + 
               (637 + 50*s1)*Power(t2,2) - 315*Power(t2,3) - 
               Power(t1,6)*(200 + 1015*s1 + 1918*t2) + 
               Power(t1,5)*(-8569 - 63*Power(s1,2) + 2852*t2 + 
                  1169*Power(t2,2) + s1*(-2482 + 1855*t2)) + 
               Power(t1,3)*(-7940 + 408*Power(s1,3) - 2553*t2 + 
                  2613*Power(t2,2) + 4584*Power(t2,3) + 
                  Power(s1,2)*(-5962 + 46*t2) + 
                  s1*(583 + 566*t2 - 670*Power(t2,2))) + 
               Power(t1,4)*(11342 - 77*Power(s1,3) + 1439*t2 - 
                  3386*Power(t2,2) - 1176*Power(t2,3) + 
                  5*Power(s1,2)*(398 + 35*t2) + 
                  s1*(7937 - 3600*t2 - 238*Power(t2,2))) + 
               t1*(-2289 + 36*Power(s1,3) + 1982*t2 - 
                  2246*Power(t2,2) + 2496*Power(t2,3) + 
                  Power(s1,2)*(-894 + 220*t2) - 
                  2*s1*(-1244 + 501*t2 + 346*Power(t2,2))) + 
               Power(t1,2)*(6103 - 272*Power(s1,3) + 
                  Power(s1,2)*(4521 - 553*t2) - 1705*t2 + 
                  1144*Power(t2,2) - 5559*Power(t2,3) + 
                  s1*(-7170 + 2680*t2 + 1539*Power(t2,2)))) + 
            Power(s2,4)*(392 + Power(s1,3)*(43 - 130*t1) + 
               515*Power(t1,4) - 183*t2 - 924*Power(t2,2) - 
               1050*Power(t2,3) + 4*Power(t1,3)*(-915 + 259*t2) + 
               Power(t1,2)*(5140 - 2647*t2 - 1268*Power(t2,2)) + 
               t1*(-2522 + 1684*t2 + 2601*Power(t2,2) + 
                  1470*Power(t2,3)) + 
               Power(s1,2)*(-42 + 47*Power(t1,2) - 440*t2 + 
                  t1*(207 + 895*t2)) + 
               s1*(223 + 104*Power(t1,3) + 345*t2 + 1265*Power(t2,2) + 
                  Power(t1,2)*(851 + 221*t2) - 
                  t1*(1033 + 1176*t2 + 2030*Power(t2,2)))) + 
            Power(s2,2)*(204 - 1705*Power(t1,6) + 
               2*Power(s1,3)*
                (-3 + 41*t1 - 96*Power(t1,2) + 52*Power(t1,3)) - 
               78*t2 + 778*Power(t2,2) - 1193*Power(t2,3) + 
               Power(t1,5)*(-2431 + 3981*t2) + 
               Power(t1,4)*(13925 - 8675*t2 - 2642*Power(t2,2)) + 
               Power(t1,2)*(4058 + 488*t2 - 6287*Power(t2,2) - 
                  7128*Power(t2,3)) + 
               Power(t1,3)*(-13067 + 5112*t2 + 7271*Power(t2,2) + 
                  2760*Power(t2,3)) + 
               t1*(-849 - 749*t2 + 594*Power(t2,2) + 
                  5376*Power(t2,3)) + 
               Power(s1,2)*(160 + 83*Power(t1,4) + 
                  Power(t1,2)*(2295 - 718*t2) - 55*t2 + 
                  Power(t1,3)*(-999 + 42*t2) + t1*(-1339 + 532*t2)) + 
               s1*(-246 + 500*Power(t1,5) + 
                  Power(t1,4)*(5188 - 1708*t2) - 107*t2 + 
                  405*Power(t2,2) + 
                  t1*(67 - 264*t2 - 2580*Power(t2,2)) + 
                  Power(t1,3)*(-12773 + 2836*t2 - 1226*Power(t2,2)) + 
                  Power(t1,2)*(6886 - 570*t2 + 3774*Power(t2,2)))) + 
            Power(s2,3)*(51 + 420*Power(t1,5) + 
               Power(s1,3)*(10 - 94*t1 + 69*Power(t1,2)) + 
               Power(t1,4)*(4831 - 3378*t2) + 253*t2 - 460*Power(t2,2) - 
               1641*Power(t2,3) + 
               Power(t1,3)*(-11763 + 8209*t2 + 2811*Power(t2,2)) - 
               Power(t1,2)*(-8356 + 5925*t2 + 6799*Power(t2,2) + 
                  2981*Power(t2,3)) + 
               t1*(-1855 + 934*t2 + 4569*Power(t2,2) + 
                  4628*Power(t2,3)) + 
               Power(s1,2)*(42 + 43*Power(t1,3) - 248*t2 - 
                  3*Power(t1,2)*(75 + 298*t2) + 2*t1*(94 + 599*t2)) + 
               s1*(400 + 79*Power(t1,4) - 102*t2 + 1203*Power(t2,2) + 
                  Power(t1,3)*(-3951 + 146*t2) + 
                  Power(t1,2)*(6865 + 788*t2 + 2876*Power(t2,2)) - 
                  t1*(3460 + 1013*t2 + 4124*Power(t2,2))))) + 
         Power(s,2)*(2*Power(s2,9)*(s1 - t2)*
             (-3 + Power(s1,2) - 3*Power(t1,2) + s1*(-1 + t1 - 2*t2) - 
               t1*(-6 + t2) + t2 + Power(t2,2)) + 
            Power(s2,8)*(-12 + Power(s1,3)*(31 - 42*t1) - 
               4*Power(t1,4) - 73*t2 - 126*Power(t2,2) - 
               46*Power(t2,3) + Power(t1,3)*(24 + 5*t2) - 
               Power(t1,2)*(48 + 83*t2 + 149*Power(t2,2)) + 
               t1*(40 + 151*t2 + 275*Power(t2,2) + 57*Power(t2,3)) + 
               Power(s1,2)*(-125*Power(t1,2) - 6*(17 + 18*t2) + 
                  t1*(227 + 141*t2)) + 
               s1*(65 + 3*Power(t1,3) + 228*t2 + 123*Power(t2,2) + 
                  Power(t1,2)*(59 + 274*t2) - 
                  t1*(127 + 502*t2 + 156*Power(t2,2)))) - 
            Power(-1 + t1,2)*
             (3*Power(t1,10) - 4*Power(-1 + t2,3) - 
               Power(t1,9)*(38 + 33*s1 + t2) + 
               t1*Power(-1 + t2,2)*(-71 + 8*s1 + 83*t2) + 
               Power(t1,8)*(36 + 22*Power(s1,2) - 35*t2 + 
                  3*Power(t2,2) + s1*(7 + 37*t2)) + 
               Power(t1,2)*(-1 + t2)*
                (439 + 14*Power(s1,2) - 41*t2 - 244*Power(t2,2) + 
                  s1*(-181 + 63*t2)) + 
               Power(t1,7)*(33 + 46*Power(s1,2) + 243*t2 - 
                  8*Power(t2,2) - Power(t2,3) + 
                  s1*(5 - 74*t2 - 10*Power(t2,2))) + 
               Power(t1,6)*(-855 + 147*Power(s1,3) - 5*t2 - 
                  136*Power(t2,2) + 5*Power(t2,3) + 
                  Power(s1,2)*(-1199 + 63*t2) + 
                  s1*(2067 - 361*t2 + 7*Power(t2,2))) + 
               Power(t1,5)*(3320 - 180*Power(s1,3) + 
                  Power(s1,2)*(2199 - 591*t2) - 2740*t2 + 
                  461*Power(t2,2) + 69*Power(t2,3) + 
                  s1*(-5472 + 2766*t2 + 20*Power(t2,2))) + 
               Power(t1,3)*(2748 - 2*Power(s1,3) - 3737*t2 + 
                  664*Power(t2,2) + 377*Power(t2,3) - 
                  5*Power(s1,2)*(-57 + 43*t2) - 
                  2*s1*(931 - 1023*t2 + 177*Power(t2,2))) + 
               Power(t1,4)*(-4741 + 53*Power(s1,3) + 5582*t2 - 
                  962*Power(t2,2) - 303*Power(t2,3) + 
                  Power(s1,2)*(-1339 + 675*t2) + 
                  s1*(5099 - 4154*t2 + 320*Power(t2,2)))) - 
            Power(s2,5)*(67 - 148*Power(t1,7) + 
               Power(s1,3)*(105 - 746*t1 + 1596*Power(t1,2) - 
                  1448*Power(t1,3) + 515*Power(t1,4)) - 477*t2 + 
               48*Power(t2,2) + 196*Power(t2,3) + 
               Power(t1,6)*(1020 + 1051*t2) - 
               Power(t1,5)*(2227 + 6352*t2 + 2773*Power(t2,2)) + 
               Power(t1,4)*(1937 + 12880*t2 + 7962*Power(t2,2) + 
                  816*Power(t2,3)) - 
               t1*(97 - 1121*t2 + 365*Power(t2,2) + 1254*Power(t2,3)) - 
               Power(t1,3)*(496 + 10741*t2 + 7912*Power(t2,2) + 
                  2624*Power(t2,3)) + 
               Power(t1,2)*(-56 + 2518*t2 + 3040*Power(t2,2) + 
                  2844*Power(t2,3)) + 
               Power(s1,2)*(-638 - 827*Power(t1,5) + 
                  Power(t1,4)*(422 - 975*t2) - 133*t2 + 
                  t1*(3599 + 1200*t2) - 
                  2*Power(t1,2)*(3239 + 1338*t2) + 
                  Power(t1,3)*(3922 + 2518*t2)) + 
               s1*(350 + 253*Power(t1,6) + 800*t2 - 126*Power(t2,2) + 
                  Power(t1,5)*(148 + 3113*t2) + 
                  Power(t1,2)*(692 + 7216*t2 - 1560*Power(t2,2)) - 
                  3*Power(t1,4)*(553 + 2012*t2 + 110*Power(t2,2)) + 
                  3*Power(t1,3)*(463 - 114*t2 + 478*Power(t2,2)) + 
                  t1*(-1173 - 4751*t2 + 648*Power(t2,2)))) + 
            Power(s2,7)*(-45 + 26*Power(t1,5) + 
               Power(s1,3)*(-1 - 74*t1 + 91*Power(t1,2)) - 234*t2 - 
               239*Power(t2,2) - 109*Power(t2,3) - 
               Power(t1,4)*(185 + 158*t2) + 
               Power(t1,3)*(444 + 957*t2 + 817*Power(t2,2)) - 
               Power(t1,2)*(482 + 1674*t2 + 1775*Power(t2,2) + 
                  259*Power(t2,3)) + 
               t1*(242 + 1109*t2 + 1197*Power(t2,2) + 
                  352*Power(t2,3)) + 
               Power(s1,2)*(-55 + 561*Power(t1,3) - 80*t2 - 
                  Power(t1,2)*(1079 + 414*t2) + t1*(573 + 446*t2)) + 
               s1*(135 + 53*Power(t1,4) + 276*t2 + 190*Power(t2,2) - 
                  Power(t1,3)*(543 + 1360*t2) + 
                  2*Power(t1,2)*(531 + 1400*t2 + 291*Power(t2,2)) - 
                  t1*(707 + 1716*t2 + 724*Power(t2,2)))) - 
            Power(s2,3)*(-825 - 154*Power(t1,9) + 
               Power(s1,3)*(2 - 174*t1 + 1602*Power(t1,2) - 
                  4830*Power(t1,3) + 6071*Power(t1,4) - 
                  3078*Power(t1,5) + 403*Power(t1,6)) + 798*t2 + 
               97*Power(t2,2) - 122*Power(t2,3) + 
               5*Power(t1,8)*(205 + 148*t2) - 
               Power(t1,7)*(1608 + 4053*t2 + 1199*Power(t2,2)) + 
               Power(t1,3)*(7700 + 20316*t2 - 7082*Power(t2,2) - 
                  4062*Power(t2,3)) + 
               Power(t1,5)*(171 + 5289*t2 - 5304*Power(t2,2) - 
                  2584*Power(t2,3)) + 
               t1*(4651 - 1974*t2 - 1889*Power(t2,2) + 
                  358*Power(t2,3)) + 
               Power(t1,6)*(593 + 5961*t2 + 4441*Power(t2,2) + 
                  437*Power(t2,3)) + 
               Power(t1,2)*(-9245 - 4787*t2 + 6054*Power(t2,2) + 
                  856*Power(t2,3)) + 
               Power(t1,4)*(-2308 - 22290*t2 + 4882*Power(t2,2) + 
                  5121*Power(t2,3)) + 
               Power(s1,2)*(25*Power(t1,7) + 5*(-41 + 27*t2) - 
                  6*t1*(-577 + 220*t2) + 
                  Power(t1,3)*(38086 + 480*t2) + 
                  6*Power(t1,2)*(-2919 + 485*t2) - 
                  9*Power(t1,4)*(4318 + 775*t2) - 
                  Power(t1,6)*(2225 + 1584*t2) + 
                  Power(t1,5)*(17233 + 6366*t2)) + 
               s1*(1015 + 973*Power(t1,8) - 964*t2 + 119*Power(t2,2) - 
                  Power(t1,7)*(3173 + 46*t2) + 
                  Power(t1,2)*(30816 - 4880*t2 - 2670*Power(t2,2)) + 
                  Power(t1,4)*(31484 + 34796*t2 - 2327*Power(t2,2)) + 
                  4*t1*(-2373 + 1369*t2 + 32*Power(t2,2)) + 
                  Power(t1,6)*(3290 + 4212*t2 + 694*Power(t2,2)) + 
                  6*Power(t1,3)*(-7598 - 2721*t2 + 846*Power(t2,2)) - 
                  Power(t1,5)*(9325 + 22268*t2 + 1032*Power(t2,2)))) - 
            s2*(-1 + t1)*(-24*Power(t1,10) + 
               Power(s1,3)*Power(t1,2)*
                (-6 + 204*t1 - 993*Power(t1,2) + 1591*Power(t1,3) - 
                  821*Power(t1,4) + 7*Power(t1,5)) + 
               Power(t1,9)*(226 + 41*t2) + 
               Power(-1 + t2,2)*(-53 + 65*t2) + 
               Power(t1,8)*(-327 + 31*t2 - 57*Power(t2,2)) + 
               t1*(-833 + 963*t2 + 265*Power(t2,2) - 395*Power(t2,3)) + 
               Power(t1,6)*(2080 + 3751*t2 + 8*Power(t2,2) - 
                  170*Power(t2,3)) + 
               Power(t1,7)*(169 - 1426*t2 + 227*Power(t2,2) + 
                  22*Power(t2,3)) + 
               Power(t1,5)*(-11122 + 1938*t2 + 100*Power(t2,2) + 
                  95*Power(t2,3)) + 
               3*Power(t1,4)*
                (6957 - 5413*t2 + 109*Power(t2,2) + 325*Power(t2,3)) - 
               2*Power(t1,3)*
                (8954 - 9814*t2 + 799*Power(t2,2) + 878*Power(t2,3)) + 
               Power(t1,2)*(6921 - 8858*t2 + 911*Power(t2,2) + 
                  1182*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-75*Power(t1,7) + Power(t1,3)*(13481 - 4016*t2) + 
                  t1*(803 - 593*t2) + 28*(-1 + t2) - 
                  Power(t1,6)*(257 + 117*t2) + 
                  Power(t1,5)*(6185 + 397*t2) + 
                  2*Power(t1,4)*(-7401 + 848*t2) + 
                  Power(t1,2)*(-5307 + 2659*t2)) + 
               s1*(229*Power(t1,9) + 8*Power(-1 + t2,2) - 
                  Power(t1,8)*(405 + 239*t2) + 
                  Power(t1,2)*(-5075 + 5492*t2 - 927*Power(t2,2)) + 
                  Power(t1,5)*(26738 - 5333*t2 - 851*Power(t2,2)) + 
                  4*t1*(90 - 121*t2 + 31*Power(t2,2)) - 
                  Power(t1,6)*(7739 + 2225*t2 + 54*Power(t2,2)) + 
                  Power(t1,7)*(76 + 1125*t2 + 96*Power(t2,2)) + 
                  Power(t1,4)*(-34249 + 17948*t2 + 273*Power(t2,2)) + 
                  Power(t1,3)*(20057 - 16268*t2 + 1277*Power(t2,2)))) + 
            Power(s2,6)*(25 - 79*Power(t1,6) + 
               Power(s1,3)*(-85 + 272*t1 - 276*Power(t1,2) + 
                  92*Power(t1,3)) - 132*t2 - 98*Power(t2,2) - 
               166*Power(t2,3) + Power(t1,5)*(579 + 571*t2) - 
               Power(t1,4)*(1415 + 3405*t2 + 2002*Power(t2,2)) + 
               Power(t1,3)*(1536 + 6525*t2 + 5029*Power(t2,2) + 
                  582*Power(t2,3)) + 
               t1*(77 + 1692*t2 + 1388*Power(t2,2) + 835*Power(t2,3)) - 
               Power(t1,2)*(723 + 5251*t2 + 4317*Power(t2,2) + 
                  1254*Power(t2,3)) + 
               Power(s1,2)*(289 - 1005*Power(t1,4) + 138*t2 - 
                  Power(t1,2)*(305 + 108*t2) + 
                  Power(t1,3)*(1721 + 168*t2) - t1*(700 + 207*t2)) - 
               s1*(17 + 56*Power(t1,5) + 330*t2 - 105*Power(t2,2) - 
                  2*Power(t1,4)*(538 + 1433*t2) + 
                  Power(t1,3)*(2479 + 6188*t2 + 834*Power(t2,2)) + 
                  t1*(461 + 130*t2 + 876*Power(t2,2)) - 
                  Power(t1,2)*(1937 + 3782*t2 + 1614*Power(t2,2)))) + 
            Power(s2,4)*(176 - 185*Power(t1,8) + 
               Power(s1,3)*(-39 + 648*t1 - 2632*Power(t1,2) + 
                  4156*Power(t1,3) - 2800*Power(t1,4) + 678*Power(t1,5)\
) + 476*t2 - 391*Power(t2,2) - 55*Power(t2,3) + 
               2*Power(t1,7)*(597 + 572*t2) - 
               Power(t1,6)*(2119 + 6808*t2 + 2332*Power(t2,2)) + 
               Power(t1,5)*(1044 + 13378*t2 + 7597*Power(t2,2) + 
                  752*Power(t2,3)) + 
               t1*(-256 - 4333*t2 + 1596*Power(t2,2) + 
                  957*Power(t2,3)) - 
               Power(t1,4)*(-145 + 7575*t2 + 8277*Power(t2,2) + 
                  3373*Power(t2,3)) - 
               Power(t1,2)*(289 - 10571*t2 + 2309*Power(t2,2) + 
                  3514*Power(t2,3)) + 
               Power(t1,3)*(290 - 6853*t2 + 4116*Power(t2,2) + 
                  5222*Power(t2,3)) + 
               Power(s1,2)*(599 - 237*Power(t1,6) - 153*t2 + 
                  t1*(-5482 + 74*t2) - 9*Power(t1,5)*(201 + 211*t2) - 
                  20*Power(t1,3)*(1088 + 341*t2) + 
                  2*Power(t1,2)*(8329 + 1350*t2) + 
                  Power(t1,4)*(12031 + 6065*t2)) + 
               s1*(-1134 + 776*Power(t1,7) + 248*t2 + 125*Power(t2,2) + 
                  Power(t1,6)*(-2134 + 1589*t2) + 
                  Power(t1,3)*(9721 + 25944*t2 - 1830*Power(t2,2)) + 
                  t1*(6248 + 2876*t2 - 1116*Power(t2,2)) - 
                  Power(t1,4)*(3037 + 13754*t2 + 104*Power(t2,2)) + 
                  Power(t1,5)*(1663 - 614*t2 + 474*Power(t2,2)) + 
                  Power(t1,2)*(-12103 - 16289*t2 + 2484*Power(t2,2)))) + 
            Power(s2,2)*(429 - 81*Power(t1,10) + 
               Power(s1,3)*t1*
                (6 - 288*t1 + 1968*Power(t1,2) - 4883*Power(t1,3) + 
                  5178*Power(t1,4) - 2088*Power(t1,5) + 104*Power(t1,6)) \
- 600*t2 + 67*Power(t2,2) + 104*Power(t2,3) + 
               Power(t1,9)*(639 + 263*t2) - 
               Power(t1,8)*(1135 + 1097*t2 + 362*Power(t2,2)) + 
               Power(t1,3)*(-32165 + 17337*t2 + 5588*Power(t2,2) - 
                  1570*Power(t2,3)) + 
               Power(t1,4)*(27630 + 3860*t2 - 6815*Power(t2,2) - 
                  1198*Power(t2,3)) + 
               Power(t1,6)*(1049 + 10989*t2 - 2029*Power(t2,2) - 
                  1072*Power(t2,3)) + 
               Power(t1,7)*(811 - 690*t2 + 1549*Power(t2,2) + 
                  144*Power(t2,3)) - 
               t1*(4972 - 5991*t2 + 372*Power(t2,2) + 803*Power(t2,3)) + 
               2*Power(t1,2)*
                (9310 - 8688*t2 - 550*Power(t2,2) + 1029*Power(t2,3)) + 
               Power(t1,5)*(-10825 - 18677*t2 + 3474*Power(t2,2) + 
                  2340*Power(t2,3)) - 
               Power(s1,2)*(51*Power(t1,8) + 
                  Power(t1,3)*(26296 - 7222*t2) + t1*(723 - 513*t2) + 
                  14*(-1 + t2) + 150*Power(t1,2)*(-47 + 22*t2) + 
                  3*Power(t1,7)*(347 + 222*t2) + 
                  5*Power(t1,4)*(-9217 + 990*t2) + 
                  Power(t1,5)*(39169 + 1962*t2) - 
                  Power(t1,6)*(14131 + 3166*t2)) + 
               s1*(-187 + 652*Power(t1,9) + 256*t2 - 69*Power(t2,2) - 
                  4*Power(t1,8)*(514 + 125*t2) + 
                  Power(t1,5)*(47657 + 16228*t2 - 2172*Power(t2,2)) - 
                  6*Power(t1,2)*(4081 - 3083*t2 + 185*Power(t2,2)) + 
                  Power(t1,7)*(1957 + 3892*t2 + 378*Power(t2,2)) - 
                  Power(t1,6)*(11945 + 14676*t2 + 630*Power(t2,2)) + 
                  t1*(4234 - 4422*t2 + 698*Power(t2,2)) - 
                  2*Power(t1,3)*(-30846 + 14026*t2 + 825*Power(t2,2)) + 
                  Power(t1,4)*(-77518 + 8776*t2 + 4546*Power(t2,2))))) + 
         Power(s,3)*(-(Power(s2,8)*
               (10 + 12*Power(s1,3) + 2*Power(t1,3) + 
                 2*Power(s1,2)*(-5 + 3*t1 - 21*t2) + 23*t2 - 
                 20*Power(t2,2) - 18*Power(t2,3) + 
                 Power(t1,2)*(6 + 39*t2) + 
                 2*t1*(-9 - 31*t2 + 8*Power(t2,2)) + 
                 s1*(-27 - 43*Power(t1,2) + t1*(70 - 22*t2) + 30*t2 + 
                    48*Power(t2,2)))) - 
            (-1 + t1)*(30*Power(t1,10) - 
               6*Power(t1,9)*(26 + 25*s1 + 2*t2) - 
               2*Power(-1 + t2,2)*(-7 + 13*t2) + 
               t1*(-1 + t2)*(187 - 455*t2 + 286*Power(t2,2) + 
                  s1*(-50 + 54*t2)) + 
               Power(t1,8)*(28 + 68*Power(s1,2) - 149*t2 + 
                  24*Power(t2,2) + s1*(173 + 152*t2)) + 
               Power(t1,3)*(8342 - 31*Power(s1,3) + 
                  Power(s1,2)*(1377 - 736*t2) - 9260*t2 + 
                  494*Power(t2,2) + 1076*Power(t2,3) + 
                  s1*(-6661 + 5340*t2 - 186*Power(t2,2))) + 
               Power(t1,2)*(-1526 + 1205*t2 + 1074*Power(t2,2) - 
                  825*Power(t2,3) + Power(s1,2)*(-121 + 91*t2) + 
                  s1*(824 - 685*t2 - 39*Power(t2,2))) + 
               Power(t1,5)*(9755 - 499*Power(s1,3) + 
                  Power(s1,2)*(6377 - 1152*t2) - 6394*t2 + 
                  1069*Power(t2,2) - 20*Power(t2,3) + 
                  34*s1*(-461 + 187*t2 + 5*Power(t2,2))) + 
               Power(t1,4)*(-13820 + 241*Power(s1,3) + 13583*t2 - 
                  1664*Power(t2,2) - 576*Power(t2,3) + 
                  Power(s1,2)*(-4717 + 1628*t2) + 
                  s1*(15897 - 9854*t2 + 20*Power(t2,2))) + 
               Power(t1,7)*(490 + 194*Power(s1,2) + 1008*t2 - 
                  98*Power(t2,2) - 10*Power(t2,3) - 
                  s1*(189 + 374*t2 + 44*Power(t2,2))) + 
               Power(t1,6)*(-2970 + 301*Power(s1,3) - 569*t2 - 
                  224*Power(t2,2) + 83*Power(t2,3) + 
                  7*Power(s1,2)*(-454 + 19*t2) + 
                  s1*(5730 - 833*t2 + 61*Power(t2,2)))) + 
            Power(s2,7)*(27 + 39*Power(t1,4) + 
               Power(s1,3)*(-82 + 139*t1) + 282*t2 + 415*Power(t2,2) + 
               198*Power(t2,3) + Power(t1,3)*(-86 + 52*t2) + 
               Power(t1,2)*(82 + 315*t2 + 541*Power(t2,2)) - 
               t1*(62 + 649*t2 + 971*Power(t2,2) + 276*Power(t2,3)) + 
               Power(s1,2)*(231 + 331*Power(t1,2) + 341*t2 - 
                  t1*(577 + 533*t2)) - 
               s1*(190 + 150*Power(t1,3) + 624*t2 + 457*Power(t2,2) + 
                  Power(t1,2)*(27 + 850*t2) - 
                  t1*(367 + 1504*t2 + 670*Power(t2,2)))) + 
            s2*(90 + 205*Power(t1,10) + 
               Power(s1,3)*Power(t1,2)*
                (89 - 915*t1 + 2856*Power(t1,2) - 3520*Power(t1,3) + 
                  1545*Power(t1,4) - 49*Power(t1,5)) - 339*t2 + 
               426*Power(t2,2) - 177*Power(t2,3) - 
               Power(t1,9)*(1009 + 236*t2) + 
               Power(t1,8)*(853 + 244*t2 + 289*Power(t2,2)) + 
               Power(t1,2)*(-18278 + 18909*t2 + 127*Power(t2,2) - 
                  2502*Power(t2,3)) + 
               Power(t1,5)*(26575 + 1275*t2 - 1110*Power(t2,2) - 
                  2223*Power(t2,3)) + 
               Power(t1,7)*(1085 + 3871*t2 - 1387*Power(t2,2) - 
                  144*Power(t2,3)) + 
               Power(t1,4)*(-49283 + 28850*t2 - 169*Power(t2,2) + 
                  775*Power(t2,3)) + 
               t1*(2606 - 2429*t2 - 1110*Power(t2,2) + 
                  1077*Power(t2,3)) + 
               Power(t1,6)*(-6883 - 11220*t2 + 1821*Power(t2,2) + 
                  1086*Power(t2,3)) + 
               Power(t1,3)*(44039 - 38925*t2 + 1113*Power(t2,2) + 
                  2102*Power(t2,3)) + 
               Power(s1,2)*t1*
                (218 + 189*Power(t1,7) + 
                  Power(t1,2)*(17495 - 5843*t2) + 
                  Power(t1,4)*(35576 - 2090*t2) - 158*t2 + 
                  Power(t1,6)*(929 + 287*t2) - 
                  Power(t1,5)*(14174 + 949*t2) + 
                  t1*(-3582 + 1807*t2) + Power(t1,3)*(-36651 + 6928*t2)\
) + s1*(-30 - 863*Power(t1,9) + 64*t2 - 34*Power(t2,2) + 
                  Power(t1,8)*(1987 + 839*t2) + 
                  Power(t1,4)*(84782 - 31605*t2 - 2514*Power(t2,2)) + 
                  t1*(-1513 + 1372*t2 - 59*Power(t2,2)) - 
                  Power(t1,7)*(959 + 3897*t2 + 302*Power(t2,2)) + 
                  Power(t1,6)*(16213 + 6853*t2 + 418*Power(t2,2)) + 
                  Power(t1,3)*(-55149 + 32305*t2 + 436*Power(t2,2)) + 
                  Power(t1,2)*(16172 - 12555*t2 + 504*Power(t2,2)) + 
                  Power(t1,5)*(-60640 + 6624*t2 + 1569*Power(t2,2)))) + 
            Power(s2,6)*(28 - 200*Power(t1,5) + 
               Power(s1,3)*(-37 + 256*t1 - 276*Power(t1,2)) + 697*t2 + 
               839*Power(t2,2) + 494*Power(t2,3) + 
               Power(t1,4)*(653 + 441*t2) - 
               Power(t1,3)*(921 + 3483*t2 + 2479*Power(t2,2)) + 
               Power(t1,2)*(711 + 6002*t2 + 5441*Power(t2,2) + 
                  1114*Power(t2,3)) - 
               t1*(271 + 3657*t2 + 3816*Power(t2,2) + 
                  1526*Power(t2,3)) + 
               Power(s1,2)*(158 - 1254*Power(t1,3) + 362*t2 + 
                  6*Power(t1,2)*(382 + 233*t2) - t1*(1211 + 1564*t2)) + 
               s1*(-388 + 278*Power(t1,4) - 768*t2 - 801*Power(t2,2) + 
                  Power(t1,3)*(947 + 3476*t2) - 
                  2*Power(t1,2)*(1389 + 3495*t2 + 1109*Power(t2,2)) + 
                  t1*(1941 + 4312*t2 + 2798*Power(t2,2)))) + 
            Power(s2,2)*(-1076 - 584*Power(t1,9) + 
               Power(s1,3)*t1*
                (-85 + 1143*t1 - 4285*Power(t1,2) + 6182*Power(t1,3) - 
                  3300*Power(t1,4) + 316*Power(t1,5)) + 1239*t2 - 
               24*Power(t2,2) - 211*Power(t2,3) + 
               Power(t1,8)*(2381 + 1143*t2) - 
               Power(t1,7)*(1652 + 4329*t2 + 1411*Power(t2,2)) + 
               Power(t1,3)*(30310 + 251*t2 - 6851*Power(t2,2) - 
                  5695*Power(t2,3)) + 
               Power(t1,5)*(3205 + 15901*t2 - 8806*Power(t2,2) - 
                  4218*Power(t2,3)) + 
               t1*(9529 - 8893*t2 - 37*Power(t2,2) + 717*Power(t2,3)) + 
               Power(t1,6)*(-1452 + 1338*t2 + 5891*Power(t2,2) + 
                  726*Power(t2,3)) + 
               Power(t1,2)*(-26322 + 15783*t2 + 2507*Power(t2,2) + 
                  753*Power(t2,3)) + 
               Power(t1,4)*(-14339 - 22433*t2 + 8760*Power(t2,2) + 
                  7950*Power(t2,3)) - 
               Power(s1,2)*(97 + 146*Power(t1,7) + 
                  Power(t1,2)*(17853 - 4651*t2) - 67*t2 + 
                  4*Power(t1,4)*(12903 + 415*t2) - 
                  8*Power(t1,5)*(3003 + 563*t2) + 
                  2*Power(t1,6)*(1261 + 651*t2) + 
                  t1*(-2744 + 1207*t2) + Power(t1,3)*(-45491 + 4973*t2)\
) + s1*(683 + 2020*Power(t1,8) - 679*t2 + 96*Power(t2,2) - 
                  35*Power(t1,7)*(133 + 40*t2) + 
                  Power(t1,4)*(58714 + 18374*t2 - 4302*Power(t2,2)) + 
                  Power(t1,2)*(45248 - 20163*t2 - 1557*Power(t2,2)) + 
                  t1*(-10669 + 7593*t2 - 345*Power(t2,2)) - 
                  9*Power(t1,5)*(1383 + 2614*t2 + 22*Power(t2,2)) + 
                  Power(t1,6)*(197 + 8502*t2 + 680*Power(t2,2)) + 
                  Power(t1,3)*(-79091 + 11241*t2 + 5553*Power(t2,2)))) + 
            Power(s2,5)*(101 + 535*Power(t1,6) + 
               Power(s1,3)*(121 - 325*t1 + 219*Power(t1,2) - 
                  55*Power(t1,3)) + 59*t2 + 530*Power(t2,2) + 
               648*Power(t2,3) - Power(t1,5)*(1853 + 1752*t2) + 
               Power(t1,4)*(2350 + 10614*t2 + 5059*Power(t2,2)) - 
               Power(t1,3)*(1263 + 19172*t2 + 13273*Power(t2,2) + 
                  2230*Power(t2,3)) - 
               t1*(184 + 3620*t2 + 4741*Power(t2,2) + 
                  3216*Power(t2,3)) + 
               Power(t1,2)*(314 + 13871*t2 + 12491*Power(t2,2) + 
                  4836*Power(t2,3)) + 
               Power(s1,2)*(-496 + 1823*Power(t1,4) + 
                  t1*(1153 - 511*t2) - 89*t2 - 
                  25*Power(t1,3)*(119 + 39*t2) + 
                  Power(t1,2)*(561 + 1693*t2)) + 
               s1*(-234 - 695*Power(t1,5) + 566*t2 - 550*Power(t2,2) - 
                  Power(t1,4)*(1323 + 5753*t2) + 
                  Power(t1,3)*(6430 + 12125*t2 + 3116*Power(t2,2)) + 
                  t1*(2469 + 523*t2 + 3648*Power(t2,2)) - 
                  Power(t1,2)*(6647 + 7593*t2 + 6330*Power(t2,2)))) + 
            Power(s2,4)*(162 - 880*Power(t1,7) + 
               Power(s1,3)*(129 - 1035*t1 + 2303*Power(t1,2) - 
                  1991*Power(t1,3) + 660*Power(t1,4)) - 946*t2 + 
               269*Power(t2,2) + 507*Power(t2,3) + 
               Power(t1,6)*(3042 + 2875*t2) - 
               Power(t1,5)*(2853 + 15779*t2 + 5670*Power(t2,2)) - 
               3*t1*(444 - 1310*t2 + 871*Power(t2,2) + 
                  1213*Power(t2,3)) + 
               Power(t1,4)*(51 + 28614*t2 + 17506*Power(t2,2) + 
                  2600*Power(t2,3)) - 
               Power(t1,3)*(319 + 19355*t2 + 20133*Power(t2,2) + 
                  8195*Power(t2,3)) + 
               Power(t1,2)*(2129 + 661*t2 + 10623*Power(t2,2) + 
                  8637*Power(t2,3)) + 
               Power(s1,2)*(-1067 - 1150*Power(t1,5) + 
                  Power(t1,4)*(290 - 1050*t2) - 21*t2 + 
                  t1*(6433 + 751*t2) - Power(t1,2)*(11903 + 2051*t2) + 
                  Power(t1,3)*(7379 + 2149*t2)) + 
               s1*(1052 + 1685*Power(t1,6) + 645*t2 - 275*Power(t2,2) + 
                  Power(t1,5)*(-1387 + 4190*t2) + 
                  Power(t1,2)*(-2497 + 11865*t2 - 6573*Power(t2,2)) - 
                  2*Power(t1,4)*(3382 + 3430*t2 + 905*Power(t2,2)) + 
                  t1*(-2572 - 6045*t2 + 2455*Power(t2,2)) + 
                  Power(t1,3)*(10483 - 3759*t2 + 6449*Power(t2,2)))) + 
            Power(s2,3)*(-1151 + 917*Power(t1,8) + 
               Power(s1,3)*(27 - 629*t1 + 3083*Power(t1,2) - 
                  5403*Power(t1,3) + 3654*Power(t1,4) - 723*Power(t1,5)) \
+ 687*t2 + 110*Power(t2,2) + 58*Power(t2,3) - 
               4*Power(t1,7)*(827 + 624*t2) + 
               3*Power(t1,6)*(769 + 4073*t2 + 1237*Power(t2,2)) + 
               t1*(4131 + 1537*t2 - 1859*Power(t2,2) - 
                  1722*Power(t2,3)) - 
               Power(t1,5)*(-1402 + 18824*t2 + 13349*Power(t2,2) + 
                  1818*Power(t2,3)) + 
               Power(t1,2)*(-3262 - 13368*t2 + 6553*Power(t2,2) + 
                  7318*Power(t2,3)) + 
               Power(t1,4)*(469 + 2597*t2 + 17904*Power(t2,2) + 
                  7912*Power(t2,3)) - 
               Power(t1,3)*(1505 - 17648*t2 + 13121*Power(t2,2) + 
                  11734*Power(t2,3)) + 
               Power(s1,2)*(-660 + 281*Power(t1,6) + 
                  t1*(7519 - 1151*t2) + 227*t2 + 
                  Power(t1,2)*(-25871 + 163*t2) + 
                  3*Power(t1,5)*(893 + 711*t2) + 
                  Power(t1,3)*(36183 + 4665*t2) - 
                  Power(t1,4)*(20182 + 6041*t2)) + 
               s1*(2026 - 2468*Power(t1,7) + 
                  Power(t1,6)*(4851 - 372*t2) - 1151*t2 + 
                  32*Power(t2,2) + 
                  Power(t1,2)*(28670 + 9663*t2 - 5110*Power(t2,2)) + 
                  Power(t1,4)*(-3434 + 23878*t2 - 2575*Power(t2,2)) - 
                  2*Power(t1,5)*(-1559 + 2440*t2 + 66*Power(t2,2)) + 
                  t1*(-13705 + 2399*t2 + 1194*Power(t2,2)) + 
                  Power(t1,3)*(-19058 - 29435*t2 + 6572*Power(t2,2))))) - 
         Power(s,5)*(4 + 304*Power(t1,9) + 
            2*Power(s2,7)*(7 - 16*t1 + 9*Power(t1,2)) + 30*t2 - 
            114*Power(t2,2) + 84*Power(t2,3) - 
            14*Power(t1,8)*(46 + 39*s1 + 12*t2) + 
            Power(t1,7)*(-1108 + 98*Power(s1,2) - 315*t2 + 
               168*Power(t2,2) + 7*s1*(71 + 80*t2)) + 
            Power(t1,6)*(4130 + 910*Power(s1,2) + 2829*t2 - 
               616*Power(t2,2) - 112*Power(t2,3) - 
               2*s1*(90 + 637*t2 + 91*Power(t2,2))) + 
            t1*(54 - 332*t2 + 531*Power(t2,2) - 308*Power(t2,3) - 
               2*s1*(51 - 172*t2 + 119*Power(t2,2))) + 
            Power(t1,5)*(-8842 + 315*Power(s1,3) - 1963*t2 + 
               174*Power(t2,2) + 791*Power(t2,3) + 
               3*Power(s1,2)*(-1894 + 49*t2) + 
               s1*(9189 - 1159*t2 + 315*Power(t2,2))) + 
            Power(t1,4)*(15696 - 492*Power(s1,3) + 
               Power(s1,2)*(8495 - 879*t2) - 7045*t2 + 
               1409*Power(t2,2) - 1615*Power(t2,3) + 
               s1*(-20415 + 6911*t2 + 361*Power(t2,2))) + 
            Power(t1,3)*(-13874 + 218*Power(s1,3) + 10604*t2 - 
               1783*Power(t2,2) + 1142*Power(t2,3) + 
               Power(s1,2)*(-4689 + 966*t2) - 
               2*s1*(-7501 + 3360*t2 + 491*Power(t2,2))) + 
            Power(t1,2)*(4282 - 24*Power(s1,3) + 
               Power(s1,2)*(836 - 278*t2) - 3644*t2 + 199*Power(t2,2) + 
               8*Power(t2,3) + s1*(-3441 + 1402*t2 + 763*Power(t2,2))) + 
            Power(s2,6)*(40*Power(s1,3) - 98*Power(t1,3) - 
               10*Power(s1,2)*(2 + 5*t1 + 21*t2) + 
               Power(t1,2)*(464 + 107*t2) + 
               2*t1*(-252 - 29*t2 + 56*Power(t2,2)) + 
               s1*(-44 - 195*Power(t1,2) + t1*(228 - 14*t2) + 170*t2 + 
                  336*Power(t2,2)) - 
               2*(-75 + 19*t2 + 98*Power(t2,2) + 84*Power(t2,3))) + 
            Power(s2,5)*(Power(s1,3)*(97 - 235*t1) + 41*Power(t1,4) + 
               Power(t1,3)*(-1531 + 373*t2) + 
               Power(t1,2)*(2727 - 2201*t2 - 1483*Power(t2,2)) - 
               2*(-153 + 249*t2 + 549*Power(t2,2) + 448*Power(t2,3)) + 
               t1*(-1588 + 2195*t2 + 2861*Power(t2,2) + 
                  1302*Power(t2,3)) + 
               Power(s1,2)*(-203*Power(t1,2) + 17*t1*(32 + 75*t2) - 
                  5*(32 + 133*t2)) + 
               s1*(248 + 556*Power(t1,3) + 793*t2 + 1395*Power(t2,2) + 
                  35*Power(t1,2)*(4 + 31*t2) - 
                  2*t1*(407 + 1166*t2 + 1134*Power(t2,2)))) + 
            s2*(52 - 1407*Power(t1,8) + 
               Power(s1,3)*t1*
                (36 - 448*t1 + 1314*Power(t1,2) - 1125*Power(t1,3) + 
                  119*Power(t1,4)) - 182*t2 + 152*Power(t2,2) + 
               37*Power(t2,3) + Power(t1,7)*(2271 + 1498*t2) + 
               Power(t1,6)*(4741 - 2342*t2 - 1197*Power(t2,2)) + 
               Power(t1,2)*(15995 - 10035*t2 + 2909*Power(t2,2) - 
                  4841*Power(t2,3)) + 
               Power(t1,4)*(13543 + 8161*t2 - 4773*Power(t2,2) - 
                  4740*Power(t2,3)) + 
               Power(t1,5)*(-11276 - 4027*t2 + 4321*Power(t2,2) + 
                  938*Power(t2,3)) + 
               t1*(-4433 + 4132*t2 - 1757*Power(t2,2) + 
                  1013*Power(t2,3)) + 
               Power(t1,3)*(-19520 + 2872*t2 + 552*Power(t2,2) + 
                  7619*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-1112 - 105*Power(t1,5) + t1*(8183 - 1596*t2) + 
                  354*t2 - Power(t1,4)*(2749 + 357*t2) + 
                  Power(t1,3)*(14036 + 389*t2) + 
                  2*Power(t1,2)*(-8986 + 717*t2)) + 
               s1*(20 + 1925*Power(t1,7) - 78*t2 + 46*Power(t2,2) - 
                  Power(t1,6)*(683 + 2191*t2) + 
                  Power(t1,3)*(30235 - 6544*t2 - 2437*Power(t2,2)) + 
                  t1*(4100 - 1864*t2 - 571*Power(t2,2)) + 
                  Power(t1,4)*(-8692 - 4021*t2 + 198*Power(t2,2)) + 
                  Power(t1,5)*(-5869 + 6109*t2 + 504*Power(t2,2)) + 
                  Power(t1,2)*(-21105 + 8101*t2 + 2114*Power(t2,2)))) - 
            Power(s2,2)*(-844 - 2502*Power(t1,7) + 
               Power(s1,3)*(12 - 269*t1 + 1065*Power(t1,2) - 
                  1224*Power(t1,3) + 316*Power(t1,4)) + 777*t2 - 
               892*Power(t2,2) + 689*Power(t2,3) + 
               Power(t1,6)*(2483 + 4195*t2) + 
               Power(t1,5)*(9623 - 11615*t2 - 3595*Power(t2,2)) + 
               Power(t1,3)*(7378 + 3259*t2 - 13768*Power(t2,2) - 
                  10346*Power(t2,3)) + 
               t1*(3882 - 925*t2 + 1348*Power(t2,2) - 
                  5161*Power(t2,3)) + 
               Power(t1,4)*(-15292 + 8347*t2 + 11821*Power(t2,2) + 
                  2910*Power(t2,3)) + 
               Power(t1,2)*(-4759 - 3917*t2 + 5199*Power(t2,2) + 
                  11808*Power(t2,3)) + 
               Power(s1,2)*(-313 - 48*Power(t1,5) + 
                  t1*(4022 - 765*t2) + 79*t2 + 
                  22*Power(t1,2)*(-529 + 54*t2) + 
                  Power(t1,3)*(10923 + 188*t2) - 
                  2*Power(t1,4)*(1268 + 315*t2)) + 
               s1*(936 + 2510*Power(t1,6) + 
                  Power(t1,5)*(1481 - 2380*t2) - 305*t2 - 
                  144*Power(t2,2) + 
                  Power(t1,2)*(8471 + 622*t2 - 5736*Power(t2,2)) - 
                  2*Power(t1,4)*(8212 - 3533*t2 + 318*Power(t2,2)) + 
                  t1*(-7219 + 1627*t2 + 2125*Power(t2,2)) + 
                  Power(t1,3)*(10075 - 7136*t2 + 4530*Power(t2,2)))) + 
            Power(s2,4)*(74 + 740*Power(t1,5) + 
               Power(s1,3)*(41 - 266*t1 + 252*Power(t1,2)) + 
               Power(t1,4)*(1891 - 2740*t2) - 217*t2 - 
               1332*Power(t2,2) - 1718*Power(t2,3) + 
               Power(t1,3)*(-7204 + 10372*t2 + 4333*Power(t2,2)) - 
               Power(t1,2)*(-6465 + 11272*t2 + 10258*Power(t2,2) + 
                  3452*Power(t2,3)) + 
               t1*(-1915 + 4063*t2 + 7350*Power(t2,2) + 
                  5044*Power(t2,3)) + 
               Power(s1,2)*(-65 + 648*Power(t1,3) - 536*t2 + 
                  50*t1*(16 + 47*t2) - 3*Power(t1,2)*(429 + 652*t2)) + 
               s1*(765 - 845*Power(t1,4) + 362*t2 + 1779*Power(t2,2) - 
                  4*Power(t1,3)*(590 + 599*t2) + 
                  Power(t1,2)*(6494 + 5288*t2 + 4611*Power(t2,2)) - 
                  t1*(4245 + 3460*t2 + 6148*Power(t2,2)))) + 
            Power(s2,3)*(-53 - 2100*Power(t1,6) + 
               Power(s1,3)*(-39 + 206*t1 - 249*Power(t1,2) + 
                  140*Power(t1,3)) + 444*t2 + 75*Power(t2,2) - 
               1627*Power(t2,3) + Power(t1,5)*(64 + 5125*t2) + 
               Power(t1,4)*(10947 - 17057*t2 - 5530*Power(t2,2)) + 
               Power(t1,3)*(-13086 + 18563*t2 + 15726*Power(t2,2) + 
                  4403*Power(t2,3)) + 
               t1*(367 - 1083*t2 + 4909*Power(t2,2) + 
                  7533*Power(t2,3)) - 
               Power(t1,2)*(-3844 + 6030*t2 + 15531*Power(t2,2) + 
                  10537*Power(t2,3)) + 
               Power(s1,2)*(479 - 436*Power(t1,4) + 
                  Power(t1,2)*(1930 - 2028*t2) - 144*t2 + 
                  Power(t1,3)*(58 + 618*t2) + t1*(-2077 + 1176*t2)) + 
               s1*(-234 + 1615*Power(t1,5) - 314*t2 + 883*Power(t2,2) + 
                  Power(t1,4)*(3655 + 576*t2) + 
                  t1*(-2792 + 475*t2 - 5495*Power(t2,2)) - 
                  11*Power(t1,3)*(1456 + 80*t2 + 331*Power(t2,2)) + 
                  Power(t1,2)*(13739 + 565*t2 + 8796*Power(t2,2))))) + 
         Power(s,4)*(2*Power(s2,8)*Power(-1 + t1,2) - 128*Power(t1,10) + 
            Power(t1,9)*(458 + 372*s1 + 60*t2) + 
            2*(-7 + 42*t2 - 69*Power(t2,2) + 34*Power(t2,3)) - 
            Power(t1,8)*(-131 + 112*Power(s1,2) - 287*t2 + 
               84*Power(t2,2) + 91*s1*(7 + 4*t2)) + 
            Power(t1,5)*(-18537 + 810*Power(s1,3) + 8138*t2 - 
               1407*Power(t2,2) + 772*Power(t2,3) + 
               5*Power(s1,2)*(-2339 + 275*t2) + 
               s1*(27498 - 9000*t2 - 290*Power(t2,2))) + 
            Power(t1,7)*(-2135 - 490*Power(s1,2) - 2344*t2 + 
               378*Power(t2,2) + 44*Power(t2,3) + 
               2*s1*(337 + 525*t2 + 56*Power(t2,2))) + 
            t1*(214 - 847*t2 + 1121*Power(t2,2) - 504*Power(t2,3) - 
               2*s1*(56 - 135*t2 + 77*Power(t2,2))) - 
            Power(t1,6)*(-7246 + 385*Power(s1,3) - 2430*t2 + 
               71*Power(t2,2) + 371*Power(t2,3) + 
               25*Power(s1,2)*(-221 + 7*t2) + 
               s1*(9666 - 850*t2 + 231*Power(t2,2))) + 
            Power(t1,3)*(-15404 + 128*Power(s1,3) + 14199*t2 - 
               373*Power(t2,2) - 993*Power(t2,3) + 
               2*Power(s1,2)*(-1790 + 643*t2) - 
               2*s1*(-7063 + 4111*t2 + 403*Power(t2,2))) + 
            Power(t1,2)*(3211 - 6*Power(s1,3) + 
               Power(s1,2)*(436 - 227*t2) - 2298*t2 - 1667*Power(t2,2) + 
               1177*Power(t2,3) + s1*(-2137 + 1048*t2 + 505*Power(t2,2))) \
+ Power(t1,4)*(24958 - 550*Power(s1,3) + Power(s1,2)*(9916 - 2250*t2) - 
               19709*t2 + 2241*Power(t2,2) - 190*Power(t2,3) + 
               s1*(-30118 + 14368*t2 + 855*Power(t2,2))) + 
            Power(s2,7)*(56 + 30*Power(s1,3) - 5*Power(t1,3) + 19*t2 - 
               84*Power(t2,2) - 72*Power(t2,3) - 
               2*Power(s1,2)*(10 + 3*t1 + 63*t2) + 
               Power(t1,2)*(92 + 99*t2) + 
               t1*(-143 - 114*t2 + 56*Power(t2,2)) - 
               2*s1*(25 + 64*Power(t1,2) - 48*t2 - 84*Power(t2,2) + 
                  3*t1*(-29 + 7*t2))) - 
            Power(s2,6)*(-74 + 93*Power(t1,4) + 
               2*Power(s1,3)*(-59 + 120*t1) + 526*t2 + 836*Power(t2,2) + 
               518*Power(t2,3) + Power(t1,3)*(179 + 39*t2) + 
               Power(t1,2)*(-592 + 1057*t2 + 1128*Power(t2,2)) - 
               t1*(-394 + 1581*t2 + 2065*Power(t2,2) + 
                  756*Power(t2,3)) + 
               Power(s1,2)*(266 + 411*Power(t1,2) + 610*t2 - 
                  t1*(758 + 1077*t2)) + 
               s1*(-262 - 464*Power(t1,3) + Power(t1,2)*(76 - 1354*t2) - 
                  934*t2 - 999*Power(t2,2) + 
                  t1*(609 + 2470*t2 + 1582*Power(t2,2)))) + 
            s2*(-36 + 733*Power(t1,9) + 
               Power(s1,3)*t1*
                (12 - 318*t1 + 1654*Power(t1,2) - 2910*Power(t1,3) + 
                  1710*Power(t1,4) - 105*Power(t1,5)) + 208*t2 - 
               383*Power(t2,2) + 227*Power(t2,3) - 
               6*Power(t1,8)*(361 + 126*t2) + 
               Power(t1,7)*(-625 + 1088*t2 + 763*Power(t2,2)) + 
               t1*(-4601 + 4328*t2 + 297*Power(t2,2) - 
                  748*Power(t2,3)) + 
               Power(t1,6)*(6053 + 5375*t2 - 3260*Power(t2,2) - 
                  476*Power(t2,3)) + 
               Power(t1,2)*(23192 - 19597*t2 + 1407*Power(t2,2) - 
                  305*Power(t2,3)) + 
               Power(t1,5)*(-12917 - 12952*t2 + 4305*Power(t2,2) + 
                  2988*Power(t2,3)) + 
               Power(t1,3)*(-40262 + 23775*t2 - 1205*Power(t2,2) + 
                  4060*Power(t2,3)) - 
               Power(t1,4)*(-30629 + 1471*t2 + 1984*Power(t2,2) + 
                  5775*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-700 + 231*Power(t1,6) + t1*(7517 - 2399*t2) + 
                  Power(t1,3)*(34089 - 2165*t2) + 334*t2 - 
                  20*Power(t1,4)*(917 + 46*t2) + 
                  Power(t1,5)*(2158 + 413*t2) + 
                  Power(t1,2)*(-25015 + 4622*t2)) + 
               s1*(40 - 1729*Power(t1,8) - 98*t2 + 54*Power(t2,2) + 
                  Power(t1,7)*(2684 + 1701*t2) + 
                  Power(t1,3)*(61277 - 22765*t2 - 3136*Power(t2,2)) + 
                  Power(t1,6)*(1087 - 6274*t2 - 518*Power(t2,2)) + 
                  t1*(3392 - 2136*t2 - 242*Power(t2,2)) + 
                  2*Power(t1,5)*(8740 + 3718*t2 + 183*Power(t2,2)) + 
                  Power(t1,2)*(-25575 + 13706*t2 + 1212*Power(t2,2)) + 
                  Power(t1,4)*(-58654 + 8550*t2 + 2365*Power(t2,2)))) + 
            Power(s2,2)*(1365 - 1705*Power(t1,8) + 
               2*Power(s1,3)*
                (-3 + 129*t1 - 881*Power(t1,2) + 1899*Power(t1,3) - 
                  1410*Power(t1,4) + 220*Power(t1,5)) - 1396*t2 + 
               355*Power(t2,2) - 23*Power(t2,3) + 
               Power(t1,7)*(4143 + 2791*t2) + 
               Power(t1,6)*(1804 - 9241*t2 - 2936*Power(t2,2)) + 
               Power(t1,4)*(6654 + 10865*t2 - 15515*Power(t2,2) - 
                  8720*Power(t2,3)) + 
               t1*(-9024 + 6001*t2 - 1062*Power(t2,2) + 
                  1846*Power(t2,3)) + 
               Power(t1,5)*(-8725 + 6191*t2 + 11129*Power(t2,2) + 
                  1892*Power(t2,3)) - 
               Power(t1,2)*(-16745 + 1985*t2 + 2144*Power(t2,2) + 
                  8267*Power(t2,3)) + 
               Power(t1,3)*(-11257 - 13213*t2 + 10358*Power(t2,2) + 
                  13290*Power(t2,3)) + 
               Power(s1,2)*(262 - 169*Power(t1,6) - 105*t2 + 
                  5*Power(t1,3)*(-7085 + 63*t2) + 
                  8*t1*(-606 + 155*t2) - 
                  3*Power(t1,2)*(-7148 + 933*t2) - 
                  Power(t1,5)*(3365 + 1344*t2) + 
                  Power(t1,4)*(22318 + 2890*t2)) + 
               s1*(-1167 + 3188*Power(t1,7) + 778*t2 - 41*Power(t2,2) - 
                  6*Power(t1,6)*(649 + 378*t2) + 
                  Power(t1,3)*(35286 + 8606*t2 - 6488*Power(t2,2)) + 
                  3*Power(t1,5)*(-2921 + 3388*t2 + 114*Power(t2,2)) - 
                  2*t1*(-6643 + 2859*t2 + 392*Power(t2,2)) + 
                  Power(t1,4)*(258 - 19097*t2 + 2300*Power(t2,2)) + 
                  Power(t1,2)*(-38207 + 7133*t2 + 4548*Power(t2,2)))) + 
            Power(s2,5)*(66 + 563*Power(t1,5) + 
               Power(s1,3)*(62 - 366*t1 + 375*Power(t1,2)) - 821*t2 - 
               1466*Power(t2,2) - 1166*Power(t2,3) - 
               4*Power(t1,4)*(106 + 325*t2) + 
               Power(t1,3)*(-1218 + 7730*t2 + 4189*Power(t2,2)) - 
               Power(t1,2)*(-1722 + 11142*t2 + 9504*Power(t2,2) + 
                  2519*Power(t2,3)) + 
               t1*(-709 + 5625*t2 + 6833*Power(t2,2) + 
                  3526*Power(t2,3)) + 
               Power(s1,2)*(-187 + 1337*Power(t1,3) - 609*t2 - 
                  Power(t1,2)*(2413 + 2250*t2) + t1*(1331 + 2572*t2)) + 
               s1*(713 - 897*Power(t1,4) + 888*t2 + 1570*Power(t2,2) - 
                  Power(t1,3)*(1226 + 4341*t2) + 
                  Power(t1,2)*(4679 + 8734*t2 + 4233*Power(t2,2)) - 
                  t1*(3361 + 5401*t2 + 5428*Power(t2,2)))) + 
            Power(s2,3)*(578 + 2093*Power(t1,7) - 
               2*Power(s1,3)*
                (34 - 377*t1 + 1029*Power(t1,2) - 968*Power(t1,3) + 
                  290*Power(t1,4)) + 91*t2 + 224*Power(t2,2) - 
               780*Power(t2,3) - Power(t1,6)*(4142 + 4635*t2) + 
               2*Power(t1,5)*(-1336 + 9642*t2 + 3015*Power(t2,2)) - 
               Power(t1,4)*(-8561 + 26614*t2 + 19691*Power(t2,2) + 
                  3760*Power(t2,3)) + 
               t1*(-308 - 3763*t2 + 2213*Power(t2,2) + 
                  5738*Power(t2,3)) + 
               Power(t1,3)*(-2161 + 9723*t2 + 24123*Power(t2,2) + 
                  12408*Power(t2,3)) - 
               Power(t1,2)*(1949 - 5916*t2 + 12919*Power(t2,2) + 
                  13466*Power(t2,3)) + 
               Power(s1,2)*(917 + 590*Power(t1,5) - 133*t2 - 
                  2*Power(t1,2)*(-7486 + 85*t2) + t1*(-6963 + 516*t2) + 
                  Power(t1,4)*(1291 + 975*t2) - 
                  Power(t1,3)*(10909 + 1004*t2)) + 
               s1*(-1723 - 3030*Power(t1,6) + 
                  Power(t1,5)*(1444 - 870*t2) + 238*t2 + 
                  260*Power(t2,2) + 
                  Power(t1,3)*(-17875 + 9689*t2 - 8340*Power(t2,2)) + 
                  t1*(6607 + 2105*t2 - 3292*Power(t2,2)) + 
                  Power(t1,4)*(14906 - 1700*t2 + 2015*Power(t2,2)) + 
                  Power(t1,2)*(-331 - 9340*t2 + 9049*Power(t2,2)))) + 
            Power(s2,4)*(-343 - 1460*Power(t1,6) + 
               Power(s1,3)*(-96 + 298*t1 - 193*Power(t1,2) + 
                  80*Power(t1,3)) + 458*t2 - 681*Power(t2,2) - 
               1331*Power(t2,3) + Power(t1,5)*(2222 + 3780*t2) + 
               Power(t1,4)*(2129 - 17977*t2 - 6890*Power(t2,2)) + 
               2*t1*(364 + 992*t2 + 3588*Power(t2,2) + 
                  3187*Power(t2,3)) + 
               Power(t1,3)*(-5146 + 26946*t2 + 18963*Power(t2,2) + 
                  4135*Power(t2,3)) - 
               Power(t1,2)*(-1870 + 15259*t2 + 18798*Power(t2,2) + 
                  9317*Power(t2,3)) + 
               Power(s1,2)*(612 - 1460*Power(t1,4) + 
                  Power(t1,2)*(341 - 2753*t2) - 83*t2 + 
                  4*t1*(-438 + 313*t2) + Power(t1,3)*(2081 + 1255*t2)) + 
               s1*(231 + 1760*Power(t1,5) - 593*t2 + 987*Power(t2,2) + 
                  Power(t1,4)*(1531 + 4830*t2) - 
                  2*Power(t1,3)*(5962 + 4800*t2 + 2385*Power(t2,2)) - 
                  2*t1*(2213 - 84*t2 + 3082*Power(t2,2)) + 
                  Power(t1,2)*(12896 + 5603*t2 + 10326*Power(t2,2))))))*
       R1q(1 - s + s2 - t1))/
     (s*(-1 + s1)*(-1 + s2)*Power(-s + s2 - t1,3)*(1 - s + s2 - t1)*
       Power(-1 + s + t1,3)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*
       (-s + s1 - t2)) - (8*(-4*Power(s,9)*Power(t1,2) - 
         2*Power(s,8)*(4*s2*(1 - 3*t1)*t1 + 2*Power(t1,3) + 
            Power(t2,3) + Power(t1,2)*(-7 - 3*s1 + t2)) + 
         Power(s,7)*(5*Power(t1,4) + 
            Power(s2,2)*(-4 + 40*t1 - 60*Power(t1,2)) + 
            Power(t1,3)*(-8 + 5*s1 - 5*t2) + 2*Power(t2,2)*(-3 + 7*t2) + 
            t1*t2*(-6 + 6*s1 + 4*t2 - 2*s1*t2 - 3*Power(t2,2)) + 
            Power(t1,2)*(58 - 46*s1 - 2*Power(s1,2) - 10*t2 + 5*s1*t2 + 
               3*Power(t2,2)) + 
            s2*(19*Power(t1,3) + 2*Power(t2,2)*(3 - 3*s1 + 8*t2) + 
               Power(t1,2)*(-40 - 16*s1 + 15*t2) - 
               2*t1*(-10 + 3*s1*(-2 + t2) - t2 + Power(t2,2)))) + 
         Power(s,6)*(5*Power(t1,5) + 
            16*Power(s2,3)*(1 - 5*t1 + 5*Power(t1,2)) - 
            Power(t1,4)*(41 + 9*s1 + t2) - 
            2*t2*(3 - 18*t2 + 19*Power(t2,2)) + 
            Power(t1,3)*(207 - 31*t2 + 3*Power(t2,2) + 
               5*s1*(-5 + 2*t2)) + 
            Power(t1,2)*(-444 - Power(s1,2)*(-34 + t2) + 142*t2 - 
               13*Power(t2,2) + Power(t2,3) + 
               s1*(77 - 31*t2 - 3*Power(t2,2))) + 
            t1*(-6 + 44*t2 - 23*Power(t2,2) + 19*Power(t2,3) + 
               s1*(6 - 34*t2 + 4*Power(t2,2))) + 
            s2*(-24*Power(t1,4) + Power(t1,3)*(90 + 14*s1 + 33*t2) - 
               Power(t1,2)*(333 + 11*Power(s1,2) - 62*t2 + 
                  22*Power(t2,2) + 3*s1*(-26 + 9*t2)) + 
               t2*(6 + 10*t2 - 75*Power(t2,2) + s1*(-6 + 28*t2)) + 
               t1*(148 - 36*t2 + 15*Power(t2,2) + 21*Power(t2,3) + 
                  2*Power(s1,2)*(1 + t2) + 
                  s1*(-98 + 4*t2 + 3*Power(t2,2)))) - 
            Power(s2,2)*(35*Power(t1,3) + 6*Power(s1,2)*(t1 + t2) + 
               2*Power(t1,2)*(5 + 24*t2) + t1*(22 - 14*Power(t2,2)) + 
               2*(-3 + t2 + 19*Power(t2,2) + 28*Power(t2,3)) + 
               s1*(9*Power(t1,2) + t1*(8 - 32*t2) - 
                  6*(1 + t2 + 7*Power(t2,2))))) + 
         Power(-1 + s2,4)*((-1 + s1)*Power(t1,5)*(-1 + 3*s1 - 2*t2) + 
            2*Power(s2,4)*Power(s1 - t2,2)*(-1 + s1 + t1 - t2) + 
            4*Power(-1 + t2,3) - 
            t1*Power(-1 + t2,2)*(-25 + 8*s1 + 7*t2) + 
            Power(t1,3)*(14 - 3*Power(s1,3) - 45*t2 + 53*Power(t2,2) + 
               2*Power(t2,3) + Power(s1,2)*(19 + 11*t2) + 
               s1*(8 - 40*t2 - 19*Power(t2,2))) - 
            Power(t1,4)*(7*Power(s1,2) + Power(s1,3) + 
               3*t2*(-1 + 4*t2) + s1*(2 - 17*t2 - 2*Power(t2,2))) + 
            Power(t1,2)*(-36 + 2*Power(s1,3) + 85*t2 - 68*Power(t2,2) + 
               3*Power(t2,3) - 5*Power(s1,2)*(3 + t2) + 
               s1*(6 + 9*t2 + 19*Power(t2,2))) + 
            Power(s2,2)*(-1 + t1)*
             (-1 - Power(s1,3)*(2 + t1) - 10*t2 + 29*Power(t2,2) - 
               2*Power(t2,3) + Power(t1,2)*(3 + 8*t2) + 
               t1*(-2 + 2*t2 - 21*Power(t2,2) + Power(t2,3)) + 
               Power(s1,2)*(-18*t1 + 11*Power(t1,2) + 5*(3 + t2)) + 
               s1*(14 - 49*t2 + t1*(41 - 2*t2)*t2 + Power(t2,2) - 
                  2*Power(t1,2)*(7 + 4*t2))) - 
            Power(s2,3)*(2 + Power(s1,3)*(1 + 3*t1) + t2 + 
               4*Power(t2,2) - Power(t2,3) + 
               Power(t1,2)*(2 + t2 + 4*Power(t2,2)) - 
               t1*(4 + 2*t2 + 8*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s1,2)*(7 + 7*Power(t1,2) - 3*t2 - 
                  t1*(14 + 9*t2)) + 
               s1*(-5 - 9*t2 + 3*Power(t2,2) - Power(t1,2)*(5 + 9*t2) + 
                  t1*(10 + 18*t2 + 9*Power(t2,2)))) + 
            s2*(Power(s1,3)*t1*(-4 + 5*t1 + 3*Power(t1,2)) - 
               Power(-1 + t2,2)*(13 + 5*t2) + 
               Power(t1,4)*(-2 - 9*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(1 + t2 + 27*Power(t2,2) - 2*Power(t2,3)) - 
               Power(t1,2)*(9 - 46*t2 + 87*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(23 - 59*t2 + 61*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-9*Power(t1,3) - 3*Power(t1,2)*(-8 + t2) + 
                  10*(3 + t2) - t1*(45 + 19*t2)) + 
               s1*(8*Power(-1 + t2,2) + Power(t1,4)*(13 + 5*t2) + 
                  Power(t1,3)*(-2 - 52*t2 + 3*Power(t2,2)) - 
                  2*t1*(-4 + 29*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*(-27 + 121*t2 + 19*Power(t2,2))))) + 
         Power(s,4)*(8 + 8*Power(s2,5)*(2 - 5*t1 + 3*Power(t1,2)) + 
            Power(t1,5)*(39 + 3*Power(s1,2) - 2*s1*(-13 + t2) - 6*t2) - 
            48*t2 + 60*Power(t2,2) - 10*Power(t2,3) + 
            t1*(-51 + 39*t2 + 29*Power(t2,2) + 35*Power(t2,3) + 
               s1*(24 + 28*t2 - 80*Power(t2,2))) + 
            Power(t1,3)*(1202 + Power(s1,3) - 747*t2 + 
               151*Power(t2,2) - 6*Power(t2,3) + 
               Power(s1,2)*(-53 + 15*t2) + 
               s1*(469 - 28*t2 - 23*Power(t2,2))) - 
            Power(t1,4)*(360 + 21*Power(s1,2) + Power(s1,3) - 113*t2 + 
               12*Power(t2,2) + s1*(108 - 5*t2 - 2*Power(t2,2))) + 
            Power(t1,2)*(-1065 + 26*Power(s1,3) + 
               Power(s1,2)*(153 - 83*t2) + 1055*t2 - 294*Power(t2,2) - 
               7*Power(t2,3) + s1*(-856 + 83*t2 + 113*Power(t2,2))) + 
            Power(s2,4)*(10*Power(s1,3) - 10*Power(t1,3) - 
               10*Power(t1,2)*(23 + 9*t2) - 
               2*Power(s1,2)*(1 + 25*t1 + 45*t2) + 
               t1*(271 + 80*t2 + 70*Power(t2,2)) - 
               2*(32 + 20*t2 + 75*Power(t2,2) + 70*Power(t2,3)) + 
               s1*(-37 - 100*Power(t1,2) + 80*t2 + 210*Power(t2,2) + 
                  12*t1*(14 + 5*t2))) + 
            Power(s2,2)*(-336 + 24*Power(t1,5) - 
               2*Power(s1,3)*t1*(1 + 8*t1) + 130*t2 + 19*Power(t2,2) - 
               98*Power(t2,3) - 6*Power(t1,4)*(22 + 3*t2) + 
               Power(t1,3)*(692 - 358*t2 + 30*Power(t2,2)) + 
               Power(t1,2)*(-1790 + 978*t2 - 235*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(1420 - 600*t2 + 412*Power(t2,2) + 115*Power(t2,3)) + 
               Power(s1,2)*(32 + 120*Power(t1,3) - 29*t2 + 
                  Power(t1,2)*(-233 + 10*t2) + t1*(89 + 105*t2)) + 
               s1*(27 - 140*Power(t1,4) - 39*t2 + 79*Power(t2,2) + 
                  Power(t1,3)*(473 + 28*t2) + 
                  t1*(25 - 262*t2 - 133*Power(t2,2)) + 
                  Power(t1,2)*(-349 + 47*t2 - 40*Power(t2,2)))) - 
            Power(s2,3)*(147 + 44*Power(t1,4) + 
               Power(s1,3)*(-6 + 19*t1) - 127*t2 + 32*Power(t2,2) + 
               145*Power(t2,3) - Power(t1,3)*(343 + 130*t2) + 
               Power(t1,2)*(698 - 267*t2 + 120*Power(t2,2)) - 
               t1*(561 - 310*t2 + 310*Power(t2,2) + 105*Power(t2,3)) + 
               Power(s1,2)*(19 + 77*Power(t1,2) + 53*t2 - 
                  5*t1*(33 + 13*t2)) + 
               s1*(10 - 200*Power(t1,3) + 31*t2 - 145*Power(t2,2) + 
                  Power(t1,2)*(608 + 25*t2) + 
                  t1*(-259 + 162*t2 + 95*Power(t2,2)))) + 
            s2*(14 + Power(s1,3)*t1*(-12 - 13*t1 + 12*Power(t1,2)) + 
               Power(t1,5)*(22 - 8*t2) - 63*t2 + 93*Power(t2,2) - 
               55*Power(t2,3) + 
               Power(t1,4)*(-260 + 79*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(1318 - 673*t2 + 131*Power(t2,2) - 
                  10*Power(t2,3)) - 
               Power(t1,2)*(2420 - 1494*t2 + 483*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(1333 - 875*t2 + 371*Power(t2,2) + 81*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-37*Power(t1,3) + t1*(88 - 91*t2) + 
                  Power(t1,2)*(112 - 3*t2) + 2*(-99 + 53*t2)) + 
               s1*(-2 + 30*Power(t1,5) - 24*t2 + 32*Power(t2,2) - 
                  3*Power(t1,4)*(51 + t2) + 
                  t1*(521 - 98*t2 - 148*Power(t2,2)) - 
                  Power(t1,3)*(-369 + 44*t2 + Power(t2,2)) + 
                  Power(t1,2)*(-700 + 163*t2 + 99*Power(t2,2))))) - 
         s*Power(-1 + s2,2)*(2*Power(-1 + t2,2)*(-5 + 11*t2) + 
            2*Power(t1,5)*(2 + 6*Power(s1,2) + 3*t2 - s1*(5 + 4*t2)) - 
            t1*(-1 + t2)*(94 - 137*t2 + 31*Power(t2,2) + 
               s1*(-42 + 46*t2)) + 
            Power(t1,3)*(42 - 11*Power(s1,3) - 273*t2 + 
               232*Power(t2,2) + 6*Power(t2,3) + 
               5*Power(s1,2)*(14 + 9*t2) + 
               s1*(152 - 178*t2 - 77*Power(t2,2))) - 
            Power(t1,4)*(6 + 31*Power(s1,2) + 4*Power(s1,3) - 44*t2 + 
               48*Power(t2,2) + s1*(38 - 65*t2 - 8*Power(t2,2))) + 
            Power(t1,2)*(14*Power(s1,3) - Power(s1,2)*(77 + 38*t2) + 
               s1*(-62 + 85*t2 + 104*Power(t2,2)) + 
               4*(-31 + 103*t2 - 81*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s2,5)*(10*Power(s1,3) + 
               4*Power(s1,2)*(-2 + t1 - 9*t2) + 
               s1*(5 + Power(t1,2) + 26*t2 + 42*Power(t2,2) - 
                  6*t1*(1 + 3*t2)) - 
               t2*(7 + 3*Power(t1,2) + 18*t2 + 16*Power(t2,2) - 
                  2*t1*(5 + 7*t2))) + 
            Power(s2,3)*(16 + Power(s1,3)*(9 - 19*t1 + 2*Power(t1,2)) + 
               Power(t1,4)*(4 - 3*t2) - 73*t2 - 107*Power(t2,2) + 
               12*Power(t2,3) + 
               Power(t1,3)*(55 - 30*t2 + 3*Power(t2,2)) + 
               t1*(19 + 38*t2 + 200*Power(t2,2) - 14*Power(t2,3)) + 
               2*Power(t1,2)*
                (-47 + 34*t2 - 56*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s1,2)*(-103 + 42*Power(t1,3) - 12*t2 + 
                  2*Power(t1,2)*(-69 + 2*t2) + t1*(183 + 28*t2)) - 
               s1*(-38 + 11*Power(t1,4) - 228*t2 + 6*Power(t2,2) + 
                  Power(t1,3)*(24 + 38*t2) + 
                  t1*(22 + 432*t2 - 3*Power(t2,2)) + 
                  Power(t1,2)*(-19 - 274*t2 + 13*Power(t2,2)))) + 
            Power(s2,4)*(-31 - Power(s1,3)*(1 + 16*t1) + 46*t2 - 
               14*Power(t2,2) + 8*Power(t2,3) + 
               Power(t1,3)*(-4 + 8*t2) + 
               Power(s1,2)*(-18 + 59*t1 - 19*Power(t1,2) + 16*t2 + 
                  47*t1*t2) + 
               Power(t1,2)*(-27 + 14*t2 - 27*Power(t2,2)) + 
               t1*(62 - 68*t2 + 63*Power(t2,2) + 21*Power(t2,3)) + 
               s1*(-14 + 4*Power(t1,3) + 33*t2 - 23*Power(t2,2) + 
                  Power(t1,2)*(2 + 43*t2) - 
                  4*t1*(-2 + 30*t2 + 13*Power(t2,2)))) + 
            s2*(-53 - Power(s1,3)*t1*
                (24 - 19*t1 + Power(t1,2) + 4*Power(t1,3)) + 102*t2 - 
               33*Power(t2,2) - 16*Power(t2,3) + 
               4*Power(t1,5)*(1 + 3*t2) + 
               Power(t1,4)*(20 - 87*t2 - 40*Power(t2,2)) + 
               Power(t1,3)*(-2 - 48*t2 + 283*Power(t2,2) + 
                  4*Power(t2,3)) + 
               2*t1*(44 - 241*t2 + 204*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(-57 + 503*t2 - 550*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s1,2)*t1*
                (120 - 58*Power(t1,3) + 12*Power(t1,4) + 70*t2 + 
                  Power(t1,2)*(167 + 34*t2) - t1*(173 + 80*t2)) + 
               s1*(22 - 48*t2 + 26*Power(t2,2) - 
                  4*Power(t1,5)*(7 + 2*t2) + 
                  Power(t1,4)*(81 + 90*t2 + 8*Power(t2,2)) - 
                  3*t1*(-79 + 114*t2 + 27*Power(t2,2)) - 
                  Power(t1,3)*(37 + 330*t2 + 62*Power(t2,2)) + 
                  Power(t1,2)*(-275 + 502*t2 + 91*Power(t2,2)))) + 
            Power(s2,2)*(Power(s1,3)*
                (10 - 17*t1 + 25*Power(t1,2) + 8*Power(t1,3)) - 
               2*Power(t1,5)*t2 + 
               Power(t1,4)*(-34 - 2*t2 + 8*Power(t2,2)) + 
               Power(t1,3)*(25 + 55*t2 + 122*Power(t2,2) - 
                  10*Power(t2,3)) - 
               2*(19 - 113*t2 + 87*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(77 - 275*t2 + 427*Power(t2,2) + 14*Power(t2,3)) - 
               Power(t1,2)*(30 + 2*t2 + 427*Power(t2,2) + 
                  20*Power(t2,3)) - 
               Power(s1,2)*(43 + 39*Power(t1,4) + 32*t2 + 
                  Power(t1,3)*(-133 + 15*t2) - t1*(206 + 47*t2) + 
                  Power(t1,2)*(301 + 78*t2)) + 
               s1*(-111 + 6*Power(t1,5) + 129*t2 + 41*Power(t2,2) + 
                  7*Power(t1,4)*(8 + 3*t2) + 
                  t1*(13 - 408*t2 - 80*Power(t2,2)) + 
                  Power(t1,3)*(-107 - 254*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*(143 + 600*t2 + 106*Power(t2,2))))) + 
         Power(s,3)*(-8 - 4*Power(s2,6)*Power(-1 + t1,2) - 
            2*Power(t1,5)*(20 + 6*Power(s1,2) + s1*(13 - 4*t2) - 2*t2) + 
            12*t2 + 30*Power(t2,2) - 38*Power(t2,3) + 
            Power(t1,2)*(477 - 44*Power(s1,3) - 1250*t2 + 
               591*Power(t2,2) + 8*Power(t2,3) + 
               4*Power(s1,2)*(-4 + 33*t2) + 
               s1*(1048 - 245*t2 - 232*Power(t2,2))) + 
            Power(t1,4)*(325 + 44*Power(s1,2) + 4*Power(s1,3) - 
               188*t2 + 48*Power(t2,2) - 
               2*s1*(-92 + 25*t2 + 4*Power(t2,2))) + 
            t1*(-20 + 196*t2 - 196*Power(t2,2) + 15*Power(t2,3) + 
               2*s1*(16 - 81*t2 + 65*Power(t2,2))) + 
            Power(t1,3)*(-740 + 6*Power(s1,3) + 
               Power(s1,2)*(20 - 50*t2) + 989*t2 - 344*Power(t2,2) + 
               4*Power(t2,3) + s1*(-882 + 204*t2 + 82*Power(t2,2))) + 
            Power(s2,5)*(-20*Power(s1,3) - Power(t1,3) + 
               8*Power(s1,2)*(1 + 5*t1 + 15*t2) + 
               Power(t1,2)*(136 + 57*t2) - 
               5*t1*(43 + 18*t2 + 14*Power(t2,2)) + 
               2*(40 + 25*t2 + 65*Power(t2,2) + 56*Power(t2,3)) + 
               s1*(39 + 54*Power(t1,2) - 100*t2 - 210*Power(t2,2) - 
                  2*t1*(56 + 5*t2))) + 
            Power(s2,3)*(240 - 14*Power(t1,5) + 
               Power(s1,3)*(-1 + 13*t1 + 7*Power(t1,2)) - 38*t2 + 
               138*Power(t2,2) - 8*Power(t2,3) + 
               Power(t1,4)*(52 + 22*t2) + 
               Power(t1,3)*(-240 + 428*t2 - 30*Power(t2,2)) + 
               Power(t1,2)*(777 - 750*t2 + 300*Power(t2,2) - 
                  20*Power(t2,3)) - 
               4*t1*(227 - 111*t2 + 121*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s1,2)*(53 - 149*Power(t1,3) + 
                  Power(t1,2)*(446 - 20*t2) + 32*t2 - t1*(389 + 128*t2)\
) + s1*(144*Power(t1,4) + Power(t1,3)*(-467 + 28*t2) - 
                  4*(5 + 40*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*(224 - 420*t2 + 50*Power(t2,2)) + 
                  2*t1*(63 + 330*t2 + 61*Power(t2,2)))) + 
            Power(s2,4)*(41 + 21*Power(t1,4) + 
               9*Power(s1,3)*(-1 + 4*t1) - 172*t2 - 2*Power(t2,2) + 
               30*Power(t2,3) - 15*Power(t1,3)*(13 + 7*t2) + 
               Power(t1,2)*(364 - 238*t2 + 125*Power(t2,2)) - 
               t1*(196 - 394*t2 + 330*Power(t2,2) + 105*Power(t2,3)) + 
               Power(s1,2)*(48*Power(t1,2) + 12*(4 + t2) - 
                  t1*(233 + 110*t2)) + 
               s1*(110 - 155*Power(t1,3) + Power(t1,2)*(559 - 45*t2) + 
                  22*t2 - 10*Power(t2,2) + 
                  t1*(-427 + 358*t2 + 150*Power(t2,2)))) + 
            s2*(-17 - 10*Power(t1,5) + 
               Power(s1,3)*t1*
                (40 + 8*t1 - 27*Power(t1,2) + 4*Power(t1,3)) - 12*t2 + 
               32*Power(t2,2) + 
               2*Power(t1,4)*(58 - 31*t2 + 20*Power(t2,2)) - 
               2*t1*(402 - 743*t2 + 389*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(-638 + 572*t2 - 406*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(1254 - 1643*t2 + 1056*Power(t2,2) + 
                  12*Power(t2,3)) - 
               Power(s1,2)*t1*
                (-92*Power(t1,3) + 12*Power(t1,4) + t1*(77 - 188*t2) + 
                  4*Power(t1,2)*(37 + 8*t2) + 8*(-23 + 24*t2)) + 
               s1*(-2 + 28*t2 - 18*Power(t2,2) + 
                  4*Power(t1,5)*(-3 + 2*t2) + 
                  Power(t1,2)*(1499 - 716*t2 - 246*Power(t2,2)) - 
                  8*Power(t1,4)*(-8 + 10*t2 + Power(t2,2)) + 
                  Power(t1,3)*(-530 + 412*t2 + 68*Power(t2,2)) + 
                  t1*(-1359 + 358*t2 + 202*Power(t2,2)))) + 
            Power(s2,2)*(-(Power(s1,3)*
                  (6 + 13*t1 - 25*Power(t1,2) + 23*Power(t1,3))) + 
               4*Power(t1,5)*(2 + 3*t2) - 
               2*Power(t1,4)*(-43 + 54*t2 + 4*Power(t2,2)) + 
               8*(45 - 44*t2 + 31*Power(t2,2)) + 
               8*Power(t1,2)*
                (159 - 132*t2 + 115*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(-686 + 452*t2 - 228*Power(t2,2) + 
                  20*Power(t2,3)) - 
               t1*(877 - 946*t2 + 782*Power(t2,2) + 22*Power(t2,3)) + 
               Power(s1,2)*(-81 + 80*Power(t1,4) + t1*(34 - 178*t2) + 
                  44*t2 + Power(t1,3)*(-295 + 18*t2) + 
                  Power(t1,2)*(315 + 164*t2)) - 
               s1*(-277 + 42*Power(t1,5) + 62*t2 + 42*Power(t2,2) + 
                  2*Power(t1,4)*(-48 + 7*t2) + 
                  Power(t1,3)*(66 - 316*t2 + 6*Power(t2,2)) - 
                  4*t1*(-180 + 143*t2 + 53*Power(t2,2)) + 
                  Power(t1,2)*(-364 + 910*t2 + 228*Power(t2,2))))) + 
         Power(s,2)*(-4 + 112*t1 - 70*s1*t1 - 157*Power(t1,2) - 
            505*s1*Power(t1,2) - 108*Power(s1,2)*Power(t1,2) + 
            36*Power(s1,3)*Power(t1,2) + 129*Power(t1,3) + 
            628*s1*Power(t1,3) + 66*Power(s1,2)*Power(t1,3) - 
            14*Power(s1,3)*Power(t1,3) - 83*Power(t1,4) - 
            145*s1*Power(t1,4) - 52*Power(s1,2)*Power(t1,4) - 
            6*Power(s1,3)*Power(t1,4) + 19*Power(t1,5) + 
            2*s1*Power(t1,5) + 18*Power(s1,2)*Power(t1,5) + 42*t2 - 
            346*t1*t2 + 182*s1*t1*t2 + 928*Power(t1,2)*t2 + 
            227*s1*Power(t1,2)*t2 - 103*Power(s1,2)*Power(t1,2)*t2 - 
            713*Power(t1,3)*t2 - 294*s1*Power(t1,3)*t2 + 
            70*Power(s1,2)*Power(t1,3)*t2 + 141*Power(t1,4)*t2 + 
            90*s1*Power(t1,4)*t2 + 4*Power(t1,5)*t2 - 
            12*s1*Power(t1,5)*t2 - 84*Power(t2,2) + 275*t1*Power(t2,2) - 
            108*s1*t1*Power(t2,2) - 617*Power(t1,2)*Power(t2,2) + 
            223*s1*Power(t1,2)*Power(t2,2) + 
            401*Power(t1,3)*Power(t2,2) - 
            118*s1*Power(t1,3)*Power(t2,2) - 
            72*Power(t1,4)*Power(t2,2) + 12*s1*Power(t1,4)*Power(t2,2) + 
            46*Power(t2,3) - 47*t1*Power(t2,3) + 
            3*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) + 
            Power(s2,6)*(20*Power(s1,3) + Power(t1,3) - 
               10*Power(t1,2)*(3 + 2*t2) - 
               2*Power(s1,2)*(6 + 5*t1 + 45*t2) + 
               t1*(59 + 48*t2 + 42*Power(t2,2)) - 
               2*(15 + 15*t2 + 33*Power(t2,2) + 28*Power(t2,3)) + 
               s1*(-5 - 9*Power(t1,2) + 70*t2 + 126*Power(t2,2) - 
                  8*t1*(-2 + 3*t2))) + 
            Power(s2,5)*(29 - 4*Power(t1,4) - Power(s1,3)*(9 + 34*t1) + 
               156*t2 + 34*Power(t2,2) + 51*Power(t2,3) + 
               5*Power(t1,3)*(7 + 9*t2) + 
               Power(t1,2)*(-115 + 112*t2 - 78*Power(t2,2)) + 
               t1*(53 - 296*t2 + 171*Power(t2,2) + 63*Power(t2,3)) + 
               Power(s1,2)*(-30 - 25*Power(t1,2) + 82*t2 + 
                  t1*(159 + 100*t2)) + 
               s1*(-89 + 50*Power(t1,3) - 20*t2 - 130*Power(t2,2) + 
                  3*Power(t1,2)*(-61 + 25*t2) + 
                  t1*(209 - 288*t2 - 123*Power(t2,2)))) + 
            Power(s2,4)*(-207 + 3*Power(t1,5) + 
               Power(s1,3)*(4 - 9*t1 + 5*Power(t1,2)) + 
               Power(t1,4)*(1 - 13*t2) - 94*t2 - 174*Power(t2,2) + 
               22*Power(t2,3) + 
               Power(t1,3)*(80 - 251*t2 + 15*Power(t2,2)) + 
               t1*(228 + 80*t2 + 245*Power(t2,2) - 55*Power(t2,3)) + 
               Power(t1,2)*(-97 + 274*t2 - 195*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(s1,2)*(97*Power(t1,3) + 
                  5*Power(t1,2)*(-68 + 3*t2) - 5*(29 + 6*t2) + 
                  2*t1*(166 + 7*t2)) + 
               s1*(258 - 67*Power(t1,4) + Power(t1,3)*(126 - 62*t2) + 
                  330*t2 + 10*Power(t2,2) + 
                  Power(t1,2)*(88 + 483*t2 - 35*Power(t2,2)) + 
                  t1*(-419 - 574*t2 + 42*Power(t2,2)))) + 
            Power(s2,2)*(-41 + 
               Power(s1,3)*(14 + 27*t1 - 39*Power(t1,2) + 
                  8*Power(t1,3) - 6*Power(t1,4)) + 490*t2 - 
               316*Power(t2,2) - 4*Power(t2,3) + 
               10*Power(t1,5)*(3 + 2*t2) - 
               2*Power(t1,4)*(49 + 56*t2 + 32*Power(t2,2)) + 
               4*Power(t1,3)*
                (10 - 3*t2 + 88*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(-329 - 46*t2 + 614*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(454 - 78*t2 - 548*Power(t2,2) + 
                  22*Power(t2,3)) + 
               Power(s1,2)*(23 - 64*Power(t1,4) + 18*Power(t1,5) - 
                  56*t2 - 2*Power(t1,2)*(21 + 16*t2) + 
                  t1*(56 + 34*t2) + Power(t1,3)*(115 + 58*t2)) + 
               s1*(-451 + 184*t2 + 56*Power(t2,2) - 
                  2*Power(t1,5)*(25 + 6*t2) + 
                  t1*(295 - 600*t2 - 94*Power(t2,2)) + 
                  2*Power(t1,4)*(100 + 63*t2 + 6*Power(t2,2)) + 
                  2*Power(t1,2)*(-76 + 333*t2 + 34*Power(t2,2)) - 
                  2*Power(t1,3)*(114 + 214*t2 + 49*Power(t2,2)))) + 
            s2*(-33 - Power(s1,3)*t1*
                (48 + 8*t1 - 21*Power(t1,2) + 4*Power(t1,3)) + 100*t2 - 
               20*Power(t2,2) - 41*Power(t2,3) + 
               2*Power(t1,5)*(-13 + 8*t2) - 
               2*Power(t1,4)*(-95 + 89*t2 + 18*Power(t2,2)) + 
               2*Power(t1,2)*
                (64 + 301*t2 - 364*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(-333 + 259*t2 + 294*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(24 - 1002*t2 + 653*Power(t2,2) + 23*Power(t2,3)) + 
               2*Power(s1,2)*t1*
                (27 - 31*Power(t1,3) + 6*Power(t1,4) + 85*t2 + 
                  Power(t1,2)*(64 + 13*t2) - t1*(14 + 51*t2)) + 
               s1*(18 - 50*t2 + 28*Power(t2,2) - 
                  2*Power(t1,5)*(21 + 4*t2) + 
                  Power(t1,3)*(57 - 416*t2 - 54*Power(t2,2)) + 
                  2*Power(t1,4)*(54 + 53*t2 + 4*Power(t2,2)) - 
                  3*t1*(-363 + 232*t2 + 31*Power(t2,2)) + 
                  Power(t1,2)*(-919 + 731*t2 + 70*Power(t2,2)))) + 
            Power(s2,3)*(Power(s1,3)*
                (-13 + 32*t1 + 6*Power(t1,2) + 17*Power(t1,3)) - 
               2*Power(t1,5)*(5 + 4*t2) + 
               2*Power(t1,4)*(-19 + 33*t2 + 6*Power(t2,2)) + 
               Power(t1,3)*(176 + 96*t2 + 218*Power(t2,2) - 
                  20*Power(t2,3)) + 
               2*(79 + 4*t2 - 87*Power(t2,2) - 9*Power(t2,3)) + 
               t1*(221 - 454*t2 + 560*Power(t2,2) + 10*Power(t2,3)) - 
               Power(t1,2)*(535 - 166*t2 + 714*Power(t2,2) + 
                  48*Power(t2,3)) + 
               Power(s1,2)*(4 + t1 - 78*Power(t1,4) - 
                  26*Power(t1,3)*(-9 + t2) - 34*t2 + 66*t1*t2 - 
                  3*Power(t1,2)*(107 + 54*t2)) + 
               s1*(-99 + 26*Power(t1,5) + 222*t2 + 70*Power(t2,2) + 
                  10*Power(t1,4)*(8 + 3*t2) - 8*t1*t2*(58 + 17*t2) + 
                  Power(t1,3)*(-409 - 400*t2 + 14*Power(t2,2)) + 
                  Power(t1,2)*(592 + 826*t2 + 250*Power(t2,2))))) + 
         Power(s,5)*(-2 - 4*Power(s2,4)*(6 - 20*t1 + 15*Power(t1,2)) + 
            30*t2 - 78*Power(t2,2) + 46*Power(t2,3) + 
            Power(t1,5)*(-20 - 8*s1 + 2*t2) + 
            Power(t1,4)*(172 + 5*Power(s1,2) - 24*t2 + 3*s1*(14 + t2)) + 
            Power(t1,2)*(1033 - 6*Power(s1,3) - 536*t2 + 
               74*Power(t2,2) + Power(s1,2)*(-123 + 22*t2) + 
               s1*(208 + 37*t2 - 16*Power(t2,2))) - 
            Power(t1,3)*(782 + Power(s1,3) + Power(s1,2)*(-18 + t2) - 
               279*t2 + 32*Power(t2,2) - 2*Power(t2,3) + 
               s1*(51 + 30*t2 - Power(t2,2))) + 
            t1*(34 - 101*t2 + 40*Power(t2,2) - 43*Power(t2,3) + 
               2*s1*(-13 + 26*t2 + 9*Power(t2,2))) + 
            Power(s2,3)*(8 - 2*Power(s1,3) + 30*Power(t1,3) + 15*t2 + 
               102*Power(t2,2) + 112*Power(t2,3) + 
               4*Power(s1,2)*(7*t1 + 9*t2) + 
               5*Power(t1,2)*(32 + 17*t2) - 
               t1*(113 + 30*t2 + 42*Power(t2,2)) + 
               s1*(2 + 75*Power(t1,2) - 34*t2 - 126*Power(t2,2) - 
                  2*t1*(41 + 33*t2))) + 
            Power(s2,2)*(4*Power(s1,3)*t1 + 46*Power(t1,4) - 
               Power(t1,3)*(269 + 90*t2) + 
               Power(t1,2)*(699 - 172*t2 + 69*Power(t2,2)) - 
               t1*(508 - 164*t2 + 135*Power(t2,2) + 63*Power(t2,3)) + 
               2*(40 - 28*t2 + 7*Power(t2,2) + 78*Power(t2,3)) + 
               Power(s1,2)*(-2 + 55*Power(t1,2) + 20*t2 - 
                  t1*(48 + 19*t2)) + 
               s1*(-40 - 110*Power(t1,3) + 23*t2 - 113*Power(t2,2) + 
                  3*Power(t1,2)*(65 + 17*t2) + 3*t1*(29 + 8*Power(t2,2)))\
) + s2*(-18*Power(t1,5) + Power(t1,4)*(124 + 61*s1 + 7*t2) + 
               t2*(14 + s1*(20 - 46*t2) - 91*t2 + 116*Power(t2,2)) - 
               Power(t1,3)*(679 + 37*Power(s1,2) - 158*t2 + 
                  15*Power(t2,2) + s1*(125 + 34*t2)) + 
               Power(t1,2)*(1543 - 6*Power(s1,2) + 7*Power(s1,3) - 
                  605*t2 + 90*Power(t2,2) - 6*Power(t2,3) + 
                  s1*(-18 + 96*t2 + 17*Power(t2,2))) + 
               t1*(Power(s1,2)*(48 - 26*t2) + 
                  s1*(116 + 42*t2 + 33*Power(t2,2)) - 
                  4*(198 - 59*t2 + 23*Power(t2,2) + 20*Power(t2,3))))))*
       R2q(s))/(s*(-1 + s1)*(-1 + s2)*
       Power(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2),2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(-s + s1 - t2)) + 
    (8*(Power(-1 + s2,6)*(s2 - t1)*(-1 + t1)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         Power(s,12)*(-2*Power(t1,3) + Power(t1,2)*t2 + Power(t2,3)) + 
         Power(s,11)*(-5*Power(t1,4) + 
            (3 + s2*(-3 + 3*s1 - 11*t2) - 11*t2)*Power(t2,2) + 
            Power(t1,3)*(9 + 2*s1 + 18*s2 + 4*t2) + 
            t1*t2*(3 + s2*(-1 + t2) - 2*t2 + 3*Power(t2,2) + 
               s1*(-3 + 3*s2 + t2)) + 
            Power(t1,2)*(s1*(-3 + 4*s2 - t2) - 2*(-2 + s2*(5 + 6*t2)))) + 
         Power(s,10)*(-3*Power(t1,5) + 
            Power(t1,4)*(11 + 5*s1 + 42*s2 + 5*t2) + 
            Power(t1,3)*(34 - 72*Power(s2,2) + s1*(-25 + 3*s2 - 3*t2) - 
               10*t2 + Power(t2,2) - 3*s2*(27 + 14*t2)) + 
            t1*(3 + Power(s1,2)*s2*(-3 + 3*s2 - t2) - 34*t2 + 
               24*Power(t2,2) - 31*Power(t2,3) + 
               s1*(-3 + Power(s2,2)*(2 - 25*t2) + s2*(3 - 2*t2) + 
                  29*t2 - 6*Power(t2,2)) - 
               Power(s2,2)*(11 + 10*Power(t2,2)) + 
               s2*(2 + 11*t2 - 7*Power(t2,2) - 30*Power(t2,3))) + 
            t2*(3 - 30*t2 + 52*Power(t2,2) + 
               s2*(-3 + s1*(3 - 26*t2) - 2*t2 + 95*Power(t2,2)) + 
               Power(s2,2)*(1 + 3*Power(s1,2) + 28*t2 + 
                  55*Power(t2,2) - 3*s1*(1 + 10*t2))) + 
            Power(t1,2)*(-33 + Power(s1,2)*(1 - 5*s2) - 27*t2 - 
               2*Power(t2,2) + 3*Power(t2,3) + 
               7*Power(s2,2)*(11 + 9*t2) + 
               s2*(21 + 4*t2 + 3*Power(t2,2)) + 
               s1*(29 - 33*Power(s2,2) - 10*t2 + 3*Power(t2,2) + 
                  s2*(7 + 22*t2)))) + 
         Power(s,9)*(1 + Power(t1,6) + 
            Power(t1,5)*(3*s1 + 2*(-7 + t2)) - 27*t2 + 126*Power(t2,2) - 
            136*Power(t2,3) + 
            Power(t1,4)*(137 - 15*t2 + 2*Power(t2,2) - 2*s1*(22 + t2)) + 
            Power(t1,2)*(117 + 2*Power(s1,2)*(-8 + t2) + 64*t2 + 
               17*Power(t2,2) - 28*Power(t2,3) + 
               s1*(-109 + 128*t2 - 18*Power(t2,2))) + 
            Power(t1,3)*(-358 + 8*Power(s1,2) - 20*t2 - 9*Power(t2,2) + 
               Power(t2,3) + s1*(113 - 12*t2 + 3*Power(t2,2))) + 
            t1*(-29 + 158*t2 - 118*Power(t2,2) + 134*Power(t2,3) + 
               s1*(25 - 109*t2 + 4*Power(t2,2))) + 
            Power(s2,3)*(-4 + Power(s1,3) + 168*Power(t1,3) - 12*t2 - 
               117*Power(t2,2) - 165*Power(t2,3) - 
               Power(s1,2)*(23*t1 + 27*t2) - 
               Power(t1,2)*(259 + 192*t2) + 
               t1*(75 + 36*t2 + 45*Power(t2,2)) + 
               s1*(1 + 120*Power(t1,2) + 26*t2 + 135*Power(t2,2) + 
                  18*t1*(-1 + 5*t2))) + 
            s2*(27*Power(t1,5) - Power(t1,4)*(73 + 5*s1 + 47*t2) - 
               2*t2*(2 + s1*(11 - 47*t2) - 59*t2 + 168*Power(t2,2)) + 
               Power(t1,3)*(-150 - 23*Power(s1,2) + 77*t2 - 
                  5*Power(t2,2) + s1*(62 + 50*t2)) + 
               t1*(-23 - 13*t2 + 16*Power(t2,2) + 238*Power(t2,3) + 
                  2*Power(s1,2)*(11 + 8*t2) + 
                  s1*(4 - 117*t2 - 36*Power(t2,2))) + 
               Power(t1,2)*(177 + Power(s1,3) + 
                  Power(s1,2)*(26 - 4*t2) + 199*t2 - 40*Power(t2,2) - 
                  27*Power(t2,3) - s1*(183 + 16*t2 + 15*Power(t2,2)))) - 
            Power(s2,2)*(-1 + 154*Power(t1,4) + 
               Power(t1,3)*(-302 + 79*s1 - 192*t2) + 
               (-32 + 7*s1 + 22*Power(s1,2))*t2 + 
               (71 - 196*s1)*Power(t2,2) + 360*Power(t2,3) + 
               Power(t1,2)*(176 - 44*Power(s1,2) + 37*t2 + 
                  27*Power(t2,2) + 7*s1*(-4 + 19*t2)) + 
               t1*(-26 + 2*Power(s1,3) + Power(s1,2)*(2 - 17*t2) + 
                  101*t2 - 129*Power(t2,2) - 135*Power(t2,3) + 
                  s1*(10 - 66*t2 + 45*Power(t2,2))))) + 
         s*Power(-1 + s2,4)*(Power(s2,7)*Power(s1 - t2,2)*
             (-1 + s1 + t1 - t2) + 
            Power(s2,5)*(-1 + t1)*(s1 - t2)*
             (2 + Power(s1,2)*(-5 + 2*t1) - 13*t2 - 7*Power(t2,2) - 
               3*Power(t1,2)*(1 + t2) + 
               t1*(1 + 10*t2 + 3*Power(t2,2)) + 
               s1*(4 + 6*Power(t1,2) + 13*t2 - 2*t1*(2 + 3*t2))) - 
            Power(s2,6)*(s1 - t2)*
             (-1 + 3*Power(s1,2)*t1 - 2*t2 - Power(t1,2)*(1 + 3*t2) + 
               t1*(2 + 5*t2 + 3*Power(t2,2)) + 
               s1*(3 + 4*Power(t1,2) - t1*(7 + 6*t2))) - 
            (-1 + t1)*(4*Power(-1 + t2,3) - 
               t1*Power(-1 + t2,2)*(-31 + 8*s1 + 16*t2) + 
               (-1 + s1)*Power(t1,4)*
                (7 + 3*Power(s1,2) - 7*t2 + 3*s1*t2) + 
               Power(t1,2)*(-1 + t2)*
                (57 + 2*Power(s1,2) - 53*t2 + 3*Power(t2,2) + 
                  s1*(-40 + 37*t2)) + 
               Power(t1,3)*(37 + 2*Power(s1,3) + 
                  Power(s1,2)*(10 - 24*t2) - 51*t2 + 10*Power(t2,2) + 
                  s1*(-39 + 61*t2 - 6*Power(t2,2)))) + 
            Power(s2,3)*(-4 - 
               Power(s1,3)*(2 + 19*t1 - 54*Power(t1,2) + 
                  45*Power(t1,3) + 3*Power(t1,4)) + 13*t2 + 
               Power(t1,5)*t2 + 2*Power(t2,2) - 7*Power(t2,3) - 
               Power(t1,4)*(-3 + 12*t2 + Power(t2,2)) + 
               Power(t1,3)*(-5 + 68*t2 + 30*Power(t2,2)) + 
               t1*(9 + 21*t2 - 57*Power(t2,2) - 4*Power(t2,3)) + 
               Power(t1,2)*(-3 - 91*t2 + 26*Power(t2,2) + 
                  26*Power(t2,3)) + 
               Power(s1,2)*(5 + Power(t1,5) + t1*(49 - 18*t2) + 9*t2 + 
                  Power(t1,4)*(11 + 3*t2) - 
                  Power(t1,2)*(181 + 17*t2) + Power(t1,3)*(115 + 68*t2)\
) - s1*(-12 + Power(t1,4)*(6 - 5*t2) + 33*t2 - 5*Power(t2,2) + 
                  Power(t1,5)*(1 + t2) + 
                  t1*(78 - 71*t2 - 26*Power(t2,2)) + 
                  2*Power(t1,2)*(-56 - 61*t2 + 24*Power(t2,2)) + 
                  Power(t1,3)*(39 + 164*t2 + 28*Power(t2,2)))) - 
            s2*(Power(s1,3)*Power(t1,2)*
                (6 + 9*t1 - 21*Power(t1,2) + 8*Power(t1,3)) + 
               Power(-1 + t2,2)*(14 + t2) - Power(t1,5)*(2 + 5*t2) + 
               Power(t1,4)*(-1 + 14*t2 - 17*Power(t2,2)) + 
               t1*(-35 + 22*t2 + 47*Power(t2,2) - 34*Power(t2,3)) + 
               Power(t1,3)*(1 - 45*t2 + 73*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,2)*(23 + 41*t2 - 115*Power(t2,2) + 
                  39*Power(t2,3)) + 
               Power(s1,2)*t1*
                (t1*(19 - 61*t2) + 4*(-1 + t2) + 
                  Power(t1,3)*(41 + 3*t2) - Power(t1,4)*(31 + 4*t2) + 
                  Power(t1,2)*(-25 + 52*t2)) + 
               s1*(-8*Power(-1 + t2,2) + Power(t1,5)*(25 + 9*t2) + 
                  Power(t1,3)*(44 - 49*t2 - 52*Power(t2,2)) + 
                  Power(t1,2)*(-70 + 109*t2 + 9*Power(t2,2)) + 
                  Power(t1,4)*(-42 + 11*t2 + 12*Power(t2,2)) + 
                  t1*(51 - 96*t2 + 45*Power(t2,2)))) + 
            Power(s2,2)*(8 + 
               Power(s1,3)*t1*
                (6 + 21*t1 - 50*Power(t1,2) + 31*Power(t1,3) + 
                  Power(t1,4)) - 31*t2 + 31*Power(t2,2) - 
               8*Power(t2,3) + Power(t1,5)*(1 + t2) - 
               Power(t1,4)*(10 + 29*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(16 + 32*t2 - 36*Power(t2,2) - 
                  6*Power(t2,3)) - 
               t1*(17 - 39*t2 + 33*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(2 - 12*t2 + 43*Power(t2,2) + 
                  6*Power(t2,3)) - 
               Power(s1,2)*(2 + Power(t1,2)*(36 - 33*t2) - 2*t2 + 
                  Power(t1,5)*(5 + t2) - 2*Power(t1,3)*(72 + 5*t2) + 
                  Power(t1,4)*(103 + 27*t2) + t1*(-2 + 44*t2)) + 
               s1*(3 + 3*Power(t1,5) - 3*t2 + 
                  Power(t1,2)*(68 - 77*t2 - 48*Power(t2,2)) + 
                  Power(t1,4)*(65 + 72*t2 + 6*Power(t2,2)) + 
                  2*Power(t1,3)*(-68 + 2*t2 + 11*Power(t2,2)) + 
                  t1*(-3 + 4*t2 + 47*Power(t2,2)))) + 
            Power(s2,4)*(4 + Power(s1,3)*
                (6 - 27*t1 + 29*Power(t1,2) + 2*Power(t1,3)) - 22*t2 + 
               7*Power(t2,2) + 20*Power(t2,3) - 
               Power(t1,4)*t2*(3 + t2) + 
               t1*(-12 + 51*t2 + 33*Power(t2,2) - 22*Power(t2,3)) + 
               Power(t1,3)*(-4 + 13*t2 + 10*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(-12 + 39*t2 + 49*Power(t2,2) + 
                  9*Power(t2,3)) - 
               Power(s1,2)*(25 + 2*Power(t1,3) + 4*Power(t1,4) - 
                  10*t2 - t1*(79 + 31*t2) + Power(t1,2)*(48 + 71*t2)) + 
               s1*(8 + 28*t2 - 36*Power(t2,2) + Power(t1,4)*(3 + 5*t2) + 
                  Power(t1,3)*(1 - 18*t2 - 3*Power(t2,2)) + 
                  t1*(-9 - 142*t2 + 18*Power(t2,2)) + 
                  Power(t1,2)*(-3 + 127*t2 + 51*Power(t2,2))))) - 
         Power(s,2)*Power(-1 + s2,2)*
          (-20 + 186*t1 - 62*s1*t1 - 460*Power(t1,2) + 
            264*s1*Power(t1,2) - 18*Power(s1,2)*Power(t1,2) + 
            544*Power(t1,3) - 398*s1*Power(t1,3) + 
            6*Power(s1,2)*Power(t1,3) + 16*Power(s1,3)*Power(t1,3) - 
            383*Power(t1,4) + 295*s1*Power(t1,4) + 
            25*Power(s1,2)*Power(t1,4) - 14*Power(s1,3)*Power(t1,4) + 
            171*Power(t1,5) - 128*s1*Power(t1,5) - 
            13*Power(s1,2)*Power(t1,5) - 45*Power(t1,6) + 
            30*s1*Power(t1,6) - 4*Power(s1,2)*Power(t1,6) + 
            7*Power(t1,7) - s1*Power(t1,7) + 72*t2 - 492*t1*t2 + 
            128*s1*t1*t2 + 917*Power(t1,2)*t2 - 472*s1*Power(t1,2)*t2 + 
            16*Power(s1,2)*Power(t1,2)*t2 - 675*Power(t1,3)*t2 + 
            577*s1*Power(t1,3)*t2 - 105*Power(s1,2)*Power(t1,3)*t2 + 
            178*Power(t1,4)*t2 - 262*s1*Power(t1,4)*t2 + 
            97*Power(s1,2)*Power(t1,4)*t2 + 14*Power(t1,5)*t2 + 
            32*s1*Power(t1,5)*t2 - 15*Power(s1,2)*Power(t1,5)*t2 - 
            14*Power(t1,6)*t2 + 5*s1*Power(t1,6)*t2 - 84*Power(t2,2) + 
            417*t1*Power(t2,2) - 66*s1*t1*Power(t2,2) - 
            587*Power(t1,2)*Power(t2,2) + 
            234*s1*Power(t1,2)*Power(t2,2) + 
            292*Power(t1,3)*Power(t2,2) - 
            191*s1*Power(t1,3)*Power(t2,2) - 
            41*Power(t1,4)*Power(t2,2) + 31*s1*Power(t1,4)*Power(t2,2) - 
            Power(t1,5)*Power(t2,2) + 32*Power(t2,3) - 
            111*t1*Power(t2,3) + 92*Power(t1,2)*Power(t2,3) - 
            16*Power(t1,3)*Power(t2,3) + 
            Power(s2,8)*(8*Power(s1,3) + 
               Power(s1,2)*(-7 + 5*t1 - 27*t2) - 
               t2*(5 + 3*Power(t1,2) + 12*t2 + 11*Power(t2,2) - 
                  2*t1*(4 + 5*t2)) + 
               s1*(3 + Power(t1,2) + 19*t2 + 30*Power(t2,2) - 
                  t1*(4 + 15*t2))) + 
            Power(s2,7)*(-1 - Power(s1,3)*(1 + 23*t1) + 9*t2 - 
               11*Power(t2,2) + 3*Power(t2,3) + 
               2*Power(t1,3)*(1 + 5*t2) - 
               Power(t1,2)*(5 + 22*t2 + 27*Power(t2,2)) + 
               t1*(4 + 3*t2 + 51*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s1,2)*(-15 - 19*Power(t1,2) + 8*t2 + 
                  t1*(47 + 73*t2)) + 
               s1*(-2*Power(t1,3) + Power(t1,2)*(5 + 42*t2) + 
                  t1*(7 - 90*t2 - 80*Power(t2,2)) - 
                  2*(5 - 11*t2 + 5*Power(t2,2)))) - 
            Power(s2,2)*(2 - 5*Power(t1,7) + 
               2*Power(s1,3)*t1*
                (-16 - 6*t1 + 21*Power(t1,2) - 12*Power(t1,3) + 
                  Power(t1,4)) + Power(t1,6)*(-2 + t2) + 75*t2 - 
               43*Power(t2,2) + 4*Power(t2,3) + 
               Power(t1,5)*(91 + 54*t2 + 6*Power(t2,2)) - 
               2*Power(t1,4)*(94 + 49*t2 + 45*Power(t2,2)) + 
               t1*(-207 + 16*t2 + 23*Power(t2,2) - 13*Power(t2,3)) - 
               Power(t1,3)*(67 + 79*t2 - 289*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(376 + 31*t2 - 247*Power(t2,2) + 
                  63*Power(t2,3)) + 
               Power(s1,2)*(10 + 8*Power(t1,6) - 8*t2 + 
                  4*Power(t1,4)*(13 + 5*t2) - 
                  Power(t1,5)*(31 + 11*t2) - 
                  4*Power(t1,2)*(1 + 34*t2) + 
                  Power(t1,3)*(-179 + 108*t2) + t1*(82 + 113*t2)) + 
               s1*(6 + 3*Power(t1,7) - 64*t2 + 32*Power(t2,2) - 
                  Power(t1,6)*(34 + 3*t2) + Power(t1,5)*(4 + 39*t2) + 
                  Power(t1,3)*(240 - 306*t2 - 94*Power(t2,2)) + 
                  t1*(276 - 209*t2 - 75*Power(t2,2)) + 
                  3*Power(t1,2)*(-237 + 237*t2 + 5*Power(t2,2)) + 
                  Power(t1,4)*(216 - 44*t2 + 22*Power(t2,2)))) + 
            Power(s2,6)*(21 + 
               4*Power(s1,3)*(3 - 6*t1 + 4*Power(t1,2)) - 37*t2 - 
               89*Power(t2,2) - 18*Power(t2,3) - 
               Power(t1,4)*(7 + 12*t2) + Power(t1,3)*t2*(25 + 23*t2) - 
               Power(t1,2)*(-42 + 14*t2 + 115*Power(t2,2) + 
                  27*Power(t2,3)) + 
               t1*(-56 + 38*t2 + 167*Power(t2,2) + 39*Power(t2,3)) + 
               Power(s1,2)*(-50 + 27*Power(t1,3) - 48*t2 - 
                  12*Power(t1,2)*(6 + 5*t2) + t1*(81 + 94*t2)) + 
               s1*(1 - Power(t1,4) + Power(t1,3)*(31 - 34*t2) + 
                  128*t2 + 52*Power(t2,2) + 
                  t1*(64 - 210*t2 - 105*Power(t2,2)) + 
                  Power(t1,2)*(-95 + 144*t2 + 69*Power(t2,2)))) + 
            s2*(-13*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (-40 + 10*t1 + 35*Power(t1,2) - 19*Power(t1,3)) + 
               Power(t1,6)*(54 + 17*t2) + 
               Power(t1,4)*(-240 - 32*t2 + 3*Power(t2,2)) + 
               Power(t1,5)*(-29 - 8*t2 + 4*Power(t2,2)) - 
               9*(8 - 13*t2 + Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(455 + 113*t2 - 96*Power(t2,2) + 
                  24*Power(t2,3)) - 
               Power(t1,2)*(338 + 169*t2 - 324*Power(t2,2) + 
                  125*Power(t2,3)) + 
               t1*(183 - 38*t2 - 227*Power(t2,2) + 158*Power(t2,3)) + 
               Power(s1,2)*t1*
                (28 + 12*Power(t1,5) - 24*t2 + 
                  4*Power(t1,3)*(-18 + 5*t2) + 
                  Power(t1,4)*(54 + 5*t2) + 6*t1*(10 + 31*t2) - 
                  Power(t1,2)*(83 + 138*t2)) + 
               s1*(42 + 3*Power(t1,7) - 88*t2 + 46*Power(t2,2) + 
                  9*Power(t1,5)*(2 + t2) - Power(t1,6)*(56 + 5*t2) + 
                  Power(t1,4)*(347 - 151*t2 - 36*Power(t2,2)) - 
                  4*t1*(37 - 47*t2 + 23*Power(t2,2)) + 
                  5*Power(t1,3)*(-100 + 37*t2 + 34*Power(t2,2)) - 
                  2*Power(t1,2)*(-147 + 68*t2 + 72*Power(t2,2)))) + 
            Power(s2,3)*(-23 + Power(t1,7) + 
               Power(s1,3)*(-8 + 2*t1 - 6*Power(t1,2) - 
                  32*Power(t1,3) + 51*Power(t1,4) + 5*Power(t1,5)) - 
               65*t2 + 87*Power(t2,2) - 17*Power(t2,3) - 
               Power(t1,6)*(6 + t2) + 
               Power(t1,5)*(26 + 42*t2 + 4*Power(t2,2)) - 
               2*Power(t1,4)*(3 + 43*t2 + 15*Power(t2,2)) - 
               t1*(78 - 377*t2 + 351*Power(t2,2) + 6*Power(t2,3)) - 
               Power(t1,3)*(171 + 76*t2 + 172*Power(t2,2) + 
                  32*Power(t2,3)) + 
               Power(t1,2)*(257 - 191*t2 + 309*Power(t2,2) + 
                  46*Power(t2,3)) + 
               Power(s1,2)*(16 - Power(t1,5)*(-36 + t2) + 32*t2 - 
                  3*t1*(1 + 34*t2) + Power(t1,2)*(-17 + 60*t2) + 
                  Power(t1,3)*(212 + 82*t2) - 
                  Power(t1,4)*(397 + 104*t2)) + 
               s1*(Power(t1,7) - Power(t1,6)*(4 + 3*t2) - 
                  Power(t1,5)*(113 + 13*t2) + 
                  10*(13 - 15*t2 + Power(t2,2)) + 
                  Power(t1,4)*(470 + 348*t2 + 28*Power(t2,2)) + 
                  Power(t1,3)*(-75 - 172*t2 + 48*Power(t2,2)) - 
                  3*Power(t1,2)*(97 - 23*t2 + 52*Power(t2,2)) + 
                  t1*(-118 + 227*t2 + 100*Power(t2,2)))) - 
            Power(s2,4)*(-27 + 
               2*Power(s1,3)*
                (5 + t1 - 40*Power(t1,2) + 61*Power(t1,3) + 
                  8*Power(t1,4)) - 13*t2 - 6*Power(t2,2) + 
               15*Power(t2,3) + Power(t1,6)*(5 + t2) + 
               Power(t1,5)*(6 + Power(t2,2)) + 
               Power(t1,4)*(-36 + 136*t2 + 17*Power(t2,2)) - 
               Power(t1,3)*(5 + 403*t2 + 158*Power(t2,2)) + 
               Power(t1,2)*(42 + 101*t2 - 111*Power(t2,2) - 
                  110*Power(t2,3)) + 
               t1*(15 + 178*t2 + 107*Power(t2,2) + 5*Power(t2,3)) - 
               Power(s1,2)*(57 + 4*Power(t1,5) + 7*t2 + 
                  Power(t1,4)*(-47 + 7*t2) + t1*(26 + 23*t2) - 
                  2*Power(t1,2)*(309 + 34*t2) + 
                  Power(t1,3)*(728 + 261*t2)) + 
               s1*(88 + 4*Power(t1,6) + 39*t2 - 14*Power(t2,2) - 
                  Power(t1,5)*(46 + 11*t2) + 
                  Power(t1,4)*(-56 - 22*t2 + Power(t2,2)) - 
                  4*t1*(2 + 25*t2 + 2*Power(t2,2)) + 
                  Power(t1,3)*(681 + 777*t2 + 103*Power(t2,2)) + 
                  Power(t1,2)*(-663 - 383*t2 + 168*Power(t2,2)))) + 
            Power(s2,5)*(-10 + 
               Power(s1,3)*(15 - 65*t1 + 98*Power(t1,2) + 
                  10*Power(t1,3)) - 29*t2 + 69*Power(t2,2) + 
               66*Power(t2,3) + Power(t1,5)*(9 + 6*t2) + 
               Power(t1,4)*(12 - 10*t2 - 5*Power(t2,2)) + 
               t1*(-31 + 298*t2 + 63*Power(t2,2) - 118*Power(t2,3)) + 
               Power(t1,3)*(-102 + 121*t2 + 84*Power(t2,2) + 
                  8*Power(t2,3)) - 
               Power(t1,2)*(-122 + 386*t2 + 262*Power(t2,2) + 
                  33*Power(t2,3)) + 
               Power(s1,2)*(-103 - 17*Power(t1,4) + 20*t2 + 
                  Power(t1,3)*(51 + 8*t2) - 
                  10*Power(t1,2)*(44 + 27*t2) + t1*(458 + 49*t2)) + 
               s1*(104 + 5*Power(t1,5) + 44*t2 - 110*Power(t2,2) - 
                  Power(t1,4)*(71 + t2) + 
                  Power(t1,3)*(105 - 85*t2 - 18*Power(t2,2)) + 
                  t1*(-351 - 537*t2 + 160*Power(t2,2)) + 
                  Power(t1,2)*(208 + 681*t2 + 180*Power(t2,2))))) + 
         Power(s,8)*(-8 - (24 + s1)*Power(t1,6) + Power(t1,7) + 99*t2 - 
            282*Power(t2,2) + 206*Power(t2,3) + 
            Power(t1,5)*(204 + s1*(-16 + t2) - 5*t2 + Power(t2,2)) + 
            Power(t1,3)*(1167 - 2*Power(s1,3) + 8*t2 + 28*Power(t2,2) - 
               8*Power(t2,3) + Power(s1,2)*(-83 + 6*t2) + 
               s1*(-135 + 192*t2 - 16*Power(t2,2))) + 
            Power(t1,4)*(-853 + 18*Power(s1,2) + 27*t2 - 
               15*Power(t2,2) + s1*(114 - 21*t2 + Power(t2,2))) + 
            Power(t1,2)*(-208 + Power(s1,2)*(87 - 16*t2) + 59*t2 - 
               43*Power(t2,2) + 106*Power(t2,3) + 
               s1*(175 - 480*t2 + 18*Power(t2,2))) + 
            t1*(110 - 375*t2 + 286*Power(t2,2) - 309*Power(t2,3) + 
               s1*(-79 + 181*t2 + 64*Power(t2,2))) + 
            Power(s2,4)*(24 - 8*Power(s1,3) - 252*Power(t1,3) + 56*t2 + 
               288*Power(t2,2) + 330*Power(t2,3) + 
               7*Power(t1,2)*(71 + 54*t2) + 
               Power(s1,2)*(1 + 76*t1 + 108*t2) - 
               3*t1*(73 + 56*t2 + 40*Power(t2,2)) - 
               s1*(9 + 252*Power(t1,2) + 100*t2 + 360*Power(t2,2) + 
                  10*t1*(-7 + 18*t2))) + 
            Power(s2,3)*(1 + 322*Power(t1,4) + 
               Power(s1,3)*(-7 + 17*t1) - 110*t2 + 299*Power(t2,2) + 
               780*Power(t2,3) + 
               Power(s1,2)*(-3 - 164*Power(t1,2) + t1*(56 - 100*t2) + 
                  142*t2) - Power(t1,3)*(617 + 504*t2) + 
               2*Power(t1,2)*(191 + 74*t2 + 54*Power(t2,2)) - 
               t1*(101 - 232*t2 + 540*Power(t2,2) + 360*Power(t2,3)) + 
               s1*(9 + 301*Power(t1,3) - 23*t2 - 632*Power(t2,2) + 
                  8*Power(t1,2)*(-18 + 49*t2) + 
                  6*t1*(3 - 25*t2 + 40*Power(t2,2)))) + 
            Power(s2,2)*(4 - 99*Power(t1,5) - 
               2*Power(s1,3)*t1*(-9 + 5*t1) - 103*t2 - 
               111*Power(t2,2) + 902*Power(t2,3) + 
               3*Power(t1,4)*(58 + 63*t2) + 
               Power(t1,3)*(430 - 239*t2 + 4*Power(t2,2)) + 
               t1*(116 + 331*t2 - 621*Power(t2,2) - 789*Power(t2,3)) + 
               Power(t1,2)*(-578 - 521*t2 + 337*Power(t2,2) + 
                  108*Power(t2,3)) + 
               Power(s1,2)*(147*Power(t1,3) + 69*t2 + 
                  Power(t1,2)*(-223 + 36*t2) - t1*(47 + 167*t2)) + 
               s1*(-1 - 74*Power(t1,4) + Power(t1,3)*(75 - 257*t2) + 
                  66*t2 - 501*Power(t2,2) + 
                  Power(t1,2)*(461 + 216*t2 + 12*Power(t2,2)) + 
                  2*t1*(-51 + 188*t2 + 196*Power(t2,2)))) + 
            s2*(-10 + Power(s1,3)*Power(t1,2)*(-13 + 5*t1) + 
               Power(t1,5)*(65 - 18*t2) + 103*t2 - 444*Power(t2,2) + 
               614*Power(t2,3) + 
               Power(t1,4)*(-663 + 88*t2 - 13*Power(t2,2)) + 
               Power(t1,3)*(1719 + 87*t2 + 4*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-1044 - 367*t2 + 260*Power(t2,2) + 
                  186*Power(t2,3)) - 
               t1*(-112 + 71*t2 + 58*Power(t2,2) + 734*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-69 - 35*Power(t1,3) + Power(t1,2)*(116 - 8*t2) - 
                  81*t2 + t1*(47 + 39*t2)) + 
               s1*(1 + 67*t2 - 184*Power(t2,2) + 
                  Power(t1,4)*(69 + 43*t2) - 
                  Power(t1,3)*(442 + 65*t2 + 18*Power(t2,2)) + 
                  Power(t1,2)*(570 - 536*t2 + 30*Power(t2,2)) + 
                  t1*(-96 + 542*t2 + 242*Power(t2,2))))) + 
         Power(s,7)*(25 - (8 + s1)*Power(t1,7) - 183*t2 + 
            336*Power(t2,2) - 154*Power(t2,3) + 
            Power(t1,6)*(116 + s1*(13 + t2)) + 
            Power(t1,5)*(-770 + 15*Power(s1,2) + 28*t2 - 
               6*Power(t2,2) - s1*(9 + 25*t2)) + 
            t1*(-194 + 398*t2 - 273*Power(t2,2) + 384*Power(t2,3) + 
               s1*(103 - 27*t2 - 266*Power(t2,2))) + 
            Power(t1,4)*(2124 - 4*Power(s1,3) - 196*t2 + 
               46*Power(t2,2) + Power(s1,2)*(-132 + 7*t2) + 
               s1*(167 + 172*t2 - 4*Power(t2,2))) + 
            Power(t1,3)*(-1815 + 16*Power(s1,3) + 
               Power(s1,2)*(322 - 50*t2) + 385*t2 - 21*Power(t2,2) + 
               24*Power(t2,3) + s1*(-561 - 644*t2 + 14*Power(t2,2))) + 
            Power(t1,2)*(80 - 324*t2 - 52*Power(t2,2) - 
               203*Power(t2,3) + 14*Power(s1,2)*(-17 + 4*t2) + 
               s1*(24 + 803*t2 + 129*Power(t2,2))) + 
            Power(s2,5)*(28*Power(s1,3) + 252*Power(t1,3) - 
               7*Power(s1,2)*(1 + 20*t1 + 36*t2) - 
               7*Power(t1,2)*(85 + 72*t2) + 
               t1*(355 + 378*t2 + 210*Power(t2,2)) - 
               2*(30 + 70*t2 + 231*Power(t2,2) + 231*Power(t2,3)) + 
               s1*(33 + 336*Power(t1,2) + 224*t2 + 630*Power(t2,2) + 
                  14*t1*(-11 + 15*t2))) + 
            Power(s2,3)*(19 + 195*Power(t1,5) + 
               Power(s1,3)*(22 - 108*t1 + 41*Power(t1,2)) + 252*t2 - 
               189*Power(t2,2) - 1275*Power(t2,3) - 
               Power(t1,4)*(167 + 427*t2) + 
               4*Power(t1,3)*(-275 + 95*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(1440 + 592*t2 - 1071*Power(t2,2) - 
                  252*Power(t2,3)) + 
               t1*(-435 - 469*t2 + 2055*Power(t2,2) + 
                  1464*Power(t2,3)) + 
               Power(s1,2)*(3 - 393*Power(t1,3) + 
                  Power(t1,2)*(626 - 140*t2) - 301*t2 + 
                  t1*(41 + 638*t2)) + 
               s1*(-50 + 269*Power(t1,4) + 44*t2 + 1052*Power(t2,2) + 
                  Power(t1,3)*(-380 + 651*t2) + 
                  t1*(294 - 1210*t2 - 1235*Power(t2,2)) + 
                  Power(t1,2)*(-547 - 405*t2 + 84*Power(t2,2)))) + 
            s2*(53 - 3*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*(52 - 42*t1 + 7*Power(t1,2)) - 
               Power(t1,6)*(-83 + t2) - 287*t2 + 664*Power(t2,2) - 
               585*Power(t2,3) - 
               6*Power(t1,5)*(118 - 4*t2 + Power(t2,2)) + 
               Power(t1,4)*(2945 - 157*t2 + 53*Power(t2,2)) + 
               Power(t1,3)*(-4999 + 450*t2 + 182*Power(t2,2) + 
                  44*Power(t2,3)) - 
               Power(t1,2)*(-2458 + 342*t2 + 1005*Power(t2,2) + 
                  466*Power(t2,3)) + 
               t1*(-189 + 111*t2 + 449*Power(t2,2) + 
                  1080*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-21*Power(t1,4) + Power(t1,3)*(134 - 8*t2) + 
                  Power(t1,2)*(45 + 46*t2) + 4*(32 + 49*t2) - 
                  3*t1*(180 + 49*t2)) + 
               s1*(-3 + 7*Power(t1,6) - 118*t2 + 216*Power(t2,2) + 
                  Power(t1,5)*(-40 + 11*t2) + 
                  t1*(226 - 1083*t2 - 655*Power(t2,2)) - 
                  6*Power(t1,4)*(29 + 3*t2 + Power(t2,2)) + 
                  Power(t1,3)*(524 - 681*t2 + 48*Power(t2,2)) + 
                  Power(t1,2)*(-185 + 1999*t2 + 174*Power(t2,2)))) + 
            Power(s2,2)*(-3 - 15*Power(t1,6) + 
               Power(s1,3)*t1*(-70 + 108*t1 - 25*Power(t1,2)) + 43*t2 + 
               352*Power(t2,2) - 1050*Power(t2,3) + 
               2*Power(t1,5)*(-83 + 33*t2) + 
               Power(t1,4)*(1528 - 186*t2 + 35*Power(t2,2)) + 
               Power(t1,2)*(3112 + 157*t2 - 1584*Power(t2,2) - 
                  519*Power(t2,3)) + 
               Power(t1,3)*(-3718 - 15*t2 + 163*Power(t2,2) + 
                  28*Power(t2,3)) + 
               t1*(-790 - 121*t2 + 1627*Power(t2,2) + 
                  1594*Power(t2,3)) + 
               Power(s1,2)*(178*Power(t1,4) + 
                  Power(t1,2)*(99 - 315*t2) - 122*t2 + 
                  Power(t1,3)*(-665 + 48*t2) + t1*(227 + 542*t2)) + 
               s1*(-10 - 40*Power(t1,5) + Power(t1,4)*(169 - 211*t2) - 
                  55*t2 + 636*Power(t2,2) + 
                  t1*(457 - 1840*t2 - 1110*Power(t2,2)) + 
                  Power(t1,3)*(572 + 398*t2 + 42*Power(t2,2)) + 
                  Power(t1,2)*(-1128 + 1484*t2 + 177*Power(t2,2)))) + 
            Power(s2,4)*(11 + Power(s1,3)*(38 - 63*t1) - 
               420*Power(t1,4) + 199*t2 - 567*Power(t2,2) - 
               1050*Power(t2,3) + Power(t1,3)*(762 + 840*t2) - 
               2*Power(t1,2)*(145 + 168*t2 + 126*Power(t2,2)) + 
               t1*(23 - 263*t2 + 1176*Power(t2,2) + 630*Power(t2,3)) + 
               Power(s1,2)*(9 + 336*Power(t1,2) - 378*t2 + 
                  t1*(-123 + 308*t2)) - 
               s1*(567*Power(t1,3) + Power(t1,2)*(-263 + 658*t2) + 
                  10*t1*(2 + 63*Power(t2,2)) - 
                  2*(-18 + 57*t2 + 560*Power(t2,2))))) + 
         Power(s,3)*(-37 + 430*t1 - 191*s1*t1 - 1142*Power(t1,2) + 
            745*s1*Power(t1,2) - 82*Power(s1,2)*Power(t1,2) + 
            1668*Power(t1,3) - 1455*s1*Power(t1,3) + 
            34*Power(s1,2)*Power(t1,3) + 52*Power(s1,3)*Power(t1,3) - 
            1477*Power(t1,4) + 1505*s1*Power(t1,4) + 
            14*Power(s1,2)*Power(t1,4) - 50*Power(s1,3)*Power(t1,4) + 
            674*Power(t1,5) - 761*s1*Power(t1,5) + 
            27*Power(s1,2)*Power(t1,5) + 10*Power(s1,3)*Power(t1,5) - 
            128*Power(t1,6) + 153*s1*Power(t1,6) - 
            20*Power(s1,2)*Power(t1,6) + 12*Power(t1,7) - 
            7*s1*Power(t1,7) + 171*t2 - 1225*t1*t2 + 434*s1*t1*t2 + 
            2036*Power(t1,2)*t2 - 1150*s1*Power(t1,2)*t2 + 
            56*Power(s1,2)*Power(t1,2)*t2 - 1089*Power(t1,3)*t2 + 
            1112*s1*Power(t1,3)*t2 - 246*Power(s1,2)*Power(t1,3)*t2 - 
            8*Power(t1,4)*t2 - 378*s1*Power(t1,4)*t2 + 
            185*Power(s1,2)*Power(t1,4)*t2 + 178*Power(t1,5)*t2 - 
            s1*Power(t1,5)*t2 - 30*Power(s1,2)*Power(t1,5)*t2 - 
            52*Power(t1,6)*t2 + 21*s1*Power(t1,6)*t2 - 243*Power(t2,2) + 
            1079*t1*Power(t2,2) - 239*s1*t1*Power(t2,2) - 
            1372*Power(t1,2)*Power(t2,2) + 
            627*s1*Power(t1,2)*Power(t2,2) + 
            589*Power(t1,3)*Power(t2,2) - 
            414*s1*Power(t1,3)*Power(t2,2) - 
            58*Power(t1,4)*Power(t2,2) + 64*s1*Power(t1,4)*Power(t2,2) - 
            6*Power(t1,5)*Power(t2,2) + 109*Power(t2,3) - 
            291*t1*Power(t2,3) + 199*Power(t1,2)*Power(t2,3) - 
            32*Power(t1,3)*Power(t2,3) + 
            Power(s2,9)*(-4 + 28*Power(s1,3) + 2*Power(t1,3) - 36*t2 - 
               63*Power(t2,2) - 55*Power(t2,3) - 
               7*Power(t1,2)*(1 + 4*t2) + 
               9*t1*(1 + 7*t2 + 5*Power(t2,2)) + 
               s1*(19 + 12*Power(t1,2) + 80*t2 + 135*Power(t2,2) - 
                  15*t1*(2 + 3*t2)) + Power(s1,2)*(4*t1 - 3*(7 + 36*t2))\
) - Power(s2,2)*(49 + 42*Power(t1,7) + 
               Power(s1,3)*t1*
                (-38 - 158*t1 + 209*Power(t1,2) - 94*Power(t1,3) + 
                  28*Power(t1,4)) + 117*t2 + 164*Power(t2,2) - 
               78*Power(t2,3) - Power(t1,6)*(181 + 28*t2) + 
               Power(t1,4)*(640 - 764*t2 - 97*Power(t2,2)) + 
               2*Power(t1,5)*(26 + 101*t2 + 6*Power(t2,2)) + 
               Power(t1,3)*(-1256 + 471*t2 + 325*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,2)*(1580 + 113*t2 - 144*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(-926 + 29*t2 - 519*Power(t2,2) + 126*Power(t2,3)) + 
               Power(s1,2)*(20 - 8*Power(t1,6) + 
                  Power(t1,2)*(1253 - 103*t2) + 
                  Power(t1,4)*(770 - 28*t2) - 6*t2 + 
                  Power(t1,5)*(-218 + 8*t2) + 
                  Power(t1,3)*(-1667 + 76*t2) + t1*(-121 + 140*t2)) + 
               s1*(74 - 4*Power(t1,7) + Power(t1,5)*(110 - 46*t2) - 
                  369*t2 + 124*Power(t2,2) + 2*Power(t1,6)*(54 + t2) + 
                  Power(t1,2)*(-3658 + 1200*t2 - 201*Power(t2,2)) + 
                  t1*(1813 - 404*t2 - 34*Power(t2,2)) + 
                  Power(t1,4)*(-693 + 145*t2 + 8*Power(t2,2)) + 
                  Power(t1,3)*(2110 - 298*t2 + 30*Power(t2,2)))) - 
            Power(s2,3)*(155 - 28*Power(t1,7) + 
               Power(s1,3)*t1*
                (88 - 187*t1 + 208*Power(t1,2) - 90*Power(t1,3) + 
                  6*Power(t1,4)) + 334*t2 - 309*Power(t2,2) + 
               15*Power(t2,3) + 2*Power(t1,6)*(2 + 7*t2) + 
               Power(t1,5)*(699 + 112*t2 + 12*Power(t2,2)) - 
               Power(t1,4)*(1213 + 403*t2 + 166*Power(t2,2)) - 
               t1*(507 + 573*t2 - 845*Power(t2,2) + 8*Power(t2,3)) - 
               4*Power(t1,3)*
                (56 - 269*t2 - 84*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,2)*(1114 - 1006*t2 - 545*Power(t2,2) + 
                  88*Power(t2,3)) + 
               Power(s1,2)*(123 + 4*Power(t1,6) + 
                  Power(t1,4)*(326 - 74*t2) + 
                  Power(t1,5)*(80 - 12*t2) - 45*t2 + 
                  2*Power(t1,2)*(687 + 29*t2) + t1*(-727 + 34*t2) + 
                  Power(t1,3)*(-1379 + 132*t2)) + 
               s1*(-702 + 10*Power(t1,7) + 322*t2 - 32*Power(t2,2) - 
                  12*Power(t1,6)*(3 + t2) + 
                  Power(t1,5)*(-652 + 54*t2) + 
                  Power(t1,2)*(-1279 + 217*t2 - 96*Power(t2,2)) + 
                  t1*(1150 - 180*t2 - 49*Power(t2,2)) + 
                  Power(t1,3)*(1072 - 655*t2 - 32*Power(t2,2)) + 
                  Power(t1,4)*(883 + 280*t2 + 36*Power(t2,2)))) + 
            Power(s2,4)*(-152 - 64*Power(t1,6) - 2*Power(t1,7) + 
               Power(s1,3)*(16 - 117*t1 + 132*Power(t1,2) - 
                  18*Power(t1,3) + 88*Power(t1,4) + 6*Power(t1,5)) + 
               164*t2 + 53*Power(t2,2) - 6*Power(t2,3) + 
               2*Power(t1,5)*(220 + 105*t2 + 9*Power(t2,2)) - 
               2*Power(t1,4)*(-79 + 97*t2 + 32*Power(t2,2)) + 
               Power(t1,2)*(1346 + 26*t2 + 416*Power(t2,2) - 
                  15*Power(t2,3)) + 
               t1*(155 - 443*t2 - 377*Power(t2,2) + 28*Power(t2,3)) - 
               Power(t1,3)*(1881 + 347*t2 + 493*Power(t2,2) + 
                  72*Power(t2,3)) + 
               Power(s1,2)*(-133 + 4*Power(t1,6) + t1*(661 - 88*t2) + 
                  8*t2 + Power(t1,5)*(63 + 6*t2) - 
                  2*Power(t1,3)*(210 + 71*t2) - 
                  Power(t1,4)*(314 + 197*t2) + 
                  Power(t1,2)*(-544 + 226*t2)) + 
               s1*(3*Power(t1,7) + Power(t1,6)*(27 - 11*t2) - 
                  3*Power(t1,5)*(157 + 31*t2) - 
                  2*(-53 + 26*t2 + 8*Power(t2,2)) + 
                  Power(t1,4)*(-85 + 756*t2 + 56*Power(t2,2)) + 
                  t1*(-543 + 585*t2 + 62*Power(t2,2)) + 
                  2*Power(t1,3)*(1230 + 258*t2 + 83*Power(t2,2)) - 
                  Power(t1,2)*(913 + 571*t2 + 123*Power(t2,2)))) + 
            Power(s2,7)*(-1 + 9*Power(t1,5) + 
               Power(s1,3)*(20 - 18*t1 + 55*Power(t1,2)) - 266*t2 - 
               259*Power(t2,2) - 17*Power(t2,3) - 
               Power(t1,4)*(9 + 89*t2) + 
               4*Power(t1,3)*(-54 - 4*t2 + 19*Power(t2,2)) + 
               Power(t1,2)*(450 + 66*t2 - 397*Power(t2,2) - 
                  108*Power(t2,3)) + 
               t1*(-233 + 295*t2 + 421*Power(t2,2) + 24*Power(t2,3)) + 
               Power(s1,2)*(-143 + 13*Power(t1,3) - 89*t2 - 
                  14*Power(t1,2)*(13 + 14*t2) + t1*(181 + 82*t2)) + 
               s1*(140 + 19*Power(t1,4) + 410*t2 + 76*Power(t2,2) + 
                  Power(t1,3)*(210 + 17*t2) + 
                  t1*(10 - 526*t2 - 57*Power(t2,2)) + 
                  Power(t1,2)*(-369 + 389*t2 + 228*Power(t2,2)))) + 
            Power(s2,8)*(37 - 7*Power(t1,4) - 
               Power(s1,3)*(18 + 77*t1) + 105*t2 + 16*Power(t2,2) + 
               45*Power(t2,3) + Power(t1,3)*(17 + 84*t2) - 
               2*Power(t1,2)*(-9 + 44*t2 + 54*Power(t2,2)) + 
               t1*(-65 - 92*t2 + 210*Power(t2,2) + 135*Power(t2,3)) + 
               Power(s1,2)*(-17 - 16*Power(t1,2) + 98*t2 + 
                  t1*(139 + 260*t2)) - 
               s1*(74 + 31*Power(t1,3) + 22*t2 + 128*Power(t2,2) - 
                  Power(t1,2)*(4 + 83*t2) + 
                  t1*(-92 + 285*t2 + 315*Power(t2,2)))) + 
            s2*(-135 + 3*Power(t1,7) - 
               Power(s1,3)*Power(t1,2)*
                (90 + 36*t1 - 125*Power(t1,2) + 46*Power(t1,3)) + 
               187*t2 + 139*Power(t2,2) - 184*Power(t2,3) + 
               Power(t1,6)*(9 + 43*t2) + 
               2*Power(t1,5)*(19 - 29*t2 + 9*Power(t2,2)) - 
               Power(t1,4)*(679 + 407*t2 + 69*Power(t2,2)) + 
               Power(t1,3)*(1501 + 1354*t2 - 98*Power(t2,2) + 
                  36*Power(t2,3)) - 
               Power(t1,2)*(1020 + 1774*t2 - 821*Power(t2,2) + 
                  258*Power(t2,3)) + 
               t1*(283 + 646*t2 - 862*Power(t2,2) + 464*Power(t2,3)) + 
               Power(s1,2)*t1*
                (84 + 12*Power(t1,5) + Power(t1,2)*(385 - 2*t2) - 
                  44*t2 + 5*Power(t1,4)*(47 + 4*t2) - 
                  4*Power(t1,3)*(183 + 20*t2) + t1*(74 + 235*t2)) + 
               s1*(89 + 10*Power(t1,7) - 202*t2 + 109*Power(t2,2) - 
                  5*Power(t1,6)*(19 + 4*t2) + 
                  Power(t1,5)*(-124 + 33*t2) + 
                  Power(t1,2)*(1469 + 1025*t2 - 774*Power(t2,2)) + 
                  Power(t1,4)*(1756 + 16*t2 - 70*Power(t2,2)) + 
                  3*t1*(-48 - 124*t2 + 41*Power(t2,2)) + 
                  Power(t1,3)*(-2952 - 487*t2 + 472*Power(t2,2)))) + 
            Power(s2,5)*(227 + Power(t1,7) + 
               Power(s1,3)*(4 + 14*t1 + 8*Power(t1,2) - 
                  236*Power(t1,3) - 27*Power(t1,4)) + 
               Power(t1,6)*(11 - 5*t2) + 9*t2 - 30*Power(t2,2) - 
               81*Power(t2,3) - 
               2*Power(t1,5)*(40 + 27*t2 + 3*Power(t2,2)) - 
               Power(t1,4)*(457 + 571*t2 + 65*Power(t2,2)) + 
               Power(t1,3)*(921 + 1114*t2 + 326*Power(t2,2) - 
                  12*Power(t2,3)) + 
               t1*(-874 - 393*t2 + 25*Power(t2,2) + 144*Power(t2,3)) + 
               Power(t1,2)*(251 + 274*t2 + 471*Power(t2,2) + 
                  294*Power(t2,3)) + 
               Power(s1,2)*(91 - 15*Power(t1,5) - 8*t2 - 
                  2*Power(t1,4)*(61 + 5*t2) + t1*(-448 + 60*t2) + 
                  5*Power(t1,3)*(259 + 110*t2) + 
                  Power(t1,2)*(-14 + 163*t2)) - 
               s1*(306 + 13*Power(t1,6) + 166*t2 - 112*Power(t2,2) - 
                  Power(t1,5)*(100 + 69*t2) + 
                  6*Power(t1,4)*(-136 - 24*t2 + Power(t2,2)) + 
                  Power(t1,3)*(1946 + 1881*t2 + 184*Power(t2,2)) + 
                  t1*(-1438 - 395*t2 + 275*Power(t2,2)) + 
                  Power(t1,2)*(463 + 69*t2 + 510*Power(t2,2)))) + 
            Power(s2,6)*(-51 - 5*Power(t1,6) + 
               Power(s1,3)*(14 - 72*t1 + 190*Power(t1,2) + 
                  15*Power(t1,3)) + 117*t2 + 242*Power(t2,2) + 
               126*Power(t2,3) + 2*Power(t1,5)*(-5 + 19*t2) + 
               Power(t1,4)*(298 + 102*t2 - 7*Power(t2,2)) + 
               t1*(462 + 605*t2 - 215*Power(t2,2) - 386*Power(t2,3)) - 
               Power(t1,2)*(402 + 1405*t2 + 520*Power(t2,2) + 
                  21*Power(t2,3)) + 
               Power(t1,3)*(-292 + 447*t2 + 261*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(s1,2)*(-82 + 10*Power(t1,4) + t1*(771 - 96*t2) + 
                  48*t2 + Power(t1,3)*(127 + 48*t2) - 
                  Power(t1,2)*(1089 + 529*t2)) + 
               s1*(102 + 10*Power(t1,5) - 95*t2 - 196*Power(t2,2) - 
                  Power(t1,4)*(301 + 113*t2) - 
                  2*Power(t1,3)*(72 + 115*t2 + 21*Power(t2,2)) + 
                  Power(t1,2)*(1618 + 1710*t2 + 255*Power(t2,2)) + 
                  t1*(-1189 - 770*t2 + 618*Power(t2,2))))) + 
         Power(s,4)*(24 - 501*t1 + 295*s1*t1 + 1598*Power(t1,2) - 
            1226*s1*Power(t1,2) + 221*Power(s1,2)*Power(t1,2) - 
            2521*Power(t1,3) + 3140*s1*Power(t1,3) - 
            262*Power(s1,2)*Power(t1,3) - 90*Power(s1,3)*Power(t1,3) + 
            1753*Power(t1,4) - 3269*s1*Power(t1,4) + 
            226*Power(s1,2)*Power(t1,4) + 75*Power(s1,3)*Power(t1,4) - 
            311*Power(t1,5) + 1377*s1*Power(t1,5) - 
            147*Power(s1,2)*Power(t1,5) - 15*Power(s1,3)*Power(t1,5) - 
            64*Power(t1,6) - 225*s1*Power(t1,6) + 
            32*Power(s1,2)*Power(t1,6) + 3*Power(t1,7) + 
            16*s1*Power(t1,7) - 183*t2 + 1652*t1*t2 - 801*s1*t1*t2 - 
            2339*Power(t1,2)*t2 + 1442*s1*Power(t1,2)*t2 - 
            112*Power(s1,2)*Power(t1,2)*t2 + 443*Power(t1,3)*t2 - 
            904*s1*Power(t1,3)*t2 + 355*Power(s1,2)*Power(t1,3)*t2 + 
            700*Power(t1,4)*t2 + 85*s1*Power(t1,4)*t2 - 
            205*Power(s1,2)*Power(t1,4)*t2 - 397*Power(t1,5)*t2 + 
            117*s1*Power(t1,5)*t2 + 30*Power(s1,2)*Power(t1,5)*t2 + 
            72*Power(t1,6)*t2 - 34*s1*Power(t1,6)*t2 + 354*Power(t2,2) - 
            1527*t1*Power(t2,2) + 496*s1*t1*Power(t2,2) + 
            1793*Power(t1,2)*Power(t2,2) - 
            987*s1*Power(t1,2)*Power(t2,2) - 
            649*Power(t1,3)*Power(t2,2) + 
            497*s1*Power(t1,3)*Power(t2,2) + 
            15*Power(t1,4)*Power(t2,2) - 65*s1*Power(t1,4)*Power(t2,2) + 
            15*Power(t1,5)*Power(t2,2) - 199*Power(t2,3) + 
            396*t1*Power(t2,3) - 197*Power(t1,2)*Power(t2,3) + 
            24*Power(t1,3)*Power(t2,3) + 
            Power(s2,8)*(24 - 56*Power(s1,3) - 18*Power(t1,3) + 
               112*t2 + 192*Power(t2,2) + 165*Power(t2,3) + 
               7*Power(s1,2)*(5 + 4*t1 + 36*t2) + 
               Power(t1,2)*(59 + 117*t2) - 
               t1*(65 + 216*t2 + 120*Power(t2,2)) - 
               s1*(51 + 60*Power(t1,2) + 196*t2 + 360*Power(t2,2) - 
                  2*t1*(49 + 30*t2))) + 
            s2*(155 - 12*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (71 + 113*t1 - 168*Power(t1,2) + 40*Power(t1,3)) - 
               436*t2 + 34*Power(t1,6)*t2 + 157*Power(t2,2) + 
               111*Power(t2,3) - 
               3*Power(t1,5)*(-119 + 122*t2 + 4*Power(t2,2)) + 
               Power(t1,4)*(-186 + 1643*t2 + 197*Power(t2,2)) + 
               Power(t1,2)*(2496 + 741*t2 + 1725*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,3)*(-1749 - 2307*t2 - 1036*Power(t2,2) + 
                  8*Power(t2,3)) - 
               t1*(992 - 623*t2 + 896*Power(t2,2) + 138*Power(t2,3)) - 
               Power(s1,2)*t1*
                (147 + 4*Power(t1,5) + Power(t1,2)*(1750 - 76*t2) + 
                  11*t2 + 2*Power(t1,4)*(109 + t2) + 
                  Power(t1,3)*(-1348 + 13*t2) + 2*t1*(-311 + 61*t2)) + 
               s1*(-95 + 6*Power(t1,7) + 257*t2 - 144*Power(t2,2) - 
                  2*Power(t1,6)*(13 + 3*t2) + 
                  Power(t1,5)*(584 + 58*t2) - 
                  Power(t1,4)*(4201 + 177*t2 + 12*Power(t2,2)) + 
                  Power(t1,3)*(8128 + 365*t2 + 22*Power(t2,2)) + 
                  2*Power(t1,2)*(-2466 - 551*t2 + 48*Power(t2,2)) + 
                  t1*(396 + 702*t2 + 56*Power(t2,2)))) + 
            Power(s2,3)*(-128 + 8*Power(t1,7) + 
               Power(s1,3)*(-31 + 249*t1 - 578*Power(t1,2) + 
                  494*Power(t1,3) - 130*Power(t1,4) + 2*Power(t1,5)) + 
               309*t2 + 339*Power(t2,2) - 14*Power(t2,3) + 
               6*Power(t1,6)*(14 + t2) - 
               2*Power(t1,5)*(193 + 166*t2 + 6*Power(t2,2)) + 
               Power(t1,4)*(-1255 + 1657*t2 + 210*Power(t2,2)) + 
               Power(t1,3)*(3587 - 3198*t2 - 732*Power(t2,2) + 
                  48*Power(t2,3)) + 
               t1*(931 - 1278*t2 - 1340*Power(t2,2) + 52*Power(t2,3)) - 
               2*Power(t1,2)*
                (1340 - 1371*t2 - 713*Power(t2,2) + 67*Power(t2,3)) + 
               Power(s1,2)*(302 - 12*Power(t1,6) + 
                  Power(t1,5)*(80 - 14*t2) + 21*t2 - 
                  2*t1*(829 + 70*t2) - 4*Power(t1,3)*(364 + 107*t2) + 
                  Power(t1,4)*(11 + 221*t2) + 
                  Power(t1,2)*(2734 + 274*t2)) + 
               s1*(-655 - 2*Power(t1,7) - 357*t2 - 40*Power(t2,2) + 
                  2*Power(t1,6)*(-41 + 7*t2) + 
                  Power(t1,5)*(476 + 54*t2) + 
                  Power(t1,4)*(76 - 984*t2 - 44*Power(t2,2)) - 
                  2*Power(t1,2)*(1411 + 1175*t2 + 6*Power(t2,2)) + 
                  2*t1*(1157 + 842*t2 + 54*Power(t2,2)) + 
                  Power(t1,3)*(481 + 2126*t2 + 104*Power(t2,2)))) + 
            Power(s2,2)*(284 - 28*Power(t1,7) + 
               Power(s1,3)*t1*
                (46 - 360*t1 + 510*Power(t1,2) - 249*Power(t1,3) + 
                  21*Power(t1,4)) - 472*t2 + 771*Power(t2,2) - 
               15*Power(t2,3) + Power(t1,6)*(30 + 38*t2) + 
               Power(t1,5)*(592 - 346*t2 - 6*Power(t2,2)) + 
               2*Power(t1,4)*(-791 + 748*t2 + 55*Power(t2,2)) + 
               Power(t1,2)*(56 + 2703*t2 + 1605*Power(t2,2) - 
                  54*Power(t2,3)) + 
               Power(t1,3)*(1380 - 2871*t2 - 706*Power(t2,2) + 
                  16*Power(t2,3)) + 
               t1*(-857 - 177*t2 - 1841*Power(t2,2) + 22*Power(t2,3)) + 
               Power(s1,2)*(20 - 16*Power(t1,6) + 
                  Power(t1,2)*(2993 - 26*t2) + 
                  Power(t1,5)*(61 - 14*t2) + 27*t2 + 
                  16*t1*(-57 + 2*t2) + 3*Power(t1,4)*(346 + 25*t2) - 
                  Power(t1,3)*(2993 + 118*t2)) + 
               s1*(30 + 12*Power(t1,7) - 438*t2 - 16*Power(t2,2) - 
                  2*Power(t1,6)*(31 + 3*t2) + 
                  Power(t1,5)*(-185 + 108*t2) - 
                  Power(t1,4)*(1259 + 492*t2 + 22*Power(t2,2)) + 
                  Power(t1,3)*(5453 + 1029*t2 + 94*Power(t2,2)) - 
                  Power(t1,2)*(7615 + 1030*t2 + 102*Power(t2,2)) + 
                  t1*(3585 + 592*t2 + 145*Power(t2,2)))) + 
            Power(s2,4)*(-268 - 3*Power(t1,7) + 
               Power(s1,3)*(-57 + 273*t1 - 458*Power(t1,2) + 
                  344*Power(t1,3) + 8*Power(t1,4)) + 2*t2 + 
               420*Power(t2,2) - 20*Power(t2,3) + 
               2*Power(t1,6)*(-37 + 5*t2) + 
               Power(t1,5)*(290 + 71*t2 + 15*Power(t2,2)) + 
               Power(t1,4)*(325 + 1021*t2 + 115*Power(t2,2)) + 
               Power(t1,2)*(-2333 + 2812*t2 + 1551*Power(t2,2) - 
                  145*Power(t2,3)) - 
               Power(t1,3)*(-325 + 3203*t2 + 1201*Power(t2,2) + 
                  40*Power(t2,3)) + 
               t1*(1601 - 1150*t2 - 1401*Power(t2,2) + 
                  120*Power(t2,3)) + 
               Power(s1,2)*(299 + 80*Power(t1,5) + 71*t2 + 
                  10*Power(t1,4)*(-17 + 5*t2) + 
                  19*Power(t1,2)*(61 + 42*t2) - t1*(967 + 354*t2) - 
                  Power(t1,3)*(1022 + 789*t2)) + 
               s1*(11*Power(t1,6) + Power(t1,5)*(14 - 145*t2) + 
                  3*Power(t1,4)*(-404 - 19*t2 + 5*Power(t2,2)) - 
                  2*(64 + 261*t2 + 20*Power(t2,2)) - 
                  3*Power(t1,2)*(94 + 1184*t2 + 49*Power(t2,2)) + 
                  t1*(134 + 2189*t2 + 86*Power(t2,2)) + 
                  Power(t1,3)*(2084 + 3182*t2 + 265*Power(t2,2)))) + 
            Power(s2,7)*(-137 + 54*Power(t1,4) + 
               Power(s1,3)*(-25 + 147*t1) - 222*t2 + 81*Power(t2,2) + 
               60*Power(t2,3) - 3*Power(t1,3)*(29 + 104*t2) + 
               14*Power(t1,2)*(-11 + 18*t2 + 18*Power(t2,2)) + 
               t1*(323 + 196*t2 - 684*Power(t2,2) - 360*Power(t2,3)) + 
               Power(s1,2)*(-84*Power(t1,2) + 7*(5 + 6*t2) - 
                  76*t1*(3 + 7*t2)) + 
               s1*(125 + 159*Power(t1,3) - 33*t2 - 56*Power(t2,2) + 
                  4*Power(t1,2)*(-7 + 2*t2) + 
                  2*t1*(-85 + 327*t2 + 360*Power(t2,2)))) + 
            Power(s2,5)*(-136 + 24*Power(t1,6) + 
               Power(s1,3)*(-58 + 246*t1 - 393*Power(t1,2) + 
                  5*Power(t1,3)) + Power(t1,5)*(139 - 102*t2) + 
               237*t2 + 359*Power(t2,2) - 109*Power(t2,3) - 
               Power(t1,4)*(1099 + 180*t2 + 7*Power(t2,2)) + 
               t1*t2*(-1485 - 1000*t2 + 286*Power(t2,2)) - 
               Power(t1,3)*(-1097 + 991*t2 + 656*Power(t2,2) + 
                  56*Power(t2,3)) + 
               Power(t1,2)*(34 + 2785*t2 + 2013*Power(t2,2) + 
                  273*Power(t2,3)) + 
               Power(s1,2)*(265 - 165*Power(t1,4) + 
                  Power(t1,3)*(130 - 120*t2) + 97*t2 + 
                  4*Power(t1,2)*(377 + 282*t2) - t1*(1089 + 397*t2)) + 
               s1*(-137 + 22*Power(t1,5) - 747*t2 + 80*Power(t2,2) + 
                  Power(t1,4)*(347 + 377*t2) + 
                  t1*(1350 + 2576*t2 - 284*Power(t2,2)) + 
                  Power(t1,3)*(624 + 277*t2 + 42*Power(t2,2)) - 
                  2*Power(t1,2)*(1265 + 1922*t2 + 366*Power(t2,2)))) + 
            Power(s2,6)*(6 - 57*Power(t1,5) + 
               Power(s1,3)*(-45 + 175*t1 - 106*Power(t1,2)) + 429*t2 + 
               527*Power(t2,2) + 21*Power(t2,3) + 
               7*Power(t1,4)*(-6 + 41*t2) + 
               Power(t1,3)*(994 + 23*t2 - 140*Power(t2,2)) + 
               Power(t1,2)*(-1476 + 11*t2 + 1155*Power(t2,2) + 
                  252*Power(t2,3)) - 
               t1*(-568 + 661*t2 + 1431*Power(t2,2) + 378*Power(t2,3)) + 
               Power(s1,2)*(244 + 153*Power(t1,3) + 130*t2 + 
                  Power(t1,2)*(159 + 364*t2) - t1*(483 + 646*t2)) - 
               s1*(225 + 130*Power(t1,4) + 812*t2 + 64*Power(t2,2) + 
                  21*Power(t1,3)*(17 + 15*t2) + 
                  Power(t1,2)*(-455 + 804*t2 + 420*Power(t2,2)) - 
                  t1*(174 + 1752*t2 + 721*Power(t2,2))))) - 
         Power(s,6)*(36 - (19 + 7*s1)*Power(t1,7) - 153*t2 + 
            126*Power(t2,2) + 28*Power(t2,3) + 
            Power(t1,6)*(265 - 4*Power(s1,2) - 10*t2 + s1*(56 + 9*t2)) + 
            Power(t1,5)*(-1366 + 2*Power(s1,3) - 
               3*Power(s1,2)*(-29 + t2) + 160*t2 - 15*Power(t2,2) - 
               2*s1*(158 + 53*t2)) + 
            t1*(-95 - 182*t2 + 283*Power(t2,2) + 168*Power(t2,3) + 
               s1*(-13 + 433*t2 - 532*Power(t2,2))) + 
            Power(t1,4)*(2410 - 24*Power(s1,3) - 713*t2 + 
               73*Power(t2,2) + Power(s1,2)*(-367 + 47*t2) + 
               s1*(1530 + 412*t2 + Power(t2,2))) + 
            Power(t1,3)*(-880 + 52*Power(s1,3) + 
               Power(s1,2)*(606 - 171*t2) + 997*t2 + 109*Power(t2,2) + 
               32*Power(t2,3) - s1*(2341 + 856*t2 + 93*Power(t2,2))) + 
            Power(t1,2)*(-499 - 32*t2 - 539*Power(t2,2) - 
               181*Power(t2,3) + Power(s1,2)*(-377 + 112*t2) + 
               s1*(615 + 406*t2 + 525*Power(t2,2))) + 
            Power(s2,6)*(56*Power(s1,3) + 168*Power(t1,3) - 
               7*Power(s1,2)*(3 + 22*t1 + 54*t2) - 
               7*Power(t1,2)*(65 + 66*t2) + 
               3*t1*(115 + 168*t2 + 84*Power(t2,2)) - 
               2*(40 + 105*t2 + 252*Power(t2,2) + 231*Power(t2,3)) + 
               s1*(65 + 294*Power(t1,2) + 322*t2 + 756*Power(t2,2) + 
                  42*t1*(-5 + 3*t2))) + 
            Power(s2,5)*(89 + Power(s1,3)*(81 - 133*t1) - 
               350*Power(t1,4) + 254*t2 - 595*Power(t2,2) - 
               882*Power(t2,3) + Power(t1,3)*(589 + 924*t2) + 
               Power(t1,2)*(94 - 476*t2 - 378*Power(t2,2)) + 
               2*t1*(-162 - 110*t2 + 777*Power(t2,2) + 
                  378*Power(t2,3)) + 
               Power(s1,2)*(1 + 406*Power(t1,2) - 518*t2 + 
                  t1*(-52 + 574*t2)) + 
               s1*(-74 - 623*Power(t1,3) + Power(t1,2)*(248 - 644*t2) + 
                  187*t2 + 1148*Power(t2,2) - 
                  14*t1*(-2 + 33*t2 + 72*Power(t2,2)))) + 
            Power(s2,4)*(75 + 225*Power(t1,5) + 
               4*Power(s1,3)*(19 - 65*t1 + 23*Power(t1,2)) + 93*t2 - 
               591*Power(t2,2) - 973*Power(t2,3) - 
               5*Power(t1,4)*(2 + 119*t2) + 
               Power(t1,3)*(-1914 + 313*t2 + 98*Power(t2,2)) + 
               Power(t1,2)*(2421 + 220*t2 - 1841*Power(t2,2) - 
                  378*Power(t2,3)) + 
               t1*(-893 + 60*t2 + 3209*Power(t2,2) + 
                  1638*Power(t2,3)) + 
               Power(s1,2)*(-41 - 559*Power(t1,3) + 
                  Power(t1,2)*(771 - 308*t2) - 491*t2 + 
                  2*t1*(91 + 610*t2)) + 
               s1*(-62 + 410*Power(t1,4) + 463*t2 + 1058*Power(t2,2) + 
                  Power(t1,3)*(-358 + 931*t2) + 
                  t1*(191 - 2470*t2 - 1953*Power(t2,2)) + 
                  Power(t1,2)*(-327 - 50*t2 + 294*Power(t2,2)))) + 
            s2*(82 - 17*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (82 - 128*t1 + 43*Power(t1,2) - 3*Power(t1,3)) + 
               Power(t1,6)*(220 - 3*t2) - 216*t2 + 323*Power(t2,2) - 
               233*Power(t2,3) + 
               Power(t1,5)*(-1561 + 124*t2 - 18*Power(t2,2)) + 
               Power(t1,4)*(5272 - 1080*t2 + 35*Power(t2,2)) + 
               Power(t1,3)*(-6926 + 2695*t2 + 776*Power(t2,2) + 
                  72*Power(t2,3)) - 
               Power(t1,2)*(-2481 + 1341*t2 + 2387*Power(t2,2) + 
                  483*Power(t2,3)) + 
               t1*(145 - 363*t2 + 1494*Power(t2,2) + 694*Power(t2,3)) + 
               Power(s1,2)*t1*
                (169 + 4*Power(t1,5) + 251*t2 + 
                  Power(t1,4)*(-56 + 3*t2) - 
                  2*Power(t1,3)*(16 + 13*t2) + 
                  Power(t1,2)*(1133 + 64*t2) - t1*(1343 + 246*t2)) + 
               s1*(8 - 3*Power(t1,7) - 161*t2 + 176*Power(t2,2) + 
                  Power(t1,6)*(66 + t2) - 3*Power(t1,5)*(64 + 13*t2) + 
                  t1*(61 - 1230*t2 - 888*Power(t2,2)) + 
                  Power(t1,4)*(447 + 427*t2 - 10*Power(t2,2)) - 
                  Power(t1,3)*(2331 + 2003*t2 + 44*Power(t2,2)) + 
                  Power(t1,2)*(2447 + 2950*t2 + 660*Power(t2,2)))) + 
            Power(s2,3)*(195 - 40*Power(t1,6) + 
               Power(s1,3)*(42 - 284*t1 + 324*Power(t1,2) - 
                  48*Power(t1,3)) - 126*t2 - 478*Power(t2,2) - 
               793*Power(t2,3) + 5*Power(t1,5)*(-57 + 26*t2) + 
               Power(t1,4)*(2196 - 138*t2 + 49*Power(t2,2)) + 
               Power(t1,3)*(-4508 + 487*t2 + 558*Power(t2,2) + 
                  56*Power(t2,3)) - 
               Power(t1,2)*(-4249 + 1530*t2 + 3425*Power(t2,2) + 
                  777*Power(t2,3)) + 
               t1*(-1757 + 859*t2 + 3540*Power(t2,2) + 
                  1646*Power(t2,3)) + 
               Power(s1,2)*(361*Power(t1,4) + 
                  Power(t1,2)*(113 - 914*t2) + 
                  3*Power(t1,3)*(-413 + 40*t2) - 6*(9 + 56*t2) + 
                  3*t1*(171 + 416*t2)) + 
               s1*(-116 - 95*Power(t1,5) + 560*t2 + 752*Power(t2,2) - 
                  5*Power(t1,4)*(-81 + 95*t2) + 
                  t1*(554 - 3879*t2 - 1790*Power(t2,2)) + 
                  Power(t1,3)*(5 + 613*t2 + 42*Power(t2,2)) + 
                  Power(t1,2)*(-427 + 3323*t2 + 690*Power(t2,2)))) + 
            Power(s2,2)*(-2*Power(t1,7) + 
               2*Power(s1,3)*t1*
                (-72 + 178*t1 - 103*Power(t1,2) + 12*Power(t1,3)) + 
               Power(t1,6)*(131 - 5*t2) + 
               t2*(68 - 193*t2 - 525*Power(t2,2)) + 
               Power(t1,5)*(-1006 + 30*t2 - 15*Power(t2,2)) + 
               Power(t1,4)*(3859 - 496*t2 + 43*Power(t2,2)) + 
               Power(t1,3)*(-7809 + 2336*t2 + 1019*Power(t2,2) + 
                  96*Power(t2,3)) + 
               2*t1*(-823 + 325*t2 + 1482*Power(t2,2) + 
                  621*Power(t2,3)) - 
               Power(t1,2)*(-6191 + 2745*t2 + 3606*Power(t2,2) + 
                  741*Power(t2,3)) - 
               Power(s1,2)*(2 + 84*Power(t1,5) + 
                  Power(t1,3)*(359 - 307*t2) + 133*t2 + 
                  44*Power(t1,2)*(27 + 19*t2) + 
                  Power(t1,4)*(-589 + 37*t2) - t1*(677 + 771*t2)) + 
               s1*(-44 + 14*Power(t1,6) + 191*t2 + 438*Power(t2,2) + 
                  23*Power(t1,5)*(-10 + 3*t2) - 
                  Power(t1,3)*(-396 + 1605*t2 + Power(t2,2)) - 
                  3*Power(t1,4)*(-79 + 66*t2 + 5*Power(t2,2)) + 
                  Power(t1,2)*(224 + 4757*t2 + 759*Power(t2,2)) - 
                  t1*(20 + 3154*t2 + 1349*Power(t2,2))))) + 
         Power(s,5)*(15 + 238*t1 - 217*s1*t1 - 1291*Power(t1,2) + 
            1199*s1*Power(t1,2) - 366*Power(s1,2)*Power(t1,2) + 
            1394*Power(t1,3) - 3748*s1*Power(t1,3) + 
            584*Power(s1,2)*Power(t1,3) + 90*Power(s1,3)*Power(t1,3) + 
            377*Power(t1,4) + 3310*s1*Power(t1,4) - 
            461*Power(s1,2)*Power(t1,4) - 59*Power(s1,3)*Power(t1,4) - 
            957*Power(t1,5) - 1034*s1*Power(t1,5) + 
            179*Power(s1,2)*Power(t1,5) + 9*Power(s1,3)*Power(t1,5) + 
            295*Power(t1,6) + 146*s1*Power(t1,6) - 
            20*Power(s1,2)*Power(t1,6) - 20*Power(t1,7) - 
            16*s1*Power(t1,7) + 27*t2 - 1164*t1*t2 + 841*s1*t1*t2 + 
            1236*Power(t1,2)*t2 - 711*s1*Power(t1,2)*t2 + 
            140*Power(s1,2)*Power(t1,2)*t2 + 753*Power(t1,3)*t2 - 
            146*s1*Power(t1,3)*t2 - 318*Power(s1,2)*Power(t1,3)*t2 - 
            1103*Power(t1,4)*t2 + 359*s1*Power(t1,4)*t2 + 
            133*Power(s1,2)*Power(t1,4)*t2 + 375*Power(t1,5)*t2 - 
            176*s1*Power(t1,5)*t2 - 15*Power(s1,2)*Power(t1,5)*t2 - 
            44*Power(t1,6)*t2 + 26*s1*Power(t1,6)*t2 - 210*Power(t2,2) + 
            1159*t1*Power(t2,2) - 644*s1*t1*Power(t2,2) - 
            1354*Power(t1,2)*Power(t2,2) + 
            945*s1*Power(t1,2)*Power(t2,2) + 
            399*Power(t1,3)*Power(t2,2) - 
            328*s1*Power(t1,3)*Power(t2,2) + 52*Power(t1,4)*Power(t2,2) + 
            30*s1*Power(t1,4)*Power(t2,2) - 20*Power(t1,5)*Power(t2,2) + 
            188*Power(t2,3) - 210*t1*Power(t2,3) + 
            13*Power(t1,2)*Power(t2,3) + 10*Power(t1,3)*Power(t2,3) + 
            Power(s2,7)*(70*Power(s1,3) + 72*Power(t1,3) - 
               7*Power(s1,2)*(5 + 14*t1 + 54*t2) - 
               Power(t1,2)*(217 + 288*t2) + 
               3*t1*(67 + 140*t2 + 70*Power(t2,2)) + 
               s1*(75 - 182*t1 + 168*Power(t1,2) + 308*t2 + 
                  630*Power(t2,2)) - 
               2*(30 + 98*t2 + 189*Power(t2,2) + 165*Power(t2,3))) + 
            Power(s2,6)*(Power(s1,3)*(80 - 175*t1) - 182*Power(t1,4) + 
               Power(t1,3)*(286 + 672*t2) + 
               Power(s1,2)*(-25 + 143*t1 + 280*Power(t1,2) - 350*t2 + 
                  686*t1*t2) + 
               Power(t1,2)*(284 - 434*t2 - 378*Power(t2,2)) - 
               7*(-25 - 39*t2 + 49*Power(t2,2) + 60*Power(t2,3)) + 
               t1*(-528 - 221*t2 + 1302*Power(t2,2) + 630*Power(t2,3)) + 
               s1*(-115 - 413*Power(t1,3) + Power(t1,2)*(123 - 322*t2) + 
                  142*t2 + 616*Power(t2,2) - 
                  6*t1*(-22 + 133*t2 + 175*Power(t2,2)))) - 
            Power(s2,2)*(149 + 14*Power(t1,7) + 
               Power(s1,3)*t1*
                (148 - 513*t1 + 508*Power(t1,2) - 133*Power(t1,3) + 
                  7*Power(t1,4)) + 8*Power(t1,6)*(-11 + t2) - 493*t2 + 
               1004*Power(t2,2) + 64*Power(t2,3) + 
               Power(t1,5)*(783 - 269*t2 + 12*Power(t2,2)) + 
               Power(t1,4)*(-4168 + 2175*t2 + 151*Power(t2,2)) - 
               2*Power(t1,3)*
                (-3726 + 2724*t2 + 851*Power(t2,2) + 24*Power(t2,3)) - 
               t1*(-971 + 728*t2 + 3440*Power(t2,2) + 222*Power(t2,3)) + 
               Power(t1,2)*(-5242 + 4639*t2 + 4082*Power(t2,2) + 
                  234*Power(t2,3)) + 
               Power(s1,2)*(10 - 12*Power(t1,6) + 
                  Power(t1,5)*(177 - 11*t2) + 88*t2 + 
                  Power(t1,4)*(-245 + 139*t2) - 
                  2*Power(t1,3)*(905 + 227*t2) - t1*(1172 + 441*t2) + 
                  Power(t1,2)*(2974 + 709*t2)) + 
               s1*(47 + 2*Power(t1,7) - 432*t2 - 188*Power(t2,2) + 
                  2*Power(t1,6)*(-56 + 3*t2) + 
                  Power(t1,5)*(395 + 14*t2) - 
                  Power(t1,4)*(395 + 710*t2 + 6*Power(t2,2)) + 
                  Power(t1,3)*(2997 + 3220*t2 + 184*Power(t2,2)) - 
                  2*Power(t1,2)*(2483 + 2372*t2 + 378*Power(t2,2)) + 
                  t1*(2307 + 2554*t2 + 706*Power(t2,2)))) - 
            s2*(16 + Power(s1,3)*Power(t1,2)*
                (-27 + 177*t1 - 123*Power(t1,2) + 14*Power(t1,3)) - 
               221*t2 + 186*Power(t2,2) + 19*Power(t2,3) + 
               2*Power(t1,6)*(-86 + 17*t2) + 
               Power(t1,5)*(1487 - 441*t2 + 12*Power(t2,2)) + 
               5*Power(t1,4)*(-822 + 463*t2 + 21*Power(t2,2)) - 
               Power(t1,3)*(-3393 + 3910*t2 + 1362*Power(t2,2) + 
                  28*Power(t2,3)) - 
               t1*(866 - 1049*t2 + 2119*Power(t2,2) + 88*Power(t2,3)) + 
               Power(t1,2)*(326 + 1054*t2 + 3199*Power(t2,2) + 
                  126*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-8*Power(t1,5) + 16*Power(t1,2)*(-143 + t2) - 
                  Power(t1,4)*(15 + 4*t2) + Power(t1,3)*(853 + 9*t2) - 
                  2*(89 + 78*t2) + t1*(1469 + 137*t2)) + 
               s1*(-50 + 14*Power(t1,7) + 218*t2 - 146*Power(t2,2) - 
                  12*Power(t1,6)*(10 + t2) + 
                  Power(t1,5)*(415 + 147*t2) - 
                  Power(t1,4)*(2829 + 705*t2 + 16*Power(t2,2)) + 
                  Power(t1,3)*(7594 + 1723*t2 + 216*Power(t2,2)) + 
                  t1*(339 + 991*t2 + 571*Power(t2,2)) - 
                  Power(t1,2)*(5585 + 2121*t2 + 708*Power(t2,2)))) + 
            Power(s2,3)*(424 + 2*Power(t1,7) + 
               Power(s1,3)*(51 - 377*t1 + 686*Power(t1,2) - 
                  365*Power(t1,3) + 23*Power(t1,4)) - 
               10*Power(t1,6)*(-13 + t2) - 361*t2 - 1065*Power(t2,2) - 
               116*Power(t2,3) + 
               Power(t1,4)*(1901 - 944*t2 - 55*Power(t2,2)) - 
               Power(t1,5)*(736 + 19*t2 + 20*Power(t2,2)) + 
               Power(t1,3)*(-4977 + 4122*t2 + 1692*Power(t2,2) + 
                  100*Power(t2,3)) + 
               3*t1*(-870 + 717*t2 + 1188*Power(t2,2) + 
                  124*Power(t2,3)) - 
               Power(t1,2)*(-5786 + 4945*t2 + 4198*Power(t2,2) + 
                  416*Power(t2,3)) - 
               Power(s1,2)*(125*Power(t1,5) + 
                  Power(t1,3)*(91 - 680*t2) + 
                  13*Power(t1,4)*(-53 + 5*t2) + 3*(71 + 66*t2) - 
                  2*t1*(687 + 455*t2) + Power(t1,2)*(1657 + 1387*t2)) + 
               s1*(67 + 6*Power(t1,6) + 985*t2 + 248*Power(t2,2) + 
                  Power(t1,5)*(-251 + 145*t2) + 
                  Power(t1,4)*(908 - 207*t2 - 20*Power(t2,2)) - 
                  Power(t1,3)*(933 + 2924*t2 + 168*Power(t2,2)) - 
                  t1*(662 + 4299*t2 + 838*Power(t2,2)) + 
                  Power(t1,2)*(932 + 6282*t2 + 918*Power(t2,2)))) + 
            Power(s2,4)*(281 - 45*Power(t1,6) + 
               Power(s1,3)*(88 - 409*t1 + 481*Power(t1,2) - 
                  40*Power(t1,3)) - 509*t2 - 931*Power(t2,2) - 
               176*Power(t2,3) + 10*Power(t1,5)*(-29 + 15*t2) + 
               Power(t1,4)*(2021 + 70*t2 + 35*Power(t2,2)) + 
               Power(t1,3)*(-3052 + 1059*t2 + 835*Power(t2,2) + 
                  70*Power(t2,3)) + 
               t1*(-1367 + 1931*t2 + 3123*Power(t2,2) + 
                  622*Power(t2,3)) - 
               Power(t1,2)*(-2453 + 3115*t2 + 3658*Power(t2,2) + 
                  651*Power(t2,3)) + 
               Power(s1,2)*(-235 + 360*Power(t1,4) - 308*t2 + 
                  4*Power(t1,3)*(-233 + 40*t2) + t1*(937 + 1221*t2) - 
                  Power(t1,2)*(722 + 1359*t2)) + 
               s1*(-85*Power(t1,5) + Power(t1,4)*(93 - 575*t2) + 
                  30*Power(t1,3)*(-23 + 7*t2) + 
                  2*(7 + 612*t2 + 148*Power(t2,2)) + 
                  Power(t1,2)*(1568 + 4745*t2 + 1011*Power(t2,2)) - 
                  t1*(440 + 4415*t2 + 1032*Power(t2,2)))) + 
            Power(s2,5)*(70 + 153*Power(t1,5) + 
               Power(s1,3)*(91 - 311*t1 + 125*Power(t1,2)) + 
               Power(t1,4)*(89 - 525*t2) - 284*t2 - 715*Power(t2,2) - 
               343*Power(t2,3) + 
               2*Power(t1,3)*(-955 + 50*t2 + 77*Power(t2,2)) - 
               Power(t1,2)*(-2493 + 73*t2 + 1883*Power(t2,2) + 
                  378*Power(t2,3)) + 
               t1*(-953 + 660*t2 + 2811*Power(t2,2) + 1092*Power(t2,3)) + 
               Power(s1,2)*(-435*Power(t1,3) + 
                  Power(t1,2)*(324 - 420*t2) - 2*(87 + 179*t2) + 
                  2*t1*(237 + 629*t2)) + 
               s1*(325*Power(t1,4) + Power(t1,3)*(95 + 763*t2) + 
                  13*(8 + 67*t2 + 36*Power(t2,2)) + 
                  Power(t1,2)*(-277 + 645*t2 + 462*Power(t2,2)) - 
                  t1*(149 + 2824*t2 + 1687*Power(t2,2))))))*T2q(s2,s))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*
       Power(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2),2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*(-s + s1 - t2)) + 
    (8*(-((-1 + s2)*(s2 - t1)*Power(-1 + t1,4)*
            Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3)) + 
         Power(s,10)*(2*Power(t1,3) - Power(t1,2)*t2 - Power(t2,3)) + 
         Power(s,9)*(13*Power(t1,4) - 
            Power(t1,3)*(9 + 2*s1 + 8*s2 + 5*t2) + 
            3*Power(t2,2)*(-1 + s2 - s1*s2 + 3*t2 + 2*s2*t2) - 
            t1*t2*(3 + s2*(-1 + t2) - 2*t2 + 6*Power(t2,2) + 
               s1*(-3 + 3*s2 + t2)) + 
            Power(t1,2)*(-2*(2 + t2) + s1*(3 - 4*s2 + t2) + 
               s2*(10 + 7*t2))) + 
         Power(s,8)*(36*Power(t1,5) - 
            Power(t1,4)*(56 + 13*s1 + 47*s2 + 10*t2) + 
            Power(t1,2)*(25 + Power(s1,2)*(-1 + 5*s2) + 15*t2 + 
               8*Power(t2,2) - 15*Power(t2,3) - 
               9*Power(s2,2)*(3 + 2*t2) + 
               s2*(-33 + 4*t2 - 6*Power(t2,2)) + 
               s1*(-23 + 13*Power(s2,2) + 21*t2 - 26*s2*t2 - 
                  6*Power(t2,2))) + 
            Power(t1,3)*(4 + 12*Power(s2,2) - 21*t2 + Power(t2,2) + 
               s2*(86 + 35*t2) + s1*(-15*s2 + 6*(3 + t2))) - 
            t2*(3 - 24*t2 + 33*Power(t2,2) + 
               s2*(-3 + s1*(3 - 20*t2) + 7*t2 + 43*Power(t2,2)) + 
               Power(s2,2)*(1 + 3*Power(s1,2) + 13*t2 + 
                  15*Power(t2,2) - 3*s1*(1 + 5*t2))) + 
            t1*(-3 + 28*t2 - 29*Power(t2,2) + 49*Power(t2,3) + 
               Power(s1,2)*s2*(3 - 3*s2 + t2) + 
               Power(s2,2)*(11 + 5*t2 + 5*Power(t2,2)) + 
               s2*(-2 - 24*t2 + 24*Power(t2,2) + 33*Power(t2,3)) + 
               s1*(3 - 23*t2 + 4*Power(t2,2) + 
                  2*Power(s2,2)*(-1 + 5*t2) + 
                  s2*(-3 + 11*t2 - 14*Power(t2,2))))) - 
         s*Power(-1 + t1,3)*(Power(s2,6)*Power(s1 - t2,2)*
             (-1 + s1 + t1 - t2) - 4*Power(-1 + t2,3) + 
            t1*Power(-1 + t2,2)*(-33 + 8*s1 + 18*t2) - 
            (-1 + s1)*Power(t1,5)*
             (-2 + 2*Power(s1,2) + 7*t2 - s1*(10 + 3*t2)) - 
            Power(t1,2)*(-1 + t2)*
             (77 + 2*Power(s1,2) - 84*t2 + 14*Power(t2,2) + 
               s1*(-42 + 39*t2)) + 
            Power(t1,4)*(27 + Power(s1,3) - 43*t2 + 10*Power(t2,2) - 
               4*Power(s1,2)*(2 + 3*t2) + 
               s1*(-10 + 41*t2 - 6*Power(t2,2))) + 
            Power(t1,3)*(-73 - 2*Power(s1,3) + 125*t2 - 
               51*Power(t2,2) + 3*Power(t2,3) + 
               Power(s1,2)*(-6 + 20*t2) + 
               4*s1*(13 - 24*t2 + 7*Power(t2,2))) - 
            Power(s2,5)*(s1 - t2)*
             (-1 + Power(s1,2)*(-4 + 3*t1) - 3*t2 - 4*Power(t2,2) - 
               Power(t1,2)*(1 + 3*t2) + t1*(2 + 6*t2 + 3*Power(t2,2)) + 
               s1*(4 + 4*Power(t1,2) + 8*t2 - 2*t1*(4 + 3*t2))) + 
            s2*(Power(s1,3)*Power(t1,2)*
                (6 - t1 + 11*Power(t1,2) + Power(t1,3)) - 
               (-16 + t2)*Power(-1 + t2,2) + Power(t1,5)*(1 + 2*t2) - 
               Power(t1,4)*(5 + 29*t2 + 7*Power(t2,2)) + 
               t1*(-46 + 52*t2 + 20*Power(t2,2) - 26*Power(t2,3)) + 
               Power(t1,3)*(-7 + 38*t2 + 7*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(t1,2)*(41 - 30*t2 - 38*Power(t2,2) + 
                  15*Power(t2,3)) - 
               Power(s1,2)*t1*
                (4 - 65*Power(t1,2) - 4*t2 + Power(t1,4)*(4 + t2) + 
                  Power(t1,3)*(60 + 9*t2) + t1*(-3 + 45*t2)) + 
               s1*(-(Power(t1,5)*(-2 + t2)) - 8*Power(-1 + t2,2) + 
                  Power(t1,4)*(41 + 52*t2 + 6*Power(t2,2)) - 
                  Power(t1,3)*(57 + 31*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*(-25 + 52*t2 + 21*Power(t2,2)) + 
                  t1*(47 - 88*t2 + 41*Power(t2,2)))) + 
            Power(s2,4)*(Power(s1,3)*(-2 - 17*t1 + 2*Power(t1,2)) + 
               Power(s1,2)*(-17 + 6*Power(t1,3) + 12*t2 - 
                  2*Power(t1,2)*(7 + 4*t2) + t1*(25 + 47*t2)) + 
               t2*(1 - 25*t2 + 9*Power(t2,2) + 
                  3*Power(t1,3)*(1 + t2) - 
                  Power(t1,2)*(5 + 16*t2 + 3*Power(t2,2)) + 
                  t1*(1 + 38*t2 + 11*Power(t2,2))) - 
               s1*(1 - 42*t2 + 19*Power(t2,2) + 
                  Power(t1,3)*(3 + 9*t2) - 
                  Power(t1,2)*(5 + 30*t2 + 9*Power(t2,2)) + 
                  t1*(1 + 63*t2 + 41*Power(t2,2)))) + 
            Power(s2,2)*(-3 - 
               Power(s1,3)*t1*
                (6 + 3*t1 + 25*Power(t1,2) + 3*Power(t1,3)) + 25*t2 + 
               Power(t1,5)*t2 - 34*Power(t2,2) + 12*Power(t2,3) + 
               Power(t1,4)*(3 - 15*t2 - 2*Power(t2,2)) + 
               9*Power(t1,2)*t2*(-10 + 2*t2 + Power(t2,2)) + 
               Power(t1,3)*(-6 + 78*t2 + 28*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(6 + t2 - 10*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s1,2)*(2 + Power(t1,5) - 2*t2 + 
                  Power(t1,4)*(7 + 3*t2) + 6*t1*(2 + 5*t2) + 
                  3*Power(t1,2)*(-41 + 16*t2) + 
                  Power(t1,3)*(101 + 32*t2)) - 
               s1*(5 + Power(t1,4)*(3 - 10*t2) - 7*t2 + 2*Power(t2,2) + 
                  Power(t1,5)*(1 + t2) + 
                  Power(t1,3)*(44 + 149*t2 + 16*Power(t2,2)) + 
                  Power(t1,2)*(-96 - 75*t2 + 30*Power(t2,2)) + 
                  t1*(43 - 58*t2 + 63*Power(t2,2)))) + 
            Power(s2,3)*(4 + Power(s1,3)*
                (2 + 5*t1 + 29*Power(t1,2) + 2*Power(t1,3)) - 30*t2 + 
               33*Power(t2,2) - 11*Power(t2,3) - 
               Power(t1,4)*t2*(3 + t2) + 
               t1*(-12 + 70*t2 + 14*Power(t2,2) - 21*Power(t2,3)) + 
               Power(t1,3)*(-4 + 16*t2 + 13*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(-12 + 53*t2 + 59*Power(t2,2) + 
                  7*Power(t2,3)) - 
               Power(s1,2)*(9 - 4*Power(t1,3) + 4*Power(t1,4) + 5*t2 + 
                  t1*(-83 + 48*t2) + Power(t1,2)*(74 + 61*t2)) + 
               s1*(Power(t1,4)*(3 + 5*t2) - 
                  Power(t1,3)*(2 + 27*t2 + 3*Power(t2,2)) + 
                  2*(8 - 7*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*(11 + 163*t2 + 39*Power(t2,2)) + 
                  t1*(-28 - 127*t2 + 64*Power(t2,2))))) - 
         Power(s,2)*Power(-1 + t1,2)*
          ((1 + s1)*Power(t1,8) - 12*Power(-1 + t2,2)*(-1 + 2*t2) - 
            Power(t1,7)*(6 - 2*t2 + s1*(5 + t2)) + 
            Power(t1,6)*(24 - 3*Power(s1,2) - 17*t2 + Power(t2,2) + 
               s1*(8 + 3*t2)) - 
            Power(s2,6)*(s1 - t2)*
             (2 + 3*Power(s1,2) + 2*Power(t1,2) + 
               s1*(-7 + 7*t1 - 6*t2) + 7*t2 + 3*Power(t2,2) - 
               t1*(4 + 7*t2)) + 
            t1*(-1 + t2)*(119 - 214*t2 + 86*Power(t2,2) + 
               s1*(-46 + 50*t2)) + 
            Power(t1,2)*(319 - 646*t2 + 445*Power(t2,2) - 
               80*Power(t2,3) - 2*Power(s1,2)*(-7 + 6*t2) + 
               s1*(-199 + 352*t2 - 179*Power(t2,2))) + 
            Power(t1,4)*(224 + 19*Power(s1,3) - 263*t2 + 
               44*Power(t2,2) - Power(t2,3) - 
               Power(s1,2)*(101 + 63*t2) + 
               s1*(4 + 217*t2 - 40*Power(t2,2))) + 
            Power(t1,5)*(-80 - 10*Power(s1,3) + 91*t2 + 
               Power(s1,2)*(73 + 16*t2) + s1*(-60 - 53*t2 + Power(t2,2))\
) + Power(t1,3)*(-375 - 12*Power(s1,3) + 548*t2 - 250*Power(t2,2) + 
               22*Power(t2,3) + 17*Power(s1,2)*(1 + 4*t2) + 
               s1*(205 - 422*t2 + 159*Power(t2,2))) + 
            Power(s2,5)*(Power(s1,3)*(-7 + 4*t1) - 
               Power(t1,3)*(2 + 7*t2) + 
               Power(t1,2)*(6 + 30*t2 + 22*Power(t2,2)) + 
               2*(1 + 8*t2 + 13*Power(t2,2) + 5*Power(t2,3)) - 
               t1*(6 + 39*t2 + 48*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s1,2)*(16*Power(t1,2) - 3*t1*(12 + 5*t2) + 
                  4*(5 + 6*t2)) + 
               s1*(-16 + 7*Power(t1,3) - 46*t2 - 27*Power(t2,2) - 
                  2*Power(t1,2)*(15 + 19*t2) + 
                  3*t1*(13 + 28*t2 + 6*Power(t2,2)))) + 
            Power(s2,4)*(3 + Power(s1,3)*(9 - 2*t1 + 2*Power(t1,2)) + 
               36*t2 - 20*Power(t2,2) + 3*Power(t2,3) + 
               2*Power(t1,4)*(4 + 5*t2) + 
               Power(s1,2)*(-30 + t1 - 3*Power(t1,3) - 
                  2*Power(t1,2)*(-16 + t2) - 19*t2 - 6*t1*t2) - 
               3*Power(t1,3)*(9 + 25*t2 + 9*Power(t2,2)) + 
               3*Power(t1,2)*
                (11 + 52*t2 + 30*Power(t2,2) + 2*Power(t2,3)) - 
               t1*(17 + 127*t2 + 43*Power(t2,2) + 18*Power(t2,3)) + 
               s1*(-17 - 8*Power(t1,4) + 52*t2 + 7*Power(t2,2) + 
                  Power(t1,3)*(50 + 28*t2) - 
                  Power(t1,2)*(93 + 116*t2 + 6*Power(t2,2)) + 
                  t1*(68 + 36*t2 + 26*Power(t2,2)))) + 
            Power(s2,2)*(-(Power(s1,3)*t1*
                  (20 - 32*t1 + 56*Power(t1,2) + 7*Power(t1,3))) + 
               Power(t1,6)*(11 + 4*t2) - 
               2*Power(t1,5)*(28 + 17*t2 + 3*Power(t2,2)) + 
               2*t2*(23 - 10*t2 + 6*Power(t2,2)) + 
               Power(t1,4)*(142 + 41*t2 + 41*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(t1,3)*(200 - 205*t2 + 5*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-40 + 109*t2 - 150*Power(t2,2) + 18*Power(t2,3)) + 
               Power(t1,2)*(143 - 371*t2 + 140*Power(t2,2) + 
                  24*Power(t2,3)) + 
               Power(s1,2)*(6 + 14*Power(t1,5) + 
                  Power(t1,4)*(-18 + t2) - 4*t2 + 
                  Power(t1,2)*(-439 + 2*t2) + 
                  2*Power(t1,3)*(173 + 50*t2) + t1*(91 + 54*t2)) + 
               s1*(13 + 5*Power(t1,6) - 60*t2 + 21*Power(t2,2) - 
                  3*Power(t1,5)*(9 + 5*t2) + 
                  t1*(-163 + 192*t2 - 143*Power(t2,2)) + 
                  Power(t1,4)*(-35 + 87*t2 + 9*Power(t2,2)) - 
                  3*Power(t1,3)*(2 + 185*t2 + 27*Power(t2,2)) + 
                  Power(t1,2)*(213 + 351*t2 + 41*Power(t2,2)))) + 
            Power(s2,3)*(18 + 
               4*Power(s1,3)*(1 - 5*t1 + 10*Power(t1,2)) - 78*t2 + 
               71*Power(t2,2) - 7*Power(t2,3) - 
               Power(t1,5)*(13 + 8*t2) + 
               Power(t1,4)*(57 + 77*t2 + 17*Power(t2,2)) - 
               Power(t1,3)*(111 + 178*t2 + 74*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(121 + 79*t2 - 4*Power(t2,2) + 
                  11*Power(t2,3)) - 
               t1*(72 - 108*t2 + 10*Power(t2,2) + 25*Power(t2,3)) + 
               Power(s1,2)*(-17 - 17*Power(t1,4) - 13*t2 + 
                  6*t1*(31 + 4*t2) + 2*Power(t1,3)*(7 + 5*t2) - 
                  Power(t1,2)*(166 + 93*t2)) + 
               s1*(13 + Power(t1,5) - 3*t2 + 18*Power(t2,2) + 
                  2*Power(t1,4)*(-7 + 3*t2) + 
                  Power(t1,3)*(96 - 9*t2 - 9*Power(t2,2)) + 
                  5*t1*(9 - 67*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*(-141 + 341*t2 + 48*Power(t2,2)))) + 
            s2*(Power(s1,3)*Power(t1,2)*
                (28 - 40*t1 + 35*Power(t1,2) + 4*Power(t1,3)) - 
               Power(t1,7)*(5 + t2) + 
               Power(t1,5)*(-77 + 25*t2 - 12*Power(t2,2)) + 
               Power(t1,4)*(142 - 201*t2 - 4*Power(t2,2)) + 
               Power(t1,6)*(26 + 4*t2 + Power(t2,2)) + 
               3*(13 - 28*t2 + 14*Power(t2,2) + Power(t2,3)) - 
               Power(t1,3)*(188 - 256*t2 + 16*Power(t2,2) + 
                  15*Power(t2,3)) - 
               2*t1*(67 - 54*t2 - 2*Power(t2,2) + 27*Power(t2,3)) + 
               Power(t1,2)*(197 - 107*t2 - 15*Power(t2,2) + 
                  39*Power(t2,3)) - 
               Power(s1,2)*t1*
                (20 + 3*Power(t1,5) - 16*t2 + Power(t1,4)*(-4 + 3*t2) - 
                  8*Power(t1,2)*(48 + 7*t2) + 
                  Power(t1,3)*(274 + 41*t2) + t1*(91 + 109*t2)) + 
               s1*(-26 - 4*Power(t1,7) + 56*t2 - 30*Power(t2,2) + 
                  Power(t1,6)*(22 + 6*t2) - 
                  Power(t1,5)*(13 + 35*t2 + 3*Power(t2,2)) + 
                  4*t1*(29 - 38*t2 + 22*Power(t2,2)) + 
                  Power(t1,4)*(165 + 257*t2 + 43*Power(t2,2)) + 
                  Power(t1,2)*(35 + 53*t2 + 56*Power(t2,2)) - 
                  Power(t1,3)*(295 + 185*t2 + 73*Power(t2,2))))) + 
         Power(s,7)*(-1 + 55*Power(t1,6) + 21*t2 - 75*Power(t2,2) + 
            61*Power(t2,3) - Power(t1,5)*(143 + 36*s1 + 10*t2) + 
            Power(t1,4)*(97 - 75*t2 + 5*Power(t2,2) + 
               2*s1*(29 + 8*t2)) + 
            Power(t1,3)*(18 + Power(s1,2) + 148*t2 + 5*Power(t2,2) - 
               20*Power(t2,3) + s1*(-81 + 56*t2 - 15*Power(t2,2))) + 
            t1*(23 - 108*t2 + 139*Power(t2,2) - 156*Power(t2,3) + 
               s1*(-19 + 60*t2 + 5*Power(t2,2))) + 
            Power(t1,2)*(-72 - 2*Power(s1,2)*(-7 + t2) + 61*t2 - 
               76*Power(t2,2) + 109*Power(t2,3) + 
               3*s1*(23 - 55*t2 + 7*Power(t2,2))) - 
            Power(s2,3)*(-4 + Power(s1,3) + 8*Power(t1,3) - 7*t2 - 
               22*Power(t2,2) - 20*Power(t2,3) - 
               4*Power(s1,2)*(2*t1 + 3*t2) - 
               2*Power(t1,2)*(12 + 11*t2) + 
               t1*(20 + 21*t2 + 10*Power(t2,2)) + 
               s1*(1 + 15*Power(t1,2) + 11*t2 + 30*Power(t2,2) + 
                  2*t1*(-4 + 5*t2))) + 
            Power(s2,2)*(-1 + 65*Power(t1,4) + 
               Power(t1,3)*(-167 + 51*s1 - 80*t2) + 
               (-19 - 2*s1 + 16*Power(s1,2))*t2 + 
               (55 - 81*s1)*Power(t2,2) + 85*Power(t2,3) + 
               Power(t1,2)*(134 - 28*Power(s1,2) + 58*t2 + 
                  27*Power(t2,2) + 4*s1*(-7 + 17*t2)) + 
               t1*(-26 + 2*Power(s1,3) + Power(s1,2)*(11 - 21*t2) + 
                  29*t2 - 108*Power(t2,2) - 75*Power(t2,3) + 
                  3*s1*(-3 - 9*t2 + 25*Power(t2,2)))) + 
            s2*(-113*Power(t1,5) + Power(t1,4)*(299 - 9*s1 + 73*t2) + 
               t2*(-5 + s1*(16 - 51*t2) - 30*t2 + 124*Power(t2,2)) + 
               Power(t1,2)*(44 - Power(s1,3) - 211*t2 + 
                  101*Power(t2,2) + 75*Power(t2,3) + 
                  Power(s1,2)*(-12 + 7*t2) + 
                  s1*(42 + 71*t2 - 24*Power(t2,2))) + 
               Power(t1,3)*(26*Power(s1,2) - 5*s1*(3 + 17*t2) - 
                  4*(59 - 5*t2 + 5*Power(t2,2))) + 
               t1*(4 + 98*t2 - 87*Power(t2,2) - 209*Power(t2,3) - 
                  2*Power(s1,2)*(8 + 7*t2) + 
                  s1*(5 + 33*t2 + 92*Power(t2,2))))) + 
         Power(s,6)*(6 + 50*Power(t1,7) - 54*t2 + 108*Power(t2,2) - 
            51*Power(t2,3) - Power(t1,6)*(193 + 55*s1 + 5*t2) + 
            Power(t1,5)*(263 - 132*t2 + 10*Power(t2,2) + 
               s1*(119 + 25*t2)) + 
            t1*(-64 + 203*t2 - 276*Power(t2,2) + 230*Power(t2,3) + 
               s1*(38 - 38*t2 - 58*Power(t2,2))) + 
            Power(t1,4)*(-225 + 17*Power(s1,2) + 406*t2 - 
               21*Power(t2,2) - 15*Power(t2,3) + 
               s1*(-138 + 69*t2 - 20*Power(t2,2))) + 
            Power(s2,4)*(-4 + 3*Power(s1,3) + 2*Power(t1,3) - 11*t2 - 
               18*Power(t2,2) - 15*Power(t2,3) - 
               Power(t1,2)*(7 + 13*t2) - 
               Power(s1,2)*(1 + 6*t1 + 18*t2) + 
               t1*(9 + 23*t2 + 10*Power(t2,2)) + 
               s1*(4 - 10*t1 + 7*Power(t1,2) + 15*t2 + 30*Power(t2,2))) \
+ Power(t1,2)*(117 - 342*t2 + 243*Power(t2,2) - 290*Power(t2,3) + 
               2*Power(s1,2)*(-29 + 6*t2) + 
               s1*(-80 + 378*t2 + 33*Power(t2,2))) + 
            Power(t1,3)*(46 + 2*Power(s1,3) - 119*t2 - 65*Power(t2,2) + 
               126*Power(t2,3) - 2*Power(s1,2)*(-7 + 6*t2) + 
               s1*(159 - 406*t2 + 43*Power(t2,2))) - 
            Power(s2,3)*(2 + 43*Power(t1,4) + 
               5*Power(s1,3)*(-1 + 2*t1) + 4*t2 + 95*Power(t2,2) + 
               90*Power(t2,3) + 
               Power(s1,2)*(-3 - 43*Power(t1,2) + t1*(30 - 66*t2) + 
                  53*t2) - Power(t1,3)*(119 + 78*t2) + 
               2*Power(t1,2)*(65 + 71*t2 + 24*Power(t2,2)) - 
               2*t1*(28 + 35*t2 + 83*Power(t2,2) + 45*Power(t2,3)) + 
               s1*(11 + 48*Power(t1,3) - 36*t2 - 132*Power(t2,2) + 
                  Power(t1,2)*(-57 + 52*t2) + t1*t2*(23 + 140*t2))) + 
            Power(s2,2)*(-6 + 134*Power(t1,5) + 
               Power(s1,3)*t1*(-14 + 11*t1) + 86*t2 - 101*Power(t2,2) - 
               192*Power(t2,3) - Power(t1,4)*(443 + 147*t2) + 
               Power(t1,3)*(514 + 198*t2 + 75*Power(t2,2)) - 
               Power(t1,2)*(243 - 130*t2 + 377*Power(t2,2) + 
                  153*Power(t2,3)) + 
               t1*(44 - 239*t2 + 450*Power(t2,2) + 364*Power(t2,3)) + 
               Power(s1,2)*(-82*Power(t1,3) + 
                  Power(t1,2)*(103 - 61*t2) - 34*t2 + t1*(7 + 117*t2)) \
+ s1*(1 + 65*Power(t1,4) - 8*t2 + 169*Power(t2,2) + 
                  2*Power(t1,3)*(-32 + 81*t2) - 
                  10*t1*(-3 + 5*t2 + 36*Power(t2,2)) + 
                  Power(t1,2)*(-60 - 179*t2 + 153*Power(t2,2)))) - 
            s2*(-5 + 142*Power(t1,6) + 
               Power(s1,3)*Power(t1,2)*(-11 + 4*t1) + 26*t2 - 
               136*Power(t2,2) + 183*Power(t2,3) - 
               Power(t1,5)*(522 + 83*t2) + 
               Power(t1,4)*(687 - 53*t2 + 41*Power(t2,2)) + 
               t1*(2 + 88*t2 - 71*Power(t2,2) - 504*Power(t2,3)) - 
               Power(t1,3)*(445 - 579*t2 + 261*Power(t2,2) + 
                  90*Power(t2,3)) + 
               Power(t1,2)*(141 - 579*t2 + 446*Power(t2,2) + 
                  409*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-34 - 57*Power(t1,3) + Power(t1,2)*(91 - 17*t2) - 
                  52*t2 + 16*t1*(2 + 5*t2)) + 
               s1*(1 - 38*Power(t1,5) + 32*t2 - 62*Power(t2,2) + 
                  2*Power(t1,4)*(56 + 73*t2) + 
                  Power(t1,2)*(97 - 293*t2 - 165*Power(t2,2)) + 
                  Power(t1,3)*(-121 - 181*t2 + 13*Power(t2,2)) + 
                  t1*(-29 + 245*t2 + 212*Power(t2,2))))) + 
         Power(s,3)*(-1 + t1)*
          (-9 - 4*(3 + 2*s1)*Power(t1,8) + Power(t1,9) + 63*t2 - 
            111*Power(t2,2) + 57*Power(t2,3) + 
            Power(s2,6)*(s1 - t2)*
             (-2 + 3*Power(s1,2) - 2*Power(t1,2) + t1*(4 - 3*t2) + 
               3*s1*(-1 + t1 - 2*t2) + 3*t2 + 3*Power(t2,2)) + 
            Power(t1,7)*(48 - 19*t2 + Power(t2,2) + 6*s1*(6 + t2)) + 
            Power(t1,5)*(449 + 20*Power(s1,3) - 358*t2 + 
               15*Power(t2,2) + 3*Power(t2,3) - 
               4*Power(s1,2)*(44 + 9*t2) + s1*(150 + 59*t2)) + 
            Power(t1,3)*(833 + 26*Power(s1,3) - 862*t2 + 
               486*Power(t2,2) - 24*Power(t2,3) - 
               2*Power(s1,2)*(13 + 66*t2) + 
               s1*(-443 + 697*t2 - 391*Power(t2,2))) + 
            t1*(153 - 517*t2 + 536*Power(t2,2) - 179*Power(t2,3) + 
               s1*(-91 + 226*t2 - 131*Power(t2,2))) - 
            Power(t1,4)*(768 + 45*Power(s1,3) - 568*t2 + 
               97*Power(t2,2) + 14*Power(t2,3) - 
               3*Power(s1,2)*(79 + 45*t2) + 
               s1*(7 + 303*t2 - 108*Power(t2,2))) + 
            Power(t1,6)*(-171 + 17*Power(s1,2) + 126*t2 - 
               10*Power(t2,2) - s1*(54 + 12*t2 + Power(t2,2))) + 
            Power(t1,2)*(-524 + 999*t2 - 820*Power(t2,2) + 
               156*Power(t2,3) + Power(s1,2)*(-52 + 30*t2) + 
               s1*(417 - 673*t2 + 418*Power(t2,2))) - 
            Power(s2,5)*(2 + 2*Power(t1,4) + 
               Power(s1,3)*(-13 + 18*t1) + 22*t2 + 36*Power(t2,2) + 
               22*Power(t2,3) - Power(t1,3)*(8 + 5*t2) + 
               2*Power(t1,2)*(6 + 16*t2 + 21*Power(t2,2)) - 
               t1*(8 + 49*t2 + 78*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s1,2)*(22 + 28*Power(t1,2) + 48*t2 - 
                  t1*(50 + 63*t2)) + 
               s1*(-20 + 3*Power(t1,3) - 58*t2 - 57*Power(t2,2) - 
                  2*Power(t1,2)*(13 + 35*t2) + 
                  t1*(43 + 128*t2 + 72*Power(t2,2)))) + 
            Power(s2,2)*(-17 + 11*Power(t1,7) + 
               Power(s1,3)*t1*
                (-8 + 9*t1 + 16*Power(t1,2) + 8*Power(t1,3)) - 25*t2 - 
               68*Power(t2,2) - 31*Power(t2,3) - 
               3*Power(t1,6)*(32 + 9*t2) + 
               2*Power(t1,5)*(167 + 87*t2 + 21*Power(t2,2)) - 
               Power(t1,4)*(712 + 395*t2 + 329*Power(t2,2) + 
                  21*Power(t2,3)) + 
               2*Power(t1,3)*
                (472 + 49*t2 + 389*Power(t2,2) + 68*Power(t2,3)) + 
               t1*(227 - 228*t2 + 649*Power(t2,2) + 124*Power(t2,3)) - 
               Power(t1,2)*(691 - 403*t2 + 1072*Power(t2,2) + 
                  233*Power(t2,3)) - 
               Power(s1,2)*(6 + 52*Power(t1,5) + t1*(5 - 56*t2) - 
                  2*Power(t1,3)*(-178 + t2) + 
                  Power(t1,4)*(-114 + t2) + 4*t2 + 
                  Power(t1,2)*(-305 + 128*t2)) + 
               s1*(-38 - 25*Power(t1,6) + 166*t2 - 29*Power(t2,2) + 
                  Power(t1,5)*(100 + 57*t2) + 
                  Power(t1,3)*(-544 + 332*t2 - 21*Power(t2,2)) + 
                  Power(t1,4)*(190 - 161*t2 - 9*Power(t2,2)) + 
                  t1*(112 - 685*t2 + 43*Power(t2,2)) + 
                  Power(t1,2)*(205 + 291*t2 + 91*Power(t2,2)))) - 
            s2*(26 + 5*Power(t1,8) + 
               Power(s1,3)*Power(t1,2)*
                (28 - 51*t1 + 39*Power(t1,2) + 7*Power(t1,3)) - 61*t2 + 
               14*Power(t2,2) + 14*Power(t2,3) - 
               Power(t1,7)*(51 + 7*t2) + 
               Power(t1,6)*(189 + 4*t2 + 10*Power(t2,2)) - 
               Power(t1,5)*(508 - 133*t2 + 104*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,4)*(881 - 509*t2 + 285*Power(t2,2) + 
                  37*Power(t2,3)) - 
               t1*(134 + 85*t2 + 8*Power(t2,2) + 103*Power(t2,3)) - 
               Power(t1,3)*(851 - 431*t2 + 482*Power(t2,2) + 
                  125*Power(t2,3)) + 
               Power(t1,2)*(443 + 94*t2 + 285*Power(t2,2) + 
                  157*Power(t2,3)) - 
               Power(s1,2)*t1*
                (18*Power(t1,5) - 8*(-5 + t2) + 25*t1*(5 + 2*t2) + 
                  Power(t1,4)*(-73 + 5*t2) - 
                  Power(t1,2)*(611 + 14*t2) + Power(t1,3)*(501 + 36*t2)\
) + s1*(-29 - 27*Power(t1,7) + 74*t2 - 41*Power(t2,2) + 
                  5*Power(t1,6)*(26 + 7*t2) + 
                  Power(t1,3)*(-421 + 362*t2 - 105*Power(t2,2)) - 
                  2*Power(t1,5)*(45 + 67*t2 + 7*Power(t2,2)) + 
                  2*Power(t1,2)*(83 - 335*t2 + 24*Power(t2,2)) + 
                  Power(t1,4)*(173 + 263*t2 + 88*Power(t2,2)) + 
                  t1*(98 + 70*t2 + 93*Power(t2,2)))) + 
            Power(s2,4)*(-11 + 8*Power(t1,5) + 
               Power(s1,3)*(14 - 52*t1 + 21*Power(t1,2)) - 111*t2 - 
               122*Power(t2,2) - 50*Power(t2,3) - 
               Power(t1,4)*(43 + 29*t2) + 
               Power(t1,3)*(92 + 204*t2 + 99*Power(t2,2)) - 
               Power(t1,2)*(98 + 432*t2 + 326*Power(t2,2) + 
                  57*Power(t2,3)) + 
               t1*(52 + 368*t2 + 349*Power(t2,2) + 124*Power(t2,3)) + 
               Power(s1,2)*(-45 + 31*Power(t1,3) - 67*t2 - 
                  Power(t1,2)*(113 + 88*t2) + t1*(127 + 206*t2)) + 
               s1*(63 + 17*Power(t1,4) + 155*t2 + 103*Power(t2,2) - 
                  2*Power(t1,3)*(60 + 59*t2) + 
                  Power(t1,2)*(252 + 403*t2 + 124*Power(t2,2)) - 
                  2*t1*(106 + 220*t2 + 139*Power(t2,2)))) + 
            Power(s2,3)*(-44 - 13*Power(t1,6) + 
               Power(s1,3)*(10 - 29*t1 + 42*Power(t1,2) - 
                  7*Power(t1,3)) + 45*t2 - 182*Power(t2,2) - 
               45*Power(t2,3) + Power(t1,5)*(92 + 42*t2) - 
               Power(t1,4)*(266 + 326*t2 + 93*Power(t2,2)) + 
               Power(t1,3)*(420 + 832*t2 + 482*Power(t2,2) + 
                  51*Power(t2,3)) + 
               t1*(200 + 216*t2 + 576*Power(t2,2) + 179*Power(t2,3)) - 
               Power(t1,2)*(389 + 809*t2 + 783*Power(t2,2) + 
                  201*Power(t2,3)) + 
               2*Power(s1,2)*
                (-21 + 14*Power(t1,4) - 13*t2 + 
                  Power(t1,3)*(4 + 15*t2) + t1*(32 + 62*t2) - 
                  Power(t1,2)*(29 + 88*t2)) + 
               s1*(49 - 6*Power(t1,5) + 110*t2 + 52*Power(t2,2) + 
                  Power(t1,4)*(84 + 26*t2) - 
                  Power(t1,3)*(416 + 259*t2 + 65*Power(t2,2)) - 
                  t1*(364 + 259*t2 + 247*Power(t2,2)) + 
                  Power(t1,2)*(653 + 382*t2 + 308*Power(t2,2))))) + 
         Power(s,5)*(27*Power(t1,8) - Power(t1,7)*(147 + 50*s1 + t2) - 
            3*(4 - 18*t2 + 15*Power(t2,2) + 3*Power(t2,3)) + 
            Power(t1,6)*(315 - 128*t2 + 10*Power(t2,2) + 
               s1*(151 + 25*t2)) - 
            Power(t1,3)*(338 + 12*Power(s1,3) + 
               Power(s1,2)*(59 - 68*t2) + 188*t2 - 113*Power(t2,2) + 
               263*Power(t2,3) + s1*(23 - 653*t2 - 93*Power(t2,2))) + 
            Power(t1,5)*(-549 + 38*Power(s1,2) + 546*t2 - 
               44*Power(t2,2) - 6*Power(t2,3) + 
               s1*(-177 + 31*t2 - 15*Power(t2,2))) + 
            Power(t1,4)*(669 + 10*Power(s1,3) - 560*t2 + 
               18*Power(t2,2) + 79*Power(t2,3) - 
               Power(s1,2)*(85 + 31*t2) + 
               s1*(189 - 428*t2 + 42*Power(t2,2))) + 
            t1*(64 - 109*t2 + 131*Power(t2,2) - 104*Power(t2,3) + 
               s1*(-8 - 109*t2 + 145*Power(t2,2))) - 
            Power(t1,2)*(29 - 386*t2 + 181*Power(t2,2) - 
               306*Power(t2,3) + 6*Power(s1,2)*(-18 + 5*t2) + 
               s1*(82 + 176*t2 + 273*Power(t2,2))) + 
            Power(s2,5)*(-3*Power(s1,3) + 2*Power(s1,2)*(1 + 6*t2) - 
               s1*(3 + Power(t1,2) + 9*t2 + 15*Power(t2,2) - 
                  t1*(4 + 5*t2)) + 
               t2*(5 + 3*Power(t1,2) + 7*t2 + 6*Power(t2,2) - 
                  t1*(8 + 5*t2))) - 
            Power(s2,3)*(4 + 75*Power(t1,5) + 
               Power(s1,3)*(11 - 46*t1 + 30*Power(t1,2)) + 14*t2 - 
               199*Power(t2,2) - 160*Power(t2,3) - 
               29*Power(t1,4)*(9 + 4*t2) + 
               Power(t1,3)*(341 + 364*t2 + 122*Power(t2,2)) - 
               Power(t1,2)*(195 + 334*t2 + 497*Power(t2,2) + 
                  162*Power(t2,3)) + 
               t1*(36 + 72*t2 + 591*Power(t2,2) + 334*Power(t2,3)) - 
               Power(s1,2)*(3 + 68*Power(t1,3) + 93*t2 + 
                  2*Power(t1,2)*(-56 + 73*t2) - 3*t1*(-8 + 87*t2)) + 
               s1*(-22 + 55*Power(t1,4) + 81*t2 + 228*Power(t2,2) + 
                  3*Power(t1,3)*(-41 + 17*t2) + 
                  t1*(63 - 220*t2 - 521*Power(t2,2)) + 
                  3*Power(t1,2)*(9 + 18*t2 + 88*Power(t2,2)))) + 
            Power(s2,4)*(9 + 14*Power(t1,4) + 
               Power(s1,3)*(-12 + 17*t1) + 32*t2 + 71*Power(t2,2) + 
               55*Power(t2,3) - Power(t1,3)*(31 + 29*t2) + 
               Power(s1,2)*(4 - 19*Power(t1,2) + t1*(8 - 82*t2) + 
                  67*t2) + Power(t1,2)*(29 + 98*t2 + 42*Power(t2,2)) - 
               t1*(21 + 101*t2 + 120*Power(t2,2) + 60*Power(t2,3)) + 
               s1*(-6 + 11*Power(t1,3) - 59*t2 - 110*Power(t2,2) - 
                  Power(t1,2)*(36 + 7*t2) + 
                  t1*(31 + 80*t2 + 125*Power(t2,2)))) + 
            s2*(-18 - 98*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*(-29 + 24*t1 - 7*Power(t1,2)) + 
               75*t2 - 197*Power(t2,2) + 145*Power(t2,3) + 
               Power(t1,6)*(487 + 57*t2) + 
               Power(t1,5)*(-966 + 58*t2 - 49*Power(t2,2)) + 
               t1*(6 - 198*t2 + 88*Power(t2,2) - 579*Power(t2,3)) + 
               Power(t1,4)*(1217 - 724*t2 + 386*Power(t2,2) + 
                  60*Power(t2,3)) - 
               Power(t1,3)*(1024 - 1128*t2 + 985*Power(t2,2) + 
                  403*Power(t2,3)) + 
               Power(t1,2)*(396 - 396*t2 + 814*Power(t2,2) + 
                  783*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-44 + 68*Power(t1,4) + Power(t1,2)*(120 - 139*t2) - 
                  78*t2 + Power(t1,3)*(-193 + 19*t2) + 
                  2*t1*(53 + 114*t2)) + 
               s1*(1 + 82*Power(t1,6) + 38*t2 - 41*Power(t2,2) - 
                  Power(t1,5)*(276 + 149*t2) + 
                  Power(t1,2)*(179 - 1379*t2 - 321*Power(t2,2)) + 
                  Power(t1,4)*(196 + 271*t2 + 13*Power(t2,2)) + 
                  Power(t1,3)*(-101 + 579*t2 + 112*Power(t2,2)) + 
                  t1*(-81 + 526*t2 + 213*Power(t2,2)))) + 
            Power(s2,2)*(27 + 132*Power(t1,6) + 
               Power(s1,3)*t1*(40 - 72*t1 + 19*Power(t1,2)) - 127*t2 + 
               131*Power(t2,2) + 216*Power(t2,3) - 
               Power(t1,5)*(578 + 145*t2) + 
               Power(t1,4)*(986 + 356*t2 + 119*Power(t2,2)) - 
               Power(t1,3)*(901 + 93*t2 + 741*Power(t2,2) + 
                  160*Power(t2,3)) + 
               Power(t1,2)*(523 - 483*t2 + 1437*Power(t2,2) + 
                  615*Power(t2,3)) - 
               t1*(189 - 492*t2 + 919*Power(t2,2) + 657*Power(t2,3)) + 
               Power(s1,2)*(-119*Power(t1,4) + 
                  Power(t1,3)*(235 - 79*t2) + 38*t2 - 
                  t1*(98 + 247*t2) + Power(t1,2)*(9 + 328*t2)) + 
               s1*(7 + 14*Power(t1,5) - 39*t2 - 162*Power(t2,2) + 
                  2*Power(t1,4)*(1 + 95*t2) + 
                  3*Power(t1,3)*(17 - 96*t2 + 50*Power(t2,2)) + 
                  t1*(29 + 419*t2 + 610*Power(t2,2)) - 
                  Power(t1,2)*(103 + 336*t2 + 639*Power(t2,2))))) + 
         Power(s,4)*(Power(s2,6)*Power(s1 - t2,2)*(-1 + s1 + t1 - t2) - 
            Power(s2,5)*(-4 + 2*Power(t1,4) + 
               2*Power(s1,3)*(-5 + 6*t1) + Power(t1,3)*(-2 + t2) + 
               12*t2 + 24*Power(t2,2) + 19*Power(t2,3) + 
               2*Power(t1,2)*(-3 + 5*t2 + 9*Power(t2,2)) - 
               t1*(-10 + 23*t2 + 42*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s1,2)*(10 + 4*Power(t1,2) + 39*t2 - 
                  t1*(14 + 45*t2)) + 
               s1*(-5*Power(t1,3) + Power(t1,2)*(2 - 22*t2) - 
                  2*(4 + 17*t2 + 24*Power(t2,2)) + 
                  t1*(11 + 56*t2 + 54*Power(t2,2)))) + 
            s2*(11 - 35*Power(t1,8) + 
               Power(s1,3)*Power(t1,2)*
                (13 - 14*t1 + Power(t1,2) - 8*Power(t1,3)) - 40*t2 + 
               108*Power(t2,2) - 60*Power(t2,3) + 
               Power(t1,7)*(239 + 25*t2) + 
               Power(t1,6)*(-686 + 17*t2 - 32*Power(t2,2)) + 
               Power(t1,5)*(1380 - 447*t2 + 305*Power(t2,2) + 
                  21*Power(t2,3)) - 
               2*Power(t1,4)*
                (976 - 537*t2 + 490*Power(t2,2) + 100*Power(t2,3)) + 
               t1*(30 + 372*t2 - 125*Power(t2,2) + 331*Power(t2,3)) + 
               Power(t1,3)*(1575 - 708*t2 + 1432*Power(t2,2) + 
                  555*Power(t2,3)) - 
               Power(t1,2)*(562 + 293*t2 + 708*Power(t2,2) + 
                  639*Power(t2,3)) + 
               Power(s1,2)*t1*
                (47 + 47*Power(t1,5) + Power(t1,3)*(470 - 71*t2) + 
                  43*t2 + Power(t1,4)*(-194 + 11*t2) - 
                  t1*(39 + 190*t2) + Power(t1,2)*(-331 + 231*t2)) + 
               s1*(11 + 69*Power(t1,7) - 53*t2 + 32*Power(t2,2) - 
                  2*Power(t1,6)*(153 + 47*t2) + 
                  Power(t1,4)*(-81 + 203*t2 - 40*Power(t2,2)) + 
                  Power(t1,5)*(277 + 273*t2 + 24*Power(t2,2)) - 
                  3*Power(t1,3)*(-86 + 583*t2 + 34*Power(t2,2)) - 
                  2*t1*(-13 + 229*t2 + 62*Power(t2,2)) + 
                  2*Power(t1,2)*(-127 + 939*t2 + 93*Power(t2,2)))) + 
            (-1 + t1)*(-6 - 27*(2 + s1)*Power(t1,7) + 8*Power(t1,8) - 
               9*t2 + 72*Power(t2,2) - 61*Power(t2,3) + 
               Power(t1,6)*(139 - 69*t2 + 5*Power(t2,2) + 
                  s1*(85 + 16*t2)) + 
               Power(t1,2)*(277 + Power(s1,2)*(103 - 40*t2) - 200*t2 + 
                  281*Power(t2,2) + 58*Power(t2,3) + 
                  s1*(-326 + 284*t2 - 371*Power(t2,2))) + 
               Power(t1,5)*(-374 + 37*Power(s1,2) + 325*t2 - 
                  29*Power(t2,2) - Power(t2,3) + 
                  s1*(-83 + 3*t2 - 6*Power(t2,2))) + 
               Power(t1,4)*(668 + 20*Power(s1,3) - 447*t2 + 
                  27*Power(t2,2) + 24*Power(t2,3) - 
                  2*Power(s1,2)*(87 + 22*t2) + 
                  4*s1*(36 - 35*t2 + 3*Power(t2,2))) + 
               Power(t1,3)*(-609 - 26*Power(s1,3) + 130*t2 - 
                  65*Power(t2,2) - 90*Power(t2,3) + 
                  Power(s1,2)*(35 + 109*t2) + 
                  s1*(140 + 88*t2 + 149*Power(t2,2))) + 
               t1*(-49 + 270*t2 - 290*Power(t2,2) + 83*Power(t2,3) + 
                  s1*(67 - 253*t2 + 184*Power(t2,2)))) + 
            Power(s2,4)*(20*Power(t1,5) + 
               Power(s1,3)*(19 - 56*t1 + 35*Power(t1,2)) - 
               Power(t1,4)*(69 + 35*t2) + 
               Power(t1,3)*(84 + 196*t2 + 101*Power(t2,2)) + 
               t1*t2*(216 + 350*t2 + 173*Power(t2,2)) - 
               3*(-1 + 15*t2 + 41*Power(t2,2) + 26*Power(t2,3)) - 
               Power(t1,2)*(38 + 332*t2 + 328*Power(t2,2) + 
                  93*Power(t2,3)) + 
               Power(s1,2)*(-14 + 9*Power(t1,3) - 99*t2 - 
                  Power(t1,2)*(35 + 146*t2) + t1*(40 + 251*t2)) + 
               s1*(-3 + 13*Power(t1,4) + 115*t2 + 158*Power(t2,2) - 
                  2*Power(t1,3)*(41 + 44*t2) - 
                  2*t1*(25 + 162*t2 + 184*Power(t2,2)) + 
                  Power(t1,2)*(122 + 297*t2 + 204*Power(t2,2)))) + 
            Power(s2,2)*(-35 + 62*Power(t1,7) + 
               Power(s1,3)*t1*
                (-50 + 117*t1 - 80*Power(t1,2) + 14*Power(t1,3)) + 
               63*t2 - 135*Power(t2,2) - 121*Power(t2,3) - 
               Power(t1,6)*(371 + 83*t2) + 
               Power(t1,5)*(947 + 369*t2 + 102*Power(t2,2)) - 
               Power(t1,4)*(1486 + 631*t2 + 775*Power(t2,2) + 
                  87*Power(t2,3)) + 
               Power(t1,3)*(1591 + 181*t2 + 2022*Power(t2,2) + 
                  486*Power(t2,3)) + 
               t1*(356 - 432*t2 + 1123*Power(t2,2) + 528*Power(t2,3)) - 
               Power(t1,2)*(1064 - 533*t2 + 2337*Power(t2,2) + 
                  807*Power(t2,3)) - 
               Power(s1,2)*(2 + 101*Power(t1,5) + 
                  Power(t1,3)*(146 - 308*t2) + 23*t2 + 
                  Power(t1,4)*(-259 + 39*t2) - 2*t1*(74 + 123*t2) + 
                  Power(t1,2)*(158 + 495*t2)) + 
               s1*(-31 - 33*Power(t1,6) + 151*t2 + 50*Power(t2,2) + 
                  129*Power(t1,5)*(1 + t2) + 
                  Power(t1,4)*(216 - 253*t2 + 57*Power(t2,2)) + 
                  2*Power(t1,2)*(281 + 635*t2 + 369*Power(t2,2)) - 
                  t1*(47 + 934*t2 + 387*Power(t2,2)) - 
                  Power(t1,3)*(796 + 363*t2 + 455*Power(t2,2)))) + 
            Power(s2,3)*(-24 - 53*Power(t1,6) + 
               Power(s1,3)*(15 - 88*t1 + 113*Power(t1,2) - 
                  30*Power(t1,3)) + 19*t2 - 247*Power(t2,2) - 
               133*Power(t2,3) + Power(t1,5)*(258 + 94*t2) - 
               Power(t1,4)*(516 + 515*t2 + 159*Power(t2,2)) + 
               Power(t1,3)*(554 + 1038*t2 + 804*Power(t2,2) + 
                  139*Power(t2,3)) + 
               3*t1*(44 + 84*t2 + 346*Power(t2,2) + 151*Power(t2,3)) - 
               Power(t1,2)*(351 + 888*t2 + 1436*Power(t2,2) + 
                  469*Power(t2,3)) + 
               Power(s1,2)*(-39 + 48*Power(t1,4) - 77*t2 + 
                  6*t1*(27 + 65*t2) + 2*Power(t1,3)*(-35 + 66*t2) - 
                  Power(t1,2)*(101 + 475*t2)) + 
               s1*(25 - 27*Power(t1,5) + 137*t2 + 179*Power(t2,2) + 
                  Power(t1,4)*(145 + 18*t2) - 
                  Power(t1,3)*(374 + 306*t2 + 225*Power(t2,2)) - 
                  t1*(215 + 660*t2 + 707*Power(t2,2)) + 
                  Power(t1,2)*(446 + 811*t2 + 783*Power(t2,2))))))*
       T3q(s2,1 - s + s2 - t1))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*Power(-1 + s + t1,2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*(-s + s1 - t2)) + 
    (8*(-((-1 + s2)*Power(s2 - t1,4)*(-1 + t1)*
            Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3)) + 
         Power(s,10)*(2*Power(t1,3) - Power(t1,2)*t2 - Power(t2,3)) + 
         Power(s,9)*(13*Power(t1,4) - 
            Power(t1,3)*(3 + 2*s1 + 14*s2 + 5*t2) + 
            3*Power(t2,2)*(-1 + s2 - s1*s2 + 2*t2 + 3*s2*t2) + 
            Power(t1,2)*(-4 - 5*t2 + 10*s2*(1 + t2) + 
               s1*(3 - 4*s2 + t2)) - 
            t1*t2*(3 + s2*(-1 + t2) - 2*t2 + 6*Power(t2,2) + 
               s1*(-3 + 3*s2 + t2))) + 
         Power(s,8)*(36*Power(t1,5) - 
            Power(t1,4)*(25 + 13*s1 + 78*s2 + 10*t2) + 
            Power(t1,3)*(-27 + 42*Power(s2,2) - 35*t2 + Power(t2,2) + 
               s2*(75 + 49*t2) + s1*(-9*s2 + 6*(4 + t2))) - 
            t2*(3*(1 - 5*t2 + 4*Power(t2,2)) + 
               s2*(-3 + s1*(3 - 11*t2) - 11*t2 + 43*Power(t2,2)) + 
               Power(s2,2)*(1 + 3*Power(s1,2) + 22*t2 + 
                  36*Power(t2,2) - 3*s1*(1 + 8*t2))) + 
            Power(t1,2)*(13 + Power(s1,2)*(-1 + 5*s2) + 3*t2 + 
               8*Power(t2,2) - 15*Power(t2,3) - 
               3*Power(s2,2)*(19 + 14*t2) + 
               s2*(9 + 40*t2 - 6*Power(t2,2)) + 
               s1*(-14 + 25*Power(s2,2) + 24*t2 - 6*Power(t2,2) - 
                  s2*(21 + 29*t2))) + 
            t1*(-3 + 19*t2 - 23*Power(t2,2) + 34*Power(t2,3) + 
               Power(s1,2)*s2*(3 - 3*s2 + t2) + 
               Power(s2,2)*(11 + 2*t2 + 8*Power(t2,2)) + 
               s2*(-2 - 12*t2 + 15*Power(t2,2) + 48*Power(t2,3)) + 
               s1*(3 - 14*t2 + Power(t2,2) + Power(s2,2)*(-2 + 19*t2) - 
                  s2*(3 + 7*t2 + 11*Power(t2,2))))) - 
         s*Power(s2 - t1,3)*(Power(s2,6)*Power(s1 - t2,2)*
             (-1 + s1 + t1 - t2) - 
            Power(s2,5)*(s1 - t2)*
             (-1 + Power(s1,2)*(-1 + 3*t1) - 3*t2 - Power(t2,2) - 
               Power(t1,2)*(1 + 3*t2) + t1*(2 + 6*t2 + 3*Power(t2,2)) + 
               2*s1*(2 + 2*Power(t1,2) + t2 - t1*(4 + 3*t2))) - 
            (-1 + t1)*(-7*Power(-1 + t2,3) + 
               t1*Power(-1 + t2,2)*(-35 + 17*s1 + 11*t2) + 
               (-1 + s1)*Power(t1,4)*
                (-2 + 2*Power(s1,2) + 7*t2 - s1*(10 + 3*t2)) - 
               Power(t1,2)*(-1 + t2)*
                (51 + 11*Power(s1,2) - 38*t2 + 3*Power(t2,2) + 
                  s1*(-43 + 22*t2)) + 
               Power(t1,3)*(-25 + Power(s1,3) + 36*t2 - 
                  10*Power(t2,2) + Power(s1,2)*(-4 + 9*t2) + 
                  s1*(18 - 31*t2 + 6*Power(t2,2)))) + 
            Power(s2,2)*(Power(s1,3)*t1*
                (3 + 15*t1 - 22*Power(t1,2) - 3*Power(t1,3)) + 
               Power(t1,5)*t2 + 
               Power(t1,4)*(3 - 15*t2 - 2*Power(t2,2)) - 
               2*t2*(1 - 10*t2 + 9*Power(t2,2)) + 
               9*Power(t1,2)*
                (1 - 13*t2 + 2*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(-9 + 78*t2 + 28*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-3 + 55*t2 - 64*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s1,2)*(Power(t1,5) + t1*(39 - 24*t2) - 
                  11*(-1 + t2) + Power(t1,4)*(7 + 3*t2) + 
                  3*Power(t1,2)*(-50 + 7*t2) + 
                  4*Power(t1,3)*(23 + 8*t2)) - 
               s1*(-13 + Power(t1,4)*(3 - 10*t2) + 47*t2 - 
                  34*Power(t2,2) + Power(t1,5)*(1 + t2) + 
                  t1*(70 - 58*t2 + 9*Power(t2,2)) + 
                  3*Power(t1,2)*(-32 - 43*t2 + 10*Power(t2,2)) + 
                  Power(t1,3)*(35 + 149*t2 + 16*Power(t2,2)))) + 
            s2*(Power(s1,3)*Power(t1,2)*
                (-3 - 7*t1 + 11*Power(t1,2) + Power(t1,3)) + 
               Power(t1,5)*(1 + 2*t2) + 2*Power(-1 + t2,2)*(5 + 7*t2) - 
               Power(t1,4)*(5 + 29*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(-1 + 38*t2 + 7*Power(t2,2) - 
                  5*Power(t2,3)) - 
               2*t1*(14 + t2 - 28*Power(t2,2) + 13*Power(t2,3)) + 
               Power(t1,2)*(23 - 3*t2 - 38*Power(t2,2) + 
                  15*Power(t2,3)) - 
               Power(s1,2)*t1*
                (-83*Power(t1,2) - 22*(-1 + t2) + 
                  Power(t1,4)*(4 + t2) + 3*t1*(-1 + 6*t2) + 
                  Power(t1,3)*(60 + 9*t2)) + 
               s1*(-(Power(t1,5)*(-2 + t2)) - 17*Power(-1 + t2,2) + 
                  t1*(47 - 52*t2 + 5*Power(t2,2)) + 
                  Power(t1,4)*(41 + 52*t2 + 6*Power(t2,2)) - 
                  Power(t1,3)*(75 + 31*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*(2 - 2*t2 + 21*Power(t2,2)))) + 
            Power(s2,4)*(2*Power(s1,3)*(2 - 4*t1 + Power(t1,2)) + 
               Power(s1,2)*(-8 + 6*Power(t1,3) - 15*t2 - 
                  2*Power(t1,2)*(7 + 4*t2) + t1*(16 + 29*t2)) + 
               t2*(1 - 16*t2 - 6*Power(t2,2) + 
                  3*Power(t1,3)*(1 + t2) - 
                  Power(t1,2)*(5 + 16*t2 + 3*Power(t2,2)) + 
                  t1*(1 + 29*t2 + 11*Power(t2,2))) - 
               s1*(1 - 24*t2 - 17*Power(t2,2) + 
                  Power(t1,3)*(3 + 9*t2) - 
                  Power(t1,2)*(5 + 30*t2 + 9*Power(t2,2)) + 
                  t1*(1 + 45*t2 + 32*Power(t2,2)))) + 
            Power(s2,3)*(4 + Power(s1,3)*
                (-1 - 13*t1 + 20*Power(t1,2) + 2*Power(t1,3)) - 21*t2 - 
               3*Power(t2,2) + 19*Power(t2,3) - 
               Power(t1,4)*t2*(3 + t2) + 
               t1*(-12 + 52*t2 + 50*Power(t2,2) - 21*Power(t2,3)) + 
               Power(t1,3)*(-4 + 16*t2 + 13*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(-12 + 44*t2 + 59*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(s1,2)*(-27 + 4*Power(t1,3) - 4*Power(t1,4) + 
                  22*t2 + t1*(83 + 6*t2) - 4*Power(t1,2)*(14 + 13*t2)) + 
               s1*(7 + 40*t2 - 40*Power(t2,2) + Power(t1,4)*(3 + 5*t2) - 
                  Power(t1,3)*(2 + 27*t2 + 3*Power(t2,2)) + 
                  t1*(-10 - 163*t2 + 28*Power(t2,2)) + 
                  Power(t1,2)*(2 + 145*t2 + 39*Power(t2,2))))) + 
         Power(s,7)*(-1 + 55*Power(t1,6) + 12*t2 - 21*Power(t2,2) + 
            6*Power(t2,3) - 2*Power(t1,5)*(38 + 18*s1 + 5*t2) + 
            Power(t1,4)*(-58 - 103*t2 + 5*Power(t2,2) + 
               s1*(89 + 16*t2)) + 
            Power(t1,3)*(90 - 3*Power(s1,2) + 73*t2 + 6*Power(t2,2) - 
               20*Power(t2,3) + s1*(-104 + 77*t2 - 15*Power(t2,2))) + 
            Power(t1,2)*(-21 + Power(s1,2)*(11 - 2*t2) + 93*t2 - 
               58*Power(t2,2) + 79*Power(t2,3) + 
               3*s1*(6 - 35*t2 + 2*Power(t2,2))) + 
            t1*(14 - 42*t2 + 73*Power(t2,2) - 60*Power(t2,3) + 
               s1*(-10 + 9*t2 + 11*Power(t2,2))) - 
            Power(s2,3)*(Power(s1,3) + 70*Power(t1,3) - 
               Power(s1,2)*(17*t1 + 21*t2) - 
               Power(t1,2)*(135 + 98*t2) + 
               t1*(53 + 33*t2 + 28*Power(t2,2)) - 
               2*(2 + 5*t2 + 35*Power(t2,2) + 42*Power(t2,3)) + 
               s1*(1 + 66*Power(t1,2) + 20*t2 + 84*Power(t2,2) + 
                  7*t1*(-2 + 7*t2))) + 
            Power(s2,2)*(-1 + 195*Power(t1,4) + 
               Power(t1,3)*(-318 + 80*s1 - 189*t2) + 
               (-31 + 16*s1 + 7*Power(s1,2))*t2 + 
               (1 - 69*s1)*Power(t2,2) + 133*Power(t2,3) + 
               Power(t1,2)*(62 - 43*Power(s1,2) - 77*t2 + 
                  42*Power(t2,2) + 28*s1*(2 + 5*t2)) + 
               t1*(13 + 2*Power(s1,3) + 98*t2 - 141*Power(t2,2) - 
                  168*Power(t2,3) - Power(s1,2)*(7 + 24*t2) + 
                  3*s1*(-2 + 2*t2 + 35*Power(t2,2)))) + 
            s2*(-180*Power(t1,5) + Power(t1,4)*(262 + 22*s1 + 101*t2) + 
               t2*(13 + s1*(7 - 9*t2) - 78*t2 + 67*Power(t2,2)) + 
               Power(t1,3)*(49 + 26*Power(s1,2) + 202*t2 - 
                  21*Power(t2,2) - s1*(161 + 104*t2)) - 
               Power(t1,2)*(82 + Power(s1,3) + 184*t2 - 
                  68*Power(t2,2) - 105*Power(t2,3) - 
                  Power(s1,2)*(6 + 7*t2) + 
                  s1*(-96 + 61*t2 + 9*Power(t2,2))) + 
               t1*(7 - 25*t2 + 30*Power(t2,2) - 212*Power(t2,3) - 
                  Power(s1,2)*(7 + 11*t2) + 
                  s1*(-13 + 90*t2 + 56*Power(t2,2))))) + 
         Power(s,2)*Power(s2 - t1,2)*
          (-((1 + s1)*Power(t1,8)) + 
            t1*(-22 + 7*s1 - 38*t2)*Power(-1 + t2,2) + 
            15*Power(-1 + t2,3) - 
            Power(t1,6)*(9 + 3*Power(s1,2) + s1*(4 - 6*t2) + t2 + 
               Power(t2,2)) + 
            Power(t1,2)*(-1 + t2)*
             (-252 - 15*Power(s1,2) + s1*(126 - 43*t2) + 129*t2 + 
               46*Power(t2,2)) + Power(t1,7)*(5 - 2*t2 + s1*(4 + t2)) - 
            Power(t1,3)*(418 + 3*Power(s1,3) + 
               Power(s1,2)*(9 - 48*t2) - 645*t2 + 151*Power(t2,2) + 
               33*Power(t2,3) + s1*(-232 + 361*t2 - 48*Power(t2,2))) + 
            Power(t1,4)*(241 + 15*Power(s1,3) - 416*t2 + 
               55*Power(t2,2) + 7*Power(t2,3) - 
               Power(s1,2)*(79 + 56*t2) - s1*(98 - 334*t2 + Power(t2,2))\
) + Power(t1,5)*(-33 - 10*Power(s1,3) + 104*t2 + Power(t2,2) + 
               8*Power(s1,2)*(9 + 2*t2) - 
               s1*(14 + 127*t2 + 3*Power(t2,2))) + 
            Power(s2,6)*(6*Power(s1,3) + 
               Power(s1,2)*(-5 + 3*t1 - 21*t2) + 
               s1*(3 - 4*t1 + Power(t1,2) + 15*t2 - 11*t1*t2 + 
                  24*Power(t2,2)) - 
               t2*(5 + 3*Power(t1,2) + 10*t2 + 9*Power(t2,2) - 
                  8*t1*(1 + t2))) + 
            Power(s2,4)*(14 + 
               Power(s1,3)*(8 - 34*t1 + 20*Power(t1,2)) - 19*t2 - 
               94*Power(t2,2) - 22*Power(t2,3) - 
               Power(t1,4)*(9 + 22*t2) + 
               Power(t1,3)*(15 + 67*t2 + 31*Power(t2,2)) - 
               Power(t1,2)*(-11 + 66*t2 + 161*Power(t2,2) + 
                  36*Power(t2,3)) + 
               t1*(-31 + 40*t2 + 230*Power(t2,2) + 71*Power(t2,3)) + 
               Power(s1,2)*(-36 + 14*Power(t1,3) - 33*t2 - 
                  24*Power(t1,2)*(2 + 3*t2) + 2*t1*(38 + 65*t2)) + 
               s1*(-8 + Power(t1,4) + Power(t1,3)*(12 - 29*t2) + 
                  111*t2 + 45*Power(t2,2) + 
                  t1*(51 - 252*t2 - 163*Power(t2,2)) + 
                  2*Power(t1,2)*(-28 + 79*t2 + 43*Power(t2,2)))) + 
            s2*(Power(s1,3)*Power(t1,2)*
                (9 - 53*t1 + 46*Power(t1,2) + 4*Power(t1,3)) + 
               Power(t1,7)*(6 + t2) + 2*Power(-1 + t2,2)*(23 + 7*t2) - 
               Power(t1,6)*(22 - 9*t2 + Power(t2,2)) + 
               Power(t1,5)*(43 + t2 + 16*Power(t2,2)) + 
               Power(t1,3)*(-258 + 635*t2 + 99*Power(t2,2) - 
                  16*Power(t2,3)) - 
               2*Power(t1,4)*
                (-5 + 149*t2 + 51*Power(t2,2) + 2*Power(t2,3)) - 
               t1*(259 - 312*t2 + Power(t2,2) + 52*Power(t2,3)) + 
               Power(t1,2)*(434 - 582*t2 - 38*Power(t2,2) + 
                  57*Power(t2,3)) + 
               Power(s1,2)*t1*
                (Power(t1,5) + 30*(-1 + t2) - 3*Power(t1,4)*(4 + t2) - 
                  Power(t1,3)*(301 + 64*t2) - t1*(49 + 68*t2) + 
                  Power(t1,2)*(382 + 92*t2)) + 
               s1*(5*Power(t1,7) - 7*Power(-1 + t2,2) - 
                  Power(t1,6)*(30 + 7*t2) + 
                  Power(t1,5)*(67 - 13*t2 + Power(t2,2)) + 
                  t1*(177 - 188*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*(-226 + 453*t2 + 16*Power(t2,2)) + 
                  Power(t1,4)*(116 + 466*t2 + 31*Power(t2,2)) - 
                  Power(t1,3)*(102 + 707*t2 + 44*Power(t2,2)))) - 
            Power(s2,5)*(1 + Power(s1,3)*(-6 + 20*t1) + 4*t2 + 
               26*Power(t2,2) + 13*Power(t2,3) - 
               Power(t1,3)*(2 + 13*t2) + 
               Power(t1,2)*(5 + 37*t2 + 26*Power(t2,2)) - 
               t1*(4 + 28*t2 + 61*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s1,2)*(13 + 11*Power(t1,2) + 22*t2 - 
                  t1*(33 + 67*t2)) + 
               s1*(3 + 3*Power(t1,3) - 35*t2 - 29*Power(t2,2) - 
                  Power(t1,2)*(10 + 33*t2) + 
                  t1*(4 + 86*t2 + 77*Power(t2,2)))) - 
            Power(s2,2)*(-31 + 
               Power(s1,3)*t1*
                (9 - 69*t1 + 84*Power(t1,2) + 10*Power(t1,3)) + 3*t2 + 
               10*Power(t2,2) + 18*Power(t2,3) + 
               7*Power(t1,6)*(2 + t2) - 
               Power(t1,5)*(34 + 5*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-67 + 538*t2 + 243*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,4)*(20 + 64*t2 + 78*Power(t2,2) + 
                  3*Power(t2,3)) - 
               t1*(-77 + 179*t2 + 18*Power(t2,2) + 9*Power(t2,3)) - 
               Power(t1,3)*(-21 + 428*t2 + 356*Power(t2,2) + 
                  31*Power(t2,3)) + 
               Power(s1,2)*(Power(t1,5) + 15*(-1 + t2) - 
                  Power(t1,4)*(36 + 5*t2) + t1*(-125 + 8*t2) + 
                  Power(t1,2)*(563 + 49*t2) - 
                  2*Power(t1,3)*(218 + 83*t2)) + 
               s1*(51 + 9*Power(t1,6) - 19*t2 - 32*Power(t2,2) - 
                  Power(t1,5)*(68 + 15*t2) + 
                  t1*(57 + 197*t2 - 11*Power(t2,2)) + 
                  Power(t1,4)*(177 - 39*t2 - 2*Power(t2,2)) + 
                  Power(t1,2)*(-313 - 877*t2 + 41*Power(t2,2)) + 
                  Power(t1,3)*(87 + 849*t2 + 100*Power(t2,2)))) + 
            Power(s2,3)*(-5 + Power(s1,3)*(3 - 39*t1 + 76*Power(t1,2)) - 
               44*t2 - 27*Power(t2,2) + 33*Power(t2,3) + 
               2*Power(t1,5)*(8 + 9*t2) - 
               Power(t1,4)*(27 + 50*t2 + 17*Power(t2,2)) + 
               t1*(-25 + 221*t2 + 300*Power(t2,2) - 20*Power(t2,3)) + 
               Power(t1,3)*(-29 + 107*t2 + 173*Power(t2,2) + 
                  18*Power(t2,3)) - 
               Power(t1,2)*(-70 + 252*t2 + 477*Power(t2,2) + 
                  79*Power(t2,3)) - 
               Power(s1,2)*(67 + 6*Power(t1,4) + 
                  Power(t1,3)*(1 - 24*t2) - 28*t2 - 
                  2*t1*(148 + 23*t2) + Power(t1,2)*(270 + 226*t2)) + 
               s1*(51 + 6*Power(t1,5) + 105*t2 - 75*Power(t2,2) - 
                  2*Power(t1,4)*(30 + t2) + 
                  Power(t1,3)*(171 - 119*t2 - 36*Power(t2,2)) + 
                  t1*(-105 - 615*t2 + 41*Power(t2,2)) + 
                  Power(t1,2)*(-63 + 727*t2 + 206*Power(t2,2))))) + 
         Power(s,6)*(3 + 50*Power(t1,7) - 9*t2 - 3*Power(t2,2) + 
            9*Power(t2,3) - 5*Power(t1,6)*(23 + 11*s1 + t2) - 
            t1*(13 + s1 - 26*t2 - 34*s1*t2 + 49*Power(t2,2) + 
               29*s1*Power(t2,2) - 17*Power(t2,3)) + 
            Power(t1,5)*(-55 - 164*t2 + 10*Power(t2,2) + 
               5*s1*(36 + 5*t2)) + 
            Power(t1,3)*(-201 - 2*Power(s1,3) - 
               4*Power(s1,2)*(-12 + t2) + 233*t2 - 42*Power(t2,2) + 
               96*Power(t2,3) + s1*(167 - 348*t2 + 11*Power(t2,2))) - 
            Power(t1,4)*(-222 + 5*Power(s1,2) - 206*t2 + 
               17*Power(t2,2) + 15*Power(t2,3) + 
               s1*(317 - 134*t2 + 20*Power(t2,2))) + 
            Power(t1,2)*(20 - 152*t2 + 117*Power(t2,2) - 
               119*Power(t2,3) + Power(s1,2)*(-22 + 6*t2) + 
               2*s1*(5 + 26*t2 + 30*Power(t2,2))) + 
            Power(s2,4)*(-16 + 6*Power(s1,3) + 70*Power(t1,3) - 35*t2 - 
               126*Power(t2,2) - 126*Power(t2,3) - 
               10*Power(t1,2)*(17 + 14*t2) - 
               Power(s1,2)*(1 + 39*t1 + 63*t2) + 
               2*t1*(51 + 50*t2 + 28*Power(t2,2)) + 
               s1*(7 + 95*Power(t1,2) + 57*t2 + 168*Power(t2,2) + 
                  t1*(-40 + 63*t2))) - 
            s2*(-8 + 4*Power(s1,3)*(-2 + t1)*Power(t1,2) + 
               220*Power(t1,6) + 59*t2 - 97*Power(t2,2) + 
               27*Power(t2,3) - Power(t1,5)*(486 + 115*t2) + 
               Power(t1,4)*(-43 - 445*t2 + 45*Power(t2,2)) + 
               t1*(44 - 180*t2 + 245*Power(t2,2) - 276*Power(t2,3)) + 
               Power(t1,3)*(397 + 647*t2 - 212*Power(t2,2) - 
                  120*Power(t2,3)) + 
               Power(t1,2)*(-151 + 128*t2 + 122*Power(t2,2) + 
                  418*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-55*Power(t1,3) + Power(t1,2)*(16 - 19*t2) - 
                  4*(1 + 4*t2) + t1*(92 + 56*t2)) + 
               s1*(-105*Power(t1,5) + Power(1 + t2,2) + 
                  Power(t1,4)*(512 + 195*t2) + 
                  Power(t1,2)*(179 - 644*t2 - 111*Power(t2,2)) + 
                  Power(t1,3)*(-638 + 179*t2 - 15*Power(t2,2)) + 
                  t1*(-56 + 128*t2 + 77*Power(t2,2)))) + 
            Power(s2,2)*(-9 + 360*Power(t1,5) + 
               2*Power(s1,3)*t1*(-4 + 7*t1) + 2*t2 + 130*Power(t2,2) - 
               156*Power(t2,3) - 5*Power(t1,4)*(157 + 71*t2) + 
               Power(t1,3)*(158 - 333*t2 + 105*Power(t2,2)) + 
               Power(t1,2)*(210 + 667*t2 - 497*Power(t2,2) - 
                  315*Power(t2,3)) + 
               t1*(-19 - 155*t2 + 240*Power(t2,2) + 559*Power(t2,3)) + 
               Power(s1,2)*(-149*Power(t1,3) + 
                  Power(t1,2)*(25 - 79*t2) - 4*t2 + t1*(43 + 96*t2)) + 
               s1*(1 + 50*Power(t1,4) - 17*t2 + 43*Power(t2,2) + 
                  Power(t1,3)*(432 + 394*t2) + 
                  t1*(48 - 275*t2 - 321*Power(t2,2)) + 
                  Power(t1,2)*(-387 + 37*t2 + 180*Power(t2,2)))) - 
            Power(s2,3)*(-13 + 260*Power(t1,4) + 
               2*Power(s1,3)*(-1 + 8*t1) - 92*t2 + 68*Power(t2,2) + 
               231*Power(t2,3) + 
               Power(s1,2)*(-3 - 133*Power(t1,2) + t1*(3 - 123*t2) + 
                  38*t2) - Power(t1,3)*(584 + 385*t2) + 
               Power(t1,2)*(255 + 41*t2 + 126*Power(t2,2)) + 
               t1*(19 + 182*t2 - 419*Power(t2,2) - 336*Power(t2,3)) + 
               s1*(14 + 195*Power(t1,3) + 27*t2 - 183*Power(t2,2) + 
                  Power(t1,2)*(60 + 287*t2) + 
                  t1*(-60 + 50*t2 + 343*Power(t2,2))))) - 
         Power(s,3)*(s2 - t1)*
          (-2*(5 + 4*s1)*Power(t1,8) + Power(t1,9) + 
            13*Power(-1 + t2,3) - 
            t1*Power(-1 + t2,2)*(-94 + 27*s1 + 82*t2) + 
            Power(t1,7)*(12 - 21*t2 + Power(t2,2) + s1*(39 + 6*t2)) + 
            Power(t1,2)*(-1 + t2)*
             (-103 - 5*Power(s1,2) - 167*t2 + 135*Power(t2,2) + 
               5*s1*(2 + 15*t2)) + 
            Power(t1,3)*(-710 - 5*Power(s1,3) + 867*t2 + 
               17*Power(t2,2) - 88*Power(t2,3) + 
               Power(s1,2)*(5 + 42*t2) + 
               s1*(317 - 353*t2 - 71*Power(t2,2))) + 
            Power(t1,5)*(-245 - 20*Power(s1,3) + 356*t2 + 
               27*Power(t2,2) + 3*Power(t2,3) + 
               Power(s1,2)*(159 + 32*t2) + 
               s1*(173 - 438*t2 - 10*Power(t2,2))) - 
            Power(t1,6)*(-21 + 11*Power(s1,2) - 32*t2 + 
               10*Power(t2,2) + s1*(101 - 35*t2 + Power(t2,2))) + 
            Power(t1,4)*(747 + 29*Power(s1,3) - 1078*t2 + 
               27*Power(t2,2) + 10*Power(t2,3) - 
               Power(s1,2)*(163 + 90*t2) + 
               s1*(-372 + 787*t2 + 60*Power(t2,2))) + 
            Power(s2,6)*(15*Power(s1,3) + 2*Power(t1,3) - 
               Power(t1,2)*(7 + 22*t2) - 
               Power(s1,2)*(10 + 3*t1 + 63*t2) + 
               t1*(9 + 47*t2 + 28*Power(t2,2)) - 
               2*(2 + 13*t2 + 21*Power(t2,2) + 18*Power(t2,3)) + 
               s1*(13 + 10*Power(t1,2) + 48*t2 + 84*Power(t2,2) - 
                  t1*(22 + 21*t2))) + 
            Power(s2,5)*(7 + Power(s1,3)*(14 - 55*t1) - 
               11*Power(t1,4) + 11*t2 - 89*Power(t2,2) - 
               63*Power(t2,3) + Power(t1,3)*(49 + 97*t2) - 
               Power(t1,2)*(61 + 188*t2 + 98*Power(t2,2)) + 
               t1*(16 + 86*t2 + 253*Power(t2,2) + 132*Power(t2,3)) + 
               Power(s1,2)*(-12 + 22*Power(t1,2) - 65*t2 + 
                  t1*(47 + 213*t2)) + 
               s1*(-29 - 34*Power(t1,3) + 66*t2 + 111*Power(t2,2) + 
                  Power(t1,2)*(38 + 35*t2) + 
                  t1*(19 - 224*t2 - 287*Power(t2,2)))) + 
            Power(s2,2)*(112 + 20*Power(t1,7) - 
               Power(s1,3)*t1*
                (15 - 117*t1 + 176*Power(t1,2) + 5*Power(t1,3)) - 
               86*t2 - 29*Power(t2,2) + 3*Power(t2,3) - 
               2*Power(t1,6)*(80 + 29*t2) + 
               2*Power(t1,5)*(87 - 30*t2 + 19*Power(t2,2)) + 
               Power(t1,4)*(131 + 148*t2 - 433*Power(t2,2) - 
                  33*Power(t2,3)) + 
               t1*(-393 + 485*t2 + 152*Power(t2,2) + 14*Power(t2,3)) - 
               Power(t1,2)*(-488 + 1217*t2 + 853*Power(t2,2) + 
                  134*Power(t2,3)) + 
               Power(t1,3)*(-372 + 889*t2 + 1339*Power(t2,2) + 
                  216*Power(t2,3)) + 
               Power(s1,2)*(5 - 43*Power(t1,5) + 
                  Power(t1,4)*(112 - 27*t2) - 5*t2 + 
                  3*t1*(41 + 6*t2) - 3*Power(t1,2)*(285 + 71*t2) + 
                  Power(t1,3)*(929 + 438*t2)) + 
               s1*(-73 - 62*Power(t1,6) + 61*t2 + 12*Power(t2,2) + 
                  4*Power(t1,5)*(121 + 34*t2) + 
                  Power(t1,3)*(429 - 2349*t2 - 307*Power(t2,2)) + 
                  4*Power(t1,2)*(-2 + 546*t2 + 11*Power(t2,2)) + 
                  Power(t1,4)*(-1107 + 76*t2 + 17*Power(t2,2)) + 
                  t1*(236 - 593*t2 + 36*Power(t2,2)))) + 
            s2*(-7*Power(t1,8) + 
               Power(s1,3)*Power(t1,2)*
                (15 - 97*t1 + 94*Power(t1,2) + 5*Power(t1,3)) + 
               Power(t1,7)*(64 + 9*t2) + 
               2*Power(-1 + t2,2)*(-19 + 13*t2) + 
               Power(t1,6)*(-80 + 87*t2 - 10*Power(t2,2)) + 
               t1*(-267 + 178*t2 + 175*Power(t2,2) - 86*Power(t2,3)) + 
               Power(t1,5)*(-40 - 153*t2 + 112*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,3)*(-1194 + 1914*t2 + 397*Power(t2,2) + 
                  43*Power(t2,3)) - 
               Power(t1,4)*(-467 + 900*t2 + 446*Power(t2,2) + 
                  49*Power(t2,3)) + 
               Power(t1,2)*(1095 - 1261*t2 - 166*Power(t2,2) + 
                  74*Power(t2,3)) + 
               Power(s1,2)*t1*
                (10*Power(t1,5) + Power(t1,4)*(-21 + t2) + 
                  10*(-1 + t2) - 3*t1*(23 + 24*t2) - 
                  Power(t1,3)*(641 + 171*t2) + 
                  Power(t1,2)*(642 + 220*t2)) + 
               s1*(38*Power(t1,7) + 27*Power(-1 + t2,2) - 
                  2*Power(t1,6)*(115 + 24*t2) + 
                  Power(t1,3)*(482 - 2023*t2 - 149*Power(t2,2)) + 
                  t1*(83 + 4*t2 - 87*Power(t2,2)) + 
                  Power(t1,5)*(555 - 72*t2 + 8*Power(t2,2)) + 
                  Power(t1,4)*(-360 + 1529*t2 + 74*Power(t2,2)) + 
                  Power(t1,2)*(-571 + 781*t2 + 111*Power(t2,2)))) - 
            Power(s2,3)*(8 + 30*Power(t1,6) + 
               Power(s1,3)*(-5 + 59*t1 - 164*Power(t1,2) + 
                  30*Power(t1,3)) + 43*t2 + 51*Power(t2,2) - 
               16*Power(t2,3) - Power(t1,5)*(203 + 142*t2) + 
               Power(t1,4)*(199 + 151*t2 + 92*Power(t2,2)) - 
               Power(t1,3)*(-188 + 9*t2 + 769*Power(t2,2) + 
                  117*Power(t2,3)) - 
               t1*(-33 + 326*t2 + 716*Power(t2,2) + 119*Power(t2,3)) + 
               Power(t1,2)*(-255 + 365*t2 + 1564*Power(t2,2) + 
                  384*Power(t2,3)) + 
               Power(s1,2)*(59 - 72*Power(t1,4) + 
                  Power(t1,3)*(90 - 138*t2) - 12*t2 - 
                  2*t1*(218 + 63*t2) + Power(t1,2)*(583 + 556*t2)) + 
               s1*(-18 - 28*Power(t1,5) - 165*t2 + 76*Power(t2,2) + 
                  Power(t1,4)*(440 + 159*t2) + 
                  Power(t1,2)*(427 - 1785*t2 - 615*Power(t2,2)) + 
                  t1*(95 + 1141*t2 + 34*Power(t2,2)) + 
                  Power(t1,3)*(-998 + 204*t2 + 173*Power(t2,2)))) + 
            Power(s2,4)*(21 + 25*Power(t1,5) + 
               2*Power(s1,3)*(5 - 38*t1 + 35*Power(t1,2)) - 32*t2 - 
               200*Power(t2,2) - 67*Power(t2,3) - 
               Power(t1,4)*(139 + 168*t2) + 
               Power(t1,3)*(145 + 286*t2 + 133*Power(t2,2)) - 
               Power(t1,2)*(-64 + 96*t2 + 649*Power(t2,2) + 
                  183*Power(t2,3)) + 
               t1*(-116 + 21*t2 + 721*Power(t2,2) + 281*Power(t2,3)) - 
               Power(s1,2)*(60 + 58*Power(t1,3) + 43*t2 - 
                  2*t1*(74 + 161*t2) + Power(t1,2)*(27 + 262*t2)) + 
               s1*(-7 + 28*Power(t1,4) + 193*t2 + 79*Power(t2,2) + 
                  Power(t1,3)*(131 + 51*t2) + 
                  t1*(214 - 593*t2 - 483*Power(t2,2)) + 
                  Power(t1,2)*(-377 + 341*t2 + 352*Power(t2,2))))) + 
         Power(s,5)*(27*Power(t1,8) - 12*Power(-1 + t2,2)*t2 - 
            Power(t1,7)*(95 + 50*s1 + t2) + 
            Power(t1,6)*(-22 - 151*t2 + 10*Power(t2,2) + 
               5*s1*(41 + 5*t2)) + 
            t1*(-1 + t2)*(17 - 43*t2 + 53*Power(t2,2) + 
               s1*(-22 + 26*t2)) + 
            Power(t1,2)*(47 + Power(s1,2)*(8 - 6*t2) - 106*t2 + 
               61*Power(t2,2) + 3*Power(t2,3) + 
               s1*(-72 + 199*t2 - 141*Power(t2,2))) + 
            Power(t1,4)*(-530 - 10*Power(s1,3) + 432*t2 + 
               38*Power(t2,2) + 64*Power(t2,3) + 
               Power(s1,2)*(114 + 7*t2) + 
               s1*(469 - 651*t2 + 4*Power(t2,2))) - 
            Power(t1,5)*(-265 + 10*Power(s1,2) - 254*t2 + 
               38*Power(t2,2) + 6*Power(t2,3) + 
               3*s1*(157 - 47*t2 + 5*Power(t2,2))) + 
            Power(t1,3)*(362 + 6*Power(s1,3) - 535*t2 + 
               22*Power(t2,2) - 110*Power(t2,3) - 
               Power(s1,2)*(85 + 2*t2) + 
               s1*(-132 + 319*t2 + 131*Power(t2,2))) + 
            Power(s2,5)*(-15*Power(s1,3) - 42*Power(t1,3) + 
               6*Power(t1,2)*(20 + 21*t2) + 
               5*Power(s1,2)*(1 + 9*t1 + 21*t2) - 
               t1*(98 + 145*t2 + 70*Power(t2,2)) - 
               s1*(18 - 60*t1 + 80*Power(t1,2) + 90*t2 + 35*t1*t2 + 
                  210*Power(t2,2)) + 
               2*(12 + 30*t2 + 70*Power(t2,2) + 63*Power(t2,3))) + 
            Power(s2,4)*(-32 + 195*Power(t1,4) + 
               Power(s1,3)*(-9 + 50*t1) - 119*t2 + 150*Power(t2,2) + 
               245*Power(t2,3) + 
               Power(s1,2)*(-8 - 195*Power(t1,2) + t1*(11 - 290*t2) + 
                  85*t2) - Power(t1,3)*(551 + 455*t2) + 
               Power(t1,2)*(368 + 310*t2 + 210*Power(t2,2)) - 
               t1*(5 - 91*t2 + 645*Power(t2,2) + 420*Power(t2,3)) + 
               s1*(48 + 230*Power(t1,3) + t2 - 265*Power(t2,2) + 
                  Power(t1,2)*(-6 + 280*t2) + 
                  5*t1*(-22 + 39*t2 + 119*Power(t2,2)))) + 
            Power(s2,2)*(-15 + 330*Power(t1,6) + 
               Power(s1,3)*t1*(10 - 69*t1 + 30*Power(t1,2)) + 112*t2 - 
               137*Power(t2,2) + 45*Power(t2,3) - 
               6*Power(t1,5)*(169 + 60*t2) + 
               Power(t1,4)*(316 - 541*t2 + 150*Power(t2,2)) + 
               t1*(98 - 229*t2 + 61*Power(t2,2) - 501*Power(t2,3)) + 
               Power(t1,3)*(689 + 1393*t2 - 988*Power(t2,2) - 
                  300*Power(t2,3)) + 
               Power(t1,2)*(-368 - 165*t2 + 1230*Power(t2,2) + 
                  909*Power(t2,3)) + 
               Power(s1,2)*(-225*Power(t1,4) + 
                  Power(t1,3)*(147 - 130*t2) + 2*t2 - 
                  t1*(65 + 82*t2) + 4*Power(t1,2)*(87 + 88*t2)) + 
               s1*(13 - 100*Power(t1,5) - 39*t2 + 12*Power(t2,2) + 
                  5*Power(t1,4)*(227 + 113*t2) - 
                  6*Power(t1,2)*(-94 + 283*t2 + 98*Power(t2,2)) + 
                  Power(t1,3)*(-1704 + 113*t2 + 150*Power(t2,2)) + 
                  t1*(-159 + 546*t2 + 181*Power(t2,2)))) - 
            s2*(150*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*(14 - 45*t1 + 5*Power(t1,2)) - 
               2*Power(t1,6)*(247 + 40*t2) + 
               Power(t1,5)*(53 - 521*t2 + 55*Power(t2,2)) + 
               Power(t1,2)*(465 - 778*t2 + 65*Power(t2,2) - 
                  429*Power(t2,3)) + 
               Power(t1,4)*(700 + 975*t2 - 351*Power(t2,2) - 
                  75*Power(t2,3)) + 
               3*(4 - 8*t2 - 5*Power(t2,2) + 9*Power(t2,3)) + 
               2*t1*(-20 + 81*t2 - 86*Power(t2,2) + 30*Power(t2,3)) + 
               Power(t1,3)*(-832 + 527*t2 + 576*Power(t2,2) + 
                  416*Power(t2,3)) + 
               Power(s1,2)*t1*
                (8 - 60*Power(t1,4) + Power(t1,3)*(48 - 25*t2) - 
                  4*t2 - 6*t1*(23 + 11*t2) + Power(t1,2)*(349 + 144*t2)\
) + s1*(2 - 160*Power(t1,6) - 8*t2 + 6*Power(t2,2) + 
                  Power(t1,5)*(818 + 215*t2) + 
                  t1*(-9 + 60*t2 - 79*Power(t2,2)) + 
                  Power(t1,4)*(-1488 + 263*t2 - 35*Power(t2,2)) - 
                  3*Power(t1,3)*(-295 + 593*t2 + 42*Power(t2,2)) + 
                  Power(t1,2)*(-299 + 835*t2 + 225*Power(t2,2)))) - 
            Power(s2,3)*(-25 + 360*Power(t1,5) + 
               Power(s1,3)*(2 - 43*t1 + 60*Power(t1,2)) + 40*t2 + 
               26*Power(t2,2) - 196*Power(t2,3) - 
               2*Power(t1,4)*(523 + 305*t2) + 
               Power(t1,3)*(511 - 6*t2 + 245*Power(t2,2)) + 
               Power(t1,2)*(268 + 833*t2 - 1185*Power(t2,2) - 
                  525*Power(t2,3)) + 
               t1*(-94 - 383*t2 + 838*Power(t2,2) + 806*Power(t2,3)) + 
               Power(s1,2)*(-315*Power(t1,3) - 6*(2 + 3*t2) + 
                  15*t1*(7 + 20*t2) - 5*Power(t1,2)*(-21 + 58*t2)) + 
               s1*(8 + 160*Power(t1,4) + 30*t2 + 87*Power(t2,2) + 
                  Power(t1,3)*(576 + 620*t2) + 
                  t1*(186 - 549*t2 - 733*Power(t2,2)) + 
                  Power(t1,2)*(-815 + 96*t2 + 555*Power(t2,2))))) + 
         Power(s,4)*(-((43 + 27*s1)*Power(t1,8)) + 8*Power(t1,9) + 
            4*Power(-1 + t2,3) - 
            t1*Power(-1 + t2,2)*(-30 + 8*s1 + 51*t2) + 
            Power(t1,7)*(5 - 79*t2 + 5*Power(t2,2) + 16*s1*(8 + t2)) + 
            Power(t1,2)*(-1 + t2)*
             (137 + 2*Power(s1,2) - 240*t2 + 119*Power(t2,2) + 
               9*s1*(-11 + 12*t2)) + 
            Power(t1,3)*(-317 - 2*Power(s1,3) + 212*t2 + 
               206*Power(t2,2) - 60*Power(t2,3) + 
               Power(s1,2)*(13 + 5*t2) + 
               s1*(66 + 83*t2 - 203*Power(t2,2))) + 
            Power(t1,5)*(-561 - 20*Power(s1,3) + 532*t2 + 
               68*Power(t2,2) + 22*Power(t2,3) + 
               4*Power(s1,2)*(44 + 7*t2) + 
               s1*(496 - 715*t2 - 9*Power(t2,2))) - 
            Power(t1,6)*(-150 + 15*Power(s1,2) - 148*t2 + 
               30*Power(t2,2) + Power(t2,3) + 
               s1*(341 - 92*t2 + 6*Power(t2,2))) + 
            Power(t1,4)*(888 + 22*Power(s1,3) - 1127*t2 - 
               55*Power(t2,2) - 36*Power(t2,3) - 
               2*Power(s1,2)*(79 + 27*t2) + 
               s1*(-433 + 770*t2 + 137*Power(t2,2))) + 
            Power(s2,6)*(-16 + 20*Power(s1,3) + 14*Power(t1,3) - 55*t2 - 
               98*Power(t2,2) - 84*Power(t2,3) - 
               5*Power(t1,2)*(9 + 14*t2) - 
               5*Power(s1,2)*(2 + 5*t1 + 21*t2) + 
               t1*(47 + 114*t2 + 56*Power(t2,2)) + 
               s1*(22 + 39*Power(t1,2) + 85*t2 + 168*Power(t2,2) - 
                  t1*(50 + 7*t2))) + 
            Power(s2,5)*(28 + Power(s1,3)*(16 - 80*t1) - 
               78*Power(t1,4) + 69*t2 - 157*Power(t2,2) - 
               161*Power(t2,3) + 3*Power(t1,3)*(89 + 105*t2) - 
               5*Power(t1,2)*(49 + 82*t2 + 42*Power(t2,2)) + 
               t1*(29 + 84*t2 + 573*Power(t2,2) + 336*Power(t2,3)) + 
               Power(s1,2)*(2 + 135*Power(t1,2) - 100*t2 + 
                  3*t1*(7 + 125*t2)) + 
               s1*(-59 - 143*Power(t1,3) + Power(t1,2)*(71 - 91*t2) + 
                  51*t2 + 225*Power(t2,2) + 
                  t1*(73 - 323*t2 - 609*Power(t2,2)))) + 
            Power(s2,2)*(18 + 150*Power(t1,7) + 
               Power(s1,3)*t1*
                (-6 + 87*t1 - 199*Power(t1,2) + 20*Power(t1,3)) - 
               25*t2 - 20*Power(t2,2) + 27*Power(t2,3) - 
               3*Power(t1,6)*(229 + 70*t2) + 
               2*Power(t1,5)*(190 - 209*t2 + 60*Power(t2,2)) + 
               t1*(-408 + 506*t2 - 41*Power(t2,2) + 66*Power(t2,3)) - 
               2*Power(t1,4)*
                (-380 - 586*t2 + 523*Power(t2,2) + 75*Power(t2,3)) - 
               3*Power(t1,2)*
                (-312 + 477*t2 + 283*Power(t2,2) + 183*Power(t2,3)) + 
               Power(t1,3)*(-1105 + 785*t2 + 2264*Power(t2,2) + 
                  710*Power(t2,3)) + 
               Power(s1,2)*(-165*Power(t1,5) - 
                  105*Power(t1,4)*(-2 + t2) + 2*(-1 + t2) + 
                  9*t1*(5 + t2) - 6*Power(t1,2)*(101 + 45*t2) + 
                  Power(t1,3)*(1055 + 627*t2)) + 
               s1*(2 - 175*Power(t1,6) - 13*t2 + 11*Power(t2,2) + 
                  Power(t1,5)*(1342 + 445*t2) + 
                  Power(t1,3)*(1435 - 3706*t2 - 586*Power(t2,2)) + 
                  t1*(235 - 373*t2 - 24*Power(t2,2)) + 
                  Power(t1,4)*(-2679 + 169*t2 + 60*Power(t2,2)) + 
                  3*Power(t1,2)*(-207 + 844*t2 + 91*Power(t2,2)))) + 
            s2*(-54*Power(t1,8) + 
               Power(s1,3)*Power(t1,2)*(6 - 73*t1 + 101*Power(t1,2)) + 
               Power(-1 + t2,2)*(-13 + 34*t2) + 
               Power(t1,7)*(271 + 35*t2) - 
               2*Power(t1,6)*(58 - 167*t2 + 18*Power(t2,2)) + 
               t1*(91 - 268*t2 + 295*Power(t2,2) - 118*Power(t2,3)) + 
               Power(t1,2)*(700 - 657*t2 - 178*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,5)*(-504 - 686*t2 + 299*Power(t2,2) + 
                  24*Power(t2,3)) - 
               Power(t1,4)*(-1320 + 1227*t2 + 811*Power(t2,2) + 
                  215*Power(t2,3)) + 
               Power(t1,3)*(-1726 + 2279*t2 + 460*Power(t2,2) + 
                  278*Power(t2,3)) + 
               Power(s1,2)*t1*
                (4 + 35*Power(t1,5) - 4*t2 - 6*t1*(7 + 2*t2) + 
                  Power(t1,4)*(-37 + 15*t2) - 
                  8*Power(t1,3)*(89 + 28*t2) + 
                  2*Power(t1,2)*(259 + 99*t2)) + 
               s1*(121*Power(t1,7) + 8*Power(-1 + t2,2) - 
                  Power(t1,6)*(677 + 143*t2) + 
                  Power(t1,3)*(940 - 2301*t2 - 315*Power(t2,2)) + 
                  t1*(-101 + 220*t2 - 119*Power(t2,2)) + 
                  Power(t1,5)*(1532 - 221*t2 + 27*Power(t2,2)) + 
                  Power(t1,4)*(-1334 + 2557*t2 + 113*Power(t2,2)) + 
                  Power(t1,2)*(-263 + 155*t2 + 270*Power(t2,2)))) - 
            Power(s2,3)*(-33 + 220*Power(t1,6) + 
               Power(s1,3)*(-2 + 43*t1 - 191*Power(t1,2) + 
                  80*Power(t1,3)) + 85*t2 - 37*Power(t2,2) + 
               26*Power(t2,3) - 70*Power(t1,5)*(13 + 7*t2) + 
               Power(t1,4)*(583 + 32*t2 + 250*Power(t2,2)) + 
               Power(t1,3)*(591 + 944*t2 - 1712*Power(t2,2) - 
                  400*Power(t2,3)) - 
               t1*(-81 + 239*t2 + 631*Power(t2,2) + 444*Power(t2,3)) + 
               2*Power(t1,2)*
                (-245 - 50*t2 + 1262*Power(t2,2) + 520*Power(t2,3)) + 
               2*Power(s1,2)*
                (8 - 155*Power(t1,4) + Power(t1,3)*(124 - 165*t2) + 
                  t2 - t1*(145 + 81*t2) + Power(t1,2)*(338 + 397*t2)) + 
               s1*(38 - 30*Power(t1,5) - 135*t2 + 43*Power(t2,2) + 
                  6*Power(t1,4)*(199 + 105*t2) + 
                  Power(t1,2)*(868 - 2566*t2 - 1137*Power(t2,2)) + 
                  t1*(-107 + 1147*t2 + 196*Power(t2,2)) + 
                  Power(t1,3)*(-2243 + 196*t2 + 450*Power(t2,2)))) + 
            Power(s2,4)*(-9 + 180*Power(t1,5) + 
               Power(s1,3)*(7 - 89*t1 + 120*Power(t1,2)) + 16*t2 - 
               163*Power(t2,2) - 145*Power(t2,3) - 
               Power(t1,4)*(673 + 560*t2) + 
               Power(t1,3)*(512 + 491*t2 + 315*Power(t2,2)) + 
               Power(t1,2)*(172 + 281*t2 - 1410*Power(t2,2) - 
                  525*Power(t2,3)) + 
               t1*(-173 - 256*t2 + 1157*Power(t2,2) + 685*Power(t2,3)) + 
               Power(s1,2)*(-290*Power(t1,3) + 
                  Power(t1,2)*(79 - 510*t2) - 4*(11 + 9*t2) + 
                  t1*(155 + 463*t2)) + 
               s1*(7 + 155*Power(t1,4) + 146*t2 + 101*Power(t2,2) + 
                  10*Power(t1,3)*(38 + 41*t2) + 
                  t1*(330 - 753*t2 - 880*Power(t2,2)) + 
                  Power(t1,2)*(-850 + 394*t2 + 810*Power(t2,2))))))*
       T4q(1 - s + s2 - t1))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*Power(-s + s2 - t1,2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*(-s + s1 - t2)) - 
    (8*(Power(s,7)*(2*Power(t1,3) - Power(t1,2)*t2 - Power(t2,3)) + 
         Power(s,6)*(7*Power(t1,4) - 
            Power(t1,3)*(7 + 2*s1 + 8*s2 + 2*t2) + 
            3*Power(t2,2)*(-1 + s2 - s1*s2 + 2*t2 + 2*s2*t2) - 
            t1*t2*(3 + s2*(-1 + t2) - 2*t2 + 3*Power(t2,2) + 
               s1*(-3 + 3*s2 + t2)) + 
            Power(t1,2)*(-4 - 5*t2 + s1*(3 - 4*s2 + t2) + s2*(10 + 7*t2))) \
- Power(s,4)*(1 + (29 + 9*s1)*Power(t1,5) - 5*Power(t1,6) - 12*t2 + 
            21*Power(t2,2) - 6*Power(t2,3) - 
            Power(t1,4)*(49 - 48*t2 + 2*Power(t2,2) + s1*(27 + 4*t2)) + 
            t1*(-14 + 33*t2 - 28*Power(t2,2) + 24*Power(t2,3) + 
               s1*(10 - 9*t2 - 11*Power(t2,2))) + 
            Power(t1,2)*(12 - 36*t2 + 7*Power(t2,2) - 13*Power(t2,3) + 
               Power(s1,2)*(-11 + 2*t2) - 3*s1*(3 - 21*t2 + Power(t2,2))) \
+ Power(t1,3)*(27 - 100*t2 + 16*Power(t2,2) + Power(t2,3) + 
               s1*(62 - 33*t2 + 3*Power(t2,2))) + 
            Power(s2,3)*(-4 + Power(s1,3) + 8*Power(t1,3) - 7*t2 - 
               22*Power(t2,2) - 20*Power(t2,3) - 
               4*Power(s1,2)*(2*t1 + 3*t2) - 
               2*Power(t1,2)*(12 + 11*t2) + 
               t1*(20 + 21*t2 + 10*Power(t2,2)) + 
               s1*(1 + 15*Power(t1,2) + 11*t2 + 30*Power(t2,2) + 
                  2*t1*(-4 + 5*t2))) + 
            Power(s2,2)*(1 - 21*Power(t1,4) + 
               (22 - 7*s1 - 7*Power(s1,2))*t2 + 
               4*(-4 + 9*s1)*Power(t2,2) - 40*Power(t2,3) + 
               Power(t1,3)*(-14*s1 + 36*(2 + t2)) + 
               Power(t1,2)*(-56 + 19*Power(s1,2) + 5*t2 - 
                  12*Power(t2,2) - s1*(17 + 38*t2)) + 
               t1*(5 - 2*Power(s1,3) - 47*t2 + 54*Power(t2,2) + 
                  30*Power(t2,3) + 2*Power(s1,2)*(-1 + 6*t2) + 
                  s1*(15 + 6*t2 - 30*Power(t2,2)))) + 
            s2*(18*Power(t1,5) - 2*Power(t1,4)*(37 + 5*s1 + 7*t2) + 
               Power(t1,2)*(-23 + Power(s1,3) + Power(s1,2)*(6 - 4*t2) + 
                  133*t2 - 41*Power(t2,2) - 12*Power(t2,3) + 
                  s1*(-63 + 4*t2)) + 
               t2*(-4 + 33*t2 - 31*Power(t2,2) + s1*(-7 + 9*t2)) + 
               Power(t1,3)*(89 - 11*Power(s1,2) - 77*t2 + 
                  6*Power(t2,2) + s1*(52 + 29*t2)) + 
               t1*(2 - 23*t2 + 36*Power(t2,2) + 53*Power(t2,3) + 
                  Power(s1,2)*(7 + 11*t2) + 
                  s1*(4 - 57*t2 - 26*Power(t2,2))))) + 
         Power(s,5)*(9*Power(t1,5) - 
            Power(t1,4)*(22 + 7*s1 + 21*s2 + t2) + 
            Power(t1,3)*(9 + 12*Power(s2,2) - 3*s1*(-5 + s2 - t2) - 
               30*t2 + Power(t2,2) + 16*s2*(3 + t2)) - 
            t2*(3*(1 - 5*t2 + 4*Power(t2,2)) + 
               s2*(-3 + s1*(3 - 11*t2) - 2*t2 + 25*Power(t2,2)) + 
               Power(s2,2)*(1 + 3*Power(s1,2) + 13*t2 + 15*Power(t2,2) - 
                  3*s1*(1 + 5*t2))) + 
            Power(t1,2)*(13 + Power(s1,2)*(-1 + 5*s2) + 12*t2 + 
               2*Power(t2,2) - 3*Power(t2,3) - 
               9*Power(s2,2)*(3 + 2*t2) + 
               s2*(-15 + 22*t2 - 3*Power(t2,2)) + 
               s1*(-14 + 13*Power(s2,2) + 15*t2 - 3*Power(t2,2) - 
                  s2*(12 + 17*t2))) + 
            t1*(-3 + 19*t2 - 14*Power(t2,2) + 16*Power(t2,3) + 
               Power(s1,2)*s2*(3 - 3*s2 + t2) + 
               Power(s2,2)*(11 + 5*t2 + 5*Power(t2,2)) + 
               s2*(-2 - 21*t2 + 12*Power(t2,2) + 15*Power(t2,3)) + 
               s1*(3 - 14*t2 + Power(t2,2) + 2*Power(s2,2)*(-1 + 5*t2) + 
                  s2*(-3 + 2*t2 - 5*Power(t2,2))))) + 
         (s2 - t1)*(-1 + t1)*(2*(-1 + s1)*Power(t1,5)*(1 + s1 - 2*t2) + 
            Power(s2,4)*Power(s1 - t2,3) - 
            3*(-1 + s1)*t1*Power(-1 + t2,2) + Power(-1 + t2,3) - 
            Power(t1,2)*(-1 + t2)*
             (-1 + 7*Power(s1,2) + 24*t2 + 6*Power(t2,2) - 
               4*s1*(5 + 4*t2)) + 
            Power(t1,3)*(-5 + Power(s1,3) - 40*t2 + 42*Power(t2,2) + 
               4*Power(t2,3) + Power(s1,2)*(-9 + 8*t2) + 
               s1*(49 - 36*t2 - 14*Power(t2,2))) + 
            Power(t1,4)*(6 + 14*t2 - 4*Power(s1,2)*t2 - 24*Power(t2,2) + 
               s1*(-26 + 30*t2 + 4*Power(t2,2))) - 
            Power(s2,3)*(s1 - t2)*
             (-6 + Power(s1,2)*(1 + 3*t1) + t1*(12 - 5*t2) + t2 + 
               4*Power(t2,2) + Power(t1,2)*(-6 + 4*t2) + 
               s1*(5 + 2*Power(t1,2) - 5*t2 - t1*(7 + 3*t2))) + 
            s2*(-(Power(s1,3)*Power(t1,2)*(3 + t1)) - 
               Power(-1 + t2,2)*(-1 + 4*t2) + 
               Power(t1,2)*(15 + 68*t2 - 86*Power(t2,2)) + 
               2*Power(t1,4)*(2 - 7*t2 + 2*Power(t2,2)) - 
               Power(t1,3)*(13 + 16*t2 - 46*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-7 - 32*t2 + 27*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-6*Power(t1,3) + t1*(13 - 10*t2) + 14*(-1 + t2) + 
                  Power(t1,2)*(7 + 8*t2)) + 
               s1*(3*Power(-1 + t2,2) + 6*Power(t1,4)*(1 + t2) + 
                  t1*(49 - 26*t2 - 23*Power(t2,2)) + 
                  Power(t1,3)*(43 - 68*t2 - 2*Power(t2,2)) + 
                  Power(t1,2)*(-101 + 94*t2 + 10*Power(t2,2)))) + 
            Power(s2,2)*(2 + 3*Power(s1,3)*t1*(1 + t1) + 25*t2 - 
               27*Power(t2,2) - 
               2*Power(t1,3)*(1 - 8*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(6 - 7*t2 - 20*Power(t2,2) + 6*Power(t2,3)) - 
               t1*(6 + 34*t2 - 55*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s1,2)*(7 + t1 + 6*Power(t1,3) - 7*t2 - 4*t1*t2 - 
                  7*Power(t1,2)*(2 + t2)) + 
               s1*(-29 - 12*Power(t1,3) + 22*t2 + 7*Power(t2,2) + 
                  Power(t1,2)*(-5 + 40*t2 - 2*Power(t2,2)) + 
                  t1*(46 - 62*t2 + 13*Power(t2,2))))) + 
         Power(s,3)*(3 - (19 + 5*s1)*Power(t1,6) + Power(t1,7) - 9*t2 - 
            3*Power(t2,2) + 9*Power(t2,3) - 
            t1*(10 + s1 + 10*t2 - 34*s1*t2 - 14*Power(t2,2) + 
               29*s1*Power(t2,2) + Power(t2,3)) + 
            Power(t1,5)*(57 - 35*t2 + Power(t2,2) + s1*(29 + 3*t2)) + 
            Power(t1,4)*(-82 + 4*Power(s1,2) + 179*t2 - 25*Power(t2,2) - 
               s1*(113 - 27*t2 + Power(t2,2))) + 
            Power(t1,3)*(72 - 2*Power(s1,3) + Power(s1,2)*(19 - 2*t2) - 
               124*t2 + 52*Power(t2,2) - 3*Power(t2,3) + 
               s1*(93 - 117*t2 + 7*Power(t2,2))) + 
            Power(t1,2)*(-22 - 44*t2 - 12*Power(t2,2) - 11*Power(t2,3) + 
               Power(s1,2)*(-22 + 6*t2) + 
               s1*(40 + 25*t2 + 27*Power(t2,2))) + 
            Power(s2,4)*(-4 + 3*Power(s1,3) + 2*Power(t1,3) - 11*t2 - 
               18*Power(t2,2) - 15*Power(t2,3) - 
               Power(t1,2)*(7 + 13*t2) - 
               Power(s1,2)*(1 + 6*t1 + 18*t2) + 
               t1*(9 + 23*t2 + 10*Power(t2,2)) + 
               s1*(4 - 10*t1 + 7*Power(t1,2) + 15*t2 + 30*Power(t2,2))) - 
            s2*(-5 + Power(s1,3)*(-8 + t1)*Power(t1,2) + 5*Power(t1,6) + 
               23*t2 - 34*Power(t2,2) + 9*Power(t2,3) - 
               6*Power(t1,5)*(9 + t2) + 
               Power(t1,4)*(144 - 70*t2 + 5*Power(t2,2)) - 
               Power(t1,3)*(163 - 292*t2 + 84*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(71 - 148*t2 + 164*Power(t2,2) + 
                  31*Power(t2,3)) - 
               t1*(-2 + 51*t2 + 28*Power(t2,2) + 39*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-7*Power(t1,3) + Power(t1,2)*(19 - 4*t2) - 
                  4*(1 + 4*t2) + t1*(38 + 29*t2)) + 
               s1*(-13*Power(t1,5) + Power(1 + t2,2) + 
                  Power(t1,4)*(68 + 21*t2) + 
                  Power(t1,2)*(122 - 239*t2 - 15*Power(t2,2)) + 
                  Power(t1,3)*(-192 + 25*t2 - 3*Power(t2,2)) + 
                  t1*(-26 + 122*t2 + 17*Power(t2,2)))) + 
            Power(s2,2)*(-9 + 9*Power(t1,5) + 
               Power(s1,3)*t1*(-8 + 5*t1) + 23*t2 - 14*Power(t2,2) - 
               27*Power(t2,3) - Power(t1,4)*(61 + 25*t2) + 
               Power(t1,3)*(125 - 28*t2 + 12*Power(t2,2)) - 
               Power(t1,2)*(99 - 139*t2 + 104*Power(t2,2) + 
                  18*Power(t2,3)) + 
               t1*(35 - 89*t2 + 147*Power(t2,2) + 64*Power(t2,3)) + 
               Power(s1,2)*(-20*Power(t1,3) + 
                  Power(t1,2)*(22 - 16*t2) - 4*t2 + t1*(22 + 42*t2)) + 
               s1*(1 - 4*Power(t1,4) + 4*t2 + 16*Power(t2,2) + 
                  5*Power(t1,3)*(11 + 8*t2) + 
                  t1*(27 - 101*t2 - 72*Power(t2,2)) + 
                  Power(t1,2)*(-99 - 8*t2 + 18*Power(t2,2)))) - 
            Power(s2,3)*(-6 + 7*Power(t1,4) + Power(s1,3)*(-2 + 7*t1) - 
               17*t2 + 29*Power(t2,2) + 30*Power(t2,3) + 
               Power(s1,2)*(-3 - 19*Power(t1,2) + t1*(6 - 30*t2) + 
                  17*t2) - Power(t1,3)*(33 + 32*t2) + 
               Power(t1,2)*(40 + 37*t2 + 18*Power(t2,2)) - 
               2*t1*(4 - 4*t2 + 35*Power(t2,2) + 15*Power(t2,3)) + 
               s1*(14 + 11*Power(t1,3) - 3*t2 - 42*Power(t2,2) + 
                  Power(t1,2)*(6 + 22*t2) + 
                  t1*(-27 + 20*t2 + 50*Power(t2,2))))) + 
         Power(s,2)*(-((5 + s1)*Power(t1,7)) - 12*Power(-1 + t2,2)*t2 + 
            Power(t1,6)*(19 - 16*t2 + s1*(20 + t2)) + 
            Power(t1,5)*(-33 + Power(s1,2) + 135*t2 - 9*Power(t2,2) + 
               2*s1*(-48 + 5*t2)) + 
            t1*(-1 + t2)*(26 - 61*t2 + 26*Power(t2,2) + 
               s1*(-22 + 26*t2)) + 
            Power(t1,2)*(80 + Power(s1,2)*(8 - 6*t2) - 112*t2 + 
               82*Power(t2,2) - 12*Power(t2,3) + 
               s1*(-69 + 97*t2 - 54*Power(t2,2))) + 
            Power(t1,4)*(57 - 4*Power(s1,3) - 277*t2 + 93*Power(t2,2) - 
               10*Power(t2,3) + Power(s1,2)*(18 + 7*t2) + 
               3*s1*(64 - 43*t2 + Power(t2,2))) + 
            Power(t1,3)*(-92 + 6*Power(s1,3) + 195*t2 - 
               107*Power(t2,2) + 5*Power(t2,3) - 
               Power(s1,2)*(31 + 8*t2) + 
               s1*(-68 + 77*t2 + 33*Power(t2,2))) + 
            Power(s2,5)*(-3*Power(s1,3) + 2*Power(s1,2)*(1 + 6*t2) - 
               s1*(3 + Power(t1,2) + 9*t2 + 15*Power(t2,2) - 
                  t1*(4 + 5*t2)) + 
               t2*(5 + 3*Power(t1,2) + 7*t2 + 6*Power(t2,2) - 
                  t1*(8 + 5*t2))) + 
            Power(s2,4)*(1 + Power(s1,3)*(-3 + 8*t1) + t2 + 
               17*Power(t2,2) + 10*Power(t2,3) - 
               2*Power(t1,3)*(1 + 5*t2) + 
               Power(t1,2)*(5 + 26*t2 + 12*Power(t2,2)) - 
               t1*(4 + 17*t2 + 36*Power(t2,2) + 15*Power(t2,3)) - 
               Power(s1,2)*(-1 + Power(t1,2) - 13*t2 + 
                  7*t1*(1 + 4*t2)) + 
               s1*(6 + 2*Power(t1,3) - 14*t2 - 20*Power(t2,2) - 
                  Power(t1,2)*(3 + 7*t2) + 
                  5*t1*(-1 + 7*t2 + 7*Power(t2,2)))) + 
            Power(s2,2)*(3 + Power(s1,3)*(10 - 21*t1)*t1 + 7*t2 + 
               28*Power(t2,2) - 3*Power(t1,5)*(7 + 2*t2) + 
               Power(t1,4)*(89 - 3*t2 + 4*Power(t2,2)) - 
               Power(t1,3)*(158 - 165*t2 + 91*Power(t2,2) + 
                  3*Power(t2,3)) + 
               4*Power(t1,2)*
                (34 - 63*t2 + 66*Power(t2,2) + 6*Power(t2,3)) - 
               t1*(49 - 89*t2 + 170*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s1,2)*(-3*Power(t1,4) - 4*Power(t1,3)*(-4 + t2) + 
                  2*t2 - t1*(53 + 22*t2) + Power(t1,2)*(75 + 58*t2)) + 
               s1*(10 - 5*Power(t1,5) - 45*t2 + 9*Power(t2,2) + 
                  Power(t1,4)*(41 + 12*t2) + 
                  t1*(-54 + 213*t2 - 5*Power(t2,2)) + 
                  Power(t1,3)*(-139 + 5*t2 + 3*Power(t2,2)) - 
                  3*Power(t1,2)*(-49 + 85*t2 + 14*Power(t2,2)))) + 
            s2*(-3 + Power(s1,3)*Power(t1,2)*
                (-14 + 15*t1 + Power(t1,2)) - 3*t2 + 6*Power(t2,2) + 
               Power(t1,6)*(17 + t2) - 
               Power(t1,5)*(67 - 27*t2 + Power(t2,2)) - 
               t1*(11 + 51*t2 + 14*Power(t2,2)) + 
               Power(t1,4)*(121 - 282*t2 + 47*Power(t2,2)) + 
               Power(t1,2)*(69 - 170*t2 + 181*Power(t2,2) - 
                  12*Power(t2,3)) + 
               2*Power(t1,3)*
                (-63 + 239*t2 - 116*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-11*Power(t1,3) + Power(t1,4) + 4*(-2 + t2) + 
                  24*t1*(3 + t2) - Power(t1,2)*(67 + 30*t2)) + 
               s1*(-2 + 4*Power(t1,6) + 8*t2 - 6*Power(t2,2) - 
                  6*Power(t1,5)*(8 + t2) + 
                  Power(t1,3)*(-337 + 315*t2 - 16*Power(t2,2)) + 
                  t1*(9 + 48*t2 - 5*Power(t2,2)) + 
                  Power(t1,4)*(207 - 8*t2 + Power(t2,2)) + 
                  Power(t1,2)*(167 - 331*t2 + 24*Power(t2,2)))) + 
            Power(s2,3)*(Power(s1,3)*(-2 + 13*t1 - 6*Power(t1,2)) + 
               Power(t1,4)*(11 + 12*t2) - 
               2*Power(t1,3)*(23 + 13*t2 + 5*Power(t2,2)) + 
               2*(5 + 2*t2 + 23*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(69 + 4*t2 + 77*Power(t2,2) + 
                  12*Power(t2,3)) - 
               2*t1*(22 - 3*t2 + 63*Power(t2,2) + 17*Power(t2,3)) + 
               Power(s1,2)*(3*Power(t1,3) + 6*(2 + t2) - 
                  3*t1*(9 + 16*t2) + Power(t1,2)*(-1 + 20*t2)) + 
               s1*(-5 + Power(t1,4) - 39*t2 - 12*Power(t2,2) - 
                  Power(t1,3)*(14 + 5*t2) - 
                  3*Power(t1,2)*(-12 + 11*t2 + 8*Power(t2,2)) + 
                  t1*(-18 + 103*t2 + 65*Power(t2,2))))) + 
         s*(Power(s2,6)*Power(s1 - t2,2)*(-1 + s1 + t1 - t2) - 
            Power(s2,5)*(s1 - t2)*
             (-1 + Power(s1,2)*(-1 + 3*t1) - 3*t2 - Power(t2,2) - 
               Power(t1,2)*(1 + 3*t2) + t1*(2 + 6*t2 + 3*Power(t2,2)) + 
               2*s1*(2 + 2*Power(t1,2) + t2 - t1*(4 + 3*t2))) + 
            s2*(Power(s1,3)*Power(t1,2)*
                (6 - 13*t1 + 8*Power(t1,2) + Power(t1,3)) - 
               Power(-1 + t2,2)*(13 + 2*t2) + Power(t1,6)*(6 + 8*t2) - 
               Power(t1,5)*(31 + 94*t2 + 4*Power(t2,2)) + 
               2*t1*(20 - 26*t2 - Power(t2,2) + 7*Power(t2,3)) - 
               Power(t1,2)*(29 - 249*t2 + 115*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(t1,4)*(56 + 381*t2 - 165*Power(t2,2) + 
                  20*Power(t2,3)) - 
               Power(t1,3)*(29 + 516*t2 - 295*Power(t2,2) + 
                  25*Power(t2,3)) - 
               Power(s1,2)*t1*
                (Power(t1,4)*(-8 + t2) + 4*(-1 + t2) + 18*t1*(1 + t2) + 
                  Power(t1,3)*(53 + 25*t2) - Power(t1,2)*(59 + 42*t2)) + 
               s1*(-18*Power(t1,6) + Power(t1,5)*(78 - 5*t2) + 
                  8*Power(-1 + t2,2) + 
                  t1*(-29 + 52*t2 - 23*Power(t2,2)) + 
                  Power(t1,4)*(-288 + 242*t2 - 8*Power(t2,2)) + 
                  Power(t1,3)*(401 - 395*t2 + 5*Power(t2,2)) + 
                  2*Power(t1,2)*(-76 + 61*t2 + 12*Power(t2,2)))) + 
            Power(s2,2)*(-(Power(s1,3)*t1*
                  (6 - 15*t1 + 13*Power(t1,2) + 3*Power(t1,3))) + 
               Power(t1,5)*(-6 + t2) + 7*(-1 + t2)*t2 + 
               Power(t1,4)*(21 + 77*t2 - 4*Power(t2,2)) + 
               3*Power(t1,2)*(5 + 136*t2 - 79*Power(t2,2) + 
                  15*Power(t2,3)) - 
               Power(t1,3)*(27 + 325*t2 - 152*Power(t2,2) + 
                  17*Power(t2,3)) - 
               t1*(3 + 154*t2 - 82*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s1,2)*(Power(t1,5) + 2*(-1 + t2) + 3*t1*(11 + t2) + 
                  Power(t1,4)*(-5 + 3*t2) - 3*Power(t1,2)*(37 + 6*t2) + 
                  Power(t1,3)*(84 + 31*t2)) - 
               s1*(4 + Power(t1,5)*(-17 + t2) - 11*t2 + 7*Power(t2,2) + 
                  Power(t1,4)*(59 + 6*t2) + 
                  t1*(-115 + 148*t2 - 39*Power(t2,2)) + 
                  Power(t1,3)*(-174 + 207*t2 - 4*Power(t2,2)) + 
                  3*Power(t1,2)*(81 - 117*t2 + 19*Power(t2,2)))) + 
            Power(s2,4)*(Power(s1,3)*(1 - 5*t1 + 2*Power(t1,2)) + 
               2*Power(s1,2)*(-4 + 3*Power(t1,3) - 3*t2 - 
                  Power(t1,2)*(7 + 4*t2) + 2*t1*(4 + 5*t2)) + 
               t2*(-3 - 16*t2 - 3*Power(t2,2) + Power(t1,3)*(7 + 3*t2) - 
                  Power(t1,2)*(17 + 16*t2 + 3*Power(t2,2)) + 
                  t1*(13 + 29*t2 + 8*Power(t2,2))) - 
               s1*(1 - 24*t2 - 8*Power(t2,2) + Power(t1,3)*(3 + 9*t2) - 
                  Power(t1,2)*(5 + 30*t2 + 9*Power(t2,2)) + 
                  t1*(1 + 45*t2 + 23*Power(t2,2)))) + 
            Power(s2,3)*(-4 + 
               Power(s1,3)*(2 - 7*t1 + 11*Power(t1,2) + 2*Power(t1,3)) + 
               41*t2 - 12*Power(t2,2) + 7*Power(t2,3) - 
               Power(t1,4)*(-2 + 11*t2 + Power(t2,2)) + 
               t1*(10 - 108*t2 + 74*Power(t2,2) - 9*Power(t2,3)) + 
               Power(t1,3)*(-2 - 4*t2 + 19*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(6 - 82*t2 + 80*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s1,2)*(8*Power(t1,3) - 4*Power(t1,4) + 
                  4*(-4 + t2) + t1*(65 + 6*t2) - Power(t1,2)*(53 + 34*t2)\
) + s1*(-5 + 16*t2 - 13*Power(t2,2) + Power(t1,4)*(-3 + 5*t2) + 
                  Power(t1,3)*(10 - 15*t2 - 3*Power(t2,2)) + 
                  t1*(14 - 103*t2 + 10*Power(t2,2)) + 
                  Power(t1,2)*(-16 + 97*t2 + 30*Power(t2,2)))) + 
            (-1 + t1)*(Power(t1,6)*(-2 + 6*s1 - 4*t2) - 
               4*Power(-1 + t2,3) + 
               t1*Power(-1 + t2,2)*(-26 + 8*s1 + 11*t2) + 
               Power(t1,5)*(10 - 4*Power(s1,2) + 32*t2 + 
                  s1*(-26 + 8*t2)) - 
               Power(t1,2)*(-1 + t2)*
                (42 + 2*Power(s1,2) - 38*t2 + 3*Power(t2,2) + 
                  s1*(-25 + 22*t2)) - 
               Power(t1,4)*(12 + 2*Power(s1,3) + 121*t2 - 
                  70*Power(t2,2) + 4*Power(t2,3) - 
                  Power(s1,2)*(6 + 11*t2) + 
                  2*s1*(-52 + 43*t2 + 3*Power(t2,2))) + 
               Power(t1,3)*(2*Power(s1,3) + Power(s1,2)*(1 - 13*t2) + 
                  s1*(-67 + 37*t2 + 28*Power(t2,2)) - 
                  2*(8 - 61*t2 + 35*Power(t2,2) + 2*Power(t2,3))))))*T5q(s))/
     (s*(-1 + s1)*(-1 + s2)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*
       (-s + s1 - t2)));
   return a;
};
