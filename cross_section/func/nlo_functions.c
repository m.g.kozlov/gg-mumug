#include <math.h>
#include <tgmath.h>
#include <quadmath.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_sf_dilog.h>
#include <gsl/gsl_pow_int.h>
#include <gsl/gsl_sf_log.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>

#include "../h/dilog.h"



#define Pi       3.141592653589793238462643383279502884L /* pi */
#define ln gsl_sf_log_abs



#define plus gsl_complex_add
#define div gsl_complex_div

double B1_inty(double , void *);
double B1_integrand(double , void *);

struct mc_pars 
{
    double s;
    double mf;
    double om_min;
    double th_particles_min;
    double th_max;
    double th_gmu;
    double om_s; //omega*
};



int cuts(double s, double s1, double s2,double t1, double t2)
{
    double Em=30./105.6;    //minimum energy of photon
    //double Emu=1.2;          //minimum energy of muon
    double THc=10./180.*Pi; //minimum angle of photons and muons
    double TH1=0./180.*Pi; //minimum angle between muons and photon
    //double E=sqrt(s)/2; //energy of inital photons
    double E1=(s-s2+1.)/2./sqrt(s), E2=(s-s1+1.)/2./sqrt(s); //energy of muons
    double omega=(s1+s2-2.)/2./sqrt(s); // energy of final photon
    
    //return ( (s1+s2-2)/sqrt(s)>=Em && (s1+t1-t2-1)/(s1+s2-2)>(1-cos(THc))  && (s2-t1+t2-1)/(s1+s2-2)>(1-cos(THc)) );
    //return ((s-s2+1)/(2*sqrt(s))>Emu && (s-s1+1)/(2*sqrt(s))>Emu && (s1+s2-2)/sqrt(s)>=Em && fabs((s1-s2+2*(t1-t2))/(s1+s2-2))<cos(THc) && fabs((s-s1-1+2*t2)/sqrtl(Power(s-s1+1,2)-4*s))<cos(THc) && fabs((s-s2-1+2*t1)/sqrtl(Power(s-s2+1,2)-4*s))<cos(THc) );
    return (omega > Em && fabs((s1-s2+2*(t1-t2))/(s1+s2-2))<cos(THc) && fabs((s-s1-1+2*t2)/sqrtl(pow(s-s1+1,2)-4*s))<cos(THc) && fabs((s-s2-1+2*t1)/sqrtl(pow(s-s2+1,2)-4*s))<cos(THc) && (2*E1*omega+1.-s1)/(2.*omega*sqrt(E1*E1-1.))<cos(TH1) && (2*E2*omega+1.-s2)/(2.*omega*sqrt(E2*E2-1.))<cos(TH1) );
};





double GG(double x,double y,double z,double u,double v,double w)
{
    return pow(v,2)*w + pow(u,2)*z + u*((w - x)*(-v + y) - (v + w + x + y)*z + pow(z,2)) + x*(y*(x + y - z) + w*(-y + z)) + v*(pow(w,2) + y*(-x + z) - w*(x + y + z)) ;
};

int Tphys(double s,double s1,double s2,double t1,double t2)
{
    return GG(s,t1,s2,0.,0.,1.)<0 && GG(s1,s2,s,0.,1.,1.)<0;
};


int inv_test(double s, double s1, double s2, double t1, double t2)
{
    return cuts(s,s1,s2,t1,t2) && Tphys(s,s1,s2,t1,t2);
};

long double pq(long double x,unsigned int a)
{
    long double tmp=1.L;
    do
    {
        if (a & 1) tmp=tmp*x;
        a>>=1;
        x=x*x;
    }
    while(a);
    return tmp;
};


/*some functions*/

double Power(double x,int a)
{
    return gsl_pow_int(x,a);
};



long double Log(long double x)
{
    return logl(fabsl(x));
};

long double lg2(long double x)
{
    long double a;
    if( x > 0.L ) 
    {
        a = logl(x);
        return a*a;
    }
    else
    {
        a = logl(-x);
        return a*a - Pi*Pi;
    };
    
};



long double Sqrt(long double x)
{
    return sqrtl(x);
};


long double sign(long double x)
{
    if(x<0.) return -1.L;
    else return 1.L;
};

long double ln2b(long double x)
{
    if(x<0) 
    {
        return pq( Log((1.L+sqrtl(1.L-4.L/x))/(sqrtl(1.L-4.L/x)-1.L)) ,2);
    };
    if(x>4)
    {
        return pq( Log((1.L+sqrtl(1.L-4.L/x))/(1.L-sqrtl(1.L-4.L/x))) ,2)-pq(Pi,2);
    }
    else
    {
        return -pq(Pi-2.L*atanl(sqrtl(4.L/x-1.L)),2);
    };   
    if(x==0) return 0.L;
    return 0.L;
};




long double Li2(long double x)
{
    return dilog(x);
   };
   
   
long double PolyLog(int a,long double x)
{
    return dilog(x);
};

   
long double Li2im(long double re,long double im)
{
    long double result_re, result_im;
    
    dilogc(re,im,&result_re,&result_im);
       
    return result_im;
};

long double Li2re(long double re,long double im)
{
    long double result_re, result_im;
    
    dilogc(re,im,&result_re,&result_im);
       
    return result_re;
};




long double ht(long double x)
{
    if (x>0.) return 1.L;
    else return 0.L;
    };

long double R(long double x,long double y)
{
    return dilog(x/(x-y))-dilog((x-1.L)/(x-y));
    };
    
    
long double Rv2(long double a,long double b,long double y)
{
    long double re1,re2,im1,im2;
    if(b<0.)
    {
        re1=(a*(a-y)-b)/(pq(a-y,2)-b);
        im1=-y*sqrtl(-b)/(pq(a-y,2)-b);
        re2=((a-1.0L)*(a-y)-b)/(pq(a-y,2)-b);
        im2=(1.0L-y)*sqrtl(-b)/(pq(a-y,2)-b);
        return 2.0L*(Li2im(re1,im1)-Li2im(re2,im2));
    }
    else
    {
        return R(a+sqrtl(b),y)-R(a-sqrtl(b),y);
    };
    return 0.0L;
};

long double Rv3(long double x,long double a,long double b)
{
    long double re1,re2,im1,im2;
    if(b<0.)
    {
        re1=(x*(x-a))/(pq(x-a,2)-b);
        im1=x*sqrtl(-b)/(pq(x-a,2)-b);
        re2=(x-1.0L)*(x-a)/(pq(x-a,2)-b);
        im2=(x-1.0L)*sqrtl(-b)/(pq(x-a,2)-b);
              
        return 2.0L*(Li2re(re1,im1)-Li2re(re2,im2));
    }
    else
    {
        return R(x,a+sqrtl(b))+R(x,a-sqrtl(b));
    };
    return 0.0L;
    
};

long double reRc(long double a,long double b,long double y)
{
    long double re1,re2,im1,im2;
    
    re1=(a*a+b*b-y*a)/(pq(a-y,2)+b*b);
    im1=-b*y/(pq(a-y,2)+b*b);
    re2=((a-1.0L)*(a-y)+b*b)/(pq(a-y,2)+b*b);
    im2=b*(1.0L-y)/(pq(a-y,2)+b*b);
    
    return Li2re(re1,im1)-Li2re(re2,im2);    
};

long double imRc(long double a,long double b,long double y)
{
    long double re1,re2,im1,im2;
    
    re1=(a*a+b*b-y*a)/(pq(a-y,2)+b*b);
    im1=-b*y/(pq(a-y,2)+b*b);
    re2=((a-1.0L)*(a-y)+b*b)/(pq(a-y,2)+b*b);
    im2=b*(1.0L-y)/(pq(a-y,2)+b*b);
    
    return Li2im(re1,im1)-Li2im(re2,im2);    
};



long double RD(long double a,long double b,long double y,long double D)
{
    if(D>=0. && b<=0.) return 0.L;
    if(D>0. && b>0.) return Rv2(a,b,y)/sqrtl(D);
    if(D<0. && b<0.) return Rv2(a,b,y)/sqrtl(-D);
    return 0.L;
};
    

    
    
long double S3(long double x,long double x1,long double x2)
{
    return R(x,x1)+R(x,x2);
    };
    

long double reS3c(long double a,long double b, long double x1, long double x2)
{
    return reRc(a,b,x1)+reRc(a,b,x2);
};

long double imS3c(long double a,long double b,long  double x1,long  double x2)
{
    return imRc(a,b,x1)+imRc(a,b,x2);
};


    
long double S3v2(long double ya,long double yb,long  double xa,long  double xb)
{
    long double re1,re2,im1,im2,re3,re4,im3,im4;
    if(xb>=0.)
    {
        return Rv2(ya,yb,xa+sqrt(xb))+Rv2(ya,yb,xa-sqrt(xb));
    };
    if(yb>=0.)
    {
        return Rv3(ya+sqrt(yb),xa,xb)-Rv3(ya-sqrt(yb),xa,xb);
    };
    if(xb<0. && yb<0.)
    {
        re1=(ya*(ya-xa)-yb-sqrtl(xb*yb))/(pq(ya-xa,2)+pq(sqrtl(-xb)-sqrtl(-yb),2));
        im1=(ya*sqrtl(-xb)-xa*sqrtl(-yb))/(pq(ya-xa,2)+pq(sqrtl(-xb)-sqrtl(-yb),2));
        re2=((ya-1.0L)*(ya-xa)-yb-sqrtl(xb*yb))/(pq(ya-xa,2)+pq(sqrtl(-xb)-sqrtl(-yb),2));
        im2=((1.0L-xa)*sqrtl(-yb)+(ya-1.0L)*sqrtl(-xb))/(pq(ya-xa,2)+pq(sqrtl(-xb)-sqrtl(-yb),2));
        
        re3=(ya*(ya-xa)-yb+sqrtl(xb*yb))/(pq(ya-xa,2)+pq(sqrtl(-xb)+sqrtl(-yb),2));
        im3=(-ya*sqrtl(-xb)-xa*sqrtl(-yb))/(pq(ya-xa,2)+pq(sqrtl(-xb)+sqrtl(-yb),2));
        re4=((ya-1.0L)*(ya-xa)-yb+sqrtl(xb*yb))/(pq(ya-xa,2)+pq(sqrtl(-xb)+sqrtl(-yb),2));
        im4=((1.0L-xa)*sqrtl(-yb)+(1.0L-ya)*sqrtl(-xb))/(pq(ya-xa,2)+pq(sqrtl(-xb)+sqrtl(-yb),2));
        
        return 2.0L*(Li2im(re1,im1)-Li2im(re2,im2)+Li2im(re3,im3)-Li2im(re4,im4));
    };
    return 0.L;
};
    

long double S3D(long double ya,long double yb, long double xa, long double xb,long double D)
{
    if(yb>=0. && xb>=0. && D>=0.)
    {
        return S3v2(ya,yb,xa,xb)/sqrtl(D);
    };
    if(yb>0. && xb>0. && D<0.) return 0.L;
    if(yb<0. && xb>0. && D>0.) return 0.L;
    if(yb>0. && xb<0. && D>0.)
    { 
        return S3v2(ya,yb,xa,xb)/sqrtl(D);
    };
    if(yb<0. && xb<0. && D>0.) return 0.L;
    if(yb<0. && xb>0. && D<0.) 
    {
        return S3v2(ya,yb,xa,xb)/sqrtl(-D);
    };
    if(yb>0. && xb<0. && D<0.) return 0.L;
    if(yb<0. && xb<0. && D<0.) 
    {
        return S3v2(ya,yb,xa,xb)/sqrtl(-D);
    };
    return 0.L;
};
    

long double ff(long double x, long double y)
{
    long double a;
    a=pq(Pi,2)*(ht(-x) + ht(-y))*ht(-1.L + x*y) + ht(-(x*y))*(pq(Pi,2)/6.L-dilog(x*y))-(Log(x) + Log(y))*Log(1.L-x*y)+
    ht(x*y)*(dilog(1.L-x*y)+Log(x*y)*Log(1.L - x*y));
    return a;
    };

long double bb(long double x)
{
    return sqrtl(1.L-4.L/x);
    };

long double X(long double a)
{
    return (bb(a)-1.L)/(bb(a)+1.L);
    };

/*Simple functions for master integrals*/    
    
long double R1(long double x)
{
    return (1.L/x-1.L)*Log(1.L-x);
    };
    
long double R1n(long double t1,long double t2)
{
   long double a; 
   if(fabsl(t1-t2)>0.001)
   {
        a=((-1.L + 1.L/t1)*Log(1.L - t1) - ((t1 - t2)*(-t2 - Log(1.L - t2)))/pq(t2,2) + ((-1.L + t2)*Log(1.L - t2))/t2 - (pq(t1 - t2,2)*(-2.L*t2 + pq(t2,2) - 2.L*Log(1.L - t2) + 2.L*t2*Log(1.L - t2)))/(2.L*(-1.L + t2)*pq(t2,3)))/
   pq(t1 - t2,3);
   }
   else 
   {
      a=-1.L/(3.L*pq(-1.L + t2,3)) + 1.L/((-1.L + t2)*pq(t2,3)) + 1.L/(2.L*pq(-1.L + t2,2)*pq(t2,2)) + 1.L/(3.L*pq(-1.L + t2,3)*t2) - Log(1.L - t2)/pq(t2,4) + 
   (t1 - t2)*(1.L/(4.L*pq(-1.L + t2,4)) - 1.L/((-1.L + t2)*pq(t2,4)) - 1.L/(2.L*pq(-1.L + t2,2)*pq(t2,3)) - 1.L/(3.L*pq(-1.L + t2,3)*pq(t2,2)) - 1.L/(4.L*pq(-1.L + t2,4)*t2) + logl(1.L - t2)/pq(t2,5));
    };
    return a;
};
    
    
    
    
    
    

long double R2(long double x)
{
    if(x<0.L || x>4.L)
    { 
        return -sqrtl(1.L - 4.L/x)*logl( fabsl( (1.L + sqrtl(1.L - 4.L/x))/(1.L - sqrtl(1.L - 4.L/x)) ) );
    }
    else
    {
        return sqrtl(4.L/x-1.L)*(2.L*atanl(sqrtl(4.L/x-1.L))-Pi);
    };
};

long double R2n(long double x)
{
    return (R2(x)+2.)/x;
};


double i_r21(double x, void * params)
{
    double s=*(double *) params;
    double f = lg2(1.-s*x*(1.-x))/2.;
    return f;
};

long double R21(long double x)
{
    long double b;
    double ires=1, error;
    double pars[1];
    
    if(x<0.L || x>4.L)
    { 
        
        b = sqrtl(1.L - 4.L/x);
        return (8.L + b*logl(fabsl( (1.L+b)/(1.L-b) ))*( logl(fabsl(x)) - 4.L + 2.L*logl(b) - Pi*Pi*ht(x) ) + 2.L*b*( dilog((1.L-1.L/b)/2.L) - dilog((1.L+1.L/b)/2.L) ) )/2.L + Pi*Pi/6.L;
        
    }
    else
    {
            
        
        gsl_integration_workspace *w=gsl_integration_workspace_alloc(1000);
    
        pars[0]=x;
    
        gsl_function func;
        func.function=&i_r21;
        func.params=pars;
    
        gsl_integration_qags(&func,0.,1.,1e-7,1e-7,1000,w,&ires,&error);
        gsl_integration_workspace_free(w);
        
        
        
        return (ires + Pi*Pi/6.);
    };
};



/*
long double T1(long double x)
{
    return  (pq(Pi,2)/6.L-dilog(1.L+x))/x;
    };*/




long double T3(long double t1,long double t2)
{
    return -(dilog(t1)-dilog(t2))/(t1-t2);
    };
    
    
long double T3n(long double t1,long double t2)
{
    long double a;
    if(fabsl(t1-t2)>0.001)
    {
        a=((t1 - t2)*(t2*(-t1 + t2) + (t1 - 3.L*t2)*(-1.L + t2)*Log(1.L - t2)) - 2.L*(-1.L + t2)*pq(t2,2)*dilog(t1) + 2.L*(-1.L + t2)*pq(t2,2)*dilog(t2))/(2.L*pq(t1 - t2,3)*(-1.L + t2)*pq(t2,2));
    }
    else
    {
        a=(2.L*t2 - 3.L*pq(t2,2) + 2.L*Log(1.L - t2) - 4.L*t2*Log(1.L - t2) + 2.L*pq(t2,2)*Log(1.L - t2))/(6.L*pq(-1.L + t2,2)*pq(t2,3)) - 
   ((t1 - t2)*(-6.L*t2 + 15.L*pq(t2,2) - 11.L*pq(t2,3) - 6.L*Log(1.L - t2) + 18.L*t2*Log(1.L - t2) - 18.L*pq(t2,2)*Log(1.L - t2) + 6.L*pq(t2,3)*Log(1.L - t2)))/(24.L*pq(-1.L + t2,3)*pq(t2,4));
    };
    return a;
};
    




    
    
long double T4(long double x)
{
    if(fabsl(x)<0.0000000000001L) return -Pi*Pi/6.+(1-Pi*Pi/6.)*x;
    return (dilog(1.L-x)+Log(x)*Log(x-1.L))/(x-1.L);
     };
     
long double T1(long double x)
{
    return  T4(x);
    };
    
long double T5(long double x)
{
    return ln2b(x)/2.L/x;
    };
    
long double T5n(long double x)
{
    return (ln2b(x)/x+1.L)/(2.L*x);
    };

long double T6(long double s,long double t)
{
    return (ln2b(s)-ln2b(t))/(2.L*(s - t));
    };
    
long double T7(long double s,long double t)
{
    return T6(s,t);
    };

double F(double x)
{
    return (x*ln((1 + sqrtl(1-Power(x,-2)))/(1-sqrtl(1-Power(x,-2)))))/sqrtl(-1+Power(x,2));
    };



/*Complicated functions*/

long double T2(long double t1, long double t2)
{
    long double x1,x2,x3,x31,x32,D3;
    D3=sqrtl(pq((t1+t2-1.L),2)-4.L*t1*t2);
    x1=(1.L-pq(t1,2)-t2+t1*t2+(1.L+t1)*D3)/2.L/t1/D3;
    x2=-(-2.L+2.L*t1+3.L*t2+t1*t2-pq(t2,2)+(t2-2.L)*D3)/(D3*(1.L+t1-t2+D3));
    x3=-(-2.L+2.L*t1+3.L*t2+t1*t2-pq(t2,2)+(t2-2.L)*D3)/(D3*(1.L-t1-t2+D3));
    x31=(-sqrtl((-4.L+t2)*t2)+t2)/(2.L*t2);
    x32=(sqrtl((-4.L+t2)*t2)+t2)/(2.L*t2);
    if(fabsl(t1-1.L)<0.0000000000001L) return  S3(x3,x31,x32)/D3;
    return (S3(x1,1.L,1.L/t1)-S3(x2,1.L,1.L)+S3(x3,x31,x32))/D3;        
    };

    






long double B2(long double s,long double t,long double m)
{
    return (pq(Pi,2)/6.L-2.L*(ff(X(s),1.L/X(m)) + ff(X(s),X(m))) + pq(Pi,2)*ht(-X(m)) - dilog(pq(X(s),2)) - pq(Log(X(m)),2) - 2.*(-(pq(Pi,2)*ht(-1.+t)*ht(-X(s))) + Log(1.L-t)*Log(X(s))) - 2.L*Log(X(s))*Log(1.L-pq(X(s),2)))/(s*(-1.L+t)*bb(s));    
    };


long double B33(long double s, long double t,long  double w)
{
    return   -(2.*pq(Pi,2) - 3.*( -pq(Log(s),2) + pq(Log((pq(s,2)*t)/(s - w)),2) + 2.*pq(Log((s*t)/(t - w)),2) + 
         pq(Log((s*t)/(s + t - w)),2) - pq(Log((s*t)/(-s + w)),2) - pq(Log((s*(-s + w))/(s + t - w)),2) - 2.*pq(Log((s*t*w)/((s - w)*(-t + w))),2)) - 12.*dilog(1.L + s/(t - w)) + 
      6.*dilog((s + t - w)/t) + 6.*dilog(t/(-s + w)) + 12.*dilog(((s + t - w)*w)/((s - w)*(-t + w))))/(6.*s*t);  
};

long double h_b3(long double s, long double t, long double m)
{
    long double SS, tmp, D;
    D=(1.0L+4.0L*(m-s-t)/(s*t));
    SS=sqrtl(-D);
    
    tmp = 2.L*Pi*Log((1.L - D)*( pq(2*(m - s) - t,2) - t*t*D)/( 1.L-4.L/t-D )/sqrtl(pq((m-s-t)*sqrtl(1.L-4.L/s)+m-s,2)-t*t*D)/sqrtl(pq((m-s-t)*sqrtl(1.L-4.L/s)-m+s,2)-t*t*D))/(s*t*SS);
    
    return tmp;
    
};

long double h2_b3(long double s, long double t, long double m)
{
    long double SS, tmp, D;
    D=(1.0L+4.0L*(m-s-t)/(s*t));
    SS=sqrtl(-D);
    
    tmp = 2.L*Pi*Log(( pq(2*(m - s) - t,2) - t*t*D)*( 1.L-4.L/m-D )/(1.L - D)/sqrtl(pq((m-s-t)*sqrtl(1.L-4.L/s)+m-s,2)-t*t*D)/sqrtl(pq((m-s-t)*sqrtl(1.L-4.L/s)-m+s,2)-t*t*D))/(s*t*SS);
    
    return tmp;
    
};

long double g_b3(long double s, long double t, long double m)
{
    long double SS, tmp, D;
    D=(1.0L+4.0L*(m-s-t)/(s*t));
    SS=sqrtl(-D);
    
    tmp = 2.L*Pi*Log((m-s-t)*m/(m-s)/(m-t))/(s*t*SS);
    
    return tmp;
    
};

long double g2_b3(long double s, long double t, long double m)
{
    long double SS, tmp, D;
    D=(1.0L+4.0L*(m-s-t)/(s*t));
    SS=sqrtl(-D);
    
    tmp = 2.L*Pi*Log((pq(t+2.*(s-m),2)-D*t*t)/(pq(s-m,2)+pq(t*sqrtl(-D)-(s+t-m)*sqrtl(4./s-1.),2)))/(s*t*SS);
    
    return tmp;
    
};

long double B3(long double s,long double t,long double m)
{
    
    long double a1,a2,a3,b1,b2,b3;
    long double y1a,y1b,y2a,y2b;
    long double D, tmp;
    D=1.0L+4.0L*(m-s-t)/(s*t);
    
    y1a=-1.L/2.L;
    y1b=D/4.L;
    
    y2a=t/(s+t-m)/2.L;
    y2b=pq(t/(s+t-m)/2.L,2)*D;
    
    a1=1.L/2.L;
    b1=(1.L-4.L/m)/4.L;
    a2=1.L/2.L;
    b2=(1.L-4.L/t)/4.L;
    a3=1.L/2.L;
    b3=(1.L-4.L/s)/4.L;
    
     tmp=( 
            RD(-y1a,y1b,1.L,D)-RD(1.L+y1a,y1b,0.L,D)+sign(y2a)*RD(y2a,y2b,0.L,D)-sign(y2a)*RD(y2a,y2b,-t/(m-s-t),D)+\
            + S3D(1.L+y1a,y1b,a1,b1,D) - S3D(-y1a,y1b,a2,b2,D) - sign(y2a)*S3D(y2a,y2b,a3,b3,D) 
        )/(s*t);
    
    if( isnan(tmp) ) tmp = B33(s,t,m);
    
    if (  (s<0 && t<0) || (s<0 && m<0) || (t<0 && m<0) ) return tmp;
    if ( m>0 && s>0 && t>0 && m>s && m>t ) return tmp;
    if ( (s>0 && t>0) && s>m && t>m && D>0) return tmp - 4.L*Pi*Pi/(s*t*sqrtl(D));
    if ( (s>0 && t>0 && m>0) && D>0 && ( (s>m && m>t) || (t>m && m>s) ) ) return tmp - 4.L*Pi*Pi/(s*t*sqrtl(D));
    if ( ((s>0 && t<0)||(s<0 && t>0)) && D>0) return tmp;
    if ( ((s>0 && s<4 && t<0)||(s<0 && t<4 && t>0)) && s>m && m>0) return tmp;
    if ( ((s>4 && t<0)) && D<0 && m > 4) return tmp - h2_b3(s,t,m);
    if ( ((s<0 && t>4)) && D<0 && m > 4) return tmp - h2_b3(t,s,m);
    if ( (s<4 && s>0 && m>t && t<4)  && D<0 && m>0)  return tmp + g_b3(s,t,m);
    if ( (t<4 && t>0 && m>s && s<4)  && D<0 && m>0)  return tmp + g_b3(t,s,m);
    if ( (s>0 && s<4 && D<0) && t<0) 
    {
        if((2.*(s-m)+t)/t-sqrtl(D/(1.-4./s))<0) return tmp - g2_b3(s,t,m) + g_b3(s,t,m);
        else return tmp + g_b3(s,t,m);
    };
    if ( (t>0 && t<4 && D<0) && s<0) 
    {
        if((2.*(t-m)+s)/s-sqrtl(D/(1.-4./t))<0) return tmp - g2_b3(t,s,m) + g_b3(t,s,m);
        else return tmp + g_b3(t,s,m);
    };
    if ( (s>0 && t>0) && D<0) return tmp - h_b3(s,t,m);
    
    return tmp;
        
};



long double B3n(long double s,long double t,long double m)
{
    return ( B3(s,t,m)-(s*R2n(s)+t*R2n(t)-m*R2n(m))/(s+t-m) )/(s*t);
};


double integrand(double x, void * params)
{
    double a=*(double *) params;
    double b=*((double *) params+1);
    double c=*((double *) params+2);
    double f=ln((b-c*x+sqrt(1-Power(x,2)+Power(b*x-c,2)))/(2.*(b-c)))/(1-a*x);
    return f;
};
    
    

double G(double a, double b, double c)
{
    gsl_integration_workspace *w=gsl_integration_workspace_alloc(1000);
    
    double ires, result, error;
    double pars[3];
    
    pars[0]=a;
    pars[1]=b;
    pars[2]=c;
    
    gsl_function func;
    func.function=&integrand;
    func.params=pars;
    
    gsl_integration_qags(&func,-1,1,1e-7,1e-7,1000,w,&ires,&error);
    gsl_integration_workspace_free(w);
    
    
    result=ires+( -ln((b-c)/2.)*ln((1.-a)/(1.+a))+Power(ln(2.*a/(1.-a)),2)/4.+Power(Pi,2)/12.+Li2((a-1.)/(2.*a))/2.+Li2(2.*a/(a+1.))/2. )/a;
    
    return result;
};



long double B1(long double t,long double s,long double m)
{
    long double D=1.L+4.L*(m-t)/(s*(t-1.L));
    long double ya, yb, a, b;
    
    ya=1.L/2.L;
    yb=D/4.L;
    
    a=1.L/2.L;
    b=1.L/4.L-1.L/s;
    
    return S3D(ya,yb,a,b,D)/(s*(1.L-t));
    
};


long double B(long double s, long double t)
{
    long double x, beta, a;
    beta = sqrtl(1-4/s);
    x = (1-beta)/(1+beta);
    
    a = (-2.*logl(x)*logl(1.-x) + 2.*logl(x)*logl(1.+x) - 2.*logl(x)*logl(1.-t) - 2.*dilog(x) + 2.*dilog(-x) - Pi*Pi/2.)/(s*(t-1.)*beta);
    return a;
};

