#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m312_4_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-16*(2*Power(s,6)*(-1 + s2)*s2 + 
         Power(s,5)*s2*(10 + 2*Power(s2,2) + 3*t1 + 4*Power(t1,2) - 
            13*t2 - 8*t1*t2 + 4*Power(t2,2) - 
            4*s1*(-2 + 2*s2 - t1 + t2) + s2*(-12 - 7*t1 + 17*t2)) + 
         Power(s,4)*(-12*Power(s2,4) + Power(s2,3)*(18 + 50*t1 - 33*t2) + 
            (2 + t1)*(-1 + t1 - t2) + 
            2*Power(s1,2)*s2*(1 + 3*s2 - 2*t1 + 2*t2) + 
            Power(s2,2)*(30 - 51*Power(t1,2) - 17*t2 + 9*Power(t2,2) + 
               t1*(-50 + 62*t2)) + 
            s1*(2 + 13*Power(s2,3) + t1 + 
               Power(s2,2)*(6 - 20*t1 - 15*t2) + 
               s2*(-37 + 23*t1 + 12*Power(t1,2) + 11*t2 - 4*t1*t2 - 
                  8*Power(t2,2))) + 
            s2*(-26 + 16*Power(t1,3) + Power(t1,2)*(26 - 28*t2) + 52*t2 - 
               13*Power(t2,2) + 4*Power(t2,3) + 
               t1*(-1 - 33*t2 + 8*Power(t2,2)))) + 
         Power(s,3)*(-8*Power(s1,3)*s2 + 10*Power(s2,5) + 
            2*(-5 + 2*t1 + 2*Power(t1,2))*(-1 + t1 - t2) + 
            Power(s2,4)*(18 - 71*t1 + 15*t2) + 
            Power(s1,2)*s2*(5 - 13*Power(s2,2) + 3*t1 + 25*s2*t1 - 
               16*Power(t1,2) + 8*t2 - 4*s2*t2 + 16*t1*t2) + 
            Power(s2,3)*(-58 + 130*Power(t1,2) + t1*(4 - 51*t2) + 
               78*t2 - 31*Power(t2,2)) + 
            Power(s2,2)*(-40 - 93*Power(t1,3) - 26*t2 + Power(t2,2) - 
               4*Power(t2,3) + Power(t1,2)*(-67 + 72*t2) + 
               t1*(151 - 82*t2 + 45*Power(t2,2))) + 
            s2*(36 + 24*Power(t1,4) + Power(t1,3)*(45 - 32*t2) - 
               101*t2 + 18*Power(t2,2) - 8*Power(t2,3) - 
               Power(t1,2)*(75 + 32*t2 + 8*Power(t2,2)) + 
               t1*(-14 + 161*t2 - 25*Power(t2,2) + 16*Power(t2,3))) + 
            s1*(-10 - 2*Power(s2,4) + 4*t1 + 4*Power(t1,2) - 
               11*Power(s2,3)*(4 + t1 - 4*t2) + 
               Power(s2,2)*(14 + 2*Power(t1,2) + t1*(37 - 70*t2) - t2 + 
                  8*Power(t2,2)) + 
               s2*(82 + 8*Power(t1,3) - 23*t2 + 8*Power(t2,2) + 
                  Power(t1,2)*(26 + 24*t2) - 
                  2*t1*(57 - 11*t2 + 16*Power(t2,2))))) - 
         Power(s,2)*(2*Power(s2,6) + 8*Power(s1,3)*s2*(-4 + 3*t1) - 
            (18 - 25*t1 + 6*Power(t1,3))*(-1 + t1 - t2) - 
            Power(s2,5)*(-26 + 32*t1 + t2) + 
            Power(s2,4)*(4 + 99*Power(t1,2) + 35*t2 - 19*Power(t2,2) + 
               t1*(-81 + 16*t2)) + 
            Power(s2,3)*(-45 - 126*Power(t1,3) + 
               Power(t1,2)*(49 - 9*t2) + 99*t2 - 52*Power(t2,2) + 
               4*Power(t2,3) + t1*(88 - 135*t2 + 69*Power(t2,2))) + 
            Power(s2,2)*(-29 + 73*Power(t1,4) + 
               Power(t1,3)*(41 - 14*t2) - 20*t2 + 51*Power(t2,2) - 
               20*Power(t2,3) + 
               Power(t1,2)*(-188 + 84*t2 - 81*Power(t2,2)) + 
               t1*(119 - 58*t2 + 57*Power(t2,2) + 12*Power(t2,3))) + 
            s2*(16 - 16*Power(t1,5) - 123*t2 + 32*Power(t2,2) + 
               12*Power(t2,3) + Power(t1,4)*(-39 + 8*t2) + 
               Power(t1,3)*(119 + 28*t2 + 32*Power(t2,2)) - 
               3*Power(t1,2)*
                (31 + 53*t2 + Power(t2,2) + 8*Power(t2,3)) + 
               t1*(13 + 250*t2 - 78*Power(t2,2) + 24*Power(t2,3))) + 
            Power(s1,2)*s2*(35 - 8*Power(s2,3) + 24*Power(t1,3) + 
               Power(s2,2)*(23*t1 + 4*(-6 + t2)) + 
               Power(t1,2)*(3 - 24*t2) + 76*t2 - t1*(79 + 24*t2) + 
               s2*(53 - 39*Power(t1,2) - 20*t2 + 4*t1*(5 + 3*t2))) + 
            s1*(-18 + 3*Power(s2,5) + 25*t1 - 6*Power(t1,3) + 
               Power(s2,4)*(-13 - 44*t1 + 27*t2) + 
               Power(s2,3)*(-89 + 79*Power(t1,2) + t1*(61 - 92*t2) + 
                  76*t2 - 8*Power(t2,2)) + 
               s2*(110 + 8*Power(t1,4) - 67*t2 - 56*Power(t2,2) - 
                  4*Power(t1,3)*(5 + 14*t2) + 
                  t1*(-190 + 157*t2 - 24*Power(t2,2)) + 
                  8*Power(t1,2)*(11 + 6*Power(t2,2))) - 
               Power(s2,2)*(9 + 46*Power(t1,3) + 104*t2 - 
                  40*Power(t2,2) - 5*Power(t1,2)*(-7 + 24*t2) + 
                  t1*(-96 + 77*t2 + 24*Power(t2,2))))) + 
         (-1 + t1)*(-2*Power(s2,6)*(-1 + t1) + 
            8*Power(s1,3)*s2*
             (-2 - s2 + Power(s2,2) + 3*t1 - Power(t1,2)) + 
            (-4 + 10*t1 - 6*Power(t1,2) - Power(t1,3) + Power(t1,4))*
             (-1 + t1 - t2) + 
            Power(s2,5)*(2 - 10*t1 + 8*Power(t1,2) + 5*Power(t2,2)) - 
            Power(s2,4)*(-11 + 12*Power(t1,3) - 5*t2 + 22*Power(t2,2) - 
               4*Power(t2,3) + Power(t1,2)*(-13 + 4*t2) + 
               t1*(12 + t2 + 8*Power(t2,2))) + 
            Power(s2,3)*(9 + 8*Power(t1,4) + 34*t2 - 14*Power(t2,2) - 
               12*Power(t2,3) + Power(t1,3)*(1 + 12*t2) + 
               Power(t1,2)*(17 - 9*t2 - 7*Power(t2,2)) - 
               t1*(35 + 37*t2 - 57*Power(t2,2) + 4*Power(t2,3))) - 
            Power(s2,2)*(10 + 2*Power(t1,5) - 11*t2 - 39*Power(t2,2) + 
               Power(t1,4)*(11 + 12*t2) + 
               Power(t1,3)*(1 - 17*t2 - 18*Power(t2,2)) + 
               t1*(-7 + 59*t2 + 26*Power(t2,2) - 16*Power(t2,3)) + 
               Power(t1,2)*(-17 - 43*t2 + 41*Power(t2,2) + 4*Power(t2,3))\
) + s2*(Power(t1,5)*(5 + 4*t2) - 
               Power(t1,4)*(7 + 7*t2 + 8*Power(t2,2)) + 
               t1*(18 + 15*t2 - 71*Power(t2,2) - 16*Power(t2,3)) + 
               Power(t1,2)*(-27 + 16*t2 + 48*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(t1,3)*(13 - 14*t2 + 6*Power(t2,2) + 
                  4*Power(t2,3)) + 
               2*(-1 - 7*t2 + 12*Power(t2,2) + 8*Power(t2,3))) + 
            Power(s1,2)*s2*(5*Power(s2,4) + 
               Power(s2,2)*(-6 + 3*Power(t1,2) + t1*(39 - 4*t2) - 
                  28*t2) + Power(s2,3)*(-19 - 11*t1 + 4*t2) + 
               s2*(44 + 7*Power(t1,3) + 16*t1*(-3 + t2) + 16*t2 - 
                  Power(t1,2)*(13 + 4*t2)) - 
               (-2 + t1)*(12 + 4*Power(t1,3) + Power(t1,2)*(15 - 4*t2) + 
                  24*t2 - 4*t1*(8 + 5*t2))) + 
            s1*(-4 + 10*t1 - 6*Power(t1,2) - Power(t1,3) + Power(t1,4) - 
               2*Power(s2,5)*(-1 + t1 + 5*t2) + 
               Power(s2,4)*(-5 + 10*Power(t1,2) + 41*t2 - 
                  8*Power(t2,2) + t1*(-5 + 19*t2)) + 
               Power(s2,3)*(-18*Power(t1,3) + Power(t1,2)*(9 + 4*t2) + 
                  t1*(49 - 96*t2 + 8*Power(t2,2)) + 
                  4*(-10 + 5*t2 + 8*Power(t2,2))) + 
               Power(s2,2)*(-15 + 14*Power(t1,4) - 83*t2 - 
                  8*Power(t2,2) - Power(t1,3)*(7 + 25*t2) + 
                  t1*(79 + 74*t2 - 32*Power(t2,2)) + 
                  Power(t1,2)*(-71 + 54*t2 + 8*Power(t2,2))) + 
               s2*(14 - 4*Power(t1,5) - 48*t2 - 48*Power(t2,2) - 
                  10*Power(t1,2)*(3 + 11*t2) + Power(t1,4)*(1 + 12*t2) + 
                  Power(t1,3)*(30 + t2 - 8*Power(t2,2)) + 
                  t1*(-11 + 147*t2 + 56*Power(t2,2))))) + 
         s*(-4*Power(s2,6)*(-1 + t1) + 
            8*Power(s1,3)*s2*(-5 - s2 + Power(s2,2) + 8*t1 - 
               3*Power(t1,2)) + 
            2*(-7 + 17*t1 - 10*Power(t1,2) - 2*Power(t1,3) + 
               2*Power(t1,4))*(-1 + t1 - t2) + 
            Power(s2,5)*(12 + 24*Power(t1,2) - 13*t2 - Power(t2,2) + 
               t1*(-36 + 13*t2)) + 
            Power(s2,4)*(15 - 52*Power(t1,3) + Power(t1,2)*(75 - 35*t2) + 
               37*t2 - 35*Power(t2,2) + 4*Power(t2,3) + 
               t1*(-38 - 2*t2 + 11*Power(t2,2))) + 
            Power(s2,3)*(8 + 52*Power(t1,4) + 100*t2 - 29*Power(t2,2) - 
               8*Power(t2,3) + Power(t1,3)*(-42 + 39*t2) + 
               Power(t1,2)*(4 + 36*t2 - 45*Power(t2,2)) - 
               t1*(22 + 175*t2 - 116*Power(t2,2) + 8*Power(t2,3))) - 
            Power(s2,2)*(25 + 24*Power(t1,5) - 5*t2 - 74*Power(t2,2) + 
               16*Power(t2,3) + Power(t1,4)*(21 + 25*t2) - 
               Power(t1,3)*(77 + 10*t2 + 63*Power(t2,2)) + 
               t1*(-45 + 100*t2 + 36*Power(t2,2) - 40*Power(t2,3)) + 
               Power(t1,2)*(52 - 110*t2 + 117*Power(t2,2) + 
                  12*Power(t2,3))) + 
            s2*(-4 + 4*Power(t1,6) - 75*t2 + 47*Power(t2,2) + 
               32*Power(t2,3) + 4*Power(t1,5)*(5 + 2*t2) - 
               Power(t1,4)*(67 + 27*t2 + 28*Power(t2,2)) + 
               t1*(51 + 170*t2 - 151*Power(t2,2) - 24*Power(t2,3)) + 
               Power(t1,3)*(101 + 43*t2 + 29*Power(t2,2) + 
                  16*Power(t2,3)) - 
               Power(t1,2)*(105 + 119*t2 - 102*Power(t2,2) + 
                  24*Power(t2,3))) - 
            Power(s1,2)*s2*(Power(s2,4) + 16*Power(t1,4) + 
               Power(t1,3)*(7 - 16*t2) + Power(s2,3)*(21 + 3*t1 - 4*t2) - 
               Power(t1,2)*(143 + 24*t2) - 4*(13 + 28*t2) + 
               t1*(173 + 152*t2) + 
               s2*(-85 - 27*Power(t1,3) + t1*(88 - 40*t2) + 
                  4*Power(t1,2)*(10 + 3*t2)) + 
               Power(s2,2)*(11 + 7*Power(t1,2) + 24*t2 + t1*(-60 + 8*t2))) \
+ s1*(-14 + 34*t1 - 20*Power(t1,2) - 4*Power(t1,3) + 4*Power(t1,4) + 
               Power(s2,5)*(17 - 17*t1 + 2*t2) + 
               4*Power(s2,4)*(-7 + 14*Power(t1,2) + 14*t2 - 
                  2*Power(t2,2) - t1*(7 + 2*t2)) + 
               Power(s2,3)*(-110 - 73*Power(t1,3) + 40*t2 + 
                  24*Power(t2,2) + 2*Power(t1,2)*(5 + 26*t2) + 
                  t1*(173 - 176*t2 + 16*Power(t2,2))) + 
               Power(s2,2)*(46*Power(t1,4) - Power(t1,3)*(17 + 90*t2) + 
                  t1*(169 + 124*t2 - 80*Power(t2,2)) + 
                  3*(-8 - 53*t2 + 8*Power(t2,2)) + 
                  Power(t1,2)*(-174 + 157*t2 + 24*Power(t2,2))) + 
               s2*(71 - 12*Power(t1,5) - 99*t2 - 104*Power(t2,2) + 
                  2*Power(t1,4)*(7 + 22*t2) - 
                  2*Power(t1,3)*(-9 + 11*t2 + 16*Power(t2,2)) + 
                  Power(t1,2)*(48 - 245*t2 + 24*Power(t2,2)) + 
                  t1*(-139 + 324*t2 + 112*Power(t2,2)))))))/
     ((-1 + s2)*s2*Power(-s + s2 - t1,2)*(1 - s + s2 - t1)*
       Power(-1 + s + t1,2)*(-1 + s1 + t1 - t2)*(s - s1 + t2)) - 
    (8*(-24 + 4*Power(s,4)*(-1 + s2) + 21*s2 + 
         8*Power(s1,3)*(-2 + s2)*s2 + 19*Power(s2,2) + 6*Power(s2,3) - 
         2*Power(s2,4) + 75*t1 - 15*s2*t1 - 26*Power(s2,2)*t1 + 
         2*Power(s2,4)*t1 - 52*Power(t1,2) - 13*s2*Power(t1,2) + 
         3*Power(s2,2)*Power(t1,2) - 6*Power(s2,3)*Power(t1,2) + 
         Power(t1,3) + 7*s2*Power(t1,3) + 4*Power(s2,2)*Power(t1,3) - 
         32*t2 - 39*s2*t2 + 16*Power(s2,2)*t2 + 15*Power(s2,3)*t2 + 
         4*Power(s2,4)*t2 - 2*Power(s2,5)*t2 + 15*t1*t2 + 93*s2*t1*t2 + 
         13*Power(s2,2)*t1*t2 - 3*Power(s2,3)*t1*t2 + 
         6*Power(s2,4)*t1*t2 + 11*Power(t1,2)*t2 - 24*s2*Power(t1,2)*t2 - 
         5*Power(s2,2)*Power(t1,2)*t2 - 4*Power(s2,3)*Power(t1,2)*t2 + 
         4*Power(t1,3)*t2 - 4*s2*Power(t1,3)*t2 + 16*Power(t2,2) - 
         50*s2*Power(t2,2) - 12*Power(s2,2)*Power(t2,2) - 
         4*Power(s2,3)*Power(t2,2) - 2*Power(s2,4)*Power(t2,2) - 
         6*t1*Power(t2,2) - 4*s2*t1*Power(t2,2) + 
         20*Power(s2,2)*t1*Power(t2,2) - 2*Power(s2,3)*t1*Power(t2,2) - 
         8*Power(t1,2)*Power(t2,2) + 
         4*Power(s2,2)*Power(t1,2)*Power(t2,2) + 16*s2*Power(t2,3) - 
         16*Power(s2,2)*Power(t2,3) + 4*Power(s2,3)*Power(t2,3) + 
         8*s2*t1*Power(t2,3) - 4*Power(s2,2)*t1*Power(t2,3) + 
         2*Power(s1,2)*(8 + Power(s2,4) - 8*t1 + Power(t1,2) - 
            s2*(20 + Power(t1,2) - 4*t1*(-1 + t2) - 24*t2) + 
            Power(s2,2)*(-9 + Power(t1,2) - 2*t1*(-6 + t2) - 16*t2) + 
            Power(s2,3)*(-3 - 2*t1 + 2*t2)) + 
         Power(s,3)*(-4 - 9*t1 + 2*Power(s2,2)*(2 + t1 - t2) - 7*t2 + 
            s2*(4 - 4*s1 + t1 + 15*t2)) + 
         Power(s,2)*(-11 + 4*t1 - 8*Power(t1,2) - 
            2*Power(s2,3)*(8 + 2*t1 - t2) - 6*t2 - 6*t1*t2 - 
            10*Power(t2,2) + Power(s2,2)*
             (-4 + 21*t1 + 2*Power(t1,2) - 10*t2 + 4*t1*t2 - 
               6*Power(t2,2)) + 
            s2*(41 - 17*t1 - 8*Power(t1,2) + 20*t2 + 14*t1*t2 + 
               18*Power(t2,2)) + 
            s1*(16 + 2*Power(s2,3) - 5*t1 + s2*(-19 + t1 - 18*t2) + 
               10*t2 + Power(s2,2)*(-9 - 4*t1 + 6*t2))) + 
         s1*(2*Power(s2,5) - Power(t1,3) - 2*Power(s2,4)*(3 + 2*t1) - 
            32*(-1 + t2) + Power(t1,2)*(-11 + 6*t2) + 
            2*t1*(-9 + 11*t2) + 
            Power(s2,3)*(-21 + 2*Power(t1,2) + 10*t2 - 8*Power(t2,2) + 
               t1*(11 + 6*t2)) + 
            s2*(42 + 5*Power(t1,3) + 90*t2 - 48*Power(t2,2) + 
               Power(t1,2)*(21 + 2*t2) - 
               2*t1*(47 - 6*t2 + 8*Power(t2,2))) - 
            Power(s2,2)*(Power(t1,2)*(2 + 6*t2) + 
               t1*(7 + 44*t2 - 8*Power(t2,2)) - 
               5*(-3 + 6*t2 + 8*Power(t2,2)))) + 
         s*(43 - 79*t1 + 9*Power(t1,2) - 3*Power(t1,3) + 47*t2 + 
            5*t1*t2 + 5*Power(t1,2)*t2 - 6*Power(t2,2) - 
            18*t1*Power(t2,2) + 2*Power(s2,4)*(4 + t1 + t2) - 
            Power(s2,3)*(-14 + 2*Power(t1,2) + 9*t2 - 8*Power(t2,2) + 
               10*t1*(2 + t2)) - 
            2*Power(s1,2)*(8 + Power(s2,3) - t1 + s2*(-21 + t1 - 4*t2) + 
               Power(s2,2)*(1 - t1 + 2*t2)) + 
            Power(s2,2)*(-11 - 20*t2 - 22*Power(t2,2) - 4*Power(t2,3) + 
               Power(t1,2)*(11 + 6*t2) + t1*(20 + 21*t2 - 2*Power(t2,2))) \
+ s2*(-64 - 5*Power(t1,3) + Power(t1,2)*(4 - 5*t2) - 2*t2 + 
               46*Power(t2,2) + 8*Power(t2,3) + 
               t1*(73 - 56*t2 + 18*Power(t2,2))) + 
            s1*(-50 - 4*Power(s2,4) + 5*t1 - 6*Power(t1,2) + 
               Power(s2,3)*(19 + 8*t1 - 6*t2) + 22*t2 + 16*t1*t2 + 
               s2*(-9 + 10*Power(t1,2) + t1*(54 - 16*t2) - 88*t2 - 
                  16*Power(t2,2)) + 
               Power(s2,2)*(-39*t1 - 4*Power(t1,2) + 
                  8*(4 + 3*t2 + Power(t2,2))))))*
       B1(1 - s1 - t1 + t2,s2,1 - s + s2 - t1))/
     (Power(s - s2 + t1,2)*(s - s1 + t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)) - 
    (16*(2*Power(s,5)*s2*(-2 + s2 + Power(s2,2) - t1 - s2*t1 + 2*s2*t2) + 
         Power(s,4)*(2 - Power(s2,4) + t1 - 
            Power(s2,3)*(9 + 2*s1 - 13*t1 + 4*t2) + 
            s2*(3 - 15*t1 - 8*Power(t1,2) + 2*s1*(2 + t1) - 2*t2 - 
               2*t1*t2) + Power(s2,2)*
             (13 + 5*t1 - 8*Power(t1,2) + 2*s1*(-5 + t1 - 2*t2) + 2*t2 + 
               14*t1*t2 + 4*Power(t2,2))) + 
         Power(s,3)*(-10 - 4*Power(s2,5) + 4*t1 + 4*Power(t1,2) + 
            Power(s2,4)*(6 - 17*t1 + 5*t2) + 
            s2*(42 + t1 - 12*Power(t1,3) + 
               8*s1*(-3 + t1 + Power(t1,2)) + 17*t2 - 
               4*Power(t1,2)*(7 + 2*t2)) - 
            Power(s2,3)*(-26 - 28*Power(t1,2) + 4*s1*(-2 + 3*t1 - t2) + 
               t2 + 4*Power(t2,2) + t1*(17 + 8*t2)) + 
            Power(s2,2)*(-76 + 8*Power(s1,2) - 12*Power(t1,3) - 21*t2 - 
               4*Power(t2,2) + 4*Power(t1,2)*(1 + 4*t2) + 
               t1*(61 + 8*t2 + 16*Power(t2,2)) + 
               4*s1*(6 + 2*Power(t1,2) - t2 - t1*(7 + 4*t2)))) + 
         s*(-14 + 34*t1 - 20*Power(t1,2) - 4*Power(t1,3) + 
            4*Power(t1,4) + Power(s2,6)*(4 - s1 - 4*t1 + t2) + 
            Power(s2,5)*(-1 + 17*Power(t1,2) + 16*t2 - 4*Power(t2,2) + 
               s1*(-14 - 9*t1 + 4*t2) + t1*(-16 + 7*t2)) + 
            s2*(87 - 2*Power(t1,5) + 
               s1*(-48 + 99*t1 - 50*Power(t1,2) - 8*Power(t1,3) + 
                  8*Power(t1,4)) + 45*t2 - 8*Power(t1,4)*(3 + t2) + 
               Power(t1,3)*(1 + 16*t2) + Power(t1,2)*(183 + 29*t2) - 
               t1*(245 + 83*t2)) + 
            Power(s2,4)*(8*Power(s1,2) - 21*Power(t1,3) + 
               s1*(8 + 22*Power(t1,2) + t1*(36 - 8*t2) - 20*t2) + 
               Power(t1,2)*(4 - 15*t2) + 3*(4 - 3*t2 + 4*Power(t2,2)) + 
               t1*(5 - 42*t2 + 8*Power(t2,2))) - 
            Power(s2,3)*(20 + 16*Power(s1,2) - 10*Power(t1,4) + 
               Power(t1,3)*(1 - 8*t2) + 29*t2 + 24*Power(t2,2) + 
               t1*(17 - 68*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(-28 - 11*t2 + 12*Power(t2,2)) + 
               2*s1*(10*Power(t1,3) - 5*(3 + 4*t2) + t1*(35 + 4*t2) - 
                  Power(t1,2)*(1 + 6*t2))) + 
            Power(s2,2)*(-2*Power(t1,5) + 
               8*Power(s1,2)*(4 - 6*t1 + 3*Power(t1,2)) + 
               Power(t1,4)*(2 - 4*t2) + 
               t1*(215 + 2*t2 - 16*Power(t2,2)) + 
               4*(-13 + t2 + 6*Power(t2,2)) - 
               Power(t1,2)*(220 + 25*t2 + 12*Power(t2,2)) + 
               Power(t1,3)*(57 + 8*t2 + 16*Power(t2,2)) + 
               s1*(-15 + 8*Power(t1,4) + Power(t1,2)*(2 - 12*t2) - 
                  56*t2 - 4*Power(t1,3)*(1 + 4*t2) + 8*t1*(3 + 8*t2)))) + 
         Power(s,2)*(18 + 3*Power(s2,6) - 25*t1 + 6*Power(t1,3) + 
            Power(s2,5)*(-3 + 3*s1 + 10*t1 - 6*t2) + 
            Power(s2,4)*(-13 + 2*t1 - 34*Power(t1,2) + 
               s1*(5 + 11*t1 - 4*t2) - 6*t2 + 4*Power(t2,2)) + 
            s2*(-8*Power(t1,4) + s1*(52 - 61*t1 + 12*Power(t1,3)) + 
               4*Power(t1,2)*(-1 + 3*t2) - 2*Power(t1,3)*(17 + 6*t2) - 
               2*(51 + 22*t2) + t1*(157 + 40*t2)) + 
            Power(s2,2)*(104 + 24*Power(s1,2)*(-1 + t1) - 
               8*Power(t1,4) + 18*t2 - 8*Power(t2,2) + 
               Power(t1,3)*(2 + 4*t2) + 
               2*Power(t1,2)*(47 + 6*t2 + 12*Power(t2,2)) - 
               t1*(229 + 44*t2 + 12*Power(t2,2)) + 
               s1*(-9 + 12*Power(t1,3) + t1*(37 - 12*t2) + 32*t2 - 
                  24*Power(t1,2)*(1 + t2))) + 
            Power(s2,3)*(-7 - 8*Power(t1,2) + 26*Power(t1,3) + 22*t2 + 
               4*Power(t2,2) + t1*(53 + 4*t2 - 12*Power(t2,2)) + 
               s1*(-27 - 24*Power(t1,2) - 4*t2 + t1*(13 + 12*t2)))) + 
         (-1 + t1)*(-4 + 10*t1 - 6*Power(t1,2) - Power(t1,3) + 
            Power(t1,4) + Power(s2,6)*(1 + 5*s1 - t1 - 5*t2) + 
            Power(s2,5)*(4 + 3*Power(t1,2) + 22*t2 - 4*Power(t2,2) + 
               s1*(-23 - 12*t1 + 4*t2) + t1*(-7 + 13*t2)) + 
            s2*(26 + s1*(-16 + 32*t1 - 15*Power(t1,2) - 2*Power(t1,3) + 
                  2*Power(t1,4)) + 4*Power(t1,3)*(-1 + t2) + 16*t2 - 
               Power(t1,4)*(7 + 2*t2) + 2*Power(t1,2)*(32 + 5*t2) - 
               t1*(79 + 29*t2)) + 
            Power(s2,4)*(8*Power(s1,2) - 3*Power(t1,3) + 
               s1*(13 + 11*Power(t1,2) + t1*(42 - 4*t2) - 24*t2) + 
               Power(t1,2)*(5 - 10*t2) + 
               t1*(-6 - 46*t2 + 4*Power(t2,2)) + 
               2*(2 - 5*t2 + 8*Power(t2,2))) + 
            Power(s2,3)*(-16*Power(s1,2) + Power(t1,4) + 
               4*Power(t1,3)*t2 + 2*t1*(9 + 28*t2) + 
               Power(t1,2)*(1 + 10*t2 - 4*Power(t2,2)) - 
               4*(5 + 6*t2 + 6*Power(t2,2)) + 
               s1*(21 - 52*t1 - 6*Power(t1,3) + 40*t2 + 
                  Power(t1,2)*(-9 + 4*t2))) + 
            Power(s2,2)*(-3 + 8*Power(s1,2)*(2 - 2*t1 + Power(t1,2)) + 
               Power(t1,4)*(1 - 2*t2) + 13*t2 + 16*Power(t2,2) - 
               Power(t1,2)*(55 + 2*t2) + 
               t1*(45 - 18*t2 - 8*Power(t2,2)) + 
               4*Power(t1,3)*(3 + Power(t2,2)) + 
               s1*(2*Power(t1,4) - 4*Power(t1,3)*(-1 + t2) - 
                  16*(1 + 2*t2) - Power(t1,2)*(7 + 8*t2) + t1*(26 + 24*t2)\
))))*R1q(s2))/(Power(-1 + s2,2)*s2*Power(-s + s2 - t1,2)*
       Power(-1 + s + t1,3)*(s - s1 + t2)) + 
    (16*(Power(s,7)*(2 + s1 + 3*s2 + 6*t1 - 6*t2) - 
         Power(s,6)*(-9 + 2*Power(s1,2) + 15*Power(s2,2) + 15*t1 - 
            25*Power(t1,2) + s2*(12 + 25*t1 - 35*t2) + 
            12*s1*(2 + s2 + t1 - t2) - 43*t2 + 15*t1*t2 + 10*Power(t2,2)) \
+ Power(s,5)*(-26 + Power(s1,3) + 30*Power(s2,3) + 68*t1 - 
            54*Power(t1,2) + 40*Power(t1,3) + 
            Power(s2,2)*(26 + 44*t1 - 81*t2) + 
            Power(s1,2)*(28 + 13*s2 + 6*t1 - 8*t2) - 82*t2 + 126*t1*t2 - 
            2*Power(t1,2)*t2 + 54*Power(t2,2) - 32*t1*Power(t2,2) - 
            6*Power(t2,3) + s1*
             (50 + 39*Power(s2,2) - 38*t1 - 60*Power(t1,2) + 
               s2*(130 + 43*t1 - 62*t2) - 82*t2 + 26*t1*t2 + 
               13*Power(t2,2)) + 
            s2*(-77 + 86*t1 - 111*Power(t1,2) - 198*t2 + 59*t1*t2 + 
               49*Power(t2,2))) - 
         Power(s,4)*(38 + 30*Power(s2,4) - 6*t1 - 72*Power(t1,2) + 
            46*Power(t1,3) - 30*Power(t1,4) + 
            Power(s2,3)*(22 + 46*t1 - 94*t2) + 
            Power(s1,3)*(6 + 4*s2 - 2*t2) + 28*t2 + 138*t1*t2 - 
            150*Power(t1,2)*t2 - 22*Power(t1,3)*t2 + 139*Power(t2,2) - 
            153*t1*Power(t2,2) + 31*Power(t1,2)*Power(t2,2) - 
            23*Power(t2,3) + 19*t1*Power(t2,3) + 2*Power(t2,4) + 
            Power(s1,2)*(115 + 29*Power(s2,2) - 45*Power(t1,2) + 
               s2*(120 + 23*t1 - 37*t2) + 19*t1*(-3 + t2) - 35*t2 + 
               6*Power(t2,2)) + 
            Power(s2,2)*(-172 - 204*Power(t1,2) - 341*t2 + 
               93*Power(t2,2) + t1*(184 + 71*t2)) + 
            s2*(-193 + 155*Power(t1,3) - 286*t2 + 206*Power(t2,2) - 
               29*Power(t2,3) + Power(t1,2)*(-249 + 41*t2) + 
               t1*(409 + 423*t2 - 107*Power(t2,2))) + 
            s1*(56*Power(s2,3) + Power(s2,2)*(249 + 75*t1 - 122*t2) + 
               s2*(198 - 231*Power(t1,2) - 326*t2 + 62*Power(t2,2) + 
                  t1*(-169 + 84*t2)) + 
               2*(-33 + 50*Power(t1,3) - 127*t2 + 26*Power(t2,2) - 
                  3*Power(t2,3) + Power(t1,2)*(-11 + 7*t2) + 
                  t1*(-1 + 105*t2 - 19*Power(t2,2))))) + 
         Power(s,3)*(-4*Power(s1,4) + 15*Power(s2,5) + 
            Power(s2,4)*(34*t1 - 56*t2) + 
            Power(s2,3)*(-153 - 197*Power(t1,2) - 262*t2 + 
               85*Power(t2,2) + 4*t1*(49 + t2)) + 
            Power(s1,3)*(17 + 6*Power(s2,2) - 10*Power(t1,2) + 
               s2*(12 + 5*t1 - 8*t2) + 10*t2 + 4*t1*(-5 + 2*t2)) + 
            Power(s2,2)*(-318 + 232*Power(t1,3) - 333*t2 + 
               282*Power(t2,2) - 49*Power(t2,3) + 
               5*Power(t1,2)*(-76 + 33*t2) + 
               t1*(624 + 466*t2 - 118*Power(t2,2))) + 
            s2*(4 - 94*Power(t1,4) + Power(t1,3)*(172 - 125*t2) + 
               124*t2 + 416*Power(t2,2) - 82*Power(t2,3) + 
               8*Power(t2,4) + 
               Power(t1,2)*(-439 - 346*t2 + 25*Power(t2,2)) + 
               t1*(296 + 301*t2 - 384*Power(t2,2) + 71*Power(t2,3))) + 
            2*(91 + 5*Power(t1,5) + 152*t2 + 115*Power(t2,2) - 
               19*Power(t2,3) + Power(t2,4) + Power(t1,4)*(1 + 9*t2) + 
               Power(t1,3)*(-48 + 75*t2 - 2*Power(t2,2)) - 
               Power(t1,2)*(-211 + t2 - 63*Power(t2,2) + 
                  8*Power(t2,3)) - 
               t1*(256 + 240*t2 + 182*Power(t2,2) - 35*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s1,2)*(29*Power(s2,3) + 
               Power(s2,2)*(176 + 52*t1 - 61*t2) + 
               s2*(380 - 159*Power(t1,2) - 106*t2 + 24*Power(t2,2) + 
                  t1*(-180 + 61*t2)) + 
               2*(40*Power(t1,3) + 2*Power(t1,2)*(-4 + t2) + 
                  t1*(-155 + 55*t2 - 12*Power(t2,2)) - 
                  3*(-39 + 12*t2 + Power(t2,2)))) + 
            s1*(-341 + 39*Power(s2,4) - 75*Power(t1,4) + 
               Power(t1,3)*(32 - 76*t2) + 
               2*Power(s2,3)*(102 + 46*t1 - 57*t2) - 464*t2 + 
               93*Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,2)*(-243 - 110*t2 + 22*Power(t2,2)) + 
               2*t1*(318 + 337*t2 - 80*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s2,2)*(248 - 372*Power(t1,2) - 458*t2 + 
                  104*Power(t2,2) + 6*t1*(-34 + 11*t2)) + 
               s2*(317*Power(t1,3) + 2*Power(t1,2)*(-30 + 67*t2) + 
                  t1*(15 + 564*t2 - 137*Power(t2,2)) - 
                  4*(54 + 199*t2 - 44*Power(t2,2) + 6*Power(t2,3))))) - 
         (-1 + t1)*(4*Power(s1,4)*
             (2*Power(s2,2) - 3*s2*(-1 + t1) + Power(-1 + t1,2)) - 
            Power(s2,6)*(-1 + t1 + 5*t2) + 
            Power(s2,5)*(5 + 4*Power(t1,2) + 13*t2 + 6*Power(t2,2) + 
               t1*(-9 + 22*t2)) + 
            Power(s2,4)*(12 - 6*Power(t1,3) + 
               Power(t1,2)*(19 - 34*t2) + 28*t2 + 11*Power(t2,2) + 
               3*Power(t2,3) - t1*(25 + 60*t2 + 31*Power(t2,2))) + 
            Power(s2,3)*(-37 + 4*Power(t1,4) - 93*t2 - 59*Power(t2,2) + 
               9*Power(t2,3) - 4*Power(t2,4) + 
               Power(t1,3)*(-17 + 20*t2) + 
               t1*(41 - 10*t2 - 49*Power(t2,2)) + 
               Power(t1,2)*(9 + 129*t2 + 52*Power(t2,2))) + 
            (-1 + t1)*(4*Power(t1,5) + 
               Power(t1,4)*(15 - 26*t2 + 3*Power(t2,2)) - 
               Power(t1,3)*(129 + 5*t2 - 15*Power(t2,2) + 
                  5*Power(t2,3)) - 
               4*(-8 - 17*t2 - 8*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4)) + 
               2*t1*(-71 - 104*t2 - 25*Power(t2,2) + 13*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*(219 + 175*t2 - 5*Power(t2,2) - 
                  11*Power(t2,3) + 2*Power(t2,4))) - 
            Power(s2,2)*(123 + Power(t1,5) + Power(t1,4)*(-12 + t2) + 
               263*t2 + 157*Power(t2,2) - 11*Power(t2,3) - 
               2*Power(t2,4) + 
               16*Power(t1,3)*(-3 + 10*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(276 + 63*t2 - 87*Power(t2,2) + 
                  14*Power(t2,3)) - 
               2*t1*(170 + 239*t2 + 69*Power(t2,2) - 16*Power(t2,3) + 
                  5*Power(t2,4))) - 
            s2*(2*Power(t1,5)*(5 + t2) - 
               2*Power(t1,4)*(-24 + 52*t2 + Power(t2,2)) - 
               Power(t1,3)*(367 + 20*t2 - 61*Power(t2,2) + 
                  16*Power(t2,3)) + 
               2*(55 + 114*t2 + 63*Power(t2,2) - 7*Power(t2,3) - 
                  5*Power(t2,4)) + 
               2*t1*(-226 - 343*t2 - 117*Power(t2,2) + 
                  30*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(651 + 581*t2 + 47*Power(t2,2) - 
                  29*Power(t2,3) + 8*Power(t2,4))) + 
            Power(s1,3)*(5*Power(s2,4) + 
               Power(s2,3)*(10 - 19*t1 + 4*t2) + 
               Power(s2,2)*(2 + 27*Power(t1,2) - 26*t2 - 
                  2*t1*(1 + 5*t2)) + 
               2*(-1 + t1)*(4 + 2*Power(t1,3) - 
                  Power(t1,2)*(-5 + t2) + 8*t2 - t1*(12 + 7*t2)) + 
               s2*(-17*Power(t1,3) + 2*Power(t1,2)*(-7 + 4*t2) - 
                  2*(6 + 23*t2) + t1*(44 + 38*t2))) + 
            Power(s1,2)*(10*Power(s2,5) + 
               Power(s2,4)*(7 - 39*t1 - 7*t2) + 
               Power(s2,3)*(-85 + 58*Power(t1,2) - 11*t2 - 
                  12*Power(t2,2) + t1*(-17 + 38*t2)) - 
               (-1 + t1)*(Power(t1,4) + Power(t1,3)*(-17 + 13*t2) + 
                  Power(t1,2)*(-3 + 31*t2 - 6*Power(t2,2)) + 
                  8*(-4 + 3*t2 + 3*Power(t2,2)) - 
                  2*t1*(-28 + 37*t2 + 9*Power(t2,2))) + 
               s2*(-132 + 12*Power(t1,4) + 38*t2 + 66*Power(t2,2) + 
                  Power(t1,3)*(-51 + 50*t2) - 
                  3*Power(t1,2)*(33 - 19*t2 + 8*Power(t2,2)) - 
                  2*t1*(-136 + 74*t2 + 21*Power(t2,2))) + 
               Power(s2,2)*(-181 - 40*Power(t1,3) + 
                  Power(t1,2)*(43 - 68*t2) + 7*t2 + 30*Power(t2,2) + 
                  t1*(210 - 28*t2 + 30*Power(t2,2)))) + 
            s1*(5*Power(s2,6) - Power(s2,5)*(14 + 21*t1 + 16*t2) + 
               Power(s2,4)*(-25 + 35*Power(t1,2) - 18*t2 - 
                  Power(t2,2) + 14*t1*(4 + 5*t2)) - 
               Power(s2,3)*(29*Power(t1,3) + 
                  2*Power(t1,2)*(48 + 55*t2) + 
                  t1*(29 - 66*t2 + 19*Power(t2,2)) - 
                  4*(27 + 36*t2 - 2*Power(t2,2) + 3*Power(t2,3))) - 
               2*(-1 + t1)*(34 + Power(t1,4)*(-11 + t2) + 32*t2 - 
                  12*Power(t2,2) - 8*Power(t2,3) + 
                  Power(t1,3)*(-10 + 16*t2 - 7*Power(t2,2)) + 
                  Power(t1,2)*
                   (96 - t2 - 16*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(-107 - 53*t2 + 38*Power(t2,2) + 5*Power(t2,3))) - 
               s2*(-234 + 2*Power(t1,5) - 258*t2 + 40*Power(t2,2) + 
                  42*Power(t2,3) + 2*Power(t1,4)*(37 + 7*t2) + 
                  Power(t1,3)*(97 - 112*t2 + 49*Power(t2,2)) - 
                  2*t1*(-363 - 253*t2 + 82*Power(t2,2) + 
                     9*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (333 + 73*t2 - 36*Power(t2,2) + 12*Power(t2,3))) + 
               Power(s2,2)*(280 + 12*Power(t1,4) + 338*t2 - 
                  20*Power(t2,2) - 14*Power(t2,3) + 
                  2*Power(t1,3)*(53 + 36*t2) + 
                  Power(t1,2)*(157 - 130*t2 + 55*Power(t2,2)) - 
                  2*t1*(273 + 174*t2 - 31*Power(t2,2) + 15*Power(t2,3))))\
) - s*(4*Power(s1,4)*(2*Power(s2,2) - 6*s2*(-1 + t1) + 
               3*Power(-1 + t1,2)) + Power(s2,6)*(4 - 4*t1 + t2) + 
            Power(s2,5)*(6 + 24*Power(t1,2) - 8*t2 - 6*Power(t2,2) + 
               t1*(-30 + 31*t2)) + 
            Power(s2,4)*(30 - 50*Power(t1,3) + 
               Power(t1,2)*(80 - 121*t2) + 39*t2 - 20*Power(t2,2) + 
               9*Power(t2,3) - 4*t1*(15 - 4*t2 + 3*Power(t2,2))) + 
            Power(s2,3)*(46*Power(t1,4) + Power(t1,3)*(-78 + 151*t2) + 
               2*Power(t1,2)*(56 + 41*t2 + 50*Power(t2,2)) - 
               t1*(98 + 105*t2 + 26*Power(t2,2) + 35*Power(t2,3)) - 
               2*(-9 + 35*t2 + 59*Power(t2,2) - 22*Power(t2,3) + 
                  2*Power(t2,4))) + 
            2*(-1 + t1)*(69 + 140*t2 + 69*Power(t2,2) - 
               13*Power(t2,3) - 5*Power(t2,4) + Power(t1,5)*(3 + t2) + 
               Power(t1,4)*(46 - 53*t2 + 2*Power(t2,2)) - 
               Power(t1,3)*(276 + 20*t2 - 20*Power(t2,2) + 
                  7*Power(t2,3)) + 
               t1*(-300 - 423*t2 - 127*Power(t2,2) + 42*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*(457 + 359*t2 + 31*Power(t2,2) - 
                  20*Power(t2,3) + 4*Power(t2,4))) - 
            Power(s2,2)*(222 + 18*Power(t1,5) + 534*t2 + 
               380*Power(t2,2) - 56*Power(t2,3) + 8*Power(t2,4) + 
               6*Power(t1,4)*(-3 + 11*t2) + 
               4*Power(t1,3)*(-12 + 71*t2 + 33*Power(t2,2)) + 
               Power(t1,2)*(356 + 7*t2 - 154*Power(t2,2) - 
                  21*Power(t2,3)) - 
               2*t1*(265 + 438*t2 + 203*Power(t2,2) - 59*Power(t2,3) + 
                  10*Power(t2,4))) + 
            s2*(-346 + 2*Power(t1,6) - 698*t2 + 2*Power(t1,5)*t2 - 
               414*Power(t2,2) + 42*Power(t2,3) + 12*Power(t2,4) + 
               2*Power(t1,4)*(-98 + 153*t2 + 23*Power(t2,2)) + 
               Power(t1,3)*(1070 + 31*t2 - 154*Power(t2,2) + 
                  19*Power(t2,3)) + 
               2*t1*(680 + 1025*t2 + 432*Power(t2,2) - 
                  85*Power(t2,3) + 6*Power(t2,4)) - 
               2*Power(t1,2)*
                (945 + 846*t2 + 170*Power(t2,2) - 54*Power(t2,3) + 
                  12*Power(t2,4))) - 
            Power(s1,3)*(Power(s2,4) + 
               Power(s2,3)*(-6 + 15*t1 - 4*t2) + 
               s2*(18 + 47*Power(t1,3) + Power(t1,2)*(6 - 24*t2) + 
                  84*t2 - 12*t1*(6 + 5*t2)) + 
               Power(s2,2)*(-9 - 48*Power(t1,2) + 16*t2 + 
                  4*t1*(6 + 5*t2)) - 
               (-1 + t1)*(24 + 15*Power(t1,3) + 
                  Power(t1,2)*(27 - 8*t2) + 46*t2 - 2*t1*(35 + 19*t2))) \
- Power(s1,2)*(2*Power(s2,5) + 11*Power(s2,4)*(4*t1 - t2) + 
               Power(s2,2)*(450 + 190*Power(t1,3) - 38*t2 + 
                  5*Power(t1,2)*(-28 + 15*t2) + 
                  t1*(-544 + 70*t2 - 60*Power(t2,2))) + 
               2*(-1 + t1)*(-72 + 9*Power(t1,4) + 37*t2 + 
                  33*Power(t2,2) + 11*Power(t1,3)*(-3 + 2*t2) + 
                  t1*(142 - 112*t2 - 21*Power(t2,2)) + 
                  Power(t1,2)*(-41 + 47*t2 - 12*Power(t2,2))) + 
               Power(s2,3)*(-156*Power(t1,2) + 5*t1*(10 + t2) + 
                  2*(69 - 16*t2 + 6*Power(t2,2))) + 
               s2*(454 - 98*Power(t1,4) + Power(t1,3)*(186 - 113*t2) - 
                  78*t2 - 108*Power(t2,2) + 
                  8*Power(t1,2)*(58 - 15*t2 + 9*Power(t2,2)) + 
                  2*t1*(-504 + 157*t2 + 18*Power(t2,2)))) + 
            s1*(-Power(s2,6) + Power(s2,5)*(10 - 33*t1 + 8*t2) + 
               Power(s2,4)*(-36 + 132*Power(t1,2) + 20*t2 - 
                  19*Power(t2,2) + t1*(-30 + 56*t2)) + 
               Power(s2,3)*(98 - 193*Power(t1,3) + 
                  Power(t1,2)*(18 - 256*t2) + 256*t2 - 82*Power(t2,2) + 
                  12*Power(t2,3) + t1*(19 + 76*t2 + 55*Power(t2,2))) - 
               s2*(-738 + 36*Power(t1,5) - 868*t2 + 102*Power(t2,2) + 
                  60*Power(t2,3) + 8*Power(t1,4)*(17 + 18*t2) + 
                  Power(t1,3)*(397 - 340*t2 + 85*Power(t2,2)) + 
                  4*t1*(564 + 468*t2 - 103*Power(t2,2) + 
                     3*Power(t2,3)) - 
                  6*Power(t1,2)*
                   (348 + 134*t2 - 37*Power(t2,2) + 12*Power(t2,3))) + 
               (-1 + t1)*(-286 + 2*Power(t1,5) - 282*t2 + 
                  76*Power(t2,2) + 42*Power(t2,3) + 
                  2*Power(t1,4)*(38 + 7*t2) + 
                  Power(t1,3)*(125 - 106*t2 + 43*Power(t2,2)) + 
                  t1*(892 + 538*t2 - 238*Power(t2,2) - 
                     18*Power(t2,3)) - 
                  Power(t1,2)*
                   (817 + 144*t2 - 107*Power(t2,2) + 24*Power(t2,3))) + 
               Power(s2,2)*(595 + 129*Power(t1,4) + 830*t2 - 
                  103*Power(t2,2) + 16*Power(t2,3) + 
                  Power(t1,3)*(60 + 322*t2) + 
                  Power(t1,2)*(335 - 294*t2 + 6*Power(t2,2)) - 
                  2*t1*(552 + 475*t2 - 106*Power(t2,2) + 30*Power(t2,3)))\
)) + Power(s,2)*(-235 - 3*Power(s2,6) + 12*Power(s1,4)*(1 + s2 - t1) + 
            979*t1 - 1444*Power(t1,2) + 879*Power(t1,3) - 
            188*Power(t1,4) + 9*Power(t1,5) + Power(t1,6) - 443*t2 + 
            1289*t1*t2 - 1051*Power(t1,2)*t2 + 35*Power(t1,3)*t2 + 
            165*Power(t1,4)*t2 + Power(t1,5)*t2 - 241*Power(t2,2) + 
            521*t1*Power(t2,2) - 279*Power(t1,2)*Power(t2,2) + 
            4*Power(t1,4)*Power(t2,2) + 39*Power(t2,3) - 
            119*t1*Power(t2,3) + 72*Power(t1,2)*Power(t2,3) + 
            6*Power(t1,3)*Power(t2,3) + 6*Power(t2,4) + 
            6*t1*Power(t2,4) - 12*Power(t1,2)*Power(t2,4) + 
            Power(s2,5)*(10 - 17*t1 + 15*t2) + 
            Power(s2,4)*(54 + 102*Power(t1,2) + 73*t2 - 37*Power(t2,2) + 
               t1*(-111 + 49*t2)) + 
            Power(s2,3)*(176 - 163*Power(t1,3) + 
               Power(t1,2)*(252 - 221*t2) + 155*t2 - 156*Power(t2,2) + 
               35*Power(t2,3) + t1*(-329 - 162*t2 + 37*Power(t2,2))) + 
            Power(s2,2)*(104*Power(t1,4) + 5*Power(t1,3)*(-35 + 44*t2) + 
               Power(t1,2)*(421 + 250*t2 + 75*Power(t2,2)) - 
               t1*(349 + 174*t2 - 247*Power(t2,2) + 84*Power(t2,3)) - 
               2*(-18 + 99*t2 + 203*Power(t2,2) - 50*Power(t2,3) + 
                  5*Power(t2,4))) - 
            s2*(347 + 24*Power(t1,5) + 717*t2 + 547*Power(t2,2) - 
               81*Power(t2,3) + 6*Power(t2,4) + 
               Power(t1,4)*(-11 + 64*t2) + 
               Power(t1,3)*(-51 + 321*t2 + 77*Power(t2,2)) + 
               Power(t1,2)*(550 + 100*t2 + 87*Power(t2,2) - 
                  39*Power(t2,3)) - 
               t1*(850 + 1199*t2 + 770*Power(t2,2) - 177*Power(t2,3) + 
                  24*Power(t2,4))) - 
            Power(s1,3)*(28 + 4*Power(s2,3) + 20*Power(t1,3) + 
               5*Power(s2,2)*(1 + 3*t1 - 2*t2) - 
               12*Power(t1,2)*(-2 + t2) + 42*t2 - 2*t1*(37 + 15*t2) + 
               s2*(14 - 39*Power(t1,2) + 30*t2 + 3*t1*(-7 + 8*t2))) + 
            Power(s1,2)*(-257 - 13*Power(s2,4) + 60*Power(t1,4) + 
               95*t2 + 54*Power(t2,2) + 2*Power(t1,3)*(-55 + 23*t2) + 
               Power(s2,3)*(-94 - 69*t1 + 43*t2) + 
               Power(s2,2)*(-410 + 119*t1 + 231*Power(t1,2) + 110*t2 - 
                  54*t1*t2 - 30*Power(t2,2)) - 
               3*t1*(-185 + 89*t2 + 6*Power(t2,2)) - 
               3*Power(t1,2)*(81 - 40*t2 + 12*Power(t2,2)) + 
               s2*(-595 - 209*Power(t1,3) + Power(t1,2)*(63 - 39*t2) + 
                  109*t2 + 18*Power(t2,2) + 
                  t1*(796 - 219*t2 + 72*Power(t2,2)))) - 
            s1*(12*Power(s2,5) + Power(s2,4)*(56 + 76*t1 - 50*t2) - 
               2*Power(s2,3)*(-61 + 156*Power(t1,2) + 125*t2 - 
                  37*Power(t2,2) + t1*(25 + 16*t2)) + 
               Power(s2,2)*(-277 + 375*Power(t1,3) - 816*t2 + 
                  205*Power(t2,2) - 30*Power(t2,3) + 
                  Power(t1,2)*(-79 + 306*t2) + 
                  t1*(85 + 366*t2 - 153*Power(t2,2))) + 
               2*(-233 + 12*Power(t1,5) - 249*t2 + 53*Power(t2,2) + 
                  15*Power(t2,3) + 4*Power(t1,4)*(7 + 8*t2) + 
                  Power(t1,3)*(134 - 55*t2 + 16*Power(t2,2)) + 
                  t1*(708 + 538*t2 - 156*Power(t2,2) + 3*Power(t2,3)) - 
                  3*Power(t1,2)*
                   (217 + 87*t2 - 28*Power(t2,2) + 6*Power(t2,3))) - 
               s2*(175*Power(t1,4) + Power(t1,3)*(-35 + 286*t2) + 
                  Power(t1,2)*(587 + 24*t2 - 39*Power(t2,2)) + 
                  2*(400 + 571*t2 - 88*Power(t2,2) + 3*Power(t2,3)) - 
                  t1*(1523 + 1566*t2 - 375*Power(t2,2) + 72*Power(t2,3))))\
))*R1q(1 - s + s2 - t1))/
     (Power(-1 + s + t1,3)*(-1 + s - s2 + t1)*Power(s - s2 + t1,2)*
       (s - s1 + t2)*(-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 
         2*s2 - 2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 
         2*s1*t2 - 2*s2*t2 + Power(t2,2))) + 
    (16*(-4 + Power(s,5) + 12*Power(s1,4) - 43*s2 - 27*Power(s2,2) + 
         10*Power(s2,3) - Power(s2,4) + Power(s2,5) + 55*t1 + 101*s2*t1 + 
         2*Power(s2,2)*t1 + Power(s2,3)*t1 - 3*Power(s2,4)*t1 - 
         86*Power(t1,2) - 31*s2*Power(t1,2) - 2*Power(s2,2)*Power(t1,2) + 
         3*Power(s2,3)*Power(t1,2) + 23*Power(t1,3) - 2*s2*Power(t1,3) - 
         Power(s2,2)*Power(t1,3) + 4*Power(t1,4) - 
         Power(s,4)*(1 + 4*s1 + s2 + 4*t1 - 8*t2) - 76*t2 - 121*s2*t2 - 
         20*Power(s2,2)*t2 + 19*Power(s2,3)*t2 - 4*Power(s2,4)*t2 + 
         201*t1*t2 + 109*s2*t1*t2 - 43*Power(s2,2)*t1*t2 + 
         9*Power(s2,3)*t1*t2 - 85*Power(t1,2)*t2 + 46*s2*Power(t1,2)*t2 - 
         3*Power(s2,2)*Power(t1,2)*t2 - 30*Power(t1,3)*t2 - 
         2*s2*Power(t1,3)*t2 - 80*Power(t2,2) - 71*s2*Power(t2,2) + 
         Power(s2,2)*Power(t2,2) + 3*Power(s2,3)*Power(t2,2) + 
         59*t1*Power(t2,2) - 13*s2*t1*Power(t2,2) - 
         15*Power(s2,2)*t1*Power(t2,2) + 40*Power(t1,2)*Power(t2,2) + 
         9*s2*Power(t1,2)*Power(t2,2) + 3*Power(t1,3)*Power(t2,2) + 
         4*Power(t2,3) + s2*Power(t2,3) + 6*Power(s2,2)*Power(t2,3) - 
         33*t1*Power(t2,3) + 3*s2*t1*Power(t2,3) - 
         9*Power(t1,2)*Power(t2,3) + 12*Power(t2,4) - 6*s2*Power(t2,4) + 
         6*t1*Power(t2,4) + Power(s,3)*
          (5*Power(s1,2) - 4*Power(s2,2) - 11*t1 - 6*Power(t1,2) + 
            s1*(6 + 3*s2 + 9*t1 - 20*t2) + s2*(12 + 17*t1 - 16*t2) + 
            t2 - 3*t1*t2 + 15*Power(t2,2)) + 
         Power(s1,3)*(-4 + 3*Power(s2,2) + 3*Power(t1,2) - 
            6*t1*(-5 + t2) - 48*t2 + s2*(2 - 6*t1 + 6*t2)) + 
         Power(s1,2)*(-80 + 7*Power(s2,3) - Power(t1,3) - 
            3*Power(s2,2)*(3 + 5*t1) - 15*Power(t1,2)*(-2 + t2) + 
            12*t2 + 72*Power(t2,2) + t1*(71 - 93*t2 + 18*Power(t2,2)) + 
            s2*(-83 + 9*Power(t1,2) - 3*t2 - 18*Power(t2,2) + 
               t1*(7 + 15*t2))) + 
         Power(s,2)*(29 - 2*Power(s1,3) + 8*Power(s2,3) - 35*t1 - 
            16*Power(t1,2) - Power(t1,3) - 
            Power(s1,2)*(-5 + s2 + 6*t1 - 18*t2) + 36*t2 - 17*t1*t2 - 
            9*Power(t1,2)*t2 + 9*Power(t2,2) + 14*Power(t2,3) + 
            Power(s2,2)*(-22 - 25*t1 + 4*t2) + 
            s2*(12 + 35*t1 + 15*Power(t1,2) + 21*t2 + 19*t1*t2 - 
               25*Power(t2,2)) + 
            s1*(-44 + 11*Power(s2,2) + 34*t1 + 15*Power(t1,2) - 14*t2 + 
               6*t1*t2 - 30*Power(t2,2) + s2*(-36 - 34*t1 + 26*t2))) + 
         s1*(76 + 5*Power(s2,4) - 2*Power(t1,3)*(-15 + t2) + 160*t2 - 
            12*Power(t2,2) - 48*Power(t2,3) + 
            Power(t1,2)*(89 - 70*t2 + 21*Power(t2,2)) - 
            2*t1*(102 + 65*t2 - 48*Power(t2,2) + 9*Power(t2,3)) - 
            2*Power(s2,3)*(6*t1 + 5*(2 + t2)) + 
            Power(s2,2)*(21 + 9*Power(t1,2) + 8*t2 - 9*Power(t2,2) + 
               6*t1*(7 + 5*t2)) - 
            2*s2*(-62 + Power(t1,3) - 77*t2 - 9*Power(t2,3) + 
               Power(t1,2)*(22 + 9*t2) + t1*(57 - 3*t2 + 6*Power(t2,2)))) \
+ s*(-21 - 5*Power(s2,4) + 23*t1 - 8*Power(t1,2) - 14*Power(t1,3) + 
            41*t2 - 61*t1*t2 + 2*Power(t1,3)*t2 + 71*Power(t2,2) - 
            23*t1*Power(t2,2) - 12*Power(t1,2)*Power(t2,2) + 
            15*Power(t2,3) + 5*t1*Power(t2,3) + 6*Power(t2,4) + 
            Power(s2,3)*(12 + 15*t1 + 8*t2) - 
            Power(s2,2)*(22 + 12*Power(t1,2) + 41*t2 - 7*Power(t2,2) + 
               25*t1*(1 + t2)) - Power(s1,3)*(s2 - t1 + 6*(3 + t2)) + 
            Power(s1,2)*(83 - 11*Power(s2,2) - 12*Power(t1,2) + 
               s2*(8 + 23*t1 - 18*t2) + 51*t2 + 18*Power(t2,2) + 
               t1*(-37 + 3*t2)) + 
            s2*(2*Power(t1,3) + 2*Power(t1,2)*(13 + 6*t2) + 
               t1*(43 + 56*t2 + 17*Power(t2,2)) - 
               2*(7 + 12*t2 + 3*Power(t2,2) + 10*Power(t2,3))) + 
            s1*(-15*Power(s2,3) + 2*Power(t1,3) + 
               Power(s2,2)*(50 + 37*t1 + 4*t2) + 
               2*Power(t1,2)*(5 + 12*t2) + 
               t1*(57 + 60*t2 - 9*Power(t2,2)) - 
               2*(22 + 77*t2 + 24*Power(t2,2) + 9*Power(t2,3)) - 
               s2*(-31 + 24*Power(t1,2) + 2*t2 - 39*Power(t2,2) + 
                  8*t1*(9 + 5*t2)))))*R2q(1 - s1 - t1 + t2))/
     (Power(s - s2 + t1,2)*(-1 + s1 + t1 - t2)*(s - s1 + t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) - 
    (8*(-144 - 203*s2 - 8*Power(s1,6)*s2 - 29*Power(s2,2) + 
         41*Power(s2,3) + 3*Power(s2,4) - 6*Power(s2,5) + 2*Power(s2,6) + 
         563*t1 + 601*s2*t1 + 27*Power(s2,2)*t1 - 81*Power(s2,3)*t1 + 
         12*Power(s2,4)*t1 - 2*Power(s2,6)*t1 - 772*Power(t1,2) - 
         581*s2*Power(t1,2) + 15*Power(s2,2)*Power(t1,2) + 
         31*Power(s2,3)*Power(t1,2) - 11*Power(s2,4)*Power(t1,2) + 
         6*Power(s2,5)*Power(t1,2) + 401*Power(t1,3) + 
         183*s2*Power(t1,3) - 13*Power(s2,2)*Power(t1,3) + 
         9*Power(s2,3)*Power(t1,3) - 4*Power(s2,4)*Power(t1,3) - 
         24*Power(t1,4) + 8*s2*Power(t1,4) - 24*Power(t1,5) - 
         8*s2*Power(t1,5) + Power(s,6)*(-1 + 2*s2)*(t1 - t2) - 240*t2 - 
         508*s2*t2 - 294*Power(s2,2)*t2 + 19*Power(s2,3)*t2 + 
         48*Power(s2,4)*t2 - 7*Power(s2,5)*t2 - 4*Power(s2,6)*t2 + 
         2*Power(s2,7)*t2 + 612*t1*t2 + 1159*s2*t1*t2 + 
         525*Power(s2,2)*t1*t2 - 86*Power(s2,3)*t1*t2 - 
         21*Power(s2,4)*t1*t2 + 17*Power(s2,5)*t1*t2 - 
         6*Power(s2,6)*t1*t2 - 377*Power(t1,2)*t2 - 
         735*s2*Power(t1,2)*t2 - 188*Power(s2,2)*Power(t1,2)*t2 + 
         41*Power(s2,3)*Power(t1,2)*t2 - 25*Power(s2,4)*Power(t1,2)*t2 + 
         4*Power(s2,5)*Power(t1,2)*t2 - 81*Power(t1,3)*t2 + 
         22*s2*Power(t1,3)*t2 - 45*Power(s2,2)*Power(t1,3)*t2 + 
         8*Power(s2,3)*Power(t1,3)*t2 + 76*Power(t1,4)*t2 + 
         64*s2*Power(t1,4)*t2 + 12*Power(s2,2)*Power(t1,4)*t2 + 
         8*Power(t1,5)*t2 - 24*Power(t2,2) - 364*s2*Power(t2,2) - 
         442*Power(s2,2)*Power(t2,2) - 85*Power(s2,3)*Power(t2,2) + 
         43*Power(s2,4)*Power(t2,2) + 6*Power(s2,5)*Power(t2,2) - 
         4*Power(s2,6)*Power(t2,2) - 124*t1*Power(t2,2) + 
         489*s2*t1*Power(t2,2) + 488*Power(s2,2)*t1*Power(t2,2) + 
         12*Power(s2,3)*t1*Power(t2,2) - 39*Power(s2,4)*t1*Power(t2,2) + 
         16*Power(s2,5)*t1*Power(t2,2) + 257*Power(t1,2)*Power(t2,2) - 
         6*s2*Power(t1,2)*Power(t2,2) - 
         10*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         61*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         8*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         61*Power(t1,3)*Power(t2,2) - 93*s2*Power(t1,3)*Power(t2,2) - 
         52*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         4*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         40*Power(t1,4)*Power(t2,2) - 16*s2*Power(t1,4)*Power(t2,2) + 
         88*Power(t2,3) + 14*s2*Power(t2,3) - 
         154*Power(s2,2)*Power(t2,3) - 71*Power(s2,3)*Power(t2,3) + 
         12*Power(s2,4)*Power(t2,3) - 150*t1*Power(t2,3) - 
         175*s2*t1*Power(t2,3) + 25*Power(s2,2)*t1*Power(t2,3) + 
         3*Power(s2,3)*t1*Power(t2,3) - 20*Power(s2,4)*t1*Power(t2,3) + 
         Power(t1,2)*Power(t2,3) + 105*s2*Power(t1,2)*Power(t2,3) + 
         5*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         12*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         45*Power(t1,3)*Power(t2,3) + 56*s2*Power(t1,3)*Power(t2,3) + 
         8*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         4*Power(t1,4)*Power(t2,3) + 8*Power(t2,4) + 95*s2*Power(t2,4) + 
         31*Power(s2,2)*Power(t2,4) - 18*Power(s2,3)*Power(t2,4) + 
         8*Power(s2,4)*Power(t2,4) + 25*t1*Power(t2,4) - 
         78*s2*t1*Power(t2,4) - 3*Power(s2,2)*t1*Power(t2,4) + 
         12*Power(s2,3)*t1*Power(t2,4) - 17*Power(t1,2)*Power(t2,4) - 
         51*s2*Power(t1,2)*Power(t2,4) - 
         16*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         8*Power(t1,3)*Power(t2,4) - 4*s2*Power(t1,3)*Power(t2,4) - 
         8*Power(t2,5) + 14*s2*Power(t2,5) + 12*Power(s2,2)*Power(t2,5) - 
         10*Power(s2,3)*Power(t2,5) + 2*t1*Power(t2,5) + 
         24*s2*t1*Power(t2,5) + 2*Power(s2,2)*t1*Power(t2,5) + 
         4*Power(t1,2)*Power(t2,5) + 8*s2*Power(t1,2)*Power(t2,5) - 
         8*s2*Power(t2,6) + 4*Power(s2,2)*Power(t2,6) - 
         4*s2*t1*Power(t2,6) - 
         2*Power(s1,5)*(Power(s2,3) - Power(-2 + t1,2) + 
            s2*(4 + Power(t1,2) - 2*t1*(-7 + t2) - 24*t2) + 
            Power(s2,2)*(7 - 2*t1 + 2*t2)) + 
         Power(s,5)*(-7 + 5*t1 - 7*t2 - 5*t1*t2 + 5*Power(t2,2) + 
            Power(s2,2)*(-10*t1 + 8*t2) + 
            s2*(8 + 2*t1 + 2*Power(t1,2) + 3*t2 + 6*t1*t2 - 
               8*Power(t2,2)) + 
            s1*(8 + 2*Power(s2,2) + 6*t1 - 5*t2 + 
               s2*(-11 - 10*t1 + 8*t2))) + 
         Power(s1,4)*(8 - 8*Power(s2,4) + Power(t1,3) + 
            2*Power(s2,3)*(-5 + 8*t1 - t2) - 40*t2 - 
            Power(t1,2)*(19 + 4*t2) + 2*t1*(9 + 17*t2) + 
            Power(s2,2)*(69 - 8*Power(t1,2) + 68*t2 + 20*Power(t2,2) - 
               t1*(41 + 14*t2)) + 
            2*s2*(51 + 23*t2 - 60*Power(t2,2) + 
               Power(t1,2)*(-15 + 8*t2) + 
               t1*(-57 + 68*t2 - 10*Power(t2,2)))) + 
         Power(s,4)*(18 - 34*t1 + 17*Power(t1,2) + Power(t1,3) + 
            10*Power(s2,3)*(2*t1 - t2) + 15*t2 - 31*t1*t2 + 
            Power(t1,2)*t2 + 4*Power(t2,2) - 15*t1*Power(t2,2) + 
            13*Power(t2,3) + Power(s1,2)*
             (-12 - 8*Power(s2,2) - 14*t1 + s2*(27 + 20*t1 - 16*t2) + 
               13*t2) - 2*Power(s2,2)*
             (15 + 4*Power(t1,2) + 13*t2 + 15*t1*t2 - 14*Power(t2,2)) - 
            s1*(9 + 10*Power(s2,3) - 20*t1 - 2*Power(t1,2) - 8*t2 - 
               29*t1*t2 + 26*Power(t2,2) + 
               Power(s2,2)*(-50 - 44*t1 + 20*t2) + 
               s2*(42 + 39*t1 + 10*Power(t1,2) + 17*t2 + 32*t1*t2 - 
                  32*Power(t2,2))) + 
            s2*(16 + 35*t2 - 10*Power(t2,2) - 16*Power(t2,3) + 
               Power(t1,2)*(-1 + 4*t2) + t1*(1 + 36*t2 + 12*Power(t2,2)))\
) + Power(s1,3)*(-88 - 12*Power(s2,5) - 32*t2 + 80*Power(t2,2) + 
            8*Power(s2,4)*(1 + 3*t1 + 2*t2) + Power(t1,3)*(-16 + 5*t2) + 
            t1*(179 - 79*t2 - 56*Power(t2,2)) + 
            Power(t1,2)*(-63 + 74*t2 - 4*Power(t2,2)) + 
            Power(s2,3)*(99 - 12*Power(t1,2) + 48*t2 + 28*Power(t2,2) - 
               5*t1*(11 + 12*t2)) + 
            Power(s2,2)*(121 - 238*t2 - 132*Power(t2,2) - 
               40*Power(t2,3) + Power(t1,2)*(-2 + 40*t2) + 
               t1*(21 + 126*t2 + 16*Power(t2,2))) + 
            s2*(-43 - 401*t2 - 104*Power(t2,2) + 160*Power(t2,3) + 
               Power(t1,3)*(-31 + 4*t2) + 
               Power(t1,2)*(-208 + 141*t2 - 44*Power(t2,2)) + 
               t1*(270 + 420*t2 - 264*Power(t2,2) + 40*Power(t2,3)))) + 
         Power(s1,2)*(-8*Power(s2,6) + 4*Power(t1,4)*(-4 + t2) + 
            2*Power(s2,5)*(9 + 8*t1 + 12*t2) - 
            7*Power(t1,3)*(18 - 11*t2 + 3*Power(t2,2)) + 
            8*(-3 + 33*t2 + 6*Power(t2,2) - 10*Power(t2,3)) + 
            Power(t1,2)*(311 + 127*t2 - 108*Power(t2,2) + 
               16*Power(t2,3)) + 
            t1*(-137 - 508*t2 + 129*Power(t2,2) + 44*Power(t2,3)) - 
            Power(s2,4)*(-53 + 8*Power(t1,2) + 4*t2 + t1*(57 + 68*t2)) + 
            Power(s2,3)*(-116 - 269*t2 - 84*Power(t2,2) - 
               52*Power(t2,3) + 4*Power(t1,2)*(10 + 9*t2) + 
               t1*(52 + 113*t2 + 84*Power(t2,2))) + 
            Power(s2,2)*(-458 - 396*t2 + 300*Power(t2,2) + 
               128*Power(t2,3) + 40*Power(t2,4) + 
               Power(t1,3)*(-33 + 8*t2) + 
               Power(t1,2)*(-46 + 9*t2 - 72*Power(t2,2)) - 
               t1*(-533 + 17*t2 + 132*Power(t2,2) + 4*Power(t2,3))) + 
            s2*(-351 - 8*Power(t1,4) + 100*t2 + 591*Power(t2,2) + 
               116*Power(t2,3) - 120*Power(t2,4) + 
               Power(t1,3)*(-131 + 118*t2 - 12*Power(t2,2)) + 
               Power(t1,2)*(45 + 521*t2 - 243*Power(t2,2) + 
                  56*Power(t2,3)) + 
               t1*(451 - 715*t2 - 576*Power(t2,2) + 256*Power(t2,3) - 
                  40*Power(t2,4)))) - 
         s1*(2*Power(s2,7) + 8*Power(t1,5) - 
            2*Power(s2,6)*(3 + 2*t1 + 6*t2) + 
            8*Power(t1,4)*(10 - 7*t2 + Power(t2,2)) - 
            Power(t1,3)*(84 + 187*t2 - 106*Power(t2,2) + 
               23*Power(t2,3)) - 
            8*(30 + 6*t2 - 33*Power(t2,2) - 4*Power(t2,3) + 
               5*Power(t2,4)) + 
            Power(t1,2)*(-381 + 568*t2 + 65*Power(t2,2) - 
               70*Power(t2,3) + 14*Power(t2,4)) + 
            t1*(615 - 261*t2 - 479*Power(t2,2) + 93*Power(t2,3) + 
               16*Power(t2,4)) + 
            Power(s2,5)*(-5 + 2*Power(t1,2) + 24*t2 + 12*Power(t2,2) + 
               t1*(17 + 32*t2)) + 
            Power(s2,4)*(59 + 96*t2 + 16*Power(t2,2) + 16*Power(t2,3) - 
               16*Power(t1,2)*(1 + t2) - 
               t1*(41 + 96*t2 + 64*Power(t2,2))) + 
            Power(s2,3)*(12 + Power(t1,3)*(5 - 4*t2) - 201*t2 - 
               241*Power(t2,2) - 64*Power(t2,3) - 38*Power(t2,4) + 
               Power(t1,2)*(48 + 101*t2 + 36*Power(t2,2)) + 
               t1*(-83 + 64*t2 + 61*Power(t2,2) + 52*Power(t2,3))) + 
            Power(s2,2)*(-311 + 8*Power(t1,4) - 900*t2 - 
               429*Power(t2,2) + 162*Power(t2,3) + 62*Power(t2,4) + 
               20*Power(t2,5) + 
               Power(t1,3)*(-18 - 85*t2 + 16*Power(t2,2)) - 
               Power(t1,2)*(249 + 56*t2 - 12*Power(t2,2) + 
                  56*Power(t2,3)) + 
               t1*(580 + 1021*t2 + 29*Power(t2,2) - 50*Power(t2,3) + 
                  4*Power(t2,4))) + 
            s2*(-511 - 24*Power(t1,4)*(-2 + t2) - 715*t2 + 
               71*Power(t2,2) + 387*Power(t2,3) + 64*Power(t2,4) - 
               48*Power(t2,5) + 
               Power(t1,3)*(65 - 224*t2 + 143*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,2)*(-780 + 39*t2 + 418*Power(t2,2) - 
                  183*Power(t2,3) + 34*Power(t2,4)) - 
               4*t1*(-295 - 235*t2 + 155*Power(t2,2) + 87*Power(t2,3) - 
                  31*Power(t2,4) + 5*Power(t2,5)))) + 
         Power(s,3)*(12 + 9*t1 - 20*Power(s2,4)*t1 - 41*Power(t1,2) + 
            28*Power(t1,3) + Power(s1,3)*
             (12*Power(s2,2) + 16*t1 - 5*s2*(3 + 4*t1 - 4*t2) - 15*t2) + 
            55*t2 + 8*t1*t2 - 75*Power(t1,2)*t2 + 11*Power(t1,3)*t2 + 
            27*Power(t2,2) + 28*t1*Power(t2,2) - 
            17*Power(t1,2)*Power(t2,2) - Power(t2,3) - 
            9*t1*Power(t2,3) + 15*Power(t2,4) + 
            2*Power(s2,3)*(20 + 6*Power(t1,2) + 27*t2 + 30*t1*t2 - 
               16*Power(t2,2)) + 
            Power(s2,2)*(20 + Power(t1,2)*(1 - 16*t2) - 32*t2 - 
               2*Power(t2,2) + 48*Power(t2,3) - 
               t1*(57 + 103*t2 + 52*Power(t2,2))) - 
            s2*(111 + 181*t2 + 73*Power(t2,2) + 47*Power(t2,3) + 
               20*Power(t2,4) + Power(t1,3)*(1 + 4*t2) + 
               Power(t1,2)*(65 - 21*t2 - 8*Power(t2,2)) - 
               t1*(173 + 202*t2 + 77*Power(t2,2) + 16*Power(t2,3))) + 
            Power(s1,2)*(65 + 32*Power(s2,3) - 8*Power(t1,2) - t2 + 
               45*Power(t2,2) + Power(s2,2)*(-95 - 76*t1 + 24*t2) - 
               t1*(38 + 41*t2) + 
               s2*(-37 + 20*Power(t1,2) - 17*t2 - 60*Power(t2,2) + 
                  t1*(101 + 56*t2))) + 
            s1*(-77 + 20*Power(s2,4) - 4*Power(t1,3) - 
               2*Power(s2,3)*(45 + 38*t1) - 92*t2 + 2*Power(t2,2) - 
               45*Power(t2,3) + 5*Power(t1,2)*(4 + 5*t2) + 
               2*t1*(27 + 5*t2 + 17*Power(t2,2)) + 
               Power(s2,2)*(49 + 32*Power(t1,2) + 97*t2 - 
                  84*Power(t2,2) + 2*t1*(53 + 64*t2)) + 
               s2*(175 + 110*t2 + 79*Power(t2,2) + 60*Power(t2,3) - 
                  Power(t1,2)*(15 + 28*t2) - 
                  2*t1*(97 + 89*t2 + 26*Power(t2,2))))) + 
         Power(s,2)*(-202 + 400*t1 - 193*Power(t1,2) - 22*Power(t1,3) + 
            24*Power(t1,4) - 388*t2 + 429*t1*t2 + 18*Power(t1,2)*t2 - 
            75*Power(t1,3)*t2 + 4*Power(t1,4)*t2 - 152*Power(t2,2) + 
            12*t1*Power(t2,2) + 6*Power(t1,2)*Power(t2,2) + 
            11*Power(t1,3)*Power(t2,2) + 18*Power(t2,3) + 
            35*t1*Power(t2,3) - 33*Power(t1,2)*Power(t2,3) - 
            10*Power(t2,4) + 12*t1*Power(t2,4) + 6*Power(t2,5) + 
            10*Power(s2,5)*(t1 + t2) + 
            Power(s1,4)*(-8*Power(s2,2) - 9*t1 + 
               s2*(-15 + 10*t1 - 14*t2) + 6*(2 + t2)) - 
            Power(s2,4)*(20 + 8*Power(t1,2) + 51*t2 - 8*Power(t2,2) + 
               t1*(5 + 60*t2)) + 
            Power(s2,3)*(-66 - 20*t2 + 20*Power(t2,2) - 
               48*Power(t2,3) + Power(t1,2)*(7 + 24*t2) + 
               t1*(91 + 135*t2 + 84*Power(t2,2))) + 
            Power(s2,2)*(183 + 393*t2 + 197*Power(t2,2) + 
               71*Power(t2,3) + 48*Power(t2,4) + 
               Power(t1,3)*(-1 + 8*t2) + 
               Power(t1,2)*(40 - 74*t2 - 24*Power(t2,2)) - 
               4*t1*(55 + 81*t2 + 38*Power(t2,2) + 13*Power(t2,3))) + 
            s2*(187 - 8*Power(t1,4) + 219*t2 - 25*Power(t2,2) - 
               95*Power(t2,3) - 52*Power(t2,4) - 14*Power(t2,5) + 
               Power(t1,3)*(-59 + 22*t2 - 12*Power(t2,2)) + 
               Power(t1,2)*(352 + 177*t2 + 42*Power(t2,2) + 
                  20*Power(t2,3)) + 
               t1*(-490 - 447*t2 + 33*Power(t2,2) + 46*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,3)*(-59 - 36*Power(s2,3) + 12*Power(t1,2) + 
               Power(s2,2)*(34 + 64*t1 - 24*t2) - 26*t2 - 
               24*Power(t2,2) + t1*(28 + 15*t2) + 
               s2*(136 - 20*Power(t1,2) + 97*t2 + 56*Power(t2,2) - 
                  t1*(121 + 36*t2))) + 
            Power(s1,2)*(-48*Power(s2,4) + 6*Power(t1,3) - 
               3*Power(t1,2)*(10 + 19*t2) + 
               Power(s2,3)*(127 + 108*t1 + 24*t2) + 
               t1*(44 - 21*t2 + 9*Power(t2,2)) + 
               2*(-78 + 68*t2 + 3*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s2,2)*(-48*Power(t1,2) - t1*(221 + 180*t2) + 
                  3*(61 + t2 + 40*Power(t2,2))) + 
               s2*(-143 - 367*t2 - 201*Power(t2,2) - 84*Power(t2,3) + 
                  Power(t1,2)*(3 + 60*t2) + 
                  t1*(223 + 288*t2 + 48*Power(t2,2)))) + 
            s1*(405 - 20*Power(s2,5) + Power(t1,3)*(8 - 17*t2) + 
               308*t2 - 95*Power(t2,2) + 18*Power(t2,3) - 
               24*Power(t2,4) + 
               Power(t1,2)*(91 + 24*t2 + 78*Power(t2,2)) - 
               t1*(499 + 56*t2 + 42*Power(t2,2) + 27*Power(t2,3)) + 
               8*Power(s2,4)*(8*t1 + 5*(2 + t2)) - 
               Power(s2,3)*(-1 + 36*Power(t1,2) + 147*t2 - 
                  60*Power(t2,2) + 8*t1*(17 + 24*t2)) + 
               s2*(-165 + 168*t2 + 326*Power(t2,2) + 171*Power(t2,3) + 
                  56*Power(t2,4) + Power(t1,3)*(-29 + 12*t2) - 
                  5*Power(t1,2)*(14 + 9*t2 + 12*Power(t2,2)) - 
                  t1*(-311 + 256*t2 + 213*Power(t2,2) + 28*Power(t2,3))) \
+ Power(s2,2)*(Power(t1,2)*(44 + 72*t2) + 
                  t1*(361 + 373*t2 + 168*Power(t2,2)) - 
                  2*(205 + 190*t2 + 54*Power(t2,2) + 68*Power(t2,3))))) + 
         s*(323 - 950*t1 + 877*Power(t1,2) - 216*Power(t1,3) - 
            40*Power(t1,4) + 8*Power(t1,5) + 564*t2 - 1029*t1*t2 + 
            325*Power(t1,2)*t2 + 141*Power(t1,3)*t2 - 16*Power(t1,4)*t2 + 
            140*Power(t2,2) + 57*t1*Power(t2,2) - 
            156*Power(t1,2)*Power(t2,2) - 58*Power(t1,3)*Power(t2,2) + 
            8*Power(t1,4)*Power(t2,2) - 118*Power(t2,3) + 
            99*t1*Power(t2,3) + 81*Power(t1,2)*Power(t2,3) - 
            7*Power(t1,3)*Power(t2,3) - 15*Power(t2,4) - 
            27*t1*Power(t2,4) - 11*Power(t1,2)*Power(t2,4) + 
            2*Power(t2,5) + 10*t1*Power(t2,5) - 
            2*Power(s2,6)*(t1 + 4*t2) + 
            2*Power(s1,5)*(-4 + Power(s2,2) + t1 + s2*(11 - t1 + 2*t2)) + 
            Power(s2,5)*(2*Power(t1,2) + 6*t1*(1 + 5*t2) + 
               t2*(23 + 8*t2)) - 
            Power(s2,4)*(-43 - 31*t2 + 19*Power(t2,2) - 16*Power(t2,3) + 
               Power(t1,2)*(13 + 16*t2) + 
               20*t1*(2 + 4*t2 + 3*Power(t2,2))) + 
            Power(s2,3)*(-93 + Power(t1,3)*(5 - 4*t2) - 275*t2 - 
               171*Power(t2,2) - 49*Power(t2,3) - 36*Power(t2,4) + 
               Power(t1,2)*(19 + 77*t2 + 24*Power(t2,2)) + 
               t1*(69 + 174*t2 + 129*Power(t2,2) + 56*Power(t2,3))) + 
            Power(s2,2)*(-264 + 8*Power(t1,4) - 333*t2 + 
               75*Power(t2,2) + 175*Power(t2,3) + 55*Power(t2,4) + 
               24*Power(t2,5) + 
               Power(t1,3)*(10 - 37*t2 + 16*Power(t2,2)) - 
               Power(t1,2)*(358 + 135*t2 + 94*Power(t2,2) + 
                  32*Power(t2,3)) + 
               t1*(614 + 569*t2 - 77*Power(t2,2) - 36*Power(t2,3) - 
                  18*Power(t2,4))) - 
            s2*(-111 - 458*t2 - 538*Power(t2,2) - 208*Power(t2,3) + 
               5*Power(t2,4) + 26*Power(t2,5) + 4*Power(t2,6) + 
               8*Power(t1,4)*(-1 + 3*t2) + 
               Power(t1,3)*(-105 + 8*t2 - 79*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(175 - 145*t2 - 171*Power(t2,2) + 
                  31*Power(t2,3) - 22*Power(t2,4)) + 
               t1*(57 + 572*t2 + 610*Power(t2,2) + 142*Power(t2,3) - 
                  27*Power(t2,4) + 6*Power(t2,5))) + 
            Power(s1,4)*(-22 + 16*Power(s2,3) - 8*Power(t1,2) + 34*t2 + 
               t1*(-7 + 2*t2) + Power(s2,2)*(25 - 26*t1 + 16*t2) + 
               s2*(-65 + 10*Power(t1,2) - 114*t2 - 20*Power(t2,2) + 
                  t1*(85 + 2*t2))) + 
            Power(s1,3)*(147 + 36*Power(s2,4) - 4*Power(t1,3) + 81*t2 - 
               56*Power(t2,2) - Power(s2,3)*(27 + 68*t1 + 12*t2) + 
               Power(t1,2)*(12 + 35*t2) + 
               t1*(-202 + 48*t2 - 28*Power(t2,2)) + 
               Power(s2,2)*(-243 + 32*Power(t1,2) - 130*t2 - 
                  84*Power(t2,2) + 12*t1*(13 + 8*t2)) + 
               s2*(-134 + Power(t1,2)*(43 - 52*t2) + 200*t2 + 
                  236*Power(t2,2) + 40*Power(t2,3) + 
                  2*t1*(22 - 141*t2 + 6*Power(t2,2)))) + 
            Power(s1,2)*(127 + 32*Power(s2,5) + Power(t1,3)*(-20 + t2) - 
               412*t2 - 111*Power(t2,2) + 44*Power(t2,3) - 
               Power(s2,4)*(77 + 68*t1 + 56*t2) + 
               Power(t1,2)*(-227 + 57*t2 - 57*Power(t2,2)) + 
               t1*(107 + 503*t2 - 102*Power(t2,2) + 52*Power(t2,3)) + 
               Power(s2,3)*(-187 + 36*Power(t1,2) + 5*t2 - 
                  60*Power(t2,2) + t1*(191 + 192*t2)) + 
               Power(s2,2)*(186 + 661*t2 + 240*Power(t2,2) + 
                  136*Power(t2,3) - Power(t1,2)*(43 + 96*t2) - 
                  t1*(241 + 348*t2 + 132*Power(t2,2))) - 
               s2*(-558 - 476*t2 + 210*Power(t2,2) + 244*Power(t2,3) + 
                  40*Power(t2,4) + Power(t1,3)*(-61 + 12*t2) + 
                  Power(t1,2)*(-263 + 117*t2 - 96*Power(t2,2)) + 
                  t1*(698 + 230*t2 - 336*Power(t2,2) + 28*Power(t2,3)))) \
+ s1*(-567 + 10*Power(s2,6) - 267*t2 + 383*Power(t2,2) + 67*Power(t2,3) - 
               16*Power(t2,4) - 8*Power(t1,4)*(1 + t2) - 
               Power(s2,5)*(35 + 26*t1 + 40*t2) + 
               2*Power(t1,3)*(-46 + 39*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-370 + 383*t2 - 150*Power(t2,2) + 
                  41*Power(t2,3)) - 
               2*t1*(-525 + 82*t2 + 200*Power(t2,2) - 44*Power(t2,3) + 
                  19*Power(t2,4)) + 
               Power(s2,4)*(-21 + 16*Power(t1,2) + 96*t2 + 
                  4*Power(t2,2) + 16*t1*(5 + 8*t2)) + 
               Power(s2,3)*(303 + 358*t2 + 71*Power(t2,2) + 
                  92*Power(t2,3) - Power(t1,2)*(47 + 60*t2) - 
                  4*t1*(57 + 80*t2 + 45*Power(t2,2))) + 
               Power(s2,2)*(294 + Power(t1,3)*(34 - 16*t2) - 261*t2 - 
                  593*Power(t2,2) - 190*Power(t2,3) - 94*Power(t2,4) + 
                  Power(t1,2)*(90 + 137*t2 + 96*Power(t2,2)) + 
                  t1*(-492 + 318*t2 + 228*Power(t2,2) + 80*Power(t2,3))) \
+ s2*(16*Power(t1,4) + 2*Power(t1,3)*(55 - 70*t2 + 12*Power(t2,2)) - 
                  Power(t1,2)*
                   (325 + 434*t2 - 105*Power(t2,2) + 76*Power(t2,3)) + 
                  2*t1*(350 + 654*t2 + 164*Power(t2,2) - 
                     83*Power(t2,3) + 11*Power(t2,4)) + 
                  2*(-246 - 548*t2 - 275*Power(t2,2) + 40*Power(t2,3) + 
                     63*Power(t2,4) + 10*Power(t2,5))))))*
       T2q(1 - s + s2 - t1,1 - s1 - t1 + t2))/
     (Power(s - s2 + t1,2)*(-1 + s1 + t1 - t2)*(s - s1 + t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) + 
    (8*(Power(s,6)*(-1 + 2*s2)*(t1 - t2) + 
         Power(s,5)*(-11 + 11*t1 + Power(t1,2) - 13*t2 - 7*t1*t2 + 
            6*Power(t2,2) + Power(s2,2)*(4 - 8*t1 + 6*t2) + 
            s2*(8 - 9*t1 + 4*Power(t1,2) + 14*t2 + 2*t1*t2 - 
               6*Power(t2,2)) + 
            s1*(12 - 2*Power(s2,2) + 7*t1 - 6*t2 + 
               s2*(-11 - 8*t1 + 6*t2))) + 
         Power(s,4)*(48 - 79*t1 + 19*Power(t1,2) + 14*Power(t1,3) + 
            2*Power(s2,3)*(-4 + 5*t1 - 3*t2) + 40*t2 - t1*t2 - 
            34*Power(t1,2)*t2 - 28*Power(t2,2) + 16*t1*Power(t2,2) + 
            4*Power(t2,3) + 2*Power(s1,2)*
             (-4 + Power(s2,2) - 3*t1 + s2*(9 + 5*t1 - 4*t2) + 2*t2) - 
            Power(s2,2)*(38 + 10*Power(t1,2) + 37*t2 - 16*Power(t2,2) + 
               2*t1*(-23 + 8*t2)) + 
            s1*(-46 + 4*Power(s2,3) + 16*t1 + 23*Power(t1,2) + 
               Power(s2,2)*(33 + 22*t1 - 18*t2) + 36*t2 - 10*t1*t2 - 
               8*Power(t2,2) - 
               2*s2*(6 + 10*t1 + 10*Power(t1,2) + 17*t2 + 3*t1*t2 - 
                  8*Power(t2,2))) + 
            s2*(7 + 5*t2 + 16*Power(t2,2) - 8*Power(t2,3) + 
               4*Power(t1,2)*(-7 + 3*t2) + 
               t1*(19 + 37*t2 - 4*Power(t2,2)))) - 
         2*Power(s,3)*(49 - 110*t1 + 68*Power(t1,2) + 6*Power(t1,3) - 
            13*Power(t1,4) + 2*Power(s1,3)*(2 + s2*(t1 - t2)) + 
            Power(s2,4)*(-2 + 2*t1 - t2) + 26*t2 - 9*t1*t2 - 
            37*Power(t1,2)*t2 + 23*Power(t1,3)*t2 - 36*Power(t2,2) + 
            37*t1*Power(t2,2) - 2*Power(t1,2)*Power(t2,2) + 
            4*Power(t2,3) - 8*t1*Power(t2,3) - 
            Power(s2,3)*(15 + 3*Power(t1,2) + 8*t2 - 5*Power(t2,2) + 
               2*t1*(-9 + 5*t2)) + 
            Power(s1,2)*(Power(s2,3) + 12*Power(t1,2) + 
               Power(s2,2)*(28 + 13*t1 - 10*t2) + t1*(13 - 8*t2) - 
               4*(7 + t2) + s2*
                (29 - 14*Power(t1,2) + 4*t1*(-6 + t2) - 6*t2 + 
                  6*Power(t2,2))) + 
            Power(s2,2)*(-3*Power(t1,3) + 3*Power(t1,2)*(-11 + 6*t2) + 
               t1*(61 + 5*t2 + 5*Power(t2,2)) - 
               5*(5 + t2 - 6*Power(t2,2) + 2*Power(t2,3))) + 
            s2*(30 + 2*Power(t1,4) + Power(t1,3)*(23 - 2*t2) + 37*t2 + 
               9*Power(t2,2) - 6*Power(t2,3) + 2*Power(t2,4) - 
               2*Power(t1,2)*(7 + 17*t2 + 5*Power(t2,2)) + 
               t1*(-41 - 3*t2 - 8*Power(t2,2) + 8*Power(t2,3))) + 
            s1*(-31 + Power(s2,4) - 11*Power(t1,3) + 
               2*Power(s2,3)*(4 + 5*t1 - 3*t2) - 
               10*Power(t1,2)*(-1 + t2) + 64*t2 - 4*Power(t2,2) + 
               t1*(29 - 50*t2 + 16*Power(t2,2)) + 
               Power(s2,2)*(1 + t1 - 20*Power(t1,2) - 58*t2 - 
                  18*t1*t2 + 20*Power(t2,2)) + 
               s2*(6*Power(t1,3) + 3*Power(t1,2)*(3 + 8*t2) + 
                  t1*(37 + 32*t2 - 14*Power(t2,2)) - 
                  2*(25 + 19*t2 - 6*Power(t2,2) + 3*Power(t2,3))))) - 
         Power(s,2)*(-116 + 8*Power(s1,4)*s2 + 313*t1 - 
            276*Power(t1,2) + 58*Power(t1,3) + 40*Power(t1,4) - 
            19*Power(t1,5) - 23*t2 + 6*Power(s2,4)*(-1 + t1)*t2 - 
            68*t1*t2 + 170*Power(t1,2)*t2 - 98*Power(t1,3)*t2 + 
            19*Power(t1,4)*t2 + 128*Power(t2,2) - 226*t1*Power(t2,2) + 
            54*Power(t1,2)*Power(t2,2) + 24*Power(t1,3)*Power(t2,2) + 
            12*Power(t2,3) + 24*t1*Power(t2,3) - 
            24*Power(t1,2)*Power(t2,3) - 
            4*Power(s1,3)*(8 - 6*t1 + Power(s2,2)*(3 + 3*t1 - 2*t2) + 
               s2*(8 - 3*Power(t1,2) + 3*t1*(-1 + t2) + 5*t2)) + 
            2*Power(s2,3)*(11 + 3*Power(t1,3) + 
               Power(t1,2)*(5 - 6*t2) - 12*t2 - 17*Power(t2,2) + 
               6*Power(t2,3) + t1*(-19 + 18*t2 - 15*Power(t2,2))) - 
            2*Power(s2,2)*(-13 + 5*Power(t1,4) + 16*t2 + 
               30*Power(t2,2) - 20*Power(t2,3) + 4*Power(t2,4) + 
               2*Power(t1,3)*(13 + t2) - 
               Power(t1,2)*(80 + 14*t2 + 33*Power(t2,2)) + 
               t1*(62 - 4*t2 - 5*Power(t2,2) + 12*Power(t2,3))) + 
            2*s2*(-47 + Power(t1,5) - 61*t2 - 3*Power(t2,2) + 
               6*Power(t2,3) - 2*Power(t2,4) + 
               Power(t1,4)*(24 + 5*t2) - Power(t1,2)*(93 + 8*t2) - 
               Power(t1,3)*(15 + 41*t2 + 12*Power(t2,2)) + 
               t1*(130 + 105*t2 + 25*Power(t2,2) - 6*Power(t2,3) + 
                  6*Power(t2,4))) - 
            2*Power(s1,2)*(-66 - 18*Power(t1,3) + 
               Power(s2,3)*(17 + 15*t1 - 6*t2) - 38*t2 + 
               3*Power(t1,2)*(-5 + 4*t2) + t1*(109 + 12*t2) + 
               Power(s2,2)*(46 - 27*t1 - 27*Power(t1,2) - 32*t2 + 
                  12*Power(t2,2)) + 
               s2*(19 + 12*Power(t1,3) - 38*t2 - 6*Power(t2,2) + 
                  12*Power(t1,2)*(2 + t2) + 
                  t1*(-65 + 18*t2 - 18*Power(t2,2)))) - 
            2*s1*(-13 + 3*Power(s2,4)*(-1 + t1) - Power(t1,4) + 130*t2 + 
               28*Power(t2,2) + 6*Power(t1,3)*(-3 + 5*t2) + 
               Power(t1,2)*(53 + 42*t2 - 24*Power(t2,2)) + 
               3*t1*(-7 - 74*t2 + 4*Power(t2,2)) - 
               2*Power(s2,3)*
                (6 + 3*Power(t1,2) + 17*t2 - 6*Power(t2,2) + 
                  3*t1*(-3 + 5*t2)) + 
               2*Power(s2,2)*
                (-14 + Power(t1,3) - 38*t2 + 23*Power(t2,2) - 
                  6*Power(t2,3) + Power(t1,2)*(-3 + 30*t2) + 
                  t1*(16 + 16*t2 - 9*Power(t2,2))) + 
               s2*(-75 + 2*Power(t1,4) - 22*t2 + 28*Power(t2,2) - 
                  2*Power(t2,3) - 8*Power(t1,3)*(1 + 3*t2) - 
                  Power(t1,2)*(79 + 24*t2 + 6*Power(t2,2)) + 
                  2*t1*(80 + 45*t2 - 9*Power(t2,2) + 9*Power(t2,3))))) + 
         s*(8*Power(s1,4)*s2*(3 + s2 - 2*t1) + 
            6*Power(s2,4)*(-1 + t1)*(-1 + t1 - 2*t2)*t2 - 
            2*Power(s2,3)*(-7 + Power(t1,4) + 2*t2 - 5*Power(t2,2) - 
               14*Power(t2,3) + 2*Power(t2,4) + 
               2*Power(t1,3)*(2 + 5*t2) - 
               3*Power(t1,2)*(6 + 6*t2 + 7*Power(t2,2)) + 
               t1*(20 + 6*t2 + 26*Power(t2,2))) + 
            (-1 + t1)*(71 + 5*Power(t1,5) - 17*t2 - 126*Power(t2,2) - 
               32*Power(t2,3) + Power(t1,4)*(-18 + 5*t2) + 
               Power(t1,3)*(13 + 40*t2 - 26*Power(t2,2)) - 
               2*t1*(75 - 83*t2 - 106*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(79 - 194*t2 - 24*Power(t2,2) + 
                  16*Power(t2,3))) + 
            s2*(-68 - 76*t2 + 44*Power(t2,2) + 52*Power(t2,3) + 
               16*Power(t2,4) - Power(t1,5)*(25 + 6*t2) + 
               2*Power(t1,4)*(8 + 19*t2 + Power(t2,2)) + 
               2*Power(t1,3)*
                (85 + 3*t2 + 8*Power(t2,2) + 8*Power(t2,3)) - 
               2*Power(t1,2)*
                (178 + 89*t2 - 7*Power(t2,2) + 6*Power(t2,3) + 
                  6*Power(t2,4)) + 
               t1*(263 + 216*t2 - 76*Power(t2,2) - 76*Power(t2,3) + 
                  8*Power(t2,4))) + 
            2*Power(s2,2)*(-5 + Power(t1,5) - 30*t2 - 16*Power(t2,2) + 
               20*Power(t2,3) - 4*Power(t2,4) + 
               Power(t1,4)*(17 + 11*t2) - 
               Power(t1,3)*(52 + 29*t2 + 19*Power(t2,2)) + 
               Power(t1,2)*(44 - 5*t2 + 14*Power(t2,2) - 
                  6*Power(t2,3)) + 
               t1*(-5 + 53*t2 + 21*Power(t2,2) - 10*Power(t2,3) + 
                  8*Power(t2,4))) - 
            4*Power(s1,3)*(2*(5 - 8*t1 + 3*Power(t1,2)) + 
               Power(s2,3)*(4 + 3*t1 - t2) + 
               s2*(21 + 3*Power(t1,3) - 3*Power(t1,2)*(-2 + t2) + 
                  22*t2 - 5*t1*(7 + 2*t2)) + 
               Power(s2,2)*(11 - 6*Power(t1,2) + 4*t2 + t1*(-3 + 4*t2))) \
- 2*Power(s1,2)*(6*Power(s2,4)*(-1 + t1) + 
               (-1 + t1)*(66 + 12*Power(t1,3) + 
                  Power(t1,2)*(19 - 8*t2) + 56*t2 - 5*t1*(23 + 4*t2)) + 
               Power(s2,3)*(1 - 15*Power(t1,2) - 30*t2 + 
                  6*Power(t2,2) - 2*t1*(-7 + 6*t2)) + 
               Power(s2,2)*(26 + 11*Power(t1,3) - 64*t2 + 
                  6*Power(t1,2)*(2 + 5*t2) + 
                  t1*(-49 + 22*t2 - 24*Power(t2,2))) + 
               s2*(-2*Power(t1,4) - 4*Power(t1,3)*(3 + 5*t2) + 
                  2*t1*(19 + 89*t2 + 6*Power(t2,2)) - 
                  5*(5 + 22*t2 + 12*Power(t2,2)) + 
                  Power(t1,2)*(1 - 18*t2 + 18*Power(t2,2)))) - 
            s1*(6*Power(s2,4)*(-1 + t1)*(-1 + t1 - 4*t2) - 
               4*Power(s2,3)*
                (2 + 4*Power(t1,3) - 2*t2 + t1*(20 - 3*t2)*t2 - 
                  18*Power(t2,2) + 3*Power(t2,3) - 
                  6*Power(t1,2)*(1 + 3*t2)) + 
               (-1 + t1)*(13*Power(t1,4) + Power(t1,3)*(21 - 50*t2) + 
                  t1*(165 + 442*t2 + 8*Power(t2,2)) + 
                  Power(t1,2)*(-181 - 62*t2 + 32*Power(t2,2)) - 
                  2*(9 + 129*t2 + 52*Power(t2,2))) + 
               s2*(-83 - 4*Power(t1,5) + 94*t2 + 188*Power(t2,2) + 
                  72*Power(t2,3) + Power(t1,4)*(3 + 6*t2) + 
                  2*Power(t1,3)*(53 + 20*t2 + 22*Power(t2,2)) - 
                  12*Power(t1,2)*(24 - t2 + 3*Power(t2,3)) + 
                  2*t1*(133 - 76*t2 - 146*Power(t2,2) + 4*Power(t2,3))) \
+ 2*Power(s2,2)*(-40 + 7*Power(t1,4) - 42*t2 + 62*Power(t2,2) - 
                  8*Power(t2,3) - Power(t1,3)*(7 + 30*t2) + 
                  Power(t1,2)*(-47 + 2*t2 - 24*Power(t2,2)) + 
                  t1*(87 + 70*t2 - 26*Power(t2,2) + 24*Power(t2,3))))) + 
         (-1 + t1)*(8*Power(s1,4)*s2*(2 + s2 - t1) + 
            2*Power(s2,4)*t2*(1 - 2*t1 + Power(t1,2) - 2*Power(t2,2)) - 
            2*Power(s2,3)*(-1 + 3*t2 + Power(t2,2) - 2*Power(t2,3) + 
               2*Power(t2,4) + Power(t1,3)*(1 + 3*t2) - 
               Power(t1,2)*(3 + 3*t2 + Power(t2,2)) - 
               3*t1*(-1 + t2 + 2*Power(t2,3))) + 
            (-1 + t1)*(Power(t1,4)*(-3 + 4*t2) + 
               Power(t1,3)*(15 + 7*t2 - 8*Power(t2,2)) + 
               t1*(-23 + 81*t2 + 78*Power(t2,2)) - 
               16*(-1 + t2 + 3*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-5 - 76*t2 - 6*Power(t2,2) + 4*Power(t2,3))) \
+ s2*(-15 - 4*Power(t1,5) + t2 + 54*Power(t2,2) + 48*Power(t2,3) + 
               16*Power(t2,4) + Power(t1,4)*(-1 + t2 - 4*Power(t2,2)) + 
               2*Power(t1,3)*
                (21 - 4*t2 + 6*Power(t2,2) + 4*Power(t2,3)) - 
               2*t1*(-29 + 4*t2 + 60*Power(t2,2) + 34*Power(t2,3)) - 
               2*Power(t1,2)*(40 - 7*t2 - 29*Power(t2,2) + 
                  2*Power(t2,3) + 2*Power(t2,4))) + 
            Power(s2,2)*(-8 - 13*t2 + 20*Power(t2,2) + 32*Power(t2,3) + 
               Power(t1,4)*(6 + 4*t2) + 
               Power(t1,3)*(-10 + t2 + 2*Power(t2,2)) - 
               Power(t1,2)*(6 + 27*t2 + 20*Power(t2,2) + 
                  16*Power(t2,3)) + 
               t1*(18 + 35*t2 - 2*Power(t2,2) + 4*Power(t2,3) + 
                  8*Power(t2,4))) + 
            4*Power(s1,3)*(Power(s2,4) - 2*(2 - 3*t1 + Power(t1,2)) + 
               Power(s2,2)*(-11 + 3*Power(t1,2) + t1*(3 - 2*t2) - 
                  6*t2) + Power(s2,3)*(-1 - 3*t1 + t2) - 
               s2*(Power(t1,3) - Power(t1,2)*(-4 + t2) - 
                  t1*(23 + 6*t2) + 2*(7 + 8*t2))) - 
            2*Power(s1,2)*(6*Power(s2,4)*t2 + 
               (-1 + t1)*(3*Power(t1,3) + Power(t1,2)*(7 - 2*t2) + 
                  24*(1 + t2) - 2*t1*(21 + 4*t2)) + 
               Power(s2,3)*(3 + Power(t1,2) - 6*t2 + 6*Power(t2,2) - 
                  2*t1*(2 + 9*t2)) + 
               Power(s2,2)*(-11 - 2*Power(t1,3) - 60*t2 - 
                  12*Power(t2,2) + Power(t1,2)*(11 + 20*t2) + 
                  t1*(2 + 10*t2 - 12*Power(t2,2))) + 
               s2*(Power(t1,4) - 2*Power(t1,3)*(1 + 4*t2) + 
                  Power(t1,2)*(-37 - 14*t2 + 6*Power(t2,2)) + 
                  2*t1*(34 + 63*t2 + 6*Power(t2,2)) - 
                  2*(15 + 40*t2 + 24*Power(t2,2)))) - 
            s1*(2*Power(s2,4)*(1 - 2*t1 + Power(t1,2) - 6*Power(t2,2)) + 
               (-1 + t1)*(5*Power(t1,4) + Power(t1,3)*(6 - 14*t2) - 
                  16*(1 + 6*t2 + 3*Power(t2,2)) + 
                  2*t1*(41 + 81*t2 + 4*Power(t2,2)) + 
                  Power(t1,2)*(-77 - 20*t2 + 8*Power(t2,2))) - 
               4*Power(s2,3)*(2 + Power(t1,3) + 2*t2 - 3*Power(t2,2) + 
                  3*Power(t2,3) - t1*(3 + 2*t2 + 9*Power(t2,2))) - 
               2*s2*(-1 - 57*t2 - 76*Power(t2,2) - 32*Power(t2,3) + 
                  Power(t1,4)*(2 + 3*t2) - 
                  Power(t1,3)*(3 + 8*t2 + 10*Power(t2,2)) + 
                  t1*(3 + 128*t2 + 114*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(t1,2)*
                   (-1 - 66*t2 - 4*Power(t2,2) + 6*Power(t2,3))) + 
               Power(s2,2)*(-17 + 2*Power(t1,4) + 42*t2 + 
                  108*Power(t2,2) + 8*Power(t2,3) + 
                  Power(t1,3)*(11 + 6*t2) - 
                  Power(t1,2)*(45 + 42*t2 + 44*Power(t2,2)) + 
                  t1*(49 - 6*t2 - 4*Power(t2,2) + 24*Power(t2,3))))))*
       T3q(s2,1 - s + s2 - t1))/
     (Power(-1 + s + t1,2)*Power(s - s2 + t1,2)*(-1 + s1 + t1 - t2)*
       (s - s1 + t2)*(1 - s + s*s2 - s1*s2 - t1 + s2*t2)) - 
    (8*(16 - 23*s2 - 16*Power(s1,4)*s2 - 8*Power(s2,2) - 3*Power(s2,3) + 
         4*Power(s2,4) - 2*Power(s2,5) - t1 + 16*s2*t1 + 
         23*Power(s2,2)*t1 - 2*Power(s2,3)*t1 + 2*Power(s2,4)*t1 + 
         2*Power(s2,5)*t1 - 40*Power(t1,2) + 47*s2*Power(t1,2) - 
         26*Power(s2,2)*Power(t1,2) + Power(s2,3)*Power(t1,2) - 
         6*Power(s2,4)*Power(t1,2) + 13*Power(t1,3) - 36*s2*Power(t1,3) + 
         11*Power(s2,2)*Power(t1,3) + 4*Power(s2,3)*Power(t1,3) + 
         12*Power(t1,4) - 4*s2*Power(t1,4) - 16*t2 + s2*t2 - 
         27*Power(s2,2)*t2 - 13*Power(s2,3)*t2 - Power(s2,4)*t2 + 
         2*Power(s2,5)*t2 - 2*Power(s2,6)*t2 + 95*t1*t2 - 40*s2*t1*t2 + 
         22*Power(s2,2)*t1*t2 + 14*Power(s2,3)*t1*t2 - 
         Power(s2,4)*t1*t2 + 6*Power(s2,5)*t1*t2 - 69*Power(t1,2)*t2 + 
         51*s2*Power(t1,2)*t2 + 15*Power(s2,2)*Power(t1,2)*t2 - 
         9*Power(s2,3)*Power(t1,2)*t2 - 4*Power(s2,4)*Power(t1,2)*t2 - 
         20*Power(t1,3)*t2 - 12*s2*Power(t1,3)*t2 + 8*Power(t1,4)*t2 - 
         48*Power(t2,2) + 6*s2*Power(t2,2) - 22*Power(s2,2)*Power(t2,2) - 
         4*Power(s2,3)*Power(t2,2) - 2*Power(s2,4)*Power(t2,2) - 
         2*Power(s2,5)*Power(t2,2) + 66*t1*Power(t2,2) + 
         38*s2*t1*Power(t2,2) - 60*Power(s2,2)*t1*Power(t2,2) + 
         22*Power(s2,3)*t1*Power(t2,2) - 2*Power(s2,4)*t1*Power(t2,2) + 
         24*s2*Power(t1,2)*Power(t2,2) + 
         4*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         4*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         16*Power(t1,3)*Power(t2,2) - 8*s2*Power(t1,3)*Power(t2,2) - 
         16*Power(t2,3) - 40*s2*Power(t2,3) + 
         24*Power(s2,2)*Power(t2,3) - 12*Power(s2,3)*Power(t2,3) + 
         4*Power(s2,4)*Power(t2,3) + 8*t1*Power(t2,3) + 
         8*s2*t1*Power(t2,3) - 12*Power(s2,2)*t1*Power(t2,3) - 
         4*Power(s2,3)*t1*Power(t2,3) + 8*Power(t1,2)*Power(t2,3) + 
         16*s2*Power(t1,2)*Power(t2,3) - 16*s2*Power(t2,4) + 
         8*Power(s2,2)*Power(t2,4) - 8*s2*t1*Power(t2,4) + 
         Power(s,3)*(-1 + s2)*
          (8 + t1 - 8*Power(t1,2) + s2*(-8 + 5*t1 - 5*t2) + 
            2*Power(s2,2)*(t1 - t2) - t2 + 16*t1*t2 - 8*Power(t2,2) + 
            8*s1*(-1 + s2 - t1 + t2)) + 
         8*Power(s1,3)*(2 - 2*t1 + Power(s2,2)*(2*t1 - t2) + 
            s2*(6 - Power(t1,2) + t1*(-5 + t2) + 8*t2)) + 
         2*Power(s1,2)*(Power(s2,5) - 4*Power(t1,3) + 
            Power(s2,3)*(Power(t1,2) - 2*t1*(-5 + t2) - 6*t2) - 
            2*Power(s2,4)*(2 + t1 - t2) - 24*(1 + t2) + 
            Power(t1,2)*(-15 + 4*t2) + 4*t1*(11 + 5*t2) + 
            Power(s2,2)*(-13 + 6*Power(t1,2) + 12*t2 + 12*Power(t2,2) - 
               2*t1*(14 + 11*t2)) - 
            4*s2*(2 + Power(t1,3) + 17*t2 + 12*Power(t2,2) - 
               Power(t1,2)*(1 + 4*t2) + t1*(-9 - 11*t2 + 3*Power(t2,2)))) \
+ s1*(2*Power(s2,6) - 8*Power(t1,4) - 4*Power(s2,5)*(1 + t1) + 
            Power(t1,3)*(7 + 24*t2) + 
            Power(t1,2)*(93 + 30*t2 - 16*Power(t2,2)) + 
            16*(1 + 6*t2 + 3*Power(t2,2)) - 
            2*t1*(53 + 77*t2 + 16*Power(t2,2)) + 
            Power(s2,4)*(1 + 2*Power(t1,2) + 10*t2 - 8*Power(t2,2) + 
               t1*(3 + 6*t2)) + 
            Power(s2,3)*(Power(t1,2)*(8 - 6*t2) + 
               2*t1*(-8 - 21*t2 + 4*Power(t2,2)) + 
               4*(4 + t2 + 6*Power(t2,2))) + 
            Power(s2,2)*(23 + Power(t1,3) + 48*t2 - 48*Power(t2,2) - 
               24*Power(t2,3) - Power(t1,2)*(17 + 16*t2) + 
               t1*(-17 + 116*t2 + 40*Power(t2,2))) + 
            2*s2*(5 + 5*t2 + 64*Power(t2,2) + 32*Power(t2,3) + 
               8*Power(t1,3)*(1 + t2) - 
               Power(t1,2)*(23 + 16*t2 + 20*Power(t2,2)) + 
               t1*(10 - 55*t2 - 28*Power(t2,2) + 12*Power(t2,3)))) + 
         Power(s,2)*(9 - 40*t1 + 16*Power(t1,2) + 16*Power(t1,3) + 
            18*t2 - 38*t1*t2 - 24*Power(t1,2)*t2 + 22*Power(t2,2) + 
            8*Power(t2,3) + Power(s2,4)*(-4*t1 + 2*t2) + 
            Power(s2,3)*(8 + t1 + 2*Power(t1,2) + 4*t2 + 4*t1*t2 - 
               6*Power(t2,2)) - 
            8*Power(s1,2)*(-1 + 2*Power(s2,2) + t1 - t2 + 
               s2*(-1 - 2*t1 + 2*t2)) + 
            Power(s2,2)*(-31 + 10*Power(t1,2) - 38*t2 + 8*Power(t2,2) - 
               6*t1*(-2 + 3*t2)) + 
            s2*(14 - 44*Power(t1,2) - 8*Power(t1,3) + 6*t2 - 
               40*Power(t2,2) - 16*Power(t2,3) + 
               3*t1*(13 + 28*t2 + 8*Power(t2,2))) + 
            s1*(2*Power(s2,4) + 8*Power(t1,2) + 
               Power(s2,3)*(-15 - 4*t1 + 6*t2) + t1*(51 + 8*t2) - 
               2*(8 + 15*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*(9*t1 + 8*(7 + t2)) + 
               s2*(-27 + 8*Power(t1,2) + 32*t2 + 32*Power(t2,2) - 
                  8*t1*(9 + 5*t2)))) + 
         s*(-17 + 17*t1 - 35*Power(t1,2) + 29*Power(t1,3) + 
            8*Power(t1,4) - t2 - 3*t1*t2 - 59*Power(t1,2)*t2 + 
            18*Power(t2,2) + 22*t1*Power(t2,2) - 
            24*Power(t1,2)*Power(t2,2) + 8*Power(t2,3) + 
            16*t1*Power(t2,3) + 2*Power(s2,5)*(t1 + t2) + 
            8*Power(s1,3)*(-2 + Power(s2,2) + s2*(3 - t1 + t2)) - 
            Power(s2,4)*(-2 + 2*Power(t1,2) + 3*t2 - 8*Power(t2,2) + 
               2*t1*(3 + 5*t2)) + 
            Power(s2,3)*(-13 + 3*t2 - 6*Power(t2,2) - 4*Power(t2,3) + 
               Power(t1,2)*(1 + 6*t2) + t1*(16 + 15*t2 - 2*Power(t2,2))) \
- Power(s2,2)*(3 + Power(t1,3) + 18*t2 - 4*Power(t2,2) - 
               20*Power(t2,3) - Power(t1,2)*(47 + 17*t2) + 
               t1*(39 + 79*t2 + 36*Power(t2,2))) + 
            s2*(15 - 31*t2 - 80*Power(t2,2) - 56*Power(t2,3) - 
               8*Power(t2,4) - 4*Power(t1,3)*(7 + 4*t2) + 
               Power(t1,2)*(-51 + 4*t2 + 24*Power(t2,2)) + 
               t1*(58 + 173*t2 + 80*Power(t2,2))) - 
            2*Power(s1,2)*(Power(s2,4) + 8*Power(t1,2) + 
               t1*(11 - 8*t2) - Power(s2,3)*(8 + t1 - 2*t2) + 
               2*Power(s2,2)*(10 + 7*t1 - t2) - 20*(1 + t2) + 
               s2*(31 - 4*Power(t1,2) + 52*t2 + 12*Power(t2,2) - 
                  8*t1*(7 + t2))) + 
            s1*(-4*Power(s2,5) - 8*Power(t1,3) + 
               Power(s2,4)*(11 + 8*t1 - 6*t2) + 
               10*Power(t1,2)*(5 + 4*t2) + t1*(29 - 32*Power(t2,2)) - 
               Power(s2,3)*(9 + 15*t1 + 4*Power(t1,2) + 10*t2 - 
                  8*Power(t2,2)) - 2*(5 + 29*t2 + 16*Power(t2,2)) + 
               Power(s2,2)*(27 - 22*Power(t1,2) + 36*t2 - 
                  32*Power(t2,2) + t1*(71 + 64*t2)) + 
               s2*(33 + 16*Power(t1,3) + 142*t2 + 136*Power(t2,2) + 
                  24*Power(t2,3) - 8*Power(t1,2)*(-3 + 4*t2) - 
                  t1*(197 + 192*t2 + 8*Power(t2,2))))))*T4q(-1 + s2))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(-1 + s1 + t1 - t2)*(s - s1 + t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)) + 
    (8*(-15*s2 + 8*Power(s1,4)*s2 - 8*Power(s2,2) + 2*Power(s2,3) - 9*t1 + 
         43*s2*t1 + 10*Power(s2,2)*t1 - 4*Power(s2,3)*t1 + 
         21*Power(t1,2) - 37*s2*Power(t1,2) + 4*Power(s2,2)*Power(t1,2) + 
         2*Power(s2,3)*Power(t1,2) - 15*Power(t1,3) + 5*s2*Power(t1,3) - 
         6*Power(s2,2)*Power(t1,3) + 3*Power(t1,4) + 4*s2*Power(t1,4) + 
         16*t2 + 6*s2*t2 - 25*Power(s2,2)*t2 - 4*Power(s2,3)*t2 + 
         2*Power(s2,4)*t2 - 78*t1*t2 + 5*s2*t1*t2 + 28*Power(s2,2)*t1*t2 - 
         2*Power(s2,3)*t1*t2 - 2*Power(s2,4)*t1*t2 + 76*Power(t1,2)*t2 - 
         6*s2*Power(t1,2)*t2 + Power(s2,2)*Power(t1,2)*t2 + 
         6*Power(s2,3)*Power(t1,2)*t2 - 10*Power(t1,3)*t2 - 
         5*s2*Power(t1,3)*t2 - 4*Power(s2,2)*Power(t1,3)*t2 - 
         4*Power(t1,4)*t2 + 40*Power(t2,2) + 11*s2*Power(t2,2) + 
         7*Power(s2,2)*Power(t2,2) - 8*Power(s2,3)*Power(t2,2) + 
         2*Power(s2,4)*Power(t2,2) - 59*t1*Power(t2,2) - 
         52*s2*t1*Power(t2,2) + 19*Power(s2,2)*t1*Power(t2,2) - 
         8*Power(s2,3)*t1*Power(t2,2) + 13*Power(t1,2)*Power(t2,2) + 
         5*s2*Power(t1,2)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         8*Power(t1,3)*Power(t2,2) + 4*s2*Power(t1,3)*Power(t2,2) + 
         8*Power(t2,3) + 38*s2*Power(t2,3) - 8*Power(s2,2)*Power(t2,3) + 
         2*Power(s2,3)*Power(t2,3) - 6*t1*Power(t2,3) - 
         12*s2*t1*Power(t2,3) + 6*Power(s2,2)*t1*Power(t2,3) - 
         4*Power(t1,2)*Power(t2,3) - 8*s2*Power(t1,2)*Power(t2,3) + 
         8*s2*Power(t2,4) - 4*Power(s2,2)*Power(t2,4) + 
         4*s2*t1*Power(t2,4) + 
         Power(s,3)*(8*Power(s2,2) + 
            s1*(-4 - 3*t1 + 2*s2*(2 + t1 - t2) + 3*t2) + 
            2*s2*(-10 - t1 + Power(t1,2) + t2 - 2*t1*t2 + Power(t2,2)) - 
            3*(-4 - t1 + Power(t1,2) + t2 - 2*t1*t2 + Power(t2,2))) + 
         2*Power(s1,3)*(-4 + Power(s2,3) + 4*t1 + Power(t1,2) + 
            Power(s2,2)*(-1 - 2*t1 + 2*t2) + 
            s2*(Power(t1,2) - 2*t1*(-6 + t2) - 4*(5 + 4*t2))) + 
         Power(s1,2)*(2*Power(s2,4) + 7*Power(t1,3) + 
            Power(t1,2)*(21 - 8*t2) - 22*t1*(3 + t2) - 
            2*Power(s2,3)*(7 + t1 + t2) + 8*(5 + 3*t2) + 
            Power(s2,2)*(5 - 2*Power(t1,2) - 4*t2 - 12*Power(t2,2) + 
               t1*(25 + 14*t2)) + 
            2*s2*(9 + Power(t1,3) + Power(t1,2)*(3 - 6*t2) + 59*t2 + 
               24*Power(t2,2) + t1*(-29 - 30*t2 + 6*Power(t2,2)))) + 
         Power(s,2)*(-19 - 8*Power(s2,3) + 51*t1 + 4*Power(t1,2) - 
            8*Power(t1,3) - 17*t2 + 14*Power(t1,2)*t2 - 4*Power(t2,2) - 
            4*t1*Power(t2,2) - 2*Power(t2,3) + 
            Power(s2,2)*(44 - 4*Power(t1,2) + 18*t2 - 2*Power(t2,2) + 
               t1*(4 + 6*t2)) + 
            Power(s1,2)*(2*Power(s2,2) + t1 - 2*t2 + 
               s2*(-5 - 4*t1 + 6*t2)) + 
            s1*(23 - 17*t1 - 7*Power(t1,2) - 2*Power(s2,2)*(13 + t1) + 
               4*t2 + 3*t1*t2 + 4*Power(t2,2) + 
               s2*(9 + 26*t1 - 2*Power(t1,2) - 13*t2 + 14*t1*t2 - 
                  12*Power(t2,2))) + 
            s2*(-20 + 2*Power(t1,3) + 4*t2 + 18*Power(t2,2) + 
               6*Power(t2,3) + Power(t1,2)*(13 + 2*t2) - 
               t1*(59 + 31*t2 + 10*Power(t2,2)))) + 
         s1*(5*Power(t1,4) + Power(t1,3)*(9 - 15*t2) + 
            2*Power(s2,4)*(-1 + t1 - 2*t2) - 
            2*Power(s2,3)*(-3 + t1 + 2*Power(t1,2) - 11*t2 - 5*t1*t2 + 
               Power(t2,2)) - 8*(2 + 10*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(-77 - 34*t2 + 10*Power(t2,2)) + 
            t1*(79 + 125*t2 + 20*Power(t2,2)) + 
            Power(s2,2)*(29 + 7*Power(t1,2) + 2*Power(t1,3) - 12*t2 + 
               14*Power(t2,2) + 12*Power(t2,3) - 
               2*t1*(19 + 22*t2 + 8*Power(t2,2))) - 
            s2*(7 + 29*t2 + 6*Power(t1,3)*t2 + 116*Power(t2,2) + 
               32*Power(t2,3) + 
               Power(t1,2)*(-15 + 11*t2 - 18*Power(t2,2)) + 
               2*t1*(4 - 55*t2 - 24*Power(t2,2) + 6*Power(t2,3)))) - 
         s*(-7 + 38*t1 - 32*Power(t1,2) - 4*Power(t1,3) + 5*Power(t1,4) - 
            2*t2 - 35*t1*t2 + 7*Power(t1,2)*t2 - 4*Power(t1,3)*t2 + 
            35*Power(t2,2) - 9*t1*Power(t2,2) - 
            7*Power(t1,2)*Power(t2,2) + 6*Power(t2,3) + 6*t1*Power(t2,3) + 
            Power(s2,3)*(8 - 6*t1 - 2*Power(t1,2) + 22*t2 + 
               2*Power(t2,2)) + 
            2*Power(s1,3)*(-4 + Power(s2,2) - t1 + s2*(3 - t1 + 2*t2)) + 
            Power(s2,2)*(18 + 2*Power(t1,3) - 43*t2 + 7*Power(t2,2) + 
               8*Power(t2,3) + 4*Power(t1,2)*(5 + 2*t2) - 
               t1*(40 + 47*t2 + 18*Power(t2,2))) - 
            s2*(51 + 26*t2 + 61*Power(t2,2) + 18*Power(t2,3) + 
               4*Power(t2,4) + Power(t1,3)*(13 + 6*t2) + 
               Power(t1,2)*(3 - 12*t2 - 8*Power(t2,2)) - 
               t1*(67 + 114*t2 + 19*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,2)*(42 + 4*Power(s2,3) - 8*Power(t1,2) + 22*t2 + 
               Power(s2,2)*(-19 - 6*t1 + 4*t2) + t1*(-21 + 10*t2) + 
               s2*(-59 + 2*Power(t1,2) - 30*t2 - 12*Power(t2,2) + 
                  t1*(41 + 6*t2))) + 
            s1*(1 - Power(t1,3) - 77*t2 - 20*Power(t2,2) + 
               Power(t1,2)*(4 + 15*t2) + 
               t1*(30 + 30*t2 - 14*Power(t2,2)) + 
               2*Power(s2,3)*(t1 - 3*(4 + t2)) + 
               Power(s2,2)*(51 - 6*Power(t1,2) + 12*t2 - 14*Power(t2,2) + 
                  t1*(37 + 24*t2)) + 
               2*s2*(2*Power(t1,3) + Power(t1,2)*(2 - 5*t2) - 
                  3*t1*(23 + 10*t2 + Power(t2,2)) + 
                  3*(6 + 20*t2 + 7*Power(t2,2) + 2*Power(t2,3))))))*
       T5q(1 - s1 - t1 + t2))/
     (Power(s - s2 + t1,2)*(-1 + s1 + t1 - t2)*(s - s1 + t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)));
   return a;
};
