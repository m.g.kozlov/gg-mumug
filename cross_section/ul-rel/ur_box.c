#include "../h/nlo_functions.h"
#include "../h/dilog.h"

#define Pi       3.141592653589793238462643383279502884L
#define Power pq



long double ur_box(double *vars)
{
    long double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    long double aG=1/Power(4.*Pi,2);
    long double a=aG*((-8*Power(Pi,2)*(Power(s2,5)*(s2 - 2*t1)*Power(s2 - t1,2)*Power(t1,2)*
          Power(t2,7) - s1*Power(s2,4)*Power(s2 - t1,2)*Power(t1,2)*
          Power(t2,6)*(Power(s2,2) - 11*s2*t1 + 4*Power(t1,2) - 3*s2*t2 + 
            6*t1*t2) + Power(s1,2)*Power(s2,3)*Power(t2,5)*
          (Power(s2,5)*(2*Power(t1,2) + Power(t2,2)) - 
            4*Power(s2,4)*t1*(10*Power(t1,2) - 2*t1*t2 + Power(t2,2)) - 
            2*Power(t1,5)*(5*Power(t1,2) - 3*t1*t2 + 6*Power(t2,2)) - 
            4*Power(s2,2)*Power(t1,3)*
             (25*Power(t1,2) + 4*t1*t2 + 7*Power(t2,2)) + 
            Power(s2,3)*Power(t1,2)*
             (101*Power(t1,2) - t1*t2 + 12*Power(t2,2)) + 
            s2*Power(t1,4)*(47*Power(t1,2) + 3*t1*t2 + 30*Power(t2,2))) + 
         Power(s1,8)*Power(s2 - t1,2)*
          (s2*Power(t1,4)*Power(t2,2) + Power(t1,5)*Power(t2,2) - 
            Power(s2,3)*Power(t1,2)*
             (Power(t1,2) + 2*t1*t2 - 5*Power(t2,2)) + 
            Power(s2,5)*(Power(t1,2) + Power(t2,2)) + 
            Power(s2,4)*t1*(-Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(Power(t1,5) + 3*Power(t1,3)*Power(t2,2))) + 
         Power(s1,7)*(s2 - t1)*
          (4*Power(t1,6)*Power(t2,3) - 
            3*s2*Power(t1,5)*Power(t2,2)*(t1 + 3*t2) + 
            Power(s2,5)*t1*t2*(-13*Power(t1,2) + t1*t2 - 7*Power(t2,2)) + 
            Power(s2,7)*(Power(t1,2) + Power(t2,2)) + 
            Power(s2,2)*Power(t1,4)*t2*
             (4*Power(t1,2) - 11*t1*t2 + 31*Power(t2,2)) + 
            Power(s2,6)*(-2*Power(t1,3) + 3*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - 3*Power(t2,3)) + 
            Power(s2,4)*Power(t1,2)*
             (2*Power(t1,3) + 21*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
               Power(t2,3)) - 
            Power(s2,3)*Power(t1,3)*
             (Power(t1,3) + 15*Power(t1,2)*t2 - 11*t1*Power(t2,2) + 
               19*Power(t2,3))) + 
         Power(s1,3)*Power(s2,2)*Power(t2,4)*
          (-(Power(s2,6)*t2*(2*t1 + 3*t2)) - 
            2*Power(t1,5)*t2*(6*Power(t1,2) - 3*t1*t2 + 5*Power(t2,2)) + 
            Power(s2,5)*(50*Power(t1,3) - 22*Power(t1,2)*t2 + 
               19*t1*Power(t2,2) + Power(t2,3)) - 
            Power(s2,4)*t1*(145*Power(t1,3) - 9*Power(t1,2)*t2 + 
               22*t1*Power(t2,2) + 6*Power(t2,3)) + 
            Power(s2,3)*Power(t1,2)*
             (165*Power(t1,3) + 58*Power(t1,2)*t2 + 42*t1*Power(t2,2) + 
               17*Power(t2,3)) + 
            s2*Power(t1,4)*(25*Power(t1,3) + 24*Power(t1,2)*t2 + 
               24*t1*Power(t2,2) + 25*Power(t2,3)) - 
            Power(s2,2)*Power(t1,3)*
             (95*Power(t1,3) + 55*Power(t1,2)*t2 + 62*t1*Power(t2,2) + 
               28*Power(t2,3))) + 
         Power(s1,4)*s2*Power(t2,3)*
          (-2*Power(t1,6)*Power(t2,2)*(3*t1 + 2*t2) + 
            Power(s2,7)*(-6*Power(t1,2) + 6*t1*t2 + 2*Power(t2,2)) + 
            s2*Power(t1,5)*t2*
             (30*Power(t1,2) + 3*t1*t2 + 47*Power(t2,2)) - 
            Power(s2,6)*(20*Power(t1,3) - 14*Power(t1,2)*t2 + 
               36*t1*Power(t2,2) + 3*Power(t2,3)) + 
            Power(s2,5)*t1*(95*Power(t1,3) + 7*Power(t1,2)*t2 + 
               23*t1*Power(t2,2) + 25*Power(t2,3)) - 
            Power(s2,4)*Power(t1,2)*
             (134*Power(t1,3) + 86*Power(t1,2)*t2 + 27*t1*Power(t2,2) + 
               59*Power(t2,3)) + 
            Power(s2,3)*Power(t1,3)*
             (93*Power(t1,3) + 91*Power(t1,2)*t2 + 91*t1*Power(t2,2) + 
               93*Power(t2,3)) - 
            Power(s2,2)*Power(t1,4)*
             (28*Power(t1,3) + 62*Power(t1,2)*t2 + 55*t1*Power(t2,2) + 
               95*Power(t2,3))) + 
         Power(s1,6)*t2*(5*Power(t1,7)*Power(t2,3) - 
            3*s2*Power(t1,6)*Power(t2,2)*(4*t1 + 9*t2) - 
            Power(s2,8)*(Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*Power(t1,5)*t2*
             (12*Power(t1,2) - t1*t2 + 101*Power(t2,2)) + 
            Power(s2,5)*Power(t1,2)*
             (-38*Power(t1,3) - 27*Power(t1,2)*t2 + t1*Power(t2,2) - 
               53*Power(t2,3)) - 
            Power(s2,7)*(4*Power(t1,3) + 3*Power(t1,2)*t2 + 
               16*t1*Power(t2,2) - 2*Power(t2,3)) + 
            3*Power(s2,6)*t1*(8*Power(t1,3) + 5*Power(t1,2)*t2 + 
               5*t1*Power(t2,2) + 8*Power(t2,3)) + 
            Power(s2,4)*Power(t1,3)*
             (25*Power(t1,3) + 23*Power(t1,2)*t2 + 7*t1*Power(t2,2) + 
               95*Power(t2,3)) - 
            Power(s2,3)*Power(t1,4)*
             (6*Power(t1,3) + 22*Power(t1,2)*t2 - 9*t1*Power(t2,2) + 
               145*Power(t2,3))) + 
         Power(s1,5)*Power(t2,2)*
          (-2*Power(t1,7)*Power(t2,3) + 
            s2*Power(t1,6)*Power(t2,2)*(15*t1 + 19*t2) + 
            2*Power(s2,8)*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) - 
            4*Power(s2,2)*Power(t1,5)*t2*
             (7*Power(t1,2) + 4*t1*t2 + 25*Power(t2,2)) + 
            Power(s2,6)*t1*(-53*Power(t1,3) + Power(t1,2)*t2 - 
               27*t1*Power(t2,2) - 38*Power(t2,3)) + 
            2*Power(s2,7)*(4*Power(t1,3) - 2*Power(t1,2)*t2 + 
               17*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s2,5)*Power(t1,2)*
             (83*Power(t1,3) + 22*Power(t1,2)*t2 + 22*t1*Power(t2,2) + 
               83*Power(t2,3)) - 
            Power(s2,4)*Power(t1,3)*
             (59*Power(t1,3) + 27*Power(t1,2)*t2 + 86*t1*Power(t2,2) + 
               134*Power(t2,3)) + 
            Power(s2,3)*Power(t1,4)*
             (17*Power(t1,3) + 42*Power(t1,2)*t2 + 58*t1*Power(t2,2) + 
               165*Power(t2,3))) - 
         s*(-(Power(s2,4)*(s2 - t1)*Power(t1,2)*Power(t2,6)*
               (2*Power(s2,3) - s2*t1*(t1 - 14*t2) + 
                 2*Power(t1,2)*(2*t1 - 3*t2) - Power(s2,2)*(5*t1 + 6*t2))\
) - s1*Power(s2,3)*(s2 - t1)*Power(t1,2)*Power(t2,5)*
             (4*Power(t1,2)*(2*t1 - 3*t2)*t2 + 
               Power(s2,3)*(24*t1 + 13*t2) + s2*t1*t2*(38*t1 + 33*t2) - 
               Power(s2,2)*(24*Power(t1,2) + 71*t1*t2 + 15*Power(t2,2))) \
+ 2*Power(s1,8)*(s2 - t1)*(s2*Power(t1,4)*Power(t2,2) + 
               Power(t1,5)*Power(t2,2) - 
               Power(s2,3)*Power(t1,2)*
                (Power(t1,2) + 2*t1*t2 - 4*Power(t2,2)) + 
               Power(s2,5)*(Power(t1,2) + Power(t2,2)) + 
               Power(s2,4)*t1*(-Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(Power(t1,5) + 3*Power(t1,3)*Power(t2,2))) + 
            Power(s1,2)*Power(s2,2)*Power(t2,4)*
             (-2*Power(s2,6)*(2*Power(t1,2) + Power(t2,2)) - 
               4*Power(t1,5)*t2*
                (4*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,5)*(68*Power(t1,3) - 10*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + 6*Power(t2,3)) - 
               Power(s2,4)*t1*
                (154*Power(t1,3) + 181*Power(t1,2)*t2 - 
                  11*t1*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,3)*Power(t1,2)*
                (145*Power(t1,3) + 375*Power(t1,2)*t2 + 
                  56*t1*Power(t2,2) + 50*Power(t2,3)) + 
               s2*Power(t1,4)*
                (25*Power(t1,3) + 49*Power(t1,2)*t2 - 
                  18*t1*Power(t2,2) + 70*Power(t2,3)) - 
               Power(s2,2)*Power(t1,3)*
                (80*Power(t1,3) + 217*Power(t1,2)*t2 + 
                  79*t1*Power(t2,2) + 92*Power(t2,3))) + 
            Power(s1,7)*(Power(t1,6)*Power(t2,2)*(6*t1 + 7*t2) - 
               s2*Power(t1,5)*Power(t2,2)*(13*t1 + 24*t2) + 
               5*Power(s2,7)*(Power(t1,2) + Power(t2,2)) + 
               2*Power(s2,5)*t1*
                (3*Power(t1,3) - 30*Power(t1,2)*t2 - t1*Power(t2,2) - 
                  10*Power(t2,3)) + 
               Power(s2,6)*(-14*Power(t1,3) + 15*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) - 7*Power(t2,3)) - 
               Power(s2,3)*Power(t1,3)*
                (19*Power(t1,3) + 48*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + 44*Power(t2,3)) + 
               Power(s2,2)*Power(t1,4)*
                (6*Power(t1,3) + 9*Power(t1,2)*t2 - 10*t1*Power(t2,2) + 
                  68*Power(t2,3)) + 
               2*Power(s2,4)*(8*Power(t1,5) + 42*Power(t1,4)*t2 + 
                  7*Power(t1,2)*Power(t2,3))) + 
            Power(s1,3)*s2*Power(t2,3)*
             (4*Power(t1,6)*Power(t2,2)*(-3*t1 + 2*t2) + 
               Power(s2,7)*(-2*Power(t1,2) + 4*t1*t2 + 4*Power(t2,2)) - 
               Power(s2,6)*(44*Power(t1,3) - 6*Power(t1,2)*t2 + 
                  48*t1*Power(t2,2) + 19*Power(t2,3)) + 
               s2*Power(t1,4)*t2*
                (70*Power(t1,3) - 18*Power(t1,2)*t2 + 
                  49*t1*Power(t2,2) + 25*Power(t2,3)) - 
               6*Power(s2,4)*t1*
                (32*Power(t1,4) + 107*Power(t1,3)*t2 + 
                  8*Power(t1,2)*Power(t2,2) + 20*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,5)*(138*Power(t1,4) + 299*Power(t1,3)*t2 - 
                  79*Power(t1,2)*Power(t2,2) + 100*t1*Power(t2,3) + 
                  5*Power(t2,4)) - 
               4*Power(s2,2)*Power(t1,3)*
                (14*Power(t1,4) + 51*Power(t1,3)*t2 + 
                  19*Power(t1,2)*Power(t2,2) + 51*t1*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,3)*Power(t1,2)*
                (156*Power(t1,4) + 467*Power(t1,3)*t2 + 
                  263*Power(t1,2)*Power(t2,2) + 207*t1*Power(t2,3) + 
                  51*Power(t2,4))) + 
            Power(s1,6)*(-4*Power(t1,6)*Power(t2,3)*(5*t1 + t2) + 
               2*Power(s2,8)*(Power(t1,2) + Power(t2,2)) + 
               3*s2*Power(t1,5)*Power(t2,2)*
                (5*Power(t1,2) + 28*t1*t2 + 16*Power(t2,2)) + 
               Power(s2,7)*(-7*Power(t1,3) + 8*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - 14*Power(t2,3)) - 
               Power(s2,2)*Power(t1,4)*t2*
                (20*Power(t1,3) - 11*Power(t1,2)*t2 + 
                  181*t1*Power(t2,2) + 154*Power(t2,3)) + 
               4*Power(s2,6)*
                (Power(t1,4) - 19*Power(t1,3)*t2 - 
                  9*Power(t1,2)*Power(t2,2) - 19*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,5)*t1*
                (10*Power(t1,4) + 188*Power(t1,3)*t2 + 
                  65*Power(t1,2)*Power(t2,2) + 73*t1*Power(t2,3) + 
                  82*Power(t2,4)) - 
               Power(s2,4)*Power(t1,2)*
                (14*Power(t1,4) + 200*Power(t1,3)*t2 - 
                  7*Power(t1,2)*Power(t2,2) + 173*t1*Power(t2,3) + 
                  96*Power(t2,4)) + 
               Power(s2,3)*Power(t1,3)*
                (5*Power(t1,4) + 100*Power(t1,3)*t2 - 
                  79*Power(t1,2)*Power(t2,2) + 299*t1*Power(t2,3) + 
                  138*Power(t2,4))) + 
            Power(s1,4)*Power(t2,2)*
             (2*Power(s2,8)*t1*(3*t1 - 4*t2) + 
               2*Power(t1,6)*Power(t2,3)*(-3*t1 + 2*t2) + 
               15*s2*Power(t1,6)*Power(t2,2)*(3*t1 + 2*t2) + 
               2*Power(s2,7)*
                (7*Power(t1,3) + 42*t1*Power(t2,2) + 8*Power(t2,3)) - 
               Power(s2,2)*Power(t1,4)*t2*
                (92*Power(t1,3) + 79*Power(t1,2)*t2 + 
                  217*t1*Power(t2,2) + 80*Power(t2,3)) - 
               Power(s2,6)*(96*Power(t1,4) + 173*Power(t1,3)*t2 - 
                  7*Power(t1,2)*Power(t2,2) + 200*t1*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,5)*t1*
                (177*Power(t1,4) + 351*Power(t1,3)*t2 + 
                  225*Power(t1,2)*Power(t2,2) + 149*t1*Power(t2,3) + 
                  90*Power(t2,4)) - 
               Power(s2,4)*Power(t1,2)*
                (152*Power(t1,4) + 285*Power(t1,3)*t2 + 
                  534*Power(t1,2)*Power(t2,2) + 285*t1*Power(t2,3) + 
                  152*Power(t2,4)) + 
               Power(s2,3)*Power(t1,3)*
                (51*Power(t1,4) + 207*Power(t1,3)*t2 + 
                  263*Power(t1,2)*Power(t2,2) + 467*t1*Power(t2,3) + 
                  156*Power(t2,4))) + 
            Power(s1,5)*t2*(4*Power(s2,8)*(t1 - t2)*t2 + 
               5*Power(t1,6)*(4*t1 - t2)*Power(t2,3) - 
               s2*Power(t1,5)*Power(t2,2)*
                (48*Power(t1,2) + 109*t1*t2 + 24*Power(t2,2)) - 
               2*Power(s2,7)*(10*Power(t1,3) + Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*Power(t1,4)*t2*
                (50*Power(t1,3) + 56*Power(t1,2)*t2 + 
                  375*t1*Power(t2,2) + 145*Power(t2,3)) + 
               Power(s2,6)*(82*Power(t1,4) + 73*Power(t1,3)*t2 + 
                  65*Power(t1,2)*Power(t2,2) + 188*t1*Power(t2,3) + 
                  10*Power(t2,4)) - 
               6*Power(s2,3)*Power(t1,3)*
                (4*Power(t1,4) + 20*Power(t1,3)*t2 + 
                  8*Power(t1,2)*Power(t2,2) + 107*t1*Power(t2,3) + 
                  32*Power(t2,4)) - 
               2*Power(s2,5)*t1*
                (64*Power(t1,4) + 77*Power(t1,3)*t2 + 
                  94*Power(t1,2)*Power(t2,2) + 77*t1*Power(t2,3) + 
                  64*Power(t2,4)) + 
               Power(s2,4)*Power(t1,2)*
                (90*Power(t1,4) + 149*Power(t1,3)*t2 + 
                  225*Power(t1,2)*Power(t2,2) + 351*t1*Power(t2,3) + 
                  177*Power(t2,4)))) + 
         Power(s,8)*(2*s1*Power(s2,2)*Power(t1,3)*(t1 - t2)*Power(t2,2) + 
            Power(s2,2)*Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*
             (t1 + t2) + Power(s1,2)*
             (2*s2*Power(t1,2)*Power(t2,3)*(-t1 + t2) + 
               Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*(t1 + t2) + 
               Power(s2,2)*(Power(t1,5) + Power(t1,4)*t2 + 
                  4*Power(t1,3)*Power(t2,2) + 4*Power(t1,2)*Power(t2,3) + 
                  t1*Power(t2,4) + Power(t2,5)))) + 
         Power(s,2)*(Power(s2,3)*Power(t1,2)*Power(t2,5)*
             (Power(s2,5) - 2*Power(s2,4)*(t1 + 6*t2) - 
               5*s2*Power(t1,2)*
                (Power(t1,2) + 3*t1*t2 - 6*Power(t2,2)) + 
               8*Power(s2,2)*t1*
                (2*Power(t1,2) - 2*t1*t2 - 5*Power(t2,2)) - 
               2*Power(t1,3)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*(-8*Power(t1,2) + 35*t1*t2 + 15*Power(t2,2))) \
+ Power(s1,8)*(s2*Power(t1,4)*Power(t2,2) + Power(t1,5)*Power(t2,2) - 
               Power(s2,3)*Power(t1,2)*
                (Power(t1,2) + 2*t1*t2 - 4*Power(t2,2)) + 
               Power(s2,5)*(Power(t1,2) + Power(t2,2)) + 
               Power(s2,4)*t1*(-Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(Power(t1,5) + 4*Power(t1,3)*Power(t2,2))) + 
            Power(s1,7)*(-2*Power(t1,5)*Power(t2,2)*(6*t1 + t2) + 
               s2*Power(t1,4)*Power(t2,2)*(5*t1 + 19*t2) + 
               7*Power(s2,6)*(Power(t1,2) + Power(t2,2)) + 
               Power(s2,5)*(-15*Power(t1,3) + 21*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,3)*Power(t1,2)*
                (23*Power(t1,3) + 45*Power(t1,2)*t2 + 
                  26*t1*Power(t2,2) - 4*Power(t2,3)) - 
               3*Power(s2,4)*t1*
                (Power(t1,3) + 20*Power(t1,2)*t2 - t1*Power(t2,2) + 
                  8*Power(t2,3)) - 
               2*Power(s2,2)*Power(t1,3)*
                (6*Power(t1,3) + 3*Power(t1,2)*t2 + 4*t1*Power(t2,2) + 
                  16*Power(t2,3))) + 
            s1*Power(s2,2)*Power(t1,2)*Power(t2,4)*
             (Power(s2,6) - 6*Power(t1,3)*Power(t1 - t2,2)*t2 + 
               Power(s2,5)*(19*t1 + 5*t2) - 
               2*Power(s2,4)*
                (16*Power(t1,2) + 58*t1*t2 + 25*Power(t2,2)) + 
               2*Power(s2,3)*
                (4*Power(t1,3) + 65*Power(t1,2)*t2 + 
                  103*t1*Power(t2,2) + 15*Power(t2,3)) + 
               s2*Power(t1,2)*
                (5*Power(t1,3) - 27*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
                  45*Power(t2,3)) - 
               Power(s2,2)*t1*
                (Power(t1,3) - 14*Power(t1,2)*t2 + 165*t1*Power(t2,2) + 
                  72*Power(t2,3))) + 
            Power(s1,6)*(Power(t1,5)*Power(t2,2)*
                (15*Power(t1,2) + 35*t1*t2 - 8*Power(t2,2)) + 
               7*Power(s2,7)*(Power(t1,2) + Power(t2,2)) - 
               2*s2*Power(t1,4)*Power(t2,2)*
                (25*Power(t1,2) + 58*t1*t2 + 16*Power(t2,2)) + 
               Power(s2,6)*(-22*Power(t1,3) + 34*Power(t1,2)*t2 + 
                  34*t1*Power(t2,2) - 22*Power(t2,3)) + 
               Power(s2,5)*(3*Power(t1,4) - 192*Power(t1,3)*t2 - 
                  79*Power(t1,2)*Power(t2,2) - 140*t1*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s2,3)*Power(t1,2)*
                (50*Power(t1,4) + 196*Power(t1,3)*t2 - 
                  34*Power(t1,2)*Power(t2,2) + 319*t1*Power(t2,3) + 
                  26*Power(t2,4)) + 
               Power(s2,2)*Power(t1,3)*
                (15*Power(t1,4) + 45*Power(t1,3)*t2 - 
                  56*Power(t1,2)*Power(t2,2) + 335*t1*Power(t2,3) + 
                  52*Power(t2,4)) + 
               Power(s2,4)*t1*
                (47*Power(t1,4) + 309*Power(t1,3)*t2 + 
                  127*Power(t1,2)*Power(t2,2) + 77*t1*Power(t2,3) + 
                  93*Power(t2,4))) + 
            Power(s1,2)*s2*Power(t2,3)*
             (-6*Power(t1,5)*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s2,7)*(4*Power(t1,2) + Power(t2,2)) - 
               2*Power(s2,6)*
                (16*Power(t1,3) + 4*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
                  6*Power(t2,3)) + 
               s2*Power(t1,4)*t2*
                (55*Power(t1,3) - 43*Power(t1,2)*t2 - 
                  43*t1*Power(t2,2) + 55*Power(t2,3)) + 
               Power(s2,5)*(52*Power(t1,4) + 335*Power(t1,3)*t2 - 
                  56*Power(t1,2)*Power(t2,2) + 45*t1*Power(t2,3) + 
                  15*Power(t2,4)) - 
               Power(s2,4)*t1*
                (56*Power(t1,4) + 571*Power(t1,3)*t2 + 
                  365*Power(t1,2)*Power(t2,2) + 38*t1*Power(t2,3) + 
                  40*Power(t2,4)) + 
               Power(s2,3)*Power(t1,2)*
                (64*Power(t1,4) + 326*Power(t1,3)*t2 + 
                  477*Power(t1,2)*Power(t2,2) + 180*t1*Power(t2,3) + 
                  81*Power(t2,4)) - 
               Power(s2,2)*Power(t1,3)*
                (32*Power(t1,4) + 137*Power(t1,3)*t2 + 
                  29*Power(t1,2)*Power(t2,2) + 104*t1*Power(t2,3) + 
                  112*Power(t2,4))) + 
            Power(s1,3)*Power(t2,2)*
             (Power(s2,8)*(4*Power(t1,2) - 2*t1*t2 - Power(t2,2)) - 
               2*Power(t1,5)*Power(t2,3)*
                (3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*Power(t1,4)*Power(t2,2)*
                (45*Power(t1,3) + 3*Power(t1,2)*t2 - 
                  27*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,7)*(-4*Power(t1,3) + 26*Power(t1,2)*t2 + 
                  45*t1*Power(t2,2) + 23*Power(t2,3)) - 
               Power(s2,2)*Power(t1,3)*t2*
                (112*Power(t1,4) + 104*Power(t1,3)*t2 + 
                  29*Power(t1,2)*Power(t2,2) + 137*t1*Power(t2,3) + 
                  32*Power(t2,4)) - 
               Power(s2,6)*(26*Power(t1,4) + 319*Power(t1,3)*t2 - 
                  34*Power(t1,2)*Power(t2,2) + 196*t1*Power(t2,3) + 
                  50*Power(t2,4)) + 
               Power(s2,5)*(100*Power(t1,5) + 521*Power(t1,4)*t2 + 
                  791*Power(t1,3)*Power(t2,2) - 
                  98*Power(t1,2)*Power(t2,3) + 210*t1*Power(t2,4) + 
                  10*Power(t2,5)) - 
               Power(s2,4)*t1*
                (126*Power(t1,5) + 409*Power(t1,4)*t2 + 
                  1176*Power(t1,3)*Power(t2,2) + 
                  262*Power(t1,2)*Power(t2,3) + 229*t1*Power(t2,4) + 
                  36*Power(t2,5)) + 
               Power(s2,3)*Power(t1,2)*
                (52*Power(t1,5) + 295*Power(t1,4)*t2 + 
                  414*Power(t1,3)*Power(t2,2) + 
                  414*Power(t1,2)*Power(t2,3) + 295*t1*Power(t2,4) + 
                  52*Power(t2,5))) + 
            Power(s1,5)*(-8*Power(t1,5)*Power(t2,3)*
                (5*Power(t1,2) + 2*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,8)*(Power(t1,2) + Power(t2,2)) + 
               Power(s2,7)*(-5*Power(t1,3) + 11*Power(t1,2)*t2 + 
                  21*t1*Power(t2,2) - 15*Power(t2,3)) + 
               2*s2*Power(t1,4)*Power(t2,2)*
                (15*Power(t1,3) + 103*Power(t1,2)*t2 + 
                  65*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,6)*(Power(t1,4) - 140*Power(t1,3)*t2 - 
                  79*Power(t1,2)*Power(t2,2) - 192*t1*Power(t2,3) + 
                  3*Power(t2,4)) - 
               Power(s2,2)*Power(t1,3)*t2*
                (40*Power(t1,4) + 38*Power(t1,3)*t2 + 
                  365*Power(t1,2)*Power(t2,2) + 571*t1*Power(t2,3) + 
                  56*Power(t2,4)) + 
               Power(s2,5)*(19*Power(t1,5) + 373*Power(t1,4)*t2 + 
                  217*Power(t1,3)*Power(t2,2) + 
                  217*Power(t1,2)*Power(t2,3) + 373*t1*Power(t2,4) + 
                  19*Power(t2,5)) - 
               2*Power(s2,4)*t1*
                (13*Power(t1,5) + 207*Power(t1,4)*t2 + 
                  21*Power(t1,3)*Power(t2,2) + 
                  364*Power(t1,2)*Power(t2,3) + 88*t1*Power(t2,4) + 
                  77*Power(t2,5)) + 
               Power(s2,3)*Power(t1,2)*
                (10*Power(t1,5) + 210*Power(t1,4)*t2 - 
                  98*Power(t1,3)*Power(t2,2) + 
                  791*Power(t1,2)*Power(t2,3) + 521*t1*Power(t2,4) + 
                  100*Power(t2,5))) + 
            Power(s1,4)*t2*(5*Power(t1,5)*Power(t2,3)*
                (6*Power(t1,2) - 3*t1*t2 - Power(t2,2)) + 
               Power(s2,8)*(Power(t1,2) + 2*t1*t2 - Power(t2,2)) - 
               s2*Power(t1,4)*Power(t2,2)*
                (72*Power(t1,3) + 165*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + Power(t2,3)) - 
               3*Power(s2,7)*(8*Power(t1,3) - Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,6)*(93*Power(t1,4) + 77*Power(t1,3)*t2 + 
                  127*Power(t1,2)*Power(t2,2) + 309*t1*Power(t2,3) + 
                  47*Power(t2,4)) + 
               Power(s2,2)*Power(t1,3)*t2*
                (81*Power(t1,4) + 180*Power(t1,3)*t2 + 
                  477*Power(t1,2)*Power(t2,2) + 326*t1*Power(t2,3) + 
                  64*Power(t2,4)) - 
               2*Power(s2,5)*(77*Power(t1,5) + 88*Power(t1,4)*t2 + 
                  364*Power(t1,3)*Power(t2,2) + 
                  21*Power(t1,2)*Power(t2,3) + 207*t1*Power(t2,4) + 
                  13*Power(t2,5)) + 
               2*Power(s2,4)*t1*
                (60*Power(t1,5) + 121*Power(t1,4)*t2 + 
                  390*Power(t1,3)*Power(t2,2) + 
                  390*Power(t1,2)*Power(t2,3) + 121*t1*Power(t2,4) + 
                  60*Power(t2,5)) - 
               Power(s2,3)*Power(t1,2)*
                (36*Power(t1,5) + 229*Power(t1,4)*t2 + 
                  262*Power(t1,3)*Power(t2,2) + 
                  1176*Power(t1,2)*Power(t2,3) + 409*t1*Power(t2,4) + 
                  126*Power(t2,5)))) + 
         Power(s,7)*(Power(s2,2)*Power(t1,2)*(t1 - t2)*Power(t2,2)*
             (-3*s2*Power(t1,2) + 2*Power(t1,3) + s2*t1*t2 + 
               4*Power(t1,2)*t2 + 6*s2*Power(t2,2) - 4*t1*Power(t2,2) - 
               2*Power(t2,3)) - 
            s1*Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (2*Power(t1,3) - 9*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
               5*Power(t2,3) + 
               s2*(9*Power(t1,2) - 16*t1*t2 + Power(t2,2))) + 
            Power(s1,3)*(-(s2*Power(t1,2)*Power(t2,2)*
                  (Power(t1,2) - 16*t1*t2 + 9*Power(t2,2))) + 
               Power(t1,2)*Power(t2,2)*
                (-6*Power(t1,3) + 5*Power(t1,2)*t2 + 4*t1*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(s2,3)*(Power(t1,4) + 8*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) + 8*t1*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s2,2)*(6*Power(t1,5) + 5*Power(t1,4)*t2 + 
                  22*Power(t1,3)*Power(t2,2) + 
                  6*Power(t1,2)*Power(t2,3) + 6*t1*Power(t2,4) + 
                  3*Power(t2,5))) - 
            Power(s1,2)*(-2*Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*
                (Power(t1,2) + 3*t1*t2 + Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (5*Power(t1,3) + 2*Power(t1,2)*t2 - 9*t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,3)*(3*Power(t1,5) + 6*Power(t1,4)*t2 + 
                  6*Power(t1,3)*Power(t2,2) + 
                  22*Power(t1,2)*Power(t2,3) + 5*t1*Power(t2,4) + 
                  6*Power(t2,5)) - 
               Power(s2,2)*(2*Power(t1,6) + 6*Power(t1,5)*t2 - 
                  11*Power(t1,4)*Power(t2,2) + 
                  70*Power(t1,3)*Power(t2,3) - 
                  11*Power(t1,2)*Power(t2,4) + 6*t1*Power(t2,5) + 
                  2*Power(t2,6)))) + 
         Power(s,3)*(Power(s1,7)*
             (Power(t1,4)*Power(t2,2)*(-6*t1 + t2) + 
               s2*Power(t1,3)*Power(t2,2)*(-5*t1 + 8*t2) - 
               3*Power(s2,5)*(Power(t1,2) + Power(t2,2)) - 
               Power(s2,2)*Power(t1,2)*
                (6*Power(t1,3) + Power(t1,2)*t2 + 22*t1*Power(t2,2) - 
                  6*Power(t2,3)) + 
               Power(s2,4)*(4*Power(t1,3) - 9*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*t1*
                (5*Power(t1,3) + 16*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
                  8*Power(t2,3))) + 
            Power(s2,2)*Power(t1,2)*Power(t2,4)*
             (Power(s2,5)*(t1 - 6*t2) + 
               2*Power(t1,3)*Power(t1 - t2,2)*t2 + 
               Power(s2,4)*(-8*Power(t1,2) + 10*t1*t2 + 
                  30*Power(t2,2)) + 
               Power(s2,3)*(8*Power(t1,3) + 32*Power(t1,2)*t2 - 
                  70*t1*Power(t2,2) - 20*Power(t2,3)) - 
               5*s2*Power(t1,2)*
                (Power(t1,3) - 2*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
                  4*Power(t2,3)) + 
               4*Power(s2,2)*t1*
                (Power(t1,3) - 12*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
                  10*Power(t2,3))) + 
            Power(s1,6)*(2*Power(t1,4)*Power(t2,2)*
                (15*Power(t1,2) + 5*t1*t2 - 4*Power(t2,2)) - 
               8*Power(s2,6)*(Power(t1,2) + Power(t2,2)) - 
               s2*Power(t1,3)*Power(t2,2)*
                (25*Power(t1,2) + 78*t1*t2 + 8*Power(t2,2)) + 
               Power(s2,5)*(21*Power(t1,3) - 40*Power(t1,2)*t2 - 
                  33*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*
                (30*Power(t1,4) + 30*Power(t1,3)*t2 - 
                  11*Power(t1,2)*Power(t2,2) + 179*t1*Power(t2,3) - 
                  18*Power(t2,4)) + 
               2*Power(s2,4)*
                (6*Power(t1,4) + 82*Power(t1,3)*t2 + 
                  30*Power(t1,2)*Power(t2,2) + 56*t1*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s2,3)*t1*
                (55*Power(t1,4) + 146*Power(t1,3)*t2 + 
                  129*Power(t1,2)*Power(t2,2) - 9*t1*Power(t2,3) + 
                  38*Power(t2,4))) + 
            s1*Power(s2,2)*Power(t1,2)*Power(t2,3)*
             (Power(s2,5)*(8*t1 - 5*t2) - 
               Power(s2,4)*(8*Power(t1,2) + 78*t1*t2 + 25*Power(t2,2)) + 
               2*Power(s2,3)*t2*
                (31*Power(t1,2) + 112*t1*t2 + 45*Power(t2,2)) + 
               Power(t1,2)*t2*
                (-20*Power(t1,3) + 27*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) - 15*Power(t2,3)) - 
               2*Power(s2,2)*
                (4*Power(t1,4) - 19*Power(t1,3)*t2 + 
                  51*Power(t1,2)*Power(t2,2) + 122*t1*Power(t2,3) + 
                  15*Power(t2,4)) + 
               s2*t1*(8*Power(t1,4) + 3*Power(t1,3)*t2 - 
                  100*Power(t1,2)*Power(t2,2) + 111*t1*Power(t2,3) + 
                  48*Power(t2,4))) - 
            Power(s1,2)*Power(t2,2)*
             (-2*Power(t1,5)*Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,7)*(-6*Power(t1,3) + 22*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + 6*Power(t2,3)) + 
               s2*Power(t1,4)*Power(t2,2)*
                (15*Power(t1,3) - 8*Power(t1,2)*t2 - 
                  27*t1*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,6)*(18*Power(t1,4) - 179*Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) - 30*t1*Power(t2,3) - 
                  30*Power(t2,4)) - 
               Power(s2,2)*Power(t1,3)*t2*
                (64*Power(t1,4) + 39*Power(t1,3)*t2 - 
                  134*Power(t1,2)*Power(t2,2) + 39*t1*Power(t2,3) + 
                  64*Power(t2,4)) + 
               Power(s2,5)*(2*Power(t1,5) + 211*Power(t1,4)*t2 + 
                  678*Power(t1,3)*Power(t2,2) - 
                  86*Power(t1,2)*Power(t2,3) + 90*t1*Power(t2,4) + 
                  20*Power(t2,5)) - 
               Power(s2,4)*t1*
                (34*Power(t1,5) + 141*Power(t1,4)*t2 + 
                  738*Power(t1,3)*Power(t2,2) + 
                  399*Power(t1,2)*Power(t2,3) + 104*t1*Power(t2,4) + 
                  40*Power(t2,5)) + 
               Power(s2,3)*Power(t1,2)*
                (20*Power(t1,5) + 151*Power(t1,4)*t2 + 
                  133*Power(t1,3)*Power(t2,2) + 
                  195*Power(t1,2)*Power(t2,3) + 201*t1*Power(t2,4) + 
                  64*Power(t2,5))) - 
            Power(s1,5)*(3*Power(s2,7)*(Power(t1,2) + Power(t2,2)) - 
               2*s2*Power(t1,4)*Power(t2,2)*
                (45*Power(t1,2) + 112*t1*t2 + 31*Power(t2,2)) + 
               Power(s2,6)*(-14*Power(t1,3) + 33*Power(t1,2)*t2 + 
                  40*t1*Power(t2,2) - 21*Power(t2,3)) + 
               2*Power(t1,4)*Power(t2,2)*
                (10*Power(t1,3) + 35*Power(t1,2)*t2 - 
                  16*t1*Power(t2,2) - 4*Power(t2,3)) - 
               Power(s2,5)*(7*Power(t1,4) + 276*Power(t1,3)*t2 + 
                  170*Power(t1,2)*Power(t2,2) + 276*t1*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (20*Power(t1,5) + 90*Power(t1,4)*t2 - 
                  86*Power(t1,3)*Power(t2,2) + 
                  678*Power(t1,2)*Power(t2,3) + 211*t1*Power(t2,4) + 
                  2*Power(t2,5)) + 
               Power(s2,4)*(68*Power(t1,5) + 495*Power(t1,4)*t2 + 
                  301*Power(t1,3)*Power(t2,2) + 
                  187*Power(t1,2)*Power(t2,3) + 328*t1*Power(t2,4) + 
                  17*Power(t2,5)) - 
               Power(s2,3)*t1*
                (70*Power(t1,5) + 344*Power(t1,4)*t2 - 
                  96*Power(t1,3)*Power(t2,2) + 
                  892*Power(t1,2)*Power(t2,3) - 11*t1*Power(t2,4) + 
                  76*Power(t2,5))) + 
            Power(s1,3)*t2*(-5*Power(t1,4)*Power(t2,3)*
                (4*Power(t1,3) - 3*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,7)*(8*Power(t1,3) - 6*Power(t1,2)*t2 + 
                  16*t1*Power(t2,2) + 5*Power(t2,3)) + 
               s2*Power(t1,3)*Power(t2,2)*
                (48*Power(t1,4) + 111*Power(t1,3)*t2 - 
                  100*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  8*Power(t2,4)) - 
               Power(s2,6)*(38*Power(t1,4) - 9*Power(t1,3)*t2 + 
                  129*Power(t1,2)*Power(t2,2) + 146*t1*Power(t2,3) + 
                  55*Power(t2,4)) - 
               Power(s2,2)*Power(t1,2)*t2*
                (64*Power(t1,5) + 201*Power(t1,4)*t2 + 
                  195*Power(t1,3)*Power(t2,2) + 
                  133*Power(t1,2)*Power(t2,3) + 151*t1*Power(t2,4) + 
                  20*Power(t2,5)) + 
               Power(s2,5)*(76*Power(t1,5) - 11*Power(t1,4)*t2 + 
                  892*Power(t1,3)*Power(t2,2) - 
                  96*Power(t1,2)*Power(t2,3) + 344*t1*Power(t2,4) + 
                  70*Power(t2,5)) - 
               Power(s2,4)*(70*Power(t1,6) + 119*Power(t1,5)*t2 + 
                  878*Power(t1,4)*Power(t2,2) + 
                  1128*Power(t1,3)*Power(t2,3) - 
                  48*Power(t1,2)*Power(t2,4) + 220*t1*Power(t2,5) + 
                  10*Power(t2,6)) + 
               Power(s2,3)*t1*
                (24*Power(t1,6) + 191*Power(t1,5)*t2 + 
                  324*Power(t1,4)*Power(t2,2) + 
                  1078*Power(t1,3)*Power(t2,3) + 
                  324*Power(t1,2)*Power(t2,4) + 191*t1*Power(t2,5) + 
                  24*Power(t2,6))) + 
            Power(s1,4)*(4*Power(t1,4)*Power(t2,3)*
                (10*Power(t1,3) + 6*Power(t1,2)*t2 - 12*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,7)*(Power(t1,3) - 6*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) + 4*Power(t2,3)) - 
               2*s2*Power(t1,3)*Power(t2,2)*
                (15*Power(t1,4) + 122*Power(t1,3)*t2 + 
                  51*Power(t1,2)*Power(t2,2) - 19*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               2*Power(s2,6)*(Power(t1,4) + 56*Power(t1,3)*t2 + 
                  30*Power(t1,2)*Power(t2,2) + 82*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (40*Power(t1,5) + 104*Power(t1,4)*t2 + 
                  399*Power(t1,3)*Power(t2,2) + 
                  738*Power(t1,2)*Power(t2,3) + 141*t1*Power(t2,4) + 
                  34*Power(t2,5)) - 
               Power(s2,5)*(17*Power(t1,5) + 328*Power(t1,4)*t2 + 
                  187*Power(t1,3)*Power(t2,2) + 
                  301*Power(t1,2)*Power(t2,3) + 495*t1*Power(t2,4) + 
                  68*Power(t2,5)) + 
               2*Power(s2,4)*(12*Power(t1,6) + 201*Power(t1,5)*t2 - 
                  10*Power(t1,4)*Power(t2,2) + 
                  698*Power(t1,3)*Power(t2,3) - 
                  10*Power(t1,2)*Power(t2,4) + 201*t1*Power(t2,5) + 
                  12*Power(t2,6)) - 
               Power(s2,3)*t1*
                (10*Power(t1,6) + 220*Power(t1,5)*t2 - 
                  48*Power(t1,4)*Power(t2,2) + 
                  1128*Power(t1,3)*Power(t2,3) + 
                  878*Power(t1,2)*Power(t2,4) + 119*t1*Power(t2,5) + 
                  70*Power(t2,6)))) + 
         Power(s,6)*(s1*Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (-10*Power(t1,4) + 4*Power(t1,3)*t2 + 
               34*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) - 
               8*Power(t2,4) + 
               Power(s2,2)*(15*Power(t1,2) - 44*t1*t2 + 5*Power(t2,2)) + 
               s2*(Power(t1,3) - 19*Power(t1,2)*t2 + 27*t1*Power(t2,2) + 
                  25*Power(t2,3))) + 
            Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (Power(t1 - t2,2)*
                (Power(t1,3) + 9*Power(t1,2)*t2 + 9*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,2)*(3*Power(t1,3) - 6*Power(t1,2)*t2 - 
                  10*t1*Power(t2,2) + 15*Power(t2,3)) - 
               2*s2*(2*Power(t1,4) + 3*Power(t1,3)*t2 - 
                  16*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
                  6*Power(t2,4))) - 
            Power(s1,4)*(s2*Power(t1,2)*Power(t2,2)*
                (-5*Power(t1,2) + 44*t1*t2 - 15*Power(t2,2)) + 
               Power(t1,2)*Power(t2,2)*
                (-15*Power(t1,3) + 10*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,4)*(Power(t1,3) - 3*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(5*Power(t1,4) + 34*Power(t1,3)*t2 + 
                  24*Power(t1,2)*Power(t2,2) + 32*t1*Power(t2,3) + 
                  3*Power(t2,4)) - 
               Power(s2,2)*(15*Power(t1,5) + 10*Power(t1,4)*t2 + 
                  52*Power(t1,3)*Power(t2,2) - 
                  12*Power(t1,2)*Power(t2,3) + 12*t1*Power(t2,4) + 
                  3*Power(t2,5))) - 
            Power(s1,3)*(-(s2*Power(t1,2)*Power(t2,2)*
                  (25*Power(t1,3) + 27*Power(t1,2)*t2 - 
                    19*t1*Power(t2,2) + Power(t2,3))) + 
               2*Power(t1,2)*Power(t2,2)*
                (6*Power(t1,4) + 5*Power(t1,3)*t2 - 
                  16*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,4)*(3*Power(t1,4) + 32*Power(t1,3)*t2 + 
                  24*Power(t1,2)*Power(t2,2) + 34*t1*Power(t2,3) + 
                  5*Power(t2,4)) - 
               Power(s2,3)*(19*Power(t1,5) + 53*Power(t1,4)*t2 + 
                  57*Power(t1,3)*Power(t2,2) + 
                  57*Power(t1,2)*Power(t2,3) + 53*t1*Power(t2,4) + 
                  19*Power(t2,5)) + 
               Power(s2,2)*(12*Power(t1,6) + 30*Power(t1,5)*t2 - 
                  32*Power(t1,4)*Power(t2,2) + 
                  281*Power(t1,3)*Power(t2,3) - 
                  57*Power(t1,2)*Power(t2,4) + 24*t1*Power(t2,5) + 
                  4*Power(t2,6))) + 
            Power(s1,2)*(Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*
                (Power(t1,3) + 9*Power(t1,2)*t2 + 9*t1*Power(t2,2) + 
                  Power(t2,3)) - 
               2*s2*Power(t1,2)*Power(t2,2)*
                (4*Power(t1,4) + 10*Power(t1,3)*t2 - 
                  17*Power(t1,2)*Power(t2,2) - 2*t1*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,4)*(3*Power(t1,5) + 12*Power(t1,4)*t2 - 
                  12*Power(t1,3)*Power(t2,2) + 
                  52*Power(t1,2)*Power(t2,3) + 10*t1*Power(t2,4) + 
                  15*Power(t2,5)) - 
               Power(s2,3)*(4*Power(t1,6) + 24*Power(t1,5)*t2 - 
                  57*Power(t1,4)*Power(t2,2) + 
                  281*Power(t1,3)*Power(t2,3) - 
                  32*Power(t1,2)*Power(t2,4) + 30*t1*Power(t2,5) + 
                  12*Power(t2,6)) + 
               Power(s2,2)*(Power(t1,7) + 9*Power(t1,6)*t2 + 
                  2*Power(t1,5)*Power(t2,2) + 
                  60*Power(t1,4)*Power(t2,3) + 
                  60*Power(t1,3)*Power(t2,4) + 
                  2*Power(t1,2)*Power(t2,5) + 9*t1*Power(t2,6) + 
                  Power(t2,7)))) + 
         Power(s,4)*(Power(s1,6)*
             (Power(t1,3)*Power(t2,2)*
                (15*Power(t1,2) - 5*t1*t2 - Power(t2,2)) + 
               3*Power(s2,5)*(Power(t1,2) + Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (10*Power(t1,2) - 34*t1*t2 + 3*Power(t2,2)) - 
               3*Power(s2,4)*
                (2*Power(t1,3) - 5*Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(s2,3)*(10*Power(t1,4) + 44*Power(t1,3)*t2 + 
                  12*Power(t1,2)*Power(t2,2) + 32*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*t1*
                (15*Power(t1,4) + 5*Power(t1,3)*t2 + 
                  52*Power(t1,2)*Power(t2,2) - 24*t1*Power(t2,3) + 
                  3*Power(t2,4))) - 
            Power(s2,2)*Power(t1,2)*Power(t2,3)*
             (-5*Power(t1,2)*Power(t1 - t2,2)*t2*(t1 + t2) + 
               Power(s2,4)*(Power(t1,2) + 5*t1*t2 - 15*Power(t2,2)) + 
               2*Power(s2,3)*
                (Power(t1,3) - 16*Power(t1,2)*t2 + 10*t1*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(s2,2)*(-7*Power(t1,4) + 24*Power(t1,3)*t2 + 
                  48*Power(t1,2)*Power(t2,2) - 70*t1*Power(t2,3) - 
                  15*Power(t2,4)) + 
               4*s2*t1*(Power(t1,4) + 2*Power(t1,3)*t2 - 
                  12*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) + 
                  5*Power(t2,4))) + 
            Power(s1,5)*(3*Power(s2,6)*(Power(t1,2) + Power(t2,2)) + 
               Power(s2,5)*(-13*Power(t1,3) + 33*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) - 13*Power(t2,3)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (50*Power(t1,3) + 122*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - 3*Power(t2,3)) - 
               2*Power(t1,3)*Power(t2,2)*
                (20*Power(t1,3) + 10*Power(t1,2)*t2 - 
                  16*t1*Power(t2,2) + Power(t2,3)) - 
               Power(s2,4)*(18*Power(t1,4) + 212*Power(t1,3)*t2 + 
                  129*Power(t1,2)*Power(t2,2) + 186*t1*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(s2,3)*(70*Power(t1,5) + 214*Power(t1,4)*t2 + 
                  217*Power(t1,3)*Power(t2,2) + 
                  7*Power(t1,2)*Power(t2,3) + 119*t1*Power(t2,4) + 
                  7*Power(t2,5)) - 
               Power(s2,2)*t1*
                (40*Power(t1,5) + 60*Power(t1,4)*t2 - 
                  46*Power(t1,3)*Power(t2,2) + 
                  409*Power(t1,2)*Power(t2,3) - 71*t1*Power(t2,4) + 
                  12*Power(t2,5))) + 
            s1*Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (Power(s2,4)*(3*Power(t1,2) - 34*t1*t2 + 10*Power(t2,2)) + 
               Power(s2,3)*(-3*Power(t1,3) + 15*Power(t1,2)*t2 + 
                  122*t1*Power(t2,2) + 50*Power(t2,3)) + 
               Power(s2,2)*(-3*Power(t1,4) + 4*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) - 216*t1*Power(t2,3) - 
                  85*Power(t2,4)) - 
               2*t1*t2*(10*Power(t1,4) + Power(t1,3)*t2 - 
                  31*Power(t1,2)*Power(t2,2) + 14*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s2*(3*Power(t1,5) + 35*Power(t1,4)*t2 - 
                  100*Power(t1,3)*Power(t2,2) + 
                  6*Power(t1,2)*Power(t2,3) + 141*t1*Power(t2,4) + 
                  15*Power(t2,5))) + 
            Power(s1,4)*(-3*Power(s2,6)*
                (Power(t1,3) - 4*Power(t1,2)*t2 - 5*t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (-85*Power(t1,4) - 216*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(t1,3)*Power(t2,2)*
                (15*Power(t1,4) + 70*Power(t1,3)*t2 - 
                  48*Power(t1,2)*Power(t2,2) - 24*t1*Power(t2,3) + 
                  7*Power(t2,4)) - 
               Power(s2,5)*(9*Power(t1,4) + 186*Power(t1,3)*t2 + 
                  129*Power(t1,2)*Power(t2,2) + 212*t1*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s2,4)*(52*Power(t1,5) + 381*Power(t1,4)*t2 + 
                  239*Power(t1,3)*Power(t2,2) + 
                  239*Power(t1,2)*Power(t2,3) + 381*t1*Power(t2,4) + 
                  52*Power(t2,5)) - 
               Power(s2,3)*(55*Power(t1,6) + 306*Power(t1,5)*t2 - 
                  153*Power(t1,4)*Power(t2,2) + 
                  1252*Power(t1,3)*Power(t2,3) - 
                  139*Power(t1,2)*Power(t2,4) + 182*t1*Power(t2,5) + 
                  11*Power(t2,6)) + 
               Power(s2,2)*t1*
                (15*Power(t1,6) + 90*Power(t1,5)*t2 - 
                  53*Power(t1,4)*Power(t2,2) + 
                  698*Power(t1,3)*Power(t2,3) + 
                  319*Power(t1,2)*Power(t2,4) - 4*t1*Power(t2,5) + 
                  15*Power(t2,6))) + 
            Power(s1,2)*t2*(5*Power(t1,4)*Power(t1 - t2,2)*Power(t2,3)*
                (t1 + t2) - 2*s2*Power(t1,3)*Power(t2,2)*
                (6*Power(t1,4) + 14*Power(t1,3)*t2 - 
                  31*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,6)*(3*Power(t1,4) - 24*Power(t1,3)*t2 + 
                  52*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
                  15*Power(t2,4)) + 
               2*Power(s2,2)*Power(t1,2)*t2*
                (13*Power(t1,5) + 49*Power(t1,4)*t2 - 
                  27*Power(t1,3)*Power(t2,2) - 
                  27*Power(t1,2)*Power(t2,3) + 49*t1*Power(t2,4) + 
                  13*Power(t2,5)) - 
               Power(s2,5)*(12*Power(t1,5) - 71*Power(t1,4)*t2 + 
                  409*Power(t1,3)*Power(t2,2) - 
                  46*Power(t1,2)*Power(t2,3) + 60*t1*Power(t2,4) + 
                  40*Power(t2,5)) + 
               Power(s2,4)*(15*Power(t1,6) - 4*Power(t1,5)*t2 + 
                  319*Power(t1,4)*Power(t2,2) + 
                  698*Power(t1,3)*Power(t2,3) - 
                  53*Power(t1,2)*Power(t2,4) + 90*t1*Power(t2,5) + 
                  15*Power(t2,6)) - 
               Power(s2,3)*t1*
                (6*Power(t1,6) + 69*Power(t1,5)*t2 + 
                  105*Power(t1,4)*Power(t2,2) + 
                  361*Power(t1,3)*Power(t2,3) + 
                  219*Power(t1,2)*Power(t2,4) + 96*t1*Power(t2,5) + 
                  20*Power(t2,6))) - 
            Power(s1,3)*(4*Power(t1,3)*Power(t2,3)*
                (5*Power(t1,4) + 4*Power(t1,3)*t2 - 
                  12*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,6)*(Power(t1,4) + 32*Power(t1,3)*t2 + 
                  12*Power(t1,2)*Power(t2,2) + 44*t1*Power(t2,3) + 
                  10*Power(t2,4)) - 
               s2*Power(t1,2)*Power(t2,2)*
                (15*Power(t1,5) + 141*Power(t1,4)*t2 + 
                  6*Power(t1,3)*Power(t2,2) - 
                  100*Power(t1,2)*Power(t2,3) + 35*t1*Power(t2,4) + 
                  3*Power(t2,5)) - 
               Power(s2,5)*(7*Power(t1,5) + 119*Power(t1,4)*t2 + 
                  7*Power(t1,3)*Power(t2,2) + 
                  217*Power(t1,2)*Power(t2,3) + 214*t1*Power(t2,4) + 
                  70*Power(t2,5)) + 
               Power(s2,2)*t1*t2*
                (20*Power(t1,6) + 96*Power(t1,5)*t2 + 
                  219*Power(t1,4)*Power(t2,2) + 
                  361*Power(t1,3)*Power(t2,3) + 
                  105*Power(t1,2)*Power(t2,4) + 69*t1*Power(t2,5) + 
                  6*Power(t2,6)) + 
               Power(s2,4)*(11*Power(t1,6) + 182*Power(t1,5)*t2 - 
                  139*Power(t1,4)*Power(t2,2) + 
                  1252*Power(t1,3)*Power(t2,3) - 
                  153*Power(t1,2)*Power(t2,4) + 306*t1*Power(t2,5) + 
                  55*Power(t2,6)) - 
               Power(s2,3)*(5*Power(t1,7) + 115*Power(t1,6)*t2 - 
                  13*Power(t1,5)*Power(t2,2) + 
                  818*Power(t1,4)*Power(t2,3) + 
                  818*Power(t1,3)*Power(t2,4) - 
                  13*Power(t1,2)*Power(t2,5) + 115*t1*Power(t2,6) + 
                  5*Power(t2,7)))) - 
         Power(s,5)*(s1*Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (6*Power(t1,5) + 27*Power(t1,4)*t2 - 
               54*Power(t1,3)*Power(t2,2) - 14*Power(t1,2)*Power(t2,3) + 
               32*t1*Power(t2,4) + 3*Power(t2,5) + 
               Power(s2,3)*(11*Power(t1,2) - 56*t1*t2 + 10*Power(t2,2)) + 
               Power(s2,2)*(-4*Power(t1,3) - 3*Power(t1,2)*t2 + 
                  88*t1*Power(t2,2) + 50*Power(t2,3)) + 
               s2*(-13*Power(t1,4) + 8*Power(t1,3)*t2 + 
                  70*Power(t1,2)*Power(t2,2) - 104*t1*Power(t2,3) - 
                  41*Power(t2,4))) + 
            Power(s1,5)*(Power(s2,5)*(Power(t1,2) + Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (10*Power(t1,2) - 56*t1*t2 + 11*Power(t2,2)) + 
               Power(s2,4)*(-4*Power(t1,3) + 11*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(t1,2)*Power(t2,2)*
                (20*Power(t1,3) - 10*Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(s2,3)*(10*Power(t1,4) + 56*Power(t1,3)*t2 + 
                  32*Power(t1,2)*Power(t2,2) + 48*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,2)*(20*Power(t1,5) + 10*Power(t1,4)*t2 + 
                  68*Power(t1,3)*Power(t2,2) - 
                  32*Power(t1,2)*Power(t2,3) + 10*t1*Power(t2,4) + 
                  Power(t2,5))) + 
            Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (-4*t1*Power(t1 - t2,2)*t2*
                (Power(t1,2) + 3*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(Power(t1,3) - 4*Power(t1,2)*t2 - 
                  10*t1*Power(t2,2) + 20*Power(t2,3)) - 
               2*Power(s2,2)*(Power(t1,4) + 3*Power(t1,3)*t2 - 
                  24*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) + 
                  15*Power(t2,4)) + 
               s2*(Power(t1,5) + 14*Power(t1,4)*t2 - 
                  24*Power(t1,3)*Power(t2,2) - 
                  32*Power(t1,2)*Power(t2,3) + 35*t1*Power(t2,4) + 
                  6*Power(t2,5))) - 
            Power(s1,4)*(Power(s2,5)*
                (3*Power(t1,3) - 10*Power(t1,2)*t2 - 11*t1*Power(t2,2) + 
                  4*Power(t2,3)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (-50*Power(t1,3) - 88*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 4*Power(t2,3)) + 
               2*Power(t1,2)*Power(t2,2)*
                (15*Power(t1,4) + 10*Power(t1,3)*t2 - 
                  24*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               12*Power(s2,4)*
                (Power(t1,4) + 11*Power(t1,3)*t2 + 
                  8*Power(t1,2)*Power(t2,2) + 11*t1*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s2,3)*(50*Power(t1,5) + 156*Power(t1,4)*t2 + 
                  167*Power(t1,3)*Power(t2,2) + 
                  47*Power(t1,2)*Power(t2,3) + 129*t1*Power(t2,4) + 
                  20*Power(t2,5)) + 
               Power(s2,2)*(30*Power(t1,6) + 60*Power(t1,5)*t2 - 
                  54*Power(t1,4)*Power(t2,2) + 
                  475*Power(t1,3)*Power(t2,3) - 
                  99*Power(t1,2)*Power(t2,4) + 30*t1*Power(t2,5) + 
                  2*Power(t2,6))) + 
            Power(s1,3)*(s2*Power(t1,2)*Power(t2,2)*
                (-41*Power(t1,4) - 104*Power(t1,3)*t2 + 
                  70*Power(t1,2)*Power(t2,2) + 8*t1*Power(t2,3) - 
                  13*Power(t2,4)) - 
               Power(s2,5)*(3*Power(t1,4) + 48*Power(t1,3)*t2 + 
                  32*Power(t1,2)*Power(t2,2) + 56*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(t1,2)*Power(t2,2)*
                (6*Power(t1,5) + 35*Power(t1,4)*t2 - 
                  32*Power(t1,3)*Power(t2,2) - 
                  24*Power(t1,2)*Power(t2,3) + 14*t1*Power(t2,4) + 
                  Power(t2,5)) + 
               Power(s2,4)*(20*Power(t1,5) + 129*Power(t1,4)*t2 + 
                  47*Power(t1,3)*Power(t2,2) + 
                  167*Power(t1,2)*Power(t2,3) + 156*t1*Power(t2,4) + 
                  50*Power(t2,5)) - 
               Power(s2,3)*(23*Power(t1,6) + 136*Power(t1,5)*t2 - 
                  133*Power(t1,4)*Power(t2,2) + 
                  900*Power(t1,3)*Power(t2,3) - 
                  133*Power(t1,2)*Power(t2,4) + 136*t1*Power(t2,5) + 
                  23*Power(t2,6)) + 
               Power(s2,2)*(6*Power(t1,7) + 45*Power(t1,6)*t2 - 
                  10*Power(t1,5)*Power(t2,2) + 
                  355*Power(t1,4)*Power(t2,3) + 
                  217*Power(t1,3)*Power(t2,4) - 
                  8*Power(t1,2)*Power(t2,5) + 24*t1*Power(t2,6) + 
                  Power(t2,7))) + 
            Power(s1,2)*(-4*Power(t1,3)*Power(t1 - t2,2)*Power(t2,3)*
                (Power(t1,2) + 3*t1*t2 + Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (3*Power(t1,5) + 32*Power(t1,4)*t2 - 
                  14*Power(t1,3)*Power(t2,2) - 
                  54*Power(t1,2)*Power(t2,3) + 27*t1*Power(t2,4) + 
                  6*Power(t2,5)) + 
               Power(s2,5)*(Power(t1,5) + 10*Power(t1,4)*t2 - 
                  32*Power(t1,3)*Power(t2,2) + 
                  68*Power(t1,2)*Power(t2,3) + 10*t1*Power(t2,4) + 
                  20*Power(t2,5)) - 
               Power(s2,2)*t1*t2*
                (4*Power(t1,6) + 41*Power(t1,5)*t2 + 
                  38*Power(t1,4)*Power(t2,2) + 
                  14*Power(t1,3)*Power(t2,3) + 
                  38*Power(t1,2)*Power(t2,4) + 41*t1*Power(t2,5) + 
                  4*Power(t2,6)) - 
               Power(s2,4)*(2*Power(t1,6) + 30*Power(t1,5)*t2 - 
                  99*Power(t1,4)*Power(t2,2) + 
                  475*Power(t1,3)*Power(t2,3) - 
                  54*Power(t1,2)*Power(t2,4) + 60*t1*Power(t2,5) + 
                  30*Power(t2,6)) + 
               Power(s2,3)*(Power(t1,7) + 24*Power(t1,6)*t2 - 
                  8*Power(t1,5)*Power(t2,2) + 
                  217*Power(t1,4)*Power(t2,3) + 
                  355*Power(t1,3)*Power(t2,4) - 
                  10*Power(t1,2)*Power(t2,5) + 45*t1*Power(t2,6) + 
                  6*Power(t2,7))))))/
     (3.*Power(s1,3)*Power(s2,3)*Power(t1,3)*Power(s - s2 + t1,3)*
       Power(t2,3)*Power(s - s1 + t2,3)) - 
    (8*(s1*s2*(t1 - t2)*(s1 + t1 - t2)*(s2 - t1 + t2)*
          Power(s1*(s2 - t1) - s2*t2,2)*
          (Power(s1,3)*(s2 - t1)*t2 + 
            t2*(Power(s2,3)*t1 - 2*t1*Power(t1 - t2,3) + 
               s2*Power(t1 - t2,2)*(5*t1 - t2) - 
               Power(s2,2)*(4*Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(Power(s2,2)*(t1 - t2) + 
               s2*(-2*Power(t1,2) + 6*t1*t2 - 3*Power(t2,2)) + 
               t1*(Power(t1,2) - 5*t1*t2 + 4*Power(t2,2))) + 
            s1*(-(Power(s2,3)*t1) + t1*(t1 - 5*t2)*Power(t1 - t2,2) + 
               Power(s2,2)*(3*Power(t1,2) - 6*t1*t2 + 2*Power(t2,2)) + 
               s2*(-3*Power(t1,3) + 13*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) + 3*Power(t2,3)))) + 
         Power(s,7)*(t1 - t2)*
          (s2*(t1 - t2)*t2*(Power(t1 - t2,2)*t2 + 
               Power(s2,2)*(t1 + t2) - 
               s2*(Power(t1,2) + t1*t2 - 2*Power(t2,2))) + 
            Power(s1,3)*(Power(t1,3) - t1*Power(t2,2) + 
               2*Power(s2,2)*(t1 + t2) - 
               2*s2*(Power(t1,2) + t1*t2 - Power(t2,2))) + 
            Power(s1,2)*(-2*Power(s2,3)*(t1 + t2) + 
               t1*Power(t1 - t2,2)*(2*t1 + t2) + 
               6*Power(s2,2)*(Power(t1,2) - Power(t2,2)) + 
               s2*(-5*Power(t1,3) + 2*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) - 4*Power(t2,3))) + 
            s1*(Power(t1,2)*Power(t1 - t2,3) - 
               3*s2*Power(t1 - t2,3)*(t1 + t2) + 
               2*Power(s2,3)*(-Power(t1,2) + t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,3) - 7*Power(t1,2)*t2 - 
                  2*t1*Power(t2,2) + 5*Power(t2,3)))) + 
         Power(s,6)*(t1 - t2)*
          (Power(s1,4)*(2*Power(s2,3) + Power(s2,2)*(-8*t1 + 4*t2) + 
               t1*(-4*Power(t1,2) + 3*t1*t2 + Power(t2,2)) + 
               s2*(9*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) - 
            Power(s1,3)*(2*Power(s2,4) - 14*Power(s2,3)*(t1 - t2) + 
               Power(s2,2)*(22*Power(t1,2) - 53*t1*t2 + 
                  15*Power(t2,2)) + 
               s2*(-12*Power(t1,3) + 53*Power(t1,2)*t2 - 
                  22*t1*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(2*Power(t1,3) - 20*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 5*Power(t2,3))) - 
            (t1 - t2)*t2*(Power(s2,4)*(t1 + 4*t2) + 
               4*t1*Power(Power(t1,2) - Power(t2,2),2) + 
               Power(s2,3)*(-5*Power(t1,2) - 18*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(11*Power(t1,3) + 23*Power(t1,2)*t2 - 
                  11*t1*Power(t2,2) - 7*Power(t2,3)) + 
               s2*(-11*Power(t1,4) - 10*Power(t1,3)*t2 + 
                  16*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) - 
                  5*Power(t2,4))) + 
            Power(s1,2)*(-4*Power(s2,4)*(t1 - 2*t2) + 
               Power(s2,3)*(15*Power(t1,2) - 53*t1*t2 + 
                  22*Power(t2,2)) + 
               Power(s2,2)*(-9*Power(t1,3) + 85*Power(t1,2)*t2 - 
                  85*t1*Power(t2,2) + 9*Power(t2,3)) - 
               s2*(8*Power(t1,4) + 44*Power(t1,3)*t2 - 
                  84*Power(t1,2)*Power(t2,2) + 11*t1*Power(t2,3) + 
                  5*Power(t2,4)) + 
               t1*(7*Power(t1,4) + 4*Power(t1,3)*t2 - 
                  34*Power(t1,2)*Power(t2,2) + 12*t1*Power(t2,3) + 
                  11*Power(t2,4))) + 
            s1*(Power(s2,4)*(-2*Power(t1,2) + 5*t1*t2 - 9*Power(t2,2)) + 
               Power(s2,3)*(3*Power(t1,3) - 22*Power(t1,2)*t2 + 
                  53*t1*Power(t2,2) - 12*Power(t2,3)) + 
               t1*Power(t1 - t2,2)*
                (5*Power(t1,3) - 5*Power(t1,2)*t2 - 21*t1*Power(t2,2) - 
                  11*Power(t2,3)) + 
               Power(s2,2)*(5*Power(t1,4) + 11*Power(t1,3)*t2 - 
                  84*Power(t1,2)*Power(t2,2) + 44*t1*Power(t2,3) + 
                  8*Power(t2,4)) + 
               s2*(-11*Power(t1,5) + 19*Power(t1,4)*t2 + 
                  46*Power(t1,3)*Power(t2,2) - 
                  46*Power(t1,2)*Power(t2,3) - 19*t1*Power(t2,4) + 
                  11*Power(t2,5)))) - 
         Power(s,5)*(Power(s1,5)*
             (2*Power(s2,3)*(t1 - 3*t2) + 
               Power(s2,2)*(-9*Power(t1,2) + 23*t1*t2 - 
                  12*Power(t2,2)) + 
               s2*(13*Power(t1,3) - 28*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) - 6*Power(t2,3)) + 
               t1*(-6*Power(t1,3) + 13*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,4)*(4*Power(s2,4)*(t1 + t2) + 
               Power(s2,3)*(-5*Power(t1,2) - 35*t1*t2 + 
                  38*Power(t2,2)) + 
               Power(s2,2)*(5*Power(t1,3) + 78*Power(t1,2)*t2 - 
                  142*t1*Power(t2,2) + 55*Power(t2,3)) + 
               t1*(7*Power(t1,4) + 22*Power(t1,3)*t2 - 
                  62*Power(t1,2)*Power(t2,2) + 31*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*(-10*Power(t1,4) - 71*Power(t1,3)*t2 + 
                  154*Power(t1,2)*Power(t2,2) - 96*t1*Power(t2,3) + 
                  21*Power(t2,4))) + 
            (t1 - t2)*t2*(8*t1*Power(Power(t1,2) - Power(t2,2),3) + 
               Power(s2,5)*(2*Power(t1,2) - 7*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,4)*(2*Power(t1,3) + 33*Power(t1,2)*t2 - 
                  29*t1*Power(t2,2) - 7*Power(t2,3)) + 
               Power(s2,3)*(-25*Power(t1,4) - 40*Power(t1,3)*t2 + 
                  42*Power(t1,2)*Power(t2,2) + 44*t1*Power(t2,3) - 
                  21*Power(t2,4)) - 
               s2*Power(t1 - t2,2)*
                (32*Power(t1,4) + 60*Power(t1,3)*t2 + 
                  52*Power(t1,2)*Power(t2,2) - 9*t1*Power(t2,3) - 
                  7*Power(t2,4)) + 
               Power(s2,2)*(45*Power(t1,5) + 10*Power(t1,4)*t2 - 
                  32*Power(t1,3)*Power(t2,2) - 
                  83*Power(t1,2)*Power(t2,3) + 61*t1*Power(t2,4) - 
                  Power(t2,5))) - 
            Power(s1,3)*(Power(s2,5)*(6*t1 - 2*t2) + 
               Power(s2,4)*(-38*Power(t1,2) + 35*t1*t2 + 
                  5*Power(t2,2)) - 
               t1*Power(t1 - t2,2)*
                (21*Power(t1,3) - 23*Power(t1,2)*t2 - 
                  65*t1*Power(t2,2) - 25*Power(t2,3)) + 
               Power(s2,3)*(72*Power(t1,3) - 74*Power(t1,2)*t2 - 
                  74*t1*Power(t2,2) + 72*Power(t2,3)) + 
               Power(s2,2)*(-74*Power(t1,4) + 105*Power(t1,3)*t2 + 
                  148*Power(t1,2)*Power(t2,2) - 267*t1*Power(t2,3) + 
                  86*Power(t2,4)) + 
               s2*(54*Power(t1,5) - 121*Power(t1,4)*t2 - 
                  84*Power(t1,3)*Power(t2,2) + 
                  250*Power(t1,2)*Power(t2,3) - 120*t1*Power(t2,4) + 
                  21*Power(t2,5))) + 
            Power(s1,2)*(Power(s2,5)*
                (-12*Power(t1,2) + 23*t1*t2 - 9*Power(t2,2)) + 
               Power(s2,4)*(55*Power(t1,3) - 142*Power(t1,2)*t2 + 
                  78*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (55*Power(t1,3) - 144*Power(t1,2)*t2 - 
                  144*t1*Power(t2,2) + 55*Power(t2,3)) + 
               t1*Power(t1 - t2,2)*
                (Power(t1,4) - 60*Power(t1,3)*t2 + 
                  23*Power(t1,2)*Power(t2,2) + 55*t1*Power(t2,3) + 
                  45*Power(t2,4)) + 
               Power(s2,3)*(-86*Power(t1,4) + 267*Power(t1,3)*t2 - 
                  148*Power(t1,2)*Power(t2,2) - 105*t1*Power(t2,3) + 
                  74*Power(t2,4)) - 
               s2*(13*Power(t1,6) - 165*Power(t1,5)*t2 + 
                  243*Power(t1,4)*Power(t2,2) + 
                  62*Power(t1,3)*Power(t2,3) - 
                  189*Power(t1,2)*Power(t2,4) + 31*t1*Power(t2,5) + 
                  5*Power(t2,6))) - 
            s1*(Power(s2,5)*(6*Power(t1,3) - 23*Power(t1,2)*t2 + 
                  28*t1*Power(t2,2) - 13*Power(t2,3)) + 
               t1*Power(t1 - t2,3)*
                (7*Power(t1,4) + 9*Power(t1,3)*t2 - 
                  52*Power(t1,2)*Power(t2,2) - 60*t1*Power(t2,3) - 
                  32*Power(t2,4)) + 
               Power(s2,4)*(-21*Power(t1,4) + 96*Power(t1,3)*t2 - 
                  154*Power(t1,2)*Power(t2,2) + 71*t1*Power(t2,3) + 
                  10*Power(t2,4)) - 
               2*s2*Power(t1 - t2,2)*
                (9*Power(t1,5) + 4*Power(t1,4)*t2 - 
                  61*Power(t1,3)*Power(t2,2) - 
                  61*Power(t1,2)*Power(t2,3) + 4*t1*Power(t2,4) + 
                  9*Power(t2,5)) + 
               Power(s2,3)*(21*Power(t1,5) - 120*Power(t1,4)*t2 + 
                  250*Power(t1,3)*Power(t2,2) - 
                  84*Power(t1,2)*Power(t2,3) - 121*t1*Power(t2,4) + 
                  54*Power(t2,5)) + 
               Power(s2,2)*(5*Power(t1,6) + 31*Power(t1,5)*t2 - 
                  189*Power(t1,4)*Power(t2,2) + 
                  62*Power(t1,3)*Power(t2,3) + 
                  243*Power(t1,2)*Power(t2,4) - 165*t1*Power(t2,5) + 
                  13*Power(t2,6)))) + 
         s*(Power(s1,7)*(s2 - t1)*
             (Power(s2,4)*(t1 - t2) + 
               Power(s2,3)*(-3*Power(t1,2) + 2*t1*t2 + Power(t2,2)) - 
               Power(t1,2)*t2*(Power(t1,2) - 2*t1*t2 + 2*Power(t2,2)) + 
               s2*t1*(-Power(t1,3) + 2*Power(t1,2)*t2 + 
                  t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*(3*Power(t1,3) - 2*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,6)*(s2 - t1)*
             (Power(s2,4)*(Power(t1,2) - 5*t1*t2 + 4*Power(t2,2)) - 
               Power(s2,3)*(4*Power(t1,3) + Power(t1,2)*t2 - 
                  18*t1*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,2)*(6*Power(t1,4) - Power(t1,3)*t2 - 
                  43*Power(t1,2)*Power(t2,2) + 47*t1*Power(t2,3) - 
                  13*Power(t2,4)) + 
               s2*t1*(-4*Power(t1,4) + 9*Power(t1,3)*t2 - 
                  5*Power(t1,2)*Power(t2,2) - 9*t1*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(t1,2)*(Power(t1,4) - 2*Power(t1,3)*t2 + 
                  10*Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) + 
                  14*Power(t2,4))) + 
            Power(s1,5)*(Power(s2,7)*(-t1 + t2) + 
               Power(s2,6)*(4*Power(t1,2) - 5*t1*t2 + Power(t2,2)) - 
               3*Power(s2,5)*(Power(t1,3) + Power(t2,3)) + 
               Power(s2,4)*(-10*Power(t1,4) + 16*Power(t1,3)*t2 + 
                  79*Power(t1,2)*Power(t2,2) - 110*t1*Power(t2,3) + 
                  32*Power(t2,4)) + 
               s2*Power(t1,2)*
                (11*Power(t1,5) - 58*Power(t1,4)*t2 + 
                  117*Power(t1,3)*Power(t2,2) - 
                  135*Power(t1,2)*Power(t2,3) + 96*t1*Power(t2,4) - 
                  37*Power(t2,5)) + 
               Power(s2,3)*(25*Power(t1,5) - 55*Power(t1,4)*t2 - 
                  137*Power(t1,3)*Power(t2,2) + 
                  338*Power(t1,2)*Power(t2,3) - 193*t1*Power(t2,4) + 
                  35*Power(t2,5)) - 
               Power(s2,2)*t1*
                (24*Power(t1,5) - 91*Power(t1,4)*t2 + 
                  30*Power(t1,3)*Power(t2,2) + 
                  164*Power(t1,2)*Power(t2,3) - 160*t1*Power(t2,4) + 
                  42*Power(t2,5)) + 
               Power(t1,3)*(-2*Power(t1,5) + 10*Power(t1,4)*t2 - 
                  30*Power(t1,3)*Power(t2,2) + 
                  74*Power(t1,2)*Power(t2,3) - 93*t1*Power(t2,4) + 
                  42*Power(t2,5))) + 
            s2*Power(t2,3)*(Power(s2,6)*t1*
                (2*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               8*Power(t1,3)*Power(t1 - t2,4)*
                (Power(t1,2) - t1*t2 + Power(t2,2)) - 
               4*s2*Power(t1,2)*Power(t1 - t2,3)*
                (9*Power(t1,3) - 13*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) - 3*Power(t2,3)) - 
               Power(s2,5)*(14*Power(t1,4) - 21*Power(t1,3)*t2 + 
                  10*Power(t1,2)*Power(t2,2) - 2*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*t1*Power(t1 - t2,2)*
                (68*Power(t1,4) - 121*Power(t1,3)*t2 + 
                  100*Power(t1,2)*Power(t2,2) - 43*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,4)*(42*Power(t1,5) - 93*Power(t1,4)*t2 + 
                  74*Power(t1,3)*Power(t2,2) - 
                  30*Power(t1,2)*Power(t2,3) + 10*t1*Power(t2,4) - 
                  2*Power(t2,5)) - 
               Power(s2,3)*(70*Power(t1,6) - 211*Power(t1,5)*t2 + 
                  255*Power(t1,4)*Power(t2,2) - 
                  168*Power(t1,3)*Power(t2,3) + 
                  68*Power(t1,2)*Power(t2,4) - 15*t1*Power(t2,5) + 
                  Power(t2,6))) + 
            Power(s1,4)*(Power(s2,7)*
                (Power(t1,2) + 3*t1*t2 - 4*Power(t2,2)) + 
               Power(s2,6)*(-9*Power(t1,3) + 14*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,5)*(32*Power(t1,4) - 110*Power(t1,3)*t2 + 
                  79*Power(t1,2)*Power(t2,2) + 16*t1*Power(t2,3) - 
                  10*Power(t2,4)) - 
               Power(s2,4)*(59*Power(t1,5) - 255*Power(t1,4)*t2 + 
                  210*Power(t1,3)*Power(t2,2) + 
                  210*Power(t1,2)*Power(t2,3) - 255*t1*Power(t2,4) + 
                  59*Power(t2,5)) + 
               Power(s2,3)*(61*Power(t1,6) - 321*Power(t1,5)*t2 + 
                  384*Power(t1,4)*Power(t2,2) + 
                  278*Power(t1,3)*Power(t2,3) - 
                  672*Power(t1,2)*Power(t2,4) + 324*t1*Power(t2,5) - 
                  50*Power(t2,6)) + 
               Power(s2,2)*t1*
                (-35*Power(t1,6) + 244*Power(t1,5)*t2 - 
                  534*Power(t1,4)*Power(t2,2) + 
                  335*Power(t1,3)*Power(t2,3) + 
                  146*Power(t1,2)*Power(t2,4) - 192*t1*Power(t2,5) + 
                  46*Power(t2,6)) - 
               Power(t1,3)*(Power(t1,6) - 15*Power(t1,5)*t2 + 
                  68*Power(t1,4)*Power(t2,2) - 
                  168*Power(t1,3)*Power(t2,3) + 
                  255*Power(t1,2)*Power(t2,4) - 211*t1*Power(t2,5) + 
                  70*Power(t2,6)) + 
               s2*Power(t1,2)*
                (10*Power(t1,6) - 100*Power(t1,5)*t2 + 
                  349*Power(t1,4)*Power(t2,2) - 
                  582*Power(t1,3)*Power(t2,3) + 
                  537*Power(t1,2)*Power(t2,4) - 292*t1*Power(t2,5) + 
                  81*Power(t2,6))) + 
            s1*Power(t2,2)*(Power(s2,7)*t2*
                (Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               8*Power(t1,3)*Power(t1 - t2,4)*t2*
                (Power(t1,2) - t1*t2 + Power(t2,2)) - 
               4*s2*Power(t1,2)*Power(t1 - t2,4)*
                (3*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,6)*(7*Power(t1,4) - 12*Power(t1,3)*t2 + 
                  15*Power(t1,2)*Power(t2,2) - 11*t1*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*Power(t1 - t2,2)*
                (52*Power(t1,4) - 140*Power(t1,3)*t2 + 
                  207*Power(t1,2)*Power(t2,2) - 154*t1*Power(t2,3) + 
                  33*Power(t2,4)) + 
               Power(s2,5)*(-37*Power(t1,5) + 96*Power(t1,4)*t2 - 
                  135*Power(t1,3)*Power(t2,2) + 
                  117*Power(t1,2)*Power(t2,3) - 58*t1*Power(t2,4) + 
                  11*Power(t2,5)) + 
               Power(s2,4)*(81*Power(t1,6) - 292*Power(t1,5)*t2 + 
                  537*Power(t1,4)*Power(t2,2) - 
                  582*Power(t1,3)*Power(t2,3) + 
                  349*Power(t1,2)*Power(t2,4) - 100*t1*Power(t2,5) + 
                  10*Power(t2,6)) + 
               Power(s2,3)*(-91*Power(t1,7) + 399*Power(t1,6)*t2 - 
                  853*Power(t1,5)*Power(t2,2) + 
                  1067*Power(t1,4)*Power(t2,3) - 
                  763*Power(t1,3)*Power(t2,4) + 
                  287*Power(t1,2)*Power(t2,5) - 49*t1*Power(t2,6) + 
                  3*Power(t2,7))) - 
            Power(s1,2)*t2*(4*Power(t1,3)*Power(t1 - t2,3)*t2*
                (3*Power(t1,3) - 11*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - 9*Power(t2,3)) + 
               Power(s2,7)*(4*Power(t1,3) - 5*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + 4*Power(t2,3)) - 
               2*Power(s2,6)*
                (10*Power(t1,4) - 28*Power(t1,3)*t2 + 
                  19*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) - 
                  5*Power(t2,4)) - 
               s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                (33*Power(t1,4) - 154*Power(t1,3)*t2 + 
                  207*Power(t1,2)*Power(t2,2) - 140*t1*Power(t2,3) + 
                  52*Power(t2,4)) - 
               2*Power(s2,2)*t1*Power(t1 - t2,2)*
                (3*Power(t1,5) - 19*Power(t1,4)*t2 + 
                  33*Power(t1,3)*Power(t2,2) + 
                  33*Power(t1,2)*Power(t2,3) - 19*t1*Power(t2,4) + 
                  3*Power(t2,5)) + 
               Power(s2,5)*(42*Power(t1,5) - 160*Power(t1,4)*t2 + 
                  164*Power(t1,3)*Power(t2,2) + 
                  30*Power(t1,2)*Power(t2,3) - 91*t1*Power(t2,4) + 
                  24*Power(t2,5)) + 
               Power(s2,4)*(-46*Power(t1,6) + 192*Power(t1,5)*t2 - 
                  146*Power(t1,4)*Power(t2,2) - 
                  335*Power(t1,3)*Power(t2,3) + 
                  534*Power(t1,2)*Power(t2,4) - 244*t1*Power(t2,5) + 
                  35*Power(t2,6)) + 
               Power(s2,3)*(26*Power(t1,7) - 112*Power(t1,6)*t2 + 
                  32*Power(t1,5)*Power(t2,2) + 
                  535*Power(t1,4)*Power(t2,3) - 
                  932*Power(t1,3)*Power(t2,4) + 
                  604*Power(t1,2)*Power(t2,5) - 166*t1*Power(t2,6) + 
                  17*Power(t2,7))) + 
            Power(s1,3)*(Power(s2,7)*
                (2*Power(t1,3) - 5*Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s2,6)*(-13*Power(t1,4) + 56*Power(t1,3)*t2 - 
                  61*Power(t1,2)*Power(t2,2) + 10*Power(t2,4)) + 
               Power(t1,3)*Power(t1 - t2,2)*t2*
                (6*Power(t1,4) - 43*Power(t1,3)*t2 + 
                  100*Power(t1,2)*Power(t2,2) - 121*t1*Power(t2,3) + 
                  68*Power(t2,4)) + 
               Power(s2,5)*(35*Power(t1,5) - 193*Power(t1,4)*t2 + 
                  338*Power(t1,3)*Power(t2,2) - 
                  137*Power(t1,2)*Power(t2,3) - 55*t1*Power(t2,4) + 
                  25*Power(t2,5)) + 
               Power(s2,4)*(-50*Power(t1,6) + 324*Power(t1,5)*t2 - 
                  672*Power(t1,4)*Power(t2,2) + 
                  278*Power(t1,3)*Power(t2,3) + 
                  384*Power(t1,2)*Power(t2,4) - 321*t1*Power(t2,5) + 
                  61*Power(t2,6)) + 
               s2*Power(t1,2)*
                (3*Power(t1,7) - 49*Power(t1,6)*t2 + 
                  287*Power(t1,5)*Power(t2,2) - 
                  763*Power(t1,4)*Power(t2,3) + 
                  1067*Power(t1,3)*Power(t2,4) - 
                  853*Power(t1,2)*Power(t2,5) + 399*t1*Power(t2,6) - 
                  91*Power(t2,7)) - 
               Power(s2,2)*t1*
                (17*Power(t1,7) - 166*Power(t1,6)*t2 + 
                  604*Power(t1,5)*Power(t2,2) - 
                  932*Power(t1,4)*Power(t2,3) + 
                  535*Power(t1,3)*Power(t2,4) + 
                  32*Power(t1,2)*Power(t2,5) - 112*t1*Power(t2,6) + 
                  26*Power(t2,7)) + 
               Power(s2,3)*(40*Power(t1,7) - 305*Power(t1,6)*t2 + 
                  771*Power(t1,5)*Power(t2,2) - 
                  508*Power(t1,4)*Power(t2,3) - 
                  508*Power(t1,3)*Power(t2,4) + 
                  771*Power(t1,2)*Power(t2,5) - 305*t1*Power(t2,6) + 
                  40*Power(t2,7)))) + 
         Power(s,4)*(-(Power(s1,6)*
               (Power(s2,3)*(t1 + 2*t2) + 
                 Power(s2,2)*(2*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)) + 
                 t1*(4*Power(t1,3) - 9*Power(t1,2)*t2 + 
                    8*t1*Power(t2,2) - 2*Power(t2,3)) + 
                 s2*(-7*Power(t1,3) + 14*Power(t1,2)*t2 - 
                    12*t1*Power(t2,2) + 2*Power(t2,3)))) + 
            Power(s1,5)*(3*Power(s2,4)*(4*t1 - 3*t2) + 
               Power(s2,3)*(-30*Power(t1,2) + 43*t1*t2 + 
                  7*Power(t2,2)) + 
               Power(s2,2)*(36*Power(t1,3) - 47*Power(t1,2)*t2 - 
                  33*t1*Power(t2,2) + 32*Power(t2,3)) + 
               t1*(13*Power(t1,4) + 3*Power(t1,3)*t2 - 
                  52*Power(t1,2)*Power(t2,2) + 45*t1*Power(t2,3) - 
                  8*Power(t2,4)) + 
               s2*(-31*Power(t1,4) + 8*Power(t1,3)*t2 + 
                  74*Power(t1,2)*Power(t2,2) - 79*t1*Power(t2,3) + 
                  16*Power(t2,4))) + 
            t2*(Power(s2,6)*(2*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,4)*Power(t1 - t2,2)*
                (Power(t1,3) - 92*Power(t1,2)*t2 - 57*t1*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(s2,3)*Power(t1 - t2,2)*
                (31*Power(t1,4) + 141*Power(t1,3)*t2 + 
                  67*Power(t1,2)*Power(t2,2) - 38*t1*Power(t2,3) - 
                  20*Power(t2,4)) - 
               4*t1*Power(t1 - t2,4)*
                (Power(t1,4) + 7*Power(t1,3)*t2 + 
                  8*Power(t1,2)*Power(t2,2) + 7*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,5)*(-8*Power(t1,4) + 45*Power(t1,3)*t2 - 
                  52*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  13*Power(t2,4)) + 
               s2*Power(t1 - t2,3)*
                (25*Power(t1,5) + 95*Power(t1,4)*t2 + 
                  60*Power(t1,3)*Power(t2,2) + 
                  46*Power(t1,2)*Power(t2,3) - 31*t1*Power(t2,4) - 
                  3*Power(t2,5)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (47*Power(t1,5) + 124*Power(t1,4)*t2 + 
                  8*Power(t1,3)*Power(t2,2) - 
                  29*Power(t1,2)*Power(t2,3) - 67*t1*Power(t2,4) + 
                  13*Power(t2,5))) + 
            Power(s1,4)*(Power(s2,5)*(-9*t1 + 12*t2) + 
               3*Power(s2,4)*
                (17*Power(t1,2) - 48*t1*t2 + 17*Power(t2,2)) + 
               t1*Power(t1 - t2,2)*
                (13*Power(t1,3) - 57*Power(t1,2)*t2 - 
                  92*t1*Power(t2,2) + Power(t2,3)) - 
               Power(s2,3)*(87*Power(t1,3) - 338*Power(t1,2)*t2 + 
                  225*t1*Power(t2,2) + 17*Power(t2,3)) + 
               Power(s2,2)*(70*Power(t1,4) - 349*Power(t1,3)*t2 + 
                  294*Power(t1,2)*Power(t2,2) + 100*t1*Power(t2,3) - 
                  94*Power(t2,4)) - 
               s2*(38*Power(t1,5) - 225*Power(t1,4)*t2 + 
                  144*Power(t1,3)*Power(t2,2) + 
                  192*Power(t1,2)*Power(t2,3) - 196*t1*Power(t2,4) + 
                  38*Power(t2,5))) + 
            Power(s1,3)*(-(Power(s2,6)*(2*t1 + t2)) + 
               Power(s2,5)*(7*Power(t1,2) + 43*t1*t2 - 30*Power(t2,2)) - 
               Power(s2,4)*(17*Power(t1,3) + 225*Power(t1,2)*t2 - 
                  338*t1*Power(t2,2) + 87*Power(t2,3)) - 
               t1*Power(t1 - t2,2)*
                (20*Power(t1,4) + 38*Power(t1,3)*t2 - 
                  67*Power(t1,2)*Power(t2,2) - 141*t1*Power(t2,3) - 
                  31*Power(t2,4)) + 
               Power(s2,3)*(37*Power(t1,4) + 335*Power(t1,3)*t2 - 
                  760*Power(t1,2)*Power(t2,2) + 335*t1*Power(t2,3) + 
                  37*Power(t2,4)) - 
               Power(s2,2)*(61*Power(t1,5) + 161*Power(t1,4)*t2 - 
                  672*Power(t1,3)*Power(t2,2) + 
                  367*Power(t1,2)*Power(t2,3) + 221*t1*Power(t2,4) - 
                  128*Power(t2,5)) + 
               s2*(56*Power(t1,6) + 8*Power(t1,5)*t2 - 
                  335*Power(t1,4)*Power(t2,2) + 
                  126*Power(t1,3)*Power(t2,3) + 
                  332*Power(t1,2)*Power(t2,4) - 220*t1*Power(t2,5) + 
                  33*Power(t2,6))) - 
            Power(s1,2)*(Power(s2,6)*
                (4*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,5)*(-32*Power(t1,3) + 33*Power(t1,2)*t2 + 
                  47*t1*Power(t2,2) - 36*Power(t2,3)) + 
               Power(s2,4)*(94*Power(t1,4) - 100*Power(t1,3)*t2 - 
                  294*Power(t1,2)*Power(t2,2) + 349*t1*Power(t2,3) - 
                  70*Power(t2,4)) + 
               2*Power(s2,2)*Power(t1 - t2,2)*
                (48*Power(t1,4) - 67*Power(t1,3)*t2 - 
                  153*Power(t1,2)*Power(t2,2) - 67*t1*Power(t2,3) + 
                  48*Power(t2,4)) + 
               t1*Power(t1 - t2,2)*
                (13*Power(t1,5) - 67*Power(t1,4)*t2 - 
                  29*Power(t1,3)*Power(t2,2) + 
                  8*Power(t1,2)*Power(t2,3) + 124*t1*Power(t2,4) + 
                  47*Power(t2,5)) + 
               Power(s2,3)*(-128*Power(t1,5) + 221*Power(t1,4)*t2 + 
                  367*Power(t1,3)*Power(t2,2) - 
                  672*Power(t1,2)*Power(t2,3) + 161*t1*Power(t2,4) + 
                  61*Power(t2,5)) + 
               s2*(-47*Power(t1,7) + 272*Power(t1,6)*t2 - 
                  294*Power(t1,5)*Power(t2,2) + 
                  35*Power(t1,4)*Power(t2,3) - 
                  264*Power(t1,3)*Power(t2,4) + 
                  428*Power(t1,2)*Power(t2,5) - 133*t1*Power(t2,6) + 
                  3*Power(t2,7))) + 
            s1*(Power(s2,6)*(-2*Power(t1,3) + 12*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,5)*(16*Power(t1,4) - 79*Power(t1,3)*t2 + 
                  74*Power(t1,2)*Power(t2,2) + 8*t1*Power(t2,3) - 
                  31*Power(t2,4)) + 
               t1*Power(t1 - t2,3)*
                (3*Power(t1,5) + 31*Power(t1,4)*t2 - 
                  46*Power(t1,3)*Power(t2,2) - 
                  60*Power(t1,2)*Power(t2,3) - 95*t1*Power(t2,4) - 
                  25*Power(t2,5)) - 
               Power(s2,4)*(38*Power(t1,5) - 196*Power(t1,4)*t2 + 
                  192*Power(t1,3)*Power(t2,2) + 
                  144*Power(t1,2)*Power(t2,3) - 225*t1*Power(t2,4) + 
                  38*Power(t2,5)) - 
               s2*Power(t1 - t2,2)*
                (9*Power(t1,6) + 82*Power(t1,5)*t2 - 
                  203*Power(t1,4)*Power(t2,2) - 
                  64*Power(t1,3)*Power(t2,3) - 
                  203*Power(t1,2)*Power(t2,4) + 82*t1*Power(t2,5) + 
                  9*Power(t2,6)) + 
               Power(s2,3)*(33*Power(t1,6) - 220*Power(t1,5)*t2 + 
                  332*Power(t1,4)*Power(t2,2) + 
                  126*Power(t1,3)*Power(t2,3) - 
                  335*Power(t1,2)*Power(t2,4) + 8*t1*Power(t2,5) + 
                  56*Power(t2,6)) + 
               Power(s2,2)*(-3*Power(t1,7) + 133*Power(t1,6)*t2 - 
                  428*Power(t1,5)*Power(t2,2) + 
                  264*Power(t1,4)*Power(t2,3) - 
                  35*Power(t1,3)*Power(t2,4) + 
                  294*Power(t1,2)*Power(t2,5) - 272*t1*Power(t2,6) + 
                  47*Power(t2,7)))) - 
         Power(s,2)*(Power(s1,7)*(s2 - t1)*
             (Power(t1,4) + 2*Power(s2,3)*(t1 - t2) - Power(t1,3)*t2 + 
               2*t1*Power(t2,3) + Power(s2,2)*t1*(-3*t1 + 5*t2) + 
               s2*(-2*Power(t1,2)*t2 + 2*Power(t2,3))) + 
            Power(s1,6)*(s2 - t1)*
             (2*Power(s2,4)*(t1 - t2) + 
               2*Power(s2,3)*(4*Power(t1,2) - 9*t1*t2 + Power(t2,2)) - 
               Power(s2,2)*(10*Power(t1,3) - 41*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 8*Power(t2,3)) + 
               s2*(4*Power(t1,4) - Power(t1,3)*t2 - 
                  6*Power(t1,2)*Power(t2,2) + 21*t1*Power(t2,3) - 
                  12*Power(t2,4)) - 
               t1*(4*Power(t1,4) + 4*Power(t1,3)*t2 - 
                  16*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
                  12*Power(t2,4))) + 
            Power(s1,5)*(-2*Power(s2,6)*(t1 - t2) + 
               Power(s2,5)*(5*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,4)*(17*Power(t1,3) - 137*Power(t1,2)*t2 + 
                  99*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,3)*(-34*Power(t1,4) + 295*Power(t1,3)*t2 - 
                  355*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) + 
                  34*Power(t2,4)) + 
               Power(t1,2)*(5*Power(t1,5) - 22*Power(t1,4)*t2 - 
                  10*Power(t1,3)*Power(t2,2) + 
                  43*Power(t1,2)*Power(t2,3) + 17*t1*Power(t2,4) - 
                  36*Power(t2,5)) + 
               s2*t1*(-3*Power(t1,5) + 47*Power(t1,4)*t2 - 
                  20*Power(t1,3)*Power(t2,2) - 
                  84*Power(t1,2)*Power(t2,3) + 79*t1*Power(t2,4) + 
                  Power(t2,5)) + 
               Power(s2,2)*(12*Power(t1,5) - 187*Power(t1,4)*t2 + 
                  281*Power(t1,3)*Power(t2,2) + 
                  13*Power(t1,2)*Power(t2,3) - 126*t1*Power(t2,4) + 
                  33*Power(t2,5))) + 
            Power(t2,2)*(8*Power(t1,3)*Power(t1 - t2,4)*t2*
                (Power(t1,2) + Power(t2,2)) - 
               Power(s2,7)*(2*Power(t1,3) - t1*Power(t2,2) + 
                  Power(t2,3)) - 
               4*s2*Power(t1,2)*Power(t1 - t2,3)*
                (3*Power(t1,4) + 7*Power(t1,3)*t2 - 
                  5*Power(t1,2)*Power(t2,2) + 9*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,6)*(12*Power(t1,4) + Power(t1,3)*t2 - 
                  16*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(s2,3)*Power(t1 - t2,2)*
                (78*Power(t1,5) + 13*Power(t1,4)*t2 - 
                  4*Power(t1,3)*Power(t2,2) + 
                  12*Power(t1,2)*Power(t2,3) - 32*t1*Power(t2,4) + 
                  5*Power(t2,5)) + 
               Power(s2,5)*(-36*Power(t1,5) + 17*Power(t1,4)*t2 + 
                  43*Power(t1,3)*Power(t2,2) - 
                  10*Power(t1,2)*Power(t2,3) - 22*t1*Power(t2,4) + 
                  5*Power(t2,5)) + 
               2*Power(s2,2)*t1*Power(t1 - t2,2)*
                (24*Power(t1,5) + 3*Power(t1,4)*t2 - 
                  22*Power(t1,3)*Power(t2,2) + 
                  29*Power(t1,2)*Power(t2,3) - 36*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               Power(s2,4)*(68*Power(t1,6) - 87*Power(t1,5)*t2 - 
                  8*Power(t1,4)*Power(t2,2) + 
                  33*Power(t1,2)*Power(t2,4) - t1*Power(t2,5) - 
                  5*Power(t2,6))) - 
            Power(s1,4)*(2*Power(s2,7)*(t1 - t2) - 
               2*Power(s2,6)*(Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) - 
               Power(s2,5)*(4*Power(t1,3) + 99*Power(t1,2)*t2 - 
                  137*t1*Power(t2,2) + 17*Power(t2,3)) + 
               Power(s2,4)*(5*Power(t1,4) + 367*Power(t1,3)*t2 - 
                  826*Power(t1,2)*Power(t2,2) + 367*t1*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,3)*(-22*Power(t1,5) - 452*Power(t1,4)*t2 + 
                  1461*Power(t1,3)*Power(t2,2) - 
                  1097*Power(t1,2)*Power(t2,3) + 55*t1*Power(t2,4) + 
                  68*Power(t2,5)) + 
               Power(t1,2)*(5*Power(t1,6) + Power(t1,5)*t2 - 
                  33*Power(t1,4)*Power(t2,2) + 
                  8*Power(t1,2)*Power(t2,4) + 87*t1*Power(t2,5) - 
                  68*Power(t2,6)) + 
               s2*t1*(-32*Power(t1,6) + 53*Power(t1,5)*t2 + 
                  102*Power(t1,4)*Power(t2,2) - 
                  128*Power(t1,3)*Power(t2,3) - 
                  92*Power(t1,2)*Power(t2,4) + 99*t1*Power(t2,5) + 
                  9*Power(t2,6)) + 
               Power(s2,2)*(48*Power(t1,6) + 116*Power(t1,5)*t2 - 
                  835*Power(t1,4)*Power(t2,2) + 
                  875*Power(t1,3)*Power(t2,3) + 
                  15*Power(t1,2)*Power(t2,4) - 236*t1*Power(t2,5) + 
                  50*Power(t2,6))) + 
            Power(s1,3)*(Power(s2,7)*(7*t1 - 5*t2)*t2 - 
               Power(s2,6)*(8*Power(t1,3) + 14*Power(t1,2)*t2 - 
                  59*t1*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s2,5)*(34*Power(t1,4) + 22*Power(t1,3)*t2 - 
                  355*Power(t1,2)*Power(t2,2) + 295*t1*Power(t2,3) - 
                  34*Power(t2,4)) + 
               Power(s2,4)*(-68*Power(t1,5) - 55*Power(t1,4)*t2 + 
                  1097*Power(t1,3)*Power(t2,2) - 
                  1461*Power(t1,2)*Power(t2,3) + 452*t1*Power(t2,4) + 
                  22*Power(t2,5)) - 
               Power(t1,2)*Power(t1 - t2,2)*
                (5*Power(t1,5) - 32*Power(t1,4)*t2 + 
                  12*Power(t1,3)*Power(t2,2) - 
                  4*Power(t1,2)*Power(t2,3) + 13*t1*Power(t2,4) + 
                  78*Power(t2,5)) + 
               Power(s2,3)*(85*Power(t1,6) - 54*Power(t1,5)*t2 - 
                  1185*Power(t1,4)*Power(t2,2) + 
                  2324*Power(t1,3)*Power(t2,3) - 
                  1185*Power(t1,2)*Power(t2,4) - 54*t1*Power(t2,5) + 
                  85*Power(t2,6)) + 
               s2*t1*(29*Power(t1,7) - 203*Power(t1,6)*t2 + 
                  375*Power(t1,5)*Power(t2,2) - 
                  219*Power(t1,4)*Power(t2,3) + 
                  109*Power(t1,3)*Power(t2,4) - 
                  179*Power(t1,2)*Power(t2,5) + 67*t1*Power(t2,6) + 
                  21*Power(t2,7)) + 
               Power(s2,2)*(-67*Power(t1,7) + 255*Power(t1,6)*t2 + 
                  95*Power(t1,5)*Power(t2,2) - 
                  979*Power(t1,4)*Power(t2,3) + 
                  687*Power(t1,3)*Power(t2,4) + 
                  244*Power(t1,2)*Power(t2,5) - 263*t1*Power(t2,6) + 
                  42*Power(t2,7))) + 
            Power(s1,2)*(Power(s2,7)*
                (2*Power(t1,3) - 7*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,6)*(-12*Power(t1,4) + 29*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) - 42*t1*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,5)*(33*Power(t1,5) - 126*Power(t1,4)*t2 + 
                  13*Power(t1,3)*Power(t2,2) + 
                  281*Power(t1,2)*Power(t2,3) - 187*t1*Power(t2,4) + 
                  12*Power(t2,5)) + 
               2*Power(t1,2)*Power(t1 - t2,2)*t2*
                (10*Power(t1,5) - 36*Power(t1,4)*t2 + 
                  29*Power(t1,3)*Power(t2,2) - 
                  22*Power(t1,2)*Power(t2,3) + 3*t1*Power(t2,4) + 
                  24*Power(t2,5)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (18*Power(t1,6) - 161*Power(t1,5)*t2 + 
                  255*Power(t1,4)*Power(t2,2) + 
                  56*Power(t1,3)*Power(t2,3) + 
                  255*Power(t1,2)*Power(t2,4) - 161*t1*Power(t2,5) + 
                  18*Power(t2,6)) - 
               Power(s2,4)*(50*Power(t1,6) - 236*Power(t1,5)*t2 + 
                  15*Power(t1,4)*Power(t2,2) + 
                  875*Power(t1,3)*Power(t2,3) - 
                  835*Power(t1,2)*Power(t2,4) + 116*t1*Power(t2,5) + 
                  48*Power(t2,6)) + 
               Power(s2,3)*(42*Power(t1,7) - 263*Power(t1,6)*t2 + 
                  244*Power(t1,5)*Power(t2,2) + 
                  687*Power(t1,4)*Power(t2,3) - 
                  979*Power(t1,3)*Power(t2,4) + 
                  95*Power(t1,2)*Power(t2,5) + 255*t1*Power(t2,6) - 
                  67*Power(t2,7)) + 
               s2*t1*(3*Power(t1,8) - 93*Power(t1,7)*t2 + 
                  466*Power(t1,6)*Power(t2,2) - 
                  891*Power(t1,5)*Power(t2,3) + 
                  942*Power(t1,4)*Power(t2,4) - 
                  749*Power(t1,3)*Power(t2,5) + 
                  408*Power(t1,2)*Power(t2,6) - 67*t1*Power(t2,7) - 
                  19*Power(t2,8))) + 
            s1*t2*(Power(s2,7)*Power(t2,2)*(t1 + t2) - 
               Power(s2,6)*t2*
                (22*Power(t1,3) - 22*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
                  8*Power(t2,3)) - 
               4*Power(t1,2)*Power(t1 - t2,3)*t2*
                (6*Power(t1,4) - 9*Power(t1,3)*t2 + 
                  5*Power(t1,2)*Power(t2,2) - 7*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,5)*(Power(t1,5) + 79*Power(t1,4)*t2 - 
                  84*Power(t1,3)*Power(t2,2) - 
                  20*Power(t1,2)*Power(t2,3) + 47*t1*Power(t2,4) - 
                  3*Power(t2,5)) + 
               2*s2*t1*Power(t1 - t2,2)*
                (3*Power(t1,6) + 39*Power(t1,5)*t2 - 
                  109*Power(t1,4)*Power(t2,2) + 
                  110*Power(t1,3)*Power(t2,3) - 
                  109*Power(t1,2)*Power(t2,4) + 39*t1*Power(t2,5) + 
                  3*Power(t2,6)) + 
               Power(s2,4)*(-9*Power(t1,6) - 99*Power(t1,5)*t2 + 
                  92*Power(t1,4)*Power(t2,2) + 
                  128*Power(t1,3)*Power(t2,3) - 
                  102*Power(t1,2)*Power(t2,4) - 53*t1*Power(t2,5) + 
                  32*Power(t2,6)) + 
               Power(s2,3)*(21*Power(t1,7) + 67*Power(t1,6)*t2 - 
                  179*Power(t1,5)*Power(t2,2) + 
                  109*Power(t1,4)*Power(t2,3) - 
                  219*Power(t1,3)*Power(t2,4) + 
                  375*Power(t1,2)*Power(t2,5) - 203*t1*Power(t2,6) + 
                  29*Power(t2,7)) + 
               Power(s2,2)*(-19*Power(t1,8) - 67*Power(t1,7)*t2 + 
                  408*Power(t1,6)*Power(t2,2) - 
                  749*Power(t1,5)*Power(t2,3) + 
                  942*Power(t1,4)*Power(t2,4) - 
                  891*Power(t1,3)*Power(t2,5) + 
                  466*Power(t1,2)*Power(t2,6) - 93*t1*Power(t2,7) + 
                  3*Power(t2,8)))) + 
         Power(s,3)*(Power(s1,7)*(s2 - t1)*
             (Power(s2,2)*(t1 - 2*t2) + 2*s2*(t1 - t2)*t2 - 
               t1*(Power(t1,2) - 2*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,6)*(-(Power(s2,4)*(t1 - 5*t2)) + 
               Power(s2,3)*(3*Power(t1,2) - 32*t1*t2 + 15*Power(t2,2)) + 
               Power(s2,2)*(-10*Power(t1,3) + 44*Power(t1,2)*t2 - 
                  38*t1*Power(t2,2) + 8*Power(t2,3)) + 
               s2*(15*Power(t1,4) - 20*Power(t1,3)*t2 + 
                  5*Power(t1,2)*Power(t2,2) + 12*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               t1*(-7*Power(t1,4) + 3*Power(t1,3)*t2 + 
                  18*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
                  2*Power(t2,4))) - 
            Power(s1,5)*(3*Power(s2,5)*(t1 + t2) + 
               Power(s2,4)*(-23*Power(t1,2) - 34*t1*t2 + 
                  44*Power(t2,2)) + 
               Power(s2,3)*(43*Power(t1,3) + 76*Power(t1,2)*t2 - 
                  228*t1*Power(t2,2) + 68*Power(t2,3)) + 
               Power(s2,2)*(-33*Power(t1,4) - 94*Power(t1,3)*t2 + 
                  323*Power(t1,2)*Power(t2,2) - 186*t1*Power(t2,3) + 
                  17*Power(t2,4)) + 
               s2*(14*Power(t1,5) + 77*Power(t1,4)*t2 - 
                  158*Power(t1,3)*Power(t2,2) + 
                  52*Power(t1,2)*Power(t2,3) + 49*t1*Power(t2,4) - 
                  10*Power(t2,5)) + 
               t1*(-4*Power(t1,5) - 28*Power(t1,4)*t2 + 
                  21*Power(t1,3)*Power(t2,2) + 
                  70*Power(t1,2)*Power(t2,3) - 68*t1*Power(t2,4) + 
                  6*Power(t2,5))) + 
            Power(s1,4)*(Power(s2,6)*(5*t1 - t2) + 
               Power(s2,5)*(-44*Power(t1,2) + 34*t1*t2 + 
                  23*Power(t2,2)) + 
               2*Power(s2,4)*(73*Power(t1,3) - 95*Power(t1,2)*t2 - 
                  95*t1*Power(t2,2) + 73*Power(t2,3)) + 
               Power(s2,3)*(-202*Power(t1,4) + 362*Power(t1,3)*t2 + 
                  386*Power(t1,2)*Power(t2,2) - 688*t1*Power(t2,3) + 
                  158*Power(t2,4)) + 
               Power(s2,2)*(139*Power(t1,5) - 321*Power(t1,4)*t2 - 
                  330*Power(t1,3)*Power(t2,2) + 
                  948*Power(t1,2)*Power(t2,3) - 413*t1*Power(t2,4) + 
                  17*Power(t2,5)) + 
               s2*(-66*Power(t1,6) + 184*Power(t1,5)*t2 + 
                  105*Power(t1,4)*Power(t2,2) - 
                  390*Power(t1,3)*Power(t2,3) + 
                  105*Power(t1,2)*Power(t2,4) + 96*t1*Power(t2,5) - 
                  19*Power(t2,6)) + 
               t1*(22*Power(t1,6) - 68*Power(t1,5)*t2 + 
                  6*Power(t1,4)*Power(t2,2) - 
                  10*Power(t1,3)*Power(t2,3) + 
                  171*Power(t1,2)*Power(t2,4) - 123*t1*Power(t2,5) + 
                  2*Power(t2,6))) + 
            t2*(Power(s2,7)*t2*(2*Power(t1,2) - 2*t1*t2 + Power(t2,2)) - 
               4*Power(t1,2)*Power(t1 - t2,4)*t2*
                (3*Power(t1,3) + 5*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,6)*(2*Power(t1,4) - 20*Power(t1,3)*t2 + 
                  18*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) - 
                  7*Power(t2,4)) + 
               s2*t1*Power(t1 - t2,3)*
                (4*Power(t1,5) + 69*Power(t1,4)*t2 + 
                  27*Power(t1,3)*Power(t2,2) + 
                  55*Power(t1,2)*Power(t2,3) - 9*t1*Power(t2,4) - 
                  18*Power(t2,5)) + 
               Power(s2,3)*Power(t1 - t2,2)*
                (10*Power(t1,5) + 165*Power(t1,4)*t2 + 
                  12*Power(t1,3)*Power(t2,2) + 
                  62*Power(t1,2)*Power(t2,3) - 82*t1*Power(t2,4) + 
                  3*Power(t2,5)) + 
               Power(s2,5)*(-6*Power(t1,5) + 68*Power(t1,4)*t2 - 
                  70*Power(t1,3)*Power(t2,2) - 
                  21*Power(t1,2)*Power(t2,3) + 28*t1*Power(t2,4) + 
                  4*Power(t2,5)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (12*Power(t1,6) + 141*Power(t1,5)*t2 - 
                  61*Power(t1,4)*Power(t2,2) + 
                  78*Power(t1,3)*Power(t2,3) - 
                  114*Power(t1,2)*Power(t2,4) + t1*Power(t2,5) + 
                  7*Power(t2,6)) + 
               Power(s2,4)*(2*Power(t1,6) - 123*Power(t1,5)*t2 + 
                  171*Power(t1,4)*Power(t2,2) - 
                  10*Power(t1,3)*Power(t2,3) + 
                  6*Power(t1,2)*Power(t2,4) - 68*t1*Power(t2,5) + 
                  22*Power(t2,6))) + 
            Power(s1,3)*(Power(s2,7)*(-2*t1 + t2) + 
               Power(s2,6)*(15*Power(t1,2) - 32*t1*t2 + 3*Power(t2,2)) - 
               Power(s2,5)*(68*Power(t1,3) - 228*Power(t1,2)*t2 + 
                  76*t1*Power(t2,2) + 43*Power(t2,3)) + 
               2*Power(s2,4)*(79*Power(t1,4) - 344*Power(t1,3)*t2 + 
                  193*Power(t1,2)*Power(t2,2) + 181*t1*Power(t2,3) - 
                  101*Power(t2,4)) + 
               t1*Power(t1 - t2,2)*
                (3*Power(t1,5) - 82*Power(t1,4)*t2 + 
                  62*Power(t1,3)*Power(t2,2) + 
                  12*Power(t1,2)*Power(t2,3) + 165*t1*Power(t2,4) + 
                  10*Power(t2,5)) - 
               Power(s2,3)*(163*Power(t1,5) - 887*Power(t1,4)*t2 + 
                  736*Power(t1,3)*Power(t2,2) + 
                  736*Power(t1,2)*Power(t2,3) - 887*t1*Power(t2,4) + 
                  163*Power(t2,5)) + 
               Power(s2,2)*(64*Power(t1,6) - 538*Power(t1,5)*t2 + 
                  700*Power(t1,4)*Power(t2,2) + 
                  500*Power(t1,3)*Power(t2,3) - 
                  1093*Power(t1,2)*Power(t2,4) + 338*t1*Power(t2,5) + 
                  11*Power(t2,6)) + 
               s2*(-7*Power(t1,7) + 230*Power(t1,6)*t2 - 
                  504*Power(t1,5)*Power(t2,2) + 
                  111*Power(t1,4)*Power(t2,3) + 
                  201*Power(t1,3)*Power(t2,4) + 
                  90*Power(t1,2)*Power(t2,5) - 138*t1*Power(t2,6) + 
                  17*Power(t2,7))) - 
            Power(s1,2)*(Power(s2,7)*
                (2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,6)*(-8*Power(t1,3) + 38*Power(t1,2)*t2 - 
                  44*t1*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,5)*(17*Power(t1,4) - 186*Power(t1,3)*t2 + 
                  323*Power(t1,2)*Power(t2,2) - 94*t1*Power(t2,3) - 
                  33*Power(t2,4)) + 
               Power(s2,4)*(-17*Power(t1,5) + 413*Power(t1,4)*t2 - 
                  948*Power(t1,3)*Power(t2,2) + 
                  330*Power(t1,2)*Power(t2,3) + 321*t1*Power(t2,4) - 
                  139*Power(t2,5)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (40*Power(t1,5) + 82*Power(t1,4)*t2 - 
                  353*Power(t1,3)*Power(t2,2) - 
                  353*Power(t1,2)*Power(t2,3) + 82*t1*Power(t2,4) + 
                  40*Power(t2,5)) + 
               t1*Power(t1 - t2,2)*
                (7*Power(t1,6) + Power(t1,5)*t2 - 
                  114*Power(t1,4)*Power(t2,2) + 
                  78*Power(t1,3)*Power(t2,3) - 
                  61*Power(t1,2)*Power(t2,4) + 141*t1*Power(t2,5) + 
                  12*Power(t2,6)) - 
               Power(s2,3)*(11*Power(t1,6) + 338*Power(t1,5)*t2 - 
                  1093*Power(t1,4)*Power(t2,2) + 
                  500*Power(t1,3)*Power(t2,3) + 
                  700*Power(t1,2)*Power(t2,4) - 538*t1*Power(t2,5) + 
                  64*Power(t2,6)) + 
               s2*(-30*Power(t1,8) + 88*Power(t1,7)*t2 + 
                  161*Power(t1,6)*Power(t2,2) - 
                  482*Power(t1,5)*Power(t2,3) + 
                  307*Power(t1,4)*Power(t2,4) - 
                  264*Power(t1,3)*Power(t2,5) + 
                  355*Power(t1,2)*Power(t2,6) - 142*t1*Power(t2,7) + 
                  7*Power(t2,8))) + 
            s1*(-(Power(s2,7)*Power(t2,3)) + 
               Power(s2,6)*(-2*Power(t1,4) + 12*Power(t1,3)*t2 + 
                  5*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(s2,5)*(10*Power(t1,5) - 49*Power(t1,4)*t2 - 
                  52*Power(t1,3)*Power(t2,2) + 
                  158*Power(t1,2)*Power(t2,3) - 77*t1*Power(t2,4) - 
                  14*Power(t2,5)) + 
               t1*Power(t1 - t2,3)*t2*
                (18*Power(t1,5) + 9*Power(t1,4)*t2 - 
                  55*Power(t1,3)*Power(t2,2) - 
                  27*Power(t1,2)*Power(t2,3) - 69*t1*Power(t2,4) - 
                  4*Power(t2,5)) + 
               Power(s2,4)*(-19*Power(t1,6) + 96*Power(t1,5)*t2 + 
                  105*Power(t1,4)*Power(t2,2) - 
                  390*Power(t1,3)*Power(t2,3) + 
                  105*Power(t1,2)*Power(t2,4) + 184*t1*Power(t2,5) - 
                  66*Power(t2,6)) + 
               Power(s2,3)*(17*Power(t1,7) - 138*Power(t1,6)*t2 + 
                  90*Power(t1,5)*Power(t2,2) + 
                  201*Power(t1,4)*Power(t2,3) + 
                  111*Power(t1,3)*Power(t2,4) - 
                  504*Power(t1,2)*Power(t2,5) + 230*t1*Power(t2,6) - 
                  7*Power(t2,7)) + 
               s2*Power(t1 - t2,2)*
                (Power(t1,7) - 79*Power(t1,6)*t2 + 
                  93*Power(t1,5)*Power(t2,2) + 
                  81*Power(t1,4)*Power(t2,3) + 
                  81*Power(t1,3)*Power(t2,4) + 
                  93*Power(t1,2)*Power(t2,5) - 79*t1*Power(t2,6) + 
                  Power(t2,7)) + 
               Power(s2,2)*(-7*Power(t1,8) + 142*Power(t1,7)*t2 - 
                  355*Power(t1,6)*Power(t2,2) + 
                  264*Power(t1,5)*Power(t2,3) - 
                  307*Power(t1,4)*Power(t2,4) + 
                  482*Power(t1,3)*Power(t2,5) - 
                  161*Power(t1,2)*Power(t2,6) - 88*t1*Power(t2,7) + 
                  30*Power(t2,8))))))/
     (s*s1*s2*(s2 - t1)*t1*(s + t1)*(s - s2 + t1)*(s1 - t2)*(t1 - t2)*
       (s1 + t1 - t2)*(s1 - s2 + t1 - t2)*t2*(s + t2)*(s - s1 + t2)*
       (s2 - t1 + t2)) + (16*(4*Power(s2,2)*(s2 - t1)*t1*Power(t2,6) - 
         Power(s1,7)*(s2 - t1)*t1*(s2 - t1 + t2) + 
         Power(s,7)*(Power(t1,3) + Power(t2,3)) + 
         Power(s1,4)*Power(t2,2)*
          (4*Power(s2,4) - 6*s2*Power(t1,2)*(t1 - t2) - 
            2*Power(t1,2)*t2*(t1 + 5*t2) - Power(s2,3)*(17*t1 + 8*t2) + 
            Power(s2,2)*t1*(19*t1 + 8*t2)) - 
         Power(s1,6)*(Power(s2,3)*(t1 + 2*t2) + 
            Power(t1,2)*t2*(-3*t1 + 5*t2) - 
            Power(s2,2)*t1*(2*t1 + 7*t2) + 
            s2*t1*(Power(t1,2) + 8*t1*t2 - 4*Power(t2,2))) - 
         s1*s2*Power(t2,5)*(Power(s2,3) + 2*t1*(3*t1 - t2)*t2 + 
            2*Power(s2,2)*(5*t1 + t2) + 
            s2*(-12*Power(t1,2) - 6*t1*t2 + Power(t2,2))) + 
         Power(s1,2)*Power(t2,4)*
          (4*Power(s2,4) + s2*t1*(21*t1 - 5*t2)*t2 - 
            2*Power(t1,2)*t2*(t1 + t2) + Power(s2,3)*(t1 + 4*t2) + 
            Power(s2,2)*(-6*Power(t1,2) - 15*t1*t2 + 2*Power(t2,2))) + 
         Power(s1,3)*Power(t2,3)*
          (-6*Power(s2,4) + Power(s2,3)*(16*t1 + t2) + 
            Power(t1,2)*t2*(5*t1 + 6*t2) - 
            Power(s2,2)*(14*Power(t1,2) - 9*t1*t2 + Power(t2,2)) + 
            s2*t1*(4*Power(t1,2) - 24*t1*t2 + 5*Power(t2,2))) - 
         Power(s1,5)*t2*(Power(s2,4) + 2*Power(t1,2)*(t1 - 5*t2)*t2 - 
            7*Power(s2,3)*(t1 + t2) + 2*Power(s2,2)*t1*(5*t1 + 7*t2) + 
            s2*t1*(-4*Power(t1,2) - 8*t1*t2 + 5*Power(t2,2))) + 
         Power(s,6)*(3*Power(t1,3)*t2 - t1*Power(t2,3) - 
            s2*(Power(t1,3) + Power(t1,2)*t2 - t1*Power(t2,2) + 
               2*Power(t2,3)) + 
            s1*(-7*Power(t1,3) + Power(t1,2)*t2 - t1*Power(t2,2) - 
               4*Power(t2,3) + 
               s2*(2*Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)))) + 
         s*(Power(s1,6)*(s2*t1*(-12*t1 + t2) + 
               Power(s2,2)*(5*t1 + 2*t2) + 
               t1*(7*Power(t1,2) - 6*t1*t2 + Power(t2,2))) - 
            s2*Power(t2,5)*(Power(s2,3) - 14*Power(s2,2)*t1 - 
               2*t1*t2*(t1 + t2) + 
               s2*(12*Power(t1,2) + 6*t1*t2 + Power(t2,2))) + 
            2*s1*Power(t2,4)*
             (-(Power(s2,3)*(9*t1 + t2)) + 
               t1*t2*(2*Power(t1,2) + t1*t2 - Power(t2,2)) + 
               s2*t2*(-15*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(9*Power(t1,2) + 16*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,5)*(Power(s2,3)*(4*t1 + 9*t2) + 
               t1*t2*(-18*Power(t1,2) + 25*t1*t2 - 5*Power(t2,2)) + 
               s2*t1*(6*Power(t1,2) + 41*t1*t2 - Power(t2,2)) - 
               2*Power(s2,2)*(5*Power(t1,2) + 18*t1*t2 + 5*Power(t2,2))) \
+ Power(s1,4)*t2*(3*Power(s2,4) - Power(s2,3)*(25*t1 + 28*t2) + 
               10*t1*t2*(Power(t1,2) - 4*t1*t2 + Power(t2,2)) - 
               s2*t1*(20*Power(t1,2) + 35*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*(41*Power(t1,2) + 68*t1*t2 + 14*Power(t2,2))) \
+ Power(s1,3)*Power(t2,2)*(-9*Power(s2,4) + 
               Power(s2,3)*(48*t1 + 26*t2) + 
               t1*t2*(8*Power(t1,2) + 36*t1*t2 - 9*Power(t2,2)) - 
               Power(s2,2)*(61*Power(t1,2) + 40*t1*t2 + 
                  3*Power(t2,2)) + 
               s2*(24*Power(t1,3) - 16*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + Power(t2,3))) - 
            Power(s1,2)*Power(t2,3)*
             (-7*Power(s2,4) + Power(s2,3)*(26*t1 + 5*t2) + 
               5*t1*t2*(3*Power(t1,2) + 3*t1*t2 - Power(t2,2)) + 
               Power(s2,2)*(-30*Power(t1,2) + 27*t1*t2 + 
                  8*Power(t2,2)) + 
               s2*(12*Power(t1,3) - 61*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + 3*Power(t2,3)))) + 
         Power(s,2)*(Power(t2,4)*
             (-2*Power(s2,4) + Power(s2,3)*(17*t1 + 4*t2) - 
               2*t1*t2*(Power(t1,2) + Power(t2,2)) + 
               s2*t2*(9*Power(t1,2) + 9*t1*t2 + 2*Power(t2,2)) - 
               Power(s2,2)*(12*Power(t1,2) + 25*t1*t2 + 2*Power(t2,2))) \
+ Power(s1,5)*(-2*Power(s2,2)*(5*t1 + 4*t2) + 
               t1*(-21*Power(t1,2) + 15*t1*t2 - 5*Power(t2,2)) + 
               s2*(30*Power(t1,2) + 10*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*t2*(-3*Power(s2,4) + 
               Power(s2,3)*(33*t1 + 39*t2) + 
               t1*t2*(-20*Power(t1,2) + 60*t1*t2 - 19*Power(t2,2)) - 
               Power(s2,2)*(64*Power(t1,2) + 119*t1*t2 + 
                  39*Power(t2,2)) + 
               2*s2*(20*Power(t1,3) + 30*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) + Power(t2,3))) + 
            Power(s1,2)*Power(t2,2)*
             (6*Power(s2,4) - Power(s2,3)*(45*t1 + 23*t2) + 
               Power(s2,2)*(69*Power(t1,2) + 48*t1*t2 + Power(t2,2)) + 
               t2*(-12*Power(t1,3) - 48*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + Power(t2,3)) + 
               s2*(-36*Power(t1,3) + 12*Power(t1,2)*t2 + 
                  19*t1*Power(t2,2) + 5*Power(t2,3))) - 
            s1*Power(t2,3)*(Power(s2,4) - 6*Power(s2,3)*(t1 - t2) + 
               Power(s2,2)*(18*Power(t1,2) - 53*t1*t2 - 
                  11*Power(t2,2)) + 
               t2*(-15*Power(t1,3) - 12*Power(t1,2)*t2 - 
                  2*t1*Power(t2,2) + Power(t2,3)) + 
               s2*(-12*Power(t1,3) + 50*Power(t1,2)*t2 + 
                  35*t1*Power(t2,2) + 5*Power(t2,3))) - 
            Power(s1,4)*(3*Power(s2,3)*(2*t1 + 5*t2) + 
               t1*t2*(-45*Power(t1,2) + 50*t1*t2 - 19*Power(t2,2)) - 
               2*Power(s2,2)*
                (10*Power(t1,2) + 37*t1*t2 + 18*Power(t2,2)) + 
               s2*(15*Power(t1,3) + 85*Power(t1,2)*t2 + 
                  35*t1*Power(t2,2) + 7*Power(t2,3)))) + 
         Power(s,5)*(Power(s1,2)*
             (21*Power(t1,3) - 6*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
               6*Power(t2,3) + Power(s2,2)*(t1 + 2*t2) - 
               s2*(12*Power(t1,2) + 19*t1*t2 + 12*Power(t2,2))) + 
            t2*(2*Power(t1,3)*t2 - 11*t1*Power(t2,3) - Power(t2,4) + 
               Power(s2,2)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
               s2*(-4*Power(t1,3) - 3*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + 4*Power(t2,3))) + 
            s1*(-(t1*t2*(18*Power(t1,2) - 5*t1*t2 + Power(t2,2))) - 
               2*Power(s2,2)*(Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)) + 
               s2*(6*Power(t1,3) + 13*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) + 13*Power(t2,3)))) + 
         Power(s,3)*(Power(s1,4)*
             (35*Power(t1,3) - 20*Power(t1,2)*t2 + 10*t1*Power(t2,2) + 
               Power(t2,3) + 2*Power(s2,2)*(5*t1 + 6*t2) - 
               2*s2*(20*Power(t1,2) + 15*t1*t2 + 6*Power(t2,2))) - 
            Power(s1,2)*t2*(-Power(s2,4) + 
               Power(s2,3)*(19*t1 + 22*t2) + 
               2*s2*t1*(20*Power(t1,2) + 25*t1*t2 + 23*Power(t2,2)) - 
               Power(s2,2)*(46*Power(t1,2) + 89*t1*t2 + 
                  37*Power(t2,2)) + 
               t2*(-20*Power(t1,3) + 40*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + Power(t2,3))) - 
            Power(t2,3)*(-4*Power(s2,3)*(t1 + 2*t2) + 
               Power(s2,2)*(-2*Power(t1,2) + 35*t1*t2 + 
                  6*Power(t2,2)) + 
               s2*(4*Power(t1,3) - 13*Power(t1,2)*t2 - 
                  23*t1*Power(t2,2) - 4*Power(t2,3)) + 
               t2*(5*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) + Power(t2,3))) + 
            s1*Power(t2,2)*(-Power(s2,4) + Power(s2,3)*(14*t1 + 5*t2) + 
               Power(s2,2)*(-31*Power(t1,2) - 8*t1*t2 + 
                  12*Power(t2,2)) + 
               s2*(24*Power(t1,3) - 53*t1*Power(t2,2) - 
                  10*Power(t2,3)) + 
               t2*(8*Power(t1,3) + 28*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*(Power(s2,3)*(4*t1 + 11*t2) - 
               4*Power(s2,2)*
                (5*Power(t1,2) + 19*t1*t2 + 12*Power(t2,2)) - 
               2*t1*t2*(30*Power(t1,2) - 25*t1*t2 + 13*Power(t2,2)) + 
               s2*(20*Power(t1,3) + 90*Power(t1,2)*t2 + 
                  70*t1*Power(t2,2) + 23*Power(t2,3)))) - 
         Power(s,4)*(Power(s1,3)*
             (35*Power(t1,3) - 15*Power(t1,2)*t2 + 10*t1*Power(t2,2) + 
               4*Power(t2,3) + Power(s2,2)*(5*t1 + 8*t2) - 
               s2*(30*Power(t1,2) + 35*t1*t2 + 18*Power(t2,2))) + 
            Power(t2,2)*(Power(s2,2)*
                (-4*Power(t1,2) + 8*t1*t2 + 10*Power(t2,2)) + 
               s2*(6*Power(t1,3) + 2*Power(t1,2)*t2 - 
                  29*t1*Power(t2,2) - 4*Power(t2,3)) + 
               2*t2*(Power(t1,3) + 3*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
                  Power(t2,3))) - 
            s1*t2*(4*Power(s2,3)*(t1 + t2) - 
               Power(s2,2)*(14*Power(t1,2) + 23*t1*t2 + 
                  13*Power(t2,2)) + 
               s2*(20*Power(t1,3) + 20*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) - 6*Power(t2,3)) + 
               t2*(-10*Power(t1,3) + 10*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,3)*(t1 + 3*t2) + 
               t1*t2*(-45*Power(t1,2) + 25*t1*t2 - 14*Power(t2,2)) - 
               Power(s2,2)*(10*Power(t1,2) + 39*t1*t2 + 28*Power(t2,2)) + 
               s2*(15*Power(t1,3) + 50*Power(t1,2)*t2 + 
                  50*t1*Power(t2,2) + 27*Power(t2,3)))))*dilog(1 - s1/s))/
     ((s - s1)*s1*s2*t1*(s - s2 + t1)*Power(t2,3)*Power(s - s1 + t2,3)) - 
    (16*(-(Power(s1,4)*s2*Power(s2 - t1,4)*t1) + 
         Power(s,7)*(Power(t1,3) + Power(t2,3)) - 
         Power(s2,2)*Power(t2,2)*
          (Power(s2,4)*t1*(5*t1 - 3*t2) + 
            2*Power(s2,3)*Power(t1,2)*(-5*t1 + t2) + 
            Power(s2,5)*(-t1 + t2) + 2*Power(t1,5)*(t1 + t2) + 
            2*Power(s2,2)*Power(t1,3)*(5*t1 + t2) - 
            s2*Power(t1,4)*(6*t1 + 5*t2)) - 
         Power(s1,3)*(s2 - t1)*
          (Power(s2,2)*Power(t1,3)*(2*t1 - 5*t2) + 4*Power(t1,5)*t2 + 
            Power(s2,5)*(2*t1 + t2) - 2*s2*Power(t1,4)*(t1 + 3*t2) - 
            Power(s2,4)*t1*(5*t1 + 6*t2) + 
            Power(s2,3)*Power(t1,2)*(3*t1 + 11*t2)) + 
         s1*s2*t2*(2*Power(t1,6)*(t1 - 3*t2) - Power(s2,6)*(t1 - 2*t2) + 
            6*Power(s2,3)*Power(t1,2)*(t1 - t2)*t2 + 
            s2*Power(t1,5)*(-5*t1 + 21*t2) + 
            Power(s2,5)*(4*Power(t1,2) - 8*t1*t2 - Power(t2,2)) + 
            Power(s2,2)*Power(t1,3)*
             (5*Power(t1,2) - 24*t1*t2 + 4*Power(t2,2)) + 
            Power(s2,4)*t1*(-5*Power(t1,2) + 8*t1*t2 + 4*Power(t2,2))) - 
         Power(s1,2)*(Power(s2,7)*t2 + 4*Power(t1,6)*Power(t2,2) - 
            Power(s2,6)*t2*(7*t1 + 2*t2) + 
            2*Power(s2,5)*t1*t2*(7*t1 + 5*t2) - 
            Power(s2,4)*Power(t1,2)*t2*(8*t1 + 19*t2) + 
            s2*Power(t1,5)*(Power(t1,2) - 6*t1*t2 - 12*Power(t2,2)) + 
            Power(s2,2)*Power(t1,4)*
             (-2*Power(t1,2) + 15*t1*t2 + 6*Power(t2,2)) + 
            Power(s2,3)*Power(t1,3)*
             (Power(t1,2) - 9*t1*t2 + 14*Power(t2,2))) + 
         Power(s,6)*(-(Power(t1,3)*t2) + 3*t1*Power(t2,3) - 
            s2*(4*Power(t1,3) + Power(t1,2)*t2 - t1*Power(t2,2) + 
               7*Power(t2,3)) + 
            s1*(-2*Power(t1,3) + Power(t1,2)*t2 - t1*Power(t2,2) - 
               Power(t2,3) + s2*(3*Power(t1,2) + 4*t1*t2 + 2*Power(t2,2))\
)) + Power(s,5)*(-(s2*t1*t2*(Power(t1,2) - 5*t1*t2 + 18*Power(t2,2))) - 
            Power(t1,2)*(Power(t1,3) + 11*Power(t1,2)*t2 - 
               2*Power(t2,3)) + 
            Power(s2,2)*(6*Power(t1,3) + 5*Power(t1,2)*t2 - 
               6*t1*Power(t2,2) + 21*Power(t2,3)) + 
            Power(s1,2)*(Power(s2,2)*(2*t1 + t2) + 
               t1*(Power(t1,2) - t1*t2 + Power(t2,2)) - 
               2*s2*(3*Power(t1,2) + 4*t1*t2 + Power(t2,2))) + 
            s1*(-(Power(s2,2)*
                  (12*Power(t1,2) + 19*t1*t2 + 12*Power(t2,2))) + 
               t1*(4*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) - 4*Power(t2,3)) + 
               s2*(13*Power(t1,3) + 11*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 6*Power(t2,3)))) - 
         Power(s,4)*(Power(s2,2)*t1*t2*
             (-14*Power(t1,2) + 25*t1*t2 - 45*Power(t2,2)) - 
            s2*Power(t1,2)*(2*Power(t1,3) + 23*Power(t1,2)*t2 + 
               10*t1*Power(t2,2) - 10*Power(t2,3)) + 
            2*Power(t1,3)*(Power(t1,3) + 6*Power(t1,2)*t2 + 
               3*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s2,3)*(4*Power(t1,3) + 10*Power(t1,2)*t2 - 
               15*t1*Power(t2,2) + 35*Power(t2,3)) + 
            Power(s1,3)*s2*(-4*t1*(t1 + t2) + s2*(3*t1 + t2)) + 
            Power(s1,2)*(Power(s2,3)*(8*t1 + 5*t2) + 
               2*Power(t1,2)*
                (5*Power(t1,2) + 4*t1*t2 - 2*Power(t2,2)) - 
               Power(s2,2)*(28*Power(t1,2) + 39*t1*t2 + 
                  10*Power(t2,2)) + 
               s2*t1*(13*Power(t1,2) + 23*t1*t2 + 14*Power(t2,2))) + 
            s1*(-(Power(s2,3)*
                  (18*Power(t1,2) + 35*t1*t2 + 30*Power(t2,2))) + 
               s2*t1*(6*Power(t1,3) - 7*Power(t1,2)*t2 - 
                  20*t1*Power(t2,2) - 20*Power(t2,3)) + 
               Power(t1,2)*(-4*Power(t1,3) - 29*Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(27*Power(t1,3) + 50*Power(t1,2)*t2 + 
                  50*t1*Power(t2,2) + 15*Power(t2,3)))) + 
         Power(s,2)*(-(Power(s1,4)*t1*
               (3*Power(s2,3) - 6*Power(s2,2)*t1 + s2*Power(t1,2) + 
                 2*Power(t1,3))) + 
            Power(s2,5)*t2*(-5*Power(t1,2) + 15*t1*t2 - 
               21*Power(t2,2)) + 
            Power(s2,3)*Power(t1,2)*t2*
             (-19*Power(t1,2) + 60*t1*t2 - 20*Power(t2,2)) - 
            2*Power(t1,5)*t2*(Power(t1,2) + Power(t2,2)) + 
            Power(s2,4)*t1*t2*
             (19*Power(t1,2) - 50*t1*t2 + 45*Power(t2,2)) + 
            Power(s2,2)*Power(t1,3)*
             (Power(t1,3) + 6*Power(t1,2)*t2 - 48*t1*Power(t2,2) - 
               12*Power(t2,3)) + 
            s2*Power(t1,4)*(-Power(t1,3) + 2*Power(t1,2)*t2 + 
               12*t1*Power(t2,2) + 15*Power(t2,3)) + 
            Power(s1,3)*(-6*s2*Power(t1,3)*(t1 - t2) - 
               3*Power(s2,4)*(5*t1 + 2*t2) + 
               3*Power(s2,3)*t1*(13*t1 + 11*t2) + 
               Power(t1,4)*(4*t1 + 17*t2) - 
               Power(s2,2)*Power(t1,2)*(23*t1 + 45*t2)) + 
            Power(s1,2)*(-2*Power(s2,5)*(4*t1 + 5*t2) + 
               s2*Power(t1,3)*
                (11*Power(t1,2) + 53*t1*t2 - 18*Power(t2,2)) - 
               Power(t1,4)*(2*Power(t1,2) + 25*t1*t2 + 
                  12*Power(t2,2)) + 
               Power(s2,4)*(36*Power(t1,2) + 74*t1*t2 + 
                  20*Power(t2,2)) - 
               Power(s2,3)*t1*
                (39*Power(t1,2) + 119*t1*t2 + 64*Power(t2,2)) + 
               Power(s2,2)*Power(t1,2)*
                (Power(t1,2) + 48*t1*t2 + 69*Power(t2,2))) + 
            s1*(Power(t1,5)*(2*Power(t1,2) + 9*t1*t2 + 9*Power(t2,2)) + 
               Power(s2,5)*(3*Power(t1,2) + 10*t1*t2 + 30*Power(t2,2)) + 
               Power(s2,2)*Power(t1,2)*
                (5*Power(t1,3) + 19*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) - 36*Power(t2,3)) - 
               s2*Power(t1,3)*
                (5*Power(t1,3) + 35*Power(t1,2)*t2 + 
                  50*t1*Power(t2,2) - 12*Power(t2,3)) - 
               Power(s2,4)*(7*Power(t1,3) + 35*Power(t1,2)*t2 + 
                  85*t1*Power(t2,2) + 15*Power(t2,3)) + 
               2*Power(s2,3)*t1*
                (Power(t1,3) + 23*Power(t1,2)*t2 + 30*t1*Power(t2,2) + 
                  20*Power(t2,3)))) + 
         s*(-(Power(s1,4)*t1*
               (-3*Power(s2,4) + 9*Power(s2,3)*t1 - 
                 7*Power(s2,2)*Power(t1,2) + Power(t1,4))) + 
            Power(s1,3)*(14*Power(t1,5)*t2 + 
               Power(s2,5)*(9*t1 + 4*t2) - 
               2*s2*Power(t1,4)*(t1 + 9*t2) + 
               2*Power(s2,3)*Power(t1,2)*(13*t1 + 24*t2) - 
               Power(s2,4)*t1*(28*t1 + 25*t2) - 
               Power(s2,2)*Power(t1,3)*(5*t1 + 26*t2)) + 
            s2*t2*(Power(s2,4)*t1*
                (-5*Power(t1,2) + 25*t1*t2 - 18*Power(t2,2)) + 
               5*s2*Power(t1,4)*
                (Power(t1,2) - 3*t1*t2 - 3*Power(t2,2)) + 
               10*Power(s2,3)*Power(t1,2)*
                (Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               2*Power(t1,5)*(-Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
               Power(s2,5)*(Power(t1,2) - 6*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*Power(t1,3)*
                (-9*Power(t1,2) + 36*t1*t2 + 8*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,6)*(2*t1 + 5*t2) - 
               2*Power(s2,5)*
                (5*Power(t1,2) + 18*t1*t2 + 5*Power(t2,2)) + 
               2*s2*Power(t1,4)*
                (3*Power(t1,2) + 16*t1*t2 + 9*Power(t2,2)) - 
               Power(t1,5)*(Power(t1,2) + 6*t1*t2 + 12*Power(t2,2)) + 
               Power(s2,2)*Power(t1,3)*
                (-8*Power(t1,2) - 27*t1*t2 + 30*Power(t2,2)) + 
               Power(s2,4)*t1*
                (14*Power(t1,2) + 68*t1*t2 + 41*Power(t2,2)) - 
               Power(s2,3)*Power(t1,2)*
                (3*Power(t1,2) + 40*t1*t2 + 61*Power(t2,2))) + 
            s1*(Power(s2,6)*(t1 - 12*t2)*t2 + 
               2*Power(t1,6)*t2*(t1 + t2) + 
               2*s2*Power(t1,5)*
                (Power(t1,2) - 4*t1*t2 - 15*Power(t2,2)) + 
               Power(s2,5)*t2*
                (-Power(t1,2) + 41*t1*t2 + 6*Power(t2,2)) - 
               Power(s2,4)*t1*t2*
                (7*Power(t1,2) + 35*t1*t2 + 20*Power(t2,2)) + 
               Power(s2,2)*Power(t1,3)*
                (-3*Power(t1,3) + 7*Power(t1,2)*t2 + 
                  61*t1*Power(t2,2) - 12*Power(t2,3)) + 
               Power(s2,3)*Power(t1,2)*
                (Power(t1,3) + 5*Power(t1,2)*t2 - 16*t1*Power(t2,2) + 
                  24*Power(t2,3)))) + 
         Power(s,3)*(Power(s1,4)*s2*(s2 - t1)*t1 - 
            2*Power(s2,3)*t1*t2*
             (13*Power(t1,2) - 25*t1*t2 + 30*Power(t2,2)) - 
            Power(s2,2)*Power(t1,2)*
             (Power(t1,3) + 3*Power(t1,2)*t2 + 40*t1*Power(t2,2) - 
               20*Power(t2,3)) - 
            Power(t1,4)*(Power(t1,3) + 7*Power(t1,2)*t2 + 
               3*t1*Power(t2,2) + 5*Power(t2,3)) + 
            s2*Power(t1,3)*(Power(t1,3) + 15*Power(t1,2)*t2 + 
               28*t1*Power(t2,2) + 8*Power(t2,3)) + 
            Power(s2,4)*(Power(t1,3) + 10*Power(t1,2)*t2 - 
               20*t1*Power(t2,2) + 35*Power(t2,3)) + 
            Power(s1,3)*(4*Power(t1,3)*(2*t1 + t2) + 
               Power(s2,3)*(11*t1 + 4*t2) + 
               s2*Power(t1,2)*(5*t1 + 14*t2) - 
               Power(s2,2)*t1*(22*t1 + 19*t2)) + 
            Power(s1,2)*(2*Power(s2,4)*(6*t1 + 5*t2) + 
               s2*Power(t1,2)*
                (12*Power(t1,2) - 8*t1*t2 - 31*Power(t2,2)) + 
               Power(t1,3)*(-6*Power(t1,2) - 35*t1*t2 + 2*Power(t2,2)) - 
               4*Power(s2,3)*
                (12*Power(t1,2) + 19*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*t1*
                (37*Power(t1,2) + 89*t1*t2 + 46*Power(t2,2))) + 
            s1*(-2*Power(s2,4)*
                (6*Power(t1,2) + 15*t1*t2 + 20*Power(t2,2)) - 
               2*Power(s2,2)*t1*t2*
                (23*Power(t1,2) + 25*t1*t2 + 20*Power(t2,2)) + 
               Power(t1,3)*(4*Power(t1,3) + 23*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,3)*(23*Power(t1,3) + 70*Power(t1,2)*t2 + 
                  90*t1*Power(t2,2) + 20*Power(t2,3)) + 
               s2*(-10*Power(t1,5) - 53*Power(t1,4)*t2 + 
                  24*Power(t1,2)*Power(t2,3)))))*dilog(1 - s2/s))/
     (s1*(s - s2)*s2*Power(t1,3)*Power(s - s2 + t1,3)*(-s + s1 - t2)*t2) - 
    (16*(Power(s1,6)*(s2 + t1) + 
         Power(s1,5)*(3*Power(s2,2) + s2*(t1 - 3*t2) + 
            2*t1*(t1 - 2*t2)) - 
         s2*t1*Power(t2,3)*(2*Power(s2,2) - 2*s2*t2 + Power(t2,2)) + 
         s1*Power(t2,2)*(Power(s2,3)*(7*t1 - 3*t2) - 
            7*Power(s2,2)*t1*t2 - s2*t1*(t1 - 7*t2)*t2 + 
            t1*(t1 - 2*t2)*Power(t2,2)) + 
         Power(s,4)*(Power(s1,2)*(t1 - t2) + 
            s1*(2*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
            t1*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,2)*t2*(Power(s2,3)*(-7*t1 + 10*t2) + 
            s2*t1*(-2*Power(t1,2) + t1*t2 - 14*Power(t2,2)) + 
            Power(s2,2)*(Power(t1,2) + 8*t1*t2 - 2*Power(t2,2)) + 
            t1*t2*(Power(t1,2) - 4*t1*t2 + 8*Power(t2,2))) + 
         Power(s1,4)*(4*Power(s2,3) - 8*Power(s2,2)*t2 + 
            s2*(2*Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
            t1*(2*Power(t1,2) - 5*t1*t2 + 9*Power(t2,2))) + 
         Power(s1,3)*(Power(s2,3)*(2*t1 - 11*t2) + 
            Power(s2,2)*t2*(-3*t1 + 7*t2) - 
            3*t1*t2*(Power(t1,2) - 2*t1*t2 + 4*Power(t2,2)) + 
            s2*(2*Power(t1,3) - Power(t1,2)*t2 + 11*t1*Power(t2,2) - 
               Power(t2,3))) - 
         Power(s,3)*(Power(s1,3)*(s2 + 4*t1 - 3*t2) + 
            Power(s1,2)*(8*Power(t1,2) + s2*(t1 - 5*t2) - 19*t1*t2 + 
               4*Power(t2,2)) + 
            s1*(8*Power(t1,3) - 20*Power(t1,2)*t2 + 20*t1*Power(t2,2) - 
               Power(t2,3) + 
               s2*(2*Power(t1,2) - 15*t1*t2 + 5*Power(t2,2))) + 
            t1*(-3*t2*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
               s2*(2*Power(t1,2) - 9*t1*t2 + 8*Power(t2,2)))) + 
         Power(s,2)*(3*Power(s1,4)*(s2 + 2*t1 - t2) + 
            Power(s1,3)*(3*Power(s2,2) + 3*s2*t1 + 12*Power(t1,2) - 
               13*s2*t2 - 27*t1*t2 + 5*Power(t2,2)) + 
            t1*t2*(Power(s2,2)*(-4*t1 + 7*t2) + 
               s2*(-2*Power(t1,2) + 12*t1*t2 - 13*Power(t2,2)) + 
               t2*(Power(t1,2) - 5*t1*t2 + 4*Power(t2,2))) + 
            Power(s1,2)*(12*Power(t1,3) - 11*Power(s2,2)*t2 - 
               30*Power(t1,2)*t2 + 40*t1*Power(t2,2) - 2*Power(t2,3) + 
               2*s2*(3*Power(t1,2) - 17*t1*t2 + 7*Power(t2,2))) + 
            s1*(Power(s2,3)*(t1 + t2) + Power(s2,2)*t2*(-16*t1 + 9*t2) - 
               3*t1*t2*(3*Power(t1,2) - 8*t1*t2 + 8*Power(t2,2)) + 
               s2*(6*Power(t1,3) - 19*Power(t1,2)*t2 + 
                  37*t1*Power(t2,2) - 4*Power(t2,3)))) + 
         s*(Power(s1,5)*(-3*s2 - 4*t1 + t2) - 
            Power(s1,4)*(6*Power(s2,2) + 3*s2*t1 + 8*Power(t1,2) - 
               11*s2*t2 - 17*t1*t2 + 2*Power(t2,2)) + 
            t1*Power(t2,2)*(-2*Power(s2,3) + 3*s2*(t1 - 2*t2)*t2 + 
               Power(t2,2)*(-t1 + t2) + Power(s2,2)*(-4*t1 + 9*t2)) + 
            Power(s1,3)*(-4*Power(s2,3) - 8*Power(t1,3) + 
               19*Power(s2,2)*t2 + 20*Power(t1,2)*t2 - 
               32*t1*Power(t2,2) + Power(t2,3) + 
               s2*(-6*Power(t1,2) + 23*t1*t2 - 12*Power(t2,2))) - 
            s1*t2*(Power(s2,4) + Power(s2,3)*(-8*t1 + 5*t2) + 
               s2*t1*(-4*Power(t1,2) + 13*t1*t2 - 27*Power(t2,2)) - 
               3*Power(s2,2)*(Power(t1,2) - 8*t1*t2 + 2*Power(t2,2)) + 
               t1*t2*(2*Power(t1,2) - 9*t1*t2 + 12*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,4) - 3*Power(s2,3)*(t1 - 3*t2) + 
               19*Power(s2,2)*(t1 - t2)*t2 + 
               3*t1*t2*(3*Power(t1,2) - 7*t1*t2 + 10*Power(t2,2)) + 
               s2*(-6*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  40*t1*Power(t2,2) + 4*Power(t2,3)))))*
       dilog(1 - s2/(-s + s2 - t1)))/
     (s1*s2*(-s + s2 - t1)*t1*t2*Power(s - s1 + t2,3)) + 
    (16*(Power(s1,4)*Power(s2 - t1,2)*t1 + 
         Power(s,4)*(2*Power(t1,3) + Power(t1,2)*t2 + Power(t2,3)) + 
         s2*Power(t2,2)*(-3*s2*Power(t1,3) + Power(t1,3)*(t1 - t2) + 
            Power(s2,2)*t1*(3*t1 - t2) + Power(s2,3)*(-t1 + t2)) + 
         Power(s1,2)*t2*(Power(s2,4) + s2*Power(t1,2)*(t1 - 7*t2) - 
            Power(t1,3)*(t1 - 4*t2) + 3*Power(s2,2)*t1*(t1 + 2*t2) - 
            Power(s2,3)*(5*t1 + 2*t2)) + 
         Power(s1,3)*(s2 - t1)*
          (3*Power(t1,2)*t2 + Power(s2,2)*(2*t1 + t2) - 
            s2*t1*(t1 + 4*t2)) + 
         s1*t2*(Power(s2,4)*(t1 - 2*t2) + 
            Power(s2,2)*t1*(t1 - 2*t2)*t2 + 
            2*s2*Power(t1,2)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(s2,3)*(-2*Power(t1,2) + 4*t1*t2 + Power(t2,2)) - 
            Power(t1,3)*(Power(t1,2) - t1*t2 + 2*Power(t2,2))) + 
         Power(s,3)*(-2*s2*Power(t1,3) - 3*s2*Power(t1,2)*t2 + 
            7*Power(t1,3)*t2 + s2*t1*Power(t2,2) - 4*s2*Power(t2,3) + 
            t1*Power(t2,3) - s1*
             (6*Power(t1,3) + t1*Power(t2,2) + Power(t2,3) - 
               2*s2*(2*Power(t1,2) + 2*t1*t2 + Power(t2,2)))) - 
         s*(Power(s1,3)*(Power(s2,2)*(3*t1 + t2) + 
               Power(t1,2)*(4*t1 + t2) - s2*t1*(7*t1 + 4*t2)) + 
            t2*(3*s2*Power(t1,4) - 3*Power(s2,2)*t1*Power(t1 - t2,2) - 
               Power(t1,3)*(Power(t1,2) + Power(t2,2)) + 
               Power(s2,3)*(Power(t1,2) - 3*t1*t2 + 4*Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,3)*(t1 + t2) - 
               Power(t1,2)*t2*(7*t1 + 3*t2) - 
               Power(s2,2)*(7*Power(t1,2) + 13*t1*t2 + 4*Power(t2,2)) + 
               s2*t1*(4*Power(t1,2) + 10*t1*t2 + 7*Power(t2,2))) + 
            s1*t2*(-2*Power(s2,3)*(t1 + 3*t2) + 
               Power(t1,2)*(2*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,2) + 9*t1*t2 + 3*Power(t2,2)) - 
               s2*(7*Power(t1,3) + 4*t1*Power(t2,2)))) + 
         Power(s,2)*(t2*(3*Power(t1,3)*(t1 + t2) + 
               s2*t1*(-10*Power(t1,2) + 3*t1*t2 - 3*Power(t2,2)) + 
               3*Power(s2,2)*(Power(t1,2) - t1*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,2)*(2*t1 + t2) + 
               t1*(7*Power(t1,2) + Power(t2,2)) - 
               s2*(9*Power(t1,2) + 8*t1*t2 + 2*Power(t2,2))) - 
            s1*(t1*t2*(11*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,2) + 7*t1*t2 + 6*Power(t2,2)) - 
               s2*(5*Power(t1,3) + 6*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
                  3*Power(t2,3)))))*dilog(1 - (-s + s2 - t1)/s2))/
     (s1*s2*(-s + s2 - t1)*Power(t1,3)*(-s + s1 - t2)*t2) + 
    (16*(Power(s,4)*Power(t1 - t2,2)*t2 + 
         Power(s1,4)*(s2 - t1)*(s2 + t2)*(s2 - t1 + t2) + 
         s2*(s2 - t1)*Power(t2,3)*
          (2*Power(s2,2) + 2*s2*t2 + Power(t2,2)) + 
         Power(s1,3)*(Power(s2,4) - Power(t1,3)*t2 + 
            Power(s2,3)*(-2*t1 + 3*t2) + 
            s2*t2*(5*Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(Power(t1,2) - 7*t1*t2 + 2*Power(t2,2))) + 
         s1*Power(t2,2)*(Power(s2,4) - Power(s2,3)*(t1 - 3*t2) + 
            t1*(t1 - 2*t2)*Power(t2,2) + Power(s2,2)*t2*(-t1 + 3*t2) + 
            s2*t2*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2))) - 
         Power(s1,2)*t2*(Power(s2,3)*(3*t1 + t2) + 
            t1*t2*(Power(t1,2) - 2*Power(t2,2)) + 
            Power(s2,2)*(-5*Power(t1,2) - 2*t1*t2 + 2*Power(t2,2)) + 
            s2*(2*Power(t1,3) - Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
               2*Power(t2,3))) - 
         Power(s,3)*(-((t1 - t2)*t2*
               (-3*s2*t1 + Power(t1,2) + 4*s2*t2 - 2*t1*t2)) + 
            s1*(s2*(Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               2*t2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)))) - 
         s*(Power(s1,3)*(Power(s2,3) + t2*Power(-2*t1 + t2,2) + 
               Power(s2,2)*(-4*t1 + 3*t2) + 
               s2*(3*Power(t1,2) - 10*t1*t2 + 2*Power(t2,2))) + 
            Power(t2,2)*(t1*(t1 - t2)*Power(t2,2) + 
               Power(s2,3)*(-4*t1 + 6*t2) + 
               s2*t2*(3*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,2)*t1*(2*t1 - 15*t2) + 
               s2*t1*(13*t1 - 9*t2)*t2 + Power(s2,3)*(-2*t1 + t2) + 
               t2*(-3*Power(t1,3) + 3*Power(t1,2)*t2 - 2*Power(t2,3))) + 
            s1*t2*(-4*Power(s2,3)*t1 + 
               Power(s2,2)*(7*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
               s2*(-4*Power(t1,3) + 7*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  Power(t2,3)) + 
               t2*(-2*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  2*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,2)*
             (Power(s2,2)*(-2*t1 + t2) + 
               s2*(3*Power(t1,2) - 11*t1*t2 + 2*Power(t2,2)) + 
               t2*(6*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2))) + 
            t2*(Power(s2,2)*(2*Power(t1,2) - 9*t1*t2 + 7*Power(t2,2)) + 
               t2*(-Power(t1,3) + Power(t1,2)*t2 - t1*Power(t2,2) + 
                  Power(t2,3)) + 
               s2*(-2*Power(t1,3) + 6*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(Power(s2,2)*t1*(t1 - 8*t2) + 
               s2*t2*(11*Power(t1,2) - 15*t1*t2 + 2*Power(t2,2)) - 
               t2*(3*Power(t1,3) - 6*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
                  2*Power(t2,3)))))*dilog(1 - s2/t1))/
     (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*Power(t2,3)) - 
    (16*(Power(s,6)*(t1 + t2) + 
         Power(s,5)*(-3*s2*t1 + s1*(s2 - 4*t1 - t2) - 4*s2*t2 + 
            6*t1*t2) + s2*Power(t2,2)*
          (-(s2*Power(t1,2)*(t1 - 3*t2)) + Power(t1,3)*(t1 - t2) + 
            Power(s2,3)*t2 - 4*Power(s2,2)*t1*t2) + 
         Power(s1,3)*(s2 - t1)*
          (Power(t1,2)*t2 + Power(s2,2)*(t1 + t2) - s2*t1*(t1 + 3*t2)) + 
         s1*t2*(-(Power(s2,4)*t2) - 4*Power(s2,2)*t1*t2*(2*t1 + t2) + 
            Power(s2,3)*t2*(8*t1 + t2) - 
            Power(t1,3)*(Power(t1,2) - t1*t2 + 2*Power(t2,2)) + 
            s2*Power(t1,2)*(Power(t1,2) + t1*t2 + 4*Power(t2,2))) + 
         Power(s1,2)*(-2*Power(s2,3)*t2*(2*t1 + t2) + 
            Power(t1,3)*(Power(t1,2) - t1*t2 + 2*Power(t2,2)) - 
            s2*Power(t1,2)*(2*Power(t1,2) + 2*t1*t2 + 7*Power(t2,2)) + 
            Power(s2,2)*t1*(Power(t1,2) + 7*t1*t2 + 8*Power(t2,2))) + 
         Power(s,4)*(-18*s2*t1*t2 + Power(t1 + t2,3) + 
            Power(s1,2)*(-3*s2 + 6*t1 + t2) + 
            3*Power(s2,2)*(t1 + 2*t2) + 
            s1*(-3*Power(s2,2) - 12*t1*t2 + 2*s2*(5*t1 + t2))) + 
         Power(s,3)*(Power(s1,3)*(3*s2 - 4*t1 - t2) + 
            18*Power(s2,2)*t1*t2 - Power(s2,3)*(t1 + 4*t2) - 
            s2*Power(t1 + t2,2)*(t1 + 4*t2) + 
            2*(Power(t1,4) + Power(t1,3)*t2 + 2*t1*Power(t2,3)) + 
            Power(s1,2)*(7*Power(s2,2) - 2*s2*(6*t1 + t2) + 
               t2*(11*t1 + t2)) + 
            s1*(3*Power(s2,3) - 8*Power(s2,2)*t1 - 2*Power(t1,3) - 
               3*Power(t1,2)*t2 - 6*t1*Power(t2,2) - Power(t2,3) + 
               s2*(2*Power(t1,2) + 29*t1*t2 + Power(t2,2)))) + 
         Power(s,2)*(Power(t1,5) + Power(s1,4)*(-s2 + t1) + 
            Power(s2,4)*t2 - 6*Power(s2,3)*t1*t2 + 2*Power(t1,4)*t2 + 
            3*Power(t1,2)*Power(t2,3) + 
            3*Power(s2,2)*t2*(Power(t1,2) + 3*t1*t2 + 2*Power(t2,2)) - 
            s2*(Power(t1,4) + Power(t1,3)*t2 + 12*t1*Power(t2,3)) + 
            Power(s1,3)*(-5*Power(s2,2) - 5*t1*t2 + 3*s2*(2*t1 + t2)) + 
            Power(s1,2)*(-5*Power(s2,3) + Power(s2,2)*(8*t1 + t2) - 
               s2*(3*Power(t1,2) + 21*t1*t2 + 4*Power(t2,2)) + 
               t1*(Power(t1,2) + 2*t1*t2 + 5*Power(t2,2))) - 
            s1*(Power(s2,4) - 2*Power(s2,3)*(t1 - t2) + 
               Power(s2,2)*(2*Power(t1,2) + 22*t1*t2 + 3*Power(t2,2)) - 
               s2*(5*Power(t1,3) + 12*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(4*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  4*Power(t2,3)))) + 
         s*(Power(s1,4)*s2*(s2 - t1) + 
            Power(s1,3)*(2*Power(s2,3) - 2*Power(t1,2)*t2 - 
               3*Power(s2,2)*(t1 + t2) + s2*t1*(t1 + 8*t2)) + 
            Power(s1,2)*(Power(s2,4) - 2*Power(s2,3)*t1 + 
               2*Power(t1,4) + 3*Power(t1,2)*Power(t2,2) + 
               Power(s2,2)*(4*Power(t1,2) + 14*t1*t2 + 5*Power(t2,2)) - 
               s2*t1*(5*Power(t1,2) + 10*t1*t2 + 13*Power(t2,2))) + 
            t2*(s2*Power(t1,2)*(t1 - 6*t2)*t2 - 
               Power(s2,3)*t2*(3*t1 + 4*t2) + 
               Power(t1,3)*(Power(t1,2) + Power(t2,2)) - 
               Power(s2,2)*(Power(t1,3) - 12*t1*Power(t2,2))) + 
            s1*(Power(s2,4)*t2 + Power(s2,3)*t2*(5*t1 + 3*t2) - 
               Power(s2,2)*(Power(t1,3) + 9*Power(t1,2)*t2 + 
                  22*t1*Power(t2,2) + 3*Power(t2,3)) - 
               Power(t1,2)*(2*Power(t1,3) + Power(t1,2)*t2 + 
                  4*Power(t2,3)) + 
               s2*t1*(3*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + 8*Power(t2,3)))))*dilog(1 - t1/s2))/
     (s1*s2*t1*Power(s - s2 + t1,3)*(-s + s1 - t2)*t2) - 
    (16*(Power(s1,3)*(s2 - t1)*
          (4*Power(s2,3) + s2*t1*(3*t1 - 5*t2) + 2*Power(t1,2)*t2 + 
            Power(s2,2)*(-7*t1 + 2*t2)) + 
         Power(s1,2)*(3*Power(s2,5) - 8*Power(s2,4)*t1 + 
            Power(s2,3)*t1*(7*t1 - 3*t2) - 7*s2*Power(t1,3)*t2 + 
            2*Power(t1,4)*t2 + 
            Power(s2,2)*t1*(-2*Power(t1,2) + 8*t1*t2 + Power(t2,2))) + 
         Power(s,4)*(Power(s2,2)*(-t1 + t2) + 
            s2*(Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) + 
            t2*(3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) + 
         s2*(s2 - t1)*t2*(Power(s2,4) + Power(t1,3)*(2*t1 - t2) + 
            Power(s2,3)*(-3*t1 + 2*t2) - 
            s2*t1*(6*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(6*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2))) + 
         s1*(Power(s2,6) - Power(t1,5)*t2 + 
            s2*Power(t1,3)*(7*t1 - t2)*t2 + Power(s2,5)*(-3*t1 + t2) + 
            Power(s2,2)*t1*t2*
             (-14*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
            Power(s2,4)*(3*Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) - 
            Power(s2,3)*(Power(t1,3) - 11*Power(t1,2)*t2 + 
               t1*Power(t2,2) - 2*Power(t2,3))) - 
         Power(s,3)*(Power(s2,3)*(-3*t1 + 4*t2) - 
            3*t1*t2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(4*Power(t1,2) - 19*t1*t2 + 8*Power(t2,2)) - 
            s2*(Power(t1,3) - 20*Power(t1,2)*t2 + 20*t1*Power(t2,2) - 
               8*Power(t2,3)) + 
            s1*(Power(s2,3) + Power(s2,2)*(-5*t1 + t2) + 
               s2*(5*Power(t1,2) - 15*t1*t2 + 2*Power(t2,2)) + 
               t2*(8*Power(t1,2) - 9*t1*t2 + 2*Power(t2,2)))) + 
         s*(Power(s1,4)*s2*(s2 - t1) + Power(s2,5)*(t1 - 4*t2) + 
            Power(t1,4)*(t1 - t2)*t2 + 
            Power(s2,4)*(-2*Power(t1,2) + 17*t1*t2 - 8*Power(t2,2)) + 
            s2*Power(t1,2)*t2*
             (-12*Power(t1,2) + 9*t1*t2 - 2*Power(t2,2)) + 
            3*Power(s2,2)*t1*t2*
             (10*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,3)*(Power(t1,3) - 32*Power(t1,2)*t2 + 
               20*t1*Power(t2,2) - 8*Power(t2,3)) - 
            Power(s1,3)*(4*Power(s2,3) + s2*t1*(5*t1 - 8*t2) + 
               2*Power(t1,2)*t2 + Power(s2,2)*(-9*t1 + 3*t2)) + 
            Power(s1,2)*(-6*Power(s2,4) + 19*Power(s2,3)*t1 - 
               19*Power(s2,2)*t1*(t1 - t2) + 
               Power(t1,2)*(9*t1 - 4*t2)*t2 + 
               3*s2*t1*(2*Power(t1,2) - 8*t1*t2 + Power(t2,2))) + 
            s1*(-3*Power(s2,5) + Power(s2,4)*(11*t1 - 3*t2) + 
               3*Power(t1,3)*t2*(-2*t1 + t2) + 
               Power(s2,3)*(-12*Power(t1,2) + 23*t1*t2 - 
                  6*Power(t2,2)) + 
               s2*t1*t2*(27*Power(t1,2) - 13*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,3) - 40*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) - 6*Power(t2,3)))) + 
         Power(s,2)*(-3*Power(s2,4)*(t1 - 2*t2) + 
            Power(s1,3)*s2*(t1 + t2) + 
            Power(s1,2)*(3*Power(s2,3) - 11*Power(s2,2)*t1 + 
               s2*t1*(9*t1 - 16*t2) + t1*(7*t1 - 4*t2)*t2) + 
            Power(t1,2)*t2*(4*Power(t1,2) - 5*t1*t2 + Power(t2,2)) - 
            3*s2*t1*t2*(8*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,3)*(5*Power(t1,2) - 27*t1*t2 + 12*Power(t2,2)) - 
            2*Power(s2,2)*(Power(t1,3) - 20*Power(t1,2)*t2 + 
               15*t1*Power(t2,2) - 6*Power(t2,3)) + 
            s1*(3*Power(s2,4) + Power(s2,3)*(-13*t1 + 3*t2) + 
               t1*t2*(-13*Power(t1,2) + 12*t1*t2 - 2*Power(t2,2)) + 
               2*Power(s2,2)*(7*Power(t1,2) - 17*t1*t2 + 3*Power(t2,2)) + 
               s2*(-4*Power(t1,3) + 37*Power(t1,2)*t2 - 
                  19*t1*Power(t2,2) + 6*Power(t2,3)))))*
       dilog(1 - s1/(-s + s1 - t2)))/
     (s1*s2*t1*Power(s - s2 + t1,3)*(-s + s1 - t2)*t2) - 
    (16*(Power(s,4)*(t1 - t2)*(s1 + t1 - t2)*t2 + 
         Power(s2,3)*(s2 - t1)*Power(t2,3) + 
         s1*Power(s2,2)*Power(t2,2)*
          (3*s2*t1 - 2*Power(t1,2) + 4*s2*t2 - 3*t1*t2) + 
         Power(s1,4)*(2*Power(s2,3) + t1*(t1 - t2)*t2 + 
            Power(s2,2)*(-5*t1 + 2*t2) + 
            s2*(3*Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
         Power(s1,2)*s2*t2*(Power(s2,3) - 4*Power(s2,2)*t1 - 
            t1*(Power(t1,2) + t1*t2 + Power(t2,2)) + 
            s2*(4*Power(t1,2) + 3*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,3)*(2*Power(s2,4) + Power(t1,2)*t2*(-2*t1 + t2) + 
            Power(s2,3)*(-6*t1 + 2*t2) + 
            Power(s2,2)*(6*Power(t1,2) - 9*t1*t2 + Power(t2,2)) + 
            s2*(-2*Power(t1,3) + 8*Power(t1,2)*t2 - t1*Power(t2,2) + 
               Power(t2,3))) - 
         Power(s,3)*(Power(s1,2)*(s2 + t1)*t2 + 
            (t1 - t2)*t2*(2*s2*(t1 - 2*t2) + t1*(-t1 + t2)) + 
            s1*(s2*(2*Power(t1,2) - 2*t1*t2 - 3*Power(t2,2)) + 
               t2*(3*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)))) - 
         Power(s,2)*(Power(s1,2)*
             (Power(s2,2)*(4*t1 - 3*t2) + s2*t1*(-7*t1 + 16*t2) + 
               t2*(-6*Power(t1,2) + 11*t1*t2 - 3*Power(t2,2))) + 
            s2*t2*(t1*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) - 
               s2*(Power(t1,2) - 6*t1*t2 + 6*Power(t2,2))) + 
            s1*(3*t1*Power(t1 - t2,2)*t2 + 
               Power(s2,2)*(-2*Power(t1,2) + 7*t1*t2 + 3*Power(t2,2)) + 
               s2*(Power(t1,3) - 5*Power(t1,2)*t2 + 11*t1*Power(t2,2) - 
                  12*Power(t2,3)))) + 
         s*(Power(s2,2)*Power(t2,2)*
             (2*s2*(t1 - 2*t2) + t1*(-2*t1 + 3*t2)) + 
            s1*s2*t2*(Power(s2,2)*(4*t1 + t2) + 2*t1*t2*(-2*t1 + 3*t2) + 
               s2*(-2*Power(t1,2) + t1*t2 - 12*Power(t2,2))) - 
            Power(s1,3)*(2*Power(s2,3) + Power(s2,2)*(-9*t1 + 2*t2) + 
               s2*(8*Power(t1,2) - 15*t1*t2 + Power(t2,2)) + 
               t2*(5*Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(-7*Power(s2,2)*t1*(t1 - 3*t2) + 
               Power(s2,3)*(4*t1 - 3*t2) + 
               t1*t2*(4*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               s2*(3*Power(t1,3) - 15*Power(t1,2)*t2 + 8*t1*Power(t2,2) - 
                  6*Power(t2,3)))))*
       dilog(1 - (-s + s2 - t1)/(-s + s1 - t2)))/
     (Power(s1,3)*s2*(-s + s2 - t1)*t1*t2*(s - s1 + t2)) + 
    (16*(Power(s1,4)*(s2 - t1)*t1*(s2 - t1 + t2) + 
         Power(s,4)*(Power(t1,3) + t1*Power(t2,2) + 2*Power(t2,3)) + 
         Power(s1,2)*t2*(Power(s2,4) - 3*Power(t1,2)*Power(t2,2) + 
            s2*Power(t1,2)*(-2*t1 + t2) + 3*Power(s2,2)*t1*(2*t1 + t2) - 
            Power(s2,3)*(5*t1 + 3*t2)) + 
         Power(s1,3)*(-(Power(t1,2)*(t1 - 3*t2)*t2) + 
            Power(s2,3)*(t1 + 2*t2) - Power(s2,2)*t1*(2*t1 + 5*t2) + 
            s2*t1*(Power(t1,2) + 4*t1*t2 - 2*Power(t2,2))) + 
         s1*Power(t2,2)*(-2*Power(s2,4) + Power(s2,2)*t1*(-7*t1 + t2) + 
            Power(t1,2)*t2*(-t1 + t2) + Power(s2,3)*(7*t1 + t2) + 
            2*s2*t1*(Power(t1,2) - t1*t2 + Power(t2,2))) + 
         s2*Power(t2,3)*(Power(s2,3) - 3*Power(s2,2)*t1 + 
            s2*t1*(4*t1 - t2) - t1*(2*Power(t1,2) - t1*t2 + Power(t2,2))) \
+ Power(s,3)*(t1*t2*(Power(t1,2) + 7*Power(t2,2)) - 
            s2*(Power(t1,3) + Power(t1,2)*t2 + 6*Power(t2,3)) + 
            s1*(-4*Power(t1,3) + Power(t1,2)*t2 - 3*t1*Power(t2,2) - 
               2*Power(t2,3) + 
               2*s2*(Power(t1,2) + 2*t1*t2 + 2*Power(t2,2)))) - 
         s*(Power(s1,3)*(2*Power(s2,2)*(t1 + t2) - 
               2*s2*t1*(3*t1 + t2) + 
               t1*(4*Power(t1,2) - 3*t1*t2 + Power(t2,2))) + 
            Power(t2,2)*(Power(s2,3)*(t1 + 4*t2) - 
               Power(s2,2)*t1*(3*t1 + 7*t2) - 
               t1*t2*(Power(t1,2) + Power(t2,2)) + 
               s2*t1*(2*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(-3*t1*Power(t1 - t2,2)*t2 + 
               Power(s2,3)*(t1 + 3*t2) + 
               s2*t1*(3*Power(t1,2) + 9*t1*t2 + 4*Power(t2,2)) - 
               Power(s2,2)*(4*Power(t1,2) + 13*t1*t2 + 7*Power(t2,2))) + 
            s1*t2*(3*t1*Power(t2,3) - Power(s2,3)*(4*t1 + 7*t2) + 
               Power(s2,2)*(7*Power(t1,2) + 10*t1*t2 + 4*Power(t2,2)) - 
               s2*(4*Power(t1,3) + 7*t1*Power(t2,2)))) + 
         Power(s,2)*(Power(s1,2)*
             (Power(s2,2)*(t1 + 2*t2) + 
               3*t1*(2*Power(t1,2) - t1*t2 + Power(t2,2)) - 
               s2*(6*Power(t1,2) + 7*t1*t2 + 4*Power(t2,2))) + 
            t2*(3*t1*Power(t2,2)*(t1 + t2) + 
               Power(s2,2)*(Power(t1,2) + 7*Power(t2,2)) - 
               s2*t1*(2*Power(t1,2) + t1*t2 + 11*Power(t2,2))) + 
            s1*(t1*t2*(-3*Power(t1,2) + 3*t1*t2 - 10*Power(t2,2)) - 
               Power(s2,2)*(2*Power(t1,2) + 8*t1*t2 + 9*Power(t2,2)) + 
               s2*(3*Power(t1,3) + 6*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
                  5*Power(t2,3)))))*dilog(1 - (-s + s1 - t2)/s1))/
     (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*Power(t2,3)) - 
    (16*(Power(s1,4)*(2*Power(s2,3) + Power(s2,2)*t1 + Power(t1,3)) - 
         Power(s,4)*t1*(t1 - t2)*(s2 - t1 + t2) + 
         Power(s2,3)*t1*t2*((t1 - 2*t2)*t2 + s2*(-t1 + t2)) + 
         Power(s1,3)*(2*Power(s2,4) + 2*Power(s2,3)*(t1 - 3*t2) - 
            4*Power(s2,2)*t1*t2 - Power(t1,3)*t2 + 
            s2*Power(t1,2)*(4*t1 + 3*t2)) + 
         Power(s1,2)*s2*(Power(s2,3)*(2*t1 - 5*t2) - 
            Power(t1,2)*t2*(3*t1 + 2*t2) + 
            s2*t1*(3*Power(t1,2) + 3*t1*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*(Power(t1,2) - 9*t1*t2 + 6*Power(t2,2))) + 
         s1*Power(s2,2)*(-(t1*t2*(Power(t1,2) + t1*t2 + Power(t2,2))) + 
            Power(s2,2)*(Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) + 
            s2*(Power(t1,3) - Power(t1,2)*t2 + 8*t1*Power(t2,2) - 
               2*Power(t2,3))) - 
         Power(s,3)*(s1*(Power(s2,2)*t1 + 
               2*t1*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               s2*(-3*Power(t1,2) - 2*t1*t2 + 2*Power(t2,2))) + 
            t1*(Power(s2,2)*t2 - Power(t1 - t2,2)*t2 + 
               s2*(4*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2)))) + 
         s*(Power(s1,3)*(-2*Power(s2,3) + 2*Power(t1,2)*(-2*t1 + t2) + 
               Power(s2,2)*(-3*t1 + 4*t2) + s2*t1*(t1 + 4*t2)) + 
            Power(s1,2)*(Power(t1,2)*(3*t1 - 2*t2)*t2 + 
               7*Power(s2,2)*(3*t1 - t2)*t2 + 
               Power(s2,3)*(-2*t1 + 9*t2) + 
               s2*t1*(-12*Power(t1,2) + t1*t2 - 2*Power(t2,2))) + 
            Power(s2,2)*t1*(t2*
                (Power(t1,2) - 6*t1*t2 + 4*Power(t2,2)) - 
               s2*(Power(t1,2) - 5*t1*t2 + 5*Power(t2,2))) - 
            s1*s2*(2*Power(t1,2)*t2*(-3*t1 + 2*t2) + 
               Power(s2,2)*(Power(t1,2) - 15*t1*t2 + 8*Power(t2,2)) + 
               s2*(6*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - 3*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,2)*
             (Power(s2,2)*(3*t1 - 4*t2) + 
               t1*(6*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               s2*(-3*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2))) + 
            s2*t1*(-3*Power(t1 - t2,2)*t2 + 
               s2*(3*Power(t1,2) - 11*t1*t2 + 6*Power(t2,2))) - 
            s1*(Power(s2,2)*(16*t1 - 7*t2)*t2 + 
               t1*t2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*(-12*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) + Power(t2,3)))))*
       dilog(1 - (-s + s1 - t2)/(-s + s2 - t1)))/
     (s1*Power(s2,3)*t1*(s - s2 + t1)*(-s + s1 - t2)*t2) - 
    (16*(Power(s2,3)*Power(t2,3)*
          (Power(s2,4) + 5*Power(s2,3)*t2 + t1*(t1 - t2)*Power(t2,2) - 
            Power(s2,2)*(Power(t1,2) + 7*t1*t2 - 5*Power(t2,2)) + 
            s2*t2*(2*Power(t1,2) - 6*t1*t2 + Power(t2,2))) + 
         Power(s1,4)*s2*(Power(s2,5) + 2*Power(s2,2)*t1*(t1 - t2)*t2 - 
            s2*t1*Power(t2,3) + Power(t1,2)*Power(t2,3) + 
            Power(s2,4)*(-2*t1 + 3*t2) + 
            Power(s2,3)*(Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) + 
         Power(s,4)*t2*(Power(s2,3)*t1*(t1 - t2) + 
            Power(t1 - t2,2)*Power(t2,3) + 
            2*Power(s2,2)*Power(t2,2)*(-t1 + t2) + 
            s2*Power(t2,2)*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,3)*(Power(s2,7) - 2*Power(s2,6)*(t1 - 2*t2) + 
            Power(s2,2)*t1*(9*t1 - 4*t2)*Power(t2,3) - 
            2*s2*Power(t1,2)*(t1 - 2*t2)*Power(t2,3) - 
            Power(t1,3)*Power(t2,4) + 
            2*Power(s2,4)*t2*(3*Power(t1,2) - t1*t2 + 2*Power(t2,2)) + 
            Power(s2,5)*(Power(t1,2) - 9*t1*t2 + 3*Power(t2,2)) - 
            Power(s2,3)*t1*t2*(Power(t1,2) + 9*Power(t2,2))) - 
         Power(s1,2)*s2*t2*(-5*Power(s2,4)*t1*(t1 + t2) + 
            Power(s2,5)*(3*t1 + 2*t2) + 
            Power(t1,2)*Power(t2,3)*(3*t1 + 2*t2) + 
            s2*t1*Power(t2,2)*(4*Power(t1,2) - 6*t1*t2 - Power(t2,2)) + 
            Power(s2,2)*t2*(Power(t1,3) - 14*Power(t1,2)*t2 + 
               t1*Power(t2,2) - Power(t2,3)) + 
            Power(s2,3)*(2*Power(t1,3) + Power(t1,2)*t2 + 
               6*t1*Power(t2,2) + 5*Power(t2,3))) + 
         s1*Power(s2,2)*Power(t2,2)*
          (2*Power(s2,4)*(t1 + 2*t2) + 
            t1*Power(t2,2)*(-Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
            Power(s2,3)*(-3*Power(t1,2) - 7*t1*t2 + 9*Power(t2,2)) + 
            s2*t2*(-2*Power(t1,3) + 3*Power(t1,2)*t2 - 
               3*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s2,2)*(Power(t1,3) + 8*Power(t1,2)*t2 - 
               11*t1*Power(t2,2) + 10*Power(t2,3))) - 
         Power(s,3)*(s1*(Power(s2,4)*t1*(t1 - 4*t2) + 
               Power(s2,2)*Power(t2,3)*(-8*t1 + 3*t2) + 
               Power(t2,4)*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*Power(t2,3)*
                (7*Power(t1,2) - 14*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,3)*t2*(5*Power(t1,2) - 6*t1*t2 + 4*Power(t2,2))) \
+ t2*(Power(s2,4)*t1*(3*t1 - 4*t2) - t1*Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,2)*Power(t2,2)*
                (8*Power(t1,2) - 19*t1*t2 + 12*Power(t2,2)) - 
               Power(s2,3)*(Power(t1,3) - 2*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) - 8*Power(t2,3)) + 
               s2*Power(t2,2)*
                (-2*Power(t1,3) + 9*Power(t1,2)*t2 - 11*t1*Power(t2,2) + 
                  4*Power(t2,3)))) - 
         s*(Power(s1,3)*(Power(s2,6) - 4*Power(s2,5)*(t1 - t2) - 
               6*Power(s2,2)*t1*Power(t2,3) + 
               s2*t1*(5*t1 - 4*t2)*Power(t2,3) + 
               Power(t1,2)*Power(t2,4) + 
               Power(s2,4)*(3*Power(t1,2) - 14*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*t2*(7*Power(t1,2) - 8*t1*t2 + 4*Power(t2,2))) \
- s1*s2*t2*(2*Power(t1,2)*(3*t1 - 2*t2)*Power(t2,3) + 
               Power(s2,5)*(4*t1 + t2) + 
               Power(s2,4)*(-7*Power(t1,2) + 3*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*t2*
                (2*Power(t1,3) - 25*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) - 15*Power(t2,3)) + 
               Power(s2,3)*(4*Power(t1,3) - 2*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - 5*Power(t2,3)) + 
               s2*Power(t2,2)*
                (8*Power(t1,3) - 22*Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
                  2*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,6)*(-2*t1 + t2) + 
               Power(t1,2)*Power(t2,4)*(-3*t1 + 2*t2) + 
               s2*t1*Power(t2,3)*
                (-6*Power(t1,2) + 17*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,5)*(2*Power(t1,2) - 17*t1*t2 - Power(t2,2)) + 
               Power(s2,2)*Power(t2,3)*
                (26*Power(t1,2) - 22*t1*t2 + Power(t2,2)) + 
               Power(s2,4)*t2*
                (15*Power(t1,2) - 10*t1*t2 + 6*Power(t2,2)) - 
               Power(s2,3)*t2*
                (3*Power(t1,3) - 2*Power(t1,2)*t2 + 25*t1*Power(t2,2) + 
                  7*Power(t2,3))) + 
            Power(s2,2)*Power(t2,2)*
             (Power(s2,4)*(-3*t1 + 2*t2) - 
               t1*Power(t2,2)*(Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*(2*Power(t1,2) - 5*t1*t2 + 14*Power(t2,2)) + 
               s2*t2*(-2*Power(t1,3) + 12*Power(t1,2)*t2 - 
                  17*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(Power(t1,3) + 7*Power(t1,2)*t2 - 
                  22*t1*Power(t2,2) + 16*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,2)*
             (t1*(3*t1 - 2*t2)*Power(t2,4) + 
               Power(s2,2)*Power(t2,3)*(-11*t1 + t2) + 
               Power(s2,5)*(-2*t1 + t2) + 
               Power(s2,4)*(3*Power(t1,2) - 13*t1*t2 + Power(t2,2)) + 
               s2*Power(t2,3)*(9*Power(t1,2) - 13*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*t2*(9*Power(t1,2) - 11*t1*t2 + 8*Power(t2,2))) \
+ s1*(3*Power(s2,4)*t2*Power(-2*t1 + t2,2) + 
               Power(s2,5)*(Power(t1,2) - 8*t1*t2 - Power(t2,2)) - 
               t1*Power(t2,4)*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*Power(t2,3)*
                (25*Power(t1,2) - 37*t1*t2 + 9*Power(t2,2)) - 
               Power(s2,3)*t2*
                (3*Power(t1,3) - 4*Power(t1,2)*t2 + 23*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               s2*Power(t2,3)*
                (-6*Power(t1,3) + 22*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) + 2*Power(t2,3))) + 
            s2*t2*(-3*t1*Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,4)*(2*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               2*s2*Power(t2,2)*
                (-2*Power(t1,3) + 8*Power(t1,2)*t2 - 10*t1*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,3)*(-2*Power(t1,3) + 3*Power(t1,2)*t2 - 
                  10*t1*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-Power(t1,3) + 11*Power(t1,2)*t2 - 29*t1*Power(t2,2) + 
                  20*Power(t2,3)))))*dilog(1 - t1/(-s2 + t1 - t2)))/
     (s1*Power(s2,3)*t1*(s - s2 + t1)*(-s + s1 - t2)*Power(t2,3)*(s2 + t2)) \
- (16*(Power(s,7)*(t1 - t2)*(s1 + t1 - t2)*t2 - 
         Power(s2,3)*Power(s2 - t1,4)*Power(t2,3) - 
         s1*Power(s2,2)*Power(s2 - t1,3)*Power(t2,2)*
          (3*s2*t1 - 2*Power(t1,2) + 5*s2*t2 - 3*t1*t2) - 
         Power(s1,6)*(s2 - t1)*
          (4*Power(s2,3) + s2*t1*(3*t1 - 5*t2) + 2*Power(t1,2)*t2 + 
            Power(s2,2)*(-7*t1 + 2*t2)) + 
         Power(s1,2)*s2*Power(s2 - t1,3)*t2*
          (2*Power(s2,2)*(t1 + t2) - s2*t2*(7*t1 + 5*t2) + 
            t1*(-Power(t1,2) + 2*t1*t2 + Power(t2,2))) + 
         Power(s1,5)*(-5*Power(s2,5) + 14*Power(s2,4)*t1 + 
            3*s2*Power(t1,2)*t2*(3*t1 + t2) - 
            Power(t1,3)*t2*(3*t1 + t2) + 
            Power(s2,2)*t1*(4*Power(t1,2) - 9*t1*t2 - 4*Power(t2,2)) + 
            Power(s2,3)*(-13*Power(t1,2) + 3*t1*t2 + Power(t2,2))) + 
         Power(s1,4)*(-4*Power(s2,6) + Power(s2,5)*(19*t1 - 5*t2) + 
            2*Power(t1,4)*(2*t1 - t2)*t2 + 
            Power(s2,4)*(-38*Power(t1,2) + 27*t1*t2 - 4*Power(t2,2)) - 
            2*Power(s2,2)*Power(t1,2)*
             (11*Power(t1,2) - 31*t1*t2 + 7*Power(t2,2)) + 
            Power(s2,3)*(40*Power(t1,3) - 60*Power(t1,2)*t2 + 
               9*t1*Power(t2,2) - Power(t2,3)) + 
            s2*Power(t1,2)*(5*Power(t1,3) - 28*Power(t1,2)*t2 + 
               10*t1*Power(t2,2) + Power(t2,3))) - 
         Power(s1,3)*(s2 - t1)*
          (Power(s2,6) + Power(t1,4)*t2*(-t1 + t2) + 
            Power(s2,5)*(-7*t1 + 5*t2) + 
            Power(s2,4)*(17*Power(t1,2) - 23*t1*t2 + 5*Power(t2,2)) + 
            Power(s2,2)*t1*(10*Power(t1,3) - 36*Power(t1,2)*t2 + 
               8*t1*Power(t2,2) - 5*Power(t2,3)) - 
            2*s2*Power(t1,2)*
             (Power(t1,3) - 6*Power(t1,2)*t2 + 2*t1*Power(t2,2) - 
               Power(t2,3)) + 
            Power(s2,3)*(-19*Power(t1,3) + 43*Power(t1,2)*t2 - 
               8*t1*Power(t2,2) + 4*Power(t2,3))) - 
         Power(s,6)*(Power(s1,2)*(s2 + 2*t1 - t2)*t2 + 
            (t1 - t2)*t2*(s2*(5*t1 - 7*t2) + 4*t1*(-t1 + t2)) + 
            s1*(s2*(2*Power(t1,2) + t1*t2 - 6*Power(t2,2)) + 
               t2*(Power(t1,2) - 6*t1*t2 + 5*Power(t2,2)))) + 
         s*(Power(s1,7)*s2*(-s2 + t1) - 
            Power(s2,2)*Power(s2 - t1,3)*Power(t2,2)*
             (2*s2*t1 - 2*Power(t1,2) - 7*s2*t2 + 3*t1*t2) + 
            Power(s1,6)*(3*Power(s2,3) + 4*s2*t1*(t1 - 2*t2) + 
               2*Power(t1,2)*t2 + Power(s2,2)*(-7*t1 + 3*t2)) + 
            Power(s1,5)*(10*Power(s2,4) + Power(s2,3)*(-29*t1 + t2) + 
               Power(t1,2)*t2*(-11*t1 + 2*t2) + 
               Power(s2,2)*(27*Power(t1,2) - 23*t1*t2 - 
                  2*Power(t2,2)) + 
               s2*t1*(-8*Power(t1,2) + 29*t1*t2 + Power(t2,2))) - 
            s1*s2*Power(s2 - t1,2)*t2*
             (2*Power(t1,2)*(2*t1 - 3*t2)*t2 + 
               Power(s2,3)*(4*t1 + t2) + 
               s2*t1*(2*Power(t1,2) - t1*t2 + 30*Power(t2,2)) - 
               Power(s2,2)*(6*Power(t1,2) + 7*t1*t2 + 30*Power(t2,2))) + 
            Power(s1,4)*(12*Power(s2,5) + 
               Power(s2,4)*(-54*t1 + 15*t2) + 
               Power(t1,2)*t2*
                (17*Power(t1,2) - 9*t1*t2 - Power(t2,2)) + 
               Power(s2,3)*(86*Power(t1,2) - 85*t1*t2 + 
                  12*Power(t2,2)) + 
               s2*Power(t1,2)*
                (15*Power(t1,2) - 85*t1*t2 + 32*Power(t2,2)) + 
               Power(s2,2)*(-59*Power(t1,3) + 137*Power(t1,2)*t2 - 
                  29*t1*Power(t2,2) + 3*Power(t2,3))) - 
            Power(s1,2)*Power(s2 - t1,2)*
             (Power(s2,4)*(3*t1 - t2) - 
               Power(t1,2)*t2*(3*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(-10*Power(t1,2) + 25*t1*t2 + 
                  11*Power(t2,2)) + 
               Power(s2,2)*(10*Power(t1,3) - 27*Power(t1,2)*t2 - 
                  19*t1*Power(t2,2) - 25*Power(t2,3)) + 
               s2*t1*(-3*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + 14*Power(t2,3))) + 
            Power(s1,3)*(4*Power(s2,6) - 7*Power(s2,5)*(5*t1 - 3*t2) + 
               Power(t1,3)*t2*
                (-11*Power(t1,2) + 14*t1*t2 - 2*Power(t2,2)) + 
               2*Power(s2,4)*
                (48*Power(t1,2) - 59*t1*t2 + 10*Power(t2,2)) + 
               Power(s2,2)*t1*
                (72*Power(t1,3) - 217*Power(t1,2)*t2 + 
                  74*t1*Power(t2,2) - 27*Power(t2,3)) + 
               s2*Power(t1,2)*
                (-17*Power(t1,3) + 87*Power(t1,2)*t2 - 
                  51*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,3)*(-120*Power(t1,3) + 238*Power(t1,2)*t2 - 
                  55*t1*Power(t2,2) + 16*Power(t2,3)))) + 
         Power(s,5)*(Power(s1,3)*(s2 + t1)*t2 + 
            Power(s1,2)*(Power(s2,2)*(-3*t1 + 5*t2) + 
               s2*(8*Power(t1,2) - 11*t1*t2 - 7*Power(t2,2)) + 
               t2*(Power(t1,2) - 11*t1*t2 + 5*Power(t2,2))) + 
            t2*(6*Power(t1,2)*Power(t1 - t2,2) - 
               8*s2*t1*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(10*Power(t1,2) - 30*t1*t2 + 21*Power(t2,2))) \
+ s1*(Power(s2,2)*(8*Power(t1,2) - 10*t1*t2 - 15*Power(t2,2)) - 
               6*t1*t2*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) + 
               s2*(-7*Power(t1,3) + 18*Power(t1,2)*t2 - 
                  27*t1*Power(t2,2) + 30*Power(t2,3)))) - 
         Power(s,4)*(Power(s1,3)*
             (Power(s2,3) + Power(s2,2)*(-11*t1 + 9*t2) + 
               4*t2*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*(13*Power(t1,2) - 31*t1*t2 + 5*Power(t2,2))) + 
            t2*(-4*Power(t1,3)*Power(t1 - t2,2) + 
               6*s2*Power(t1,2)*
                (3*Power(t1,2) - 8*t1*t2 + 5*Power(t2,2)) + 
               5*Power(s2,3)*
                (2*Power(t1,2) - 8*t1*t2 + 7*Power(t2,2)) - 
               4*Power(s2,2)*t1*
                (6*Power(t1,2) - 20*t1*t2 + 15*Power(t2,2))) - 
            Power(s1,2)*(2*Power(s2,3)*(6*t1 - 5*t2) + 
               2*t1*t2*(9*Power(t1,2) - 22*t1*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*(-40*Power(t1,2) + 66*t1*t2 + 
                  20*Power(t2,2)) + 
               s2*(27*Power(t1,3) - 63*Power(t1,2)*t2 + 
                  31*t1*Power(t2,2) - 25*Power(t2,3))) + 
            s1*(2*Power(s2,3)*
                (6*Power(t1,2) - 15*t1*t2 - 10*Power(t2,2)) + 
               4*Power(t1,2)*t2*
                (5*Power(t1,2) - 11*t1*t2 + 6*Power(t2,2)) + 
               s2*t1*(9*Power(t1,3) - 54*Power(t1,2)*t2 + 
                  109*t1*Power(t2,2) - 90*Power(t2,3)) + 
               Power(s2,2)*(-21*Power(t1,3) + 62*Power(t1,2)*t2 - 
                  45*t1*Power(t2,2) + 75*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,6)*s2*(s2 - 2*t1 - t2) + 
            Power(s1,5)*(-5*Power(s2,3) + 2*Power(s2,2)*(7*t1 - t2) + 
               t1*t2*(-8*t1 + 3*t2) + 
               s2*(-10*Power(t1,2) + 20*t1*t2 + Power(t2,2))) - 
            s2*Power(s2 - t1,2)*t2*
             (Power(t1,2)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) - 
               2*s2*t1*(Power(t1,2) - 8*t1*t2 + 9*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) - 12*t1*t2 + 21*Power(t2,2))) - 
            Power(s1,4)*(12*Power(s2,4) + 
               2*Power(t1,2)*t2*(-13*t1 + 9*t2) + 
               Power(s2,3)*(-51*t1 + 15*t2) + 
               Power(s2,2)*(61*Power(t1,2) - 89*t1*t2 + 
                  12*Power(t2,2)) + 
               s2*(-22*Power(t1,3) + 90*Power(t1,2)*t2 - 
                  31*t1*Power(t2,2) + 3*Power(t2,3))) - 
            Power(s1,3)*(6*Power(s2,5) + Power(s2,4)*(-57*t1 + 34*t2) + 
               Power(t1,2)*t2*
                (28*Power(t1,2) - 39*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,3)*(133*Power(t1,2) - 185*t1*t2 + 
                  30*Power(t2,2)) + 
               s2*t1*(39*Power(t1,3) - 165*Power(t1,2)*t2 + 
                  100*t1*Power(t2,2) - 27*Power(t2,3)) + 
               Power(s2,2)*(-121*Power(t1,3) + 286*Power(t1,2)*t2 - 
                  87*t1*Power(t2,2) + 24*Power(t2,3))) + 
            Power(s1,2)*(s2 - t1)*
             (Power(s2,4)*(12*t1 - 5*t2) + 
               Power(t1,2)*t2*
                (-16*Power(t1,2) + 29*t1*t2 - 8*Power(t2,2)) + 
               Power(s2,3)*(-44*Power(t1,2) + 81*t1*t2 + 
                  25*Power(t2,2)) + 
               Power(s2,2)*(49*Power(t1,3) - 118*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) - 50*Power(t2,3)) + 
               s2*t1*(-17*Power(t1,3) + 61*Power(t1,2)*t2 - 
                  54*t1*Power(t2,2) + 46*Power(t2,3))) - 
            s1*(s2 - t1)*(-3*Power(t1,3)*Power(t1 - t2,2)*t2 + 
               Power(s2,4)*(2*Power(t1,2) - 19*t1*t2 - 6*Power(t2,2)) + 
               Power(s2,2)*t1*
                (4*Power(t1,3) - 28*Power(t1,2)*t2 + 
                  48*t1*Power(t2,2) - 105*Power(t2,3)) - 
               s2*Power(t1,2)*
                (Power(t1,3) - 12*Power(t1,2)*t2 + 39*t1*Power(t2,2) - 
                  39*Power(t2,3)) + 
               Power(s2,3)*(-5*Power(t1,3) + 38*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + 75*Power(t2,3)))) + 
         Power(s,3)*(Power(s1,5)*s2*(t1 + t2) + 
            Power(s1,4)*(4*Power(s2,3) + Power(s2,2)*(-16*t1 + 5*t2) + 
               t2*(13*Power(t1,2) - 11*t1*t2 + Power(t2,2)) + 
               s2*(13*Power(t1,2) - 31*t1*t2 + 4*Power(t2,2))) + 
            (s2 - t1)*t2*(-(Power(t1,3)*Power(t1 - t2,2)) + 
               Power(s2,2)*t1*
                (-11*Power(t1,2) + 50*t1*t2 - 45*Power(t2,2)) + 
               5*Power(s2,3)*(Power(t1,2) - 6*t1*t2 + 7*Power(t2,2)) + 
               s2*Power(t1,2)*(7*Power(t1,2) - 22*t1*t2 + 15*Power(t2,2))\
) + Power(s1,3)*(4*Power(s2,4) + Power(s2,3)*(-41*t1 + 26*t2) - 
               3*t1*t2*(9*Power(t1,2) - 14*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(74*Power(t1,2) - 127*t1*t2 + 
                  20*Power(t2,2)) + 
               s2*(-37*Power(t1,3) + 122*Power(t1,2)*t2 - 
                  61*t1*Power(t2,2) + 16*Power(t2,3))) + 
            Power(s1,2)*(-2*Power(s2,4)*(9*t1 - 5*t2) + 
               6*Power(s2,3)*
                (12*Power(t1,2) - 19*t1*t2 - 5*Power(t2,2)) + 
               2*Power(t1,2)*t2*
                (14*Power(t1,2) - 28*t1*t2 + 9*Power(t2,2)) + 
               s2*t1*(33*Power(t1,3) - 114*Power(t1,2)*t2 + 
                  103*t1*Power(t2,2) - 64*Power(t2,3)) + 
               Power(s2,2)*(-87*Power(t1,3) + 189*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + 50*Power(t2,3))) + 
            s1*(Power(s2,4)*(8*Power(t1,2) - 35*t1*t2 - 15*Power(t2,2)) + 
               Power(t1,3)*t2*
                (-13*Power(t1,2) + 27*t1*t2 - 14*Power(t2,2)) + 
               2*Power(s2,2)*t1*
                (9*Power(t1,3) - 45*Power(t1,2)*t2 + 68*t1*Power(t2,2) - 
                  90*Power(t2,3)) + 
               s2*Power(t1,2)*
                (-5*Power(t1,3) + 50*Power(t1,2)*t2 - 
                  117*t1*Power(t2,2) + 96*Power(t2,3)) + 
               Power(s2,3)*(-21*Power(t1,3) + 88*Power(t1,2)*t2 - 
                  30*t1*Power(t2,2) + 100*Power(t2,3)))))*
       dilog(1 - (-s + s1 - t2)/(-s2 + t1 - t2)))/
     (Power(s1,3)*s2*(-s + s1 + s2 - t1)*t1*Power(s - s2 + t1,3)*t2*
       (s - s1 + t2)) + (16*(Power(s1,4)*
          (Power(s2,3) + s2*Power(t1,2) + 2*Power(t1,3)) + 
         Power(s,4)*t1*Power(t1 - t2,2) + 
         Power(s1,3)*(Power(s2,4) + Power(s2,3)*(3*t1 - 2*t2) + 
            2*Power(t1,3)*(t1 - t2) + s2*Power(t1,2)*(3*t1 - t2) - 
            Power(s2,2)*t1*(t1 + 3*t2)) + 
         Power(s1,2)*(Power(t1,4)*(t1 - 2*t2) + 
            2*Power(s2,4)*(t1 - t2) + s2*Power(t1,3)*(3*t1 - t2) + 
            Power(s2,3)*(2*Power(t1,2) - 7*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*t1*(-2*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2))) - 
         s2*t1*t2*(Power(s2,3)*(t1 - t2) + Power(t1,3)*(2*t1 - t2) + 
            Power(s2,2)*Power(t2,2) + 
            s2*(-2*Power(t1,3) + t1*Power(t2,2))) + 
         s1*(-(Power(t1,5)*t2) + 
            s2*Power(t1,3)*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
            Power(s2,4)*(Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
            Power(s2,3)*t1*(Power(t1,2) - t1*t2 + 5*Power(t2,2)) + 
            Power(s2,2)*t1*(-2*Power(t1,3) + 4*Power(t1,2)*t2 + 
               t1*Power(t2,2) - 2*Power(t2,3))) - 
         Power(s,3)*(t1*(t1 - t2)*(2*s2*(t1 - 2*t2) + t2*(-2*t1 + t2)) + 
            s1*(s2*(Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               t1*(4*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2)))) - 
         s*(Power(s1,3)*(Power(s2,3) + Power(s2,2)*(t1 - 2*t2) + 
               2*Power(t1,2)*(3*t1 - 2*t2) - 4*s2*t1*t2) + 
            Power(s1,2)*(Power(s2,3)*(3*t1 - 4*t2) + 
               Power(s2,2)*t2*(-15*t1 + 2*t2) + 
               Power(t1,2)*(4*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)) + 
               s2*t1*(3*Power(t1,2) - 8*t1*t2 + 7*Power(t2,2))) + 
            s1*(Power(s2,2)*t1*t2*(-9*t1 + 13*t2) + 
               Power(s2,3)*(2*Power(t1,2) - 10*t1*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(2*Power(t1,2) - 3*t1*t2 + 3*Power(t2,2)) + 
               s2*t1*(Power(t1,3) + Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
                  4*Power(t2,3))) + 
            t1*(Power(s2,3)*Power(t1 - 2*t2,2) + 
               Power(t1,3)*t2*(-t1 + t2) + 
               Power(s2,2)*(-2*Power(t1,3) + 3*t1*Power(t2,2) - 
                  3*Power(t2,3)) + 
               s2*t1*(2*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) - 
                  2*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,2)*
             (Power(s2,2)*(t1 - 2*t2) + s2*t2*(-8*t1 + t2) + 
               t1*(7*Power(t1,2) - 9*t1*t2 + 2*Power(t2,2))) + 
            s1*(Power(s2,2)*(2*Power(t1,2) - 11*t1*t2 + 3*Power(t2,2)) + 
               s2*t1*(2*Power(t1,2) - 15*t1*t2 + 11*Power(t2,2)) + 
               t1*(2*Power(t1,3) - 7*Power(t1,2)*t2 + 6*t1*Power(t2,2) - 
                  2*Power(t2,3))) + 
            t1*(Power(s2,2)*(2*Power(t1,2) - 7*t1*t2 + 6*Power(t2,2)) + 
               t1*(Power(t1,3) - Power(t1,2)*t2 + t1*Power(t2,2) - 
                  Power(t2,3)) - 
               s2*(2*Power(t1,3) + 2*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
                  3*Power(t2,3)))))*dilog(1 - s1/t2))/
     (s1*s2*(-s + s2 - t1)*Power(t1,3)*(-s + s1 - t2)*t2) - 
    (16*(Power(s1,6)*(s2 + t1) + 
         Power(s1,5)*(Power(s2,2) + 3*s2*(t1 - t2) + 2*t1*(t1 - t2)) + 
         Power(s,4)*(s1 + t1)*(t1 - t2)*(s1 + t1 - t2) - 
         Power(s2,3)*t1*Power(t2,3) + 
         s1*Power(s2,2)*Power(t2,2)*
          (Power(s2,2) + 4*s2*t1 - 2*Power(t1,2) - s2*t2 - 3*t1*t2) - 
         Power(s1,2)*s2*t2*(2*Power(s2,3) + Power(s2,2)*(4*t1 - 5*t2) + 
            t1*(Power(t1,2) + t1*t2 + Power(t2,2)) + 
            s2*(-3*Power(t1,2) - 8*t1*t2 + 2*Power(t2,2))) + 
         Power(s1,4)*(2*Power(s2,3) - 6*Power(s2,2)*t2 + 
            t1*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
            s2*(4*Power(t1,2) - 4*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,3)*(Power(s2,4) + Power(s2,3)*(t1 - 6*t2) + 
            Power(t1,2)*t2*(-2*t1 + t2) - 
            Power(s2,2)*(Power(t1,2) + 6*t1*t2 - 7*Power(t2,2)) + 
            s2*(3*Power(t1,3) + 2*t1*Power(t2,2) - Power(t2,3))) - 
         Power(s,3)*(Power(s1,3)*(s2 + 4*t1 - 3*t2) + 
            t1*(t1 - t2)*(s2*t1 - 3*s2*t2 - t1*t2 + Power(t2,2)) + 
            Power(s1,2)*(2*s2*t1 + 8*Power(t1,2) - 4*s2*t2 - 11*t1*t2 + 
               4*Power(t2,2)) + 
            s1*(3*s2*Power(t1,2) + 4*Power(t1,3) - 10*s2*t1*t2 - 
               10*Power(t1,2)*t2 + 4*s2*Power(t2,2) + 7*t1*Power(t2,2) - 
               Power(t2,3))) + 
         Power(s,2)*(Power(s1,4)*(s2 + 6*t1 - 3*t2) - 
            s2*t1*t2*(2*s2*t1 + Power(t1,2) - 3*s2*t2 - 4*t1*t2 + 
               3*Power(t2,2)) + 
            Power(s1,3)*(2*Power(s2,2) + 2*s2*t1 + 12*Power(t1,2) - 
               11*s2*t2 - 15*t1*t2 + 5*Power(t2,2)) + 
            Power(s1,2)*(7*Power(t1,3) + Power(s2,2)*(2*t1 - 7*t2) - 
               18*Power(t1,2)*t2 + 11*t1*Power(t2,2) - 2*Power(t2,3) + 
               s2*(5*Power(t1,2) - 27*t1*t2 + 13*Power(t2,2))) + 
            s1*(-3*t1*Power(t1 - t2,2)*t2 + 
               Power(s2,2)*(Power(t1,2) - 11*t1*t2 + 6*Power(t2,2)) + 
               s2*(2*Power(t1,3) - 12*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) - 3*Power(t2,3)))) + 
         s*(-(Power(s2,2)*t1*(s2 + 2*t1 - 3*t2)*Power(t2,2)) + 
            Power(s1,5)*(-s2 - 4*t1 + t2) - 
            Power(s1,4)*(2*Power(s2,2) + 8*Power(t1,2) + 
               3*s2*(t1 - 3*t2) - 9*t1*t2 + 2*Power(t2,2)) + 
            Power(s1,3)*(-2*Power(s2,3) - 6*Power(t1,3) + 
               14*Power(s2,2)*t2 + 13*Power(t1,2)*t2 - 
               6*t1*Power(t2,2) + Power(t2,3) + 
               s2*(-6*Power(t1,2) + 19*t1*t2 - 12*Power(t2,2))) + 
            s1*s2*t2*(4*Power(s2,2)*(t1 - t2) + 2*t1*t2*(-2*t1 + 3*t2) + 
               s2*(2*Power(t1,2) - 15*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,2)*(-(Power(s2,3)*(t1 - 6*t2)) + 
               Power(s2,2)*(Power(t1,2) + 20*t1*t2 - 14*Power(t2,2)) + 
               t1*t2*(4*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               s2*(-4*Power(t1,3) + 6*Power(t1,2)*t2 - 
                  19*t1*Power(t2,2) + 4*Power(t2,3)))))*dilog(1 - t1/t2))/
     (Power(s1,3)*s2*(-s + s2 - t1)*t1*t2*(s - s1 + t2)) - 
    (16*(Power(s1,4)*Power(t1,2)*(-s2 + t1) + Power(s,6)*(t1 + t2) + 
         Power(s,5)*(s1*(s2 - 4*t1 - 3*t2) + 6*t1*t2 - s2*(t1 + 4*t2)) + 
         Power(s1,3)*(-4*Power(t1,3)*t2 + Power(s2,3)*(t1 + t2) - 
            2*Power(s2,2)*t1*(t1 + 2*t2) + s2*Power(t1,2)*(t1 + 8*t2)) - 
         s2*Power(t2,3)*(Power(s2,2)*t1 - 
            s2*(2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
            t1*(2*Power(t1,2) - t1*t2 + Power(t2,2))) - 
         Power(s1,2)*(s2 - t1)*t2*
          (t1*(3*t1 - t2)*t2 + 2*Power(s2,2)*(2*t1 + t2) - 
            s2*(4*Power(t1,2) + 5*t1*t2 + Power(t2,2))) + 
         s1*Power(t2,2)*(Power(t1,2)*t2*(-t1 + t2) + 
            Power(s2,3)*(4*t1 + t2) + 
            s2*t1*(4*Power(t1,2) + t1*t2 + Power(t2,2)) - 
            Power(s2,2)*(7*Power(t1,2) + 2*t1*t2 + 2*Power(t2,2))) + 
         Power(s,4)*(-12*s2*t1*t2 + Power(t1 + t2,3) + 
            Power(s1,2)*(-3*s2 + 6*t1 + 3*t2) + 
            Power(s2,2)*(t1 + 6*t2) + 
            s1*(-3*Power(s2,2) - 18*t1*t2 + 2*s2*(t1 + 5*t2))) + 
         Power(s,3)*(Power(s1,3)*(3*s2 - 4*t1 - t2) - 
            Power(s2,3)*(t1 + 4*t2) + Power(s2,2)*t1*(t1 + 11*t2) + 
            Power(s1,2)*(7*Power(s2,2) - 8*s2*t2 + 18*t1*t2) - 
            s2*(Power(t1,3) + 6*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
               2*Power(t2,3)) + 
            2*(2*Power(t1,3)*t2 + t1*Power(t2,3) + Power(t2,4)) + 
            s1*(3*Power(s2,3) - Power(t1 + t2,2)*(4*t1 + t2) - 
               2*Power(s2,2)*(t1 + 6*t2) + 
               s2*(Power(t1,2) + 29*t1*t2 + 2*Power(t2,2)))) + 
         s*(Power(s1,4)*s2*(s2 + t1) + 
            Power(s1,3)*(2*Power(s2,3) - 2*Power(s2,2)*t2 - 
               Power(t1,2)*(4*t1 + 3*t2) + s2*t1*(3*t1 + 5*t2)) + 
            Power(s1,2)*(Power(s2,4) + 12*Power(t1,3)*t2 - 
               t1*Power(t2,3) - 3*Power(s2,3)*(t1 + t2) + 
               Power(s2,2)*(5*Power(t1,2) + 14*t1*t2 + 4*Power(t2,2)) - 
               s2*(3*Power(t1,3) + 22*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + Power(t2,3))) + 
            Power(t2,2)*(-2*Power(s2,3)*t1 + 
               t1*t2*(Power(t1,2) + Power(t2,2)) + 
               Power(s2,2)*(3*Power(t1,2) + 2*Power(t2,2)) - 
               s2*(4*Power(t1,3) + t1*Power(t2,2) + 2*Power(t2,3))) + 
            s1*t2*(-Power(s2,4) + Power(t1,2)*t2*(-6*t1 + t2) + 
               Power(s2,3)*(8*t1 + t2) - 
               Power(s2,2)*(13*Power(t1,2) + 10*t1*t2 + 5*Power(t2,2)) + 
               s2*(8*Power(t1,3) + 9*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
                  3*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,4)*(-s2 + t1) - 
            Power(s1,3)*(5*Power(s2,2) + 2*s2*(t1 - t2) + 6*t1*t2) + 
            Power(s1,2)*(-5*Power(s2,3) + Power(s2,2)*(t1 + 8*t2) + 
               3*t1*(2*Power(t1,2) + 3*t1*t2 + Power(t2,2)) - 
               s2*(3*Power(t1,2) + 22*t1*t2 + 2*Power(t2,2))) + 
            t2*(Power(s2,4) - 5*Power(s2,3)*t1 + 3*Power(t1,3)*t2 + 
               2*t1*Power(t2,3) + Power(t2,4) + 
               Power(s2,2)*(5*Power(t1,2) + 2*t1*t2 + Power(t2,2)) - 
               s2*(4*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  4*Power(t2,3))) - 
            s1*(Power(s2,4) + 12*Power(t1,3)*t2 + t1*Power(t2,3) + 
               Power(t2,4) - 3*Power(s2,3)*(t1 + 2*t2) + 
               Power(s2,2)*(4*Power(t1,2) + 21*t1*t2 + 3*Power(t2,2)) - 
               s2*(3*Power(t1,3) + 20*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 5*Power(t2,3)))))*dilog(1 - t2/s1))/
     (s1*s2*(-s + s2 - t1)*t1*t2*Power(s - s1 + t2,3)) - 
    (16*(Power(s1,4)*s2*Power(s2 - t1,2) - 
         Power(s,4)*(t1 - t2)*(s2 + t2)*(s2 - t1 + t2) + 
         Power(s1,3)*(s2 - t1)*
          (2*Power(s2,3) + s2*t1*(t1 - 3*t2) + Power(t1,2)*t2 + 
            Power(s2,2)*(-4*t1 + t2)) + 
         Power(s2,3)*t2*(Power(s2,3) - 2*Power(s2,2)*(t1 - t2) + 
            t1*(t1 - 2*t2)*t2 + 
            s2*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2))) + 
         Power(s1,2)*s2*(Power(s2,4) - 6*Power(s2,3)*t1 - 
            Power(t1,2)*t2*(3*t1 + 2*t2) + 
            Power(s2,2)*(7*Power(t1,2) - 6*t1*t2 - Power(t2,2)) + 
            s2*t1*(-2*Power(t1,2) + 8*t1*t2 + 3*Power(t2,2))) + 
         s1*Power(s2,2)*(Power(s2,4) - 3*Power(s2,3)*(t1 - t2) - 
            t1*t2*(Power(t1,2) + t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(3*Power(t1,2) - 4*t1*t2 + 4*Power(t2,2)) + 
            s2*(-Power(t1,3) + 2*Power(t1,2)*t2 + 3*Power(t2,3))) - 
         Power(s,3)*(-(t1*Power(t1 - t2,2)*t2) + 
            Power(s2,3)*(-3*t1 + 4*t2) + 
            Power(s2,2)*(4*Power(t1,2) - 11*t1*t2 + 8*Power(t2,2)) - 
            s2*(Power(t1,3) - 7*Power(t1,2)*t2 + 10*t1*Power(t2,2) - 
               4*Power(t2,3)) + 
            s1*(Power(s2,3) + Power(s2,2)*(-4*t1 + 2*t2) + 
               t2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*(4*Power(t1,2) - 10*t1*t2 + 3*Power(t2,2)))) + 
         s*(-(Power(s1,3)*(2*Power(s2,3) + 4*s2*t1*(t1 - t2) + 
                 Power(t1,2)*t2 + Power(s2,2)*(-6*t1 + t2))) + 
            Power(s1,2)*(-2*Power(s2,4) + 14*Power(s2,3)*t1 + 
               Power(t1,2)*(3*t1 - 2*t2)*t2 + 
               Power(s2,2)*(-14*Power(t1,2) + 20*t1*t2 + Power(t2,2)) + 
               s2*t1*(3*Power(t1,2) - 15*t1*t2 + 2*Power(t2,2))) + 
            Power(s2,2)*(Power(s2,3)*(t1 - 4*t2) + 
               Power(s2,2)*(-2*Power(t1,2) + 9*t1*t2 - 8*Power(t2,2)) + 
               t1*t2*(Power(t1,2) - 6*t1*t2 + 4*Power(t2,2)) + 
               s2*(Power(t1,3) - 6*Power(t1,2)*t2 + 13*t1*Power(t2,2) - 
                  6*Power(t2,3))) - 
            s1*s2*(Power(s2,4) + 2*Power(t1,2)*t2*(-3*t1 + 2*t2) + 
               Power(s2,3)*(-9*t1 + 3*t2) + 
               Power(s2,2)*(12*Power(t1,2) - 19*t1*t2 + 6*Power(t2,2)) + 
               s2*(-4*Power(t1,3) + 19*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + 4*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,2)*
             (2*Power(s2,3) + t1*(3*t1 - 2*t2)*t2 + 
               Power(s2,2)*(-7*t1 + 2*t2) + 
               s2*(6*Power(t1,2) - 11*t1*t2 + Power(t2,2))) + 
            s1*(Power(s2,4) + Power(s2,3)*(-11*t1 + 2*t2) - 
               t1*t2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(13*Power(t1,2) - 27*t1*t2 + 5*Power(t2,2)) + 
               s2*(-3*Power(t1,3) + 18*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + 2*Power(t2,3))) + 
            s2*(-3*Power(s2,3)*(t1 - 2*t2) - 3*t1*Power(t1 - t2,2)*t2 + 
               Power(s2,2)*(5*Power(t1,2) - 15*t1*t2 + 12*Power(t2,2)) + 
               s2*(-2*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  18*t1*Power(t2,2) + 7*Power(t2,3)))))*dilog(1 - t2/t1))/
     (s1*Power(s2,3)*t1*(s - s2 + t1)*(-s + s1 - t2)*t2) + 
    (16*(Power(s1,7)*(Power(s2,3) + Power(t1,3)) + 
         Power(s,7)*t1*(t1 - t2)*(s2 - t1 + t2) + 
         Power(s2,3)*t1*Power(t2,3)*
          (-2*Power(s2,3) + 2*s2*(t1 - 2*t2)*t2 + 
            Power(t2,2)*(-t1 + t2) + Power(s2,2)*(t1 + 3*t2)) + 
         Power(s1,6)*(4*Power(s2,4) + Power(s2,3)*(5*t1 - 8*t2) - 
            4*Power(t1,3)*t2 - 2*Power(s2,2)*t1*(t1 + t2) + 
            s2*Power(t1,2)*(5*t1 + 3*t2)) + 
         Power(s1,5)*(5*Power(s2,5) + Power(s2,4)*(5*t1 - 19*t2) + 
            6*Power(t1,3)*Power(t2,2) - 
            s2*Power(t1,2)*t2*(18*t1 + 11*t2) + 
            Power(s2,2)*t1*(5*Power(t1,2) + 13*t1*t2 + 6*Power(t2,2)) + 
            Power(s2,3)*(5*Power(t1,2) - 28*t1*t2 + 24*Power(t2,2))) + 
         Power(s1,4)*(4*Power(s2,6) - 14*Power(s2,5)*t2 - 
            4*Power(t1,3)*Power(t2,3) + 
            3*s2*Power(t1,2)*Power(t2,2)*(8*t1 + 5*t2) - 
            Power(s2,2)*t1*t2*
             (16*Power(t1,2) + 29*t1*t2 + 5*Power(t2,2)) + 
            Power(s2,4)*(4*Power(t1,2) - 27*t1*t2 + 38*Power(t2,2)) + 
            Power(s2,3)*(4*Power(t1,3) - 13*Power(t1,2)*t2 + 
               66*t1*Power(t2,2) - 36*Power(t2,3))) + 
         Power(s1,2)*s2*t2*(Power(t1,2)*Power(t2,3)*(3*t1 + 2*t2) + 
            Power(s2,5)*(-7*t1 + 10*t2) + 
            Power(s2,4)*(4*Power(t1,2) + 9*t1*t2 - 4*Power(t2,2)) + 
            s2*t1*Power(t2,2)*
             (-8*Power(t1,2) - 13*t1*t2 + 3*Power(t2,2)) + 
            2*Power(s2,3)*t2*
             (7*Power(t1,2) - 31*t1*t2 + 11*Power(t2,2)) + 
            Power(s2,2)*t2*(7*Power(t1,3) - 12*Power(t1,2)*t2 + 
               48*t1*Power(t2,2) - 12*Power(t2,3))) - 
         s1*Power(s2,2)*Power(t2,2)*
          (Power(s2,4)*(-7*t1 + 3*t2) + 3*Power(s2,3)*t1*(t1 + 3*t2) + 
            t1*Power(t2,2)*(-Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
            s2*t2*(2*Power(t1,3) - 5*Power(t1,2)*t2 + 
               13*t1*Power(t2,2) - 2*Power(t2,3)) + 
            Power(s2,2)*(Power(t1,3) + 10*Power(t1,2)*t2 - 
               28*t1*Power(t2,2) + 5*Power(t2,3))) + 
         Power(s1,3)*(Power(s2,6)*(2*t1 - 11*t2) + 
            Power(t1,3)*Power(t2,4) - 
            s2*Power(t1,2)*Power(t2,3)*(14*t1 + 9*t2) - 
            Power(s2,5)*(Power(t1,2) + 3*t1*t2 - 13*Power(t2,2)) + 
            Power(s2,2)*t1*Power(t2,2)*
             (18*Power(t1,2) + 29*t1*t2 - Power(t2,2)) + 
            Power(s2,4)*(Power(t1,3) - 9*Power(t1,2)*t2 + 
               60*t1*Power(t2,2) - 40*Power(t2,3)) + 
            Power(s2,3)*t2*(-9*Power(t1,3) + 16*Power(t1,2)*t2 - 
               79*t1*Power(t2,2) + 29*Power(t2,3))) + 
         Power(s,6)*(t1*(-(Power(s2,2)*(t1 - 2*t2)) - 
               4*Power(t1 - t2,2)*t2 + 
               s2*(5*Power(t1,2) - 6*t1*t2 + Power(t2,2))) + 
            s1*(Power(s2,2)*t1 + 
               s2*(-6*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
               t1*(7*Power(t1,2) - 12*t1*t2 + 5*Power(t2,2)))) - 
         Power(s,5)*(t1*(Power(s2,3)*t2 + 
               6*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s2,2)*(5*Power(t1,2) - 11*t1*t2 + Power(t2,2)) - 
               6*s2*t2*(3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,2)*(5*t1 - 3*t2) + 
               s2*(-15*Power(t1,2) - 10*t1*t2 + 8*Power(t2,2)) + 
               t1*(21*Power(t1,2) - 30*t1*t2 + 10*Power(t2,2))) + 
            s1*(Power(s2,3)*t1 - 
               8*t1*t2*(3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-7*Power(t1,2) - 11*t1*t2 + 8*Power(t2,2)) + 
               s2*(30*Power(t1,3) - 27*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) - 7*Power(t2,3)))) + 
         Power(s,4)*(Power(s1,3)*
             (Power(s2,3) + 2*Power(s2,2)*(5*t1 - 6*t2) - 
               2*s2*(10*Power(t1,2) + 15*t1*t2 - 6*Power(t2,2)) + 
               5*t1*(7*Power(t1,2) - 8*t1*t2 + 2*Power(t2,2))) + 
            2*t1*(-2*Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,2)*t2*
                (-8*Power(t1,2) + 22*t1*t2 - 9*Power(t2,2)) + 
               2*Power(s2,3)*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) + 
               2*s2*Power(t2,2)*
                (6*Power(t1,2) - 11*t1*t2 + 5*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,3)*(9*t1 - 11*t2) - 
               4*t1*t2*(15*Power(t1,2) - 20*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(-20*Power(t1,2) - 66*t1*t2 + 
                  40*Power(t2,2)) + 
               s2*(75*Power(t1,3) - 45*Power(t1,2)*t2 + 
                  62*t1*Power(t2,2) - 21*Power(t2,3))) + 
            s1*(6*t1*Power(t2,2)*
                (5*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*(5*Power(t1,2) - 31*t1*t2 + 13*Power(t2,2)) + 
               Power(s2,2)*(25*Power(t1,3) - 31*Power(t1,2)*t2 + 
                  63*t1*Power(t2,2) - 27*Power(t2,3)) + 
               s2*t2*(-90*Power(t1,3) + 109*Power(t1,2)*t2 - 
                  54*t1*Power(t2,2) + 9*Power(t2,3)))) - 
         Power(s,3)*(Power(s1,4)*
             (4*Power(s2,3) + 2*Power(s2,2)*(5*t1 - 9*t2) + 
               5*t1*(7*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               s2*(-15*Power(t1,2) - 35*t1*t2 + 8*Power(t2,2))) + 
            t1*(Power(t1 - t2,2)*Power(t2,4) + 
               s2*Power(t2,3)*
                (-14*Power(t1,2) + 27*t1*t2 - 13*Power(t2,2)) - 
               3*Power(s2,3)*t2*
                (3*Power(t1,2) - 14*t1*t2 + 9*Power(t2,2)) + 
               Power(s2,4)*(Power(t1,2) - 11*t1*t2 + 13*Power(t2,2)) + 
               2*Power(s2,2)*Power(t2,2)*
                (9*Power(t1,2) - 28*t1*t2 + 14*Power(t2,2))) + 
            Power(s1,3)*(4*Power(s2,4) + Power(s2,3)*(26*t1 - 41*t2) - 
               6*Power(s2,2)*
                (5*Power(t1,2) + 19*t1*t2 - 12*Power(t2,2)) - 
               16*t1*t2*(5*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
               s2*(100*Power(t1,3) - 30*Power(t1,2)*t2 + 
                  88*t1*Power(t2,2) - 21*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,4)*(5*t1 - 16*t2) + 
               6*t1*Power(t2,2)*
                (10*Power(t1,2) - 12*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*(20*Power(t1,2) - 127*t1*t2 + 
                  74*Power(t2,2)) + 
               Power(s2,2)*(50*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  189*t1*Power(t2,2) - 87*Power(t2,3)) + 
               2*s2*t2*(-90*Power(t1,3) + 68*Power(t1,2)*t2 - 
                  45*t1*Power(t2,2) + 9*Power(t2,3))) + 
            s1*(Power(s2,5)*(t1 + t2) - 
               8*t1*Power(t2,3)*
                (2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               Power(s2,4)*(4*Power(t1,2) - 31*t1*t2 + 13*Power(t2,2)) + 
               Power(s2,3)*(16*Power(t1,3) - 61*Power(t1,2)*t2 + 
                  122*t1*Power(t2,2) - 37*Power(t2,3)) + 
               s2*Power(t2,2)*
                (96*Power(t1,3) - 117*Power(t1,2)*t2 + 
                  50*t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-64*Power(t1,3) + 103*Power(t1,2)*t2 - 
                  114*t1*Power(t2,2) + 33*Power(t2,3)))) - 
         s*(Power(s1,6)*(4*Power(s2,3) + Power(s2,2)*(t1 - 3*t2) + 
               Power(t1,2)*(7*t1 - 2*t2) - s2*t1*(t1 + 4*t2)) + 
            Power(s2,2)*t1*Power(t2,2)*
             (2*Power(s2,4) + Power(s2,3)*(2*t1 - 11*t2) - 
               Power(s2,2)*(Power(t1,2) + 9*t1*t2 - 17*Power(t2,2)) + 
               s2*t2*(-2*Power(t1,2) + 14*t1*t2 - 11*Power(t2,2)) + 
               Power(t2,2)*(Power(t1,2) - 5*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,5)*(12*Power(s2,4) + 7*Power(s2,3)*(3*t1 - 5*t2) + 
               8*Power(t1,2)*t2*(-3*t1 + t2) + 
               s2*t1*(30*Power(t1,2) + 9*t1*t2 + 14*Power(t2,2)) + 
               Power(s2,2)*(-11*Power(t1,2) - 27*t1*t2 + 16*Power(t2,2))\
) + Power(s1,4)*(10*Power(s2,5) + 3*Power(s2,4)*(5*t1 - 18*t2) + 
               6*Power(t1,2)*(5*t1 - 2*t2)*Power(t2,2) - 
               2*s2*t1*t2*(45*Power(t1,2) + 7*t1*t2 + 9*Power(t2,2)) + 
               2*Power(s2,3)*
                (10*Power(t1,2) - 59*t1*t2 + 48*Power(t2,2)) + 
               Power(s2,2)*(25*Power(t1,3) + 41*Power(t1,2)*t2 + 
                  78*t1*Power(t2,2) - 33*Power(t2,3))) + 
            s1*s2*t2*(Power(s2,6) + 
               2*Power(t1,2)*(3*t1 - 2*t2)*Power(t2,3) + 
               Power(s2,5)*(-8*t1 + 4*t2) + 
               Power(s2,4)*(Power(t1,2) + 29*t1*t2 - 8*Power(t2,2)) + 
               Power(s2,3)*t2*
                (32*Power(t1,2) - 85*t1*t2 + 15*Power(t2,2)) + 
               Power(s2,2)*t2*
                (14*Power(t1,3) - 51*Power(t1,2)*t2 + 
                  87*t1*Power(t2,2) - 17*Power(t2,3)) + 
               s2*Power(t2,2)*
                (-16*Power(t1,3) + 16*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s1,3)*(3*Power(s2,6) + Power(s2,5)*(t1 - 29*t2) + 
               8*Power(t1,2)*Power(t2,3)*(-2*t1 + t2) + 
               s2*t1*Power(t2,2)*
                (96*Power(t1,2) + t1*t2 + 10*Power(t2,2)) + 
               Power(s2,4)*(12*Power(t1,2) - 85*t1*t2 + 
                  86*Power(t2,2)) + 
               Power(s2,3)*(16*Power(t1,3) - 55*Power(t1,2)*t2 + 
                  238*t1*Power(t2,2) - 120*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-64*Power(t1,3) - 43*Power(t1,2)*t2 - 
                  88*t1*Power(t2,2) + 33*Power(t2,3))) - 
            Power(s1,2)*(Power(s2,7) + 
               Power(t1,2)*Power(t2,4)*(-3*t1 + 2*t2) + 
               Power(s2,6)*(-3*t1 + 7*t2) + 
               Power(s2,5)*(2*Power(t1,2) + 23*t1*t2 - 27*Power(t2,2)) + 
               s2*t1*Power(t2,3)*
                (42*Power(t1,2) - 9*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*t2*
                (27*Power(t1,3) - 74*Power(t1,2)*t2 + 
                  217*t1*Power(t2,2) - 72*Power(t2,3)) - 
               2*Power(s2,2)*Power(t2,2)*
                (27*Power(t1,3) + Power(t1,2)*t2 + 24*t1*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(s2,4)*(-3*Power(t1,3) + 29*Power(t1,2)*t2 - 
                  137*t1*Power(t2,2) + 59*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,5)*
             (6*Power(s2,3) + Power(s2,2)*(5*t1 - 12*t2) + 
               t1*(21*Power(t1,2) - 12*t1*t2 + Power(t2,2)) + 
               s2*(-6*Power(t1,2) - 19*t1*t2 + 2*Power(t2,2))) + 
            s2*t1*t2*(2*Power(s2,3)*(9*t1 - 13*t2)*t2 + 
               3*Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,4)*(-3*t1 + 8*t2) + 
               s2*Power(t2,2)*
                (-8*Power(t1,2) + 29*t1*t2 - 16*Power(t2,2)) + 
               Power(s2,2)*t2*(7*Power(t1,2) - 39*t1*t2 + 28*Power(t2,2))\
) + Power(s1,4)*(12*Power(s2,4) + Power(s2,3)*(34*t1 - 57*t2) - 
               4*t1*t2*(15*Power(t1,2) - 10*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(-25*Power(t1,2) - 86*t1*t2 + 
                  56*Power(t2,2)) + 
               s2*(75*Power(t1,3) + 57*t1*Power(t2,2) - 7*Power(t2,3))) + 
            Power(s1,3)*(5*Power(s2,5) + 3*Power(s2,4)*(5*t1 - 17*t2) + 
               6*t1*Power(t2,2)*
                (10*Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(30*Power(t1,2) - 185*t1*t2 + 
                  133*Power(t2,2)) + 
               Power(s2,2)*(50*Power(t1,3) + 34*Power(t1,2)*t2 + 
                  199*t1*Power(t2,2) - 93*Power(t2,3)) + 
               3*s2*t2*(-60*Power(t1,3) + 18*Power(t1,2)*t2 - 
                  22*t1*Power(t2,2) + 3*Power(t2,3))) + 
            s1*(Power(s2,6)*(t1 + 2*t2) - 
               Power(s2,5)*(Power(t1,2) + 20*t1*t2 - 10*Power(t2,2)) + 
               t1*Power(t2,4)*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,4)*(3*Power(t1,3) - 31*Power(t1,2)*t2 + 
                  90*t1*Power(t2,2) - 22*Power(t2,3)) + 
               Power(s2,2)*Power(t2,2)*
                (54*Power(t1,3) - 83*Power(t1,2)*t2 + 
                  77*t1*Power(t2,2) - 17*Power(t2,3)) + 
               s2*Power(t2,3)*
                (-42*Power(t1,3) + 45*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*t2*
                (-27*Power(t1,3) + 100*Power(t1,2)*t2 - 
                  165*t1*Power(t2,2) + 39*Power(t2,3))) - 
            Power(s1,2)*(Power(s2,6) - 2*Power(s2,5)*(t1 - 7*t2) + 
               Power(s2,4)*(-12*Power(t1,2) + 89*t1*t2 - 
                  61*Power(t2,2)) + 
               4*t1*Power(t2,3)*(6*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*t2*
                (96*Power(t1,3) - 45*Power(t1,2)*t2 + 
                  179*t1*Power(t2,2) - 66*Power(t2,3)) + 
               s2*Power(t2,2)*
                (-144*Power(t1,3) + 87*Power(t1,2)*t2 - 
                  40*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,3)*(-24*Power(t1,3) + 87*Power(t1,2)*t2 - 
                  286*t1*Power(t2,2) + 121*Power(t2,3)))))*
       dilog(1 - (-s + s2 - t1)/(-s1 - t1 + t2)))/
     (s1*Power(s2,3)*t1*(s - s2 + t1)*(-s + s1 + s2 - t2)*t2*
       Power(s - s1 + t2,3)) - 
    (16*(Power(s1,7)*(Power(s2,3) + Power(t1,3)) - 
         Power(s2,3)*Power(t1,4)*Power(t2,3) + 
         s1*Power(s2,2)*Power(t1,3)*Power(t2,2)*
          (Power(s2,2) + 4*s2*t1 - 2*Power(t1,2) - 2*s2*t2 - 3*t1*t2) + 
         Power(s1,6)*(Power(s2,4) + 5*Power(t1,4) + 
            Power(s2,3)*(4*t1 - 2*t2) + 2*s2*Power(t1,2)*(2*t1 + t2) - 
            Power(s2,2)*t1*(2*t1 + 3*t2)) + 
         Power(s1,2)*s2*Power(t1,3)*t2*
          (-Power(s2,3) + Power(s2,2)*(-4*t1 + 9*t2) + 
            s2*(Power(t1,2) + 6*t1*t2 - 4*Power(t2,2)) + 
            t1*(Power(t1,2) - 2*t1*t2 - Power(t2,2))) + 
         Power(s1,5)*(Power(s2,4)*(3*t1 - 2*t2) + 
            5*Power(s2,2)*t1*t2*(t1 + t2) + 
            s2*Power(t1,2)*(9*Power(t1,2) - 7*t1*t2 - 3*Power(t2,2)) + 
            Power(t1,3)*(5*Power(t1,2) - 7*t1*t2 - Power(t2,2)) + 
            Power(s2,3)*(3*Power(t1,2) - 9*t1*t2 + Power(t2,2))) + 
         Power(s,4)*t1*(2*Power(s1,2)*Power(t1,2)*(t1 - t2) + 
            Power(t1,3)*Power(t1 - t2,2) + Power(s1,3)*t2*(-t1 + t2) + 
            s1*Power(t1,2)*(3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) - 
         Power(s1,3)*t1*(2*Power(s2,4)*(t1 - t2)*t2 + 
            Power(t1,4)*(t1 - t2)*t2 - 
            s2*Power(t1,2)*(Power(t1,3) - 3*Power(t1,2)*t2 + 
               3*t1*Power(t2,2) - 2*Power(t2,3)) + 
            Power(s2,3)*(9*Power(t1,2)*t2 + Power(t2,3)) + 
            Power(s2,2)*t1*(-Power(t1,3) + Power(t1,2)*t2 - 
               14*t1*Power(t2,2) + Power(t2,3))) + 
         Power(s,3)*(Power(s1,4)*t2*
             (4*s2*t1 + 4*Power(t1,2) - s2*t2 - 3*t1*t2) + 
            Power(s1,2)*Power(t1,3)*
             (-3*s2*t1 - 12*Power(t1,2) + 8*s2*t2 + 19*t1*t2 - 
               8*Power(t2,2)) + 
            Power(t1,4)*(t1 - t2)*
             (-(s2*t1) + 3*s2*t2 + t1*t2 - Power(t2,2)) + 
            Power(s1,3)*t1*(-4*s2*Power(t1,2) - 8*Power(t1,3) + 
               6*s2*t1*t2 + 7*Power(t1,2)*t2 - 5*s2*Power(t2,2) - 
               2*t1*Power(t2,2) + Power(t2,3)) + 
            s1*Power(t1,3)*(-4*s2*Power(t1,2) - 4*Power(t1,3) + 
               14*s2*t1*t2 + 11*Power(t1,2)*t2 - 7*s2*Power(t2,2) - 
               9*t1*Power(t2,2) + 2*Power(t2,3))) + 
         Power(s1,4)*(Power(s2,4)*
             (2*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
            Power(t1,4)*(Power(t1,2) - 6*t1*t2 + 2*Power(t2,2)) + 
            2*Power(s2,3)*t1*(2*Power(t1,2) - t1*t2 + 3*Power(t2,2)) + 
            s2*Power(t1,2)*(10*Power(t1,3) - 11*Power(t1,2)*t2 + 
               8*t1*Power(t2,2) + Power(t2,3)) - 
            Power(s2,2)*t1*(5*Power(t1,3) + 6*Power(t1,2)*t2 + 
               t1*Power(t2,2) + 2*Power(t2,3))) + 
         Power(s,2)*(-(s2*Power(t1,4)*t2*
               (2*s2*t1 + Power(t1,2) - 3*s2*t2 - 4*t1*t2 + 
                 3*Power(t2,2))) + 
            Power(s1,5)*(Power(s2,2)*(t1 - 2*t2) + 
               s2*(-Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               t1*(Power(t1,2) - 6*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*Power(t1,3)*
             (6*Power(t1,3) + Power(s2,2)*(t1 - 11*t2) - 
               20*Power(t1,2)*t2 + 16*t1*Power(t2,2) - 4*Power(t2,3) + 
               s2*(9*Power(t1,2) - 37*t1*t2 + 25*Power(t2,2))) + 
            s1*Power(t1,3)*(-3*t1*Power(t1 - t2,2)*t2 + 
               Power(s2,2)*(Power(t1,2) - 13*t1*t2 + 9*Power(t2,2)) + 
               s2*(2*Power(t1,3) - 13*Power(t1,2)*t2 + 
                  22*t1*Power(t2,2) - 6*Power(t2,3))) + 
            Power(s1,4)*(3*s2*t1*Power(t1 - 2*t2,2) + 
               Power(s2,2)*(Power(t1,2) - 13*t1*t2 + 3*Power(t2,2)) + 
               t1*(15*Power(t1,3) - 10*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 2*Power(t2,3))) + 
            Power(s1,3)*t1*(Power(s2,2)*
                (8*Power(t1,2) - 11*t1*t2 + 9*Power(t2,2)) + 
               t1*(20*Power(t1,3) - 29*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) - Power(t2,3)) - 
               s2*(Power(t1,3) + 23*Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
                  3*Power(t2,3)))) - 
         s*(Power(s2,2)*Power(t1,4)*(s2 + 2*t1 - 3*t2)*Power(t2,2) + 
            Power(s1,6)*(Power(s2,3) + Power(t1,2)*(2*t1 - 3*t2) + 
               Power(s2,2)*(t1 - 2*t2) - s2*t1*(t1 + 4*t2)) + 
            s1*s2*Power(t1,3)*t2*
             (2*t1*(2*t1 - 3*t2)*t2 + Power(s2,2)*(-4*t1 + 5*t2) + 
               s2*(-2*Power(t1,2) + 17*t1*t2 - 6*Power(t2,2))) + 
            Power(s1,5)*(4*Power(s2,3)*(t1 - t2) - 
               Power(s2,2)*(Power(t1,2) + 17*t1*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(14*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) + 
               s2*t1*(3*Power(t1,2) - 3*t1*t2 + 7*Power(t2,2))) + 
            Power(s1,2)*Power(t1,3)*
             (-6*Power(s2,3)*t2 - 
               t1*t2*(3*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) - 22*t1*t2 + 26*Power(t2,2)) + 
               s2*(2*Power(t1,3) - 7*Power(t1,2)*t2 + 
                  22*t1*Power(t2,2) - 8*Power(t2,3))) + 
            Power(s1,4)*(Power(s2,3)*
                (3*Power(t1,2) - 14*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*t1*
                (6*Power(t1,2) - 10*t1*t2 + 15*Power(t2,2)) + 
               s2*t1*(5*Power(t1,3) - 15*Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(t1,2)*(16*Power(t1,3) - 22*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*t1*(Power(s2,3)*
                (4*Power(t1,2) - 8*t1*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(4*Power(t1,3) - 17*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) - 2*Power(t2,3)) + 
               s2*t1*(15*Power(t1,3) - 23*Power(t1,2)*t2 + 
                  25*t1*Power(t2,2) - 2*Power(t2,3)) - 
               Power(s2,2)*(7*Power(t1,3) + 25*Power(t1,2)*t2 - 
                  2*t1*Power(t2,2) + 3*Power(t2,3)))))*
       dilog(1 - t2/(-s1 - t1 + t2)))/
     (Power(s1,3)*s2*(-s + s2 - t1)*Power(t1,3)*(s1 + t1)*t2*(s - s1 + t2)) \
+ (8*((2*(Power(s,3)*(2*s1 + t1 - t2) + 
              s*s2*(3*s1*s2 - s1*t1 + s2*t1 - 3*s2*t2) + 
              Power(s2,2)*(s1*(-s2 + t1) + s2*t2) + 
              Power(s,2)*(-2*s2*t1 + s1*(-4*s2 + t1) + 3*s2*t2)))/
          ((s - s2)*s2*Power(s - s2 + t1,2)) - 
         (2*(Power(s,3)*(t1 - t2) + 
              s*s2*(2*s1*s2 - s1*t1 + s2*t1 - 3*s2*t2) + 
              Power(s2,2)*(s1*(-s2 + t1) + s2*t2) + 
              Power(s,2)*(-2*s2*t1 + s1*(-s2 + t1) + 3*s2*t2)))/
          ((s - s2)*s2*Power(t1,2)) + 
         (2*(Power(s,3)*(2*s2 - t1 + t2) + 
              Power(s1,2)*(s1*(-s2 + t1) + s2*t2) + 
              Power(s,2)*(s1*(-4*s2 + 3*t1 - 2*t2) + s2*t2) + 
              s*s1*(-(s2*t2) + s1*(3*s2 - 3*t1 + t2))))/
          ((s - s1)*s1*Power(s - s1 + t2,2)) + 
         (Power(s1,2)*(s1*(-s2 + t1) + s2*t2) + 
            s*(Power(s1,2)*(s2 - 2*t1 - t2) - 
               s1*(2*s2 + t1 - 2*t2)*t2 + s2*Power(t2,2)) + 
            Power(s,2)*((t1 - t2)*t2 + s1*(t1 + t2)))/
          ((s - s1)*s1*t2*(s - s1 + t2)) - 
         (2*(Power(s,3)*(-t1 + t2) + 
              Power(s1,2)*(s1*(-s2 + t1) + s2*t2) + 
              s*s1*(-(s2*t2) + s1*(2*s2 - 3*t1 + t2)) + 
              Power(s,2)*(s2*t2 - s1*(s2 - 3*t1 + 2*t2))))/
          ((s - s1)*s1*Power(t2,2)) + 
         (Power(s,3)*(t1 - t2) + Power(s1,2)*(s1*(s2 - t1) - s2*t2) + 
            s*(Power(s1,2)*(-4*s2 + 3*t1 - 3*t2) + s2*Power(t2,2) + 
               s1*t2*(s2 - t1 + 2*t2)) + 
            Power(s,2)*((s2 + t1 - t2)*t2 + s1*(3*s2 - 3*t1 + 4*t2)))/
          ((s - s1)*s1*t2*(s - s1 + t2)) + 
         (Power(s,3)*(-t1 + t2) + Power(s2,2)*(s1*(s2 - t1) - s2*t2) + 
            Power(s,2)*(4*s2*t1 - Power(t1,2) + s1*(3*s2 + t1) - 
               3*s2*t2 + t1*t2) + 
            s*(s1*(-4*Power(s2,2) + s2*t1 + Power(t1,2)) + 
               s2*(-3*s2*t1 + 2*Power(t1,2) + 3*s2*t2 - t1*t2)))/
          ((s - s2)*s2*t1*(s - s2 + t1)) - 
         (2*Power(s,6) + Power(s,5)*(-6*s1 - 2*s2 + t1 + 5*t2) + 
            Power(s,4)*(4*Power(s1,2) + s1*(5*s2 - 4*t1 - 9*t2) + 
               t2*(-5*s2 + 2*t1 + 4*t2)) + 
            Power(s,3)*(4*Power(s1,3) + Power(s1,2)*(s2 + 5*t1) + 
               Power(t2,2)*(-4*s2 + t1 + t2) + 
               s1*(2*Power(s2,2) + s2*t1 - Power(t1,2) + 6*s2*t2 - 
                  3*t1*t2 - 4*Power(t2,2))) - 
            Power(s,2)*(6*Power(s1,4) + 
               Power(s1,3)*(10*s2 + t1 - 7*t2) + s2*Power(t2,3) + 
               s1*t2*(-5*Power(s2,2) + s2*t1 - 3*s2*t2 + t1*t2) + 
               Power(s1,2)*(7*Power(s2,2) + 2*s2*t1 - 3*Power(t1,2) - 
                  6*s2*t2 + t1*t2 + Power(t2,2))) + 
            s*s1*(2*Power(s1,4) + Power(s1,3)*(7*s2 - 2*t1 - 3*t2) + 
               2*Power(s2,2)*Power(t2,2) + 
               s1*s2*t2*(-8*s2 + t1 + 2*t2) + 
               Power(s1,2)*(6*Power(s2,2) + s2*t1 - 3*Power(t1,2) - 
                  9*s2*t2 + 3*t1*t2 + Power(t2,2))) - 
            Power(s1,2)*(Power(s1,3)*(s2 - t1) + 
               Power(s2,2)*Power(t2,2) + s1*s2*t2*(-2*s2 + t2) + 
               Power(s1,2)*(Power(s2,2) - 2*s2*t2 + t1*(-t1 + t2))))/
          ((s - s1)*s1*(s - s2 + t1)*Power(s - s1 + t2,3)) + 
         (Power(s1,2)*(s1*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) - 
               Power(s1,2)*(s2 - t1)*(s2 - t1 + t2)) + 
            Power(s,3)*(s1*t1*(t1 - t2) + Power(t2,2)*(t1 + t2)) + 
            s*s1*(-2*Power(s2,2)*Power(t2,2) - 
               s1*s2*t2*(s2 + t1 + 2*t2) + 
               Power(s1,2)*(Power(s2,2) - 4*s2*t1 + 3*Power(t1,2) - 
                  3*t1*t2 - Power(t2,2))) + 
            Power(s,2)*(-(s2*Power(t2,3)) + 
               s1*t2*(-(t1*t2) + s2*(t1 + t2)) + 
               Power(s1,2)*(-3*Power(t1,2) + 3*t1*t2 + Power(t2,2) + 
                  s2*(2*t1 + t2))))/((s - s1)*s1*t1*Power(t2,3)) - 
         (Power(s,5)*(t1 + t2) + 
            Power(s1,2)*t1*(s1 + t2)*(s1*(-s2 + t1) + s2*t2) + 
            Power(s,4)*(-(s2*t1) + Power(t1,2) + s1*(s2 - 4*t1 - t2) + 
               3*Power(t2,2)) + 
            Power(s,2)*(Power(s1,2)*
                (Power(s2,2) - 8*s2*t1 + 6*t1*(t1 - t2)) - 
               s2*Power(t2,3) + Power(s1,3)*(-s2 - 4*t1 + t2) + 
               s1*t2*(-Power(s2,2) - s2*t1 + Power(t1,2) + 4*s2*t2 + 
                  t1*t2 + Power(t2,2))) + 
            s*s1*(Power(s1,3)*(s2 + t1) + 
               Power(t2,2)*(-2*Power(s2,2) + s2*(t1 - t2) + 2*t1*t2) + 
               Power(s1,2)*(5*s2*t1 - 4*Power(t1,2) - s2*t2 + 
                  3*t1*t2) + 
               s1*t2*(2*Power(s2,2) + s2*t2 - 2*t1*(t1 + t2))) + 
            Power(s,3)*(-(Power(s1,2)*(s2 - 6*t1 + t2)) + 
               t2*(-Power(s2,2) + s2*(t1 - 3*t2) + t2*(t1 + t2)) + 
               s1*(-Power(s2,2) - 4*Power(t1,2) + 3*t1*t2 - 
                  3*Power(t2,2) + s2*(5*t1 + t2))))/
          ((s - s1)*s1*t1*t2*Power(s - s1 + t2,2)) + 
         (Power(s2,2)*(s1*(-s2 + t1) + s2*t2) + 
            Power(s,2)*(t1*(-t1 + t2) + s2*(t1 + t2)) + 
            s*(s1*Power(s2 - t1,2) - 
               s2*(t1*(-2*t1 + t2) + s2*(t1 + 2*t2))))/
          ((s - s2)*s2*t1*(s - s2 + t1)) - 
         (Power(s1,2)*(s2 - t1)*(2*s1 - t2)*(s1*(s2 - t1) - s2*t2) + 
            Power(s,4)*(Power(t1,2) - 2*t1*t2 - Power(t2,2)) + 
            Power(s,3)*((s2 - t2)*t2*(t1 + t2) + 
               s1*(2*s2*t1 - 5*Power(t1,2) + 7*t1*t2 + 2*Power(t2,2))) \
+ Power(s,2)*(s2*Power(t2,3) + 
               Power(s1,2)*(Power(s2,2) + 9*Power(t1,2) - 8*t1*t2 - 
                  Power(t2,2) - s2*(8*t1 + t2)) - 
               s1*t2*(Power(s2,2) + Power(t1,2) - 5*t1*t2 - 
                  Power(t2,2) + s2*(t1 + 2*t2))) + 
            s*s1*(s1*t2*(4*Power(s2,2) + 2*t1*(t1 - 2*t2) + 
                  s2*(-4*t1 + t2)) - 
               Power(t2,2)*(Power(s2,2) - 2*t1*t2 + s2*(t1 + t2)) + 
               Power(s1,2)*(-3*Power(s2,2) + s2*(10*t1 + t2) + 
                  t1*(-7*t1 + 3*t2))))/
          ((s - s1)*s1*(s - s2 + t1)*Power(t2,2)*(s - s1 + t2)) - 
         (2*Power(s,6) + Power(s,5)*(-2*s1 - 6*s2 + 5*t1 + t2) - 
            Power(s2,2)*(Power(s1,2)*Power(s2 - t1,2) + 
               s1*s2*Power(s2 - t1,2) - Power(s2,2)*t2*(s2 - t1 + t2)) \
+ Power(s,4)*(4*Power(s2,2) + 5*s1*(s2 - t1) + 2*t1*(2*t1 + t2) - 
               s2*(9*t1 + 4*t2)) + 
            s*s2*(2*Power(s1,2)*
                (3*Power(s2,2) - 4*s2*t1 + Power(t1,2)) + 
               Power(s2,2)*(2*Power(s2,2) - 3*s2*t1 + Power(t1,2) - 
                  2*s2*t2 + 3*t1*t2 - 3*Power(t2,2)) + 
               s1*s2*(7*Power(s2,2) + s2*(-9*t1 + t2) + t1*(2*t1 + t2))\
) + Power(s,3)*(2*Power(s1,2)*s2 + 4*Power(s2,3) + 5*Power(s2,2)*t2 + 
               Power(t1,2)*(t1 + t2) - 
               s2*(4*Power(t1,2) + 3*t1*t2 + Power(t2,2)) + 
               s1*(Power(s2,2) - 4*Power(t1,2) + s2*(6*t1 + t2))) - 
            Power(s,2)*(Power(s1,2)*s2*(7*s2 - 5*t1) + 
               s1*(10*Power(s2,3) + Power(t1,3) + s2*t1*(-3*t1 + t2) + 
                  Power(s2,2)*(-6*t1 + 2*t2)) + 
               s2*(6*Power(s2,3) + Power(t1,2)*t2 + 
                  Power(s2,2)*(-7*t1 + t2) + 
                  s2*(Power(t1,2) + t1*t2 - 3*Power(t2,2)))))/
          ((s - s2)*s2*Power(s - s2 + t1,3)*(s - s1 + t2)) + 
         (Power(s,3)*(Power(t1,3) - s2*t1*t2 + Power(t1,2)*t2 + 
               s2*Power(t2,2)) + 
            Power(s2,2)*(Power(s1,2)*(-Power(s2,2) + Power(t1,2)) + 
               Power(s2,2)*(t1 - t2)*t2 + 
               s1*s2*(-(s2*t1) + Power(t1,2) + 2*s2*t2)) + 
            s*s2*(Power(s1,2)*(Power(s2,2) - s2*t1 - 2*Power(t1,2)) - 
               s1*s2*(2*Power(t1,2) + 4*s2*t2 + t1*t2) - 
               Power(s2,2)*(Power(t1,2) + 3*t1*t2 - 3*Power(t2,2))) + 
            Power(s,2)*(s1*(-Power(t1,3) + s2*t1*(t1 + t2) + 
                  Power(s2,2)*(t1 + 2*t2)) + 
               s2*(-(Power(t1,2)*t2) + 
                  s2*(Power(t1,2) + 3*t1*t2 - 3*Power(t2,2)))))/
          ((s - s2)*s2*Power(t1,3)*t2) + 
         (-(Power(s2,2)*(2*s2 - t1)*(s1 - t2)*(s1*(s2 - t1) - s2*t2)) + 
            Power(s,4)*(Power(t1,2) + 2*t1*t2 - Power(t2,2)) + 
            Power(s,3)*(Power(t1,2)*(t1 + t2) - 
               s1*(Power(t1,2) + 2*s2*t2 + t1*t2) + 
               s2*(-2*Power(t1,2) - 7*t1*t2 + 5*Power(t2,2))) + 
            s*s2*(Power(s1,2)*
                (3*Power(s2,2) - 4*s2*t1 + Power(t1,2)) + 
               t2*(-2*Power(t1,3) + 2*s2*t1*(2*t1 - t2) + 
                  Power(s2,2)*(-3*t1 + 7*t2)) + 
               s1*(-(s2*t1*(t1 - 4*t2)) + Power(t1,2)*(t1 + t2) - 
                  Power(s2,2)*(t1 + 10*t2))) + 
            Power(s,2)*(Power(s1,2)*s2*(-s2 + t1) + 
               s1*(-Power(t1,3) + s2*t1*(2*t1 + t2) + 
                  Power(s2,2)*(t1 + 8*t2)) + 
               s2*(s2*(Power(t1,2) + 8*t1*t2 - 9*Power(t2,2)) + 
                  t1*(-Power(t1,2) - 5*t1*t2 + Power(t2,2)))))/
          ((s - s2)*s2*Power(t1,2)*(s - s2 + t1)*(s - s1 + t2)) - 
         (Power(s,5)*(t1 + t2) + 
            Power(s2,2)*(s2 + t1)*t2*(s1*(-s2 + t1) + s2*t2) + 
            Power(s,4)*(3*Power(t1,2) + s1*(s2 - t2) + Power(t2,2) - 
               s2*(t1 + 4*t2)) + 
            s*s2*(2*Power(s1,2)*(s2 - t1)*t1 + 
               s1*(Power(s2,3) + s2*Power(t1,2) - 
                  Power(s2,2)*(t1 - 5*t2) + Power(t1,2)*(-t1 + t2)) + 
               t2*(Power(s2,3) + 2*Power(t1,3) + 
                  Power(s2,2)*(3*t1 - 4*t2) - 2*s2*t1*(t1 + t2))) + 
            Power(s,3)*(-(Power(s1,2)*(s2 + t1)) - 
               Power(s2,2)*(t1 - 6*t2) + Power(t1,2)*(t1 + t2) + 
               s2*(-3*Power(t1,2) + 3*t1*t2 - 4*Power(t2,2)) + 
               s1*(-Power(s2,2) + t1*(-3*t1 + t2) + s2*(t1 + 5*t2))) + 
            Power(s,2)*(Power(s1,2)*s2*(s2 - t1) - 
               s1*(Power(s2,3) + Power(t1,3) + 8*Power(s2,2)*t2 + 
                  s2*t1*(-4*t1 + t2)) + 
               s2*(Power(s2,2)*(t1 - 4*t2) + 6*s2*t2*(-t1 + t2) + 
                  t1*(Power(t1,2) + t1*t2 + Power(t2,2)))))/
          ((s - s2)*s2*t1*Power(s - s2 + t1,2)*t2) + 
         (2*Power(s,7)*(s2 - 2*t1 + 2*t2) - 
            Power(s,6)*(2*Power(s2,2) - 5*s2*t1 + 3*Power(t1,2) + 
               3*s2*t2 + 2*t1*t2 - 5*Power(t2,2) + 
               2*s1*(3*s2 - 10*t1 + 8*t2)) + 
            Power(s1,3)*t1*(Power(s1,3)*(-s2 + t1) + 
               s1*s2*(2*s2 - t2)*t2 - Power(s2,2)*Power(t2,2) + 
               Power(s1,2)*(-Power(s2,2) + Power(t1,2) + 2*s2*t2 - 
                  t1*t2)) + Power(s,5)*
             (-(Power(s2,2)*t1) + 2*s2*Power(t1,2) - Power(t1,3) - 
               4*s2*Power(t2,2) - t1*Power(t2,2) + 2*Power(t2,3) + 
               4*Power(s1,2)*(s2 - 10*t1 + 6*t2) + 
               s1*(Power(s2,2) + 14*Power(t1,2) + 9*t1*t2 - 
                  15*Power(t2,2) + 9*s2*(-2*t1 + t2))) + 
            s*Power(s1,2)*(Power(s1,3)*
                (Power(s2,2) + 6*s2*t1 + t1*(-2*t1 + t2)) + 
               s2*Power(t2,2)*
                (2*Power(s2,2) + 2*s2*t2 + t2*(-2*t1 + t2)) - 
               s1*t2*(4*Power(s2,3) + t1*Power(t2,2) + 
                  Power(s2,2)*(5*t1 + 3*t2) - 
                  s2*(Power(t1,2) + 3*t1*t2 - 2*Power(t2,2))) + 
               Power(s1,2)*(2*Power(s2,3) + 5*Power(s2,2)*t1 + 
                  Power(t1,2)*(-5*t1 + 4*t2) + 
                  s2*(2*Power(t1,2) - 7*t1*t2 + Power(t2,2)))) + 
            Power(s,3)*s1*(Power(s1,2)*
                (-10*Power(s2,2) - 4*s2*t1 + 20*Power(t1,2) + 
                  3*s2*t2 + 14*t1*t2 - 5*Power(t2,2)) + 
               Power(s1,3)*(-6*s2 + 4*(-5*t1 + t2)) + 
               s1*(-4*Power(s2,3) + Power(s2,2)*(4*t1 + 6*t2) + 
                  t1*(-10*Power(t1,2) + 4*t1*t2 - 3*Power(t2,2)) + 
                  s2*(12*Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
               t2*(4*Power(s2,3) + Power(t2,2)*(-t1 + t2) + 
                  Power(s2,2)*(-5*t1 + 3*t2) + 
                  s2*(3*Power(t1,2) - t1*t2 + Power(t2,2)))) + 
            Power(s,2)*s1*(2*Power(s1,4)*(s2 + 2*t1) + 
               s2*Power(t2,2)*(s2*t1 + (2*t1 - t2)*t2) + 
               Power(s1,3)*(2*Power(s2,2) - 9*s2*t1 - 
                  t1*(5*t1 + 6*t2)) + 
               Power(s1,2)*(-Power(s2,3) + 10*Power(t1,3) - 
                  6*Power(t1,2)*t2 + t1*Power(t2,2) + Power(t2,3) - 
                  Power(s2,2)*(8*t1 + 3*t2) + 
                  s2*(-8*Power(t1,2) + 9*t1*t2 - 4*Power(t2,2))) + 
               s1*t2*(Power(s2,3) + (2*t1 - t2)*Power(t2,2) + 
                  Power(s2,2)*(6*t1 + t2) + 
                  s2*(-3*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)))) + 
            Power(s,4)*(4*Power(s1,3)*(s2 + 10*t1 - 4*t2) + 
               Power(s1,2)*(8*Power(s2,2) + 21*s2*t1 - 
                  25*Power(t1,2) - 9*s2*t2 - 16*t1*t2 + 15*Power(t2,2)) \
- s2*t2*(Power(s2,2) + Power(t1,2) - t1*t2 + 2*Power(t2,2) + 
                  s2*(-2*t1 + t2)) + 
               s1*(3*Power(s2,3) + 5*Power(t1,3) + 
                  Power(s2,2)*(t1 - 3*t2) - Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 3*Power(t2,3) + 
                  s2*(-8*Power(t1,2) + t1*t2 + 6*Power(t2,2)))))/
          (Power(s - s1,2)*s1*s2*t1*Power(s - s1 + t2,3)) - 
         (Power(s,5)*(2*Power(t1,3) + Power(t1,2)*t2 + Power(t2,3)) + 
            Power(s2,3)*(s1 - t2)*
             (Power(s1,2)*(Power(s2,2) - Power(t1,2)) + 
               Power(s2,2)*t2*(-t1 + t2) + 
               s1*s2*(s2*t1 - Power(t1,2) - 2*s2*t2)) + 
            s*Power(s2,2)*(2*Power(s1,3)*
                (-Power(s2,2) + s2*t1 + Power(t1,2)) + 
               Power(s2,2)*t2*
                (Power(t1,2) - 4*t1*t2 + 5*Power(t2,2)) + 
               Power(s1,2)*s2*(t1*(3*t1 - t2) + s2*(t1 + 9*t2)) + 
               s1*s2*(t1*(t1 - t2)*t2 + 
                  s2*(Power(t1,2) + 3*t1*t2 - 12*Power(t2,2)))) + 
            Power(s,4)*(s2*(-4*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  t1*Power(t2,2) - 5*Power(t2,3)) + 
               s1*(t1*(-2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
                  s2*(4*Power(t1,2) + 3*t1*t2 + 3*Power(t2,2)))) + 
            Power(s,3)*s2*(2*s2*
                (Power(t1,3) + 3*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(s1,2)*(3*s2*(t1 + t2) - t1*(5*t1 + t2)) - 
               s1*(t1*t2*(-5*t1 + 3*t2) + 
                  s2*(7*Power(t1,2) + 7*t1*t2 + 12*Power(t2,2)))) + 
            Power(s,2)*s2*(Power(s1,3)*Power(s2 - t1,2) - 
               2*Power(s2,2)*t2*
                (2*Power(t1,2) - 3*t1*t2 + 5*Power(t2,2)) + 
               Power(s1,2)*(Power(t1,2)*(2*t1 - t2) + 
                  s2*t1*(3*t1 + 2*t2) - Power(s2,2)*(5*t1 + 9*t2)) + 
               s1*s2*(t1*(2*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) + 
                  s2*(2*Power(t1,2) + 3*t1*t2 + 18*Power(t2,2)))))/
          (s1*Power(s - s2,2)*s2*Power(t1,3)*t2) + 
         (4*Power(s,8) + Power(s,6)*
             (2*Power(s1,2) + 40*Power(s2,2) - 33*s2*t1 + 
               7*Power(t1,2) + s1*(18*s2 - 9*t1 - 5*t2) - 19*s2*t2 + 
               2*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,3)*t2*(Power(s1,2)*Power(s2 - t1,2) + 
               s1*s2*Power(s2 - t1,2) - Power(s2,2)*t2*(s2 - t1 + t2)) \
+ Power(s,7)*(-6*s1 + 4*(-5*s2 + 2*t1 + t2)) + 
            Power(s,5)*(-40*Power(s2,3) + 2*Power(t1,3) + 
               Power(t1,2)*t2 + Power(t2,3) + 
               Power(s1,2)*(3*s2 + t2) + Power(s2,2)*(52*t1 + 35*t2) - 
               2*s2*(11*Power(t1,2) + 4*t1*t2 + 7*Power(t2,2)) + 
               s1*(-13*Power(s2,2) + 18*s2*(t1 + t2) - 
                  2*(4*Power(t1,2) + Power(t2,2)))) - 
            s*Power(s2,2)*(2*Power(s1,3)*Power(s2 - t1,2) + 
               Power(s1,2)*s2*(s2 - t1)*(3*s2 - 3*t1 + 5*t2) + 
               Power(s2,2)*t2*
                (Power(s2,2) - Power(t1,2) - 2*s2*t2 + 4*t1*t2 - 
                  5*Power(t2,2)) + 
               s1*s2*(Power(s2,3) - 2*Power(s2,2)*(t1 - 3*t2) + 
                  t1*t2*(-t1 + t2) + 
                  s2*(Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)))) + 
            Power(s,2)*s2*(Power(s1,3)*s2*(s2 - t1) + 
               Power(s1,2)*(Power(t1,2)*(2*t1 - t2) - 
                  3*s2*t1*(t1 + 2*t2) + Power(s2,2)*(t1 + 8*t2)) + 
               Power(s2,2)*(Power(s2,2)*(-t1 + t2) - 
                  2*t2*(2*Power(t1,2) - 3*t1*t2 + 5*Power(t2,2)) + 
                  s2*(Power(t1,2) + 2*t1*t2 + 5*Power(t2,2))) + 
               s1*s2*(-2*Power(s2,3) + 3*Power(s2,2)*(t1 + 3*t2) + 
                  t1*(2*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) + 
                  s2*(-3*Power(t1,2) - 3*t1*t2 + 8*Power(t2,2)))) + 
            Power(s,3)*s2*(4*Power(s1,3)*(s2 - t1) + 
               Power(s1,2)*(16*Power(s2,2) + 5*t1*(t1 + t2) - 
                  4*s2*(5*t1 + t2)) + 
               s1*(12*Power(s2,3) + t1*(5*t1 - 3*t2)*t2 + 
                  4*Power(s2,2)*(-3*t1 + t2) + 
                  s2*(Power(t1,2) - t1*t2 - 12*Power(t2,2))) - 
               2*s2*(2*Power(s2,3) - Power(t1,3) - 3*Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) - 5*Power(t2,3) - 
                  Power(s2,2)*(6*t1 + 5*t2) + 
                  s2*(5*Power(t1,2) + 4*t1*t2 + 10*Power(t2,2)))) + 
            Power(s,4)*(Power(s1,3)*(-3*s2 + t1) + 
               Power(s1,2)*(-18*Power(s2,2) + 13*s2*t1 + Power(t1,2) - 
                  s2*t2 - 2*t1*t2) + 
               s1*(-8*Power(s2,3) - Power(s2,2)*(2*t1 + 21*t2) + 
                  t1*(-2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
                  s2*(11*Power(t1,2) + t1*t2 + 8*Power(t2,2))) + 
               s2*(20*Power(s2,3) - 4*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  t1*Power(t2,2) - 5*Power(t2,3) - 
                  2*Power(s2,2)*(19*t1 + 15*t2) + 
                  s2*(24*Power(t1,2) + 12*t1*t2 + 25*Power(t2,2)))))/
          (s1*Power(s - s2,2)*s2*Power(s - s2 + t1,3)*(s - s1 + t2)) - 
         (Power(s,5)*(2*Power(t1,3) - Power(t1,2)*t2 - Power(t2,3)) - 
            Power(s2,3)*(s1 - t2)*
             (Power(s1,2)*(Power(s2,2) - Power(t1,2)) + 
               s1*s2*(-Power(t1,2) + s2*(t1 - 2*t2)) + 
               Power(s2,2)*t2*(-t1 + t2)) - 
            Power(s,4)*(s2*(6*Power(t1,3) - 6*Power(t1,2)*t2 + 
                  t1*Power(t2,2) - 5*Power(t2,3)) + 
               s1*(t1*(2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
                  s2*(4*Power(t1,2) + 3*t1*t2 + 3*Power(t2,2)))) + 
            s*Power(s2,2)*(2*Power(s1,3)*
                (Power(s2,2) - s2*t1 - Power(t1,2)) - 
               Power(s1,2)*s2*(t1*(t1 - t2) + s2*(t1 + 9*t2)) + 
               s2*t2*(Power(t1,3) + 
                  s2*(-3*Power(t1,2) + 4*t1*t2 - 5*Power(t2,2))) + 
               s1*(-(Power(t1,3)*(t1 - 2*t2)) + 
                  s2*t1*(Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
                  Power(s2,2)*(Power(t1,2) - 3*t1*t2 + 12*Power(t2,2)))\
) + Power(s,3)*s2*(Power(t1,3)*(-t1 + t2) + 
               2*s2*(3*Power(t1,3) - 6*Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(s1,2)*(-3*s2*(t1 + t2) + t1*(5*t1 + t2)) + 
               s1*(t1*(2*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2)) + 
                  s2*(9*Power(t1,2) + 7*t1*t2 + 12*Power(t2,2)))) - 
            Power(s,2)*s2*(Power(s1,3)*Power(s2 - t1,2) - 
               Power(s1,2)*(Power(t1,2)*(2*t1 + t2) - 
                  s2*t1*(5*t1 + 2*t2) + Power(s2,2)*(5*t1 + 9*t2)) + 
               s1*(-(Power(t1,3)*(t1 - 2*t2)) + 
                  s2*t1*(Power(t1,2) - 10*t1*t2 + 3*Power(t2,2)) + 
                  3*Power(s2,2)*(2*Power(t1,2) + t1*t2 + 6*Power(t2,2))\
) + s2*(-(Power(t1,3)*(t1 - 2*t2)) + 
                  2*s2*(Power(t1,3) - 5*Power(t1,2)*t2 + 
                     3*t1*Power(t2,2) - 5*Power(t2,3)))))/
          (s1*Power(s - s2,2)*s2*Power(t1,3)*(s - s1 + t2)) + 
         (Power(s,5)*(Power(t1,3) + t1*Power(t2,2) - 2*Power(t2,3)) + 
            Power(s1,3)*(s2 - t1)*
             (-(s1*s2*Power(t2,2)) - Power(s2,2)*Power(t2,2) + 
               Power(s1,2)*(s2 - t1)*(s2 - t1 + t2)) + 
            Power(s,4)*(s2*t2*(Power(t1,2) - t1*t2 + 2*Power(t2,2)) + 
               s1*(3*s2*Power(t1,2) - 5*Power(t1,3) + 3*s2*t1*t2 + 
                  Power(t1,2)*t2 + 4*s2*Power(t2,2) - 
                  6*t1*Power(t2,2) + 6*Power(t2,3))) + 
            s*Power(s1,2)*(s2*Power(t2,2)*
                (2*Power(s2,2) + t2*(-2*t1 + t2)) - 
               s1*t2*(-2*Power(s2,3) + Power(s2,2)*(t1 - t2) + 
                  t1*Power(t2,2) + 
                  s2*(Power(t1,2) - 3*t1*t2 + Power(t2,2))) + 
               Power(s1,2)*(-2*Power(s2,3) + Power(s2,2)*(9*t1 + t2) - 
                  s2*(12*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
                  t1*(5*Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)))) + 
            Power(s,3)*s1*(t2*
                (Power(t2,2)*(-t1 + t2) - Power(s2,2)*(t1 + 5*t2) + 
                  s2*(-3*Power(t1,2) + 7*t1*t2 - 2*Power(t2,2))) + 
               s1*(3*Power(s2,2)*(t1 + t2) - 
                  s2*(12*Power(t1,2) + 7*t1*t2 + 9*Power(t2,2)) + 
                  2*(5*Power(t1,3) - 2*Power(t1,2)*t2 + 
                     6*t1*Power(t2,2) - 3*Power(t2,3)))) + 
            Power(s,2)*s1*(s2*Power(t2,2)*
                (Power(s2,2) + (2*t1 - t2)*t2 - s2*(t1 + 2*t2)) + 
               s1*t2*(-2*Power(s2,3) + (2*t1 - t2)*Power(t2,2) + 
                  Power(s2,2)*(2*t1 + 5*t2) + 
                  s2*(3*Power(t1,2) - 10*t1*t2 + Power(t2,2))) + 
               Power(s1,2)*(Power(s2,3) - Power(s2,2)*(9*t1 + 5*t2) + 
                  3*s2*(6*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
                  2*(-5*Power(t1,3) + 3*Power(t1,2)*t2 - 
                     5*t1*Power(t2,2) + Power(t2,3)))))/
          (Power(s - s1,2)*s1*s2*(s - s2 + t1)*Power(t2,3)) + 
         (4*Power(s,8) + Power(s,7)*(-20*s1 - 6*s2 + 4*t1 + 8*t2) + 
            Power(s,6)*(40*Power(s1,2) + 2*Power(s2,2) - 5*s2*t1 + 
               3*Power(t1,2) + s1*(18*s2 - 19*t1 - 33*t2) - 9*s2*t2 + 
               2*t1*t2 + 7*Power(t2,2)) + 
            Power(s1,3)*t1*(Power(s1,3)*(s2 - t1) + 
               Power(s2,2)*Power(t2,2) + s1*s2*t2*(-2*s2 + t2) + 
               Power(s1,2)*(Power(s2,2) - 2*s2*t2 + t1*(-t1 + t2))) - 
            s*Power(s1,2)*(Power(s1,4)*(s2 + t1) + 
               2*Power(s2,3)*Power(t2,2) + 
               Power(s1,3)*(3*Power(s2,2) + 6*s2*t1 - 2*Power(t1,2) - 
                  2*s2*t2) + 
               s1*s2*t2*(-4*Power(s2,2) - 5*s2*t1 + Power(t1,2) + 
                  3*s2*t2 - t1*t2) + 
               Power(s1,2)*(2*Power(s2,3) + 
                  Power(s2,2)*(5*t1 - 6*t2) + 
                  s2*(2*Power(t1,2) - 5*t1*t2 + Power(t2,2)) - 
                  t1*(5*Power(t1,2) - 4*t1*t2 + Power(t2,2)))) + 
            Power(s,2)*s1*(Power(s1,4)*(-2*s2 + t1 - t2) + 
               Power(s2,2)*Power(t2,2)*(-t1 + 2*t2) - 
               s1*s2*t2*(Power(s2,2) - 3*Power(t1,2) + 6*t1*t2 - 
                  2*Power(t2,2) + 3*s2*(2*t1 + t2)) + 
               Power(s1,3)*(5*Power(t1,2) + 2*t1*t2 + Power(t2,2) + 
                  3*s2*(3*t1 + t2)) + 
               Power(s1,2)*(Power(s2,3) + Power(s2,2)*(8*t1 + t2) + 
                  s2*(8*Power(t1,2) - 3*t1*t2 - 3*Power(t2,2)) - 
                  2*t1*(5*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)))) + 
            Power(s,4)*(20*Power(s1,4) - 
               2*Power(s1,3)*(4*s2 + 15*t1 + 19*t2) + 
               Power(s1,2)*(-18*Power(s2,2) - 21*s2*t1 + 
                  25*Power(t1,2) - 2*s2*t2 + 12*t1*t2 + 24*Power(t2,2)\
) + s2*t2*(Power(s2,2) + Power(t1,2) - t1*t2 - 2*Power(t2,2) + 
                  s2*(-2*t1 + t2)) + 
               s1*(-3*Power(s2,3) - 5*Power(t1,3) - 
                  Power(s2,2)*(t1 - 13*t2) + Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) - 4*Power(t2,3) + 
                  s2*(8*Power(t1,2) + t1*t2 + 11*Power(t2,2)))) + 
            Power(s,5)*(-40*Power(s1,3) + Power(s2,2)*t1 + 
               Power(t1,3) + t1*Power(t2,2) + 2*Power(t2,3) + 
               Power(s1,2)*(-13*s2 + 35*t1 + 52*t2) - 
               2*s2*(Power(t1,2) + 4*Power(t2,2)) + 
               s1*(3*Power(s2,2) + 18*s2*(t1 + t2) - 
                  2*(7*Power(t1,2) + 4*t1*t2 + 11*Power(t2,2)))) + 
            Power(s,3)*s1*(-4*Power(s1,4) + 
               2*Power(s1,3)*(6*s2 + 5*t1 + 6*t2) + 
               2*Power(s1,2)*
                (8*Power(s2,2) - 10*Power(t1,2) + 2*s2*(t1 - 3*t2) - 
                  4*t1*t2 - 5*Power(t2,2)) + 
               s2*t2*(-4*Power(s2,2) + 5*s2*(t1 + t2) + 
                  t1*(-3*t1 + 5*t2)) + 
               s1*(4*Power(s2,3) - 4*Power(s2,2)*(t1 + 5*t2) + 
                  s2*(-12*Power(t1,2) - t1*t2 + Power(t2,2)) + 
                  2*(5*Power(t1,3) - 2*Power(t1,2)*t2 + 
                     3*t1*Power(t2,2) + Power(t2,3)))))/
          (Power(s - s1,2)*s1*s2*(s - s2 + t1)*Power(s - s1 + t2,3)) - 
         (Power(s,5)*(Power(t1,3) + t1*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s1,3)*(s2 - t1)*
             (-(s1*s2*Power(t2,2)) - Power(s2,2)*Power(t2,2) + 
               Power(s1,2)*(s2 - t1)*(s2 - t1 + t2)) + 
            Power(s,4)*(s2*t2*(Power(t1,2) - t1*t2 - 2*Power(t2,2)) + 
               s1*(3*s2*Power(t1,2) - 5*Power(t1,3) + 3*s2*t1*t2 + 
                  Power(t1,2)*t2 + 4*s2*Power(t2,2) - 
                  4*t1*Power(t2,2) - 4*Power(t2,3))) + 
            s*Power(s1,2)*(2*Power(s2,3)*Power(t2,2) + 
               s1*s2*t2*(2*Power(s2,2) - s2*(t1 - 3*t2) + 
                  t1*(-t1 + t2)) + 
               Power(s1,2)*(-2*Power(s2,3) + Power(s2,2)*(9*t1 + t2) + 
                  t1*(5*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
                  s2*(-12*Power(t1,2) + 3*t1*t2 + Power(t2,2)))) + 
            Power(s,2)*s1*(Power(s2,2)*Power(t2,2)*(s2 - t1 + 2*t2) + 
               s1*s2*t2*(-2*Power(s2,2) + 2*s2*t1 + 3*Power(t1,2) + 
                  3*s2*t2 - 6*t1*t2 + 2*Power(t2,2)) + 
               Power(s1,2)*(Power(s2,3) - Power(s2,2)*(9*t1 + 5*t2) - 
                  2*t1*(5*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
                  s2*(18*Power(t1,2) + 3*t1*t2 + 2*Power(t2,2)))) + 
            Power(s,3)*s1*(-(s2*t2*
                  (t1*(3*t1 - 5*t2) + s2*(t1 + 5*t2))) + 
               s1*(3*Power(s2,2)*(t1 + t2) - 
                  s2*(12*Power(t1,2) + 7*t1*t2 + 7*Power(t2,2)) + 
                  2*(5*Power(t1,3) - 2*Power(t1,2)*t2 + 
                     3*t1*Power(t2,2) + Power(t2,3)))))/
          (Power(s - s1,2)*s1*s2*t1*Power(t2,3)) + 
         (2*Power(s,7)*(s1 + 2*t1 - 2*t2) - 
            Power(s,6)*(2*Power(s1,2) - 5*Power(t1,2) + 
               s1*(6*s2 + 3*t1 - 5*t2) + 4*s2*(4*t1 - 5*t2) + 2*t1*t2 + 
               3*Power(t2,2)) + 
            Power(s2,3)*t2*(-(Power(s1,2)*Power(s2 - t1,2)) - 
               s1*s2*Power(s2 - t1,2) + Power(s2,2)*t2*(s2 - t1 + t2)) + 
            Power(s,5)*(2*Power(t1,3) + 8*Power(s2,2)*(3*t1 - 5*t2) + 
               Power(s1,2)*(s2 - t2) - Power(t1,2)*t2 - Power(t2,3) + 
               s1*(4*Power(s2,2) - 4*Power(t1,2) + 9*s2*(t1 - 2*t2) + 
                  2*Power(t2,2)) + 
               s2*(-15*Power(t1,2) + 9*t1*t2 + 14*Power(t2,2))) + 
            s*Power(s2,2)*(2*Power(s1,3)*Power(s2 - t1,2) - 
               s2*t2*(Power(t1,3) - Power(s2,2)*(t1 - 2*t2) + 
                  s2*t2*(-4*t1 + 5*t2)) + 
               Power(s1,2)*(s2 - t1)*
                (Power(s2,2) - 2*Power(t1,2) + s2*(t1 + 5*t2)) + 
               s1*(Power(t1,3)*(t1 - 2*t2) + 6*Power(s2,3)*t2 + 
                  s2*t1*(-2*Power(t1,2) + 3*t1*t2 + Power(t2,2)) + 
                  Power(s2,2)*(Power(t1,2) - 7*t1*t2 + 2*Power(t2,2)))) \
+ Power(s,3)*s2*(-4*Power(s1,3)*(s2 - t1) + 4*Power(s2,3)*(t1 - 5*t2) + 
               Power(t1,3)*(t1 - t2) + 
               Power(s1,2)*(-10*Power(s2,2) + 6*s2*t1 + 
                  3*Power(t1,2) + 4*s2*t2 - 5*t1*t2) + 
               s2*t2*(-3*Power(t1,2) + 4*t1*t2 - 10*Power(t2,2)) + 
               Power(s2,2)*(-5*Power(t1,2) + 14*t1*t2 + 
                  20*Power(t2,2)) + 
               s1*(-6*Power(s2,3) + Power(s2,2)*(3*t1 - 4*t2) + 
                  t1*(Power(t1,2) - t1*t2 + 3*Power(t2,2)) + 
                  s2*(Power(t1,2) - 5*t1*t2 + 12*Power(t2,2)))) + 
            Power(s,4)*(Power(s1,3)*(3*s2 - t1) + 
               Power(s1,2)*(8*Power(s2,2) - t1*(t1 - 2*t2) + 
                  s2*(-3*t1 + t2)) + 
               s1*(4*Power(s2,3) + Power(s2,2)*(-9*t1 + 21*t2) + 
                  s2*(6*Power(t1,2) + t1*t2 - 8*Power(t2,2)) - 
                  t1*(2*Power(t1,2) - t1*t2 + Power(t2,2))) - 
               s2*(3*Power(t1,3) + 8*Power(s2,2)*(2*t1 - 5*t2) - 
                  3*Power(t1,2)*t2 + t1*Power(t2,2) - 5*Power(t2,3) + 
                  s2*(-15*Power(t1,2) + 16*t1*t2 + 25*Power(t2,2)))) + 
            Power(s,2)*s2*(Power(s1,3)*s2*(-s2 + t1) + 
               Power(s1,2)*(2*Power(s2,3) + Power(t1,2)*t2 + 
                  s2*t1*(t1 + 6*t2) - Power(s2,2)*(3*t1 + 8*t2)) + 
               s1*(2*Power(s2,4) - Power(t1,3)*(t1 - 2*t2) - 
                  9*Power(s2,3)*t2 + 
                  Power(s2,2)*
                   (-4*Power(t1,2) + 9*t1*t2 - 8*Power(t2,2)) + 
                  s2*t1*(3*Power(t1,2) - 2*t1*t2 - 3*Power(t2,2))) + 
               s2*(-(Power(t1,3)*(t1 - 2*t2)) + 4*Power(s2,3)*t2 - 
                  Power(s2,2)*t2*(6*t1 + 5*t2) + 
                  s2*(Power(t1,3) + Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
                     10*Power(t2,3)))))/
          (s1*Power(s - s2,2)*s2*Power(s - s2 + t1,3)*t2))*lg2(-s))/s + 
    (8*(2*Power(s2,2)*Power(s2 - t1,3)*Power(t1,3)*Power(t2,6) + 
         Power(s,9)*t1*(Power(t1,4) + 4*t1*Power(t2,3) - Power(t2,4)) - 
         s1*s2*(s2 - t1)*t1*Power(t2,5)*
          (Power(s2,4)*(t1 - t2) + 
            s2*Power(t1,2)*(11*Power(t1,2) + 4*t1*t2 - 3*Power(t2,2)) - 
            2*Power(t1,3)*(Power(t1,2) + t1*t2 - Power(t2,2)) + 
            Power(s2,3)*(4*Power(t1,2) + Power(t2,2)) - 
            Power(s2,2)*t1*(14*Power(t1,2) + 2*t1*t2 + Power(t2,2))) + 
         Power(s1,7)*Power(s2 - t1,2)*
          (-(Power(s2,2)*Power(t1,3)) + Power(s2,3)*Power(t2,2) + 
            s2*Power(t1,2)*(2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(t1,3)*(-Power(t1,2) + t1*t2 + 2*Power(t2,2))) + 
         Power(s1,6)*(s2 - t1)*
          (Power(s2,5)*Power(t2,2) + 
            Power(t1,4)*t2*(-3*Power(t1,2) + 3*t1*t2 + 8*Power(t2,2)) + 
            s2*Power(t1,3)*(Power(t1,3) + 11*Power(t1,2)*t2 - 
               7*t1*Power(t2,2) - 8*Power(t2,3)) + 
            Power(s2,3)*t1*(3*Power(t1,3) + 9*Power(t1,2)*t2 + 
               Power(t2,3)) + 
            Power(s2,2)*Power(t1,2)*
             (-3*Power(t1,3) - 15*Power(t1,2)*t2 + t1*Power(t2,2) + 
               2*Power(t2,3)) - 
            Power(s2,4)*(Power(t1,3) + 2*Power(t1,2)*t2 - 
               2*t1*Power(t2,2) + 4*Power(t2,3))) + 
         Power(s1,2)*Power(t2,4)*
          (-2*Power(t1,6)*t2*(t1 + t2) + 
            s2*Power(t1,5)*(5*Power(t1,2) + 13*t1*t2 - Power(t2,2)) + 
            Power(s2,6)*(6*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
            Power(s2,5)*t1*(-16*Power(t1,2) + 7*t1*t2 + 5*Power(t2,2)) + 
            Power(s2,4)*t1*(8*Power(t1,3) - 3*Power(t1,2)*t2 - 
               10*t1*Power(t2,2) - 2*Power(t2,3)) - 
            Power(s2,2)*Power(t1,3)*
             (16*Power(t1,3) + 23*Power(t1,2)*t2 - 10*t1*Power(t2,2) + 
               4*Power(t2,3)) + 
            Power(s2,3)*Power(t1,2)*
             (13*Power(t1,3) + 13*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
               6*Power(t2,3))) + 
         Power(s1,5)*t2*(-2*Power(t1,5)*t2*
             (Power(t1,2) - 3*t1*t2 - 5*Power(t2,2)) - 
            Power(s2,6)*(Power(t1,2) - 2*t1*t2 + 4*Power(t2,2)) + 
            s2*Power(t1,4)*(4*Power(t1,3) + 16*Power(t1,2)*t2 - 
               21*t1*Power(t2,2) - 23*Power(t2,3)) + 
            Power(s2,3)*Power(t1,2)*
             (31*Power(t1,3) + 60*Power(t1,2)*t2 - 14*t1*Power(t2,2) - 
               17*Power(t2,3)) - 
            Power(s2,4)*t1*(25*Power(t1,3) + 40*Power(t1,2)*t2 - 
               18*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s2,5)*(9*Power(t1,3) + 8*Power(t1,2)*t2 - 
               5*t1*Power(t2,2) + 6*Power(t2,3)) + 
            2*Power(s2,2)*Power(t1,3)*
             (-9*Power(t1,3) - 22*Power(t1,2)*t2 + 10*t1*Power(t2,2) + 
               13*Power(t2,3))) + 
         Power(s1,3)*Power(t2,3)*
          (Power(t1,6)*t2*(5*t1 + 6*t2) - 
            s2*Power(t1,5)*t2*(18*t1 + 11*t2) + 
            Power(s2,6)*(-10*Power(t1,2) + 9*t1*t2 - 4*Power(t2,2)) + 
            Power(s2,3)*Power(t1,2)*
             (45*Power(t1,3) + 18*Power(t1,2)*t2 - 16*t1*Power(t2,2) - 
               21*Power(t2,3)) + 
            Power(s2,5)*(39*Power(t1,3) - 7*Power(t1,2)*t2 - 
               10*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s2,4)*t1*(-61*Power(t1,3) - 21*Power(t1,2)*t2 + 
               28*t1*Power(t2,2) + 7*Power(t2,3)) + 
            Power(s2,2)*Power(t1,3)*
             (-13*Power(t1,3) + 14*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
               14*Power(t2,3))) - 
         Power(s1,4)*Power(t2,2)*
          (Power(s2,6)*(-6*Power(t1,2) + 7*t1*t2 - 6*Power(t2,2)) + 
            2*Power(t1,5)*t2*(Power(t1,2) + 4*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,3)*Power(t1,2)*
             (61*Power(t1,3) + 66*Power(t1,2)*t2 - 30*t1*Power(t2,2) - 
               29*Power(t2,3)) + 
            s2*Power(t1,4)*(5*Power(t1,3) + 2*Power(t1,2)*t2 - 
               24*t1*Power(t2,2) - 10*Power(t2,3)) + 
            Power(s2,5)*(30*Power(t1,3) + 5*Power(t1,2)*t2 - 
               10*t1*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s2,4)*t1*(-61*Power(t1,3) - 52*Power(t1,2)*t2 + 
               36*t1*Power(t2,2) + 7*Power(t2,3)) + 
            Power(s2,2)*Power(t1,3)*
             (-29*Power(t1,3) - 30*Power(t1,2)*t2 + 25*t1*Power(t2,2) + 
               26*Power(t2,3))) + 
         Power(s,8)*(t1*(2*Power(t1,5) + 3*Power(t1,4)*t2 + 
               11*Power(t1,2)*Power(t2,3) + 7*t1*Power(t2,4) - 
               3*Power(t2,5) - 
               s2*(3*Power(t1,4) + Power(t1,3)*t2 - 
                  2*Power(t1,2)*Power(t2,2) + 20*t1*Power(t2,3) - 
                  6*Power(t2,4))) + 
            s1*(s2*(2*Power(t1,4) + 4*Power(t1,3)*t2 + 
                  5*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               t1*(-7*Power(t1,4) + Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) + 
                  6*Power(t2,4)))) - 
         s*(Power(s1,7)*(s2 - t1)*
             (-2*Power(s2,2)*Power(t1,3) + 2*Power(s2,3)*Power(t2,2) + 
               s2*Power(t1,2)*(4*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               2*Power(t1,3)*(-Power(t1,2) + t1*t2 + 2*Power(t2,2))) + 
            s2*(s2 - t1)*t1*Power(t2,5)*
             (2*Power(t1,5) + Power(s2,4)*t2 - 
               s2*Power(t1,3)*(11*t1 + 6*t2) - 
               Power(s2,3)*(7*Power(t1,2) - t1*t2 + Power(t2,2)) + 
               Power(s2,2)*t1*(16*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2))\
) + Power(s1,6)*(6*Power(s2,5)*Power(t2,2) + 
               s2*Power(t1,3)*
                (28*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  34*t1*Power(t2,2) - 12*Power(t2,3)) + 
               Power(s2,3)*t1*
                (28*Power(t1,3) + 21*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*
                (-42*Power(t1,3) - 24*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 7*Power(t2,3)) - 
               Power(s2,4)*(7*Power(t1,3) + 6*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,4)*(-7*Power(t1,3) + 17*t1*Power(t2,2) + 
                  10*Power(t2,3))) + 
            s1*Power(t2,4)*(-4*Power(t1,7)*t2 + 
               2*Power(t1,5)*Power(t2,3) + 
               Power(s2,6)*(5*Power(t1,2) - 7*t1*t2 + Power(t2,2)) + 
               Power(s2,5)*(-6*Power(t1,3) + 13*t1*Power(t2,2)) + 
               s2*Power(t1,4)*
                (10*Power(t1,3) + 27*Power(t1,2)*t2 - t1*Power(t2,2) - 
                  10*Power(t2,3)) + 
               Power(s2,2)*Power(t1,3)*
                (-39*Power(t1,3) - 66*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) + 2*Power(t2,3)) - 
               Power(s2,4)*t1*
                (22*Power(t1,3) + 6*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,3)*Power(t1,2)*
                (52*Power(t1,3) + 56*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  14*Power(t2,3))) + 
            Power(s1,5)*(3*Power(s2,6)*Power(t2,2) + 
               Power(t1,5)*t2*
                (18*Power(t1,2) - 11*t1*t2 - 51*Power(t2,2)) - 
               Power(s2,5)*(4*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  10*t1*Power(t2,2) + 24*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*
                (22*Power(t1,4) + 164*Power(t1,3)*t2 + 
                  70*Power(t1,2)*Power(t2,2) - 78*t1*Power(t2,3) - 
                  35*Power(t2,4)) + 
               s2*Power(t1,3)*
                (-6*Power(t1,4) - 85*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) + 108*t1*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(s2,3)*t1*
                (-30*Power(t1,4) - 156*Power(t1,3)*t2 - 
                  92*Power(t1,2)*Power(t2,2) + 53*t1*Power(t2,3) + 
                  13*Power(t2,4)) + 
               Power(s2,4)*(18*Power(t1,4) + 70*Power(t1,3)*t2 + 
                  19*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
                  18*Power(t2,4))) + 
            Power(s1,3)*Power(t2,2)*
             (Power(s2,6)*(16*Power(t1,2) - 17*t1*t2 + 
                  12*Power(t2,2)) - 
               Power(t1,4)*t2*
                (8*Power(t1,3) + 34*Power(t1,2)*t2 + 
                  19*t1*Power(t2,2) - 8*Power(t2,3)) - 
               Power(s2,5)*(84*Power(t1,3) + 48*Power(t1,2)*t2 - 
                  67*t1*Power(t2,2) + 24*Power(t2,3)) + 
               2*Power(s2,2)*Power(t1,2)*
                (50*Power(t1,4) + 63*Power(t1,3)*t2 - 
                  4*Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) - 
                  27*Power(t2,4)) + 
               Power(s2,4)*(180*Power(t1,4) + 246*Power(t1,3)*t2 - 
                  92*Power(t1,2)*Power(t2,2) - 69*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s2*Power(t1,3)*
                (-20*Power(t1,4) + Power(t1,3)*t2 + 
                  76*Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,3)*t1*
                (-192*Power(t1,4) - 300*Power(t1,3)*t2 - 
                  15*Power(t1,2)*Power(t2,2) + 140*t1*Power(t2,3) + 
                  27*Power(t2,4))) + 
            Power(s1,4)*t2*(Power(s2,6)*
                (-3*Power(t1,2) + 6*t1*t2 - 10*Power(t2,2)) + 
               2*Power(t1,4)*t2*
                (-5*Power(t1,3) + 13*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) - 7*Power(t2,3)) + 
               Power(s2,5)*(31*Power(t1,3) + 48*Power(t1,2)*t2 - 
                  43*t1*Power(t2,2) + 36*Power(t2,3)) + 
               Power(s2,3)*t1*
                (127*Power(t1,4) + 340*Power(t1,3)*t2 + 
                  91*Power(t1,2)*Power(t2,2) - 140*t1*Power(t2,3) - 
                  32*Power(t2,4)) + 
               Power(s2,4)*(-94*Power(t1,4) - 223*Power(t1,3)*t2 + 
                  32*Power(t1,2)*Power(t2,2) + 55*t1*Power(t2,3) - 
                  14*Power(t2,4)) + 
               s2*Power(t1,3)*
                (20*Power(t1,4) + 79*Power(t1,3)*t2 - 
                  38*Power(t1,2)*Power(t2,2) - 96*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (-81*Power(t1,4) - 240*Power(t1,3)*t2 - 
                  59*Power(t1,2)*Power(t2,2) + 107*t1*Power(t2,3) + 
                  63*Power(t2,4))) + 
            Power(s1,2)*Power(t2,3)*
             (Power(s2,6)*(-18*Power(t1,2) + 17*t1*t2 - 6*Power(t2,2)) + 
               Power(t1,5)*t2*
                (15*Power(t1,2) + 13*t1*t2 - Power(t2,2)) + 
               Power(s2,5)*(73*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  46*t1*Power(t2,2) + 6*Power(t2,3)) - 
               s2*Power(t1,3)*t2*
                (71*Power(t1,3) + 32*Power(t1,2)*t2 - 
                  31*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,4)*t1*
                (-117*Power(t1,3) - 80*Power(t1,2)*t2 + 
                  64*t1*Power(t2,2) + 36*Power(t2,3)) + 
               Power(s2,3)*t1*
                (87*Power(t1,4) + 22*Power(t1,3)*t2 - 
                  Power(t1,2)*Power(t2,2) - 68*t1*Power(t2,3) - 
                  8*Power(t2,4)) + 
               Power(s2,2)*(-25*Power(t1,6) + 86*Power(t1,5)*t2 + 
                  Power(t1,4)*Power(t2,2) + 18*Power(t1,2)*Power(t2,4)))) \
+ Power(s,7)*(Power(s1,2)*(Power(s2,2)*
                (Power(t1,3) + 2*Power(t1,2)*t2 - t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(21*Power(t1,4) - 6*Power(t1,3)*t2 - 
                  7*Power(t1,2)*Power(t2,2) + 48*t1*Power(t2,3) - 
                  14*Power(t2,4)) - 
               s2*(12*Power(t1,4) + 19*Power(t1,3)*t2 + 
                  19*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
                  4*Power(t2,4))) + 
            t1*(Power(t1,6) + 6*Power(t1,5)*t2 + 
               2*Power(t1,4)*Power(t2,2) + 11*Power(t1,3)*Power(t2,3) + 
               21*Power(t1,2)*Power(t2,4) + 2*t1*Power(t2,5) - 
               3*Power(t2,6) + 
               Power(s2,2)*(3*Power(t1,4) + 3*Power(t1,3)*t2 - 
                  5*Power(t1,2)*Power(t2,2) + 41*t1*Power(t2,3) - 
                  15*Power(t2,4)) - 
               s2*(4*Power(t1,5) + 12*Power(t1,4)*t2 - 
                  2*Power(t1,3)*Power(t2,2) + 
                  36*Power(t1,2)*Power(t2,3) + 32*t1*Power(t2,4) - 
                  17*Power(t2,5))) + 
            s1*(-(Power(s2,2)*
                  (6*Power(t1,4) + 16*Power(t1,3)*t2 + 
                    21*Power(t1,2)*Power(t2,2) - 19*t1*Power(t2,3) + 
                    5*Power(t2,4))) - 
               t1*(14*Power(t1,5) + 16*Power(t1,4)*t2 - 
                  5*Power(t1,3)*Power(t2,2) + 
                  50*Power(t1,2)*Power(t2,3) + 31*t1*Power(t2,4) - 
                  15*Power(t2,5)) + 
               s2*(24*Power(t1,5) + 19*Power(t1,4)*t2 + 
                  14*Power(t1,3)*Power(t2,2) + 
                  98*Power(t1,2)*Power(t2,3) - 41*t1*Power(t2,4) + 
                  2*Power(t2,5)))) + 
         Power(s,2)*(Power(s1,7)*
             (-(Power(s2,2)*Power(t1,3)) + Power(s2,3)*Power(t2,2) + 
               s2*Power(t1,2)*(2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
               Power(t1,3)*(-Power(t1,2) + t1*t2 + 2*Power(t2,2))) - 
            t1*Power(t2,4)*(2*Power(s2,6)*t2 + 
               2*Power(t1,5)*(t1 - t2)*t2 + 
               Power(s2,5)*(-8*Power(t1,2) + 2*t1*t2 - 8*Power(t2,2)) + 
               s2*Power(t1,4)*
                (-5*Power(t1,2) - 14*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,4)*(29*Power(t1,3) + 19*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*
                (23*Power(t1,3) + 47*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + 6*Power(t2,3)) - 
               Power(s2,3)*t1*
                (39*Power(t1,3) + 58*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) + 12*Power(t2,3))) + 
            Power(s1,6)*(9*Power(s2,4)*Power(t2,2) + 
               s2*Power(t1,2)*
                (-39*Power(t1,3) + 6*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(t1,3)*(14*Power(t1,3) - 9*Power(t1,2)*t2 - 
                  25*t1*Power(t2,2) + 2*Power(t2,3)) - 
               Power(s2,3)*(11*Power(t1,3) + 6*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(36*Power(t1,4) + 9*Power(t1,3)*t2 - 
                  7*t1*Power(t2,3))) + 
            Power(s1,5)*(12*Power(s2,5)*Power(t2,2) - 
               Power(s2,4)*(18*Power(t1,3) + 27*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + 44*Power(t2,3)) + 
               Power(t1,3)*(-21*Power(t1,4) - 21*Power(t1,3)*t2 + 
                  53*Power(t1,2)*Power(t2,2) + 52*t1*Power(t2,3) - 
                  20*Power(t2,4)) + 
               s2*Power(t1,2)*
                (84*Power(t1,4) + 102*Power(t1,3)*t2 - 
                  65*Power(t1,2)*Power(t2,2) - 62*t1*Power(t2,3) - 
                  8*Power(t2,4)) + 
               Power(s2,3)*(78*Power(t1,4) + 123*Power(t1,3)*t2 + 
                  21*Power(t1,2)*Power(t2,2) - 37*t1*Power(t2,3) + 
                  19*Power(t2,4)) + 
               Power(s2,2)*t1*
                (-123*Power(t1,4) - 177*Power(t1,3)*t2 - 
                  27*Power(t1,2)*Power(t2,2) + 50*t1*Power(t2,3) + 
                  29*Power(t2,4))) + 
            s1*Power(t2,3)*(Power(s2,6)*
                (-9*Power(t1,2) + 9*t1*t2 - 2*Power(t2,2)) + 
               Power(t1,4)*t2*
                (15*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) - 5*Power(t2,3)) - 
               2*s2*Power(t1,3)*t2*
                (44*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  23*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,5)*(37*Power(t1,3) + 19*Power(t1,2)*t2 - 
                  49*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,4)*t1*
                (-58*Power(t1,3) - 26*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + 54*Power(t2,3)) + 
               Power(s2,3)*t1*
                (41*Power(t1,4) - 90*Power(t1,3)*t2 + 
                  42*Power(t1,2)*Power(t2,2) - 76*t1*Power(t2,3) - 
                  18*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (-11*Power(t1,4) + 161*Power(t1,3)*t2 + 
                  10*Power(t1,2)*Power(t2,2) - 19*t1*Power(t2,3) + 
                  30*Power(t2,4))) - 
            Power(s1,2)*Power(t2,2)*
             (Power(s2,6)*(-14*Power(t1,2) + 13*t1*t2 - 
                  7*Power(t2,2)) + 
               Power(s2,5)*(79*Power(t1,3) + 80*Power(t1,2)*t2 - 
                  104*t1*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s2,3)*t1*
                (213*Power(t1,4) + 394*Power(t1,3)*t2 + 
                  172*Power(t1,2)*Power(t2,2) - 179*t1*Power(t2,3) - 
                  93*Power(t2,4)) + 
               Power(t1,3)*t2*
                (12*Power(t1,4) + 54*Power(t1,3)*t2 + 
                  19*Power(t1,2)*Power(t2,2) - 29*t1*Power(t2,3) + 
                  4*Power(t2,4)) - 
               2*Power(s2,4)*
                (91*Power(t1,4) + 166*Power(t1,3)*t2 - 
                  12*Power(t1,2)*Power(t2,2) - 84*t1*Power(t2,3) + 
                  7*Power(t2,4)) + 
               s2*Power(t1,2)*
                (30*Power(t1,5) - 15*Power(t1,4)*t2 - 
                  135*Power(t1,3)*Power(t2,2) + 
                  81*Power(t1,2)*Power(t2,3) + 10*t1*Power(t2,4) - 
                  18*Power(t2,5)) - 
               Power(s2,2)*t1*
                (126*Power(t1,5) + 152*Power(t1,4)*t2 + 
                  19*Power(t1,3)*Power(t2,2) + 
                  83*Power(t1,2)*Power(t2,3) - 135*t1*Power(t2,4) - 
                  12*Power(t2,5))) + 
            Power(s1,3)*t2*(Power(s2,6)*
                (-3*Power(t1,2) + 6*t1*t2 - 8*Power(t2,2)) + 
               Power(s2,5)*(39*Power(t1,3) + 90*Power(t1,2)*t2 - 
                  87*t1*Power(t2,2) + 57*Power(t2,3)) + 
               Power(t1,3)*t2*
                (-20*Power(t1,4) + 44*Power(t1,3)*t2 + 
                  107*Power(t1,2)*Power(t2,2) - 56*t1*Power(t2,3) - 
                  2*Power(t2,4)) - 
               Power(s2,4)*(133*Power(t1,4) + 416*Power(t1,3)*t2 + 
                  63*Power(t1,2)*Power(t2,2) - 215*t1*Power(t2,3) + 
                  54*Power(t2,4)) + 
               s2*Power(t1,2)*
                (40*Power(t1,5) + 156*Power(t1,4)*t2 + 
                  7*Power(t1,3)*Power(t2,2) - 
                  78*Power(t1,2)*Power(t2,3) + 54*t1*Power(t2,4) - 
                  45*Power(t2,5)) - 
               Power(s2,2)*t1*
                (144*Power(t1,5) + 476*Power(t1,4)*t2 + 
                  352*Power(t1,3)*Power(t2,2) + 
                  13*Power(t1,2)*Power(t2,3) - 207*t1*Power(t2,4) - 
                  39*Power(t2,5)) + 
               Power(s2,3)*(201*Power(t1,5) + 660*Power(t1,4)*t2 + 
                  450*Power(t1,3)*Power(t2,2) - 
                  226*Power(t1,2)*Power(t2,3) - 173*t1*Power(t2,4) + 
                  6*Power(t2,5))) + 
            Power(s1,4)*(3*Power(s2,6)*Power(t2,2) - 
               Power(s2,5)*(6*Power(t1,3) + 21*Power(t1,2)*t2 - 
                  24*t1*Power(t2,2) + 44*Power(t2,3)) + 
               Power(t1,3)*t2*
                (45*Power(t1,4) - 10*Power(t1,3)*t2 - 
                  120*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) + 
                  22*Power(t2,4)) + 
               Power(s2,4)*(32*Power(t1,4) + 160*Power(t1,3)*t2 + 
                  110*Power(t1,2)*Power(t2,2) - 108*t1*Power(t2,3) + 
                  75*Power(t2,4)) + 
               Power(s2,2)*t1*
                (50*Power(t1,5) + 411*Power(t1,4)*t2 + 
                  401*Power(t1,3)*Power(t2,2) - 
                  31*Power(t1,2)*Power(t2,3) - 152*t1*Power(t2,4) - 
                  49*Power(t2,5)) - 
               s2*Power(t1,2)*
                (15*Power(t1,5) + 215*Power(t1,4)*t2 + 
                  106*Power(t1,3)*Power(t2,2) - 
                  168*Power(t1,2)*Power(t2,3) + 6*t1*Power(t2,4) - 
                  36*Power(t2,5)) - 
               Power(s2,3)*(61*Power(t1,5) + 380*Power(t1,4)*t2 + 
                  420*Power(t1,3)*Power(t2,2) - 
                  100*Power(t1,2)*Power(t2,3) - 138*t1*Power(t2,4) + 
                  18*Power(t2,5)))) + 
         Power(s,6)*(Power(s1,3)*
             (Power(s2,3)*Power(t2,2) - 
               Power(s2,2)*(5*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + 8*Power(t2,3)) + 
               s2*(30*Power(t1,4) + 35*Power(t1,3)*t2 + 
                  27*Power(t1,2)*Power(t2,2) - 40*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               t1*(-35*Power(t1,4) + 15*Power(t1,3)*t2 + 
                  20*Power(t1,2)*Power(t2,2) - 62*t1*Power(t2,3) + 
                  16*Power(t2,4))) + 
            Power(s1,2)*(-(Power(s2,3)*
                  (3*Power(t1,3) + 7*Power(t1,2)*t2 - 
                    5*t1*Power(t2,2) + 8*Power(t2,3))) + 
               Power(s2,2)*(36*Power(t1,4) + 81*Power(t1,3)*t2 + 
                  80*Power(t1,2)*Power(t2,2) - 86*t1*Power(t2,3) + 
                  23*Power(t2,4)) + 
               t1*(42*Power(t1,5) + 33*Power(t1,4)*t2 - 
                  29*Power(t1,3)*Power(t2,2) + 
                  92*Power(t1,2)*Power(t2,3) + 59*t1*Power(t2,4) - 
                  27*Power(t2,5)) - 
               s2*(81*Power(t1,5) + 76*Power(t1,4)*t2 + 
                  61*Power(t1,3)*Power(t2,2) + 
                  182*Power(t1,2)*Power(t2,3) - 104*t1*Power(t2,4) + 
                  6*Power(t2,5))) + 
            s1*(Power(s2,3)*(6*Power(t1,4) + 24*Power(t1,3)*t2 + 
                  37*Power(t1,2)*Power(t2,2) - 36*t1*Power(t2,3) + 
                  10*Power(t2,4)) - 
               Power(s2,2)*(27*Power(t1,5) + 63*Power(t1,4)*t2 + 
                  71*Power(t1,3)*Power(t2,2) + 
                  186*Power(t1,2)*Power(t2,3) - 113*t1*Power(t2,4) + 
                  10*Power(t2,5)) - 
               t1*(7*Power(t1,6) + 35*Power(t1,5)*t2 + 
                  3*Power(t1,4)*Power(t2,2) + 
                  32*Power(t1,3)*Power(t2,3) + 
                  96*Power(t1,2)*Power(t2,4) - 12*Power(t2,6)) + 
               s2*(28*Power(t1,6) + 84*Power(t1,5)*t2 + 
                  33*Power(t1,4)*Power(t2,2) + 
                  187*Power(t1,3)*Power(t2,3) + 
                  118*Power(t1,2)*Power(t2,4) - 81*t1*Power(t2,5) + 
                  Power(t2,6))) - 
            t1*(Power(s2,3)*(Power(t1,4) + 3*Power(t1,3)*t2 - 
                  3*Power(t1,2)*Power(t2,2) + 44*t1*Power(t2,3) - 
                  20*Power(t2,4)) - 
               Power(s2,2)*(2*Power(t1,5) + 15*Power(t1,4)*t2 + 
                  Power(t1,3)*Power(t2,2) + 
                  45*Power(t1,2)*Power(t2,3) + 58*t1*Power(t2,4) - 
                  40*Power(t2,5)) + 
               s2*(Power(t1,6) + 15*Power(t1,5)*t2 + 
                  10*Power(t1,4)*Power(t2,2) + 
                  17*Power(t1,3)*Power(t2,3) + 
                  58*Power(t1,2)*Power(t2,4) + 10*t1*Power(t2,5) - 
                  16*Power(t2,6)) + 
               t2*(-3*Power(t1,6) - 4*Power(t1,5)*t2 - 
                  4*Power(t1,4)*Power(t2,2) - 
                  13*Power(t1,3)*Power(t2,3) - 
                  22*Power(t1,2)*Power(t2,4) + 5*t1*Power(t2,5) + 
                  Power(t2,6)))) - 
         Power(s,3)*(Power(s1,6)*
             (4*Power(s2,3)*Power(t2,2) + 
               s2*t1*(12*Power(t1,3) - Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(t1,2)*(-7*Power(t1,3) + 6*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,2)*(-5*Power(t1,3) - 2*Power(t1,2)*t2 + 
                  t1*Power(t2,2) - 2*Power(t2,3))) + 
            Power(s1,5)*(15*Power(s2,4)*Power(t2,2) - 
               Power(s2,3)*(24*Power(t1,3) + 25*Power(t1,2)*t2 - 
                  8*t1*Power(t2,2) + 32*Power(t2,3)) + 
               Power(s2,2)*(90*Power(t1,4) + 72*Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) - 50*t1*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(t1,2)*(42*Power(t1,4) - 12*Power(t1,3)*t2 - 
                  65*Power(t1,2)*Power(t2,2) + 14*t1*Power(t2,3) + 
                  8*Power(t2,4)) + 
               s2*t1*(-108*Power(t1,4) - 31*Power(t1,3)*t2 + 
                  56*Power(t1,2)*Power(t2,2) - 14*t1*Power(t2,3) + 
                  17*Power(t2,4))) + 
            t1*Power(t2,3)*(Power(s2,6)*(-t1 + t2) + 
               Power(s2,5)*(3*Power(t1,2) + 4*t1*t2 - 13*Power(t2,2)) + 
               Power(t1,4)*t2*(5*Power(t1,2) + t1*t2 - 6*Power(t2,2)) - 
               s2*Power(t1,2)*t2*
                (35*Power(t1,3) + 13*Power(t1,2)*t2 - 
                  23*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,4)*(-2*Power(t1,3) + 29*Power(t1,2)*t2 - 
                  10*t1*Power(t2,2) + 25*Power(t2,3)) - 
               Power(s2,3)*(Power(t1,4) + 93*Power(t1,3)*t2 - 
                  7*Power(t1,2)*Power(t2,2) + 20*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,2)*t1*
                (Power(t1,4) + 89*Power(t1,3)*t2 + 
                  24*Power(t1,2)*Power(t2,2) - 19*t1*Power(t2,3) + 
                  18*Power(t2,4))) + 
            Power(s1,4)*(10*Power(s2,5)*Power(t2,2) - 
               Power(s2,4)*(22*Power(t1,3) + 45*Power(t1,2)*t2 - 
                  34*t1*Power(t2,2) + 70*Power(t2,3)) + 
               Power(s2,3)*(112*Power(t1,4) + 257*Power(t1,3)*t2 + 
                  116*Power(t1,2)*Power(t2,2) - 159*t1*Power(t2,3) + 
                  70*Power(t2,4)) + 
               s2*t1*(140*Power(t1,5) + 270*Power(t1,4)*t2 + 
                  18*Power(t1,3)*Power(t2,2) - 
                  35*Power(t1,2)*Power(t2,3) - 12*t1*Power(t2,4) - 
                  30*Power(t2,5)) + 
               Power(t1,2)*(-35*Power(t1,5) - 70*Power(t1,4)*t2 + 
                  80*Power(t1,3)*Power(t2,2) + 
                  97*Power(t1,2)*Power(t2,3) - 93*t1*Power(t2,4) + 
                  2*Power(t2,5)) - 
               Power(s2,2)*(195*Power(t1,5) + 411*Power(t1,4)*t2 + 
                  245*Power(t1,3)*Power(t2,2) - 
                  47*Power(t1,2)*Power(t2,3) - 158*t1*Power(t2,4) + 
                  10*Power(t2,5))) + 
            s1*Power(t2,2)*(Power(s2,6)*
                (4*Power(t1,2) - 3*t1*t2 + Power(t2,2)) - 
               Power(s2,5)*(26*Power(t1,3) + 45*Power(t1,2)*t2 - 
                  53*t1*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,3)*t2*
                (-8*Power(t1,4) - 38*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) + 32*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,4)*(68*Power(t1,4) + 150*Power(t1,3)*t2 + 
                  52*Power(t1,2)*Power(t2,2) - 141*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,3)*t1*
                (-94*Power(t1,4) - 159*Power(t1,3)*t2 - 
                  107*Power(t1,2)*Power(t2,2) + 41*t1*Power(t2,3) + 
                  106*Power(t2,4)) + 
               2*Power(s2,2)*t1*
                (34*Power(t1,5) + 23*Power(t1,4)*t2 - 
                  42*Power(t1,3)*Power(t2,2) + 
                  86*Power(t1,2)*Power(t2,3) - 59*t1*Power(t2,4) - 
                  11*Power(t2,5)) + 
               s2*Power(t1,2)*
                (-20*Power(t1,5) + 19*Power(t1,4)*t2 + 
                  138*Power(t1,3)*Power(t2,2) - 
                  102*Power(t1,2)*Power(t2,3) - 33*t1*Power(t2,4) + 
                  26*Power(t2,5))) + 
            Power(s1,2)*t2*(-(Power(s2,6)*
                  (Power(t1,2) - 2*t1*t2 + 2*Power(t2,2))) + 
               Power(s2,5)*(21*Power(t1,3) + 68*Power(t1,2)*t2 - 
                  65*t1*Power(t2,2) + 32*Power(t2,3)) - 
               Power(s2,4)*(85*Power(t1,4) + 324*Power(t1,3)*t2 + 
                  167*Power(t1,2)*Power(t2,2) - 272*t1*Power(t2,3) + 
                  60*Power(t2,4)) - 
               Power(s2,2)*t1*
                (126*Power(t1,5) + 428*Power(t1,4)*t2 + 
                  450*Power(t1,3)*Power(t2,2) + 
                  310*Power(t1,2)*Power(t2,3) - 202*t1*Power(t2,4) - 
                  113*Power(t2,5)) + 
               Power(t1,2)*t2*
                (-20*Power(t1,5) + 36*Power(t1,4)*t2 + 
                  97*Power(t1,3)*Power(t2,2) - 
                  93*Power(t1,2)*Power(t2,3) - 12*t1*Power(t2,4) + 
                  6*Power(t2,5)) + 
               Power(s2,3)*(151*Power(t1,5) + 548*Power(t1,4)*t2 + 
                  592*Power(t1,3)*Power(t2,2) - 
                  31*Power(t1,2)*Power(t2,3) - 305*t1*Power(t2,4) + 
                  16*Power(t2,5)) + 
               s2*t1*(40*Power(t1,6) + 154*Power(t1,5)*t2 + 
                  39*Power(t1,4)*Power(t2,2) + 
                  31*Power(t1,3)*Power(t2,3) + 
                  201*Power(t1,2)*Power(t2,4) - 106*t1*Power(t2,5) - 
                  8*Power(t2,6))) + 
            Power(s1,3)*(Power(s2,6)*Power(t2,2) - 
               Power(s2,5)*(4*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  22*t1*Power(t2,2) + 32*Power(t2,3)) + 
               Power(s2,4)*(28*Power(t1,4) + 170*Power(t1,3)*t2 + 
                  190*Power(t1,2)*Power(t2,2) - 194*t1*Power(t2,3) + 
                  105*Power(t2,4)) + 
               Power(t1,2)*t2*
                (60*Power(t1,5) + 10*Power(t1,4)*t2 - 
                  130*Power(t1,3)*Power(t2,2) + 
                  25*Power(t1,2)*Power(t2,3) + 82*t1*Power(t2,4) - 
                  12*Power(t2,5)) - 
               Power(s2,3)*(64*Power(t1,5) + 447*Power(t1,4)*t2 + 
                  688*Power(t1,3)*Power(t2,2) + 
                  59*Power(t1,2)*Power(t2,3) - 359*t1*Power(t2,4) + 
                  58*Power(t2,5)) - 
               s2*t1*(20*Power(t1,6) + 290*Power(t1,5)*t2 + 
                  234*Power(t1,4)*Power(t2,2) + 
                  Power(t1,3)*Power(t2,3) + 
                  194*Power(t1,2)*Power(t2,4) - 103*t1*Power(t2,5) - 
                  25*Power(t2,6)) + 
               2*Power(s2,2)*(30*Power(t1,6) + 262*Power(t1,5)*t2 + 
                  352*Power(t1,4)*Power(t2,2) + 
                  171*Power(t1,3)*Power(t2,3) - 
                  74*Power(t1,2)*Power(t2,4) - 100*t1*Power(t2,5) + 
                  2*Power(t2,6)))) + 
         Power(s,5)*(-(Power(s1,4)*
               (4*Power(s2,3)*Power(t2,2) - 
                 2*Power(s2,2)*
                  (5*Power(t1,3) + 6*Power(t1,2)*t2 - 
                    3*t1*Power(t2,2) + 6*Power(t2,3)) + 
                 2*s2*(20*Power(t1,4) + 15*Power(t1,3)*t2 + 
                    9*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
                    2*Power(t2,4)) + 
                 t1*(-35*Power(t1,4) + 20*Power(t1,3)*t2 + 
                    30*Power(t1,2)*Power(t2,2) - 48*t1*Power(t2,3) + 
                    9*Power(t2,4)))) + 
            Power(s1,3)*(-3*Power(s2,4)*Power(t2,2) + 
               Power(s2,3)*(14*Power(t1,3) + 27*Power(t1,2)*t2 - 
                  18*t1*Power(t2,2) + 32*Power(t2,3)) - 
               Power(s2,2)*(90*Power(t1,4) + 162*Power(t1,3)*t2 + 
                  111*Power(t1,2)*Power(t2,2) - 151*t1*Power(t2,3) + 
                  39*Power(t2,4)) - 
               t1*(70*Power(t1,5) + 30*Power(t1,4)*t2 - 
                  70*Power(t1,3)*Power(t2,2) + 
                  88*Power(t1,2)*Power(t2,3) + 61*t1*Power(t2,4) - 
                  21*Power(t2,5)) + 
               s2*(150*Power(t1,5) + 130*Power(t1,4)*t2 + 
                  61*Power(t1,3)*Power(t2,2) + 
                  160*Power(t1,2)*Power(t2,3) - 126*t1*Power(t2,4) + 
                  6*Power(t2,5))) + 
            Power(s1,2)*(3*Power(s2,4)*
                (Power(t1,3) + 3*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
                  4*Power(t2,3)) - 
               Power(s2,3)*(36*Power(t1,4) + 126*Power(t1,3)*t2 + 
                  143*Power(t1,2)*Power(t2,2) - 151*t1*Power(t2,3) + 
                  52*Power(t2,4)) + 
               Power(s2,2)*(96*Power(t1,5) + 258*Power(t1,4)*t2 + 
                  309*Power(t1,3)*Power(t2,2) + 
                  281*Power(t1,2)*Power(t2,3) - 281*t1*Power(t2,4) + 
                  30*Power(t2,5)) + 
               t1*(21*Power(t1,6) + 84*Power(t1,5)*t2 - 
                  17*Power(t1,4)*Power(t2,2) + 
                  8*Power(t1,3)*Power(t2,3) + 
                  182*Power(t1,2)*Power(t2,4) - 8*t1*Power(t2,5) - 
                  15*Power(t2,6)) - 
               s2*(84*Power(t1,6) + 237*Power(t1,5)*t2 + 
                  130*Power(t1,4)*Power(t2,2) + 
                  319*Power(t1,3)*Power(t2,3) + 
                  144*Power(t1,2)*Power(t2,4) - 141*t1*Power(t2,5) + 
                  2*Power(t2,6))) - 
            s1*(Power(s2,4)*(2*Power(t1,4) + 16*Power(t1,3)*t2 + 
                  35*Power(t1,2)*Power(t2,2) - 34*t1*Power(t2,3) + 
                  10*Power(t2,4)) - 
               Power(s2,3)*(10*Power(t1,5) + 65*Power(t1,4)*t2 + 
                  122*Power(t1,3)*Power(t2,2) + 
                  189*Power(t1,2)*Power(t2,3) - 162*t1*Power(t2,4) + 
                  20*Power(t2,5)) - 
               s2*t1*(6*Power(t1,6) + 89*Power(t1,5)*t2 + 
                  79*Power(t1,4)*Power(t2,2) + 
                  111*Power(t1,3)*Power(t2,3) + 
                  265*Power(t1,2)*Power(t2,4) - 11*t1*Power(t2,5) - 
                  57*Power(t2,6)) + 
               t1*t2*(18*Power(t1,6) + 17*Power(t1,5)*t2 - 
                  3*Power(t1,4)*Power(t2,2) + 
                  43*Power(t1,3)*Power(t2,3) + 
                  82*Power(t1,2)*Power(t2,4) - 22*t1*Power(t2,5) - 
                  3*Power(t2,6)) + 
               Power(s2,2)*(14*Power(t1,6) + 120*Power(t1,5)*t2 + 
                  146*Power(t1,4)*Power(t2,2) + 
                  300*Power(t1,3)*Power(t2,3) + 
                  172*Power(t1,2)*Power(t2,4) - 181*t1*Power(t2,5) + 
                  5*Power(t2,6))) + 
            t1*t2*(Power(s2,4)*
                (Power(t1,3) + Power(t1,2)*t2 + 26*t1*Power(t2,2) - 
                  15*Power(t2,3)) - 
               Power(s2,3)*(6*Power(t1,4) + 8*Power(t1,3)*t2 + 
                  29*Power(t1,2)*Power(t2,2) + 52*t1*Power(t2,3) - 
                  50*Power(t2,4)) + 
               Power(s2,2)*(9*Power(t1,5) + 20*Power(t1,4)*t2 + 
                  3*Power(t1,3)*Power(t2,2) + 
                  40*Power(t1,2)*Power(t2,3) + 20*t1*Power(t2,4) - 
                  35*Power(t2,5)) + 
               t1*t2*(2*Power(t1,5) - 2*Power(t1,4)*t2 - 
                  5*Power(t1,3)*Power(t2,2) + 
                  23*Power(t1,2)*Power(t2,3) + 5*t1*Power(t2,4) - 
                  3*Power(t2,5)) + 
               s2*(-4*Power(t1,6) - 15*Power(t1,5)*t2 + 
                  5*Power(t1,4)*Power(t2,2) + 
                  9*Power(t1,3)*Power(t2,3) - 
                  68*Power(t1,2)*Power(t2,4) + 20*t1*Power(t2,5) + 
                  5*Power(t2,6)))) + 
         Power(s,4)*(Power(s1,5)*
             (6*Power(s2,3)*Power(t2,2) - 
               2*Power(s2,2)*
                (5*Power(t1,3) + 4*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
                  4*Power(t2,3)) + 
               s2*(30*Power(t1,4) + 10*Power(t1,3)*t2 + 
                  7*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               t1*(-21*Power(t1,4) + 15*Power(t1,3)*t2 + 
                  25*Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,4)*(11*Power(s2,4)*Power(t2,2) - 
               Power(s2,3)*(26*Power(t1,3) + 39*Power(t1,2)*t2 - 
                  22*t1*Power(t2,2) + 48*Power(t2,3)) + 
               Power(s2,2)*(120*Power(t1,4) + 158*Power(t1,3)*t2 + 
                  64*Power(t1,2)*Power(t2,2) - 127*t1*Power(t2,3) + 
                  29*Power(t2,4)) + 
               t1*(70*Power(t1,5) + 5*Power(t1,4)*t2 - 
                  90*Power(t1,3)*Power(t2,2) + 
                  47*Power(t1,2)*Power(t2,3) + 34*t1*Power(t2,4) - 
                  6*Power(t2,5)) - 
               s2*(165*Power(t1,5) + 105*Power(t1,4)*t2 - 
                  16*Power(t1,3)*Power(t2,2) + 
                  68*Power(t1,2)*Power(t2,3) - 74*t1*Power(t2,4) + 
                  2*Power(t2,5))) + 
            t1*Power(t2,2)*(-(Power(s2,5)*
                  (Power(t1,2) + 8*t1*t2 - 6*Power(t2,2))) + 
               Power(s2,4)*(5*Power(t1,3) + 12*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) - 35*Power(t2,3)) + 
               Power(t1,2)*t2*
                (-2*Power(t1,4) - 10*Power(t1,3)*t2 + 
                  7*Power(t1,2)*Power(t2,2) + 11*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,3)*(-12*Power(t1,4) + Power(t1,3)*t2 + 
                  18*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
                  40*Power(t2,4)) + 
               Power(s2,2)*(13*Power(t1,5) - 10*Power(t1,4)*t2 - 
                  86*Power(t1,3)*Power(t2,2) + 
                  65*Power(t1,2)*Power(t2,3) - 30*t1*Power(t2,4) - 
                  10*Power(t2,5)) + 
               s2*t1*(-5*Power(t1,5) + 7*Power(t1,4)*t2 + 
                  55*Power(t1,3)*Power(t2,2) - 
                  34*Power(t1,2)*Power(t2,3) - 17*t1*Power(t2,4) + 
                  12*Power(t2,5))) + 
            s1*t2*(Power(s2,5)*
                (4*Power(t1,3) + 18*Power(t1,2)*t2 - 
                  16*t1*Power(t2,2) + 5*Power(t2,3)) - 
               2*Power(s2,4)*
                (11*Power(t1,4) + 46*Power(t1,3)*t2 + 
                  58*Power(t1,2)*Power(t2,2) - 64*t1*Power(t2,3) + 
                  10*Power(t2,4)) - 
               Power(s2,2)*t1*
                (54*Power(t1,5) + 168*Power(t1,4)*t2 + 
                  180*Power(t1,3)*Power(t2,2) + 
                  256*Power(t1,2)*Power(t2,3) - 36*t1*Power(t2,4) - 
                  109*Power(t2,5)) + 
               Power(t1,2)*t2*
                (-10*Power(t1,5) + 14*Power(t1,4)*t2 + 
                  39*Power(t1,3)*Power(t2,2) - 
                  74*Power(t1,2)*Power(t2,3) - 15*t1*Power(t2,4) + 
                  8*Power(t2,5)) + 
               Power(s2,3)*(52*Power(t1,5) + 176*Power(t1,4)*t2 + 
                  276*Power(t1,3)*Power(t2,2) + 
                  123*Power(t1,2)*Power(t2,3) - 214*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               s2*t1*(20*Power(t1,6) + 76*Power(t1,5)*t2 + 
                  10*Power(t1,4)*Power(t2,2) + 
                  27*Power(t1,3)*Power(t2,3) + 
                  209*Power(t1,2)*Power(t2,4) - 83*t1*Power(t2,5) - 
                  13*Power(t2,6))) + 
            Power(s1,3)*(3*Power(s2,5)*Power(t2,2) - 
               Power(s2,4)*(13*Power(t1,3) + 33*Power(t1,2)*t2 - 
                  30*t1*Power(t2,2) + 48*Power(t2,3)) + 
               Power(s2,3)*(88*Power(t1,4) + 258*Power(t1,3)*t2 + 
                  200*Power(t1,2)*Power(t2,2) - 235*t1*Power(t2,3) + 
                  93*Power(t2,4)) - 
               3*Power(s2,2)*
                (60*Power(t1,5) + 153*Power(t1,4)*t2 + 
                  146*Power(t1,3)*Power(t2,2) + 
                  44*Power(t1,2)*Power(t2,3) - 104*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               s2*(140*Power(t1,6) + 345*Power(t1,5)*t2 + 
                  147*Power(t1,4)*Power(t2,2) + 
                  183*Power(t1,3)*Power(t2,3) + 
                  54*Power(t1,2)*Power(t2,4) - 107*t1*Power(t2,5) + 
                  Power(t2,6)) + 
               t1*(-35*Power(t1,6) - 105*Power(t1,5)*t2 + 
                  60*Power(t1,4)*Power(t2,2) + 
                  68*Power(t1,3)*Power(t2,3) - 
                  180*Power(t1,2)*Power(t2,4) + 8*t1*Power(t2,5) + 
                  6*Power(t2,6))) + 
            Power(s1,2)*(-(Power(s2,5)*
                  (Power(t1,3) + 5*Power(t1,2)*t2 - 7*t1*Power(t2,2) + 
                    8*Power(t2,3))) + 
               Power(s2,4)*(12*Power(t1,4) + 85*Power(t1,3)*t2 + 
                  136*Power(t1,2)*Power(t2,2) - 137*t1*Power(t2,3) + 
                  58*Power(t2,4)) - 
               Power(s2,3)*(36*Power(t1,5) + 261*Power(t1,4)*t2 + 
                  486*Power(t1,3)*Power(t2,2) + 
                  252*Power(t1,2)*Power(t2,3) - 376*t1*Power(t2,4) + 
                  60*Power(t2,5)) - 
               s2*t1*(15*Power(t1,6) + 220*Power(t1,5)*t2 + 
                  206*Power(t1,4)*Power(t2,2) + 
                  171*Power(t1,3)*Power(t2,3) + 
                  384*Power(t1,2)*Power(t2,4) - 88*t1*Power(t2,5) - 
                  66*Power(t2,6)) + 
               t1*t2*(45*Power(t1,6) + 25*Power(t1,5)*t2 - 
                  60*Power(t1,4)*Power(t2,2) + 
                  51*Power(t1,3)*Power(t2,3) + 
                  120*Power(t1,2)*Power(t2,4) - 29*t1*Power(t2,5) - 
                  2*Power(t2,6)) + 
               Power(s2,2)*(40*Power(t1,6) + 356*Power(t1,5)*t2 + 
                  526*Power(t1,4)*Power(t2,2) + 
                  560*Power(t1,3)*Power(t2,3) + 
                  83*Power(t1,2)*Power(t2,4) - 292*t1*Power(t2,5) + 
                  9*Power(t2,6)))))*lg2(-s1))/
     ((s - s1)*s1*s2*Power(t1,3)*Power(s - s2 + t1,3)*Power(t2,3)*
       Power(s - s1 + t2,3)) - 
    (8*(Power(s,9)*t2*(-Power(t1,4) + 4*Power(t1,3)*t2 + Power(t2,4)) - 
         Power(s2,2)*Power(t2,5)*
          (2*Power(t1,5)*t2*(t1 + t2) - 
            s2*Power(t1,4)*t2*(6*t1 + 5*t2) + 
            Power(s2,4)*t1*(8*Power(t1,2) + 3*t1*t2 - 3*Power(t2,2)) - 
            2*Power(s2,3)*Power(t1,2)*
             (5*Power(t1,2) + 3*t1*t2 - Power(t2,2)) + 
            Power(s2,5)*(-2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
            2*Power(s2,2)*Power(t1,3)*
             (2*Power(t1,2) + 4*t1*t2 + Power(t2,2))) + 
         Power(s1,6)*s2*Power(s2 - t1,2)*t1*
          (Power(s2,3)*t1 + Power(t1,2)*(t1 - t2)*t2 - 
            Power(s2,2)*(2*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
            s2*t1*(Power(t1,2) - 3*t1*t2 + 4*Power(t2,2))) - 
         s1*s2*Power(t2,4)*(Power(s2,2)*Power(t1,4)*t2*(11*t1 + 18*t2) + 
            s2*Power(t1,4)*t2*(Power(t1,2) - 13*t1*t2 - 5*Power(t2,2)) + 
            Power(s2,6)*(3*Power(t1,2) + 3*t1*t2 - 4*Power(t2,2)) + 
            2*Power(t1,5)*t2*(-Power(t1,2) + t1*t2 + Power(t2,2)) + 
            Power(s2,4)*t1*(23*Power(t1,3) + 21*Power(t1,2)*t2 - 
               16*t1*Power(t2,2) - 4*Power(t2,3)) + 
            Power(s2,5)*(-16*Power(t1,3) - 10*Power(t1,2)*t2 + 
               14*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s2,3)*Power(t1,2)*
             (-10*Power(t1,3) - 24*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
               5*Power(t2,3))) + 
         Power(s1,5)*(s2 - t1)*
          (Power(s2,6)*Power(t1,2) - 2*Power(t1,5)*Power(t2,3) + 
            s2*Power(t1,4)*t2*(Power(t1,2) + t1*t2 + Power(t2,2)) + 
            Power(s2,2)*Power(t1,3)*t2*
             (-4*Power(t1,2) - 6*t1*t2 + 17*Power(t2,2)) + 
            Power(s2,3)*Power(t1,2)*
             (-Power(t1,3) + 6*Power(t1,2)*t2 + t1*Power(t2,2) - 
               22*Power(t2,3)) - 
            Power(s2,5)*(3*Power(t1,3) - Power(t1,2)*t2 + 
               2*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s2,4)*t1*(3*Power(t1,3) - 4*Power(t1,2)*t2 + 
               6*t1*Power(t2,2) + 8*Power(t2,3))) + 
         Power(s1,2)*Power(t2,3)*
          (3*Power(s2,7)*(t1 - 2*t2)*t2 - 2*Power(t1,6)*Power(t2,3) + 
            s2*Power(t1,5)*t2*
             (-5*Power(t1,2) + 6*t1*t2 + 13*Power(t2,2)) + 
            Power(s2,3)*Power(t1,3)*
             (14*Power(t1,3) + 5*Power(t1,2)*t2 + 14*t1*Power(t2,2) - 
               13*Power(t2,3)) + 
            2*Power(s2,5)*t1*
             (13*Power(t1,3) + 10*Power(t1,2)*t2 - 22*t1*Power(t2,2) - 
               9*Power(t2,3)) + 
            Power(s2,6)*(-10*Power(t1,3) - 8*Power(t1,2)*t2 + 
               26*t1*Power(t2,2) + 4*Power(t2,3)) - 
            Power(s2,2)*Power(t1,4)*
             (4*Power(t1,3) - 10*Power(t1,2)*t2 + 23*t1*Power(t2,2) + 
               16*Power(t2,3)) + 
            Power(s2,4)*Power(t1,2)*
             (-26*Power(t1,3) - 25*Power(t1,2)*t2 + 30*t1*Power(t2,2) + 
               29*Power(t2,3))) + 
         Power(s1,3)*Power(t2,2)*
          (6*Power(t1,6)*Power(t2,3) + 
            s2*Power(t1,5)*t2*
             (2*Power(t1,2) - 6*t1*t2 - 25*Power(t2,2)) + 
            Power(s2,7)*(2*Power(t1,2) - t1*t2 + 4*Power(t2,2)) + 
            Power(s2,4)*Power(t1,2)*
             (29*Power(t1,3) + 30*Power(t1,2)*t2 - 66*t1*Power(t2,2) - 
               61*Power(t2,3)) + 
            Power(s2,6)*(Power(t1,3) + Power(t1,2)*t2 - 
               24*t1*Power(t2,2) - 6*Power(t2,3)) + 
            Power(s2,2)*Power(t1,4)*
             (6*Power(t1,3) - 2*Power(t1,2)*t2 + 13*t1*Power(t2,2) + 
               13*Power(t2,3)) + 
            Power(s2,5)*t1*(-17*Power(t1,3) - 14*Power(t1,2)*t2 + 
               60*t1*Power(t2,2) + 31*Power(t2,3)) + 
            Power(s2,3)*Power(t1,3)*
             (-21*Power(t1,3) - 16*Power(t1,2)*t2 + 18*t1*Power(t2,2) + 
               45*Power(t2,3))) - 
         Power(s1,4)*t2*(6*Power(t1,6)*Power(t2,3) + 
            Power(s2,7)*(2*Power(t1,2) + Power(t2,2)) - 
            2*s2*Power(t1,5)*t2*(Power(t1,2) + t1*t2 + 9*Power(t2,2)) + 
            Power(s2,4)*Power(t1,2)*
             (7*Power(t1,3) + 36*Power(t1,2)*t2 - 52*t1*Power(t2,2) - 
               61*Power(t2,3)) + 
            Power(s2,2)*Power(t1,4)*
             (2*Power(t1,3) + 10*Power(t1,2)*t2 + 3*t1*Power(t2,2) - 
               8*Power(t2,3)) - 
            Power(s2,6)*(5*Power(t1,3) - 2*Power(t1,2)*t2 + 
               11*t1*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s2,5)*t1*(Power(t1,3) - 18*Power(t1,2)*t2 + 
               40*t1*Power(t2,2) + 25*Power(t2,3)) + 
            Power(s2,3)*Power(t1,3)*
             (-7*Power(t1,3) - 28*Power(t1,2)*t2 + 21*t1*Power(t2,2) + 
               61*Power(t2,3))) + 
         Power(s,8)*(t2*(-3*Power(t1,5) + 7*Power(t1,4)*t2 + 
               11*Power(t1,3)*Power(t2,2) + 3*t1*Power(t2,4) + 
               2*Power(t2,5) + 
               s2*(6*Power(t1,4) - 21*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 
                  7*Power(t2,4))) + 
            s1*(s2*(Power(t1,4) - 4*Power(t1,3)*t2 + 
                  5*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) + 
                  2*Power(t2,4)) - 
               t2*(-6*Power(t1,4) + 20*Power(t1,3)*t2 - 
                  2*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
                  3*Power(t2,4)))) - 
         s*(Power(s1,6)*(s2 - t1)*t1*
             (3*Power(s2,4)*t1 - Power(t1,4)*t2 + 
               Power(s2,3)*(-7*Power(t1,2) + 6*t1*t2 - 3*Power(t2,2)) - 
               s2*Power(t1,2)*(Power(t1,2) - 6*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*t1*
                (5*Power(t1,2) - 11*t1*t2 + 13*Power(t2,2))) + 
            s2*Power(t2,4)*(2*Power(t1,5)*t2*
                (Power(t1,2) - 2*Power(t2,2)) - 
               2*Power(s2,6)*(2*Power(t1,2) + t1*t2 - Power(t2,2)) + 
               s2*Power(t1,4)*t2*
                (-Power(t1,2) + 13*t1*t2 + 15*Power(t2,2)) + 
               Power(s2,4)*t1*t2*
                (-51*Power(t1,2) - 11*t1*t2 + 18*Power(t2,2)) + 
               Power(s2,2)*Power(t1,3)*
                (8*Power(t1,3) - 19*Power(t1,2)*t2 - 
                  34*t1*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s2,5)*(10*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  7*Power(t2,3)) - 
               2*Power(s2,3)*Power(t1,2)*
                (7*Power(t1,3) - 27*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s1,5)*(6*Power(s2,6)*Power(t1,2) - 
               Power(t1,5)*t2*(Power(t1,2) + 7*Power(t2,2)) - 
               Power(s2,5)*(24*Power(t1,3) - 10*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,4)*t1*
                (36*Power(t1,3) - 43*Power(t1,2)*t2 + 
                  48*t1*Power(t2,2) + 31*Power(t2,3)) + 
               Power(s2,2)*Power(t1,3)*
                (6*Power(t1,3) - 46*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) + 73*Power(t2,3)) - 
               Power(s2,3)*Power(t1,2)*
                (24*Power(t1,3) - 67*Power(t1,2)*t2 + 
                  48*t1*Power(t2,2) + 84*Power(t2,3)) + 
               s2*(13*Power(t1,6)*t2 - 6*Power(t1,4)*Power(t2,3))) + 
            s1*Power(t2,3)*(-2*Power(t1,5)*Power(t2,4) + 
               Power(s2,7)*(3*Power(t1,2) + 4*t1*t2 - 6*Power(t2,2)) + 
               s2*Power(t1,4)*t2*
                (-10*Power(t1,3) - Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,6)*(-12*Power(t1,3) - 34*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + 28*Power(t2,3)) - 
               Power(s2,2)*Power(t1,4)*
                (8*Power(t1,3) - 31*Power(t1,2)*t2 + 
                  32*t1*Power(t2,2) + 71*Power(t2,3)) + 
               Power(s2,3)*Power(t1,2)*
                (12*Power(t1,4) - 3*Power(t1,3)*t2 + 
                  76*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 
                  20*Power(t2,4)) + 
               Power(s2,5)*(11*Power(t1,4) + 108*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 85*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,4)*t1*
                (-6*Power(t1,4) - 96*Power(t1,3)*t2 - 
                  38*Power(t1,2)*Power(t2,2) + 79*t1*Power(t2,3) + 
                  20*Power(t2,4))) + 
            Power(s1,2)*Power(t2,2)*
             (Power(t1,5)*Power(t2,3)*(6*t1 + 13*t2) + 
               Power(s2,7)*(Power(t1,2) - 2*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,6)*(7*Power(t1,3) + 13*Power(t1,2)*t2 - 
                  24*t1*Power(t2,2) - 42*Power(t2,3)) + 
               s2*Power(t1,4)*t2*
                (2*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  66*t1*Power(t2,2) - 39*Power(t2,3)) + 
               Power(s2,4)*t1*
                (63*Power(t1,4) + 107*Power(t1,3)*t2 - 
                  59*Power(t1,2)*Power(t2,2) - 240*t1*Power(t2,3) - 
                  81*Power(t2,4)) - 
               2*Power(s2,3)*Power(t1,2)*
                (27*Power(t1,4) + 21*Power(t1,3)*t2 + 
                  4*Power(t1,2)*Power(t2,2) - 63*t1*Power(t2,3) - 
                  50*Power(t2,4)) + 
               Power(s2,2)*Power(t1,3)*
                (18*Power(t1,4) + Power(t1,2)*Power(t2,2) + 
                  86*t1*Power(t2,3) - 25*Power(t2,4)) + 
               Power(s2,5)*(-35*Power(t1,4) - 78*Power(t1,3)*t2 + 
                  70*Power(t1,2)*Power(t2,2) + 164*t1*Power(t2,3) + 
                  22*Power(t2,4))) + 
            Power(s1,3)*t2*(-2*Power(s2,7)*
                (Power(t1,2) + Power(t2,2)) - 
               Power(t1,5)*Power(t2,2)*
                (2*Power(t1,2) + 11*t1*t2 + 27*Power(t2,2)) + 
               Power(s2,6)*(2*Power(t1,3) + Power(t1,2)*t2 + 
                  21*t1*Power(t2,2) + 28*Power(t2,3)) + 
               s2*Power(t1,4)*t2*
                (14*Power(t1,3) + Power(t1,2)*t2 + 56*t1*Power(t2,2) + 
                  52*Power(t2,3)) + 
               Power(s2,3)*Power(t1,2)*
                (27*Power(t1,4) + 140*Power(t1,3)*t2 - 
                  15*Power(t1,2)*Power(t2,2) - 300*t1*Power(t2,3) - 
                  192*Power(t2,4)) - 
               Power(s2,2)*Power(t1,3)*
                (8*Power(t1,4) + 68*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 22*t1*Power(t2,3) - 
                  87*Power(t2,4)) + 
               Power(s2,5)*(13*Power(t1,4) + 53*Power(t1,3)*t2 - 
                  92*Power(t1,2)*Power(t2,2) - 156*t1*Power(t2,3) - 
                  30*Power(t2,4)) + 
               Power(s2,4)*t1*
                (-32*Power(t1,4) - 140*Power(t1,3)*t2 + 
                  91*Power(t1,2)*Power(t2,2) + 340*t1*Power(t2,3) + 
                  127*Power(t2,4))) + 
            Power(s1,4)*(2*Power(s2,7)*Power(t1,2) + 
               Power(t1,5)*Power(t2,2)*
                (3*Power(t1,2) + 4*t1*t2 + 23*Power(t2,2)) + 
               Power(s2,2)*Power(t1,3)*t2*
                (36*Power(t1,3) + 64*Power(t1,2)*t2 - 
                  80*t1*Power(t2,2) - 117*Power(t2,3)) - 
               Power(s2,6)*(10*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + 7*Power(t2,3)) - 
               s2*Power(t1,4)*t2*
                (7*Power(t1,3) + 20*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
                  22*Power(t2,3)) + 
               Power(s2,4)*t1*
                (-14*Power(t1,4) + 55*Power(t1,3)*t2 + 
                  32*Power(t1,2)*Power(t2,2) - 223*t1*Power(t2,3) - 
                  94*Power(t2,4)) + 
               Power(s2,5)*(18*Power(t1,4) - 12*Power(t1,3)*t2 + 
                  19*Power(t1,2)*Power(t2,2) + 70*t1*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s2,3)*Power(t1,2)*
                (4*Power(t1,4) - 69*Power(t1,3)*t2 - 
                  92*Power(t1,2)*Power(t2,2) + 246*t1*Power(t2,3) + 
                  180*Power(t2,4)))) + 
         Power(s,7)*(Power(s1,2)*
             (Power(s2,2)*(2*Power(t1,3) - Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) + Power(t2,3)) + 
               t2*(-15*Power(t1,4) + 41*Power(t1,3)*t2 - 
                  5*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  3*Power(t2,4)) - 
               s2*(5*Power(t1,4) - 19*Power(t1,3)*t2 + 
                  21*Power(t1,2)*Power(t2,2) + 16*t1*Power(t2,3) + 
                  6*Power(t2,4))) + 
            t2*(-3*Power(t1,6) + 2*Power(t1,5)*t2 + 
               21*Power(t1,4)*Power(t2,2) + 
               11*Power(t1,3)*Power(t2,3) + 2*Power(t1,2)*Power(t2,4) + 
               6*t1*Power(t2,5) + Power(t2,6) + 
               Power(s2,2)*(-14*Power(t1,4) + 48*Power(t1,3)*t2 - 
                  7*Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) + 
                  21*Power(t2,4)) + 
               s2*(15*Power(t1,5) - 31*Power(t1,4)*t2 - 
                  50*Power(t1,3)*Power(t2,2) + 
                  5*Power(t1,2)*Power(t2,3) - 16*t1*Power(t2,4) - 
                  14*Power(t2,5))) + 
            s1*(-(Power(s2,2)*
                  (4*Power(t1,4) - 20*Power(t1,3)*t2 + 
                    19*Power(t1,2)*Power(t2,2) + 19*t1*Power(t2,3) + 
                    12*Power(t2,4))) - 
               t2*(-17*Power(t1,5) + 32*Power(t1,4)*t2 + 
                  36*Power(t1,3)*Power(t2,2) - 
                  2*Power(t1,2)*Power(t2,3) + 12*t1*Power(t2,4) + 
                  4*Power(t2,5)) + 
               s2*(2*Power(t1,5) - 41*Power(t1,4)*t2 + 
                  98*Power(t1,3)*Power(t2,2) + 
                  14*Power(t1,2)*Power(t2,3) + 19*t1*Power(t2,4) + 
                  24*Power(t2,5)))) + 
         Power(s,2)*(Power(s1,6)*t1*
             (3*Power(s2,4)*t1 - 2*Power(t1,4)*t2 + 
               s2*Power(t1,2)*
                (-2*Power(t1,2) + 9*t1*t2 - 9*Power(t2,2)) + 
               Power(s2,3)*(-8*Power(t1,2) + 6*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*t1*
                (7*Power(t1,2) - 13*t1*t2 + 14*Power(t2,2))) + 
            Power(s1,5)*(12*Power(s2,5)*Power(t1,2) + 
               2*Power(t1,4)*t2*
                (4*Power(t1,2) - t1*t2 + 4*Power(t2,2)) - 
               Power(s2,4)*(44*Power(t1,3) - 24*Power(t1,2)*t2 + 
                  21*t1*Power(t2,2) + 6*Power(t2,3)) + 
               3*Power(s2,3)*t1*
                (19*Power(t1,3) - 29*Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) + 13*Power(t2,3)) + 
               s2*Power(t1,3)*
                (5*Power(t1,3) - 49*Power(t1,2)*t2 + 
                  19*t1*Power(t2,2) + 37*Power(t2,3)) - 
               Power(s2,2)*Power(t1,2)*
                (30*Power(t1,3) - 104*Power(t1,2)*t2 + 
                  80*t1*Power(t2,2) + 79*Power(t2,3))) - 
            Power(t2,3)*(2*Power(t1,5)*Power(t2,3)*(-t1 + t2) + 
               Power(s2,7)*(-2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
               s2*Power(t1,4)*t2*
                (5*Power(t1,3) + 7*Power(t1,2)*t2 - 8*t1*Power(t2,2) - 
                  15*Power(t2,3)) + 
               Power(s2,6)*(-2*Power(t1,3) + 25*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) - 14*Power(t2,3)) + 
               Power(s2,4)*t1*
                (-22*Power(t1,4) - 4*Power(t1,3)*t2 + 
                  120*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) - 
                  45*Power(t2,4)) + 
               Power(s2,2)*Power(t1,3)*
                (4*Power(t1,4) - 29*Power(t1,3)*t2 + 
                  19*Power(t1,2)*Power(t2,2) + 54*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,3)*Power(t1,2)*
                (2*Power(t1,4) + 56*Power(t1,3)*t2 - 
                  107*Power(t1,2)*Power(t2,2) - 44*t1*Power(t2,3) + 
                  20*Power(t2,4)) + 
               Power(s2,5)*(20*Power(t1,4) - 52*Power(t1,3)*t2 - 
                  53*Power(t1,2)*Power(t2,2) + 21*t1*Power(t2,3) + 
                  21*Power(t2,4))) + 
            Power(s1,4)*(9*Power(s2,6)*Power(t1,2) + 
               s2*Power(t1,3)*t2*
                (54*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  26*t1*Power(t2,2) - 58*Power(t2,3)) - 
               Power(s2,5)*(44*Power(t1,3) - 12*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) + 18*Power(t2,3)) - 
               Power(t1,4)*t2*
                (5*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  19*t1*Power(t2,2) + 29*Power(t2,3)) + 
               Power(s2,4)*(75*Power(t1,4) - 108*Power(t1,3)*t2 + 
                  110*Power(t1,2)*Power(t2,2) + 160*t1*Power(t2,3) + 
                  32*Power(t2,4)) + 
               2*Power(s2,2)*Power(t1,2)*
                (7*Power(t1,4) - 84*Power(t1,3)*t2 - 
                  12*Power(t1,2)*Power(t2,2) + 166*t1*Power(t2,3) + 
                  91*Power(t2,4)) - 
               Power(s2,3)*t1*
                (54*Power(t1,4) - 215*Power(t1,3)*t2 + 
                  63*Power(t1,2)*Power(t2,2) + 416*t1*Power(t2,3) + 
                  133*Power(t2,4))) + 
            s1*Power(t2,2)*(Power(s2,7)*
                (Power(t1,2) - t1*t2 + 2*Power(t2,2)) + 
               Power(t1,4)*Power(t2,3)*
                (-6*Power(t1,2) + 14*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,6)*(-2*Power(t1,3) + 27*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) - 39*Power(t2,3)) - 
               2*s2*Power(t1,4)*t2*
                (Power(t1,3) - 23*Power(t1,2)*t2 + 17*t1*Power(t2,2) + 
                  44*Power(t2,3)) + 
               Power(s2,5)*(-8*Power(t1,4) - 62*Power(t1,3)*t2 - 
                  65*Power(t1,2)*Power(t2,2) + 102*t1*Power(t2,3) + 
                  84*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (18*Power(t1,5) - 10*Power(t1,4)*t2 - 
                  81*Power(t1,3)*Power(t2,2) + 
                  135*Power(t1,2)*Power(t2,3) + 15*t1*Power(t2,4) - 
                  30*Power(t2,5)) + 
               Power(s2,4)*(36*Power(t1,5) - 6*Power(t1,4)*t2 + 
                  168*Power(t1,3)*Power(t2,2) - 
                  106*Power(t1,2)*Power(t2,3) - 215*t1*Power(t2,4) - 
                  15*Power(t2,5)) + 
               Power(s2,3)*t1*
                (-45*Power(t1,5) + 54*Power(t1,4)*t2 - 
                  78*Power(t1,3)*Power(t2,2) + 
                  7*Power(t1,2)*Power(t2,3) + 156*t1*Power(t2,4) + 
                  40*Power(t2,5))) - 
            Power(s1,2)*t2*(Power(s2,7)*Power(t2,2) + 
               Power(s2,6)*(7*Power(t1,3) - 9*t1*Power(t2,2) - 
                  36*Power(t2,3)) + 
               Power(t1,4)*Power(t2,2)*
                (6*Power(t1,3) - 7*Power(t1,2)*t2 + 
                  47*t1*Power(t2,2) + 23*Power(t2,3)) + 
               s2*Power(t1,3)*t2*
                (-30*Power(t1,4) + 19*Power(t1,3)*t2 - 
                  10*Power(t1,2)*Power(t2,2) - 161*t1*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(s2,5)*(-29*Power(t1,4) - 50*Power(t1,3)*t2 + 
                  27*Power(t1,2)*Power(t2,2) + 177*t1*Power(t2,3) + 
                  123*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (12*Power(t1,5) + 135*Power(t1,4)*t2 - 
                  83*Power(t1,3)*Power(t2,2) - 
                  19*Power(t1,2)*Power(t2,3) - 152*t1*Power(t2,4) - 
                  126*Power(t2,5)) + 
               Power(s2,4)*(49*Power(t1,5) + 152*Power(t1,4)*t2 + 
                  31*Power(t1,3)*Power(t2,2) - 
                  401*Power(t1,2)*Power(t2,3) - 411*t1*Power(t2,4) - 
                  50*Power(t2,5)) + 
               Power(s2,3)*t1*
                (-39*Power(t1,5) - 207*Power(t1,4)*t2 + 
                  13*Power(t1,3)*Power(t2,2) + 
                  352*Power(t1,2)*Power(t2,3) + 476*t1*Power(t2,4) + 
                  144*Power(t2,5))) + 
            Power(s1,3)*(Power(s2,7)*Power(t1,2) - 
               Power(s2,6)*(8*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + 11*Power(t2,3)) + 
               Power(t1,4)*Power(t2,2)*
                (12*Power(t1,3) - 5*Power(t1,2)*t2 + 
                  58*t1*Power(t2,2) + 39*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (93*Power(t1,4) + 179*Power(t1,3)*t2 - 
                  172*Power(t1,2)*Power(t2,2) - 394*t1*Power(t2,3) - 
                  213*Power(t2,4)) + 
               s2*Power(t1,3)*t2*
                (-18*Power(t1,4) - 76*Power(t1,3)*t2 + 
                  42*Power(t1,2)*Power(t2,2) - 90*t1*Power(t2,3) + 
                  41*Power(t2,4)) + 
               Power(s2,5)*(19*Power(t1,4) - 37*Power(t1,3)*t2 + 
                  21*Power(t1,2)*Power(t2,2) + 123*t1*Power(t2,3) + 
                  78*Power(t2,4)) - 
               Power(s2,4)*(18*Power(t1,5) - 138*Power(t1,4)*t2 - 
                  100*Power(t1,3)*Power(t2,2) + 
                  420*Power(t1,2)*Power(t2,3) + 380*t1*Power(t2,4) + 
                  61*Power(t2,5)) + 
               Power(s2,3)*t1*
                (6*Power(t1,5) - 173*Power(t1,4)*t2 - 
                  226*Power(t1,3)*Power(t2,2) + 
                  450*Power(t1,2)*Power(t2,3) + 660*t1*Power(t2,4) + 
                  201*Power(t2,5)))) + 
         Power(s,6)*(Power(s1,3)*
             (Power(s2,3)*Power(t1,2) - 
               Power(s2,2)*(8*Power(t1,3) - 5*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) + 3*Power(t2,3)) - 
               t2*(-20*Power(t1,4) + 44*Power(t1,3)*t2 - 
                  3*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(10*Power(t1,4) - 36*Power(t1,3)*t2 + 
                  37*Power(t1,2)*Power(t2,2) + 24*t1*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,2)*(-(Power(s2,3)*
                  (8*Power(t1,3) - 4*Power(t1,2)*t2 + 
                    8*t1*Power(t2,2) + 5*Power(t2,3))) + 
               Power(s2,2)*(23*Power(t1,4) - 86*Power(t1,3)*t2 + 
                  80*Power(t1,2)*Power(t2,2) + 81*t1*Power(t2,3) + 
                  36*Power(t2,4)) + 
               t2*(-40*Power(t1,5) + 58*Power(t1,4)*t2 + 
                  45*Power(t1,3)*Power(t2,2) + 
                  Power(t1,2)*Power(t2,3) + 15*t1*Power(t2,4) + 
                  2*Power(t2,5)) - 
               s2*(10*Power(t1,5) - 113*Power(t1,4)*t2 + 
                  186*Power(t1,3)*Power(t2,2) + 
                  71*Power(t1,2)*Power(t2,3) + 63*t1*Power(t2,4) + 
                  27*Power(t2,5))) + 
            t2*(Power(s2,3)*(16*Power(t1,4) - 62*Power(t1,3)*t2 + 
                  20*Power(t1,2)*Power(t2,2) + 15*t1*Power(t2,3) - 
                  35*Power(t2,4)) + 
               Power(s2,2)*(-27*Power(t1,5) + 59*Power(t1,4)*t2 + 
                  92*Power(t1,3)*Power(t2,2) - 
                  29*Power(t1,2)*Power(t2,3) + 33*t1*Power(t2,4) + 
                  42*Power(t2,5)) + 
               s2*(12*Power(t1,6) - 96*Power(t1,4)*Power(t2,2) - 
                  32*Power(t1,3)*Power(t2,3) - 
                  3*Power(t1,2)*Power(t2,4) - 35*t1*Power(t2,5) - 
                  7*Power(t2,6)) + 
               t1*(-Power(t1,6) - 5*Power(t1,5)*t2 + 
                  22*Power(t1,4)*Power(t2,2) + 
                  13*Power(t1,3)*Power(t2,3) + 
                  4*Power(t1,2)*Power(t2,4) + 4*t1*Power(t2,5) + 
                  3*Power(t2,6))) + 
            s1*(Power(s2,3)*(6*Power(t1,4) - 40*Power(t1,3)*t2 + 
                  27*Power(t1,2)*Power(t2,2) + 35*t1*Power(t2,3) + 
                  30*Power(t2,4)) - 
               Power(s2,2)*(6*Power(t1,5) - 104*Power(t1,4)*t2 + 
                  182*Power(t1,3)*Power(t2,2) + 
                  61*Power(t1,2)*Power(t2,3) + 76*t1*Power(t2,4) + 
                  81*Power(t2,5)) - 
               t2*(-16*Power(t1,6) + 10*Power(t1,5)*t2 + 
                  58*Power(t1,4)*Power(t2,2) + 
                  17*Power(t1,3)*Power(t2,3) + 
                  10*Power(t1,2)*Power(t2,4) + 15*t1*Power(t2,5) + 
                  Power(t2,6)) + 
               s2*(Power(t1,6) - 81*Power(t1,5)*t2 + 
                  118*Power(t1,4)*Power(t2,2) + 
                  187*Power(t1,3)*Power(t2,3) + 
                  33*Power(t1,2)*Power(t2,4) + 84*t1*Power(t2,5) + 
                  28*Power(t2,6)))) - 
         Power(s,3)*(Power(s1,6)*t1*
             (Power(s2,3)*t1 + Power(t1,2)*(t1 - t2)*t2 - 
               Power(s2,2)*(2*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               s2*t1*(Power(t1,2) - 3*t1*t2 + 4*Power(t2,2))) + 
            Power(s1,5)*(10*Power(s2,4)*Power(t1,2) + 
               Power(t1,3)*t2*
                (-13*Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)) - 
               Power(s2,3)*(32*Power(t1,3) - 22*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*t1*
                (32*Power(t1,3) - 65*Power(t1,2)*t2 + 
                  68*t1*Power(t2,2) + 21*Power(t2,3)) - 
               s2*Power(t1,2)*
                (10*Power(t1,3) - 53*Power(t1,2)*t2 + 
                  45*t1*Power(t2,2) + 26*Power(t2,3))) + 
            Power(s1,4)*(15*Power(s2,5)*Power(t1,2) + 
               Power(t1,3)*t2*
                (25*Power(t1,3) - 10*Power(t1,2)*t2 + 
                  29*t1*Power(t2,2) - 2*Power(t2,3)) - 
               Power(s2,4)*(70*Power(t1,3) - 34*Power(t1,2)*t2 + 
                  45*t1*Power(t2,2) + 22*Power(t2,3)) + 
               Power(s2,3)*(105*Power(t1,4) - 194*Power(t1,3)*t2 + 
                  190*Power(t1,2)*Power(t2,2) + 170*t1*Power(t2,3) + 
                  28*Power(t2,4)) + 
               s2*Power(t1,2)*
                (10*Power(t1,4) - 141*Power(t1,3)*t2 + 
                  52*Power(t1,2)*Power(t2,2) + 150*t1*Power(t2,3) + 
                  68*Power(t2,4)) - 
               Power(s2,2)*t1*
                (60*Power(t1,4) - 272*Power(t1,3)*t2 + 
                  167*Power(t1,2)*Power(t2,2) + 324*t1*Power(t2,3) + 
                  85*Power(t2,4))) + 
            Power(t2,2)*(Power(t1,4)*Power(t2,3)*
                (-6*Power(t1,2) + t1*t2 + 5*Power(t2,2)) + 
               Power(s2,6)*(-4*Power(t1,3) + 11*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) - 7*Power(t2,3)) + 
               s2*Power(t1,3)*t2*
                (-2*Power(t1,4) + 32*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) - 38*t1*Power(t2,3) - 
                  8*Power(t2,4)) + 
               Power(s2,5)*(8*Power(t1,4) + 14*Power(t1,3)*t2 - 
                  65*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
                  42*Power(t2,4)) + 
               Power(s2,4)*(2*Power(t1,5) - 93*Power(t1,4)*t2 + 
                  97*Power(t1,3)*Power(t2,2) + 
                  80*Power(t1,2)*Power(t2,3) - 70*t1*Power(t2,4) - 
                  35*Power(t2,5)) + 
               Power(s2,2)*Power(t1,2)*
                (6*Power(t1,5) - 12*Power(t1,4)*t2 - 
                  93*Power(t1,3)*Power(t2,2) + 
                  97*Power(t1,2)*Power(t2,3) + 36*t1*Power(t2,4) - 
                  20*Power(t2,5)) + 
               Power(s2,3)*t1*
                (-12*Power(t1,5) + 82*Power(t1,4)*t2 + 
                  25*Power(t1,3)*Power(t2,2) - 
                  130*Power(t1,2)*Power(t2,3) + 10*t1*Power(t2,4) + 
                  60*Power(t2,5))) + 
            Power(s1,3)*(4*Power(s2,6)*Power(t1,2) - 
               Power(s2,5)*(32*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  25*t1*Power(t2,2) + 24*Power(t2,3)) + 
               s2*Power(t1,2)*t2*
                (106*Power(t1,4) + 41*Power(t1,3)*t2 - 
                  107*Power(t1,2)*Power(t2,2) - 159*t1*Power(t2,3) - 
                  94*Power(t2,4)) - 
               Power(t1,3)*t2*
                (10*Power(t1,4) + 20*Power(t1,3)*t2 - 
                  7*Power(t1,2)*Power(t2,2) + 93*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,4)*(70*Power(t1,4) - 159*Power(t1,3)*t2 + 
                  116*Power(t1,2)*Power(t2,2) + 257*t1*Power(t2,3) + 
                  112*Power(t2,4)) - 
               Power(s2,3)*(58*Power(t1,5) - 359*Power(t1,4)*t2 + 
                  59*Power(t1,3)*Power(t2,2) + 
                  688*Power(t1,2)*Power(t2,3) + 447*t1*Power(t2,4) + 
                  64*Power(t2,5)) + 
               Power(s2,2)*t1*
                (16*Power(t1,5) - 305*Power(t1,4)*t2 - 
                  31*Power(t1,3)*Power(t2,2) + 
                  592*Power(t1,2)*Power(t2,3) + 548*t1*Power(t2,4) + 
                  151*Power(t2,5))) - 
            s1*t2*(Power(s2,6)*
                (4*Power(t1,3) - 3*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,4)*Power(t2,2)*
                (6*Power(t1,3) - 23*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 35*Power(t2,3)) + 
               Power(s2,5)*(-17*Power(t1,4) + 14*Power(t1,3)*t2 - 
                  56*Power(t1,2)*Power(t2,2) + 31*t1*Power(t2,3) + 
                  108*Power(t2,4)) + 
               Power(s2,4)*(30*Power(t1,5) + 12*Power(t1,4)*t2 + 
                  35*Power(t1,3)*Power(t2,2) - 
                  18*Power(t1,2)*Power(t2,3) - 270*t1*Power(t2,4) - 
                  140*Power(t2,5)) + 
               s2*Power(t1,2)*t2*
                (-26*Power(t1,5) + 33*Power(t1,4)*t2 + 
                  102*Power(t1,3)*Power(t2,2) - 
                  138*Power(t1,2)*Power(t2,3) - 19*t1*Power(t2,4) + 
                  20*Power(t2,5)) + 
               Power(s2,2)*t1*
                (8*Power(t1,6) + 106*Power(t1,5)*t2 - 
                  201*Power(t1,4)*Power(t2,2) - 
                  31*Power(t1,3)*Power(t2,3) - 
                  39*Power(t1,2)*Power(t2,4) - 154*t1*Power(t2,5) - 
                  40*Power(t2,6)) + 
               Power(s2,3)*(-25*Power(t1,6) - 103*Power(t1,5)*t2 + 
                  194*Power(t1,4)*Power(t2,2) + 
                  Power(t1,3)*Power(t2,3) + 
                  234*Power(t1,2)*Power(t2,4) + 290*t1*Power(t2,5) + 
                  20*Power(t2,6))) + 
            Power(s1,2)*(Power(s2,6)*
                (-2*Power(t1,3) + Power(t1,2)*t2 - 2*t1*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(t1,3)*Power(t2,2)*
                (18*Power(t1,4) - 19*Power(t1,3)*t2 + 
                  24*Power(t1,2)*Power(t2,2) + 89*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,5)*(8*Power(t1,4) - 50*Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) + 72*t1*Power(t2,3) + 
                  90*Power(t2,4)) + 
               Power(s2,2)*t1*t2*
                (113*Power(t1,5) + 202*Power(t1,4)*t2 - 
                  310*Power(t1,3)*Power(t2,2) - 
                  450*Power(t1,2)*Power(t2,3) - 428*t1*Power(t2,4) - 
                  126*Power(t2,5)) - 
               2*s2*Power(t1,2)*t2*
                (11*Power(t1,5) + 59*Power(t1,4)*t2 - 
                  86*Power(t1,3)*Power(t2,2) + 
                  42*Power(t1,2)*Power(t2,3) - 23*t1*Power(t2,4) - 
                  34*Power(t2,5)) - 
               Power(s2,4)*(10*Power(t1,5) - 158*Power(t1,4)*t2 - 
                  47*Power(t1,3)*Power(t2,2) + 
                  245*Power(t1,2)*Power(t2,3) + 411*t1*Power(t2,4) + 
                  195*Power(t2,5)) + 
               2*Power(s2,3)*(2*Power(t1,6) - 100*Power(t1,5)*t2 - 
                  74*Power(t1,4)*Power(t2,2) + 
                  171*Power(t1,3)*Power(t2,3) + 
                  352*Power(t1,2)*Power(t2,4) + 262*t1*Power(t2,5) + 
                  30*Power(t2,6)))) + 
         Power(s,5)*(Power(s1,4)*
             (-3*Power(s2,3)*Power(t1,2) + 
               t1*t2*(-15*Power(t1,3) + 26*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + Power(t2,3)) + 
               3*Power(s2,2)*
                (4*Power(t1,3) - 3*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
                  Power(t2,3)) - 
               s2*(10*Power(t1,4) - 34*Power(t1,3)*t2 + 
                  35*Power(t1,2)*Power(t2,2) + 16*t1*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,3)*(-4*Power(s2,4)*Power(t1,2) + 
               Power(s2,3)*(32*Power(t1,3) - 18*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) + 14*Power(t2,3)) + 
               t1*t2*(50*Power(t1,4) - 52*Power(t1,3)*t2 - 
                  29*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) - 
                  6*Power(t2,4)) - 
               Power(s2,2)*(52*Power(t1,4) - 151*Power(t1,3)*t2 + 
                  143*Power(t1,2)*Power(t2,2) + 126*t1*Power(t2,3) + 
                  36*Power(t2,4)) + 
               s2*(20*Power(t1,5) - 162*Power(t1,4)*t2 + 
                  189*Power(t1,3)*Power(t2,2) + 
                  122*Power(t1,2)*Power(t2,3) + 65*t1*Power(t2,4) + 
                  10*Power(t2,5))) + 
            Power(s1,2)*(2*Power(s2,4)*
                (6*Power(t1,3) - 3*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
                  5*Power(t2,3)) - 
               Power(s2,3)*(39*Power(t1,4) - 151*Power(t1,3)*t2 + 
                  111*Power(t1,2)*Power(t2,2) + 162*t1*Power(t2,3) + 
                  90*Power(t2,4)) + 
               t1*t2*(-35*Power(t1,5) + 20*Power(t1,4)*t2 + 
                  40*Power(t1,3)*Power(t2,2) + 
                  3*Power(t1,2)*Power(t2,3) + 20*t1*Power(t2,4) + 
                  9*Power(t2,5)) + 
               Power(s2,2)*(30*Power(t1,5) - 281*Power(t1,4)*t2 + 
                  281*Power(t1,3)*Power(t2,2) + 
                  309*Power(t1,2)*Power(t2,3) + 258*t1*Power(t2,4) + 
                  96*Power(t2,5)) - 
               s2*(5*Power(t1,6) - 181*Power(t1,5)*t2 + 
                  172*Power(t1,4)*Power(t2,2) + 
                  300*Power(t1,3)*Power(t2,3) + 
                  146*Power(t1,2)*Power(t2,4) + 120*t1*Power(t2,5) + 
                  14*Power(t2,6))) + 
            t2*(Power(s2,4)*(-9*Power(t1,4) + 48*Power(t1,3)*t2 - 
                  30*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
                  35*Power(t2,4)) + 
               Power(s2,3)*(21*Power(t1,5) - 61*Power(t1,4)*t2 - 
                  88*Power(t1,3)*Power(t2,2) + 
                  70*Power(t1,2)*Power(t2,3) - 30*t1*Power(t2,4) - 
                  70*Power(t2,5)) + 
               Power(t1,2)*t2*
                (-3*Power(t1,5) + 5*Power(t1,4)*t2 + 
                  23*Power(t1,3)*Power(t2,2) - 
                  5*Power(t1,2)*Power(t2,3) - 2*t1*Power(t2,4) + 
                  2*Power(t2,5)) + 
               s2*t1*(3*Power(t1,6) + 22*Power(t1,5)*t2 - 
                  82*Power(t1,4)*Power(t2,2) - 
                  43*Power(t1,3)*Power(t2,3) + 
                  3*Power(t1,2)*Power(t2,4) - 17*t1*Power(t2,5) - 
                  18*Power(t2,6)) + 
               Power(s2,2)*(-15*Power(t1,6) - 8*Power(t1,5)*t2 + 
                  182*Power(t1,4)*Power(t2,2) + 
                  8*Power(t1,3)*Power(t2,3) - 
                  17*Power(t1,2)*Power(t2,4) + 84*t1*Power(t2,5) + 
                  21*Power(t2,6))) + 
            s1*(-2*Power(s2,4)*
                (2*Power(t1,4) - 20*Power(t1,3)*t2 + 
                  9*Power(t1,2)*Power(t2,2) + 15*t1*Power(t2,3) + 
                  20*Power(t2,4)) + 
               Power(s2,3)*(6*Power(t1,5) - 126*Power(t1,4)*t2 + 
                  160*Power(t1,3)*Power(t2,2) + 
                  61*Power(t1,2)*Power(t2,3) + 130*t1*Power(t2,4) + 
                  150*Power(t2,5)) + 
               t1*t2*(5*Power(t1,6) + 20*Power(t1,5)*t2 - 
                  68*Power(t1,4)*Power(t2,2) + 
                  9*Power(t1,3)*Power(t2,3) + 
                  5*Power(t1,2)*Power(t2,4) - 15*t1*Power(t2,5) - 
                  4*Power(t2,6)) + 
               s2*t2*(-57*Power(t1,6) - 11*Power(t1,5)*t2 + 
                  265*Power(t1,4)*Power(t2,2) + 
                  111*Power(t1,3)*Power(t2,3) + 
                  79*Power(t1,2)*Power(t2,4) + 89*t1*Power(t2,5) + 
                  6*Power(t2,6)) - 
               Power(s2,2)*(2*Power(t1,6) - 141*Power(t1,5)*t2 + 
                  144*Power(t1,4)*Power(t2,2) + 
                  319*Power(t1,3)*Power(t2,3) + 
                  130*Power(t1,2)*Power(t2,4) + 237*t1*Power(t2,5) + 
                  84*Power(t2,6)))) + 
         Power(s,4)*(Power(s1,5)*
             (3*Power(s2,3)*Power(t1,2) + 
               Power(t1,2)*t2*(6*Power(t1,2) - 8*t1*t2 - Power(t2,2)) - 
               Power(s2,2)*(8*Power(t1,3) - 7*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + Power(t2,3)) + 
               s2*t1*(5*Power(t1,3) - 16*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,4)*(11*Power(s2,4)*Power(t1,2) + 
               Power(t1,2)*t2*
                (-35*Power(t1,3) + 23*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 5*Power(t2,3)) - 
               Power(s2,3)*(48*Power(t1,3) - 30*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) + 13*Power(t2,3)) - 
               2*s2*t1*(10*Power(t1,4) - 64*Power(t1,3)*t2 + 
                  58*Power(t1,2)*Power(t2,2) + 46*t1*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(s2,2)*(58*Power(t1,4) - 137*Power(t1,3)*t2 + 
                  136*Power(t1,2)*Power(t2,2) + 85*t1*Power(t2,3) + 
                  12*Power(t2,4))) + 
            Power(s1,3)*(6*Power(s2,5)*Power(t1,2) - 
               Power(s2,4)*(48*Power(t1,3) - 22*Power(t1,2)*t2 + 
                  39*t1*Power(t2,2) + 26*Power(t2,3)) + 
               Power(t1,2)*t2*
                (40*Power(t1,4) - 20*Power(t1,3)*t2 + 
                  18*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 
                  12*Power(t2,4)) + 
               Power(s2,3)*(93*Power(t1,4) - 235*Power(t1,3)*t2 + 
                  200*Power(t1,2)*Power(t2,2) + 258*t1*Power(t2,3) + 
                  88*Power(t2,4)) - 
               Power(s2,2)*(60*Power(t1,5) - 376*Power(t1,4)*t2 + 
                  252*Power(t1,3)*Power(t2,2) + 
                  486*Power(t1,2)*Power(t2,3) + 261*t1*Power(t2,4) + 
                  36*Power(t2,5)) + 
               s2*t1*(10*Power(t1,5) - 214*Power(t1,4)*t2 + 
                  123*Power(t1,3)*Power(t2,2) + 
                  276*Power(t1,2)*Power(t2,3) + 176*t1*Power(t2,4) + 
                  52*Power(t2,5))) - 
            Power(s1,2)*(2*Power(s2,5)*
                (4*Power(t1,3) - 2*Power(t1,2)*t2 + 4*t1*Power(t2,2) + 
                  5*Power(t2,3)) - 
               Power(s2,4)*(29*Power(t1,4) - 127*Power(t1,3)*t2 + 
                  64*Power(t1,2)*Power(t2,2) + 158*t1*Power(t2,3) + 
                  120*Power(t2,4)) + 
               Power(t1,2)*t2*
                (10*Power(t1,5) + 30*Power(t1,4)*t2 - 
                  65*Power(t1,3)*Power(t2,2) + 
                  86*Power(t1,2)*Power(t2,3) + 10*t1*Power(t2,4) - 
                  13*Power(t2,5)) + 
               s2*t1*t2*(-109*Power(t1,5) - 36*Power(t1,4)*t2 + 
                  256*Power(t1,3)*Power(t2,2) + 
                  180*Power(t1,2)*Power(t2,3) + 168*t1*Power(t2,4) + 
                  54*Power(t2,5)) + 
               3*Power(s2,3)*
                (10*Power(t1,5) - 104*Power(t1,4)*t2 + 
                  44*Power(t1,3)*Power(t2,2) + 
                  146*Power(t1,2)*Power(t2,3) + 153*t1*Power(t2,4) + 
                  60*Power(t2,5)) - 
               Power(s2,2)*(9*Power(t1,6) - 292*Power(t1,5)*t2 + 
                  83*Power(t1,4)*Power(t2,2) + 
                  560*Power(t1,3)*Power(t2,3) + 
                  526*Power(t1,2)*Power(t2,4) + 356*t1*Power(t2,5) + 
                  40*Power(t2,6))) + 
            t2*(Power(s2,5)*(2*Power(t1,4) - 21*Power(t1,3)*t2 + 
                  25*Power(t1,2)*Power(t2,2) + 15*t1*Power(t2,3) - 
                  21*Power(t2,4)) + 
               Power(t1,3)*Power(t2,2)*
                (-2*Power(t1,4) + 11*Power(t1,3)*t2 + 
                  7*Power(t1,2)*Power(t2,2) - 10*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s2*Power(t1,2)*t2*
                (8*Power(t1,5) - 15*Power(t1,4)*t2 - 
                  74*Power(t1,3)*Power(t2,2) + 
                  39*Power(t1,2)*Power(t2,3) + 14*t1*Power(t2,4) - 
                  10*Power(t2,5)) + 
               Power(s2,4)*(-6*Power(t1,5) + 34*Power(t1,4)*t2 + 
                  47*Power(t1,3)*Power(t2,2) - 
                  90*Power(t1,2)*Power(t2,3) + 5*t1*Power(t2,4) + 
                  70*Power(t2,5)) + 
               Power(s2,3)*(6*Power(t1,6) + 8*Power(t1,5)*t2 - 
                  180*Power(t1,4)*Power(t2,2) + 
                  68*Power(t1,3)*Power(t2,3) + 
                  60*Power(t1,2)*Power(t2,4) - 105*t1*Power(t2,5) - 
                  35*Power(t2,6)) + 
               Power(s2,2)*t1*
                (-2*Power(t1,6) - 29*Power(t1,5)*t2 + 
                  120*Power(t1,4)*Power(t2,2) + 
                  51*Power(t1,3)*Power(t2,3) - 
                  60*Power(t1,2)*Power(t2,4) + 25*t1*Power(t2,5) + 
                  45*Power(t2,6))) + 
            s1*(Power(s2,5)*(Power(t1,4) - 20*Power(t1,3)*t2 + 
                  7*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) + 
                  30*Power(t2,4)) + 
               Power(t1,2)*Power(t2,2)*
                (12*Power(t1,5) - 17*Power(t1,4)*t2 - 
                  34*Power(t1,3)*Power(t2,2) + 
                  55*Power(t1,2)*Power(t2,3) + 7*t1*Power(t2,4) - 
                  5*Power(t2,5)) - 
               Power(s2,4)*(2*Power(t1,5) - 74*Power(t1,4)*t2 + 
                  68*Power(t1,3)*Power(t2,2) - 
                  16*Power(t1,2)*Power(t2,3) + 105*t1*Power(t2,4) + 
                  165*Power(t2,5)) - 
               Power(s2,2)*t2*
                (-66*Power(t1,6) - 88*Power(t1,5)*t2 + 
                  384*Power(t1,4)*Power(t2,2) + 
                  171*Power(t1,3)*Power(t2,3) + 
                  206*Power(t1,2)*Power(t2,4) + 220*t1*Power(t2,5) + 
                  15*Power(t2,6)) + 
               s2*t1*t2*(-13*Power(t1,6) - 83*Power(t1,5)*t2 + 
                  209*Power(t1,4)*Power(t2,2) + 
                  27*Power(t1,3)*Power(t2,3) + 
                  10*Power(t1,2)*Power(t2,4) + 76*t1*Power(t2,5) + 
                  20*Power(t2,6)) + 
               Power(s2,3)*(Power(t1,6) - 107*Power(t1,5)*t2 + 
                  54*Power(t1,4)*Power(t2,2) + 
                  183*Power(t1,3)*Power(t2,3) + 
                  147*Power(t1,2)*Power(t2,4) + 345*t1*Power(t2,5) + 
                  140*Power(t2,6)))))*lg2(-s2))/
     (s1*s2*(-s + s2)*Power(t1,3)*Power(s - s2 + t1,3)*Power(t2,3)*
       Power(s - s1 + t2,3)) + 
    (8*(Power(s1,4)*Power(s2 - t1,2)*t1 + 
         Power(s,4)*(2*Power(t1,3) + Power(t1,2)*t2 + Power(t2,3)) + 
         s2*Power(t2,2)*(-3*s2*Power(t1,3) + Power(t1,3)*(t1 - t2) + 
            Power(s2,2)*t1*(3*t1 - t2) + Power(s2,3)*(-t1 + t2)) + 
         Power(s1,2)*t2*(Power(s2,4) + s2*Power(t1,2)*(t1 - 7*t2) - 
            Power(t1,3)*(t1 - 4*t2) + 3*Power(s2,2)*t1*(t1 + 2*t2) - 
            Power(s2,3)*(5*t1 + 2*t2)) + 
         Power(s1,3)*(s2 - t1)*
          (3*Power(t1,2)*t2 + Power(s2,2)*(2*t1 + t2) - 
            s2*t1*(t1 + 4*t2)) + 
         s1*t2*(Power(s2,4)*(t1 - 2*t2) + 
            Power(s2,2)*t1*(t1 - 2*t2)*t2 + 
            2*s2*Power(t1,2)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(s2,3)*(-2*Power(t1,2) + 4*t1*t2 + Power(t2,2)) - 
            Power(t1,3)*(Power(t1,2) - t1*t2 + 2*Power(t2,2))) + 
         Power(s,3)*(-2*s2*Power(t1,3) - 3*s2*Power(t1,2)*t2 + 
            7*Power(t1,3)*t2 + s2*t1*Power(t2,2) - 4*s2*Power(t2,3) + 
            t1*Power(t2,3) - s1*
             (6*Power(t1,3) + t1*Power(t2,2) + Power(t2,3) - 
               2*s2*(2*Power(t1,2) + 2*t1*t2 + Power(t2,2)))) - 
         s*(Power(s1,3)*(Power(s2,2)*(3*t1 + t2) + 
               Power(t1,2)*(4*t1 + t2) - s2*t1*(7*t1 + 4*t2)) + 
            t2*(3*s2*Power(t1,4) - 3*Power(s2,2)*t1*Power(t1 - t2,2) - 
               Power(t1,3)*(Power(t1,2) + Power(t2,2)) + 
               Power(s2,3)*(Power(t1,2) - 3*t1*t2 + 4*Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,3)*(t1 + t2) - 
               Power(t1,2)*t2*(7*t1 + 3*t2) - 
               Power(s2,2)*(7*Power(t1,2) + 13*t1*t2 + 4*Power(t2,2)) + 
               s2*t1*(4*Power(t1,2) + 10*t1*t2 + 7*Power(t2,2))) + 
            s1*t2*(-2*Power(s2,3)*(t1 + 3*t2) + 
               Power(t1,2)*(2*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,2) + 9*t1*t2 + 3*Power(t2,2)) - 
               s2*(7*Power(t1,3) + 4*t1*Power(t2,2)))) + 
         Power(s,2)*(t2*(3*Power(t1,3)*(t1 + t2) + 
               s2*t1*(-10*Power(t1,2) + 3*t1*t2 - 3*Power(t2,2)) + 
               3*Power(s2,2)*(Power(t1,2) - t1*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,2)*(2*t1 + t2) + 
               t1*(7*Power(t1,2) + Power(t2,2)) - 
               s2*(9*Power(t1,2) + 8*t1*t2 + 2*Power(t2,2))) - 
            s1*(t1*t2*(11*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,2) + 7*t1*t2 + 6*Power(t2,2)) - 
               s2*(5*Power(t1,3) + 6*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
                  3*Power(t2,3)))))*lg2(-(s2/(s*(-s + s2 - t1)))))/
     (s1*s2*(-s + s2 - t1)*Power(t1,3)*(-s + s1 - t2)*t2) - 
    (8*(Power(s,6)*(t1 + t2) + 
         Power(s,5)*(-3*s2*t1 + s1*(s2 - 4*t1 - t2) - 4*s2*t2 + 
            6*t1*t2) + s2*Power(t2,2)*
          (-(s2*Power(t1,2)*(t1 - 3*t2)) + Power(t1,3)*(t1 - t2) + 
            Power(s2,3)*t2 - 4*Power(s2,2)*t1*t2) + 
         Power(s1,3)*(s2 - t1)*
          (Power(t1,2)*t2 + Power(s2,2)*(t1 + t2) - s2*t1*(t1 + 3*t2)) + 
         s1*t2*(-(Power(s2,4)*t2) - 4*Power(s2,2)*t1*t2*(2*t1 + t2) + 
            Power(s2,3)*t2*(8*t1 + t2) - 
            Power(t1,3)*(Power(t1,2) - t1*t2 + 2*Power(t2,2)) + 
            s2*Power(t1,2)*(Power(t1,2) + t1*t2 + 4*Power(t2,2))) + 
         Power(s1,2)*(-2*Power(s2,3)*t2*(2*t1 + t2) + 
            Power(t1,3)*(Power(t1,2) - t1*t2 + 2*Power(t2,2)) - 
            s2*Power(t1,2)*(2*Power(t1,2) + 2*t1*t2 + 7*Power(t2,2)) + 
            Power(s2,2)*t1*(Power(t1,2) + 7*t1*t2 + 8*Power(t2,2))) + 
         Power(s,4)*(-18*s2*t1*t2 + Power(t1 + t2,3) + 
            Power(s1,2)*(-3*s2 + 6*t1 + t2) + 
            3*Power(s2,2)*(t1 + 2*t2) + 
            s1*(-3*Power(s2,2) - 12*t1*t2 + 2*s2*(5*t1 + t2))) + 
         Power(s,3)*(Power(s1,3)*(3*s2 - 4*t1 - t2) + 
            18*Power(s2,2)*t1*t2 - Power(s2,3)*(t1 + 4*t2) - 
            s2*Power(t1 + t2,2)*(t1 + 4*t2) + 
            2*(Power(t1,4) + Power(t1,3)*t2 + 2*t1*Power(t2,3)) + 
            Power(s1,2)*(7*Power(s2,2) - 2*s2*(6*t1 + t2) + 
               t2*(11*t1 + t2)) + 
            s1*(3*Power(s2,3) - 8*Power(s2,2)*t1 - 2*Power(t1,3) - 
               3*Power(t1,2)*t2 - 6*t1*Power(t2,2) - Power(t2,3) + 
               s2*(2*Power(t1,2) + 29*t1*t2 + Power(t2,2)))) + 
         Power(s,2)*(Power(t1,5) + Power(s1,4)*(-s2 + t1) + 
            Power(s2,4)*t2 - 6*Power(s2,3)*t1*t2 + 2*Power(t1,4)*t2 + 
            3*Power(t1,2)*Power(t2,3) + 
            3*Power(s2,2)*t2*(Power(t1,2) + 3*t1*t2 + 2*Power(t2,2)) - 
            s2*(Power(t1,4) + Power(t1,3)*t2 + 12*t1*Power(t2,3)) + 
            Power(s1,3)*(-5*Power(s2,2) - 5*t1*t2 + 3*s2*(2*t1 + t2)) + 
            Power(s1,2)*(-5*Power(s2,3) + Power(s2,2)*(8*t1 + t2) - 
               s2*(3*Power(t1,2) + 21*t1*t2 + 4*Power(t2,2)) + 
               t1*(Power(t1,2) + 2*t1*t2 + 5*Power(t2,2))) - 
            s1*(Power(s2,4) - 2*Power(s2,3)*(t1 - t2) + 
               Power(s2,2)*(2*Power(t1,2) + 22*t1*t2 + 3*Power(t2,2)) - 
               s2*(5*Power(t1,3) + 12*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(4*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  4*Power(t2,3)))) + 
         s*(Power(s1,4)*s2*(s2 - t1) + 
            Power(s1,3)*(2*Power(s2,3) - 2*Power(t1,2)*t2 - 
               3*Power(s2,2)*(t1 + t2) + s2*t1*(t1 + 8*t2)) + 
            Power(s1,2)*(Power(s2,4) - 2*Power(s2,3)*t1 + 
               2*Power(t1,4) + 3*Power(t1,2)*Power(t2,2) + 
               Power(s2,2)*(4*Power(t1,2) + 14*t1*t2 + 5*Power(t2,2)) - 
               s2*t1*(5*Power(t1,2) + 10*t1*t2 + 13*Power(t2,2))) + 
            t2*(s2*Power(t1,2)*(t1 - 6*t2)*t2 - 
               Power(s2,3)*t2*(3*t1 + 4*t2) + 
               Power(t1,3)*(Power(t1,2) + Power(t2,2)) - 
               Power(s2,2)*(Power(t1,3) - 12*t1*Power(t2,2))) + 
            s1*(Power(s2,4)*t2 + Power(s2,3)*t2*(5*t1 + 3*t2) - 
               Power(s2,2)*(Power(t1,3) + 9*Power(t1,2)*t2 + 
                  22*t1*Power(t2,2) + 3*Power(t2,3)) - 
               Power(t1,2)*(2*Power(t1,3) + Power(t1,2)*t2 + 
                  4*Power(t2,3)) + 
               s2*t1*(3*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + 8*Power(t2,3)))))*lg2(-(s2/(s*t1))))/
     (s1*s2*t1*Power(s - s2 + t1,3)*(-s + s1 - t2)*t2) + 
    (8*(Power(s2,5)*Power(s2 - t1,2)*t1*Power(t2,5)*(s2 + t2) - 
         s1*Power(s2,4)*Power(s2 - t1,2)*Power(t2,4)*(s2 + t2)*
          (Power(s2,2) + 4*s2*t1 - 2*Power(t1,2) - s2*t2 - 3*t1*t2) + 
         Power(s1,6)*s2*Power(s2 - t1,2)*
          (Power(s2,5) + Power(s2,2)*t1*(2*t1 - 5*t2)*t2 + 
            s2*t1*(t1 - 3*t2)*Power(t2,2) + Power(t1,2)*Power(t2,3) + 
            Power(s2,4)*(-2*t1 + 3*t2) + 
            Power(s2,3)*(Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) + 
         Power(s1,5)*(s2 - t1)*
          (Power(s2,8) + s2*Power(t1,3)*(2*t1 - 5*t2)*Power(t2,3) + 
            Power(t1,4)*Power(t2,4) + Power(s2,7)*(-3*t1 + 4*t2) + 
            Power(s2,6)*(3*Power(t1,2) - 13*t1*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (Power(t1,2) - 9*t1*t2 + 5*Power(t2,2)) - 
            Power(s2,5)*(Power(t1,3) - 15*Power(t1,2)*t2 + 
               16*t1*Power(t2,2) - 6*Power(t2,3)) + 
            Power(s2,3)*t1*t2*
             (Power(t1,3) - 4*Power(t1,2)*t2 + 18*t1*Power(t2,2) - 
               3*Power(t2,3)) + 
            Power(s2,4)*t2*(-7*Power(t1,3) + 15*Power(t1,2)*t2 - 
               16*t1*Power(t2,2) + 3*Power(t2,3))) + 
         Power(s1,2)*Power(s2,3)*Power(t2,3)*
          (4*Power(s2,6) + Power(t1,4)*t2*(t1 + t2) + 
            Power(s2,5)*(-6*t1 + 3*t2) + 
            Power(s2,4)*(-3*Power(t1,2) - 18*t1*t2 + 2*Power(t2,2)) - 
            5*Power(s2,2)*t1*
             (Power(t1,3) + 2*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
               Power(t2,3)) + 
            s2*Power(t1,2)*(Power(t1,3) - 3*Power(t1,2)*t2 - 
               16*t1*Power(t2,2) + 3*Power(t2,3)) + 
            Power(s2,3)*(9*Power(t1,3) + 27*Power(t1,2)*t2 - 
               16*t1*Power(t2,2) + 3*Power(t2,3))) - 
         Power(s1,4)*s2*t2*(3*Power(s2,7)*(t1 + t2) + 
            Power(t1,4)*Power(t2,3)*(3*t1 + 2*t2) + 
            4*s2*Power(t1,3)*Power(t2,2)*
             (2*Power(t1,2) - 4*t1*t2 - Power(t2,2)) - 
            Power(s2,6)*(11*Power(t1,2) + 5*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,5)*t1*(15*Power(t1,2) - t1*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*Power(t1,2)*t2*
             (6*Power(t1,3) - 31*Power(t1,2)*t2 + 17*t1*Power(t2,2) + 
               3*Power(t2,3)) + 
            Power(s2,3)*t1*(2*Power(t1,4) - 14*Power(t1,3)*t2 + 
               36*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 
               2*Power(t2,4)) + 
            Power(s2,4)*(-9*Power(t1,4) + 11*Power(t1,3)*t2 - 
               14*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
               2*Power(t2,4))) + 
         Power(s1,3)*Power(s2,2)*Power(t2,2)*
          (Power(s2,6)*(-2*t1 + 11*t2) + 
            Power(t1,3)*Power(t2,2)*
             (2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
            Power(s2,5)*(5*Power(t1,2) - 25*t1*t2 + 11*Power(t2,2)) - 
            2*s2*Power(t1,2)*t2*
             (Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) - 
               Power(t2,3)) + 
            Power(s2,4)*(-7*Power(t1,3) + 25*Power(t1,2)*t2 - 
               34*t1*Power(t2,2) + 5*Power(t2,3)) + 
            Power(s2,2)*t1*(-3*Power(t1,4) + 13*Power(t1,3)*t2 - 
               10*Power(t1,2)*Power(t2,2) + 21*t1*Power(t2,3) - 
               5*Power(t2,4)) + 
            Power(s2,3)*(7*Power(t1,4) - 22*Power(t1,3)*t2 + 
               32*Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) + 
               3*Power(t2,4))) + 
         Power(s,6)*t2*(-(Power(s2,2)*t1*Power(t1 - t2,2)*t2*
               (s2 + t2)) - s1*Power(s2,2)*t2*(s2 + t2)*
             (2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
            Power(s1,2)*(Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,3)*(Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*t2*(Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) + 
               s2*Power(t2,2)*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)))) \
- Power(s,5)*(s1*Power(s2,2)*Power(t2,2)*
             (Power(s2,2)*(-7*Power(t1,2) + 16*t1*t2 - 6*Power(t2,2)) + 
               s2*t2*(-3*Power(t1,2) + 11*t1*t2 - 5*Power(t2,2)) + 
               Power(t2,2)*(4*Power(t1,2) - 5*t1*t2 + Power(t2,2))) - 
            Power(s2,2)*t1*(t1 - t2)*Power(t2,2)*
             (Power(s2,2)*(3*t1 - 5*t2) + 
               t2*(-2*Power(t1,2) + t1*t2 + Power(t2,2)) - 
               2*s2*(Power(t1,2) - 2*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,3)*(Power(s2,4)*
                (Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(t2,4)*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*Power(t2,3)*
                (7*Power(t1,2) - 14*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*Power(t2,2)*
                (4*Power(t1,2) - 22*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,3)*t2*(5*Power(t1,2) - 18*t1*t2 + 7*Power(t2,2))\
) + Power(s1,2)*t2*(-3*t1*Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,4)*(5*Power(t1,2) - 18*t1*t2 + 8*Power(t2,2)) + 
               3*s2*Power(t2,2)*
                (-2*Power(t1,3) + 7*Power(t1,2)*t2 - 7*t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-3*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  22*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,3)*(-3*Power(t1,3) + 6*Power(t1,2)*t2 - 
                  17*t1*Power(t2,2) + 16*Power(t2,3)))) + 
         Power(s,4)*(Power(s1,4)*
             (t1*(3*t1 - 2*t2)*Power(t2,4) + Power(s2,5)*(-2*t1 + t2) + 
               s2*Power(t2,3)*
                (9*Power(t1,2) - 13*t1*t2 + Power(t2,2)) + 
               3*Power(s2,3)*t2*
                (3*Power(t1,2) - 10*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*Power(t2,2)*
                (6*Power(t1,2) - 30*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,4)*(3*Power(t1,2) - 13*t1*t2 + 5*Power(t2,2))) + 
            Power(s1,3)*(Power(s2,5)*
                (3*Power(t1,2) - 16*t1*t2 + Power(t2,2)) - 
               3*t1*Power(t2,4)*
                (3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*Power(t2,3)*
                (-20*Power(t1,3) + 56*Power(t1,2)*t2 - 
                  29*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*Power(t2,2)*
                (-11*Power(t1,3) + 69*Power(t1,2)*t2 - 
                  56*t1*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,3)*t2*
                (-13*Power(t1,3) + 41*Power(t1,2)*t2 - 
                  74*t1*Power(t2,2) + 23*Power(t2,3)) + 
               Power(s2,4)*(-2*Power(t1,3) + 30*Power(t1,2)*t2 - 
                  63*t1*Power(t2,2) + 28*Power(t2,3))) - 
            Power(s2,2)*t1*Power(t2,2)*
             (t1*Power(t1 - t2,2)*t2*(t1 + 2*t2) + 
               Power(s2,3)*(3*Power(t1,2) - 12*t1*t2 + 
                  10*Power(t2,2)) + 
               Power(s2,2)*(-4*Power(t1,3) + 12*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(Power(t1,4) - 4*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) - 
                  5*Power(t2,4))) + 
            s1*Power(s2,2)*Power(t2,2)*
             (Power(s2,3)*(-9*Power(t1,2) + 34*t1*t2 - 
                  15*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-3*Power(t1,2) + 12*t1*t2 - 10*Power(t2,2)) + 
               t1*t2*(6*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) + Power(t2,3)) + 
               s2*(6*Power(t1,4) - 14*Power(t1,3)*t2 + 
                  13*Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) + 
                  5*Power(t2,4))) + 
            Power(s1,2)*t2*(3*Power(t1,2)*Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,5)*(9*Power(t1,2) - 35*t1*t2 + 26*Power(t2,2)) + 
               3*s2*t1*Power(t2,2)*
                (2*Power(t1,3) - 10*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,4)*(-12*Power(t1,3) + 24*Power(t1,2)*t2 - 
                  19*t1*Power(t2,2) + 37*Power(t2,3)) + 
               Power(s2,2)*t2*
                (3*Power(t1,4) - 32*Power(t1,3)*t2 + 
                  85*Power(t1,2)*Power(t2,2) - 81*t1*Power(t2,3) + 
                  17*Power(t2,4)) + 
               Power(s2,3)*(3*Power(t1,4) - 14*Power(t1,3)*t2 + 
                  59*Power(t1,2)*Power(t2,2) - 61*t1*Power(t2,3) + 
                  28*Power(t2,4)))) - 
         Power(s,3)*(Power(s1,5)*
             (Power(s2,6) - 4*Power(s2,5)*(t1 - t2) + 
               s2*t1*(5*t1 - 4*t2)*Power(t2,3) + 
               Power(t1,2)*Power(t2,4) + 
               Power(s2,2)*Power(t2,2)*
                (4*Power(t1,2) - 18*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*t2*
                (7*Power(t1,2) - 22*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,4)*(3*Power(t1,2) - 14*t1*t2 + 7*Power(t2,2))) + 
            Power(s2,2)*t1*Power(t2,2)*(-Power(s2,2) + Power(t2,2))*
             (Power(t1,2)*Power(t1 - t2,2) - 
               2*s2*t1*(Power(t1,2) - 5*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) - 8*t1*t2 + 10*Power(t2,2))) + 
            Power(s1,4)*(3*Power(t1,2)*Power(t2,4)*(-3*t1 + 2*t2) + 
               Power(s2,6)*(-6*t1 + 3*t2) + 
               Power(s2,2)*t1*Power(t2,2)*
                (-15*Power(t1,2) + 87*t1*t2 - 40*Power(t2,2)) + 
               s2*t1*Power(t2,3)*
                (-24*Power(t1,2) + 49*t1*t2 - 8*Power(t2,2)) + 
               Power(s2,5)*(12*Power(t1,2) - 45*t1*t2 + 
                  10*Power(t2,2)) + 
               Power(s2,3)*t2*
                (-21*Power(t1,3) + 68*Power(t1,2)*t2 - 
                  97*t1*Power(t2,2) + 13*Power(t2,3)) + 
               Power(s2,4)*(-6*Power(t1,3) + 59*Power(t1,2)*t2 - 
                  91*t1*Power(t2,2) + 36*Power(t2,3))) - 
            s1*Power(s2,2)*Power(t2,2)*
             (Power(s2,3)*t2*
                (11*Power(t1,2) + 2*t1*t2 + 10*Power(t2,2)) + 
               Power(s2,4)*(5*Power(t1,2) - 36*t1*t2 + 
                  20*Power(t2,2)) + 
               Power(t1,2)*t2*
                (4*Power(t1,3) - 4*Power(t1,2)*t2 - 5*t1*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(s2,2)*(-9*Power(t1,4) + 28*Power(t1,3)*t2 - 
                  24*Power(t1,2)*Power(t2,2) + 34*t1*Power(t2,3) - 
                  10*Power(t2,4)) + 
               s2*t1*(4*Power(t1,4) - 13*Power(t1,3)*t2 + 
                  23*Power(t1,2)*Power(t2,2) - 25*t1*Power(t2,3) - 
                  4*Power(t2,4))) + 
            Power(s1,3)*(3*Power(s2,6)*
                (Power(t1,2) - 8*t1*t2 - Power(t2,2)) + 
               3*Power(t1,2)*Power(t2,4)*
                (3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*t1*Power(t2,3)*
                (19*Power(t1,3) - 76*Power(t1,2)*t2 + 
                  50*t1*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s2,5)*(-4*Power(t1,3) + 60*Power(t1,2)*t2 - 
                  80*t1*Power(t2,2) + 53*Power(t2,3)) + 
               Power(s2,2)*Power(t2,2)*
                (10*Power(t1,4) - 100*Power(t1,3)*t2 + 
                  164*Power(t1,2)*Power(t2,2) - 60*t1*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(s2,3)*t2*
                (11*Power(t1,4) - 52*Power(t1,3)*t2 + 
                  178*Power(t1,2)*Power(t2,2) - 83*t1*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(s2,4)*(Power(t1,4) - 48*Power(t1,3)*t2 + 
                  102*Power(t1,2)*Power(t2,2) - 95*t1*Power(t2,3) + 
                  38*Power(t2,4))) + 
            Power(s1,2)*t2*(-(Power(t1,3)*Power(t1 - t2,2)*
                  Power(t2,3)) + 
               Power(s2,6)*(7*Power(t1,2) - 37*t1*t2 + 44*Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (-2*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  27*t1*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s2,5)*(-15*Power(t1,3) + 49*Power(t1,2)*t2 - 
                  21*t1*Power(t2,2) + 48*Power(t2,3)) + 
               Power(s2,4)*(9*Power(t1,4) - 37*Power(t1,3)*t2 + 
                  89*Power(t1,2)*Power(t2,2) - 119*t1*Power(t2,3) + 
                  32*Power(t2,4)) - 
               Power(s2,2)*t1*t2*
                (Power(t1,4) - 33*Power(t1,3)*t2 + 
                  100*Power(t1,2)*Power(t2,2) - 108*t1*Power(t2,3) + 
                  32*Power(t2,4)) - 
               Power(s2,3)*(Power(t1,5) - 22*Power(t1,4)*t2 + 
                  99*Power(t1,3)*Power(t2,2) - 
                  157*Power(t1,2)*Power(t2,3) + 153*t1*Power(t2,4) - 
                  28*Power(t2,5)))) + 
         s*(Power(s2,4)*(s2 - t1)*t1*Power(t2,4)*(s2 + t2)*
             (Power(s2,2) + s2*(t1 - 5*t2) + t1*(-2*t1 + 3*t2)) - 
            s1*Power(s2,3)*(s2 - t1)*Power(t2,3)*(s2 + t2)*
             (Power(s2,3)*(4*t1 - 6*t2) + 
               2*Power(t1,2)*(2*t1 - 3*t2)*t2 + 
               Power(s2,2)*(-2*Power(t1,2) - 19*t1*t2 + 
                  5*Power(t2,2)) + 
               s2*t1*(-2*Power(t1,2) + 15*t1*t2 + 9*Power(t2,2))) + 
            Power(s1,6)*s2*(-2*Power(s2,6) + 6*Power(s2,5)*(t1 - t2) + 
               2*s2*Power(t1,2)*(t1 - 4*t2)*Power(t2,2) + 
               2*Power(t1,3)*Power(t2,3) + 
               Power(s2,4)*(-6*Power(t1,2) + 16*t1*t2 - 
                  5*Power(t2,2)) + 
               Power(s2,2)*t1*t2*
                (4*Power(t1,2) - 12*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,3)*(2*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - Power(t2,3))) + 
            Power(s1,4)*(Power(s2,8)*(2*t1 - t2) + 
               Power(t1,4)*(3*t1 - 2*t2)*Power(t2,4) + 
               s2*Power(t1,3)*Power(t2,3)*
                (6*Power(t1,2) - 29*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,7)*(-6*Power(t1,2) + 25*t1*t2 + 
                  6*Power(t2,2)) + 
               Power(s2,6)*(6*Power(t1,3) - 66*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) - 18*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*Power(t2,2)*
                (3*Power(t1,3) - 49*Power(t1,2)*t2 + 
                  74*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,3)*t1*t2*
                (3*Power(t1,4) - 26*Power(t1,3)*t2 + 
                  123*Power(t1,2)*Power(t2,2) - 48*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,5)*(-2*Power(t1,4) + 64*Power(t1,3)*t2 - 
                  71*Power(t1,2)*Power(t2,2) + 48*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,4)*t2*
                (-25*Power(t1,4) + 65*Power(t1,3)*t2 - 
                  114*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) + 
                  6*Power(t2,4))) - 
            Power(s1,5)*(3*Power(s2,8) - 12*Power(s2,7)*(t1 - t2) + 
               s2*Power(t1,3)*(9*t1 - 16*t2)*Power(t2,3) + 
               3*Power(t1,4)*Power(t2,4) + 
               2*Power(s2,6)*
                (9*Power(t1,2) - 24*t1*t2 + 7*Power(t2,2)) + 
               3*Power(s2,2)*Power(t1,2)*Power(t2,2)*
                (2*Power(t1,2) - 14*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,3)*t1*t2*
                (9*Power(t1,3) - 34*Power(t1,2)*t2 + 
                  73*t1*Power(t2,2) - 12*Power(t2,3)) + 
               Power(s2,5)*(-12*Power(t1,3) + 69*Power(t1,2)*t2 - 
                  59*t1*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s2,4)*(3*Power(t1,4) - 42*Power(t1,3)*t2 + 
                  73*Power(t1,2)*Power(t2,2) - 55*t1*Power(t2,3) + 
                  7*Power(t2,4))) + 
            Power(s1,2)*Power(s2,2)*Power(t2,2)*
             (5*Power(s2,6)*(t1 - 4*t2) - 4*Power(t1,5)*Power(t2,2) + 
               2*Power(t1,3)*Power(t2,4) + 
               Power(s2,5)*(-15*Power(t1,2) + 22*t1*t2 - 
                  16*Power(t2,2)) + 
               s2*Power(t1,2)*t2*
                (Power(t1,3) - 6*Power(t1,2)*t2 + 39*t1*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(s2,4)*(19*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  79*t1*Power(t2,2) - 10*Power(t2,3)) + 
               Power(s2,3)*(-13*Power(t1,4) + 25*Power(t1,3)*t2 - 
                  100*Power(t1,2)*Power(t2,2) + 78*t1*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s2,2)*t1*
                (4*Power(t1,4) - 20*Power(t1,3)*t2 + 
                  49*Power(t1,2)*Power(t2,2) - 108*t1*Power(t2,3) + 
                  21*Power(t2,4))) + 
            Power(s1,3)*s2*t2*
             (2*Power(t1,4)*(3*t1 - 2*t2)*Power(t2,3) + 
               2*Power(s2,7)*(2*t1 + t2) + 
               Power(s2,6)*(-15*Power(t1,2) + 12*t1*t2 - 
                  40*Power(t2,2)) + 
               Power(s2,5)*(22*Power(t1,3) - 36*Power(t1,2)*t2 + 
                  63*t1*Power(t2,2) - 35*Power(t2,3)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (18*Power(t1,3) - 45*Power(t1,2)*t2 + 
                  16*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,4)*(-15*Power(t1,4) + 49*Power(t1,3)*t2 - 
                  81*Power(t1,2)*Power(t2,2) + 81*t1*Power(t2,3) - 
                  13*Power(t2,4)) + 
               Power(s2,2)*t1*t2*
                (14*Power(t1,4) - 79*Power(t1,3)*t2 + 
                  85*Power(t1,2)*Power(t2,2) - 46*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,3)*(4*Power(t1,5) - 41*Power(t1,4)*t2 + 
                  118*Power(t1,3)*Power(t2,2) - 
                  83*Power(t1,2)*Power(t2,3) + 56*t1*Power(t2,4) - 
                  10*Power(t2,5)))) + 
         Power(s,2)*(Power(s1,6)*s2*
             (Power(s2,5) + s2*t1*(t1 - 4*t2)*Power(t2,2) + 
               Power(t1,2)*Power(t2,3) + Power(s2,4)*(-2*t1 + 3*t2) + 
               Power(s2,2)*t2*(2*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(Power(t1,2) - 5*t1*t2 + 3*Power(t2,2))) + 
            Power(s2,3)*t1*Power(t2,3)*
             (Power(s2,4)*(2*t1 - 5*t2) + 
               Power(t1,2)*t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
               2*Power(s2,2)*t2*
                (3*Power(t1,2) - 6*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(-3*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2)) + 
               s2*t1*(Power(t1,3) - 4*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) - 12*Power(t2,3))) + 
            Power(s1,5)*(3*Power(s2,7) - 12*Power(s2,6)*(t1 - t2) - 
               2*s2*Power(t1,2)*(6*t1 - 7*t2)*Power(t2,3) - 
               3*Power(t1,3)*Power(t2,4) - 
               9*Power(s2,2)*t1*Power(t2,2)*
                (Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
               Power(s2,5)*(15*Power(t1,2) - 45*t1*t2 + 
                  17*Power(t2,2)) + 
               Power(s2,3)*t2*
                (-15*Power(t1,3) + 47*Power(t1,2)*t2 - 
                  50*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,4)*(-6*Power(t1,3) + 48*Power(t1,2)*t2 - 
                  61*t1*Power(t2,2) + 19*Power(t2,3))) + 
            s1*Power(s2,2)*Power(t2,2)*
             (3*Power(t1,3)*Power(t1 - t2,2)*Power(t2,2) - 
               Power(s2,4)*t2*
                (15*Power(t1,2) + 13*t1*t2 + 5*Power(t2,2)) - 
               Power(s2,5)*(Power(t1,2) - 19*t1*t2 + 15*Power(t2,2)) + 
               s2*Power(t1,2)*t2*
                (Power(t1,3) + 5*t1*Power(t2,2) - 15*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-2*Power(t1,4) + 9*Power(t1,3)*t2 - 
                  12*Power(t1,2)*Power(t2,2) + 33*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,3)*(3*Power(t1,4) - 14*Power(t1,3)*t2 + 
                  34*Power(t1,2)*Power(t2,2) - 26*t1*Power(t2,3) + 
                  10*Power(t2,4))) + 
            Power(s1,4)*(3*Power(t1,3)*(3*t1 - 2*t2)*Power(t2,4) + 
               Power(s2,7)*(-6*t1 + 3*t2) + 
               Power(s2,6)*(15*Power(t1,2) - 54*t1*t2 + 2*Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,3)*
                (21*Power(t1,2) - 62*t1*t2 + 11*Power(t2,2)) + 
               Power(s2,2)*t1*Power(t2,2)*
                (12*Power(t1,3) - 104*Power(t1,2)*t2 + 
                  102*t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,5)*(-12*Power(t1,3) + 105*Power(t1,2)*t2 - 
                  89*t1*Power(t2,2) + 39*Power(t2,3)) + 
               Power(s2,3)*t2*
                (15*Power(t1,4) - 64*Power(t1,3)*t2 + 
                  180*Power(t1,2)*Power(t2,2) - 35*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,4)*(3*Power(t1,4) - 69*Power(t1,3)*t2 + 
                  131*Power(t1,2)*Power(t2,2) - 111*t1*Power(t2,3) + 
                  11*Power(t2,4))) + 
            Power(s1,3)*(Power(s2,7)*
                (Power(t1,2) - 16*t1*t2 - 5*Power(t2,2)) - 
               Power(t1,3)*Power(t2,4)*
                (3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,3)*
                (-6*Power(t1,3) + 40*Power(t1,2)*t2 - 
                  29*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,6)*(-2*Power(t1,3) + 50*Power(t1,2)*t2 - 
                  45*t1*Power(t2,2) + 61*Power(t2,3)) + 
               Power(s2,2)*t1*Power(t2,2)*
                (-3*Power(t1,4) + 67*Power(t1,3)*t2 - 
                  153*Power(t1,2)*Power(t2,2) + 63*t1*Power(t2,3) - 
                  12*Power(t2,4)) + 
               Power(s2,4)*t2*
                (26*Power(t1,4) - 86*Power(t1,3)*t2 + 
                  172*Power(t1,2)*Power(t2,2) - 88*t1*Power(t2,3) + 
                  13*Power(t2,4)) + 
               Power(s2,5)*(Power(t1,4) - 57*Power(t1,3)*t2 + 
                  96*Power(t1,2)*Power(t2,2) - 81*t1*Power(t2,3) + 
                  46*Power(t2,4)) + 
               Power(s2,3)*t2*
                (-3*Power(t1,5) + 39*Power(t1,4)*t2 - 
                  187*Power(t1,3)*Power(t2,2) + 
                  162*Power(t1,2)*Power(t2,3) - 70*t1*Power(t2,4) + 
                  13*Power(t2,5))) + 
            Power(s1,2)*s2*t2*
             (-3*Power(t1,3)*Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,6)*(2*Power(t1,2) - 21*t1*t2 + 41*Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (-13*Power(t1,3) + 33*Power(t1,2)*t2 - 
                  44*t1*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s2,5)*(-6*Power(t1,3) + 45*Power(t1,2)*t2 - 
                  29*t1*Power(t2,2) + 37*Power(t2,3)) + 
               Power(s2,2)*t1*t2*
                (-11*Power(t1,4) + 57*Power(t1,3)*t2 - 
                  112*Power(t1,2)*Power(t2,2) + 153*t1*Power(t2,3) - 
                  36*Power(t2,4)) + 
               Power(s2,4)*(6*Power(t1,4) - 45*Power(t1,3)*t2 + 
                  56*Power(t1,2)*Power(t2,2) - 136*t1*Power(t2,3) + 
                  23*Power(t2,4)) + 
               Power(s2,3)*(-2*Power(t1,5) + 32*Power(t1,4)*t2 - 
                  107*Power(t1,3)*Power(t2,2) + 
                  165*Power(t1,2)*Power(t2,3) - 153*t1*Power(t2,4) + 
                  27*Power(t2,5)))))*lg2(-t1))/
     (Power(s1,3)*Power(s2,3)*t1*Power(s - s2 + t1,3)*Power(t2,3)*
       (s - s1 + t2)*(s2 + t2)) - 
    (8*(Power(s1,9)*t1*(-Power(s2,4) + 4*Power(s2,3)*t1 + Power(t1,4)) - 
         Power(s2,5)*(s2 - t1)*Power(t1,2)*(s2 - t2)*Power(t2,5) - 
         Power(s1,8)*(Power(s2,2)*Power(t1,3)*t2 + 4*Power(t1,5)*t2 + 
            Power(s2,5)*(3*t1 + t2) - s2*Power(t1,4)*(5*t1 + 3*t2) - 
            Power(s2,4)*t1*(11*t1 + 8*t2) + 
            Power(s2,3)*Power(t1,2)*(-3*t1 + 25*t2)) + 
         s1*Power(s2,4)*Power(t1,2)*Power(t2,4)*
          (2*Power(s2,3) - t1*t2*(2*t1 + 3*t2) - 
            Power(s2,2)*(5*t1 + 7*t2) + 
            s2*(2*Power(t1,2) + 9*t1*t2 + 4*Power(t2,2))) + 
         Power(s1,7)*(6*Power(t1,5)*Power(t2,2) - 
            2*Power(s2,6)*(t1 + t2) - 
            s2*Power(t1,4)*t2*(18*t1 + 11*t2) + 
            Power(s2,4)*t1*(9*Power(t1,2) - 52*t1*t2 - 24*Power(t2,2)) + 
            Power(s2,2)*Power(t1,3)*
             (7*Power(t1,2) + 6*t1*t2 + 4*Power(t2,2)) + 
            Power(s2,5)*(8*Power(t1,2) + 18*t1*t2 + 5*Power(t2,2)) + 
            Power(s2,3)*Power(t1,2)*
             (2*Power(t1,2) - 17*t1*t2 + 63*Power(t2,2))) + 
         Power(s1,2)*Power(s2,3)*Power(t2,3)*
          (-(Power(t1,4)*t2*(t1 + t2)) - 
            Power(s2,4)*(2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(s2,3)*(9*Power(t1,3) + 9*Power(t1,2)*t2 + 
               Power(t2,3)) - 
            Power(s2,2)*t1*(8*Power(t1,3) + 18*Power(t1,2)*t2 + 
               12*t1*Power(t2,2) + Power(t2,3)) + 
            s2*Power(t1,2)*(Power(t1,3) + 12*Power(t1,2)*t2 + 
               6*t1*Power(t2,2) + 3*Power(t2,3))) - 
         Power(s1,6)*(Power(s2,7)*t2 + 4*Power(t1,5)*Power(t2,3) - 
            3*s2*Power(t1,4)*Power(t2,2)*(8*t1 + 5*t2) - 
            Power(s2,6)*t2*(8*t1 + 9*t2) + 
            Power(s2,2)*Power(t1,3)*t2*
             (18*Power(t1,2) + 20*t1*t2 + 5*Power(t2,2)) + 
            Power(s2,4)*t1*(5*Power(t1,3) + 29*Power(t1,2)*t2 - 
               94*t1*Power(t2,2) - 36*Power(t2,3)) + 
            Power(s2,5)*(-13*Power(t1,3) + 28*Power(t1,2)*t2 + 
               42*t1*Power(t2,2) + 10*Power(t2,3)) + 
            2*Power(s2,3)*Power(t1,2)*
             (-4*Power(t1,3) + 6*Power(t1,2)*t2 - 25*t1*Power(t2,2) + 
               42*Power(t2,3))) + 
         Power(s1,3)*Power(s2,2)*Power(t2,2)*
          (Power(s2,5)*t2*(-3*t1 + 4*t2) - 
            Power(t1,3)*Power(t2,2)*
             (2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
            Power(s2,4)*(2*Power(t1,3) - 3*Power(t1,2)*t2 - 
               2*t1*Power(t2,2) - 6*Power(t2,3)) - 
            Power(s2,2)*t1*t2*
             (31*Power(t1,3) - 13*Power(t1,2)*t2 + 15*t1*Power(t2,2) + 
               2*Power(t2,3)) + 
            s2*Power(t1,2)*t2*
             (2*Power(t1,3) + 13*Power(t1,2)*t2 - 14*t1*Power(t2,2) + 
               5*Power(t2,3)) + 
            Power(s2,3)*(4*Power(t1,4) + 12*Power(t1,3)*t2 + 
               10*Power(t1,2)*Power(t2,2) + 9*t1*Power(t2,3) + 
               Power(t2,4))) + 
         Power(s1,5)*(Power(t1,5)*Power(t2,4) - 
            s2*Power(t1,4)*Power(t2,3)*(14*t1 + 9*t2) - 
            Power(s2,7)*(2*Power(t1,2) + t1*t2 - 4*Power(t2,2)) + 
            Power(s2,2)*Power(t1,3)*Power(t2,2)*
             (13*Power(t1,2) + 25*t1*t2 + Power(t2,2)) + 
            2*Power(s2,6)*(4*Power(t1,3) - Power(t1,2)*t2 - 
               6*t1*Power(t2,2) - 8*Power(t2,3)) + 
            Power(s2,3)*Power(t1,2)*t2*
             (-15*Power(t1,3) + 31*Power(t1,2)*t2 - 75*t1*Power(t2,2) + 
               64*Power(t2,3)) + 
            Power(s2,4)*t1*(4*Power(t1,4) + 2*Power(t1,3)*t2 + 
               48*Power(t1,2)*Power(t2,2) - 85*t1*Power(t2,3) - 
               29*Power(t2,4)) + 
            Power(s2,5)*(-6*Power(t1,4) - 27*Power(t1,3)*t2 + 
               30*Power(t1,2)*Power(t2,2) + 49*t1*Power(t2,3) + 
               10*Power(t2,4))) + 
         Power(s1,4)*s2*t2*(2*s2*Power(t1,3)*Power(t2,3)*(-7*t1 + t2) + 
            Power(t1,4)*Power(t2,3)*(3*t1 + 2*t2) + 
            3*Power(s2,6)*(Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
            Power(s2,2)*Power(t1,2)*t2*
             (6*Power(t1,3) - 33*Power(t1,2)*t2 + 53*t1*Power(t2,2) - 
               27*Power(t2,3)) + 
            Power(s2,5)*(-15*Power(t1,3) + 2*Power(t1,2)*t2 + 
               8*t1*Power(t2,2) + 14*Power(t2,3)) + 
            Power(s2,4)*(9*Power(t1,4) + 12*Power(t1,3)*t2 - 
               12*Power(t1,2)*Power(t2,2) - 30*t1*Power(t2,3) - 
               5*Power(t2,4)) + 
            Power(s2,3)*t1*(-5*Power(t1,4) + 25*Power(t1,3)*t2 - 
               44*Power(t1,2)*Power(t2,2) + 44*t1*Power(t2,3) + 
               12*Power(t2,4))) + 
         Power(s,7)*(s1*Power(s2,2)*Power(t1,2)*(t1 - t2)*t2 + 
            Power(s2,2)*Power(t1,2)*Power(t1 - t2,2)*t2 + 
            Power(s1,2)*(s2*Power(t1,3)*(t1 - t2) - 
               Power(t1,3)*Power(t1 - t2,2) + 
               Power(s2,2)*(2*Power(t1,3) + Power(t1,2)*t2 + Power(t2,3))\
)) + Power(s,6)*(Power(s2,2)*Power(t1,2)*(t1 - t2)*t2*
             (-3*s2*t1 + Power(t1,2) + 5*s2*t2 + 2*t1*t2 - 
               3*Power(t2,2)) + 
            s1*Power(s2,2)*Power(t1,2)*
             (s2*(-2*Power(t1,2) + t1*t2 + 4*Power(t2,2)) - 
               2*t2*(3*Power(t1,2) - 8*t1*t2 + 5*Power(t2,2))) + 
            Power(s1,3)*(2*Power(s2,3)*
                (2*Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
               s2*Power(t1,2)*
                (-6*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(7*Power(t1,2) - 12*t1*t2 + 5*Power(t2,2)) - 
               Power(s2,2)*(11*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,2)*(-4*Power(t1,3)*Power(t1 - t2,2)*t2 + 
               s2*Power(t1,3)*(5*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(-4*Power(t1,3) - 5*Power(t1,2)*t2 + 
                  t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,2)*(-Power(t1,4) + 11*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(s,5)*(Power(s1,4)*
             (Power(s2,4)*(2*t1 + t2) + 
               Power(t1,3)*(-21*Power(t1,2) + 30*t1*t2 - 
                  10*Power(t2,2)) + 
               s2*Power(t1,2)*
                (15*Power(t1,2) + 10*t1*t2 - 8*Power(t2,2)) - 
               Power(s2,3)*(21*Power(t1,2) + 20*t1*t2 + 
                  8*Power(t2,2)) + 
               Power(s2,2)*(25*Power(t1,3) + 7*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) + 6*Power(t2,3))) + 
            s1*Power(s2,2)*Power(t1,2)*
             (Power(s2,2)*(4*Power(t1,2) - 9*t1*t2 - 6*Power(t2,2)) - 
               s2*(Power(t1,3) - 10*Power(t1,2)*t2 + 
                  36*t1*Power(t2,2) - 41*Power(t2,3)) - 
               3*t2*(2*Power(t1,3) + Power(t1,2)*t2 - 
                  10*t1*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s2,2)*Power(t1,2)*t2*
             (3*Power(t1 - t2,2)*t2*(t1 + t2) + 
               Power(s2,2)*(3*Power(t1,2) - 12*t1*t2 + 
                  10*Power(t2,2)) - 
               2*s2*(Power(t1,3) + Power(t1,2)*t2 - 9*t1*Power(t2,2) + 
                  7*Power(t2,3))) + 
            Power(s1,3)*(8*Power(t1,3)*t2*
                (3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) - 
               Power(s2,4)*(8*Power(t1,2) + 11*t1*t2 + 8*Power(t2,2)) + 
               4*Power(s2,3)*
                (5*Power(t1,3) + 8*Power(t1,2)*t2 + 4*t1*Power(t2,2) + 
                  6*Power(t2,3)) + 
               s2*Power(t1,2)*
                (-30*Power(t1,3) + 27*Power(t1,2)*t2 - 
                  18*t1*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(5*Power(t1,4) - 40*Power(t1,3)*t2 - 
                  19*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) - 
                  9*Power(t2,4))) - 
            Power(s1,2)*(6*Power(t1,3)*Power(t1 - t2,2)*Power(t2,2) - 
               6*s2*Power(t1,3)*t2*
                (3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) + 
               2*Power(s2,4)*
                (Power(t1,3) - 5*Power(t1,2)*t2 + 2*t1*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(s2,3)*(-13*Power(t1,4) + 47*Power(t1,3)*t2 + 
                  22*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,2)*(7*Power(t1,5) - 37*Power(t1,4)*t2 + 
                  21*Power(t1,3)*Power(t2,2) - 
                  27*Power(t1,2)*Power(t2,3) - 3*t1*Power(t2,4) - 
                  3*Power(t2,5)))) + 
         Power(s,4)*(-(Power(s1,5)*
               (Power(s2,4)*(9*t1 + 4*t2) + 
                 2*s2*Power(t1,2)*
                  (10*Power(t1,2) + 15*t1*t2 - 6*Power(t2,2)) - 
                 5*Power(t1,3)*
                  (7*Power(t1,2) - 8*t1*t2 + 2*Power(t2,2)) - 
                 4*Power(s2,3)*
                  (12*Power(t1,2) + 10*t1*t2 + 3*Power(t2,2)) + 
                 2*Power(s2,2)*
                  (15*Power(t1,3) + 9*Power(t1,2)*t2 + 
                    3*t1*Power(t2,2) + 2*Power(t2,3)))) + 
            Power(s2,2)*Power(t1,2)*t2*
             (Power(t1 - t2,2)*Power(t2,2)*(3*t1 + t2) - 
               Power(s2,3)*(Power(t1,2) - 8*t1*t2 + 10*Power(t2,2)) + 
               s2*t2*(-5*Power(t1,3) + 9*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) - 13*Power(t2,3)) + 
               Power(s2,2)*(Power(t1,3) + Power(t1,2)*t2 - 
                  24*t1*Power(t2,2) + 26*Power(t2,3))) + 
            s1*Power(s2,2)*Power(t1,2)*
             (Power(s2,3)*(-2*Power(t1,2) + 11*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,3) - 4*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) - 65*Power(t2,3)) + 
               Power(t2,2)*(-15*Power(t1,3) + 18*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - 16*Power(t2,3)) + 
               s2*t2*(5*Power(t1,3) + 3*Power(t1,2)*t2 - 
                  62*t1*Power(t2,2) + 80*Power(t2,3))) + 
            Power(s1,4)*(-(Power(s2,5)*(4*t1 + 3*t2)) - 
               4*Power(t1,3)*t2*
                (15*Power(t1,2) - 20*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,4)*(36*Power(t1,2) + 56*t1*t2 + 
                  31*Power(t2,2)) + 
               s2*Power(t1,2)*
                (75*Power(t1,3) - 45*Power(t1,2)*t2 + 
                  62*t1*Power(t2,2) - 21*Power(t2,3)) - 
               Power(s2,3)*(37*Power(t1,3) + 107*Power(t1,2)*t2 + 
                  73*t1*Power(t2,2) + 42*Power(t2,3)) + 
               Power(s2,2)*(-10*Power(t1,4) + 29*Power(t1,3)*t2 + 
                  56*Power(t1,2)*Power(t2,2) + 18*t1*Power(t2,3) + 
                  9*Power(t2,4))) + 
            Power(s1,3)*(6*Power(t1,3)*Power(t2,2)*
                (5*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,5)*(2*Power(t1,2) + 9*t1*t2 + 12*Power(t2,2)) + 
               Power(s2,4)*(12*Power(t1,3) - 58*Power(t1,2)*t2 - 
                  33*t1*Power(t2,2) - 54*Power(t2,3)) + 
               s2*Power(t1,2)*t2*
                (-90*Power(t1,3) + 109*Power(t1,2)*t2 - 
                  54*t1*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,3)*(-30*Power(t1,4) + 130*Power(t1,3)*t2 + 
                  74*Power(t1,2)*Power(t2,2) + 43*t1*Power(t2,3) + 
                  43*Power(t2,4)) + 
               Power(s2,2)*(35*Power(t1,5) - 102*Power(t1,4)*t2 + 
                  48*Power(t1,3)*Power(t2,2) - 
                  63*Power(t1,2)*Power(t2,3) - 15*t1*Power(t2,4) - 
                  6*Power(t2,5))) + 
            Power(s1,2)*(-4*Power(t1,3)*Power(t1 - t2,2)*Power(t2,3) + 
               4*s2*Power(t1,3)*Power(t2,2)*
                (6*Power(t1,2) - 11*t1*t2 + 5*Power(t2,2)) + 
               2*Power(s2,5)*
                (4*Power(t1,3) - 5*Power(t1,2)*t2 + 3*t1*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(s2,4)*(-24*Power(t1,4) + 67*Power(t1,3)*t2 + 
                  32*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) + 
                  26*Power(t2,4)) + 
               Power(s2,3)*(12*Power(t1,5) - 45*Power(t1,4)*t2 - 
                  11*Power(t1,3)*Power(t2,2) - 
                  90*Power(t1,2)*Power(t2,3) - 8*t1*Power(t2,4) - 
                  13*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-4*Power(t1,5) + 76*Power(t1,4)*t2 - 
                  68*Power(t1,3)*Power(t2,2) + 
                  40*Power(t1,2)*Power(t2,3) + 3*t1*Power(t2,4) + 
                  Power(t2,5)))) + 
         s*(Power(s1,8)*(Power(s2,4)*(6*t1 + t2) + 
               Power(t1,4)*(-7*t1 + 2*t2) + 
               s2*Power(t1,3)*(t1 + 4*t2) - 
               Power(s2,3)*t1*(21*t1 + 4*t2) + 
               Power(s2,2)*Power(t1,2)*(t1 + 5*t2)) + 
            Power(s2,4)*Power(t1,2)*Power(t2,4)*
             (-2*Power(s2,3) + 7*Power(s2,2)*t2 + 
               t1*t2*(-2*t1 + 3*t2) + 
               2*s2*(Power(t1,2) - 2*t1*t2 - 2*Power(t2,2))) + 
            Power(s1,7)*(8*Power(t1,4)*(3*t1 - t2)*t2 + 
               Power(s2,5)*(13*t1 + 6*t2) + 
               Power(s2,2)*Power(t1,2)*
                (Power(t1,2) + 18*t1*t2 - 25*Power(t2,2)) - 
               Power(s2,4)*(45*Power(t1,2) + 47*t1*t2 + 
                  7*Power(t2,2)) - 
               s2*Power(t1,3)*
                (30*Power(t1,2) + 9*t1*t2 + 14*Power(t2,2)) + 
               Power(s2,3)*t1*
                (-8*Power(t1,2) + 116*t1*t2 + 19*Power(t2,2))) + 
            s1*Power(s2,3)*Power(t1,2)*Power(t2,3)*
             (2*Power(s2,4) + 2*t1*Power(t2,2)*(-2*t1 + 3*t2) - 
               Power(s2,3)*(8*t1 + 23*t2) + 
               s2*t2*(2*Power(t1,2) - 23*t1*t2 - 12*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,2) + 24*t1*t2 + 37*Power(t2,2))) \
+ Power(s1,6)*(6*Power(t1,4)*Power(t2,2)*(-5*t1 + 2*t2) + 
               Power(s2,6)*(6*t1 + 7*t2) + 
               2*s2*Power(t1,3)*t2*
                (45*Power(t1,2) + 7*t1*t2 + 9*Power(t2,2)) - 
               Power(s2,5)*(19*Power(t1,2) + 66*t1*t2 + 
                  30*Power(t2,2)) - 
               Power(s2,2)*Power(t1,2)*
                (35*Power(t1,3) + Power(t1,2)*t2 + 73*t1*Power(t2,2) - 
                  51*Power(t2,3)) + 
               Power(s2,4)*(-33*Power(t1,3) + 179*Power(t1,2)*t2 + 
                  123*t1*Power(t2,2) + 18*Power(t2,3)) - 
               Power(s2,3)*t1*
                (3*Power(t1,3) - 56*Power(t1,2)*t2 + 
                  241*t1*Power(t2,2) + 37*Power(t2,3))) - 
            Power(s1,2)*Power(s2,2)*Power(t2,2)*
             (-4*Power(t1,5)*Power(t2,2) + 2*Power(t1,3)*Power(t2,4) + 
               2*Power(s2,5)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
               s2*Power(t1,2)*t2*
                (3*Power(t1,3) + 16*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  6*Power(t2,3)) - 
               Power(s2,4)*(14*Power(t1,3) + 18*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*t1*
                (Power(t1,4) - 40*Power(t1,3)*t2 - 
                  33*Power(t1,2)*Power(t2,2) - 45*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,3)*(11*Power(t1,4) + 53*Power(t1,3)*t2 + 
                  58*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s1,5)*(2*Power(s2,7)*t2 + 
               8*Power(t1,4)*(2*t1 - t2)*Power(t2,3) + 
               Power(s2,6)*(9*Power(t1,2) - 17*t1*t2 - 
                  28*Power(t2,2)) - 
               s2*Power(t1,3)*Power(t2,2)*
                (96*Power(t1,2) + t1*t2 + 10*Power(t2,2)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (70*Power(t1,3) - 13*Power(t1,2)*t2 + 
                  102*t1*Power(t2,2) - 53*Power(t2,3)) + 
               Power(s2,5)*(-46*Power(t1,3) + 65*Power(t1,2)*t2 + 
                  117*t1*Power(t2,2) + 58*Power(t2,3)) + 
               Power(s2,4)*(27*Power(t1,4) + 65*Power(t1,3)*t2 - 
                  238*Power(t1,2)*Power(t2,2) - 151*t1*Power(t2,3) - 
                  22*Power(t2,4)) + 
               Power(s2,3)*t1*
                (-33*Power(t1,4) + 69*Power(t1,3)*t2 - 
                  162*Power(t1,2)*Power(t2,2) + 252*t1*Power(t2,3) + 
                  37*Power(t2,4))) + 
            Power(s1,3)*s2*t2*
             (2*Power(t1,4)*Power(t2,3)*(-3*t1 + 2*t2) + 
               Power(s2,6)*(-3*Power(t1,2) - 4*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,5)*(16*Power(t1,3) - 5*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 28*Power(t2,3)) - 
               2*s2*Power(t1,2)*Power(t2,2)*
                (3*Power(t1,3) + 5*Power(t1,2)*t2 - 9*t1*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,2)*t1*t2*
                (-11*Power(t1,4) + 115*Power(t1,3)*t2 - 
                  88*Power(t1,2)*Power(t2,2) + 46*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,4)*(-13*Power(t1,4) + 28*Power(t1,3)*t2 + 
                  33*Power(t1,2)*Power(t2,2) + 30*t1*Power(t2,3) + 
                  24*Power(t2,4)) + 
               Power(s2,3)*(12*Power(t1,5) - 77*Power(t1,4)*t2 - 
                  74*Power(t1,2)*Power(t2,3) - 28*t1*Power(t2,4) - 
                  3*Power(t2,5))) + 
            Power(s1,4)*(Power(t1,4)*Power(t2,4)*(-3*t1 + 2*t2) + 
               Power(s2,7)*(5*Power(t1,2) + 2*t1*t2 - 6*Power(t2,2)) + 
               s2*Power(t1,3)*Power(t2,3)*
                (42*Power(t1,2) - 9*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*Power(t1,2)*Power(t2,2)*
                (-33*Power(t1,3) + 23*Power(t1,2)*t2 - 
                  64*t1*Power(t2,2) + 28*Power(t2,3)) + 
               Power(s2,6)*(-19*Power(t1,3) - 6*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 42*Power(t2,3)) + 
               Power(s2,5)*(19*Power(t1,4) + 55*Power(t1,3)*t2 - 
                  54*Power(t1,2)*Power(t2,2) - 91*t1*Power(t2,3) - 
                  54*Power(t2,4)) + 
               Power(s2,3)*t1*t2*
                (46*Power(t1,4) - 163*Power(t1,3)*t2 + 
                  197*Power(t1,2)*Power(t2,2) - 146*t1*Power(t2,3) - 
                  19*Power(t2,4)) + 
               Power(s2,4)*(-13*Power(t1,5) + 4*Power(t1,4)*t2 - 
                  44*Power(t1,3)*Power(t2,2) + 
                  145*Power(t1,2)*Power(t2,3) + 94*t1*Power(t2,4) + 
                  13*Power(t2,5)))) + 
         Power(s,3)*(Power(s1,6)*
             (2*Power(s2,4)*(8*t1 + 3*t2) + 
               s2*Power(t1,2)*
                (15*Power(t1,2) + 35*t1*t2 - 8*Power(t2,2)) - 
               5*Power(t1,3)*(7*Power(t1,2) - 6*t1*t2 + Power(t2,2)) - 
               2*Power(s2,3)*
                (31*Power(t1,2) + 20*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(20*Power(t1,3) + 27*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) + Power(t2,3))) - 
            s1*Power(s2,2)*Power(t1,2)*t2*
             (Power(s2,4)*(4*t1 + t2) - 
               Power(s2,3)*t2*(10*t1 + 49*t2) + 
               2*Power(s2,2)*t2*
                (Power(t1,2) - 13*t1*t2 + 58*Power(t2,2)) + 
               s2*t2*(-11*Power(t1,3) + 30*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) - 55*Power(t2,3)) + 
               Power(t2,2)*(12*Power(t1,3) - 21*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (2*Power(s2,3)*(7*t1 - 12*t2)*t2 + 
               t1*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s2,4)*(-2*t1 + 5*t2) - 
               4*s2*t2*(Power(t1,3) - 3*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(2*Power(t1,3) - 9*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) + 22*Power(t2,3))) + 
            Power(s1,5)*(5*Power(s2,5)*(3*t1 + 2*t2) + 
               16*Power(t1,3)*t2*
                (5*Power(t1,2) - 5*t1*t2 + Power(t2,2)) - 
               Power(s2,4)*(71*Power(t1,2) + 110*t1*t2 + 
                  45*Power(t2,2)) + 
               s2*Power(t1,2)*
                (-100*Power(t1,3) + 30*Power(t1,2)*t2 - 
                  88*t1*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s2,3)*(28*Power(t1,3) + 204*Power(t1,2)*t2 + 
                  113*t1*Power(t2,2) + 32*Power(t2,3)) + 
               Power(s2,2)*(10*Power(t1,4) + 29*Power(t1,3)*t2 - 
                  99*Power(t1,2)*Power(t2,2) - 16*t1*Power(t2,3) - 
                  3*Power(t2,4))) + 
            Power(s1,4)*(Power(s2,6)*(2*t1 + 3*t2) - 
               6*Power(t1,3)*Power(t2,2)*
                (10*Power(t1,2) - 12*t1*t2 + 3*Power(t2,2)) - 
               Power(s2,5)*(7*Power(t1,2) + 48*t1*t2 + 
                  44*Power(t2,2)) + 
               2*s2*Power(t1,2)*t2*
                (90*Power(t1,3) - 68*Power(t1,2)*t2 + 
                  45*t1*Power(t2,2) - 9*Power(t2,3)) + 
               Power(s2,4)*(-33*Power(t1,3) + 161*Power(t1,2)*t2 + 
                  153*t1*Power(t2,2) + 96*Power(t2,3)) + 
               Power(s2,3)*(30*Power(t1,4) - 101*Power(t1,3)*t2 - 
                  213*Power(t1,2)*Power(t2,2) - 120*t1*Power(t2,3) - 
                  44*Power(t2,4)) + 
               Power(s2,2)*(-70*Power(t1,5) + 123*Power(t1,4)*t2 - 
                  152*Power(t1,3)*Power(t2,2) + 
                  131*Power(t1,2)*Power(t2,3) + 21*t1*Power(t2,4) + 
                  3*Power(t2,5))) + 
            Power(s1,2)*(-(Power(t1,3)*Power(t1 - t2,2)*Power(t2,4)) + 
               s2*Power(t1,3)*Power(t2,3)*
                (14*Power(t1,2) - 27*t1*t2 + 13*Power(t2,2)) + 
               Power(s2,6)*(-4*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,5)*(11*Power(t1,4) - 34*Power(t1,3)*t2 - 
                  24*Power(t1,2)*Power(t2,2) + 11*t1*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(s2,2)*t1*Power(t2,2)*
                (14*Power(t1,4) + 46*Power(t1,3)*t2 - 
                  50*Power(t1,2)*Power(t2,2) + 21*t1*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s2,3)*t2*
                (-12*Power(t1,5) + 101*Power(t1,4)*t2 - 
                  51*Power(t1,3)*Power(t2,2) + 
                  120*Power(t1,2)*Power(t2,3) + 9*t1*Power(t2,4) + 
                  4*Power(t2,5)) + 
               Power(s2,4)*(-7*Power(t1,5) + 7*Power(t1,4)*t2 + 
                  86*Power(t1,3)*Power(t2,2) + 
                  113*Power(t1,2)*Power(t2,3) + 5*t1*Power(t2,4) + 
                  22*Power(t2,5))) + 
            Power(s1,3)*(Power(s2,6)*
                (4*Power(t1,2) - t1*t2 - 8*Power(t2,2)) + 
               8*Power(t1,3)*Power(t2,3)*
                (2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (-96*Power(t1,3) + 117*Power(t1,2)*t2 - 
                  50*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,5)*(-36*Power(t1,3) + 31*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) + 58*Power(t2,3)) + 
               Power(s2,4)*(53*Power(t1,4) - 100*Power(t1,3)*t2 - 
                  94*Power(t1,2)*Power(t2,2) - 65*t1*Power(t2,3) - 
                  79*Power(t2,4)) - 
               Power(s2,2)*t2*
                (-56*Power(t1,5) + 200*Power(t1,4)*t2 - 
                  158*Power(t1,3)*Power(t2,2) + 
                  76*Power(t1,2)*Power(t2,3) + 10*t1*Power(t2,4) + 
                  Power(t2,5)) + 
               Power(s2,3)*(-38*Power(t1,5) + 117*Power(t1,4)*t2 + 
                  36*Power(t1,3)*Power(t2,2) + 
                  143*Power(t1,2)*Power(t2,3) + 56*t1*Power(t2,4) + 
                  24*Power(t2,5)))) - 
         Power(s,2)*(Power(s1,7)*
             (2*Power(s2,4)*(7*t1 + 2*t2) + 
               s2*Power(t1,2)*
                (6*Power(t1,2) + 19*t1*t2 - 2*Power(t2,2)) - 
               Power(t1,3)*(21*Power(t1,2) - 12*t1*t2 + Power(t2,2)) - 
               2*Power(s2,3)*(24*Power(t1,2) + 10*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*t1*(7*Power(t1,2) + 19*t1*t2 + Power(t2,2))) + 
            Power(s2,3)*Power(t1,2)*Power(t2,3)*
             (Power(s2,4) + Power(s2,3)*(3*t1 - 11*t2) - 
               3*Power(s2,2)*(Power(t1,2) + t1*t2 - 6*Power(t2,2)) + 
               t1*t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) - 
               s2*(Power(t1,3) - 9*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
                  6*Power(t2,3))) + 
            Power(s1,6)*(3*Power(s2,5)*(7*t1 + 4*t2) + 
               4*Power(t1,3)*t2*
                (15*Power(t1,2) - 10*t1*t2 + Power(t2,2)) - 
               Power(s2,4)*(77*Power(t1,2) + 104*t1*t2 + 
                  29*Power(t2,2)) + 
               Power(s2,2)*t1*
                (5*Power(t1,3) + 47*Power(t1,2)*t2 - 
                  82*t1*Power(t2,2) - 5*Power(t2,3)) + 
               s2*Power(t1,2)*
                (-75*Power(t1,3) - 57*t1*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,3)*(2*Power(t1,3) + 215*Power(t1,2)*t2 + 
                  76*t1*Power(t2,2) + 9*Power(t2,3))) + 
            s1*Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (3*t1*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s2,4)*(7*t1 + 17*t2) - 
               2*Power(s2,3)*(Power(t1,2) + 7*t1*t2 + 39*Power(t2,2)) - 
               s2*t2*(5*Power(t1,3) - 25*Power(t1,2)*t2 + 
                  19*t1*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s2,2)*(Power(t1,3) - 10*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) + 69*Power(t2,3))) + 
            Power(s1,5)*(Power(s2,6)*(6*t1 + 8*t2) - 
               6*Power(t1,3)*Power(t2,2)*
                (10*Power(t1,2) - 8*t1*t2 + Power(t2,2)) - 
               Power(s2,5)*(16*Power(t1,2) + 87*t1*t2 + 
                  57*Power(t2,2)) + 
               3*s2*Power(t1,2)*t2*
                (60*Power(t1,3) - 18*Power(t1,2)*t2 + 
                  22*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,4)*(-47*Power(t1,3) + 240*Power(t1,2)*t2 + 
                  215*t1*Power(t2,2) + 70*Power(t2,3)) + 
               Power(s2,3)*(10*Power(t1,4) + 22*Power(t1,3)*t2 - 
                  335*Power(t1,2)*Power(t2,2) - 115*t1*Power(t2,3) - 
                  15*Power(t2,4)) + 
               Power(s2,2)*t1*
                (-70*Power(t1,4) + 58*Power(t1,3)*t2 - 
                  180*Power(t1,2)*Power(t2,2) + 137*t1*Power(t2,3) + 
                  9*Power(t2,4))) + 
            Power(s1,2)*s2*t2*
             (-3*Power(t1,3)*Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,6)*(Power(t1,2) - t1*t2 + Power(t2,2)) - 
               s2*Power(t1,2)*Power(t2,2)*
                (15*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + 3*Power(t2,3)) - 
               Power(s2,5)*(4*Power(t1,3) + 10*Power(t1,2)*t2 - 
                  8*t1*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,2)*t1*t2*
                (Power(t1,4) + 61*Power(t1,3)*t2 - 
                  21*Power(t1,2)*Power(t2,2) + 54*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,4)*(-5*Power(t1,4) + 68*Power(t1,3)*t2 + 
                  66*Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s2,3)*(8*Power(t1,5) - 53*Power(t1,4)*t2 - 
                  53*Power(t1,3)*Power(t2,2) - 
                  129*Power(t1,2)*Power(t2,3) - 9*t1*Power(t2,4) - 
                  6*Power(t2,5))) + 
            Power(s1,4)*(Power(s2,7)*t2 + 
               Power(s2,6)*(13*Power(t1,2) - 10*t1*t2 - 
                  27*Power(t2,2)) + 
               4*Power(t1,3)*Power(t2,3)*
                (6*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (-144*Power(t1,3) + 87*Power(t1,2)*t2 - 
                  40*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,5)*(-61*Power(t1,3) + 58*Power(t1,2)*t2 + 
                  99*t1*Power(t2,2) + 96*Power(t2,3)) + 
               Power(s2,4)*(55*Power(t1,4) - 6*Power(t1,3)*t2 - 
                  212*Power(t1,2)*Power(t2,2) - 185*t1*Power(t2,3) - 
                  75*Power(t2,4)) + 
               Power(s2,2)*t1*t2*
                (99*Power(t1,4) - 159*Power(t1,3)*t2 + 
                  212*Power(t1,2)*Power(t2,2) - 107*t1*Power(t2,3) - 
                  7*Power(t2,4)) + 
               Power(s2,3)*(-52*Power(t1,5) + 136*Power(t1,4)*t2 - 
                  115*Power(t1,3)*Power(t2,2) + 
                  257*Power(t1,2)*Power(t2,3) + 85*t1*Power(t2,4) + 
                  11*Power(t2,5))) + 
            Power(s1,3)*(Power(s2,7)*
                (2*Power(t1,2) + t1*t2 - 2*Power(t2,2)) - 
               Power(t1,3)*Power(t2,4)*
                (3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) - 
               Power(s2,6)*(15*Power(t1,3) + Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) - 30*Power(t2,3)) + 
               s2*Power(t1,2)*Power(t2,3)*
                (42*Power(t1,3) - 45*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - Power(t2,3)) + 
               Power(s2,5)*(22*Power(t1,4) + 5*Power(t1,3)*t2 - 
                  42*Power(t1,2)*Power(t2,2) - 30*t1*Power(t2,3) - 
                  69*Power(t2,4)) + 
               Power(s2,2)*t1*Power(t2,2)*
                (-18*Power(t1,4) + 109*Power(t1,3)*t2 - 
                  103*Power(t1,2)*Power(t2,2) + 36*t1*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s2,3)*t2*
                (-46*Power(t1,5) + 232*Power(t1,4)*t2 - 
                  129*Power(t1,3)*Power(t2,2) + 
                  131*Power(t1,2)*Power(t2,3) + 29*t1*Power(t2,4) + 
                  3*Power(t2,5)) + 
               Power(s2,4)*(-15*Power(t1,5) + 12*Power(t1,4)*t2 + 
                  95*Power(t1,3)*Power(t2,2) + 
                  117*Power(t1,2)*Power(t2,3) + 69*t1*Power(t2,4) + 
                  36*Power(t2,5)))))*lg2(s - s2 + t1))/
     (Power(s1,3)*Power(s2,3)*Power(t1,3)*(s - s2 + t1)*t2*
       Power(s - s1 + t2,3)*(s - s1 - s2 + t2)) + 
    (8*(Power(s1,4)*(s2 - t1)*t1*(s2 - t1 + t2) + 
         Power(s,4)*(Power(t1,3) + t1*Power(t2,2) + 2*Power(t2,3)) + 
         Power(s1,2)*t2*(Power(s2,4) - 3*Power(t1,2)*Power(t2,2) + 
            s2*Power(t1,2)*(-2*t1 + t2) + 3*Power(s2,2)*t1*(2*t1 + t2) - 
            Power(s2,3)*(5*t1 + 3*t2)) + 
         Power(s1,3)*(-(Power(t1,2)*(t1 - 3*t2)*t2) + 
            Power(s2,3)*(t1 + 2*t2) - Power(s2,2)*t1*(2*t1 + 5*t2) + 
            s2*t1*(Power(t1,2) + 4*t1*t2 - 2*Power(t2,2))) + 
         s1*Power(t2,2)*(-2*Power(s2,4) + Power(s2,2)*t1*(-7*t1 + t2) + 
            Power(t1,2)*t2*(-t1 + t2) + Power(s2,3)*(7*t1 + t2) + 
            2*s2*t1*(Power(t1,2) - t1*t2 + Power(t2,2))) + 
         s2*Power(t2,3)*(Power(s2,3) - 3*Power(s2,2)*t1 + 
            s2*t1*(4*t1 - t2) - t1*(2*Power(t1,2) - t1*t2 + Power(t2,2))) \
+ Power(s,3)*(t1*t2*(Power(t1,2) + 7*Power(t2,2)) - 
            s2*(Power(t1,3) + Power(t1,2)*t2 + 6*Power(t2,3)) + 
            s1*(-4*Power(t1,3) + Power(t1,2)*t2 - 3*t1*Power(t2,2) - 
               2*Power(t2,3) + 
               2*s2*(Power(t1,2) + 2*t1*t2 + 2*Power(t2,2)))) - 
         s*(Power(s1,3)*(2*Power(s2,2)*(t1 + t2) - 
               2*s2*t1*(3*t1 + t2) + 
               t1*(4*Power(t1,2) - 3*t1*t2 + Power(t2,2))) + 
            Power(t2,2)*(Power(s2,3)*(t1 + 4*t2) - 
               Power(s2,2)*t1*(3*t1 + 7*t2) - 
               t1*t2*(Power(t1,2) + Power(t2,2)) + 
               s2*t1*(2*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(-3*t1*Power(t1 - t2,2)*t2 + 
               Power(s2,3)*(t1 + 3*t2) + 
               s2*t1*(3*Power(t1,2) + 9*t1*t2 + 4*Power(t2,2)) - 
               Power(s2,2)*(4*Power(t1,2) + 13*t1*t2 + 7*Power(t2,2))) + 
            s1*t2*(3*t1*Power(t2,3) - Power(s2,3)*(4*t1 + 7*t2) + 
               Power(s2,2)*(7*Power(t1,2) + 10*t1*t2 + 4*Power(t2,2)) - 
               s2*(4*Power(t1,3) + 7*t1*Power(t2,2)))) + 
         Power(s,2)*(Power(s1,2)*
             (Power(s2,2)*(t1 + 2*t2) + 
               3*t1*(2*Power(t1,2) - t1*t2 + Power(t2,2)) - 
               s2*(6*Power(t1,2) + 7*t1*t2 + 4*Power(t2,2))) + 
            t2*(3*t1*Power(t2,2)*(t1 + t2) + 
               Power(s2,2)*(Power(t1,2) + 7*Power(t2,2)) - 
               s2*t1*(2*Power(t1,2) + t1*t2 + 11*Power(t2,2))) + 
            s1*(t1*t2*(-3*Power(t1,2) + 3*t1*t2 - 10*Power(t2,2)) - 
               Power(s2,2)*(2*Power(t1,2) + 8*t1*t2 + 9*Power(t2,2)) + 
               s2*(3*Power(t1,3) + 6*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
                  5*Power(t2,3)))))*lg2(-(s1/(s*(-s + s1 - t2)))))/
     (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*Power(t2,3)) + 
    (8*(Power(s1,10)*(Power(s2,5) + 3*Power(s2,3)*Power(t1,2) + 
            3*Power(s2,2)*Power(t1,3) + Power(t1,5)) - 
         Power(s,7)*t1*(Power(s1,3)*
             (-(s2*Power(t1,2)) + Power(s2,2)*(t1 - t2) + 
               Power(t1,2)*(t1 - t2)) + 
            Power(s1,2)*t1*(-(s2*Power(t1,2)) + 
               Power(t1,2)*(t1 - t2) + Power(s2,2)*(3*t1 - t2)) + 
            s1*Power(s2,2)*Power(t1,2)*(3*t1 - 2*t2) + 
            Power(s2,2)*Power(t1,3)*(t1 - t2))*(t1 - t2) + 
         Power(s2,5)*Power(t1,4)*Power(t2,5)*(-s2 + t2) + 
         Power(s1,9)*(2*Power(s2,6) + 
            Power(s2,3)*Power(t1,2)*(15*t1 - 16*t2) + 
            Power(s2,2)*Power(t1,3)*(7*t1 - 15*t2) + 
            Power(s2,5)*(4*t1 - 5*t2) + Power(t1,5)*(t1 - 4*t2) + 
            3*Power(s2,4)*t1*(2*t1 - t2) + s2*Power(t1,4)*(5*t1 + 3*t2)) \
+ s1*Power(s2,4)*Power(t1,3)*Power(t2,4)*
          (Power(s2,3) + Power(s2,2)*(6*t1 - 3*t2) + 
            t1*t2*(2*t1 + 3*t2) - 
            2*s2*(Power(t1,2) + 5*t1*t2 - Power(t2,2))) + 
         Power(s1,8)*(Power(s2,7) + Power(s2,6)*(7*t1 - 9*t2) + 
            2*Power(t1,5)*t2*(-2*t1 + 3*t2) + 
            s2*Power(t1,4)*(5*Power(t1,2) - 15*t1*t2 - 11*Power(t2,2)) + 
            2*Power(s2,4)*t1*
             (8*Power(t1,2) - 14*t1*t2 + 7*Power(t2,2)) + 
            Power(s2,5)*(11*Power(t1,2) - 24*t1*t2 + 10*Power(t2,2)) + 
            Power(s2,2)*Power(t1,3)*
             (13*Power(t1,2) - 27*t1*t2 + 31*Power(t2,2)) + 
            Power(s2,3)*Power(t1,2)*
             (27*Power(t1,2) - 73*t1*t2 + 36*Power(t2,2))) + 
         Power(s1,7)*(Power(s2,7)*(3*t1 - 4*t2) + 
            2*Power(t1,5)*(3*t1 - 2*t2)*Power(t2,2) + 
            s2*Power(t1,4)*t2*
             (-18*Power(t1,2) + 13*t1*t2 + 15*Power(t2,2)) + 
            Power(s2,6)*(12*Power(t1,2) - 31*t1*t2 + 16*Power(t2,2)) + 
            Power(s2,3)*Power(t1,2)*
             (28*Power(t1,3) - 103*Power(t1,2)*t2 + 
               157*t1*Power(t2,2) - 44*Power(t2,3)) + 
            Power(s2,2)*Power(t1,3)*
             (9*Power(t1,3) - 40*Power(t1,2)*t2 + 45*t1*Power(t2,2) - 
               34*Power(t2,3)) + 
            Power(s2,4)*t1*(15*Power(t1,3) - 79*Power(t1,2)*t2 + 
               59*t1*Power(t2,2) - 26*Power(t2,3)) + 
            Power(s2,5)*(13*Power(t1,3) - 45*Power(t1,2)*t2 + 
               56*t1*Power(t2,2) - 10*Power(t2,3))) + 
         Power(s1,2)*Power(s2,3)*Power(t1,2)*Power(t2,3)*
          (Power(s2,4)*(-4*t1 + t2) + 
            Power(s2,3)*(-15*Power(t1,2) + 18*t1*t2 - 2*Power(t2,2)) + 
            Power(t1,2)*t2*(Power(t1,2) + 3*Power(t2,2)) - 
            s2*t1*(Power(t1,3) + 8*Power(t1,2)*t2 + 21*t1*Power(t2,2) - 
               5*Power(t2,3)) + 
            Power(s2,2)*(7*Power(t1,3) + 35*Power(t1,2)*t2 - 
               20*t1*Power(t2,2) + Power(t2,3))) + 
         Power(s1,3)*Power(s2,2)*t1*Power(t2,2)*
          (Power(t1,3)*Power(t2,2)*(3*Power(t1,2) + Power(t2,2)) + 
            2*Power(s2,5)*(3*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
            Power(s2,4)*(20*Power(t1,3) - 45*Power(t1,2)*t2 + 
               14*t1*Power(t2,2) - 3*Power(t2,3)) + 
            2*s2*Power(t1,2)*t2*
             (-5*Power(t1,3) + 4*Power(t1,2)*t2 - 13*t1*Power(t2,2) + 
               3*Power(t2,3)) + 
            Power(s2,3)*(-9*Power(t1,4) - 56*Power(t1,3)*t2 + 
               75*Power(t1,2)*Power(t2,2) - 11*t1*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,2)*t1*(5*Power(t1,4) + 4*Power(t1,3)*t2 + 
               65*Power(t1,2)*Power(t2,2) - 42*t1*Power(t2,3) + 
               3*Power(t2,4))) + 
         Power(s1,6)*(Power(t1,5)*Power(t2,3)*(-4*t1 + t2) + 
            s2*Power(t1,4)*Power(t2,2)*
             (24*Power(t1,2) + t1*t2 - 9*Power(t2,2)) + 
            Power(s2,7)*(3*Power(t1,2) - 11*t1*t2 + 6*Power(t2,2)) + 
            2*Power(s2,6)*(6*Power(t1,3) - 21*Power(t1,2)*t2 + 
               27*t1*Power(t2,2) - 7*Power(t2,3)) + 
            Power(s2,2)*Power(t1,3)*t2*
             (-29*Power(t1,3) + 44*Power(t1,2)*t2 - 43*t1*Power(t2,2) + 
               21*Power(t2,3)) + 
            Power(s2,5)*(-2*Power(t1,4) - 64*Power(t1,3)*t2 + 
               79*Power(t1,2)*Power(t2,2) - 65*t1*Power(t2,3) + 
               5*Power(t2,4)) + 
            Power(s2,4)*t1*(16*Power(t1,4) - 53*Power(t1,3)*t2 + 
               175*Power(t1,2)*Power(t2,2) - 72*t1*Power(t2,3) + 
               24*Power(t2,4)) + 
            Power(s2,3)*Power(t1,2)*
             (11*Power(t1,4) - 81*Power(t1,3)*t2 + 
               174*Power(t1,2)*Power(t2,2) - 187*t1*Power(t2,3) + 
               31*Power(t2,4))) + 
         Power(s1,5)*(Power(t1,6)*Power(t2,4) - 
            2*s2*Power(t1,4)*Power(t2,3)*
             (7*Power(t1,2) + 3*t1*t2 - Power(t2,2)) + 
            Power(s2,2)*Power(t1,3)*Power(t2,2)*
             (34*Power(t1,3) - 20*Power(t1,2)*t2 + 25*t1*Power(t2,2) - 
               7*Power(t2,3)) + 
            Power(s2,7)*(Power(t1,3) - 10*Power(t1,2)*t2 + 
               15*t1*Power(t2,2) - 4*Power(t2,3)) + 
            Power(s2,3)*Power(t1,2)*t2*
             (-29*Power(t1,4) + 89*Power(t1,3)*t2 - 
               165*Power(t1,2)*Power(t2,2) + 126*t1*Power(t2,3) - 
               12*Power(t2,4)) + 
            Power(s2,6)*(3*Power(t1,4) - 43*Power(t1,3)*t2 + 
               58*Power(t1,2)*Power(t2,2) - 46*t1*Power(t2,3) + 
               6*Power(t2,4)) + 
            Power(s2,4)*t1*(5*Power(t1,5) - 31*Power(t1,4)*t2 + 
               100*Power(t1,3)*Power(t2,2) - 
               208*Power(t1,2)*Power(t2,3) + 52*t1*Power(t2,4) - 
               11*Power(t2,5)) - 
            Power(s2,5)*(Power(t1,5) + 7*Power(t1,4)*t2 - 
               130*Power(t1,3)*Power(t2,2) + 
               76*Power(t1,2)*Power(t2,3) - 39*t1*Power(t2,4) + 
               Power(t2,5))) + 
         Power(s1,4)*s2*t2*(Power(t1,5)*Power(t2,3)*(3*t1 + 2*t2) + 
            Power(s2,6)*(-4*Power(t1,3) + 12*Power(t1,2)*t2 - 
               9*t1*Power(t2,2) + Power(t2,3)) + 
            s2*Power(t1,3)*Power(t2,2)*
             (-17*Power(t1,3) + 3*Power(t1,2)*t2 - 8*t1*Power(t2,2) + 
               Power(t2,3)) + 
            Power(s2,4)*t1*(6*Power(t1,4) + 40*Power(t1,3)*t2 - 
               136*Power(t1,2)*Power(t2,2) + 41*t1*Power(t2,3) - 
               11*Power(t2,4)) - 
            Power(s2,5)*(13*Power(t1,4) - 61*Power(t1,3)*t2 + 
               40*Power(t1,2)*Power(t2,2) - 19*t1*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,2)*Power(t1,2)*t2*
             (27*Power(t1,4) - 44*Power(t1,3)*t2 + 
               90*Power(t1,2)*Power(t2,2) - 44*t1*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,3)*t1*(-9*Power(t1,5) + 18*Power(t1,4)*t2 - 
               108*Power(t1,3)*Power(t2,2) + 
               133*Power(t1,2)*Power(t2,3) - 20*t1*Power(t2,4) + 
               2*Power(t2,5))) + 
         Power(s,6)*(2*Power(s2,2)*Power(t1,4)*(t1 - t2)*
             (s2*(t1 - 2*t2) + 2*t2*(-t1 + t2)) + 
            Power(s1,4)*(Power(s2,3)*
                (Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*Power(t1,2)*
                (-6*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(7*Power(t1,2) - 12*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*t1*(8*Power(t1,2) - 13*t1*t2 + 6*Power(t2,2))\
) + s1*Power(s2,2)*Power(t1,3)*
             (7*Power(t1,3) - 26*Power(t1,2)*t2 + 27*t1*Power(t2,2) - 
               8*Power(t2,3) + 
               s2*(7*Power(t1,2) - 19*t1*t2 + 9*Power(t2,2))) + 
            Power(s1,3)*t1*(6*Power(s2,3)*Power(t1 - t2,2) - 
               s2*Power(t1,2)*(Power(t1,2) + 5*t1*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*(7*Power(t1,3) - 16*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,2)*(21*Power(t1,3) - 28*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - 4*Power(t2,3))) + 
            Power(s1,2)*Power(t1,2)*
             (-4*Power(t1,2)*Power(t1 - t2,2)*t2 + 
               s2*Power(t1,2)*(5*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(8*Power(t1,2) - 18*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(20*Power(t1,3) - 43*Power(t1,2)*t2 + 
                  29*t1*Power(t2,2) - 4*Power(t2,3)))) - 
         Power(s,5)*(Power(s2,2)*Power(t1,4)*
             (6*Power(t1 - t2,2)*Power(t2,2) + 
               s2*t2*(-7*Power(t1,2) + 22*t1*t2 - 15*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) - 6*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,5)*(Power(s2,4)*(t1 - 2*t2) + 
               Power(s2,3)*(3*Power(t1,2) - 20*t1*t2 + 4*Power(t2,2)) + 
               2*Power(s2,2)*t1*
                (14*Power(t1,2) - 20*t1*t2 + 7*Power(t2,2)) + 
               s2*Power(t1,2)*
                (-15*Power(t1,2) - 10*t1*t2 + 8*Power(t2,2)) + 
               Power(t1,3)*(21*Power(t1,2) - 30*t1*t2 + 10*Power(t2,2))) \
+ Power(s1,4)*(Power(s2,4)*(4*Power(t1,2) - 17*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*t1*
                (67*Power(t1,3) - 117*Power(t1,2)*t2 + 
                  71*t1*Power(t2,2) - 20*Power(t2,3)) + 
               Power(t1,3)*(21*Power(t1,3) - 54*Power(t1,2)*t2 + 
                  50*t1*Power(t2,2) - 16*Power(t2,3)) + 
               s2*Power(t1,2)*
                (15*Power(t1,3) - 37*Power(t1,2)*t2 + 
                  26*t1*Power(t2,2) - 7*Power(t2,3)) + 
               Power(s2,3)*(26*Power(t1,3) - 67*Power(t1,2)*t2 + 
                  44*t1*Power(t2,2) - 3*Power(t2,3))) + 
            s1*Power(s2,2)*Power(t1,3)*
             (Power(s2,2)*(5*Power(t1,2) - 27*t1*t2 + 16*Power(t2,2)) + 
               s2*(11*Power(t1,3) - 58*Power(t1,2)*t2 + 
                  94*t1*Power(t2,2) - 33*Power(t2,3)) + 
               6*t2*(-4*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,2)*Power(t1,2)*
             (6*Power(t1,2)*Power(t1 - t2,2)*Power(t2,2) - 
               6*s2*Power(t1,2)*t2*
                (3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) + 
               2*Power(s2,4)*
                (4*Power(t1,2) - 16*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(37*Power(t1,3) - 132*Power(t1,2)*t2 + 
                  117*t1*Power(t2,2) - 18*Power(t2,3)) + 
               Power(s2,2)*(29*Power(t1,4) - 130*Power(t1,3)*t2 + 
                  153*Power(t1,2)*Power(t2,2) - 68*t1*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,3)*t1*(Power(s2,4)*
                (13*Power(t1,2) - 28*t1*t2 + 14*Power(t2,2)) + 
               Power(s2,3)*(37*Power(t1,3) - 120*Power(t1,2)*t2 + 
                  71*t1*Power(t2,2) - 21*Power(t2,3)) + 
               s2*Power(t1,2)*
                (30*Power(t1,3) - 45*Power(t1,2)*t2 + 
                  48*t1*Power(t2,2) - 19*Power(t2,3)) + 
               2*Power(t1,2)*t2*
                (-12*Power(t1,3) + 23*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*(68*Power(t1,4) - 199*Power(t1,3)*t2 + 
                  141*Power(t1,2)*Power(t2,2) - 36*t1*Power(t2,3) + 
                  6*Power(t2,4)))) + 
         Power(s,4)*(Power(s2,2)*Power(t1,4)*t2*
             (-2*Power(s2,3)*(t1 - 2*t2) - 
               4*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s2,2)*(-3*Power(t1,2) + 20*t1*t2 - 
                  21*Power(t2,2)) + 
               3*s2*t2*(3*Power(t1,2) - 10*t1*t2 + 7*Power(t2,2))) + 
            Power(s1,6)*(Power(s2,5) + 4*Power(s2,4)*(t1 - 2*t2) - 
               2*s2*Power(t1,2)*
                (10*Power(t1,2) + 15*t1*t2 - 6*Power(t2,2)) + 
               5*Power(t1,3)*
                (7*Power(t1,2) - 8*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(5*Power(t1,2) - 40*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*t1*
                (55*Power(t1,2) - 70*t1*t2 + 16*Power(t2,2))) + 
            Power(s1,5)*(Power(s2,5)*(5*t1 - 6*t2) + 
               Power(s2,4)*(12*Power(t1,2) - 75*t1*t2 + 
                  20*Power(t2,2)) + 
               Power(s2,2)*t1*
                (125*Power(t1,3) - 283*Power(t1,2)*t2 + 
                  168*t1*Power(t2,2) - 36*Power(t2,3)) + 
               Power(t1,3)*(35*Power(t1,3) - 100*Power(t1,2)*t2 + 
                  90*t1*Power(t2,2) - 24*Power(t2,3)) + 
               s2*Power(t1,2)*
                (55*Power(t1,3) - 75*Power(t1,2)*t2 + 
                  74*t1*Power(t2,2) - 21*Power(t2,3)) + 
               Power(s2,3)*(59*Power(t1,3) - 157*Power(t1,2)*t2 + 
                  115*t1*Power(t2,2) - 9*Power(t2,3))) + 
            s1*Power(s2,2)*Power(t1,3)*
             (Power(s2,3)*(Power(t1,2) - 17*t1*t2 + 14*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,3) - 40*Power(t1,2)*t2 + 
                  121*t1*Power(t2,2) - 53*Power(t2,3)) + 
               2*Power(t2,2)*
                (15*Power(t1,3) - 36*Power(t1,2)*t2 + 
                  25*t1*Power(t2,2) - 4*Power(t2,3)) + 
               s2*t2*(-30*Power(t1,3) + 130*Power(t1,2)*t2 - 
                  171*t1*Power(t2,2) + 45*Power(t2,3))) + 
            Power(s1,2)*Power(t1,2)*
             (-4*Power(t1,2)*Power(t1 - t2,2)*Power(t2,3) + 
               2*Power(s2,5)*
                (2*Power(t1,2) - 14*t1*t2 + 5*Power(t2,2)) + 
               4*s2*Power(t1,2)*Power(t2,2)*
                (6*Power(t1,2) - 11*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,4)*(17*Power(t1,3) - 147*Power(t1,2)*t2 + 
                  183*t1*Power(t2,2) - 32*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-85*Power(t1,4) + 268*Power(t1,3)*t2 - 
                  229*Power(t1,2)*Power(t2,2) + 70*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,3)*(35*Power(t1,4) - 200*Power(t1,3)*t2 + 
                  419*Power(t1,2)*Power(t2,2) - 244*t1*Power(t2,3) + 
                  24*Power(t2,4))) + 
            Power(s1,4)*(Power(s2,5)*
                (8*Power(t1,2) - 27*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,4)*(42*Power(t1,3) - 132*Power(t1,2)*t2 + 
                  111*t1*Power(t2,2) - 11*Power(t2,3)) + 
               2*Power(t1,3)*t2*
                (-30*Power(t1,3) + 55*Power(t1,2)*t2 - 
                  36*t1*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,3)*(95*Power(t1,4) - 348*Power(t1,3)*t2 + 
                  265*Power(t1,2)*Power(t2,2) - 103*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               3*s2*Power(t1,2)*
                (25*Power(t1,4) - 45*Power(t1,3)*t2 + 
                  57*Power(t1,2)*Power(t2,2) - 25*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,2)*t1*
                (145*Power(t1,4) - 493*Power(t1,3)*t2 + 
                  417*Power(t1,2)*Power(t2,2) - 131*t1*Power(t2,3) + 
                  24*Power(t2,4))) + 
            Power(s1,3)*t1*(Power(s2,5)*
                (13*Power(t1,2) - 32*t1*t2 + 16*Power(t2,2)) + 
               Power(s2,4)*(17*Power(t1,3) - 189*Power(t1,2)*t2 + 
                  133*t1*Power(t2,2) - 43*Power(t2,3)) + 
               2*Power(t1,2)*Power(t2,2)*
                (15*Power(t1,3) - 26*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - 2*Power(t2,3)) + 
               s2*Power(t1,2)*t2*
                (-90*Power(t1,3) + 133*Power(t1,2)*t2 - 
                  98*t1*Power(t2,2) + 29*Power(t2,3)) + 
               Power(s2,3)*(106*Power(t1,4) - 397*Power(t1,3)*t2 + 
                  473*Power(t1,2)*Power(t2,2) - 141*t1*Power(t2,3) + 
                  27*Power(t2,4)) + 
               Power(s2,2)*(75*Power(t1,5) - 340*Power(t1,4)*t2 + 
                  533*Power(t1,3)*Power(t2,2) - 
                  260*Power(t1,2)*Power(t2,3) + 38*t1*Power(t2,4) - 
                  4*Power(t2,5)))) - 
         Power(s,3)*(Power(s1,7)*
             (4*Power(s2,5) + 6*Power(s2,4)*(t1 - 2*t2) + 
               5*Power(t1,3)*(7*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               2*Power(s2,3)*
                (5*Power(t1,2) - 20*t1*t2 + 2*Power(t2,2)) + 
               s2*Power(t1,2)*
                (-15*Power(t1,2) - 35*t1*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*t1*
                (65*Power(t1,2) - 70*t1*t2 + 9*Power(t2,2))) + 
            Power(s2,2)*Power(t1,4)*Power(t2,2)*
             (Power(s2,4) + Power(s2,3)*(6*t1 - 13*t2) + 
               Power(t1 - t2,2)*Power(t2,2) + 
               s2*t2*(-5*Power(t1,2) + 18*t1*t2 - 13*Power(t2,2)) + 
               3*Power(s2,2)*(Power(t1,2) - 8*t1*t2 + 9*Power(t2,2))) + 
            Power(s1,6)*(2*Power(s2,6) + Power(s2,5)*(19*t1 - 23*t2) + 
               18*Power(s2,4)*(Power(t1,2) - 7*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*t1*
                (145*Power(t1,3) - 382*Power(t1,2)*t2 + 
                  213*t1*Power(t2,2) - 28*Power(t2,3)) + 
               s2*Power(t1,2)*
                (85*Power(t1,3) - 65*Power(t1,2)*t2 + 
                  96*t1*Power(t2,2) - 21*Power(t2,3)) + 
               Power(t1,3)*(35*Power(t1,3) - 110*Power(t1,2)*t2 + 
                  85*t1*Power(t2,2) - 16*Power(t2,3)) + 
               Power(s2,3)*(96*Power(t1,3) - 205*Power(t1,2)*t2 + 
                  141*t1*Power(t2,2) - 9*Power(t2,3))) + 
            s1*Power(s2,2)*Power(t1,3)*t2*
             (Power(s2,4)*(-4*t1 + 6*t2) + 
               Power(s2,3)*(-8*Power(t1,2) + 68*t1*t2 - 
                  41*Power(t2,2)) + 
               s2*t2*(27*Power(t1,3) - 118*Power(t1,2)*t2 + 
                  142*t1*Power(t2,2) - 27*Power(t2,3)) + 
               Power(t2,2)*(-16*Power(t1,3) + 35*Power(t1,2)*t2 - 
                  21*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(-8*Power(t1,3) + 75*Power(t1,2)*t2 - 
                  196*t1*Power(t2,2) + 64*Power(t2,3))) + 
            Power(s1,5)*(Power(s2,6)*(7*t1 - 6*t2) + 
               Power(s2,5)*(27*Power(t1,2) - 108*t1*t2 + 
                  34*Power(t2,2)) + 
               Power(s2,4)*(64*Power(t1,3) - 241*Power(t1,2)*t2 + 
                  269*t1*Power(t2,2) - 34*Power(t2,3)) + 
               2*Power(t1,3)*t2*
                (-40*Power(t1,3) + 70*Power(t1,2)*t2 - 
                  44*t1*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,3)*(170*Power(t1,4) - 577*Power(t1,3)*t2 + 
                  456*Power(t1,2)*Power(t2,2) - 180*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s2*Power(t1,2)*
                (100*Power(t1,4) - 210*Power(t1,3)*t2 + 
                  224*Power(t1,2)*Power(t2,2) - 111*t1*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s2,2)*t1*
                (195*Power(t1,4) - 667*Power(t1,3)*t2 + 
                  696*Power(t1,2)*Power(t2,2) - 230*t1*Power(t2,3) + 
                  30*Power(t2,4))) + 
            Power(s1,2)*Power(t1,2)*
             (Power(t1,2)*Power(t1 - t2,2)*Power(t2,4) + 
               s2*Power(t1,2)*Power(t2,3)*
                (-14*Power(t1,2) + 27*t1*t2 - 13*Power(t2,2)) + 
               Power(s2,6)*(Power(t1,2) - 12*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,5)*(Power(t1,3) - 73*Power(t1,2)*t2 + 
                  137*t1*Power(t2,2) - 28*Power(t2,3)) + 
               Power(s2,3)*t2*
                (-77*Power(t1,4) + 318*Power(t1,3)*t2 - 
                  526*Power(t1,2)*Power(t2,2) + 219*t1*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s2,2)*Power(t2,2)*
                (88*Power(t1,4) - 238*Power(t1,3)*t2 + 
                  161*Power(t1,2)*Power(t2,2) - 32*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,4)*(11*Power(t1,4) - 85*Power(t1,3)*t2 + 
                  417*Power(t1,2)*Power(t2,2) - 330*t1*Power(t2,3) + 
                  37*Power(t2,4))) + 
            Power(s1,4)*(Power(s2,6)*
                (8*Power(t1,2) - 19*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,5)*(41*Power(t1,3) - 130*Power(t1,2)*t2 + 
                  128*t1*Power(t2,2) - 15*Power(t2,3)) + 
               2*Power(t1,3)*Power(t2,2)*
                (30*Power(t1,3) - 44*Power(t1,2)*t2 + 
                  21*t1*Power(t2,2) - 4*Power(t2,3)) + 
               s2*Power(t1,2)*t2*
                (-180*Power(t1,4) + 232*Power(t1,3)*t2 - 
                  207*Power(t1,2)*Power(t2,2) + 68*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,4)*(18*Power(t1,4) - 421*Power(t1,3)*t2 + 
                  399*Power(t1,2)*Power(t2,2) - 197*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,2)*t1*
                (115*Power(t1,5) - 510*Power(t1,4)*t2 + 
                  981*Power(t1,3)*Power(t2,2) - 
                  564*Power(t1,2)*Power(t2,3) + 104*t1*Power(t2,4) - 
                  12*Power(t2,5)) + 
               Power(s2,3)*(194*Power(t1,5) - 692*Power(t1,4)*t2 + 
                  973*Power(t1,3)*Power(t2,2) - 
                  370*Power(t1,2)*Power(t2,3) + 94*t1*Power(t2,4) - 
                  Power(t2,5))) + 
            Power(s1,3)*t1*(3*Power(s2,6)*
                (2*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,5)*(6*Power(t1,3) - 140*Power(t1,2)*t2 + 
                  123*t1*Power(t2,2) - 43*Power(t2,3)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (96*Power(t1,3) - 131*Power(t1,2)*t2 + 
                  77*t1*Power(t2,2) - 18*Power(t2,3)) + 
               Power(t1,2)*Power(t2,3)*
                (-16*Power(t1,3) + 25*Power(t1,2)*t2 - 
                  10*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,4)*(37*Power(t1,4) - 294*Power(t1,3)*t2 + 
                  611*Power(t1,2)*Power(t2,2) - 214*t1*Power(t2,3) + 
                  47*Power(t2,4)) + 
               Power(s2,3)*(70*Power(t1,5) - 393*Power(t1,4)*t2 + 
                  913*Power(t1,3)*Power(t2,2) - 
                  690*Power(t1,2)*Power(t2,3) + 123*t1*Power(t2,4) - 
                  15*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-180*Power(t1,5) + 532*Power(t1,4)*t2 - 
                  598*Power(t1,3)*Power(t2,2) + 
                  213*Power(t1,2)*Power(t2,3) - 18*t1*Power(t2,4) + 
                  Power(t2,5)))) + 
         Power(s,2)*(Power(s1,8)*
             (6*Power(s2,5) + 4*Power(s2,4)*(t1 - 2*t2) + 
               Power(s2,3)*(15*Power(t1,2) - 20*t1*t2 + Power(t2,2)) + 
               Power(t1,3)*(21*Power(t1,2) - 12*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*t1*
                (46*Power(t1,2) - 37*t1*t2 + 2*Power(t2,2)) + 
               s2*Power(t1,2)*
                (-6*Power(t1,2) - 19*t1*t2 + 2*Power(t2,2))) + 
            Power(s2,3)*Power(t1,4)*Power(t2,3)*
             (-3*Power(s2,3) + Power(s2,2)*(-6*t1 + 15*t2) + 
               t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) - 
               s2*(Power(t1,2) - 12*t1*t2 + 15*Power(t2,2))) + 
            Power(s1,7)*(6*Power(s2,6) + 3*Power(s2,5)*(9*t1 - 11*t2) + 
               Power(s2,4)*(22*Power(t1,2) - 98*t1*t2 + 
                  28*Power(t2,2)) + 
               Power(s2,2)*t1*
                (103*Power(t1,3) - 282*Power(t1,2)*t2 + 
                  137*t1*Power(t2,2) - 8*Power(t2,3)) + 
               s2*Power(t1,2)*
                (69*Power(t1,3) - 19*Power(t1,2)*t2 + 
                  59*t1*Power(t2,2) - 7*Power(t2,3)) + 
               Power(t1,3)*(21*Power(t1,3) - 72*Power(t1,2)*t2 + 
                  41*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,3)*(104*Power(t1,3) - 163*Power(t1,2)*t2 + 
                  83*t1*Power(t2,2) - 3*Power(t2,3))) + 
            s1*Power(s2,2)*Power(t1,3)*Power(t2,2)*
             (Power(s2,5) + Power(s2,4)*(14*t1 - 15*t2) + 
               3*t1*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s2,3)*(11*Power(t1,2) - 95*t1*t2 + 
                  42*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,3) - 48*Power(t1,2)*t2 + 
                  140*t1*Power(t2,2) - 33*Power(t2,3)) + 
               s2*t2*(-8*Power(t1,3) + 43*Power(t1,2)*t2 - 
                  52*t1*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s1,2)*s2*Power(t1,2)*t2*
             (3*Power(t1,2)*Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,6)*(-2*t1 + t2) - 
               2*Power(s2,5)*
                (7*Power(t1,2) - 24*t1*t2 + 6*Power(t2,2)) + 
               s2*t1*Power(t2,2)*
                (-37*Power(t1,3) + 92*Power(t1,2)*t2 - 
                  50*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,4)*(-7*Power(t1,3) + 179*Power(t1,2)*t2 - 
                  204*t1*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s2,3)*(-16*Power(t1,4) + 91*Power(t1,3)*t2 - 
                  435*Power(t1,2)*Power(t2,2) + 246*t1*Power(t2,3) - 
                  18*Power(t2,4)) + 
               Power(s2,2)*t2*
                (54*Power(t1,4) - 192*Power(t1,3)*t2 + 
                  290*Power(t1,2)*Power(t2,2) - 84*t1*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s1,6)*(Power(s2,7) + 21*Power(s2,6)*(t1 - t2) + 
               Power(s2,5)*(41*Power(t1,2) - 159*t1*t2 + 
                  60*Power(t2,2)) + 
               Power(s2,4)*(70*Power(t1,3) - 226*Power(t1,2)*t2 + 
                  275*t1*Power(t2,2) - 35*Power(t2,3)) + 
               2*Power(t1,3)*t2*
                (-30*Power(t1,3) + 50*Power(t1,2)*t2 - 
                  26*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,3)*(190*Power(t1,4) - 573*Power(t1,3)*t2 + 
                  412*Power(t1,2)*Power(t2,2) - 135*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s2*Power(t1,2)*
                (75*Power(t1,4) - 180*Power(t1,3)*t2 + 
                  111*Power(t1,2)*Power(t2,2) - 73*t1*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(s2,2)*t1*
                (158*Power(t1,4) - 493*Power(t1,3)*t2 + 
                  603*Power(t1,2)*Power(t2,2) - 191*t1*Power(t2,3) + 
                  12*Power(t2,4))) + 
            Power(s1,5)*(Power(s2,7)*(3*t1 - 2*t2) + 
               Power(s2,6)*(23*Power(t1,2) - 69*t1*t2 + 
                  24*Power(t2,2)) + 
               Power(s2,5)*(56*Power(t1,3) - 205*Power(t1,2)*t2 + 
                  267*t1*Power(t2,2) - 45*Power(t2,3)) + 
               2*Power(t1,3)*Power(t2,2)*
                (30*Power(t1,3) - 36*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - 2*Power(t2,3)) + 
               s2*Power(t1,2)*t2*
                (-180*Power(t1,4) + 198*Power(t1,3)*t2 - 
                  153*Power(t1,2)*Power(t2,2) + 49*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,4)*(32*Power(t1,4) - 482*Power(t1,3)*t2 + 
                  489*Power(t1,2)*Power(t2,2) - 295*t1*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s2,2)*t1*
                (101*Power(t1,5) - 442*Power(t1,4)*t2 + 
                  891*Power(t1,3)*Power(t2,2) - 
                  614*Power(t1,2)*Power(t2,3) + 122*t1*Power(t2,4) - 
                  8*Power(t2,5)) + 
               Power(s2,3)*(211*Power(t1,5) - 729*Power(t1,4)*t2 + 
                  1107*Power(t1,3)*Power(t2,2) - 
                  438*Power(t1,2)*Power(t2,3) + 104*t1*Power(t2,4) - 
                  Power(t2,5))) + 
            Power(s1,4)*(Power(s2,7)*
                (3*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
               Power(s2,6)*(18*Power(t1,3) - 65*Power(t1,2)*t2 + 
                  69*t1*Power(t2,2) - 9*Power(t2,3)) + 
               Power(t1,3)*Power(t2,3)*
                (-24*Power(t1,3) + 27*Power(t1,2)*t2 - 
                  8*t1*Power(t2,2) + Power(t2,3)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (144*Power(t1,4) - 129*Power(t1,3)*t2 + 
                  85*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,5)*(-2*Power(t1,4) - 265*Power(t1,3)*t2 + 
                  294*Power(t1,2)*Power(t2,2) - 174*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,3)*t1*
                (80*Power(t1,5) - 469*Power(t1,4)*t2 + 
                  1099*Power(t1,3)*Power(t2,2) - 
                  991*Power(t1,2)*Power(t2,3) + 218*t1*Power(t2,4) - 
                  35*Power(t2,5)) + 
               Power(s2,4)*(59*Power(t1,5) - 308*Power(t1,4)*t2 + 
                  896*Power(t1,3)*Power(t2,2) - 
                  413*Power(t1,2)*Power(t2,3) + 135*t1*Power(t2,4) - 
                  3*Power(t2,5)) + 
               2*Power(s2,2)*t1*t2*
                (-105*Power(t1,5) + 264*Power(t1,4)*t2 - 
                  374*Power(t1,3)*Power(t2,2) + 
                  159*Power(t1,2)*Power(t2,3) - 17*t1*Power(t2,4) + 
                  Power(t2,5))) + 
            Power(s1,3)*t1*(Power(t1,3)*Power(t2,4)*
                (3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,7)*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,6)*(4*Power(t1,3) - 47*Power(t1,2)*t2 + 
                  56*t1*Power(t2,2) - 21*Power(t2,3)) + 
               s2*Power(t1,2)*Power(t2,3)*
                (-42*Power(t1,3) + 48*Power(t1,2)*t2 - 
                  21*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*t1*Power(t2,2)*
                (144*Power(t1,4) - 328*Power(t1,3)*t2 + 
                  295*Power(t1,2)*Power(t2,2) - 76*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,5)*(-2*Power(t1,4) - 114*Power(t1,3)*t2 + 
                  363*Power(t1,2)*Power(t2,2) - 155*t1*Power(t2,3) + 
                  39*Power(t2,4)) + 
               Power(s2,4)*(19*Power(t1,5) - 105*Power(t1,4)*t2 + 
                  578*Power(t1,3)*Power(t2,2) - 
                  705*Power(t1,2)*Power(t2,3) + 146*t1*Power(t2,4) - 
                  21*Power(t2,5)) + 
               Power(s2,3)*t2*
                (-121*Power(t1,5) + 423*Power(t1,4)*t2 - 
                  797*Power(t1,3)*Power(t2,2) + 
                  429*Power(t1,2)*Power(t2,3) - 47*t1*Power(t2,4) + 
                  3*Power(t2,5)))) - 
         s*(Power(s2,4)*Power(t1,4)*Power(t2,4)*
             (3*Power(s2,2) + 2*s2*t1 - 7*s2*t2 - 2*t1*t2 + 
               3*Power(t2,2)) + 
            Power(s1,9)*(4*Power(s2,5) + 
               2*Power(s2,2)*Power(t1,2)*(9*t1 - 4*t2) + 
               Power(s2,3)*t1*(11*t1 - 4*t2) + Power(s2,4)*(t1 - 2*t2) + 
               Power(t1,4)*(7*t1 - 2*t2) - s2*Power(t1,3)*(t1 + 4*t2)) + 
            s1*Power(s2,3)*Power(t1,3)*Power(t2,3)*
             (-2*Power(s2,4) - 4*Power(s2,3)*(4*t1 - 3*t2) + 
               2*t1*Power(t2,2)*(-2*t1 + 3*t2) + 
               Power(s2,2)*(-2*Power(t1,2) + 54*t1*t2 - 
                  17*Power(t2,2)) + 
               s2*t2*(6*Power(t1,2) - 41*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,8)*(6*Power(s2,6) + Power(s2,5)*(17*t1 - 21*t2) + 
               Power(s2,4)*(18*Power(t1,2) - 33*t1*t2 + 8*Power(t2,2)) + 
               Power(t1,4)*(7*Power(t1,2) - 26*t1*t2 + 8*Power(t2,2)) + 
               s2*Power(t1,3)*
                (29*Power(t1,2) + 5*t1*t2 + 14*Power(t2,2)) + 
               Power(s2,3)*t1*
                (62*Power(t1,2) - 76*t1*t2 + 19*Power(t2,2)) + 
               Power(s2,2)*Power(t1,2)*
                (41*Power(t1,2) - 105*t1*t2 + 35*Power(t2,2))) + 
            Power(s1,7)*(2*Power(s2,7) + 3*Power(s2,6)*(7*t1 - 8*t2) - 
               2*Power(t1,4)*t2*
                (12*Power(t1,2) - 19*t1*t2 + 6*Power(t2,2)) + 
               3*Power(s2,5)*
                (11*Power(t1,2) - 34*t1*t2 + 14*Power(t2,2)) + 
               Power(s2,2)*Power(t1,2)*
                (70*Power(t1,3) - 185*Power(t1,2)*t2 + 
                  241*t1*Power(t2,2) - 60*Power(t2,3)) + 
               Power(s2,3)*t1*
                (113*Power(t1,3) - 315*Power(t1,2)*t2 + 
                  191*t1*Power(t2,2) - 37*Power(t2,3)) + 
               3*Power(s2,4)*
                (17*Power(t1,3) - 39*Power(t1,2)*t2 + 
                  39*t1*Power(t2,2) - 4*Power(t2,3)) + 
               3*s2*(10*Power(t1,6) - 27*Power(t1,5)*t2 - 
                  6*Power(t1,3)*Power(t2,3))) + 
            Power(s1,2)*Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (Power(s2,5)*(6*t1 - 2*t2) + 
               Power(t1,2)*Power(t2,2)*
                (5*Power(t1,2) - 12*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,4)*(28*Power(t1,2) - 54*t1*t2 + 9*Power(t2,2)) - 
               Power(s2,3)*(Power(t1,3) + 145*Power(t1,2)*t2 - 
                  115*t1*Power(t2,2) + 10*Power(t2,3)) + 
               s2*t1*t2*(-13*Power(t1,3) + 37*Power(t1,2)*t2 - 
                  62*t1*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,2)*(6*Power(t1,4) - 15*Power(t1,3)*t2 + 
                  178*Power(t1,2)*Power(t2,2) - 72*t1*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s1,6)*(6*Power(s2,7)*(t1 - t2) + 
               2*Power(t1,4)*Power(t2,2)*
                (15*Power(t1,2) - 14*t1*t2 + 4*Power(t2,2)) + 
               9*Power(s2,6)*(3*Power(t1,2) - 9*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,5)*(41*Power(t1,3) - 152*Power(t1,2)*t2 + 
                  211*t1*Power(t2,2) - 40*Power(t2,3)) + 
               s2*Power(t1,3)*t2*
                (-90*Power(t1,3) + 82*Power(t1,2)*t2 - 
                  17*t1*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,4)*(38*Power(t1,4) - 297*Power(t1,3)*t2 + 
                  272*Power(t1,2)*Power(t2,2) - 167*t1*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s2,3)*t1*
                (121*Power(t1,4) - 424*Power(t1,3)*t2 + 
                  656*Power(t1,2)*Power(t2,2) - 235*t1*Power(t2,3) + 
                  37*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (47*Power(t1,4) - 206*Power(t1,3)*t2 + 
                  361*Power(t1,2)*Power(t2,2) - 284*t1*Power(t2,3) + 
                  50*Power(t2,4))) + 
            Power(s1,5)*(Power(t1,4)*Power(t2,3)*
                (-16*Power(t1,2) + 11*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,7)*(5*Power(t1,2) - 16*t1*t2 + 6*Power(t2,2)) + 
               3*Power(s2,6)*
                (8*Power(t1,3) - 29*Power(t1,2)*t2 + 
                  38*t1*Power(t2,2) - 8*Power(t2,3)) + 
               s2*Power(t1,3)*Power(t2,2)*
                (96*Power(t1,3) - 41*Power(t1,2)*t2 + 
                  19*t1*Power(t2,2) - 2*Power(t2,3)) - 
               2*Power(s2,2)*Power(t1,2)*t2*
                (62*Power(t1,4) - 125*Power(t1,3)*t2 + 
                  186*Power(t1,2)*Power(t2,2) - 92*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,5)*(-6*Power(t1,4) - 217*Power(t1,3)*t2 + 
                  261*Power(t1,2)*Power(t2,2) - 197*t1*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s2,3)*t1*
                (47*Power(t1,5) - 305*Power(t1,4)*t2 + 
                  689*Power(t1,3)*Power(t2,2) - 
                  699*Power(t1,2)*Power(t2,3) + 152*t1*Power(t2,4) - 
                  19*Power(t2,5)) + 
               Power(s2,4)*(50*Power(t1,5) - 187*Power(t1,4)*t2 + 
                  627*Power(t1,3)*Power(t2,2) - 
                  300*Power(t1,2)*Power(t2,3) + 113*t1*Power(t2,4) - 
                  2*Power(t2,5))) + 
            Power(s1,3)*s2*t1*t2*
             (2*Power(t1,4)*(3*t1 - 2*t2)*Power(t2,3) + 
               Power(s2,6)*(-5*Power(t1,2) + 10*t1*t2 - 4*Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (-42*Power(t1,3) + 68*Power(t1,2)*t2 - 
                  53*t1*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,5)*(-24*Power(t1,3) + 92*Power(t1,2)*t2 - 
                  52*t1*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s2,4)*(5*Power(t1,4) + 166*Power(t1,3)*t2 - 
                  305*Power(t1,2)*Power(t2,2) + 75*t1*Power(t2,3) - 
                  13*Power(t2,4)) + 
               Power(s2,2)*t1*t2*
                (63*Power(t1,4) - 148*Power(t1,3)*t2 + 
                  272*Power(t1,2)*Power(t2,2) - 104*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,3)*(-20*Power(t1,5) + 54*Power(t1,4)*t2 - 
                  362*Power(t1,3)*Power(t2,2) + 
                  314*Power(t1,2)*Power(t2,3) - 40*t1*Power(t2,4) + 
                  3*Power(t2,5))) + 
            Power(s1,4)*(Power(t1,5)*(3*t1 - 2*t2)*Power(t2,4) - 
               3*s2*Power(t1,4)*Power(t2,3)*
                (14*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,7)*(Power(t1,3) - 13*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*Power(t2,2)*
                (114*Power(t1,4) - 170*Power(t1,3)*t2 + 
                  203*Power(t1,2)*Power(t2,2) - 63*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,6)*(6*Power(t1,4) - 77*Power(t1,3)*t2 + 
                  103*Power(t1,2)*Power(t2,2) - 69*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,4)*t1*
                (16*Power(t1,5) - 85*Power(t1,4)*t2 + 
                  376*Power(t1,3)*Power(t2,2) - 
                  630*Power(t1,2)*Power(t2,3) + 164*t1*Power(t2,4) - 
                  34*Power(t2,5)) - 
               Power(s2,5)*(3*Power(t1,5) + 65*Power(t1,4)*t2 - 
                  379*Power(t1,3)*Power(t2,2) + 
                  207*Power(t1,2)*Power(t2,3) - 84*t1*Power(t2,4) + 
                  3*Power(t2,5)) + 
               Power(s2,3)*t1*t2*
                (-96*Power(t1,5) + 302*Power(t1,4)*t2 - 
                  592*Power(t1,3)*Power(t2,2) + 
                  390*Power(t1,2)*Power(t2,3) - 49*t1*Power(t2,4) + 
                  4*Power(t2,5)))))*lg2(s1 + t1 - t2))/
     (Power(s1,3)*Power(s2,3)*Power(t1,3)*(s1 + t1)*(s - s2 + t1)*t2*
       Power(s - s1 + t2,3)*(s - s1 - s2 + t2)) + 
    (8*(Power(s,4)*Power(t1 - t2,2)*t2 + 
         Power(s1,4)*(s2 - t1)*(s2 + t2)*(s2 - t1 + t2) + 
         s2*(s2 - t1)*Power(t2,3)*
          (2*Power(s2,2) + 2*s2*t2 + Power(t2,2)) + 
         Power(s1,3)*(Power(s2,4) - Power(t1,3)*t2 + 
            Power(s2,3)*(-2*t1 + 3*t2) + 
            s2*t2*(5*Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(Power(t1,2) - 7*t1*t2 + 2*Power(t2,2))) + 
         s1*Power(t2,2)*(Power(s2,4) - Power(s2,3)*(t1 - 3*t2) + 
            t1*(t1 - 2*t2)*Power(t2,2) + Power(s2,2)*t2*(-t1 + 3*t2) + 
            s2*t2*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2))) - 
         Power(s1,2)*t2*(Power(s2,3)*(3*t1 + t2) + 
            t1*t2*(Power(t1,2) - 2*Power(t2,2)) + 
            Power(s2,2)*(-5*Power(t1,2) - 2*t1*t2 + 2*Power(t2,2)) + 
            s2*(2*Power(t1,3) - Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
               2*Power(t2,3))) - 
         Power(s,3)*(-((t1 - t2)*t2*
               (-3*s2*t1 + Power(t1,2) + 4*s2*t2 - 2*t1*t2)) + 
            s1*(s2*(Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               2*t2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)))) - 
         s*(Power(s1,3)*(Power(s2,3) + t2*Power(-2*t1 + t2,2) + 
               Power(s2,2)*(-4*t1 + 3*t2) + 
               s2*(3*Power(t1,2) - 10*t1*t2 + 2*Power(t2,2))) + 
            Power(t2,2)*(t1*(t1 - t2)*Power(t2,2) + 
               Power(s2,3)*(-4*t1 + 6*t2) + 
               s2*t2*(3*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,2)*t1*(2*t1 - 15*t2) + 
               s2*t1*(13*t1 - 9*t2)*t2 + Power(s2,3)*(-2*t1 + t2) + 
               t2*(-3*Power(t1,3) + 3*Power(t1,2)*t2 - 2*Power(t2,3))) + 
            s1*t2*(-4*Power(s2,3)*t1 + 
               Power(s2,2)*(7*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
               s2*(-4*Power(t1,3) + 7*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  Power(t2,3)) + 
               t2*(-2*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  2*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,2)*
             (Power(s2,2)*(-2*t1 + t2) + 
               s2*(3*Power(t1,2) - 11*t1*t2 + 2*Power(t2,2)) + 
               t2*(6*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2))) + 
            t2*(Power(s2,2)*(2*Power(t1,2) - 9*t1*t2 + 7*Power(t2,2)) + 
               t2*(-Power(t1,3) + Power(t1,2)*t2 - t1*Power(t2,2) + 
                  Power(t2,3)) + 
               s2*(-2*Power(t1,3) + 6*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(Power(s2,2)*t1*(t1 - 8*t2) + 
               s2*t2*(11*Power(t1,2) - 15*t1*t2 + 2*Power(t2,2)) - 
               t2*(3*Power(t1,3) - 6*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
                  2*Power(t2,3)))))*lg2(-(t1/(s2*(-s2 + t1 - t2)))))/
     (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*Power(t2,3)) - 
    (8*(Power(s1,3)*(s2 - t1)*
          (4*Power(s2,3) + s2*t1*(3*t1 - 5*t2) + 2*Power(t1,2)*t2 + 
            Power(s2,2)*(-7*t1 + 2*t2)) + 
         Power(s1,2)*(3*Power(s2,5) - 8*Power(s2,4)*t1 + 
            Power(s2,3)*t1*(7*t1 - 3*t2) - 7*s2*Power(t1,3)*t2 + 
            2*Power(t1,4)*t2 + 
            Power(s2,2)*t1*(-2*Power(t1,2) + 8*t1*t2 + Power(t2,2))) + 
         Power(s,4)*(Power(s2,2)*(-t1 + t2) + 
            s2*(Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) + 
            t2*(3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) + 
         s2*(s2 - t1)*t2*(Power(s2,4) + Power(t1,3)*(2*t1 - t2) + 
            Power(s2,3)*(-3*t1 + 2*t2) - 
            s2*t1*(6*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(6*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2))) + 
         s1*(Power(s2,6) - Power(t1,5)*t2 + 
            s2*Power(t1,3)*(7*t1 - t2)*t2 + Power(s2,5)*(-3*t1 + t2) + 
            Power(s2,2)*t1*t2*
             (-14*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
            Power(s2,4)*(3*Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) - 
            Power(s2,3)*(Power(t1,3) - 11*Power(t1,2)*t2 + 
               t1*Power(t2,2) - 2*Power(t2,3))) - 
         Power(s,3)*(Power(s2,3)*(-3*t1 + 4*t2) - 
            3*t1*t2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(4*Power(t1,2) - 19*t1*t2 + 8*Power(t2,2)) - 
            s2*(Power(t1,3) - 20*Power(t1,2)*t2 + 20*t1*Power(t2,2) - 
               8*Power(t2,3)) + 
            s1*(Power(s2,3) + Power(s2,2)*(-5*t1 + t2) + 
               s2*(5*Power(t1,2) - 15*t1*t2 + 2*Power(t2,2)) + 
               t2*(8*Power(t1,2) - 9*t1*t2 + 2*Power(t2,2)))) + 
         s*(Power(s1,4)*s2*(s2 - t1) + Power(s2,5)*(t1 - 4*t2) + 
            Power(t1,4)*(t1 - t2)*t2 + 
            Power(s2,4)*(-2*Power(t1,2) + 17*t1*t2 - 8*Power(t2,2)) + 
            s2*Power(t1,2)*t2*
             (-12*Power(t1,2) + 9*t1*t2 - 2*Power(t2,2)) + 
            3*Power(s2,2)*t1*t2*
             (10*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,3)*(Power(t1,3) - 32*Power(t1,2)*t2 + 
               20*t1*Power(t2,2) - 8*Power(t2,3)) - 
            Power(s1,3)*(4*Power(s2,3) + s2*t1*(5*t1 - 8*t2) + 
               2*Power(t1,2)*t2 + Power(s2,2)*(-9*t1 + 3*t2)) + 
            Power(s1,2)*(-6*Power(s2,4) + 19*Power(s2,3)*t1 - 
               19*Power(s2,2)*t1*(t1 - t2) + 
               Power(t1,2)*(9*t1 - 4*t2)*t2 + 
               3*s2*t1*(2*Power(t1,2) - 8*t1*t2 + Power(t2,2))) + 
            s1*(-3*Power(s2,5) + Power(s2,4)*(11*t1 - 3*t2) + 
               3*Power(t1,3)*t2*(-2*t1 + t2) + 
               Power(s2,3)*(-12*Power(t1,2) + 23*t1*t2 - 
                  6*Power(t2,2)) + 
               s2*t1*t2*(27*Power(t1,2) - 13*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,3) - 40*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) - 6*Power(t2,3)))) + 
         Power(s,2)*(-3*Power(s2,4)*(t1 - 2*t2) + 
            Power(s1,3)*s2*(t1 + t2) + 
            Power(s1,2)*(3*Power(s2,3) - 11*Power(s2,2)*t1 + 
               s2*t1*(9*t1 - 16*t2) + t1*(7*t1 - 4*t2)*t2) + 
            Power(t1,2)*t2*(4*Power(t1,2) - 5*t1*t2 + Power(t2,2)) - 
            3*s2*t1*t2*(8*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,3)*(5*Power(t1,2) - 27*t1*t2 + 12*Power(t2,2)) - 
            2*Power(s2,2)*(Power(t1,3) - 20*Power(t1,2)*t2 + 
               15*t1*Power(t2,2) - 6*Power(t2,3)) + 
            s1*(3*Power(s2,4) + Power(s2,3)*(-13*t1 + 3*t2) + 
               t1*t2*(-13*Power(t1,2) + 12*t1*t2 - 2*Power(t2,2)) + 
               2*Power(s2,2)*(7*Power(t1,2) - 17*t1*t2 + 3*Power(t2,2)) + 
               s2*(-4*Power(t1,3) + 37*Power(t1,2)*t2 - 
                  19*t1*Power(t2,2) + 6*Power(t2,3)))))*
       lg2(-((-s + s1 - t2)/(s1*(-s2 + t1 - t2)))))/
     (s1*s2*t1*Power(s - s2 + t1,3)*(-s + s1 - t2)*t2) - 
    (8*(Power(s,4)*(t1 - t2)*(s1 + t1 - t2)*t2 + 
         Power(s2,3)*(s2 - t1)*Power(t2,3) + 
         s1*Power(s2,2)*Power(t2,2)*
          (3*s2*t1 - 2*Power(t1,2) + 4*s2*t2 - 3*t1*t2) + 
         Power(s1,4)*(2*Power(s2,3) + t1*(t1 - t2)*t2 + 
            Power(s2,2)*(-5*t1 + 2*t2) + 
            s2*(3*Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
         Power(s1,2)*s2*t2*(Power(s2,3) - 4*Power(s2,2)*t1 - 
            t1*(Power(t1,2) + t1*t2 + Power(t2,2)) + 
            s2*(4*Power(t1,2) + 3*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,3)*(2*Power(s2,4) + Power(t1,2)*t2*(-2*t1 + t2) + 
            Power(s2,3)*(-6*t1 + 2*t2) + 
            Power(s2,2)*(6*Power(t1,2) - 9*t1*t2 + Power(t2,2)) + 
            s2*(-2*Power(t1,3) + 8*Power(t1,2)*t2 - t1*Power(t2,2) + 
               Power(t2,3))) - 
         Power(s,3)*(Power(s1,2)*(s2 + t1)*t2 + 
            (t1 - t2)*t2*(2*s2*(t1 - 2*t2) + t1*(-t1 + t2)) + 
            s1*(s2*(2*Power(t1,2) - 2*t1*t2 - 3*Power(t2,2)) + 
               t2*(3*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)))) - 
         Power(s,2)*(Power(s1,2)*
             (Power(s2,2)*(4*t1 - 3*t2) + s2*t1*(-7*t1 + 16*t2) + 
               t2*(-6*Power(t1,2) + 11*t1*t2 - 3*Power(t2,2))) + 
            s2*t2*(t1*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) - 
               s2*(Power(t1,2) - 6*t1*t2 + 6*Power(t2,2))) + 
            s1*(3*t1*Power(t1 - t2,2)*t2 + 
               Power(s2,2)*(-2*Power(t1,2) + 7*t1*t2 + 3*Power(t2,2)) + 
               s2*(Power(t1,3) - 5*Power(t1,2)*t2 + 11*t1*Power(t2,2) - 
                  12*Power(t2,3)))) + 
         s*(Power(s2,2)*Power(t2,2)*
             (2*s2*(t1 - 2*t2) + t1*(-2*t1 + 3*t2)) + 
            s1*s2*t2*(Power(s2,2)*(4*t1 + t2) + 2*t1*t2*(-2*t1 + 3*t2) + 
               s2*(-2*Power(t1,2) + t1*t2 - 12*Power(t2,2))) - 
            Power(s1,3)*(2*Power(s2,3) + Power(s2,2)*(-9*t1 + 2*t2) + 
               s2*(8*Power(t1,2) - 15*t1*t2 + Power(t2,2)) + 
               t2*(5*Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(-7*Power(s2,2)*t1*(t1 - 3*t2) + 
               Power(s2,3)*(4*t1 - 3*t2) + 
               t1*t2*(4*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               s2*(3*Power(t1,3) - 15*Power(t1,2)*t2 + 8*t1*Power(t2,2) - 
                  6*Power(t2,3)))))*
       lg2(-((-s + s1 - t2)/((-s + s2 - t1)*(-s2 + t1 - t2)))))/
     (Power(s1,3)*s2*(-s + s2 - t1)*t1*t2*(s - s1 + t2)) - 
    (8*(Power(s1,4)*Power(t1,2)*(-s2 + t1) + Power(s,6)*(t1 + t2) + 
         Power(s,5)*(s1*(s2 - 4*t1 - 3*t2) + 6*t1*t2 - s2*(t1 + 4*t2)) + 
         Power(s1,3)*(-4*Power(t1,3)*t2 + Power(s2,3)*(t1 + t2) - 
            2*Power(s2,2)*t1*(t1 + 2*t2) + s2*Power(t1,2)*(t1 + 8*t2)) - 
         s2*Power(t2,3)*(Power(s2,2)*t1 - 
            s2*(2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
            t1*(2*Power(t1,2) - t1*t2 + Power(t2,2))) - 
         Power(s1,2)*(s2 - t1)*t2*
          (t1*(3*t1 - t2)*t2 + 2*Power(s2,2)*(2*t1 + t2) - 
            s2*(4*Power(t1,2) + 5*t1*t2 + Power(t2,2))) + 
         s1*Power(t2,2)*(Power(t1,2)*t2*(-t1 + t2) + 
            Power(s2,3)*(4*t1 + t2) + 
            s2*t1*(4*Power(t1,2) + t1*t2 + Power(t2,2)) - 
            Power(s2,2)*(7*Power(t1,2) + 2*t1*t2 + 2*Power(t2,2))) + 
         Power(s,4)*(-12*s2*t1*t2 + Power(t1 + t2,3) + 
            Power(s1,2)*(-3*s2 + 6*t1 + 3*t2) + 
            Power(s2,2)*(t1 + 6*t2) + 
            s1*(-3*Power(s2,2) - 18*t1*t2 + 2*s2*(t1 + 5*t2))) + 
         Power(s,3)*(Power(s1,3)*(3*s2 - 4*t1 - t2) - 
            Power(s2,3)*(t1 + 4*t2) + Power(s2,2)*t1*(t1 + 11*t2) + 
            Power(s1,2)*(7*Power(s2,2) - 8*s2*t2 + 18*t1*t2) - 
            s2*(Power(t1,3) + 6*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
               2*Power(t2,3)) + 
            2*(2*Power(t1,3)*t2 + t1*Power(t2,3) + Power(t2,4)) + 
            s1*(3*Power(s2,3) - Power(t1 + t2,2)*(4*t1 + t2) - 
               2*Power(s2,2)*(t1 + 6*t2) + 
               s2*(Power(t1,2) + 29*t1*t2 + 2*Power(t2,2)))) + 
         s*(Power(s1,4)*s2*(s2 + t1) + 
            Power(s1,3)*(2*Power(s2,3) - 2*Power(s2,2)*t2 - 
               Power(t1,2)*(4*t1 + 3*t2) + s2*t1*(3*t1 + 5*t2)) + 
            Power(s1,2)*(Power(s2,4) + 12*Power(t1,3)*t2 - 
               t1*Power(t2,3) - 3*Power(s2,3)*(t1 + t2) + 
               Power(s2,2)*(5*Power(t1,2) + 14*t1*t2 + 4*Power(t2,2)) - 
               s2*(3*Power(t1,3) + 22*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + Power(t2,3))) + 
            Power(t2,2)*(-2*Power(s2,3)*t1 + 
               t1*t2*(Power(t1,2) + Power(t2,2)) + 
               Power(s2,2)*(3*Power(t1,2) + 2*Power(t2,2)) - 
               s2*(4*Power(t1,3) + t1*Power(t2,2) + 2*Power(t2,3))) + 
            s1*t2*(-Power(s2,4) + Power(t1,2)*t2*(-6*t1 + t2) + 
               Power(s2,3)*(8*t1 + t2) - 
               Power(s2,2)*(13*Power(t1,2) + 10*t1*t2 + 5*Power(t2,2)) + 
               s2*(8*Power(t1,3) + 9*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
                  3*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,4)*(-s2 + t1) - 
            Power(s1,3)*(5*Power(s2,2) + 2*s2*(t1 - t2) + 6*t1*t2) + 
            Power(s1,2)*(-5*Power(s2,3) + Power(s2,2)*(t1 + 8*t2) + 
               3*t1*(2*Power(t1,2) + 3*t1*t2 + Power(t2,2)) - 
               s2*(3*Power(t1,2) + 22*t1*t2 + 2*Power(t2,2))) + 
            t2*(Power(s2,4) - 5*Power(s2,3)*t1 + 3*Power(t1,3)*t2 + 
               2*t1*Power(t2,3) + Power(t2,4) + 
               Power(s2,2)*(5*Power(t1,2) + 2*t1*t2 + Power(t2,2)) - 
               s2*(4*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  4*Power(t2,3))) - 
            s1*(Power(s2,4) + 12*Power(t1,3)*t2 + t1*Power(t2,3) + 
               Power(t2,4) - 3*Power(s2,3)*(t1 + 2*t2) + 
               Power(s2,2)*(4*Power(t1,2) + 21*t1*t2 + 3*Power(t2,2)) - 
               s2*(3*Power(t1,3) + 20*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 5*Power(t2,3)))))*lg2(-(s1/(s*t2))))/
     (s1*s2*(-s + s2 - t1)*t1*t2*Power(s - s1 + t2,3)) - 
    (8*(Power(s1,4)*s2*Power(s2 - t1,2) - 
         Power(s,4)*(t1 - t2)*(s2 + t2)*(s2 - t1 + t2) + 
         Power(s1,3)*(s2 - t1)*
          (2*Power(s2,3) + s2*t1*(t1 - 3*t2) + Power(t1,2)*t2 + 
            Power(s2,2)*(-4*t1 + t2)) + 
         Power(s2,3)*t2*(Power(s2,3) - 2*Power(s2,2)*(t1 - t2) + 
            t1*(t1 - 2*t2)*t2 + 
            s2*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2))) + 
         Power(s1,2)*s2*(Power(s2,4) - 6*Power(s2,3)*t1 - 
            Power(t1,2)*t2*(3*t1 + 2*t2) + 
            Power(s2,2)*(7*Power(t1,2) - 6*t1*t2 - Power(t2,2)) + 
            s2*t1*(-2*Power(t1,2) + 8*t1*t2 + 3*Power(t2,2))) + 
         s1*Power(s2,2)*(Power(s2,4) - 3*Power(s2,3)*(t1 - t2) - 
            t1*t2*(Power(t1,2) + t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(3*Power(t1,2) - 4*t1*t2 + 4*Power(t2,2)) + 
            s2*(-Power(t1,3) + 2*Power(t1,2)*t2 + 3*Power(t2,3))) - 
         Power(s,3)*(-(t1*Power(t1 - t2,2)*t2) + 
            Power(s2,3)*(-3*t1 + 4*t2) + 
            Power(s2,2)*(4*Power(t1,2) - 11*t1*t2 + 8*Power(t2,2)) - 
            s2*(Power(t1,3) - 7*Power(t1,2)*t2 + 10*t1*Power(t2,2) - 
               4*Power(t2,3)) + 
            s1*(Power(s2,3) + Power(s2,2)*(-4*t1 + 2*t2) + 
               t2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*(4*Power(t1,2) - 10*t1*t2 + 3*Power(t2,2)))) + 
         s*(-(Power(s1,3)*(2*Power(s2,3) + 4*s2*t1*(t1 - t2) + 
                 Power(t1,2)*t2 + Power(s2,2)*(-6*t1 + t2))) + 
            Power(s1,2)*(-2*Power(s2,4) + 14*Power(s2,3)*t1 + 
               Power(t1,2)*(3*t1 - 2*t2)*t2 + 
               Power(s2,2)*(-14*Power(t1,2) + 20*t1*t2 + Power(t2,2)) + 
               s2*t1*(3*Power(t1,2) - 15*t1*t2 + 2*Power(t2,2))) + 
            Power(s2,2)*(Power(s2,3)*(t1 - 4*t2) + 
               Power(s2,2)*(-2*Power(t1,2) + 9*t1*t2 - 8*Power(t2,2)) + 
               t1*t2*(Power(t1,2) - 6*t1*t2 + 4*Power(t2,2)) + 
               s2*(Power(t1,3) - 6*Power(t1,2)*t2 + 13*t1*Power(t2,2) - 
                  6*Power(t2,3))) - 
            s1*s2*(Power(s2,4) + 2*Power(t1,2)*t2*(-3*t1 + 2*t2) + 
               Power(s2,3)*(-9*t1 + 3*t2) + 
               Power(s2,2)*(12*Power(t1,2) - 19*t1*t2 + 6*Power(t2,2)) + 
               s2*(-4*Power(t1,3) + 19*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + 4*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,2)*
             (2*Power(s2,3) + t1*(3*t1 - 2*t2)*t2 + 
               Power(s2,2)*(-7*t1 + 2*t2) + 
               s2*(6*Power(t1,2) - 11*t1*t2 + Power(t2,2))) + 
            s1*(Power(s2,4) + Power(s2,3)*(-11*t1 + 2*t2) - 
               t1*t2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(13*Power(t1,2) - 27*t1*t2 + 5*Power(t2,2)) + 
               s2*(-3*Power(t1,3) + 18*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + 2*Power(t2,3))) + 
            s2*(-3*Power(s2,3)*(t1 - 2*t2) - 3*t1*Power(t1 - t2,2)*t2 + 
               Power(s2,2)*(5*Power(t1,2) - 15*t1*t2 + 12*Power(t2,2)) + 
               s2*(-2*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  18*t1*Power(t2,2) + 7*Power(t2,3)))))*
       lg2(-(t1/((-s2 + t1 - t2)*t2))))/
     (s1*Power(s2,3)*t1*(s - s2 + t1)*(-s + s1 - t2)*t2) + 
    (8*(Power(s1,9)*s2*(Power(s2,4) + 4*s2*Power(t1,3) - Power(t1,4)) - 
         Power(s2,5)*Power(t1,4)*Power(t2,5) + 
         s1*Power(s2,4)*Power(t1,3)*Power(t2,4)*
          (Power(s2,2) + 6*s2*t1 - 2*Power(t1,2) - 2*s2*t2 - 3*t1*t2) + 
         Power(s1,8)*(Power(s2,6) + 
            3*Power(s2,2)*Power(t1,3)*(t1 - 2*t2) + 
            Power(s2,3)*Power(t1,2)*(11*t1 - 2*t2) + 
            4*Power(s2,5)*(t1 - t2) - 2*s2*Power(t1,4)*t2 + 
            Power(t1,5)*t2 - 3*Power(s2,4)*t1*(t1 + t2)) + 
         Power(s1,2)*Power(s2,3)*Power(t1,2)*Power(t2,3)*
          (Power(s2,3)*(-5*t1 + t2) + 
            4*s2*t1*(Power(t1,2) + 4*t1*t2 - 2*Power(t2,2)) - 
            Power(s2,2)*(10*Power(t1,2) - 11*t1*t2 + Power(t2,2)) + 
            Power(t1,2)*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2))) + 
         Power(s1,7)*(Power(s2,6)*(3*t1 - 4*t2) + 
            Power(t1,5)*(t1 - 2*t2)*t2 + 
            Power(s2,2)*Power(t1,3)*
             (2*Power(t1,2) - 18*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,3)*Power(t1,2)*
             (11*Power(t1,2) - 25*t1*t2 + 5*Power(t2,2)) + 
            Power(s2,5)*(4*Power(t1,2) - 17*t1*t2 + 6*Power(t2,2)) + 
            s2*Power(t1,4)*(Power(t1,2) - t1*t2 + 9*Power(t2,2)) + 
            Power(s2,4)*t1*(3*Power(t1,2) + 5*t1*t2 + 11*Power(t2,2))) + 
         Power(s1,3)*Power(s2,2)*t1*Power(t2,2)*
          (Power(t1,3)*Power(t2,2)*(t1 + t2) + 
            Power(s2,4)*(7*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2)) + 
            2*s2*Power(t1,2)*
             (Power(t1,3) - Power(t1,2)*t2 - t1*Power(t2,2) - 
               Power(t2,3)) + 
            Power(s2,3)*(8*Power(t1,3) - 27*Power(t1,2)*t2 + 
               5*t1*Power(t2,2) - Power(t2,3)) - 
            Power(s2,2)*t1*(3*Power(t1,3) + 17*Power(t1,2)*t2 - 
               31*t1*Power(t2,2) + 6*Power(t2,3))) + 
         Power(s1,6)*(Power(t1,5)*Power(t2,2)*(-2*t1 + t2) + 
            Power(s2,4)*t1*t2*
             (-4*Power(t1,2) + t1*t2 - 15*Power(t2,2)) + 
            s2*Power(t1,4)*t2*(Power(t1,2) + 4*t1*t2 - 8*Power(t2,2)) + 
            Power(s2,6)*(2*Power(t1,2) - 11*t1*t2 + 6*Power(t2,2)) + 
            Power(s2,3)*Power(t1,2)*
             (5*Power(t1,3) - 34*Power(t1,2)*t2 + 25*t1*Power(t2,2) - 
               7*Power(t2,3)) + 
            Power(s2,5)*(6*Power(t1,3) - 20*Power(t1,2)*t2 + 
               28*t1*Power(t2,2) - 4*Power(t2,3)) + 
            Power(s2,2)*Power(t1,3)*
             (3*Power(t1,3) - 16*Power(t1,2)*t2 + 27*t1*Power(t2,2) + 
               9*Power(t2,3))) + 
         Power(s1,4)*s2*t2*(Power(t1,5)*Power(t2,2)*(3*t1 + 2*t2) + 
            Power(s2,5)*(-3*Power(t1,3) + 13*Power(t1,2)*t2 - 
               9*t1*Power(t2,2) + Power(t2,3)) + 
            s2*Power(t1,3)*t2*
             (3*Power(t1,3) - 16*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
               Power(t2,3)) + 
            Power(s2,4)*t1*(-6*Power(t1,3) + 34*Power(t1,2)*t2 - 
               19*t1*Power(t2,2) + 8*Power(t2,3)) + 
            Power(s2,2)*Power(t1,2)*
             (-5*Power(t1,4) + 21*Power(t1,3)*t2 - 
               10*Power(t1,2)*Power(t2,2) + 13*t1*Power(t2,3) - 
               3*Power(t2,4)) + 
            Power(s2,3)*t1*(2*Power(t1,4) - Power(t1,3)*t2 - 
               36*Power(t1,2)*Power(t2,2) + 14*t1*Power(t2,3) - 
               2*Power(t2,4))) + 
         Power(s1,5)*(Power(t1,6)*Power(t2,3) + 
            Power(s2,6)*t2*(-9*Power(t1,2) + 15*t1*t2 - 4*Power(t2,2)) + 
            s2*Power(t1,4)*Power(t2,2)*
             (-5*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) - 
            5*Power(s2,2)*Power(t1,3)*t2*
             (Power(t1,3) - 6*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
               Power(t2,3)) + 
            Power(s2,5)*(3*Power(t1,4) - 22*Power(t1,3)*t2 + 
               31*Power(t1,2)*Power(t2,2) - 22*t1*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,3)*Power(t1,2)*
             (3*Power(t1,4) - 21*Power(t1,3)*t2 + 
               32*Power(t1,2)*Power(t2,2) - 22*t1*Power(t2,3) + 
               7*Power(t2,4)) + 
            Power(s2,4)*t1*(-2*Power(t1,4) + 4*Power(t1,3)*t2 + 
               14*Power(t1,2)*Power(t2,2) - 11*t1*Power(t2,3) + 
               9*Power(t2,4))) + 
         Power(s,6)*t1*(Power(s2,2)*Power(t1,3)*Power(t1 - t2,2) + 
            s1*Power(s2,2)*Power(t1,2)*
             (3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) + 
            Power(s1,3)*(-(t1*Power(t1 - t2,2)*t2) + 
               Power(s2,2)*(Power(t1,2) - 4*t1*t2 + Power(t2,2)) - 
               s2*t1*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2))) - 
            Power(s1,2)*t1*(t1*Power(t1 - t2,2)*t2 - 
               Power(s2,2)*(3*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               s2*t1*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)))) - 
         Power(s,5)*(s1*Power(s2,2)*Power(t1,3)*
             (4*s2*Power(t1,2) + 6*Power(t1,3) - 14*s2*t1*t2 - 
               21*Power(t1,2)*t2 + 7*s2*Power(t2,2) + 
               21*t1*Power(t2,2) - 6*Power(t2,3)) + 
            Power(s2,2)*Power(t1,4)*(t1 - t2)*
             (s2*(t1 - 3*t2) + 3*t2*(-t1 + t2)) + 
            Power(s1,4)*(s2*Power(t1,2)*
                (-6*Power(t1,2) + 16*t1*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*t2*
                (-5*Power(t1,2) + 8*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,3)*(Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*t1*(8*Power(t1,2) - 18*t1*t2 + 5*Power(t2,2))\
) + Power(s1,2)*Power(t1,2)*(t1*Power(t1 - t2,2)*t2*(t1 + 2*t2) + 
               Power(s2,3)*(7*Power(t1,2) - 22*t1*t2 + 4*Power(t2,2)) + 
               s2*Power(t1,2)*(Power(t1,2) - 5*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(14*Power(t1,3) - 22*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) - 3*Power(t2,3))) + 
            Power(s1,3)*t1*(s2*Power(t1,2)*
                (-5*Power(t1,2) + 11*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,3)*(7*Power(t1,2) - 18*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(16*Power(t1,3) - 17*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) - 3*Power(t2,3)) + 
               2*t1*t2*(-2*Power(t1,3) + 4*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + Power(t2,3)))) + 
         Power(s,4)*(Power(s2,2)*Power(t1,4)*t2*
             (3*Power(t1 - t2,2)*t2 + Power(s2,2)*(-2*t1 + 3*t2) - 
               3*s2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,5)*(Power(s2,4)*(t1 - 2*t2) + 
               s2*Power(t1,2)*
                (-15*Power(t1,2) + 34*t1*t2 - 9*Power(t2,2)) + 
               Power(t1,2)*t2*
                (-10*Power(t1,2) + 12*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,3)*(Power(t1,2) - 16*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*t1*
                (26*Power(t1,2) - 35*t1*t2 + 9*Power(t2,2))) + 
            s1*Power(s2,2)*Power(t1,3)*
             (Power(s2,2)*(Power(t1,2) - 13*t1*t2 + 9*Power(t2,2)) + 
               s2*(4*Power(t1,3) - 29*Power(t1,2)*t2 + 
                  56*t1*Power(t2,2) - 20*Power(t2,3)) + 
               3*t2*(-5*Power(t1,3) + 13*Power(t1,2)*t2 - 
                  10*t1*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,4)*(s2*Power(t1,3)*
                (-10*Power(t1,2) + 12*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,4)*(5*Power(t1,2) - 13*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*t1*
                (37*Power(t1,3) - 19*Power(t1,2)*t2 + 
                  24*t1*Power(t2,2) - 12*Power(t2,3)) + 
               Power(s2,3)*(28*Power(t1,3) - 63*Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(t1,2)*t2*
                (-5*Power(t1,3) + 12*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,2)*Power(t1,2)*
             (-(t1*Power(t1 - t2,2)*Power(t2,2)*(2*t1 + t2)) + 
               Power(s2,4)*(5*Power(t1,2) - 30*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,3)*(10*Power(t1,3) - 56*Power(t1,2)*t2 + 
                  69*t1*Power(t2,2) - 11*Power(t2,3)) + 
               s2*t1*t2*(Power(t1,3) + 7*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(17*Power(t1,4) - 81*Power(t1,3)*t2 + 
                  85*Power(t1,2)*Power(t2,2) - 32*t1*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s1,3)*t1*(3*Power(s2,4)*
                (4*Power(t1,2) - 10*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*(23*Power(t1,3) - 74*Power(t1,2)*t2 + 
                  41*t1*Power(t2,2) - 13*Power(t2,3)) + 
               t1*t2*(5*Power(t1,4) - 2*Power(t1,3)*t2 - 
                  6*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) - 
                  Power(t2,4)) + 
               Power(s2,2)*(28*Power(t1,4) - 61*Power(t1,3)*t2 + 
                  59*Power(t1,2)*Power(t2,2) - 14*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s2*t1*(5*Power(t1,4) - 21*Power(t1,3)*t2 + 
                  13*Power(t1,2)*Power(t2,2) - 14*t1*Power(t2,3) + 
                  6*Power(t2,4)))) + 
         Power(s,2)*(-(Power(s2,3)*Power(t1,4)*Power(t2,3)*
               (3*Power(s2,2) + 6*s2*t1 + Power(t1,2) - 9*s2*t2 - 
                 4*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,7)*(3*Power(s2,5) + 3*Power(s2,4)*(t1 - 2*t2) + 
               Power(t1,3)*t2*(-5*t1 + 2*t2) - 
               s2*Power(t1,2)*
                (15*Power(t1,2) - 19*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(-5*Power(t1,2) - 16*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*t1*
                (41*Power(t1,2) - 21*t1*t2 + 2*Power(t2,2))) + 
            s1*Power(s2,2)*Power(t1,3)*Power(t2,2)*
             (Power(s2,4) + 2*Power(s2,3)*(7*t1 - 6*t2) - 
               3*t1*Power(t1 - t2,2)*t2 + 
               Power(s2,2)*(11*Power(t1,2) - 62*t1*t2 + 
                  21*Power(t2,2)) + 
               s2*(4*Power(t1,3) - 29*Power(t1,2)*t2 + 
                  40*t1*Power(t2,2) - 6*Power(t2,3))) + 
            Power(s1,6)*(Power(s2,6) + 12*Power(s2,5)*(t1 - t2) + 
               Power(t1,3)*t2*
                (5*Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,4)*(2*Power(t1,2) - 54*t1*t2 + 
                  15*Power(t2,2)) - 
               s2*Power(t1,3)*
                (5*Power(t1,2) + 13*t1*t2 + 15*Power(t2,2)) + 
               Power(s2,2)*t1*
                (37*Power(t1,3) - 29*Power(t1,2)*t2 + 
                  45*t1*Power(t2,2) - 6*Power(t2,3)) + 
               Power(s2,3)*(61*Power(t1,3) - 45*Power(t1,2)*t2 + 
                  50*t1*Power(t2,2) - 2*Power(t2,3))) + 
            Power(s1,2)*s2*Power(t1,2)*t2*
             (3*Power(t1,2)*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s2,5)*(-4*t1 + t2) - 
               9*Power(s2,4)*(Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
               s2*t1*t2*(18*Power(t1,3) - 44*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) - 13*Power(t2,3)) + 
               Power(s2,3)*(-5*Power(t1,3) + 102*Power(t1,2)*t2 - 
                  104*t1*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s2,2)*(-12*Power(t1,4) + 63*Power(t1,3)*t2 - 
                  153*Power(t1,2)*Power(t2,2) + 67*t1*Power(t2,3) - 
                  3*Power(t2,4))) + 
            Power(s1,5)*(Power(s2,6)*(3*t1 - 2*t2) + 
               2*Power(t1,4)*t2*
                (5*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,5)*(17*Power(t1,2) - 45*t1*t2 + 
                  15*Power(t2,2)) + 
               Power(s2,4)*(39*Power(t1,3) - 89*Power(t1,2)*t2 + 
                  105*t1*Power(t2,2) - 12*Power(t2,3)) + 
               Power(s2,3)*(46*Power(t1,4) - 81*Power(t1,3)*t2 + 
                  96*Power(t1,2)*Power(t2,2) - 57*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*Power(t1,2)*
                (10*Power(t1,4) - 26*Power(t1,3)*t2 + 
                  34*Power(t1,2)*Power(t2,2) - 14*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,2)*t1*
                (23*Power(t1,4) - 136*Power(t1,3)*t2 + 
                  56*Power(t1,2)*Power(t2,2) - 45*t1*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,3)*t1*(Power(t1,3)*Power(t2,3)*
                (3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,6)*(Power(t1,2) - 6*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,5)*(5*Power(t1,3) - 50*Power(t1,2)*t2 + 
                  47*t1*Power(t2,2) - 15*Power(t2,3)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (-15*Power(t1,3) + 5*Power(t1,2)*t2 + Power(t2,3)) + 
               Power(s2,2)*t1*t2*
                (-36*Power(t1,4) + 153*Power(t1,3)*t2 - 
                  112*Power(t1,2)*Power(t2,2) + 57*t1*Power(t2,3) - 
                  11*Power(t2,4)) + 
               Power(s2,4)*(-5*Power(t1,4) - 35*Power(t1,3)*t2 + 
                  180*Power(t1,2)*Power(t2,2) - 64*t1*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(s2,3)*(13*Power(t1,5) - 70*Power(t1,4)*t2 + 
                  162*Power(t1,3)*Power(t2,2) - 
                  187*Power(t1,2)*Power(t2,3) + 39*t1*Power(t2,4) - 
                  3*Power(t2,5))) + 
            Power(s1,4)*(Power(s2,6)*
                (3*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
               Power(s2,5)*(19*Power(t1,3) - 61*Power(t1,2)*t2 + 
                  48*t1*Power(t2,2) - 6*Power(t2,3)) + 
               Power(t1,3)*Power(t2,2)*
                (-12*Power(t1,3) + 12*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + Power(t2,3)) + 
               s2*Power(t1,2)*t2*
                (6*Power(t1,4) + 33*Power(t1,3)*t2 - 
                  12*Power(t1,2)*Power(t2,2) + 9*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,4)*(11*Power(t1,4) - 111*Power(t1,3)*t2 + 
                  131*Power(t1,2)*Power(t2,2) - 69*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,3)*t1*
                (13*Power(t1,4) - 88*Power(t1,3)*t2 + 
                  172*Power(t1,2)*Power(t2,2) - 86*t1*Power(t2,3) + 
                  26*Power(t2,4)) + 
               Power(s2,2)*t1*
                (27*Power(t1,5) - 153*Power(t1,4)*t2 + 
                  165*Power(t1,3)*Power(t2,2) - 
                  107*Power(t1,2)*Power(t2,3) + 32*t1*Power(t2,4) - 
                  2*Power(t2,5)))) - 
         Power(s,3)*(Power(s2,2)*Power(t1,4)*Power(t2,2)*
             (Power(s2,3) + Power(s2,2)*(6*t1 - 9*t2) - 
               Power(t1 - t2,2)*t2 + 
               3*s2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,6)*(Power(s2,5) + 3*Power(s2,4)*(t1 - 2*t2) + 
               s2*Power(t1,2)*
                (-20*Power(t1,2) + 36*t1*t2 - 5*Power(t2,2)) - 
               3*Power(s2,3)*(Power(t1,2) + 8*t1*t2 - Power(t2,2)) - 
               Power(t1,2)*t2*
                (10*Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*t1*
                (44*Power(t1,2) - 37*t1*t2 + 7*Power(t2,2))) + 
            Power(s1,5)*(4*Power(s2,5)*(t1 - t2) + 
               2*Power(t1,2)*Power(t2,2)*
                (4*Power(t1,2) - 5*t1*t2 + Power(t2,2)) - 
               s2*Power(t1,3)*
                (10*Power(t1,2) + 2*t1*t2 + 11*Power(t2,2)) + 
               Power(s2,4)*(10*Power(t1,2) - 45*t1*t2 + 
                  12*Power(t2,2)) + 
               Power(s2,2)*t1*
                (48*Power(t1,3) - 21*Power(t1,2)*t2 + 
                  49*t1*Power(t2,2) - 15*Power(t2,3)) + 
               Power(s2,3)*(53*Power(t1,3) - 80*Power(t1,2)*t2 + 
                  60*t1*Power(t2,2) - 4*Power(t2,3))) + 
            s1*Power(s2,2)*Power(t1,3)*t2*
             (Power(s2,3)*(-4*t1 + 5*t2) + 
               Power(s2,2)*(-8*Power(t1,2) + 49*t1*t2 - 
                  24*Power(t2,2)) + 
               t2*(12*Power(t1,3) - 27*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) - 2*Power(t2,3)) + 
               s2*(-8*Power(t1,3) + 50*Power(t1,2)*t2 - 
                  76*t1*Power(t2,2) + 19*Power(t2,3))) + 
            Power(s1,4)*(Power(s2,5)*
                (7*Power(t1,2) - 14*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,4)*(36*Power(t1,3) - 91*Power(t1,2)*t2 + 
                  59*t1*Power(t2,2) - 6*Power(t2,3)) + 
               Power(t1,2)*t2*
                (10*Power(t1,4) - 8*Power(t1,3)*t2 + 
                  2*t1*Power(t2,3) - Power(t2,4)) + 
               Power(s2,3)*(38*Power(t1,4) - 95*Power(t1,3)*t2 + 
                  102*Power(t1,2)*Power(t2,2) - 48*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*t1*
                (32*Power(t1,4) - 119*Power(t1,3)*t2 + 
                  89*Power(t1,2)*Power(t2,2) - 37*t1*Power(t2,3) + 
                  9*Power(t2,4)) + 
               s2*Power(t1,2)*
                (10*Power(t1,4) - 34*Power(t1,3)*t2 + 
                  24*Power(t1,2)*Power(t2,2) - 28*t1*Power(t2,3) + 
                  9*Power(t2,4))) + 
            Power(s1,2)*Power(t1,2)*
             (Power(t1,2)*Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,4)*t2*
                (-40*Power(t1,2) + 87*t1*t2 - 15*Power(t2,2)) + 
               Power(s2,5)*(Power(t1,2) - 18*t1*t2 + 4*Power(t2,2)) + 
               s2*t1*Power(t2,2)*
                (-5*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) - 4*Power(t2,3)) - 
               Power(s2,2)*t2*
                (32*Power(t1,4) - 108*Power(t1,3)*t2 + 
                  100*Power(t1,2)*Power(t2,2) - 33*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,3)*(9*Power(t1,4) - 60*Power(t1,3)*t2 + 
                  164*Power(t1,2)*Power(t2,2) - 100*t1*Power(t2,3) + 
                  10*Power(t2,4))) + 
            Power(s1,3)*t1*(-2*Power(t1,3)*Power(t2,2)*
                (4*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
               Power(s2,5)*(7*Power(t1,2) - 22*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,4)*(13*Power(t1,3) - 97*Power(t1,2)*t2 + 
                  68*t1*Power(t2,2) - 21*Power(t2,3)) + 
               s2*t1*t2*(4*Power(t1,4) + 25*Power(t1,3)*t2 - 
                  23*Power(t1,2)*Power(t2,2) + 13*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,3)*(11*Power(t1,4) - 83*Power(t1,3)*t2 + 
                  178*Power(t1,2)*Power(t2,2) - 52*t1*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(s2,2)*(28*Power(t1,5) - 153*Power(t1,4)*t2 + 
                  157*Power(t1,3)*Power(t2,2) - 
                  99*Power(t1,2)*Power(t2,3) + 22*t1*Power(t2,4) - 
                  Power(t2,5)))) + 
         s*(Power(s2,4)*Power(t1,4)*Power(t2,4)*(-3*s2 - 2*t1 + 3*t2) + 
            Power(s1,8)*(-3*Power(s2,5) - Power(s2,4)*(t1 - 2*t2) + 
               2*s2*Power(t1,3)*(3*t1 - 2*t2) + Power(t1,4)*t2 + 
               5*Power(s2,2)*Power(t1,2)*(-4*t1 + t2) + 
               2*Power(s2,3)*t1*(t1 + 2*t2)) + 
            s1*Power(s2,3)*Power(t1,3)*Power(t2,3)*
             (2*Power(s2,3) + Power(s2,2)*(16*t1 - 9*t2) + 
               2*t1*t2*(-2*t1 + 3*t2) + 
               s2*(2*Power(t1,2) - 29*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,7)*(-2*Power(s2,6) - 12*Power(s2,5)*(t1 - t2) - 
               4*Power(t1,5)*t2 + 
               Power(s2,3)*t1*
                (-40*Power(t1,2) + 12*t1*t2 - 15*Power(t2,2)) + 
               Power(s2,2)*Power(t1,2)*
                (-16*Power(t1,2) + 22*t1*t2 - 15*Power(t2,2)) + 
               Power(s2,4)*(6*Power(t1,2) + 25*t1*t2 - 6*Power(t2,2)) + 
               s2*Power(t1,3)*(Power(t1,2) + 9*t1*t2 + 6*Power(t2,2))) - 
            Power(s1,6)*(6*Power(s2,6)*(t1 - t2) + 
               Power(t1,4)*t2*
                (5*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
               2*Power(s2,5)*
                (7*Power(t1,2) - 24*t1*t2 + 9*Power(t2,2)) + 
               s2*Power(t1,4)*
                (5*Power(t1,2) - 9*t1*t2 + 28*Power(t2,2)) + 
               Power(s2,3)*t1*
                (35*Power(t1,3) - 63*Power(t1,2)*t2 + 
                  36*t1*Power(t2,2) - 22*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*
                (10*Power(t1,3) - 79*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) - 19*Power(t2,3)) + 
               Power(s2,4)*(18*Power(t1,3) - 23*Power(t1,2)*t2 + 
                  66*t1*Power(t2,2) - 6*Power(t2,3))) + 
            Power(s1,2)*Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (Power(s2,4)*(-8*t1 + 2*t2) + 
               2*Power(t1,2)*t2*(Power(t1,2) - 2*Power(t2,2)) - 
               3*Power(s2,3)*
                (7*Power(t1,2) - 14*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,3) + 74*Power(t1,2)*t2 - 
                  49*t1*Power(t2,2) + 3*Power(t2,3)) + 
               s2*t1*(-3*Power(t1,3) + 16*Power(t1,2)*t2 - 
                  45*t1*Power(t2,2) + 18*Power(t2,3))) + 
            Power(s1,3)*s2*t1*t2*
             (2*Power(t1,4)*Power(t2,2)*(-3*t1 + 2*t2) + 
               Power(s2,5)*(7*Power(t1,2) - 12*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,4)*(12*Power(t1,3) - 73*Power(t1,2)*t2 + 
                  34*t1*Power(t2,2) - 9*Power(t2,3)) + 
               s2*Power(t1,2)*t2*
                (-12*Power(t1,3) + 39*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(-3*Power(t1,4) - 48*Power(t1,3)*t2 + 
                  123*Power(t1,2)*Power(t2,2) - 26*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,2)*t1*
                (12*Power(t1,4) - 46*Power(t1,3)*t2 + 
                  85*Power(t1,2)*Power(t2,2) - 79*t1*Power(t2,3) + 
                  14*Power(t2,4))) - 
            Power(s1,5)*(-2*Power(t1,4)*Power(t2,2)*
                (4*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               Power(s2,6)*(5*Power(t1,2) - 16*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,5)*(18*Power(t1,3) - 59*Power(t1,2)*t2 + 
                  69*t1*Power(t2,2) - 12*Power(t2,3)) + 
               s2*Power(t1,3)*t2*
                (4*Power(t1,3) + 19*Power(t1,2)*t2 - 
                  11*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,4)*(3*Power(t1,4) - 48*Power(t1,3)*t2 + 
                  71*Power(t1,2)*Power(t2,2) - 64*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (14*Power(t1,4) - 78*Power(t1,3)*t2 + 
                  100*Power(t1,2)*Power(t2,2) - 25*t1*Power(t2,3) + 
                  13*Power(t2,4)) + 
               Power(s2,3)*t1*
                (13*Power(t1,4) - 81*Power(t1,3)*t2 + 
                  81*Power(t1,2)*Power(t2,2) - 49*t1*Power(t2,3) + 
                  15*Power(t2,4))) + 
            Power(s1,4)*(Power(t1,5)*Power(t2,3)*(-3*t1 + 2*t2) + 
               s2*Power(t1,4)*Power(t2,2)*
                (15*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2)) - 
               Power(s2,6)*(Power(t1,3) - 15*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,4)*t1*
                (6*Power(t1,4) + 4*Power(t1,3)*t2 - 
                  114*Power(t1,2)*Power(t2,2) + 65*t1*Power(t2,3) - 
                  25*Power(t2,4)) + 
               Power(s2,5)*(-7*Power(t1,4) + 55*Power(t1,3)*t2 - 
                  73*Power(t1,2)*Power(t2,2) + 42*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (21*Power(t1,4) - 108*Power(t1,3)*t2 + 
                  49*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,3)*t1*
                (-10*Power(t1,5) + 56*Power(t1,4)*t2 - 
                  83*Power(t1,3)*Power(t2,2) + 
                  118*Power(t1,2)*Power(t2,3) - 41*t1*Power(t2,4) + 
                  4*Power(t2,5)))))*lg2(-t2))/
     (Power(s1,3)*Power(s2,3)*Power(t1,3)*(s1 + t1)*(s - s2 + t1)*t2*
       Power(s - s1 + t2,3)) - 
    (8*(Power(s2,5)*Power(s2 - t1,4)*Power(t2,5) + 
         s1*Power(s2,4)*Power(s2 - t1,3)*Power(t2,4)*
          (3*s2*t1 - 2*Power(t1,2) + 5*s2*t2 - 3*t1*t2) - 
         Power(s1,7)*Power(s2 - t1,2)*
          (Power(s2,4)*t1 + Power(t1,3)*Power(t2,2) + 
            Power(s2,2)*t1*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(s2,3)*(-2*Power(t1,2) + t1*t2 + 2*Power(t2,2))) - 
         Power(s1,2)*Power(s2,3)*Power(s2 - t1,2)*Power(t2,3)*
          (Power(s2,3)*t1 + 4*s2*t1*t2*(2*t1 + t2) + 
            Power(t1,2)*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) - 
            Power(s2,2)*(2*Power(t1,2) + 6*t1*t2 + 7*Power(t2,2))) + 
         Power(s1,3)*Power(s2,2)*(s2 - t1)*Power(t2,2)*
          (4*Power(s2,6) + 3*Power(s2,5)*(-7*t1 + t2) + 
            Power(t1,3)*Power(t2,2)*(t1 + t2) + 
            2*Power(s2,4)*(21*Power(t1,2) - 7*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*t1*(22*Power(t1,3) - 39*Power(t1,2)*t2 + 
               21*t1*Power(t2,2) - 7*Power(t2,3)) - 
            s2*Power(t1,2)*(5*Power(t1,3) - 14*Power(t1,2)*t2 + 
               12*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s2,3)*(-42*Power(t1,3) + 36*Power(t1,2)*t2 - 
               10*t1*Power(t2,2) + 8*Power(t2,3))) - 
         Power(s1,6)*(s2 - t1)*
          (2*Power(s2,6)*(t1 + t2) + Power(t1,4)*Power(t2,2)*(t1 + t2) - 
            2*s2*Power(t1,3)*Power(t2,2)*(3*t1 + 2*t2) - 
            Power(s2,5)*t1*(7*t1 + 6*t2) + 
            Power(s2,4)*(9*Power(t1,3) + 6*Power(t1,2)*t2 + 
               2*t1*Power(t2,2) - 8*Power(t2,3)) + 
            Power(s2,2)*Power(t1,2)*
             (Power(t1,3) + 3*t1*Power(t2,2) + 5*Power(t2,3)) + 
            Power(s2,3)*(-5*Power(t1,4) - 2*Power(t1,3)*t2 + 
               7*t1*Power(t2,3))) + 
         Power(s1,4)*s2*t2*(-Power(s2,8) - 
            Power(t1,5)*Power(t2,2)*(3*t1 + 2*t2) + 
            Power(s2,7)*(8*t1 + 11*t2) + 
            Power(s2,6)*(-24*Power(t1,2) - 52*t1*t2 + 9*Power(t2,2)) + 
            Power(s2,5)*(36*Power(t1,3) + 94*Power(t1,2)*t2 - 
               29*t1*Power(t2,2) - 5*Power(t2,3)) + 
            s2*Power(t1,3)*t2*
             (3*Power(t1,3) + 6*Power(t1,2)*t2 + 12*t1*Power(t2,2) + 
               Power(t2,3)) - 
            Power(s2,2)*Power(t1,3)*
             (2*Power(t1,3) + 15*Power(t1,2)*t2 - 13*t1*Power(t2,2) + 
               31*Power(t2,3)) + 
            Power(s2,3)*t1*(12*Power(t1,4) + 44*Power(t1,3)*t2 - 
               44*Power(t1,2)*Power(t2,2) + 25*t1*Power(t2,3) - 
               5*Power(t2,4)) + 
            Power(s2,4)*(-29*Power(t1,4) - 85*Power(t1,3)*t2 + 
               48*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) + 
               4*Power(t2,4))) + 
         Power(s1,5)*(-(Power(t1,6)*Power(t2,3)) - 
            Power(s2,8)*(t1 + 3*t2) + 
            s2*Power(t1,4)*Power(t2,2)*
             (4*Power(t1,2) + 9*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,7)*(5*Power(t1,2) + 18*t1*t2 + 8*Power(t2,2)) - 
            Power(s2,6)*(10*Power(t1,3) + 42*Power(t1,2)*t2 + 
               28*t1*Power(t2,2) - 13*Power(t2,3)) - 
            Power(s2,2)*Power(t1,3)*t2*
             (Power(t1,3) + 12*Power(t1,2)*t2 + 18*t1*Power(t2,2) + 
               8*Power(t2,3)) + 
            Power(s2,5)*(10*Power(t1,4) + 49*Power(t1,3)*t2 + 
               30*Power(t1,2)*Power(t2,2) - 27*t1*Power(t2,3) - 
               6*Power(t2,4)) + 
            Power(s2,3)*Power(t1,2)*
             (Power(t1,4) + 9*Power(t1,3)*t2 + 
               10*Power(t1,2)*Power(t2,2) + 12*t1*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(s2,4)*t1*(-5*Power(t1,4) - 30*Power(t1,3)*t2 - 
               12*Power(t1,2)*Power(t2,2) + 12*t1*Power(t2,3) + 
               9*Power(t2,4))) + 
         Power(s,7)*(-(Power(s2,2)*Power(t1 - t2,2)*Power(t2,3)) + 
            s1*Power(s2,2)*Power(t2,3)*(-t1 + t2) + 
            Power(s1,2)*(t1*Power(t1 - t2,2)*Power(t2,2) + 
               s2*t1*Power(t2,2)*(-t1 + t2) + 
               Power(s2,2)*(Power(t1,3) + t1*Power(t2,2) + 2*Power(t2,3))\
)) + Power(s,6)*(Power(s2,2)*(t1 - t2)*Power(t2,3)*
             (s2*(5*t1 - 7*t2) + 4*t1*(-t1 + t2)) + 
            s1*Power(s2,2)*Power(t2,2)*
             (s2*(2*Power(t1,2) + t1*t2 - 6*Power(t2,2)) + 
               t2*(Power(t1,2) - 6*t1*t2 + 5*Power(t2,2))) + 
            Power(s1,3)*(t1*Power(t2,2)*
                (-5*Power(t1,2) + 8*t1*t2 - 3*Power(t2,2)) + 
               s2*Power(t2,2)*(4*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               2*Power(s2,3)*(Power(t1,2) + 2*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-5*Power(t1,3) + Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) - 4*Power(t2,3))) + 
            Power(s1,2)*(t1*Power(t1 - t2,2)*Power(t2,2)*(3*t1 + t2) - 
               2*s2*t1*Power(t2,2)*
                (5*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) - 
               Power(s2,3)*(4*Power(t1,3) + Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,2)*(3*Power(t1,4) + Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) + 11*t1*Power(t2,3) - 
                  Power(t2,4)))) + 
         s*(Power(s2,4)*Power(s2 - t1,3)*Power(t2,4)*
             (2*s2*t1 - 2*Power(t1,2) - 7*s2*t2 + 3*t1*t2) + 
            Power(s1,7)*(s2 - t1)*
             (2*Power(s2,4)*t1 + 2*Power(t1,3)*Power(t2,2) + 
               2*Power(s2,2)*t1*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(-4*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2))) + 
            s1*Power(s2,3)*Power(s2 - t1,2)*Power(t2,3)*
             (2*Power(t1,2)*(2*t1 - 3*t2)*t2 + 
               Power(s2,3)*(4*t1 + t2) + 
               s2*t1*(2*Power(t1,2) - t1*t2 + 30*Power(t2,2)) - 
               Power(s2,2)*(6*Power(t1,2) + 7*t1*t2 + 30*Power(t2,2))) + 
            Power(s1,6)*(7*Power(t1,5)*Power(t2,2) + 
               Power(s2,6)*(7*t1 + 6*t2) - 
               s2*Power(t1,3)*Power(t2,2)*(23*t1 + 8*t2) + 
               Power(s2,5)*(-28*Power(t1,2) - 17*t1*t2 + 
                  9*Power(t2,2)) + 
               Power(s2,4)*(42*Power(t1,3) + 12*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) - 19*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*
                (7*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,3)*t1*
                (-28*Power(t1,3) + 3*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) + 16*Power(t2,3))) + 
            Power(s1,2)*Power(s2,2)*(s2 - t1)*Power(t2,2)*
             (Power(s2,5)*(5*t1 + t2) + 
               2*Power(t1,3)*t2*(Power(t1,2) - 2*Power(t2,2)) + 
               Power(s2,4)*(-20*Power(t1,2) + 19*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(31*Power(t1,3) - 54*Power(t1,2)*t2 - 
                  35*Power(t2,3)) + 
               2*s2*Power(t1,2)*
                (3*Power(t1,3) - 8*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,2)*t1*
                (-22*Power(t1,3) + 48*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) + 35*Power(t2,3))) + 
            Power(s1,5)*(Power(s2,7)*(6*t1 + 13*t2) + 
               2*Power(t1,4)*Power(t2,2)*
                (-2*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               s2*Power(t1,3)*Power(t2,2)*
                (37*Power(t1,2) + 24*t1*t2 + 2*Power(t2,2)) - 
               Power(s2,6)*(30*Power(t1,2) + 66*t1*t2 + 
                  19*Power(t2,2)) + 
               Power(s2,5)*(58*Power(t1,3) + 117*Power(t1,2)*t2 + 
                  65*t1*Power(t2,2) - 46*Power(t2,3)) + 
               Power(s2,3)*t1*
                (24*Power(t1,4) + 30*Power(t1,3)*t2 + 
                  33*Power(t1,2)*Power(t2,2) + 28*t1*Power(t2,3) - 
                  13*Power(t2,4)) - 
               Power(s2,2)*Power(t1,2)*
                (4*Power(t1,4) + 3*Power(t1,3)*t2 + 
                  58*Power(t1,2)*Power(t2,2) + 53*t1*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(s2,4)*(-54*Power(t1,4) - 91*Power(t1,3)*t2 - 
                  54*Power(t1,2)*Power(t2,2) + 55*t1*Power(t2,3) + 
                  19*Power(t2,4))) - 
            Power(s1,3)*s2*t2*
             (2*Power(t1,5)*Power(t2,2)*(-3*t1 + 2*t2) + 
               Power(s2,7)*(4*t1 + 21*t2) + 
               Power(s2,6)*(-19*Power(t1,2) - 116*t1*t2 + 
                  8*Power(t2,2)) + 
               Power(s2,5)*(37*Power(t1,3) + 241*Power(t1,2)*t2 - 
                  56*t1*Power(t2,2) + 3*Power(t2,3)) + 
               s2*Power(t1,3)*t2*
                (6*Power(t1,3) + Power(t1,2)*t2 + 16*t1*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,3)*t1*
                (19*Power(t1,4) + 146*Power(t1,3)*t2 - 
                  197*Power(t1,2)*Power(t2,2) + 163*t1*Power(t2,3) - 
                  46*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (-4*Power(t1,4) - 46*Power(t1,3)*t2 + 
                  88*Power(t1,2)*Power(t2,2) - 115*t1*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(s2,4)*(-37*Power(t1,4) - 252*Power(t1,3)*t2 + 
                  162*Power(t1,2)*Power(t2,2) - 69*t1*Power(t2,3) + 
                  33*Power(t2,4))) + 
            Power(s1,4)*(Power(t1,5)*(3*t1 - 2*t2)*Power(t2,3) + 
               Power(s2,8)*(t1 + 6*t2) + 
               s2*Power(t1,4)*Power(t2,2)*
                (-12*Power(t1,2) - 23*t1*t2 + 2*Power(t2,2)) - 
               Power(s2,7)*(7*Power(t1,2) + 47*t1*t2 + 45*Power(t2,2)) + 
               Power(s2,6)*(18*Power(t1,3) + 123*Power(t1,2)*t2 + 
                  179*t1*Power(t2,2) - 33*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (3*Power(t1,4) + 45*Power(t1,3)*t2 + 
                  33*Power(t1,2)*Power(t2,2) + 40*t1*Power(t2,3) - 
                  Power(t2,4)) + 
               Power(s2,5)*(-22*Power(t1,4) - 151*Power(t1,3)*t2 - 
                  238*Power(t1,2)*Power(t2,2) + 65*t1*Power(t2,3) + 
                  27*Power(t2,4)) + 
               Power(s2,4)*(13*Power(t1,5) + 94*Power(t1,4)*t2 + 
                  145*Power(t1,3)*Power(t2,2) - 
                  44*Power(t1,2)*Power(t2,3) + 4*t1*Power(t2,4) - 
                  13*Power(t2,5)) - 
               Power(s2,3)*t1*
                (3*Power(t1,5) + 28*Power(t1,4)*t2 + 
                  74*Power(t1,3)*Power(t2,2) + 77*t1*Power(t2,4) - 
                  12*Power(t2,5)))) - 
         Power(s,2)*(Power(s1,7)*
             (Power(s2,4)*t1 + Power(t1,3)*Power(t2,2) + 
               Power(s2,2)*t1*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(-2*Power(t1,2) + t1*t2 + 2*Power(t2,2))) - 
            Power(s2,3)*Power(s2 - t1,2)*Power(t2,3)*
             (Power(t1,2)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) - 
               2*s2*t1*(Power(t1,2) - 8*t1*t2 + 9*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) - 12*t1*t2 + 21*Power(t2,2))) + 
            Power(s1,6)*(Power(t1,3)*Power(t2,2)*(-11*t1 + 3*t2) + 
               Power(s2,5)*(8*t1 + 6*t2) + 
               s2*Power(t1,2)*Power(t2,2)*(17*t1 + 7*t2) + 
               Power(s2,4)*(-27*Power(t1,2) - 10*t1*t2 + 
                  13*Power(t2,2)) + 
               Power(s2,3)*(30*Power(t1,3) - 4*Power(t1,2)*t2 - 
                  t1*Power(t2,2) - 15*Power(t2,3)) - 
               Power(s2,2)*t1*
                (11*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) + 4*Power(t2,3))) - 
            s1*Power(s2,2)*(s2 - t1)*Power(t2,2)*
             (-3*Power(t1,3)*Power(t1 - t2,2)*t2 + 
               Power(s2,4)*(2*Power(t1,2) - 19*t1*t2 - 6*Power(t2,2)) + 
               Power(s2,2)*t1*
                (4*Power(t1,3) - 28*Power(t1,2)*t2 + 
                  48*t1*Power(t2,2) - 105*Power(t2,3)) - 
               s2*Power(t1,2)*
                (Power(t1,3) - 12*Power(t1,2)*t2 + 39*t1*Power(t2,2) - 
                  39*Power(t2,3)) + 
               Power(s2,3)*(-5*Power(t1,3) + 38*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + 75*Power(t2,3))) + 
            Power(s1,5)*(3*Power(s2,6)*(4*t1 + 7*t2) + 
               3*Power(t1,3)*Power(t2,2)*
                (6*Power(t1,2) - t1*t2 - Power(t2,2)) - 
               2*s2*Power(t1,2)*Power(t2,2)*
                (39*Power(t1,2) + 7*t1*t2 + Power(t2,2)) - 
               Power(s2,5)*(57*Power(t1,2) + 87*t1*t2 + 
                  16*Power(t2,2)) + 
               Power(s2,4)*(96*Power(t1,3) + 99*Power(t1,2)*t2 + 
                  58*t1*Power(t2,2) - 61*Power(t2,3)) + 
               Power(s2,2)*t1*
                (18*Power(t1,4) - 3*Power(t1,3)*t2 + 
                  66*Power(t1,2)*Power(t2,2) + 68*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,3)*(-69*Power(t1,4) - 30*Power(t1,3)*t2 - 
                  42*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
                  22*Power(t2,4))) + 
            Power(s1,2)*s2*t2*
             (3*Power(t1,4)*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s2,6)*(Power(t1,2) + 19*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,5)*(-5*Power(t1,3) - 82*Power(t1,2)*t2 + 
                  47*t1*Power(t2,2) + 5*Power(t2,3)) - 
               s2*Power(t1,3)*t2*
                (3*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s2,4)*(9*Power(t1,4) + 137*Power(t1,3)*t2 - 
                  180*Power(t1,2)*Power(t2,2) + 58*t1*Power(t2,3) - 
                  70*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (2*Power(t1,4) + 36*Power(t1,3)*t2 - 
                  103*Power(t1,2)*Power(t2,2) + 109*t1*Power(t2,3) - 
                  18*Power(t2,4)) + 
               Power(s2,3)*t1*
                (-7*Power(t1,4) - 107*Power(t1,3)*t2 + 
                  212*Power(t1,2)*Power(t2,2) - 159*t1*Power(t2,3) + 
                  99*Power(t2,4))) + 
            Power(s1,3)*(Power(t1,4)*Power(t2,3)*
                (3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) - 
               2*Power(s2,7)*
                (Power(t1,2) + 10*t1*t2 + 24*Power(t2,2)) + 
               Power(s2,6)*(9*Power(t1,3) + 76*Power(t1,2)*t2 + 
                  215*t1*Power(t2,2) + 2*Power(t2,3)) - 
               s2*Power(t1,3)*Power(t2,2)*
                (12*Power(t1,3) + 19*Power(t1,2)*t2 - 
                  25*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (3*Power(t1,4) + 54*Power(t1,3)*t2 - 
                  21*Power(t1,2)*Power(t2,2) + 61*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,5)*(-15*Power(t1,4) - 115*Power(t1,3)*t2 - 
                  335*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,4)*(11*Power(t1,5) + 85*Power(t1,4)*t2 + 
                  257*Power(t1,3)*Power(t2,2) - 
                  115*Power(t1,2)*Power(t2,3) + 136*t1*Power(t2,4) - 
                  52*Power(t2,5)) - 
               Power(s2,3)*t1*
                (3*Power(t1,5) + 29*Power(t1,4)*t2 + 
                  131*Power(t1,3)*Power(t2,2) - 
                  129*Power(t1,2)*Power(t2,3) + 232*t1*Power(t2,4) - 
                  46*Power(t2,5))) + 
            Power(s1,4)*(2*Power(s2,7)*(2*t1 + 7*t2) - 
               Power(s2,6)*(29*Power(t1,2) + 104*t1*t2 + 
                  77*Power(t2,2)) + 
               Power(s2,5)*(70*Power(t1,3) + 215*Power(t1,2)*t2 + 
                  240*t1*Power(t2,2) - 47*Power(t2,3)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (69*Power(t1,3) + 20*Power(t1,2)*t2 - 
                  10*t1*Power(t2,2) + Power(t2,3)) - 
               Power(t1,3)*Power(t2,2)*
                (6*Power(t1,3) + 6*Power(t1,2)*t2 - 9*t1*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(s2,4)*(75*Power(t1,4) + 185*Power(t1,3)*t2 + 
                  212*Power(t1,2)*Power(t2,2) + 6*t1*Power(t2,3) - 
                  55*Power(t2,4)) + 
               Power(s2,3)*(36*Power(t1,5) + 69*Power(t1,4)*t2 + 
                  117*Power(t1,3)*Power(t2,2) + 
                  95*Power(t1,2)*Power(t2,3) + 12*t1*Power(t2,4) - 
                  15*Power(t2,5)) - 
               Power(s2,2)*t1*
                (6*Power(t1,5) + 9*Power(t1,4)*t2 + 
                  129*Power(t1,3)*Power(t2,2) + 
                  53*Power(t1,2)*Power(t2,3) + 53*t1*Power(t2,4) - 
                  8*Power(t2,5)))) + 
         Power(s,3)*(-(Power(s2,2)*(s2 - t1)*Power(t2,3)*
               (-(Power(t1,3)*Power(t1 - t2,2)) + 
                 Power(s2,2)*t1*
                  (-11*Power(t1,2) + 50*t1*t2 - 45*Power(t2,2)) + 
                 5*Power(s2,3)*
                  (Power(t1,2) - 6*t1*t2 + 7*Power(t2,2)) + 
                 s2*Power(t1,2)*
                  (7*Power(t1,2) - 22*t1*t2 + 15*Power(t2,2)))) + 
            s1*Power(s2,2)*Power(t2,2)*
             (Power(t1,3)*t2*
                (13*Power(t1,2) - 27*t1*t2 + 14*Power(t2,2)) + 
               Power(s2,4)*(-8*Power(t1,2) + 35*t1*t2 + 
                  15*Power(t2,2)) + 
               Power(s2,3)*(21*Power(t1,3) - 88*Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) - 100*Power(t2,3)) + 
               s2*Power(t1,2)*
                (5*Power(t1,3) - 50*Power(t1,2)*t2 + 
                  117*t1*Power(t2,2) - 96*Power(t2,3)) - 
               2*Power(s2,2)*t1*
                (9*Power(t1,3) - 45*Power(t1,2)*t2 + 
                  68*t1*Power(t2,2) - 90*Power(t2,3))) + 
            Power(s1,6)*(Power(t1,2)*(5*t1 - 2*t2)*Power(t2,2) + 
               Power(s2,4)*(3*t1 + 2*t2) - 
               s2*t1*Power(t2,2)*(t1 + 4*t2) - 
               Power(s2,3)*(8*Power(t1,2) + t1*t2 - 4*Power(t2,2)) + 
               Power(s2,2)*(5*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - 4*Power(t2,3))) + 
            Power(s1,5)*(5*Power(s2,5)*(2*t1 + 3*t2) + 
               2*Power(t1,3)*Power(t2,2)*(-12*t1 + 7*t2) + 
               s2*Power(t1,2)*Power(t2,2)*(49*t1 + 10*t2) - 
               Power(s2,4)*(44*Power(t1,2) + 48*t1*t2 + 
                  7*Power(t2,2)) + 
               Power(s2,3)*(58*Power(t1,3) + 18*Power(t1,2)*t2 + 
                  31*t1*Power(t2,2) - 36*Power(t2,3)) + 
               Power(s2,2)*(-24*Power(t1,4) + 11*Power(t1,3)*t2 - 
                  24*Power(t1,2)*Power(t2,2) - 34*t1*Power(t2,3) + 
                  11*Power(t2,4))) + 
            Power(s1,2)*(Power(t1,4)*Power(t1 - t2,2)*Power(t2,3) - 
               s2*Power(t1,3)*Power(t2,2)*
                (4*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  21*t1*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s2,6)*(Power(t1,3) + 4*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,5)*(-3*Power(t1,4) - 16*Power(t1,3)*t2 - 
                  99*Power(t1,2)*Power(t2,2) + 29*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (Power(t1,4) + 21*Power(t1,3)*t2 - 
                  50*Power(t1,2)*Power(t2,2) + 46*t1*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,4)*(3*Power(t1,5) + 21*Power(t1,4)*t2 + 
                  131*Power(t1,3)*Power(t2,2) - 
                  152*Power(t1,2)*Power(t2,3) + 123*t1*Power(t2,4) - 
                  70*Power(t2,5)) - 
               Power(s2,3)*t1*
                (Power(t1,5) + 10*Power(t1,4)*t2 + 
                  76*Power(t1,3)*Power(t2,2) - 
                  158*Power(t1,2)*Power(t2,3) + 200*t1*Power(t2,4) - 
                  56*Power(t2,5))) - 
            Power(s1,3)*(Power(s2,6)*
                (8*Power(t1,2) + 40*t1*t2 + 62*Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (-55*Power(t1,3) + 12*Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) - 11*Power(t2,3)) + 
               4*Power(t1,3)*Power(t2,2)*
                (Power(t1,3) + Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(s2,5)*(32*Power(t1,3) + 113*Power(t1,2)*t2 + 
                  204*t1*Power(t2,2) + 28*Power(t2,3)) + 
               Power(s2,4)*(44*Power(t1,4) + 120*Power(t1,3)*t2 + 
                  213*Power(t1,2)*Power(t2,2) + 101*t1*Power(t2,3) - 
                  30*Power(t2,4)) - 
               Power(s2,3)*(24*Power(t1,5) + 56*Power(t1,4)*t2 + 
                  143*Power(t1,3)*Power(t2,2) + 
                  36*Power(t1,2)*Power(t2,3) + 117*t1*Power(t2,4) - 
                  38*Power(t2,5)) + 
               Power(s2,2)*t1*
                (4*Power(t1,5) + 9*Power(t1,4)*t2 + 
                  120*Power(t1,3)*Power(t2,2) - 
                  51*Power(t1,2)*Power(t2,3) + 101*t1*Power(t2,4) - 
                  12*Power(t2,5))) + 
            Power(s1,4)*(2*Power(s2,6)*(3*t1 + 8*t2) - 
               2*s2*Power(t1,2)*Power(t2,2)*
                (58*Power(t1,2) - 13*t1*t2 + Power(t2,2)) - 
               Power(s2,5)*(45*Power(t1,2) + 110*t1*t2 + 
                  71*Power(t2,2)) + 
               Power(s2,4)*(96*Power(t1,3) + 153*Power(t1,2)*t2 + 
                  161*t1*Power(t2,2) - 33*Power(t2,3)) + 
               Power(t1,2)*Power(t2,2)*
                (22*Power(t1,3) - 9*Power(t1,2)*t2 - 9*t1*Power(t2,2) + 
                  2*Power(t2,3)) - 
               Power(s2,3)*(79*Power(t1,4) + 65*Power(t1,3)*t2 + 
                  94*Power(t1,2)*Power(t2,2) + 100*t1*Power(t2,3) - 
                  53*Power(t2,4)) + 
               Power(s2,2)*(22*Power(t1,5) + 5*Power(t1,4)*t2 + 
                  113*Power(t1,3)*Power(t2,2) + 
                  86*Power(t1,2)*Power(t2,3) + 7*t1*Power(t2,4) - 
                  7*Power(t2,5)))) + 
         Power(s,5)*(Power(s2,2)*Power(t2,3)*
             (-6*Power(t1,2)*Power(t1 - t2,2) + 
               Power(s2,2)*(-10*Power(t1,2) + 30*t1*t2 - 
                  21*Power(t2,2)) + 
               8*s2*t1*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2))) + 
            s1*Power(s2,2)*Power(t2,2)*
             (6*t1*t2*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(-8*Power(t1,2) + 10*t1*t2 + 
                  15*Power(t2,2)) + 
               s2*(7*Power(t1,3) - 18*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) - 30*Power(t2,3))) + 
            Power(s1,4)*(Power(s2,4)*(t1 + 2*t2) + 
               t1*Power(t2,2)*
                (10*Power(t1,2) - 12*t1*t2 + 3*Power(t2,2)) + 
               s2*Power(t2,2)*
                (-6*Power(t1,2) - 9*t1*t2 + 4*Power(t2,2)) - 
               Power(s2,3)*(8*Power(t1,2) + 11*t1*t2 + 8*Power(t2,2)) + 
               2*Power(s2,2)*
                (5*Power(t1,3) - 2*Power(t1,2)*t2 + 5*t1*Power(t2,2) - 
                  Power(t2,3))) - 
            Power(s1,3)*(Power(s2,4)*
                (8*Power(t1,2) + 20*t1*t2 + 21*Power(t2,2)) + 
               s2*Power(t2,2)*
                (-41*Power(t1,3) + 36*Power(t1,2)*t2 - 
                  10*t1*Power(t2,2) + Power(t2,3)) + 
               2*t1*Power(t2,2)*
                (7*Power(t1,3) - 9*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  Power(t2,3)) - 
               4*Power(s2,3)*
                (6*Power(t1,3) + 4*Power(t1,2)*t2 + 8*t1*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(s2,2)*(14*Power(t1,4) + Power(t1,3)*t2 + 
                  22*Power(t1,2)*Power(t2,2) + 47*t1*Power(t2,3) - 
                  13*Power(t2,4))) + 
            Power(s1,2)*(3*Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*
                (t1 + t2) - 3*s2*t1*Power(t2,2)*
                (7*Power(t1,3) - 10*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,4)*(6*Power(t1,3) + 4*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) + 25*Power(t2,3)) - 
               Power(s2,3)*(9*Power(t1,4) + 8*Power(t1,3)*t2 + 
                  19*Power(t1,2)*Power(t2,2) + 40*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,2)*(3*Power(t1,5) + 3*Power(t1,4)*t2 + 
                  27*Power(t1,3)*Power(t2,2) - 
                  21*Power(t1,2)*Power(t2,3) + 37*t1*Power(t2,4) - 
                  7*Power(t2,5)))) + 
         Power(s,4)*(Power(s2,2)*Power(t2,3)*
             (-4*Power(t1,3)*Power(t1 - t2,2) + 
               6*s2*Power(t1,2)*
                (3*Power(t1,2) - 8*t1*t2 + 5*Power(t2,2)) + 
               5*Power(s2,3)*(2*Power(t1,2) - 8*t1*t2 + 7*Power(t2,2)) - 
               4*Power(s2,2)*t1*
                (6*Power(t1,2) - 20*t1*t2 + 15*Power(t2,2))) + 
            Power(s1,5)*(-(Power(s2,4)*(3*t1 + 4*t2)) + 
               s2*Power(t2,2)*
                (4*Power(t1,2) + 11*t1*t2 - 2*Power(t2,2)) - 
               t1*Power(t2,2)*(10*Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(12*Power(t1,2) + 9*t1*t2 + 2*Power(t2,2)) - 
               2*Power(s2,2)*(5*Power(t1,3) - 3*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - 4*Power(t2,3))) + 
            s1*Power(s2,2)*Power(t2,2)*
             (2*Power(s2,3)*(6*Power(t1,2) - 15*t1*t2 - 
                  10*Power(t2,2)) + 
               4*Power(t1,2)*t2*
                (5*Power(t1,2) - 11*t1*t2 + 6*Power(t2,2)) + 
               s2*t1*(9*Power(t1,3) - 54*Power(t1,2)*t2 + 
                  109*t1*Power(t2,2) - 90*Power(t2,3)) + 
               Power(s2,2)*(-21*Power(t1,3) + 62*Power(t1,2)*t2 - 
                  45*t1*Power(t2,2) + 75*Power(t2,3))) + 
            Power(s1,4)*(-(Power(s2,5)*(4*t1 + 9*t2)) + 
               Power(s2,4)*(31*Power(t1,2) + 56*t1*t2 + 
                  36*Power(t2,2)) - 
               Power(s2,3)*(54*Power(t1,3) + 33*Power(t1,2)*t2 + 
                  58*t1*Power(t2,2) - 12*Power(t2,3)) + 
               s2*Power(t2,2)*
                (-65*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + Power(t2,3)) + 
               t1*Power(t2,2)*
                (26*Power(t1,3) - 24*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,2)*(26*Power(t1,4) - 5*Power(t1,3)*t2 + 
                  32*Power(t1,2)*Power(t2,2) + 67*t1*Power(t2,3) - 
                  24*Power(t2,4))) + 
            Power(s1,3)*(4*Power(s2,5)*
                (3*Power(t1,2) + 10*t1*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*Power(t2,2)*
                (-13*Power(t1,3) + 9*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) - 5*Power(t2,3)) + 
               s2*t1*Power(t2,2)*
                (80*Power(t1,3) - 62*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 5*Power(t2,3)) - 
               Power(s2,4)*(42*Power(t1,3) + 73*Power(t1,2)*t2 + 
                  107*t1*Power(t2,2) + 37*Power(t2,3)) + 
               Power(s2,3)*(43*Power(t1,4) + 43*Power(t1,3)*t2 + 
                  74*Power(t1,2)*Power(t2,2) + 130*t1*Power(t2,3) - 
                  30*Power(t2,4)) - 
               Power(s2,2)*(13*Power(t1,5) + 8*Power(t1,4)*t2 + 
                  90*Power(t1,3)*Power(t2,2) + 
                  11*Power(t1,2)*Power(t2,3) + 45*t1*Power(t2,4) - 
                  12*Power(t2,5))) + 
            Power(s1,2)*(Power(t1,3)*Power(t1 - t2,2)*Power(t2,2)*
                (t1 + 3*t2) + 
               s2*Power(t1,2)*Power(t2,2)*
                (-16*Power(t1,3) + 13*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) - 15*Power(t2,3)) - 
               2*Power(s2,5)*(2*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s2,4)*(9*Power(t1,4) + 18*Power(t1,3)*t2 + 
                  56*Power(t1,2)*Power(t2,2) + 29*t1*Power(t2,3) - 
                  10*Power(t2,4)) - 
               Power(s2,3)*(6*Power(t1,5) + 15*Power(t1,4)*t2 + 
                  63*Power(t1,3)*Power(t2,2) - 
                  48*Power(t1,2)*Power(t2,3) + 102*t1*Power(t2,4) - 
                  35*Power(t2,5)) + 
               Power(s2,2)*t1*
                (Power(t1,5) + 3*Power(t1,4)*t2 + 
                  40*Power(t1,3)*Power(t2,2) - 
                  68*Power(t1,2)*Power(t2,3) + 76*t1*Power(t2,4) - 
                  4*Power(t2,5)))))*lg2(s - s1 + t2))/
     (Power(s1,3)*Power(s2,3)*t1*Power(s - s2 + t1,3)*(s - s1 - s2 + t1)*
       Power(t2,3)*(s - s1 + t2)) - 
    (8*(Power(s1,6)*(s2 + t1) + 
         Power(s1,5)*(3*Power(s2,2) + s2*(t1 - 3*t2) + 
            2*t1*(t1 - 2*t2)) - 
         s2*t1*Power(t2,3)*(2*Power(s2,2) - 2*s2*t2 + Power(t2,2)) + 
         s1*Power(t2,2)*(Power(s2,3)*(7*t1 - 3*t2) - 
            7*Power(s2,2)*t1*t2 - s2*t1*(t1 - 7*t2)*t2 + 
            t1*(t1 - 2*t2)*Power(t2,2)) + 
         Power(s,4)*(Power(s1,2)*(t1 - t2) + 
            s1*(2*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
            t1*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,2)*t2*(Power(s2,3)*(-7*t1 + 10*t2) + 
            s2*t1*(-2*Power(t1,2) + t1*t2 - 14*Power(t2,2)) + 
            Power(s2,2)*(Power(t1,2) + 8*t1*t2 - 2*Power(t2,2)) + 
            t1*t2*(Power(t1,2) - 4*t1*t2 + 8*Power(t2,2))) + 
         Power(s1,4)*(4*Power(s2,3) - 8*Power(s2,2)*t2 + 
            s2*(2*Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
            t1*(2*Power(t1,2) - 5*t1*t2 + 9*Power(t2,2))) + 
         Power(s1,3)*(Power(s2,3)*(2*t1 - 11*t2) + 
            Power(s2,2)*t2*(-3*t1 + 7*t2) - 
            3*t1*t2*(Power(t1,2) - 2*t1*t2 + 4*Power(t2,2)) + 
            s2*(2*Power(t1,3) - Power(t1,2)*t2 + 11*t1*Power(t2,2) - 
               Power(t2,3))) - 
         Power(s,3)*(Power(s1,3)*(s2 + 4*t1 - 3*t2) + 
            Power(s1,2)*(8*Power(t1,2) + s2*(t1 - 5*t2) - 19*t1*t2 + 
               4*Power(t2,2)) + 
            s1*(8*Power(t1,3) - 20*Power(t1,2)*t2 + 20*t1*Power(t2,2) - 
               Power(t2,3) + 
               s2*(2*Power(t1,2) - 15*t1*t2 + 5*Power(t2,2))) + 
            t1*(-3*t2*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
               s2*(2*Power(t1,2) - 9*t1*t2 + 8*Power(t2,2)))) + 
         Power(s,2)*(3*Power(s1,4)*(s2 + 2*t1 - t2) + 
            Power(s1,3)*(3*Power(s2,2) + 3*s2*t1 + 12*Power(t1,2) - 
               13*s2*t2 - 27*t1*t2 + 5*Power(t2,2)) + 
            t1*t2*(Power(s2,2)*(-4*t1 + 7*t2) + 
               s2*(-2*Power(t1,2) + 12*t1*t2 - 13*Power(t2,2)) + 
               t2*(Power(t1,2) - 5*t1*t2 + 4*Power(t2,2))) + 
            Power(s1,2)*(12*Power(t1,3) - 11*Power(s2,2)*t2 - 
               30*Power(t1,2)*t2 + 40*t1*Power(t2,2) - 2*Power(t2,3) + 
               2*s2*(3*Power(t1,2) - 17*t1*t2 + 7*Power(t2,2))) + 
            s1*(Power(s2,3)*(t1 + t2) + Power(s2,2)*t2*(-16*t1 + 9*t2) - 
               3*t1*t2*(3*Power(t1,2) - 8*t1*t2 + 8*Power(t2,2)) + 
               s2*(6*Power(t1,3) - 19*Power(t1,2)*t2 + 
                  37*t1*Power(t2,2) - 4*Power(t2,3)))) + 
         s*(Power(s1,5)*(-3*s2 - 4*t1 + t2) - 
            Power(s1,4)*(6*Power(s2,2) + 3*s2*t1 + 8*Power(t1,2) - 
               11*s2*t2 - 17*t1*t2 + 2*Power(t2,2)) + 
            t1*Power(t2,2)*(-2*Power(s2,3) + 3*s2*(t1 - 2*t2)*t2 + 
               Power(t2,2)*(-t1 + t2) + Power(s2,2)*(-4*t1 + 9*t2)) + 
            Power(s1,3)*(-4*Power(s2,3) - 8*Power(t1,3) + 
               19*Power(s2,2)*t2 + 20*Power(t1,2)*t2 - 
               32*t1*Power(t2,2) + Power(t2,3) + 
               s2*(-6*Power(t1,2) + 23*t1*t2 - 12*Power(t2,2))) - 
            s1*t2*(Power(s2,4) + Power(s2,3)*(-8*t1 + 5*t2) + 
               s2*t1*(-4*Power(t1,2) + 13*t1*t2 - 27*Power(t2,2)) - 
               3*Power(s2,2)*(Power(t1,2) - 8*t1*t2 + 2*Power(t2,2)) + 
               t1*t2*(2*Power(t1,2) - 9*t1*t2 + 12*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,4) - 3*Power(s2,3)*(t1 - 3*t2) + 
               19*Power(s2,2)*(t1 - t2)*t2 + 
               3*t1*t2*(3*Power(t1,2) - 7*t1*t2 + 10*Power(t2,2)) + 
               s2*(-6*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  40*t1*Power(t2,2) + 4*Power(t2,3)))))*
       lg2(-((-s + s2 - t1)/(s2*(-s1 - t1 + t2)))))/
     (s1*s2*(-s + s2 - t1)*t1*t2*Power(s - s1 + t2,3)) - 
    (8*(Power(s1,4)*(2*Power(s2,3) + Power(s2,2)*t1 + Power(t1,3)) - 
         Power(s,4)*t1*(t1 - t2)*(s2 - t1 + t2) + 
         Power(s2,3)*t1*t2*((t1 - 2*t2)*t2 + s2*(-t1 + t2)) + 
         Power(s1,3)*(2*Power(s2,4) + 2*Power(s2,3)*(t1 - 3*t2) - 
            4*Power(s2,2)*t1*t2 - Power(t1,3)*t2 + 
            s2*Power(t1,2)*(4*t1 + 3*t2)) + 
         Power(s1,2)*s2*(Power(s2,3)*(2*t1 - 5*t2) - 
            Power(t1,2)*t2*(3*t1 + 2*t2) + 
            s2*t1*(3*Power(t1,2) + 3*t1*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*(Power(t1,2) - 9*t1*t2 + 6*Power(t2,2))) + 
         s1*Power(s2,2)*(-(t1*t2*(Power(t1,2) + t1*t2 + Power(t2,2))) + 
            Power(s2,2)*(Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) + 
            s2*(Power(t1,3) - Power(t1,2)*t2 + 8*t1*Power(t2,2) - 
               2*Power(t2,3))) - 
         Power(s,3)*(s1*(Power(s2,2)*t1 + 
               2*t1*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               s2*(-3*Power(t1,2) - 2*t1*t2 + 2*Power(t2,2))) + 
            t1*(Power(s2,2)*t2 - Power(t1 - t2,2)*t2 + 
               s2*(4*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2)))) + 
         s*(Power(s1,3)*(-2*Power(s2,3) + 2*Power(t1,2)*(-2*t1 + t2) + 
               Power(s2,2)*(-3*t1 + 4*t2) + s2*t1*(t1 + 4*t2)) + 
            Power(s1,2)*(Power(t1,2)*(3*t1 - 2*t2)*t2 + 
               7*Power(s2,2)*(3*t1 - t2)*t2 + 
               Power(s2,3)*(-2*t1 + 9*t2) + 
               s2*t1*(-12*Power(t1,2) + t1*t2 - 2*Power(t2,2))) + 
            Power(s2,2)*t1*(t2*
                (Power(t1,2) - 6*t1*t2 + 4*Power(t2,2)) - 
               s2*(Power(t1,2) - 5*t1*t2 + 5*Power(t2,2))) - 
            s1*s2*(2*Power(t1,2)*t2*(-3*t1 + 2*t2) + 
               Power(s2,2)*(Power(t1,2) - 15*t1*t2 + 8*Power(t2,2)) + 
               s2*(6*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - 3*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,2)*
             (Power(s2,2)*(3*t1 - 4*t2) + 
               t1*(6*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               s2*(-3*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2))) + 
            s2*t1*(-3*Power(t1 - t2,2)*t2 + 
               s2*(3*Power(t1,2) - 11*t1*t2 + 6*Power(t2,2))) - 
            s1*(Power(s2,2)*(16*t1 - 7*t2)*t2 + 
               t1*t2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*(-12*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) + Power(t2,3)))))*
       lg2(-((-s + s2 - t1)/((-s + s1 - t2)*(-s1 - t1 + t2)))))/
     (s1*Power(s2,3)*t1*(s - s2 + t1)*(-s + s1 - t2)*t2) + 
    (8*(Power(s1,4)*(Power(s2,3) + s2*Power(t1,2) + 2*Power(t1,3)) + 
         Power(s,4)*t1*Power(t1 - t2,2) + 
         Power(s1,3)*(Power(s2,4) + Power(s2,3)*(3*t1 - 2*t2) + 
            2*Power(t1,3)*(t1 - t2) + s2*Power(t1,2)*(3*t1 - t2) - 
            Power(s2,2)*t1*(t1 + 3*t2)) + 
         Power(s1,2)*(Power(t1,4)*(t1 - 2*t2) + 
            2*Power(s2,4)*(t1 - t2) + s2*Power(t1,3)*(3*t1 - t2) + 
            Power(s2,3)*(2*Power(t1,2) - 7*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*t1*(-2*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2))) - 
         s2*t1*t2*(Power(s2,3)*(t1 - t2) + Power(t1,3)*(2*t1 - t2) + 
            Power(s2,2)*Power(t2,2) + 
            s2*(-2*Power(t1,3) + t1*Power(t2,2))) + 
         s1*(-(Power(t1,5)*t2) + 
            s2*Power(t1,3)*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
            Power(s2,4)*(Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
            Power(s2,3)*t1*(Power(t1,2) - t1*t2 + 5*Power(t2,2)) + 
            Power(s2,2)*t1*(-2*Power(t1,3) + 4*Power(t1,2)*t2 + 
               t1*Power(t2,2) - 2*Power(t2,3))) - 
         Power(s,3)*(t1*(t1 - t2)*(2*s2*(t1 - 2*t2) + t2*(-2*t1 + t2)) + 
            s1*(s2*(Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               t1*(4*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2)))) - 
         s*(Power(s1,3)*(Power(s2,3) + Power(s2,2)*(t1 - 2*t2) + 
               2*Power(t1,2)*(3*t1 - 2*t2) - 4*s2*t1*t2) + 
            Power(s1,2)*(Power(s2,3)*(3*t1 - 4*t2) + 
               Power(s2,2)*t2*(-15*t1 + 2*t2) + 
               Power(t1,2)*(4*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)) + 
               s2*t1*(3*Power(t1,2) - 8*t1*t2 + 7*Power(t2,2))) + 
            s1*(Power(s2,2)*t1*t2*(-9*t1 + 13*t2) + 
               Power(s2,3)*(2*Power(t1,2) - 10*t1*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(2*Power(t1,2) - 3*t1*t2 + 3*Power(t2,2)) + 
               s2*t1*(Power(t1,3) + Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
                  4*Power(t2,3))) + 
            t1*(Power(s2,3)*Power(t1 - 2*t2,2) + 
               Power(t1,3)*t2*(-t1 + t2) + 
               Power(s2,2)*(-2*Power(t1,3) + 3*t1*Power(t2,2) - 
                  3*Power(t2,3)) + 
               s2*t1*(2*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) - 
                  2*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,2)*
             (Power(s2,2)*(t1 - 2*t2) + s2*t2*(-8*t1 + t2) + 
               t1*(7*Power(t1,2) - 9*t1*t2 + 2*Power(t2,2))) + 
            s1*(Power(s2,2)*(2*Power(t1,2) - 11*t1*t2 + 3*Power(t2,2)) + 
               s2*t1*(2*Power(t1,2) - 15*t1*t2 + 11*Power(t2,2)) + 
               t1*(2*Power(t1,3) - 7*Power(t1,2)*t2 + 6*t1*Power(t2,2) - 
                  2*Power(t2,3))) + 
            t1*(Power(s2,2)*(2*Power(t1,2) - 7*t1*t2 + 6*Power(t2,2)) + 
               t1*(Power(t1,3) - Power(t1,2)*t2 + t1*Power(t2,2) - 
                  Power(t2,3)) - 
               s2*(2*Power(t1,3) + 2*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
                  3*Power(t2,3)))))*lg2(-(t2/(s1*(-s1 - t1 + t2)))))/
     (s1*s2*(-s + s2 - t1)*Power(t1,3)*(-s + s1 - t2)*t2) - 
    (8*(Power(s1,6)*(s2 + t1) + 
         Power(s1,5)*(Power(s2,2) + 3*s2*(t1 - t2) + 2*t1*(t1 - t2)) + 
         Power(s,4)*(s1 + t1)*(t1 - t2)*(s1 + t1 - t2) - 
         Power(s2,3)*t1*Power(t2,3) + 
         s1*Power(s2,2)*Power(t2,2)*
          (Power(s2,2) + 4*s2*t1 - 2*Power(t1,2) - s2*t2 - 3*t1*t2) - 
         Power(s1,2)*s2*t2*(2*Power(s2,3) + Power(s2,2)*(4*t1 - 5*t2) + 
            t1*(Power(t1,2) + t1*t2 + Power(t2,2)) + 
            s2*(-3*Power(t1,2) - 8*t1*t2 + 2*Power(t2,2))) + 
         Power(s1,4)*(2*Power(s2,3) - 6*Power(s2,2)*t2 + 
            t1*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
            s2*(4*Power(t1,2) - 4*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,3)*(Power(s2,4) + Power(s2,3)*(t1 - 6*t2) + 
            Power(t1,2)*t2*(-2*t1 + t2) - 
            Power(s2,2)*(Power(t1,2) + 6*t1*t2 - 7*Power(t2,2)) + 
            s2*(3*Power(t1,3) + 2*t1*Power(t2,2) - Power(t2,3))) - 
         Power(s,3)*(Power(s1,3)*(s2 + 4*t1 - 3*t2) + 
            t1*(t1 - t2)*(s2*t1 - 3*s2*t2 - t1*t2 + Power(t2,2)) + 
            Power(s1,2)*(2*s2*t1 + 8*Power(t1,2) - 4*s2*t2 - 11*t1*t2 + 
               4*Power(t2,2)) + 
            s1*(3*s2*Power(t1,2) + 4*Power(t1,3) - 10*s2*t1*t2 - 
               10*Power(t1,2)*t2 + 4*s2*Power(t2,2) + 7*t1*Power(t2,2) - 
               Power(t2,3))) + 
         Power(s,2)*(Power(s1,4)*(s2 + 6*t1 - 3*t2) - 
            s2*t1*t2*(2*s2*t1 + Power(t1,2) - 3*s2*t2 - 4*t1*t2 + 
               3*Power(t2,2)) + 
            Power(s1,3)*(2*Power(s2,2) + 2*s2*t1 + 12*Power(t1,2) - 
               11*s2*t2 - 15*t1*t2 + 5*Power(t2,2)) + 
            Power(s1,2)*(7*Power(t1,3) + Power(s2,2)*(2*t1 - 7*t2) - 
               18*Power(t1,2)*t2 + 11*t1*Power(t2,2) - 2*Power(t2,3) + 
               s2*(5*Power(t1,2) - 27*t1*t2 + 13*Power(t2,2))) + 
            s1*(-3*t1*Power(t1 - t2,2)*t2 + 
               Power(s2,2)*(Power(t1,2) - 11*t1*t2 + 6*Power(t2,2)) + 
               s2*(2*Power(t1,3) - 12*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) - 3*Power(t2,3)))) + 
         s*(-(Power(s2,2)*t1*(s2 + 2*t1 - 3*t2)*Power(t2,2)) + 
            Power(s1,5)*(-s2 - 4*t1 + t2) - 
            Power(s1,4)*(2*Power(s2,2) + 8*Power(t1,2) + 
               3*s2*(t1 - 3*t2) - 9*t1*t2 + 2*Power(t2,2)) + 
            Power(s1,3)*(-2*Power(s2,3) - 6*Power(t1,3) + 
               14*Power(s2,2)*t2 + 13*Power(t1,2)*t2 - 
               6*t1*Power(t2,2) + Power(t2,3) + 
               s2*(-6*Power(t1,2) + 19*t1*t2 - 12*Power(t2,2))) + 
            s1*s2*t2*(4*Power(s2,2)*(t1 - t2) + 2*t1*t2*(-2*t1 + 3*t2) + 
               s2*(2*Power(t1,2) - 15*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,2)*(-(Power(s2,3)*(t1 - 6*t2)) + 
               Power(s2,2)*(Power(t1,2) + 20*t1*t2 - 14*Power(t2,2)) + 
               t1*t2*(4*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               s2*(-4*Power(t1,3) + 6*Power(t1,2)*t2 - 
                  19*t1*Power(t2,2) + 4*Power(t2,3)))))*
       lg2(-(t2/(t1*(-s1 - t1 + t2)))))/
     (Power(s1,3)*s2*(-s + s2 - t1)*t1*t2*(s - s1 + t2)) + 
    (8*(Power(s2,5)*Power(s2 - t1,4)*Power(t2,5)*(s2 + t2) + 
         s1*Power(s2,4)*Power(s2 - t1,3)*Power(t2,4)*(s2 + t2)*
          (3*s2*t1 - 2*Power(t1,2) + 5*s2*t2 - 3*t1*t2) + 
         Power(s1,7)*s2*Power(s2 - t1,3)*
          (Power(s2,4) - Power(s2,3)*(t1 - 3*t2) - t1*Power(t2,3) + 
            s2*Power(t2,2)*(-t1 + t2) + Power(s2,2)*t2*(-2*t1 + 3*t2)) + 
         Power(s1,2)*Power(s2,3)*Power(s2 - t1,2)*Power(t2,3)*
          (3*Power(s2,5) + Power(s2,4)*(-9*t1 + 7*t2) + 
            Power(t1,2)*t2*(Power(t1,2) + 3*Power(t2,2)) + 
            Power(s2,3)*(10*Power(t1,2) - 13*t1*t2 + 13*Power(t2,2)) + 
            s2*t1*(Power(t1,3) - 6*Power(t1,2)*t2 + 3*t1*Power(t2,2) - 
               11*Power(t2,3)) + 
            Power(s2,2)*(-5*Power(t1,3) + 12*Power(t1,2)*t2 - 
               14*t1*Power(t2,2) + 9*Power(t2,3))) - 
         Power(s,7)*(t1 - t2)*t2*
          (s1*Power(s2,2)*Power(t2,2)*(s2 + t2) + 
            Power(s2,2)*(t1 - t2)*Power(t2,2)*(s2 + t2) + 
            Power(s1,2)*(Power(s2,3)*(t1 - t2) + 
               Power(s2,2)*(t1 - 3*t2)*t2 + 
               s2*(2*t1 - 3*t2)*Power(t2,2) + (t1 - t2)*Power(t2,3))) + 
         Power(s1,6)*(s2 - t1)*
          (2*Power(s2,8) - 7*Power(s2,7)*(t1 - t2) + 
            s2*Power(t1,3)*(3*t1 - 5*t2)*Power(t2,3) + 
            Power(t1,4)*Power(t2,4) + 
            3*Power(s2,6)*(3*Power(t1,2) - 8*t1*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (2*Power(t1,2) - 15*t1*t2 + 10*Power(t2,2)) + 
            Power(s2,3)*t1*t2*
             (3*Power(t1,3) - 12*Power(t1,2)*t2 + 30*t1*Power(t2,2) - 
               10*Power(t2,3)) + 
            Power(s2,5)*(-5*Power(t1,3) + 30*Power(t1,2)*t2 - 
               30*t1*Power(t2,2) + 12*Power(t2,3)) + 
            Power(s2,4)*(Power(t1,4) - 16*Power(t1,3)*t2 + 
               28*Power(t1,2)*Power(t2,2) - 31*t1*Power(t2,3) + 
               3*Power(t2,4))) + 
         Power(s1,3)*Power(s2,2)*(s2 - t1)*Power(t2,2)*
          (3*Power(s2,7) + Power(s2,6)*(-13*t1 + 15*t2) - 
            Power(t1,3)*Power(t2,2)*(3*Power(t1,2) + Power(t2,2)) + 
            Power(s2,5)*(23*Power(t1,2) - 58*t1*t2 + 27*Power(t2,2)) + 
            s2*Power(t1,2)*t2*
             (-6*Power(t1,3) + 23*Power(t1,2)*t2 - 8*t1*Power(t2,2) + 
               9*Power(t2,3)) + 
            Power(s2,4)*(-21*Power(t1,3) + 99*Power(t1,2)*t2 - 
               76*t1*Power(t2,2) + 28*Power(t2,3)) + 
            Power(s2,2)*t1*(-2*Power(t1,4) + 38*Power(t1,3)*t2 - 
               67*Power(t1,2)*Power(t2,2) + 36*t1*Power(t2,3) - 
               18*Power(t2,4)) + 
            Power(s2,3)*(10*Power(t1,4) - 88*Power(t1,3)*t2 + 
               98*Power(t1,2)*Power(t2,2) - 53*t1*Power(t2,3) + 
               11*Power(t2,4))) + 
         Power(s1,5)*(Power(s2,10) + Power(t1,6)*Power(t2,4) + 
            Power(s2,9)*(-5*t1 + 4*t2) + 
            2*s2*Power(t1,4)*Power(t2,3)*
             (Power(t1,2) - 5*t1*t2 - Power(t2,2)) + 
            Power(s2,8)*(10*Power(t1,2) - 24*t1*t2 + 11*Power(t2,2)) + 
            Power(s2,2)*Power(t1,3)*Power(t2,2)*
             (Power(t1,3) - 20*Power(t1,2)*t2 + 35*t1*Power(t2,2) + 
               7*Power(t2,3)) + 
            Power(s2,7)*(-10*Power(t1,3) + 56*Power(t1,2)*t2 - 
               45*t1*Power(t2,2) + 13*Power(t2,3)) + 
            Power(s2,3)*Power(t1,2)*t2*
             (Power(t1,4) - 11*Power(t1,3)*t2 + 
               75*Power(t1,2)*Power(t2,2) - 56*t1*Power(t2,3) - 
               9*Power(t2,4)) + 
            Power(s2,6)*(5*Power(t1,4) - 65*Power(t1,3)*t2 + 
               79*Power(t1,2)*Power(t2,2) - 64*t1*Power(t2,3) - 
               2*Power(t2,4)) + 
            Power(s2,4)*t1*t2*
             (-11*Power(t1,4) + 41*Power(t1,3)*t2 - 
               136*Power(t1,2)*Power(t2,2) + 40*t1*Power(t2,3) + 
               6*Power(t2,4)) - 
            Power(s2,5)*(Power(t1,5) - 39*Power(t1,4)*t2 + 
               76*Power(t1,3)*Power(t2,2) - 
               130*Power(t1,2)*Power(t2,3) + 7*t1*Power(t2,4) + 
               Power(t2,5))) + 
         Power(s1,4)*s2*t2*(-3*Power(s2,8)*(t1 - 2*t2) + 
            Power(t1,5)*Power(t2,3)*(3*t1 + 2*t2) + 
            2*Power(s2,7)*(7*Power(t1,2) - 14*t1*t2 + 8*Power(t2,2)) + 
            s2*Power(t1,3)*Power(t2,2)*
             (5*Power(t1,3) - 21*Power(t1,2)*t2 - 8*t1*Power(t2,2) - 
               Power(t2,3)) + 
            Power(s2,6)*(-26*Power(t1,3) + 59*Power(t1,2)*t2 - 
               79*t1*Power(t2,2) + 15*Power(t2,3)) + 
            Power(s2,2)*Power(t1,2)*t2*
             (3*Power(t1,4) - 42*Power(t1,3)*t2 + 
               65*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) + 
               5*Power(t2,4)) + 
            Power(s2,5)*(24*Power(t1,4) - 72*Power(t1,3)*t2 + 
               175*Power(t1,2)*Power(t2,2) - 53*t1*Power(t2,3) + 
               16*Power(t2,4)) + 
            Power(s2,3)*t1*(2*Power(t1,5) - 20*Power(t1,4)*t2 + 
               133*Power(t1,3)*Power(t2,2) - 
               108*Power(t1,2)*Power(t2,3) + 18*t1*Power(t2,4) - 
               9*Power(t2,5)) + 
            Power(s2,4)*(-11*Power(t1,5) + 52*Power(t1,4)*t2 - 
               208*Power(t1,3)*Power(t2,2) + 
               100*Power(t1,2)*Power(t2,3) - 31*t1*Power(t2,4) + 
               5*Power(t2,5))) + 
         Power(s,6)*(Power(s2,2)*(t1 - t2)*Power(t2,3)*
             (Power(s2,2)*(5*t1 - 7*t2) + 4*t1*t2*(-t1 + t2) + 
               s2*(-4*Power(t1,2) + 9*t1*t2 - 7*Power(t2,2))) + 
            s1*Power(s2,2)*Power(t2,2)*
             (Power(s2,2)*(2*Power(t1,2) + t1*t2 - 6*Power(t2,2)) - 
               s2*t2*(-3*Power(t1,2) + 5*t1*t2 + Power(t2,2)) + 
               Power(t2,2)*(Power(t1,2) - 6*t1*t2 + 5*Power(t2,2))) + 
            Power(s1,3)*(6*Power(s2,3)*Power(t1 - t2,2)*t2 + 
               Power(s2,4)*(Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               2*Power(t2,4)*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               s2*Power(t2,3)*
                (9*Power(t1,2) - 19*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*Power(t2,2)*
                (5*Power(t1,2) - 18*t1*t2 + 8*Power(t2,2))) + 
            Power(s1,2)*t2*(-4*t1*Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,4)*(6*Power(t1,2) - 13*t1*t2 + 8*Power(t2,2)) + 
               s2*Power(t2,2)*
                (-8*Power(t1,3) + 27*Power(t1,2)*t2 - 
                  26*t1*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-4*Power(t1,3) + 29*Power(t1,2)*t2 - 
                  43*t1*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,3)*(-4*Power(t1,3) + 15*Power(t1,2)*t2 - 
                  28*t1*Power(t2,2) + 21*Power(t2,3)))) + 
         Power(s,5)*(Power(s1,4)*
             (Power(s2,5)*(2*t1 - t2) + 
               Power(s2,3)*t2*
                (-14*Power(t1,2) + 28*t1*t2 - 13*Power(t2,2)) + 
               s2*Power(t2,3)*
                (-16*Power(t1,2) + 27*t1*t2 - 5*Power(t2,2)) + 
               Power(s2,4)*(-4*Power(t1,2) + 17*t1*t2 - 
                  4*Power(t2,2)) - 
               Power(t2,4)*(6*Power(t1,2) - 6*t1*t2 + Power(t2,2)) - 
               2*Power(s2,2)*Power(t2,2)*
                (5*Power(t1,2) - 16*t1*t2 + 4*Power(t2,2))) + 
            s1*Power(s2,2)*Power(t2,2)*(s2 + t2)*
             (6*t1*t2*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(-8*Power(t1,2) + 10*t1*t2 + 
                  15*Power(t2,2)) + 
               s2*(7*Power(t1,3) - 18*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) - 30*Power(t2,3))) + 
            Power(s1,3)*(Power(s2,5)*
                (-4*Power(t1,2) + 20*t1*t2 - 3*Power(t2,2)) + 
               t1*Power(t2,4)*
                (15*Power(t1,2) - 22*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,3)*t2*
                (21*Power(t1,3) - 71*Power(t1,2)*t2 + 
                  120*t1*Power(t2,2) - 37*Power(t2,3)) + 
               Power(s2,2)*Power(t2,2)*
                (18*Power(t1,3) - 117*Power(t1,2)*t2 + 
                  132*t1*Power(t2,2) - 37*Power(t2,3)) + 
               Power(s2,4)*(3*Power(t1,3) - 44*Power(t1,2)*t2 + 
                  67*t1*Power(t2,2) - 26*Power(t2,3)) + 
               s2*Power(t2,3)*
                (33*Power(t1,3) - 94*Power(t1,2)*t2 + 
                  58*t1*Power(t2,2) - 11*Power(t2,3))) - 
            Power(s2,2)*Power(t2,3)*
             (6*Power(t1,2)*Power(t1 - t2,2)*t2 + 
               Power(s2,3)*(10*Power(t1,2) - 30*t1*t2 + 
                  21*Power(t2,2)) + 
               2*s2*t1*(3*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) - 12*Power(t2,3)) + 
               Power(s2,2)*(-16*Power(t1,3) + 50*Power(t1,2)*t2 - 
                  54*t1*Power(t2,2) + 21*Power(t2,3))) - 
            Power(s1,2)*t2*(6*Power(t1,2)*Power(t1 - t2,2)*Power(t2,3) + 
               2*Power(s2,5)*
                (7*Power(t1,2) - 20*t1*t2 + 14*Power(t2,2)) + 
               6*s2*t1*Power(t2,2)*
                (2*Power(t1,3) - 9*Power(t1,2)*t2 + 11*t1*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(s2,4)*(-20*Power(t1,3) + 71*Power(t1,2)*t2 - 
                  117*t1*Power(t2,2) + 67*Power(t2,3)) + 
               Power(s2,2)*t2*
                (6*Power(t1,4) - 68*Power(t1,3)*t2 + 
                  153*Power(t1,2)*Power(t2,2) - 130*t1*Power(t2,3) + 
                  29*Power(t2,4)) + 
               Power(s2,3)*(6*Power(t1,4) - 36*Power(t1,3)*t2 + 
                  141*Power(t1,2)*Power(t2,2) - 199*t1*Power(t2,3) + 
                  68*Power(t2,4)))) - 
         s*(-(Power(s2,4)*Power(s2 - t1,3)*Power(t2,4)*
               (Power(s2,2)*(2*t1 - 7*t2) + t1*t2*(-2*t1 + 3*t2) + 
                 s2*(-2*Power(t1,2) + 5*t1*t2 - 7*Power(t2,2)))) - 
            s1*Power(s2,3)*Power(s2 - t1,2)*Power(t2,3)*(s2 + t2)*
             (2*Power(t1,2)*(2*t1 - 3*t2)*t2 + 
               Power(s2,3)*(4*t1 + t2) + 
               s2*t1*(2*Power(t1,2) - t1*t2 + 30*Power(t2,2)) - 
               Power(s2,2)*(6*Power(t1,2) + 7*t1*t2 + 30*Power(t2,2))) + 
            Power(s1,7)*s2*(2*Power(s2,6) - 6*Power(s2,5)*(t1 - t2) - 
               2*s2*Power(t1,2)*(t1 - 3*t2)*Power(t2,2) - 
               2*Power(t1,3)*Power(t2,3) + 
               Power(s2,2)*t1*t2*
                (-4*Power(t1,2) + 10*t1*t2 - 5*Power(t2,2)) + 
               Power(s2,4)*(6*Power(t1,2) - 16*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(-2*Power(t1,3) + 14*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) + Power(t2,3))) + 
            Power(s1,6)*(6*Power(s2,8) - 3*Power(s2,7)*(8*t1 - 7*t2) + 
               4*s2*Power(t1,3)*(3*t1 - 4*t2)*Power(t2,3) + 
               3*Power(t1,4)*Power(t2,4) + 
               9*Power(s2,6)*
                (4*Power(t1,2) - 9*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*Power(t1,2)*Power(t2,2)*
                (9*Power(t1,2) - 54*t1*t2 + 28*Power(t2,2)) + 
               Power(s2,3)*t1*t2*
                (15*Power(t1,3) - 52*Power(t1,2)*t2 + 
                  92*t1*Power(t2,2) - 24*Power(t2,3)) - 
               3*Power(s2,5)*
                (8*Power(t1,3) - 38*Power(t1,2)*t2 + 
                  29*t1*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s2,4)*(6*Power(t1,4) - 69*Power(t1,3)*t2 + 
                  103*Power(t1,2)*Power(t2,2) - 77*t1*Power(t2,3) + 
                  6*Power(t2,4))) - 
            Power(s1,2)*Power(s2,2)*(s2 - t1)*Power(t2,2)*
             (2*Power(s2,6)*(4*t1 - 9*t2) + 
               Power(s2,5)*(-27*Power(t1,2) + 87*t1*t2 - 
                  41*Power(t2,2)) + 
               Power(t1,3)*Power(t2,2)*
                (5*Power(t1,2) - 12*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,4)*(33*Power(t1,3) - 154*Power(t1,2)*t2 + 
                  144*t1*Power(t2,2) - 70*Power(t2,3)) + 
               s2*Power(t1,2)*t2*
                (9*Power(t1,3) - 48*Power(t1,2)*t2 + 
                  56*t1*Power(t2,2) - 37*Power(t2,3)) + 
               Power(s2,3)*(-17*Power(t1,4) + 130*Power(t1,3)*t2 - 
                  217*Power(t1,2)*Power(t2,2) + 136*t1*Power(t2,3) - 
                  47*Power(t2,4)) + 
               Power(s2,2)*t1*
                (3*Power(t1,4) - 54*Power(t1,3)*t2 + 
                  155*Power(t1,2)*Power(t2,2) - 114*t1*Power(t2,3) + 
                  77*Power(t2,4))) + 
            Power(s1,5)*(4*Power(s2,9) + 
               Power(t1,4)*Power(t2,4)*(-7*t1 + 2*t2) + 
               Power(s2,8)*(-21*t1 + 17*t2) + 
               s2*Power(t1,3)*Power(t2,3)*
                (-17*Power(t1,2) + 54*t1*t2 - 2*Power(t2,2)) + 
               3*Power(s2,7)*
                (14*Power(t1,2) - 34*t1*t2 + 11*Power(t2,2)) - 
               Power(s2,2)*Power(t1,2)*Power(t2,2)*
                (10*Power(t1,3) - 115*Power(t1,2)*t2 + 
                  145*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,6)*(-40*Power(t1,3) + 211*Power(t1,2)*t2 - 
                  152*t1*Power(t2,2) + 41*Power(t2,3)) + 
               Power(s2,5)*(18*Power(t1,4) - 197*Power(t1,3)*t2 + 
                  261*Power(t1,2)*Power(t2,2) - 217*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,3)*t1*t2*
                (-13*Power(t1,4) + 75*Power(t1,3)*t2 - 
                  305*Power(t1,2)*Power(t2,2) + 166*t1*Power(t2,3) + 
                  5*Power(t2,4)) - 
               Power(s2,4)*(3*Power(t1,5) - 84*Power(t1,4)*t2 + 
                  207*Power(t1,3)*Power(t2,2) - 
                  379*Power(t1,2)*Power(t2,3) + 65*t1*Power(t2,4) + 
                  3*Power(t2,5))) + 
            Power(s1,3)*s2*t2*
             (2*Power(t1,5)*(3*t1 - 2*t2)*Power(t2,3) + 
               Power(s2,8)*(-4*t1 + 11*t2) + 
               Power(s2,7)*(19*Power(t1,2) - 76*t1*t2 + 
                  62*Power(t2,2)) + 
               s2*Power(t1,3)*Power(t2,2)*
                (10*Power(t1,3) - 62*Power(t1,2)*t2 + 
                  37*t1*Power(t2,2) - 13*Power(t2,3)) + 
               Power(s2,6)*(-37*Power(t1,3) + 191*Power(t1,2)*t2 - 
                  315*t1*Power(t2,2) + 113*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (6*Power(t1,4) - 104*Power(t1,3)*t2 + 
                  272*Power(t1,2)*Power(t2,2) - 148*t1*Power(t2,3) + 
                  63*Power(t2,4)) + 
               Power(s2,5)*(37*Power(t1,4) - 235*Power(t1,3)*t2 + 
                  656*Power(t1,2)*Power(t2,2) - 424*t1*Power(t2,3) + 
                  121*Power(t2,4)) + 
               Power(s2,3)*t1*
                (4*Power(t1,5) - 49*Power(t1,4)*t2 + 
                  390*Power(t1,3)*Power(t2,2) - 
                  592*Power(t1,2)*Power(t2,3) + 302*t1*Power(t2,4) - 
                  96*Power(t2,5)) + 
               Power(s2,4)*(-19*Power(t1,5) + 152*Power(t1,4)*t2 - 
                  699*Power(t1,3)*Power(t2,2) + 
                  689*Power(t1,2)*Power(t2,3) - 305*t1*Power(t2,4) + 
                  47*Power(t2,5))) + 
            Power(s1,4)*(Power(t1,5)*(3*t1 - 2*t2)*Power(t2,4) + 
               Power(s2,9)*(-2*t1 + t2) + 
               s2*Power(t1,4)*Power(t2,3)*
                (6*Power(t1,2) - 41*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,8)*(8*Power(t1,2) - 33*t1*t2 + 18*Power(t2,2)) - 
               3*Power(s2,7)*
                (4*Power(t1,3) - 39*Power(t1,2)*t2 + 
                  39*t1*Power(t2,2) - 17*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*Power(t2,2)*
                (3*Power(t1,4) - 72*Power(t1,3)*t2 + 
                  178*Power(t1,2)*Power(t2,2) - 15*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,6)*(8*Power(t1,4) - 167*Power(t1,3)*t2 + 
                  272*Power(t1,2)*Power(t2,2) - 297*t1*Power(t2,3) + 
                  38*Power(t2,4)) + 
               Power(s2,3)*t1*t2*
                (3*Power(t1,5) - 40*Power(t1,4)*t2 + 
                  314*Power(t1,3)*Power(t2,2) - 
                  362*Power(t1,2)*Power(t2,3) + 54*t1*Power(t2,4) - 
                  20*Power(t2,5)) + 
               Power(s2,4)*t2*
                (-34*Power(t1,5) + 164*Power(t1,4)*t2 - 
                  630*Power(t1,3)*Power(t2,2) + 
                  376*Power(t1,2)*Power(t2,3) - 85*t1*Power(t2,4) + 
                  16*Power(t2,5)) + 
               Power(s2,5)*(-2*Power(t1,5) + 113*Power(t1,4)*t2 - 
                  300*Power(t1,3)*Power(t2,2) + 
                  627*Power(t1,2)*Power(t2,3) - 187*t1*Power(t2,4) + 
                  50*Power(t2,5)))) + 
         Power(s,4)*(Power(s1,5)*
             (Power(s2,6) + 2*t1*(2*t1 - t2)*Power(t2,4) + 
               Power(s2,5)*(-6*t1 + 5*t2) + 
               s2*Power(t2,3)*
                (14*Power(t1,2) - 17*t1*t2 + Power(t2,2)) + 
               2*Power(s2,2)*Power(t2,2)*
                (5*Power(t1,2) - 14*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,4)*(6*Power(t1,2) - 27*t1*t2 + 8*Power(t2,2)) + 
               Power(s2,3)*t2*
                (16*Power(t1,2) - 32*t1*t2 + 13*Power(t2,2))) + 
            Power(s1,4)*(Power(s2,6)*(-8*t1 + 4*t2) + 
               t1*Power(t2,4)*
                (-21*Power(t1,2) + 20*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,5)*(20*Power(t1,2) - 75*t1*t2 + 
                  12*Power(t2,2)) + 
               s2*Power(t2,3)*
                (-53*Power(t1,3) + 121*Power(t1,2)*t2 - 
                  40*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,3)*t2*
                (-43*Power(t1,3) + 133*Power(t1,2)*t2 - 
                  189*t1*Power(t2,2) + 17*Power(t2,3)) + 
               Power(s2,2)*Power(t2,2)*
                (-32*Power(t1,3) + 183*Power(t1,2)*t2 - 
                  147*t1*Power(t2,2) + 17*Power(t2,3)) + 
               Power(s2,4)*(-11*Power(t1,3) + 111*Power(t1,2)*t2 - 
                  132*t1*Power(t2,2) + 42*Power(t2,3))) + 
            Power(s2,2)*Power(t2,3)*
             (-4*Power(t1,3)*Power(t1 - t2,2)*t2 + 
               5*Power(s2,4)*
                (2*Power(t1,2) - 8*t1*t2 + 7*Power(t2,2)) + 
               2*Power(s2,2)*t1*
                (9*Power(t1,3) - 36*Power(t1,2)*t2 + 
                  55*t1*Power(t2,2) - 30*Power(t2,3)) + 
               Power(s2,3)*(-24*Power(t1,3) + 90*Power(t1,2)*t2 - 
                  100*t1*Power(t2,2) + 35*Power(t2,3)) + 
               s2*(-4*Power(t1,5) + 26*Power(t1,4)*t2 - 
                  52*Power(t1,3)*Power(t2,2) + 
                  30*Power(t1,2)*Power(t2,3))) + 
            s1*Power(s2,2)*Power(t2,2)*
             (2*Power(s2,4)*(6*Power(t1,2) - 15*t1*t2 - 
                  10*Power(t2,2)) + 
               4*Power(t1,2)*Power(t2,2)*
                (5*Power(t1,2) - 11*t1*t2 + 6*Power(t2,2)) + 
               s2*t1*t2*(29*Power(t1,3) - 98*Power(t1,2)*t2 + 
                  133*t1*Power(t2,2) - 90*Power(t2,3)) + 
               Power(s2,3)*(-21*Power(t1,3) + 74*Power(t1,2)*t2 - 
                  75*t1*Power(t2,2) + 55*Power(t2,3)) + 
               3*Power(s2,2)*
                (3*Power(t1,4) - 25*Power(t1,3)*t2 + 
                  57*Power(t1,2)*Power(t2,2) - 45*t1*Power(t2,3) + 
                  25*Power(t2,4))) + 
            Power(s1,3)*(3*Power(t1,2)*Power(t2,4)*
                (7*Power(t1,2) - 10*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,6)*(6*Power(t1,2) - 40*t1*t2 + 5*Power(t2,2)) + 
               s2*t1*Power(t2,3)*
                (45*Power(t1,3) - 171*Power(t1,2)*t2 + 
                  130*t1*Power(t2,2) - 30*Power(t2,3)) + 
               Power(s2,5)*(-9*Power(t1,3) + 115*Power(t1,2)*t2 - 
                  157*t1*Power(t2,2) + 59*Power(t2,3)) + 
               Power(s2,2)*Power(t2,2)*
                (24*Power(t1,4) - 244*Power(t1,3)*t2 + 
                  419*Power(t1,2)*Power(t2,2) - 200*t1*Power(t2,3) + 
                  35*Power(t2,4)) + 
               Power(s2,4)*(3*Power(t1,4) - 103*Power(t1,3)*t2 + 
                  265*Power(t1,2)*Power(t2,2) - 348*t1*Power(t2,3) + 
                  95*Power(t2,4)) + 
               Power(s2,3)*t2*
                (27*Power(t1,4) - 141*Power(t1,3)*t2 + 
                  473*Power(t1,2)*Power(t2,2) - 397*t1*Power(t2,3) + 
                  106*Power(t2,4))) + 
            Power(s1,2)*t2*(-4*Power(t1,3)*Power(t1 - t2,2)*
                Power(t2,3) + 
               Power(s2,6)*(16*Power(t1,2) - 70*t1*t2 + 
                  55*Power(t2,2)) + 
               2*s2*Power(t1,2)*Power(t2,2)*
                (-4*Power(t1,3) + 25*Power(t1,2)*t2 - 
                  36*t1*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s2,5)*(-36*Power(t1,3) + 168*Power(t1,2)*t2 - 
                  283*t1*Power(t2,2) + 125*Power(t2,3)) + 
               Power(s2,2)*t1*t2*
                (-4*Power(t1,4) + 70*Power(t1,3)*t2 - 
                  229*Power(t1,2)*Power(t2,2) + 268*t1*Power(t2,3) - 
                  85*Power(t2,4)) + 
               Power(s2,4)*(24*Power(t1,4) - 131*Power(t1,3)*t2 + 
                  417*Power(t1,2)*Power(t2,2) - 493*t1*Power(t2,3) + 
                  145*Power(t2,4)) + 
               Power(s2,3)*(-4*Power(t1,5) + 38*Power(t1,4)*t2 - 
                  260*Power(t1,3)*Power(t2,2) + 
                  533*Power(t1,2)*Power(t2,3) - 340*t1*Power(t2,4) + 
                  75*Power(t2,5)))) + 
         Power(s,2)*(Power(s1,7)*s2*
             (Power(s2,5) + s2*t1*(t1 - 2*t2)*Power(t2,2) + 
               Power(t1,2)*Power(t2,3) + Power(s2,4)*(-2*t1 + 3*t2) + 
               Power(s2,2)*t2*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(Power(t1,2) - 5*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,6)*(6*Power(s2,7) - 21*Power(s2,6)*(t1 - t2) - 
               3*Power(t1,3)*Power(t2,4) + 
               s2*Power(t1,2)*Power(t2,3)*(-15*t1 + 14*t2) - 
               2*Power(s2,2)*t1*Power(t2,2)*
                (6*Power(t1,2) - 24*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,5)*(24*Power(t1,2) - 69*t1*t2 + 
                  23*Power(t2,2)) + 
               Power(s2,3)*t2*
                (-21*Power(t1,3) + 56*Power(t1,2)*t2 - 
                  47*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,4)*(-9*Power(t1,3) + 69*Power(t1,2)*t2 - 
                  65*t1*Power(t2,2) + 18*Power(t2,3))) + 
            Power(s2,3)*Power(s2 - t1,2)*Power(t2,3)*
             (Power(t1,2)*t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*(Power(t1,2) - 12*t1*t2 + 21*Power(t2,2)) + 
               s2*t1*(Power(t1,3) - 6*Power(t1,2)*t2 + 
                  19*t1*Power(t2,2) - 18*Power(t2,3)) + 
               Power(s2,2)*(-2*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  30*t1*Power(t2,2) + 21*Power(t2,3))) + 
            Power(s1,5)*(6*Power(s2,8) + 
               3*Power(t1,3)*(5*t1 - 2*t2)*Power(t2,4) + 
               Power(s2,7)*(-33*t1 + 27*t2) + 
               s2*Power(t1,2)*Power(t2,3)*
                (42*Power(t1,2) - 95*t1*t2 + 11*Power(t2,2)) + 
               Power(s2,6)*(60*Power(t1,2) - 159*t1*t2 + 
                  41*Power(t2,2)) + 
               Power(s2,2)*t1*Power(t2,2)*
                (27*Power(t1,3) - 204*Power(t1,2)*t2 + 
                  179*t1*Power(t2,2) - 7*Power(t2,3)) + 
               Power(s2,5)*(-45*Power(t1,3) + 267*Power(t1,2)*t2 - 
                  205*t1*Power(t2,2) + 56*Power(t2,3)) + 
               Power(s2,4)*(12*Power(t1,4) - 174*Power(t1,3)*t2 + 
                  294*Power(t1,2)*Power(t2,2) - 265*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,3)*t2*
                (39*Power(t1,4) - 155*Power(t1,3)*t2 + 
                  363*Power(t1,2)*Power(t2,2) - 114*t1*Power(t2,3) - 
                  2*Power(t2,4))) + 
            s1*Power(s2,2)*(s2 - t1)*Power(t2,2)*
             (-3*Power(t1,3)*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s2,5)*(2*Power(t1,2) - 19*t1*t2 - 6*Power(t2,2)) + 
               s2*Power(t1,2)*t2*
                (-4*Power(t1,3) + 18*Power(t1,2)*t2 - 
                  42*t1*Power(t2,2) + 39*Power(t2,3)) + 
               Power(s2,4)*(-5*Power(t1,3) + 40*Power(t1,2)*t2 - 
                  25*t1*Power(t2,2) + 69*Power(t2,3)) + 
               Power(s2,3)*(4*Power(t1,4) - 33*Power(t1,3)*t2 + 
                  86*Power(t1,2)*Power(t2,2) - 111*t1*Power(t2,3) + 
                  75*Power(t2,4)) - 
               Power(s2,2)*t1*
                (Power(t1,4) - 16*Power(t1,3)*t2 + 
                  67*Power(t1,2)*Power(t2,2) - 87*t1*Power(t2,3) + 
                  105*Power(t2,4))) + 
            Power(s1,4)*(Power(s2,8)*(-8*t1 + 4*t2) - 
               Power(t1,3)*Power(t2,4)*
                (15*Power(t1,2) - 12*t1*t2 + Power(t2,2)) + 
               Power(s2,7)*(28*Power(t1,2) - 98*t1*t2 + 
                  22*Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,3)*
                (-33*Power(t1,3) + 140*Power(t1,2)*t2 - 
                  48*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,6)*(-35*Power(t1,3) + 275*Power(t1,2)*t2 - 
                  226*t1*Power(t2,2) + 70*Power(t2,3)) + 
               Power(s2,2)*t1*Power(t2,2)*
                (-18*Power(t1,4) + 246*Power(t1,3)*t2 - 
                  435*Power(t1,2)*Power(t2,2) + 91*t1*Power(t2,3) - 
                  16*Power(t2,4)) + 
               Power(s2,5)*(18*Power(t1,4) - 295*Power(t1,3)*t2 + 
                  489*Power(t1,2)*Power(t2,2) - 482*t1*Power(t2,3) + 
                  32*Power(t2,4)) + 
               Power(s2,3)*t2*
                (-21*Power(t1,5) + 146*Power(t1,4)*t2 - 
                  705*Power(t1,3)*Power(t2,2) + 
                  578*Power(t1,2)*Power(t2,3) - 105*t1*Power(t2,4) + 
                  19*Power(t2,5)) + 
               Power(s2,4)*(-3*Power(t1,5) + 135*Power(t1,4)*t2 - 
                  413*Power(t1,3)*Power(t2,2) + 
                  896*Power(t1,2)*Power(t2,3) - 308*t1*Power(t2,4) + 
                  59*Power(t2,5))) + 
            Power(s1,3)*(Power(t1,4)*Power(t2,4)*
                (3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,8)*(Power(t1,2) - 20*t1*t2 + 15*Power(t2,2)) + 
               s2*Power(t1,3)*Power(t2,3)*
                (6*Power(t1,3) - 52*Power(t1,2)*t2 + 
                  43*t1*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s2,7)*(-3*Power(t1,3) + 83*Power(t1,2)*t2 - 
                  163*t1*Power(t2,2) + 104*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*Power(t2,2)*
                (3*Power(t1,4) - 84*Power(t1,3)*t2 + 
                  290*Power(t1,2)*Power(t2,2) - 192*t1*Power(t2,3) + 
                  54*Power(t2,4)) + 
               Power(s2,6)*(3*Power(t1,4) - 135*Power(t1,3)*t2 + 
                  412*Power(t1,2)*Power(t2,2) - 573*t1*Power(t2,3) + 
                  190*Power(t2,4)) - 
               Power(s2,5)*(Power(t1,5) - 104*Power(t1,4)*t2 + 
                  438*Power(t1,3)*Power(t2,2) - 
                  1107*Power(t1,2)*Power(t2,3) + 729*t1*Power(t2,4) - 
                  211*Power(t2,5)) + 
               Power(s2,3)*t1*t2*
                (3*Power(t1,5) - 47*Power(t1,4)*t2 + 
                  429*Power(t1,3)*Power(t2,2) - 
                  797*Power(t1,2)*Power(t2,3) + 423*t1*Power(t2,4) - 
                  121*Power(t2,5)) + 
               Power(s2,4)*t2*
                (-35*Power(t1,5) + 218*Power(t1,4)*t2 - 
                  991*Power(t1,3)*Power(t2,2) + 
                  1099*Power(t1,2)*Power(t2,3) - 469*t1*Power(t2,4) + 
                  80*Power(t2,5))) + 
            Power(s1,2)*s2*t2*
             (3*Power(t1,4)*Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,7)*(2*Power(t1,2) - 37*t1*t2 + 46*Power(t2,2)) + 
               s2*Power(t1,3)*Power(t2,2)*
                (5*Power(t1,3) - 50*Power(t1,2)*t2 + 
                  92*t1*Power(t2,2) - 37*Power(t2,3)) + 
               Power(s2,6)*(-8*Power(t1,3) + 137*Power(t1,2)*t2 - 
                  282*t1*Power(t2,2) + 103*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (3*Power(t1,4) - 76*Power(t1,3)*t2 + 
                  295*Power(t1,2)*Power(t2,2) - 328*t1*Power(t2,3) + 
                  144*Power(t2,4)) + 
               Power(s2,5)*(12*Power(t1,4) - 191*Power(t1,3)*t2 + 
                  603*Power(t1,2)*Power(t2,2) - 493*t1*Power(t2,3) + 
                  158*Power(t2,4)) + 
               2*Power(s2,3)*t1*
                (Power(t1,5) - 17*Power(t1,4)*t2 + 
                  159*Power(t1,3)*Power(t2,2) - 
                  374*Power(t1,2)*Power(t2,3) + 264*t1*Power(t2,4) - 
                  105*Power(t2,5)) + 
               Power(s2,4)*(-8*Power(t1,5) + 122*Power(t1,4)*t2 - 
                  614*Power(t1,3)*Power(t2,2) + 
                  891*Power(t1,2)*Power(t2,3) - 442*t1*Power(t2,4) + 
                  101*Power(t2,5)))) - 
         Power(s,3)*(Power(s1,6)*
             (2*Power(s2,6) + 2*s2*t1*(3*t1 - 2*t2)*Power(t2,3) + 
               Power(t1,2)*Power(t2,4) + Power(s2,5)*(-6*t1 + 7*t2) + 
               Power(s2,2)*Power(t2,2)*
                (5*Power(t1,2) - 12*t1*t2 + Power(t2,2)) + 
               3*Power(s2,3)*t2*
                (3*Power(t1,2) - 6*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,4)*(4*Power(t1,2) - 19*t1*t2 + 8*Power(t2,2))) + 
            Power(s2,2)*(s2 - t1)*Power(t2,3)*
             (-(Power(t1,3)*Power(t1 - t2,2)*t2) + 
               5*Power(s2,4)*(Power(t1,2) - 6*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*t1*
                (7*Power(t1,3) - 33*Power(t1,2)*t2 + 
                  65*t1*Power(t2,2) - 45*Power(t2,3)) - 
               s2*Power(t1,2)*
                (Power(t1,3) - 9*Power(t1,2)*t2 + 23*t1*Power(t2,2) - 
                  15*Power(t2,3)) + 
               Power(s2,3)*(-11*Power(t1,3) + 55*Power(t1,2)*t2 - 
                  75*t1*Power(t2,2) + 35*Power(t2,3))) + 
            Power(s1,5)*(4*Power(s2,7) + 
               Power(t1,2)*Power(t2,4)*(-13*t1 + 6*t2) + 
               Power(s2,6)*(-23*t1 + 19*t2) + 
               s2*t1*Power(t2,3)*
                (-41*Power(t1,2) + 68*t1*t2 - 8*Power(t2,2)) + 
               Power(s2,5)*(34*Power(t1,2) - 108*t1*t2 + 
                  27*Power(t2,2)) + 
               Power(s2,2)*Power(t2,2)*
                (-28*Power(t1,3) + 137*Power(t1,2)*t2 - 
                  73*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*t2*
                (-43*Power(t1,3) + 123*Power(t1,2)*t2 - 
                  140*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,4)*(-15*Power(t1,3) + 128*Power(t1,2)*t2 - 
                  130*t1*Power(t2,2) + 41*Power(t2,3))) + 
            Power(s1,4)*(6*Power(s2,7)*(-2*t1 + t2) + 
               3*Power(t1,2)*Power(t2,4)*
                (9*Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               18*Power(s2,6)*(2*Power(t1,2) - 7*t1*t2 + Power(t2,2)) + 
               s2*t1*Power(t2,3)*
                (64*Power(t1,3) - 196*Power(t1,2)*t2 + 
                  75*t1*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s2,5)*(-34*Power(t1,3) + 269*Power(t1,2)*t2 - 
                  241*t1*Power(t2,2) + 64*Power(t2,3)) + 
               Power(s2,2)*Power(t2,2)*
                (37*Power(t1,4) - 330*Power(t1,3)*t2 + 
                  417*Power(t1,2)*Power(t2,2) - 85*t1*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(s2,4)*(10*Power(t1,4) - 197*Power(t1,3)*t2 + 
                  399*Power(t1,2)*Power(t2,2) - 421*t1*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s2,3)*t2*
                (47*Power(t1,4) - 214*Power(t1,3)*t2 + 
                  611*Power(t1,2)*Power(t2,2) - 294*t1*Power(t2,3) + 
                  37*Power(t2,4))) - 
            s1*Power(s2,2)*Power(t2,2)*
             (Power(t1,3)*Power(t2,2)*
                (13*Power(t1,2) - 27*t1*t2 + 14*Power(t2,2)) + 
               Power(s2,5)*(-8*Power(t1,2) + 35*t1*t2 + 
                  15*Power(t2,2)) + 
               s2*Power(t1,2)*t2*
                (18*Power(t1,3) - 77*Power(t1,2)*t2 + 
                  131*t1*Power(t2,2) - 96*Power(t2,3)) + 
               Power(s2,4)*(21*Power(t1,3) - 96*Power(t1,2)*t2 + 
                  65*t1*Power(t2,2) - 85*Power(t2,3)) + 
               Power(s2,3)*(-18*Power(t1,4) + 111*Power(t1,3)*t2 - 
                  224*Power(t1,2)*Power(t2,2) + 210*t1*Power(t2,3) - 
                  100*Power(t2,4)) + 
               Power(s2,2)*t1*
                (5*Power(t1,4) - 68*Power(t1,3)*t2 + 
                  207*Power(t1,2)*Power(t2,2) - 232*t1*Power(t2,3) + 
                  180*Power(t2,4))) + 
            Power(s1,3)*(Power(t1,3)*Power(t2,4)*
                (-13*Power(t1,2) + 18*t1*t2 - 5*Power(t2,2)) + 
               2*Power(s2,7)*
                (2*Power(t1,2) - 20*t1*t2 + 5*Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,3)*
                (-27*Power(t1,3) + 142*Power(t1,2)*t2 - 
                  118*t1*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s2,6)*(-9*Power(t1,3) + 141*Power(t1,2)*t2 - 
                  205*t1*Power(t2,2) + 96*Power(t2,3)) + 
               Power(s2,2)*t1*Power(t2,2)*
                (-14*Power(t1,4) + 219*Power(t1,3)*t2 - 
                  526*Power(t1,2)*Power(t2,2) + 318*t1*Power(t2,3) - 
                  77*Power(t2,4)) + 
               Power(s2,5)*(6*Power(t1,4) - 180*Power(t1,3)*t2 + 
                  456*Power(t1,2)*Power(t2,2) - 577*t1*Power(t2,3) + 
                  170*Power(t2,4)) - 
               Power(s2,4)*(Power(t1,5) - 94*Power(t1,4)*t2 + 
                  370*Power(t1,3)*Power(t2,2) - 
                  973*Power(t1,2)*Power(t2,3) + 692*t1*Power(t2,4) - 
                  194*Power(t2,5)) + 
               Power(s2,3)*t2*
                (-15*Power(t1,5) + 123*Power(t1,4)*t2 - 
                  690*Power(t1,3)*Power(t2,2) + 
                  913*Power(t1,2)*Power(t2,3) - 393*t1*Power(t2,4) + 
                  70*Power(t2,5))) + 
            Power(s1,2)*t2*(Power(t1,4)*Power(t1 - t2,2)*Power(t2,3) + 
               Power(s2,7)*(9*Power(t1,2) - 70*t1*t2 + 65*Power(t2,2)) + 
               s2*Power(t1,3)*Power(t2,2)*
                (2*Power(t1,3) - 21*Power(t1,2)*t2 + 35*t1*Power(t2,2) - 
                  16*Power(t2,3)) + 
               Power(s2,6)*(-28*Power(t1,3) + 213*Power(t1,2)*t2 - 
                  382*t1*Power(t2,2) + 145*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (Power(t1,4) - 32*Power(t1,3)*t2 + 
                  161*Power(t1,2)*Power(t2,2) - 238*t1*Power(t2,3) + 
                  88*Power(t2,4)) + 
               Power(s2,5)*(30*Power(t1,4) - 230*Power(t1,3)*t2 + 
                  696*Power(t1,2)*Power(t2,2) - 667*t1*Power(t2,3) + 
                  195*Power(t2,4)) + 
               Power(s2,3)*t1*
                (Power(t1,5) - 18*Power(t1,4)*t2 + 
                  213*Power(t1,3)*Power(t2,2) - 
                  598*Power(t1,2)*Power(t2,3) + 532*t1*Power(t2,4) - 
                  180*Power(t2,5)) + 
               Power(s2,4)*(-12*Power(t1,5) + 104*Power(t1,4)*t2 - 
                  564*Power(t1,3)*Power(t2,2) + 
                  981*Power(t1,2)*Power(t2,3) - 510*t1*Power(t2,4) + 
                  115*Power(t2,5)))))*lg2(s2 - t1 + t2))/
     (Power(s1,3)*Power(s2,3)*t1*Power(s - s2 + t1,3)*(s - s1 - s2 + t1)*
       Power(t2,3)*(s - s1 + t2)*(s2 + t2)) - 
    (16*(-2*Power(s2,3)*Power(s2 - t1,2)*Power(t1,2)*Power(t2,4) + 
         Power(s,7)*Power(t1 + t2,2)*(Power(t1,2) + Power(t2,2)) - 
         Power(s1,5)*(s2 - t1)*
          (-2*Power(t1,3)*Power(t2,2) + 
            Power(s2,3)*(Power(t1,2) + t1*t2 + Power(t2,2)) + 
            s2*Power(t1,2)*(Power(t1,2) + t1*t2 + 3*Power(t2,2)) - 
            Power(s2,2)*t1*(2*Power(t1,2) + 2*t1*t2 + 3*Power(t2,2))) + 
         s1*Power(s2,2)*Power(t2,3)*
          (-2*Power(t1,3)*(Power(t1,2) + t1*t2 + Power(t2,2)) - 
            3*Power(s2,2)*t1*(4*Power(t1,2) + t1*t2 + Power(t2,2)) + 
            Power(s2,3)*(5*Power(t1,2) + t1*t2 + Power(t2,2)) + 
            s2*Power(t1,2)*(9*Power(t1,2) + 4*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,2)*s2*Power(t2,2)*
          (3*s2*Power(t1,2)*Power(t1 + t2,3) - 
            2*Power(t1,3)*t2*(Power(t1,2) + t1*t2 + Power(t2,2)) - 
            3*Power(s2,4)*(2*Power(t1,2) + t1*t2 + Power(t2,2)) + 
            Power(s2,3)*(15*Power(t1,3) + 10*Power(t1,2)*t2 + 
               11*t1*Power(t2,2) + Power(t2,3)) - 
            Power(s2,2)*t1*(12*Power(t1,3) + 14*Power(t1,2)*t2 + 
               13*t1*Power(t2,2) + 3*Power(t2,3))) + 
         Power(s1,3)*t2*(-2*Power(t1,4)*Power(t2,3) + 
            Power(s2,5)*(4*Power(t1,2) + 3*t1*t2 + 3*Power(t2,2)) + 
            s2*Power(t1,3)*t2*
             (3*Power(t1,2) + 4*t1*t2 + 9*Power(t2,2)) + 
            10*Power(s2,3)*t1*
             (Power(t1,3) + 2*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
               Power(t2,3)) - 
            Power(s2,4)*(11*Power(t1,3) + 13*Power(t1,2)*t2 + 
               14*t1*Power(t2,2) + 3*Power(t2,3)) - 
            Power(s2,2)*Power(t1,2)*
             (3*Power(t1,3) + 13*Power(t1,2)*t2 + 14*t1*Power(t2,2) + 
               12*Power(t2,3))) - 
         Power(s1,4)*(-4*Power(t1,4)*Power(t2,3) + 
            Power(s2,5)*(Power(t1,2) + t1*t2 + Power(t2,2)) + 
            3*s2*Power(t1,3)*t2*(Power(t1,2) + t1*t2 + 4*Power(t2,2)) - 
            Power(s2,4)*(3*Power(t1,3) + 7*Power(t1,2)*t2 + 
               7*t1*Power(t2,2) + 3*Power(t2,3)) + 
            Power(s2,3)*t1*(3*Power(t1,3) + 14*Power(t1,2)*t2 + 
               13*t1*Power(t2,2) + 11*Power(t2,3)) - 
            Power(s2,2)*Power(t1,2)*
             (Power(t1,3) + 11*Power(t1,2)*t2 + 10*t1*Power(t2,2) + 
               15*Power(t2,3))) + 
         Power(s,6)*(t1 + t2)*
          (Power(t1 + t2,4) - 
            s2*(3*Power(t1,3) + Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
               5*Power(t2,3)) + 
            s1*(-5*Power(t1,3) - 5*Power(t1,2)*t2 - t1*Power(t2,2) - 
               3*Power(t2,3) + 2*s2*Power(t1 + t2,2))) + 
         Power(s,5)*(2*t1*t2*
             (2*Power(t1,4) + 5*Power(t1,3)*t2 + 
               10*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,2)*(3*Power(t1,4) + 2*Power(t1,3)*t2 + 
               8*Power(t1,2)*Power(t2,2) + 21*t1*Power(t2,3) + 
               10*Power(t2,4)) - 
            s2*(2*Power(t1,5) + 11*Power(t1,4)*t2 + 
               20*Power(t1,3)*Power(t2,2) + 
               38*Power(t1,2)*Power(t2,3) + 20*t1*Power(t2,4) + 
               5*Power(t2,5)) + 
            Power(s1,2)*(10*Power(t1,4) + 21*Power(t1,3)*t2 + 
               8*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) + 
               3*Power(t2,4) + Power(s2,2)*Power(t1 + t2,2) - 
               s2*(8*Power(t1,3) + 21*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + 6*Power(t2,3))) - 
            s1*(5*Power(t1,5) + 20*Power(t1,4)*t2 + 
               38*Power(t1,3)*Power(t2,2) + 20*Power(t1,2)*Power(t2,3) + 
               11*t1*Power(t2,4) + 2*Power(t2,5) + 
               Power(s2,2)*(6*Power(t1,3) + 17*Power(t1,2)*t2 + 
                  21*t1*Power(t2,2) + 8*Power(t2,3)) - 
               s2*(16*Power(t1,4) + 33*Power(t1,3)*t2 + 
                  38*Power(t1,2)*Power(t2,2) + 33*t1*Power(t2,3) + 
                  16*Power(t2,4)))) + 
         s*(Power(s1,6)*Power(t1,2)*(-s2 + t1)*t2 + 
            Power(s2,2)*Power(t2,3)*
             (Power(s2,4)*t1 - 
               Power(s2,3)*(8*Power(t1,2) + Power(t2,2)) + 
               2*Power(t1,3)*(Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
               2*Power(s2,2)*t1*
                (7*Power(t1,2) + 3*t1*t2 + 2*Power(t2,2)) - 
               s2*Power(t1,2)*(9*Power(t1,2) + 8*t1*t2 + 6*Power(t2,2))) \
+ Power(s1,5)*(2*Power(s2,3)*(Power(t1,2) + t1*t2 + Power(t2,2)) - 
               Power(s2,2)*t1*
                (5*Power(t1,2) + 6*t1*t2 + 6*Power(t2,2)) - 
               Power(t1,3)*(Power(t1,2) + 8*Power(t2,2)) + 
               s2*Power(t1,2)*(4*Power(t1,2) + 4*t1*t2 + 9*Power(t2,2))) \
+ s1*s2*Power(t2,2)*(-(Power(s2,5)*t1) + 
               2*Power(t1,3)*t2*
                (3*Power(t1,2) + 2*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,4)*(9*Power(t1,2) + 4*t1*t2 + 4*Power(t2,2)) - 
               Power(s2,3)*(19*Power(t1,3) + 30*Power(t1,2)*t2 + 
                  21*t1*Power(t2,2) + 6*Power(t2,3)) - 
               s2*Power(t1,2)*
                (6*Power(t1,3) + 28*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s2,2)*t1*
                (17*Power(t1,3) + 48*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) + 15*Power(t2,3))) + 
            Power(s1,4)*(Power(s2,4)*
                (5*Power(t1,2) + 6*t1*t2 + 5*Power(t2,2)) + 
               2*Power(t1,3)*t2*
                (2*Power(t1,2) + 3*t1*t2 + 7*Power(t2,2)) - 
               Power(s2,3)*(16*Power(t1,3) + 29*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) + 8*Power(t2,3)) - 
               s2*Power(t1,2)*
                (6*Power(t1,3) + 21*Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) + 19*Power(t2,3)) + 
               Power(s2,2)*t1*
                (17*Power(t1,3) + 40*Power(t1,2)*t2 + 
                  41*t1*Power(t2,2) + 20*Power(t2,3))) + 
            Power(s1,2)*t2*(2*Power(t1,3)*Power(t2,2)*
                (2*Power(t1,2) + t1*t2 + Power(t2,2)) - 
               Power(s2,5)*(6*Power(t1,2) + 6*t1*t2 + 5*Power(t2,2)) - 
               s2*Power(t1,2)*t2*
                (12*Power(t1,3) + 27*Power(t1,2)*t2 + 
                  28*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,4)*(20*Power(t1,3) + 41*Power(t1,2)*t2 + 
                  40*t1*Power(t2,2) + 17*Power(t2,3)) - 
               Power(s2,3)*(23*Power(t1,4) + 67*Power(t1,3)*t2 + 
                  76*Power(t1,2)*Power(t2,2) + 49*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,2)*t1*
                (9*Power(t1,4) + 44*Power(t1,3)*t2 + 
                  72*Power(t1,2)*Power(t2,2) + 44*t1*Power(t2,3) + 
                  9*Power(t2,4))) + 
            Power(s1,3)*(2*Power(s2,5)*
                (Power(t1,2) + t1*t2 + Power(t2,2)) - 
               Power(t1,3)*Power(t2,2)*
                (6*Power(t1,2) + 8*t1*t2 + 9*Power(t2,2)) - 
               Power(s2,4)*(8*Power(t1,3) + 27*Power(t1,2)*t2 + 
                  29*t1*Power(t2,2) + 16*Power(t2,3)) + 
               s2*Power(t1,2)*t2*
                (15*Power(t1,3) + 33*Power(t1,2)*t2 + 
                  48*t1*Power(t2,2) + 17*Power(t2,3)) + 
               Power(s2,3)*(10*Power(t1,4) + 59*Power(t1,3)*t2 + 
                  74*Power(t1,2)*Power(t2,2) + 59*t1*Power(t2,3) + 
                  10*Power(t2,4)) - 
               Power(s2,2)*t1*
                (4*Power(t1,4) + 49*Power(t1,3)*t2 + 
                  76*Power(t1,2)*Power(t2,2) + 67*t1*Power(t2,3) + 
                  23*Power(t2,4)))) - 
         Power(s,2)*(Power(s1,5)*
             (Power(s2,2)*(Power(t1,2) + t1*t2 + Power(t2,2)) - 
               2*s2*t1*(Power(t1,2) + 3*t1*t2 + Power(t2,2)) + 
               Power(t1,2)*(Power(t1,2) + 6*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,4)*(Power(s2,3)*
                (7*Power(t1,2) + 9*t1*t2 + 7*Power(t2,2)) - 
               Power(t1,2)*(5*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  32*t1*Power(t2,2) + 4*Power(t2,3)) - 
               Power(s2,2)*(21*Power(t1,3) + 37*Power(t1,2)*t2 + 
                  29*t1*Power(t2,2) + 7*Power(t2,3)) + 
               s2*t1*(19*Power(t1,3) + 33*Power(t1,2)*t2 + 
                  36*t1*Power(t2,2) + 9*Power(t2,3))) + 
            s2*Power(t2,2)*(Power(s2,4)*
                (2*Power(t1,2) + 6*t1*t2 + Power(t2,2)) + 
               4*Power(t1,3)*t2*(Power(t1,2) + 2*Power(t2,2)) - 
               Power(s2,3)*(4*Power(t1,3) + 32*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*t1*
                (5*Power(t1,3) + 40*Power(t1,2)*t2 + 
                  16*t1*Power(t2,2) + 16*Power(t2,3)) - 
               s2*Power(t1,2)*
                (3*Power(t1,3) + 19*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) + 18*Power(t2,3))) + 
            Power(s1,3)*(Power(s2,4)*
                (7*Power(t1,2) + 9*t1*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*t2*
                (16*Power(t1,3) + 16*Power(t1,2)*t2 + 
                  40*t1*Power(t2,2) + 5*Power(t2,3)) - 
               Power(s2,3)*(29*Power(t1,3) + 69*Power(t1,2)*t2 + 
                  69*t1*Power(t2,2) + 29*Power(t2,3)) + 
               Power(s2,2)*(36*Power(t1,4) + 110*Power(t1,3)*t2 + 
                  123*Power(t1,2)*Power(t2,2) + 74*t1*Power(t2,3) + 
                  12*Power(t2,4)) - 
               s2*t1*(14*Power(t1,4) + 68*Power(t1,3)*t2 + 
                  99*Power(t1,2)*Power(t2,2) + 55*t1*Power(t2,3) + 
                  16*Power(t2,4))) + 
            s1*t2*(4*Power(t1,3)*Power(t2,2)*
                (2*Power(t1,2) + Power(t2,2)) - 
               2*Power(s2,5)*(Power(t1,2) + 3*t1*t2 + Power(t2,2)) - 
               s2*Power(t1,2)*t2*
                (15*Power(t1,3) + 43*Power(t1,2)*t2 + 
                  43*t1*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s2,4)*(9*Power(t1,3) + 36*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) + 19*Power(t2,3)) - 
               Power(s2,3)*(16*Power(t1,4) + 55*Power(t1,3)*t2 + 
                  99*Power(t1,2)*Power(t2,2) + 68*t1*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,2)*t1*
                (9*Power(t1,4) + 42*Power(t1,3)*t2 + 
                  112*Power(t1,2)*Power(t2,2) + 73*t1*Power(t2,3) + 
                  27*Power(t2,4))) + 
            Power(s1,2)*(Power(s2,5)*
                (Power(t1,2) + t1*t2 + Power(t2,2)) - 
               Power(t1,2)*Power(t2,2)*
                (18*Power(t1,3) + 14*Power(t1,2)*t2 + 
                  19*t1*Power(t2,2) + 3*Power(t2,3)) - 
               Power(s2,4)*(7*Power(t1,3) + 29*Power(t1,2)*t2 + 
                  37*t1*Power(t2,2) + 21*Power(t2,3)) + 
               s2*t1*t2*(27*Power(t1,4) + 73*Power(t1,3)*t2 + 
                  112*Power(t1,2)*Power(t2,2) + 42*t1*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(s2,3)*(12*Power(t1,4) + 74*Power(t1,3)*t2 + 
                  123*Power(t1,2)*Power(t2,2) + 110*t1*Power(t2,3) + 
                  36*Power(t2,4)) - 
               2*Power(s2,2)*(3*Power(t1,5) + 37*Power(t1,4)*t2 + 
                  72*Power(t1,3)*Power(t2,2) + 
                  72*Power(t1,2)*Power(t2,3) + 37*t1*Power(t2,4) + 
                  3*Power(t2,5)))) + 
         Power(s,4)*(2*Power(t1,2)*Power(t2,2)*
             (3*Power(t1,3) + 5*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
               3*Power(t2,3)) - 
            2*s2*t1*t2*(3*Power(t1,4) + 8*Power(t1,3)*t2 + 
               26*Power(t1,2)*Power(t2,2) + 15*t1*Power(t2,3) + 
               8*Power(t2,4)) - 
            Power(s2,3)*(Power(t1,4) + 8*Power(t1,2)*Power(t2,2) + 
               24*t1*Power(t2,3) + 10*Power(t2,4)) + 
            Power(s2,2)*(Power(t1,5) + 9*Power(t1,4)*t2 + 
               14*Power(t1,3)*Power(t2,2) + 
               62*Power(t1,2)*Power(t2,3) + 30*t1*Power(t2,4) + 
               10*Power(t2,5)) - 
            Power(s1,3)*(10*Power(t1,4) + 24*Power(t1,3)*t2 + 
               8*Power(t1,2)*Power(t2,2) + Power(t2,4) + 
               Power(s2,2)*(3*Power(t1,2) + 5*t1*t2 + 3*Power(t2,2)) - 
               2*s2*(6*Power(t1,3) + 14*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s1,2)*(10*Power(t1,5) + 30*Power(t1,4)*t2 + 
               62*Power(t1,3)*Power(t2,2) + 
               14*Power(t1,2)*Power(t2,3) + 9*t1*Power(t2,4) + 
               Power(t2,5) - 
               Power(s2,3)*(3*Power(t1,2) + 5*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(23*Power(t1,3) + 59*Power(t1,2)*t2 + 
                  59*t1*Power(t2,2) + 23*Power(t2,3)) - 
               s2*(34*Power(t1,4) + 79*Power(t1,3)*t2 + 
                  76*Power(t1,2)*Power(t2,2) + 43*t1*Power(t2,3) + 
                  17*Power(t2,4))) + 
            s1*(2*Power(s2,3)*
                (3*Power(t1,3) + 9*Power(t1,2)*t2 + 14*t1*Power(t2,2) + 
                  6*Power(t2,3)) - 
               2*t1*t2*(8*Power(t1,4) + 15*Power(t1,3)*t2 + 
                  26*Power(t1,2)*Power(t2,2) + 8*t1*Power(t2,3) + 
                  3*Power(t2,4)) - 
               Power(s2,2)*(17*Power(t1,4) + 43*Power(t1,3)*t2 + 
                  76*Power(t1,2)*Power(t2,2) + 79*t1*Power(t2,3) + 
                  34*Power(t2,4)) + 
               s2*(9*Power(t1,5) + 51*Power(t1,4)*t2 + 
                  94*Power(t1,3)*Power(t2,2) + 
                  94*Power(t1,2)*Power(t2,3) + 51*t1*Power(t2,4) + 
                  9*Power(t2,5)))) + 
         Power(s,3)*(Power(s1,4)*
             (Power(s2,2)*(3*Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(5*Power(t1,2) + 16*t1*t2 + 6*Power(t2,2)) - 
               s2*(8*Power(t1,3) + 18*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,3)*(4*Power(s2,3)*
                (2*Power(t1,2) + 3*t1*t2 + 2*Power(t2,2)) - 
               Power(s2,2)*(33*Power(t1,3) + 73*Power(t1,2)*t2 + 
                  61*t1*Power(t2,2) + 22*Power(t2,3)) - 
               t1*(10*Power(t1,4) + 20*Power(t1,3)*t2 + 
                  58*Power(t1,2)*Power(t2,2) + 8*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s2*(36*Power(t1,4) + 79*Power(t1,3)*t2 + 
                  71*Power(t1,2)*Power(t2,2) + 29*t1*Power(t2,3) + 
                  6*Power(t2,4))) + 
            t2*(4*Power(t1,3)*Power(t2,2)*(Power(t1,2) + Power(t2,2)) + 
               Power(s2,4)*t2*
                (6*Power(t1,2) + 16*t1*t2 + 5*Power(t2,2)) - 
               2*s2*Power(t1,2)*t2*
                (3*Power(t1,3) + 10*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) + 9*Power(t2,3)) - 
               Power(s2,3)*(3*Power(t1,4) + 8*Power(t1,3)*t2 + 
                  58*Power(t1,2)*Power(t2,2) + 20*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,2)*t1*
                (3*Power(t1,4) + 11*Power(t1,3)*t2 + 
                  58*Power(t1,2)*Power(t2,2) + 32*t1*Power(t2,3) + 
                  24*Power(t2,4))) + 
            Power(s1,2)*(Power(s2,4)*
                (3*Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)) + 
               6*Power(s2,2)*Power(t1 + t2,2)*
                (6*Power(t1,2) + 7*t1*t2 + 6*Power(t2,2)) - 
               Power(s2,3)*(22*Power(t1,3) + 61*Power(t1,2)*t2 + 
                  73*t1*Power(t2,2) + 33*Power(t2,3)) + 
               t1*t2*(24*Power(t1,4) + 32*Power(t1,3)*t2 + 
                  58*Power(t1,2)*Power(t2,2) + 11*t1*Power(t2,3) + 
                  3*Power(t2,4)) - 
               s2*(16*Power(t1,5) + 88*Power(t1,4)*t2 + 
                  148*Power(t1,3)*Power(t2,2) + 
                  92*Power(t1,2)*Power(t2,3) + 45*t1*Power(t2,4) + 
                  4*Power(t2,5))) - 
            s1*(2*Power(t1,2)*Power(t2,2)*
                (9*Power(t1,3) + 10*Power(t1,2)*t2 + 10*t1*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,4)*(2*Power(t1,3) + 9*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) + 8*Power(t2,3)) - 
               s2*t1*t2*(21*Power(t1,4) + 59*Power(t1,3)*t2 + 
                  128*Power(t1,2)*Power(t2,2) + 59*t1*Power(t2,3) + 
                  21*Power(t2,4)) - 
               Power(s2,3)*(6*Power(t1,4) + 29*Power(t1,3)*t2 + 
                  71*Power(t1,2)*Power(t2,2) + 79*t1*Power(t2,3) + 
                  36*Power(t2,4)) + 
               Power(s2,2)*(4*Power(t1,5) + 45*Power(t1,4)*t2 + 
                  92*Power(t1,3)*Power(t2,2) + 
                  148*Power(t1,2)*Power(t2,3) + 88*t1*Power(t2,4) + 
                  16*Power(t2,5)))))*Log(-s))/
     ((s - s1)*s1*(s - s2)*s2*Power(t1,2)*Power(s - s2 + t1,2)*Power(t2,2)*
       Power(s - s1 + t2,2)) + 
    (8*(-(Power(s1,3)*s2*(s2 - t1)*t1*(s1 - t2)*(s1 + t1 - t2)*t2*
            (s2 - t1 + t2)*Power(s1*(s2 - t1) - s2*t2,2)*
            (s1*(-3*s2 + 2*t1 - 2*t2)*t2 + 2*s2*Power(t2,2) + 
              Power(s1,2)*(s2 - t1 + 2*t2))) + 
         Power(s,10)*t1*t2*(s2 - t1 + t2)*
          (-4*s1*(t1 - 2*t2)*Power(t2,2) + 4*(t1 - t2)*Power(t2,3) + 
            Power(s1,3)*(t1 + t2) + 
            Power(s1,2)*(Power(t1,2) - t1*t2 - 4*Power(t2,2))) + 
         s*t2*(4*Power(s2,2)*Power(s2 - t1,2)*Power(t1,2)*(t1 - t2)*
             Power(t2,7)*(s2 - t1 + t2) + 
            Power(s1,9)*Power(s2 - t1,2)*t1*
             (3*Power(s2,3) + Power(s2,2)*(-3*t1 + 7*t2) + 
               t1*(3*Power(t1,2) - 8*t1*t2 + 4*Power(t2,2)) + 
               s2*(-3*Power(t1,2) + t1*t2 + 4*Power(t2,2))) + 
            Power(s1,8)*(s2 - t1)*
             (5*Power(s2,5)*t1 + 
               Power(s2,4)*(-14*Power(t1,2) + 3*t1*t2 + 
                  2*Power(t2,2)) + 
               s2*Power(t1,2)*
                (8*Power(t1,3) - 51*Power(t1,2)*t2 + 
                  90*t1*Power(t2,2) - 14*Power(t2,3)) + 
               Power(s2,3)*(15*Power(t1,3) - 9*Power(t1,2)*t2 - 
                  17*t1*Power(t2,2) + 2*Power(t2,3)) - 
               Power(s2,2)*t1*
                (11*Power(t1,3) - 33*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) + 13*Power(t2,3)) + 
               Power(t1,3)*(-3*Power(t1,3) + 24*Power(t1,2)*t2 - 
                  64*t1*Power(t2,2) + 37*Power(t2,3))) - 
            s1*s2*(s2 - t1)*t1*Power(t2,6)*
             (Power(s2,4)*(3*t1 - 2*t2) + 
               Power(s2,3)*(Power(t1,2) + 2*t1*t2 - 10*Power(t2,2)) + 
               4*t1*Power(t1 - t2,2)*
                (3*Power(t1,2) - t1*t2 + Power(t2,2)) - 
               2*Power(s2,2)*
                (Power(t1,3) - 3*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
                  5*Power(t2,3)) - 
               2*s2*(7*Power(t1,4) - 10*Power(t1,3)*t2 + 
                  8*Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s1,3)*Power(t2,4)*
             (10*Power(s2,6)*Power(t1 - t2,2) - 
               2*Power(t1,4)*(7*t1 - 10*t2)*Power(t1 - t2,2)*t2 + 
               Power(s2,5)*(59*Power(t1,3) - 63*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*
                (-270*Power(t1,4) + 607*Power(t1,3)*t2 - 
                  372*Power(t1,2)*Power(t2,2) + 75*t1*Power(t2,3) - 
                  28*Power(t2,4)) + 
               Power(s2,3)*t1*
                (397*Power(t1,4) - 742*Power(t1,3)*t2 + 
                  352*Power(t1,2)*Power(t2,2) - 19*t1*Power(t2,3) - 
                  20*Power(t2,4)) - 
               2*Power(s2,4)*
                (135*Power(t1,4) - 209*Power(t1,3)*t2 + 
                  87*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               2*s2*Power(t1,3)*
                (37*Power(t1,4) - 93*Power(t1,3)*t2 + 
                  64*Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) + 
                  16*Power(t2,4))) + 
            Power(s1,2)*Power(t2,5)*
             (4*Power(t1,4)*Power(t1 - t2,3)*t2 + 
               Power(s2,6)*(4*Power(t1,2) - t1*t2 - 2*Power(t2,2)) + 
               Power(s2,4)*t1*
                (106*Power(t1,3) - 140*Power(t1,2)*t2 + 
                  100*t1*Power(t2,2) - 27*Power(t2,3)) - 
               Power(s2,5)*(35*Power(t1,3) - 38*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,3)*t1*
                (-165*Power(t1,4) + 261*Power(t1,3)*t2 - 
                  152*Power(t1,2)*Power(t2,2) + 47*t1*Power(t2,3) + 
                  Power(t2,4)) - 
               2*s2*Power(t1,3)*
                (21*Power(t1,4) - 49*Power(t1,3)*t2 + 
                  36*Power(t1,2)*Power(t2,2) - 15*t1*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (132*Power(t1,4) - 260*Power(t1,3)*t2 + 
                  163*Power(t1,2)*Power(t2,2) - 58*t1*Power(t2,3) + 
                  17*Power(t2,4))) - 
            Power(s1,6)*t2*(Power(s2,6)*
                (19*Power(t1,2) - 53*t1*t2 + 10*Power(t2,2)) - 
               Power(s2,5)*(73*Power(t1,3) - 176*Power(t1,2)*t2 + 
                  75*t1*Power(t2,2) + 10*Power(t2,3)) + 
               2*Power(t1,4)*t2*
                (-12*Power(t1,3) + 87*Power(t1,2)*t2 - 
                  170*t1*Power(t2,2) + 87*Power(t2,3)) + 
               s2*Power(t1,3)*
                (7*Power(t1,4) + 10*Power(t1,3)*t2 - 
                  337*Power(t1,2)*Power(t2,2) + 628*t1*Power(t2,3) - 
                  176*Power(t2,4)) + 
               Power(s2,4)*(98*Power(t1,4) - 197*Power(t1,3)*t2 + 
                  191*Power(t1,2)*Power(t2,2) + 25*t1*Power(t2,3) - 
                  20*Power(t2,4)) + 
               Power(s2,3)*t1*
                (-46*Power(t1,4) + 40*Power(t1,3)*t2 - 
                  234*Power(t1,2)*Power(t2,2) + 133*t1*Power(t2,3) + 
                  17*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (-5*Power(t1,4) + 48*Power(t1,3)*t2 + 
                  271*Power(t1,2)*Power(t2,2) - 441*t1*Power(t2,3) + 
                  19*Power(t2,4))) + 
            Power(s1,7)*(Power(s2,6)*
                (5*Power(t1,2) - 24*t1*t2 + 2*Power(t2,2)) - 
               Power(s2,5)*(22*Power(t1,3) - 101*Power(t1,2)*t2 + 
                  38*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,4)*t2*
                (-13*Power(t1,3) + 89*Power(t1,2)*t2 - 
                  206*t1*Power(t2,2) + 116*Power(t2,3)) + 
               s2*Power(t1,3)*
                (-2*Power(t1,4) + 44*Power(t1,3)*t2 - 
                  245*Power(t1,2)*Power(t2,2) + 464*t1*Power(t2,3) - 
                  152*Power(t2,4)) + 
               Power(s2,4)*(38*Power(t1,4) - 164*Power(t1,3)*t2 + 
                  133*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) - 
                  10*Power(t2,4)) + 
               Power(s2,3)*t1*
                (-32*Power(t1,4) + 139*Power(t1,3)*t2 - 
                  227*Power(t1,2)*Power(t2,2) + 79*t1*Power(t2,3) + 
                  24*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (13*Power(t1,4) - 83*Power(t1,3)*t2 + 
                  286*Power(t1,2)*Power(t2,2) - 351*t1*Power(t2,3) + 
                  26*Power(t2,4))) - 
            Power(s1,4)*Power(t2,3)*
             (Power(s2,6)*(29*Power(t1,2) - 55*t1*t2 + 
                  20*Power(t2,2)) + 
               Power(s2,5)*(12*Power(t1,3) - 17*Power(t1,2)*t2 - 
                  37*t1*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,4)*t2*
                (-26*Power(t1,3) + 119*Power(t1,2)*t2 - 
                  160*t1*Power(t2,2) + 65*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*
                (-300*Power(t1,4) + 809*Power(t1,3)*t2 - 
                  501*Power(t1,2)*Power(t2,2) + 42*t1*Power(t2,3) - 
                  43*Power(t2,4)) + 
               Power(s2,3)*t1*
                (430*Power(t1,4) - 1013*Power(t1,3)*t2 + 
                  470*Power(t1,2)*Power(t2,2) + 24*t1*Power(t2,3) - 
                  31*Power(t2,4)) + 
               Power(s2,4)*(-251*Power(t1,4) + 524*Power(t1,3)*t2 - 
                  159*Power(t1,2)*Power(t2,2) + 17*t1*Power(t2,3) - 
                  10*Power(t2,4)) + 
               s2*Power(t1,3)*
                (80*Power(t1,4) - 222*Power(t1,3)*t2 + 
                  91*Power(t1,2)*Power(t2,2) + 52*t1*Power(t2,3) + 
                  29*Power(t2,4))) - 
            Power(s1,5)*Power(t2,2)*
             (Power(s2,6)*(-32*Power(t1,2) + 70*t1*t2 - 
                  20*Power(t2,2)) + 
               Power(s2,5)*t1*
                (69*Power(t1,2) - 110*t1*t2 + 75*Power(t2,2)) + 
               Power(t1,4)*t2*
                (30*Power(t1,3) - 189*Power(t1,2)*t2 + 
                  308*t1*Power(t2,2) - 140*Power(t2,3)) + 
               Power(s2,3)*t1*
                (-161*Power(t1,4) + 571*Power(t1,3)*t2 - 
                  179*Power(t1,2)*Power(t2,2) - 79*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,4)*(18*Power(t1,4) - 172*Power(t1,3)*t2 - 
                  20*Power(t1,2)*Power(t2,2) - 25*t1*Power(t2,3) + 
                  20*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (152*Power(t1,4) - 519*Power(t1,3)*t2 + 
                  185*Power(t1,2)*Power(t2,2) + 187*t1*Power(t2,3) + 
                  28*Power(t2,4)) + 
               s2*Power(t1,3)*
                (-46*Power(t1,4) + 130*Power(t1,3)*t2 + 
                  147*Power(t1,2)*Power(t2,2) - 377*t1*Power(t2,3) + 
                  64*Power(t2,4)))) + 
         Power(s,9)*t2*(Power(s1,4)*t1*
             (Power(s2,2) + 5*Power(t1,2) - t1*t2 - 4*Power(t2,2) - 
               3*s2*(2*t1 + t2)) + 
            2*t1*(t1 - t2)*t2*
             (2*Power(s2,2)*(Power(t1,2) - t1*t2 - 4*Power(t2,2)) + 
               2*s2*t1*(-Power(t1,2) + t1*t2 + 3*Power(t2,2)) + 
               t2*(Power(t1,3) - 9*t1*Power(t2,2) + 8*Power(t2,3))) + 
            Power(s1,3)*(Power(s2,2)*
                (Power(t1,2) - 9*t1*t2 + 2*Power(t2,2)) + 
               s2*(-5*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(2*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  35*t1*Power(t2,2) + 28*Power(t2,3))) - 
            Power(s1,2)*(Power(s2,2)*
                (4*Power(t1,3) - 3*Power(t1,2)*t2 - 
                  28*t1*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-5*Power(t1,4) + 17*Power(t1,3)*t2 - 
                  24*Power(t1,2)*Power(t2,2) + 30*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               t1*(3*Power(t1,4) - 21*Power(t1,3)*t2 + 
                  73*Power(t1,2)*Power(t2,2) - 117*t1*Power(t2,3) + 
                  62*Power(t2,4))) + 
            s1*(t1*Power(t2,2)*
                (-17*Power(t1,3) + 77*Power(t1,2)*t2 - 
                  115*t1*Power(t2,2) + 55*Power(t2,3)) + 
               2*Power(s2,2)*
                (-2*Power(t1,4) + 7*Power(t1,3)*t2 + 
                  5*Power(t1,2)*Power(t2,2) - 19*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(4*Power(t1,5) - 14*Power(t1,4)*t2 + 
                  11*Power(t1,3)*Power(t2,2) - 
                  6*Power(t1,2)*Power(t2,3) + 15*t1*Power(t2,4) + 
                  2*Power(t2,5)))) + 
         Power(s,2)*(Power(s1,9)*(s2 - t1)*t1*
             (2*Power(s2,3)*(t1 - 2*t2) + 
               Power(s2,2)*(-6*Power(t1,2) + 9*t1*t2 - 4*Power(t2,2)) + 
               2*s2*t1*(3*Power(t1,2) - 3*t1*t2 - 2*Power(t2,2)) + 
               t1*(-2*Power(t1,3) + Power(t1,2)*t2 + 8*t1*Power(t2,2) - 
                  4*Power(t2,3))) + 
            s2*(s2 - t1)*t1*Power(t2,7)*
             (Power(s2,4)*t1 + 12*Power(t1,3)*Power(t1 - t2,2) + 
               Power(s2,3)*(7*Power(t1,2) - 4*t1*t2 - 6*Power(t2,2)) + 
               Power(s2,2)*(-6*Power(t1,3) + 4*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) - 6*Power(t2,3)) - 
               2*s2*t1*(7*Power(t1,3) - 12*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) - 4*Power(t2,3))) + 
            Power(s1,8)*(2*Power(s2,5)*
                (Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,4)*(-6*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(2*Power(t1,4) - 25*Power(t1,3)*t2 + 
                  49*Power(t1,2)*Power(t2,2) + 33*t1*Power(t2,3) - 
                  38*Power(t2,4)) + 
               Power(s2,2)*t1*
                (4*Power(t1,4) - 99*Power(t1,3)*t2 + 
                  98*Power(t1,2)*Power(t2,2) + 63*t1*Power(t2,3) - 
                  10*Power(t2,4)) + 
               Power(s2,3)*(4*Power(t1,4) + 37*Power(t1,3)*t2 - 
                  10*Power(t1,2)*Power(t2,2) - 13*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s2*Power(t1,2)*
                (-6*Power(t1,4) + 85*Power(t1,3)*t2 - 
                  126*Power(t1,2)*Power(t2,2) - 85*t1*Power(t2,3) + 
                  38*Power(t2,4))) + 
            s1*Power(t2,6)*(-(Power(s2,6)*
                  (7*Power(t1,2) + t1*t2 - 2*Power(t2,2))) - 
               4*Power(t1,3)*Power(t1 - t2,2)*t2*
                (2*Power(t1,2) + Power(t2,2)) + 
               6*s2*Power(t1,2)*Power(t1 - t2,2)*
                (14*Power(t1,3) + 5*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,5)*(20*Power(t1,3) - 15*Power(t1,2)*t2 + 
                  37*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,4)*t1*
                (-81*Power(t1,3) + 86*Power(t1,2)*t2 - 
                  80*t1*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,3)*t1*
                (214*Power(t1,4) - 266*Power(t1,3)*t2 + 
                  76*Power(t1,2)*Power(t2,2) + 39*t1*Power(t2,3) - 
                  39*Power(t2,4)) - 
               Power(s2,2)*t1*
                (230*Power(t1,5) - 342*Power(t1,4)*t2 + 
                  81*Power(t1,3)*Power(t2,2) + 
                  58*Power(t1,2)*Power(t2,3) - 39*t1*Power(t2,4) + 
                  6*Power(t2,5))) + 
            Power(s1,7)*(Power(s2,6)*t2*(-5*t1 + 4*t2) + 
               Power(s2,5)*(2*Power(t1,3) + 8*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - 24*Power(t2,3)) + 
               Power(s2,4)*(-8*Power(t1,4) + 19*Power(t1,3)*t2 - 
                  28*Power(t1,2)*Power(t2,2) + 103*t1*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(t1,3)*t2*
                (-22*Power(t1,4) + 147*Power(t1,3)*t2 - 
                  289*Power(t1,2)*Power(t2,2) + 8*t1*Power(t2,3) + 
                  98*Power(t2,4)) + 
               s2*Power(t1,2)*
                (2*Power(t1,5) + 53*Power(t1,4)*t2 - 
                  391*Power(t1,3)*Power(t2,2) + 
                  631*Power(t1,2)*Power(t2,3) + 119*t1*Power(t2,4) - 
                  75*Power(t2,5)) - 
               Power(s2,2)*t1*
                (8*Power(t1,5) + 20*Power(t1,4)*t2 - 
                  355*Power(t1,3)*Power(t2,2) + 
                  385*Power(t1,2)*Power(t2,3) + 215*t1*Power(t2,4) - 
                  35*Power(t2,5)) + 
               Power(s2,3)*(12*Power(t1,5) - 33*Power(t1,4)*t2 - 
                  102*Power(t1,3)*Power(t2,2) - 
                  36*Power(t1,2)*Power(t2,3) + 72*t1*Power(t2,4) + 
                  14*Power(t2,5))) - 
            Power(s1,2)*Power(t2,5)*
             (-2*Power(s2,6)*(Power(t1,2) + 11*t1*t2 - 8*Power(t2,2)) + 
               Power(s2,5)*(129*Power(t1,3) - 108*Power(t1,2)*t2 + 
                  77*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,3)*t1*
                (859*Power(t1,4) - 1146*Power(t1,3)*t2 + 
                  158*Power(t1,2)*Power(t2,2) + 272*t1*Power(t2,3) - 
                  143*Power(t2,4)) - 
               2*Power(t1,3)*t2*
                (21*Power(t1,4) - 49*Power(t1,3)*t2 + 
                  36*Power(t1,2)*Power(t2,2) - 15*t1*Power(t2,3) + 
                  7*Power(t2,4)) - 
               Power(s2,4)*(510*Power(t1,4) - 584*Power(t1,3)*t2 + 
                  205*Power(t1,2)*Power(t2,2) + 23*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,2)*t1*
                (-694*Power(t1,5) + 1096*Power(t1,4)*t2 - 
                  211*Power(t1,3)*Power(t2,2) - 
                  154*Power(t1,2)*Power(t2,3) + 26*t1*Power(t2,4) + 
                  5*Power(t2,5)) + 
               2*s2*Power(t1,2)*
                (109*Power(t1,5) - 181*Power(t1,4)*t2 + 
                  34*Power(t1,3)*Power(t2,2) + 
                  2*Power(t1,2)*Power(t2,3) + 17*t1*Power(t2,4) + 
                  16*Power(t2,5))) + 
            Power(s1,3)*Power(t2,4)*
             (Power(s2,6)*(23*Power(t1,2) - 74*t1*t2 + 
                  48*Power(t2,2)) + 
               Power(s2,5)*(205*Power(t1,3) - 252*Power(t1,2)*t2 + 
                  84*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(t1,3)*t2*
                (-96*Power(t1,4) + 293*Power(t1,3)*t2 - 
                  296*Power(t1,2)*Power(t2,2) + 139*t1*Power(t2,3) - 
                  42*Power(t2,4)) - 
               Power(s2,4)*(925*Power(t1,4) - 1406*Power(t1,3)*t2 + 
                  393*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) + 
                  46*Power(t2,4)) - 
               Power(s2,2)*t1*
                (1066*Power(t1,5) - 2003*Power(t1,4)*t2 + 
                  524*Power(t1,3)*Power(t2,2) + 
                  92*Power(t1,2)*Power(t2,3) + 223*t1*Power(t2,4) - 
                  69*Power(t2,5)) + 
               Power(s2,3)*(1453*Power(t1,5) - 2421*Power(t1,4)*t2 + 
                  438*Power(t1,3)*Power(t2,2) + 
                  452*Power(t1,2)*Power(t2,3) - 197*t1*Power(t2,4) + 
                  6*Power(t2,5)) + 
               s2*Power(t1,2)*
                (310*Power(t1,5) - 566*Power(t1,4)*t2 + 
                  47*Power(t1,3)*Power(t2,2) + 
                  8*Power(t1,2)*Power(t2,3) + 160*t1*Power(t2,4) + 
                  61*Power(t2,5))) - 
            Power(s1,6)*t2*(Power(s2,6)*
                (5*Power(t1,2) - 29*t1*t2 + 24*Power(t2,2)) + 
               Power(s2,5)*(-16*Power(t1,3) + 24*Power(t1,2)*t2 + 
                  24*t1*Power(t2,2) - 58*Power(t2,3)) + 
               s2*Power(t1,3)*
                (15*Power(t1,4) + 81*Power(t1,3)*t2 - 
                  947*Power(t1,2)*Power(t2,2) + 1408*t1*Power(t2,3) - 
                  53*Power(t2,4)) + 
               Power(s2,4)*(3*Power(t1,4) + 142*Power(t1,3)*t2 - 
                  61*Power(t1,2)*Power(t2,2) + 228*t1*Power(t2,3) - 
                  46*Power(t2,4)) + 
               Power(t1,3)*t2*
                (-69*Power(t1,4) + 437*Power(t1,3)*t2 - 
                  749*Power(t1,2)*Power(t2,2) + 219*t1*Power(t2,3) + 
                  82*Power(t2,4)) + 
               Power(s2,3)*(37*Power(t1,5) - 297*Power(t1,4)*t2 - 
                  165*Power(t1,3)*Power(t2,2) - 
                  22*Power(t1,2)*Power(t2,3) + 96*t1*Power(t2,4) + 
                  36*Power(t2,5)) + 
               Power(s2,2)*t1*
                (-44*Power(t1,5) + 148*Power(t1,4)*t2 + 
                  688*Power(t1,3)*Power(t2,2) - 
                  783*Power(t1,2)*Power(t2,3) - 343*t1*Power(t2,4) + 
                  63*Power(t2,5))) + 
            Power(s1,4)*Power(t2,3)*
             (Power(s2,6)*(-34*Power(t1,2) + 105*t1*t2 - 
                  72*Power(t2,2)) + 
               Power(s2,5)*(-107*Power(t1,3) + 258*Power(t1,2)*t2 - 
                  67*t1*Power(t2,2) + 40*Power(t2,3)) + 
               Power(t1,3)*t2*
                (130*Power(t1,4) - 564*Power(t1,3)*t2 + 
                  735*Power(t1,2)*Power(t2,2) - 356*t1*Power(t2,3) + 
                  74*Power(t2,4)) + 
               Power(s2,4)*(672*Power(t1,4) - 1563*Power(t1,3)*t2 + 
                  448*Power(t1,2)*Power(t2,2) - 146*t1*Power(t2,3) + 
                  86*Power(t2,4)) + 
               Power(s2,2)*t1*
                (848*Power(t1,5) - 2035*Power(t1,4)*t2 + 
                  380*Power(t1,3)*Power(t2,2) + 
                  242*Power(t1,2)*Power(t2,3) + 470*t1*Power(t2,4) - 
                  122*Power(t2,5)) - 
               Power(s2,3)*(1133*Power(t1,5) - 2615*Power(t1,4)*t2 + 
                  528*Power(t1,3)*Power(t2,2) + 
                  342*Power(t1,2)*Power(t2,3) - 114*t1*Power(t2,4) + 
                  26*Power(t2,5)) - 
               2*s2*Power(t1,2)*
                (123*Power(t1,5) - 245*Power(t1,4)*t2 - 
                  208*Power(t1,3)*Power(t2,2) + 
                  307*Power(t1,2)*Power(t2,3) + 31*t1*Power(t2,4) + 
                  63*Power(t2,5))) + 
            Power(s1,5)*Power(t2,2)*
             (Power(s2,6)*(20*Power(t1,2) - 76*t1*t2 + 58*Power(t2,2)) - 
               Power(s2,5)*(13*Power(t1,3) + 81*Power(t1,2)*t2 - 
                  44*t1*Power(t2,2) + 70*Power(t2,3)) + 
               Power(s2,4)*(-150*Power(t1,4) + 776*Power(t1,3)*t2 - 
                  239*Power(t1,2)*Power(t2,2) + 268*t1*Power(t2,3) - 
                  84*Power(t2,4)) + 
               Power(t1,3)*t2*
                (-116*Power(t1,4) + 665*Power(t1,3)*t2 - 
                  1007*Power(t1,2)*Power(t2,2) + 425*t1*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(s2,3)*(353*Power(t1,5) - 1362*Power(t1,4)*t2 + 
                  108*Power(t1,3)*Power(t2,2) + 
                  120*Power(t1,2)*Power(t2,3) + 20*t1*Power(t2,4) + 
                  44*Power(t2,5)) + 
               Power(s2,2)*t1*
                (-304*Power(t1,5) + 999*Power(t1,4)*t2 + 
                  402*Power(t1,3)*Power(t2,2) - 
                  719*Power(t1,2)*Power(t2,3) - 451*t1*Power(t2,4) + 
                  102*Power(t2,5)) + 
               s2*Power(t1,2)*
                (94*Power(t1,5) - 140*Power(t1,4)*t2 - 
                  1044*Power(t1,3)*Power(t2,2) + 
                  1476*Power(t1,2)*Power(t2,3) - 145*t1*Power(t2,4) + 
                  126*Power(t2,5)))) + 
         Power(s,8)*(2*Power(s1,5)*t1*t2*
             (-2*Power(s2,2) - 5*Power(t1,2) + 2*t1*t2 + 
               3*Power(t2,2) + s2*(7*t1 + t2)) + 
            Power(s1,4)*(2*Power(s2,3)*t2*(-5*t1 + 2*t2) + 
               Power(s2,2)*t2*
                (20*Power(t1,2) + 23*t1*t2 - 2*Power(t2,2)) + 
               t1*(-2*Power(t1,4) + 12*Power(t1,3)*t2 - 
                  50*Power(t1,2)*Power(t2,2) + 97*t1*Power(t2,3) - 
                  57*Power(t2,4)) + 
               s2*(2*Power(t1,4) - 12*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 14*t1*Power(t2,3) - 
                  6*Power(t2,4))) - 
            t1*(t1 - t2)*Power(t2,2)*
             (Power(s2,3)*(8*Power(t1,2) - 17*t1*t2 - 24*Power(t2,2)) + 
               Power(s2,2)*(-16*Power(t1,3) + 6*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) + 42*Power(t2,3)) - 
               2*t2*(2*Power(t1,4) + 9*Power(t1,3)*t2 - 
                  8*Power(t1,2)*Power(t2,2) - 15*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               2*s2*(4*Power(t1,4) + 8*Power(t1,3)*t2 + 
                  2*Power(t1,2)*Power(t2,2) - 31*t1*Power(t2,3) + 
                  21*Power(t2,4))) - 
            Power(s1,3)*(4*Power(s2,3)*t2*
                (5*Power(t1,2) - 11*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-62*Power(t1,3) + 50*Power(t1,2)*t2 + 
                  93*t1*Power(t2,2) - 2*Power(t2,3)) + 
               t1*(2*Power(t1,5) - 27*Power(t1,4)*t2 + 
                  97*Power(t1,3)*Power(t2,2) - 
                  289*Power(t1,2)*Power(t2,3) + 387*t1*Power(t2,4) - 
                  170*Power(t2,5)) + 
               s2*(-2*Power(t1,5) + 60*Power(t1,4)*t2 - 
                  52*Power(t1,3)*Power(t2,2) + 
                  Power(t1,2)*Power(t2,3) + t1*Power(t2,4) - 
                  18*Power(t2,5))) + 
            Power(s1,2)*t2*(t1*Power(t1 - t2,2)*
                (3*Power(t1,3) + 9*Power(t1,2)*t2 + 
                  90*t1*Power(t2,2) - 224*Power(t2,3)) - 
               2*Power(s2,3)*
                (Power(t1,3) - 8*Power(t1,2)*t2 + 41*t1*Power(t2,2) - 
                  10*Power(t2,3)) + 
               Power(s2,2)*(22*Power(t1,4) - 57*Power(t1,3)*t2 - 
                  24*Power(t1,2)*Power(t2,2) + 179*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*(-24*Power(t1,5) + 28*Power(t1,4)*t2 + 
                  99*Power(t1,3)*Power(t2,2) - 
                  164*Power(t1,2)*Power(t2,3) + 75*t1*Power(t2,4) - 
                  18*Power(t2,5))) + 
            s1*t2*(Power(s2,3)*
                (8*Power(t1,4) - 30*Power(t1,3)*t2 - 
                  7*Power(t1,2)*Power(t2,2) + 73*t1*Power(t2,3) - 
                  8*Power(t2,4)) - 
               4*t1*t2*(Power(t1,5) + 9*Power(t1,4)*t2 - 
                  11*Power(t1,3)*Power(t2,2) - 
                  43*Power(t1,2)*Power(t2,3) + 76*t1*Power(t2,4) - 
                  32*Power(t2,5)) - 
               2*Power(s2,2)*
                (8*Power(t1,5) - 9*Power(t1,4)*t2 - 
                  9*Power(t1,3)*Power(t2,2) - 
                  38*Power(t1,2)*Power(t2,3) + 74*t1*Power(t2,4) + 
                  Power(t2,5)) + 
               s2*(8*Power(t1,6) + 18*Power(t1,5)*t2 + 
                  8*Power(t1,4)*Power(t2,2) - 
                  201*Power(t1,3)*Power(t2,3) + 
                  260*Power(t1,2)*Power(t2,4) - 107*t1*Power(t2,5) + 
                  6*Power(t2,6)))) - 
         Power(s,3)*(2*Power(s1,9)*(s2 - t1)*t1*
             (Power(s2,2)*(t1 - 2*t2) + Power(t1,2)*(t1 - 2*t2) - 
               2*s2*Power(t1 - t2,2)) + 
            t1*Power(t2,6)*(-3*Power(s2,6)*t1 - 
               4*Power(t1,3)*Power(t1 - t2,2)*t2*(t1 + t2) + 
               Power(s2,4)*t1*
                (Power(t1,2) + 14*t1*t2 - 36*Power(t2,2)) - 
               Power(s2,5)*(7*Power(t1,2) + t1*t2 - 22*Power(t2,2)) + 
               Power(s2,3)*(73*Power(t1,4) - 75*Power(t1,3)*t2 + 
                  4*Power(t1,2)*Power(t2,2) + 30*t1*Power(t2,3) - 
                  22*Power(t2,4)) - 
               2*Power(s2,2)*t1*
                (53*Power(t1,4) - 53*Power(t1,3)*t2 - 
                  18*Power(t1,2)*Power(t2,2) + 26*t1*Power(t2,3) - 
                  8*Power(t2,4)) + 
               2*s2*Power(t1,2)*
                (21*Power(t1,4) - 20*Power(t1,3)*t2 - 
                  15*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s1,8)*(-2*Power(s2,5)*t2 + 
               2*Power(s2,4)*(4*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(-32*Power(t1,3) + 18*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*t1*
                (48*Power(t1,3) - 13*Power(t1,2)*t2 - 
                  76*t1*Power(t2,2) + 13*Power(t2,3)) + 
               Power(t1,2)*(8*Power(t1,4) + 3*Power(t1,3)*t2 - 
                  64*Power(t1,2)*Power(t2,2) + 29*t1*Power(t2,3) + 
                  Power(t2,4)) - 
               s2*t1*(32*Power(t1,4) + 2*Power(t1,3)*t2 - 
                  130*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) + 
                  5*Power(t2,4))) + 
            Power(s1,7)*(-2*Power(s2,6)*t2 + 
               Power(s2,5)*(4*Power(t1,2) - 11*t1*t2 + 
                  24*Power(t2,2)) - 
               Power(s2,4)*(12*Power(t1,3) - 3*Power(t1,2)*t2 + 
                  26*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,3)*(2*Power(t1,4) + 132*Power(t1,3)*t2 - 
                  57*Power(t1,2)*Power(t2,2) + 8*t1*Power(t2,3) - 
                  30*Power(t2,4)) + 
               Power(s2,2)*(26*Power(t1,5) - 295*Power(t1,4)*t2 + 
                  222*Power(t1,3)*Power(t2,2) + 
                  243*Power(t1,2)*Power(t2,3) - 21*t1*Power(t2,4) + 
                  4*Power(t2,5)) + 
               Power(t1,2)*(10*Power(t1,5) - 76*Power(t1,4)*t2 + 
                  89*Power(t1,3)*Power(t2,2) + 
                  211*Power(t1,2)*Power(t2,3) - 164*t1*Power(t2,4) + 
                  17*Power(t2,5)) + 
               s2*t1*(-30*Power(t1,5) + 249*Power(t1,4)*t2 - 
                  252*Power(t1,3)*Power(t2,2) - 
                  390*Power(t1,2)*Power(t2,3) + 29*t1*Power(t2,4) + 
                  22*Power(t2,5))) + 
            Power(s1,6)*(-6*Power(s2,6)*(t1 - 3*t2)*t2 + 
               Power(s2,5)*(4*Power(t1,3) - 24*Power(t1,2)*t2 + 
                  87*t1*Power(t2,2) - 96*Power(t2,3)) + 
               Power(s2,4)*(-20*Power(t1,4) + 143*Power(t1,3)*t2 - 
                  150*Power(t1,2)*Power(t2,2) + 176*t1*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(t1,2)*t2*
                (63*Power(t1,5) - 364*Power(t1,4)*t2 + 
                  466*Power(t1,3)*Power(t2,2) + 
                  171*Power(t1,2)*Power(t2,3) - 263*t1*Power(t2,4) + 
                  81*Power(t2,5)) + 
               Power(s2,3)*(36*Power(t1,5) - 195*Power(t1,4)*t2 - 
                  218*Power(t1,3)*Power(t2,2) + 
                  37*Power(t1,2)*Power(t2,3) - 75*t1*Power(t2,4) + 
                  98*Power(t2,5)) + 
               s2*t1*(8*Power(t1,6) + 121*Power(t1,5)*t2 - 
                  943*Power(t1,4)*Power(t2,2) + 
                  1015*Power(t1,3)*Power(t2,3) + 
                  316*Power(t1,2)*Power(t2,4) + 224*t1*Power(t2,5) - 
                  57*Power(t2,6)) + 
               Power(s2,2)*(-28*Power(t1,6) + 24*Power(t1,5)*t2 + 
                  842*Power(t1,4)*Power(t2,2) - 
                  610*Power(t1,3)*Power(t2,3) - 
                  451*Power(t1,2)*Power(t2,4) + 51*t1*Power(t2,5) - 
                  18*Power(t2,6))) + 
            s1*Power(t2,5)*(Power(s2,6)*
                (2*Power(t1,2) + 15*t1*t2 - 6*Power(t2,2)) - 
               Power(s2,5)*(9*Power(t1,3) + 11*Power(t1,2)*t2 + 
                  88*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,4)*(174*Power(t1,4) - 112*Power(t1,3)*t2 + 
                  39*Power(t1,2)*Power(t2,2) + 79*t1*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s2,3)*t1*
                (-535*Power(t1,4) + 425*Power(t1,3)*t2 + 
                  276*Power(t1,2)*Power(t2,2) - 359*t1*Power(t2,3) + 
                  145*Power(t2,4)) + 
               Power(s2,2)*t1*
                (574*Power(t1,5) - 509*Power(t1,4)*t2 - 
                  355*Power(t1,3)*Power(t2,2) + 
                  249*Power(t1,2)*Power(t2,3) + 46*t1*Power(t2,4) - 
                  57*Power(t2,5)) + 
               2*Power(t1,2)*t2*
                (21*Power(t1,5) - 26*Power(t1,4)*t2 - 
                  6*Power(t1,3)*Power(t2,2) + 
                  5*Power(t1,2)*Power(t2,3) + 3*t1*Power(t2,4) + 
                  3*Power(t2,5)) - 
               2*s2*t1*(103*Power(t1,6) - 75*Power(t1,5)*t2 - 
                  95*Power(t1,4)*Power(t2,2) + 
                  3*Power(t1,3)*Power(t2,3) + 
                  63*Power(t1,2)*Power(t2,4) - 6*t1*Power(t2,5) + 
                  3*Power(t2,6))) - 
            Power(s1,2)*Power(t2,4)*
             (Power(s2,6)*(-20*Power(t1,2) + 71*t1*t2 - 
                  34*Power(t2,2)) + 
               Power(s2,5)*(-121*Power(t1,3) + 61*Power(t1,2)*t2 - 
                  184*t1*Power(t2,2) + 32*Power(t2,3)) + 
               Power(s2,4)*(768*Power(t1,4) - 688*Power(t1,3)*t2 + 
                  48*Power(t1,2)*Power(t2,2) + 219*t1*Power(t2,3) + 
                  48*Power(t2,4)) + 
               Power(s2,2)*t1*
                (1326*Power(t1,5) - 1373*Power(t1,4)*t2 - 
                  728*Power(t1,3)*Power(t2,2) + 
                  140*Power(t1,2)*Power(t2,3) + 599*t1*Power(t2,4) - 
                  280*Power(t2,5)) + 
               Power(s2,3)*(-1523*Power(t1,5) + 1465*Power(t1,4)*t2 + 
                  757*Power(t1,3)*Power(t2,2) - 
                  935*Power(t1,2)*Power(t2,3) + 305*t1*Power(t2,4) - 
                  18*Power(t2,5)) + 
               Power(t1,2)*t2*
                (132*Power(t1,5) - 219*Power(t1,4)*t2 + 
                  68*Power(t1,3)*Power(t2,2) - 
                  90*Power(t1,2)*Power(t2,3) + 94*t1*Power(t2,4) + 
                  15*Power(t2,5)) + 
               s2*t1*(-430*Power(t1,6) + 332*Power(t1,5)*t2 + 
                  385*Power(t1,4)*Power(t2,2) + 
                  265*Power(t1,3)*Power(t2,3) - 
                  413*Power(t1,2)*Power(t2,4) - 82*t1*Power(t2,5) + 
                  7*Power(t2,6))) + 
            Power(s1,3)*Power(t2,3)*
             (Power(s2,6)*(-38*Power(t1,2) + 129*t1*t2 - 
                  78*Power(t2,2)) + 
               Power(s2,5)*(-180*Power(t1,3) + 272*Power(t1,2)*t2 - 
                  297*t1*Power(t2,2) + 120*Power(t2,3)) + 
               Power(s2,4)*(1056*Power(t1,4) - 1602*Power(t1,3)*t2 + 
                  309*Power(t1,2)*Power(t2,2) + 112*t1*Power(t2,3) + 
                  106*Power(t2,4)) + 
               Power(s2,3)*(-1892*Power(t1,5) + 2681*Power(t1,4)*t2 + 
                  711*Power(t1,3)*Power(t2,2) - 
                  1032*Power(t1,2)*Power(t2,3) + 315*t1*Power(t2,4) - 
                  86*Power(t2,5)) + 
               Power(t1,2)*t2*
                (220*Power(t1,5) - 556*Power(t1,4)*t2 + 
                  500*Power(t1,3)*Power(t2,2) - 
                  476*Power(t1,2)*Power(t2,3) + 289*t1*Power(t2,4) + 
                  33*Power(t2,5)) + 
               Power(s2,2)*(1526*Power(t1,6) - 2114*Power(t1,5)*t2 - 
                  781*Power(t1,4)*Power(t2,2) - 
                  233*Power(t1,3)*Power(t2,3) + 
                  1383*Power(t1,2)*Power(t2,4) - 519*t1*Power(t2,5) + 
                  6*Power(t2,6)) + 
               s2*t1*(-472*Power(t1,6) + 414*Power(t1,5)*t2 + 
                  752*Power(t1,4)*Power(t2,2) + 
                  117*Power(t1,3)*Power(t2,3) - 
                  286*Power(t1,2)*Power(t2,4) - 533*t1*Power(t2,5) + 
                  78*Power(t2,6))) + 
            Power(s1,5)*t2*(Power(s2,6)*
                (-4*Power(t1,2) + 44*t1*t2 - 58*Power(t2,2)) + 
               Power(s2,5)*(-11*Power(t1,3) + 143*Power(t1,2)*t2 - 
                  246*t1*Power(t2,2) + 190*Power(t2,3)) + 
               Power(s2,4)*(108*Power(t1,4) - 745*Power(t1,3)*t2 + 
                  434*Power(t1,2)*Power(t2,2) - 347*t1*Power(t2,3) + 
                  46*Power(t2,4)) + 
               Power(s2,3)*(-233*Power(t1,5) + 1128*Power(t1,4)*t2 + 
                  276*Power(t1,3)*Power(t2,2) - 
                  102*Power(t1,2)*Power(t2,3) + 166*t1*Power(t2,4) - 
                  172*Power(t2,5)) + 
               Power(t1,2)*t2*
                (156*Power(t1,5) - 811*Power(t1,4)*t2 + 
                  1028*Power(t1,3)*Power(t2,2) - 
                  331*Power(t1,2)*Power(t2,3) - 31*t1*Power(t2,4) + 
                  128*Power(t2,5)) + 
               Power(s2,2)*(206*Power(t1,6) - 650*Power(t1,5)*t2 - 
                  1344*Power(t1,4)*Power(t2,2) + 
                  688*Power(t1,3)*Power(t2,3) + 
                  811*Power(t1,2)*Power(t2,4) - 201*t1*Power(t2,5) + 
                  30*Power(t2,6)) + 
               s2*t1*(-66*Power(t1,6) - 76*Power(t1,5)*t2 + 
                  1765*Power(t1,4)*Power(t2,2) - 
                  1708*Power(t1,3)*Power(t2,3) + 
                  258*Power(t1,2)*Power(t2,4) - 800*t1*Power(t2,5) + 
                  114*Power(t2,6))) - 
            Power(s1,4)*Power(t2,2)*
             (Power(s2,6)*(-23*Power(t1,2) + 111*t1*t2 - 
                  92*Power(t2,2)) + 
               Power(s2,5)*(-82*Power(t1,3) + 322*Power(t1,2)*t2 - 
                  349*t1*Power(t2,2) + 206*Power(t2,3)) + 
               Power(s2,4)*(549*Power(t1,4) - 1626*Power(t1,3)*t2 + 
                  559*Power(t1,2)*Power(t2,2) - 229*t1*Power(t2,3) + 
                  108*Power(t2,4)) + 
               Power(s2,3)*(-1022*Power(t1,5) + 2497*Power(t1,4)*t2 + 
                  392*Power(t1,3)*Power(t2,2) - 
                  528*Power(t1,2)*Power(t2,3) + 236*t1*Power(t2,4) - 
                  168*Power(t2,5)) + 
               Power(t1,2)*t2*
                (230*Power(t1,5) - 900*Power(t1,4)*t2 + 
                  1075*Power(t1,3)*Power(t2,2) - 
                  731*Power(t1,2)*Power(t2,3) + 298*t1*Power(t2,4) + 
                  89*Power(t2,5)) + 
               Power(s2,2)*(840*Power(t1,6) - 1736*Power(t1,5)*t2 - 
                  1121*Power(t1,4)*Power(t2,2) + 
                  105*Power(t1,3)*Power(t2,3) + 
                  1385*Power(t1,2)*Power(t2,4) - 458*t1*Power(t2,5) + 
                  22*Power(t2,6)) + 
               s2*t1*(-262*Power(t1,6) + 202*Power(t1,5)*t2 + 
                  1565*Power(t1,4)*Power(t2,2) - 
                  1073*Power(t1,3)*Power(t2,3) + 
                  311*Power(t1,2)*Power(t2,4) - 1004*t1*Power(t2,5) + 
                  139*Power(t2,6)))) + 
         Power(s,7)*(2*Power(s1,6)*t1*t2*
             (3*Power(s2,2) + 5*Power(t1,2) - 3*t1*t2 - 2*Power(t2,2) + 
               s2*(-8*t1 + t2)) + 
            Power(s1,5)*(2*Power(s2,4)*t2 + 
               Power(s2,3)*(33*t1 - 10*t2)*t2 + 
               Power(s2,2)*(4*Power(t1,3) - 75*Power(t1,2)*t2 - 
                  2*t1*Power(t2,2) - 6*Power(t2,3)) + 
               s2*(-14*Power(t1,4) + 74*Power(t1,3)*t2 - 
                  31*Power(t1,2)*Power(t2,2) - 7*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               2*t1*(5*Power(t1,4) - 26*Power(t1,3)*t2 + 
                  55*Power(t1,2)*Power(t2,2) - 59*t1*Power(t2,3) + 
                  24*Power(t2,4))) + 
            Power(s1,4)*(8*Power(s2,4)*(3*t1 - 2*t2)*t2 + 
               Power(s2,3)*t2*
                (8*Power(t1,2) - 137*t1*t2 + 42*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-122*Power(t1,3) + 200*Power(t1,2)*t2 + 
                  25*t1*Power(t2,2) + 30*Power(t2,3)) + 
               t1*(8*Power(t1,5) - 85*Power(t1,4)*t2 + 
                  221*Power(t1,3)*Power(t2,2) - 
                  577*Power(t1,2)*Power(t2,3) + 640*t1*Power(t2,4) - 
                  203*Power(t2,5)) + 
               s2*(-8*Power(t1,5) + 158*Power(t1,4)*t2 - 
                  120*Power(t1,3)*Power(t2,2) + 
                  10*Power(t1,2)*Power(t2,3) + 85*t1*Power(t2,4) - 
                  28*Power(t2,5))) + 
            t1*Power(t2,2)*(Power(s2,4)*
                (4*Power(t1,3) - 26*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) + 16*Power(t2,3)) + 
               Power(s2,3)*(-12*Power(t1,4) + 20*Power(t1,3)*t2 + 
                  35*Power(t1,2)*Power(t2,2) + 41*t1*Power(t2,3) - 
                  86*Power(t2,4)) + 
               2*Power(t1 - t2,2)*t2*
                (Power(t1,4) + 13*Power(t1,3)*t2 + 
                  31*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) - 
                  8*Power(t2,4)) + 
               2*Power(s2,2)*t1*
                (6*Power(t1,4) + 21*Power(t1,3)*t2 - 
                  26*Power(t1,2)*Power(t2,2) - 36*t1*Power(t2,3) + 
                  35*Power(t2,4)) - 
               2*s2*(2*Power(t1,6) + 19*Power(t1,5)*t2 + 
                  8*Power(t1,4)*Power(t2,2) - 
                  28*Power(t1,3)*Power(t2,3) - 
                  57*Power(t1,2)*Power(t2,4) + 99*t1*Power(t2,5) - 
                  43*Power(t2,6))) + 
            Power(s1,2)*t2*(Power(s2,4)*
                (12*Power(t1,3) - 46*Power(t1,2)*t2 + 
                  117*t1*Power(t2,2) - 36*Power(t2,3)) + 
               Power(s2,3)*(-74*Power(t1,4) + 177*Power(t1,3)*t2 - 
                  18*Power(t1,2)*Power(t2,2) - 399*t1*Power(t2,3) + 
                  46*Power(t2,4)) + 
               Power(s2,2)*(122*Power(t1,5) - 141*Power(t1,4)*t2 - 
                  184*Power(t1,3)*Power(t2,2) + 
                  63*Power(t1,2)*Power(t2,3) + 228*t1*Power(t2,4) + 
                  54*Power(t2,5)) + 
               t1*(5*Power(t1,6) + 23*Power(t1,5)*t2 + 
                  108*Power(t1,4)*Power(t2,2) - 
                  206*Power(t1,3)*Power(t2,3) - 
                  564*Power(t1,2)*Power(t2,4) + 979*t1*Power(t2,5) - 
                  345*Power(t2,6)) - 
               s2*(65*Power(t1,6) + 20*Power(t1,5)*t2 + 
                  35*Power(t1,4)*Power(t2,2) - 
                  971*Power(t1,3)*Power(t2,3) + 
                  1204*Power(t1,2)*Power(t2,4) - 545*t1*Power(t2,5) + 
                  28*Power(t2,6))) + 
            Power(s1,3)*(2*Power(s2,4)*t2*
                (19*Power(t1,2) - 43*t1*t2 + 19*Power(t2,2)) + 
               Power(s2,3)*t2*
                (-111*Power(t1,3) + 24*Power(t1,2)*t2 + 
                  295*t1*Power(t2,2) - 66*Power(t2,3)) - 
               Power(s2,2)*(4*Power(t1,5) - 101*Power(t1,4)*t2 - 
                  174*Power(t1,3)*Power(t2,2) + 
                  175*Power(t1,2)*Power(t2,3) + 158*t1*Power(t2,4) + 
                  60*Power(t2,5)) + 
               s2*(6*Power(t1,6) - 21*Power(t1,5)*t2 - 
                  104*Power(t1,4)*Power(t2,2) - 
                  348*Power(t1,3)*Power(t2,3) + 
                  552*Power(t1,2)*Power(t2,4) - 342*t1*Power(t2,5) + 
                  44*Power(t2,6)) + 
               t1*(-2*Power(t1,6) - 6*Power(t1,5)*t2 + 
                  43*Power(t1,4)*Power(t2,2) - 
                  197*Power(t1,3)*Power(t2,3) + 
                  963*Power(t1,2)*Power(t2,4) - 1186*t1*Power(t2,5) + 
                  383*Power(t2,6))) + 
            s1*t2*(Power(s2,4)*
                (-4*Power(t1,4) + 18*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) - 71*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,3)*(12*Power(t1,5) + 34*Power(t1,4)*t2 - 
                  114*Power(t1,3)*Power(t2,2) - 
                  59*Power(t1,2)*Power(t2,3) + 295*t1*Power(t2,4) - 
                  12*Power(t2,5)) - 
               Power(s2,2)*(12*Power(t1,6) + 130*Power(t1,5)*t2 - 
                  114*Power(t1,4)*Power(t2,2) - 
                  205*Power(t1,3)*Power(t2,3) + 
                  91*Power(t1,2)*Power(t2,4) + 96*t1*Power(t2,5) + 
                  18*Power(t2,6)) + 
               t1*t2*(-4*Power(t1,6) - 43*Power(t1,5)*t2 - 
                  82*Power(t1,4)*Power(t2,2) + 
                  314*Power(t1,3)*Power(t2,3) + 
                  26*Power(t1,2)*Power(t2,4) - 347*t1*Power(t2,5) + 
                  136*Power(t2,6)) + 
               s2*(4*Power(t1,7) + 82*Power(t1,6)*t2 + 
                  51*Power(t1,5)*Power(t2,2) - 
                  63*Power(t1,4)*Power(t2,3) - 
                  675*Power(t1,3)*Power(t2,4) + 
                  883*Power(t1,2)*Power(t2,5) - 368*t1*Power(t2,6) + 
                  6*Power(t2,7)))) + 
         Power(s,4)*(-2*Power(s1,8)*
             (Power(s2,4)*t2 + 
               Power(s2,3)*(-3*Power(t1,2) + 5*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*t1*
                (11*Power(t1,2) - 20*t1*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(5*Power(t1,3) - 13*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - Power(t2,3)) - 
               s2*t1*(13*Power(t1,3) - 27*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + Power(t2,3))) + 
            t1*Power(t2,5)*(3*Power(s2,6)*t1 + 
               3*Power(s2,5)*
                (7*Power(t1,2) - 3*t1*t2 - 10*Power(t2,2)) + 
               2*Power(t1,2)*Power(t1 - t2,2)*t2*
                (7*Power(t1,2) + 13*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,4)*(-25*Power(t1,3) + 12*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) + 52*Power(t2,3)) + 
               Power(s2,3)*(-87*Power(t1,4) + 21*Power(t1,3)*t2 + 
                  124*Power(t1,2)*Power(t2,2) - 130*t1*Power(t2,3) + 
                  52*Power(t2,4)) + 
               2*Power(s2,2)*
                (75*Power(t1,5) - 10*Power(t1,4)*t2 - 
                  97*Power(t1,3)*Power(t2,2) + 
                  28*Power(t1,2)*Power(t2,3) + 19*t1*Power(t2,4) - 
                  15*Power(t2,5)) - 
               2*s2*t1*(31*Power(t1,5) + 9*Power(t1,4)*t2 - 
                  53*Power(t1,3)*Power(t2,2) - 
                  24*Power(t1,2)*Power(t2,3) + 40*t1*Power(t2,4) - 
                  3*Power(t2,5))) - 
            Power(s1,7)*(8*Power(s2,5)*t2 + 
               Power(s2,4)*(-10*Power(t1,2) + 17*t1*t2 - 
                  18*Power(t2,2)) + 
               Power(s2,3)*(46*Power(t1,3) - 45*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 18*Power(t2,3)) + 
               Power(s2,2)*(-72*Power(t1,4) + Power(t1,3)*t2 + 
                  173*Power(t1,2)*Power(t2,2) - 38*t1*Power(t2,3) + 
                  8*Power(t2,4)) + 
               s2*t1*(46*Power(t1,4) + 56*Power(t1,3)*t2 - 
                  324*Power(t1,2)*Power(t2,2) + 33*t1*Power(t2,3) + 
                  17*Power(t2,4)) - 
               t1*(10*Power(t1,5) + 37*Power(t1,4)*t2 - 
                  200*Power(t1,3)*Power(t2,2) + 
                  145*Power(t1,2)*Power(t2,3) - 48*t1*Power(t2,4) + 
                  Power(t2,5))) + 
            s1*Power(t2,4)*(Power(s2,6)*
                (5*Power(t1,2) - 19*t1*t2 + 6*Power(t2,2)) - 
               2*Power(s2,5)*
                (16*Power(t1,3) - 21*Power(t1,2)*t2 - 
                  70*t1*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,4)*(-139*Power(t1,4) + 52*Power(t1,3)*t2 + 
                  33*Power(t1,2)*Power(t2,2) - 262*t1*Power(t2,3) - 
                  12*Power(t2,4)) + 
               Power(s2,3)*(604*Power(t1,5) - 252*Power(t1,4)*t2 - 
                  623*Power(t1,3)*Power(t2,2) + 
                  500*Power(t1,2)*Power(t2,3) - 133*t1*Power(t2,4) + 
                  12*Power(t2,5)) + 
               Power(s2,2)*t1*
                (-688*Power(t1,5) + 163*Power(t1,4)*t2 + 
                  793*Power(t1,3)*Power(t2,2) + 
                  258*Power(t1,2)*Power(t2,3) - 663*t1*Power(t2,4) + 
                  295*Power(t2,5)) + 
               s2*t1*(250*Power(t1,6) + 94*Power(t1,5)*t2 - 
                  379*Power(t1,4)*Power(t2,2) - 
                  513*Power(t1,3)*Power(t2,3) + 
                  480*Power(t1,2)*Power(t2,4) + 57*t1*Power(t2,5) - 
                  37*Power(t2,6)) + 
               t1*t2*(-80*Power(t1,6) + 23*Power(t1,5)*t2 + 
                  134*Power(t1,4)*Power(t2,2) + 
                  22*Power(t1,3)*Power(t2,3) - 
                  92*Power(t1,2)*Power(t2,4) - 5*t1*Power(t2,5) - 
                  2*Power(t2,6))) + 
            Power(s1,6)*(-4*Power(s2,6)*t2 + 
               2*Power(s2,5)*
                (Power(t1,2) - 19*t1*t2 + 29*Power(t2,2)) + 
               Power(s2,4)*(-6*Power(t1,3) + 27*Power(t1,2)*t2 + 
                  71*t1*Power(t2,2) - 68*Power(t2,3)) + 
               Power(s2,3)*(-14*Power(t1,4) + 219*Power(t1,3)*t2 - 
                  308*Power(t1,2)*Power(t2,2) + 194*t1*Power(t2,3) - 
                  82*Power(t2,4)) + 
               Power(s2,2)*(58*Power(t1,5) - 489*Power(t1,4)*t2 + 
                  360*Power(t1,3)*Power(t2,2) + 
                  170*Power(t1,2)*Power(t2,3) - 66*t1*Power(t2,4) + 
                  46*Power(t2,5)) + 
               t1*(20*Power(t1,6) - 111*Power(t1,5)*t2 + 
                  Power(t1,4)*Power(t2,2) + 
                  430*Power(t1,3)*Power(t2,3) - 
                  462*Power(t1,2)*Power(t2,4) + 272*t1*Power(t2,5) - 
                  11*Power(t2,6)) - 
               s2*(60*Power(t1,6) - 396*Power(t1,5)*t2 + 
                  214*Power(t1,4)*Power(t2,2) + 
                  367*Power(t1,3)*Power(t2,3) + 
                  228*Power(t1,2)*Power(t2,4) - 84*t1*Power(t2,5) + 
                  2*Power(t2,6))) + 
            Power(s1,2)*Power(t2,3)*
             (Power(s2,6)*(-27*Power(t1,2) + 69*t1*t2 - 
                  28*Power(t2,2)) + 
               Power(s2,5)*(-35*Power(t1,3) + 38*Power(t1,2)*t2 - 
                  339*t1*Power(t2,2) + 102*Power(t2,3)) + 
               Power(s2,4)*(564*Power(t1,4) - 513*Power(t1,3)*t2 + 
                  84*Power(t1,2)*Power(t2,2) + 473*t1*Power(t2,3) + 
                  28*Power(t2,4)) + 
               Power(s2,3)*(-1325*Power(t1,5) + 877*Power(t1,4)*t2 + 
                  1178*Power(t1,3)*Power(t2,2) - 
                  784*Power(t1,2)*Power(t2,3) + 91*t1*Power(t2,4) - 
                  88*Power(t2,5)) + 
               t1*t2*(180*Power(t1,6) - 138*Power(t1,5)*t2 - 
                  69*Power(t1,4)*Power(t2,2) - 
                  433*Power(t1,3)*Power(t2,3) + 
                  402*Power(t1,2)*Power(t2,4) + 61*t1*Power(t2,5) - 
                  3*Power(t2,6)) + 
               Power(s2,2)*(1233*Power(t1,6) - 473*Power(t1,5)*t2 - 
                  1410*Power(t1,4)*Power(t2,2) - 
                  1272*Power(t1,3)*Power(t2,3) + 
                  2044*Power(t1,2)*Power(t2,4) - 822*t1*Power(t2,5) + 
                  14*Power(t2,6)) + 
               s2*t1*(-410*Power(t1,6) - 178*Power(t1,5)*t2 + 
                  704*Power(t1,4)*Power(t2,2) + 
                  1125*Power(t1,3)*Power(t2,3) - 
                  488*Power(t1,2)*Power(t2,4) - 752*t1*Power(t2,5) + 
                  239*Power(t2,6))) + 
            Power(s1,3)*Power(t2,2)*
             (Power(s2,6)*(27*Power(t1,2) - 93*t1*t2 + 
                  52*Power(t2,2)) + 
               Power(s2,5)*(62*Power(t1,3) - 218*Power(t1,2)*t2 + 
                  514*t1*Power(t2,2) - 232*Power(t2,3)) + 
               Power(s2,4)*(-561*Power(t1,4) + 1084*Power(t1,3)*t2 - 
                  362*Power(t1,2)*Power(t2,2) - 390*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,3)*(1151*Power(t1,5) - 1461*Power(t1,4)*t2 - 
                  1286*Power(t1,3)*Power(t2,2) + 
                  847*Power(t1,2)*Power(t2,3) - 135*t1*Power(t2,4) + 
                  228*Power(t2,5)) + 
               Power(s2,2)*(-1002*Power(t1,6) + 698*Power(t1,5)*t2 + 
                  1887*Power(t1,4)*Power(t2,2) + 
                  1273*Power(t1,3)*Power(t2,3) - 
                  2389*Power(t1,2)*Power(t2,4) + 1006*t1*Power(t2,5) - 
                  66*Power(t2,6)) + 
               t1*t2*(-232*Power(t1,6) + 442*Power(t1,5)*t2 - 
                  445*Power(t1,4)*Power(t2,2) + 
                  1087*Power(t1,3)*Power(t2,3) - 
                  604*Power(t1,2)*Power(t2,4) - 297*t1*Power(t2,5) + 
                  29*Power(t2,6)) + 
               s2*(323*Power(t1,7) + 222*Power(t1,6)*t2 - 
                  1338*Power(t1,5)*Power(t2,2) - 
                  326*Power(t1,4)*Power(t2,3) - 
                  884*Power(t1,3)*Power(t2,4) + 
                  2111*Power(t1,2)*Power(t2,5) - 529*t1*Power(t2,6) + 
                  2*Power(t2,7))) + 
            Power(s1,4)*t2*(Power(s2,6)*
                (-8*Power(t1,2) + 55*t1*t2 - 48*Power(t2,2)) + 
               Power(s2,5)*(-18*Power(t1,3) + 195*Power(t1,2)*t2 - 
                  459*t1*Power(t2,2) + 272*Power(t2,3)) + 
               Power(s2,4)*(171*Power(t1,4) - 872*Power(t1,3)*t2 + 
                  418*Power(t1,2)*Power(t2,2) + 187*t1*Power(t2,3) - 
                  98*Power(t2,4)) + 
               Power(s2,3)*(-363*Power(t1,5) + 1140*Power(t1,4)*t2 + 
                  1066*Power(t1,3)*Power(t2,2) - 
                  850*Power(t1,2)*Power(t2,3) + 426*t1*Power(t2,4) - 
                  292*Power(t2,5)) + 
               t1*t2*(188*Power(t1,6) - 664*Power(t1,5)*t2 + 
                  715*Power(t1,4)*Power(t2,2) - 
                  843*Power(t1,3)*Power(t2,3) + 
                  134*Power(t1,2)*Power(t2,4) + 613*t1*Power(t2,5) - 
                  50*Power(t2,6)) + 
               Power(s2,2)*(325*Power(t1,6) - 477*Power(t1,5)*t2 - 
                  2181*Power(t1,4)*Power(t2,2) + 
                  106*Power(t1,3)*Power(t2,3) + 
                  1080*Power(t1,2)*Power(t2,4) - 598*t1*Power(t2,5) + 
                  120*Power(t2,6)) - 
               s2*(107*Power(t1,7) + 229*Power(t1,6)*t2 - 
                  1926*Power(t1,5)*Power(t2,2) + 
                  1049*Power(t1,4)*Power(t2,3) - 
                  1735*Power(t1,3)*Power(t2,4) + 
                  2493*Power(t1,2)*Power(t2,5) - 545*t1*Power(t2,6) + 
                  6*Power(t2,7))) + 
            Power(s1,5)*(2*Power(s2,6)*t2*(-6*t1 + 11*t2) + 
               2*Power(s2,5)*
                (Power(t1,3) - 25*Power(t1,2)*t2 + 106*t1*Power(t2,2) - 
                  87*Power(t2,3)) + 
               Power(s2,4)*(-16*Power(t1,4) + 239*Power(t1,3)*t2 - 
                  207*Power(t1,2)*Power(t2,2) - 114*t1*Power(t2,3) + 
                  122*Power(t2,4)) + 
               Power(s2,3)*(38*Power(t1,5) - 295*Power(t1,4)*t2 - 
                  626*Power(t1,3)*Power(t2,2) + 
                  674*Power(t1,2)*Power(t2,3) - 482*t1*Power(t2,4) + 
                  206*Power(t2,5)) + 
               Power(s2,2)*(-36*Power(t1,6) + 29*Power(t1,5)*t2 + 
                  1522*Power(t1,4)*Power(t2,2) - 
                  756*Power(t1,3)*Power(t2,3) - 
                  145*Power(t1,2)*Power(t2,4) + 187*t1*Power(t2,5) - 
                  106*Power(t2,6)) + 
               t1*t2*(-92*Power(t1,6) + 439*Power(t1,5)*t2 - 
                  332*Power(t1,4)*Power(t2,2) - 
                  97*Power(t1,3)*Power(t2,3) + 
                  479*Power(t1,2)*Power(t2,4) - 598*t1*Power(t2,5) + 
                  36*Power(t2,6)) + 
               s2*(12*Power(t1,7) + 181*Power(t1,6)*t2 - 
                  1372*Power(t1,5)*Power(t2,2) + 
                  955*Power(t1,4)*Power(t2,3) - 
                  670*Power(t1,3)*Power(t2,4) + 
                  1322*Power(t1,2)*Power(t2,5) - 287*t1*Power(t2,6) + 
                  6*Power(t2,7)))) + 
         Power(s,5)*(Power(s1,8)*(s2 - t1)*t1*t2*(s2 - t1 + t2) + 
            Power(s1,7)*(6*Power(s2,4)*t2 + 
               Power(s2,3)*(-6*Power(t1,2) + 21*t1*t2 + 
                  2*Power(t2,2)) + 
               Power(s2,2)*(30*Power(t1,3) - 76*Power(t1,2)*t2 + 
                  25*t1*Power(t2,2) - 4*Power(t2,3)) - 
               s2*t1*(44*Power(t1,3) - 111*Power(t1,2)*t2 + 
                  26*t1*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(20*Power(t1,4) - 66*Power(t1,3)*t2 + 
                  53*Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,6)*(10*Power(s2,5)*t2 + 
               Power(s2,4)*(-4*Power(t1,2) + 62*t1*t2 - 
                  46*Power(t2,2)) + 
               2*Power(s2,3)*
                (14*Power(t1,3) - 60*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) - 7*Power(t2,3)) + 
               t1*t2*(-101*Power(t1,4) + 330*Power(t1,3)*t2 - 
                  421*Power(t1,2)*Power(t2,2) + 273*t1*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(s2,2)*(-48*Power(t1,4) + 13*Power(t1,3)*t2 + 
                  230*Power(t1,2)*Power(t2,2) - 125*t1*Power(t2,3) + 
                  38*Power(t2,4)) + 
               s2*(24*Power(t1,5) + 131*Power(t1,4)*t2 - 
                  373*Power(t1,3)*Power(t2,2) + 
                  88*Power(t1,2)*Power(t2,3) + 31*t1*Power(t2,4) - 
                  4*Power(t2,5))) + 
            t1*Power(t2,4)*(Power(s2,6)*t1 + 
               Power(s2,5)*(25*Power(t1,2) - 17*t1*t2 - 
                  18*Power(t2,2)) + 
               2*Power(t1,2)*Power(t1 - t2,2)*t2*
                (9*Power(t1,2) + 29*t1*t2 + 20*Power(t2,2)) - 
               Power(s2,4)*(35*Power(t1,3) + 8*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) - 96*Power(t2,3)) + 
               Power(s2,3)*t1*
                (-59*Power(t1,3) + 33*Power(t1,2)*t2 + 
                  93*t1*Power(t2,2) - 87*Power(t2,3)) + 
               2*Power(s2,2)*
                (59*Power(t1,5) + 28*Power(t1,4)*t2 - 
                  68*Power(t1,3)*Power(t2,2) - 
                  74*Power(t1,2)*Power(t2,3) + 103*t1*Power(t2,4) - 
                  48*Power(t2,5)) - 
               2*s2*(25*Power(t1,6) + 41*Power(t1,5)*t2 - 
                  31*Power(t1,4)*Power(t2,2) - 
                  106*Power(t1,3)*Power(t2,3) + 
                  65*Power(t1,2)*Power(t2,4) + 15*t1*Power(t2,5) - 
                  9*Power(t2,6))) + 
            s1*Power(t2,3)*(Power(s2,6)*
                (3*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,5)*(-25*Power(t1,3) + 15*Power(t1,2)*t2 + 
                  114*t1*Power(t2,2) - 22*Power(t2,3)) + 
               Power(s2,4)*(-83*Power(t1,4) + 118*Power(t1,3)*t2 - 
                  8*Power(t1,2)*Power(t2,2) - 380*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,3)*(387*Power(t1,5) - 133*Power(t1,4)*t2 - 
                  380*Power(t1,3)*Power(t2,2) + 
                  168*Power(t1,2)*Power(t2,3) + 152*t1*Power(t2,4) + 
                  28*Power(t2,5)) + 
               s2*t1*(158*Power(t1,6) + 312*Power(t1,5)*t2 - 
                  201*Power(t1,4)*Power(t2,2) - 
                  945*Power(t1,3)*Power(t2,3) + 
                  230*Power(t1,2)*Power(t2,4) + 563*t1*Power(t2,5) - 
                  233*Power(t2,6)) + 
               Power(s2,2)*(-440*Power(t1,6) - 235*Power(t1,5)*t2 + 
                  559*Power(t1,4)*Power(t2,2) + 
                  1058*Power(t1,3)*Power(t2,3) - 
                  1199*Power(t1,2)*Power(t2,4) + 481*t1*Power(t2,5) - 
                  8*Power(t2,6)) + 
               t1*t2*(-70*Power(t1,6) - 81*Power(t1,5)*t2 + 
                  174*Power(t1,4)*Power(t2,2) + 
                  281*Power(t1,3)*Power(t2,3) - 
                  285*Power(t1,2)*Power(t2,4) - 28*t1*Power(t2,5) + 
                  9*Power(t2,6))) + 
            Power(s1,5)*(2*Power(s2,6)*t2 + 
               2*Power(s2,5)*(26*t1 - 29*t2)*t2 + 
               2*Power(s2,4)*t2*
                (5*Power(t1,2) - 139*t1*t2 + 74*Power(t2,2)) + 
               Power(s2,3)*(18*Power(t1,4) - 339*Power(t1,3)*t2 + 
                  601*Power(t1,2)*Power(t2,2) - 169*t1*Power(t2,3) + 
                  62*Power(t2,4)) - 
               Power(s2,2)*(58*Power(t1,5) - 592*Power(t1,4)*t2 + 
                  325*Power(t1,3)*Power(t2,2) + 
                  101*Power(t1,2)*Power(t2,3) - 254*t1*Power(t2,4) + 
                  128*Power(t2,5)) + 
               s2*(60*Power(t1,6) - 397*Power(t1,5)*t2 + 
                  Power(t1,4)*Power(t2,2) - 
                  163*Power(t1,3)*Power(t2,3) + 
                  529*Power(t1,2)*Power(t2,4) - 237*t1*Power(t2,5) + 
                  18*Power(t2,6)) + 
               t1*(-20*Power(t1,6) + 79*Power(t1,5)*t2 + 
                  158*Power(t1,4)*Power(t2,2) - 
                  477*Power(t1,3)*Power(t2,3) + 
                  996*Power(t1,2)*Power(t2,4) - 938*t1*Power(t2,5) + 
                  104*Power(t2,6))) + 
            Power(s1,2)*Power(t2,2)*
             (Power(s2,6)*(-9*Power(t1,2) + 21*t1*t2 - 8*Power(t2,2)) + 
               Power(s2,5)*(-15*Power(t1,3) + 84*Power(t1,2)*t2 - 
                  285*t1*Power(t2,2) + 94*Power(t2,3)) + 
               Power(s2,4)*(248*Power(t1,4) - 371*Power(t1,3)*t2 + 
                  145*Power(t1,2)*Power(t2,2) + 657*t1*Power(t2,3) - 
                  84*Power(t2,4)) - 
               Power(s2,3)*(602*Power(t1,5) - 240*Power(t1,4)*t2 - 
                  789*Power(t1,3)*Power(t2,2) + 
                  313*Power(t1,2)*Power(t2,3) + 385*t1*Power(t2,4) + 
                  116*Power(t2,5)) + 
               t1*t2*(120*Power(t1,6) + 52*Power(t1,5)*t2 + 
                  3*Power(t1,4)*Power(t2,2) - 
                  1001*Power(t1,3)*Power(t2,3) + 
                  576*Power(t1,2)*Power(t2,4) + 325*t1*Power(t2,5) - 
                  75*Power(t2,6)) + 
               Power(s2,2)*(565*Power(t1,6) + 406*Power(t1,5)*t2 - 
                  1145*Power(t1,4)*Power(t2,2) - 
                  1864*Power(t1,3)*Power(t2,3) + 
                  2144*Power(t1,2)*Power(t2,4) - 871*t1*Power(t2,5) + 
                  66*Power(t2,6)) + 
               s2*(-187*Power(t1,7) - 500*Power(t1,6)*t2 + 
                  480*Power(t1,5)*Power(t2,2) + 
                  1082*Power(t1,4)*Power(t2,3) + 
                  977*Power(t1,3)*Power(t2,4) - 
                  2186*Power(t1,2)*Power(t2,5) + 772*t1*Power(t2,6) - 
                  4*Power(t2,7))) + 
            Power(s1,3)*t2*(Power(s2,6)*
                (5*Power(t1,2) - 21*t1*t2 + 12*Power(t2,2)) + 
               Power(s2,5)*(15*Power(t1,3) - 139*Power(t1,2)*t2 + 
                  352*t1*Power(t2,2) - 160*Power(t2,3)) + 
               Power(s2,4)*(-132*Power(t1,4) + 443*Power(t1,3)*t2 - 
                  124*Power(t1,2)*Power(t2,2) - 711*t1*Power(t2,3) + 
                  206*Power(t2,4)) + 
               Power(s2,3)*(283*Power(t1,5) - 275*Power(t1,4)*t2 - 
                  1227*Power(t1,3)*Power(t2,2) + 
                  828*Power(t1,2)*Power(t2,3) + 193*t1*Power(t2,4) + 
                  188*Power(t2,5)) + 
               Power(s2,2)*(-255*Power(t1,6) - 327*Power(t1,5)*t2 + 
                  1854*Power(t1,4)*Power(t2,2) + 
                  798*Power(t1,3)*Power(t2,3) - 
                  1421*Power(t1,2)*Power(t2,4) + 767*t1*Power(t2,5) - 
                  172*Power(t2,6)) + 
               t1*t2*(-123*Power(t1,6) + 163*Power(t1,5)*t2 - 
                  344*Power(t1,4)*Power(t2,2) + 
                  1216*Power(t1,3)*Power(t2,3) - 
                  89*Power(t1,2)*Power(t2,4) - 1028*t1*Power(t2,5) + 
                  185*Power(t2,6)) + 
               s2*(84*Power(t1,7) + 442*Power(t1,6)*t2 - 
                  1079*Power(t1,5)*Power(t2,2) + 
                  128*Power(t1,4)*Power(t2,3) - 
                  2643*Power(t1,3)*Power(t2,4) + 
                  3383*Power(t1,2)*Power(t2,5) - 1095*t1*Power(t2,6) + 
                  18*Power(t2,7))) + 
            Power(s1,4)*(Power(s2,6)*(7*t1 - 8*t2)*t2 + 
               Power(s2,5)*t2*
                (57*Power(t1,2) - 215*t1*t2 + 136*Power(t2,2)) + 
               2*Power(s2,4)*
                (2*Power(t1,4) - 93*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) + 277*t1*Power(t2,3) - 
                  121*Power(t2,4)) + 
               Power(s2,3)*(-16*Power(t1,5) + 141*Power(t1,4)*t2 + 
                  1049*Power(t1,3)*Power(t2,2) - 
                  1067*Power(t1,2)*Power(t2,3) + 193*t1*Power(t2,4) - 
                  150*Power(t2,5)) + 
               t1*t2*(73*Power(t1,6) - 239*Power(t1,5)*t2 + 
                  144*Power(t1,4)*Power(t2,2) - 
                  253*Power(t1,3)*Power(t2,3) - 
                  869*Power(t1,2)*Power(t2,4) + 1418*t1*Power(t2,5) - 
                  201*Power(t2,6)) + 
               Power(s2,2)*(20*Power(t1,6) + 119*Power(t1,5)*t2 - 
                  1698*Power(t1,4)*Power(t2,2) + 
                  431*Power(t1,3)*Power(t2,3) + 
                  223*Power(t1,2)*Power(t2,4) - 437*t1*Power(t2,5) + 
                  208*Power(t2,6)) - 
               s2*(8*Power(t1,7) + 211*Power(t1,6)*t2 - 
                  1126*Power(t1,5)*Power(t2,2) + 
                  564*Power(t1,4)*Power(t2,3) - 
                  1978*Power(t1,3)*Power(t2,4) + 
                  2320*Power(t1,2)*Power(t2,5) - 747*t1*Power(t2,6) + 
                  28*Power(t2,7)))) + 
         Power(s,6)*(Power(s1,7)*t1*t2*
             (-4*Power(s2,2) + 9*s2*t1 - 5*Power(t1,2) - 3*s2*t2 + 
               4*t1*t2 + Power(t2,2)) + 
            Power(s1,6)*(-6*Power(s2,4)*t2 + 
               2*Power(s2,3)*(Power(t1,2) - 19*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(-18*Power(t1,3) + 102*Power(t1,2)*t2 - 
                  31*t1*Power(t2,2) + 10*Power(t2,3)) + 
               t1*(-20*Power(t1,4) + 83*Power(t1,3)*t2 - 
                  110*Power(t1,2)*Power(t2,2) + 72*t1*Power(t2,3) - 
                  17*Power(t2,4)) + 
               s2*(36*Power(t1,4) - 127*Power(t1,3)*t2 + 
                  46*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) - 
                  2*Power(t2,4))) + 
            Power(s1,5)*(-4*Power(s2,5)*t2 + 
               23*Power(s2,4)*t2*(-3*t1 + 2*t2) + 
               Power(s2,3)*(-6*Power(t1,3) + 92*Power(t1,2)*t2 + 
                  114*t1*Power(t2,2) - 26*Power(t2,3)) + 
               2*Power(s2,2)*
                (6*Power(t1,4) + 27*Power(t1,3)*t2 - 
                  143*Power(t1,2)*Power(t2,2) + 70*t1*Power(t2,3) - 
                  29*Power(t2,4)) + 
               s2*(4*Power(t1,5) - 187*Power(t1,4)*t2 + 
                  252*Power(t1,3)*Power(t2,2) - 
                  102*Power(t1,2)*Power(t2,3) - 81*t1*Power(t2,4) + 
                  18*Power(t2,5)) + 
               t1*(-10*Power(t1,5) + 129*Power(t1,4)*t2 - 
                  331*Power(t1,3)*Power(t2,2) + 
                  658*Power(t1,2)*Power(t2,3) - 582*t1*Power(t2,4) + 
                  110*Power(t2,5))) + 
            t1*Power(t2,3)*(Power(s2,5)*
                (9*Power(t1,2) - 7*t1*t2 - 4*Power(t2,2)) + 
               Power(s2,4)*(-6*Power(t1,3) - 52*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + 66*Power(t2,3)) + 
               Power(s2,3)*(-37*Power(t1,4) + 75*Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) + 33*t1*Power(t2,3) - 
                  92*Power(t2,4)) + 
               2*Power(t1 - t2,2)*t2*
                (5*Power(t1,4) + 29*Power(t1,3)*t2 + 
                  37*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,2)*(56*Power(t1,5) + 52*Power(t1,4)*t2 - 
                  38*Power(t1,3)*Power(t2,2) - 
                  212*Power(t1,2)*Power(t2,3) + 234*t1*Power(t2,4) - 
                  92*Power(t2,5)) - 
               2*s2*(11*Power(t1,6) + 39*Power(t1,5)*t2 + 
                  7*Power(t1,4)*Power(t2,2) - 
                  98*Power(t1,3)*Power(t2,3) + Power(t1,2)*Power(t2,4) + 
                  73*t1*Power(t2,5) - 33*Power(t2,6))) - 
            Power(s1,4)*(2*Power(s2,5)*(11*t1 - 10*t2)*t2 + 
               Power(s2,4)*t2*
                (71*Power(t1,2) - 277*t1*t2 + 128*Power(t2,2)) + 
               Power(s2,3)*(6*Power(t1,4) - 322*Power(t1,3)*t2 + 
                  369*Power(t1,2)*Power(t2,2) + 215*t1*Power(t2,3) - 
                  38*Power(t2,4)) + 
               Power(s2,2)*(-26*Power(t1,5) + 424*Power(t1,4)*t2 + 
                  39*Power(t1,3)*Power(t2,2) - 
                  300*Power(t1,2)*Power(t2,3) + 174*t1*Power(t2,4) - 
                  142*Power(t2,5)) + 
               s2*(30*Power(t1,6) - 215*Power(t1,5)*t2 - 
                  160*Power(t1,4)*Power(t2,2) - 
                  467*Power(t1,3)*Power(t2,3) + 
                  700*Power(t1,2)*Power(t2,4) - 437*t1*Power(t2,5) + 
                  44*Power(t2,6)) + 
               t1*(-10*Power(t1,6) + 19*Power(t1,5)*t2 + 
                  157*Power(t1,4)*Power(t2,2) - 
                  360*Power(t1,3)*Power(t2,3) + 
                  1301*Power(t1,2)*Power(t2,4) - 1436*t1*Power(t2,5) + 
                  301*Power(t2,6))) + 
            Power(s1,2)*t2*(Power(s2,5)*
                (-7*Power(t1,3) + 37*Power(t1,2)*t2 - 
                  80*t1*Power(t2,2) + 28*Power(t2,3)) + 
               2*Power(s2,4)*
                (24*Power(t1,4) - 45*Power(t1,3)*t2 - 
                  21*Power(t1,2)*Power(t2,2) + 235*t1*Power(t2,3) - 
                  55*Power(t2,4)) - 
               Power(s2,3)*(106*Power(t1,5) + 131*Power(t1,4)*t2 - 
                  553*Power(t1,3)*Power(t2,2) + 
                  179*Power(t1,2)*Power(t2,3) + 630*t1*Power(t2,4) + 
                  20*Power(t2,5)) + 
               t1*t2*(39*Power(t1,6) + 77*Power(t1,5)*t2 + 
                  182*Power(t1,4)*Power(t2,2) - 
                  891*Power(t1,3)*Power(t2,3) + 
                  10*Power(t1,2)*Power(t2,4) + 834*t1*Power(t2,5) - 
                  251*Power(t2,6)) + 
               Power(s2,2)*(96*Power(t1,6) + 464*Power(t1,5)*t2 - 
                  674*Power(t1,4)*Power(t2,2) - 
                  795*Power(t1,3)*Power(t2,3) + 
                  832*Power(t1,2)*Power(t2,4) - 247*t1*Power(t2,5) + 
                  100*Power(t2,6)) + 
               s2*(-31*Power(t1,7) - 319*Power(t1,6)*t2 + 
                  161*Power(t1,5)*Power(t2,2) + 
                  198*Power(t1,4)*Power(t2,3) + 
                  1926*Power(t1,3)*Power(t2,4) - 
                  2499*Power(t1,2)*Power(t2,5) + 984*t1*Power(t2,6) - 
                  18*Power(t2,7))) - 
            s1*Power(t2,2)*(Power(s2,5)*
                (2*Power(t1,3) + 5*Power(t1,2)*t2 - 35*t1*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,4)*(38*Power(t1,4) - 106*Power(t1,3)*t2 + 
                  19*Power(t1,2)*Power(t2,2) + 265*t1*Power(t2,3) - 
                  28*Power(t2,4)) + 
               Power(s2,3)*(-128*Power(t1,5) + 28*Power(t1,4)*t2 + 
                  165*Power(t1,3)*Power(t2,2) + 
                  86*Power(t1,2)*Power(t2,3) - 403*t1*Power(t2,4) - 
                  12*Power(t2,5)) + 
               t1*t2*(28*Power(t1,6) + 107*Power(t1,5)*t2 - 
                  18*Power(t1,4)*Power(t2,2) - 
                  494*Power(t1,3)*Power(t2,3) + 
                  268*Power(t1,2)*Power(t2,4) + 175*t1*Power(t2,5) - 
                  66*Power(t2,6)) + 
               Power(s2,2)*(134*Power(t1,6) + 297*Power(t1,5)*t2 - 
                  254*Power(t1,4)*Power(t2,2) - 
                  834*Power(t1,3)*Power(t2,3) + 
                  784*Power(t1,2)*Power(t2,4) - 263*t1*Power(t2,5) + 
                  22*Power(t2,6)) - 
               s2*(46*Power(t1,7) + 252*Power(t1,6)*t2 + 
                  19*Power(t1,5)*Power(t2,2) - 
                  553*Power(t1,4)*Power(t2,3) - 
                  565*Power(t1,3)*Power(t2,4) + 
                  1103*Power(t1,2)*Power(t2,5) - 444*t1*Power(t2,6) + 
                  2*Power(t2,7))) + 
            Power(s1,3)*(Power(s2,5)*t2*
                (-25*Power(t1,2) + 71*t1*t2 - 36*Power(t2,2)) + 
               Power(s2,4)*t2*
                (40*Power(t1,3) + 133*Power(t1,2)*t2 - 
                  479*t1*Power(t2,2) + 170*Power(t2,3)) + 
               Power(s2,3)*(2*Power(t1,5) + 74*Power(t1,4)*t2 - 
                  714*Power(t1,3)*Power(t2,2) + 
                  511*Power(t1,2)*Power(t2,3) + 457*t1*Power(t2,4) - 
                  10*Power(t2,5)) + 
               Power(s2,2)*(-4*Power(t1,6) - 224*Power(t1,5)*t2 + 
                  854*Power(t1,4)*Power(t2,2) + 
                  163*Power(t1,3)*Power(t2,3) - 
                  398*Power(t1,2)*Power(t2,4) + 146*t1*Power(t2,5) - 
                  172*Power(t2,6)) + 
               t1*t2*(-30*Power(t1,6) + 22*Power(t1,5)*t2 - 
                  127*Power(t1,4)*Power(t2,2) + 
                  372*Power(t1,3)*Power(t2,3) + 
                  957*Power(t1,2)*Power(t2,4) - 1600*t1*Power(t2,5) + 
                  396*Power(t2,6)) + 
               s2*(2*Power(t1,7) + 165*Power(t1,6)*t2 - 
                  372*Power(t1,5)*Power(t2,2) + 
                  164*Power(t1,4)*Power(t2,3) - 
                  1952*Power(t1,3)*Power(t2,4) + 
                  2283*Power(t1,2)*Power(t2,5) - 967*t1*Power(t2,6) + 
                  44*Power(t2,7)))))*Log(-s1))/
     (s*(s - s1)*s1*s2*Power(t1,2)*Power(s - s2 + t1,2)*Power(s1 - t2,2)*
       (s1 + t1 - t2)*Power(t2,2)*Power(s + t2,2)*Power(s - s1 + t2,2)*
       (s2 - t1 + t2)) + (8*(-(s1*Power(s2,3)*(s2 - t1)*t1*(s1 - t2)*
            (s1 + t1 - t2)*t2*(s2 - t1 + t2)*
            Power(s1*(s2 - t1) - s2*t2,2)*
            (s1*(Power(s2,2) - 3*s2*t1 + 2*Power(t1,2)) + 
              s2*(2*s2*t1 - 2*Power(t1,2) - s2*t2 + 2*t1*t2))) + 
         Power(s,10)*t1*(s1 + t1 - t2)*t2*
          (4*s2*Power(t1,2)*(2*t1 - t2) + 4*Power(t1,3)*(-t1 + t2) + 
            Power(s2,3)*(t1 + t2) + 
            Power(s2,2)*(-4*Power(t1,2) - t1*t2 + Power(t2,2))) + 
         s*t1*(Power(s1,6)*s2*Power(s2 - t1,3)*
             (5*Power(s2,4)*t2 + Power(t1,3)*t2*(-2*t1 + 3*t2) + 
               Power(s2,2)*t1*
                (-4*Power(t1,2) + 11*t1*t2 - 4*Power(t2,2)) + 
               Power(s2,3)*(2*Power(t1,2) - 9*t1*t2 + 5*Power(t2,2)) + 
               s2*Power(t1,2)*(2*Power(t1,2) - 5*t1*t2 + 5*Power(t2,2))) \
+ Power(s2,2)*(s2 - t1)*Power(t2,4)*
             (-2*s2*Power(t1,4)*(8*t1 - 5*t2)*Power(t1 - t2,2) + 
               4*Power(t1,5)*Power(t1 - t2,3) + 
               Power(s2,6)*(4*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*Power(t1,3)*
                (49*Power(t1,3) - 118*Power(t1,2)*t2 + 
                  83*t1*Power(t2,2) - 16*Power(t2,3)) + 
               Power(s2,4)*t1*
                (83*Power(t1,3) - 150*Power(t1,2)*t2 + 
                  68*t1*Power(t2,2) - 10*Power(t2,3)) + 
               Power(s2,5)*(-33*Power(t1,3) + 56*Power(t1,2)*t2 - 
                  21*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,3)*Power(t1,2)*
                (-91*Power(t1,3) + 190*Power(t1,2)*t2 - 
                  106*t1*Power(t2,2) + 14*Power(t2,3))) + 
            Power(s1,5)*Power(s2 - t1,2)*
             (3*Power(s2,7)*t2 + 4*Power(t1,5)*Power(t2,2)*(-t1 + t2) + 
               Power(s2,6)*(2*Power(t1,2) + 9*t1*t2 - 19*Power(t2,2)) + 
               2*s2*Power(t1,4)*t2*
                (5*Power(t1,2) - 6*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,4)*t1*t2*
                (20*Power(t1,2) - 31*t1*t2 + 29*Power(t2,2)) + 
               Power(s2,3)*Power(t1,2)*
                (4*Power(t1,3) - 12*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 11*Power(t2,3)) - 
               Power(s2,2)*Power(t1,3)*
                (2*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  18*t1*Power(t2,2) + 19*Power(t2,3)) - 
               Power(s2,5)*(4*Power(t1,3) + 23*Power(t1,2)*t2 - 
                  63*t1*Power(t2,2) + 22*Power(t2,3))) - 
            s1*s2*Power(t2,3)*
             (-4*Power(t1,6)*Power(t1 - t2,2)*
                (Power(t1,2) - t1*t2 + 3*Power(t2,2)) + 
               Power(s2,8)*(4*Power(t1,2) - 17*t1*t2 + 9*Power(t2,2)) + 
               Power(s2,7)*(-51*Power(t1,3) + 154*Power(t1,2)*t2 - 
                  75*t1*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,4)*Power(t1,2)*
                (64*Power(t1,4) - 377*Power(t1,3)*t2 + 
                  147*Power(t1,2)*Power(t2,2) + 130*t1*Power(t2,3) - 
                  46*Power(t2,4)) + 
               Power(s2,6)*(152*Power(t1,4) - 464*Power(t1,3)*t2 + 
                  245*Power(t1,2)*Power(t2,2) - 44*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,5)*t1*
                (-176*Power(t1,4) + 628*Power(t1,3)*t2 - 
                  337*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) + 
                  7*Power(t2,4)) + 
               2*s2*Power(t1,5)*
                (7*Power(t1,4) - 15*Power(t1,3)*t2 + 
                  36*Power(t1,2)*Power(t2,2) - 49*t1*Power(t2,3) + 
                  21*Power(t2,4)) - 
               2*Power(s2,2)*Power(t1,4)*
                (16*Power(t1,4) - 21*Power(t1,3)*t2 + 
                  64*Power(t1,2)*Power(t2,2) - 93*t1*Power(t2,3) + 
                  37*Power(t2,4)) + 
               Power(s2,3)*Power(t1,3)*
                (29*Power(t1,4) + 52*Power(t1,3)*t2 + 
                  91*Power(t1,2)*Power(t2,2) - 222*t1*Power(t2,3) + 
                  80*Power(t2,4))) + 
            Power(s1,4)*(s2 - t1)*
             (Power(s2,8)*(7*t1 - 9*t2)*t2 + 
               4*Power(t1,6)*Power(t2,2)*
                (Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*Power(t1,4)*t2*
                (17*Power(t1,3) - 74*Power(t1,2)*t2 + 
                  128*t1*Power(t2,2) - 97*Power(t2,3)) + 
               s2*Power(t1,5)*t2*
                (-10*Power(t1,3) + 26*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,7)*(2*Power(t1,3) - 12*Power(t1,2)*t2 - 
                  21*t1*Power(t2,2) + 29*Power(t2,3)) + 
               Power(s2,4)*Power(t1,2)*
                (-8*Power(t1,4) + 10*Power(t1,3)*t2 - 
                  59*Power(t1,2)*Power(t2,2) + 234*t1*Power(t2,3) - 
                  78*Power(t2,4)) + 
               Power(s2,5)*t1*
                (12*Power(t1,4) - 15*Power(t1,3)*t2 - 
                  79*Power(t1,2)*Power(t2,2) + 62*t1*Power(t2,3) - 
                  60*Power(t2,4)) + 
               Power(s2,6)*(-8*Power(t1,4) + 10*Power(t1,3)*t2 + 
                  112*Power(t1,2)*Power(t2,2) - 135*t1*Power(t2,3) + 
                  38*Power(t2,4)) + 
               Power(s2,3)*Power(t1,3)*
                (2*Power(t1,4) - 7*Power(t1,3)*t2 + 
                  100*Power(t1,2)*Power(t2,2) - 290*t1*Power(t2,3) + 
                  173*Power(t2,4))) - 
            Power(s1,2)*Power(t2,2)*
             (4*Power(t1,7)*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s2,9)*(4*Power(t1,2) + 3*t1*t2 - 6*Power(t2,2)) + 
               Power(s2,8)*(Power(t1,3) - 101*Power(t1,2)*t2 + 
                  84*t1*Power(t2,2) - 19*Power(t2,3)) + 
               Power(s2,4)*Power(t1,3)*
                (-43*Power(t1,4) + 42*Power(t1,3)*t2 - 
                  501*Power(t1,2)*Power(t2,2) + 809*t1*Power(t2,3) - 
                  300*Power(t2,4)) + 
               Power(s2,2)*Power(t1,5)*
                (-17*Power(t1,4) + 58*Power(t1,3)*t2 - 
                  163*Power(t1,2)*Power(t2,2) + 260*t1*Power(t2,3) - 
                  132*Power(t2,4)) + 
               Power(s2,7)*(-26*Power(t1,4) + 351*Power(t1,3)*t2 - 
                  286*Power(t1,2)*Power(t2,2) + 83*t1*Power(t2,3) - 
                  13*Power(t2,4)) + 
               Power(s2,6)*t1*
                (19*Power(t1,4) - 441*Power(t1,3)*t2 + 
                  271*Power(t1,2)*Power(t2,2) + 48*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               2*s2*Power(t1,6)*
                (3*Power(t1,4) - 12*Power(t1,3)*t2 + 
                  20*Power(t1,2)*Power(t2,2) - 24*t1*Power(t2,3) + 
                  13*Power(t2,4)) + 
               Power(s2,5)*Power(t1,2)*
                (28*Power(t1,4) + 187*Power(t1,3)*t2 + 
                  185*Power(t1,2)*Power(t2,2) - 519*t1*Power(t2,3) + 
                  152*Power(t2,4)) + 
               Power(s2,3)*Power(t1,4)*
                (28*Power(t1,4) - 75*Power(t1,3)*t2 + 
                  372*Power(t1,2)*Power(t2,2) - 607*t1*Power(t2,3) + 
                  270*Power(t2,4))) + 
            Power(s1,3)*t2*(4*Power(t1,7)*Power(t2,2)*
                (2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,9)*(4*Power(t1,2) - 13*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,8)*(-15*Power(t1,3) + 6*Power(t1,2)*t2 + 
                  42*t1*Power(t2,2) - 26*Power(t2,3)) + 
               Power(s2,4)*Power(t1,3)*
                (31*Power(t1,4) - 24*Power(t1,3)*t2 - 
                  470*Power(t1,2)*Power(t2,2) + 1013*t1*Power(t2,3) - 
                  430*Power(t2,4)) + 
               Power(s2,2)*Power(t1,5)*
                (Power(t1,4) + 47*Power(t1,3)*t2 - 
                  152*Power(t1,2)*Power(t2,2) + 261*t1*Power(t2,3) - 
                  165*Power(t2,4)) + 
               Power(s2,7)*(24*Power(t1,4) + 79*Power(t1,3)*t2 - 
                  227*Power(t1,2)*Power(t2,2) + 139*t1*Power(t2,3) - 
                  32*Power(t2,4)) + 
               2*s2*Power(t1,6)*
                (Power(t1,4) - 11*Power(t1,3)*t2 + 
                  14*Power(t1,2)*Power(t2,2) - 7*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,6)*t1*
                (-17*Power(t1,4) - 133*Power(t1,3)*t2 + 
                  234*Power(t1,2)*Power(t2,2) - 40*t1*Power(t2,3) + 
                  46*Power(t2,4)) + 
               Power(s2,5)*Power(t1,2)*
                (-10*Power(t1,4) + 79*Power(t1,3)*t2 + 
                  179*Power(t1,2)*Power(t2,2) - 571*t1*Power(t2,3) + 
                  161*Power(t2,4)) + 
               Power(s2,3)*Power(t1,4)*
                (-20*Power(t1,4) - 19*Power(t1,3)*t2 + 
                  352*Power(t1,2)*Power(t2,2) - 742*t1*Power(t2,3) + 
                  397*Power(t2,4)))) + 
         Power(s,9)*t1*(-((t1 - t2)*t2*
               (Power(s2,4)*(4*t1 + 5*t2) + 
                 s2*Power(t1,2)*
                  (-55*Power(t1,2) + 60*t1*t2 - 17*Power(t2,2)) + 
                 Power(s2,3)*
                  (-28*Power(t1,2) + 7*t1*t2 + 2*Power(t2,2)) + 
                 Power(s2,2)*
                  (62*Power(t1,3) - 55*Power(t1,2)*t2 + 
                    18*t1*Power(t2,2) - 3*Power(t2,3)) + 
                 2*Power(t1,2)*
                  (8*Power(t1,3) - 9*Power(t1,2)*t2 + Power(t2,3)))) + 
            Power(s1,2)*(Power(s2,4)*t2 + 
               Power(s2,3)*(2*Power(t1,2) - 9*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(-4*Power(t1,3) + 28*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 4*Power(t2,3)) + 
               4*t1*t2*(4*Power(t1,3) - 3*Power(t1,2)*t2 - 
                  2*t1*Power(t2,2) + Power(t2,3)) + 
               2*s2*(Power(t1,4) - 19*Power(t1,3)*t2 + 
                  5*Power(t1,2)*Power(t2,2) + 7*t1*Power(t2,3) - 
                  2*Power(t2,4))) + 
            s1*(-3*Power(s2,4)*t2*(t1 + 2*t2) + 
               Power(s2,3)*(2*Power(t1,3) + 17*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - 5*Power(t2,3)) - 
               4*t1*Power(t2,2)*
                (3*Power(t1,3) - 2*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,2)*(-4*Power(t1,4) - 30*Power(t1,3)*t2 + 
                  24*Power(t1,2)*Power(t2,2) - 17*t1*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s2*(2*Power(t1,5) + 15*Power(t1,4)*t2 - 
                  6*Power(t1,3)*Power(t2,2) + 
                  11*Power(t1,2)*Power(t2,3) - 14*t1*Power(t2,4) + 
                  4*Power(t2,5)))) + 
         Power(s,2)*(Power(s1,6)*Power(s2 - t1,2)*t1*
             (Power(s2,5)*(4*t1 - 5*t2) + Power(t1,4)*Power(t2,2) + 
               Power(s2,2)*Power(t1,2)*
                (-12*Power(t1,2) + 20*t1*t2 - 9*Power(t2,2)) + 
               s2*Power(t1,3)*(2*Power(t1,2) - t1*t2 - 5*Power(t2,2)) + 
               Power(s2,4)*(-16*Power(t1,2) + 19*t1*t2 - 
                  5*Power(t2,2)) + 
               Power(s2,3)*t1*
                (22*Power(t1,2) - 33*t1*t2 + 10*Power(t2,2))) + 
            Power(s1,5)*(s2 - t1)*
             (2*Power(t1,6)*t2*
                (3*Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)) + 
               2*Power(s2,7)*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*Power(t1,3)*
                (8*Power(t1,3) - 38*Power(t1,2)*t2 + 
                  163*t1*Power(t2,2) - 102*Power(t2,3)) + 
               Power(s2,6)*(-20*Power(t1,3) + 9*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,4)*Power(t1,2)*
                (-32*Power(t1,3) + 29*Power(t1,2)*t2 - 
                  95*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,5)*t1*
                (38*Power(t1,3) - 15*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + 18*Power(t2,3)) - 
               s2*Power(t1,5)*
                (2*Power(t1,3) + 31*Power(t1,2)*t2 - 
                  19*t1*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s2,2)*Power(t1,4)*
                (4*Power(t1,3) + 46*Power(t1,2)*t2 - 
                  89*t1*Power(t2,2) + 103*Power(t2,3))) + 
            s2*Power(t2,3)*(-4*Power(t1,7)*Power(t1 - t2,2)*
                (Power(t1,2) + 2*Power(t2,2)) + 
               Power(s2,8)*(4*Power(t1,3) - 8*Power(t1,2)*t2 - 
                  t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,4)*Power(t1,3)*
                (-24*Power(t1,4) + 425*Power(t1,3)*t2 - 
                  1007*Power(t1,2)*Power(t2,2) + 665*t1*Power(t2,3) - 
                  116*Power(t2,4)) + 
               Power(s2,2)*Power(t1,5)*
                (-42*Power(t1,4) + 139*Power(t1,3)*t2 - 
                  296*Power(t1,2)*Power(t2,2) + 293*t1*Power(t2,3) - 
                  96*Power(t2,4)) + 
               Power(s2,6)*t1*
                (98*Power(t1,4) + 8*Power(t1,3)*t2 - 
                  289*Power(t1,2)*Power(t2,2) + 147*t1*Power(t2,3) - 
                  22*Power(t2,4)) + 
               Power(s2,7)*(-38*Power(t1,4) + 33*Power(t1,3)*t2 + 
                  49*Power(t1,2)*Power(t2,2) - 25*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               2*s2*Power(t1,6)*
                (7*Power(t1,4) - 15*Power(t1,3)*t2 + 
                  36*Power(t1,2)*Power(t2,2) - 49*t1*Power(t2,3) + 
                  21*Power(t2,4)) + 
               Power(s2,5)*Power(t1,2)*
                (-82*Power(t1,4) - 219*Power(t1,3)*t2 + 
                  749*Power(t1,2)*Power(t2,2) - 437*t1*Power(t2,3) + 
                  69*Power(t2,4)) + 
               Power(s2,3)*Power(t1,4)*
                (74*Power(t1,4) - 356*Power(t1,3)*t2 + 
                  735*Power(t1,2)*Power(t2,2) - 564*t1*Power(t2,3) + 
                  130*Power(t2,4))) - 
            Power(s1,4)*(2*Power(s2,9)*(2*t1 - t2)*t2 + 
               Power(s2,8)*(-2*Power(t1,3) + 15*Power(t1,2)*t2 - 
                  8*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,7)*t2*
                (6*Power(t1,3) - 16*Power(t1,2)*t2 - 
                  8*t1*Power(t2,2) + 13*Power(t2,3)) + 
               s2*Power(t1,6)*t2*
                (-7*Power(t1,3) + 80*Power(t1,2)*t2 - 
                  86*t1*Power(t2,2) + 81*Power(t2,3)) + 
               Power(s2,4)*Power(t1,3)*
                (-86*Power(t1,4) + 146*Power(t1,3)*t2 - 
                  448*Power(t1,2)*Power(t2,2) + 1563*t1*Power(t2,3) - 
                  672*Power(t2,4)) + 
               Power(s2,6)*t1*
                (-46*Power(t1,4) + 228*Power(t1,3)*t2 - 
                  61*Power(t1,2)*Power(t2,2) + 142*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,7)*(14*Power(t1,4) - 103*Power(t1,3)*t2 + 
                  28*Power(t1,2)*Power(t2,2) - 19*t1*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s2,5)*Power(t1,2)*
                (84*Power(t1,4) - 268*Power(t1,3)*t2 + 
                  239*Power(t1,2)*Power(t2,2) - 776*t1*Power(t2,3) + 
                  150*Power(t2,4)) - 
               Power(s2,2)*Power(t1,5)*
                (10*Power(t1,4) + 23*Power(t1,3)*t2 + 
                  205*Power(t1,2)*Power(t2,2) - 584*t1*Power(t2,3) + 
                  510*Power(t2,4)) + 
               Power(s2,3)*Power(t1,4)*
                (46*Power(t1,4) + 2*Power(t1,3)*t2 + 
                  393*Power(t1,2)*Power(t2,2) - 1406*t1*Power(t2,3) + 
                  925*Power(t2,4))) + 
            s1*Power(t2,2)*(-12*Power(t1,7)*Power(t1 - t2,2)*
                Power(t2,3) + 
               Power(s2,9)*(-4*Power(t1,3) + 12*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) - 8*Power(t2,3)) + 
               6*s2*Power(t1,6)*Power(t1 - t2,2)*
                (2*Power(t1,3) + Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
                  14*Power(t2,3)) + 
               Power(s2,6)*t1*t2*
                (53*Power(t1,4) - 1408*Power(t1,3)*t2 + 
                  947*Power(t1,2)*Power(t2,2) - 81*t1*Power(t2,3) - 
                  15*Power(t2,4)) + 
               Power(s2,8)*(38*Power(t1,4) - 85*Power(t1,3)*t2 - 
                  126*Power(t1,2)*Power(t2,2) + 85*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,7)*(-75*Power(t1,5) + 119*Power(t1,4)*t2 + 
                  631*Power(t1,3)*Power(t2,2) - 
                  391*Power(t1,2)*Power(t2,3) + 53*t1*Power(t2,4) + 
                  2*Power(t2,5)) + 
               Power(s2,5)*Power(t1,2)*
                (126*Power(t1,5) - 145*Power(t1,4)*t2 + 
                  1476*Power(t1,3)*Power(t2,2) - 
                  1044*Power(t1,2)*Power(t2,3) - 140*t1*Power(t2,4) + 
                  94*Power(t2,5)) - 
               2*Power(s2,2)*Power(t1,5)*
                (16*Power(t1,5) + 17*Power(t1,4)*t2 + 
                  2*Power(t1,3)*Power(t2,2) + 
                  34*Power(t1,2)*Power(t2,3) - 181*t1*Power(t2,4) + 
                  109*Power(t2,5)) - 
               2*Power(s2,4)*Power(t1,3)*
                (63*Power(t1,5) + 31*Power(t1,4)*t2 + 
                  307*Power(t1,3)*Power(t2,2) - 
                  208*Power(t1,2)*Power(t2,3) - 245*t1*Power(t2,4) + 
                  123*Power(t2,5)) + 
               Power(s2,3)*Power(t1,4)*
                (61*Power(t1,5) + 160*Power(t1,4)*t2 + 
                  8*Power(t1,3)*Power(t2,2) + 
                  47*Power(t1,2)*Power(t2,3) - 566*t1*Power(t2,4) + 
                  310*Power(t2,5))) - 
            Power(s1,2)*t2*(3*Power(s2,9)*(5*t1 - 4*t2)*Power(t2,2) + 
               2*Power(t1,7)*Power(t2,2)*
                (4*Power(t1,3) - 15*Power(t1,2)*t2 + 
                  24*t1*Power(t2,2) - 13*Power(t2,3)) + 
               Power(s2,8)*(10*Power(t1,4) - 63*Power(t1,3)*t2 - 
                  98*Power(t1,2)*Power(t2,2) + 99*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,4)*Power(t1,3)*
                (122*Power(t1,5) - 470*Power(t1,4)*t2 - 
                  242*Power(t1,3)*Power(t2,2) - 
                  380*Power(t1,2)*Power(t2,3) + 2035*t1*Power(t2,4) - 
                  848*Power(t2,5)) + 
               Power(s2,2)*Power(t1,5)*
                (5*Power(t1,5) + 26*Power(t1,4)*t2 - 
                  154*Power(t1,3)*Power(t2,2) - 
                  211*Power(t1,2)*Power(t2,3) + 1096*t1*Power(t2,4) - 
                  694*Power(t2,5)) + 
               Power(s2,6)*t1*
                (63*Power(t1,5) - 343*Power(t1,4)*t2 - 
                  783*Power(t1,3)*Power(t2,2) + 
                  688*Power(t1,2)*Power(t2,3) + 148*t1*Power(t2,4) - 
                  44*Power(t2,5)) + 
               Power(s2,7)*(-35*Power(t1,5) + 215*Power(t1,4)*t2 + 
                  385*Power(t1,3)*Power(t2,2) - 
                  355*Power(t1,2)*Power(t2,3) + 20*t1*Power(t2,4) + 
                  8*Power(t2,5)) + 
               s2*Power(t1,6)*
                (6*Power(t1,5) - 39*Power(t1,4)*t2 + 
                  58*Power(t1,3)*Power(t2,2) + 
                  81*Power(t1,2)*Power(t2,3) - 342*t1*Power(t2,4) + 
                  230*Power(t2,5)) + 
               Power(s2,5)*Power(t1,2)*
                (-102*Power(t1,5) + 451*Power(t1,4)*t2 + 
                  719*Power(t1,3)*Power(t2,2) - 
                  402*Power(t1,2)*Power(t2,3) - 999*t1*Power(t2,4) + 
                  304*Power(t2,5)) + 
               Power(s2,3)*Power(t1,4)*
                (-69*Power(t1,5) + 223*Power(t1,4)*t2 + 
                  92*Power(t1,3)*Power(t2,2) + 
                  524*Power(t1,2)*Power(t2,3) - 2003*t1*Power(t2,4) + 
                  1066*Power(t2,5))) + 
            Power(s1,3)*(Power(s2,9)*t2*
                (-4*Power(t1,2) + 13*t1*t2 - 8*Power(t2,2)) + 
               2*Power(t1,7)*Power(t2,2)*
                (7*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,2)*Power(t1,5)*t2*
                (143*Power(t1,4) - 272*Power(t1,3)*t2 - 
                  158*Power(t1,2)*Power(t2,2) + 1146*t1*Power(t2,3) - 
                  859*Power(t2,4)) + 
               Power(s2,8)*(-2*Power(t1,4) - 13*Power(t1,3)*t2 - 
                  10*Power(t1,2)*Power(t2,2) + 37*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s2*Power(t1,6)*t2*
                (-39*Power(t1,4) + 39*Power(t1,3)*t2 + 
                  76*Power(t1,2)*Power(t2,2) - 266*t1*Power(t2,3) + 
                  214*Power(t2,4)) + 
               Power(s2,6)*t1*
                (-36*Power(t1,5) - 96*Power(t1,4)*t2 + 
                  22*Power(t1,3)*Power(t2,2) + 
                  165*Power(t1,2)*Power(t2,3) + 297*t1*Power(t2,4) - 
                  37*Power(t2,5)) + 
               Power(s2,7)*(14*Power(t1,5) + 72*Power(t1,4)*t2 - 
                  36*Power(t1,3)*Power(t2,2) - 
                  102*Power(t1,2)*Power(t2,3) - 33*t1*Power(t2,4) + 
                  12*Power(t2,5)) + 
               Power(s2,5)*Power(t1,2)*
                (44*Power(t1,5) + 20*Power(t1,4)*t2 + 
                  120*Power(t1,3)*Power(t2,2) + 
                  108*Power(t1,2)*Power(t2,3) - 1362*t1*Power(t2,4) + 
                  353*Power(t2,5)) - 
               Power(s2,4)*Power(t1,3)*
                (26*Power(t1,5) - 114*Power(t1,4)*t2 + 
                  342*Power(t1,3)*Power(t2,2) + 
                  528*Power(t1,2)*Power(t2,3) - 2615*t1*Power(t2,4) + 
                  1133*Power(t2,5)) + 
               Power(s2,3)*Power(t1,4)*
                (6*Power(t1,5) - 197*Power(t1,4)*t2 + 
                  452*Power(t1,3)*Power(t2,2) + 
                  438*Power(t1,2)*Power(t2,3) - 2421*t1*Power(t2,4) + 
                  1453*Power(t2,5)))) + 
         Power(s,8)*(-(Power(s1,3)*t1*
               (Power(s2,4)*(-4*t1 + 10*t2) + 
                 4*Power(s2,3)*
                  (4*Power(t1,2) - 11*t1*t2 + 5*Power(t2,2)) + 
                 2*Power(s2,2)*
                  (-10*Power(t1,3) + 41*Power(t1,2)*t2 - 
                    8*t1*Power(t2,2) + Power(t2,3)) + 
                 t1*t2*(24*Power(t1,3) - 7*Power(t1,2)*t2 - 
                    25*t1*Power(t2,2) + 8*Power(t2,3)) + 
                 s2*(8*Power(t1,4) - 73*Power(t1,3)*t2 + 
                    7*Power(t1,2)*Power(t2,2) + 30*t1*Power(t2,3) - 
                    8*Power(t2,4)))) + 
            (t1 - t2)*t2*(2*Power(s2,5)*t1*(3*t1 + 5*t2) + 
               Power(s2,4)*(-57*Power(t1,3) + 40*Power(t1,2)*t2 - 
                  10*t1*Power(t2,2) + 2*Power(t2,3)) + 
               4*s2*Power(t1,2)*
                (32*Power(t1,4) - 44*Power(t1,3)*t2 - 
                  Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,3)*(170*Power(t1,4) - 217*Power(t1,3)*t2 + 
                  72*Power(t1,2)*Power(t2,2) - 25*t1*Power(t2,3) + 
                  2*Power(t2,4)) - 
               2*Power(t1,3)*
                (12*Power(t1,4) - 15*Power(t1,3)*t2 - 
                  8*Power(t1,2)*Power(t2,2) + 9*t1*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s2,2)*t1*
                (224*Power(t1,4) - 314*Power(t1,3)*t2 + 
                  81*Power(t1,2)*Power(t2,2) + 6*t1*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s1,2)*t1*(-4*Power(s2,5)*t2 + 
               Power(s2,4)*(-2*Power(t1,2) + 23*t1*t2 + 
                  20*Power(t2,2)) + 
               Power(s2,3)*(2*Power(t1,3) - 93*Power(t1,2)*t2 - 
                  50*t1*Power(t2,2) + 62*Power(t2,3)) + 
               2*t1*t2*(21*Power(t1,4) - 12*Power(t1,3)*t2 - 
                  6*Power(t1,2)*Power(t2,2) - 11*t1*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s2,2)*(2*Power(t1,4) + 179*Power(t1,3)*t2 - 
                  24*Power(t1,2)*Power(t2,2) - 57*t1*Power(t2,3) + 
                  22*Power(t2,4)) - 
               2*s2*(Power(t1,5) + 74*Power(t1,4)*t2 - 
                  38*Power(t1,3)*Power(t2,2) - 
                  9*Power(t1,2)*Power(t2,3) - 9*t1*Power(t2,4) + 
                  8*Power(t2,5))) + 
            s1*(2*Power(s2,5)*t1*t2*(t1 + 7*t2) + 
               Power(s2,4)*(-6*Power(t1,4) - 14*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,2)*t1*
                (-18*Power(t1,5) + 75*Power(t1,4)*t2 - 
                  164*Power(t1,3)*Power(t2,2) + 
                  99*Power(t1,2)*Power(t2,3) + 28*t1*Power(t2,4) - 
                  24*Power(t2,5)) + 
               2*Power(t1,2)*t2*
                (21*Power(t1,5) - 52*Power(t1,4)*t2 + 
                  33*Power(t1,3)*Power(t2,2) + 
                  6*Power(t1,2)*Power(t2,3) - 4*t1*Power(t2,4) - 
                  4*Power(t2,5)) + 
               Power(s2,3)*(18*Power(t1,5) - Power(t1,4)*t2 - 
                  Power(t1,3)*Power(t2,2) + 
                  52*Power(t1,2)*Power(t2,3) - 60*t1*Power(t2,4) + 
                  2*Power(t2,5)) + 
               s2*t1*(6*Power(t1,6) - 107*Power(t1,5)*t2 + 
                  260*Power(t1,4)*Power(t2,2) - 
                  201*Power(t1,3)*Power(t2,3) + 
                  8*Power(t1,2)*Power(t2,4) + 18*t1*Power(t2,5) + 
                  8*Power(t2,6)))) + 
         Power(s,3)*(Power(s1,6)*Power(s2 - t1,2)*t1*
             (2*Power(s2,5) + 3*Power(t1,3)*Power(t2,2) + 
               Power(s2,4)*(-14*t1 + 6*t2) + 
               Power(s2,2)*t1*
                (-22*Power(t1,2) + 41*t1*t2 - 15*Power(t2,2)) + 
               4*Power(s2,3)*(7*Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               s2*Power(t1,2)*(6*Power(t1,2) - 15*t1*t2 + 4*Power(t2,2))\
) + Power(s1,5)*(s2 - t1)*(2*Power(s2,7)*t1 + 
               Power(t1,5)*t2*
                (22*Power(t1,2) - t1*t2 - 7*Power(t2,2)) + 
               Power(s2,6)*(-22*Power(t1,2) + 11*t1*t2 - 
                  4*Power(t2,2)) + 
               Power(s2,3)*Power(t1,2)*
                (90*Power(t1,3) - 179*Power(t1,2)*t2 + 
                  199*t1*Power(t2,2) - 75*Power(t2,3)) + 
               2*s2*Power(t1,4)*
                (Power(t1,3) - 33*Power(t1,2)*t2 - 6*t1*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(s2,5)*(74*Power(t1,3) - 76*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,4)*t1*
                (-116*Power(t1,3) + 170*Power(t1,2)*t2 - 
                  123*t1*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*Power(t1,3)*
                (-30*Power(t1,3) + 118*Power(t1,2)*t2 - 
                  73*t1*Power(t2,2) + 105*Power(t2,3))) - 
            Power(s1,4)*(Power(t1,6)*Power(t2,2)*
                (-36*Power(t1,2) + 14*t1*t2 + Power(t2,2)) + 
               2*Power(s2,8)*(Power(t1,2) - 2*t1*t2 + 4*Power(t2,2)) - 
               Power(s2,7)*(8*Power(t1,3) + 26*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s2,4)*Power(t1,2)*
                (-108*Power(t1,4) + 229*Power(t1,3)*t2 - 
                  559*Power(t1,2)*Power(t2,2) + 1626*t1*Power(t2,3) - 
                  549*Power(t2,4)) + 
               Power(s2,6)*(2*Power(t1,4) + 176*Power(t1,3)*t2 - 
                  150*Power(t1,2)*Power(t2,2) + 143*t1*Power(t2,3) - 
                  20*Power(t2,4)) + 
               Power(s2,5)*t1*
                (46*Power(t1,4) - 347*Power(t1,3)*t2 + 
                  434*Power(t1,2)*Power(t2,2) - 745*t1*Power(t2,3) + 
                  108*Power(t2,4)) + 
               s2*Power(t1,5)*
                (8*Power(t1,4) + 79*Power(t1,3)*t2 + 
                  39*Power(t1,2)*Power(t2,2) - 112*t1*Power(t2,3) + 
                  174*Power(t2,4)) - 
               Power(s2,2)*Power(t1,4)*
                (48*Power(t1,4) + 219*Power(t1,3)*t2 + 
                  48*Power(t1,2)*Power(t2,2) - 688*t1*Power(t2,3) + 
                  768*Power(t2,4)) + 
               Power(s2,3)*Power(t1,3)*
                (106*Power(t1,4) + 112*Power(t1,3)*t2 + 
                  309*Power(t1,2)*Power(t2,2) - 1602*t1*Power(t2,3) + 
                  1056*Power(t2,4))) - 
            Power(t2,2)*(2*Power(s2,9)*(2*t1 - t2)*Power(t2,2) - 
               4*Power(t1,7)*Power(t1 - t2,2)*Power(t2,2)*(t1 + t2) + 
               Power(s2,8)*(Power(t1,4) + 29*Power(t1,3)*t2 - 
                  64*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s2,7)*(17*Power(t1,5) - 164*Power(t1,4)*t2 + 
                  211*Power(t1,3)*Power(t2,2) + 
                  89*Power(t1,2)*Power(t2,3) - 76*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               2*s2*Power(t1,6)*
                (3*Power(t1,5) + 3*Power(t1,4)*t2 + 
                  5*Power(t1,3)*Power(t2,2) - 
                  6*Power(t1,2)*Power(t2,3) - 26*t1*Power(t2,4) + 
                  21*Power(t2,5)) - 
               Power(s2,6)*t1*
                (81*Power(t1,5) - 263*Power(t1,4)*t2 + 
                  171*Power(t1,3)*Power(t2,2) + 
                  466*Power(t1,2)*Power(t2,3) - 364*t1*Power(t2,4) + 
                  63*Power(t2,5)) - 
               Power(s2,2)*Power(t1,5)*
                (15*Power(t1,5) + 94*Power(t1,4)*t2 - 
                  90*Power(t1,3)*Power(t2,2) + 
                  68*Power(t1,2)*Power(t2,3) - 219*t1*Power(t2,4) + 
                  132*Power(t2,5)) + 
               Power(s2,5)*Power(t1,2)*
                (128*Power(t1,5) - 31*Power(t1,4)*t2 - 
                  331*Power(t1,3)*Power(t2,2) + 
                  1028*Power(t1,2)*Power(t2,3) - 811*t1*Power(t2,4) + 
                  156*Power(t2,5)) + 
               Power(s2,3)*Power(t1,4)*
                (33*Power(t1,5) + 289*Power(t1,4)*t2 - 
                  476*Power(t1,3)*Power(t2,2) + 
                  500*Power(t1,2)*Power(t2,3) - 556*t1*Power(t2,4) + 
                  220*Power(t2,5)) - 
               Power(s2,4)*Power(t1,3)*
                (89*Power(t1,5) + 298*Power(t1,4)*t2 - 
                  731*Power(t1,3)*Power(t2,2) + 
                  1075*Power(t1,2)*Power(t2,3) - 900*t1*Power(t2,4) + 
                  230*Power(t2,5))) + 
            Power(s1,3)*(2*Power(s2,9)*(2*t1 - t2)*t2 - 
               2*Power(s2,8)*
                (2*Power(t1,3) + 4*Power(t1,2)*t2 + 9*t1*Power(t2,2) - 
                  16*Power(t2,3)) + 
               Power(t1,6)*t2*
                (22*Power(t1,4) - 30*Power(t1,3)*t2 - 
                  4*Power(t1,2)*Power(t2,2) + 75*t1*Power(t2,3) - 
                  73*Power(t2,4)) + 
               Power(s2,7)*(30*Power(t1,4) - 8*Power(t1,3)*t2 + 
                  57*Power(t1,2)*Power(t2,2) - 132*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s2*Power(t1,5)*t2*
                (-145*Power(t1,4) + 359*Power(t1,3)*t2 - 
                  276*Power(t1,2)*Power(t2,2) - 425*t1*Power(t2,3) + 
                  535*Power(t2,4)) + 
               Power(s2,2)*Power(t1,4)*
                (-18*Power(t1,5) + 305*Power(t1,4)*t2 - 
                  935*Power(t1,3)*Power(t2,2) + 
                  757*Power(t1,2)*Power(t2,3) + 1465*t1*Power(t2,4) - 
                  1523*Power(t2,5)) + 
               Power(s2,4)*Power(t1,2)*
                (-168*Power(t1,5) + 236*Power(t1,4)*t2 - 
                  528*Power(t1,3)*Power(t2,2) + 
                  392*Power(t1,2)*Power(t2,3) + 2497*t1*Power(t2,4) - 
                  1022*Power(t2,5)) + 
               Power(s2,6)*(-98*Power(t1,5) + 75*Power(t1,4)*t2 - 
                  37*Power(t1,3)*Power(t2,2) + 
                  218*Power(t1,2)*Power(t2,3) + 195*t1*Power(t2,4) - 
                  36*Power(t2,5)) + 
               Power(s2,5)*t1*
                (172*Power(t1,5) - 166*Power(t1,4)*t2 + 
                  102*Power(t1,3)*Power(t2,2) - 
                  276*Power(t1,2)*Power(t2,3) - 1128*t1*Power(t2,4) + 
                  233*Power(t2,5)) + 
               Power(s2,3)*Power(t1,3)*
                (86*Power(t1,5) - 315*Power(t1,4)*t2 + 
                  1032*Power(t1,3)*Power(t2,2) - 
                  711*Power(t1,2)*Power(t2,3) - 2681*t1*Power(t2,4) + 
                  1892*Power(t2,5))) + 
            s1*t2*(-2*Power(s2,9)*t2*
                (2*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) - 
               2*Power(t1,6)*Power(t2,2)*
                (4*Power(t1,4) + 10*Power(t1,3)*t2 - 
                  15*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
                  21*Power(t2,4)) + 
               Power(s2,8)*(5*Power(t1,4) + 22*Power(t1,3)*t2 - 
                  130*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) + 
                  32*Power(t2,4)) + 
               Power(s2,7)*(-22*Power(t1,5) - 29*Power(t1,4)*t2 + 
                  390*Power(t1,3)*Power(t2,2) + 
                  252*Power(t1,2)*Power(t2,3) - 249*t1*Power(t2,4) + 
                  30*Power(t2,5)) + 
               Power(s2,2)*Power(t1,4)*
                (7*Power(t1,6) - 82*Power(t1,5)*t2 - 
                  413*Power(t1,4)*Power(t2,2) + 
                  265*Power(t1,3)*Power(t2,3) + 
                  385*Power(t1,2)*Power(t2,4) + 332*t1*Power(t2,5) - 
                  430*Power(t2,6)) + 
               Power(s2,4)*Power(t1,2)*
                (139*Power(t1,6) - 1004*Power(t1,5)*t2 + 
                  311*Power(t1,4)*Power(t2,2) - 
                  1073*Power(t1,3)*Power(t2,3) + 
                  1565*Power(t1,2)*Power(t2,4) + 202*t1*Power(t2,5) - 
                  262*Power(t2,6)) + 
               Power(s2,6)*(57*Power(t1,6) - 224*Power(t1,5)*t2 - 
                  316*Power(t1,4)*Power(t2,2) - 
                  1015*Power(t1,3)*Power(t2,3) + 
                  943*Power(t1,2)*Power(t2,4) - 121*t1*Power(t2,5) - 
                  8*Power(t2,6)) + 
               Power(s2,5)*t1*
                (-114*Power(t1,6) + 800*Power(t1,5)*t2 - 
                  258*Power(t1,4)*Power(t2,2) + 
                  1708*Power(t1,3)*Power(t2,3) - 
                  1765*Power(t1,2)*Power(t2,4) + 76*t1*Power(t2,5) + 
                  66*Power(t2,6)) + 
               2*s2*Power(t1,5)*
                (3*Power(t1,6) - 6*Power(t1,5)*t2 + 
                  63*Power(t1,4)*Power(t2,2) + 
                  3*Power(t1,3)*Power(t2,3) - 
                  95*Power(t1,2)*Power(t2,4) - 75*t1*Power(t2,5) + 
                  103*Power(t2,6)) + 
               Power(s2,3)*Power(t1,3)*
                (-78*Power(t1,6) + 533*Power(t1,5)*t2 + 
                  286*Power(t1,4)*Power(t2,2) - 
                  117*Power(t1,3)*Power(t2,3) - 
                  752*Power(t1,2)*Power(t2,4) - 414*t1*Power(t2,5) + 
                  472*Power(t2,6))) + 
            Power(s1,2)*(2*Power(s2,9)*t2*
                (2*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,8)*t2*
                (-13*Power(t1,3) + 76*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - 48*Power(t2,3)) - 
               2*Power(t1,6)*Power(t2,2)*
                (8*Power(t1,4) - 26*Power(t1,3)*t2 + 
                  18*Power(t1,2)*Power(t2,2) + 53*t1*Power(t2,3) - 
                  53*Power(t2,4)) + 
               s2*Power(t1,5)*t2*
                (57*Power(t1,5) - 46*Power(t1,4)*t2 - 
                  249*Power(t1,3)*Power(t2,2) + 
                  355*Power(t1,2)*Power(t2,3) + 509*t1*Power(t2,4) - 
                  574*Power(t2,5)) - 
               Power(s2,7)*(4*Power(t1,5) - 21*Power(t1,4)*t2 + 
                  243*Power(t1,3)*Power(t2,2) + 
                  222*Power(t1,2)*Power(t2,3) - 295*t1*Power(t2,4) + 
                  26*Power(t2,5)) + 
               Power(s2,2)*Power(t1,4)*t2*
                (-280*Power(t1,5) + 599*Power(t1,4)*t2 + 
                  140*Power(t1,3)*Power(t2,2) - 
                  728*Power(t1,2)*Power(t2,3) - 1373*t1*Power(t2,4) + 
                  1326*Power(t2,5)) + 
               Power(s2,3)*Power(t1,3)*
                (-6*Power(t1,6) + 519*Power(t1,5)*t2 - 
                  1383*Power(t1,4)*Power(t2,2) + 
                  233*Power(t1,3)*Power(t2,3) + 
                  781*Power(t1,2)*Power(t2,4) + 2114*t1*Power(t2,5) - 
                  1526*Power(t2,6)) + 
               Power(s2,5)*t1*
                (-30*Power(t1,6) + 201*Power(t1,5)*t2 - 
                  811*Power(t1,4)*Power(t2,2) - 
                  688*Power(t1,3)*Power(t2,3) + 
                  1344*Power(t1,2)*Power(t2,4) + 650*t1*Power(t2,5) - 
                  206*Power(t2,6)) + 
               Power(s2,6)*(18*Power(t1,6) - 51*Power(t1,5)*t2 + 
                  451*Power(t1,4)*Power(t2,2) + 
                  610*Power(t1,3)*Power(t2,3) - 
                  842*Power(t1,2)*Power(t2,4) - 24*t1*Power(t2,5) + 
                  28*Power(t2,6)) + 
               Power(s2,4)*Power(t1,2)*
                (22*Power(t1,6) - 458*Power(t1,5)*t2 + 
                  1385*Power(t1,4)*Power(t2,2) + 
                  105*Power(t1,3)*Power(t2,3) - 
                  1121*Power(t1,2)*Power(t2,4) - 1736*t1*Power(t2,5) + 
                  840*Power(t2,6)))) + 
         Power(s,7)*(Power(s1,4)*t1*
             (2*Power(s2,5) - 8*Power(s2,4)*(2*t1 - 3*t2) + 
               Power(s2,3)*(38*Power(t1,2) - 86*t1*t2 + 
                  38*Power(t2,2)) + 
               t1*t2*(16*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  26*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(-36*Power(t1,3) + 117*Power(t1,2)*t2 - 
                  46*t1*Power(t2,2) + 12*Power(t2,3)) + 
               s2*(12*Power(t1,4) - 71*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) + 18*t1*Power(t2,3) - 
                  4*Power(t2,4))) + 
            Power(s1,3)*t1*(Power(s2,5)*(-10*t1 + 33*t2) + 
               Power(s2,4)*(42*Power(t1,2) - 137*t1*t2 + 
                  8*Power(t2,2)) + 
               Power(s2,3)*(-66*Power(t1,3) + 295*Power(t1,2)*t2 + 
                  24*t1*Power(t2,2) - 111*Power(t2,3)) + 
               Power(s2,2)*(46*Power(t1,4) - 399*Power(t1,3)*t2 - 
                  18*Power(t1,2)*Power(t2,2) + 177*t1*Power(t2,3) - 
                  74*Power(t2,4)) + 
               t1*t2*(-86*Power(t1,4) + 41*Power(t1,3)*t2 + 
                  35*Power(t1,2)*Power(t2,2) + 20*t1*Power(t2,3) - 
                  12*Power(t2,4)) + 
               s2*(-12*Power(t1,5) + 295*Power(t1,4)*t2 - 
                  59*Power(t1,3)*Power(t2,2) - 
                  114*Power(t1,2)*Power(t2,3) + 34*t1*Power(t2,4) + 
                  12*Power(t2,5))) + 
            t2*(-2*Power(s2,6)*t1*
                (2*Power(t1,2) + 3*t1*t2 - 5*Power(t2,2)) - 
               2*Power(t1,3)*Power(t1 - t2,2)*
                (8*Power(t1,4) - 3*Power(t1,3)*t2 - 
                  31*Power(t1,2)*Power(t2,2) - 13*t1*Power(t2,3) - 
                  Power(t2,4)) + 
               2*Power(s2,5)*
                (24*Power(t1,4) - 59*Power(t1,3)*t2 + 
                  55*Power(t1,2)*Power(t2,2) - 26*t1*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,4)*(-203*Power(t1,5) + 640*Power(t1,4)*t2 - 
                  577*Power(t1,3)*Power(t2,2) + 
                  221*Power(t1,2)*Power(t2,3) - 85*t1*Power(t2,4) + 
                  8*Power(t2,5)) + 
               s2*Power(t1,2)*
                (136*Power(t1,6) - 347*Power(t1,5)*t2 + 
                  26*Power(t1,4)*Power(t2,2) + 
                  314*Power(t1,3)*Power(t2,3) - 
                  82*Power(t1,2)*Power(t2,4) - 43*t1*Power(t2,5) - 
                  4*Power(t2,6)) + 
               Power(s2,3)*(383*Power(t1,6) - 1186*Power(t1,5)*t2 + 
                  963*Power(t1,4)*Power(t2,2) - 
                  197*Power(t1,3)*Power(t2,3) + 
                  43*Power(t1,2)*Power(t2,4) - 6*t1*Power(t2,5) - 
                  2*Power(t2,6)) + 
               Power(s2,2)*t1*
                (-345*Power(t1,6) + 979*Power(t1,5)*t2 - 
                  564*Power(t1,4)*Power(t2,2) - 
                  206*Power(t1,3)*Power(t2,3) + 
                  108*Power(t1,2)*Power(t2,4) + 23*t1*Power(t2,5) + 
                  5*Power(t2,6))) + 
            Power(s1,2)*(6*Power(s2,6)*t1*t2 + 
               Power(s2,4)*t1*
                (30*Power(t1,3) + 25*Power(t1,2)*t2 + 
                  200*t1*Power(t2,2) - 122*Power(t2,3)) - 
               Power(s2,5)*(6*Power(t1,3) + 2*Power(t1,2)*t2 + 
                  75*t1*Power(t2,2) - 4*Power(t2,3)) + 
               2*Power(t1,2)*Power(t2,2)*
                (35*Power(t1,4) - 36*Power(t1,3)*t2 - 
                  26*Power(t1,2)*Power(t2,2) + 21*t1*Power(t2,3) + 
                  6*Power(t2,4)) - 
               Power(s2,3)*(60*Power(t1,5) + 158*Power(t1,4)*t2 + 
                  175*Power(t1,3)*Power(t2,2) - 
                  174*Power(t1,2)*Power(t2,3) - 101*t1*Power(t2,4) + 
                  4*Power(t2,5)) + 
               Power(s2,2)*t1*
                (54*Power(t1,5) + 228*Power(t1,4)*t2 + 
                  63*Power(t1,3)*Power(t2,2) - 
                  184*Power(t1,2)*Power(t2,3) - 141*t1*Power(t2,4) + 
                  122*Power(t2,5)) - 
               s2*t1*(18*Power(t1,6) + 96*Power(t1,5)*t2 + 
                  91*Power(t1,4)*Power(t2,2) - 
                  205*Power(t1,3)*Power(t2,3) - 
                  114*Power(t1,2)*Power(t2,4) + 130*t1*Power(t2,5) + 
                  12*Power(t2,6))) + 
            s1*(2*Power(s2,6)*t1*(t1 - 8*t2)*t2 + 
               Power(s2,5)*(6*Power(t1,4) - 7*Power(t1,3)*t2 - 
                  31*Power(t1,2)*Power(t2,2) + 74*t1*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s2,4)*(-28*Power(t1,5) + 85*Power(t1,4)*t2 + 
                  10*Power(t1,3)*Power(t2,2) - 
                  120*Power(t1,2)*Power(t2,3) + 158*t1*Power(t2,4) - 
                  8*Power(t2,5)) + 
               2*Power(t1,2)*t2*
                (43*Power(t1,6) - 99*Power(t1,5)*t2 + 
                  57*Power(t1,4)*Power(t2,2) + 
                  28*Power(t1,3)*Power(t2,3) - 
                  8*Power(t1,2)*Power(t2,4) - 19*t1*Power(t2,5) - 
                  2*Power(t2,6)) + 
               Power(s2,3)*(44*Power(t1,6) - 342*Power(t1,5)*t2 + 
                  552*Power(t1,4)*Power(t2,2) - 
                  348*Power(t1,3)*Power(t2,3) - 
                  104*Power(t1,2)*Power(t2,4) - 21*t1*Power(t2,5) + 
                  6*Power(t2,6)) - 
               Power(s2,2)*t1*
                (28*Power(t1,6) - 545*Power(t1,5)*t2 + 
                  1204*Power(t1,4)*Power(t2,2) - 
                  971*Power(t1,3)*Power(t2,3) + 
                  35*Power(t1,2)*Power(t2,4) + 20*t1*Power(t2,5) + 
                  65*Power(t2,6)) + 
               s2*t1*(6*Power(t1,7) - 368*Power(t1,6)*t2 + 
                  883*Power(t1,5)*Power(t2,2) - 
                  675*Power(t1,4)*Power(t2,3) - 
                  63*Power(t1,3)*Power(t2,4) + 
                  51*Power(t1,2)*Power(t2,5) + 82*t1*Power(t2,6) + 
                  4*Power(t2,7)))) + 
         Power(s,6)*(-(Power(s1,5)*(s2 - t1)*t1*
               (4*Power(s2,4) + Power(s2,3)*(-16*t1 + 22*t2) + 
                 t1*t2*(-4*Power(t1,2) - 7*t1*t2 + 9*Power(t2,2)) + 
                 Power(s2,2)*
                  (20*Power(t1,2) - 49*t1*t2 + 25*Power(t2,2)) + 
                 s2*(-8*Power(t1,3) + 31*Power(t1,2)*t2 - 
                    12*t1*Power(t2,2) + 7*Power(t2,3)))) + 
            Power(s1,4)*t1*(-6*Power(s2,6) + 
               23*Power(s2,5)*(2*t1 - 3*t2) + 
               Power(s2,4)*(-128*Power(t1,2) + 277*t1*t2 - 
                  71*Power(t2,2)) + 
               Power(t1,2)*t2*
                (66*Power(t1,3) + Power(t1,2)*t2 - 52*t1*Power(t2,2) - 
                  6*Power(t2,3)) + 
               Power(s2,3)*(170*Power(t1,3) - 479*Power(t1,2)*t2 + 
                  133*t1*Power(t2,2) + 40*Power(t2,3)) + 
               s2*t1*(28*Power(t1,4) - 265*Power(t1,3)*t2 - 
                  19*Power(t1,2)*Power(t2,2) + 106*t1*Power(t2,3) - 
                  38*Power(t2,4)) - 
               2*Power(s2,2)*
                (55*Power(t1,4) - 235*Power(t1,3)*t2 + 
                  21*Power(t1,2)*Power(t2,2) + 45*t1*Power(t2,3) - 
                  24*Power(t2,4))) + 
            Power(s1,3)*(2*Power(s2,6)*
                (3*Power(t1,2) - 19*t1*t2 + Power(t2,2)) + 
               Power(s2,5)*(-26*Power(t1,3) + 114*Power(t1,2)*t2 + 
                  92*t1*Power(t2,2) - 6*Power(t2,3)) + 
               Power(t1,3)*t2*
                (-92*Power(t1,4) + 33*Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) + 75*t1*Power(t2,3) - 
                  37*Power(t2,4)) + 
               Power(s2,4)*(38*Power(t1,4) - 215*Power(t1,3)*t2 - 
                  369*Power(t1,2)*Power(t2,2) + 322*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,3)*(-10*Power(t1,5) + 457*Power(t1,4)*t2 + 
                  511*Power(t1,3)*Power(t2,2) - 
                  714*Power(t1,2)*Power(t2,3) + 74*t1*Power(t2,4) + 
                  2*Power(t2,5)) - 
               Power(s2,2)*t1*
                (20*Power(t1,5) + 630*Power(t1,4)*t2 + 
                  179*Power(t1,3)*Power(t2,2) - 
                  553*Power(t1,2)*Power(t2,3) + 131*t1*Power(t2,4) + 
                  106*Power(t2,5)) + 
               s2*Power(t1,2)*
                (12*Power(t1,5) + 403*Power(t1,4)*t2 - 
                  86*Power(t1,3)*Power(t2,2) - 
                  165*Power(t1,2)*Power(t2,3) - 28*t1*Power(t2,4) + 
                  128*Power(t2,5))) + 
            t2*(Power(s2,7)*t1*
                (Power(t1,2) + 4*t1*t2 - 5*Power(t2,2)) + 
               Power(s2,6)*(-17*Power(t1,4) + 72*Power(t1,3)*t2 - 
                  110*Power(t1,2)*Power(t2,2) + 83*t1*Power(t2,3) - 
                  20*Power(t2,4)) - 
               2*Power(t1,4)*Power(t1 - t2,2)*
                (2*Power(t1,4) - Power(t1,3)*t2 - 
                  37*Power(t1,2)*Power(t2,2) - 29*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,5)*(110*Power(t1,5) - 582*Power(t1,4)*t2 + 
                  658*Power(t1,3)*Power(t2,2) - 
                  331*Power(t1,2)*Power(t2,3) + 129*t1*Power(t2,4) - 
                  10*Power(t2,5)) + 
               Power(s2,3)*t1*
                (396*Power(t1,6) - 1600*Power(t1,5)*t2 + 
                  957*Power(t1,4)*Power(t2,2) + 
                  372*Power(t1,3)*Power(t2,3) - 
                  127*Power(t1,2)*Power(t2,4) + 22*t1*Power(t2,5) - 
                  30*Power(t2,6)) + 
               s2*Power(t1,3)*
                (66*Power(t1,6) - 175*Power(t1,5)*t2 - 
                  268*Power(t1,4)*Power(t2,2) + 
                  494*Power(t1,3)*Power(t2,3) + 
                  18*Power(t1,2)*Power(t2,4) - 107*t1*Power(t2,5) - 
                  28*Power(t2,6)) + 
               Power(s2,4)*(-301*Power(t1,6) + 1436*Power(t1,5)*t2 - 
                  1301*Power(t1,4)*Power(t2,2) + 
                  360*Power(t1,3)*Power(t2,3) - 
                  157*Power(t1,2)*Power(t2,4) - 19*t1*Power(t2,5) + 
                  10*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*
                (-251*Power(t1,6) + 834*Power(t1,5)*t2 + 
                  10*Power(t1,4)*Power(t2,2) - 
                  891*Power(t1,3)*Power(t2,3) + 
                  182*Power(t1,2)*Power(t2,4) + 77*t1*Power(t2,5) + 
                  39*Power(t2,6))) + 
            Power(s1,2)*(-4*Power(s2,7)*t1*t2 + 
               Power(s2,6)*(10*Power(t1,3) - 31*Power(t1,2)*t2 + 
                  102*t1*Power(t2,2) - 18*Power(t2,3)) + 
               Power(s2,5)*(-58*Power(t1,4) + 140*Power(t1,3)*t2 - 
                  286*Power(t1,2)*Power(t2,2) + 54*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,4)*(142*Power(t1,5) - 174*Power(t1,4)*t2 + 
                  300*Power(t1,3)*Power(t2,2) - 
                  39*Power(t1,2)*Power(t2,3) - 424*t1*Power(t2,4) + 
                  26*Power(t2,5)) + 
               2*Power(t1,3)*t2*
                (-46*Power(t1,5) + 117*Power(t1,4)*t2 - 
                  106*Power(t1,3)*Power(t2,2) - 
                  19*Power(t1,2)*Power(t2,3) + 26*t1*Power(t2,4) + 
                  28*Power(t2,5)) + 
               s2*Power(t1,2)*
                (-22*Power(t1,6) + 263*Power(t1,5)*t2 - 
                  784*Power(t1,4)*Power(t2,2) + 
                  834*Power(t1,3)*Power(t2,3) + 
                  254*Power(t1,2)*Power(t2,4) - 297*t1*Power(t2,5) - 
                  134*Power(t2,6)) + 
               Power(s2,3)*(-172*Power(t1,6) + 146*Power(t1,5)*t2 - 
                  398*Power(t1,4)*Power(t2,2) + 
                  163*Power(t1,3)*Power(t2,3) + 
                  854*Power(t1,2)*Power(t2,4) - 224*t1*Power(t2,5) - 
                  4*Power(t2,6)) + 
               Power(s2,2)*t1*
                (100*Power(t1,6) - 247*Power(t1,5)*t2 + 
                  832*Power(t1,4)*Power(t2,2) - 
                  795*Power(t1,3)*Power(t2,3) - 
                  674*Power(t1,2)*Power(t2,4) + 464*t1*Power(t2,5) + 
                  96*Power(t2,6))) + 
            s1*(-3*Power(s2,7)*t1*(t1 - 3*t2)*t2 + 
               Power(s2,6)*(-2*Power(t1,4) + 10*Power(t1,3)*t2 + 
                  46*Power(t1,2)*Power(t2,2) - 127*t1*Power(t2,3) + 
                  36*Power(t2,4)) + 
               Power(s2,5)*(18*Power(t1,5) - 81*Power(t1,4)*t2 - 
                  102*Power(t1,3)*Power(t2,2) + 
                  252*Power(t1,2)*Power(t2,3) - 187*t1*Power(t2,4) + 
                  4*Power(t2,5)) + 
               Power(s2,4)*(-44*Power(t1,6) + 437*Power(t1,5)*t2 - 
                  700*Power(t1,4)*Power(t2,2) + 
                  467*Power(t1,3)*Power(t2,3) + 
                  160*Power(t1,2)*Power(t2,4) + 215*t1*Power(t2,5) - 
                  30*Power(t2,6)) + 
               2*Power(t1,3)*t2*
                (33*Power(t1,6) - 73*Power(t1,5)*t2 - 
                  Power(t1,4)*Power(t2,2) + 
                  98*Power(t1,3)*Power(t2,3) - 
                  7*Power(t1,2)*Power(t2,4) - 39*t1*Power(t2,5) - 
                  11*Power(t2,6)) + 
               Power(s2,2)*t1*
                (-18*Power(t1,7) + 984*Power(t1,6)*t2 - 
                  2499*Power(t1,5)*Power(t2,2) + 
                  1926*Power(t1,4)*Power(t2,3) + 
                  198*Power(t1,3)*Power(t2,4) + 
                  161*Power(t1,2)*Power(t2,5) - 319*t1*Power(t2,6) - 
                  31*Power(t2,7)) + 
               Power(s2,3)*(44*Power(t1,7) - 967*Power(t1,6)*t2 + 
                  2283*Power(t1,5)*Power(t2,2) - 
                  1952*Power(t1,4)*Power(t2,3) + 
                  164*Power(t1,3)*Power(t2,4) - 
                  372*Power(t1,2)*Power(t2,5) + 165*t1*Power(t2,6) + 
                  2*Power(t2,7)) + 
               s2*Power(t1,2)*
                (2*Power(t1,7) - 444*Power(t1,6)*t2 + 
                  1103*Power(t1,5)*Power(t2,2) - 
                  565*Power(t1,4)*Power(t2,3) - 
                  553*Power(t1,3)*Power(t2,4) + 
                  19*Power(t1,2)*Power(t2,5) + 252*t1*Power(t2,6) + 
                  46*Power(t2,7)))) + 
         Power(s,5)*(Power(s1,6)*Power(s2 - t1,2)*t1*
             (2*Power(s2,3) + t1*Power(t2,2) + 
               Power(s2,2)*(-4*t1 + 7*t2) + 
               s2*(2*Power(t1,2) - 7*t1*t2 + 5*Power(t2,2))) + 
            Power(s1,5)*(s2 - t1)*t1*
             (10*Power(s2,5) + Power(s2,4)*(-48*t1 + 52*t2) + 
               Power(t1,2)*t2*
                (18*Power(t1,2) + 17*t1*t2 - 25*Power(t2,2)) + 
               2*s2*Power(t1,2)*
                (11*Power(t1,2) - 48*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(88*Power(t1,2) - 163*t1*t2 + 
                  57*Power(t2,2)) + 
               Power(s2,2)*(-72*Power(t1,3) + 189*Power(t1,2)*t2 - 
                  82*t1*Power(t2,2) + 15*Power(t2,3))) + 
            Power(s1,4)*(6*Power(s2,7)*t1 + 
               Power(s2,6)*(-46*Power(t1,2) + 62*t1*t2 - 
                  4*Power(t2,2)) + 
               2*Power(s2,5)*t1*
                (74*Power(t1,2) - 139*t1*t2 + 5*Power(t2,2)) + 
               Power(t1,4)*t2*
                (96*Power(t1,3) - 27*Power(t1,2)*t2 - 
                  8*t1*Power(t2,2) - 35*Power(t2,3)) + 
               Power(s2,3)*t1*
                (206*Power(t1,4) - 711*Power(t1,3)*t2 - 
                  124*Power(t1,2)*Power(t2,2) + 443*t1*Power(t2,3) - 
                  132*Power(t2,4)) + 
               s2*Power(t1,3)*
                (12*Power(t1,4) - 380*Power(t1,3)*t2 - 
                  8*Power(t1,2)*Power(t2,2) + 118*t1*Power(t2,3) - 
                  83*Power(t2,4)) + 
               Power(s2,4)*(-242*Power(t1,4) + 554*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) - 186*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (-84*Power(t1,4) + 657*Power(t1,3)*t2 + 
                  145*Power(t1,2)*Power(t2,2) - 371*t1*Power(t2,3) + 
                  248*Power(t2,4))) + 
            Power(s1,3)*(Power(s2,7)*
                (2*Power(t1,2) + 21*t1*t2 - 6*Power(t2,2)) + 
               Power(t1,4)*Power(t2,2)*
                (-87*Power(t1,3) + 93*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) - 59*Power(t2,3)) - 
               2*Power(s2,6)*
                (7*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  60*t1*Power(t2,2) - 14*Power(t2,3)) + 
               Power(s2,5)*(62*Power(t1,4) - 169*Power(t1,3)*t2 + 
                  601*Power(t1,2)*Power(t2,2) - 339*t1*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s2,4)*(-150*Power(t1,5) + 193*Power(t1,4)*t2 - 
                  1067*Power(t1,3)*Power(t2,2) + 
                  1049*Power(t1,2)*Power(t2,3) + 141*t1*Power(t2,4) - 
                  16*Power(t2,5)) + 
               Power(s2,3)*t1*
                (188*Power(t1,5) + 193*Power(t1,4)*t2 + 
                  828*Power(t1,3)*Power(t2,2) - 
                  1227*Power(t1,2)*Power(t2,3) - 275*t1*Power(t2,4) + 
                  283*Power(t2,5)) + 
               s2*Power(t1,3)*
                (28*Power(t1,5) + 152*Power(t1,4)*t2 + 
                  168*Power(t1,3)*Power(t2,2) - 
                  380*Power(t1,2)*Power(t2,3) - 133*t1*Power(t2,4) + 
                  387*Power(t2,5)) - 
               Power(s2,2)*Power(t1,2)*
                (116*Power(t1,5) + 385*Power(t1,4)*t2 + 
                  313*Power(t1,3)*Power(t2,2) - 
                  789*Power(t1,2)*Power(t2,3) - 240*t1*Power(t2,4) + 
                  602*Power(t2,5))) + 
            t2*(Power(s2,8)*t1*t2*(-t1 + t2) + 
               2*Power(t1,5)*Power(t1 - t2,2)*Power(t2,2)*
                (20*Power(t1,2) + 29*t1*t2 + 9*Power(t2,2)) + 
               Power(s2,6)*t1*
                (-24*Power(t1,4) + 273*Power(t1,3)*t2 - 
                  421*Power(t1,2)*Power(t2,2) + 330*t1*Power(t2,3) - 
                  101*Power(t2,4)) + 
               Power(s2,7)*(2*Power(t1,4) - 21*Power(t1,3)*t2 + 
                  53*Power(t1,2)*Power(t2,2) - 66*t1*Power(t2,3) + 
                  20*Power(t2,4)) + 
               Power(s2,3)*Power(t1,2)*
                (185*Power(t1,6) - 1028*Power(t1,5)*t2 - 
                  89*Power(t1,4)*Power(t2,2) + 
                  1216*Power(t1,3)*Power(t2,3) - 
                  344*Power(t1,2)*Power(t2,4) + 163*t1*Power(t2,5) - 
                  123*Power(t2,6)) + 
               s2*Power(t1,4)*
                (9*Power(t1,6) - 28*Power(t1,5)*t2 - 
                  285*Power(t1,4)*Power(t2,2) + 
                  281*Power(t1,3)*Power(t2,3) + 
                  174*Power(t1,2)*Power(t2,4) - 81*t1*Power(t2,5) - 
                  70*Power(t2,6)) + 
               Power(s2,5)*(104*Power(t1,6) - 938*Power(t1,5)*t2 + 
                  996*Power(t1,4)*Power(t2,2) - 
                  477*Power(t1,3)*Power(t2,3) + 
                  158*Power(t1,2)*Power(t2,4) + 79*t1*Power(t2,5) - 
                  20*Power(t2,6)) + 
               Power(s2,4)*t1*
                (-201*Power(t1,6) + 1418*Power(t1,5)*t2 - 
                  869*Power(t1,4)*Power(t2,2) - 
                  253*Power(t1,3)*Power(t2,3) + 
                  144*Power(t1,2)*Power(t2,4) - 239*t1*Power(t2,5) + 
                  73*Power(t2,6)) + 
               Power(s2,2)*Power(t1,3)*
                (-75*Power(t1,6) + 325*Power(t1,5)*t2 + 
                  576*Power(t1,4)*Power(t2,2) - 
                  1001*Power(t1,3)*Power(t2,3) + 
                  3*Power(t1,2)*Power(t2,4) + 52*t1*Power(t2,5) + 
                  120*Power(t2,6))) + 
            Power(s1,2)*(Power(s2,8)*t1*t2 + 
               Power(s2,7)*(-4*Power(t1,3) + 25*Power(t1,2)*t2 - 
                  76*t1*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s2,6)*(38*Power(t1,4) - 125*Power(t1,3)*t2 + 
                  230*Power(t1,2)*Power(t2,2) + 13*t1*Power(t2,3) - 
                  48*Power(t2,4)) - 
               Power(s2,5)*(128*Power(t1,5) - 254*Power(t1,4)*t2 + 
                  101*Power(t1,3)*Power(t2,2) + 
                  325*Power(t1,2)*Power(t2,3) - 592*t1*Power(t2,4) + 
                  58*Power(t2,5)) + 
               2*Power(t1,4)*t2*
                (-48*Power(t1,5) + 103*Power(t1,4)*t2 - 
                  74*Power(t1,3)*Power(t2,2) - 
                  68*Power(t1,2)*Power(t2,3) + 28*t1*Power(t2,4) + 
                  59*Power(t2,5)) + 
               s2*Power(t1,3)*
                (-8*Power(t1,6) + 481*Power(t1,5)*t2 - 
                  1199*Power(t1,4)*Power(t2,2) + 
                  1058*Power(t1,3)*Power(t2,3) + 
                  559*Power(t1,2)*Power(t2,4) - 235*t1*Power(t2,5) - 
                  440*Power(t2,6)) + 
               Power(s2,3)*t1*
                (-172*Power(t1,6) + 767*Power(t1,5)*t2 - 
                  1421*Power(t1,4)*Power(t2,2) + 
                  798*Power(t1,3)*Power(t2,3) + 
                  1854*Power(t1,2)*Power(t2,4) - 327*t1*Power(t2,5) - 
                  255*Power(t2,6)) + 
               Power(s2,4)*(208*Power(t1,6) - 437*Power(t1,5)*t2 + 
                  223*Power(t1,4)*Power(t2,2) + 
                  431*Power(t1,3)*Power(t2,3) - 
                  1698*Power(t1,2)*Power(t2,4) + 119*t1*Power(t2,5) + 
                  20*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*
                (66*Power(t1,6) - 871*Power(t1,5)*t2 + 
                  2144*Power(t1,4)*Power(t2,2) - 
                  1864*Power(t1,3)*Power(t2,3) - 
                  1145*Power(t1,2)*Power(t2,4) + 406*t1*Power(t2,5) + 
                  565*Power(t2,6))) + 
            s1*(Power(s2,8)*t1*(t1 - 2*t2)*t2 - 
               Power(s2,7)*t2*
                (4*Power(t1,3) + 26*Power(t1,2)*t2 - 
                  111*t1*Power(t2,2) + 44*Power(t2,3)) + 
               Power(s2,6)*(-4*Power(t1,5) + 31*Power(t1,4)*t2 + 
                  88*Power(t1,3)*Power(t2,2) - 
                  373*Power(t1,2)*Power(t2,3) + 131*t1*Power(t2,4) + 
                  24*Power(t2,5)) + 
               2*Power(t1,4)*t2*
                (9*Power(t1,6) - 15*Power(t1,5)*t2 - 
                  65*Power(t1,4)*Power(t2,2) + 
                  106*Power(t1,3)*Power(t2,3) + 
                  31*Power(t1,2)*Power(t2,4) - 41*t1*Power(t2,5) - 
                  25*Power(t2,6)) + 
               Power(s2,5)*(18*Power(t1,6) - 237*Power(t1,5)*t2 + 
                  529*Power(t1,4)*Power(t2,2) - 
                  163*Power(t1,3)*Power(t2,3) + 
                  Power(t1,2)*Power(t2,4) - 397*t1*Power(t2,5) + 
                  60*Power(t2,6)) + 
               s2*Power(t1,3)*t2*
                (-233*Power(t1,6) + 563*Power(t1,5)*t2 + 
                  230*Power(t1,4)*Power(t2,2) - 
                  945*Power(t1,3)*Power(t2,3) - 
                  201*Power(t1,2)*Power(t2,4) + 312*t1*Power(t2,5) + 
                  158*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*
                (-4*Power(t1,7) + 772*Power(t1,6)*t2 - 
                  2186*Power(t1,5)*Power(t2,2) + 
                  977*Power(t1,4)*Power(t2,3) + 
                  1082*Power(t1,3)*Power(t2,4) + 
                  480*Power(t1,2)*Power(t2,5) - 500*t1*Power(t2,6) - 
                  187*Power(t2,7)) - 
               Power(s2,4)*(28*Power(t1,7) - 747*Power(t1,6)*t2 + 
                  2320*Power(t1,5)*Power(t2,2) - 
                  1978*Power(t1,4)*Power(t2,3) + 
                  564*Power(t1,3)*Power(t2,4) - 
                  1126*Power(t1,2)*Power(t2,5) + 211*t1*Power(t2,6) + 
                  8*Power(t2,7)) + 
               Power(s2,3)*t1*
                (18*Power(t1,7) - 1095*Power(t1,6)*t2 + 
                  3383*Power(t1,5)*Power(t2,2) - 
                  2643*Power(t1,4)*Power(t2,3) + 
                  128*Power(t1,3)*Power(t2,4) - 
                  1079*Power(t1,2)*Power(t2,5) + 442*t1*Power(t2,6) + 
                  84*Power(t2,7)))) + 
         Power(s,4)*(-(Power(s1,6)*Power(s2 - t1,2)*t1*
               (4*Power(s2,4) - 2*Power(s2,3)*(7*t1 - 6*t2) - 
                 3*Power(t1,2)*Power(t2,2) + 
                 s2*t1*(-6*Power(t1,2) + 19*t1*t2 - 11*Power(t2,2)) + 
                 Power(s2,2)*(16*Power(t1,2) - 31*t1*t2 + 8*Power(t2,2)))\
) - Power(s1,5)*(s2 - t1)*(8*Power(s2,6)*t1 - 
               3*Power(t1,4)*t2*
                (10*Power(t1,2) + 3*t1*t2 - 7*Power(t2,2)) - 
               2*Power(s2,5)*(25*Power(t1,2) - 19*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*Power(t1,2)*
                (84*Power(t1,3) - 229*Power(t1,2)*t2 + 
                  71*t1*Power(t2,2) - 46*Power(t2,3)) + 
               s2*Power(t1,3)*
                (-18*Power(t1,3) + 110*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) - 11*Power(t2,3)) + 
               2*Power(s2,4)*
                (62*Power(t1,3) - 87*Power(t1,2)*t2 + 
                  24*t1*Power(t2,2) - Power(t2,3)) + 
               Power(s2,3)*t1*
                (-148*Power(t1,3) + 285*Power(t1,2)*t2 - 
                  147*t1*Power(t2,2) + 16*Power(t2,3))) + 
            Power(s1,4)*(-2*Power(s2,8)*t1 + 
               Power(s2,7)*(18*Power(t1,2) - 17*t1*t2 + 
                  10*Power(t2,2)) + 
               Power(t1,5)*t2*
                (52*Power(t1,3) - 5*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) - 25*Power(t2,3)) + 
               Power(s2,6)*(-68*Power(t1,3) + 71*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) - 6*Power(t2,3)) + 
               Power(s2,3)*Power(t1,2)*
                (12*Power(t1,4) - 390*Power(t1,3)*t2 - 
                  362*Power(t1,2)*Power(t2,2) + 1084*t1*Power(t2,3) - 
                  561*Power(t2,4)) + 
               s2*Power(t1,4)*
                (-12*Power(t1,4) - 262*Power(t1,3)*t2 + 
                  33*Power(t1,2)*Power(t2,2) + 52*t1*Power(t2,3) - 
                  139*Power(t2,4)) + 
               Power(s2,5)*(122*Power(t1,4) - 114*Power(t1,3)*t2 - 
                  207*Power(t1,2)*Power(t2,2) + 239*t1*Power(t2,3) - 
                  16*Power(t2,4)) + 
               Power(s2,4)*t1*
                (-98*Power(t1,4) + 187*Power(t1,3)*t2 + 
                  418*Power(t1,2)*Power(t2,2) - 872*t1*Power(t2,3) + 
                  171*Power(t2,4)) + 
               Power(s2,2)*Power(t1,3)*
                (28*Power(t1,4) + 473*Power(t1,3)*t2 + 
                  84*Power(t1,2)*Power(t2,2) - 513*t1*Power(t2,3) + 
                  564*Power(t2,4))) + 
            Power(s1,3)*(-2*Power(s2,8)*
                (Power(t1,2) + 5*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,7)*(18*Power(t1,3) - 3*Power(t1,2)*t2 + 
                  45*t1*Power(t2,2) - 46*Power(t2,3)) + 
               Power(t1,5)*t2*
                (52*Power(t1,4) - 130*Power(t1,3)*t2 + 
                  124*Power(t1,2)*Power(t2,2) + 21*t1*Power(t2,3) - 
                  87*Power(t2,4)) + 
               Power(s2,6)*(-82*Power(t1,4) + 194*Power(t1,3)*t2 - 
                  308*Power(t1,2)*Power(t2,2) + 219*t1*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s2,2)*Power(t1,3)*
                (-88*Power(t1,5) + 91*Power(t1,4)*t2 - 
                  784*Power(t1,3)*Power(t2,2) + 
                  1178*Power(t1,2)*Power(t2,3) + 877*t1*Power(t2,4) - 
                  1325*Power(t2,5)) + 
               Power(s2,4)*t1*
                (-292*Power(t1,5) + 426*Power(t1,4)*t2 - 
                  850*Power(t1,3)*Power(t2,2) + 
                  1066*Power(t1,2)*Power(t2,3) + 1140*t1*Power(t2,4) - 
                  363*Power(t2,5)) + 
               Power(s2,5)*(206*Power(t1,5) - 482*Power(t1,4)*t2 + 
                  674*Power(t1,3)*Power(t2,2) - 
                  626*Power(t1,2)*Power(t2,3) - 295*t1*Power(t2,4) + 
                  38*Power(t2,5)) + 
               s2*Power(t1,4)*
                (12*Power(t1,5) - 133*Power(t1,4)*t2 + 
                  500*Power(t1,3)*Power(t2,2) - 
                  623*Power(t1,2)*Power(t2,3) - 252*t1*Power(t2,4) + 
                  604*Power(t2,5)) + 
               Power(s2,3)*Power(t1,2)*
                (228*Power(t1,5) - 135*Power(t1,4)*t2 + 
                  847*Power(t1,3)*Power(t2,2) - 
                  1286*Power(t1,2)*Power(t2,3) - 1461*t1*Power(t2,4) + 
                  1151*Power(t2,5))) + 
            t2*(2*Power(t1,6)*Power(t1 - t2,2)*Power(t2,2)*
                (4*Power(t1,2) + 13*t1*t2 + 7*Power(t2,2)) + 
               2*Power(s2,8)*t2*
                (Power(t1,3) - 5*Power(t1,2)*t2 + 13*t1*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(s2,7)*(Power(t1,5) - 48*Power(t1,4)*t2 + 
                  145*Power(t1,3)*Power(t2,2) - 
                  200*Power(t1,2)*Power(t2,3) + 37*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               Power(s2,3)*Power(t1,3)*
                (29*Power(t1,6) - 297*Power(t1,5)*t2 - 
                  604*Power(t1,4)*Power(t2,2) + 
                  1087*Power(t1,3)*Power(t2,3) - 
                  445*Power(t1,2)*Power(t2,4) + 442*t1*Power(t2,5) - 
                  232*Power(t2,6)) + 
               Power(s2,5)*t1*
                (36*Power(t1,6) - 598*Power(t1,5)*t2 + 
                  479*Power(t1,4)*Power(t2,2) - 
                  97*Power(t1,3)*Power(t2,3) - 
                  332*Power(t1,2)*Power(t2,4) + 439*t1*Power(t2,5) - 
                  92*Power(t2,6)) + 
               s2*Power(t1,5)*
                (-2*Power(t1,6) - 5*Power(t1,5)*t2 - 
                  92*Power(t1,4)*Power(t2,2) + 
                  22*Power(t1,3)*Power(t2,3) + 
                  134*Power(t1,2)*Power(t2,4) + 23*t1*Power(t2,5) - 
                  80*Power(t2,6)) + 
               Power(s2,6)*(-11*Power(t1,6) + 272*Power(t1,5)*t2 - 
                  462*Power(t1,4)*Power(t2,2) + 
                  430*Power(t1,3)*Power(t2,3) + 
                  Power(t1,2)*Power(t2,4) - 111*t1*Power(t2,5) + 
                  20*Power(t2,6)) + 
               Power(s2,2)*Power(t1,4)*
                (-3*Power(t1,6) + 61*Power(t1,5)*t2 + 
                  402*Power(t1,4)*Power(t2,2) - 
                  433*Power(t1,3)*Power(t2,3) - 
                  69*Power(t1,2)*Power(t2,4) - 138*t1*Power(t2,5) + 
                  180*Power(t2,6)) + 
               Power(s2,4)*Power(t1,2)*
                (-50*Power(t1,6) + 613*Power(t1,5)*t2 + 
                  134*Power(t1,4)*Power(t2,2) - 
                  843*Power(t1,3)*Power(t2,3) + 
                  715*Power(t1,2)*Power(t2,4) - 664*t1*Power(t2,5) + 
                  188*Power(t2,6))) + 
            Power(s1,2)*(-2*Power(s2,8)*t2*
                (5*Power(t1,2) - 20*t1*t2 + 11*Power(t2,2)) - 
               Power(s2,7)*(8*Power(t1,4) - 38*Power(t1,3)*t2 + 
                  173*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 
                  72*Power(t2,4)) + 
               s2*Power(t1,4)*t2*
                (295*Power(t1,5) - 663*Power(t1,4)*t2 + 
                  258*Power(t1,3)*Power(t2,2) + 
                  793*Power(t1,2)*Power(t2,3) + 163*t1*Power(t2,4) - 
                  688*Power(t2,5)) + 
               Power(s2,6)*(46*Power(t1,5) - 66*Power(t1,4)*t2 + 
                  170*Power(t1,3)*Power(t2,2) + 
                  360*Power(t1,2)*Power(t2,3) - 489*t1*Power(t2,4) + 
                  58*Power(t2,5)) + 
               2*Power(t1,5)*t2*
                (-15*Power(t1,5) + 19*Power(t1,4)*t2 + 
                  28*Power(t1,3)*Power(t2,2) - 
                  97*Power(t1,2)*Power(t2,3) - 10*t1*Power(t2,4) + 
                  75*Power(t2,5)) + 
               Power(s2,3)*Power(t1,2)*
                (-66*Power(t1,6) + 1006*Power(t1,5)*t2 - 
                  2389*Power(t1,4)*Power(t2,2) + 
                  1273*Power(t1,3)*Power(t2,3) + 
                  1887*Power(t1,2)*Power(t2,4) + 698*t1*Power(t2,5) - 
                  1002*Power(t2,6)) + 
               Power(s2,5)*(-106*Power(t1,6) + 187*Power(t1,5)*t2 - 
                  145*Power(t1,4)*Power(t2,2) - 
                  756*Power(t1,3)*Power(t2,3) + 
                  1522*Power(t1,2)*Power(t2,4) + 29*t1*Power(t2,5) - 
                  36*Power(t2,6)) + 
               Power(s2,4)*t1*
                (120*Power(t1,6) - 598*Power(t1,5)*t2 + 
                  1080*Power(t1,4)*Power(t2,2) + 
                  106*Power(t1,3)*Power(t2,3) - 
                  2181*Power(t1,2)*Power(t2,4) - 477*t1*Power(t2,5) + 
                  325*Power(t2,6)) + 
               Power(s2,2)*Power(t1,3)*
                (14*Power(t1,6) - 822*Power(t1,5)*t2 + 
                  2044*Power(t1,4)*Power(t2,2) - 
                  1272*Power(t1,3)*Power(t2,3) - 
                  1410*Power(t1,2)*Power(t2,4) - 473*t1*Power(t2,5) + 
                  1233*Power(t2,6))) + 
            s1*(2*Power(s2,8)*t2*
                (Power(t1,3) + 5*Power(t1,2)*t2 - 27*t1*Power(t2,2) + 
                  13*Power(t2,3)) - 
               Power(s2,7)*t2*
                (17*Power(t1,4) + 33*Power(t1,3)*t2 - 
                  324*Power(t1,2)*Power(t2,2) + 56*t1*Power(t2,3) + 
                  46*Power(t2,4)) + 
               2*Power(t1,5)*Power(t2,2)*
                (3*Power(t1,5) - 40*Power(t1,4)*t2 + 
                  24*Power(t1,3)*Power(t2,2) + 
                  53*Power(t1,2)*Power(t2,3) - 9*t1*Power(t2,4) - 
                  31*Power(t2,5)) + 
               Power(s2,2)*Power(t1,3)*t2*
                (239*Power(t1,6) - 752*Power(t1,5)*t2 - 
                  488*Power(t1,4)*Power(t2,2) + 
                  1125*Power(t1,3)*Power(t2,3) + 
                  704*Power(t1,2)*Power(t2,4) - 178*t1*Power(t2,5) - 
                  410*Power(t2,6)) - 
               Power(s2,6)*(2*Power(t1,6) - 84*Power(t1,5)*t2 + 
                  228*Power(t1,4)*Power(t2,2) + 
                  367*Power(t1,3)*Power(t2,3) + 
                  214*Power(t1,2)*Power(t2,4) - 396*t1*Power(t2,5) + 
                  60*Power(t2,6)) + 
               s2*Power(t1,4)*t2*
                (-37*Power(t1,6) + 57*Power(t1,5)*t2 + 
                  480*Power(t1,4)*Power(t2,2) - 
                  513*Power(t1,3)*Power(t2,3) - 
                  379*Power(t1,2)*Power(t2,4) + 94*t1*Power(t2,5) + 
                  250*Power(t2,6)) + 
               Power(s2,5)*(6*Power(t1,7) - 287*Power(t1,6)*t2 + 
                  1322*Power(t1,5)*Power(t2,2) - 
                  670*Power(t1,4)*Power(t2,3) + 
                  955*Power(t1,3)*Power(t2,4) - 
                  1372*Power(t1,2)*Power(t2,5) + 181*t1*Power(t2,6) + 
                  12*Power(t2,7)) - 
               Power(s2,4)*t1*
                (6*Power(t1,7) - 545*Power(t1,6)*t2 + 
                  2493*Power(t1,5)*Power(t2,2) - 
                  1735*Power(t1,4)*Power(t2,3) + 
                  1049*Power(t1,3)*Power(t2,4) - 
                  1926*Power(t1,2)*Power(t2,5) + 229*t1*Power(t2,6) + 
                  107*Power(t2,7)) + 
               Power(s2,3)*Power(t1,2)*
                (2*Power(t1,7) - 529*Power(t1,6)*t2 + 
                  2111*Power(t1,5)*Power(t2,2) - 
                  884*Power(t1,4)*Power(t2,3) - 
                  326*Power(t1,3)*Power(t2,4) - 
                  1338*Power(t1,2)*Power(t2,5) + 222*t1*Power(t2,6) + 
                  323*Power(t2,7)))))*Log(-s2))/
     (s*s1*(s - s2)*s2*Power(s2 - t1,2)*Power(t1,2)*Power(s + t1,2)*
       Power(s - s2 + t1,2)*(s1 + t1 - t2)*Power(t2,2)*Power(s - s1 + t2,2)*
       (s2 - t1 + t2)) - (8*(Power(s1,2)*Power(s2,2)*(s2 - t1)*
          Power(t1 - t2,2)*t2*(s2 + t2)*(s2 - t1 + t2)*
          Power(s1*(-s2 + t1) + s2*t2,2)*
          (Power(s1,2)*(s2 - t1) - s2*(t1 - t2)*(2*s2 - 2*t1 + t2) - 
            s1*(2*s2 - t1)*(s2 - t1 + t2)) + 
         Power(s,6)*Power(t1 - t2,2)*t2*
          (2*s2*Power(s2 - t1,2)*t1*Power(t1 - t2,2)*(s2 + t2)*
             (s2 - t1 + t2) + 
            Power(s1,2)*(-2*Power(t1,3)*Power(t1 - t2,2)*t2 + 
               Power(s2,5)*(5*t1 + t2) - 
               s2*Power(t1,2)*t2*
                (3*Power(t1,2) + t1*t2 - 4*Power(t2,2)) + 
               Power(s2,4)*(-17*Power(t1,2) + 9*t1*t2 + 
                  2*Power(t2,2)) + 
               Power(s2,3)*(22*Power(t1,3) - 29*Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) + Power(t2,3)) - 
               Power(s2,2)*t1*
                (9*Power(t1,3) - 25*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
                  2*Power(t2,3))) + 
            s1*t1*(7*Power(s2,5)*(t1 - t2) - 
               2*Power(t1,2)*Power(t1 - t2,3)*t2 - 
               s2*t1*Power(t1 - t2,2)*t2*(5*t1 + 3*t2) + 
               Power(s2,4)*(-23*Power(t1,2) + 39*t1*t2 - 
                  16*Power(t2,2)) + 
               Power(s2,3)*(28*Power(t1,3) - 70*Power(t1,2)*t2 + 
                  51*t1*Power(t2,2) - 9*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-11*Power(t1,3) + 45*Power(t1,2)*t2 - 
                  48*t1*Power(t2,2) + 14*Power(t2,3)))) - 
         Power(s,5)*(t1 - t2)*
          (2*s2*Power(s2 - t1,2)*t1*Power(t1 - t2,2)*t2*
             (2*Power(s2,3)*(t1 - 2*t2) + 
               s2*(t1 - 2*t2)*Power(t1 - t2,2) + 
               Power(t1 - t2,2)*t2*(t1 + t2) + 
               Power(s2,2)*(-3*Power(t1,2) + 8*t1*t2 - 7*Power(t2,2))) + 
            s1*(t1 - t2)*t2*(4*Power(t1,4)*Power(t1 - t2,3)*t2 + 
               Power(s2,6)*(22*Power(t1,2) - 27*t1*t2 + 
                  4*Power(t2,2)) - 
               s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                (19*Power(t1,2) - 45*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,4)*t1*
                (72*Power(t1,3) - 152*Power(t1,2)*t2 + 
                  86*t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,5)*(-68*Power(t1,3) + 117*Power(t1,2)*t2 - 
                  54*t1*Power(t2,2) + 6*Power(t2,3)) - 
               Power(s2,2)*t1*
                (Power(t1,5) - 36*Power(t1,4)*t2 + 
                  156*Power(t1,3)*Power(t2,2) - 
                  200*Power(t1,2)*Power(t2,3) + 87*t1*Power(t2,4) - 
                  8*Power(t2,5)) + 
               Power(s2,3)*(-25*Power(t1,5) + 41*Power(t1,4)*t2 + 
                  53*Power(t1,3)*Power(t2,2) - 
                  98*Power(t1,2)*Power(t2,3) + 30*t1*Power(t2,4) - 
                  2*Power(t2,5))) + 
            Power(s1,3)*(-2*Power(t1,3)*Power(t1 - t2,2)*(3*t1 - t2)*
                Power(t2,2) + Power(s2,6)*t2*(7*t1 + 9*t2) + 
               Power(s2,5)*t2*
                (3*Power(t1,2) - 36*t1*t2 + 23*Power(t2,2)) + 
               s2*t1*Power(t2,2)*
                (-16*Power(t1,4) + 33*Power(t1,3)*t2 - 
                  13*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,4)*(-2*Power(t1,4) - 56*Power(t1,3)*t2 + 
                  110*Power(t1,2)*Power(t2,2) - 93*t1*Power(t2,3) + 
                  19*Power(t2,4)) + 
               Power(s2,3)*(4*Power(t1,5) + 77*Power(t1,4)*t2 - 
                  193*Power(t1,3)*Power(t2,2) + 
                  171*Power(t1,2)*Power(t2,3) - 50*t1*Power(t2,4) + 
                  Power(t2,5)) - 
               2*Power(s2,2)*
                (Power(t1,6) + 15*Power(t1,5)*t2 - 
                  65*Power(t1,4)*Power(t2,2) + 
                  76*Power(t1,3)*Power(t2,3) - 
                  28*Power(t1,2)*Power(t2,4) - 4*t1*Power(t2,5) + 
                  2*Power(t2,6))) + 
            Power(s1,2)*(-2*Power(t1,3)*Power(t1 - t2,4)*Power(t2,2) + 
               Power(s2,6)*t2*
                (25*Power(t1,2) - 12*t1*t2 - 7*Power(t2,2)) + 
               Power(s2,5)*t2*
                (-51*Power(t1,3) + 26*Power(t1,2)*t2 + 
                  38*t1*Power(t2,2) - 19*Power(t2,3)) - 
               s2*t1*Power(t1 - t2,2)*Power(t2,2)*
                (37*Power(t1,3) - 65*Power(t1,2)*t2 + 4*Power(t2,3)) + 
               Power(s2,4)*(-2*Power(t1,5) + 
                  92*Power(t1,3)*Power(t2,2) - 
                  205*Power(t1,2)*Power(t2,3) + 126*t1*Power(t2,4) - 
                  17*Power(t2,5)) + 
               Power(s2,3)*(4*Power(t1,6) + 58*Power(t1,5)*t2 - 
                  283*Power(t1,4)*Power(t2,2) + 
                  509*Power(t1,3)*Power(t2,3) - 
                  357*Power(t1,2)*Power(t2,4) + 76*t1*Power(t2,5) - 
                  Power(t2,6)) - 
               Power(s2,2)*(2*Power(t1,7) + 31*Power(t1,6)*t2 - 
                  213*Power(t1,5)*Power(t2,2) + 
                  480*Power(t1,4)*Power(t2,3) - 
                  435*Power(t1,3)*Power(t2,4) + 
                  131*Power(t1,2)*Power(t2,5) + 8*t1*Power(t2,6) - 
                  4*Power(t2,7)))) + 
         Power(s,4)*(2*s2*Power(s2 - t1,2)*t1*Power(t1 - t2,2)*t2*
             (s2*(t1 - 4*t2)*Power(t1 - t2,2)*Power(t2,2) - 
               t1*Power(t1 - t2,3)*Power(t2,2) + 
               Power(s2,4)*(Power(t1,2) - 6*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,3)*(-2*Power(t1,3) + 10*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(Power(t1,4) - 4*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) - t1*Power(t2,3) - 
                  2*Power(t2,4))) - 
            Power(s1,4)*(4*Power(s2,7)*(t1 - t2)*t2 + 
               2*Power(t1,4)*(3*t1 - 2*t2)*Power(t1 - t2,2)*
                Power(t2,2) + 
               Power(s2,6)*t2*
                (-15*Power(t1,2) + 8*t1*t2 + 11*Power(t2,2)) + 
               Power(s2,5)*(-4*Power(t1,4) - 12*Power(t1,3)*t2 + 
                  76*Power(t1,2)*Power(t2,2) - 107*t1*Power(t2,3) + 
                  45*Power(t2,4)) + 
               s2*t1*Power(t2,2)*
                (32*Power(t1,5) - 95*Power(t1,4)*t2 + 
                  86*Power(t1,3)*Power(t2,2) - 
                  12*Power(t1,2)*Power(t2,3) - 15*t1*Power(t2,4) + 
                  4*Power(t2,5)) + 
               Power(s2,4)*(14*Power(t1,5) + 86*Power(t1,4)*t2 - 
                  296*Power(t1,3)*Power(t2,2) + 
                  359*Power(t1,2)*Power(t2,3) - 214*t1*Power(t2,4) + 
                  45*Power(t2,5)) + 
               Power(s2,3)*(-16*Power(t1,6) - 95*Power(t1,5)*t2 + 
                  414*Power(t1,4)*Power(t2,2) - 
                  564*Power(t1,3)*Power(t2,3) + 
                  359*Power(t1,2)*Power(t2,4) - 107*t1*Power(t2,5) + 
                  11*Power(t2,6)) + 
               Power(s2,2)*(6*Power(t1,7) + 32*Power(t1,6)*t2 - 
                  234*Power(t1,5)*Power(t2,2) + 
                  414*Power(t1,4)*Power(t2,3) - 
                  296*Power(t1,3)*Power(t2,4) + 
                  76*Power(t1,2)*Power(t2,5) + 8*t1*Power(t2,6) - 
                  4*Power(t2,7))) - 
            s1*(t1 - t2)*t2*(2*Power(t1,5)*Power(t1 - t2,4)*t2 - 
               s2*Power(t1,2)*Power(t1 - t2,3)*t2*
                (29*Power(t1,3) - 39*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,7)*(-24*Power(t1,3) + 62*Power(t1,2)*t2 - 
                  55*t1*Power(t2,2) + 16*Power(t2,3)) + 
               Power(s2,6)*(79*Power(t1,4) - 204*Power(t1,3)*t2 + 
                  203*Power(t1,2)*Power(t2,2) - 103*t1*Power(t2,3) + 
                  26*Power(t2,4)) - 
               Power(s2,2)*t1*Power(t1 - t2,2)*
                (15*Power(t1,5) - 110*Power(t1,4)*t2 + 
                  249*Power(t1,3)*Power(t2,2) - 
                  159*Power(t1,2)*Power(t2,3) - 2*t1*Power(t2,4) + 
                  4*Power(t2,5)) + 
               Power(s2,5)*(-82*Power(t1,5) + 164*Power(t1,4)*t2 - 
                  57*Power(t1,3)*Power(t2,2) - 
                  44*Power(t1,2)*Power(t2,3) + 14*t1*Power(t2,4) + 
                  6*Power(t2,5)) + 
               Power(s2,4)*(8*Power(t1,6) + 124*Power(t1,5)*t2 - 
                  560*Power(t1,4)*Power(t2,2) + 
                  782*Power(t1,3)*Power(t2,3) - 
                  428*Power(t1,2)*Power(t2,4) + 75*t1*Power(t2,5) - 
                  2*Power(t2,6)) + 
               Power(s2,3)*(34*Power(t1,7) - 259*Power(t1,6)*t2 + 
                  837*Power(t1,5)*Power(t2,2) - 
                  1229*Power(t1,4)*Power(t2,3) + 
                  828*Power(t1,3)*Power(t2,4) - 
                  222*Power(t1,2)*Power(t2,5) + 9*t1*Power(t2,6) + 
                  2*Power(t2,7))) + 
            Power(s1,2)*(2*Power(t1,4)*(5*t1 - 2*t2)*Power(t1 - t2,4)*
                Power(t2,2) + 
               Power(s2,7)*t2*
                (38*Power(t1,3) - 46*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 7*Power(t2,3)) - 
               s2*t1*Power(t1 - t2,3)*Power(t2,2)*
                (17*Power(t1,4) - 60*Power(t1,3)*t2 + 
                  86*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,6)*t2*
                (-85*Power(t1,4) - 25*Power(t1,3)*t2 + 
                  284*Power(t1,2)*Power(t2,2) - 209*t1*Power(t2,3) + 
                  33*Power(t2,4)) + 
               Power(s2,5)*(-4*Power(t1,6) + 12*Power(t1,5)*t2 + 
                  518*Power(t1,4)*Power(t2,2) - 
                  1431*Power(t1,3)*Power(t2,3) + 
                  1318*Power(t1,2)*Power(t2,4) - 449*t1*Power(t2,5) + 
                  34*Power(t2,6)) + 
               Power(s2,4)*(10*Power(t1,7) + 98*Power(t1,6)*t2 - 
                  898*Power(t1,5)*Power(t2,2) + 
                  2427*Power(t1,4)*Power(t2,3) - 
                  2758*Power(t1,3)*Power(t2,4) + 
                  1316*Power(t1,2)*Power(t2,5) - 178*t1*Power(t2,6) - 
                  15*Power(t2,7)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (2*Power(t1,7) + 21*Power(t1,6)*t2 - 
                  59*Power(t1,5)*Power(t2,2) + 
                  105*Power(t1,4)*Power(t2,3) + 
                  62*Power(t1,3)*Power(t2,4) - 
                  141*Power(t1,2)*Power(t2,5) + 18*t1*Power(t2,6) + 
                  4*Power(t2,7)) + 
               Power(s2,3)*(-8*Power(t1,8) - 80*Power(t1,7)*t2 + 
                  559*Power(t1,6)*Power(t2,2) - 
                  1594*Power(t1,5)*Power(t2,3) + 
                  2084*Power(t1,4)*Power(t2,4) - 
                  1150*Power(t1,3)*Power(t2,5) + 
                  119*Power(t1,2)*Power(t2,6) + 89*t1*Power(t2,7) - 
                  19*Power(t2,8))) + 
            Power(s1,3)*(6*Power(t1,5)*Power(t1 - t2,3)*Power(t2,2) + 
               2*Power(s2,7)*t2*
                (6*Power(t1,2) + 9*t1*t2 - 13*Power(t2,2)) + 
               Power(s2,6)*t2*
                (Power(t1,3) - 172*Power(t1,2)*t2 + 
                  217*t1*Power(t2,2) - 50*Power(t2,3)) - 
               2*s2*t1*Power(t1 - t2,2)*Power(t2,2)*
                (39*Power(t1,4) - 85*Power(t1,3)*t2 + 
                  48*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,5)*t2*
                (-54*Power(t1,4) + 372*Power(t1,3)*t2 - 
                  575*Power(t1,2)*Power(t2,2) + 255*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,4)*(-4*Power(t1,6) + 36*Power(t1,5)*t2 - 
                  148*Power(t1,4)*Power(t2,2) + 
                  366*Power(t1,3)*Power(t2,3) - 
                  207*Power(t1,2)*Power(t2,4) - 97*t1*Power(t2,5) + 
                  58*Power(t2,6)) + 
               Power(s2,3)*(8*Power(t1,7) + 27*Power(t1,6)*t2 - 
                  319*Power(t1,5)*Power(t2,2) + 
                  613*Power(t1,4)*Power(t2,3) - 
                  591*Power(t1,3)*Power(t2,4) + 
                  417*Power(t1,2)*Power(t2,5) - 183*t1*Power(t2,6) + 
                  28*Power(t2,7)) + 
               Power(s2,2)*(-4*Power(t1,8) - 22*Power(t1,7)*t2 + 
                  321*Power(t1,6)*Power(t2,2) - 
                  899*Power(t1,5)*Power(t2,3) + 
                  1097*Power(t1,4)*Power(t2,4) - 
                  688*Power(t1,3)*Power(t2,5) + 
                  201*Power(t1,2)*Power(t2,6) + 2*t1*Power(t2,7) - 
                  8*Power(t2,8)))) + 
         s*(2*Power(s2,4)*Power(s2 - t1,3)*t1*Power(t1 - t2,2)*
             Power(t2,4)*(s2 + t2)*(s2 - t1 + t2) + 
            Power(s1,6)*s2*Power(s2 - t1,3)*
             (t1*Power(t2,2)*
                (7*Power(t1,3) - 16*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,3)*(2*Power(t1,3) - 5*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*(-4*Power(t1,4) + 4*Power(t1,3)*t2 - 
                  2*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               s2*(2*Power(t1,5) + Power(t1,4)*t2 - 
                  16*Power(t1,3)*Power(t2,2) + 
                  16*Power(t1,2)*Power(t2,3) - 2*t1*Power(t2,4) - 
                  2*Power(t2,5))) + 
            Power(s1,5)*Power(s2 - t1,2)*
             (4*Power(s2,7)*(t1 - t2)*t2 + 
               2*Power(t1,5)*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s2,6)*(2*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 3*Power(t2,3)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (-19*Power(t1,4) + 60*Power(t1,3)*t2 - 
                  72*Power(t1,2)*Power(t2,2) + 41*t1*Power(t2,3) - 
                  11*Power(t2,4)) + 
               Power(s2,5)*(-4*Power(t1,4) + 8*Power(t1,3)*t2 + 
                  23*Power(t1,2)*Power(t2,2) - 67*t1*Power(t2,3) + 
                  35*Power(t2,4)) + 
               Power(s2,4)*t2*
                (9*Power(t1,4) - 44*Power(t1,3)*t2 + 
                  125*Power(t1,2)*Power(t2,2) - 145*t1*Power(t2,3) + 
                  49*Power(t2,4)) - 
               Power(s2,2)*t1*
                (2*Power(t1,6) + Power(t1,5)*t2 - 
                  54*Power(t1,4)*Power(t2,2) + 
                  127*Power(t1,3)*Power(t2,3) - 
                  108*Power(t1,2)*Power(t2,4) + 32*t1*Power(t2,5) - 
                  3*Power(t2,6)) + 
               Power(s2,3)*(4*Power(t1,6) - 6*Power(t1,5)*t2 - 
                  24*Power(t1,4)*Power(t2,2) + 
                  23*Power(t1,3)*Power(t2,3) + 
                  59*Power(t1,2)*Power(t2,4) - 71*t1*Power(t2,5) + 
                  21*Power(t2,6))) + 
            s1*Power(s2,2)*(s2 - t1)*Power(t2,2)*
             (2*Power(t1,3)*Power(t1 - t2,3)*(3*t1 - t2)*Power(t2,3) + 
               Power(s2,7)*(Power(t1,3) - 4*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,6)*(-4*Power(t1,4) + 7*Power(t1,3)*t2 - 
                  18*Power(t1,2)*Power(t2,2) + 21*t1*Power(t2,3) - 
                  8*Power(t2,4)) + 
               s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                (2*Power(t1,4) - 13*Power(t1,3)*t2 - 
                  4*Power(t1,2)*Power(t2,2) + 16*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               3*Power(s2,5)*
                (2*Power(t1,5) + 5*Power(t1,4)*t2 - 
                  11*Power(t1,3)*Power(t2,2) - 
                  Power(t1,2)*Power(t2,3) + 7*t1*Power(t2,4) - 
                  2*Power(t2,5)) - 
               Power(s2,4)*(4*Power(t1,6) + 53*Power(t1,5)*t2 - 
                  155*Power(t1,4)*Power(t2,2) + 
                  114*Power(t1,3)*Power(t2,3) + 
                  3*Power(t1,2)*Power(t2,4) - 25*t1*Power(t2,5) + 
                  4*Power(t2,6)) + 
               Power(s2,2)*t1*t2*
                (-22*Power(t1,6) + 109*Power(t1,5)*t2 - 
                  158*Power(t1,4)*Power(t2,2) + 
                  56*Power(t1,3)*Power(t2,3) + 
                  44*Power(t1,2)*Power(t2,4) - 35*t1*Power(t2,5) + 
                  6*Power(t2,6)) + 
               Power(s2,3)*(Power(t1,7) + 55*Power(t1,6)*t2 - 
                  204*Power(t1,5)*Power(t2,2) + 
                  225*Power(t1,4)*Power(t2,3) - 
                  51*Power(t1,3)*Power(t2,4) - 
                  48*Power(t1,2)*Power(t2,5) + 23*t1*Power(t2,6) - 
                  2*Power(t2,7))) + 
            Power(s1,3)*s2*t2*
             (Power(s2,9)*Power(t1,2) + 
               Power(s2,8)*t1*
                (-7*Power(t1,2) + 13*t1*t2 - 3*Power(t2,2)) + 
               Power(t1,4)*Power(t1 - t2,2)*t2*
                (7*Power(t1,4) - 5*Power(t1,3)*t2 - 6*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,7)*(16*Power(t1,4) - 10*Power(t1,3)*t2 - 
                  84*Power(t1,2)*Power(t2,2) + 59*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,6)*(-19*Power(t1,5) - 117*Power(t1,4)*t2 + 
                  526*Power(t1,3)*Power(t2,2) - 
                  467*Power(t1,2)*Power(t2,3) + 62*t1*Power(t2,4) + 
                  29*Power(t2,5)) + 
               Power(s2,5)*(26*Power(t1,6) + 282*Power(t1,5)*t2 - 
                  1193*Power(t1,4)*Power(t2,2) + 
                  1398*Power(t1,3)*Power(t2,3) - 
                  429*Power(t1,2)*Power(t2,4) - 136*t1*Power(t2,5) + 
                  61*Power(t2,6)) + 
               Power(s2,3)*t1*
                (48*Power(t1,7) - 34*Power(t1,6)*t2 - 
                  603*Power(t1,5)*Power(t2,2) + 
                  1545*Power(t1,4)*Power(t2,3) - 
                  1193*Power(t1,3)*Power(t2,4) + 
                  53*Power(t1,2)*Power(t2,5) + 259*t1*Power(t2,6) - 
                  69*Power(t2,7)) + 
               s2*Power(t1,3)*
                (5*Power(t1,7) - 54*Power(t1,6)*t2 + 
                  90*Power(t1,5)*Power(t2,2) + 
                  12*Power(t1,4)*Power(t2,3) - 
                  101*Power(t1,3)*Power(t2,4) + 
                  28*Power(t1,2)*Power(t2,5) + 28*t1*Power(t2,6) - 
                  8*Power(t2,7)) + 
               Power(s2,4)*(-45*Power(t1,7) - 206*Power(t1,6)*t2 + 
                  1284*Power(t1,5)*Power(t2,2) - 
                  2058*Power(t1,4)*Power(t2,3) + 
                  1048*Power(t1,3)*Power(t2,4) + 
                  142*Power(t1,2)*Power(t2,5) - 217*t1*Power(t2,6) + 
                  35*Power(t2,7)) + 
               Power(s2,2)*Power(t1,2)*
                (-25*Power(t1,7) + 119*Power(t1,6)*t2 + 
                  2*Power(t1,5)*Power(t2,2) - 
                  506*Power(t1,4)*Power(t2,3) + 
                  622*Power(t1,3)*Power(t2,4) - 
                  131*Power(t1,2)*Power(t2,5) - 122*t1*Power(t2,6) + 
                  41*Power(t2,7))) + 
            Power(s1,4)*(s2 - t1)*
             (-2*Power(t1,6)*Power(t1 - t2,3)*Power(t2,2) + 
               Power(s2,8)*t2*
                (3*Power(t1,2) - 22*t1*t2 + 16*Power(t2,2)) + 
               Power(s2,7)*(2*Power(t1,4) - 20*Power(t1,3)*t2 + 
                  133*Power(t1,2)*Power(t2,2) - 135*t1*Power(t2,3) + 
                  23*Power(t2,4)) + 
               s2*Power(t1,3)*Power(t2,2)*
                (7*Power(t1,5) - 44*Power(t1,4)*t2 + 
                  88*Power(t1,3)*Power(t2,2) - 
                  80*Power(t1,2)*Power(t2,3) + 39*t1*Power(t2,4) - 
                  10*Power(t2,5)) - 
               Power(s2,6)*(8*Power(t1,5) - 38*Power(t1,4)*t2 + 
                  263*Power(t1,3)*Power(t2,2) - 
                  340*Power(t1,2)*Power(t2,3) + 65*t1*Power(t2,4) + 
                  33*Power(t2,5)) + 
               Power(s2,5)*(12*Power(t1,6) - 21*Power(t1,5)*t2 + 
                  208*Power(t1,4)*Power(t2,2) - 
                  410*Power(t1,3)*Power(t2,3) + 
                  74*Power(t1,2)*Power(t2,4) + 215*t1*Power(t2,5) - 
                  83*Power(t2,6)) - 
               Power(s2,2)*Power(t1,2)*t2*
                (3*Power(t1,6) + Power(t1,5)*t2 - 
                  120*Power(t1,4)*Power(t2,2) + 
                  281*Power(t1,3)*Power(t2,3) - 
                  227*Power(t1,2)*Power(t2,4) + 68*t1*Power(t2,5) - 
                  10*Power(t2,6)) + 
               Power(s2,3)*t1*
                (2*Power(t1,7) + 13*Power(t1,6)*t2 - 
                  18*Power(t1,5)*Power(t2,2) - 
                  216*Power(t1,4)*Power(t2,3) + 
                  381*Power(t1,3)*Power(t2,4) - 
                  96*Power(t1,2)*Power(t2,5) - 104*t1*Power(t2,6) + 
                  40*Power(t2,7)) - 
               Power(s2,4)*(8*Power(t1,7) + 10*Power(t1,6)*t2 + 
                  42*Power(t1,5)*Power(t2,2) - 
                  323*Power(t1,4)*Power(t2,3) + 
                  217*Power(t1,3)*Power(t2,4) + 
                  232*Power(t1,2)*Power(t2,5) - 219*t1*Power(t2,6) + 
                  43*Power(t2,7))) + 
            Power(s1,2)*s2*t2*
             (s2*Power(t1,3)*Power(t1 - t2,3)*Power(t2,2)*
                (6*Power(t1,3) + 7*Power(t1,2)*t2 + 25*t1*Power(t2,2) - 
                  10*Power(t2,3)) - 
               2*Power(t1,4)*Power(t1 - t2,3)*Power(t2,2)*
                (Power(t1,3) - Power(t1,2)*t2 + 4*t1*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(s2,9)*(Power(t1,3) - Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,8)*(-6*Power(t1,4) + 35*Power(t1,3)*t2 - 
                  44*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,7)*(15*Power(t1,5) - 155*Power(t1,4)*t2 + 
                  297*Power(t1,3)*Power(t2,2) - 
                  247*Power(t1,2)*Power(t2,3) + 103*t1*Power(t2,4) - 
                  23*Power(t2,5)) + 
               Power(s2,6)*(-20*Power(t1,6) + 285*Power(t1,5)*t2 - 
                  648*Power(t1,4)*Power(t2,2) + 
                  642*Power(t1,3)*Power(t2,3) - 
                  352*Power(t1,2)*Power(t2,4) + 126*t1*Power(t2,5) - 
                  25*Power(t2,6)) + 
               Power(s2,5)*(15*Power(t1,7) - 253*Power(t1,6)*t2 + 
                  647*Power(t1,5)*Power(t2,2) - 
                  669*Power(t1,4)*Power(t2,3) + 
                  371*Power(t1,3)*Power(t2,4) - 
                  177*Power(t1,2)*Power(t2,5) + 93*t1*Power(t2,6) - 
                  21*Power(t2,7)) - 
               Power(s2,2)*Power(t1,2)*t2*
                (7*Power(t1,7) - 18*Power(t1,6)*t2 + 
                  84*Power(t1,5)*Power(t2,2) - 
                  151*Power(t1,4)*Power(t2,3) + 
                  46*Power(t1,3)*Power(t2,4) + 
                  101*Power(t1,2)*Power(t2,5) - 87*t1*Power(t2,6) + 
                  18*Power(t2,7)) + 
               Power(s2,4)*(-6*Power(t1,8) + 95*Power(t1,7)*t2 - 
                  270*Power(t1,6)*Power(t2,2) + 
                  191*Power(t1,5)*Power(t2,3) + 
                  42*Power(t1,4)*Power(t2,4) - 
                  12*Power(t1,3)*Power(t2,5) - 
                  110*Power(t1,2)*Power(t2,6) + 70*t1*Power(t2,7) - 
                  11*Power(t2,8)) + 
               Power(s2,3)*t1*
                (Power(t1,8) + Power(t1,7)*t2 + 
                  144*Power(t1,5)*Power(t2,3) - 
                  312*Power(t1,4)*Power(t2,4) + 
                  164*Power(t1,3)*Power(t2,5) + 
                  83*Power(t1,2)*Power(t2,6) - 99*t1*Power(t2,7) + 
                  22*Power(t2,8)))) + 
         Power(s,2)*(2*Power(s2,3)*Power(s2 - t1,2)*t1*Power(t1 - t2,2)*
             Power(t2,3)*(s2 + t2)*
             (Power(s2,3) - 3*Power(s2,2)*t2 + 
               s2*(-3*Power(t1,2) + 8*t1*t2 - 4*Power(t2,2)) + 
               t1*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2))) - 
            Power(s1,6)*s2*Power(s2 - t1,2)*
             (t1*Power(t2,2)*
                (9*Power(t1,3) - 22*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,3)*(2*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) - Power(t2,3)) + 
               Power(s2,2)*(-4*Power(t1,4) + Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) - 2*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s2*(2*Power(t1,5) + 3*Power(t1,4)*t2 - 
                  25*Power(t1,3)*Power(t2,2) + 
                  29*Power(t1,2)*Power(t2,3) - 9*t1*Power(t2,4) - 
                  Power(t2,5))) - 
            Power(s1,5)*(s2 - t1)*
             (8*Power(s2,7)*(t1 - t2)*t2 + 
               4*Power(t1,5)*Power(t1 - t2,2)*Power(t2,2) + 
               2*Power(s2,6)*
                (2*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) + Power(t2,3)) - 
               s2*Power(t1,2)*Power(t2,2)*
                (Power(t1,4) - 25*Power(t1,3)*t2 + 
                  72*Power(t1,2)*Power(t2,2) - 72*t1*Power(t2,3) + 
                  26*Power(t2,4)) + 
               Power(s2,5)*(-16*Power(t1,4) + 10*Power(t1,3)*t2 + 
                  49*Power(t1,2)*Power(t2,2) - 95*t1*Power(t2,3) + 
                  46*Power(t2,4)) + 
               Power(s2,4)*(24*Power(t1,5) + 37*Power(t1,4)*t2 - 
                  195*Power(t1,3)*Power(t2,2) + 
                  276*Power(t1,2)*Power(t2,3) - 216*t1*Power(t2,4) + 
                  62*Power(t2,5)) + 
               Power(s2,2)*t1*
                (4*Power(t1,6) + 5*Power(t1,5)*t2 - 
                  45*Power(t1,4)*Power(t2,2) + 
                  17*Power(t1,3)*Power(t2,3) + 
                  56*Power(t1,2)*Power(t2,4) - 37*t1*Power(t2,5) + 
                  6*Power(t2,6)) + 
               Power(s2,3)*(-16*Power(t1,6) - 32*Power(t1,5)*t2 + 
                  168*Power(t1,4)*Power(t2,2) - 
                  211*Power(t1,3)*Power(t2,3) + 
                  176*Power(t1,2)*Power(t2,4) - 103*t1*Power(t2,5) + 
                  26*Power(t2,6))) + 
            Power(s1,4)*(-2*Power(t1,7)*Power(t1 - t2,2)*Power(t2,2) + 
               4*Power(s2,9)*t2*(-t1 + t2) + 
               Power(s2,8)*t2*
                (20*Power(t1,2) + 24*t1*t2 - 41*Power(t2,2)) - 
               Power(s2,7)*t2*
                (20*Power(t1,3) + 272*Power(t1,2)*t2 - 
                  400*t1*Power(t2,2) + 101*Power(t2,3)) + 
               Power(s2,6)*(6*Power(t1,5) + 2*Power(t1,4)*t2 + 
                  600*Power(t1,3)*Power(t2,2) - 
                  1130*Power(t1,2)*Power(t2,3) + 526*t1*Power(t2,4) - 
                  27*Power(t2,5)) + 
               s2*Power(t1,3)*Power(t2,2)*
                (61*Power(t1,5) - 169*Power(t1,4)*t2 + 
                  135*Power(t1,3)*Power(t2,2) + 
                  23*Power(t1,2)*Power(t2,3) - 83*t1*Power(t2,4) + 
                  33*Power(t2,5)) - 
               Power(s2,5)*(24*Power(t1,6) + 41*Power(t1,5)*t2 + 
                  302*Power(t1,4)*Power(t2,2) - 
                  1160*Power(t1,3)*Power(t2,3) + 
                  841*Power(t1,2)*Power(t2,4) + 56*t1*Power(t2,5) - 
                  97*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*
                (6*Power(t1,7) + 15*Power(t1,6)*t2 - 
                  326*Power(t1,5)*Power(t2,2) + 
                  679*Power(t1,4)*Power(t2,3) - 
                  422*Power(t1,3)*Power(t2,4) - 
                  Power(t1,2)*Power(t2,5) + 56*t1*Power(t2,6) - 
                  17*Power(t2,7)) + 
               Power(s2,4)*(36*Power(t1,7) + 95*Power(t1,6)*t2 - 
                  462*Power(t1,5)*Power(t2,2) + 
                  25*Power(t1,4)*Power(t2,3) + 
                  374*Power(t1,3)*Power(t2,4) + 
                  218*Power(t1,2)*Power(t2,5) - 324*t1*Power(t2,6) + 
                  68*Power(t2,7)) - 
               Power(s2,3)*t1*
                (24*Power(t1,7) + 67*Power(t1,6)*t2 - 
                  675*Power(t1,5)*Power(t2,2) + 
                  928*Power(t1,4)*Power(t2,3) - 
                  333*Power(t1,3)*Power(t2,4) + 
                  159*Power(t1,2)*Power(t2,5) - 252*t1*Power(t2,6) + 
                  82*Power(t2,7))) + 
            s1*s2*t2*(2*Power(t1,4)*Power(t1 - t2,4)*Power(t2,2)*
                (Power(t1,2) + Power(t2,2)) + 
               s2*Power(t1,3)*Power(t1 - t2,3)*Power(t2,2)*
                (14*Power(t1,3) - 9*Power(t1,2)*t2 - 
                  29*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*Power(t1 - t2,3)*t2*
                (19*Power(t1,4) - 106*Power(t1,3)*t2 + 
                  51*Power(t1,2)*Power(t2,2) + 73*t1*Power(t2,3) - 
                  19*Power(t2,4)) + 
               Power(s2,8)*(Power(t1,4) - 9*Power(t1,3)*t2 + 
                  28*Power(t1,2)*Power(t2,2) - 38*t1*Power(t2,3) + 
                  16*Power(t2,4)) + 
               Power(s2,7)*(-5*Power(t1,5) + 25*Power(t1,4)*t2 - 
                  60*Power(t1,3)*Power(t2,2) + 
                  109*Power(t1,2)*Power(t2,3) - 95*t1*Power(t2,4) + 
                  30*Power(t2,5)) + 
               Power(s2,6)*(10*Power(t1,6) + Power(t1,5)*t2 - 
                  114*Power(t1,4)*Power(t2,2) + 
                  143*Power(t1,3)*Power(t2,3) + 
                  9*Power(t1,2)*Power(t2,4) - 67*t1*Power(t2,5) + 
                  18*Power(t2,6)) - 
               Power(s2,5)*(10*Power(t1,7) + 82*Power(t1,6)*t2 - 
                  519*Power(t1,5)*Power(t2,2) + 
                  914*Power(t1,4)*Power(t2,3) - 
                  609*Power(t1,3)*Power(t2,4) + 
                  83*Power(t1,2)*Power(t2,5) + 53*t1*Power(t2,6) - 
                  10*Power(t2,7)) + 
               Power(s2,4)*(5*Power(t1,8) + 125*Power(t1,7)*t2 - 
                  725*Power(t1,6)*Power(t2,2) + 
                  1453*Power(t1,5)*Power(t2,3) - 
                  1236*Power(t1,4)*Power(t2,4) + 
                  319*Power(t1,3)*Power(t2,5) + 
                  118*Power(t1,2)*Power(t2,6) - 63*t1*Power(t2,7) + 
                  6*Power(t2,8)) - 
               Power(s2,3)*t1*
                (Power(t1,8) + 79*Power(t1,7)*t2 - 
                  499*Power(t1,6)*Power(t2,2) + 
                  1120*Power(t1,5)*Power(t2,3) - 
                  1061*Power(t1,4)*Power(t2,4) + 
                  260*Power(t1,3)*Power(t2,5) + 
                  220*Power(t1,2)*Power(t2,6) - 140*t1*Power(t2,7) + 
                  20*Power(t2,8))) + 
            Power(s1,2)*s2*t2*
             (Power(s2,8)*(Power(t1,3) + 3*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) - 12*Power(t2,3)) - 
               Power(t1,3)*Power(t1 - t2,3)*t2*
                (14*Power(t1,4) - 8*Power(t1,3)*t2 - 
                  Power(t1,2)*Power(t2,2) + 9*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,7)*(Power(t1,4) - 160*Power(t1,3)*t2 + 
                  255*Power(t1,2)*Power(t2,2) - 118*t1*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(s2,6)*(-8*Power(t1,5) + 633*Power(t1,4)*t2 - 
                  1374*Power(t1,3)*Power(t2,2) + 
                  1052*Power(t1,2)*Power(t2,3) - 327*t1*Power(t2,4) + 
                  43*Power(t2,5)) - 
               s2*Power(t1,2)*Power(t1 - t2,2)*
                (10*Power(t1,6) - 82*Power(t1,5)*t2 + 
                  65*Power(t1,4)*Power(t2,2) - 
                  60*Power(t1,3)*Power(t2,3) + 
                  39*Power(t1,2)*Power(t2,4) + 54*t1*Power(t2,5) - 
                  22*Power(t2,6)) + 
               Power(s2,5)*(-2*Power(t1,6) - 955*Power(t1,5)*t2 + 
                  2601*Power(t1,4)*Power(t2,2) - 
                  2528*Power(t1,3)*Power(t2,3) + 
                  1044*Power(t1,2)*Power(t2,4) - 175*t1*Power(t2,5) + 
                  18*Power(t2,6)) + 
               Power(s2,4)*(39*Power(t1,7) + 571*Power(t1,6)*t2 - 
                  2207*Power(t1,5)*Power(t2,2) + 
                  2552*Power(t1,4)*Power(t2,3) - 
                  1014*Power(t1,3)*Power(t2,4) + 
                  20*Power(t1,2)*Power(t2,5) - t1*Power(t2,6) + 
                  19*Power(t2,7)) + 
               Power(s2,2)*t1*
                (40*Power(t1,8) - 227*Power(t1,7)*t2 + 
                  272*Power(t1,6)*Power(t2,2) - 
                  207*Power(t1,5)*Power(t2,3) + 
                  458*Power(t1,4)*Power(t2,4) - 
                  468*Power(t1,3)*Power(t2,5) + 
                  40*Power(t1,2)*Power(t2,6) + 129*t1*Power(t2,7) - 
                  37*Power(t2,8)) + 
               Power(s2,3)*(-61*Power(t1,8) + 47*Power(t1,7)*t2 + 
                  632*Power(t1,6)*Power(t2,2) - 
                  948*Power(t1,5)*Power(t2,3) + 
                  31*Power(t1,4)*Power(t2,4) + 
                  471*Power(t1,3)*Power(t2,5) - 
                  107*Power(t1,2)*Power(t2,6) - 78*t1*Power(t2,7) + 
                  23*Power(t2,8))) - 
            Power(s1,3)*(2*Power(t1,6)*(3*t1 - 2*t2)*Power(t1 - t2,3)*
                Power(t2,2) + 
               2*Power(s2,9)*t2*
                (2*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,8)*t2*
                (-34*Power(t1,3) + 159*Power(t1,2)*t2 - 
                  111*t1*Power(t2,2) + 6*Power(t2,3)) - 
               s2*Power(t1,3)*Power(t1 - t2,3)*Power(t2,2)*
                (43*Power(t1,3) - 56*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,7)*(-4*Power(t1,5) + 82*Power(t1,4)*t2 - 
                  359*Power(t1,3)*Power(t2,2) + 
                  163*Power(t1,2)*Power(t2,3) + 128*t1*Power(t2,4) - 
                  42*Power(t2,5)) + 
               Power(s2,6)*(16*Power(t1,6) - 92*Power(t1,5)*t2 + 
                  177*Power(t1,4)*Power(t2,2) + 
                  470*Power(t1,3)*Power(t2,3) - 
                  1058*Power(t1,2)*Power(t2,4) + 527*t1*Power(t2,5) - 
                  44*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (-5*Power(t1,7) + 148*Power(t1,6)*t2 - 
                  513*Power(t1,5)*Power(t2,2) + 
                  665*Power(t1,4)*Power(t2,3) - 
                  381*Power(t1,3)*Power(t2,4) + 
                  172*Power(t1,2)*Power(t2,5) - 114*t1*Power(t2,6) + 
                  28*Power(t2,7)) + 
               Power(s2,5)*(-24*Power(t1,7) + 69*Power(t1,6)*t2 + 
                  170*Power(t1,5)*Power(t2,2) - 
                  1278*Power(t1,4)*Power(t2,3) + 
                  2444*Power(t1,3)*Power(t2,4) - 
                  1599*Power(t1,2)*Power(t2,5) + 190*t1*Power(t2,6) + 
                  62*Power(t2,7)) + 
               Power(s2,3)*t1*
                (-4*Power(t1,8) + 25*Power(t1,7)*t2 - 
                  178*Power(t1,6)*Power(t2,2) + 
                  308*Power(t1,5)*Power(t2,3) + 
                  212*Power(t1,4)*Power(t2,4) - 
                  619*Power(t1,3)*Power(t2,5) + 
                  55*Power(t1,2)*Power(t2,6) + 297*t1*Power(t2,7) - 
                  96*Power(t2,8)) + 
               Power(s2,4)*(16*Power(t1,8) - 49*Power(t1,7)*t2 - 
                  66*Power(t1,6)*Power(t2,2) + 
                  790*Power(t1,5)*Power(t2,3) - 
                  2113*Power(t1,4)*Power(t2,4) + 
                  1880*Power(t1,3)*Power(t2,5) - 
                  292*Power(t1,2)*Power(t2,6) - 248*t1*Power(t2,7) + 
                  66*Power(t2,8)))) + 
         Power(s,3)*(2*Power(s2,2)*Power(s2 - t1,2)*t1*Power(t1 - t2,2)*
             Power(t2,2)*(2*Power(s2,4)*(t1 - 2*t2) + 
               t1*(t1 - 3*t2)*Power(t1 - t2,2)*t2 + 
               Power(s2,3)*(-3*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) + 
               s2*Power(t1 - t2,2)*
                (Power(t1,2) - 3*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*t2*(3*Power(t1,2) - 9*t1*t2 + 8*Power(t2,2))) \
+ Power(s1,5)*(s2 - t1)*(4*Power(s2,6)*(t1 - t2)*t2 - 
               2*Power(t1,4)*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s2,5)*(2*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) + Power(t2,3)) + 
               s2*t1*Power(t2,2)*
                (-30*Power(t1,4) + 77*Power(t1,3)*t2 - 
                  56*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s2,4)*(-10*Power(t1,4) - 13*Power(t1,3)*t2 + 
                  56*Power(t1,2)*Power(t2,2) - 50*t1*Power(t2,3) + 
                  19*Power(t2,4)) + 
               Power(s2,3)*(14*Power(t1,5) + 33*Power(t1,4)*t2 - 
                  152*Power(t1,3)*Power(t2,2) + 
                  171*Power(t1,2)*Power(t2,3) - 93*t1*Power(t2,4) + 
                  23*Power(t2,5)) + 
               Power(s2,2)*(-6*Power(t1,6) - 16*Power(t1,5)*t2 + 
                  130*Power(t1,4)*Power(t2,2) - 
                  193*Power(t1,3)*Power(t2,3) + 
                  110*Power(t1,2)*Power(t2,4) - 36*t1*Power(t2,5) + 
                  9*Power(t2,6))) + 
            Power(s1,4)*(8*Power(s2,8)*(t1 - t2)*t2 - 
               2*Power(t1,5)*(5*t1 - 3*t2)*Power(t1 - t2,2)*
                Power(t2,2) + 
               s2*Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*
                (58*Power(t1,3) - 100*Power(t1,2)*t2 + 
                  53*t1*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,7)*(-38*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
                  36*Power(t2,3)) + 
               Power(s2,6)*(-6*Power(t1,4) + 26*Power(t1,3)*t2 + 
                  191*Power(t1,2)*Power(t2,2) - 344*t1*Power(t2,3) + 
                  117*Power(t2,4)) + 
               Power(s2,2)*t1*Power(t2,2)*
                (-137*Power(t1,5) + 433*Power(t1,4)*t2 - 
                  504*Power(t1,3)*Power(t2,2) + 
                  301*Power(t1,2)*Power(t2,3) - 125*t1*Power(t2,4) + 
                  24*Power(t2,5)) + 
               Power(s2,5)*(18*Power(t1,5) + 67*Power(t1,4)*t2 - 
                  535*Power(t1,3)*Power(t2,2) + 
                  952*Power(t1,2)*Power(t2,3) - 589*t1*Power(t2,4) + 
                  87*Power(t2,5)) - 
               Power(s2,4)*(18*Power(t1,6) + 100*Power(t1,5)*t2 - 
                  473*Power(t1,4)*Power(t2,2) + 
                  956*Power(t1,3)*Power(t2,3) - 
                  834*Power(t1,2)*Power(t2,4) + 188*t1*Power(t2,5) + 
                  21*Power(t2,6)) + 
               Power(s2,3)*(6*Power(t1,7) + 37*Power(t1,6)*t2 - 
                  38*Power(t1,5)*Power(t2,2) + 
                  69*Power(t1,4)*Power(t2,3) - 
                  151*Power(t1,3)*Power(t2,4) - 
                  3*Power(t1,2)*Power(t2,5) + 111*t1*Power(t2,6) - 
                  35*Power(t2,7))) + 
            s1*s2*t2*(Power(t1,3)*Power(t1 - t2,4)*t2*
                (7*Power(t1,3) - Power(t1,2)*t2 - 5*t1*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,7)*(-10*Power(t1,4) + 44*Power(t1,3)*t2 - 
                  82*Power(t1,2)*Power(t2,2) + 73*t1*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(s2,6)*(39*Power(t1,5) - 135*Power(t1,4)*t2 + 
                  217*Power(t1,3)*Power(t2,2) - 
                  229*Power(t1,2)*Power(t2,3) + 149*t1*Power(t2,4) - 
                  42*Power(t2,5)) + 
               s2*Power(t1,2)*Power(t1 - t2,3)*
                (5*Power(t1,5) - 33*Power(t1,4)*t2 + 
                  96*Power(t1,3)*Power(t2,2) - 
                  54*Power(t1,2)*Power(t2,3) - 49*t1*Power(t2,4) + 
                  13*Power(t2,5)) + 
               Power(s2,5)*(-55*Power(t1,6) + 80*Power(t1,5)*t2 + 
                  121*Power(t1,4)*Power(t2,2) - 
                  263*Power(t1,3)*Power(t2,3) + 
                  100*Power(t1,2)*Power(t2,4) + 34*t1*Power(t2,5) - 
                  18*Power(t2,6)) - 
               Power(s2,2)*t1*Power(t1 - t2,2)*
                (17*Power(t1,6) - 129*Power(t1,5)*t2 + 
                  462*Power(t1,4)*Power(t2,2) - 
                  507*Power(t1,3)*Power(t2,3) + 
                  108*Power(t1,2)*Power(t2,4) + 70*t1*Power(t2,5) - 
                  16*Power(t2,6)) + 
               Power(s2,3)*Power(t1 - t2,2)*
                (12*Power(t1,6) - 244*Power(t1,5)*t2 + 
                  763*Power(t1,4)*Power(t2,2) - 
                  693*Power(t1,3)*Power(t2,3) + 
                  142*Power(t1,2)*Power(t2,4) + 25*t1*Power(t2,5) - 
                  6*Power(t2,6)) + 
               Power(s2,4)*(26*Power(t1,7) + 157*Power(t1,6)*t2 - 
                  963*Power(t1,5)*Power(t2,2) + 
                  1727*Power(t1,4)*Power(t2,3) - 
                  1355*Power(t1,3)*Power(t2,4) + 
                  436*Power(t1,2)*Power(t2,5) - 21*t1*Power(t2,6) - 
                  6*Power(t2,7))) - 
            Power(s1,3)*(6*Power(t1,5)*Power(t1 - t2,4)*Power(t2,2) + 
               Power(s2,8)*t2*
                (2*Power(t1,2) + 30*t1*t2 - 25*Power(t2,2)) + 
               Power(s2,7)*t2*
                (25*Power(t1,3) - 279*Power(t1,2)*t2 + 
                  266*t1*Power(t2,2) - 33*Power(t2,3)) + 
               s2*Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*
                (38*Power(t1,4) - 25*Power(t1,3)*t2 - 
                  65*Power(t1,2)*Power(t2,2) + 55*t1*Power(t2,3) - 
                  8*Power(t2,4)) + 
               Power(s2,6)*(6*Power(t1,5) - 49*Power(t1,4)*t2 + 
                  532*Power(t1,3)*Power(t2,2) - 
                  626*Power(t1,2)*Power(t2,3) + 88*t1*Power(t2,4) + 
                  56*Power(t2,5)) + 
               Power(s2,5)*(-24*Power(t1,6) - 46*Power(t1,5)*t2 + 
                  26*Power(t1,4)*Power(t2,2) - 
                  22*Power(t1,3)*Power(t2,3) + 
                  525*Power(t1,2)*Power(t2,4) - 554*t1*Power(t2,5) + 
                  116*Power(t2,6)) + 
               Power(s2,4)*(36*Power(t1,7) + 159*Power(t1,6)*t2 - 
                  940*Power(t1,5)*Power(t2,2) + 
                  1589*Power(t1,4)*Power(t2,3) - 
                  1890*Power(t1,3)*Power(t2,4) + 
                  1439*Power(t1,2)*Power(t2,5) - 416*t1*Power(t2,6) + 
                  9*Power(t2,7)) + 
               Power(s2,3)*(-24*Power(t1,8) - 121*Power(t1,7)*t2 + 
                  941*Power(t1,6)*Power(t2,2) - 
                  1878*Power(t1,5)*Power(t2,3) + 
                  2053*Power(t1,4)*Power(t2,4) - 
                  1436*Power(t1,3)*Power(t2,5) + 
                  428*Power(t1,2)*Power(t2,6) + 80*t1*Power(t2,7) - 
                  43*Power(t2,8)) + 
               Power(s2,2)*t1*
                (6*Power(t1,8) + 30*Power(t1,7)*t2 - 
                  354*Power(t1,6)*Power(t2,2) + 
                  821*Power(t1,5)*Power(t2,3) - 
                  800*Power(t1,4)*Power(t2,4) + 
                  348*Power(t1,3)*Power(t2,5) + 
                  56*Power(t1,2)*Power(t2,6) - 149*t1*Power(t2,7) + 
                  42*Power(t2,8))) + 
            Power(s1,2)*(2*Power(t1,5)*Power(t1 - t2,4)*(3*t1 - t2)*
                Power(t2,2) + 
               Power(s2,8)*t2*
                (-20*Power(t1,3) + 16*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,7)*t2*
                (56*Power(t1,4) + 184*Power(t1,3)*t2 - 
                  450*Power(t1,2)*Power(t2,2) + 243*t1*Power(t2,3) - 
                  21*Power(t2,4)) - 
               s2*Power(t1,2)*Power(t1 - t2,3)*Power(t2,2)*
                (61*Power(t1,4) - 99*Power(t1,3)*t2 + 
                  51*Power(t1,2)*Power(t2,2) + 9*t1*Power(t2,3) - 
                  8*Power(t2,4)) + 
               Power(s2,6)*(2*Power(t1,6) - 56*Power(t1,5)*t2 - 
                  801*Power(t1,4)*Power(t2,2) + 
                  2123*Power(t1,3)*Power(t2,3) - 
                  1768*Power(t1,2)*Power(t2,4) + 536*t1*Power(t2,5) - 
                  40*Power(t2,6)) - 
               Power(s2,2)*t1*Power(t1 - t2,2)*t2*
                (21*Power(t1,6) - 236*Power(t1,5)*t2 + 
                  354*Power(t1,4)*Power(t2,2) - 
                  357*Power(t1,3)*Power(t2,3) + 
                  232*Power(t1,2)*Power(t2,4) + 10*t1*Power(t2,5) - 
                  20*Power(t2,6)) + 
               Power(s2,5)*(-6*Power(t1,7) + 48*Power(t1,6)*t2 + 
                  993*Power(t1,5)*Power(t2,2) - 
                  3357*Power(t1,4)*Power(t2,3) + 
                  3733*Power(t1,3)*Power(t2,4) - 
                  1614*Power(t1,2)*Power(t2,5) + 173*t1*Power(t2,6) + 
                  18*Power(t2,7)) + 
               Power(s2,4)*(6*Power(t1,8) - 73*Power(t1,7)*t2 - 
                  232*Power(t1,6)*Power(t2,2) + 
                  1801*Power(t1,5)*Power(t2,3) - 
                  2694*Power(t1,4)*Power(t2,4) + 
                  1245*Power(t1,3)*Power(t2,5) + 
                  87*Power(t1,2)*Power(t2,6) - 145*t1*Power(t2,7) + 
                  13*Power(t2,8)) + 
               Power(s2,3)*(-2*Power(t1,9) + 66*Power(t1,8)*t2 - 
                  383*Power(t1,7)*Power(t2,2) + 
                  481*Power(t1,6)*Power(t2,3) - 
                  333*Power(t1,5)*Power(t2,4) + 
                  689*Power(t1,4)*Power(t2,5) - 
                  838*Power(t1,3)*Power(t2,6) + 
                  325*Power(t1,2)*Power(t2,7) + 12*t1*Power(t2,8) - 
                  17*Power(t2,9)))))*Log(-t1))/
     (s*Power(s1,2)*Power(s2,2)*Power(s2 - t1,2)*t1*Power(s - s2 + t1,2)*
       Power(t1 - t2,2)*(s1 + t1 - t2)*Power(t2,2)*(s - s1 + t2)*(s2 + t2)*
       (s2 - t1 + t2)) + (8*(-(Power(s1,2)*Power(s2,2)*t1*(s1 - t2)*
            (s1 + s2 - t2)*(s1 + t1 - t2)*(s2 - t1 + t2)*
            (s1*s2 - s1*t1 + 2*s2*t1 - s2*t2)*
            Power(Power(s1,2)*(s2 - t1) + s2*t2*(s2 - t1 + t2) - 
              s1*(Power(s2,2) - 2*s2*t1 + Power(t1,2) + 2*s2*t2 - t1*t2),
             2)) + Power(s,9)*t1*(t1 - t2)*
          (Power(s1,2)*(4*Power(s2,3)*t1 - 
               8*Power(s2,2)*Power(t1 - t2,2) + 
               s2*(11*t1 - 8*t2)*Power(t1 - t2,2) - 
               4*t1*Power(t1 - t2,3)) - 
            2*s2*Power(t1 - t2,2)*t2*Power(s2 - t1 + t2,2) + 
            Power(s1,3)*(4*s2*Power(t1 - t2,2) - 
               2*t1*Power(t1 - t2,2) + 4*Power(s2,2)*t2) + 
            s1*Power(t1 - t2,2)*
             (4*Power(s2,3) + 7*s2*Power(t1 - t2,2) - 
               2*t1*Power(t1 - t2,2) + Power(s2,2)*(-8*t1 + 11*t2))) - 
         Power(s,8)*(Power(s1,5)*s2*t1*(t1 - t2)*(s2 - t1 + t2) - 
            2*s2*t1*Power(t1 - t2,2)*t2*Power(s2 - t1 + t2,2)*
             (3*s2*t1 - 3*Power(t1,2) - 5*s2*t2 + t1*t2 + 2*Power(t2,2)) \
+ Power(s1,2)*(2*Power(t1,2)*Power(t1 - t2,4)*(t1 + 7*t2) - 
               s2*Power(t1 - t2,3)*
                (5*Power(t1,3) + 18*Power(t1,2)*t2 - 
                  44*t1*Power(t2,2) - 6*Power(t2,3)) + 
               Power(s2,4)*(Power(t1,3) - 16*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - 2*Power(t2,3)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (22*Power(t1,3) + 33*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) + 14*Power(t2,3)) + 
               2*Power(s2,3)*
                (12*Power(t1,4) + Power(t1,3)*t2 - 
                  30*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) - 
                  5*Power(t2,4))) + 
            Power(s1,4)*(-2*Power(t1,2)*(5*t1 - 3*t2)*
                Power(t1 - t2,2) + 2*Power(s2,3)*t1*(t1 + t2) - 
               Power(s2,2)*(5*Power(t1,3) + 5*t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               s2*(21*Power(t1,4) - 39*Power(t1,3)*t2 + 
                  25*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) - 
                  2*Power(t2,4))) - 
            s1*(t1 - t2)*(-2*Power(t1,2)*Power(t1 - t2,4)*
                (3*t1 + 2*t2) + 
               2*Power(s2,4)*t2*
                (14*Power(t1,2) - 9*t1*t2 + Power(t2,2)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (30*Power(t1,3) - 56*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) - 6*Power(t2,3)) + 
               2*s2*Power(t1 - t2,3)*
                (13*Power(t1,3) - 5*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) - Power(t2,3)) + 
               Power(s2,3)*(11*Power(t1,4) - 74*Power(t1,3)*t2 + 
                  94*Power(t1,2)*Power(t2,2) - 37*t1*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,3)*(Power(s2,4)*t1*(t1 - 5*t2) - 
               2*Power(t1,2)*(7*t1 - 8*t2)*Power(t1 - t2,3) + 
               Power(s2,3)*(37*Power(t1,3) + 14*Power(t1,2)*t2 - 
                  31*t1*Power(t2,2) + 4*Power(t2,3)) + 
               s2*Power(t1 - t2,2)*
                (43*Power(t1,3) - 34*Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(-58*Power(t1,4) + 36*Power(t1,3)*t2 + 
                  18*Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) + 
                  10*Power(t2,4)))) + 
         Power(s,7)*(Power(s1,6)*s2*t1*
             (Power(s2,2) + 4*s2*t1 - 5*Power(t1,2) - 3*s2*t2 + 
               9*t1*t2 - 4*Power(t2,2)) - 
            2*s2*t1*(t1 - t2)*t2*Power(s2 - t1 + t2,2)*
             (Power(t1 - t2,2)*
                (3*Power(t1,2) + 6*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(3*Power(t1,2) - 12*t1*t2 + 
                  10*Power(t2,2)) + 
               s2*(-8*Power(t1,3) + 17*Power(t1,2)*t2 - 9*Power(t2,3))) \
+ Power(s1,5)*(-2*Power(s2,4)*t1 - 20*Power(t1,5) + 44*Power(t1,4)*t2 - 
               30*Power(t1,3)*Power(t2,2) + 6*Power(t1,2)*Power(t2,3) + 
               Power(s2,3)*(35*Power(t1,2) - 10*t1*t2 + 
                  4*Power(t2,2)) - 
               Power(s2,2)*(45*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(44*Power(t1,4) - 43*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s1,3)*(2*Power(t1,2)*Power(t1 - t2,3)*
                (17*Power(t1,2) + 4*t1*t2 - 12*Power(t2,2)) + 
               Power(s2,5)*(29*Power(t1,2) - 22*t1*t2 + 
                  4*Power(t2,2)) + 
               Power(s2,4)*(-88*Power(t1,3) + 201*Power(t1,2)*t2 - 
                  109*t1*Power(t2,2) + 34*Power(t2,3)) - 
               s2*Power(t1 - t2,2)*
                (112*Power(t1,4) + 9*Power(t1,3)*t2 - 
                  27*Power(t1,2)*Power(t2,2) + 93*t1*Power(t2,3) + 
                  30*Power(t2,4)) + 
               Power(s2,3)*(117*Power(t1,4) - 646*Power(t1,3)*t2 + 
                  429*Power(t1,2)*Power(t2,2) - 68*t1*Power(t2,3) + 
                  40*Power(t2,4)) + 
               Power(s2,2)*(7*Power(t1,5) + 392*Power(t1,4)*t2 - 
                  438*Power(t1,3)*Power(t2,2) + 
                  87*Power(t1,2)*Power(t2,3) - 28*t1*Power(t2,4) - 
                  20*Power(t2,5))) + 
            Power(s1,4)*(Power(s2,5)*t1 + 
               Power(s2,4)*(-42*Power(t1,2) + 9*t1*t2 - 
                  8*Power(t2,2)) - 
               2*Power(t1,2)*Power(t1 - t2,2)*
                (5*Power(t1,2) - 23*t1*t2 + 10*Power(t2,2)) + 
               Power(s2,3)*(209*Power(t1,3) - 164*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - 18*Power(t2,3)) + 
               Power(s2,2)*(-197*Power(t1,4) + 242*Power(t1,3)*t2 - 
                  38*Power(t1,2)*Power(t2,2) + 43*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               s2*(44*Power(t1,5) - 136*Power(t1,4)*t2 + 
                  153*Power(t1,3)*Power(t2,2) - 
                  97*Power(t1,2)*Power(t2,3) + 14*t1*Power(t2,4) + 
                  22*Power(t2,5))) + 
            s1*(-2*Power(t1,2)*Power(t1 - t2,5)*
                (3*Power(t1,2) + 6*t1*t2 + Power(t2,2)) - 
               Power(s2,2)*Power(t1 - t2,3)*
                (51*Power(t1,4) - 125*Power(t1,3)*t2 - 
                  155*Power(t1,2)*Power(t2,2) + 31*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               4*s2*Power(t1 - t2,4)*
                (9*Power(t1,4) + 5*Power(t1,3)*t2 - 
                  21*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) - 
                  Power(t2,4)) + 
               Power(s2,5)*(-9*Power(t1,4) - 34*Power(t1,3)*t2 + 
                  73*Power(t1,2)*Power(t2,2) - 44*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,3)*Power(t1 - t2,2)*
                (13*Power(t1,4) - 263*Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) + 54*t1*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s2,4)*(16*Power(t1,5) + 157*Power(t1,4)*t2 - 
                  306*Power(t1,3)*Power(t2,2) + 
                  186*Power(t1,2)*Power(t2,3) - 79*t1*Power(t2,4) + 
                  26*Power(t2,5))) + 
            Power(s1,2)*(2*Power(t1,2)*Power(t1 - t2,4)*
                (9*Power(t1,2) - 13*t1*t2 - 6*Power(t2,2)) + 
               Power(s2,5)*(19*Power(t1,3) - 56*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) - 14*Power(t2,3)) + 
               Power(s2,4)*(-32*Power(t1,4) + 224*Power(t1,3)*t2 - 
                  213*Power(t1,2)*Power(t2,2) + 139*t1*Power(t2,3) - 
                  52*Power(t2,4)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (104*Power(t1,4) + 199*Power(t1,3)*t2 + 
                  129*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) + 
                  12*Power(t2,4)) - 
               s2*Power(t1 - t2,3)*
                (71*Power(t1,4) - 80*Power(t1,3)*t2 + 
                  87*Power(t1,2)*Power(t2,2) + 88*t1*Power(t2,3) + 
                  18*Power(t2,4)) - 
               Power(s2,3)*(45*Power(t1,5) + 340*Power(t1,4)*t2 - 
                  580*Power(t1,3)*Power(t2,2) + 
                  209*Power(t1,2)*Power(t2,3) - 58*t1*Power(t2,4) + 
                  44*Power(t2,5)))) + 
         Power(s,6)*(-2*Power(s1,7)*s2*t1*
             (2*Power(s2,2) + 3*s2*t1 - 5*Power(t1,2) - s2*t2 + 
               8*t1*t2 - 3*Power(t2,2)) + 
            2*s2*t1*(t1 - t2)*t2*Power(s2 - t1 + t2,2)*
             (-(t1*Power(t1 - t2,2)*
                  (Power(t1,2) + 6*t1*t2 + 3*Power(t2,2))) + 
               Power(s2,3)*(Power(t1,2) - 8*t1*t2 + 10*Power(t2,2)) - 
               Power(s2,2)*(7*Power(t1,3) - 26*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) + 16*Power(t2,3)) + 
               s2*(7*Power(t1,4) - 7*Power(t1,3)*t2 - 
                  23*Power(t1,2)*Power(t2,2) + 19*t1*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s1,6)*(2*Power(s2,4)*(5*t1 + t2) + 
               Power(s2,3)*(-121*Power(t1,2) + 39*t1*t2 - 
                  10*Power(t2,2)) + 
               Power(s2,2)*(131*Power(t1,3) - 74*Power(t1,2)*t2 + 
                  45*t1*Power(t2,2) - 6*Power(t2,3)) + 
               2*Power(t1,2)*
                (10*Power(t1,3) - 18*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) - Power(t2,3)) + 
               s2*(-48*Power(t1,4) + 8*Power(t1,3)*t2 + 
                  24*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
                  6*Power(t2,4))) - 
            Power(s1,5)*(2*Power(s2,5)*(5*t1 + 2*t2) + 
               Power(s2,4)*(-144*Power(t1,2) + 17*t1*t2 + 
                  2*Power(t2,2)) + 
               Power(s2,3)*(461*Power(t1,3) - 614*Power(t1,2)*t2 + 
                  104*t1*Power(t2,2) - 60*Power(t2,3)) + 
               Power(s2,2)*(-325*Power(t1,4) + 798*Power(t1,3)*t2 - 
                  391*Power(t1,2)*Power(t2,2) + 221*t1*Power(t2,3) - 
                  32*Power(t2,4)) + 
               2*Power(t1,2)*
                (10*Power(t1,4) + 2*Power(t1,3)*t2 - 
                  33*Power(t1,2)*Power(t2,2) + 25*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               s2*(-31*Power(t1,5) - 82*Power(t1,4)*t2 + 
                  129*Power(t1,3)*Power(t2,2) - 
                  111*Power(t1,2)*Power(t2,3) + 46*t1*Power(t2,4) + 
                  26*Power(t2,5))) + 
            Power(s1,2)*(2*Power(t1,2)*Power(t1 - t2,5)*
                (13*Power(t1,2) + 16*t1*t2 + Power(t2,2)) + 
               Power(s2,6)*(-50*Power(t1,3) + 88*Power(t1,2)*t2 - 
                  61*t1*Power(t2,2) + 36*Power(t2,3)) + 
               Power(s2,5)*(211*Power(t1,4) - 528*Power(t1,3)*t2 + 
                  412*Power(t1,2)*Power(t2,2) - 276*t1*Power(t2,3) + 
                  96*Power(t2,4)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (150*Power(t1,5) + 260*Power(t1,4)*t2 + 
                  603*Power(t1,3)*Power(t2,2) + 
                  24*Power(t1,2)*Power(t2,3) - 7*t1*Power(t2,4) - 
                  50*Power(t2,5)) + 
               Power(s2,4)*(-268*Power(t1,5) + 1097*Power(t1,4)*t2 - 
                  1075*Power(t1,3)*Power(t2,2) + 
                  534*Power(t1,2)*Power(t2,3) - 144*t1*Power(t2,4) + 
                  6*Power(t2,5)) - 
               s2*Power(t1 - t2,3)*
                (133*Power(t1,5) - 36*Power(t1,4)*t2 - 
                  6*Power(t1,3)*Power(t2,2) + 
                  207*Power(t1,2)*Power(t2,3) + 94*t1*Power(t2,4) + 
                  14*Power(t2,5)) + 
               Power(s2,3)*(62*Power(t1,6) - 949*Power(t1,5)*t2 + 
                  832*Power(t1,4)*Power(t2,2) + 
                  318*Power(t1,3)*Power(t2,3) - 
                  257*Power(t1,2)*Power(t2,4) + 112*t1*Power(t2,5) - 
                  118*Power(t2,6))) + 
            s1*(-2*Power(t1,3)*Power(t1 - t2,5)*
                (Power(t1,2) + 6*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,6)*(4*Power(t1,4) + 35*Power(t1,3)*t2 - 
                  61*Power(t1,2)*Power(t2,2) + 56*t1*Power(t2,3) - 
                  20*Power(t2,4)) + 
               2*s2*Power(t1 - t2,4)*
                (11*Power(t1,5) + 25*Power(t1,4)*t2 - 
                  46*Power(t1,3)*Power(t2,2) - 
                  50*Power(t1,2)*Power(t2,3) - 9*t1*Power(t2,4) - 
                  Power(t2,5)) - 
               Power(s2,2)*Power(t1 - t2,3)*
                (51*Power(t1,5) - 140*Power(t1,4)*t2 - 
                  435*Power(t1,3)*Power(t2,2) + 
                  25*Power(t1,2)*Power(t2,3) + 53*t1*Power(t2,4) + 
                  12*Power(t2,5)) + 
               Power(s2,3)*Power(t1 - t2,2)*
                (27*Power(t1,5) - 518*Power(t1,4)*t2 - 
                  313*Power(t1,3)*Power(t2,2) + 
                  317*Power(t1,2)*Power(t2,3) + 19*t1*Power(t2,4) + 
                  28*Power(t2,5)) - 
               Power(s2,5)*(22*Power(t1,5) + 231*Power(t1,4)*t2 - 
                  368*Power(t1,3)*Power(t2,2) + 
                  197*Power(t1,2)*Power(t2,3) - 101*t1*Power(t2,4) + 
                  42*Power(t2,5)) + 
               Power(s2,4)*(21*Power(t1,6) + 516*Power(t1,5)*t2 - 
                  803*Power(t1,4)*Power(t2,2) + 
                  184*Power(t1,3)*Power(t2,3) + 
                  109*Power(t1,2)*Power(t2,4) - 19*t1*Power(t2,5) - 
                  8*Power(t2,6))) + 
            Power(s1,4)*(2*Power(s2,6)*(2*t1 + t2) + 
               Power(s2,5)*(-35*Power(t1,2) + 9*t1*t2 + 
                  30*Power(t2,2)) + 
               Power(s2,4)*(73*Power(t1,3) - 363*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) - 8*Power(t2,3)) - 
               2*Power(t1,2)*Power(t1 - t2,2)*
                (35*Power(t1,3) - 45*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,3)*(11*Power(t1,4) + 1464*Power(t1,3)*t2 - 
                  1248*Power(t1,2)*Power(t2,2) + 196*t1*Power(t2,3) - 
                  152*Power(t2,4)) - 
               Power(s2,2)*(194*Power(t1,5) + 1033*Power(t1,4)*t2 - 
                  1817*Power(t1,3)*Power(t2,2) + 
                  790*Power(t1,2)*Power(t2,3) - 402*t1*Power(t2,4) + 
                  72*Power(t2,5)) + 
               s2*(237*Power(t1,6) - 413*Power(t1,5)*t2 + 
                  111*Power(t1,4)*Power(t2,2) + 
                  234*Power(t1,3)*Power(t2,3) - 
                  325*Power(t1,2)*Power(t2,4) + 112*t1*Power(t2,5) + 
                  44*Power(t2,6))) + 
            Power(s1,3)*(Power(s2,6)*
                (-50*Power(t1,2) + 35*t1*t2 - 18*Power(t2,2)) + 
               Power(s2,5)*(208*Power(t1,3) - 257*Power(t1,2)*t2 + 
                  200*t1*Power(t2,2) - 80*Power(t2,3)) - 
               2*Power(t1,2)*Power(t1 - t2,3)*
                (Power(t1,3) - 48*Power(t1,2)*t2 + 18*t1*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s2,4)*(-350*Power(t1,4) + 647*Power(t1,3)*t2 - 
                  349*Power(t1,2)*Power(t2,2) + 127*t1*Power(t2,3) + 
                  10*Power(t2,4)) - 
               s2*Power(t1 - t2,2)*
                (7*Power(t1,5) + 238*Power(t1,4)*t2 - 
                  118*Power(t1,3)*Power(t2,2) + 
                  55*Power(t1,2)*Power(t2,3) + 182*t1*Power(t2,4) + 
                  36*Power(t2,5)) + 
               Power(s2,3)*(390*Power(t1,5) - 633*Power(t1,4)*t2 - 
                  841*Power(t1,3)*Power(t2,2) + 
                  830*Power(t1,2)*Power(t2,3) - 218*t1*Power(t2,4) + 
                  192*Power(t2,5)) + 
               Power(s2,2)*(-181*Power(t1,6) + 326*Power(t1,5)*t2 + 
                  981*Power(t1,4)*Power(t2,2) - 
                  1471*Power(t1,3)*Power(t2,3) + 
                  591*Power(t1,2)*Power(t2,4) - 330*t1*Power(t2,5) + 
                  84*Power(t2,6)))) + 
         Power(s,5)*(2*Power(s1,8)*s2*t1*
             (3*Power(s2,2) - 5*Power(t1,2) + 7*t1*t2 - 2*Power(t2,2) + 
               s2*(2*t1 + t2)) - 
            Power(s1,7)*(6*Power(s2,4)*(4*t1 + t2) + 
               Power(s2,3)*(-178*Power(t1,2) + 53*t1*t2 - 
                  6*Power(t2,2)) + 
               2*Power(t1,3)*
                (5*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(174*Power(t1,3) - 123*Power(t1,2)*t2 + 
                  50*t1*Power(t2,2) - 10*Power(t2,3)) + 
               s2*(-32*Power(t1,4) - 22*Power(t1,3)*t2 + 
                  33*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
                  2*Power(t2,4))) + 
            2*s2*t1*(t1 - t2)*t2*Power(s2 - t1 + t2,2)*
             (Power(s2,4)*(2*t1 - 5*t2)*t2 - 
               Power(t1,2)*Power(t1 - t2,2)*t2*(2*t1 + 3*t2) + 
               Power(s2,3)*(2*Power(t1,3) - 17*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,2)*(-5*Power(t1,4) + 15*Power(t1,3)*t2 + 
                  21*Power(t1,2)*Power(t2,2) - 35*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               s2*t1*(2*Power(t1,4) + 5*Power(t1,3)*t2 - 
                  25*Power(t1,2)*Power(t2,2) + 7*t1*Power(t2,3) + 
                  11*Power(t2,4))) + 
            Power(s1,6)*(Power(s2,5)*(29*t1 + 6*t2) + 
               Power(s2,4)*(-174*Power(t1,2) + 54*t1*t2 + 
                  44*Power(t2,2)) + 
               Power(s2,3)*(392*Power(t1,3) - 852*Power(t1,2)*t2 + 
                  55*t1*Power(t2,2) - 32*Power(t2,3)) + 
               2*Power(t1,3)*
                (20*Power(t1,3) - 22*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*(-209*Power(t1,4) + 1020*Power(t1,3)*t2 - 
                  531*Power(t1,2)*Power(t2,2) + 214*t1*Power(t2,3) - 
                  60*Power(t2,4)) + 
               s2*(-90*Power(t1,5) - 42*Power(t1,4)*t2 + 
                  41*Power(t1,3)*Power(t2,2) - 
                  60*Power(t1,2)*Power(t2,3) + 72*t1*Power(t2,4) + 
                  10*Power(t2,5))) + 
            Power(s1,5)*(-2*Power(s2,6)*(t1 - 3*t2) - 
               Power(s2,5)*(89*Power(t1,2) + 83*t1*t2 + 
                  52*Power(t2,2)) + 
               Power(s2,4)*(367*Power(t1,3) + 273*Power(t1,2)*t2 + 
                  167*t1*Power(t2,2) - 158*Power(t2,3)) + 
               10*Power(t1,4)*
                (5*Power(t1,3) - 21*Power(t1,2)*t2 + 
                  25*t1*Power(t2,2) - 9*Power(t2,3)) + 
               Power(s2,3)*(-690*Power(t1,4) - 797*Power(t1,3)*t2 + 
                  1229*Power(t1,2)*Power(t2,2) + 271*t1*Power(t2,3) + 
                  68*Power(t2,4)) + 
               Power(s2,2)*(523*Power(t1,5) + 422*Power(t1,4)*t2 - 
                  1988*Power(t1,3)*Power(t2,2) + 
                  738*Power(t1,2)*Power(t2,3) - 404*t1*Power(t2,4) + 
                  148*Power(t2,5)) - 
               s2*(175*Power(t1,6) - 382*Power(t1,5)*t2 + 
                  127*Power(t1,4)*Power(t2,2) + 
                  91*Power(t1,3)*Power(t2,3) - 
                  289*Power(t1,2)*Power(t2,4) + 203*t1*Power(t2,5) + 
                  20*Power(t2,6))) + 
            Power(s1,3)*(Power(s2,7)*
                (19*Power(t1,2) - 34*t1*t2 + 30*Power(t2,2)) - 
               10*Power(t1,3)*Power(t1 - t2,3)*
                (4*Power(t1,3) - 8*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,6)*(-41*Power(t1,3) + 71*Power(t1,2)*t2 - 
                  240*t1*Power(t2,2) + 62*Power(t2,3)) + 
               Power(s2,5)*(-49*Power(t1,4) + 398*Power(t1,3)*t2 + 
                  22*Power(t1,2)*Power(t2,2) + 42*t1*Power(t2,3) - 
                  198*Power(t2,4)) + 
               Power(s2,4)*(115*Power(t1,5) - 1429*Power(t1,4)*t2 + 
                  2357*Power(t1,3)*Power(t2,2) - 
                  1691*Power(t1,2)*Power(t2,3) + 1033*t1*Power(t2,4) - 
                  364*Power(t2,5)) + 
               s2*Power(t1 - t2,2)*
                (160*Power(t1,6) - 289*Power(t1,5)*t2 + 
                  36*Power(t1,4)*Power(t2,2) + 
                  139*Power(t1,3)*Power(t2,3) - 
                  177*Power(t1,2)*Power(t2,4) - 179*t1*Power(t2,5) - 
                  10*Power(t2,6)) + 
               Power(s2,3)*(16*Power(t1,6) + 2073*Power(t1,5)*t2 - 
                  4908*Power(t1,4)*Power(t2,2) + 
                  3066*Power(t1,3)*Power(t2,3) - 
                  1431*Power(t1,2)*Power(t2,4) + 850*t1*Power(t2,5) + 
                  14*Power(t2,6)) - 
               Power(s2,2)*(171*Power(t1,7) + 729*Power(t1,6)*t2 - 
                  2728*Power(t1,5)*Power(t2,2) + 
                  1669*Power(t1,4)*Power(t2,3) - 
                  67*Power(t1,3)*Power(t2,4) + 
                  210*Power(t1,2)*Power(t2,5) + 154*t1*Power(t2,6) - 
                  138*Power(t2,7))) + 
            Power(s1,2)*(Power(s2,7)*
                (29*Power(t1,3) - 56*Power(t1,2)*t2 + 
                  73*t1*Power(t2,2) - 44*Power(t2,3)) + 
               2*Power(t1,3)*Power(t1 - t2,4)*
                (5*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,6)*(-192*Power(t1,4) + 342*Power(t1,3)*t2 - 
                  421*Power(t1,2)*Power(t2,2) + 314*t1*Power(t2,3) - 
                  68*Power(t2,4)) + 
               Power(s2,5)*(442*Power(t1,5) - 821*Power(t1,4)*t2 + 
                  697*Power(t1,3)*Power(t2,2) - 
                  558*Power(t1,2)*Power(t2,3) + 36*t1*Power(t2,4) + 
                  122*Power(t2,5)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (145*Power(t1,6) - 66*Power(t1,5)*t2 + 
                  895*Power(t1,4)*Power(t2,2) + 
                  376*Power(t1,3)*Power(t2,3) - 
                  68*Power(t1,2)*Power(t2,4) - 110*t1*Power(t2,5) - 
                  52*Power(t2,6)) - 
               s2*Power(t1 - t2,3)*
                (88*Power(t1,6) + 84*Power(t1,5)*t2 - 
                  123*Power(t1,4)*Power(t2,2) + 
                  158*Power(t1,3)*Power(t2,3) + 
                  162*Power(t1,2)*Power(t2,4) + 54*t1*Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s2,4)*(-409*Power(t1,6) + 958*Power(t1,5)*t2 - 
                  44*Power(t1,4)*Power(t2,2) - 
                  804*Power(t1,3)*Power(t2,3) + 
                  809*Power(t1,2)*Power(t2,4) - 546*t1*Power(t2,5) + 
                  216*Power(t2,6)) + 
               Power(s2,3)*(65*Power(t1,7) - 248*Power(t1,6)*t2 - 
                  1391*Power(t1,5)*Power(t2,2) + 
                  2621*Power(t1,4)*Power(t2,3) - 
                  1345*Power(t1,3)*Power(t2,4) + 
                  677*Power(t1,2)*Power(t2,5) - 395*t1*Power(t2,6) + 
                  16*Power(t2,7))) + 
            Power(s1,4)*(-3*Power(s2,7)*(3*t1 + 2*t2) + 
               4*Power(s2,6)*
                (38*Power(t1,2) + 6*t1*t2 - 7*Power(t2,2)) - 
               10*Power(t1,3)*Power(t1 - t2,3)*
                (5*Power(t1,2) + 10*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,5)*(-619*Power(t1,3) + 505*Power(t1,2)*t2 - 
                  11*t1*Power(t2,2) + 150*Power(t2,3)) + 
               Power(s2,4)*(1034*Power(t1,4) - 2229*Power(t1,3)*t2 + 
                  944*Power(t1,2)*Power(t2,2) - 774*t1*Power(t2,3) + 
                  320*Power(t2,4)) - 
               Power(s2,3)*(924*Power(t1,5) - 3632*Power(t1,4)*t2 + 
                  1395*Power(t1,3)*Power(t2,2) - 
                  166*Power(t1,2)*Power(t2,3) + 792*t1*Power(t2,4) + 
                  64*Power(t2,5)) + 
               Power(s2,2)*(211*Power(t1,6) - 2042*Power(t1,5)*t2 + 
                  920*Power(t1,4)*Power(t2,2) + 
                  1116*Power(t1,3)*Power(t2,3) - 
                  255*Power(t1,2)*Power(t2,4) + 382*t1*Power(t2,5) - 
                  192*Power(t2,6)) + 
               s2*(210*Power(t1,7) - 62*Power(t1,6)*t2 - 
                  445*Power(t1,5)*Power(t2,2) + 
                  512*Power(t1,4)*Power(t2,3) - 
                  119*Power(t1,3)*Power(t2,4) - 
                  368*Power(t1,2)*Power(t2,5) + 252*t1*Power(t2,6) + 
                  20*Power(t2,7))) + 
            s1*(-2*Power(t1,4)*Power(t1 - t2,5)*t2*(2*t1 + 3*t2) + 
               Power(s2,7)*(Power(t1,4) - 22*Power(t1,3)*t2 + 
                  27*Power(t1,2)*Power(t2,2) - 44*t1*Power(t2,3) + 
                  20*Power(t2,4)) + 
               s2*t1*Power(t1 - t2,4)*
                (5*Power(t1,5) + 35*Power(t1,4)*t2 - 
                  35*Power(t1,3)*Power(t2,2) - 
                  121*Power(t1,2)*Power(t2,3) - 39*t1*Power(t2,4) - 
                  5*Power(t2,5)) + 
               Power(s2,6)*(3*Power(t1,5) + 164*Power(t1,4)*t2 - 
                  225*Power(t1,3)*Power(t2,2) + 
                  191*Power(t1,2)*Power(t2,3) - 93*t1*Power(t2,4) + 
                  28*Power(t2,5)) + 
               Power(s2,3)*Power(t1 - t2,2)*
                (35*Power(t1,6) - 464*Power(t1,5)*t2 - 
                  827*Power(t1,4)*Power(t2,2) + 
                  483*Power(t1,3)*Power(t2,3) + 
                  111*Power(t1,2)*Power(t2,4) + 30*t1*Power(t2,5) - 
                  8*Power(t2,6)) - 
               Power(s2,2)*Power(t1 - t2,3)*
                (27*Power(t1,6) - 61*Power(t1,5)*t2 - 
                  526*Power(t1,4)*Power(t2,2) - 
                  172*Power(t1,3)*Power(t2,3) + 
                  149*Power(t1,2)*Power(t2,4) + 40*t1*Power(t2,5) + 
                  8*Power(t2,6)) - 
               Power(s2,5)*(10*Power(t1,6) + 497*Power(t1,5)*t2 - 
                  562*Power(t1,4)*Power(t2,2) + 
                  96*Power(t1,3)*Power(t2,3) + 
                  6*Power(t1,2)*Power(t2,4) - 20*t1*Power(t2,5) + 
                  28*Power(t2,6)) - 
               Power(s2,4)*(7*Power(t1,7) - 734*Power(t1,6)*t2 + 
                  630*Power(t1,5)*Power(t2,2) + 
                  781*Power(t1,4)*Power(t2,3) - 
                  941*Power(t1,3)*Power(t2,4) + 
                  299*Power(t1,2)*Power(t2,5) - 94*t1*Power(t2,6) + 
                  52*Power(t2,7)))) + 
         s*t1*(Power(s1,10)*s2*
             (3*Power(s2,3) - Power(s2,2)*t1 + s2*Power(t1,2) + 
               Power(t1,3))*(s2 - t1 + t2) + 
            2*Power(s2,4)*(s2 - t1)*Power(t1,2)*(s2 - t2)*(t1 - t2)*
             Power(t2,4)*Power(s2 - t1 + t2,2) + 
            Power(s1,9)*s2*(Power(s2,5) + 4*Power(s2,4)*(4*t1 - 5*t2) + 
               Power(s2,3)*(-30*Power(t1,2) + 37*t1*t2 - 
                  21*Power(t2,2)) + 
               Power(t1,3)*(-2*Power(t1,2) + 17*t1*t2 - 
                  13*Power(t2,2)) + 
               Power(s2,2)*t1*(34*Power(t1,2) - t1*t2 - Power(t2,2)) + 
               s2*Power(t1,2)*
                (-19*Power(t1,2) - 5*t1*t2 + 11*Power(t2,2))) + 
            Power(s1,8)*(-8*Power(s2,7) + 5*Power(s2,6)*(4*t1 - 3*t2) + 
               2*Power(t1,6)*(t1 - t2) + 
               Power(s2,5)*(16*Power(t1,2) - 85*t1*t2 + 
                  56*Power(t2,2)) + 
               Power(s2,2)*Power(t1,2)*
                (-46*Power(t1,3) + 75*Power(t1,2)*t2 + 
                  116*t1*Power(t2,2) - 94*Power(t2,3)) + 
               Power(s2,3)*t1*
                (76*Power(t1,3) - 119*Power(t1,2)*t2 - 
                  97*t1*Power(t2,2) + 39*Power(t2,3)) + 
               Power(s2,4)*(-52*Power(t1,3) + 168*Power(t1,2)*t2 - 
                  128*t1*Power(t2,2) + 63*Power(t2,3)) + 
               s2*Power(t1,3)*
                (-8*Power(t1,3) + 42*Power(t1,2)*t2 - 
                  109*t1*Power(t2,2) + 64*Power(t2,3))) + 
            Power(s1,7)*(Power(s2,8) + Power(s2,7)*(-39*t1 + 43*t2) + 
               4*Power(t1,6)*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
               2*Power(s2,6)*
                (52*Power(t1,2) - 63*t1*t2 + 30*Power(t2,2)) - 
               Power(s2,5)*(111*Power(t1,3) + 101*Power(t1,2)*t2 - 
                  205*t1*Power(t2,2) + 87*Power(t2,3)) + 
               s2*Power(t1,3)*
                (-12*Power(t1,4) + 71*Power(t1,3)*t2 - 
                  232*Power(t1,2)*Power(t2,2) + 356*t1*Power(t2,3) - 
                  164*Power(t2,4)) + 
               Power(s2,4)*(84*Power(t1,4) + 249*Power(t1,3)*t2 - 
                  429*Power(t1,2)*Power(t2,2) + 217*t1*Power(t2,3) - 
                  105*Power(t2,4)) - 
               Power(s2,3)*t1*
                (2*Power(t1,4) + 211*Power(t1,3)*t2 + 
                  22*Power(t1,2)*Power(t2,2) - 486*t1*Power(t2,3) + 
                  157*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (-29*Power(t1,4) + 151*Power(t1,3)*t2 + 
                  70*Power(t1,2)*Power(t2,2) - 552*t1*Power(t2,3) + 
                  276*Power(t2,4))) + 
            Power(s1,6)*(3*Power(s2,9) - 2*Power(s2,8)*t2 + 
               Power(s2,7)*(-62*Power(t1,2) + 169*t1*t2 - 
                  92*Power(t2,2)) + 
               Power(s2,6)*(159*Power(t1,3) - 409*Power(t1,2)*t2 + 
                  274*t1*Power(t2,2) - 104*Power(t2,3)) + 
               2*Power(t1,6)*
                (Power(t1,3) - 7*Power(t1,2)*t2 + 12*t1*Power(t2,2) - 
                  6*Power(t2,3)) + 
               Power(s2,5)*(-218*Power(t1,4) + 252*Power(t1,3)*t2 + 
                  443*Power(t1,2)*Power(t2,2) - 343*t1*Power(t2,3) + 
                  88*Power(t2,4)) + 
               s2*Power(t1,3)*t2*
                (54*Power(t1,4) - 255*Power(t1,3)*t2 + 
                  602*Power(t1,2)*Power(t2,2) - 659*t1*Power(t2,3) + 
                  245*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (-10*Power(t1,5) + 39*Power(t1,4)*t2 + 
                  33*Power(t1,3)*Power(t2,2) - 
                  758*Power(t1,2)*Power(t2,3) + 1183*t1*Power(t2,4) - 
                  425*Power(t2,5)) + 
               Power(s2,4)*(213*Power(t1,5) - 218*Power(t1,4)*t2 - 
                  742*Power(t1,3)*Power(t2,2) + 
                  698*Power(t1,2)*Power(t2,3) - 186*t1*Power(t2,4) + 
                  105*Power(t2,5)) + 
               Power(s2,3)*t1*
                (-87*Power(t1,5) + 157*Power(t1,4)*t2 + 
                  9*Power(t1,3)*Power(t2,2) + 
                  685*Power(t1,2)*Power(t2,3) - 1076*t1*Power(t2,4) + 
                  305*Power(t2,5))) + 
            Power(s1,5)*(Power(s2,9)*(7*t1 - 9*t2) - 
               4*Power(t1,6)*(t1 - 2*t2)*Power(t1 - t2,2)*t2 + 
               Power(s2,8)*(-21*Power(t1,2) - 7*t1*t2 + 
                  4*Power(t2,2)) + 
               Power(s2,7)*(30*Power(t1,3) + 231*Power(t1,2)*t2 - 
                  294*t1*Power(t2,2) + 98*Power(t2,3)) + 
               Power(s2,6)*(-34*Power(t1,4) - 411*Power(t1,3)*t2 + 
                  523*Power(t1,2)*Power(t2,2) - 207*t1*Power(t2,3) + 
                  75*Power(t2,4)) + 
               Power(s2,5)*(-15*Power(t1,5) + 275*Power(t1,4)*t2 + 
                  418*Power(t1,3)*Power(t2,2) - 
                  1188*Power(t1,2)*Power(t2,3) + 491*t1*Power(t2,4) - 
                  73*Power(t2,5)) + 
               Power(s2,3)*t1*
                (-55*Power(t1,6) + 266*Power(t1,5)*t2 - 
                  368*Power(t1,4)*Power(t2,2) + 
                  446*Power(t1,3)*Power(t2,3) - 
                  1294*Power(t1,2)*Power(t2,4) + 1304*t1*Power(t2,5) - 
                  339*Power(t2,6)) + 
               s2*Power(t1,3)*
                (5*Power(t1,6) - 3*Power(t1,5)*t2 - 
                  115*Power(t1,4)*Power(t2,2) + 
                  462*Power(t1,3)*Power(t2,3) - 
                  842*Power(t1,2)*Power(t2,4) + 717*t1*Power(t2,5) - 
                  221*Power(t2,6)) + 
               Power(s2,4)*(86*Power(t1,6) - 328*Power(t1,5)*t2 - 
                  458*Power(t1,4)*Power(t2,2) + 
                  1648*Power(t1,3)*Power(t2,3) - 
                  854*Power(t1,2)*Power(t2,4) + 69*t1*Power(t2,5) - 
                  63*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*
                (-3*Power(t1,6) - 6*Power(t1,5)*t2 + 
                  154*Power(t1,4)*Power(t2,2) - 
                  645*Power(t1,3)*Power(t2,3) + 
                  1459*Power(t1,2)*Power(t2,4) - 1355*t1*Power(t2,5) + 
                  379*Power(t2,6))) + 
            s1*Power(s2,2)*Power(t2,2)*
             (2*Power(t1,3)*Power(t1 - t2,3)*(3*t1 - t2)*Power(t2,3) + 
               Power(s2,7)*t2*
                (4*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,5)*t1*
                (-3*Power(t1,4) + 79*Power(t1,3)*t2 - 
                  172*Power(t1,2)*Power(t2,2) + 101*t1*Power(t2,3) - 
                  21*Power(t2,4)) - 
               s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                (2*Power(t1,4) - 13*Power(t1,3)*t2 + 
                  38*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,6)*(Power(t1,4) - 29*Power(t1,3)*t2 + 
                  61*Power(t1,2)*Power(t2,2) - 31*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,4)*(3*Power(t1,6) - 97*Power(t1,5)*t2 + 
                  215*Power(t1,4)*Power(t2,2) - 
                  104*Power(t1,3)*Power(t2,3) - 
                  24*Power(t1,2)*Power(t2,4) + 25*t1*Power(t2,5) - 
                  6*Power(t2,6)) + 
               Power(s2,2)*t1*t2*
                (-6*Power(t1,6) - 20*Power(t1,5)*t2 + 
                  148*Power(t1,4)*Power(t2,2) - 
                  231*Power(t1,3)*Power(t2,3) + 
                  144*Power(t1,2)*Power(t2,4) - 41*t1*Power(t2,5) + 
                  6*Power(t2,6)) - 
               Power(s2,3)*(Power(t1,7) - 51*Power(t1,6)*t2 + 
                  93*Power(t1,5)*Power(t2,2) + 
                  53*Power(t1,4)*Power(t2,3) - 
                  173*Power(t1,3)*Power(t2,4) + 
                  106*Power(t1,2)*Power(t2,5) - 29*t1*Power(t2,6) + 
                  3*Power(t2,7))) + 
            Power(s1,2)*s2*t2*
             (Power(s2,8)*t2*
                (-4*Power(t1,2) + 17*t1*t2 - 9*Power(t2,2)) - 
               s2*Power(t1,2)*Power(t1 - t2,3)*Power(t2,2)*
                (14*Power(t1,3) + 11*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) - 6*Power(t2,3)) + 
               2*Power(t1,3)*Power(t1 - t2,3)*Power(t2,2)*
                (Power(t1,3) - Power(t1,2)*t2 + 4*t1*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(s2,7)*(-2*Power(t1,4) + 45*Power(t1,3)*t2 - 
                  149*Power(t1,2)*Power(t2,2) + 100*t1*Power(t2,3) - 
                  22*Power(t2,4)) + 
               Power(s2,6)*t1*
                (5*Power(t1,4) - 119*Power(t1,3)*t2 + 
                  445*Power(t1,2)*Power(t2,2) - 347*t1*Power(t2,3) + 
                  84*Power(t2,4)) + 
               Power(s2,5)*(-3*Power(t1,6) + 101*Power(t1,5)*t2 - 
                  493*Power(t1,4)*Power(t2,2) + 
                  304*Power(t1,3)*Power(t2,3) + 
                  133*Power(t1,2)*Power(t2,4) - 134*t1*Power(t2,5) + 
                  30*Power(t2,6)) + 
               Power(s2,3)*t1*
                (Power(t1,7) - 44*Power(t1,6)*t2 + 
                  180*Power(t1,5)*Power(t2,2) - 
                  730*Power(t1,4)*Power(t2,3) + 
                  1196*Power(t1,3)*Power(t2,4) - 
                  808*Power(t1,2)*Power(t2,5) + 227*t1*Power(t2,6) - 
                  22*Power(t2,7)) + 
               Power(s2,2)*t1*t2*
                (13*Power(t1,7) - 50*Power(t1,6)*t2 + 
                  214*Power(t1,5)*Power(t2,2) - 
                  399*Power(t1,4)*Power(t2,3) + 
                  277*Power(t1,3)*Power(t2,4) - 
                  27*Power(t1,2)*Power(t2,5) - 40*t1*Power(t2,6) + 
                  12*Power(t2,7)) + 
               Power(s2,4)*(-Power(t1,7) + 8*Power(t1,6)*t2 + 
                  62*Power(t1,5)*Power(t2,2) + 
                  429*Power(t1,4)*Power(t2,3) - 
                  936*Power(t1,3)*Power(t2,4) + 
                  608*Power(t1,2)*Power(t2,5) - 169*t1*Power(t2,6) + 
                  17*Power(t2,7))) + 
            Power(s1,4)*(2*Power(t1,6)*Power(t1 - t2,3)*Power(t2,2) + 
               Power(s2,9)*(4*Power(t1,2) - 13*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,8)*(-19*Power(t1,3) + 9*Power(t1,2)*t2 + 
                  52*t1*Power(t2,2) - 16*Power(t2,3)) + 
               Power(s2,7)*(59*Power(t1,4) + 48*Power(t1,3)*t2 - 
                  427*Power(t1,2)*Power(t2,2) + 279*t1*Power(t2,3) - 
                  52*Power(t2,4)) + 
               Power(s2,6)*(-108*Power(t1,5) + 33*Power(t1,4)*t2 + 
                  433*Power(t1,3)*Power(t2,2) - 
                  144*Power(t1,2)*Power(t2,3) - 88*t1*Power(t2,4) + 
                  9*Power(t2,5)) + 
               Power(s2,5)*(91*Power(t1,6) - 156*Power(t1,5)*t2 + 
                  482*Power(t1,4)*Power(t2,2) - 
                  1765*Power(t1,3)*Power(t2,3) + 
                  1773*Power(t1,2)*Power(t2,4) - 555*t1*Power(t2,5) + 
                  60*Power(t2,6)) + 
               s2*Power(t1,3)*t2*
                (-8*Power(t1,6) - 2*Power(t1,5)*t2 + 
                  136*Power(t1,4)*Power(t2,2) - 
                  439*Power(t1,3)*Power(t2,3) + 
                  646*Power(t1,2)*Power(t2,4) - 451*t1*Power(t2,5) + 
                  118*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*
                (5*Power(t1,7) - 7*Power(t1,6)*t2 + 
                  80*Power(t1,5)*Power(t2,2) - 
                  368*Power(t1,4)*Power(t2,3) + 
                  879*Power(t1,3)*Power(t2,4) - 
                  1243*Power(t1,2)*Power(t2,5) + 850*t1*Power(t2,6) - 
                  196*Power(t2,7)) + 
               Power(s2,4)*(-18*Power(t1,7) + 11*Power(t1,6)*t2 - 
                  478*Power(t1,5)*Power(t2,2) + 
                  1993*Power(t1,4)*Power(t2,3) - 
                  2332*Power(t1,3)*Power(t2,4) + 
                  815*Power(t1,2)*Power(t2,5) - 12*t1*Power(t2,6) + 
                  21*Power(t2,7)) + 
               Power(s2,3)*t1*
                (-14*Power(t1,7) + 83*Power(t1,6)*t2 - 
                  164*Power(t1,5)*Power(t2,2) + 
                  89*Power(t1,4)*Power(t2,3) - 
                  340*Power(t1,3)*Power(t2,4) + 
                  1037*Power(t1,2)*Power(t2,5) - 893*t1*Power(t2,6) + 
                  221*Power(t2,7))) + 
            Power(s1,3)*s2*(Power(s2,8)*t2*
                (-4*Power(t1,2) - 3*t1*t2 + 6*Power(t2,2)) + 
               Power(t1,3)*Power(t1 - t2,2)*Power(t2,2)*
                (Power(t1,4) + 15*Power(t1,3)*t2 - 
                  54*Power(t1,2)*Power(t2,2) + 82*t1*Power(t2,3) - 
                  34*Power(t2,4)) + 
               Power(s2,7)*(Power(t1,4) + Power(t1,3)*t2 + 
                  102*Power(t1,2)*Power(t2,2) - 114*t1*Power(t2,3) + 
                  29*Power(t2,4)) + 
               Power(s2,5)*t2*
                (84*Power(t1,5) + 292*Power(t1,4)*t2 - 
                  381*Power(t1,3)*Power(t2,2) - 
                  185*Power(t1,2)*Power(t2,3) + 236*t1*Power(t2,4) - 
                  50*Power(t2,5)) - 
               Power(s2,6)*(2*Power(t1,5) + 9*Power(t1,4)*t2 + 
                  359*Power(t1,3)*Power(t2,2) - 
                  506*Power(t1,2)*Power(t2,3) + 178*t1*Power(t2,4) - 
                  11*Power(t2,5)) + 
               Power(s2,2)*t1*t2*
                (5*Power(t1,7) - 4*Power(t1,6)*t2 - 
                  158*Power(t1,5)*Power(t2,2) + 
                  414*Power(t1,4)*Power(t2,3) - 
                  176*Power(t1,3)*Power(t2,4) - 
                  320*Power(t1,2)*Power(t2,5) + 318*t1*Power(t2,6) - 
                  79*Power(t2,7)) + 
               Power(s2,4)*(2*Power(t1,7) - 130*Power(t1,6)*t2 + 
                  187*Power(t1,5)*Power(t2,2) - 
                  922*Power(t1,4)*Power(t2,3) + 
                  1977*Power(t1,3)*Power(t2,4) - 
                  1447*Power(t1,2)*Power(t2,5) + 411*t1*Power(t2,6) - 
                  41*Power(t2,7)) + 
               s2*Power(t1,2)*t2*
                (-5*Power(t1,7) + 26*Power(t1,6)*t2 - 
                  103*Power(t1,5)*Power(t2,2) + 
                  242*Power(t1,4)*Power(t2,3) - 
                  410*Power(t1,3)*Power(t2,4) + 
                  462*Power(t1,2)*Power(t2,5) - 266*t1*Power(t2,6) + 
                  54*Power(t2,7)) - 
               Power(s2,3)*(Power(t1,8) - 58*Power(t1,7)*t2 + 
                  242*Power(t1,6)*Power(t2,2) - 
                  1177*Power(t1,5)*Power(t2,3) + 
                  2372*Power(t1,4)*Power(t2,4) - 
                  1895*Power(t1,3)*Power(t2,5) + 
                  554*Power(t1,2)*Power(t2,6) - 23*t1*Power(t2,7) + 
                  3*Power(t2,8)))) - 
         Power(s,4)*(Power(s1,9)*s2*t1*
             (4*Power(s2,2) - 5*Power(t1,2) + 6*t1*t2 - Power(t2,2) + 
               s2*(t1 + 3*t2)) - 
            Power(s1,8)*(2*Power(t1,4)*(t1 - t2) + 
               3*Power(s2,4)*(9*t1 + 2*t2) + 
               Power(s2,3)*(-123*Power(t1,2) + 40*t1*t2 + 
                  2*Power(t2,2)) + 
               Power(s2,2)*(111*Power(t1,3) - 79*Power(t1,2)*t2 + 
                  24*t1*Power(t2,2) - 4*Power(t2,3)) + 
               s2*t1*(-17*Power(t1,3) - 17*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) + Power(t2,3))) - 
            2*s2*t1*(t1 - t2)*t2*Power(s2 - t1 + t2,2)*
             (Power(s2,5)*Power(t2,2) - 
               Power(t1,3)*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s2,4)*t2*
                (4*Power(t1,2) - 9*t1*t2 - 6*Power(t2,2)) + 
               s2*Power(t1,2)*t2*
                (3*Power(t1,3) - 6*Power(t1,2)*t2 - 7*t1*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(s2,3)*(Power(t1,4) - 10*Power(t1,3)*t2 - 
                  4*Power(t1,2)*Power(t2,2) + 31*t1*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(s2,2)*t1*
                (Power(t1,4) - 26*Power(t1,2)*Power(t2,2) + 
                  20*t1*Power(t2,3) + 15*Power(t2,4))) + 
            Power(s1,7)*(Power(s2,5)*(23*t1 - 2*t2) + 
               2*Power(t1,4)*
                (13*Power(t1,2) - 15*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,4)*(-56*Power(t1,2) + 118*t1*t2 + 
                  48*Power(t2,2)) + 
               Power(s2,3)*(7*Power(t1,3) - 606*Power(t1,2)*t2 + 
                  25*t1*Power(t2,2) + 24*Power(t2,3)) + 
               Power(s2,2)*(55*Power(t1,4) + 569*Power(t1,3)*t2 - 
                  299*Power(t1,2)*Power(t2,2) + 38*t1*Power(t2,3) - 
                  26*Power(t2,4)) + 
               s2*t1*(-59*Power(t1,4) - 54*Power(t1,3)*t2 - 
                  35*Power(t1,2)*Power(t2,2) + 45*t1*Power(t2,3) + 
                  30*Power(t2,4))) + 
            Power(s1,6)*(Power(s2,6)*(29*t1 + 16*t2) - 
               2*Power(s2,5)*
                (119*Power(t1,2) + 64*t1*t2 - 8*Power(t2,2)) + 
               Power(s2,4)*(708*Power(t1,3) + 59*Power(t1,2)*t2 - 
                  159*t1*Power(t2,2) - 170*Power(t2,3)) - 
               2*Power(t1,4)*
                (Power(t1,3) + 35*Power(t1,2)*t2 - 51*t1*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(s2,3)*(-1109*Power(t1,4) + 551*Power(t1,3)*t2 + 
                  997*Power(t1,2)*Power(t2,2) + 400*t1*Power(t2,3) - 
                  100*Power(t2,4)) + 
               Power(s2,2)*(615*Power(t1,5) - 775*Power(t1,4)*t2 - 
                  930*Power(t1,3)*Power(t2,2) + 
                  144*Power(t1,2)*Power(t2,3) + 78*t1*Power(t2,4) + 
                  70*Power(t2,5)) - 
               s2*t1*(3*Power(t1,5) - 145*Power(t1,4)*t2 + 
                  27*Power(t1,3)*Power(t2,2) - 
                  210*Power(t1,2)*Power(t2,3) + 89*t1*Power(t2,4) + 
                  100*Power(t2,5))) + 
            Power(s1,5)*(-(Power(s2,7)*(29*t1 + 2*t2)) + 
               Power(s2,6)*(166*Power(t1,2) - 65*t1*t2 - 
                  94*Power(t2,2)) + 
               Power(s2,5)*(-467*Power(t1,3) + 738*Power(t1,2)*t2 + 
                  474*t1*Power(t2,2) - 74*Power(t2,3)) - 
               10*Power(t1,4)*
                (7*Power(t1,4) - 17*Power(t1,3)*t2 + 
                  7*Power(t1,2)*Power(t2,2) + 7*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,4)*(705*Power(t1,4) - 2447*Power(t1,3)*t2 + 
                  55*Power(t1,2)*Power(t2,2) + 83*t1*Power(t2,3) + 
                  330*Power(t2,4)) - 
               Power(s2,3)*(636*Power(t1,5) - 4266*Power(t1,4)*t2 + 
                  2146*Power(t1,3)*Power(t2,2) + 
                  314*Power(t1,2)*Power(t2,3) + 1221*t1*Power(t2,4) - 
                  212*Power(t2,5)) + 
               Power(s2,2)*(82*Power(t1,6) - 2567*Power(t1,5)*t2 + 
                  2642*Power(t1,4)*Power(t2,2) + 
                  75*Power(t1,3)*Power(t2,3) + 
                  817*Power(t1,2)*Power(t2,4) - 335*t1*Power(t2,5) - 
                  100*Power(t2,6)) + 
               s2*t1*(260*Power(t1,6) - 310*Power(t1,5)*t2 - 
                  68*Power(t1,4)*Power(t2,2) + 
                  372*Power(t1,3)*Power(t2,3) - 
                  641*Power(t1,2)*Power(t2,4) + 162*t1*Power(t2,5) + 
                  155*Power(t2,6))) - 
            Power(s1,4)*(6*Power(s2,8)*t2 - 
               Power(s2,7)*(63*Power(t1,2) + 80*t1*t2 + 
                  10*Power(t2,2)) + 
               Power(s2,6)*(381*Power(t1,3) + 173*Power(t1,2)*t2 + 
                  133*t1*Power(t2,2) - 228*Power(t2,3)) + 
               10*Power(t1,4)*Power(t1 - t2,2)*
                (2*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,5)*(-867*Power(t1,4) + 30*Power(t1,3)*t2 - 
                  150*Power(t1,2)*Power(t2,2) + 1134*t1*Power(t2,3) - 
                  182*Power(t2,4)) + 
               Power(s2,4)*(980*Power(t1,5) - 651*Power(t1,4)*t2 - 
                  936*Power(t1,3)*Power(t2,2) - 
                  725*Power(t1,2)*Power(t2,3) + 173*t1*Power(t2,4) + 
                  368*Power(t2,5)) + 
               Power(s2,3)*(-590*Power(t1,6) + 279*Power(t1,5)*t2 + 
                  3776*Power(t1,4)*Power(t2,2) - 
                  2186*Power(t1,3)*Power(t2,3) + 
                  732*Power(t1,2)*Power(t2,4) - 1547*t1*Power(t2,5) + 
                  258*Power(t2,6)) + 
               Power(s2,2)*(161*Power(t1,7) - 4*Power(t1,6)*t2 - 
                  3123*Power(t1,5)*Power(t2,2) + 
                  3230*Power(t1,4)*Power(t2,3) - 
                  1158*Power(t1,3)*Power(t2,4) + 
                  1526*Power(t1,2)*Power(t2,5) - 472*t1*Power(t2,6) - 
                  80*Power(t2,7)) + 
               s2*t1*(-30*Power(t1,7) + 476*Power(t1,6)*t2 - 
                  764*Power(t1,5)*Power(t2,2) + 
                  328*Power(t1,4)*Power(t2,3) + 
                  519*Power(t1,3)*Power(t2,4) - 
                  847*Power(t1,2)*Power(t2,5) + 189*t1*Power(t2,6) + 
                  129*Power(t2,7))) + 
            Power(s1,2)*(-2*Power(t1,4)*Power(t1 - t2,4)*t2*
                (8*Power(t1,2) + 3*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,8)*(Power(t1,3) - 16*Power(t1,2)*t2 + 
                  46*t1*Power(t2,2) - 26*Power(t2,3)) + 
               Power(s2,7)*(-45*Power(t1,4) + 82*Power(t1,3)*t2 - 
                  294*Power(t1,2)*Power(t2,2) + 177*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,6)*(193*Power(t1,5) - 152*Power(t1,4)*t2 + 
                  554*Power(t1,3)*Power(t2,2) - 
                  329*Power(t1,2)*Power(t2,3) - 223*t1*Power(t2,4) + 
                  172*Power(t2,5)) + 
               s2*t1*Power(t1 - t2,3)*
                (20*Power(t1,6) + 90*Power(t1,5)*t2 - 
                  78*Power(t1,4)*Power(t2,2) - 
                  19*Power(t1,3)*Power(t2,3) + 
                  111*Power(t1,2)*Power(t2,4) + 73*t1*Power(t2,5) + 
                  10*Power(t2,6)) + 
               Power(s2,5)*(-298*Power(t1,6) - 25*Power(t1,5)*t2 + 
                  84*Power(t1,4)*Power(t2,2) - 
                  342*Power(t1,3)*Power(t2,3) + 
                  1207*Power(t1,2)*Power(t2,4) - 818*t1*Power(t2,5) + 
                  150*Power(t2,6)) + 
               Power(s2,4)*(159*Power(t1,7) + 489*Power(t1,6)*t2 - 
                  1875*Power(t1,5)*Power(t2,2) + 
                  2160*Power(t1,4)*Power(t2,3) - 
                  1791*Power(t1,3)*Power(t2,4) + 
                  1141*Power(t1,2)*Power(t2,5) - 331*t1*Power(t2,6) - 
                  72*Power(t2,7)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (84*Power(t1,7) - 173*Power(t1,6)*t2 + 
                  292*Power(t1,5)*Power(t2,2) + 
                  711*Power(t1,4)*Power(t2,3) - 
                  57*Power(t1,3)*Power(t2,4) - 
                  17*Power(t1,2)*Power(t2,5) - 134*t1*Power(t2,6) - 
                  6*Power(t2,7)) + 
               Power(s2,3)*(54*Power(t1,8) - 730*Power(t1,7)*t2 + 
                  2390*Power(t1,6)*Power(t2,2) - 
                  2149*Power(t1,5)*Power(t2,3) + 
                  296*Power(t1,4)*Power(t2,4) + 
                  98*Power(t1,3)*Power(t2,5) - 
                  152*Power(t1,2)*Power(t2,6) + 265*t1*Power(t2,7) - 
                  72*Power(t2,8))) + 
            Power(s1,3)*(Power(s2,8)*
                (Power(t1,2) - 28*t1*t2 + 22*Power(t2,2)) + 
               Power(s2,7)*(49*Power(t1,3) + 81*Power(t1,2)*t2 - 
                  171*t1*Power(t2,2) - 12*Power(t2,3)) + 
               2*Power(t1,4)*Power(t1 - t2,3)*
                (10*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  33*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,6)*(-329*Power(t1,4) + 269*Power(t1,3)*t2 + 
                  232*Power(t1,2)*Power(t2,2) + 349*t1*Power(t2,3) - 
                  280*Power(t2,4)) + 
               Power(s2,5)*(781*Power(t1,5) - 1569*Power(t1,4)*t2 + 
                  829*Power(t1,3)*Power(t2,2) - 
                  1586*Power(t1,2)*Power(t2,3) + 1413*t1*Power(t2,4) - 
                  234*Power(t2,5)) - 
               s2*t1*Power(t1 - t2,2)*
                (132*Power(t1,6) - 42*Power(t1,5)*t2 - 
                  154*Power(t1,4)*Power(t2,2) + 
                  243*Power(t1,3)*Power(t2,3) + 
                  69*Power(t1,2)*Power(t2,4) - 237*t1*Power(t2,5) - 
                  56*Power(t2,6)) + 
               Power(s2,4)*(-802*Power(t1,6) + 2993*Power(t1,5)*t2 - 
                  3359*Power(t1,4)*Power(t2,2) + 
                  2424*Power(t1,3)*Power(t2,3) - 
                  1741*Power(t1,2)*Power(t2,4) + 401*t1*Power(t2,5) + 
                  230*Power(t2,6)) + 
               Power(s2,3)*(297*Power(t1,7) - 2472*Power(t1,6)*t2 + 
                  3171*Power(t1,5)*Power(t2,2) - 
                  274*Power(t1,4)*Power(t2,3) - 
                  426*Power(t1,3)*Power(t2,4) + 
                  682*Power(t1,2)*Power(t2,5) - 962*t1*Power(t2,6) + 
                  184*Power(t2,7)) + 
               Power(s2,2)*(116*Power(t1,8) + 479*Power(t1,7)*t2 - 
                  542*Power(t1,6)*Power(t2,2) - 
                  1486*Power(t1,5)*Power(t2,3) + 
                  1718*Power(t1,4)*Power(t2,4) - 
                  949*Power(t1,3)*Power(t2,5) + 
                  1034*Power(t1,2)*Power(t2,6) - 336*t1*Power(t2,7) - 
                  34*Power(t2,8))) + 
            s1*(2*Power(t1,5)*Power(t1 - t2,5)*Power(t2,2) + 
               5*Power(s2,8)*t2*
                (-Power(t1,3) + Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
                  2*Power(t2,3)) - 
               2*s2*Power(t1,2)*Power(t1 - t2,4)*t2*
                (4*Power(t1,4) - 31*Power(t1,2)*Power(t2,2) - 
                  20*t1*Power(t2,3) - 3*Power(t2,4)) + 
               Power(s2,7)*(-2*Power(t1,5) + 44*Power(t1,4)*t2 - 
                  59*Power(t1,3)*Power(t2,2) + 
                  134*Power(t1,2)*Power(t2,3) - 49*t1*Power(t2,4) + 
                  2*Power(t2,5)) + 
               Power(s2,6)*(4*Power(t1,6) - 195*Power(t1,5)*t2 + 
                  187*Power(t1,4)*Power(t2,2) - 
                  163*Power(t1,3)*Power(t2,3) + 
                  20*Power(t1,2)*Power(t2,4) + 57*t1*Power(t2,5) - 
                  42*Power(t2,6)) + 
               Power(s2,2)*t1*Power(t1 - t2,3)*
                (5*Power(t1,6) + 4*Power(t1,5)*t2 - 
                  292*Power(t1,4)*Power(t2,2) - 
                  299*Power(t1,3)*Power(t2,3) + 
                  112*Power(t1,2)*Power(t2,4) + 59*t1*Power(t2,5) + 
                  18*Power(t2,6)) - 
               Power(s2,3)*Power(t1 - t2,2)*
                (14*Power(t1,7) - 159*Power(t1,6)*t2 - 
                  796*Power(t1,5)*Power(t2,2) + 
                  134*Power(t1,4)*Power(t2,3) + 
                  302*Power(t1,3)*Power(t2,4) - 
                  32*Power(t1,2)*Power(t2,5) - 6*t1*Power(t2,6) - 
                  12*Power(t2,7)) - 
               Power(s2,5)*(6*Power(t1,7) - 405*Power(t1,6)*t2 + 
                  171*Power(t1,5)*Power(t2,2) + 
                  400*Power(t1,4)*Power(t2,3) - 
                  471*Power(t1,3)*Power(t2,4) + 
                  357*Power(t1,2)*Power(t2,5) - 166*t1*Power(t2,6) + 
                  38*Power(t2,7)) + 
               Power(s2,4)*(13*Power(t1,8) - 417*Power(t1,7)*t2 - 
                  174*Power(t1,6)*Power(t2,2) + 
                  1443*Power(t1,5)*Power(t2,3) - 
                  1128*Power(t1,4)*Power(t2,4) + 
                  336*Power(t1,3)*Power(t2,5) - 
                  161*Power(t1,2)*Power(t2,6) + 80*t1*Power(t2,7) + 
                  8*Power(t2,8)))) + 
         Power(s,3)*(Power(s1,10)*s2*t1*(s2 + t1)*(s2 - t1 + t2) - 
            Power(s1,9)*s2*(2*Power(s2,3)*(6*t1 + t2) - 
               Power(t1,2)*(8*Power(t1,2) + t1*t2 - 7*Power(t2,2)) + 
               Power(s2,2)*(-33*Power(t1,2) + 15*t1*t2 + 
                  2*Power(t2,2)) + 
               s2*t1*(29*Power(t1,2) - 17*t1*t2 + 5*Power(t2,2))) + 
            2*Power(s2,2)*t1*(t1 - t2)*Power(t2,2)*
             Power(s2 - t1 + t2,2)*
             (Power(s2,4)*t2*(2*t1 + t2) + 
               Power(t1,3)*t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
               s2*Power(t1,2)*
                (-Power(t1,3) + 7*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  12*Power(t2,3)) - 
               Power(s2,2)*t1*
                (Power(t1,3) + 10*Power(t1,2)*t2 - 20*t1*Power(t2,2) - 
                  9*Power(t2,3)) + 
               Power(s2,3)*(2*Power(t1,3) - 3*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) - Power(t2,3))) + 
            Power(s1,8)*(2*Power(s2,5)*(t1 - 3*t2) + 
               6*Power(t1,5)*(t1 - t2) + 
               Power(s2,4)*(41*Power(t1,2) + 77*t1*t2 + 
                  12*Power(t2,2)) + 
               Power(s2,2)*t1*
                (130*Power(t1,3) + 109*Power(t1,2)*t2 - 
                  90*t1*Power(t2,2) - 12*Power(t2,3)) + 
               Power(s2,3)*(-167*Power(t1,3) - 183*Power(t1,2)*t2 + 
                  39*t1*Power(t2,2) + 18*Power(t2,3)) + 
               s2*Power(t1,2)*
                (-12*Power(t1,3) - 62*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + 38*Power(t2,3))) + 
            Power(s1,7)*(Power(s2,6)*(29*t1 + 8*t2) - 
               6*Power(s2,5)*
                (25*Power(t1,2) + 4*t1*t2 - 8*Power(t2,2)) + 
               6*Power(t1,5)*(-3*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
               Power(s2,4)*(405*Power(t1,3) - 206*Power(t1,2)*t2 - 
                  274*t1*Power(t2,2) - 26*Power(t2,3)) + 
               s2*Power(t1,2)*
                (50*Power(t1,4) - 35*Power(t1,3)*t2 + 
                  213*Power(t1,2)*Power(t2,2) + 17*t1*Power(t2,3) - 
                  134*Power(t2,4)) - 
               Power(s2,3)*(619*Power(t1,4) - 926*Power(t1,3)*t2 - 
                  545*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
                  66*Power(t2,4)) + 
               Power(s2,2)*t1*
                (305*Power(t1,4) - 826*Power(t1,3)*t2 - 
                  270*Power(t1,2)*Power(t2,2) + 168*t1*Power(t2,3) + 
                  122*Power(t2,4))) - 
            Power(s1,6)*(Power(s2,7)*(13*t1 - 8*t2) + 
               Power(s2,6)*(9*Power(t1,2) + 145*t1*t2 + 
                  40*Power(t2,2)) - 
               2*Power(s2,5)*
                (79*Power(t1,3) + 306*Power(t1,2)*t2 + 
                  97*t1*Power(t2,2) - 83*Power(t2,3)) + 
               2*Power(t1,5)*
                (17*Power(t1,3) - 57*Power(t1,2)*t2 + 
                  45*t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,4)*(275*Power(t1,4) + 1365*Power(t1,3)*t2 - 
                  150*Power(t1,2)*Power(t2,2) - 741*t1*Power(t2,3) - 
                  12*Power(t2,4)) + 
               s2*Power(t1,2)*
                (-109*Power(t1,5) + 263*Power(t1,4)*t2 - 
                  352*Power(t1,3)*Power(t2,2) + 
                  494*Power(t1,2)*Power(t2,3) + 113*t1*Power(t2,4) - 
                  275*Power(t2,5)) + 
               Power(s2,3)*(-205*Power(t1,5) - 2067*Power(t1,4)*t2 + 
                  1576*Power(t1,3)*Power(t2,2) + 
                  1271*Power(t1,2)*Power(t2,3) + 102*t1*Power(t2,4) - 
                  130*Power(t2,5)) + 
               Power(s2,2)*t1*
                (137*Power(t1,5) + 1080*Power(t1,4)*t2 - 
                  1578*Power(t1,3)*Power(t2,2) - 
                  800*Power(t1,2)*Power(t2,3) + 101*t1*Power(t2,4) + 
                  325*Power(t2,5))) + 
            Power(s1,5)*(-(Power(s2,8)*(11*t1 + 6*t2)) + 
               3*Power(s2,7)*
                (35*Power(t1,2) + 25*t1*t2 - 16*Power(t2,2)) + 
               Power(s2,6)*(-497*Power(t1,3) - 9*Power(t1,2)*t2 + 
                  359*t1*Power(t2,2) + 74*Power(t2,3)) + 
               10*Power(t1,5)*
                (Power(t1,4) + 5*Power(t1,3)*t2 - 
                  18*Power(t1,2)*Power(t2,2) + 16*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,5)*(1140*Power(t1,4) - 718*Power(t1,3)*t2 - 
                  979*Power(t1,2)*Power(t2,2) - 570*t1*Power(t2,3) + 
                  316*Power(t2,4)) + 
               Power(s2,4)*(-1331*Power(t1,5) + 1904*Power(t1,4)*t2 + 
                  1175*Power(t1,3)*Power(t2,2) + 
                  810*Power(t1,2)*Power(t2,3) - 1397*t1*Power(t2,4) + 
                  50*Power(t2,5)) + 
               s2*Power(t1,2)*
                (-80*Power(t1,6) - 138*Power(t1,5)*t2 + 
                  562*Power(t1,4)*Power(t2,2) - 
                  980*Power(t1,3)*Power(t2,3) + 
                  819*Power(t1,2)*Power(t2,4) + 198*t1*Power(t2,5) - 
                  331*Power(t2,6)) + 
               Power(s2,3)*(855*Power(t1,6) - 2113*Power(t1,5)*t2 - 
                  1224*Power(t1,4)*Power(t2,2) + 
                  16*Power(t1,3)*Power(t2,3) + 
                  2219*Power(t1,2)*Power(t2,4) + 83*t1*Power(t2,5) - 
                  150*Power(t2,6)) + 
               Power(s2,2)*t1*
                (-189*Power(t1,6) + 1038*Power(t1,5)*t2 + 
                  445*Power(t1,4)*Power(t2,2) - 
                  351*Power(t1,3)*Power(t2,3) - 
                  1703*Power(t1,2)*Power(t2,4) - 55*t1*Power(t2,5) + 
                  439*Power(t2,6))) + 
            Power(s1,4)*(Power(s2,9)*(4*t1 - 2*t2) + 
               Power(s2,8)*(-8*Power(t1,2) + 27*t1*t2 + 
                  24*Power(t2,2)) - 
               Power(s2,7)*(25*Power(t1,3) + 176*Power(t1,2)*t2 + 
                  246*t1*Power(t2,2) - 118*Power(t2,3)) + 
               2*Power(t1,5)*Power(t1 - t2,2)*
                (10*Power(t1,3) - 18*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s2,6)*(30*Power(t1,4) + 704*Power(t1,3)*t2 + 
                  608*Power(t1,2)*Power(t2,2) - 653*t1*Power(t2,3) - 
                  52*Power(t2,4)) + 
               Power(s2,5)*(137*Power(t1,5) - 1566*Power(t1,4)*t2 - 
                  335*Power(t1,3)*Power(t2,2) + 
                  1199*Power(t1,2)*Power(t2,3) + 715*t1*Power(t2,4) - 
                  354*Power(t2,5)) - 
               Power(s2,4)*(281*Power(t1,6) - 2140*Power(t1,5)*t2 + 
                  1856*Power(t1,4)*Power(t2,2) + 
                  72*Power(t1,3)*Power(t2,3) + 
                  1873*Power(t1,2)*Power(t2,4) - 1632*t1*Power(t2,5) + 
                  108*Power(t2,6)) + 
               Power(s2,2)*t1*
                (-11*Power(t1,7) + 773*Power(t1,6)*t2 - 
                  2324*Power(t1,5)*Power(t2,2) + 
                  1791*Power(t1,4)*Power(t2,3) - 
                  1944*Power(t1,3)*Power(t2,4) + 
                  1999*Power(t1,2)*Power(t2,5) + 68*t1*Power(t2,6) - 
                  330*Power(t2,7)) - 
               s2*Power(t1,2)*
                (88*Power(t1,7) - 308*Power(t1,6)*t2 + 
                  162*Power(t1,5)*Power(t2,2) + 
                  553*Power(t1,4)*Power(t2,3) - 
                  1278*Power(t1,3)*Power(t2,4) + 
                  866*Power(t1,2)*Power(t2,5) + 149*t1*Power(t2,6) - 
                  232*Power(t2,7)) + 
               Power(s2,3)*(222*Power(t1,7) - 2065*Power(t1,6)*t2 + 
                  3912*Power(t1,5)*Power(t2,2) - 
                  1940*Power(t1,4)*Power(t2,3) + 
                  2619*Power(t1,3)*Power(t2,4) - 
                  2495*Power(t1,2)*Power(t2,5) + 99*t1*Power(t2,6) + 
                  102*Power(t2,7))) + 
            Power(s1,3)*(-2*Power(t1,5)*Power(t1 - t2,3)*t2*
                (12*Power(t1,2) - 9*t1*t2 - 2*Power(t2,2)) + 
               2*Power(s2,9)*
                (2*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,8)*(5*Power(t1,3) + 78*Power(t1,2)*t2 - 
                  34*t1*Power(t2,2) - 36*Power(t2,3)) + 
               Power(s2,7)*(-133*Power(t1,4) - 154*Power(t1,3)*t2 + 
                  84*Power(t1,2)*Power(t2,2) + 377*t1*Power(t2,3) - 
                  146*Power(t2,4)) + 
               Power(s2,6)*(440*Power(t1,5) - 51*Power(t1,4)*t2 + 
                  43*Power(t1,3)*Power(t2,2) - 
                  1393*Power(t1,2)*Power(t2,3) + 795*t1*Power(t2,4) - 
                  8*Power(t2,5)) + 
               s2*Power(t1,2)*Power(t1 - t2,2)*
                (30*Power(t1,6) + 80*Power(t1,5)*t2 - 
                  118*Power(t1,4)*Power(t2,2) + 
                  13*Power(t1,3)*Power(t2,3) + 
                  317*Power(t1,2)*Power(t2,4) - 139*t1*Power(t2,5) - 
                  88*Power(t2,6)) + 
               Power(s2,5)*(-645*Power(t1,6) + 611*Power(t1,5)*t2 - 
                  696*Power(t1,4)*Power(t2,2) + 
                  2607*Power(t1,3)*Power(t2,3) - 
                  1406*Power(t1,2)*Power(t2,4) - 350*t1*Power(t2,5) + 
                  232*Power(t2,6)) + 
               Power(s2,4)*(439*Power(t1,7) - 903*Power(t1,6)*t2 + 
                  721*Power(t1,5)*Power(t2,2) - 
                  1143*Power(t1,4)*Power(t2,3) + 
                  159*Power(t1,3)*Power(t2,4) + 
                  1524*Power(t1,2)*Power(t2,5) - 1095*t1*Power(t2,6) + 
                  98*Power(t2,7)) + 
               Power(s2,3)*(-50*Power(t1,8) + 266*Power(t1,7)*t2 + 
                  870*Power(t1,6)*Power(t2,2) - 
                  2413*Power(t1,5)*Power(t2,3) + 
                  2537*Power(t1,4)*Power(t2,4) - 
                  2666*Power(t1,3)*Power(t2,5) + 
                  1625*Power(t1,2)*Power(t2,6) - 195*t1*Power(t2,7) - 
                  38*Power(t2,8)) + 
               Power(s2,2)*t1*
                (-90*Power(t1,8) + 182*Power(t1,7)*t2 - 
                  1018*Power(t1,6)*Power(t2,2) + 
                  2203*Power(t1,5)*Power(t2,3) - 
                  2161*Power(t1,4)*Power(t2,4) + 
                  1928*Power(t1,3)*Power(t2,5) - 
                  1206*Power(t1,2)*Power(t2,6) + 30*t1*Power(t2,7) + 
                  132*Power(t2,8))) - 
            s1*s2*(2*Power(s2,8)*(2*t1 - t2)*Power(t2,3) - 
               Power(t1,3)*Power(t1 - t2,4)*Power(t2,2)*
                (Power(t1,3) - 13*Power(t1,2)*t2 - 15*t1*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(s2,7)*t2*
                (-2*Power(t1,4) + 11*Power(t1,3)*t2 - 
                  52*Power(t1,2)*Power(t2,2) + 9*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s2*Power(t1,2)*Power(t1 - t2,3)*t2*
                (5*Power(t1,5) - 59*Power(t1,4)*t2 - 
                  173*Power(t1,3)*Power(t2,2) + 
                  9*Power(t1,2)*Power(t2,3) + 32*t1*Power(t2,4) + 
                  16*Power(t2,5)) + 
               Power(s2,6)*(-Power(t1,6) + 18*Power(t1,5)*t2 - 
                  59*Power(t1,4)*Power(t2,2) + 
                  156*Power(t1,3)*Power(t2,3) + 
                  28*Power(t1,2)*Power(t2,4) - 62*t1*Power(t2,5) + 
                  22*Power(t2,6)) + 
               Power(s2,2)*t1*Power(t1 - t2,2)*t2*
                (7*Power(t1,6) + 298*Power(t1,5)*t2 + 
                  134*Power(t1,4)*Power(t2,2) - 
                  202*Power(t1,3)*Power(t2,3) + 
                  44*Power(t1,2)*Power(t2,4) - 18*t1*Power(t2,5) + 
                  24*Power(t2,6)) + 
               Power(s2,5)*(3*Power(t1,7) - 70*Power(t1,6)*t2 + 
                  55*Power(t1,5)*Power(t2,2) - 
                  55*Power(t1,4)*Power(t2,3) - 
                  266*Power(t1,3)*Power(t2,4) + 
                  322*Power(t1,2)*Power(t2,5) - 127*t1*Power(t2,6) + 
                  10*Power(t2,7)) + 
               Power(s2,4)*(-3*Power(t1,8) + 111*Power(t1,7)*t2 + 
                  129*Power(t1,6)*Power(t2,2) - 
                  461*Power(t1,5)*Power(t2,3) + 
                  698*Power(t1,4)*Power(t2,4) - 
                  602*Power(t1,3)*Power(t2,5) + 
                  224*Power(t1,2)*Power(t2,6) - 34*t1*Power(t2,7) - 
                  12*Power(t2,8)) + 
               Power(s2,3)*(Power(t1,9) - 69*Power(t1,8)*t2 - 
                  345*Power(t1,7)*Power(t2,2) + 
                  815*Power(t1,6)*Power(t2,3) - 
                  565*Power(t1,5)*Power(t2,4) + 
                  193*Power(t1,4)*Power(t2,5) - 
                  12*Power(t1,3)*Power(t2,6) - 
                  60*Power(t1,2)*Power(t2,7) + 50*t1*Power(t2,8) - 
                  8*Power(t2,9))) - 
            Power(s1,2)*(-2*Power(t1,5)*Power(t1 - t2,4)*(3*t1 - t2)*
                Power(t2,2) + 
               2*Power(s2,9)*t2*
                (2*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) - 
               Power(s2,8)*(2*Power(t1,4) + 26*Power(t1,3)*t2 - 
                  120*Power(t1,2)*Power(t2,2) + 29*t1*Power(t2,3) + 
                  24*Power(t2,4)) + 
               Power(s2,7)*(-11*Power(t1,5) + 28*Power(t1,4)*t2 - 
                  407*Power(t1,3)*Power(t2,2) + 
                  9*Power(t1,2)*Power(t2,3) + 253*t1*Power(t2,4) - 
                  90*Power(t2,5)) + 
               2*s2*Power(t1,2)*Power(t1 - t2,3)*t2*
                (12*Power(t1,5) - Power(t1,4)*t2 - 
                  25*Power(t1,3)*Power(t2,2) - 
                  8*Power(t1,2)*Power(t2,3) + 26*t1*Power(t2,4) + 
                  7*Power(t2,5)) + 
               Power(s2,6)*(52*Power(t1,6) + 137*Power(t1,5)*t2 + 
                  431*Power(t1,4)*Power(t2,2) + 
                  356*Power(t1,3)*Power(t2,3) - 
                  1097*Power(t1,2)*Power(t2,4) + 514*t1*Power(t2,5) - 
                  28*Power(t2,6)) - 
               Power(s2,2)*t1*Power(t1 - t2,2)*
                (15*Power(t1,7) - 11*Power(t1,6)*t2 - 
                  200*Power(t1,5)*Power(t2,2) + 
                  480*Power(t1,4)*Power(t2,3) - 
                  85*Power(t1,3)*Power(t2,4) + 
                  144*Power(t1,2)*Power(t2,5) - 97*t1*Power(t2,6) - 
                  22*Power(t2,7)) + 
               Power(s2,5)*(-51*Power(t1,7) - 484*Power(t1,6)*t2 + 
                  391*Power(t1,5)*Power(t2,2) - 
                  1353*Power(t1,4)*Power(t2,3) + 
                  2248*Power(t1,3)*Power(t2,4) - 
                  964*Power(t1,2)*Power(t2,5) + 3*t1*Power(t2,6) + 
                  82*Power(t2,7)) + 
               Power(s2,4)*(-15*Power(t1,8) + 681*Power(t1,7)*t2 - 
                  1223*Power(t1,6)*Power(t2,2) + 
                  1496*Power(t1,5)*Power(t2,3) - 
                  1511*Power(t1,4)*Power(t2,4) + 
                  376*Power(t1,3)*Power(t2,5) + 
                  488*Power(t1,2)*Power(t2,6) - 378*t1*Power(t2,7) + 
                  44*Power(t2,8)) + 
               Power(s2,3)*(42*Power(t1,9) - 405*Power(t1,8)*t2 + 
                  642*Power(t1,7)*Power(t2,2) + 
                  178*Power(t1,6)*Power(t2,3) - 
                  990*Power(t1,5)*Power(t2,4) + 
                  1043*Power(t1,4)*Power(t2,5) - 
                  928*Power(t1,3)*Power(t2,6) + 
                  539*Power(t1,2)*Power(t2,7) - 115*t1*Power(t2,8) - 
                  6*Power(t2,9)))) + 
         Power(s,2)*(Power(s1,10)*s2*t1*
             (Power(s2,2) + s2*t1 + 2*Power(t1,2))*(s2 - t1 + t2) + 
            2*Power(s2,3)*Power(t1,2)*(t1 - t2)*Power(t2,3)*
             Power(s2 - t1 + t2,2)*
             (Power(t1,2)*(2*t1 - 3*t2)*t2 + Power(s2,3)*(t1 + 2*t2) + 
               Power(s2,2)*(Power(t1,2) - 8*t1*t2 - 2*Power(t2,2)) + 
               s2*t1*(-2*Power(t1,2) + 3*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,9)*s2*(-2*Power(s2,4)*(t1 - t2) + 
               Power(t1,3)*(Power(t1,2) + 24*t1*t2 - 21*Power(t2,2)) + 
               Power(s2,2)*t1*
                (65*Power(t1,2) - 2*t1*t2 - 9*Power(t2,2)) + 
               Power(s2,3)*(-20*Power(t1,2) - 13*t1*t2 + 
                  2*Power(t2,2)) + 
               s2*Power(t1,2)*
                (-44*Power(t1,2) + 7*t1*t2 + 11*Power(t2,2))) - 
            Power(s1,8)*(4*Power(s2,6)*t1 + 6*Power(t1,6)*(-t1 + t2) + 
               Power(s2,5)*(11*Power(t1,2) - 10*t1*t2 + 
                  14*Power(t2,2)) + 
               s2*Power(t1,3)*
                (13*Power(t1,3) - 11*Power(t1,2)*t2 + 
                  131*t1*Power(t2,2) - 101*Power(t2,3)) + 
               Power(s2,4)*(12*Power(t1,3) - 99*Power(t1,2)*t2 - 
                  74*t1*Power(t2,2) + 14*Power(t2,3)) + 
               2*Power(s2,2)*Power(t1,2)*
                (10*Power(t1,3) - 125*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + 56*Power(t2,3)) + 
               Power(s2,3)*(-54*Power(t1,4) + 378*Power(t1,3)*t2 + 
                  30*Power(t1,2)*Power(t2,2) - 46*t1*Power(t2,3))) + 
            Power(s1,7)*(Power(s2,7)*(6*t1 - 4*t2) + 
               Power(s2,6)*(46*Power(t1,2) + 45*t1*t2 - 6*Power(t2,2)) + 
               2*Power(t1,6)*(Power(t1,2) - 11*t1*t2 + 10*Power(t2,2)) + 
               Power(s2,5)*(-263*Power(t1,3) + 22*Power(t1,2)*t2 - 
                  2*t1*Power(t2,2) + 40*Power(t2,3)) + 
               s2*Power(t1,3)*
                (Power(t1,4) + 20*Power(t1,3)*t2 - 
                  93*Power(t1,2)*Power(t2,2) + 417*t1*Power(t2,3) - 
                  270*Power(t2,4)) + 
               Power(s2,3)*t1*
                (-364*Power(t1,4) - 219*Power(t1,3)*t2 + 
                  975*Power(t1,2)*Power(t2,2) + 215*t1*Power(t2,3) - 
                  143*Power(t2,4)) + 
               Power(s2,4)*(408*Power(t1,4) + 37*Power(t1,3)*t2 - 
                  266*Power(t1,2)*Power(t2,2) - 224*t1*Power(t2,3) + 
                  42*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (164*Power(t1,4) + 5*Power(t1,3)*t2 - 
                  522*Power(t1,2)*Power(t2,2) - 364*t1*Power(t2,3) + 
                  382*Power(t2,4))) + 
            Power(s1,6)*(3*Power(s2,8)*t1 + 
               Power(s2,7)*(9*Power(t1,2) - 28*t1*t2 + 22*Power(t2,2)) + 
               2*Power(t1,6)*
                (-7*Power(t1,3) + 11*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) - 10*Power(t2,3)) + 
               Power(s2,6)*(72*Power(t1,3) - 138*Power(t1,2)*t2 - 
                  195*t1*Power(t2,2) + 34*Power(t2,3)) + 
               Power(s2,5)*(-333*Power(t1,4) + 976*Power(t1,3)*t2 + 
                  234*Power(t1,2)*Power(t2,2) - 113*t1*Power(t2,3) - 
                  58*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (132*Power(t1,5) - 727*Power(t1,4)*t2 + 
                  116*Power(t1,3)*Power(t2,2) + 
                  385*Power(t1,2)*Power(t2,3) + 1117*t1*Power(t2,4) - 
                  685*Power(t2,5)) + 
               Power(s2,4)*(445*Power(t1,5) - 1465*Power(t1,4)*t2 - 
                  459*Power(t1,3)*Power(t2,2) + 
                  651*Power(t1,2)*Power(t2,3) + 381*t1*Power(t2,4) - 
                  70*Power(t2,5)) + 
               Power(s2,3)*t1*
                (-373*Power(t1,5) + 1315*Power(t1,4)*t2 + 
                  733*Power(t1,3)*Power(t2,2) - 
                  1568*Power(t1,2)*Power(t2,3) - 536*t1*Power(t2,4) + 
                  270*Power(t2,5)) + 
               s2*Power(t1,3)*
                (59*Power(t1,5) - 105*Power(t1,4)*t2 + 
                  83*Power(t1,3)*Power(t2,2) + 
                  262*Power(t1,2)*Power(t2,3) - 795*t1*Power(t2,4) + 
                  430*Power(t2,5))) + 
            Power(s1,5)*(Power(s2,9)*(-4*t1 + 2*t2) - 
               4*Power(s2,8)*(5*Power(t1,2) + 7*t1*t2 - Power(t2,2)) + 
               8*Power(s2,7)*
                (23*Power(t1,3) + 4*t1*Power(t2,2) - 6*Power(t2,3)) + 
               2*Power(t1,7)*
                (-5*Power(t1,3) + 27*Power(t1,2)*t2 - 
                  40*t1*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s2,6)*(-405*Power(t1,4) - 170*Power(t1,3)*t2 + 
                  117*Power(t1,2)*Power(t2,2) + 420*t1*Power(t2,3) - 
                  80*Power(t2,4)) + 
               Power(s2,5)*(455*Power(t1,5) + 735*Power(t1,4)*t2 - 
                  1009*Power(t1,3)*Power(t2,2) - 
                  1037*Power(t1,2)*Power(t2,3) + 374*t1*Power(t2,4) + 
                  40*Power(t2,5)) + 
               s2*Power(t1,3)*
                (22*Power(t1,6) - 192*Power(t1,5)*t2 + 
                  386*Power(t1,4)*Power(t2,2) - 
                  345*Power(t1,3)*Power(t2,3) - 
                  335*Power(t1,2)*Power(t2,4) + 900*t1*Power(t2,5) - 
                  417*Power(t2,6)) + 
               Power(s2,3)*t1*
                (61*Power(t1,6) + 380*Power(t1,5)*t2 - 
                  676*Power(t1,4)*Power(t2,2) - 
                  2196*Power(t1,3)*Power(t2,3) + 
                  1902*Power(t1,2)*Power(t2,4) + 648*t1*Power(t2,5) - 
                  311*Power(t2,6)) + 
               Power(s2,4)*(-327*Power(t1,6) - 547*Power(t1,5)*t2 + 
                  971*Power(t1,4)*Power(t2,2) + 
                  2049*Power(t1,3)*Power(t2,3) - 
                  1306*Power(t1,2)*Power(t2,4) - 351*t1*Power(t2,5) + 
                  70*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*
                (44*Power(t1,6) - 308*Power(t1,5)*t2 + 
                  848*Power(t1,4)*Power(t2,2) + 
                  32*Power(t1,3)*Power(t2,3) + 
                  143*Power(t1,2)*Power(t2,4) - 1601*t1*Power(t2,5) + 
                  719*Power(t2,6))) + 
            Power(s1,4)*(Power(s2,9)*
                (-4*Power(t1,2) + 13*t1*t2 - 8*Power(t2,2)) + 
               2*Power(t1,6)*Power(t1 - t2,2)*t2*
                (8*Power(t1,2) - 15*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,8)*(-23*Power(t1,3) + 10*Power(t1,2)*t2 + 
                  91*t1*Power(t2,2) - 16*Power(t2,3)) + 
               Power(s2,7)*(140*Power(t1,4) - 295*Power(t1,3)*t2 - 
                  237*Power(t1,2)*Power(t2,2) + 55*t1*Power(t2,3) + 
                  52*Power(t2,4)) + 
               Power(s2,6)*(-294*Power(t1,5) + 579*Power(t1,4)*t2 + 
                  685*Power(t1,3)*Power(t2,2) - 
                  136*Power(t1,2)*Power(t2,3) - 479*t1*Power(t2,4) + 
                  100*Power(t2,5)) + 
               Power(s2,5)*(403*Power(t1,6) - 626*Power(t1,5)*t2 - 
                  1142*Power(t1,4)*Power(t2,2) + 
                  13*Power(t1,3)*Power(t2,3) + 
                  1701*Power(t1,2)*Power(t2,4) - 564*t1*Power(t2,5) - 
                  2*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*
                (36*Power(t1,7) + 3*Power(t1,6)*t2 - 
                  61*Power(t1,5)*Power(t2,2) + 
                  140*Power(t1,4)*Power(t2,3) - 
                  552*Power(t1,3)*Power(t2,4) - 
                  316*Power(t1,2)*Power(t2,5) + 1198*t1*Power(t2,6) - 
                  446*Power(t2,7)) + 
               Power(s2,4)*(-361*Power(t1,7) + 944*Power(t1,6)*t2 - 
                  437*Power(t1,5)*Power(t2,2) + 
                  1735*Power(t1,4)*Power(t2,3) - 
                  3739*Power(t1,3)*Power(t2,4) + 
                  1688*Power(t1,2)*Power(t2,5) + 128*t1*Power(t2,6) - 
                  42*Power(t2,7)) + 
               Power(s2,3)*t1*
                (123*Power(t1,7) - 656*Power(t1,6)*t2 + 
                  1198*Power(t1,5)*Power(t2,2) - 
                  2232*Power(t1,4)*Power(t2,3) + 
                  3688*Power(t1,3)*Power(t2,4) - 
                  1818*Power(t1,2)*Power(t2,5) - 370*t1*Power(t2,6) + 
                  214*Power(t2,7)) + 
               s2*Power(t1,3)*
                (-20*Power(t1,7) + 222*Power(t1,5)*Power(t2,2) - 
                  569*Power(t1,4)*Power(t2,3) + 
                  538*Power(t1,3)*Power(t2,4) + 
                  169*Power(t1,2)*Power(t2,5) - 581*t1*Power(t2,6) + 
                  241*Power(t2,7))) + 
            s1*s2*t2*(-2*Power(t1,4)*Power(t1 - t2,4)*Power(t2,2)*
                (Power(t1,2) + Power(t2,2)) + 
               s2*Power(t1,3)*Power(t1 - t2,3)*Power(t2,2)*
                (34*Power(t1,3) + 9*Power(t1,2)*t2 + 5*t1*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(s2,8)*t2*
                (4*Power(t1,3) - 8*Power(t1,2)*t2 - t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,7)*(Power(t1,5) - 25*Power(t1,4)*t2 + 
                  39*Power(t1,3)*Power(t2,2) + 
                  32*Power(t1,2)*Power(t2,3) - 25*t1*Power(t2,4) + 
                  4*Power(t2,5)) - 
               Power(s2,2)*Power(t1,2)*Power(t1 - t2,2)*t2*
                (25*Power(t1,5) + 69*Power(t1,4)*t2 - 
                  53*Power(t1,3)*Power(t2,2) + 
                  68*Power(t1,2)*Power(t2,3) - 43*t1*Power(t2,4) + 
                  16*Power(t2,5)) - 
               Power(s2,6)*t1*
                (2*Power(t1,5) - 49*Power(t1,4)*t2 + 
                  26*Power(t1,3)*Power(t2,2) + 
                  202*Power(t1,2)*Power(t2,3) - 158*t1*Power(t2,4) + 
                  43*Power(t2,5)) - 
               Power(s2,5)*t2*
                (3*Power(t1,6) + 142*Power(t1,5)*t2 - 
                  538*Power(t1,4)*Power(t2,2) + 
                  441*Power(t1,3)*Power(t2,3) - 
                  115*Power(t1,2)*Power(t2,4) + t1*Power(t2,5) + 
                  4*Power(t2,6)) - 
               Power(s2,3)*t1*
                (Power(t1,8) - 86*Power(t1,7)*t2 + 
                  189*Power(t1,6)*Power(t2,2) - 
                  202*Power(t1,5)*Power(t2,3) + 
                  3*Power(t1,4)*Power(t2,4) + 
                  248*Power(t1,3)*Power(t2,5) - 
                  220*Power(t1,2)*Power(t2,6) + 81*t1*Power(t2,7) - 
                  14*Power(t2,8)) + 
               Power(s2,4)*(2*Power(t1,8) - 86*Power(t1,7)*t2 + 
                  313*Power(t1,6)*Power(t2,2) - 
                  638*Power(t1,5)*Power(t2,3) + 
                  459*Power(t1,4)*Power(t2,4) - 
                  31*Power(t1,3)*Power(t2,5) - 
                  68*Power(t1,2)*Power(t2,6) + 32*t1*Power(t2,7) - 
                  2*Power(t2,8))) + 
            Power(s1,3)*(-3*Power(s2,9)*(5*t1 - 4*t2)*Power(t2,2) - 
               2*Power(t1,6)*(3*t1 - 2*t2)*Power(t1 - t2,3)*
                Power(t2,2) + 
               Power(s2,8)*(-Power(t1,4) + 75*Power(t1,3)*t2 + 
                  64*Power(t1,2)*Power(t2,2) - 135*t1*Power(t2,3) + 
                  24*Power(t2,4)) + 
               s2*Power(t1,3)*Power(t1 - t2,2)*t2*
                (24*Power(t1,5) + 12*Power(t1,4)*t2 - 
                  108*Power(t1,3)*Power(t2,2) + 
                  169*Power(t1,2)*Power(t2,3) + 37*t1*Power(t2,4) - 
                  76*Power(t2,5)) - 
               Power(s2,7)*(40*Power(t1,5) + 259*Power(t1,4)*t2 + 
                  135*Power(t1,3)*Power(t2,2) - 
                  588*Power(t1,2)*Power(t2,3) + 167*t1*Power(t2,4) + 
                  28*Power(t2,5)) + 
               Power(s2,6)*(136*Power(t1,6) + 234*Power(t1,5)*t2 + 
                  577*Power(t1,4)*Power(t2,2) - 
                  1571*Power(t1,3)*Power(t2,3) + 
                  366*Power(t1,2)*Power(t2,4) + 276*t1*Power(t2,5) - 
                  70*Power(t2,6)) - 
               Power(s2,5)*(136*Power(t1,7) + 87*Power(t1,6)*t2 + 
                  965*Power(t1,5)*Power(t2,2) - 
                  1920*Power(t1,4)*Power(t2,3) - 
                  340*Power(t1,3)*Power(t2,4) + 
                  1331*Power(t1,2)*Power(t2,5) - 454*t1*Power(t2,6) + 
                  16*Power(t2,7)) + 
               Power(s2,3)*t1*
                (42*Power(t1,8) - 280*Power(t1,7)*t2 + 
                  992*Power(t1,6)*Power(t2,2) - 
                  2059*Power(t1,5)*Power(t2,3) + 
                  3227*Power(t1,4)*Power(t2,4) - 
                  3107*Power(t1,3)*Power(t2,5) + 
                  1215*Power(t1,2)*Power(t2,6) + 43*t1*Power(t2,7) - 
                  81*Power(t2,8)) + 
               Power(s2,4)*(14*Power(t1,8) + 260*Power(t1,7)*t2 - 
                  229*Power(t1,6)*Power(t2,2) + 
                  531*Power(t1,5)*Power(t2,3) - 
                  2722*Power(t1,4)*Power(t2,4) + 
                  3250*Power(t1,3)*Power(t2,5) - 
                  1267*Power(t1,2)*Power(t2,6) + 46*t1*Power(t2,7) + 
                  14*Power(t2,8)) + 
               Power(s2,2)*Power(t1,2)*
                (-15*Power(t1,8) + 33*Power(t1,7)*t2 - 
                  211*Power(t1,6)*Power(t2,2) + 
                  497*Power(t1,5)*Power(t2,3) - 
                  743*Power(t1,4)*Power(t2,4) + 
                  669*Power(t1,3)*Power(t2,5) + 
                  52*Power(t1,2)*Power(t2,6) - 434*t1*Power(t2,7) + 
                  152*Power(t2,8))) + 
            Power(s1,2)*s2*(Power(s2,8)*t2*
                (-4*Power(t1,3) + 12*Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
                  8*Power(t2,3)) - 
               Power(t1,3)*Power(t1 - t2,3)*Power(t2,2)*
                (2*Power(t1,4) - 39*Power(t1,2)*Power(t2,2) + 
                  11*t1*Power(t2,3) + 10*Power(t2,4)) - 
               Power(s2,7)*(Power(t1,5) - 36*Power(t1,4)*t2 + 
                  93*Power(t1,3)*Power(t2,2) + 
                  82*Power(t1,2)*Power(t2,3) - 94*t1*Power(t2,4) + 
                  16*Power(t2,5)) + 
               s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                (10*Power(t1,6) - 86*Power(t1,5)*t2 + 
                  97*Power(t1,4)*Power(t2,2) - 
                  74*Power(t1,3)*Power(t2,3) + 
                  103*Power(t1,2)*Power(t2,4) - 22*Power(t2,6)) + 
               Power(s2,6)*(Power(t1,6) - 49*Power(t1,5)*t2 + 
                  197*Power(t1,4)*Power(t2,2) + 
                  424*Power(t1,3)*Power(t2,3) - 
                  514*Power(t1,2)*Power(t2,4) + 145*t1*Power(t2,5) + 
                  6*Power(t2,6)) + 
               Power(s2,5)*(3*Power(t1,7) - 80*Power(t1,6)*t2 + 
                  23*Power(t1,5)*Power(t2,2) - 
                  1197*Power(t1,4)*Power(t2,3) + 
                  1411*Power(t1,3)*Power(t2,4) - 
                  374*Power(t1,2)*Power(t2,5) - 62*t1*Power(t2,6) + 
                  26*Power(t2,7)) + 
               Power(s2,4)*(-5*Power(t1,8) + 207*Power(t1,7)*t2 - 
                  390*Power(t1,6)*Power(t2,2) + 
                  1603*Power(t1,5)*Power(t2,3) - 
                  1633*Power(t1,4)*Power(t2,4) - 
                  6*Power(t1,3)*Power(t2,5) + 
                  486*Power(t1,2)*Power(t2,6) - 189*t1*Power(t2,7) + 
                  10*Power(t2,8)) + 
               Power(s2,2)*t1*t2*
                (4*Power(t1,8) + 211*Power(t1,7)*t2 - 
                  644*Power(t1,6)*Power(t2,2) + 
                  1105*Power(t1,5)*Power(t2,3) - 
                  1480*Power(t1,4)*Power(t2,4) + 
                  1211*Power(t1,3)*Power(t2,5) - 
                  468*Power(t1,2)*Power(t2,6) + 48*t1*Power(t2,7) + 
                  13*Power(t2,8)) + 
               Power(s2,3)*(2*Power(t1,9) - 124*Power(t1,8)*t2 + 
                  148*Power(t1,7)*Power(t2,2) - 
                  432*Power(t1,6)*Power(t2,3) + 
                  23*Power(t1,5)*Power(t2,4) + 
                  1279*Power(t1,4)*Power(t2,5) - 
                  1334*Power(t1,3)*Power(t2,6) + 
                  502*Power(t1,2)*Power(t2,7) - 56*t1*Power(t2,8) - 
                  2*Power(t2,9)))))*Log(s - s2 + t1))/
     (s*Power(s1,2)*Power(s2,2)*Power(t1,2)*Power(s + t1,2)*(s - s2 + t1)*
       (s1 + t1 - t2)*Power(s1 - s2 + t1 - t2,2)*t2*Power(s - s1 + t2,2)*
       (s - s1 - s2 + t2)*(s2 - t1 + t2)) + 
    (16*(Power(s1,7)*Power(s2 + t1,2)*(Power(s2,2) + Power(t1,2)) + 
         Power(s1,6)*(s2 + t1)*
          (2*Power(s2,4) + Power(s2,3)*(t1 - 4*t2) + 
            s2*Power(t1,2)*(3*t1 - 4*t2) + 
            Power(s2,2)*t1*(5*t1 - 4*t2) + Power(t1,3)*(t1 - 2*t2)) + 
         Power(s,6)*s2*Power(t1,2)*(t1 - t2)*(s1 + t1 - t2) + 
         Power(s2,4)*Power(t1,2)*(s2 - t2)*Power(t2,4) - 
         s1*Power(s2,3)*Power(t1,2)*Power(t2,3)*
          (5*Power(s2,2) + Power(t1,2) - 8*s2*t2 - t1*t2 + 2*Power(t2,2)) \
+ Power(s1,5)*(Power(s2,6) + Power(s2,5)*(3*t1 - 7*t2) + 
            Power(t1,4)*t2*(-2*t1 + t2) + 
            Power(s2,4)*(5*Power(t1,2) - 8*t1*t2 + 6*Power(t2,2)) + 
            s2*Power(t1,3)*(2*Power(t1,2) - 9*t1*t2 + 7*Power(t2,2)) + 
            2*Power(s2,2)*Power(t1,2)*
             (5*Power(t1,2) - 10*t1*t2 + 8*Power(t2,2)) + 
            Power(s2,3)*t1*(3*Power(t1,2) - 26*t1*t2 + 14*Power(t2,2))) + 
         Power(s1,2)*Power(s2,2)*Power(t2,2)*
          (Power(s2,4)*(t1 - t2) + 
            Power(s2,2)*t1*(-2*Power(t1,2) - 23*t1*t2 + Power(t2,2)) + 
            Power(s2,3)*(9*Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(t1,2)*(Power(t1,3) - 2*Power(t1,2)*t2 + 
               2*t1*Power(t2,2) - Power(t2,3)) + 
            s2*t1*(5*Power(t1,3) - 7*Power(t1,2)*t2 + 
               16*t1*Power(t2,2) - Power(t2,3))) + 
         Power(s1,4)*(Power(s2,6)*(t1 - 3*t2) + 
            Power(t1,5)*Power(t2,2) - 
            2*s2*Power(t1,3)*t2*
             (2*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,5)*(3*Power(t1,2) - 7*t1*t2 + 9*Power(t2,2)) + 
            Power(s2,2)*Power(t1,2)*
             (2*Power(t1,3) - 19*Power(t1,2)*t2 + 22*t1*Power(t2,2) - 
               16*Power(t2,3)) + 
            Power(s2,3)*t1*(5*Power(t1,3) - 9*Power(t1,2)*t2 + 
               48*t1*Power(t2,2) - 13*Power(t2,3)) - 
            Power(s2,4)*(3*Power(t1,3) + 18*Power(t1,2)*t2 - 
               8*t1*Power(t2,2) + 4*Power(t2,3))) + 
         Power(s1,3)*s2*t2*(Power(s2,5)*(-2*t1 + 3*t2) + 
            Power(s2,4)*(-8*Power(t1,2) + 5*t1*t2 - 5*Power(t2,2)) + 
            Power(t1,3)*t2*(2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(s2,3)*(5*Power(t1,3) + 29*Power(t1,2)*t2 - 
               4*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s2,2)*t1*(-8*Power(t1,3) + 13*Power(t1,2)*t2 - 
               42*t1*Power(t2,2) + 6*Power(t2,3)) + 
            s2*Power(t1,2)*(-3*Power(t1,3) + 12*Power(t1,2)*t2 - 
               11*t1*Power(t2,2) + 7*Power(t2,3))) - 
         Power(s,5)*(s2*Power(t1,2)*(t1 - t2)*
             (2*s2*(t1 - 2*t2) + 3*t2*(-t1 + t2)) + 
            Power(s1,2)*(Power(t1,2)*Power(t1 - t2,2) + 
               s2*t1*(6*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,2) - 2*t1*t2 + Power(t2,2))) + 
            s1*Power(t1,2)*(Power(s2,2)*(2*t1 - 5*t2) + 
               t1*Power(t1 - t2,2) + 
               2*s2*(3*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)))) + 
         Power(s,4)*(Power(s1,3)*
             (-2*Power(s2,3)*t2 + 
               3*Power(s2,2)*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               2*s2*t1*(8*Power(t1,2) - 9*t1*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(5*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2))) + 
            s2*Power(t1,2)*(3*Power(t1 - t2,2)*Power(t2,2) + 
               s2*t2*(-5*Power(t1,2) + 16*t1*t2 - 11*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) - 6*t1*t2 + 6*Power(t2,2))) + 
            s1*Power(t1,2)*(Power(s2,3)*(t1 - 9*t2) - 
               2*t1*Power(t1 - t2,2)*t2 + 
               2*Power(s2,2)*
                (5*Power(t1,2) - 16*t1*t2 + 16*Power(t2,2)) + 
               s2*(2*Power(t1,3) - 20*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) - 15*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,3)*
                (6*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)) + 
               s2*t1*(18*Power(t1,3) - 49*Power(t1,2)*t2 + 
                  29*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*(6*Power(t1,3) - 29*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(t1,2)*(5*Power(t1,3) - 10*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) - 2*Power(t2,3)))) - 
         Power(s,3)*(Power(s1,4)*
             (Power(s2,4) - 6*Power(s2,3)*t2 + 
               Power(s2,2)*(8*Power(t1,2) - 18*t1*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(10*Power(t1,2) - 12*t1*t2 + 
                  3*Power(t2,2)) + 
               s2*t1*(24*Power(t1,2) - 28*t1*t2 + 5*Power(t2,2))) - 
            s2*Power(t1,2)*t2*
             (2*Power(s2,3)*(t1 - 2*t2) + 
               Power(t1 - t2,2)*Power(t2,2) - 
               2*s2*t2*(2*Power(t1,2) - 7*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,2) - 14*t1*t2 + 15*Power(t2,2))) \
+ Power(s1,3)*(Power(s2,4)*(t1 - 6*t2) + 
               Power(s2,3)*(16*Power(t1,2) - 23*t1*t2 + 
                  14*Power(t2,2)) + 
               s2*t1*(32*Power(t1,3) - 89*Power(t1,2)*t2 + 
                  55*t1*Power(t2,2) - 8*Power(t2,3)) + 
               Power(t1,2)*(10*Power(t1,3) - 20*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,2)*(14*Power(t1,3) - 69*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) - 4*Power(t2,3))) + 
            s1*Power(t1,2)*(-7*Power(s2,4)*t2 + 
               t1*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s2,3)*(4*Power(t1,2) - 23*t1*t2 + 
                  47*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,3) - 26*Power(t1,2)*t2 + 
                  63*t1*Power(t2,2) - 51*Power(t2,3)) + 
               2*s2*t2*(-2*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,4)*
                (6*Power(t1,2) - 9*t1*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*t2*
                (-8*Power(t1,3) + 13*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + Power(t2,3)) - 
               Power(s2,3)*(4*Power(t1,3) + 48*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(28*Power(t1,4) - 76*Power(t1,3)*t2 + 
                  97*Power(t1,2)*Power(t2,2) - 10*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*t1*(8*Power(t1,4) - 51*Power(t1,3)*t2 + 
                  86*Power(t1,2)*Power(t2,2) - 38*t1*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(s,2)*(Power(s1,5)*
             (3*Power(s2,4) + 2*Power(s2,3)*(t1 - 3*t2) + 
               Power(s2,2)*(8*Power(t1,2) - 17*t1*t2 + Power(t2,2)) + 
               Power(t1,2)*(10*Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               s2*t1*(21*Power(t1,2) - 21*t1*t2 + 2*Power(t2,2))) + 
            Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (Power(s2,3) + Power(s2,2)*(4*t1 - 9*t2) - 
               t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
               s2*(Power(t1,2) - 10*t1*t2 + 12*Power(t2,2))) + 
            Power(s1,4)*(2*Power(s2,5) + Power(s2,4)*(3*t1 - 16*t2) + 
               Power(s2,3)*(20*Power(t1,2) - 33*t1*t2 + 
                  16*Power(t2,2)) + 
               s2*t1*(33*Power(t1,3) - 83*Power(t1,2)*t2 + 
                  50*t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(t1,2)*(10*Power(t1,3) - 20*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*(26*Power(t1,3) - 79*Power(t1,2)*t2 + 
                  37*t1*Power(t2,2) - 2*Power(t2,3))) - 
            s1*s2*Power(t1,2)*t2*
             (2*Power(s2,4) + 5*Power(s2,3)*(t1 - 6*t2) + 
               Power(s2,2)*(8*Power(t1,2) - 36*t1*t2 + 
                  62*Power(t2,2)) + 
               s2*(3*Power(t1,3) - 22*Power(t1,2)*t2 + 
                  41*t1*Power(t2,2) - 28*Power(t2,3)) + 
               2*t2*(-Power(t1,3) + 4*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*(2*Power(s2,5)*(t1 - 3*t2) + 
               2*Power(s2,4)*
                (7*Power(t1,2) - 11*t1*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*t2*
                (-12*Power(t1,3) + 15*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + Power(t2,3)) - 
               Power(s2,3)*(8*Power(t1,3) + 90*Power(t1,2)*t2 - 
                  41*t1*Power(t2,2) + 13*Power(t2,3)) + 
               Power(s2,2)*(44*Power(t1,4) - 100*Power(t1,3)*t2 + 
                  142*Power(t1,2)*Power(t2,2) - 27*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*t1*(12*Power(t1,4) - 63*Power(t1,3)*t2 + 
                  104*Power(t1,2)*Power(t2,2) - 43*t1*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s1,2)*(Power(t1,3)*Power(t2,2)*
                (3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,5)*(2*Power(t1,2) - 5*t1*t2 + 4*Power(t2,2)) - 
               Power(s2,4)*(4*Power(t1,3) + 33*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + 9*Power(t2,3)) - 
               s2*t1*t2*(12*Power(t1,4) - 41*Power(t1,3)*t2 + 
                  49*Power(t1,2)*Power(t2,2) - 16*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,3)*(10*Power(t1,4) - 28*Power(t1,3)*t2 + 
                  118*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,2)*t1*
                (6*Power(t1,4) - 56*Power(t1,3)*t2 + 
                  111*Power(t1,2)*Power(t2,2) - 98*t1*Power(t2,3) + 
                  7*Power(t2,4)))) - 
         s*(Power(s1,6)*(s2 + t1)*
             (3*Power(s2,3) + s2*t1*(5*t1 - 4*t2) + 
               Power(s2,2)*(t1 - 2*t2) + Power(t1,2)*(5*t1 - 2*t2)) + 
            Power(s2,3)*Power(t1,2)*Power(t2,3)*
             (-2*Power(s2,2) - 2*s2*t1 + 6*s2*t2 + 2*t1*t2 - 
               3*Power(t2,2)) + 
            Power(s1,5)*(4*Power(s2,5) + Power(s2,4)*(5*t1 - 14*t2) + 
               Power(t1,3)*(5*Power(t1,2) - 10*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,3)*(16*Power(t1,2) - 25*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*t1*
                (24*Power(t1,2) - 42*t1*t2 + 17*Power(t2,2)) + 
               s2*Power(t1,2)*
                (18*Power(t1,2) - 37*t1*t2 + 17*Power(t2,2))) + 
            s1*Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (7*Power(s2,3) + Power(t1,3) + Power(s2,2)*(5*t1 - 31*t2) - 
               6*Power(t1,2)*t2 + 8*t1*Power(t2,2) - 4*Power(t2,3) + 
               s2*(5*Power(t1,2) - 15*t1*t2 + 26*Power(t2,2))) + 
            Power(s1,4)*(Power(s2,6) + Power(s2,5)*(4*t1 - 13*t2) + 
               Power(s2,3)*t2*
                (-77*Power(t1,2) + 39*t1*t2 - 6*Power(t2,2)) + 
               Power(t1,3)*t2*
                (-8*Power(t1,2) + 7*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,4)*(13*Power(t1,2) - 22*t1*t2 + 
                  22*Power(t2,2)) + 
               Power(s2,2)*t1*
                (34*Power(t1,3) - 70*Power(t1,2)*t2 + 
                  89*t1*Power(t2,2) - 19*Power(t2,3)) + 
               s2*Power(t1,2)*
                (8*Power(t1,3) - 38*Power(t1,2)*t2 + 52*t1*Power(t2,2) - 
                  17*Power(t2,3))) + 
            Power(s1,2)*s2*t2*
             (Power(s2,5)*(-t1 + t2) + 
               Power(s2,4)*(-8*Power(t1,2) + 6*t1*t2 - 5*Power(t2,2)) + 
               Power(t1,2)*t2*
                (4*Power(t1,3) - 8*Power(t1,2)*t2 + 6*t1*Power(t2,2) - 
                  Power(t2,3)) + 
               Power(s2,3)*(Power(t1,3) + 58*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-14*Power(t1,3) + 31*Power(t1,2)*t2 - 
                  85*t1*Power(t2,2) + 5*Power(t2,3)) + 
               s2*t1*(-6*Power(t1,4) + 30*Power(t1,3)*t2 - 
                  46*Power(t1,2)*Power(t2,2) + 31*t1*Power(t2,3) - 
                  2*Power(t2,4))) + 
            Power(s1,3)*(Power(s2,6)*(t1 - 2*t2) + 
               Power(t1,4)*(3*t1 - 2*t2)*Power(t2,2) + 
               2*Power(s2,5)*(2*Power(t1,2) - 5*t1*t2 + 7*Power(t2,2)) + 
               s2*Power(t1,2)*t2*
                (-12*Power(t1,3) + 28*Power(t1,2)*t2 - 
                  31*t1*Power(t2,2) + 7*Power(t2,3)) - 
               Power(s2,4)*(7*Power(t1,3) + 46*Power(t1,2)*t2 - 
                  23*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,3)*(12*Power(t1,4) - 20*Power(t1,3)*t2 + 
                  121*Power(t1,2)*Power(t2,2) - 23*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,2)*t1*
                (6*Power(t1,4) - 54*Power(t1,3)*t2 + 
                  86*Power(t1,2)*Power(t2,2) - 81*t1*Power(t2,3) + 
                  10*Power(t2,4)))))*Log(s1 + t1 - t2))/
     (Power(s1,2)*Power(s2,2)*Power(t1,2)*(s1 + t1)*(s - s2 + t1)*t2*
       Power(s - s1 + t2,2)*(s - s1 - s2 + t2)) - 
    (8*(-(Power(s1,2)*Power(s2,2)*t1*(s1 + t1)*(s1 - t2)*
            Power(t1 - t2,2)*(s1 + t1 - t2)*(s2 - t1 + t2)*
            Power(s1*(-s2 + t1) + s2*t2,2)*
            (2*Power(s1,2) + s1*(-s2 + t1 - 2*t2) + s2*t2)) + 
         Power(s,6)*t1*Power(t1 - t2,2)*
          (-2*s2*t1*Power(t1 - t2,2)*Power(t2,3)*(s2 - t1 + t2) + 
            Power(s1,5)*(2*Power(t1 - t2,2)*t2 + 7*s2*t2*(-t1 + t2) + 
               Power(s2,2)*(t1 + 5*t2)) + 
            Power(s1,4)*(2*(2*t1 - 3*t2)*Power(t1 - t2,2)*t2 + 
               s2*t2*(-16*Power(t1,2) + 39*t1*t2 - 23*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,2) + 9*t1*t2 - 17*Power(t2,2))) + 
            s1*t1*(t1 - t2)*Power(t2,2)*
             (2*Power(t1 - t2,2)*t2 + Power(s2,2)*(4*t1 + 3*t2) + 
               s2*(-3*Power(t1,2) - 2*t1*t2 + 5*Power(t2,2))) - 
            Power(s1,2)*t2*(2*Power(t1 - t2,2)*t2*
                (2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,3) + 6*Power(t1,2)*t2 - 
                  25*t1*Power(t2,2) + 9*Power(t2,3)) + 
               s2*t2*(-14*Power(t1,3) + 48*Power(t1,2)*t2 - 
                  45*t1*Power(t2,2) + 11*Power(t2,3))) + 
            Power(s1,3)*(2*Power(t1 - t2,2)*t2*
                (Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,3) + 2*Power(t1,2)*t2 - 
                  29*t1*Power(t2,2) + 22*Power(t2,3)) + 
               s2*t2*(-9*Power(t1,3) + 51*Power(t1,2)*t2 - 
                  70*t1*Power(t2,2) + 28*Power(t2,3)))) + 
         Power(s,5)*(t1 - t2)*
          (2*s2*Power(t1,2)*Power(t1 - t2,2)*Power(t2,3)*
             (Power(s2,2)*(t1 - 3*t2) - s2*Power(t1 - t2,2) + 
               2*Power(t1 - t2,2)*t2) + 
            Power(s1,6)*t1*(-4*Power(t1 - t2,2)*(2*t1 - t2)*t2 + 
               Power(s2,3)*(9*t1 + 7*t2) + 
               Power(s2,2)*(-7*Power(t1,2) - 12*t1*t2 + 
                  25*Power(t2,2)) + 
               s2*(-4*Power(t1,3) + 31*Power(t1,2)*t2 - 
                  49*t1*Power(t2,2) + 22*Power(t2,3))) + 
            Power(s1,5)*t1*(Power(s2,3)*
                (23*Power(t1,2) - 36*t1*t2 + 3*Power(t2,2)) - 
               2*Power(t1 - t2,2)*t2*
                (7*Power(t1,2) - 16*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*(-19*Power(t1,3) + 38*Power(t1,2)*t2 + 
                  26*t1*Power(t2,2) - 51*Power(t2,3)) + 
               s2*(-6*Power(t1,4) + 60*Power(t1,3)*t2 - 
                  171*Power(t1,2)*Power(t2,2) + 185*t1*Power(t2,3) - 
                  68*Power(t2,4))) + 
            s1*Power(t1,2)*(t1 - t2)*t2*
             (2*Power(t1 - t2,3)*Power(t2,2)*(t1 + t2) + 
               s2*Power(t1 - t2,2)*t2*
                (6*Power(t1,2) - 45*t1*t2 + 19*Power(t2,2)) + 
               Power(s2,3)*(4*Power(t1,3) - 4*Power(t1,2)*t2 - 
                  17*t1*Power(t2,2) + 16*Power(t2,3)) + 
               Power(s2,2)*(-4*Power(t1,4) + 4*Power(t1,3)*t2 + 
                  65*Power(t1,2)*Power(t2,2) - 102*t1*Power(t2,3) + 
                  37*Power(t2,4))) + 
            Power(s1,4)*(-2*t1*Power(t1 - t2,2)*t2*
                (2*Power(t1,3) - 19*Power(t1,2)*t2 + 
                  24*t1*Power(t2,2) - 9*Power(t2,3)) + 
               Power(s2,3)*(19*Power(t1,4) - 93*Power(t1,3)*t2 + 
                  110*Power(t1,2)*Power(t2,2) - 56*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s2*t1*t2*(5*Power(t1,4) - 91*Power(t1,3)*t2 + 
                  238*Power(t1,2)*Power(t2,2) - 224*t1*Power(t2,3) + 
                  72*Power(t2,4)) + 
               Power(s2,2)*(-17*Power(t1,5) + 126*Power(t1,4)*t2 - 
                  205*Power(t1,3)*Power(t2,2) + 
                  92*Power(t1,2)*Power(t2,3) - 2*Power(t2,5))) + 
            Power(s1,3)*(2*t1*Power(t1 - t2,2)*t2*
                (Power(t1,4) + 3*Power(t1,3)*t2 - 
                  18*Power(t1,2)*Power(t2,2) + 17*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,3)*(Power(t1,5) - 50*Power(t1,4)*t2 + 
                  171*Power(t1,3)*Power(t2,2) - 
                  193*Power(t1,2)*Power(t2,3) + 77*t1*Power(t2,4) + 
                  4*Power(t2,5)) + 
               s2*t1*(2*Power(t1,6) - 32*Power(t1,5)*t2 + 
                  128*Power(t1,4)*Power(t2,2) - 
                  151*Power(t1,3)*Power(t2,3) + 
                  12*Power(t1,2)*Power(t2,4) + 66*t1*Power(t2,5) - 
                  25*Power(t2,6)) + 
               Power(s2,2)*(-Power(t1,6) + 76*Power(t1,5)*t2 - 
                  357*Power(t1,4)*Power(t2,2) + 
                  509*Power(t1,3)*Power(t2,3) - 
                  283*Power(t1,2)*Power(t2,4) + 58*t1*Power(t2,5) + 
                  4*Power(t2,6))) - 
            Power(s1,2)*(2*t1*Power(t1 - t2,4)*Power(t2,2)*
                (2*Power(t1,2) + 4*t1*t2 - Power(t2,2)) + 
               s2*t1*Power(t1 - t2,2)*t2*
                (8*Power(t1,4) - 79*Power(t1,3)*t2 + 
                  121*Power(t1,2)*Power(t2,2) - 35*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               2*Power(s2,3)*
                (2*Power(t1,6) - 4*Power(t1,5)*t2 - 
                  28*Power(t1,4)*Power(t2,2) + 
                  76*Power(t1,3)*Power(t2,3) - 
                  65*Power(t1,2)*Power(t2,4) + 15*t1*Power(t2,5) + 
                  Power(t2,6)) + 
               Power(s2,2)*(-4*Power(t1,7) + 8*Power(t1,6)*t2 + 
                  131*Power(t1,5)*Power(t2,2) - 
                  435*Power(t1,4)*Power(t2,3) + 
                  480*Power(t1,3)*Power(t2,4) - 
                  213*Power(t1,2)*Power(t2,5) + 31*t1*Power(t2,6) + 
                  2*Power(t2,7)))) + 
         Power(s,4)*(2*s2*Power(t1,2)*Power(t1 - t2,2)*Power(t2,4)*
             (Power(s2,3)*(2*t1 - 3*t2) - 
               s2*(2*t1 - 5*t2)*Power(t1 - t2,2) + 
               Power(t1 - t2,3)*t2 + 3*Power(s2,2)*t2*(-t1 + t2)) + 
            Power(s1,7)*t1*(4*Power(s2,4)*(t1 - t2) + 
               2*Power(t1 - t2,2)*t2*
                (6*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(-26*Power(t1,2) + 18*t1*t2 + 
                  12*Power(t2,2)) + 
               Power(s2,2)*(7*Power(t1,3) + 3*Power(t1,2)*t2 - 
                  46*t1*Power(t2,2) + 38*Power(t2,3)) + 
               s2*(16*Power(t1,4) - 71*Power(t1,3)*t2 + 
                  117*Power(t1,2)*Power(t2,2) - 86*t1*Power(t2,3) + 
                  24*Power(t2,4))) + 
            Power(s1,6)*t1*(Power(s2,4)*
                (-11*Power(t1,2) - 8*t1*t2 + 15*Power(t2,2)) + 
               2*Power(t1 - t2,2)*t2*
                (8*Power(t1,3) - 27*Power(t1,2)*t2 + 
                  22*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,3)*(-50*Power(t1,3) + 217*Power(t1,2)*t2 - 
                  172*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(33*Power(t1,4) - 209*Power(t1,3)*t2 + 
                  284*Power(t1,2)*Power(t2,2) - 25*t1*Power(t2,3) - 
                  85*Power(t2,4)) + 
               s2*(26*Power(t1,5) - 129*Power(t1,4)*t2 + 
                  306*Power(t1,3)*Power(t2,2) - 
                  407*Power(t1,2)*Power(t2,3) + 283*t1*Power(t2,4) - 
                  79*Power(t2,5))) + 
            s1*Power(t1,2)*(t1 - t2)*t2*
             (2*t1*Power(t1 - t2,4)*Power(t2,3) + 
               s2*Power(t1 - t2,3)*t2*
                (3*Power(t1,3) - 5*Power(t1,2)*t2 - 
                  39*t1*Power(t2,2) + 29*Power(t2,3)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (4*Power(t1,4) + 12*Power(t1,3)*t2 - 
                  86*Power(t1,2)*Power(t2,2) + 60*t1*Power(t2,3) - 
                  17*Power(t2,4)) + 
               Power(s2,4)*(-4*Power(t1,4) + 11*Power(t1,3)*t2 + 
                  23*Power(t1,2)*Power(t2,2) - 63*t1*Power(t2,3) + 
                  32*Power(t2,4)) + 
               2*Power(s2,3)*
                (4*Power(t1,5) - 5*Power(t1,4)*t2 - 
                  47*Power(t1,3)*Power(t2,2) + 
                  133*Power(t1,2)*Power(t2,3) - 124*t1*Power(t2,4) + 
                  39*Power(t2,5))) + 
            Power(s1,5)*(Power(s2,3)*t1*
                (-2*Power(t1,4) + 255*Power(t1,3)*t2 - 
                  575*Power(t1,2)*Power(t2,2) + 372*t1*Power(t2,3) - 
                  54*Power(t2,4)) - 
               2*t1*Power(t1 - t2,2)*t2*
                (2*Power(t1,4) + 17*Power(t1,3)*t2 - 
                  42*Power(t1,2)*Power(t2,2) + 30*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,4)*(-45*Power(t1,4) + 107*Power(t1,3)*t2 - 
                  76*Power(t1,2)*Power(t2,2) + 12*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,2)*(34*Power(t1,6) - 449*Power(t1,5)*t2 + 
                  1318*Power(t1,4)*Power(t2,2) - 
                  1431*Power(t1,3)*Power(t2,3) + 
                  518*Power(t1,2)*Power(t2,4) + 12*t1*Power(t2,5) - 
                  4*Power(t2,6)) + 
               s2*t1*(6*Power(t1,6) + 8*Power(t1,5)*t2 - 
                  58*Power(t1,4)*Power(t2,2) - 
                  13*Power(t1,3)*Power(t2,3) + 
                  221*Power(t1,2)*Power(t2,4) - 246*t1*Power(t2,5) + 
                  82*Power(t2,6))) - 
            Power(s1,4)*(2*t1*Power(t1 - t2,2)*t2*
                (4*Power(t1,5) - 13*Power(t1,4)*t2 - 
                  4*Power(t1,3)*Power(t2,2) + 
                  26*Power(t1,2)*Power(t2,3) - 18*t1*Power(t2,4) + 
                  4*Power(t2,5)) + 
               Power(s2,4)*(45*Power(t1,5) - 214*Power(t1,4)*t2 + 
                  359*Power(t1,3)*Power(t2,2) - 
                  296*Power(t1,2)*Power(t2,3) + 86*t1*Power(t2,4) + 
                  14*Power(t2,5)) + 
               Power(s2,3)*(-58*Power(t1,6) + 97*Power(t1,5)*t2 + 
                  207*Power(t1,4)*Power(t2,2) - 
                  366*Power(t1,3)*Power(t2,3) + 
                  148*Power(t1,2)*Power(t2,4) - 36*t1*Power(t2,5) + 
                  4*Power(t2,6)) + 
               Power(s2,2)*(15*Power(t1,7) + 178*Power(t1,6)*t2 - 
                  1316*Power(t1,5)*Power(t2,2) + 
                  2758*Power(t1,4)*Power(t2,3) - 
                  2427*Power(t1,3)*Power(t2,4) + 
                  898*Power(t1,2)*Power(t2,5) - 98*t1*Power(t2,6) - 
                  10*Power(t2,7)) + 
               s2*t1*(2*Power(t1,7) - 77*Power(t1,6)*t2 + 
                  503*Power(t1,5)*Power(t2,2) - 
                  1210*Power(t1,4)*Power(t2,3) + 
                  1342*Power(t1,3)*Power(t2,4) - 
                  684*Power(t1,2)*Power(t2,5) + 116*t1*Power(t2,6) + 
                  8*Power(t2,7))) + 
            Power(s1,3)*(2*t1*Power(t1 - t2,3)*Power(t2,2)*
                (9*Power(t1,4) - 14*Power(t1,3)*t2 + 
                  3*t1*Power(t2,3) - Power(t2,4)) + 
               s2*t1*Power(t1 - t2,2)*
                (2*Power(t1,6) + 11*Power(t1,5)*t2 - 
                  211*Power(t1,4)*Power(t2,2) + 
                  617*Power(t1,3)*Power(t2,3) - 
                  612*Power(t1,2)*Power(t2,4) + 225*t1*Power(t2,5) - 
                  34*Power(t2,6)) + 
               Power(s2,4)*(-11*Power(t1,6) + 107*Power(t1,5)*t2 - 
                  359*Power(t1,4)*Power(t2,2) + 
                  564*Power(t1,3)*Power(t2,3) - 
                  414*Power(t1,2)*Power(t2,4) + 95*t1*Power(t2,5) + 
                  16*Power(t2,6)) + 
               Power(s2,3)*(28*Power(t1,7) - 183*Power(t1,6)*t2 + 
                  417*Power(t1,5)*Power(t2,2) - 
                  591*Power(t1,4)*Power(t2,3) + 
                  613*Power(t1,3)*Power(t2,4) - 
                  319*Power(t1,2)*Power(t2,5) + 27*t1*Power(t2,6) + 
                  8*Power(t2,7)) + 
               Power(s2,2)*(-19*Power(t1,8) + 89*Power(t1,7)*t2 + 
                  119*Power(t1,6)*Power(t2,2) - 
                  1150*Power(t1,5)*Power(t2,3) + 
                  2084*Power(t1,4)*Power(t2,4) - 
                  1594*Power(t1,3)*Power(t2,5) + 
                  559*Power(t1,2)*Power(t2,6) - 80*t1*Power(t2,7) - 
                  8*Power(t2,8))) + 
            Power(s1,2)*(-6*Power(t1,3)*Power(t1 - t2,4)*(2*t1 - t2)*
                Power(t2,3) - 
               s2*t1*Power(t1 - t2,3)*t2*
                (4*Power(t1,5) - 2*Power(t1,4)*t2 - 
                  159*Power(t1,3)*Power(t2,2) + 
                  249*Power(t1,2)*Power(t2,3) - 110*t1*Power(t2,4) + 
                  15*Power(t2,5)) + 
               Power(s2,4)*(4*Power(t1,7) - 8*Power(t1,6)*t2 - 
                  76*Power(t1,5)*Power(t2,2) + 
                  296*Power(t1,4)*Power(t2,3) - 
                  414*Power(t1,3)*Power(t2,4) + 
                  234*Power(t1,2)*Power(t2,5) - 32*t1*Power(t2,6) - 
                  6*Power(t2,7)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (4*Power(t1,7) + 18*Power(t1,6)*t2 - 
                  141*Power(t1,5)*Power(t2,2) + 
                  62*Power(t1,4)*Power(t2,3) + 
                  105*Power(t1,3)*Power(t2,4) - 
                  59*Power(t1,2)*Power(t2,5) + 21*t1*Power(t2,6) + 
                  2*Power(t2,7)) + 
               Power(s2,3)*(-8*Power(t1,8) + 2*Power(t1,7)*t2 + 
                  201*Power(t1,6)*Power(t2,2) - 
                  688*Power(t1,5)*Power(t2,3) + 
                  1097*Power(t1,4)*Power(t2,4) - 
                  899*Power(t1,3)*Power(t2,5) + 
                  321*Power(t1,2)*Power(t2,6) - 22*t1*Power(t2,7) - 
                  4*Power(t2,8)))) + 
         s*(2*Power(s2,4)*Power(t1,2)*Power(t1 - t2,2)*Power(t2,7)*
             (s2 - t1 + t2) + 
            Power(s1,10)*s2*t1*
             (Power(s2,2)*Power(t2,2) + 
               t1*(-4*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + Power(t2,3)) + 
               s2*(4*Power(t1,3) - 4*Power(t1,2)*t2 - t1*Power(t2,2) + 
                  Power(t2,3))) + 
            Power(s1,9)*t1*(-4*Power(s2,5)*(t1 - t2) + 
               2*Power(t1,3)*Power(t1 - t2,2)*t2 + 
               Power(s2,3)*t2*
                (-3*Power(t1,2) + 13*t1*t2 - 7*Power(t2,2)) + 
               Power(s2,4)*(16*Power(t1,2) - 22*t1*t2 + 
                  3*Power(t2,2)) + 
               Power(s2,2)*(-4*Power(t1,4) + 22*Power(t1,3)*t2 - 
                  44*Power(t1,2)*Power(t2,2) + 35*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               s2*t1*(-8*Power(t1,4) + 25*Power(t1,3)*t2 - 
                  26*Power(t1,2)*Power(t2,2) + 11*t1*Power(t2,3) - 
                  5*Power(t2,4))) + 
            s1*Power(s2,2)*Power(t1,2)*Power(t2,4)*
             (Power(s2,4)*(3*Power(t1,3) - 11*Power(t1,2)*t2 + 
                  16*t1*Power(t2,2) - 7*Power(t2,3)) - 
               2*t1*Power(t1 - t2,3)*
                (2*Power(t1,3) - 4*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  Power(t2,3)) + 
               Power(s2,3)*(-11*Power(t1,4) + 41*Power(t1,3)*t2 - 
                  76*Power(t1,2)*Power(t2,2) + 68*t1*Power(t2,3) - 
                  23*Power(t2,4)) + 
               s2*Power(t1 - t2,2)*
                (2*Power(t1,4) - 6*Power(t1,3)*t2 - 5*t1*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s2,2)*(10*Power(t1,5) - 39*Power(t1,4)*t2 + 
                  82*Power(t1,3)*Power(t2,2) - 
                  94*Power(t1,2)*Power(t2,3) + 50*t1*Power(t2,4) - 
                  9*Power(t2,5))) + 
            Power(s1,8)*(4*Power(t1,4)*(t1 - 2*t2)*Power(t1 - t2,2)*
                t2 + Power(s2,5)*
                (3*Power(t1,3) + 20*Power(t1,2)*t2 - 
                  22*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,4)*(23*Power(t1,4) - 151*Power(t1,3)*t2 + 
                  155*Power(t1,2)*Power(t2,2) - 23*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,3)*t1*
                (3*Power(t1,4) + 59*Power(t1,3)*t2 - 
                  84*Power(t1,2)*Power(t2,2) - 10*t1*Power(t2,3) + 
                  16*Power(t2,4)) + 
               s2*Power(t1,2)*
                (-6*Power(t1,5) + 29*Power(t1,4)*t2 - 
                  24*Power(t1,3)*Power(t2,2) - 
                  15*Power(t1,2)*Power(t2,3) + 8*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               Power(s2,2)*t1*
                (-23*Power(t1,5) + 103*Power(t1,4)*t2 - 
                  247*Power(t1,3)*Power(t2,2) + 
                  297*Power(t1,2)*Power(t2,3) - 155*t1*Power(t2,4) + 
                  15*Power(t2,5))) - 
            Power(s1,7)*(-2*Power(t1,4)*(t1 - 6*t2)*Power(t1 - t2,3)*
                t2 + Power(s2,6)*
                (2*Power(t1,3) - 4*Power(t1,2)*t2 + 5*t1*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(s2,5)*(-35*Power(t1,4) + 73*Power(t1,3)*t2 + 
                  5*Power(t1,2)*Power(t2,2) - 40*t1*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s2,4)*(33*Power(t1,5) + 88*Power(t1,4)*t2 - 
                  475*Power(t1,3)*Power(t2,2) + 
                  396*Power(t1,2)*Power(t2,3) - 58*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               Power(s2,3)*t1*
                (-29*Power(t1,5) - 62*Power(t1,4)*t2 + 
                  467*Power(t1,3)*Power(t2,2) - 
                  526*Power(t1,2)*Power(t2,3) + 117*t1*Power(t2,4) + 
                  19*Power(t2,5)) + 
               s2*Power(t1,2)*
                (4*Power(t1,6) - 31*Power(t1,5)*t2 + 
                  24*Power(t1,4)*Power(t2,2) + 
                  111*Power(t1,3)*Power(t2,3) - 
                  188*Power(t1,2)*Power(t2,4) + 68*t1*Power(t2,5) + 
                  10*Power(t2,6)) + 
               Power(s2,2)*t1*
                (25*Power(t1,6) - 126*Power(t1,5)*t2 + 
                  352*Power(t1,4)*Power(t2,2) - 
                  642*Power(t1,3)*Power(t2,3) + 
                  648*Power(t1,2)*Power(t2,4) - 285*t1*Power(t2,5) + 
                  20*Power(t2,6))) - 
            Power(s1,2)*s2*Power(t2,3)*
             (2*Power(t1,5)*(t1 - 3*t2)*Power(t1 - t2,3)*t2 - 
               s2*Power(t1,3)*Power(t1 - t2,3)*
                (10*Power(t1,3) - 25*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) - 6*Power(t2,3)) + 
               Power(s2,5)*(7*Power(t1,5) - 35*Power(t1,4)*t2 + 
                  64*Power(t1,3)*Power(t2,2) - 
                  37*Power(t1,2)*Power(t2,3) + t1*Power(t2,4) + 
                  2*Power(t2,5)) + 
               Power(s2,3)*t1*
                (20*Power(t1,6) - 107*Power(t1,5)*t2 + 
                  307*Power(t1,4)*Power(t2,2) - 
                  369*Power(t1,3)*Power(t2,3) + 
                  164*Power(t1,2)*Power(t2,4) - 8*t1*Power(t2,5) - 
                  3*Power(t2,6)) + 
               Power(s2,4)*(-25*Power(t1,6) + 114*Power(t1,5)*t2 - 
                  254*Power(t1,4)*Power(t2,2) + 
                  251*Power(t1,3)*Power(t2,3) - 
                  94*Power(t1,2)*Power(t2,4) + t1*Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s2,2)*t1*
                (8*Power(t1,7) - 28*Power(t1,6)*t2 - 
                  28*Power(t1,5)*Power(t2,2) + 
                  101*Power(t1,4)*Power(t2,3) - 
                  12*Power(t1,3)*Power(t2,4) - 
                  90*Power(t1,2)*Power(t2,5) + 54*t1*Power(t2,6) - 
                  5*Power(t2,7))) + 
            Power(s1,3)*s2*Power(t2,2)*
             (Power(t1,3)*Power(t1 - t2,2)*t2*
                (5*Power(t1,4) - 24*Power(t1,3)*t2 + 
                  10*Power(t1,2)*Power(t2,2) + 13*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,5)*(3*Power(t1,5) - 35*Power(t1,4)*t2 + 
                  91*Power(t1,3)*Power(t2,2) - 
                  67*Power(t1,2)*Power(t2,3) - t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               Power(s2,4)*(4*Power(t1,6) + 34*Power(t1,5)*t2 - 
                  229*Power(t1,4)*Power(t2,2) + 
                  337*Power(t1,3)*Power(t2,3) - 
                  151*Power(t1,2)*Power(t2,4) - 4*t1*Power(t2,5) + 
                  8*Power(t2,6)) + 
               Power(s2,2)*t1*
                (41*Power(t1,7) - 122*Power(t1,6)*t2 - 
                  131*Power(t1,5)*Power(t2,2) + 
                  622*Power(t1,4)*Power(t2,3) - 
                  506*Power(t1,3)*Power(t2,4) + 
                  2*Power(t1,2)*Power(t2,5) + 119*t1*Power(t2,6) - 
                  25*Power(t2,7)) + 
               Power(s2,3)*(-30*Power(t1,7) + 36*Power(t1,6)*t2 + 
                  323*Power(t1,5)*Power(t2,2) - 
                  662*Power(t1,4)*Power(t2,3) + 
                  336*Power(t1,3)*Power(t2,4) + 
                  17*Power(t1,2)*Power(t2,5) - 16*t1*Power(t2,6) - 
                  2*Power(t2,7)) - 
               s2*Power(t1,2)*
                (18*Power(t1,7) - 87*Power(t1,6)*t2 + 
                  101*Power(t1,5)*Power(t2,2) + 
                  46*Power(t1,4)*Power(t2,3) - 
                  151*Power(t1,3)*Power(t2,4) + 
                  84*Power(t1,2)*Power(t2,5) - 18*t1*Power(t2,6) + 
                  7*Power(t2,7))) + 
            Power(s1,6)*(-2*Power(t1,4)*Power(t1 - t2,2)*Power(t2,2)*
                (3*Power(t1,2) - 9*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,6)*(-4*Power(t1,4) + 11*Power(t1,3)*t2 - 
                  14*Power(t1,2)*Power(t2,2) + 19*t1*Power(t2,3) - 
                  10*Power(t2,4)) + 
               Power(s2,5)*(49*Power(t1,5) - 215*Power(t1,4)*t2 + 
                  262*Power(t1,3)*Power(t2,2) - 
                  78*Power(t1,2)*Power(t2,3) - 21*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               Power(s2,4)*(-83*Power(t1,6) + 248*Power(t1,5)*t2 + 
                  139*Power(t1,4)*Power(t2,2) - 
                  750*Power(t1,3)*Power(t2,3) + 
                  471*Power(t1,2)*Power(t2,4) - 59*t1*Power(t2,5) + 
                  20*Power(t2,6)) + 
               Power(s2,3)*t1*
                (61*Power(t1,6) - 136*Power(t1,5)*t2 - 
                  429*Power(t1,4)*Power(t2,2) + 
                  1398*Power(t1,3)*Power(t2,3) - 
                  1193*Power(t1,2)*Power(t2,4) + 282*t1*Power(t2,5) + 
                  26*Power(t2,6)) + 
               s2*Power(t1,2)*
                (-2*Power(t1,7) + 27*Power(t1,6)*t2 - 
                  73*Power(t1,5)*Power(t2,2) - 
                  48*Power(t1,4)*Power(t2,3) + 
                  339*Power(t1,3)*Power(t2,4) - 
                  359*Power(t1,2)*Power(t2,5) + 108*t1*Power(t2,6) + 
                  5*Power(t2,7)) + 
               Power(s2,2)*t1*
                (-21*Power(t1,7) + 93*Power(t1,6)*t2 - 
                  177*Power(t1,5)*Power(t2,2) + 
                  371*Power(t1,4)*Power(t2,3) - 
                  669*Power(t1,3)*Power(t2,4) + 
                  647*Power(t1,2)*Power(t2,5) - 253*t1*Power(t2,6) + 
                  15*Power(t2,7))) + 
            Power(s1,4)*t2*(-2*Power(t1,5)*Power(t1 - t2,3)*
                Power(t2,3) + 
               Power(s2,6)*(3*Power(t1,5) + 5*Power(t1,4)*t2 - 
                  47*Power(t1,3)*Power(t2,2) + 
                  45*Power(t1,2)*Power(t2,3) + 14*t1*Power(t2,4) - 
                  20*Power(t2,5)) + 
               Power(s2,5)*(-39*Power(t1,6) + 159*Power(t1,5)*t2 - 
                  155*Power(t1,4)*Power(t2,2) - 
                  48*Power(t1,3)*Power(t2,3) + 
                  58*Power(t1,2)*Power(t2,4) + 20*t1*Power(t2,5) - 
                  10*Power(t2,6)) + 
               s2*Power(t1,3)*t2*
                (-9*Power(t1,6) + 57*Power(t1,5)*t2 - 
                  83*Power(t1,4)*Power(t2,2) - 
                  45*Power(t1,3)*Power(t2,3) + 
                  182*Power(t1,2)*Power(t2,4) - 126*t1*Power(t2,5) + 
                  24*Power(t2,6)) + 
               Power(s2,4)*(83*Power(t1,7) - 323*Power(t1,6)*t2 + 
                  136*Power(t1,5)*Power(t2,2) + 
                  598*Power(t1,4)*Power(t2,3) - 
                  539*Power(t1,3)*Power(t2,4) + 
                  24*Power(t1,2)*Power(t2,5) + 23*t1*Power(t2,6) + 
                  10*Power(t2,7)) + 
               Power(s2,3)*t1*
                (-69*Power(t1,7) + 259*Power(t1,6)*t2 + 
                  53*Power(t1,5)*Power(t2,2) - 
                  1193*Power(t1,4)*Power(t2,3) + 
                  1545*Power(t1,3)*Power(t2,4) - 
                  603*Power(t1,2)*Power(t2,5) - 34*t1*Power(t2,6) + 
                  48*Power(t2,7)) + 
               Power(s2,2)*t1*
                (22*Power(t1,8) - 99*Power(t1,7)*t2 + 
                  83*Power(t1,6)*Power(t2,2) + 
                  164*Power(t1,5)*Power(t2,3) - 
                  312*Power(t1,4)*Power(t2,4) + 
                  144*Power(t1,3)*Power(t2,5) + t1*Power(t2,7) + 
                  Power(t2,8))) - 
            Power(s1,5)*(-2*Power(t1,4)*Power(t1 - t2,2)*Power(t2,3)*
                (3*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
               Power(s2,6)*(2*Power(t1,5) - 10*Power(t1,4)*t2 + 
                  5*Power(t1,3)*Power(t2,2) - 
                  2*Power(t1,2)*Power(t2,3) + 26*t1*Power(t2,4) - 
                  20*Power(t2,5)) + 
               Power(s2,5)*t1*
                (-21*Power(t1,5) + 169*Power(t1,4)*t2 - 
                  384*Power(t1,3)*Power(t2,2) + 
                  294*Power(t1,2)*Power(t2,3) - 87*t1*Power(t2,4) + 
                  16*Power(t2,5)) + 
               s2*Power(t1,2)*t2*
                (-8*Power(t1,7) + 58*Power(t1,6)*t2 - 
                  92*Power(t1,5)*Power(t2,2) - 
                  107*Power(t1,4)*Power(t2,3) + 
                  383*Power(t1,3)*Power(t2,4) - 
                  313*Power(t1,2)*Power(t2,5) + 77*t1*Power(t2,6) + 
                  Power(t2,7)) + 
               Power(s2,4)*(43*Power(t1,7) - 302*Power(t1,6)*t2 + 
                  447*Power(t1,5)*Power(t2,2) + 
                  291*Power(t1,4)*Power(t2,3) - 
                  733*Power(t1,3)*Power(t2,4) + 
                  250*Power(t1,2)*Power(t2,5) - 11*t1*Power(t2,6) + 
                  20*Power(t2,7)) + 
               Power(s2,3)*t1*
                (-35*Power(t1,7) + 217*Power(t1,6)*t2 - 
                  142*Power(t1,5)*Power(t2,2) - 
                  1048*Power(t1,4)*Power(t2,3) + 
                  2058*Power(t1,3)*Power(t2,4) - 
                  1284*Power(t1,2)*Power(t2,5) + 206*t1*Power(t2,6) + 
                  45*Power(t2,7)) + 
               Power(s2,2)*t1*
                (11*Power(t1,8) - 70*Power(t1,7)*t2 + 
                  110*Power(t1,6)*Power(t2,2) + 
                  12*Power(t1,5)*Power(t2,3) - 
                  42*Power(t1,4)*Power(t2,4) - 
                  191*Power(t1,3)*Power(t2,5) + 
                  270*Power(t1,2)*Power(t2,6) - 95*t1*Power(t2,7) + 
                  6*Power(t2,8)))) + 
         Power(s,2)*(-2*Power(s2,3)*Power(t1,2)*Power(t1 - t2,2)*
             Power(t2,6)*(-2*Power(s2,2) + 2*Power(t1,2) + s2*t2 - 
               5*t1*t2 + 3*Power(t2,2)) + 
            Power(s1,9)*t1*(4*Power(s2,4)*(t1 - t2) + 
               2*Power(t1,2)*Power(t1 - t2,2)*t2 - 
               2*Power(s2,3)*
                (4*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-12*Power(t1,3) + 10*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + Power(t2,3)) + 
               s2*(16*Power(t1,4) - 38*Power(t1,3)*t2 + 
                  28*Power(t1,2)*Power(t2,2) - 9*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s1,8)*t1*(8*Power(s2,5)*(t1 - t2) - 
               4*Power(t1,2)*Power(t1 - t2,2)*t2*(t1 + t2) + 
               Power(s2,4)*(-41*Power(t1,2) + 24*t1*t2 + 
                  20*Power(t2,2)) + 
               Power(s2,3)*(-6*Power(t1,3) + 111*Power(t1,2)*t2 - 
                  159*t1*Power(t2,2) + 34*Power(t2,3)) + 
               Power(s2,2)*(9*Power(t1,4) - 118*Power(t1,3)*t2 + 
                  255*Power(t1,2)*Power(t2,2) - 160*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(30*Power(t1,5) - 95*Power(t1,4)*t2 + 
                  109*Power(t1,3)*Power(t2,2) - 
                  60*Power(t1,2)*Power(t2,3) + 25*t1*Power(t2,4) - 
                  5*Power(t2,5))) + 
            s1*s2*Power(t1,2)*Power(t2,3)*
             (2*t1*Power(t1 - t2,4)*t2*(Power(t1,2) + Power(t2,2)) + 
               Power(s2,5)*(5*Power(t1,3) - 17*Power(t1,2)*t2 + 
                  22*t1*Power(t2,2) - 9*Power(t2,3)) - 
               Power(s2,2)*Power(t1 - t2,3)*
                (6*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  56*t1*Power(t2,2) + 43*Power(t2,3)) - 
               s2*Power(t1 - t2,3)*
                (6*Power(t1,4) - 9*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) + 8*t1*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s2,4)*(-26*Power(t1,4) + 72*Power(t1,3)*t2 - 
                  76*Power(t1,2)*Power(t2,2) + 33*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,3)*(33*Power(t1,5) - 83*Power(t1,4)*t2 + 
                  23*Power(t1,3)*Power(t2,2) + 
                  135*Power(t1,2)*Power(t2,3) - 169*t1*Power(t2,4) + 
                  61*Power(t2,5))) + 
            Power(s1,7)*(-2*Power(t1,3)*Power(t1 - t2,2)*t2*
                (7*Power(t1,2) - 12*t1*t2 + 2*Power(t2,2)) - 
               2*Power(s2,5)*
                (Power(t1,3) + 18*Power(t1,2)*t2 - 18*t1*Power(t2,2) + 
                  2*Power(t2,3)) - 
               Power(s2,4)*t1*
                (101*Power(t1,3) - 400*Power(t1,2)*t2 + 
                  272*t1*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,2)*t1*
                (43*Power(t1,5) - 327*Power(t1,4)*t2 + 
                  1052*Power(t1,3)*Power(t2,2) - 
                  1374*Power(t1,2)*Power(t2,3) + 633*t1*Power(t2,4) - 
                  8*Power(t2,5)) + 
               Power(s2,3)*(42*Power(t1,5) - 128*Power(t1,4)*t2 - 
                  163*Power(t1,3)*Power(t2,2) + 
                  359*Power(t1,2)*Power(t2,3) - 82*t1*Power(t2,4) + 
                  4*Power(t2,5)) + 
               s2*t1*(18*Power(t1,6) - 67*Power(t1,5)*t2 + 
                  9*Power(t1,4)*Power(t2,2) + 
                  143*Power(t1,3)*Power(t2,3) - 
                  114*Power(t1,2)*Power(t2,4) + t1*Power(t2,5) + 
                  10*Power(t2,6))) + 
            Power(s1,6)*(-2*Power(t1,3)*Power(t1 - t2,2)*t2*
                (4*Power(t1,3) - 25*Power(t1,2)*t2 + 
                  26*t1*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s2,6)*(Power(t1,3) - 2*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,5)*(-46*Power(t1,4) + 97*Power(t1,3)*t2 - 
                  21*Power(t1,2)*Power(t2,2) - 38*t1*Power(t2,3) + 
                  20*Power(t2,4)) + 
               Power(s2,4)*(-27*Power(t1,5) + 526*Power(t1,4)*t2 - 
                  1130*Power(t1,3)*Power(t2,2) + 
                  600*Power(t1,2)*Power(t2,3) + 2*t1*Power(t2,4) + 
                  6*Power(t2,5)) + 
               Power(s2,3)*(44*Power(t1,6) - 527*Power(t1,5)*t2 + 
                  1058*Power(t1,4)*Power(t2,2) - 
                  470*Power(t1,3)*Power(t2,3) - 
                  177*Power(t1,2)*Power(t2,4) + 92*t1*Power(t2,5) - 
                  16*Power(t2,6)) + 
               Power(s2,2)*t1*
                (18*Power(t1,6) - 175*Power(t1,5)*t2 + 
                  1044*Power(t1,4)*Power(t2,2) - 
                  2528*Power(t1,3)*Power(t2,3) + 
                  2601*Power(t1,2)*Power(t2,4) - 955*t1*Power(t2,5) - 
                  2*Power(t2,6)) + 
               s2*t1*(10*Power(t1,7) - 53*Power(t1,6)*t2 - 
                  83*Power(t1,5)*Power(t2,2) + 
                  609*Power(t1,4)*Power(t2,3) - 
                  914*Power(t1,3)*Power(t2,4) + 
                  519*Power(t1,2)*Power(t2,5) - 82*t1*Power(t2,6) - 
                  10*Power(t2,7))) + 
            Power(s1,2)*s2*Power(t2,2)*
             (-(Power(t1,3)*Power(t1 - t2,3)*t2*
                  (8*Power(t1,3) - 29*Power(t1,2)*t2 - 
                    9*t1*Power(t2,2) + 14*Power(t2,3))) - 
               Power(s2,5)*(9*Power(t1,5) - 43*Power(t1,4)*t2 + 
                  73*Power(t1,3)*Power(t2,2) - 
                  43*Power(t1,2)*Power(t2,3) + 3*t1*Power(t2,4) + 
                  2*Power(t2,5)) + 
               s2*t1*Power(t1 - t2,2)*
                (22*Power(t1,6) - 54*Power(t1,5)*t2 - 
                  39*Power(t1,4)*Power(t2,2) + 
                  60*Power(t1,3)*Power(t2,3) - 
                  65*Power(t1,2)*Power(t2,4) + 82*t1*Power(t2,5) - 
                  10*Power(t2,6)) + 
               Power(s2,4)*(32*Power(t1,6) - 109*Power(t1,5)*t2 + 
                  128*Power(t1,4)*Power(t2,2) - 
                  8*Power(t1,3)*Power(t2,3) - 
                  44*Power(t1,2)*Power(t2,4) + 5*t1*Power(t2,5) + 
                  4*Power(t2,6)) + 
               Power(s2,2)*t1*
                (-28*Power(t1,7) + 114*Power(t1,6)*t2 - 
                  172*Power(t1,5)*Power(t2,2) + 
                  381*Power(t1,4)*Power(t2,3) - 
                  665*Power(t1,3)*Power(t2,4) + 
                  513*Power(t1,2)*Power(t2,5) - 148*t1*Power(t2,6) + 
                  5*Power(t2,7)) + 
               Power(s2,3)*(-17*Power(t1,7) + 56*Power(t1,6)*t2 - 
                  Power(t1,5)*Power(t2,2) - 
                  422*Power(t1,4)*Power(t2,3) + 
                  679*Power(t1,3)*Power(t2,4) - 
                  326*Power(t1,2)*Power(t2,5) + 15*t1*Power(t2,6) + 
                  6*Power(t2,7))) + 
            Power(s1,4)*(-2*Power(t1,3)*Power(t1 - t2,2)*Power(t2,3)*
                (10*Power(t1,3) - 21*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,6)*(Power(t1,5) + 5*Power(t1,4)*t2 - 
                  32*Power(t1,3)*Power(t2,2) + 
                  35*Power(t1,2)*Power(t2,3) + 3*t1*Power(t2,4) - 
                  12*Power(t2,5)) + 
               Power(s2,5)*(-26*Power(t1,6) + 165*Power(t1,5)*t2 - 
                  392*Power(t1,4)*Power(t2,2) + 
                  487*Power(t1,3)*Power(t2,3) - 
                  363*Power(t1,2)*Power(t2,4) + 69*t1*Power(t2,5) + 
                  40*Power(t2,6)) + 
               Power(s2,4)*(68*Power(t1,7) - 324*Power(t1,6)*t2 + 
                  218*Power(t1,5)*Power(t2,2) + 
                  374*Power(t1,4)*Power(t2,3) + 
                  25*Power(t1,3)*Power(t2,4) - 
                  462*Power(t1,2)*Power(t2,5) + 95*t1*Power(t2,6) + 
                  36*Power(t2,7)) + 
               Power(s2,2)*t1*
                (23*Power(t1,8) - 78*Power(t1,7)*t2 - 
                  107*Power(t1,6)*Power(t2,2) + 
                  471*Power(t1,5)*Power(t2,3) + 
                  31*Power(t1,4)*Power(t2,4) - 
                  948*Power(t1,3)*Power(t2,5) + 
                  632*Power(t1,2)*Power(t2,6) + 47*t1*Power(t2,7) - 
                  61*Power(t2,8)) + 
               Power(s2,3)*(-66*Power(t1,8) + 248*Power(t1,7)*t2 + 
                  292*Power(t1,6)*Power(t2,2) - 
                  1880*Power(t1,5)*Power(t2,3) + 
                  2113*Power(t1,4)*Power(t2,4) - 
                  790*Power(t1,3)*Power(t2,5) + 
                  66*Power(t1,2)*Power(t2,6) + 49*t1*Power(t2,7) - 
                  16*Power(t2,8)) - 
               s2*t1*t2*(20*Power(t1,8) - 140*Power(t1,7)*t2 + 
                  220*Power(t1,6)*Power(t2,2) + 
                  260*Power(t1,5)*Power(t2,3) - 
                  1061*Power(t1,4)*Power(t2,4) + 
                  1120*Power(t1,3)*Power(t2,5) - 
                  499*Power(t1,2)*Power(t2,6) + 79*t1*Power(t2,7) + 
                  Power(t2,8))) + 
            Power(s1,5)*(2*Power(t1,3)*Power(t1 - t2,2)*Power(t2,2)*
                (11*Power(t1,3) - 34*Power(t1,2)*t2 + 
                  26*t1*Power(t2,2) - 7*Power(t2,3)) + 
               Power(s2,6)*(2*Power(t1,4) - 
                  2*Power(t1,2)*Power(t2,2) - 9*t1*Power(t2,3) + 
                  8*Power(t2,4)) - 
               Power(s2,5)*(62*Power(t1,5) - 262*Power(t1,4)*t2 + 
                  371*Power(t1,3)*Power(t2,2) - 
                  244*Power(t1,2)*Power(t2,3) + 27*t1*Power(t2,4) + 
                  40*Power(t2,5)) + 
               Power(s2,4)*(97*Power(t1,6) - 56*Power(t1,5)*t2 - 
                  841*Power(t1,4)*Power(t2,2) + 
                  1160*Power(t1,3)*Power(t2,3) - 
                  302*Power(t1,2)*Power(t2,4) - 41*t1*Power(t2,5) - 
                  24*Power(t2,6)) - 
               Power(s2,3)*(62*Power(t1,7) + 190*Power(t1,6)*t2 - 
                  1599*Power(t1,5)*Power(t2,2) + 
                  2444*Power(t1,4)*Power(t2,3) - 
                  1278*Power(t1,3)*Power(t2,4) + 
                  170*Power(t1,2)*Power(t2,5) + 69*t1*Power(t2,6) - 
                  24*Power(t2,7)) + 
               Power(s2,2)*t1*
                (19*Power(t1,7) - Power(t1,6)*t2 + 
                  20*Power(t1,5)*Power(t2,2) - 
                  1014*Power(t1,4)*Power(t2,3) + 
                  2552*Power(t1,3)*Power(t2,4) - 
                  2207*Power(t1,2)*Power(t2,5) + 571*t1*Power(t2,6) + 
                  39*Power(t2,7)) + 
               s2*t1*(6*Power(t1,8) - 63*Power(t1,7)*t2 + 
                  118*Power(t1,6)*Power(t2,2) + 
                  319*Power(t1,5)*Power(t2,3) - 
                  1236*Power(t1,4)*Power(t2,4) + 
                  1453*Power(t1,3)*Power(t2,5) - 
                  725*Power(t1,2)*Power(t2,6) + 125*t1*Power(t2,7) + 
                  5*Power(t2,8))) + 
            Power(s1,3)*t2*(2*Power(t1,4)*(3*t1 - 2*t2)*
                Power(t1 - t2,3)*Power(t2,3) + 
               s2*Power(t1,2)*Power(t1 - t2,3)*t2*
                (19*Power(t1,4) - 73*Power(t1,3)*t2 - 
                  51*Power(t1,2)*Power(t2,2) + 106*t1*Power(t2,3) - 
                  19*Power(t2,4)) + 
               Power(s2,6)*(3*Power(t1,5) - 33*Power(t1,4)*t2 + 
                  82*Power(t1,3)*Power(t2,2) - 
                  65*Power(t1,2)*Power(t2,3) + 5*t1*Power(t2,4) + 
                  8*Power(t2,5)) + 
               Power(s2,5)*(20*Power(t1,6) - 66*Power(t1,5)*t2 + 
                  120*Power(t1,4)*Power(t2,2) - 
                  228*Power(t1,3)*Power(t2,3) + 
                  213*Power(t1,2)*Power(t2,4) - 37*t1*Power(t2,5) - 
                  20*Power(t2,6)) - 
               Power(s2,4)*(82*Power(t1,7) - 252*Power(t1,6)*t2 + 
                  159*Power(t1,5)*Power(t2,2) - 
                  333*Power(t1,4)*Power(t2,3) + 
                  928*Power(t1,3)*Power(t2,4) - 
                  675*Power(t1,2)*Power(t2,5) + 67*t1*Power(t2,6) + 
                  24*Power(t2,7)) + 
               Power(s2,3)*(96*Power(t1,8) - 297*Power(t1,7)*t2 - 
                  55*Power(t1,6)*Power(t2,2) + 
                  619*Power(t1,5)*Power(t2,3) - 
                  212*Power(t1,4)*Power(t2,4) - 
                  308*Power(t1,3)*Power(t2,5) + 
                  178*Power(t1,2)*Power(t2,6) - 25*t1*Power(t2,7) + 
                  4*Power(t2,8)) + 
               Power(s2,2)*t1*
                (-37*Power(t1,8) + 129*Power(t1,7)*t2 + 
                  40*Power(t1,6)*Power(t2,2) - 
                  468*Power(t1,5)*Power(t2,3) + 
                  458*Power(t1,4)*Power(t2,4) - 
                  207*Power(t1,3)*Power(t2,5) + 
                  272*Power(t1,2)*Power(t2,6) - 227*t1*Power(t2,7) + 
                  40*Power(t2,8)))) + 
         Power(s,3)*(2*Power(s2,2)*Power(t1,2)*
             (Power(s2,3) + Power(s2,2)*(3*t1 - 5*t2) - 
               3*s2*Power(t1 - t2,2) - (t1 - 3*t2)*Power(t1 - t2,2))*
             Power(t1 - t2,2)*Power(t2,5) - 
            Power(s1,8)*t1*(8*Power(s2,4)*(t1 - t2) + 
               4*t1*Power(t1 - t2,2)*(2*t1 - t2)*t2 + 
               Power(s2,3)*(-25*Power(t1,2) + 30*t1*t2 + 
                  2*Power(t2,2)) + 
               Power(s2,2)*(-7*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  16*t1*Power(t2,2) + 20*Power(t2,3)) + 
               s2*(24*Power(t1,4) - 73*Power(t1,3)*t2 + 
                  82*Power(t1,2)*Power(t2,2) - 44*t1*Power(t2,3) + 
                  10*Power(t2,4))) + 
            s1*s2*Power(t1,2)*Power(t2,2)*
             (Power(t1 - t2,4)*t2*
                (3*Power(t1,3) - 5*Power(t1,2)*t2 - t1*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(s2,3)*Power(t1 - t2,2)*
                (7*Power(t1,3) + 53*Power(t1,2)*t2 - 
                  100*t1*Power(t2,2) + 58*Power(t2,3)) - 
               s2*Power(t1 - t2,3)*
                (8*Power(t1,4) - 9*Power(t1,3)*t2 - 
                  51*Power(t1,2)*Power(t2,2) + 99*t1*Power(t2,3) - 
                  61*Power(t2,4)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (8*Power(t1,4) - 55*Power(t1,3)*t2 + 
                  65*Power(t1,2)*Power(t2,2) + 25*t1*Power(t2,3) - 
                  38*Power(t2,4)) + 
               Power(s2,4)*(-7*Power(t1,4) - 3*Power(t1,3)*t2 + 
                  54*Power(t1,2)*Power(t2,2) - 73*t1*Power(t2,3) + 
                  28*Power(t2,4))) + 
            Power(s1,7)*t1*(-4*Power(s2,5)*(t1 - t2) + 
               Power(s2,4)*(36*Power(t1,2) + 6*t1*t2 - 38*Power(t2,2)) - 
               2*t1*Power(t1 - t2,2)*t2*
                (2*Power(t1,2) - 13*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,3)*(33*Power(t1,3) - 266*Power(t1,2)*t2 + 
                  279*t1*Power(t2,2) - 25*Power(t2,3)) + 
               Power(s2,2)*(-21*Power(t1,4) + 243*Power(t1,3)*t2 - 
                  450*Power(t1,2)*Power(t2,2) + 184*t1*Power(t2,3) + 
                  56*Power(t2,4)) + 
               s2*(-42*Power(t1,5) + 149*Power(t1,4)*t2 - 
                  229*Power(t1,3)*Power(t2,2) + 
                  217*Power(t1,2)*Power(t2,3) - 135*t1*Power(t2,4) + 
                  39*Power(t2,5))) + 
            Power(s1,6)*(2*Power(t1,2)*Power(t1 - t2,3)*t2*
                (8*Power(t1,2) + 3*t1*t2 - 8*Power(t2,2)) + 
               Power(s2,5)*(Power(t1,3) + 12*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,4)*(117*Power(t1,4) - 344*Power(t1,3)*t2 + 
                  191*Power(t1,2)*Power(t2,2) + 26*t1*Power(t2,3) - 
                  6*Power(t2,4)) - 
               Power(s2,3)*(56*Power(t1,5) + 88*Power(t1,4)*t2 - 
                  626*Power(t1,3)*Power(t2,2) + 
                  532*Power(t1,2)*Power(t2,3) - 49*t1*Power(t2,4) + 
                  6*Power(t2,5)) + 
               s2*t1*(-18*Power(t1,6) + 34*Power(t1,5)*t2 + 
                  100*Power(t1,4)*Power(t2,2) - 
                  263*Power(t1,3)*Power(t2,3) + 
                  121*Power(t1,2)*Power(t2,4) + 80*t1*Power(t2,5) - 
                  55*Power(t2,6)) + 
               Power(s2,2)*(-40*Power(t1,6) + 536*Power(t1,5)*t2 - 
                  1768*Power(t1,4)*Power(t2,2) + 
                  2123*Power(t1,3)*Power(t2,3) - 
                  801*Power(t1,2)*Power(t2,4) - 56*t1*Power(t2,5) + 
                  2*Power(t2,6))) + 
            Power(s1,5)*(Power(s2,5)*
                (19*Power(t1,4) - 51*Power(t1,3)*t2 + 
                  48*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) - 
                  12*Power(t2,4)) + 
               2*Power(t1,2)*Power(t1 - t2,2)*t2*
                (6*Power(t1,4) - 31*Power(t1,3)*t2 + 
                  29*Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,4)*(87*Power(t1,5) - 589*Power(t1,4)*t2 + 
                  952*Power(t1,3)*Power(t2,2) - 
                  535*Power(t1,2)*Power(t2,3) + 67*t1*Power(t2,4) + 
                  18*Power(t2,5)) + 
               Power(s2,3)*(-116*Power(t1,6) + 554*Power(t1,5)*t2 - 
                  525*Power(t1,4)*Power(t2,2) + 
                  22*Power(t1,3)*Power(t2,3) - 
                  26*Power(t1,2)*Power(t2,4) + 46*t1*Power(t2,5) + 
                  24*Power(t2,6)) + 
               Power(s2,2)*(18*Power(t1,7) + 173*Power(t1,6)*t2 - 
                  1614*Power(t1,5)*Power(t2,2) + 
                  3733*Power(t1,4)*Power(t2,3) - 
                  3357*Power(t1,3)*Power(t2,4) + 
                  993*Power(t1,2)*Power(t2,5) + 48*t1*Power(t2,6) - 
                  6*Power(t2,7)) + 
               s2*t1*(-6*Power(t1,7) - 21*Power(t1,6)*t2 + 
                  436*Power(t1,5)*Power(t2,2) - 
                  1355*Power(t1,4)*Power(t2,3) + 
                  1727*Power(t1,3)*Power(t2,4) - 
                  963*Power(t1,2)*Power(t2,5) + 157*t1*Power(t2,6) + 
                  26*Power(t2,7))) + 
            Power(s1,4)*(-2*Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*
                (15*Power(t1,4) - 45*Power(t1,3)*t2 + 
                  40*Power(t1,2)*Power(t2,2) - 14*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,5)*(23*Power(t1,5) - 112*Power(t1,4)*t2 + 
                  221*Power(t1,3)*Power(t2,2) - 
                  208*Power(t1,2)*Power(t2,3) + 46*t1*Power(t2,4) + 
                  24*Power(t2,5)) - 
               s2*t1*Power(t1 - t2,2)*
                (6*Power(t1,6) - 25*Power(t1,5)*t2 - 
                  142*Power(t1,4)*Power(t2,2) + 
                  693*Power(t1,3)*Power(t2,3) - 
                  763*Power(t1,2)*Power(t2,4) + 244*t1*Power(t2,5) - 
                  12*Power(t2,6)) - 
               Power(s2,4)*(21*Power(t1,6) + 188*Power(t1,5)*t2 - 
                  834*Power(t1,4)*Power(t2,2) + 
                  956*Power(t1,3)*Power(t2,3) - 
                  473*Power(t1,2)*Power(t2,4) + 100*t1*Power(t2,5) + 
                  18*Power(t2,6)) - 
               Power(s2,3)*(9*Power(t1,7) - 416*Power(t1,6)*t2 + 
                  1439*Power(t1,5)*Power(t2,2) - 
                  1890*Power(t1,4)*Power(t2,3) + 
                  1589*Power(t1,3)*Power(t2,4) - 
                  940*Power(t1,2)*Power(t2,5) + 159*t1*Power(t2,6) + 
                  36*Power(t2,7)) + 
               Power(s2,2)*(13*Power(t1,8) - 145*Power(t1,7)*t2 + 
                  87*Power(t1,6)*Power(t2,2) + 
                  1245*Power(t1,5)*Power(t2,3) - 
                  2694*Power(t1,4)*Power(t2,4) + 
                  1801*Power(t1,3)*Power(t2,5) - 
                  232*Power(t1,2)*Power(t2,6) - 73*t1*Power(t2,7) + 
                  6*Power(t2,8))) - 
            Power(s1,2)*t2*(2*Power(t1,3)*Power(t1 - t2,4)*(3*t1 - t2)*
                Power(t2,3) + 
               s2*t1*Power(t1 - t2,3)*t2*
                (13*Power(t1,5) - 49*Power(t1,4)*t2 - 
                  54*Power(t1,3)*Power(t2,2) + 
                  96*Power(t1,2)*Power(t2,3) - 33*t1*Power(t2,4) + 
                  5*Power(t2,5)) + 
               Power(s2,4)*Power(t1,2)*
                (-24*Power(t1,5) + 125*Power(t1,4)*t2 - 
                  301*Power(t1,3)*Power(t2,2) + 
                  504*Power(t1,2)*Power(t2,3) - 433*t1*Power(t2,4) + 
                  137*Power(t2,5)) - 
               Power(s2,2)*t1*Power(t1 - t2,2)*
                (20*Power(t1,6) - 10*Power(t1,5)*t2 - 
                  232*Power(t1,4)*Power(t2,2) + 
                  357*Power(t1,3)*Power(t2,3) - 
                  354*Power(t1,2)*Power(t2,4) + 236*t1*Power(t2,5) - 
                  21*Power(t2,6)) + 
               Power(s2,5)*(2*Power(t1,6) - 39*Power(t1,5)*t2 + 
                  166*Power(t1,4)*Power(t2,2) - 
                  270*Power(t1,3)*Power(t2,3) + 
                  160*Power(t1,2)*Power(t2,4) - 16*t1*Power(t2,5) - 
                  6*Power(t2,6)) + 
               Power(s2,3)*(42*Power(t1,8) - 149*Power(t1,7)*t2 + 
                  56*Power(t1,6)*Power(t2,2) + 
                  348*Power(t1,5)*Power(t2,3) - 
                  800*Power(t1,4)*Power(t2,4) + 
                  821*Power(t1,3)*Power(t2,5) - 
                  354*Power(t1,2)*Power(t2,6) + 30*t1*Power(t2,7) + 
                  6*Power(t2,8))) + 
            Power(s1,3)*(2*Power(t1,2)*Power(t1 - t2,4)*Power(t2,3)*
                (12*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
               Power(s2,5)*(9*Power(t1,6) - 59*Power(t1,5)*t2 + 
                  203*Power(t1,4)*Power(t2,2) - 
                  364*Power(t1,3)*Power(t2,3) + 
                  282*Power(t1,2)*Power(t2,4) - 49*t1*Power(t2,5) - 
                  20*Power(t2,6)) + 
               s2*t1*Power(t1 - t2,2)*t2*
                (16*Power(t1,6) - 70*Power(t1,5)*t2 - 
                  108*Power(t1,4)*Power(t2,2) + 
                  507*Power(t1,3)*Power(t2,3) - 
                  462*Power(t1,2)*Power(t2,4) + 129*t1*Power(t2,5) - 
                  17*Power(t2,6)) + 
               Power(s2,4)*(-35*Power(t1,7) + 111*Power(t1,6)*t2 - 
                  3*Power(t1,5)*Power(t2,2) - 
                  151*Power(t1,4)*Power(t2,3) + 
                  69*Power(t1,3)*Power(t2,4) - 
                  38*Power(t1,2)*Power(t2,5) + 37*t1*Power(t2,6) + 
                  6*Power(t2,7)) + 
               Power(s2,3)*(43*Power(t1,8) - 80*Power(t1,7)*t2 - 
                  428*Power(t1,6)*Power(t2,2) + 
                  1436*Power(t1,5)*Power(t2,3) - 
                  2053*Power(t1,4)*Power(t2,4) + 
                  1878*Power(t1,3)*Power(t2,5) - 
                  941*Power(t1,2)*Power(t2,6) + 121*t1*Power(t2,7) + 
                  24*Power(t2,8)) + 
               Power(s2,2)*(-17*Power(t1,9) + 12*Power(t1,8)*t2 + 
                  325*Power(t1,7)*Power(t2,2) - 
                  838*Power(t1,6)*Power(t2,3) + 
                  689*Power(t1,5)*Power(t2,4) - 
                  333*Power(t1,4)*Power(t2,5) + 
                  481*Power(t1,3)*Power(t2,6) - 
                  383*Power(t1,2)*Power(t2,7) + 66*t1*Power(t2,8) - 
                  2*Power(t2,9)))))*Log(-t2))/
     (s*Power(s1,2)*Power(s2,2)*Power(t1,2)*(s1 + t1)*(s - s2 + t1)*
       Power(s1 - t2,2)*Power(t1 - t2,2)*(s1 + t1 - t2)*t2*
       Power(s - s1 + t2,2)*(s2 - t1 + t2)) - 
    (8*(Power(s1,2)*Power(s2,2)*(s2 - t1)*(s1 + s2 - t1)*(s1 + t1 - t2)*
          t2*(s2 - t1 + t2)*(-(s2*t2) + s1*(s2 - t1 + 2*t2))*
          Power(Power(s1,2)*(s2 - t1) + s2*t2*(s2 - t1 + t2) - 
            s1*(Power(s2,2) - 2*s2*t1 + Power(t1,2) + 2*s2*t2 - t1*t2),2) \
+ Power(s,9)*(t1 - t2)*t2*(Power(s1,2)*
             (4*Power(s2,3)*t1 - 8*Power(s2,2)*Power(t1 - t2,2) + 
               s2*(11*t1 - 8*t2)*Power(t1 - t2,2) - 
               4*t1*Power(t1 - t2,3)) - 
            2*s2*Power(t1 - t2,2)*t2*Power(s2 - t1 + t2,2) + 
            Power(s1,3)*(4*s2*Power(t1 - t2,2) - 
               2*t1*Power(t1 - t2,2) + 4*Power(s2,2)*t2) + 
            s1*Power(t1 - t2,2)*
             (4*Power(s2,3) + 7*s2*Power(t1 - t2,2) - 
               2*t1*Power(t1 - t2,2) + Power(s2,2)*(-8*t1 + 11*t2))) + 
         Power(s,8)*(2*s2*Power(t1 - t2,2)*Power(t2,2)*
             Power(s2 - t1 + t2,2)*
             (3*s2*t1 - 2*Power(t1,2) - 5*s2*t2 - t1*t2 + 3*Power(t2,2)) \
+ Power(s1,4)*(2*t1*(5*t1 - 3*t2)*Power(t1 - t2,2)*t2 + 
               Power(s2,3)*t2*(-5*t1 + t2) + 
               2*s2*t1*(Power(t1,3) - 10*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) - 14*Power(t2,3)) + 
               Power(s2,2)*(-2*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  16*t1*Power(t2,2) + Power(t2,3))) + 
            Power(s1,2)*(2*t1*(t1 - 9*t2)*Power(t1 - t2,4)*t2 + 
               Power(s2,5)*t2*(-t1 + t2) + 
               s2*Power(t1 - t2,3)*
                (6*Power(t1,3) + 13*Power(t1,2)*t2 + 
                  56*t1*Power(t2,2) - 30*Power(t2,3)) - 
               Power(s2,4)*(2*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  5*Power(t2,3)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (14*Power(t1,3) + 15*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) + 22*Power(t2,3)) + 
               2*Power(s2,3)*
                (5*Power(t1,4) - 3*Power(t1,3)*t2 + 
                  9*Power(t1,2)*Power(t2,2) + 18*t1*Power(t2,3) - 
                  29*Power(t2,4))) + 
            Power(s1,3)*(2*t1*(8*t1 - 9*t2)*Power(t1 - t2,3)*t2 + 
               2*Power(s2,4)*t2*(t1 + t2) + 
               s2*Power(t1 - t2,2)*
                (6*Power(t1,3) - 31*Power(t1,2)*t2 + 
                  63*t1*Power(t2,2) - 11*Power(t2,3)) + 
               Power(s2,3)*(4*Power(t1,3) - 31*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) + 37*Power(t2,3)) + 
               Power(s2,2)*(-10*Power(t1,4) + 44*Power(t1,3)*t2 - 
                  60*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) + 
                  24*Power(t2,4))) - 
            s1*(t1 - t2)*(Power(s2,5)*(t1 - t2)*t2 + 
               2*t1*Power(t1 - t2,4)*t2*(2*t1 + 3*t2) - 
               2*s2*Power(t1 - t2,3)*
                (Power(t1,3) + 13*Power(t1,2)*t2 + 5*t1*Power(t2,2) - 
                  13*Power(t2,3)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (6*Power(t1,3) + 44*Power(t1,2)*t2 - 
                  18*t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,4)*(2*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  18*t1*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s2,3)*(-6*Power(t1,4) - 24*Power(t1,3)*t2 + 
                  64*Power(t1,2)*Power(t2,2) - 77*t1*Power(t2,3) + 
                  43*Power(t2,4)))) + 
         Power(s,7)*(-2*s2*(t1 - t2)*Power(t2,2)*Power(s2 - t1 + t2,2)*
             (Power(t1 - t2,2)*
                (Power(t1,2) + 6*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(3*Power(t1,2) - 12*t1*t2 + 
                  10*Power(t2,2)) + 
               s2*(-4*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  16*t1*Power(t2,2) - 15*Power(t2,3))) - 
            Power(s1,5)*(Power(s2,4)*t2 + 
               Power(s2,3)*(4*Power(t1,2) - 22*t1*t2 + 
                  29*Power(t2,2)) + 
               2*t1*t2*(10*Power(t1,3) - 22*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*(-14*Power(t1,3) + 27*Power(t1,2)*t2 - 
                  56*t1*Power(t2,2) + 19*Power(t2,3)) + 
               s2*(10*Power(t1,4) - 44*Power(t1,3)*t2 + 
                  73*Power(t1,2)*Power(t2,2) - 34*t1*Power(t2,3) - 
                  9*Power(t2,4))) + 
            Power(s1,2)*(Power(s2,6)*(3*t1 - 4*t2)*t2 + 
               2*t1*Power(t1 - t2,4)*t2*
                (7*Power(t1,2) - 3*t1*t2 - 14*Power(t2,2)) + 
               Power(s2,5)*(2*Power(t1,3) + 15*Power(t1,2)*t2 - 
                  8*t1*Power(t2,2) + 45*Power(t2,3)) + 
               s2*Power(t1 - t2,3)*
                (2*Power(t1,4) - 31*Power(t1,3)*t2 + 
                  155*Power(t1,2)*Power(t2,2) + 125*t1*Power(t2,3) - 
                  51*Power(t2,4)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (12*Power(t1,4) + 4*Power(t1,3)*t2 + 
                  129*Power(t1,2)*Power(t2,2) + 199*t1*Power(t2,3) + 
                  104*Power(t2,4)) + 
               Power(s2,4)*(-12*Power(t1,4) - 43*Power(t1,3)*t2 + 
                  38*Power(t1,2)*Power(t2,2) - 242*t1*Power(t2,3) + 
                  197*Power(t2,4)) + 
               Power(s2,3)*(20*Power(t1,5) + 28*Power(t1,4)*t2 - 
                  87*Power(t1,3)*Power(t2,2) + 
                  438*Power(t1,2)*Power(t2,3) - 392*t1*Power(t2,4) - 
                  7*Power(t2,5))) + 
            Power(s1,4)*(2*Power(s2,5)*t2 - 
               2*t1*Power(t1 - t2,2)*t2*
                (11*Power(t1,2) - 33*t1*t2 + 14*Power(t2,2)) + 
               Power(s2,4)*(8*Power(t1,2) - 9*t1*t2 + 42*Power(t2,2)) + 
               Power(s2,3)*(-34*Power(t1,3) + 109*Power(t1,2)*t2 - 
                  201*t1*Power(t2,2) + 88*Power(t2,3)) + 
               Power(s2,2)*(52*Power(t1,4) - 139*Power(t1,3)*t2 + 
                  213*Power(t1,2)*Power(t2,2) - 224*t1*Power(t2,3) + 
                  32*Power(t2,4)) - 
               s2*(26*Power(t1,5) - 79*Power(t1,4)*t2 + 
                  186*Power(t1,3)*Power(t2,2) - 
                  306*Power(t1,2)*Power(t2,3) + 157*t1*Power(t2,4) + 
                  16*Power(t2,5))) + 
            s1*(-2*t1*Power(t1 - t2,5)*t2*
                (Power(t1,2) + 6*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,6)*t2*
                (4*Power(t1,2) - 9*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,5)*(6*Power(t1,4) - 3*Power(t1,3)*t2 - 
                  6*Power(t1,2)*Power(t2,2) + 43*t1*Power(t2,3) - 
                  44*Power(t2,4)) + 
               4*s2*Power(t1 - t2,4)*
                (Power(t1,4) + 8*Power(t1,3)*t2 + 
                  21*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) - 
                  9*Power(t2,4)) - 
               Power(s2,2)*Power(t1 - t2,3)*
                (18*Power(t1,4) + 88*Power(t1,3)*t2 + 
                  87*Power(t1,2)*Power(t2,2) - 80*t1*Power(t2,3) + 
                  71*Power(t2,4)) + 
               Power(s2,3)*Power(t1 - t2,2)*
                (30*Power(t1,4) + 93*Power(t1,3)*t2 - 
                  27*Power(t1,2)*Power(t2,2) + 9*t1*Power(t2,3) + 
                  112*Power(t2,4)) - 
               Power(s2,4)*(22*Power(t1,5) + 14*Power(t1,4)*t2 - 
                  97*Power(t1,3)*Power(t2,2) + 
                  153*Power(t1,2)*Power(t2,3) - 136*t1*Power(t2,4) + 
                  44*Power(t2,5))) + 
            Power(s1,3)*(-(Power(s2,6)*t2) + 
               Power(s2,5)*(-4*Power(t1,2) + 10*t1*t2 - 
                  35*Power(t2,2)) + 
               2*t1*Power(t1 - t2,3)*t2*
                (7*Power(t1,2) + 24*t1*t2 - 22*Power(t2,2)) + 
               Power(s2,4)*(18*Power(t1,3) - 13*Power(t1,2)*t2 + 
                  164*t1*Power(t2,2) - 209*Power(t2,3)) + 
               Power(s2,3)*(-40*Power(t1,4) + 68*Power(t1,3)*t2 - 
                  429*Power(t1,2)*Power(t2,2) + 646*t1*Power(t2,3) - 
                  117*Power(t2,4)) - 
               s2*Power(t1 - t2,2)*
                (18*Power(t1,4) + 54*Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) - 263*t1*Power(t2,3) + 
                  13*Power(t2,4)) + 
               Power(s2,2)*(44*Power(t1,5) - 58*Power(t1,4)*t2 + 
                  209*Power(t1,3)*Power(t2,2) - 
                  580*Power(t1,2)*Power(t2,3) + 340*t1*Power(t2,4) + 
                  45*Power(t2,5)))) + 
         Power(s,6)*(-(Power(s1,6)*
               (2*Power(s2,4)*(t1 + 2*t2) + 
                 Power(s2,3)*
                  (-18*Power(t1,2) + 35*t1*t2 - 50*Power(t2,2)) + 
                 Power(s2,2)*
                  (36*Power(t1,3) - 61*Power(t1,2)*t2 + 
                    88*t1*Power(t2,2) - 50*Power(t2,3)) + 
                 2*t1*t2*(-10*Power(t1,3) + 18*Power(t1,2)*t2 - 
                    9*t1*Power(t2,2) + Power(t2,3)) + 
                 s2*(-20*Power(t1,4) + 56*Power(t1,3)*t2 - 
                    61*Power(t1,2)*Power(t2,2) + 35*t1*Power(t2,3) + 
                    4*Power(t2,4)))) + 
            2*s2*(t1 - t2)*Power(t2,2)*Power(s2 - t1 + t2,2)*
             (-(Power(t1 - t2,2)*t2*
                  (3*Power(t1,2) + 6*t1*t2 + Power(t2,2))) + 
               Power(s2,3)*(Power(t1,2) - 8*t1*t2 + 10*Power(t2,2)) + 
               Power(s2,2)*(-2*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  24*t1*Power(t2,2) - 30*Power(t2,3)) + 
               s2*(Power(t1,4) + 8*Power(t1,3)*t2 - 
                  24*Power(t1,2)*Power(t2,2) + 15*Power(t2,4))) + 
            Power(s1,5)*(2*Power(s2,5)*(2*t1 + 5*t2) + 
               Power(s2,4)*(-30*Power(t1,2) - 9*t1*t2 + 
                  35*Power(t2,2)) + 
               Power(s2,3)*(80*Power(t1,3) - 200*Power(t1,2)*t2 + 
                  257*t1*Power(t2,2) - 208*Power(t2,3)) + 
               Power(s2,2)*(-96*Power(t1,4) + 276*Power(t1,3)*t2 - 
                  412*Power(t1,2)*Power(t2,2) + 528*t1*Power(t2,3) - 
                  211*Power(t2,4)) + 
               2*t1*t2*(4*Power(t1,4) - 48*Power(t1,3)*t2 + 
                  88*Power(t1,2)*Power(t2,2) - 53*t1*Power(t2,3) + 
                  9*Power(t2,4)) + 
               s2*(42*Power(t1,5) - 101*Power(t1,4)*t2 + 
                  197*Power(t1,3)*Power(t2,2) - 
                  368*Power(t1,2)*Power(t2,3) + 231*t1*Power(t2,4) + 
                  22*Power(t2,5))) + 
            Power(s1,3)*(4*Power(s2,7)*t2 + 
               Power(s2,6)*(10*Power(t1,2) - 39*t1*t2 + 
                  121*Power(t2,2)) - 
               2*t1*Power(t1 - t2,3)*t2*
                (8*Power(t1,3) - 35*Power(t1,2)*t2 - 
                  20*t1*Power(t2,2) + 22*Power(t2,3)) + 
               Power(s2,5)*(-60*Power(t1,3) + 104*Power(t1,2)*t2 - 
                  614*t1*Power(t2,2) + 461*Power(t2,3)) + 
               Power(s2,4)*(152*Power(t1,4) - 196*Power(t1,3)*t2 + 
                  1248*Power(t1,2)*Power(t2,2) - 1464*t1*Power(t2,3) - 
                  11*Power(t2,4)) + 
               Power(s2,3)*(-192*Power(t1,5) + 218*Power(t1,4)*t2 - 
                  830*Power(t1,3)*Power(t2,2) + 
                  841*Power(t1,2)*Power(t2,3) + 633*t1*Power(t2,4) - 
                  390*Power(t2,5)) - 
               s2*Power(t1 - t2,2)*
                (28*Power(t1,5) + 19*Power(t1,4)*t2 + 
                  317*Power(t1,3)*Power(t2,2) - 
                  313*Power(t1,2)*Power(t2,3) - 518*t1*Power(t2,4) + 
                  27*Power(t2,5)) + 
               Power(s2,2)*(118*Power(t1,6) - 112*Power(t1,5)*t2 + 
                  257*Power(t1,4)*Power(t2,2) - 
                  318*Power(t1,3)*Power(t2,3) - 
                  832*Power(t1,2)*Power(t2,4) + 949*t1*Power(t2,5) - 
                  62*Power(t2,6))) - 
            Power(s1,4)*(2*Power(s2,6)*(t1 + 5*t2) + 
               Power(s2,5)*(-2*Power(t1,2) - 17*t1*t2 + 
                  144*Power(t2,2)) + 
               2*t1*Power(t1 - t2,2)*t2*
                (18*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  61*t1*Power(t2,2) + 22*Power(t2,3)) + 
               Power(s2,4)*(-8*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  363*t1*Power(t2,2) + 73*Power(t2,3)) + 
               Power(s2,3)*(10*Power(t1,4) + 127*Power(t1,3)*t2 - 
                  349*Power(t1,2)*Power(t2,2) + 647*t1*Power(t2,3) - 
                  350*Power(t2,4)) + 
               Power(s2,2)*(6*Power(t1,5) - 144*Power(t1,4)*t2 + 
                  534*Power(t1,3)*Power(t2,2) - 
                  1075*Power(t1,2)*Power(t2,3) + 1097*t1*Power(t2,4) - 
                  268*Power(t2,5)) + 
               s2*(-8*Power(t1,6) - 19*Power(t1,5)*t2 + 
                  109*Power(t1,4)*Power(t2,2) + 
                  184*Power(t1,3)*Power(t2,3) - 
                  803*Power(t1,2)*Power(t2,4) + 516*t1*Power(t2,5) + 
                  21*Power(t2,6))) + 
            Power(s1,2)*(-2*Power(s2,7)*(t1 - 3*t2)*t2 + 
               2*t1*Power(t1 - t2,5)*t2*
                (4*Power(t1,2) + 21*t1*t2 + 9*Power(t2,2)) + 
               Power(s2,6)*(6*Power(t1,3) - 45*Power(t1,2)*t2 + 
                  74*t1*Power(t2,2) - 131*Power(t2,3)) + 
               Power(s2,5)*(-32*Power(t1,4) + 221*Power(t1,3)*t2 - 
                  391*Power(t1,2)*Power(t2,2) + 798*t1*Power(t2,3) - 
                  325*Power(t2,4)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (50*Power(t1,5) + 7*Power(t1,4)*t2 - 
                  24*Power(t1,3)*Power(t2,2) - 
                  603*Power(t1,2)*Power(t2,3) - 260*t1*Power(t2,4) - 
                  150*Power(t2,5)) - 
               s2*Power(t1 - t2,3)*
                (12*Power(t1,5) + 53*Power(t1,4)*t2 + 
                  25*Power(t1,3)*Power(t2,2) - 
                  435*Power(t1,2)*Power(t2,3) - 140*t1*Power(t2,4) + 
                  51*Power(t2,5)) + 
               Power(s2,4)*(72*Power(t1,5) - 402*Power(t1,4)*t2 + 
                  790*Power(t1,3)*Power(t2,2) - 
                  1817*Power(t1,2)*Power(t2,3) + 1033*t1*Power(t2,4) + 
                  194*Power(t2,5)) + 
               Power(s2,3)*(-84*Power(t1,6) + 330*Power(t1,5)*t2 - 
                  591*Power(t1,4)*Power(t2,2) + 
                  1471*Power(t1,3)*Power(t2,3) - 
                  981*Power(t1,2)*Power(t2,4) - 326*t1*Power(t2,5) + 
                  181*Power(t2,6))) + 
            s1*(-2*t1*Power(t1 - t2,5)*Power(t2,2)*
                (3*Power(t1,2) + 6*t1*t2 + Power(t2,2)) - 
               2*Power(s2,7)*t2*
                (3*Power(t1,2) - 8*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,6)*(-6*Power(t1,4) + 4*Power(t1,3)*t2 - 
                  24*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
                  48*Power(t2,4)) + 
               Power(s2,5)*(26*Power(t1,5) + 46*Power(t1,4)*t2 - 
                  111*Power(t1,3)*Power(t2,2) + 
                  129*Power(t1,2)*Power(t2,3) - 82*t1*Power(t2,4) - 
                  31*Power(t2,5)) + 
               2*s2*Power(t1 - t2,4)*
                (Power(t1,5) + 9*Power(t1,4)*t2 + 
                  50*Power(t1,3)*Power(t2,2) + 
                  46*Power(t1,2)*Power(t2,3) - 25*t1*Power(t2,4) - 
                  11*Power(t2,5)) + 
               Power(s2,3)*Power(t1 - t2,2)*
                (36*Power(t1,5) + 182*Power(t1,4)*t2 + 
                  55*Power(t1,3)*Power(t2,2) - 
                  118*Power(t1,2)*Power(t2,3) + 238*t1*Power(t2,4) + 
                  7*Power(t2,5)) - 
               Power(s2,2)*Power(t1 - t2,3)*
                (14*Power(t1,5) + 94*Power(t1,4)*t2 + 
                  207*Power(t1,3)*Power(t2,2) - 
                  6*Power(t1,2)*Power(t2,3) - 36*t1*Power(t2,4) + 
                  133*Power(t2,5)) - 
               Power(s2,4)*(44*Power(t1,6) + 112*Power(t1,5)*t2 - 
                  325*Power(t1,4)*Power(t2,2) + 
                  234*Power(t1,3)*Power(t2,3) + 
                  111*Power(t1,2)*Power(t2,4) - 413*t1*Power(t2,5) + 
                  237*Power(t2,6)))) + 
         Power(s,5)*(Power(s1,7)*
             (Power(s2,4)*(6*t1 + 9*t2) + 
               Power(s2,3)*(-30*Power(t1,2) + 34*t1*t2 - 
                  19*Power(t2,2)) - 
               2*Power(t1,2)*t2*
                (5*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(44*Power(t1,3) - 73*Power(t1,2)*t2 + 
                  56*t1*Power(t2,2) - 29*Power(t2,3)) - 
               s2*(20*Power(t1,4) - 44*Power(t1,3)*t2 + 
                  27*Power(t1,2)*Power(t2,2) - 22*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            2*s2*(t1 - t2)*Power(t2,3)*Power(s2 - t1 + t2,2)*
             (Power(s2,4)*(2*t1 - 5*t2) - 
               t1*Power(t1 - t2,2)*t2*(3*t1 + 2*t2) - 
               Power(s2,3)*(Power(t1,2) + 16*t1*t2 - 30*Power(t2,2)) + 
               Power(s2,2)*(-4*Power(t1,3) + 24*Power(t1,2)*t2 - 
                  30*Power(t2,3)) + 
               s2*(3*Power(t1,4) - 24*Power(t1,2)*Power(t2,2) + 
                  16*t1*Power(t2,3) + 5*Power(t2,4))) + 
            Power(s1,6)*(Power(s2,5)*(-6*t1 + 2*t2) + 
               4*Power(s2,4)*
                (7*Power(t1,2) - 6*t1*t2 - 38*Power(t2,2)) + 
               Power(s2,3)*(-62*Power(t1,3) + 240*Power(t1,2)*t2 - 
                  71*t1*Power(t2,2) + 41*Power(t2,3)) + 
               2*t1*t2*(4*Power(t1,4) + 24*Power(t1,3)*t2 - 
                  49*Power(t1,2)*Power(t2,2) + 23*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,2)*(68*Power(t1,4) - 314*Power(t1,3)*t2 + 
                  421*Power(t1,2)*Power(t2,2) - 342*t1*Power(t2,3) + 
                  192*Power(t2,4)) - 
               s2*(28*Power(t1,5) - 93*Power(t1,4)*t2 + 
                  191*Power(t1,3)*Power(t2,2) - 
                  225*Power(t1,2)*Power(t2,3) + 164*t1*Power(t2,4) + 
                  3*Power(t2,5))) + 
            Power(s1,5)*(-(Power(s2,6)*(6*t1 + 29*t2)) + 
               Power(s2,5)*(52*Power(t1,2) + 83*t1*t2 + 
                  89*Power(t2,2)) + 
               Power(s2,4)*(-150*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  505*t1*Power(t2,2) + 619*Power(t2,3)) + 
               Power(s2,3)*(198*Power(t1,4) - 42*Power(t1,3)*t2 - 
                  22*Power(t1,2)*Power(t2,2) - 398*t1*Power(t2,3) + 
                  49*Power(t2,4)) + 
               2*t1*t2*(17*Power(t1,5) - 40*Power(t1,4)*t2 - 
                  27*Power(t1,3)*Power(t2,2) + 
                  105*Power(t1,2)*Power(t2,3) - 64*t1*Power(t2,4) + 
                  9*Power(t2,5)) - 
               Power(s2,2)*(122*Power(t1,5) + 36*Power(t1,4)*t2 - 
                  558*Power(t1,3)*Power(t2,2) + 
                  697*Power(t1,2)*Power(t2,3) - 821*t1*Power(t2,4) + 
                  442*Power(t2,5)) + 
               s2*(28*Power(t1,6) - 20*Power(t1,5)*t2 + 
                  6*Power(t1,4)*Power(t2,2) + 
                  96*Power(t1,3)*Power(t2,3) - 
                  562*Power(t1,2)*Power(t2,4) + 497*t1*Power(t2,5) + 
                  10*Power(t2,6))) + 
            s1*(-2*Power(t1,2)*Power(t1 - t2,5)*Power(t2,3)*
                (3*t1 + 2*t2) + 
               2*Power(s2,8)*t2*
                (2*Power(t1,2) - 7*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,7)*(2*Power(t1,4) + Power(t1,3)*t2 + 
                  33*Power(t1,2)*Power(t2,2) - 22*t1*Power(t2,3) - 
                  32*Power(t2,4)) + 
               s2*Power(t1 - t2,4)*t2*
                (5*Power(t1,5) + 39*Power(t1,4)*t2 + 
                  121*Power(t1,3)*Power(t2,2) + 
                  35*Power(t1,2)*Power(t2,3) - 35*t1*Power(t2,4) - 
                  5*Power(t2,5)) + 
               Power(s2,6)*(-10*Power(t1,5) - 72*Power(t1,4)*t2 + 
                  60*Power(t1,3)*Power(t2,2) - 
                  41*Power(t1,2)*Power(t2,3) + 42*t1*Power(t2,4) + 
                  90*Power(t2,5)) + 
               Power(s2,3)*Power(t1 - t2,2)*
                (10*Power(t1,6) + 179*Power(t1,5)*t2 + 
                  177*Power(t1,4)*Power(t2,2) - 
                  139*Power(t1,3)*Power(t2,3) - 
                  36*Power(t1,2)*Power(t2,4) + 289*t1*Power(t2,5) - 
                  160*Power(t2,6)) - 
               Power(s2,2)*Power(t1 - t2,3)*
                (2*Power(t1,6) + 54*Power(t1,5)*t2 + 
                  162*Power(t1,4)*Power(t2,2) + 
                  158*Power(t1,3)*Power(t2,3) - 
                  123*Power(t1,2)*Power(t2,4) + 84*t1*Power(t2,5) + 
                  88*Power(t2,6)) + 
               Power(s2,5)*(20*Power(t1,6) + 203*Power(t1,5)*t2 - 
                  289*Power(t1,4)*Power(t2,2) + 
                  91*Power(t1,3)*Power(t2,3) + 
                  127*Power(t1,2)*Power(t2,4) - 382*t1*Power(t2,5) + 
                  175*Power(t2,6)) + 
               Power(s2,4)*(-20*Power(t1,7) - 252*Power(t1,6)*t2 + 
                  368*Power(t1,5)*Power(t2,2) + 
                  119*Power(t1,4)*Power(t2,3) - 
                  512*Power(t1,3)*Power(t2,4) + 
                  445*Power(t1,2)*Power(t2,5) + 62*t1*Power(t2,6) - 
                  210*Power(t2,7))) - 
            Power(s1,2)*(2*Power(s2,8)*t2*(t1 + 2*t2) + 
               Power(s2,7)*(10*Power(t1,3) - 50*Power(t1,2)*t2 + 
                  123*t1*Power(t2,2) - 174*Power(t2,3)) - 
               2*t1*Power(t1 - t2,4)*Power(t2,2)*
                (11*Power(t1,3) + 12*Power(t1,2)*t2 - 
                  11*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,6)*(-60*Power(t1,4) + 214*Power(t1,3)*t2 - 
                  531*Power(t1,2)*Power(t2,2) + 1020*t1*Power(t2,3) - 
                  209*Power(t2,4)) + 
               Power(s2,5)*(148*Power(t1,5) - 404*Power(t1,4)*t2 + 
                  738*Power(t1,3)*Power(t2,2) - 
                  1988*Power(t1,2)*Power(t2,3) + 422*t1*Power(t2,4) + 
                  523*Power(t2,5)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (52*Power(t1,6) + 110*Power(t1,5)*t2 + 
                  68*Power(t1,4)*Power(t2,2) - 
                  376*Power(t1,3)*Power(t2,3) - 
                  895*Power(t1,2)*Power(t2,4) + 66*t1*Power(t2,5) - 
                  145*Power(t2,6)) + 
               s2*Power(t1 - t2,3)*
                (8*Power(t1,6) + 40*Power(t1,5)*t2 + 
                  149*Power(t1,4)*Power(t2,2) - 
                  172*Power(t1,3)*Power(t2,3) - 
                  526*Power(t1,2)*Power(t2,4) - 61*t1*Power(t2,5) + 
                  27*Power(t2,6)) + 
               Power(s2,4)*(-192*Power(t1,6) + 382*Power(t1,5)*t2 - 
                  255*Power(t1,4)*Power(t2,2) + 
                  1116*Power(t1,3)*Power(t2,3) + 
                  920*Power(t1,2)*Power(t2,4) - 2042*t1*Power(t2,5) + 
                  211*Power(t2,6)) + 
               Power(s2,3)*(138*Power(t1,7) - 154*Power(t1,6)*t2 - 
                  210*Power(t1,5)*Power(t2,2) + 
                  67*Power(t1,4)*Power(t2,3) - 
                  1669*Power(t1,3)*Power(t2,4) + 
                  2728*Power(t1,2)*Power(t2,5) - 729*t1*Power(t2,6) - 
                  171*Power(t2,7))) + 
            Power(s1,4)*(6*Power(s2,7)*(t1 + 4*t2) - 
               2*Power(s2,6)*
                (22*Power(t1,2) + 27*t1*t2 - 87*Power(t2,2)) + 
               Power(s2,5)*(158*Power(t1,3) - 167*Power(t1,2)*t2 - 
                  273*t1*Power(t2,2) - 367*Power(t2,3)) + 
               2*t1*Power(t1 - t2,3)*t2*
                (2*Power(t1,3) - 57*Power(t1,2)*t2 - 
                  28*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,4)*(-320*Power(t1,4) + 774*Power(t1,3)*t2 - 
                  944*Power(t1,2)*Power(t2,2) + 2229*t1*Power(t2,3) - 
                  1034*Power(t2,4)) + 
               Power(s2,3)*(364*Power(t1,5) - 1033*Power(t1,4)*t2 + 
                  1691*Power(t1,3)*Power(t2,2) - 
                  2357*Power(t1,2)*Power(t2,3) + 1429*t1*Power(t2,4) - 
                  115*Power(t2,5)) + 
               Power(s2,2)*(-216*Power(t1,6) + 546*Power(t1,5)*t2 - 
                  809*Power(t1,4)*Power(t2,2) + 
                  804*Power(t1,3)*Power(t2,3) + 
                  44*Power(t1,2)*Power(t2,4) - 958*t1*Power(t2,5) + 
                  409*Power(t2,6)) + 
               s2*(52*Power(t1,7) - 94*Power(t1,6)*t2 + 
                  299*Power(t1,5)*Power(t2,2) - 
                  941*Power(t1,4)*Power(t2,3) + 
                  781*Power(t1,3)*Power(t2,4) + 
                  630*Power(t1,2)*Power(t2,5) - 734*t1*Power(t2,6) + 
                  7*Power(t2,7))) - 
            Power(s1,3)*(6*Power(s2,8)*t2 + 
               Power(s2,7)*(6*Power(t1,2) - 53*t1*t2 + 
                  178*Power(t2,2)) + 
               Power(s2,6)*(-32*Power(t1,3) + 55*Power(t1,2)*t2 - 
                  852*t1*Power(t2,2) + 392*Power(t2,3)) + 
               Power(s2,5)*(68*Power(t1,4) + 271*Power(t1,3)*t2 + 
                  1229*Power(t1,2)*Power(t2,2) - 797*t1*Power(t2,3) - 
                  690*Power(t2,4)) + 
               2*t1*Power(t1 - t2,3)*t2*
                (6*Power(t1,4) + 13*Power(t1,3)*t2 - 
                  54*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
                  9*Power(t2,4)) - 
               Power(s2,4)*(64*Power(t1,5) + 792*Power(t1,4)*t2 - 
                  166*Power(t1,3)*Power(t2,2) + 
                  1395*Power(t1,2)*Power(t2,3) - 3632*t1*Power(t2,4) + 
                  924*Power(t2,5)) - 
               s2*Power(t1 - t2,2)*
                (8*Power(t1,6) - 30*Power(t1,5)*t2 - 
                  111*Power(t1,4)*Power(t2,2) - 
                  483*Power(t1,3)*Power(t2,3) + 
                  827*Power(t1,2)*Power(t2,4) + 464*t1*Power(t2,5) - 
                  35*Power(t2,6)) + 
               Power(s2,3)*(14*Power(t1,6) + 850*Power(t1,5)*t2 - 
                  1431*Power(t1,4)*Power(t2,2) + 
                  3066*Power(t1,3)*Power(t2,3) - 
                  4908*Power(t1,2)*Power(t2,4) + 2073*t1*Power(t2,5) + 
                  16*Power(t2,6)) + 
               Power(s2,2)*(16*Power(t1,7) - 395*Power(t1,6)*t2 + 
                  677*Power(t1,5)*Power(t2,2) - 
                  1345*Power(t1,4)*Power(t2,3) + 
                  2621*Power(t1,3)*Power(t2,4) - 
                  1391*Power(t1,2)*Power(t2,5) - 248*t1*Power(t2,6) + 
                  65*Power(t2,7)))) + 
         s*t2*(2*Power(s2,4)*Power(s2 - t1,2)*(t1 - t2)*Power(t2,6)*
             Power(s2 - t1 + t2,2) - 
            Power(s1,9)*s2*Power(s2 - t1,2)*
             (3*Power(s2,3) + Power(s2,2)*(-3*t1 + 7*t2) + 
               t1*(3*Power(t1,2) - 8*t1*t2 + 4*Power(t2,2)) + 
               s2*(-3*Power(t1,2) + t1*t2 + 4*Power(t2,2))) - 
            Power(s1,8)*(s2 - t1)*
             (Power(s2,6) - Power(s2,5)*t1 + 
               2*Power(t1,3)*(t1 - t2)*Power(t2,2) + 
               Power(s2,4)*(3*Power(t1,2) - 7*t1*t2 - 21*Power(t2,2)) - 
               Power(s2,3)*(13*Power(t1,3) - 45*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 19*Power(t2,3)) - 
               s2*t1*(6*Power(t1,4) - 31*Power(t1,3)*t2 + 
                  59*Power(t1,2)*Power(t2,2) - 27*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(16*Power(t1,4) - 69*Power(t1,3)*t2 + 
                  90*Power(t1,2)*Power(t2,2) - 18*t1*Power(t2,3) + 
                  Power(t2,4))) - 
            s1*Power(s2,2)*Power(s2 - t1,2)*Power(t2,3)*
             (Power(s2,6)*(t1 - t2) + 
               Power(s2,5)*(-11*Power(t1,2) + 15*t1*t2 - 
                  2*Power(t2,2)) + 
               Power(s2,4)*(41*Power(t1,3) - 78*Power(t1,2)*t2 + 
                  38*t1*Power(t2,2) - 8*Power(t2,3)) + 
               2*t1*Power(t1 - t2,3)*
                (2*Power(t1,3) - 4*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  Power(t2,3)) + 
               Power(s2,3)*(-71*Power(t1,4) + 185*Power(t1,3)*t2 - 
                  154*Power(t1,2)*Power(t2,2) + 55*t1*Power(t2,3) - 
                  12*Power(t2,4)) - 
               s2*Power(t1 - t2,2)*
                (26*Power(t1,4) - 58*Power(t1,3)*t2 + 
                  34*Power(t1,2)*Power(t2,2) - 7*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,2)*t1*
                (62*Power(t1,4) - 211*Power(t1,3)*t2 + 
                  256*Power(t1,2)*Power(t2,2) - 137*t1*Power(t2,3) + 
                  30*Power(t2,4))) + 
            Power(s1,7)*(8*Power(s2,8) + Power(s2,7)*(-43*t1 + 39*t2) + 
               2*Power(t1,4)*Power(t2,2)*
                (Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,6)*(92*Power(t1,2) - 169*t1*t2 + 
                  62*Power(t2,2)) - 
               Power(s2,5)*(98*Power(t1,3) - 294*Power(t1,2)*t2 + 
                  231*t1*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s2,4)*(52*Power(t1,4) - 279*Power(t1,3)*t2 + 
                  427*Power(t1,2)*Power(t2,2) - 48*t1*Power(t2,3) - 
                  59*Power(t2,4)) + 
               Power(s2,2)*t1*t2*
                (-84*Power(t1,4) + 347*Power(t1,3)*t2 - 
                  445*Power(t1,2)*Power(t2,2) + 119*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               s2*Power(t1,2)*t2*
                (21*Power(t1,4) - 101*Power(t1,3)*t2 + 
                  172*Power(t1,2)*Power(t2,2) - 79*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,3)*(-11*Power(t1,5) + 178*Power(t1,4)*t2 - 
                  506*Power(t1,3)*Power(t2,2) + 
                  359*Power(t1,2)*Power(t2,3) + 9*t1*Power(t2,4) + 
                  2*Power(t2,5))) - 
            Power(s1,6)*(Power(s2,9) - 5*Power(s2,8)*(3*t1 - 4*t2) + 
               2*Power(s2,7)*
                (30*Power(t1,2) - 63*t1*t2 + 52*Power(t2,2)) + 
               2*Power(t1,4)*Power(t2,2)*
                (Power(t1,3) - 4*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,6)*(-104*Power(t1,3) + 274*Power(t1,2)*t2 - 
                  409*t1*Power(t2,2) + 159*Power(t2,3)) + 
               Power(s2,5)*(75*Power(t1,4) - 207*Power(t1,3)*t2 + 
                  523*Power(t1,2)*Power(t2,2) - 411*t1*Power(t2,3) - 
                  34*Power(t2,4)) + 
               Power(s2,4)*(9*Power(t1,5) - 88*Power(t1,4)*t2 - 
                  144*Power(t1,3)*Power(t2,2) + 
                  433*Power(t1,2)*Power(t2,3) + 33*t1*Power(t2,4) - 
                  108*Power(t2,5)) + 
               Power(s2,3)*t1*
                (-50*Power(t1,5) + 236*Power(t1,4)*t2 - 
                  185*Power(t1,3)*Power(t2,2) - 
                  381*Power(t1,2)*Power(t2,3) + 292*t1*Power(t2,4) + 
                  84*Power(t2,5)) + 
               Power(s2,2)*t1*
                (30*Power(t1,6) - 134*Power(t1,5)*t2 + 
                  133*Power(t1,4)*Power(t2,2) + 
                  304*Power(t1,3)*Power(t2,3) - 
                  493*Power(t1,2)*Power(t2,4) + 101*t1*Power(t2,5) - 
                  3*Power(t2,6)) + 
               s2*Power(t1,2)*
                (-6*Power(t1,6) + 25*Power(t1,5)*t2 - 
                  24*Power(t1,4)*Power(t2,2) - 
                  104*Power(t1,3)*Power(t2,3) + 
                  215*Power(t1,2)*Power(t2,4) - 97*t1*Power(t2,5) + 
                  3*Power(t2,6))) + 
            Power(s1,5)*(-3*Power(s2,10) + 
               4*Power(s2,9)*(5*t1 - 4*t2) + 
               Power(s2,8)*(-56*Power(t1,2) + 85*t1*t2 - 
                  16*Power(t2,2)) + 
               Power(s2,7)*(87*Power(t1,3) - 205*Power(t1,2)*t2 + 
                  101*t1*Power(t2,2) + 111*Power(t2,3)) + 
               2*Power(t1,4)*Power(t2,2)*
                (-Power(t1,4) + 4*Power(t1,3)*t2 - 
                  4*Power(t1,2)*Power(t2,2) + Power(t2,4)) + 
               Power(s2,6)*(-88*Power(t1,4) + 343*Power(t1,3)*t2 - 
                  443*Power(t1,2)*Power(t2,2) - 252*t1*Power(t2,3) + 
                  218*Power(t2,4)) + 
               Power(s2,5)*(73*Power(t1,5) - 491*Power(t1,4)*t2 + 
                  1188*Power(t1,3)*Power(t2,2) - 
                  418*Power(t1,2)*Power(t2,3) - 275*t1*Power(t2,4) + 
                  15*Power(t2,5)) + 
               Power(s2,4)*(-60*Power(t1,6) + 555*Power(t1,5)*t2 - 
                  1773*Power(t1,4)*Power(t2,2) + 
                  1765*Power(t1,3)*Power(t2,3) - 
                  482*Power(t1,2)*Power(t2,4) + 156*t1*Power(t2,5) - 
                  91*Power(t2,6)) + 
               Power(s2,3)*(41*Power(t1,7) - 411*Power(t1,6)*t2 + 
                  1447*Power(t1,5)*Power(t2,2) - 
                  1977*Power(t1,4)*Power(t2,3) + 
                  922*Power(t1,3)*Power(t2,4) - 
                  187*Power(t1,2)*Power(t2,5) + 130*t1*Power(t2,6) - 
                  2*Power(t2,7)) + 
               s2*Power(t1,2)*
                (3*Power(t1,7) - 29*Power(t1,6)*t2 + 
                  106*Power(t1,5)*Power(t2,2) - 
                  173*Power(t1,4)*Power(t2,3) + 
                  53*Power(t1,3)*Power(t2,4) + 
                  93*Power(t1,2)*Power(t2,5) - 51*t1*Power(t2,6) + 
                  Power(t2,7)) + 
               Power(s2,2)*t1*
                (-17*Power(t1,7) + 169*Power(t1,6)*t2 - 
                  608*Power(t1,5)*Power(t2,2) + 
                  936*Power(t1,4)*Power(t2,3) - 
                  429*Power(t1,3)*Power(t2,4) - 
                  62*Power(t1,2)*Power(t2,5) - 8*t1*Power(t2,6) + 
                  Power(t2,7))) + 
            Power(s1,2)*s2*Power(t2,2)*
             (-(Power(s2,9)*t1) - 
               2*Power(t1,5)*(t1 - 3*t2)*Power(t1 - t2,3)*t2 + 
               Power(s2,8)*(-11*Power(t1,2) + 5*t1*t2 + 
                  19*Power(t2,2)) + 
               s2*Power(t1,3)*Power(t1 - t2,3)*
                (6*Power(t1,3) - 9*Power(t1,2)*t2 - 
                  11*t1*Power(t2,2) - 14*Power(t2,3)) + 
               Power(s2,7)*(94*Power(t1,3) - 116*Power(t1,2)*t2 - 
                  75*t1*Power(t2,2) + 46*Power(t2,3)) + 
               Power(s2,6)*(-276*Power(t1,4) + 552*Power(t1,3)*t2 - 
                  70*Power(t1,2)*Power(t2,2) - 151*t1*Power(t2,3) + 
                  29*Power(t2,4)) + 
               Power(s2,5)*(425*Power(t1,5) - 1183*Power(t1,4)*t2 + 
                  758*Power(t1,3)*Power(t2,2) - 
                  33*Power(t1,2)*Power(t2,3) - 39*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               Power(s2,4)*(-379*Power(t1,6) + 1355*Power(t1,5)*t2 - 
                  1459*Power(t1,4)*Power(t2,2) + 
                  645*Power(t1,3)*Power(t2,3) - 
                  154*Power(t1,2)*Power(t2,4) + 6*t1*Power(t2,5) + 
                  3*Power(t2,6)) + 
               Power(s2,3)*(196*Power(t1,7) - 850*Power(t1,6)*t2 + 
                  1243*Power(t1,5)*Power(t2,2) - 
                  879*Power(t1,4)*Power(t2,3) + 
                  368*Power(t1,3)*Power(t2,4) - 
                  80*Power(t1,2)*Power(t2,5) + 7*t1*Power(t2,6) - 
                  5*Power(t2,7)) + 
               Power(s2,2)*t1*
                (-54*Power(t1,7) + 266*Power(t1,6)*t2 - 
                  462*Power(t1,5)*Power(t2,2) + 
                  410*Power(t1,4)*Power(t2,3) - 
                  242*Power(t1,3)*Power(t2,4) + 
                  103*Power(t1,2)*Power(t2,5) - 26*t1*Power(t2,6) + 
                  5*Power(t2,7))) + 
            Power(s1,3)*s2*t2*
             (Power(s2,9)*(t1 - 2*t2) + 
               Power(s2,8)*(Power(t1,2) + t1*t2 - 34*Power(t2,2)) + 
               Power(s2,7)*(-39*Power(t1,3) + 97*Power(t1,2)*t2 + 
                  119*t1*Power(t2,2) - 76*Power(t2,3)) + 
               Power(t1,3)*Power(t1 - t2,2)*t2*
                (3*Power(t1,4) - 20*Power(t1,3)*t2 + 
                  38*Power(t1,2)*Power(t2,2) - 13*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,6)*(157*Power(t1,4) - 486*Power(t1,3)*t2 + 
                  22*Power(t1,2)*Power(t2,2) + 211*t1*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s2,5)*(305*Power(t1,5) - 1076*Power(t1,4)*t2 + 
                  685*Power(t1,3)*Power(t2,2) + 
                  9*Power(t1,2)*Power(t2,3) + 157*t1*Power(t2,4) - 
                  87*Power(t2,5)) + 
               Power(s2,4)*(339*Power(t1,6) - 1304*Power(t1,5)*t2 + 
                  1294*Power(t1,4)*Power(t2,2) - 
                  446*Power(t1,3)*Power(t2,3) + 
                  368*Power(t1,2)*Power(t2,4) - 266*t1*Power(t2,5) + 
                  55*Power(t2,6)) + 
               s2*Power(t1,2)*
                (-12*Power(t1,7) + 40*Power(t1,6)*t2 + 
                  27*Power(t1,5)*Power(t2,2) - 
                  277*Power(t1,4)*Power(t2,3) + 
                  399*Power(t1,3)*Power(t2,4) - 
                  214*Power(t1,2)*Power(t2,5) + 50*t1*Power(t2,6) - 
                  13*Power(t2,7)) + 
               Power(s2,2)*t1*
                (79*Power(t1,7) - 318*Power(t1,6)*t2 + 
                  320*Power(t1,5)*Power(t2,2) + 
                  176*Power(t1,4)*Power(t2,3) - 
                  414*Power(t1,3)*Power(t2,4) + 
                  158*Power(t1,2)*Power(t2,5) + 4*t1*Power(t2,6) - 
                  5*Power(t2,7)) + 
               Power(s2,3)*(-221*Power(t1,7) + 893*Power(t1,6)*t2 - 
                  1037*Power(t1,5)*Power(t2,2) + 
                  340*Power(t1,4)*Power(t2,3) - 
                  89*Power(t1,3)*Power(t2,4) + 
                  164*Power(t1,2)*Power(t2,5) - 83*t1*Power(t2,6) + 
                  14*Power(t2,7))) + 
            Power(s1,4)*(2*Power(t1,5)*Power(t1 - t2,3)*Power(t2,3) + 
               Power(s2,10)*(-3*t1 + 4*t2) + 
               Power(s2,9)*(21*Power(t1,2) - 37*t1*t2 + 
                  30*Power(t2,2)) + 
               Power(s2,8)*(-63*Power(t1,3) + 128*Power(t1,2)*t2 - 
                  168*t1*Power(t2,2) + 52*Power(t2,3)) + 
               Power(s2,7)*(105*Power(t1,4) - 217*Power(t1,3)*t2 + 
                  429*Power(t1,2)*Power(t2,2) - 249*t1*Power(t2,3) - 
                  84*Power(t2,4)) + 
               Power(s2,6)*(-105*Power(t1,5) + 186*Power(t1,4)*t2 - 
                  698*Power(t1,3)*Power(t2,2) + 
                  742*Power(t1,2)*Power(t2,3) + 218*t1*Power(t2,4) - 
                  213*Power(t2,5)) + 
               Power(s2,5)*(63*Power(t1,6) - 69*Power(t1,5)*t2 + 
                  854*Power(t1,4)*Power(t2,2) - 
                  1648*Power(t1,3)*Power(t2,3) + 
                  458*Power(t1,2)*Power(t2,4) + 328*t1*Power(t2,5) - 
                  86*Power(t2,6)) + 
               s2*Power(t1,3)*t2*
                (-6*Power(t1,6) + 41*Power(t1,5)*t2 - 
                  144*Power(t1,4)*Power(t2,2) + 
                  231*Power(t1,3)*Power(t2,3) - 
                  148*Power(t1,2)*Power(t2,4) + 20*t1*Power(t2,5) + 
                  6*Power(t2,6)) + 
               Power(s2,2)*t1*t2*
                (22*Power(t1,7) - 227*Power(t1,6)*t2 + 
                  808*Power(t1,5)*Power(t2,2) - 
                  1196*Power(t1,4)*Power(t2,3) + 
                  730*Power(t1,3)*Power(t2,4) - 
                  180*Power(t1,2)*Power(t2,5) + 44*t1*Power(t2,6) - 
                  Power(t2,7)) + 
               Power(s2,4)*(-21*Power(t1,7) + 12*Power(t1,6)*t2 - 
                  815*Power(t1,5)*Power(t2,2) + 
                  2332*Power(t1,4)*Power(t2,3) - 
                  1993*Power(t1,3)*Power(t2,4) + 
                  478*Power(t1,2)*Power(t2,5) - 11*t1*Power(t2,6) + 
                  18*Power(t2,7)) + 
               Power(s2,3)*(3*Power(t1,8) - 23*Power(t1,7)*t2 + 
                  554*Power(t1,6)*Power(t2,2) - 
                  1895*Power(t1,5)*Power(t2,3) + 
                  2372*Power(t1,4)*Power(t2,4) - 
                  1177*Power(t1,3)*Power(t2,5) + 
                  242*Power(t1,2)*Power(t2,6) - 58*t1*Power(t2,7) + 
                  Power(t2,8)))) + 
         Power(s,4)*(Power(s1,8)*
             (-6*Power(s2,4)*t1 + 2*Power(t1,3)*(t1 - t2)*t2 + 
               Power(s2,3)*(22*Power(t1,2) - 28*t1*t2 + Power(t2,2)) + 
               5*s2*t1*(2*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  t1*Power(t2,2) - Power(t2,3)) + 
               Power(s2,2)*(-26*Power(t1,3) + 46*Power(t1,2)*t2 - 
                  16*t1*Power(t2,2) + Power(t2,3))) + 
            2*s2*Power(t2,4)*Power(s2 - t1 + t2,2)*
             (Power(s2,5)*(t1 - t2) - Power(t1,2)*Power(t1 - t2,3)*t2 + 
               s2*t1*Power(t1 - t2,2)*
                (3*Power(t1,2) - 5*t1*t2 - 8*Power(t2,2)) + 
               Power(s2,4)*(4*Power(t1,2) - 19*t1*t2 + 
                  15*Power(t2,2)) + 
               Power(s2,3)*(-8*Power(t1,3) + 8*Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) - 30*Power(t2,3)) + 
               2*Power(s2,2)*t2*
                (12*Power(t1,3) - 24*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s1,7)*(-(Power(s2,5)*(2*t1 + 29*t2)) + 
               Power(s2,4)*(10*Power(t1,2) + 80*t1*t2 + 
                  63*Power(t2,2)) - 
               2*Power(t1,2)*t2*
                (4*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,3)*(-12*Power(t1,3) - 171*Power(t1,2)*t2 + 
                  81*t1*Power(t2,2) + 49*Power(t2,3)) + 
               Power(s2,2)*(2*Power(t1,4) + 177*Power(t1,3)*t2 - 
                  294*Power(t1,2)*Power(t2,2) + 82*t1*Power(t2,3) - 
                  45*Power(t2,4)) + 
               s2*(2*Power(t1,5) - 49*Power(t1,4)*t2 + 
                  134*Power(t1,3)*Power(t2,2) - 
                  59*Power(t1,2)*Power(t2,3) + 44*t1*Power(t2,4) - 
                  2*Power(t2,5))) + 
            Power(s1,6)*(Power(s2,6)*(16*t1 + 29*t2) + 
               Power(s2,5)*(-94*Power(t1,2) - 65*t1*t2 + 
                  166*Power(t2,2)) + 
               Power(s2,4)*(228*Power(t1,3) - 133*Power(t1,2)*t2 - 
                  173*t1*Power(t2,2) - 381*Power(t2,3)) + 
               Power(s2,3)*(-280*Power(t1,4) + 349*Power(t1,3)*t2 + 
                  232*Power(t1,2)*Power(t2,2) + 269*t1*Power(t2,3) - 
                  329*Power(t2,4)) - 
               2*t1*t2*(7*Power(t1,5) - 30*Power(t1,4)*t2 + 
                  41*Power(t1,2)*Power(t2,3) - 19*t1*Power(t2,4) + 
                  Power(t2,5)) + 
               Power(s2,2)*(172*Power(t1,5) - 223*Power(t1,4)*t2 - 
                  329*Power(t1,3)*Power(t2,2) + 
                  554*Power(t1,2)*Power(t2,3) - 152*t1*Power(t2,4) + 
                  193*Power(t2,5)) + 
               s2*(-42*Power(t1,6) + 57*Power(t1,5)*t2 + 
                  20*Power(t1,4)*Power(t2,2) - 
                  163*Power(t1,3)*Power(t2,3) + 
                  187*Power(t1,2)*Power(t2,4) - 195*t1*Power(t2,5) + 
                  4*Power(t2,6))) - 
            s1*t2*(2*Power(t1,3)*Power(t1 - t2,5)*Power(t2,3) + 
               Power(s2,9)*(Power(t1,2) - 6*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,8)*(Power(t1,3) + 23*Power(t1,2)*t2 - 
                  17*t1*Power(t2,2) - 17*Power(t2,3)) - 
               2*s2*t1*Power(t1 - t2,4)*t2*
                (3*Power(t1,4) + 20*Power(t1,3)*t2 + 
                  31*Power(t1,2)*Power(t2,2) - 4*Power(t2,4)) + 
               Power(s2,7)*(-30*Power(t1,4) - 45*Power(t1,3)*t2 + 
                  35*Power(t1,2)*Power(t2,2) + 54*t1*Power(t2,3) + 
                  59*Power(t2,4)) + 
               Power(s2,6)*(100*Power(t1,5) + 89*Power(t1,4)*t2 - 
                  210*Power(t1,3)*Power(t2,2) + 
                  27*Power(t1,2)*Power(t2,3) - 145*t1*Power(t2,4) + 
                  3*Power(t2,5)) + 
               Power(s2,5)*(-155*Power(t1,6) - 162*Power(t1,5)*t2 + 
                  641*Power(t1,4)*Power(t2,2) - 
                  372*Power(t1,3)*Power(t2,3) + 
                  68*Power(t1,2)*Power(t2,4) + 310*t1*Power(t2,5) - 
                  260*Power(t2,6)) - 
               Power(s2,3)*Power(t1 - t2,2)*
                (56*Power(t1,6) + 237*Power(t1,5)*t2 - 
                  69*Power(t1,4)*Power(t2,2) - 
                  243*Power(t1,3)*Power(t2,3) + 
                  154*Power(t1,2)*Power(t2,4) + 42*t1*Power(t2,5) - 
                  132*Power(t2,6)) + 
               Power(s2,2)*Power(t1 - t2,3)*
                (10*Power(t1,6) + 73*Power(t1,5)*t2 + 
                  111*Power(t1,4)*Power(t2,2) - 
                  19*Power(t1,3)*Power(t2,3) - 
                  78*Power(t1,2)*Power(t2,4) + 90*t1*Power(t2,5) + 
                  20*Power(t2,6)) + 
               Power(s2,4)*(129*Power(t1,7) + 189*Power(t1,6)*t2 - 
                  847*Power(t1,5)*Power(t2,2) + 
                  519*Power(t1,4)*Power(t2,3) + 
                  328*Power(t1,3)*Power(t2,4) - 
                  764*Power(t1,2)*Power(t2,5) + 476*t1*Power(t2,6) - 
                  30*Power(t2,7))) + 
            Power(s1,5)*(Power(s2,7)*(-2*t1 + 23*t2) + 
               2*Power(s2,6)*
                (8*Power(t1,2) - 64*t1*t2 - 119*Power(t2,2)) + 
               Power(s2,5)*(-74*Power(t1,3) + 474*Power(t1,2)*t2 + 
                  738*t1*Power(t2,2) - 467*Power(t2,3)) + 
               Power(s2,4)*(182*Power(t1,4) - 1134*Power(t1,3)*t2 + 
                  150*Power(t1,2)*Power(t2,2) - 30*t1*Power(t2,3) + 
                  867*Power(t2,4)) + 
               Power(s2,3)*(-234*Power(t1,5) + 1413*Power(t1,4)*t2 - 
                  1586*Power(t1,3)*Power(t2,2) + 
                  829*Power(t1,2)*Power(t2,3) - 1569*t1*Power(t2,4) + 
                  781*Power(t2,5)) + 
               Power(s2,2)*(150*Power(t1,6) - 818*Power(t1,5)*t2 + 
                  1207*Power(t1,4)*Power(t2,2) - 
                  342*Power(t1,3)*Power(t2,3) + 
                  84*Power(t1,2)*Power(t2,4) - 25*t1*Power(t2,5) - 
                  298*Power(t2,6)) + 
               2*t1*t2*(2*Power(t1,6) + 40*Power(t1,5)*t2 - 
                  116*Power(t1,4)*Power(t2,2) + 
                  71*Power(t1,3)*Power(t2,3) + 
                  29*Power(t1,2)*Power(t2,4) - 29*t1*Power(t2,5) + 
                  3*Power(t2,6)) - 
               s2*(38*Power(t1,7) - 166*Power(t1,6)*t2 + 
                  357*Power(t1,5)*Power(t2,2) - 
                  471*Power(t1,4)*Power(t2,3) + 
                  400*Power(t1,3)*Power(t2,4) + 
                  171*Power(t1,2)*Power(t2,5) - 405*t1*Power(t2,6) + 
                  6*Power(t2,7))) + 
            Power(s1,4)*(-3*Power(s2,8)*(2*t1 + 9*t2) + 
               2*Power(s2,7)*
                (24*Power(t1,2) + 59*t1*t2 - 28*Power(t2,2)) + 
               Power(s2,6)*(-170*Power(t1,3) - 159*Power(t1,2)*t2 + 
                  59*t1*Power(t2,2) + 708*Power(t2,3)) + 
               Power(s2,5)*(330*Power(t1,4) + 83*Power(t1,3)*t2 + 
                  55*Power(t1,2)*Power(t2,2) - 2447*t1*Power(t2,3) + 
                  705*Power(t2,4)) + 
               Power(s2,4)*(-368*Power(t1,5) - 173*Power(t1,4)*t2 + 
                  725*Power(t1,3)*Power(t2,2) + 
                  936*Power(t1,2)*Power(t2,3) + 651*t1*Power(t2,4) - 
                  980*Power(t2,5)) + 
               2*t1*Power(t1 - t2,2)*t2*
                (4*Power(t1,5) - 3*Power(t1,4)*t2 - 
                  65*Power(t1,3)*Power(t2,2) + 
                  49*Power(t1,2)*Power(t2,3) + 8*t1*Power(t2,4) - 
                  3*Power(t2,5)) + 
               Power(s2,3)*(230*Power(t1,6) + 401*Power(t1,5)*t2 - 
                  1741*Power(t1,4)*Power(t2,2) + 
                  2424*Power(t1,3)*Power(t2,3) - 
                  3359*Power(t1,2)*Power(t2,4) + 2993*t1*Power(t2,5) - 
                  802*Power(t2,6)) + 
               Power(s2,2)*(-72*Power(t1,7) - 331*Power(t1,6)*t2 + 
                  1141*Power(t1,5)*Power(t2,2) - 
                  1791*Power(t1,4)*Power(t2,3) + 
                  2160*Power(t1,3)*Power(t2,4) - 
                  1875*Power(t1,2)*Power(t2,5) + 489*t1*Power(t2,6) + 
                  159*Power(t2,7)) + 
               s2*(8*Power(t1,8) + 80*Power(t1,7)*t2 - 
                  161*Power(t1,6)*Power(t2,2) + 
                  336*Power(t1,5)*Power(t2,3) - 
                  1128*Power(t1,4)*Power(t2,4) + 
                  1443*Power(t1,3)*Power(t2,5) - 
                  174*Power(t1,2)*Power(t2,6) - 417*t1*Power(t2,7) + 
                  13*Power(t2,8))) + 
            Power(s1,3)*(4*Power(s2,9)*t2 + 
               Power(s2,8)*(-2*Power(t1,2) - 40*t1*t2 + 
                  123*Power(t2,2)) + 
               Power(s2,7)*(24*Power(t1,3) + 25*Power(t1,2)*t2 - 
                  606*t1*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,6)*(-100*Power(t1,4) + 400*Power(t1,3)*t2 + 
                  997*Power(t1,2)*Power(t2,2) + 551*t1*Power(t2,3) - 
                  1109*Power(t2,4)) - 
               2*t1*Power(t1 - t2,3)*Power(t2,2)*
                (15*Power(t1,4) - 31*Power(t1,2)*Power(t2,2) + 
                  6*t1*Power(t2,3) + Power(t2,4)) + 
               Power(s2,5)*(212*Power(t1,5) - 1221*Power(t1,4)*t2 - 
                  314*Power(t1,3)*Power(t2,2) - 
                  2146*Power(t1,2)*Power(t2,3) + 4266*t1*Power(t2,4) - 
                  636*Power(t2,5)) + 
               Power(s2,4)*(-258*Power(t1,6) + 1547*Power(t1,5)*t2 - 
                  732*Power(t1,4)*Power(t2,2) + 
                  2186*Power(t1,3)*Power(t2,3) - 
                  3776*Power(t1,2)*Power(t2,4) - 279*t1*Power(t2,5) + 
                  590*Power(t2,6)) + 
               s2*Power(t1 - t2,2)*
                (12*Power(t1,7) + 6*Power(t1,6)*t2 + 
                  32*Power(t1,5)*Power(t2,2) - 
                  302*Power(t1,4)*Power(t2,3) - 
                  134*Power(t1,3)*Power(t2,4) + 
                  796*Power(t1,2)*Power(t2,5) + 159*t1*Power(t2,6) - 
                  14*Power(t2,7)) + 
               Power(s2,3)*(184*Power(t1,7) - 962*Power(t1,6)*t2 + 
                  682*Power(t1,5)*Power(t2,2) - 
                  426*Power(t1,4)*Power(t2,3) - 
                  274*Power(t1,3)*Power(t2,4) + 
                  3171*Power(t1,2)*Power(t2,5) - 2472*t1*Power(t2,6) + 
                  297*Power(t2,7)) + 
               Power(s2,2)*(-72*Power(t1,8) + 265*Power(t1,7)*t2 - 
                  152*Power(t1,6)*Power(t2,2) + 
                  98*Power(t1,5)*Power(t2,3) + 
                  296*Power(t1,4)*Power(t2,4) - 
                  2149*Power(t1,3)*Power(t2,5) + 
                  2390*Power(t1,2)*Power(t2,6) - 730*t1*Power(t2,7) + 
                  54*Power(t2,8))) + 
            Power(s1,2)*(Power(s2,9)*t2*(3*t1 + t2) + 
               2*Power(t1,2)*Power(t1 - t2,4)*Power(t2,3)*
                (10*Power(t1,2) + t1*t2 - 3*Power(t2,2)) + 
               Power(s2,8)*(4*Power(t1,3) - 24*Power(t1,2)*t2 + 
                  79*t1*Power(t2,2) - 111*Power(t2,3)) + 
               Power(s2,7)*(-26*Power(t1,4) + 38*Power(t1,3)*t2 - 
                  299*Power(t1,2)*Power(t2,2) + 569*t1*Power(t2,3) + 
                  55*Power(t2,4)) + 
               Power(s2,6)*(70*Power(t1,5) + 78*Power(t1,4)*t2 + 
                  144*Power(t1,3)*Power(t2,2) - 
                  930*Power(t1,2)*Power(t2,3) - 775*t1*Power(t2,4) + 
                  615*Power(t2,5)) - 
               s2*Power(t1 - t2,3)*t2*
                (18*Power(t1,6) + 59*Power(t1,5)*t2 + 
                  112*Power(t1,4)*Power(t2,2) - 
                  299*Power(t1,3)*Power(t2,3) - 
                  292*Power(t1,2)*Power(t2,4) + 4*t1*Power(t2,5) + 
                  5*Power(t2,6)) + 
               Power(s2,5)*(-100*Power(t1,6) - 335*Power(t1,5)*t2 + 
                  817*Power(t1,4)*Power(t2,2) + 
                  75*Power(t1,3)*Power(t2,3) + 
                  2642*Power(t1,2)*Power(t2,4) - 2567*t1*Power(t2,5) + 
                  82*Power(t2,6)) + 
               Power(s2,4)*(80*Power(t1,7) + 472*Power(t1,6)*t2 - 
                  1526*Power(t1,5)*Power(t2,2) + 
                  1158*Power(t1,4)*Power(t2,3) - 
                  3230*Power(t1,3)*Power(t2,4) + 
                  3123*Power(t1,2)*Power(t2,5) + 4*t1*Power(t2,6) - 
                  161*Power(t2,7)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (6*Power(t1,7) + 134*Power(t1,6)*t2 + 
                  17*Power(t1,5)*Power(t2,2) + 
                  57*Power(t1,4)*Power(t2,3) - 
                  711*Power(t1,3)*Power(t2,4) - 
                  292*Power(t1,2)*Power(t2,5) + 173*t1*Power(t2,6) - 
                  84*Power(t2,7)) + 
               Power(s2,3)*(-34*Power(t1,8) - 336*Power(t1,7)*t2 + 
                  1034*Power(t1,6)*Power(t2,2) - 
                  949*Power(t1,5)*Power(t2,3) + 
                  1718*Power(t1,4)*Power(t2,4) - 
                  1486*Power(t1,3)*Power(t2,5) - 
                  542*Power(t1,2)*Power(t2,6) + 479*t1*Power(t2,7) + 
                  116*Power(t2,8)))) - 
         Power(s,2)*(-2*Power(s2,3)*(s2 - t1)*(t1 - t2)*Power(t2,6)*
             Power(s2 - t1 + t2,2)*
             (3*Power(s2,2) + t1*(-2*t1 + 3*t2) - s2*(t1 + 5*t2)) + 
            Power(s1,9)*s2*(s2 - t1)*
             (2*Power(s2,3)*(t1 - 2*t2) + 
               Power(s2,2)*(-6*Power(t1,2) + 9*t1*t2 - 4*Power(t2,2)) + 
               2*s2*t1*(3*Power(t1,2) - 3*t1*t2 - 2*Power(t2,2)) + 
               t1*(-2*Power(t1,3) + Power(t1,2)*t2 + 8*t1*Power(t2,2) - 
                  4*Power(t2,3))) + 
            Power(s1,8)*(3*Power(s2,6)*t2 + 
               4*Power(s2,5)*(Power(t1,2) - 7*t1*t2 - 5*Power(t2,2)) + 
               2*Power(t1,3)*Power(t2,2)*
                (-2*Power(t1,2) + t1*t2 + Power(t2,2)) + 
               Power(s2,4)*(-16*Power(t1,3) + 91*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) - 23*Power(t2,3)) + 
               Power(s2,3)*(24*Power(t1,4) - 135*Power(t1,3)*t2 + 
                  64*Power(t1,2)*Power(t2,2) + 75*t1*Power(t2,3) - 
                  Power(t2,4)) - 
               Power(s2,2)*(16*Power(t1,5) - 94*Power(t1,4)*t2 + 
                  82*Power(t1,3)*Power(t2,2) + 
                  93*Power(t1,2)*Power(t2,3) - 36*t1*Power(t2,4) + 
                  Power(t2,5)) + 
               s2*t1*(4*Power(t1,5) - 25*Power(t1,4)*t2 + 
                  32*Power(t1,3)*Power(t2,2) + 
                  39*Power(t1,2)*Power(t2,3) - 25*t1*Power(t2,4) + 
                  Power(t2,5))) + 
            Power(s1,7)*(Power(s2,7)*(-4*t1 + 6*t2) + 
               Power(s2,6)*(22*Power(t1,2) - 28*t1*t2 + 
                  9*Power(t2,2)) - 
               8*Power(s2,5)*
                (6*Power(t1,3) - 4*Power(t1,2)*t2 - 23*Power(t2,3)) - 
               2*Power(t1,3)*Power(t2,2)*
                (2*Power(t1,3) - 12*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,4)*(52*Power(t1,4) + 55*Power(t1,3)*t2 - 
                  237*Power(t1,2)*Power(t2,2) - 295*t1*Power(t2,3) + 
                  140*Power(t2,4)) - 
               s2*t1*t2*(43*Power(t1,5) - 158*Power(t1,4)*t2 + 
                  202*Power(t1,3)*Power(t2,2) + 
                  26*Power(t1,2)*Power(t2,3) - 49*t1*Power(t2,4) + 
                  2*Power(t2,5)) - 
               Power(s2,3)*(28*Power(t1,5) + 167*Power(t1,4)*t2 - 
                  588*Power(t1,3)*Power(t2,2) + 
                  135*Power(t1,2)*Power(t2,3) + 259*t1*Power(t2,4) + 
                  40*Power(t2,5)) + 
               Power(s2,2)*(6*Power(t1,6) + 145*Power(t1,5)*t2 - 
                  514*Power(t1,4)*Power(t2,2) + 
                  424*Power(t1,3)*Power(t2,3) + 
                  197*Power(t1,2)*Power(t2,4) - 49*t1*Power(t2,5) + 
                  Power(t2,6))) + 
            s1*s2*(s2 - t1)*Power(t2,3)*
             (2*Power(s2,8)*(t1 - t2) + 
               2*Power(t1,2)*Power(t1 - t2,4)*t2*
                (Power(t1,2) + Power(t2,2)) + 
               Power(s2,7)*(-19*Power(t1,2) + 22*t1*t2 + Power(t2,2)) + 
               Power(s2,6)*(82*Power(t1,3) - 109*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) - 13*Power(t2,3)) + 
               Power(s2,5)*(-188*Power(t1,4) + 308*Power(t1,3)*t2 - 
                  81*Power(t1,2)*Power(t2,2) + 7*t1*Power(t2,3) + 
                  Power(t2,4)) - 
               s2*t1*Power(t1 - t2,3)*
                (10*Power(t1,4) + 9*Power(t1,3)*t2 - 
                  37*Power(t1,2)*Power(t2,2) - 2*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (66*Power(t1,5) - 36*Power(t1,4)*t2 - 
                  123*Power(t1,3)*Power(t2,2) + 
                  73*Power(t1,2)*Power(t2,3) - 18*t1*Power(t2,4) - 
                  20*Power(t2,5)) + 
               Power(s2,4)*(242*Power(t1,5) - 487*Power(t1,4)*t2 + 
                  181*Power(t1,3)*Power(t2,2) + 
                  90*Power(t1,2)*Power(t2,3) - 104*t1*Power(t2,4) + 
                  59*Power(t2,5)) + 
               Power(s2,3)*(-175*Power(t1,6) + 413*Power(t1,5)*t2 - 
                  154*Power(t1,4)*Power(t2,2) - 
                  255*Power(t1,3)*Power(t2,3) + 
                  282*Power(t1,2)*Power(t2,4) - 133*t1*Power(t2,5) + 
                  22*Power(t2,6))) + 
            Power(s1,6)*(-4*Power(s2,8)*t2 + 
               Power(s2,7)*(-6*Power(t1,2) + 45*t1*t2 + 
                  46*Power(t2,2)) + 
               Power(s2,6)*(34*Power(t1,3) - 195*Power(t1,2)*t2 - 
                  138*t1*Power(t2,2) + 72*Power(t2,3)) + 
               Power(s2,5)*(-80*Power(t1,4) + 420*Power(t1,3)*t2 + 
                  117*Power(t1,2)*Power(t2,2) - 170*t1*Power(t2,3) - 
                  405*Power(t2,4)) + 
               2*Power(t1,3)*Power(t2,2)*
                (2*Power(t1,4) + 7*Power(t1,3)*t2 - 
                  30*Power(t1,2)*Power(t2,2) + 24*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,4)*(100*Power(t1,5) - 479*Power(t1,4)*t2 - 
                  136*Power(t1,3)*Power(t2,2) + 
                  685*Power(t1,2)*Power(t2,3) + 579*t1*Power(t2,4) - 
                  294*Power(t2,5)) - 
               s2*Power(t1,2)*
                (4*Power(t1,6) + Power(t1,5)*t2 - 
                  115*Power(t1,4)*Power(t2,2) + 
                  441*Power(t1,3)*Power(t2,3) - 
                  538*Power(t1,2)*Power(t2,4) + 142*t1*Power(t2,5) + 
                  3*Power(t2,6)) + 
               Power(s2,3)*(-70*Power(t1,6) + 276*Power(t1,5)*t2 + 
                  366*Power(t1,4)*Power(t2,2) - 
                  1571*Power(t1,3)*Power(t2,3) + 
                  577*Power(t1,2)*Power(t2,4) + 234*t1*Power(t2,5) + 
                  136*Power(t2,6)) + 
               Power(s2,2)*(26*Power(t1,7) - 62*Power(t1,6)*t2 - 
                  374*Power(t1,5)*Power(t2,2) + 
                  1411*Power(t1,4)*Power(t2,3) - 
                  1197*Power(t1,3)*Power(t2,4) + 
                  23*Power(t1,2)*Power(t2,5) - 80*t1*Power(t2,6) + 
                  3*Power(t2,7))) + 
            Power(s1,2)*s2*Power(t2,2)*
             (Power(s2,9)*(t1 + t2) + 
               Power(s2,8)*(11*Power(t1,2) + 7*t1*t2 - 
                  44*Power(t2,2)) + 
               Power(t1,3)*Power(t1 - t2,3)*t2*
                (8*Power(t1,3) - 5*Power(t1,2)*t2 - 9*t1*Power(t2,2) - 
                  34*Power(t2,3)) + 
               Power(s2,7)*(-112*Power(t1,3) + 24*Power(t1,2)*t2 + 
                  250*t1*Power(t2,2) - 20*Power(t2,3)) + 
               Power(s2,6)*(382*Power(t1,4) - 364*Power(t1,3)*t2 - 
                  522*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
                  164*Power(t2,4)) + 
               Power(s2,5)*(-685*Power(t1,5) + 1117*Power(t1,4)*t2 + 
                  385*Power(t1,3)*Power(t2,2) + 
                  116*Power(t1,2)*Power(t2,3) - 727*t1*Power(t2,4) + 
                  132*Power(t2,5)) - 
               s2*t1*Power(t1 - t2,2)*
                (22*Power(t1,6) - 103*Power(t1,4)*Power(t2,2) + 
                  74*Power(t1,3)*Power(t2,3) - 
                  97*Power(t1,2)*Power(t2,4) + 86*t1*Power(t2,5) - 
                  10*Power(t2,6)) + 
               Power(s2,4)*(719*Power(t1,6) - 1601*Power(t1,5)*t2 + 
                  143*Power(t1,4)*Power(t2,2) + 
                  32*Power(t1,3)*Power(t2,3) + 
                  848*Power(t1,2)*Power(t2,4) - 308*t1*Power(t2,5) + 
                  44*Power(t2,6)) + 
               Power(s2,3)*(-446*Power(t1,7) + 1198*Power(t1,6)*t2 - 
                  316*Power(t1,5)*Power(t2,2) - 
                  552*Power(t1,4)*Power(t2,3) + 
                  140*Power(t1,3)*Power(t2,4) - 
                  61*Power(t1,2)*Power(t2,5) + 3*t1*Power(t2,6) + 
                  36*Power(t2,7)) + 
               Power(s2,2)*(152*Power(t1,8) - 434*Power(t1,7)*t2 + 
                  52*Power(t1,6)*Power(t2,2) + 
                  669*Power(t1,5)*Power(t2,3) - 
                  743*Power(t1,4)*Power(t2,4) + 
                  497*Power(t1,3)*Power(t2,5) - 
                  211*Power(t1,2)*Power(t2,6) + 33*t1*Power(t2,7) - 
                  15*Power(t2,8))) + 
            Power(s1,5)*(2*Power(s2,9)*(t1 - t2) + 
               Power(s2,8)*(-14*Power(t1,2) + 10*t1*t2 - 
                  11*Power(t2,2)) + 
               Power(s2,7)*(40*Power(t1,3) - 2*Power(t1,2)*t2 + 
                  22*t1*Power(t2,2) - 263*Power(t2,3)) + 
               Power(s2,6)*(-58*Power(t1,4) - 113*Power(t1,3)*t2 + 
                  234*Power(t1,2)*Power(t2,2) + 976*t1*Power(t2,3) - 
                  333*Power(t2,4)) + 
               2*Power(t1,3)*Power(t2,2)*
                (2*Power(t1,5) - 10*Power(t1,4)*t2 + 
                  2*Power(t1,3)*Power(t2,2) + 
                  24*Power(t1,2)*Power(t2,3) - 23*t1*Power(t2,4) + 
                  5*Power(t2,5)) + 
               Power(s2,5)*(40*Power(t1,5) + 374*Power(t1,4)*t2 - 
                  1037*Power(t1,3)*Power(t2,2) - 
                  1009*Power(t1,2)*Power(t2,3) + 735*t1*Power(t2,4) + 
                  455*Power(t2,5)) + 
               Power(s2,4)*(-2*Power(t1,6) - 564*Power(t1,5)*t2 + 
                  1701*Power(t1,4)*Power(t2,2) + 
                  13*Power(t1,3)*Power(t2,3) - 
                  1142*Power(t1,2)*Power(t2,4) - 626*t1*Power(t2,5) + 
                  403*Power(t2,6)) - 
               Power(s2,3)*(16*Power(t1,7) - 454*Power(t1,6)*t2 + 
                  1331*Power(t1,5)*Power(t2,2) - 
                  340*Power(t1,4)*Power(t2,3) - 
                  1920*Power(t1,3)*Power(t2,4) + 
                  965*Power(t1,2)*Power(t2,5) + 87*t1*Power(t2,6) + 
                  136*Power(t2,7)) + 
               Power(s2,2)*(10*Power(t1,8) - 189*Power(t1,7)*t2 + 
                  486*Power(t1,6)*Power(t2,2) - 
                  6*Power(t1,5)*Power(t2,3) - 
                  1633*Power(t1,4)*Power(t2,4) + 
                  1603*Power(t1,3)*Power(t2,5) - 
                  390*Power(t1,2)*Power(t2,6) + 207*t1*Power(t2,7) - 
                  5*Power(t2,8)) + 
               s2*t1*(-2*Power(t1,8) + 32*Power(t1,7)*t2 - 
                  68*Power(t1,6)*Power(t2,2) - 
                  31*Power(t1,5)*Power(t2,3) + 
                  459*Power(t1,4)*Power(t2,4) - 
                  638*Power(t1,3)*Power(t2,5) + 
                  313*Power(t1,2)*Power(t2,6) - 86*t1*Power(t2,7) + 
                  2*Power(t2,8))) + 
            Power(s1,3)*t2*(Power(s2,10)*t1 + 
               2*Power(t1,4)*(3*t1 - 2*t2)*Power(t1 - t2,3)*
                Power(t2,3) + 
               Power(s2,9)*(-9*Power(t1,2) - 2*t1*t2 + 
                  65*Power(t2,2)) + 
               Power(s2,8)*(46*Power(t1,3) - 30*Power(t1,2)*t2 - 
                  378*t1*Power(t2,2) + 54*Power(t2,3)) + 
               Power(s2,7)*(-143*Power(t1,4) + 215*Power(t1,3)*t2 + 
                  975*Power(t1,2)*Power(t2,2) - 219*t1*Power(t2,3) - 
                  364*Power(t2,4)) + 
               Power(s2,6)*(270*Power(t1,5) - 536*Power(t1,4)*t2 - 
                  1568*Power(t1,3)*Power(t2,2) + 
                  733*Power(t1,2)*Power(t2,3) + 1315*t1*Power(t2,4) - 
                  373*Power(t2,5)) - 
               s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                (16*Power(t1,5) - 43*Power(t1,4)*t2 + 
                  68*Power(t1,3)*Power(t2,2) - 
                  53*Power(t1,2)*Power(t2,3) + 69*t1*Power(t2,4) + 
                  25*Power(t2,5)) + 
               Power(s2,5)*(-311*Power(t1,6) + 648*Power(t1,5)*t2 + 
                  1902*Power(t1,4)*Power(t2,2) - 
                  2196*Power(t1,3)*Power(t2,3) - 
                  676*Power(t1,2)*Power(t2,4) + 380*t1*Power(t2,5) + 
                  61*Power(t2,6)) + 
               Power(s2,4)*(214*Power(t1,7) - 370*Power(t1,6)*t2 - 
                  1818*Power(t1,5)*Power(t2,2) + 
                  3688*Power(t1,4)*Power(t2,3) - 
                  2232*Power(t1,3)*Power(t2,4) + 
                  1198*Power(t1,2)*Power(t2,5) - 656*t1*Power(t2,6) + 
                  123*Power(t2,7)) + 
               Power(s2,2)*t1*
                (13*Power(t1,8) + 48*Power(t1,7)*t2 - 
                  468*Power(t1,6)*Power(t2,2) + 
                  1211*Power(t1,5)*Power(t2,3) - 
                  1480*Power(t1,4)*Power(t2,4) + 
                  1105*Power(t1,3)*Power(t2,5) - 
                  644*Power(t1,2)*Power(t2,6) + 211*t1*Power(t2,7) + 
                  4*Power(t2,8)) + 
               Power(s2,3)*(-81*Power(t1,8) + 43*Power(t1,7)*t2 + 
                  1215*Power(t1,6)*Power(t2,2) - 
                  3107*Power(t1,5)*Power(t2,3) + 
                  3227*Power(t1,4)*Power(t2,4) - 
                  2059*Power(t1,3)*Power(t2,5) + 
                  992*Power(t1,2)*Power(t2,6) - 280*t1*Power(t2,7) + 
                  42*Power(t2,8))) + 
            Power(s1,4)*(Power(s2,10)*t2 + 
               Power(s2,9)*(2*Power(t1,2) - 13*t1*t2 - 20*Power(t2,2)) + 
               Power(s2,8)*(-14*Power(t1,3) + 74*Power(t1,2)*t2 + 
                  99*t1*Power(t2,2) - 12*Power(t2,3)) - 
               2*Power(t1,3)*Power(t1 - t2,2)*Power(t2,3)*
                (6*Power(t1,3) - 9*Power(t1,2)*t2 - t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,7)*(42*Power(t1,4) - 224*Power(t1,3)*t2 - 
                  266*Power(t1,2)*Power(t2,2) + 37*t1*Power(t2,3) + 
                  408*Power(t2,4)) + 
               Power(s2,6)*(-70*Power(t1,5) + 381*Power(t1,4)*t2 + 
                  651*Power(t1,3)*Power(t2,2) - 
                  459*Power(t1,2)*Power(t2,3) - 1465*t1*Power(t2,4) + 
                  445*Power(t2,5)) + 
               Power(s2,5)*(70*Power(t1,6) - 351*Power(t1,5)*t2 - 
                  1306*Power(t1,4)*Power(t2,2) + 
                  2049*Power(t1,3)*Power(t2,3) + 
                  971*Power(t1,2)*Power(t2,4) - 547*t1*Power(t2,5) - 
                  327*Power(t2,6)) + 
               Power(s2,4)*(-42*Power(t1,7) + 128*Power(t1,6)*t2 + 
                  1688*Power(t1,5)*Power(t2,2) - 
                  3739*Power(t1,4)*Power(t2,3) + 
                  1735*Power(t1,3)*Power(t2,4) - 
                  437*Power(t1,2)*Power(t2,5) + 944*t1*Power(t2,6) - 
                  361*Power(t2,7)) + 
               s2*t1*t2*(14*Power(t1,8) - 81*Power(t1,7)*t2 + 
                  220*Power(t1,6)*Power(t2,2) - 
                  248*Power(t1,5)*Power(t2,3) - 
                  3*Power(t1,4)*Power(t2,4) + 
                  202*Power(t1,3)*Power(t2,5) - 
                  189*Power(t1,2)*Power(t2,6) + 86*t1*Power(t2,7) - 
                  Power(t2,8)) + 
               Power(s2,3)*(14*Power(t1,8) + 46*Power(t1,7)*t2 - 
                  1267*Power(t1,6)*Power(t2,2) + 
                  3250*Power(t1,5)*Power(t2,3) - 
                  2722*Power(t1,4)*Power(t2,4) + 
                  531*Power(t1,3)*Power(t2,5) - 
                  229*Power(t1,2)*Power(t2,6) + 260*t1*Power(t2,7) + 
                  14*Power(t2,8)) + 
               Power(s2,2)*(-2*Power(t1,9) - 56*Power(t1,8)*t2 + 
                  502*Power(t1,7)*Power(t2,2) - 
                  1334*Power(t1,6)*Power(t2,3) + 
                  1279*Power(t1,5)*Power(t2,4) + 
                  23*Power(t1,4)*Power(t2,5) - 
                  432*Power(t1,3)*Power(t2,6) + 
                  148*Power(t1,2)*Power(t2,7) - 124*t1*Power(t2,8) + 
                  2*Power(t2,9)))) + 
         Power(s,3)*(2*Power(s1,9)*s2*(s2 - t1)*
             (Power(s2,2)*(t1 - 2*t2) + Power(t1,2)*(t1 - 2*t2) - 
               2*s2*Power(t1 - t2,2)) + 
            2*Power(s2,2)*Power(t2,5)*Power(s2 - t1 + t2,2)*
             (3*Power(s2,4)*(t1 - t2) + 
               Power(t1,2)*(t1 - 3*t2)*Power(t1 - t2,2) - 
               15*Power(s2,3)*(t1 - t2)*t2 - 
               2*Power(s2,2)*
                (4*Power(t1,3) - 12*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
                  5*Power(t2,3)) + 
               s2*t1*(4*Power(t1,3) - Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 12*Power(t2,3))) + 
            Power(s1,8)*(Power(s2,5)*(6*t1 + 11*t2) + 
               2*Power(t1,3)*t2*(Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               Power(s2,4)*(-24*Power(t1,2) - 27*t1*t2 + 
                  8*Power(t2,2)) + 
               Power(s2,3)*(36*Power(t1,3) + 34*Power(t1,2)*t2 - 
                  78*t1*Power(t2,2) - 5*Power(t2,3)) + 
               s2*t1*(6*Power(t1,4) + 9*Power(t1,3)*t2 - 
                  52*Power(t1,2)*Power(t2,2) + 11*t1*Power(t2,3) - 
                  2*Power(t2,4)) - 
               Power(s2,2)*(24*Power(t1,4) + 29*Power(t1,3)*t2 - 
                  120*Power(t1,2)*Power(t2,2) + 26*t1*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,7)*(Power(s2,6)*(-8*t1 + 13*t2) + 
               3*Power(s2,5)*
                (16*Power(t1,2) - 25*t1*t2 - 35*Power(t2,2)) + 
               Power(s2,4)*(-118*Power(t1,3) + 246*Power(t1,2)*t2 + 
                  176*t1*Power(t2,2) + 25*Power(t2,3)) + 
               2*Power(t1,2)*t2*
                (Power(t1,4) - 12*Power(t1,3)*t2 + 
                  4*Power(t1,2)*Power(t2,2) + 9*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,3)*(146*Power(t1,4) - 377*Power(t1,3)*t2 - 
                  84*Power(t1,2)*Power(t2,2) + 154*t1*Power(t2,3) + 
                  133*Power(t2,4)) + 
               Power(s2,2)*(-90*Power(t1,5) + 253*Power(t1,4)*t2 + 
                  9*Power(t1,3)*Power(t2,2) - 
                  407*Power(t1,2)*Power(t2,3) + 28*t1*Power(t2,4) - 
                  11*Power(t2,5)) + 
               s2*(22*Power(t1,6) - 62*Power(t1,5)*t2 + 
                  28*Power(t1,4)*Power(t2,2) + 
                  156*Power(t1,3)*Power(t2,3) - 
                  59*Power(t1,2)*Power(t2,4) + 18*t1*Power(t2,5) - 
                  Power(t2,6))) - 
            s1*s2*Power(t2,2)*
             (Power(s2,9)*(t1 - t2) + 
               Power(s2,8)*(-7*Power(t1,2) + t1*t2 + 8*Power(t2,2)) + 
               Power(s2,7)*(38*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  62*t1*Power(t2,2) - 12*Power(t2,3)) - 
               Power(t1,2)*Power(t1 - t2,4)*t2*
                (5*Power(t1,3) + 15*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - Power(t2,3)) + 
               Power(s2,6)*(-134*Power(t1,4) + 17*Power(t1,3)*t2 + 
                  213*Power(t1,2)*Power(t2,2) - 35*t1*Power(t2,3) + 
                  50*Power(t2,4)) + 
               2*s2*t1*Power(t1 - t2,3)*
                (7*Power(t1,5) + 26*Power(t1,4)*t2 - 
                  8*Power(t1,3)*Power(t2,2) - 
                  25*Power(t1,2)*Power(t2,3) - t1*Power(t2,4) + 
                  12*Power(t2,5)) + 
               Power(s2,5)*(275*Power(t1,5) - 113*Power(t1,4)*t2 - 
                  494*Power(t1,3)*Power(t2,2) + 
                  352*Power(t1,2)*Power(t2,3) - 263*t1*Power(t2,4) + 
                  109*Power(t2,5)) + 
               Power(s2,4)*(-331*Power(t1,6) + 198*Power(t1,5)*t2 + 
                  819*Power(t1,4)*Power(t2,2) - 
                  980*Power(t1,3)*Power(t2,3) + 
                  562*Power(t1,2)*Power(t2,4) - 138*t1*Power(t2,5) - 
                  80*Power(t2,6)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (88*Power(t1,6) + 139*Power(t1,5)*t2 - 
                  317*Power(t1,4)*Power(t2,2) - 
                  13*Power(t1,3)*Power(t2,3) + 
                  118*Power(t1,2)*Power(t2,4) - 80*t1*Power(t2,5) - 
                  30*Power(t2,6)) + 
               Power(s2,3)*(232*Power(t1,7) - 149*Power(t1,6)*t2 - 
                  866*Power(t1,5)*Power(t2,2) + 
                  1278*Power(t1,4)*Power(t2,3) - 
                  553*Power(t1,3)*Power(t2,4) - 
                  162*Power(t1,2)*Power(t2,5) + 308*t1*Power(t2,6) - 
                  88*Power(t2,7))) + 
            Power(s1,6)*(-(Power(s2,7)*(8*t1 + 29*t2)) + 
               Power(s2,6)*(40*Power(t1,2) + 145*t1*t2 + 
                  9*Power(t2,2)) + 
               Power(s2,5)*(-74*Power(t1,3) - 359*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + 497*Power(t2,3)) + 
               Power(s2,4)*(52*Power(t1,4) + 653*Power(t1,3)*t2 - 
                  608*Power(t1,2)*Power(t2,2) - 704*t1*Power(t2,3) - 
                  30*Power(t2,4)) + 
               Power(s2,3)*(8*Power(t1,5) - 795*Power(t1,4)*t2 + 
                  1393*Power(t1,3)*Power(t2,2) - 
                  43*Power(t1,2)*Power(t2,3) + 51*t1*Power(t2,4) - 
                  440*Power(t2,5)) - 
               2*Power(t1,2)*t2*
                (Power(t1,5) + 14*Power(t1,4)*t2 - 
                  52*Power(t1,3)*Power(t2,2) + 
                  35*Power(t1,2)*Power(t2,3) + 7*t1*Power(t2,4) - 
                  5*Power(t2,5)) + 
               Power(s2,2)*(-28*Power(t1,6) + 514*Power(t1,5)*t2 - 
                  1097*Power(t1,4)*Power(t2,2) + 
                  356*Power(t1,3)*Power(t2,3) + 
                  431*Power(t1,2)*Power(t2,4) + 137*t1*Power(t2,5) + 
                  52*Power(t2,6)) + 
               s2*(10*Power(t1,7) - 127*Power(t1,6)*t2 + 
                  322*Power(t1,5)*Power(t2,2) - 
                  266*Power(t1,4)*Power(t2,3) - 
                  55*Power(t1,3)*Power(t2,4) + 
                  55*Power(t1,2)*Power(t2,5) - 70*t1*Power(t2,6) + 
                  3*Power(t2,7))) + 
            Power(s1,5)*(Power(s2,8)*(6*t1 - 2*t2) - 
               6*Power(s2,7)*
                (8*Power(t1,2) - 4*t1*t2 - 25*Power(t2,2)) + 
               2*Power(s2,6)*
                (83*Power(t1,3) - 97*Power(t1,2)*t2 - 
                  306*t1*Power(t2,2) - 79*Power(t2,3)) + 
               Power(s2,5)*(-316*Power(t1,4) + 570*Power(t1,3)*t2 + 
                  979*Power(t1,2)*Power(t2,2) + 718*t1*Power(t2,3) - 
                  1140*Power(t2,4)) + 
               Power(s2,4)*(354*Power(t1,5) - 715*Power(t1,4)*t2 - 
                  1199*Power(t1,3)*Power(t2,2) + 
                  335*Power(t1,2)*Power(t2,3) + 1566*t1*Power(t2,4) - 
                  137*Power(t2,5)) - 
               2*Power(t1,2)*t2*
                (Power(t1,6) - 8*Power(t1,5)*t2 - 
                  25*Power(t1,4)*Power(t2,2) + 
                  96*Power(t1,3)*Power(t2,3) - 
                  82*Power(t1,2)*Power(t2,4) + 15*t1*Power(t2,5) + 
                  3*Power(t2,6)) + 
               Power(s2,3)*(-232*Power(t1,6) + 350*Power(t1,5)*t2 + 
                  1406*Power(t1,4)*Power(t2,2) - 
                  2607*Power(t1,3)*Power(t2,3) + 
                  696*Power(t1,2)*Power(t2,4) - 611*t1*Power(t2,5) + 
                  645*Power(t2,6)) + 
               Power(s2,2)*(82*Power(t1,7) + 3*Power(t1,6)*t2 - 
                  964*Power(t1,5)*Power(t2,2) + 
                  2248*Power(t1,4)*Power(t2,3) - 
                  1353*Power(t1,3)*Power(t2,4) + 
                  391*Power(t1,2)*Power(t2,5) - 484*t1*Power(t2,6) - 
                  51*Power(t2,7)) + 
               s2*(-12*Power(t1,8) - 34*Power(t1,7)*t2 + 
                  224*Power(t1,6)*Power(t2,2) - 
                  602*Power(t1,5)*Power(t2,3) + 
                  698*Power(t1,4)*Power(t2,4) - 
                  461*Power(t1,3)*Power(t2,5) + 
                  129*Power(t1,2)*Power(t2,6) + 111*t1*Power(t2,7) - 
                  3*Power(t2,8))) + 
            Power(s1,2)*t2*(-(Power(s2,10)*t1) + 
               2*Power(t1,3)*Power(t1 - t2,4)*(3*t1 - t2)*Power(t2,3) + 
               Power(s2,9)*(5*Power(t1,2) - 17*t1*t2 + 29*Power(t2,2)) + 
               Power(s2,8)*(12*Power(t1,3) + 90*Power(t1,2)*t2 - 
                  109*t1*Power(t2,2) - 130*Power(t2,3)) + 
               Power(s2,7)*(-122*Power(t1,4) - 168*Power(t1,3)*t2 + 
                  270*Power(t1,2)*Power(t2,2) + 826*t1*Power(t2,3) - 
                  305*Power(t2,4)) - 
               s2*t1*Power(t1 - t2,3)*t2*
                (16*Power(t1,5) + 32*Power(t1,4)*t2 + 
                  9*Power(t1,3)*Power(t2,2) - 
                  173*Power(t1,2)*Power(t2,3) - 59*t1*Power(t2,4) + 
                  5*Power(t2,5)) + 
               Power(s2,6)*(325*Power(t1,5) + 101*Power(t1,4)*t2 - 
                  800*Power(t1,3)*Power(t2,2) - 
                  1578*Power(t1,2)*Power(t2,3) + 1080*t1*Power(t2,4) + 
                  137*Power(t2,5)) + 
               Power(s2,5)*(-439*Power(t1,6) + 55*Power(t1,5)*t2 + 
                  1703*Power(t1,4)*Power(t2,2) + 
                  351*Power(t1,3)*Power(t2,3) - 
                  445*Power(t1,2)*Power(t2,4) - 1038*t1*Power(t2,5) + 
                  189*Power(t2,6)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (22*Power(t1,7) + 97*Power(t1,6)*t2 - 
                  144*Power(t1,5)*Power(t2,2) + 
                  85*Power(t1,4)*Power(t2,3) - 
                  480*Power(t1,3)*Power(t2,4) + 
                  200*Power(t1,2)*Power(t2,5) + 11*t1*Power(t2,6) - 
                  15*Power(t2,7)) + 
               Power(s2,4)*(330*Power(t1,7) - 68*Power(t1,6)*t2 - 
                  1999*Power(t1,5)*Power(t2,2) + 
                  1944*Power(t1,4)*Power(t2,3) - 
                  1791*Power(t1,3)*Power(t2,4) + 
                  2324*Power(t1,2)*Power(t2,5) - 773*t1*Power(t2,6) + 
                  11*Power(t2,7)) + 
               Power(s2,3)*(-132*Power(t1,8) - 30*Power(t1,7)*t2 + 
                  1206*Power(t1,6)*Power(t2,2) - 
                  1928*Power(t1,5)*Power(t2,3) + 
                  2161*Power(t1,4)*Power(t2,4) - 
                  2203*Power(t1,3)*Power(t2,5) + 
                  1018*Power(t1,2)*Power(t2,6) - 182*t1*Power(t2,7) + 
                  90*Power(t2,8))) + 
            Power(s1,4)*(2*Power(s2,9)*(t1 + 6*t2) - 
               Power(s2,8)*(12*Power(t1,2) + 77*t1*t2 + 
                  41*Power(t2,2)) + 
               Power(s2,7)*(26*Power(t1,3) + 274*Power(t1,2)*t2 + 
                  206*t1*Power(t2,2) - 405*Power(t2,3)) + 
               2*Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*
                (9*Power(t1,4) - 13*Power(t1,3)*t2 - 
                  25*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) - 
                  Power(t2,4)) + 
               Power(s2,6)*(-12*Power(t1,4) - 741*Power(t1,3)*t2 - 
                  150*Power(t1,2)*Power(t2,2) + 1365*t1*Power(t2,3) + 
                  275*Power(t2,4)) - 
               Power(s2,5)*(50*Power(t1,5) - 1397*Power(t1,4)*t2 + 
                  810*Power(t1,3)*Power(t2,2) + 
                  1175*Power(t1,2)*Power(t2,3) + 1904*t1*Power(t2,4) - 
                  1331*Power(t2,5)) + 
               Power(s2,4)*(108*Power(t1,6) - 1632*Power(t1,5)*t2 + 
                  1873*Power(t1,4)*Power(t2,2) + 
                  72*Power(t1,3)*Power(t2,3) + 
                  1856*Power(t1,2)*Power(t2,4) - 2140*t1*Power(t2,5) + 
                  281*Power(t2,6)) - 
               Power(s2,3)*(98*Power(t1,7) - 1095*Power(t1,6)*t2 + 
                  1524*Power(t1,5)*Power(t2,2) + 
                  159*Power(t1,4)*Power(t2,3) - 
                  1143*Power(t1,3)*Power(t2,4) + 
                  721*Power(t1,2)*Power(t2,5) - 903*t1*Power(t2,6) + 
                  439*Power(t2,7)) + 
               Power(s2,2)*(44*Power(t1,8) - 378*Power(t1,7)*t2 + 
                  488*Power(t1,6)*Power(t2,2) + 
                  376*Power(t1,5)*Power(t2,3) - 
                  1511*Power(t1,4)*Power(t2,4) + 
                  1496*Power(t1,3)*Power(t2,5) - 
                  1223*Power(t1,2)*Power(t2,6) + 681*t1*Power(t2,7) - 
                  15*Power(t2,8)) + 
               s2*(-8*Power(t1,9) + 50*Power(t1,8)*t2 - 
                  60*Power(t1,7)*Power(t2,2) - 
                  12*Power(t1,6)*Power(t2,3) + 
                  193*Power(t1,5)*Power(t2,4) - 
                  565*Power(t1,4)*Power(t2,5) + 
                  815*Power(t1,3)*Power(t2,6) - 
                  345*Power(t1,2)*Power(t2,7) - 69*t1*Power(t2,8) + 
                  Power(t2,9))) + 
            Power(s1,3)*(-(Power(s2,10)*t2) + 
               Power(s2,9)*(2*Power(t1,2) + 15*t1*t2 - 33*Power(t2,2)) - 
               2*Power(t1,2)*Power(t1 - t2,3)*Power(t2,3)*
                (12*Power(t1,3) - 7*Power(t1,2)*t2 - 5*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,8)*(-18*Power(t1,3) - 39*Power(t1,2)*t2 + 
                  183*t1*Power(t2,2) + 167*Power(t2,3)) + 
               Power(s2,7)*(66*Power(t1,4) + Power(t1,3)*t2 - 
                  545*Power(t1,2)*Power(t2,2) - 926*t1*Power(t2,3) + 
                  619*Power(t2,4)) + 
               Power(s2,6)*(-130*Power(t1,5) + 102*Power(t1,4)*t2 + 
                  1271*Power(t1,3)*Power(t2,2) + 
                  1576*Power(t1,2)*Power(t2,3) - 2067*t1*Power(t2,4) - 
                  205*Power(t2,5)) + 
               Power(s2,5)*(150*Power(t1,6) - 83*Power(t1,5)*t2 - 
                  2219*Power(t1,4)*Power(t2,2) - 
                  16*Power(t1,3)*Power(t2,3) + 
                  1224*Power(t1,2)*Power(t2,4) + 2113*t1*Power(t2,5) - 
                  855*Power(t2,6)) + 
               s2*t1*Power(t1 - t2,2)*t2*
                (24*Power(t1,6) - 18*Power(t1,5)*t2 + 
                  44*Power(t1,4)*Power(t2,2) - 
                  202*Power(t1,3)*Power(t2,3) + 
                  134*Power(t1,2)*Power(t2,4) + 298*t1*Power(t2,5) + 
                  7*Power(t2,6)) - 
               Power(s2,4)*(102*Power(t1,7) + 99*Power(t1,6)*t2 - 
                  2495*Power(t1,5)*Power(t2,2) + 
                  2619*Power(t1,4)*Power(t2,3) - 
                  1940*Power(t1,3)*Power(t2,4) + 
                  3912*Power(t1,2)*Power(t2,5) - 2065*t1*Power(t2,6) + 
                  222*Power(t2,7)) + 
               Power(s2,3)*(38*Power(t1,8) + 195*Power(t1,7)*t2 - 
                  1625*Power(t1,6)*Power(t2,2) + 
                  2666*Power(t1,5)*Power(t2,3) - 
                  2537*Power(t1,4)*Power(t2,4) + 
                  2413*Power(t1,3)*Power(t2,5) - 
                  870*Power(t1,2)*Power(t2,6) - 266*t1*Power(t2,7) + 
                  50*Power(t2,8)) + 
               Power(s2,2)*(-6*Power(t1,9) - 115*Power(t1,8)*t2 + 
                  539*Power(t1,7)*Power(t2,2) - 
                  928*Power(t1,6)*Power(t2,3) + 
                  1043*Power(t1,5)*Power(t2,4) - 
                  990*Power(t1,4)*Power(t2,5) + 
                  178*Power(t1,3)*Power(t2,6) + 
                  642*Power(t1,2)*Power(t2,7) - 405*t1*Power(t2,8) + 
                  42*Power(t2,9)))))*Log(s - s1 + t2))/
     (s*Power(s1,2)*Power(s2,2)*t1*Power(s - s2 + t1,2)*(s - s1 - s2 + t1)*
       (s1 + t1 - t2)*Power(s1 - s2 + t1 - t2,2)*Power(t2,2)*
       Power(s + t2,2)*(s - s1 + t2)*(s2 - t1 + t2)) + 
    (16*(Power(s2,4)*Power(s2 - t1,2)*Power(t2,4)*(s2 + t2) + 
         Power(s1,6)*Power(s2,2)*Power(s2 - t1,2)*(s2 - t1 + t2) - 
         Power(s,6)*s1*(t1 - t2)*Power(t2,2)*(s2 - t1 + t2) + 
         s1*Power(s2,3)*Power(s2 - t1,2)*Power(t2,3)*
          (2*Power(s2,2) - 2*s2*t1 + Power(t1,2) + 4*s2*t2 - t1*t2 + 
            2*Power(t2,2)) + Power(s1,5)*(s2 - t1)*
          (2*Power(s2,5) + 4*s2*Power(t1,2)*Power(t2,2) - 
            Power(t1,3)*Power(t2,2) + Power(s2,4)*(-5*t1 + 3*t2) + 
            Power(s2,3)*(4*Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) - 
            Power(s2,2)*t1*(Power(t1,2) - t1*t2 + 5*Power(t2,2))) + 
         Power(s1,2)*Power(s2,2)*Power(t2,2)*
          (2*Power(s2,5) - 8*Power(s2,4)*(t1 - t2) + 
            2*Power(s2,3)*(8*Power(t1,2) - 10*t1*t2 + 5*Power(t2,2)) + 
            s2*t1*(7*Power(t1,3) - 11*Power(t1,2)*t2 + 
               12*t1*Power(t2,2) - 3*Power(t2,3)) + 
            Power(t1,2)*(-Power(t1,3) + 2*Power(t1,2)*t2 - 
               2*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s2,2)*(-16*Power(t1,3) + 22*Power(t1,2)*t2 - 
               19*t1*Power(t2,2) + 2*Power(t2,3))) + 
         Power(s1,4)*(s2 - t1)*
          (Power(s2,6) - 3*Power(s2,5)*(t1 - t2) - 
            7*s2*Power(t1,3)*Power(t2,2) + Power(t1,4)*Power(t2,2) + 
            Power(s2,2)*t1*t2*(-Power(t1,2) + 16*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,4)*(3*Power(t1,2) - 5*t1*t2 + 5*Power(t2,2)) - 
            Power(s2,3)*(Power(t1,3) - 3*Power(t1,2)*t2 + 
               13*t1*Power(t2,2) + 3*Power(t2,3))) + 
         Power(s1,3)*s2*t2*(2*Power(s2,6) + Power(s2,5)*(-8*t1 + 6*t2) - 
            Power(t1,3)*t2*(2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(s2,4)*(14*Power(t1,2) - 26*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*t1*(6*Power(t1,3) - 42*Power(t1,2)*t2 + 
               13*t1*Power(t2,2) - 8*Power(t2,3)) - 
            s2*Power(t1,2)*(Power(t1,3) - 16*Power(t1,2)*t2 + 
               7*t1*Power(t2,2) - 5*Power(t2,3)) + 
            Power(s2,3)*(-13*Power(t1,3) + 48*Power(t1,2)*t2 - 
               9*t1*Power(t2,2) + 5*Power(t2,3))) - 
         Power(s,5)*(s2*Power(t1 - t2,2)*Power(t2,2)*(s2 + t2) + 
            Power(s1,2)*(s2*Power(t2,2)*(-5*t1 + 2*t2) + 
               2*Power(t2,2)*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) - 2*t1*t2 + 2*Power(t2,2))) + 
            s1*t2*(-3*t1*Power(t1 - t2,2)*t2 + 
               2*s2*t2*(4*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) - 6*t1*t2 + 6*Power(t2,2)))) + 
         Power(s,4)*(s2*(t1 - t2)*Power(t2,2)*
             (Power(s2,2)*(3*t1 - 5*t2) + 2*t1*t2*(-t1 + t2) + 
               s2*(-2*Power(t1,2) + 5*t1*t2 - 5*Power(t2,2))) + 
            Power(s1,3)*(-2*Power(s2,3)*t1 + 
               s2*Power(t2,2)*(-9*t1 + t2) + 
               Power(t2,2)*(6*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,2) - 7*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,2)*(t1*Power(t2,2)*
                (-11*Power(t1,2) + 16*t1*t2 - 5*Power(t2,2)) + 
               3*Power(s2,3)*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
               2*s2*Power(t2,2)*
                (16*Power(t1,2) - 16*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(-2*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  29*t1*Power(t2,2) + 6*Power(t2,3))) + 
            s1*t2*(3*Power(t1,2)*Power(t1 - t2,2)*t2 + 
               2*Power(s2,3)*(2*Power(t1,2) - 9*t1*t2 + 8*Power(t2,2)) + 
               s2*t2*(-15*Power(t1,3) + 33*Power(t1,2)*t2 - 
                  20*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(-3*Power(t1,3) + 29*Power(t1,2)*t2 - 
                  49*t1*Power(t2,2) + 18*Power(t2,3)))) - 
         s*(Power(s1,6)*Power(s2,2)*(s2 - t1)*(s2 - t1 + t2) - 
            Power(s2,3)*(s2 - t1)*Power(t2,3)*
             (Power(s2,2)*(2*t1 - 5*t2) + t1*t2*(-2*t1 + 3*t2) + 
               s2*(-2*Power(t1,2) + 5*t1*t2 - 5*Power(t2,2))) + 
            Power(s1,5)*(4*Power(s2,5) + 7*s2*Power(t1,2)*Power(t2,2) - 
               2*Power(t1,3)*Power(t2,2) + Power(s2,4)*(-13*t1 + 4*t2) + 
               Power(s2,2)*t1*
                (-5*Power(t1,2) + 6*t1*t2 - 8*Power(t2,2)) + 
               2*Power(s2,3)*(7*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) - 
            s1*Power(s2,2)*(s2 - t1)*Power(t2,2)*
             (2*Power(s2,3)*(3*t1 - 5*t2) + 
               Power(s2,2)*(-11*Power(t1,2) + 27*t1*t2 - 
                  18*Power(t2,2)) + 
               s2*(6*Power(t1,3) - 25*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) - 8*Power(t2,3)) - 
               t1*(Power(t1,3) - 6*Power(t1,2)*t2 + 8*t1*Power(t2,2) - 
                  4*Power(t2,3))) + 
            Power(s1,4)*(3*Power(s2,6) + 
               2*Power(t1,3)*(3*t1 - t2)*Power(t2,2) + 
               s2*Power(t1,2)*Power(t2,2)*(-31*t1 + 5*t2) + 
               Power(s2,5)*(-14*t1 + 5*t2) + 
               Power(s2,4)*(22*Power(t1,2) - 22*t1*t2 + 
                  13*Power(t2,2)) + 
               Power(s2,2)*t1*
                (3*Power(t1,3) - 6*Power(t1,2)*t2 + 58*t1*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(s2,3)*(14*Power(t1,3) - 23*Power(t1,2)*t2 + 
                  46*t1*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s1,2)*s2*t2*
             (-6*Power(s2,5)*(t1 - t2) + 
               Power(s2,4)*(17*Power(t1,2) - 42*t1*t2 + 
                  24*Power(t2,2)) + 
               Power(t1,2)*t2*
                (-4*Power(t1,3) + 8*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,3)*(-19*Power(t1,3) + 89*Power(t1,2)*t2 - 
                  70*t1*Power(t2,2) + 34*Power(t2,3)) + 
               s2*t1*(-2*Power(t1,4) + 31*Power(t1,3)*t2 - 
                  46*Power(t1,2)*Power(t2,2) + 30*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,2)*(10*Power(t1,4) - 81*Power(t1,3)*t2 + 
                  86*Power(t1,2)*Power(t2,2) - 54*t1*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,3)*(-2*Power(s2,6)*(t1 - 2*t2) + 
               Power(t1,4)*Power(t2,2)*(-3*t1 + 2*t2) + 
               Power(s2,4)*t1*
                (-6*Power(t1,2) + 39*t1*t2 - 77*Power(t2,2)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (26*Power(t1,2) - 15*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,5)*(6*Power(t1,2) - 25*t1*t2 + 16*Power(t2,2)) + 
               Power(s2,2)*t1*t2*
                (5*Power(t1,3) - 85*Power(t1,2)*t2 + 31*t1*Power(t2,2) - 
                  14*Power(t2,3)) + 
               Power(s2,3)*(2*Power(t1,4) - 23*Power(t1,3)*t2 + 
                  121*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
                  12*Power(t2,4)))) - 
         Power(s,3)*(Power(s1,4)*
             (Power(s2,4) - 7*s2*t1*Power(t2,2) + 
               2*t1*(2*t1 - t2)*Power(t2,2) + Power(s2,3)*(-6*t1 + t2) + 
               Power(s2,2)*(6*Power(t1,2) - 9*t1*t2 + 6*Power(t2,2))) - 
            Power(s1,3)*(6*Power(s2,4)*t1 + 
               Power(s2,3)*(-14*Power(t1,2) + 23*t1*t2 - 
                  16*Power(t2,2)) + 
               s2*Power(t2,2)*
                (-47*Power(t1,2) + 23*t1*t2 - 4*Power(t2,2)) + 
               t1*Power(t2,2)*
                (15*Power(t1,2) - 14*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(7*Power(t1,3) - 15*Power(t1,2)*t2 + 
                  48*t1*Power(t2,2) + 4*Power(t2,3))) + 
            s2*Power(t2,2)*(Power(t1,2)*Power(t1 - t2,2)*t2 + 
               Power(s2,3)*(3*Power(t1,2) - 12*t1*t2 + 10*Power(t2,2)) + 
               s2*t1*(Power(t1,3) - 6*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s2,2)*(-4*Power(t1,3) + 15*Power(t1,2)*t2 - 
                  20*t1*Power(t2,2) + 10*Power(t2,3))) + 
            s1*t2*(-(Power(t1,3)*Power(t1 - t2,2)*t2) + 
               Power(s2,4)*(5*Power(t1,2) - 28*t1*t2 + 24*Power(t2,2)) + 
               2*s2*t1*t2*(5*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,3)*(-8*Power(t1,3) + 55*Power(t1,2)*t2 - 
                  89*t1*Power(t2,2) + 32*Power(t2,3)) + 
               Power(s2,2)*(3*Power(t1,4) - 38*Power(t1,3)*t2 + 
                  86*Power(t1,2)*Power(t2,2) - 51*t1*Power(t2,3) + 
                  8*Power(t2,4))) + 
            Power(s1,2)*(2*Power(t1,2)*Power(t2,2)*
                (5*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,4)*(3*Power(t1,2) - 18*t1*t2 + 8*Power(t2,2)) + 
               s2*Power(t2,2)*
                (-51*Power(t1,3) + 63*Power(t1,2)*t2 - 
                  26*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,3)*(-4*Power(t1,3) + 27*Power(t1,2)*t2 - 
                  69*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,2)*(Power(t1,4) - 10*Power(t1,3)*t2 + 
                  97*Power(t1,2)*Power(t2,2) - 76*t1*Power(t2,3) + 
                  28*Power(t2,4)))) + 
         Power(s,2)*(Power(s1,5)*
             (2*Power(s2,4) - 2*s2*t1*Power(t2,2) + 
               Power(t1,2)*Power(t2,2) + Power(s2,3)*(-6*t1 + 2*t2) + 
               Power(s2,2)*(4*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,4)*(3*Power(s2,5) + 
               5*s2*t1*(6*t1 - t2)*Power(t2,2) + 
               Power(s2,4)*(-16*t1 + 3*t2) + 
               Power(t1,2)*Power(t2,2)*(-9*t1 + 4*t2) + 
               2*Power(s2,3)*
                (11*Power(t1,2) - 11*t1*t2 + 7*Power(t2,2)) - 
               Power(s2,2)*(9*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,2)*Power(t2,2)*
             (Power(t1,2)*t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*(Power(t1,2) - 8*t1*t2 + 10*Power(t2,2)) + 
               s2*t1*(Power(t1,3) - 6*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - 12*Power(t2,3)) + 
               Power(s2,2)*(-2*Power(t1,3) + 13*Power(t1,2)*t2 - 
                  20*t1*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s1,3)*(Power(s2,5)*(-6*t1 + 2*t2) + 
               Power(t1,2)*Power(t2,2)*
                (12*Power(t1,2) - 10*t1*t2 + Power(t2,2)) - 
               2*s2*t1*Power(t2,2)*
                (31*Power(t1,2) - 18*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,4)*(16*Power(t1,2) - 33*t1*t2 + 20*Power(t2,2)) - 
               Power(s2,3)*(13*Power(t1,3) - 41*Power(t1,2)*t2 + 
                  90*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(3*Power(t1,4) - 12*Power(t1,3)*t2 + 
                  118*Power(t1,2)*Power(t2,2) - 28*t1*Power(t2,3) + 
                  10*Power(t2,4))) + 
            s1*s2*t2*(Power(s2,4)*
                (2*Power(t1,2) - 21*t1*t2 + 21*Power(t2,2)) + 
               2*Power(t1,2)*t2*
                (-Power(t1,3) + 4*Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,3)*(-5*Power(t1,3) + 50*Power(t1,2)*t2 - 
                  83*t1*Power(t2,2) + 33*Power(t2,3)) + 
               Power(s2,2)*(4*Power(t1,4) - 43*Power(t1,3)*t2 + 
                  104*Power(t1,2)*Power(t2,2) - 63*t1*Power(t2,3) + 
                  12*Power(t2,4)) - 
               s2*t1*(Power(t1,4) - 16*Power(t1,3)*t2 + 
                  49*Power(t1,2)*Power(t2,2) - 41*t1*Power(t2,3) + 
                  12*Power(t2,4))) + 
            Power(s1,2)*(-(Power(t1,3)*Power(t2,2)*
                  (3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
               Power(s2,5)*(Power(t1,2) - 17*t1*t2 + 8*Power(t2,2)) + 
               s2*t1*Power(t2,2)*
                (28*Power(t1,3) - 41*Power(t1,2)*t2 + 22*t1*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(s2,4)*(-2*Power(t1,3) + 37*Power(t1,2)*t2 - 
                  79*t1*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s2,2)*t2*
                (7*Power(t1,4) - 98*Power(t1,3)*t2 + 
                  111*Power(t1,2)*Power(t2,2) - 56*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,3)*(Power(t1,4) - 27*Power(t1,3)*t2 + 
                  142*Power(t1,2)*Power(t2,2) - 100*t1*Power(t2,3) + 
                  44*Power(t2,4)))))*Log(s2 - t1 + t2))/
     (Power(s1,2)*Power(s2,2)*t1*Power(s - s2 + t1,2)*(s - s1 - s2 + t1)*
       Power(t2,2)*(s - s1 + t2)*(s2 + t2)));
       
       return a;
};
