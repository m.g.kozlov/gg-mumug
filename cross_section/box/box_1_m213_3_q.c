#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m213_3_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(-2*Power(s1,10)*t1*(s2 + t1)*t2 + 
         (-2 + s)*Power(t2,3)*
          (Power(t1,3) - 4*t1*t2 - Power(t1,2)*(1 + t2) + 4*t2*(1 + t2))*
          (Power(s,2)*(-2 + t1 - t2) + s*(3 - 3*t1 + (-3 + s2)*t2) + 
            2*(t1 + t2 - Power(t2,2))) + 
         Power(s1,9)*(-8*Power(t1,3)*t2 + s2*t2*(Power(s2,2) + 2*t2) + 
            Power(t1,2)*(2 + (-8 + s - 11*s2)*t2 + 6*Power(t2,2)) + 
            t1*(2 + (12 + s*(-14 + s2) + 8*s2 - 2*Power(s2,2))*t2 + 
               (4 - 2*s + 8*s2)*Power(t2,2))) - 
         Power(s1,8)*(17*Power(t1,4)*t2 + 
            Power(t1,3)*(-8 + (-10 + 2*s + 21*s2)*t2 - 13*Power(t2,2)) + 
            Power(t1,2)*(-4 + 
               (-12 + 5*s + Power(s,2) - 53*s2 - 11*s*s2 + 
                  15*Power(s2,2))*t2 + 
               (-47 + 18*s - 29*s2)*Power(t2,2) + 6*Power(t2,3)) + 
            t1*(4 - (-6 - 53*s2 + 23*Power(s2,2) + Power(s2,3) + 
                  2*Power(s,2)*(7 + s2) - 5*s*(-5 + 2*s2 + Power(s2,2))\
)*t2 - (-18 + Power(s,2) + s*(36 - 14*s2) - 7*s2 + 14*Power(s2,2))*
                Power(t2,2) - 6*(s - 2*(1 + s2))*Power(t2,3)) + 
            t2*(Power(s2,3)*(2 + 4*t2) + s2*t2*(8 + s + 8*t2) + 
               Power(s2,2)*(3 - 2*t2 - 3*s*t2) + 
               2*(1 + 6*t2 - 7*s*t2 + Power(t2,2) - s*Power(t2,2)))) + 
         Power(s1,7)*(-18*Power(t1,5)*t2 + 
            Power(t1,4)*(12 + (85 - 25*s2)*t2 + 9*Power(t2,2)) + 
            Power(t1,3)*(-4 + 
               (-86 + 6*Power(s,2) + 90*s2 - 32*Power(s2,2) + 
                  s*(90 + 11*s2))*t2 + (79 - 36*s + 44*s2)*Power(t2,2) - 
               9*Power(t2,3)) + 
            Power(t1,2)*(-18 + 
               (53 + 2*Power(s,3) + Power(s,2)*(71 - 2*s2) - 224*s2 + 
                  106*Power(s2,2) - Power(s2,3) + 
                  s*(15 - 76*s2 - 11*Power(s2,2)))*t2 + 
               (-67 + 7*Power(s,2) + s*(93 - 50*s2) - 65*s2 + 
                  54*Power(s2,2))*Power(t2,2) + 
               (-61 + 22*s - 27*s2)*Power(t2,3) + 2*Power(t2,4)) + 
            t1*(-2 + (23 + 14*Power(s,3) - 3*s2 + 13*Power(s2,2) - 
                  17*Power(s2,3) - Power(s,2)*(30 + 13*s2) + 
                  s*(-24 - 11*s2 + 44*Power(s2,2)))*t2 + 
               (-79 + 2*Power(s,3) + Power(s,2)*(-5 + s2) + 46*s2 - 
                  55*Power(s2,2) + Power(s2,3) + 
                  s*(-59 + 63*s2 + 6*Power(s2,2)))*Power(t2,2) - 
               (36 + 11*Power(s,2) + 29*s2 + 26*Power(s2,2) - 
                  s*(7 + 33*s2))*Power(t2,3) + 
               (12 - 6*s + 8*s2)*Power(t2,4)) + 
            t2*(4 + (4 - 25*s - 14*Power(s,2))*t2 - 
               (-26 + 37*s + Power(s,2))*Power(t2,2) - 
               6*(-1 + s)*Power(t2,3) + 
               Power(s2,3)*(-1 + 7*t2 + 6*Power(t2,2)) - 
               Power(s2,2)*(-6 + (11 + 4*s)*t2 + 
                  (14 + 9*s)*Power(t2,2)) + 
               s2*(3 + (53 + 4*s - 2*Power(s,2))*t2 + 
                  (18 + 14*s + 3*Power(s,2))*Power(t2,2) + 12*Power(t2,3))\
)) + s1*Power(t2,2)*(Power(t1,5)*
             (-1 + 2*Power(s,2) + s*(-11 + t2) + (27 - 8*s2)*t2) - 
            Power(t1,4)*(-25 + Power(s,3)*t2 + (32 - 11*s2)*t2 + 
               (28 - 19*s2 + 8*Power(s2,2))*Power(t2,2) - 
               2*Power(s,2)*(-5 + t2 + s2*t2) + 
               s*(6 + (-43 + 16*s2)*t2 + (2 - 4*s2)*Power(t2,2))) + 
            Power(t1,3)*(-35 - (27 + 4*s2)*t2 + 
               (-63 - 17*s2 + 22*Power(s2,2))*Power(t2,2) + 
               (37 - 31*s2 + 4*Power(s2,2))*Power(t2,3) + 
               Power(s,3)*(-5 + 7*t2 + 2*Power(t2,2)) + 
               Power(s,2)*(7 + (3 - 11*s2)*t2 - 
                  (4 + 5*s2)*Power(t2,2)) + 
               s*(41 + 3*(27 + 4*s2)*t2 + 
                  (23 - 15*s2 - 10*Power(s2,2))*Power(t2,2) - 
                  (3 + 4*s2)*Power(t2,3))) + 
            2*t1*t2*(70 + 50*t2 + Power(s2,3)*(-1 + t2)*t2 - 
               90*Power(t2,2) - 78*Power(t2,3) - 
               2*Power(s,3)*(-6 + 3*t2 + 2*Power(t2,2)) - 
               2*Power(s2,2)*t2*(1 + 22*t2 + 3*Power(t2,2)) + 
               Power(s,2)*(-21 + 2*(-25 + 9*s2)*t2 + 
                  3*(3 + 4*s2)*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-3 + 9*t2 + 101*Power(t2,2) + 61*Power(t2,3)) + 
               s*(-73 + 3*(-15 - 6*s2 + Power(s2,2))*t2 + 
                  (-63 + 32*s2 + 17*Power(s2,2))*Power(t2,2) + 
                  (9 + 4*s2)*Power(t2,3))) + 
            Power(t1,2)*(11 + 7*(-12 + s2)*t2 + 
               (111 - 64*s2 + 18*Power(s2,2) - 2*Power(s2,3))*
                Power(t2,2) + 
               (150 - 133*s2 + 61*Power(s2,2) - 4*Power(s2,3))*
                Power(t2,3) + (-24 + 26*s2)*Power(t2,4) - 
               Power(s,3)*(-10 - 5*t2 + 11*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(1 - 2*(-48 + s2)*t2 + 
                  2*(8 + s2)*Power(t2,2) + 3*(-4 + s2)*Power(t2,3)) + 
               s*(-24 + (-69 + 4*s2)*t2 + 
                  2*(-64 - s2 + 15*Power(s2,2))*Power(t2,2) + 
                  (-15 + 22*s2 + 2*Power(s2,2))*Power(t2,3))) + 
            4*t2*(-10 + (-8 - 6*s2 + 3*Power(s2,2))*t2 + 
               (15 + 21*s2 - 20*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,2) + 
               (-30 + 56*s2 - 30*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,3) + (25 - 27*s2)*Power(t2,4) + 
               Power(s,3)*(-10 - 11*t2 + 10*Power(t2,2) + 
                  2*Power(t2,3)) - 
               Power(s,2)*(1 + (29 - 2*s2)*t2 + 
                  (18 + 5*s2)*Power(t2,2) + (-11 + 6*s2)*Power(t2,3)) + 
               s*(24 + (64 - 10*s2)*t2 + 
                  (-45 + 66*s2 - 33*Power(s2,2))*Power(t2,2) + 
                  (20 - 36*s2 + Power(s2,2))*Power(t2,3) + Power(t2,4)))) \
+ Power(s1,6)*(-7*Power(t1,6)*t2 + 
            Power(t1,5)*(8 + (108 + 10*s - 21*s2)*t2 + 3*Power(t2,2)) + 
            Power(t1,4)*(-16 + 
               (-215 + 15*Power(s,2) + s*(131 - 7*s2) + 69*s2 - 
                  27*Power(s2,2))*t2 + (22 - 27*s + 41*s2)*Power(t2,2)) + 
            Power(t1,3)*(-24 + 
               (217 + 4*Power(s,3) + Power(s,2)*(94 - 10*s2) - 
                  328*s2 + 151*Power(s2,2) - Power(s2,3) - 
                  s*(140 + 102*s2 + 7*Power(s2,2)))*t2 + 
               (-240 + 15*Power(s,2) - 107*s2 + 82*Power(s2,2) - 
                  9*s*(-5 + 6*s2))*Power(t2,2) + 
               (-51 + 32*s - 25*s2)*Power(t2,3) + 4*Power(t2,4)) + 
            Power(t1,2)*(10 + 
               (67 + 29*Power(s,3) + 15*s2 + 12*Power(s2,2) - 
                  28*Power(s2,3) - 2*Power(s,2)*(76 + 9*s2) + 
                  s*(-28 + 9*s2 + 101*Power(s2,2)))*t2 + 
               (-22 + 7*Power(s,3) + 309*s2 - 210*Power(s2,2) + 
                  14*Power(s2,3) + Power(s,2)*(-83 + 10*s2) + 
                  s*(-470 + 229*s2 + 5*Power(s2,2)))*Power(t2,2) - 
               (129 + 16*Power(s,2) + 17*s2 + 67*Power(s2,2) - 
                  3*s*(6 + 25*s2))*Power(t2,3) + 
               (34 - 7*s + 11*s2)*Power(t2,4)) + 
            t1*(10 + (-26 - 38*Power(s,3) + 50*s2 + 16*Power(s2,2) + 
                  Power(s,2)*(29 + 20*s2) + 
                  s*(52 - 32*s2 - 39*Power(s2,2)))*t2 + 
               (-86 - 29*Power(s,3) + 460*s2 - 263*Power(s2,2) + 
                  59*Power(s2,3) + Power(s,2)*(-128 + 19*s2) + 
                  s*(3 + 158*s2 - 86*Power(s2,2)))*Power(t2,2) + 
               (247 + 8*Power(s,3) + 91*s2 - 11*Power(s2,2) - 
                  5*Power(s2,3) - Power(s,2)*(5 + 8*s2) + 
                  s*(-161 - 44*s2 + 3*Power(s2,2)))*Power(t2,3) + 
               (69 + 14*Power(s,2) + 53*s2 + 18*Power(s2,2) - 
                  2*s*(23 + 14*s2))*Power(t2,4) + 
               2*(-2 + s - s2)*Power(t2,5)) + 
            t2*(1 + (-27 + 27*s + 30*Power(s,2) - 14*Power(s,3))*t2 + 
               (67 + 64*s + 3*Power(s,2) - 2*Power(s,3))*Power(t2,2) + 
               (-3 + 11*s + 11*Power(s,2) + Power(s,3))*Power(t2,3) + 
               6*(-1 + s)*Power(t2,4) + 
               Power(s2,3)*(2 + 23*t2 - 13*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,2)*(3 - (37 + 41*s)*t2 + 
                  (52 + 21*s)*Power(t2,2) + (26 + 9*s)*Power(t2,3)) - 
               s2*(6 + (9 - 29*s - 13*Power(s,2))*t2 + 
                  (99 + 56*s + 13*Power(s,2))*Power(t2,2) + 
                  3*s*(11 + 2*s)*Power(t2,3) + 8*Power(t2,4)))) + 
         Power(s1,5)*(Power(t1,6)*
             (2 + (41 + 7*s - 8*s2)*t2 + Power(t2,2)) + 
            Power(t1,5)*(-14 + 
               2*(-93 + 4*Power(s,2) + s*(21 - 4*s2) + 20*s2 - 
                  4*Power(s2,2))*t2 + (-63 - 8*s + 26*s2)*Power(t2,2) + 
               3*Power(t2,3)) + 
            Power(t1,4)*(-8 + 
               (2*Power(s,3) + Power(s,2)*(31 - 6*s2) - 
                  s*(234 + 16*s2 + Power(s2,2)) + 
                  4*(67 - 50*s2 + 19*Power(s2,2)))*t2 + 
               (-170 + 13*Power(s,2) - 35*s2 + 58*Power(s2,2) - 
                  s*(109 + 14*s2))*Power(t2,2) + 
               (-19 + 20*s - 15*s2)*Power(t2,3) + 2*Power(t2,4)) + 
            Power(t1,3)*(26 + 
               (93 + 16*Power(s,3) + 3*Power(s,2)*(-76 + s2) + 59*s2 - 
                  29*Power(s2,2) - 13*Power(s2,3) + 
                  s*(44 + 43*s2 + 70*Power(s2,2)))*t2 + 
               (63 + 8*Power(s,3) + 656*s2 - 289*Power(s2,2) + 
                  9*Power(s2,3) + Power(s,2)*(-191 + 17*s2) + 
                  s*(-480 + 329*s2 + 4*Power(s2,2)))*Power(t2,2) - 
               (-116 + 3*Power(s,2) + s*(30 - 63*s2) + 37*s2 + 
                  68*Power(s2,2))*Power(t2,3) + 2*(-5 + s2)*Power(t2,4)) \
+ Power(t1,2)*(10 + (-77 - 84*Power(s,3) + 92*s2 + 4*Power(s2,2) + 
                  3*Power(s2,3) + Power(s,2)*(115 + 38*s2) + 
                  s*(145 - 56*s2 - 78*Power(s2,2)))*t2 + 
               (-596 - 70*Power(s,3) + 874*s2 - 485*Power(s2,2) + 
                  75*Power(s2,3) + Power(s,2)*(-143 + 70*s2) + 
                  s*(428 + 330*s2 - 216*Power(s2,2)))*Power(t2,2) + 
               (539 + 15*Power(s,3) - 116*s2 + 76*Power(s2,2) - 
                  26*Power(s2,3) - Power(s,2)*(31 + 21*s2) + 
                  s*(179 - 206*s2 + 26*Power(s2,2)))*Power(t2,3) + 
               (142 + 20*Power(s,2) + 17*s2 + 32*Power(s2,2) - 
                  2*s*(41 + 22*s2))*Power(t2,4) + 
               2*(-6 + s - s2)*Power(t2,5)) - 
            t1*(8 + (-4 - 34*Power(s,3) + s*(10 - 64*s2) + 74*s2 + 
                  8*Power(s2,2) - 4*Power(s2,3) + 
                  3*Power(s,2)*(8 + 3*s2))*t2 + 
               (91 + 31*Power(s,3) - 9*s2 + 162*Power(s2,2) - 
                  68*Power(s2,3) - Power(s,2)*(238 + 3*s2) + 
                  s*(2 - 99*s2 + 160*Power(s2,2)))*Power(t2,2) + 
               (-122 + 31*Power(s,3) + 23*Power(s,2)*(-8 + s2) + 
                  1036*s2 - 669*Power(s2,2) + 111*Power(s2,3) + 
                  s*(-802 + 631*s2 - 100*Power(s2,2)))*Power(t2,3) + 
               (4*Power(s,3) - Power(s,2)*(12 + 5*s2) + 
                  s*(-135 + 69*s2 + 4*Power(s2,2)) - 
                  3*(-12 - 46*s2 + 33*Power(s2,2) + Power(s2,3)))*
                Power(t2,4) + 
               (39 + 4*Power(s,2) + 31*s2 + 4*Power(s2,2) - 
                  s*(23 + 8*s2))*Power(t2,5)) + 
            t2*(-8 + (48 - 61*s - 29*Power(s,2) + 38*Power(s,3))*t2 + 
               (25 - 27*s + 69*Power(s,2) + 27*Power(s,3))*Power(t2,2) + 
               (-190 + 70*s + 4*Power(s,2) - 13*Power(s,3))*Power(t2,3) - 
               (21 - 24*s + 14*Power(s,2) + Power(s,3))*Power(t2,4) - 
               2*(-1 + s)*Power(t2,5) + 
               Power(s2,3)*t2*
                (-8 - 73*t2 + 13*Power(t2,2) + Power(t2,3)) - 
               Power(s2,2)*(6 + (34 - 48*s)*t2 - 
                  (193 + 94*s)*Power(t2,2) + (31 + 30*s)*Power(t2,3) + 
                  3*(6 + s)*Power(t2,4)) + 
               s2*(-3 + (-26 + 26*s - 20*Power(s,2))*t2 - 
                  (218 + 136*s + 5*Power(s,2))*Power(t2,2) + 
                  (-5 + 76*s + 32*Power(s,2))*Power(t2,3) + 
                  (-26 + 28*s + 3*Power(s,2))*Power(t2,4) + 2*Power(t2,5))\
)) + Power(s1,3)*(Power(t1,6)*
             (2 + (19 - 5*s - 8*s2)*t2 + (55 + 2*s - 16*s2)*Power(t2,2)) \
+ Power(t1,5)*(2 - 4*(3*Power(s,2) - 2*(2 + s2) + s*(-13 + 3*s2))*t2 - 
               (108 - 107*s + 6*Power(s,2) - 54*s2 + 16*s*s2 + 
                  16*Power(s2,2))*Power(t2,2) + 
               (22 - 8*s2 - 8*Power(s2,2) + s*(-7 + 4*s2))*Power(t2,3)) - 
            Power(t1,4)*(4 + (123 + 8*Power(s,3) - 11*s2 + 
                  Power(s,2)*(-87 + 2*s2) + s*(-41 + 12*s2))*t2 + 
               (98 + 6*Power(s,3) + 15*s2 - 42*Power(s2,2) + 
                  Power(s,2)*(-109 + 8*s2) + 
                  s*(-57 + 50*s2 + 26*Power(s2,2)))*Power(t2,2) + 
               (167 - Power(s,3) + 126*s2 - 68*Power(s2,2) + 
                  2*Power(s,2)*(4 + s2) + s*(-86 + 68*s2 + Power(s2,2))\
)*Power(t2,3) + (16 - 23*s2 - 4*Power(s2,2) + 4*s*(2 + s2))*Power(t2,4)) \
+ Power(t1,3)*(-2 + (30 + 44*Power(s,3) - 20*s2 + 4*s*(-23 + 9*s2) - 
                  Power(s,2)*(40 + 9*s2))*t2 + 
               (4 + 49*Power(s,3) + Power(s,2)*(64 - 45*s2) - 177*s2 + 
                  34*Power(s2,2) - 4*Power(s2,3) + 
                  s*(-283 + 57*s2 + 68*Power(s2,2)))*Power(t2,2) + 
               (579 - 15*Power(s,3) + Power(s,2)*(122 - 11*s2) - 
                  568*s2 + 249*Power(s2,2) - 23*Power(s2,3) + 
                  s*(-695 + 55*s2 + 78*Power(s2,2)))*Power(t2,3) + 
               (-246 - 2*Power(s,3) + 184*s2 + 21*Power(s2,2) + 
                  7*Power(s2,3) + Power(s,2)*(-16 + 5*s2) + 
                  s*(35 + 37*s2 - 10*Power(s2,2)))*Power(t2,4) + 
               (-1 + s + 3*s2)*Power(t2,5)) + 
            Power(t1,2)*(2 + (48 + 16*Power(s,2) - 20*Power(s,3) + 
                  15*s2 - 12*s*(1 + s2))*t2 + 
               (451 + 38*Power(s,3) + 8*s2 + 40*Power(s2,2) - 
                  6*Power(s2,3) + Power(s,2)*(-359 + 44*s2) + 
                  s*(-243 - 118*s2 + 30*Power(s2,2)))*Power(t2,2) + 
               (123 + 50*Power(s,3) + 321*s2 - 169*Power(s2,2) - 
                  15*Power(s2,3) + Power(s,2)*(-685 + 111*s2) + 
                  6*s*(-97 + 68*s2 + 19*Power(s2,2)))*Power(t2,3) + 
               (-914 + Power(s,3) + 1408*s2 - 519*Power(s2,2) + 
                  33*Power(s2,3) + Power(s,2)*(30 + 8*s2) + 
                  s*(-550 + 468*s2 - 28*Power(s2,2)))*Power(t2,4) + 
               (129 + Power(s,3) + Power(s,2)*(2 - 3*s2) - 166*s2 - 
                  26*Power(s2,2) - Power(s2,3) + 
                  s*(48 + 20*s2 + 3*Power(s2,2)))*Power(t2,5) + 
               2*Power(t2,6)) + 
            t2*(-4 + (-23 - 11*Power(s,2) + 10*Power(s,3) - 68*s2 + 
                  12*Power(s2,2) + 8*s*(7 + 3*s2))*t2 - 
               (106 + 117*Power(s,3) - 6*Power(s,2)*(-3 + s2) + 
                  19*s2 + 126*Power(s2,2) - 24*Power(s2,3) + 
                  s*(-261 - 232*s2 + 120*Power(s2,2)))*Power(t2,2) + 
               (-179 - 89*Power(s,3) + Power(s,2)*(122 - 30*s2) + 
                  398*s2 - 420*Power(s2,2) + 84*Power(s2,3) - 
                  2*s*(-79 - 51*s2 + 96*Power(s2,2)))*Power(t2,3) + 
               (232 + Power(s,3) + Power(s,2)*(52 - 23*s2) - 675*s2 + 
                  441*Power(s2,2) - 102*Power(s2,3) + 
                  s*(515 - 618*s2 + 116*Power(s2,2)))*Power(t2,4) + 
               2*(14 + 7*s + 6*Power(s,2) - 38*s2 - 12*s*s2 + 
                  14*Power(s2,2))*Power(t2,5) + 
               4*(-1 + s - s2)*Power(t2,6)) + 
            t1*t2*(-2 - 89*t2 - 101*Power(t2,2) - 861*Power(t2,3) + 
               605*Power(t2,4) + 24*Power(t2,5) + 
               Power(s,3)*t2*(-203 - 221*t2 + 68*Power(t2,2) + 
                  8*Power(t2,3)) - 
               2*Power(s2,3)*t2*
                (1 - 13*t2 - 64*Power(t2,2) + 22*Power(t2,3)) + 
               2*Power(s2,2)*t2*
                (4 - 118*t2 - 489*Power(t2,2) + 83*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*(-6 + 10*t2 + 484*Power(t2,2) + 1847*Power(t2,3) - 
                  585*Power(t2,4) - 30*Power(t2,5)) + 
               s*(18 + (481 - 148*s2 + 6*Power(s2,2))*t2 + 
                  (219 + 424*s2 - 386*Power(s2,2))*Power(t2,2) + 
                  (99 + 243*s2 - 290*Power(s2,2))*Power(t2,3) + 
                  (119 - 376*s2 + 60*Power(s2,2))*Power(t2,4) + 
                  (6 - 8*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(15 - 353*t2 - 328*Power(t2,2) + 
                  102*Power(t2,3) + 4*Power(t2,4) + 
                  3*s2*(12 + 51*t2 + 9*Power(t2,2) - 8*Power(t2,3))))) + 
         Power(s1,4)*(-(Power(t1,6)*
               (4 + (57 + 8*s - 16*s2)*t2 + (29 + s - 8*s2)*Power(t2,2))) \
+ Power(t1,5)*(4 + (129 - 6*Power(s,2) - 51*s2 + 8*Power(s2,2) + 
                  s*(-109 + 20*s2))*t2 + 
               (4*Power(s,2) + s*(-47 + 4*s2) + 
                  4*(27 - 8*s2 + 4*Power(s2,2)))*Power(t2,2) + 
               (3 + 4*s - 5*s2)*Power(t2,3)) + 
            Power(t1,4)*(16 + 
               (55 + Power(s,3) + 49*s2 - 25*Power(s2,2) + 
                  2*Power(s,2)*(-59 + 4*s2) + 
                  s*(94 + 11*s2 + 13*Power(s2,2)))*t2 + 
               (1 + 3*Power(s,3) + 295*s2 - 148*Power(s2,2) + 
                  Power(s,2)*(-83 + 8*s2) + 
                  s*(145 + 84*s2 + 2*Power(s2,2)))*Power(t2,2) + 
               (209 + 2*Power(s,2) - 57*s2 - 35*Power(s2,2) + 
                  s*(-26 + 25*s2))*Power(t2,3) + 
               (-4 + s - s2)*Power(t2,4)) - 
            Power(t1,3)*(4 + (181 + 54*Power(s,3) - 64*s2 + 
                  6*Power(s2,2) - 2*Power(s2,3) - 
                  Power(s,2)*(163 + 16*s2) + 
                  s*(-121 + 36*s2 + 39*Power(s2,2)))*t2 + 
               (569 + 39*Power(s,3) - 244*s2 + 79*Power(s2,2) - 
                  31*Power(s2,3) - 9*Power(s,2)*(24 + s2) + 
                  2*s*(-345 + 71*s2 + 72*Power(s2,2)))*Power(t2,2) + 
               (-294 - 8*Power(s,3) + 468*s2 - 121*Power(s2,2) + 
                  15*Power(s2,3) + Power(s,2)*(31 + 12*s2) + 
                  s*(-503 + 268*s2 - 13*Power(s2,2)))*Power(t2,3) + 
               (13 - 6*Power(s,2) - 51*s2 - 18*Power(s2,2) + 
                  s*(46 + 20*s2))*Power(t2,4) + 4*Power(t2,5)) + 
            Power(t1,2)*(-10 + 
               (-38 + 73*Power(s,3) - 60*s2 - 11*Power(s2,2) + 
                  2*Power(s2,3) + 10*s*(-11 + 10*s2) - 
                  Power(s,2)*(49 + 18*s2))*t2 + 
               (-48 + 77*Power(s,3) + Power(s,2)*(523 - 127*s2) - 
                  329*s2 + 109*Power(s2,2) + Power(s2,3) + 
                  s*(-151 - 157*s2 + 61*Power(s2,2)))*Power(t2,2) + 
               (848 - 43*Power(s,3) + Power(s,2)*(498 - 61*s2) - 
                  2048*s2 + 963*Power(s2,2) - 73*Power(s2,3) + 
                  s*(407 - 731*s2 + 136*Power(s2,2)))*Power(t2,3) + 
               (-441 - 7*Power(s,3) + 199*s2 + 54*Power(s2,2) + 
                  14*Power(s2,3) + Power(s,2)*(-43 + 16*s2) + 
                  s*(86 + 33*s2 - 23*Power(s2,2)))*Power(t2,4) + 
               (2 - 4*Power(s,2) + 12*s2 - 4*Power(s2,2) + 
                  s*(-6 + 8*s2))*Power(t2,5)) - 
            t1*(-2 + (-7 - 11*Power(s,2) + 10*Power(s,3) - 44*s2 + 
                  6*Power(s2,2) + s*(47 + 12*s2))*t2 + 
               (-219 - 201*Power(s,3) + 118*s2 - 86*Power(s2,2) + 
                  18*Power(s2,3) + Power(s,2)*(103 + 59*s2) + 
                  s*(409 + 110*s2 - 210*Power(s2,2)))*Power(t2,2) + 
               (-550 - 153*Power(s,3) + 920*s2 - 694*Power(s2,2) + 
                  166*Power(s2,3) + Power(s,2)*(55 + 54*s2) + 
                  s*(473 + 306*s2 - 463*Power(s2,2)))*Power(t2,3) + 
               (616 + 22*Power(s,3) + Power(s,2)*(24 - 41*s2) - 
                  926*s2 + 593*Power(s2,2) - 113*Power(s2,3) + 
                  2*s*(371 - 430*s2 + 59*Power(s2,2)))*Power(t2,4) + 
               4*(32 + 8*Power(s,2) - 21*s2 + 15*Power(s2,2) - 
                  s*(9 + 17*s2))*Power(t2,5) + 
               6*(-2 + s - s2)*Power(t2,6)) + 
            t2*(9 + (-22 + 13*s + 24*Power(s,2) - 34*Power(s,3))*t2 + 
               (22 + 57*s - 98*Power(s,2) + 2*Power(s,3))*Power(t2,2) + 
               (-14 - 413*s - 131*Power(s,2) + 32*Power(s,3))*
                Power(t2,3) + 
               (103 - 117*s + Power(s,2) + 9*Power(s,3))*Power(t2,4) + 
               2*(7 - 8*s + 2*Power(s,2))*Power(t2,5) - 
               Power(s2,3)*t2*
                (6 + 28*t2 - 117*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(6 + (92 - 82*s + 9*Power(s,2))*t2 + 
                  3*(-20 - 34*s + 7*Power(s,2))*Power(t2,2) - 
                  5*(-125 - 89*s + Power(s,2))*Power(t2,3) + 
                  (111 - 17*Power(s,2))*Power(t2,4) + 
                  (20 - 8*s)*Power(t2,5)) + 
               Power(s2,2)*t2*
                (32 + 195*t2 - 451*Power(t2,2) - 35*Power(t2,3) + 
                  4*Power(t2,4) + 
                  s*(-6 + 26*t2 - 117*Power(t2,2) + 13*Power(t2,3))))) + 
         Power(s1,2)*t2*(Power(t1,6)*(4 - s*(-6 + t2) + (-27 + 8*s2)*t2) + 
            Power(t1,5)*(-25 + 10*Power(s,2) + 13*t2 + 
               (-31 - 3*s2 + 8*Power(s2,2))*Power(t2,2) + 
               s*(5 + (-41 + 12*s2)*t2 + (2 - 4*s2)*Power(t2,2))) + 
            Power(t1,4)*(19 - (5 + 22*s2)*t2 + 
               (229 - 53*s2 - 9*Power(s2,2))*Power(t2,2) + 
               (-26 + 31*s2 + 4*Power(s2,2))*Power(t2,3) + 
               Power(s,3)*(5 + 3*t2 - Power(t2,2)) + 
               Power(s,2)*(-15 - 29*t2 + 9*Power(t2,2)) + 
               s*(-20 + 28*(-3 + s2)*t2 + 
                  (-119 + 35*s2 + 13*Power(s2,2))*Power(t2,2) + 
                  9*Power(t2,3))) + 
            Power(t1,3)*(9 + (217 + 24*s2)*t2 + 
               2*(-75 + 65*s2 - 25*Power(s2,2) + Power(s2,3))*
                Power(t2,2) + 
               (-116 + 296*s2 - 145*Power(s2,2) + 5*Power(s2,3))*
                Power(t2,3) - 4*(-9 + 11*s2 + Power(s2,2))*Power(t2,4) + 
               Power(s,3)*(-10 - 13*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s,2)*(5 + (-111 + 19*s2)*t2 + 
                  (-103 + 34*s2)*Power(t2,2) - (-10 + s2)*Power(t2,3)) + 
               s*(3 - (49 + 46*s2)*t2 + 
                  (195 - 6*s2 - 19*Power(s2,2))*Power(t2,2) - 
                  4*(9 - 12*s2 + Power(s2,2))*Power(t2,3) + 
                  4*(1 + s2)*Power(t2,4))) - 
            Power(t1,2)*(7 + 2*(89 + 11*s2)*t2 + 
               (114 - 116*s2 + 47*Power(s2,2) - 6*Power(s2,3))*
                Power(t2,2) + 
               (214 - 49*s2 + 5*Power(s2,2) - 15*Power(s2,3))*
                Power(t2,3) + 
               (-247 + 275*s2 - 29*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,4) + 2*(-1 + s2)*Power(t2,5) + 
               Power(s,3)*t2*
                (62 + 29*t2 - 18*Power(t2,2) + Power(t2,3)) + 
               s*(-6 - (265 + 6*s2)*t2 + 
                  (-235 - 18*s2 + 60*Power(s2,2))*Power(t2,2) + 
                  (-595 + 217*s2 + 99*Power(s2,2))*Power(t2,3) + 
                  (11 + 76*s2 - 7*Power(s2,2))*Power(t2,4) + 
                  2*Power(t2,5)) - 
               Power(s,2)*t2*(54 + 145*t2 - 37*Power(t2,2) + 
                  13*Power(t2,3) + 
                  s2*(3 - 27*t2 - 25*Power(t2,2) + Power(t2,3)))) + 
            t2*(Power(s,3)*t2*
                (130 + 121*t2 - 37*Power(t2,2) - 4*Power(t2,3)) - 
               2*s*(12 + (202 - 12*s2)*t2 + 
                  (62 + 203*s2 - 129*Power(s2,2))*Power(t2,2) + 
                  (74 - 59*s2 - 57*Power(s2,2))*Power(t2,3) + 
                  2*(37 - 70*s2 + 13*Power(s2,2))*Power(t2,4)) + 
               2*(10 + (58 + 43*s2 - 12*Power(s2,2))*t2 + 
                  (32 - 77*s2 + 90*Power(s2,2) - 15*Power(s2,3))*
                   Power(t2,2) + 
                  (82 - 265*s2 + 188*Power(s2,2) - 31*Power(s2,3))*
                   Power(t2,3) + 
                  (-142 + 191*s2 - 76*Power(s2,2) + 18*Power(s2,3))*
                   Power(t2,4) + 8*(-1 + s2)*Power(t2,5)) + 
               Power(s,2)*t2*(49 + 82*t2 + 9*Power(t2,2) - 
                  56*Power(t2,3) + 
                  s2*(-12 + t2 + 53*Power(t2,2) + 20*Power(t2,3)))) - 
            t1*t2*(Power(s,3)*
                (-60 - 67*t2 + 42*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s,2)*(18 + (-282 + 68*s2)*t2 + 
                  (-474 + 109*s2)*Power(t2,2) - 
                  4*(-17 + s2)*Power(t2,3) + 8*Power(t2,4)) + 
               2*s*(36 + (43 - 94*s2 + 6*Power(s2,2))*t2 + 
                  (-165 + 221*s2 - 71*Power(s2,2))*Power(t2,2) + 
                  (-33 + 6*s2 - 13*Power(s2,2))*Power(t2,3) + 
                  13*Power(t2,4)) + 
               2*(8 + 125*t2 - 56*Power(t2,2) - 401*Power(t2,3) + 
                  100*Power(t2,4) + 
                  Power(s2,3)*t2*(-2 + 7*t2 + 15*Power(t2,2)) - 
                  Power(s2,2)*t2*
                   (1 + 123*t2 + 221*Power(t2,2) + 4*Power(t2,3)) + 
                  s2*(-6 + 36*t2 + 247*Power(t2,2) + 554*Power(t2,3) - 
                     119*Power(t2,4)))))))/
     (Power(-1 + s1,2)*s1*(-1 + s2)*(-s + s2 - t1)*
       (Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*(-1 + s1 + t1 - t2)*
       (s1*t1 - t2)*(-1 + t2)*t2*Power(-s1 + t2,2)) - 
    (8*(-2 + Power(s1,8)*(Power(s2,3) + s2*Power(t1,2) + 2*Power(t1,3)) + 
         2*t2 - 5*s*t2 - 5*s2*t2 + 6*s*s2*t2 + 2*Power(t2,2) + 
         15*s*Power(t2,2) - 2*Power(s,2)*Power(t2,2) + 2*s2*Power(t2,2) - 
         13*s*s2*Power(t2,2) + 4*Power(s,2)*s2*Power(t2,2) - 
         3*Power(s2,2)*Power(t2,2) + 4*s*Power(s2,2)*Power(t2,2) + 
         5*Power(t2,3) - 9*s*Power(t2,3) + 9*Power(s,2)*Power(t2,3) - 
         Power(s,3)*Power(t2,3) + 16*s2*Power(t2,3) - 
         4*s*s2*Power(t2,3) - 10*Power(s,2)*s2*Power(t2,3) - 
         3*Power(s2,2)*Power(t2,3) - 13*s*Power(s2,2)*Power(t2,3) + 
         9*Power(t2,4) - 16*s*Power(t2,4) + 4*Power(s,3)*Power(t2,4) + 
         15*s2*Power(t2,4) - 37*s*s2*Power(t2,4) + 
         2*Power(s,2)*s2*Power(t2,4) - Power(s2,2)*Power(t2,4) - 
         8*s*Power(s2,2)*Power(t2,4) + 2*Power(s2,3)*Power(t2,4) - 
         7*Power(t2,5) - 10*s*Power(t2,5) + 13*Power(s,2)*Power(t2,5) - 
         Power(s,3)*Power(t2,5) - 3*s2*Power(t2,5) - 
         20*s*s2*Power(t2,5) + 2*Power(s,2)*s2*Power(t2,5) + 
         7*Power(s2,2)*Power(t2,5) - s*Power(s2,2)*Power(t2,5) - 
         9*Power(t2,6) + s*Power(t2,6) - s2*Power(t2,6) - 
         Power(-1 + s,2)*Power(t1,5)*(-1 + s + t2) + 
         Power(t1,4)*(-6 + 3*Power(s,3) + (11 - 4*s2)*t2 + 
            (-6 + 4*s2)*Power(t2,2) + 4*Power(t2,3) + 
            2*Power(s,2)*(-6 + (3 - 2*s2)*t2 + Power(t2,2)) + 
            s*(15 + (-17 + 8*s2)*t2 + (2 - 4*s2)*Power(t2,2))) + 
         Power(t1,3)*(14 + (-29 + 17*s2)*t2 + 
            5*(5 - 6*s2 + Power(s2,2))*Power(t2,2) + 
            (16 + 2*s2 - 3*Power(s2,2))*Power(t2,3) - 12*Power(t2,4) + 
            Power(s,3)*(-2 + t2 + 3*Power(t2,2)) - 
            Power(s,2)*(-15 + (6 - 12*s2)*t2 + (14 + s2)*Power(t2,2) + 
               2*Power(t2,3)) + 
            s*(-27 + (30 - 29*s2)*t2 + 
               (4 + 24*s2 - 5*Power(s2,2))*Power(t2,2) + 
               2*(-5 + 3*s2)*Power(t2,3))) + 
         Power(t1,2)*(-16 + (31 - 27*s2)*t2 + 
            (-30 + 50*s2 - 13*Power(s2,2))*Power(t2,2) + 
            (16 - 39*s2 + 26*Power(s2,2) - 2*Power(s2,3))*Power(t2,3) + 
            (-37 + 2*s2 + 2*Power(s2,2))*Power(t2,4) + 12*Power(t2,5) + 
            Power(s,3)*(t2 - 7*Power(t2,2) - 3*Power(t2,3)) + 
            Power(s,2)*(-6 + (5 - 14*s2)*t2 + 
               7*(3 + 2*s2)*Power(t2,2) + (11 + 8*s2)*Power(t2,3) + 
               2*Power(t2,4)) + 
            s*(21 + (-22 + 40*s2)*t2 + 
               (4 - 47*s2 + 13*Power(s2,2))*Power(t2,2) + 
               (2 - 21*s2 - 2*Power(s2,2))*Power(t2,3) + 
               (7 - 4*s2)*Power(t2,4))) + 
         Power(s1,7)*(Power(s2,3)*(3*t1 - 4*t2) + 
            Power(s2,2)*(-3 + 2*Power(t1,2) + t1*(2 + s - t2) + 
               3*s*t2) - s2*t1*
             (5*t1 - 5*Power(t1,2) + s*(3 + t1 - 3*t2) + 2*t2 + 3*t1*t2) \
+ Power(t1,2)*(-1 - 13*t1 + 6*Power(t1,2) - 6*t2 - 4*t1*t2 + 
               s*(-3 - 4*t1 + 4*t2))) + 
         t1*(9 + (-14 + 19*s2)*t2 + 
            (9 - 26*s2 + 11*Power(s2,2))*Power(t2,2) + 
            (-41 + 21*s2 - 20*Power(s2,2) + 2*Power(s2,3))*Power(t2,3) - 
            (-28 + 11*s2 + 8*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
            (29 + s2 - Power(s2,2))*Power(t2,5) - 4*Power(t2,6) + 
            Power(s,3)*Power(t2,2)*(2 - t2 + 2*Power(t2,2)) + 
            s*(-6 + (12 - 25*s2)*t2 + 
               (-25 + 40*s2 - 12*Power(s2,2))*Power(t2,2) + 
               (93 - 28*s2 + 21*Power(s2,2))*Power(t2,3) + 
               s2*(33 + 4*s2)*Power(t2,4) + 2*(-1 + s2)*Power(t2,5)) - 
            Power(s,2)*t2*(4 - 15*t2 + 19*Power(t2,2) + 19*Power(t2,3) + 
               Power(t2,4) + s2*
                (-6 + 22*t2 + 12*Power(t2,2) + 5*Power(t2,3)))) + 
         Power(s1,6)*(Power(s2,3)*
             (-4 + 3*Power(t1,2) + t1*(2 - 11*t2) - 2*t2 + 
               6*Power(t2,2)) + 
            Power(s2,2)*(6*Power(t1,3) + Power(t1,2)*(3*s - 8*t2) + 
               t1*(-11 - 12*t2 + 3*Power(t2,2) + s*(-4 + 9*t2)) + 
               t2*(10 + t2 - s*(4 + 9*t2))) + 
            s2*(3 + 9*Power(t1,4) + Power(t2,2) + 
               3*Power(s,2)*Power(t2,2) - 3*s*t2*(1 + t2) - 
               Power(t1,3)*(11 + 5*s + 12*t2) + 
               t1*(-4 + s + 12*t2 + 9*s*t2 - Power(s,2)*t2 + 
                  6*Power(t2,2) - 8*s*Power(t2,2)) + 
               Power(t1,2)*(-4 + t2 + 3*Power(t2,2) + s*(-5 + 17*t2))) + 
            t1*(6*Power(t1,4) + 2*t2*(1 + 3*t2) - 
               Power(t1,3)*(41 + 7*t2) + 
               Power(s,2)*(t1 + 2*Power(t1,2) - 5*t1*t2 + 
                  3*(-1 + t2)*t2) + 
               Power(t1,2)*(-13 + 7*t2 + 2*Power(t2,2)) + 
               t1*(5 + 42*t2 + 12*Power(t2,2)) + 
               s*(3 - 12*Power(t1,3) + 3*t2 - 8*Power(t2,2) + 
                  5*Power(t1,2)*(-1 + 4*t2) + 
                  t1*(3 + 9*t2 - 7*Power(t2,2))))) + 
         Power(s1,5)*(-1 + 2*Power(t1,6) - Power(t2,2) - 2*Power(t2,3) + 
            4*s*Power(t2,3) - 3*Power(s,2)*Power(t2,3) + 
            Power(s,3)*Power(t2,3) - 3*Power(t1,5)*(15 + 4*s + t2) + 
            Power(s2,3)*(-2 + Power(t1,3) + Power(t1,2)*(3 - 10*t2) + 
               16*t2 + 7*Power(t2,2) - 4*Power(t2,3) + 
               t1*t2*(-13 + 15*t2)) + 
            Power(t1,4)*(-22 + 6*Power(s,2) + 37*t2 + Power(t2,2) + 
               s*(7 + 25*t2)) + 
            Power(t1,3)*(19 + Power(s,2)*(4 - 19*t2) + 168*t2 + 
               4*Power(t2,2) - 4*s*(-9 - 4*t2 + 4*Power(t2,2))) - 
            t1*(-2 + 11*t2 + 45*Power(t2,2) + 
               2*Power(s,3)*Power(t2,2) + 12*Power(t2,3) + 
               Power(s,2)*t2*(-5 - 9*t2 + 4*Power(t2,2)) + 
               s*(2 + 13*t2 - 2*Power(t2,2) - 14*Power(t2,3))) + 
            Power(t1,2)*(2 + 23*t2 + Power(s,3)*t2 - 60*Power(t2,2) - 
               6*Power(t2,3) + 
               Power(s,2)*(7 - 16*t2 + 17*Power(t2,2)) + 
               s*(22 - 17*t2 - 46*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s2,2)*(12 + 6*Power(t1,4) + 
               Power(t1,3)*(-8 + 3*s - 16*t2) - 8*(-1 + s)*t2 + 
               (-8 + 3*s)*Power(t2,2) + (-3 + 9*s)*Power(t2,3) + 
               2*Power(t1,2)*
                (-10 - 6*t2 + 5*Power(t2,2) + s*(-4 + 5*t2)) + 
               t1*(-17 + 35*t2 + 28*Power(t2,2) - 3*Power(t2,3) + 
                  s*(-6 + 4*t2 - 27*Power(t2,2)))) + 
            s2*(7*Power(t1,5) - Power(t1,4)*(8 + 9*s + 13*t2) + 
               Power(t1,3)*(-14 - 6*t2 + 8*Power(t2,2) + 
                  s*(-5 + 37*t2)) - 
               t2*(8 + 7*t2 + 3*Power(t2,2) + 
                  Power(s,2)*t2*(5 + 6*t2) - 
                  s*(5 + 10*t2 + 8*Power(t2,2))) - 
               Power(t1,2)*(-4 - 45*t2 - 17*Power(t2,2) + Power(t2,3) + 
                  Power(s,2)*(2 + 4*t2) + s*(2 - 11*t2 + 30*Power(t2,2))\
) + t1*(13 + 24*t2 - 23*Power(t2,2) - 6*Power(t2,3) + 
                  7*Power(s,2)*t2*(-1 + 2*t2) + 
                  s*(20 - 37*t2 - 38*Power(t2,2) + 7*Power(t2,3))))) + 
         Power(s1,4)*(-((21 + 4*s)*Power(t1,6)) + 
            Power(t1,5)*(-20 + 6*Power(s,2) + 34*t2 + 9*s*(1 + t2)) + 
            Power(t1,4)*(10 + 206*t2 - 17*Power(t2,2) - 
               2*Power(s,2)*(1 + 10*t2) + s*(49 + 12*t2 - 6*Power(t2,2))\
) + t2*(2 + 6*t2 + 16*Power(t2,2) + 4*Power(t2,3) - 
               Power(s,3)*Power(t2,2)*(1 + t2) + 
               2*Power(s,2)*Power(t2,2)*(1 + 2*t2) + 
               s*(-1 + t2 - 7*Power(t2,2) - 7*Power(t2,3))) + 
            Power(t1,3)*(28 + 37*t2 - 185*Power(t2,2) + 2*Power(t2,3) + 
               Power(s,3)*(-2 + 3*t2) + 
               Power(s,2)*(6 - 16*t2 + 23*Power(t2,2)) + 
               s*(24 - 86*t2 - 57*Power(t2,2) + Power(t2,3))) + 
            Power(s2,3)*(3 + Power(t1,3)*(1 - 3*t2) + 8*t2 - 
               22*Power(t2,2) - 9*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*t2*(-9 + 11*t2) - 
               t1*(3 + 4*t2 - 29*Power(t2,2) + 9*Power(t2,3))) - 
            Power(t1,2)*(4 + 61*t2 + 239*Power(t2,2) - 31*Power(t2,3) + 
               Power(s,3)*t2*(-5 + 7*t2) + 
               s*(1 + 113*t2 - 53*Power(t2,2) - 38*Power(t2,3)) + 
               Power(s,2)*(6 - 20*t2 - 41*Power(t2,2) + 10*Power(t2,3))) \
+ t1*(-5 - 12*t2 - 4*Power(t2,2) + 75*Power(t2,3) + 6*Power(t2,4) + 
               Power(s,3)*Power(t2,2)*(-2 + 5*t2) + 
               Power(s,2)*t2*
                (2 - 12*t2 - 27*Power(t2,2) + Power(t2,3)) + 
               s*(-16 - 6*t2 + 53*Power(t2,2) + 25*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s2,2)*(6 + 2*Power(t1,5) + 
               Power(t1,4)*(-12 + s - 11*t2) + (-37 + 12*s)*t2 + 
               (-23 + 32*s)*Power(t2,2) + (-8 + 9*s)*Power(t2,3) - 
               3*(-1 + s)*Power(t2,4) + 
               2*Power(t1,3)*
                (1 - 3*t2 + 6*Power(t2,2) + s*(-4 + 3*t2)) + 
               Power(t1,2)*(-2 + 72*t2 + 33*Power(t2,2) - 
                  4*Power(t2,3) + s*(2 + 7*t2 - 27*Power(t2,2))) + 
               t1*(-3 + 96*t2 - 32*Power(t2,2) - 28*Power(t2,3) + 
                  Power(t2,4) + 
                  s*(4 + 18*t2 - 25*Power(t2,2) + 23*Power(t2,3)))) + 
            s2*(-12 + 2*Power(t1,6) + Power(t1,5)*(2 - 7*s - 4*t2) + 
               2*(-5 + 2*s)*t2 - 2*(1 - 9*s + Power(s,2))*Power(t2,2) + 
               (17 + 3*s + Power(s,2))*Power(t2,3) + 
               (3 - 7*s + 3*Power(s,2))*Power(t2,4) + 
               Power(t1,4)*(s*(3 + 31*t2) + 
                  3*(-4 - 5*t2 + Power(t2,2))) - 
               Power(t1,3)*(19 - 54*t2 + 7*Power(s,2)*t2 - 
                  27*Power(t2,2) + Power(t2,3) + 
                  s*(7 - 8*t2 + 36*Power(t2,2))) + 
               Power(t1,2)*(17 + 10*t2 - 46*Power(t2,2) - 
                  14*Power(t2,3) + Power(s,2)*t2*(-17 + 23*t2) + 
                  s*(9 - 47*t2 - 80*Power(t2,2) + 14*Power(t2,3))) + 
               t1*(28 - 45*t2 - 81*Power(t2,2) + 8*Power(t2,3) + 
                  2*Power(t2,4) + 
                  Power(s,2)*t2*(12 + 6*t2 - 19*Power(t2,2)) + 
                  s*(6 - 55*t2 + 95*Power(t2,2) + 59*Power(t2,3) - 
                     2*Power(t2,4))))) + 
         Power(s1,3)*(4 - 4*Power(t1,7) + 4*t2 + 4*s*t2 + 
            4*Power(t2,2) - 4*s*Power(t2,2) - 6*Power(t2,3) - 
            22*s*Power(t2,3) + 11*Power(s,2)*Power(t2,3) - 
            28*Power(t2,4) + s*Power(t2,4) + 7*Power(s,2)*Power(t2,4) - 
            Power(s,3)*Power(t2,4) - 2*Power(t2,5) + 3*s*Power(t2,5) - 
            Power(s,2)*Power(t2,5) + 
            Power(t1,6)*(-13 + 2*Power(s,2) + 12*t2) + 
            t1*(-13 + (21 + 51*s - 14*Power(s,2))*t2 + 
               (77 + 43*s - 46*Power(s,2) + 2*Power(s,3))*Power(t2,2) + 
               (135 - 105*s + 11*Power(s,2) + 5*Power(s,3))*
                Power(t2,3) - 
               (46 + 26*s - 17*Power(s,2) + 2*Power(s,3))*Power(t2,4)) + 
            Power(t1,5)*(-2*Power(s,2)*(2 + 3*t2) + s*(11 + 13*t2) - 
               2*(10 - 51*t2 + 6*Power(t2,2))) + 
            Power(t1,4)*(26 + 2*Power(s,3)*(-1 + t2) + 39*t2 - 
               145*Power(t2,2) + 4*Power(t2,3) + 
               Power(s,2)*(-10 - 3*t2 + 6*Power(t2,2)) - 
               s*(34 + 58*t2 + 23*Power(t2,2))) + 
            Power(t1,3)*(20 - 72*t2 - 339*Power(t2,2) + 
               80*Power(t2,3) + 
               Power(s,3)*(-2 + 7*t2 - 6*Power(t2,2)) + 
               Power(s,2)*(-32 + 41*t2 + 41*Power(t2,2) - 
                  2*Power(t2,3)) + 
               s*(-27 - 168*t2 + 75*Power(t2,2) + 11*Power(t2,3))) + 
            Power(t1,2)*(3*Power(s,3)*Power(t2,2)*(-3 + 2*t2) + 
               t2*(-58 + 21*t2 + 263*Power(t2,2) - 12*Power(t2,3)) - 
               Power(s,2)*(14 - 11*t2 + 33*Power(t2,2) + 
                  50*Power(t2,3)) - 
               s*(26 + 16*t2 - 170*Power(t2,2) - 27*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s2,3)*(2 - 11*t2 - 11*Power(t2,2) + 9*Power(t2,3) + 
               5*Power(t2,4) + 2*Power(t1,3)*(1 - t2 + Power(t2,2)) - 
               2*Power(t1,2)*t2*(1 - 6*t2 + 2*Power(t2,2)) + 
               t1*(-4 + 11*t2 + 9*Power(t2,2) - 27*Power(t2,3) + 
                  2*Power(t2,4))) + 
            s2*(-6 - 2*(-2 + s)*Power(t1,6) + (26 - 18*s)*t2 + 
               (29 - 21*s + 8*Power(s,2))*Power(t2,2) + 
               (35 - 65*s + 16*Power(s,2))*Power(t2,3) + 
               (-13 - 23*s + 7*Power(s,2))*Power(t2,4) + 
               (-1 + 2*s)*Power(t2,5) + 
               2*Power(t1,5)*(6 - 7*t2 + s*(2 + 4*t2)) + 
               Power(t1,4)*(2 + Power(s,2)*(1 - 4*t2) + 11*t2 + 
                  11*Power(t2,2) + s*(23 - 2*t2 - 10*Power(t2,2))) + 
               Power(t1,3)*(-11 + 65*t2 - 15*Power(t2,2) - 
                  6*Power(t2,3) + 
                  2*Power(s,2)*(8 - 9*t2 + 7*Power(t2,2)) + 
                  s*(35 - 26*t2 - 73*Power(t2,2) + 4*Power(t2,3))) + 
               Power(t1,2)*(-7 + 13*t2 - 58*Power(t2,2) - 
                  3*Power(t2,3) + Power(t2,4) + 
                  Power(s,2)*
                   (2 + 18*t2 + 22*Power(t2,2) - 16*Power(t2,3)) + 
                  s*(10 + 7*t2 + 108*Power(t2,2) + 80*Power(t2,3))) + 
               t1*(6 - 125*t2 + 53*Power(t2,2) + 90*Power(t2,3) + 
                  2*Power(t2,4) + 
                  2*Power(s,2)*t2*
                   (7 + t2 - 14*Power(t2,2) + 3*Power(t2,3)) + 
                  s*(-23 + 27*t2 + 167*Power(t2,2) - 16*Power(t2,3) - 
                     27*Power(t2,4)))) - 
            Power(s2,2)*(9 + (21 - 11*s)*t2 + (-20 + 34*s)*Power(t2,2) + 
               10*(-2 + 3*s)*Power(t2,3) + (-15 + 11*s)*Power(t2,4) + 
               Power(t2,5) + 2*Power(t1,5)*(3 + t2) + 
               Power(t1,4)*(1 - 2*s*(-2 + t2) - 2*t2 - 4*Power(t2,2)) + 
               Power(t1,3)*(3 - 41*t2 - 28*Power(t2,2) + 
                  2*Power(t2,3) + s*(8 - 9*t2 + 10*Power(t2,2))) - 
               Power(t1,2)*(1 - 8*t2 - 53*Power(t2,2) - 
                  24*Power(t2,3) + 
                  s*(1 + 16*t2 - 22*Power(t2,2) + 14*Power(t2,3))) + 
               t1*(-18 - 16*t2 + 185*Power(t2,2) + 4*Power(t2,3) - 
                  10*Power(t2,4) + 
                  s*(-5 + 31*t2 - 4*Power(t2,2) - 50*Power(t2,3) + 
                     6*Power(t2,4))))) + 
         Power(s1,2)*(2 - 5*t2 + 6*s*t2 - 13*Power(t2,2) - 
            11*s*Power(t2,2) + 2*Power(s,2)*Power(t2,2) - 
            31*Power(t2,3) + 16*s*Power(t2,3) + 
            4*Power(s,2)*Power(t2,3) + 2*Power(s,3)*Power(t2,3) - 
            23*Power(t2,4) + 45*s*Power(t2,4) - 
            21*Power(s,2)*Power(t2,4) + 2*Power(s,3)*Power(t2,4) + 
            18*Power(t2,5) + 4*s*Power(t2,5) - 
            7*Power(s,2)*Power(t2,5) + Power(s,3)*Power(t2,5) + 
            Power(t1,6)*(-5 + 2*s + Power(s,2) + 12*t2) - 
            Power(t1,5)*(-15 + s - 41*t2 + 6*s*t2 + 36*Power(t2,2) + 
               Power(s,2)*(1 + 8*t2)) + 
            Power(t1,4)*(-9 + 2*Power(s,3) + 62*t2 - 179*Power(t2,2) + 
               36*Power(t2,3) + s*(19 - 41*t2 - 7*Power(t2,2)) + 
               Power(s,2)*(1 + 19*t2 + 16*Power(t2,2))) + 
            Power(s2,3)*t2*(-5 + Power(t1,3)*(-3 + t2) + 15*t2 + 
               7*Power(t2,2) + 4*Power(t2,3) - Power(t2,4) + 
               Power(t1,2)*(-5 + 4*t2 - 6*Power(t2,2)) + 
               t1*(13 - 14*t2 - 9*Power(t2,2) + 9*Power(t2,3))) + 
            Power(t1,3)*(-6 - 98*t2 + 17*Power(t2,2) + 
               209*Power(t2,3) - 12*Power(t2,4) + 
               Power(s,3)*(4 - 5*Power(t2,2)) - 
               2*Power(s,2)*(-5 - 13*t2 + 19*Power(t2,2) + 
                  6*Power(t2,3)) + 
               s*(-21 + 156*t2 + 92*Power(t2,2) + 15*Power(t2,3))) + 
            Power(t1,2)*(6 - 54*t2 + 115*Power(t2,2) + 
               235*Power(t2,3) - 106*Power(t2,4) + 
               Power(s,3)*t2*(-6 - 4*t2 + 11*Power(t2,2)) + 
               s*(-18 + 108*t2 + 175*Power(t2,2) - 146*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s,2)*(11 + 73*t2 - 112*Power(t2,2) + 3*Power(t2,4))\
) + t1*(-3 + 42*t2 + 24*Power(t2,2) - 73*Power(t2,3) - 
               148*Power(t2,4) - 7*Power(s,3)*Power(t2,4) + 
               14*Power(t2,5) + 
               Power(s,2)*t2*
                (5 + 45*t2 + 58*Power(t2,2) + 27*Power(t2,3)) - 
               s*(-19 - 5*t2 + 93*Power(t2,2) + 68*Power(t2,3) - 
                  16*Power(t2,4) + Power(t2,5))) - 
            Power(s2,2)*(6 + 8*(-3 + s)*t2 + 2*Power(t1,5)*t2 + 
               22*(-1 + s)*Power(t2,2) - (37 + 22*s)*Power(t2,3) + 
               (1 + 2*s)*Power(t2,4) - 3*(-2 + s)*Power(t2,5) + 
               Power(t1,4)*(-3 + s - 23*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*(13 - (2 + 21*s)*t2 + 
                  (38 + 23*s)*Power(t2,2) + (12 - 26*s)*Power(t2,3) - 
                  3*Power(t2,4)) + 
               t1*(-17 + (62 + 13*s)*t2 + (9 - 38*s)*Power(t2,2) + 
                  (-129 + 10*s)*Power(t2,3) + (-13 + 25*s)*Power(t2,4)) \
+ Power(t1,3)*(1 - 15*t2 + 24*Power(t2,2) + 10*Power(t2,3) + 
                  s*(-1 - 18*t2 + 8*Power(t2,2)))) - 
            s2*(-9 + 2*(-1 + s)*Power(t1,6) + (-18 + 7*s)*t2 + 
               (-12 - 12*s + 5*Power(s,2))*Power(t2,2) + 
               (31 + 25*s + 30*Power(s,2))*Power(t2,3) + 
               (44 - 35*s + 4*Power(s,2))*Power(t2,4) + 
               (-4 - 13*s + 3*Power(s,2))*Power(t2,5) + 
               Power(t1,5)*(6 + Power(s,2) + 8*t2 - 5*s*(1 + 2*t2)) + 
               Power(t1,3)*(-21 + 19*t2 + 50*Power(t2,2) + 
                  7*Power(t2,3) + s*t2*(95 - 72*t2 - 22*Power(t2,2)) - 
                  3*Power(s,2)*(-3 + t2 + 4*Power(t2,2))) + 
               Power(t1,4)*(10 - 5*Power(s,2) + 45*t2 - 
                  26*Power(t2,2) + 2*s*(3 + 8*t2 + 12*Power(t2,2))) + 
               Power(t1,2)*(-11 - 70*t2 + 86*Power(t2,2) - 
                  33*Power(t2,3) - 2*Power(t2,4) + 
                  Power(s,2)*t2*(38 - 29*t2 + 31*Power(t2,2)) + 
                  s*(-10 + 124*t2 - 47*Power(t2,2) - 27*Power(t2,3) + 
                     6*Power(t2,4))) + 
               t1*(27 + 16*t2 - 134*Power(t2,2) + 15*Power(t2,3) + 
                  27*Power(t2,4) - Power(t2,5) + 
                  Power(s,2)*t2*
                   (17 + 59*t2 - 15*Power(t2,2) - 23*Power(t2,3)) + 
                  s*(7 - 91*t2 + 119*Power(t2,2) + 223*Power(t2,3) + 
                     45*Power(t2,4))))) + 
         s1*(-3 + Power(-1 + s,2)*Power(t1,6) - 5*t2 - 4*s*t2 - 
            10*Power(t2,2) - 3*s*Power(t2,2) + 40*s*Power(t2,3) - 
            17*Power(s,2)*Power(t2,3) - Power(s,3)*Power(t2,3) + 
            36*Power(t2,4) - 11*s*Power(t2,4) - 
            6*Power(s,2)*Power(t2,4) - 6*Power(s,3)*Power(t2,4) + 
            27*Power(t2,5) - 11*s*Power(t2,5) - 
            3*Power(s,2)*Power(t2,5) + 2*Power(s,3)*Power(t2,5) - 
            5*Power(t2,6) + s*Power(t2,6) + 
            Power(t1,5)*(-9 + Power(s,3) + s*(14 - 4*t2) + 11*t2 - 
               12*Power(t2,2) - 3*Power(s,2)*(2 + t2)) + 
            Power(s2,3)*Power(t2,2)*
             (3 + Power(t1,3) + Power(t1,2)*(7 - 2*t2) - 9*t2 - 
               2*Power(t2,2) - 3*Power(t2,3) + 
               t1*(-11 + 7*t2 + 4*Power(t2,2))) + 
            Power(t1,4)*(18 - 5*Power(s,3) - 40*t2 - 44*Power(t2,2) + 
               36*Power(t2,3) + Power(s,2)*(9 + 6*t2 + 8*Power(t2,2)) + 
               s*(-18 + 3*t2 + 16*Power(t2,2))) - 
            Power(t1,3)*(7 - 41*t2 + 60*Power(t2,2) - 135*Power(t2,3) + 
               36*Power(t2,4) + Power(s,3)*(-2 + 3*t2) + 
               s*(-4 + 56*t2 - 34*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s,2)*(11 - 14*t2 + 26*Power(t2,2) + 12*Power(t2,3))) \
- Power(t1,2)*(12 + 11*t2 - 125*Power(t2,2) + 65*Power(t2,3) + 
               129*Power(t2,4) - 12*Power(t2,5) + 
               Power(s,3)*t2*(1 - 15*t2 + 2*Power(t2,2)) - 
               s*t2*(109 - 239*t2 - 46*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(-7 + 70*t2 + 6*Power(t2,2) - 63*Power(t2,3) - 
                  7*Power(t2,4))) - 
            t1*(-12 - 4*t2 - Power(t2,2) + 58*Power(t2,3) + 
               50*Power(t2,4) - 55*Power(t2,5) + 
               Power(s,3)*Power(t2,3)*(1 + t2) + 
               Power(s,2)*t2*
                (-9 + 32*t2 - 37*Power(t2,2) + 34*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(-2 + 48*t2 + 36*Power(t2,2) + 19*Power(t2,3) - 
                  78*Power(t2,4) + 3*Power(t2,5))) + 
            Power(s2,2)*t2*(10 - 17*t2 - 9*Power(t2,2) - 
               40*Power(t2,3) - 4*Power(t2,4) + 
               Power(t1,4)*(-2 + 5*t2) - 
               Power(t1,3)*(8 + 37*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(32 - 27*t2 + 28*Power(t2,2) + 
                  7*Power(t2,3)) - 
               t1*(32 - 76*t2 - 16*Power(t2,2) + 18*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(-6 + 2*Power(t1,4) - Power(t1,3)*(-3 + t2) + 24*t2 + 
                  17*Power(t2,2) + 2*Power(t2,3) + 8*Power(t2,4) + 
                  Power(t1,2)*(-16 - 23*t2 + 8*Power(t2,2)) - 
                  t1*(-17 + 18*t2 + 12*Power(t2,2) + 9*Power(t2,3)))) + 
            s2*(6 + (-15 + 13*s)*t2 + 
               (-13 + s - 3*Power(s,2))*Power(t2,2) + 
               (-44 + 47*s + 23*Power(s,2))*Power(t2,3) + 
               (15 + 74*s + 6*Power(s,2))*Power(t2,4) + 
               (16 + 7*s - 7*Power(s,2))*Power(t2,5) - Power(t2,6) + 
               (-1 + s)*Power(t1,5)*(-1 + s + 6*t2) + 
               Power(t1,4)*(1 + 30*t2 + 2*Power(s,2)*t2 + 
                  2*Power(t2,2) - s*(1 + 23*t2 + 14*Power(t2,2))) + 
               Power(t1,3)*(-15 - 7*t2 + 72*Power(t2,2) - 
                  16*Power(t2,3) + 
                  Power(s,2)*(-1 - 22*t2 + Power(t2,2)) + 
                  s*(14 + 29*t2 + 21*Power(t2,2) + 20*Power(t2,3))) - 
               Power(t1,2)*(-29 + 67*t2 + 10*Power(t2,2) - 
                  54*Power(t2,3) + Power(t2,4) + 
                  Power(s,2)*t2*(-37 + 28*t2 + 3*Power(t2,2)) + 
                  s*(17 + 4*t2 - 166*Power(t2,2) + 108*Power(t2,3) + 
                     14*Power(t2,4))) + 
               t1*(-22 + 65*t2 - 51*Power(t2,2) - 18*Power(t2,3) - 
                  4*Power(t2,4) + 2*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-7 + 77*t2 - 4*Power(t2,2) + 6*Power(t2,3)) + 
                  s*(6 - 21*t2 - 33*Power(t2,2) + 61*Power(t2,3) + 
                     41*Power(t2,4) + 2*Power(t2,5))))))*
       B1(1 - s1 - t1 + t2,s1,t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,3)*
       (-1 + t2)) + (8*(-2*Power(s1,11)*
          (Power(s2,3) + 4*s2*Power(t1,2) + Power(t1,2)*(-6 + s + 3*t1)) \
- (-2 + s)*(-1 + t1 - t2)*Power(t2,4)*
          (Power(s,2)*(-2 + t1 - t2) + s*(3 - 3*t1 + (-3 + s2)*t2) + 
            2*(t1 + t2 - Power(t2,2))) + 
         s1*Power(t2,3)*(Power(s,3)*
             (8 + 3*Power(t1,3) + 24*t2 + 14*Power(t2,2) - 
               Power(t2,3) - Power(t1,2)*(5 + 3*t2) + 
               t1*(-6 - 13*t2 + Power(t2,2))) + 
            Power(s,2)*(-22 + (-56 + s2)*t2 + (1 + 5*s2)*Power(t2,2) + 
               (27 + 5*s2)*Power(t2,3) + Power(t1,3)*(-13 + 2*t2) + 
               Power(t1,2)*(16 + (-8 + 7*s2)*t2) - 
               t1*(-19 + 8*(-6 + s2)*t2 + 2*(5 + 6*s2)*Power(t2,2) + 
                  2*Power(t2,3))) - 
            2*s*(-5 + 3*Power(t1,3)*(-3 + t2) - 4*(-1 + s2)*t2 + 
               (36 - s2 + 2*Power(s2,2))*Power(t2,2) + 
               (15 + 4*s2 + 2*Power(s2,2))*Power(t2,3) - 
               4*Power(t2,4) + 
               Power(t1,2)*(8 + (-29 + 10*s2)*t2 + 
                  (1 + s2)*Power(t2,2)) - 
               t1*(-6 + (-28 + 6*s2)*t2 + 
                  (-11 + 15*s2 + 2*Power(s2,2))*Power(t2,2) + 
                  s2*Power(t2,3))) + 
            2*(3 + 2*Power(t1,3)*(-2 + t2) - 4*(-6 + s2)*t2 + 
               (10 - s2 + 3*Power(s2,2))*Power(t2,2) + 
               (-28 + 6*s2 + 3*Power(s2,2))*Power(t2,3) + 
               (-13 + 3*s2)*Power(t2,4) + 
               Power(t1,2)*(3 + (-30 + 7*s2)*t2 + 
                  (3 + 2*s2)*Power(t2,2)) - 
               t1*(2 + (-14 + 3*s2)*t2 + 
                  (-40 + 12*s2 + 3*Power(s2,2))*Power(t2,2) + 
                  (-8 + 5*s2)*Power(t2,3)))) - 
         Power(s1,2)*Power(t2,2)*
          (Power(t1,4)*(-1 + 6*t2) + 
            Power(t1,3)*(-9 + (-119 + 25*s2)*t2 + 
               4*(2 + 3*s2)*Power(t2,2)) + 
            Power(t1,2)*(3 + (-60 + 46*s2)*t2 + 
               (55 - 44*s2 + Power(s2,2))*Power(t2,2) + 
               (68 - 40*s2 + 6*Power(s2,2))*Power(t2,3)) - 
            t1*(-1 + (-133 + 79*s2)*t2 + 
               (-225 + 38*s2 + 33*Power(s2,2))*Power(t2,2) + 
               (-103 + 27*s2 + 11*Power(s2,2) + 5*Power(s2,3))*
                Power(t2,3) + (78 - 40*s2 + 4*Power(s2,2))*Power(t2,4)) \
+ 2*(3 + 4*(10 + s2)*t2 + (91 - 20*s2 + 16*Power(s2,2))*Power(t2,2) + 
               (-82 + 42*s2 + 12*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,3) + 
               (-116 + 56*s2 - 11*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,4) + (2 - 10*s2)*Power(t2,5)) + 
            Power(s,3)*(-6 + 27*t2 + 70*Power(t2,2) + 17*Power(t2,3) - 
               7*Power(t2,4) + Power(t1,3)*(6 + 7*t2) - 
               Power(t1,2)*(18 + 39*t2 + 5*Power(t2,2)) + 
               t1*(15 + 10*t2 - 4*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s,2)*(-9 + 2*Power(t1,4)*(-1 + t2) + 
               9*(-13 + s2)*t2 + (-115 + 28*s2)*Power(t2,2) + 
               2*(55 + 12*s2)*Power(t2,3) + (61 + 15*s2)*Power(t2,4) + 
               Power(t1,3)*(-34 - 32*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(86 + (115 + 19*s2)*t2 + 
                  3*(-17 + s2)*Power(t2,2) - 6*Power(t2,3)) - 
               t1*(41 + 3*(9 + 7*s2)*t2 + (-52 + 41*s2)*Power(t2,2) + 
                  2*(-2 + 9*s2)*Power(t2,3) + 4*Power(t2,4))) + 
            s*(Power(t1,4)*(3 - 8*t2) + 
               Power(t1,3)*(54 + (87 - 12*s2)*t2 - 
                  8*(2 + s2)*Power(t2,2)) + 
               Power(t1,2)*(-83 - 2*(25 + 31*s2)*t2 + 
                  (121 + 18*s2 - 5*Power(s2,2))*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(10 + 3*(-35 + 32*s2)*t2 + 
                  4*(-59 + 26*s2 + 6*Power(s2,2))*Power(t2,2) + 
                  (-127 + 62*s2 + 25*Power(s2,2))*Power(t2,3) + 
                  2*(7 + 4*s2)*Power(t2,4)) - 
               2*(-8 + (-24 + 11*s2)*t2 + 
                  (92 - 11*s2 + 12*Power(s2,2))*Power(t2,2) + 
                  (120 + 5*s2 + 27*Power(s2,2))*Power(t2,3) + 
                  (-10 + 35*s2 + 8*Power(s2,2))*Power(t2,4) - 
                  3*Power(t2,5)))) + 
         Power(s1,3)*t2*(10 + 
            (29 - 24*Power(s,3) + s*(94 - 40*s2) + 
               7*Power(s,2)*(-7 + s2) + 44*s2)*t2 + 
            (243 + 33*Power(s,3) - 23*s2 + 72*Power(s2,2) + 
               Power(s,2)*(-216 + 49*s2) - 
               3*s*(3 + 8*s2 + 16*Power(s2,2)))*Power(t2,2) + 
            (-59 + 83*Power(s,3) + 130*s2 + 57*Power(s2,2) + 
               26*Power(s2,3) + Power(s,2)*(-2 + 36*s2) - 
               2*s*(220 - 40*s2 + 79*Power(s2,2)))*Power(t2,3) + 
            (-601 + 5*Power(s,3) + 359*s2 - 69*Power(s2,2) + 
               45*Power(s2,3) + Power(s,2)*(204 + 22*s2) - 
               s*(59 + 220*s2 + 81*Power(s2,2)))*Power(t2,4) - 
            (7*Power(s,3) - 3*Power(s,2)*(13 + 5*s2) + 
               2*s*(-43 + 52*s2 + 3*Power(s2,2)) + 
               2*(40 + 43*s2 - 25*Power(s2,2) + Power(s2,3)))*
             Power(t2,5) - 2*(-3 + s2)*Power(t2,6) - 
            (-1 + s)*Power(t1,5)*(1 + 2*t2) + 
            Power(t1,4)*(10 + (-76 + 25*s2)*t2 + 
               2*(5 + 4*s2)*Power(t2,2) + 
               Power(s,2)*(-8 - 2*t2 + 8*Power(t2,2)) + 
               s*(25 + (11 - 6*s2)*t2 - 2*(11 + 3*s2)*Power(t2,2))) + 
            Power(t1,3)*(-52 + 4*(-37 + 7*s2)*t2 + 
               (-157 + 36*s2 + Power(s2,2))*Power(t2,2) + 
               2*(37 - 21*s2 + 7*Power(s2,2))*Power(t2,3) + 
               Power(s,3)*t2*(11 + 5*t2) + 
               Power(s,2)*(12 - (49 + 4*s2)*t2 - 
                  4*(10 + s2)*Power(t2,2) + 6*Power(t2,3)) + 
               s*(29 + 106*t2 + 
                  (181 - 22*s2 - 3*Power(s2,2))*Power(t2,2) - 
                  2*(1 + 9*s2)*Power(t2,3))) + 
            Power(t1,2)*(64 + 5*(5 + 9*s2)*t2 + 
               (223 - 99*s2 + 6*Power(s2,2))*Power(t2,2) - 
               (-450 + 211*s2 + 18*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,3) + 
               (-64 + 54*s2 - 4*Power(s2,2))*Power(t2,4) + 
               Power(s,3)*(6 - 59*t2 - 63*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(s,2)*(14 + (226 + 28*s2)*t2 + 
                  (119 + 26*s2)*Power(t2,2) - 
                  3*(41 + 5*s2)*Power(t2,3) - 12*Power(t2,4)) + 
               s*(-115 - 3*(97 + 22*s2)*t2 + 
                  (-110 + 34*s2 - 17*Power(s2,2))*Power(t2,2) + 
                  (23 + 148*s2 + 27*Power(s2,2))*Power(t2,3) + 
                  18*(1 + s2)*Power(t2,4))) - 
            t1*(33 + 2*(-104 + 71*s2)*t2 + 
               (-581 + 142*s2 + 79*Power(s2,2))*Power(t2,2) + 
               2*(-15 - 89*s2 + 24*Power(s2,2) + 9*Power(s2,3))*
                Power(t2,3) - 
               4*(-129 + 99*s2 - 27*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,4) + (-4 + 46*s2 + 4*Power(s2,2))*Power(t2,5) + 
               Power(s,3)*(12 - 45*t2 - 86*Power(t2,2) - 
                  7*Power(t2,3) + 5*Power(t2,4)) + 
               Power(s,2)*(18 + (239 + 10*s2)*t2 + 
                  (218 + 38*s2)*Power(t2,2) + 
                  2*(-93 + 13*s2)*Power(t2,3) - 
                  2*(43 + 2*s2)*Power(t2,4) + 2*Power(t2,5)) - 
               2*s*(31 + (25 + 56*s2)*t2 + 
                  (-250 + 151*s2 + 24*Power(s2,2))*Power(t2,2) + 
                  (-360 + 87*s2 + 17*Power(s2,2))*Power(t2,3) - 
                  (41 + 18*s2 + 3*Power(s2,2))*Power(t2,4) + 
                  (4 + 3*s2)*Power(t2,5)))) + 
         Power(s1,10)*(Power(s2,3)*(6 - 3*t1 + 10*t2) - 
            Power(s2,2)*(-6 + 11*Power(t1,2) + 2*t1*(2 + s - t2) + 
               6*s*t2) + s2*t1*
             (48*t1 - 21*Power(t1,2) + 6*s*(1 + t1 - t2) + 16*t2 + 
               32*t1*t2) + t1*
             (-13*Power(t1,3) + 4*(-6 + s)*t2 + 
               2*Power(t1,2)*(28 + s + 9*t2) + 
               2*t1*(-16 + Power(s,2) - 17*t2 - s*(2 + t2)))) + 
         Power(s1,9)*(4*Power(t1,2) - 125*Power(t1,3) + 85*Power(t1,4) - 
            7*Power(t1,5) + 64*t1*t2 - 24*Power(t1,2)*t2 - 
            132*Power(t1,3)*t2 + 23*Power(t1,4)*t2 + 12*Power(t2,2) + 
            86*t1*Power(t2,2) + 34*Power(t1,2)*Power(t2,2) - 
            18*Power(t1,3)*Power(t2,2) + 
            2*Power(s,2)*t1*(2*Power(t1,2) - t1*(-5 + t2) + t2 - 
               3*Power(t2,2)) - 
            Power(s2,3)*(2 + Power(t1,2) + 29*t2 + 20*Power(t2,2) - 
               2*t1*(1 + 8*t2)) + 
            Power(s2,2)*(-18 - 19*Power(t1,3) + 26*(-1 + s)*t2 + 
               (-2 + 24*s)*Power(t2,2) + 
               Power(t1,2)*(41 - 3*s + 51*t2) + 
               t1*(25 - 7*s*(-2 + t2) + 36*t2 - 8*Power(t2,2))) + 
            s*(11*Power(t1,4) + Power(t1,3)*(4 - 35*t2) - 
               2*Power(t2,2) + 2*t1*(-3 + 7*t2 + 2*Power(t2,2)) + 
               2*Power(t1,2)*(15 + 30*t2 + 8*Power(t2,2))) - 
            s2*(6 + 21*Power(t1,4) + 8*Power(t2,2) + 
               6*Power(s,2)*Power(t2,2) - 6*s*t2*(1 + t2) - 
               2*Power(t1,3)*(40 + 5*s + 33*t2) + 
               3*Power(t1,2)*
                (26 + 43*t2 + 16*Power(t2,2) + 2*s*(5 + 8*t2)) - 
               2*t1*(4 - 50*t2 + Power(s,2)*t2 - 32*Power(t2,2) + 
                  s*(-10 - 7*t2 + 11*Power(t2,2))))) + 
         Power(s1,8)*(Power(t1,5)*(45 + 7*s + 3*t2) + 
            Power(t1,4)*(-163 + 2*Power(s,2) + s*(6 - 40*t2) - 131*t2 - 
               14*Power(t2,2)) - 
            2*(-1 + (16 + 5*s - Power(s,2))*Power(t2,2) + 
               (23 + s - 3*Power(s,2) + Power(s,3))*Power(t2,3)) + 
            Power(s2,3)*(-6 + 10*Power(t1,2)*(-1 + t2) + 10*t2 + 
               54*Power(t2,2) + 20*Power(t2,3) + 
               t1*(3 - 13*t2 - 34*Power(t2,2))) + 
            Power(t1,3)*(Power(s,2)*(23 + 11*t2) + 
               s*(-4 + 105*t2 + 55*Power(t2,2)) + 
               2*(48 + 55*t2 + 61*Power(t2,2) + 3*Power(t2,3))) + 
            2*t1*(-2 - 3*t2 - 60*Power(t2,2) + 
               2*Power(s,3)*Power(t2,2) - 61*Power(t2,3) + 
               Power(s,2)*t2*(-26 + 9*t2 + 7*Power(t2,2)) - 
               s*(-11 + 29*t2 + 74*Power(t2,2) + 16*Power(t2,3))) + 
            Power(t1,2)*(45 + 335*t2 + 218*Power(t2,2) - 
               18*Power(t2,3) + Power(s,3)*(6 + 4*t2) - 
               Power(s,2)*(20 + 40*t2 + 21*Power(t2,2)) - 
               s*(34 + 194*t2 + 65*Power(t2,2) + 18*Power(t2,3))) + 
            s2*(-8*Power(t1,5) + Power(t1,4)*(42 + 4*s + 50*t2) + 
               2*(9 + (11 - 14*s)*t2 + 
                  2*(13 - 10*s + 7*Power(s,2))*Power(t2,2) + 
                  (16 - 11*s + 9*Power(s,2))*Power(t2,3)) - 
               Power(t1,3)*(130 + 169*t2 + 73*Power(t2,2) + 
                  s*(66 + 60*t2)) + 
               Power(t1,2)*(-20 + 3*Power(s,2)*(-7 + t2) + 86*t2 + 
                  96*Power(t2,2) + 32*Power(t2,3) + 
                  2*s*(-5 + 93*t2 + 54*Power(t2,2))) + 
               t1*(-41 + 128*t2 + Power(s,2)*(8 - 21*t2)*t2 + 
                  337*Power(t2,2) + 96*Power(t2,3) + 
                  s*(-4 + 168*t2 + 52*Power(t2,2) - 30*Power(t2,3)))) - 
            Power(s2,2)*(-6 + 8*Power(t1,4) + 
               Power(t1,3)*(-69 + s - 63*t2) + (-71 + 38*s)*t2 + 
               5*(-7 + 19*s)*Power(t2,2) + 4*(-2 + 9*s)*Power(t2,3) + 
               Power(t1,2)*(6*s*(-7 + t2) + t2*(137 + 87*t2)) + 
               t1*(s*(22 + 19*t2 - 45*Power(t2,2)) + 
                  2*(8 + 94*t2 + 59*Power(t2,2) - 6*Power(t2,3))))) + 
         Power(s1,7)*(2*(-3 + (-3 + s)*t2 + 
               (1 + 26*s + 6*Power(s,2))*Power(t2,2) + 
               (44 + 43*s - 17*Power(s,2) + 4*Power(s,3))*Power(t2,3) + 
               (35 + 8*s - 7*Power(s,2) + 2*Power(s,3))*Power(t2,4)) - 
            Power(t1,5)*(86 + 31*t2 + 2*Power(t2,2) + s*(16 + 3*t2)) + 
            Power(t1,4)*(2*Power(s,2)*(-1 + 8*t2) + 
               s*(-70 + 65*t2 + 29*Power(t2,2)) + 
               2*(65 + 69*t2 + 44*Power(t2,2) + 2*Power(t2,3))) + 
            Power(t1,3)*(67 + 179*t2 + 27*Power(t2,2) - 
               44*Power(t2,3) + Power(s,3)*(1 + 5*t2) - 
               Power(s,2)*(80 + 86*t2 + 31*Power(t2,2)) - 
               2*s*(3 + 124*t2 + 52*Power(t2,2) + 13*Power(t2,3))) - 
            Power(s2,3)*(-4 - 29*t2 + 16*Power(t2,2) + 46*Power(t2,3) + 
               10*Power(t2,4) + 
               Power(t1,2)*(1 - 32*t2 + 24*Power(t2,2)) - 
               2*t1*(-1 + 6*t2 + 10*Power(t2,2) + 18*Power(t2,3))) + 
            Power(t1,2)*(23 - 489*t2 - 718*Power(t2,2) - 
               280*Power(t2,3) + 10*Power(t2,4) + 
               Power(s,3)*(-24 - 17*t2 + 4*Power(t2,2)) + 
               Power(s,2)*(-3 - 34*t2 + 106*Power(t2,2) + 
                  35*Power(t2,3)) + 
               s*(75 + 50*t2 + 31*Power(t2,2) - 3*Power(t2,3) + 
                  6*Power(t2,4))) + 
            t1*(19 - 76*t2 - 303*Power(t2,2) + 12*Power(t2,3) + 
               90*Power(t2,4) - 
               Power(s,3)*t2*(12 + 20*t2 + 11*Power(t2,2)) + 
               Power(s,2)*t2*
                (80 + 131*t2 - 16*Power(t2,2) - 10*Power(t2,3)) + 
               s*(-10 - 33*t2 + 420*Power(t2,2) + 265*Power(t2,3) + 
                  36*Power(t2,4))) + 
            Power(s2,2)*(18 + 10*(-2 + s)*t2 + 
               (-63 + 130*s)*Power(t2,2) + (7 + 123*s)*Power(t2,3) + 
               12*(-1 + 2*s)*Power(t2,4) + 8*Power(t1,4)*(1 + 3*t2) + 
               Power(t1,3)*(-25 - 199*t2 - 75*Power(t2,2) + 
                  s*(10 + 3*t2)) + 
               Power(t1,2)*(9 - 149*t2 + 155*Power(t2,2) + 
                  65*Power(t2,3) + s*(-41 - 115*t2 + 36*Power(t2,2))) + 
               t1*(-21 + 63*t2 + 503*Power(t2,2) + 178*Power(t2,3) - 
                  8*Power(t2,4) + 
                  s*(10 + 3*t2 - 30*Power(t2,2) - 73*Power(t2,3)))) + 
            s2*(-6 + (-55 + 52*s)*t2 - 
               2*(55 - 33*s + 28*Power(s,2))*Power(t2,2) + 
               (-187 + 68*s - 75*Power(s,2))*Power(t2,3) - 
               6*(8 - 5*s + 3*Power(s,2))*Power(t2,4) + 
               8*Power(t1,5)*(3 + t2) - 
               Power(t1,4)*(48 + 64*t2 + 37*Power(t2,2) + 
                  s*(-2 + 30*t2)) + 
               Power(t1,3)*(-18 + 280*t2 + 76*Power(t2,2) + 
                  32*Power(t2,3) - 4*Power(s,2)*(1 + t2) + 
                  2*s*(33 + 127*t2 + 50*Power(t2,2))) + 
               Power(t1,2)*(29 + 444*t2 + 101*Power(t2,2) + 
                  9*Power(t2,3) - 8*Power(t2,4) + 
                  Power(s,2)*(49 + 62*t2 - 18*Power(t2,2)) - 
                  2*s*(4 - 58*t2 + 163*Power(t2,2) + 48*Power(t2,3))) + 
               2*t1*(13 + 102*t2 - 190*Power(t2,2) - 207*Power(t2,3) - 
                  32*Power(t2,4) + 
                  Power(s,2)*t2*(-3 + 10*t2 + 24*Power(t2,2)) + 
                  s*(28 - 39*t2 - 301*Power(t2,2) - 66*Power(t2,3) + 
                     9*Power(t2,4))))) + 
         Power(s1,4)*(-4 + (-17 - 40*s2 + 2*s*(-5 + 6*s2))*t2 + 
            (-71 + 36*Power(s,3) + Power(s,2)*(89 - 27*s2) - 60*s2 - 
               84*Power(s2,2) + s*(-143 + 56*s2 + 40*Power(s2,2)))*
             Power(t2,2) - (75 + 17*Power(s,3) + 82*s2 + 
               105*Power(s2,2) + 46*Power(s2,3) + 
               4*Power(s,2)*(-31 + 8*s2) - 
               2*s*(81 - 65*s2 + 95*Power(s2,2)))*Power(t2,3) + 
            (631 - 49*Power(s,3) - 428*s2 + 77*Power(s2,2) - 
               98*Power(s2,3) + Power(s,2)*(-137 + 44*s2) + 
               s*(73 + 186*s2 + 137*Power(s2,2)))*Power(t2,4) + 
            (250 - 2*Power(s,3) + 208*s2 - 192*Power(s2,2) + 
               4*Power(s2,3) - 3*Power(s,2)*(30 + s2) + 
               2*s*(-127 + 163*s2 + Power(s2,2)))*Power(t2,5) + 
            2*(-5 + Power(s,3) + 19*s2 - 8*Power(s2,2) - Power(s2,3) - 
               Power(s,2)*(1 + 3*s2) + s*(-18 + 8*s2 + 3*Power(s2,2)))*
             Power(t2,6) - 4*Power(t2,7) + 
            Power(t1,5)*(-7 + (19 - 8*s2)*t2 - 4*Power(t2,2) + 
               s*(-5 + 6*t2 + 6*Power(t2,2))) - 
            Power(t1,4)*(-19 + (-51 + 26*s2)*t2 + 
               (-200 + 86*s2)*Power(t2,2) + 
               2*(13 - 6*s2 + 4*Power(s2,2))*Power(t2,3) + 
               2*Power(s,2)*
                (3 - 7*t2 + 6*Power(t2,2) + 5*Power(t2,3)) - 
               2*s*(-5 - 16*t2 + 2*(-8 + 9*s2)*Power(t2,2) + 
                  (7 + 8*s2)*Power(t2,3))) - 
            Power(t1,3)*(9 - (169 + 3*s2)*t2 + 
               (-87 - 59*s2 + 3*Power(s2,2))*Power(t2,2) + 
               (311 - 190*s2 + 17*Power(s2,2))*Power(t2,3) + 
               2*(-1 + 8*s2 + 4*Power(s2,2))*Power(t2,4) + 
               Power(s,3)*(3 - t2 + 3*Power(t2,2) + Power(t2,3)) - 
               Power(s,2)*(9 + (-31 + 4*s2)*t2 + 
                  (-2 + 24*s2)*Power(t2,2) + (66 + 4*s2)*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s*(-19 + (-7 + 20*s2)*t2 + 
                  (46 + 70*s2 - 9*Power(s2,2))*Power(t2,2) + 
                  2*(95 + 6*s2 + 5*Power(s2,2))*Power(t2,3) + 
                  20*Power(t2,4))) + 
            Power(t1,2)*(-11 - 4*(61 + 6*s2)*t2 + 
               (-443 + 80*s2 - 12*Power(s2,2))*Power(t2,2) + 
               (-663 + 202*s2 + 81*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,3) + 
               (205 - 380*s2 + 161*Power(s2,2) - 2*Power(s2,3))*
                Power(t2,4) + 
               2*(-6 + 20*s2 + 5*Power(s2,2))*Power(t2,5) + 
               Power(s,3)*(6 - 7*t2 + 47*Power(t2,2) + 
                  23*Power(t2,3) - 12*Power(t2,4)) + 
               Power(s,2)*(-3 + (22 - 23*s2)*t2 - 
                  5*(5 + 18*s2)*Power(t2,2) + 
                  (34 - 4*s2)*Power(t2,3) + (74 + 23*s2)*Power(t2,4) + 
                  6*Power(t2,5)) + 
               s*(8 + (294 + 50*s2)*t2 + 
                  (464 - 10*s2 + 21*Power(s2,2))*Power(t2,2) + 
                  (468 - 400*s2 - 40*Power(s2,2))*Power(t2,3) + 
                  (95 - 200*s2 - 11*Power(s2,2))*Power(t2,4) - 
                  4*(1 + 4*s2)*Power(t2,5))) + 
            t1*(12 + (2 + 95*s2)*t2 + 
               (-649 + 227*s2 + 99*Power(s2,2))*Power(t2,2) + 
               (-175 - 412*s2 + 99*Power(s2,2) + 26*Power(s2,3))*
                Power(t2,3) + 
               (1327 - 1151*s2 + 285*Power(s2,2) - 45*Power(s2,3))*
                Power(t2,4) + 
               (245 + 129*s2 - 77*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,5) - 6*Power(t2,6) + 
               Power(s,3)*t2*
                (48 - 43*t2 - 109*Power(t2,2) + 22*Power(t2,3) + 
                  11*Power(t2,4)) + 
               s*(-12 - (247 + 42*s2)*t2 + 
                  (81 - 318*s2 - 40*Power(s2,2))*Power(t2,2) + 
                  2*(571 - 185*s2 + 19*Power(s2,2))*Power(t2,3) + 
                  (521 + 270*s2 + 81*Power(s2,2))*Power(t2,4) + 
                  (-89 + 188*s2 + 3*Power(s2,2))*Power(t2,5) + 
                  4*Power(t2,6)) - 
               Power(s,2)*t2*(-90 - 460*t2 - 68*Power(t2,2) + 
                  424*Power(t2,3) + 86*Power(t2,4) + 
                  s2*(2 + 11*t2 + 12*Power(t2,2) + 44*Power(t2,3) + 
                     21*Power(t2,4))))) + 
         Power(s1,5)*(6 + 22*s*t2 + 27*Power(t2,2) + 60*s*Power(t2,2) - 
            51*Power(s,2)*Power(t2,2) - 24*Power(s,3)*Power(t2,2) - 
            301*Power(t2,3) + 55*s*Power(t2,3) + 
            24*Power(s,2)*Power(t2,3) + 7*Power(s,3)*Power(t2,3) - 
            255*Power(t2,4) + 382*s*Power(t2,4) + 
            32*Power(s,2)*Power(t2,4) + 25*Power(s,3)*Power(t2,4) + 
            33*Power(t2,5) + 125*s*Power(t2,5) - 
            21*Power(s,2)*Power(t2,5) + 5*Power(s,3)*Power(t2,5) + 
            22*Power(t2,6) + 6*s*Power(t2,6) - 
            2*Power(s,2)*Power(t2,6) - 
            Power(t1,5)*(3 + 66*t2 + 2*s*(-4 + 6*t2 + 3*Power(t2,2))) + 
            Power(s2,3)*Power(t2,2)*
             (42 + 110*t2 - 4*Power(t2,2) + 3*Power(t2,3) + 
               Power(t1,2)*(-12 + 16*t2 - 7*Power(t2,2)) + 
               2*t1*(-10 + 42*t2 - 7*Power(t2,2) + 2*Power(t2,3))) + 
            Power(t1,4)*(-59 - 175*t2 - 56*Power(t2,2) + 
               6*Power(t2,3) + 
               2*Power(s,2)*(7 + 6*t2 + 11*Power(t2,2) + 
                  2*Power(t2,3)) + 
               s*(-5 - 26*t2 + 72*Power(t2,2) + 6*Power(t2,3))) + 
            Power(t1,3)*(36 + 49*t2 + 559*Power(t2,2) + 
               181*Power(t2,3) + 26*Power(t2,4) + 
               Power(s,3)*(7 + 3*t2 - 3*Power(t2,2)) - 
               Power(s,2)*(44 + 26*t2 + 28*Power(t2,2) + 
                  71*Power(t2,3) + 4*Power(t2,4)) + 
               s*(-68 - 168*t2 - 52*Power(t2,2) + 81*Power(t2,3) + 
                  8*Power(t2,4))) + 
            Power(t1,2)*(39 + 457*t2 + 563*Power(t2,2) - 
               633*Power(t2,3) - 158*Power(t2,4) - 2*Power(t2,5) + 
               Power(s,3)*(-24 - 13*t2 + 15*Power(t2,2) + 
                  12*Power(t2,3) + 4*Power(t2,4)) + 
               Power(s,2)*(1 - 194*t2 - 256*Power(t2,2) + 
                  26*Power(t2,3) + 15*Power(t2,4)) - 
               s*(-21 + 209*t2 + 738*Power(t2,2) + 617*Power(t2,3) + 
                  37*Power(t2,4) + 8*Power(t2,5))) - 
            t1*(15 - 169*t2 - 273*Power(t2,2) + 1393*Power(t2,3) + 
               748*Power(t2,4) + 52*Power(t2,5) - 8*Power(t2,6) + 
               Power(s,2)*t2*
                (144 + 230*t2 - 363*Power(t2,2) - 284*Power(t2,3) - 
                  16*Power(t2,4)) + 
               Power(s,3)*t2*
                (72 + t2 - 19*Power(t2,2) + 34*Power(t2,3) + 
                  4*Power(t2,4)) - 
               s*(40 + 285*t2 - 428*Power(t2,2) - 571*Power(t2,3) + 
                  254*Power(t2,4) + 42*Power(t2,5))) - 
            Power(s2,2)*t2*(-50 - 123*t2 + 43*Power(t2,2) - 
               259*Power(t2,3) - 55*Power(t2,4) + 2*Power(t2,5) - 
               8*Power(t1,4)*t2*(3 + t2) + 
               Power(t1,3)*(-3 + 33*t2 + 45*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,2)*(-10 + 75*t2 + 459*Power(t2,2) + 
                  16*Power(t2,3)) + 
               t1*(63 + 119*t2 + 231*Power(t2,2) - 368*Power(t2,3) - 
                  34*Power(t2,4)) + 
               s*(12 + 102*t2 + 85*Power(t2,2) - 68*Power(t2,3) + 
                  Power(t2,4) - 
                  Power(t1,3)*(-9 + 30*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (11 + 42*t2 + 9*Power(t2,2) - 15*Power(t2,3)) + 
                  t1*(-12 + 68*t2 + 185*Power(t2,2) + 32*Power(t2,3) + 
                     12*Power(t2,4)))) + 
            s2*(12 + (63 - 10*s)*t2 + 
               5*(4 + 16*s + 3*Power(s,2))*Power(t2,2) + 
               (186 + 40*s - 90*Power(s,2))*Power(t2,3) - 
               (322 + 330*s + 95*Power(s,2))*Power(t2,4) - 
               (153 + 26*s + 7*Power(s,2))*Power(t2,5) + 
               4*(-2 + s)*Power(t2,6) + 8*Power(t1,5)*(1 + 3*t2) + 
               Power(t1,4)*(1 + 76*t2 - 4*Power(t2,3) - 
                  2*s*(-3 + 9*t2 + 33*Power(t2,2) + 5*Power(t2,3))) - 
               2*Power(t1,3)*
                (3 + 65*t2 + 160*Power(t2,2) - 34*Power(t2,3) + 
                  11*Power(t2,4) + 12*Power(s,2)*t2*(1 + t2) - 
                  s*(4 + 63*t2 + 96*Power(t2,2) + 53*Power(t2,3) + 
                     5*Power(t2,4))) + 
               Power(t1,2)*(11 - 11*t2 + 50*Power(t2,2) + 
                  1018*Power(t2,3) - 41*Power(t2,4) + 6*Power(t2,5) + 
                  Power(s,2)*
                   (7 + 102*t2 + 114*Power(t2,2) - 30*Power(t2,3) - 
                     12*Power(t2,4)) - 
                  2*s*(13 + 36*t2 - 210*Power(t2,2) - 209*Power(t2,3) + 
                     6*Power(t2,4))) + 
               2*t1*(-13 - 66*t2 + 165*Power(t2,2) + 686*Power(t2,3) - 
                  110*Power(t2,4) - 20*Power(t2,5) + 
                  Power(s,2)*t2*
                   (10 + 21*t2 + 33*Power(t2,2) + 39*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  s*(6 + 70*t2 + 184*Power(t2,2) - 219*Power(t2,3) - 
                     360*Power(t2,4) - 23*Power(t2,5))))) + 
         Power(s1,6)*(2 + 13*t2 - 14*s*t2 + 51*Power(t2,2) - 
            35*s*Power(t2,2) - 12*Power(s,2)*Power(t2,2) + 
            6*Power(s,3)*Power(t2,2) + 93*Power(t2,3) - 
            266*s*Power(t2,3) + Power(s,2)*Power(t2,3) - 
            10*Power(s,3)*Power(t2,3) - 85*Power(t2,4) - 
            165*s*Power(t2,4) + 55*Power(s,2)*Power(t2,4) - 
            15*Power(s,3)*Power(t2,4) - 54*Power(t2,5) - 
            18*s*Power(t2,5) + 10*Power(s,2)*Power(t2,5) - 
            2*Power(s,3)*Power(t2,5) + 
            2*Power(t1,5)*(29 + 37*t2 + 2*Power(t2,2) + 
               s*(3 + 5*t2 + Power(t2,2))) - 
            Power(t1,4)*(-25 - 12*t2 + 71*Power(t2,2) + 12*Power(t2,3) + 
               2*Power(s,2)*(4 + 17*t2 + 5*Power(t2,2)) + 
               s*(-68 - 8*t2 + 77*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s2,3)*t2*(Power(t1,2)*(6 - 36*t2 + 22*Power(t2,2)) - 
               t1*(-9 + 62*t2 + 2*Power(t2,2) + 19*Power(t2,3)) + 
               2*(-10 - 36*t2 + 5*Power(t2,2) + 7*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(t1,3)*(-91 - 509*t2 - 278*Power(t2,2) - 
               98*Power(t2,3) - 2*Power(t2,4) + 
               Power(s,3)*(-5 - 9*t2 + Power(t2,2)) + 
               2*Power(s,2)*(44 + 60*t2 + 38*Power(t2,2) + 
                  13*Power(t2,3)) + 
               s*(65 + 274*t2 + 131*Power(t2,2) + 11*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(t1,2)*(-68 - 185*t2 + 756*Power(t2,2) + 
               609*Power(t2,3) + 120*Power(t2,4) - 4*Power(t2,5) + 
               Power(s,3)*(36 + 27*t2 - 25*Power(t2,2) - 
                  6*Power(t2,3)) + 
               Power(s,2)*(13 + 234*t2 + 56*Power(t2,2) - 
                  115*Power(t2,3) - 14*Power(t2,4)) + 
               2*s*(-47 + 40*t2 + 304*Power(t2,2) + 69*Power(t2,3) + 
                  10*Power(t2,4))) + 
            t1*(Power(s,3)*t2*
                (48 + 30*t2 + 31*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s,2)*t2*
                (42 - 175*t2 - 257*Power(t2,2) - 20*Power(t2,3) + 
                  2*Power(t2,4)) + 
               2*(-6 - 60*t2 + 337*Power(t2,2) + 389*Power(t2,3) + 
                  51*Power(t2,4) - 19*Power(t2,5)) - 
               s*(34 + 27*t2 - 67*Power(t2,2) + 505*Power(t2,3) + 
                  177*Power(t2,4) + 12*Power(t2,5))) + 
            Power(s2,2)*(-12 + 5*(-15 + 4*s)*t2 + 
               3*(9 + s)*Power(t2,2) - (107 + 156*s)*Power(t2,3) - 
               (61 + 59*s)*Power(t2,4) + (8 - 6*s)*Power(t2,5) - 
               24*Power(t1,4)*t2*(1 + t2) + 
               t1*(16 + (78 + 11*s)*t2 + 3*(1 + 43*s)*Power(t2,2) + 
                  (-627 + 64*s)*Power(t2,3) + 
                  7*(-18 + 7*s)*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,3)*(-1 + 61*t2 + 183*Power(t2,2) + 
                  37*Power(t2,3) - 3*s*(-1 + 10*t2 + Power(t2,2))) + 
               Power(t1,2)*(-3 + 9*t2 + 451*Power(t2,2) - 
                  53*Power(t2,3) - 18*Power(t2,4) + 
                  s*(2 + 96*t2 + 93*Power(t2,2) - 42*Power(t2,3)))) + 
            s2*(-18 + (10 - 32*s)*t2 + 
               (26 - 128*s + 39*Power(s,2))*Power(t2,2) + 
               (274 + 70*s + 139*Power(s,2))*Power(t2,3) + 
               12*(21 - 2*s + 5*Power(s,2))*Power(t2,4) + 
               2*(16 - 9*s + 3*Power(s,2))*Power(t2,5) - 
               24*Power(t1,5)*(1 + t2) + 
               2*Power(t1,4)*(1 + 18*t2 + 13*Power(t2,2) + 
                  4*Power(t2,3) + 6*s*(-1 + 4*t2 + 3*Power(t2,2))) + 
               Power(t1,3)*(47 + 190*t2 - 202*Power(t2,2) + 
                  35*Power(t2,3) - 4*Power(t2,4) + 
                  4*Power(s,2)*(1 + 6*t2 + Power(t2,2)) - 
                  6*s*(7 + 38*t2 + 49*Power(t2,2) + 10*Power(t2,3))) + 
               Power(t1,2)*(Power(s,2)*
                   (-35 - 144*t2 - 34*Power(t2,2) + 27*Power(t2,3)) - 
                  2*(9 + 55*t2 + 558*Power(t2,2) + 54*Power(t2,3) + 
                     15*Power(t2,4)) + 
                  2*s*(34 - 80*t2 - 171*Power(t2,2) + 99*Power(t2,3) + 
                     15*Power(t2,4))) - 
               t1*(-33 + 139*t2 + 740*Power(t2,2) - 381*Power(t2,3) - 
                  217*Power(t2,4) - 16*Power(t2,5) + 
                  Power(s,2)*t2*
                   (22 + 20*t2 + 85*Power(t2,2) + 41*Power(t2,3)) + 
                  2*s*(25 + 84*t2 - 147*Power(t2,2) - 490*Power(t2,3) - 
                     67*Power(t2,4) + 2*Power(t2,5))))))*R1q(s1))/
     (Power(-1 + s1,3)*s1*(-1 + s2)*(-s + s2 - t1)*(-1 + s1 + t1 - t2)*
       Power(s1*t1 - t2,2)*(-1 + t2)*Power(-s1 + t2,3)) - 
    (8*(-4*Power(s1,11)*(-1 + t1)*Power(t1,2)*t2 - 
         Power(s1,10)*(24*Power(t1,4)*t2 + 2*Power(s2,3)*Power(t2,2) + 
            8*t1*Power(t2,2) + 
            Power(t1,3)*(2 + (-29 - 4*s + 4*s2)*t2 - 13*Power(t2,2)) + 
            Power(t1,2)*(4*s*t2*(-1 + 3*t2) + 
               (1 + t2)*(2 + (-2 + s2)*t2))) + 
         Power(s1,9)*(-60*Power(t1,5)*t2 + 
            2*Power(t1,4)*(-6 + 2*(25 + 6*s - 6*s2)*t2 + 
               31*Power(t2,2)) + 
            Power(t1,3)*(-12 + 3*(10 + 11*s - 4*s2)*t2 + 
               (38 - 87*s + 10*s2)*Power(t2,2) - 18*Power(t2,3)) + 
            2*t1*t2*(2 - 5*Power(s2,3)*t2 - 
               s*(4 + Power(s2,2) + 3*s2*(-1 + t2) - 12*t2)*t2 + 
               Power(s2,2)*(-2 + t2)*t2 + 4*Power(t2,2) + s2*t2*(1 + t2)\
) + 2*Power(t2,2)*(2*t2 + Power(s2,3)*(1 + 5*t2) + 
               Power(s2,2)*(3 - 3*s*t2)) + 
            Power(t1,2)*t2*(13 - 98*t2 + 12*Power(s,2)*t2 - 
               2*Power(s2,2)*t2 - 45*Power(t2,2) + 
               s2*(-5 + 46*t2 - 5*Power(t2,2)) + 
               s*(12 - (41 + 12*s2)*t2 + 47*Power(t2,2)))) - 
         Power(s1,7)*(60*Power(t1,7)*t2 - 
            4*Power(t1,6)*(-10 + (63 + 20*s - 20*s2)*t2 + 
               27*Power(t2,2)) + 
            Power(t1,5)*(40 + (-289 - 167*s + 72*s2)*t2 + 
               (-147 + 377*s - 124*s2)*Power(t2,2) + 112*Power(t2,3)) + 
            Power(t1,4)*(-16 + (-244 - 99*s + 28*s2)*t2 + 
               (1640 - 182*Power(s,2) - 677*s2 + 12*Power(s2,2) + 
                  4*s*(109 + 45*s2))*Power(t2,2) + 
               (548 - 511*s + 101*s2)*Power(t2,3) - 56*Power(t2,4)) + 
            t1*t2*(8 + (66 + 40*s2 - 7*Power(s2,2) - 14*Power(s2,3) + 
                  s*(-30 + 26*s2))*t2 - 
               (200 - 118*s2 - 163*Power(s2,2) + 34*Power(s2,3) + 
                  Power(s,2)*(-92 + 8*s2) + 
                  s*(161 + 122*s2 + 27*Power(s2,2)))*Power(t2,2) + 
               (-4*Power(s,3) + 2*Power(s,2)*(-71 + 21*s2) + 
                  s*(567 + 74*s2 - 129*Power(s2,2)) + 
                  2*(80 - 157*s2 + 20*Power(s2,2) + 52*Power(s2,3)))*
                Power(t2,3) - 
               2*(-71 + 7*Power(s,2) + s*(80 - 15*s2) - 30*s2 + 
                  6*Power(s2,2))*Power(t2,4)) + 
            Power(t1,3)*(-14 + (-119 + 49*s + 4*s2)*t2 + 
               (753 - 2*Power(s,3) - 202*s2 - 25*Power(s2,2) + 
                  20*Power(s2,3) - 6*Power(s,2)*(28 + s2) + 
                  s*(469 + 74*s2 + 28*Power(s2,2)))*Power(t2,2) + 
               (-560 + 308*Power(s,2) + 796*s2 + 5*Power(s2,2) - 
                  2*s*(726 + 133*s2))*Power(t2,3) + 
               2*(-201 + 158*s - 55*s2)*Power(t2,4) + 10*Power(t2,5)) + 
            Power(t1,2)*(2 + (25 + 18*s - 15*s2)*t2 + 
               (263 - 14*Power(s,2)*(-2 + s2) + 32*s2 - 
                  103*Power(s2,2) + 9*Power(s2,3) + 
                  s*(69 + 10*s2 - 23*Power(s2,2)))*Power(t2,2) - 
               (1401 + 24*Power(s,3) - 704*s2 + 18*Power(s2,2) + 
                  97*Power(s2,3) - 2*Power(s,2)*(143 + 18*s2) + 
                  s*(426 + 312*s2 - 15*Power(s2,2)))*Power(t2,3) - 
               (389 + 118*Power(s,2) + 343*s2 + 23*Power(s2,2) - 
                  s*(1033 + 94*s2))*Power(t2,4) + 
               (104 - 76*s + 50*s2)*Power(t2,5)) + 
            Power(t2,2)*(-2 - 3*(3 + 4*s)*t2 + 
               (40 + 39*s - 12*Power(s,2))*Power(t2,2) + 
               (19 - 47*s - 6*Power(s,2) + 2*Power(s,3))*Power(t2,3) + 
               Power(s2,3)*t2*(29 + 51*t2 - 20*Power(t2,2)) + 
               4*Power(s2,2)*
                (3 + (5 + s)*t2 + 2*(-5 + s)*Power(t2,2) + 
                  (-2 + 9*s)*Power(t2,3)) + 
               s2*(-6 + (-17 + 16*s)*t2 - 
                  2*(21 - 23*s + 8*Power(s,2))*Power(t2,2) + 
                  (5 + 22*s - 18*Power(s,2))*Power(t2,3)))) - 
         Power(t2,4)*(Power(t1,7)*
             (-3 + s + 2*Power(s,3) + 2*Power(s,2)*(-3 + t2) - 5*s*t2 + 
               (17 - 4*s2)*t2) + 
            Power(t1,6)*(25 + (-71 + 21*s2)*t2 + 
               (-16 + s2)*Power(t2,2) - Power(s,3)*(3 + 5*t2) + 
               Power(s,2)*(3 + (5 + 8*s2)*t2 - 4*Power(t2,2)) + 
               2*s*(-8 + (29 - 14*s2)*t2 + (1 + 2*s2)*Power(t2,2))) + 
            Power(t1,5)*(-37 + (24 - 14*s2)*t2 + 
               (-65 + 27*s2 - 10*Power(s2,2))*Power(t2,2) + 
               (8 + 3*s2 + 2*Power(s2,2))*Power(t2,3) + 
               Power(s,3)*(-8 - 15*t2 + 7*Power(t2,2)) + 
               Power(s,2)*(15 + (62 - 17*s2)*t2 - 
                  (35 + 17*s2)*Power(t2,2) + 2*Power(t2,3)) + 
               s*(31 + (-1 + 28*s2)*t2 + 
                  2*(11 + 12*s2 + 5*Power(s2,2))*Power(t2,2) - 
                  2*(3 + 2*s2)*Power(t2,3))) + 
            Power(t1,3)*t2*(287 - 32*t2 - 304*Power(t2,2) - 
               64*Power(t2,3) + Power(t2,4) - 
               Power(s2,3)*Power(t2,2)*(5 + 3*t2) + 
               Power(s2,2)*(t2 + 56*Power(t2,2) - 25*Power(t2,3)) + 
               Power(s,3)*(64 + 30*t2 - 49*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s,2)*(-120 + (-227 + 124*s2)*t2 + 
                  (152 + 117*s2)*Power(t2,2) - 3*(7 + 3*s2)*Power(t2,3)\
) - s2*(6 - 55*t2 + 19*Power(t2,2) + 13*Power(t2,3) + Power(t2,4)) + 
               s*(-236 + (27 - 206*s2 + 6*Power(s2,2))*t2 + 
                  (61 - 136*s2 - 63*Power(s2,2))*Power(t2,2) + 
                  (35 + 46*s2 + 9*Power(s2,2))*Power(t2,3) + Power(t2,4)\
)) + 4*t1*Power(t2,2)*(Power(s2,3)*t2*(3 + 5*t2) + 
               2*Power(s2,2)*t2*(7 - 11*t2 + 4*Power(t2,2)) + 
               Power(s,3)*(-32 - 5*t2 + 21*Power(t2,2)) + 
               Power(s,2)*(60 + (90 - 47*s2)*t2 - 
                  (50 + 49*s2)*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(15 + t2 - 31*Power(t2,2) + Power(t2,3) - 
                  2*Power(t2,4)) + 
               2*(-68 - 8*t2 + 143*Power(t2,2) + 16*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(103 + (-39 + 64*s2 - 15*Power(s2,2))*t2 + 
                  (-63 + 52*s2 + 23*Power(s2,2))*Power(t2,2) - 
                  (11 + 12*s2)*Power(t2,3) + 2*Power(t2,4))) + 
            Power(t1,4)*(15 + 3*(-49 + s2)*t2 + 
               3*(185 - 51*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (75 + 23*s2 - 9*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,3) - (2 + 7*s2)*Power(t2,4) + 
               Power(s,3)*(12 + 42*t2 + 35*Power(t2,2) - 
                  7*Power(t2,3)) + 
               Power(s,2)*(-12 + 2*(-11 + s2)*t2 + 
                  (15 - 48*s2)*Power(t2,2) + (41 + 18*s2)*Power(t2,3)) \
+ s*(-16 + 54*t2 + (-510 + 174*s2 - 11*Power(s2,2))*Power(t2,2) - 
                  (30 + 34*s2 + 15*Power(s2,2))*Power(t2,3) + 
                  8*Power(t2,4))) - 
            16*Power(t2,2)*(-12 + 3*(-14 + 2*s2 + Power(s2,2))*t2 + 
               (15 - 18*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (38 - 11*s2 + Power(s2,2))*Power(t2,3) + 
               (1 + 5*s2 + Power(s2,2))*Power(t2,4) + 
               2*Power(s,3)*(-6 - 9*t2 + 2*Power(t2,3)) + 
               Power(s,2)*(12 + (7 - 2*s2)*t2 - 
                  (29 + 6*s2)*Power(t2,2) - (17 + 8*s2)*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(16 - 12*(-5 + s2)*t2 + 
                  (44 + 4*s2 + 6*Power(s2,2))*Power(t2,2) + 
                  (-5 + 18*s2 + 4*Power(s2,2))*Power(t2,3) - 
                  (5 + 2*s2)*Power(t2,4))) + 
            2*Power(t1,2)*t2*(-57 + (10 - 6*s2 - 3*Power(s2,2))*t2 - 
               2*(253 - 51*s2 + 10*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,2) + 
               (54 - 64*s2 + 23*Power(s2,2) - 2*Power(s2,3))*
                Power(t2,3) + (3 + 24*s2 + 8*Power(s2,2))*Power(t2,4) + 
               8*Power(s,3)*(-6 - 12*t2 - 3*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s,2)*(48 + (25 - 8*s2)*t2 + 
                  2*(-64 + s2)*Power(t2,2) - (81 + 34*s2)*Power(t2,3) + 
                  8*Power(t2,4)) + 
               2*s*(32 + (76 - 3*s2)*t2 + 
                  (322 - 58*s2 + 26*Power(s2,2))*Power(t2,2) + 
                  (11 + 33*s2 + 10*Power(s2,2))*Power(t2,3) - 
                  (13 + 8*s2)*Power(t2,4)))) + 
         s1*Power(t2,3)*(2*Power(t1,8)*
             (-1 + Power(s,2)*t2 - 4*(-4 + s2)*t2 - s*(5 + 3*t2)) + 
            Power(t1,7)*(34 - Power(s,3)*(-5 + t2) + 
               7*(-17 + 4*s2)*t2 + (-33 + 6*s2)*Power(t2,2) + 
               Power(s,2)*(-39 + (3 + 2*s2)*t2 - 4*Power(t2,2)) + 
               s*(26 + (94 - 28*s2)*t2 + 4*s2*Power(t2,2))) + 
            Power(t1,6)*(-24 + (-101 + 59*s2)*t2 + 
               (-186 + 31*s2 - 4*Power(s2,2))*Power(t2,2) + 
               (17 + 6*s2 + 2*Power(s2,2))*Power(t2,3) + 
               Power(s,3)*(-21 - 32*t2 + 7*Power(t2,2)) + 
               Power(s,2)*(78 + (138 + 13*s2)*t2 - 
                  (82 + 11*s2)*Power(t2,2) + 2*Power(t2,3)) + 
               s*(-26 + (109 - 72*s2)*t2 + 
                  (77 + 56*s2 + 4*Power(s2,2))*Power(t2,2) - 
                  4*(3 + s2)*Power(t2,3))) + 
            Power(t1,5)*(-30 - 2*(137 + 50*s2)*t2 + 
               (1372 - 272*s2 - 39*Power(s2,2))*Power(t2,2) + 
               (233 + 22*s2 + Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) - (5 + 14*s2)*Power(t2,4) + 
               Power(s,3)*(22 + 22*t2 + 45*Power(t2,2) - 
                  11*Power(t2,3)) + 
               Power(s,2)*(-33 + (235 - 44*s2)*t2 - 
                  (15 + 98*s2)*Power(t2,2) + (89 + 24*s2)*Power(t2,3)) \
+ s*(20 + 18*(-11 + 8*s2)*t2 + 
                  (-1128 + 366*s2 + 37*Power(s2,2))*Power(t2,2) - 
                  (76 + 86*s2 + 15*Power(s2,2))*Power(t2,3) + 
                  16*Power(t2,4))) + 
            Power(t1,3)*t2*(157 + 53*t2 - 4041*Power(t2,2) + 
               2*Power(s2,3)*(-21 + t2)*Power(t2,2) + 53*Power(t2,3) + 
               18*Power(t2,4) + 
               Power(s2,2)*t2*
                (-15 + 281*t2 - 44*Power(t2,2) + 34*Power(t2,3)) + 
               2*Power(s,3)*(-112 - 211*t2 - 94*Power(t2,2) + 
                  41*Power(t2,3)) + 
               Power(s,2)*(312 + (-115 + 308*s2)*t2 + 
                  (-275 + 406*s2)*Power(t2,2) - 
                  54*(10 + 3*s2)*Power(t2,3) + 34*Power(t2,4)) + 
               2*s2*(-9 + 299*t2 + 202*Power(t2,2) - 181*Power(t2,3) + 
                  61*Power(t2,4)) - 
               2*s*(30 + (-542 + 543*s2 - 9*Power(s2,2))*t2 + 
                  (-1885 + 507*s2 + 72*Power(s2,2))*Power(t2,2) - 
                  (192 + 280*s2 + 39*Power(s2,2))*Power(t2,3) + 
                  (69 + 34*s2)*Power(t2,4))) - 
            4*t1*Power(t2,2)*
             (40 - 477*t2 - 459*Power(t2,2) + 427*Power(t2,3) + 
               5*Power(t2,4) + 
               3*Power(s2,3)*t2*(-3 - 10*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-36 + 183*t2 - 29*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s,3)*(-136 - 245*t2 - 46*Power(t2,2) + 
                  35*Power(t2,3)) + 
               Power(s,2)*(180 + 3*(64 + 35*s2)*t2 + 
                  (-217 + 102*s2)*Power(t2,2) - 
                  (237 + 55*s2)*Power(t2,3) + 18*Power(t2,4)) + 
               s2*(-45 + 234*t2 - 212*Power(t2,2) - 168*Power(t2,3) + 
                  71*Power(t2,4)) + 
               s*(47 + (662 - 480*s2 + 45*Power(s2,2))*t2 - 
                  14*(-52 + 9*s2 + 3*Power(s2,2))*Power(t2,2) + 
                  (72 + 258*s2 + 5*Power(s2,2))*Power(t2,3) - 
                  (77 + 36*s2)*Power(t2,4))) + 
            Power(t1,4)*(22 + (443 + 39*s2)*t2 + 
               19*(51 - 23*s2 + 4*Power(s2,2))*Power(t2,2) + 
               (-774 + 302*s2 + 16*Power(s2,2) + 13*Power(s2,3))*
                Power(t2,3) - 
               (181 + 62*s2 + 42*Power(s2,2) + 5*Power(s2,3))*
                Power(t2,4) + (5 - 2*s2)*Power(t2,5) + 
               Power(s,3)*t2*
                (212 + 212*t2 - 93*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s,2)*(-6 + (-669 + 8*s2)*t2 + 
                  (-946 + 7*s2)*Power(t2,2) + 
                  (575 + 194*s2)*Power(t2,3) - (44 + 15*s2)*Power(t2,4)\
) + s*(-10 - (27 + 44*s2)*t2 + 
                  (-455 + 280*s2 - 39*Power(s2,2))*Power(t2,2) - 
                  2*(98 + 237*s2 + 57*Power(s2,2))*Power(t2,3) + 
                  (132 + 86*s2 + 15*Power(s2,2))*Power(t2,4) + 
                  2*Power(t2,5))) - 
            4*Power(t2,2)*(-52 + 9*(-32 - 5*s2 + 4*Power(s2,2))*t2 + 
               (76 - 199*s2 + 22*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,2) + 
               (482 - 135*s2 + 6*Power(s2,2) - 11*Power(s2,3))*
                Power(t2,3) + 
               (28 + 81*s2 + 8*Power(s2,2) - 4*Power(s2,3))*
                Power(t2,4) - 6*(1 + s2)*Power(t2,5) + 
               Power(s,3)*t2*
                (-48 - 17*t2 + 29*Power(t2,2) + 4*Power(t2,3)) - 
               Power(s,2)*(-24 + 16*(-3 + 2*s2)*t2 + 
                  (150 + 67*s2)*Power(t2,2) + 
                  (166 + 97*s2)*Power(t2,3) + 4*(-5 + 3*s2)*Power(t2,4)\
) + s*(40 + (291 + 32*s2)*t2 + 
                  (353 + 88*s2 + 93*Power(s2,2))*Power(t2,2) + 
                  (-71 + 220*s2 + 79*Power(s2,2))*Power(t2,3) + 
                  (-115 - 28*s2 + 12*Power(s2,2))*Power(t2,4) + 
                  6*Power(t2,5))) + 
            Power(t1,2)*t2*(-158 - (1649 + 216*s2 + 18*Power(s2,2))*t2 - 
               (2052 - 719*s2 + 215*Power(s2,2) + 14*Power(s2,3))*
                Power(t2,2) + 
               (4480 - 1307*s2 + 66*Power(s2,2) - 17*Power(s2,3))*
                Power(t2,3) + 
               (462 + 235*s2 + 135*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,4) + (5 - 23*s2)*Power(t2,5) - 
               Power(s,3)*t2*
                (560 + 344*t2 - 239*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s,2)*(48 + (1422 - 64*s2)*t2 + 
                  (1533 - 342*s2)*Power(t2,2) - 
                  (1206 + 551*s2)*Power(t2,3) + 21*(7 + s2)*Power(t2,4)) \
+ s*(80 + (818 + 316*s2)*t2 + 
                  (935 - 54*s2 + 300*Power(s2,2))*Power(t2,2) + 
                  (-159 + 1084*s2 + 329*Power(s2,2))*Power(t2,3) - 
                  (449 + 282*s2 + 21*Power(s2,2))*Power(t2,4) + 
                  23*Power(t2,5)))) + 
         Power(s1,8)*(-80*Power(t1,6)*t2 + 
            Power(t1,5)*(-30 + (203 + 60*s - 60*s2)*t2 + 
               117*Power(t2,2)) + 
            Power(t1,4)*(-30 + (145 + 104*s - 43*s2)*t2 + 
               (121 - 252*s + 61*s2)*Power(t2,2) - 72*Power(t2,3)) + 
            Power(t1,3)*(4 + (94 + 53*s - 19*s2)*t2 + 
               (-593 + 72*Power(s,2) + 277*s2 - 8*Power(s2,2) - 
                  3*s*(71 + 24*s2))*Power(t2,2) + 
               (-267 + 242*s - 38*s2)*Power(t2,3) + 16*Power(t2,4)) + 
            Power(t1,2)*(4 + (49 - 10*s - 3*s2)*t2 + 
               (-144 + 59*s2 - Power(s2,2) - 20*Power(s2,3) + 
                  2*Power(s,2)*(18 + s2) - 
                  3*s*(41 + 2*s2 + 4*Power(s2,2)))*Power(t2,2) + 
               (71 - 70*Power(s,2) - 164*s2 + Power(s2,2) + 
                  s*(403 + 46*s2))*Power(t2,3) + 
               (98 - 80*s + 30*s2)*Power(t2,4)) - 
            Power(t2,2)*(2 + 6*Power(t2,2) + 4*s*t2*(-1 + 3*t2) + 
               4*Power(s2,3)*(-1 - t2 + 5*Power(t2,2)) + 
               2*Power(s2,2)*
                (3 + (13 - 7*s)*t2 + (1 - 12*s)*Power(t2,2)) + 
               s2*(6 + t2 - 6*s*t2 + 
                  (1 - 6*s + 6*Power(s,2))*Power(t2,2))) + 
            t1*Power(t2,2)*(-20 + 109*t2 + 51*Power(t2,2) - 
               2*Power(s,2)*t2*(9 - s2 + 3*t2) + 
               Power(s2,3)*(1 + 51*t2) + 
               Power(s2,2)*(38 + 22*t2 - 8*Power(t2,2)) + 
               2*s2*(9 - 42*t2 + 5*Power(t2,2)) - 
               2*s*(15 - 38*t2 + 47*Power(t2,2) + 
                  Power(s2,2)*(-5 + 14*t2) + 
                  s2*(4 - 5*t2 - 11*Power(t2,2))))) + 
         Power(s1,2)*Power(t2,2)*
          (Power(t1,9)*(1 + (-15 + 4*s2)*t2 + s*(5 + t2)) + 
            Power(t1,8)*(-30 - 4*Power(s,2)*(-3 + t2) + 
               (48 - 13*s2)*t2 + (18 - 5*s2)*Power(t2,2) + 
               s*(-7 + (-37 + 12*s2)*t2 + 2*Power(t2,2))) + 
            Power(t1,7)*(25 + (193 - 53*s2)*t2 - 
               4*(-46 + 7*s2)*Power(t2,2) - (10 + 3*s2)*Power(t2,3) + 
               Power(s,3)*(3 + 5*t2) + 
               Power(s,2)*(-9 - 72*t2 + 41*Power(t2,2)) + 
               s*(-32 + 5*(-27 + 8*s2)*t2 - (61 + 32*s2)*Power(t2,2) + 
                  6*Power(t2,3))) + 
            Power(t1,6)*(-7 + (409 - 34*s2)*t2 + 
               4*(-293 + 78*s2 + 3*Power(s2,2))*Power(t2,2) + 
               (-246 + 9*s2 - 4*Power(s2,2))*Power(t2,3) + 
               (4 + 7*s2)*Power(t2,4) - 
               6*Power(s,3)*(1 - 3*t2 + 3*Power(t2,2)) - 
               Power(s,2)*(21 + (322 + 6*s2)*t2 - 
                  (135 + 32*s2)*Power(t2,2) + 42*Power(t2,3)) + 
               s*(65 + (227 + 36*s2)*t2 - 
                  2*(-349 + 158*s2 + 6*Power(s2,2))*Power(t2,2) + 
                  (68 + 40*s2)*Power(t2,3) - 8*Power(t2,4))) + 
            Power(t1,5)*(30 + (-459 + 145*s2)*t2 + 
               (-2278 + 564*s2 + 57*Power(s2,2))*Power(t2,2) - 
               (-523 + 471*s2 + 14*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,3) + 
               (181 + 81*s2 + 17*Power(s2,2))*Power(t2,4) + 
               (-7 + s2)*Power(t2,5) + 
               Power(s,3)*t2*(-143 - 175*t2 + 60*Power(t2,2)) + 
               Power(s,2)*(18 + (486 + 33*s2)*t2 + 
                  3*(331 + 38*s2)*Power(t2,2) - 
                  (648 + 119*s2)*Power(t2,3) + 23*Power(t2,4)) - 
               s*(37 + 4*(-41 + 39*s2)*t2 + 
                  (-1020 + 566*s2 + 51*Power(s2,2))*Power(t2,2) - 
                  (577 + 586*s2 + 63*Power(s2,2))*Power(t2,3) + 
                  5*(31 + 8*s2)*Power(t2,4) + Power(t2,5))) - 
            Power(t1,3)*t2*(137 - 2502*t2 - 6432*Power(t2,2) + 
               6317*Power(t2,3) + 675*Power(t2,4) - 23*Power(t2,5) + 
               3*Power(s2,3)*Power(t2,2)*(-30 + 13*t2 + Power(t2,2)) - 
               Power(s,3)*t2*
                (742 + 628*t2 - 341*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-51 + 452*t2 - 67*Power(t2,2) + 194*Power(t2,3)) + 
               Power(s,2)*(168 + (2199 + 196*s2)*t2 + 
                  56*(43 + 5*s2)*Power(t2,2) - 
                  (2517 + 697*s2)*Power(t2,3) + 
                  (238 + 9*s2)*Power(t2,4)) + 
               s2*(-18 + 788*t2 + 757*Power(t2,2) - 2761*Power(t2,3) + 
                  544*Power(t2,4) - 22*Power(t2,5)) + 
               s*(-220 + 2*(501 - 491*s2 + 9*Power(s2,2))*t2 + 
                  (3425 - 1224*s2 - 42*Power(s2,2))*Power(t2,2) + 
                  (1433 + 2486*s2 + 317*Power(s2,2))*Power(t2,3) - 
                  (938 + 432*s2 + 9*Power(s2,2))*Power(t2,4) + 
                  22*Power(t2,5))) + 
            t1*Power(t2,2)*(176 - 3397*t2 - 1616*Power(t2,2) + 
               6424*Power(t2,3) + 448*Power(t2,4) - 67*Power(t2,5) - 
               Power(s2,3)*t2*
                (36 + 208*t2 - 199*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s,3)*t2*
                (-708 - 418*t2 + 379*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-96 + 1613*t2 - 272*Power(t2,2) + 227*Power(t2,3)) + 
               Power(s,2)*(384 + 4*(402 + 37*s2)*t2 + 
                  3*(-253 + 68*s2)*Power(t2,2) - 
                  (2656 + 807*s2)*Power(t2,3) + 
                  (311 - 81*s2)*Power(t2,4)) - 
               s2*(180 - 1258*t2 + 2673*Power(t2,2) + 
                  2287*Power(t2,3) - 1051*Power(t2,4) + 65*Power(t2,5)) \
+ s*(-180 + 4*(605 - 448*s2 + 45*Power(s2,2))*t2 + 
                  (4835 - 294*s2 + 102*Power(s2,2))*Power(t2,2) + 
                  (721 + 3128*s2 + 229*Power(s2,2))*Power(t2,3) + 
                  (-1573 - 538*s2 + 81*Power(s2,2))*Power(t2,4) + 
                  65*Power(t2,5))) + 
            Power(t1,2)*t2*(134 + (659 + 320*s2 + 18*Power(s2,2))*t2 + 
               (-583 + 276*s2 + 417*Power(s2,2) - 30*Power(s2,3))*
                Power(t2,2) + 
               (-8839 + 1206*s2 + 471*Power(s2,2) - 14*Power(s2,3))*
                Power(t2,3) + 
               (1391 - 1456*s2 - 352*Power(s2,2) + 44*Power(s2,3))*
                Power(t2,4) + 
               (38 + 394*s2 + 90*Power(s2,2))*Power(t2,5) + 
               2*Power(s,3)*t2*
                (-260 - 486*t2 - 209*Power(t2,2) + 73*Power(t2,3)) - 
               2*s*(24 + (-297 + 254*s2)*t2 + 
                  (-1516 + 1211*s2 + 138*Power(s2,2))*Power(t2,2) + 
                  (-1921 + 957*s2 + 345*Power(s2,2))*Power(t2,3) - 
                  (982 + 832*s2 + 29*Power(s2,2))*Power(t2,4) + 
                  10*(23 + 9*s2)*Power(t2,5)) + 
               Power(s,2)*t2*
                (410 + 225*t2 + 571*Power(t2,2) - 1492*Power(t2,3) + 
                  90*Power(t2,4) + 
                  s2*(48 + 726*t2 + 994*Power(t2,2) - 248*Power(t2,3)))) \
- Power(t1,4)*(19 + (133 + 67*s2)*t2 + 
               (1149 - 25*s2 + 138*Power(s2,2))*Power(t2,2) + 
               (-6305 + 1408*s2 + 165*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,3) - 
               (407 + 416*s2 + 121*Power(s2,2) + 12*Power(s2,3))*
                Power(t2,4) + 
               (31 + 100*s2 + 18*Power(s2,2))*Power(t2,5) + 
               2*Power(s,3)*t2*
                (-68 - 82*t2 - 107*Power(t2,2) + 25*Power(t2,3)) - 
               s*(6 + 4*(-95 + 17*s2)*t2 + 
                  (-1349 + 536*s2 + 51*Power(s2,2))*Power(t2,2) + 
                  (-3309 + 1632*s2 + 263*Power(s2,2))*Power(t2,3) - 
                  2*(406 + 298*s2 + 37*Power(s2,2))*Power(t2,4) + 
                  12*(10 + 3*s2)*Power(t2,5)) + 
               Power(s,2)*t2*
                (-11 - 986*t2 + 660*Power(t2,2) - 543*Power(t2,3) + 
                  18*Power(t2,4) + 
                  s2*(6 + 166*t2 + 462*Power(t2,2) - 112*Power(t2,3)))) \
- 2*Power(t2,2)*(80 + (281 + 302*s2 - 72*Power(s2,2))*t2 + 
               (-212 + 442*s2 + 147*Power(s2,2) - 42*Power(s2,3))*
                Power(t2,2) + 
               2*(-582 + 89*s2 + 25*Power(s2,2) + 45*Power(s2,3))*
                Power(t2,3) + 
               (32 - 320*s2 - 9*Power(s2,2) + 40*Power(s2,3))*
                Power(t2,4) + (55 + 62*s2 + 4*Power(s2,2))*Power(t2,5) + 
               2*Power(s,3)*t2*
                (-24 - 49*t2 - 18*Power(t2,2) + 7*Power(t2,3)) - 
               2*s*(24 + (85 + 128*s2)*t2 + 
                  (126 + 197*s2 + 135*Power(s2,2))*Power(t2,2) + 
                  2*(-123 + 106*s2 + 70*Power(s2,2))*Power(t2,3) + 
                  (-253 - 97*s2 + 33*Power(s2,2))*Power(t2,4) + 
                  4*(9 + s2)*Power(t2,5)) + 
               Power(s,2)*t2*(32 + 255*t2 + 126*Power(t2,2) - 
                  209*Power(t2,3) + 4*Power(t2,4) + 
                  2*s2*(24 + 73*t2 + 85*Power(t2,2) + 6*Power(t2,3))))) + 
         Power(s1,6)*(-24*Power(t1,8)*t2 + 
            Power(t1,7)*(-30 + (187 + 60*s - 60*s2)*t2 + 
               47*Power(t2,2)) + 
            Power(t1,6)*(-30 + (304 + 148*s - 63*s2)*t2 + 
               (67 - 308*s + 121*s2)*Power(t2,2) - 85*Power(t2,3)) + 
            Power(t1,5)*(24 + 2*(151 + 52*s - 10*s2)*t2 + 
               (-2332 + 248*Power(s,2) + 855*s2 - 8*Power(s2,2) - 
                  2*s*(211 + 120*s2))*Power(t2,2) + 
               (-501 + 560*s - 119*s2)*Power(t2,3) + 73*Power(t2,4)) + 
            Power(t1,4)*(18 + (101 - 92*s + 6*s2)*t2 + 
               (-1582 + 8*Power(s,3) + 320*s2 + 41*Power(s2,2) - 
                  10*Power(s2,3) + Power(s,2)*(335 + 4*s2) - 
                  s*(821 + 138*s2 + 32*Power(s2,2)))*Power(t2,2) - 
               (-1611 + 555*Power(s,2) + 1515*s2 + Power(s2,2) - 
                  s*(2221 + 526*s2))*Power(t2,3) + 
               (635 - 506*s + 167*s2)*Power(t2,4) - 31*Power(t2,5)) + 
            t1*t2*(4 + (56 - 47*s2 - 42*Power(s2,2) + 4*s*(13 + 3*s2))*
                t2 + (310 + Power(s,2)*(50 - 54*s2) + 298*s2 - 
                  260*Power(s2,2) - 45*Power(s2,3) + 
                  s*(-103 + 60*s2 - 27*Power(s2,2)))*Power(t2,2) - 
               (1431 + 52*Power(s,3) - 749*s2 - 260*Power(s2,2) + 
                  185*Power(s2,3) - Power(s,2)*(432 + 107*s2) + 
                  s*(327 + 756*s2 + 66*Power(s2,2)))*Power(t2,3) + 
               (-225 - 18*Power(s,3) - 596*s2 + 6*Power(s2,2) + 
                  106*Power(s2,3) + Power(s,2)*(-248 + 111*s2) + 
                  s*(1370 + 146*s2 - 199*Power(s2,2)))*Power(t2,4) - 
               2*(-80 + 5*Power(s,2) + s*(76 - 9*s2) - 50*s2 + 
                  4*Power(s2,2))*Power(t2,5)) + 
            Power(t1,3)*(-6 + (-113 - 57*s + 41*s2)*t2 + 
               (-647 + 6*Power(s,3) - 105*s2 + 125*Power(s2,2) - 
                  13*Power(s2,3) + Power(s,2)*(-104 + 43*s2) + 
                  s*(-175 + 50*s2 + 9*Power(s2,2)))*Power(t2,2) + 
               (4991 + 90*Power(s,3) - 1947*s2 - 70*Power(s2,2) + 
                  85*Power(s2,3) - Power(s,2)*(986 + 135*s2) + 
                  s*(733 + 1028*s2 + 75*Power(s2,2)))*Power(t2,3) + 
               (617 + 466*Power(s,2) + 1102*s2 + 97*Power(s2,2) - 
                  s*(2737 + 454*s2))*Power(t2,4) + 
               (-309 + 238*s - 143*s2)*Power(t2,5) + 3*Power(t2,6)) + 
            Power(t1,2)*t2*(-69 - 35*t2 + 1441*Power(t2,2) - 
               2221*Power(t2,3) - 804*Power(t2,4) + 60*Power(t2,5) - 
               2*Power(s,3)*Power(t2,2)*(1 + 22*t2) + 
               2*Power(s2,3)*t2*(8 + 35*t2 - 91*Power(t2,2)) - 
               Power(s2,2)*t2*
                (11 + 402*t2 - 8*Power(t2,2) + 55*Power(t2,3)) + 
               s*(12 + (217 + 82*s2 + 6*Power(s2,2))*t2 + 
                  (363 + 272*s2 + 52*Power(s2,2))*Power(t2,2) + 
                  2*(-1307 - 512*s2 + 75*Power(s2,2))*Power(t2,3) + 
                  2*(632 + 63*s2)*Power(t2,4) - 40*Power(t2,5)) + 
               s2*(-6 - 130*t2 + 16*Power(t2,2) + 1972*Power(t2,3) - 
                  545*Power(t2,4) + 35*Power(t2,5)) - 
               Power(s,2)*t2*
                (108 + 372*t2 - 1075*Power(t2,2) + 93*Power(t2,3) + 
                  s2*(12 + 32*t2 - 42*Power(t2,2)))) + 
            Power(t2,2)*(2 + (19 - 8*s)*t2 + 
               (-86 - 47*s + 38*Power(s,2))*Power(t2,2) + 
               (75 + 251*s - 90*Power(s,2) + 4*Power(s,3))*Power(t2,3) + 
               2*(31 - 40*s - 7*Power(s,2) + 2*Power(s,3))*Power(t2,4) - 
               2*Power(s2,3)*t2*
                (13 - 33*t2 - 62*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(12 + (25 + 14*s)*t2 + 
                  (11 - 44*s - 16*Power(s,2))*Power(t2,2) + 
                  20*(-8 + 5*s)*Power(t2,3) - 
                  6*(-5 - 5*s + 3*Power(s,2))*Power(t2,4)) + 
               Power(s2,2)*t2*
                (77 + 164*t2 - 21*Power(t2,2) - 12*Power(t2,3) + 
                  3*s*(-4 - 25*t2 - 35*Power(t2,2) + 8*Power(t2,3))))) + 
         Power(s1,5)*(-4*Power(t1,9)*t2 + 
            2*Power(t1,8)*(-6 + 2*(19 + 6*s - 6*s2)*t2 + 
               3*Power(t2,2)) + 
            Power(t1,7)*(-12 + (178 + 71*s - 28*s2)*t2 + 
               (1 - 129*s + 58*s2)*Power(t2,2) - 33*Power(t2,3)) + 
            Power(t1,6)*(16 + (187 + 68*s - 7*s2)*t2 + 
               (-1775 + 192*Power(s,2) + 583*s2 - 2*Power(s2,2) - 
                  20*s*(8 + 9*s2))*Power(t2,2) + 
               (-233 + 330*s - 52*s2)*Power(t2,3) + 43*Power(t2,4)) + 
            Power(t1,5)*(10 + (15 - 82*s + 12*s2)*t2 + 
               (-1682 + 12*Power(s,3) + Power(s,2)*(369 - 4*s2) + 
                  282*s2 + 23*Power(s2,2) - 2*Power(s2,3) - 
                  2*s*(374 + 60*s2 + 9*Power(s2,2)))*Power(t2,2) + 
               (2043 - 521*Power(s,2) - 1432*s2 + 7*Power(s2,2) + 
                  4*s*(389 + 136*s2))*Power(t2,3) + 
               (515 - 410*s + 126*s2)*Power(t2,4) - 33*Power(t2,5)) + 
            Power(t2,2)*(-6 + 
               (-29 - 52*s2 + 78*Power(s2,2) + 4*s*(-7 + 3*s2))*t2 + 
               (-109 - 247*s2 - 83*Power(s2,2) + 126*Power(s2,3) + 
                  2*Power(s,2)*(-8 + 11*s2) + 
                  s*(83 + 70*s2 + 40*Power(s2,2)))*Power(t2,2) + 
               (523 + 20*Power(s,3) - 238*s2 - 330*Power(s2,2) - 
                  32*Power(s2,3) - Power(s,2)*(176 + 101*s2) + 
                  9*s*(6 + 52*s2 + 29*Power(s2,2)))*Power(t2,3) + 
               (41 + 6*Power(s,3) + Power(s,2)*(148 - 93*s2) + 
                  291*s2 + Power(s2,2) - 136*Power(s2,3) + 
                  s*(-579 - 100*s2 + 223*Power(s2,2)))*Power(t2,4) - 
               2*(36 + Power(s,3) + 25*s2 - 4*Power(s2,2) - 
                  Power(s2,3) - Power(s,2)*(5 + 3*s2) + 
                  s*(-38 + 9*s2 + 3*Power(s2,2)))*Power(t2,5)) + 
            Power(t1,4)*(-6 + (-157 - 67*s + 37*s2)*t2 + 
               (-544 + 21*Power(s,3) - 67*s2 + 63*Power(s2,2) - 
                  5*Power(s2,3) + 5*Power(s,2)*(-29 + 9*s2) + 
                  s*(-367 + 106*s2 - 11*Power(s2,2)))*Power(t2,2) + 
               (7676 + 131*Power(s,3) - 2464*s2 - 118*Power(s2,2) + 
                  33*Power(s2,3) - Power(s,2)*(1522 + 169*s2) + 
                  s*(232 + 1400*s2 + 125*Power(s2,2)))*Power(t2,3) + 
               (-95 + 685*Power(s,2) + 1400*s2 + 93*Power(s2,2) - 
                  2*s*(1548 + 325*s2))*Power(t2,4) + 
               (-361 + 294*s - 162*s2)*Power(t2,5) + 9*Power(t2,6)) + 
            Power(t1,2)*t2*(34 + 
               (350 + 84*Power(s,2) - 15*s2 - 12*Power(s2,2) - 
                  s*(7 + 72*s2))*t2 - 
               (-347 + 126*Power(s,3) - 817*s2 + 360*Power(s2,2) + 
                  7*Power(s2,3) + Power(s,2)*(-590 + 19*s2) + 
                  s*(7 + 480*s2 + 75*Power(s2,2)))*Power(t2,2) + 
               (-7188 - 279*Power(s,3) + 1884*s2 + 631*Power(s2,2) - 
                  221*Power(s2,3) + Power(s,2)*(1529 + 392*s2) + 
                  s*(534 - 2018*s2 - 289*Power(s2,2)))*Power(t2,3) + 
               (646 + 27*Power(s,3) - 2628*s2 - 212*Power(s2,2) + 
                  164*Power(s2,3) + Power(s,2)*(-1566 + 49*s2) + 
                  s*(4970 + 1476*s2 - 240*Power(s2,2)))*Power(t2,4) + 
               (676 + 47*Power(s,2) + 551*s2 + 47*Power(s2,2) - 
                  s*(879 + 94*s2))*Power(t2,5) + 
               3*(-5 + 3*s - 3*s2)*Power(t2,6)) - 
            Power(t1,3)*t2*(99 - 240*t2 - 3180*Power(t2,2) + 
               7058*Power(t2,3) + 1309*Power(t2,4) - 144*Power(t2,5) + 
               6*Power(s2,3)*t2*(-1 - 9*t2 + 22*Power(t2,2)) + 
               Power(s,3)*t2*(8 + 11*t2 + 161*Power(t2,2)) + 
               Power(s2,2)*t2*
                (55 + 460*t2 - 152*Power(t2,2) + 181*Power(t2,3)) - 
               2*s2*(-6 - 133*t2 + 103*Power(t2,2) + 
                  2146*Power(t2,3) - 538*Power(t2,4) + 41*Power(t2,5)) \
+ 2*s*(-15 - (284 + 129*s2 + 6*Power(s2,2))*t2 - 
                  5*(81 + 30*s2 + 11*Power(s2,2))*Power(t2,2) + 
                  (2059 + 1272*s2 + 33*Power(s2,2))*Power(t2,3) - 
                  (1224 + 233*s2)*Power(t2,4) + 49*Power(t2,5)) + 
               Power(s,2)*t2*
                (281 + 853*t2 - 2347*Power(t2,2) + 333*Power(t2,3) + 
                  s2*(30 + 126*t2 - 314*Power(t2,2)))) + 
            t1*Power(t2,2)*(105 - 230*t2 - 1241*Power(t2,2) + 
               2590*Power(t2,3) + 690*Power(t2,4) - 90*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(-18 + 119*t2 + 25*Power(t2,2)) + 
               2*Power(s2,3)*t2*
                (-16 + 16*t2 + 184*Power(t2,2) - 27*Power(t2,3)) + 
               Power(s2,2)*t2*
                (181 + 854*t2 - 193*Power(t2,2) + 62*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*(54 + 398*t2 - 698*Power(t2,2) - 1904*Power(t2,3) + 
                  776*Power(t2,4) - 70*Power(t2,5)) - 
               s*(36 + 6*(55 + 41*s2 + 9*Power(s2,2))*t2 + 
                  4*(-121 + 87*s2 + 22*Power(s2,2))*Power(t2,2) + 
                  (-1922 - 1710*s2 + 145*Power(s2,2))*Power(t2,3) + 
                  (1598 + 184*s2 - 133*Power(s2,2))*Power(t2,4) + 
                  4*(-20 + s2)*Power(t2,5)) + 
               Power(s,2)*t2*(260 + 213*t2 - 1381*Power(t2,2) + 
                  174*Power(t2,3) + 2*Power(t2,4) - 
                  2*s2*(-18 - 50*t2 + 119*Power(t2,2) + 52*Power(t2,3))))\
) + Power(s1,4)*(-(Power(t1,9)*
               (2 + (-13 - 4*s + 4*s2)*t2 + Power(t2,2))) - 
            Power(t1,8)*(2 + (-55 - 16*s + 5*s2)*t2 + 
               (2 + 20*s - 11*s2)*Power(t2,2) + 7*Power(t2,3)) + 
            Power(t1,7)*(4 + (52 + 27*s - s2)*t2 + 
               (-680 + 26*s + 80*Power(s,2) + 200*s2 - 72*s*s2)*
                Power(t2,2) + (-105 + 97*s + 13*s2)*Power(t2,3) + 
               11*Power(t2,4)) + 
            Power(t1,6)*(2 + (-18 - 34*s + 5*s2)*t2 + 
               (-944 + 8*Power(s,3) + Power(s,2)*(237 - 6*s2) + 
                  129*s2 + 4*Power(s2,2) - 
                  s*(349 + 56*s2 + 4*Power(s2,2)))*Power(t2,2) + 
               (1238 - 263*Power(s,2) - 684*s2 + 4*Power(s2,2) + 
                  s*(349 + 320*s2))*Power(t2,3) + 
               (261 - 164*s + 36*s2)*Power(t2,4) - 13*Power(t2,5)) + 
            Power(t1,5)*(-2 + (-75 - 35*s + 11*s2)*t2 + 
               (-62 + 27*Power(s,3) - 21*s2 + 9*Power(s2,2) + 
                  Power(s,2)*(-99 + 17*s2) + 
                  s*(-383 + 62*s2 - 7*Power(s2,2)))*Power(t2,2) + 
               (5715 + 93*Power(s,3) - 1548*s2 - 64*Power(s2,2) + 
                  4*Power(s2,3) - 3*Power(s,2)*(404 + 29*s2) + 
                  s*(-530 + 954*s2 + 69*Power(s2,2)))*Power(t2,3) + 
               (-496 + 511*Power(s,2) + 800*s2 + 27*Power(s2,2) - 
                  152*s*(11 + 3*s2))*Power(t2,4) + 
               (-221 + 184*s - 98*s2)*Power(t2,5) + 9*Power(t2,6)) + 
            Power(t1,3)*t2*(36 + 
               (522 + 150*Power(s,2) + 43*s2 + 18*Power(s2,2) - 
                  9*s*(11 + 12*s2))*t2 + 
               (-631 - 341*Power(s,3) + 661*s2 + 52*Power(s2,2) + 
                  9*Power(s2,3) + Power(s,2)*(1256 + 137*s2) + 
                  s*(835 - 1328*s2 - 81*Power(s2,2)))*Power(t2,2) + 
               (-11642 - 499*Power(s,3) + 2059*s2 + 599*Power(s2,2) - 
                  95*Power(s2,3) + Power(s,2)*(2621 + 571*s2) + 
                  s*(2440 - 2286*s2 - 435*Power(s2,2)))*Power(t2,3) + 
               (3855 + 146*Power(s,3) - 3769*s2 - 320*Power(s2,2) + 
                  86*Power(s2,3) - 2*Power(s,2)*(1331 + 130*s2) + 
                  4*s*(1448 + 653*s2 + 7*Power(s2,2)))*Power(t2,4) + 
               (727 + 143*Power(s,2) + 739*s2 + 131*Power(s2,2) - 
                  s*(1253 + 274*s2))*Power(t2,5) + 
               (-37 + 17*s - 17*s2)*Power(t2,6)) + 
            t1*Power(t2,2)*(-64 + 
               (-491 - 192*Power(s,2) - 222*s2 + 24*Power(s2,2) + 
                  8*s*(31 + 24*s2))*t2 + 
               (485 + 262*Power(s,3) - 1484*s2 + 177*Power(s2,2) + 
                  36*Power(s2,3) - 2*Power(s,2)*(491 + 72*s2) + 
                  5*s*(53 + 202*s2 + 54*Power(s2,2)))*Power(t2,2) + 
               (5000 + 309*Power(s,3) - 269*s2 - 1092*Power(s2,2) + 
                  94*Power(s2,3) - Power(s,2)*(754 + 401*s2) + 
                  2*s*(-1042 + 890*s2 + 223*Power(s2,2)))*Power(t2,3) + 
               (-1544 - 104*Power(s,3) + 2450*s2 + 151*Power(s2,2) - 
                  349*Power(s2,3) + 2*Power(s,2)*(950 + 21*s2) + 
                  s*(-3841 - 1850*s2 + 411*Power(s2,2)))*Power(t2,4) - 
               (649 + 11*Power(s,3) + Power(s,2)*(72 - 33*s2) + 
                  677*s2 + 68*Power(s2,2) - 11*Power(s2,3) + 
                  s*(-1048 - 140*s2 + 33*Power(s2,2)))*Power(t2,5) - 
               3*(-7 + 6*s - 6*s2)*Power(t2,6)) - 
            Power(t1,4)*t2*(43 - 352*t2 - 3430*Power(t2,2) + 
               8429*Power(t2,3) + 982*Power(t2,4) - 112*Power(t2,5) + 
               2*Power(s2,3)*Power(t2,2)*(-7 + 17*t2) + 
               2*Power(s,3)*t2*(11 + 4*t2 + 95*Power(t2,2)) + 
               Power(s2,2)*t2*
                (31 + 232*t2 - 150*Power(t2,2) + 153*Power(t2,3)) + 
               s2*(6 + 240*t2 + 213*Power(t2,2) - 4137*Power(t2,3) + 
                  828*Power(t2,4) - 66*Power(t2,5)) + 
               2*s*(-12 - (327 + 95*s2 + 3*Power(s2,2))*t2 - 
                  (611 + 135*s2 + 51*Power(s2,2))*Power(t2,2) + 
                  (1010 + 1325*s2 + 99*Power(s2,2))*Power(t2,3) - 
                  (964 + 233*s2)*Power(t2,4) + 42*Power(t2,5)) + 
               Power(s,2)*t2*
                (271 + 1198*t2 - 2206*Power(t2,2) + 365*Power(t2,3) + 
                  s2*(24 + 130*t2 - 392*Power(t2,2)))) + 
            Power(t1,2)*Power(t2,2)*
             (89 - 1162*t2 - 2356*Power(t2,2) + 11377*Power(t2,3) + 
               1099*Power(t2,4) - 305*Power(t2,5) + 
               Power(s,3)*t2*
                (104 + 165*t2 + 441*Power(t2,2) + 10*Power(t2,3)) - 
               2*Power(s2,3)*t2*
                (11 + 35*t2 - 163*Power(t2,2) + 35*Power(t2,3)) + 
               Power(s2,2)*t2*
                (131 + 1218*t2 - 598*Power(t2,2) + 423*Power(t2,3) - 
                  14*Power(t2,4)) - 
               s2*(12 - 462*t2 + 2104*Power(t2,2) + 4819*Power(t2,3) - 
                  2239*Power(t2,4) + 288*Power(t2,5)) + 
               2*s*(-3 + (-332 - 209*s2 + 6*Power(s2,2))*t2 + 
                  (328 + 94*s2 - 10*Power(s2,2))*Power(t2,2) + 
                  (1130 + 2240*s2 + 173*Power(s2,2))*Power(t2,3) + 
                  (-2171 - 639*s2 + 75*Power(s2,2))*Power(t2,4) + 
                  2*(85 + 7*s2)*Power(t2,5)) + 
               Power(s,2)*t2*
                (549 + 255*t2 - 3437*Power(t2,2) + 1007*Power(t2,3) - 
                  14*Power(t2,4) + 
                  s2*(6 + 154*t2 - 946*Power(t2,2) - 90*Power(t2,3)))) - 
            Power(t2,3)*(22 - 176*t2 - 388*Power(t2,2) + 
               990*Power(t2,3) + 216*Power(t2,4) - 40*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(-14 + 79*t2 + 25*Power(t2,2)) + 
               2*Power(s2,3)*t2*
                (-14 + 133*t2 + 43*Power(t2,2) - 36*Power(t2,3)) + 
               Power(s2,2)*t2*
                (386 + 117*t2 - 280*Power(t2,2) + 13*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*(84 + 24*t2 - 583*Power(t2,2) - 637*Power(t2,3) + 
                  341*Power(t2,4) - 35*Power(t2,5)) + 
               s*(-12 - 2*(51 + 46*s2 + 42*Power(s2,2))*t2 + 
                  (458 + 210*s2 - 76*Power(s2,2))*Power(t2,2) + 
                  (484 + 1000*s2 + 245*Power(s2,2))*Power(t2,3) + 
                  (-650 - 70*s2 + 169*Power(s2,2))*Power(t2,4) - 
                  4*(-10 + s2)*Power(t2,5)) + 
               Power(s,2)*t2*(134 - 21*t2 - 596*Power(t2,2) + 
                  87*Power(t2,3) + 2*Power(t2,4) - 
                  2*s2*(-6 + 2*t2 + 161*Power(t2,2) + 61*Power(t2,3))))) \
- Power(s1,3)*t2*(-(Power(t1,9)*(7 + s + 2*t2 + s*t2 - Power(t2,2))) + 
            Power(t1,2)*t2*(42 + 
               (589 + 402*Power(s,2) + 104*s2 + 6*Power(s2,2) - 
                  2*s*(181 + 90*s2))*t2 + 
               (-3058 - 816*Power(s,3) + 1232*s2 + 361*Power(s2,2) - 
                  54*Power(s2,3) + Power(s,2)*(2383 + 386*s2) + 
                  s*(2008 - 2734*s2 - 68*Power(s2,2)))*Power(t2,2) - 
               (7948 + 688*Power(s,3) + 1375*s2 - 1338*Power(s2,2) + 
                  88*Power(s2,3) - 2*Power(s,2)*(579 + 317*s2) + 
                  s*(-5603 + 1172*s2 + 490*Power(s2,2)))*Power(t2,3) + 
               (7863 + 413*Power(s,3) - 4223*s2 - 499*Power(s2,2) + 
                  217*Power(s2,3) - Power(s,2)*(3921 + 799*s2) + 
                  s*(4249 + 4138*s2 + 169*Power(s2,2)))*Power(t2,4) + 
               (775 + 11*Power(s,3) + Power(s,2)*(374 - 33*s2) + 
                  1304*s2 + 326*Power(s2,2) - 11*Power(s2,3) + 
                  s*(-2072 - 700*s2 + 33*Power(s2,2)))*Power(t2,5) + 
               (-71 + 58*s - 58*s2)*Power(t2,6)) - 
            Power(t1,4)*(6 + (192 + 84*Power(s,2) + 37*s2 - 
                  s*(109 + 24*s2))*t2 + 
               (-337*Power(s,3) + 3*Power(s,2)*(364 + 45*s2) + 
                  s*(821 - 832*s2 - 29*Power(s2,2)) + 
                  6*(-212 + 77*s2 + 18*Power(s2,2)))*Power(t2,2) + 
               (-8579 - 417*Power(s,3) + 1403*s2 + 309*Power(s2,2) - 
                  8*Power(s2,3) + Power(s,2)*(2405 + 371*s2) + 
                  s*(2736 - 1568*s2 - 255*Power(s2,2)))*Power(t2,3) + 
               (3663 + 156*Power(s,3) - 2326*s2 - 152*Power(s2,2) + 
                  4*Power(s2,3) - 6*Power(s,2)*(325 + 54*s2) + 
                  2*s*(1341 + 949*s2 + 82*Power(s2,2)))*Power(t2,4) + 
               (519 + 107*Power(s,2) + 411*s2 + 91*Power(s2,2) - 
                  3*s*(239 + 66*s2))*Power(t2,5) + 
               (-29 + 7*s - 7*s2)*Power(t2,6)) + 
            Power(t2,2)*(-48 - 
               2*(79 + 48*Power(s,2) + 206*s2 - 24*Power(s2,2) - 
                  2*s*(37 + 48*s2))*t2 + 
               (419 + 132*Power(s,3) - 440*s2 - 610*Power(s2,2) + 
                  84*Power(s2,3) - 22*Power(s,2)*(19 + 6*s2) + 
                  s*(2 + 604*s2 + 348*Power(s2,2)))*Power(t2,2) + 
               (1356 + 88*Power(s,3) + Power(s,2)*(97 - 54*s2) + 
                  375*s2 - 243*Power(s2,2) - 302*Power(s2,3) + 
                  s*(-1217 + 130*s2 + 412*Power(s2,2)))*Power(t2,3) + 
               (-712 - 109*Power(s,3) + 817*s2 + 90*Power(s2,2) - 
                  141*Power(s2,3) + Power(s,2)*(818 + 229*s2) + 
                  s*(-1091 - 900*s2 + 21*Power(s2,2)))*Power(t2,4) - 
               (226 + 15*Power(s,3) + 269*s2 + 21*Power(s2,2) - 
                  15*Power(s2,3) - 5*Power(s,2)*(-5 + 9*s2) + 
                  s*(-407 - 46*s2 + 45*Power(s2,2)))*Power(t2,5) + 
               (9 - 9*s + 9*s2)*Power(t2,6)) - 
            Power(t1,8)*(4 + 14*Power(s,2)*t2 + (-99 + 26*s2)*t2 + 
               2*(-31 + 9*s2)*Power(t2,2) + Power(t2,3) + 
               s*(5 + (33 - 12*s2)*t2 + 12*Power(t2,2))) + 
            Power(t1,7)*(6 - 2*Power(s,3)*t2 + (265 - 22*s2)*t2 + 
               3*(-119 + 50*s2)*Power(t2,2) + (-95 + 8*s2)*Power(t2,3) + 
               Power(t2,4) + Power(s,2)*t2*(-83 + 2*s2 + 63*t2) + 
               s*(5 + 3*(25 + 4*s2)*t2 + (96 - 100*s2)*Power(t2,2) + 
                  22*Power(t2,3))) + 
            Power(t1,6)*(6 + (-103 + 9*s2)*t2 + 
               (-1913 + 464*s2 + 12*Power(s2,2))*Power(t2,2) - 
               (46 + 149*s2)*Power(t2,3) + (75 + 32*s2)*Power(t2,4) - 
               3*Power(t2,5) - 3*Power(s,3)*t2*(5 + 11*t2) + 
               s*(7 - 8*(-23 + s2)*t2 - 
                  4*(-126 + 80*s2 + 3*Power(s2,2))*Power(t2,2) + 
                  11*(39 + 16*s2)*Power(t2,3) - 58*Power(t2,4)) + 
               Power(s,2)*t2*(39 + 480*t2 - 207*Power(t2,2) + 
                  s2*(-1 + 15*t2))) + 
            Power(t1,5)*(5 + 2*(-51 + 35*s2)*t2 + 
               (-1770 + 256*s2 + 37*Power(s2,2))*Power(t2,2) + 
               (4491 - 1738*s2 - 56*Power(s2,2))*Power(t2,3) + 
               (580 + 218*s2 + 43*Power(s2,2))*Power(t2,4) - 
               2*(16 + 13*s2)*Power(t2,5) + 
               2*Power(s,3)*t2*(10 - 7*t2 + 47*Power(t2,2)) + 
               s*(-6 - 2*(169 + 20*s2)*t2 - 
                  (787 + 162*s2 + 31*Power(s2,2))*Power(t2,2) + 
                  (-691 + 1338*s2 + 99*Power(s2,2))*Power(t2,3) - 
                  12*(53 + 16*s2)*Power(t2,4) + 34*Power(t2,5)) + 
               Power(s,2)*t2*(119 + 908*t2 - 924*Power(t2,2) + 
                  177*Power(t2,3) + s2*(6 + 50*t2 - 186*Power(t2,2)))) + 
            Power(t1,3)*t2*(33 + 904*t2 + 2661*Power(t2,2) - 
               13146*Power(t2,3) - 261*Power(t2,4) + 113*Power(t2,5) + 
               4*Power(s2,3)*Power(t2,2)*(17 - 22*t2 + 4*Power(t2,2)) + 
               2*Power(s,3)*t2*
                (-101 - 164*t2 - 242*Power(t2,2) + 25*Power(t2,3)) + 
               Power(s2,2)*t2*
                (53 - 230*t2 + 312*Power(t2,2) - 373*Power(t2,3) + 
                  34*Power(t2,4)) + 
               2*s2*(3 - 107*t2 + 51*Power(t2,2) + 2284*Power(t2,3) - 
                  831*Power(t2,4) + 128*Power(t2,5)) - 
               2*s*(-6 + (-515 + 3*s2 + 3*Power(s2,2))*t2 + 
                  (-833 + 362*s2 + 54*Power(s2,2))*Power(t2,2) + 
                  (-449 + 1954*s2 + 282*Power(s2,2))*Power(t2,3) - 
                  9*(173 + 77*s2 + Power(s2,2))*Power(t2,4) + 
                  2*(79 + 17*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(-317 - 1020*t2 + 2780*Power(t2,2) - 
                  1181*Power(t2,3) + 34*Power(t2,4) - 
                  4*s2*(3 - 29*t2 - 258*Power(t2,2) + 21*Power(t2,3)))) + 
            t1*Power(t2,2)*(-80 - 1543*t2 - 75*Power(t2,2) + 
               8327*Power(t2,3) + 249*Power(t2,4) - 302*Power(t2,5) + 
               2*Power(s,3)*t2*
                (98 + 185*t2 + 196*Power(t2,2) + Power(t2,3)) - 
               2*Power(s2,3)*t2*
                (6 + 52*t2 - 117*Power(t2,2) + 79*Power(t2,3)) + 
               Power(s2,2)*t2*
                (16 + 1285*t2 - 679*Power(t2,2) + 248*Power(t2,3) - 
                  22*Power(t2,4)) - 
               2*s2*(30 - 75*t2 + 1417*Power(t2,2) + 1164*Power(t2,3) - 
                  985*Power(t2,4) + 165*Power(t2,5)) + 
               2*s*(42 + (70 + 32*s2 + 30*Power(s2,2))*t2 + 
                  (778 + 389*s2 + 197*Power(s2,2))*Power(t2,2) + 
                  (-481 + 1805*s2 + 270*Power(s2,2))*Power(t2,3) + 
                  (-1706 - 588*s2 + 159*Power(s2,2))*Power(t2,4) + 
                  (193 + 22*s2)*Power(t2,5)) - 
               Power(s,2)*t2*(-216 + 863*t2 + 2035*Power(t2,2) - 
                  1080*Power(t2,3) + 22*Power(t2,4) + 
                  2*s2*(42 + 58*t2 + 471*Power(t2,2) + 81*Power(t2,3))))))*
       R1q(t2))/((-1 + s2)*(-s + s2 - t1)*
       Power(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2,2)*
       (-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)*t2*
       Power(-s1 + t2,3)) + (8*
       (-2*Power(s1,8)*(Power(s2,3) + 2*s2*Power(t1,2) + 
            Power(t1,2)*(-2 + 3*s + t1)) - 
         2*Power(-1 + s,2)*Power(t1,7)*(-1 + s + t2) - 
         4*t1*t2*(7 + (-40 - 27*Power(s,2) + s*(73 - 30*s2) + 31*s2 + 
               9*Power(s2,2))*t2 + 
            (44 + 5*Power(s,3) - 59*s2 + 44*Power(s2,2) + 
               3*Power(s2,3) - Power(s,2)*(72 + 23*s2) + 
               s*(31 + 48*s2 - 25*Power(s2,2)))*Power(t2,2) + 
            (70 + 19*Power(s,3) + 15*s2 - 21*Power(s2,2) + 
               9*Power(s2,3) - Power(s,2)*(21 + 41*s2) + 
               s*(-67 + 30*s2 + 13*Power(s2,2)))*Power(t2,3) + 
            (29 + s + 4*Power(s,2) - 9*s2 - 12*s*s2 + 8*Power(s2,2))*
             Power(t2,4) + 2*(1 + s - s2)*Power(t2,5)) + 
         Power(t1,3)*(4 + (-115 + 214*s - 84*Power(s,2) + 10*s2 - 
               12*s*s2)*t2 + (142 + 6*Power(s,3) - 163*s2 + 
               29*Power(s2,2) - 6*Power(s2,3) - 
               Power(s,2)*(139 + 114*s2) + 
               s*(31 + 246*s2 + 2*Power(s2,2)))*Power(t2,2) + 
            (140 + 41*Power(s,3) + 25*s2 - 28*Power(s2,2) + 
               15*Power(s2,3) - Power(s,2)*(68 + 91*s2) + 
               5*s*(-13 + 8*s2 + 7*Power(s2,2)))*Power(t2,3) - 
            (-54 + 3*Power(s,3) + s2 - 31*Power(s2,2) - 
               3*Power(s2,3) - 9*Power(s,2)*(3 + s2) + 
               s*(19 + 58*s2 + 9*Power(s2,2)))*Power(t2,4) + 
            (-1 - s + s2)*Power(t2,5)) + 
         Power(t1,6)*(-10 + (15 - 8*s2)*t2 - 2*(-6 + s2)*Power(t2,2) + 
            Power(s,3)*(4 + 5*t2) - 
            Power(s,2)*(18 + (5 + 8*s2)*t2 - 4*Power(t2,2)) + 
            s*(24 + (-15 + 16*s2)*t2 - 2*(1 + 2*s2)*Power(t2,2))) + 
         Power(t1,5)*(18 + (-31 + 26*s2)*t2 + 
            Power(s,3)*(10 - 7*t2)*t2 + 
            2*(-4 + s2 + 2*Power(s2,2))*Power(t2,2) - 
            (7 + 2*s2 + 2*Power(s2,2))*Power(t2,3) + 
            Power(s,2)*(12 + (-11 + 16*s2)*t2 + 
               (22 + 17*s2)*Power(t2,2) - 2*Power(t2,3)) + 
            s*(-30 - 6*(-4 + 7*s2)*t2 - 
               (21 + 7*s2 + 10*Power(s2,2))*Power(t2,2) + 
               (5 + 4*s2)*Power(t2,3))) - 
         Power(s1,7)*(Power(s2,3)*(-2 + 11*t1 - 6*t2) + 
            2*Power(s2,2)*(-3 + 3*Power(t1,2) + t1*(2 + s - t2) + 
               3*s*t2) + s2*t1*
             (23*Power(t1,2) - 8*t2 - t1*(27 + t2) + s*(-6 + t1 + 6*t2)) \
+ t1*(12*Power(t1,3) + 4*(2 - 3*s)*t2 + Power(t1,2)*(-39 + 33*s + t2) + 
               2*t1*(5 + s - 3*Power(s,2) + 4*t2 - 6*s*t2))) + 
         Power(t1,4)*(-14 + (65 - 28*s2)*t2 + 
            (-130 + 67*s2 - 17*Power(s2,2))*Power(t2,2) - 
            (59 + 14*s2 - 14*Power(s2,2) + 4*Power(s2,3))*Power(t2,3) + 
            (2 + 7*s2)*Power(t2,4) + 
            7*Power(s,3)*t2*(-4 - 5*t2 + Power(t2,2)) + 
            s*(12 + (-143 + 38*s2)*t2 + 
               (158 - 74*s2 + 17*Power(s2,2))*Power(t2,2) + 
               (29 + 26*s2 + 15*Power(s2,2))*Power(t2,3) - 8*Power(t2,4)\
) - Power(s,2)*t2*(-128 + 9*t2 + 38*Power(t2,2) + 
               6*s2*(2 - 7*t2 + 3*Power(t2,2)))) - 
         2*Power(t1,2)*t2*(-48 - 
            (-24 + 57*s2 + 20*Power(s2,2) + 2*Power(s2,3))*t2 - 
            (136 - 61*s2 + 6*Power(s2,2) + 5*Power(s2,3))*Power(t2,2) + 
            3*(8 - 21*s2 + 18*Power(s2,2) + Power(s2,3))*Power(t2,3) + 
            s2*(27 + 8*s2)*Power(t2,4) + 
            Power(s,3)*t2*(-20 - 27*t2 + 11*Power(t2,2)) + 
            s*(42 + (-71 + 160*s2)*t2 + 
               (275 - 56*s2 + 71*Power(s2,2))*Power(t2,2) + 
               (31 + 16*s2 + 5*Power(s2,2))*Power(t2,3) - 
               (29 + 16*s2)*Power(t2,4)) + 
            Power(s,2)*t2*(92 - 74*t2 - 62*Power(t2,2) + 8*Power(t2,3) - 
               s2*(42 + 15*t2 + 19*Power(t2,2)))) + 
         8*Power(t2,2)*(Power(s2,3)*t2*(1 + t2 + 2*Power(t2,2)) + 
            Power(s,3)*t2*(1 - 3*t2 + 6*Power(t2,2)) + 
            2*Power(s2,2)*t2*(2 + 11*t2 + 7*Power(t2,2) + Power(t2,3)) + 
            2*(-7 - 13*t2 - 4*Power(t2,2) + 23*Power(t2,3) + 
               Power(t2,4)) + 
            s2*(9 + 11*t2 - 15*Power(t2,2) - 31*Power(t2,3) + 
               10*Power(t2,4)) + 
            s*(9 + (11 + 36*s2 - 9*Power(s2,2))*t2 + 
               (85 - 24*s2 + 19*Power(s2,2))*Power(t2,2) + 
               (1 + 20*s2 + 2*Power(s2,2))*Power(t2,3) - 
               2*(5 + 2*s2)*Power(t2,4)) + 
            Power(s,2)*t2*(-(s2*(9 + 17*t2 + 10*Power(t2,2))) + 
               2*(2 - 11*t2 - 15*Power(t2,2) + Power(t2,3)))) - 
         Power(s1,6)*(39*Power(t1,2) + 78*Power(t1,3) - 
            128*Power(t1,4) + 28*Power(t1,5) - 20*t1*t2 + 
            114*Power(t1,2)*t2 + 8*Power(t1,3)*t2 + 4*Power(t1,4)*t2 - 
            4*Power(t2,2) - 22*t1*Power(t2,2) - 
            19*Power(t1,2)*Power(t2,2) - 3*Power(t1,3)*Power(t2,2) + 
            Power(s,2)*t1*(-36*Power(t1,2) + 6*t2*(1 + t2) + 
               t1*(-4 + 19*t2)) + 
            Power(s2,3)*(-4 + 24*Power(t1,2) - 9*t2 + 6*Power(t2,2) - 
               t1*(2 + 33*t2)) + 
            s*(70*Power(t1,4) + Power(t1,3)*(20 - 53*t2) + 
               6*Power(t2,2) + 2*t1*(3 - 5*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(-11 - 162*t2 + 17*Power(t2,2))) + 
            Power(s2,2)*(30*Power(t1,3) + 
               Power(t1,2)*(11 + 9*s - 13*t2) + 
               2*(3 - 7*(-1 + s)*t2 + (1 - 6*s)*Power(t2,2)) + 
               t1*(-41 - 22*t2 + 4*Power(t2,2) + 5*s*(-2 + 7*t2))) + 
            s2*(6 + 58*Power(t1,4) + Power(t1,3)*(-137 + 11*s - 2*t2) + 
               4*Power(t2,2) + 6*Power(s,2)*Power(t2,2) - 
               6*s*t2*(1 + t2) + 
               Power(t1,2)*(-18 + Power(s,2) - 23*t2 - 12*Power(t2,2) + 
                  4*s*(-5 + 4*t2)) - 
               2*t1*(4 - 29*t2 + Power(s,2)*t2 - Power(t2,2) + 
                  s*(-4 + 5*Power(t2,2))))) + 
         Power(s1,4)*(-2 - 18*Power(t1,7) - 2*t2 + 2*s*t2 - 
            41*Power(t2,2) + 21*s*Power(t2,2) + 
            6*Power(s,2)*Power(t2,2) - 36*Power(t2,3) + 
            106*s*Power(t2,3) - 39*Power(s,2)*Power(t2,3) + 
            4*Power(s,3)*Power(t2,3) + 17*Power(t2,4) - 
            17*s*Power(t2,4) - 2*Power(s,2)*Power(t2,4) - 
            2*Power(t1,6)*(-84 + 15*s + 2*t2) + 
            Power(t1,5)*(-218 + 104*Power(s,2) + 75*t2 + 
               9*Power(t2,2) + s*(-96 + 78*t2)) + 
            Power(t1,4)*(-210 - 4*Power(s,3) + 
               Power(s,2)*(20 - 130*t2) - 739*t2 + 17*Power(t2,2) + 
               s*(186 + 545*t2 - 75*Power(t2,2))) + 
            Power(s2,3)*(-14*Power(t1,4) + Power(t1,3)*(-28 + 78*t2) + 
               Power(t1,2)*(24 + 67*t2 - 61*Power(t2,2)) + 
               t1*t2*(-18 - 145*t2 + 11*Power(t2,2)) + 
               t2*(-28 + 17*t2 + 43*Power(t2,2))) + 
            Power(t1,3)*(-61 + 1050*t2 + 140*Power(t2,2) - 
               37*Power(t2,3) + Power(s,3)*(-26 + 28*t2) + 
               Power(s,2)*(-4 - 389*t2 + 53*Power(t2,2)) + 
               s*(138 + 28*t2 - 441*Power(t2,2) + 17*Power(t2,3))) + 
            Power(t1,2)*(73 + 450*t2 + 452*Power(t2,2) - 
               106*Power(t2,3) + Power(s,3)*(4 + 8*t2 + Power(t2,2)) - 
               Power(s,2)*(42 + 40*t2 - 313*Power(t2,2) + 
                  14*Power(t2,3)) + 
               s*(61 - 468*t2 - 958*Power(t2,2) + 200*Power(t2,3))) + 
            t1*(19 + 82*t2 - 542*Power(t2,2) - 152*Power(t2,3) + 
               21*Power(t2,4) + Power(s,2)*t2*(-4 + 165*t2) + 
               Power(s,3)*t2*(12 - 18*t2 - 11*Power(t2,2)) + 
               s*(16 - 137*t2 + 38*Power(t2,2) + 291*Power(t2,3) - 
                  18*Power(t2,4))) + 
            Power(s2,2)*(-60*Power(t1,5) + 
               Power(t1,4)*(14 - 14*s + 38*t2) + 
               Power(t1,3)*(156 + s*(60 - 74*t2) + 209*t2 + 
                  41*Power(t2,2)) + 
               Power(t1,2)*(44 + (-203 + 57*s)*t2 + 
                  3*(-45 + 41*s)*Power(t2,2) - 14*Power(t2,3)) - 
               t1*(48 + (265 + 58*s)*t2 + (73 - 89*s)*Power(t2,2) + 
                  (-4 + 33*s)*Power(t2,3)) + 
               t2*(56 + 102*t2 + 21*Power(t2,2) - 2*Power(t2,3) - 
                  2*s*(6 + 43*t2 + 41*Power(t2,2)))) + 
            s2*(12 - 68*Power(t1,6) + Power(t1,5)*(282 - 46*s - 8*t2) + 
               (43 + 14*s)*t2 + 
               (20 - 56*s - 19*Power(s,2))*Power(t2,2) + 
               (-31 - 12*s + 35*Power(s,2))*Power(t2,3) + 
               4*(3 + s)*Power(t2,4) + 
               Power(t1,4)*(44 + 2*Power(s,2) + 85*t2 + 
                  57*Power(t2,2) + 8*s*(-2 + 5*t2)) - 
               Power(t1,3)*(56 + 1072*t2 - 73*Power(t2,2) + 
                  17*Power(t2,3) + 8*Power(s,2)*(-1 + 4*t2) + 
                  2*s*(60 - 145*t2 + 47*Power(t2,2))) - 
               Power(t1,2)*(165 + 43*t2 - 226*Power(t2,2) + 
                  148*Power(t2,3) + 7*Power(s,2)*t2*(-5 + 9*t2) + 
                  s*(48 - 162*t2 + 26*Power(t2,2) - 28*Power(t2,3))) + 
               t1*(-14 + 107*t2 + 555*Power(t2,2) - 68*Power(t2,3) + 
                  18*Power(t2,4) + 
                  Power(s,2)*t2*(-30 + 74*t2 + 33*Power(t2,2)) + 
                  s*(12 + 84*t2 - 277*Power(t2,2) - 4*Power(t2,3))))) + 
         Power(s1,5)*(-32*Power(t1,6) + 
            Power(t1,5)*(202 - 70*s - 6*t2) - 
            2*(-1 + (5 + 4*s - 3*Power(s,2))*Power(t2,2) + 
               (6 - 6*s - 3*Power(s,2) + Power(s,3))*Power(t2,3)) + 
            Power(t1,4)*(-196 + 86*Power(s,2) + 41*t2 + 9*Power(t2,2) + 
               s*(-64 + 92*t2)) + 
            Power(s2,3)*(-26*Power(t1,3) + 12*Power(t1,2)*(-1 + 6*t2) + 
               t1*(16 + 43*t2 - 33*Power(t2,2)) + 
               t2*(-22 - 39*t2 + 2*Power(t2,2))) - 
            Power(t1,3)*(141 + Power(s,3) + 447*t2 - 35*Power(t2,2) + 
               5*Power(s,2)*(-3 + 16*t2) + 
               s*(-79 - 479*t2 + 59*Power(t2,2))) + 
            Power(s2,2)*(-12 - 60*Power(t1,4) - 
               4*Power(t1,3)*(1 + 4*s - 8*t2) - (35 + 4*s)*t2 + 
               (2 + 23*s)*Power(t2,2) + (4 - 6*s)*Power(t2,3) + 
               Power(t1,2)*(114 + s*(40 - 76*t2) + 111*t2 + 
                  5*Power(t2,2)) + 
               t1*(4 + (-73 + 47*s)*t2 + 10*(-4 + 7*s)*Power(t2,2) + 
                  2*Power(t2,3))) + 
            Power(t1,2)*(-36 + 386*t2 + 112*Power(t2,2) - 
               15*Power(t2,3) + Power(s,3)*(-6 + 7*t2) + 
               Power(s,2)*(-1 - 117*t2 + 5*Power(t2,2)) + 
               s*(36 + 7*t2 - 223*Power(t2,2) + 9*Power(t2,3))) + 
            t1*(-4 + 80*t2 + 111*Power(t2,2) + 
               4*Power(s,3)*Power(t2,2) - 35*Power(t2,3) + 
               2*Power(s,2)*t2*(-14 + 26*t2 + Power(t2,2)) + 
               s*(10 - 20*t2 - 235*Power(t2,2) + 34*Power(t2,3))) + 
            s2*(6 - 82*Power(t1,5) - 2*(-5 + 8*s)*t2 + 
               (31 - 23*s + 16*Power(s,2))*Power(t2,2) + 
               (1 - 10*s + 6*Power(s,2))*Power(t2,3) - 
               2*Power(t1,4)*(-139 + 17*s + t2) + 
               Power(t1,3)*(50 + 16*s - 2*Power(s,2) + 37*t2 + 
                  43*Power(t2,2)) - 
               Power(t1,2)*(8 + 469*t2 - 29*Power(t2,2) + 
                  9*Power(t2,3) + Power(s,2)*(-2 + 3*t2) + 
                  2*s*(27 - 38*t2 + 5*Power(t2,2))) - 
               t1*(49 + 56*t2 - 31*Power(t2,2) + 24*Power(t2,3) + 
                  Power(s,2)*t2*(-14 + 41*t2) + 
                  s*(26 - 60*t2 - 40*Power(t2,2) + 4*Power(t2,3))))) + 
         Power(s1,3)*(-4 - 4*Power(t1,8) + 
            (-17 - 46*s2 + 84*Power(s2,2) + 2*s*(-5 + 6*s2))*t2 + 
            (-40 - 6*Power(s,3) - 117*s2 + 43*Power(s2,2) + 
               78*Power(s2,3) + Power(s,2)*(11 + 10*s2) + 
               s*(89 + 66*s2 + 30*Power(s2,2)))*Power(t2,2) + 
            (234 + 3*Power(s,3) - 217*s2 - 38*Power(s2,2) + 
               45*Power(s2,3) - Power(s,2)*(78 + 113*s2) + 
               s*(-31 + 236*s2 + 65*Power(s2,2)))*Power(t2,3) + 
            (60 + 15*Power(s,3) + 37*s2 - 9*Power(s2,2) - 
               15*Power(s2,3) - 5*Power(s,2)*(1 + 9*s2) + 
               s*(-121 + 14*s2 + 45*Power(s2,2)))*Power(t2,4) + 
            9*(-1 + s - s2)*Power(t2,5) - 
            Power(t1,7)*(-71 + s + 31*s2 + t2) + 
            Power(t1,6)*(-108 + 66*Power(s,2) - 30*Power(s2,2) + 
               s2*(143 - 7*t2) + 35*t2 + 3*Power(t2,2) + 
               s*(-74 - 29*s2 + 32*t2)) + 
            Power(t1,5)*(-166 - 6*Power(s,3) - 3*Power(s2,3) + 
               2*Power(s,2)*(5 + 4*s2 - 50*t2) - 577*t2 + 
               5*Power(t2,2) + 2*Power(s2,2)*(8 + 11*t2) + 
               s2*(12 + 119*t2 + 33*Power(t2,2)) + 
               s*(194 - 6*Power(s2,2) + 241*t2 - 41*Power(t2,2) + 
                  s2*(-34 + 50*t2))) + 
            Power(t1,4)*(-1 + 1030*t2 + 46*Power(t2,2) - 
               29*Power(t2,3) + Power(s,3)*(-44 + 42*t2) + 
               Power(s2,3)*(-22 + 42*t2) + 
               Power(s2,2)*(104 + 181*t2 + 53*Power(t2,2)) - 
               s2*(52 + 1006*t2 - 55*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s,2)*(s2*(12 - 58*t2) + 3*t2*(-165 + 23*t2)) + 
               s*(144 + Power(s2,2)*(40 - 26*t2) + 122*t2 - 
                  319*Power(t2,2) + 7*Power(t2,3) - 
                  2*s2*(58 - 183*t2 + 61*Power(t2,2)))) + 
            t1*(8 - 175*t2 - 491*Power(t2,2) - 73*Power(t2,3) + 
               123*Power(t2,4) + 
               Power(s,3)*t2*(-8 - 29*t2 + 25*Power(t2,2)) + 
               Power(s2,3)*t2*(-52 - 19*t2 + 131*Power(t2,2)) + 
               Power(s2,2)*t2*
                (8 + 459*t2 + 183*Power(t2,2) + 22*Power(t2,3)) + 
               s2*(48 + 419*t2 - 83*Power(t2,2) - 359*Power(t2,3) + 
                  167*Power(t2,4)) - 
               s*(12 + (127 - 32*s2 + 48*Power(s2,2))*t2 + 
                  (-787 + 390*s2 + 111*Power(s2,2))*Power(t2,2) + 
                  3*(-261 - 38*s2 + 79*Power(s2,2))*Power(t2,3) + 
                  (223 + 44*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (128 - 13*t2 - 449*Power(t2,2) + 22*Power(t2,3) + 
                  s2*(12 - 65*t2 + 81*Power(t2,2)))) + 
            Power(t1,3)*(131 + 727*t2 + 798*Power(t2,2) - 
               16*Power(t2,3) + 
               Power(s2,3)*(16 + 41*t2 - 45*Power(t2,2)) + 
               Power(s,3)*(16 + 37*t2 - 21*Power(t2,2)) + 
               s2*(-233 + 107*t2 + 316*Power(t2,2) - 230*Power(t2,3)) - 
               Power(s2,2)*(-52 + 261*t2 + 171*Power(t2,2) + 
                  34*Power(t2,3)) - 
               Power(s,2)*(140 + (-53 + 11*s2)*t2 + 
                  (-541 + 3*s2)*Power(t2,2) + 34*Power(t2,3)) + 
               s*(127 - 1139*t2 - 1208*Power(t2,2) + 290*Power(t2,3) + 
                  Power(s2,2)*t2*(37 + 69*t2) + 
                  s2*(4 + 206*t2 - 202*Power(t2,2) + 68*Power(t2,3)))) + 
            Power(t1,2)*(61 + 117*t2 - 2106*Power(t2,2) - 
               375*Power(t2,3) + 71*Power(t2,4) + 
               Power(s,3)*t2*(58 - 83*t2 - 11*Power(t2,2)) + 
               Power(s2,3)*t2*(42 - 187*t2 + 11*Power(t2,2)) - 
               Power(s2,2)*(60 + 455*t2 + 249*Power(t2,2) + 
                  70*Power(t2,3)) + 
               Power(s,2)*(24 + (67 - 22*s2)*t2 + 
                  (603 + 169*s2)*Power(t2,2) + 
                  (-118 + 33*s2)*Power(t2,3)) + 
               s2*(10 + 233*t2 + 1619*Power(t2,2) - 196*Power(t2,3) + 
                  58*Power(t2,4)) + 
               s*(-62 - 437*t2 + 371*Power(t2,2) + 790*Power(t2,3) - 
                  58*Power(t2,4) + 
                  Power(s2,2)*t2*(-146 + 101*t2 - 33*Power(t2,2)) + 
                  2*s2*(6 + 131*t2 - 344*Power(t2,2) + 94*Power(t2,3))))) \
+ s1*(2*Power(-1 + s,2)*Power(t1,8) + 
            Power(t1,2)*(16 - 
               (163 + 132*Power(s,2) + 70*s2 - 72*Power(s2,2) + 
                  s*(-398 + 36*s2))*t2 + 
               (284 - 54*Power(s,3) - 159*s2 + 113*Power(s2,2) - 
                  2*Power(s2,3) - Power(s,2)*(351 + 166*s2) + 
                  s*(251 + 134*s2 + 78*Power(s2,2)))*Power(t2,2) + 
               (1174 + 129*Power(s,3) - 431*s2 + 98*Power(s2,2) + 
                  47*Power(s2,3) - 3*Power(s,2)*(130 + 89*s2) + 
                  s*(-601 + 332*s2 + 91*Power(s2,2)))*Power(t2,3) + 
               (332 - 7*Power(s,3) - 21*s2 + 101*Power(s2,2) + 
                  7*Power(s2,3) + Power(s,2)*(113 + 21*s2) - 
                  s*(167 + 214*s2 + 21*Power(s2,2)))*Power(t2,4) + 
               (5 + 23*s - 23*s2)*Power(t2,5)) - 
            4*t2*(-7 + (-18 - 9*Power(s,2) - 55*s2 + 27*Power(s2,2) + 
                  s*(23 + 30*s2))*t2 + 
               (2 + 3*Power(s,3) - 29*s2 - 22*Power(s2,2) + 
                  13*Power(s2,3) - Power(s,2)*(10 + 17*s2) + 
                  s*(41 + 44*s2 + 41*Power(s2,2)))*Power(t2,2) - 
               (-152 + 3*Power(s,3) + 87*s2 - 15*Power(s2,2) + 
                  9*Power(s2,3) + Power(s,2)*(9 + 31*s2) + 
                  s*(69 - 54*s2 - 43*Power(s2,2)))*Power(t2,3) + 
               (21 + 4*Power(s,3) + Power(s,2)*(4 - 12*s2) + 17*s2 - 
                  8*Power(s2,2) - 4*Power(s2,3) + 
                  s*(-49 + 4*s2 + 12*Power(s2,2)))*Power(t2,4) + 
               6*(-1 + s - s2)*Power(t2,5)) - 
            Power(t1,7)*(13 + Power(s,3) + s*(-15 - 4*s2*(-1 + t2)) + 
               24*t2 - 2*s2*(1 + 4*t2) + Power(s,2)*(1 - 2*s2 + 4*t2)) + 
            Power(t1,3)*(-52 - 53*t2 - 653*Power(t2,2) - 
               151*Power(t2,3) - 3*Power(t2,4) + 
               Power(s2,3)*t2*(-8 - 33*t2 + 29*Power(t2,2)) + 
               Power(s,3)*t2*(-52 - 103*t2 + 55*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-100 + 233*t2 + 161*Power(t2,2) + 34*Power(t2,3)) + 
               s2*(-12 + 209*t2 - 37*Power(t2,2) - 237*Power(t2,3) + 
                  141*Power(t2,4)) + 
               s*(48 + (-253 + 104*s2 + 12*Power(s2,2))*t2 + 
                  (1365 - 290*s2 + 67*Power(s2,2))*Power(t2,2) + 
                  (301 + 206*s2 - 3*Power(s2,2))*Power(t2,3) - 
                  (157 + 68*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (356 - 303*t2 - 391*Power(t2,2) + 34*Power(t2,3) + 
                  s2*(-48 + 101*t2 - 81*Power(t2,2)))) + 
            Power(t1,6)*(17 + 7*Power(s,3)*(-2 + t2) + 24*t2 + 
               10*Power(t2,2) + 2*Power(s2,2)*t2*(4 + t2) + 
               Power(s,2)*(17 + s2*(2 - 11*t2) - 48*t2 + 
                  2*Power(t2,2)) + s2*(4 - 37*t2 + 4*Power(t2,2)) + 
               s*(-12 + 55*t2 + 4*Power(s2,2)*t2 - 10*Power(t2,2) - 
                  2*s2*(3 - 7*t2 + 2*Power(t2,2)))) - 
            4*t1*t2*(-29 - 141*t2 - 103*Power(t2,2) + 201*Power(t2,3) + 
               8*Power(t2,4) + 
               Power(s,3)*t2*(-1 - 20*t2 + 21*Power(t2,2)) + 
               Power(s2,3)*t2*(-1 - 20*t2 + 29*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-51 + 217*t2 + 68*Power(t2,2) + 18*Power(t2,3)) + 
               s2*(15 + 190*t2 - 140*Power(t2,2) - 174*Power(t2,3) + 
                  69*Power(t2,4)) + 
               s*(15 + (-26 + 62*s2 - 15*Power(s2,2))*t2 + 
                  (528 - 206*s2 + 48*Power(s2,2))*Power(t2,2) + 
                  (82 + 108*s2 - 37*Power(s2,2))*Power(t2,3) - 
                  3*(25 + 12*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (69 - 87*t2 - 184*Power(t2,2) + 18*Power(t2,3) - 
                  s2*(15 + 24*t2 + 13*Power(t2,2)))) - 
            Power(t1,5)*(23 - 197*t2 - 191*Power(t2,2) - 
               2*Power(s2,3)*Power(t2,2) + 5*Power(t2,3) + 
               Power(s2,2)*t2*(18 + 13*t2) + 
               Power(s,3)*(-16 - 31*t2 + 11*Power(t2,2)) + 
               Power(s,2)*(92 + (-39 + 55*s2)*t2 - 
                  (79 + 24*s2)*Power(t2,2)) + 
               s2*(26 + 27*t2 + 19*Power(t2,2) + 14*Power(t2,3)) + 
               s*(-83 + 273*t2 + 53*Power(t2,2) - 16*Power(t2,3) + 
                  Power(s2,2)*t2*(-8 + 15*t2) + 
                  s2*(-22 - 70*t2 + 62*Power(t2,2)))) + 
            Power(t1,4)*(53 - 157*t2 - 404*Power(t2,2) - 
               109*Power(t2,3) + 5*Power(t2,4) + 
               Power(s2,3)*t2*(12 - 14*t2 - 5*Power(t2,2)) + 
               Power(s,3)*t2*(54 - 61*t2 + 5*Power(t2,2)) - 
               Power(s2,2)*t2*(2 + 53*t2 + 54*Power(t2,2)) + 
               Power(s,2)*(48 + (65 + 70*s2)*t2 + 
                  (259 + 103*s2)*Power(t2,2) - (56 + 15*s2)*Power(t2,3)) \
+ s2*(32 + 45*t2 + 226*Power(t2,2) - 9*Power(t2,3) - 2*Power(t2,4)) + 
               s*(-130 - 43*t2 - 15*Power(t2,2) + 74*Power(t2,3) + 
                  2*Power(t2,4) + 
                  Power(s2,2)*t2*(-42 - 28*t2 + 15*Power(t2,2)) + 
                  s2*(-12 - 78*t2 - 129*Power(t2,2) + 110*Power(t2,3))))) \
+ Power(s1,2)*(2*(6 + s - 3*s2)*Power(t1,8) + 
            t1*(-16 + (-169 - 72*Power(s,2) - 86*s2 + 
                  132*Power(s2,2) + 2*s*(83 + 18*s2))*t2 - 
               (58 + 10*Power(s,3) + 377*s2 - 439*Power(s2,2) + 
                  6*Power(s2,3) + Power(s,2)*(145 + 18*s2) + 
                  s*(-541 + 62*s2 - 178*Power(s2,2)))*Power(t2,2) + 
               (1860 + 67*Power(s,3) - 1185*s2 + 136*Power(s2,2) + 
                  133*Power(s2,3) - 3*Power(s,2)*(128 + 83*s2) + 
                  s*(-599 + 720*s2 + 49*Power(s2,2)))*Power(t2,3) + 
               (306 + 27*Power(s,3) - 81*Power(s,2)*(-1 + s2) + 
                  193*s2 - 3*Power(s2,2) - 27*Power(s2,3) + 
                  s*(-637 - 78*s2 + 81*Power(s2,2)))*Power(t2,4) + 
               (-67 + 65*s - 65*s2)*Power(t2,5)) + 
            Power(t1,7)*(-16 + 20*Power(s,2) - 6*Power(s2,2) + 
               s2*(29 - 2*t2) + t2 + s*(-28 - 7*s2 + 5*t2)) + 
            Power(t1,6)*(-71 - 4*Power(s,3) + 7*Power(s,2)*(s2 - 5*t2) - 
               203*t2 + 4*Power(t2,2) + 5*Power(s2,2)*(1 + t2) + 
               s2*(2 + 64*t2 + 7*Power(t2,2)) - 
               s*(-91 + Power(s2,2) - 25*t2 + 8*Power(t2,2) - 
                  4*s2*(-5 + 6*t2))) + 
            Power(t1,2)*(-52 - 459*t2 - 861*Power(t2,2) + 
               175*Power(t2,3) + 21*Power(t2,4) + 
               Power(s,3)*t2*(-32 - 81*t2 + 69*Power(t2,2)) + 
               Power(s2,3)*t2*(-32 - 53*t2 + 121*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-156 + 759*t2 + 293*Power(t2,2) + 90*Power(t2,3)) + 
               s2*(24 + 677*t2 - 567*Power(t2,2) - 767*Power(t2,3) + 
                  369*Power(t2,4)) + 
               s*(24 + (-271 + 100*s2 - 24*Power(s2,2))*t2 + 
                  (2349 - 630*s2 + 41*Power(s2,2))*Power(t2,2) + 
                  (1053 + 434*s2 - 173*Power(s2,2))*Power(t2,3) - 
                  15*(29 + 12*s2)*Power(t2,4)) + 
               Power(s,2)*t2*(348 - 257*t2 - 907*Power(t2,2) + 
                  90*Power(t2,3) - s2*(24 + 35*t2 + 17*Power(t2,2)))) + 
            Power(t1,5)*(39 + 372*t2 + 38*Power(t2,2) - 7*Power(t2,3) + 
               4*Power(s,3)*(-9 + 7*t2) + Power(s2,3)*(-6 + 9*t2) + 
               Power(s2,2)*(27 + 69*t2 + 23*Power(t2,2)) + 
               Power(s,2)*(14 + s2*(8 - 42*t2) - 263*t2 + 
                  29*Power(t2,2)) + 
               s2*(-8 - 382*t2 + 17*Power(t2,2) + Power(t2,3)) + 
               s*(42 + 142*t2 - 87*Power(t2,2) - Power(t2,3) + 
                  5*Power(s2,2)*(2 + t2) - 
                  2*s2*(24 - 83*t2 + 26*Power(t2,2)))) - 
            2*t2*(-6 - (54 + 9*s - 34*Power(s,2) + 2*Power(s,3))*t2 - 
               (90 - 205*s + 40*Power(s,2) + 9*Power(s,3))*Power(t2,2) + 
               (30 + 117*s - 114*Power(s,2) + 23*Power(s,3))*
                Power(t2,3) + (24 - 41*s + 4*Power(s,2))*Power(t2,4) + 
               Power(s2,3)*t2*(-20 + 49*t2 + 31*Power(t2,2)) + 
               s2*(42 + (73 + 4*s)*t2 - 
                  (53 + 28*s + 37*Power(s,2))*Power(t2,2) + 
                  (-77 + 56*s - 15*Power(s,2))*Power(t2,3) + 
                  (31 - 8*s)*Power(t2,4)) + 
               Power(s2,2)*t2*
                (98 + 84*t2 + 34*Power(t2,2) + 4*Power(t2,3) - 
                  s*(42 + 59*t2 + 39*Power(t2,2)))) + 
            Power(t1,4)*(41 + 547*t2 + 608*Power(t2,2) + 2*Power(t2,3) + 
               Power(s,3)*(24 + 55*t2 - 29*Power(t2,2)) + 
               Power(s2,3)*(4 + 8*t2 - 9*Power(t2,2)) + 
               s2*(-137 + 75*t2 + 60*Power(t2,2) - 120*Power(t2,3)) + 
               Power(s,2)*(-172 + (109 - 79*s2)*t2 + 
                  (355 + 49*s2)*Power(t2,2) - 18*Power(t2,3)) - 
               3*Power(s2,2)*
                (-6 + 45*t2 + 29*Power(t2,2) + 6*Power(t2,3)) + 
               s*(135 - 949*t2 + Power(s2,2)*(21 - 11*t2)*t2 - 
                  450*Power(t2,2) + 140*Power(t2,3) + 
                  4*s2*(12 + 38*t2 - 50*Power(t2,2) + 9*Power(t2,3)))) + 
            Power(t1,3)*(79 - 115*t2 - 1706*Power(t2,2) - 
               305*Power(t2,3) + 23*Power(t2,4) + 
               Power(s2,3)*t2*(50 - 95*t2 - 3*Power(t2,2)) + 
               Power(s,3)*t2*(90 - 119*t2 + 3*Power(t2,2)) - 
               Power(s2,2)*(24 + 227*t2 + 231*Power(t2,2) + 
                  122*Power(t2,3)) + 
               Power(s,2)*(60 + (147 + 62*s2)*t2 + 
                  (745 + 197*s2)*Power(t2,2) - (166 + 9*s2)*Power(t2,3)) \
+ s2*(62 + 155*t2 + 1215*Power(t2,2) - 62*Power(t2,3) + 22*Power(t2,4)) + 
               s*(-178 - 367*t2 + 307*Power(t2,2) + 412*Power(t2,3) - 
                  22*Power(t2,4) + 
                  Power(s2,2)*t2*(-134 + 17*t2 + 9*Power(t2,2)) + 
                  2*s2*(-6 + 63*t2 - 326*Power(t2,2) + 144*Power(t2,3)))))\
)*R2q(1 - s1 - t1 + t2))/
     ((-1 + s2)*(-s + s2 - t1)*
       Power(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2,2)*
       (-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)) + 
    (8*(Power(s1,12)*(Power(s2,3) + s2*Power(t1,2) + 2*Power(t1,3)) + 
         Power(-1 + s,2)*Power(t1,10)*(-1 + s + t2) - 
         4*t1*Power(t2,2)*(-15 + 
            3*(26 + 9*Power(s,2) - 20*s2 - 3*Power(s2,2) + 
               6*s*(-7 + 3*s2))*t2 - 
            (7 + 6*Power(s,3) - 94*s2 + 60*Power(s2,2) + 
               4*Power(s2,3) - 4*Power(s,2)*(11 + 18*s2) + 
               s*(56 + 176*s2 - 50*Power(s2,2)))*Power(t2,2) + 
            2*(2 + 12*Power(s,3) - 7*s2 + 21*Power(s2,2) + 
               Power(s2,3) + Power(s,2)*(3 + 41*s2) + 
               s*(-85 + 16*s2 - 70*Power(s2,2)))*Power(t2,3) + 
            (-189 - 30*Power(s,3) + 122*s2 + 52*Power(s2,2) + 
               4*Power(s,2)*(9 + 23*s2) - 
               2*s*(9 + 52*s2 + 39*Power(s2,2)))*Power(t2,4) - 
            (2 + 4*Power(s,3) + Power(s,2)*(1 - 10*s2) - 50*s2 + 
               9*Power(s2,2) - 2*Power(s2,3) + 
               s*(48 - 2*s2 + 8*Power(s2,2)))*Power(t2,5) + 
            (3 + 2*s)*Power(t2,6)) + 
         Power(t1,5)*(2 + (-158 - 60*Power(s,2) + s*(215 - 6*s2) + 
               5*s2)*t2 + (312 - 80*Power(s,3) - 262*s2 + 
               3*Power(s2,2) - 4*Power(s,2)*(-73 + 36*s2) + 
               s*(-375 + 401*s2 - 4*Power(s2,2)))*Power(t2,2) - 
            (351 + 109*Power(s,3) + Power(s,2)*(3 - 86*s2) - 352*s2 + 
               117*Power(s2,2) + 4*Power(s2,3) + 
               s*(-439 + 308*s2 - 147*Power(s2,2)))*Power(t2,3) + 
            (227 + 10*Power(s,3) - 213*s2 + 101*Power(s2,2) - 
               14*Power(s2,3) - 2*Power(s,2)*(3 + 29*s2) + 
               s*(-18 - 59*s2 + 74*Power(s2,2)))*Power(t2,4) - 
            (15 + Power(s,3) + 57*s2 - 33*Power(s2,2) - 
               2*Power(s2,3) - Power(s,2)*(29 + 4*s2) + 
               s*(-82 + 60*s2 + 5*Power(s2,2)))*Power(t2,5) + 
            (-1 + s - s2)*Power(t2,6)) + 
         2*Power(t1,3)*t2*(-10 + 
            5*(46 + 18*Power(s,2) - 5*s2 + s*(-68 + 6*s2))*t2 + 
            (-243 + 15*Power(s,3) + 346*s2 - 21*Power(s2,2) + 
               4*Power(s2,3) + Power(s,2)*(-27 + 230*s2) + 
               15*s*(5 - 40*s2 + Power(s2,2)))*Power(t2,2) + 
            (202 + 100*Power(s,3) - 345*s2 + 124*Power(s2,2) + 
               3*Power(s2,3) + Power(s,2)*(-26 + 27*s2) + 
               s*(-337 + 262*s2 - 242*Power(s2,2)))*Power(t2,3) + 
            (-298 - 21*Power(s,3) + 245*s2 - 86*Power(s2,2) + 
               6*Power(s2,3) + 32*Power(s,2)*(-2 + 3*s2) + 
               s*(37 + 102*s2 - 105*Power(s2,2)))*Power(t2,4) + 
            (24 - 6*Power(s,3) + 94*s2 - 28*Power(s2,2) + 
               3*Power(s2,3) + Power(s,2)*(-16 + 15*s2) + 
               s*(-119 + 36*s2 - 12*Power(s2,2)))*Power(t2,5) + 
            (-1 + 3*Power(s,2) + 5*s2 + 3*Power(s2,2) - 2*s*(2 + 3*s2))*
             Power(t2,6)) + Power(t1,9)*
          (6 + (-9 + 4*s2)*t2 - 4*(-1 + s2)*Power(t2,2) - 
            Power(s,3)*(3 + 2*t2) + 
            4*Power(s,2)*(3 + s2*t2 - Power(t2,2)) + 
            s*(-15 + (11 - 8*s2)*t2 + (2 + 4*s2)*Power(t2,2))) + 
         Power(t1,8)*(-14 + (29 - 17*s2)*t2 + 
            (-19 + 22*s2 - 5*Power(s2,2))*Power(t2,2) + 
            (-4 - 2*s2 + 3*Power(s2,2))*Power(t2,3) + 
            Power(s,3)*(2 - 7*t2 + Power(t2,2)) + 
            Power(s,2)*(-15 - 6*(-3 + 2*s2)*t2 + 
               (2 - 7*s2)*Power(t2,2) + 6*Power(t2,3)) + 
            s*(27 + (-36 + 29*s2)*t2 + 
               (-2 - 8*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (6 - 10*s2)*Power(t2,3))) + 
         Power(t1,7)*(16 + (-73 + 27*s2)*t2 + 
            (98 - 64*s2 + 13*Power(s2,2))*Power(t2,2) + 
            (-46 + 47*s2 - 12*Power(s2,2) + 2*Power(s2,3))*Power(t2,3) + 
            (1 + 6*s2 - 4*Power(s2,2))*Power(t2,4) + 
            Power(s,3)*t2*(29 + 25*t2 - Power(t2,2)) + 
            Power(s,2)*(6 + (-113 + 14*s2)*t2 + 
               (1 - 38*s2)*Power(t2,2) + 3*(9 + 2*s2)*Power(t2,3) - 
               4*Power(t2,4)) + 
            s*(-21 + (142 - 40*s2)*t2 + 
               (-120 + 85*s2 - 13*Power(s2,2))*Power(t2,2) - 
               (10 + 19*s2 + 8*Power(s2,2))*Power(t2,3) + 
               (-9 + 8*s2)*Power(t2,4))) + 
         Power(s1,11)*(Power(s2,3)*(-1 + 8*t1 - 4*t2) + 
            Power(s2,2)*(-3 + 2*Power(t1,2) + t1*(2 + s - t2) + 
               3*s*t2) - s2*t1*
             (-10*Power(t1,2) + s*(3 + t1 - 3*t2) + 2*t2 + 
               3*t1*(2 + t2)) + 
            Power(t1,2)*(-1 + 16*Power(t1,2) - 6*t2 + s*(-3 + 4*t2) - 
               t1*(11 + 4*t2))) + 
         Power(t1,6)*(-9 + (140 - 19*s2)*t2 + 
            (-225 + 168*s2 - 11*Power(s2,2))*Power(t2,2) + 
            (107 - 207*s2 + 54*Power(s2,2) - 2*Power(s2,3))*
             Power(t2,3) + (52 + s2 - 12*Power(s2,2) - 3*Power(s2,3))*
             Power(t2,4) + (-1 + s2 + Power(s2,2))*Power(t2,5) + 
            Power(s,3)*t2*(-20 - 2*t2 - 17*Power(t2,2) + 
               2*Power(t2,3)) + 
            s*(6 + 5*(-54 + 5*s2)*t2 + 
               (185 - 292*s2 + 12*Power(s2,2))*Power(t2,2) + 
               (133 + 100*s2 - 51*Power(s2,2))*Power(t2,3) + 
               (-48 + 75*s2 + 8*Power(s2,2))*Power(t2,4) - 
               2*(-1 + s2)*Power(t2,5)) + 
            Power(s,2)*t2*(154 + 23*t2 - 93*Power(t2,2) - 
               47*Power(t2,3) + Power(t2,4) + 
               s2*(-6 + 130*t2 + 84*Power(t2,2) - 7*Power(t2,3)))) - 
         2*Power(t1,4)*t2*(-45 + (210 - 95*s2)*t2 + 
            (-336 + 274*s2 - 68*Power(s2,2))*Power(t2,2) + 
            (134 - 335*s2 + 127*Power(s2,2) - 18*Power(s2,3))*
             Power(t2,3) + (109 - 20*s2 + 12*Power(s2,2) - 
               4*Power(s2,3))*Power(t2,4) + 
            (-8 + 9*Power(s2,2))*Power(t2,5) + 
            Power(s,3)*t2*(-30 - 45*t2 - 46*Power(t2,2) + 
               3*Power(t2,3)) + 
            s*(30 + 5*(-81 + 25*s2)*t2 + 
               (269 - 419*s2 + 61*Power(s2,2))*Power(t2,2) + 
               (226 + 177*s2 - 60*Power(s2,2))*Power(t2,3) + 
               (-73 + 89*s2 + 13*Power(s2,2))*Power(t2,4) + 
               (17 - 16*s2)*Power(t2,5)) + 
            Power(s,2)*t2*(245 + 103*t2 - 162*Power(t2,2) - 
               53*Power(t2,3) + 7*Power(t2,4) - 
               2*s2*(15 - 116*t2 - 80*Power(t2,2) + 6*Power(t2,3)))) - 
         8*Power(t2,3)*(2*Power(s2,2)*t2*
             (2 + 3*t2 + 9*Power(t2,2) - 15*Power(t2,3) + Power(t2,4)) + 
            Power(s,3)*t2*(1 - 7*t2 - 11*Power(t2,2) + 7*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,3)*(t2 - 3*Power(t2,2) - 7*Power(t2,3) + 
               3*Power(t2,4) - 2*Power(t2,5)) + 
            s2*(9 + t2 - 8*Power(t2,2) - 100*Power(t2,3) - 
               Power(t2,4) + 3*Power(t2,5)) - 
            2*(9 + 18*t2 + 24*Power(t2,2) - 38*Power(t2,3) - 
               17*Power(t2,4) + 4*Power(t2,5)) + 
            s*(9 + (1 + 48*s2 - 9*Power(s2,2))*t2 + 
               (88 + 39*Power(s2,2))*Power(t2,2) + 
               (-12 + 136*s2 + 27*Power(s2,2))*Power(t2,3) + 
               (-1 + 88*s2 + 9*Power(s2,2))*Power(t2,4) + 
               (11 + 6*Power(s2,2))*Power(t2,5)) - 
            Power(s,2)*t2*(2*
                (-2 + 19*t2 + 9*Power(t2,2) + 13*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(9 - 35*t2 - 7*Power(t2,2) + 19*Power(t2,3) + 
                  6*Power(t2,4)))) + 
         2*Power(t1,2)*Power(t2,2)*
          (-135 - (-138 + 251*s2 + 29*Power(s2,2) + 2*Power(s2,3))*t2 - 
            (371 - 327*s2 + 154*Power(s2,2) + 15*Power(s2,3))*
             Power(t2,2) + (224 - 464*s2 + 284*Power(s2,2) - 
               33*Power(s2,3))*Power(t2,3) - 
            (-167 + 52*s2 - 18*Power(s2,2) + Power(s2,3))*Power(t2,4) - 
            (26 + 5*s2 - 9*Power(s2,2) + 5*Power(s2,3))*Power(t2,5) - 
            3*(-1 + s2)*Power(t2,6) + 
            Power(s,3)*t2*(-20 - 63*t2 - 121*Power(t2,2) + 
               15*Power(t2,3) + 5*Power(t2,4)) + 
            s*(90 + 23*(-13 + 18*s2)*t2 + 
               (413 - 316*s2 + 223*Power(s2,2))*Power(t2,2) + 
               (124 + 240*s2 - 55*Power(s2,2))*Power(t2,3) + 
               (-74 + 164*s2 + 33*Power(s2,2))*Power(t2,4) + 
               3*(21 - 2*s2 + 5*Power(s2,2))*Power(t2,5) + 3*Power(t2,6)\
) - Power(s,2)*t2*(-199 - 22*t2 + 84*Power(t2,2) + 54*Power(t2,3) + 
               3*Power(t2,4) + 
               s2*(90 - 271*t2 - 289*Power(t2,2) + 47*Power(t2,3) + 
                  15*Power(t2,4)))) + 
         Power(s1,10)*(Power(s2,3)*
             (-3 + 28*Power(t1,2) - 8*t2 + 6*Power(t2,2) - 
               t1*(4 + 31*t2)) + 
            Power(s2,2)*(3 + 16*Power(t1,3) + 
               Power(t1,2)*(8 + 8*s - 13*t2) + (10 - 7*s)*t2 + 
               (1 - 9*s)*Power(t2,2) + 
               t1*(-28 - 11*t2 + 3*Power(t2,2) + s*(-5 + 24*t2))) + 
            s2*(3 + 44*Power(t1,4) + Power(t2,2) + 
               3*Power(s,2)*Power(t2,2) - 3*s*t2*(1 + t2) - 
               Power(t1,3)*(48 + 6*s + 23*t2) + 
               Power(t1,2)*(2 - 16*t2 + 3*Power(t2,2) + 
                  s*(-19 + 32*t2)) + 
               t1*(-4 + 14*t2 - Power(s,2)*t2 + 6*Power(t2,2) + 
                  s*(4 + 6*t2 - 8*Power(t2,2)))) + 
            t1*(56*Power(t1,4) + 2*t2*(1 + 3*t2) - 
               Power(t1,3)*(82 + 27*t2) + 
               Power(s,2)*(t1 - 2*Power(t1,2) - 5*t1*t2 + 
                  3*(-1 + t2)*t2) + 
               Power(t1,2)*(1 - 47*t2 + 2*Power(t2,2)) + 
               6*t1*(1 + 6*t2 + 2*Power(t2,2)) + 
               s*(3 + 3*t2 - 8*Power(t2,2) + 4*Power(t1,2)*(-5 + 6*t2) + 
                  t1*(6 - 7*t2 - 7*Power(t2,2))))) + 
         Power(s1,9)*(-1 + 112*Power(t1,6) - Power(t2,2) - 
            2*Power(t2,3) + 4*s*Power(t2,3) - 3*Power(s,2)*Power(t2,3) + 
            Power(s,3)*Power(t2,3) - 6*Power(t1,5)*(44 + 13*t2) + 
            Power(t1,4)*(45 - 16*Power(s,2) - 159*t2 + 11*Power(t2,2) + 
               s*(-52 + 57*t2)) + 
            Power(t1,3)*(53 + Power(s,2)*(11 - 24*t2) + 363*t2 + 
               102*Power(t2,2) + s*(49 - 59*t2 - 27*Power(t2,2))) + 
            Power(s2,3)*(1 + 56*Power(t1,3) + 22*t2 + 41*Power(t2,2) - 
               4*Power(t2,3) - Power(t1,2)*(1 + 105*t2) + 
               t1*(-18 - 58*t2 + 45*Power(t2,2))) - 
            t1*(-2 + 13*t2 + 39*Power(t2,2) + 
               2*Power(s,3)*Power(t2,2) + 12*Power(t2,3) + 
               2*Power(s,2)*t2*(-4 - 3*t2 + 2*Power(t2,2)) + 
               s*(5 + 16*t2 - 22*Power(t2,2) - 14*Power(t2,3))) + 
            Power(t1,2)*(-4 - 17*t2 + Power(s,3)*t2 + 42*Power(t2,2) - 
               6*Power(t2,3) + 
               2*Power(s,2)*(3 - 7*t2 + 16*Power(t2,2)) + 
               s*(31 + 35*t2 - 71*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s2,2)*(9 + 56*Power(t1,4) + 
               Power(t1,3)*(4 + 28*s - 62*t2) - (-28 + s)*t2 - 
               9*(1 + 2*s)*Power(t2,2) + (-3 + 9*s)*Power(t2,3) - 
               t1*(-5 + s - 76*t2 + 44*s*t2 - 40*Power(t2,2) + 
                  72*s*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(-109 - 81*t2 + 25*Power(t2,2) + 
                  17*s*(-2 + 5*t2))) + 
            s2*(-3 + 112*Power(t1,5) + 8*(-1 + s)*t2 + 
               (-8 + 13*s - 8*Power(s,2))*Power(t2,2) + 
               (-3 + 8*s - 6*Power(s,2))*Power(t2,3) - 
               Power(t1,4)*(175 + 12*s + 75*t2) + 
               Power(t1,3)*(-4 - 72*t2 + 11*Power(t2,2) + 
                  8*s*(-7 + 16*t2)) - 
               Power(t1,2)*(18 - 177*t2 - 62*Power(t2,2) + 
                  Power(t2,3) + Power(s,2)*(2 + 9*t2) + 
                  2*s*(-8 - 14*t2 + 35*Power(t2,2))) + 
               t1*(32 + 10*t2 - 4*Power(t2,2) - 6*Power(t2,3) + 
                  Power(s,2)*t2*(-6 + 29*t2) + 
                  s*(16 - 28*t2 - 75*Power(t2,2) + 7*Power(t2,3))))) + 
         Power(s1,8)*(1 + 140*Power(t1,7) + 2*t2 - s*t2 + 
            7*Power(t2,2) + s*Power(t2,2) + 14*Power(t2,3) - 
            15*s*Power(t2,3) + 5*Power(s,2)*Power(t2,3) - 
            2*Power(s,3)*Power(t2,3) + 4*Power(t2,4) - 7*s*Power(t2,4) + 
            4*Power(s,2)*Power(t2,4) - Power(s,3)*Power(t2,4) - 
            Power(t1,6)*(478 + 125*t2) + 
            Power(t1,5)*(196 - 56*Power(s,2) - 303*t2 + 
               25*Power(t2,2) + s*(-60 + 62*t2)) + 
            Power(t1,4)*(200 + Power(s,2)*(48 - 33*t2) + 1337*t2 + 
               340*Power(t2,2) + s*(155 - 197*t2 - 24*Power(t2,2))) + 
            Power(t1,3)*(11 - 341*t2 + 15*Power(t2,2) - 
               40*Power(t2,3) + Power(s,3)*(2 + 4*t2) + 
               s*(95 + 128*t2 - 180*Power(t2,2)) + 
               2*Power(s,2)*(16 + 8*t2 + 51*Power(t2,2))) + 
            Power(s2,3)*(2 + 70*Power(t1,4) + 
               Power(t1,3)*(22 - 203*t2) + 28*t2 - 7*Power(t2,2) - 
               65*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(-45 - 181*t2 + 146*Power(t2,2)) + 
               t1*(1 + 84*t2 + 255*Power(t2,2) - 29*Power(t2,3))) - 
            Power(t1,2)*(-10 + 179*t2 + 590*Power(t2,2) + 
               143*Power(t2,3) + Power(s,3)*t2*(-4 + 17*t2) + 
               s*(36 + 183*t2 - 260*Power(t2,2) - 103*Power(t2,3)) + 
               Power(s,2)*(12 - 43*t2 - 50*Power(t2,2) + 30*Power(t2,3))\
) + t1*(-12 + t2 + 34*Power(t2,2) - 7*Power(t2,3) + 
               10*Power(s,3)*Power(t2,3) + 6*Power(t2,4) + 
               Power(s,2)*t2*(-6 - 68*Power(t2,2) + Power(t2,3)) + 
               s*(-11 - 20*t2 + Power(t2,2) + 63*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s2,2)*(-3 + 112*Power(t1,5) + 
               Power(t1,4)*(-32 + 56*s - 153*t2) + (-59 + 13*s)*t2 + 
               6*(-19 + 14*s)*Power(t2,2) + 15*(-1 + 6*s)*Power(t2,3) - 
               3*(-1 + s)*Power(t2,4) + 
               2*Power(t1,3)*
                (-118 - 129*t2 + 38*Power(t2,2) + s*(-49 + 86*t2)) - 
               Power(t1,2)*(28 - 334*t2 - 260*Power(t2,2) + 
                  19*Power(t2,3) + 2*s*(2 + 57*t2 + 126*Power(t2,2))) + 
               t1*(58 + 270*t2 + 12*Power(t2,2) - 70*Power(t2,3) + 
                  Power(t2,4) + 
                  s*(5 + 46*t2 - 130*Power(t2,2) + 68*Power(t2,3)))) + 
            s2*(-9 + 182*Power(t1,6) - 4*(8 + s)*t2 + 
               (6 + 35*s + 6*Power(s,2))*Power(t2,2) + 
               (10 + 25*s - 23*Power(s,2))*Power(t2,3) + 
               (3 - 7*s + 3*Power(s,2))*Power(t2,4) - 
               5*Power(t1,5)*(76 + 27*t2) + 
               Power(t1,4)*(-61 - 216*t2 + 5*Power(t2,2) + 
                  4*s*(-27 + 64*t2)) + 
               Power(t1,3)*(-21 + 835*t2 + 233*Power(t2,2) + 
                  6*Power(t2,3) - Power(s,2)*(18 + 29*t2) + 
                  s*(21 + t2 - 214*Power(t2,2))) + 
               Power(t1,2)*(122 + 51*t2 - 151*Power(t2,2) - 
                  37*Power(t2,3) + 
                  Power(s,2)*(2 - 45*t2 + 123*Power(t2,2)) + 
                  s*(101 - 142*t2 - 428*Power(t2,2) + 49*Power(t2,3))) - 
               t1*(-2 + 55*t2 + 238*Power(t2,2) + 49*Power(t2,3) - 
                  2*Power(t2,4) + 
                  Power(s,2)*t2*(-18 + 32*t2 + 49*Power(t2,2)) + 
                  2*s*(5 + 6*t2 - 65*Power(t2,2) - 86*Power(t2,3) + 
                     Power(t2,4))))) + 
         Power(s1,7)*(3 + 112*Power(t1,8) + 12*t2 + 5*s*t2 - 
            3*Power(t2,2) - 5*s*Power(t2,2) - 18*Power(t2,3) - 
            7*s*Power(t2,3) + 10*Power(s,2)*Power(t2,3) + 
            2*Power(s,3)*Power(t2,3) - 4*Power(t2,4) - 
            16*s*Power(t2,4) + 33*Power(s,2)*Power(t2,4) - 
            10*Power(s,3)*Power(t2,4) - 2*Power(t2,5) + 
            3*s*Power(t2,5) - Power(s,2)*Power(t2,5) - 
            10*Power(t1,7)*(53 + 12*t2) + 
            Power(t1,6)*(414 - 112*Power(s,2) - 355*t2 + 
               30*Power(t2,2) + 5*s*(-2 + 3*t2)) + 
            Power(t1,5)*(396 + 2503*t2 + 588*Power(t2,2) + 
               2*Power(s,2)*(56 + 13*t2) + 
               s*(244 - 341*t2 + 40*Power(t2,2))) + 
            Power(t1,4)*(60 - 1475*t2 - 224*Power(t2,2) - 
               114*Power(t2,3) + 3*Power(s,3)*(4 + t2) + 
               Power(s,2)*(70 + 214*t2 + 129*Power(t2,2)) + 
               s*(77 + 209*t2 - 146*Power(t2,2) - 33*Power(t2,3))) + 
            t1*(-3 + 10*t2 + 213*Power(t2,2) + 416*Power(t2,3) + 
               88*Power(t2,4) + 
               Power(s,3)*Power(t2,2)*(2 + 12*t2 - 7*Power(t2,2)) + 
               Power(s,2)*t2*
                (-8 - 90*t2 + 47*Power(t2,2) + 76*Power(t2,3)) + 
               s*(11 + 80*t2 + 123*Power(t2,2) - 375*Power(t2,3) - 
                  123*Power(t2,4))) - 
            Power(t1,3)*(25 + 926*t2 + 2547*Power(t2,2) + 
               561*Power(t2,3) + 4*Power(t2,4) + 
               7*Power(s,3)*t2*(-2 + 7*t2) + 
               Power(s,2)*(58 - 5*t2 - 34*Power(t2,2) + 
                  64*Power(t2,3)) + 
               s*(141 + 619*t2 - 952*Power(t2,2) - 125*Power(t2,3) - 
                  4*Power(t2,4))) + 
            Power(s2,3)*(56*Power(t1,5) + Power(t1,4)*(55 - 245*t2) + 
               Power(t1,3)*(-60 - 312*t2 + 263*Power(t2,2)) + 
               Power(t1,2)*(-10 + 95*t2 + 673*Power(t2,2) - 
                  89*Power(t2,3)) + 
               t2*(-15 - 148*t2 - 122*Power(t2,2) + 44*Power(t2,3)) + 
               t1*(10 + 123*t2 + 56*Power(t2,2) - 375*Power(t2,3) + 
                  7*Power(t2,4))) + 
            Power(t1,2)*(-41 - 39*t2 + 831*Power(t2,2) + 
               353*Power(t2,3) + 54*Power(t2,4) + 
               Power(s,3)*t2*(-16 - 4*t2 + 41*Power(t2,2)) + 
               Power(s,2)*(-2 - 78*t2 - 129*Power(t2,2) - 
                  339*Power(t2,3) + 5*Power(t2,4)) - 
               s*(80 + 115*t2 + 11*Power(t2,2) - 117*Power(t2,3) + 
                  13*Power(t2,4))) + 
            Power(s2,2)*(-6 + 140*Power(t1,6) + 
               Power(t1,5)*(-80 + 70*s - 218*t2) - 2*(44 + s)*t2 - 
               2*(25 + 12*s)*Power(t2,2) + (113 - 48*s)*Power(t2,3) + 
               (42 - 98*s)*Power(t2,4) - Power(t2,5) + 
               t1*(2 + (-163 + 57*s)*t2 + 
                  4*(-219 + 79*s)*Power(t2,2) + 
                  (-310 + 573*s)*Power(t2,3) + (54 - 21*s)*Power(t2,4)) \
+ Power(t1,4)*(-318 - 469*t2 + 106*Power(t2,2) + s*(-154 + 213*t2)) - 
               Power(t1,3)*(119 - 855*t2 - 721*Power(t2,2) + 
                  32*Power(t2,3) + s*(6 + 170*t2 + 485*Power(t2,2))) + 
               Power(t1,2)*(142 + 934*t2 + 47*Power(t2,2) - 
                  319*Power(t2,3) + 5*Power(t2,4) + 
                  s*(25 + 232*t2 - 401*Power(t2,2) + 219*Power(t2,3)))) \
+ s2*(3 + 196*Power(t1,7) + Power(t1,6)*(-537 + 42*s - 145*t2) + 
               (52 - 14*s)*t2 + 
               (103 - 100*s + 2*Power(s,2))*Power(t2,2) + 
               (97 - 188*s + 83*Power(s,2))*Power(t2,3) + 
               2*(5 - 48*s + 32*Power(s,2))*Power(t2,4) + 
               (-1 + 2*s)*Power(t2,5) - 
               Power(t1,5)*(170 + s*(158 - 262*t2) + 426*t2 + 
                  35*Power(t2,2)) + 
               Power(t1,4)*(52 + 2085*t2 + 457*Power(t2,2) + 
                  37*Power(t2,3) - Power(s,2)*(67 + 41*t2) - 
                  2*s*(-8 + 116*t2 + 145*Power(t2,2))) + 
               Power(t1,3)*(299 + 170*t2 - 514*Power(t2,2) - 
                  4*Power(t2,4) + 
                  Power(s,2)*(18 - 120*t2 + 271*Power(t2,2)) + 
                  s*(267 - 276*t2 - 977*Power(t2,2) + 96*Power(t2,3))) + 
               t1*(-62 - 342*t2 - 21*Power(t2,2) + 382*Power(t2,3) + 
                  39*Power(t2,4) + 
                  Power(s,2)*t2*
                   (-4 + 80*t2 - 210*Power(t2,2) + 21*Power(t2,3)) + 
                  s*(-13 - 133*t2 + 511*Power(t2,2) + 335*Power(t2,3) - 
                     130*Power(t2,4))) - 
               Power(t1,2)*(Power(s,2)*t2*
                   (-155 + 25*t2 + 171*Power(t2,2)) + 
                  3*(-23 + 95*t2 + 518*Power(t2,2) + 50*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  s*(35 + 108*t2 - 801*Power(t2,2) - 794*Power(t2,3) + 
                     10*Power(t2,4))))) + 
         s1*(-(Power(-1 + s,2)*Power(t1,11)) + 
            4*Power(t2,2)*(-15 - 
               3*(16 + 3*Power(s,2) + 30*s2 - 9*Power(s2,2) - 
                  2*s*(2 + 9*s2))*t2 + 
               (-21 + 4*Power(s,3) - 136*s2 - 70*Power(s2,2) + 
                  14*Power(s2,3) - 2*Power(s,2)*(7 + 5*s2) + 
                  s*(26 + 84*s2 + 72*Power(s2,2)))*Power(t2,2) - 
               2*(-102 + 11*Power(s,3) + Power(s,2)*(34 - 80*s2) + 
                  117*s2 + 32*Power(s2,2) + 22*Power(s2,3) - 
                  s*(1 + 74*s2 + 65*Power(s2,2)))*Power(t2,3) + 
               (243 - 16*Power(s,3) + 70*s2 - 214*Power(s2,2) - 
                  26*Power(s2,3) + Power(s,2)*(90 + 22*s2) + 
                  s*(-302 + 364*s2 + 36*Power(s2,2)))*Power(t2,4) + 
               (-44 + 2*Power(s,3) + Power(s,2)*(61 - 28*s2) + 
                  92*s2 - 35*Power(s2,2) - 24*Power(s2,3) + 
                  s*(-6 - 34*s2 + 50*Power(s2,2)))*Power(t2,5) + 
               (1 + 4*Power(s,2) + s*(12 - 8*s2) - 22*s2 + 
                  4*Power(s2,2))*Power(t2,6)) + 
            2*Power(t1,2)*t2*
             (-30 + 3*(61 + 24*Power(s,2) + 40*s2 - 18*Power(s2,2) + 
                  s*(-137 + 18*s2))*t2 + 
               (-46 + 34*Power(s,3) - 207*s2 + 170*Power(s2,2) - 
                  5*Power(s2,3) + Power(s,2)*(-180 + 287*s2) - 
                  2*s*(-75 + 163*s2 + 50*Power(s2,2)))*Power(t2,2) + 
               (-62 + 239*Power(s,3) + Power(s,2)*(31 - 412*s2) + 
                  289*s2 - 387*Power(s2,2) + 178*Power(s2,3) + 
                  s*(-1391 + 1156*s2 - 661*Power(s2,2)))*Power(t2,3) + 
               (-2286 - 132*Power(s,3) + 1387*s2 + 375*Power(s2,2) - 
                  69*Power(s2,3) + Power(s,2)*(13 + 251*s2) + 
                  s*(499 - 1036*s2 - 122*Power(s2,2)))*Power(t2,4) + 
               (-33 - 29*Power(s,3) + 307*s2 + 17*Power(s2,2) + 
                  32*Power(s2,3) + Power(s,2)*(-103 + 90*s2) + 
                  s*(-410 + 70*s2 - 93*Power(s2,2)))*Power(t2,5) + 
               (2 + 7*Power(s,2) + 56*s2 + 7*Power(s2,2) - 
                  s*(37 + 14*s2))*Power(t2,6)) + 
            Power(t1,4)*(10 - 
               5*(80 + 36*Power(s,2) + 39*s2 - 3*s*(49 + 2*s2))*t2 + 
               (738 - 180*Power(s,3) + Power(s,2)*(722 - 540*s2) + 
                  34*s2 - 269*Power(s2,2) - 24*Power(s2,3) + 
                  5*s*(-121 + 159*s2 + 36*Power(s2,2)))*Power(t2,2) + 
               (-1237 - 465*Power(s,3) + 716*s2 + 369*Power(s2,2) - 
                  156*Power(s2,3) + Power(s,2)*(-421 + 862*s2) + 
                  s*(3095 - 2104*s2 + 615*Power(s2,2)))*Power(t2,3) + 
               (3095 + 194*Power(s,3) - 2325*s2 + 467*Power(s2,2) + 
                  54*Power(s2,3) - 38*Power(s,2)*(-3 + 11*s2) + 
                  s*(-698 + 143*s2 + 230*Power(s2,2)))*Power(t2,4) + 
               (-67 + 11*Power(s,3) + Power(s,2)*(247 - 20*s2) - 
                  537*s2 + 163*Power(s2,2) + 2*Power(s2,3) + 
                  s*(682 - 396*s2 + 7*Power(s2,2)))*Power(t2,5) + 
               (21 - 18*Power(s,2) - 61*s2 - 18*Power(s2,2) + 
                  s*(55 + 36*s2))*Power(t2,6)) + 
            Power(t1,9)*(-19 + Power(s,3)*(9 - 2*t2) + 34*t2 + 
               Power(s2,2)*(2 - 5*t2)*t2 + 4*Power(t2,2) + 
               Power(s,2)*(-18 + (7 + 4*s2)*t2 - 12*Power(t2,2)) + 
               s2*(-1 - 24*t2 + 6*Power(t2,2)) + 
               s*(24 + s2 - 8*t2 + 11*s2*t2 - 2*Power(s2,2)*t2 - 
                  10*Power(t2,2) + 18*s2*Power(t2,2))) - 
            Power(t1,10)*(-8 + s2 + Power(s,2)*(-3 + s2 - 6*t2) + 
               8*t2 - 6*s2*t2 + s*(11 + 2*t2 + s2*(-2 + 6*t2))) + 
            Power(t1,8)*(33 - 149*t2 + 141*Power(t2,2) - 
               Power(s2,3)*Power(t2,2) - Power(t2,3) + 
               8*Power(s2,2)*t2*Power(1 + t2,2) + 
               Power(s,3)*(-19 - 16*t2 + 7*Power(t2,2)) + 
               s2*(15 + 32*t2 - 114*Power(t2,2) - 10*Power(t2,3)) + 
               Power(s,2)*(74 + s2 - 52*t2 + 50*s2*t2 - 
                  76*Power(t2,2) - 16*s2*Power(t2,2) + 10*Power(t2,3)) \
+ s*(-76 + 233*t2 + 10*Power(t2,2) + 13*Power(t2,3) + 
                  Power(s2,2)*t2*(-3 + 10*t2) - 
                  s2*(14 + 82*t2 - 73*Power(t2,2) + 18*Power(t2,3)))) - 
            Power(t1,7)*(60 - 293*t2 + 268*Power(t2,2) + 
               Power(s2,3)*(7 - 6*t2)*Power(t2,2) + 89*Power(t2,3) - 
               4*Power(t2,4) + 
               Power(s2,2)*t2*
                (32 + 35*t2 - 33*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s,3)*(-10 + 53*t2 - 25*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(s,2)*(88 + (-89 + 99*s2)*t2 + 
                  (-139 + 83*s2)*Power(t2,2) - 
                  (136 + 27*s2)*Power(t2,3) + 3*Power(t2,4)) + 
               s2*(29 + 33*t2 - 338*Power(t2,2) + 39*Power(t2,3) + 
                  7*Power(t2,4)) + 
               s*(-144 + 319*t2 + 179*Power(t2,2) - 110*Power(t2,3) - 
                  2*Power(t2,4) + 
                  Power(s2,2)*t2*(-16 - 63*t2 + 24*Power(t2,2)) + 
                  s2*(-17 - 163*t2 + 267*Power(t2,2) + 
                     193*Power(t2,3) - 6*Power(t2,4)))) + 
            Power(t1,5)*(-44 + 325*t2 - 1411*Power(t2,2) + 
               836*Power(t2,3) + 527*Power(t2,4) - 57*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*
                (-3 + 11*t2 - 11*Power(t2,2) + Power(t2,3)) - 
               Power(s,3)*t2*
                (60 - 30*t2 + 101*Power(t2,2) - 58*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*t2*
                (-10 + 181*t2 + 320*Power(t2,2) + 9*Power(t2,3) + 
                  64*Power(t2,4)) + 
               s2*(-6 + 193*t2 + 225*Power(t2,2) - 1561*Power(t2,3) + 
                  65*Power(t2,4) + 36*Power(t2,5)) + 
               s*(30 + (-901 - 52*s2 + 6*Power(s2,2))*t2 + 
                  (1655 - 990*s2 - 102*Power(s2,2))*Power(t2,2) + 
                  (1004 + 1581*s2 - 305*Power(s2,2))*Power(t2,3) + 
                  (-451 + 638*s2 + 118*Power(s2,2))*Power(t2,4) - 
                  3*(-13 + 39*s2 + Power(s2,2))*Power(t2,5)) + 
               Power(s,2)*t2*
                (600 - 219*t2 - 1023*Power(t2,2) - 467*Power(t2,3) + 
                  53*Power(t2,4) + 
                  s2*(-30 + 819*t2 + 407*Power(t2,2) - 
                     165*Power(t2,3) + 3*Power(t2,4)))) - 
            2*Power(t1,3)*t2*
             (-130 + 79*t2 - 1255*Power(t2,2) + 711*Power(t2,3) + 
               537*Power(t2,4) - 82*Power(t2,5) + 12*Power(t2,6) + 
               Power(s2,3)*t2*
                (-6 - 63*t2 + 22*Power(t2,2) + 57*Power(t2,3) - 
                  10*Power(t2,4)) + 
               Power(s,3)*t2*
                (-30 - 69*t2 - 138*Power(t2,2) + 83*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-137 + 248*t2 + 711*Power(t2,2) + 22*Power(t2,3) + 
                  76*Power(t2,4)) + 
               s2*(-30 + 242*t2 + 306*Power(t2,2) - 1551*Power(t2,3) - 
                  13*Power(t2,4) + 17*Power(t2,5) - 11*Power(t2,6)) + 
               s*(90 + (-632 + 202*s2 + 30*Power(s2,2))*t2 + 
                  (1736 - 802*s2 + 33*Power(s2,2))*Power(t2,2) + 
                  (473 + 1588*s2 - 390*Power(s2,2))*Power(t2,3) + 
                  (-337 + 590*s2 + 57*Power(s2,2))*Power(t2,4) + 
                  (147 - 106*s2 + 30*Power(s2,2))*Power(t2,5) + 
                  11*Power(t2,6)) - 
               Power(s,2)*t2*
                (-447 + 454*t2 + 843*Power(t2,2) + 340*Power(t2,3) - 
                  30*Power(t2,4) + 
                  s2*(90 - 963*t2 - 506*Power(t2,2) + 197*Power(t2,3) + 
                     30*Power(t2,4)))) + 
            4*t1*Power(t2,2)*
             (-33 - 186*t2 - 481*Power(t2,2) + 458*Power(t2,3) + 
               279*Power(t2,4) - 40*Power(t2,5) + 3*Power(t2,6) + 
               Power(s,3)*t2*
                (1 - 53*t2 - 27*Power(t2,2) + 57*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-101 + 302*t2 + 354*Power(t2,2) - 218*Power(t2,3) + 
                  31*Power(t2,4)) + 
               Power(s2,3)*(t2 - 69*Power(t2,2) + 25*Power(t2,3) + 
                  81*Power(t2,4) - 14*Power(t2,5)) + 
               s2*(9 + 337*t2 + 130*Power(t2,2) - 810*Power(t2,3) - 
                  23*Power(t2,4) + 9*Power(t2,5) - 4*Power(t2,6)) + 
               s*(9 + (-155 + 30*s2 - 9*Power(s2,2))*t2 + 
                  (598 - 360*s2 + 61*Power(s2,2))*Power(t2,2) + 
                  (-90 + 908*s2 - 149*Power(s2,2))*Power(t2,3) + 
                  (-11 + 608*s2 - 33*Power(s2,2))*Power(t2,4) + 
                  3*(31 - 6*s2 + 14*Power(s2,2))*Power(t2,5) + 
                  4*Power(t2,6)) - 
               Power(s,2)*t2*
                (-79 + 278*t2 + 278*Power(t2,2) + 246*Power(t2,3) + 
                  13*Power(t2,4) + 
                  s2*(9 - 413*t2 - 135*Power(t2,2) + 105*Power(t2,3) + 
                     42*Power(t2,4)))) + 
            Power(t1,6)*(73 - 295*t2 + 780*Power(t2,2) - 
               951*Power(t2,3) + 19*Power(t2,4) + 2*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(23 + 7*t2 - 7*Power(t2,2)) + 
               Power(s,3)*t2*
                (125 + 147*t2 - 67*Power(t2,2) + 5*Power(t2,3)) - 
               2*Power(s2,2)*t2*
                (-16 + 27*t2 + 97*Power(t2,2) + 42*Power(t2,3)) + 
               Power(s,2)*(30 + (-478 + 73*s2)*t2 + 
                  (288 - 345*s2)*Power(t2,2) + 
                  (197 + 158*s2)*Power(t2,3) - (105 + 17*s2)*Power(t2,4)\
) + s2*(22 - 39*t2 - 281*Power(t2,2) + 756*Power(t2,3) + 
                  133*Power(t2,4) + 5*Power(t2,5)) + 
               s*(-113 + 442*t2 - 1391*Power(t2,2) + 128*Power(t2,3) - 
                  173*Power(t2,4) - 5*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-17 - 79*t2 - 105*Power(t2,2) + 19*Power(t2,3)) + 
                  2*s2*(-3 - 32*t2 + 314*Power(t2,2) - 85*Power(t2,3) + 
                     94*Power(t2,4))))) + 
         Power(s1,5)*(-2 + 16*Power(t1,10) + 
            2*Power(t1,9)*(-76 + 32*s2 - 11*t2) - 32*t2 - 5*s*t2 - 
            35*s2*t2 + 6*s*s2*t2 + 60*Power(s2,2)*t2 - 52*Power(t2,2) - 
            45*s*Power(t2,2) - 2*Power(s,2)*Power(t2,2) - 
            318*s2*Power(t2,2) + 139*s*s2*Power(t2,2) + 
            4*Power(s,2)*s2*Power(t2,2) + 247*Power(s2,2)*Power(t2,2) + 
            24*s*Power(s2,2)*Power(t2,2) + 80*Power(s2,3)*Power(t2,2) + 
            15*Power(t2,3) + 81*s*Power(t2,3) - 
            11*Power(s,2)*Power(t2,3) - 5*Power(s,3)*Power(t2,3) - 
            500*s2*Power(t2,3) + 456*s*s2*Power(t2,3) + 
            10*Power(s,2)*s2*Power(t2,3) - 201*Power(s2,2)*Power(t2,3) + 
            299*s*Power(s2,2)*Power(t2,3) + 
            360*Power(s2,3)*Power(t2,3) + 303*Power(t2,4) - 
            10*s*Power(t2,4) - 90*Power(s,2)*Power(t2,4) - 
            18*Power(s,3)*Power(t2,4) - 403*s2*Power(t2,4) + 
            999*s*s2*Power(t2,4) - 274*Power(s,2)*s2*Power(t2,4) - 
            521*Power(s2,2)*Power(t2,4) + 
            482*s*Power(s2,2)*Power(t2,4) + 30*Power(s2,3)*Power(t2,4) + 
            129*Power(t2,5) - 104*s*Power(t2,5) - 
            83*Power(s,2)*Power(t2,5) + 31*Power(s,3)*Power(t2,5) + 
            123*s2*Power(t2,5) + 348*s*s2*Power(t2,5) - 
            224*Power(s,2)*s2*Power(t2,5) - 
            195*Power(s2,2)*Power(t2,5) + 
            355*s*Power(s2,2)*Power(t2,5) - 
            162*Power(s2,3)*Power(t2,5) + 7*Power(t2,6) - 
            13*s*Power(t2,6) + 10*Power(s,2)*Power(t2,6) - 
            3*s2*Power(t2,6) - 20*s*s2*Power(t2,6) + 
            10*Power(s2,2)*Power(t2,6) + 
            Power(t1,8)*(381 - 112*Power(s,2) + 56*Power(s2,2) + 
               s*(60 + 84*s2 - 41*t2) - 117*t2 + 7*Power(t2,2) - 
               s2*(313 + 33*t2)) + 
            Power(t1,7)*(216 + 8*Power(s2,3) + 1601*t2 + 
               310*Power(t2,2) - 26*Power(s2,2)*(2 + 3*t2) + 
               18*Power(s,2)*(7 + 10*t2) - 
               s2*(128 + 436*t2 + 67*Power(t2,2)) + 
               s*(51 + 28*Power(s2,2) - 157*t2 + 93*Power(t2,2) - 
                  8*s2*(18 + 11*t2))) + 
            Power(t1,6)*(90 + Power(s2,3)*(41 - 91*t2) - 3407*t2 - 
               320*Power(t2,2) - 118*Power(t2,3) - 
               5*Power(s,3)*(-8 + 5*t2) - 
               Power(s2,2)*(173 + 411*t2 + 27*Power(t2,2)) + 
               s2*(281 + 2865*t2 + 384*Power(t2,2) + 57*Power(t2,3)) + 
               Power(s,2)*(65 + 732*t2 - 102*Power(t2,2) + 
                  s2*(-165 + 29*t2)) + 
               s*(-462 + 27*t2 + 197*Power(t2,2) - 63*Power(t2,3) + 
                  Power(s2,2)*(-70 + 59*t2) + 
                  2*s2*(57 - 397*t2 + 51*Power(t2,2)))) - 
            Power(t1,5)*(311 + 2089*t2 + 4941*Power(t2,2) + 
               1040*Power(t2,3) - 8*Power(t2,4) + 
               Power(s2,3)*(18 + 178*t2 - 171*Power(t2,2)) + 
               Power(s,3)*(-21 + 30*t2 + 20*Power(t2,2)) - 
               Power(s2,2)*(-177 + 1052*t2 + 817*Power(t2,2) + 
                  61*Power(t2,3)) + 
               s2*(-407 - 398*t2 + 80*Power(t2,2) - 406*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s,2)*(118 + 755*t2 + 882*Power(t2,2) - 
                  44*Power(t2,3) + s2*(-100 + 10*t2 - 163*Power(t2,2))) \
+ s*(145 + 442*t2 - 1286*Power(t2,2) + 480*Power(t2,3) - 
                  12*Power(t2,4) + 
                  Power(s2,2)*(5 + 202*t2 + 314*Power(t2,2)) + 
                  s2*(-275 - 531*t2 + 27*Power(t2,2) + 105*Power(t2,3)))\
) + Power(t1,3)*(Power(s,3)*(2 + t2 + 41*Power(t2,2) + 
                  149*Power(t2,3) - 26*Power(t2,4)) + 
               t2*(915 + 4076*t2 + 4277*Power(t2,2) + 
                  812*Power(t2,3) + 16*Power(t2,4)) + 
               Power(s2,3)*(20 + 204*t2 + 485*Power(t2,2) - 
                  1066*Power(t2,3) + 26*Power(t2,4)) + 
               Power(s2,2)*(102 + 254*t2 - 4389*Power(t2,2) - 
                  1727*Power(t2,3) + 53*Power(t2,4)) + 
               Power(s,2)*(-6 + (293 - 301*s2)*t2 + 
                  (297 + 281*s2)*Power(t2,2) + 
                  (1370 - 963*s2)*Power(t2,3) + 
                  13*(17 + 6*s2)*Power(t2,4)) + 
               s2*(-237 - 2105*t2 + 702*Power(t2,2) + 
                  3195*Power(t2,3) - 563*Power(t2,4) + 46*Power(t2,5)) \
+ s*(138 + 993*t2 + 805*Power(t2,2) - 3070*Power(t2,3) + 
                  290*Power(t2,4) - 46*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-8 + 427*t2 + 1880*Power(t2,2) - 78*Power(t2,3)) + 
                  s2*(-113 - 903*t2 + 1511*Power(t2,2) + 
                     381*Power(t2,3) - 274*Power(t2,4)))) + 
            t1*(4 + 25*t2 - 395*Power(t2,2) - 1262*Power(t2,3) - 
               729*Power(t2,4) - 127*Power(t2,5) - 12*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*
                (2 - 49*t2 + 14*Power(t2,2) + 55*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-30 + 957*t2 + 3208*Power(t2,2) + 617*Power(t2,3) - 
                  236*Power(t2,4)) + 
               Power(s2,3)*t2*
                (-60 - 235*t2 + 295*Power(t2,2) + 981*Power(t2,3) - 
                  55*Power(t2,4)) + 
               s2*(30 + 469*t2 + 1539*Power(t2,2) - 941*Power(t2,3) - 
                  2117*Power(t2,4) + 112*Power(t2,5) - 12*Power(t2,6)) \
+ s*(-6 - 5*(21 - 28*s2 + 6*Power(s2,2))*t2 + 
                  (-583 + 194*s2 - 218*Power(s2,2))*Power(t2,2) + 
                  (480 - 1831*s2 - 733*Power(s2,2))*Power(t2,3) + 
                  (1591 + 566*s2 - 1358*Power(s2,2))*Power(t2,4) + 
                  (179 + 583*s2 + 165*Power(s2,2))*Power(t2,5) + 
                  12*Power(t2,6)) + 
               Power(s,2)*t2*
                (-4 + 109*t2 + 189*Power(t2,2) - 891*Power(t2,3) - 
                  347*Power(t2,4) + 
                  s2*(6 + 43*t2 - 269*Power(t2,2) + 363*Power(t2,3) - 
                     165*Power(t2,4)))) + 
            Power(t1,4)*(-256 + 89*t2 + 8762*Power(t2,2) + 
               2499*Power(t2,3) + 354*Power(t2,4) + 
               Power(s,3)*(-23 - 192*t2 + 91*Power(t2,2) + 
                  67*Power(t2,3)) - 
               Power(s2,3)*(35 + 148*t2 - 798*Power(t2,2) + 
                  118*Power(t2,3)) + 
               Power(s2,2)*(185 + 1434*t2 + 5*Power(t2,2) - 
                  343*Power(t2,3) - 14*Power(t2,4)) - 
               s2*(-162 + 1698*t2 + 7016*Power(t2,2) + 
                  701*Power(t2,3) + 328*Power(t2,4)) - 
               Power(s,2)*(-102 + 578*t2 + 1218*Power(t2,2) + 
                  105*Power(t2,3) + 14*Power(t2,4) + 
                  s2*(-5 - 936*t2 + 114*Power(t2,2) + 252*Power(t2,3))) \
+ s*(-60 + 1205*t2 - 1108*Power(t2,2) - 315*Power(t2,3) + 
                  330*Power(t2,4) + 
                  Power(s2,2)*
                   (50 + 456*t2 - 490*Power(t2,2) + 303*Power(t2,3)) + 
                  s2*(-200 - 66*t2 + 3336*Power(t2,2) + 
                     674*Power(t2,3) + 28*Power(t2,4)))) - 
            Power(t1,2)*(-49 - 597*t2 + 348*Power(t2,2) + 
               5691*Power(t2,3) + 2233*Power(t2,4) + 178*Power(t2,5) + 
               Power(s2,3)*t2*
                (-40 + 309*t2 + 1307*Power(t2,2) - 481*Power(t2,3)) + 
               Power(s,3)*t2*
                (-25 - 157*t2 + 79*Power(t2,2) + 229*Power(t2,3)) + 
               s2*(80 + 145*t2 - 3035*Power(t2,2) - 4448*Power(t2,3) + 
                  505*Power(t2,4) - 243*Power(t2,5)) + 
               2*Power(s2,2)*
                (30 + 392*t2 + 1218*Power(t2,2) - 1063*Power(t2,3) - 
                  537*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s,2)*(6 + (42 + 85*s2)*t2 + 
                  (-818 + 1157*s2)*Power(t2,2) - 
                  3*(373 + 150*s2)*Power(t2,3) - 
                  (727 + 939*s2)*Power(t2,4) + 16*Power(t2,5)) + 
               s*(-61 - 252*t2 + 951*Power(t2,2) - 792*Power(t2,3) - 
                  647*Power(t2,4) + 145*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (85 + 653*t2 - 267*Power(t2,2) + 1191*Power(t2,3)) + 
                  s2*(-30 - 304*t2 + 572*Power(t2,2) + 
                     6106*Power(t2,3) + 2208*Power(t2,4) - 32*Power(t2,5)\
)))) + Power(s1,6)*(-1 + 56*Power(t1,9) - 15*t2 + s*t2 - 
            30*Power(t2,2) + 4*s*Power(t2,2) + 
            2*Power(s,2)*Power(t2,2) - 83*Power(t2,3) + 
            29*s*Power(t2,3) - 6*Power(s,2)*Power(t2,3) - 
            107*Power(t2,4) + 171*s*Power(t2,4) - 
            88*Power(s,2)*Power(t2,4) + 20*Power(s,3)*Power(t2,4) - 
            20*Power(t2,5) + 47*s*Power(t2,5) - 
            46*Power(s,2)*Power(t2,5) + 11*Power(s,3)*Power(t2,5) - 
            3*Power(t1,8)*(122 + 23*t2) + 
            Power(t1,7)*(509 - 140*Power(s,2) + s*(52 - 36*t2) - 
               261*t2 + 20*Power(t2,2)) + 
            Power(t1,6)*(424 + 2645*t2 + 572*Power(t2,2) + 
               Power(s,2)*(154 + 135*t2) + 
               s*(192 - 325*t2 + 105*Power(t2,2))) + 
            Power(t1,5)*(85 - 10*Power(s,3)*(-3 + t2) - 3011*t2 - 
               437*Power(t2,2) - 164*Power(t2,3) + 
               Power(s,2)*(83 + 556*t2 + 25*Power(t2,2)) + 
               s*(-166 + 154*t2 + 75*Power(t2,2) - 72*Power(t2,3))) - 
            Power(t1,4)*(163 + 2053*t2 + 4980*Power(t2,2) + 
               1050*Power(t2,3) + 2*Power(t2,4) + 
               Power(s,3)*(-4 - 8*t2 + 60*Power(t2,2)) + 
               Power(s,2)*(113 + 322*t2 + 330*Power(t2,2) + 
                  34*Power(t2,3)) + 
               s*(249 + 886*t2 - 1590*Power(t2,2) + 180*Power(t2,3) - 
                  12*Power(t2,4))) + 
            Power(s2,3)*(28*Power(t1,6) + Power(t1,5)*(64 - 189*t2) + 
               5*Power(t1,4)*(-9 - 63*t2 + 56*Power(t2,2)) - 
               Power(t1,3)*(30 + 27*t2 - 966*Power(t2,2) + 
                  142*Power(t2,3)) + 
               t1*t2*(-15 - 401*t2 - 645*Power(t2,2) + 
                  237*Power(t2,3)) + 
               t2*(-20 - 67*t2 + 209*Power(t2,2) + 232*Power(t2,3) - 
                  11*Power(t2,4)) + 
               Power(t1,2)*(20 + 225*t2 + 294*Power(t2,2) - 
                  884*Power(t2,3) + 20*Power(t2,4))) + 
            Power(t1,3)*(-151 - 54*t2 + 4153*Power(t2,2) + 
               1651*Power(t2,3) + 204*Power(t2,4) + 
               Power(s,3)*(-4 - 86*t2 + 11*Power(t2,2) + 
                  78*Power(t2,3)) + 
               Power(s,2)*(16 - 306*t2 - 617*Power(t2,2) - 
                  532*Power(t2,3) + 2*Power(t2,4)) + 
               s*(-142 + 81*t2 - 392*Power(t2,2) - 85*Power(t2,3) + 
                  121*Power(t2,4))) + 
            Power(t1,2)*(-40 + 182*t2 + 1603*Power(t2,2) + 
               2179*Power(t2,3) + 420*Power(t2,4) + 12*Power(t2,5) + 
               Power(s,3)*t2*
                (-2 + 6*t2 + 93*Power(t2,2) - 20*Power(t2,3)) + 
               Power(s,2)*(13 + 39*t2 - 293*Power(t2,2) + 
                  390*Power(t2,3) + 259*Power(t2,4)) + 
               s*(46 + 582*t2 + 511*Power(t2,2) - 1816*Power(t2,3) - 
                  234*Power(t2,4) - 12*Power(t2,5))) + 
            t1*(22 + 130*t2 + 33*Power(t2,2) - 839*Power(t2,3) - 
               394*Power(t2,4) - 32*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(10 - 4*t2 - 85*Power(t2,2)) + 
               Power(s,2)*t2*
                (13 + 99*t2 + 288*Power(t2,2) + 332*Power(t2,3) - 
                  15*Power(t2,4)) + 
               s*(8 + 113*t2 - 154*Power(t2,2) - 85*Power(t2,3) + 
                  110*Power(t2,4) + 26*Power(t2,5))) + 
            Power(s2,2)*(112*Power(t1,7) + 
               Power(t1,6)*(-88 + 56*s - 181*t2) + 
               Power(t1,2)*(55 + (-84 + 50*s)*t2 + 
                  10*(-283 + 47*s)*Power(t2,2) + 
                  17*(-65 + 87*s)*Power(t2,3) + 
                  (148 - 60*s)*Power(t2,4)) - 
               t1*(30 + (422 + 43*s)*t2 + (917 + 274*s)*Power(t2,2) + 
                  (-667 + 13*s)*Power(t2,3) + 
                  (-450 + 559*s)*Power(t2,4) + 15*Power(t2,5)) + 
               Power(t1,5)*(-283 - 540*t2 + 55*Power(t2,2) + 
                  2*s*(-70 + 79*t2)) + 
               Power(t1,4)*(-200 + 1255*t2 + 1044*Power(t2,2) + 
                  4*Power(t2,3) - s*(5 + 192*t2 + 535*Power(t2,2))) + 
               Power(t1,3)*(193 + 1570*t2 - 18*Power(t2,2) - 
                  552*Power(t2,3) + 2*Power(t2,4) + 
                  s*(50 + 456*t2 - 635*Power(t2,2) + 362*Power(t2,3))) \
+ t2*(40 + 386*t2 + 454*Power(t2,2) + 67*Power(t2,3) - 35*Power(t2,4) + 
                  s*(-6 - 122*t2 - 326*Power(t2,2) - 270*Power(t2,3) + 
                     33*Power(t2,4)))) + 
            s2*(6 + 140*Power(t1,8) + 
               Power(t1,7)*(-506 + 84*s - 93*t2) + (92 + 7*s)*t2 + 
               (109 + 56*s - 7*Power(s,2))*Power(t2,2) - 
               (86 + 127*s + 77*Power(s,2))*Power(t2,3) + 
               (-200 + 25*s + 18*Power(s,2))*Power(t2,4) + 
               (-13 + 81*s - 33*Power(s,2))*Power(t2,5) - 
               Power(t1,6)*(215 + s*(178 - 88*t2) + 542*t2 + 
                  75*Power(t2,2)) + 
               Power(t1,5)*(204 + 3105*t2 + 532*Power(t2,2) + 
                  68*Power(t2,3) - Power(s,2)*(136 + 15*t2) + 
                  s*(47 - 621*t2 - 130*Power(t2,2))) + 
               Power(t1,4)*(470 + 375*t2 - 597*Power(t2,2) + 
                  225*Power(t2,3) - 12*Power(t2,4) + 
                  Power(s,2)*(60 - 131*t2 + 315*Power(t2,2)) + 
                  s*(371 - 53*t2 - 928*Power(t2,2) + 30*Power(t2,3))) + 
               Power(t1,2)*(-146 - 1254*t2 + 251*Power(t2,2) + 
                  1931*Power(t2,3) - 61*Power(t2,4) + 12*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-49 + 258*t2 - 688*Power(t2,2) + 60*Power(t2,3)) + 
                  s*(-82 - 505*t2 + 1701*Power(t2,2) + 
                     831*Power(t2,3) - 407*Power(t2,4))) + 
               Power(t1,3)*(161 - 922*t2 - 4444*Power(t2,2) - 
                  364*Power(t2,3) - 155*Power(t2,4) + 
                  Power(s,2)*
                   (1 + 523*t2 + 3*Power(t2,2) - 298*Power(t2,3)) + 
                  s*(-89 - 118*t2 + 2285*Power(t2,2) + 
                     1328*Power(t2,3) - 4*Power(t2,4))) + 
               t1*(-7 + 54*t2 + 890*Power(t2,2) + 1342*Power(t2,3) - 
                  131*Power(t2,4) + 12*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-13 - 213*t2 + 283*Power(t2,2) + 407*Power(t2,3)) + 
                  s*(6 + 34*t2 - 177*Power(t2,2) - 1891*Power(t2,3) - 
                     952*Power(t2,4) + 30*Power(t2,5))))) - 
         Power(s1,2)*(2*(-2 + Power(s,2) + s2 - s*s2)*Power(t1,11) + 
            2*t1*t2*(-30 - 3*
                (96 + 18*Power(s,2) + 5*s2 - 24*Power(s2,2) - 
                  2*s*(17 + 9*s2))*t2 + 
               (-511 + Power(s,3) - 480*s2 + 425*Power(s2,2) - 
                  26*Power(s2,3) + Power(s,2)*(-117 + 20*s2) + 
                  s*(441 + 4*s2 + 77*Power(s2,2)))*Power(t2,2) + 
               (1326 - 114*Power(s,3) - 2409*s2 + 294*Power(s2,2) + 
                  29*Power(s2,3) + Power(s,2)*(-968 + 897*s2) + 
                  3*s*(289 + 494*s2 + 76*Power(s2,2)))*Power(t2,3) + 
               (2474 + 97*Power(s,3) + Power(s,2)*(30 - 390*s2) + 
                  57*s2 - 1984*Power(s2,2) + 156*Power(s2,3) + 
                  s*(-1867 + 3378*s2 + 305*Power(s2,2)))*Power(t2,4) + 
               (10 + 88*Power(s,3) + Power(s,2)*(246 - 399*s2) + 
                  412*s2 - 214*Power(s2,2) - 223*Power(s2,3) + 
                  s*(19 + 8*s2 + 534*Power(s2,2)))*Power(t2,5) + 
               (27 + 7*Power(s,2) - 189*s2 + 7*Power(s2,2) - 
                  2*s*(-67 + 7*s2))*Power(t2,6)) + 
            2*Power(t1,3)*(-10 + 
               (188 + 72*Power(s,2) + 155*s2 - 24*Power(s2,2) - 
                  s*(361 + 6*s2))*t2 + 
               (151 + 84*Power(s,3) - 294*s2 - 85*Power(s2,2) + 
                  58*Power(s2,3) + Power(s,2)*(-388 + 242*s2) + 
                  s*(57 + 89*s2 - 180*Power(s2,2)))*Power(t2,2) + 
               (-354 + 339*Power(s,3) + Power(s,2)*(586 - 1150*s2) + 
                  1651*s2 - 878*Power(s2,2) + 136*Power(s2,3) - 
                  3*s*(855 - 296*s2 + 151*Power(s2,2)))*Power(t2,3) - 
               (5378 + 226*Power(s,3) + Power(s,2)*(6 - 468*s2) - 
                  2911*s2 - 881*Power(s2,2) + 248*Power(s2,3) + 
                  s*(-1669 + 2327*s2 + 126*Power(s2,2)))*Power(t2,4) + 
               (-49*Power(s,3) + Power(s,2)*(-334 + 200*s2) + 
                  s*(-620 + 288*s2 - 253*Power(s2,2)) + 
                  2*(-169 + 246*s2 + 6*Power(s2,2) + 51*Power(s2,3)))*
                Power(t2,5) + 
               (-59 + 26*Power(s,2) + 189*s2 + 26*Power(s2,2) - 
                  4*s*(41 + 13*s2))*Power(t2,6)) + 
            Power(t1,10)*(14 + Power(s,2)*(3 - 6*t2) - 3*t2 - 
               2*Power(s2,2)*t2 + s2*(-5 + 6*t2) + 
               s*(-4*(1 + t2) + s2*(3 + 8*t2))) + 
            Power(t1,9)*(-49 + 2*Power(s,3)*(-1 + t2) + 135*t2 - 
               Power(t2,2) + Power(s2,2)*(3 + 9*t2 + 4*Power(t2,2)) - 
               s2*(2 + 89*t2 + 4*Power(t2,2)) + 
               Power(s,2)*(-15 - 4*s2*(-3 + t2) - 59*t2 + 
                  6*Power(t2,2)) + 
               s*(83 + 5*t2 + Power(t2,2) + Power(s2,2)*(-1 + 2*t2) + 
                  s2*(-21 + 57*t2 - 10*Power(t2,2)))) + 
            Power(t1,8)*(85 - 191*t2 + 6*Power(t2,2) + 8*Power(t2,3) + 
               Power(s2,3)*t2*(-3 + 2*t2) - 
               2*Power(s,3)*(15 - 8*t2 + 3*Power(t2,2)) + 
               s2*(15 + 139*t2 - 74*Power(t2,2) - 11*Power(t2,3)) - 
               Power(s2,2)*(1 + 13*t2 - 25*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s,2)*(43 + 54*t2 + 134*Power(t2,2) - 
                  2*Power(t2,3) + s2*(-8 - 35*t2 + 14*Power(t2,2))) + 
               s*(-95 - 80*t2 + 72*Power(t2,2) + 10*Power(t2,3) + 
                  Power(s2,2)*(1 + 25*t2 - 10*Power(t2,2)) + 
                  s2*(3 - 130*t2 - 167*Power(t2,2) + 4*Power(t2,3)))) + 
            Power(t1,7)*(-71 + 504*t2 - 1339*Power(t2,2) - 
               11*Power(t2,3) + Power(s2,3)*(7*t2 - 4*Power(t2,3)) + 
               Power(s,3)*(50 + 66*t2 - 55*Power(t2,2) + 
                  6*Power(t2,3)) - 
               Power(s2,2)*(13 + 72*t2 + 134*Power(t2,2) + 
                  78*Power(t2,3)) + 
               s2*(-61 + 74*t2 + 924*Power(t2,2) + 112*Power(t2,3) + 
                  7*Power(t2,4)) - 
               Power(s,2)*(192 - 224*t2 - 345*Power(t2,2) + 
                  122*Power(t2,3) + 
                  s2*(5 + 243*t2 - 121*Power(t2,2) + 16*Power(t2,3))) + 
               s*(130 - 1095*t2 + 140*Power(t2,2) - 89*Power(t2,3) - 
                  7*Power(t2,4) + 
                  Power(s2,2)*t2*(4 - 69*t2 + 14*Power(t2,2)) + 
                  s2*(85 + 318*t2 - 377*Power(t2,2) + 200*Power(t2,3)))) \
+ Power(t1,4)*(85 - 53*t2 + 2818*Power(t2,2) - 911*Power(t2,3) - 
               857*Power(t2,4) + 298*Power(t2,5) - 36*Power(t2,6) + 
               Power(s,3)*t2*
                (60 + 38*t2 + 236*Power(t2,2) - 178*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,3)*t2*
                (12 - 7*t2 + 33*Power(t2,2) - 204*Power(t2,3) + 
                  5*Power(t2,4)) - 
               Power(s2,2)*t2*
                (-54 + 216*t2 + 1542*Power(t2,2) + 387*Power(t2,3) + 
                  293*Power(t2,4)) + 
               s*(-60 + (1149 + 191*s2 - 30*Power(s2,2))*t2 + 
                  (-3310 + 680*s2 + 328*Power(s2,2))*Power(t2,2) + 
                  (-1323 - 4141*s2 + 666*Power(s2,2))*Power(t2,3) + 
                  (755 - 1683*s2 + 64*Power(s2,2))*Power(t2,4) + 
                  (-25 + 499*s2 - 15*Power(s2,2))*Power(t2,5) - 
                  30*Power(t2,6)) + 
               s2*(30 - 704*t2 - 237*Power(t2,2) + 2952*Power(t2,3) + 
                  52*Power(t2,4) - 271*Power(t2,5) + 30*Power(t2,6)) + 
               Power(s,2)*t2*
                (-814 + 1108*t2 + 2652*Power(t2,2) + 1602*Power(t2,3) - 
                  206*Power(t2,4) + 
                  s2*(60 - 1931*t2 - 713*Power(t2,2) + 
                     318*Power(t2,3) + 15*Power(t2,4)))) + 
            2*Power(t2,2)*(15 + 
               (138 + 35*s - 43*Power(s,2) + 2*Power(s,3))*t2 + 
               (319 - 179*s + 54*Power(s,2) + 19*Power(s,3))*
                Power(t2,2) + 
               (116 - 508*s + 204*Power(s,2) + 5*Power(s,3))*
                Power(t2,3) - 
               (307 + 168*s - 386*Power(s,2) + 43*Power(s,3))*
                Power(t2,4) + 
               (-30 + 25*s + 87*Power(s,2) - 23*Power(s,3))*
                Power(t2,5) - 5*(-1 + s)*Power(t2,6) + 
               Power(s2,3)*t2*
                (20 - 93*t2 - 139*Power(t2,2) - 43*Power(t2,3) + 
                  23*Power(t2,4)) + 
               s2*(-90 - (181 + 54*s)*t2 + 
                  (95 - 324*s - 19*Power(s,2))*Power(t2,2) + 
                  (616 - 432*s + 299*Power(s,2))*Power(t2,3) + 
                  (454 - 740*s + 171*Power(s,2))*Power(t2,4) + 
                  3*(-33 - 54*s + 23*Power(s,2))*Power(t2,5) + 
                  5*Power(t2,6)) - 
               Power(s2,2)*t2*
                (199 + 338*t2 + 244*Power(t2,2) - 66*Power(t2,3) - 
                  75*Power(t2,4) + 
                  s*(-90 - 61*t2 + 245*Power(t2,2) + 85*Power(t2,3) + 
                     69*Power(t2,4)))) + 
            2*Power(t1,2)*t2*
             (-66 - 372*t2 - 1675*Power(t2,2) + 427*Power(t2,3) + 
               875*Power(t2,4) - 75*Power(t2,5) + 22*Power(t2,6) + 
               Power(s2,3)*t2*
                (-18 - 75*t2 + 44*Power(t2,2) + 345*Power(t2,3) - 
                  36*Power(t2,4)) + 
               Power(s,3)*t2*
                (-18 - 159*t2 - 108*Power(t2,2) + 109*Power(t2,3) + 
                  36*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-147 + 471*t2 + 1544*Power(t2,2) - 197*Power(t2,3) + 
                  121*Power(t2,4)) + 
               s2*(18 + 798*t2 + 662*Power(t2,2) - 2176*Power(t2,3) - 
                  626*Power(t2,4) + 130*Power(t2,5) - 30*Power(t2,6)) + 
               s*(18 - 6*(104 - 8*s2 + 3*Power(s2,2))*t2 - 
                  5*(-244 + 74*s2 + Power(s2,2))*Power(t2,2) + 
                  (-238 + 2244*s2 - 376*Power(s2,2))*Power(t2,3) + 
                  (116 + 1650*s2 - 353*Power(s2,2))*Power(t2,4) + 
                  6*(29 - 18*s2 + 18*Power(s2,2))*Power(t2,5) + 
                  30*Power(t2,6)) - 
               Power(s,2)*t2*
                (-303 + 709*t2 + 1016*Power(t2,2) + 1053*Power(t2,3) + 
                  13*Power(t2,4) + 
                  s2*(18 - 1183*t2 - 272*Power(t2,2) + 
                     101*Power(t2,3) + 108*Power(t2,4)))) + 
            Power(t1,6)*(82 - 906*t2 + 719*Power(t2,2) + 
               199*Power(t2,3) - 106*Power(t2,4) + 
               Power(s2,3)*t2*
                (13 + 12*t2 - 4*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s,3)*(-20 + 110*t2 - 104*Power(t2,2) + 
                  65*Power(t2,3) - 2*Power(t2,4)) + 
               Power(s2,2)*(17 + 64*t2 + 212*Power(t2,2) + 
                  19*Power(t2,3) + 68*Power(t2,4)) + 
               s2*(128 - 68*t2 - 1007*Power(t2,2) + 225*Power(t2,3) + 
                  103*Power(t2,4)) + 
               Power(s,2)*(215 + (-265 + 403*s2)*t2 + 
                  7*(-129 + 40*s2)*Power(t2,2) - 
                  2*(381 + 76*s2)*Power(t2,3) + (63 + 6*s2)*Power(t2,4)\
) - s*(304 - 1044*t2 - 835*Power(t2,2) + 394*Power(t2,3) + 
                  64*Power(t2,4) + 
                  Power(s2,2)*t2*
                   (102 + 232*t2 - 91*Power(t2,2) + 6*Power(t2,3)) + 
                  s2*(98 + 277*t2 - 1369*Power(t2,2) - 
                     851*Power(t2,3) + 131*Power(t2,4)))) + 
            Power(t1,5)*(-122 + 330*t2 - 1155*Power(t2,2) + 
               5743*Power(t2,3) + 170*Power(t2,4) + 58*Power(t2,5) + 
               Power(s2,3)*t2*
                (-29 - 105*t2 + 97*Power(t2,2) + Power(t2,3)) - 
               Power(s,3)*t2*
                (200 + 380*t2 - 272*Power(t2,2) + 19*Power(t2,3)) + 
               Power(s2,2)*(-6 + 8*t2 + 555*Power(t2,2) + 
                  349*Power(t2,3) + 270*Power(t2,4) - 19*Power(t2,5)) - 
               Power(s,2)*(60 + (-807 + 175*s2)*t2 + 
                  (865 - 1207*s2)*Power(t2,2) + 
                  (350 + 579*s2)*Power(t2,3) - 
                  (532 + 39*s2)*Power(t2,4) + 19*Power(t2,5)) - 
               s2*(107 - 296*t2 + 664*Power(t2,2) + 3834*Power(t2,3) + 
                  649*Power(t2,4) + 114*Power(t2,5)) + 
               s*(250 - 375*t2 + 4056*Power(t2,2) - 1317*Power(t2,3) + 
                  630*Power(t2,4) + 110*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (113 + 232*t2 + 267*Power(t2,2) - 21*Power(t2,3)) + 
                  s2*(30 - 194*t2 - 1433*Power(t2,2) + 
                     1113*Power(t2,3) - 792*Power(t2,4) + 38*Power(t2,5))\
))) + Power(s1,3)*((-3 + 2*s2)*Power(t1,11) + 
            Power(t1,10)*(41 - 16*Power(s,2) - 25*s2 + 2*Power(s2,2) + 
               s*(5 + 15*s2 - 3*t2) - 3*t2) - 
            2*t2*(-10 + 5*(-11 - 14*s2 + 18*Power(s2,2) + 
                  s*(-5 + 6*s2))*t2 + 
               (-94 - 4*Power(s,3) - 417*s2 + 64*Power(s2,2) + 
                  85*Power(s2,3) + Power(s,2)*(10 + 21*s2) + 
                  2*s*(-2 + 83*s2 + 25*Power(s2,2)))*Power(t2,2) + 
               (162 - 15*Power(s,3) - 579*s2 - 385*Power(s2,2) + 
                  84*Power(s2,3) + 7*Power(s,2)*(-17 + 14*s2) + 
                  s*(153 + 568*s2 + 457*Power(s2,2)))*Power(t2,3) - 
               (-538 + 22*Power(s,3) + 173*s2 + 491*Power(s2,2) + 
                  135*Power(s2,3) + Power(s,2)*(13 + 147*s2) + 
                  s*(305 - 1008*s2 - 424*Power(s2,2)))*Power(t2,4) + 
               (125 + 9*Power(s,3) + 237*s2 - 169*Power(s2,2) - 
                  106*Power(s2,3) - 31*Power(s,2)*(-1 + 4*s2) + 
                  s*(-172 + 170*s2 + 221*Power(s2,2)))*Power(t2,5) + 
               (6 + s + 11*Power(s,2) - 22*s2 - 22*s*s2 + 
                  11*Power(s2,2))*Power(t2,6)) - 
            2*Power(t1,2)*(10 + 
               (188 + 24*Power(s,2) - 35*s2 - 72*Power(s2,2) + 
                  s*(-47 + 6*s2))*t2 + 
               (741 + 50*Power(s,3) + 58*s2 - 555*Power(s2,2) + 
                  36*Power(s2,3) - 4*Power(s,2)*(28 + 15*s2) + 
                  s*(-201 + 331*s2 - 62*Power(s2,2)))*Power(t2,2) + 
               (-1481 + 253*Power(s,3) + 3278*s2 - 988*Power(s2,2) - 
                  46*Power(s2,3) - 8*Power(s,2)*(-157 + 174*s2) - 
                  s*(1778 + 1208*s2 + 359*Power(s2,2)))*Power(t2,3) + 
               (-5521 - 188*Power(s,3) + 1686*s2 + 2509*Power(s2,2) - 
                  454*Power(s2,3) + Power(s,2)*(460 + 574*s2) + 
                  s*(2368 - 5101*s2 - 240*Power(s2,2)))*Power(t2,4) - 
               (839 + 155*Power(s,3) + Power(s,2)*(136 - 626*s2) + 
                  281*s2 - 382*Power(s2,2) - 316*Power(s2,3) + 
                  s*(-11 + 412*s2 + 787*Power(s2,2)))*Power(t2,5) + 
               (-106 - 289*s + 12*Power(s,2) + 342*s2 - 24*s*s2 + 
                  12*Power(s2,2))*Power(t2,6)) + 
            Power(t1,9)*(-40 + 77*t2 + 8*Power(t2,2) + 
               Power(s2,2)*(-2 + 7*t2) + Power(s,2)*(8 + 42*t2) + 
               s2*(14 - 56*t2 - 5*Power(t2,2)) + 
               s*(-10 + Power(s2,2) + 13*t2 + 6*Power(t2,2) - 
                  s2*(23 + 49*t2))) + 
            Power(t1,8)*(110 + Power(s,3)*(12 - 11*t2) + 
               Power(s2,3)*(2 - 3*t2) - 789*t2 - 2*Power(t2,2) - 
               2*Power(t2,3) - 
               3*Power(s2,2)*(7 + 21*t2 + 8*Power(t2,2)) + 
               s2*(54 + 531*t2 + 41*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s,2)*(32 + 246*t2 - 43*Power(t2,2) + 
                  s2*(-53 + 21*t2)) + 
               s*(-275 + Power(s2,2)*(2 - 8*t2) - 13*t2 + 
                  20*Power(t2,2) - 3*Power(t2,3) + 
                  s2*(76 - 244*t2 + 66*Power(t2,2)))) + 
            Power(t1,7)*(-207 + 174*t2 - 481*Power(t2,2) - 
               127*Power(t2,3) + Power(s2,3)*t2*(4 + t2) + 
               Power(s,3)*(50 - 46*t2 + 23*Power(t2,2)) + 
               Power(s2,2)*(-13 + 111*t2 + 11*Power(t2,2) + 
                  22*Power(t2,3)) + 
               s2*(-9 - 202*t2 + 280*Power(t2,2) + 96*Power(t2,3)) + 
               Power(s,2)*(-62 - 353*t2 - 522*Power(t2,2) + 
                  24*Power(t2,3) + s2*(42 + 100*t2 - 47*Power(t2,2))) + 
               s*(179 + 301*t2 - 82*Power(t2,2) - 107*Power(t2,3) + 
                  Power(s2,2)*(-4 - 94*t2 + 23*Power(t2,2)) + 
                  s2*(1 + 482*t2 + 533*Power(t2,2) - 46*Power(t2,3)))) + 
            2*Power(t1,3)*(-22 - 199*t2 - 1683*Power(t2,2) - 
               1209*Power(t2,3) - 23*Power(t2,4) - 166*Power(t2,5) + 
               30*Power(t2,6) - 
               Power(s2,3)*t2*
                (16 + 67*t2 + 111*Power(t2,2) - 493*Power(t2,3) + 
                  25*Power(t2,4)) + 
               Power(s,3)*t2*
                (-16 - 66*t2 - 115*Power(t2,2) + 38*Power(t2,3) + 
                  25*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-94 + 39*t2 + 2372*Power(t2,2) + 431*Power(t2,3) + 
                  136*Power(t2,4)) + 
               s2*(6 + 575*t2 + 1106*Power(t2,2) - 1693*Power(t2,3) - 
                  1308*Power(t2,4) + 372*Power(t2,5) - 42*Power(t2,6)) \
+ s*(6 - (487 + 16*s2 + 6*Power(s2,2))*t2 + 
                  (604 + 190*s2 - 62*Power(s2,2))*Power(t2,2) + 
                  (114 + 1349*s2 - 331*Power(s2,2))*Power(t2,3) + 
                  (600 + 1366*s2 - 718*Power(s2,2))*Power(t2,4) + 
                  (-159 - 173*s2 + 75*Power(s2,2))*Power(t2,5) + 
                  42*Power(t2,6)) + 
               Power(s,2)*t2*
                (236 - 601*t2 - 1231*Power(t2,2) - 1457*Power(t2,3) + 
                  37*Power(t2,4) + 
                  s2*(-6 + 923*t2 + 161*Power(t2,2) + 187*Power(t2,3) - 
                     75*Power(t2,4)))) + 
            Power(t1,6)*(33 - 545*t2 + 5079*Power(t2,2) + 
               339*Power(t2,3) + 68*Power(t2,4) + 
               Power(s2,3)*(-4 - 49*t2 + 73*Power(t2,2) + 
                  3*Power(t2,3)) - 
               Power(s,3)*(70 + 160*t2 - 146*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(s2,2)*(64 + 276*t2 + 309*Power(t2,2) + 
                  201*Power(t2,3) - 7*Power(t2,4)) - 
               s2*(-101 + 757*t2 + 3400*Power(t2,2) + 
                  436*Power(t2,3) + 85*Power(t2,4)) + 
               Power(s,2)*(274 - 468*t2 - 849*Power(t2,2) + 
                  413*Power(t2,3) - 7*Power(t2,4) + 
                  s2*(10 + 635*t2 - 301*Power(t2,2) + 29*Power(t2,3))) \
+ s*(-64 + 2313*t2 - 663*Power(t2,2) + 113*Power(t2,3) + 
                  87*Power(t2,4) + 
                  Power(s2,2)*
                   (5 + 52*t2 + 115*Power(t2,2) - 19*Power(t2,3)) + 
                  s2*(-215 - 488*t2 + 1265*Power(t2,2) - 
                     594*Power(t2,3) + 14*Power(t2,4)))) - 
            2*t1*t2*(30 - 129*t2 - 993*Power(t2,2) - 787*Power(t2,3) + 
               639*Power(t2,4) + 72*Power(t2,5) - 16*Power(t2,6) + 
               Power(s2,2)*t2*
                (-3 + 774*t2 + 2223*Power(t2,2) - 60*Power(t2,3) - 
                  158*Power(t2,4)) + 
               Power(s2,3)*t2*
                (-30 - 81*t2 + 150*Power(t2,2) + 479*Power(t2,3) - 
                  54*Power(t2,4)) + 
               Power(s,3)*t2*
                (-6 - 43*t2 - 42*Power(t2,2) + 69*Power(t2,3) + 
                  54*Power(t2,4)) + 
               s2*(90 + 698*t2 + 1072*Power(t2,2) - 1855*Power(t2,3) - 
                  1561*Power(t2,4) + 297*Power(t2,5) - 21*Power(t2,6)) \
+ s*(-30 + (-300 + 22*s2 - 90*Power(s2,2))*t2 - 
                  (38 + 122*s2 + 33*Power(s2,2))*Power(t2,2) + 
                  (1169 - 224*s2 - 102*Power(s2,2))*Power(t2,3) + 
                  (811 + 1806*s2 - 545*Power(s2,2))*Power(t2,4) + 
                  (-65 + 406*s2 + 162*Power(s2,2))*Power(t2,5) + 
                  21*Power(t2,6)) - 
               Power(s,2)*t2*
                (-109 + 60*t2 + 351*Power(t2,2) + 1186*Power(t2,3) + 
                  248*Power(t2,4) + 
                  s2*(-30 - 317*t2 + 198*Power(t2,2) + 3*Power(t2,3) + 
                     162*Power(t2,4)))) + 
            Power(t1,5)*(3 + 1542*t2 + 587*Power(t2,2) + 
               1076*Power(t2,3) + 484*Power(t2,4) - 24*Power(t2,5) + 
               Power(s,3)*(20 - 84*t2 + 172*Power(t2,2) - 
                  96*Power(t2,3) + Power(t2,4)) - 
               Power(s2,3)*(-2 + 15*t2 - 90*Power(t2,2) + 
                  173*Power(t2,3) + Power(t2,4)) - 
               Power(s2,2)*(24 - 137*t2 + 1240*Power(t2,2) + 
                  512*Power(t2,3) + 220*Power(t2,4)) + 
               Power(s,2)*(-258 + (526 - 772*s2)*t2 + 
                  (2010 - 358*s2)*Power(t2,2) + 
                  (1851 + 98*s2)*Power(t2,3) - (202 + 3*s2)*Power(t2,4)\
) + s2*(-264 - 292*t2 + 1153*Power(t2,2) + 50*Power(t2,3) - 
                  457*Power(t2,4) + 18*Power(t2,5)) + 
               s*(363 - 1222*t2 - 1009*Power(t2,2) + 11*Power(t2,3) + 
                  411*Power(t2,4) - 18*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (167 + 394*t2 + 171*Power(t2,2) + 3*Power(t2,3)) + 
                  s2*(157 - 95*t2 - 2467*Power(t2,2) - 
                     1539*Power(t2,3) + 422*Power(t2,4)))) - 
            Power(t1,4)*(-127 - 398*t2 + 445*Power(t2,2) + 
               13324*Power(t2,3) + 1682*Power(t2,4) + 278*Power(t2,5) + 
               2*Power(s,3)*t2*
                (-83 - 278*t2 + 206*Power(t2,2) + 33*Power(t2,3)) - 
               Power(s2,3)*t2*
                (83 + 190*t2 - 590*Power(t2,2) + 158*Power(t2,3)) + 
               Power(s2,2)*(6 + 340*t2 + 1544*Power(t2,2) - 
                  967*Power(t2,3) + 92*Power(t2,4) - 57*Power(t2,5)) - 
               Power(s,2)*(48 + 4*(-177 + 32*s2)*t2 + 
                  (1514 - 2256*s2)*Power(t2,2) + 
                  (838 + 841*s2)*Power(t2,3) + 
                  (-803 + 290*s2)*Power(t2,4) + 57*Power(t2,5)) - 
               s2*(115 - 440*t2 + 3543*Power(t2,2) + 7779*Power(t2,3) + 
                  1054*Power(t2,4) + 477*Power(t2,5)) + 
               s*(224 + 23*t2 + 5053*Power(t2,2) - 3327*Power(t2,3) + 
                  638*Power(t2,4) + 465*Power(t2,5) + 
                  2*Power(s2,2)*t2*
                   (90 + 228*t2 + 51*Power(t2,2) + 191*Power(t2,3)) + 
                  2*s2*(12 - 323*t2 - 338*Power(t2,2) + 
                     2364*Power(t2,3) - 374*Power(t2,4) + 57*Power(t2,5))\
))) + Power(s1,4)*(2*Power(t1,11) + Power(t1,10)*(-34 + 17*s2 - 3*t2) + 
            t1*(-10 + 5*(-34 + 21*s2 + 36*Power(s2,2) - 3*s*(7 + 2*s2))*
                t2 + (-774 - 24*Power(s,3) - 622*s2 + 
                  1019*Power(s2,2) + 60*Power(s2,3) + 
                  4*Power(s,2)*(-5 + 39*s2) + 
                  s*(41 - 159*s2 + 60*Power(s2,2)))*Power(t2,2) + 
               (663 - 69*Power(s,3) - 3758*s2 + 1041*Power(s2,2) + 
                  484*Power(s2,3) + Power(s,2)*(-1009 + 942*s2) + 
                  s*(1339 + 1876*s2 + 875*Power(s2,2)))*Power(t2,3) + 
               (3897 + 82*Power(s,3) - 2097*s2 - 2955*Power(s2,2) + 
                  550*Power(s2,3) - 10*Power(s,2)*(83 + 79*s2) + 
                  s*(-1144 + 6477*s2 + 778*Power(s2,2)))*Power(t2,4) + 
               (1247 + 199*Power(s,3) + 1021*s2 - 981*Power(s2,2) - 
                  566*Power(s2,3) - Power(s,2)*(277 + 964*s2) + 
                  s*(-822 + 1540*s2 + 1331*Power(s2,2)))*Power(t2,5) + 
               (75 + 36*Power(s,2) + s*(59 - 72*s2) - 169*s2 + 
                  36*Power(s2,2))*Power(t2,6)) + 
            Power(t1,9)*(170 - 56*Power(s,2) + 16*Power(s2,2) + 
               2*s*(14 + 24*s2 - 9*t2) - 29*t2 + Power(t2,2) - 
               5*s2*(24 + t2)) + 
            Power(t1,8)*(8 + Power(s2,3) + 523*t2 + 84*Power(t2,2) - 
               Power(s2,2)*(16 + 7*t2) + Power(s,2)*(56 + 121*t2) - 
               s2*(19 + 212*t2 + 29*Power(t2,2)) + 
               s*(-21 + 8*Power(s2,2) - 19*t2 + 38*Power(t2,2) - 
                  4*s2*(19 + 28*t2))) + 
            Power(t1,6)*(-321 - 780*t2 - 2428*Power(t2,2) - 
               539*Power(t2,3) + 6*Power(t2,4) + 
               Power(s,3)*(45 - 60*t2 + 23*Power(t2,2)) + 
               Power(s2,3)*(-3 - 43*t2 + 50*Power(t2,2)) + 
               Power(s2,2)*(-80 + 484*t2 + 304*Power(t2,2) + 
                  61*Power(t2,3)) + 
               s2*(148 + 65*t2 + 361*Power(t2,2) + 297*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s,2)*(-85 - 756*t2 - 962*Power(t2,2) + 
                  62*Power(t2,3) + s2*(90 + 109*t2 - 11*Power(t2,2))) + 
               s*(99 + 207*t2 + 390*Power(t2,2) - 361*Power(t2,3) + 
                  4*Power(t2,4) - 
                  2*Power(s2,2)*(3 + 87*t2 + 31*Power(t2,2)) + 
                  s2*(91 + 788*t2 + 664*Power(t2,2) - 123*Power(t2,3)))) \
- Power(t1,7)*(-119 + 2211*t2 + 89*Power(t2,2) + 36*Power(t2,3) + 
               6*Power(s,3)*(-5 + 4*t2) + Power(s2,3)*(-14 + 25*t2) + 
               Power(s2,2)*(74 + 206*t2 + 50*Power(t2,2)) + 
               Power(s,2)*(-46 + s2*(122 - 41*t2) - 556*t2 + 
                  108*Power(t2,2)) - 
               s2*(187 + 1621*t2 + 169*Power(t2,2) + 22*Power(t2,3)) + 
               s*(14*Power(s2,2) + 
                  s2*(-131 + 577*t2 - 150*Power(t2,2)) + 
                  3*(163 + 8*t2 - 38*Power(t2,2) + 8*Power(t2,3)))) + 
            2*t2*(5 - 5*(-8 + s)*t2 + 
               (105 + 23*s - 12*Power(s,2))*Power(t2,2) + 
               (185 - 238*s + 48*Power(s,2) + 6*Power(s,3))*
                Power(t2,3) + 
               (12 - 259*s + 244*Power(s,2) - 36*Power(s,3))*
                Power(t2,4) + 
               (3 - 23*s + 76*Power(s,2) - 20*Power(s,3))*Power(t2,5) - 
               2*(-1 + s)*Power(t2,6) + 
               Power(s2,3)*t2*
                (30 - 15*t2 - 278*Power(t2,2) - 143*Power(t2,3) + 
                  20*Power(t2,4)) + 
               s2*(-30 - 5*(29 + 7*s)*t2 + 
                  (-82 - 137*s + 27*Power(s,2))*Power(t2,2) + 
                  (295 + 17*s + 160*Power(s,2))*Power(t2,3) + 
                  (396 - 353*s + 99*Power(s,2))*Power(t2,4) + 
                  4*(-7 - 34*s + 15*Power(s,2))*Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s2,2)*t2*
                (-95 - 491*t2 - 383*Power(t2,2) - 19*Power(t2,3) + 
                  60*Power(t2,4) + 
                  s*(30 + 156*t2 + 172*Power(t2,2) + 80*Power(t2,3) - 
                     60*Power(t2,4)))) + 
            Power(t1,5)*(-146 - 78*t2 + 9282*Power(t2,2) + 
               1533*Power(t2,3) + 266*Power(t2,4) + 
               Power(s,3)*(-55 - 230*t2 + 174*Power(t2,2) + 
                  14*Power(t2,3)) - 
               Power(s2,3)*(19 + 130*t2 - 363*Power(t2,2) + 
                  41*Power(t2,3)) + 
               Power(s2,2)*(136 + 768*t2 + 240*Power(t2,2) + 
                  88*Power(t2,3) - 19*Power(t2,4)) - 
               s2*(-120 + 1669*t2 + 6482*Power(t2,2) + 
                  775*Power(t2,3) + 268*Power(t2,4)) + 
               Power(s,2)*(228 - 640*t2 - 1306*Power(t2,2) + 
                  454*Power(t2,3) - 19*Power(t2,4) + 
                  s2*(10 + 988*t2 - 314*Power(t2,2) - 69*Power(t2,3))) + 
               s*(18 + 2469*t2 - 1243*Power(t2,2) - 135*Power(t2,3) + 
                  282*Power(t2,4) + 
                  Power(s2,2)*
                   (25 + 238*t2 - 88*Power(t2,2) + 96*Power(t2,3)) + 
                  s2*(-280 - 280*t2 + 2692*Power(t2,2) - 
                     436*Power(t2,3) + 38*Power(t2,4)))) + 
            Power(t1,2)*(35 - 58*t2 - 1955*Power(t2,2) - 
               3747*Power(t2,3) - 658*Power(t2,4) - 141*Power(t2,5) - 
               36*Power(t2,6) + 
               Power(s2,2)*t2*
                (-206 + 713*t2 + 6470*Power(t2,2) + 958*Power(t2,3) - 
                  223*Power(t2,4)) - 
               Power(s2,3)*t2*
                (60 + 312*t2 + 90*Power(t2,2) - 1523*Power(t2,3) + 
                  88*Power(t2,4)) + 
               Power(s,3)*t2*
                (-12 - 60*t2 - 127*Power(t2,2) - 52*Power(t2,3) + 
                  88*Power(t2,4)) + 
               s2*(60 + 971*t2 + 3342*Power(t2,2) - 3163*Power(t2,3) - 
                  4553*Power(t2,4) + 769*Power(t2,5) - 66*Power(t2,6)) + 
               s*(-30 + (-530 + 199*s2 - 60*Power(s2,2))*t2 + 
                  (-1211 + 420*s2 - 6*Power(s2,2))*Power(t2,2) + 
                  (955 - 2162*s2 - 657*Power(s2,2))*Power(t2,3) + 
                  (3092 + 2297*s2 - 2350*Power(s2,2))*Power(t2,4) + 
                  (-230 + 732*s2 + 264*Power(s2,2))*Power(t2,5) + 
                  66*Power(t2,6)) - 
               Power(s,2)*t2*(-108 + 237*t2 + 297*Power(t2,2) + 
                  2591*Power(t2,3) + 509*Power(t2,4) + 
                  s2*(-30 - 654*t2 + 260*Power(t2,2) - 879*Power(t2,3) + 
                     264*Power(t2,4)))) + 
            Power(t1,4)*(75 + 1633*t2 + 3728*Power(t2,2) + 
               3778*Power(t2,3) + 866*Power(t2,4) - 36*Power(t2,5) + 
               Power(s2,2)*(45 + 403*t2 - 3379*Power(t2,2) - 
                  1380*Power(t2,3) - 199*Power(t2,4)) + 
               Power(s,3)*(10 - 17*t2 + 129*Power(t2,2) + 
                  30*Power(t2,3) - 13*Power(t2,4)) + 
               Power(s2,3)*(10 + 72*t2 + 349*Power(t2,2) - 
                  663*Power(t2,3) + 13*Power(t2,4)) + 
               Power(s,2)*(-135 + (578 - 712*s2)*t2 + 
                  (1744 - 57*s2)*Power(t2,2) + 
                  (2241 - 489*s2)*Power(t2,3) + 
                  3*(-36 + 13*s2)*Power(t2,4)) + 
               s2*(-309 - 1537*t2 + 886*Power(t2,2) + 
                  1874*Power(t2,3) - 827*Power(t2,4) + 52*Power(t2,5)) + 
               s*(279 + 31*t2 + 65*Power(t2,2) - 1893*Power(t2,3) + 
                  757*Power(t2,4) - 52*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (67 + 409*t2 + 1122*Power(t2,2) - 39*Power(t2,3)) + 
                  s2*(32 - 765*t2 - 1071*Power(t2,2) - 
                     1011*Power(t2,3) + 307*Power(t2,4)))) + 
            Power(t1,3)*(92 + 1009*t2 - 1374*Power(t2,2) - 
               13236*Power(t2,3) - 3329*Power(t2,4) - 382*Power(t2,5) + 
               Power(s,3)*t2*(87 + 445*t2 - 279*Power(t2,2) - 
                  234*Power(t2,3)) + 
               2*Power(s2,3)*t2*
                (47 + 26*t2 - 636*Power(t2,2) + 220*Power(t2,3)) + 
               Power(s2,2)*(-48 - 750*t2 - 2625*Power(t2,2) + 
                  2706*Power(t2,3) + 764*Power(t2,4) + 36*Power(t2,5)) + 
               Power(s,2)*(6 - 3*(107 + 20*s2)*t2 + 
                  (1655 - 2298*s2)*Power(t2,2) + 
                  (1499 + 664*s2)*Power(t2,3) + 
                  (56 + 908*s2)*Power(t2,4) + 36*Power(t2,5)) + 
               s2*(-40 - 357*t2 + 4870*Power(t2,2) + 7609*Power(t2,3) + 
                  208*Power(t2,4) + 654*Power(t2,5)) - 
               s*(13 - 46*t2 + 3094*Power(t2,2) - 3234*Power(t2,3) - 
                  309*Power(t2,4) + 616*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (128 + 693*t2 - 300*Power(t2,2) + 1114*Power(t2,3)) + 
                  s2*(-24 - 684*t2 + 709*Power(t2,2) + 8169*Power(t2,3) + 
                     1216*Power(t2,4) + 72*Power(t2,5))))))*
       T2q(t2,1 - s1 - t1 + t2))/
     ((-1 + s2)*(-s + s2 - t1)*
       Power(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2,2)*
       Power(-1 + s1 + t1 - t2,2)*Power(s1*t1 - t2,3)*(-1 + t2)) - 
    (8*(Power(s1,10)*(Power(s2,3) + s2*Power(t1,2) + 2*Power(t1,3)) + 
         Power(s1,9)*(Power(s2,3)*(-1 + 3*t1 - 7*t2) + 
            Power(s2,2)*(-3 + 2*Power(t1,2) + t1*(2 + s - t2) + 
               3*s*t2) - s2*t1*
             (-5*Power(t1,2) + s*(3 + t1 - 3*t2) + 2*t2 + 6*t1*(1 + t2)) \
+ Power(t1,2)*(-1 + 6*Power(t1,2) - 6*t2 + s*(-3 - 4*t1 + 4*t2) - 
               t1*(11 + 10*t2))) + 
         Power(t2,3)*(Power(t1,5)*(-1 + t2) + 
            Power(t1,4)*(4 + 5*t2 + (14 - 8*s2)*Power(t2,2)) + 
            Power(t1,3)*(-6 - 3*(1 + 3*s2)*t2 + 
               (-67 + 42*s2 - 5*Power(s2,2))*Power(t2,2) + 
               (-24 + 8*s2 + 3*Power(s2,2))*Power(t2,3)) + 
            Power(t1,2)*(4 + (-31 + 27*s2)*t2 + 
               (72 - 50*s2 + 15*Power(s2,2))*Power(t2,2) + 
               (62 - 33*s2 - 6*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) + (13 - 4*Power(s2,2))*Power(t2,4)) + 
            t1*(-1 + (46 - 27*s2)*t2 + 
               (1 + 6*s2 - 15*Power(s2,2))*Power(t2,2) - 
               (35 - 29*s2 + 10*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) + 
               (-30 + 25*s2 + 2*Power(s2,2) - 3*Power(s2,3))*
                Power(t2,4) + (-5 - s2 + Power(s2,2))*Power(t2,5)) + 
            Power(s,3)*(-4 + Power(t1,5) - 22*t2 - 24*Power(t2,2) - 
               Power(t2,3) + 2*Power(t2,4) - Power(t2,5) - 
               Power(t1,4)*(3 + 2*t2) + 
               Power(t1,3)*(2 + 3*t2 + Power(t2,2)) - 
               Power(t1,2)*(2 + 9*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               t1*(6 + 30*t2 + 8*Power(t2,2) - 7*Power(t2,3) + 
                  2*Power(t2,4))) + 
            t2*(-18 - 20*t2 - 3*Power(t2,2) + 27*Power(t2,3) + 
               13*Power(t2,4) + 2*Power(s2,3)*Power(t2,4) + 
               Power(t2,5) + 
               Power(s2,2)*t2*
                (5 + 13*t2 + 21*Power(t2,2) + Power(t2,3)) + 
               s2*(9 + 10*t2 - 4*Power(t2,2) - 43*Power(t2,3) - 
                  13*Power(t2,4) + Power(t2,5))) + 
            Power(s,2)*(2 + Power(t1,5)*(-3 + t2) + 2*(9 + s2)*t2 + 
               (22 - 18*s2)*Power(t2,2) - 5*(3 + 4*s2)*Power(t2,3) - 
               2*(5 + s2)*Power(t2,4) + (-1 + 4*s2)*Power(t2,5) + 
               4*Power(t1,4)*(3 + s2*t2 - Power(t2,2)) + 
               Power(t1,3)*(-19 - 2*(11 + 6*s2)*t2 - 
                  7*(-2 + s2)*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(16 + 3*(21 + 4*s2)*t2 + 9*Power(t2,2) + 
                  (-17 + 6*s2)*Power(t2,3) - 4*Power(t2,4)) + 
               t1*(-8 - 6*(10 + s2)*t2 + (-41 + 24*s2)*Power(t2,2) + 
                  (11 + 14*s2)*Power(t2,3) - 7*(-1 + s2)*Power(t2,4) + 
                  Power(t2,5))) - 
            s*(-4 + (-37 + 16*s2)*t2 + 
               (-47 + 21*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (-53 + 52*s2 - 9*Power(s2,2))*Power(t2,3) + 
               (-22 + 23*s2)*Power(t2,4) + 
               (-6 + 5*Power(s2,2))*Power(t2,5) + Power(t2,6) + 
               Power(t1,5)*(-3 + 2*t2) + 
               Power(t1,4)*(17 + (-9 + 8*s2)*t2 - 
                  4*(1 + s2)*Power(t2,2)) + 
               Power(t1,3)*(-29 - (32 + 23*s2)*t2 + 
                  (16 + 14*s2 - 5*Power(s2,2))*Power(t2,2) + 
                  2*(1 + 5*s2)*Power(t2,3)) + 
               Power(t1,2)*(15 + (46 + 38*s2)*t2 + 
                  (62 - 31*s2 + 13*Power(s2,2))*Power(t2,2) + 
                  (-24 - 25*s2 + 8*Power(s2,2))*Power(t2,3) + 
                  (1 - 8*s2)*Power(t2,4)) + 
               t1*(4 + (30 - 39*s2)*t2 - 
                  (27 + 10*Power(s2,2))*Power(t2,2) - 
                  3*(-3 + 2*s2 + Power(s2,2))*Power(t2,3) + 
                  (26 + 3*s2 - 8*Power(s2,2))*Power(t2,4) + 
                  2*(-1 + s2)*Power(t2,5)))) + 
         s1*Power(t2,2)*(-6 + 
            (9 + 8*Power(s,3) - 25*s2 - 2*Power(s,2)*(6 + s2) + 
               s*(-33 + 28*s2))*t2 - Power(-1 + s,2)*Power(t1,6)*t2 + 
            (37 + 28*Power(s,3) - 26*s2 - 23*Power(s2,2) + 
               Power(s,2)*(-68 + 42*s2) + 
               2*s*(-19 + 12*s2 + 7*Power(s2,2)))*Power(t2,2) + 
            (43 + 5*Power(s,3) + 11*s2 - 43*Power(s2,2) - 
               3*Power(s2,3) + Power(s,2)*(-9 + 77*s2) + 
               s*(-107 + 120*s2 - 37*Power(s2,2)))*Power(t2,3) + 
            (-70 - 4*Power(s,3) + 179*s2 - 81*Power(s2,2) + 
               Power(s2,3) + Power(s,2)*(40 + 23*s2) + 
               s*(-113 + 54*s2 - 24*Power(s2,2)))*Power(t2,4) + 
            (-66 + 3*Power(s,3) + Power(s,2)*(23 - 10*s2) + 69*s2 + 
               6*Power(s2,2) - 8*Power(s2,3) + 
               s*(-47 - 43*s2 + 15*Power(s2,2)))*Power(t2,5) + 
            (-11 - Power(s,3) - 8*s2 + 5*Power(s2,2) + Power(s2,3) + 
               3*Power(s,2)*(2 + s2) + s*(2 - 11*s2 - 3*Power(s2,2)))*
             Power(t2,6) + Power(t1,5)*
             (9 - 3*Power(s,3) + (-37 + 11*s2)*t2 + 
               2*(-19 + 9*s2)*Power(t2,2) + 
               Power(s,2)*(9 - s2*t2 + 6*Power(t2,2)) + 
               s*(-3 + t2 + 2*s2*t2 - 2*(4 + 3*s2)*Power(t2,2))) + 
            Power(t1,4)*(-42 + (59 - 33*s2)*t2 + 
               (175 - 68*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (62 - 24*s2 - 5*Power(s2,2))*Power(t2,3) + 
               Power(s,3)*(9 + 10*t2 - 2*Power(t2,2)) - 
               Power(s,2)*(24 + 3*(-9 + 4*s2)*t2 + 
                  (2 - 4*s2)*Power(t2,2) + 12*Power(t2,3)) + 
               s*(39 + (-144 + 43*s2)*t2 + 
                  (-6 + 17*s2 - 2*Power(s2,2))*Power(t2,2) + 
                  2*(7 + 9*s2)*Power(t2,3))) + 
            Power(t1,3)*(78 + (46 + 58*s2)*t2 + 
               (-109 - 18*s2 + 17*Power(s2,2))*Power(t2,2) - 
               (101 - 68*s2 + 11*Power(s2,2) + Power(s2,3))*
                Power(t2,3) + 
               (-31 + 8*s2 + 8*Power(s2,2))*Power(t2,4) + 
               Power(s,3)*t2*(11 - 9*t2 + 7*Power(t2,2)) + 
               Power(s,2)*(15 + (-124 + 43*s2)*t2 + 
                  (-88 + 47*s2)*Power(t2,2) - 
                  2*(1 + 8*s2)*Power(t2,3) + 10*Power(t2,4)) - 
               s*(99 + (-103 + 101*s2)*t2 + 
                  2*(-158 + 35*s2 + 9*Power(s2,2))*Power(t2,2) + 
                  (62 - 11*s2 - 10*Power(s2,2))*Power(t2,3) + 
                  (11 + 18*s2)*Power(t2,4))) + 
            Power(t1,2)*(-72 - (107 + 86*s2)*t2 + 
               (-118 + 142*s2 - 63*Power(s2,2))*Power(t2,2) + 
               (-43 + 25*s2 + 72*Power(s2,2) - 13*Power(s2,3))*
                Power(t2,3) + 
               (14 - 85*s2 + 28*Power(s2,2) + 6*Power(s2,3))*
                Power(t2,4) - (-10 + s2 + 3*Power(s2,2))*Power(t2,5) - 
               3*Power(s,3)*(6 + 29*t2 + 15*Power(t2,2) - 
                  Power(t2,3) + 3*Power(t2,4)) + 
               Power(s,2)*(6 - 20*(-7 + 2*s2)*t2 + 
                  (150 - 85*s2)*Power(t2,2) + 
                  (64 - 26*s2)*Power(t2,3) + 
                  (16 + 27*s2)*Power(t2,4) - 3*Power(t2,5)) + 
               s*(93 + (179 + 93*s2)*t2 + 
                  (25 - 43*s2 + 61*Power(s2,2))*Power(t2,2) + 
                  (-113 - 144*s2 + 30*Power(s2,2))*Power(t2,3) + 
                  (87 - 53*s2 - 24*Power(s2,2))*Power(t2,4) + 
                  (2 + 6*s2)*Power(t2,5))) + 
            t1*(33 + (31 + 75*s2)*t2 + 
               (53 - 48*s2 + 67*Power(s2,2))*Power(t2,2) + 
               (39 - 80*s2 - 13*Power(s2,2) + 17*Power(s2,3))*
                Power(t2,3) + 
               (17 + 24*s2 - 88*Power(s2,2) + 14*Power(s2,3))*
                Power(t2,4) + 
               (13 + 34*s2 - 11*Power(s2,2) - 7*Power(s2,3))*
                Power(t2,5) - (2 + s2)*Power(t2,6) + 
               Power(s,3)*(12 + 58*t2 + 70*Power(t2,2) + 
                  29*Power(t2,3) - 4*Power(t2,4) + 5*Power(t2,5)) - 
               Power(s,2)*(6 - 6*(-5 + 2*s2)*t2 + 
                  (-2 + 3*s2)*Power(t2,2) + (2 + 29*s2)*Power(t2,3) - 
                  (-35 + s2)*Power(t2,4) + (18 + 17*s2)*Power(t2,5)) + 
               s*(-30 - (108 + 65*s2)*t2 + 
                  (-289 + 78*s2 - 55*Power(s2,2))*Power(t2,2) + 
                  (-128 + 181*s2 - 27*Power(s2,2))*Power(t2,3) + 
                  (96 + 122*s2 - 7*Power(s2,2))*Power(t2,4) + 
                  (-22 + 34*s2 + 19*Power(s2,2))*Power(t2,5) + 
                  Power(t2,6)))) + 
         Power(s1,8)*(Power(s2,3)*
             (-3 + t1 + 3*Power(t1,2) + 5*t2 - 20*t1*t2 + 
               21*Power(t2,2)) + 
            Power(s2,2)*(3 + 6*Power(t1,3) + 
               Power(t1,2)*(-2 + 3*s - 14*t2) + (19 - 7*s)*t2 + 
               (1 - 18*s)*Power(t2,2) + 
               t1*(-13 - 17*t2 + 6*Power(t2,2) + s*(-5 + 6*t2))) + 
            t1*(6*Power(t1,4) + 2*t2*(1 + 3*t2) - 
               5*Power(t1,3)*(7 + 5*t2) + 
               Power(s,2)*(t1 + 2*Power(t1,2) - 5*t1*t2 + 
                  3*(-1 + t2)*t2) + 
               2*Power(t1,2)*(5 + 16*t2 + 10*Power(t2,2)) + 
               t1*(6 + 39*t2 + 30*Power(t2,2)) + 
               s*(3 - 12*Power(t1,3) + 3*t2 - 8*Power(t2,2) + 
                  Power(t1,2)*(-9 + 32*t2) + 
                  t1*(6 + 14*t2 - 19*Power(t2,2)))) + 
            s2*(3 + 9*Power(t1,4) + Power(t2,2) + 
               3*Power(s,2)*Power(t2,2) - 3*s*t2*(1 + t2) - 
               Power(t1,3)*(14 + 5*s + 27*t2) + 
               Power(t1,2)*(2 + 22*t2 + 15*Power(t2,2) + 
                  4*s*(-1 + 5*t2)) + 
               t1*(-(Power(s,2)*t2) + s*(4 + 15*t2 - 17*Power(t2,2)) + 
                  2*(-2 + 7*t2 + 6*Power(t2,2))))) + 
         Power(s1,7)*(-1 + 2*Power(t1,6) - Power(t2,2) - 2*Power(t2,3) + 
            4*s*Power(t2,3) - 3*Power(s,2)*Power(t2,3) + 
            Power(s,3)*Power(t2,3) - Power(t1,5)*(39 + 12*s + 23*t2) + 
            Power(t1,4)*(27 + 6*Power(s,2) + 115*t2 + 40*Power(t2,2) + 
               s*(-1 + 59*t2)) + 
            Power(t1,3)*(3 + 71*t2 - 16*Power(t2,2) - 20*Power(t2,3) - 
               5*Power(s,2)*(-2 + 5*t2) + 
               s*(31 + 35*t2 - 88*Power(t2,2))) + 
            Power(s2,3)*(1 + Power(t1,3) + Power(t1,2)*(4 - 19*t2) + 
               21*t2 - 8*Power(t2,2) - 35*Power(t2,3) + 
               t1*(-3 - 11*t2 + 57*Power(t2,2))) + 
            Power(s2,2)*(9 + 6*Power(t1,4) + 
               Power(t1,3)*(-10 + 3*s - 34*t2) - (11 + s)*t2 + 
               (-48 + 33*s)*Power(t2,2) + (-6 + 45*s)*Power(t2,3) + 
               Power(t1,2)*(-14 + s*(-9 + t2) + 40*Power(t2,2)) - 
               t1*(10 + s - 85*t2 - 16*s*t2 - 64*Power(t2,2) + 
                  51*s*Power(t2,2) + 15*Power(t2,3))) + 
            Power(t1,2)*(-4 - 67*t2 + Power(s,3)*t2 - 147*Power(t2,2) - 
               60*Power(t2,3) + 
               2*Power(s,2)*(3 - 7*t2 + 16*Power(t2,2)) + 
               s*(16 - 16*t2 - 63*Power(t2,2) + 36*Power(t2,3))) + 
            t1*(2 - 13*t2 - 45*Power(t2,2) - 2*Power(s,3)*Power(t2,2) - 
               30*Power(t2,3) + 
               Power(s,2)*t2*(8 + 15*t2 - 13*Power(t2,2)) + 
               s*(-5 - 25*t2 + Power(t2,2) + 38*Power(t2,3))) + 
            s2*(-3 + 7*Power(t1,5) + (-17 + 8*s)*t2 + 
               (-8 + 22*s - 8*Power(s,2))*Power(t2,2) + 
               (-6 + 17*s - 15*Power(s,2))*Power(t2,3) - 
               Power(t1,4)*(13 + 9*s + 42*t2) + 
               Power(t1,3)*(8 + 50*t2 + 59*Power(t2,2) + 
                  s*(-6 + 50*t2)) - 
               Power(t1,2)*(-2 - 29*t2 + 16*Power(t2,2) + 
                  20*Power(t2,3) + Power(s,2)*(2 + 4*t2) + 
                  4*s*(1 - 3*t2 + 21*Power(t2,2))) + 
               t1*(17 + 22*t2 - 71*Power(t2,2) - 30*Power(t2,3) + 
                  Power(s,2)*t2*(-6 + 17*t2) + 
                  s*(16 - 55*t2 - 57*Power(t2,2) + 40*Power(t2,3))))) + 
         Power(s1,6)*(1 + 5*t2 - s*t2 + 7*Power(t2,2) + s*Power(t2,2) + 
            17*Power(t2,3) - 11*s*Power(t2,3) + 
            5*Power(s,2)*Power(t2,3) - 2*Power(s,3)*Power(t2,3) + 
            10*Power(t2,4) - 19*s*Power(t2,4) + 
            13*Power(s,2)*Power(t2,4) - 4*Power(s,3)*Power(t2,4) - 
            Power(t1,6)*(17 + 4*s + 12*t2) + 
            Power(t1,5)*(39 + 6*Power(s,2) + 118*t2 + 31*Power(t2,2) + 
               3*s*(7 + 13*t2)) + 
            Power(t1,4)*(-21 + Power(s,2)*(28 - 38*t2) + 41*t2 - 
               119*Power(t2,2) - 30*Power(t2,3) + 
               s*(58 + 2*t2 - 115*Power(t2,2))) - 
            Power(s2,3)*(-2 + 5*t2 + Power(t1,2)*(23 - 50*t2)*t2 + 
               62*Power(t2,2) + Power(t2,3) - 35*Power(t2,4) + 
               Power(t1,3)*(-2 + 6*t2) + 
               t1*(4 - 13*t2 - 47*Power(t2,2) + 90*Power(t2,3))) + 
            Power(t1,3)*(5 - 95*t2 - 340*Power(t2,2) - 36*Power(t2,3) + 
               10*Power(t2,4) + Power(s,3)*(2 + 3*t2) + 
               Power(s,2)*(14 - 45*t2 + 84*Power(t2,2)) + 
               s*(13 - 117*t2 - 88*Power(t2,2) + 113*Power(t2,3))) + 
            t1*(-7 - 5*t2 + 110*Power(t2,2) + 174*Power(t2,3) + 
               11*Power(s,3)*Power(t2,3) + 60*Power(t2,4) + 
               2*Power(s,2)*t2*
                (-3 - 21*t2 - 25*Power(t2,2) + 11*Power(t2,3)) + 
               s*(-11 + 25*t2 + 76*Power(t2,2) - 10*Power(t2,3) - 
                  72*Power(t2,4))) + 
            Power(t1,2)*(2*Power(s,3)*(2 - 5*t2)*t2 + 
               2*t2*(9 + 47*t2 + 109*Power(t2,2) + 30*Power(t2,3)) - 
               Power(s,2)*(12 + 17*t2 - 66*Power(t2,2) + 
                  76*Power(t2,3)) + 
               s*(-11 - 127*t2 + 68*Power(t2,2) + 170*Power(t2,3) - 
                  34*Power(t2,4))) + 
            Power(s2,2)*(-3 + 2*Power(t1,5) + 
               Power(t1,4)*(-8 + s - 31*t2) + (-56 + 13*s)*t2 + 
               (1 + 17*s)*Power(t2,2) + (55 - 57*s)*Power(t2,3) + 
               (15 - 60*s)*Power(t2,4) + 
               Power(t1,2)*(-8 + s + 98*t2 + 38*s*t2 + 
                  39*Power(t2,2) - 48*s*Power(t2,2) - 60*Power(t2,3)) + 
               Power(t1,3)*(4 + 32*t2 + 78*Power(t2,2) - 
                  s*(3 + 5*t2)) + 
               t1*(13 + 90*t2 - 230*Power(t2,2) - 135*Power(t2,3) + 
                  20*Power(t2,4) + 
                  s*(5 + 4*t2 - 28*Power(t2,2) + 130*Power(t2,3)))) + 
            s2*(-9 + 2*Power(t1,6) + (7 - 4*s)*t2 + 
               (39 - 19*s + 6*Power(s,2))*Power(t2,2) + 
               (44 - 53*s + 31*Power(s,2))*Power(t2,3) + 
               5*(3 - 8*s + 6*Power(s,2))*Power(t2,4) - 
               Power(t1,5)*(11 + 7*s + 29*t2) + 
               Power(t1,4)*(17 + 51*t2 + 75*Power(t2,2) + 
                  4*s*(-5 + 14*t2)) - 
               Power(t1,3)*(23 - 2*t2 + 43*Power(t2,2) + 
                  66*Power(t2,3) + Power(s,2)*(8 + 5*t2) + 
                  s*(15 - 23*t2 + 156*Power(t2,2))) + 
               Power(t1,2)*(7 - 51*t2 - 165*Power(t2,2) - 
                  37*Power(t2,3) + 15*Power(t2,4) + 
                  Power(s,2)*(2 - 9*t2 + 35*Power(t2,2)) + 
                  3*s*(7 - 5*t2 - 22*Power(t2,2) + 52*Power(t2,3))) + 
               t1*(17 - 106*t2 - 88*Power(t2,2) + 143*Power(t2,3) + 
                  40*Power(t2,4) + 
                  2*Power(s,2)*t2*(9 + 8*t2 - 32*Power(t2,2)) + 
                  s*(-10 - 60*t2 + 254*Power(t2,2) + 163*Power(t2,3) - 
                     50*Power(t2,4))))) + 
         Power(s1,5)*(3 - t2 + 5*s*t2 - 12*Power(t2,2) - 
            2*s*Power(t2,2) - 53*Power(t2,3) - 6*s*Power(t2,3) + 
            6*Power(s,2)*Power(t2,3) + 2*Power(s,3)*Power(t2,3) - 
            65*Power(t2,4) + 41*s*Power(t2,4) - 
            12*Power(s,2)*Power(t2,4) + 6*Power(s,3)*Power(t2,4) - 
            20*Power(t2,5) + 36*s*Power(t2,5) - 
            22*Power(s,2)*Power(t2,5) + 6*Power(s,3)*Power(t2,5) - 
            2*Power(t1,7)*(1 + 2*t2) + 
            Power(t1,6)*(2*Power(s,2) + 2*s*(11 + 6*t2) + 
               7*(6 + 7*t2 + 2*Power(t2,2))) - 
            Power(t1,5)*(67 + 38*t2 + 92*Power(t2,2) + 17*Power(t2,3) + 
               2*Power(s,2)*(-13 + 9*t2) + 
               s*(-52 + 43*t2 + 59*Power(t2,2))) + 
            Power(s2,3)*t2*(-11 + 11*t2 + 98*Power(t2,2) + 
               20*Power(t2,3) - 21*Power(t2,4) + 
               Power(t1,3)*(-9 + 14*t2) + 
               Power(t1,2)*(-5 + 58*t2 - 70*Power(t2,2)) + 
               5*t1*(5 - 4*t2 - 21*Power(t2,2) + 17*Power(t2,3))) + 
            Power(t1,4)*(13 - 84*t2 - 294*Power(t2,2) + 
               27*Power(t2,3) + 10*Power(t2,4) + 
               Power(s,3)*(2 + 4*t2) + 
               Power(s,2)*(12 - 61*t2 + 82*Power(t2,2)) + 
               s*(-44 - 205*t2 + 7*Power(t2,2) + 107*Power(t2,3))) + 
            Power(t1,3)*(15 + 43*t2 + Power(s,3)*(12 - 13*t2)*t2 + 
               139*Power(t2,2) + 476*Power(t2,3) + 49*Power(t2,4) - 
               2*Power(t2,5) - 
               Power(s,2)*(38 + 90*t2 - 113*Power(t2,2) + 
                  126*Power(t2,3)) + 
               s*(-16 - 267*t2 + 251*Power(t2,2) + 170*Power(t2,3) - 
                  71*Power(t2,4))) - 
            t1*(8 - 41*t2 + 9*Power(t2,2) + 276*Power(t2,3) + 
               288*Power(t2,4) + 60*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(-2 - 2*t2 + 23*Power(t2,2)) + 
               2*Power(s,2)*t2*
                (4 - 10*t2 - 50*Power(t2,2) - 53*Power(t2,3) + 
                  9*Power(t2,4)) + 
               s*(-11 - 68*t2 + 51*Power(t2,2) + 188*Power(t2,3) + 
                  26*Power(t2,4) - 68*Power(t2,5))) - 
            Power(s2,2)*(6 + (-11 + 2*s)*t2 + 
               (-134 + 73*s)*Power(t2,2) + 7*(-8 + 9*s)*Power(t2,3) - 
               5*(-2 + 7*s)*Power(t2,4) - 5*(-4 + 9*s)*Power(t2,5) + 
               2*Power(t1,5)*(1 + 5*t2) - 
               Power(t1,4)*(7 + s + 25*t2 - 3*s*t2 + 61*Power(t2,2)) + 
               Power(t1,3)*(3 + s - 13*t2 - 20*s*t2 + 8*Power(t2,2) + 
                  13*s*Power(t2,2) + 92*Power(t2,3)) + 
               Power(t1,2)*(13 + (-28 + 6*s)*t2 + 
                  (258 + 77*s)*Power(t2,2) + 
                  (119 - 122*s)*Power(t2,3) - 50*Power(t2,4)) + 
               t1*(-17 + (67 + 33*s)*t2 + (328 - 10*s)*Power(t2,2) - 
                  (323 + 71*s)*Power(t2,3) + 
                  5*(-34 + 33*s)*Power(t2,4) + 15*Power(t2,5))) + 
            Power(t1,2)*(4 - 6*t2 + 44*Power(t2,2) + 130*Power(t2,3) - 
               147*Power(t2,4) - 30*Power(t2,5) + 
               2*Power(s,3)*t2*(-8 - 9*t2 + 15*Power(t2,2)) + 
               2*Power(s,2)*(-1 + 6*t2 - 4*Power(t2,2) - 
                  80*Power(t2,3) + 43*Power(t2,4)) + 
               s*(-25 + 34*t2 + 319*Power(t2,2) - 176*Power(t2,3) - 
                  228*Power(t2,4) + 16*Power(t2,5))) - 
            s2*(-3 + 7*(-7 + 2*s)*t2 - 
               2*(10 - 4*s + Power(s,2))*Power(t2,2) + 
               (32 + 45*s + 15*Power(s,2))*Power(t2,3) + 
               (100 - 41*s + 41*Power(s,2))*Power(t2,4) + 
               10*(2 - 5*s + 3*Power(s,2))*Power(t2,5) + 
               2*Power(t1,6)*(5 + s + 6*t2) - 
               Power(t1,5)*(21 + 41*t2 + 43*Power(t2,2) + 
                  s*(-21 + 23*t2)) + 
               Power(t1,4)*(23 + 22*t2 + 51*Power(t2,2) + 
                  63*Power(t2,3) + Power(s,2)*(7 + 4*t2) + 
                  s*(-1 - 59*t2 + 126*Power(t2,2))) - 
               Power(t1,3)*(12 + 22*t2 - 142*Power(t2,2) - 
                  36*Power(t2,3) + 39*Power(t2,4) + 
                  Power(s,2)*(8 - t2 + 29*Power(t2,2)) + 
                  2*s*(10 + 52*t2 - 44*Power(t2,2) + 111*Power(t2,3))) + 
               Power(t1,2)*(-14 - 45*t2 - 177*Power(t2,2) - 
                  311*Power(t2,3) - 78*Power(t2,4) + 6*Power(t2,5) + 
                  Power(s,2)*t2*(-39 - 52*t2 + 97*Power(t2,2)) + 
                  s*(-15 + 63*t2 - 145*Power(t2,2) - 236*Power(t2,3) + 
                     149*Power(t2,4))) + 
               t1*(17 + 123*t2 - 331*Power(t2,2) - 254*Power(t2,3) + 
                  143*Power(t2,4) + 30*Power(t2,5) + 
                  Power(s,2)*t2*
                   (4 + 64*t2 + 27*Power(t2,2) - 106*Power(t2,3)) + 
                  s*(13 - 77*t2 - 140*Power(t2,2) + 526*Power(t2,3) + 
                     277*Power(t2,4) - 35*Power(t2,5))))) + 
         Power(s1,2)*t2*(6 + (3 + 33*s2 - 3*s*(-5 + 6*s2))*t2 + 
            (-22 + Power(s,2)*(42 - 28*s2) + 25*s2 + 47*Power(s2,2) - 
               4*s*(4 - 3*s2 + 7*Power(s2,2)))*Power(t2,2) + 
            (-71 + 3*Power(s,3) + Power(s,2)*(49 - 101*s2) - 33*s2 + 
               51*Power(s2,2) + 14*Power(s2,3) + 
               s*(32 - 54*s2 + 57*Power(s2,2)))*Power(t2,3) + 
            (73 + 4*Power(s,3) - 300*s2 + 97*Power(s2,2) - 
               5*Power(s2,3) - Power(s,2)*(60 + 59*s2) + 
               2*s*(93 - 3*s2 + 53*Power(s2,2)))*Power(t2,4) + 
            (154 - 5*Power(s,3) - 142*s2 - 47*Power(s2,2) + 
               4*Power(s2,3) + Power(s,2)*(-67 + 3*s2) + 
               s*(104 + 183*s2 + 3*Power(s2,2)))*Power(t2,5) + 
            (47 + 3*Power(s,3) + 34*s2 - 27*Power(s2,2) - 
               7*Power(s2,3) - Power(s,2)*(21 + 13*s2) + 
               s*(-8 + 49*s2 + 17*Power(s2,2)))*Power(t2,6) + 
            (2 + Power(s,2) + s2 + Power(s2,2) - s*(3 + 2*s2))*
             Power(t2,7) + Power(t1,6)*
             (-6 + (45 - 12*s2)*t2 + Power(s,2)*(3 - 2*t2)*t2 + 
               (34 - 14*s2)*Power(t2,2) + 
               2*s*(-3 - 6*t2 + (3 + s2)*Power(t2,2))) + 
            Power(t1,5)*(21 + 3*Power(s,3) + 3*(-9 + 5*s2)*t2 + 
               (-141 + 35*s2)*Power(t2,2) + 
               (-51 + 24*s2 + 2*Power(s2,2))*Power(t2,3) + 
               Power(s,2)*(-21 + 3*(-12 + s2)*t2 - 10*Power(t2,2) + 
                  6*Power(t2,3)) + 
               s*(15 - 3*(-53 + 8*s2)*t2 - 3*(-10 + s2)*Power(t2,2) - 
                  4*(5 + 2*s2)*Power(t2,3))) - 
            Power(t1,4)*(18 + (177 - 39*s2)*t2 + 
               (82 - 85*s2 + 3*Power(s2,2))*Power(t2,2) + 
               (9 + 27*s2 - 24*Power(s2,2))*Power(t2,3) + 
               (-19 + 14*s2 + 4*Power(s2,2))*Power(t2,4) + 
               Power(s,3)*(15 + 42*t2 - 8*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s,2)*(-66 - 3*(65 + 2*s2)*t2 + 
                  (-54 + 25*s2)*Power(t2,2) - 
                  (33 + 4*s2)*Power(t2,3) + 6*Power(t2,4)) + 
               s*(27 + 3*(8 + 13*s2)*t2 + 
                  (322 - 50*s2 - 7*Power(s2,2))*Power(t2,2) + 
                  (-23 + 49*s2 + 2*Power(s2,2))*Power(t2,3) - 
                  (23 + 10*s2)*Power(t2,4))) + 
            Power(t1,3)*(-18 - 120*(-2 + s2)*t2 + 
               (324 - 70*s2 - 38*Power(s2,2))*Power(t2,2) + 
               (169 - 125*s2 - 30*Power(s2,2) + 6*Power(s2,3))*
                Power(t2,3) + 
               (101 + 44*s2 - 52*Power(s2,2) - 2*Power(s2,3))*
                Power(t2,4) + (-2 + 5*s2 + 2*Power(s2,2))*Power(t2,5) + 
               3*Power(s,3)*(8 + 37*t2 + 19*Power(t2,2) - 
                  9*Power(t2,3) + 2*Power(t2,4)) + 
               Power(s,2)*(-69 - 9*(24 + 5*s2)*t2 - 
                  (64 + 37*s2)*Power(t2,2) + 
                  (-10 + 63*s2)*Power(t2,3) - 
                  2*(31 + 7*s2)*Power(t2,4) + 2*Power(t2,5)) + 
               s*(51 + 3*(-91 + 59*s2)*t2 + 
                  (-536 + 190*s2 + 17*Power(s2,2))*Power(t2,2) + 
                  (290 + 12*s2 - 33*Power(s2,2))*Power(t2,3) + 
                  (-33 + 117*s2 + 10*Power(s2,2))*Power(t2,4) - 
                  2*(5 + 2*s2)*Power(t2,5))) - 
            Power(t1,2)*(-42 + (63 - 156*s2)*t2 + 
               (30 + 160*s2 - 132*Power(s2,2))*Power(t2,2) - 
               (77 + 7*s2 - 164*Power(s2,2) + 32*Power(s2,3))*
                Power(t2,3) + 
               (24 - 239*s2 + 12*Power(s2,2) + 31*Power(s2,3))*
                Power(t2,4) + 
               (80 + 7*s2 - 31*Power(s2,2) - 4*Power(s2,3))*
                Power(t2,5) + s2*Power(t2,6) + 
               Power(s,3)*(12 + 45*t2 + 39*Power(t2,2) + 
                  5*Power(t2,3) - 33*Power(t2,4) + 6*Power(t2,5)) - 
               Power(s,2)*(24 + 6*(1 + 8*s2)*t2 + 
                  (-212 + 165*s2)*Power(t2,2) + 
                  27*(-6 + s2)*Power(t2,3) - (7 + 94*s2)*Power(t2,4) + 
                  (51 + 16*s2)*Power(t2,5)) + 
               s*(51 + 3*(-19 + 53*s2)*t2 + 
                  3*(-194 + 52*s2 + 31*Power(s2,2))*Power(t2,2) + 
                  (-398 - 186*s2 + 51*Power(s2,2))*Power(t2,3) + 
                  (389 - 23*s2 - 89*Power(s2,2))*Power(t2,4) + 
                  2*(11 + 44*s2 + 7*Power(s2,2))*Power(t2,5) - 
                  Power(t2,6))) + 
            t1*(-27 - 3*(7 + 37*s2)*t2 + 
               (-83 + 99*s2 - 138*Power(s2,2))*Power(t2,2) + 
               (-115 + 154*s2 + 117*Power(s2,2) - 52*Power(s2,3))*
                Power(t2,3) + 
               (41 - 347*s2 + 370*Power(s2,2) - 25*Power(s2,3))*
                Power(t2,4) + 
               (5 - 178*s2 + 14*Power(s2,2) + 41*Power(s2,3))*
                Power(t2,5) + 
               (22 + 3*s2 - 9*Power(s2,2) - 2*Power(s2,3))*Power(t2,6) + 
               Power(s,3)*t2*
                (-24 - 94*t2 - 53*Power(t2,2) - 8*Power(t2,3) - 
                  17*Power(t2,4) + 2*Power(t2,5)) + 
               s*(18 + (78 + 63*s2)*t2 + 
                  (256 - 95*s2 + 97*Power(s2,2))*Power(t2,2) + 
                  (267 - 552*s2 + 89*Power(s2,2))*Power(t2,3) - 
                  (36 + 396*s2 + 43*Power(s2,2))*Power(t2,4) - 
                  6*(-25 + 6*s2 + 15*Power(s2,2))*Power(t2,5) + 
                  (17 + 25*s2 + 6*Power(s2,2))*Power(t2,6)) + 
               Power(s,2)*t2*(48 + 192*t2 + 114*Power(t2,2) + 
                  63*Power(t2,3) + 20*Power(t2,4) - 16*Power(t2,5) - 
                  s2*(12 + 85*t2 + 23*Power(t2,2) - 49*Power(t2,3) - 
                     66*Power(t2,4) + 6*Power(t2,5))))) - 
         Power(s1,3)*(2 + (-1 + s*(5 - 6*s2) + 23*s2)*t2 + 
            (-22 + Power(s,2)*(2 - 4*s2) + 5*s2 + 51*Power(s2,2) + 
               s*(-2 + 22*s2 - 22*Power(s2,2)))*Power(t2,2) + 
            (-46 + 5*Power(s,3) + Power(s,2)*(29 - 51*s2) - 80*s2 + 
               20*Power(s2,2) + 26*Power(s2,3) + 
               s*(-36 + 58*s2 + 41*Power(s2,2)))*Power(t2,3) + 
            (45 + 2*Power(s,3) - 260*s2 - 14*Power(s2,2) - 
               11*Power(s2,3) - Power(s,2)*(40 + 59*s2) + 
               s*(131 + 74*s2 + 187*Power(s2,2)))*Power(t2,4) - 
            (-201 + 7*Power(s,3) + 136*s2 + 109*Power(s2,2) + 
               33*Power(s2,3) + 5*Power(s,2)*(15 + s2) - 
               s*(95 + 296*s2 + 63*Power(s2,2)))*Power(t2,5) + 
            (99 + Power(s,3) + 84*s2 - 56*Power(s2,2) - 
               20*Power(s2,3) - 2*Power(s,2)*(11 + 7*s2) + 
               s*(-33 + 76*s2 + 33*Power(s2,2)))*Power(t2,6) + 
            (10 - Power(s,3) + 6*s2 + 6*Power(s2,2) + Power(s2,3) + 
               Power(s,2)*(7 + 3*s2) - s*(16 + 13*s2 + 3*Power(s2,2)))*
             Power(t2,7) - 2*Power(t1,7)*
             (1 + (-7 + 2*s2)*t2 + (-5 + 2*s2)*Power(t2,2) + 
               s*(1 + t2 - Power(t2,2))) + 
            Power(t1,6)*(8 + (41 - 4*s2)*t2 + 
               2*(-14 + s2)*Power(t2,2) + (-11 + 8*s2)*Power(t2,3) - 
               Power(s,2)*(4 + 7*t2 + 4*Power(t2,2)) + 
               s*(2 + (50 - 6*s2)*t2 + 18*Power(t2,2) - 8*Power(t2,3))) \
+ Power(t1,5)*(-13 + (-179 + 27*s2)*t2 + 
               (-173 + 67*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (-82 + 29*s2 + 10*Power(s2,2))*Power(t2,3) - 
               3*(1 + 2*s2)*Power(t2,4) - Power(s,3)*(1 + 8*t2) + 
               Power(s,2)*(7 + (66 + s2)*t2 - 
                  2*(-7 + s2)*Power(t2,2) + 20*Power(t2,3)) + 
               s*(9 + (77 - 22*s2)*t2 + (-142 + 17*s2)*Power(t2,2) - 
                  (33 + 25*s2)*Power(t2,3) + 11*Power(t2,4))) + 
            Power(t1,4)*(14 - (-133 + s2)*t2 + 
               (154 - 23*s2 - 7*Power(s2,2))*Power(t2,2) + 
               2*(37 - 69*s2 + 12*Power(s2,2))*Power(t2,3) + 
               (109 - 40*s2 - 25*Power(s2,2))*Power(t2,4) + 
               3*(2 + s2)*Power(t2,5) + 
               Power(s,3)*(3 + 16*t2 + 28*Power(t2,2) - 
                  6*Power(t2,3)) - 
               Power(s,2)*(-4 + t2 + 2*s2*t2 + 
                  5*(16 + 5*s2)*Power(t2,2) - 
                  3*(15 + 4*s2)*Power(t2,3) + 36*Power(t2,4)) + 
               s*(-25 + (-214 + 9*s2)*t2 + 
                  (-462 + 156*s2 + 7*Power(s2,2))*Power(t2,2) - 
                  3*(-69 + 21*s2 + Power(s2,2))*Power(t2,3) + 
                  (68 + 61*s2)*Power(t2,4) - 6*Power(t2,5))) + 
            Power(t1,3)*(-16 + (56 - 82*s2)*t2 - 
               2*(-139 + 9*s2 + 21*Power(s2,2))*Power(t2,2) + 
               2*(113 + 5*s2 - 51*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,3) + 
               (231 + 226*s2 - 106*Power(s2,2) - 9*Power(s2,3))*
                Power(t2,4) + 
               (-53 + 38*s2 + 18*Power(s2,2))*Power(t2,5) - 
               (2 + s2)*Power(t2,6) + 
               Power(s,3)*(-2 + 17*t2 + 19*Power(t2,2) - 
                  43*Power(t2,3) + 21*Power(t2,4)) + 
               Power(s,2)*(-13 - (144 + 13*s2)*t2 + 
                  (-142 + 27*s2)*Power(t2,2) + 95*s2*Power(t2,3) - 
                  (147 + 47*s2)*Power(t2,4) + 29*Power(t2,5)) + 
               s*(31 + (41 + 103*s2)*t2 + 
                  (-102 + 54*s2 + Power(s2,2))*Power(t2,2) + 
                  (527 - 57*s2 - 50*Power(s2,2))*Power(t2,3) + 
                  (-269 + 240*s2 + 34*Power(s2,2))*Power(t2,4) - 
                  (83 + 48*s2)*Power(t2,5) + Power(t2,6))) + 
            Power(t1,2)*(16 + (-85 + 126*s2)*t2 + 
               4*(-67 - 36*s2 + 37*Power(s2,2))*Power(t2,2) + 
               (3 - 102*s2 - 148*Power(s2,2) + 38*Power(s2,3))*
                Power(t2,3) - 
               2*(41 - 71*s2 - 74*Power(s2,2) + 34*Power(s2,3))*
                Power(t2,4) + 
               (-271 - 93*s2 + 102*Power(s2,2) + 23*Power(s2,3))*
                Power(t2,5) + (1 - 16*s2 - 4*Power(s2,2))*Power(t2,6) - 
               Power(s,3)*t2*
                (25 + 97*t2 + 19*Power(t2,2) - 43*Power(t2,3) + 
                  25*Power(t2,4)) + 
               Power(s,2)*(6 + (82 + 20*s2)*t2 + 
                  (70 + 71*s2)*Power(t2,2) - 
                  (88 + 43*s2)*Power(t2,3) + 
                  (42 - 135*s2)*Power(t2,4) + 
                  (159 + 71*s2)*Power(t2,5) - 10*Power(t2,6)) + 
               s*(-21 + (19 - 121*s2)*t2 + 
                  (764 - 262*s2 - 55*Power(s2,2))*Power(t2,2) + 
                  (661 + 61*s2 - 40*Power(s2,2))*Power(t2,3) + 
                  2*(-287 - 92*s2 + 67*Power(s2,2))*Power(t2,4) + 
                  (26 - 282*s2 - 69*Power(s2,2))*Power(t2,5) + 
                  2*(19 + 7*s2)*Power(t2,6))) + 
            t1*(-9 + (21 - 85*s2)*t2 + 
               (49 + 115*s2 - 152*Power(s2,2))*Power(t2,2) + 
               (-164 + 273*s2 + 196*Power(s2,2) - 78*Power(s2,3))*
                Power(t2,3) + 
               (50 - 692*s2 + 662*Power(s2,2) - 17*Power(s2,3))*
                Power(t2,4) + 
               (124 - 386*s2 - 67*Power(s2,2) + 101*Power(s2,3))*
                Power(t2,5) + 
               (105 + 17*s2 - 52*Power(s2,2) - 15*Power(s2,3))*
                Power(t2,6) + (6 + 2*s2 + Power(s2,2))*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*
                (-2 - 13*t2 - 16*Power(t2,2) - 21*Power(t2,3) + 
                  11*Power(t2,4)) + 
               s*(6 + (24 + 43*s2)*t2 + 
                  (-76 + 13*s2 + 69*Power(s2,2))*Power(t2,2) + 
                  2*(1 - 297*s2 + 66*Power(s2,2))*Power(t2,3) + 
                  (98 - 506*s2 - 90*Power(s2,2))*Power(t2,4) + 
                  (341 + 189*s2 - 166*Power(s2,2))*Power(t2,5) + 
                  (68 + 127*s2 + 41*Power(s2,2))*Power(t2,6) - 
                  2*(3 + s2)*Power(t2,7)) + 
               Power(s,2)*t2*(4 + 140*t2 + 204*Power(t2,2) + 
                  41*Power(t2,3) - 50*Power(t2,4) - 71*Power(t2,5) + 
                  Power(t2,6) - 
                  s2*(6 + 77*t2 + 73*Power(t2,2) - 101*Power(t2,3) - 
                     94*Power(t2,4) + 37*Power(t2,5))))) + 
         Power(s1,4)*(-1 - 14*t2 + s*t2 - 13*Power(t2,2) - 
            21*s*Power(t2,2) + 2*Power(s,2)*Power(t2,2) + 
            23*Power(t2,3) + 39*s*Power(t2,3) - 
            10*Power(s,2)*Power(t2,3) + 146*Power(t2,4) + 
            37*s*Power(t2,4) - 36*Power(s,2)*Power(t2,4) - 
            6*Power(s,3)*Power(t2,4) + 111*Power(t2,5) - 
            56*s*Power(t2,5) - 5*Power(s,3)*Power(t2,5) + 
            20*Power(t2,6) - 34*s*Power(t2,6) + 
            18*Power(s,2)*Power(t2,6) - 4*Power(s,3)*Power(t2,6) + 
            2*Power(t1,7)*(10 + 3*t2 + Power(t2,2) + s*(3 + 2*t2)) + 
            Power(t1,6)*(-45 + 7*Power(s,2) - 62*t2 - 29*Power(t2,2) - 
               4*Power(t2,3) - 2*s*(-13 + 13*t2 + 9*Power(t2,2))) + 
            Power(s2,3)*Power(t2,2)*
             (24 - 16*Power(t1,3)*(-1 + t2) - 14*t2 - 85*Power(t2,2) - 
               29*Power(t2,3) + 7*Power(t2,4) + 
               Power(t1,2)*(22 - 82*t2 + 55*Power(t2,2)) + 
               t1*(-62 + 7*t2 + 135*Power(t2,2) - 48*Power(t2,3))) + 
            Power(t1,5)*(10 - 53*t2 + 2*Power(s,3)*t2 - 74*Power(t2,2) + 
               10*Power(t2,3) + 3*Power(t2,4) + 
               Power(s,2)*(15 - 22*t2 + 28*Power(t2,2)) + 
               s*(-45 - 162*t2 + 21*Power(t2,2) + 43*Power(t2,3))) + 
            Power(t1,4)*(36 + 244*t2 + 196*Power(t2,2) + 
               316*Power(t2,3) + 18*Power(t2,4) - Power(t2,5) + 
               Power(s,3)*(4 + 16*t2 - 6*Power(t2,2)) - 
               Power(s,2)*(33 + 130*t2 - 47*Power(t2,2) + 
                  80*Power(t2,3)) + 
               s*(-5 - 194*t2 + 287*Power(t2,2) + 37*Power(t2,3) - 
                  45*Power(t2,4))) + 
            Power(t1,3)*(-17 - 95*t2 + 64*Power(t2,2) + 
               114*Power(t2,3) - 268*Power(t2,4) - 20*Power(t2,5) + 
               Power(s,3)*(-4 - 34*t2 - 45*Power(t2,2) + 
                  25*Power(t2,3)) + 
               Power(s,2)*(-2 + 54*t2 + 102*Power(t2,2) - 
                  173*Power(t2,3) + 92*Power(t2,4)) + 
               s*(19 + 260*t2 + 595*Power(t2,2) - 390*Power(t2,3) - 
                  181*Power(t2,4) + 19*Power(t2,5))) + 
            t1*(7 + 44*t2 - 101*Power(t2,2) + 34*Power(t2,3) + 
               290*Power(t2,4) + 242*Power(t2,5) + 30*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*
                (10 - 8*t2 - 11*Power(t2,2) + 23*Power(t2,3)) + 
               Power(s,2)*t2*
                (13 + 99*t2 - 8*Power(t2,2) - 119*Power(t2,3) - 
                  123*Power(t2,4) + 7*Power(t2,5)) + 
               s*(8 - 55*t2 - 173*Power(t2,2) + 95*Power(t2,3) + 
                  345*Power(t2,4) + 82*Power(t2,5) - 32*Power(t2,6))) + 
            Power(t1,2)*(-10 - 70*t2 - 45*Power(t2,2) - 
               143*Power(t2,3) - 364*Power(t2,4) + 39*Power(t2,5) + 
               6*Power(t2,6) + 
               Power(s,3)*t2*
                (-2 + 12*t2 + 33*Power(t2,2) - 40*Power(t2,3)) + 
               Power(s,2)*(13 + 85*t2 + 7*Power(t2,2) + 
                  56*Power(t2,3) + 218*Power(t2,4) - 47*Power(t2,5)) + 
               s*(-9 + 172*t2 + 299*Power(t2,2) - 479*Power(t2,3) + 
                  164*Power(t2,4) + 147*Power(t2,5) - 3*Power(t2,6))) + 
            Power(s2,2)*t2*(28 - 9*t2 - 138*Power(t2,2) - 
               118*Power(t2,3) - 47*Power(t2,4) + 15*Power(t2,5) + 
               4*Power(t1,5)*(1 + 4*t2) - 
               Power(t1,4)*(13 + 12*t2 + 57*Power(t2,2)) + 
               Power(t1,3)*(-13 - 81*t2 - 76*Power(t2,2) + 
                  58*Power(t2,3)) + 
               Power(t1,2)*(77 - 70*t2 + 310*Power(t2,2) + 
                  156*Power(t2,3) - 22*Power(t2,4)) + 
               t1*(-83 + 156*t2 + 626*Power(t2,2) - 236*Power(t2,3) - 
                  127*Power(t2,4) + 6*Power(t2,5)) + 
               s*(-6 + 14*t2 + 165*Power(t2,2) + 97*Power(t2,3) + 
                  15*Power(t2,4) - 18*Power(t2,5) + 
                  Power(t1,4)*(1 + t2) + 
                  Power(t1,3)*(-2 - 44*t2 + 39*Power(t2,2)) - 
                  Power(t1,2)*
                   (10 + 6*t2 - 117*Power(t2,2) + 133*Power(t2,3)) + 
                  t1*(17 + 95*t2 - 61*Power(t2,2) - 149*Power(t2,3) + 
                     114*Power(t2,4)))) + 
            s2*(6 + 7*(-1 + s)*t2 + 
               (-94 + 58*s - 7*Power(s,2))*Power(t2,2) + 
               (-120 + 61*s - 23*Power(s,2))*Power(t2,3) + 
               (-43 + 212*s + 7*Power(s,2))*Power(t2,4) + 
               (121 + 31*s + 14*Power(s,2))*Power(t2,5) + 
               5*(3 - 7*s + 3*Power(s,2))*Power(t2,6) - 
               4*Power(t1,7)*(1 + t2) + 
               Power(t1,6)*(8 - 6*s + 26*t2 + 18*Power(t2,2)) - 
               Power(t1,5)*(-1 + 7*t2 + 25*Power(t2,2) + 
                  27*Power(t2,3) + Power(s,2)*(1 + 2*t2) + 
                  s*t2*(-47 + 33*t2)) + 
               Power(t1,4)*(-7 - 9*t2 - 82*Power(t2,2) - 
                  13*Power(t2,3) + 24*Power(t2,4) + 
                  3*Power(s,2)*t2*(1 + 4*t2) + 
                  s*(13 + 84*t2 - 71*Power(t2,2) + 130*Power(t2,3))) + 
               Power(t1,3)*(-11 + 16*t2 + 60*Power(t2,2) + 
                  306*Power(t2,3) + 76*Power(t2,4) - 11*Power(t2,5) + 
                  Power(s,2)*
                   (1 + 16*t2 + 57*Power(t2,2) - 57*Power(t2,3)) + 
                  s*(4 - 72*t2 - 159*Power(t2,2) + 212*Power(t2,3) - 
                     155*Power(t2,4))) + 
               Power(t1,2)*(29 - 90*t2 - 153*Power(t2,2) - 
                  140*Power(t2,3) - 262*Power(t2,4) - 56*Power(t2,5) + 
                  Power(t2,6) + 
                  Power(s,2)*t2*
                   (-9 - 91*t2 - 109*Power(t2,2) + 121*Power(t2,3)) + 
                  s*(-17 - 109*t2 + 36*Power(t2,2) - 288*Power(t2,3) - 
                     378*Power(t2,4) + 72*Power(t2,5))) + 
               t1*(-22 + 75*t2 + 276*Power(t2,2) - 636*Power(t2,3) - 
                  425*Power(t2,4) + 72*Power(t2,5) + 12*Power(t2,6) + 
                  Power(s,2)*t2*
                   (-13 - 31*t2 + 104*Power(t2,2) + 62*Power(t2,3) - 
                     89*Power(t2,4)) + 
                  s*(6 + 43*t2 - 296*Power(t2,2) - 325*Power(t2,3) + 
                     516*Power(t2,4) + 261*Power(t2,5) - 13*Power(t2,6))))\
))*T3q(t2,s1))/((-1 + s2)*(-s + s2 - t1)*Power(-1 + s1 + t1 - t2,2)*
       Power(s1*t1 - t2,3)*(-1 + t2)*Power(-s1 + t2,2)) - 
    (8*(2 + Power(s1,10)*(Power(s2,3) + s2*Power(t1,2) + 2*Power(t1,3)) + 
         2*t2 + 5*s*t2 + 5*s2*t2 - 6*s*s2*t2 + 2*Power(t2,2) - 
         5*s*Power(t2,2) + 2*Power(s,2)*Power(t2,2) + 8*s2*Power(t2,2) + 
         s*s2*Power(t2,2) - 4*Power(s,2)*s2*Power(t2,2) + 
         3*Power(s2,2)*Power(t2,2) - 4*s*Power(s2,2)*Power(t2,2) - 
         13*Power(t2,3) - 25*s*Power(t2,3) + 19*Power(s,2)*Power(t2,3) + 
         Power(s,3)*Power(t2,3) + 20*s2*Power(t2,3) + 
         32*s*s2*Power(t2,3) - 26*Power(s,2)*s2*Power(t2,3) - 
         Power(s2,2)*Power(t2,3) - 27*s*Power(s2,2)*Power(t2,3) + 
         4*Power(s2,3)*Power(t2,3) - 45*Power(t2,4) + 50*s*Power(t2,4) + 
         8*Power(s,2)*Power(t2,4) + 2*Power(s,3)*Power(t2,4) + 
         59*s2*Power(t2,4) - 45*s*s2*Power(t2,4) - 
         16*Power(s,2)*s2*Power(t2,4) - 21*Power(s2,2)*Power(t2,4) - 
         18*s*Power(s2,2)*Power(t2,4) + 8*Power(s2,3)*Power(t2,4) + 
         19*Power(t2,5) + 36*s*Power(t2,5) + 3*Power(s,2)*Power(t2,5) - 
         Power(s,3)*Power(t2,5) - s2*Power(t2,5) - 46*s*s2*Power(t2,5) - 
         5*Power(s2,2)*Power(t2,5) - s*Power(s2,2)*Power(t2,5) + 
         2*Power(s2,3)*Power(t2,5) + 33*Power(t2,6) - 5*s*Power(t2,6) - 
         19*s2*Power(t2,6) + Power(-1 + s,2)*Power(t1,5)*(-1 + s + t2) + 
         Power(t1,4)*(6 + (-9 + 4*s2)*t2 - 4*(-1 + s2)*Power(t2,2) - 
            Power(s,3)*(3 + 2*t2) + 
            4*Power(s,2)*(3 + s2*t2 - Power(t2,2)) + 
            s*(-15 + (11 - 8*s2)*t2 + (2 + 4*s2)*Power(t2,2))) + 
         Power(t1,3)*(-14 + (19 - 17*s2)*t2 + 
            (-9 + 22*s2 - 5*Power(s2,2))*Power(t2,2) + 
            (-32 + 22*s2 - 5*Power(s2,2))*Power(t2,3) + 
            Power(s,3)*(2 + 3*t2 + Power(t2,2)) + 
            Power(s,2)*(-15 - 12*(1 + s2)*t2 + 
               (12 - 7*s2)*Power(t2,2) + 6*Power(t2,3)) + 
            s*(27 + (-6 + 29*s2)*t2 + 
               (-22 - 8*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (2 - 6*s2)*Power(t2,3))) + 
         Power(t1,2)*(16 + (-13 + 27*s2)*t2 + 
            (8 - 24*s2 + 13*Power(s2,2))*Power(t2,2) + 
            (54 - 73*s2 + 26*Power(s2,2) + 2*Power(s2,3))*Power(t2,3) + 
            (71 - 42*s2 + 8*Power(s2,2))*Power(t2,4) - 
            Power(s,3)*t2*(1 - 5*t2 + Power(t2,2)) + 
            Power(s,2)*(6 + 7*(1 + 2*s2)*t2 + (1 + 2*s2)*Power(t2,2) + 
               (-9 + 2*s2)*Power(t2,3) - 4*Power(t2,4)) - 
            s*(21 + 8*(1 + 5*s2)*t2 + 
               (10 - 5*s2 + 13*Power(s2,2))*Power(t2,2) + 
               (-34 - 5*s2 + 18*Power(s2,2))*Power(t2,3) + 7*Power(t2,4))\
) + Power(s1,9)*(Power(s2,2)*
             (-3 + 2*Power(t1,2) + t1*(2 + s - t2) + 3*s*t2) + 
            Power(s2,3)*(3*t1 - 4*(1 + t2)) - 
            s2*t1*(-5*Power(t1,2) + s*(3 + t1 - 3*t2) + 2*t2 + 
               3*t1*(3 + t2)) + 
            Power(t1,2)*(-1 + 6*Power(t1,2) - 6*t2 - t1*(17 + 4*t2) + 
               s*(-3 - 4*t1 + 4*t2))) + 
         t1*(-9 - 5*Power(t2,2) - 9*Power(t2,3) - 36*Power(t2,4) - 
            77*Power(t2,5) - Power(s2,3)*Power(t2,3)*(6 + 5*t2) + 
            Power(s,3)*Power(t2,2)*(-2 - 7*t2 + 2*Power(t2,2)) - 
            Power(s2,2)*Power(t2,2)*
             (11 + 20*t2 + 8*Power(t2,2) + 3*Power(t2,3)) + 
            s2*t2*(-19 - 2*t2 + 31*Power(t2,2) + 27*Power(t2,3) + 
               43*Power(t2,4)) + 
            Power(s,2)*t2*(4 - 7*t2 - 9*Power(t2,2) - 3*Power(t2,3) + 
               Power(t2,4) + 
               s2*(-6 + 10*t2 + 26*Power(t2,2) + Power(t2,3))) + 
            s*(6 + 35*Power(t2,2) + 3*Power(t2,3) - 62*Power(t2,4) + 
               10*Power(t2,5) + 
               Power(s2,2)*Power(t2,2)*(12 + 41*t2 + 14*Power(t2,2)) + 
               s2*t2*(25 - 2*t2 - 18*Power(t2,2) + 57*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         Power(s1,8)*(Power(s2,3)*
             (3 + 3*Power(t1,2) + 14*t2 + 6*Power(t2,2) - 
               t1*(8 + 11*t2)) + 
            Power(s2,2)*(12 + 6*Power(t1,3) - 2*(-5 + 8*s)*t2 + 
               (1 - 9*s)*Power(t2,2) + Power(t1,2)*(3*s - 8*(1 + t2)) + 
               t1*(-19 - 8*t2 + 3*Power(t2,2) + s*(-8 + 9*t2))) + 
            t1*(6*Power(t1,4) + 2*t2*(1 + 3*t2) - 
               Power(t1,3)*(45 + 7*t2) + 
               Power(s,2)*(t1 + 2*Power(t1,2) - 5*t1*t2 + 
                  3*(-1 + t2)*t2) + 
               Power(t1,2)*(49 + 11*t2 + 2*Power(t2,2)) + 
               3*t1*(3 + 18*t2 + 4*Power(t2,2)) + 
               s*(3 - 12*Power(t1,3) + 3*t2 - 8*Power(t2,2) + 
                  Power(t1,2)*(11 + 20*t2) + 
                  t1*(15 - 7*t2 - 7*Power(t2,2)))) + 
            s2*(3 + 9*Power(t1,4) + Power(t2,2) + 
               3*Power(s,2)*Power(t2,2) - 3*s*t2*(1 + t2) - 
               Power(t1,3)*(37 + 5*s + 12*t2) + 
               Power(t1,2)*(23 + 13*t2 + 3*Power(t2,2) + 
                  s*(-1 + 17*t2)) - 
               t1*(4 - 20*t2 + Power(s,2)*t2 - 6*Power(t2,2) + 
                  s*(-13 + 3*t2 + 8*Power(t2,2))))) + 
         Power(s1,7)*(-1 + 2*Power(t1,6) - Power(t2,2) - 2*Power(t2,3) + 
            4*s*Power(t2,3) - 3*Power(s,2)*Power(t2,3) + 
            Power(s,3)*Power(t2,3) - 3*Power(t1,5)*(13 + 4*s + t2) + 
            Power(t1,4)*(128 + 6*Power(s,2) + 25*t2 + Power(t2,2) + 
               5*s*(9 + 5*t2)) - 
            Power(t1,3)*(58 - 64*t2 - 8*Power(t2,2) + 
               Power(s,2)*(4 + 19*t2) + 
               2*s*(-11 + 28*t2 + 8*Power(t2,2))) + 
            Power(s2,3)*(6 + Power(t1,3) - 6*t2 - 17*Power(t2,2) - 
               4*Power(t2,3) - 5*Power(t1,2)*(1 + 2*t2) + 
               t1*(3 + 25*t2 + 15*Power(t2,2))) - 
            t1*(-2 + 19*t2 + 57*Power(t2,2) + 
               2*Power(s,3)*Power(t2,2) + 12*Power(t2,3) + 
               Power(s,2)*t2*(-17 + 3*t2 + 4*Power(t2,2)) + 
               s*(14 + 25*t2 - 34*Power(t2,2) - 14*Power(t2,3))) + 
            Power(t1,2)*(-25 - 175*t2 + Power(s,3)*t2 - 
               72*Power(t2,2) - 6*Power(t2,3) + 
               Power(s,2)*(3 + 4*t2 + 17*Power(t2,2)) + 
               s*(-11 - 25*t2 - 18*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s2,2)*(-9 + 6*Power(t1,4) + (-32 + 29*s)*t2 + 
               3*(-4 + 13*s)*Power(t2,2) + (-3 + 9*s)*Power(t2,3) + 
               Power(t1,3)*(3*s - 4*(7 + 4*t2)) + 
               2*Power(t1,2)*
                (-1 + 9*t2 + 5*Power(t2,2) + s*(-9 + 5*t2)) + 
               t1*(35 + 76*t2 + 16*Power(t2,2) - 3*Power(t2,3) + 
                  s*(17 - 26*t2 - 27*Power(t2,2)))) + 
            s2*(-12 + 7*Power(t1,5) + (-8 + 17*s)*t2 + 
               (-11 + 22*s - 17*Power(s,2))*Power(t2,2) + 
               (-3 + 8*s - 6*Power(s,2))*Power(t2,3) - 
               Power(t1,4)*(62 + 9*s + 13*t2) + 
               Power(t1,3)*(83 + 54*t2 + 8*Power(t2,2) + 
                  s*(15 + 37*t2)) - 
               Power(t1,2)*(23 - 38*t2 - 5*Power(t2,2) + Power(t2,3) + 
                  Power(s,2)*(2 + 4*t2) + 
                  s*(-5 + 51*t2 + 30*Power(t2,2))) + 
               t1*(29 - 38*t2 - 47*Power(t2,2) - 6*Power(t2,3) + 
                  Power(s,2)*t2*(-3 + 14*t2) + 
                  s*(-5 - 52*t2 - 6*Power(t2,2) + 7*Power(t2,3))))) + 
         Power(s1,6)*(4 - (11 + 4*s)*Power(t1,6) + 2*t2 - s*t2 + 
            10*Power(t2,2) + s*Power(t2,2) + 20*Power(t2,3) - 
            23*s*Power(t2,3) + 14*Power(s,2)*Power(t2,3) - 
            5*Power(s,3)*Power(t2,3) + 4*Power(t2,4) - 7*s*Power(t2,4) + 
            4*Power(s,2)*Power(t2,4) - Power(s,3)*Power(t2,4) + 
            Power(t1,5)*(106 + 43*s + 6*Power(s,2) + 10*t2 + 9*s*t2) - 
            Power(t1,4)*(163 - 11*t2 - 3*Power(t2,2) + 
               20*Power(s,2)*(1 + t2) + s*(29 + 64*t2 + 6*Power(t2,2))) \
+ Power(t1,3)*(39 - 330*t2 - 79*Power(t2,2) - 2*Power(t2,3) + 
               Power(s,3)*(-2 + 3*t2) + 
               Power(s,2)*(10 + 48*t2 + 23*Power(t2,2)) + 
               s*(-61 - 16*t2 + Power(t2,2) + Power(t2,3))) - 
            Power(s2,3)*(9 + 28*t2 + 2*Power(t2,2) - 7*Power(t2,3) - 
               Power(t2,4) + 3*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(3 - 19*t2 - 11*Power(t2,2)) + 
               t1*(-5 + 5*t2 + 25*Power(t2,2) + 9*Power(t2,3))) + 
            Power(t1,2)*(31 + 255*t2 + 109*Power(t2,2) + 
               19*Power(t2,3) + Power(s,3)*(t2 - 7*Power(t2,2)) - 
               Power(s,2)*(27 - 43*t2 + 21*Power(t2,2) + 
                  10*Power(t2,3)) + 
               2*s*(-19 - t2 + 82*Power(t2,2) + 13*Power(t2,3))) + 
            t1*(-13 + 46*t2 + 206*Power(t2,2) + 87*Power(t2,3) + 
               6*Power(t2,4) + Power(s,3)*Power(t2,2)*(6 + 5*t2) + 
               Power(s,2)*t2*
                (-39 - 27*t2 - 11*Power(t2,2) + Power(t2,3)) + 
               s*(13 + 67*t2 - 11*Power(t2,2) - 31*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s2,2)*(-18 + 2*Power(t1,5) + 
               Power(t1,4)*(-30 + s - 11*t2) + (7 - 8*s)*t2 + 
               (16 - 49*s)*Power(t2,2) + (4 - 27*s)*Power(t2,3) - 
               3*(-1 + s)*Power(t2,4) + 
               2*Power(t1,3)*
                (34 + 33*t2 + 6*Power(t2,2) + s*(-5 + 3*t2)) + 
               Power(t1,2)*(26 + 50*t2 - 3*Power(t2,2) - 
                  4*Power(t2,3) + s*(37 - 19*t2 - 27*Power(t2,2))) + 
               t1*(2 - 110*t2 - 119*Power(t2,2) - 16*Power(t2,3) + 
                  Power(t2,4) + 
                  s*(-8 + 25*t2 + 71*Power(t2,2) + 23*Power(t2,3)))) + 
            s2*(9 + 2*Power(t1,6) + (22 - 37*s)*t2 + 
               (33 - 43*s + 39*Power(s,2))*Power(t2,2) + 
               (29 - 29*s + 25*Power(s,2))*Power(t2,3) + 
               (3 - 7*s + 3*Power(s,2))*Power(t2,4) - 
               Power(t1,5)*(50 + 7*s + 4*t2) + 
               Power(t1,4)*(125 + 55*t2 + 3*Power(t2,2) + 
                  s*(33 + 31*t2)) - 
               Power(t1,3)*(116 - 18*t2 + 11*Power(t2,2) + 
                  Power(t2,3) + Power(s,2)*(4 + 7*t2) + 
                  2*s*(23 + 63*t2 + 18*Power(t2,2))) + 
               Power(t1,2)*(13 - 189*t2 - 129*Power(t2,2) - 
                  10*Power(t2,3) + 
                  Power(s,2)*(8 - 3*t2 + 23*Power(t2,2)) + 
                  2*s*(11 + 3*t2 + 15*Power(t2,2) + 7*Power(t2,3))) + 
               t1*(-46 - 41*t2 + 35*Power(t2,2) + 32*Power(t2,3) + 
                  2*Power(t2,4) + 
                  Power(s,2)*t2*(33 - 44*t2 - 19*Power(t2,2)) + 
                  s*(-43 + 132*t2 + 179*Power(t2,2) + 31*Power(t2,3) - 
                     2*Power(t2,4))))) + 
         Power(s1,4)*(-6 - (12 + 14*s + 5*Power(s,2))*Power(t1,6) - 
            5*t2 - 17*s*t2 + 21*Power(t2,2) + 12*s*Power(t2,2) + 
            2*Power(s,2)*Power(t2,2) + 143*Power(t2,3) + 
            23*s*Power(t2,3) - 2*Power(s,2)*Power(t2,3) - 
            13*Power(s,3)*Power(t2,3) + 105*Power(t2,4) - 
            16*s*Power(t2,4) - 15*Power(s,2)*Power(t2,4) - 
            3*Power(s,3)*Power(t2,4) + 14*Power(t2,5) - 
            8*s*Power(t2,5) - 3*Power(s,2)*Power(t2,5) + 
            Power(s,3)*Power(t2,5) + 
            Power(t1,5)*(65 - 77*t2 + s*(44 + t2) + 
               Power(s,2)*(25 + 12*t2)) + 
            Power(t1,4)*(-33 + Power(s,3)*(2 - 4*t2) + 238*t2 + 
               94*Power(t2,2) - 
               3*Power(s,2)*(11 + 11*t2 + 2*Power(t2,2)) + 
               s*(-54 + 33*t2 + 39*Power(t2,2))) + 
            Power(t1,3)*(-14 - 483*t2 + 104*Power(t2,2) - 
               9*Power(t2,3) + 
               Power(s,3)*(-2 - 3*t2 + 11*Power(t2,2)) - 
               Power(s,2)*(-20 + 46*t2 + 45*Power(t2,2) + 
                  4*Power(t2,3)) - 
               s*(-43 + 44*t2 + 131*Power(t2,2) + 26*Power(t2,3))) + 
            Power(s2,3)*(5 - 3*t2 - 55*Power(t2,2) - 36*Power(t2,3) - 
               7*Power(t2,4) - Power(t2,5) + 
               2*Power(t1,4)*(-7 + 5*t2) + 
               Power(t1,3)*(25 + 12*t2 - 17*Power(t2,2)) + 
               Power(t1,2)*(-4 + 34*t2 - 41*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-9 - 19*t2 + 25*Power(t2,2) + 28*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(t1,2)*(-18 + 57*t2 - 366*Power(t2,2) - 
               162*Power(t2,3) + 2*Power(t2,4) - 
               3*Power(s,3)*t2*(-7 + t2 + 3*Power(t2,2)) + 
               s*(17 - 67*t2 + 242*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s,2)*(-23 + 93*t2 + 191*Power(t2,2) + 
                  86*Power(t2,3) + 3*Power(t2,4))) + 
            t1*(Power(s,3)*Power(t2,2)*(-6 + 7*t2 + Power(t2,2)) + 
               Power(s,2)*t2*
                (11 + 69*t2 - 97*Power(t2,2) - 30*Power(t2,3)) - 
               s*(53 + 81*t2 - 156*Power(t2,2) - 297*Power(t2,3) - 
                  60*Power(t2,4) + Power(t2,5)) + 
               2*(4 - 12*t2 + 137*Power(t2,2) + 116*Power(t2,3) + 
                  41*Power(t2,4) + Power(t2,5))) - 
            Power(s2,2)*(8*Power(t1,6) - 
               2*Power(t1,5)*(39 + s + 9*t2) + 
               Power(t1,4)*(93 + 102*t2 + 11*Power(t2,2) + 
                  s*(-40 + 22*t2)) + 
               Power(t1,3)*(25 + 305*t2 + 28*Power(t2,2) + 
                  2*Power(t2,3) + s*(89 + 18*t2 - 36*Power(t2,2))) - 
               Power(t1,2)*(29 + 128*t2 + 451*Power(t2,2) + 
                  98*Power(t2,3) + 3*Power(t2,4) - 
                  2*s*(-6 + 43*t2 - 27*Power(t2,2) + 9*Power(t2,3))) + 
               t1*(2 - 186*t2 - 251*Power(t2,2) - 19*Power(t2,3) + 
                  16*Power(t2,4) + 
                  s*(-16 - 73*t2 + 19*Power(t2,2) + 41*Power(t2,3) + 
                     Power(t2,4))) + 
               t2*(81 + 87*t2 + 63*Power(t2,2) + 36*Power(t2,3) + 
                  2*Power(t2,4) - 
                  s*(40 + 140*t2 + 69*Power(t2,2) + 15*Power(t2,3) + 
                     3*Power(t2,4)))) + 
            s2*(-27 + 2*(4 + 5*s)*Power(t1,6) + (-62 + 29*s)*t2 + 
               (-44 + 78*s + 13*Power(s,2))*Power(t2,2) + 
               (-42 + 146*s + Power(s,2))*Power(t2,3) + 
               (17 + 68*s - 5*Power(s,2))*Power(t2,4) + 
               (8 + 5*s - 3*Power(s,2))*Power(t2,5) - 
               Power(t1,5)*(130 + 3*Power(s,2) - 48*t2 + 
                  s*(76 + 26*t2)) + 
               Power(t1,4)*(170 - 20*t2 - 81*Power(t2,2) + 
                  Power(s,2)*(3 + 16*t2) + 
                  s*(79 + 149*t2 + 16*Power(t2,2))) + 
               Power(t1,3)*(-46 + 703*t2 + 11*Power(t2,2) + 
                  26*Power(t2,3) + 
                  Power(s,2)*(11 + 16*t2 - 30*Power(t2,2)) + 
                  s*(57 + 125*t2 + 50*Power(t2,2) + 6*Power(t2,3))) - 
               t1*(-63 - 65*t2 + 511*Power(t2,2) + 327*Power(t2,3) + 
                  39*Power(t2,4) - Power(t2,5) + 
                  Power(s,2)*t2*
                   (-59 + 21*t2 - 2*Power(t2,2) + Power(t2,3)) + 
                  s*(1 + 130*t2 + 26*Power(t2,2) - 128*Power(t2,3) - 
                     41*Power(t2,4))) + 
               Power(t1,2)*(6 - 284*t2 - 284*Power(t2,2) - 
                  23*Power(t2,3) - 2*Power(t2,4) + 
                  Power(s,2)*
                   (8 - 101*t2 - 16*Power(t2,2) + 21*Power(t2,3)) - 
                  s*(-5 + 67*t2 + 735*Power(t2,2) + 169*Power(t2,3) + 
                     6*Power(t2,4))))) - 
         Power(s1,5)*(3 - (29 + 12*s + 2*Power(s,2))*Power(t1,6) + 
            4*t2 - 8*s*t2 + 27*Power(t2,2) + 8*s*Power(t2,2) + 
            80*Power(t2,3) - 34*s*Power(t2,3) + 
            18*Power(s,2)*Power(t2,3) - 11*Power(s,3)*Power(t2,3) + 
            32*Power(t2,4) - 29*s*Power(t2,4) + 
            9*Power(s,2)*Power(t2,4) - 3*Power(s,3)*Power(t2,4) + 
            2*Power(t2,5) - 3*s*Power(t2,5) + Power(s,2)*Power(t2,5) + 
            Power(t1,5)*(101 + 11*t2 + Power(s,2)*(20 + 6*t2) + 
               s*(45 + 19*t2)) - 
            Power(t1,4)*(140 + 2*Power(s,3)*(-1 + t2) - 242*t2 - 
               22*Power(t2,2) + s*(15 - 7*t2 + Power(t2,2)) + 
               Power(s,2)*(36 + 51*t2 + 6*Power(t2,2))) + 
            Power(t1,3)*(11 - 488*t2 - 179*Power(t2,2) - 
               4*Power(t2,3) + 3*Power(s,3)*(-2 + t2 + 2*Power(t2,2)) + 
               Power(s,2)*(28 + 26*t2 + 31*Power(t2,2) + 
                  2*Power(t2,3)) - 
               s*(21 + 22*t2 + 137*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s2,3)*(2*Power(t1,4) + 
               Power(t1,3)*(17 - 20*t2 - 2*Power(t2,2)) - 
               t2*(35 + 50*t2 + 11*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-9 - 4*t2 + 20*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-2 + 12*t2 + 14*Power(t2,2) - 7*Power(t2,3) - 
                  2*Power(t2,4))) + 
            Power(t1,2)*(14 + 213*t2 - 125*Power(t2,2) - 
               13*Power(t2,3) + 
               Power(s,3)*t2*(13 - 15*t2 - 6*Power(t2,2)) + 
               Power(s,2)*(-51 + 115*t2 + 80*Power(t2,2) + 
                  12*Power(t2,3)) + 
               s*(-50 - 113*t2 + 220*Power(t2,2) + 86*Power(t2,3) + 
                  Power(t2,4))) + 
            t1*(-19 + 24*t2 + 336*Power(t2,2) + 233*Power(t2,3) + 
               34*Power(t2,4) + 
               Power(s,3)*Power(t2,2)*(4 + 13*t2 + 2*Power(t2,2)) - 
               Power(s,2)*t2*
                (37 + 47*t2 + 79*Power(t2,2) + 13*Power(t2,3)) + 
               s*(-26 + 46*t2 + 97*Power(t2,2) + 83*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s2,2)*(-27 + (-73 + 37*s)*t2 + 
               (-34 + 31*s)*Power(t2,2) - (29 + 9*s)*Power(t2,3) - 
               (3 + s)*Power(t2,4) + Power(t2,5) + 
               2*Power(t1,5)*(10 + t2) - 
               Power(t1,4)*(125 + 52*t2 + 4*Power(t2,2) + 
                  2*s*(1 + t2)) + 
               Power(t1,3)*(27 + 111*t2 + 32*Power(t2,2) + 
                  2*Power(t2,3) + s*(-53 + 35*t2 + 10*Power(t2,2))) + 
               Power(t1,2)*(29 + 222*t2 + 147*Power(t2,2) + 
                  10*Power(t2,3) + 
                  s*(33 + 12*t2 - 58*Power(t2,2) - 14*Power(t2,3))) + 
               t1*(39 + 59*t2 - 113*Power(t2,2) - 79*Power(t2,3) - 
                  6*Power(t2,4) + 
                  s*(13 + 27*t2 + 35*Power(t2,2) + 36*Power(t2,3) + 
                     6*Power(t2,4)))) + 
            s2*(-18 + 2*(8 + s)*Power(t1,6) - (4 + 25*s)*t2 + 
               (20 - 7*s + 43*Power(s,2))*Power(t2,2) + 
               3*(16 + 5*s + 12*Power(s,2))*Power(t2,3) + 
               5*(5 - s + Power(s,2))*Power(t2,4) + 
               (1 - 2*s)*Power(t2,5) - 
               Power(t1,5)*(71 + 22*t2 + s*(30 + 8*t2)) + 
               Power(t1,4)*(234 - 54*t2 + 5*Power(t2,2) + 
                  Power(s,2)*(5 + 4*t2) + 
                  2*s*(56 + 48*t2 + 5*Power(t2,2))) - 
               Power(t1,3)*(75 - 177*t2 - 109*Power(t2,2) - 
                  2*Power(t2,3) + 
                  2*Power(s,2)*(2 + 9*t2 + 7*Power(t2,2)) + 
                  s*(26 + 213*t2 + 55*Power(t2,2) + 4*Power(t2,3))) + 
               Power(t1,2)*(3 - 368*t2 - 249*Power(t2,2) - 
                  64*Power(t2,3) - Power(t2,4) + 
                  2*Power(s,2)*
                   (6 - 33*t2 + 25*Power(t2,2) + 8*Power(t2,3)) - 
                  2*s*(-28 + 83*t2 + 113*Power(t2,2) + 14*Power(t2,3))) \
+ t1*(13 - 109*t2 - 214*Power(t2,2) - 52*Power(t2,3) + 6*Power(t2,4) - 
                  Power(s,2)*t2*
                   (-75 + 38*t2 + 42*Power(t2,2) + 6*Power(t2,3)) + 
                  s*(-61 + 52*t2 + 347*Power(t2,2) + 183*Power(t2,3) + 
                     19*Power(t2,4))))) + 
         Power(s1,3)*(9 + (10 + 10*s + 3*Power(s,2))*Power(t1,6) + 
            17*t2 + 8*s*t2 + 12*Power(t2,2) + 7*s*Power(t2,2) - 
            8*Power(s,2)*Power(t2,2) - 122*Power(t2,3) - 
            118*s*Power(t2,3) + 7*Power(s,3)*Power(t2,3) - 
            156*Power(t2,4) - 82*s*Power(t2,4) + 
            63*Power(s,2)*Power(t2,4) - Power(s,3)*Power(t2,4) - 
            47*Power(t2,5) + 16*Power(s,2)*Power(t2,5) - 
            2*Power(s,3)*Power(t2,5) - Power(t2,6) + s*Power(t2,6) - 
            Power(t1,5)*(-13 + Power(s,3) + s*(37 - 15*t2) - t2 + 
               Power(s,2)*(4 + t2)) + 
            Power(t1,4)*(-64 - 235*t2 + 101*Power(t2,2) + 
               Power(s,3)*(9 + 2*t2) + 
               s*(96 - 91*t2 - 37*Power(t2,2)) - 
               7*Power(s,2)*(1 + 5*t2 + 2*Power(t2,2))) + 
            Power(t1,3)*(35 + 161*t2 - 61*Power(t2,2) - 
               117*Power(t2,3) + 
               2*Power(s,3)*(-6 - 3*t2 + Power(t2,2)) + 
               s*(-81 + 136*t2 + 189*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s,2)*(41 + 63*t2 + 111*Power(t2,2) + 
                  18*Power(t2,3))) + 
            Power(s2,3)*(-2 - 13*t2 + 14*Power(t2,2) + 34*Power(t2,3) + 
               5*Power(t2,4) + Power(t2,5) + 2*Power(t1,4)*(2 + t2) + 
               Power(t1,3)*(-6 + 28*t2 - 27*Power(t2,2)) + 
               Power(t1,2)*t2*(-64 - 17*t2 + 40*Power(t2,2)) + 
               t1*(4 + 35*t2 - 26*Power(t2,2) + 8*Power(t2,3) - 
                  12*Power(t2,4))) - 
            Power(t1,2)*(-26 - 97*t2 - 576*Power(t2,2) - 
               4*Power(t2,3) - 19*Power(t2,4) + 
               Power(s,3)*t2*(9 + 16*t2 + 8*Power(t2,2)) + 
               s*(16 + 159*t2 + 121*Power(t2,2) - 21*Power(t2,3) - 
                  18*Power(t2,4)) + 
               Power(s,2)*(39 - 19*t2 + 67*Power(t2,2) + 
                  85*Power(t2,3) + 5*Power(t2,4))) + 
            t1*(-29 - t2 - 42*Power(t2,2) - 73*Power(t2,3) - 
               64*Power(t2,4) - 13*Power(t2,5) + 
               7*Power(s,3)*Power(t2,2)*(2 + 2*t2 + Power(t2,2)) - 
               Power(s,2)*t2*
                (61 + 230*t2 + 52*Power(t2,2) + 3*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(14 + 161*t2 - 33*Power(t2,2) - 510*Power(t2,3) - 
                  82*Power(t2,4) + Power(t2,5))) + 
            Power(s2,2)*(-15 + 8*Power(t1,6) + 
               2*Power(t1,5)*(5*s + 3*(-6 + t2)) - (-5 + s)*t2 + 
               (53 - 136*s)*Power(t2,2) - (7 + 139*s)*Power(t2,3) - 
               23*(-1 + s)*Power(t2,4) + (11 - 4*s)*Power(t2,5) - 
               Power(t1,4)*(-31 + 196*t2 + 35*Power(t2,2) + 
                  4*s*(11 + 3*t2)) + 
               Power(t1,3)*(5 + 307*t2 + 389*Power(t2,2) + 
                  27*Power(t2,3) + s*(44 - 70*t2 + 45*Power(t2,2))) - 
               Power(t1,2)*(31 - 10*t2 - 170*Power(t2,2) + 
                  174*Power(t2,3) + 5*Power(t2,4) + 
                  s*(1 - 250*t2 - 7*Power(t2,2) + 70*Power(t2,3))) - 
               t1*(-38 + 48*t2 + 323*Power(t2,2) + 460*Power(t2,3) + 
                  56*Power(t2,4) + Power(t2,5) + 
                  s*(5 + 106*t2 - 70*Power(t2,2) - 30*Power(t2,3) - 
                     31*Power(t2,4)))) + 
            s2*(-2*(8 + 5*s)*Power(t1,6) + 
               Power(t1,5)*(47 + Power(s,2) + 24*t2 + 2*s*(18 + t2)) + 
               Power(t1,4)*(-53 + 9*Power(s,2) + 349*t2 - 
                  109*Power(t2,2) + s*t2*(121 + 40*t2)) - 
               Power(t1,3)*(-76 + 506*t2 + 381*Power(t2,2) - 
                  130*Power(t2,3) + 
                  Power(s,2)*(17 + 26*t2 + 19*Power(t2,2)) + 
                  s*(67 + 253*t2 + 456*Power(t2,2) + 44*Power(t2,3))) + 
               Power(t1,2)*(-52 + 114*t2 - 588*Power(t2,2) + 
                  161*Power(t2,3) - 26*Power(t2,4) + 
                  Power(s,2)*
                   (-2 - 17*t2 + 44*Power(t2,2) + 39*Power(t2,3)) + 
                  s*(75 - 167*t2 + 262*Power(t2,2) + 282*Power(t2,3) + 
                     10*Power(t2,4))) - 
               t2*(-57 - 58*t2 - 187*Power(t2,2) - 98*Power(t2,3) + 
                  Power(t2,4) + Power(t2,5) - 
                  Power(s,2)*t2*
                   (25 + 79*t2 + 19*Power(t2,2) + 5*Power(t2,3)) + 
                  s*(53 + 114*t2 + 130*Power(t2,2) + 123*Power(t2,3) + 
                     27*Power(t2,4))) + 
               t1*(Power(s,2)*t2*
                   (15 + 55*t2 - 46*Power(t2,2) - 26*Power(t2,3)) - 
                  2*(1 + 107*t2 - 120*Power(t2,2) - 309*Power(t2,3) - 
                     56*Power(t2,4) + Power(t2,5)) + 
                  s*(-47 + 116*t2 + 390*Power(t2,2) + 569*Power(t2,3) + 
                     44*Power(t2,4) + 2*Power(t2,5))))) - 
         s1*(5 + Power(-1 + s,2)*Power(t1,6) + t2 + 16*s*t2 - 
            4*Power(t2,2) - 21*s*Power(t2,2) + 
            8*Power(s,2)*Power(t2,2) - 34*Power(t2,3) - 
            8*s*Power(t2,3) + 55*Power(s,2)*Power(t2,3) + 
            3*Power(s,3)*Power(t2,3) - 14*Power(t2,4) + 
            201*s*Power(t2,4) - 6*Power(s,2)*Power(t2,4) + 
            6*Power(s,3)*Power(t2,4) + 95*Power(t2,5) + 
            59*s*Power(t2,5) - 5*Power(s,2)*Power(t2,5) - 
            2*Power(s,3)*Power(t2,5) + 15*Power(t2,6) - 
            7*s*Power(t2,6) + 
            Power(t1,5)*(3*Power(s,3) - 4*s*(-5 + t2) + 11*(-1 + t2) - 
               3*Power(s,2)*(4 + t2)) + 
            Power(t1,4)*(32 - 46*t2 - 76*Power(t2,2) - 
               Power(s,3)*(13 + 4*t2) + Power(s,2)*(39 + 8*t2) + 
               s*(-54 + 11*t2 + 4*Power(t2,2))) + 
            Power(s2,3)*Power(t2,2)*
             (3 + Power(t1,3) + 19*t2 + 8*Power(t2,2) - 7*Power(t2,3) - 
               Power(t1,2)*(5 + 6*t2) + t1*(1 - t2 + 8*Power(t2,2))) + 
            Power(t1,3)*(-45 + 81*t2 + 112*Power(t2,2) + 
               163*Power(t2,3) + 
               Power(s,3)*(10 + 15*t2 - 4*Power(t2,2)) - 
               s*(-82 + 86*t2 - 26*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(-59 - 14*t2 + 44*Power(t2,2) + 8*Power(t2,3))\
) + Power(t1,2)*(38 - 67*t2 + 9*Power(t2,2) + 49*Power(t2,3) - 
               185*Power(t2,4) + 
               Power(s,3)*t2*(-5 + 15*t2 + 6*Power(t2,2)) + 
               s*(-72 + 85*t2 + 29*Power(t2,2) - 106*Power(t2,3) + 
                  9*Power(t2,4)) - 
               Power(s,2)*(-31 + 8*t2 + 74*Power(t2,2) + 
                  69*Power(t2,3) + 9*Power(t2,4))) + 
            t1*(-20 + 20*t2 - 41*Power(t2,2) - 218*Power(t2,3) - 
               92*Power(t2,4) + 71*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(-8 - 23*t2 + Power(t2,2)) + 
               Power(s,2)*t2*
                (25 + 42*t2 + 55*Power(t2,2) + 34*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s*(26 - 22*t2 + 4*Power(t2,2) + 41*Power(t2,3) - 
                  10*Power(t2,4) + Power(t2,5))) + 
            Power(s2,2)*t2*(10 + 15*t2 + Power(t2,2) + 56*Power(t2,3) + 
               70*Power(t2,4) - Power(t1,4)*(2 + 19*t2) + 
               Power(t1,3)*(-8 + 83*t2 + 29*Power(t2,2)) + 
               Power(t1,2)*(32 - 43*t2 + 2*Power(t2,2) - 
                  9*Power(t2,3)) - 
               t1*(32 + 36*t2 + 116*Power(t2,2) + 116*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(-6 + 2*Power(t1,4) + Power(t1,3)*(3 - 25*t2) - 4*t2 - 
                  17*Power(t2,2) + 12*Power(t2,3) + 4*Power(t2,4) + 
                  Power(t1,2)*(-16 + 69*t2 + 20*Power(t2,2)) - 
                  t1*(-17 + 52*t2 + 34*Power(t2,2) + Power(t2,3)))) + 
            s2*(6 + (17 - 11*s)*t2 + 
               (23 + 19*s - 19*Power(s,2))*Power(t2,2) + 
               (22 + 23*s - 95*Power(s,2))*Power(t2,3) - 
               (61 + 182*s + 38*Power(s,2))*Power(t2,4) + 
               (-118 - 69*s + 5*Power(s,2))*Power(t2,5) + 
               7*Power(t2,6) + (-1 + s)*Power(t1,5)*(-1 + s + 6*t2) + 
               Power(t1,4)*(1 + 36*t2 + 8*Power(s,2)*t2 + 
                  54*Power(t2,2) + s*(-1 - 35*t2 + 6*Power(t2,2))) - 
               Power(t1,3)*(15 + 53*t2 + 140*Power(t2,2) + 
                  120*Power(t2,3) + 
                  Power(s,2)*(1 + 56*t2 + 17*Power(t2,2)) + 
                  s*(-14 - 109*t2 + 65*Power(t2,2) + 32*Power(t2,3))) + 
               Power(t1,2)*(29 + 39*t2 - 6*Power(t2,2) - 
                  70*Power(t2,3) + 151*Power(t2,4) + 
                  Power(s,2)*t2*(81 + 50*t2 + 11*Power(t2,2)) + 
                  s*(-17 - 148*t2 + 18*Power(t2,2) + 222*Power(t2,3) + 
                     22*Power(t2,4))) - 
               t1*(22 + 33*t2 - 69*Power(t2,2) - 344*Power(t2,3) - 
                  180*Power(t2,4) + 86*Power(t2,5) + 
                  Power(s,2)*t2*
                   (31 + t2 - 44*Power(t2,2) + 8*Power(t2,3)) + 
                  s*(-6 - 79*t2 - 61*Power(t2,2) + 103*Power(t2,3) + 
                     51*Power(t2,4) + 2*Power(t2,5))))) + 
         Power(s1,2)*((7 - 6*s + Power(s,2))*Power(t1,6) + 
            Power(t1,5)*(3*Power(s,3) + s*(36 - 8*t2) - 18*(2 + 3*t2) - 
               Power(s,2)*(16 + 9*t2)) + 
            Power(t1,4)*(69 + 41*t2 + 103*Power(t2,2) - 
               Power(s,3)*(19 + 2*t2) + 
               9*Power(s,2)*(5 + 5*t2 + 2*Power(t2,2)) + 
               s*(-100 + 44*t2 + 5*Power(t2,2))) + 
            Power(s2,3)*t2*(5 + 7*t2 + 3*Power(t2,2) - 12*Power(t2,3) + 
               3*Power(t2,4) - Power(t1,3)*(9 + 5*t2) + 
               Power(t1,2)*(17 - 10*t2 + 22*Power(t2,2)) + 
               t1*(-13 + 26*t2 + 19*Power(t2,2) - 26*Power(t2,3))) - 
            Power(t1,3)*(56 - 107*t2 - 256*Power(t2,2) + 
               161*Power(t2,3) + 
               3*Power(s,3)*(-6 - 7*t2 + 4*Power(t2,2)) + 
               s*(-104 + 142*t2 + 12*Power(t2,2) - 23*Power(t2,3)) + 
               Power(s,2)*(85 + 22*t2 + 26*Power(t2,2) + 10*Power(t2,3))) \
+ Power(t1,2)*(14 - 123*t2 - 311*Power(t2,2) - 185*Power(t2,3) + 
               83*Power(t2,4) + 
               Power(s,3)*t2*(-5 + 21*t2 + 18*Power(t2,2)) - 
               s*(65 - 236*t2 + Power(t2,2) + 226*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s,2)*(-59 + 54*t2 + 115*Power(t2,2) + 
                  39*Power(t2,3) + 3*Power(t2,4))) + 
            t1*(2 + 40*t2 - 87*Power(t2,2) - 258*Power(t2,3) + 
               122*Power(t2,4) + 15*Power(t2,5) - 
               Power(s,3)*Power(t2,2)*(14 + 29*t2 + 7*Power(t2,2)) + 
               Power(s,2)*t2*
                (59 + 190*t2 + 149*Power(t2,2) + 56*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s*(31 - 101*t2 - 72*Power(t2,2) + 351*Power(t2,3) + 
                  142*Power(t2,4) - 9*Power(t2,5))) + 
            t2*(-11 - 21*t2 + 20*Power(t2,2) + 122*Power(t2,3) + 
               75*Power(t2,4) + 7*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(1 + 6*t2) + 
               Power(s,2)*t2*
                (12 + 45*t2 - 57*Power(t2,2) - 20*Power(t2,3)) + 
               s*(13 - 28*t2 + 97*Power(t2,2) + 227*Power(t2,3) + 
                  16*Power(t2,4) - 3*Power(t2,5))) + 
            Power(s2,2)*(6 - 4*(-7 + 4*s)*t2 - 22*Power(t1,5)*t2 + 
               (7 + 46*s)*Power(t2,2) + (42 + 89*s)*Power(t2,3) + 
               40*(3 + s)*Power(t2,4) + (7 - 6*s)*Power(t2,5) + 
               Power(t1,4)*(-3 + s + 99*t2 - 26*s*t2 + 23*Power(t2,2)) - 
               Power(t1,2)*(-13 + (-56 + 159*s)*t2 + 
                  (321 + 10*s)*Power(t2,2) + (390 + 8*s)*Power(t2,3) + 
                  15*Power(t2,4)) + 
               t1*(-17 + (-68 + 69*s)*t2 + (34 - 124*s)*Power(t2,2) + 
                  (141 - 51*s)*Power(t2,3) + (221 + 21*s)*Power(t2,4) + 
                  3*Power(t2,5)) + 
               Power(t1,3)*(1 - 93*t2 + 131*Power(t2,2) + 
                  11*Power(t2,3) + s*(-1 + 120*t2 + 19*Power(t2,2)))) + 
            s2*(15 + 2*(-1 + s)*Power(t1,6) + (-1 + 17*s)*t2 + 
               (-2 + 71*s - 35*Power(s,2))*Power(t2,2) - 
               11*(11 - s + 12*Power(s,2))*Power(t2,3) - 
               (249 + 80*s + 34*Power(s,2))*Power(t2,4) + 
               3*(-13 + 7*s + Power(s,2))*Power(t2,5) + 3*Power(t2,6) + 
               Power(t1,5)*(8 + 3*Power(s,2) + 48*t2 + s*(-9 + 22*t2)) - 
               Power(t1,4)*(-10 + 117*t2 + 110*Power(t2,2) + 
                  Power(s,2)*(7 + 8*t2) + 
                  4*s*(-2 + 28*t2 + 11*Power(t2,2))) + 
               Power(t1,3)*(-55 + 28*t2 - 304*Power(t2,2) + 
                  185*Power(t2,3) + 
                  5*Power(s,2)*(1 - 9*t2 + 5*Power(t2,2)) + 
                  s*(34 + 84*t2 + 90*Power(t2,2) + 8*Power(t2,3))) + 
               Power(t1,2)*(73 - 45*t2 + 546*Power(t2,2) + 
                  508*Power(t2,3) - 140*Power(t2,4) + 
                  Power(s,2)*t2*(126 + 47*t2 - 35*Power(t2,2)) + 
                  2*s*(-33 - 6*t2 + 130*Power(t2,2) + 151*Power(t2,3) + 
                     9*Power(t2,4))) + 
               t1*(-49 + 87*t2 + 134*Power(t2,2) + 88*Power(t2,3) - 
                  300*Power(t2,4) + 16*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-53 - 53*t2 + 39*Power(t2,2) + 12*Power(t2,3)) - 
                  s*(-31 - 40*t2 + 119*Power(t2,2) + 637*Power(t2,3) + 
                     292*Power(t2,4) + 6*Power(t2,5))))))*T4q(s1))/
     (Power(-1 + s1,2)*(-1 + s2)*(-s + s2 - t1)*Power(-1 + s1 + t1 - t2,2)*
       Power(s1*t1 - t2,3)*(-1 + t2)) + 
    (8*(-2 + Power(s1,7)*(Power(s2,3) + s2*Power(t1,2) + 2*Power(t1,3)) - 
         2*t2 - 5*s*t2 - 5*s2*t2 + 6*s*s2*t2 - 2*Power(t2,2) + 
         5*s*Power(t2,2) - 2*Power(s,2)*Power(t2,2) - 8*s2*Power(t2,2) - 
         s*s2*Power(t2,2) + 4*Power(s,2)*s2*Power(t2,2) - 
         3*Power(s2,2)*Power(t2,2) + 4*s*Power(s2,2)*Power(t2,2) + 
         27*Power(t2,3) - 9*s*Power(t2,3) - 15*Power(s,2)*Power(t2,3) - 
         Power(s,3)*Power(t2,3) - 24*s2*Power(t2,3) + 
         40*s*s2*Power(t2,3) + 22*Power(s,2)*s2*Power(t2,3) - 
         7*Power(s2,2)*Power(t2,3) - 13*s*Power(s2,2)*Power(t2,3) - 
         7*Power(t2,4) - 18*s*Power(t2,4) + 6*Power(s,2)*Power(t2,4) - 
         2*Power(s,3)*Power(t2,4) + 29*s2*Power(t2,4) + 
         19*s*s2*Power(t2,4) + 4*Power(s,2)*s2*Power(t2,4) - 
         19*Power(s2,2)*Power(t2,4) - 2*s*Power(s2,2)*Power(t2,4) - 
         13*Power(t2,5) - 6*s*Power(t2,5) + Power(s,2)*Power(t2,5) + 
         Power(s,3)*Power(t2,5) + 9*s2*Power(t2,5) - 
         4*Power(s,2)*s2*Power(t2,5) - Power(s2,2)*Power(t2,5) + 
         5*s*Power(s2,2)*Power(t2,5) - 2*Power(s2,3)*Power(t2,5) - 
         Power(t2,6) + s*Power(t2,6) - s2*Power(t2,6) - 
         Power(-1 + s,2)*Power(t1,5)*(-1 + s + t2) + 
         Power(t1,4)*(-6 + (9 - 4*s2)*t2 + 4*(-1 + s2)*Power(t2,2) + 
            Power(s,3)*(3 + 2*t2) - 
            4*Power(s,2)*(3 + s2*t2 - Power(t2,2)) + 
            s*(15 + (-11 + 8*s2)*t2 - 2*(1 + 2*s2)*Power(t2,2))) + 
         Power(t1,3)*(14 + (-19 + 17*s2)*t2 + 
            (9 - 22*s2 + 5*Power(s2,2))*Power(t2,2) + 
            (12 - 2*s2 - 3*Power(s2,2))*Power(t2,3) - 
            Power(s,3)*(2 + 3*t2 + Power(t2,2)) + 
            Power(s,2)*(15 + 12*(1 + s2)*t2 + (-12 + 7*s2)*Power(t2,2) - 
               6*Power(t2,3)) + 
            s*(-27 + (6 - 29*s2)*t2 + 
               (22 + 8*s2 - 5*Power(s2,2))*Power(t2,2) + 
               2*(-1 + 5*s2)*Power(t2,3))) + 
         Power(t1,2)*(-16 + (13 - 27*s2)*t2 + 
            (-8 + 24*s2 - 13*Power(s2,2))*Power(t2,2) + 
            (-28 + 15*s2 + 8*Power(s2,2) - 2*Power(s2,3))*Power(t2,3) + 
            (-11 - 2*s2 + 4*Power(s2,2))*Power(t2,4) + 
            Power(s,3)*t2*(1 - 5*t2 + Power(t2,2)) + 
            Power(s,2)*(-6 - 7*(1 + 2*s2)*t2 - (1 + 2*s2)*Power(t2,2) + 
               (13 - 6*s2)*Power(t2,3) + 4*Power(t2,4)) + 
            s*(21 + 8*(1 + 5*s2)*t2 + 
               (10 - 5*s2 + 13*Power(s2,2))*Power(t2,2) + 
               (-24 - 19*s2 + 8*Power(s2,2))*Power(t2,3) + 
               (3 - 8*s2)*Power(t2,4))) + 
         Power(s1,6)*(Power(s2,3)*(-1 + 3*t1 - 4*t2) + 
            Power(s2,2)*(-3 + 2*Power(t1,2) + t1*(2 + s - t2) + 3*s*t2) - 
            s2*t1*(-5*Power(t1,2) + s*(3 + t1 - 3*t2) + 2*t2 + 
               3*t1*(2 + t2)) + 
            Power(t1,2)*(-1 + 6*Power(t1,2) - 6*t2 - t1*(11 + 4*t2) + 
               s*(-3 - 4*t1 + 4*t2))) + 
         t1*(9 + 5*Power(t2,2) - 11*Power(t2,3) + 24*Power(t2,4) + 
            5*Power(t2,5) + Power(s2,3)*Power(t2,3)*(2 + 3*t2) + 
            Power(s,3)*Power(t2,2)*(2 + 7*t2 - 2*Power(t2,2)) - 
            Power(s2,2)*Power(t2,2)*
             (-11 - 2*t2 + 4*Power(t2,2) + Power(t2,3)) + 
            s2*t2*(19 + 2*t2 + 11*Power(t2,2) - 13*Power(t2,3) + 
               Power(t2,4)) - 
            Power(s,2)*t2*(4 - 7*t2 + 5*Power(t2,2) + 5*Power(t2,3) + 
               Power(t2,4) + s2*
                (-6 + 10*t2 + 14*Power(t2,2) - 7*Power(t2,3))) - 
            s*(6 + 35*Power(t2,2) + 11*Power(t2,3) - 22*Power(t2,4) + 
               2*Power(t2,5) + 
               Power(s2,2)*Power(t2,2)*(12 + t2 + 8*Power(t2,2)) + 
               s2*t2*(25 - 2*t2 + 2*Power(t2,2) - 3*Power(t2,3) - 
                  2*Power(t2,4)))) + 
         Power(s1,5)*(Power(s2,3)*
             (-3 + t1 + 3*Power(t1,2) + 2*t2 - 11*t1*t2 + 6*Power(t2,2)) \
+ Power(s2,2)*(3 + 6*Power(t1,3) + Power(t1,2)*(-2 + 3*s - 8*t2) + 
               (10 - 7*s)*t2 + (1 - 9*s)*Power(t2,2) + 
               t1*(-13 - 11*t2 + 3*Power(t2,2) + s*(-5 + 9*t2))) + 
            s2*(3 + 9*Power(t1,4) + Power(t2,2) + 
               3*Power(s,2)*Power(t2,2) - 3*s*t2*(1 + t2) - 
               Power(t1,3)*(14 + 5*s + 12*t2) + 
               Power(t1,2)*(2 + 4*t2 + 3*Power(t2,2) + s*(-4 + 17*t2)) + 
               t1*(-4 + 14*t2 - Power(s,2)*t2 + 6*Power(t2,2) + 
                  s*(4 + 6*t2 - 8*Power(t2,2)))) + 
            t1*(6*Power(t1,4) + 2*t2*(1 + 3*t2) - 
               Power(t1,3)*(27 + 7*t2) + 
               Power(s,2)*(t1 + 2*Power(t1,2) - 5*t1*t2 + 
                  3*(-1 + t2)*t2) + 
               Power(t1,2)*(10 - t2 + 2*Power(t2,2)) + 
               6*t1*(1 + 6*t2 + 2*Power(t2,2)) + 
               s*(3 - 12*Power(t1,3) + 3*t2 - 8*Power(t2,2) + 
                  Power(t1,2)*(-1 + 20*t2) + t1*(6 + 5*t2 - 7*Power(t2,2))\
))) + Power(s1,4)*(-1 + 2*Power(t1,6) - Power(t2,2) - 2*Power(t2,3) + 
            4*s*Power(t2,3) - 3*Power(s,2)*Power(t2,3) + 
            Power(s,3)*Power(t2,3) - Power(t1,5)*(23 + 12*s + 3*t2) + 
            Power(t1,4)*(27 + 6*Power(s,2) + 4*t2 + Power(t2,2) + 
               s*(7 + 25*t2)) + 
            Power(t1,3)*(3 + Power(s,2)*(2 - 19*t2) + 73*t2 + 
               14*Power(t2,2) + s*(35 + 4*t2 - 16*Power(t2,2))) + 
            Power(s2,3)*(1 + Power(t1,3) + Power(t1,2)*(4 - 10*t2) + 
               12*t2 + Power(t2,2) - 4*Power(t2,3) + 
               t1*(-3 - 8*t2 + 15*Power(t2,2))) - 
            t1*(-2 + 13*t2 + 39*Power(t2,2) + 2*Power(s,3)*Power(t2,2) + 
               12*Power(t2,3) + 
               2*Power(s,2)*t2*(-4 - 3*t2 + 2*Power(t2,2)) + 
               s*(5 + 16*t2 - 10*Power(t2,2) - 14*Power(t2,3))) + 
            Power(t1,2)*(-4 - 49*t2 + Power(s,3)*t2 - 36*Power(t2,2) - 
               6*Power(t2,3) + Power(s,2)*(6 - 11*t2 + 17*Power(t2,2)) + 
               s*(16 - 22*t2 - 39*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s2,2)*(9 + 6*Power(t1,4) - (2 + s)*t2 + 
               3*(-3 + 4*s)*Power(t2,2) + (-3 + 9*s)*Power(t2,3) + 
               Power(t1,2)*(-14 - 9*s - 6*t2 + 10*s*t2 + 
                  10*Power(t2,2)) - 
               t1*(10 + s - 46*t2 - s*t2 - 25*Power(t2,2) + 
                  27*s*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,3)*(3*s - 2*(5 + 8*t2))) + 
            s2*(-3 + 7*Power(t1,5) + 8*(-1 + s)*t2 + 
               (-8 + 13*s - 8*Power(s,2))*Power(t2,2) + 
               (-3 + 8*s - 6*Power(s,2))*Power(t2,3) + 
               Power(t1,3)*(4 - 6*s + 2*t2 + 37*s*t2 + 8*Power(t2,2)) - 
               Power(t1,4)*(9*s + 13*(1 + t2)) - 
               Power(t1,2)*(-2 - 35*t2 - 14*Power(t2,2) + Power(t2,3) + 
                  Power(s,2)*(2 + 4*t2) + s*(4 + 30*Power(t2,2))) + 
               t1*(17 + 10*t2 - 29*Power(t2,2) - 6*Power(t2,3) + 
                  2*Power(s,2)*t2*(-3 + 7*t2) + 
                  s*(16 - 43*t2 - 30*Power(t2,2) + 7*Power(t2,3))))) + 
         Power(s1,2)*(3 + 2*(-2 + Power(s,2))*Power(t1,6) + 2*t2 + 
            5*s*t2 - 3*Power(t2,2) - 5*s*Power(t2,2) - 32*Power(t2,3) - 
            11*s*Power(t2,3) + 6*Power(s,2)*Power(t2,3) + 
            2*Power(s,3)*Power(t2,3) - 20*Power(t2,4) + 8*s*Power(t2,4) + 
            3*Power(s,2)*Power(t2,4) - 2*Power(t2,5) + 3*s*Power(t2,5) - 
            Power(s,2)*Power(t2,5) + 
            Power(t1,5)*(9 + 21*t2 - 2*Power(s,2)*(1 + 3*t2) + 
               s*(6 + 8*t2)) + 
            Power(t1,4)*(-9 + 2*Power(s,3)*(-1 + t2) - 47*t2 - 
               19*Power(t2,2) + 3*Power(s,2)*t2*(-3 + 2*t2) + 
               s*(28 - 47*t2 - 17*Power(t2,2))) + 
            Power(s2,3)*t2*(-5 + 2*t2 + 8*Power(t2,2) + 4*Power(t2,3) + 
               Power(t1,3)*(-3 + 2*t2) + 
               Power(t1,2)*(-5 + 13*t2 - 4*Power(t2,2)) + 
               t1*(13 + t2 - 20*Power(t2,2) + 2*Power(t2,3))) + 
            Power(t1,3)*(5 - 38*t2 - 6*Power(s,3)*(-1 + t2)*t2 - 
               22*Power(t2,2) + 2*Power(t2,3) - 
               2*Power(s,2)*(1 - 5*t2 - 19*Power(t2,2) + Power(t2,3)) + 
               2*s*(-10 - 52*t2 + 24*Power(t2,2) + 5*Power(t2,3))) + 
            Power(t1,2)*(4 + 42*t2 + 92*Power(t2,2) + 46*Power(t2,3) + 
               2*Power(s,3)*t2*(-2 - 3*t2 + 3*Power(t2,2)) - 
               Power(s,2)*(2 + 35*Power(t2,2) + 42*Power(t2,3)) - 
               s*(25 + 35*t2 - 112*Power(t2,2) - 10*Power(t2,3) + 
                  Power(t2,4))) + 
            t1*(2*Power(s,3)*Power(t2,2)*(1 + t2 - Power(t2,2)) + 
               2*Power(s,2)*t2*
                (-4 - 11*t2 + 11*Power(t2,2) + 8*Power(t2,3)) + 
               s*(11 + 35*t2 + 6*Power(t2,2) - 92*Power(t2,3) - 
                  20*Power(t2,4)) - 
               4*(2 - 5*t2 + 3*Power(t2,2) + 11*Power(t2,3) + 
                  4*Power(t2,4))) - 
            Power(s2,2)*(6 + 2*(-1 + s)*t2 + 2*Power(t1,5)*t2 + 
               (-20 + 34*s)*Power(t2,2) + (-23 + 18*s)*Power(t2,3) + 
               4*(-3 + 2*s)*Power(t2,4) + Power(t2,5) + 
               Power(t1,4)*(-3 + s + 3*t2 - 2*s*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(13 + (-16 + 9*s)*t2 + 
                  (42 + 23*s)*Power(t2,2) + (22 - 14*s)*Power(t2,3)) + 
               t1*(-17 + 2*(14 + 9*s)*t2 - 2*(-59 + 8*s)*Power(t2,2) - 
                  (13 + 33*s)*Power(t2,3) + (-9 + 6*s)*Power(t2,4)) + 
               Power(t1,3)*(1 - 15*t2 - 22*Power(t2,2) + 2*Power(t2,3) + 
                  s*(-1 - 9*t2 + 10*Power(t2,2)))) + 
            s2*(3 - 2*(-1 + s)*Power(t1,6) + (22 - 14*s)*t2 + 
               (23 - 20*s + 2*Power(s,2))*Power(t2,2) + 
               (13 - 54*s + 3*Power(s,2))*Power(t2,3) + 
               4*(-4 - 4*s + Power(s,2))*Power(t2,4) + 
               (-1 + 2*s)*Power(t2,5) + 
               Power(t1,5)*(-5 - 6*t2 + s*(3 + 8*t2)) + 
               Power(t1,4)*(-7 + Power(s,2)*(7 - 4*t2) + 27*t2 + 
                  8*Power(t2,2) + s*(-11 + 13*t2 - 10*Power(t2,2))) + 
               Power(t1,3)*(10 + 41*t2 - 38*Power(t2,2) - 
                  5*Power(t2,3) + 
                  Power(s,2)*(-8 - 15*t2 + 14*Power(t2,2)) + 
                  s*(8 + 9*t2 - 63*Power(t2,2) + 4*Power(t2,3))) + 
               Power(t1,2)*(14 - 12*t2 - 12*Power(t2,2) + 
                  4*Power(t2,3) + Power(t2,4) + 
                  Power(s,2)*t2*(21 + 19*t2 - 16*Power(t2,2)) + 
                  s*(15 + 24*t2 + 70*Power(t2,2) + 70*Power(t2,3))) + 
               t1*(-17 - 72*t2 + 103*Power(t2,2) + 64*Power(t2,3) + 
                  Power(s,2)*t2*
                   (-4 - 10*t2 - 15*Power(t2,2) + 6*Power(t2,3)) + 
                  s*(-13 + 47*t2 + 56*Power(t2,2) - 48*Power(t2,3) - 
                     25*Power(t2,4))))) + 
         s1*(-1 + Power(-1 + s,2)*Power(t1,6) - 5*t2 + s*t2 - 
            10*Power(t2,2) - 6*s*Power(t2,2) + 2*Power(s,2)*Power(t2,2) + 
            7*Power(t2,3) + 23*s*Power(t2,3) - 2*Power(s,2)*Power(t2,3) + 
            33*Power(t2,4) + 29*s*Power(t2,4) - 
            18*Power(s,2)*Power(t2,4) + 8*Power(t2,5) + s*Power(t2,5) - 
            6*Power(s,2)*Power(t2,5) + Power(s,3)*Power(t2,5) + 
            Power(t1,5)*(8*(-1 + t2) - 3*Power(s,2)*(1 + 2*t2) + 
               s*(11 + 2*t2)) - 
            Power(s2,3)*Power(t2,2)*
             (-3 - Power(t1,3) + t2 - 2*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(-7 + 6*t2) + t1*(11 + 5*t2 - 7*Power(t2,2))) \
+ Power(t1,4)*(14 + 2*Power(s,3)*(-2 + t2) - 19*t2 - 28*Power(t2,2) - 
               s*(9 + 22*t2 + 2*Power(t2,2)) + 
               Power(s,2)*(3 + 8*t2 + 12*Power(t2,2))) + 
            Power(t1,3)*(-3 + 24*t2 + 61*Power(t2,2) + 27*Power(t2,3) + 
               Power(s,3)*(4 + 6*t2 - 7*Power(t2,2)) - 
               2*Power(s,2)*(7 - 11*t2 + 2*Power(t2,2) + 
                  5*Power(t2,3)) + 
               s*(1 - 68*t2 + 62*Power(t2,2) + 5*Power(t2,3))) + 
            Power(t1,2)*(-10 - 28*t2 + 45*Power(t2,2) - 37*Power(t2,3) - 
               10*Power(t2,4) + Power(s,3)*t2*(-2 + 9*Power(t2,2)) + 
               s*(-9 + 109*t2 + 83*Power(t2,2) - 70*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s,2)*(13 - 29*t2 - 35*Power(t2,2) - 
                  10*Power(t2,3) + 3*Power(t2,4))) + 
            t1*(7 + 20*t2 - 68*Power(t2,2) - 21*Power(t2,3) + 
               4*Power(t2,4) + 2*Power(t2,5) - 
               Power(s,3)*Power(t2,2)*(2 + 2*t2 + 5*Power(t2,2)) + 
               Power(s,2)*t2*
                (13 + 51*t2 + 28*Power(t2,2) + 15*Power(t2,3)) + 
               s*(8 - 22*t2 + Power(t2,2) - 20*Power(t2,3) + 
                  18*Power(t2,4) - Power(t2,5))) + 
            Power(s2,2)*t2*(10 + 6*t2 + 18*Power(t2,2) - 9*Power(t2,3) - 
               5*Power(t2,4) + Power(t1,4)*(-2 + 5*t2) - 
               4*Power(t1,3)*(2 + t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(32 - 28*t2 - 10*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(-32 + 21*t2 + 76*Power(t2,2) + 8*Power(t2,3)) + 
               s*(-6 + 2*Power(t1,4) + Power(t1,3)*(3 - 10*t2) + 8*t2 + 
                  24*Power(t2,2) + 3*Power(t2,4) + 
                  4*Power(t1,2)*(-4 - 3*t2 + 6*Power(t2,2)) + 
                  t1*(17 + 32*t2 - 11*Power(t2,2) - 19*Power(t2,3)))) + 
            s2*(6 + (2 + 7*s)*t2 + 
               (-1 + 16*s - 7*Power(s,2))*Power(t2,2) + 
               (-44 + 13*s - 17*Power(s,2))*Power(t2,3) + 
               (-24 + 41*s - 2*Power(s,2))*Power(t2,4) + 
               (5 + 11*s - 3*Power(s,2))*Power(t2,5) + 
               (-1 + s)*Power(t1,5)*(-1 + s + 6*t2) - 
               Power(t1,4)*(-1 + s - 24*t2 + 11*s*t2 + 4*Power(s,2)*t2 - 
                  6*Power(t2,2) + 18*s*Power(t2,2)) + 
               Power(t1,3)*(-15 - 2*t2 - 32*Power(t2,2) - 
                  2*Power(t2,3) + 
                  Power(s,2)*(-1 - 20*t2 + 16*Power(t2,2)) + 
                  s*(14 + 22*t2 + Power(t2,2) + 18*Power(t2,3))) + 
               t1*(-22 + 24*t2 + 87*Power(t2,2) - 61*Power(t2,3) - 
                  21*Power(t2,4) + Power(t2,5) + 
                  Power(s,2)*t2*
                   (-13 - 19*t2 + 14*Power(t2,2) + 17*Power(t2,3)) + 
                  s*(6 + 4*t2 - 149*Power(t2,2) - 99*Power(t2,3) - 
                     28*Power(t2,4))) + 
               Power(t1,2)*(29 - 42*t2 - 60*Power(t2,2) + 
                  51*Power(t2,3) + Power(t2,4) + 
                  Power(s,2)*t2*(39 + 8*t2 - 27*Power(t2,2)) + 
                  s*(-17 - 28*t2 + 63*Power(t2,2) + 29*Power(t2,3) - 
                     6*Power(t2,4))))) + 
         Power(s1,3)*(1 - (5 + 4*s)*Power(t1,6) + 2*t2 - s*t2 + 
            7*Power(t2,2) + s*Power(t2,2) + 14*Power(t2,3) - 
            11*s*Power(t2,3) + 5*Power(s,2)*Power(t2,3) - 
            2*Power(s,3)*Power(t2,3) + 4*Power(t2,4) - 7*s*Power(t2,4) + 
            4*Power(s,2)*Power(t2,4) - Power(s,3)*Power(t2,4) + 
            Power(t1,5)*(13 + 6*Power(s,2) + t2 + 9*s*(1 + t2)) + 
            Power(t1,4)*(9 + 58*t2 - 20*Power(s,2)*t2 + 6*Power(t2,2) + 
               s*(38 + 9*t2 - 6*Power(t2,2))) + 
            Power(s2,3)*(2 + Power(t1,3)*(2 - 3*t2) - 2*t2 + 
               11*Power(t1,2)*(-1 + t2)*t2 - 17*Power(t2,2) - 
               5*Power(t2,3) + Power(t2,4) + 
               t1*(-4 + 4*t2 + 20*Power(t2,2) - 9*Power(t2,3))) + 
            Power(t1,3)*(-11 - 92*t2 - 47*Power(t2,2) - 2*Power(t2,3) + 
               Power(s,3)*(-2 + 3*t2) + 
               Power(s,2)*(6 - 11*t2 + 23*Power(t2,2)) + 
               s*(25 - 66*t2 - 47*Power(t2,2) + Power(t2,3))) + 
            Power(t1,2)*(Power(s,3)*(4 - 7*t2)*t2 + 
               t2*(6 - 35*t2 + Power(t2,2)) + 
               Power(s,2)*(-12 + 25*t2 + 30*Power(t2,2) - 
                  10*Power(t2,3)) + 
               s*(-11 - 91*t2 + 68*Power(t2,2) + 35*Power(t2,3))) + 
            t1*(-7 + t2 + 71*Power(t2,2) + 51*Power(t2,3) + 
               5*Power(s,3)*Power(t2,3) + 6*Power(t2,4) + 
               Power(s,2)*t2*
                (-6 - 18*t2 - 23*Power(t2,2) + Power(t2,3)) + 
               s*(-11 + 10*t2 + 43*Power(t2,2) + 11*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s2,2)*(-3 + 2*Power(t1,5) + 
               Power(t1,4)*(-4 + s - 11*t2) + (-29 + 13*s)*t2 + 
               14*(-1 + s)*Power(t2,2) - 5*Power(t2,3) - 
               3*(-1 + s)*Power(t2,4) + 
               Power(t1,2)*(-8 + s + 56*t2 + 11*s*t2 + 27*Power(t2,2) - 
                  27*s*Power(t2,2) - 4*Power(t2,3)) + 
               Power(t1,3)*(12*Power(t2,2) + s*(-1 + 6*t2)) + 
               t1*(13 + 60*t2 - 53*Power(t2,2) - 25*Power(t2,3) + 
                  Power(t2,4) + 
                  s*(5 + t2 - 10*Power(t2,2) + 23*Power(t2,3)))) + 
            s2*(-9 + 2*Power(t1,6) - 2*(1 + 2*s)*t2 + 
               (6 + 5*s + 6*Power(s,2))*Power(t2,2) + 
               (20 - 5*s + 7*Power(s,2))*Power(t2,3) + 
               (3 - 7*s + 3*Power(s,2))*Power(t2,4) - 
               Power(t1,5)*(7 + 7*s + 4*t2) + 
               Power(t1,4)*(-13 - 4*t2 + 3*Power(t2,2) + 
                  s*(-10 + 31*t2)) - 
               Power(t1,3)*(-3 - 42*t2 + 7*Power(s,2)*t2 - 
                  21*Power(t2,2) + Power(t2,3) + 
                  s*(23 - 7*t2 + 36*Power(t2,2))) + 
               Power(t1,2)*(7 - 33*t2 - 48*Power(t2,2) - 13*Power(t2,3) + 
                  Power(s,2)*(2 - 15*t2 + 23*Power(t2,2)) + 
                  s*(21 - 27*t2 - 60*Power(t2,2) + 14*Power(t2,3))) + 
               t1*(17 - 55*t2 - 46*Power(t2,2) + 14*Power(t2,3) + 
                  2*Power(t2,4) + 
                  Power(s,2)*t2*(18 - 2*t2 - 19*Power(t2,2)) + 
                  s*(-10 - 12*t2 + 113*Power(t2,2) + 52*Power(t2,3) - 
                     2*Power(t2,4))))))*T5q(1 - s1 - t1 + t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,3)*
       (-1 + t2)));
   return a;
};
