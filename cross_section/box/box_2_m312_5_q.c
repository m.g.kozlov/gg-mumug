#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m312_5_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(Power(s,5)*(-1 + s2)*s2*(-4 + 9*t1 - 5*t2) + 
         Power(s,4)*(2*(-1 + t1 - t2) + Power(s2,3)*(6 - 19*t1 + 18*t2) + 
            s2*(-5 - 25*Power(t1,2) + 6*t2 + 9*t1*(4 + t2)) + 
            s1*(2 - 5*Power(s2,3) - s2*(22 + t1 - 2*t2) + 
               Power(s2,2)*(25 - 7*t1 + 6*t2)) + 
            Power(s2,2)*(1 + 25*Power(t1,2) - 30*t2 - t1*(11 + 9*t2))) + 
         Power(s,3)*(2*Power(s1,2)*s2*
             (2 + 3*Power(s2,2) + 5*t1 - s2*(9 + t1)) + 
            Power(s2,4)*(2 + 10*t1 - 21*t2) + 
            2*(-4 + 3*t1)*(-1 + t1 - t2) + 
            Power(s2,3)*(-37 - 28*Power(t1,2) + 29*t2 + 
               t1*(47 + 35*t2)) + 
            s1*(-8 + 9*Power(s2,4) + 6*t1 - 4*Power(s2,3)*(9 + 4*t2) + 
               s2*(104 - 73*t1 - 9*Power(t1,2) - 24*t2 + 2*t1*t2) + 
               Power(s2,2)*(-53 + 99*t1 - 15*Power(t1,2) - 8*t2 + 
                  22*t1*t2)) + 
            s2*(-45 - 25*Power(t1,3) - 28*t2 + 8*Power(t2,2) + 
               5*Power(t1,2)*(17 + t2) + t1*(-47 + 37*t2 - 4*Power(t2,2))\
) + Power(s2,2)*(64 + 25*Power(t1,3) + 60*t2 + 8*Power(t2,2) - 
               Power(t1,2)*(23 + 5*t2) + 
               2*t1*(-18 - 61*t2 + 2*Power(t2,2)))) - 
         2*Power(-1 + t1,2)*(Power(s1,2)*s2*
             (-2 + 5*Power(s2,3) + 9*t1 - 7*Power(t1,2) - 
               2*Power(s2,2)*(1 + 4*t1) + s2*(-9 + 11*t1 + 3*Power(t1,2))\
) + Power(s2,5)*(2 - 8*t2) + (-1 + t1 + Power(t1,2))*(-1 + t1 - t2) + 
            Power(s2,4)*(-4 + 15*(2 + t1)*t2 - 2*Power(t2,2)) + 
            Power(s2,2)*(-2 + Power(t1,3) - 24*t2 + 10*Power(t2,2) + 
               4*Power(t1,2)*(1 + 8*t2) + 
               t1*(-19 + 8*t2 - 14*Power(t2,2))) + 
            Power(s2,3)*(7 + 5*t2 + 12*Power(t2,2) - 
               Power(t1,2)*(3 + 7*t2) + t1*(14 - 63*t2 + 2*Power(t2,2))) \
+ s2*(-10*Power(t1,3) + Power(t1,2)*(19 - 8*t2) - 
               4*(3 - 3*t2 + Power(t2,2)) + t1*(3 + t2 + 4*Power(t2,2))) \
+ s1*(-1 + 6*Power(s2,5) + t1 + Power(t1,2) - 
               Power(s2,4)*(9*t1 + 5*(6 + t2)) + 
               Power(s2,2)*(40 + Power(t1,3) - 3*t2 - 
                  2*Power(t1,2)*(10 + 3*t2) + t1*(-19 + 6*t2)) + 
               s2*(13 - Power(t1,3) + 3*t2 + Power(t1,2)*(17 + 6*t2) - 
                  t1*(34 + 9*t2)) + 
               Power(s2,3)*(-12 + 2*Power(t1,2) - 11*t2 + t1*(53 + 11*t2))\
)) - s*(-1 + t1)*(2*Power(s1,2)*s2*
             (-2 + 8*Power(s2,3) + 21*t1 - 19*Power(t1,2) - 
               Power(s2,2)*(3 + 17*t1) + s2*(-23 + 28*t1 + 7*Power(t1,2))\
) + 2*(-4 + 5*t1 + Power(t1,2))*(-1 + t1 - t2) - 
            4*Power(s2,5)*(-5 + 3*t1 + 3*t2) + 
            Power(s2,4)*(-32 + 20*Power(t1,2) + 61*t2 - 4*Power(t2,2) + 
               t1*(-4 + 47*t2)) + 
            s2*(-81 + 2*Power(t1,4) + 57*t2 - 8*Power(t2,2) - 
               Power(t1,3)*(71 + 2*t2) + 
               Power(t1,2)*(135 - 43*t2 + 4*Power(t2,2)) + 
               t1*(15 + 4*t2 + 4*Power(t2,2))) + 
            Power(s2,3)*(5 - 6*Power(t1,3) + 91*t2 + 28*Power(t2,2) - 
               5*Power(t1,2)*(9 + 7*t2) + 
               2*t1*(59 - 126*t2 + 6*Power(t2,2))) + 
            Power(s2,2)*(40 - 2*Power(t1,4) - 109*t2 + 64*Power(t2,2) + 
               Power(t1,3)*(19 + 2*t2) + 
               Power(t1,2)*(10 + 184*t2 - 4*Power(t2,2)) - 
               t1*(131 + 21*t2 + 64*Power(t2,2))) + 
            s1*(4*Power(s2,5) + 2*(-4 + 5*t1 + Power(t1,2)) - 
               Power(s2,4)*(65 + 19*t1 + 20*t2) + 
               2*Power(s2,3)*(-58 + 109*t1 + 3*Power(t1,2) - 18*t2 + 
                  26*t1*t2) + 
               s2*(102 + 3*Power(t1,3) - 8*t2 - 3*t1*(71 + 6*t2) + 
                  Power(t1,2)*(92 + 26*t2)) + 
               Power(s2,2)*(163 + 5*Power(t1,3) - 32*t2 - 
                  2*Power(t1,2)*(62 + 17*t2) + t1*(-28 + 38*t2)))) - 
         Power(s,2)*(2*Power(s1,2)*s2*
             (2 + 3*Power(s2,3) + Power(s2,2)*(2 - 12*t1) + 15*t1 - 
               17*Power(t1,2) + s2*(-23 + 25*t1 + 5*Power(t1,2))) + 
            Power(s2,5)*(4 - 8*t2) - 
            2*(6 - 9*t1 + 2*Power(t1,2))*(-1 + t1 - t2) + 
            2*Power(s2,4)*(-5 + 5*Power(t1,2) - 4*t2 + t1*(-4 + 19*t2)) + 
            s2*(-103 + 11*Power(t1,4) + 16*t2 + 8*Power(t2,2) - 
               Power(t1,3)*(106 + 3*t2) + 
               t1*(32 + 49*t2 - 16*Power(t2,2)) + 
               2*Power(t1,2)*(83 - 28*t2 + 4*Power(t2,2))) + 
            Power(s2,3)*(3*Power(t1,3) - 2*Power(t1,2)*(35 + 19*t2) + 
               4*(-7 + 35*t2 + Power(t2,2)) + 
               t1*(131 - 156*t2 + 8*Power(t2,2))) + 
            Power(s2,2)*(93 - 11*Power(t1,4) - 48*t2 + 52*Power(t2,2) + 
               Power(t1,3)*(35 + 3*t2) + 
               Power(t1,2)*(24 + 210*t2 - 8*Power(t2,2)) - 
               t1*(173 + 153*t2 + 40*Power(t2,2))) + 
            s1*(4*Power(s2,5) - 2*(6 - 9*t1 + 2*Power(t1,2)) - 
               2*Power(s2,4)*(4 + 5*t1 + 5*t2) + 
               s2*(158 + 13*Power(t1,3) - 36*t2 + 
                  2*Power(t1,2)*(51 + 7*t2) + t1*(-279 + 22*t2)) + 
               Power(s2,3)*(-3*Power(t1,2) - 5*(29 + 6*t2) + 
                  2*t1*(79 + 23*t2)) + 
               Power(s2,2)*(67 + 11*Power(t1,3) - 28*t2 - 
                  Power(t1,2)*(159 + 38*t2) + t1*(105 + 44*t2))))))/
     (Power(-1 + s2,2)*s2*(-s + s2 - t1)*(1 - s + s2 - t1)*(-1 + t1)*
       Power(-1 + s + t1,2)*(-1 + s1 + t1 - t2)) - 
    (8*(21 - 2*s2 + 19*Power(s2,2) + 2*Power(s2,3) - 2*Power(s2,4) - 
         68*t1 + 3*s2*t1 - 65*Power(s2,2)*t1 - 2*Power(s2,3)*t1 + 
         6*Power(s2,4)*t1 + 78*Power(t1,2) + 10*s2*Power(t1,2) + 
         80*Power(s2,2)*Power(t1,2) - 6*Power(s2,3)*Power(t1,2) - 
         6*Power(s2,4)*Power(t1,2) - 36*Power(t1,3) - 28*s2*Power(t1,3) - 
         40*Power(s2,2)*Power(t1,3) + 10*Power(s2,3)*Power(t1,3) + 
         2*Power(s2,4)*Power(t1,3) + 5*Power(t1,4) + 24*s2*Power(t1,4) + 
         5*Power(s2,2)*Power(t1,4) - 4*Power(s2,3)*Power(t1,4) - 
         7*s2*Power(t1,5) + Power(s2,2)*Power(t1,5) + 
         Power(s1,5)*(-1 + s2)*s2*(Power(s2,2) - 2*t1 - s2*(2 + t1)) - 
         2*Power(s,4)*(-1 + s2)*(t1 - t2)*
          (-1 + s1*(1 + s2) + t1 + s2*(-1 + 3*t1 - 3*t2) - t2) - 2*t2 + 
         60*s2*t2 + 12*Power(s2,2)*t2 + 46*Power(s2,3)*t2 + 
         2*Power(s2,4)*t2 - 4*Power(s2,5)*t2 + 42*t1*t2 - 122*s2*t1*t2 - 
         52*Power(s2,2)*t1*t2 - 100*Power(s2,3)*t1*t2 + 
         4*Power(s2,4)*t1*t2 + 8*Power(s2,5)*t1*t2 - 96*Power(t1,2)*t2 + 
         78*s2*Power(t1,2)*t2 + 80*Power(s2,2)*Power(t1,2)*t2 + 
         60*Power(s2,3)*Power(t1,2)*t2 - 14*Power(s2,4)*Power(t1,2)*t2 - 
         4*Power(s2,5)*Power(t1,2)*t2 + 74*Power(t1,3)*t2 - 
         30*s2*Power(t1,3)*t2 - 52*Power(s2,2)*Power(t1,3)*t2 - 
         4*Power(s2,3)*Power(t1,3)*t2 + 8*Power(s2,4)*Power(t1,3)*t2 - 
         18*Power(t1,4)*t2 + 14*s2*Power(t1,4)*t2 + 
         12*Power(s2,2)*Power(t1,4)*t2 - 2*Power(s2,3)*Power(t1,4)*t2 - 
         16*Power(t2,2) - 17*s2*Power(t2,2) + 
         57*Power(s2,2)*Power(t2,2) + 20*Power(s2,3)*Power(t2,2) + 
         30*Power(s2,4)*Power(t2,2) - 2*Power(s2,5)*Power(t2,2) - 
         2*Power(s2,6)*Power(t2,2) + 69*t1*Power(t2,2) + 
         84*s2*t1*Power(t2,2) - 48*Power(s2,2)*t1*Power(t2,2) - 
         54*Power(s2,3)*t1*Power(t2,2) - 25*Power(s2,4)*t1*Power(t2,2) + 
         6*Power(s2,5)*t1*Power(t2,2) + 2*Power(s2,6)*t1*Power(t2,2) - 
         74*Power(t1,2)*Power(t2,2) - 93*s2*Power(t1,2)*Power(t2,2) + 
         21*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         38*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         6*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         4*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         21*Power(t1,3)*Power(t2,2) + 26*s2*Power(t1,3)*Power(t2,2) - 
         30*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         4*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         Power(s2,4)*Power(t1,3)*Power(t2,2) - 10*Power(t2,3) - 
         37*s2*Power(t2,3) - 22*Power(s2,2)*Power(t2,3) + 
         16*Power(s2,3)*Power(t2,3) + 4*Power(s2,4)*Power(t2,3) + 
         5*Power(s2,5)*Power(t2,3) - 2*Power(s2,6)*Power(t2,3) + 
         14*t1*Power(t2,3) + 93*s2*t1*Power(t2,3) + 
         25*Power(s2,2)*t1*Power(t2,3) + 4*Power(s2,3)*t1*Power(t2,3) - 
         9*Power(s2,4)*t1*Power(t2,3) + 5*Power(s2,5)*t1*Power(t2,3) - 
         6*Power(t1,2)*Power(t2,3) - 38*s2*Power(t1,2)*Power(t2,3) + 
         7*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         16*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         Power(s2,4)*Power(t1,2)*Power(t2,3) - Power(t2,4) - 
         9*s2*Power(t2,4) - 20*Power(s2,2)*Power(t2,4) - 
         11*Power(s2,3)*Power(t2,4) + 2*Power(s2,4)*Power(t2,4) - 
         Power(s2,5)*Power(t2,4) + 3*t1*Power(t2,4) + 
         8*s2*t1*Power(t2,4) + 12*Power(s2,2)*t1*Power(t2,4) - 
         15*Power(s2,3)*t1*Power(t2,4) - 3*s2*Power(t2,5) - 
         2*Power(s2,2)*Power(t2,5) + 5*Power(s2,3)*Power(t2,5) + 
         Power(s1,4)*(Power(s2,5) + 2*t1*(-1 + 2*t1) - 
            Power(s2,3)*(23 + Power(t1,2) + t1*(6 - 5*t2) - 16*t2) - 
            2*Power(s2,4)*(-2 + t1 + 2*t2) + 
            Power(s2,2)*(-4 - 5*t1 + Power(t1,2) - 10*t2 + 4*t1*t2) + 
            s2*(2*Power(t1,2) + t1*(3 - 9*t2) - 2*(2 + t2))) + 
         Power(s1,3)*(2*Power(s2,6) + 2*Power(t1,3) + t1*(-2 + t2) - 
            11*Power(t1,2)*t2 + 2*(1 + t2) - 
            Power(s2,5)*(7 + 3*t1 + 2*t2) + 
            Power(s2,4)*(-18 + 27*t1 - 3*Power(t1,2) - 16*t2 + 
               8*t1*t2 + 6*Power(t2,2)) + 
            Power(s2,3)*(10 + 83*t2 - 35*Power(t2,2) + 
               Power(t1,2)*(-22 + 4*t2) + 
               t1*(-24 + 29*t2 - 9*Power(t2,2))) + 
            Power(s2,2)*(34 + 8*Power(t1,3) + 36*t2 + 20*Power(t2,2) - 
               Power(t1,2)*(11 + t2) - t1*(41 + 3*t2 + 6*Power(t2,2))) + 
            s2*(17 + 4*Power(t1,3) + 19*t2 + 9*Power(t2,2) - 
               2*Power(t1,2)*(4 + 3*t2) + 
               t1*(-31 - 15*t2 + 15*Power(t2,2)))) + 
         Power(s1,2)*(-10 + Power(t1,3)*(1 - 5*t2) + 
            2*Power(s2,6)*(-1 + t1 - 3*t2) - 9*t2 - 5*Power(t2,2) + 
            t1*(37 + 7*t2 + 7*Power(t2,2)) + 
            Power(t1,2)*(-28 + t2 + 10*Power(t2,2)) + 
            Power(s2,5)*(-2 - 4*Power(t1,2) + 19*t2 + t1*(6 + 11*t2)) + 
            Power(s2,4)*(33 + 44*t2 + 22*Power(t2,2) - 4*Power(t2,3) + 
               Power(t1,2)*(-1 + 9*t2) - 
               t1*(32 + 71*t2 + 10*Power(t2,2))) + 
            s2*(-3 + 11*Power(t1,4) - 76*t2 - 35*Power(t2,2) - 
               15*Power(t2,3) - Power(t1,3)*(13 + 7*t2) + 
               Power(t1,2)*(-34 - 29*t2 + 6*Power(t2,2)) + 
               t1*(39 + 166*t2 + 29*Power(t2,2) - 11*Power(t2,3))) - 
            Power(s2,3)*(-31 + 7*t2 + 108*Power(t2,2) - 
               39*Power(t2,3) + Power(t1,3)*(7 + t2) + 
               Power(t1,2)*(-55 - 59*t2 + 5*Power(t2,2)) + 
               t1*(79 - 57*t2 + 55*Power(t2,2) - 7*Power(t2,3))) + 
            Power(s2,2)*(-11 + 3*Power(t1,4) - 95*t2 - 80*Power(t2,2) - 
               20*Power(t2,3) - Power(t1,3)*(39 + 11*t2) - 
               Power(t1,2)*(38 - 14*t2 + Power(t2,2)) + 
               t1*(85 + 122*t2 + 33*Power(t2,2) + 4*Power(t2,3)))) + 
         s1*(-9 + 2*Power(t1,5) + 24*t2 + 17*Power(t2,2) + 
            4*Power(t2,3) - Power(t1,4)*(5 + 2*t2) + 
            2*Power(s2,6)*t2*(2 - 2*t1 + 3*t2) + 
            Power(t1,3)*(-6 - 14*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(10 + 90*t2 + 5*Power(t2,2) - 3*Power(t2,3)) - 
            t1*(-8 + 98*t2 + 19*Power(t2,2) + 9*Power(t2,3)) + 
            Power(s2,5)*(4 + 4*t2 - 17*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(4 + 8*t2) - t1*(8 + 12*t2 + 13*Power(t2,2))) \
+ Power(s2,4)*(-2 + Power(t1,3)*(-8 + t2) - 65*t2 - 30*Power(t2,2) - 
               12*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(14 + t2 - 5*Power(t2,2)) + 
               t1*(-4 + 63*t2 + 53*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,2)*(-12 + Power(t1,5) - 37*t2 + 83*Power(t2,2) + 
               68*Power(t2,3) + 10*Power(t2,4) - 
               4*Power(t1,4)*(3 + t2) + 
               Power(t1,3)*(46 + 63*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-72 + 41*t2 - 10*Power(t2,2) + 
                  Power(t2,3)) - 
               t1*(-49 + 63*t2 + 106*Power(t2,2) + 37*Power(t2,3) + 
                  Power(t2,4))) - 
            Power(s2,3)*(45 + 54*t2 + 19*Power(t2,2) - 59*Power(t2,3) + 
               22*Power(t2,4) + Power(t1,4)*(1 + t2) - 
               Power(t1,3)*(12 + 17*t2 + Power(t2,2)) + 
               Power(t1,2)*(66 + 105*t2 + 53*Power(t2,2) - 
                  2*Power(t2,3)) + 
               t1*(-100 - 143*t2 + 37*Power(t2,2) - 47*Power(t2,3) + 
                  2*Power(t2,4))) + 
            s2*(-24 + 3*Power(t1,5) + 18*t2 + 96*Power(t2,2) + 
               29*Power(t2,3) + 11*Power(t2,4) - 
               Power(t1,4)*(18 + 7*t2) + 
               Power(t1,3)*(-12 - 23*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(42 + 133*t2 + 75*Power(t2,2) - 
                  2*Power(t2,3)) + 
               t1*(9 - 121*t2 - 228*Power(t2,2) - 25*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(s,3)*(-6 - 3*t1 + 3*Power(t1,2) + 2*Power(t1,3) + 13*t2 - 
            17*t1*t2 - 5*Power(t1,2)*t2 + 14*Power(t2,2) + 
            4*t1*Power(t2,2) - Power(t2,3) + 
            Power(s2,3)*(-4 + 20*t2 + 21*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(8 + t2) - t1*t2*(29 + 2*t2)) + 
            Power(s1,2)*(4*(t1 - t2) + 10*Power(s2,2)*(t1 - t2) + 
               Power(s2,3)*(2 + t1 + t2) + s2*(-2 - 11*t1 + 9*t2)) + 
            s2*(4 - 46*t2 - 51*Power(t2,2) + 11*Power(t2,3) + 
               Power(t1,2)*(-34 + 11*t2) + 
               t1*(42 + 85*t2 - 22*Power(t2,2))) + 
            Power(s2,2)*(6 - 2*Power(t1,3) + Power(t1,2)*(19 - 7*t2) + 
               9*t2 + 12*Power(t2,2) - 11*Power(t2,3) + 
               t1*(-35 - 31*t2 + 20*Power(t2,2))) + 
            s1*(6 + 5*t1 + 6*Power(t1,2) - 15*t2 - 11*t1*t2 + 
               5*Power(t2,2) + 
               Power(s2,3)*(2 + Power(t1,2) + t1*(-3 + t2) - 19*t2 - 
                  2*Power(t2,2)) - 
               s2*(2 + 27*Power(t1,2) + t1*(45 - 47*t2) - 51*t2 + 
                  20*Power(t2,2)) + 
               Power(s2,2)*(-6 + 24*Power(t1,2) - 9*t2 + 
                  21*Power(t2,2) - 5*t1*(-7 + 9*t2)))) + 
         Power(s,2)*(17 - 26*t1 - 15*Power(t1,2) + 20*Power(t1,3) + 
            4*Power(t1,4) - 43*t2 + 97*t1*t2 - 79*Power(t1,2)*t2 - 
            15*Power(t1,3)*t2 - 44*Power(t2,2) + 64*t1*Power(t2,2) + 
            17*Power(t1,2)*Power(t2,2) - 5*Power(t2,3) - 
            5*t1*Power(t2,3) - Power(t2,4) - 
            Power(s2,4)*(-2 + t1 + Power(t1,3) + 25*t2 - 21*t1*t2 + 
               26*Power(t2,2) - 4*t1*Power(t2,2) + 3*Power(t2,3)) + 
            Power(s2,3)*(-30 - 40*t2 + Power(t2,2) + 19*Power(t2,3) + 
               2*Power(t1,3)*(1 + t2) - 
               2*Power(t1,2)*(19 + 7*t2 + 2*Power(t2,2)) + 
               t1*(66 + 100*t2 - 7*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s2,2)*(35 + 35*t2 + 54*Power(t2,2) + 45*Power(t2,3) - 
               9*Power(t2,4) + Power(t1,3)*(24 + t2) - 
               Power(t1,2)*(5 + 6*t2 + 11*Power(t2,2)) + 
               t1*(-54 - 122*t2 - 63*Power(t2,2) + 19*Power(t2,3))) - 
            s2*(20 + 6*Power(t1,4) + Power(t1,3)*(51 - 16*t2) - 95*t2 - 
               37*Power(t2,2) + 50*Power(t2,3) - 12*Power(t2,4) + 
               Power(t1,2)*(-80 - 117*t2 + 2*Power(t2,2)) + 
               t1*(3 + 140*t2 + 16*Power(t2,2) + 20*Power(t2,3))) + 
            Power(s1,3)*(Power(s2,4) - 4*t1 + s2*(-4 + 16*t1 - 9*t2) + 
               2*t2 + Power(s2,2)*(9 - 15*t1 + 11*t2) - 
               Power(s2,3)*(3*t1 + 2*(5 + t2))) + 
            Power(s1,2)*(-8 + 14*Power(t1,2) + t1*(3 - 4*t2) + 6*t2 - 
               5*Power(t2,2) - Power(s2,4)*(7 + 3*t1 + 5*t2) + 
               Power(s2,3)*(-24 + t1 - 3*Power(t1,2) + 43*t2 + 
                  4*t1*t2 + 4*Power(t2,2)) + 
               s2*(10 + 24*t1 + 17*Power(t1,2) - 50*t2 - 52*t1*t2 + 
                  30*Power(t2,2)) + 
               Power(s2,2)*(41 - 30*Power(t1,2) + 24*t2 - 
                  31*Power(t2,2) + t1*(-31 + 56*t2))) + 
            s1*(-9 + 22*Power(t1,3) + Power(t1,2)*(17 - 39*t2) + 52*t2 - 
               Power(t2,2) + 4*Power(t2,3) + 
               t1*(10 - 59*t2 + 13*Power(t2,2)) + 
               Power(s2,4)*(-5*Power(t1,2) - t1*(-9 + t2) + 
                  t2*(33 + 7*t2)) + 
               Power(s2,2)*(-97 + 9*Power(t1,3) - 82*t2 - 
                  78*Power(t2,2) + 29*Power(t2,3) + 
                  22*Power(t1,2)*(2 + t2) + 
                  t1*(136 + 100*t2 - 60*Power(t2,2))) - 
               s2*(-18 + 29*Power(t1,3) + Power(t1,2)*(50 - 6*t2) + 
                  54*t2 - 104*Power(t2,2) + 33*Power(t2,3) + 
                  t1*(27 + 22*t2 - 56*Power(t2,2))) + 
               Power(s2,3)*(76 + 9*t2 - 52*Power(t2,2) - 2*Power(t2,3) + 
                  Power(t1,2)*(-26 + 5*t2) + 
                  t1*(-98 + 22*t2 - 3*Power(t2,2))))) + 
         s*(-32 + 81*t1 - 53*Power(t1,2) - 7*Power(t1,3) + 
            9*Power(t1,4) + 2*Power(t1,5) + 32*t2 - 129*t1*t2 + 
            162*Power(t1,2)*t2 - 59*Power(t1,3)*t2 - 6*Power(t1,4)*t2 + 
            46*Power(t2,2) - 125*t1*Power(t2,2) + 
            50*Power(t1,2)*Power(t2,2) + 3*Power(t1,3)*Power(t2,2) + 
            16*Power(t2,3) - 2*t1*Power(t2,3) + 
            4*Power(t1,2)*Power(t2,3) + 2*Power(t2,4) - 
            3*t1*Power(t2,4) + 
            Power(s2,5)*t2*(7 + Power(t1,2) + 15*t2 + 4*Power(t2,2) - 
               t1*(8 + 5*t2)) + 
            Power(s2,3)*(16 - 67*t2 - 71*Power(t2,2) - 46*Power(t2,3) + 
               10*Power(t2,4) + Power(t1,4)*(4 + t2) - 
               Power(t1,3)*(22 + 27*t2 + 2*Power(t2,2)) + 
               t1*(-46 + 85*t2 + 126*Power(t2,2) - 20*Power(t2,3)) + 
               Power(t1,2)*(48 + 8*t2 + 33*Power(t2,2) + Power(t2,3))) - 
            Power(s2,4)*(-3 - 28*t2 + 14*Power(t2,2) + 10*Power(t2,3) - 
               Power(t2,4) + Power(t1,3)*(1 + 4*t2) - 
               Power(t1,2)*(5 + 26*t2 + 11*Power(t2,2)) + 
               t1*(7 + 50*t2 + 25*Power(t2,2) + 8*Power(t2,3))) - 
            s2*(-39 + 3*Power(t1,5) + Power(t1,4)*(27 - 4*t2) + 76*t2 - 
               31*Power(t2,2) - 61*Power(t2,3) + 10*Power(t2,4) - 
               3*Power(t2,5) - 
               2*Power(t1,3)*(35 + 32*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(8 + 154*t2 - 22*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(71 - 162*t2 + 11*Power(t2,2) + 49*Power(t2,3) + 
                  Power(t2,4))) - 
            Power(s2,2)*(44 + Power(t1,5) + 8*t2 + 95*Power(t2,2) + 
               33*Power(t2,3) - 11*Power(t2,4) + 3*Power(t2,5) - 
               Power(t1,4)*(16 + 5*t2) + 
               Power(t1,3)*(-16 - 6*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(120 + 163*t2 + 68*Power(t2,2)) - 
               t1*(133 + 160*t2 + 112*Power(t2,2) + 35*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,4)*(-2*Power(s2,4) + 2*t1 + 
               Power(s2,3)*(11 + 3*t1 + t2) + s2*(2 - 7*t1 + 2*t2) + 
               Power(s2,2)*(8*t1 - 3*(3 + t2))) + 
            Power(s1,3)*(-2 - 2*Power(s2,5) - 10*Power(t1,2) - 4*t2 - 
               t1*t2 + Power(s2,4)*(3 + 5*t1 + 7*t2) + 
               Power(s2,3)*(55 + 3*Power(t1,2) + t1*(11 - 10*t2) - 
                  44*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*(-31 + 11*Power(t1,2) + t1*(6 - 26*t2) + 
                  10*t2 + 12*Power(t2,2)) + 
               s2*(-11 + 4*Power(t1,2) + 3*t2 - 9*Power(t2,2) + 
                  t1*(-18 + 25*t2))) + 
            Power(s1,2)*(20 - 2*Power(t1,3) + 7*t2 + 10*Power(t2,2) + 
               Power(s2,5)*(9 + t1 + 8*t2) + Power(t1,2)*(-8 + 19*t2) + 
               t1*(-36 + 16*t2 - 7*Power(t2,2)) + 
               Power(s2,4)*(9 + 8*Power(t1,2) - 20*t2 - 7*Power(t2,2) - 
                  t1*(45 + 14*t2)) + 
               s2*(10 + 25*Power(t1,3) + Power(t1,2)*(1 - 14*t2) + 
                  98*t2 - 22*Power(t2,2) + 15*Power(t2,3) + 
                  t1*(12 - 31*t2 - 30*Power(t2,2))) + 
               Power(s2,3)*(-103 + Power(t1,2)*(49 - 10*t2) - 157*t2 + 
                  65*Power(t2,2) + 3*Power(t2,3) + 
                  t1*(140 - 36*t2 + 11*Power(t2,2))) + 
               Power(s2,2)*(7 - 15*Power(t1,3) + 36*t2 + 
                  18*Power(t2,2) - 18*Power(t2,3) - 
                  Power(t1,2)*(42 + 11*t2) + 
                  t1*(-8 + 5*t2 + 34*Power(t2,2)))) + 
            s1*(10 + 12*Power(t1,4) - 63*t2 - 21*Power(t2,2) - 
               8*Power(t2,3) - Power(t1,3)*(9 + 8*t2) + 
               Power(s2,5)*(-3 + 3*Power(t1,2) - 24*t2 + 4*t1*t2 - 
                  10*Power(t2,2)) - 
               Power(t1,2)*(2 + 25*t2 + 13*Power(t2,2)) + 
               t1*(-11 + 148*t2 - 14*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,4)*(-25 + Power(t1,3) + 7*t2 + 27*Power(t2,2) + 
                  Power(t2,3) - 17*Power(t1,2)*(1 + t2) + 
                  t1*(41 + 66*t2 + 17*Power(t2,2))) - 
               Power(s2,3)*(-37 + Power(t1,3)*(-14 + t2) - 187*t2 - 
                  148*Power(t2,2) + 42*Power(t2,3) + Power(t2,4) + 
                  Power(t1,2)*(15 + 63*t2 - 6*Power(t2,2)) + 
                  t1*(36 + 295*t2 - 45*Power(t2,2) + 4*Power(t2,3))) - 
               s2*(28 + 5*Power(t1,4) + 34*t2 + 148*Power(t2,2) - 
                  27*Power(t2,3) + 11*Power(t2,4) + 
                  Power(t1,3)*(33 + 16*t2) + 
                  Power(t1,2)*(9 + 46*t2 - 19*Power(t2,2)) - 
                  t1*(75 + 98*Power(t2,2) + 13*Power(t2,3))) + 
               Power(s2,2)*(61 - 3*Power(t1,4) + 67*t2 + 28*Power(t2,2) - 
                  30*Power(t2,3) + 12*Power(t2,4) + 
                  Power(t1,3)*(23 + 13*t2) + Power(t1,2)*(152 + 107*t2) - 
                  t1*(233 + 71*t2 + 46*Power(t2,2) + 22*Power(t2,3))))))*
       B1(1 - s1 - t1 + t2,s2,1 - s + s2 - t1))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s1 + t1 - t2)*
       Power(1 - s + s*s2 - s1*s2 - t1 + s2*t2,2)) - 
    (16*(Power(s,5)*(-1 + s2)*
          (1 - t1 + t2 + Power(s2,3)*
             (-5 + 5*t1 + 2*Power(t1,2) - 7*t2 - 2*t1*t2) + 
            Power(s2,2)*(7 - 5*t1 - 2*Power(t1,2) + 9*t2 + 
               2*Power(t2,2)) - 
            s2*(3 - 5*t1 + 2*Power(t1,2) + 7*t2 - 6*t1*t2 + 
               4*Power(t2,2)) + 
            s1*(-1 + Power(s2,3)*(5 + 2*t2) - 
               Power(s2,2)*(7 + 2*t1 + 2*t2) + s2*(3 - 2*t1 + 4*t2))) + 
         Power(s,4)*((-5 + 4*t1)*(-1 + t1 - t2) + 
            Power(s2,6)*(1 - t1 + t2) + 
            Power(s1,2)*s2*(-1 + 2*Power(s2,4) - 4*t1 + 
               s2*(1 + 6*t1 - 4*t2) - Power(s2,3)*(13 + 2*t2) + 
               Power(s2,2)*(11 + 2*t1 + 2*t2)) + 
            Power(s2,5)*(8 - 8*Power(t1,2) + 7*t2 + Power(t2,2) + 
               t1*(-2 + 7*t2)) + 
            Power(s2,4)*(-10 + 5*Power(t1,3) - Power(t1,2)*(-33 + t2) - 
               2*t2 - 3*Power(t2,2) + Power(t2,3) - 
               t1*(22 + 38*t2 + 5*Power(t2,2))) + 
            Power(s2,2)*(32 + 52*t2 + 20*Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,2)*(34 + 22*t2) - 
               2*t1*(32 + 39*t2 + 10*Power(t2,2))) + 
            Power(s2,3)*(-21 - 12*Power(t1,3) + 
               Power(t1,2)*(-35 + t2) - 30*t2 + Power(t2,3) + 
               t1*(62 + 59*t2 + 10*Power(t2,2))) + 
            s2*(-15 + 7*Power(t1,3) - 37*t2 - 22*Power(t2,2) - 
               2*Power(t1,2)*(16 + 11*t2) + 
               t1*(40 + 62*t2 + 15*Power(t2,2))) + 
            s1*(-5 - Power(s2,6) + 4*t1 - 5*Power(s2,5)*(2 + t2) + 
               s2*(16 + 3*Power(t1,2) + 23*t2 - 11*t1*(2 + t2)) - 
               Power(s2,3)*(-10 + 8*t2 + 3*Power(t2,2) + 
                  7*t1*(4 + 3*t2)) + 
               Power(s2,2)*(-33 + 6*Power(t1,2) - 17*t2 + 
                  6*Power(t2,2) + 3*t1*(9 + 4*t2)) + 
               Power(s2,4)*(23 - 5*Power(t1,2) + 15*t2 + Power(t2,2) + 
                  t1*(11 + 12*t2)))) + 
         Power(-1 + t1,2)*(-(Power(s1,3)*Power(s2,2)*
               (-2 + 2*Power(s2,4) + Power(s2,3)*(-5 + t1) - 5*t1 + 
                 5*Power(t1,2) + s2*(3 + 3*t1 - 2*Power(t1,2)) + 
                 Power(s2,2)*(6 - 7*t1 + Power(t1,2)))) - 
            (1 - 2*t1 + Power(t1,3))*(-1 + t1 - t2) - 
            Power(s2,7)*t2*(1 - t1 + 3*t2) + 
            Power(s2,6)*(-1 - Power(t1,2) + 5*t2 + 18*Power(t2,2) + 
               t1*(2 - 5*t2 + 2*Power(t2,2))) + 
            s2*(8*Power(t1,4) - 6*t2*(1 + t2) - 
               2*Power(t1,3)*(7 + 3*t2) + 
               Power(t1,2)*(4 - 3*t2 - 2*Power(t2,2)) + 
               t1*(2 + 15*t2 + 7*Power(t2,2))) + 
            Power(s2,5)*(8 + 10*t2 - 22*Power(t2,2) + 4*Power(t2,3) + 
               Power(t1,2)*(8 + 11*t2) - 
               t1*(16 + 21*t2 + 25*Power(t2,2))) - 
            Power(s2,4)*(8 + 32*t2 + Power(t2,2) + 11*Power(t2,3) + 
               Power(t1,3)*(13 + 7*t2) - 
               Power(t1,2)*(18 + t2 + 8*Power(t2,2)) + 
               t1*(-3 - 38*t2 - 45*Power(t2,2) + Power(t2,3))) + 
            Power(s2,2)*(12 - 22*Power(t1,4) + 7*t2 - 2*Power(t2,2) - 
               5*Power(t2,3) + Power(t1,3)*(29 + 22*t2) - 
               Power(t1,2)*(-20 + 12*t2 + Power(t2,2)) + 
               t1*(-39 - 17*t2 + 11*Power(t2,2) + Power(t2,3))) + 
            Power(s2,3)*(7*Power(t1,4) + Power(t1,3)*(17 + 14*t2) - 
               Power(t1,2)*(63 + 49*t2 + 29*Power(t2,2)) + 
               4*(-2 + 3*t2 + Power(t2,3)) + 
               t1*(47 + 23*t2 + 8*Power(t2,3))) - 
            Power(s1,2)*s2*(1 + 3*Power(s2,6) + 4*t1 - 7*Power(t1,2) + 
               3*Power(t1,3) - 4*Power(s2,5)*(4 + t1 + t2) - 
               Power(s2,3)*(-1 + Power(t1,3) - 
                  Power(t1,2)*(-18 + t2) - 17*t1*(-2 + t2) + 6*t2) + 
               Power(s2,4)*(19 + 7*Power(t1,2) + t1*(21 - 6*t2) + 
                  10*t2) + Power(s2,2)*
                (-19 - 2*Power(t1,3) - 16*t1*(-2 + t2) - 7*t2 + 
                  3*Power(t1,2)*(6 + t2)) + 
               s2*(-1 + t1 + 4*Power(t1,3) + 11*t2 + 5*t1*t2 - 
                  4*Power(t1,2)*(3 + 2*t2))) + 
            s1*(-1 + 2*t1 - Power(t1,3) + Power(s2,7)*(1 - t1 + 6*t2) + 
               s2*(1 + t1 - 3*Power(t1,4) + 7*t2 - 3*t1*t2 + 
                  3*Power(t1,3)*(6 + t2) - Power(t1,2)*(17 + 5*t2)) - 
               Power(s2,6)*(5 + 34*t2 + 2*Power(t2,2) + 
                  t1*(-5 + 6*t2)) + 
               Power(s2,5)*(-8 + 9*Power(t1,2)*(-1 + t2) + 43*t2 + 
                  Power(t2,2) + t1*(17 + 42*t2 - 5*Power(t2,2))) + 
               Power(s2,2)*(-13 + Power(t1,4) + 5*t2 + 14*Power(t2,2) + 
                  Power(t1,3)*(-23 + 2*t2) + 
                  Power(t1,2)*(5 - 3*t2 - 3*Power(t2,2)) - 
                  t1*(-30 + 20*t2 + Power(t2,2))) + 
               Power(s2,3)*(2*Power(t1,4) - 3*Power(t1,3)*t2 + 
                  t1*(3 + 33*t2 - 21*Power(t2,2)) + 
                  Power(t1,2)*(11 + 48*t2 + Power(t2,2)) - 
                  4*(4 + 5*t2 + 2*Power(t2,2))) + 
               Power(s2,4)*(29 + t2 + 11*Power(t2,2) - 
                  2*Power(t1,3)*(1 + t2) + 
                  Power(t1,2)*(14 - 25*t2 + 2*Power(t2,2)) + 
                  t1*(-41 - 78*t2 + 11*Power(t2,2))))) + 
         Power(s,2)*(-(Power(s1,3)*Power(s2,2)*
               (-6 - 9*t1 + 13*Power(t1,2) + Power(s2,3)*(-9 + 7*t1) + 
                 s2*(3 + t1 - 2*Power(t1,2)) + 
                 Power(s2,2)*(24 - 23*t1 + Power(t1,2)))) + 
            (-10 + 24*t1 - 15*Power(t1,2) + Power(t1,3))*
             (-1 + t1 - t2) + 
            Power(s2,7)*(-4 - 4*Power(t1,2) - t2 + Power(t2,2) + 
               t1*(8 + t2)) + 
            Power(s2,6)*(11 + 10*Power(t1,3) - 4*t2 + 3*Power(t2,2) + 
               2*Power(t2,3) + Power(t1,2)*(-9 + 11*t2) - 
               t1*(12 + 7*t2 + 17*Power(t2,2))) + 
            s2*(5*Power(t1,5) - 2*Power(t1,4)*(8 + 9*t2) - 
               13*(1 + 5*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(67 + 88*t2 + 13*Power(t2,2)) - 
               Power(t1,2)*(119 + 222*t2 + 64*Power(t2,2)) + 
               t1*(76 + 217*t2 + 102*Power(t2,2))) - 
            Power(s2,5)*(11 + 6*Power(t1,4) - 17*t2 + 12*Power(t2,2) + 
               2*Power(t2,3) + Power(t1,3)*(23 + 19*t2) - 
               Power(t1,2)*(53 + 6*t2 + 29*Power(t2,2)) + 
               t1*(13 + 4*t2 - 4*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,3)*(-44 - 8*Power(t1,5) - Power(t1,4)*(-20 + t2) + 
               18*t2 + 23*Power(t2,2) + 5*Power(t2,3) + 
               Power(t1,3)*(60 + 13*t2 + 2*Power(t2,2)) + 
               t1*(164 + 76*t2 - 43*Power(t2,2) - 6*Power(t2,3)) + 
               Power(t1,2)*(-192 - 106*t2 - 3*Power(t2,2) + 
                  7*Power(t2,3))) + 
            Power(s2,4)*(9 + Power(t1,5) - 40*t2 - 15*Power(t2,2) - 
               8*Power(t2,3) + Power(t1,4)*(27 + 7*t2) - 
               Power(t1,3)*(49 + 3*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(22 + 22*t2 - 41*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-10 + 14*t2 + 69*Power(t2,2) + 9*Power(t2,3))) + 
            Power(s2,2)*(54 + 2*Power(t1,5) + 41*t2 - 8*Power(t2,2) - 
               21*Power(t2,3) + 2*Power(t1,4)*(-31 + 6*t2) + 
               Power(t1,3)*(47 + 18*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*(138 + 58*t2 - 5*Power(t2,2) - 
                  8*Power(t2,3)) + 
               t1*(-179 - 129*t2 + 29*Power(t2,2) + 25*Power(t2,3))) + 
            Power(s1,2)*s2*(-6 + Power(s2,6) - 18*t1 + 44*Power(t1,2) - 
               21*Power(t1,3) + Power(s2,5)*(-11 - 3*t1 + 2*t2) + 
               Power(s2,2)*(73 + 5*t2 - t1*(125 + 4*t2) + 
                  Power(t1,2)*(31 + 9*t2)) + 
               Power(s2,4)*(8 - 5*Power(t1,2) - 32*t2 + 
                  2*t1*(9 + 11*t2)) + 
               Power(s2,3)*(-29 + 9*Power(t1,3) + t1*(55 - 25*t2) + 
                  46*t2 - Power(t1,2)*(31 + 15*t2)) + 
               s2*(12*Power(t1,3) - 45*t2 + 3*Power(t1,2)*(-9 + 2*t2) + 
                  t1*(25 + 31*t2))) + 
            s1*(-10 + 24*t1 - 15*Power(t1,2) + Power(t1,3) - 
               Power(s2,7)*(-1 + t1 + 2*t2) + 
               Power(s2,6)*(4 - 11*Power(t1,2) + 8*t2 - 4*Power(t2,2) + 
                  t1*(7 + 20*t2)) + 
               s2*(19 - 16*Power(t1,4) + 58*t2 + 
                  Power(t1,3)*(54 + 8*t2) + Power(t1,2)*(-6 + 20*t2) - 
                  3*t1*(17 + 28*t2)) + 
               Power(s2,5)*(-8 + 32*Power(t1,3) + 4*t2 + 
                  25*Power(t2,2) - Power(t1,2)*(23 + 24*t2) - 
                  t1*(1 + 22*t2 + 11*Power(t2,2))) + 
               Power(s2,4)*(46 - 9*Power(t1,4) + 54*t2 - 
                  14*Power(t2,2) + Power(t1,3)*(-81 + 2*t2) - 
                  t1*(114 + 142*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*(158 + 78*t2 + 15*Power(t2,2))) + 
               Power(s2,2)*(-58 + 27*Power(t1,4) + 32*t2 + 
                  60*Power(t2,2) - 2*Power(t1,3)*(73 + 9*t2) + 
                  t1*(113 - 114*t2 - 65*Power(t2,2)) + 
                  Power(t1,2)*(64 + 80*t2 + 15*Power(t2,2))) + 
               Power(s2,3)*(-30 + 10*Power(t1,4) + 
                  Power(t1,3)*(68 - 16*t2) - 106*t2 - 7*Power(t2,2) + 
                  t1*(47 + 174*t2 + 11*Power(t2,2)) - 
                  Power(t1,2)*(95 + 10*t2 + 18*Power(t2,2))))) + 
         Power(s,3)*(-2*Power(s1,3)*Power(s2,2)*
             (1 - s2 - 3*Power(s2,2) + Power(s2,3) + 2*t1) + 
            Power(s2,7)*(-1 + t1 - t2) + 
            (10 - 16*t1 + 5*Power(t1,2))*(-1 + t1 - t2) + 
            Power(s2,6)*(9*Power(t1,2) - t1*(7 + 5*t2) - 
               2*(1 + t2 + Power(t2,2))) + 
            Power(s2,5)*(-14*Power(t1,3) - Power(t1,2)*(21 + 2*t2) + 
               t2*(3 + 2*t2 - 3*Power(t2,2)) + 
               t1*(35 + 24*t2 + 19*Power(t2,2))) + 
            s2*(21 + 9*Power(t1,4) + 69*t2 + 48*Power(t2,2) - 
               6*Power(t1,3)*(8 + 5*t2) + 
               Power(t1,2)*(103 + 124*t2 + 21*Power(t2,2)) - 
               t1*(85 + 176*t2 + 64*Power(t2,2))) + 
            Power(s2,2)*(-52 + Power(t1,4) - 66*t2 - 15*Power(t2,2) + 
               11*Power(t2,3) + 3*Power(t1,3)*(4 + 9*t2) - 
               Power(t1,2)*(98 + 90*t2 + 21*Power(t2,2)) + 
               t1*(137 + 160*t2 + 31*Power(t2,2) - 7*Power(t2,3))) + 
            Power(s2,4)*(4 + 4*Power(t1,4) + 3*t2 + 15*Power(t2,2) + 
               6*Power(t2,3) + Power(t1,3)*(52 + 6*t2) - 
               Power(t1,2)*(47 + 38*t2 + 12*Power(t2,2)) + 
               t1*(-13 + 12*t2 - 32*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s2,3)*(36 - 14*Power(t1,4) + 20*t2 - 20*Power(t2,2) - 
               6*Power(t2,3) - 3*Power(t1,3)*(9 + t2) + 
               Power(t1,2)*(111 + 59*t2 + 12*Power(t2,2)) + 
               t1*(-106 - 95*t2 + 10*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s1,2)*s2*(4 + 14*t1 - 15*Power(t1,2) + 
               Power(s2,4)*(4 + 7*t1 + 5*t2) + 
               Power(s2,2)*(-44 + 40*t1 + 2*Power(t1,2) - 10*t2 + 
                  9*t1*t2) + 
               Power(s2,3)*(25 + 4*Power(t1,2) - 2*t2 - 
                  2*t1*(23 + 5*t2)) + 
               s2*(-1 + 17*Power(t1,2) + 23*t2 - t1*(19 + 7*t2))) + 
            s1*(10 + Power(s2,7) - 16*t1 + 5*Power(t1,2) + 
               Power(s2,6)*(4 + 3*t1 + 2*t2) + 
               Power(s2,5)*(19*Power(t1,2) - 2*(4 + t2) - 
                  6*t1*(6 + 5*t2)) - 
               s2*(25 + 6*Power(t1,3) + 52*t2 + 
                  2*Power(t1,2)*(5 + 3*t2) - 2*t1*(27 + 25*t2)) + 
               Power(s2,3)*(4 + 8*Power(t1,3) + 
                  Power(t1,2)*(15 - 30*t2) + 68*t2 + 14*Power(t2,2) - 
                  2*t1*(4 + 19*t2 + 7*Power(t2,2))) + 
               Power(s2,4)*(-29 - 12*Power(t1,3) - 48*t2 - 
                  10*Power(t2,2) + 4*Power(t1,2)*(-9 + 4*t2) + 
                  t1*(94 + 78*t2 + 8*Power(t2,2))) + 
               Power(s2,2)*(55 + 22*Power(t1,3) - 32*Power(t2,2) - 
                  Power(t1,2)*(37 + 4*t2) + 
                  t1*(-71 + 12*t2 + 18*Power(t2,2))))) - 
         s*(-1 + t1)*(2*Power(s1,3)*Power(s2,2)*
             (-3 + Power(s2,4) + 3*Power(s2,3)*(-2 + t1) - 6*t1 + 
               7*Power(t1,2) + s2*(2 + 3*t1 - 2*Power(t1,2)) + 
               Power(s2,2)*(12 - 12*t1 + Power(t1,2))) + 
            (5 - 11*t1 + 4*Power(t1,2) + 2*Power(t1,3))*(-1 + t1 - t2) - 
            Power(s2,7)*(1 + Power(t1,2) + 9*t2 + 4*Power(t2,2) - 
               t1*(2 + 9*t2)) + 
            Power(s2,6)*(3 + Power(t1,2)*(3 - 17*t2) + 24*t2 - 
               7*Power(t2,2) - 2*Power(t2,3) + 
               t1*(-6 - 7*t2 + 13*Power(t2,2))) + 
            s2*(3 - Power(t1,5) + 31*t2 + 28*Power(t2,2) + 
               Power(t1,4)*(-15 + 4*t2) + 
               Power(t1,3)*(15 - 8*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*(22 + 66*t2 + 21*Power(t2,2)) - 
               t1*(24 + 93*t2 + 44*Power(t2,2))) + 
            Power(s2,5)*(-5 - 37*t2 + 31*Power(t2,2) + Power(t2,3) + 
               2*Power(t1,3)*(1 + 5*t2) + 
               Power(t1,2)*(-9 + 16*t2 - 11*Power(t2,2)) + 
               t1*(12 + 11*t2 + 6*Power(t2,2) + Power(t2,3))) + 
            Power(s2,4)*(-2 + 54*t2 - 8*Power(t2,2) + 12*Power(t2,3) - 
               Power(t1,4)*(3 + 2*t2) + 
               Power(t1,3)*(32 + t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-57 - 31*t2 + 6*Power(t2,2)) - 
               2*t1*(-15 + 11*t2 + 34*Power(t2,2) + Power(t2,3))) + 
            Power(s2,3)*(31 + 2*Power(t1,5) - 24*t2 + Power(t2,2) - 
               4*Power(t2,3) - Power(t1,4)*(27 + t2) + 
               Power(t1,3)*(-29 - 12*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(162 + 106*t2 + 44*Power(t2,2) - 
                  3*Power(t2,3)) - 
               t1*(139 + 69*t2 - 9*Power(t2,2) + 11*Power(t2,3))) - 
            Power(s2,2)*(36 + Power(t1,5) + Power(t1,4)*(-71 + t2) + 
               18*t2 - 11*Power(t2,2) - 17*Power(t2,3) + 
               Power(t1,3)*(94 + 69*t2 + Power(t2,2)) - 
               Power(t1,2)*(-57 + 40*t2 + 16*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(-117 - 48*t2 + 44*Power(t2,2) + 12*Power(t2,3))) - 
            Power(s1,2)*s2*(-4 + 4*Power(s2,6) - 14*t1 + 
               29*Power(t1,2) - 13*Power(t1,3) + 
               Power(s2,5)*(-7 + t1 + 6*t2) - 
               Power(s2,4)*(5 + 17*Power(t1,2) + t1*(4 - 23*t2) + 
                  37*t2) + Power(s2,3)*
                (-6 + 6*Power(t1,3) + t1*(44 - 46*t2) - 
                  8*Power(t1,2)*(-3 + t2) + 48*t2) + 
               Power(s2,2)*(53 + 2*Power(t1,3) + 4*t2 - 
                  Power(t1,2)*(18 + t2) + 3*t1*(-31 + 9*t2)) + 
               s2*(1 - 3*Power(t1,3) - 37*t2 + 2*t1*(5 + 2*t2) + 
                  Power(t1,2)*(10 + 17*t2))) + 
            s1*(5 - 11*t1 + 4*Power(t1,2) + 2*Power(t1,3) + 
               Power(s2,7)*(9 - 9*t1 + 8*t2) + 
               Power(s2,6)*(-26 + 15*Power(t1,2) + t1*(11 - 12*t2) + 
                  6*Power(t2,2)) + 
               s2*(-7 + 12*Power(t1,4) - 32*t2 - 
                  2*Power(t1,3)*(29 + 5*t2) + Power(t1,2)*(42 + 8*t2) + 
                  t1*(11 + 30*t2)) + 
               Power(s2,4)*(-50 + 2*Power(t1,4) + 12*Power(t2,2) + 
                  Power(t1,3)*(43 + 6*t2) + 
                  t1*(58 + 118*t2 - 20*Power(t2,2)) + 
                  Power(t1,2)*(-53 + 12*t2 - 10*Power(t2,2))) - 
               Power(s2,5)*(-34 + 13*Power(t1,3) + 40*t2 + 
                  26*Power(t2,2) + Power(t1,2)*(13 + 10*t2) + 
                  2*t1*(4 + t2 - 8*Power(t2,2))) + 
               Power(s2,3)*(32 - 6*Power(t1,4) + 58*t2 + 4*Power(t2,2) + 
                  Power(t1,3)*(-39 + 6*t2) + 
                  Power(t1,2)*(25 - 68*t2 + 6*Power(t2,2)) + 
                  4*t1*(-3 - 27*t2 + 8*Power(t2,2))) + 
               Power(s2,2)*(39 - 12*Power(t1,4) - 26*t2 - 
                  48*Power(t2,2) + 3*Power(t1,3)*(35 + 2*t2) - 
                  2*Power(t1,2)*(26 + 19*t2) + 
                  t1*(-80 + 94*t2 + 28*Power(t2,2))))))*R1q(s2))/
     (Power(-1 + s2,3)*s2*(-s + s2 - t1)*(-1 + t1)*Power(-1 + s + t1,3)*
       (-1 + s1 + t1 - t2)*(1 - s + s*s2 - s1*s2 - t1 + s2*t2)) - 
    (16*(Power(s,8)*(2 - 2*Power(t1,2) + Power(s2,2)*(-1 + t1 - t2) + 
            5*t2 - t1*t2 + 3*Power(t2,2) + 
            s1*(-1 + s2)*(2 + s2 + 2*t1 + 3*t2) + 
            s2*(-1 - t1 + 4*Power(t1,2) - 4*t2 - 3*t1*t2 - Power(t2,2))) \
- Power(s,7)*(-2 + 9*t1 - 15*Power(t1,2) + 10*Power(t1,3) + 
            5*Power(s2,3)*(-1 + t1 - t2) + 10*t2 - 32*t1*t2 + 
            13*Power(t1,2)*t2 + 12*Power(t2,2) - 21*t1*Power(t2,2) - 
            2*Power(t2,3) + Power(s1,2)*
             (-4 - 5*t1 - 2*t2 + s2*(7 + 7*t1 + 5*t2)) + 
            Power(s2,2)*(-9 + 19*Power(t1,2) - 17*t2 - 4*Power(t2,2) - 
               t1*(2 + 15*t2)) + 
            s1*(6 + 5*Power(s2,3) - 6*t1 + 5*Power(t1,2) - 6*t2 + 
               26*t1*t2 + 4*Power(t2,2) + 
               Power(s2,2)*(9 + 7*t1 + 12*t2) + 
               s2*(-26 + 5*t1 + 8*Power(t1,2) - 22*t2 - 38*t1*t2 - 
                  5*Power(t2,2))) + 
            s2*(19 - 15*Power(t1,3) - 5*Power(t1,2)*(-2 + t2) + 25*t2 + 
               20*Power(t2,2) + 5*t1*(-4 + t2 + 4*Power(t2,2)))) + 
         Power(s,6)*(-34 + 87*t1 - 103*Power(t1,2) + 68*Power(t1,3) - 
            20*Power(t1,4) + 10*Power(s2,4)*(-1 + t1 - t2) - 8*t2 - 
            80*t1*t2 + 117*Power(t1,2)*t2 - 52*Power(t1,3)*t2 + 
            16*Power(t2,2) - 55*t1*Power(t2,2) + 
            60*Power(t1,2)*Power(t2,2) - 25*Power(t2,3) + 
            11*t1*Power(t2,3) + Power(t2,4) + 
            Power(s1,3)*(-2*Power(s2,2) - 4*t1 + t2 + 
               s2*(5 + 9*t1 + t2)) + 
            Power(s2,3)*(-25 + 40*Power(t1,2) - 30*t2 - 7*Power(t2,2) - 
               t1*(3 + 31*t2)) + 
            Power(s1,2)*(-18 + Power(s2,3) + 21*Power(t1,2) - 21*t2 - 
               Power(t2,2) + Power(s2,2)*(32 + 34*t1 + 21*t2) + 
               t1*(-15 + 23*t2) + 
               s2*(-4 + t1 - 2*Power(t1,2) + 6*t2 - 69*t1*t2)) + 
            Power(s2,2)*(59 - 65*Power(t1,3) + 
               Power(t1,2)*(62 - 30*t2) + 68*t2 + 41*Power(t2,2) - 
               2*Power(t2,3) + t1*(-74 + 23*t2 + 97*Power(t2,2))) + 
            s2*(27 + 22*Power(t1,4) + 58*t2 + 67*Power(t2,2) + 
               2*Power(t2,4) + Power(t1,3)*(-9 + 43*t2) + 
               Power(t1,2)*(28 - 18*t2 - 45*Power(t2,2)) - 
               2*t1*(29 + 42*t2 + 39*Power(t2,2) + 11*Power(t2,3))) + 
            s1*(52 + 10*Power(s2,4) + 5*Power(t1,3) + 
               Power(t1,2)*(18 - 79*t2) + 8*t2 + 46*Power(t2,2) - 
               Power(t2,3) + Power(s2,3)*(26 + 11*t1 + 18*t2) + 
               Power(s2,2)*(-93 + 55*Power(t1,2) + t1*(13 - 168*t2) - 
                  54*t2 - 17*Power(t2,2)) - 
               6*t1*(8 - 10*t2 + 5*Power(t2,2)) + 
               s2*(-26 - 45*Power(t1,3) - 58*t2 - 11*Power(t2,2) - 
                  3*Power(t2,3) + Power(t1,2)*(15 + 71*t2) + 
                  t1*(37 + 58*t2 + 82*Power(t2,2))))) + 
         Power(s,5)*(85 - 198*t1 + 229*Power(t1,2) - 212*Power(t1,3) + 
            116*Power(t1,4) - 20*Power(t1,5) - 
            10*Power(s2,5)*(-1 + t1 - t2) + 9*t2 + 186*t1*t2 - 
            387*Power(t1,2)*t2 + 288*Power(t1,3)*t2 - 
            100*Power(t1,4)*t2 - 8*Power(t2,2) - 2*t1*Power(t2,2) - 
            113*Power(t1,2)*Power(t2,2) + 90*Power(t1,3)*Power(t2,2) + 
            110*Power(t2,3) - 106*t1*Power(t2,3) + 
            25*Power(t1,2)*Power(t2,3) - 10*Power(t2,4) + 
            5*t1*Power(t2,4) + 
            Power(s1,4)*(-2 + t1 + s2*(3 - 5*t1 + t2)) + 
            Power(s2,4)*(27 - 50*Power(t1,2) + 27*t2 + 8*Power(t2,2) + 
               t1*(15 + 34*t2)) + 
            Power(s2,3)*(-89 + 119*Power(t1,3) - 115*t2 - 
               28*Power(t2,2) + 6*Power(t2,3) + 
               5*Power(t1,2)*(-25 + 16*t2) + 
               t1*(113 - 53*t2 - 193*Power(t2,2))) + 
            Power(s2,2)*(-70 - 76*Power(t1,4) + 
               Power(t1,3)*(88 - 187*t2) - 63*t2 - 100*Power(t2,2) - 
               17*Power(t2,3) - 10*Power(t2,4) + 
               Power(t1,2)*(-149 + 216*t2 + 183*Power(t2,2)) + 
               t1*(193 + 15*t2 + 14*Power(t2,2) + 90*Power(t2,3))) + 
            s2*(30 + 16*Power(t1,5) + 7*t2 - 57*Power(t2,2) + 
               21*Power(t2,3) + Power(t2,5) + 
               Power(t1,4)*(25 + 72*t2) - 
               Power(t1,3)*(175 + 11*t2 + 18*Power(t2,2)) - 
               Power(t1,2)*(-260 + 85*t2 + 255*Power(t2,2) + 
                  68*Power(t2,3)) + 
               t1*(-152 + 78*t2 + 354*Power(t2,2) + 66*Power(t2,3) - 
                  3*Power(t2,4))) + 
            Power(s1,3)*(-8 + 7*Power(s2,3) + 14*t1 - 19*Power(t1,2) - 
               2*Power(s2,2)*(5 + 24*t1) + 4*t2 + 4*t1*t2 + 
               s2*(-14 + 14*Power(t1,2) - 27*t2 - 4*Power(t2,2) + 
                  t1*(-13 + 36*t2))) + 
            Power(s1,2)*(-2*Power(s2,4) + 30*Power(t1,3) + 
               3*Power(t1,2)*(-39 + 29*t2) - 
               Power(s2,3)*(69 + 64*t1 + 35*t2) + 
               6*(6 + 17*t2 - 2*Power(t2,2)) - 
               2*t1*(-11 + 67*t2 + 3*Power(t2,2)) - 
               Power(s2,2)*(-34 + 9*Power(t1,2) + t1*(44 - 280*t2) + 
                  57*t2 + 10*Power(t2,2)) + 
               s2*(41 + 45*Power(t1,3) + 38*t2 + 45*Power(t2,2) + 
                  6*Power(t2,3) - Power(t1,2)*(17 + 165*t2) + 
                  t1*(23 + 152*t2 - 60*Power(t2,2)))) - 
            s1*(113 + 10*Power(s2,5) - 30*Power(t1,4) + 22*t2 + 
               204*Power(t2,2) - 20*Power(t2,3) + 
               Power(s2,4)*(33 + 12*t1 + 14*t2) + 
               6*Power(t1,3)*(11 + 18*t2) + 
               Power(t1,2)*(-81 - 200*t2 + 93*Power(t2,2)) + 
               t1*(-72 + 8*t2 - 226*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,3)*(-161 + 132*Power(t1,2) - 70*t2 - 
                  22*Power(t2,2) - t1*(23 + 302*t2)) - 
               Power(s2,2)*(48 + 215*Power(t1,3) + 62*t2 + 
                  84*Power(t2,2) + 20*Power(t2,3) - 
                  Power(t1,2)*(145 + 238*t2) + 
                  t1*(-71 + 84*t2 - 322*Power(t2,2))) + 
               s2*(62 + 70*Power(t1,4) + 2*Power(t1,3)*(-26 + t2) - 
                  3*t2 + 45*Power(t2,2) + 21*Power(t2,3) + 
                  4*Power(t2,4) + 
                  Power(t1,2)*(130 - 245*t2 - 219*Power(t2,2)) + 
                  t1*(-141 + 358*t2 + 205*Power(t2,2) - 32*Power(t2,3))))\
) + Power(s,4)*(-113 + 117*t1 + 248*Power(t1,2) - 342*Power(t1,3) + 
            16*Power(t1,4) + 84*Power(t1,5) - 10*Power(t1,6) + 
            Power(s1,5)*s2*(-2 + s2 + t1) + 
            5*Power(s2,6)*(-1 + t1 - t2) + 93*t2 - 713*t1*t2 + 
            1429*Power(t1,2)*t2 - 1158*Power(t1,3)*t2 + 
            443*Power(t1,4)*t2 - 105*Power(t1,5)*t2 + 24*Power(t2,2) + 
            72*t1*Power(t2,2) - 31*Power(t1,2)*Power(t2,2) - 
            118*Power(t1,3)*Power(t2,2) + 75*Power(t1,4)*Power(t2,2) - 
            239*Power(t2,3) + 387*t1*Power(t2,3) - 
            199*Power(t1,2)*Power(t2,3) + 30*Power(t1,3)*Power(t2,3) + 
            31*Power(t2,4) - 35*t1*Power(t2,4) + 
            10*Power(t1,2)*Power(t2,4) - 
            Power(s1,4)*(-8 + 2*Power(s2,3) + 9*t1 - 5*Power(t1,2) + 
               s2*(15 + 10*Power(t1,2) + 2*t1*(-12 + t2) - 7*t2) + 
               Power(s2,2)*(6 - 17*t1 + 8*t2)) + 
            Power(s2,5)*(-6 + 40*Power(t1,2) - 11*t2 - 7*Power(t2,2) - 
               t1*(32 + 21*t2)) + 
            Power(s2,4)*(58 - 115*Power(t1,3) + 
               Power(t1,2)*(129 - 121*t2) + 85*t2 - 15*Power(t2,2) - 
               6*Power(t2,3) + t1*(-78 + 109*t2 + 202*Power(t2,2))) + 
            Power(s2,3)*(97 + 103*Power(t1,4) + 20*t2 + 
               11*Power(t2,2) + 35*Power(t2,3) + 17*Power(t2,4) + 
               Power(t1,3)*(-126 + 335*t2) - 
               2*Power(t1,2)*(-97 + 234*t2 + 141*Power(t2,2)) + 
               t1*(-262 + 107*t2 + 199*Power(t2,2) - 143*Power(t2,3))) + 
            Power(s2,2)*(-147 - 39*Power(t1,5) - 230*t2 + 
               121*Power(t2,2) + 87*Power(t2,3) + 3*Power(t2,4) - 
               4*Power(t2,5) - 2*Power(t1,4)*(5 + 119*t2) + 
               Power(t1,3)*(341 + 404*t2 + 42*Power(t2,2)) + 
               Power(t1,2)*(-689 - 619*t2 + 290*Power(t2,2) + 
                  232*Power(t2,3)) + 
               t1*(542 + 625*t2 - 574*Power(t2,2) - 337*Power(t2,3) + 
                  7*Power(t2,4))) + 
            s2*(13 + 6*Power(t1,6) + 46*t2 - 114*Power(t2,3) - 
               16*Power(t2,4) - Power(t2,5) + 
               Power(t1,5)*(52 + 53*t2) + 
               Power(t1,4)*(-530 + 72*t2 + 33*Power(t2,2)) - 
               2*Power(t1,3)*
                (-520 + 81*t2 + 246*Power(t2,2) + 36*Power(t2,3)) + 
               Power(t1,2)*(-722 + 327*t2 + 1008*Power(t2,2) + 
                  159*Power(t2,3) - 22*Power(t2,4)) + 
               t1*(141 - 301*t2 - 513*Power(t2,2) + 5*Power(t2,3) + 
                  35*Power(t2,4) + 2*Power(t2,5))) + 
            Power(s1,3)*(32 - 10*Power(s2,4) - 35*Power(t1,3) + 
               Power(s2,3)*(13 + 82*t1) + 2*t1*(-26 + t2) - 25*t2 + 
               Power(t1,2)*(72 + 5*t2) + 
               Power(s2,2)*(-14 - 64*Power(t1,2) + t1*(139 - 108*t2) + 
                  59*t2 + 22*Power(t2,2)) - 
               s2*(-44 + 15*Power(t1,3) + Power(t1,2)*(29 - 97*t2) - 
                  106*t2 + 8*Power(t2,2) + 
                  t1*(2 + 197*t2 + 2*Power(t2,2)))) + 
            Power(s1,2)*(-36 + 10*Power(t1,4) - 243*t2 + 
               57*Power(t2,2) + 20*Power(t1,3)*(-11 + 8*t2) + 
               Power(s2,4)*(71 + 67*t1 + 31*t2) + 
               Power(t1,2)*(318 - 403*t2 - 15*Power(t2,2)) + 
               t1*(-52 + 431*t2 - 12*Power(t2,2)) + 
               Power(s2,3)*(-79 + 48*Power(t1,2) + t1*(36 - 420*t2) + 
                  93*t2 + 23*Power(t2,2)) + 
               Power(s2,2)*(-130 - 209*Power(t1,3) + 158*t2 - 
                  97*Power(t2,2) - 28*Power(t2,3) + 
                  Power(t1,2)*(36 + 545*t2) + 
                  t1*(143 - 827*t2 + 172*Power(t2,2))) + 
               s2*(-10 + 80*Power(t1,4) - 212*t2 - 183*Power(t2,2) + 
                  2*Power(t2,3) - Power(t1,3)*(86 + 127*t2) + 
                  Power(t1,2)*(351 + 277*t2 - 186*Power(t2,2)) + 
                  t1*(-301 + 44*t2 + 357*Power(t2,2) + 8*Power(t2,3)))) \
+ s1*(117 + 5*Power(s2,6) + 40*Power(t1,5) - 38*t2 + 450*Power(t2,2) - 
               71*Power(t2,3) - Power(t1,4)*(252 + 55*t2) + 
               Power(t1,3)*(859 + 268*t2 - 155*Power(t2,2)) - 
               Power(t1,2)*(936 + 287*t2 - 530*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(183 + 70*t2 - 766*Power(t2,2) + 54*Power(t2,3)) + 
               Power(s2,5)*(10*t1 + 9*(2 + t2)) + 
               Power(s2,4)*(-121 + 162*Power(t1,2) - 39*t2 - 
                  15*Power(t2,2) - 2*t1*(51 + 146*t2)) + 
               Power(s2,3)*(-51 - 383*Power(t1,3) + 69*t2 - 
                  141*Power(t2,2) - 38*Power(t2,3) + 
                  Power(t1,2)*(372 + 290*t2) + 
                  t1*(56 - 286*t2 + 481*Power(t2,2))) + 
               Power(s2,2)*(306 + 252*Power(t1,4) - 231*Power(t2,2) + 
                  41*Power(t2,3) + 17*Power(t2,4) + 
                  2*Power(t1,3)*(-177 + 61*t2) + 
                  Power(t1,2)*(688 - 297*t2 - 713*Power(t2,2)) + 
                  t1*(-830 + 454*t2 + 1025*Power(t2,2) - 88*Power(t2,3))\
) + s2*(-36 - 50*Power(t1,5) + Power(t1,4)*(21 - 103*t2) + 35*t2 + 
                  282*Power(t2,2) + 108*Power(t2,3) + 2*Power(t2,4) + 
                  Power(t1,3)*(-207 + 553*t2 + 214*Power(t2,2)) + 
                  Power(t1,2)*
                   (80 - 1294*t2 - 407*Power(t2,2) + 121*Power(t2,3)) + 
                  t1*(157 + 739*t2 - 47*Power(t2,2) - 219*Power(t2,3) - 
                     7*Power(t2,4))))) - 
         Power(s,3)*(-100 - 135*t1 + 1514*Power(t1,2) - 
            2767*Power(t1,3) + 2026*Power(t1,4) - 529*Power(t1,5) - 
            11*Power(t1,6) + 2*Power(t1,7) + 
            Power(s1,5)*s2*(-6 + Power(s2,2) + 6*t1 - 3*s2*t1 - 
               2*Power(t1,2)) + Power(s2,7)*(-1 + t1 - t2) + 232*t2 - 
            1594*t1*t2 + 3744*Power(t1,2)*t2 - 3946*Power(t1,3)*t2 + 
            1896*Power(t1,4)*t2 - 392*Power(t1,5)*t2 + 
            61*Power(t1,6)*t2 + 81*Power(t2,2) - 134*t1*Power(t2,2) + 
            62*Power(t1,2)*Power(t2,2) + 22*Power(t1,4)*Power(t2,2) - 
            33*Power(t1,5)*Power(t2,2) - 289*Power(t2,3) + 
            694*t1*Power(t2,3) - 620*Power(t1,2)*Power(t2,3) + 
            236*Power(t1,3)*Power(t2,3) - 20*Power(t1,4)*Power(t2,3) + 
            44*Power(t2,4) - 74*t1*Power(t2,4) + 
            40*Power(t1,2)*Power(t2,4) - 10*Power(t1,3)*Power(t2,4) + 
            Power(s2,6)*(8 + 19*Power(t1,2) - 4*Power(t2,2) - 
               t1*(27 + 7*t2)) - 
            Power(s2,5)*(-1 + 57*Power(t1,3) + 7*t2 + 36*Power(t2,2) + 
               2*Power(t2,3) + 2*Power(t1,2)*(-33 + 53*t2) + 
               t1*(10 - 138*t2 - 117*Power(t2,2))) + 
            Power(s2,4)*(70 + 63*Power(t1,4) + 4*t2 - 68*Power(t2,2) + 
               40*Power(t2,3) + 12*Power(t2,4) + 
               Power(t1,3)*(-61 + 307*t2) + 
               Power(t1,2)*(68 - 467*t2 - 191*Power(t2,2)) + 
               t1*(-140 + 139*t2 + 216*Power(t2,2) - 111*Power(t2,3))) + 
            Power(s2,3)*(-189 - 32*Power(t1,5) - 370*t2 + 
               86*Power(t2,2) + 161*Power(t2,3) + 15*Power(t2,4) - 
               5*Power(t2,5) - Power(t1,4)*(7 + 298*t2) + 
               Power(t1,3)*(273 + 619*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(-607 - 949*t2 + 73*Power(t2,2) + 
                  278*Power(t2,3)) + 
               t1*(562 + 979*t2 - 253*Power(t2,2) - 485*Power(t2,3) + 
                  5*Power(t2,4))) - 
            s2*(-235 + Power(t1,7) - 423*t2 - 93*Power(t2,2) + 
               213*Power(t2,3) + 34*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,6)*(39 + 19*t2) + 
               Power(t1,5)*(-610 + 123*t2 + 36*Power(t2,2)) - 
               Power(t1,4)*(-1122 + 433*t2 + 484*Power(t2,2) + 
                  28*Power(t2,3)) + 
               2*Power(t1,3)*
                (11 + 919*t2 + 794*Power(t2,2) + 60*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(t1,2)*(-1419 - 3192*t2 - 1919*Power(t2,2) - 
                  38*Power(t2,3) + 99*Power(t2,4)) + 
               t1*(1080 + 2081*t2 + 855*Power(t2,2) - 260*Power(t2,3) - 
                  96*Power(t2,4) - 2*Power(t2,5))) + 
            Power(s2,2)*(-166 + 9*Power(t1,6) - 124*t2 + 
               283*Power(t2,2) + 132*Power(t2,3) - 59*Power(t2,4) - 
               4*Power(t2,5) + Power(t1,5)*(61 + 118*t2) + 
               2*Power(t1,4)*(-492 - 94*t2 + 51*Power(t2,2)) + 
               Power(t1,3)*(2559 + 794*t2 - 765*Power(t2,2) - 
                  178*Power(t2,3)) + 
               Power(t1,2)*(-2640 - 1248*t2 + 1956*Power(t2,2) + 
                  605*Power(t2,3) - 58*Power(t2,4)) + 
               t1*(1161 + 679*t2 - 1551*Power(t2,2) - 552*Power(t2,3) + 
                  102*Power(t2,4) + 7*Power(t2,5))) - 
            Power(s1,4)*(3*Power(s2,4) + 
               2*(-6 + 9*t1 - 8*Power(t1,2) + 5*Power(t1,3)) + 
               Power(s2,3)*(2 - 19*t1 + 9*t2) + 
               Power(s2,2)*(39 + 30*Power(t1,2) + 4*t2 - 
                  t1*(64 + 19*t2)) + 
               s2*(30 + Power(t1,2)*(41 - 8*t2) - 26*t2 + 
                  t1*(-64 + 22*t2))) + 
            Power(s1,3)*(51 - 7*Power(s2,5) - 148*Power(t1,3) + 
               30*Power(t1,4) - 40*t2 + 32*Power(t1,2)*(6 + t2) + 
               Power(s2,4)*(3 + 68*t1 + t2) + 2*t1*(-63 + 4*t2) + 
               Power(s2,3)*(-47 - 66*Power(t1,2) + 31*t2 + 
                  26*Power(t2,2) - 2*t1*(-95 + 53*t2)) + 
               Power(s2,2)*(-86 - 42*Power(t1,3) + 252*t2 + 
                  16*Power(t2,2) + Power(t1,2)*(-281 + 248*t2) + 
                  t1*(390 - 470*t2 - 46*Power(t2,2))) + 
               s2*(95 + 40*Power(t1,4) + Power(t1,3)*(6 - 88*t2) + 
                  184*t2 - 44*Power(t2,2) - 
                  2*Power(t1,2)*(20 - 201*t2 + 6*Power(t2,2)) + 
                  4*t1*(-24 - 117*t2 + 7*Power(t2,2)))) + 
            Power(s1,2)*(-7 - 2*Power(s2,6) + 15*Power(t1,5) + 
               Power(t1,4)*(150 - 160*t2) - 311*t2 + 88*Power(t2,2) + 
               Power(s2,5)*(24 + 47*t1 + 16*t2) + 
               4*Power(t1,3)*(-146 + 173*t2 + 5*Power(t2,2)) - 
               6*t1*(25 - 131*t2 + 6*Power(t2,2)) - 
               2*Power(t1,2)*(-287 + 502*t2 + 36*Power(t2,2)) + 
               Power(s2,4)*(-43 + 80*Power(t1,2) + 86*t2 + 
                  19*Power(t2,2) - t1*(68 + 307*t2)) + 
               Power(s2,3)*(-213 - 332*Power(t1,3) + 333*t2 - 
                  41*Power(t2,2) - 34*Power(t2,3) + 
                  16*Power(t1,2)*(10 + 37*t2) + 
                  t1*(299 - 1121*t2 + 160*Power(t2,2))) + 
               s2*(-55*Power(t1,5) + 3*Power(t1,4)*(33 + t2) + 
                  Power(t1,3)*(-902 - 92*t2 + 204*Power(t2,2)) - 
                  2*t1*(529 - 306*t2 - 420*Power(t2,2) + 
                     6*Power(t2,3)) + 
                  Power(t1,2)*
                   (1807 - 132*t2 - 780*Power(t2,2) + 8*Power(t2,3)) + 
                  4*(31 - 102*t2 - 78*Power(t2,2) + 9*Power(t2,3))) + 
               Power(s2,2)*(163 + 274*Power(t1,4) + 256*t2 - 
                  446*Power(t2,2) - 24*Power(t2,3) - 
                  Power(t1,3)*(339 + 274*t2) + 
                  Power(t1,2)*(628 + 1415*t2 - 464*Power(t2,2)) + 
                  t1*(-699 - 1352*t2 + 850*Power(t2,2) + 54*Power(t2,3))\
)) + s1*(56 + Power(s2,7) - 23*Power(t1,6) + Power(t1,5)*(310 - 22*t2) - 
               164*t2 + 549*Power(t2,2) - 104*Power(t2,3) + 
               Power(s2,6)*(2 + 5*t1 + 6*t2) + 
               Power(t1,3)*(3478 + 584*t2 - 780*Power(t2,2)) + 
               2*Power(t1,4)*(-898 - 31*t2 + 75*Power(t2,2)) - 
               2*Power(t1,2)*
                (1341 + 448*t2 - 716*Power(t2,2) + 8*Power(t2,3)) + 
               2*t1*(328 + 282*t2 - 677*Power(t2,2) + 60*Power(t2,3)) + 
               Power(s2,5)*(-4 + 117*Power(t1,2) + 16*t2 - 
                  7*Power(t2,2) - 6*t1*(23 + 28*t2)) + 
               Power(s2,4)*(-57 - 328*Power(t1,3) + 111*t2 - 
                  129*Power(t2,2) - 29*Power(t2,3) + 
                  Power(t1,2)*(396 + 127*t2) + 
                  2*t1*(3 - 82*t2 + 175*Power(t2,2))) + 
               Power(s2,3)*(452 + 320*Power(t1,4) + 93*t2 - 
                  447*Power(t2,2) - 3*Power(t2,3) + 21*Power(t2,4) + 
                  Power(t1,3)*(-524 + 306*t2) + 
                  Power(t1,2)*(835 - 259*t2 - 804*Power(t2,2)) - 
                  2*t1*(532 - 14*t2 - 708*Power(t2,2) + 39*Power(t2,3))) \
+ s2*(18*Power(t1,6) + Power(t1,5)*(35 + 94*t2) - 
                  Power(t1,4)*(148 + 548*t2 + 71*Power(t2,2)) + 
                  Power(t1,3)*
                   (1636 + 2320*t2 + 206*Power(t2,2) - 144*Power(t2,3)) \
+ t1*(2189 + 1808*t2 - 776*Power(t2,2) - 532*Power(t2,3) - 
                     2*Power(t2,4)) + 
                  Power(t1,2)*
                   (-3291 - 3496*t2 + 134*Power(t2,2) + 
                     518*Power(t2,3) - 2*Power(t2,4)) - 
                  2*(213 + 105*t2 - 263*Power(t2,2) - 96*Power(t2,3) + 
                     7*Power(t2,4))) + 
               Power(s2,2)*(-122*Power(t1,5) - 
                  4*Power(t1,4)*(-49 + 94*t2) + 
                  2*Power(t1,3)*(-520 + 574*t2 + 247*Power(t2,2)) + 
                  Power(t1,2)*
                   (1720 - 2640*t2 - 1739*Power(t2,2) + 304*Power(t2,3)) \
+ t1*(-915 + 2238*t2 + 1514*Power(t2,2) - 546*Power(t2,3) - 
                     31*Power(t2,4)) + 
                  2*(65 - 211*t2 - 151*Power(t2,2) + 146*Power(t2,3) + 
                     8*Power(t2,4))))) + 
         Power(s,2)*(Power(s1,5)*s2*
             (Power(s2,2)*(3 - 5*t1) + 6*(-1 + t1) + s2*t1*(-9 + 7*t1)) \
+ Power(s2,7)*(4 + 4*Power(t1,2) + t2 - Power(t2,2) - t1*(8 + t2)) + 
            Power(s2,6)*(-11 - 10*Power(t1,3) + 
               Power(t1,2)*(9 - 49*t2) - 34*t2 - 21*Power(t2,2) + 
               t1*(12 + 83*t2 + 35*Power(t2,2))) + 
            Power(s2,5)*(15 + 12*Power(t1,4) + 16*t2 - 22*Power(t2,2) + 
               26*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,3)*(1 + 140*t2) - 
               Power(t1,2)*(23 + 215*t2 + 41*Power(t2,2)) + 
               t1*(-5 + 59*t2 + 42*Power(t2,2) - 42*Power(t2,3))) + 
            Power(s2,4)*(-37 - 8*Power(t1,5) + 
               Power(t1,4)*(11 - 159*t2) - 159*t2 + Power(t2,2) + 
               110*Power(t2,3) + 11*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,3)*(5 + 282*t2 - 41*Power(t2,2)) + 
               Power(t1,2)*(-48 - 323*t2 + 17*Power(t2,2) + 
                  129*Power(t2,3)) + 
               t1*(77 + 359*t2 + 19*Power(t2,2) - 273*Power(t2,3) + 
                  Power(t2,4))) - 
            (-1 + t1)*(-60 + 238*t2 + 107*Power(t2,2) - 
               200*Power(t2,3) + 31*Power(t2,4) + 
               2*Power(t1,6)*(8 + 9*t2) - 
               Power(t1,5)*(655 + 157*t2 + 6*Power(t2,2)) + 
               Power(t1,4)*(2292 + 1463*t2 - 91*Power(t2,2) - 
                  7*Power(t2,3)) + 
               Power(t1,2)*(1819 + 3566*t2 + 315*Power(t2,2) - 
                  446*Power(t2,3) + 5*Power(t2,4)) - 
               Power(t1,3)*(3109 + 3555*t2 + 17*Power(t2,2) - 
                  192*Power(t2,3) + 5*Power(t2,4)) - 
               t1*(303 + 1570*t2 + 314*Power(t2,2) - 464*Power(t2,3) + 
                  31*Power(t2,4))) - 
            Power(s2,2)*(-154 + Power(t1,7) - 515*t2 - 
               599*Power(t2,2) - 145*Power(t2,3) + 103*Power(t2,4) + 
               23*Power(t1,6)*(1 + t2) + 
               Power(t1,5)*(-851 + 15*t2 + 59*Power(t2,2)) - 
               4*Power(t1,4)*
                (-649 - 5*t2 + 147*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,3)*(-2846 + 860*t2 + 2794*Power(t2,2) + 
                  325*Power(t2,3) - 52*Power(t2,4)) + 
               t1*(198 + 1978*t2 + 2910*Power(t2,2) + 
                  610*Power(t2,3) - 251*Power(t2,4) - 5*Power(t2,5)) + 
               Power(t1,2)*(1033 - 2381*t2 - 4566*Power(t2,2) - 
                  776*Power(t2,3) + 216*Power(t2,4) + Power(t2,5))) + 
            s2*(349 + 690*t2 + 279*Power(t2,2) - 181*Power(t2,3) - 
               39*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,7)*(14 + 3*t2) + 
               Power(t1,6)*(-345 + 74*t2 + 13*Power(t2,2)) - 
               Power(t1,5)*(-181 + 538*t2 + 240*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,4)*(2584 + 3458*t2 + 1365*Power(t2,2) + 
                  6*Power(t2,3) - 12*Power(t2,4)) + 
               Power(t1,2)*(5480 + 8643*t2 + 3321*Power(t2,2) - 
                  230*Power(t2,3) - 213*Power(t2,4) - 6*Power(t2,5)) - 
               Power(t1,3)*(5983 + 8264*t2 + 3143*Power(t2,2) - 
                  30*Power(t2,3) - 117*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(-2280 - 4066*t2 - 1594*Power(t2,2) + 
                  375*Power(t2,3) + 148*Power(t2,4) + 6*Power(t2,5))) + 
            Power(s2,3)*(-264 + 3*Power(t1,6) - 203*t2 + 
               402*Power(t2,2) + 236*Power(t2,3) - 32*Power(t2,4) - 
               5*Power(t2,5) + Power(t1,5)*(4 + 86*t2) + 
               Power(t1,4)*(-506 - 209*t2 + 103*Power(t2,2)) - 
               2*Power(t1,3)*
                (-850 - 407*t2 + 205*Power(t2,2) + 62*Power(t2,3)) + 
               Power(t1,2)*(-2177 - 1447*t2 + 1227*Power(t2,2) + 
                  606*Power(t2,3) - 45*Power(t2,4)) + 
               t1*(1240 + 959*t2 - 1301*Power(t2,2) - 716*Power(t2,3) + 
                  69*Power(t2,4) + 7*Power(t2,5))) - 
            Power(s1,4)*(Power(s2,5) - 
               2*Power(-1 + t1,2)*(4 + 3*t1 + 5*Power(t1,2)) + 
               Power(s2,4)*(-2 - 6*t1 + 2*t2) + 
               s2*(33 - 10*Power(t1,4) + 18*t1*(-6 + t2) - 26*t2 + 
                  Power(t1,3)*(-39 + 2*t2) + 3*Power(t1,2)*(41 + 2*t2)) \
+ Power(s2,3)*(13*Power(t1,2) + 17*(1 + t2) - t1*(26 + 27*t2)) + 
               Power(s2,2)*(73 - 8*Power(t1,3) + 
                  Power(t1,2)*(112 + 29*t2) - t1*(165 + 41*t2))) + 
            Power(s1,3)*(-2*Power(s2,6) + Power(s2,5)*(-11 + 31*t1) - 
               Power(s2,4)*(33 + 12*Power(t1,2) + 5*t2 - 
                  8*Power(t2,2) + t1*(-79 + 31*t2)) - 
               (-1 + t1)*(41 + 10*Power(t1,4) - 25*t2 + 
                  Power(t1,3)*(-142 + 5*t2) - t1*(114 + 53*t2) + 
                  Power(t1,2)*(202 + 73*t2)) + 
               Power(s2,3)*(-206 - 76*Power(t1,3) + 137*t2 + 
                  38*Power(t2,2) + 3*Power(t1,2)*(-93 + 50*t2) + 
                  t1*(555 - 267*t2 - 58*Power(t2,2))) + 
               s2*(102 - 25*Power(t1,5) + 183*t2 - 44*Power(t2,2) + 
                  Power(t1,4)*(11 + 27*t2) + 
                  4*t1*(-53 - 163*t2 + 3*Power(t2,2)) + 
                  Power(t1,3)*(28 - 414*t2 + 8*Power(t2,2)) + 
                  Power(t1,2)*(98 + 852*t2 + 24*Power(t2,2))) + 
               Power(s2,2)*(-125 + 100*Power(t1,4) + 
                  Power(t1,3)*(175 - 176*t2) + 386*t2 + 
                  t1*(640 - 974*t2 - 74*Power(t2,2)) + 
                  Power(t1,2)*(-806 + 816*t2 + 46*Power(t2,2)))) + 
            Power(s1,2)*(-Power(s2,7) + 
               Power(s2,6)*(-7 + 21*t1 + 4*t2) + 
               Power(s2,5)*(28 + 63*Power(t1,2) + 60*t2 + 
                  6*Power(t2,2) - 4*t1*(28 + 29*t2)) + 
               Power(s2,4)*(-120 - 248*Power(t1,3) + 230*t2 + 
                  15*Power(t2,2) - 12*Power(t2,3) + 
                  Power(t1,2)*(250 + 231*t2) + 
                  t1*(114 - 563*t2 + 45*Power(t2,2))) - 
               (-1 + t1)*(15*Power(t1,5) + Power(t1,4)*(6 - 87*t2) + 
                  Power(t1,2)*(488 - 850*t2 - 153*Power(t2,2)) + 
                  Power(t1,3)*(-328 + 596*t2 + 15*Power(t2,2)) + 
                  3*(7 - 74*t2 + 19*Power(t2,2)) + 
                  t1*(-208 + 572*t2 + 81*Power(t2,2))) + 
               s2*(216 + 18*Power(t1,6) - 365*t2 - 306*Power(t2,2) + 
                  36*Power(t2,3) + Power(t1,5)*(-53 + 33*t2) - 
                  4*Power(t1,4)*(-251 + 34*t2 + 24*Power(t2,2)) + 
                  Power(t1,3)*
                   (-3151 + 404*t2 + 828*Power(t2,2) - 12*Power(t2,3)) \
+ t1*(-1623 + 904*t2 + 1128*Power(t2,2) + 12*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (-1795 + 423*t2 + 774*Power(t2,2) + 18*Power(t2,3))) \
+ Power(s2,3)*(211 + 285*Power(t1,4) + 648*t2 - 255*Power(t2,2) - 
                  42*Power(t2,3) - Power(t1,3)*(369 + 110*t2) + 
                  Power(t1,2)*(428 + 1428*t2 - 306*Power(t2,2)) + 
                  t1*(-534 - 1952*t2 + 525*Power(t2,2) + 
                     62*Power(t2,3))) + 
               Power(s2,2)*(624 - 128*Power(t1,5) + 
                  Power(t1,4)*(325 - 83*t2) + 308*t2 - 
                  656*Power(t2,2) + 
                  Power(t1,3)*(-1850 - 747*t2 + 380*Power(t2,2)) + 
                  Power(t1,2)*
                   (3762 + 2190*t2 - 1512*Power(t2,2) - 34*Power(t2,3)) \
+ t1*(-2743 - 1618*t2 + 1704*Power(t2,2) + 66*Power(t2,3)))) + 
            s1*(Power(s2,7)*(-1 + t1 + 2*t2) + 
               Power(s2,6)*(34 + 49*Power(t1,2) + 28*t2 - 
                  2*Power(t2,2) - t1*(83 + 56*t2)) - 
               Power(s2,5)*(41 + 137*Power(t1,3) + 6*t2 + 
                  75*Power(t2,2) + 8*Power(t2,3) + 
                  2*Power(t1,2)*(-92 + 11*t2) + 
                  t1*(6 - 70*t2 - 127*Power(t2,2))) + 
               (-1 + t1)*(2 + 5*Power(t1,6) + 206*t2 - 
                  381*Power(t2,2) + 71*Power(t2,3) + 
                  Power(t1,5)*(-161 + 39*t2) + 
                  Power(t1,4)*(1477 - 157*t2 - 84*Power(t2,2)) + 
                  Power(t1,2)*
                   (2697 + 1061*t2 - 1094*Power(t2,2) - 71*Power(t2,3)) \
- t1*(798 + 774*t2 - 922*Power(t2,2) + 5*Power(t2,3)) + 
                  Power(t1,3)*
                   (-3219 - 387*t2 + 646*Power(t2,2) + 5*Power(t2,3))) + 
               Power(s2,4)*(170 + 162*Power(t1,4) + 97*t2 - 
                  307*Power(t2,2) - 23*Power(t2,3) + 8*Power(t2,4) + 
                  Power(t1,3)*(-218 + 299*t2) + 
                  Power(t1,2)*(197 - 309*t2 - 348*Power(t2,2)) - 
                  t1*(311 + 79*t2 - 757*Power(t2,2) + 21*Power(t2,3))) + 
               Power(s2,3)*(291 - 94*Power(t1,5) + 
                  Power(t1,4)*(172 - 412*t2) - 631*t2 - 
                  678*Power(t2,2) + 167*Power(t2,3) + 23*Power(t2,4) + 
                  Power(t1,3)*(-839 + 893*t2 + 310*Power(t2,2)) + 
                  Power(t1,2)*
                   (1728 - 1829*t2 - 1755*Power(t2,2) + 
                     214*Power(t2,3)) + 
                  t1*(-1258 + 1937*t2 + 2113*Power(t2,2) - 
                     353*Power(t2,3) - 33*Power(t2,4))) + 
               Power(s2,2)*(-596 + 24*Power(t1,6) - 1174*t2 - 
                  328*Power(t2,2) + 446*Power(t2,3) + 
                  Power(t1,5)*(13 + 204*t2) + 
                  Power(t1,4)*(133 - 964*t2 - 49*Power(t2,2)) + 
                  Power(t1,3)*
                   (632 + 4634*t2 + 897*Power(t2,2) - 264*Power(t2,3)) \
+ t1*(2136 + 5494*t2 + 1588*Power(t2,2) - 1146*Power(t2,3) - 
                     29*Power(t2,4)) + 
                  Power(t1,2)*
                   (-2342 - 8174*t2 - 2160*Power(t2,2) + 
                     1024*Power(t2,3) + 11*Power(t2,4))) - 
               s2*(626 + 3*Power(t1,7) + 520*t2 - 444*Power(t2,2) - 
                  195*Power(t2,3) + 14*Power(t2,4) + 
                  Power(t1,6)*(39 + 35*t2) + 
                  Power(t1,5)*(-473 - 248*t2 + 6*Power(t2,2)) + 
                  Power(t1,4)*
                   (3600 + 2134*t2 - 119*Power(t2,2) - 71*Power(t2,3)) + 
                  Power(t1,2)*
                   (8884 + 6701*t2 - 978*Power(t2,2) - 
                     1032*Power(t2,3) - 24*Power(t2,4)) + 
                  Power(t1,3)*
                   (-8672 - 5924*t2 + 462*Power(t2,2) + 
                     570*Power(t2,3) - 8*Power(t2,4)) + 
                  t1*(-4007 - 3216*t2 + 1067*Power(t2,2) + 
                     732*Power(t2,3) + 18*Power(t2,4))))) - 
         Power(-1 + t1,2)*(Power(s1,5)*s2*
             (-2*Power(s2,3) + 5*Power(s2,2)*(-1 + t1) + 
               s2*(-3 + 9*t1 - 4*Power(t1,2)) + 
               t1*(3 - 4*t1 + Power(t1,2))) - 
            Power(s2,7)*t2*(1 - t1 + 3*t2) - 
            Power(s2,6)*(1 - 3*t2 - 16*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,2)*(1 + 2*t2) + t1*(-2 + t2 - 4*Power(t2,2))) + 
            Power(s2,5)*(6 + 33*t2 + 24*Power(t2,2) - 6*Power(t2,3) - 
               3*Power(t2,4) + Power(t1,3)*(2 + t2) + 
               2*Power(t1,2)*(1 + 16*t2 + Power(t2,2)) - 
               t1*(10 + 66*t2 + 73*Power(t2,2) + 12*Power(t2,3))) - 
            Power(s2,4)*(-17 + Power(t1,4) + 26*t2 + 113*Power(t2,2) + 
               60*Power(t2,3) + 
               Power(t1,3)*(35 + 61*t2 + 5*Power(t2,2)) - 
               Power(t1,2)*(90 + 115*t2 + 78*Power(t2,2) + 
                  7*Power(t2,3)) + 
               t1*(71 + 28*t2 - 92*Power(t2,2) - 45*Power(t2,3) - 
                  6*Power(t2,4))) + 
            (-1 + t1)*(-3 + 24*t2 + 14*Power(t2,2) - 12*Power(t2,3) + 
               Power(t2,4) + Power(t1,5)*(-62 + 4*t2) + 
               Power(t1,4)*(228 + 106*t2 - 24*Power(t2,2)) + 
               Power(t1,3)*(-319 - 323*t2 + 7*Power(t2,2) + 
                  25*Power(t2,3)) + 
               Power(t1,2)*(198 + 348*t2 + 53*Power(t2,2) - 
                  44*Power(t2,3) - 5*Power(t2,4)) + 
               2*t1*(-21 - 79*t2 - 26*Power(t2,2) + 16*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s2,3)*(Power(t1,4)*(59 + 41*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(-105 + 52*t2 - 23*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(t1,2)*(9 + 473*t2 + 422*Power(t2,2) + 
                  31*Power(t2,3) + 6*Power(t2,4)) + 
               t1*(97 + 593*t2 + 681*Power(t2,2) + 149*Power(t2,3) - 
                  28*Power(t2,4) + Power(t2,5)) + 
               3*(-14 - 71*t2 - 89*Power(t2,2) - 29*Power(t2,3) + 
                  6*Power(t2,4) + Power(t2,5))) + 
            s2*(-52 + 8*Power(t1,6) - 122*t2 - 73*Power(t2,2) + 
               13*Power(t2,3) + 9*Power(t2,4) + Power(t2,5) - 
               2*Power(t1,5)*(-86 - 29*t2 + Power(t2,2)) + 
               Power(t1,4)*(-776 - 593*t2 - 76*Power(t2,2) + 
                  21*Power(t2,3)) + 
               Power(t1,3)*(1278 + 1481*t2 + 368*Power(t2,2) - 
                  55*Power(t2,3) - 24*Power(t2,4)) + 
               t1*(378 + 722*t2 + 345*Power(t2,2) - 30*Power(t2,3) - 
                  37*Power(t2,4) - 6*Power(t2,5)) + 
               Power(t1,2)*(-1008 - 1546*t2 - 563*Power(t2,2) + 
                  53*Power(t2,3) + 51*Power(t2,4) + 5*Power(t2,5))) - 
            Power(s2,2)*(107 + 256*t2 + 209*Power(t2,2) + 
               38*Power(t2,3) - 26*Power(t2,4) - 4*Power(t2,5) + 
               2*Power(t1,5)*(18 + 5*t2) + 
               Power(t1,4)*(87 + 162*t2 - 3*Power(t2,2) + 
                  2*Power(t2,3)) - 
               Power(t1,3)*(559 + 971*t2 + 394*Power(t2,2) - 
                  26*Power(t2,3) + 3*Power(t2,4)) + 
               Power(t1,2)*(820 + 1647*t2 + 973*Power(t2,2) + 
                  49*Power(t2,3) - 49*Power(t2,4) + Power(t2,5)) + 
               t1*(-491 - 1104*t2 - 793*Power(t2,2) - 101*Power(t2,3) + 
                  64*Power(t2,4) + 7*Power(t2,5))) - 
            Power(s1,4)*(7*Power(s2,5) + Power(-1 + t1,2)*t1*(3 + t1) + 
               Power(s2,4)*(5 - 15*t1 - 8*t2) + 
               s2*(-6 - t2 + Power(t1,3)*(17 + 4*t2) - 
                  3*Power(t1,2)*(12 + 7*t2) + 2*t1*(13 + 9*t2)) + 
               Power(s2,3)*(-18 + 9*Power(t1,2) - 23*t2 + 
                  t1*(21 + 19*t2)) - 
               Power(s2,2)*(23 + Power(t1,3) + 16*t2 + 
                  Power(t1,2)*(44 + 15*t2) - t1*(58 + 43*t2))) - 
            Power(s1,3)*(8*Power(s2,6) - 
               Power(s2,5)*(9 + 17*t1 + 24*t2) + 
               (-1 + t1)*(-3 + Power(t1,4) - Power(t1,3)*(-19 + t2) - 
                  t2 - Power(t1,2)*(30 + 17*t2) + t1*(14 + 19*t2)) + 
               Power(s2,4)*(-81 + 14*Power(t1,2) - 11*t2 + 
                  12*Power(t2,2) + t1*(71 + 47*t2)) + 
               Power(s2,2)*(-19 + 6*Power(t1,4) + 
                  Power(t1,3)*(2 - 4*t2) + 99*t2 + 34*Power(t2,2) + 
                  t1*(87 - 256*t2 - 82*Power(t2,2)) + 
                  Power(t1,2)*(-88 + 205*t2 + 20*Power(t2,2))) - 
               Power(s2,3)*(93 + 10*Power(t1,3) - 79*t2 - 
                  42*Power(t2,2) + Power(t1,2)*(76 + 22*t2) + 
                  t1*(-203 + 109*t2 + 26*Power(t2,2))) + 
               s2*(13 - Power(t1,5) + 30*t2 + 4*Power(t2,2) + 
                  Power(t1,4)*(11 + 3*t2) - 
                  3*Power(t1,3)*(13 + 29*t2 + 2*Power(t2,2)) - 
                  t1*(39 + 127*t2 + 42*Power(t2,2)) + 
                  Power(t1,2)*(57 + 177*t2 + 44*Power(t2,2)))) + 
            Power(s1,2)*(-3*Power(s2,7) + 
               2*Power(s2,6)*(7 + 3*t1 + 11*t2) - 
               Power(s2,5)*(-25 + 7*Power(t1,2) + 28*t2 + 
                  30*Power(t2,2) + t1*(65 + 42*t2)) - 
               (-1 + t1)*(-5 + 14*t2 + Power(t2,2) + 
                  2*Power(t1,4)*(9 + t2) + 
                  t1*(27 - 52*t2 - 33*Power(t2,2)) - 
                  Power(t1,3)*(4 + 71*t2 + Power(t2,2)) + 
                  Power(t1,2)*(-34 + 104*t2 + 33*Power(t2,2))) + 
               Power(s2,4)*(-120 + 9*Power(t1,3) - 223*t2 - 
                  7*Power(t2,2) + 8*Power(t2,3) + 
                  Power(t1,2)*(55 + 26*t2) + 
                  t1*(108 + 197*t2 + 55*Power(t2,2))) + 
               Power(s2,2)*(-188 + Power(t1,5) - 69*t2 + 
                  155*Power(t2,2) + 36*Power(t2,3) + 
                  Power(t1,4)*(-9 + 11*t2) + 
                  Power(t1,3)*(434 - 48*t2 - 8*Power(t2,2)) + 
                  t1*(768 + 237*t2 - 402*Power(t2,2) - 
                     78*Power(t2,3)) + 
                  Power(t1,2)*
                   (-998 - 169*t2 + 327*Power(t2,2) + 10*Power(t2,3))) \
- Power(s2,3)*(280 + 6*Power(t1,4) + 261*t2 - 122*Power(t2,2) - 
                  38*Power(t2,3) + 7*Power(t1,3)*(-1 + 2*t2) + 
                  Power(t1,2)*(475 + 177*t2 + 23*Power(t2,2)) + 
                  t1*(-725 - 532*t2 + 183*Power(t2,2) + 14*Power(t2,3))\
) + s2*(-40 + 33*t2 + 51*Power(t2,2) + 6*Power(t2,3) - 
                  Power(t1,5)*(2 + 3*t2) + 
                  Power(t1,4)*(-85 + 61*t2 + 6*Power(t2,2)) - 
                  Power(t1,3)*
                   (-354 + 175*t2 + 147*Power(t2,2) + 4*Power(t2,3)) - 
                  3*t1*(-83 + 35*t2 + 71*Power(t2,2) + 
                     16*Power(t2,3)) + 
                  Power(t1,2)*
                   (-477 + 195*t2 + 297*Power(t2,2) + 46*Power(t2,3)))) \
+ s1*(Power(s2,7)*(1 - t1 + 6*t2) + 
               Power(s2,6)*(-3 + t1 + 2*Power(t1,2) - 30*t2 - 
                  10*t1*t2 - 20*Power(t2,2)) + 
               Power(s2,5)*(-31 - Power(t1,3) - 47*t2 + 
                  25*Power(t2,2) + 16*Power(t2,3) + 
                  Power(t1,2)*(-30 + 7*t2) + 
                  t1*(62 + 134*t2 + 37*Power(t2,2))) - 
               (-1 + t1)*(5 + 25*t2 - 23*Power(t2,2) + Power(t2,3) + 
                  2*Power(t1,5)*(1 + t2) + 
                  Power(t1,4)*(101 - 46*t2 - 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (264 + 109*t2 - 118*Power(t2,2) - 23*Power(t2,3)) + 
                  Power(t1,3)*
                   (-279 + 5*t2 + 77*Power(t2,2) + Power(t2,3)) + 
                  t1*(-92 - 99*t2 + 70*Power(t2,2) + 21*Power(t2,3))) + 
               Power(s2,4)*(25 + Power(t1,3)*(50 - 9*t2) + 236*t2 + 
                  202*Power(t2,2) + Power(t2,3) - 2*Power(t2,4) - 
                  Power(t1,2)*(94 + 120*t2 + 19*Power(t2,2)) - 
                  t1*(-19 + 211*t2 + 171*Power(t2,2) + 29*Power(t2,3))) \
+ Power(s2,3)*(218 + 542*t2 + 255*Power(t2,2) - 79*Power(t2,3) - 
                  17*Power(t2,4) + Power(t1,4)*(-22 + 8*t2) + 
                  Power(t1,3)*(-98 + 7*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (513 + 898*t2 + 132*Power(t2,2) + 16*Power(t2,3)) + 
                  t1*(-611 - 1397*t2 - 478*Power(t2,2) + 
                     123*Power(t2,3) + Power(t2,4))) + 
               s2*(97 + 2*Power(t1,6) + 120*t2 - 33*Power(t2,2) - 
                  36*Power(t2,3) - 4*Power(t2,4) + 
                  Power(t1,5)*(-71 + 11*t2 + 2*Power(t2,2)) + 
                  Power(t1,4)*
                   (610 + 136*t2 - 71*Power(t2,2) - 3*Power(t2,3)) + 
                  Power(t1,2)*
                   (1452 + 1042*t2 - 191*Power(t2,2) - 
                     207*Power(t2,3) - 24*Power(t2,4)) + 
                  Power(t1,3)*
                   (-1451 - 696*t2 + 191*Power(t2,2) + 
                     101*Power(t2,3) + Power(t2,4)) + 
                  t1*(-639 - 611*t2 + 96*Power(t2,2) + 
                     149*Power(t2,3) + 27*Power(t2,4))) + 
               Power(s2,2)*(258 + 392*t2 + 88*Power(t2,2) - 
                  105*Power(t2,3) - 19*Power(t2,4) - 
                  2*Power(t1,5)*(1 + t2) + 
                  Power(t1,4)*(203 + t2 - 3*Power(t2,2)) + 
                  4*Power(t1,3)*(-257 - 200*t2 + 19*Power(t2,2)) + 
                  Power(t1,2)*
                   (1688 + 1929*t2 + 130*Power(t2,2) - 215*Power(t2,3)) \
+ t1*(-1119 - 1536*t2 - 251*Power(t2,2) + 268*Power(t2,3) + 
                     37*Power(t2,4))))) + 
         s*(-1 + t1)*(Power(s1,5)*s2*
             (2*Power(s2,3) + Power(s2,2)*(7 - 9*t1) + 
               s2*(4 - 17*t1 + 9*Power(t1,2)) - 
               2*(1 + 2*t1 - 4*Power(t1,2) + Power(t1,3))) - 
            Power(s2,7)*(1 + Power(t1,2) + 9*t2 + 4*Power(t2,2) - 
               t1*(2 + 9*t2)) + 
            Power(s2,6)*(1 + 2*Power(t1,3) + 18*t2 + t1*(5 - 9*t2)*t2 + 
               15*Power(t2,2) + 6*Power(t2,3) - Power(t1,2)*(3 + 23*t2)) \
+ Power(s2,5)*(18 - Power(t1,4) + t2 - 17*Power(t2,2) + 22*Power(t2,3) + 
               2*Power(t1,3)*(-9 + 13*t2) + 
               Power(t1,2)*(57 + 22*t2 + 31*Power(t2,2)) - 
               t1*(56 + 49*t2 - 12*Power(t2,2) + 8*Power(t2,3))) - 
            Power(s2,4)*(73 + 95*t2 - 127*Power(t2,2) - 
               112*Power(t2,3) + Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,4)*(-32 + 17*t2) + 
               Power(t1,3)*(2 - 35*t2 + 33*Power(t2,2)) + 
               Power(t1,2)*(165 + 203*t2 + 49*Power(t2,2) - 
                  5*Power(t2,3)) + 
               t1*(-208 - 280*t2 + 113*Power(t2,2) + 155*Power(t2,3) - 
                  7*Power(t2,4))) - 
            (-1 + t1)*(-21 + 119*t2 + 63*Power(t2,2) - 75*Power(t2,3) + 
               10*Power(t2,4) + 2*Power(t1,6)*(3 + t2) - 
               4*Power(t1,5)*(82 + 5*t2) - 
               Power(t1,4)*(-1159 - 617*t2 + 83*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,2)*(959 + 1749*t2 + 233*Power(t2,2) - 
                  205*Power(t2,3) - 12*Power(t2,4)) - 
               Power(t1,3)*(1589 + 1682*t2 - 104*Power(t2,3) + 
                  Power(t2,4)) + 
               t1*(-186 - 782*t2 - 219*Power(t2,2) + 180*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s2,3)*(-45 + 7*Power(t1,5)*(-2 + t2) + 260*t2 + 
               565*Power(t2,2) + 215*Power(t2,3) - 42*Power(t2,4) - 
               3*Power(t2,5) + 
               Power(t1,4)*(-273 - 59*t2 + 17*Power(t2,2)) + 
               Power(t1,3)*(881 + 148*t2 - 86*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(-932 + 216*t2 + 1143*Power(t2,2) + 
                  213*Power(t2,3) - 17*Power(t2,4)) + 
               t1*(383 - 572*t2 - 1583*Power(t2,2) - 474*Power(t2,3) + 
                  73*Power(t2,4) + Power(t2,5))) - 
            Power(s2,2)*(-277 - 686*t2 - 591*Power(t2,2) - 
               119*Power(t2,3) + 77*Power(t2,4) + 4*Power(t2,5) + 
               Power(t1,6)*(1 + 2*t2) + 
               Power(t1,5)*(-305 + t2 + 5*Power(t2,2)) + 
               Power(t1,4)*(540 - 369*t2 - 132*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,3)*(400 + 2347*t2 + 1608*Power(t2,2) + 
                  22*Power(t2,3) - 8*Power(t2,4)) + 
               t1*(1123 + 2909*t2 + 2458*Power(t2,2) + 
                  389*Power(t2,3) - 185*Power(t2,4) - 9*Power(t2,5)) - 
               Power(t1,2)*(1482 + 4204*t2 + 3330*Power(t2,2) + 
                  328*Power(t2,3) - 146*Power(t2,4) + 3*Power(t2,5))) + 
            s2*(216 + 2*Power(t1,7) + 471*t2 + 248*Power(t2,2) - 
               74*Power(t2,3) - 28*Power(t2,4) - Power(t2,5) + 
               Power(t1,6)*(-89 + 17*t2 + 2*Power(t2,2)) - 
               Power(t1,5)*(447 + 282*t2 + 47*Power(t2,2)) + 
               Power(t1,4)*(2690 + 2364*t2 + 523*Power(t2,2) - 
                  42*Power(t2,3) - Power(t2,4)) + 
               Power(t1,2)*(3921 + 5997*t2 + 2210*Power(t2,2) - 
                  141*Power(t2,3) - 156*Power(t2,4) - 11*Power(t2,5)) - 
               Power(t1,3)*(4777 + 5772*t2 + 1695*Power(t2,2) - 
                  107*Power(t2,3) - 76*Power(t2,4) + Power(t2,5)) + 
               t1*(-1516 - 2795*t2 - 1239*Power(t2,2) + 
                  146*Power(t2,3) + 111*Power(t2,4) + 13*Power(t2,5))) + 
            Power(s1,4)*(Power(-1 + t1,2)*(2 + 9*t1 + 5*Power(t1,2)) - 
               2*Power(s2,4)*(-5 + 6*t1 + 5*t2) + 
               Power(s2,3)*(-29 + 13*Power(t1,2) - 31*t2 + 
                  t1*(30 + 37*t2)) + 
               s2*(5*Power(t1,4) + 7*(-3 + t2) + 
                  Power(t1,3)*(41 + 7*t2) + t1*(80 + 29*t2) - 
                  Power(t1,2)*(103 + 43*t2)) - 
               Power(s2,2)*(63 + 6*Power(t1,3) + 20*t2 + 
                  3*Power(t1,2)*(34 + 11*t2) - t1*(149 + 77*t2))) - 
            Power(s1,3)*(6*Power(s2,6) + Power(s2,5)*(1 + 17*t1) - 
               Power(s2,4)*(-115 + 60*Power(t1,2) - 17*t2 + 
                  20*Power(t2,2) + t1*(105 + 17*t2)) - 
               (-1 + t1)*(-17 + Power(t1,4) - 
                  4*Power(t1,3)*(-20 + t2) + 4*t2 + 60*t1*(1 + t2) - 
                  Power(t1,2)*(121 + 60*t2)) + 
               Power(s2,3)*(244 + 77*Power(t1,3) + 
                  Power(t1,2)*(229 - 22*t2) - 161*t2 - 54*Power(t2,2) + 
                  t1*(-580 + 239*t2 + 58*Power(t2,2))) - 
               Power(s2,2)*(-84 + 46*Power(t1,4) + 
                  Power(t1,3)*(79 - 40*t2) + 292*t2 + 40*Power(t2,2) + 
                  Power(t1,2)*(-477 + 578*t2 + 42*Power(t2,2)) - 
                  2*t1*(-204 + 367*t2 + 69*Power(t2,2))) + 
               s2*(-55 + 6*Power(t1,5) - 109*t2 + 8*Power(t2,2) - 
                  Power(t1,4)*(13 + 4*t2) + 
                  Power(t1,3)*(49 + 271*t2 + 8*Power(t2,2)) + 
                  t1*(132 + 423*t2 + 76*Power(t2,2)) - 
                  Power(t1,2)*(123 + 573*t2 + 92*Power(t2,2)))) + 
            Power(s1,2)*(-4*Power(s2,7) + 
               Power(s2,6)*(29 - 23*t1 + 18*t2) + 
               Power(s2,5)*(-21 + 81*Power(t1,2) + 36*t2 + 
                  2*t1*(-17 + 7*t2)) - 
               Power(s2,4)*(-64 + 108*Power(t1,3) - 362*t2 - 
                  3*Power(t2,2) + 20*Power(t2,3) + 
                  Power(t1,2)*(22 + 71*t2) + 
                  t1*(2 + 429*t2 - 9*Power(t2,2))) - 
               (-1 + t1)*(19 + 4*Power(t1,5) - 85*t2 + 12*Power(t2,2) - 
                  Power(t1,4)*(47 + 23*t2) + 
                  Power(t1,2)*(209 - 447*t2 - 120*Power(t2,2)) + 
                  Power(t1,3)*(-68 + 312*t2 + 6*Power(t2,2)) + 
                  3*t1*(-41 + 84*t2 + 34*Power(t2,2))) - 
               Power(s2,2)*(-585 + 21*Power(t1,5) - 243*t2 + 
                  472*Power(t2,2) + 40*Power(t2,3) + 
                  Power(t1,4)*(-88 + 82*t2) - 
                  2*Power(t1,3)*(-759 - 57*t2 + 53*Power(t2,2)) + 
                  t1*(2486 + 1023*t2 - 1206*Power(t2,2) - 
                     122*Power(t2,3)) + 
                  2*Power(t1,2)*
                   (-1667 - 532*t2 + 498*Power(t2,2) + 9*Power(t2,3))) + 
               Power(s2,3)*(563 + 72*Power(t1,4) + 670*t2 - 
                  277*Power(t2,2) - 46*Power(t2,3) + 
                  Power(t1,3)*(-71 + 111*t2) + 
                  Power(t1,2)*(966 + 720*t2 - 100*Power(t2,2)) + 
                  t1*(-1474 - 1603*t2 + 461*Power(t2,2) + 
                     42*Power(t2,3))) + 
               s2*(152 + 3*Power(t1,6) - 163*t2 - 183*Power(t2,2) + 
                  2*Power(t2,3) + Power(t1,5)*(-10 + 13*t2) + 
                  Power(t1,4)*(497 - 151*t2 - 24*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1731 + 427*t2 + 495*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(-1026 + 427*t2 + 717*Power(t2,2) + 
                     94*Power(t2,3)) - 
                  Power(t1,2)*
                   (-2117 + 565*t2 + 993*Power(t2,2) + 98*Power(t2,3)))) \
+ s1*(Power(s2,7)*(9 - 9*t1 + 8*t2) + 
               Power(s2,6)*(21*Power(t1,2) + t1*(-1 + 32*t2) - 
                  2*(10 + 22*t2 + 9*Power(t2,2))) + 
               Power(s2,5)*(-10 - 23*Power(t1,3) + 34*t2 - 
                  57*Power(t2,2) - Power(t1,2)*(37 + 116*t2) + 
                  t1*(70 + 30*t2 + 11*Power(t2,2))) + 
               Power(s2,4)*(118 + 18*Power(t1,4) - 207*t2 - 
                  359*Power(t2,2) + 5*Power(t2,3) + 10*Power(t2,4) + 
                  Power(t1,3)*(-13 + 157*t2) + 
                  Power(t1,2)*(179 + 23*t2 + 6*Power(t2,2)) + 
                  t1*(-302 + 163*t2 + 479*Power(t2,2) - 21*Power(t2,3))) \
+ (-1 + t1)*(15 + 116*t2 - 143*Power(t2,2) + 20*Power(t2,3) + 
                  2*Power(t1,5)*(-15 + 8*t2) + 
                  Power(t1,4)*(613 - 156*t2 - 25*Power(t2,2)) + 
                  Power(t1,2)*
                   (1331 + 562*t2 - 531*Power(t2,2) - 76*Power(t2,3)) + 
                  2*Power(t1,3)*
                   (-747 - 48*t2 + 168*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(-432 - 454*t2 + 372*Power(t2,2) + 52*Power(t2,3))) \
+ Power(s2,2)*(-731 + 2*Power(t1,6) - 1148*t2 - 278*Power(t2,2) + 
                  320*Power(t2,3) + 20*Power(t2,4) + 
                  Power(t1,5)*(26 + 34*t2) + 
                  Power(t1,4)*(-442 - 222*t2 + 40*Power(t2,2)) + 
                  Power(t1,3)*
                   (2479 + 3048*t2 + 57*Power(t2,2) - 68*Power(t2,3)) + 
                  t1*(3060 + 4830*t2 + 1004*Power(t2,2) - 
                     806*Power(t2,3) - 53*Power(t2,4)) - 
                  Power(t1,2)*
                   (4394 + 6506*t2 + 915*Power(t2,2) - 666*Power(t2,3) + 
                     3*Power(t2,4))) - 
               Power(s2,3)*(248 + 9*Power(t1,5) + 1121*t2 + 
                  641*Power(t2,2) - 187*Power(t2,3) - 19*Power(t2,4) + 
                  2*Power(t1,4)*(-13 + 54*t2) + 
                  Power(t1,3)*(113 - 219*t2 + 38*Power(t2,2)) + 
                  Power(t1,2)*
                   (167 + 2169*t2 + 704*Power(t2,2) - 82*Power(t2,3)) + 
                  t1*(-511 - 3067*t2 - 1497*Power(t2,2) + 
                     325*Power(t2,3) + 13*Power(t2,4))) + 
               s2*(-394 - 425*t2 + 182*Power(t2,2) + 123*Power(t2,3) + 
                  2*Power(t2,4) - 2*Power(t1,6)*(8 + 3*t2) + 
                  Power(t1,5)*(318 + 27*t2 - 7*Power(t2,2)) + 
                  Power(t1,4)*
                   (-2497 - 895*t2 + 180*Power(t2,2) + 16*Power(t2,3)) + 
                  t1*(2589 + 2311*t2 - 441*Power(t2,2) - 
                     485*Power(t2,3) - 56*Power(t2,4)) + 
                  Power(t1,3)*
                   (5874 + 3266*t2 - 485*Power(t2,2) - 341*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  Power(t1,2)*
                   (-5874 - 4282*t2 + 583*Power(t2,2) + 679*Power(t2,3) + 
                     52*Power(t2,4))))))*R1q(1 - s + s2 - t1))/
     ((-1 + s2)*(-s + s2 - t1)*(1 - s + s2 - t1)*(-1 + t1)*
       Power(-1 + s + t1,3)*(-1 + s1 + t1 - t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) - 
    (8*(6 + 67*s2 + 50*Power(s2,2) - 35*Power(s2,3) + 4*Power(s2,4) - 
         96*t1 - 350*s2*t1 - 36*Power(s2,2)*t1 + 64*Power(s2,3)*t1 - 
         6*Power(s2,4)*t1 + 290*Power(t1,2) + 449*s2*Power(t1,2) - 
         56*Power(s2,2)*Power(t1,2) - 29*Power(s2,3)*Power(t1,2) + 
         2*Power(s2,4)*Power(t1,2) - 300*Power(t1,3) - 
         150*s2*Power(t1,3) + 42*Power(s2,2)*Power(t1,3) + 
         100*Power(t1,4) - 16*s2*Power(t1,4) + 
         3*Power(s1,5)*s2*(-2 + s2 + t1) + 
         2*Power(s,5)*(-1 + s2)*(-1 + 2*t1 - t2) + 72*t2 + 109*s2*t2 + 
         96*Power(s2,2)*t2 + 53*Power(s2,3)*t2 - 40*Power(s2,4)*t2 + 
         4*Power(s2,5)*t2 - 264*t1*t2 - 375*s2*t1*t2 - 
         350*Power(s2,2)*t1*t2 + 23*Power(s2,3)*t1*t2 + 
         34*Power(s2,4)*t1*t2 - 2*Power(s2,5)*t1*t2 + 
         306*Power(t1,2)*t2 + 398*s2*Power(t1,2)*t2 + 
         200*Power(s2,2)*Power(t1,2)*t2 - 54*Power(s2,3)*Power(t1,2)*t2 - 
         106*Power(t1,3)*t2 - 116*s2*Power(t1,3)*t2 + 
         20*Power(s2,2)*Power(t1,3)*t2 - 8*Power(t1,4)*t2 + 
         44*Power(t2,2) + 95*s2*Power(t2,2) + 
         111*Power(s2,2)*Power(t2,2) + 31*Power(s2,3)*Power(t2,2) + 
         3*Power(s2,4)*Power(t2,2) - 5*Power(s2,5)*Power(t2,2) - 
         46*t1*Power(t2,2) - 190*s2*t1*Power(t2,2) - 
         188*Power(s2,2)*t1*Power(t2,2) - 44*Power(s2,3)*t1*Power(t2,2) + 
         12*Power(s2,4)*t1*Power(t2,2) - 40*Power(t1,2)*Power(t2,2) + 
         81*s2*Power(t1,2)*Power(t2,2) + 
         26*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         4*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         46*Power(t1,3)*Power(t2,2) + 4*s2*Power(t1,3)*Power(t2,2) - 
         24*Power(t2,3) + 41*s2*Power(t2,3) + 
         26*Power(s2,2)*Power(t2,3) - 2*Power(s2,3)*Power(t2,3) + 
         6*Power(s2,4)*Power(t2,3) + 48*t1*Power(t2,3) + 
         7*s2*t1*Power(t2,3) + 38*Power(s2,2)*t1*Power(t2,3) - 
         10*Power(s2,3)*t1*Power(t2,3) - 32*Power(t1,2)*Power(t2,3) - 
         46*s2*Power(t1,2)*Power(t2,3) + 
         4*Power(s2,2)*Power(t1,2)*Power(t2,3) - 2*Power(t2,4) - 
         18*s2*Power(t2,4) - 11*Power(s2,2)*Power(t2,4) - 
         Power(s2,3)*Power(t2,4) + 6*t1*Power(t2,4) + 
         32*s2*t1*Power(t2,4) - 6*s2*Power(t2,5) + 
         Power(s1,4)*(6 - 3*Power(s2,3) - 9*t1 + 7*Power(t1,2) + 
            s2*(-10 + t1*(2 - 9*t2) + 21*t2) + 
            Power(s2,2)*(23*t1 - 18*(1 + t2))) + 
         Power(s,4)*(Power(s2,2)*(4 - 11*t1 + 7*t2) + 
            2*(-2*Power(t1,2) + t1*(3 - 7*t2) + t2*(6 + 5*t2)) + 
            s1*(-8 + 7*t1 - 9*t2 + s2*(10 - 11*t1 + 11*t2)) + 
            s2*(-6 + 4*Power(t1,2) - 23*t2 - 8*Power(t2,2) + 
               t1*(11 + 12*t2))) + 
         Power(s1,3)*(7 - 9*Power(s2,4) + 2*Power(t1,3) + 
            Power(t1,2)*(5 - 14*t2) - 15*t2 + 
            Power(s2,3)*(-10 + 25*t1 + 6*t2) + t1*(-6 + 13*t2) + 
            Power(s2,2)*(45 + 10*Power(t1,2) + 77*t2 + 36*Power(t2,2) - 
               3*t1*(16 + 27*t2)) + 
            s2*(56 - 2*Power(t1,3) + 58*t2 - 21*Power(t2,2) + 
               Power(t1,2)*(59 + 8*t2) + 
               t1*(-126 - 52*t2 + 9*Power(t2,2)))) + 
         Power(s1,2)*(-38 - 3*Power(s2,5) - 36*t2 + 10*Power(t2,2) + 
            6*Power(t1,3)*(8 + t2) + Power(s2,4)*(7*t1 + 26*t2) + 
            Power(t1,2)*(-129 - 56*t2 + 7*Power(t2,2)) + 
            t1*(123 + 62*t2 + 7*Power(t2,2)) + 
            Power(s2,3)*(73 + 10*Power(t1,2) + 27*t2 - 4*Power(t2,2) - 
               3*t1*(24 + 25*t2)) - 
            Power(s2,2)*(-50 + 2*Power(t1,3) + 68*t2 + 
               111*Power(t2,2) + 30*Power(t2,3) + 
               Power(t1,2)*(-77 + 12*t2) + 
               t1*(216 - 140*t2 - 93*Power(t2,2))) + 
            s2*(-61 - 88*t2 - 104*Power(t2,2) - 3*Power(t2,3) + 
               6*Power(t1,3)*(1 + t2) - 
               Power(t1,2)*(29 + 186*t2 + 16*Power(t2,2)) + 
               t1*(92 + 294*t2 + 130*Power(t2,2) - 3*Power(t2,3)))) + 
         s1*(19 - 9*t2 + 53*Power(t2,2) + Power(t2,3) + 
            4*Power(t1,4)*(1 + t2) + 2*Power(s2,5)*(-2 + t1 + 4*t2) - 
            2*Power(t1,3)*(-13 + 50*t2 + 4*Power(t2,2)) + 
            Power(t1,2)*(-25 + 164*t2 + 83*Power(t2,2)) - 
            t1*(24 + 67*t2 + 104*Power(t2,2) + 17*Power(t2,3)) - 
            Power(s2,4)*(-38 + 3*t2 + 23*Power(t2,2) + 
               t1*(32 + 19*t2)) + 
            Power(s2,3)*(-50 + Power(t1,2)*(49 - 10*t2) - 110*t2 - 
               15*Power(t2,2) + 2*Power(t2,3) + 
               3*t1*(-7 + 42*t2 + 20*Power(t2,2))) + 
            Power(s2,2)*(-131 - 160*t2 - 3*Power(t2,2) + 
               63*Power(t2,3) + 9*Power(t2,4) + 
               Power(t1,3)*(-2 + 4*t2) - 
               Power(t1,2)*(243 + 112*t2 + 2*Power(t2,2)) - 
               5*t1*(-82 - 82*t2 + 26*Power(t2,2) + 7*Power(t2,3))) - 
            s2*(46 + 4*Power(t1,4) + 30*t2 + 9*Power(t2,2) - 
               74*Power(t2,3) - 15*Power(t2,4) + 
               2*Power(t1,3)*(-73 + 7*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(423 + 34*t2 - 173*Power(t2,2) - 
                  8*Power(t2,3)) + 
               t1*(-311 - 80*t2 + 175*Power(t2,2) + 112*Power(t2,3)))) + 
         Power(s,3)*(Power(s2,3)*(2 + 8*t1 - 9*t2) + 
            Power(s1,2)*(8 + 5*Power(s2,2) + t1 + 
               s2*(-21 + 6*t1 - 13*t2) + 4*t2) + 
            Power(s2,2)*(-25 - 8*Power(t1,2) + 46*t2 + 27*Power(t2,2) - 
               3*t1*(-5 + 9*t2)) - 
            2*(7 + 2*Power(t1,3) + t2 + Power(t2,2) - 4*Power(t2,3) + 
               Power(t1,2)*(-4 + 7*t2) + t1*(-2 - 24*t2 + Power(t2,2))) \
+ s2*(39 + 2*Power(t1,3) - 48*t2 - 56*Power(t2,2) - 8*Power(t2,3) + 
               Power(t1,2)*(5 + 12*t2) + 
               t1*(-32 + 11*t2 + 6*Power(t2,2))) + 
            s1*(17 - Power(s2,3) + Power(t1,2) + t1*(-44 + t2) - 6*t2 - 
               12*Power(t2,2) + 
               s2*(5 - 12*Power(t1,2) - 5*t1*(-2 + t2) + 78*t2 + 
                  21*Power(t2,2)) + Power(s2,2)*(22*t1 - 5*(3 + 8*t2)))) \
+ Power(s,2)*(Power(s2,4)*(-8 + t1 + 5*t2) + 
            Power(s1,3)*(4 - 7*Power(s2,2) - 7*t1 + 3*t2 + 
               s2*(6 + 8*t1 + t2)) + 
            Power(s2,3)*(64 + 4*Power(t1,2) - 23*t2 - 35*Power(t2,2) + 
               7*t1*(-7 + 2*t2)) + 
            Power(s2,2)*(-112 - 2*Power(t1,3) + 
               Power(t1,2)*(26 - 20*t2) - 13*t2 + 89*Power(t2,2) + 
               21*Power(t2,3) + t1*(45 + 68*t2 + Power(t2,2))) + 
            s2*(57 + 38*t2 - 44*Power(t2,2) - 15*Power(t2,3) - 
               2*Power(t2,4) + 2*Power(t1,3)*(7 + 3*t2) + 
               Power(t1,2)*(-141 - 14*t2 + 6*Power(t2,2)) - 
               t1*(-127 + 19*t2 + 53*Power(t2,2) + 2*Power(t2,3))) + 
            2*(-3 + Power(t1,3)*(4 - 9*t2) + 4*t2 + 5*Power(t2,2) - 
               14*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,2)*(31 + 20*t2 + 7*Power(t2,2)) - 
               t1*(45 + 34*t2 - 27*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,2)*(-11*Power(s2,3) + 17*Power(t1,2) + 
               t1*(37 + 8*t2) + Power(s2,2)*(22 + 12*t1 + 45*t2) - 
               2*(24 + 17*t2 + Power(t2,2)) + 
               s2*(12*Power(t1,2) - t1*(67 + 35*t2) - 
                  4*(-15 + 3*t2 + Power(t2,2)))) + 
            s1*(49 + 2*Power(s2,4) + 10*Power(t1,3) + 32*t2 + 
               58*Power(t2,2) - 5*Power(t2,3) - 
               15*Power(t1,2)*(5 + 2*t2) + 
               Power(s2,3)*(-10 - 7*t1 + 53*t2) + 
               t1*(83 - 89*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*(108 + 26*Power(t1,2) - 114*t2 - 
                  59*Power(t2,2) - t1*(109 + 27*t2)) + 
               s2*(-170 - 6*Power(t1,3) + Power(t1,2)*(45 - 16*t2) - 
                  23*t2 + 21*Power(t2,2) + 5*Power(t2,3) + 
                  t1*(50 + 138*t2 + 29*Power(t2,2))))) - 
         s*(Power(s1,4)*(6 + Power(s2,2) - 3*t1 + 
               s2*(-13 + 10*t1 - 3*t2)) + Power(s2,5)*(-4 + 2*t1 + t2) + 
            Power(s2,4)*(35 + 6*t2 - 21*Power(t2,2) - 3*t1*(9 + t2)) + 
            Power(s2,3)*(-63 + Power(t1,2)*(33 - 8*t2) - 85*t2 + 
               44*Power(t2,2) + 19*Power(t2,3) + 
               t1*(2 + 97*t2 + 20*Power(t2,2))) + 
            Power(s2,2)*(-89 + 4*Power(t1,3)*(-1 + t2) + 4*t2 + 
               2*Power(t2,2) - 4*Power(t2,3) - 3*Power(t2,4) + 
               2*Power(t1,2)*(-120 - 26*t2 + Power(t2,2)) + 
               t1*(389 + 101*t2 - 139*Power(t2,2) - 11*Power(t2,3))) + 
            2*(-6 + 46*t2 + 29*Power(t2,2) - 22*Power(t2,3) + 
               Power(t2,4) + 2*Power(t1,4)*(3 + t2) + 
               Power(t1,2)*(170 + 104*t2 - 15*Power(t2,2)) - 
               Power(t1,3)*(88 + 26*t2 + Power(t2,2)) + 
               t1*(-74 - 133*t2 + 5*Power(t2,2) + 10*Power(t2,3))) - 
            s2*(-123 + 4*Power(t1,4) + 47*Power(t2,2) - 26*Power(t2,3) - 
               6*Power(t2,4) + 
               2*Power(t1,3)*(-69 + 13*t2 + 2*Power(t2,2)) - 
               Power(t1,2)*(-83 + 70*t2 + 113*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(224 + 95*t2 + 42*Power(t2,2) + 35*Power(t2,3))) + 
            Power(s1,3)*(11 - 15*Power(s2,3) + 21*Power(t1,2) + 
               Power(s2,2)*(-19 + 46*t1 - 6*t2) - 12*t2 - 
               t1*(2 + 5*t2) + 
               s2*(57 + 4*Power(t1,2) + 56*t2 + 9*Power(t2,2) - 
                  t1*(44 + 37*t2))) + 
            Power(s1,2)*(-82 - 9*Power(s2,4) + 8*Power(t1,3) - 66*t2 + 
               8*Power(t2,2) - 2*Power(t1,2)*(19 + 29*t2) + 
               Power(s2,3)*(1 + 26*t1 + 56*t2) + 
               t1*(151 + 36*t2 + 19*Power(t2,2)) + 
               Power(s2,2)*(134 + 28*Power(t1,2) + 52*t2 + 
                  6*Power(t2,2) - 3*t1*(56 + 45*t2)) - 
               s2*(58 + 6*Power(t1,3) + 103*t2 + 67*Power(t2,2) + 
                  9*Power(t2,3) - Power(t1,2)*(109 + 4*t2) + 
                  t1*(132 - 69*t2 - 44*Power(t2,2)))) + 
            s1*(77 + Power(s2,5) + Power(t1,3)*(80 - 12*t2) + 16*t2 + 
               99*Power(t2,2) - 4*Power(t2,3) + 
               Power(s2,4)*(-19 + 6*t1 + 32*t2) + 
               Power(t1,2)*(-135 + 72*t2 + 37*Power(t2,2)) - 
               t1*(26 + 149*t2 + 54*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,3)*(136 + 14*Power(t1,2) - 46*t2 - 
                  60*Power(t2,2) - t1*(118 + 53*t2)) - 
               Power(s2,2)*(88 + 4*Power(t1,3) + 146*t2 + 
                  29*Power(t2,2) - 2*Power(t2,3) + 
                  Power(t1,2)*(-87 + 32*t2) + 
                  t1*(120 - 331*t2 - 100*Power(t2,2))) + 
               s2*(-111 + 106*t2 + 20*Power(t2,2) + 18*Power(t2,3) + 
                  3*Power(t2,4) + 4*Power(t1,3)*(5 + 3*t2) - 
                  2*Power(t1,2)*(113 + 116*t2 + 5*Power(t2,2)) + 
                  t1*(426 + 173*t2 + 10*Power(t2,2) - 17*Power(t2,3))))))*
       R2q(1 - s1 - t1 + t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s1 + t1 - t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) - 
    (8*(-39 - 14*s2 - 36*Power(s2,2) - 40*Power(s2,3) + 5*Power(s2,4) + 
         10*Power(s2,5) - 2*Power(s2,6) + 235*t1 + 173*s2*t1 + 
         157*Power(s2,2)*t1 + 145*Power(s2,3)*t1 - 54*Power(s2,4)*t1 - 
         16*Power(s2,5)*t1 + 4*Power(s2,6)*t1 - 503*Power(t1,2) - 
         371*s2*Power(t1,2) - 355*Power(s2,2)*Power(t1,2) - 
         127*Power(s2,3)*Power(t1,2) + 92*Power(s2,4)*Power(t1,2) + 
         2*Power(s2,5)*Power(t1,2) - 2*Power(s2,6)*Power(t1,2) + 
         421*Power(t1,3) + 325*s2*Power(t1,3) + 
         377*Power(s2,2)*Power(t1,3) - 21*Power(s2,3)*Power(t1,3) - 
         42*Power(s2,4)*Power(t1,3) + 4*Power(s2,5)*Power(t1,3) - 
         54*Power(t1,4) - 159*s2*Power(t1,4) - 
         137*Power(s2,2)*Power(t1,4) + 43*Power(s2,3)*Power(t1,4) - 
         Power(s2,4)*Power(t1,4) - 84*Power(t1,5) + 46*s2*Power(t1,5) - 
         6*Power(s2,2)*Power(t1,5) + 24*Power(t1,6) + 
         Power(s1,7)*s2*(3*Power(s2,2) + s2*(-2 + t1) + 2*t1) + 
         2*Power(s,7)*(-1 + s2)*(t1 - t2) - 20*t2 - 127*s2*t2 - 
         101*Power(s2,2)*t2 - 147*Power(s2,3)*t2 - 79*Power(s2,4)*t2 + 
         18*Power(s2,5)*t2 + 20*Power(s2,6)*t2 - 4*Power(s2,7)*t2 + 
         23*t1*t2 + 511*s2*t1*t2 + 616*Power(s2,2)*t1*t2 + 
         453*Power(s2,3)*t1*t2 + 175*Power(s2,4)*t1*t2 - 
         108*Power(s2,5)*t1*t2 - 12*Power(s2,6)*t1*t2 + 
         4*Power(s2,7)*t1*t2 + 246*Power(t1,2)*t2 - 
         594*s2*Power(t1,2)*t2 - 899*Power(s2,2)*Power(t1,2)*t2 - 
         618*Power(s2,3)*Power(t1,2)*t2 + Power(s2,4)*Power(t1,2)*t2 + 
         88*Power(s2,5)*Power(t1,2)*t2 - 8*Power(s2,6)*Power(t1,2)*t2 - 
         613*Power(t1,3)*t2 + 61*s2*Power(t1,3)*t2 + 
         514*Power(s2,2)*Power(t1,3)*t2 + 
         297*Power(s2,3)*Power(t1,3)*t2 - 97*Power(s2,4)*Power(t1,3)*t2 + 
         2*Power(s2,5)*Power(t1,3)*t2 + 482*Power(t1,4)*t2 + 
         209*s2*Power(t1,4)*t2 - 130*Power(s2,2)*Power(t1,4)*t2 + 
         15*Power(s2,3)*Power(t1,4)*t2 - 118*Power(t1,5)*t2 - 
         60*s2*Power(t1,5)*t2 + 41*Power(t2,2) - 32*s2*Power(t2,2) - 
         179*Power(s2,2)*Power(t2,2) - 176*Power(s2,3)*Power(t2,2) - 
         172*Power(s2,4)*Power(t2,2) - 30*Power(s2,5)*Power(t2,2) + 
         16*Power(s2,6)*Power(t2,2) + 10*Power(s2,7)*Power(t2,2) - 
         2*Power(s2,8)*Power(t2,2) - 337*t1*Power(t2,2) - 
         280*s2*t1*Power(t2,2) + 389*Power(s2,2)*t1*Power(t2,2) + 
         645*Power(s2,3)*t1*Power(t2,2) + 
         302*Power(s2,4)*t1*Power(t2,2) + 18*Power(s2,5)*t1*Power(t2,2) - 
         51*Power(s2,6)*t1*Power(t2,2) + 4*Power(s2,7)*t1*Power(t2,2) + 
         757*Power(t1,2)*Power(t2,2) + 1053*s2*Power(t1,2)*Power(t2,2) - 
         41*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         556*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         190*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         66*Power(s2,5)*Power(t1,2)*Power(t2,2) - 
         Power(s2,6)*Power(t1,2)*Power(t2,2) - 
         619*Power(t1,3)*Power(t2,2) - 1060*s2*Power(t1,3)*Power(t2,2) - 
         202*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         137*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         12*Power(s2,4)*Power(t1,3)*Power(t2,2) + 
         150*Power(t1,4)*Power(t2,2) + 297*s2*Power(t1,4)*Power(t2,2) + 
         45*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         10*Power(t1,5)*Power(t2,2) + 44*Power(t2,3) + 
         187*s2*Power(t2,3) + 37*Power(s2,2)*Power(t2,3) - 
         120*Power(s2,3)*Power(t2,3) - 82*Power(s2,4)*Power(t2,3) - 
         63*Power(s2,5)*Power(t2,3) + 2*Power(s2,6)*Power(t2,3) + 
         5*Power(s2,7)*Power(t2,3) - 176*t1*Power(t2,3) - 
         836*s2*t1*Power(t2,3) - 603*Power(s2,2)*t1*Power(t2,3) + 
         58*Power(s2,3)*t1*Power(t2,3) + 213*Power(s2,4)*t1*Power(t2,3) + 
         33*Power(s2,5)*t1*Power(t2,3) - 12*Power(s2,6)*t1*Power(t2,3) + 
         147*Power(t1,2)*Power(t2,3) + 1074*s2*Power(t1,2)*Power(t2,3) + 
         839*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         85*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         65*Power(s2,4)*Power(t1,2)*Power(t2,3) + 
         3*Power(s2,5)*Power(t1,2)*Power(t2,3) + 
         11*Power(t1,3)*Power(t2,3) - 353*s2*Power(t1,3)*Power(t2,3) - 
         241*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         6*Power(s2,3)*Power(t1,3)*Power(t2,3) - 
         34*Power(t1,4)*Power(t2,3) - 19*s2*Power(t1,4)*Power(t2,3) + 
         15*Power(t2,4) + 106*s2*Power(t2,4) + 
         228*Power(s2,2)*Power(t2,4) + 90*Power(s2,3)*Power(t2,4) - 
         18*Power(s2,4)*Power(t2,4) - 12*Power(s2,5)*Power(t2,4) - 
         3*Power(s2,6)*Power(t2,4) + 19*t1*Power(t2,4) - 
         243*s2*t1*Power(t2,4) - 548*Power(s2,2)*t1*Power(t2,4) - 
         280*Power(s2,3)*t1*Power(t2,4) - 7*Power(s2,4)*t1*Power(t2,4) + 
         12*Power(s2,5)*t1*Power(t2,4) - 54*Power(t1,2)*Power(t2,4) + 
         32*s2*Power(t1,2)*Power(t2,4) + 
         235*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         66*Power(s2,3)*Power(t1,2)*Power(t2,4) - 
         3*Power(s2,4)*Power(t1,2)*Power(t2,4) + 
         32*Power(t1,3)*Power(t2,4) + 69*s2*Power(t1,3)*Power(t2,4) + 
         8*Power(s2,2)*Power(t1,3)*Power(t2,4) - 8*Power(t2,5) - 
         s2*Power(t2,5) + 73*Power(s2,2)*Power(t2,5) + 
         99*Power(s2,3)*Power(t2,5) + 19*Power(s2,4)*Power(t2,5) - 
         Power(s2,5)*Power(t2,5) + 9*t1*Power(t2,5) + 
         61*s2*t1*Power(t2,5) - 37*Power(s2,2)*t1*Power(t2,5) - 
         33*Power(s2,3)*t1*Power(t2,5) - 4*Power(s2,4)*t1*Power(t2,5) - 
         9*Power(t1,2)*Power(t2,5) - 62*s2*Power(t1,2)*Power(t2,5) - 
         35*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
         Power(s2,3)*Power(t1,2)*Power(t2,5) - Power(t2,6) - 
         4*s2*Power(t2,6) - 9*Power(s2,2)*Power(t2,6) - 
         6*Power(s2,3)*Power(t2,6) + Power(s2,4)*Power(t2,6) + 
         3*t1*Power(t2,6) + 14*s2*t1*Power(t2,6) + 
         30*Power(s2,2)*t1*Power(t2,6) - 3*s2*Power(t2,7) - 
         5*Power(s2,2)*Power(t2,7) + 
         Power(s,6)*(-2 - t1 - 2*Power(t1,2) - 5*t2 - 3*t1*t2 + 
            5*Power(t2,2) + Power(s2,2)*(t1*(-12 + t2) - (-8 + t2)*t2) + 
            s2*(2 + 4*Power(t1,2) + t1*(15 - 2*t2) - 5*t2 - 
               2*Power(t2,2)) + 
            s1*(4 + 10*t1 - 6*t2 + Power(s2,2)*(2 + t1 + t2) + 
               s2*(-6 - 13*t1 + 7*t2))) + 
         Power(s1,6)*(4*Power(s2,4) + 2*t1*(-1 + 2*t1) + 
            Power(s2,3)*(-7 + 14*t1 - 22*t2) + 
            Power(s2,2)*(-12 + t1*(10 - 3*t2) + 8*t2) + 
            s2*(4 + 8*Power(t1,2) - 2*t2 - t1*(3 + 13*t2))) + 
         Power(s1,5)*(-4*Power(s2,5) + 6*Power(t1,3) + 
            Power(s2,4)*(-25 + 32*t1 - 29*t2) + 
            Power(t1,2)*(2 - 19*t2) + 2*(-1 + t2) + t1*(2 + 5*t2) + 
            Power(s2,3)*(-22 + 4*Power(t1,2) + t1*(9 - 71*t2) + 42*t2 + 
               65*Power(t2,2)) + 
            Power(s2,2)*(11 + 83*t2 - 5*Power(t2,2) + 
               Power(t1,2)*(58 + 4*t2) - 6*t1*(14 + 15*t2)) + 
            s2*(15 + 2*Power(t1,3) + Power(t1,2)*(39 - 32*t2) - 15*t2 + 
               13*Power(t2,2) + t1*(-51 - 8*t2 + 35*Power(t2,2)))) + 
         Power(s,5)*(1 - 4*t1 - 16*Power(t1,2) + 15*t2 - 2*t1*t2 + 
            Power(t1,2)*t2 - 10*Power(t2,2) - 7*t1*Power(t2,2) + 
            6*Power(t2,3) + Power(s1,2)*
             (Power(s2,3) + s2*(26 + 33*t1 - 14*t2) - 
               2*(6 + 9*t1 - 4*t2) - Power(s2,2)*(13 + 6*t1 + t2)) - 
            Power(s2,3)*(2 + Power(t1,2) + 7*t2 - 6*Power(t2,2) + 
               t1*(-31 + 4*t2)) + 
            Power(s2,2)*(-4 - 10*t2 + 7*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,2)*(-21 + 2*t2) + t1*(-46 - 8*t2 + Power(t2,2))) \
+ s1*(6 - 6*t1 + 12*Power(t1,2) + 19*t2 + 21*t1*t2 - 14*Power(t2,2) - 
               Power(s2,3)*(10 + 7*t1 + 7*t2) - 
               s2*(34 + 65*t1 + 27*Power(t1,2) - 5*t2 + 10*t1*t2 - 
                  7*Power(t2,2)) + 
               Power(s2,2)*(34 + 72*t1 - 15*t2 + 7*t1*t2 + 
                  4*Power(t2,2))) + 
            s2*(Power(t1,2)*(39 + 7*t2) + 
               t1*(16 + 24*t2 - 14*Power(t2,2)) + 
               7*(1 + t2 - 2*Power(t2,2) + Power(t2,3)))) + 
         Power(s1,4)*(-6 - 10*Power(s2,6) + 2*Power(t1,4) + 7*t2 - 
            9*Power(t2,2) - 5*Power(t1,3)*(-7 + 4*t2) + 
            Power(s2,5)*(-15 + 30*t1 + 11*t2) + 
            t1*(41 - 9*t2 + 3*Power(t2,2)) + 
            6*Power(t1,2)*(-10 - 3*t2 + 6*Power(t2,2)) + 
            Power(s2,4)*(69 + 8*Power(t1,2) + 136*t2 + 77*Power(t2,2) - 
               t1*(69 + 145*t2)) + 
            Power(s2,3)*(89 + Power(t1,2)*(140 - 9*t2) + 226*t2 - 
               104*Power(t2,2) - 100*Power(t2,3) + 
               t1*(-293 - 110*t2 + 144*Power(t2,2))) + 
            s2*(-1 + 87*Power(t1,3) + Power(t1,4) - 69*t2 + 
               16*Power(t2,2) - 35*Power(t2,3) + 
               Power(t1,2)*(-160 - 223*t2 + 48*Power(t2,2)) + 
               t1*(42 + 270*t2 + 76*Power(t2,2) - 50*Power(t2,3))) + 
            Power(s2,2)*(41 + 31*t2 - 221*Power(t2,2) - 25*Power(t2,3) + 
               Power(t1,3)*(13 + t2) - 
               16*Power(t1,2)*(-2 + 16*t2 + Power(t2,2)) + 
               t1*(-144 + 281*t2 + 290*Power(t2,2) + 10*Power(t2,3)))) + 
         Power(s1,3)*(-3 - 7*Power(s2,7) - 2*Power(t1,4)*(-20 + t2) + 
            8*t2 - Power(t2,2) + 16*Power(t2,3) + 
            Power(s2,6)*(11 + 15*t1 + 33*t2) + 
            Power(t1,3)*(-68 - 131*t2 + 24*Power(t2,2)) + 
            Power(t1,2)*(11 + 215*t2 + 51*Power(t2,2) - 
               34*Power(t2,3)) + 
            t1*(28 - 138*t2 + 6*Power(t2,2) - 22*Power(t2,3)) + 
            Power(s2,5)*(94 + 4*Power(t1,2) + 76*t2 - 8*Power(t2,2) - 
               113*t1*(1 + t2)) + 
            Power(s2,4)*(26 + Power(t1,2)*(144 - 23*t2) - 193*t2 - 
               277*Power(t2,2) - 98*Power(t2,3) + 
               t1*(-156 + 196*t2 + 247*Power(t2,2))) + 
            Power(s2,3)*(-133 - 414*t2 - 645*Power(t2,2) + 
               136*Power(t2,3) + 85*Power(t2,4) + 
               Power(t1,3)*(11 + 3*t2) + 
               Power(t1,2)*(-274 - 507*t2 + 2*Power(t2,2)) + 
               t1*(293 + 1258*t2 + 309*Power(t2,2) - 146*Power(t2,3))) + 
            Power(s2,2)*(-62 + 3*Power(t1,4) - 401*t2 - 
               232*Power(t2,2) + 294*Power(t2,3) + 60*Power(t2,4) + 
               Power(t1,3)*(250 - 40*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*(-698 - 417*t2 + 455*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(507 + 1101*t2 - 302*Power(t2,2) - 460*Power(t2,3) - 
                  15*Power(t2,4))) + 
            s2*(Power(t1,4)*(26 + t2) - 
               6*Power(t1,3)*(-10 + 51*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-358 + 384*t2 + 497*Power(t2,2) - 
                  32*Power(t2,3)) + 
               2*(-16 - 52*t2 + 59*Power(t2,2) + 3*Power(t2,3) + 
                  25*Power(t2,4)) + 
               t1*(253 + 154*t2 - 565*Power(t2,2) - 164*Power(t2,3) + 
                  40*Power(t2,4)))) + 
         Power(s1,2)*(9 - 2*Power(s2,8) + 31*t2 + 17*Power(t2,2) - 
            19*Power(t2,3) - 14*Power(t2,4) + 2*Power(t1,5)*(6 + t2) + 
            Power(s2,7)*(10 + 4*t1 + 19*t2) - 
            2*Power(t1,4)*(-18 + 52*t2 + Power(t2,2)) + 
            Power(t1,3)*(-236 + 133*t2 + 189*Power(t2,2) - 
               12*Power(t2,3)) + 
            2*t1*(-59 - 91*t2 + 86*Power(t2,2) + 8*Power(t2,3) + 
               14*Power(t2,4)) + 
            Power(t1,2)*(299 + 96*t2 - 304*Power(t2,2) - 
               65*Power(t2,3) + 16*Power(t2,4)) - 
            Power(s2,6)*(-19 + 18*t2 + 39*Power(t2,2) + 
               11*t1*(5 + 4*t2)) + 
            Power(s2,5)*(-62 + Power(t1,2)*(62 - 11*t2) - 267*t2 - 
               119*Power(t2,2) - 2*Power(t2,3) + 
               t1*(54 + 281*t2 + 148*Power(t2,2))) + 
            Power(s2,4)*(-194 - 157*t2 + 161*Power(t2,2) + 
               265*Power(t2,3) + 62*Power(t2,4) + 
               3*Power(t1,3)*(3 + t2) + 
               Power(t1,2)*(-324 - 386*t2 + 19*Power(t2,2)) + 
               t1*(437 + 578*t2 - 192*Power(t2,2) - 191*Power(t2,3))) + 
            Power(s2,3)*(-21 - 3*Power(t1,4) + 185*t2 + 
               651*Power(t2,2) + 799*Power(t2,3) - 99*Power(t2,4) - 
               38*Power(t2,5) + 
               Power(t1,3)*(227 - 32*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*(-452 + 652*t2 + 660*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(299 - 582*t2 - 1917*Power(t2,2) - 357*Power(t2,3) + 
                  74*Power(t2,4))) + 
            Power(s2,2)*(36 + 19*Power(t1,4) + 213*t2 + 
               907*Power(t2,2) + 400*Power(t2,3) - 206*Power(t2,4) - 
               58*Power(t2,5) + 
               Power(t1,3)*(-338 - 774*t2 + 49*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(398 + 2373*t2 + 973*Power(t2,2) - 
                  409*Power(t2,3) - 16*Power(t2,4)) + 
               t1*(-103 - 1780*t2 - 2318*Power(t2,2) + 60*Power(t2,3) + 
                  390*Power(t2,4) + 9*Power(t2,5))) + 
            s2*(-43 + 6*Power(t1,5) + 248*t2 + 317*Power(t2,2) - 
               90*Power(t2,3) - 24*Power(t2,4) - 40*Power(t2,5) + 
               Power(t1,4)*(220 - 63*t2 - 5*Power(t2,2)) + 
               Power(t1,3)*(-698 - 541*t2 + 420*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(569 + 1885*t2 - 256*Power(t2,2) - 
                  543*Power(t2,3) + 8*Power(t2,4)) - 
               t1*(76 + 1374*t2 + 677*Power(t2,2) - 585*Power(t2,3) - 
                  161*Power(t2,4) + 17*Power(t2,5)))) + 
         s1*(41 - 44*t2 + 4*Power(s2,8)*t2 - 72*Power(t2,2) - 
            34*Power(t2,3) + 23*Power(t2,4) + 6*Power(t2,5) - 
            2*Power(t1,5)*(-36 + 10*t2 + Power(t2,2)) + 
            2*Power(t1,4)*(-116 - 94*t2 + 49*Power(t2,2) + 
               Power(t2,3)) + 
            Power(t1,3)*(146 + 839*t2 - 76*Power(t2,2) - 
               125*Power(t2,3) + 2*Power(t2,4)) + 
            Power(t1,2)*(143 - 1020*t2 - 254*Power(t2,2) + 
               203*Power(t2,3) + 39*Power(t2,4) - 3*Power(t2,5)) - 
            t1*(170 - 429*t2 - 330*Power(t2,2) + 94*Power(t2,3) + 
               24*Power(t2,4) + 15*Power(t2,5)) - 
            Power(s2,7)*(-4 + 20*t2 + 17*Power(t2,2) + t1*(4 + 8*t2)) + 
            Power(s2,6)*(-20 - Power(t1,2)*(-8 + t2) - 37*t2 + 
               5*Power(t2,2) + 19*Power(t2,3) + 
               t1*(12 + 110*t2 + 41*Power(t2,2))) + 
            Power(s2,5)*(-17 + 97*t2 + 236*Power(t2,2) + 
               70*Power(t2,3) + 4*Power(t2,4) + Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(-93 - 125*t2 + 4*Power(t2,2)) - 
               t1*(-109 + 81*t2 + 201*Power(t2,2) + 77*Power(t2,3))) - 
            Power(s2,4)*(-91 + Power(t1,4) - 381*t2 - 213*Power(t2,2) + 
               19*Power(t2,3) + 118*Power(t2,4) + 17*Power(t2,5) + 
               3*Power(t1,3)*(-28 + 4*t2 + Power(t2,2)) + 
               Power(t1,2)*(-40 - 559*t2 - 307*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(214 + 784*t2 + 635*Power(t2,2) - 72*Power(t2,3) - 
                  61*Power(t2,4))) + 
            Power(s2,3)*(146 + 182*t2 + 68*Power(t2,2) - 
               416*Power(t2,3) - 457*Power(t2,4) + 38*Power(t2,5) + 
               7*Power(t2,6) + Power(t1,4)*(8 + 9*t2) + 
               Power(t1,3)*(-373 - 379*t2 + 27*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(700 + 1005*t2 - 463*Power(t2,2) - 
                  359*Power(t2,3) - 6*Power(t2,4)) + 
               t1*(-481 - 917*t2 + 231*Power(t2,2) + 1232*Power(t2,3) + 
                  182*Power(t2,4) - 15*Power(t2,5))) - 
            Power(s2,2)*(-24 + 6*Power(t1,5) - 124*t2 + 
               188*Power(t2,2) + 775*Power(t2,3) + 283*Power(t2,4) - 
               71*Power(t2,5) - 27*Power(t2,6) + 
               Power(t1,4)*(-149 + 78*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(418 - 607*t2 - 765*Power(t2,2) + 
                  30*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(-597 + 468*t2 + 2514*Power(t2,2) + 
                  823*Power(t2,3) - 187*Power(t2,4) - 4*Power(t2,5)) + 
               t1*(346 + 209*t2 - 1876*Power(t2,2) - 1909*Power(t2,3) - 
                  82*Power(t2,4) + 170*Power(t2,5) + 2*Power(t2,6))) + 
            s2*(71 + 52*Power(t1,5) + 85*t2 - 403*Power(t2,2) - 
               318*Power(t2,3) + 27*Power(t2,4) + 17*Power(t2,5) + 
               17*Power(t2,6) + 
               Power(t1,4)*(-232 - 539*t2 + 56*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,3)*(52 + 1784*t2 + 834*Power(t2,2) - 
                  270*Power(t2,3) - 6*Power(t2,4)) + 
               Power(t1,2)*(413 - 1622*t2 - 2601*Power(t2,2) + 
                  292*Power(t2,4)) + 
               t1*(-356 + 336*t2 + 1957*Power(t2,2) + 724*Power(t2,3) - 
                  300*Power(t2,4) - 76*Power(t2,5) + 3*Power(t2,6)))) + 
         Power(s,4)*(29 - 65*t1 - 13*Power(t1,3) + 2*Power(t1,4) + 
            30*t2 + 36*t1*t2 - 67*Power(t1,2)*t2 + 6*Power(t1,3)*t2 + 
            49*Power(t2,2) + 42*t1*Power(t2,2) - 
            6*Power(t1,2)*Power(t2,2) - 12*Power(t2,3) - 
            4*t1*Power(t2,3) + 2*Power(t2,4) + 
            Power(s2,4)*(6 - 41*t1 + 3*Power(t1,2) - 14*t2 + 7*t1*t2 - 
               16*Power(t2,2)) - 
            Power(s1,3)*(Power(s2,3) + 2*s2*(16 + 20*t1 - 9*t2) + 
               4*(-2 - 3*t1 + t2) + Power(s2,2)*(-22 - 15*t1 + 6*t2)) + 
            Power(s2,3)*(-17 + Power(t1,2)*(31 - 13*t2) + 31*t2 + 
               Power(t2,2) + 14*Power(t2,3) + 
               t1*(93 + 85*t2 + 4*Power(t2,2))) + 
            s2*(-79 + Power(t1,4) + Power(t1,3)*(29 - 15*t2) - 114*t2 - 
               75*Power(t2,2) - 58*Power(t2,3) + 20*Power(t2,4) + 
               Power(t1,2)*(55 + 68*t2 + 27*Power(t2,2)) + 
               t1*(131 + 65*t2 + 56*Power(t2,2) - 33*Power(t2,3))) + 
            Power(s2,2)*(58 + 40*t2 + Power(t1,3)*t2 + 4*Power(t2,2) + 
               2*Power(t2,3) - 3*Power(t2,4) + 
               Power(t1,2)*(-125 - 44*t2 + 5*Power(t2,2)) - 
               t1*(98 + 110*t2 + 8*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s1,2)*(28 - 5*Power(s2,4) - 24*Power(t1,2) + 
               t1*(28 - 22*t2) - 25*t2 + 10*Power(t2,2) + 
               Power(s2,3)*(37 + 42*t1 + 12*t2) + 
               s2*(5 + 76*Power(t1,2) - 8*t2 - 16*Power(t2,2) + 
                  t1*(107 + 18*t2)) + 
               Power(s2,2)*(-62 + 28*t2 + 9*Power(t2,2) - 
                  t1*(181 + 41*t2))) + 
            s1*(-45 + 6*Power(t1,3) - 73*t2 + 29*Power(t2,2) - 
               8*Power(t2,3) + 3*Power(s2,4)*(7 + 6*t1 + 7*t2) + 
               Power(t1,2)*(42 + 23*t2) + 
               2*t1*(27 - 41*t2 + 7*Power(t2,2)) + 
               Power(s2,3)*(-39 + 8*Power(t1,2) - 7*t2 - 
                  25*Power(t2,2) - 2*t1*(103 + 25*t2)) + 
               Power(s2,2)*(-24 + Power(t1,2)*(117 - 4*t2) + 23*t2 - 
                  52*Power(t2,2) + t1*(254 + 154*t2 + 29*Power(t2,2))) + 
               s2*(90 + 2*Power(t1,3) + 48*t2 + 98*Power(t2,2) - 
                  22*Power(t2,3) - Power(t1,2)*(167 + 95*t2) + 
                  t1*(-123 - 92*t2 + 55*Power(t2,2))))) - 
         Power(s,3)*(66 - 220*t1 + 193*Power(t1,2) + 12*Power(t1,3) - 
            4*Power(t1,4) + 196*t2 - 197*t1*t2 - 36*Power(t1,2)*t2 + 
            79*Power(t1,3)*t2 - 14*Power(t1,4)*t2 + 178*Power(t2,2) - 
            184*t1*Power(t2,2) + 26*Power(t1,2)*Power(t2,2) + 
            14*Power(t1,3)*Power(t2,2) + 18*Power(t2,3) - 
            69*t1*Power(t2,3) - 4*Power(t1,2)*Power(t2,3) + 
            8*Power(t2,4) + 2*t1*Power(t2,4) + 2*Power(t2,5) + 
            2*Power(s1,4)*(-2 + 3*Power(s2,3) - t1 + 
               Power(s2,2)*(3 + 10*t1 - 7*t2) + t2 + 
               s2*(-2 - 10*t1 + 5*t2)) + 
            Power(s2,5)*(6 + 3*Power(t1,2) - 34*t2 - 24*Power(t2,2) + 
               t1*(-27 + 7*t2)) + 
            Power(s2,4)*(-41 + Power(t1,2)*(14 - 24*t2) + 29*t2 + 
               41*Power(t2,2) + 29*Power(t2,3) + 
               t1*(102 + 175*t2 + 19*Power(t2,2))) + 
            Power(s2,3)*(117 + 136*t2 + 68*Power(t2,2) + 
               46*Power(t2,3) - 9*Power(t2,4) + 
               Power(t1,3)*(14 + 3*t2) + 
               Power(t1,2)*(-210 - 90*t2 + 23*Power(t2,2)) - 
               t1*(85 + 298*t2 + 198*Power(t2,2) + 27*Power(t2,3))) + 
            Power(s2,2)*(-137 + 3*Power(t1,4) - 310*t2 - 
               247*Power(t2,2) - 27*Power(t2,3) - 4*Power(t2,4) + 
               Power(t2,5) + Power(t1,3)*(45 - 52*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*(228 + 433*t2 + 155*Power(t2,2) - 
                  3*Power(t2,3)) + 
               t1*(83 + 143*t2 - 87*Power(t2,2) - 42*Power(t2,3) + 
                  5*Power(t2,4))) + 
            s2*(-27 - 94*t2 - 76*Power(t2,2) - 46*Power(t2,3) + 
               83*Power(t2,4) - 24*Power(t2,5) + Power(t1,4)*(6 + t2) + 
               Power(t1,3)*(-80 - 57*t2 + 29*Power(t2,2)) - 
               Power(t1,2)*(241 + 279*t2 + 127*Power(t2,2) + 
                  55*Power(t2,3)) + 
               t1*(180 + 283*t2 + 415*Power(t2,2) + 5*Power(t2,3) + 
                  49*Power(t2,4))) + 
            Power(s1,3)*(32 - 11*Power(s2,4) - 16*Power(t1,2) + 
               Power(s2,3)*(24 + 98*t1 - 28*t2) + 7*t2 - 
               8*Power(t2,2) + 4*t1*(10 + 3*t2) + 
               s2*(-67 + 114*Power(t1,2) + t1*(64 - 39*t2) - 46*t2 - 
                  6*Power(t2,2)) + 
               Power(s2,2)*(10 + 82*t2 + 41*Power(t2,2) - 
                  t1*(221 + 74*t2))) + 
            Power(s1,2)*(11 - 11*Power(s2,5) + 24*Power(t1,3) - 36*t2 + 
               6*Power(t2,2) + 12*Power(t2,3) + 
               8*Power(t1,2)*(1 + 3*t2) + 
               Power(s2,4)*(55 + 86*t1 + 51*t2) + 
               t1*(36 - 155*t2 - 16*Power(t2,2)) + 
               Power(s2,3)*(22*Power(t1,2) - t1*(466 + 245*t2) + 
                  29*(1 + 3*t2 + Power(t2,2))) + 
               Power(s2,2)*(-264 - 76*t2 - 186*Power(t2,2) - 
                  39*Power(t2,3) + Power(t1,2)*(283 + 4*t2) + 
                  3*t1*(87 + 104*t2 + 31*Power(t2,2))) + 
               s2*(176 + 8*Power(t1,3) + 17*t2 + 187*Power(t2,2) - 
                  42*Power(t2,3) - Power(t1,2)*(252 + 275*t2) + 
                  t1*(16 - 17*t2 + 187*Power(t2,2)))) + 
            s1*(-90 + 8*Power(t1,4) - 199*t2 - 14*Power(t2,2) - 
               17*Power(t2,3) - 8*Power(t2,4) - 
               30*Power(t1,3)*(2 + t2) + 
               Power(s2,5)*(23 + 22*t1 + 35*t2) - 
               Power(t1,2)*(28 + 13*t2 + 4*Power(t2,2)) + 
               2*t1*(41 + 71*t2 + 92*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,4)*(6 + 17*Power(t1,2) - 79*t2 - 
                  69*Power(t2,2) - 5*t1*(59 + 21*t2)) + 
               Power(s2,3)*(-182 - 123*t2 - 157*Power(t2,2) + 
                  2*Power(t2,3) - 6*Power(t1,2)*(-35 + 8*t2) + 
                  t1*(449 + 629*t2 + 174*Power(t2,2))) + 
               Power(s2,2)*(313 + 463*t2 + 93*Power(t2,2) + 
                  102*Power(t2,3) + 11*Power(t2,4) + 
                  Power(t1,3)*(13 + 4*t2) - 
                  Power(t1,2)*(668 + 412*t2 + Power(t2,2)) - 
                  t1*(184 + 63*t2 + 49*Power(t2,2) + 44*Power(t2,3))) + 
               s2*(-42 + 4*Power(t1,4) + Power(t1,3)*(146 - 45*t2) - 
                  76*t2 + 96*Power(t2,2) - 220*Power(t2,3) + 
                  62*Power(t2,4) + 
                  Power(t1,2)*(275 + 290*t2 + 216*Power(t2,2)) - 
                  t1*(19 + 414*t2 + 52*Power(t2,2) + 177*Power(t2,3))))) \
+ Power(s,2)*(12 - 41*t1 + 205*Power(t1,2) - 212*Power(t1,3) + 
            8*Power(t1,4) + 12*Power(t1,5) + 235*t2 - 592*t1*t2 + 
            589*Power(t1,2)*t2 - 197*Power(t1,3)*t2 - 
            40*Power(t1,4)*t2 + 2*Power(t1,5)*t2 + 277*Power(t2,2) - 
            761*t1*Power(t2,2) + 602*Power(t1,2)*Power(t2,2) - 
            51*Power(t1,3)*Power(t2,2) + 12*Power(t1,4)*Power(t2,2) + 
            82*Power(t2,3) - 241*t1*Power(t2,3) + 
            32*Power(t1,2)*Power(t2,3) - 26*Power(t1,3)*Power(t2,3) + 
            28*Power(t2,4) + 38*t1*Power(t2,4) + 
            20*Power(t1,2)*Power(t2,4) - Power(t2,5) - 
            7*t1*Power(t2,5) - Power(t2,6) + 
            Power(s1,5)*(-4 + 14*Power(s2,3) - 6*t1 + 
               Power(s2,2)*(-14 + 15*t1 - 11*t2) + 
               s2*(14 + 3*t1 - t2) + 2*t2) + 
            Power(s2,6)*(2 + Power(t1,2) - 26*t2 - 21*Power(t2,2) + 
               t1*(-7 + 4*t2)) + 
            Power(s2,5)*(-25 + 14*t2 + 71*Power(t2,2) + 
               33*Power(t2,3) - Power(t1,2)*(1 + 17*t2) + 
               t1*(46 + 141*t2 + 26*Power(t2,2))) + 
            Power(s2,4)*(50 + 171*t2 + 153*Power(t2,2) + 
               50*Power(t2,3) - 12*Power(t2,4) + 
               3*Power(t1,3)*(6 + t2) + 
               Power(t1,2)*(-156 - 82*t2 + 27*Power(t2,2)) - 
               2*t1*(-16 + 185*t2 + 173*Power(t2,2) + 27*Power(t2,3))) - 
            Power(s2,3)*(28 + 3*Power(t1,4) + 219*t2 + 
               239*Power(t2,2) + 51*Power(t2,3) + 87*Power(t2,4) + 
               Power(t1,3)*(-31 + 67*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(-294 - 849*t2 - 270*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(190 + 174*t2 + 273*Power(t2,2) - 109*Power(t2,3) - 
                  26*Power(t2,4))) + 
            Power(s2,2)*(-176 + 49*Power(t1,4) - 235*t2 - 
               34*Power(t2,2) - 104*Power(t2,3) - 78*Power(t2,4) - 
               9*Power(t2,5) + 
               Power(t1,3)*(-353 - 272*t2 + 88*Power(t2,2) + 
                  3*Power(t2,3)) - 
               Power(t1,2)*(107 + 264*t2 + 457*Power(t2,2) + 
                  261*Power(t2,3) + Power(t2,4)) + 
               t1*(471 + 382*t2 + 657*Power(t2,2) + 492*Power(t2,3) + 
                  142*Power(t2,4) - 2*Power(t2,5))) + 
            s2*(195 + 6*Power(t1,5) + 266*t2 + 168*Power(t2,2) + 
               188*Power(t2,3) + 131*Power(t2,4) - 54*Power(t2,5) + 
               14*Power(t2,6) - 
               Power(t1,4)*(48 + 83*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(288 + 682*t2 + 244*Power(t2,2) - 
                  13*Power(t2,3)) + 
               Power(t1,2)*(160 - 662*t2 - 730*Power(t2,2) - 
                  48*Power(t2,3) + 37*Power(t2,4)) - 
               t1*(533 + 38*t2 - 256*Power(t2,2) + 135*Power(t2,3) + 
                  25*Power(t2,4) + 33*Power(t2,5))) + 
            Power(s1,4)*(-14 - 3*Power(s2,4) + 6*Power(t1,2) + 
               2*Power(s2,3)*(-15 + 56*t1 - 43*t2) + 22*t2 - 
               9*Power(t2,2) + t1*(23 + 21*t2) + 
               s2*(-15 - 11*t1 + 96*Power(t1,2) - 69*t2 - 95*t1*t2 + 
                  18*Power(t2,2)) + 
               Power(s2,2)*(48 + 108*t2 + 44*Power(t2,2) - 
                  t1*(117 + 61*t2))) + 
            Power(s1,3)*(47 - 26*Power(s2,5) + 36*Power(t1,3) + 31*t2 - 
               41*Power(t2,2) + 16*Power(t2,3) - 
               44*Power(t1,2)*(1 + t2) + 
               2*Power(s2,4)*(4 + 75*t1 + 5*t2) + 
               t1*(32 - 95*t2 - 20*Power(t2,2)) + 
               Power(s2,3)*(169 + 28*Power(t1,2) + 285*t2 + 
                  174*Power(t2,2) - t1*(429 + 395*t2)) + 
               Power(s2,2)*(-213 - 14*t2 - 231*Power(t2,2) - 
                  66*Power(t2,3) + Power(t1,2)*(357 + 16*t2) + 
                  t1*(-158 + 104*t2 + 95*Power(t2,2))) + 
               s2*(-33 + 12*Power(t1,3) - 159*t2 + 177*Power(t2,2) - 
                  62*Power(t2,3) - 5*Power(t1,2)*(24 + 65*t2) + 
                  2*t1*(93 + 53*t2 + 150*Power(t2,2)))) + 
            Power(s1,2)*(7 - 13*Power(s2,6) + 12*Power(t1,4) - 28*t2 + 
               8*Power(t2,2) + 31*Power(t2,3) - 14*Power(t2,4) - 
               2*Power(t1,3)*(23 + 49*t2) + 
               Power(s2,5)*(61 + 74*t1 + 89*t2) + 
               Power(t1,2)*(76 + 141*t2 + 90*Power(t2,2)) + 
               t1*(4 - 301*t2 + 159*Power(t2,2) - 6*Power(t2,3)) + 
               Power(s2,4)*(150 + 33*Power(t1,2) + 88*t2 - 
                  23*Power(t2,2) - t1*(557 + 376*t2)) + 
               Power(s2,3)*(-391 + Power(t1,2)*(467 - 66*t2) - 
                  451*t2 - 567*Power(t2,2) - 146*Power(t2,3) + 
                  2*t1*(81 + 458*t2 + 240*Power(t2,2))) + 
               Power(s2,2)*(51 + 224*t2 - 194*Power(t2,2) + 
                  185*Power(t2,3) + 44*Power(t2,4) + 
                  Power(t1,3)*(39 + 6*t2) - 
                  Power(t1,2)*(917 + 948*t2 + 33*Power(t2,2)) + 
                  t1*(515 + 999*t2 + 285*Power(t2,2) - 69*Power(t2,3))) \
+ s2*(263 + 6*Power(t1,4) + Power(t1,3)*(292 - 45*t2) + 251*t2 + 
                  494*Power(t2,2) - 257*Power(t2,3) + 88*Power(t2,4) + 
                  Power(t1,2)*(-151 + 81*t2 + 399*Power(t2,2)) - 
                  t1*(209 + 440*t2 + 204*Power(t2,2) + 360*Power(t2,3)))\
) + s1*(-48 + Power(t1,4)*(32 - 30*t2) - 293*t2 - 101*Power(t2,2) - 
               53*Power(t2,3) - 7*Power(t2,4) + 6*Power(t2,5) + 
               Power(s2,6)*(13 + 13*t1 + 34*t2) + 
               Power(s2,5)*(27 - 199*t1 + 10*Power(t1,2) - 132*t2 - 
                  96*t1*t2 - 96*Power(t2,2)) + 
               Power(t1,3)*(84 + 83*t2 + 88*Power(t2,2)) - 
               Power(t1,2)*(19 + 667*t2 + 129*Power(t2,2) + 
                  72*Power(t2,3)) + 
               t1*(-26 + 771*t2 + 510*Power(t2,2) - 125*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s2,4)*(-207 + Power(t1,2)*(173 - 71*t2) - 315*t2 - 
                  146*Power(t2,2) + 28*Power(t2,3) + 
                  t1*(388 + 906*t2 + 280*Power(t2,2))) + 
               Power(s2,3)*(196 + 624*t2 + 333*Power(t2,2) + 
                  399*Power(t2,3) + 44*Power(t2,4) + 
                  Power(t1,3)*(39 + 9*t2) + 
                  2*Power(t1,2)*(-527 - 355*t2 + 24*Power(t2,2)) + 
                  t1*(286 + 127*t2 - 596*Power(t2,2) - 223*Power(t2,3))) \
+ Power(s2,2)*(292 + 9*Power(t1,4) - 15*t2 + 93*Power(t2,2) + 
                  238*Power(t2,3) - 39*Power(t2,4) - 11*Power(t2,5) - 
                  3*Power(t1,3)*(-128 + 48*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (415 + 1303*t2 + 852*Power(t2,2) + 18*Power(t2,3)) + 
                  t1*(-555 - 1126*t2 - 1333*Power(t2,2) - 
                     414*Power(t2,3) + 22*Power(t2,4))) + 
               s2*(-379 - 400*t2 - 406*Power(t2,2) - 451*Power(t2,3) + 
                  189*Power(t2,4) - 57*Power(t2,5) + 
                  Power(t1,4)*(38 + 3*t2) + 
                  Power(t1,3)*(-588 - 484*t2 + 46*Power(t2,2)) + 
                  Power(t1,2)*
                   (150 + 889*t2 + 87*Power(t2,2) - 207*Power(t2,3)) + 
                  t1*(526 - 122*t2 + 389*Power(t2,2) + 134*Power(t2,3) + 
                     185*Power(t2,4))))) - 
         s*(-65 + 342*t1 - 477*Power(t1,2) + 80*Power(t1,3) + 
            160*Power(t1,4) - 40*Power(t1,5) + 
            Power(s1,6)*(11*Power(s2,3) - 2*t1 + 
               Power(s2,2)*(-11 + 6*t1 - 3*t2) + s2*(6 + 7*t1 - 2*t2)) + 
            61*t2 - 341*t1*t2 + 885*Power(t1,2)*t2 - 907*Power(t1,3)*t2 + 
            300*Power(t1,4)*t2 - 20*Power(t1,5)*t2 + 
            Power(s2,7)*(-7 + t1 - 10*t2)*t2 + 184*Power(t2,2) - 
            891*t1*Power(t2,2) + 1273*Power(t1,2)*Power(t2,2) - 
            558*Power(t1,3)*Power(t2,2) + 68*Power(t1,4)*Power(t2,2) - 
            2*Power(t1,5)*Power(t2,2) + 102*Power(t2,3) - 
            334*t1*Power(t2,3) + 156*Power(t1,2)*Power(t2,3) - 
            27*Power(t1,3)*Power(t2,3) + 35*Power(t2,4) + 
            50*t1*Power(t2,4) - 18*Power(t1,2)*Power(t2,4) + 
            6*Power(t1,3)*Power(t2,4) - 11*Power(t2,5) - 
            5*t1*Power(t2,5) - 7*Power(t1,2)*Power(t2,5) - 
            2*Power(t2,6) + 3*t1*Power(t2,6) - 
            Power(s2,6)*(3 + t2 - 46*Power(t2,2) - 20*Power(t2,3) + 
               Power(t1,2)*(1 + 4*t2) - t1*(4 + 45*t2 + 16*Power(t2,2))) \
+ Power(s2,5)*(-6 + 97*t2 + 96*Power(t2,2) + 15*Power(t2,3) - 
               9*Power(t2,4) + Power(t1,3)*(4 + t2) + 
               Power(t1,2)*(-36 - 37*t2 + 8*Power(t2,2)) - 
               t1*(-38 + 173*t2 + 221*Power(t2,2) + 42*Power(t2,3))) - 
            Power(s2,4)*(-59 + Power(t1,4) - 22*t2 + 131*Power(t2,2) + 
               151*Power(t2,3) + 69*Power(t2,4) + 2*Power(t2,5) + 
               3*Power(t1,3)*(-7 + 10*t2 + Power(t2,2)) + 
               Power(t1,2)*(-108 - 520*t2 - 211*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(187 + 318*t2 + 61*Power(t2,2) - 139*Power(t2,3) - 
                  32*Power(t2,4))) + 
            Power(s2,3)*(-22 - 83*t2 - 115*Power(t2,2) - 
               222*Power(t2,3) - 188*Power(t2,4) + 45*Power(t2,5) + 
               Power(t2,6) + Power(t1,4)*(22 + 9*t2) + 
               Power(t1,3)*(-318 - 240*t2 + 35*Power(t2,2) + 
                  3*Power(t2,3)) - 
               Power(t1,2)*(-328 + 77*t2 + 730*Power(t2,2) + 
                  241*Power(t2,3) + 2*Power(t2,4)) + 
               t1*(-10 + 193*t2 + 770*Power(t2,2) + 838*Power(t2,3) + 
                  44*Power(t2,4) - 7*Power(t2,5))) + 
            Power(s2,2)*(11 - 6*Power(t1,5) + 186*t2 + 149*Power(t2,2) - 
               112*Power(t2,3) + 77*Power(t2,4) + 70*Power(t2,5) + 
               15*Power(t2,6) + 
               Power(t1,4)*(71 - 100*t2 - 3*Power(t2,2)) + 
               Power(t1,3)*(159 + 925*t2 + 552*Power(t2,2) - 
                  44*Power(t2,3) - Power(t2,4)) + 
               t1*(-61 + 22*t2 + 877*Power(t2,2) + 394*Power(t2,3) - 
                  190*Power(t2,4) - 118*Power(t2,5)) + 
               Power(t1,2)*(-174 - 957*t2 - 1499*Power(t2,2) - 
                  242*Power(t2,3) + 164*Power(t2,4) + Power(t2,5))) + 
            s2*(138 + 68*Power(t1,5) + 119*t2 + 133*Power(t2,2) + 
               355*Power(t2,3) + 160*Power(t2,4) - 23*Power(t2,5) + 
               17*Power(t2,6) - 3*Power(t2,7) + 
               Power(t1,4)*(-340 - 303*t2 + 66*Power(t2,2) + 
                  3*Power(t2,3)) - 
               Power(t1,3)*(-74 - 739*t2 + 113*Power(t2,2) + 
                  225*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(500 - 550*t2 + 254*Power(t2,2) + 
                  758*Power(t2,3) + 148*Power(t2,4) - 6*Power(t2,5)) + 
               t1*(-440 + 23*t2 - 401*Power(t2,2) - 884*Power(t2,3) - 
                  294*Power(t2,4) - 11*Power(t2,5) + 7*Power(t2,6))) + 
            Power(s1,5)*(-6 + 7*Power(s2,4) + 12*Power(t1,2) + 
               Power(s2,3)*(-34 + 63*t1 - 75*t2) + 4*t2 + 
               t1*(2 + 5*t2) + 
               s2*(29 + 43*Power(t1,2) - 33*t2 + 13*Power(t2,2) - 
                  21*t1*(1 + 3*t2)) + 
               Power(s2,2)*(-2 + 55*t2 + 15*Power(t2,2) - t1*(7 + 23*t2))\
) + Power(s1,4)*(-19*Power(s2,5) + 24*Power(t1,3) + 
               Power(s2,4)*(-51 + 114*t1 - 49*t2) - 
               3*Power(t1,2)*(8 + 21*t2) - 
               9*(2 - 3*t2 + 2*Power(t2,2)) + 
               t1*(64 - 19*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*(67 + 17*Power(t1,2) + 226*t2 + 
                  191*Power(t2,2) - t1*(129 + 275*t2)) + 
               Power(s2,2)*(28 + 152*t2 - 95*Power(t2,2) - 
                  30*Power(t2,3) + 2*Power(t1,2)*(114 + 7*t2) + 
                  t1*(-295 - 152*t2 + 32*Power(t2,2))) + 
               s2*(-29 + 8*Power(t1,3) + Power(t1,2)*(43 - 170*t2) - 
                  140*t2 + 89*Power(t2,2) - 35*Power(t2,3) + 
                  t1*(28 + 47*t2 + 189*Power(t2,2)))) + 
            Power(s1,3)*(20 - 23*Power(s2,6) + 8*Power(t1,4) + 
               Power(t1,3)*(36 - 82*t2) + 36*t2 - 34*Power(t2,2) + 
               32*Power(t2,3) + Power(s2,5)*(17 + 82*t1 + 65*t2) + 
               Power(t1,2)*(4 + 69*t2 + 124*Power(t2,2)) + 
               t1*(14 - 234*t2 + 50*Power(t2,2) - 22*Power(t2,3)) + 
               Power(s2,4)*(245 + 27*Power(t1,2) + 291*t2 + 
                  107*Power(t2,2) - t1*(372 + 409*t2)) + 
               Power(s2,3)*(-71 + Power(t1,2)*(428 - 40*t2) + 5*t2 - 
                  519*Power(t2,2) - 234*Power(t2,3) + 
                  t1*(-503 + 262*t2 + 454*Power(t2,2))) + 
               s2*(-15 + 4*Power(t1,4) + Power(t1,3)*(262 - 15*t2) - 
                  134*t2 + 269*Power(t2,2) - 146*Power(t2,3) + 
                  50*Power(t2,4) + 
                  Power(t1,2)*(-467 - 364*t2 + 258*Power(t2,2)) + 
                  t1*(217 + 353*t2 - 4*Power(t2,2) - 266*Power(t2,3))) + 
               Power(s2,2)*(-209 - 276*t2 - 514*Power(t2,2) + 
                  50*Power(t2,3) + 30*Power(t2,4) + 
                  Power(t1,3)*(39 + 4*t2) - 
                  Power(t1,2)*(342 + 836*t2 + 43*Power(t2,2)) + 
                  t1*(343 + 1193*t2 + 616*Power(t2,2) - 18*Power(t2,3)))) \
+ Power(s1,2)*(21 - 8*Power(s2,7) + Power(t1,4)*(76 - 18*t2) + 22*t2 + 
               17*Power(t2,2) - 28*Power(t2,4) + 
               4*Power(s2,6)*(10 + 7*t1 + 17*t2) + 
               Power(t1,3)*(-156 - 127*t2 + 98*Power(t2,2)) + 
               Power(t1,2)*(279 + 152*t2 - 84*Power(t2,2) - 
                  114*Power(t2,3)) + 
               2*t1*(-76 - 148*t2 + 163*Power(t2,2) - 28*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,5)*(104 + 11*Power(t1,2) - 3*t2 - 
                  82*Power(t2,2) - 6*t1*(49 + 36*t2)) + 
               Power(s2,4)*(-214 + Power(t1,2)*(303 - 70*t2) - 702*t2 - 
                  498*Power(t2,2) - 97*Power(t2,3) + 
                  t1*(117 + 913*t2 + 508*Power(t2,2))) + 
               Power(s2,3)*(-189 - 140*t2 - 399*Power(t2,2) + 
                  541*Power(t2,3) + 141*Power(t2,4) + 
                  9*Power(t1,3)*(4 + t2) + 
                  Power(t1,2)*(-1106 - 1127*t2 + 27*Power(t2,2)) + 
                  t1*(937 + 2004*t2 - 93*Power(t2,2) - 342*Power(t2,3))) \
+ Power(s2,2)*(403 + 9*Power(t1,4) + 369*t2 + 545*Power(t2,2) + 
                  650*Power(t2,3) + 35*Power(t2,4) - 15*Power(t2,5) + 
                  Power(t1,3)*(589 - 132*t2 - 9*Power(t2,2)) + 
                  Power(t1,2)*
                   (-811 + 369*t2 + 1152*Power(t2,2) + 45*Power(t2,3)) \
+ t1*(80 - 326*t2 - 1691*Power(t2,2) - 866*Power(t2,3) + 2*Power(t2,4))) \
+ s2*(87 + 415*t2 + 515*Power(t2,2) - 257*Power(t2,3) + 
                  144*Power(t2,4) - 40*Power(t2,5) + 
                  Power(t1,4)*(58 + 3*t2) + 
                  Power(t1,3)*(-352 - 733*t2 + 5*Power(t2,2)) + 
                  Power(t1,2)*
                   (281 + 1768*t2 + 747*Power(t2,2) - 184*Power(t2,3)) + 
                  t1*(-177 - 1434*t2 - 1084*Power(t2,2) - 
                     60*Power(t2,3) + 189*Power(t2,4)))) + 
            s1*(4*Power(t1,5)*(6 + t2) + 3*Power(s2,7)*(1 + t1 + 6*t2) + 
               2*Power(t1,4)*(-78 - 72*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(216 + 708*t2 + 118*Power(t2,2) - 
                  46*Power(t2,3)) + 
               Power(t1,2)*(110 - 1533*t2 - 312*Power(t2,2) + 
                  57*Power(t2,3) + 48*Power(t2,4)) + 
               t1*(-220 + 1021*t2 + 616*Power(t2,2) - 206*Power(t2,3) + 
                  28*Power(t2,4) - 15*Power(t2,5)) + 
               2*(24 - 99*t2 - 72*Power(t2,2) - 35*Power(t2,3) + 
                  12*Power(t2,4) + 6*Power(t2,5)) + 
               Power(s2,6)*(14 + Power(t1,2) - 88*t2 - 65*Power(t2,2) - 
                  t1*(55 + 42*t2)) + 
               Power(s2,5)*(-99 + Power(t1,2)*(61 - 28*t2) - 207*t2 - 
                  29*Power(t2,2) + 45*Power(t2,3) + 
                  t1*(150 + 531*t2 + 176*Power(t2,2))) + 
               Power(s2,4)*(-59 + 367*t2 + 608*Power(t2,2) + 
                  327*Power(t2,3) + 34*Power(t2,4) + 
                  3*Power(t1,3)*(9 + 2*t2) + 
                  Power(t1,2)*(-583 - 502*t2 + 46*Power(t2,2)) - 
                  t1*(-421 + 93*t2 + 680*Power(t2,2) + 245*Power(t2,3))) \
+ Power(s2,3)*(186 - 6*Power(t1,4) + 316*t2 + 433*Power(t2,2) + 
                  515*Power(t2,3) - 259*Power(t2,4) - 35*Power(t2,5) + 
                  Power(t1,3)*(290 - 99*t2 - 12*Power(t2,2)) + 
                  Power(t1,2)*
                   (147 + 1900*t2 + 940*Power(t2,2) - 2*Power(t2,3)) + 
                  t1*(-419 - 1755*t2 - 2339*Power(t2,2) - 
                     84*Power(t2,3) + 107*Power(t2,4))) + 
               s2*(-216 + 12*Power(t1,5) - 203*t2 - 755*Power(t2,2) - 
                  512*Power(t2,3) + 122*Power(t2,4) - 77*Power(t2,5) + 
                  17*Power(t2,6) - 
                  2*Power(t1,4)*(-118 + 73*t2 + 5*Power(t2,2)) + 
                  Power(t1,3)*
                   (-390 + 485*t2 + 696*Power(t2,2) + 3*Power(t2,3)) + 
                  Power(t1,2)*
                   (56 - 518*t2 - 2059*Power(t2,2) - 574*Power(t2,3) + 
                     59*Power(t2,4)) + 
                  t1*(274 + 546*t2 + 2101*Power(t2,2) + 
                     997*Power(t2,3) + 49*Power(t2,4) - 63*Power(t2,5))) \
+ Power(s2,2)*(-175 + 68*Power(t1,4) - 577*t2 - 48*Power(t2,2) - 
                  374*Power(t2,3) - 356*Power(t2,4) - 49*Power(t2,5) + 
                  3*Power(t2,6) + 
                  Power(t1,3)*
                   (-967 - 1118*t2 + 137*Power(t2,2) + 6*Power(t2,3)) + 
                  Power(t1,2)*
                   (988 + 2247*t2 + 215*Power(t2,2) - 708*Power(t2,3) - 
                     17*Power(t2,4)) + 
                  t1*(10 - 898*t2 - 411*Power(t2,2) + 983*Power(t2,3) + 
                     527*Power(t2,4) + Power(t2,5))))))*
       T2q(1 - s + s2 - t1,1 - s1 - t1 + t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s1 + t1 - t2)*
       Power(1 - s + s*s2 - s1*s2 - t1 + s2*t2,2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) - 
    (8*(-2*Power(s,7)*(-1 + s2)*(t1 - t2) + 
         Power(s,6)*(-2 + 10*Power(t1,2) + 11*t2 + 2*s1*t2 - 
            Power(t2,2) - t1*(9 + 2*s1 + 9*t2) + 
            Power(s2,2)*(-4 + 8*t1 + 2*Power(t1,2) - 8*t2 - t1*t2 - 
               Power(t2,2) + s1*(2 + t1 + t2)) - 
            s2*(-6 + t1 + 14*Power(t1,2) + t2 - 14*t1*t2 + 
               s1*(2 - 3*t1 + 5*t2))) + 
         Power(s,5)*(5 + 34*t1 - 43*Power(t1,2) + 20*Power(t1,3) - 
            45*t2 + 40*t1*t2 - 14*Power(t1,2)*t2 + 11*Power(t2,2) - 
            7*t1*Power(t2,2) + Power(t2,3) + 
            Power(s1,2)*(Power(s2,3) - 2*t1 + 2*t2 + 
               s2*(2 + 2*t1 + t2) - Power(s2,2)*(5 + 3*t1 + 2*t2)) + 
            Power(s2,3)*(10 - 7*Power(t1,2) + 9*t2 + 3*Power(t2,2) + 
               t1*(-7 + 3*t2)) + 
            s2*(-9 - 30*Power(t1,3) - 8*Power(t2,2) - 5*Power(t2,3) + 
               Power(t1,2)*(14 + 15*t2) + 
               5*t1*(1 - 3*t2 + 4*Power(t2,2))) + 
            Power(s2,2)*(-8 + 4*Power(t1,3) + 25*t2 + 5*Power(t2,2) + 
               7*Power(t1,2)*(5 + t2) - t1*(23 + 38*t2 + 11*Power(t2,2))\
) + s1*(6 + 5*t1 - 12*Power(t1,2) - 14*t2 + 16*t1*t2 - 3*Power(t2,2) - 
               2*Power(s2,3)*(3 + t1 + 2*t2) + 
               2*s2*(-5 + 16*Power(t1,2) + t1*(3 - 23*t2) + 11*t2 + 
                  2*Power(t2,2)) + 
               Power(s2,2)*(-5*Power(t1,2) + 3*t1*(-5 + 4*t2) + 
                  2*(7 + 3*t2 + Power(t2,2))))) + 
         Power(s,4)*(-1 - 116*t1 + 189*Power(t1,2) - 90*Power(t1,3) + 
            18*Power(t1,4) + 145*t2 - 173*t1*t2 + 58*Power(t1,2)*t2 - 
            37*Power(t2,2) + 47*t1*Power(t2,2) - 
            24*Power(t1,2)*Power(t2,2) - 5*Power(t2,3) + 
            6*t1*Power(t2,3) + 
            Power(s2,4)*(-4 + 8*Power(t1,2) - 5*t2 - 4*Power(t2,2) - 
               t1*(4 + t2)) + 
            Power(s1,3)*(-2*Power(s2,3) + 2*t1 + 
               Power(s2,2)*(1 + 3*t1 + t2) + s2*(2 - 5*t1 + 2*t2)) + 
            Power(s1,2)*(2 - 2*Power(s2,4) - 8*Power(t1,2) - 10*t2 + 
               Power(s2,3)*(4 + 4*t1 + 5*t2) + t1*(-2 + 7*t2) + 
               Power(s2,2)*(2 + 3*Power(t1,2) + t1*(8 - 20*t2) + 
                  13*t2 - 2*Power(t2,2)) - 
               s2*(3 + 17*Power(t1,2) + t1*(3 - 34*t2) + 12*t2 + 
                  7*Power(t2,2))) - 
            Power(s2,3)*(-5 + 13*Power(t1,3) + t2 + 16*Power(t2,2) + 
               Power(t2,3) + Power(t1,2)*(3 + 19*t2) - 
               t1*(11 + 18*t2 + 28*Power(t2,2))) + 
            Power(s2,2)*(-23 + 2*Power(t1,4) - 76*t2 + 22*Power(t2,2) + 
               11*Power(t2,3) + Power(t1,3)*(67 + 20*t2) - 
               Power(t1,2)*(107 + 75*t2 + 16*Power(t2,2)) + 
               t1*(61 + 143*t2 + 7*Power(t2,2) - 6*Power(t2,3))) + 
            s2*(28 - 27*Power(t1,4) + Power(t1,3)*(33 - 16*t2) - 
               11*t2 + 48*Power(t2,2) + 18*Power(t2,3) - 
               3*Power(t2,4) + 
               Power(t1,2)*(-39 + 35*t2 + 50*Power(t2,2)) - 
               t1*(-5 + 58*t2 + 101*Power(t2,2) + 4*Power(t2,3))) + 
            s1*(-42 - 32*Power(t1,3) + 35*t2 + 15*Power(t2,2) + 
               13*Power(t1,2)*(1 + 4*t2) + Power(s2,4)*(5 + t1 + 6*t2) + 
               t1*(31 - 65*t2 - 15*Power(t2,2)) + 
               Power(s2,3)*(-29 + 13*Power(t1,2) + 10*t2 - 
                  2*Power(t2,2) - 6*t1*(-3 + 5*t2)) + 
               s2*(31 + 76*Power(t1,3) - 53*t2 - 8*Power(t2,2) + 
                  8*Power(t2,3) - Power(t1,2)*(7 + 79*t2) + 
                  t1*(-50 + 158*t2 - 25*Power(t2,2))) + 
               Power(s2,2)*(26 - 13*Power(t1,3) - 49*t2 - 
                  25*Power(t2,2) + Power(t2,3) + 
                  Power(t1,2)*(-61 + 4*t2) + 
                  t1*(36 + 19*t2 + 23*Power(t2,2))))) + 
         Power(s,3)*(Power(s1,4)*s2*
             (Power(s2,2) - s2*(-2 + t1) + 2*t1) + 
            Power(s2,5)*(-4 + 8*t1 - 4*Power(t1,2) + 2*Power(t2,2)) + 
            Power(s2,4)*(-4 + 8*Power(t1,3) - 9*t2 + 14*Power(t2,2) + 
               Power(t2,3) + 5*Power(t1,2)*(-4 + 5*t2) - 
               2*t1*(-8 + 8*t2 + 11*Power(t2,2))) + 
            2*(2*Power(t1,5) + 4*Power(t1,4)*(-13 + 3*t2) + 
               Power(t1,3)*(206 + 18*t2 - 20*Power(t2,2)) + 
               5*t2*(-28 + 5*t2 + Power(t2,2)) + 
               2*Power(t1,2)*
                (-132 - 84*t2 + 23*Power(t2,2) + 3*Power(t2,3)) - 
               2*t1*(-54 - 139*t2 + 25*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s2,3)*(3 - 7*Power(t1,4) + 69*t2 + 46*Power(t2,2) - 
               12*Power(t2,3) - Power(t1,3)*(10 + 43*t2) + 
               Power(t1,2)*(44 + 93*t2 + 31*Power(t2,2)) + 
               t1*(-30 - 119*t2 - 95*Power(t2,2) + 9*Power(t2,3))) + 
            Power(s2,2)*(101 + 52*t2 - 110*Power(t2,2) - 
               18*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,4)*(59 + 16*t2) - 
               Power(t1,3)*(174 + 51*t2 + 4*Power(t2,2)) - 
               2*Power(t1,2)*
                (-136 - 84*t2 + 10*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(-258 - 185*t2 + 156*Power(t2,2) + 29*Power(t2,3) - 
                  2*Power(t2,4))) - 
            s2*(121 + 11*Power(t1,5) + 20*t2 + 146*Power(t2,2) + 
               27*Power(t2,3) - 12*Power(t2,4) + 
               Power(t1,4)*(-31 + 28*t2) - 
               2*Power(t1,3)*(-37 + 56*t2 + 15*Power(t2,2)) + 
               Power(t1,2)*(22 + 282*t2 + 173*Power(t2,2) - 
                  18*Power(t2,3)) + 
               t1*(-197 - 218*t2 - 257*Power(t2,2) - 8*Power(t2,3) + 
                  9*Power(t2,4))) + 
            Power(s1,3)*(Power(s2,4) + 2*t1*(-4 + 5*t1) + 
               Power(s2,3)*(4 - 3*t1 - 3*t2) - 
               s2*(2 + 7*Power(t1,2) + t1*(-12 + t2) + 8*t2) + 
               Power(s2,2)*(-3 + Power(t1,2) - 13*t2 + t1*(-1 + 9*t2))) \
+ Power(s1,2)*(2*Power(s2,5) - Power(s2,4)*(4 + 4*t1 + t2) + 
               2*(-6 - 3*Power(t1,3) + t1*(19 - 14*t2) + 
                  Power(t1,2)*(-9 + t2) + 10*t2) + 
               Power(s2,3)*(45 - 12*Power(t1,2) - 28*t2 + 
                  3*Power(t2,2) + t1*(-51 + 23*t2)) + 
               Power(s2,2)*(-23 + 15*Power(t1,3) + 
                  Power(t1,2)*(47 - 24*t2) - 8*t2 + 23*Power(t2,2) + 
                  t1*(-17 + 39*t2 - 17*Power(t2,2))) + 
               s2*(-61*Power(t1,3) + 7*Power(t1,2)*(5 + 12*t2) + 
                  t1*(36 - 112*t2 - 13*Power(t2,2)) + 
                  7*(-6 + 3*t2 + 4*Power(t2,2)))) - 
            s1*(4*Power(s2,5)*t2 + 
               Power(s2,4)*(-17 + 17*Power(t1,2) + 10*t2 - 26*t1*t2 + 
                  Power(t2,2)) + 
               2*(-52 + 21*Power(t1,4) + 14*t2 + 15*Power(t2,2) - 
                  Power(t1,3)*(3 + 38*t2) + 
                  t1*(99 - 36*t2 - 30*Power(t2,2)) + 
                  Power(t1,2)*(-65 + 62*t2 + 12*Power(t2,2))) + 
               Power(s2,3)*(19 - 25*Power(t1,3) + 83*t2 - 
                  36*Power(t2,2) + Power(t2,3) + 
                  Power(t1,2)*(7 + 11*t2) + 
                  t1*(-1 - 130*t2 + 29*Power(t2,2))) + 
               s2*(-29 - 67*Power(t1,4) - 160*t2 - 8*Power(t2,2) + 
                  32*Power(t2,3) + Power(t1,3)*(6 + 13*t2) + 
                  Power(t1,2)*(38 - 198*t2 + 95*Power(t2,2)) + 
                  t1*(52 + 281*t2 - 92*Power(t2,2) - 21*Power(t2,3))) + 
               Power(s2,2)*(75 + 7*Power(t1,4) - 173*t2 - 
                  29*Power(t2,2) + 15*Power(t2,3) + 
                  Power(t1,3)*(101 + 27*t2) - 
                  Power(t1,2)*(86 + 45*t2 + 33*Power(t2,2)) + 
                  t1*(-97 + 235*t2 + 67*Power(t2,2) - 11*Power(t2,3))))) \
+ s*(-1 + t1)*(Power(s1,4)*s2*
             (2*Power(s2,3) + Power(s2,2)*(7 - 3*t1) + 6*(-1 + t1)*t1 + 
               s2*(6 - 5*t1 + Power(t1,2))) - 
            6*Power(s2,5)*(-1 + t1)*Power(t2,2) + 
            Power(s2,4)*t2*(-11 + Power(t1,3) - 12*t2 + 
               27*Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,2)*(-13 + 8*t2) + t1*(23 + 4*t2 + 5*Power(t2,2))\
) - (-1 + t1)*(-11 + 2*Power(t1,5) + Power(t1,4)*(37 - 8*t2) + 169*t2 + 
               5*Power(t2,2) - 5*Power(t2,3) + 
               Power(t1,3)*(-204 - 32*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(278 + 225*t2 - 11*Power(t2,2) - 
                  3*Power(t2,3)) + 
               t1*(-102 - 354*t2 - 9*Power(t2,2) + 14*Power(t2,3))) - 
            Power(s2,3)*(13 + 26*t2 - 65*Power(t2,2) - 40*Power(t2,3) + 
               2*Power(t2,4) + Power(t1,4)*(1 + 4*t2) + 
               2*Power(t1,3)*(-8 - 14*t2 + Power(t2,2)) + 
               Power(t1,2)*(42 + 70*t2 + 21*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(-40 - 72*t2 + 42*Power(t2,2) + 43*Power(t2,3) - 
                  6*Power(t2,4))) - 
            s2*(-1 + t1)*(122 + Power(t1,5) + 94*t2 + 206*Power(t2,2) + 
               44*Power(t2,3) - 12*Power(t2,4) - 
               Power(t1,4)*(11 + 3*t2) + 
               Power(t1,3)*(-11 - 29*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(173 + 279*t2 + 55*Power(t2,2) - 
                  11*Power(t2,3)) + 
               t1*(-274 - 341*t2 - 263*Power(t2,2) - 6*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s2,2)*(-17 + 167*t2 + 165*Power(t2,2) + 
               36*Power(t2,3) + 11*Power(t2,4) + Power(t1,5)*(4 + t2) - 
               Power(t1,4)*(37 + 10*t2 + Power(t2,2)) + 
               Power(t1,3)*(104 + 11*t2 + 18*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,2)*(-130 + 171*t2 + 130*Power(t2,2) + 
                  Power(t2,3) - 2*Power(t2,4)) - 
               t1*(-76 + 340*t2 + 312*Power(t2,2) + 47*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s1,3)*(2*t1*(4 - 11*t1 + 7*Power(t1,2)) - 
               Power(s2,4)*(21 + 11*t1 + 4*t2) + 
               Power(s2,3)*(-60 + 11*Power(t1,2) - 19*t2 + 
                  3*t1*(19 + t2)) + 
               Power(s2,2)*(-53 - 5*Power(t1,3) - 25*t2 + 
                  Power(t1,2)*(-22 + 3*t2) + 2*t1*(44 + 5*t2)) + 
               s2*(-1 + t1)*(26 + 19*Power(t1,2) - 8*t2 - 
                  t1*(28 + 19*t2))) + 
            Power(s1,2)*(-6*Power(s2,5)*(-1 + t1) + 
               Power(s2,4)*(-10 + 10*Power(t1,2) + 69*t2 + 27*t1*t2) + 
               2*(-1 + t1)*(8*Power(t1,3) + t1*(26 - 4*t2) + 
                  5*(-2 + t2) - Power(t1,2)*(21 + 10*t2)) + 
               Power(s2,2)*(182 + 128*t2 + 43*Power(t2,2) + 
                  2*Power(t1,3)*(28 + 5*t2) + 
                  Power(t1,2)*(68 + 35*t2 - 11*Power(t2,2)) - 
                  t1*(306 + 197*t2 + 8*Power(t2,2))) + 
               Power(s2,3)*(38 - 5*Power(t1,3) + 168*t2 + 
                  15*Power(t2,2) - Power(t1,2)*(42 + 19*t2) + 
                  t1*(9 - 173*t2 + 9*Power(t2,2))) - 
               s2*(-1 + t1)*(120 + 17*Power(t1,3) + 82*t2 - 
                  28*Power(t2,2) + Power(t1,2)*(61 + 5*t2) - 
                  t1*(190 + 26*t2 + 17*Power(t2,2)))) + 
            s1*(12*Power(s2,5)*(-1 + t1)*t2 - 
               (-1 + t1)*(-74 + 2*Power(t1,4) + 
                  Power(t1,3)*(51 - 4*t2) - 34*t2 + 15*Power(t2,2) + 
                  t1*(171 + 68*t2 - 30*Power(t2,2)) - 
                  3*Power(t1,2)*(50 + 6*t2 + Power(t2,2))) + 
               Power(s2,4)*(7 + 3*Power(t1,3) + 
                  Power(t1,2)*(1 - 18*t2) + 22*t2 - 75*Power(t2,2) + 
                  4*Power(t2,3) - t1*(11 + 4*t2 + 21*Power(t2,2))) + 
               s2*(-1 + t1)*(113 + Power(t1,4) + 298*t2 + 
                  100*Power(t2,2) - 32*Power(t2,3) + 
                  Power(t1,3)*(16 + 23*t2) + 
                  Power(t1,2)*(196 + 96*t2 - 25*Power(t2,2)) - 
                  t1*(326 + 401*t2 + 4*Power(t2,2) + Power(t2,3))) - 
               Power(s2,2)*(139 + 357*t2 + 111*Power(t2,2) + 
                  35*Power(t2,3) + 5*Power(t1,4)*(2 + t2) + 
                  Power(t1,3)*(-15 + 46*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*
                   (139 + 246*t2 + 14*Power(t2,2) - 9*Power(t2,3)) - 
                  3*t1*(91 + 218*t2 + 52*Power(t2,2) + 2*Power(t2,3))) + 
               Power(s2,3)*(27 + Power(t1,4) - 111*t2 - 
                  148*Power(t2,2) - Power(t2,3) + 
                  5*Power(t1,3)*(-4 + 3*t2) + 
                  Power(t1,2)*(64 + 39*t2 + 13*Power(t2,2)) - 
                  3*t1*(24 - 19*t2 - 53*Power(t2,2) + 5*Power(t2,3))))) + 
         Power(-1 + t1,2)*(Power(s1,4)*s2*
             (2*Power(s2,3) + Power(s2,2)*(7 - 3*t1) + 2*(-1 + t1)*t1 + 
               s2*(6 - 5*t1 + Power(t1,2))) - 
            2*Power(s2,5)*Power(t2,2)*(1 - t1 + 2*t2) + 
            Power(s2,4)*t2*(-4 + 11*Power(t2,2) - 2*Power(t2,3) - 
               4*Power(t1,2)*(1 + t2) + t1*(8 + 4*t2 + 9*Power(t2,2))) - 
            (-1 + t1)*(-3 + 8*Power(t1,4) + 39*t2 + 5*Power(t2,2) - 
               Power(t2,3) - 5*Power(t1,3)*(9 + 2*t2) + 
               Power(t1,2)*(63 + 57*t2 + Power(t2,2)) + 
               t1*(-23 - 86*t2 - 8*Power(t2,2) + 3*Power(t2,3))) + 
            s2*(-1 + t1)*(-31 + Power(t1,4) - 37*t2 - 70*Power(t2,2) - 
               19*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,3)*(14 + 11*t2) - 
               Power(t1,2)*(62 + 99*t2 + 20*Power(t2,2)) + 
               t1*(78 + 125*t2 + 98*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s2,3)*(-2 + 12*t2 + 54*Power(t2,2) + 31*Power(t2,3) + 
               2*Power(t2,4) + Power(t1,3)*(2 + 8*t2 + Power(t2,2)) - 
               2*Power(t1,2)*
                (3 + 2*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(6 - 16*t2 - 49*Power(t2,2) - 15*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s2,2)*(8 + 76*t2 + 74*Power(t2,2) + 31*Power(t2,3) + 
               11*Power(t2,4) - 2*Power(t1,4)*(2 + t2) + 
               Power(t1,3)*(4 - 16*t2 + Power(t2,2)) + 
               2*Power(t1,2)*
                (6 + 57*t2 + 37*Power(t2,2) + 4*Power(t2,3)) - 
               t1*(20 + 172*t2 + 149*Power(t2,2) + 51*Power(t2,3) + 
                  5*Power(t2,4))) + 
            Power(s1,3)*(4*Power(s2,5) + 
               2*t1*(1 - 3*t1 + 2*Power(t1,2)) - 
               Power(s2,4)*(13 + 7*t1 + 4*t2) + 
               s2*(-1 + t1)*(12 - 13*t1 + 8*Power(t1,2) - 2*t2 - 
                  7*t1*t2) + 
               Power(s2,3)*(-48 + Power(t1,2) - 23*t2 + 
                  7*t1*(5 + t2)) - 
               Power(s2,2)*(-9*t1*(7 + 2*t2) + 
                  Power(t1,2)*(19 + 2*t2) + 4*(8 + 7*t2))) + 
            Power(s1,2)*(2*Power(s2,5)*(-1 + t1 - 6*t2) + 
               (-1 + t1)*(6*Power(t1,3) - t1*(-14 + t2) + 
                  2*(-3 + t2) - Power(t1,2)*(12 + 7*t2)) + 
               Power(s2,4)*(-4*Power(t1,2) + 37*t2 + t1*(4 + 23*t2)) + 
               s2*(-1 + t1)*(-43 + 2*Power(t1,3) - 41*t2 + 
                  7*Power(t2,2) - Power(t1,2)*(31 + 12*t2) + 
                  t1*(80 + 26*t2 + 8*Power(t2,2))) - 
               Power(s2,3)*(Power(t1,2)*(1 + 4*t2) + 
                  t1*(56 + 89*t2 + 3*Power(t2,2)) - 
                  3*(19 + 43*t2 + 9*Power(t2,2))) + 
               Power(s2,2)*(87 + 2*Power(t1,3) + 91*t2 + 
                  49*Power(t2,2) + 
                  Power(t1,2)*(85 + 42*t2 + Power(t2,2)) - 
                  t1*(174 + 169*t2 + 26*Power(t2,2)))) + 
            s1*(4*Power(s2,5)*t2*(1 - t1 + 3*t2) + 
               (-1 + t1)*(18 + 2*Power(t1,4) + 13*t2 - 3*Power(t2,2) - 
                  Power(t1,3)*(15 + 4*t2) + 
                  3*t1*(-15 - 8*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*(40 + 11*t2 + 3*Power(t2,2))) + 
               Power(s2,4)*(4 - 35*Power(t2,2) + 4*Power(t2,3) + 
                  Power(t1,2)*(4 + 8*t2) - 
                  t1*(8 + 8*t2 + 25*Power(t2,2))) + 
               Power(s2,3)*(-12 + Power(t1,3)*(-8 + t2) - 113*t2 - 
                  112*Power(t2,2) - 13*Power(t2,3) + 
                  Power(t1,2)*(4 + t2 + 7*Power(t2,2)) + 
                  t1*(16 + 111*t2 + 69*Power(t2,2) - 3*Power(t2,3))) + 
               s2*(-1 + t1)*(38 + Power(t1,4) + 107*t2 + 
                  48*Power(t2,2) - 8*Power(t2,3) - 
                  Power(t1,3)*(7 + 2*t2) + 
                  Power(t1,2)*(89 + 45*t2 + 4*Power(t2,2)) - 
                  t1*(121 + 166*t2 + 19*Power(t2,2) + 3*Power(t2,3))) - 
               Power(s2,2)*(75 + 162*t2 + 90*Power(t2,2) + 
                  38*Power(t2,3) + Power(t1,4)*(1 + t2) - 
                  Power(t1,3)*(24 + t2) + 
                  Power(t1,2)*(120 + 165*t2 + 31*Power(t2,2)) - 
                  t1*(172 + 327*t2 + 157*Power(t2,2) + 18*Power(t2,3))))) \
+ Power(s,2)*(Power(s1,4)*s2*(-1 + t1)*
             (Power(s2,2) - s2*(-2 + t1) + 6*t1) - 
            6*Power(s2,5)*(-1 + t1)*(-2 + 2*t1 - t2)*t2 + 
            Power(s2,4)*(-1 + t1)*
             (4 + 29*t2 + 58*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,2)*(4 + 27*t2) - 2*t1*(4 + 28*t2 + 3*Power(t2,2))\
) - (-1 + t1)*(-10 + 4*Power(t1,5) + Power(t1,4)*(77 - 25*t2) + 301*t2 - 
               23*Power(t2,2) - 10*Power(t2,3) + 
               Power(t1,3)*(-389 - 38*t2 + 31*Power(t2,2)) + 
               Power(t1,2)*(521 + 364*t2 - 55*Power(t2,2) - 
                  10*Power(t2,3)) + 
               t1*(-203 - 602*t2 + 41*Power(t2,2) + 26*Power(t2,3))) - 
            Power(s2,3)*(17 + Power(t1,5) + 79*t2 - 14*Power(t2,2) - 
               34*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,4)*(13 + 25*t2) - 
               Power(t1,3)*(62 + 100*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(98 + 204*t2 + 78*Power(t2,2) - 
                  9*Power(t2,3)) - 
               t1*(67 + 208*t2 + 61*Power(t2,2) - 51*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s2,2)*(-87 + 110*t2 + 187*Power(t2,2) + 
               16*Power(t2,3) - 3*Power(t2,4) + 
               Power(t1,5)*(23 + 5*t2) + 
               Power(t1,4)*(-119 - 15*t2 + Power(t2,2)) + 
               Power(t1,3)*(306 + 67*t2 - 4*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(t1,2)*(-434 + t2 + 198*Power(t2,2) + 
                  9*Power(t2,3) - 4*Power(t2,4)) + 
               t1*(311 - 168*t2 - 382*Power(t2,2) - 15*Power(t2,3) + 
                  7*Power(t2,4))) - 
            s2*(-1 + t1)*(187 + 3*Power(t1,5) + 87*t2 + 
               242*Power(t2,2) + 39*Power(t2,3) - 18*Power(t2,4) + 
               Power(t1,4)*(-22 + 6*t2) + 
               Power(t1,3)*(37 - 69*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(167 + 357*t2 + 105*Power(t2,2) - 
                  28*Power(t2,3)) + 
               t1*(-372 - 381*t2 - 309*Power(t2,2) + 4*Power(t2,3) + 
                  9*Power(t2,4))) + 
            Power(s1,3)*(-3*Power(s2,4)*(-1 + t1) + 
               6*t1*(2 - 5*t1 + 3*Power(t1,2)) + 
               Power(s2,3)*(-26 + 9*Power(t1,2) + t1*(25 - 7*t2) + 
                  7*t2) + Power(s2,2)*
                (-23 - 7*Power(t1,3) + t1*(22 - 28*t2) + 15*t2 + 
                  13*Power(t1,2)*t2) + 
               s2*(-1 + t1)*(14 + 9*Power(t1,2) - 12*t2 - 
                  5*t1*(2 + 3*t2))) + 
            Power(s1,2)*(6*Power(s2,5)*(-1 + t1) + 
               Power(s2,4)*(-1 + t1)*(40 + 12*t1 + 3*t2) + 
               2*(-1 + t1)*(5*Power(t1,3) + t1*(35 - 11*t2) + 
                  2*(-6 + 5*t2) - Power(t1,2)*(25 + 8*t2)) - 
               s2*(-1 + t1)*(120 + 61*Power(t1,3) + 
                  Power(t1,2)*(11 - 58*t2) + 31*t2 - 42*Power(t2,2) + 
                  t1*(-144 + 68*t2 - 3*Power(t2,2))) + 
               Power(s2,3)*(-33 - 20*Power(t1,3) + 98*t2 - 
                  15*Power(t2,2) + Power(t1,2)*(-79 + 3*t2) + 
                  t1*(132 - 125*t2 + 15*Power(t2,2))) + 
               Power(s2,2)*(133 + 9*Power(t1,4) + 46*t2 - 
                  27*Power(t2,2) + 4*Power(t1,3)*(22 + t2) + 
                  Power(t1,2)*(-64 + 9*t2 - 27*Power(t2,2)) + 
                  t1*(-166 - 35*t2 + 54*Power(t2,2)))) + 
            s1*(12*Power(s2,5)*(-1 + t1)*(-1 + t1 - t2) - 
               Power(s2,4)*(-1 + t1)*
                (17 + 15*Power(t1,2) + 98*t2 - 3*Power(t2,2) + 
                  t1*(-32 + 6*t2)) - 
               2*(-1 + t1)*(-62 + 12*Power(t1,4) + 
                  Power(t1,3)*(26 - 23*t2) - 8*t2 + 15*Power(t2,2) + 
                  t1*(130 + 17*t2 - 30*Power(t2,2)) + 
                  2*Power(t1,2)*(-53 + 10*t2 + 3*Power(t2,2))) + 
               Power(s2,3)*(57 + 11*Power(t1,4) + 7*t2 - 
                  106*Power(t2,2) + 13*Power(t2,3) + 
                  Power(t1,3)*(-36 + 29*t2) + 
                  Power(t1,2)*(96 + 121*t2 - 21*Power(t2,2)) - 
                  t1*(128 + 157*t2 - 151*Power(t2,2) + 13*Power(t2,3))) + 
               s2*(-1 + t1)*(123 + 20*Power(t1,4) + 315*t2 + 
                  56*Power(t2,2) - 48*Power(t2,3) + 
                  Power(t1,3)*(53 + 50*t2) + 
                  Power(t1,2)*(107 + 111*t2 - 95*Power(t2,2)) + 
                  t1*(-303 - 380*t2 + 82*Power(t2,2) + 15*Power(t2,3))) - 
               Power(s2,2)*(43 + 350*t2 + 39*Power(t2,2) - 
                  17*Power(t2,3) + 6*Power(t1,4)*(11 + 4*t2) + 
                  Power(t1,3)*(-79 + 12*t2 - 5*Power(t2,2)) + 
                  Power(t1,2)*
                   (3 + 266*t2 + 18*Power(t2,2) - 19*Power(t2,3)) + 
                  t1*(-33 - 652*t2 - 28*Power(t2,2) + 36*Power(t2,3))))))*
       T3q(s2,1 - s + s2 - t1))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*Power(-1 + s + t1,2)*
       (-1 + s1 + t1 - t2)*Power(1 - s + s*s2 - s1*s2 - t1 + s2*t2,2)) - 
    (8*(-29 + 13*s2 + 15*Power(s2,2) - Power(s2,3) - 8*Power(s2,4) + 
         2*Power(s2,5) + 35*t1 + 10*s2*t1 - 75*Power(s2,2)*t1 + 
         30*Power(s2,3)*t1 + 12*Power(s2,4)*t1 - 4*Power(s2,5)*t1 + 
         25*Power(t1,2) - 42*s2*Power(t1,2) + 
         95*Power(s2,2)*Power(t1,2) - 56*Power(s2,3)*Power(t1,2) + 
         2*Power(s2,5)*Power(t1,2) - 39*Power(t1,3) + 2*s2*Power(t1,3) - 
         25*Power(s2,2)*Power(t1,3) + 26*Power(s2,3)*Power(t1,3) - 
         4*Power(s2,4)*Power(t1,3) + 8*Power(t1,4) + 17*s2*Power(t1,4) - 
         10*Power(s2,2)*Power(t1,4) + Power(s2,3)*Power(t1,4) + 
         Power(s1,4)*s2*(Power(s2,4) - Power(s2,3)*(-6 + t1) + 2*t1 + 
            s2*(10 + t1) + Power(s2,2)*(-25 + 6*t1)) + 
         2*Power(s,4)*Power(-1 + s2,3)*(t1 - t2) + 13*t2 - 67*s2*t2 + 
         55*Power(s2,2)*t2 + 15*Power(s2,3)*t2 - 4*Power(s2,4)*t2 - 
         16*Power(s2,5)*t2 + 4*Power(s2,6)*t2 - 48*t1*t2 + 37*s2*t1*t2 - 
         38*Power(s2,2)*t1*t2 - 75*Power(s2,3)*t1*t2 + 
         56*Power(s2,4)*t1*t2 + 8*Power(s2,5)*t1*t2 - 
         4*Power(s2,6)*t1*t2 + 43*Power(t1,2)*t2 + 71*s2*Power(t1,2)*t2 + 
         15*Power(s2,2)*Power(t1,2)*t2 + 41*Power(s2,3)*Power(t1,2)*t2 - 
         50*Power(s2,4)*Power(t1,2)*t2 + 8*Power(s2,5)*Power(t1,2)*t2 - 
         8*Power(t1,3)*t2 - 41*s2*Power(t1,3)*t2 - 
         32*Power(s2,2)*Power(t1,3)*t2 + 19*Power(s2,3)*Power(t1,3)*t2 - 
         2*Power(s2,4)*Power(t1,3)*t2 + 7*Power(t2,2) + 
         34*s2*Power(t2,2) - 49*Power(s2,2)*Power(t2,2) + 
         72*Power(s2,3)*Power(t2,2) - 18*Power(s2,4)*Power(t2,2) - 
         8*Power(s2,6)*Power(t2,2) + 2*Power(s2,7)*Power(t2,2) - 
         10*t1*Power(t2,2) - 70*s2*t1*Power(t2,2) - 
         29*Power(s2,2)*t1*Power(t2,2) - 43*Power(s2,3)*t1*Power(t2,2) - 
         3*Power(s2,4)*t1*Power(t2,2) + 23*Power(s2,5)*t1*Power(t2,2) - 
         4*Power(s2,6)*t1*Power(t2,2) + 5*Power(t1,2)*Power(t2,2) + 
         16*s2*Power(t1,2)*Power(t2,2) + 
         70*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         13*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         9*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         Power(s2,5)*Power(t1,2)*Power(t2,2) + Power(t2,3) + 
         3*s2*Power(t2,3) + 40*Power(s2,2)*Power(t2,3) - 
         12*Power(s2,3)*Power(t2,3) + 28*Power(s2,4)*Power(t2,3) - 
         13*Power(s2,5)*Power(t2,3) + Power(s2,6)*Power(t2,3) - 
         3*t1*Power(t2,3) - 21*Power(s2,2)*t1*Power(t2,3) - 
         42*Power(s2,3)*t1*Power(t2,3) + 2*Power(s2,4)*t1*Power(t2,3) + 
         3*s2*Power(t2,4) - 5*Power(s2,2)*Power(t2,4) + 
         13*Power(s2,3)*Power(t2,4) + 5*Power(s2,4)*Power(t2,4) + 
         Power(s1,3)*(Power(s2,6) + 2*t1*(-1 + 2*t1) + 
            Power(s2,5)*(2 - 3*t1 - 3*t2) + 
            Power(s2,3)*(24 + 71*t2 - 11*t1*(2 + t2)) + 
            s2*(8*Power(t1,2) + t1*(11 - 7*t2) - 2*(10 + t2)) + 
            Power(s2,2)*(54 + 4*Power(t1,2) - 34*t2 - t1*(43 + 2*t2)) + 
            Power(s2,4)*(-29 - 32*t2 + t1*(27 + 4*t2))) + 
         Power(s,3)*Power(-1 + s2,2)*
          (14 - 3*t1 - 10*Power(t1,2) + 5*t2 + 11*t1*t2 - Power(t2,2) + 
            Power(s2,2)*(t1*(-6 + t2) - (-2 + t2)*t2) + 
            s2*(-14 - 21*t2 - 8*Power(t2,2) + t1*(23 + 8*t2)) + 
            s1*(-4*(3 + t1) + Power(s2,2)*(2 + t1 + t2) + 
               s2*(10 - 11*t1 + 13*t2))) + 
         Power(s1,2)*(2*Power(s2,7) + 10*Power(t1,3) + 2*(5 + t2) - 
            t1*(12 + t2) - Power(s2,6)*(8 + 4*t1 + t2) - 
            Power(t1,2)*(6 + 7*t2) + 
            Power(s2,5)*(-3 - 19*t2 + 3*Power(t2,2) + t1*(27 + 8*t2)) + 
            Power(s2,4)*(11 - 6*Power(t1,2) + 94*t2 + 51*Power(t2,2) - 
               5*t1*(7 + 12*t2 + Power(t2,2))) + 
            Power(s2,2)*(-95 - 8*Power(t1,3) - 79*t2 + 33*Power(t2,2) + 
               Power(t1,2)*(-36 + 5*t2) + t1*(131 + 63*t2 + Power(t2,2))\
) + s2*(-33 + 4*Power(t1,3) + 55*t2 + 7*Power(t2,2) - 
               9*Power(t1,2)*(1 + 2*t2) + 
               2*t1*(9 - 16*t2 + 4*Power(t2,2))) + 
            Power(s2,3)*(68 + 2*Power(t1,3) - 52*t2 - 54*Power(t2,2) + 
               Power(t1,2)*(49 + 4*t2) + t1*(-77 - 10*t2 + 4*Power(t2,2))\
)) + s1*(4 + 6*Power(t1,4) + Power(t1,3)*(1 - 10*t2) - 21*t2 - 
            4*Power(s2,7)*t2 - 3*Power(t2,2) + 
            3*Power(t1,2)*(-10 - t2 + Power(t2,2)) + 
            t1*(19 + 30*t2 + 6*Power(t2,2)) + 
            Power(s2,6)*(-4 + 16*t2 - Power(t2,2) + t1*(4 + 8*t2)) + 
            Power(s2,5)*(16 + Power(t1,2)*(-8 + t2) + 5*t2 + 
               30*Power(t2,2) - Power(t2,3) - 
               t1*(8 + 54*t2 + 5*Power(t2,2))) + 
            Power(s2,2)*(-53 + 151*t2 - 15*Power(t2,2) - 
               4*Power(t2,3) + Power(t1,3)*(38 + 9*t2) + 
               t1*(40 - 115*t2 + Power(t2,2)) - 
               Power(t1,2)*(25 + 29*t2 + 9*Power(t2,2))) + 
            s2*(94 - 7*Power(t1,4) - 5*t2 - 38*Power(t2,2) - 
               8*Power(t2,3) + Power(t1,3)*(3 + 2*t2) + 
               Power(t1,2)*(53 - 23*t2 + 10*Power(t2,2)) + 
               t1*(-143 + 66*t2 + 21*Power(t2,2) - 3*Power(t2,3))) + 
            Power(s2,3)*(-28 - 9*Power(t1,3) + Power(t1,4) - 146*t2 + 
               40*Power(t2,2) - 5*Power(t2,3) - 
               Power(t1,2)*(77 + 72*t2 + 4*Power(t2,2)) + 
               t1*(113 + 134*t2 + 74*Power(t2,2) + Power(t2,3))) + 
            Power(s2,4)*(3 + 4*t2 - 93*Power(t2,2) - 30*Power(t2,3) - 
               Power(t1,3)*(1 + t2) + Power(t1,2)*(55 + 14*t2) + 
               t1*(-57 + 43*t2 + 31*Power(t2,2) + 2*Power(t2,3)))) + 
         Power(s,2)*(-1 + s2)*
          (41 - 29*t1 - 8*Power(t1,2) + 12*Power(t1,3) + 6*t2 + 
            8*t1*t2 - 13*Power(t1,2)*t2 - 6*Power(t2,2) + 
            2*t1*Power(t2,2) - Power(t2,3) - 
            Power(s2,4)*(2 + Power(t1,2) + t1*(-7 + t2) - 5*t2 - 
               3*Power(t2,2)) + 
            Power(s1,2)*(Power(s2,4) + 
               Power(s2,2)*(-45 + 23*t1 - 21*t2) - 2*(4 + 2*t1 + t2) - 
               Power(s2,3)*(3*t1 + 2*t2) + s2*(52 + 4*t1 + 5*t2)) + 
            Power(s2,3)*(24 + 22*t2 + 21*Power(t2,2) + 
               2*Power(t1,2)*(2 + t2) - 2*t1*(20 + 18*t2 + Power(t2,2))) \
+ s2*(-28 - 49*Power(t1,2) - 8*Power(t1,3) + 5*t2 + 6*Power(t2,2) + 
               6*Power(t2,3) + t1*(49 + 46*t2 + 2*Power(t2,2))) - 
            Power(s2,2)*(35 + Power(t1,2)*(-30 + t2) + 58*t2 + 
               48*Power(t2,2) + 9*Power(t2,3) - 
               t1*(33 + 31*t2 + 10*Power(t2,2))) + 
            s1*(-28 + 5*t1 + 2*Power(t1,2) + 16*t2 + 4*t1*t2 + 
               3*Power(t2,2) - 4*Power(s2,4)*(1 + t1 + t2) + 
               s2*(-34 + 23*Power(t1,2) + t1*(8 - 25*t2) - 51*t2 - 
                  11*Power(t2,2)) + 
               Power(s2,3)*(-14 - 23*t2 + 2*Power(t2,2) + 
                  3*t1*(14 + t2)) - 
               Power(s2,2)*(Power(t1,2) + t1*(91 + 30*t2) - 
                  2*(40 + 51*t2 + 15*Power(t2,2))))) + 
         s*(56 - 41*t1 - 24*Power(t1,2) + 15*Power(t1,3) - 
            6*Power(t1,4) - 16*t2 + 40*t1*t2 - 12*Power(t1,2)*t2 + 
            10*Power(t1,3)*t2 + Power(s2,6)*(-7 + t1 - 4*t2)*t2 - 
            12*Power(t2,2) + 5*t1*Power(t2,2) - 
            7*Power(t1,2)*Power(t2,2) - 2*Power(t2,3) + 
            3*t1*Power(t2,3) + 
            Power(s2,4)*(-3 + 26*t2 + 67*Power(t2,2) + 27*Power(t2,3) + 
               Power(t1,3)*(4 + t2) + t1*(30 - 95*t2 - 33*Power(t2,2)) - 
               Power(t1,2)*(31 + 10*t2 + Power(t2,2))) - 
            Power(s2,5)*(3 - 4*t2 + 3*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(1 + 4*t2) - t1*(4 + 34*t2 + 7*Power(t2,2))) + 
            s2*(-61 + 7*Power(t1,4) + Power(t1,2)*(18 - 55*t2) - 
               5*Power(t1,3)*(-7 + t2) + 93*t2 - 8*Power(t2,2) + 
               11*Power(t2,3) - 3*Power(t2,4) + 
               t1*(1 - 39*t2 - 3*Power(t2,2) + Power(t2,3))) + 
            Power(s2,3)*(38 - Power(t1,4) + 27*t2 - 85*Power(t2,2) - 
               18*Power(t2,3) - 3*Power(t2,4) + Power(t1,3)*(3 + t2) + 
               Power(t1,2)*(103 + 87*t2) + 
               t1*(-143 + t2 - 54*Power(t2,2) + 3*Power(t2,3))) - 
            Power(s2,2)*(27 + 135*t2 - 29*Power(t2,2) + 25*Power(t2,3) - 
               6*Power(t2,4) + 7*Power(t1,3)*(7 + t2) + 
               Power(t1,2)*(81 + 30*t2 - 8*Power(t2,2)) + 
               t1*(-157 - 90*t2 - 102*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s1,3)*(-2*Power(s2,5) + 2*t1 + 
               5*Power(s2,2)*(-15 + t1 - t2) + 
               Power(s2,4)*(-7 + 3*t1 + t2) + 
               Power(s2,3)*(66 - 21*t1 + 10*t2) + s2*(3*t1 + 2*(9 + t2))) \
+ Power(s1,2)*(-2*Power(s2,6) + 8*Power(t1,2) + t1*(2 + t2) - 
               2*(9 + 2*t2) + Power(s2,5)*(5 + 7*t1 + 5*t2) - 
               s2*(-93 + 7*Power(t1,2) + 37*t2 + 7*Power(t2,2) + 
                  7*t1*(7 + t2)) + 
               Power(s2,4)*(43 + 51*t2 - 2*Power(t2,2) - 
                  8*t1*(8 + t2)) + 
               Power(s2,2)*(-1 - 18*Power(t1,2) + 141*t2 + 
                  16*Power(t2,2) + t1*(12 + 7*t2)) + 
               Power(s2,3)*(-120 + Power(t1,2) - 180*t2 - 
                  23*Power(t2,2) + t1*(116 + 39*t2))) + 
            s1*(-18 + 2*Power(t1,3) - 2*Power(t1,2)*(-1 + t2) + 37*t2 + 
               6*Power(t2,2) + 3*Power(s2,6)*(1 + t1 + 2*t2) - 
               t1*(8 + 13*t2 + 6*Power(t2,2)) + 
               Power(s2,5)*(5 + Power(t1,2) - 4*t2 - 2*Power(t2,2) - 
                  4*t1*(10 + 3*t2)) + 
               s2*(-121 - 24*Power(t1,3) - 83*t2 + 8*Power(t2,2) + 
                  8*Power(t2,3) + 3*Power(t1,2)*(-5 + 9*t2) + 
                  t1*(166 + 30*t2 + 3*Power(t2,2))) + 
               Power(s2,2)*(188 + 125*Power(t1,2) + 16*Power(t1,3) - 
                  30*t2 - 41*Power(t2,2) - 17*Power(t2,3) - 
                  t1*(247 + 102*t2 + 5*Power(t2,2))) + 
               Power(s2,4)*(-43 + Power(t1,2)*(7 - 2*t2) - 113*t2 - 
                  71*Power(t2,2) + Power(t2,3) + 
                  t1*(114 + 103*t2 + 5*Power(t2,2))) + 
               Power(s2,3)*(-14 - 2*Power(t1,3) + 
                  Power(t1,2)*(-88 + t2) + 211*t2 + 132*Power(t2,2) + 
                  16*Power(t2,3) - t1*(12 + 70*t2 + 21*Power(t2,2))))))*
       T4q(-1 + s2))/
     (Power(-1 + s2,2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s1 + t1 - t2)*
       Power(1 - s + s*s2 - s1*s2 - t1 + s2*t2,2)) + 
    (8*(11 + 15*s2 + 4*Power(s2,2) - 2*Power(s2,3) - 48*t1 - 57*s2*t1 - 
         8*Power(s2,2)*t1 + 6*Power(s2,3)*t1 + 82*Power(t1,2) + 
         80*s2*Power(t1,2) - 6*Power(s2,3)*Power(t1,2) - 68*Power(t1,3) - 
         48*s2*Power(t1,3) + 8*Power(s2,2)*Power(t1,3) + 
         2*Power(s2,3)*Power(t1,3) + 27*Power(t1,4) + 9*s2*Power(t1,4) - 
         4*Power(s2,2)*Power(t1,4) - 4*Power(t1,5) + s2*Power(t1,5) + 
         Power(s1,5)*s2*(Power(s2,2) - s2*(-2 + t1) + 2*t1) - 
         2*Power(s,4)*(-1 + s2)*(t1 - t2)*(-2 + s1 + s2 + t1 - t2) + 
         20*t2 + 32*s2*t2 + 40*Power(s2,2)*t2 + 6*Power(s2,3)*t2 - 
         4*Power(s2,4)*t2 - 34*t1*t2 - 124*s2*t1*t2 - 
         96*Power(s2,2)*t1*t2 - 4*Power(s2,3)*t1*t2 + 
         8*Power(s2,4)*t1*t2 + 4*Power(t1,2)*t2 + 158*s2*Power(t1,2)*t2 + 
         70*Power(s2,2)*Power(t1,2)*t2 - 10*Power(s2,3)*Power(t1,2)*t2 - 
         4*Power(s2,4)*Power(t1,2)*t2 + 14*Power(t1,3)*t2 - 
         72*s2*Power(t1,3)*t2 - 12*Power(s2,2)*Power(t1,3)*t2 + 
         8*Power(s2,3)*Power(t1,3)*t2 - 4*Power(t1,4)*t2 + 
         6*s2*Power(t1,4)*t2 - 2*Power(s2,2)*Power(t1,4)*t2 + 
         2*Power(t2,2) + 41*s2*Power(t2,2) + 26*Power(s2,2)*Power(t2,2) + 
         30*Power(s2,3)*Power(t2,2) - 2*Power(s2,5)*Power(t2,2) + 
         21*t1*Power(t2,2) - 47*s2*t1*Power(t2,2) - 
         85*Power(s2,2)*t1*Power(t2,2) - 29*Power(s2,3)*t1*Power(t2,2) + 
         4*Power(s2,4)*t1*Power(t2,2) + 2*Power(s2,5)*t1*Power(t2,2) - 
         36*Power(t1,2)*Power(t2,2) - s2*Power(t1,2)*Power(t2,2) + 
         60*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         2*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         4*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         13*Power(t1,3)*Power(t2,2) + 7*s2*Power(t1,3)*Power(t2,2) - 
         Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         Power(s2,3)*Power(t1,3)*Power(t2,2) - 8*Power(t2,3) + 
         3*s2*Power(t2,3) + 23*Power(s2,2)*Power(t2,3) + 
         Power(s2,3)*Power(t2,3) + 7*Power(s2,4)*Power(t2,3) - 
         2*Power(s2,5)*Power(t2,3) + 14*t1*Power(t2,3) + 
         33*s2*t1*Power(t2,3) - 14*Power(s2,2)*t1*Power(t2,3) - 
         14*Power(s2,3)*t1*Power(t2,3) + 5*Power(s2,4)*t1*Power(t2,3) - 
         8*Power(t1,2)*Power(t2,3) - 20*s2*Power(t1,2)*Power(t2,3) - 
         3*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         Power(s2,3)*Power(t1,2)*Power(t2,3) - Power(t2,4) - 
         8*s2*Power(t2,4) - 4*Power(s2,2)*Power(t2,4) + 
         3*Power(s2,3)*Power(t2,4) - Power(s2,4)*Power(t2,4) + 
         3*t1*Power(t2,4) + 9*s2*t1*Power(t2,4) + 
         7*Power(s2,2)*t1*Power(t2,4) - 3*s2*Power(t2,5) - 
         Power(s2,2)*Power(t2,5) + 
         Power(s1,4)*(Power(s2,4) + 2*t1*(-1 + 2*t1) + 
            Power(s2,3)*(3 - 2*t1 - 4*t2) + 
            s2*(t1 + 6*Power(t1,2) - 9*t1*t2 - 2*(2 + t2)) - 
            Power(s2,2)*(Power(t1,2) + 10*(1 + t2) - t1*(12 + 5*t2))) + 
         Power(s1,3)*(2*Power(s2,5) + 8*Power(t1,3) + t1*t2 + 
            2*(1 + t2) - Power(s2,4)*(9 + 3*t1 + 2*t2) - 
            Power(t1,2)*(8 + 11*t2) + 
            Power(s2,3)*(-9 - 3*Power(t1,2) - 14*t2 + 6*Power(t2,2) + 
               t1*(26 + 8*t2)) + 
            Power(s2,2)*(-19 + 39*t2 + 19*Power(t2,2) + 
               Power(t1,2)*(3 + 4*t2) + t1*(10 - 49*t2 - 9*Power(t2,2))) \
+ s2*(17 + 6*Power(t1,3) + 21*t2 + 9*Power(t2,2) - 
               3*Power(t1,2)*(-4 + 5*t2) + 
               t1*(-51 - 16*t2 + 15*Power(t2,2)))) + 
         Power(s1,2)*(-8 + 6*Power(t1,4) + 
            2*Power(s2,5)*(-1 + t1 - 3*t2) - 11*t2 - 5*Power(t2,2) - 
            Power(t1,3)*(1 + 13*t2) + t1*(39 + 15*t2 + 7*Power(t2,2)) + 
            Power(t1,2)*(-36 + 3*t2 + 10*Power(t2,2)) + 
            Power(s2,4)*(-4*Power(t1,2) + 25*t2 + t1*(4 + 11*t2)) + 
            Power(s2,3)*(33 + 23*t2 + 22*Power(t2,2) - 4*Power(t2,3) + 
               Power(t1,2)*(3 + 9*t2) - 2*t1*(18 + 37*t2 + 5*Power(t2,2))\
) + s2*(29 + 3*Power(t1,4) - 39*t2 - 8*Power(t1,3)*t2 - 38*Power(t2,2) - 
               15*Power(t2,3) + 
               4*Power(t1,2)*(-2 - 15*t2 + 3*Power(t2,2)) + 
               t1*(-24 + 155*t2 + 38*Power(t2,2) - 11*Power(t2,3))) - 
            Power(s2,2)*(-26 - 58*t2 + 52*Power(t2,2) + 17*Power(t2,3) + 
               Power(t1,3)*(3 + t2) + 
               Power(t1,2)*(-64 + 10*t2 + 5*Power(t2,2)) + 
               t1*(87 + 29*t2 - 69*Power(t2,2) - 7*Power(t2,3)))) + 
         s1*(-13 + 2*Power(t1,5) + 4*t2 + 17*Power(t2,2) + 
            4*Power(t2,3) + 2*Power(s2,5)*t2*(2 - 2*t1 + 3*t2) - 
            Power(t1,4)*(5 + 4*t2) + 
            Power(t1,3)*(-6 - 16*t2 + 5*Power(t2,2)) + 
            Power(t1,2)*(6 + 72*t2 + 13*Power(t2,2) - 3*Power(t2,3)) - 
            t1*(-16 + 56*t2 + 29*Power(t2,2) + 9*Power(t2,3)) + 
            Power(s2,4)*(4 - 23*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(4 + 8*t2) - t1*(8 + 8*t2 + 13*Power(t2,2))) + 
            Power(s2,3)*(-6 + Power(t1,3)*(-8 + t2) - 65*t2 - 
               15*Power(t2,2) - 14*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(10 - 7*t2 - 5*Power(t2,2)) + 
               t1*(4 + 71*t2 + 62*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,2)*(-39 - 53*t2 - 62*Power(t2,2) + 27*Power(t2,3) + 
               7*Power(t2,4) - Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(20 + 8*t2 + Power(t2,2)) + 
               2*Power(t1,2)*
                (-38 - 65*t2 + 5*Power(t2,2) + Power(t2,3)) + 
               t1*(96 + 176*t2 + 33*Power(t2,2) - 39*Power(t2,3) - 
                  2*Power(t2,4))) + 
            s2*(-29 + Power(t1,5) - 64*t2 + 19*Power(t2,2) + 
               29*Power(t2,3) + 11*Power(t2,4) - 3*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(54 - 13*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-132 + 27*t2 + 68*Power(t2,2) - 
                  3*Power(t2,3)) + 
               t1*(109 + 53*t2 - 137*Power(t2,2) - 32*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(s,3)*(-12 + 13*t1 - 15*Power(t1,2) + 2*Power(t1,3) + 
            Power(s2,3)*(4 - 6*t2) + t2 + 17*t1*t2 - 3*Power(t1,2)*t2 - 
            2*Power(t2,2) + Power(t2,3) + 
            Power(s1,2)*(-2*t1 + s2*(-2 + 3*t1 - 5*t2) + 2*t2 + 
               Power(s2,2)*(2 + t1 + t2)) + 
            Power(s2,2)*(-18 + 32*t2 + 15*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(2 + t2) - t1*(4 + 17*t2 + 2*Power(t2,2))) - 
            s2*(-26 + 4*Power(t1,3) + 29*t2 + 13*Power(t2,2) - 
               Power(t1,2)*(13 + 8*t2) + t1*(7 + 4*Power(t2,2))) + 
            s1*(6 + 2*Power(s2,3) + 3*t1*(-3 + t2) + t2 - 3*Power(t2,2) - 
               s2*(6 + Power(t1,2) - 20*t2 + 4*t1*t2 - 5*Power(t2,2)) + 
               Power(s2,2)*(-2 + Power(t1,2) - 17*t2 - 2*Power(t2,2) + 
                  t1*(5 + t2)))) + 
         Power(s,2)*(19 - 60*t1 + 59*Power(t1,2) - 18*Power(t1,3) - 
            7*t2 + 4*Power(s2,4)*t2 - 19*t1*t2 + 5*Power(t1,2)*t2 + 
            Power(t1,3)*t2 + 6*Power(t2,2) + 20*t1*Power(t2,2) - 
            3*Power(t1,2)*Power(t2,2) - 7*Power(t2,3) + 
            3*t1*Power(t2,3) - Power(t2,4) + 
            Power(s1,3)*(Power(s2,3) - 2*t1 + 2*t2 + 
               s2*(2 + 2*t1 + t2) - Power(s2,2)*(5 + 3*t1 + 2*t2)) - 
            Power(s2,3)*(-6 + Power(t1,3) + 23*t2 + 28*Power(t2,2) + 
               3*Power(t2,3) + t1*(5 - 11*t2 - 4*Power(t2,2))) + 
            Power(s2,2)*(-14 - 35*t2 + 27*Power(t2,2) + 6*Power(t2,3) + 
               Power(t1,3)*(7 + 2*t2) - 
               2*Power(t1,2)*(4 + 19*t2 + 2*Power(t2,2)) + 
               t1*(15 + 63*t2 + 25*Power(t2,2) + 2*Power(t2,3))) - 
            s2*(9 - 70*t2 + Power(t2,2) - 5*Power(t2,3) - 
               5*Power(t2,4) + Power(t1,3)*(-9 + 7*t2) + 
               Power(t1,2)*(43 - 40*t2 - 19*Power(t2,2)) + 
               t1*(-43 + 67*t2 + 54*Power(t2,2) + 17*Power(t2,3))) - 
            Power(s1,2)*(6 + 4*Power(t1,2) + 8*t2 + 5*Power(t2,2) + 
               Power(s2,3)*(9 + 3*t1 + 5*t2) - t1*(13 + 12*t2) + 
               Power(s2,2)*(3 + 3*Power(t1,2) - 24*t2 - 4*Power(t2,2) - 
                  2*t1*(5 + 2*t2)) + 
               s2*(-24 - 13*Power(t1,2) + 4*t2 - 3*Power(t2,2) + 
                  3*t1*(7 + 8*t2))) - 
            s1*(9 + 4*Power(s2,4) + 2*Power(t1,3) - 
               11*Power(t1,2)*(-1 + t2) - 15*Power(t2,2) - 
               4*Power(t2,3) + 
               Power(s2,3)*(-10 + 5*Power(t1,2) + t1*(-7 + t2) - 37*t2 - 
                  7*Power(t2,2)) + t1*(-42 + 37*t2 + 13*Power(t2,2)) + 
               s2*(25 - 11*Power(t1,3) + 24*t2 + 3*Power(t2,2) + 
                  9*Power(t2,3) + Power(t1,2)*(11 + 41*t2) + 
                  t1*(11 - 85*t2 - 39*Power(t2,2))) + 
               Power(s2,2)*(-22 + 30*t2 + 25*Power(t2,2) + 
                  2*Power(t2,3) - Power(t1,2)*(13 + 5*t2) + 
                  3*t1*(9 + 9*t2 + Power(t2,2))))) - 
         s*(18 - 81*t1 + 125*Power(t1,2) - 77*Power(t1,3) + 
            13*Power(t1,4) + 2*Power(t1,5) + 16*t2 - 25*t1*t2 + 
            12*Power(t1,2)*t2 + 5*Power(t1,3)*t2 - 8*Power(t1,4)*t2 + 
            6*Power(t2,2) + 29*t1*Power(t2,2) - 
            26*Power(t1,2)*Power(t2,2) + 13*Power(t1,3)*Power(t2,2) - 
            14*Power(t2,3) + 10*t1*Power(t2,3) - 
            10*Power(t1,2)*Power(t2,3) - 2*Power(t2,4) + 
            3*t1*Power(t2,4) - 
            Power(s2,4)*t2*(7 + Power(t1,2) + 19*t2 + 4*Power(t2,2) - 
               t1*(8 + 5*t2)) + 
            Power(s2,3)*(-3 - 29*t2 + 23*Power(t2,2) + 10*Power(t2,3) - 
               Power(t2,4) + Power(t1,3)*(1 + 4*t2) - 
               Power(t1,2)*(5 + 25*t2 + 11*Power(t2,2)) + 
               t1*(7 + 50*t2 + 30*Power(t2,2) + 8*Power(t2,3))) - 
            Power(s2,2)*(17 - 36*t2 - 40*Power(t2,2) - 24*Power(t2,3) - 
               5*Power(t2,4) + Power(t1,4)*(4 + t2) - 
               Power(t1,3)*(21 + 15*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(47 + 43*t2 - 16*Power(t2,2) + Power(t2,3)) + 
               t1*(-47 + 7*t2 + 82*Power(t2,2) + 32*Power(t2,3))) + 
            s2*(11 + Power(t1,5) + 32*t2 + 19*Power(t2,2) + 
               23*Power(t2,3) + 4*Power(t2,4) - 3*Power(t2,5) - 
               4*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(5 - 41*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(9 + 194*t2 + 46*Power(t2,2) - 
                  13*Power(t2,3)) + 
               t1*(-22 - 181*t2 - 108*Power(t2,2) - 5*Power(t2,3) + 
                  10*Power(t2,4))) + 
            Power(s1,4)*(2*Power(s2,3) - 2*t1 - 
               Power(s2,2)*(1 + 3*t1 + t2) + s2*(5*t1 - 2*(1 + t2))) + 
            Power(s1,3)*(2 + 2*Power(s2,4) + 2*Power(t1,2) + 4*t2 + 
               t1*(4 + t2) - Power(s2,3)*(5 + 5*t1 + 7*t2) + 
               Power(s2,2)*(-22 - 3*Power(t1,2) + t2 + 3*Power(t2,2) + 
                  2*t1*(11 + 5*t2)) + 
               s2*(17 + 16*Power(t1,2) + 9*t2 + 9*Power(t2,2) - 
                  2*t1*(9 + 17*t2))) + 
            Power(s1,2)*(-16 + 8*Power(t1,3) - 17*t2 - 10*Power(t2,2) - 
               3*Power(t1,2)*(4 + 7*t2) - Power(s2,4)*(13 + t1 + 8*t2) + 
               t1*(42 + 8*t2 + 7*Power(t2,2)) + 
               Power(s2,3)*(10 - 8*Power(t1,2) + 24*t2 + 7*Power(t2,2) + 
                  2*t1*(20 + 7*t2)) + 
               Power(s2,2)*(9 + 73*t2 + 6*Power(t2,2) - 3*Power(t2,3) + 
                  Power(t1,2)*(9 + 10*t2) - 
                  t1*(42 + 86*t2 + 11*Power(t2,2))) + 
               s2*(34 + 13*Power(t1,3) + Power(t1,2)*(13 - 51*t2) - 
                  29*t2 - 8*Power(t2,2) - 15*Power(t2,3) + 
                  t1*(-94 + 55*t2 + 63*Power(t2,2)))) - 
            s1*(14 - 6*Power(t1,4) - 7*t2 - 29*Power(t2,2) - 
               8*Power(t2,3) + Power(t1,3)*(-11 + 26*t2) + 
               Power(t1,2)*(36 - 45*t2 - 29*Power(t2,2)) + 
               Power(s2,4)*(-3 + 3*Power(t1,2) - 32*t2 + 4*t1*t2 - 
                  10*Power(t2,2)) + 
               t1*(-33 + 70*t2 + 22*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,3)*(-30 + Power(t1,3) + 35*t2 + 29*Power(t2,2) + 
                  Power(t2,3) - Power(t1,2)*(20 + 17*t2) + 
                  t1*(49 + 66*t2 + 17*Power(t2,2))) + 
               Power(s2,2)*(23 - Power(t1,3)*(-5 + t2) + 56*t2 + 
                  75*Power(t2,2) + 11*Power(t2,3) - Power(t2,4) + 
                  Power(t1,2)*(-39 + 38*t2 + 6*Power(t2,2)) - 
                  t1*(-11 + 141*t2 + 96*Power(t2,2) + 4*Power(t2,3))) + 
               s2*(22 - 3*Power(t1,4) + 43*t2 + 11*Power(t2,2) + 
                  3*Power(t2,3) - 11*Power(t2,4) + 
                  2*Power(t1,3)*(-5 + 9*t2) + 
                  Power(t1,2)*(119 + 57*t2 - 48*Power(t2,2)) + 
                  2*t1*(-64 - 93*t2 + 16*Power(t2,2) + 22*Power(t2,3))))))*
       T5q(1 - s1 - t1 + t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s1 + t1 - t2)*
       Power(1 - s + s*s2 - s1*s2 - t1 + s2*t2,2)));
   return a;
};
