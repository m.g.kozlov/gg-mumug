#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m321_4_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(2*Power(s2,10)*t2*(1 + t1*(5 + 2*t2)) - 
         Power(t1,3)*(-1 + s + t1)*
          (Power(s,2)*(2 + t1 - t2) + 2*(-t1 + Power(t1,2) - t2) + 
            s*(-3 + 3*t1 - s1*t1 + 3*t2))*
          (4*Power(t1,2) + (-1 + t2)*Power(t2,2) - 
            t1*(-4 + 4*t2 + Power(t2,2))) + 
         Power(s2,9)*(2*t2*(-1 + 4*t2) + 
            2*Power(t1,2)*(-5 + (-15 + s1)*t2 - 5*Power(t2,2)) + 
            t1*(-2 + Power(s1,3) + 3*Power(s1,2)*t2 + 3*s1*(-8 + t2)*t2 - 
               8*Power(t2,2) + 21*Power(t2,3) - 4*s*t2*(1 + t2))) + 
         Power(s2,8)*(2*t2*(-3 - 4*t2 + 6*Power(t2,2)) + 
            2*Power(t1,3)*(13 + (11 + 3*s)*t2 + 6*Power(t2,2) - 
               s1*(1 + 3*t2)) + 
            t1*(2 + Power(s1,3)*(-2 + t2) + 
               (8 + 2*s - 8*Power(s,2))*t2 + 
               (94 + 34*s - 2*Power(s,2))*Power(t2,2) - 
               2*(45 + 16*s)*Power(t2,3) + 30*Power(t2,4) + 
               Power(s1,2)*(-3 + (19 - 5*s)*t2 + 4*Power(t2,2)) + 
               s1*t2*(-23 + 2*Power(s,2) + 9*s*(-2 + t2) - 95*t2 + 
                  25*Power(t2,2))) + 
            Power(t1,2)*(-4*Power(s1,3) - Power(s1,2)*(3 + 8*t2) + 
               s1*(24 + 73*t2 - 23*Power(t2,2)) + 
               s*(4 + 3*Power(s1,2) - 52*t2 - 7*s1*t2 + 22*Power(t2,2)) - 
               t2*(70 + 32*t2 + 41*Power(t2,2)))) + 
         Power(s2,7)*(4*t2*(2 - 4*t2 - 3*Power(t2,2) + 2*Power(t2,3)) + 
            2*Power(t1,4)*(-6 + t2 - 5*Power(t2,2) + 3*s1*(1 + t2) - 
               3*s*(1 + 2*t2)) - 
            t1*(-6 - 2*(-10 + 4*s + 23*Power(s,2) + 7*Power(s,3))*t2 + 
               (15 + 9*s - 9*Power(s,2) - 2*Power(s,3))*Power(t2,2) - 
               (304 + 181*s + 7*Power(s,2))*Power(t2,3) + 
               (98 + 52*s)*Power(t2,4) - 13*Power(t2,5) + 
               Power(s1,3)*(1 + 17*t2 + Power(t2,2)) + 
               Power(s1,2)*(-6 - 22*(1 + 2*s)*t2 + 
                  (-91 + 11*s)*Power(t2,2) + Power(t2,3)) + 
               s1*(-3 + (-25 + 11*s + 13*Power(s,2))*t2 + 
                  (108 + 123*s + 2*Power(s,2))*Power(t2,2) + 
                  (200 - 3*s)*Power(t2,3) - 41*Power(t2,4))) + 
            Power(t1,3)*(78 + 6*Power(s1,3) + 
               Power(s,2)*(3*s1 - 14*t2) + 245*t2 + 53*Power(t2,2) + 
               28*Power(t2,3) + 2*Power(s1,2)*(4 + 5*t2) + 
               s1*(-76 - 49*t2 + 43*Power(t2,2)) + 
               s*(56 - 9*Power(s1,2) + 115*t2 - 37*Power(t2,2) + 
                  7*s1*(1 + 2*t2))) + 
            Power(t1,2)*(-16 - 139*t2 + 2*Power(s,3)*t2 - 
               147*Power(t2,2) + 55*Power(t2,3) - 52*Power(t2,4) + 
               Power(s1,3)*(7 + t2) + 
               Power(s,2)*(8 + s1*(-2 + t2) + 48*t2 - 13*Power(t2,2)) - 
               Power(s1,2)*(7 + 85*t2 + 27*Power(t2,2)) + 
               s1*(23 + 250*t2 + 299*Power(t2,2) - 90*Power(t2,3)) + 
               s*(-2 + 68*t2 - 219*Power(t2,2) + 75*Power(t2,3) + 
                  Power(s1,2)*(-4 + 6*t2) + 
                  s1*(12 + 115*t2 - 9*Power(t2,2))))) + 
         s2*Power(t1,2)*(-104*Power(t1,6) + 
            Power(t1,5)*(96 - 16*Power(s,2) - 268*s1 + 4*Power(s1,2) + 
               4*s*(-14 + 3*s1) + 264*t2 + 26*Power(t2,2)) + 
            Power(t2,2)*(3 - 5*Power(s,3)*(-2 + t2) - 11*t2 + 
               7*Power(t2,2) + Power(t2,3) + 
               Power(s,2)*(11 - 10*t2 - 2*Power(t2,2) + Power(t2,3)) - 
               s*(15 - 32*t2 + 15*Power(t2,2) + 2*Power(t2,3))) + 
            t1*(-8 + (40 - 6*s1)*t2 + 3*(-11 + 5*s1)*Power(t2,2) + 
               (2 - 12*s1)*Power(t2,3) + (-7 + 3*s1)*Power(t2,4) - 
               2*Power(t2,5) + 
               Power(s,3)*(-40 + 24*t2 + 5*Power(t2,2) + 
                  7*Power(t2,3) - Power(t2,4)) - 
               Power(s,2)*(44 - 30*t2 + (-83 + 2*s1)*Power(t2,2) + 
                  (17 + 11*s1)*Power(t2,3) - (7 + 2*s1)*Power(t2,4) + 
                  Power(t2,5)) + 
               s*(60 - 116*t2 + (26 - 6*s1)*Power(t2,2) + 
                  (17 + 19*s1)*Power(t2,3) + (16 - 13*s1)*Power(t2,4) + 
                  3*Power(t2,5))) + 
            Power(t1,4)*(-84 + 8*Power(s,3) + 12*Power(s1,3) + 12*t2 - 
               207*Power(t2,2) - 64*Power(t2,3) - 
               Power(s1,2)*(140 - 20*t2 + Power(t2,2)) + 
               Power(s,2)*(88 - 24*s1 + 46*t2 + 4*Power(t2,2)) + 
               2*s1*(40 + 97*t2 + 31*Power(t2,2)) + 
               s*(44 + 4*Power(s1,2) + 244*t2 + 19*Power(t2,2) - 
                  s1*(268 + 14*t2 + 3*Power(t2,2)))) + 
            Power(t1,2)*(52 - 44*t2 + 14*Power(t2,2) + 15*Power(t2,3) + 
               39*Power(t2,4) - 6*Power(t2,5) - 
               2*Power(s1,3)*t2*(1 + t2) + 
               Power(s,3)*(-44 - 12*t2 - 11*Power(t2,2) + 
                  2*Power(t2,3)) + 
               4*Power(s1,2)*
                (3 - 2*t2 + 6*Power(t2,2) + 3*Power(t2,3)) - 
               s1*(40 - 34*t2 + 20*Power(t2,2) + 27*Power(t2,3) + 
                  15*Power(t2,4)) + 
               s*(52 + (8 - 58*s1 + 6*Power(s1,2))*t2 + 
                  2*(8 + 2*s1 + 15*Power(s1,2))*Power(t2,2) - 
                  (3 + 55*s1 + 10*Power(s1,2))*Power(t2,3) + 
                  4*(9 + s1)*Power(t2,4)) + 
               Power(s,2)*(-216 + 10*t2 - 6*Power(t2,2) + 
                  9*Power(t2,3) + 6*Power(t2,4) + 
                  s1*(8 + 36*t2 + 2*Power(t2,2) - 5*Power(t2,3)))) - 
            Power(t1,3)*(-48 + 16*t2 + 135*Power(t2,2) - 22*Power(t2,3) - 
               44*Power(t2,4) + Power(s,3)*(-40 + 8*t2 + Power(t2,2)) + 
               2*Power(s1,3)*(-6 - t2 + 2*Power(t2,2)) + 
               Power(s1,2)*(88 + 48*t2 - 30*Power(t2,2) + 
                  3*Power(t2,3)) + 
               s1*(-52 - 114*t2 - 35*Power(t2,2) + 45*Power(t2,3)) + 
               Power(s,2)*(88 + 2*t2 + 55*Power(t2,2) + 9*Power(t2,3) + 
                  s1*(20 - 24*t2 - 3*Power(t2,2))) + 
               s*(292 + 72*t2 + 154*Power(t2,2) + 62*Power(t2,3) - 
                  2*Power(s1,2)*(-66 + 17*t2 + Power(t2,2)) + 
                  s1*(-168 - 220*t2 - 63*Power(t2,2) + Power(t2,3))))) - 
         Power(s2,6)*(-2*t2*(1 + 9*t2 - 8*Power(t2,2) - 4*Power(t2,3) + 
               Power(t2,4)) + 
            2*Power(t1,5)*(7 - 2*Power(t2,2) - 3*s*(2 + t2) + 
               s1*(3 + t2)) + 
            t1*(9 + (-32 - 24*s + 67*Power(s,2) + 38*Power(s,3))*t2 - 
               (-28 + 21*s + 50*Power(s,2) + 29*Power(s,3))*
                Power(t2,2) + 
               (79 + 111*s - 31*Power(s,2) - 4*Power(s,3))*Power(t2,3) - 
               2*(165 + 122*s + 10*Power(s,2))*Power(t2,4) + 
               8*(1 + 3*s)*Power(t2,5) + 
               Power(s1,3)*(-2 + 28*Power(t2,2) + Power(t2,3)) + 
               Power(s1,2)*(-3 + (-2 + 39*s)*t2 - 
                  (36 + 101*s)*Power(t2,2) + (-125 + 7*s)*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s1*(6 + (18 + 4*s - 20*Power(s,2))*t2 + 
                  (-65 - 41*s + 18*Power(s,2))*Power(t2,2) + 
                  (128 + 181*s + 10*Power(s,2))*Power(t2,3) + 
                  (211 + 21*s)*Power(t2,4) - 19*Power(t2,5))) + 
            Power(t1,4)*(234 - Power(s,3) + 4*Power(s1,3) + 
               Power(s,2)*(-14 + 6*s1 - 17*t2) + 217*t2 - 
               7*Power(t2,2) + 12*Power(t2,3) + 
               2*Power(s1,2)*(5 + 4*t2) + 
               s1*(-72 + 43*t2 + 29*Power(t2,2)) + 
               s*(137 - 9*Power(s1,2) + 50*t2 - 17*Power(t2,2) + 
                  7*s1*(2 + t2))) - 
            Power(t1,3)*(-(Power(s1,3)*(13 + 5*t2)) + 
               Power(s,3)*(-2 + 8*t2) + 
               Power(s1,2)*(63 + 177*t2 + 56*Power(t2,2)) + 
               s1*(-155 - 733*t2 - 219*Power(t2,2) + 113*Power(t2,3)) + 
               3*(15 + 121*t2 + 89*Power(t2,2) + 33*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s,2)*(-49 + 29*t2 + 5*Power(t2,2) - 
                  s1*(13 + 8*t2)) + 
               s*(-102 + 256*t2 + 396*Power(t2,2) - 95*Power(t2,3) + 
                  3*Power(s1,2)*(7 + t2) - 
                  s1*(106 + 226*t2 + 25*Power(t2,2)))) + 
            Power(t1,2)*(-28 - 44*t2 + 722*Power(t2,2) + 
               630*Power(t2,3) - 104*Power(t2,4) + 21*Power(t2,5) + 
               Power(s,3)*(14 + 29*t2 - 7*Power(t2,2)) - 
               Power(s1,3)*(23 + 59*t2 + 14*Power(t2,2)) + 
               Power(s1,2)*(46 + 267*t2 + 238*Power(t2,2) + 
                  30*Power(t2,3)) + 
               s1*(37 - 224*t2 - 749*Power(t2,2) - 497*Power(t2,3) + 
                  103*Power(t2,4)) + 
               Power(s,2)*(46 + 132*t2 - 75*Power(t2,2) + 
                  36*Power(t2,3) - s1*(13 + 19*t2 + 10*Power(t2,2))) + 
               s*(5 + 85*t2 + 208*Power(t2,2) + 369*Power(t2,3) - 
                  84*Power(t2,4) + 
                  Power(s1,2)*(41 + 86*t2 - 5*Power(t2,2)) - 
                  s1*(29 + 220*t2 + 397*Power(t2,2) + 25*Power(t2,3))))) + 
         Power(s2,5)*(-2*Power(t1,6)*(-5 + 3*s + 2*t2) - 
            2*t2*(3 - 3*t2 - 8*Power(t2,2) + 4*Power(t2,3) + 
               Power(t2,4)) - 
            Power(t1,5)*(-205 + Power(s,3) + 38*t2 + 20*Power(t2,2) - 
               4*Power(t2,3) + Power(s,2)*(17 + 3*t2) + 
               s*(-87 + 3*t2 - 2*Power(t2,2))) + 
            Power(s1,3)*t1*(Power(t1,4) + Power(t1,3)*(13 + 3*t2) + 
               t2*(4 + 3*t2 - 13*Power(t2,2)) - 
               Power(t1,2)*(73 + 111*t2 + 26*Power(t2,2)) + 
               t1*(-8 + 68*t2 + 75*Power(t2,2) + 9*Power(t2,3))) + 
            Power(t1,3)*(-41 + 504*t2 + 2043*Power(t2,2) + 
               767*Power(t2,3) + 57*Power(t2,4) + 8*Power(t2,5) + 
               Power(s,3)*(27 - 31*t2 + 15*Power(t2,2)) + 
               Power(s,2)*(135 - 157*t2 + 55*Power(t2,2) + 
                  49*Power(t2,3)) + 
               s*(85 + 31*t2 + 658*Power(t2,2) + 477*Power(t2,3) - 
                  65*Power(t2,4))) - 
            Power(t1,4)*(Power(s,3)*(13 + 4*t2) + 
               Power(s,2)*(10 + 67*t2 - 8*Power(t2,2)) + 
               s*(5 + 965*t2 + 137*Power(t2,2) - 50*Power(t2,3)) + 
               2*(63 + 382*t2 + 230*Power(t2,2) + 30*Power(t2,3) + 
                  Power(t2,4))) + 
            t1*t2*(-19 + 44*t2 - 33*Power(t2,2) - 81*Power(t2,3) + 
               126*Power(t2,4) + 18*Power(t2,5) + 
               2*Power(s,3)*(17 - 42*t2 + 8*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(28 - 119*t2 - 41*Power(t2,2) + 
                  3*Power(t2,3) + 11*Power(t2,4)) + 
               s*(-8 + 8*t2 + 16*Power(t2,2) - 183*Power(t2,3) + 
                  101*Power(t2,4))) + 
            Power(t1,2)*(-12 + 95*t2 + 285*Power(t2,2) - 
               627*Power(t2,3) - 931*Power(t2,4) - 25*Power(t2,5) + 
               Power(s,3)*(38 - 31*t2 - 70*Power(t2,2) + 
                  8*Power(t2,3)) - 
               Power(s,2)*(-67 + 98*t2 + 333*Power(t2,2) + 
                  39*Power(t2,3) + 33*Power(t2,4)) + 
               s*(-33 - 158*t2 + 95*Power(t2,2) - 397*Power(t2,3) - 
                  427*Power(t2,4) + 31*Power(t2,5))) - 
            Power(s1,2)*t1*(6 + Power(t1,4)*(-8 + 3*s - 3*t2) + 4*t2 + 
               2*(8 + 39*s)*Power(t2,2) + (4 - 70*s)*Power(t2,3) + 
               (-53 + s)*Power(t2,4) + 
               Power(t1,3)*(138 + 233*t2 + 47*Power(t2,2) + 
                  s*(30 + 4*t2)) - 
               Power(t1,2)*(212 + 637*t2 + 313*Power(t2,2) + 
                  74*Power(t2,3) + 2*s*(47 + 50*t2 + 13*Power(t2,2))) + 
               t1*(20 + 146*t2 + 481*Power(t2,2) + 279*Power(t2,3) + 
                  11*Power(t2,4) - 
                  4*s*(12 - 40*t2 - 54*Power(t2,2) + Power(t2,3)))) + 
            s1*t1*(-3 + 2*Power(t1,5) - 9*(2 - 5*s + Power(s,2))*t2 + 
               (4 - 9*s + 38*Power(s,2))*Power(t2,2) + 
               (67 + 103*s + 3*Power(s,2))*Power(t2,3) - 
               (28 + 65*s + 6*Power(s,2))*Power(t2,4) - 
               (82 + 15*s)*Power(t2,5) + 
               Power(t1,4)*(7*s + 3*Power(s,2) + t2*(61 + 6*t2)) + 
               Power(t1,3)*(409 + 786*t2 - 87*Power(t2,2) - 
                  56*Power(t2,3) + Power(s,2)*(32 + 5*t2) + 
                  s*(217 + 259*t2 + 41*Power(t2,2))) - 
               Power(t1,2)*(98 + 1118*t2 + 1802*Power(t2,2) + 
                  391*Power(t2,3) - 83*Power(t2,4) + 
                  Power(s,2)*(5 + 23*t2 + 21*Power(t2,2)) + 
                  s*(151 + 880*t2 + 574*Power(t2,2) + 83*Power(t2,3))) + 
               t1*(42 - 33*t2 + 286*Power(t2,2) + 944*Power(t2,3) + 
                  499*Power(t2,4) - 38*Power(t2,5) + 
                  Power(s,2)*(-20 + 3*t2 + 70*Power(t2,2) + 
                     17*Power(t2,3)) + 
                  s*(-2 - 59*t2 + 354*Power(t2,2) + 563*Power(t2,3) + 
                     49*Power(t2,4))))) + 
         Power(s2,2)*t1*(16*Power(t1,7) + 
            2*Power(t1,6)*(209 + 10*Power(s,2) + s*(60 - 32*s1) - 
               16*s1 + 22*Power(s1,2) + 90*t2 - 2*Power(t2,2)) + 
            Power(t2,2)*(-5 + 13*t2 + 5*Power(s,3)*(-2 + t2)*t2 - 
               5*Power(t2,2) - 3*Power(t2,3) + 
               5*Power(s,2)*t2*(-1 + Power(t2,2)) + 
               s*(6 - 6*t2 - 8*Power(t2,2) + 8*Power(t2,3))) + 
            t1*(12 + 4*(-10 + 3*s1)*t2 + (1 - 30*s1)*Power(t2,2) + 
               (37 + 24*s1)*Power(t2,3) - (7 + 6*s1)*Power(t2,4) + 
               9*Power(t2,5) + 
               Power(s,2)*t2*
                (42 + (-88 + 3*s1)*t2 + (-50 + 19*s1)*Power(t2,2) - 
                  17*Power(t2,3)) + 
               Power(s,3)*t2*
                (60 - 62*t2 - 13*Power(t2,2) + 3*Power(t2,3)) + 
               s*(-24 - 18*t2 + (149 + 17*s1)*Power(t2,2) - 
                  (101 + 43*s1)*Power(t2,3) + (13 + 18*s1)*Power(t2,4) + 
                  (-25 + 8*s1)*Power(t2,5))) - 
            Power(t1,5)*(182 + 4*Power(s,3) - 36*Power(s1,3) + 1160*t2 + 
               601*Power(t2,2) + 34*Power(t2,3) + 
               Power(s,2)*(43 - 20*s1 + 32*t2 + 5*Power(t2,2)) - 
               2*s1*(399 + 379*t2 + 9*Power(t2,2)) + 
               Power(s1,2)*(120 + 82*t2 + 11*Power(t2,2)) + 
               s*(-307 + 52*Power(s1,2) + 304*t2 + 40*Power(t2,2) - 
                  s1*(303 + 106*t2 + 16*Power(t2,2)))) + 
            Power(t1,4)*(34 - 66*t2 + 830*Power(t2,2) + 
               604*Power(t2,3) + 92*Power(t2,4) - 
               Power(s,3)*(37 + 8*t2 + Power(t2,2)) - 
               Power(s1,3)*(62 + 30*t2 + 7*Power(t2,2)) + 
               4*Power(s1,2)*
                (101 + 93*t2 + 4*Power(t2,2) + 4*Power(t2,3)) - 
               s1*(162 + 756*t2 + 795*Power(t2,2) + 158*Power(t2,3)) + 
               Power(s,2)*(117 - 220*t2 - 15*Power(t2,2) + 
                  6*Power(t2,3) + s1*(53 + 4*t2 + Power(t2,2))) + 
               s*(231 - 484*t2 - 206*Power(t2,2) + 85*Power(t2,3) + 
                  Power(s1,2)*(114 + 26*t2 + 7*Power(t2,2)) + 
                  s1*(496 + 67*t2 - 122*Power(t2,2) - 20*Power(t2,3)))) + 
            Power(t1,3)*(-174 + 44*t2 + 288*Power(t2,2) + 
               220*Power(t2,3) - 232*Power(t2,4) - 60*Power(t2,5) + 
               Power(s,3)*(121 - 42*t2 + 18*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s1,3)*(-30 - 14*t2 + 15*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(s1,2)*(192 + 194*t2 - 8*Power(t2,2) - 
                  77*Power(t2,3) + 3*Power(t2,4)) + 
               s1*(-74 - 302*t2 - 239*Power(t2,2) + 80*Power(t2,3) + 
                  127*Power(t2,4)) + 
               Power(s,2)*(475 + 281*t2 + 56*Power(t2,2) + 
                  70*Power(t2,3) + 3*Power(t2,4) - 
                  s1*(-1 + 109*t2 + 25*Power(t2,2) + Power(t2,3))) + 
               s*(357 + 472*t2 + 494*Power(t2,2) + 332*Power(t2,3) + 
                  Power(t2,4) + 
                  Power(s1,2)*
                   (258 + 142*t2 - 99*Power(t2,2) - 4*Power(t2,3)) + 
                  s1*(-327 - 589*t2 - 520*Power(t2,2) + 48*Power(t2,3) + 
                     9*Power(t2,4)))) - 
            Power(t1,2)*(44 + 54*t2 - 95*Power(t2,2) + 68*Power(t2,3) + 
               74*Power(t2,4) + 29*Power(t2,5) - 6*Power(t2,6) - 
               2*Power(s1,3)*t2*(2 + 3*t2 + Power(t2,2)) + 
               Power(s,3)*(-130 - 67*t2 + 29*Power(t2,2) - 
                  3*Power(t2,3) + Power(t2,4)) + 
               Power(s1,2)*(24 - 10*t2 + 55*Power(t2,2) + 
                  36*Power(t2,3) + 15*Power(t2,4)) - 
               s1*(86 - 40*t2 + 12*Power(t2,2) + 74*Power(t2,3) + 
                  57*Power(t2,4) + 15*Power(t2,5)) + 
               Power(s,2)*(-243 - 237*t2 + 64*Power(t2,2) + 
                  44*Power(t2,3) + 19*Power(t2,4) + 4*Power(t2,5) + 
                  s1*(12 + 68*t2 + 27*Power(t2,2) - 34*Power(t2,3))) + 
               s*(175 - 86*t2 + 149*Power(t2,2) - 53*Power(t2,3) + 
                  126*Power(t2,4) + 34*Power(t2,5) + 
                  Power(s1,2)*t2*
                   (12 + 60*t2 + 19*Power(t2,2) - 13*Power(t2,3)) + 
                  s1*(20 - 140*t2 - 62*Power(t2,2) - 65*Power(t2,3) - 
                     67*Power(t2,4) + 5*Power(t2,5))))) + 
         Power(s2,3)*(2*Power(t1,7)*(-33 + 2*s + 6*s1 - 10*t2) + 
            2*Power(t2,2)*(1 - 2*t2 + Power(t2,3)) - 
            t1*(4 - 6*(1 + 3*s - s1)*t2 + 
               (-32 + 4*Power(s,2) + 20*Power(s,3) - 15*s1 + 
                  6*s*(5 + 2*s1))*Power(t2,2) + 
               (41 - 44*Power(s,3) + s*(18 - 25*s1) + 12*s1 + 
                  Power(s,2)*(-38 + 9*s1))*Power(t2,3) + 
               (-4 + 8*Power(s,3) - 3*s1 + Power(s,2)*(-27 + 2*s1) + 
                  s*(-22 + 5*s1))*Power(t2,4) + 
               (5 + 5*Power(s,2) + 2*s*(-5 + 4*s1))*Power(t2,5)) - 
            Power(t1,6)*(631 + 690*t2 + 66*Power(t2,2) - 4*Power(t2,3) + 
               Power(s,2)*(28 + 22*t2) + Power(s1,2)*(133 + 58*t2) + 
               s1*(-232 + 22*t2 + 8*Power(t2,2)) + 
               s*(499 + 110*t2 - 4*Power(t2,2) - s1*(169 + 80*t2))) + 
            Power(t1,5)*(187 + 1876*t2 + 1855*Power(t2,2) + 
               408*Power(t2,3) + 8*Power(t2,4) + 
               Power(s,3)*(1 + 8*t2 + Power(t2,2)) - 
               Power(s1,3)*(102 + 44*t2 + Power(t2,2)) + 
               s1*(-955 - 2111*t2 - 664*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s1,2)*(366 + 333*t2 + 132*Power(t2,2) + 
                  11*Power(t2,3)) + 
               Power(s,2)*(-75 + 201*t2 + 67*Power(t2,2) + 
                  5*Power(t2,3) - s1*(23 + 24*t2 + 3*Power(t2,2))) + 
               s*(-372 + 338*t2 + 661*Power(t2,2) + 19*Power(t2,3) + 
                  Power(s1,2)*(116 + 60*t2 + 3*Power(t2,2)) - 
                  s1*(727 + 693*t2 + 189*Power(t2,2) + 16*Power(t2,3)))) \
- Power(t1,4)*(-124 - 245*t2 + 883*Power(t2,2) + 1794*Power(t2,3) + 
               468*Power(t2,4) + 30*Power(t2,5) + 
               Power(s,3)*(89 - 68*t2 - Power(t2,2) + 2*Power(t2,3)) - 
               Power(s1,3)*(84 + 128*t2 + 33*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(s1,2)*(402 + 946*t2 + 438*Power(t2,2) + 
                  61*Power(t2,3) + 15*Power(t2,4)) - 
               s1*(114 + 861*t2 + 1820*Power(t2,2) + 1004*Power(t2,3) + 
                  77*Power(t2,4)) + 
               Power(s,2)*(334 + 273*t2 - 134*Power(t2,2) + 
                  59*Power(t2,3) + 10*Power(t2,4) - 
                  s1*(-30 + 27*t2 + 8*Power(t2,2) + 5*Power(t2,3))) + 
               s*(338 + 121*t2 - 241*Power(t2,2) + 365*Power(t2,3) + 
                  91*Power(t2,4) + 
                  2*Power(s1,2)*
                   (96 + 145*t2 + 14*Power(t2,2) + 5*Power(t2,3)) + 
                  s1*(254 + 95*t2 - 721*Power(t2,2) - 177*Power(t2,3) - 
                     23*Power(t2,4)))) - 
            Power(t1,2)*(7 - 101*t2 + 47*Power(t2,2) + 2*Power(t2,3) - 
               67*Power(t2,4) - 64*Power(t2,5) + 12*Power(t2,6) + 
               2*Power(s1,3)*t2*(1 + 3*t2 + 2*Power(t2,2)) + 
               Power(s,3)*(-10 + 203*t2 - 38*Power(t2,2) - 
                  49*Power(t2,3) + 6*Power(t2,4)) - 
               2*Power(s1,2)*
                (6 + 2*t2 + 19*Power(t2,2) + 18*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(s,2)*(1 - 6*(-49 + 6*s1)*t2 + 
                  (21 - 44*s1)*Power(t2,2) + 
                  5*(-23 + 9*s1)*Power(t2,3) + 
                  (-57 + 8*s1)*Power(t2,4) - 9*Power(t2,5)) + 
               s1*(52 + 22*t2 - 36*Power(t2,2) + 67*Power(t2,3) + 
                  69*Power(t2,4) + 30*Power(t2,5)) + 
               s*(-49 - 156*t2 + 96*Power(t2,2) - 72*Power(t2,3) - 
                  53*Power(t2,4) - 117*Power(t2,5) + 
                  2*Power(s1,2)*t2*
                   (-3 - 15*t2 - 34*Power(t2,2) + 13*Power(t2,3)) + 
                  s1*(-24 + 74*t2 + 136*Power(t2,2) + Power(t2,3) + 
                     114*Power(t2,4) + 6*Power(t2,5)))) + 
            Power(t1,3)*(125 + 138*t2 - 282*Power(t2,2) - 
               462*Power(t2,3) + 221*Power(t2,4) + 248*Power(t2,5) + 
               18*Power(t2,6) + 
               Power(s1,3)*(24 + 26*t2 - 15*Power(t2,2) - 
                  23*Power(t2,3)) + 
               Power(s,3)*(-117 - 221*t2 + 50*Power(t2,2) - 
                  15*Power(t2,3) + Power(t2,4)) + 
               Power(s1,2)*(-126 - 248*t2 - 90*Power(t2,2) + 
                  159*Power(t2,3) + 47*Power(t2,4)) + 
               s1*(-11 + 244*t2 + 377*Power(t2,2) + 42*Power(t2,3) - 
                  282*Power(t2,4) - 82*Power(t2,5)) - 
               Power(s,2)*(321 + 779*t2 + 252*Power(t2,2) + 
                  16*Power(t2,3) + 19*Power(t2,4) - 5*Power(t2,5) + 
                  s1*(-6 - 153*t2 - 111*Power(t2,2) + 11*Power(t2,3) + 
                     2*Power(t2,4))) - 
               s*(76 + 501*t2 + 445*Power(t2,2) + 734*Power(t2,3) + 
                  38*Power(t2,4) - 56*Power(t2,5) + 
                  Power(s1,2)*
                   (120 + 386*t2 - 114*Power(t2,2) - 78*Power(t2,3) + 
                     Power(t2,4)) + 
                  s1*(-222 - 475*t2 - 814*Power(t2,2) - 106*Power(t2,3) + 
                     123*Power(t2,4) + 7*Power(t2,5))))) + 
         Power(s2,4)*(-2*t2*(-1 + 5*t2 - 2*Power(t2,2) - 4*Power(t2,3) + 
               Power(t2,4)) + Power(t1,6)*
             (3 + 3*Power(s,2) - 3*Power(s1,2) + 98*t2 - 2*s*(7 + 3*t2) - 
               2*s1*(16 + 9*t2)) + 
            t1*(7 + (-5 - 40*s + Power(s,2) - 10*Power(s,3))*t2 + 
               (-58 - 14*s + 66*Power(s,2) + 73*Power(s,3))*Power(t2,2) + 
               (37 + 6*s - 30*Power(s,2) - 54*Power(s,3))*Power(t2,3) + 
               (-24 + 13*s - 50*Power(s,2) + Power(s,3))*Power(t2,4) - 
               (27 + 83*s + 11*Power(s,2))*Power(t2,5) + 6*Power(t2,6) + 
               2*Power(s1,3)*Power(t2,2)*(1 + t2) + 
               Power(s1,2)*t2*
                (-6 - 7*t2 - 3*(4 + 13*s)*Power(t2,2) + 
                  (-15 + 13*s)*Power(t2,3)) + 
               s1*(6 - 4*(-7 + 3*s)*t2 - 
                  2*(14 - 35*s + 9*Power(s,2))*Power(t2,2) + 
                  2*(10 - 5*s + 8*Power(s,2))*Power(t2,3) + 
                  (27 + 43*s + 8*Power(s,2))*Power(t2,4) + 
                  (15 + 11*s)*Power(t2,5))) + 
            Power(t1,5)*(412 + 9*Power(s,3) - 5*Power(s1,3) + 1044*t2 + 
               308*Power(t2,2) - 8*Power(t2,3) + 
               Power(s,2)*(59 - 17*s1 + 20*t2 + 2*Power(t2,2)) + 
               2*Power(s1,2)*(87 + 90*t2 + 7*Power(t2,2)) + 
               s1*(-477 - 258*t2 + 110*Power(t2,2) + 8*Power(t2,3)) + 
               s*(494 + 13*Power(s1,2) + 731*t2 - 60*Power(t2,2) - 
                  4*Power(t2,3) - 2*s1*(114 + 105*t2 + 8*Power(t2,2)))) + 
            Power(t1,4)*(-86 - 1502*t2 - 2578*Power(t2,2) - 
               827*Power(t2,3) - 71*Power(t2,4) + 
               Power(s,3)*(32 - 22*t2 - 7*Power(t2,2)) + 
               Power(s1,3)*(117 + 113*t2 + 14*Power(t2,2)) - 
               Power(s1,2)*(422 + 643*t2 + 287*Power(t2,2) + 
                  54*Power(t2,3)) + 
               s1*(557 + 2222*t2 + 1807*Power(t2,2) + 91*Power(t2,3) - 
                  21*Power(t2,4)) + 
               Power(s,2)*(51 + 68*t2 - 207*Power(t2,2) - 
                  19*Power(t2,3) + s1*(-5 + 41*t2 + 16*Power(t2,2))) + 
               s*(5 + 132*t2 - 1206*Power(t2,2) - 236*Power(t2,3) + 
                  21*Power(t2,4) - 
                  Power(s1,2)*(117 + 118*t2 + 23*Power(t2,2)) + 
                  s1*(534 + 1258*t2 + 473*Power(t2,2) + 71*Power(t2,3)))) \
+ Power(t1,3)*(-61 - 334*t2 + 17*Power(t2,2) + 1972*Power(t2,3) + 
               969*Power(t2,4) + 63*Power(t2,5) + 
               Power(s,3)*(2 + 153*t2 - 43*Power(t2,2) + 8*Power(t2,3)) - 
               Power(s1,3)*(28 + 166*t2 + 73*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(s1,2)*(155 + 698*t2 + 868*Power(t2,2) + 
                  199*Power(t2,3) + 28*Power(t2,4)) + 
               s1*(-68 - 248*t2 - 1438*Power(t2,2) - 1662*Power(t2,3) - 
                  365*Power(t2,4) + 19*Power(t2,5)) + 
               Power(s,2)*(36 + 654*t2 + 178*Power(t2,2) + 
                  19*Power(t2,3) + 35*Power(t2,4) - 
                  s1*(-21 + 54*t2 + 61*Power(t2,2) + 12*Power(t2,3))) + 
               s*(164 + 327*t2 + 52*Power(t2,2) + 650*Power(t2,3) + 
                  316*Power(t2,4) - 13*Power(t2,5) + 
                  Power(s1,2)*
                   (26 + 463*t2 + 136*Power(t2,2) + 13*Power(t2,3)) + 
                  s1*(24 + 105*t2 - 991*Power(t2,2) - 539*Power(t2,3) - 
                     51*Power(t2,4)))) + 
            Power(t1,2)*(-7 - 141*t2 + 69*Power(t2,2) + 271*Power(t2,3) + 
               56*Power(t2,4) - 316*Power(t2,5) - 36*Power(t2,6) + 
               Power(s1,3)*(-6 - 18*t2 + Power(t2,2) + 31*Power(t2,3)) + 
               Power(s1,2)*(28 + 106*t2 + 84*Power(t2,2) - 
                  75*Power(t2,3) - 103*Power(t2,4)) + 
               Power(s,3)*(-34 + 201*t2 + 77*Power(t2,2) - 
                  39*Power(t2,3) + 3*Power(t2,4)) + 
               s1*(36 - 38*t2 - 177*Power(t2,2) - 144*Power(t2,3) + 
                  183*Power(t2,4) + 164*Power(t2,5)) + 
               Power(s,2)*(-28 + 434*t2 + 369*Power(t2,2) + 
                  20*Power(t2,3) - 5*Power(t2,4) - 10*Power(t2,5) + 
                  s1*(9 - 59*t2 - 127*Power(t2,2) + 9*Power(t2,3) + 
                     8*Power(t2,4))) + 
               s*(11 + 65*t2 + 116*Power(t2,2) + 472*Power(t2,3) + 
                  184*Power(t2,4) - 145*Power(t2,5) + 
                  Power(s1,2)*
                   (-6 + 210*t2 + 61*Power(t2,2) - 144*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  s1*(-63 - 147*t2 - 347*Power(t2,2) - 256*Power(t2,3) + 
                     179*Power(t2,4) + 22*Power(t2,5)))))))/
     (Power(-1 + s2,2)*s2*Power(s2 - t1,2)*(-s + s2 - t1)*(-1 + t1)*t1*
       (s - s1 + t2)*(-1 + s2 - t1 + t2)*(-t1 + s2*t2)*
       (Power(s2,2) - 4*t1 + 2*s2*t2 + Power(t2,2))) - 
    (8*(-2 - 5*s1*t1 + 2*Power(t1,2) - 3*s1*Power(t1,2) - 
         3*Power(s1,2)*Power(t1,2) + Power(t1,3) + s1*Power(t1,3) + 
         Power(t1,4) - 4*s1*Power(t1,4) + 5*Power(s1,2)*Power(t1,4) + 
         2*Power(s1,3)*Power(t1,4) - Power(t1,5) - 16*s1*Power(t1,5) + 
         10*Power(s1,2)*Power(t1,5) - Power(t1,6) - 5*s1*Power(t1,6) + 
         9*t2 - 4*t1*t2 + 19*s1*t1*t2 + 3*Power(t1,2)*t2 - 
         9*s1*Power(t1,2)*t2 + 11*Power(s1,2)*Power(t1,2)*t2 - 
         19*Power(t1,3)*t2 + 2*s1*Power(t1,3)*t2 - 
         12*Power(s1,2)*Power(t1,3)*t2 + 2*Power(s1,3)*Power(t1,3)*t2 + 
         42*Power(t1,4)*t2 - 46*s1*Power(t1,4)*t2 - 
         Power(s1,2)*Power(t1,4)*t2 - Power(s1,3)*Power(t1,4)*t2 + 
         5*Power(t1,5)*t2 + 2*s1*Power(t1,5)*t2 - 
         2*Power(s1,2)*Power(t1,5)*t2 - 4*Power(t1,6)*t2 - 
         16*Power(t2,2) + 11*t1*Power(t2,2) - 27*s1*t1*Power(t2,2) - 
         9*Power(t1,2)*Power(t2,2) + 26*s1*Power(t1,2)*Power(t2,2) - 
         13*Power(s1,2)*Power(t1,2)*Power(t2,2) + 
         16*Power(t1,3)*Power(t2,2) + s1*Power(t1,3)*Power(t2,2) + 
         12*Power(s1,2)*Power(t1,3)*Power(t2,2) - 
         2*Power(s1,3)*Power(t1,3)*Power(t2,2) - 
         14*Power(t1,4)*Power(t2,2) + 16*s1*Power(t1,4)*Power(t2,2) + 
         12*Power(t1,5)*Power(t2,2) + 14*Power(t2,3) - 9*t1*Power(t2,3) + 
         17*s1*t1*Power(t2,3) + Power(t1,2)*Power(t2,3) - 
         13*s1*Power(t1,2)*Power(t2,3) + 
         5*Power(s1,2)*Power(t1,2)*Power(t2,3) - 
         2*Power(t1,3)*Power(t2,3) - 4*s1*Power(t1,3)*Power(t2,3) - 
         12*Power(t1,4)*Power(t2,3) - 6*Power(t2,4) + t1*Power(t2,4) - 
         4*s1*t1*Power(t2,4) + 3*Power(t1,2)*Power(t2,4) - 
         s1*Power(t1,2)*Power(t2,4) + 4*Power(t1,3)*Power(t2,4) + 
         Power(t2,5) + t1*Power(t2,5) + 
         Power(s2,8)*(Power(s1,3) + s1*Power(t2,2) - 2*Power(t2,3)) - 
         Power(s,3)*(Power(t1,5) - t1*Power(t2,2)*(1 + t2) - 
            2*Power(t1,4)*(2 + t2) + 
            Power(t1,2)*t2*(-2 + 7*t2 - 3*Power(t2,2)) + 
            Power(t2,3)*(2 - 3*t2 + Power(t2,2)) + 
            Power(t1,3)*(1 + t2 + 3*Power(t2,2))) - 
         Power(s2,7)*(Power(s1,3)*(4*t1 - 3*t2) + 
            s1*t2*(-2*(-5 + t2)*t2 + t1*(2 + t2) + s*(3 - 3*t1 + t2)) + 
            Power(s1,2)*(3 + (-4 + t1)*t2 - 2*Power(t2,2) - 
               s*(3*t1 + t2)) + 
            Power(t2,2)*(1 + s*(3 - 4*t1 - 2*t2) + 4*t2 + 
               5*Power(t2,2) - 2*t1*(3 + 5*t2))) + 
         Power(s,2)*(Power(t1,5)*(21 + 2*s1 - 3*t2) + 
            3*(-2 + t2)*Power(-1 + t2,2)*Power(t2,2) + 
            Power(t1,4)*(4 + s1*(2 - 5*t2) - 38*t2 + 6*Power(t2,2)) + 
            t1*(-1 + t2)*t2*(4 + t2 + Power(t2,2) + Power(t2,3) + 
               s1*(-6 + 8*t2 - 4*Power(t2,2))) + 
            Power(t1,3)*(5 - 26*t2 + 23*Power(t2,2) - 2*Power(t2,3) + 
               2*s1*(-5 - 6*t2 + 4*Power(t2,2))) - 
            Power(t1,2)*(2 - 13*t2 - 22*Power(t2,2) + 9*Power(t2,3) + 
               2*Power(t2,4) + 
               s1*(-4 + 22*t2 - 14*Power(t2,2) + Power(t2,3)))) + 
         s*(3*Power(t1,6) - Power(t1,5)*
             (12 + Power(s1,2) + s1*(33 - 5*t2) - 10*t2) - 
            3*(-2 + t2)*Power(-1 + t2,3)*t2 - 
            t1*Power(-1 + t2,2)*
             (5 + 2*t2 + 5*Power(t2,2) + 2*Power(t2,3) + 
               s1*(-6 + 13*t2 - 8*Power(t2,2))) + 
            Power(t1,4)*(-8 + 4*Power(s1,2)*(-2 + t2) + 17*t2 - 
               28*Power(t2,2) + s1*(-71 + 62*t2 - 5*Power(t2,2))) + 
            Power(t1,2)*(-1 + t2)*
             (-9 + 5*t2 + 3*Power(t2,2) + Power(t2,3) + 
               Power(s1,2)*(-4 + 8*t2 - 5*Power(t2,2)) + 
               s1*(9 - 14*t2 + 9*Power(t2,2) + Power(t2,3))) - 
            Power(t1,3)*(3 - 89*t2 + 23*Power(t2,2) - 16*Power(t2,3) + 
               Power(s1,2)*(13 - 21*t2 + 2*Power(t2,2)) + 
               s1*(21 + 7*t2 + 21*Power(t2,2) + Power(t2,3)))) + 
         Power(s2,6)*(Power(s1,3)*
             (-4 + 6*Power(t1,2) + 2*t2 + 3*Power(t2,2) - 
               t1*(2 + 11*t2)) + 
            Power(s1,2)*(Power(t1,2)*(1 - 9*s + 3*t2) + 
               t2*(-11 + 2*t2 + 5*Power(t2,2) + s*(-4 + 3*t2)) + 
               t1*(8 - 22*t2 - 6*Power(t2,2) + s*(-4 + 9*t2))) - 
            t2*(2*Power(t1,2)*(3 + 15*t2 + 7*Power(t2,2)) + 
               2*t2*(-5 + 5*t2 + 7*Power(t2,2) + 2*Power(t2,3)) - 
               t1*(2 + 13*t2 + 20*Power(t2,2) + 19*Power(t2,3)) - 
               Power(s,2)*(3*Power(t1,2) + t2 - t1*(3 + 5*t2)) + 
               s*(-(Power(-1 + t2,2)*(3 + 5*t2)) + 
                  Power(t1,2)*(8 + 5*t2) + t1*(-3 + 10*t2 + Power(t2,2))\
)) + s1*(3 + Power(s,2)*t1*(3*t1 - t2) - 8*t2 + 9*Power(t2,2) - 
               19*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,2)*(1 + 2*t2 - 3*Power(t2,2)) + 
               t1*t2*(22 + 24*t2 + 3*Power(t2,2)) + 
               s*(t2 - 3*Power(t2,3) - Power(t1,2)*(3 + 8*t2) + 
                  t1*(-3 + 13*t2 + 15*Power(t2,2))))) + 
         Power(s2,5)*(-1 + 4*t2 - 2*s*t2 - 11*Power(t2,2) + 
            21*s*Power(t2,2) + 7*Power(s,2)*Power(t2,2) + 
            23*Power(t2,3) + 20*s*Power(t2,3) + Power(s,2)*Power(t2,3) - 
            19*Power(t2,4) - 5*s*Power(t2,4) - 18*Power(t2,5) + 
            4*s*Power(t2,5) - Power(t2,6) + 
            Power(t1,2)*(-1 + 
               (-14 + 22*s + 11*Power(s,2) - 2*Power(s,3))*t2 + 
               3*(-9 + 4*s + 5*Power(s,2))*Power(t2,2) + 
               (-65 + 12*s)*Power(t2,3) - 14*Power(t2,4)) + 
            Power(s1,3)*(-2 - 4*Power(t1,3) + 3*Power(t2,2) + 
               Power(t2,3) + Power(t1,2)*(7 + 15*t2) + 
               t1*(16 - 13*t2 - 10*Power(t2,2))) + 
            Power(t1,3)*(2 + Power(s,3) + 30*t2 + 42*Power(t2,2) + 
               6*Power(t2,3) - Power(s,2)*(3 + 4*t2) + 
               s*(4 + 10*t2 - Power(t2,2))) + 
            t1*t2*(-21 + Power(s,3)*t2 + 68*Power(t2,2) + 
               26*Power(t2,3) + 9*Power(t2,4) + 
               Power(s,2)*(5 - 9*t2 - 11*Power(t2,2)) - 
               s*(9 - 17*t2 + 35*Power(t2,2) + 15*Power(t2,3))) + 
            Power(s1,2)*(12 - 3*(7 + 2*s)*t2 - (19 + 8*s)*Power(t2,2) + 
               3*(-1 + s)*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,2)*(2 + s*(3 - 27*t2) + 44*t2 + 
                  4*Power(t2,2)) + Power(t1,3)*(9*s - 3*(1 + t2)) + 
               t1*(8 + 31*t2 - 17*Power(t2,2) - 9*Power(t2,3) + 
                  2*s*(-4 + 2*t2 + 5*Power(t2,2)))) + 
            s1*(Power(t1,3)*(-1 - 6*Power(s,2) + 6*t2 + 5*Power(t2,2) + 
                  s*(8 + 7*t2)) - 
               Power(t1,2)*(12 + Power(s,2)*(5 - 14*t2) + 60*t2 + 
                  40*Power(t2,2) + 13*Power(t2,3) + 
                  s*(-6 + 50*t2 + 22*Power(t2,2))) + 
               t2*(13 + 3*t2 - 2*Power(s,2)*t2 + 3*Power(t2,2) - 
                  11*Power(t2,3) + 2*Power(t2,4) + 
                  s*(20 - 8*t2 + 7*Power(t2,2) - 4*Power(t2,3))) + 
               t1*(-4 + 18*t2 + 24*Power(t2,2) + 27*Power(t2,3) + 
                  7*Power(t2,4) - Power(s,2)*t2*(7 + 4*t2) + 
                  s*(5 - 51*t2 + Power(t2,2) + 24*Power(t2,3))))) + 
         Power(s2,4)*(11*Power(t1,2) + 5*Power(t1,3) - 10*Power(t1,4) - 
            5*t2 + 4*t1*t2 + 33*Power(t1,2)*t2 + 14*Power(t1,3)*t2 - 
            42*Power(t1,4)*t2 - 5*Power(t2,2) - 30*t1*Power(t2,2) - 
            89*Power(t1,2)*Power(t2,2) + 76*Power(t1,3)*Power(t2,2) - 
            18*Power(t1,4)*Power(t2,2) + 23*Power(t2,3) + 
            19*t1*Power(t2,3) - 65*Power(t1,2)*Power(t2,3) + 
            49*Power(t1,3)*Power(t2,3) + 28*Power(t2,4) + 
            93*t1*Power(t2,4) - 43*Power(t1,2)*Power(t2,4) - 
            12*Power(t2,5) + 22*t1*Power(t2,5) - 12*Power(t2,6) - 
            Power(s,3)*(Power(t1,4) + Power(t1,3)*(1 - 5*t2) + 
               2*Power(t2,3) - t1*Power(t2,2)*(5 + 3*t2) + 
               Power(t1,2)*t2*(2 + 7*t2)) + 
            Power(s,2)*(Power(t1,4)*(4 + t2) - 
               Power(t1,3)*t2*(29 + 6*t2) - 
               6*Power(t2,2)*(1 - t2 + Power(t2,2)) + 
               Power(t1,2)*t2*(-24 + 26*t2 + 9*Power(t2,2)) + 
               t1*t2*(2 + 22*t2 + 9*Power(t2,2) - 4*Power(t2,3))) + 
            Power(s1,3)*(3 + Power(t1,4) - 3*t2 + Power(t2,3) - 
               9*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(-22 + 29*t2 + 11*Power(t2,2)) - 
               t1*(-8 + 4*t2 + 9*Power(t2,2) + 3*Power(t2,3))) + 
            s*(Power(t1,4)*(-5 + 2*t2 + 2*Power(t2,2)) - 
               Power(t1,3)*(14 + 28*t2 + 33*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(1 + 7*t2 + 59*Power(t2,2) + 
                  59*Power(t2,3) + 19*Power(t2,4)) + 
               t2*(-16 + 13*t2 + 22*Power(t2,2) + 41*Power(t2,3) - 
                  7*Power(t2,4) + Power(t2,5)) - 
               t1*(1 + 91*Power(t2,2) - 3*Power(t2,3) + 
                  21*Power(t2,4) + 10*Power(t2,5))) + 
            Power(s1,2)*(6 + (-3 + 4*s)*t2 + 2*s*Power(t2,2) - 
               8*s*Power(t2,3) + s*Power(t2,4) + Power(t2,5) + 
               Power(t1,4)*(3 - 3*s + t2) + 
               Power(t1,3)*(-26 + 9*s - 36*t2 + 23*s*t2 + 
                  2*Power(t2,2)) + 
               Power(t1,2)*(-21 - 19*t2 + 27*Power(t2,2) + 
                  Power(t2,3) + s*(32 - 25*t2 - 27*Power(t2,2))) + 
               t1*(-33 + 114*t2 + 48*Power(t2,2) - 8*Power(t2,3) - 
                  5*Power(t2,4) + 
                  s*(12 + 18*t2 + 7*Power(t2,2) + 6*Power(t2,3)))) + 
            s1*(-12 + 6*(6 + s)*t2 + (-3 + 4*s)*Power(t2,2) - 
               (33 + 23*s)*Power(t2,3) + (-23 + 11*s)*Power(t2,4) - 
               3*s*Power(t2,5) + Power(t2,6) + 
               Power(t1,4)*(-3 + 3*Power(s,2) - 10*t2 - 2*Power(t2,2) - 
                  s*(7 + 2*t2)) + 
               Power(t1,3)*(34 + Power(s,2)*(1 - 19*t2) + 77*t2 + 
                  51*Power(t2,2) + 8*Power(t2,3) + 
                  s*(17 + 67*t2 + 4*Power(t2,2))) + 
               Power(t1,2)*(-9 - 55*t2 - 49*Power(t2,2) - 
                  40*Power(t2,3) - 10*Power(t2,4) + 
                  Power(s,2)*(-2 + 6*t2 + 23*Power(t2,2)) + 
                  s*(27 + 125*t2 - 62*Power(t2,2) - 12*Power(t2,3))) - 
               t1*(10 + 43*t2 + 37*Power(t2,2) - 33*Power(t2,3) - 
                  3*Power(t2,5) + 
                  Power(s,2)*t2*(-12 + 17*t2 + 7*Power(t2,2)) + 
                  s*(-4 + 51*t2 + 65*Power(t2,2) + 17*Power(t2,3) - 
                     13*Power(t2,4))))) + 
         s2*(-3 + (-5 + 7*s)*Power(t1,6) + 14*t2 + 2*s*t2 - 
            22*Power(t2,2) + 4*s*Power(t2,2) + 
            7*Power(s,2)*Power(t2,2) + 13*Power(t2,3) - 
            12*s*Power(t2,3) - 9*Power(s,2)*Power(t2,3) + 
            2*Power(s,3)*Power(t2,3) - 2*Power(t2,4) + 6*s*Power(t2,4) + 
            3*Power(s,2)*Power(t2,4) - 5*Power(s,3)*Power(t2,4) + 
            Power(t2,5) - 2*s*Power(t2,5) + Power(s,3)*Power(t2,5) - 
            Power(t2,6) + 2*s*Power(t2,6) - Power(s,2)*Power(t2,6) + 
            Power(t1,5)*(-2 + 2*Power(s,3) + 20*t2 + 12*Power(t2,2) + 
               Power(s,2)*(1 + 3*t2) - 4*s*(1 + 6*t2)) + 
            Power(s1,3)*Power(t1,2)*
             (3 - 3*Power(t1,3) - 11*t2 + 7*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(-2 + 4*t2) + t1*(-9 + 7*t2 - 2*Power(t2,2))) \
- Power(t1,4)*(-17 + 37*t2 + 31*Power(t2,2) + 36*Power(t2,3) + 
               Power(s,3)*(6 + t2) + 
               Power(s,2)*(13 + 51*t2 + 3*Power(t2,2)) + 
               s*(12 - 37*t2 + 13*Power(t2,2))) - 
            Power(t1,3)*(-9 + 61*t2 + 105*Power(t2,2) - 
               53*Power(t2,3) - 36*Power(t2,4) + 
               Power(s,3)*(1 + t2 + 2*Power(t2,2)) + 
               s*(-19 + 62*t2 + 14*Power(t2,2) - 60*Power(t2,3)) + 
               Power(s,2)*(16 - 47*t2 - 87*Power(t2,2) + 4*Power(t2,3))) \
+ Power(t1,2)*(-11 + 28*t2 + 17*Power(t2,2) + 
               15*Power(s,3)*Power(t2,2) - 17*Power(t2,3) - 
               5*Power(t2,4) - 12*Power(t2,5) + 
               Power(s,2)*t2*
                (-13 - 5*t2 - 45*Power(t2,2) + 4*Power(t2,3)) + 
               s*(2 - 75*t2 - 178*Power(t2,2) + 48*Power(t2,3) - 
                  34*Power(t2,4))) - 
            t1*(Power(s,3)*Power(t2,2)*(1 + 3*t2) - 
               s*Power(-1 + t2,2)*
                (-4 - 40*t2 - 5*Power(t2,2) + 2*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (5 + 6*t2 + 6*Power(t2,2) + 7*Power(t2,3)) - 
               Power(s,2)*t2*(9 - 62*t2 + 8*Power(t2,3) + Power(t2,4))) \
+ Power(s1,2)*t1*(Power(t1,4)*(-7 + 4*t2) + 
               Power(t1,3)*(-54 - 6*t2 + Power(t2,2)) - 
               2*Power(-1 + t2,2)*(-5 + 6*t2 + Power(t2,2)) - 
               Power(t1,2)*(15 + 5*t2 - 6*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-12 + 50*t2 - 27*Power(t2,2) - 12*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(-6 + 8*Power(t1,4) + Power(t1,3)*(2 - 9*t2) + 17*t2 - 
                  16*Power(t2,2) + 3*Power(t2,3) + 2*Power(t2,4) + 
                  Power(t1,2)*(17 - 12*t2 + 8*Power(t2,2)) - 
                  t1*(-24 + 18*t2 + 23*Power(t2,2) + Power(t2,3)))) + 
            s1*(-9*Power(t1,6) + 
               Power(t1,5)*(18 - 7*Power(s,2) + s*(4 - 7*t2) + 22*t2) + 
               (-1 + t2)*(-6 + (16 - 6*s)*t2 + 
                  (-13 + 11*s)*Power(t2,2) + 
                  (2 - 3*s + Power(s,2))*Power(t2,3) + 
                  Power(-1 + s,2)*Power(t2,4)) + 
               Power(t1,4)*(39 + 39*t2 - 12*Power(t2,2) + 
                  6*Power(s,2)*(1 + t2) + 
                  s*(110 + 42*t2 + 3*Power(t2,2))) + 
               Power(t1,3)*(-36 + 114*t2 + 91*Power(t2,2) - 
                  33*Power(t2,3) + 
                  Power(s,2)*(23 - 4*t2 - 3*Power(t2,2)) + 
                  s*(100 + 80*t2 - 140*Power(t2,2) + 7*Power(t2,3))) + 
               t1*(Power(-1 + t2,2)*(-11 + 33*t2 + 16*Power(t2,2)) + 
                  Power(s,2)*t2*
                   (-7 + 37*t2 - 22*Power(t2,2) + 2*Power(t2,3)) + 
                  s*(13 - 25*t2 + 14*Power(t2,2) + 5*Power(t2,3) - 
                     7*Power(t2,4))) + 
               Power(t1,2)*(-15 + 8*t2 + 17*Power(t2,2) - 
                  22*Power(t2,3) + 12*Power(t2,4) + 
                  Power(s,2)*
                   (-3 + 77*t2 - 28*Power(t2,2) + Power(t2,3)) + 
                  s*(3 + 18*t2 + 93*Power(t2,2) + 39*Power(t2,3) - 
                     3*Power(t2,4))))) + 
         Power(s2,2)*(2 + 2*(-3 + s)*Power(t1,6) - 3*t2 + 19*s*t2 + 
            6*Power(t2,2) - 28*s*Power(t2,2) + 
            11*Power(s,2)*Power(t2,2) - 12*Power(t2,3) + 
            6*s*Power(t2,3) + 4*Power(s,2)*Power(t2,3) + 
            4*Power(s,3)*Power(t2,3) + 12*Power(t2,4) - 
            7*s*Power(t2,4) + 14*Power(s,2)*Power(t2,4) + 
            2*Power(s,3)*Power(t2,4) - 9*Power(t2,5) + 
            13*s*Power(t2,5) - 8*Power(s,2)*Power(t2,5) + 
            4*Power(t2,6) - 3*s*Power(t2,6) + Power(s,2)*Power(t2,6) + 
            Power(t1,5)*(3 + Power(s,3) + 31*t2 + 
               Power(s,2)*(-9 + 2*t2) - s*(11 + 26*t2)) + 
            Power(s1,3)*t1*(-5 - Power(t1,4) + 13*t2 - 5*Power(t2,2) - 
               3*Power(t2,3) + Power(t1,3)*(4 + 9*t2) + 
               Power(t1,2)*(7 - 9*t2 - 6*Power(t2,2)) + 
               t1*(15 - 14*t2 + 4*Power(t2,2) + Power(t2,3))) + 
            Power(t1,4)*(Power(s,3)*(2 - 7*t2) + 
               Power(s,2)*(-32 + 17*t2 - 9*Power(t2,2)) + 
               s*(24 + 22*t2 + 62*Power(t2,2)) - 
               2*(-4 + 11*t2 + 31*Power(t2,2) + 6*Power(t2,3))) + 
            Power(t1,3)*(-12 - 27*t2 + 122*Power(t2,2) + 
               69*Power(t2,3) + 36*Power(t2,4) + 
               Power(s,3)*(2 + 11*Power(t2,2)) + 
               Power(s,2)*(2 + 79*t2 + 32*Power(t2,2) + 
                  12*Power(t2,3)) - 
               s*(-22 + 25*t2 + 43*Power(t2,2) + 16*Power(t2,3))) - 
            Power(t1,2)*(14 - t2 - 163*Power(t2,2) - 63*Power(t2,3) + 
               75*Power(t2,4) + 36*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(4 + 5*t2) + 
               Power(s,2)*(-2 - 35*t2 + 124*Power(t2,2) + 
                  59*Power(t2,3) + 4*Power(t2,4)) + 
               s*(5 + 21*t2 - 181*Power(t2,2) + 7*Power(t2,3) + 
                  43*Power(t2,4))) + 
            t1*(-1 + 24*t2 - 56*Power(t2,2) - 
               6*Power(s,3)*Power(t2,2) + 12*Power(t2,3) - 
               4*Power(t2,4) + 13*Power(t2,5) + 12*Power(t2,6) + 
               Power(s,2)*t2*
                (5 + 49*t2 + 39*Power(t2,2) + 27*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s*(6 + 3*t2 + 136*Power(t2,2) + 94*Power(t2,3) - 
                  26*Power(t2,4) + 24*Power(t2,5))) + 
            Power(s1,2)*(Power(t1,5)*(3*s + 2*(-5 + t2)) - 
               (-1 + t2)*(-6 + 11*t2 - 2*Power(t2,2) + 
                  (-3 + s)*Power(t2,3)) + 
               Power(t1,4)*(7 + 8*t2 - 7*Power(t2,2) - s*(2 + 25*t2)) + 
               Power(t1,3)*(63 + 111*t2 - 19*Power(t2,2) + 
                  Power(t2,3) + 2*s*(11 - 5*t2 + 13*Power(t2,2))) + 
               Power(t1,2)*(24 + t2 - 2*Power(t2,2) - 2*Power(t2,3) + 
                  3*Power(t2,4) - 
                  s*(22 - 38*t2 + 23*Power(t2,2) + 8*Power(t2,3))) + 
               t1*(22 - 62*t2 + 25*Power(t2,2) + 11*Power(t2,3) + 
                  5*Power(t2,4) - Power(t2,5) + 
                  s*(-8 - 13*t2 + 21*Power(t2,2) + 18*Power(t2,3)))) - 
            s1*(-9 + 2*Power(t1,6) + (31 + 7*s)*t2 - 
               2*(13 + 5*s)*Power(t2,2) + 
               (2 + s + 9*Power(s,2))*Power(t2,3) + 
               (-6 + 6*s - 5*Power(s,2))*Power(t2,4) + 
               (9 - 5*s + Power(s,2))*Power(t2,5) + 
               (-1 + s)*Power(t2,6) + 
               Power(t1,5)*(-27 + 3*Power(s,2) - 26*t2 + 
                  s*(-19 + 4*t2)) + 
               Power(t1,4)*(42 + Power(s,2)*(4 - 23*t2) + 38*t2 + 
                  43*Power(t2,2) + s*(-51 + 31*t2 - 16*Power(t2,2))) + 
               Power(t1,3)*(44 + 90*t2 + 9*Power(t2,2) - 
                  19*Power(t2,3) + 
                  Power(s,2)*(30 - 15*t2 + 31*Power(t2,2)) + 
                  s*(51 + 262*t2 - 14*Power(t2,2) + 13*Power(t2,3))) + 
               Power(t1,2)*(-32 - 24*t2 + 224*Power(t2,2) + 
                  61*Power(t2,3) - 25*Power(t2,4) + 
                  Power(s,2)*
                   (5 + 59*t2 - 29*Power(t2,2) - 12*Power(t2,3)) + 
                  s*(1 + 209*t2 - 71*Power(t2,2) - 85*Power(t2,3) + 
                     3*Power(t2,4))) + 
               t1*(-18 + 12*t2 + 3*Power(t2,2) + 16*Power(t2,3) - 
                  23*Power(t2,4) + 10*Power(t2,5) + 
                  Power(s,2)*t2*(17 + 38*t2 - 3*Power(t2,2)) + 
                  s*(7 - 83*t2 + 159*Power(t2,2) + 36*Power(t2,3) + 
                     36*Power(t2,4) - 5*Power(t2,5))))) + 
         Power(s2,3)*(4 - 17*t2 + 19*Power(t2,2) - 29*s*Power(t2,2) - 
            14*Power(s,2)*Power(t2,2) - Power(t2,3) - 22*s*Power(t2,3) - 
            23*Power(s,2)*Power(t2,3) - 2*Power(s,3)*Power(t2,3) + 
            2*Power(t2,4) - 29*s*Power(t2,4) - 
            17*Power(s,2)*Power(t2,4) - 2*Power(s,3)*Power(t2,4) + 
            3*Power(t2,5) + 7*s*Power(t2,5) - 5*Power(s,2)*Power(t2,5) - 
            6*Power(t2,6) - 6*s*Power(t2,6) - 4*Power(t2,7) - 
            Power(t1,5)*(s + Power(s,2) + 4*s*t2 - 2*(7 + 9*t2)) - 
            Power(t1,4)*(2 + 33*t2 + 61*Power(t2,2) + 
               Power(s,3)*(1 + 2*t2) + 
               Power(s,2)*(-11 - 15*t2 + 2*Power(t2,2)) - 
               s*(17 + 32*t2 + 31*Power(t2,2))) + 
            Power(s1,3)*(Power(t1,4)*(5 + 2*t2) + 
               Power(t1,3)*(9 - 27*t2 - 4*Power(t2,2)) + 
               2*(1 - 2*t2 + Power(t2,3)) - 
               t1*(11 - 11*t2 + 2*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(-11 + 9*t2 + 12*Power(t2,2) + 2*Power(t2,3))) \
+ Power(t1,3)*(-23 + 27*t2 + 67*Power(t2,2) + 77*Power(t2,3) + 
               4*Power(t2,4) + Power(s,3)*t2*(5 + 6*t2) + 
               Power(s,2)*(18 + 29*t2 - 30*Power(t2,2) + 
                  6*Power(t2,3)) - 
               2*s*(4 + 28*t2 + 31*Power(t2,2) + 30*Power(t2,3))) + 
            Power(t1,2)*(1 + 23*t2 + 9*Power(t2,2) - 159*Power(t2,3) - 
               64*Power(t2,4) - 12*Power(t2,5) + 
               Power(s,3)*t2*(2 - 9*t2 - 6*Power(t2,2)) - 
               Power(s,2)*t2*
                (43 + 66*t2 - 2*Power(t2,2) + 6*Power(t2,3)) + 
               s*(-9 + 31*t2 + 51*Power(t2,2) + 32*Power(t2,3) + 
                  29*Power(t2,4))) + 
            t1*(4 + 23*t2 - 39*Power(t2,2) - 127*Power(t2,3) + 
               11*Power(t2,4) + 48*Power(t2,5) + 12*Power(t2,6) + 
               Power(s,3)*Power(t2,3)*(7 + 2*t2) + 
               Power(s,2)*t2*
                (-14 + 20*t2 + 43*Power(t2,2) + 19*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s*(4 + 31*t2 - 65*Power(t2,2) - 131*Power(t2,3) + 
                  8*Power(t2,4) + 10*Power(t2,5))) - 
            Power(s1,2)*(9 + Power(t1,5) - 5*(4 + s)*t2 - 
               (-4 + s)*Power(t2,2) + (3 + 8*s)*Power(t2,3) + 
               (5 + 4*s)*Power(t2,4) - Power(t2,5) + 
               Power(t1,4)*(-29 - 8*t2 + 2*Power(t2,2) + 
                  s*(11 + 6*t2)) + 
               Power(t1,3)*(-13 + 13*t2 + 5*Power(t2,2) - 
                  3*Power(t2,3) - 2*s*(-15 + 25*t2 + 7*Power(t2,2))) + 
               Power(t1,2)*(-1 + 192*t2 + 17*Power(t2,2) - 
                  14*Power(t2,3) + 
                  2*s*(17 - 2*t2 + 11*Power(t2,2) + 5*Power(t2,3))) + 
               t1*(21 - 12*t2 + 10*Power(t2,2) - 8*Power(t2,3) + 
                  4*Power(t2,4) + Power(t2,5) - 
                  s*(11 - 31*t2 + 16*Power(t2,2) + 9*Power(t2,3) + 
                     2*Power(t2,4)))) + 
            s1*(-6 + (6 - 23*s)*t2 + 
               (-9 + 16*s + 2*Power(s,2))*Power(t2,2) + 
               2*(9 + 18*s + 8*Power(s,2))*Power(t2,3) + 
               (-9 + 16*s + Power(s,2))*Power(t2,4) + 
               (-2 + 6*s)*Power(t2,5) - (-2 + s)*Power(t2,6) + 
               Power(t1,5)*(5 + 2*s + 4*t2) + 
               Power(t1,4)*(Power(s,2)*(7 + 6*t2) + 
                  s*(-39 - 23*t2 + 4*Power(t2,2)) - 
                  5*(8 + 13*t2 + 5*Power(t2,2))) + 
               Power(t1,3)*(38 + 65*t2 + 53*Power(t2,2) + 
                  36*Power(t2,3) - 
                  4*Power(s,2)*(-4 + 7*t2 + 4*Power(t2,2)) - 
                  s*(87 + 37*t2 - 45*Power(t2,2) + 9*Power(t2,3))) + 
               Power(t1,2)*(28 + 96*t2 + 17*Power(t2,2) - 
                  13*Power(t2,3) - 12*Power(t2,4) + 
                  2*Power(s,2)*
                   (4 + t2 + 11*Power(t2,2) + 7*Power(t2,3)) + 
                  s*(-19 + 199*t2 + 131*Power(t2,2) - 30*Power(t2,3) + 
                     5*Power(t2,4))) + 
               t1*(18 - 125*t2 + 129*Power(t2,2) + 101*Power(t2,3) + 
                  22*Power(t2,4) - 9*Power(t2,5) - 
                  2*Power(s,2)*t2*
                   (-7 - 9*t2 + 9*Power(t2,2) + 2*Power(t2,3)) + 
                  s*(-18 + 45*t2 + 60*Power(t2,2) - 43*Power(t2,3) - 
                     16*Power(t2,4) + Power(t2,5))))))*
       B1(1 - s2 + t1 - t2,s2,t1))/
     ((-1 + t1)*(s - s2 + t1)*(1 - s2 + t1 - t2)*(s - s1 + t2)*
       Power(t1 - s2*t2,3)) - (8*
       (-4*Power(s2,12)*Power(t2,2) + 
         Power(t1,4)*(-1 + s + t1)*(1 + t1 - t2)*
          (Power(s,2)*(2 + t1 - t2) + 2*(-t1 + Power(t1,2) - t2) + 
            s*(-3 + 3*t1 - s1*t1 + 3*t2)) + 
         2*Power(s2,11)*(Power(s1,3) - s1*Power(t2,2) + 
            t2*((5 + 3*s - 8*t2)*t2 + t1*(4 + 9*t2))) - 
         Power(s2,10)*(Power(s1,3)*(6 + 10*t1 - 3*t2) + 
            4*Power(t1,2)*(1 + 9*t2 + 8*Power(t2,2)) + 
            Power(t2,2)*(4 + 2*Power(s,2) + s*(14 - 23*t2) - 55*t2 + 
               14*Power(t2,2)) - 
            2*Power(s1,2)*(-3 - (-4 + t1)*t2 + 4*Power(t2,2) + 
               s*(3*t1 + t2)) + 
            t1*t2*(20 - 2*t2 - 59*Power(t2,2) + 2*s*(6 + 13*t2)) + 
            s1*t2*(s*(6 - 6*t1 + t2) + t2*(21 + 17*t2) - t1*(4 + 21*t2))) \
+ s2*Power(t1,3)*(-3 - 14*Power(t1,5) - 
            Power(t1,4)*(30 - 9*s1 + Power(s1,2) - 14*t2) + 3*t2 - 
            5*Power(t2,2) + 5*Power(t2,3) + 
            Power(t1,3)*(39 + s1 + Power(s1,2)*(-5 + t2) + 42*t2 - 
               8*s1*t2) - t1*
             (21 + s1 - 10*s1*t2 - 11*Power(t2,2) + 9*s1*Power(t2,2)) + 
            Power(t1,2)*(29 + 4*Power(s1,2)*(-1 + t2) - 35*t2 - 
               28*Power(t2,2) - s1*(9 - 18*t2 + Power(t2,2))) + 
            Power(s,3)*(-8 + Power(t1,3) + 6*t2 + 5*Power(t2,2) - 
               3*Power(t2,3) - Power(t1,2)*(14 + t2) + 
               t1*(-24 + 13*t2 + 3*Power(t2,2))) - 
            Power(s,2)*(-14 + 4*Power(t1,4) + 
               Power(t1,3)*(53 + 5*s1 - 12*t2) + 11*t2 + 
               14*Power(t2,2) - 11*Power(t2,3) + 
               Power(t1,2)*(53 + 5*s1 - 43*t2 - 12*s1*t2 + 
                  8*Power(t2,2)) + 
               t1*(-18 + s1 + 14*t2 - 8*s1*t2 - 3*Power(t2,2) + 
                  7*s1*Power(t2,2))) + 
            s*(-2 + 5*(-8 + s1)*Power(t1,4) + t2 + 14*Power(t2,2) - 
               13*Power(t2,3) + 
               Power(t1,3)*(-49 + 4*Power(s1,2) + s1*(26 - 6*t2) + 
                  9*t2) + t1*
                (31 + s1 - 17*s1*t2 - 19*Power(t2,2) + 
                  16*s1*Power(t2,2)) + 
               Power(t1,2)*(40 - 4*Power(s1,2)*(-1 + t2) + 2*t2 + 
                  31*Power(t2,2) + s1*(20 - 37*t2 + Power(t2,2))))) + 
         Power(s2,9)*(Power(s1,3)*
             (2 + 20*Power(t1,2) + t1*(29 - 16*t2) - 2*t2 + Power(t2,2)) \
+ 2*Power(t1,3)*(9 + 32*t2 + 14*Power(t2,2)) + 
            t1*t2*(8 - 148*t2 - 143*Power(t2,2) + 38*Power(t2,3) + 
               Power(s,2)*(-2 + 11*t2) - 
               2*s*(-11 + 5*t2 + 37*Power(t2,2))) + 
            Power(t1,2)*(10 + 44*t2 + 6*Power(s,2)*t2 - 
               97*Power(t2,2) - 87*Power(t2,3) + 
               s*(6 + 52*t2 + 49*Power(t2,2))) - 
            t2*(Power(s,2)*t2*(10 + 7*t2) + 
               s*(-6 + 9*t2 + 94*Power(t2,2) - 19*Power(t2,3)) + 
               t2*(-15 + 87*t2 - 36*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,2)*(18 - (37 + 14*s)*t2 + 
               (-34 + 3*s)*Power(t2,2) + 8*Power(t2,3) + 
               Power(t1,2)*(2 - 24*s + 8*t2) + 
               t1*(22 - 54*t2 - 28*Power(t2,2) + s*(-26 + 7*t2))) + 
            s1*(6 + 2*Power(s,2)*t1*(3*t1 - t2) - 16*t2 + 
               95*Power(t2,2) + 42*Power(t2,3) - 15*Power(t2,4) - 
               2*Power(t1,2)*(1 + 21*t2 + 32*Power(t2,2)) + 
               2*t1*t2*(23 + 51*t2 + 40*Power(t2,2)) + 
               s*(-2*Power(t1,2)*(3 + 11*t2) + 
                  2*t2*(10 + 15*t2 + 4*Power(t2,2)) + 
                  t1*(-6 + 12*t2 + 13*Power(t2,2))))) - 
         Power(s2,2)*Power(t1,2)*
          (-9 + 14*Power(t1,6) + 20*t2 - 24*Power(t2,2) + 
            12*Power(t2,3) + Power(t2,4) - 
            Power(t1,5)*(167 - 93*s1 + 6*Power(s1,2) + 82*t2) + 
            Power(t1,4)*(-62 - 8*Power(s1,3) + s1*(43 - 66*t2) + 
               106*t2 + 86*Power(t2,2) + Power(s1,2)*(15 + 4*t2)) + 
            Power(t1,3)*(240 + 112*t2 + 37*Power(t2,2) - 
               18*Power(t2,3) + Power(s1,3)*(-6 + 5*t2) + 
               Power(s1,2)*(-16 + 9*t2) + s1*(-71 + t2 - 21*Power(t2,2))\
) + Power(t1,2)*(13 - 18*t2 - 127*Power(t2,2) - 38*Power(t2,3) + 
               Power(s1,2)*(-26 + 31*t2 - 5*Power(t2,2)) + 
               s1*(-31 + 54*t2 + 15*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s,3)*(6 + 7*Power(t1,4) - 15*t2 + 18*Power(t2,2) - 
               6*Power(t2,3) - Power(t1,3)*(17 + 5*t2) + 
               Power(t1,2)*(-70 + 4*t2 + 5*Power(t2,2)) - 
               t1*(27 + 10*t2 - 39*Power(t2,2) + 7*Power(t2,3))) + 
            t1*(-53 - 10*t2 + 15*Power(t2,2) + 24*Power(t2,3) + 
               4*Power(t2,4) - 
               s1*(18 - 61*t2 + 36*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s,2)*(15 + Power(t1,5) + 16*t2 - 61*Power(t2,2) + 
               29*Power(t2,3) + Power(t2,4) + 
               Power(t1,4)*(-104 - 15*s1 + 6*t2) + 
               Power(t1,3)*(-276 + 45*t2 + 2*Power(t2,2) + 
                  6*s1*(-4 + 3*t2)) + 
               t1*(86 - 63*Power(t2,2) + 23*Power(t2,3) + 
                  Power(t2,4) + s1*(-9 + 21*t2 - 19*Power(t2,2))) + 
               Power(t1,2)*(-47 + 21*t2 + 55*Power(t2,2) - 
                  10*Power(t2,3) + s1*(-28 + 41*t2 - 3*Power(t2,2)))) + 
            s*(-5 + (-32 + 5*s1)*Power(t1,5) + 
               Power(t1,4)*(-301 + 16*Power(s1,2) + s1*(143 - 17*t2) - 
                  108*t2) - 11*t2 + 53*Power(t2,2) - 35*Power(t2,3) - 
               2*Power(t2,4) + 
               Power(t1,3)*(-68 + Power(s1,2)*(54 - 25*t2) - 6*t2 + 
                  115*Power(t2,2) + s1*(129 - 109*t2 + 11*Power(t2,2))) \
+ Power(t1,2)*(216 - 6*t2 + 63*Power(t2,2) + 30*Power(t2,3) + 
                  Power(s1,2)*(24 - 24*t2 + 5*Power(t2,2)) + 
                  s1*(34 - 107*t2 - 25*Power(t2,2) + Power(t2,3))) + 
               t1*(32 + 19*t2 + 23*Power(t2,2) - 41*Power(t2,3) - 
                  5*Power(t2,4) + 
                  s1*(25 - 81*t2 + 49*Power(t2,2) + 7*Power(t2,3))))) - 
         Power(s2,8)*(2 - 8*t2 + 22*s*t2 + 95*Power(t2,2) + 
            18*s*Power(t2,2) - 6*Power(s,2)*Power(t2,2) + 
            6*Power(s,3)*Power(t2,2) - 21*Power(t2,3) - 
            62*s*Power(t2,3) + 19*Power(s,2)*Power(t2,3) + 
            91*Power(t2,4) + 92*s*Power(t2,4) + 
            5*Power(s,2)*Power(t2,4) + 8*Power(t2,5) - 2*s*Power(t2,5) + 
            4*Power(t1,4)*(8 + 14*t2 + 3*Power(t2,2)) + 
            Power(s1,3)*(-6 + 20*Power(t1,3) + 
               Power(t1,2)*(54 - 34*t2) + 3*t2 - 10*Power(t2,2) + 
               t1*(10 - 13*t2 + 10*Power(t2,2))) + 
            Power(t1,3)*(30 - 2*Power(s,3) - 17*t2 - 195*Power(t2,2) - 
               61*Power(t2,3) + 2*Power(s,2)*(3 + 7*t2) + 
               s*(26 + 98*t2 + 55*Power(t2,2))) + 
            Power(t1,2)*(4 - 131*t2 + 4*Power(s,3)*t2 - 
               503*Power(t2,2) - 133*Power(t2,3) + 39*Power(t2,4) + 
               Power(s,2)*(2 + 32*t2 + 6*Power(t2,2)) + 
               s*(8 + 27*t2 - 113*Power(t2,2) - 112*Power(t2,3))) - 
            t1*t2*(-32 + 218*t2 - 4*Power(s,3)*t2 + 215*Power(t2,2) - 
               63*Power(t2,3) + 4*Power(t2,4) + 
               Power(s,2)*(52 + 46*t2 + 21*Power(t2,2)) + 
               s*(8 + 389*t2 + 228*Power(t2,2) - 40*Power(t2,3))) - 
            Power(s1,2)*(-6 + 4*Power(t1,3)*(-2 + 9*s - 3*t2) + 
               (28 + 22*s)*t2 - (19 + 42*s)*Power(t2,2) + 
               (-53 + s)*Power(t2,3) + 
               Power(t1,2)*(-14 + s*(95 - 45*t2) + 132*t2 + 
                  25*Power(t2,2)) + 
               t1*(-59 + 246*t2 + 117*Power(t2,2) - 11*Power(t2,3) + 
                  s*(38 + 19*t2 + 6*Power(t2,2)))) + 
            s1*(18 - (65 + 4*s)*t2 + 
               (40 - 11*s - 21*Power(s,2))*Power(t2,2) - 
               2*(37 + 27*s)*Power(t2,3) - (67 + 9*s)*Power(t2,4) + 
               Power(t1,3)*(-21 + 18*Power(s,2) - 128*t2 - 
                  86*Power(t2,2) - 2*s*(11 + 15*t2)) + 
               Power(t1,2)*(25 + 169*t2 + 238*Power(t2,2) + 
                  137*Power(t2,3) - 7*Power(s,2)*(-4 + 3*t2) + 
                  s*(-37 + 22*t2 + 14*Power(t2,2))) + 
               t1*(14 + 114*t2 + 530*Power(t2,2) + 163*Power(t2,3) - 
                  41*Power(t2,4) + Power(s,2)*t2*(8 + 3*t2) + 
                  s*(-28 + 200*t2 + 215*Power(t2,2) + 28*Power(t2,3))))) \
+ Power(s2,5)*(-6 + 2*Power(t1,7) + 11*t2 - 40*s*t2 - 35*Power(t2,2) + 
            43*s*Power(t2,2) + 31*Power(s,2)*Power(t2,2) + 
            24*Power(s,3)*Power(t2,2) + 25*Power(t2,3) + 
            12*s*Power(t2,3) + 8*Power(s,2)*Power(t2,3) - 
            7*Power(s,3)*Power(t2,3) + Power(t2,4) - 9*s*Power(t2,4) - 
            7*Power(s,2)*Power(t2,4) + 2*Power(t2,5) - 2*s*Power(t2,5) + 
            Power(t1,6)*(-33 + 2*Power(s,2) - 35*t2 + 2*Power(t2,2) + 
               s*(37 + 22*t2)) + 
            Power(t1,5)*(-267 - 437*t2 - 35*Power(t2,2) + 
               2*Power(t2,3) + Power(s,3)*(-5 + 4*t2) + 
               s*(19 + 9*t2 - 26*Power(t2,2)) + 
               Power(s,2)*(38 + 30*t2 + 8*Power(t2,2))) + 
            Power(s1,3)*Power(t1,2)*
             (-42 + 20*t2 + 12*Power(t2,2) - Power(t1,3)*(3 + 4*t2) + 
               Power(t1,2)*(4 + 14*t2 + 7*Power(t2,2)) - 
               2*t1*(55 + 42*t2 + 8*Power(t2,2))) - 
            Power(t1,4)*(331 + 1726*t2 + 1254*Power(t2,2) + 
               243*Power(t2,3) + 16*Power(t2,4) + 
               Power(s,3)*(25 - 34*t2 + 4*Power(t2,2)) + 
               Power(s,2)*(112 + 343*t2 + 41*Power(t2,2) + 
                  12*Power(t2,3)) + 
               s*(616 + 1709*t2 + 834*Power(t2,2) + 53*Power(t2,3))) + 
            Power(t1,3)*(331 + 944*t2 + 852*Power(t2,2) + 
               1022*Power(t2,3) + 277*Power(t2,4) + 12*Power(t2,5) - 
               Power(s,3)*(7 + 19*t2 + 12*Power(t2,2)) + 
               Power(s,2)*(-107 - 796*t2 - 67*Power(t2,2) + 
                  28*Power(t2,3) + 4*Power(t2,4)) + 
               s*(-55 - 1097*t2 - 532*Power(t2,2) + 419*Power(t2,3) + 
                  63*Power(t2,4))) + 
            t1*(-12 - 127*t2 + 54*Power(t2,2) - 100*Power(t2,3) + 
               7*Power(t2,4) + 8*Power(t2,5) + 
               Power(s,3)*t2*(72 + 13*t2 - 3*Power(t2,2)) + 
               Power(s,2)*t2*
                (264 + 62*t2 + 77*Power(t2,2) - 12*Power(t2,3)) + 
               s*(-22 + 33*t2 + 181*Power(t2,2) + 86*Power(t2,3) + 
                  31*Power(t2,4) - 12*Power(t2,5))) + 
            Power(t1,2)*(Power(s,3)*
                (24 + t2 - 15*Power(t2,2) + 3*Power(t2,3)) + 
               t2*(486 + 352*t2 - 138*Power(t2,2) - 95*Power(t2,3) - 
                  36*Power(t2,4)) + 
               Power(s,2)*(99 + 51*t2 + 149*Power(t2,2) + 
                  119*Power(t2,3) + Power(t2,4)) + 
               s*(123 + 604*t2 + 138*Power(t2,2) + 627*Power(t2,3) + 
                  82*Power(t2,4) - 6*Power(t2,5))) + 
            Power(s1,2)*t1*(-50 + 2*Power(t1,5) + 67*t2 - 
               8*Power(t2,2) - 9*Power(t2,3) + 
               Power(t1,4)*(-37 + 36*t2 + 11*Power(t2,2)) - 
               Power(t1,3)*(409 + 462*t2 + 81*Power(t2,2) + 
                  13*Power(t2,3)) + 
               t1*(-130 + 168*t2 + 57*Power(t2,2) + 15*Power(t2,3)) + 
               Power(t1,2)*(-48 + 261*t2 + 475*Power(t2,2) + 
                  47*Power(t2,3)) + 
               s*(12 - 12*t2 + 11*Power(t2,2) + 9*Power(t2,3) + 
                  Power(t1,4)*(1 + 12*t2) + 
                  Power(t1,3)*(-68 + 32*t2 - 15*Power(t2,2)) + 
                  t1*(102 + 68*t2 + 42*Power(t2,2) - 30*Power(t2,3)) + 
                  Power(t1,2)*
                   (85 + 185*t2 + 9*Power(t2,2) - Power(t2,3)))) - 
            s1*(12 + 2*(-13 + 6*s)*t2 + 
               (12 - 21*s + 7*Power(s,2))*Power(t2,2) + 
               (-2 + 5*s)*Power(t2,3) + 4*(1 + s)*Power(t2,4) + 
               Power(t1,6)*(54 + 4*s + 26*t2) + 
               Power(t1,5)*(-51 - 112*t2 - 55*Power(t2,2) + 
                  Power(s,2)*(-7 + 12*t2) + 
                  s*(16 + 68*t2 + 19*Power(t2,2))) - 
               Power(t1,4)*(738 + 2002*t2 + 908*Power(t2,2) + 
                  42*Power(t2,3) + 
                  Power(s,2)*(95 - 78*t2 + 12*Power(t2,2)) + 
                  2*s*(293 + 464*t2 + 74*Power(t2,2) + 12*Power(t2,3))) \
+ Power(t1,2)*(130 + 362*t2 - 6*Power(t2,2) + 118*Power(t2,3) - 
                  9*Power(t2,4) + 
                  3*Power(s,2)*
                   (5 + 14*t2 + 38*Power(t2,2) - 8*Power(t2,3)) + 
                  s*(75 + 253*t2 + 469*Power(t2,2) + 189*Power(t2,3) - 
                     30*Power(t2,4))) + 
               t1*(71 - 98*t2 + 6*Power(t2,2) - 42*Power(t2,3) + 
                  23*Power(t2,4) + 
                  2*Power(s,2)*t2*(10 + 51*t2 - 12*Power(t2,2)) + 
                  s*(-10 + 184*t2 - 90*Power(t2,2) + 97*Power(t2,3) + 
                     3*Power(t2,4))) + 
               Power(t1,3)*(182 + 258*t2 + 902*Power(t2,2) + 
                  722*Power(t2,3) + 51*Power(t2,4) - 
                  6*Power(s,2)*(15 - 11*t2 + 5*Power(t2,2)) + 
                  s*(-225 - 891*t2 + 439*Power(t2,2) + 105*Power(t2,3) + 
                     5*Power(t2,4))))) + 
         Power(s2,4)*(4 + Power(t1,7)*(9 - 11*s + 13*s1 - 4*t2) - 
            12*t2 + 12*s*t2 + 14*Power(t2,2) - 19*s*Power(t2,2) - 
            3*Power(s,2)*Power(t2,2) - 6*Power(s,3)*Power(t2,2) - 
            6*Power(t2,3) - 2*s*Power(t2,3) + 3*Power(s,3)*Power(t2,3) + 
            9*s*Power(t2,4) + 3*Power(s,2)*Power(t2,4) - 
            Power(t1,6)*(2*Power(s,3) - 2*Power(s1,3) + 
               Power(s,2)*(13 - 6*s1 + 12*t2) + 
               Power(s1,2)*(7 + 18*t2) + s1*(7 + 26*t2) + 
               2*(-81 - 23*t2 + 9*Power(t2,2)) + 
               s*(46 + 6*Power(s1,2) + 26*t2 - 3*s1*(7 + 10*t2))) + 
            Power(t1,5)*(641 + Power(s,3)*(2 - 11*t2) + 1415*t2 + 
               586*Power(t2,2) + 86*Power(t2,3) - 
               Power(s1,3)*(4 + 7*t2) + 
               Power(s1,2)*(284 + 139*t2 + 22*Power(t2,2)) - 
               s1*(789 + 1135*t2 + 218*Power(t2,2)) + 
               Power(s,2)*(151 + 68*t2 + 5*Power(t2,2) + 
                  3*s1*(1 + 7*t2)) - 
               s*(-660 - 884*t2 - 197*Power(t2,2) + 
                  Power(s1,2)*(2 + 3*t2) + 
                  s1*(512 + 260*t2 + 25*Power(t2,2)))) + 
            Power(t1,4)*(-434 - 531*t2 - 904*Power(t2,2) - 
               620*Power(t2,3) - 76*Power(t2,4) + 
               Power(s1,3)*(98 + 45*t2 + 2*Power(t2,2)) + 
               Power(s,3)*(49 - 22*t2 + 12*Power(t2,2)) + 
               Power(s1,2)*(-1 - 323*t2 - 158*Power(t2,2) + 
                  2*Power(t2,3)) + 
               s1*(131 + 221*t2 + 772*Power(t2,2) + 206*Power(t2,3)) + 
               Power(s,2)*(416 + 630*t2 - 48*Power(t2,2) + 
                  10*Power(t2,3) + s1*(-44 + 44*t2 - 23*Power(t2,2))) + 
               s*(Power(s1,2)*(-137 - 81*t2 + 11*Power(t2,2)) + 
                  2*(241 + 708*t2 + 56*Power(t2,2) - 66*Power(t2,3)) + 
                  s1*(-626 - 477*t2 + 224*Power(t2,2) + Power(t2,3)))) + 
            t1*(21 + 14*t2 + 35*Power(t2,2) - 42*Power(t2,3) - 
               14*Power(t2,4) - 4*Power(t2,5) - 
               Power(s,3)*t2*(48 - 7*t2 + Power(t2,2)) + 
               s1*(40 - 93*t2 + 42*Power(t2,2) + 3*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s,2)*t2*
                (-154 + 77*t2 - 16*Power(t2,2) - 7*Power(t2,3) + 
                  s1*(2 + 23*t2 - 4*Power(t2,2))) + 
               s*(10 + 111*t2 - 222*Power(t2,2) + 73*Power(t2,3) + 
                  6*Power(t2,4) + 4*Power(t2,5) + 
                  s1*(-12 + 52*t2 - 59*Power(t2,2) + 19*Power(t2,3)))) + 
            Power(t1,3)*(-197 - 832*t2 - 82*Power(t2,2) + 
               106*Power(t2,3) + 103*Power(t2,4) + 12*Power(t2,5) - 
               2*Power(s1,3)*(-23 + 13*t2 + 5*Power(t2,2)) + 
               Power(s,3)*(17 + 109*t2 - 23*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(s1,2)*(-101 + 147*t2 + 41*Power(t2,2) + 
                  3*Power(t2,3)) + 
               s1*(267 + 262*t2 + 52*Power(t2,2) + 60*Power(t2,3) - 
                  11*Power(t2,4)) + 
               Power(s,2)*(-23 + 377*t2 - 221*Power(t2,2) - 
                  53*Power(t2,3) - 3*Power(t2,4) - 
                  4*s1*(-8 - 3*t2 - Power(t2,2) + Power(t2,3))) + 
               s*(-373 - 263*t2 - 358*Power(t2,2) - 449*Power(t2,3) - 
                  34*Power(t2,4) + 
                  2*Power(s1,2)*
                   (-95 - 19*t2 + 20*Power(t2,2) + 5*Power(t2,3)) + 
                  s1*(-2 + 209*t2 + 505*Power(t2,2) + 7*Power(t2,3) - 
                     6*Power(t2,4)))) + 
            Power(t1,2)*(86 + 68*t2 + 32*Power(t2,2) + 158*Power(t2,3) - 
               4*Power(t2,5) + 
               Power(s,3)*(-36 + 43*t2 - 47*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s1,2)*(82 - 109*t2 + 18*Power(t2,2) + 
                  9*Power(t2,3)) + 
               s1*(101 - 139*t2 - 38*Power(t2,2) - 11*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s,2)*(-149 - 305*t2 + 11*Power(t2,2) - 
                  55*Power(t2,3) + 6*Power(t2,4) + 
                  s1*(27 + 11*t2 + 90*Power(t2,2) - 24*Power(t2,3))) - 
               s*(32 + 341*t2 + 99*Power(t2,2) + 142*Power(t2,3) - 
                  25*Power(t2,4) - 6*Power(t2,5) + 
                  Power(s1,2)*
                   (40 - 40*t2 + 21*Power(t2,2) + 9*Power(t2,3)) + 
                  s1*(38 - 302*t2 + 27*Power(t2,2) - 79*Power(t2,3) + 
                     9*Power(t2,4))))) + 
         Power(s2,6)*(-2 + 24*t2 + 34*s*t2 + 11*Power(t2,2) - 
            49*s*Power(t2,2) - 73*Power(s,2)*Power(t2,2) - 
            36*Power(s,3)*Power(t2,2) + 4*Power(t2,3) - 
            13*s*Power(t2,3) - 45*Power(s,2)*Power(t2,3) + 
            5*Power(s,3)*Power(t2,3) + Power(t2,4) - 39*s*Power(t2,4) + 
            6*Power(s,2)*Power(t2,4) - 4*Power(t2,5) + 6*s*Power(t2,5) - 
            4*Power(t1,6)*(3 + t2) + 
            Power(t1,5)*(21 + 2*Power(s,3) + 127*t2 + 43*Power(t2,2) - 
               2*Power(s,2)*(5 + t2) - s*(55 + 74*t2 + 11*Power(t2,2))) \
+ Power(s1,3)*t1*(20 - 2*Power(t1,4) - 9*t2 - 6*Power(t2,2) + 
               Power(t1,3)*(-14 + 19*t2) - 
               2*Power(t1,2)*(5 - t2 + 11*Power(t2,2)) + 
               t1*(72 + 62*t2 + 36*Power(t2,2))) + 
            Power(t1,4)*(175 + Power(s,3)*(15 - 11*t2) + 743*t2 + 
               349*Power(t2,2) - 12*Power(t2,3) - 
               Power(s,2)*(66 + 38*t2 + 17*Power(t2,2)) + 
               s*(-5 + 56*t2 + 124*Power(t2,2) + 26*Power(t2,3))) + 
            Power(t1,3)*(36 + 961*t2 + 1454*Power(t2,2) + 
               333*Power(t2,3) + 25*Power(t2,4) + 
               Power(s,3)*(10 - 31*t2 + 6*Power(t2,2)) + 
               Power(s,2)*(19 + 369*t2 + 170*Power(t2,2) + 
                  24*Power(t2,3)) + 
               s*(241 + 1446*t2 + 1394*Power(t2,2) + 194*Power(t2,3) - 
                  17*Power(t2,4))) - 
            Power(t1,2)*(77 + 677*t2 + 623*Power(t2,2) + 
               751*Power(t2,3) + 404*Power(t2,4) + 32*Power(t2,5) + 
               Power(s,3)*(6 + 30*t2 - 25*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(2 - 305*t2 - 130*Power(t2,2) + 
                  64*Power(t2,3) + 9*Power(t2,4)) + 
               s*(49 - 151*t2 - 657*Power(t2,2) + 459*Power(t2,3) + 
                  169*Power(t2,4) - 2*Power(t2,5))) + 
            t1*(-1 - 6*t2 - 337*Power(t2,2) + 116*Power(t2,3) + 
               Power(t2,4) + 36*Power(t2,5) + 
               3*Power(s,3)*t2*(-16 - 9*t2 + 3*Power(t2,2)) + 
               Power(s,2)*t2*
                (-138 - 139*t2 - 145*Power(t2,2) + 11*Power(t2,3)) + 
               s*(14 - 291*t2 - 39*Power(t2,2) - 367*Power(t2,3) - 
                  136*Power(t2,4) + 12*Power(t2,5))) + 
            Power(s1,2)*(12 - 16*t2 + (1 - 2*s)*Power(t2,2) - 
               3*(-1 + s)*Power(t2,3) + 
               Power(t1,4)*(79 + s*(59 - 49*t2) + 30*t2 - 
                  29*Power(t2,2)) + Power(t1,5)*(6*s - 2*(4 + t2)) + 
               Power(t1,3)*(245 + 739*t2 + 145*Power(t2,2) + 
                  31*Power(t2,3) + 2*s*(78 - 32*t2 + 21*Power(t2,2))) + 
               Power(t1,2)*(26 + 15*t2 - 507*Power(t2,2) - 
                  153*Power(t2,3) + 
                  3*s*(-1 - 43*t2 - 31*Power(t2,2) + Power(t2,3))) + 
               t1*(79 - 88*t2 - 35*Power(t2,2) - 17*Power(t2,3) + 
                  s*(-20 - 11*t2 - 96*Power(t2,2) + 30*Power(t2,3)))) + 
            s1*(18 + 25*(-1 + 2*s)*t2 + 
               (11 - 42*s + 35*Power(s,2))*Power(t2,2) + 
               (-25 + 22*s - 4*Power(s,2))*Power(t2,3) + 
               13*(1 + s)*Power(t2,4) + 
               Power(t1,5)*(86 - 6*Power(s,2) + 108*t2 + 
                  13*Power(t2,2) + 2*s*(9 + 2*t2)) + 
               Power(t1,4)*(-102 - 223*t2 - 207*Power(t2,2) - 
                  28*Power(t2,3) + Power(s,2)*(-60 + 41*t2) + 
                  s*(38 + 30*t2 + 47*Power(t2,2))) - 
               Power(t1,3)*(354 + 1777*t2 + 1502*Power(t2,2) + 
                  163*Power(t2,3) - 11*Power(t2,4) + 
                  Power(s,2)*(139 - 85*t2 + 27*Power(t2,2)) + 
                  s*(223 + 1260*t2 + 376*Power(t2,2) + 60*Power(t2,3))) \
+ t1*(14 + 215*t2 - 21*Power(t2,2) + 64*Power(t2,3) + 15*Power(t2,4) + 
                  2*Power(s,2)*t2*(11 + 72*t2 - 12*Power(t2,2)) + 
                  s*(32 + 190*t2 + 166*Power(t2,2) + 213*Power(t2,3) - 
                     18*Power(t2,4))) + 
               Power(t1,2)*(58 + 316*t2 + 368*Power(t2,2) + 
                  900*Power(t2,3) + 169*Power(t2,4) + 
                  Power(s,2)*
                   (-39 + 20*t2 + 34*Power(t2,2) - 4*Power(t2,3)) + 
                  s*(81 - 596*t2 + 289*Power(t2,2) + 277*Power(t2,3) + 
                     11*Power(t2,4))))) + 
         Power(s2,7)*(6 - 31*t2 + 10*s*t2 + 76*Power(t2,2) + 
            60*s*Power(t2,2) + 51*Power(s,2)*Power(t2,2) + 
            24*Power(s,3)*Power(t2,2) - 32*Power(t2,3) + 
            63*Power(s,2)*Power(t2,3) - Power(s,3)*Power(t2,3) + 
            19*Power(t2,4) + 100*s*Power(t2,4) + 
            3*Power(s,2)*Power(t2,4) - 12*Power(t2,5) - 
            6*s*Power(t2,5) + 2*Power(t1,5)*(14 + 12*t2 + Power(t2,2)) + 
            Power(s1,3)*(-4 + 10*Power(t1,4) + 
               Power(t1,3)*(46 - 36*t2) + 2*t2 + Power(t2,2) + 
               4*Power(t1,2)*(4 - 5*t2 + 6*Power(t2,2)) - 
               t1*(29 + 12*t2 + 32*Power(t2,2))) + 
            Power(t1,4)*(21 - 4*Power(s,3) - 129*t2 - 155*Power(t2,2) - 
               17*Power(t2,3) + 2*Power(s,2)*(7 + 5*t2) + 
               s*(49 + 110*t2 + 37*Power(t2,2))) + 
            Power(t1,3)*(-38 - 521*t2 - 647*Power(t2,2) - 
               35*Power(t2,3) + 15*Power(t2,4) + 
               Power(s,3)*(-8 + 11*t2) + 
               Power(s,2)*(39 + 54*t2 + 6*Power(t2,2)) - 
               s*(-14 + 34*t2 + 187*Power(t2,2) + 87*Power(t2,3))) - 
            Power(t1,2)*(-17 + 167*t2 + 4*Power(s,3)*(-5 + t2)*t2 + 
               881*Power(t2,2) + 304*Power(t2,3) - 18*Power(t2,4) + 
               2*Power(t2,5) + 
               2*Power(s,2)*(6 + 77*t2 + 85*Power(t2,2) + 
                  13*Power(t2,3)) + 
               s*(23 + 500*t2 + 1101*Power(t2,2) + 299*Power(t2,3) - 
                  44*Power(t2,4))) + 
            t1*(2 + 152*t2 + 345*Power(t2,2) + 194*Power(t2,3) + 
               294*Power(t2,4) + 28*Power(t2,5) + 
               Power(s,3)*t2*(12 + 17*t2 - 5*Power(t2,2)) + 
               Power(s,2)*t2*
                (-52 - 17*t2 + 57*Power(t2,2) + 4*Power(t2,3)) + 
               s*(-2 + 169*t2 - 326*Power(t2,2) + 182*Power(t2,3) + 
                  180*Power(t2,4) - 4*Power(t2,5))) - 
            Power(s1,2)*(18 + 4*Power(t1,4)*(-3 + 6*s - 2*t2) + 
               (-17 + 10*s)*t2 - (8 + 41*s)*Power(t2,2) + 
               2*(-3 + 5*s)*Power(t2,3) + 
               Power(t1,3)*(44 + s*(123 - 73*t2) + 134*t2 - 
                  13*Power(t2,2)) + 
               Power(t1,2)*(2 + 610*t2 + 169*Power(t2,2) + 
                  15*Power(t2,3) + 2*s*(65 - 15*t2 + 18*Power(t2,2))) + 
               t1*(-8 + 97*t2 - 206*Power(t2,2) - 157*Power(t2,3) + 
                  s*(10 + 3*t2 - 115*Power(t2,2) + 3*Power(t2,3)))) + 
            s1*(6 - 2*(25 + 28*s)*t2 + 
               (5 - 19*s - 49*Power(s,2))*Power(t2,2) + 
               (-4 - 55*s + 4*Power(s,2))*Power(t2,3) - 
               (13 + 6*s)*Power(t2,4) + 
               2*Power(t1,4)*
                (-32 + 9*Power(s,2) - 86*t2 - 27*Power(t2,2) - 
                  3*s*(5 + 3*t2)) + 
               Power(t1,3)*(84 + Power(s,2)*(75 - 48*t2) + 260*t2 + 
                  309*Power(t2,2) + 102*Power(t2,3) + 
                  s*(-63 + 24*t2 - 26*Power(t2,2))) + 
               Power(t1,2)*(79 + 782*t2 + 1247*Power(t2,2) + 
                  242*Power(t2,3) - 37*Power(t2,4) + 
                  2*Power(s,2)*(28 - 10*t2 + 9*Power(t2,2)) + 
                  s*(-34 + 766*t2 + 438*Power(t2,2) + 56*Power(t2,3))) + 
               t1*(31 - 228*t2 + 32*Power(t2,2) - 458*Power(t2,3) - 
                  185*Power(t2,4) + 
                  2*Power(s,2)*t2*(3 - 31*t2 + 2*Power(t2,2)) - 
                  s*(52 - 124*t2 + 67*Power(t2,2) + 227*Power(t2,3) + 
                     15*Power(t2,4))))) + 
         Power(s2,3)*t1*(-10 + 2*Power(t1,7) + 27*t2 - 40*s*t2 + 
            30*Power(s,2)*t2 + 12*Power(s,3)*t2 - 31*Power(t2,2) + 
            89*s*Power(t2,2) - 40*Power(s,2)*Power(t2,2) - 
            6*Power(s,3)*Power(t2,2) + 13*Power(t2,3) - 
            44*s*Power(t2,3) + 6*Power(s,2)*Power(t2,3) + Power(t2,4) - 
            5*s*Power(t2,4) + 4*Power(s,2)*Power(t2,4) + 
            Power(t1,6)*(-14 + 4*Power(s,2) + s*(26 - 11*s1) - s1 + 
               7*Power(s1,2) + 30*t2) + 
            Power(t1,5)*(-516 + 7*Power(s,3) + 2*Power(s1,3) - 533*t2 - 
               138*Power(t2,2) + Power(s,2)*(-45 - 15*s1 + 8*t2) - 
               Power(s1,2)*(83 + 15*t2) + s1*(427 + 258*t2) + 
               s*(6*Power(s1,2) + 2*s1*(80 + 3*t2) - 3*(96 + 53*t2))) + 
            Power(t1,4)*(146 + 5*Power(s,3)*(-1 + t2) + 196*t2 + 
               511*Power(t2,2) + 134*Power(t2,3) - 
               Power(s1,3)*(45 + 8*t2) + 
               Power(s1,2)*(37 + 120*t2 + 3*Power(t2,2)) - 
               2*s1*(-7 + 91*t2 + 115*Power(t2,2)) - 
               Power(s,2)*(401 + 93*t2 + 4*Power(t2,2) + 
                  s1*(22 + 4*t2)) + 
               s*(-655 - 653*t2 - Power(t2,2) + 
                  Power(s1,2)*(81 + 6*t2) + 
                  s1*(510 + 37*t2 - 18*Power(t2,2)))) - 
            Power(t1,3)*(-395 - 455*t2 - 28*Power(t2,2) + 
               70*Power(t2,3) + 28*Power(t2,4) + 
               Power(s1,3)*(26 - 18*t2 - 3*Power(t2,2)) + 
               Power(s,3)*(83 + 7*t2 + 7*Power(t2,2)) + 
               Power(s1,2)*(43 - 58*t2 - 11*Power(t2,2) + Power(t2,3)) + 
               s1*(229 + 56*t2 + 63*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s,2)*(312 + 233*t2 - 162*Power(t2,2) + 
                  8*Power(t2,3) + s1*(36 - 26*t2 - 15*Power(t2,2))) + 
               s*(-224 + 123*t2 - 386*Power(t2,2) - 141*Power(t2,3) + 
                  Power(s1,2)*(-158 + 34*t2 + 27*Power(t2,2)) + 
                  s1*(-149 + 193*t2 + 172*Power(t2,2) - 24*Power(t2,3)))) \
+ Power(t1,2)*(-(Power(s1,2)*
                  (66 - 85*t2 + 16*Power(t2,2) + 3*Power(t2,3))) - 
               Power(s,3)*(33 + 86*t2 - 63*Power(t2,2) + 
                  5*Power(t2,3)) + 
               s1*(-70 + 102*t2 + 49*Power(t2,2) - 4*Power(t2,3) + 
                  3*Power(t2,4)) - 
               2*(43 - 25*t2 + 98*Power(t2,2) + 50*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s,2)*(150 + 21*t2 + 63*Power(t2,2) + 
                  13*Power(t2,3) + 
                  s1*(-49 + 38*t2 - 26*Power(t2,2) + 4*Power(t2,3))) + 
               s*(245 + 220*t2 + 71*Power(t2,2) + 51*Power(t2,3) - 
                  5*Power(t2,4) + 
                  Power(s1,2)*
                   (48 - 48*t2 + 17*Power(t2,2) + 3*Power(t2,3)) - 
                  s1*(-42 + 239*t2 + 47*Power(t2,2) + 3*Power(t2,3) + 
                     Power(t2,4)))) + 
            t1*(-49 - 33*t2 + 2*Power(t2,2) + 41*Power(t2,3) + 
               17*Power(t2,4) + 2*Power(t2,5) + 
               Power(s,3)*(24 - 45*t2 + 59*Power(t2,2) - 
                  11*Power(t2,3)) - 
               s1*(45 - 118*t2 + 57*Power(t2,2) + 12*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s,2)*(81 + 145*t2 - 175*Power(t2,2) + 
                  55*Power(t2,3) + 3*Power(t2,4) + 
                  s1*(-7 + 10*t2 - 28*Power(t2,2) + 4*Power(t2,3))) + 
               s*(s1*(35 - 103*t2 + 71*Power(t2,2) - 7*Power(t2,3) + 
                     4*Power(t2,4)) - 
                  2*(11 - 7*t2 - 58*Power(t2,2) + 27*Power(t2,3) + 
                     10*Power(t2,4) + Power(t2,5))))))*R1q(s2))/
     (Power(-1 + s2,3)*s2*Power(s2 - t1,3)*(-s + s2 - t1)*(-1 + t1)*
       (s - s1 + t2)*(-1 + s2 - t1 + t2)*Power(-t1 + s2*t2,2)) + 
    (8*(-2*Power(s2,11)*Power(t2,2)*
          (-1 + 3*Power(t1,2) + t1*(2 - s1 + t2)) - 
         Power(s2,9)*(-2*Power(t2,2)*(-2 + 6*t2 + 15*Power(t2,2)) + 
            Power(t1,4)*(6 + 56*t2 + 52*Power(t2,2)) + 
            t1*t2*(4 + (56 + 6*s - 3*s1)*t2 + 
               (97 + 56*s - 24*s1)*Power(t2,2) - 
               2*(-46 + 6*s + 15*s1)*Power(t2,3) + 30*Power(t2,4)) - 
            2*Power(t1,2)*(1 + 2*(9 + 5*s)*t2 + 
               (93 + 23*s - 6*Power(s,2))*Power(t2,2) + 
               (128 + 45*s)*Power(t2,3) - 5*Power(t2,4) + 
               Power(s1,3)*(-1 + 5*t2) + 
               Power(s1,2)*(-3 + (4 + s)*t2 - Power(t2,2)) - 
               s1*t2*(4 + s*(3 - 10*t2) + 43*t2 + 62*Power(t2,2))) + 
            Power(t1,3)*(4 + 10*Power(s1,3) + (58 + 40*s)*t2 + 
               2*(-4 + 53*s)*Power(t2,2) - 83*Power(t2,3) + 
               Power(s1,2)*(-6*s + 2*t2) - 
               s1*(2 + 6*(8 + s)*t2 + 107*Power(t2,2)))) + 
         2*Power(s2,10)*(Power(s1,3)*Power(t1,2) - 
            2*s1*t1*t2*(t1 + 6*t1*t2 - t2*(1 + 3*t2)) + 
            t2*(t2*(1 + 6*t2) + 2*Power(t1,3)*(3 + 7*t2) + 
               2*Power(t1,2)*(2 + (8 + 5*s)*t2 - 5*Power(t2,2)) - 
               t1*(2 + (9 + 5*s)*t2 - (-15 + s)*Power(t2,2) + 
                  6*Power(t2,3)))) + 
         Power(s2,8)*(4*Power(t1,5)*(7 + 26*t2 + 12*Power(t2,2)) + 
            2*Power(t2,2)*(-1 - 8*t2 + 15*Power(t2,2) + 
               20*Power(t2,3)) - 
            t1*t2*(-8 + (27 - 14*s + 8*s1)*t2 + 
               (195 + 35*s - 13*s1)*Power(t2,2) + 
               (221 + 130*s - 60*s1)*Power(t2,3) - 
               10*(-15 + 3*s + 4*s1)*Power(t2,4) + 40*Power(t2,5)) + 
            Power(t1,2)*(2 + 2*(38 + 9*s)*t2 + 
               (421 + 127*s - 38*Power(s,2))*Power(t2,2) + 
               (685 + 132*s - 56*Power(s,2))*Power(t2,3) + 
               (743 + 158*s)*Power(t2,4) + 44*Power(t2,5) + 
               Power(s1,3)*(-4 - t2 + 20*Power(t2,2)) + 
               Power(s1,2)*(6 - 2*(21 + 5*s)*t2 + 
                  (1 + 12*s)*Power(t2,2) - 8*Power(t2,3)) + 
               s1*(6 + (-22 + 8*s)*t2 - 
                  2*(-7 - 13*s + Power(s,2))*Power(t2,2) + 
                  (-293 + 96*s)*Power(t2,3) - 266*Power(t2,4))) + 
            Power(t1,4)*(28 + 20*Power(s1,3) + 44*t2 - 
               231*Power(t2,2) - 107*Power(t2,3) + 
               6*Power(s,2)*(s1 + t2) + Power(s1,2)*(2 + 8*t2) - 
               2*s1*(12 + 107*t2 + 114*Power(t2,2)) + 
               s*(20 - 24*Power(s1,2) + 212*t2 + 225*Power(t2,2) - 
                  2*s1*(3 + 11*t2))) - 
            Power(t1,3)*(18 + 282*t2 + 1023*Power(t2,2) + 
               447*Power(t2,3) - 62*Power(t2,4) - 
               2*Power(s,2)*t2*(9 - s1 + 46*t2) + 
               Power(s1,3)*(4 + 51*t2) + 
               Power(s1,2)*(-22 + 38*t2 - 31*Power(t2,2)) - 
               2*s1*(2 + 70*t2 + 275*Power(t2,2) + 219*Power(t2,3)) + 
               s*(10 + Power(s1,2)*(14 - 28*t2) + 104*t2 + 
                  400*Power(t2,2) + 381*Power(t2,3) + 
                  2*s1*(3 + 9*t2 + 53*Power(t2,2))))) - 
         Power(s2,7)*(Power(t1,6)*(52 + 96*t2 + 22*Power(t2,2)) - 
            2*Power(t2,2)*(1 - 5*t2 - 12*Power(t2,2) + 20*Power(t2,3) + 
               15*Power(t2,4)) + 
            t1*t2*(-4 + (-90 - 14*s + 7*s1)*t2 + 
               (80 - 46*s + 24*s1)*Power(t2,2) + 
               (309 + 83*s - 22*s1)*Power(t2,3) + 
               2*(137 + 80*s - 40*s1)*Power(t2,4) - 
               10*(-14 + 4*s + 3*s1)*Power(t2,5) + 30*Power(t2,6)) + 
            Power(t1,2)*(6 + (-26 + 38*s)*t2 + 
               (-273 + 62*s - 42*Power(s,2))*Power(t2,2) + 
               (-1159 - 518*s + 160*Power(s,2) + 2*Power(s,3))*
                Power(t2,3) + 
               (-1178 - 75*s + 110*Power(s,2))*Power(t2,4) - 
               (1071 + 128*s)*Power(t2,5) - 86*Power(t2,6) + 
               Power(s1,3)*t2*(14 - 9*t2 - 20*Power(t2,2)) + 
               Power(s1,2)*(-12 + 7*t2 + (71 + 23*s)*Power(t2,2) - 
                  7*(-7 + 4*s)*Power(t2,3) + 12*Power(t2,4)) + 
               s1*(6 - 2*(35 + 13*s)*t2 + 
                  2*(-29 + 33*s + 7*Power(s,2))*Power(t2,2) + 
                  6*(6 - 21*s + Power(s,2))*Power(t2,3) + 
                  (435 - 192*s)*Power(t2,4) + 304*Power(t2,5))) + 
            Power(t1,5)*(32 - 2*Power(s,3) + 20*Power(s1,3) - 213*t2 - 
               383*Power(t2,2) - 47*Power(t2,3) + 
               4*Power(s1,2)*(2 + 3*t2) + 
               2*Power(s,2)*(3 + 9*s1 + 7*t2) - 
               s1*(107 + 456*t2 + 252*Power(t2,2)) + 
               s*(106 - 36*Power(s1,2) + 450*t2 + 243*Power(t2,2) - 
                  2*s1*(11 + 15*t2))) + 
            Power(t1,4)*(-126 - 1326*t2 + 4*Power(s,3)*t2 - 
               2045*Power(t2,2) - 252*Power(t2,3) + 14*Power(t2,4) - 
               Power(s1,3)*(51 + 104*t2) + 
               2*Power(s,2)*(6 + s1*(8 - 21*t2) + 91*t2 + 
                  94*Power(t2,2)) + 
               Power(s1,2)*(20 - 36*t2 + 111*Power(t2,2)) + 
               s1*(66 + 744*t2 + 1631*Power(t2,2) + 732*Power(t2,3)) - 
               s*(56 + Power(s1,2)*(8 - 129*t2) + 552*t2 + 
                  1364*Power(t2,2) + 643*Power(t2,3) + 
                  2*s1*(23 + 77*t2 + 129*Power(t2,2)))) + 
            Power(t1,3)*(32 + 553*t2 + 1889*Power(t2,2) + 
               24*Power(s,3)*Power(t2,2) + 3280*Power(t2,3) + 
               1561*Power(t2,4) + 47*Power(t2,5) + 
               Power(s1,3)*(-29 + 34*t2 + 97*Power(t2,2)) - 
               Power(s1,2)*(24 + 175*t2 - 18*Power(t2,2) + 
                  109*Power(t2,3)) + 
               s1*(11 + 16*t2 - 212*Power(t2,2) - 1518*Power(t2,3) - 
                  747*Power(t2,4)) - 
               2*Power(s,2)*t2*
                (48 + 119*t2 + 168*Power(t2,2) + 2*s1*(-2 + 9*t2)) + 
               s*(6 + 108*t2 + 41*Power(t2,2) + 915*Power(t2,3) + 
                  512*Power(t2,4) + 
                  Power(s1,2)*(-4 + 27*t2 - 15*Power(t2,2)) + 
                  2*s1*(-8 + 89*t2 + 210*Power(t2,2) + 215*Power(t2,3))))\
) + Power(t1,4)*(-32*Power(t1,7) - 
            4*Power(t1,6)*(-112 + 4*Power(s,2) + 4*Power(s1,2) - 
               4*s*(4 + 2*s1 - t2) - 4*s1*(-16 + t2) - 10*t2 - 
               5*Power(t2,2)) + 
            Power(t2,4)*(-3*Power(s,2)*t2*(1 - 3*t2 + 2*Power(t2,2)) - 
               2*(-3 + 8*t2 - 6*Power(t2,2) + Power(t2,3)) + 
               Power(s,3)*(12 - 8*t2 - 3*Power(t2,2) + 2*Power(t2,3)) + 
               2*s*(-7 + 16*t2 - 12*Power(t2,2) + 3*Power(t2,3))) + 
            t1*Power(t2,2)*(-42 + (115 - 6*s1)*t2 + 
               (-71 + 16*s1)*Power(t2,2) + (13 - 22*s1)*Power(t2,3) + 
               (-13 + 12*s1)*Power(t2,4) - 4*Power(t2,5) + 
               Power(s,3)*(-96 + 64*t2 + 42*Power(t2,2) - 
                  15*Power(t2,3) - 5*Power(t2,4)) + 
               Power(s,2)*t2*
                (24 + 2*(-17 + s1)*t2 + (26 - 17*s1)*Power(t2,2) + 
                  (15 + 8*s1)*Power(t2,3) - 2*Power(t2,4)) + 
               s*(112 - 244*t2 - 3*(-45 + 4*s1)*Power(t2,2) + 
                  4*(-5 + 9*s1)*Power(t2,3) + (9 - 24*s1)*Power(t2,4) + 
                  6*Power(t2,5))) + 
            Power(t1,2)*(48 + 12*(-14 + 5*s1)*t2 - 
               2*(-16 + 62*s1 + 3*Power(s1,2))*Power(t2,2) + 
               (-34 + 127*s1 + Power(s1,2))*Power(t2,3) + 
               (107 - 60*s1 + 15*Power(s1,2))*Power(t2,4) + 
               (36 - 9*s1 - 10*Power(s1,2))*Power(t2,5) + 
               (29 - 2*s1)*Power(t2,6) + 
               Power(s,3)*(192 - 128*t2 - 192*Power(t2,2) + 
                  30*Power(t2,3) + 35*Power(t2,4) + 7*Power(t2,5)) + 
               Power(s,2)*t2*
                (-48 - 2*(71 + 8*s1)*t2 + (61 + 124*s1)*Power(t2,2) - 
                  (37 + 48*s1)*Power(t2,3) - 
                  (21 + 17*s1)*Power(t2,4) + 8*Power(t2,5)) + 
               s*(-224 + 428*t2 + 12*(4 + 7*s1)*Power(t2,2) + 
                  (-65 - 270*s1 + 6*Power(s1,2))*Power(t2,3) + 
                  (-87 + 118*s1 - 11*Power(s1,2))*Power(t2,4) + 
                  (-43 + 40*s1 + 10*Power(s1,2))*Power(t2,5) - 
                  (19 + 4*s1)*Power(t2,6))) + 
            Power(t1,5)*(-64*Power(s,3) + 
               4*Power(s,2)*(76 + 32*s1 - 18*t2 + 3*Power(t2,2)) + 
               4*Power(s1,2)*(-4 + 14*t2 + 3*Power(t2,2)) - 
               t2*(568 + 226*t2 + 25*Power(t2,2)) + 
               s1*(-256 + 180*t2 + 120*Power(t2,2) - 19*Power(t2,3)) + 
               s*(800 - 64*Power(s1,2) + 100*t2 - 20*Power(t2,2) + 
                  19*Power(t2,3) - 8*s1*(56 - 2*t2 + 3*Power(t2,2)))) + 
            Power(t1,4)*(-560 + 384*t2 + 332*Power(t2,2) + 
               322*Power(t2,3) + 39*Power(t2,4) + 
               Power(s1,3)*t2*(20 - 4*t2 - 3*Power(t2,2)) + 
               Power(s,3)*t2*(84 + 32*t2 + 3*Power(t2,2)) + 
               s1*(176 + 236*t2 - 40*Power(t2,2) - 117*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s1,2)*(-80 - 40*t2 + 6*Power(t2,2) - 
                  33*Power(t2,3) + 4*Power(t2,4)) + 
               Power(s,2)*(880 - 392*t2 - 50*Power(t2,2) + 
                  31*Power(t2,3) + 4*Power(t2,4) - 
                  s1*(-96 + 196*t2 + 68*Power(t2,2) + 9*Power(t2,3))) + 
               s*(336 - 372*t2 - 540*Power(t2,2) - 33*Power(t2,3) - 
                  17*Power(t2,4) + 
                  Power(s1,2)*
                   (-96 + 92*t2 + 40*Power(t2,2) + 9*Power(t2,3)) + 
                  2*s1*(-208 + 160*t2 + 94*Power(t2,2) + Power(t2,3) - 
                     4*Power(t2,4)))) + 
            Power(t1,3)*(96 + 152*t2 - 116*Power(t2,2) - 
               170*Power(t2,3) - 193*Power(t2,4) - 51*Power(t2,5) + 
               Power(s1,3)*t2*
                (12 - 12*t2 - 5*Power(t2,2) + 4*Power(t2,3)) - 
               Power(s,3)*(-288 + 20*t2 + 48*Power(t2,2) + 
                  49*Power(t2,3) + 7*Power(t2,4)) + 
               Power(s1,2)*(-48 + 80*t2 - 76*Power(t2,2) + 
                  56*Power(t2,3) + 7*Power(t2,4) - 2*Power(t2,5)) + 
               s1*(48 - 12*t2 - 68*Power(t2,2) - 17*Power(t2,3) + 
                  46*Power(t2,4) + 9*Power(t2,5)) + 
               s*(-624 + (52 + 336*s1 - 60*Power(s1,2))*t2 + 
                  8*(24 + 11*s1 + 13*Power(s1,2))*Power(t2,2) + 
                  (163 - 252*s1 - 63*Power(s1,2))*Power(t2,3) + 
                  (149 - 18*s1 - 15*Power(s1,2))*Power(t2,4) + 
                  (11 + 12*s1)*Power(t2,5)) + 
               Power(s,2)*(464 - 192*t2 - 348*Power(t2,2) + 
                  220*Power(t2,3) - 31*Power(t2,4) - 10*Power(t2,5) + 
                  s1*(32 - 188*t2 + 4*Power(t2,2) + 117*Power(t2,3) + 
                     18*Power(t2,4))))) + 
         Power(s2,6)*(4*Power(t1,7)*(12 + 11*t2 + Power(t2,2)) + 
            2*Power(t2,3)*(3 - 9*t2 - 8*Power(t2,2) + 15*Power(t2,3) + 
               6*Power(t2,4)) - 
            t1*t2*(4 + (-37 + 12*s)*t2 - (181 + 57*s)*Power(t2,2) + 
               (115 - 52*s)*Power(t2,3) + 2*(131 + 51*s)*Power(t2,4) + 
               2*(98 + 55*s)*Power(t2,5) + (74 - 30*s)*Power(t2,6) + 
               12*Power(t2,7)) + 
            Power(t1,6)*(-65 - 4*Power(s,3) - 445*t2 - 
               221*Power(t2,2) + 7*Power(t2,3) + 
               2*Power(s,2)*(7 + 5*t2) + 
               9*s*(25 + 54*t2 + 15*Power(t2,2))) + 
            Power(s1,3)*Power(t1,2)*
             (10*Power(t1,4) - 2*Power(t1,3)*(62 + 53*t2) + 
               Power(t2,2)*(-16 + 13*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(-66 + 185*t2 + 182*Power(t2,2)) + 
               t1*(26 + 45*t2 - 70*Power(t2,2) - 85*Power(t2,3))) + 
            Power(t1,5)*(-547 - 2709*t2 - 1804*Power(t2,2) - 
               126*Power(t2,3) - 91*Power(t2,4) + 
               2*Power(s,3)*(-2 + 9*t2) + 
               3*Power(s,2)*(36 + 124*t2 + 63*Power(t2,2)) - 
               s*(242 + 1615*t2 + 2163*Power(t2,2) + 549*Power(t2,3))) + 
            Power(t1,4)*(229 + 2099*t2 + 6325*Power(t2,2) + 
               6647*Power(t2,3) + 1646*Power(t2,4) + 107*Power(t2,5) + 
               4*Power(s,3)*t2*(13 + 11*t2) - 
               Power(s,2)*(40 + 396*t2 + 1107*Power(t2,2) + 
                  592*Power(t2,3)) + 
               s*(13 - 304*t2 + 705*Power(t2,2) + 2342*Power(t2,3) + 
                  623*Power(t2,4))) + 
            Power(t1,2)*t2*(-154 - 160*t2 + 670*Power(t2,2) + 
               1381*Power(t2,3) + 1167*Power(t2,4) + 839*Power(t2,5) + 
               68*Power(t2,6) - 2*Power(s,3)*Power(t2,2)*(3 + 4*t2) + 
               Power(s,2)*t2*
                (96 + 66*t2 - 281*Power(t2,2) - 120*Power(t2,3)) + 
               s*(-44 - t2 - 192*Power(t2,2) + 987*Power(t2,3) - 
                  127*Power(t2,4) + 32*Power(t2,5))) - 
            Power(t1,3)*(1 + 123*t2 + 1583*Power(t2,2) + 
               3849*Power(t2,3) + 4633*Power(t2,4) + 2182*Power(t2,5) + 
               105*Power(t2,6) + 
               2*Power(s,3)*Power(t2,2)*(-1 + 45*t2) - 
               Power(s,2)*t2*
                (-78 + 160*t2 + 730*Power(t2,2) + 469*Power(t2,3)) + 
               s*(-12 - 327*t2 + 426*Power(t2,2) - 176*Power(t2,3) + 
                  658*Power(t2,4) + 297*Power(t2,5))) + 
            Power(s1,2)*Power(t1,2)*
             (Power(t1,4)*(12 - 24*s + 8*t2) - 
               t2*(-42 + (13 + 6*s)*t2 + (49 + 9*s)*Power(t2,2) + 
                  (73 - 32*s)*Power(t2,3) + 8*Power(t2,4)) + 
               Power(t1,3)*(-7 + 90*t2 + 167*Power(t2,2) + 
                  s*(105 + 199*t2)) - 
               Power(t1,2)*(176 + 200*t2 + 112*Power(t2,2) + 
                  341*Power(t2,3) + 3*s*(-25 - 22*t2 + 50*Power(t2,2))) \
+ t1*(-77 + 208*t2 + 346*Power(t2,2) + 174*Power(t2,3) + 
                  121*Power(t2,4) + 
                  s*(12 + 27*t2 - 52*Power(t2,2) - 75*Power(t2,3)))) + 
            s1*t1*(Power(t2,2)*
                (6 - 25*t2 - 24*Power(t2,2) + 18*Power(t2,3) + 
                  60*Power(t2,4) + 12*Power(t2,5)) - 
               t1*(12 + (-31 + 12*s)*t2 - 
                  2*(25 - 17*s + 6*Power(s,2))*Power(t2,2) + 
                  (-245 + 186*s + 43*Power(s,2))*Power(t2,3) + 
                  2*(115 - 83*s + 2*Power(s,2))*Power(t2,4) + 
                  (355 - 208*s)*Power(t2,5) + 196*Power(t2,6)) + 
               2*Power(t1,5)*
                (9*Power(s,2) - 3*s*(5 + 3*t2) - 
                  2*(57 + 126*t2 + 35*Power(t2,2))) + 
               Power(t1,4)*(318 + 1972*t2 - 111*Power(s,2)*t2 + 
                  2540*Power(t2,2) + 629*Power(t2,3) - 
                  2*s*(60 + 197*t2 + 169*Power(t2,2))) + 
               Power(t1,3)*(38 + 139*t2 - 2000*Power(t2,2) - 
                  3606*Power(t2,3) - 938*Power(t2,4) + 
                  Power(s,2)*(16 - 107*t2 - 42*Power(t2,2)) + 
                  2*s*(40 + 478*t2 + 682*Power(t2,2) + 429*Power(t2,3))) \
+ Power(t1,2)*(-44 - 356*t2 - 498*Power(t2,2) + 243*Power(t2,3) + 
                  1782*Power(t2,4) + 697*Power(t2,5) + 
                  Power(s,2)*t2*(54 + 32*t2 + 135*Power(t2,2)) - 
                  2*s*(7 - 50*t2 + 38*Power(t2,2) + 636*Power(t2,3) + 
                     315*Power(t2,4))))) + 
         Power(s2,4)*(4*Power(t1,9) + 
            2*Power(t2,5)*(1 - 2*t2 + Power(t2,3)) + 
            t1*Power(t2,3)*(-36 + (67 - 24*s + 6*s1)*t2 + 
               (31 + 51*s - 11*s1)*Power(t2,2) - 
               2*(11 + s)*Power(t2,3) + (-35 - 23*s + s1)*Power(t2,4) + 
               (-13 - 6*s + 4*s1)*Power(t2,5) + 2*(-1 + s)*Power(t2,6)) \
+ Power(t1,2)*t2*(64 + 3*(-35 + 2*s + 4*s1)*t2 - 
               (394 + 150*Power(s,2) + 59*s1 + 18*Power(s1,2) - 
                  3*s*(1 + 36*s1))*Power(t2,2) + 
               (-170 + 22*Power(s,3) + 160*s1 + 31*Power(s1,2) + 
                  Power(s,2)*(343 + 24*s1) - 
                  2*s*(139 + 95*s1 + 3*Power(s1,2)))*Power(t2,3) + 
               (442 - 27*Power(s,3) + 21*s1 - 9*Power(s1,2) - 
                  Power(s,2)*(109 + 17*s1) + 
                  s*(181 - 22*s1 + 7*Power(s1,2)))*Power(t2,4) + 
               (255 - 8*Power(s,3) - 108*s1 - 4*Power(s1,2) + 
                  Power(s,2)*(-151 + 6*s1) + 
                  s*(341 + 24*s1 + 4*Power(s1,2)))*Power(t2,5) + 
               (-32*Power(s,2) - 56*(-4 + s1) + 3*s*(-35 + 16*s1))*
                Power(t2,6) - 2*(9*s + 5*(-7 + s1))*Power(t2,7) + 
               4*Power(t2,8)) + 
            Power(t1,8)*(-127 + 2*Power(s,2) + 2*Power(s1,2) - 47*t2 + 
               20*Power(t2,2) - 2*s1*(70 + 31*t2) + 
               s*(135 - 4*s1 + 62*t2)) - 
            Power(t1,3)*(2 - 371*t2 - 1144*Power(t2,2) + 
               529*Power(t2,3) + 1392*Power(t2,4) + 1932*Power(t2,5) + 
               1454*Power(t2,6) + 525*Power(t2,7) + 21*Power(t2,8) + 
               Power(s1,3)*Power(t2,2)*
                (-22 + 9*t2 + 14*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s,3)*Power(t2,2)*
                (104 - 341*t2 - 8*Power(t2,2) + 93*Power(t2,3)) + 
               Power(s1,2)*t2*
                (24 + 83*t2 + 56*Power(t2,2) - 124*Power(t2,3) - 
                  120*Power(t2,4) - 4*Power(t2,5)) - 
               Power(s,2)*t2*
                (192 - 3*(231 + 2*s1)*t2 - 
                  (252 + 137*s1)*Power(t2,2) + 
                  2*(319 + 65*s1)*Power(t2,3) + 
                  (744 + 87*s1)*Power(t2,4) + 93*Power(t2,5)) + 
               s*(12 + 8*(7 + 24*s1)*t2 + 
                  2*(108 - 161*s1 + 6*Power(s1,2))*Power(t2,2) + 
                  (61 - 1080*s1 - 81*Power(s1,2))*Power(t2,3) + 
                  2*(585 - 125*s1 + 51*Power(s1,2))*Power(t2,4) + 
                  (80 + 878*s1 + 69*Power(s1,2))*Power(t2,5) + 
                  16*(-29 + 13*s1)*Power(t2,6) - 40*Power(t2,7)) - 
               s1*(84 + 110*t2 - 136*Power(t2,2) - 375*Power(t2,3) - 
                  208*Power(t2,4) + 620*Power(t2,5) + 288*Power(t2,6) + 
                  117*Power(t2,7))) + 
            Power(t1,7)*(-1114 - 1130*t2 - 425*Power(t2,2) - 
               333*Power(t2,3) - 28*Power(t2,4) + 
               Power(s,3)*(25 + 11*t2) - Power(s1,3)*(72 + 11*t2) + 
               5*Power(s1,2)*(13 + 36*t2 + 6*Power(t2,2)) + 
               s1*(1092 + 2379*t2 + 904*Power(t2,2) + 43*Power(t2,3)) + 
               Power(s,2)*(159 + 184*t2 + 30*Power(t2,2) - 
                  s1*(122 + 33*t2)) + 
               s*(-895 - 1943*t2 - 780*Power(t2,2) - 43*Power(t2,3) + 
                  Power(s1,2)*(169 + 33*t2) - 
                  2*s1*(101 + 182*t2 + 30*Power(t2,2)))) + 
            Power(t1,6)*(1886 + 9701*t2 + 12420*Power(t2,2) + 
               4223*Power(t2,3) + 622*Power(t2,4) + 77*Power(t2,5) + 
               Power(s,3)*(79 + 104*t2 - 10*Power(t2,2)) + 
               Power(s1,3)*(86 + 349*t2 + 70*Power(t2,2)) + 
               Power(s1,2)*(-76 + t2 - 939*Power(t2,2) - 
                  271*Power(t2,3)) - 
               s1*(-4 + 3514*t2 + 7010*Power(t2,2) + 
                  2379*Power(t2,3) + 108*Power(t2,4)) - 
               Power(s,2)*(608 + 1898*t2 + 1303*Power(t2,2) + 
                  263*Power(t2,3) + s1*(322 + 42*t2 - 90*Power(t2,2))) \
+ s*(-553 + 936*t2 + 3955*Power(t2,2) + 1365*Power(t2,3) + 
                  54*Power(t2,4) + 
                  Power(s1,2)*(245 - 411*t2 - 150*Power(t2,2)) + 
                  2*s1*(526 + 1003*t2 + 1087*Power(t2,2) + 
                     267*Power(t2,3)))) - 
            Power(t1,5)*(6 + 2619*t2 + 9439*Power(t2,2) + 
               13396*Power(t2,3) + 7790*Power(t2,4) + 
               1189*Power(t2,5) + 49*Power(t2,6) + 
               Power(s1,3)*(-266 + 94*t2 + 326*Power(t2,2) + 
                  86*Power(t2,3)) + 
               Power(s,3)*(14 + 309*t2 + 441*Power(t2,2) + 
                  146*Power(t2,3)) - 
               Power(s1,2)*(73 + 1016*t2 + 734*Power(t2,2) + 
                  732*Power(t2,3) + 345*Power(t2,4)) + 
               s1*(658 + 2459*t2 + 1858*Power(t2,2) - 
                  4793*Power(t2,3) - 3230*Power(t2,4) - 222*Power(t2,5)\
) - Power(s,2)*(-229 - 122*t2 + 3143*Power(t2,2) + 2218*Power(t2,3) + 
                  377*Power(t2,4) + 
                  s1*(-4 + 401*t2 + 946*Power(t2,2) + 260*Power(t2,3))) \
+ s*(-471 - 2365*t2 - 5229*Power(t2,2) - 261*Power(t2,3) + 
                  1002*Power(t2,4) - 6*Power(t2,5) + 
                  Power(s1,2)*
                   (76 + 446*t2 + 346*Power(t2,2) + 28*Power(t2,3)) + 
                  2*s1*(-251 + 884*t2 + 2644*Power(t2,2) + 
                     1480*Power(t2,3) + 355*Power(t2,4)))) + 
            Power(t1,4)*(-305 - 1022*t2 - 251*Power(t2,2) + 
               4963*Power(t2,3) + 6855*Power(t2,4) + 5079*Power(t2,5) + 
               1240*Power(t2,6) + 36*Power(t2,7) + 
               Power(s,3)*t2*
                (-262 - 165*t2 + 499*Power(t2,2) + 190*Power(t2,3)) + 
               Power(s1,3)*(-28 - 36*t2 + 70*Power(t2,2) + 
                  95*Power(t2,3) + 34*Power(t2,4)) - 
               Power(s1,2)*(-362 + 233*t2 + 766*Power(t2,2) + 
                  587*Power(t2,3) + 406*Power(t2,4) + 95*Power(t2,5)) + 
               s1*(-88 + 740*t2 + 2130*Power(t2,2) + 957*Power(t2,3) - 
                  1194*Power(t2,4) - 1584*Power(t2,5) - 296*Power(t2,6)) \
+ Power(s,2)*(122 + 388*t2 + 691*Power(t2,2) - 1049*Power(t2,3) - 
                  1722*Power(t2,4) - 251*Power(t2,5) + 
                  s1*(12 + 144*t2 - 154*Power(t2,2) - 571*Power(t2,3) - 
                     392*Power(t2,4))) + 
               s*(116 - 144*t2 + 220*Power(t2,2) - 133*Power(t2,3) - 
                  1766*Power(t2,4) - 292*Power(t2,5) - 21*Power(t2,6) + 
                  Power(s1,2)*
                   (-84 - 270*t2 + 20*Power(t2,2) + 435*Power(t2,3) + 
                     198*Power(t2,4)) + 
                  2*s1*(-22 - 417*t2 - 908*Power(t2,2) + 
                     967*Power(t2,3) + 1391*Power(t2,4) + 182*Power(t2,5)\
)))) - Power(s2,5)*(Power(t1,8)*(22 + 8*t2) - 
            2*Power(t2,4)*(3 - 7*t2 - 2*Power(t2,2) + 6*Power(t2,3) + 
               Power(t2,4)) + 
            t1*Power(t2,2)*(34 + (-91 + 30*s - 12*s1)*t2 + 
               (-127 - 83*s + 29*s1)*Power(t2,2) + 
               (79 - 20*s + 8*s1)*Power(t2,3) + 
               (126 + 68*s - 7*s1)*Power(t2,4) + 
               (77 + 40*s - 24*s1)*Power(t2,5) - 
               2*(-10 + 6*s + s1)*Power(t2,6) + 2*Power(t2,7)) + 
            Power(t1,2)*(-6 + (53 - 36*s + 54*s1)*t2 + 
               (370 + 84*Power(s,2) + s*(89 - 72*s1) - 47*s1 - 
                  12*Power(s1,2))*Power(t2,2) + 
               (360 - 8*Power(s,3) - 86*s1 - 31*Power(s1,2) + 
                  6*Power(s1,3) - 5*Power(s,2)*(61 + 6*s1) + 
                  6*s*(16 + 35*s1 + 2*Power(s1,2)))*Power(t2,3) + 
               (-816 + 21*Power(s,3) - 195*s1 + 23*Power(s1,2) - 
                  5*Power(s1,3) + 5*Power(s,2)*(7 + 9*s1) + 
                  s*(31 + 138*s1 - 11*Power(s1,2)))*Power(t2,4) + 
               (-830 + 12*Power(s,3) + Power(s,2)*(269 - 4*s1) + 
                  268*s1 + 35*Power(s1,2) - 2*Power(s1,3) - 
                  2*s*(437 + 46*s1 + 9*Power(s1,2)))*Power(t2,5) + 
               (-689 + 80*Power(s,2) + 179*s1 + 2*Power(s1,2) - 
                  3*s*(-67 + 44*s1))*Power(t2,6) + 
               (-353 + 22*s + 68*s1)*Power(t2,7) - 26*Power(t2,8)) + 
            Power(t1,7)*(-169 - 2*Power(s,3) + 2*Power(s1,3) - 301*t2 - 
               13*Power(t2,2) + 8*Power(t2,3) + 
               2*Power(s1,2)*(4 + t2) + 2*Power(s,2)*(5 + 3*s1 + t2) - 
               s1*(252 + 280*t2 + 31*Power(t2,2)) + 
               s*(243 - 6*Power(s1,2) + 270*t2 + 31*Power(t2,2) - 
                  2*s1*(9 + 2*t2))) + 
            Power(t1,6)*(-1121 - 2604*t2 - 782*Power(t2,2) - 
               316*Power(t2,3) - 93*Power(t2,4) + 
               Power(s,3)*(6 + 25*t2) - 2*Power(s1,3)*(68 + 27*t2) + 
               Power(s1,2)*(9 + 226*t2 + 115*Power(t2,2)) + 
               s1*(779 + 2900*t2 + 2131*Power(t2,2) + 
                  266*Power(t2,3)) + 
               Power(s,2)*(202 + 342*t2 + 111*Power(t2,2) - 
                  s1*(93 + 104*t2)) + 
               s*(-632 - 2415*t2 - 1793*Power(t2,2) - 238*Power(t2,3) + 
                  Power(s1,2)*(223 + 133*t2) - 
                  2*s1*(92 + 264*t2 + 113*Power(t2,2)))) + 
            Power(t1,5)*(803 + 5644*t2 + 11805*Power(t2,2) + 
               7022*Power(t2,3) + 1153*Power(t2,4) + 135*Power(t2,5) + 
               Power(s,3)*(20 + 119*t2 + 27*Power(t2,2)) + 
               4*Power(s1,3)*(-8 + 92*t2 + 41*Power(t2,2)) - 
               Power(s1,2)*(286 - 11*t2 + 592*Power(t2,2) + 
                  449*Power(t2,3)) - 
               2*s1*(-74 + 387*t2 + 2782*Power(t2,2) + 
                  2119*Power(t2,3) + 271*Power(t2,4)) - 
               Power(s,2)*(172 + 1397*t2 + 1742*Power(t2,2) + 
                  513*Power(t2,3) + s1*(101 + 238*t2 - 49*Power(t2,2))) \
+ s*(-237 - 581*t2 + 2883*Power(t2,2) + 2670*Power(t2,3) + 
                  336*Power(t2,4) + 
                  Power(s1,2)*(261 - 145*t2 - 240*Power(t2,2)) + 
                  2*s1*(292 + 969*t2 + 1100*Power(t2,2) + 
                     467*Power(t2,3)))) + 
            Power(t1,4)*(5 - 671*t2 - 5036*Power(t2,2) - 
               9284*Power(t2,3) - 8351*Power(t2,4) - 2177*Power(t2,5) - 
               95*Power(t2,6) - 
               Power(s,3)*t2*(18 + 279*t2 + 161*Power(t2,2)) + 
               Power(s1,3)*(126 + 32*t2 - 221*Power(t2,2) - 
                  132*Power(t2,3)) + 
               Power(s1,2)*(-103 + 730*t2 + 647*Power(t2,2) + 
                  416*Power(t2,3) + 317*Power(t2,4)) + 
               s1*(-285 - 1072*t2 - 1702*Power(t2,2) + 
                  1934*Power(t2,3) + 3492*Power(t2,4) + 658*Power(t2,5)\
) + Power(s,2)*(-30 - 199*t2 + 857*Power(t2,2) + 2075*Power(t2,3) + 
                  625*Power(t2,4) + 
                  2*s1*(11 + 50*t2 + 196*Power(t2,2) + 157*Power(t2,3))\
) + s*(194 + 457*t2 + 1521*Power(t2,2) + 789*Power(t2,3) - 
                  1268*Power(t2,4) - 204*Power(t2,5) - 
                  Power(s1,2)*
                   (-40 + 88*t2 + 289*Power(t2,2) + 66*Power(t2,3)) - 
                  2*s1*(-77 - 96*t2 + 1143*Power(t2,2) + 
                     1474*Power(t2,3) + 447*Power(t2,4)))) + 
            Power(t1,3)*(-74 - 515*t2 + 2*Power(t2,2) + 
               2069*Power(t2,3) + 3750*Power(t2,4) + 3493*Power(t2,5) + 
               1512*Power(t2,6) + 70*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*(-126 - 11*t2 + 131*Power(t2,2)) + 
               Power(s1,3)*t2*
                (-32 - 7*t2 + 54*Power(t2,2) + 33*Power(t2,3)) - 
               Power(s1,2)*(-78 - 133*t2 + 268*Power(t2,2) + 
                  280*Power(t2,3) + 262*Power(t2,4) + 49*Power(t2,5)) + 
               s1*(-60 + 178*t2 + 583*Power(t2,2) + 726*Power(t2,3) - 
                  594*Power(t2,4) - 1010*Power(t2,5) - 384*Power(t2,6)) \
- Power(s,2)*t2*(-236 - 238*t2 + 301*Power(t2,2) + 1014*Power(t2,3) + 
                  309*Power(t2,4) + 
                  s1*(-36 + 19*t2 + 126*Power(t2,2) + 169*Power(t2,3))) \
+ s*(-24 + 101*t2 - 461*Power(t2,2) + 1155*Power(t2,3) - 
                  56*Power(t2,4) - 178*Power(t2,5) + 37*Power(t2,6) + 
                  Power(s1,2)*t2*
                   (-54 - 75*t2 + 110*Power(t2,2) + 125*Power(t2,3)) + 
                  2*s1*(6 - 75*t2 - 292*Power(t2,2) - 178*Power(t2,3) + 
                     770*Power(t2,4) + 234*Power(t2,5))))) + 
         s2*Power(t1,3)*(8*Power(t1,7)*(5 + 8*s - 8*s1 + 15*t2) - 
            Power(t2,4)*(17 - 41*t2 + 23*Power(t2,2) + Power(t2,3) + 
               Power(s,3)*t2*(22 - 21*t2 + 5*Power(t2,2)) + 
               s*(-14 + 31*t2 - 26*Power(t2,2) + 9*Power(t2,3)) - 
               3*Power(s,2)*(2 + 3*t2 - 14*Power(t2,2) + 9*Power(t2,3))) \
+ t1*Power(t2,2)*(118 + (-269 + 18*s1)*t2 + (49 - 57*s1)*Power(t2,2) + 
               (56 + 82*s1)*Power(t2,3) + (17 - 33*s1)*Power(t2,4) + 
               (33 - 10*s1)*Power(t2,5) + 6*Power(t2,6) + 
               Power(s,3)*t2*
                (224 - 212*t2 - 22*Power(t2,2) + 32*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(-112 + 156*t2 + 4*(9 + 14*s1)*Power(t2,2) - 
                  (59 + 128*s1)*Power(t2,3) + 
                  (34 + 52*s1)*Power(t2,4) + 
                  (-39 + 20*s1)*Power(t2,5) - 8*Power(t2,6)) + 
               Power(s,2)*(-48 - 72*t2 + (279 - 8*s1)*Power(t2,2) + 
                  (-159 + 44*s1)*Power(t2,3) - 
                  (82 + 13*s1)*Power(t2,4) - (7 + 2*s1)*Power(t2,5) + 
                  2*Power(t2,6))) + 
            Power(t1,2)*(-128 - 180*(-2 + s1)*t2 + 
               (289 + 384*s1 + 18*Power(s1,2))*Power(t2,2) + 
               5*(-15 - 82*s1 + 3*Power(s1,2))*Power(t2,3) + 
               (-173 + 107*s1 - 76*Power(s1,2))*Power(t2,4) + 
               (-318 + 106*s1 + 39*Power(s1,2))*Power(t2,5) + 
               (-163 + 25*s1 + 4*Power(s1,2))*Power(t2,6) + 
               8*(-7 + s1)*Power(t2,7) - 
               Power(s,3)*t2*
                (544 - 560*t2 - 422*Power(t2,2) + 212*Power(t2,3) + 
                  45*Power(t2,4) + 7*Power(t2,5)) + 
               Power(s,2)*(96 + 144*t2 + (-318 + 64*s1)*Power(t2,2) + 
                  (323 - 308*s1)*Power(t2,3) - 
                  7*(-42 + s1)*Power(t2,4) + 
                  (87 + 98*s1)*Power(t2,5) + 
                  (18 + 11*s1)*Power(t2,6) - 8*Power(t2,7)) + 
               s*(224 - 20*t2 - 2*(449 + 206*s1)*Power(t2,2) + 
                  (92 + 910*s1 - 18*Power(s1,2))*Power(t2,3) + 
                  (-26 - 32*s1 + 39*Power(s1,2))*Power(t2,4) + 
                  (287 - 294*s1 - 37*Power(s1,2))*Power(t2,5) - 
                  4*(-35 + 12*s1 + Power(s1,2))*Power(t2,6) + 
                  (26 + 4*s1)*Power(t2,7))) + 
            Power(t1,6)*(-1832 + 16*Power(s,3) - 16*Power(s1,3) - 
               8*Power(s,2)*(-7 + 6*s1 - 14*t2) - 1492*t2 - 
               183*Power(t2,2) - 56*Power(t2,3) + 
               8*Power(s1,2)*(-1 + 14*t2) + 
               s1*(1012 + 764*t2 - 39*Power(t2,2)) + 
               s*(-444 + 48*Power(s1,2) - 212*t2 + 39*Power(t2,2) - 
                  16*s1*(3 + 14*t2))) + 
            Power(t1,5)*(416 + 1732*t2 + 2792*Power(t2,2) + 
               678*Power(t2,3) + 63*Power(t2,4) + 
               Power(s1,3)*(-44 + 60*t2 - 7*Power(t2,2)) + 
               Power(s,3)*(116 + 140*t2 + 7*Power(t2,2)) - 
               Power(s1,2)*(-8 + 172*t2 + 195*Power(t2,2) + 
                  38*Power(t2,3)) + 
               s1*(844 + 288*t2 - 1211*Power(t2,2) - 218*Power(t2,3) + 
                  38*Power(t2,4)) - 
               Power(s,2)*(888 + 620*t2 - 5*Power(t2,2) + 
                  38*Power(t2,3) + s1*(388 + 220*t2 + 21*Power(t2,2))) \
+ s*(-2260 - 2512*t2 + 77*Power(t2,2) - 26*Power(t2,3) - 
                  38*Power(t2,4) + 
                  Power(s1,2)*(316 + 20*t2 + 21*Power(t2,2)) + 
                  2*s1*(736 + 540*t2 + 95*Power(t2,2) + 38*Power(t2,3)))\
) - Power(t1,4)*(-1400 - 468*t2 + 2246*Power(t2,2) + 1975*Power(t2,3) + 
               1046*Power(t2,4) + 82*Power(t2,5) + 
               Power(s1,3)*(-28 + 120*t2 - 17*Power(t2,2) + 
                  2*Power(t2,3) - 5*Power(t2,4)) + 
               Power(s,3)*(68 + 184*t2 + 239*Power(t2,2) + 
                  82*Power(t2,3) + 5*Power(t2,4)) + 
               2*Power(s1,2)*
                (-56 - 250*t2 - 11*Power(t2,2) - 70*Power(t2,3) - 
                  23*Power(t2,4) + 2*Power(t2,5)) + 
               s1*(460 + 1344*t2 + 335*Power(t2,2) - 478*Power(t2,3) - 
                  336*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s,2)*(-(s1*
                     (-268 + 408*t2 + 551*Power(t2,2) + 
                       162*Power(t2,3) + 15*Power(t2,4))) + 
                  2*(720 + 806*t2 - 697*Power(t2,2) - 44*Power(t2,3) + 
                     20*Power(t2,4) + 2*Power(t2,5))) + 
               s*(236 + 528*t2 - 2441*Power(t2,2) - 1332*Power(t2,3) - 
                  77*Power(t2,4) - 34*Power(t2,5) + 
                  Power(s1,2)*
                   (-372 + 168*t2 + 329*Power(t2,2) + 78*Power(t2,3) + 
                     15*Power(t2,4)) - 
                  2*s1*(552 + 140*t2 - 816*Power(t2,2) - 
                     230*Power(t2,3) - 3*Power(t2,4) + 4*Power(t2,5)))) \
+ Power(t1,3)*(-344 - 452*t2 - 66*Power(t2,2) + 769*Power(t2,3) + 
               1008*Power(t2,4) + 641*Power(t2,5) + 109*Power(t2,6) + 
               Power(s1,3)*t2*
                (-36 + 14*t2 + 42*Power(t2,2) - 13*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s,3)*(-192 - 980*t2 + 344*Power(t2,2) + 
                  188*Power(t2,3) + 93*Power(t2,4) + 11*Power(t2,5)) + 
               s1*(-276 + 216*t2 + 81*Power(t2,2) + 236*Power(t2,3) - 
                  178*Power(t2,4) - 130*Power(t2,5) - 26*Power(t2,6)) + 
               Power(s1,2)*(144 - 216*t2 + 275*Power(t2,2) - 
                  205*Power(t2,3) - 76*Power(t2,4) - 9*Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s,2)*(-288 - 584*t2 + 455*Power(t2,2) + 
                  259*Power(t2,3) - 461*Power(t2,4) + 67*Power(t2,5) + 
                  10*Power(t2,6) - 
                  2*s1*(64 - 210*t2 - 171*Power(t2,2) + 
                     203*Power(t2,3) + 97*Power(t2,4) + 12*Power(t2,5))) \
+ s*(1052 + 1224*t2 - 219*Power(t2,2) - 578*Power(t2,3) - 
                  783*Power(t2,4) - 323*Power(t2,5) - 14*Power(t2,6) + 
                  3*Power(s1,2)*t2*
                   (60 - 100*t2 + 48*Power(t2,2) + 38*Power(t2,3) + 
                     5*Power(t2,4)) - 
                  2*s1*(-160 + 664*t2 + 469*Power(t2,2) - 
                     309*Power(t2,3) - 313*Power(t2,4) + Power(t2,5) + 
                     6*Power(t2,6))))) + 
         Power(s2,2)*Power(t1,2)*
          (4*Power(t1,8) + Power(t1,7)*
             (-66 + 20*Power(s,2) + 20*Power(s1,2) - 227*t2 - 
               172*Power(t2,2) + 3*s1*(124 + 57*t2) - 
               s*(304 + 40*s1 + 171*t2)) + 
            Power(t2,4)*(17 - 36*t2 + 12*Power(t2,2) - 
               3*Power(s,3)*(-2 + t2)*Power(t2,2) + 4*Power(t2,3) + 
               3*Power(t2,4) - 
               3*Power(s,2)*t2*
                (6 - 11*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
               s*(-6 + 29*t2 - 49*Power(t2,2) + 29*Power(t2,3) - 
                  3*Power(t2,4))) + 
            t1*Power(t2,2)*(-118 + (193 - 18*s1)*t2 + 
               (125 + 72*s1)*Power(t2,2) - (72 + 109*s1)*Power(t2,3) + 
               (-79 + 30*s1)*Power(t2,4) + (-41 + 21*s1)*Power(t2,5) + 
               (-26 + 4*s1)*Power(t2,6) - 2*Power(t2,7) - 
               Power(s,3)*Power(t2,2)*
                (136 - 143*t2 + 18*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s,2)*t2*
                (168 + (-203 + 6*s1)*t2 - (142 + 33*s1)*Power(t2,2) + 
                  6*(37 + s1)*Power(t2,3) + 44*Power(t2,4) - 
                  2*Power(t2,5)) + 
               2*s*(24 - 70*t2 + (96 - 34*s1)*Power(t2,2) + 
                  (-57 + 66*s1)*Power(t2,3) - 
                  (13 + 16*s1)*Power(t2,4) - 4*(1 + 3*s1)*Power(t2,5) + 
                  (17 - 4*s1)*Power(t2,6) + Power(t2,7))) + 
            Power(t1,2)*(128 + 4*(-58 + 45*s1)*t2 - 
               (641 + 384*s1 + 18*Power(s1,2))*Power(t2,2) + 
               (-306 + 404*s1 - 51*Power(s1,2))*Power(t2,3) + 
               2*(245 + 34*s1 + 69*Power(s1,2))*Power(t2,4) + 
               (448 - 180*s1 - 57*Power(s1,2))*Power(t2,5) - 
               2*(-226 + 69*s1 + 6*Power(s1,2))*Power(t2,6) + 
               (202 - 24*s1)*Power(t2,7) + (29 - 6*s1)*Power(t2,8) + 
               Power(s,3)*Power(t2,2)*
                (520 - 742*t2 - 164*Power(t2,2) + 175*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s,2)*t2*
                (-384 + (238 - 48*s1)*t2 + 
                  (659 + 196*s1)*Power(t2,2) + 
                  2*(-388 + 83*s1)*Power(t2,3) - 
                  (481 + 114*s1)*Power(t2,4) - 
                  (99 + 32*s1)*Power(t2,5) + 9*Power(t2,6)) + 
               s*(-96 - 12*t2 + (2 + 508*s1)*Power(t2,2) + 
                  2*(404 - 371*s1 + 9*Power(s1,2))*Power(t2,3) - 
                  (21 + 544*s1 + 51*Power(s1,2))*Power(t2,4) + 
                  (131 + 318*s1 + 51*Power(s1,2))*Power(t2,5) + 
                  (-363 + 260*s1 + 12*Power(s1,2))*Power(t2,6) + 
                  2*(-61 + 4*s1)*Power(t2,7) - 7*Power(t2,8))) + 
            Power(t1,6)*(3128 + Power(s,3)*(28 - 27*t2) + 5902*t2 + 
               1992*Power(t2,2) + 323*Power(t2,3) + 52*Power(t2,4) + 
               Power(s1,3)*(80 + 27*t2) + 
               Power(s1,2)*(158 - 355*t2 - 158*Power(t2,2)) + 
               s1*(-1448 - 3299*t2 - 868*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(s,2)*(-330 - 419*t2 - 158*Power(t2,2) + 
                  3*s1*(8 + 27*t2)) + 
               s*(572 + 1561*t2 + 286*Power(t2,2) - 30*Power(t2,3) - 
                  3*Power(s1,2)*(44 + 27*t2) + 
                  2*s1*(94 + 387*t2 + 158*Power(t2,2)))) + 
            Power(t1,5)*(-792 - 3678*t2 - 7225*Power(t2,2) - 
               4879*Power(t2,3) - 727*Power(t2,4) - 51*Power(t2,5) + 
               Power(s1,3)*(180 - 199*t2 - 44*Power(t2,2) + 
                  3*Power(t2,3)) - 
               Power(s,3)*(72 + 379*t2 + 146*Power(t2,2) + 
                  3*Power(t2,3)) + 
               2*Power(s1,2)*
                (64 + 164*t2 + 274*Power(t2,2) + 151*Power(t2,3) + 
                  13*Power(t2,4)) + 
               s1*(-1164 - 2219*t2 + 2306*Power(t2,2) + 
                  2068*Power(t2,3) + 76*Power(t2,4) - 19*Power(t2,5)) + 
               Power(s,2)*(304 + 2660*t2 + 812*Power(t2,2) + 
                  210*Power(t2,3) + 26*Power(t2,4) + 
                  s1*(340 + 807*t2 + 248*Power(t2,2) + 9*Power(t2,3))) \
- s*(-2272 - 6577*t2 - 1886*Power(t2,2) + 412*Power(t2,3) - 
                  112*Power(t2,4) - 19*Power(t2,5) + 
                  Power(s1,2)*
                   (560 + 229*t2 + 58*Power(t2,2) + 9*Power(t2,3)) + 
                  4*s1*(360 + 993*t2 + 379*Power(t2,2) + 
                     128*Power(t2,3) + 13*Power(t2,4)))) + 
            Power(t1,4)*(-1252 - 2630*t2 + 3231*Power(t2,2) + 
               5499*Power(t2,3) + 4072*Power(t2,4) + 1162*Power(t2,5) + 
               47*Power(t2,6) + 
               Power(s1,3)*(-84 + 208*t2 + 14*Power(t2,2) + 
                  39*Power(t2,3) - 12*Power(t2,4)) + 
               Power(s,3)*(-196 + 418*t2 + 418*Power(t2,2) + 
                  341*Power(t2,3) + 50*Power(t2,4)) - 
               Power(s1,2)*(-222 + 1253*t2 + 331*Power(t2,2) + 
                  355*Power(t2,3) + 205*Power(t2,4) + 13*Power(t2,5)) + 
               s1*(388 + 2457*t2 + 1876*Power(t2,2) - 
                  461*Power(t2,3) - 1292*Power(t2,4) - 
                  293*Power(t2,5) + 4*Power(t2,6)) - 
               Power(s,2)*(-534 - 3295*t2 + 211*Power(t2,2) + 
                  1997*Power(t2,3) + 59*Power(t2,4) - 9*Power(t2,5) + 
                  s1*(-292 + 204*t2 + 994*Power(t2,2) + 
                     697*Power(t2,3) + 112*Power(t2,4))) + 
               s*(72 + 457*t2 - 1428*Power(t2,2) - 4157*Power(t2,3) - 
                  981*Power(t2,4) - 83*Power(t2,5) - 17*Power(t2,6) + 
                  Power(s1,2)*
                   (-540 - 102*t2 + 690*Power(t2,2) + 
                     317*Power(t2,3) + 74*Power(t2,4)) + 
                  2*s1*(-546 - 965*t2 + 943*Power(t2,2) + 
                     1437*Power(t2,3) + 182*Power(t2,4) + 2*Power(t2,5))\
)) + Power(t1,3)*(322 + 1137*t2 + 117*Power(t2,2) - 834*Power(t2,3) - 
               2173*Power(t2,4) - 1899*Power(t2,5) - 738*Power(t2,6) - 
               65*Power(t2,7) + 
               2*Power(s1,3)*t2*
                (18 + 15*t2 - 45*Power(t2,2) + 5*Power(t2,3) + 
                  2*Power(t2,4)) - 
               2*Power(s,3)*(48 - 354*t2 - 486*Power(t2,2) + 
                  314*Power(t2,3) + 107*Power(t2,4) + 30*Power(t2,5)) + 
               Power(s1,2)*(-144 + 168*t2 - 381*Power(t2,2) + 
                  248*Power(t2,3) + 209*Power(t2,4) + 74*Power(t2,5) - 
                  4*Power(t2,6)) + 
               s1*(492 - 394*t2 + 18*Power(t2,2) - 699*Power(t2,3) + 
                  328*Power(t2,4) + 371*Power(t2,5) + 162*Power(t2,6) + 
                  17*Power(t2,7)) + 
               Power(s,2)*(-32 - 16*t2 + 403*Power(t2,2) - 
                  96*Power(t2,3) + 678*Power(t2,4) + 276*Power(t2,5) - 
                  42*Power(t2,6) + 
                  s1*(96 - 148*t2 - 726*Power(t2,2) + 280*Power(t2,3) + 
                     462*Power(t2,4) + 119*Power(t2,5))) + 
               s*(-420 - 2140*t2 - 782*Power(t2,2) + 107*Power(t2,3) + 
                  1004*Power(t2,4) + 1076*Power(t2,5) + 
                  201*Power(t2,6) + 3*Power(t2,7) - 
                  Power(s1,2)*t2*
                   (180 - 276*t2 + 42*Power(t2,2) + 263*Power(t2,3) + 
                     63*Power(t2,4)) + 
                  2*s1*(-256 + 536*t2 + 1191*Power(t2,2) + 
                     170*Power(t2,3) - 804*Power(t2,4) - 
                     251*Power(t2,5) + 16*Power(t2,6))))) - 
         Power(s2,3)*t1*(Power(t1,8)*(-27 + 31*s - 31*s1 + 16*t2) + 
            (-1 + t2)*Power(t2,4)*
             (-6 + (3 - 6*s)*t2 + (6 + 5*s)*Power(t2,2) + 
               (5 + 3*s)*Power(t2,3)) + 
            t1*Power(t2,2)*(-42 - 3*(-1 + 4*s + 2*s1)*t2 + 
               (176 + 84*Power(s,2) + 37*s1 - 3*s*(15 + 8*s1))*
                Power(t2,2) + 
               (14 - 20*Power(s,3) - 60*s1 - Power(s,2)*(167 + 6*s1) + 
                  2*s*(97 + 20*s1))*Power(t2,3) + 
               (-101 + 15*Power(s,3) + 9*s1 + Power(s,2)*(59 + s1) - 
                  s*(129 + 4*s1))*Power(t2,4) + 
               (-35 + 2*Power(s,3) + Power(s,2)*(47 - 2*s1) + 12*s1 - 
                  4*s*(10 + s1))*Power(t2,5) + 
               (-31 + 6*Power(s,2) + s*(20 - 8*s1) + 8*s1)*Power(t2,6) + 
               4*(-1 + s)*Power(t2,7)) + 
            Power(t1,2)*(48 + (24 - 84*s + 60*s1)*t2 - 
               (459 + 402*Power(s,2) + 112*s1 + 6*Power(s1,2) - 
                  2*s*(77 + 90*s1))*Power(t2,2) + 
               (-712 + 202*Power(s,3) + 74*s1 - 53*Power(s1,2) + 
                  Power(s,2)*(653 + 12*s1) + 
                  s*(-296 + 6*s1 + 6*Power(s1,2)))*Power(t2,3) + 
               (-337*Power(s,3) + 5*Power(s,2)*(32 + 27*s1) + 
                  s*(361 - 648*s1 - 29*Power(s1,2)) + 
                  3*(121 + 82*s1 + 36*Power(s1,2)))*Power(t2,4) + 
               (513 + 14*Power(s,3) - 70*s1 - 37*Power(s1,2) - 
                  2*Power(s,2)*(298 + 25*s1) + 
                  s*(360 + 42*s1 + 31*Power(s1,2)))*Power(t2,5) + 
               (33*Power(s,3) - Power(s,2)*(284 + 15*s1) - 
                  3*(-159 + 72*s1 + 4*Power(s1,2)) + 
                  s*(31 + 232*s1 + 12*Power(s1,2)))*Power(t2,6) + 
               (305 - 7*Power(s,2) - 48*s1 + s*(-217 + 60*s1))*
                Power(t2,7) + (79 - 19*s - 14*s1)*Power(t2,8) + 
               2*Power(t2,9)) + 
            Power(t1,7)*(-488 + 15*Power(s,3) - 15*Power(s1,3) - 
               266*t2 - 427*Power(t2,2) - 112*Power(t2,3) + 
               Power(s1,2)*(65 + 50*t2) + 
               Power(s,2)*(73 - 45*s1 + 50*t2) + 
               s1*(877 + 1010*t2 + 150*Power(t2,2)) + 
               s*(45*Power(s1,2) - 2*s1*(69 + 50*t2) - 
                  3*(233 + 282*t2 + 50*Power(t2,2)))) + 
            Power(t1,6)*(3026 + 10125*t2 + 7247*Power(t2,2) + 
               1435*Power(t2,3) + 257*Power(t2,4) + 16*Power(t2,5) + 
               Power(s,3)*(109 - 2*t2 - 11*Power(t2,2)) + 
               Power(s1,3)*(141 + 158*t2 + 11*Power(t2,2)) - 
               2*Power(s1,2)*
                (-99 + 160*t2 + 317*Power(t2,2) + 31*Power(t2,3)) + 
               s1*(-803 - 5158*t2 - 4124*Power(t2,2) - 
                  468*Power(t2,3) + 7*Power(t2,4)) + 
               Power(s,2)*(s1*(-229 + 162*t2 + 33*Power(t2,2)) - 
                  2*(383 + 554*t2 + 313*Power(t2,2) + 31*Power(t2,3))) + 
               s*(-123 + 2480*t2 + 2146*Power(t2,2) + 192*Power(t2,3) - 
                  7*Power(t2,4) - 
                  3*Power(s1,2)*(7 + 106*t2 + 11*Power(t2,2)) + 
                  4*s1*(184 + 351*t2 + 315*Power(t2,2) + 31*Power(t2,3)))\
) - Power(t1,5)*(394 + 4551*t2 + 10403*Power(t2,2) + 11211*Power(t2,3) + 
               3749*Power(t2,4) + 324*Power(t2,5) + 13*Power(t2,6) + 
               Power(s1,3)*(-302 + 234*t2 + 217*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(s,3)*(88 + 392*t2 + 413*Power(t2,2) + 
                  50*Power(t2,3)) - 
               Power(s1,2)*(231 + 699*t2 + 727*Power(t2,2) + 
                  705*Power(t2,3) + 163*Power(t2,4)) + 
               s1*(999 + 3424*t2 - 611*Power(t2,2) - 4910*Power(t2,3) - 
                  1259*Power(t2,4) + 22*Power(t2,5)) - 
               Power(s,2)*(-485 + 1979*t2 + 3317*Power(t2,2) + 
                  873*Power(t2,3) + 143*Power(t2,4) + 
                  s1*(54 + 942*t2 + 799*Power(t2,2) + 84*Power(t2,3))) + 
               s*(-1149 - 6146*t2 - 5561*Power(t2,2) + 624*Power(t2,3) + 
                  229*Power(t2,4) - 66*Power(t2,5) + 
                  Power(s1,2)*
                   (412 + 540*t2 + 169*Power(t2,2) + 18*Power(t2,3)) + 
                  2*s1*(41 + 2355*t2 + 2247*Power(t2,2) + 
                     797*Power(t2,3) + 153*Power(t2,4)))) + 
            Power(t1,4)*(-643 - 2435*t2 + 992*Power(t2,2) + 
               7314*Power(t2,3) + 7272*Power(t2,4) + 3599*Power(t2,5) + 
               474*Power(t2,6) + 4*Power(t2,7) + 
               4*Power(s1,3)*
                (-21 + 26*t2 + 22*Power(t2,2) + 22*Power(t2,3) + 
                  Power(t2,4)) + 
               2*Power(s,3)*(-66 - 185*t2 + 344*Power(t2,2) + 
                  242*Power(t2,3) + 78*Power(t2,4)) - 
               Power(s1,2)*(-538 + 1117*t2 + 814*Power(t2,2) + 
                  604*Power(t2,3) + 372*Power(t2,4) + 71*Power(t2,5)) + 
               s1*(64 + 1942*t2 + 3091*Power(t2,2) + 352*Power(t2,3) - 
                  1760*Power(t2,4) - 1150*Power(t2,5) - 74*Power(t2,6)) \
+ Power(s,2)*(138 + 1335*t2 + 1730*Power(t2,2) - 2476*Power(t2,3) - 
                  1174*Power(t2,4) - 33*Power(t2,5) - 
                  2*s1*(-66 - 58*t2 + 317*Power(t2,2) + 
                     516*Power(t2,3) + 162*Power(t2,4))) + 
               s*(222 - 276*t2 + 207*Power(t2,2) - 3212*Power(t2,3) - 
                  2417*Power(t2,4) - 210*Power(t2,5) - 39*Power(t2,6) + 
                  2*Power(s1,2)*
                   (-174 - 197*t2 + 245*Power(t2,2) + 282*Power(t2,3) + 
                     82*Power(t2,4)) + 
                  2*s1*(-230 - 1013*t2 - 412*Power(t2,2) + 
                     2260*Power(t2,3) + 879*Power(t2,4) + 58*Power(t2,5))\
)) - Power(t1,3)*(-78 - 1151*t2 - 748*Power(t2,2) + 953*Power(t2,3) + 
               2073*Power(t2,4) + 2633*Power(t2,5) + 1560*Power(t2,6) + 
               311*Power(t2,7) + 7*Power(t2,8) + 
               2*Power(s1,3)*t2*
                (-6 - 27*t2 + 34*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s,3)*t2*(196 - 816*t2 - 328*Power(t2,2) + 
                  417*Power(t2,3) + 94*Power(t2,4)) - 
               Power(s1,2)*(-48 + 8*t2 - 253*Power(t2,2) + 
                  74*Power(t2,3) + 241*Power(t2,4) + 148*Power(t2,5)) - 
               Power(s,2)*(96 + 12*(-36 + 7*s1)*t2 - 
                  (447 + 386*s1)*Power(t2,2) + 
                  (412 - 116*s1)*Power(t2,3) + 
                  (981 + 371*s1)*Power(t2,4) + 
                  2*(332 + 93*s1)*Power(t2,5) + 17*Power(t2,6)) - 
               s1*(348 - 134*t2 - 52*Power(t2,2) - 794*Power(t2,3) + 
                  159*Power(t2,4) + 620*Power(t2,5) + 305*Power(t2,6) + 
                  78*Power(t2,7)) + 
               s*(52 + 668*t2 + 1462*Power(t2,2) + 288*Power(t2,3) + 
                  539*Power(t2,4) - 1144*Power(t2,5) - 484*Power(t2,6) - 
                  27*Power(t2,7) + 
                  Power(s1,2)*t2*
                   (60 - 68*t2 - 108*Power(t2,2) + 255*Power(t2,3) + 
                     99*Power(t2,4)) + 
                  2*s1*(96 + 56*t2 - 963*Power(t2,2) - 788*Power(t2,3) + 
                     444*Power(t2,4) + 639*Power(t2,5) + 28*Power(t2,6))))\
))*R1q(t1))/(Power(s2 - t1,3)*(-s + s2 - t1)*(-1 + t1)*t1*(s - s1 + t2)*
       (-1 + s2 - t1 + t2)*Power(-t1 + s2*t2,2)*
       Power(Power(s2,2) - 4*t1 + 2*s2*t2 + Power(t2,2),2)) + 
    (8*(-32*Power(t1,7) - 4*Power(s2,9)*Power(t2,2) + 
         2*(-2 + t2)*Power(t2,3)*Power(1 + (-1 + s)*t2,3) - 
         4*Power(t1,6)*(-60 + 4*Power(s,2) + 4*Power(s1,2) - 
            4*s*(8 + 2*s1 - t2) - 4*s1*(-20 + t2) - 12*t2 - 5*Power(t2,2)\
) - t1*t2*(1 + (-1 + s)*t2)*(-28 + (68 - 56*s)*t2 + 
            (-43 - 28*Power(s,2) + s*(90 - 12*s1) + 10*s1)*Power(t2,2) + 
            2*(3 + 5*Power(s,2) - 9*s1 + s*(-1 + 8*s1))*Power(t2,3) + 
            (-1 + 5*Power(s,2) + 8*s1 - 8*s*(1 + s1))*Power(t2,4) + 
            2*(-1 + s)*Power(t2,5)) + 
         Power(t1,2)*(64 - 48*t2 - 56*Power(t2,2) - 28*Power(t2,3) + 
            46*Power(t2,4) + 6*Power(t2,6) + 
            2*Power(s1,3)*Power(t2,2)*(-2 + 3*t2) + 
            Power(s,3)*Power(t2,2)*
             (-40 - 6*t2 + 35*Power(t2,2) + 7*Power(t2,3)) + 
            Power(s1,2)*t2*(36 - 48*t2 - 13*Power(t2,2) + 
               9*Power(t2,3) - 4*Power(t2,4)) + 
            Power(s,2)*t2*(-108 + (176 - 84*s1)*t2 + 
               3*(61 + 38*s1)*Power(t2,2) - 
               3*(17 + 14*s1)*Power(t2,3) - (12 + 17*s1)*Power(t2,4) + 
               8*Power(t2,5)) - 
            s1*(72 - 172*t2 + 202*Power(t2,2) - 163*Power(t2,3) + 
               21*Power(t2,4) + 4*Power(t2,5) + 2*Power(t2,6)) - 
            s*(72 + 4*(-61 + 30*s1)*t2 - 2*(1 + 168*s1)*Power(t2,2) + 
               (207 + 274*s1 + 2*Power(s1,2))*Power(t2,3) + 
               (22 - 70*s1 + 17*Power(s1,2))*Power(t2,4) + 
               (23 - 33*s1 - 10*Power(s1,2))*Power(t2,5) + 
               4*(3 + s1)*Power(t2,6))) + 
         2*Power(s2,8)*(Power(s1,3) - 2*s1*Power(t2,2) + 
            t2*((8 + 3*s - 13*t2)*t2 + t1*(4 + 6*t2))) - 
         Power(s2,7)*(Power(s1,3)*(2 + 6*t1 - 11*t2) + 
            4*Power(t1,2)*(1 + 6*t2 + 3*Power(t2,2)) - 
            2*Power(s1,2)*(-3 - (-4 + t1)*t2 + 4*Power(t2,2) + 
               s*(3*t1 + t2)) + 
            t1*t2*(32 - 60*t2 - 51*Power(t2,2) + 4*s*(3 + 8*t2)) + 
            s1*t2*(s*(6 - 6*t1 + t2) + t2*(29 + 33*t2) - 
               t1*(8 + 37*t2)) + 
            Power(t2,2)*(-26 + 2*Power(s,2) - 95*t2 + 66*Power(t2,2) - 
               s*(2 + 39*t2))) - 
         Power(t1,5)*(160 + 48*Power(s,3) + 16*Power(s1,3) + 260*t2 + 
            124*Power(t2,2) + 33*Power(t2,3) - 
            4*Power(s1,2)*(-48 + 20*t2 + 3*Power(t2,2)) + 
            s1*(136 - 300*t2 - 154*Power(t2,2) + 19*Power(t2,3)) - 
            4*Power(s,2)*(20*s1 + 3*(24 - 4*t2 + Power(t2,2))) + 
            s*(-344 + 16*Power(s1,2) - 20*t2 + 54*Power(t2,2) - 
               19*Power(t2,3) + 8*s1*(32 + 4*t2 + 3*Power(t2,2)))) + 
         Power(t1,4)*(Power(s1,3)*
             (-8 + 36*t2 + 6*Power(t2,2) - 3*Power(t2,3)) + 
            Power(s,3)*(24 + 76*t2 + 22*Power(t2,2) + 3*Power(t2,3)) + 
            Power(s1,2)*(-240 + 28*t2 + 84*Power(t2,2) - 
               51*Power(t2,3) + 4*Power(t2,4)) + 
            s1*(-120 + 516*t2 - 46*Power(t2,2) - 195*Power(t2,3) + 
               7*Power(t2,4)) + 
            2*(-72 + 128*t2 - 14*Power(t2,2) + 76*Power(t2,3) + 
               17*Power(t2,4)) + 
            Power(s,2)*(176 - 228*t2 - 52*Power(t2,2) + 
               13*Power(t2,3) + 4*Power(t2,4) - 
               s1*(-136 + 164*t2 + 38*Power(t2,2) + 9*Power(t2,3))) + 
            s*(-376 - 564*t2 - 234*Power(t2,2) + 23*Power(t2,3) - 
               20*Power(t2,4) + 
               Power(s1,2)*(-152 + 52*t2 + 10*Power(t2,2) + 
                  9*Power(t2,3)) + 
               s1*(128 + 216*t2 + 112*Power(t2,2) + 38*Power(t2,3) - 
                  8*Power(t2,4)))) - 
         Power(t1,3)*(-32 - 296*t2 + 212*Power(t2,2) + 38*Power(t2,3) - 
            11*Power(t2,4) + 33*Power(t2,5) + 
            Power(s1,3)*(8 - 12*t2 + 10*Power(t2,2) + 15*Power(t2,3) - 
               4*Power(t2,4)) + 
            Power(s,3)*(8 - 20*t2 + 54*Power(t2,2) + 41*Power(t2,3) + 
               7*Power(t2,4)) + 
            s1*(120 - 52*t2 + 98*Power(t2,2) + 131*Power(t2,3) - 
               58*Power(t2,4) - 12*Power(t2,5)) + 
            2*Power(s1,2)*(24 - 104*t2 + 12*Power(t2,2) - 
               6*Power(t2,4) + Power(t2,5)) + 
            Power(s,2)*(s1*(-72 + 92*t2 + 30*Power(t2,2) - 
                  91*Power(t2,3) - 18*Power(t2,4)) + 
               2*(24 + 152*t2 + 48*Power(t2,2) - 64*Power(t2,3) + 
                  8*Power(t2,4) + 5*Power(t2,5))) + 
            s*(152 - 156*t2 - 410*Power(t2,2) - 187*Power(t2,3) - 
               55*Power(t2,4) - 11*Power(t2,5) + 
               Power(s1,2)*(-72 + 100*t2 - 142*Power(t2,2) + 
                  35*Power(t2,3) + 15*Power(t2,4)) - 
               2*s1*(-128 + 56*t2 + 20*Power(t2,2) - 84*Power(t2,3) - 
                  19*Power(t2,4) + 6*Power(t2,5)))) + 
         Power(s2,6)*(4*Power(t1,3)*(3 + 6*t2 + Power(t2,2)) + 
            Power(s1,3)*(-4 + 6*Power(t1,2) - 2*t2 + 24*Power(t2,2) - 
               3*t1*(3 + 11*t2)) + 
            Power(t1,2)*(16 - 42*t2 + 6*Power(s,2)*t2 - 
               169*Power(t2,2) - 17*Power(t2,3) + 
               s*(6 + 64*t2 + 59*Power(t2,2))) + 
            t1*t2*(-52 - 430*t2 + 132*Power(t2,2) + 84*Power(t2,3) + 
               Power(s,2)*(-2 + 25*t2) - 
               s*(10 + 158*t2 + 125*Power(t2,2))) + 
            t2*(-2*Power(s,2)*t2*(10 + 7*t2) + 
               3*t2*(15 + 32*t2 + 74*Power(t2,2) - 28*Power(t2,3)) + 
               s*(6 - 37*t2 - 16*Power(t2,2) + 96*Power(t2,3))) + 
            Power(s1,2)*(6 - 5*(9 + 2*s)*t2 + (19 + 9*s)*Power(t2,2) + 
               38*Power(t2,3) + Power(t1,2)*(2 - 12*s + 4*t2) + 
               t1*(10 - 42*t2 - 3*Power(t2,2) + 7*s*(-2 + 5*t2))) + 
            s1*(6 - 16*t2 + 26*Power(t2,2) - 115*Power(t2,3) - 
               98*Power(t2,4) + 
               Power(s,2)*(6*Power(t1,2) - 2*t1*t2 + Power(t2,2)) - 
               2*Power(t1,2)*(2 + 37*t2 + 32*Power(t2,2)) + 
               t1*t2*(62 + 145*t2 + 168*Power(t2,2)) + 
               s*(-2*Power(t1,2)*(3 + 5*t2) - 
                  2*t1*(3 - 6*t2 + 2*Power(t2,2)) + 
                  t2*(8 + 5*Power(t2,2))))) + 
         Power(s2,5)*(-2 + 8*t2 - 10*s*t2 - 70*Power(t2,2) - 
            64*s*Power(t2,2) + 21*Power(s,2)*Power(t2,2) + 
            6*Power(s,3)*Power(t2,2) + 159*Power(t2,3) - 
            117*s*Power(t2,3) - 87*Power(s,2)*Power(t2,3) + 
            Power(s,3)*Power(t2,3) + 138*Power(t2,4) - 
            88*s*Power(t2,4) - 34*Power(s,2)*Power(t2,4) + 
            258*Power(t2,5) + 114*s*Power(t2,5) - 56*Power(t2,6) - 
            4*Power(t1,4)*(3 + 2*t2) + 
            Power(s1,3)*(-2*Power(t1,3) + Power(t1,2)*(39 + 33*t2) + 
               t1*(22 - 43*t2 - 72*Power(t2,2)) + 
               2*t2*(-8 + 6*t2 + 13*Power(t2,2))) + 
            Power(t1,3)*(8 + 2*Power(s,3) + 185*t2 + 73*Power(t2,2) - 
               8*Power(t2,3) - 2*Power(s,2)*(3 + t2) - 
               s*(32 + 118*t2 + 31*Power(t2,2))) + 
            t1*t2*(-92 - 384*t2 - 7*Power(s,3)*t2 - 1141*Power(t2,2) + 
               55*Power(t2,3) + 66*Power(t2,4) + 
               Power(s,2)*(60 + 57*t2 + 92*Power(t2,2)) + 
               s*(64 + 291*t2 - 503*Power(t2,2) - 180*Power(t2,3))) + 
            Power(t1,2)*(26 + 575*t2 - 4*Power(s,3)*t2 + 
               212*Power(t2,2) - 345*Power(t2,3) + 21*Power(t2,4) - 
               Power(s,2)*(2 + 60*t2 + 29*Power(t2,2)) + 
               s*(8 + 209*t2 + 391*Power(t2,2) + 157*Power(t2,3))) + 
            Power(s1,2)*(2*(6 - 2*t2 - 5*(11 + 4*s)*Power(t2,2) + 
                  (-2 + 8*s)*Power(t2,3) + 36*Power(t2,4)) + 
               Power(t1,3)*(6*s - 2*(2 + t2)) + 
               Power(t1,2)*(16 + 44*t2 - 33*Power(t2,2) - 
                  s*(23 + 70*t2)) + 
               t1*(39 + 61*t2 - 195*Power(t2,2) + 8*Power(t2,3) + 
                  s*(4 - 47*t2 + 76*Power(t2,2)))) + 
            s1*(-6 + (57 + 26*s)*t2 - 
               2*(2 - 9*s + Power(s,2))*Power(t2,2) + 
               2*(40 + 32*s + Power(s,2))*Power(t2,3) + 
               10*(-17 + 3*s)*Power(t2,4) - 142*Power(t2,5) + 
               Power(t1,3)*(37 - 6*Power(s,2) + 128*t2 + 
                  31*Power(t2,2) + 2*s*(5 + 2*t2)) + 
               Power(t1,2)*(-33 - 199*t2 - 553*Power(t2,2) - 
                  185*Power(t2,3) + Power(s,2)*(-16 + 41*t2) + 
                  s*(13 - 24*t2 + 62*Power(t2,2))) + 
               t1*(-2 + 191*Power(t2,2) + 535*Power(t2,3) + 
                  302*Power(t2,4) + Power(s,2)*t2*(-14 + 3*t2) - 
                  4*s*(-4 + 29*t2 + 10*Power(t2,2) + 18*Power(t2,3))))) - 
         Power(s2,4)*(-2 - 4*Power(t1,5) + 23*t2 + 16*s*t2 + 
            45*Power(t2,2) - 43*s*Power(t2,2) - 
            34*Power(s,2)*Power(t2,2) + 4*Power(s,3)*Power(t2,2) + 
            141*Power(t2,3) + 266*s*Power(t2,3) - 
            68*Power(s,2)*Power(t2,3) - 26*Power(s,3)*Power(t2,3) - 
            200*Power(t2,4) + 120*s*Power(t2,4) + 
            148*Power(s,2)*Power(t2,4) - 4*Power(s,3)*Power(t2,4) - 
            100*Power(t2,5) + 152*s*Power(t2,5) + 
            36*Power(s,2)*Power(t2,5) - 152*Power(t2,6) - 
            66*s*Power(t2,6) + 18*Power(t2,7) - 
            Power(t1,4)*(-67 + 2*Power(s,2) - 95*t2 + 20*Power(t2,2) + 
               s*(59 + 62*t2)) + 
            Power(t1,3)*(240 + Power(s,3)*(4 - 11*t2) + 556*t2 - 
               466*Power(t2,2) + 37*Power(t2,3) + 28*Power(t2,4) - 
               Power(s,2)*(41 + 40*t2 + 30*Power(t2,2)) + 
               s*(90 + 411*t2 + 480*Power(t2,2) + 43*Power(t2,3))) + 
            Power(s1,3)*(Power(t1,3)*(43 + 11*t2) + 
               Power(t1,2)*(17 - 145*t2 - 61*Power(t2,2)) - 
               2*Power(t2,2)*(-12 + 14*t2 + 7*Power(t2,2)) + 
               t1*(-28 - 18*t2 + 67*Power(t2,2) + 78*Power(t2,3))) + 
            Power(t1,2)*(-47 - 484*t2 + Power(s,3)*(-18 + t2)*t2 - 
               2238*Power(t2,2) - 854*Power(t2,3) + 251*Power(t2,4) - 
               45*Power(t2,5) + 
               Power(s,2)*(22 + 123*t2 + 285*Power(t2,2) + 
                  113*Power(t2,3)) + 
               s*(39 + 502*t2 - 656*Power(t2,2) - 709*Power(t2,3) - 
                  117*Power(t2,4))) + 
            t1*(2 - 114*t2 + 266*Power(t2,2) + 780*Power(t2,3) + 
               1157*Power(t2,4) + 115*Power(t2,5) - 24*Power(t2,6) + 
               4*Power(s,3)*t2*(3 + 2*t2 + 7*Power(t2,2)) - 
               Power(s,2)*t2*
                (-36 + 200*t2 + 253*Power(t2,2) + 118*Power(t2,3)) + 
               s*(2 - 209*t2 - 420*Power(t2,2) - 970*Power(t2,3) + 
                  597*Power(t2,4) + 110*Power(t2,5))) - 
            Power(s1,2)*(2*Power(t1,4) + 
               2*t2*(24 - 26*t2 - 30*(2 + s)*Power(t2,2) + 
                  (-23 + 7*s)*Power(t2,3) + 34*Power(t2,4)) + 
               Power(t1,3)*(-35 + 36*t2 + 30*Power(t2,2) + 
                  s*(82 + 33*t2)) + 
               Power(t1,2)*(-98 + 241*t2 + 135*Power(t2,2) - 
                  121*Power(t2,3) + s*(86 - 89*t2 - 123*Power(t2,2))) + 
               t1*(-56 + 269*t2 + 187*Power(t2,2) - 325*Power(t2,3) + 
                  22*Power(t2,4) + 
                  s*(12 + 58*t2 - 57*Power(t2,2) + 74*Power(t2,3)))) + 
            s1*(12 + 2*(-7 + 6*s)*t2 - (85 + 64*s)*Power(t2,2) + 
               4*(-22 - 3*s + 2*Power(s,2))*Power(t2,3) + 
               2*(-41 - 68*s + Power(s,2))*Power(t2,4) + 
               (110 - 50*s)*Power(t2,5) + 108*Power(t2,6) + 
               Power(t1,4)*(64 + 4*s + 62*t2) + 
               Power(t1,3)*(-87 - 606*t2 - 604*Power(t2,2) - 
                  43*Power(t2,3) + Power(s,2)*(35 + 33*t2) + 
                  4*s*(-4 + 19*t2 + 15*Power(t2,2))) + 
               Power(t1,2)*(8 + 113*t2 + 774*Power(t2,2) + 
                  1277*Power(t2,3) + 171*Power(t2,4) + 
                  Power(s,2)*(-19 + 74*t2 - 63*Power(t2,2)) - 
                  s*(92 + 247*t2 + 82*Power(t2,2) + 234*Power(t2,3))) + 
               t1*(51 + 91*t2 + 157*Power(t2,2) - 170*Power(t2,3) - 
                  767*Power(t2,4) - 268*Power(t2,5) + 
                  Power(s,2)*t2*(-30 + 35*t2 - 32*Power(t2,2)) + 
                  2*s*(7 + 2*t2 + 153*Power(t2,2) + 157*Power(t2,3) + 
                     64*Power(t2,4))))) + 
         Power(s2,2)*(4*Power(t1,6) + 
            t2*(16 - 4*(-1 + 6*s + 6*s1)*t2 + 
               (21 - 60*Power(s,2) - 14*s1 + 24*Power(s1,2) + 
                  2*s*(65 + 6*s1))*Power(t2,2) - 
               (101 - 164*Power(s,2) + 24*Power(s,3) - 41*s1 + 
                  26*Power(s1,2) + 4*Power(s1,3) + s*(15 + 32*s1))*
                Power(t2,3) + 
               (19 + 36*Power(s,3) + Power(s,2)*(18 - 8*s1) + 40*s1 - 
                  11*Power(s1,2) + 6*Power(s1,3) - 
                  2*s*(81 - 6*s1 + 5*Power(s1,2)))*Power(t2,4) + 
               (11 + 4*Power(s,3) - 4*s1 - 13*Power(s1,2) - 
                  Power(s,2)*(48 + 7*s1) + 
                  s*(21 + 40*s1 + Power(s1,2)))*Power(t2,5) + 
               (12 + 2*Power(s,2) + s1 + 6*Power(s1,2) + 
                  s*(-40 + 9*s1))*Power(t2,6) + (2 - 6*s1)*Power(t2,7)) \
+ Power(t1,5)*(28 + 20*Power(s,2) + 20*Power(s1,2) + 101*t2 - 
               172*Power(t2,2) + 9*s1*(26 + 19*t2) - 
               s*(166 + 40*s1 + 171*t2)) - 
            t1*(12 + (3 + 70*s - 72*Power(s,2))*t2 + 
               (-287 + 201*s + 332*Power(s,2) - 32*Power(s,3))*
                Power(t2,2) + 
               (-383 - 767*s + 295*Power(s,2) + 90*Power(s,3))*
                Power(t2,3) + 
               (331 - 413*s - 179*Power(s,2) + 55*Power(s,3))*
                Power(t2,4) + 
               (174 - 460*s - 171*Power(s,2) + 28*Power(s,3))*
                Power(t2,5) + (53 - 7*s + 7*Power(s,2))*Power(t2,6) + 
               (29 - 3*s)*Power(t2,7) + 
               Power(s1,3)*Power(t2,2)*
                (-32 + 50*t2 + 8*Power(t2,2) + 9*Power(t2,3)) - 
               Power(s1,2)*t2*
                (-132 + 4*(43 + 6*s)*t2 + (175 + 134*s)*Power(t2,2) + 
                  (163 - 21*s)*Power(t2,3) - (77 + 5*s)*Power(t2,4) + 
                  5*Power(t2,5)) + 
               s1*(-84 + 2*(5 + 18*s)*t2 + 
                  (317 + 132*s - 24*Power(s,2))*Power(t2,2) + 
                  (91 - 34*s + 62*Power(s,2))*Power(t2,3) + 
                  (259 + 216*s - 79*Power(s,2))*Power(t2,4) + 
                  (8 + 282*s - 42*Power(s,2))*Power(t2,5) + 
                  4*(-29 + 3*s)*Power(t2,6) - 20*Power(t2,7))) + 
            Power(t1,4)*(624 + Power(s,3)*(46 - 27*t2) + 2224*t2 + 
               281*Power(t2,2) + 227*Power(t2,3) + 52*Power(t2,4) + 
               Power(s1,3)*(62 + 27*t2) + 
               Power(s1,2)*(204 - 25*t2 - 158*Power(t2,2)) - 
               Power(s,2)*(188 + s1*(30 - 81*t2) + 89*t2 + 
                  158*Power(t2,2)) + 
               s1*(-58 - 1429*t2 - 1267*Power(t2,2) + 30*Power(t2,3)) + 
               s*(-14 + 657*t2 + 685*Power(t2,2) - 30*Power(t2,3) - 
                  3*Power(s1,2)*(26 + 27*t2) + 2*s1*t2*(57 + 158*t2))) - 
            Power(t1,3)*(-16 + 874*t2 + 1987*Power(t2,2) + 
               1787*Power(t2,3) + 150*Power(t2,4) + 71*Power(t2,5) + 
               Power(s1,3)*(-98 + 133*t2 + 121*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(s,3)*(18 + 67*t2 + 69*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s1,2)*(-172 + 412*t2 + 433*Power(t2,2) - 
                  278*Power(t2,3) - 26*Power(t2,4)) + 
               s1*(-2 + 205*t2 - 267*Power(t2,2) - 1978*Power(t2,3) - 
                  240*Power(t2,4) + 19*Power(t2,5)) - 
               Power(s,2)*(-76 + 436*t2 + 639*Power(t2,2) + 
                  186*Power(t2,3) + 26*Power(t2,4) + 
                  s1*(-74 + 249*t2 + 17*Power(t2,2) + 9*Power(t2,3))) + 
               s*(-310 - 1973*t2 - 367*Power(t2,2) + 660*Power(t2,3) + 
                  52*Power(t2,4) - 19*Power(t2,5) + 
                  Power(s1,2)*
                   (118 + 49*t2 - 173*Power(t2,2) + 9*Power(t2,3)) + 
                  s1*(64 + 976*t2 + 362*Power(t2,2) + 464*Power(t2,3) + 
                     52*Power(t2,4)))) + 
            Power(t1,2)*(-84 - 504*t2 + 59*Power(t2,2) + 
               1244*Power(t2,3) + 416*Power(t2,4) + 444*Power(t2,5) + 
               28*Power(t2,6) + 
               Power(s1,3)*(-40 + 6*t2 + 53*Power(t2,2) + 
                  95*Power(t2,3) + 9*Power(t2,4)) + 
               Power(s,3)*(-4 + 10*t2 + 81*Power(t2,2) + 
                  119*Power(t2,3) + 29*Power(t2,4)) + 
               Power(s1,2)*(188 - 519*t2 - 691*Power(t2,2) + 
                  383*Power(t2,3) + 7*Power(t2,4) - 39*Power(t2,5)) - 
               Power(s,2)*(-60 - (181 + 18*s1)*t2 + 
                  (19 - 35*s1)*Power(t2,2) + 
                  (765 + 197*s1)*Power(t2,3) + 
                  (155 + 49*s1)*Power(t2,4) + 17*Power(t2,5)) + 
               s1*(90 + 177*t2 + 469*Power(t2,2) + 641*Power(t2,3) - 
                  572*Power(t2,4) - 457*Power(t2,5) + 7*Power(t2,6)) + 
               s*(94 - 613*t2 - 1339*Power(t2,2) - 2319*Power(t2,3) + 
                  30*Power(t2,4) + 91*Power(t2,5) - 20*Power(t2,6) + 
                  Power(s1,2)*
                   (-84 - 178*t2 - 41*Power(t2,2) - 17*Power(t2,3) + 
                     11*Power(t2,4)) + 
                  2*s1*(12 + 5*t2 + 251*Power(t2,2) + 536*Power(t2,3) + 
                     124*Power(t2,4) + 28*Power(t2,5))))) + 
         Power(s2,3)*(4 + Power(t1,5)*(39 - 31*s + 31*s1 - 16*t2) + 
            4*(-2 + 3*s - 12*s1)*t2 + 
            (31 - 24*Power(s,2) + 38*s1 + 60*Power(s1,2) - 
               2*s*(-7 + 6*s1))*Power(t2,2) + 
            (-151 + 124*Power(s,2) - 16*Power(s,3) + 49*s1 - 
               68*Power(s1,2) - 16*Power(s1,3) + s*(89 + 28*s1))*
             Power(t2,3) + (-61 + 44*Power(s,3) - 
               12*Power(s,2)*(-6 + s1) + 120*s1 - 60*Power(s1,2) + 
               22*Power(s1,3) - 4*s*(85 - 2*s1 + 10*Power(s1,2)))*
             Power(t2,4) + (100 + 6*Power(s,3) + 26*s1 - 
               44*Power(s1,2) + 3*Power(s1,3) - 
               2*Power(s,2)*(61 + 4*s1) + 
               2*s*(-14 + 57*s1 + 3*Power(s1,2)))*Power(t2,5) + 
            (42 - 14*Power(s,2) - 25*s1 + 32*Power(s1,2) + 
               s*(-118 + 35*s1))*Power(t2,6) + 
            (39 + 15*s - 41*s1)*Power(t2,7) - 2*Power(t2,8) - 
            Power(t1,4)*(-278 + 15*Power(s,3) - 15*Power(s1,3) + 
               233*t2 + 37*Power(t2,2) - 112*Power(t2,3) + 
               Power(s1,2)*(3 + 50*t2) + 
               Power(s,2)*(11 - 45*s1 + 50*t2) + 
               s1*(221 + 653*t2 + 150*Power(t2,2)) + 
               s*(-145 + 45*Power(s1,2) - 489*t2 - 150*Power(t2,2) - 
                  2*s1*(7 + 50*t2))) + 
            Power(t1,3)*(-196 - 1943*t2 - 2293*Power(t2,2) + 
               224*Power(t2,3) - 157*Power(t2,4) - 16*Power(t2,5) + 
               Power(s,3)*(-3 - 25*t2 + 11*Power(t2,2)) - 
               Power(s1,3)*(45 + 131*t2 + 11*Power(t2,2)) + 
               Power(s1,2)*(-54 - 359*t2 + 226*Power(t2,2) + 
                  62*Power(t2,3)) + 
               s1*(31 + 395*t2 + 2016*Power(t2,2) + 730*Power(t2,3) - 
                  7*Power(t2,4)) + 
               Power(s,2)*(74 + 369*t2 + 218*Power(t2,2) + 
                  62*Power(t2,3) + s1*(113 - 81*t2 - 33*Power(t2,2))) + 
               s*(233 - 235*t2 - 1022*Power(t2,2) - 454*Power(t2,3) + 
                  7*Power(t2,4) + 
                  Power(s1,2)*(-65 + 237*t2 + 33*Power(t2,2)) - 
                  2*s1*(118 - 7*t2 + 222*Power(t2,2) + 62*Power(t2,3)))) \
+ Power(t1,2)*(-50 + 93*t2 + 1316*Power(t2,2) + 2138*Power(t2,3) + 
               1048*Power(t2,4) - 23*Power(t2,5) + 19*Power(t2,6) + 
               Power(s,3)*(6 + 29*t2 + 83*Power(t2,2) + 
                  21*Power(t2,3)) + 
               Power(s1,3)*(-78 + 19*t2 + 187*Power(t2,2) + 
                  45*Power(t2,3)) + 
               Power(s1,2)*(-51 - 439*t2 + 549*Power(t2,2) + 
                  123*Power(t2,3) - 125*Power(t2,4)) + 
               s1*(113 + 45*t2 + 269*Power(t2,2) - 1132*Power(t2,3) - 
                  1223*Power(t2,4) - 43*Power(t2,5)) + 
               Power(s,2)*(9 - 79*t2 - 523*Power(t2,2) - 
                  389*Power(t2,3) - 105*Power(t2,4) + 
                  s1*(-10 + 65*t2 - 169*Power(t2,2) + 3*Power(t2,3))) - 
               s*(133 + 601*t2 + 2263*Power(t2,2) - 692*Power(t2,3) - 
                  487*Power(t2,4) + Power(t2,5) + 
                  Power(s1,2)*
                   (30 - 111*t2 + 101*Power(t2,2) + 69*Power(t2,3)) - 
                  2*s1*(-55 + 231*t2 + 406*Power(t2,2) + 
                     141*Power(t2,3) + 115*Power(t2,4)))) + 
            t1*(21 + 123*t2 + 479*Power(t2,2) - 443*Power(t2,3) - 
               612*Power(t2,4) - 459*Power(t2,5) - 119*Power(t2,6) + 
               3*Power(t2,7) - 
               Power(s,3)*t2*
                (-8 + 58*t2 + 37*Power(t2,2) + 42*Power(t2,3)) - 
               Power(s1,3)*t2*
                (-52 + 42*t2 + 41*Power(t2,2) + 42*Power(t2,3)) + 
               Power(s1,2)*(-84 + 8*t2 + 435*Power(t2,2) + 
                  265*Power(t2,3) - 241*Power(t2,4) + 18*Power(t2,5)) + 
               s1*(46 - 283*t2 - 145*Power(t2,2) - 391*Power(t2,3) + 
                  24*Power(t2,4) + 477*Power(t2,5) + 117*Power(t2,6)) + 
               Power(s,2)*t2*
                (-112 - 183*t2 + 263*Power(t2,2) + 347*Power(t2,3) + 
                  52*Power(t2,4) + 
                  s1*(-12 + 22*t2 + 11*Power(t2,2) + 58*Power(t2,3))) + 
               s*(10 - 89*t2 + 653*Power(t2,2) + 735*Power(t2,3) + 
                  1076*Power(t2,4) - 245*Power(t2,5) - 20*Power(t2,6) + 
                  Power(s1,2)*t2*
                   (48 + 146*t2 - 37*Power(t2,2) + 26*Power(t2,3)) - 
                  2*s1*(6 + 32*t2 + 43*Power(t2,2) + 171*Power(t2,3) + 
                     251*Power(t2,4) + 41*Power(t2,5))))) + 
         s2*(8*Power(t1,6)*(-6 + 8*s - 8*s1 + 15*t2) + 
            Power(t2,2)*(1 + (-1 + s)*t2)*
             (-16 - 4*(8*s - 3*(3 + s1))*t2 - 
               (13 - 62*s + 16*Power(s,2) + 20*s1)*Power(t2,2) + 
               2*(-3 + 7*Power(s,2) + 3*s1 - s*s1)*Power(t2,3) + 
               (1 + Power(s,2) + 2*s1 - 2*s*(3 + s1))*Power(t2,4) + 
               2*(-1 + s)*Power(t2,5)) - 
            t1*(28 - 20*(-1 + 3*s + 3*s1)*t2 + 
               (49 - 132*Power(s,2) + s*(302 - 36*s1) + 26*s1 + 
                  72*Power(s1,2))*Power(t2,2) + 
               (-217 - 52*Power(s,3) + Power(s,2)*(340 - 48*s1) + 
                  25*s1 - 116*Power(s1,2) - 8*Power(s1,3) + 
                  s*(11 + 136*s1 + 12*Power(s1,2)))*Power(t2,3) + 
               (15 + 54*Power(s,3) + 77*s1 + 30*Power(s1,2) + 
                  12*Power(s1,3) + Power(s,2)*(141 + 70*s1) - 
                  s*(339 + 142*s1 + 42*Power(s1,2)))*Power(t2,4) + 
               (53 + 31*Power(s,3) + 17*s1 - 34*Power(s1,2) - 
                  Power(s,2)*(69 + 55*s1) + 
                  s*(-25 + 86*s1 + 8*Power(s1,2)))*Power(t2,5) + 
               (12 + 7*Power(s,3) - 9*s1 + 8*Power(s1,2) - 
                  Power(s,2)*(16 + 11*s1) + 
                  s*(-57 + 42*s1 + 4*Power(s1,2)))*Power(t2,6) + 
               4*(2 + 2*Power(s,2) - 2*s1 - s*(3 + s1))*Power(t2,7)) + 
            Power(t1,5)*(-756 + 16*Power(s,3) - 16*Power(s1,3) - 
               16*Power(s,2)*(1 + 3*s1 - 7*t2) - 560*t2 - 
               163*Power(t2,2) - 56*Power(t2,3) + 
               16*Power(s1,2)*(-5 + 7*t2) + 
               s1*(388 + 1028*t2 - 39*Power(t2,2)) + 
               s*(-164 + 48*Power(s1,2) - 476*t2 + 39*Power(t2,2) - 
                  32*s1*(-3 + 7*t2))) + 
            Power(t1,4)*(200 + 908*t2 + 1170*Power(t2,2) + 
               273*Power(t2,3) + 85*Power(t2,4) + 
               Power(s1,3)*(-36 + 116*t2 - 7*Power(t2,2)) + 
               Power(s,3)*(-12 + 84*t2 + 7*Power(t2,2)) + 
               Power(s1,2)*(140 + 480*t2 - 233*Power(t2,2) - 
                  38*Power(t2,3)) + 
               s1*(-52 + 376*t2 - 1323*Power(t2,2) - 351*Power(t2,3) + 
                  38*Power(t2,4)) - 
               Power(s,2)*(84 + 656*t2 + 33*Power(t2,2) + 
                  38*Power(t2,3) + s1*(124 + 52*t2 + 21*Power(t2,2))) + 
               s*(-604 - 920*t2 + 263*Power(t2,2) + 107*Power(t2,3) - 
                  38*Power(t2,4) + 
                  Power(s1,2)*(172 - 148*t2 + 21*Power(t2,2)) + 
                  s1*(376 + 464*t2 + 266*Power(t2,2) + 76*Power(t2,3)))) \
- Power(t1,3)*(-176 - 324*t2 + 988*Power(t2,2) + 81*Power(t2,3) + 
               459*Power(t2,4) + 65*Power(t2,5) + 
               Power(s1,3)*(-52 + 80*t2 + 47*Power(t2,2) + 
                  29*Power(t2,3) - 5*Power(t2,4)) + 
               Power(s,3)*(-12 + 80*t2 + 129*Power(t2,2) + 
                  55*Power(t2,3) + 5*Power(t2,4)) + 
               Power(s1,2)*(24 - 892*t2 + 310*Power(t2,2) + 
                  97*Power(t2,3) - 90*Power(t2,4) + 4*Power(t2,5)) + 
               s1*(36 + 64*t2 + 1071*Power(t2,2) - 249*Power(t2,3) - 
                  535*Power(t2,4) + 14*Power(t2,5)) + 
               Power(s,2)*(24 + 260*t2 - 634*Power(t2,2) - 
                  167*Power(t2,3) - 4*Power(t2,4) + 4*Power(t2,5) + 
                  s1*(68 + 96*t2 - 267*Power(t2,2) - 81*Power(t2,3) - 
                     15*Power(t2,4))) + 
               s*(-148 - 1136*t2 - 1959*Power(t2,2) - 383*Power(t2,3) + 
                  94*Power(t2,4) - 40*Power(t2,5) + 
                  Power(s1,2)*
                   (-164 - 192*t2 + 91*Power(t2,2) - 3*Power(t2,3) + 
                     15*Power(t2,4)) + 
                  s1*(-160 + 616*t2 + 716*Power(t2,2) + 
                     302*Power(t2,3) + 94*Power(t2,4) - 8*Power(t2,5)))) \
+ Power(t1,2)*(8 - 196*t2 - 610*Power(t2,2) + 443*Power(t2,3) + 
               170*Power(t2,4) + 3*Power(t2,5) + 64*Power(t2,6) + 
               Power(s1,3)*t2*
                (-4 + 2*t2 + 33*Power(t2,2) + 14*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s,3)*t2*(-4 + 54*t2 + 103*Power(t2,2) + 
                  61*Power(t2,3) + 11*Power(t2,4)) + 
               s1*(-172 + 616*t2 - 261*Power(t2,2) + 403*Power(t2,3) + 
                  152*Power(t2,4) - 133*Power(t2,5) - 32*Power(t2,6)) + 
               Power(s1,2)*(108 - 188*t2 - 137*Power(t2,2) - 
                  221*Power(t2,3) + 87*Power(t2,4) - 23*Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s,2)*(-36 + (292 - 60*s1)*t2 + 
                  (443 + 166*s1)*Power(t2,2) + 
                  (75 - 101*s1)*Power(t2,3) - 
                  (279 + 103*s1)*Power(t2,4) + 
                  (17 - 24*s1)*Power(t2,5) + 10*Power(t2,6)) + 
               s*(44 + 216*t2 - 487*Power(t2,2) - 855*Power(t2,3) - 
                  517*Power(t2,4) - 77*Power(t2,5) - 14*Power(t2,6) + 
                  Power(s1,2)*t2*
                   (-60 - 78*t2 - 67*Power(t2,2) + 28*Power(t2,3) + 
                     15*Power(t2,4)) + 
                  s1*(120 + 216*t2 - 106*Power(t2,2) + 122*Power(t2,3) + 
                     367*Power(t2,4) + 62*Power(t2,5) - 12*Power(t2,6)))))\
)*R2q(1 - s2 + t1 - t2))/
     ((-1 + t1)*(s - s2 + t1)*(1 - s2 + t1 - t2)*(s - s1 + t2)*
       Power(t1 - s2*t2,2)*Power(-Power(s2,2) + 4*t1 - 2*s2*t2 - 
         Power(t2,2),2)) - (8*(-8*Power(t1,9)*(2*s - 2*s1 + t2) + 
         Power(t2,5)*Power(1 + (-1 + s)*t2,3)*(2 - 3*t2 + Power(t2,2)) + 
         Power(s2,12)*(Power(s1,3) + s1*Power(t2,2) - 2*Power(t2,3)) - 
         t1*(-1 + t2)*Power(t2,3)*(1 + (-1 + s)*t2)*
          (-20 + (50 - 40*s)*t2 + 
            (-36 - 20*Power(s,2) + s*(65 - 6*s1) + 5*s1)*Power(t2,2) + 
            (8 + 9*Power(s,2) - 9*s1 + 2*s*(-5 + 4*s1))*Power(t2,3) + 
            (-1 + 2*Power(s,2) + 4*s1 - s*(3 + 4*s1))*Power(t2,4) + 
            (-1 + s)*Power(t2,5)) + 
         Power(t1,2)*t2*(60 + 90*(-3 + 2*s)*t2 + 
            10*(44 + 18*Power(s,2) - 5*s1 + s*(-68 + 6*s1))*
             Power(t2,2) + 10*
             (-32 + 6*Power(s,3) + s*(77 - 25*s1) + 19*s1 + 
               Power(s,2)*(-49 + 6*s1))*Power(t2,3) + 
            (116 - 80*Power(s,3) - 257*s1 + 3*Power(s1,2) - 
               16*Power(s,2)*(-17 + 9*s1) + 
               s*(-209 + 397*s1 - 4*Power(s1,2)))*Power(t2,4) + 
            (-35 - 2*Power(s,3) + 151*s1 - 11*Power(s1,2) + 
               5*Power(s,2)*(17 + 26*s1) + 
               s*(-58 - 275*s1 + 12*Power(s1,2)))*Power(t2,5) + 
            (1 + 25*Power(s,3) - 40*s1 + 13*Power(s1,2) - 
               2*Power(s,2)*(28 + 19*s1) + 
               s*(18 + 61*s1 - 13*Power(s1,2)))*Power(t2,6) + 
            (9 + Power(s,3) + Power(s,2)*(9 - 7*s1) + 5*s1 - 
               5*Power(s1,2) + s*(-16 + 8*s1 + 5*Power(s1,2)))*
             Power(t2,7) + (-1 + 4*Power(s,2) + s1 - s*(5 + s1))*
             Power(t2,8)) - 2*Power(t1,8)*
          (-88 + 8*Power(s,3) - 8*Power(s1,3) - 
            4*Power(s,2)*(8 + 6*s1 - 3*t2) + 54*t2 - 9*Power(t2,2) - 
            4*Power(t2,3) + 4*Power(s1,2)*(-4 + 3*t2) + 
            s*(124 + 24*Power(s1,2) - 24*s1*(-2 + t2) - 44*t2 - 
               5*Power(t2,2)) + s1*(28 + 40*t2 + 5*Power(t2,2))) - 
         Power(s2,11)*(Power(s1,3)*(1 + 4*t1 - 8*t2) + 
            s1*t2*((11 - 5*t2)*t2 + t1*(2 + t2) + s*(3 - 3*t1 + t2)) + 
            Power(s1,2)*(3 + (-4 + t1)*t2 - 2*Power(t2,2) - 
               s*(3*t1 + t2)) + 
            Power(t2,2)*(1 + s*(3 - 4*t1 - 2*t2) - 10*t2 + 
               17*Power(t2,2) - 2*t1*(3 + 5*t2))) + 
         2*Power(t1,7)*(32 - 32*t2 + 60*Power(t2,2) + 25*Power(t2,3) - 
            9*Power(t2,4) - Power(s1,3)*(12 + 4*t2 + 5*Power(t2,2)) + 
            Power(s,3)*(-28 + 8*t2 + 5*Power(t2,2)) + 
            Power(s1,2)*(232 - 54*t2 + Power(t2,2) + 9*Power(t2,3)) + 
            s1*(-332 - 340*t2 + 179*Power(t2,2) + 25*Power(t2,3) - 
               3*Power(t2,4)) + 
            Power(s,2)*(168 - 182*t2 + Power(t2,2) + 9*Power(t2,3) + 
               s1*(76 - 20*t2 - 15*Power(t2,2))) + 
            s*(-28 + 440*t2 - 53*Power(t2,2) - 26*Power(t2,3) + 
               3*Power(t2,4) + 
               Power(s1,2)*(-36 + 16*t2 + 15*Power(t2,2)) - 
               2*s1*(312 - 142*t2 + Power(t2,2) + 9*Power(t2,3)))) - 
         Power(t1,6)*(576 - 884*t2 + 58*Power(t2,2) + 228*Power(t2,3) + 
            80*Power(t2,4) - 7*Power(t2,5) + 
            Power(s1,3)*(-56 + 2*Power(t2,2) - 6*Power(t2,3)) + 
            2*Power(s,3)*(-44 - 60*t2 - 15*Power(t2,2) + 
               6*Power(t2,3)) + 
            4*Power(s1,2)*(-4 + 120*t2 + 17*Power(t2,2) - 
               16*Power(t2,3) + 5*Power(t2,4)) + 
            s1*(32 + 344*t2 - 1712*Power(t2,2) + 68*Power(t2,3) + 
               196*Power(t2,4) - 9*Power(t2,5)) + 
            Power(s,2)*(-240 + 784*t2 - 452*Power(t2,2) - 
               212*Power(t2,3) + 26*Power(t2,4) + 
               s1*(56 + 368*t2 + 94*Power(t2,2) - 30*Power(t2,3))) + 
            s*(-448 - 648*t2 + 1292*Power(t2,2) + 126*Power(t2,3) - 
               120*Power(t2,4) + 9*Power(t2,5) + 
               6*Power(s1,2)*
                (36 - 52*t2 - 11*Power(t2,2) + 4*Power(t2,3)) - 
               2*s1*(-992 + 920*t2 + 8*Power(t2,2) - 170*Power(t2,3) + 
                  23*Power(t2,4)))) - 
         Power(t1,5)*(-96 - 280*t2 - 216*Power(t2,2) + 828*Power(t2,3) - 
            262*Power(t2,4) - 91*Power(t2,5) - 3*Power(t2,6) - 
            2*Power(s1,3)*(12 - 4*t2 - 33*Power(t2,2) + 6*Power(t2,3) + 
               4*Power(t2,4) + Power(t2,5)) + 
            Power(s,3)*(-56 + 96*t2 + 242*Power(t2,2) + 
               42*Power(t2,3) + 6*Power(t2,4) + Power(t2,5)) + 
            2*Power(s1,2)*(40 + 76*t2 - 224*Power(t2,2) - 
               18*Power(t2,3) + 16*Power(t2,4) + Power(t2,6)) - 
            2*s1*(-88 + 276*t2 - 48*Power(t2,2) - 203*Power(t2,3) - 
               270*Power(t2,4) + 82*Power(t2,5) + 7*Power(t2,6)) + 
            Power(s,2)*(-304 + 328*t2 - 488*Power(t2,2) + 
               76*Power(t2,3) + 342*Power(t2,4) + 29*Power(t2,5) + 
               Power(t2,6) - 
               2*s1*(-140 - 164*t2 + 289*Power(t2,2) + 96*Power(t2,3) + 
                  12*Power(t2,4) + 2*Power(t2,5))) + 
            s*(496 - 808*t2 + 840*Power(t2,2) - 514*Power(t2,3) - 
               476*Power(t2,4) + 96*Power(t2,5) + 6*Power(t2,6) + 
               Power(s1,2)*(312 - 560*t2 + 110*Power(t2,2) + 
                  210*Power(t2,3) + 26*Power(t2,4) + 5*Power(t2,5)) - 
               s1*(-416 + 736*t2 + 488*Power(t2,2) - 632*Power(t2,3) + 
                  242*Power(t2,4) + 43*Power(t2,5) + 3*Power(t2,6)))) + 
         Power(t1,4)*(144 + 124*t2 - 490*Power(t2,2) + 52*Power(t2,3) + 
            80*Power(t2,4) + 167*Power(t2,5) - 90*Power(t2,6) - 
            6*Power(t2,7) + Power(s1,3)*
             (-8 + 16*t2 - 30*Power(t2,2) + 6*Power(t2,3) + 
               36*Power(t2,4) - 14*Power(t2,5) - 3*Power(t2,6)) + 
            2*Power(s,3)*(-4 + 12*t2 - 63*Power(t2,2) + 
               100*Power(t2,3) + 46*Power(t2,4) + 5*Power(t2,5) + 
               Power(t2,6)) + 
            s1*(-40 + 8*t2 + 58*Power(t2,2) - 402*Power(t2,3) + 
               258*Power(t2,4) + 168*Power(t2,5) + 28*Power(t2,6) - 
               22*Power(t2,7)) + 
            Power(s1,2)*(-48 + 280*t2 - 348*Power(t2,2) + 
               272*Power(t2,3) - 208*Power(t2,4) + 21*Power(t2,5) + 
               5*Power(t2,6) + 2*Power(t2,7)) + 
            s*(-72 + 448*t2 + 430*Power(t2,2) - 770*Power(t2,3) + 
               202*Power(t2,4) - 334*Power(t2,5) - 35*Power(t2,6) + 
               14*Power(t2,7) + 
               2*Power(s1,2)*
                (36 - 100*t2 + 223*Power(t2,2) - 242*Power(t2,3) + 
                  60*Power(t2,4) + 37*Power(t2,5) + 4*Power(t2,6)) + 
               s1*(-352 + 544*t2 - 392*Power(t2,3) + 44*Power(t2,4) + 
                  71*Power(t2,5) - 62*Power(t2,6) - 7*Power(t2,7))) + 
            Power(s,2)*(s1*(72 - 288*t2 + 542*Power(t2,2) + 
                  54*Power(t2,3) - 320*Power(t2,4) - 58*Power(t2,5) - 
                  7*Power(t2,6)) + 
               4*(-12 - 46*t2 + 43*Power(t2,2) + 6*Power(t2,3) - 
                  40*Power(t2,4) + 46*Power(t2,5) + 19*Power(t2,6) + 
                  Power(t2,7)))) - 
         Power(t1,3)*(Power(s,3)*Power(t2,2)*
             (40 - 30*t2 - 90*Power(t2,2) + 109*Power(t2,3) + 
               17*Power(t2,4) + Power(t2,5)) + 
            Power(s,2)*t2*(108 + 30*(-13 + 6*s1)*t2 - 
               2*(13 + 230*s1)*Power(t2,2) + 
               (414 + 464*s1)*Power(t2,3) - 43*(3 + 2*s1)*Power(t2,4) - 
               84*s1*Power(t2,5) + (51 - 6*s1)*Power(t2,6) + 
               6*Power(t2,7)) + 
            s*(72 + 24*(-19 + 9*s1)*t2 + (366 - 844*s1)*Power(t2,2) + 
               (422 + 1204*s1 - 30*Power(s1,2))*Power(t2,3) + 
               2*(-83 - 343*s1 + 61*Power(s1,2))*Power(t2,4) + 
               (-177 + 37*s1 - 147*Power(s1,2))*Power(t2,5) + 
               (-3 + 71*s1 + 51*Power(s1,2))*Power(t2,6) + 
               (-71 - 9*s1 + 8*Power(s1,2))*Power(t2,7) + 
               (2 - 5*s1)*Power(t2,8)) - 
            (-1 + t2)*(-96 + 48*t2 + 96*Power(t2,2) + 82*Power(t2,3) - 
               96*Power(t2,4) - 29*Power(t2,5) + 2*Power(t2,6) + 
               8*Power(t2,7) + 
               2*Power(s1,3)*Power(t2,2)*
                (2 - 2*t2 - 2*Power(t2,2) + Power(t2,4)) + 
               2*Power(s1,2)*t2*
                (-18 + 15*t2 + 26*Power(t2,2) - 36*Power(t2,3) + 
                  26*Power(t2,4) - 3*Power(t2,5)) + 
               s1*(72 - 216*t2 + 398*Power(t2,2) - 286*Power(t2,3) + 
                  60*Power(t2,4) - 75*Power(t2,5) - 21*Power(t2,6) + 
                  4*Power(t2,7)))) + 
         Power(s2,10)*(Power(s1,3)*
             (-3 + 6*Power(t1,2) - 4*t2 + 28*Power(t2,2) - 
               t1*(8 + 31*t2)) + 
            Power(s1,2)*(3 - 5*(6 + s)*t2 + 4*(5 + 2*s)*Power(t2,2) + 
               17*Power(t2,3) + Power(t1,2)*(1 - 9*s + 3*t2) + 
               t1*(8 - 21*t2 - 11*Power(t2,2) + s*(-7 + 24*t2))) + 
            s1*(3 + Power(s,2)*t1*(3*t1 - t2) - 8*t2 + 20*Power(t2,2) - 
               74*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,2)*(1 + 2*t2 - 3*Power(t2,2)) + 
               t1*t2*(24 + 11*t2 + 14*Power(t2,2)) - 
               s*(Power(t1,2)*(3 + 8*t2) + 
                  t1*(3 - 10*t2 - 30*Power(t2,2)) + 
                  2*t2*(-2 + 7*t2 + 4*Power(t2,2)))) + 
            t2*(-2*Power(t1,2)*(3 + 15*t2 + 7*Power(t2,2)) + 
               t2*(11 - 11*t2 + 75*Power(t2,2) - 63*Power(t2,3)) + 
               t1*(2 - 29*t2 + 32*Power(t2,2) + 67*Power(t2,3)) + 
               Power(s,2)*(3*Power(t1,2) + t2 - t1*(3 + 5*t2)) + 
               s*(3 + 2*t2 - 28*Power(t2,2) + 17*Power(t2,3) - 
                  Power(t1,2)*(8 + 5*t2) + t1*(3 - 14*t2 + 9*Power(t2,2))\
))) + Power(s2,9)*(-1 + 4*t2 - 5*s*t2 - 22*Power(t2,2) + 
            34*s*Power(t2,2) + 6*Power(s,2)*Power(t2,2) + 
            96*Power(t2,3) + 21*s*Power(t2,3) + 
            2*Power(s,2)*Power(t2,3) - 38*Power(t2,4) - 
            125*s*Power(t2,4) + 239*Power(t2,5) + 63*s*Power(t2,5) - 
            133*Power(t2,6) + 
            Power(t1,2)*(-1 + 
               (28 + 30*s + 8*Power(s,2) - 2*Power(s,3))*t2 + 
               (9 - 33*s + 30*Power(s,2))*Power(t2,2) + 
               (-249 + 17*s)*Power(t2,3) - 66*Power(t2,4)) + 
            Power(t1,3)*(2 + Power(s,3) + 30*t2 + 42*Power(t2,2) + 
               6*Power(t2,3) - Power(s,2)*(3 + 4*t2) + 
               s*(4 + 10*t2 - Power(t2,2))) - 
            Power(s1,3)*(-1 + 4*Power(t1,3) + 18*t2 + Power(t2,2) - 
               56*Power(t2,3) - Power(t1,2)*(41 + 45*t2) + 
               t1*(-22 + 58*t2 + 105*Power(t2,2))) + 
            t1*t2*(-23 + 7*t2 + Power(s,3)*t2 - 319*Power(t2,2) + 
               65*Power(t2,3) + 192*Power(t2,4) + 
               Power(s,2)*(8 - 19*t2 - 24*Power(t2,2)) - 
               2*s*(6 - 44*t2 + 51*Power(t2,2) + 18*Power(t2,3))) + 
            Power(s1,2)*(9 - (-3 + s)*t2 - 2*(58 + 17*s)*Power(t2,2) + 
               (26 + 28*s)*Power(t2,3) + 63*Power(t2,4) - 
               t1*(-30 + s - 52*t2 + 44*s*t2 + 144*Power(t2,2) - 
                  85*s*Power(t2,2) + 47*Power(t2,3)) + 
               Power(t1,3)*(9*s - 3*(1 + t2)) + 
               Power(t1,2)*(1 + 56*t2 + 19*Power(t2,2) - 
                  18*s*(1 + 4*t2))) - 
            s1*(3 - 4*(9 + 4*s)*t2 + 
               (57 - 5*s + 2*Power(s,2))*Power(t2,2) + 
               (-118 + 5*s)*Power(t2,3) + (202 + 27*s)*Power(t2,4) + 
               42*Power(t2,5) + 
               Power(t1,3)*(1 + 6*Power(s,2) - 6*t2 - 5*Power(t2,2) - 
                  s*(8 + 7*t2)) + 
               Power(t1,2)*(13 + Power(s,2)*(8 - 29*t2) + 43*t2 + 
                  65*Power(t2,2) + 64*Power(t2,3) + 
                  s*(-9 + 87*t2 + 62*Power(t2,2))) + 
               t1*(4 + 6*t2 - 223*Power(t2,2) - 145*Power(t2,3) - 
                  122*Power(t2,4) + 3*Power(s,2)*t2*(2 + 3*t2) - 
                  s*(8 - 46*t2 + 52*Power(t2,2) + 111*Power(t2,3))))) + 
         Power(s2,8)*(1 + 12*Power(t1,2) - 9*Power(t1,3) - 
            10*Power(t1,4) - 14*t2 + 27*t1*t2 + 22*Power(t1,2)*t2 - 
            46*Power(t1,3)*t2 - 42*Power(t1,4)*t2 + 37*Power(t2,2) - 
            278*t1*Power(t2,2) + 530*Power(t1,2)*Power(t2,2) + 
            340*Power(t1,3)*Power(t2,2) - 18*Power(t1,4)*Power(t2,2) - 
            134*Power(t2,3) - 46*t1*Power(t2,3) + 
            557*Power(t1,2)*Power(t2,3) + 285*Power(t1,3)*Power(t2,3) + 
            309*Power(t2,4) - 1200*t1*Power(t2,4) - 
            792*Power(t1,2)*Power(t2,4) + 8*Power(t1,3)*Power(t2,4) - 
            54*Power(t2,5) + 66*t1*Power(t2,5) - 
            120*Power(t1,2)*Power(t2,5) + 419*Power(t2,6) + 
            305*t1*Power(t2,6) - 175*Power(t2,7) + 
            Power(s,3)*(-Power(t1,4) - 17*Power(t1,2)*Power(t2,2) + 
               2*Power(t2,3) + 4*t1*Power(t2,2)*(1 + t2) + 
               2*Power(t1,3)*(-1 + 5*t2)) + 
            Power(s1,3)*(2 + Power(t1,4) + t2 - 45*Power(t2,2) + 
               22*Power(t2,3) + 70*Power(t2,4) - 
               Power(t1,3)*(65 + 29*t2) + 
               Power(t1,2)*(-7 + 255*t2 + 146*Power(t2,2)) + 
               t1*(28 + 84*t2 - 181*Power(t2,2) - 203*Power(t2,3))) + 
            Power(s,2)*(Power(t1,4)*(4 + t2) + 
               Power(t1,3)*(3 - 70*t2 - 26*Power(t2,2)) + 
               Power(t2,2)*(-12 + 37*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*t2*(-2 + 71*t2 + 78*Power(t2,2)) - 
               t1*t2*(6 - 62*t2 + 28*Power(t2,2) + 41*Power(t2,3))) + 
            s*(Power(t1,4)*(-5 + 2*t2 + 2*Power(t2,2)) - 
               Power(t1,3)*(18 - 32*t2 + 22*Power(t2,2) + 
                  47*Power(t2,3)) + 
               Power(t1,2)*(1 - 65*t2 + 254*Power(t2,2) + 
                  124*Power(t2,3) + 155*Power(t2,4)) + 
               t2*(-11 - 25*t2 + 88*Power(t2,2) + 86*Power(t2,3) - 
                  329*Power(t2,4) + 133*Power(t2,5)) - 
               t1*(1 + 18*t2 + 154*Power(t2,2) - 652*Power(t2,3) + 
                  292*Power(t2,4) + 181*Power(t2,5))) + 
            Power(s1,2)*(-3 + 5*(12 + s)*t2 - (43 + 4*s)*Power(t2,2) - 
               (237 + 98*s)*Power(t2,3) + 8*(-5 + 7*s)*Power(t2,4) + 
               133*Power(t2,5) + Power(t1,4)*(3 - 3*s + t2) + 
               Power(t1,3)*(-33 - 78*t2 - 13*Power(t2,2) + 
                  s*(90 + 68*t2)) + 
               Power(t1,2)*(-102 + 165*t2 + 326*Power(t2,2) + 
                  29*Power(t2,3) - 2*s*(-42 + 65*t2 + 126*Power(t2,2))) \
+ t1*(-57 + 304*t2 + 273*Power(t2,2) - 420*Power(t2,3) - 
                  106*Power(t2,4) + 
                  s*(13 + 46*t2 - 114*Power(t2,2) + 172*Power(t2,3)))) + 
            s1*(-9 + (6 - 10*s)*t2 + 
               (133 + 107*s + 2*Power(s,2))*Power(t2,2) - 
               2*(69 + 29*s + 9*Power(s,2))*Power(t2,3) + 
               3*(98 + 31*s)*Power(t2,4) - (275 + 49*s)*Power(t2,5) - 
               140*Power(t2,6) + 
               Power(t1,4)*(-3 + 3*Power(s,2) - 10*t2 - 2*Power(t2,2) - 
                  s*(7 + 2*t2)) + 
               Power(t1,3)*(27 + 94*t2 + 214*Power(t2,2) + 
                  65*Power(t2,3) - Power(s,2)*(23 + 49*t2) + 
                  3*s*(13 + 60*t2 + 13*Power(t2,2))) + 
               Power(t1,2)*(4 - 292*t2 - 550*Power(t2,2) - 
                  616*Power(t2,3) - 285*Power(t2,4) + 
                  Power(s,2)*(6 - 32*t2 + 123*Power(t2,2)) - 
                  2*s*(-24 - 50*t2 + 238*Power(t2,2) + 71*Power(t2,3))) \
+ t1*(-36 + 23*t2 - 244*Power(t2,2) + 630*Power(t2,3) + 
                  519*Power(t2,4) + 382*Power(t2,5) + 
                  Power(s,2)*t2*(18 - 45*t2 - 29*Power(t2,2)) + 
                  s*(-4 + 10*t2 - 332*Power(t2,2) + 53*Power(t2,3) + 
                     203*Power(t2,4))))) + 
         Power(s2,7)*(3 - 5*t2 + 11*s*t2 - 45*Power(t2,2) - 
            94*s*Power(t2,2) - 2*Power(s,2)*Power(t2,2) + 
            14*Power(t2,3) - 45*s*Power(t2,3) - 
            42*Power(s,2)*Power(t2,3) - 308*Power(t2,4) + 
            55*s*Power(t2,4) + 80*Power(s,2)*Power(t2,4) + 
            12*Power(s,3)*Power(t2,4) + 513*Power(t2,5) + 
            173*s*Power(t2,5) - 3*Power(s,2)*Power(t2,5) - 
            18*Power(t2,6) - 539*s*Power(t2,6) + 435*Power(t2,7) + 
            175*s*Power(t2,7) - 147*Power(t2,8) - 
            Power(t1,5)*(s + Power(s,2) + 4*s*t2 - 2*(7 + 9*t2)) + 
            Power(t1,4)*(22 - 201*t2 - 457*Power(t2,2) - 
               38*Power(t2,3) + 8*Power(t2,4) - 
               Power(s,3)*(10 + 7*t2) + 
               Power(s,2)*(37 + 74*t2 + 3*Power(t2,2)) + 
               s*(-8 - 5*t2 + 139*Power(t2,2) + 20*Power(t2,3))) + 
            Power(s1,3)*(Power(t1,4)*(44 + 7*t2) - 
               Power(t1,3)*(122 + 375*t2 + 89*Power(t2,2)) + 
               Power(t1,2)*(-148 + 56*t2 + 673*Power(t2,2) + 
                  263*Power(t2,3)) + 
               t1*(-15 + 123*t2 + 95*Power(t2,2) - 312*Power(t2,3) - 
                  245*Power(t2,4)) + 
               t2*(10 - 10*t2 - 60*Power(t2,2) + 55*Power(t2,3) + 
                  56*Power(t2,4))) + 
            Power(t1,3)*(-18 - 406*t2 - 1453*Power(t2,2) + 
               1018*Power(t2,3) + 631*Power(t2,4) - 28*Power(t2,5) + 
               Power(s,3)*(2 + 12*t2 + 41*Power(t2,2)) + 
               Power(s,2)*(15 + 13*t2 - 313*Power(t2,2) - 
                  28*Power(t2,3)) - 
               2*s*(-7 + 163*t2 + 100*Power(t2,2) + 270*Power(t2,3) + 
                  81*Power(t2,4))) + 
            Power(t1,2)*(-11 + 302*t2 + 450*Power(t2,2) + 
               2465*Power(t2,3) + 2035*Power(t2,4) - 1301*Power(t2,5) - 
               100*Power(t2,6) + 
               Power(s,3)*t2*(2 - 4*t2 - 49*Power(t2,2)) + 
               Power(s,2)*t2*
                (-97 - 129*t2 + 152*Power(t2,2) + 51*Power(t2,3)) + 
               s*(-10 + 137*t2 - 1096*Power(t2,2) + 271*Power(t2,3) + 
                  757*Power(t2,4) + 345*Power(t2,5))) + 
            t1*(14 - 44*t2 + 385*Power(t2,2) - 969*Power(t2,3) - 
               272*Power(t2,4) - 2211*Power(t2,5) + 55*Power(t2,6) + 
               290*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*(-16 + 14*t2 + 3*Power(t2,2)) + 
               Power(s,2)*t2*
                (-8 - 76*t2 + 157*Power(t2,2) + 27*Power(t2,3) - 
                  22*Power(t2,4)) + 
               s*(5 + 58*t2 - 77*Power(t2,2) - 520*Power(t2,3) + 
                  2113*Power(t2,4) - 430*Power(t2,5) - 330*Power(t2,6))) \
- Power(s1,2)*(6 + Power(t1,5) - 2*t2 - (152 + 25*s)*Power(t2,2) + 
               2*(71 + 3*s)*Power(t2,3) + 2*(144 + 77*s)*Power(t2,4) + 
               (170 - 70*s)*Power(t2,5) - 175*Power(t2,6) + 
               Power(t1,4)*(-56 - 52*t2 - 3*Power(t2,2) + 
                  7*s*(14 + 3*t2)) + 
               Power(t1,3)*(-22 + 567*t2 + 268*Power(t2,2) - 
                  13*Power(t2,3) - 3*s*(-16 + 191*t2 + 73*Power(t2,2))) \
+ Power(t1,2)*(77 + 870*t2 - 562*Power(t2,2) - 800*Power(t2,3) + 
                  35*Power(t2,4) + 
                  s*(24 - 316*t2 + 401*Power(t2,2) + 485*Power(t2,3))) \
+ t1*(90 + 133*t2 - 1015*Power(t2,2) - 884*Power(t2,3) + 
                  690*Power(t2,4) + 139*Power(t2,5) + 
                  s*(2 - 57*t2 - 232*Power(t2,2) + 170*Power(t2,3) - 
                     213*Power(t2,4)))) + 
            s1*(3 - (66 + 13*s)*t2 + (104 - 35*s)*Power(t2,2) + 
               (247 + 268*s + 18*Power(s,2))*Power(t2,3) - 
               (150 + 214*s + 67*Power(s,2))*Power(t2,4) + 
               (408 + 257*s)*Power(t2,5) - (166 + 49*s)*Power(t2,6) - 
               224*Power(t2,7) + Power(t1,5)*(5 + 2*s + 4*t2) + 
               Power(t1,4)*(-43 - 238*t2 - 219*Power(t2,2) - 
                  20*Power(t2,3) + Power(s,2)*(64 + 21*t2) - 
                  2*s*(56 + 63*t2 + 3*Power(t2,2))) + 
               Power(t1,3)*(131 + 737*t2 + 1244*Power(t2,2) + 
                  1246*Power(t2,3) + 200*Power(t2,4) + 
                  Power(s,2)*(83 - 210*t2 - 171*Power(t2,2)) + 
                  s*(-180 + 514*t2 + 705*Power(t2,2) + 15*Power(t2,3))) \
+ Power(t1,2)*(64 + 143*t2 - 808*Power(t2,2) - 1542*Power(t2,3) - 
                  2043*Power(t2,4) - 570*Power(t2,5) + 
                  Power(s,2)*
                   (2 + 80*t2 - 25*Power(t2,2) + 271*Power(t2,3)) + 
                  s*(-111 + 778*t2 + 848*Power(t2,2) - 
                     1040*Power(t2,3) - 65*Power(t2,4))) + 
               t1*(48 - 404*t2 + 146*Power(t2,2) - 1118*Power(t2,3) + 
                  604*Power(t2,4) + 963*Power(t2,5) + 640*Power(t2,6) - 
                  Power(s,2)*t2*
                   (4 - 155*t2 + 120*Power(t2,2) + 41*Power(t2,3)) + 
                  s*(-14 - 137*t2 + 145*Power(t2,2) - 1081*Power(t2,3) - 
                     164*Power(t2,4) + 189*Power(t2,5))))) + 
         Power(s2,6)*(-1 + 2*(-3 + s)*Power(t1,6) + 24*t2 + 8*s*t2 - 
            60*Power(t2,2) + 50*s*Power(t2,2) + 
            13*Power(s,2)*Power(t2,2) - 5*Power(t2,3) - 
            128*s*Power(t2,3) - 6*Power(s,2)*Power(t2,3) - 
            4*Power(s,3)*Power(t2,3) - 183*Power(t2,4) - 
            86*s*Power(t2,4) - 35*Power(s,2)*Power(t2,4) + 
            4*Power(s,3)*Power(t2,4) - 356*Power(t2,5) - 
            82*s*Power(t2,5) + 52*Power(s,2)*Power(t2,5) + 
            30*Power(s,3)*Power(t2,5) + 507*Power(t2,6) + 
            168*s*Power(t2,6) + 31*Power(s,2)*Power(t2,6) + 
            34*Power(t2,7) - 553*s*Power(t2,7) + 265*Power(t2,8) + 
            147*s*Power(t2,8) - 77*Power(t2,9) + 
            Power(t1,5)*(43 + 11*Power(s,3) + 323*t2 + 66*Power(t2,2) - 
               40*Power(t2,3) - Power(s,2)*(48 + 13*t2) + 
               s*(10 - 137*t2 - 70*Power(t2,2))) + 
            Power(t1,4)*(120 + 1291*t2 - 198*Power(t2,2) - 
               1283*Power(t2,3) + 114*Power(t2,4) + 24*Power(t2,5) - 
               5*Power(s,3)*(-4 + 17*t2 + 4*Power(t2,2)) + 
               Power(s,2)*(-75 + 364*t2 + 175*Power(t2,2) - 
                  12*Power(t2,3)) + 
               s*(154 + 220*t2 + 633*Power(t2,2) + 641*Power(t2,3) + 
                  40*Power(t2,4))) - 
            Power(s1,3)*(11*Power(t1,5) - 
               Power(t1,4)*(232 + 237*t2 + 20*Power(t2,2)) + 
               Power(t1,3)*(-209 + 645*t2 + 884*Power(t2,2) + 
                  142*Power(t2,3)) + 
               Power(t1,2)*(67 + 401*t2 - 294*Power(t2,2) - 
                  966*Power(t2,3) - 280*Power(t2,4)) + 
               Power(t2,2)*(-20 + 30*t2 + 45*Power(t2,2) - 
                  64*Power(t2,3) - 28*Power(t2,4)) + 
               t1*(20 + 15*t2 - 225*Power(t2,2) + 27*Power(t2,3) + 
                  315*Power(t2,4) + 189*Power(t2,5))) + 
            Power(t1,3)*(Power(s,3)*t2*(-4 + 93*t2 + 78*Power(t2,2)) + 
               Power(s,2)*(-9 + 374*t2 + 128*Power(t2,2) - 
                  396*Power(t2,3) + 62*Power(t2,4)) + 
               s*(14 + 773*t2 + 161*Power(t2,2) - 900*Power(t2,3) - 
                  1512*Power(t2,4) - 198*Power(t2,5)) - 
               2*(58 + 325*t2 + 1350*Power(t2,2) + 3081*Power(t2,3) - 
                  768*Power(t2,4) - 291*Power(t2,5) + 36*Power(t2,6))) + 
            Power(t1,2)*(-3 - 350*t2 + 998*Power(t2,2) + 
               1455*Power(t2,3) + 4869*Power(t2,4) + 2929*Power(t2,5) - 
               1214*Power(t2,6) - 30*Power(t2,7) + 
               Power(s,3)*t2*
                (10 + 6*t2 + 11*Power(t2,2) - 60*Power(t2,3)) + 
               Power(s,2)*(2 + 84*t2 - 424*Power(t2,2) - 
                  461*Power(t2,3) + 79*Power(t2,4) - 75*Power(t2,5)) + 
               s*(15 - 249*t2 + 722*Power(t2,2) - 4877*Power(t2,3) - 
                  916*Power(t2,4) + 1428*Power(t2,5) + 345*Power(t2,6))) \
+ t1*(-13 + 158*t2 - 20*Power(t2,2) + 1426*Power(t2,3) - 
               1785*Power(t2,4) - 597*Power(t2,5) - 2200*Power(t2,6) + 
               76*Power(t2,7) + 165*Power(t2,8) - 
               2*Power(s,3)*Power(t2,2)*
                (1 + 43*t2 - 4*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s,2)*t2*
                (13 - 23*t2 - 328*Power(t2,2) + 121*Power(t2,3) + 
                  96*Power(t2,4) + 15*Power(t2,5)) + 
               s*(1 + 133*t2 + 306*Power(t2,2) - 201*Power(t2,3) - 
                  852*Power(t2,4) + 3645*Power(t2,5) - 
                  360*Power(t2,6) - 321*Power(t2,7))) + 
            Power(s1,2)*(t2*(-30 + 55*t2 + (189 + 50*s)*Power(t2,2) - 
                  (182 + 5*s)*Power(t2,3) - 
                  (223 + 140*s)*Power(t2,4) + 
                  4*(-59 + 14*s)*Power(t2,5) + 147*Power(t2,6)) + 
               Power(t1,5)*(33*s - 13*(3 + t2)) + 
               Power(t1,4)*(243 + 579*t2 + 40*Power(t2,2) - 
                  12*Power(t2,3) - s*(270 + 559*t2 + 60*Power(t2,2))) + 
               Power(t1,3)*(487 + 115*t2 - 1818*Power(t2,2) - 
                  215*Power(t2,3) + 119*Power(t2,4) + 
                  s*(-326 - 13*t2 + 1479*Power(t2,2) + 362*Power(t2,3))\
) - Power(t1,2)*(-371 + 1126*t2 + 3010*Power(t2,2) - 699*Power(t2,3) - 
                  1027*Power(t2,4) + 155*Power(t2,5) + 
                  s*(122 + 274*t2 - 470*Power(t2,2) + 
                     635*Power(t2,3) + 535*Power(t2,4))) + 
               t1*(40 - 452*t2 - 20*Power(t2,2) + 1543*Power(t2,3) + 
                  1611*Power(t2,4) - 720*Power(t2,5) - 
                  106*Power(t2,6) + 
                  s*(-6 - 43*t2 + 50*Power(t2,2) + 456*Power(t2,3) - 
                     192*Power(t2,4) + 158*Power(t2,5)))) + 
            s1*(6 - 2*Power(t1,6) + (-7 + 6*s)*t2 - 
               2*(83 + 41*s)*Power(t2,2) + 
               (186 - 45*s + Power(s,2))*Power(t2,3) + 
               (311 + 316*s + 60*Power(s,2))*Power(t2,4) - 
               (90 + 303*s + 136*Power(s,2))*Power(t2,5) + 
               (360 + 317*s)*Power(t2,6) + (19 - 21*s)*Power(t2,7) - 
               210*Power(t2,8) + 
               Power(t1,5)*(88 - 33*Power(s,2) + 243*t2 + 
                  70*Power(t2,2) + s*(87 + 26*t2)) + 
               Power(t1,4)*(-331 - 1180*t2 - 2099*Power(t2,2) - 
                  859*Power(t2,3) - 40*Power(t2,4) + 
                  Power(s,2)*(18 + 407*t2 + 60*Power(t2,2)) + 
                  s*(-89 - 1078*t2 - 215*Power(t2,2) + 24*Power(t2,3))) \
+ Power(t1,3)*(-77 + 733*t2 + 2190*Power(t2,2) + 4246*Power(t2,3) + 
                  2870*Power(t2,4) + 250*Power(t2,5) - 
                  Power(s,2)*
                   (77 - 283*t2 + 688*Power(t2,2) + 298*Power(t2,3)) + 
                  s*(-255 - 2201*t2 + 1368*Power(t2,2) + 
                     813*Power(t2,3) - 181*Power(t2,4))) + 
               Power(t1,2)*(160 + 479*t2 + 1782*Power(t2,2) + 
                  280*Power(t2,3) - 1616*Power(t2,4) - 
                  3436*Power(t2,5) - 605*Power(t2,6) + 
                  Power(s,2)*
                   (-7 - 213*t2 + 258*Power(t2,2) + 3*Power(t2,3) + 
                     315*Power(t2,4)) + 
                  s*(54 - 478*t2 + 2986*Power(t2,2) + 
                     2702*Power(t2,3) - 1023*Power(t2,4) + 
                     195*Power(t2,5))) - 
               t1*(-96 + 16*t2 + 1256*Power(t2,2) - 469*Power(t2,3) + 
                  2114*Power(t2,4) + 307*Power(t2,5) - 
                  1083*Power(t2,6) - 634*Power(t2,7) + 
                  Power(s,2)*t2*
                   (13 + 49*t2 - 523*Power(t2,2) + 131*Power(t2,3) + 
                     15*Power(t2,4)) + 
                  s*(-7 - 30*t2 + 483*Power(t2,2) - 925*Power(t2,3) + 
                     1850*Power(t2,4) + 495*Power(t2,5) - 63*Power(t2,6))\
))) + Power(s2,5)*(-2 + (4 - 6*s + 30*s1)*t2 + 
            (59 - 6*Power(s,2) - 80*s1 - 60*Power(s1,2) + 
               s*(61 + 30*s1))*Power(t2,2) + 
            (-22 + 2*Power(s,2) + 2*Power(s,3) + s*(22 - 129*s1) - 
               141*s1 + 110*Power(s1,2) + 20*Power(s1,3))*Power(t2,3) + 
            (24 - 23*Power(s,3) + 32*s1 + 133*Power(s1,2) - 
               35*Power(s1,3) + Power(s,2)*(32 + 5*s1) + 
               2*s*(66 - 34*s1 + 25*Power(s1,2)))*Power(t2,4) + 
            (21*Power(s,3) + 20*Power(s,2)*(2 + 5*s1) - 
               5*s*(41 - 33*s1 + Power(s1,2)) - 
               3*(101 - 105*s1 + 35*Power(s1,2) + 6*Power(s1,3)))*
             Power(t2,5) + (-242 + 40*Power(s,3) - 74*s1 - 
               120*Power(s1,2) + 41*Power(s1,3) - 
               5*Power(s,2)*(13 + 33*s1) - 
               s*(135 + 181*s1 + 70*Power(s1,2)))*Power(t2,6) + 
            (335 + 81*Power(s,2) + 230*s1 - 170*Power(s1,2) + 
               8*Power(s1,3) + s*(37 + 201*s1 + 28*Power(s1,2)))*
             Power(t2,7) + (34 + 7*s*(-49 + s1) + 86*s1 + 
               77*Power(s1,2))*Power(t2,8) + 
            (85 + 77*s - 118*s1)*Power(t2,9) - 23*Power(t2,10) + 
            Power(t1,6)*(-85 + 10*Power(s,2) + 10*Power(s1,2) - 50*t2 + 
               72*Power(t2,2) - s1*(89 + 80*t2) + s*(45 - 20*s1 + 80*t2)\
) + Power(t1,5)*(-397 - 450*t2 + 1253*Power(t2,2) - 160*Power(t2,3) - 
               120*Power(t2,4) + Power(s,3)*(31 + 55*t2) - 
               Power(s1,3)*(162 + 55*t2) + 
               Power(s1,2)*(-328 - 203*t2 + 26*Power(t2,2)) + 
               2*s1*(215 + 807*t2 + 711*Power(t2,2) + 81*Power(t2,3)) - 
               Power(s,2)*(117 + 311*t2 - 26*Power(t2,2) + 
                  s1*(224 + 165*t2)) + 
               s*(5*Power(s1,2)*(71 + 33*t2) + 
                  s1*(491 + 514*t2 - 52*Power(t2,2)) - 
                  2*(54 + 140*t2 + 463*Power(t2,2) + 81*Power(t2,3)))) + 
            Power(t1,4)*(283 + 1636*t2 + 7679*Power(t2,2) + 
               959*Power(t2,3) - 1280*Power(t2,4) + 336*Power(t2,5) + 
               24*Power(t2,6) - 
               Power(s,3)*(18 - 14*t2 + 229*Power(t2,2) + 
                  26*Power(t2,3)) + 
               Power(s1,3)*(30 + 981*t2 + 481*Power(t2,2) + 
                  26*Power(t2,3)) + 
               Power(s1,2)*(-329 + 1708*t2 + 1099*Power(t2,2) - 
                  319*Power(t2,3) - 42*Power(t2,4)) - 
               s1*(308 + 1901*t2 + 4581*Power(t2,2) + 
                  5852*Power(t2,3) + 1224*Power(t2,4) + 20*Power(t2,5)) \
+ Power(s,2)*(-148 - 747*t2 + 847*Power(t2,2) - 69*Power(t2,3) - 
                  42*Power(t2,4) + 
                  s1*(-274 + 363*t2 + 939*Power(t2,2) + 78*Power(t2,3))\
) + s*(-234 - 167*t2 + 108*Power(t2,2) + 2487*Power(t2,3) + 
                  910*Power(t2,4) + 20*Power(t2,5) - 
                  Power(s1,2)*
                   (-482 + 1358*t2 + 1191*Power(t2,2) + 78*Power(t2,3)) \
+ s1*(1225 - 27*t2 - 2233*Power(t2,2) + 388*Power(t2,3) + 
                     84*Power(t2,4)))) + 
            Power(t1,3)*(119 - 439*t2 - 2270*Power(t2,2) - 
               5579*Power(t2,3) - 9293*Power(t2,4) + 1504*Power(t2,5) + 
               180*Power(t2,6) - 58*Power(t2,7) + 
               Power(s1,3)*(360 + 295*t2 - 1307*Power(t2,2) - 
                  1066*Power(t2,3) - 118*Power(t2,4)) + 
               Power(s,3)*(-5 - 49*t2 - 79*Power(t2,2) + 
                  149*Power(t2,3) + 67*Power(t2,4)) + 
               Power(s1,2)*(-72 + 3673*t2 + 1209*Power(t2,2) - 
                  2514*Power(t2,3) + 293*Power(t2,4) + 191*Power(t2,5)) \
+ s1*(-403 - 1599*t2 - 1821*Power(t2,2) + 122*Power(t2,3) + 
                  6802*Power(t2,4) + 3214*Power(t2,5) + 125*Power(t2,6)\
) + Power(s,2)*(-3 + 167*t2 + 1256*Power(t2,2) + 320*Power(t2,3) + 
                  35*Power(t2,4) + 148*Power(t2,5) + 
                  s1*(10 - 269*t2 + 450*Power(t2,2) - 
                     963*Power(t2,3) - 252*Power(t2,4))) + 
               s*(145 + 157*t2 + 5326*Power(t2,2) + 4745*Power(t2,3) - 
                  1330*Power(t2,4) - 1658*Power(t2,5) - 
                  77*Power(t2,6) + 
                  Power(s1,2)*
                   (299 - 733*t2 + 267*Power(t2,2) + 
                     1880*Power(t2,3) + 303*Power(t2,4)) - 
                  s1*(-583 + 2822*t2 + 7600*Power(t2,2) - 
                     1342*Power(t2,3) + 190*Power(t2,4) + 
                     339*Power(t2,5)))) + 
            Power(t1,2)*(-76 - 158*t2 - 2448*Power(t2,2) + 
               1703*Power(t2,3) + 2506*Power(t2,4) + 4578*Power(t2,5) + 
               1869*Power(t2,6) - 651*Power(t2,7) + 6*Power(t2,8) + 
               Power(s,3)*t2*
                (2 + 157*t2 + 41*Power(t2,2) + 91*Power(t2,3) - 
                  20*Power(t2,4)) + 
               Power(s1,3)*(80 - 235*t2 - 309*Power(t2,2) + 
                  485*Power(t2,3) + 798*Power(t2,4) + 171*Power(t2,5)) \
+ Power(s1,2)*(267 + 928*t2 - 2620*Power(t2,2) - 5165*Power(t2,3) + 
                  432*Power(t2,4) + 723*Power(t2,5) - 187*Power(t2,6)) \
+ s1*(-283 + 1799*t2 + 155*Power(t2,2) + 4190*Power(t2,3) + 
                  3589*Power(t2,4) - 506*Power(t2,5) - 
                  3247*Power(t2,6) - 348*Power(t2,7)) + 
               Power(s,2)*(-2 + 179*t2 + 810*Power(t2,2) - 
                  442*Power(t2,3) - 492*Power(t2,4) - 56*Power(t2,5) - 
                  132*Power(t2,6) + 
                  s1*(4 + 43*t2 - 1157*Power(t2,2) + 281*Power(t2,3) - 
                     114*Power(t2,4) + 163*Power(t2,5))) + 
               s*(-51 - 287*t2 - 105*Power(t2,2) + 1906*Power(t2,3) - 
                  9487*Power(t2,4) - 2297*Power(t2,5) + 
                  1329*Power(t2,6) + 155*Power(t2,7) - 
                  Power(s1,2)*
                   (-24 + 218*t2 + 653*Power(t2,2) - 427*Power(t2,3) + 
                     490*Power(t2,4) + 314*Power(t2,5)) + 
                  s1*(143 + 123*t2 - 2444*Power(t2,2) + 
                     4780*Power(t2,3) + 3997*Power(t2,4) - 
                     339*Power(t2,5) + 312*Power(t2,6)))) - 
            t1*(34 - 65*t2 - 239*Power(t2,2) - 463*Power(t2,3) - 
               2229*Power(t2,4) + 2097*Power(t2,5) + 669*Power(t2,6) + 
               1149*Power(t2,7) - 87*Power(t2,8) - 52*Power(t2,9) + 
               Power(s,3)*Power(t2,2)*
                (-25 - t2 + 192*Power(t2,2) + 30*Power(t2,3) + 
                  25*Power(t2,4)) + 
               Power(s1,3)*t2*
                (60 - 40*t2 - 204*Power(t2,2) + 148*Power(t2,3) + 
                  178*Power(t2,4) + 91*Power(t2,5)) + 
               Power(s1,2)*(-60 + 30*t2 + 792*Power(t2,2) - 
                  258*Power(t2,3) - 1142*Power(t2,4) - 
                  1694*Power(t2,5) + 516*Power(t2,6) + 41*Power(t2,7)) + 
               s1*(35 - 529*t2 + 235*Power(t2,2) + 1807*Power(t2,3) - 
                  776*Power(t2,4) + 2060*Power(t2,5) + 
                  1129*Power(t2,6) - 791*Power(t2,7) - 374*Power(t2,8)) \
+ Power(s,2)*t2*(4 - 28*t2 - 57*Power(t2,2) + 508*Power(t2,3) + 
                  137*Power(t2,4) - 47*Power(t2,5) - 20*Power(t2,6) + 
                  s1*(-6 + 85*t2 + 301*Power(t2,2) - 936*Power(t2,3) + 
                     10*Power(t2,4) - 29*Power(t2,5))) + 
               s*(5 + 109*t2 - 208*Power(t2,2) - 531*Power(t2,3) + 
                  67*Power(t2,4) + 692*Power(t2,5) - 3582*Power(t2,6) + 
                  194*Power(t2,7) + 176*Power(t2,8) + 
                  Power(s1,2)*t2*
                   (30 + 85*t2 + 8*Power(t2,2) - 456*Power(t2,3) + 
                     202*Power(t2,4) - 59*Power(t2,5)) + 
                  s1*(-6 - 140*t2 - 152*Power(t2,2) + 889*Power(t2,3) - 
                     1924*Power(t2,4) + 1793*Power(t2,5) + 
                     540*Power(t2,6) + 35*Power(t2,7))))) + 
         Power(s2,4)*(Power(t1,7)*(14 - 30*s + 30*s1 - 56*t2) + 
            t2*(-10 + (35 - 30*s + 60*s1)*t2 + 
               (6*Power(s,2) + s*(35 + 24*s1) - 
                  4*(5 + 22*s1 + 12*Power(s1,2)))*Power(t2,2) + 
               (163 + 23*s - 119*Power(s,2) + 10*Power(s,3) - 69*s1 + 
                  61*Power(s1,2) + 10*Power(s1,3))*Power(t2,3) + 
               (-96 - 55*Power(s,3) - 110*s1 + 74*Power(s1,2) - 
                  19*Power(s1,3) + 2*Power(s,2)*(74 + 5*s1) + 
                  s*(366 - 148*s1 + 25*Power(s1,2)))*Power(t2,4) + 
               (-142 + 45*Power(s,3) + 225*s1 - 19*Power(s1,2) - 
                  3*Power(s1,3) + Power(s,2)*(87 + 90*s1) + 
                  s*(-288 + 11*s1 - 6*Power(s1,2)))*Power(t2,5) + 
               (-124 + 30*Power(s,3) - 86*s1 - 51*Power(s1,2) + 
                  14*Power(s1,3) - Power(s,2)*(139 + 122*s1) - 
                  2*s*(24 + 7*Power(s1,2)))*Power(t2,6) + 
               (167 + 87*Power(s,2) + 118*s1 - 64*Power(s1,2) + 
                  Power(s1,3) + s*(-74 + 55*s1 + 8*Power(s1,2)))*
                Power(t2,7) + 
               (2 + 43*s1 + 23*Power(s1,2) + s*(-115 + 13*s1))*
                Power(t2,8) + (9 + 23*s - 37*s1)*Power(t2,9) - 
               3*Power(t2,10)) - 
            t1*(-10 + 5*(40 + 21*s)*t2 + 
               2*(-14 + 91*s - 42*Power(s,2) + 6*Power(s,3))*
                Power(t2,2) + 
               (71 + 430*s + 135*Power(s,2) - 87*Power(s,3))*
                Power(t2,3) + 
               (-1051 - 475*s - 80*Power(s,2) + 17*Power(s,3))*
                Power(t2,4) + 
               (-1678 - 411*s + 194*Power(s,2) + 230*Power(s,3))*
                Power(t2,5) + 
               (1625 + 80*s + 341*Power(s,2) + 60*Power(s,3))*
                Power(t2,6) + 
               2*(180 - 997*s + 32*Power(s,2) + 12*Power(s,3))*
                Power(t2,7) + (244 + 92*s - Power(s,2))*Power(t2,8) + 
               (-50 + 51*s)*Power(t2,9) - 7*Power(t2,10) + 
               Power(s1,3)*Power(t2,2)*
                (60 - 94*t2 - 72*Power(t2,2) + 130*Power(t2,3) + 
                  43*Power(t2,4) + 25*Power(t2,5)) + 
               Power(s1,2)*t2*
                (-180 + 10*(23 + 6*s)*t2 + 
                  2*(323 + 64*s)*Power(t2,2) - 
                  (293 + 67*s)*Power(t2,3) - 
                  2*(198 + 119*s)*Power(t2,4) + 
                  (-1031 + 174*s)*Power(t2,5) + 264*Power(t2,6) + 
                  2*Power(t2,7)) + 
               s1*(60 + 15*(-7 + 2*s)*t2 - 
                  (723 + 247*s + 30*Power(s,2))*Power(t2,2) + 
                  3*(43 - 112*s + 20*Power(s,2))*Power(t2,3) + 
                  (1297 + 825*s + 712*Power(s,2))*Power(t2,4) - 
                  (639 + 1656*s + 988*Power(s,2))*Power(t2,5) + 
                  (1152 + 996*s - 109*Power(s,2))*Power(t2,6) + 
                  (944 + 273*s - 41*Power(s,2))*Power(t2,7) + 
                  (-385 + 39*s)*Power(t2,8) - 122*Power(t2,9))) + 
            Power(t1,6)*(230 - 40*Power(s,3) + 40*Power(s1,3) - 
               581*t2 + 76*Power(t2,2) + 256*Power(t2,3) + 
               4*Power(s,2)*(41 + 30*s1 + t2) + 
               2*Power(s1,2)*(75 + 2*t2) - 
               s1*(476 + 1067*t2 + 266*Power(t2,2)) + 
               s*(32 - 120*Power(s1,2) + 577*t2 + 266*Power(t2,2) - 
                  2*s1*(157 + 4*t2))) + 
            Power(t1,5)*(-440 - 4551*t2 - 3703*Power(t2,2) + 
               1220*Power(t2,3) - 660*Power(t2,4) - 112*Power(t2,5) - 
               2*Power(s1,3)*(143 + 283*t2 + 44*Power(t2,2)) + 
               Power(s,3)*(-72 + 199*t2 + 88*Power(t2,2)) + 
               2*Power(s1,2)*
                (-271 - 813*t2 + 69*Power(t2,2) + 66*Power(t2,3)) + 
               s1*(764 + 2854*t2 + 6156*Power(t2,2) + 
                  2508*Power(t2,3) + 88*Power(t2,4)) + 
               Power(s,2)*(496 - 587*t2 - 275*Power(t2,2) + 
                  132*Power(t2,3) - 
                  2*s1*(-99 + 482*t2 + 132*Power(t2,2))) + 
               s*(Power(s1,2)*(160 + 1331*t2 + 264*Power(t2,2)) + 
                  s1*(-534 + 2315*t2 + 137*Power(t2,2) - 
                     264*Power(t2,3)) - 
                  2*(12 - 116*t2 + 902*Power(t2,2) + 865*Power(t2,3) + 
                     44*Power(t2,4)))) + 
            Power(t1,4)*(86 + 1677*t2 + 3550*Power(t2,2) + 
               12658*Power(t2,3) + 496*Power(t2,4) - 420*Power(t2,5) + 
               268*Power(t2,6) + 8*Power(t2,7) + 
               Power(s,3)*(12 + 82*t2 - 52*Power(t2,2) - 
                  234*Power(t2,3) - 13*Power(t2,4)) + 
               Power(s1,3)*(-556 + 550*t2 + 1523*Power(t2,2) + 
                  440*Power(t2,3) + 13*Power(t2,4)) - 
               Power(s1,2)*(1072 + 2683*t2 - 2611*Power(t2,2) - 
                  268*Power(t2,3) + 647*Power(t2,4) + 39*Power(t2,5)) + 
               s1*(650 + 1488*t2 + 1760*Power(t2,2) - 
                  5862*Power(t2,3) - 6789*Power(t2,4) - 
                  668*Power(t2,5) + 10*Power(t2,6)) + 
               Power(s,2)*(148 - 1092*t2 - 1574*Power(t2,2) + 
                  404*Power(t2,3) - 428*Power(t2,4) - 39*Power(t2,5) + 
                  s1*(320 - 790*t2 + 879*Power(t2,2) + 
                     908*Power(t2,3) + 39*Power(t2,4))) + 
               s*(-394 - 3108*t2 - 6377*Power(t2,2) - 
                  2236*Power(t2,3) + 2937*Power(t2,4) + 
                  402*Power(t2,5) - 10*Power(t2,6) + 
                  Power(s1,2)*
                   (344 + 778*t2 - 2350*Power(t2,2) - 
                     1114*Power(t2,3) - 39*Power(t2,4)) + 
                  s1*(344 + 8435*t2 + 1016*Power(t2,2) - 
                     851*Power(t2,3) + 1075*Power(t2,4) + 78*Power(t2,5)\
))) + Power(t1,3)*(136 + 1915*t2 + 721*Power(t2,2) - 3722*Power(t2,3) - 
               4623*Power(t2,4) - 5460*Power(t2,5) + 980*Power(t2,6) - 
               35*Power(t2,7) - 16*Power(t2,8) + 
               Power(s,3)*t2*
                (-69 - 127*t2 - 279*Power(t2,2) + 30*Power(t2,3) + 
                  14*Power(t2,4)) - 
               Power(s1,3)*(30 - 484*t2 + 90*Power(t2,2) + 
                  1272*Power(t2,3) + 663*Power(t2,4) + 41*Power(t2,5)) \
+ Power(s1,2)*(-994 + 1390*t2 + 7692*Power(t2,2) + 2456*Power(t2,3) - 
                  1815*Power(t2,4) + 594*Power(t2,5) + 121*Power(t2,6)) \
+ s1*(-370 - 1683*t2 - 4316*Power(t2,2) - 8145*Power(t2,3) - 
                  4665*Power(t2,4) + 6028*Power(t2,5) + 
                  1798*Power(t2,6) + 5*Power(t2,7)) + 
               Power(s,2)*(-48 - 969*t2 + 378*Power(t2,2) + 
                  1245*Power(t2,3) + 79*Power(t2,4) + 
                  328*Power(t2,5) + 98*Power(t2,6) + 
                  s1*(54 + 942*t2 - 260*Power(t2,2) + 
                     664*Power(t2,3) - 489*Power(t2,4) - 69*Power(t2,5)\
)) + s*(-70 + 613*t2 - 1825*Power(t2,2) + 12227*Power(t2,3) + 
                  9278*Power(t2,4) - 1108*Power(t2,5) - 
                  762*Power(t2,6) + 21*Power(t2,7) + 
                  Power(s1,2)*
                   (312 + 875*t2 - 657*Power(t2,2) + 300*Power(t2,3) + 
                     1122*Power(t2,4) + 96*Power(t2,5)) - 
                  s1*(226 - 3547*t2 + 5249*Power(t2,2) + 
                     10957*Power(t2,3) - 528*Power(t2,4) + 
                     918*Power(t2,5) + 219*Power(t2,6)))) + 
            Power(t1,2)*(60 - 546*t2 - 419*Power(t2,2) - 
               4921*Power(t2,3) + 2839*Power(t2,4) + 2705*Power(t2,5) + 
               1806*Power(t2,6) + 385*Power(t2,7) - 188*Power(t2,8) + 
               4*Power(t2,9) + 
               Power(s,3)*t2*
                (-24 - 60*t2 + 445*Power(t2,2) + 129*Power(t2,3) + 
                  174*Power(t2,4) + 23*Power(t2,5)) + 
               Power(s1,3)*(60 + 60*t2 - 312*Power(t2,2) + 
                  52*Power(t2,3) + 349*Power(t2,4) + 363*Power(t2,5) + 
                  50*Power(t2,6)) + 
               Power(s1,2)*(-190 + 1067*t2 + 643*Power(t2,2) - 
                  2043*Power(t2,3) - 4711*Power(t2,4) + 
                  283*Power(t2,5) + 264*Power(t2,6) - 101*Power(t2,7)) + 
               s1*(-330 - 577*t2 + 3627*Power(t2,2) - 
                  1648*Power(t2,3) + 3701*Power(t2,4) + 
                  5177*Power(t2,5) + 60*Power(t2,6) - 
                  1736*Power(t2,7) - 99*Power(t2,8)) + 
               Power(s,2)*t2*
                (-92 + 25*t2 + 1712*Power(t2,2) + 181*Power(t2,3) + 
                  96*Power(t2,4) - 11*Power(t2,5) - 60*Power(t2,6) - 
                  s1*(-156 - 654*t2 + 2298*Power(t2,2) + 
                     57*Power(t2,3) + 314*Power(t2,4) + 11*Power(t2,5))) \
+ s*(-10 + 111*t2 - 796*Power(t2,2) + 654*Power(t2,3) + 
                  2349*Power(t2,4) - 9338*Power(t2,5) - 
                  1856*Power(t2,6) + 660*Power(t2,7) + 17*Power(t2,8) + 
                  Power(s1,2)*
                   (60 + 60*t2 - 6*Power(t2,2) - 693*Power(t2,3) + 
                     409*Power(t2,4) - 88*Power(t2,5) - 62*Power(t2,6)) \
+ s1*(-70 + 33*t2 + 693*Power(t2,2) - 5339*Power(t2,3) + 
                     3664*Power(t2,4) + 2947*Power(t2,5) + 
                     114*Power(t2,6) + 168*Power(t2,7))))) + 
         Power(s2,2)*(2*Power(t1,8)*(-7 + 31*s - 31*s1 + 84*t2) + 
            Power(t2,3)*(20 + 5*(-17 + 12*s - 6*s1)*t2 + 
               (112 + 60*Power(s,2) + 107*s1 + 6*Power(s1,2) - 
                  10*s*(25 + 3*s1))*Power(t2,2) + 
               (-30 - 215*Power(s,2) + 20*Power(s,3) - 124*s1 - 
                  17*Power(s1,2) + s*(284 + 98*s1))*Power(t2,3) + 
               (-35 - 50*Power(s,3) + 46*s1 + 13*Power(s1,2) + 
                  Power(s,2)*(182 + 5*s1) - s*(36 + 85*s1))*Power(t2,4) \
+ (25 + 30*Power(s,3) + 8*s1 + Power(s1,2) + Power(s,2)*(-5 + 8*s1) - 
                  s*(60 + 2*s1 + Power(s1,2)))*Power(t2,5) + 
               (-18 + 2*Power(s,3) - 14*s1 - 3*Power(s1,2) - 
                  6*Power(s,2)*(5 + 2*s1) + 
                  s*(23 + 21*s1 + Power(s1,2)))*Power(t2,6) + 
               (14 + 12*Power(s,2) + 8*s1 - s*(22 + 3*s1))*
                Power(t2,7) + (-3 + s - s1 + s*s1)*Power(t2,8)) - 
            t1*t2*(-60 + 12*(1 + 3*s + 3*s1)*t2 + 
               2*(-14 + 72*Power(s,2) + 83*s1 - 24*Power(s1,2) - 
                  s*(289 + 6*s1))*Power(t2,2) + 
               (449 + 60*Power(s,3) - 308*s1 + 78*Power(s1,2) + 
                  12*Power(s1,3) + 10*Power(s,2)*(-79 + 6*s1) + 
                  s*(653 + 143*s1 - 30*Power(s1,2)))*Power(t2,3) - 
               (358 + 200*Power(s,3) + 34*s1 + 54*Power(s1,2) + 
                  29*Power(s1,3) + Power(s,2)*(-667 + 175*s1) + 
                  s*(-601 + 82*s1 - 113*Power(s1,2)))*Power(t2,4) + 
               (-48 + 110*Power(s,3) + 94*s1 + 100*Power(s1,2) + 
                  13*Power(s1,3) + Power(s,2)*(129 + 403*s1) - 
                  s*(340 + 235*s1 + 102*Power(s1,2)))*Power(t2,5) + 
               (-136 + 66*Power(s,3) - 85*s1 - 37*Power(s1,2) + 
                  7*Power(s1,3) - Power(s,2)*(194 + 243*s1) + 
                  s*(-153 + 87*s1 + 4*Power(s1,2)))*Power(t2,6) + 
               (203 + 16*Power(s,3) + Power(s,2)*(97 - 35*s1) + 
                  106*s1 - 53*Power(s1,2) - 3*Power(s1,3) + 
                  s*(-188 + 51*s1 + 25*Power(s1,2)))*Power(t2,7) + 
               (-25 + 2*Power(s,3) + Power(s,2)*(33 - 4*s1) + 43*s1 + 
                  15*Power(s1,2) + s*(-74 - s1 + 2*Power(s1,2)))*
                Power(t2,8) + 
               (-9 + 2*Power(s,2) - 18*s1 - Power(s1,2) + s*(10 + s1))*
                Power(t2,9)) + 
            2*Power(t1,7)*(-428 + 23*Power(s,3) - 23*Power(s1,3) - 
               145*t2 - 186*Power(t2,2) - 84*Power(t2,3) + 
               Power(s1,2)*(-139 + 15*t2) + 
               Power(s,2)*(-131 - 69*s1 + 15*t2) + 
               s*(-13 + 69*Power(s1,2) - 30*s1*(-9 + t2) - 380*t2 - 
                  56*Power(t2,2)) + s1*(411 + 781*t2 + 56*Power(t2,2))) \
+ Power(t1,2)*(-30 + 12*(25 - 5*s + 9*Power(s,2))*t2 + 
               6*(56 + 68*s - 101*Power(s,2) + 6*Power(s,3))*
                Power(t2,2) + 
               (346 + 1242*s + 572*Power(s,2) - 168*Power(s,3))*
                Power(t2,3) - 
               (1339 - 671*s + 248*Power(s,2) + 38*Power(s,3))*
                Power(t2,4) + 
               (-1026 - 1527*s + 48*Power(s,2) + 380*Power(s,3))*
                Power(t2,5) + 
               2*(611 - 130*s + 127*Power(s,2) + 52*Power(s,3))*
                Power(t2,6) + 
               (247 - 1183*s + 347*Power(s,2) + 55*Power(s,3))*
                Power(t2,7) + 
               (-153 + 14*s + 53*Power(s,2) + 6*Power(s,3))*
                Power(t2,8) + (-17 + 20*s + 6*Power(s,2))*Power(t2,9) + 
               Power(s1,3)*Power(t2,2)*
                (36 - 116*t2 + 7*Power(t2,2) + 105*Power(t2,3) - 
                  12*Power(t2,4) - 2*Power(t2,6)) + 
               Power(s1,2)*t2*
                (-144 + 6*(49 + 6*s)*t2 + 6*(19 + 60*s)*Power(t2,2) + 
                  (173 - 328*s)*Power(t2,3) - 
                  8*(16 + 29*s)*Power(t2,4) + 
                  8*(-71 + 29*s)*Power(t2,5) + 
                  3*(39 + 23*s)*Power(t2,6) + 2*(-1 + 5*s)*Power(t2,7)) \
- s1*(-180 + 6*(19 + 18*s)*t2 + 
                  (852 + 96*s - 36*Power(s,2))*Power(t2,2) + 
                  (-370 + 62*s + 484*Power(s,2))*Power(t2,3) - 
                  (22 + 216*s + 1931*Power(s,2))*Power(t2,4) + 
                  (479 + 940*s + 1207*Power(s,2))*Power(t2,5) + 
                  (-584 - 294*s + 280*Power(s,2))*Power(t2,6) + 
                  (-806 - 108*s + 121*Power(s,2))*Power(t2,7) + 
                  (127 + 4*s + 14*Power(s,2))*Power(t2,8) + 
                  (54 + 5*s)*Power(t2,9))) - 
            2*Power(t1,6)*(-167 - 1500*t2 - 1359*Power(t2,2) + 
               277*Power(t2,3) - 197*Power(t2,4) - 24*Power(t2,5) + 
               Power(s,3)*(-43 + 88*t2 + 36*Power(t2,2)) - 
               Power(s1,3)*(43 + 223*t2 + 36*Power(t2,2)) + 
               Power(s1,2)*(-82 - 514*t2 + 311*Power(t2,2) + 
                  62*Power(t2,3)) + 
               s1*(74 - 20*t2 + 1774*Power(t2,2) + 637*Power(t2,3) - 
                  10*Power(t2,4)) + 
               Power(s,2)*(458 - 16*t2 + 19*Power(t2,2) + 
                  62*Power(t2,3) - 3*s1*(-57 + 133*t2 + 36*Power(t2,2))\
) + s*(316 + 1779*t2 + 106*Power(t2,2) - 362*Power(t2,3) + 
                  10*Power(t2,4) + 
                  Power(s1,2)*(-85 + 534*t2 + 108*Power(t2,2)) - 
                  2*s1*(464 - 217*t2 + 165*Power(t2,2) + 62*Power(t2,3))\
)) + Power(t1,5)*(672 - 300*t2 + 922*Power(t2,2) - 4328*Power(t2,3) + 
               597*Power(t2,4) + 73*Power(t2,5) - 102*Power(t2,6) + 
               Power(s,3)*(-10 - 194*t2 - 218*Power(t2,2) + 
                  98*Power(t2,3) + 5*Power(t2,4)) - 
               Power(s1,3)*(-278 + 312*t2 + 690*Power(t2,2) + 
                  204*Power(t2,3) + 5*Power(t2,4)) + 
               Power(s1,2)*(1056 + 5184*t2 - 486*Power(t2,2) - 
                  18*Power(t2,3) + 515*Power(t2,4) + 25*Power(t2,5)) + 
               s1*(-944 - 5138*t2 - 9164*Power(t2,2) + 
                  226*Power(t2,3) + 3378*Power(t2,4) + 
                  165*Power(t2,5) - 22*Power(t2,6)) + 
               Power(s,2)*(-424 + 576*t2 + 562*Power(t2,2) - 
                  564*Power(t2,3) + 248*Power(t2,4) + 25*Power(t2,5) + 
                  s1*(-598 + 780*t2 + 202*Power(t2,2) - 
                     400*Power(t2,3) - 15*Power(t2,4))) + 
               s*(504 + 3118*t2 + 7840*Power(t2,2) + 4956*Power(t2,3) - 
                  1044*Power(t2,4) - 25*Power(t2,5) + 22*Power(t2,6) + 
                  Power(s1,2)*
                   (490 - 610*t2 + 706*Power(t2,2) + 506*Power(t2,3) + 
                     15*Power(t2,4)) - 
                  s1*(-728 + 10400*t2 + 1956*Power(t2,2) - 
                     466*Power(t2,3) + 763*Power(t2,4) + 50*Power(t2,5))\
)) + Power(t1,4)*(-226 - 2428*t2 - 1910*Power(t2,2) + 
               5708*Power(t2,3) - 316*Power(t2,4) + 253*Power(t2,5) - 
               388*Power(t2,6) + 37*Power(t2,7) - 
               Power(s1,3)*(-186 + 58*t2 + 88*Power(t2,2) - 
                  496*Power(t2,3) - 204*Power(t2,4) + Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s,3)*(-38 + 228*t2 + 216*Power(t2,2) + 
                  452*Power(t2,3) + 178*Power(t2,4) + 19*Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s1,2)*(748 - 148*t2 - 3724*Power(t2,2) - 
                  3154*Power(t2,3) + 845*Power(t2,4) - 
                  325*Power(t2,5) - 72*Power(t2,6) + 2*Power(t2,7)) + 
               s1*(254 + 1186*t2 - 1568*Power(t2,2) + 
                  6820*Power(t2,3) + 8123*Power(t2,4) - 
                  2540*Power(t2,5) - 769*Power(t2,6) + 29*Power(t2,7)) \
- Power(s,2)*(108 - 2180*t2 - 544*Power(t2,2) + 252*Power(t2,3) - 
                  1277*Power(t2,4) - 104*Power(t2,5) + 
                  27*Power(t2,6) - 2*Power(t2,7) + 
                  s1*(-38 + 1794*t2 + 544*Power(t2,2) + 
                     936*Power(t2,3) + 318*Power(t2,4) + 
                     39*Power(t2,5) + 6*Power(t2,6))) + 
               s*(474 + 346*t2 + 6556*Power(t2,2) - 7170*Power(t2,3) - 
                  8178*Power(t2,4) - 10*Power(t2,5) + 247*Power(t2,6) - 
                  41*Power(t2,7) + 
                  Power(s1,2)*
                   (-122 - 456*t2 + 752*Power(t2,2) + 
                     252*Power(t2,3) - 64*Power(t2,4) + 
                     21*Power(t2,5) + 6*Power(t2,6)) + 
                  s1*(784 - 6704*t2 - 2828*Power(t2,2) + 
                     9158*Power(t2,3) - 881*Power(t2,4) + 
                     280*Power(t2,5) + 99*Power(t2,6) - 4*Power(t2,7)))) \
- Power(t1,3)*(Power(s1,3)*(40 - 52*t2 - 150*Power(t2,2) + 
                  272*Power(t2,3) + 33*Power(t2,4) + 97*Power(t2,5) - 
                  4*Power(t2,6) - 4*Power(t2,7)) + 
               Power(s,3)*(4 + 2*t2 - 318*Power(t2,2) + 
                  678*Power(t2,3) + 236*Power(t2,4) + 272*Power(t2,5) + 
                  65*Power(t2,6) + 6*Power(t2,7)) + 
               s1*(-346 - 864*t2 + 2400*Power(t2,2) - 
                  2120*Power(t2,3) + 457*Power(t2,4) + 
                  4371*Power(t2,5) + 960*Power(t2,6) - 
                  780*Power(t2,7) - 30*Power(t2,8)) + 
               Power(s1,2)*(-390 + 974*t2 + 582*Power(t2,2) - 
                  582*Power(t2,3) - 2443*Power(t2,4) - 79*Power(t2,5) + 
                  146*Power(t2,6) - 59*Power(t2,7) + 3*Power(t2,8)) + 
               Power(s,2)*(-78 + (-242 + 40*s1)*t2 + 
                  2*(-569 + 1183*s1)*Power(t2,2) - 
                  4*(-344 + 575*s1)*Power(t2,3) + 
                  (89 - 713*s1)*Power(t2,4) + 
                  (1052 - 579*s1)*Power(t2,5) - 
                  8*(-56 + 19*s1)*Power(t2,6) + 
                  (30 - 16*s1)*Power(t2,7) + 6*Power(t2,8)) - 
               2*(-132 + 161*t2 + 167*Power(t2,2) + 2314*Power(t2,3) - 
                  1029*Power(t2,4) - 1463*Power(t2,5) + 
                  354*Power(t2,6) + 76*Power(t2,7) + 14*Power(t2,8)) + 
               s*(-66 + 814*t2 + 364*Power(t2,2) - 806*Power(t2,3) + 
                  2424*Power(t2,4) - 5641*Power(t2,5) - 
                  1369*Power(t2,6) + 268*Power(t2,7) - 8*Power(t2,8) + 
                  Power(s1,2)*
                   (180 + 154*t2 - 10*Power(t2,2) - 906*Power(t2,3) + 
                     666*Power(t2,4) + 267*Power(t2,5) + 
                     91*Power(t2,6) + 14*Power(t2,7)) + 
                  s1*(-124 + 116*t2 + 1188*Power(t2,2) - 
                     4474*Power(t2,3) - 119*Power(t2,4) + 
                     1787*Power(t2,5) - 228*Power(t2,6) + 
                     43*Power(t2,7) - 9*Power(t2,8))))) - 
         s2*(40*Power(t1,9) - 
            (-1 + t2)*Power(t2,4)*(1 + (-1 + s)*t2)*
             (-10 + (-20*s + 6*(4 + s1))*t2 - 
               (13 - 39*s + 10*Power(s,2) + 10*s1)*Power(t2,2) + 
               (9*Power(s,2) + 3*s1 - s*(5 + s1))*Power(t2,3) + 
               (s1 - s*(2 + s1))*Power(t2,4) + (-1 + s)*Power(t2,5)) + 
            t1*Power(t2,2)*(60 + 20*(-13 + 9*s - 3*s1)*t2 + 
               5*(74 + 36*Power(s,2) + 39*s1 - 3*s*(49 + 2*s1))*
                Power(t2,2) + 
               (-173 + 60*Power(s,3) + 30*Power(s,2)*(-20 + s1) - 
                  189*s1 + 10*Power(s1,2) + 
                  s*(841 + 52*s1 - 6*Power(s1,2)))*Power(t2,3) + 
               (-7 - 125*Power(s,3) + Power(s,2)*(448 - 73*s1) + 
                  29*s1 - 32*Power(s1,2) + 
                  s*(-182 + 60*s1 + 17*Power(s1,2)))*Power(t2,4) + 
               (7 + 53*Power(s,3) + 39*s1 + 32*Power(s1,2) + 
                  11*Power(s,2)*(1 + 9*s1) - 
                  s*(79 + 145*s1 + 16*Power(s1,2)))*Power(t2,5) + 
               (-15 + 16*Power(s,3) - 24*s1 - 8*Power(s1,2) - 
                  50*Power(s,2)*(1 + s1) + 
                  s*(11 + 58*s1 + 3*Power(s1,2)))*Power(t2,6) + 
               (22 + 2*Power(s,3) + Power(s,2)*(15 - 4*s1) + 10*s1 - 
                  2*Power(s1,2) + s*(-32 + 5*s1 + 2*Power(s1,2)))*
                Power(t2,7) + 4*(-1 - s + Power(s,2))*Power(t2,8)) + 
            Power(t1,2)*(60 - 12*(1 + 3*s + 3*s1)*t2 - 
               6*(-13 + 24*Power(s,2) + 16*s1 - 18*Power(s1,2) + 
                  s*(-113 + 18*s1))*Power(t2,2) - 
               2*(267 + 30*Power(s,3) - 54*s1 + 149*Power(s1,2) + 
                  6*Power(s1,3) + 15*Power(s,2)*(-29 + 6*s1) + 
                  s*(344 - 226*s1 - 30*Power(s1,2)))*Power(t2,3) + 
               (282 + 180*Power(s,3) + 167*s1 + 329*Power(s1,2) + 
                  24*Power(s1,3) + Power(s,2)*(-542 + 540*s1) - 
                  3*s*(227 + 289*s1 + 60*Power(s1,2)))*Power(t2,4) + 
               (286 - 30*Power(s,3) - 93*s1 - 212*Power(s1,2) + 
                  3*Power(s1,3) - Power(s,2)*(275 + 819*s1) + 
                  s*(53 + 777*s1 + 102*Power(s1,2)))*Power(t2,5) + 
               (-24 - 147*Power(s,3) + 115*s1 + 16*Power(s1,2) - 
                  23*Power(s1,3) + 3*Power(s,2)*(44 + 115*s1) + 
                  s*(487 - 150*s1 + 79*Power(s1,2)))*Power(t2,6) - 
               (161 + 25*Power(s,3) + Power(s,2)*(50 - 83*s1) + 
                  122*s1 - 71*Power(s1,2) - 7*Power(s1,3) + 
                  s*(-98 + 64*s1 + 63*Power(s1,2)))*Power(t2,7) + 
               (9 - 7*Power(s,3) - 60*s1 - 15*Power(s1,2) + 
                  Power(s1,3) + 2*Power(s,2)*(-39 + 8*s1) + 
                  s*(131 + 4*s1 - 10*Power(s1,2)))*Power(t2,8) + 
               (16 - 8*Power(s,2) + 17*s1 + Power(s1,2) + 
                  s*(-9 + 4*s1))*Power(t2,9)) - 
            4*Power(t1,8)*(37 + 6*Power(s,2) + 6*Power(s1,2) + 21*t2 + 
               16*Power(t2,2) - 2*s1*(47 + 7*t2) + 
               s*(32 - 12*s1 + 14*t2)) - 
            2*Power(t1,7)*(4*Power(s,3)*(1 + 7*t2) - 
               4*Power(s1,3)*(12 + 7*t2) + 
               Power(s1,2)*(-206 + 94*t2 + 33*Power(t2,2)) + 
               s1*(56 + 370*t2 + 274*Power(t2,2) + 5*Power(t2,3)) - 
               2*(102 + 278*t2 - 91*Power(t2,2) + 37*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s,2)*(98 - 74*t2 + 33*Power(t2,2) - 
                  28*s1*(2 + 3*t2)) + 
               s*(364 + 418*t2 - 185*Power(t2,2) - 5*Power(t2,3) + 
                  4*Power(s1,2)*(25 + 21*t2) + 
                  s1*(28 - 20*t2 - 66*Power(t2,2)))) + 
            Power(t1,6)*(20 + 788*t2 - 1234*Power(t2,2) + 
               392*Power(t2,3) + 123*Power(t2,4) - 70*Power(t2,5) - 
               4*Power(s1,3)*
                (-26 + 81*t2 + 16*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s,3)*(64 - 228*t2 + 58*Power(t2,2) + 
                  20*Power(t2,3)) + 
               2*Power(s1,2)*
                (756 + 436*t2 - 165*Power(t2,2) + 112*Power(t2,3) + 
                  17*Power(t2,4)) - 
               s1*(1192 + 4116*t2 + 1510*Power(t2,2) - 
                  1650*Power(t2,3) - 155*Power(t2,4) + 20*Power(t2,5)) \
- 2*Power(s,2)*(116 - 412*t2 + 407*Power(t2,2) - 30*Power(t2,3) - 
                  17*Power(t2,4) + 
                  s1*(44 - 210*t2 + 90*Power(t2,2) + 30*Power(t2,3))) + 
               s*(760 + 1644*t2 + 3452*Power(t2,2) - 430*Power(t2,3) - 
                  101*Power(t2,4) + 20*Power(t2,5) + 
                  6*Power(s1,2)*
                   (-24 + 22*t2 + 31*Power(t2,2) + 10*Power(t2,3)) - 
                  4*s1*(656 + 704*t2 - 294*Power(t2,2) + 
                     71*Power(t2,3) + 17*Power(t2,4)))) + 
            Power(t1,5)*(-504 - 1960*t2 + 3548*Power(t2,2) - 
               454*Power(t2,3) - 367*Power(t2,4) - 272*Power(t2,5) + 
               27*Power(t2,6) + 
               Power(s,3)*(88 + 108*t2 + 264*Power(t2,2) + 
                  166*Power(t2,3) - 11*Power(t2,4) + Power(t2,5)) - 
               Power(s1,3)*(-176 + 100*t2 - 138*Power(t2,2) - 
                  114*Power(t2,3) + 2*Power(t2,4) + Power(t2,5)) + 
               Power(s1,2)*(432 - 1224*t2 - 1918*Power(t2,2) + 
                  180*Power(t2,3) - 10*Power(t2,4) - 69*Power(t2,5) + 
                  2*Power(t2,6)) + 
               2*s1*(268 - 540*t2 + 665*Power(t2,2) + 
                  3107*Power(t2,3) - 463*Power(t2,4) - 
                  309*Power(t2,5) + 14*Power(t2,6)) + 
               Power(s,2)*(320 + 824*t2 - 1254*Power(t2,2) + 
                  1248*Power(t2,3) + 391*Power(t2,4) - 
                  41*Power(t2,5) + 2*Power(t2,6) - 
                  s1*(640 + 540*t2 + 502*Power(t2,2) + 
                     394*Power(t2,3) - 20*Power(t2,4) + 3*Power(t2,5))) \
+ s*(376 + 3144*t2 - 830*Power(t2,2) - 5062*Power(t2,3) - 
                  342*Power(t2,4) + 284*Power(t2,5) - 34*Power(t2,6) + 
                  Power(s1,2)*
                   (-520 + 596*t2 + 244*Power(t2,2) + 
                     114*Power(t2,3) - 7*Power(t2,4) + 3*Power(t2,5)) - 
                  s1*(1712 + 4752*t2 - 6164*Power(t2,2) + 
                     420*Power(t2,3) + 419*Power(t2,4) - 
                     110*Power(t2,5) + 4*Power(t2,6)))) + 
            Power(t1,4)*(4 + 172*t2 + 2132*Power(t2,2) - 
               202*Power(t2,3) - 2603*Power(t2,4) + 808*Power(t2,5) + 
               205*Power(t2,6) + 15*Power(t2,7) - 
               Power(s,3)*(16 - 212*t2 + 478*Power(t2,2) + 
                  276*Power(t2,3) + 194*Power(t2,4) + 58*Power(t2,5) + 
                  5*Power(t2,6)) + 
               Power(s1,3)*(-56 + 276*t2 - 356*Power(t2,2) + 
                  44*Power(t2,3) - 54*Power(t2,4) + 11*Power(t2,5) + 
                  7*Power(t2,6)) + 
               Power(s1,2)*(208 - 1048*t2 + 270*Power(t2,2) + 
                  1582*Power(t2,3) + 77*Power(t2,4) - 
                  112*Power(t2,5) + 35*Power(t2,6) - 5*Power(t2,7)) + 
               s1*(592 - 1536*t2 + 2030*Power(t2,2) - 
                  166*Power(t2,3) - 2398*Power(t2,4) - 
                  1369*Power(t2,5) + 581*Power(t2,6) + 34*Power(t2,7)) \
+ Power(s,2)*(32 + 1048*t2 - 658*Power(t2,2) + 402*Power(t2,3) - 
                  596*Power(t2,4) - 709*Power(t2,5) - 59*Power(t2,6) - 
                  7*Power(t2,7) + 
                  s1*(40 - 1652*t2 + 824*Power(t2,2) + 
                     1012*Power(t2,3) + 418*Power(t2,4) + 
                     165*Power(t2,5) + 17*Power(t2,6))) - 
               s*(24 + 1104*t2 - 1334*Power(t2,2) + 2330*Power(t2,3) - 
                  2986*Power(t2,4) - 1325*Power(t2,5) + 
                  244*Power(t2,6) + 3*Power(t2,7) + 
                  Power(s1,2)*
                   (288 + 244*t2 - 1322*Power(t2,2) + 
                     780*Power(t2,3) + 230*Power(t2,4) + 
                     118*Power(t2,5) + 19*Power(t2,6)) - 
                  s1*(-368 + 32*t2 + 1476*Power(t2,2) + 
                     1568*Power(t2,3) - 1685*Power(t2,4) + 
                     479*Power(t2,5) + 29*Power(t2,6) + 12*Power(t2,7)))\
) + Power(t1,3)*(120 + 240*t2 + 580*Power(t2,2) - 1458*Power(t2,3) - 
               355*Power(t2,4) + 657*Power(t2,5) + 374*Power(t2,6) - 
               219*Power(t2,7) - 15*Power(t2,8) - 
               Power(s1,3)*t2*
                (4 - 10*t2 + 126*Power(t2,2) - 156*Power(t2,3) + 
                  11*Power(t2,4) + 7*Power(t2,5) + 6*Power(t2,6)) + 
               Power(s,3)*t2*
                (-4 - 68*t2 - 138*Power(t2,2) + 465*Power(t2,3) + 
                  101*Power(t2,4) + 67*Power(t2,5) + 9*Power(t2,6)) + 
               s1*(312 - 1124*t2 + 906*Power(t2,2) - 280*Power(t2,3) - 
                  809*Power(t2,4) + 643*Power(t2,5) + 649*Power(t2,6) - 
                  14*Power(t2,7) - 59*Power(t2,8)) + 
               Power(s1,2)*(-108 + 372*t2 - 272*Power(t2,2) + 
                  360*Power(t2,3) - 86*Power(t2,4) - 445*Power(t2,5) + 
                  77*Power(t2,6) + 4*Power(t2,7) + 2*Power(t2,8)) + 
               Power(s,2)*(36 + 12*(-29 + 3*s1)*t2 + 
                  (296 - 574*s1)*Power(t2,2) + 
                  6*(-62 + 321*s1)*Power(t2,3) + 
                  (169 - 862*s1)*Power(t2,4) + 
                  (5 - 407*s1)*Power(t2,5) + 
                  (412 - 158*s1)*Power(t2,6) - 
                  9*(-14 + 3*s1)*Power(t2,7) + 10*Power(t2,8)) + 
               s*(Power(s1,2)*t2*
                   (36 + 200*t2 + 66*Power(t2,2) - 615*Power(t2,3) + 
                     305*Power(t2,4) + 105*Power(t2,5) + 24*Power(t2,6)) \
+ t2*(252 + 500*t2 + 1712*Power(t2,2) - 2039*Power(t2,3) + 
                     167*Power(t2,4) - 1036*Power(t2,5) - 
                     43*Power(t2,6) + 28*Power(t2,7)) - 
                  s1*(216 + 56*t2 - 424*Power(t2,2) + 228*Power(t2,3) + 
                     351*Power(t2,4) + 178*Power(t2,5) - 
                     188*Power(t2,6) + 78*Power(t2,7) + 12*Power(t2,8))))\
) + Power(s2,3)*(16*Power(t1,8) + 
            Power(t2,2)*(-20 + 4*(1 + 3*s + 3*s1)*t2 + 
               (-1 + 48*Power(s,2) + 67*s1 - 6*Power(s1,2) - 
                  8*s*(22 + 3*s1))*Power(t2,2) + 
               (137 - 250*Power(s,2) + 20*Power(s,3) - 132*s1 - 
                  16*Power(s1,2) + 2*Power(s1,3) + s*(211 + 141*s1))*
                Power(t2,3) + 
               (-135 - 70*Power(s,3) - 20*s1 + 42*Power(s1,2) - 
                  4*Power(s1,3) + 2*Power(s,2)*(117 + 5*s1) + 
                  s*(198 - 171*s1 + 5*Power(s1,2)))*Power(t2,4) + 
               (20 + 50*Power(s,3) + 83*s1 + 4*Power(s1,2) + 
                  Power(s,2)*(46 + 42*s1) - 
                  s*(201 + 18*s1 + 4*Power(s1,2)))*Power(t2,5) + 
               (-58 + 12*Power(s,3) - 54*s1 - 17*Power(s1,2) + 
                  2*Power(s1,3) - Power(s,2)*(98 + 53*s1) + 
                  s*(25 + 52*s1 + 2*Power(s1,2)))*Power(t2,6) + 
               (63 + 47*Power(s,2) + 44*s1 - 10*Power(s1,2) + 
                  s*(-69 - 2*s1 + Power(s1,2)))*Power(t2,7) + 
               (-9 + 5*s1 + 3*Power(s1,2) + 2*s*(-7 + 3*s1))*
                Power(t2,8) + (-1 + 3*s - 5*s1)*Power(t2,9)) - 
            t1*(-20 - 60*(-1 + s - 3*s1)*t2 + 
               2*(30 + 24*Power(s,2) - 107*s1 - 72*Power(s1,2) + 
                  s*(25 + 6*s1))*Power(t2,2) + 
               2*(219 + 16*Power(s,3) - 211*s1 + 110*Power(s1,2) + 
                  16*Power(s1,3) + Power(s,2)*(-220 + 6*s1) + 
                  s*(91 - 16*s1 + 6*Power(s1,2)))*Power(t2,3) - 
               (92 + 166*Power(s,3) + 116*s1 - 194*Power(s1,2) + 
                  83*Power(s1,3) + 32*Power(s,2)*(-15 + 4*s1) + 
                  s*(-1151 + 330*s1 - 180*Power(s1,2)))*Power(t2,4) + 
               (-684 + 84*Power(s,3) + 450*s1 - 23*Power(s1,2) + 
                  15*Power(s1,3) + Power(s,2)*(98 + 772*s1) + 
                  s*(-440 + 167*s1 - 167*Power(s1,2)))*Power(t2,5) + 
               (-639 + 160*Power(s,3) - 278*s1 - 85*Power(s1,2) + 
                  49*Power(s1,3) - Power(s,2)*(206 + 635*s1) - 
                  s*(495 + 477*s1 + 52*Power(s1,2)))*Power(t2,6) + 
               (781 + 46*Power(s,3) + Power(s,2)*(269 - 100*s1) + 
                  422*s1 - 344*Power(s1,2) - 4*Power(s1,3) + 
                  s*(-296 + 307*s1 + 94*Power(s1,2)))*Power(t2,7) + 
               (46 + 11*Power(s,3) + Power(s,2)*(83 - 21*s1) + 
                  338*s1 + 90*Power(s1,2) + 3*Power(s1,3) + 
                  s*(-581 + 52*s1 + 8*Power(s1,2)))*Power(t2,8) + 
               (-15 + 6*Power(s,2) - 119*s1 - 4*Power(s1,2) + 
                  6*s*(7 + 2*s1))*Power(t2,9) + 
               (-11 + 6*s - 17*s1)*Power(t2,10)) - 
            2*Power(t1,7)*(-50 + 9*Power(s,2) + 9*Power(s1,2) - 6*t2 + 
               144*Power(t2,2) - s1*(152 + 103*t2) + 
               s*(65 - 18*s1 + 103*t2)) - 
            2*Power(t1,6)*(-533 - 1530*t2 + 140*Power(t2,2) - 
               342*Power(t2,3) - 100*Power(t2,4) + 
               9*Power(s,3)*(1 + 6*t2) - 2*Power(s1,3)*(53 + 27*t2) + 
               Power(s1,2)*(-381 - 170*t2 + 72*Power(t2,2)) + 
               s1*(429 + 1713*t2 + 1360*Power(t2,2) + 70*Power(t2,3)) - 
               Power(s,2)*(35 + 272*t2 - 72*Power(t2,2) + 
                  2*s1*(62 + 81*t2)) + 
               s*(8 - 255*t2 - 825*Power(t2,2) - 70*Power(t2,3) + 
                  Power(s1,2)*(221 + 162*t2) + 
                  s1*(400 + 442*t2 - 144*Power(t2,2)))) + 
            Power(t1,5)*(2*Power(s,3)*
                (22 - 69*t2 + 155*Power(t2,2) + 25*Power(t2,3)) - 
               2*Power(s1,3)*
                (-135 + 479*t2 + 316*Power(t2,2) + 25*Power(t2,3)) + 
               Power(s1,2)*(1110 - 1104*t2 - 1022*Power(t2,2) + 
                  858*Power(t2,3) + 121*Power(t2,4)) + 
               s1*(-318 - 926*t2 + 1704*Power(t2,2) + 
                  7044*Power(t2,3) + 1349*Power(t2,4) - 30*Power(t2,5)) \
- 2*(266 + 691*t2 + 4383*Power(t2,2) + 1429*Power(t2,3) - 
                  284*Power(t2,4) + 237*Power(t2,5) + 16*Power(t2,6)) + 
               Power(s,2)*(126 + 2188*t2 - 592*Power(t2,2) + 
                  402*Power(t2,3) + 121*Power(t2,4) - 
                  2*s1*(-147 - 3*t2 + 626*Power(t2,2) + 75*Power(t2,3))) \
+ s*(890 + 3462*t2 + 5298*Power(t2,2) - 2000*Power(t2,3) - 
                  767*Power(t2,4) + 30*Power(t2,5) + 
                  2*Power(s1,2)*
                   (-424 + 545*t2 + 787*Power(t2,2) + 75*Power(t2,3)) - 
                  2*s1*(1458 + 1750*t2 - 829*Power(t2,2) + 
                     630*Power(t2,3) + 121*Power(t2,4)))) + 
            Power(t1,4)*(-588 - 1810*t2 + 2146*Power(t2,2) + 
               1612*Power(t2,3) + 6820*Power(t2,4) - 819*Power(t2,5) + 
               29*Power(t2,6) + 66*Power(t2,7) - 
               Power(s1,3)*(168 + 300*t2 - 908*Power(t2,2) - 
                  986*Power(t2,3) - 158*Power(t2,4) + Power(t2,5)) + 
               Power(s,3)*(30 + 84*t2 + 376*Power(t2,2) + 
                  76*Power(t2,3) - 66*Power(t2,4) + Power(t2,5)) - 
               Power(s1,2)*(-602 + 5726*t2 + 5610*Power(t2,2) - 
                  1972*Power(t2,3) + 596*Power(t2,4) + 
                  414*Power(t2,5) + 9*Power(t2,6)) + 
               s1*(774 + 2998*t2 + 8930*Power(t2,2) + 
                  10024*Power(t2,3) - 4471*Power(t2,4) - 
                  3536*Power(t2,5) - 65*Power(t2,6) + 8*Power(t2,7)) - 
               Power(s,2)*(-218 - 454*t2 + 1552*Power(t2,2) + 
                  202*Power(t2,3) + 67*Power(t2,4) + 288*Power(t2,5) + 
                  9*Power(t2,6) + 
                  s1*(196 - 396*t2 + 1148*Power(t2,2) - 
                     374*Power(t2,3) - 290*Power(t2,4) + 3*Power(t2,5))) \
+ s*(-122 - 110*t2 - 8144*Power(t2,2) - 12936*Power(t2,3) - 
                  2284*Power(t2,4) + 1327*Power(t2,5) - 45*Power(t2,6) - 
                  8*Power(t2,7) + 
                  Power(s1,2)*
                   (-914 + 204*t2 + 480*Power(t2,2) - 
                     1436*Power(t2,3) - 382*Power(t2,4) + 3*Power(t2,5)) \
+ 2*s1*(-874 + 892*t2 + 7385*Power(t2,2) + 173*Power(t2,3) + 
                     359*Power(t2,4) + 351*Power(t2,5) + 9*Power(t2,6)))) \
- Power(t1,3)*(-172 - 510*t2 - 4898*Power(t2,2) + 708*Power(t2,3) + 
               5114*Power(t2,4) + 1002*Power(t2,5) + 787*Power(t2,6) - 
               332*Power(t2,7) + 21*Power(t2,8) + 
               Power(s1,3)*(170 - 162*t2 - 92*Power(t2,2) + 
                  222*Power(t2,3) + 590*Power(t2,4) + 173*Power(t2,5) - 
                  3*Power(t2,6)) + 
               Power(s,3)*(-8 - 86*t2 + 506*Power(t2,2) + 
                  230*Power(t2,3) + 412*Power(t2,4) + 96*Power(t2,5) + 
                  13*Power(t2,6)) - 
               Power(s1,2)*(-164 - 1572*t2 + 1362*Power(t2,2) + 
                  6250*Power(t2,3) + 1490*Power(t2,4) - 
                  727*Power(t2,5) + 340*Power(t2,6) + 23*Power(t2,7)) + 
               s1*(-854 + 3012*t2 - 1014*Power(t2,2) + 
                  1886*Power(t2,3) + 9093*Power(t2,4) + 
                  4277*Power(t2,5) - 3044*Power(t2,6) - 
                  446*Power(t2,7) + 10*Power(t2,8)) - 
               Power(s,2)*(4 + 16*t2 - 2804*Power(t2,2) - 
                  290*Power(t2,3) - 343*Power(t2,4) - 451*Power(t2,5) + 
                  123*Power(t2,6) + 12*Power(t2,7) + 
                  s1*(-42 - 634*t2 + 2784*Power(t2,2) + 
                     322*Power(t2,3) + 841*Power(t2,4) + 
                     98*Power(t2,5) + 29*Power(t2,6))) + 
               s*(Power(s1,2)*
                   (100 - 66*t2 - 718*Power(t2,2) + 662*Power(t2,3) + 
                     102*Power(t2,4) - 171*Power(t2,5) + 19*Power(t2,6)) \
+ s1*(416 + 124*t2 - 8026*Power(t2,2) + 2092*Power(t2,3) + 
                     6968*Power(t2,4) - 234*Power(t2,5) + 
                     503*Power(t2,6) + 35*Power(t2,7)) + 
                  2*(16 + 78*t2 + 350*Power(t2,2) + 2715*Power(t2,3) - 
                     5948*Power(t2,4) - 3163*Power(t2,5) + 
                     345*Power(t2,6) + 50*Power(t2,7) - 8*Power(t2,8)))) \
+ Power(t1,2)*(130 + 182*t2 - 62*Power(t2,2) - 1240*Power(t2,3) - 
               4077*Power(t2,4) + 2890*Power(t2,5) + 1482*Power(t2,6) - 
               3*Power(t2,7) - 65*Power(t2,8) - 23*Power(t2,9) + 
               Power(s1,3)*t2*
                (60 - 72*t2 - 134*Power(t2,2) + 190*Power(t2,3) + 
                  90*Power(t2,4) + 73*Power(t2,5) + Power(t2,6)) + 
               Power(s,3)*t2*(12 - 100*t2 - 132*Power(t2,2) + 
                  556*Power(t2,3) + 172*Power(t2,4) + 146*Power(t2,5) + 
                  23*Power(t2,6)) + 
               Power(s1,2)*(-180 + 30*t2 + 1110*Power(t2,2) - 
                  52*Power(t2,3) - 571*Power(t2,4) - 2290*Power(t2,5) + 
                  254*Power(t2,6) + 38*Power(t2,7) - 21*Power(t2,8)) - 
               s1*(-140 + 1228*t2 + 70*Power(t2,2) - 2364*Power(t2,3) + 
                  1720*Power(t2,4) - 1621*Power(t2,5) - 
                  3144*Power(t2,6) + 124*Power(t2,7) + 485*Power(t2,8) + 
                  10*Power(t2,9)) + 
               Power(s,2)*t2*(-194 + 76*t2 - 646*Power(t2,2) + 
                  1110*Power(t2,3) + 509*Power(t2,4) + 527*Power(t2,5) + 
                  80*Power(t2,6) + 3*Power(t2,7) - 
                  s1*(60 - 120*t2 - 1846*Power(t2,2) + 
                     2256*Power(t2,3) + 358*Power(t2,4) + 
                     301*Power(t2,5) + 47*Power(t2,6))) + 
               s*(50 + 232*t2 + 698*Power(t2,2) - 172*Power(t2,3) - 
                  574*Power(t2,4) + 851*Power(t2,5) - 4754*Power(t2,6) - 
                  517*Power(t2,7) + 171*Power(t2,8) - 5*Power(t2,9) + 
                  Power(s1,2)*t2*
                   (180 + 124*t2 - 124*Power(t2,2) - 456*Power(t2,3) + 
                     394*Power(t2,4) + 115*Power(t2,5) + 23*Power(t2,6)) \
+ s1*(-60 - 92*t2 - 370*Power(t2,2) + 1486*Power(t2,3) - 
                     4291*Power(t2,4) + 1378*Power(t2,5) + 
                     1006*Power(t2,6) + 78*Power(t2,7) + 23*Power(t2,8))))\
))*T2q(t1,1 - s2 + t1 - t2))/
     ((-1 + t1)*(s - s2 + t1)*Power(1 - s2 + t1 - t2,2)*(s - s1 + t2)*
       Power(t1 - s2*t2,3)*Power(-Power(s2,2) + 4*t1 - 2*s2*t2 - 
         Power(t2,2),2)) + (8*(Power(s2,10)*
          (Power(s1,3) + s1*Power(t2,2) - 2*Power(t2,3)) - 
         Power(s2,9)*(Power(s1,3)*(1 + 7*t1 - 3*t2) + 
            Power(s1,2)*(3 + (-4 + t1)*t2 - 2*Power(t2,2) - 
               s*(3*t1 + t2)) + 
            s1*t2*((11 - 2*t2)*t2 + s*(3 - 3*t1 + t2) + t1*(2 + 4*t2)) + 
            Power(t2,2)*(1 + s*(3 - 4*t1 - 2*t2) - 6*t2 + 
               5*Power(t2,2) - 2*t1*(3 + 8*t2))) + 
         Power(s2,8)*(Power(s1,3)*
             (-3 + 21*Power(t1,2) + t1*(5 - 20*t2) + t2 + 3*Power(t2,2)) \
+ Power(s1,2)*(3 - 5*(3 + s)*t2 + 3*s*Power(t2,2) + 5*Power(t2,3) + 
               Power(t1,2)*(1 - 18*s + 6*t2) + 
               t1*(17 - 33*t2 - 12*Power(t2,2) + s*(-7 + 6*t2))) - 
            t2*(Power(t1,2)*(6 + 48*t2 + 50*Power(t2,2)) + 
               t1*(-2 + 14*t2 + 28*Power(t2,2) - 36*Power(t2,3)) + 
               t2*(-11 + 20*t2 - 7*Power(t2,2) + 4*Power(t2,3)) - 
               Power(s,2)*(3*Power(t1,2) + t2 - t1*(3 + 5*t2)) + 
               s*(-3 - 2*t2 + 25*Power(t2,2) - 5*Power(t2,3) + 
                  Power(t1,2)*(8 + 17*t2) + 
                  t1*(-3 + 5*t2 + 7*Power(t2,2)))) + 
            s1*(3 + Power(s,2)*t1*(3*t1 - t2) - 8*t2 + 20*Power(t2,2) - 
               11*Power(t2,3) + 2*Power(t2,4) + 
               t1*t2*(24 + 58*t2 - Power(t2,2)) + 
               Power(t1,2)*(1 + 8*t2 + 3*Power(t2,2)) + 
               s*(-(Power(t1,2)*(3 + 17*t2)) + 
                  t2*(4 + t2 - 3*Power(t2,2)) + 
                  t1*(-3 + 19*t2 + 18*Power(t2,2))))) + 
         Power(s2,7)*(-1 + 4*t2 - 5*s*t2 - 22*Power(t2,2) + 
            19*s*Power(t2,2) + 6*Power(s,2)*Power(t2,2) + 
            41*Power(t2,3) + 35*s*Power(t2,3) + 
            9*Power(s,2)*Power(t2,3) - 40*Power(t2,4) - 
            44*s*Power(t2,4) - 4*Power(t2,5) + 4*s*Power(t2,5) - 
            Power(t2,6) + Power(t1,2)*
             (-1 + (10 + 21*s + 17*Power(s,2) - 2*Power(s,3))*t2 + 
               (111 + 50*s + 30*Power(s,2))*Power(t2,2) + 
               (9 + 23*s)*Power(t2,3) - 92*Power(t2,4)) + 
            Power(s1,3)*(1 - 35*Power(t1,3) - 3*t2 + 4*Power(t2,2) + 
               Power(t2,3) + Power(t1,2)*(-8 + 57*t2) + 
               t1*(21 - 11*t2 - 19*Power(t2,2))) + 
            Power(t1,3)*(2 + Power(s,3) + 48*t2 + 150*Power(t2,2) + 
               80*Power(t2,3) - Power(s,2)*(3 + 13*t2) + 
               s*(4 + 34*t2 + 26*Power(t2,2))) + 
            t1*t2*(-23 - 4*t2 + Power(s,3)*t2 + 88*Power(t2,2) - 
               42*Power(t2,3) + 27*Power(t2,4) + 
               Power(s,2)*(8 - 7*t2 - 11*Power(t2,2)) + 
               s*(-21 + 73*t2 + 77*Power(t2,2) - 32*Power(t2,3))) + 
            Power(s1,2)*(9 + 3*Power(t1,3)*(-2 + 15*s - 5*t2) - 
               (12 + s)*t2 - (11 + 9*s)*Power(t2,2) + 
               (-10 + 3*s)*Power(t2,3) + 4*Power(t2,4) + 
               t1*(-9 + 97*t2 - 13*Power(t2,2) - 22*Power(t2,3) + 
                  s*(-1 + 16*t2 + Power(t2,2))) + 
               Power(t1,2)*(s*(33 - 51*t2) + 
                  4*(-8 + 29*t2 + 7*Power(t2,2)))) - 
            s1*(3 - (21 + 16*s)*t2 + 
               (17 + 15*s + 2*Power(s,2))*Power(t2,2) - 
               2*(5 + 3*s)*Power(t2,3) + (-15 + 4*s)*Power(t2,4) - 
               2*Power(t2,5) + 
               Power(t1,3)*(4 + 15*Power(s,2) + 6*t2 - 10*Power(t2,2) - 
                  s*(17 + 40*t2)) + 
               Power(t1,2)*(13 + Power(s,2)*(8 - 17*t2) + 134*t2 + 
                  151*Power(t2,2) + 24*Power(t2,3) + 
                  s*(-18 + 81*t2 + 70*Power(t2,2))) + 
               t1*(13 - 18*t2 + 91*Power(t2,2) - 31*Power(t2,3) - 
                  5*Power(t2,4) + 2*Power(s,2)*t2*(3 + 2*t2) + 
                  s*(-8 + 73*t2 + 11*Power(t2,2) - 29*Power(t2,3))))) + 
         Power(t1,3)*((-9 + 7*s1)*Power(t1,6) - 
            (-2 + t2)*Power(-1 + t2,4) - 
            t1*Power(-1 + t2,3)*
             (4 + s1*(5 - 4*t2) + 2*t2 + Power(t2,2)) + 
            Power(t1,5)*(-23 + 2*Power(s1,3) - 8*s1*(-2 + t2) + 19*t2 - 
               2*Power(s1,2)*(1 + t2)) + 
            Power(t1,2)*Power(-1 + t2,2)*
             (20 + Power(s1,2)*(3 - 5*t2) + 5*t2 - Power(t2,2) + 
               s1*(7 + 3*t2 + Power(t2,2))) + 
            Power(t1,4)*(-5 + 50*t2 - 3*Power(s1,3)*t2 - 
               22*Power(t2,2) + 2*s1*(-5 - 11*t2 + 2*Power(t2,2)) + 
               Power(s1,2)*(17 + t2 + 2*Power(t2,2))) - 
            Power(t1,3)*(-1 + t2)*
             (11 + 36*t2 - 2*Power(s1,3)*t2 - 14*Power(t2,2) + 
               2*Power(s1,2)*(5 + t2) + s1*(7 - 17*t2 + 4*Power(t2,2))) \
- Power(s,3)*(4 + Power(t1,5) - 6*t2 + 2*Power(t2,2) - 2*Power(t2,3) + 
               3*Power(t2,4) - Power(t2,5) - 2*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(1 + 7*t2 + Power(t2,2)) - 
               Power(t1,2)*(-24 + 8*t2 + 5*Power(t2,2) + Power(t2,3)) + 
               t1*(22 - 30*t2 + 9*Power(t2,2) - 3*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s,2)*(Power(t1,5)*(-7 + 4*s1 - t2) - 
               Power(-1 + t2,2)*
                (2 + 2*t2 - 6*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,4)*(-32 + 18*t2 + 4*Power(t2,2) - 
                  s1*(2 + 7*t2)) + 
               Power(t1,2)*(-36 + 41*t2 - 12*Power(t2,2) + 
                  Power(t2,3) + 4*Power(t2,4) + 
                  s1*(-18 + 24*t2 - 7*Power(t2,3))) + 
               Power(t1,3)*(2*s1*(-10 + 7*t2 + 3*Power(t2,2)) - 
                  3*(21 - 18*t2 + 5*Power(t2,2) + 2*Power(t2,3))) + 
               t1*(-1 + t2)*(12 + 10*t2 - 17*Power(t2,2) + 
                  5*Power(t2,3) - Power(t2,4) + 
                  s1*(-2 + 4*t2 - 8*Power(t2,2) + 4*Power(t2,3)))) + 
            s*(-5*Power(t1,6) + 
               Power(-1 + t2,3)*(-2 - 6*t2 + 3*Power(t2,2)) - 
               Power(t1,4)*(66 + s1 - 41*t2 + 8*s1*t2 - 
                  8*Power(s1,2)*t2 + 6*Power(t2,2) + 7*s1*Power(t2,2)) + 
               Power(t1,5)*(-5*Power(s1,2) + 10*(-3 + t2) + 
                  s1*(11 + 3*t2)) + 
               t1*Power(-1 + t2,2)*
                (23 + 26*t2 - Power(t2,2) + 2*Power(t2,3) + 
                  s1*(-10 + 9*t2 - 8*Power(t2,2))) + 
               Power(t1,2)*(-1 + t2)*
                (-1 - 71*t2 + Power(t2,2) - 5*Power(t2,3) + 
                  Power(s1,2)*(2 - 8*t2 + 5*Power(t2,2)) + 
                  s1*(13 + 8*t2 + 3*Power(t2,2) - Power(t2,3))) + 
               Power(t1,3)*(-37 + 91*t2 - 15*Power(t2,2) + 
                  4*Power(t2,3) + 
                  Power(s1,2)*(9 + 3*t2 - 8*Power(t2,2)) + 
                  s1*(-39 + 7*t2 + Power(t2,2) + 5*Power(t2,3))))) + 
         Power(s2,6)*(1 - 9*t2 - 11*s*t2 + 17*Power(t2,2) - 
            12*Power(s,2)*Power(t2,2) - 18*Power(t2,3) + s*Power(t2,3) + 
            15*Power(s,2)*Power(t2,3) + 2*Power(s,3)*Power(t2,3) + 
            44*Power(t2,4) + 73*s*Power(t2,4) + 
            24*Power(s,2)*Power(t2,4) - 30*Power(t2,5) - 
            21*s*Power(t2,5) - 5*Power(t2,6) + s*Power(t2,6) + 
            Power(s1,3)*(35*Power(t1,4) - Power(t1,3)*(1 + 90*t2) + 
               Power(t1,2)*(-62 + 47*t2 + 50*Power(t2,2)) + 
               2*(1 - 2*t2 + Power(t2,3)) - 
               t1*(5 - 13*t2 + 23*Power(t2,2) + 6*Power(t2,3))) - 
            Power(t1,4)*(4*Power(s,3) - Power(s,2)*(13 + 22*t2) + 
               s*(17 + 52*t2 + 14*Power(t2,2)) + 
               2*(8 + 75*t2 + 120*Power(t2,2) + 35*Power(t2,3))) + 
            Power(t1,3)*(-2 - 118*t2 - 253*Power(t2,2) + 
               116*Power(t2,3) + 110*Power(t2,4) + 
               Power(s,3)*(-2 + 11*t2) + 
               Power(s,2)*(3 - 58*t2 - 66*Power(t2,2)) - 
               s*(18 + 119*t2 + 128*Power(t2,2) + 59*Power(t2,3))) + 
            Power(t1,2)*(12 + 74*t2 - 152*Power(t2,2) - 
               10*Power(s,3)*Power(t2,2) - 84*Power(t2,3) + 
               49*Power(t2,4) - 53*Power(t2,5) + 
               2*Power(s,2)*t2*(-28 + 16*t2 + 19*Power(t2,2)) + 
               s*(1 - 26*t2 - 297*Power(t2,2) - 52*Power(t2,3) + 
                  87*Power(t2,4))) + 
            t1*(3 + 15*t2 + 13*Power(t2,2) - 52*Power(t2,3) + 
               178*Power(t2,4) - 3*Power(t2,5) + 7*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(4 + 3*t2) - 
               Power(s,2)*t2*
                (6 + 22*t2 + 39*Power(t2,2) + 6*Power(t2,3)) - 
               s*(1 - 27*t2 + 203*Power(t2,2) + 12*Power(t2,3) - 
                  136*Power(t2,4) + 32*Power(t2,5))) + 
            Power(s1,2)*(-3 + 5*(3 + s)*t2 + (-13 + s)*Power(t2,2) - 
               3*(-2 + s)*Power(t2,3) + (-6 + s)*Power(t2,4) + 
               Power(t2,5) + Power(t1,4)*(15 - 60*s + 20*t2) + 
               Power(t1,3)*(1 - 223*t2 - 30*Power(t2,2) + 
                  s*(-57 + 130*t2)) + 
               Power(t1,2)*(-13 - 257*t2 + 60*Power(t2,2) + 
                  35*Power(t2,3) + s*(17 - 28*t2 - 48*Power(t2,2))) + 
               t1*(-54 + 100*t2 + 74*Power(t2,2) + 35*Power(t2,3) - 
                  15*Power(t2,4) + 
                  s*(13 + 4*t2 + 38*Power(t2,2) - 5*Power(t2,3)))) + 
            s1*(-9 + (21 - 10*s)*t2 + 
               (-2 + 27*s + 2*Power(s,2))*Power(t2,2) - 
               (7 + 29*s + 8*Power(s,2))*Power(t2,3) - 
               (24 + 11*s)*Power(t2,4) + (20 - 3*s)*Power(t2,5) + 
               Power(t2,6) + Power(t1,4)*
                (3 + 30*Power(s,2) - 20*t2 - 25*Power(t2,2) - 
                  10*s*(4 + 5*t2)) + 
               Power(t1,3)*(74 + Power(s,2)*(31 - 64*t2) + 335*t2 + 
                  267*Power(t2,2) + 66*Power(t2,3) + 
                  s*(-27 + 219*t2 + 116*Power(t2,2))) + 
               Power(t1,2)*(25 + 31*t2 + 219*Power(t2,2) - 
                  54*Power(t2,3) - 39*Power(t2,4) + 
                  Power(s,2)*(6 + 16*t2 + 35*Power(t2,2)) - 
                  s*(6 - 350*t2 + 11*Power(t2,2) + 79*Power(t2,3))) - 
               t1*(-3 + 100*t2 - 61*Power(t2,2) + 79*Power(t2,3) + 
                  84*Power(t2,4) - 3*Power(t2,5) + 
                  Power(s,2)*t2*(-18 + 9*t2 + 5*Power(t2,2)) + 
                  s*(4 + 38*t2 - 10*Power(t2,2) + 23*Power(t2,3) - 
                     21*Power(t2,4))))) + 
         s2*Power(t1,2)*((6 - 2*s + 2*s1)*Power(t1,7) - 
            3*(-1 + t2)*(-2 + 
               (7 - 8*s + 2*Power(s,2) + 4*Power(s,3))*t2 - 
               (9 - 19*s + 6*Power(s,2) + 2*Power(s,3))*Power(t2,2) + 
               (5 - 14*s + 7*Power(s,2) - 2*Power(s,3))*Power(t2,3) + 
               Power(-1 + s,3)*Power(t2,4)) + 
            Power(t1,6)*(56 - Power(s,3) + Power(s1,3) + 
               Power(s1,2)*(9 - 2*t2) + Power(s,2)*(8 + 3*s1 - 2*t2) + 
               11*t2 - s1*(45 + 22*t2) + 
               s*(27 - 3*Power(s1,2) + 22*t2 + s1*(-17 + 4*t2))) + 
            Power(t1,5)*(83 - 4*t2 - 45*Power(t2,2) + 
               Power(s,3)*(3 + 5*t2) - Power(s1,3)*(8 + 7*t2) + 
               Power(s1,2)*(23 + 7*t2 + 5*Power(t2,2)) + 
               s1*(-69 - 25*t2 + 32*Power(t2,2)) + 
               Power(s,2)*(58 - 4*t2 + 7*Power(t2,2) - 
                  s1*(10 + 17*t2)) + 
               s*(124 + 48*t2 - 45*Power(t2,2) + 
                  Power(s1,2)*(15 + 19*t2) + 
                  s1*(-110 + 3*t2 - 12*Power(t2,2)))) + 
            Power(t1,4)*(1 - 128*t2 - 91*Power(t2,2) + 57*Power(t2,3) + 
               Power(s1,3)*(1 + 14*t2 + 6*Power(t2,2)) - 
               Power(s,3)*(4 + 4*t2 + 9*Power(t2,2)) + 
               s1*(59 + 84*t2 + 72*Power(t2,2) - 19*Power(t2,3)) - 
               2*Power(s1,2)*
                (37 + 28*t2 + 4*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(125 - 33*t2 - 34*Power(t2,2) - 
                  10*Power(t2,3) + s1*(23 + t2 + 27*Power(t2,2))) + 
               s*(170 + 106*t2 - 113*Power(t2,2) + 32*Power(t2,3) - 
                  Power(s1,2)*(24 + 7*t2 + 24*Power(t2,2)) + 
                  s1*(-28 + 93*t2 + 22*Power(t2,2) + 12*Power(t2,3)))) + 
            Power(t1,3)*(Power(s,3)*
                (5 + 29*t2 + 3*Power(t2,2) + 7*Power(t2,3)) - 
               (-1 + t2)*(-20 - 134*t2 - 78*Power(t2,2) + 
                  34*Power(t2,3) + 
                  Power(s1,3)*(-3 + 14*t2 + Power(t2,2)) + 
                  s1*(-33 + 38*t2 + 38*Power(t2,2) - 7*Power(t2,3)) + 
                  Power(s1,2)*
                   (-41 - 30*t2 - 2*Power(t2,2) + Power(t2,3))) + 
               Power(s,2)*(109 + 26*t2 - 68*Power(t2,2) + 
                  48*Power(t2,3) + 8*Power(t2,4) - 
                  s1*(-77 + 29*t2 + 26*Power(t2,2) + 16*Power(t2,3))) + 
               s*(58 - 72*t2 - 263*Power(t2,2) + 28*Power(t2,3) - 
                  9*Power(t2,4) + 
                  Power(s1,2)*
                   (-37 - 27*t2 + 30*Power(t2,2) + 10*Power(t2,3)) + 
                  s1*(103 + 96*t2 - 19*Power(t2,2) - 20*Power(t2,3) - 
                     4*Power(t2,4)))) + 
            t1*(-1 + t2)*(Power(s,3)*
                (-8 - 66*t2 + 21*Power(t2,2) + 10*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (11 + 12*t2 + 4*Power(t2,2) + Power(t2,3) - 
                  s1*(-21 + 16*t2 + Power(t2,2))) - 
               s*(-1 + t2)*(27 + 122*t2 + 44*Power(t2,2) + 
                  5*Power(t2,3) + 2*Power(t2,4) - 
                  s1*(22 - 29*t2 + 41*Power(t2,2) + 2*Power(t2,3))) + 
               Power(s,2)*(-4 - 38*t2 + 6*Power(t2,2) - 2*Power(t2,3) + 
                  Power(t2,4) + Power(t2,5) - 
                  s1*(-2 + 10*t2 - 30*Power(t2,2) + 13*Power(t2,3) + 
                     Power(t2,4)))) + 
            Power(t1,2)*(Power(s,3)*
                (28 + 70*t2 - 45*Power(t2,2) - 9*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (-37 - 67*t2 - 20*Power(t2,2) + 4*Power(t2,3) - 
                  s1*(33 + 21*t2 + Power(t2,2)) + 
                  Power(s1,2)*(-19 + 27*t2 + 2*Power(t2,2))) + 
               s*(-1 + t2)*(2 + 166*t2 + 193*Power(t2,2) + 
                  15*Power(t2,3) + 4*Power(t2,4) - 
                  Power(s1,2)*
                   (14 - 41*t2 + 20*Power(t2,2) + 2*Power(t2,3)) + 
                  s1*(-18 - 27*t2 - 80*Power(t2,2) + 10*Power(t2,3))) + 
               Power(s,2)*(14 + 68*t2 - 87*Power(t2,2) + 
                  37*Power(t2,3) - 18*Power(t2,4) - 4*Power(t2,5) + 
                  s1*(42 - 3*t2 - 85*Power(t2,2) + 47*Power(t2,3) + 
                     4*Power(t2,4))))) + 
         Power(s2,2)*t1*(3*(-1 + t2)*
             (-2 + (7 - 6*s)*t2 + 
               (-9 + 13*s - 4*Power(s,2) + 4*Power(s,3))*Power(t2,2) + 
               (5 - 8*s + 9*Power(s,2) - 4*Power(s,3))*Power(t2,3) + 
               (-1 + s - 5*Power(s,2) + Power(s,3))*Power(t2,4)) + 
            Power(t1,7)*(-32 + Power(s,2) - 11*s1 + Power(s1,2) - 
               18*t2 - 4*s1*t2 + s*(7 - 2*s1 + 4*t2)) + 
            Power(t1,6)*(-142 - 94*t2 + 19*Power(t2,2) + 
               Power(s,3)*(3 + 2*t2) - Power(s1,3)*(7 + 2*t2) + 
               Power(s1,2)*(-53 - t2 + 2*Power(t2,2)) + 
               s1*(132 + 127*t2 + 23*Power(t2,2)) - 
               Power(s,2)*(31 + 8*t2 - 2*Power(t2,2) + 
                  s1*(13 + 6*t2)) + 
               s*(-73 - 102*t2 - 29*Power(t2,2) + 
                  Power(s1,2)*(17 + 6*t2) + 
                  s1*(83 + 9*t2 - 4*Power(t2,2)))) - 
            3*t1*(-1 + t2)*(Power(s,3)*t2*
                (-8 - 23*t2 + 14*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (3 + 8*t2 + 2*Power(t2,2) + Power(t2,3) - 
                  s1*(-11 + 8*t2 + Power(t2,2))) + 
               Power(s,2)*t2*
                (Power(t2,2)*(-22 + 3*t2 + Power(t2,2)) - 
                  s1*(4 - 12*t2 + 3*Power(t2,2) + Power(t2,3))) - 
               s*(-1 + t2)*(5 + 34*t2 + 46*Power(t2,2) + 
                  13*Power(t2,3) + 2*Power(t2,4) - 
                  3*s1*(2 - 5*t2 + 7*Power(t2,2) + 2*Power(t2,3)))) - 
            Power(t1,5)*(101 + 163*t2 - 116*Power(t2,2) - 
               22*Power(t2,3) - 
               Power(s1,3)*(4 + 41*t2 + 4*Power(t2,2)) + 
               Power(s,3)*(5 + 17*t2 + 6*Power(t2,2)) + 
               Power(s1,2)*(91 + 27*t2 + 12*Power(t2,2) + 
                  3*Power(t2,3)) + 
               s1*(-118 - 207*t2 + 79*Power(t2,2) + 36*Power(t2,3)) + 
               Power(s,2)*(143 + 36*t2 - 5*Power(t2,2) + 
                  6*Power(t2,3) - s1*(3 + 66*t2 + 16*Power(t2,2))) + 
               s*(218 + 294*t2 - 99*Power(t2,2) - 56*Power(t2,3) + 
                  Power(s1,2)*(-3 + 90*t2 + 14*Power(t2,2)) - 
                  s1*(354 + 62*t2 - 3*Power(t2,2) + 9*Power(t2,3)))) + 
            Power(t1,4)*(26 + 126*t2 + 364*Power(t2,2) + 
               14*Power(t2,3) - 47*Power(t2,4) - 
               Power(s1,3)*(5 + 25*t2 + 31*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s,3)*(4 - 8*t2 + 33*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s1,2)*(107 + 270*t2 + 36*Power(t2,2) + 
                  7*Power(t2,3)) + 
               3*s1*(-47 - 53*t2 - 89*Power(t2,2) - 15*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s,2)*(-171 - 56*t2 + 87*Power(t2,2) + 
                  29*Power(t2,3) + 6*Power(t2,4) - 
                  s1*(59 - 49*t2 + 94*Power(t2,2) + 14*Power(t2,3))) + 
               s*(-136 - 530*t2 + 168*Power(t2,2) + 19*Power(t2,3) - 
                  40*Power(t2,4) + 
                  Power(s1,2)*
                   (106 - 43*t2 + 89*Power(t2,2) + 10*Power(t2,3)) - 
                  s1*(-119 + 259*t2 + 95*Power(t2,2) + 18*Power(t2,3) + 
                     5*Power(t2,4)))) - 
            Power(t1,3)*(Power(s,3)*
                (-3 + 53*t2 + 5*Power(t2,2) + 27*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s,2)*(31 + 140*t2 - 32*Power(t2,2) - 
                  25*Power(t2,3) + 49*Power(t2,4) + 2*Power(t2,5) + 
                  s1*(101 + 23*t2 - 27*Power(t2,2) - 63*Power(t2,3) - 
                     4*Power(t2,4))) - 
               (-1 + t2)*(4 - 170*t2 - 310*Power(t2,2) - 
                  46*Power(t2,3) + 27*Power(t2,4) + 
                  2*Power(s1,3)*(-7 + 19*t2 + 3*Power(t2,2)) + 
                  s1*(-37 - 48*t2 + 141*Power(t2,2) + 
                     40*Power(t2,3) - 6*Power(t2,4)) + 
                  Power(s1,2)*
                   (-63 - 106*t2 - 13*Power(t2,2) + Power(t2,3) + 
                     Power(t2,4))) + 
               s*(25 + 105*t2 - 570*Power(t2,2) - 208*Power(t2,3) + 
                  11*Power(t2,4) - 8*Power(t2,5) + 
                  Power(s1,2)*
                   (-57 - 89*t2 + 51*Power(t2,2) + 33*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  s1*(61 + 377*t2 - 64*Power(t2,2) + 44*Power(t2,3) - 
                     29*Power(t2,4) + Power(t2,5)))) + 
            Power(t1,2)*(Power(s,3)*t2*
                (-94 - 39*t2 + 57*Power(t2,2) + 8*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-14 - 111*t2 - 103*Power(t2,2) - 15*Power(t2,3) + 
                  3*Power(t2,4) + 
                  Power(s1,2)*(-45 + 56*t2 + 9*Power(t2,2)) + 
                  s1*(-48 - 74*t2 + 5*Power(t2,2) + 6*Power(t2,3) + 
                     Power(t2,4))) - 
               Power(s,2)*(-22 + 32*t2 + 39*Power(t2,2) - 
                  43*Power(t2,3) + 39*Power(t2,4) - 25*Power(t2,5) + 
                  s1*(28 + 85*t2 - 165*Power(t2,2) + 37*Power(t2,3) + 
                     25*Power(t2,4))) + 
               s*(-1 + t2)*(2 - 121*t2 - 447*Power(t2,2) - 
                  178*Power(t2,3) - 17*Power(t2,4) + Power(t2,5) + 
                  Power(s1,2)*
                   (28 - 69*t2 + 24*Power(t2,2) + 7*Power(t2,3)) + 
                  s1*(-6 - 2*t2 + 182*Power(t2,2) + 69*Power(t2,3) - 
                     14*Power(t2,4) + Power(t2,5))))) + 
         Power(s2,3)*((-1 + t2)*
             (2 + (-7 + 6*s)*t2 + 
               3*(3 - 5*s + 2*Power(s,2))*Power(t2,2) - 
               (5 - 14*s + 11*Power(s,2) + 2*Power(s,3))*Power(t2,3) + 
               (1 - 7*s + 3*Power(s,2) + Power(s,3))*Power(t2,4) + 
               2*s*(1 + s)*Power(t2,5)) + 
            Power(t1,7)*(70 + Power(s,3) - Power(s1,3) + 96*t2 + 
               18*Power(t2,2) - Power(s1,2)*(6 + t2) - 
               Power(s,2)*(7 + 3*s1 + t2) + 
               2*s1*(12 + 11*t2 + Power(t2,2)) + 
               s*(3*Power(s1,2) + s1*(13 + 2*t2) - 
                  2*(2 + 7*t2 + Power(t2,2)))) + 
            Power(t1,6)*(185 + 292*t2 - 43*Power(t2,2) - 
               35*Power(t2,3) + 5*Power(s1,3)*(4 + 3*t2) - 
               Power(s,3)*(1 + 11*t2) - 
               8*Power(s1,2)*(-16 - 6*t2 + Power(t2,2)) - 
               s1*(231 + 326*t2 + 121*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s,2)*(42 + 61*t2 + s1*(14 + 37*t2)) + 
               s*(125 + 212*t2 + 125*Power(t2,2) + 12*Power(t2,3) - 
                  Power(s1,2)*(33 + 41*t2) + 
                  s1*(-156 - 111*t2 + 8*Power(t2,2)))) + 
            Power(t1,5)*(19 + 338*t2 - 20*Power(t2,2) - 
               86*Power(t2,3) + 15*Power(t2,4) + 
               Power(s1,3)*(33 - 101*t2 - 23*Power(t2,2)) + 
               Power(s,3)*(7 + 21*t2 + 25*Power(t2,2)) + 
               Power(s1,2)*(177 + 115*t2 - 15*Power(t2,2) + 
                  10*Power(t2,3)) + 
               s1*(-104 - 428*t2 - 21*Power(t2,2) + 135*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s,2)*(155 + 162*t2 - 62*Power(t2,2) + 
                  9*Power(t2,3) + s1*(5 - 94*t2 - 71*Power(t2,2))) + 
               s*(209 + 565*t2 + 39*Power(t2,2) - 197*Power(t2,3) - 
                  21*Power(t2,4) + 
                  Power(s1,2)*(-63 + 166*t2 + 69*Power(t2,2)) - 
                  s1*(530 + 389*t2 - 119*Power(t2,2) + 17*Power(t2,3)))) \
+ Power(t1,4)*(-48 - 71*t2 - 393*Power(t2,2) - 355*Power(t2,3) + 
               53*Power(t2,4) + 9*Power(t2,5) + 
               Power(s1,3)*(11 + 17*t2 + 68*Power(t2,2) + 
                  9*Power(t2,3)) - 
               Power(s,3)*(2 - 16*t2 + 43*Power(t2,2) + 
                  21*Power(t2,3)) + 
               Power(s1,2)*(-20 - 548*t2 - 121*Power(t2,2) - 
                  14*Power(t2,3) + 3*Power(t2,4)) + 
               s1*(175 + 230*t2 + 319*Power(t2,2) + 298*Power(t2,3) - 
                  33*Power(t2,4) - 9*Power(t2,5)) + 
               Power(s,2)*(99 + 148*t2 - 57*Power(t2,2) - 
                  19*Power(t2,3) - 16*Power(t2,4) + 
                  s1*(59 - 101*t2 + 135*Power(t2,2) + 47*Power(t2,3))) \
+ s*(13 + 670*t2 + 188*Power(t2,2) - 234*Power(t2,3) + 
                  104*Power(t2,4) + 14*Power(t2,5) - 
                  Power(s1,2)*
                   (187 - 90*t2 + 134*Power(t2,2) + 34*Power(t2,3)) + 
                  s1*(-177 + 297*t2 + 246*Power(t2,2) - 5*Power(t2,3) + 
                     4*Power(t2,4)))) + 
            t1*(-1 + t2)*(Power(s,3)*Power(t2,2)*
                (-25 - 8*t2 + 8*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (1 + 20*t2 + 4*Power(t2,2) + 3*Power(t2,3) + 
                  s1*(23 - 16*t2 - 3*Power(t2,2))) + 
               Power(s,2)*t2*
                (4 + 40*t2 - 56*Power(t2,2) - 31*Power(t2,3) + 
                  7*Power(t2,4) + 
                  s1*(-6 + 14*t2 + Power(t2,2) - Power(t2,3))) + 
               s*(-1 + t2)*(-5 - 38*t2 - 68*Power(t2,2) - 
                  71*Power(t2,3) - 18*Power(t2,4) + 
                  s1*(6 - 31*t2 + 35*Power(t2,2) + 22*Power(t2,3) + 
                     4*Power(t2,4)))) + 
            Power(t1,2)*(Power(s,3)*t2*
                (2 + 97*t2 - 19*Power(t2,2) - 28*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (12 - 69*t2 - 107*Power(t2,2) - 77*Power(t2,3) + 
                  Power(t2,4) + 
                  Power(s1,2)*(-51 + 56*t2 + 15*Power(t2,2)) + 
                  s1*(-22 - 112*t2 + 5*Power(t2,2) + 12*Power(t2,3) + 
                     7*Power(t2,4))) + 
               s*(-1 + t2)*(4 - 27*t2 + 415*Power(t2,2) + 
                  272*Power(t2,3) + 101*Power(t2,4) - 5*Power(t2,5) - 
                  Power(s1,2)*
                   (22 - 47*t2 + 8*Power(t2,2) + 7*Power(t2,3)) + 
                  s1*(18 + 54*t2 - 130*Power(t2,2) - 171*Power(t2,3) - 
                     2*Power(t2,4) + Power(t2,5))) + 
               Power(s,2)*(-2 - 82*t2 + 95*Power(t2,2) - 
                  87*Power(t2,3) + 127*Power(t2,4) - 29*Power(t2,5) - 
                  2*Power(t2,6) + 
                  s1*(4 + 77*t2 - 71*Power(t2,2) - 27*Power(t2,3) + 
                     25*Power(t2,4) + 2*Power(t2,5)))) + 
            Power(t1,3)*(Power(s,3)*
                (-5 + 13*t2 + 19*Power(t2,2) + 43*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s,2)*(-17 - 4*t2 + 170*Power(t2,2) - 
                  178*Power(t2,3) + 49*Power(t2,4) + 10*Power(t2,5) + 
                  s1*(51 + 73*t2 + 43*Power(t2,2) - 95*Power(t2,3) - 
                     12*Power(t2,4))) + 
               s*(7 + 190*t2 - 452*Power(t2,2) - 573*Power(t2,3) - 
                  16*Power(t2,4) - 13*Power(t2,5) - 3*Power(t2,6) + 
                  Power(s1,2)*
                   (-41 - 132*t2 + 40*Power(t2,2) + 50*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  s1*(-45 + 491*t2 - 99*Power(t2,2) + 207*Power(t2,3) - 
                     36*Power(t2,4) + 2*Power(t2,5))) + 
               (-1 + t2)*(-23 + 54*t2 + 367*Power(t2,2) + 
                  269*Power(t2,3) - 7*Power(t2,5) - 
                  2*Power(s1,3)*(-13 + 26*t2 + 7*Power(t2,2)) + 
                  Power(s1,2)*
                   (40 + 170*t2 + 26*Power(t2,2) + 8*Power(t2,3) - 
                     4*Power(t2,4)) + 
                  s1*(-24 + 210*t2 - 149*Power(t2,2) - 143*Power(t2,3) - 
                     17*Power(t2,4) + 3*Power(t2,5))))) + 
         Power(s2,5)*(3 - 10*t2 + 11*s*t2 + 10*Power(t2,2) - 
            39*s*Power(t2,2) - 2*Power(s,2)*Power(t2,2) - 
            13*Power(t2,3) + 23*s*Power(t2,3) - 
            26*Power(s,2)*Power(t2,3) + 10*Power(t2,4) - 
            48*s*Power(t2,4) + 11*Power(s,2)*Power(t2,4) + 
            2*Power(s,3)*Power(t2,4) + 11*Power(t2,5) + 
            52*s*Power(t2,5) + 19*Power(s,2)*Power(t2,5) - 
            11*Power(t2,6) + s*Power(t2,6) + 
            2*Power(t1,5)*(25 + 3*Power(s,3) + 120*t2 + 
               105*Power(t2,2) + 16*Power(t2,3) - 
               Power(s,2)*(11 + 9*t2) + s*(13 + 14*t2 - 2*Power(t2,2))) \
+ Power(s1,3)*t1*(-11 - 21*Power(t1,4) + 25*t2 - 5*Power(t2,2) - 
               9*Power(t2,3) + 5*Power(t1,3)*(4 + 17*t2) - 
               7*Power(t1,2)*(-14 + 15*t2 + 10*Power(t2,2)) + 
               t1*(11 - 20*t2 + 58*Power(t2,2) + 14*Power(t2,3))) + 
            Power(t1,4)*(40 + Power(s,3)*(6 - 23*t2) + 335*t2 + 
               229*Power(t2,2) - 212*Power(t2,3) - 63*Power(t2,4) + 
               2*Power(s,2)*(-1 + 58*t2 + 33*Power(t2,2)) + 
               s*(76 + 237*t2 + 208*Power(t2,2) + 79*Power(t2,3))) + 
            Power(t1,3)*(-50 - 3*t2 + 349*Power(t2,2) - 
               58*Power(t2,3) + 15*Power(t2,4) + 41*Power(t2,5) + 
               2*Power(s,3)*(1 + t2 + 15*Power(t2,2)) + 
               Power(s,2)*(15 + 162*t2 - 78*Power(t2,2) - 
                  46*Power(t2,3)) + 
               s*(23 + 264*t2 + 469*Power(t2,2) - 112*Power(t2,3) - 
                  119*Power(t2,4))) + 
            Power(t1,2)*(-14 - 57*t2 + 44*Power(t2,2) - 
               215*Power(t2,3) - 247*Power(t2,4) + 15*Power(t2,5) - 
               9*Power(t2,6) + 
               Power(s,3)*t2*(2 - 18*t2 - 13*Power(t2,2)) + 
               Power(s,2)*t2*
                (37 + 33*t2 + 76*Power(t2,2) + 4*Power(t2,3)) + 
               s*(-7 + 33*t2 + 550*Power(t2,2) - 221*Power(t2,3) - 
                  96*Power(t2,4) + 62*Power(t2,5))) + 
            t1*(1 + 23*t2 - 39*Power(t2,2) - 82*Power(t2,3) - 
               7*Power(t2,4) + 97*Power(t2,5) + 7*Power(t2,6) + 
               4*Power(s,3)*Power(t2,2)*(-4 + 3*t2 + Power(t2,2)) + 
               Power(s,2)*t2*
                (-8 + 26*t2 - 66*Power(t2,2) - 61*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s*(5 + 46*t2 + 29*Power(t2,2) - 219*Power(t2,3) - 
                  161*Power(t2,4) + 53*Power(t2,5) - 11*Power(t2,6))) + 
            Power(s1,2)*(5*Power(t1,5)*(-4 + 9*s - 3*t2) + 
               (-1 + t2)*(6 - 11*t2 + 2*Power(t2,2) + 
                  (3 + s)*Power(t2,3)) + 
               5*Power(t1,4)*
                (s*(7 - 33*t2) + 2*(9 + 25*t2 + Power(t2,2))) + 
               Power(t1,3)*(97 + 357*t2 - 105*Power(t2,2) - 
                  20*Power(t2,3) + s*(-63 + 71*t2 + 122*Power(t2,2))) - 
               Power(t1,2)*(-121 + 336*t2 + 180*Power(t2,2) + 
                  46*Power(t2,3) - 21*Power(t2,4) + 
                  s*(73 - 10*t2 + 77*Power(t2,2) + 13*Power(t2,3))) - 
               t1*(-9 + 73*t2 - 60*Power(t2,2) + 13*Power(t2,3) - 
                  21*Power(t2,4) + 4*Power(t2,5) + 
                  s*(2 + 33*t2 + 6*Power(t2,2) - 20*Power(t2,3) + 
                     3*Power(t2,4)))) + 
            s1*(3 - (21 + 13*s)*t2 + (29 + 15*s)*Power(t2,2) + 
               (-7 + 13*s + 8*Power(s,2))*Power(t2,3) + 
               (1 + 3*s - 7*Power(s,2))*Power(t2,4) - 
               (10 + 17*s)*Power(t2,5) - (-5 + s)*Power(t2,6) + 
               Power(t1,5)*(10 - 30*Power(s,2) + 50*t2 + 
                  24*Power(t2,2) + 5*s*(10 + 7*t2)) - 
               Power(t1,4)*(183 + Power(s,2)*(41 - 106*t2) + 502*t2 + 
                  333*Power(t2,2) + 74*Power(t2,3) + 
                  s*(29 + 337*t2 + 89*Power(t2,2))) - 
               Power(t1,3)*(34 + 205*t2 + 328*Power(t2,2) - 
                  122*Power(t2,3) - 67*Power(t2,4) + 
                  Power(s,2)*(15 + 27*t2 + 97*Power(t2,2)) + 
                  s*(120 + 741*t2 - 128*Power(t2,2) - 84*Power(t2,3))) + 
               Power(t1,2)*(Power(s,2)*
                   (2 - 64*t2 + 52*Power(t2,2) + 29*Power(t2,3)) + 
                  s*(-19 + 54*t2 + 119*Power(t2,2) + 23*Power(t2,3) - 
                     36*Power(t2,4)) + 
                  3*(11 + 71*t2 - 18*Power(t2,2) + 93*Power(t2,3) + 
                     46*Power(t2,4) - 7*Power(t2,5))) + 
               t1*(45 - 137*t2 + 69*Power(t2,2) + 59*Power(t2,3) + 
                  35*Power(t2,4) - 72*Power(t2,5) + Power(t2,6) - 
                  Power(s,2)*t2*
                   (4 - 39*t2 + Power(t2,2) + 4*Power(t2,3)) + 
                  s*(-14 + 73*t2 - 78*Power(t2,2) + 137*Power(t2,3) + 
                     32*Power(t2,4) + 6*Power(t2,5))))) + 
         Power(s2,4)*((-1 + t2)*
             (1 - 8*(1 + s)*t2 + 
               (12 - 3*s - 13*Power(s,2))*Power(t2,2) + 
               (-5 + 3*Power(s,2) + 4*Power(s,3))*Power(t2,3) + 
               (1 - 9*s + 16*Power(s,2))*Power(t2,4) + 
               (-1 + 20*s + 3*Power(s,2))*Power(t2,5)) - 
            Power(t1,6)*(80 + 4*Power(s,3) + 210*t2 + 96*Power(t2,2) + 
               6*Power(t2,3) - Power(s,2)*(18 + 7*t2) + 
               s*(14 - 8*t2 - 7*Power(t2,2))) + 
            Power(s1,3)*Power(t1,2)*
             (24 + 7*Power(t1,4) - 62*t2 + 22*Power(t2,2) + 
               16*Power(t2,3) - Power(t1,3)*(29 + 48*t2) + 
               5*Power(t1,2)*(-17 + 27*t2 + 11*Power(t2,2)) - 
               t1*(14 - 7*t2 + 82*Power(t2,2) + 16*Power(t2,3))) + 
            Power(t1,5)*(-127 - 438*t2 - 48*Power(t2,2) + 
               144*Power(t2,3) + 14*Power(t2,4) + 
               Power(s,3)*(-5 + 23*t2) - 
               Power(s,2)*(20 + 123*t2 + 27*Power(t2,2)) - 
               2*s*(66 + 137*t2 + 109*Power(t2,2) + 25*Power(t2,3))) - 
            Power(t1,4)*(-60 + 238*t2 + 255*Power(t2,2) - 
               138*Power(t2,3) + 44*Power(t2,4) + 11*Power(t2,5) + 
               Power(s,3)*(6 + 11*t2 + 40*Power(t2,2)) + 
               Power(s,2)*(78 + 235*t2 - 102*Power(t2,2) - 
                  16*Power(t2,3)) + 
               s*(109 + 549*t2 + 340*Power(t2,2) - 253*Power(t2,3) - 
                  80*Power(t2,4))) + 
            Power(t1,3)*(38 + 61*t2 + 107*Power(t2,2) + 
               510*Power(t2,3) + 103*Power(t2,4) - 17*Power(t2,5) + 
               3*Power(t2,6) + 
               Power(s,3)*t2*(-8 + 33*t2 + 25*Power(t2,2)) + 
               Power(s,2)*(-21 - 108*t2 - 17*Power(t2,2) - 
                  46*Power(t2,3) + 12*Power(t2,4)) - 
               3*s*(-9 + 114*t2 + 201*Power(t2,2) - 127*Power(t2,3) + 
                  20*Power(t2,4) + 16*Power(t2,5))) + 
            t1*(2*Power(s,3)*Power(t2,2)*
                (-1 - 17*t2 + 8*Power(t2,2) + Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-12 + 24*t2 + 26*Power(t2,2) + 69*Power(t2,3) + 
                  13*Power(t2,4)) + 
               Power(s,2)*t2*
                (13 + 35*t2 + 32*Power(t2,2) - 75*Power(t2,3) - 
                  19*Power(t2,4) + 4*Power(t2,5)) + 
               s*(1 - 35*t2 + 164*Power(t2,2) + 39*Power(t2,3) - 
                  58*Power(t2,4) - 108*Power(t2,5) - 3*Power(t2,6))) + 
            Power(t1,2)*(-12 - 22*t2 + 189*Power(t2,2) + 
               140*Power(t2,3) - 199*Power(t2,4) - 101*Power(t2,5) + 
               5*Power(t2,6) + 
               Power(s,3)*t2*
                (10 + 12*t2 - 45*Power(t2,2) - 6*Power(t2,3)) + 
               Power(s,2)*(2 + 72*t2 - 133*Power(t2,2) + 
                  162*Power(t2,3) + 29*Power(t2,4) - 12*Power(t2,5)) + 
               s*(-10 - 127*t2 + 101*Power(t2,2) + 591*Power(t2,3) + 
                  94*Power(t2,4) - 15*Power(t2,5) + 11*Power(t2,6))) + 
            Power(s1,2)*t1*(3*Power(t1,5)*(5 + 2*t2) + 
               Power(t1,4)*(-157 - 159*t2 + 8*Power(t2,2)) - 
               Power(-1 + t2,2)*(-28 + 27*t2 + 11*Power(t2,2)) - 
               5*Power(t1,3)*
                (37 + 55*t2 - 16*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-106 + 581*t2 + 208*Power(t2,2) + 
                  30*Power(t2,3) - 13*Power(t2,4)) + 
               2*t1*(1 + 71*t2 - 62*Power(t2,2) - 13*Power(t2,4) + 
                  3*Power(t2,5)) + 
               s*(-6 - 18*Power(t1,5) + 17*t2 - 10*Power(t2,2) - 
                  2*Power(t2,3) + Power(t2,4) + 
                  3*Power(t1,4)*(5 + 38*t2) + 
                  Power(t1,3)*(97 - 149*t2 - 133*Power(t2,2)) + 
                  Power(t1,2)*
                   (165 - 61*t2 + 117*Power(t2,2) + 39*Power(t2,3)) + 
                  t1*(14 + 95*t2 - 6*Power(t2,2) - 44*Power(t2,3) + 
                     Power(t2,4)))) + 
            s1*(-((-1 + t2)*(6 + 2*(-8 + 3*s)*t2 + 
                    (13 - 11*s)*Power(t2,2) + 
                    (-2 - s + Power(s,2))*Power(t2,3) + 
                    (-1 + 2*s + Power(s,2))*Power(t2,4) + 4*s*Power(t2,5)\
)) + Power(t1,6)*(-25 + 15*Power(s,2) - 48*t2 - 11*Power(t2,2) - 
                  s*(35 + 13*t2)) + 
               Power(t1,5)*(259 + Power(s,2)*(14 - 89*t2) + 498*t2 + 
                  268*Power(t2,2) + 39*Power(t2,3) + 
                  s*(131 + 281*t2 + 22*Power(t2,2))) + 
               Power(t1,4)*(58 + 418*t2 + 248*Power(t2,2) - 
                  187*Power(t2,3) - 47*Power(t2,4) + 
                  Power(s,2)*(7 + 62*t2 + 121*Power(t2,2)) + 
                  s*(393 + 781*t2 - 211*Power(t2,2) - 23*Power(t2,3))) - 
               Power(t1,3)*(116 + 267*t2 + 118*Power(t2,2) + 
                  444*Power(t2,3) + 60*Power(t2,4) - 25*Power(t2,5) + 
                  Power(s,2)*
                   (23 - 104*t2 + 109*Power(t2,2) + 57*Power(t2,3)) - 
                  5*s*(22 - 31*t2 - 56*Power(t2,2) + Power(t2,3) + 
                     4*Power(t2,4))) + 
               Power(t1,2)*(-71 + 292*t2 - 258*Power(t2,2) - 
                  98*Power(t2,3) + 62*Power(t2,4) + 78*Power(t2,5) - 
                  5*Power(t2,6) + 
                  Power(s,2)*
                   (-7 - 31*t2 - 91*Power(t2,2) + 57*Power(t2,3) + 
                     12*Power(t2,4)) - 
                  2*s*(-28 + 140*t2 - 52*Power(t2,2) + 128*Power(t2,3) + 
                     5*Power(t2,4) + 2*Power(t2,5))) + 
               t1*(Power(s,2)*t2*
                   (-13 - 9*t2 + 16*Power(t2,2) + 3*Power(t2,3) - 
                     2*Power(t2,4)) - 
                  Power(-1 + t2,2)*
                   (3 - 71*t2 - 4*Power(t2,2) + 6*Power(t2,3) + 
                     11*Power(t2,4)) + 
                  s*(7 + 39*t2 - 73*Power(t2,2) - 81*Power(t2,3) + 
                     74*Power(t2,4) + 35*Power(t2,5) - Power(t2,6))))))*
       T3q(s2,t1))/
     (Power(s2 - t1,2)*(-s + s2 - t1)*(-1 + t1)*(s - s1 + t2)*
       Power(-1 + s2 - t1 + t2,2)*Power(-t1 + s2*t2,3)) + 
    (8*(-24*Power(t1,7) - Power(t1,6)*
          (13 + 6*Power(s,2) + s*(43 - 8*s1) + 27*s1 + 2*Power(s1,2) - 
            72*t2) + Power(1 + (-1 + s)*t2,3)*(2 - 3*t2 + Power(t2,2)) + 
         Power(s2,10)*(Power(s1,3) + s1*Power(t2,2) - 2*Power(t2,3)) - 
         Power(s2,9)*(Power(s1,3)*(4 + 4*t1 - 3*t2) + 
            s1*t2*(-2*(-7 + t2)*t2 + t1*(2 + t2) + s*(3 - 3*t1 + t2)) + 
            Power(s1,2)*(3 + (-4 + t1)*t2 - 2*Power(t2,2) - 
               s*(3*t1 + t2)) + 
            Power(t2,2)*(1 + s*(3 - 4*t1 - 2*t2) - 12*t2 + 
               5*Power(t2,2) - 2*t1*(3 + 5*t2))) - 
         Power(t1,5)*(-23 + Power(s,3) - 2*Power(s1,3) + 
            s1*(42 - 34*t2) + Power(s1,2)*(4 - 6*t2) - 
            17*Power(s,2)*(-1 + t2) - 47*t2 + 76*Power(t2,2) + 
            s*(40 + Power(s1,2) - 112*t2 + s1*(49 + 11*t2))) - 
         t1*(-1 + t2)*(1 + (-1 + s)*t2)*
          (Power(s,2)*t2*(-1 + 2*t2) - 
            (-1 + t2)*(4 + s1*(5 - 4*t2) + 2*t2 + Power(t2,2)) + 
            s*(5 + 10*t2 - 3*Power(t2,2) + Power(t2,3) + 
               s1*(-6 + 8*t2 - 4*Power(t2,2)))) + 
         Power(t1,4)*(-3 + Power(s1,3)*(8 - 5*t2) + 10*t2 - 
            58*Power(t2,2) + 32*Power(t2,3) + 2*Power(s,3)*(1 + t2) + 
            2*s1*(9 + 16*t2 + 3*Power(t2,2)) - 
            Power(s1,2)*(17 + 3*t2 + 4*Power(t2,2)) + 
            s*(22 + 67*t2 - 96*Power(t2,2) + 
               2*Power(s1,2)*(-9 + 7*t2) + 
               s1*(-83 + 92*t2 - 3*Power(t2,2))) + 
            Power(s,2)*(s1*(-16 + t2) - 2*(7 - 18*t2 + 7*Power(t2,2)))) + 
         Power(t1,2)*(Power(s,3)*t2*(-2 + 5*t2 + Power(t2,2)) + 
            Power(-1 + t2,2)*
             (6 + Power(s1,2)*(3 - 5*t2) - 3*t2 - Power(t2,2) + 
               s1*(13 + 7*t2 + Power(t2,2))) + 
            Power(s,2)*(2 - 5*t2 + 4*Power(t2,2) - Power(t2,3) + 
               4*Power(t2,4) + 
               s1*(-4 + 10*t2 + 2*Power(t2,2) - 7*Power(t2,3))) - 
            s*(-1 + t2)*(1 + 33*t2 + Power(t2,2) + 5*Power(t2,3) + 
               Power(s1,2)*(-4 + 8*t2 - 5*Power(t2,2)) + 
               s1*(-3 + 12*t2 - 7*Power(t2,2) + Power(t2,3)))) + 
         Power(t1,3)*(-(Power(s,3)*(-1 + 7*t2 + Power(t2,2))) + 
            Power(s,2)*(11 + 16*t2 - 21*Power(t2,2) + 
               2*s1*(-13 + 13*t2 + Power(t2,2))) - 
            (-1 + t2)*(5 - 2*Power(s1,3)*(-2 + t2) - 2*t2 - 
               20*Power(s1,2)*t2 - 22*Power(t2,2) + 4*Power(t2,3) + 
               s1*(17 + 33*t2 + 14*Power(t2,2))) + 
            s*(-9 + 19*t2 - 29*Power(t2,2) + 30*Power(t2,3) + 
               Power(s1,2)*(-27 + 41*t2 - 18*Power(t2,2)) + 
               s1*(1 + 51*t2 - 43*Power(t2,2) + 7*Power(t2,3)))) + 
         Power(s2,8)*(Power(s1,3)*
             (3 + 6*Power(t1,2) + t1*(14 - 11*t2) - 8*t2 + 
               3*Power(t2,2)) + 
            Power(s1,2)*(12 - (27 + 8*s)*t2 + 3*(-2 + s)*Power(t2,2) + 
               5*Power(t2,3) + Power(t1,2)*(1 - 9*s + 3*t2) + 
               t1*(8 - 18*t2 - 6*Power(t2,2) + s*(-16 + 9*t2))) - 
            t2*(2*Power(t1,2)*(3 + 15*t2 + 7*Power(t2,2)) + 
               t1*(-2 + 35*t2 + 44*Power(t2,2) - 19*Power(t2,3)) + 
               2*t2*(-7 + 20*t2 - 18*Power(t2,2) + 2*Power(t2,3)) - 
               Power(s,2)*(3*Power(t1,2) + t2 - t1*(3 + 5*t2)) + 
               s*(-3 - 11*t2 + 19*Power(t2,2) - 5*Power(t2,3) + 
                  Power(t1,2)*(8 + 5*t2) + t1*(-3 + 26*t2 + Power(t2,2))\
)) + s1*(3 + Power(s,2)*t1*(3*t1 - t2) - 8*t2 + 56*Power(t2,2) - 
               27*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,2)*(1 + 2*t2 - 3*Power(t2,2)) + 
               t1*t2*(30 + 28*t2 + 3*Power(t2,2)) + 
               s*(-(Power(t1,2)*(3 + 8*t2)) + 
                  t2*(13 + 4*t2 - 3*Power(t2,2)) + 
                  t1*(-3 + t2 + 15*Power(t2,2))))) - 
         Power(s2,7)*(1 - 4*t2 + 14*s*t2 + 58*Power(t2,2) - 
            4*s*Power(t2,2) - 3*Power(s,2)*Power(t2,2) - 
            111*Power(t2,3) - 84*s*Power(t2,3) - 
            5*Power(s,2)*Power(t2,3) + 96*Power(t2,4) + 
            35*s*Power(t2,4) - 44*Power(t2,5) - 4*s*Power(t2,5) + 
            Power(t2,6) + Power(t1,2)*
             (1 + (-34 - 54*s + Power(s,2) + 2*Power(s,3))*t2 - 
               (165 + 32*s + 15*Power(s,2))*Power(t2,2) - 
               3*(5 + 4*s)*Power(t2,3) + 14*Power(t2,4)) - 
            Power(t1,3)*(2 + Power(s,3) + 30*t2 + 42*Power(t2,2) + 
               6*Power(t2,3) - Power(s,2)*(3 + 4*t2) + 
               s*(4 + 10*t2 - Power(t2,2))) + 
            Power(s1,3)*(-6 + 4*Power(t1,3) + 
               Power(t1,2)*(17 - 15*t2) - 3*t2 + 5*Power(t2,2) - 
               Power(t2,3) + t1*(6 - 25*t2 + 10*Power(t2,2))) - 
            t1*t2*(-29 + 86*t2 + Power(s,3)*t2 + 50*Power(t2,2) - 
               108*Power(t2,3) + 9*Power(t2,4) + 
               Power(s,2)*(17 + 11*t2 - 11*Power(t2,2)) - 
               s*(21 - 97*t2 + 5*Power(t2,2) + 15*Power(t2,3))) + 
            Power(s1,2)*(9 - (45 + 17*s)*t2 + (5 + 18*s)*Power(t2,2) + 
               (25 - 3*s)*Power(t2,3) - 4*Power(t2,4) + 
               Power(t1,3)*(3 - 9*s + 3*t2) + 
               Power(t1,2)*(2 - 32*t2 - 4*Power(t2,2) + 
                  3*s*(-13 + 9*t2)) + 
               t1*(24 - 112*t2 - 5*Power(t2,2) + 9*Power(t2,3) + 
                  s*(-29 + 26*t2 - 10*Power(t2,2)))) + 
            s1*(12 + 5*(-9 + s)*t2 + 
               (111 + 21*s + 2*Power(s,2))*Power(t2,2) - 
               (71 + 17*s)*Power(t2,3) + (15 + 4*s)*Power(t2,4) - 
               2*Power(t2,5) + 
               Power(t1,3)*(1 + 6*Power(s,2) - 6*t2 - 5*Power(t2,2) - 
                  s*(8 + 7*t2)) + 
               Power(t1,2)*(16 + Power(s,2)*(17 - 14*t2) + 68*t2 + 
                  28*Power(t2,2) + 13*Power(t2,3) + 
                  2*s*(-9 + 9*t2 + 11*Power(t2,2))) + 
               t1*(4 + 84*t2 + 79*Power(t2,2) - 3*Power(t2,3) - 
                  7*Power(t2,4) + Power(s,2)*t2*(3 + 4*t2) + 
                  s*(-17 + 82*t2 + 53*Power(t2,2) - 24*Power(t2,3))))) + 
         Power(s2,6)*(4 + 15*Power(t1,2) - 11*Power(t1,3) - 
            10*Power(t1,4) - 21*t2 + 102*t1*t2 - 49*Power(t1,2)*t2 - 
            178*Power(t1,3)*t2 - 42*Power(t1,4)*t2 + 117*Power(t2,2) - 
            167*t1*Power(t2,2) - 347*Power(t1,2)*Power(t2,2) - 
            164*Power(t1,3)*Power(t2,2) - 18*Power(t1,4)*Power(t2,2) - 
            165*Power(t2,3) - 25*t1*Power(t2,3) + 
            197*Power(t1,2)*Power(t2,3) + 17*Power(t1,3)*Power(t2,3) + 
            204*Power(t2,4) + 110*t1*Power(t2,4) + 
            45*Power(t1,2)*Power(t2,4) - 74*Power(t2,5) - 
            68*t1*Power(t2,5) + 24*Power(t2,6) + 
            Power(s,3)*(-Power(t1,4) + 5*Power(t1,3)*(-1 + t2) + 
               Power(t1,2)*(6 - 7*t2)*t2 - 2*Power(t2,3) + 
               t1*Power(t2,2)*(1 + 3*t2)) + 
            Power(s,2)*(Power(t1,4)*(4 + t2) + 
               Power(t1,3)*(12 - 13*t2 - 6*Power(t2,2)) + 
               3*Power(t2,2)*(-9 + 2*Power(t2,2)) + 
               Power(t1,2)*t2*(-47 - 28*t2 + 9*Power(t2,2)) + 
               t1*t2*(-39 + 5*t2 + 31*Power(t2,2) - 4*Power(t2,3))) + 
            Power(s1,3)*(-9 + Power(t1,4) + Power(t1,3)*(7 - 9*t2) + 
               5*t2 - 3*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,2)*(-2 - 25*t2 + 11*Power(t2,2)) - 
               t1*(28 + 5*t2 - 19*Power(t2,2) + 3*Power(t2,3))) + 
            s*(Power(t1,4)*(-5 + 2*t2 + 2*Power(t2,2)) - 
               Power(t1,3)*(30 + 68*t2 + 29*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(1 - 149*t2 - 102*Power(t2,2) - 
                  17*Power(t2,3) + 19*Power(t2,4)) + 
               t1*(-1 + 57*t2 - 333*Power(t2,2) + 16*Power(t2,3) + 
                  77*Power(t2,4) - 10*Power(t2,5)) + 
               t2*(13 - 48*t2 - 119*Power(t2,2) + 170*Power(t2,3) - 
                  33*Power(t2,4) + Power(t2,5))) + 
            Power(s1,2)*(-18 + (2 - 8*s)*t2 + (18 + 37*s)*Power(t2,2) + 
               (81 - 10*s)*Power(t2,3) + (-18 + s)*Power(t2,4) + 
               Power(t2,5) + Power(t1,4)*(3 - 3*s + t2) + 
               Power(t1,2)*(-22 - 170*t2 + 15*Power(t2,2) + 
                  Power(t2,3) + s*(-49 + 71*t2 - 27*Power(t2,2))) + 
               Power(t1,3)*(s*(-27 + 23*t2) + 
                  2*(-7 - 12*t2 + Power(t2,2))) + 
               t1*(-3 - 154*t2 + 62*Power(t2,2) + 42*Power(t2,3) - 
                  5*Power(t2,4) + 
                  s*(-8 + 25*t2 - 19*Power(t2,2) + 6*Power(t2,3)))) + 
            s1*(9 - (66 + 43*s)*t2 + 
               4*(30 + 19*s + 2*Power(s,2))*Power(t2,2) - 
               2*(66 + 51*s + 2*Power(s,2))*Power(t2,3) + 
               (-53 + 11*s)*Power(t2,4) + (2 - 3*s)*Power(t2,5) + 
               Power(t2,6) + Power(t1,4)*
                (-3 + 3*Power(s,2) - 10*t2 - 2*Power(t2,2) - 
                  s*(7 + 2*t2)) + 
               Power(t1,3)*(38 + Power(s,2)*(25 - 19*t2) + 53*t2 + 
                  31*Power(t2,2) + 8*Power(t2,3) + 
                  s*(-15 + 39*t2 + 4*Power(t2,2))) + 
               Power(t1,2)*(46 + 199*t2 + 126*Power(t2,2) + 
                  40*Power(t2,3) - 10*Power(t2,4) + 
                  Power(s,2)*(39 - 44*t2 + 23*Power(t2,2)) + 
                  s*(-18 + 257*t2 + 16*Power(t2,2) - 12*Power(t2,3))) + 
               t1*(6 + 55*t2 + 113*Power(t2,2) + 26*Power(t2,3) - 
                  76*Power(t2,4) + 3*Power(t2,5) + 
                  Power(s,2)*t2*(33 - 3*t2 - 7*Power(t2,2)) + 
                  s*(-37 + 220*t2 + 28*Power(t2,2) - 91*Power(t2,3) + 
                     13*Power(t2,4))))) + 
         s2*(Power(t1,6)*(99 + 73*s + 6*Power(s,2) + 96*t2) + 
            (-1 + t2)*(5 + 13*(-1 + 2*s)*t2 + 
               (15 - 42*s + 31*Power(s,2))*Power(t2,2) + 
               2*(-5 + 12*s - 13*Power(s,2) + 5*Power(s,3))*
                Power(t2,3) + 
               (2 - 6*s + 7*Power(s,2) - 3*Power(s,3))*Power(t2,4) + 
               Power(-1 + s,2)*Power(t2,5)) + 
            Power(t1,5)*(50 + 2*Power(s,3) + s*(194 - 58*t2) - 198*t2 - 
               280*Power(t2,2) + Power(s,2)*(53 + t2)) + 
            Power(s1,3)*Power(t1,2)*
             (-3 + 7*Power(t1,3) - t2 + 5*Power(t2,2) - Power(t2,3) - 
               8*Power(t1,2)*(1 + t2) + t1*(-19 + t2 + 6*Power(t2,2))) - 
            Power(t1,4)*(17 + 261*t2 - 55*Power(t2,2) - 
               284*Power(t2,3) + Power(s,3)*(6 + t2) + 
               Power(s,2)*(-87 + 73*t2 + 41*Power(t2,2)) + 
               s*(-2 + 201*t2 + 185*Power(t2,2))) - 
            Power(t1,3)*(31 - 13*t2 - 103*Power(t2,2) - 
               103*Power(t2,3) + 112*Power(t2,4) + 
               Power(s,3)*(3 - 23*t2 + 6*Power(t2,2)) + 
               s*(9 + 118*t2 + 78*Power(t2,2) - 246*Power(t2,3)) + 
               Power(s,2)*(16 + 93*t2 + 19*Power(t2,2) - 56*Power(t2,3))\
) + Power(t1,2)*(-15 + 28*t2 - 9*Power(t2,2) + 51*Power(t2,3) - 
               67*Power(t2,4) + 12*Power(t2,5) + 
               Power(s,3)*t2*(8 - 15*t2 + 4*Power(t2,2)) + 
               s*(-8 - 35*t2 + 10*Power(t2,2) + 72*Power(t2,3) - 
                  72*Power(t2,4)) - 
               Power(s,2)*(8 + 33*t2 + 5*Power(t2,2) - 51*Power(t2,3) + 
                  22*Power(t2,4))) + 
            t1*(Power(s,3)*Power(t2,2)*(5 - 15*t2 + 4*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-9 - 6*t2 + 4*Power(t2,2) + 7*Power(t2,3)) - 
               Power(s,2)*t2*
                (25 + 8*t2 - 44*Power(t2,2) + 18*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(-16 - 10*t2 + Power(t2,2) + 10*Power(t2,3) + 
                  17*Power(t2,4) - 2*Power(t2,5))) + 
            Power(s1,2)*t1*(14*Power(t1,5) - Power(t1,4)*(53 + 22*t2) + 
               Power(t1,3)*(-72 + 104*t2 - 7*Power(t2,2)) + 
               2*Power(-1 + t2,2)*(-5 + 6*t2 + Power(t2,2)) + 
               Power(t1,2)*(-21 + 143*t2 - 42*Power(t2,2) + 
                  16*Power(t2,3)) - 
               t1*(20 - 50*t2 - 19*Power(t2,2) + 48*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(6 - 4*Power(t1,4) + Power(t1,3)*(-12 + t2) - 17*t2 + 
                  16*Power(t2,2) - 3*Power(t2,3) - 2*Power(t2,4) + 
                  Power(t1,2)*(17 + 34*t2 - 20*Power(t2,2)) + 
                  t1*(4 + 52*t2 - 69*Power(t2,2) + 25*Power(t2,3)))) + 
            s1*(-5*(15 + 4*s)*Power(t1,6) - 
               (-1 + t2)*(-6 + (16 - 6*s)*t2 + 
                  (-13 + 11*s)*Power(t2,2) + 
                  (2 - 3*s + Power(s,2))*Power(t2,3) + 
                  Power(-1 + s,2)*Power(t2,4)) + 
               Power(t1,5)*(38 - 5*Power(s,2) + 298*t2 + 
                  s*(10 + 21*t2)) + 
               Power(t1,4)*(95 + 149*t2 - 300*Power(t2,2) + 
                  Power(s,2)*(38 + 8*t2) + 
                  11*s*(18 + 10*t2 + Power(t2,2))) - 
               Power(t1,3)*(10 + 146*t2 + 111*Power(t2,2) - 
                  43*Power(t2,3) + 
                  Power(s,2)*(-95 + 44*t2 + 11*Power(t2,2)) + 
                  s*(-50 - 98*t2 + 244*Power(t2,2) + 5*Power(t2,3))) + 
               Power(t1,2)*(-49 + 26*t2 - 63*Power(t2,2) + 
                  52*Power(t2,3) + 34*Power(t2,4) + 
                  Power(s,2)*
                   (19 + t2 - 50*Power(t2,2) + 17*Power(t2,3)) - 
                  s*(5 + 44*t2 + 95*Power(t2,2) - 103*Power(t2,3) + 
                     7*Power(t2,4))) + 
               t1*(-(Power(-1 + t2,2)*(21 - t2 + 22*Power(t2,2))) + 
                  Power(s,2)*t2*
                   (31 - 81*t2 + 56*Power(t2,2) - 8*Power(t2,3)) + 
                  s*(11 - 75*t2 + 130*Power(t2,2) - 85*Power(t2,3) + 
                     19*Power(t2,4))))) + 
         Power(s2,5)*(-3 + 29*t2 + 26*s*t2 - 118*Power(t2,2) + 
            16*s*Power(t2,2) + 51*Power(s,2)*Power(t2,2) + 
            128*Power(t2,3) + 68*s*Power(t2,3) - 
            40*Power(s,2)*Power(t2,3) + 6*Power(s,3)*Power(t2,3) - 
            157*Power(t2,4) - 232*s*Power(t2,4) + 
            9*Power(s,2)*Power(t2,4) - 2*Power(s,3)*Power(t2,4) + 
            151*Power(t2,5) + 165*s*Power(t2,5) + 
            7*Power(s,2)*Power(t2,5) + 3*Power(t2,6) - 
            14*s*Power(t2,6) + 4*Power(t2,7) - 
            Power(t1,5)*(s + Power(s,2) + 4*s*t2 - 2*(7 + 9*t2)) + 
            Power(t1,4)*(62 + Power(s,3)*(3 - 2*t2) + 207*t2 + 
               35*Power(t2,2) + 
               Power(s,2)*(-5 + 11*t2 - 2*Power(t2,2)) + 
               s*(37 + 24*t2 + 23*Power(t2,2))) + 
            Power(t1,3)*(3 + 397*t2 + 105*Power(t2,2) - 
               149*Power(t2,3) + 
               Power(s,3)*(11 - 13*t2 + 6*Power(t2,2)) + 
               s*(80 + 204*t2 + 147*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s,2)*(-3 + 105*t2 - 8*Power(t2,2) + 6*Power(t2,3))) \
+ Power(s1,3)*(Power(t1,4)*(1 + 2*t2) + 
               Power(t1,3)*(11 + 7*t2 - 4*Power(t2,2)) + 
               t2*(2 + 9*t2 - 17*Power(t2,2) - 2*Power(t2,3)) + 
               2*Power(t1,2)*
                (25 - 7*t2 - 10*Power(t2,2) + Power(t2,3)) + 
               t1*(35 - 12*t2 + 4*Power(t2,2) + 20*Power(t2,3))) - 
            Power(t1,2)*(50 - 21*t2 - 500*Power(t2,2) + 
               350*Power(t2,3) - 136*Power(t2,4) - 4*Power(t2,5) + 
               Power(s,3)*t2*(4 - 15*t2 + 6*Power(t2,2)) + 
               Power(s,2)*t2*
                (-124 + 33*t2 + 6*Power(t2,2) + 6*Power(t2,3)) + 
               s*(13 - 299*t2 - 369*Power(t2,2) + 84*Power(t2,3) + 
                  59*Power(t2,4))) + 
            t1*(4 - 156*t2 + 149*Power(t2,2) - 291*Power(t2,3) - 
               203*Power(t2,4) - 43*Power(t2,5) - 8*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(-13 - 3*t2 + 2*Power(t2,2)) + 
               Power(s,2)*t2*
                (37 - 41*t2 - 76*Power(t2,2) - 3*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s*(8 - 62*t2 + 532*Power(t2,2) - 440*Power(t2,3) - 
                  251*Power(t2,4) + 58*Power(t2,5))) - 
            Power(s1,2)*(-27 + Power(t1,5) + (49 + 13*s)*t2 + 
               (7 + 33*s)*Power(t2,2) + (62 - 53*s)*Power(t2,3) - 
               (127 + 2*s)*Power(t2,4) + 3*Power(t2,5) + 
               Power(t1,4)*(-17 - 4*t2 + 2*Power(t2,2) + 
                  s*(-1 + 6*t2)) - 
               Power(t1,3)*(94 + 102*t2 - 15*Power(t2,2) + 
                  3*Power(t2,3) + s*(9 - 36*t2 + 14*Power(t2,2))) + 
               Power(t1,2)*(-81 - 180*t2 + 123*Power(t2,2) - 
                  8*Power(t2,3) + 
                  s*(31 + 35*t2 - 58*Power(t2,2) + 10*Power(t2,3))) + 
               t1*(-73 + 43*t2 + 225*Power(t2,2) + 153*Power(t2,3) - 
                  2*Power(t2,4) + Power(t2,5) + 
                  s*(37 + 27*t2 + 12*Power(t2,2) + 35*Power(t2,3) - 
                     2*Power(t2,4)))) + 
            s1*(18 + (-13 + 61*s)*t2 - 
               4*(9 + 28*s + 3*Power(s,2))*Power(t2,2) + 
               (93 + 161*s + 4*Power(s,2))*Power(t2,3) - 
               (10 + 142*s + 5*Power(s,2))*Power(t2,4) - 
               6*(24 + s)*Power(t2,5) - (-4 + s)*Power(t2,6) + 
               Power(t1,5)*(5 + 2*s + 4*t2) + 
               Power(t1,4)*(-28 - 25*t2 - 17*Power(t2,2) + 
                  Power(s,2)*(-5 + 6*t2) + 
                  s*(-11 - 15*t2 + 4*Power(t2,2))) - 
               Power(t1,3)*(105 + 237*t2 + 200*Power(t2,2) + 
                  12*Power(t2,3) + 
                  2*Power(s,2)*(18 - 21*t2 + 8*Power(t2,2)) + 
                  s*(93 + 236*t2 - 33*Power(t2,2) + 9*Power(t2,3))) + 
               Power(t1,2)*(-28 - 194*t2 - 315*Power(t2,2) + 
                  82*Power(t2,3) + 72*Power(t2,4) + 
                  Power(s,2)*
                   (-43 + 38*t2 - 50*Power(t2,2) + 14*Power(t2,3)) + 
                  s*(-55 - 541*t2 + 185*Power(t2,2) - 16*Power(t2,3) + 
                     5*Power(t2,4))) + 
               t1*(24 - 5*t2 + 95*Power(t2,2) + 292*Power(t2,3) + 
                  349*Power(t2,4) - 51*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-75 + 66*t2 + 18*Power(t2,2) - 4*Power(t2,3)) + 
                  s*(25 - 180*t2 + 207*Power(t2,2) + 311*Power(t2,3) + 
                     2*Power(t2,4) + Power(t2,5))))) + 
         Power(s2,2)*(-((47 + 19*s)*Power(t1,6)) - 
            Power(t1,5)*(Power(s,2)*(58 + 15*t2) + s*(219 + 188*t2) + 
               4*(54 + 53*t2 + 37*Power(t2,2))) + 
            Power(t1,4)*(-61 + Power(s,3)*(6 - 7*t2) + 236*t2 + 
               620*Power(t2,2) + 416*Power(t2,3) + 
               Power(s,2)*(-172 + 11*t2 + 29*Power(t2,2)) + 
               s*(-162 - 233*t2 + 448*Power(t2,2))) + 
            Power(s1,3)*t1*(5 + 3*Power(t1,4) - 13*t2 + 
               17*Power(t2,2) - 9*Power(t2,3) - 
               2*Power(t1,3)*(6 + 13*t2) + 
               Power(t1,2)*(3 + 19*t2 + 22*Power(t2,2)) + 
               t1*(7 + 26*t2 - 10*Power(t2,2) - 5*Power(t2,3))) + 
            Power(t1,3)*(81 + 86*t2 + 456*Power(t2,2) - 
               407*Power(t2,3) - 400*Power(t2,4) + 
               Power(s,3)*(1 - 29*t2 + 18*Power(t2,2)) + 
               s*(27 + 70*t2 + 756*Power(t2,2) - 162*Power(t2,3)) + 
               3*Power(s,2)*(-11 + 31*t2 + 47*Power(t2,2) + 
                  2*Power(t2,3))) + 
            t2*(Power(s,3)*Power(t2,2)*(18 - 19*t2 + 3*Power(t2,2)) - 
               2*Power(-1 + t2,2)*(-5 + t2 + 3*Power(t2,3)) + 
               Power(s,2)*t2*
                (59 - 71*t2 + 12*Power(t2,2) + 7*Power(t2,3) - 
                  3*Power(t2,4)) + 
               s*(31 - 39*t2 + 21*Power(t2,2) - 2*Power(t2,3) - 
                  18*Power(t2,4) + 7*Power(t2,5))) + 
            Power(t1,2)*(Power(s,3)*t2*(-14 + 21*t2 - 12*Power(t2,2)) + 
               s*(28 - 63*t2 + 299*Power(t2,2) - 179*Power(t2,3) - 
                  166*Power(t2,4)) + 
               Power(s,2)*(12 + 122*t2 + 90*Power(t2,2) - 
                  76*Power(t2,3) - 42*Power(t2,4)) + 
               2*(10 + 15*t2 + 5*Power(t2,2) - 161*Power(t2,3) + 
                  2*Power(t2,4) + 72*Power(t2,5))) + 
            t1*(-1 + 28*t2 - 51*Power(t2,2) + Power(t2,3) - 
               13*Power(t2,4) + 48*Power(t2,5) - 12*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(-5 + 21*t2 - 2*Power(t2,2)) + 
               Power(s,2)*t2*
                (59 - 20*t2 - 23*Power(t2,2) - 25*Power(t2,3) + 
                  25*Power(t2,4)) + 
               s*(13 - 7*t2 + 106*Power(t2,2) - 52*Power(t2,3) - 
                  107*Power(t2,4) + 80*Power(t2,5))) + 
            Power(s1,2)*(-2*Power(t1,5)*(16 + 3*s + 21*t2) + 
               (-1 + t2)*(-6 + 11*t2 - 2*Power(t2,2) + 
                  (-3 + s)*Power(t2,3)) + 
               Power(t1,4)*(96 + 207*t2 + 83*Power(t2,2) + 
                  s*(40 + 21*t2)) + 
               Power(t1,3)*(73 + 137*t2 - 303*Power(t2,2) - 
                  27*Power(t2,3) + s*(89 - 51*t2 - 8*Power(t2,2))) + 
               Power(t1,2)*(29 + 18*t2 - 315*Power(t2,2) + 
                  139*Power(t2,3) - 15*Power(t2,4) + 
                  s*(46 - 124*t2 - 10*Power(t2,2) + 19*Power(t2,3))) + 
               t1*(30 - 68*t2 + 45*Power(t2,2) - 65*Power(t2,3) + 
                  57*Power(t2,4) + Power(t2,5) + 
                  s*(-16 + 69*t2 - 159*Power(t2,2) + 120*Power(t2,3) - 
                     26*Power(t2,4)))) + 
            s1*(15 + 33*Power(t1,6) + (-45 + 31*s)*t2 + 
               (58 - 66*s)*Power(t2,2) + 
               (-32 + 35*s + 5*Power(s,2))*Power(t2,3) + 
               (-6 + 8*s - 7*Power(s,2))*Power(t2,4) + 
               (11 - 9*s + 3*Power(s,2))*Power(t2,5) + 
               (-1 + s)*Power(t2,6) + 
               Power(t1,5)*(183 + 3*Power(s,2) + 164*t2 + 
                  s*(104 + 57*t2)) + 
               Power(t1,4)*(-78 - 388*t2 - 727*Power(t2,2) + 
                  2*Power(s,2)*(-17 + 6*t2) + 
                  s*(50 - 259*t2 - 115*Power(t2,2))) + 
               Power(t1,3)*(-97 - 166*t2 - 156*Power(t2,2) + 
                  723*Power(t2,3) + 
                  Power(s,2)*(-132 + 39*t2 - 35*Power(t2,2)) + 
                  s*(14 - 743*t2 + 105*Power(t2,2) + 66*Power(t2,3))) + 
               Power(t1,2)*(45 - 39*t2 + 342*Power(t2,2) + 
                  144*Power(t2,3) - 156*Power(t2,4) + 
                  Power(s,2)*
                   (-35 - 53*t2 + 47*Power(t2,2) + 25*Power(t2,3)) + 
                  s*(64 - 240*t2 + 274*Power(t2,2) + 183*Power(t2,3) - 
                     14*Power(t2,4))) + 
               t1*(15 + 59*t2 - 66*Power(t2,2) + 51*Power(t2,3) - 
                  23*Power(t2,4) - 36*Power(t2,5) - 
                  Power(s,2)*t2*
                   (53 - 126*t2 + 45*Power(t2,2) + 8*Power(t2,3)) + 
                  s*(17 + 32*t2 - 33*Power(t2,2) + 151*Power(t2,3) - 
                     124*Power(t2,4) + 5*Power(t2,5))))) + 
         Power(s2,3)*(9 - (-27 + s)*Power(t1,6) - 39*t2 + 14*s*t2 + 
            57*Power(t2,2) - 81*s*Power(t2,2) - 
            39*Power(s,2)*Power(t2,2) - 42*Power(t2,3) + 
            64*s*Power(t2,3) + 6*Power(s,2)*Power(t2,3) - 
            12*Power(s,3)*Power(t2,3) + 40*Power(t2,4) - 
            49*s*Power(t2,4) + 48*Power(s,2)*Power(t2,4) + 
            9*Power(s,3)*Power(t2,4) - 22*Power(t2,5) + 
            79*s*Power(t2,5) - 17*Power(s,2)*Power(t2,5) - 
            Power(s,3)*Power(t2,5) - 7*Power(t2,6) - 38*s*Power(t2,6) - 
            3*Power(s,2)*Power(t2,6) + 4*Power(t2,7) + 
            Power(t1,5)*(158 - 2*Power(s,3) + Power(s,2)*(28 - 5*t2) + 
               62*t2 + s*(61 + 76*t2)) + 
            Power(t1,4)*(Power(s,3)*(-1 + 7*t2) + 
               Power(s,2)*(142 + 46*t2 + 29*Power(t2,2)) + 
               s*(211 + 577*t2 + 60*Power(t2,2)) + 
               3*(59 + 142*t2 + 42*Power(t2,2) + 36*Power(t2,3))) + 
            Power(s1,3)*(-2 + Power(t1,5) + Power(t1,4)*(5 - 12*t2) + 
               4*t2 - 6*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,3)*(34 + 8*t2 + 40*Power(t2,2)) - 
               Power(t1,2)*(-14 + 26*t2 + 17*Power(t2,2) + 
                  27*Power(t2,3)) + 
               t1*(-13 + 35*t2 - 64*Power(t2,2) + 28*Power(t2,3) + 
                  2*Power(t2,4))) - 
            Power(t1,3)*(Power(s,3)*(-7 - 14*t2 + 8*Power(t2,2)) + 
               Power(s,2)*(-82 - 96*t2 + 121*Power(t2,2) + 
                  54*Power(t2,3)) + 
               s*(-41 - 420*t2 + 528*Power(t2,2) + 462*Power(t2,3)) + 
               4*(22 - 42*t2 + 235*Power(t2,2) + 149*Power(t2,3) + 
                  72*Power(t2,4))) + 
            Power(t1,2)*(-42 - 125*t2 - 216*Power(t2,2) - 
               98*Power(t2,3) + 471*Power(t2,4) + 256*Power(t2,5) + 
               2*Power(s,3)*t2*(7 - 8*t2 + Power(t2,2)) + 
               Power(s,2)*(-8 - 86*t2 - 225*Power(t2,2) - 
                  25*Power(t2,3) + 38*Power(t2,4)) + 
               s*(-47 + 234*t2 - 535*Power(t2,2) - 548*Power(t2,3) + 
                  407*Power(t2,4))) + 
            t1*(17 - 83*t2 + 58*Power(t2,2) - 34*Power(t2,3) + 
               281*Power(t2,4) - 83*Power(t2,5) - 80*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(-9 - 6*t2 + 2*Power(t2,2)) + 
               Power(s,2)*t2*
                (-61 + 10*t2 - 61*Power(t2,2) + 89*Power(t2,3) - 
                  5*Power(t2,4)) + 
               s*(8 + 29*t2 - 46*Power(t2,2) - 241*Power(t2,3) + 
                  359*Power(t2,4) - 42*Power(t2,5))) + 
            Power(s1,2)*(-15 + (36 - 5*s)*t2 - (26 + s)*Power(t2,2) + 
               (1 + 44*s)*Power(t2,3) + (27 - 44*s)*Power(t2,4) + 
               (-23 + 10*s)*Power(t2,5) - 4*Power(t1,5)*(-6 + s + t2) + 
               Power(t1,4)*(93 + 24*t2 + 49*Power(t2,2) + 
                  s*(-23 + 31*t2)) - 
               Power(t1,3)*(-16 + 430*t2 + 249*Power(t2,2) + 
                  91*Power(t2,3) + s*(139 - 30*t2 + 70*Power(t2,2))) + 
               Power(t1,2)*(25 - 320*t2 + 128*Power(t2,2) + 
                  314*Power(t2,3) + 43*Power(t2,4) + 
                  s*(-136 + 70*t2 + 7*Power(t2,2) + 45*Power(t2,3))) - 
               t1*(3 + 52*t2 - 22*Power(t2,2) - 278*Power(t2,3) + 
                  152*Power(t2,4) - 3*Power(t2,5) + 
                  s*(1 + 106*t2 - 250*Power(t2,2) + 70*Power(t2,3) + 
                     12*Power(t2,4)))) + 
            s1*(-Power(t1,6) + 
               Power(t1,5)*(-83 + 5*Power(s,2) + 9*s*(-6 + t2) - 
                  102*t2) - Power(t1,4)*
                (149 + 498*t2 + 11*Power(t2,2) + 
                  Power(s,2)*(-19 + 26*t2) + 
                  s*(295 + 85*t2 + 77*Power(t2,2))) + 
               Power(t1,3)*(82 + 189*t2 + 1076*Power(t2,2) + 
                  605*Power(t2,3) + 
                  Power(s,2)*(79 - 46*t2 + 39*Power(t2,2)) + 
                  s*(-281 + 483*t2 + 419*Power(t2,2) + 146*Power(t2,3))) \
+ Power(t1,2)*(25 + 210*t2 + 42*Power(t2,2) - 91*Power(t2,3) - 
                  682*Power(t2,4) + 
                  Power(s,2)*
                   (25 + 55*t2 + 44*Power(t2,2) - 19*Power(t2,3)) + 
                  s*(-150 + 439*t2 + 516*Power(t2,2) - 
                     343*Power(t2,3) - 102*Power(t2,4))) + 
               t2*(-18 + 2*t2 + 5*Power(t2,2) + 8*Power(t2,3) - 
                  13*Power(t2,4) + 16*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-2 - 17*t2 + 9*Power(t2,2) + Power(t2,3)) - 
                  s*(47 - 69*t2 + 32*Power(t2,2) + 59*Power(t2,3) - 
                     58*Power(t2,4) + 5*Power(t2,5))) + 
               t1*(37 - 208*t2 + 224*Power(t2,2) - 328*Power(t2,3) - 
                  124*Power(t2,4) + 175*Power(t2,5) + 
                  Power(s,2)*t2*(15 - 17*t2 - 26*Power(t2,2)) + 
                  s*(-53 + 94*t2 - 330*Power(t2,3) + 5*Power(t2,4) + 
                     29*Power(t2,5))))) - 
         Power(s2,4)*(6 - 2*(-3 + s)*Power(t1,6) - 8*t2 + 53*s*t2 - 
            23*Power(t2,2) - 93*s*Power(t2,2) + 
            23*Power(s,2)*Power(t2,2) + 27*Power(t2,3) + 
            62*s*Power(t2,3) - 58*Power(s,2)*Power(t2,3) + 
            2*Power(s,3)*Power(t2,3) - 4*Power(t2,4) - 
            128*s*Power(t2,4) + 54*Power(s,2)*Power(t2,4) - 
            2*Power(s,3)*Power(t2,4) + 71*Power(t2,5) + 
            184*s*Power(t2,5) - 34*Power(t2,6) - 58*s*Power(t2,6) - 
            5*Power(s,2)*Power(t2,6) - 16*Power(t2,7) + 
            Power(t1,5)*(77 - Power(s,3) + Power(s,2)*(5 - 2*t2) + 
               65*t2 + s*(7 + 10*t2)) + 
            Power(t1,4)*(136 - Power(s,3)*(-3 + t2) + 348*t2 - 
               80*Power(t2,2) + Power(s,2)*(42 + 32*t2 + Power(t2,2)) + 
               s*(105 + 176*t2 + 72*Power(t2,2))) + 
            Power(t1,3)*(-39 + 557*t2 + 14*Power(t2,2) + 
               80*Power(t2,3) + 36*Power(t2,4) + 
               Power(s,3)*(13 - 7*t2 + 9*Power(t2,2)) + 
               s*(104 + 537*t2 + 358*Power(t2,2) - 124*Power(t2,3)) + 
               2*Power(s,2)*(25 + 100*t2 - 17*Power(t2,2) + 
                  7*Power(t2,3))) + 
            Power(s1,3)*(-5 + Power(t1,5) - Power(t1,4)*(-7 + t2) + 
               9*t2 + 4*Power(t2,2) - 25*Power(t2,3) + 14*Power(t2,4) + 
               Power(t1,3)*(36 - 28*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*(55 - 25*t2 + 41*Power(t2,2) + 
                  17*Power(t2,3)) + 
               t1*(3 + 19*t2 - 34*Power(t2,2) - 12*Power(t2,3) - 
                  10*Power(t2,4))) - 
            Power(t1,2)*(67 + 82*t2 - 12*Power(t2,2) + 930*Power(t2,3) + 
               206*Power(t2,4) + 88*Power(t2,5) + 
               Power(s,3)*t2*(-6 - 3*t2 + 11*Power(t2,2)) + 
               Power(s,2)*(2 - 77*t2 + 182*Power(t2,2) + 
                  48*Power(t2,3) + 28*Power(t2,4)) + 
               s*(38 - 364*t2 - 36*Power(t2,2) + 860*Power(t2,3) + 
                  56*Power(t2,4))) + 
            t1*(15 - 134*t2 + 60*Power(t2,2) - 293*Power(t2,3) + 
               269*Power(t2,4) + 169*Power(t2,5) + 68*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(-21 + 3*t2 + 4*Power(t2,2)) + 
               Power(s,2)*t2*
                (-11 - 39*t2 - 114*Power(t2,2) + 45*Power(t2,3) + 
                  20*Power(t2,4)) + 
               s*(17 - 7*t2 + 311*Power(t2,2) - 695*Power(t2,3) + 
                  135*Power(t2,4) + 158*Power(t2,5))) + 
            Power(s1,2)*(Power(t1,5)*(6 - 3*s - 2*t2) + 
               Power(t1,4)*(84 + s*(-15 + t2) + 13*t2 - Power(t2,2)) + 
               Power(t1,3)*(145 + 49*t2 - 11*Power(t2,2) + 
                  21*Power(t2,3) + s*(-69 + 41*t2 + 18*Power(t2,2))) - 
               Power(t1,2)*(-95 + 218*t2 + 475*Power(t2,2) + 
                  95*Power(t2,3) + 33*Power(t2,4) + 
                  s*(140 - 19*t2 + 54*Power(t2,2) + 36*Power(t2,3))) - 
               t2*(6 + 11*t2 - Power(t2,2) - 101*Power(t2,3) + 
                  61*Power(t2,4) + 
                  s*(16 + 12*t2 - 89*Power(t2,2) + 40*Power(t2,3) + 
                     2*Power(t2,4))) + 
               t1*(71 - 192*t2 - 129*Power(t2,2) + 245*Power(t2,3) + 
                  120*Power(t2,4) + 15*Power(t2,5) + 
                  s*(-40 - 73*t2 + 86*Power(t2,2) + 18*Power(t2,3) + 
                     22*Power(t2,4)))) + 
            s1*(27 + 2*Power(t1,6) + (-83 + s)*t2 + 
               (47 - 34*s - 8*Power(s,2))*Power(t2,2) + 
               (-5 + 62*s - 11*Power(s,2))*Power(t2,3) - 
               (39 + 185*s + 3*Power(s,2))*Power(t2,4) + 
               (-71 + 66*s + 3*Power(s,2))*Power(t2,5) + 
               (68 + 7*s)*Power(t2,6) + 
               Power(t1,5)*(-7 + 3*Power(s,2) - 10*t2 + s*(-11 + 4*t2)) + 
               Power(t1,4)*(-109 - 236*t2 - 91*Power(t2,2) + 
                  Power(s,2)*(5 + t2) - s*(148 + 39*t2)) - 
               Power(t1,3)*(76 + 429*t2 + 310*Power(t2,2) - 
                  153*Power(t2,3) + 
                  Power(s,2)*(1 + 2*t2 + 21*Power(t2,2)) + 
                  s*(316 + 301*t2 - 34*Power(t2,2) + 35*Power(t2,3))) + 
               Power(t1,2)*(37 + 117*t2 + 317*Power(t2,2) + 
                  1083*Power(t2,3) + 99*Power(t2,4) + 
                  Power(s,2)*
                   (-13 + 21*t2 + 16*Power(t2,2) + 30*Power(t2,3)) + 
                  s*(-152 - 140*t2 + 855*Power(t2,2) + 163*Power(t2,3) + 
                     61*Power(t2,4))) - 
               t1*(-62 + 131*t2 - 296*Power(t2,2) + 69*Power(t2,3) + 
                  281*Power(t2,4) + 221*Power(t2,5) + 
                  Power(s,2)*t2*
                   (59 - 101*t2 + 16*Power(t2,2) + 16*Power(t2,3)) + 
                  s*(29 - 38*t2 - 254*Power(t2,2) - 9*Power(t2,3) + 
                     213*Power(t2,4) + 37*Power(t2,5))))))*T4q(-1 + s2))/
     (Power(-1 + s2,2)*(-s + s2 - t1)*(-1 + t1)*(s - s1 + t2)*
       Power(-1 + s2 - t1 + t2,2)*Power(-t1 + s2*t2,3)) + 
    (8*((5 + 5*s - 7*s1)*Power(t1,6) - 
         Power(1 + (-1 + s)*t2,3)*(2 - 3*t2 + Power(t2,2)) + 
         Power(s2,7)*(Power(s1,3) + s1*Power(t2,2) - 2*Power(t2,3)) + 
         Power(t1,5)*(7 + Power(s,3) - 2*Power(s1,3) - 13*t2 + 
            2*Power(s1,2)*t2 + Power(s,2)*(7 - 4*s1 + t2) + 
            2*s1*(-5 + 3*t2) + 
            s*(14 + 5*Power(s1,2) - 8*t2 - 3*s1*(3 + t2))) - 
         Power(s2,6)*(Power(s1,3)*(1 + 4*t1 - 3*t2) + 
            s1*t2*((11 - 2*t2)*t2 + t1*(2 + t2) + s*(3 - 3*t1 + t2)) + 
            Power(s1,2)*(3 + (-4 + t1)*t2 - 2*Power(t2,2) - 
               s*(3*t1 + t2)) + 
            Power(t2,2)*(1 + s*(3 - 4*t1 - 2*t2) - 2*t2 + 5*Power(t2,2) - 
               2*t1*(3 + 5*t2))) + 
         t1*(-1 + t2)*(1 + (-1 + s)*t2)*
          (Power(s,2)*t2*(-1 + 2*t2) - 
            (-1 + t2)*(4 + s1*(5 - 4*t2) + 2*t2 + Power(t2,2)) + 
            s*(5 + 10*t2 - 3*Power(t2,2) + Power(t2,3) + 
               s1*(-6 + 8*t2 - 4*Power(t2,2)))) + 
         Power(t1,4)*(-9 - 6*t2 + 3*Power(s1,3)*t2 + 14*Power(t2,2) - 
            2*Power(s,3)*(1 + t2) + 
            Power(s1,2)*(-19 + t2 - 2*Power(t2,2)) + 
            2*s1*(10 + Power(t2,2)) + 
            Power(s,2)*(s1*(4 + 7*t2) - 4*(-5 + 4*t2 + Power(t2,2))) + 
            s*(12 - 9*t2 + 2*Power(t2,2) - 2*Power(s1,2)*(1 + 4*t2) + 
               s1*(5 + 6*t2 + 7*Power(t2,2)))) + 
         Power(t1,3)*(Power(s,3)*(-1 + 7*t2 + Power(t2,2)) - 
            (-1 + t2)*(9 + 10*t2 + 2*Power(s1,3)*t2 + 8*Power(t2,2) - 
               2*Power(s1,2)*(4 + t2) - 5*s1*(5 + t2)) + 
            Power(s,2)*(-7 - 22*t2 + 11*Power(t2,2) + 6*Power(t2,3) - 
               2*s1*(-11 + 7*t2 + 3*Power(t2,2))) + 
            s*(-1 - 37*t2 - 3*Power(t2,2) - 2*Power(t2,3) + 
               Power(s1,2)*(-13 - t2 + 8*Power(t2,2)) + 
               s1*(39 - 11*t2 + 3*Power(t2,2) - 5*Power(t2,3)))) + 
         Power(t1,2)*(-(Power(s,3)*t2*(-2 + 5*t2 + Power(t2,2))) + 
            Power(-1 + t2,2)*(-6 + 3*t2 + Power(t2,2) + 
               Power(s1,2)*(-3 + 5*t2) - s1*(13 + 7*t2 + Power(t2,2))) + 
            s*(-1 + t2)*(1 + 33*t2 + Power(t2,2) + 5*Power(t2,3) + 
               Power(s1,2)*(-4 + 8*t2 - 5*Power(t2,2)) + 
               s1*(-3 + 12*t2 - 7*Power(t2,2) + Power(t2,3))) + 
            Power(s,2)*(-2 + 5*t2 - 4*Power(t2,2) + Power(t2,3) - 
               4*Power(t2,4) + 
               s1*(4 - 10*t2 - 2*Power(t2,2) + 7*Power(t2,3)))) + 
         Power(s2,5)*(Power(s1,3)*
             (-3 + 6*Power(t1,2) + t1*(2 - 11*t2) + t2 + 3*Power(t2,2)) + 
            Power(s1,2)*(3 - 5*(3 + s)*t2 + 3*s*Power(t2,2) + 
               5*Power(t2,3) + Power(t1,2)*(1 - 9*s + 3*t2) + 
               t1*(8 - 21*t2 - 6*Power(t2,2) + s*(-7 + 9*t2))) - 
            t2*(2*Power(t1,2)*(3 + 15*t2 + 7*Power(t2,2)) + 
               t1*(-2 + 5*t2 + 2*Power(t2,2) - 19*Power(t2,3)) + 
               t2*(-11 + 16*t2 - 3*Power(t2,2) + 4*Power(t2,3)) - 
               Power(s,2)*(3*Power(t1,2) + t2 - t1*(3 + 5*t2)) + 
               s*(-3 - 2*t2 + 13*Power(t2,2) - 5*Power(t2,3) + 
                  Power(t1,2)*(8 + 5*t2) + t1*(-3 + 14*t2 + Power(t2,2)))\
) + s1*(3 + Power(s,2)*t1*(3*t1 - t2) - 8*t2 + 20*Power(t2,2) - 
               11*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,2)*(1 + 2*t2 - 3*Power(t2,2)) + 
               t1*t2*(24 + 25*t2 + 3*Power(t2,2)) + 
               s*(-(Power(t1,2)*(3 + 8*t2)) + 
                  t2*(4 + t2 - 3*Power(t2,2)) + 
                  t1*(-3 + 10*t2 + 15*Power(t2,2))))) + 
         Power(s2,4)*(-1 + 4*t2 - 5*s*t2 - 22*Power(t2,2) + 
            19*s*Power(t2,2) + 6*Power(s,2)*Power(t2,2) + 
            25*Power(t2,3) + 39*s*Power(t2,3) + 
            5*Power(s,2)*Power(t2,3) - 6*Power(t2,4) - 12*s*Power(t2,4) + 
            4*s*Power(t2,5) - Power(t2,6) + 
            Power(t1,2)*(-1 + 
               (4 + 30*s + 8*Power(s,2) - 2*Power(s,3))*t2 + 
               (39 + 17*s + 15*Power(s,2))*Power(t2,2) + 
               3*(-13 + 4*s)*Power(t2,3) - 14*Power(t2,4)) + 
            Power(t1,3)*(2 + Power(s,3) + 30*t2 + 42*Power(t2,2) + 
               6*Power(t2,3) - Power(s,2)*(3 + 4*t2) + 
               s*(4 + 10*t2 - Power(t2,2))) + 
            Power(s1,3)*(1 - 4*Power(t1,3) - 3*t2 + 4*Power(t2,2) + 
               Power(t2,3) + Power(t1,2)*(1 + 15*t2) - 
               2*t1*(-6 + 4*t2 + 5*Power(t2,2))) + 
            t1*t2*(-23 + 17*t2 + Power(s,3)*t2 + 30*Power(t2,2) - 
               15*Power(t2,3) + 9*Power(t2,4) + 
               Power(s,2)*(8 - 4*t2 - 11*Power(t2,2)) - 
               s*(12 - 43*t2 + 18*Power(t2,2) + 15*Power(t2,3))) + 
            Power(s1,2)*(9 - 12*t2 - 11*Power(t2,2) - 10*Power(t2,3) + 
               4*Power(t2,4) - 3*Power(t1,3)*(1 + t2) + 
               t1*t2*(52 - 13*t2 - 9*Power(t2,2)) + 
               Power(t1,2)*(1 + 41*t2 + 4*Power(t2,2)) + 
               s*(9*Power(t1,3) - 3*Power(t1,2)*(-4 + 9*t2) + 
                  t2*(-1 - 9*t2 + 3*Power(t2,2)) + 
                  t1*(-1 + t2 + 10*Power(t2,2)))) - 
            s1*(3 - (21 + 16*s)*t2 + 
               (17 + 15*s + 2*Power(s,2))*Power(t2,2) + 
               2*(-8 + s)*Power(t2,3) + (-3 + 4*s)*Power(t2,4) - 
               2*Power(t2,5) + 
               Power(t1,3)*(1 + 6*Power(s,2) - 6*t2 - 5*Power(t2,2) - 
                  s*(8 + 7*t2)) + 
               Power(t1,2)*(13 + Power(s,2)*(8 - 14*t2) + 62*t2 + 
                  37*Power(t2,2) + 13*Power(t2,3) + 
                  s*(-9 + 42*t2 + 22*Power(t2,2))) + 
               t1*(4 + 6*t2 + 31*Power(t2,2) - 6*Power(t2,3) - 
                  7*Power(t2,4) + 2*Power(s,2)*t2*(3 + 2*t2) + 
                  s*(-8 + 61*t2 + 8*Power(t2,2) - 24*Power(t2,3))))) + 
         Power(s2,3)*(1 + 12*Power(t1,2) - Power(t1,3) - 10*Power(t1,4) - 
            9*t2 + 27*t1*t2 + 17*Power(t1,2)*t2 - 52*Power(t1,3)*t2 - 
            42*Power(t1,4)*t2 + 17*Power(t2,2) - 5*t1*Power(t2,2) - 
            80*Power(t1,2)*Power(t2,2) - 2*Power(t1,3)*Power(t2,2) - 
            18*Power(t1,4)*Power(t2,2) - 24*Power(t2,3) - 
            25*t1*Power(t2,3) + 54*Power(t1,2)*Power(t2,3) + 
            39*Power(t1,3)*Power(t2,3) + 2*Power(t2,4) + 
            14*t1*Power(t2,4) - 15*Power(t1,2)*Power(t2,4) + 
            12*Power(t2,5) - 7*t1*Power(t2,5) + Power(t2,6) - 
            Power(s,3)*(Power(t1,4) + Power(t1,3)*(2 - 5*t2) + 
               7*Power(t1,2)*Power(t2,2) + 2*Power(t2,3) - 
               t1*Power(t2,2)*(4 + 3*t2)) + 
            Power(s,2)*(Power(t1,4)*(4 + t2) + 
               Power(t2,2)*(-12 + 11*t2) + 
               Power(t1,3)*(3 - 25*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*t2*(-32 + 17*t2 + 9*Power(t2,2)) + 
               2*t1*t2*(-3 + 4*t2 + 2*Power(t2,2) - 2*Power(t2,3))) + 
            Power(s1,3)*(Power(t1,4) - Power(t1,3)*(5 + 9*t2) + 
               Power(t1,2)*(-17 + 20*t2 + 11*Power(t2,2)) + 
               2*(1 - 2*t2 + Power(t2,3)) - 
               t1*(2 - 4*t2 + 11*Power(t2,2) + 3*Power(t2,3))) + 
            s*(Power(t1,4)*(-5 + 2*t2 + 2*Power(t2,2)) - 
               2*Power(t1,3)*
                (9 + 19*t2 + 16*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(1 - 35*t2 - 6*Power(t2,2) + 
                  39*Power(t2,3) + 19*Power(t2,4)) + 
               t2*(-11 + 9*Power(t2,2) + 43*Power(t2,3) + Power(t2,4) + 
                  Power(t2,5)) - 
               t1*(1 - 12*t2 + 158*Power(t2,2) - 27*Power(t2,3) + 
                  10*Power(t2,4) + 10*Power(t2,5))) + 
            Power(s1,2)*(-3 + 5*(3 + s)*t2 + (-13 + s)*Power(t2,2) - 
               (-4 + s)*Power(t2,3) + (-4 + s)*Power(t2,4) + 
               Power(t2,5) + Power(t1,4)*(3 - 3*s + t2) + 
               Power(t1,3)*(-23 + (-33 + 23*s)*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-22 - 56*t2 + 27*Power(t2,2) + 
                  Power(t2,3) + s*(14 - 10*t2 - 27*Power(t2,2))) + 
               t1*(-27 + 64*t2 + 41*Power(t2,2) + 7*Power(t2,3) - 
                  5*Power(t2,4) + 
                  s*(13 + t2 + 11*Power(t2,2) + 6*Power(t2,3)))) + 
            s1*(-9 + (21 - 10*s)*t2 + 
               (-2 + 27*s + 2*Power(s,2))*Power(t2,2) + 
               (9 - 29*s)*Power(t2,3) - (20 + 11*s)*Power(t2,4) - 
               3*s*Power(t2,5) + Power(t2,6) + 
               Power(t1,4)*(-3 + 3*Power(s,2) - 10*t2 - 2*Power(t2,2) - 
                  s*(7 + 2*t2)) + 
               Power(t1,3)*(35 + Power(s,2)*(7 - 19*t2) + 71*t2 + 
                  46*Power(t2,2) + 8*Power(t2,3) + 
                  s*(9 + 60*t2 + 4*Power(t2,2))) + 
               Power(t1,2)*(4 + 37*t2 + 42*Power(t2,2) - 
                  21*Power(t2,3) - 10*Power(t2,4) + 
                  Power(s,2)*(6 - 2*t2 + 23*Power(t2,2)) + 
                  s*(18 + 155*t2 - 50*Power(t2,2) - 12*Power(t2,3))) - 
               t1*(6 + 37*t2 + 8*Power(t2,2) + 23*Power(t2,3) + 
                  17*Power(t2,4) - 3*Power(t2,5) + 
                  Power(s,2)*t2*(-18 + 15*t2 + 7*Power(t2,2)) + 
                  s*(4 - 10*t2 + 11*Power(t2,2) + 11*Power(t2,3) - 
                     13*Power(t2,4))))) + 
         s2*(2*(-3 + s)*Power(t1,6) - 
            (-1 + t2)*(-1 + 8*(1 + s)*t2 + 
               (-12 + 3*s + 13*Power(s,2))*Power(t2,2) + 
               (5 - 12*s + Power(s,2) + 4*Power(s,3))*Power(t2,3) + 
               (-1 + 3*s - 2*Power(s,2))*Power(t2,4) + 
               Power(-1 + s,2)*Power(t2,5)) + 
            Power(t1,5)*(-23 + Power(s,3) + 2*Power(s,2)*(-4 + t2) + 
               t2 - 2*s*(5 + 11*t2)) - 
            Power(s1,3)*Power(t1,2)*
             (-3 + Power(t1,3) + 11*t2 - 7*Power(t2,2) - Power(t2,3) - 
               Power(t1,2)*(2 + 7*t2) + t1*(1 + 5*t2 + 6*Power(t2,2))) - 
            Power(t1,4)*(8 + 3*t2 + 5*Power(s,3)*t2 - 29*Power(t2,2) + 
               s*(14 + 32*t2 - 39*Power(t2,2)) + 
               Power(s,2)*(33 - 7*t2 + 7*Power(t2,2))) + 
            Power(t1,3)*(20 + 4*t2 + 15*Power(t2,2) - 35*Power(t2,3) + 
               Power(s,3)*t2*(-2 + 9*t2) + 
               Power(s,2)*(-17 + 15*t2 + 16*Power(t2,2) + 
                  10*Power(t2,3)) - 
               s*(4 + 73*t2 - 37*Power(t2,2) + 20*Power(t2,3))) + 
            Power(t1,2)*(-3 - 25*t2 - 6*Power(t2,2) + 18*Power(t2,3) + 
               16*Power(t2,4) - Power(s,3)*t2*(2 + 7*Power(t2,2)) + 
               Power(s,2)*(2 + 36*t2 + 11*Power(t2,2) - 
                  18*Power(t2,3) - 8*Power(t2,4)) + 
               s*(5 - 31*t2 + 140*Power(t2,2) + 12*Power(t2,3) + 
                  3*Power(t2,4))) + 
            t1*(2*Power(s,3)*Power(t2,2)*(-1 + 3*t2 + Power(t2,2)) - 
               Power(-1 + t2,2)*
                (3 - 12*t2 + Power(t2,2) + 4*Power(t2,3)) + 
               Power(s,2)*t2*
                (13 - 19*t2 + 10*Power(t2,2) + 4*Power(t2,4)) - 
               s*(-1 + 2*t2 - 71*Power(t2,2) + 64*Power(t2,3) + 
                  2*Power(t2,4) + 4*Power(t2,5))) + 
            Power(s1,2)*t1*(Power(t1,4)*(-9 + 2*t2) - 
               5*Power(t1,3)*(3 - t2 + Power(t2,2)) - 
               2*Power(-1 + t2,2)*(-5 + 6*t2 + Power(t2,2)) + 
               Power(t1,2)*(23 + 59*t2 - 4*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(11 + 7*t2 - 16*Power(t2,2) - 3*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(-6 + 3*Power(t1,4) + 17*t2 - 19*Power(t1,3)*t2 - 
                  16*Power(t2,2) + 3*Power(t2,3) + 2*Power(t2,4) + 
                  Power(t1,2)*(24 - 11*t2 + 24*Power(t2,2)) - 
                  2*t1*(-4 - 16*t2 + 6*Power(t2,2) + 5*Power(t2,3)))) + 
            s1*(-2*Power(t1,6) + 
               (-1 + t2)*(-6 + (16 - 6*s)*t2 + 
                  (-13 + 11*s)*Power(t2,2) + 
                  (2 - 3*s + Power(s,2))*Power(t2,3) + 
                  Power(-1 + s,2)*Power(t2,4)) + 
               Power(t1,5)*(-3*Power(s,2) + s*(17 - 4*t2) + 
                  22*(1 + t2)) + 
               Power(t1,4)*(13 + 35*t2 - 26*Power(t2,2) + 
                  Power(s,2)*(-2 + 17*t2) + 
                  s*(73 - 18*t2 + 12*Power(t2,2))) + 
               Power(t1,3)*(-35 - 36*t2 - 18*Power(t2,2) + Power(t2,3) + 
                  Power(s,2)*(-17 + 14*t2 - 27*Power(t2,2)) + 
                  s*(37 - 79*t2 + 5*Power(t2,2) - 12*Power(t2,3))) + 
               Power(t1,2)*(Power(s,2)*
                   (-7 - 19*t2 + 8*Power(t2,2) + 16*Power(t2,3)) + 
                  5*(2 + 11*t2 - 9*Power(t2,2) - 5*Power(t2,3) + 
                     Power(t2,4)) + 
                  s*(14 - 121*t2 + 32*Power(t2,2) - 7*Power(t2,3) + 
                     4*Power(t2,4))) + 
               t1*(2*Power(-1 + t2,2)*(3 + 13*t2 + 5*Power(t2,2)) - 
                  Power(s,2)*t2*
                   (13 - 39*t2 + 20*Power(t2,2) + 4*Power(t2,3)) + 
                  s*(7 - 10*Power(t2,2) - 2*Power(t2,3) + 5*Power(t2,4))))\
) + Power(s2,2)*(3 - 10*t2 + 11*s*t2 + 10*Power(t2,2) - 39*s*Power(t2,2) - 
            2*Power(s,2)*Power(t2,2) - 5*Power(t2,3) + 15*s*Power(t2,3) - 
            10*Power(s,2)*Power(t2,3) + 8*Power(t2,4) + 2*s*Power(t2,4) + 
            15*Power(s,2)*Power(t2,4) - 2*Power(s,3)*Power(t2,4) - 
            9*Power(t2,5) + 12*s*Power(t2,5) - 7*Power(s,2)*Power(t2,5) + 
            3*Power(t2,6) - s*Power(t2,6) - 
            Power(t1,5)*(s + Power(s,2) + 4*s*t2 - 2*(7 + 9*t2)) + 
            Power(t1,4)*(20 + 45*t2 - 2*Power(s,3)*t2 - 31*Power(t2,2) + 
               Power(s,2)*(7 + 14*t2 - 2*Power(t2,2)) + 
               s*(22 + 30*t2 + 29*Power(t2,2))) + 
            Power(s1,3)*t1*(-5 + 13*t2 - 5*Power(t2,2) - 3*Power(t2,3) + 
               2*Power(t1,3)*(2 + t2) - 
               4*Power(t1,2)*(-2 + 5*t2 + Power(t2,2)) + 
               t1*(2 + t2 + 13*Power(t2,2) + 2*Power(t2,3))) + 
            Power(t1,3)*(-18 + 55*t2 - 39*Power(t2,2) - 10*Power(t2,3) + 
               2*Power(s,3)*(1 + t2 + 3*Power(t2,2)) + 
               s*(14 + 30*t2 - 6*Power(t2,2) - 50*Power(t2,3)) + 
               Power(s,2)*(15 + 42*t2 - 26*Power(t2,2) + 6*Power(t2,3))) + 
            Power(t1,2)*(-11 - 36*t2 + 35*Power(t2,2) - 23*Power(t2,3) + 
               29*Power(t2,4) - 
               2*Power(s,3)*t2*(-1 + 3*t2 + 3*Power(t2,2)) + 
               Power(s,2)*t2*
                (7 - 45*t2 + 7*Power(t2,2) - 6*Power(t2,3)) + 
               s*(-10 + 96*t2 + 61*Power(t2,2) - 25*Power(t2,3) + 
                  28*Power(t2,4))) + 
            t1*(4 - 4*t2 + 30*Power(t2,2) + 8*Power(t2,3) - 
               29*Power(t2,4) - 9*Power(t2,5) + 
               2*Power(s,3)*Power(t2,2)*(-2 + 3*t2 + Power(t2,2)) + 
               Power(s,2)*t2*(-8 + 2*t2 - 19*Power(t2,2) + 
                  13*Power(t2,3) + 2*Power(t2,4)) + 
               s*(5 + 13*t2 + 5*Power(t2,2) - 140*Power(t2,3) - 
                  10*Power(t2,4) - 2*Power(t2,5))) - 
            Power(s1,2)*(Power(t1,5) + 
               Power(t1,4)*(-26 - 7*t2 + 2*Power(t2,2)) - 
               Power(-1 + t2,2)*(-6 + 5*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-13 + 108*t2 + 30*Power(t2,2) - 
                  5*Power(t2,3)) - 
               Power(t1,3)*(34 + 12*t2 - 9*Power(t2,2) + 3*Power(t2,3)) + 
               t1*t2*(28 - 27*t2 + Power(t2,2) - 3*Power(t2,3) + 
                  Power(t2,4)) + 
               s*((-1 + t2)*Power(t2,3) + Power(t1,4)*(8 + 6*t2) + 
                  Power(t1,3)*(18 - 33*t2 - 14*Power(t2,2)) + 
                  Power(t1,2)*
                   (34 - 16*t2 + 23*Power(t2,2) + 10*Power(t2,3)) + 
                  t1*(2 + 18*t2 + 9*Power(t2,2) - 9*Power(t2,3) - 
                     2*Power(t2,4)))) + 
            s1*(3 - (21 + 13*s)*t2 + (29 + 15*s)*Power(t2,2) + 
               (-13 + 7*s - 8*Power(s,2))*Power(t2,3) + 
               (9 - 11*s + 7*Power(s,2))*Power(t2,4) + 
               (-8 + 3*s)*Power(t2,5) - (-1 + s)*Power(t2,6) + 
               Power(t1,5)*(5 + 2*s + 4*t2) + 
               Power(t1,4)*(-37 - 55*t2 - 23*Power(t2,2) + 
                  Power(s,2)*(4 + 6*t2) + s*(-32 - 21*t2 + 4*Power(t2,2))) \
- Power(t1,3)*(7 + 60*t2 + 11*Power(t2,2) - 30*Power(t2,3) + 
                  Power(s,2)*(-3 + 15*t2 + 16*Power(t2,2)) + 
                  s*(90 + 77*t2 - 45*Power(t2,2) + 9*Power(t2,3))) + 
               Power(t1,2)*(24 + 57*t2 + 21*Power(t2,2) + 
                  36*Power(t2,3) - 6*Power(t2,4) + 
                  Power(s,2)*(2 - 10*t2 + 19*Power(t2,2) + 
                     14*Power(t2,3)) + 
                  s*(-31 + 12*t2 + 77*Power(t2,2) - 24*Power(t2,3) + 
                     5*Power(t2,4))) + 
               t1*(18 - 74*t2 + 15*Power(t2,2) + 24*Power(t2,3) + 
                  23*Power(t2,4) - 6*Power(t2,5) - 
                  Power(s,2)*t2*
                   (4 - 21*t2 + 15*Power(t2,2) + 4*Power(t2,3)) + 
                  s*(-14 + 43*t2 + 3*Power(t2,2) + 50*Power(t2,3) - 
                     5*Power(t2,4) + Power(t2,5))))))*T5q(1 - s2 + t1 - t2))/
     ((-1 + t1)*(s - s2 + t1)*(1 - s2 + t1 - t2)*(s - s1 + t2)*
       Power(t1 - s2*t2,3)));
   return a;
};
