/*
 * dilog.h 
 */

long double  dilog(long double);
long double  clausen(long double);
int  dilogc(long double,long double,long double*,long double*);
int logl_c(long double,long double,long double*,long double*);

__float128  dilogq(__float128);
__float128  clausenq(__float128);
int  dilogcq(__float128,__float128,__float128*,__float128*);
int logl_cq(__float128,__float128,__float128*,__float128*);
