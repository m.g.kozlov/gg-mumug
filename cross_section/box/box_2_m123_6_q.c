#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m123_6_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(4*Power(s,5)*s1*t2 + Power(s,4)*
          (2*Power(s1,4) + Power(s1,3)*(3 + 2*t1 - 4*t2) - t2 + 
            Power(s1,2)*(-2 - 2*(2 + s2)*t2 - Power(t2,2) + 
               t1*(1 + t2)) + 
            s1*(13 + (-19 + 2*s2)*t2 + 13*Power(t2,2) - t1*(3 + t2))) + 
         s1*Power(-1 + t2,2)*(s1 + Power(s1,2) - 2*s1*t2 + (-1 + t2)*t2)*
          (5 + 2*Power(s1,3) + 11*t1 - 39*t2 - 6*t1*t2 + 18*Power(t2,2) - 
            s2*(2 + t2) + Power(s1,2)*(-6 - 3*s2 + 5*t1 + 2*t2) + 
            s1*(43 - 16*t1 - 19*t2 + 6*t1*t2 - 6*Power(t2,2) + 
               s2*(5 + t2))) - 
         Power(s,3)*(4*Power(s1,5) + Power(s1,4)*(5 + 4*t1 - 12*t2) + 
            t2*(-2 + 3*t2) + Power(s1,3)*
             (s2*(2 - 7*t2) + t1*(-2 + t2) - 
               2*(-14 + 3*t2 + Power(t2,2))) + 
            Power(s1,2)*(22 - 94*t2 + 33*Power(t2,2) + 9*Power(t2,3) + 
               t1*(-4 + 11*t2 - 9*Power(t2,2)) + 
               s2*(5 - 4*t2 + 5*Power(t2,2))) + 
            s1*(45 - 102*t2 + 102*Power(t2,2) - 33*Power(t2,3) + 
               s2*(-7 + 11*t2 - 5*Power(t2,2)) + 
               t1*(2 - 12*t2 + 9*Power(t2,2)))) + 
         s*(-1 + t2)*(4*Power(s1,6) - (-1 + t2)*Power(t2,2) + 
            Power(s1,5)*(3*s2 + t1 - 4*(2 + t2)) + 
            Power(s1,4)*(52 + s2 + 11*t2 - s2*t2 - 19*Power(t2,2) - 
               t1*(11 + t2)) + 
            Power(s1,3)*(142 - 310*t2 + 62*Power(t2,2) + 
               38*Power(t2,3) + t1*(-20 + 61*t2 - 19*Power(t2,2)) + 
               s2*(4 - 11*t2 - 3*Power(t2,2))) - 
            s1*(-13 + 67*t2 - 194*Power(t2,2) + 195*Power(t2,3) - 
               55*Power(t2,4) + 
               s2*(2 - 12*t2 + 10*Power(t2,2) + Power(t2,3)) + 
               t1*(-3 + 40*t2 - 57*Power(t2,2) + 19*Power(t2,3))) + 
            Power(s1,2)*(45 - 330*t2 + 454*Power(t2,2) - 
               118*Power(t2,3) - 19*Power(t2,4) + 
               s2*(-6 + 13*Power(t2,2) + Power(t2,3)) + 
               t1*(27 - 20*t2 - 38*Power(t2,2) + 19*Power(t2,3)))) + 
         Power(s,2)*(2*Power(s1,6) + 2*Power(s1,5)*(3 + t1 - 6*t2) + 
            t2*(-1 + 4*t2 - 3*Power(t2,2)) - 
            Power(s1,4)*(-12 - 11*t2 + Power(t2,2) + t1*(2 + t2) + 
               s2*(-5 + 8*t2)) + 
            Power(s1,3)*(113 - 190*t2 + 13*Power(t2,2) + 30*Power(t2,3) + 
               t1*(-25 + 41*t2 - 15*Power(t2,2)) + 
               s2*(9 - 13*t2 + 9*Power(t2,2))) + 
            s1*(45 - 168*t2 + 302*Power(t2,2) - 240*Power(t2,3) + 
               61*Power(t2,4) + 
               t1*(8 - 48*t2 + 62*Power(t2,2) - 21*Power(t2,3)) + 
               s2*(-9 + 26*t2 - 21*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s1,2)*(70 - 360*t2 + 418*Power(t2,2) - 91*Power(t2,3) - 
               21*Power(t2,4) - 
               s2*(5 + 5*t2 - 12*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(17 + 8*t2 - 47*Power(t2,2) + 21*Power(t2,3))))))/
     (s*Power(-1 + s1,2)*s1*(-s + s1 - t2)*(1 - s + s1 - t2)*(-1 + t2)*
       Power(-1 + s + t2,2)) + 
    (8*(2 + 2*Power(s1,5)*(s2 - t1) - 2*t1 + 73*t2 - 18*s2*t2 + 
         21*t1*t2 - 134*Power(t2,2) + 15*s2*Power(t2,2) - 
         21*t1*Power(t2,2) + 71*Power(t2,3) + s2*Power(t2,3) + 
         4*t1*Power(t2,3) - 12*Power(t2,4) - 
         2*Power(s,4)*((-1 + s1)*t1 - (-5 + s1)*t2) - 
         2*Power(s1,4)*(5 + t1 - 5*t2 - 2*t1*t2 + s2*(-2 + 3*t2)) - 
         Power(s1,3)*(12 + t1 - 32*t2 - 7*t1*t2 + 20*Power(t2,2) + 
            2*t1*Power(t2,2) + s2*(3 + 7*t2 - 6*Power(t2,2))) - 
         Power(s1,2)*(21 - 28*t2 + 13*Power(t2,2) - 6*Power(t2,3) + 
            t1*(-22 + 25*t2 + Power(t2,2)) + 
            s2*(21 - 25*t2 - 2*Power(t2,2) + 2*Power(t2,3))) + 
         s1*(-79 + 161*t2 - 89*Power(t2,2) + 3*Power(t2,3) + 
            4*Power(t2,4) + s2*
             (18 + 6*t2 - 23*Power(t2,2) + Power(t2,3)) - 
            t1*(15 + 7*t2 - 24*Power(t2,2) + 4*Power(t2,3))) + 
         Power(s,3)*(2 + 19*t2 - 2*s2*t2 - 20*Power(t2,2) - 
            2*Power(s1,2)*(-2 + s2 - 4*t1 + 3*t2) + t1*(-7 + 4*t2) + 
            s1*(-14 - t1 + 11*t2 - 4*t1*t2 + 4*Power(t2,2) + 
               2*s2*(1 + t2))) + 
         Power(s,2)*(-5 + 6*t1 + 60*t2 - s2*t2 - 8*t1*t2 + 
            24*Power(t2,2) - 4*s2*Power(t2,2) + 2*t1*Power(t2,2) - 
            10*Power(t2,3) + 2*Power(s1,3)*(-4 + 3*s2 - 6*t1 + 3*t2) - 
            Power(s1,2)*(-8 + s2 + t1*(6 - 12*t2) - 21*t2 + 10*s2*t2 + 
               8*Power(t2,2)) + 
            s1*(13 - 15*t2 + 8*Power(t2,2) + 2*Power(t2,3) - 
               2*t1*(-6 + 2*t2 + Power(t2,2)) + 
               s2*(-5 + 11*t2 + 4*Power(t2,2)))) + 
         s*(1 + t1 - 143*t2 + 21*s2*t2 - 17*t1*t2 + 123*Power(t2,2) + 
            3*t1*Power(t2,2) - 7*Power(t2,3) - 2*s2*Power(t2,3) - 
            2*Power(s1,4)*(-2 + 3*s2 - 4*t1 + t2) + 
            Power(s1,3)*(t1*(7 - 12*t2) + s2*(-5 + 14*t2) + 
               4*(4 - 8*t2 + Power(t2,2))) - 
            Power(s1,2)*(-1 + 38*t2 - 31*Power(t2,2) + 2*Power(t2,3) + 
               t1*(8 + 7*t2 - 4*Power(t2,2)) + 
               s2*(-16 + 3*t2 + 10*Power(t2,2))) + 
            s1*(74 - 97*t2 + 2*Power(t2,2) + Power(t2,3) + 
               t1*(-8 + 36*t2 - 7*Power(t2,2)) + 
               s2*(-5 - 32*t2 + 10*Power(t2,2) + 2*Power(t2,3)))))*
       B1(s,1 - s + s1 - t2,s1))/
     (Power(-1 + s1,2)*(-1 + t2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) - 
    (16*(Power(-1 + s,2)*t2*Power(-1 + s + t2,2) + 
         Power(s1,6)*(Power(s,2) - 3*(-1 + t2)*(-1 + 3*s2 - 3*t1 + t2) + 
            s*(-4 - 3*s2 + 3*t1 + 4*t2)) + 
         s1*(-1 + s + t2)*(-6 + t1 + 4*Power(s,4)*(-1 + t2) + 23*t2 - 
            3*s2*t2 - 19*Power(t2,2) + 2*s2*Power(t2,2) + 
            2*Power(t2,3) + Power(s,3)*
             (17 - t1 + 2*(-14 + s2)*t2 + 4*Power(t2,2)) + 
            Power(s,2)*(-28 + 3*t1 - 7*(-9 + s2)*t2 + 
               (-23 + 2*s2)*Power(t2,2)) + 
            s*(21 - 3*t1 + (-62 + 8*s2)*t2 + (34 - 4*s2)*Power(t2,2) - 
               2*Power(t2,3))) + 
         Power(s1,5)*(-2*Power(s,3) + 
            Power(s,2)*(7 + 8*s2 - 9*t1 - 6*t2) + 
            (-1 + t2)*(10 - 23*t1 - 13*t2 - 4*t1*t2 + 3*Power(t2,2) + 
               s2*(20 + 7*t2)) + 
            s*(29 + 10*t1 - 40*t2 - 25*t1*t2 + 11*Power(t2,2) + 
               3*s2*(-4 + 9*t2))) + 
         Power(s1,3)*(Power(s,4)*(5 - 2*s2 - 3*t1 + 2*t2) + 
            Power(s,3)*(-43 + 5*Power(t2,2) + s2*(8 + t2) - 
               t1*(4 + 7*t2)) + 
            2*s*(10 + 13*t2 - 18*Power(t2,2) - 5*Power(t2,3) - 
               t1*(7 + 2*t2) + s2*(2 + 6*t2 + Power(t2,2))) - 
            2*(-1 + t2)*(-20 + 9*t2 + 10*Power(t2,2) + Power(t2,3) + 
               t1*(-3 + 6*t2) - s2*(-8 + 10*t2 + Power(t2,2))) + 
            Power(s,2)*(34 - 47*t2 - 14*Power(t2,2) + 3*Power(t2,3) + 
               t1*(3 - 3*t2 - 4*Power(t2,2)) + 
               s2*(-2 + 3*t2 + 3*Power(t2,2)))) + 
         Power(s1,4)*(Power(s,4) + Power(s,3)*(-4 - 3*s2 + 9*t1) - 
            Power(s,2)*(23 + t1 - 41*t2 - 23*t1*t2 + 13*Power(t2,2) + 
               s2*(3 + 17*t2)) + 
            (-1 + t2)*(-44 + 43*t2 + Power(t2,2) + 2*t1*(7 + 6*t2) + 
               2*s2*(-1 - 13*t2 + Power(t2,2))) - 
            2*s*(6 + 16*t2 - 25*Power(t2,2) + 3*Power(t2,3) + 
               s2*(1 + 9*t2 + 3*Power(t2,2)) - 
               t1*(6 + 3*t2 + 4*Power(t2,2)))) - 
         Power(s1,2)*(Power(s,4)*(-13 - 4*t1 + 2*s2*(-1 + t2) + 15*t2) + 
            Power(s,3)*(34 + t1*(9 - 8*t2) - 60*t2 + 24*Power(t2,2) + 
               s2*(5 - 8*t2 + 4*Power(t2,2))) + 
            (-1 + t2)*(3 + t1*(7 - 4*t2) + 31*t2 - 34*Power(t2,2) + 
               s2*(-7 - 2*t2 + 6*Power(t2,2))) + 
            Power(s,2)*(-36 + 49*t2 - 49*Power(t2,2) + 11*Power(t2,3) + 
               t1*(-13 + 23*t2 - 4*Power(t2,2)) + 
               s2*(3 + t2 - 10*Power(t2,2) + 2*Power(t2,3))) + 
            s*(t1*(15 - 26*t2 + 8*Power(t2,2)) + 
               s2*(-13 + 10*t2 + 10*Power(t2,2) - 4*Power(t2,3)) + 
               2*(9 + 10*t2 - 18*Power(t2,2) - 2*Power(t2,3) + 
                  Power(t2,4)))))*R1q(s1))/
     (Power(-1 + s1,2)*s1*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2))*(-1 + t2)*Power(-1 + s + t2,3)) - 
    (16*(Power(s,6)*(-8 - 3*t1 + s1*(-4 + 3*t1 - 3*t2) + 19*t2) + 
         Power(s,5)*(43 + Power(s1,3) - 3*s2 + 8*t1 - 118*t2 + 5*s2*t2 - 
            15*t1*t2 + 95*Power(t2,2) + 
            Power(s1,2)*(12 + 4*s2 - 14*t1 + 9*t2) - 
            s1*(-48 + s2 + 59*t2 + 5*s2*t2 + 15*Power(t2,2) - 
               3*t1*(2 + 5*t2))) - 
         Power(s,4)*(106 + 3*Power(s1,4) + t1 - 300*t2 - 30*t1*t2 + 
            396*Power(t2,2) + 30*t1*Power(t2,2) - 190*Power(t2,3) + 
            Power(s1,3)*(18 + 15*s2 - 27*t1 + 4*t2) + 
            s2*(-16 + 37*t2 - 25*Power(t2,2)) + 
            Power(s1,2)*(83 + s2*(2 - 40*t2) - 49*t2 - 41*Power(t2,2) + 
               t1*(-5 + 66*t2)) + 
            s1*(114 - 351*t2 + 193*Power(t2,2) + 30*Power(t2,3) + 
               t1*(31 - 36*t2 - 30*Power(t2,2)) + 
               s2*(-1 + 3*t2 + 25*Power(t2,2)))) + 
         Power(s,3)*(143 + 3*Power(s1,5) - 27*s2 - 26*t1 + 
            Power(s1,4)*(20 + 21*s2 - 27*t1 - 11*t2) - 420*t2 + 
            95*s2*t2 + 17*t1*t2 + 645*Power(t2,2) - 118*s2*Power(t2,2) + 
            40*t1*Power(t2,2) - 564*Power(t2,3) + 50*s2*Power(t2,3) - 
            30*t1*Power(t2,3) + 190*Power(t2,4) + 
            Power(s1,3)*(30 + s2*(25 - 98*t2) + 19*t2 - 
               36*Power(t2,2) + 2*t1*(-16 + 59*t2)) + 
            Power(s1,2)*(157 - 319*t2 + 29*Power(t2,2) + 
               74*Power(t2,3) + t1*(31 + 11*t2 - 124*Power(t2,2)) + 
               s2*(2 - 43*t2 + 120*Power(t2,2))) + 
            s1*(111 - 581*t2 + 802*Power(t2,2) - 262*Power(t2,3) - 
               30*Power(t2,4) + 
               2*t1*(27 - 73*t2 + 42*Power(t2,2) + 15*Power(t2,3)) - 
               s2*(21 - 46*t2 + 2*Power(t2,2) + 50*Power(t2,3)))) - 
         Power(s,2)*(108 + Power(s1,6) - 18*s2 - 46*t1 + 
            Power(s1,5)*(14 + 13*s2 - 14*t1 - 13*t2) - 331*t2 + 
            97*s2*t2 + 106*t1*t2 + 508*Power(t2,2) - 
            189*s2*Power(t2,2) - 57*t1*Power(t2,2) - 565*Power(t2,3) + 
            162*s2*Power(t2,3) - 20*t1*Power(t2,3) + 376*Power(t2,4) - 
            50*s2*Power(t2,4) + 15*t1*Power(t2,4) - 95*Power(t2,5) + 
            Power(s1,4)*(-32 + 48*t2 - 11*Power(t2,2) - 
               7*s2*(-7 + 15*t2) + t1*(-49 + 107*t2)) + 
            Power(s1,3)*(47 + t1 + 67*t2 + 111*t1*t2 - 
               212*Power(t2,2) - 192*t1*Power(t2,2) + 74*Power(t2,3) + 
               2*s2*(5 - 67*t2 + 102*Power(t2,2))) + 
            Power(s1,2)*(141 - 520*t2 + 383*Power(t2,2) + 
               87*Power(t2,3) - 66*Power(t2,4) + 
               s2*(-17 + 18*t2 + 117*Power(t2,2) - 160*Power(t2,3)) + 
               t1*(64 - 137*t2 - 3*Power(t2,2) + 116*Power(t2,3))) + 
            s1*(29 - 355*t2 + 924*Power(t2,2) - 780*Power(t2,3) + 
               158*Power(t2,4) + 15*Power(t2,5) + 
               t1*(44 - 187*t2 + 252*Power(t2,2) - 96*Power(t2,3) - 
                  15*Power(t2,4)) + 
               s2*(-37 + 124*t2 - 132*Power(t2,2) - 2*Power(t2,3) + 
                  50*Power(t2,4)))) + 
         s*(Power(s1,6)*(4 + 3*s2 - 3*t1 - 4*t2) + 
            Power(s1,5)*(-22 + s2*(36 - 51*t2) + 26*t2 - 
               4*Power(t2,2) + t1*(-34 + 49*t2)) + 
            Power(s1,4)*(-52 + 197*t2 - 188*Power(t2,2) + 
               43*Power(t2,3) + t1*(-15 + 122*t2 - 133*Power(t2,2)) + 
               s2*(21 - 142*t2 + 147*Power(t2,2))) - 
            Power(s1,3)*(-77 + 57*t2 + 254*Power(t2,2) - 
               295*Power(t2,3) + 61*Power(t2,4) - 
               2*t1*(13 - 10*t2 - 63*Power(t2,2) + 69*Power(t2,3)) + 
               s2*(19 + 18*t2 - 193*Power(t2,2) + 174*Power(t2,3))) + 
            (-1 + t2)*(-44 + 92*t2 - 83*Power(t2,2) + 97*Power(t2,3) - 
               83*Power(t2,4) + 19*Power(t2,5) + 
               t1*(32 - 78*t2 + 56*Power(t2,2) - 3*Power(t2,3) - 
                  3*Power(t2,4)) + 
               s2*(4 - 34*t2 + 79*Power(t2,2) - 78*Power(t2,3) + 
                  25*Power(t2,4))) + 
            Power(s1,2)*(45 - 338*t2 + 530*Power(t2,2) - 
               141*Power(t2,3) - 125*Power(t2,4) + 29*Power(t2,5) + 
               t1*(46 - 169*t2 + 181*Power(t2,2) - 7*Power(t2,3) - 
                  54*Power(t2,4)) + 
               s2*(-17 + 75*t2 - 42*Power(t2,2) - 113*Power(t2,3) + 
                  100*Power(t2,4))) + 
            s1*(-24 + 301*Power(t2,2) - 561*Power(t2,3) + 
               318*Power(t2,4) - 31*Power(t2,5) - 3*Power(t2,6) + 
               s2*(-20 + 98*t2 - 185*Power(t2,2) + 130*Power(t2,3) + 
                  3*Power(t2,4) - 25*Power(t2,5)) + 
               t1*(12 - 92*t2 + 212*Power(t2,2) - 190*Power(t2,3) + 
                  54*Power(t2,4) + 3*Power(t2,5)))) + 
         (-1 + t2)*(3*Power(s1,6)*(-1 + 3*s2 - 3*t1 + t2) + 
            Power(s1,5)*(s2*(11 - 38*t2) + t1*(-8 + 35*t2) - 
               7*(3 - 5*t2 + 2*Power(t2,2))) + 
            Power(s1,4)*(-3 + 75*t2 - 96*Power(t2,2) + 24*Power(t2,3) + 
               t1*(7 + 20*t2 - 53*Power(t2,2)) + 
               s2*(-7 - 30*t2 + 63*Power(t2,2))) + 
            (-1 + t2)*(-8 - 4*(-2 + s2)*t2 + 4*(2 + 3*s2)*Power(t2,2) - 
               5*(1 + 3*s2)*Power(t2,3) + (-4 + 5*s2)*Power(t2,4) - 
               2*t1*(-4 + 10*t2 - 8*Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*(31 - 60*t2 - 55*Power(t2,2) + 102*Power(t2,3) - 
               18*Power(t2,4) + 
               s2*(-7 + 23*t2 + 31*Power(t2,2) - 53*Power(t2,3)) + 
               t1*(8 - 29*t2 - 10*Power(t2,2) + 37*Power(t2,3))) + 
            Power(s1,2)*(-4 - 57*t2 + 132*Power(t2,2) - 35*Power(t2,3) - 
               41*Power(t2,4) + 5*Power(t2,5) + 
               t1*(10 - 44*t2 + 61*Power(t2,2) - 14*Power(t2,3) - 
                  10*Power(t2,4)) + 
               s2*(-2 + 23*t2 - 35*Power(t2,2) - 13*Power(t2,3) + 
                  24*Power(t2,4))) - 
            s1*(12 + 10*(-4 + t1)*t2 + (7 - 38*t1)*Power(t2,2) + 
               (64 + 41*t1)*Power(t2,3) - 4*(10 + 3*t1)*Power(t2,4) - 
               3*Power(t2,5) + 
               s2*(4 - 18*t2 + 43*Power(t2,2) - 39*Power(t2,3) + 
                  4*Power(t2,4) + 5*Power(t2,5)))))*R1q(1 - s + s1 - t2))/
     (Power(-1 + s1,2)*Power(-s + s1 - t2,2)*(1 - s + s1 - t2)*(-1 + t2)*
       Power(-1 + s + t2,3)) + 
    (16*(Power(s,3)*(3*t1 - 11*t2 + s1*(4 - 3*t1 + 3*t2)) + 
         Power(-1 + s1,2)*(3 + Power(s1,3) + 5*t1 + 
            Power(s1,2)*(-2 - 2*s2 + 3*t1) - 17*t2 - 2*t1*t2 + 
            6*Power(t2,2) - s2*(1 + t2) + 
            s1*(18 + 2*t1*(-4 + t2) - 7*t2 - 2*Power(t2,2) + s2*(3 + t2))\
) + Power(s,2)*(5 + Power(s1,3) + s2 - t1 + t2 - s2*t2 - 2*t1*t2 + 
            6*Power(t2,2) - Power(s1,2)*(2 + 6*s2 - 9*t1 + 6*t2) + 
            s1*(-8 + 2*t1*(-4 + t2) + 5*t2 - 2*Power(t2,2) + s2*(5 + t2))\
) - s*(4 + 2*Power(s1,4) + t1*(7 - 4*t2) - 23*t2 - 2*s2*t2 + 
            12*Power(t2,2) - Power(s1,3)*(6 + 8*s2 - 9*t1 + 3*t2) + 
            s1*(42 - 4*s2 + 3*t1 - 73*t2 + 8*Power(t2,2)) + 
            Power(s1,2)*(38 - 19*t1 + 3*t2 + 4*t1*t2 - 4*Power(t2,2) + 
               2*s2*(6 + t2))))*R2q(s))/
     (s*Power(-1 + s1,2)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2))*(-1 + t2)) - 
    (8*(2*Power(s,6)*((-1 + s1)*t1 - (-5 + s1)*t2) - 
         Power(-1 + s1,4)*(-1 + 2*Power(s1,2) - 2*s1*(-1 + t2) - t2)*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2) + 
         Power(s,5)*(-2 + t1*(9 - 2*t2) - 41*t2 + 2*s2*t2 + 
            10*Power(t2,2) + 2*Power(s1,2)*(-2 + s2 - 6*t1 + 5*t2) + 
            s1*(14 + 3*t1 - 25*t2 + 2*t1*t2 - 2*Power(t2,2) - 
               2*s2*(1 + t2))) + 
         Power(s,4)*(7 + 5*t1*(-3 + t2) + 39*t2 - 5*s2*t2 - 
            21*Power(t2,2) + 2*s2*Power(t2,2) - 
            2*Power(s1,3)*(-8 + 5*s2 - 15*t1 + 10*t2) + 
            Power(s1,2)*(-28 + (11 - 10*t1)*t2 + 8*Power(t2,2) + 
               s2*(3 + 8*t2)) - 
            s1*(35 - 5*t1*(-3 + t2) - 26*t2 + 19*Power(t2,2) + 
               s2*(-7 + 3*t2 + 2*Power(t2,2)))) + 
         s*Power(-1 + s1,2)*(2 + 32*t2 - 2*s2*t2 - 44*Power(t2,2) + 
            9*s2*Power(t2,2) + 16*Power(t2,3) + 
            2*Power(s1,4)*(-2 + 5*s2 - 6*t1 + t2) - 
            t1*(3 + 2*t2 + 4*Power(t2,2)) + 
            Power(s1,3)*(s2 - 14*s2*t2 + t1*(3 + 10*t2) - 
               2*(6 - 11*t2 + Power(t2,2))) + 
            s1*(-35 + 66*t2 - 39*Power(t2,2) + 
               s2*(4 - 2*t2 - 9*Power(t2,2)) + 
               t1*(5 - 2*t2 + 4*Power(t2,2))) + 
            Power(s1,2)*(-19 + t1*(7 - 6*t2) + 30*t2 - 15*Power(t2,2) + 
               s2*(-11 + 10*t2 + 4*Power(t2,2)))) + 
         Power(s,3)*(-8 + 29*t2 - 14*Power(t2,2) - s2*Power(t2,2) + 
            12*Power(t2,3) + 4*Power(s1,4)*(-6 + 5*s2 - 10*t1 + 5*t2) - 
            2*t1*(-5 + 2*t2 + 2*Power(t2,2)) + 
            Power(s1,3)*(22 + 27*t2 - 12*Power(t2,2) + 
               10*t1*(1 + 2*t2) - s2*(7 + 16*t2)) + 
            s1*(9 + 161*t2 - 55*Power(t2,2) - 4*Power(t2,3) + 
               s2*(-6 + 4*t2 - 3*Power(t2,2)) + 
               2*t1*(5 - 2*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(-23 + 43*t2 + 9*Power(t2,2) - 
               2*t1*(-5 + 6*t2) + s2*(-7 + 12*t2 + 4*Power(t2,2)))) + 
         Power(s,2)*(2 + (-69 + 6*s2 + 2*t1)*t2 + 
            (60 - 9*s2 + 8*t1)*Power(t2,2) - 20*Power(t2,3) - 
            2*Power(s1,5)*(-8 + 10*s2 - 15*t1 + 5*t2) + 
            Power(s1,4)*(-6 - 39*t2 + 8*Power(t2,2) - 10*t1*(3 + 2*t2) + 
               s2*(19 + 20*t2)) + 
            Power(s1,3)*(41 - 20*t2 + 9*Power(t2,2) + t1*(-2 + 26*t2) + 
               s2*(6 - 36*t2 - 4*Power(t2,2))) + 
            s1*(37 - 142*t2 + 149*Power(t2,2) - 16*Power(t2,3) - 
               2*t1*(2 + t2) + 2*s2*(-2 - 4*t2 + Power(t2,2))) + 
            Power(s1,2)*(134 - 200*t2 + 62*Power(t2,2) + 4*Power(t2,3) + 
               t1*(6 - 6*t2 - 8*Power(t2,2)) + 
               s2*(-1 + 18*t2 + 11*Power(t2,2)))))*T2q(s1,s))/
     (s*Power(-1 + s1,2)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2))*(-1 + t2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) + 
    (8*(2*Power(s,6)*((-1 + s1)*t1 - (-5 + s1)*t2) - 
         Power(-1 + t2,3)*(1 + 2*Power(s1,3) + s1*(-3 + t2) + t2 - 
            2*Power(s1,2)*t2)*(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2) + 
         Power(s,5)*(-2 + t1*(9 - 8*t2) - 37*t2 + 2*s2*t2 + 
            40*Power(t2,2) + 2*Power(s1,2)*(-2 + s2 - 3*t1 + 2*t2) + 
            s1*(14 - 3*t1 + t2 + 8*t1*t2 - 8*Power(t2,2) - 2*s2*(1 + t2))\
) + Power(s,4)*(7 + 29*t2 - 5*s2*t2 - 118*Power(t2,2) + 
            8*s2*Power(t2,2) + 60*Power(t2,3) - 
            2*Power(s1,3)*(-2 + 2*s2 - 3*t1 + t2) + 
            t1*(-15 + 26*t2 - 12*Power(t2,2)) + 
            Power(s1,2)*(10 + 15*t1 - 38*t2 - 22*t1*t2 + 
               14*Power(t2,2) + 3*s2*(-1 + 4*t2)) - 
            s1*(37 - 43*t2 - 8*Power(t2,2) + 12*Power(t2,3) + 
               t1*(6 + 4*t2 - 12*Power(t2,2)) + 
               s2*(-7 + 7*t2 + 8*Power(t2,2)))) + 
         s*Power(-1 + t2,2)*(2 - 3*t1 + 9*t2 + 6*s2*t2 - 2*t1*t2 - 
            8*Power(t2,2) - 6*s2*Power(t2,2) + 3*t1*Power(t2,2) - 
            3*Power(t2,3) + 2*s2*Power(t2,3) + 
            Power(s1,4)*(-4 + 6*s2 - 6*t1 + 4*t2) + 
            2*Power(s1,3)*(s2*(6 - 4*t2) + t1*(-6 + 4*t2) - 
               3*(5 - 6*t2 + Power(t2,2))) + 
            2*Power(s1,2)*(-7 + 29*t2 - 23*Power(t2,2) + Power(t2,3) + 
               t1*(5 + 9*t2 - 2*Power(t2,2)) + 
               s2*(-7 - 7*t2 + 2*Power(t2,2))) + 
            s1*(-18 + 37*t2 - 36*Power(t2,2) + 17*Power(t2,3) + 
               t1*(11 - 24*t2 + Power(t2,2)) - 
               2*s2*(2 - 8*t2 - Power(t2,2) + Power(t2,3)))) - 
         2*Power(s,2)*(-1 + t2)*
          (1 - 3*Power(s1,4)*(s2 - t1) + (-3*s2 + 2*(-9 + t1))*t2 + 
            2*(1 + 5*s2 - 3*t1)*Power(t2,2) + 
            (20 - 4*s2 + t1)*Power(t2,3) - 5*Power(t2,4) + 
            Power(s1,3)*(19 + t1*(4 - 12*t2) - 24*t2 + 5*Power(t2,2) + 
               4*s2*(-1 + 3*t2)) - 
            Power(s1,2)*(-13 + t1 + 59*t2 + 12*t1*t2 - 51*Power(t2,2) - 
               9*t1*Power(t2,2) + 5*Power(t2,3) + 
               s2*(-7 - 8*t2 + 11*Power(t2,2))) + 
            s1*(15 - (31 + 17*s2)*t2 + (34 + s2)*Power(t2,2) + 
               (-19 + 4*s2)*Power(t2,3) + Power(t2,4) - 
               t1*(6 - 22*t2 + 3*Power(t2,2) + Power(t2,3)))) + 
         2*Power(s,3)*(-4 + Power(s1,4)*(s2 - t1) + 5*t1 + 14*t2 + 
            2*s2*t2 - 12*t1*t2 + 34*Power(t2,2) - 11*s2*Power(t2,2) + 
            14*t1*Power(t2,2) - 64*Power(t2,3) + 6*s2*Power(t2,3) - 
            4*t1*Power(t2,3) + 20*Power(t2,4) + 
            Power(s1,3)*(-11 + s2*(6 - 8*t2) + 14*t2 - 3*Power(t2,2) + 
               2*t1*(-4 + 5*t2)) + 
            Power(s1,2)*(-3 + 41*t2 - 47*Power(t2,2) + 9*Power(t2,3) + 
               t1*(-4 + 17*t2 - 15*Power(t2,2)) + 
               s2*(-4 - 7*t2 + 13*Power(t2,2))) + 
            s1*(6 - 13*t2 - 4*Power(t2,2) + 15*Power(t2,3) - 
               4*Power(t2,4) + 
               t1*(8 - 15*t2 + Power(t2,2) + 4*Power(t2,3)) - 
               s2*(3 - 13*t2 + 2*Power(t2,2) + 6*Power(t2,3)))))*
       T3q(1 - s + s1 - t2,s1))/
     (s*Power(-1 + s1,2)*(-1 + t2)*Power(-1 + s + t2,2)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) - 
    (8*(2*Power(s,5)*((-1 + s1)*t1 - (-5 + s1)*t2) + 
         (-1 + s1)*Power(s1 - t2,2)*
          (-1 + 2*Power(s1,2) - 2*s1*(-1 + t2) - t2)*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2) + 
         Power(s,4)*(-2 + t1*(3 - 6*t2) - 7*t2 + 2*s2*t2 + 
            30*Power(t2,2) + 2*Power(s1,2)*(-2 + s2 - 5*t1 + 4*t2) + 
            s1*(14 + 7*t1 - 25*t2 + 6*t1*t2 - 6*Power(t2,2) - 
               2*s2*(1 + t2))) + 
         Power(s,3)*(1 + (-32 + s2 + 7*t1)*t2 + 
            (-19 + 6*s2 - 6*t1)*Power(t2,2) + 30*Power(t2,3) - 
            4*Power(s1,3)*(-3 + 2*s2 - 5*t1 + 3*t2) + 
            s1*(9 + s2 - 9*t1 + 42*t2 - 15*s2*t2 + 17*t1*t2 - 
               47*Power(t2,2) - 6*s2*Power(t2,2) + 6*t1*Power(t2,2) - 
               6*Power(t2,3)) + 
            Power(s1,2)*(-30 - 11*t1 + 2*t2 - 24*t1*t2 + 
               18*Power(t2,2) + 7*s2*(1 + 2*t2))) - 
         s*(s1 - t2)*(16 - 16*t1 + 51*t2 - 4*s2*t2 + 24*t1*t2 - 
            80*Power(t2,2) - s2*Power(t2,2) - 7*t1*Power(t2,2) + 
            19*Power(t2,3) + 2*s2*Power(t2,3) + 
            2*Power(s1,4)*(-2 + 4*s2 - 5*t1 + t2) + 
            Power(s1,3)*(-6 + 18*t2 - 4*Power(t2,2) + 7*t1*(1 + 2*t2) - 
               3*s2*(1 + 6*t2)) + 
            Power(s1,2)*(19 - 16*t2 - 9*Power(t2,2) + 2*Power(t2,3) + 
               t1*(5 - 12*t2 - 4*Power(t2,2)) + 
               3*s2*(-3 + 4*t2 + 4*Power(t2,2))) + 
            s1*(-89 + 89*t2 - 3*Power(t2,2) - 5*Power(t2,3) + 
               t1*(14 - 26*t2 + 11*Power(t2,2)) + 
               s2*(4 + 10*t2 - 11*Power(t2,2) - 2*Power(t2,3)))) + 
         Power(s,2)*(1 - t1 + 12*t2 - 3*s2*t2 + 15*t1*t2 - 
            87*Power(t2,2) + s2*Power(t2,2) - 3*t1*Power(t2,2) + 
            7*Power(t2,3) + 6*s2*Power(t2,3) - 2*t1*Power(t2,3) + 
            10*Power(t2,4) + 4*Power(s1,4)*(-3 + 3*s2 - 5*t1 + 2*t2) + 
            Power(s1,3)*(12 + 33*t2 - 18*Power(t2,2) - 
               2*s2*(4 + 15*t2) + t1*(11 + 36*t2)) + 
            Power(s1,2)*(-4 - 47*t2 - 4*Power(t2,2) + 12*Power(t2,3) + 
               t1*(8 - 23*t2 - 18*Power(t2,2)) + 
               s2*(-7 + 27*t2 + 24*Power(t2,2))) + 
            s1*(-29 + 114*t2 + 21*Power(t2,2) - 27*Power(t2,3) - 
               2*Power(t2,4) + 
               s2*(3 + 6*t2 - 25*Power(t2,2) - 6*Power(t2,3)) + 
               t1*(2 - 28*t2 + 21*Power(t2,2) + 2*Power(t2,3)))))*
       T4q(-s + s1 - t2))/
     (s*Power(-1 + s1,2)*(-s + s1 - t2)*(-1 + t2)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) - 
    (8*(2*Power(s,4)*((-1 + s1)*t1 - (-5 + s1)*t2) - 
         4*(s1 - t2)*(-1 + t2)*
          (-1 + t1 - 5*t2 + s2*t2 - 2*t1*t2 + 6*Power(t2,2) + 
            Power(s1,2)*(-2 + s2 - t1 + 2*t2) - 
            s1*(-7 + s2 + 5*t2 + s2*t2 - 2*t1*t2 + 2*Power(t2,2))) + 
         Power(s,3)*(-2 + t1*(3 - 2*t2) - 7*t2 + 2*s2*t2 + 
            10*Power(t2,2) + 2*Power(s1,2)*(-2 + s2 - 3*t1 + 2*t2) + 
            s1*(14 + 3*t1 - 5*t2 + 2*t1*t2 - 2*Power(t2,2) - 
               2*s2*(1 + t2))) + 
         Power(s,2)*(1 + (-20 + s2 - t1)*t2 + (13 + 2*s2)*Power(t2,2) - 
            2*Power(s1,3)*(-2 + 2*s2 - 3*t1 + t2) + 
            s1*(5 + s2 - 3*t1 - 4*t2 - 7*s2*t2 + 5*t1*t2 + Power(t2,2) - 
               2*s2*Power(t2,2)) + 
            Power(s1,2)*(-2 - 3*t1 - 14*t2 - 4*t1*t2 + 2*Power(t2,2) + 
               s2*(3 + 6*t2))) + 
         s*(1 + 2*Power(s1,4)*(s2 - t1) - t1 + 12*t2 - 3*s2*t2 + 7*t1*t2 - 
            45*Power(t2,2) + 5*s2*Power(t2,2) - 8*t1*Power(t2,2) + 
            32*Power(t2,3) + 2*Power(s1,3)*
             (-5 + t1 + 5*t2 - 2*s2*t2 + t1*t2) + 
            Power(s1,2)*(t1 - 5*t1*t2 - 2*(-2 + t2 + Power(t2,2)) + 
               s2*(-5 + 7*t2 + 2*Power(t2,2))) - 
            s1*(19 + 4*(-13 + t1)*t2 + (25 - 8*t1)*Power(t2,2) + 
               8*Power(t2,3) + s2*(-3 + 7*Power(t2,2)))))*T5q(s))/
     (s*Power(-1 + s1,2)*(-1 + t2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))));
   return a;
};
