#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_1_m231_5_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(Power(t1,6)*(2 - 3*(-5 + s - s1)*t2) - 
         2*(-1 + s1 - t2)*Power(t2,2)*
          (-Power(s2,2) + s2*t2 + Power(t2,2)) + 
         Power(t1,4)*(-4 + (-47 + 10*Power(s,2) + 
               s*(1 - 2*s1*(-12 + s2) - 25*s2) + 
               2*Power(s1,2)*(-11 + s2) + 64*s2 + 
               s1*(13 - 15*s2 + Power(s2,2)))*t2 + 
            (86 - 10*Power(s,2) - 12*Power(s1,2) + s1*(47 - 7*s2) + 
               7*s2 + Power(s2,2) + s*(-101 + 22*s1 + 7*s2))*Power(t2,2) \
+ (20 - 4*s + 5*s1 + 7*s2)*Power(t2,3)) + 
         Power(t1,5)*(-2 + 6*Power(s1,2)*t2 + 
            (-25 + 2*Power(s,2) - 9*s2 + s*(40 + s2))*t2 + 
            (-34 + 6*s - 3*s2)*Power(t2,2) - 
            s1*(-2 + 2*(7 + 4*s)*t2 + 7*Power(t2,2))) + 
         Power(t1,3)*(6 - (-65 + 30*s + 4*Power(s,2) + 41*s2 - 
               16*s*s2 + 14*Power(s2,2))*t2 + 
            (34 - 5*s - 22*Power(s,2) - 96*s2 + 57*s*s2 + 5*Power(s2,2))*
             Power(t2,2) + (-120 + 75*s + 14*Power(s,2) + 24*s2 - 
               17*s*s2 + 2*Power(s2,2))*Power(t2,3) + 
            (2 + 2*s - 5*s2)*Power(t2,4) - 
            s1*(4 + (18 + 8*s - 29*s2 - 2*s*s2 + Power(s2,2))*t2 + 
               (-14 + 38*s + 35*s2 + 4*s*s2 - 8*Power(s2,2))*
                Power(t2,2) + (40 + 20*s - 2*s2)*Power(t2,3) + 
               Power(t2,4)) + 
            2*Power(s1,2)*t2*(9*s2 - 2*Power(s2,2) + t2*(20 + 3*t2))) + 
         Power(t1,2)*(-2 + (2 + 8*s*(-1 + s2) - 38*s2 + 24*Power(s2,2))*
             t2 + (-90 + 8*Power(s,2) + s*(76 - 48*s2) + 80*s2 + 
               6*Power(s2,2))*Power(t2,2) + 
            (53 + 14*Power(s,2) + 40*s2 - 20*Power(s2,2) - 
               s*(1 + 31*s2))*Power(t2,3) - 
            (-64 + 6*Power(s,2) + s*(7 - 9*s2) + 29*s2 + 3*Power(s2,2))*
             Power(t2,4) + (-3 - s + s2)*Power(t2,5) + 
            s1*(2 + (6 - 8*s*(-1 + s2) + 10*s2 - 10*Power(s2,2))*t2 + 
               2*(-12 + (5 + 6*s)*s2 - 4*Power(s2,2))*Power(t2,2) + 
               (-29 + 47*s2 + 3*Power(s2,2) + 6*s*(2 + s2))*
                Power(t2,3) + (7 + 6*s + 5*s2)*Power(t2,4)) - 
            2*Power(s1,2)*t2*(-4 + 2*t2 + 9*Power(t2,2) - 
               2*Power(s2,2)*(1 + t2) + s2*(6 + 14*t2 + Power(t2,2)))) + 
         t1*t2*(-10 + 4*t2 + 8*s*t2 + 39*Power(t2,2) - 46*s*Power(t2,2) - 
            4*Power(s,2)*Power(t2,2) - 52*Power(t2,3) + 5*s*Power(t2,3) - 
            2*Power(s,2)*Power(t2,3) - 7*Power(t2,4) - 7*s*Power(t2,4) + 
            2*Power(s1,2)*t2*
             (-4 + 6*s2 - 2*Power(s2,2) + 2*t2 + 5*s2*t2) + 
            Power(s2,2)*(-10 - 10*t2 + 20*Power(t2,2) + 3*Power(t2,3)) + 
            s2*(24 + (12 - 8*s)*t2 + (-65 + 32*s)*Power(t2,2) - 
               (-16 + s)*Power(t2,3) + 7*Power(t2,4)) + 
            s1*(Power(s2,2)*(10 - 2*t2 - 3*Power(t2,2)) - 
               s2*(24 - 8*(2 + s)*t2 + (15 + 14*s)*Power(t2,2) + 
                  21*Power(t2,3)) + 
               2*(5 - (7 + 4*s)*t2 + 4*(4 + s)*Power(t2,2) + 
                  (6 + s)*Power(t2,3))))))/
     ((-1 + s2)*Power(-1 + t1,2)*t1*Power(t1 - t2,2)*(-1 + s1 + t1 - t2)*
       (-1 + t2)*t2) - (8*(Power(-1 + s,2)*Power(t1,7) + 
         Power(s1,5)*(-1 + t1)*t1*((-2 + t1)*t1 - s2*(2 + t1)) + 
         t1*(3 + (-6 + 3*Power(s,2) + s*(9 - 13*s2) + 11*s2)*t2 + 
            (66 + 7*Power(s,2) + 31*s2 - 7*Power(s2,2) + 
               4*s*(-14 + 3*s2))*Power(t2,2) + 
            (62 + 3*Power(s,2) + 58*s2 - 13*Power(s2,2) + 
               s*(-84 + 8*s2))*Power(t2,3) + 
            (1 + 4*Power(s,2) + s*(3 - 9*s2) - 12*s2 + 5*Power(s2,2))*
             Power(t2,4) + 2*(-1 + s - s2)*Power(t2,5)) + 
         Power(t1,6)*(-4 + t2 - 2*s2*t2 - 2*Power(s,2)*(1 + t2) + 
            s*(6 + (3 + 2*s2)*t2)) + 
         Power(t1,5)*(1 + (-10 + 7*s2)*t2 + 
            (-1 - s2 + Power(s2,2))*Power(t2,2) + 
            Power(s,2)*(-4 + 3*t2) + 
            s*(4 + (2 - 7*s2)*t2 + (1 - 2*s2)*Power(t2,2))) + 
         Power(t1,4)*(8 + (25 + s2)*t2 + 
            (24 + 7*s2 - 3*Power(s2,2))*Power(t2,2) + 
            (-1 + 2*s2)*Power(t2,3) + 
            Power(s,2)*(2 + 17*t2 - Power(t2,2) + 2*Power(t2,3)) - 
            s*(12 + 2*(21 + s2)*t2 + (23 - 5*s2)*Power(t2,2) + 
               (3 + 2*s2)*Power(t2,3))) - 
         t2*(-15 - 21*t2 + 15*Power(t2,2) + 33*Power(t2,3) - 
            4*Power(t2,4) - Power(s2,2)*t2*(1 + 5*t2) + 
            Power(s,2)*t2*(3 + t2 + 2*Power(t2,2)) + 
            s2*(7 + 21*t2 + 37*Power(t2,2) + 7*Power(t2,3)) + 
            s*(-3 + (7 - 6*s2)*t2 - 3*(3 + 4*s2)*Power(t2,2) + 
               (-31 + 2*s2)*Power(t2,3) + 4*Power(t2,4))) + 
         Power(t1,2)*(-4 + (-25 + 8*s2)*t2 + 
            5*(-17 + 2*s2 + 2*Power(s2,2))*Power(t2,2) + 
            (20 + 11*s2 + 3*Power(s2,2))*Power(t2,3) + 
            2*(7 + 4*s2)*Power(t2,4) - 
            Power(s,2)*t2*(9 - 5*t2 + Power(t2,2) + Power(t2,3)) + 
            s*(6 + 20*t2 + (82 - 49*s2)*Power(t2,2) - 
               3*(4 + s2)*Power(t2,3) + (-11 + s2)*Power(t2,4))) + 
         Power(t1,3)*(-5 - 25*Power(t2,2) - 26*Power(t2,3) + 
            Power(s2,2)*Power(t2,2)*(-2 + t2 - Power(t2,2)) + 
            s2*t2*(-18 - 26*t2 - 14*Power(t2,2) + Power(t2,3)) + 
            Power(s,2)*(3 - 8*t2 - 16*Power(t2,2) + Power(t2,3) - 
               Power(t2,4)) + 
            s*(-2 + 5*(1 + 4*s2)*t2 + (47 + 20*s2)*Power(t2,2) + 
               (24 + s2)*Power(t2,3) + (1 + 2*s2)*Power(t2,4))) + 
         Power(s1,3)*(Power(t1,6) + Power(t1,5)*(11 - 5*s - 2*t2) + 
            2*t2*(1 + t2 - s*t2) + 
            Power(t1,4)*(-4 + Power(s,2) - 24*t2 + Power(t2,2) + 
               s*(-4 + 5*t2)) - 
            Power(s2,2)*(2 + t2 - 2*t1*(2 + t2) + 
               Power(t1,2)*(-2 + 5*t2)) + 
            Power(t1,3)*(-32 + 7*t2 - Power(s,2)*t2 + 7*Power(t2,2) + 
               s*(26 + 4*t2 - 2*Power(t2,2))) - 
            Power(t1,2)*(Power(s,2) - s*(2 - 16*t2 + Power(t2,2)) + 
               7*(3 - 10*t2 + Power(t2,2))) + 
            t1*(5 + 31*t2 + Power(s,2)*t2 + Power(t2,2) + 
               s*(-5 - 5*t2 + Power(t2,2))) + 
            s2*(-4 - 5*Power(t1,5) + 5*t2 + 3*s*t2 + 6*Power(t2,2) + 
               Power(t1,4)*(3 + 3*s + 4*t2) + 
               Power(t1,3)*(-14 - 7*s + 3*t2 + Power(t2,2)) + 
               Power(t1,2)*(5 + (-3 + 4*s)*t2 + 6*Power(t2,2)) + 
               t1*(-5 + s*(-4 + t2) + 7*t2 + 7*Power(t2,2)))) + 
         Power(s1,4)*(-2*Power(s2,2)*(-1 + t1) + 
            t1*(-2 + 2*Power(t1,4) + Power(t1,3)*(1 - 2*s - 2*t2) + 
               2*(-2 + s)*t2 + t1*(s + 4*(-3 + t2) - 3*s*t2) + 
               Power(t1,2)*(-11 + s + s*t2)) + 
            s2*((7 - 2*s)*Power(t1,2) - 4*Power(t1,4) - 2*t2 + 
               Power(t1,3)*(s + t2) + t1*(s - 5*(1 + t2)))) + 
         Power(s1,2)*(2 + (6 - 4*s)*Power(t1,6) + 
            Power(t1,5)*(12 + 3*Power(s,2) + 5*s*(-3 + t2) - 21*t2) - 
            5*t2 + s*t2 - 19*Power(t2,2) + 4*s*Power(t2,2) + 
            Power(s,2)*Power(t2,2) - 2*Power(t2,3) + 2*s*Power(t2,3) - 
            Power(s2,2)*(Power(t1,4)*t2 + 4*t2*(1 + t2) + 
               Power(t1,2)*(-8 + 11*t2 - 10*Power(t2,2)) + 
               t1*(4 - 3*t2 - 7*Power(t2,2)) + 
               Power(t1,3)*(4 - t2 + Power(t2,2))) - 
            Power(t1,4)*(71 + 27*t2 - 17*Power(t2,2) + 
               Power(s,2)*(3 + 4*t2) + s*(-36 - 39*t2 + 2*Power(t2,2))) \
+ t1*(7 + 21*t2 - 7*Power(s,2)*t2 - 111*Power(t2,2) + 10*Power(t2,3) + 
               s*(12 - 15*t2 + 52*Power(t2,2) - 6*Power(t2,3))) + 
            Power(t1,3)*(5 + 84*t2 + 49*Power(t2,2) - 4*Power(t2,3) + 
               Power(s,2)*(-2 + t2 + Power(t2,2)) + 
               s*(-5 - 30*t2 - 29*Power(t2,2) + Power(t2,3))) + 
            Power(t1,2)*(39 + 68*t2 - 56*Power(t2,2) - 4*Power(t2,3) + 
               2*Power(s,2)*(3 + t2 + Power(t2,2)) + 
               s*(20 - 94*t2 + 23*Power(t2,2) + 5*Power(t2,3))) - 
            s2*(-4 + 2*Power(t1,6) + (-11 + 4*s)*t2 + 
               (14 + s)*Power(t2,2) + 6*Power(t2,3) - 
               Power(t1,5)*(3 + 3*s + 4*t2) + 
               Power(t1,4)*(4 + s*(7 - 3*t2) + 10*t2 - Power(t2,2)) + 
               Power(t1,3)*(-22 - 28*t2 - 5*Power(t2,2) + Power(t2,3) + 
                  2*s*(-4 + 5*t2)) + 
               Power(t1,2)*(34 - 55*t2 + 15*Power(t2,2) + 
                  8*Power(t2,3) + 3*s*(5 - 10*t2 + 4*Power(t2,2))) + 
               t1*(-11 + 28*t2 + 13*Power(t2,2) + 9*Power(t2,3) + 
                  s*(-3 - 13*t2 + 11*Power(t2,2))))) + 
         s1*(-2 - (1 + s)*Power(t1,7) + 
            (-10 + s*(-4 + s2) - 11*s2 + 7*Power(s2,2))*t2 + 
            (2*Power(s,2) - 15*s*(-1 + s2) + s2*(27 + s2))*Power(t2,2) + 
            (52 - 37*s + Power(s,2) + 16*s2 + 3*Power(s2,2))*
             Power(t2,3) + 2*(-2 + 2*s + s2)*Power(t2,4) + 
            Power(t1,6)*(12 + 3*Power(s,2) + s2 + s2*t2 + 
               s*(-12 + s2 + t2)) + 
            t1*(-15 - 62*t2 - 21*Power(t2,2) + 81*Power(t2,3) - 
               5*Power(t2,4) + 
               Power(s2,2)*t2*(-13 + 6*t2 - 12*Power(t2,2)) - 
               Power(s,2)*t2*(-3 + 7*t2 + Power(t2,2)) + 
               s*(-7 + (-11 + 19*s2)*t2 + (65 - 21*s2)*Power(t2,2) + 
                  2*(-28 + 9*s2)*Power(t2,3) + Power(t2,4)) + 
               s2*(1 + 43*t2 - 14*Power(t2,2) + 23*Power(t2,3) + 
                  7*Power(t2,4))) + 
            Power(t1,5)*(-25 - 28*t2 - Power(s2,2)*t2 + Power(t2,2) - 
               5*Power(s,2)*(1 + t2) - 3*s2*(1 + 3*t2) + 
               s*(34 + 29*t2 + Power(t2,2) + s2*(-2 + 5*t2))) + 
            Power(t1,4)*(-6 + 12*t2 + 46*Power(t2,2) + 
               Power(s2,2)*t2*(3 + t2) + 
               Power(s,2)*(4 + t2 + Power(t2,2)) + 
               s2*(2 + 23*t2 + 6*Power(t2,2) - Power(t2,3)) - 
               s*(-15 + 28*t2 + 27*Power(t2,2) + Power(t2,3) + 
                  s2*(8 + 15*t2 + 3*Power(t2,2)))) + 
            Power(t1,3)*(25 + 154*t2 - 4*Power(t2,2) - 29*Power(t2,3) + 
               2*Power(s2,2)*t2*(-1 - t2 + Power(t2,2)) + 
               s2*(2 + 6*t2 + 6*Power(t2,2) - 9*Power(t2,3)) + 
               Power(s,2)*(-1 - 2*t2 + 9*Power(t2,2) + Power(t2,3)) - 
               s*(10 + 122*t2 + 22*Power(t2,2) - 13*Power(t2,3) + 
                  s2*(-2 - 36*t2 + 4*Power(t2,2) + 3*Power(t2,3)))) + 
            Power(t1,2)*(12 - 66*t2 - 142*Power(t2,2) - 28*Power(t2,3) + 
               5*Power(t2,4) + 
               Power(s2,2)*t2*(6 + 6*t2 - 5*Power(t2,2)) - 
               Power(s,2)*(5 - 7*t2 + Power(t2,2) + 5*Power(t2,3)) + 
               s2*(-3 - 53*t2 - 85*Power(t2,2) + 3*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s*(-19 + 47*t2 + 114*Power(t2,2) + 21*Power(t2,3) - 
                  3*Power(t2,4) + 
                  s2*(7 - 30*t2 + 3*Power(t2,2) + 9*Power(t2,3))))))*
       B1(1 - s1 - t1 + t2,t1,t2))/
     ((-1 + s2)*(-1 + t1)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)) \
+ (16*(Power(-1 + s,2)*Power(t1,9) + 
         Power(t2,3)*(1 + t2)*(-Power(s2,2) + s2*t2 + Power(t2,2)) + 
         Power(t1,5)*(3 + (8 + s - 4*Power(s,2) + 23*s2 - 4*s*s2 - 
               14*Power(s2,2))*t2 - 
            (66 + 14*Power(s,2) + 6*s2 + Power(s2,2) + 6*s*(-7 + 2*s2))*
             Power(t2,2) + (-86 + 10*Power(s,2) + s*(87 - 22*s2) - 
               47*s2 + 9*Power(s2,2))*Power(t2,3) - 
            (4 + 3*Power(s,2) + 11*s2 + Power(s2,2) - 4*s*(3 + s2))*
             Power(t2,4) + Power(t2,5)) - 
         Power(t1,8)*(6 + (-4 + s2)*t2 + Power(t2,2) + 
            Power(s,2)*(4 + 3*t2) - s*(10 + (3 + 2*s2)*t2)) + 
         Power(t1,7)*(12 + (-2 + s2)*t2 + 
            (-12 - 3*s2 + Power(s2,2))*Power(t2,2) + 3*Power(t2,3) + 
            Power(s,2)*(3 + 13*t2 + 2*Power(t2,2)) - 
            s*(14 + (7 + 12*s2)*t2 + 4*(-1 + s2)*Power(t2,2))) + 
         Power(t1,6)*(-10 + (-10 - 7*s2 + 5*Power(s2,2))*t2 + 
            (46 + 14*s2 - 5*Power(s2,2))*Power(t2,2) + 
            (8 + 11*s2 - Power(s2,2))*Power(t2,3) - 3*Power(t2,4) + 
            2*Power(s,2)*t2*(-1 - 8*t2 + Power(t2,2)) + 
            s*(6 + (12 + 11*s2)*t2 + (-47 + 30*s2)*Power(t2,2) - 
               12*Power(t2,3))) + 
         t1*Power(t2,2)*(Power(s2,2)*
             (4 + 9*t2 + Power(t2,2) - 3*Power(t2,3)) + 
            t2*(-1 + (-2 + s)*t2 + (-11 + 5*s)*Power(t2,2) + 
               (-9 + 4*s)*Power(t2,3)) + 
            s2*(-2 + 2*(-2 + s)*t2 + (-13 + 3*s)*Power(t2,2) + 
               (-16 + s)*Power(t2,3) - 6*Power(t2,4))) + 
         Power(t1,2)*t2*(6 + (3 - 4*s)*t2 - 3*(-4 + s)*Power(t2,2) + 
            4*(10 - 5*s + Power(s,2))*Power(t2,3) + 
            (41 - 18*s + 2*Power(s,2))*Power(t2,4) + 
            (15 - 2*s)*Power(t2,5) + 
            Power(s2,2)*(3 - 10*t2 - 19*Power(t2,2) + 4*Power(t2,3) + 
               5*Power(t2,4)) + 
            s2*(-10 - t2 + (25 - 13*s)*Power(t2,2) + 
               (55 - 13*s)*Power(t2,3) + (47 - 5*s)*Power(t2,4) + 
               4*Power(t2,5))) - 
         Power(s1,3)*Power(t1,2)*(t1 - t2)*
          (2*Power(t1,5) + Power(t1,4)*(-11 + 3*s2 - 4*t2) + 
            Power(t1,2)*(5 + s2 - Power(s2,2)*(-2 + t2) - 2*t2 + 
               5*s2*t2 - 6*Power(t2,2)) - 
            Power(t1,3)*(-4 + Power(s2,2) - 15*t2 - 2*Power(t2,2) + 
               s2*(3 + t2)) + 
            t1*(-4 + s2*(3 - 15*t2) - t2 - 2*Power(t2,2) + 
               Power(s2,2)*(-1 + 2*t2)) + 
            t2*(-Power(s2,2) + 2*t2 + s2*(3 + 4*t2))) + 
         Power(t1,4)*t2*(8 + 52*t2 + 128*Power(t2,2) + 83*Power(t2,3) + 
            6*Power(t2,4) + Power(s,2)*t2*
             (12 + 24*t2 - 4*Power(t2,2) + Power(t2,3)) + 
            Power(s2,2)*(16 + 9*t2 - 13*Power(t2,2) - Power(t2,3) + 
               Power(t2,4)) + 
            s2*(-34 - 23*t2 + 82*Power(t2,2) + 54*Power(t2,3) + 
               6*Power(t2,4)) - 
            s*(13 + 18*t2 + 78*Power(t2,2) + 57*Power(t2,3) + 
               7*Power(t2,4) + 
               s2*(-5 + 15*t2 + 10*Power(t2,2) - 2*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         Power(t1,3)*t2*(-14 - 22*t2 - 68*Power(t2,2) - 
            102*Power(t2,3) - 50*Power(t2,4) - 3*Power(t2,5) + 
            Power(s,2)*Power(t2,2)*(-12 - 13*t2 + Power(t2,2)) + 
            Power(s2,2)*(-10 + 2*t2 + 16*Power(t2,2) - 2*Power(t2,3) - 
               3*Power(t2,4)) - 
            s2*(-28 - 21*t2 + 63*Power(t2,2) + 98*Power(t2,3) + 
               26*Power(t2,4) + 2*Power(t2,5)) + 
            s*(4 + 15*t2 + 26*Power(t2,2) + 52*Power(t2,3) + 
               16*Power(t2,4) + 2*Power(t2,5) + 
               s2*(-2 + 5*t2 + 31*Power(t2,2) + 16*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         Power(s1,2)*t1*(-2*Power(t1,8) + 
            Power(t1,7)*(12 + s - 4*s2 + 7*t2) - 
            Power(t2,2)*(Power(t2,2) + Power(s2,2)*(1 + t2) + 
               2*s2*t2*(1 + 2*t2)) + 
            Power(t1,6)*(-9 + Power(s2,2) + s*(-6 + 2*s2 - 7*t2) - 
               44*t2 - 10*Power(t2,2) + s2*(8 + 15*t2)) + 
            Power(t1,2)*(6 - (7 + 4*s)*t2 + (21 + s)*Power(t2,2) - 
               12*(-2 + s)*Power(t2,3) + (23 - 2*s)*Power(t2,4) - 
               Power(s2,2)*(-3 + 12*t2 + 9*Power(t2,2) + Power(t2,3)) + 
               s2*(-10 + 10*t2 + (4 + s)*Power(t2,2) + 
                  (55 + s)*Power(t2,3) + 4*Power(t2,4))) - 
            Power(t1,5)*(4 - 2*t2 - 47*Power(t2,2) - 7*Power(t2,3) + 
               4*Power(s2,2)*(1 + t2) + 
               s2*(7 + 18*t2 + 14*Power(t2,2)) + 
               s*(-2 - 20*t2 - 13*Power(t2,2) + s2*(3 + 2*t2))) + 
            t1*t2*(Power(s2,2)*(4 + 7*t2 + Power(t2,2)) - 
               s2*(2 + t2 - 2*s*t2 + (-2 + 3*s)*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t2*(3 - 9*Power(t2,2) + s*t2*(1 + 4*t2))) + 
            Power(t1,4)*(5 + 55*t2 + 29*Power(t2,2) - 16*Power(t2,3) - 
               2*Power(t2,4) + 
               Power(s2,2)*(6 + 10*t2 - 4*Power(t2,2)) + 
               s2*(-4 + 5*t2 + 42*Power(t2,2) + 5*Power(t2,3)) + 
               s*(-9 + 4*t2 - 28*Power(t2,2) - 9*Power(t2,3) + 
                  s2*(7 - 5*t2 + 2*Power(t2,2)))) + 
            Power(t1,3)*(-8 - 25*t2 - 54*Power(t2,2) - 51*Power(t2,3) + 
               Power(t2,4) + Power(s2,2)*
                (-6 + 2*t2 + 7*Power(t2,2) + Power(t2,3)) + 
               s2*(17 + 2*t2 - 67*Power(t2,2) - 24*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s*(4 + 7*t2 + 2*Power(t2,2) + 16*Power(t2,3) + 
                  2*Power(t2,4) - 
                  s2*(2 + 5*t2 - 7*Power(t2,2) + 2*Power(t2,3))))) - 
         s1*(-(Power(t1,9)*(-5 + s - s2 + t2)) + 
            Power(t2,3)*(-Power(s2,2) + s2*t2 + Power(t2,2)) - 
            Power(t1,8)*(10 + Power(s,2) + s*(-17 + 2*s2 - 4*t2) + 
               13*t2 - 3*Power(t2,2) + s2*(11 + 5*t2)) + 
            Power(t1,7)*(6 + 7*Power(s,2) + 56*t2 + 14*Power(t2,2) - 
               3*Power(t2,3) + Power(s2,2)*(5 + t2) + 
               s*(-20 - 3*s2 - 58*t2 + 6*s2*t2 - 8*Power(t2,2)) + 
               s2*(17 + 26*t2 + 9*Power(t2,2))) - 
            t1*Power(t2,2)*(Power(s2,2)*(-3 - 2*t2 + 4*Power(t2,2)) + 
               t2*(1 - (-2 + s)*t2 + (10 - 4*s)*Power(t2,2)) + 
               s2*(2 - 2*(-2 + s)*t2 - (-16 + s)*Power(t2,2) + 
                  10*Power(t2,3))) + 
            Power(t1,2)*t2*(6 - 2*(1 + 2*s)*t2 + 
               (25 - 2*s)*Power(t2,2) + 
               (13 - 7*s + 2*Power(s,2))*Power(t2,3) + 
               2*(4 + s)*Power(t2,4) + 
               Power(s2,2)*(7 + 3*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(-12 + (11 + 2*s)*t2 + (21 - 8*s)*Power(t2,2) + 
                  (44 - 8*s)*Power(t2,3) - 2*Power(t2,4))) - 
            Power(t1,3)*(-6 + 9*t2 + (25 - 16*s)*Power(t2,2) + 
               4*(2 + 2*s + Power(s,2))*Power(t2,3) + 
               (-11 + 2*s + Power(s,2))*Power(t2,4) - 18*Power(t2,5) + 
               Power(s2,2)*(-3 + 16*t2 + 20*Power(t2,2) - Power(t2,3) + 
                  2*Power(t2,4)) - 
               s2*(-10 - 2*(-6 + s)*t2 + (4 - 6*s)*Power(t2,2) + 
                  2*(-20 + 9*s)*Power(t2,3) + (22 + 3*s)*Power(t2,4) + 
                  2*Power(t2,5))) + 
            Power(t1,6)*(-2 - 71*t2 - 128*Power(t2,2) - 14*Power(t2,3) + 
               Power(t2,4) + Power(s2,2)*(-14 - 12*t2 + Power(t2,2)) + 
               Power(s,2)*(-2 - 20*t2 + 6*Power(t2,2)) - 
               s2*(-3 + 30*t2 + 28*Power(t2,2) + 9*Power(t2,3)) + 
               s*(9 + 48*t2 + 72*Power(t2,2) + 10*Power(t2,3) + 
                  s2*(2 + 22*t2 - 10*Power(t2,2)))) + 
            Power(t1,5)*(7 + 38*t2 + 133*Power(t2,2) + 136*Power(t2,3) + 
               13*Power(t2,4) + 
               2*Power(s,2)*t2*(2 + 9*t2 - 4*Power(t2,2)) - 
               Power(s2,2)*(-16 - 15*t2 + Power(t2,2) + 3*Power(t2,3)) + 
               s2*(-28 - 10*t2 + 53*Power(t2,2) + 32*Power(t2,3) + 
                  6*Power(t2,4)) - 
               s*(9 + 4*t2 + 42*Power(t2,2) + 38*Power(t2,3) + 
                  7*Power(t2,4) + 
                  s2*(-5 + 26*t2 + 24*Power(t2,2) - 10*Power(t2,3)))) + 
            Power(t1,4)*(-12 - 6*t2 - 7*Power(t2,2) - 99*Power(t2,3) - 
               72*Power(t2,4) - 5*Power(t2,5) + 
               Power(s,2)*Power(t2,3)*(-4 + 3*t2) + 
               Power(s2,2)*(-10 + 5*t2 + 17*Power(t2,2) - 
                  2*Power(t2,3) + Power(t2,4)) - 
               s2*(-28 - 19*t2 + 35*Power(t2,2) + 36*Power(t2,3) + 
                  21*Power(t2,4) + 2*Power(t2,5)) + 
               s*(4 - 6*t2 + 6*Power(t2,2) + 14*Power(t2,3) + 
                  7*Power(t2,4) + 2*Power(t2,5) + 
                  2*s2*(-1 + 4*t2 + 7*Power(t2,2) + Power(t2,3) - 
                     2*Power(t2,4))))))*R1(t1))/
     ((-1 + s2)*Power(-1 + t1,3)*t1*Power(t1 - t2,3)*(-1 + s1 + t1 - t2)*
       (s1*t1 - t2)*(-1 + t2)) - 
    (16*(Power(s1,5)*t1*(t1 - t2)*t2*
          (Power(t1,3) + (2 + 2*Power(s2,2) + s2*(-6 + t2) - 2*t2)*t2 - 
            Power(t1,2)*(-2 + s2 + 2*t2) + 
            t1*(2 - 2*(-1 + s2)*t2 + Power(t2,2))) + 
         Power(s1,4)*(3*Power(t1,6)*t2 + 
            (2 + s2*(-6 + t2) - 2*t2)*Power(t2,4) - 
            Power(t1,5)*(-1 + (-4 + s + 3*s2)*t2 + 14*Power(t2,2)) + 
            Power(t1,4)*(1 + (4 + s2)*t2 + 
               (-1 + 2*s - s2)*Power(t2,2) + 23*Power(t2,3)) - 
            Power(t1,3)*(1 + t2 - 2*s*t2 + 
               (10 - 2*s*(-1 + s2) + 19*s2 - 5*Power(s2,2))*
                Power(t2,2) + 2*(9 + s - 7*s2)*Power(t2,3) + 
               16*Power(t2,4)) + 
            t1*Power(t2,2)*(3 - 2*(3 + s)*t2 + 2*(9 + s)*Power(t2,2) - 
               (6 + s)*Power(t2,3) + 
               Power(s2,2)*(6 + t2 + 2*Power(t2,2)) + 
               s2*(-10 + 2*(7 + 2*s)*t2 - (29 + 2*s)*Power(t2,2) + 
                  Power(t2,3))) + 
            Power(t1,2)*t2*(4 + 4*t2 - 17*Power(t2,2) + 
               (21 + 2*s)*Power(t2,3) + 4*Power(t2,4) - 
               Power(s2,2)*t2*(1 + 13*t2) - 
               s2*(2 + 4*(2 + s)*t2 - 58*Power(t2,2) + 11*Power(t2,3)))) \
+ Power(s1,3)*(3*Power(t1,7)*t2 - 
            3*Power(t1,6)*(-1 + s*t2 + s2*t2 + 7*Power(t2,2)) + 
            Power(t1,5)*(2 + (-7 + 3*s2)*t2 + 
               (6 + 8*s + s2)*Power(t2,2) + 46*Power(t2,3)) - 
            Power(t1,4)*(4 - 2*(-6 + s + 2*s2)*t2 + 
               (3 + s*(2 - 6*s2) + 16*s2 - 4*Power(s2,2))*Power(t2,2) + 
               5*(7 + 3*s - 5*s2)*Power(t2,3) + 44*Power(t2,4)) + 
            Power(t1,3)*(1 + (11 - 2*s - 6*s2)*t2 + 
               (31 - 30*s2 - 6*Power(s2,2) - 2*s*(-6 + 5*s2))*
                Power(t2,2) - 
               (68 - 119*s2 + 24*Power(s2,2) + s*(7 + 2*s2))*
                Power(t2,3) + (41 + 23*s - 35*s2)*Power(t2,4) + 
               19*Power(t2,5)) + 
            Power(t2,3)*(-3 + 2*(3 + s)*t2 - 2*(7 + s)*Power(t2,2) + 
               (7 + s)*Power(t2,3) + 
               Power(s2,2)*(-6 + t2 + 3*Power(t2,2)) - 
               s2*(-10 + 4*(1 + s)*t2 + 2*(-13 + s)*Power(t2,2) + 
                  Power(t2,3))) - 
            t1*Power(t2,2)*(7 + (5 - 2*s)*t2 - (29 + 4*s)*Power(t2,2) + 
               (49 + 5*s)*Power(t2,3) + (1 - 5*s)*Power(t2,4) + 
               2*Power(s2,2)*(3 + 4*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               2*s2*(-6 - 2*t2 + (42 - 5*s)*Power(t2,2) - 
                  (18 + s)*Power(t2,3) + 2*Power(t2,4))) + 
            Power(t1,2)*t2*(-3 - 2*(-2 + s)*t2 - 
               8*(3 + 2*s)*Power(t2,2) + (105 + 13*s)*Power(t2,3) - 
               (11 + 18*s)*Power(t2,4) - 3*Power(t2,5) + 
               Power(s2,2)*t2*(19 + 5*t2 + 10*Power(t2,2)) + 
               s2*(2 + 2*(-9 + 2*s)*t2 + 2*(30 + s)*Power(t2,2) - 
                  3*(39 + 2*s)*Power(t2,3) + 16*Power(t2,4)))) - 
         t2*(Power(t1,7)*(1 + (-3 + s - Power(s,2))*t2 - 
               2*Power(t2,2)) - 
            Power(t1,5)*(2 + (11 - 5*s - 3*s2)*t2 + 
               (8*Power(s,2) + 5*(-5 + s2) - s*(9 + 10*s2))*
                Power(t2,2) + 
               (3 + 2*Power(s,2) - 4*s*(-2 + s2) - s2 + Power(s2,2))*
                Power(t2,3) + 15*Power(t2,4)) + 
            Power(t1,4)*(1 + 2*(8 + s - 3*s2)*t2 + 
               (15 + 13*s - 14*Power(s,2) + 6*s2 - 9*Power(s2,2))*
                Power(t2,2) + 
               (-78 + 23*s + 8*Power(s,2) - 4*s2 - 20*s*s2 + 
                  2*Power(s2,2))*Power(t2,3) + 
               (-16 + 18*s - 2*Power(s,2) - 13*s2 + Power(s2,2))*
                Power(t2,4) + 11*Power(t2,5)) - 
            Power(t1,2)*t2*(4 + (27 - 8*s - 18*s2 + 6*Power(s2,2))*t2 + 
               (25 + 24*Power(s,2) + s*(4 - 68*s2) - 26*s2 + 
                  11*Power(s2,2))*Power(t2,2) + 
               (48*Power(s,2) - s*(29 + 84*s2) + 
                  4*(6 + 29*s2 + 3*Power(s2,2)))*Power(t2,3) + 
               (167 - 49*s - 11*Power(s,2) + 41*s2 + 6*Power(s2,2))*
                Power(t2,4) + 
               (33 + Power(s,2) + 13*s2 + Power(s2,2) - 2*s*(7 + s2))*
                Power(t2,5)) + 
            Power(t1,6)*t2*(-3 + s2 + 7*t2 + 2*s2*t2 + 9*Power(t2,2) + 
               3*Power(s,2)*(1 + t2) - 2*s*(4 + s2*t2)) + 
            Power(t1,3)*t2*(4 + (-15 - 14*s + 8*Power(s,2))*t2 + 
               (30 - 45*s + 44*Power(s,2))*Power(t2,2) + 
               (142 - 53*s - 8*Power(s,2))*Power(t2,3) + 
               (38 - 21*s + 3*Power(s,2))*Power(t2,4) - 3*Power(t2,5) + 
               Power(s2,2)*t2*(15 + 19*t2 + Power(t2,3)) + 
               s2*(2 - 4*(4 + 3*s)*t2 - (5 + 38*s)*Power(t2,2) + 
                  (27 + 8*s)*Power(t2,3) + (19 - 4*s)*Power(t2,4))) + 
            2*Power(t2,3)*(6 - 2*(3 + 2*s)*t2 + 
               (11 - 3*s - 4*Power(s,2))*Power(t2,2) - 
               (-16 + 3*s + Power(s,2))*Power(t2,3) + 
               (-15 + 4*s + Power(s,2))*Power(t2,4) + 
               Power(s2,2)*(12 + 14*t2 - 8*Power(t2,2) - 
                  7*Power(t2,3) + Power(t2,4)) - 
               s2*(20 - 8*(-1 + s)*t2 + (13 - 12*s)*Power(t2,2) + 
                  (39 - 6*s)*Power(t2,3) + 2*(3 + s)*Power(t2,4))) + 
            2*t1*Power(t2,2)*(8 + 14*t2 + 
               (-10 + 11*s + 12*Power(s,2))*Power(t2,2) + 
               2*(-9 + s + 5*Power(s,2))*Power(t2,3) + 
               (55 - 14*s - 4*Power(s,2))*Power(t2,4) + 
               (5 - 2*s)*Power(t2,5) + 
               Power(s2,2)*t2*
                (-14 - 6*t2 + 11*Power(t2,2) + Power(t2,3)) + 
               s2*(-4 + (4 - 8*s)*t2 - 5*(-7 + 8*s)*Power(t2,2) + 
                  (89 - 29*s)*Power(t2,3) + (17 + 3*s)*Power(t2,4) + 
                  2*Power(t2,5)))) - 
         s1*(Power(t1,8)*(-1 + (2 + s)*t2 + 2*Power(t2,2)) - 
            Power(t1,7)*t2*(-8 + s2 + (5 + 4*s)*t2 + 9*Power(t2,2)) + 
            Power(t1,6)*(2 + (7 + s - 3*s2)*t2 - 
               (35 + 2*Power(s,2) + 2*s*(-3 + s2) + 6*s2)*Power(t2,2) + 
               (-8 + 8*s - 3*s2)*Power(t2,3) + 15*Power(t2,4)) + 
            Power(t1,5)*(-1 - 2*(6 + s - 3*s2)*t2 + 
               (-28 + 9*Power(s,2) + 11*s2 + 9*Power(s2,2) - 
                  s*(17 + 6*s2))*Power(t2,2) + 
               (91 + 3*Power(s,2) + 17*s2 + 3*Power(s2,2) + 
                  s*(-21 + 2*s2))*Power(t2,3) + 
               (50 - 14*s + 11*s2)*Power(t2,4) - 11*Power(t2,5)) + 
            Power(t1,3)*t2*(6 + (39 - 8*s - 28*s2 + 6*Power(s2,2))*t2 + 
               (50 + 20*Power(s,2) + s*(35 - 58*s2) - 57*s2)*
                Power(t2,2) + 
               (-70 + 22*Power(s,2) + s*(27 - 70*s2) + 135*s2)*
                Power(t2,3) + 
               (189 - 10*Power(s,2) + s2 + 4*Power(s2,2) + 
                  2*s*(-7 + 2*s2))*Power(t2,4) + 
               (59 - 14*s + 13*s2)*Power(t2,5)) + 
            Power(t1,4)*t2*(-9 + (7 + 3*s - 8*Power(s,2))*t2 + 
               (13 + 17*s - 24*Power(s,2))*Power(t2,2) + 
               2*(-85 + 14*s + 2*Power(s,2))*Power(t2,3) + 
               (-82 + 19*s)*Power(t2,4) + 3*Power(t2,5) - 
               Power(s2,2)*t2*(15 + 25*t2 + 6*Power(t2,2)) + 
               s2*(-2 + (17 + 12*s)*t2 + (-3 + 48*s)*Power(t2,2) - 
                  6*Power(t2,3) - 17*Power(t2,4))) + 
            2*Power(t2,4)*(-6 + 2*(4 + s)*t2 + 
               (-31 + 6*s + 2*Power(s,2))*Power(t2,2) + 
               (24 - 5*s - 2*Power(s,2))*Power(t2,3) + 
               (1 - 2*s)*Power(t2,4) - 
               Power(s2,2)*(12 - 2*t2 - 9*Power(t2,2) + Power(t2,3)) + 
               s2*(20 - 2*(3 + 4*s)*t2 - 9*(-5 + s)*Power(t2,2) + 
                  3*(2 + s)*Power(t2,3) + 2*Power(t2,4))) + 
            t1*Power(t2,3)*(-34 + 2*(5 + 12*s)*t2 + 
               (46 - 37*s - 4*Power(s,2))*Power(t2,2) + 
               3*(-66 + 14*s + 3*Power(s,2))*Power(t2,3) + 
               (22 + 11*s - Power(s,2))*Power(t2,4) - 
               Power(s2,2)*(36 + 46*t2 - 2*Power(t2,2) - 
                  5*Power(t2,3) + Power(t2,4)) + 
               s2*(68 + (12 - 8*s)*t2 + (-139 + 50*s)*Power(t2,2) - 
                  4*(-26 + 3*s)*Power(t2,3) + (-5 + 2*s)*Power(t2,4))) - 
            Power(t1,2)*Power(t2,2)*
             (20 + (43 + 20*s)*t2 + 
               (-29 + 11*s + 12*Power(s,2))*Power(t2,2) + 
               12*(-18 + 5*s + Power(s,2))*Power(t2,3) + 
               (106 + 6*s - 6*Power(s,2))*Power(t2,4) - 
               4*(-4 + s)*Power(t2,5) - 
               Power(s2,2)*t2*(48 + 55*t2 + Power(t2,2)) + 
               s2*(-12 - 6*(1 + 4*s)*t2 + (37 - 14*s)*Power(t2,2) + 
                  (232 - 34*s)*Power(t2,3) + (4 + 6*s)*Power(t2,4) + 
                  4*Power(t2,5)))) + 
         Power(s1,2)*(Power(t1,8)*t2 - 
            Power(t1,7)*(-3 + (4 + 3*s + s2)*t2 + 12*Power(t2,2)) + 
            Power(t1,6)*(1 + (-17 + 3*s2)*t2 + 
               (12 + 10*s + s2)*Power(t2,2) + 35*Power(t2,3)) + 
            Power(t1,5)*(-5 - (17 + s - 7*s2)*t2 + 
               (33 + Power(s,2) + 3*s2 + Power(s2,2) + s*(-5 + 6*s2))*
                Power(t2,2) + (-9 - 21*s + 17*s2)*Power(t2,3) - 
               44*Power(t2,4)) + 
            Power(t1,4)*(2 - 5*(-3 + 2*s2)*t2 + 
               (48 + 7*s - 6*Power(s,2) - 22*s2 - 14*Power(s2,2))*
                Power(t2,2) + 
               (-99 - 6*s*(-4 + s2) + 38*s2 - 16*Power(s2,2))*
                Power(t2,3) + (-25 + 35*s - 36*s2)*Power(t2,4) + 
               26*Power(t2,5)) + 
            Power(t1,2)*t2*(-1 + 
               2*(-18 + s + 17*s2 - 6*Power(s2,2))*t2 + 
               (2*s*(8 + 11*s2) - 7*(6 - 5*s2 + Power(s2,2)))*
                Power(t2,2) + 
               (131 - 24*Power(s,2) - 279*s2 + 26*Power(s2,2) + 
                  s*(-30 + 64*s2))*Power(t2,3) + 
               (8*Power(s,2) + s*(17 - 2*s2) - 
                  5*(46 - 11*s2 + Power(s2,2)))*Power(t2,4) + 
               (-27 + 19*s - 17*s2)*Power(t2,5)) + 
            Power(t2,3)*(3 - (3 + 2*s)*t2 - 12*Power(t2,2) + 
               (25 + 3*s - 2*Power(s,2))*Power(t2,3) - 
               (3 + 5*s)*Power(t2,4) + 
               Power(s2,2)*(6 + 7*t2 - 2*Power(t2,2) - 3*Power(t2,3)) + 
               s2*(-10 + 4*(-1 + s)*t2 + (17 + 2*s)*Power(t2,2) + 
                  (-25 + 4*s)*Power(t2,3) + 4*Power(t2,4))) + 
            t1*Power(t2,2)*(4 - 11*t2 + (53 + 10*s)*Power(t2,2) + 
               (-146 - 5*s + 12*Power(s,2))*Power(t2,3) + 
               (75 + 8*s - 3*Power(s,2))*Power(t2,4) + 
               (5 - 4*s)*Power(t2,5) + 
               Power(s2,2)*t2*
                (-43 - 11*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               s2*(-2 + (62 - 4*s)*t2 - 2*(21 + 16*s)*Power(t2,2) + 
                  (203 - 20*s)*Power(t2,3) + 2*(-2 + s)*Power(t2,4) + 
                  4*Power(t2,5))) + 
            Power(t1,3)*t2*(Power(s2,2)*t2*
                (20 + 33*t2 + 13*Power(t2,2)) + 
               s2*(4 + 8*s*(1 - 6*t2)*t2 + 20*Power(t2,2) - 
                  87*Power(t2,3) + 32*Power(t2,4)) + 
               t2*(-9 + 6*t2 + 2*Power(s,2)*(10 - 3*t2)*t2 + 
                  232*Power(t2,2) + 48*Power(t2,3) - 6*Power(t2,4) - 
                  s*(26 - 26*t2 + 39*Power(t2,2) + 36*Power(t2,3))))))*
       R1(t2))/((-1 + s2)*(-1 + t1)*
       (Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*Power(t1 - t2,3)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*(-1 + t2)*t2) - 
    (8*(2*Power(-1 + s,2)*Power(t1,5) - 3*Power(s1,5)*t1*(-2 + s2 + t1) + 
         Power(t1,2)*(-6 + (95 + 28*Power(s,2) - 46*s2 + 
               10*Power(s2,2) + s*(-81 + 10*s2))*t2 + 
            (22 + Power(s,2) + 33*s2 - 8*Power(s2,2) - s*(28 + 3*s2))*
             Power(t2,2) + (13 + 2*Power(s,2) + 5*s2 + 2*Power(s2,2) - 
               s*(7 + 4*s2))*Power(t2,3)) + 
         Power(t1,4)*(-10 + t2 - 2*s2*t2 - 6*Power(t2,2) - 
            2*Power(s,2)*(3 + t2) + s*(16 + (-3 + 4*s2)*t2)) + 
         Power(t1,3)*(14 + (-34 + 8*s2)*t2 + 
            (14 - 7*s2 + 2*Power(s2,2))*Power(t2,2) + 6*Power(t2,3) + 
            Power(s,2)*(t2 - 2*Power(t2,2)) + 
            2*s*(-6 + (13 - 10*s2)*t2 + 4*Power(t2,2))) - 
         4*t2*(4 + 42*t2 - 8*Power(t2,2) - 14*Power(t2,3) + 
            Power(s,2)*t2*(-2 + Power(t2,2)) + 
            Power(s2,2)*t2*(2 - 7*t2 + Power(t2,2)) - 
            s2*(4 + 31*t2 + 2*Power(t2,2) + 7*Power(t2,3)) + 
            s*(4 + 12*(-3 + 2*s2)*t2 + (-9 + 5*s2)*Power(t2,2) + 
               (5 - 2*s2)*Power(t2,3))) + 
         2*t1*t2*(-15 + 18*t2 - 49*Power(t2,2) - 10*Power(t2,3) + 
            Power(s,2)*(-4 - 15*t2 + 5*Power(t2,2)) - 
            Power(s2,2)*(4 + 3*t2 + 5*Power(t2,2)) - 
            s2*(-3 + 21*t2 + 8*Power(t2,2) + 4*Power(t2,3)) + 
            s*(19 - 15*t2 + 8*Power(t2,2) + 4*Power(t2,3) + 
               8*s2*(1 + 5*t2))) + 
         Power(s1,4)*(2*Power(s2,2)*(-2 + t1) - 11*Power(t1,3) - 6*t2 + 
            Power(t1,2)*(30 + 11*t2) - t1*(-9 + 14*t2 + 3*s*(2 + t2)) + 
            s2*(-11*Power(t1,2) + 3*t2 + t1*(-9 + 3*s + 8*t2))) + 
         Power(s1,3)*(-13*Power(t1,4) + Power(t1,3)*(43 + 3*s + 28*t2) + 
            t2*(-9 + 17*t2 + 3*s*(2 + t2)) + 
            2*Power(s2,2)*(2*Power(t1,2) + 4*t2 - t1*(9 + t2)) + 
            Power(t1,2)*(36 + 3*Power(s,2) - 45*t2 - 6*Power(t2,2) - 
               2*s*(17 + 5*t2)) + 
            s2*(8 - 15*Power(t1,3) + 9*t2 - 11*s*t2 - 8*Power(t2,2) + 
               Power(t1,2)*(-6 + 10*s + 19*t2) + 
               t1*(26 + 2*s + 12*t2 - s*t2 - 11*Power(t2,2))) + 
            t1*(-17 + 3*Power(s,2)*(-2 + t2) - 84*t2 - 8*Power(t2,2) + 
               s*(21 + 14*t2 + 13*Power(t2,2)))) + 
         s1*(Power(t1,5)*(1 + 3*s - 2*s2 + 6*t2) + 
            Power(t1,4)*(26 + 7*Power(s,2) + s*(-30 + 4*s2 - 4*t2) - 
               9*t2 - 6*Power(t2,2) + 3*s2*(4 + t2)) + 
            2*t1*(13 + 122*t2 + 26*Power(t2,2) - 19*Power(t2,3) + 
               Power(s2,2)*t2*(13 - 2*t2 + Power(t2,2)) + 
               Power(s,2)*t2*(3 + t2 + Power(t2,2)) - 
               2*s2*(4 + 30*t2 + 2*Power(t2,2) + 3*Power(t2,3)) - 
               s*(-8 + (111 - 36*s2)*t2 + (37 + s2)*Power(t2,2) + 
                  2*s2*Power(t2,3))) - 
            Power(t1,3)*(75 + 44*t2 + 36*Power(t2,2) + 
               2*Power(s2,2)*(5 + t2) + Power(s,2)*(18 + t2) + 
               s2*(-18 + 8*t2 + 5*Power(t2,2)) - 
               s*(65 + 14*t2 + 7*Power(t2,2) + s2*(2 + 7*t2))) + 
            2*t2*(27 + 6*t2 - 63*Power(t2,2) - 2*Power(t2,3) + 
               Power(s2,2)*(-4 - 23*t2 + 3*Power(t2,2)) + 
               Power(s,2)*(-4 - t2 + 5*Power(t2,2)) - 
               s2*(19 + t2 + 14*Power(t2,2) + 4*Power(t2,3)) + 
               s*(1 - 41*t2 + 22*Power(t2,2) + 4*Power(t2,3) + 
                  s2*(8 + 30*t2 - 8*Power(t2,2)))) + 
            Power(t1,2)*(6 - 81*t2 + 143*Power(t2,2) + 32*Power(t2,3) + 
               Power(s,2)*(8 + 4*t2 - 10*Power(t2,2)) + 
               2*Power(s2,2)*(4 + 9*t2 + Power(t2,2)) + 
               s2*(6 - 13*t2 + 6*Power(t2,2) + 8*Power(t2,3)) - 
               s*(18 - 104*t2 + 9*Power(t2,2) + 8*Power(t2,3) + 
                  s2*(16 + 75*t2 - 8*Power(t2,2))))) + 
         Power(s1,2)*(-4 - 5*Power(t1,5) + 
            (17 + 6*Power(s,2) - 46*s2 + 28*Power(s2,2) + 
               s*(-13 + 6*s2))*t2 - 
            (-54 + 7*Power(s,2) + s*(14 - 13*s2) + s2 + 8*Power(s2,2))*
             Power(t2,2) + (-3 - 13*s + 11*s2)*Power(t2,3) + 
            Power(t1,4)*(20 + 6*s - 9*s2 + 23*t2) + 
            Power(t1,3)*(51 + 8*Power(s,2) + 2*Power(s2,2) + 
               s*(-54 + 11*s2 - 11*t2) - 32*t2 - 12*Power(t2,2) + 
               s2*(15 + 14*t2)) + 
            Power(t1,2)*(-86 - 127*t2 - 46*Power(t2,2) - 
               4*Power(s2,2)*(6 + t2) + 2*Power(s,2)*(-9 + 2*t2) + 
               s2*(44 + 3*t2 - 16*Power(t2,2)) + 
               s*(70 + 43*t2 + 20*Power(t2,2) + 2*s2*(2 + t2))) + 
            t1*(2*Power(s2,2)*(4 + 9*t2) + 
               Power(s,2)*(8 + 7*t2 - 8*Power(t2,2)) + 
               2*(-22 - 24*t2 + 91*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(18 + 28*t2 + 9*Power(t2,2) + 8*Power(t2,3)) - 
               2*s*(5 - 52*t2 + 17*Power(t2,2) + 4*Power(t2,3) + 
                  s2*(8 + 31*t2 - 4*Power(t2,2))))))*R2(1 - s1 - t1 + t2))/
     ((-1 + s2)*(-1 + t1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*(-1 + t2)) + 
    (8*(Power(-1 + s,2)*Power(t1,8) - 
         Power(s1,7)*t1*(s2*(2 + t1) + t1*(-2 + 3*t1)) - 
         Power(t1,4)*(3 + (9 - 15*Power(s,2) + s*(21 - 13*s2) + 11*s2)*
             t2 + (-82 + 10*Power(s,2) + s*(32 - 51*s2) + 65*s2 + 
               Power(s2,2))*Power(t2,2) + 
            (22 - 6*Power(s,2) - s*(-6 + s2) - 13*s2 + 7*Power(s2,2))*
             Power(t2,3) + 4*Power(t2,4)) - 
         2*Power(t1,2)*t2*(-9 + 
            (33 + 3*Power(s,2) - 57*s2 + 8*Power(s2,2) + 
               s*(-24 + 29*s2))*t2 + 
            (23 - 21*Power(s,2) - 62*s2 - 7*Power(s2,2) + 
               4*s*(3 + 13*s2))*Power(t2,2) + 
            (-53 + 5*Power(s,2) - 9*s2 - 6*Power(s2,2) + s*(20 + s2))*
             Power(t2,3) + 2*(-3 + s - s2)*Power(t2,4)) + 
         Power(t1,7)*(-4 - 2*(-1 + s2)*t2 - Power(s,2)*(2 + t2) + 
            s*(6 + t2 + 2*s2*t2)) + 
         Power(t1,6)*(2 + (-19 + 7*s2)*t2 + 
            (1 - 3*s2 + Power(s2,2))*Power(t2,2) - 
            Power(s,2)*(3 + 4*t2 + Power(t2,2)) + 
            s*(2 + (18 - 7*s2)*t2 + 2*Power(t2,2))) + 
         8*Power(t2,2)*(2 + 22*t2 + Power(s,2)*(-4 + t2)*t2 - 
            3*Power(s2,2)*(-2 + t2)*t2 - 21*Power(t2,2) - 
            8*Power(t2,3) + Power(t2,4) + 
            s2*(-2 - 29*t2 + Power(t2,2) - 6*Power(t2,3)) + 
            s*(2 + (-5 + 6*s2)*t2 + (9 + 2*s2)*Power(t2,2) + 
               10*Power(t2,3))) + 
         Power(t1,5)*(4 - (-47 + s2)*t2 + 
            (1 + 24*s2 - 3*Power(s2,2))*Power(t2,2) + 
            (4 - s2 + Power(s2,2))*Power(t2,3) + 
            Power(s,2)*t2*(21 + 6*t2 + Power(t2,2)) - 
            s*(6 + 65*t2 + 6*(3 + 2*s2)*Power(t2,2) + 
               (1 + 2*s2)*Power(t2,3))) + 
         Power(t1,3)*t2*(-39 - 170*t2 - 73*Power(t2,2) - 
            10*Power(t2,3) + Power(s2,2)*t2*
             (7 + 16*t2 - 6*Power(t2,2)) - 
            Power(s,2)*t2*(65 + 4*t2 + 6*Power(t2,2)) + 
            s2*(7 + 14*t2 - 71*Power(t2,2) + 4*Power(t2,3)) + 
            s*(33 - 14*(-16 + s2)*t2 + (85 + 12*s2)*Power(t2,2) + 
               4*(2 + 3*s2)*Power(t2,3))) + 
         Power(s1,6)*(-2*Power(s2,2) + 
            s2*(-6*Power(t1,3) + 2*t2 + Power(t1,2)*(-15 + s + 4*t2) + 
               t1*(3 - s + 9*t2)) - 
            t1*(-2 + 12*Power(t1,3) + 2*(2 + s)*t2 - 
               Power(t1,2)*(18 + 2*s + 7*t2) + 
               t1*(-8 + s + 2*t2 + 3*s*t2))) + 
         2*t1*Power(t2,2)*(43 + 113*t2 + 61*Power(t2,2) - 
            17*Power(t2,3) + s2*(-21 + 9*Power(t2,2) - 8*Power(t2,3)) + 
            Power(s2,2)*(4 - 19*t2 - 6*Power(t2,2) + 3*Power(t2,3)) + 
            Power(s,2)*(4 + 17*t2 - 2*Power(t2,2) + 3*Power(t2,3)) - 
            s*(9 + 144*t2 + 51*Power(t2,2) + 
               s2*(8 - 42*t2 + 6*Power(t2,3)))) - 
         Power(s1,4)*(2 + 12*Power(t1,6) + 
            (-3 + s + 18*s2 - s*s2 - 19*Power(s2,2))*t2 + 
            (-15 + Power(s,2) + s*(2 - 4*s2) + 7*s2 + 3*Power(s2,2))*
             Power(t2,2) + 2*(4 + 4*s - 5*s2)*Power(t2,3) - 
            Power(t1,5)*(58 + 8*s - 16*s2 + 33*t2) + 
            Power(t1,4)*(-107 - 5*Power(s,2) + s2*(34 - 16*t2) + 
               22*t2 + 12*Power(t2,2) + s*(55 - 10*s2 + 15*t2)) + 
            t1*(12 + 16*t2 + 4*Power(s2,2)*(-3 + t2)*t2 - 
               106*Power(t2,2) + 9*Power(t2,3) + 
               2*Power(s,2)*t2*(-7 + 4*t2) + 
               s2*(-13 + 37*t2 + 65*Power(t2,2) - 15*Power(t2,3)) + 
               s*(7 + (12 + 13*s2)*t2 + (16 - 11*s2)*Power(t2,2) + 
                  15*Power(t2,3))) + 
            Power(t1,3)*(31 + Power(s,2)*(11 - 7*t2) + 255*t2 + 
               Power(s2,2)*t2 + 50*Power(t2,2) + 
               4*s2*(-6 - 15*t2 + 4*Power(t2,2)) + 
               s*(-49 - 22*t2 - 18*Power(t2,2) + 5*s2*(1 + t2))) + 
            Power(t1,2)*(46 + 145*t2 - 119*Power(t2,2) - 8*Power(t2,3) + 
               Power(s2,2)*(26 - 14*t2 + Power(t2,2)) + 
               Power(s,2)*(1 + t2 + 4*Power(t2,2)) - 
               s2*(73 + 53*t2 - 30*Power(t2,2) + 4*Power(t2,3)) + 
               s*(14 - 83*t2 - 17*Power(t2,2) + 4*Power(t2,3) + 
                  s2*(-16 + 28*t2 - 5*Power(t2,2))))) + 
         Power(s1,5)*(-18*Power(t1,5) + Power(t1,4)*(49 + 7*s + 25*t2) + 
            2*t2*(-1 + t2 + s*t2) + 
            Power(s2,2)*(3*t2 + t1*(-10 + 3*t2)) + 
            Power(t1,3)*(52 + Power(s,2) - 9*t2 - 4*Power(t2,2) - 
               s*(17 + 11*t2)) + 
            Power(t1,2)*(12 + 3*Power(s,2)*(-1 + t2) - 77*t2 - 
               6*Power(t2,2) + s*(14 - t2 + 7*Power(t2,2))) + 
            t1*(-3 - 23*t2 - Power(s,2)*t2 + 13*Power(t2,2) + 
               s*(5 + 3*t2 + 11*Power(t2,2))) - 
            s2*(-4 + 14*Power(t1,4) + Power(t1,3)*(35 - 5*s - 13*t2) + 
               3*(1 + s)*t2 + 8*Power(t2,2) + 
               Power(t1,2)*(-11 - 40*t2 + 7*Power(t2,2) + 
                  3*s*(1 + t2)) + 
               t1*(3*s*(-1 + t2) + 2*(-6 - 11*t2 + 7*Power(t2,2))))) + 
         Power(s1,3)*(-3*Power(t1,7) + 
            Power(t1,6)*(30 + 2*s - 9*s2 + 19*t2) + 
            t1*(-3 + (104 - 5*Power(s,2) + s*(8 - 28*s2) - 195*s2 + 
                  73*Power(s2,2))*t2 + 
               (137 + Power(s,2) + 22*s2 - 42*Power(s2,2) + 
                  s*(-92 + 49*s2))*Power(t2,2) + 
               (-164 + 8*Power(s,2) + 33*s2 + 5*Power(s2,2) - 
                  s*(8 + 13*s2))*Power(t2,3) + 
               (-4 + 8*s - 8*s2)*Power(t2,4)) + 
            t2*(15 + 4*t2 - 47*Power(t2,2) + 8*Power(t2,3) + 
               Power(s2,2)*t2*(-25 + 2*t2) + 
               Power(s,2)*t2*(-7 + 4*t2) + 
               s2*(-31 + 26*t2 + 31*Power(t2,2) - 8*Power(t2,3)) + 
               s*(3 + 4*(-1 + 8*s2)*t2 + (15 - 6*s2)*Power(t2,2) + 
                  8*Power(t2,3))) + 
            Power(t1,4)*(-109 - 299*t2 - 3*Power(s2,2)*t2 - 
               84*Power(t2,2) + Power(s,2)*(-17 + 2*t2) + 
               s2*(24 + 28*t2 - 10*Power(t2,2)) + 
               s*(87 + 55*t2 + 16*Power(t2,2) + s2*(-7 + 5*t2))) + 
            Power(t1,5)*(10*Power(s,2) + s*(-75 + 10*s2 - 9*t2) + 
               2*(51 - 13*t2 - 6*Power(t2,2) + s2*(-6 + 5*t2))) - 
            Power(t1,3)*(108 + 292*t2 - 254*Power(t2,2) - 
               36*Power(t2,3) + Power(s,2)*t2*(5 + 9*t2) + 
               2*Power(s2,2)*(15 - 11*t2 + Power(t2,2)) - 
               s2*(103 + 70*t2 - 13*Power(t2,2) + 8*Power(t2,3)) + 
               s*(14 - 215*t2 + 3*Power(t2,2) + 8*Power(t2,3) + 
                  s2*(-16 + 67*t2 - 11*Power(t2,2)))) + 
            Power(t1,2)*(-48 + 135*t2 + 489*Power(t2,2) - 
               25*Power(t2,3) + 
               Power(s,2)*(8 + 31*t2 - 22*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(8 + 4*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               s2*(7 - 74*t2 - 67*Power(t2,2) + 32*Power(t2,3)) - 
               s*(3 + 151*t2 + 164*Power(t2,2) + 38*Power(t2,3) + 
                  s2*(16 - 17*t2 - 23*Power(t2,2) + 2*Power(t2,3))))) - 
         s1*((1 + s)*Power(t1,8) - 
            Power(t1,7)*(14 + 5*Power(s,2) + s*(-16 + s2) + s2 - t2 + 
               s2*t2) + Power(t1,3)*
             (7 + (-132 + 9*Power(s,2) - 40*s*(-2 + s2) + 129*s2 - 
                  25*Power(s2,2))*t2 + 
               (-197 + 9*Power(s,2) + s*(124 - 131*s2) + 142*s2 + 
                  34*Power(s2,2))*Power(t2,2) + 
               (256 - 14*Power(s,2) + 29*s2 + 3*Power(s2,2) + 
                  s*(-64 + 11*s2))*Power(t2,3) + 
               (36 - 8*s + 8*s2)*Power(t2,4)) + 
            Power(t1,4)*(-24 - 249*t2 - 173*Power(t2,2) - 
               39*Power(t2,3) - 
               Power(s2,2)*t2*(-3 + t2 + 3*Power(t2,2)) - 
               Power(s,2)*t2*(49 + 14*t2 + 3*Power(t2,2)) + 
               s2*(1 + 7*t2 - 73*Power(t2,2) + 4*Power(t2,3)) + 
               s*(11 + (281 - 22*s2)*t2 + (132 + 35*s2)*Power(t2,2) + 
                  2*(7 + 3*s2)*Power(t2,3))) + 
            2*t1*t2*(31 + 220*t2 - 117*Power(t2,2) - 140*Power(t2,3) + 
               2*Power(t2,4) + 
               Power(s2,2)*t2*(49 - 32*t2 + 5*Power(t2,2)) + 
               Power(s,2)*t2*(-29 + 22*t2 + 5*Power(t2,2)) - 
               s2*(16 + 219*t2 - 20*Power(t2,2) + 57*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s*(16 + (-31 + 4*s2)*t2 + 2*(-10 + 9*s2)*Power(t2,2) + 
                  (91 - 10*s2)*Power(t2,3) + 4*Power(t2,4))) + 
            Power(t1,6)*(33 + 8*t2 + Power(s2,2)*t2 + 8*Power(t2,2) + 
               Power(s,2)*(8 + 5*t2) + s2*(3 + 12*t2 - Power(t2,2)) - 
               s*(43 + 18*t2 + Power(t2,2) + 2*s2*(-1 + 5*t2))) + 
            Power(t1,5)*(-3 + 125*t2 - 37*Power(t2,2) - 8*Power(t2,3) - 
               Power(s2,2)*t2*(3 + 2*t2) + 
               Power(s,2)*(4 + 14*t2 + 3*Power(t2,2)) + 
               s2*(-3 - 36*t2 + 11*Power(t2,2)) - 
               s*(19 + 90*t2 + 2*Power(t2,2) + 
                  s2*(-7 - 34*t2 + Power(t2,2)))) - 
            2*Power(t2,2)*(-49 + 45*t2 + 69*Power(t2,2) - 
               25*Power(t2,3) + 
               Power(s,2)*(4 + 7*t2 - 6*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(4 + 23*t2 - 6*Power(t2,2) + Power(t2,3)) + 
               s2*(29 - 30*t2 + 19*Power(t2,2) + 2*Power(t2,3)) - 
               s*(3 - 14*t2 + 69*Power(t2,2) + 2*Power(t2,3) + 
                  2*s2*(4 + 15*t2 - 6*Power(t2,2) + Power(t2,3)))) + 
            Power(t1,2)*t2*(85 + 530*t2 + 427*Power(t2,2) - 
               50*Power(t2,3) + 
               Power(s,2)*(16 + 27*t2 + 6*Power(t2,3)) + 
               Power(s2,2)*(16 - 31*t2 + 6*Power(t2,2) + 
                  6*Power(t2,3)) - 
               s2*(27 + 30*t2 - 35*Power(t2,2) + 16*Power(t2,3)) - 
               s*(-15 + 504*t2 + 281*Power(t2,2) + 16*Power(t2,3) + 
                  2*s2*(16 - 58*t2 + 11*Power(t2,2) + 6*Power(t2,3))))) + 
         Power(s1,2)*(-2*t2*(-6 + 
               (29 - 2*Power(s,2) - 74*s2 + 31*Power(s2,2) + 
                  s*(5 + 3*s2))*t2 + 
               (22 - 5*Power(s,2) + 18*s2 - 13*Power(s2,2) + 
                  6*s*(-2 + 3*s2))*Power(t2,2) + 
               (-33 + 2*Power(s,2) + 8*s2 + Power(s2,2) - s*(1 + 3*s2))*
                Power(t2,3) + 2*(s - s2)*Power(t2,4)) - 
            2*Power(t1,7)*(s + s2 - 2*(1 + t2)) + 
            Power(t1,2)*(31 + 370*t2 + 121*Power(t2,2) - 
               430*Power(t2,3) - 28*Power(t2,4) + 
               Power(s2,2)*t2*(91 - 56*t2 + 5*Power(t2,2)) + 
               Power(s,2)*t2*(-17 + 11*t2 + 20*Power(t2,2)) - 
               s2*(16 + 351*t2 + 24*Power(t2,2) + 67*Power(t2,3) + 
                  16*Power(t2,4)) + 
               s*(16 + (14 - 26*s2)*t2 + (-262 + 101*s2)*Power(t2,2) + 
                  (118 - 25*s2)*Power(t2,3) + 16*Power(t2,4))) + 
            Power(t1,6)*(10*Power(s,2) + s2 + 4*s2*t2 - 
               4*(-13 + 3*t2 + Power(t2,2)) + s*(5*s2 - 2*(25 + t2))) - 
            Power(t1,5)*(97 + 127*t2 + 3*Power(s2,2)*t2 + 
               48*Power(t2,2) + 3*Power(s,2)*(5 + 2*t2) + 
               s2*(-5 + 11*t2) - 
               s*(89 + 51*t2 + 6*Power(t2,2) + 3*s2*(-2 + 5*t2))) + 
            Power(t1,4)*(2*Power(s2,2)*(-6 + 7*t2) - 
               Power(s,2)*t2*(15 + 7*t2) + 
               s2*(45 + 66*t2 - 5*Power(t2,2) + 4*Power(t2,3)) + 
               4*(-15 - 65*t2 + 47*Power(t2,2) + 9*Power(t2,3)) + 
               s*(22 + 207*t2 - 9*Power(t2,2) - 4*Power(t2,3) + 
                  s2*(-4 - 69*t2 + 7*Power(t2,2)))) + 
            Power(t1,3)*(20 + 423*t2 + 545*Power(t2,2) + 
               35*Power(t2,3) + 
               Power(s,2)*(8 + 45*t2 - 6*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*(8 - 8*t2 + 3*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(-7 - 66*t2 + 19*Power(t2,2) + 10*Power(t2,3)) - 
               s*(-3 + 327*t2 + 248*Power(t2,2) + 32*Power(t2,3) + 
                  s2*(16 - 49*t2 + 11*Power(t2,2) + 6*Power(t2,3)))) + 
            t1*t2*(133 - 196*t2 - 421*Power(t2,2) + 100*Power(t2,3) - 
               Power(s,2)*(16 + 53*t2 - 32*Power(t2,2) + 4*Power(t2,3)) - 
               Power(s2,2)*(16 + 29*t2 - 28*Power(t2,2) + 
                  4*Power(t2,3)) - 
               s2*(35 - 114*t2 + 21*Power(t2,2) + 20*Power(t2,3)) + 
               s*(27 + 84*t2 + 275*Power(t2,2) + 24*Power(t2,3) + 
                  s2*(32 + 26*t2 - 52*Power(t2,2) + 8*Power(t2,3))))))*
       T2(t2,1 - s1 - t1 + t2))/
     ((-1 + s2)*(-1 + t1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       (-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)) - 
    (8*(Power(-1 + s,2)*Power(t1,8) - 
         Power(t1,4)*(3 + (9 + 3*Power(s,2) + 11*s2 - s*(9 + 13*s2))*
             t2 + (75 + 40*Power(s,2) + 16*s2 + Power(s2,2) - 
               3*s*(37 + 3*s2))*Power(t2,2) + 
            (86 - 8*Power(s,2) + 20*s2 - 8*Power(s2,2) + 
               10*s*(-7 + 2*s2))*Power(t2,3) + 
            (-1 + 5*Power(s,2) + 4*s2 - 4*s*(2 + s2))*Power(t2,4)) - 
         Power(t1,7)*(4 + t2 + 2*s2*t2 + Power(s,2)*(2 + 4*t2) - 
            s*(6 + (7 + 2*s2)*t2)) + 
         Power(t1,6)*(2 + (-1 + 7*s2)*t2 + 
            (-2 + 3*s2 + Power(s2,2))*Power(t2,2) + 
            Power(s,2)*(-3 + 8*t2 + 5*Power(t2,2)) - 
            s*(-2 + (12 + 7*s2)*t2 + (7 + 6*s2)*Power(t2,2))) - 
         Power(t1,5)*(-4 + (-17 + s2)*t2 + 
            (-40 + 9*s2 + 3*Power(s2,2))*Power(t2,2) + 
            2*(-1 - s2 + Power(s2,2))*Power(t2,3) + 
            6*Power(s,2)*t2*(-3 + 2*t2) + 
            s*(6 + 35*t2 + (16 - 21*s2)*Power(t2,2) + 
               (2 - 4*s2)*Power(t2,3))) + 
         Power(t1,3)*t2*(-6 - 35*t2 + 128*Power(t2,2) + 90*Power(t2,3) - 
            Power(t2,4) + Power(s2,2)*t2*
             (7 + t2 - 6*Power(t2,2) + 2*Power(t2,3)) + 
            2*Power(s,2)*t2*(6 + 20*t2 - Power(t2,2) + 2*Power(t2,3)) + 
            s2*(7 + 39*t2 + 41*Power(t2,2) + 46*Power(t2,3)) - 
            s*(3 + (-29 + 55*s2)*t2 + (144 + 23*s2)*Power(t2,2) + 
               (86 - 4*s2)*Power(t2,3) + (5 + 6*s2)*Power(t2,4))) + 
         Power(t2,2)*(4 + (2 - 4*s)*t2 - (3 + 7*s)*Power(t2,2) + 
            (36 - 10*s - 3*Power(s,2))*Power(t2,3) + 
            (3 + s + 2*Power(s,2))*Power(t2,4) + 
            2*(-1 + s)*Power(t2,5) + 
            Power(s2,2)*(4 + 16*t2 - 6*Power(t2,2) - 7*Power(t2,3) + 
               2*Power(t2,4)) - 
            s2*(8 + (18 - 4*s)*t2 - 5*(5 + 2*s)*Power(t2,2) - 
               6*(-2 + s)*Power(t2,3) + (15 + 4*s)*Power(t2,4) + 
               2*Power(t2,5))) + 
         Power(s1,4)*t1*(t1 - t2)*
          (Power(t1,4) + 2*s2*Power(t2,2) - 
            Power(t1,3)*(-2 + s2 + 2*t2) + 
            Power(t1,2)*(-2 - 6*t2 + Power(t2,2) + 4*s2*(1 + t2)) - 
            t1*(4 + 4*t2 - 2*Power(t2,2) + 2*Power(s2,2)*(1 + t2) + 
               s2*(-6 - 2*t2 + Power(t2,2)))) + 
         t1*Power(t2,2)*(10 - 8*t2 - 105*Power(t2,2) - 
            2*Power(s,2)*(-6 + t2)*Power(t2,2) + 35*Power(t2,3) + 
            18*Power(t2,4) + Power(s2,2)*
             (-4 + 2*t2 + 3*Power(t2,2) - 5*Power(t2,3) + Power(t2,4)) + 
            s2*(-6 - 31*t2 + 51*Power(t2,2) + 32*Power(t2,3) + 
               11*Power(t2,4)) - 
            s*(-4 - 11*t2 - 53*Power(t2,2) + 21*Power(t2,3) + 
               16*Power(t2,4) + 
               s2*(4 + 20*t2 + 45*Power(t2,2) - 3*Power(t2,3) + 
                  Power(t2,4)))) - 
         Power(t1,2)*Power(t2,2)*
          (-8 - 105*t2 + 110*Power(t2,2) + 55*Power(t2,3) + 
            Power(s,2)*t2*(18 + 15*t2 + Power(t2,3)) + 
            s2*(-23 + 59*t2 + 41*Power(t2,2) + 33*Power(t2,3) - 
               Power(t2,4)) + 
            Power(s2,2)*(8 + 7*t2 - 3*Power(t2,2) + Power(t2,4)) - 
            s*(-1 - 75*t2 + 86*Power(t2,2) + 52*Power(t2,3) + 
               Power(t2,4) + s2*
                (10 + 81*t2 + 15*Power(t2,2) + 3*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         Power(s1,3)*(Power(t1,7) + 2*s2*Power(t2,3)*(-s2 + t2) - 
            Power(t1,6)*(-10 + 2*s + 3*s2 + 4*t2) + 
            Power(t1,5)*(8 - 38*t2 + 6*Power(t2,2) + 
               s*(-3 + s2 + 7*t2) + s2*(3 + 9*t2)) - 
            Power(t1,4)*(18 + 2*Power(s2,2) + 6*t2 - 58*Power(t2,2) + 
               4*Power(t2,3) + 
               s*(-2 + s2 - 13*t2 + 5*s2*t2 + 9*Power(t2,2)) + 
               s2*(-7 + 28*t2 + 13*Power(t2,2))) + 
            t1*Power(t2,2)*(Power(s2,2)*(2 - 4*t2) + 
               s2*(12 - (-9 + s)*t2 + 5*Power(t2,2)) - 
               2*(4 + 3*t2 + (-2 + s)*Power(t2,2))) + 
            Power(t1,2)*(-4 + 2*(5 + 2*s)*t2 + 
               2*(-13 + s)*Power(t2,2) + (-32 + 7*s)*Power(t2,3) - 
               (-8 + s)*Power(t2,4) - 
               2*Power(s2,2)*(2 + 6*t2 - 3*Power(t2,2) + Power(t2,3)) + 
               s2*(8 - 4*(1 + s)*t2 + (3 + s)*Power(t2,2) + 
                  (4 + s)*Power(t2,3))) + 
            Power(t1,3)*(-14 + 62*t2 + 30*Power(t2,2) - 38*Power(t2,3) + 
               Power(t2,4) + 6*Power(s2,2)*t2*(2 + t2) + 
               s2*(16 - 45*t2 + 8*Power(t2,2) + 7*Power(t2,3)) + 
               s*(-4 - 4*t2 - 15*Power(t2,2) + 5*Power(t2,3) + 
                  s2*(4 + t2 + 3*Power(t2,2))))) - 
         Power(s1,2)*((-7 + 3*s + 2*s2)*Power(t1,7) - 
            Power(t1,6)*(6 + Power(s,2) - 34*t2 + s2*(2 + 7*t2) + 
               s*(-12 + 2*s2 + 11*t2)) + 
            Power(t2,3)*(-(Power(s2,2)*(2 + 5*t2)) + 
               s2*(2 + 3*(3 + s)*t2 + 6*Power(t2,2)) - 
               2*(2 + t2 + (-1 + s)*Power(t2,2))) + 
            t1*t2*(-8 + 8*(1 + s)*t2 - (49 + s)*Power(t2,2) + 
               (-33 + 7*s + Power(s,2))*Power(t2,3) + 
               (13 - 7*s)*Power(t2,4) - 
               Power(s2,2)*(8 + 18*t2 - 19*Power(t2,2) + 
                  7*Power(t2,3)) + 
               s2*(16 + (22 - 8*s)*t2 + (3 - 16*s)*Power(t2,2) + 
                  (28 + 3*s)*Power(t2,3) + 6*Power(t2,4))) + 
            Power(t1,2)*(-4 - 2*(15 + 2*s)*t2 + 
               (121 + 17*s)*Power(t2,2) - 
               (35 + 29*s + 4*Power(s,2))*Power(t2,3) - 
               (76 - 42*s + Power(s,2))*Power(t2,4) - 
               (-4 + s)*Power(t2,5) + 
               Power(s2,2)*(-4 - 20*t2 + 9*Power(t2,2) + 
                  21*Power(t2,3)) + 
               s2*(8 + (38 + 4*s)*t2 + (-85 + 16*s)*Power(t2,2) + 
                  (-14 + 11*s)*Power(t2,3) + (-7 + s)*Power(t2,4) + 
                  Power(t2,5))) + 
            Power(t1,5)*(52 + 19*t2 - 64*Power(t2,2) + 
               Power(s,2)*(1 + 4*t2) + 
               s2*(-12 + 31*t2 + 8*Power(t2,2)) + 
               s*(-2 - 57*t2 + 16*Power(t2,2) + s2*(2 + 5*t2))) - 
            Power(t1,4)*(-25 + 133*t2 + 58*Power(t2,2) - 
               58*Power(t2,3) + 2*Power(s,2)*t2*(2 + 3*t2) + 
               Power(s2,2)*(-4 + 11*t2) + 
               s2*(32 - 36*t2 + 63*Power(t2,2) + 2*Power(t2,3)) + 
               s*(-15 + 5*t2 - 106*Power(t2,2) + 12*Power(t2,3) + 
                  s2*(7 - t2 + 3*Power(t2,2)))) + 
            Power(t1,3)*(-10 - 59*t2 + 159*Power(t2,2) + 
               108*Power(t2,3) - 25*Power(t2,4) + 
               2*Power(s,2)*Power(t2,2)*(3 + 2*t2) + 
               Power(s2,2)*(4 + 9*t2 + 9*Power(t2,2)) + 
               s2*(10 + 33*t2 - 68*Power(t2,2) + 35*Power(t2,3) - 
                  2*Power(t2,4)) - 
               s*(4 + 31*t2 - 31*Power(t2,2) + 96*Power(t2,3) - 
                  5*Power(t2,4) + 
                  s2*(-4 - 4*t2 + 17*Power(t2,2) + Power(t2,3))))) - 
         s1*((1 + s)*Power(t1,8) + 
            Power(t1,3)*(-2 + 
               (-17 + (67 + 21*s)*s2 - 9*Power(s2,2))*t2 + 
               (250 - 34*Power(s,2) - 68*s2 + 16*Power(s2,2) + 
                  s*(-85 + 53*s2))*Power(t2,2) + 
               (-87 - 23*s + 16*Power(s,2) + 39*s2 - 2*s*s2 + 
                  3*Power(s2,2))*Power(t2,3) + 
               (-107 - 2*Power(s,2) - 27*s2 + 2*Power(s2,2) + 
                  s*(62 + s2))*Power(t2,4) + (s + s2)*Power(t2,5)) - 
            Power(t1,7)*(11 + 2*Power(s,2) + s2 + 2*t2 + s2*t2 + 
               s*(-10 + s2 + 3*t2)) + 
            Power(t1,6)*(21 + 53*t2 + Power(s2,2)*t2 + 
               Power(s,2)*(2 + 8*t2) - 
               s*(25 + s2*(-2 + t2) + 47*t2 - 2*Power(t2,2)) + 
               s2*(3 + 9*t2 + 2*Power(t2,2))) + 
            Power(t2,2)*(4 + 21*Power(t2,2) + 9*Power(t2,3) + 
               Power(s,2)*Power(t2,3) - 6*Power(t2,4) + 
               Power(s2,2)*(4 + 10*t2 - 9*Power(t2,2) + 5*Power(t2,3)) + 
               s*t2*(-4 - t2 + 6*Power(t2,3)) + 
               s2*(-8 + 4*(-2 + s)*t2 + (4 + s)*Power(t2,2) - 
                  (29 + 4*s)*Power(t2,3) - 6*Power(t2,4))) + 
            Power(t1,4)*(-12 - 99*t2 + 115*Power(t2,2) + 
               148*Power(t2,3) - Power(t2,4) + 
               Power(s2,2)*t2*(3 + 5*t2) + 
               Power(s,2)*t2*(21 - 4*t2 + 8*Power(t2,2)) + 
               s2*(1 + 27*t2 + 7*Power(t2,2) + 34*Power(t2,3) - 
                  2*Power(t2,4)) - 
               s*(7 + (-59 + 35*s2)*t2 + (69 + 8*s2)*Power(t2,2) + 
                  4*(25 + 2*s2)*Power(t2,3) + 3*Power(t2,4))) + 
            t1*t2*(8 - 4*(-3 + s)*t2 - (67 + 12*s)*Power(t2,2) + 
               (93 + 6*s - 9*Power(s,2))*Power(t2,3) + 
               4*(11 - 9*s + Power(s,2))*Power(t2,4) + 
               (-5 + 3*s)*Power(t2,5) + 
               Power(s2,2)*(8 + 34*t2 - 3*Power(t2,2) - 
                  18*Power(t2,3) + 4*Power(t2,4)) - 
               s2*(16 - 4*(-13 + s)*t2 - 19*(3 + s)*Power(t2,2) - 
                  15*Power(t2,3) + (1 + 7*s)*Power(t2,4) + 3*Power(t2,5))\
) - Power(t1,5)*(-3 + 69*t2 + 117*Power(t2,2) - 2*Power(t2,3) + 
               Power(s2,2)*t2*(3 + 2*t2) + 
               Power(s,2)*(5 + 4*t2 + 12*Power(t2,2)) + 
               s2*(3 + 22*t2 + 25*Power(t2,2)) - 
               s*(-13 + 79*t2 + 93*Power(t2,2) + 2*Power(t2,3) + 
                  s2*(7 + t2 + 8*Power(t2,2)))) + 
            Power(t1,2)*t2*(26 + 39*t2 - 268*Power(t2,2) + 
               2*Power(s,2)*(13 - 7*t2)*Power(t2,2) - 18*Power(t2,3) + 
               39*Power(t2,4) - 
               Power(s2,2)*(8 + 15*t2 + 18*Power(t2,2) + 9*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(-12 - 57*t2 + 82*Power(t2,2) - 20*Power(t2,3) + 
                  13*Power(t2,4)) + 
               s*(8 + 20*t2 + 33*Power(t2,2) + 68*Power(t2,3) - 
                  21*Power(t2,4) + 
                  s2*(-8 - 41*t2 - 21*Power(t2,2) + 14*Power(t2,3) + 
                     Power(t2,4))))))*T3(t2,t1))/
     ((-1 + s2)*(-1 + t1)*Power(t1 - t2,2)*(-1 + s1 + t1 - t2)*
       Power(s1*t1 - t2,2)*(-1 + t2)) - 
    (8*(Power(-1 + s,2)*Power(t1,7) + 
         Power(s1,4)*t1*(s2*(2 + t1 + 6*Power(t1,2) - Power(t1,3)) + 
            t1*(10 - 25*t1 + 6*Power(t1,2) + Power(t1,3))) - 
         t1*(3 + (-33 + 3*Power(s,2) + s*(3 - 13*s2) + 25*s2)*t2 + 
            (28 - 4*Power(s,2) - 21*s*(-2 + s2) - 49*s2 + 
               15*Power(s2,2))*Power(t2,2) + 
            (10 + 6*Power(s,2) + s*(4 - 3*s2) + 7*s2 - 3*Power(s2,2))*
             Power(t2,3) + 2*(5 + s - s2)*Power(t2,4)) + 
         Power(t1,6)*(-6 - 2*(-1 + s2)*t2 - Power(s,2)*(4 + t2) + 
            s*(10 + t2 + 2*s2*t2)) + 
         Power(t1,5)*(11 + (-17 + 11*s2)*t2 + 
            (1 - 3*s2 + Power(s2,2))*Power(t2,2) + 
            Power(s,2)*(2 + 4*t2 - Power(t2,2)) + 
            s*(-12 + (4 - 11*s2)*t2 + 2*Power(t2,2))) - 
         t2*(15 - 12*t2 + 5*Power(s,2)*t2 + 5*Power(t2,2) + 
            Power(s2,2)*t2*(-7 + 2*t2) + 
            s2*(-7 + 32*t2 + Power(t2,2) - 4*Power(t2,3)) + 
            s*(3 + (4 - 6*s2)*t2 - (1 + 2*s2)*Power(t2,2) + 
               4*Power(t2,3))) + 
         Power(t1,4)*(-4 - 17*(-3 + s2)*t2 + 
            (15 + 18*s2 - 5*Power(s2,2))*Power(t2,2) + 
            (-1 + s2)*s2*Power(t2,3) + 
            Power(s,2)*(4 + 4*t2 + 2*Power(t2,2) + Power(t2,3)) - 
            s*(4 - 8*(-5 + 2*s2)*t2 + 20*Power(t2,2) + 
               (1 + 2*s2)*Power(t2,3))) - 
         Power(t1,3)*(9 + 2*(28 + s2)*t2 + 
            (37 + 42*s2 - 6*Power(s2,2))*Power(t2,2) + 
            (16 + 3*s2 + 3*Power(s2,2))*Power(t2,3) + 
            Power(s,2)*(3 + 19*t2 + 5*Power(t2,2) + 2*Power(t2,3)) - 
            s*(14 + (55 + 6*s2)*t2 + (52 + 9*s2)*Power(t2,2) + 
               (12 + 5*s2)*Power(t2,3))) + 
         Power(t1,2)*(10 + (2 + 28*s2)*t2 + 
            (37 + 10*s2 + 6*Power(s2,2))*Power(t2,2) + 
            (31 + 12*s2 + Power(s2,2))*Power(t2,3) + 
            2*(1 + s2)*Power(t2,4) + 
            Power(s,2)*t2*(15 + 13*t2 - Power(t2,2)) - 
            2*s*(3 + (7 + 13*s2)*t2 + 6*(-1 + 3*s2)*Power(t2,2) + 
               12*Power(t2,3) + Power(t2,4))) + 
         Power(s1,3)*(2*Power(s2,2)*Power(-1 + t1,2) - 
            s2*(-((15 + s)*Power(t1,4)) + 3*Power(t1,5) + 2*t2 + 
               Power(t1,2)*(-19 + 7*s + 4*t2) + t1*(3 - s + 13*t2) + 
               Power(t1,3)*(28 + 3*s + 13*t2)) + 
            t1*(-2 + Power(t1,5) - Power(t1,4)*(-16 + t2) - 20*t2 + 
               42*t1*t2 - Power(t1,3)*(43 + 8*t2) + 
               Power(t1,2)*(28 + 19*t2) + 
               s*(-2*Power(t1,4) + Power(t1,3)*(-3 + t2) + 2*t2 - 
                  t1*(3 + 5*t2) + 2*Power(t1,2)*(12 + 5*t2)))) + 
         Power(s1,2)*((11 - 3*s)*Power(t1,6) + 
            Power(t1,5)*(-8 + Power(s,2) + 2*s*(-3 + t2) - 17*t2) + 
            2*t2*(1 - (-5 + s)*t2) - 
            Power(s2,2)*Power(-1 + t1,2)*(3*t2 + t1*(-4 + 3*t2)) - 
            Power(t1,4)*(19 + 9*t2 - 4*Power(t2,2) + 
               Power(s,2)*(1 + t2) + s*(-67 - 15*t2 + Power(t2,2))) + 
            t1*(3 + 11*t2 + Power(s,2)*t2 - 9*Power(t2,2) + 
               s*(-5 + 5*t2 + Power(t2,2))) - 
            Power(t1,2)*(8 + 83*t2 + 56*Power(t2,2) + 
               Power(s,2)*(9 + t2) + s*(12 + 27*t2 + 11*Power(t2,2))) + 
            Power(t1,3)*(Power(s,2)*(17 - 7*t2) + 
               3*(7 + 32*t2 + Power(t2,2)) - 
               s*(41 + 43*t2 + 11*Power(t2,2))) + 
            s2*(-4 - 2*Power(t1,6) + 3*(1 + s)*t2 + 12*Power(t2,2) + 
               Power(t1,5)*(10 + 2*s + t2) + 
               Power(t1,4)*(-40 + s*(-8 + t2) - 11*t2 + Power(t2,2)) + 
               t1*(-4 - 3*s - 18*t2 + 5*s*t2 + 12*Power(t2,2)) + 
               Power(t1,3)*(46 + 19*t2 + 10*Power(t2,2) + 
                  s*(-17 + 7*t2)) + 
               Power(t1,2)*(-6 + 6*t2 + 13*Power(t2,2) + s*(26 + 8*t2)))) \
- s1*(-2 + (1 + s)*Power(t1,7) + 
            (3 + s*(-1 + s2) - 18*s2 + 7*Power(s2,2))*t2 + 
            (11 - Power(s,2) + s2 - 3*Power(s2,2) + s*(2 + 8*s2))*
             Power(t2,2) + (8 - 4*s + 14*s2)*Power(t2,3) - 
            Power(t1,6)*(13 + 2*Power(s,2) + s*(-8 + s2) + s2 - t2 + 
               s2*t2) + Power(t1,5)*
             (44 + 26*t2 + Power(s2,2)*t2 + 2*Power(s,2)*(3 + t2) + 
               s2*(5 + 8*t2 - Power(t2,2)) - 
               s*(44 + 4*s2*(-1 + t2) + 17*t2 + Power(t2,2))) + 
            t1*(-8 + 4*t2 + 2*Power(s,2)*(-5 + t2)*t2 - 60*Power(t2,2) - 
               31*Power(t2,3) + Power(s2,2)*t2*(-11 + 2*t2) + 
               s*(-7 + (-26 + 28*s2)*t2 + (-2 + s2)*Power(t2,2) - 
                  5*Power(t2,3)) + 
               s2*(1 - 22*t2 - 23*Power(t2,2) + 3*Power(t2,3))) + 
            Power(t1,3)*(3 + 18*t2 + 2*Power(s2,2)*(5 - 3*t2)*t2 + 
               38*Power(t2,2) + 5*Power(t2,3) - 
               4*Power(s,2)*(-3 - 2*t2 + Power(t2,2)) + 
               s2*(10 - 42*t2 - 2*Power(t2,2) + 3*Power(t2,3)) - 
               s*(6 - 83*t2 + 11*Power(t2,2) + 3*Power(t2,3) + 
                  s2*(12 + 36*t2 - 9*Power(t2,2)))) - 
            Power(t1,4)*(50 + 45*t2 - Power(s2,2)*(-5 + t2)*t2 + 
               32*Power(t2,2) + Power(s,2)*(11 + 2*t2) + 
               2*s2*(5 + 9*t2 + Power(t2,2)) + 
               s*(-47 - 38*t2 - 14*Power(t2,2) + 
                  s2*(-2 - 15*t2 + Power(t2,2)))) + 
            Power(t1,2)*(25 - 7*t2 + 43*Power(t2,2) - 14*Power(t2,3) + 
               2*Power(s2,2)*t2*(-1 + 3*t2) + 
               Power(s,2)*(-5 + 18*t2 - 13*Power(t2,2)) + 
               s2*(-5 + 93*t2 + 27*Power(t2,2) + 12*Power(t2,3)) + 
               s*(1 - 77*t2 - 50*Power(t2,2) - 12*Power(t2,3) + 
                  s2*(7 - 4*t2 + 7*Power(t2,2))))))*T4(t1))/
     ((-1 + s2)*Power(-1 + t1,2)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*
       (-1 + t2)) + (8*(Power(-1 + s,2)*Power(t1,6) + 
         Power(s1,5)*t1*(-(s2*(-2 + t1)) + t1*(2 + t1)) + 
         t1*(3 + (-15 + 6*s + 3*Power(s,2) + 18*s2 - 13*s*s2)*t2 + 
            (13 + 10*Power(s,2) + 16*s2 + 4*Power(s2,2) - s*(5 + 34*s2))*
             Power(t2,2) + (45 + 2*Power(s,2) + 17*s2 - s*(29 + 6*s2))*
             Power(t2,3) + (14 + 7*s2 + Power(s2,2) - s*(12 + s2))*
             Power(t2,4)) + Power(t1,5)*
          (-5 + t2 - 2*s2*t2 - Power(s,2)*(3 + 2*t2) + 
            s*(8 + (3 + 2*s2)*t2)) + 
         Power(t1,4)*(6 + (-11 + 9*s2)*t2 + 
            (-1 - s2 + Power(s2,2))*Power(t2,2) + 
            Power(s,2)*(-1 + 5*t2) - 
            s*(4 + t2 + 9*s2*t2 + (-1 + 2*s2)*Power(t2,2))) + 
         Power(t1,3)*(2 + (34 - 8*s2)*t2 + 
            (25 + 8*s2 - 4*Power(s2,2))*Power(t2,2) + 
            (-1 + 2*s2)*Power(t2,3) + 
            Power(s,2)*(3 + 10*t2 - Power(t2,2) + 2*Power(t2,3)) - 
            s*(8 + (37 - 7*s2)*t2 + (24 - 7*s2)*Power(t2,2) + 
               (3 + 2*s2)*Power(t2,3))) + 
         t2*(15 + 21*t2 - 5*Power(t2,2) - 5*Power(t2,3) - 2*Power(t2,4) + 
            Power(s,2)*t2*(-3 - t2 + 2*Power(t2,2)) + 
            Power(s2,2)*t2*(-3 + t2 + 2*Power(t2,2)) - 
            s2*(7 + t2 + 7*Power(t2,2) + 7*Power(t2,3) + 2*Power(t2,4)) + 
            s*(3 + (-19 + 18*s2)*t2 + 5*Power(t2,2) + 
               (1 - 4*s2)*Power(t2,3) + 2*Power(t2,4))) + 
         Power(t1,2)*(-7 - 2*(12 + 5*s2)*t2 + 
            2*(-29 - 11*s2 + Power(s2,2))*Power(t2,2) + 
            (-25 - 16*s2 + Power(s2,2))*Power(t2,3) - 
            (-1 + s2)*s2*Power(t2,4) - 
            Power(s,2)*t2*(12 + 13*t2 + Power(t2,2) + Power(t2,3)) + 
            s*(6 + 13*(2 + s2)*t2 + (65 + 9*s2)*Power(t2,2) + 
               3*(9 + s2)*Power(t2,3) + (1 + 2*s2)*Power(t2,4))) + 
         Power(s1,3)*(Power(t1,5) + Power(t1,4)*(14 - 5*s - 2*t2) - 
            Power(s2,2)*(2 + 6*Power(t1,2) + 3*t1*(-2 + t2) + 5*t2) + 
            2*t2*(1 + t2 - s*t2) + 
            Power(t1,3)*(-8 + Power(s,2) - 32*t2 + Power(t2,2) + 
               s*(-13 + 5*t2)) + 
            t1*(5 + 17*t2 + Power(s,2)*t2 + 17*Power(t2,2) + 
               s*(-5 + 3*t2 - 9*Power(t2,2))) - 
            Power(t1,2)*(26 + 15*t2 - 12*Power(t2,2) + 
               Power(s,2)*(1 + t2) + s*(1 - 25*t2 + 2*Power(t2,2))) + 
            s2*(-4 - 5*Power(t1,4) + 5*t2 + 3*s*t2 + 8*Power(t2,2) + 
               Power(t1,3)*(26 + 3*s + 4*t2) + 
               Power(t1,2)*(-4 + 4*s - 21*t2 + Power(t2,2)) + 
               t1*(-9 + 2*t2 + 11*Power(t2,2) - 2*s*(2 + t2)))) + 
         Power(s1,4)*(2*Power(s2,2) + 
            s2*(-4*Power(t1,3) + t1*(-5 + s - 7*t2) - 2*t2 + 
               Power(t1,2)*(12 + s + t2)) + 
            t1*(-2 + 2*Power(t1,3) + Power(t1,2)*(11 - 2*s - 2*t2) + 
               2*(-2 + s)*t2 + t1*(s*(-3 + t2) - 2*(2 + 5*t2)))) + 
         Power(s1,2)*(2 + (6 - 4*s)*Power(t1,5) - 5*t2 + s*t2 - 
            13*Power(t2,2) + Power(s,2)*Power(t2,2) - 8*Power(t2,3) + 
            8*s*Power(t2,3) + 
            Power(t1,4)*(3*Power(s,2) - 21*t2 + s*(-17 + 5*t2)) - 
            Power(s2,2)*(2*(2 - 3*t2)*t2 + Power(t1,3)*(4 + t2) + 
               t1*(4 - 5*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*(-8 - 6*t2 + Power(t2,2))) - 
            Power(t1,3)*(37 + 14*t2 - 17*Power(t2,2) + 
               4*Power(s,2)*(1 + t2) + s*(-23 - 48*t2 + 2*Power(t2,2))) + 
            Power(t1,2)*(20 + 56*t2 + 60*Power(t2,2) - 4*Power(t2,3) + 
               Power(s,2)*(6 + t2 + Power(t2,2)) + 
               s*(4 + 2*t2 - 41*Power(t2,2) + Power(t2,3))) + 
            t1*(9 + 26*t2 - 16*Power(t2,2) - 18*Power(t2,3) + 
               Power(s,2)*t2*(-7 + 3*t2) + 
               2*s*(6 - 4*t2 - 9*Power(t2,2) + 5*Power(t2,3))) - 
            s2*(-4 + 2*Power(t1,5) + (-19 + 4*s)*t2 + 
               (14 + 5*s)*Power(t2,2) + 12*Power(t2,3) - 
               Power(t1,4)*(13 + 3*s + 4*t2) + 
               Power(t1,3)*(13 + s*(2 - 3*t2) + 28*t2 - Power(t2,2)) + 
               Power(t1,2)*(9 + 16*t2 - 16*Power(t2,2) + Power(t2,3) + 
                  s*(6 + 19*t2)) + 
               t1*(-7 - 9*t2 - 9*Power(t2,2) + 9*Power(t2,3) + 
                  s*(-3 - 9*t2 + 6*Power(t2,2))))) + 
         s1*(-2 - (1 + s)*Power(t1,6) + 
            (-14 + s*(-4 + s2) - 11*s2 + 7*Power(s2,2))*t2 + 
            (2*Power(s,2) + s*(15 - 7*s2) + s2*(-11 + 5*s2))*Power(t2,2) + 
            (20 - 3*Power(s,2) + 16*s2 - 5*Power(s2,2) + s*(-5 + 6*s2))*
             Power(t2,3) + (8 - 8*s + 8*s2)*Power(t2,4) + 
            Power(t1,5)*(13 + 3*Power(s,2) + s2 + s2*t2 + 
               s*(-11 + s2 + t2)) - 
            t1*(13 + 38*t2 + 43*Power(t2,2) + 25*Power(t2,3) - 
               5*Power(t2,4) + Power(s,2)*t2*(-3 + t2 + 4*Power(t2,2)) + 
               Power(s2,2)*t2*(12 + 11*t2 + 5*Power(t2,2)) + 
               s2*(-1 - 2*t2 + 7*Power(t2,2) + 13*Power(t2,3) - 
                  3*Power(t2,4)) + 
               s*(7 + (-9 + 6*s2)*t2 - 6*(1 + 4*s2)*Power(t2,2) - 
                  (35 + 8*s2)*Power(t2,3) + 3*Power(t2,4))) + 
            Power(t1,4)*(-36 - 28*t2 - Power(s2,2)*t2 + Power(t2,2) - 
               2*s2*(2 + 5*t2) - Power(s,2)*(6 + 5*t2) + 
               s*(41 + 28*t2 + Power(t2,2) + s2*(-3 + 5*t2))) + 
            Power(t1,3)*(20 + 54*t2 + 45*Power(t2,2) + 
               Power(s,2)*Power(2 + t2,2) + Power(s2,2)*t2*(4 + t2) + 
               s2*(6 + 13*t2 + 6*Power(t2,2) - Power(t2,3)) - 
               s*(10 + 44*t2 + 28*Power(t2,2) + Power(t2,3) + 
                  s2*(5 + 16*t2 + 3*Power(t2,2)))) + 
            Power(t1,2)*(19 + 26*t2 - 45*Power(t2,2) - 29*Power(t2,3) + 
               Power(s2,2)*t2*(2 - t2 + 2*Power(t2,2)) + 
               s2*(-4 + 5*t2 + 24*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s,2)*(-5 + 6*Power(t2,2) + Power(t2,3)) - 
               s*(2*(6 + 13*t2 + 9*Power(t2,2) - 7*Power(t2,3)) + 
                  s2*(-7 - 20*t2 + Power(t2,2) + 3*Power(t2,3))))))*
       T5(1 - s1 - t1 + t2))/
     ((-1 + s2)*(-1 + t1)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)));
   return a;
};
