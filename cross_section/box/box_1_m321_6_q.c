#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m321_6_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(-2*s*(-1 + 4*s - 4*Power(s,2) + Power(s,3))*Power(-1 + t1,2)*
          Power(t1,3)*(1 + t1)*(-1 + s + t1) + 
         4*Power(s2,8)*Power(-1 + t1,2)*t1*Power(s1 - t2,2) + 
         Power(s2,7)*(-1 + t1)*
          (s*(-2 + s1 - t2)*t2 + 
            Power(t1,3)*(-4 - 13*Power(s1,2) + s*(3*s1 + 5*(-2 + t2)) + 
               13*t2 - 22*Power(t2,2) + s1*(-5 + 31*t2)) + 
            Power(t1,2)*(8*Power(s,2)*(-1 + t2) + 
               s*(-19*Power(s1,2) + s1*(-20 + 29*t2) + 
                  2*(10 + t2 - 5*Power(t2,2))) + 
               2*(4 + 3*Power(s1,3) + Power(s1,2)*(3 - 5*t2) - 13*t2 + 
                  12*Power(t2,2) + Power(t2,3) + 
                  s1*(5 - 11*t2 + Power(t2,2)))) - 
            t1*(4 - 4*Power(s1,3) + 8*Power(s,2)*(-1 + t2) - 13*t2 + 
               2*Power(t2,2) + 12*Power(t2,3) + 
               Power(s1,2)*(-7 + 20*t2) + 
               s1*(5 + 9*t2 - 28*Power(t2,2)) + 
               s*(-3*Power(s1,2) - s1*(17 + 2*t2) + 
                  5*(2 + t2 + Power(t2,2))))) + 
         Power(s2,6)*(Power(t1,5)*
             (15 + Power(s,2) + 16*Power(s1,2) + s1*(7 - 39*t2) + 
               s*(38 - 9*s1 - 23*t2) - 37*t2 + 38*Power(t2,2)) + 
            s*t2*(-3 - s1*(-3 + t2) + Power(t2,2) + 
               s*(2*s1 - 3*(2 + t2))) - 
            Power(t1,4)*(40 + 20*Power(s1,3) + 
               Power(s1,2)*(22 - 40*t2) - 39*t2 + 6*Power(t2,2) + 
               15*Power(t2,3) + Power(s,2)*(-75 + 14*s1 + 54*t2) + 
               s1*(-37 + 4*t2 + 9*Power(t2,2)) + 
               s*(79 - 63*Power(s1,2) + 10*t2 - 52*Power(t2,2) + 
                  s1*(-55 + 93*t2))) + 
            Power(t1,3)*(30 - 16*Power(s1,3) - 32*Power(s,3)*(-1 + t2) + 
               105*t2 - 128*Power(t2,2) + 59*Power(t2,3) + 
               Power(s1,2)*(52 + 46*t2) + 
               Power(s,2)*(-175 + 33*Power(s1,2) + s1*(84 - 30*t2) + 
                  72*t2) + s1*(-153 + 82*t2 - 77*Power(t2,2)) - 
               s*(-13 + 19*Power(s1,3) + Power(s1,2)*(100 - 29*t2) - 
                  108*t2 + 82*Power(t2,2) + 8*Power(t2,3) + 
                  s1*(56 - 117*t2 + 2*Power(t2,2)))) - 
            t1*(5 - 4*Power(s1,3) + 24*Power(s,3)*(-1 + t2) - 72*t2 + 
               26*Power(t2,2) + 31*Power(t2,3) + 
               Power(s1,2)*(-56 + 54*t2) + 
               s1*(58 + 43*t2 - 85*Power(t2,2)) + 
               Power(s,2)*(22 + 6*Power(s1,2) - 2*t2 + 35*Power(t2,2) - 
                  2*s1*(15 + 23*t2)) + 
               s*(31 - 8*Power(s1,3) - 25*t2 + 24*Power(t2,2) + 
                  35*Power(t2,3) + 10*Power(s1,2)*(2 + 5*t2) - 
                  s1*(27 + 17*t2 + 77*Power(t2,2)))) + 
            Power(t1,2)*(32*Power(s1,3) + 56*Power(s,3)*(-1 + t2) - 
               2*Power(s1,2)*(51 + 16*t2) + 
               t2*(-179 + 122*t2 - 13*Power(t2,2)) + 
               s1*(167 + 4*t2 + Power(t2,2)) - 
               Power(s,2)*(-121 + 15*Power(s1,2) + 14*t2 - 
                  50*Power(t2,2) + 2*s1*(50 + 21*t2)) - 
               s*(-59 + Power(s1,3) + 97*t2 - 54*Power(t2,2) - 
                  54*Power(t2,3) - 57*Power(s1,2)*(1 + t2) + 
                  s1*(17 + 44*t2 + 110*Power(t2,2))))) - 
         Power(s2,2)*t1*(8*Power(s,6)*t1*(1 - 4*t1 + 3*Power(t1,2))*
             (-1 + t2) + Power(s,5)*
             (-Power(t1,5) + Power(t2,2) + 
               Power(t1,4)*(-132 + 10*s1 + 117*t2) + 
               t1*(26 - 8*s1*(-1 + t2) - 41*t2 + 8*Power(t2,2)) + 
               Power(t1,3)*(289 - 261*t2 + 24*Power(t2,2) - 
                  2*s1*(9 + 13*t2)) + 
               Power(t1,2)*(-182 + 209*t2 - 57*Power(t2,2) + 
                  s1*(-24 + 58*t2))) - 
            (-1 + t1)*(-2*(-31 + 11*s1)*t1*Power(-1 + t2,2) + 
               5*Power(-1 + t2,3) + 
               2*(-1 + s1)*Power(t1,6)*(-7 + s1 + 6*t2) + 
               Power(t1,2)*(-57 + 12*Power(s1,3) - 34*t2 - 
                  34*Power(t2,2) + 29*Power(t2,3) + 
                  6*Power(s1,2)*(-27 + 7*t2) + 
                  s1*(160 + 115*t2 - 71*Power(t2,2))) + 
               Power(t1,3)*(-33 - 32*Power(s1,3) + 275*t2 - 
                  190*Power(t2,2) + 44*Power(t2,3) + 
                  Power(s1,2)*(94 + 66*t2) + 
                  s1*(-183 + 11*t2 - 52*Power(t2,2))) - 
               Power(t1,5)*(9 + 4*Power(s1,3) + 81*t2 - 
                  78*Power(t2,2) - 4*Power(s1,2)*(-6 + 5*t2) + 
                  s1*(-103 + 71*t2 + 12*Power(t2,2))) - 
               Power(t1,4)*(-28 + 36*Power(s1,3) + 39*t2 - 
                  99*Power(t2,2) + 18*Power(t2,3) - 
                  2*Power(s1,2)*(45 + 26*t2) + 
                  s1*(42 + 111*t2 + 23*Power(t2,2)))) + 
            Power(s,4)*(10 + Power(t1,6) - 19*t2 - 3*Power(t2,2) + 
               Power(t1,5)*(34*s1 + 47*(-4 + 3*t2)) + 
               t1*(-26 + 136*t2 - 74*Power(t2,2) + 3*Power(t2,3) + 
                  s1*(-52 + 58*t2)) - 
               Power(t1,4)*(-565 + 19*Power(s1,2) + 423*t2 - 
                  69*Power(t2,2) + s1*(62 + 86*t2)) + 
               Power(t1,3)*(-580 + 561*t2 - 301*Power(t2,2) + 
                  13*Power(t2,3) + Power(s1,2)*(47 + 2*t2) - 
                  2*s1*(35 - 154*t2 + 6*Power(t2,2))) + 
               Power(t1,2)*(218 + Power(s1,2)*(8 - 26*t2) - 420*t2 + 
                  369*Power(t2,2) - 40*Power(t2,3) + 
                  2*s1*(87 - 188*t2 + 30*Power(t2,2)))) + 
            s*(-4 + 11*t2 + 4*Power(t2,2) - 5*Power(t2,3) - 
               2*Power(t1,7)*
                (13 + Power(s1,2) - 12*t2 + s1*(-8 + 6*t2)) + 
               Power(t1,6)*(58 + 4*Power(s1,3) + 
                  Power(s1,2)*(18 - 20*t2) + 5*t2 - 60*Power(t2,2) + 
                  s1*(-115 + 102*t2 + 12*Power(t2,2))) + 
               Power(t1,2)*(644 - 26*Power(s1,3) + 
                  Power(s1,2)*(360 - 88*t2) - 954*t2 + 
                  503*Power(t2,2) - 114*Power(t2,3) + 
                  s1*(-501 + 159*t2 + 13*Power(t2,2))) - 
               Power(t1,5)*(60 + 8*Power(s1,3) + 
                  Power(s1,2)*(173 - 76*t2) + 302*t2 - 
                  171*Power(t2,2) - 12*Power(t2,3) + 
                  s1*(-539 + 213*t2 + 50*Power(t2,2))) + 
               t1*(-226 + 406*t2 - 210*Power(t2,2) + 18*Power(t2,3) + 
                  s1*(80 - 143*t2 + 63*Power(t2,2))) + 
               Power(t1,4)*(230 - 39*Power(s1,3) + 
                  Power(s1,2)*(654 - 87*t2) + 384*t2 - 73*Power(t2,2) - 
                  58*Power(t2,3) + s1*(-1138 + 53*t2 + 68*Power(t2,2))) \
+ Power(t1,3)*(-616 + 129*Power(s1,3) + 426*t2 - 335*Power(t2,2) + 
                  87*Power(t2,3) - Power(s1,2)*(857 + 61*t2) + 
                  s1*(1119 + 54*t2 + 74*Power(t2,2)))) + 
            Power(s,2)*(33 - 86*t2 + 36*Power(t2,2) - 5*Power(t2,3) + 
               2*Power(t1,7)*(-5 + 6*t2) - 
               Power(t1,6)*(6 + 22*Power(s1,2) + 9*t2 + 6*Power(t2,2) + 
                  s1*(-67 + 25*t2)) + 
               Power(t1,3)*(942 - 112*Power(s1,3) - 554*t2 - 
                  417*Power(t2,2) + 114*Power(t2,3) + 
                  Power(s1,2)*(815 + 124*t2) + 
                  s1*(-1333 + 670*t2 - 259*Power(t2,2))) + 
               Power(t1,5)*(37 + 16*Power(s1,3) - 
                  52*Power(s1,2)*(-4 + t2) + 71*t2 - 151*Power(t2,2) + 
                  18*Power(t2,3) + s1*(-451 + 230*t2 + 7*Power(t2,2))) + 
               t1*(261 - 193*t2 - 9*Power(t2,2) + 22*Power(t2,3) - 
                  2*s1*(71 - 98*t2 + 30*Power(t2,2))) + 
               Power(t1,4)*(-326 - 645*Power(s1,2) + 30*Power(s1,3) - 
                  15*t2 + 456*Power(t2,2) - 90*Power(t2,3) + 
                  s1*(1129 - 540*t2 + 119*Power(t2,2))) + 
               Power(t1,2)*(-931 + 18*Power(s1,3) + 774*t2 + 
                  175*Power(t2,2) - 59*Power(t2,3) + 
                  8*Power(s1,2)*(-34 + 3*t2) + 
                  s1*(730 - 699*t2 + 145*Power(t2,2)))) + 
            Power(s,3)*(-34 + 2*Power(t1,7) + 79*t2 - 23*Power(t2,2) + 
               5*Power(t2,3) + Power(t1,6)*(-90 + 19*s1 + 63*t2) + 
               Power(t1,5)*(339 - 47*Power(s1,2) + s1*(3 - 57*t2) - 
                  228*t2 + 33*Power(t2,2)) + 
               Power(t1,2)*(322 - 4*Power(s1,3) + 161*t2 - 
                  778*Power(t2,2) + 144*Power(t2,3) + 
                  6*Power(s1,2)*(9 + 8*t2) + 
                  s1*(-515 + 835*t2 - 169*Power(t2,2))) + 
               Power(t1,4)*(-392 + 9*Power(s1,3) + 
                  Power(s1,2)*(236 - 19*t2) + 243*t2 - 301*Power(t2,2) + 
                  34*Power(t2,3) + s1*(-262 + 363*t2 - 28*Power(t2,2))) + 
               t1*(-82 - 201*t2 + 208*Power(t2,2) - 38*Power(t2,3) + 
                  s1*(128 - 147*t2 + 19*Power(t2,2))) + 
               Power(t1,3)*(-65 + 31*Power(s1,3) - 69*t2 + 
                  789*Power(t2,2) - 157*Power(t2,3) - 
                  Power(s1,2)*(267 + 113*t2) + 
                  s1*(579 - 898*t2 + 238*Power(t2,2))))) + 
         Power(s2,5)*(-(Power(t1,6)*
               (25 + Power(s,2) + 10*Power(s1,2) + 
                 s*(52 - 9*s1 - 37*t2) - 41*t2 + 26*Power(t2,2) - 
                 s1*(9 + 11*t2))) + 
            s*t2*(1 - t2 - 3*Power(t2,2) + s1*(-3 + 2*t2) + 
               Power(s,2)*(6 - s1 + 3*t2) + 
               s*(-2 + (-5 + 2*s1)*t2 - 3*Power(t2,2))) + 
            Power(t1,5)*(57 - 3*Power(s,3) + 24*Power(s1,3) + 
               Power(s1,2)*(30 - 60*t2) + 69*t2 - 127*Power(t2,2) + 
               19*Power(t2,3) + 7*Power(s,2)*(-29 + 6*s1 + 20*t2) + 
               2*s1*(-91 + 76*t2 + 9*Power(t2,2)) + 
               s*(29 - 81*Power(s1,2) + 53*t2 - 79*Power(t2,2) + 
                  s1*(-4 + 94*t2))) + 
            t1*(29 + 4*Power(s1,3) + 24*Power(s,4)*(-1 + t2) - 167*t2 + 
               61*Power(t2,2) + 26*Power(t2,3) + 
               Power(s1,2)*(-107 + 48*t2) + 
               s1*(90 + 113*t2 - 97*Power(t2,2)) + 
               Power(s,3)*(54 + 5*Power(s1,2) - 55*t2 + 
                  41*Power(t2,2) - 3*s1*(3 + 19*t2)) + 
               Power(s,2)*(26 - 4*Power(s1,3) - 57*t2 - 
                  47*Power(t2,2) + 33*Power(t2,3) + 
                  Power(s1,2)*(-13 + 40*t2) + 
                  s1*(27 + 90*t2 - 68*Power(t2,2))) + 
               s*(-38 + 10*Power(s1,3) - 65*t2 - 36*Power(t2,2) + 
                  6*Power(t2,3) + Power(s1,2)*(-107 + 8*t2) + 
                  s1*(99 + 168*t2 - 37*Power(t2,2)))) + 
            Power(t1,4)*(-50 + 64*Power(s1,3) - 286*t2 + 
               226*Power(t2,2) - 33*Power(t2,3) + 
               2*Power(s,3)*(-95 + 12*s1 + 78*t2) - 
               Power(s1,2)*(173 + 98*t2) + 
               s1*(402 - 135*t2 + 83*Power(t2,2)) + 
               Power(s,2)*(418 - 107*Power(s1,2) - 158*t2 + 
                  s1*(-172 + 61*t2)) + 
               s*(247 + 57*Power(s1,3) + Power(s1,2)*(201 - 104*t2) - 
                  195*t2 + 2*Power(t2,2) + 58*Power(t2,3) + 
                  s1*(-229 - 42*t2 + 9*Power(t2,2)))) - 
            Power(t1,2)*(69 + 46*Power(s1,3) + 72*Power(s,4)*(-1 + t2) - 
               309*t2 + 228*Power(t2,2) - 103*Power(t2,3) - 
               Power(s1,2)*(71 + 136*t2) + 
               s1*(115 + 24*t2 + 137*Power(t2,2)) + 
               Power(s,3)*(266 + 4*Power(s1,2) - 165*t2 + 
                  100*Power(t2,2) - s1*(85 + 122*t2)) + 
               Power(s,2)*(43 - 8*Power(s1,3) - 140*t2 - 
                  153*Power(t2,2) + 78*Power(t2,3) + 
                  Power(s1,2)*(71 + 97*t2) + 
                  s1*(40 + 173*t2 - 168*Power(t2,2))) + 
               s*(-225 + 30*Power(s1,3) - 149*t2 + 51*Power(t2,2) - 
                  104*Power(t2,3) - Power(s1,2)*(283 + 63*t2) + 
                  s1*(408 + 241*t2 + 88*Power(t2,2)))) + 
            Power(t1,3)*(58 - 46*Power(s1,3) + 
               Power(s1,2)*(189 - 26*t2) + 48*Power(s,4)*(-1 + t2) + 
               34*t2 + 94*Power(t2,2) - 115*Power(t2,3) + 
               s1*(-204 - 117*t2 + 133*Power(t2,2)) + 
               Power(s,2)*(-197 + 20*Power(s1,3) + 
                  Power(s1,2)*(167 - 27*t2) - 63*t2 - 125*Power(t2,2) + 
                  12*Power(t2,3) + s1*(143 + 70*t2 - 6*Power(t2,2))) - 
               Power(s,3)*(25*Power(s1,2) + 4*s1*(28 + t2) - 
                  5*(81 - 52*t2 + 4*Power(t2,2))) + 
               s*(-411 + 23*Power(s1,3) + 20*t2 + 165*Power(t2,2) - 
                  225*Power(t2,3) - Power(s1,2)*(296 + 147*t2) + 
                  s1*(533 + 24*t2 + 294*Power(t2,2))))) - 
         Power(s2,4)*(Power(t1,7)*
             (-21 + Power(s,2) - 4*Power(s1,2) + s1*(19 - 11*t2) + 
               23*t2 - 6*Power(t2,2) + s*(-30 + 3*s1 + 25*t2)) + 
            s*t2*((-1 + s1 - 3*t2)*(-1 + t2) + Power(s,3)*(2 + t2) + 
               Power(s,2)*(-5 + s1*(-1 + t2) - 6*t2 - 3*Power(t2,2)) + 
               s*(6 - 2*s1*(-1 + t2) + 7*t2 - 2*Power(t2,2))) + 
            Power(t1,6)*(23 - 3*Power(s,3) + 12*Power(s1,3) + 
               Power(s1,2)*(28 - 40*t2) + 126*t2 - 143*Power(t2,2) + 
               6*Power(t2,3) + Power(s,2)*(-212 + 42*s1 + 151*t2) + 
               s1*(-177 + 148*t2 + 17*Power(t2,2)) + 
               s*(-92 - 51*Power(s1,2) + 102*t2 - 49*Power(t2,2) + 
                  s1*(81 + 13*t2))) + 
            Power(t1,5)*(4 - 3*Power(s,4) + 76*Power(s1,3) - 119*t2 - 
               19*Power(t2,2) + 30*Power(t2,3) - 
               2*Power(s1,2)*(69 + 59*t2) + 
               Power(s,3)*(-390 + 67*s1 + 296*t2) + 
               s1*(150 + 83*t2 + 51*Power(t2,2)) + 
               Power(s,2)*(317 - 135*Power(s1,2) - 128*t2 + 
                  Power(t2,2) + s1*(-41 + 19*t2)) + 
               s*(514 + 61*Power(s1,3) - 225*t2 - 163*Power(t2,2) + 
                  62*Power(t2,3) - 3*Power(s1,2)*(-68 + 47*t2) + 
                  3*s1*(-187 + 79*t2 + 8*Power(t2,2)))) + 
            t1*(54 + 4*Power(s1,3) + 8*Power(s,5)*(-1 + t2) - 155*t2 + 
               57*Power(t2,2) + 12*Power(t2,3) + 
               2*Power(s1,2)*(-27 + 7*t2) + 
               s1*(26 + 93*t2 - 51*Power(t2,2)) + 
               Power(s,4)*(50 + s1*(4 - 20*t2) - 60*t2 + 
                  13*Power(t2,2)) - 
               s*(17 + 6*Power(s1,3) - 20*t2 - 4*Power(t2,2) + 
                  10*Power(t2,3) + 2*Power(s1,2)*(-41 + 9*t2) + 
                  s1*(47 + 49*t2 - 29*Power(t2,2))) + 
               Power(s,3)*(-11 + 55*t2 - 73*Power(t2,2) + 
                  9*Power(t2,3) + 2*Power(s1,2)*(-2 + 5*t2) + 
                  s1*(-29 + 105*t2 - 17*Power(t2,2))) + 
               Power(s,2)*(-51 + 2*Power(s1,3) - 39*t2 + 
                  82*Power(t2,2) - 29*Power(t2,3) - 
                  6*Power(s1,2)*(6 + t2) + 
                  s1*(84 - 89*t2 + 31*Power(t2,2)))) + 
            Power(t1,2)*(-109 - 4*Power(s1,3) - 
               40*Power(s,5)*(-1 + t2) + 68*t2 - 127*Power(t2,2) + 
               96*Power(t2,3) + 8*Power(s1,2)*(-25 + 17*t2) - 
               7*s1*(-27 - 20*t2 + 27*Power(t2,2)) + 
               Power(s,4)*(-265 - 5*Power(s1,2) + 236*t2 - 
                  70*Power(t2,2) + 18*s1*(1 + 5*t2)) + 
               Power(s,2)*(241 - 36*Power(s1,3) - 97*t2 - 
                  384*Power(t2,2) + 164*Power(t2,3) + 
                  7*Power(s1,2)*(29 + 18*t2) + 
                  s1*(-247 + 284*t2 - 209*Power(t2,2))) + 
               s*(-63 + 59*Power(s1,3) - 107*t2 - 310*Power(t2,2) + 
                  32*Power(t2,3) - Power(s1,2)*(451 + 31*t2) + 
                  s1*(379 + 539*t2 - 23*Power(t2,2))) + 
               Power(s,3)*(183 + 5*Power(s1,3) + 
                  Power(s1,2)*(3 - 63*t2) - 166*t2 + 332*Power(t2,2) - 
                  50*Power(t2,3) + s1*(5 - 406*t2 + 110*Power(t2,2)))) + 
            Power(t1,4)*(Power(s,4)*(-219 + 18*s1 + 194*t2) - 
               Power(s,3)*(-878 + 77*Power(s1,2) + 547*t2 - 
                  92*Power(t2,2) + s1*(223 + 59*t2)) + 
               Power(s,2)*(137 + 52*Power(s1,3) + 
                  Power(s1,2)*(423 - 86*t2) - 206*t2 - 
                  295*Power(t2,2) + 84*Power(t2,3) + 
                  s1*(-271 + 146*t2 - 21*Power(t2,2))) + 
               2*(3 + 8*Power(s1,3) - 237*t2 + 239*Power(t2,2) - 
                  79*Power(t2,3) - 10*Power(s1,2)*(5 + 4*t2) + 
                  6*s1*(33 - 20*t2 + 13*Power(t2,2))) + 
               s*(-729 + 43*Power(s1,3) - 20*t2 + 197*Power(t2,2) - 
                  187*Power(t2,3) - 87*Power(s1,2)*(6 + t2) + 
                  s1*(1152 - 115*t2 + 240*Power(t2,2)))) + 
            Power(t1,3)*(43 - 104*Power(s1,3) + 32*Power(s,5)*(-1 + t2) + 
               531*t2 - 240*Power(t2,2) + 14*Power(t2,3) + 
               Power(s1,2)*(468 + 88*t2) + 
               s1*(-603 - 213*t2 + 16*Power(t2,2)) - 
               Power(s,4)*(-437 + 7*Power(s1,2) + 348*t2 - 
                  20*Power(t2,2) + s1*(64 + 22*t2)) + 
               Power(s,3)*(-657 + 7*Power(s1,3) + 
                  Power(s1,2)*(102 - 7*t2) + 415*t2 - 369*Power(t2,2) + 
                  8*Power(t2,3) + s1*(132 + 361*t2 - 10*Power(t2,2))) + 
               s*(417 - 37*Power(s1,3) + Power(s1,2)*(738 - 83*t2) + 
                  204*t2 + 319*Power(t2,2) - 14*Power(t2,3) + 
                  s1*(-1007 - 624*t2 + 89*Power(t2,2))) + 
               Power(s,2)*(-433 + 30*Power(s1,3) + 313*t2 + 
                  613*Power(t2,2) - 313*Power(t2,3) - 
                  Power(s1,2)*(431 + 226*t2) + 
                  s1*(433 - 410*t2 + 441*Power(t2,2))))) + 
         s2*Power(t1,2)*(8*Power(s,6)*t1*(1 - 3*t1 + 2*Power(t1,2))*
             (-1 + t2) + Power(s,5)*
             (-10 + 18*t2 - 5*Power(t2,2) + 
               Power(t1,4)*(-40 + 5*s1 + 32*t2) + 
               Power(t1,2)*(-88 - 20*s1 + 105*t2 + 37*s1*t2 - 
                  34*Power(t2,2)) + 
               Power(t1,3)*(104 - 5*s1 - 88*t2 - 17*s1*t2 + 
                  12*Power(t2,2)) + 
               t1*(34 + 8*s1 - 55*t2 - 8*s1*t2 + 15*Power(t2,2))) - 
            (-1 + t1)*(5*Power(-1 + t2,3) + 
               (-1 + s1)*Power(t1,5)*(-7 + s1 + 6*t2) - 
               t1*Power(-1 + t2,2)*(-41 + 11*s1 + 10*t2) + 
               Power(t1,2)*(-68 + 4*Power(s1,3) + 98*t2 - 
                  83*Power(t2,2) + 21*Power(t2,3) + 
                  2*Power(s1,2)*(-27 + 7*t2) + 
                  s1*(67 + 11*t2 - 10*Power(t2,2))) + 
               Power(t1,3)*(40 - 12*Power(s1,3) + 14*t2 + Power(t2,2) - 
                  6*Power(t2,3) + Power(s1,2)*(67 + 6*t2) + 
                  s1*(-109 + 2*t2 - 3*Power(t2,2))) - 
               Power(t1,4)*(15 + 2*Power(s1,3) + 29*t2 - 
                  36*Power(t2,2) - 2*Power(s1,2)*(-7 + 5*t2) + 
                  s1*(-61 + 41*t2 + 6*Power(t2,2)))) + 
            s*(4 - 8*t2 + 20*Power(t2,2) - 10*Power(t2,3) + 
               2*Power(t1,6)*(4 + Power(s1,2) - 3*t2 + s1*(-8 + 6*t2)) + 
               Power(t1,4)*(180 - 19*Power(s1,3) + 
                  Power(s1,2)*(211 - 14*t2) + 49*t2 - 112*Power(t2,2) - 
                  6*Power(t2,3) + s1*(-449 + 153*t2 + Power(t2,2))) - 
               Power(t1,5)*(4*Power(s1,3) + Power(s1,2)*(42 - 20*t2) + 
                  8*(6 + 7*t2 - 9*Power(t2,2)) + 
                  s1*(-171 + 101*t2 + 12*Power(t2,2))) - 
               Power(t1,2)*(-352 + 10*Power(s1,3) + 527*t2 - 
                  266*Power(t2,2) + 53*Power(t2,3) + 
                  Power(s1,2)*(-139 + 35*t2) + 
                  s1*(243 - 133*t2 + 22*Power(t2,2))) + 
               Power(t1,3)*(-368 + 45*Power(s1,3) + 313*t2 - 
                  102*Power(t2,2) + 32*Power(t2,3) - 
                  Power(s1,2)*(310 + 7*t2) + 
                  s1*(493 - 118*t2 + 34*Power(t2,2))) + 
               t1*(-128 + 235*t2 - 144*Power(t2,2) + 25*Power(t2,3) + 
                  s1*(44 - 79*t2 + 35*Power(t2,2)))) + 
            Power(s,3)*(-60 + 
               Power(t1,5)*(74 - 10*Power(s1,2) + s1*(5 - 7*t2) - 
                  48*t2) + 162*t2 - 75*Power(t2,2) + 10*Power(t2,3) + 
               Power(t1,6)*(-8 + 6*t2) + 
               Power(t1,2)*(292 - 2*Power(s1,3) - 156*t2 - 
                  258*Power(t2,2) + 53*Power(t2,3) + 
                  Power(s1,2)*(29 + 19*t2) + 
                  s1*(-287 + 420*t2 - 88*Power(t2,2))) + 
               Power(t1,4)*(-130 + Power(s1,3) + 
                  Power(s1,2)*(63 - 6*t2) + 79*t2 - 72*Power(t2,2) + 
                  6*Power(t2,3) + s1*(-108 + 115*t2 - 5*Power(t2,2))) + 
               t1*(-56 - 166*t2 + 163*Power(t2,2) - 25*Power(t2,3) + 
                  s1*(92 - 109*t2 + 17*Power(t2,2))) + 
               Power(t1,3)*(-112 + 13*Power(s1,3) + 135*t2 + 
                  182*Power(t2,2) - 32*Power(t2,3) - 
                  5*Power(s1,2)*(26 + 5*t2) + 
                  s1*(286 - 311*t2 + 64*Power(t2,2)))) + 
            Power(s,2)*(29 - 90*t2 + 35*Power(t2,2) - 
               Power(t1,6)*(-6 - 8*s1 + Power(s1,2) + 6*t2 + 6*s1*t2) + 
               Power(t1,3)*(470 - 42*Power(s1,3) - 397*t2 - 
                  100*Power(t2,2) + 22*Power(t2,3) + 
                  Power(s1,2)*(293 + 39*t2) + 
                  s1*(-568 + 342*t2 - 97*Power(t2,2))) + 
               t1*(142 - 75*t2 - 10*Power(t2,2) + 5*Power(t2,3) + 
                  s1*(-89 + 125*t2 - 39*Power(t2,2))) + 
               Power(t1,5)*(-36 + 2*Power(s1,3) + 
                  Power(s1,2)*(37 - 10*t2) + 62*t2 - 36*Power(t2,2) + 
                  s1*(-107 + 61*t2 + 6*Power(t2,2))) + 
               Power(t1,4)*(-93 + 8*Power(s1,3) - 11*t2 + 
                  134*Power(t2,2) - 6*Power(t2,3) + 
                  Power(s1,2)*(-187 + 16*t2) + 
                  s1*(385 - 201*t2 + 7*Power(t2,2))) + 
               Power(t1,2)*(-518 + 8*Power(s1,3) + 517*t2 + 
                  Power(t2,2) - 9*Power(t2,3) + 
                  Power(s1,2)*(-118 + 15*t2) + 
                  s1*(371 - 369*t2 + 75*Power(t2,2)))) + 
            Power(s,4)*(42 + Power(t1,6) - 97*t2 + 40*Power(t2,2) - 
               5*Power(t2,3) + Power(t1,5)*(-30 + 8*s1 + 19*t2) - 
               Power(t1,4)*(-148 + 6*Power(s1,2) + 106*t2 - 
                  15*Power(t2,2) + s1*(7 + 24*t2)) + 
               Power(t1,3)*(-156 + 105*t2 - 76*Power(t2,2) + 
                  5*Power(t2,3) + Power(s1,2)*(26 + t2) + 
                  s1*(-46 + 113*t2 - 8*Power(t2,2))) + 
               Power(t1,2)*(29 + Power(s1,2)*(4 - 13*t2) - 105*t2 + 
                  169*Power(t2,2) - 22*Power(t2,3) + 
                  s1*(113 - 210*t2 + 34*Power(t2,2))) + 
               t1*(s1*(-44 + 49*t2 - 2*Power(t2,2)) + 
                  2*(-17 + 80*t2 - 50*Power(t2,2) + 5*Power(t2,3))))) + 
         Power(s2,3)*((1 + s - s1)*Power(t1,8)*(-7 + s + s1 + 6*t2) - 
            Power(-1 + s,2)*s*t2*
             (-1 + (-2 + s)*t2 + (1 + s)*Power(t2,2)) + 
            Power(t1,7)*(-20 + 2*Power(s,3) + 2*Power(s1,3) + 
               Power(s1,2)*(7 - 10*t2) + 69*t2 - 48*Power(t2,2) + 
               Power(s,2)*(-86 + 14*s1 + 69*t2) + 
               s1*(-31 + 25*t2 + 6*Power(t2,2)) - 
               s*(98 + 16*Power(s1,2) - 86*t2 + 12*Power(t2,2) + 
                  s1*(-69 + 29*t2))) - 
            Power(t1,5)*(115 + Power(s,5) - 64*Power(s1,3) + 
               Power(s,4)*(341 - 44*s1 - 280*t2) + 381*t2 - 
               275*Power(t2,2) + 24*Power(t2,3) + 
               Power(s1,2)*(309 + 64*t2) + 
               s1*(-642 + 143*t2 - 55*Power(t2,2)) + 
               Power(s,3)*(-764 + 89*Power(s1,2) + 487*t2 - 
                  99*Power(t2,2) + s1*(113 + 105*t2)) + 
               s*(375 - 17*Power(s1,3) + Power(s1,2)*(411 - 59*t2) + 
                  264*t2 - 145*Power(t2,2) + 28*Power(t2,3) + 
                  s1*(-1021 + 206*t2 - 18*Power(t2,2))) + 
               Power(s,2)*(-324 - 46*Power(s1,3) + 206*t2 + 
                  269*Power(t2,2) - 72*Power(t2,3) + 
                  Power(s1,2)*(-442 + 101*t2) + 
                  7*s1*(94 - 41*t2 + 2*Power(t2,2)))) + 
            Power(t1,2)*(44 + 12*Power(s1,3) - 8*Power(s,6)*(-1 + t2) - 
               255*t2 + 105*Power(t2,2) + 10*Power(t2,3) + 
               6*Power(s1,2)*(-27 + 7*t2) + 
               s1*(130 + 175*t2 - 101*Power(t2,2)) + 
               Power(s,5)*(-98 + 100*t2 - 17*Power(t2,2) + 
                  s1*(-4 + 21*t2)) + 
               Power(s,3)*(41 - 2*Power(s1,3) + 252*t2 - 
                  488*Power(t2,2) + 76*Power(t2,3) + 
                  3*Power(s1,2)*(7 + 13*t2) + 
                  s1*(-257 + 521*t2 - 98*Power(t2,2))) - 
               s*(-305 + 22*Power(s1,3) + 498*t2 - 332*Power(t2,2) + 
                  98*Power(t2,3) + Power(s1,2)*(-303 + 71*t2) + 
                  s1*(305 + 20*t2 - 62*Power(t2,2))) + 
               Power(s,4)*(181 + Power(s1,2)*(4 - 13*t2) - 261*t2 + 
                  159*Power(t2,2) - 12*Power(t2,3) + 
                  s1*(65 - 186*t2 + 26*Power(t2,2))) + 
               Power(s,2)*(-402 + 12*Power(s1,3) + 64*t2 + 
                  393*Power(t2,2) - 112*Power(t2,3) + 
                  Power(s1,2)*(-190 + 3*t2) + 
                  s1*(443 - 419*t2 + 99*Power(t2,2)))) + 
            Power(t1,3)*(8*Power(s,6)*(-1 + t2) + 
               Power(s,5)*(209 - 189*t2 + 6*Power(t2,2) - 
                  s1*(13 + 9*t2)) + 
               Power(s,2)*(681 - 102*Power(s1,3) - 203*t2 - 
                  663*Power(t2,2) + 235*Power(t2,3) + 
                  9*Power(s1,2)*(82 + 19*t2) + 
                  s1*(-1039 + 520*t2 - 303*Power(t2,2))) + 
               Power(s,4)*(-635 + 602*t2 - 265*Power(t2,2) + 
                  2*Power(t2,3) + Power(s1,2)*(16 + t2) + 
                  s1*(-6 + 285*t2 - 4*Power(t2,2))) - 
               2*(111 + 18*Power(s1,3) - 314*t2 + 214*Power(t2,2) - 
                  56*Power(t2,3) - 3*Power(s1,2)*(7 + 20*t2) + 
                  s1*(34 + 34*t2 + 40*Power(t2,2))) + 
               s*(-269 + 133*Power(s1,3) + 74*t2 - 507*Power(t2,2) + 
                  80*Power(t2,3) - 3*Power(s1,2)*(297 + 31*t2) + 
                  s1*(906 + 540*t2 + 55*Power(t2,2))) + 
               Power(s,3)*(200 + 23*Power(s1,3) - 327*t2 + 
                  922*Power(t2,2) - 187*Power(t2,3) - 
                  Power(s1,2)*(139 + 151*t2) + 
                  s1*(307 - 936*t2 + 284*Power(t2,2)))) + 
            Power(t1,4)*(233 - 76*Power(s1,3) + 
               Power(s1,2)*(418 - 12*t2) - 121*t2 + 268*Power(t2,2) - 
               126*Power(t2,3) + Power(s,5)*(-118 + 5*s1 + 111*t2) + 
               s1*(-459 - 225*t2 + 100*Power(t2,2)) - 
               Power(s,4)*(-808 + 20*Power(s1,2) + 629*t2 - 
                  86*Power(t2,2) + 7*s1*(17 + 12*t2)) + 
               Power(s,3)*(-679 + 15*Power(s1,3) + 
                  Power(s1,2)*(279 - 20*t2) + 474*t2 - 558*Power(t2,2) + 
                  54*Power(t2,3) + s1*(-107 + 487*t2 - 33*Power(t2,2))) + 
               s*(241 - 35*Power(s1,3) + Power(s1,2)*(918 - 169*t2) + 
                  453*t2 + 342*Power(t2,2) - 105*Power(t2,3) + 
                  s1*(-1315 - 499*t2 + 167*Power(t2,2))) + 
               Power(s,2)*(-659 + 44*Power(s1,3) + 329*t2 + 
                  691*Power(t2,2) - 289*Power(t2,3) - 
                  29*Power(s1,2)*(28 + 5*t2) + 
                  s1*(1187 - 622*t2 + 385*Power(t2,2)))) - 
            t1*((-21 + 11*s1 - 10*t2)*Power(-1 + t2,2) + 
               Power(s,5)*(-8 + 10*t2 + Power(t2,2)) + 
               Power(s,4)*(10 - 32*t2 + 3*Power(t2,2) + Power(t2,3) + 
                  s1*(8 - 9*t2 - 2*Power(t2,2))) + 
               Power(s,3)*(20 + 64*t2 - 21*Power(t2,2) + 4*Power(t2,3) - 
                  3*s1*(12 - 13*t2 + Power(t2,2))) + 
               Power(s,2)*(-95 + 22*t2 + 46*Power(t2,2) - 
                  21*Power(t2,3) + s1*(53 - 73*t2 + 23*Power(t2,2))) + 
               s*(s1*(-36 + 65*t2 - 29*Power(t2,2)) + 
                  2*(63 - 122*t2 + 68*Power(t2,2) - 7*Power(t2,3)))) + 
            Power(t1,6)*(66 - 3*Power(s,4) + 34*Power(s1,3) + 
               Power(s1,2)*(5 - 76*t2) + 86*t2 - 173*Power(t2,2) + 
               18*Power(t2,3) + Power(s,3)*(-308 + 62*s1 + 223*t2) + 
               s1*(-211 + 220*t2 + 31*Power(t2,2)) - 
               Power(s,2)*(-46 + 82*Power(s1,2) + 29*t2 + 5*Power(t2,2) + 
                  s1*(-106 + 31*t2)) + 
               s*(27*Power(s1,3) + Power(s1,2)*(97 - 86*t2) + 
                  s1*(-412 + 279*t2 + 29*Power(t2,2)) + 
                  2*(164 - 51*t2 - 83*Power(t2,2) + 9*Power(t2,3)))))))/
     (s*(-1 + s1)*(-1 + s2)*s2*
       (1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2))*
       Power(s2 - t1,2)*Power(-1 + t1,2)*t1*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*(-1 + t2)) + 
    (8*(-(Power(s,7)*Power(t1,2)*(Power(t1,2) + t1*(-3 + t2) + t2)) + 
         (s2 - t1)*(-1 + t1)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,
           2)*(2*Power(s2,3)*(-1 + t1) + 
            Power(t1,2)*(-15 + 7*s1 - 2*t2) + 3*(-1 + t2) - 
            t1*(-18 + s1 + 7*t2) + 
            Power(s2,2)*(-2*Power(t1,2) + 3*s1*(1 + t1) - 7*t2 + 
               t1*(2 + t2)) + 
            s2*(-15 + s1*(1 - 10*t1 - 3*Power(t1,2)) + 4*t2 + 
               6*t1*(3 + t2) + Power(t1,2)*(-3 + 2*t2))) - 
         Power(s,5)*(Power(t1,6) + t1*(s1 - 2*t2)*Power(t2,2) + 
            Power(t2,3) + Power(t1,5)*(-13 + 2*s1 + 5*t2) + 
            Power(t1,4)*(107 + s1 + Power(s1,2) - 24*t2 - 7*s1*t2 + 
               6*Power(t2,2)) + 
            Power(s2,2)*(-((-33 + s1)*Power(t1,2)) + 3*Power(t1,4) + 
               t1*(-11 + 2*s1 - 5*t2) + t2 + 
               Power(t1,3)*(-25 - 3*s1 + 6*t2)) + 
            Power(t1,3)*(Power(s1,2) + 2*s1*(3 + t2) + 
               2*(-68 + t2 - 4*Power(t2,2))) + 
            Power(t1,2)*(-5 + 11*t2 - 2*Power(t2,2) + Power(t2,3) - 
               s1*(1 - 3*t2 + Power(t2,2))) + 
            s2*t1*(2 + Power(t1,3)*(36 + 4*s1 - 21*t2) - 2*(5 + s1)*t2 + 
               Power(t2,2) - t1*
                (-97 + Power(s1,2) - s1*(-9 + t2) - 14*t2 + Power(t2,2)) \
- Power(t1,2)*(153 + s1 + Power(s1,2) - 29*t2 - 3*s1*t2 + 4*Power(t2,2)))\
) + Power(s,6)*t1*(Power(t1,3)*(14 + 2*s1 + 3*s2 - 7*t2) - 2*s2*t2 - 
            Power(t1,2)*(35 + s2*(15 - 4*t2) + s1*(-2 + s2 - t2) - 
               3*t2 + Power(t2,2)) + 
            t1*(-1 + (6 + s1)*t2 - Power(t2,2) + s2*(-s1 + 2*(5 + t2)))) \
+ Power(s,4)*(-2*Power(t1,7) + Power(t2,2)*(-3 + 2*t2) - 
            Power(t1,6)*(-22 + s1 + 2*t2) + 
            t1*t2*(-3 + s1 - 5*t2 + 5*s1*t2 - 10*Power(t2,2)) + 
            Power(t1,5)*(-138 + 25*s1 + 3*Power(s1,2) + t2 - 
               3*Power(t2,2)) + 
            Power(t1,3)*(-246 + s1 + Power(s1,2) + 11*t2 + 8*s1*t2 + 
               4*Power(s1,2)*t2 - 35*Power(t2,2) - 2*s1*Power(t2,2)) + 
            Power(s2,3)*(-1 + t1)*
             (Power(t1,3) + s1*(1 - 2*t1 - 3*Power(t1,2)) + 
               4*Power(t1,2)*(-4 + t2) - 2*(2 + t2) + t1*(19 + 2*t2)) - 
            Power(t1,4)*(-326 + 7*t2 + Power(s1,2)*t2 - 
               33*Power(t2,2) + Power(t2,3) + 
               s1*(24 + 19*t2 - 2*Power(t2,2))) + 
            Power(t1,2)*(-8 + 6*t2 + 9*Power(t2,2) + 9*Power(t2,3) + 
               Power(s1,2)*(2 + t2) - s1*(1 + 10*t2 + 7*Power(t2,2))) + 
            Power(s2,2)*(-1 + Power(t1,4)*(31 + 2*s1 - 21*t2) + 
               (4 + s1)*t2 + 
               t1*(-89 + 2*Power(s1,2) - s1*(-10 + t2) - 23*t2 - 
                  3*Power(t2,2)) - 
               Power(t1,3)*(215 + Power(s1,2) - 51*t2 - 3*s1*t2 + 
                  6*Power(t2,2)) + 
               Power(t1,2)*(268 + Power(s1,2) - 9*t2 + 5*Power(t2,2) - 
                  s1*(14 + 3*t2))) + 
            s2*(Power(t1,6) + Power(t2,2)*(3 - 4*s1 + 9*t2) + 
               Power(t1,5)*(-30 + s1 + 11*t2) + 
               Power(t1,3)*(-578 + Power(s1,2)*(1 - 2*t2) + 37*t2 - 
                  47*Power(t2,2) + 7*s1*(3 + 4*t2)) - 
               Power(t1,4)*(-295 + 3*Power(s1,2) + 44*t2 - 
                  17*Power(t2,2) + s1*(14 + 13*t2)) - 
               t1*(-8 + 7*t2 + 2*Power(s1,2)*t2 + 3*Power(t2,2) + 
                  14*Power(t2,3) + s1*(-2 + 5*t2 - 11*Power(t2,2))) + 
               Power(t1,2)*(308 - 6*Power(s1,2) + 3*t2 + 
                  30*Power(t2,2) + 5*Power(t2,3) + 
                  s1*(-16 + 2*t2 - 5*Power(t2,2))))) + 
         Power(s,3)*((16 + s1)*Power(t1,7) - Power(t1,8) + 
            3*(-1 + t2)*t2 + Power(s2,4)*Power(-1 + t1,2)*
             (-4 + s1 + 4*t1 + s1*t1 - t2 - t1*t2) + 
            Power(t1,6)*(-124 + 2*Power(s1,2) - s1*(-4 + t2) + t2 - 
               Power(t2,2)) + 
            Power(t1,3)*(234 - 2*Power(s1,3) + 
               Power(s1,2)*(24 - 14*t2) - 25*t2 + 76*Power(t2,2) + 
               4*Power(t2,3) + 2*s1*t2*(-19 + 2*t2)) + 
            t1*(-3 - 13*t2 + 5*Power(t2,2) + 9*Power(t2,3) + 
               s1*(2 + 8*t2 - 9*Power(t2,2))) - 
            Power(t1,5)*(-365 + Power(s1,3) - 
               2*Power(s1,2)*(-13 + t2) - 38*t2 - 12*Power(t2,2) + 
               s1*(63 - 27*t2 + Power(t2,2))) - 
            Power(t1,4)*(468 + Power(s1,3) + 41*t2 + 69*Power(t2,2) - 
               5*Power(t2,3) - Power(s1,2)*(4 + 15*t2) + 
               s1*(-80 - 5*t2 + 13*Power(t2,2))) - 
            Power(t1,2)*(19 + t2 + 56*Power(t2,2) + 20*Power(t2,3) + 
               Power(s1,2)*(8 + 3*t2) - s1*(20 + 33*t2 + 25*Power(t2,2))\
) + Power(s2,3)*(-27 - Power(s1,2)*
                (-1 - 2*t1 + 2*Power(t1,2) + Power(t1,3)) - 8*t2 - 
               4*Power(t2,2) + Power(t1,4)*(-9 + 7*t2) + 
               Power(t1,2)*(-236 + 15*t2 - 13*Power(t2,2)) + 
               Power(t1,3)*(124 - 25*t2 + 4*Power(t2,2)) + 
               t1*(148 + 15*t2 + 17*Power(t2,2)) + 
               s1*(3 + t2 - Power(t1,3)*(5 + t2) + 
                  3*Power(t1,2)*(5 + 3*t2) - t1*(17 + 13*t2))) + 
            Power(s2,2)*(3 - Power(s1,3)*t1*(1 + t1 + 2*Power(t1,2)) + 
               Power(t1,5)*(18 - 7*t2) - 19*Power(t2,2) - 
               29*Power(t2,3) + 
               Power(t1,4)*(-286 + 12*t2 - 15*Power(t2,2)) + 
               2*Power(t1,3)*(365 + 8*t2 + 40*Power(t2,2)) - 
               Power(t1,2)*(681 - 10*t2 + 121*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(216 - 39*t2 + 57*Power(t2,2) + 37*Power(t2,3)) + 
               Power(s1,2)*(5*Power(t1,4) + Power(t1,2)*(13 - 5*t2) - 
                  5*t2 + 2*Power(t1,3)*(-9 + 2*t2) + 2*t1*(-4 + 5*t2)) \
+ s1*(1 + Power(t1,5) + 4*t2 + 27*Power(t2,2) + 
                  Power(t1,4)*(6 + 4*t2) - 2*Power(t1,3)*(25 + 16*t2) + 
                  t1*(-1 + 9*t2 - 36*Power(t2,2)) + 
                  Power(t1,2)*(51 + 41*t2 + 11*Power(t2,2)))) + 
            s2*(Power(t1,7) + Power(t1,6)*(-30 - 3*s1 + 2*t2) + 
               Power(t1,5)*(298 - 6*Power(s1,2) + 18*t2 + 
                  5*Power(t2,2) - 2*s1*(2 + t2)) + 
               t2*(3 + 10*t2 - 11*Power(t2,2) + s1*(-5 + 4*t2)) + 
               Power(t1,4)*(-845 + 3*Power(s1,3) + 
                  Power(s1,2)*(47 - 7*t2) - 110*t2 - 68*Power(t2,2) + 
                  3*Power(t2,3) + s1*(91 + 9*t2 - 4*Power(t2,2))) + 
               Power(t1,3)*(978 + 2*Power(s1,3) + 44*t2 + 
                  182*Power(t2,2) - 4*Power(t2,3) - 
                  Power(s1,2)*(24 + 5*t2) + 
                  s1*(-77 - 83*t2 + 14*Power(t2,2))) + 
               Power(t1,2)*(-401 + 3*Power(s1,3) + 65*t2 - 
                  151*Power(t2,2) - 34*Power(t2,3) - 
                  Power(s1,2)*(10 + 3*t2) + 
                  s1*(-48 + 84*t2 + 22*Power(t2,2))) + 
               t1*(-1 + 26*t2 + 66*Power(t2,2) + 50*Power(t2,3) + 
                  Power(s1,2)*(5 + 11*t2) - 
                  s1*(7 + 59*t2 + 44*Power(t2,2))))) - 
         s*(2*(3 + s1)*Power(t1,8) - Power(-1 + t2,3) + 
            t1*Power(-1 + t2,2)*(-9 + 2*s1 + 11*t2) + 
            Power(t1,2)*(-1 + t2)*
             (-49 + Power(s1,2) + s1*(17 - 28*t2) + 88*t2 - 
               21*Power(t2,2)) + 
            2*Power(t1,7)*(Power(s1,2) - s1*(4 + t2) - 2*(5 + t2)) - 
            2*Power(s2,5)*Power(-1 + t1,2)*
             (-2 - 2*Power(s1,2) - 3*t2 - 4*Power(t2,2) + t1*(2 + t2) + 
               s1*(1 + t1 + 6*t2)) - 
            Power(t1,6)*(-81 + Power(s1,3) + 23*t2 - 6*Power(t2,2) + 
               Power(s1,2)*(23 + t2) - s1*(19 + 30*t2)) + 
            Power(t1,4)*(263 - 12*Power(s1,3) + 
               Power(s1,2)*(108 - 37*t2) - 53*t2 + 14*Power(t2,2) + 
               7*Power(t2,3) + s1*(-284 + 141*t2 - 19*Power(t2,2))) + 
            Power(t1,5)*(-211 + 9*Power(s1,3) + 25*t2 - 
               12*Power(t2,2) + Power(s1,2)*(-48 + 22*t2) + 
               s1*(118 - 45*t2 - 10*Power(t2,2))) + 
            Power(t1,3)*(-160 + 2*Power(s1,3) + 166*t2 - 
               89*Power(t2,2) + 6*Power(t2,3) + 
               Power(s1,2)*(-38 + 21*t2) + 
               s1*(168 - 165*t2 + 49*Power(t2,2))) + 
            Power(s2,3)*(20 + 
               Power(s1,3)*(-2 + 27*t1 - 18*Power(t1,2) + 
                  Power(t1,3)) + 29*t2 + 45*Power(t2,2) - 
               17*Power(t2,3) + 
               Power(t1,5)*(-45 - 4*t2 + Power(t2,2)) + 
               Power(t1,4)*(142 + 79*t2 + 11*Power(t2,2) - 
                  Power(t2,3)) + 
               Power(t1,3)*(-176 - 170*t2 - 57*Power(t2,2) + 
                  6*Power(t2,3)) + 
               2*Power(t1,2)*
                (63 + 74*t2 + 112*Power(t2,2) + 15*Power(t2,3)) - 
               t1*(67 + 82*t2 + 224*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s1,2)*(24 + Power(t1,3)*(29 - 8*t2) - 7*t2 + 
                  Power(t1,4)*(42 + t2) + Power(t1,2)*(-58 + 90*t2) - 
                  t1*(37 + 100*t2)) - 
               s1*(1 + 85*t2 - 34*Power(t2,2) + 
                  2*Power(t1,5)*(3 + t2) + Power(t1,4)*(-1 + 27*t2) + 
                  t1*(52 - 286*t2 - 75*Power(t2,2)) + 
                  Power(t1,3)*(38 + 28*t2 + 7*Power(t2,2)) + 
                  6*Power(t1,2)*(-16 + 24*t2 + 13*Power(t2,2)))) + 
            s2*(Power(s1,3)*Power(t1,2)*
                (-6 + 41*t1 - 30*Power(t1,2) + 3*Power(t1,3)) - 
               2*Power(t1,7)*(12 + t2) - Power(-1 + t2,2)*(-5 + 7*t2) + 
               3*Power(t1,5)*(-80 - 8*t2 + Power(t2,2)) + 
               Power(t1,6)*(87 + 22*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(225 - 58*t2 + 78*Power(t2,2) - 
                  14*Power(t2,3)) + 
               Power(t1,4)*(467 + 165*t2 + 53*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(t1,3)*(476 + 228*t2 + 39*Power(t2,2) + 
                  6*Power(t2,3)) + 
               2*t1*(-22 + 71*t2 - 58*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s1,2)*t1*
                (2 + t1*(100 - 49*t2) + Power(t1,4)*(91 - 12*t2) - 
                  2*t2 + Power(t1,5)*(4 + t2) + 
                  4*Power(t1,3)*(18 + t2) + Power(t1,2)*(-269 + 34*t2)) \
- s1*(6*Power(t1,7) + 2*Power(-1 + t2,2) + Power(t1,6)*(-37 + 9*t2) + 
                  t1*(-9 + 40*t2 - 31*Power(t2,2)) + 
                  Power(t1,4)*(164 + 52*t2 - 20*Power(t2,2)) + 
                  Power(t1,5)*(99 + 84*t2 - 2*Power(t2,2)) + 
                  Power(t1,3)*(-512 - 36*t2 + 13*Power(t2,2)) + 
                  Power(t1,2)*(287 - 145*t2 + 14*Power(t2,2)))) + 
            Power(s2,2)*(1 - 
               3*Power(s1,3)*t1*
                (-2 + 17*t1 - 12*Power(t1,2) + Power(t1,3)) - 23*t2 + 
               25*Power(t2,2) - 3*Power(t2,3) + 
               3*Power(t1,6)*(15 + t2) - Power(t1,5)*(152 + 67*t2) + 
               Power(t1,2)*(275 + 374*t2 + 212*Power(t2,2)) + 
               Power(t1,4)*(271 + 174*t2 + 3*Power(t2,2) - 
                  7*Power(t2,3)) - 
               Power(t1,3)*(352 + 333*t2 + 197*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-88 - 128*t2 - 43*Power(t2,2) + 28*Power(t2,3)) + 
               Power(s1,2)*(-1 + Power(t1,3)*(19 - 95*t2) + t2 - 
                  Power(t1,5)*(29 + 2*t2) + 
                  Power(t1,4)*(-109 + 24*t2) + t1*(-86 + 35*t2) + 
                  Power(t1,2)*(206 + 73*t2)) + 
               s1*(8 - 5*t2 - 3*Power(t2,2) + Power(t1,6)*(7 + t2) + 
                  Power(t1,5)*(-39 + 25*t2) + 
                  t1*(120 + 105*t2 - 69*Power(t2,2)) + 
                  Power(t1,4)*(137 + 79*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*(-184 - 459*t2 + 10*Power(t2,2)) + 
                  Power(t1,3)*(-49 + 254*t2 + 19*Power(t2,2)))) + 
            Power(s2,4)*(2 + Power(s1,3)*(-5 + 3*t1) - 2*t2 + 
               28*Power(t2,2) + 28*Power(t2,3) + 
               Power(s1,2)*(-8 - 23*Power(t1,3) + t1*(11 - 21*t2) + 
                  Power(t1,2)*(20 - 3*t2) + 30*t2) + 
               Power(t1,4)*(22 + 5*t2 - Power(t2,2)) - 
               Power(t1,3)*(68 + 43*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(72 + 69*t2 + 49*Power(t2,2) + 
                  5*Power(t2,3)) - 
               t1*(28 + 29*t2 + 58*Power(t2,2) + 31*Power(t2,3)) + 
               s1*(8 - 4*t2 - 53*Power(t2,2) + Power(t1,4)*(5 + t2) + 
                  Power(t1,3)*(7 + 25*t2) - 
                  Power(t1,2)*(21 + 21*t2 + 2*Power(t2,2)) + 
                  t1*(1 - t2 + 49*Power(t2,2))))) + 
         Power(s,2)*(-1 - 9*t1 + 36*Power(t1,2) - 190*Power(t1,3) + 
            405*Power(t1,4) - 376*Power(t1,5) + 181*Power(t1,6) - 
            49*Power(t1,7) + 3*Power(t1,8) + 
            Power(s1,3)*Power(s2 - t1,2)*
             (3*t1*(2 - t1 + Power(t1,2)) + 
               s2*(-2 + t1 - 3*Power(t1,2) + 2*Power(t1,3))) + 24*t1*t2 - 
            104*Power(t1,2)*t2 + 16*Power(t1,3)*t2 + 45*Power(t1,4)*t2 + 
            2*Power(t1,5)*t2 + 15*Power(t1,6)*t2 + 2*Power(t1,7)*t2 + 
            3*Power(t2,2) - 22*t1*Power(t2,2) + 
            90*Power(t1,2)*Power(t2,2) - 112*Power(t1,3)*Power(t2,2) + 
            41*Power(t1,4)*Power(t2,2) - 27*Power(t1,5)*Power(t2,2) + 
            3*Power(t1,6)*Power(t2,2) - 2*Power(t2,3) + 
            7*t1*Power(t2,3) + Power(t1,2)*Power(t2,3) - 
            7*Power(t1,3)*Power(t2,3) - 3*Power(t1,4)*Power(t2,3) + 
            Power(s2,4)*(20 + 8*t2 + 10*Power(t2,2) - 
               Power(t1,3)*(30 + 2*t2 + Power(t2,2)) + 
               2*Power(t1,2)*(40 + 6*t2 + 7*Power(t2,2)) - 
               t1*(70 + 18*t2 + 25*Power(t2,2))) + 
            Power(s2,3)*(44 + Power(t1,5)*(-1 + t2) - 23*t2 + 
               46*Power(t2,2) + 42*Power(t2,3) + 
               Power(t1,4)*(117 + 13*t2 + 3*Power(t2,2)) - 
               Power(t1,3)*(359 + 87*t2 + 55*Power(t2,2)) + 
               Power(t1,2)*(415 + 74*t2 + 134*Power(t2,2) + 
                  10*Power(t2,3)) - 
               2*t1*(108 - 11*t2 + 59*Power(t2,2) + 24*Power(t2,3))) - 
            s2*(Power(t1,7)*(5 + t2) - 
               Power(t1,6)*(158 + 2*t2 + Power(t2,2)) + 
               t2*(1 - 3*t2 + 2*Power(t2,2)) + 
               3*Power(t1,5)*(181 + 46*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(813 + 200*t2 + 245*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,4)*(-910 - 215*t2 - 94*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(15 - 51*t2 + 68*Power(t2,2) + 14*Power(t2,3)) - 
               Power(t1,2)*(308 + 72*t2 + 275*Power(t2,2) + 
                  40*Power(t2,3))) + 
            Power(s2,2)*(3 + 3*Power(t1,6) + 2*t2 + 8*Power(t2,2) + 
               10*Power(t2,3) - 
               Power(t1,5)*(194 + 19*t2 + Power(t2,2)) + 
               Power(t1,4)*(635 + 207*t2 + 44*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(t1,3)*(-884 - 254*t2 - 199*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(t1,2)*(627 + 71*t2 + 351*Power(t2,2) + 
                  45*Power(t2,3)) - 
               t1*(190 + 7*t2 + 241*Power(t2,2) + 73*Power(t2,3))) + 
            Power(s1,2)*(s2 - t1)*
             (Power(s2,3)*(2 - 7*t1 + 2*Power(t1,2) + Power(t1,3)) + 
               Power(s2,2)*(Power(t1,3)*(3 - 2*t2) + 23*t2 - 
                  4*t1*(-7 + 5*t2) + Power(t1,2)*(-23 + 7*t2)) - 
               t1*(-1 - 16*Power(t1,4) + Power(t1,5) + 
                  Power(t1,2)*(34 - 41*t2) + 3*t2 + 
                  Power(t1,3)*(41 + 2*t2) + t1*(-83 + 20*t2)) + 
               s2*(Power(t1,3)*(85 - 19*t2) + 2*t2 + 
                  Power(t1,4)*(-26 + 7*t2) + Power(t1,2)*(-32 + 15*t2) - 
                  t1*(57 + 29*t2))) + 
            s1*(-2*Power(s2,4)*
                (3 + 5*t2 - 2*t1*(3 + 7*t2) + Power(t1,2)*(3 + 7*t2)) + 
               t1*(6 + Power(t1,7) - 13*t2 + 7*Power(t2,2) - 
                  Power(t1,6)*(9 + t2) + Power(t1,5)*(-8 + 17*t2) + 
                  t1*(-5 + 28*t2 - 40*Power(t2,2)) + 
                  Power(t1,4)*(40 - 62*t2 + Power(t2,2)) + 
                  Power(t1,3)*(-176 + 65*t2 + 21*Power(t2,2)) + 
                  Power(t1,2)*(151 + 14*t2 + 25*Power(t2,2))) + 
               s2*(-1 - 2*Power(t1,7) - 2*Power(t1,6)*(-19 + t2) + 
                  3*t2 - 2*Power(t2,2) + 
                  Power(t1,3)*(118 + 143*t2 - 40*Power(t2,2)) + 
                  Power(t1,5)*(12 - 27*t2 + Power(t2,2)) + 
                  Power(t1,4)*(-38 + 52*t2 + 15*Power(t2,2)) + 
                  2*t1*(-4 + 5*t2 + 16*Power(t2,2)) - 
                  Power(t1,2)*(119 + 287*t2 + 44*Power(t2,2))) + 
               Power(s2,3)*(5 - 16*t2 - 59*Power(t2,2) + 
                  Power(t1,4)*(13 + 3*t2) + Power(t1,3)*(25 + 19*t2) - 
                  Power(t1,2)*(50 + 33*t2 + 10*Power(t2,2)) + 
                  t1*(7 + 7*t2 + 59*Power(t2,2))) + 
               Power(s2,2)*(-42*Power(t1,5) + Power(t1,6) - 
                  t2*(12 + 5*t2) + 
                  Power(t1,2)*(59 - 209*t2 - 38*Power(t2,2)) + 
                  Power(t1,3)*(29 + 29*t2 - 13*Power(t2,2)) + 
                  Power(t1,4)*(-20 - t2 + 2*Power(t2,2)) + 
                  t1*(-27 + 269*t2 + 88*Power(t2,2))))))*B1(s,t1,s2))/
     (s*(-1 + s1)*(-1 + s2)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*
       (-1 + t2)) + (8*(-2*Power(-1 + s,3)*s*(1 - 3*s + Power(s,2))*
          Power(t1,4)*(1 + t1)*Power(-1 + s + t1,2) - 
         2*Power(s2,12)*(-1 + t1)*Power(s1 - t2,2) - 
         Power(s2,11)*(s1 - t2)*
          (4 - 8*t1 + 4*Power(t1,2) + Power(s1,2)*(1 + 3*t1) - 10*t2 + 
            4*t1*t2 + 6*Power(t1,2)*t2 + 5*Power(t2,2) - 
            t1*Power(t2,2) - 2*s*
             (2 - 2*t1 + s1*(-4 + 5*t1) + 6*t2 - 7*t1*t2) - 
            2*s1*(-3 - 2*t1 + 5*Power(t1,2) + 3*t2 + t1*t2)) + 
         (-1 + s)*s*s2*Power(t1,3)*(-1 + s + t1)*
          (2*Power(s,6)*t1*(2 + t1 - t2) + 
            2*(-1 + t1)*(5 + 2*Power(t1,3) + 
               Power(t1,2)*(21 - 2*s1 - 2*t2) - t2 + t1*(22 - 4*s1 + t2)\
) + Power(s,5)*(8 + 2*Power(t1,3) - 3*t2 - 
               2*Power(t1,2)*(-2 + s1 + t2) + t1*(-7 - 4*s1 + 12*t2)) + 
            Power(s,4)*(-51 - 2*(-1 + s1)*Power(t1,3) + 
               t1*(-30 + 25*s1 - 29*t2) + 20*t2 + 
               Power(t1,2)*(-35 + 8*s1 + 14*t2)) + 
            s*(63 - 8*Power(t1,4) + Power(t1,2)*(11 + 13*s1 - 22*t2) - 
               17*t2 + 2*Power(t1,3)*(-58 + 9*s1 + 6*t2) + 
               t1*(142 - 43*s1 + 23*t2)) + 
            Power(s,2)*(-131 + 4*Power(t1,4) + 
               t1*(-196 + 79*s1 - 45*t2) + 42*t2 - 
               2*Power(t1,3)*(-42 + 11*s1 + 6*t2) + 
               Power(t1,2)*(-73 - 4*s1 + 36*t2)) + 
            Power(s,3)*(121 + Power(t1,2)*(69 - 11*s1 - 32*t2) - 44*t2 + 
               2*Power(t1,3)*(-15 + 5*s1 + 2*t2) + 
               t1*(121 - 65*s1 + 45*t2))) + 
         Power(s2,10)*(2 - 6*t1 + 6*Power(t1,2) - 2*Power(t1,3) + 
            Power(s1,3)*(2 + 19*t1 + 15*Power(t1,2)) - 16*t2 + 
            20*t1*t2 + 8*Power(t1,2)*t2 - 12*Power(t1,3)*t2 + 
            43*Power(t2,2) - 18*t1*Power(t2,2) - 
            19*Power(t1,2)*Power(t2,2) - 6*Power(t1,3)*Power(t2,2) - 
            27*Power(t2,3) - 9*t1*Power(t2,3) - 
            Power(s1,2)*(-21 + 20*Power(t1,3) + 23*t2 + 
               3*t1*(-4 + 21*t2) + Power(t1,2)*(13 + 22*t2)) + 
            s1*(12 - 62*t2 + 48*Power(t2,2) + 
               8*Power(t1,3)*(2 + 3*t2) + 
               Power(t1,2)*(-20 + 38*t2 + 7*Power(t2,2)) + 
               t1*(-8 + 53*Power(t2,2))) + 
            Power(s,2)*(-2 + 4*Power(t1,3) - 
               4*Power(s1,2)*(-3 + 5*t1) + 25*t2 + 30*Power(t2,2) + 
               3*Power(t1,2)*(-4 + 3*t2) - 
               2*t1*(-5 + 19*t2 + 21*Power(t2,2)) + 
               s1*(-7 + 5*Power(t1,2) - 40*t2 + t1*(6 + 60*t2))) + 
            s*(-2 + 2*Power(t1,3) + Power(s1,3)*(7 + 10*t1) + 4*t2 + 
               28*Power(t2,2) - 32*Power(t2,3) - 
               Power(t1,2)*(6 + 36*t2 + 35*Power(t2,2)) + 
               t1*(6 + 32*t2 - 19*Power(t2,2) + 7*Power(t2,3)) - 
               Power(s1,2)*(-22 + 45*Power(t1,2) + 38*t2 + 
                  3*t1*(1 + 7*t2)) + 
               s1*(-2 + 6*Power(t1,3) - 26*t2 + 63*Power(t2,2) + 
                  26*Power(t1,2)*(1 + 4*t2) + 
                  t1*(-30 - 26*t2 + 4*Power(t2,2))))) + 
         Power(s2,9)*(-6 + 12*t1 - 12*Power(t1,3) + 6*Power(t1,4) - 
            2*Power(s1,3)*t1*(17 + 40*t1 + 15*Power(t1,2)) + 55*t2 - 
            69*t1*t2 - 15*Power(t1,2)*t2 + 17*Power(t1,3)*t2 + 
            12*Power(t1,4)*t2 - 126*Power(t2,2) + 22*t1*Power(t2,2) + 
            80*Power(t1,2)*Power(t2,2) + 22*Power(t1,3)*Power(t2,2) + 
            2*Power(t1,4)*Power(t2,2) + 57*Power(t2,3) + 
            69*t1*Power(t2,3) + 15*Power(t1,2)*Power(t2,3) + 
            3*Power(t1,3)*Power(t2,3) + 
            2*Power(s1,2)*(-21 + 10*Power(t1,4) + 11*t2 + 
               5*Power(t1,2)*(5 + 19*t2) + Power(t1,3)*(26 + 19*t2) + 
               t1*(-40 + 91*t2)) + 
            Power(s,3)*(4 - 13*Power(t1,3) + 
               4*Power(s1,2)*(-2 + 5*t1) + Power(t1,2)*(34 - 48*t2) - 
               54*t2 - 40*Power(t2,2) + 
               s1*(-1 - 23*Power(t1,2) + t1*(12 - 80*t2) + 40*t2) + 
               t1*(-23 + 118*t2 + 70*Power(t2,2))) - 
            s1*(39 - 154*t2 + 75*Power(t2,2) + 
               8*Power(t1,4)*(3 + 2*t2) + 
               3*Power(t1,3)*(-1 + 26*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-27 + 154*t2 + 113*Power(t2,2)) + 
               t1*(-33 - 94*t2 + 229*Power(t2,2))) - 
            Power(s,2)*(19 + 14*Power(t1,4) + 
               5*Power(s1,3)*(3 + 2*t1) + 18*t2 - 4*Power(t2,2) - 
               87*Power(t2,3) + Power(t1,3)*(-29 + 12*t2) + 
               Power(t1,2)*(19 - 147*t2 - 94*Power(t2,2)) + 
               t1*(-23 + 73*t2 - 15*Power(t2,2) + 21*Power(t2,3)) - 
               Power(s1,2)*(-14 + 88*Power(t1,2) + 82*t2 + 
                  t1*(19 + 30*t2)) + 
               s1*(-11 + 37*Power(t1,3) + 24*t2 + 150*Power(t2,2) + 
                  4*Power(t1,2)*(16 + 69*t2) + 
                  t1*(-46 - 94*t2 + 3*Power(t2,2)))) + 
            s*(22 + Power(s1,3)*(7 - 82*t1 - 39*Power(t1,2)) + 
               2*Power(t1,4)*(-4 + t2) - 70*t2 - 123*Power(t2,2) + 
               100*Power(t2,3) + 
               4*Power(t1,3)*(1 + 12*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(38 + 8*t2 + 42*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(-56 + 12*t2 + 213*Power(t2,2) + 57*Power(t2,3)) + 
               Power(s1,2)*(-99 + 90*Power(t1,3) + 58*t2 + 
                  Power(t1,2)*(102 + 91*t2) + t1*(75 + 241*t2)) - 
               s1*(10 + 22*Power(t1,4) - 174*t2 + 137*Power(t2,2) + 
                  4*Power(t1,3)*(22 + 43*t2) + 
                  34*t1*(-2 + 7*t2 + 8*Power(t2,2)) + 
                  Power(t1,2)*(-52 + 100*t2 + 29*Power(t2,2))))) + 
         Power(s2,3)*t1*(-2*Power(-1 + s1,2)*Power(t1,5)*
             (-18 + 9*s1 - 4*t2) + 
            3*t1*(-16 + 7*s1 - t2)*Power(-1 + t2,2) - 
            6*Power(-1 + t2,3) + 2*Power(s,9)*t1*(-2 + t1 + 3*t2) + 
            Power(s,8)*t1*(18 - 20*t1 + 39*Power(t1,2) + 
               2*s1*(2 + t1 - 4*t2) - 40*t2 + 23*t1*t2 + 10*Power(t2,2)) \
- (-1 + s1)*Power(t1,4)*(-78 + 23*Power(s1,2) + s1*(3 - 73*t2) + 
               105*t2 + 20*Power(t2,2)) + 
            Power(t1,2)*(-1 + t2)*
             (-72 - 20*Power(s1,2) - 61*t2 + 33*Power(t2,2) - 
               3*s1*(-47 + 7*t2)) + 
            Power(t1,3)*(12 + 5*Power(s1,3) - 177*t2 + 98*Power(t2,2) + 
               12*Power(t2,3) + Power(s1,2)*(-112 + 47*t2) + 
               s1*(129 + 74*t2 - 88*Power(t2,2))) + 
            Power(s,7)*(12 + 105*Power(t1,4) - 30*t2 + 12*Power(t2,2) + 
               5*Power(t2,3) - Power(t1,3)*(53 + 31*s1 + 37*t2) + 
               Power(t1,2)*(-82 + 113*t2 + 35*Power(t2,2) - 
                  4*s1*(7 + 10*t2)) + 
               t1*(-26 + 122*t2 - 123*Power(t2,2) + 5*Power(t2,3) + 
                  s1*(-35 + 88*t2))) + 
            Power(s,6)*(-66 + 62*Power(t1,5) + 
               Power(t1,4)*(4 - 81*s1 - 42*t2) + 215*t2 - 
               105*Power(t2,2) - 14*Power(t2,3) + 
               Power(t1,3)*(-368 + 24*Power(s1,2) + s1*(75 - 44*t2) + 
                  515*t2 + 25*Power(t2,2)) + 
               Power(t1,2)*(677 - 1108*t2 - 12*Power(s1,2)*t2 - 
                  144*Power(t2,2) + 5*Power(t2,3) + 
                  s1*(172 + 254*t2 - 5*Power(t2,2))) - 
               t1*(108 + 60*t2 - 631*Power(t2,2) + 33*Power(t2,3) + 
                  s1*(-129 + 412*t2 + 6*Power(t2,2)))) + 
            Power(s,3)*(42 + (2 + 6*s1)*Power(t1,7) - 464*t2 + 
               294*Power(t2,2) - 25*Power(t2,3) + 
               2*Power(t1,6)*
                (-23 + 5*Power(s1,2) + 10*t2 - 6*s1*(3 + t2)) + 
               Power(t1,5)*(186 + 7*Power(s1,3) + 
                  Power(s1,2)*(13 - 4*t2) - 104*t2 + s1*(-226 + 76*t2)) \
+ t1*(713 - 2970*t2 - 79*Power(t2,2) + 15*Power(t2,3) + 
                  s1*(-327 + 1252*t2 - 186*Power(t2,2))) + 
               Power(t1,2)*(-1565 + 3873*t2 + 115*Power(t2,2) + 
                  16*Power(t2,3) + 11*Power(s1,2)*(-19 + 40*t2) - 
                  4*s1*(-10 + 521*t2 + 4*Power(t2,2))) + 
               Power(t1,4)*(-522 + 10*Power(s1,3) + 636*t2 - 
                  16*Power(t2,2) + Power(s1,2)*(381 + 38*t2) + 
                  2*s1*(-43 - 250*t2 + 8*Power(t2,2))) + 
               Power(t1,3)*(944 - 105*Power(s1,3) - 2079*t2 - 
                  38*Power(t2,2) - Power(s1,2)*(309 + 248*t2) + 
                  s1*(1305 + 1296*t2 + 10*Power(t2,2)))) + 
            Power(s,5)*(141 - 2*Power(t1,6) + 
               Power(t1,5)*(167 - 28*s1 - 6*t2) - 581*t2 + 
               321*Power(t2,2) + Power(t2,3) + 
               Power(t1,4)*(-690 + 37*Power(s1,2) + s1*(152 - 60*t2) + 
                  332*t2 + 24*Power(t2,2)) + 
               t1*(563 - 785*t2 - 1290*Power(t2,2) + 73*Power(t2,3) + 
                  s1*(-273 + 1006*t2 + 3*Power(t2,2))) - 
               Power(t1,3)*(-1482 + 4*Power(s1,3) + 1743*t2 - 
                  41*Power(t2,2) + 8*Power(t2,3) + 
                  Power(s1,2)*(173 + 15*t2) + 
                  s1*(2 - 224*t2 + 9*Power(t2,2))) + 
               Power(t1,2)*(-1566 + 3187*t2 + 502*Power(t2,2) - 
                  32*Power(t2,3) + Power(s1,2)*(6 + 103*t2) - 
                  2*s1*(301 + 433*t2 + 12*Power(t2,2)))) + 
            Power(s,4)*(-6*Power(t1,7) + 
               Power(t1,6)*(78 + 8*s1 + 8*t2) + 
               6*(-23 + 126*t2 - 76*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,5)*(-423 + 15*Power(s1,2) + 112*t2 - 
                  s1*(57 + 28*t2)) + 
               Power(t1,4)*(1232 + 3*Power(s1,3) + 
                  Power(s1,2)*(-160 + t2) - 615*t2 + 4*Power(t2,2) + 
                  s1*(-307 + 202*t2 - 20*Power(t2,2))) + 
               Power(t1,3)*(-2031 + 34*Power(s1,3) + 2875*t2 - 
                  20*Power(t2,2) + 12*Power(t2,3) + 
                  Power(s1,2)*(515 + 121*t2) - 
                  2*s1*(407 + 443*t2 + 2*Power(t2,2))) + 
               t1*(-923 + 2359*t2 + 1062*Power(t2,2) - 
                  65*Power(t2,3) + s1*(365 - 1426*t2 + 69*Power(t2,2))) \
+ Power(t1,2)*(1856 + Power(s1,2)*(49 - 306*t2) - 4608*t2 - 
                  626*Power(t2,2) + 31*Power(t2,3) + 
                  s1*(809 + 1670*t2 + 77*Power(t2,2)))) + 
            s*(-27 + 6*(-1 + s1)*Power(t1,7) + 51*t2 - 51*Power(t2,2) + 
               19*Power(t2,3) + 
               2*Power(t1,6)*
                (-91 + 3*Power(s1,2) + 2*s1*(-3 + t2) + 2*t2) + 
               Power(t1,5)*(3*Power(s1,3) + Power(s1,2)*(31 - 4*t2) + 
                  21*(-9 + 2*t2) - s1*(83 + 12*t2)) - 
               Power(t1,3)*(-409 + 59*Power(s1,3) - 231*t2 + 
                  51*Power(t2,2) + 24*Power(t2,3) + 
                  Power(s1,2)*(-558 + 145*t2) + 
                  s1*(718 + 32*t2 - 151*Power(t2,2))) + 
               2*Power(t1,4)*
                (258 + 37*Power(s1,3) - 73*t2 + 12*Power(t2,2) - 
                  Power(s1,2)*(78 + 41*t2) + 
                  s1*(73 + 16*t2 + 8*Power(t2,2))) + 
               t1*(170 - 621*t2 + 228*Power(t2,2) + 3*Power(t2,3) - 
                  3*s1*(31 - 82*t2 + 35*Power(t2,2))) + 
               Power(t1,2)*(-691 + 439*t2 + 156*Power(t2,2) - 
                  64*Power(t2,3) + Power(s1,2)*(-133 + 129*t2) + 
                  s1*(754 - 850*t2 + 88*Power(t2,2)))) + 
            Power(s,2)*(30 + (-6 + 4*s1)*Power(t1,7) + 71*t2 - 
               33*Power(t2,2) - 10*Power(t2,3) + 
               4*Power(t1,6)*(12 + 3*Power(s1,2) + s1*(-19 + 2*t2)) + 
               Power(t1,5)*(-11 + 33*Power(s1,2) - 8*Power(s1,3) + 
                  92*t2 - 42*s1*(-5 + 2*t2)) + 
               Power(t1,2)*(1365 + Power(s1,2)*(267 - 334*t2) - 
                  1908*t2 + 56*Power(t2,2) + 11*Power(t2,3) + 
                  s1*(-1006 + 1754*t2 - 99*Power(t2,2))) + 
               Power(t1,4)*(-801 - 64*Power(s1,3) + 10*t2 - 
                  56*Power(t2,2) - 2*Power(s1,2)*(37 + 15*t2) + 
                  s1*(75 + 408*t2 + 8*Power(t2,2))) + 
               Power(t1,3)*(-270 + 129*Power(s1,3) + 511*t2 - 
                  55*Power(t2,2) + 8*Power(t2,3) + 
                  Power(s1,2)*(-503 + 240*t2) - 
                  4*s1*(22 + 158*t2 + 15*Power(t2,2))) + 
               t1*(-355 + 1896*t2 - 397*Power(t2,2) + 5*Power(t2,3) + 
                  s1*(209 - 704*t2 + 204*Power(t2,2))))) + 
         Power(s2,4)*(4*Power(-1 + s1,2)*Power(t1,6)*
             (-8 + 6*s1 - 3*t2) - 
            3*t1*(-10 + 7*s1 - 7*t2)*Power(-1 + t2,2) + 
            3*Power(-1 + t2,3) - 
            3*Power(t1,2)*(-1 + t2)*
             (10 + 51*s1 - 10*Power(s1,2) - 75*t2 + 9*s1*t2 + 
               15*Power(t2,2)) - 
            Power(s,8)*t1*(8 + 17*Power(t1,2) - 12*t2 + 
               t1*(-26 + 2*s1 + 27*t2)) + 
            2*(-1 + s1)*Power(t1,5)*
             (3 + 45*Power(s1,2) + 77*t2 + 20*Power(t2,2) - 
               s1*(74 + 71*t2)) - 
            Power(t1,3)*(208 + 10*Power(s1,3) - 413*t2 + 
               2*Power(t2,2) + 93*Power(t2,3) + 
               2*Power(s1,2)*(-84 + 19*t2) + 
               s1*(-87 + 542*t2 - 225*Power(t2,2))) + 
            Power(t1,4)*(189 + 40*Power(s1,3) + 38*t2 - 
               207*Power(t2,2) - 30*Power(t2,3) - 
               90*Power(s1,2)*(-2 + 3*t2) + 
               s1*(-461 + 306*t2 + 215*Power(t2,2))) + 
            Power(s,7)*(-130*Power(t1,4) + 
               Power(t1,3)*(146 + 6*s1 - 43*t2) - 2*Power(t2,3) + 
               Power(t1,2)*(-55 + 70*t2 - 59*Power(t2,2) + 
                  4*s1*(2 + 9*t2)) + 
               t1*(28 - 56*t2 + 20*Power(t2,2) + Power(t2,3) - 
                  2*s1*(-4 + 8*t2 + Power(t2,2)))) - 
            Power(s,6)*(-6 + 182*Power(t1,5) + 15*t2 - 11*Power(t2,3) - 
               Power(t1,4)*(171 + 115*s1 + 45*t2) + 
               Power(t1,3)*(-170 + 24*Power(s1,2) + s1*(3 - 116*t2) + 
                  607*t2 + 121*Power(t2,2)) + 
               t1*(35 - 156*t2 + 236*Power(t2,2) + 37*Power(t2,3) + 
                  s1*(62 - 138*t2 - 27*Power(t2,2))) + 
               Power(t1,2)*(207 - 271*t2 - 12*Power(s1,2)*t2 - 
                  454*Power(t2,2) + 30*Power(t2,3) + 
                  s1*(47 + 272*t2 + 6*Power(t2,2)))) + 
            s*(5 - 2*(1 + 3*s1)*Power(t1,8) - 8*t2 + 9*Power(t2,2) - 
               4*Power(t2,3) - 
               2*Power(t1,7)*(-72 + 9*Power(s1,2) + 4*t2 - 2*s1*t2) + 
               Power(t1,6)*(431 + 13*Power(s1,3) - 20*t2 - 
                  Power(s1,2)*(133 + 4*t2) + s1*(243 + 4*t2)) + 
               Power(t1,3)*(435 + 116*Power(s1,3) - 853*t2 - 
                  8*Power(t2,2) + 36*Power(t2,3) + 
                  Power(s1,2)*(-853 + 81*t2) + 
                  s1*(476 + 822*t2 - 282*Power(t2,2))) + 
               Power(t1,4)*(-754 - 129*Power(s1,3) + 22*t2 - 
                  235*Power(t2,2) + 16*Power(t2,3) + 
                  Power(s1,2)*(87 + 208*t2) + 
                  s1*(-243 + 216*t2 - 50*Power(t2,2))) + 
               Power(t1,2)*(464 + 356*t2 - 441*Power(t2,2) + 
                  61*Power(t2,3) - 3*Power(s1,2)*(-61 + 59*t2) + 
                  6*s1*(-138 + 125*t2 + 5*Power(t2,2))) + 
               2*Power(t1,5)*
                (-259 + 7*Power(s1,3) - 22*t2 - 52*Power(t2,2) - 
                  Power(s1,2)*(117 + 19*t2) + 
                  s1*(129 + 184*t2 + 12*Power(t2,2))) + 
               t1*(-205 + 555*t2 - 189*Power(t2,2) - 11*Power(t2,3) + 
                  4*s1*(25 - 57*t2 + 24*Power(t2,2)))) + 
            Power(s,2)*(16 - 6*s1*Power(t1,8) - 73*t2 + 
               54*Power(t2,2) - 11*Power(t2,3) - 
               4*Power(t1,7)*
                (-12 + 5*Power(s1,2) + 5*t2 - s1*(4 + 3*t2)) - 
               Power(t1,6)*(-92 + 52*Power(s1,2) + 11*Power(s1,3) + 
                  16*t2 + s1*(-351 + 40*t2)) + 
               Power(t1,2)*(-651 + s1*(865 - 1540*t2) + 701*t2 + 
                  70*Power(t2,2) - 16*Power(t2,3) + 
                  Power(s1,2)*(-345 + 438*t2)) + 
               t1*(608 - 1684*t2 + 384*Power(t2,2) - 23*Power(t2,3) + 
                  s1*(-238 + 566*t2 - 155*Power(t2,2))) + 
               Power(t1,3)*(-1540 - 198*Power(s1,3) - 517*t2 + 
                  85*Power(t2,2) + 30*Power(t2,3) - 
                  5*Power(s1,2)*(-89 + 58*t2) + 
                  s1*(829 + 1224*t2 + 2*Power(t2,2))) + 
               2*Power(t1,5)*
                (99 + 44*Power(s1,3) - 311*t2 - 24*Power(t2,2) - 
                  Power(s1,2)*(245 + 34*t2) + 
                  s1*(209 + 268*t2 + 12*Power(t2,2))) + 
               Power(t1,4)*(1229 + 56*Power(s1,3) + 647*t2 - 
                  138*Power(t2,2) + 4*Power(t2,3) + 
                  Power(s1,2)*(489 + 98*t2) + 
                  s1*(-651 - 1192*t2 + 32*Power(t2,2)))) - 
            Power(s,5)*(31 + 42*Power(t1,6) + 
               Power(t1,5)*(299 - 124*s1 - 22*t2) - 88*t2 + 
               27*Power(t2,2) + 16*Power(t2,3) + 
               Power(t1,4)*(-850 + 101*Power(s1,2) + 
                  s1*(146 - 204*t2) + 920*t2 + 95*Power(t2,2)) + 
               Power(t1,3)*(1605 - 2755*t2 - 254*Power(t2,2) + 
                  28*Power(t2,3) - 3*Power(s1,2)*(67 + 15*t2) + 
                  s1*(82 + 514*t2 - 6*Power(t2,2))) + 
               Power(t1,2)*(-987 + 1812*t2 + 1551*Power(t2,2) - 
                  65*Power(t2,3) + 3*Power(s1,2)*(2 + 41*t2) - 
                  2*s1*(153 + 511*t2 + 69*Power(t2,2))) + 
               t1*(114 + 207*t2 - 891*Power(t2,2) - 35*Power(t2,3) + 
                  s1*(-202 + 452*t2 + 76*Power(t2,2)))) + 
            Power(s,3)*(6*Power(t1,8) - 
               2*Power(t1,7)*(35 + 8*s1 + 6*t2) + 
               2*(-27 + 96*t2 - 63*Power(t2,2) + 11*Power(t2,3)) + 
               Power(t1,6)*(338 - 65*Power(s1,2) - 100*t2 + 
                  s1*(239 + 60*t2)) + 
               Power(t1,5)*(-699 - 36*Power(s1,3) + 
                  Power(s1,2)*(97 - 16*t2) + 382*t2 - 72*Power(t2,2) + 
                  4*s1*(95 - 74*t2 + 10*Power(t2,2))) + 
               Power(t1,4)*(63*Power(s1,3) - 
                  2*Power(s1,2)*(631 + 114*t2) + 
                  2*s1*(590 + 1018*t2 + 21*Power(t2,2)) - 
                  3*(-181 + 776*t2 + 58*Power(t2,2))) + 
               Power(t1,2)*(858 + Power(s1,2)*(255 - 564*t2) - 
                  3090*t2 - 429*Power(t2,2) + 34*Power(t2,3) + 
                  2*s1*(95 + 960*t2 + 72*Power(t2,2))) + 
               Power(t1,3)*(-79 + 100*Power(s1,3) + 3767*t2 + 
                  146*Power(t2,2) + 16*Power(t2,3) + 
                  6*Power(s1,2)*(143 + 93*t2) - 
                  2*s1*(1035 + 1458*t2 + 74*Power(t2,2))) + 
               t1*(-853 + 1844*t2 + 326*Power(t2,2) - 41*Power(t2,3) + 
                  s1*(362 - 840*t2 + 78*Power(t2,2)))) + 
            Power(s,4)*(61 + 22*Power(t1,7) + 
               Power(t1,6)*(-385 + 16*s1 - 16*t2) - 193*t2 + 
               99*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,5)*(1117 - 93*Power(s1,2) - 354*t2 - 
                  56*Power(t2,2) + 4*s1*(51 + 44*t2)) + 
               Power(t1,4)*(-2110 + 435*Power(s1,2) - 14*Power(s1,3) + 
                  2000*t2 - 143*Power(t2,2) + 10*Power(t2,3) + 
                  s1*(244 - 610*t2 + 49*Power(t2,2))) + 
               t1*(549 - 581*t2 - 1184*Power(t2,2) + 55*Power(t2,3) + 
                  s1*(-351 + 790*t2 + 53*Power(t2,2))) + 
               Power(t1,3)*(2088 - 8*Power(s1,3) - 4891*t2 - 
                  834*Power(t2,2) + 39*Power(t2,3) - 
                  Power(s1,2)*(843 + 356*t2) + 
                  s1*(1153 + 2098*t2 + 197*Power(t2,2))) + 
               Power(t1,2)*(-1484 + 3690*t2 + 1686*Power(t2,2) - 
                  69*Power(t2,3) + Power(s1,2)*(-57 + 384*t2) - 
                  s1*(645 + 1790*t2 + 279*Power(t2,2))))) + 
         Power(s2,5)*(-4 + 7*s1 - 84*t1 - 63*s1*t1 + 20*Power(s1,2)*t1 + 
            174*Power(t1,2) - 345*s1*Power(t1,2) - 
            112*Power(s1,2)*Power(t1,2) + 10*Power(s1,3)*Power(t1,2) + 
            12*Power(t1,3) + 509*s1*Power(t1,3) - 
            460*Power(s1,2)*Power(t1,3) - 30*Power(s1,3)*Power(t1,3) - 
            180*Power(t1,4) + 200*s1*Power(t1,4) + 
            190*Power(s1,2)*Power(t1,4) - 180*Power(s1,3)*Power(t1,4) + 
            72*Power(t1,5) - 274*s1*Power(t1,5) + 
            324*Power(s1,2)*Power(t1,5) - 122*Power(s1,3)*Power(t1,5) + 
            10*Power(t1,6) - 34*s1*Power(t1,6) + 
            38*Power(s1,2)*Power(t1,6) - 14*Power(s1,3)*Power(t1,6) - 
            5*t2 - 14*s1*t2 + 267*t1*t2 + 6*s1*t1*t2 - 
            20*Power(s1,2)*t1*t2 - 33*Power(t1,2)*t2 + 
            762*s1*Power(t1,2)*t2 - 18*Power(s1,2)*Power(t1,2)*t2 - 
            593*Power(t1,3)*t2 + 286*s1*Power(t1,3)*t2 + 
            390*Power(s1,2)*Power(t1,3)*t2 + 250*Power(t1,4)*t2 - 
            780*s1*Power(t1,4)*t2 + 510*Power(s1,2)*Power(t1,4)*t2 + 
            106*Power(t1,5)*t2 - 244*s1*Power(t1,5)*t2 + 
            138*Power(s1,2)*Power(t1,5)*t2 + 8*Power(t1,6)*t2 - 
            16*s1*Power(t1,6)*t2 + 8*Power(s1,2)*Power(t1,6)*t2 + 
            22*Power(t2,2) + 7*s1*Power(t2,2) - 182*t1*Power(t2,2) + 
            57*s1*t1*Power(t2,2) - 416*Power(t1,2)*Power(t2,2) - 
            187*s1*Power(t1,2)*Power(t2,2) + 
            306*Power(t1,3)*Power(t2,2) - 
            565*s1*Power(t1,3)*Power(t2,2) + 
            230*Power(t1,4)*Power(t2,2) - 
            280*s1*Power(t1,4)*Power(t2,2) + 
            40*Power(t1,5)*Power(t2,2) - 40*s1*Power(t1,5)*Power(t2,2) - 
            13*Power(t2,3) - t1*Power(t2,3) + 
            165*Power(t1,2)*Power(t2,3) + 145*Power(t1,3)*Power(t2,3) + 
            40*Power(t1,4)*Power(t2,3) + 
            Power(s,7)*(-4 + 51*Power(t1,3) + 6*t2 + 
               Power(t1,2)*(-78 + 5*s1 + 36*t2) + 
               t1*(38 - 4*s1 - 42*t2 + 2*Power(t2,2))) + 
            Power(s,6)*(10 + 196*Power(t1,4) + 
               4*Power(s1,2)*t1*(2*t1 - t2) - 20*t2 + 16*Power(t2,2) + 
               17*Power(t2,3) + 2*Power(t1,3)*(-159 + 5*t2) + 
               Power(t1,2)*(66 + 163*t2 + 150*Power(t2,2)) - 
               t1*(14 - 7*t2 + 101*Power(t2,2) + 7*Power(t2,3)) + 
               s1*(4 - 57*Power(t1,3) - 8*t2 - 8*Power(t2,2) - 
                  7*Power(t1,2)*(1 + 12*t2) + 
                  t1*(14 + 54*t2 + 11*Power(t2,2)))) + 
            Power(s,2)*(-219 - 2*Power(t1,8) + 
               Power(s1,3)*Power(t1,2)*
                (140 + 46*t1 - 301*Power(t1,2) + 40*Power(t1,3)) + 
               484*t2 - 120*Power(t2,2) + 19*Power(t2,3) + 
               2*Power(t1,7)*(9 + 4*t2) + 4*Power(t1,6)*(-59 + 15*t2) + 
               Power(t1,5)*(-361 - 167*t2 + 120*Power(t2,2)) + 
               t1*(-287 + 253*t2 + 57*Power(t2,2) - 5*Power(t2,3)) + 
               Power(t1,4)*(539 + 1396*t2 + 462*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,3)*(-734 - 1399*t2 + 536*Power(t2,2) + 
                  62*Power(t2,3)) + 
               Power(t1,2)*(1282 + 1409*t2 - 388*Power(t2,2) + 
                  66*Power(t2,3)) + 
               Power(s1,2)*t1*
                (193 + 95*Power(t1,5) - 250*t2 + 
                  6*Power(t1,4)*(19 + 7*t2) - 
                  2*Power(t1,2)*(509 + 53*t2) + t1*(53 + 150*t2) + 
                  2*Power(t1,3)*(753 + 182*t2)) + 
               s1*(72 + 10*Power(t1,7) - 148*t2 + 38*Power(t2,2) - 
                  Power(t1,6)*(179 + 52*t2) + 
                  Power(t1,3)*(1577 + 1576*t2 - 238*Power(t2,2)) + 
                  Power(t1,5)*(-690 + 72*t2 - 40*Power(t2,2)) + 
                  t1*(-232 + 586*t2 + 17*Power(t2,2)) - 
                  4*Power(t1,4)*(425 + 496*t2 + 41*Power(t2,2)) + 
                  2*Power(t1,2)*(-451 - 830*t2 + 56*Power(t2,2)))) + 
            Power(s,5)*(-4 + 4*Power(s1,3)*Power(t1,2) + 
               126*Power(t1,5) + 54*t2 - 87*Power(t2,2) - 
               52*Power(t2,3) - 3*Power(t1,4)*(-5 + 6*t2) + 
               Power(t1,3)*(-557 + 1222*t2 + 226*Power(t2,2)) + 
               Power(t1,2)*(779 - 1374*t2 - 740*Power(t2,2) + 
                  75*Power(t2,3)) + 
               t1*(-242 + 406*t2 + 749*Power(t2,2) + 119*Power(t2,3)) + 
               Power(s1,2)*t1*
                (2 + 101*Power(t1,2) + 61*t2 - 5*t1*(19 + 9*t2)) + 
               s1*(-27 - 202*Power(t1,4) + Power(t1,3)*(96 - 300*t2) + 
                  46*t2 + 31*Power(t2,2) + 
                  Power(t1,2)*(14 + 408*t2 + 23*Power(t2,2)) - 
                  2*t1*(31 + 195*t2 + 84*Power(t2,2)))) + 
            Power(s,3)*(191 - 24*Power(t1,7) + 
               2*Power(s1,3)*Power(t1,2)*
                (-5 - 96*t1 + 33*Power(t1,2)) - 256*t2 - 
               218*Power(t2,2) + 32*Power(t2,3) + 
               Power(t1,6)*(295 + 34*t2) + 
               4*Power(t1,5)*(-226 + 51*t2 + 16*Power(t2,2)) + 
               Power(t1,4)*(1357 - 1160*t2 + 310*Power(t2,2)) + 
               2*Power(t1,3)*
                (-385 + 1854*t2 + 507*Power(t2,2) + 24*Power(t2,3)) - 
               2*t1*(99 - 702*t2 - 165*Power(t2,2) + 25*Power(t2,3)) + 
               Power(t1,2)*(247 - 3330*t2 - 678*Power(t2,2) + 
                  86*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-131 + 177*Power(t1,4) + 82*Power(t1,3)*(-3 + t2) + 
                  312*t2 + 2*Power(t1,2)*(937 + 292*t2) - 
                  2*t1*(485 + 303*t2)) - 
               2*s1*(46 + 10*Power(t1,6) - 86*t2 + Power(t2,2) + 
                  2*Power(t1,5)*(148 + 63*t2) + 
                  2*t1*(59 + 209*t2 + 30*Power(t2,2)) + 
                  Power(t1,4)*(193 - 308*t2 + 53*Power(t2,2)) + 
                  2*Power(t1,3)*(427 + 936*t2 + 104*Power(t2,2)) - 
                  Power(t1,2)*(709 + 1486*t2 + 146*Power(t2,2)))) + 
            s*(89 + 2*Power(t1,8) + 
               Power(s1,3)*Power(t1,2)*
                (-114 + 116*t1 - 79*Power(t1,2) - 106*Power(t1,3) + 
                  Power(t1,4)) + 8*Power(t1,7)*(-5 + t2) - 220*t2 + 
               105*Power(t2,2) - 12*Power(t2,3) + 
               Power(t1,6)*(-457 + 10*t2) + 
               2*Power(t1,5)*(69 - 56*t2 + 64*Power(t2,2)) + 
               t1*(92 - 876*t2 + 487*Power(t2,2) - 61*Power(t2,3)) + 
               Power(t1,4)*(1045 + 508*t2 + 474*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(-651 + 378*t2 + 50*Power(t2,2) + 
                  31*Power(t2,3)) + 
               Power(t1,3)*(-218 + 304*t2 + 492*Power(t2,2) + 
                  96*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-111 + 10*Power(t1,6) + Power(t1,2)*(26 - 146*t2) + 
                  107*t2 + Power(t1,5)*(85 + 4*t2) + 
                  2*Power(t1,4)*(317 + 53*t2) + t1*(631 + 87*t2) + 
                  Power(t1,3)*(461 + 276*t2)) + 
               s1*(-33 + 2*Power(t1,8) - 4*Power(t1,7)*(-2 + t2) + 
                  70*t2 - 29*Power(t2,2) - Power(t1,6)*(101 + 20*t2) + 
                  Power(t1,4)*(155 - 1552*t2 - 186*Power(t2,2)) + 
                  t1*(362 - 214*t2 - 96*Power(t2,2)) - 
                  2*Power(t1,5)*(421 + 140*t2 + 24*Power(t2,2)) - 
                  4*Power(t1,3)*(-121 + 127*t2 + 32*Power(t2,2)) + 
                  Power(t1,2)*(-35 - 964*t2 + 165*Power(t2,2)))) + 
            Power(s,4)*(-59 - 26*Power(t1,6) + 
               6*Power(s1,3)*Power(t1,2)*(-5 + 4*t1) - 43*t2 + 
               282*Power(t2,2) + 9*Power(t2,3) + 
               Power(t1,5)*(633 + 32*t2) + 
               Power(t1,4)*(-1489 + 882*t2 + 172*Power(t2,2)) + 
               t1*(503 - 1003*t2 - 1438*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(-1525 + 3479*t2 + 1894*Power(t2,2) + 
                  25*Power(t2,3)) + 
               Power(t1,3)*(2329 - 2870*t2 - 142*Power(t2,2) + 
                  65*Power(t2,3)) + 
               Power(s1,2)*t1*
                (27 + 199*Power(t1,3) - 206*t2 - 
                  Power(t1,2)*(457 + 26*t2) + t1*(629 + 432*t2)) - 
               s1*(-69 + 124*Power(t1,5) + 118*t2 + 37*Power(t2,2) + 
                  Power(t1,4)*(242 + 476*t2) + 
                  Power(t1,3)*(221 - 826*t2 + 29*Power(t2,2)) - 
                  t1*(221 + 794*t2 + 299*Power(t2,2)) + 
                  Power(t1,2)*(584 + 2042*t2 + 501*Power(t2,2))))) + 
         Power(s2,6)*(30 + 6*s1 - 5*Power(s1,2) + 18*t1 + 258*s1*t1 + 
            28*Power(s1,2)*t1 - 5*Power(s1,3)*t1 - 195*Power(t1,2) - 
            36*s1*Power(t1,2) + 445*Power(s1,2)*Power(t1,2) + 
            5*Power(s1,3)*Power(t1,2) + 180*Power(t1,3) - 
            585*s1*Power(t1,3) + 180*Power(s1,2)*Power(t1,3) + 
            180*Power(s1,3)*Power(t1,3) + 225*s1*Power(t1,4) - 
            475*Power(s1,2)*Power(t1,4) + 250*Power(s1,3)*Power(t1,4) - 
            30*Power(t1,5) + 123*s1*Power(t1,5) - 
            164*Power(s1,2)*Power(t1,5) + 71*Power(s1,3)*Power(t1,5) - 
            3*Power(t1,6) + 9*s1*Power(t1,6) - 
            9*Power(s1,2)*Power(t1,6) + 3*Power(s1,3)*Power(t1,6) - 
            70*t2 + 18*s1*t2 + 5*Power(s1,2)*t2 - 330*t1*t2 - 
            404*s1*t1*t2 + 37*Power(s1,2)*t1*t2 + 531*Power(t1,2)*t2 - 
            974*s1*Power(t1,2)*t2 - 250*Power(s1,2)*Power(t1,2)*t2 + 
            165*Power(t1,3)*t2 + 570*s1*Power(t1,3)*t2 - 
            780*Power(s1,2)*Power(t1,3)*t2 - 255*Power(t1,4)*t2 + 
            680*s1*Power(t1,4)*t2 - 455*Power(s1,2)*Power(t1,4)*t2 - 
            39*Power(t1,5)*t2 + 106*s1*Power(t1,5)*t2 - 
            67*Power(s1,2)*Power(t1,5)*t2 - 2*Power(t1,6)*t2 + 
            4*s1*Power(t1,6)*t2 - 2*Power(s1,2)*Power(t1,6)*t2 + 
            25*Power(t2,2) - 24*s1*Power(t2,2) + 466*t1*Power(t2,2) + 
            31*s1*t1*Power(t2,2) + 124*Power(t1,2)*Power(t2,2) + 
            605*s1*Power(t1,2)*Power(t2,2) - 
            450*Power(t1,3)*Power(t2,2) + 
            675*s1*Power(t1,3)*Power(t2,2) - 
            145*Power(t1,4)*Power(t2,2) + 
            205*s1*Power(t1,4)*Power(t2,2) - 
            20*Power(t1,5)*Power(t2,2) + 20*s1*Power(t1,5)*Power(t2,2) + 
            15*Power(t2,3) - 99*t1*Power(t2,3) - 
            255*Power(t1,2)*Power(t2,3) - 135*Power(t1,3)*Power(t2,3) - 
            30*Power(t1,4)*Power(t2,3) + 
            Power(s,6)*(14 - 70*Power(t1,3) - 15*t2 + 2*Power(t2,2) + 
               15*Power(t1,2)*(8 + t2) + 
               t1*(-73 + 26*t2 - 14*Power(t2,2)) + 
               s1*(-2 + 5*Power(t1,2) + t1*(6 + 4*t2))) + 
            Power(s,5)*(17 - 2*Power(s1,3)*t1 - 124*Power(t1,4) - 
               34*t2 - 76*Power(t2,2) - 60*Power(t2,3) + 
               Power(t1,3)*(243 + 70*t2) + 
               Power(t1,2)*(59 - 572*t2 - 221*Power(t2,2)) + 
               t1*(-156 + 340*t2 + 201*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s1,2)*(-37*Power(t1,2) - 10*t2 + 
                  t1*(14 + 15*t2)) + 
               s1*(8 + 134*Power(t1,3) + 30*t2 + 51*Power(t2,2) + 
                  Power(t1,2)*(-57 + 176*t2) - 
                  2*t1*(4 + 41*t2 + 12*Power(t2,2)))) + 
            Power(s,3)*(22 + 46*Power(t1,6) + 
               Power(s1,3)*t1*(-30 + 203*t1 - 52*Power(t1,2)) - 
               160*t2 - 258*Power(t2,2) + 60*Power(t2,3) - 
               2*Power(t1,5)*(218 + 27*t2) + 
               Power(t1,4)*(1168 - 397*t2 - 168*Power(t2,2)) + 
               t1*(-181 + 986*t2 + 884*Power(t2,2) - 154*Power(t2,3)) - 
               Power(t1,3)*(1655 - 1434*t2 + 55*Power(t2,2) + 
                  80*Power(t2,3)) - 
               Power(t1,2)*(-862 + 2756*t2 + 1600*Power(t2,2) + 
                  195*Power(t2,3)) + 
               Power(s1,2)*(23 - 261*Power(t1,4) - 62*t2 - 
                  88*Power(t1,2)*(15 + 8*t2) - 
                  2*Power(t1,3)*(-92 + 55*t2) + t1*(477 + 320*t2)) + 
               s1*(61 + 120*Power(t1,5) + 148*t2 + 34*Power(t2,2) + 
                  60*Power(t1,4)*(11 + 10*t2) + 
                  16*Power(t1,2)*(51 + 203*t2 + 53*Power(t2,2)) + 
                  Power(t1,3)*(304 - 664*t2 + 86*Power(t2,2)) - 
                  2*t1*(219 + 670*t2 + 114*Power(t2,2)))) + 
            s*(-145 + Power(s1,3)*t1*
                (56 - 59*t1 + 116*Power(t1,2) + 301*Power(t1,3) + 
                  6*Power(t1,4)) + Power(t1,6)*(161 - 18*t2) + 446*t2 - 
               258*Power(t2,2) + 56*Power(t2,3) - 
               2*Power(t1,7)*(2 + t2) + 
               Power(t1,5)*(416 + 154*t2 - 60*Power(t2,2)) + 
               Power(t1,2)*(587 + 227*t2 - 351*Power(t2,2) - 
                  269*Power(t2,3)) + 
               t1*(46 + 384*t2 - 57*Power(t2,2) - 19*Power(t2,3)) - 
               Power(t1,4)*(1055 + 97*t2 + 342*Power(t2,2) + 
                  24*Power(t2,3)) - 
               2*Power(t1,3)*
                (3 + 547*t2 + 432*Power(t2,2) + 82*Power(t2,3)) - 
               Power(s1,2)*(-25 + 45*Power(t1,6) + 24*t2 + 
                  10*Power(t1,2)*(-3 + 5*t2) + 
                  8*Power(t1,5)*(24 + 5*t2) + t1*(225 + 107*t2) + 
                  Power(t1,4)*(1305 + 421*t2) + 
                  Power(t1,3)*(220 + 618*t2)) + 
               s1*(-49 - 2*Power(t1,7) - 2*t2 + 35*Power(t2,2) + 
                  2*Power(t1,6)*(23 + 8*t2) + 
                  t1*(4 + 350*t2 - 28*Power(t2,2)) + 
                  4*Power(t1,5)*(65 + 22*t2 + 5*Power(t2,2)) + 
                  Power(t1,4)*(1132 + 1140*t2 + 203*Power(t2,2)) + 
                  2*Power(t1,3)*(-564 + 1152*t2 + 313*Power(t2,2)) + 
                  Power(t1,2)*(-263 - 32*t2 + 404*Power(t2,2)))) + 
            Power(s,4)*(-75 + Power(s1,3)*(23 - 18*t1)*t1 + 
               12*Power(t1,5) + 134*t2 + 207*Power(t2,2) + 
               71*Power(t2,3) - Power(t1,4)*(339 + 16*t2) + 
               Power(t1,3)*(999 - 1166*t2 - 260*Power(t2,2)) + 
               Power(t1,2)*(-1296 + 1609*t2 + 651*Power(t2,2) - 
                  100*Power(t2,3)) - 
               t1*(-546 + 1002*t2 + 1110*Power(t2,2) + 
                  215*Power(t2,3)) + 
               Power(s1,2)*(-4 - 189*Power(t1,3) + 39*t2 + 
                  2*Power(t1,2)*(98 + 19*t2) - t1*(194 + 235*t2)) + 
               s1*(-27 + 218*Power(t1,4) - 102*t2 - 100*Power(t2,2) + 
                  8*Power(t1,3)*(8 + 73*t2) + 
                  Power(t1,2)*(177 - 486*t2 - 25*Power(t2,2)) + 
                  t1*(90 + 772*t2 + 455*Power(t2,2)))) - 
            Power(s,2)*(-121 - 8*Power(t1,7) + 
               Power(s1,3)*t1*
                (42 + 119*t1 - 470*Power(t1,2) + 44*Power(t1,3)) - 
               51*t2 + 218*Power(t2,2) - 50*Power(t2,3) + 
               Power(t1,6)*(62 + 24*t2) + 
               Power(t1,5)*(-489 + 45*t2 + 36*Power(t2,2)) + 
               Power(t1,4)*(68 - 626*t2 + 303*Power(t2,2) + 
                  10*Power(t2,3)) + 
               2*t1*(115 + 518*t2 - 119*Power(t2,2) + 39*Power(t2,3)) + 
               Power(t1,3)*(863 + 2234*t2 + 889*Power(t2,2) + 
                  133*Power(t2,3)) + 
               Power(t1,2)*(-605 - 1082*t2 + 427*Power(t2,2) + 
                  269*Power(t2,3)) + 
               Power(s1,2)*(39 + 193*Power(t1,5) - 52*t2 - 
                  10*Power(t1,2)*(104 + 3*t2) + t1*(244 + 30*t2) + 
                  Power(t1,4)*(205 + 156*t2) + 
                  Power(t1,3)*(2014 + 766*t2)) - 
               s1*(3 + 28*Power(t1,6) - 92*t2 + 4*Power(t2,2) + 
                  3*Power(t1,5)*(149 + 60*t2) + 
                  t1*(412 + 1052*t2 - 78*Power(t2,2)) + 
                  6*Power(t1,4)*(114 - 6*t2 + 19*Power(t2,2)) + 
                  2*Power(t1,2)*(-847 - 650*t2 + 236*Power(t2,2)) + 
                  Power(t1,3)*(1700 + 3486*t2 + 533*Power(t2,2))))) - 
         Power(s2,8)*(-19 + 42*t1 - 18*Power(t1,2) - 8*Power(t1,3) - 
            3*Power(t1,4) + 6*Power(t1,5) - 
            2*Power(s1,3)*(-1 + 9*t1 + 70*Power(t1,2) + 
               75*Power(t1,3) + 15*Power(t1,4)) + 131*t2 - 125*t1*t2 - 
            107*Power(t1,2)*t2 + 69*Power(t1,3)*t2 + 28*Power(t1,4)*t2 + 
            4*Power(t1,5)*t2 - 195*Power(t2,2) - 142*t1*Power(t2,2) + 
            238*Power(t1,2)*Power(t2,2) + 86*Power(t1,3)*Power(t2,2) + 
            13*Power(t1,4)*Power(t2,2) + 55*Power(t2,3) + 
            169*t1*Power(t2,3) + 87*Power(t1,2)*Power(t2,3) + 
            23*Power(t1,3)*Power(t2,3) + 2*Power(t1,4)*Power(t2,3) + 
            2*Power(s1,2)*(-15 + 5*Power(t1,5) - t2 + 
               2*Power(t1,4)*(17 + 8*t2) + 
               15*Power(t1,2)*(-1 + 17*t2) + 
               5*Power(t1,3)*(20 + 27*t2) + t1*(-109 + 99*t2)) + 
            Power(s,4)*(-3*Power(t1,3) + 2*Power(s1,2)*(-1 + 5*t1) + 
               Power(t1,2)*(6 - 99*t2) + 
               s1*(-5 - 40*Power(t1,2) + t1*(28 - 60*t2) + 20*t2) + 
               5*t1*(1 + 32*t2 + 14*Power(t2,2)) - 
               2*(2 + 23*t2 + 15*Power(t2,2))) - 
            s1*(78 - 176*t2 + 40*Power(t2,2) + 4*Power(t1,5)*(4 + t2) + 
               Power(t1,4)*(31 + 66*t2 + 13*Power(t2,2)) + 
               Power(t1,3)*(41 + 318*t2 + 131*Power(t2,2)) + 
               t1*(-5 - 474*t2 + 385*Power(t2,2)) + 
               Power(t1,2)*(-161 + 262*t2 + 439*Power(t2,2))) - 
            Power(s,3)*(81 + 13*Power(s1,3) + 42*Power(t1,4) - 66*t2 - 
               96*Power(t2,2) - 130*Power(t2,3) + 
               Power(t1,3)*(-99 + 57*t2) + 
               Power(s1,2)*(-14 + t1 - 102*Power(t1,2) - 88*t2 - 
                  10*t1*t2) + 
               Power(t1,2)*(154 - 398*t2 - 165*Power(t2,2)) + 
               t1*(-196 + 268*t2 + 70*Power(t2,2) + 35*Power(t2,3)) + 
               s1*(102*Power(t1,3) + Power(t1,2)*(35 + 388*t2) - 
                  2*t1*(-5 + 66*t2 + 5*Power(t2,2)) + 
                  2*(-8 + 38*t2 + 95*Power(t2,2)))) + 
            s*(86 + Power(s1,3)*
                (3 + 2*t1 - 271*Power(t1,2) - 56*Power(t1,3)) - 
               126*t2 - 189*Power(t2,2) + 84*Power(t2,3) + 
               2*Power(t1,5)*(-5 + 3*t2) + 
               Power(t1,4)*(-85 + 6*t2 + 23*Power(t2,2)) + 
               Power(t1,3)*(195 - 87*t2 + 72*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(t1,2)*(-9 + 566*t2 + 431*Power(t2,2) + 
                  97*Power(t2,3)) + 
               t1*(-177 - 365*t2 + 279*Power(t2,2) + 233*Power(t2,3)) + 
               Power(s1,2)*(-29 + 110*Power(t1,4) + 26*t2 + 
                  4*t1*(-83 + 78*t2) + Power(t1,3)*(239 + 151*t2) + 
                  Power(t1,2)*(628 + 589*t2)) - 
               s1*(119 + 28*Power(t1,5) - 212*t2 + 62*Power(t2,2) + 
                  2*Power(t1,4)*(79 + 68*t2) + 
                  Power(t1,3)*(52 + 258*t2 + 66*Power(t2,2)) + 
                  2*t1*(-173 - 56*t2 + 300*Power(t2,2)) + 
                  Power(t1,2)*(-11 + 1162*t2 + 462*Power(t2,2)))) + 
            Power(s,2)*(11 - 20*Power(t1,5) + 
               Power(s1,3)*(20 - 130*t1 - 23*Power(t1,2)) - 161*t2 - 
               150*Power(t2,2) + 119*Power(t2,3) + 
               Power(t1,4)*(48 + 11*t2) + 
               Power(t1,3)*(-138 + 151*t2 + 105*Power(t2,2)) + 
               Power(t1,2)*(279 - 47*t2 - 44*Power(t2,2) + 
                  30*Power(t2,3)) + 
               t1*(-180 + 278*t2 + 644*Power(t2,2) + 155*Power(t2,3)) + 
               Power(s1,2)*(-115 + 179*Power(t1,3) + 8*t2 + 
                  3*Power(t1,2)*(53 + 44*t2) + t1*(272 + 406*t2)) - 
               s1*(-52 + 89*Power(t1,4) - 268*t2 + 92*Power(t2,2) + 
                  Power(t1,3)*(263 + 484*t2) + 
                  8*Power(t1,2)*(-1 - 6*t2 + 5*Power(t2,2)) + 
                  t1*(-60 + 882*t2 + 585*Power(t2,2))))) + 
         Power(s2,7)*(-38 + 66*t1 + 18*Power(t1,2) - 72*Power(t1,3) + 
            18*Power(t1,4) + 6*Power(t1,5) + 2*Power(t1,6) - 
            Power(s1,3)*(-1 - 5*t1 + 90*Power(t1,2) + 260*Power(t1,3) + 
               145*Power(t1,4) + 15*Power(t1,5)) + 154*t2 + 52*t1*t2 - 
            449*Power(t1,2)*t2 + 139*Power(t1,3)*t2 + 
            91*Power(t1,4)*t2 + 13*Power(t1,5)*t2 - 142*Power(t2,2) - 
            438*t1*Power(t2,2) + 244*Power(t1,2)*Power(t2,2) + 
            278*Power(t1,3)*Power(t2,2) + 54*Power(t1,4)*Power(t2,2) + 
            4*Power(t1,5)*Power(t2,2) + 15*Power(t2,3) + 
            195*t1*Power(t2,3) + 207*Power(t1,2)*Power(t2,3) + 
            75*Power(t1,3)*Power(t2,3) + 12*Power(t1,4)*Power(t2,3) + 
            Power(s1,2)*(2*Power(t1,6) - 13*t2 + 
               15*Power(t1,4)*(18 + 13*t2) + Power(t1,5)*(40 + 13*t2) + 
               3*t1*(-64 + 19*t2) + 20*Power(t1,2)*(-19 + 29*t2) + 
               20*Power(t1,3)*(13 + 34*t2)) + 
            Power(s,5)*(2*Power(s1,2)*t1 + 40*Power(t1,3) + 
               s1*(1 - 30*Power(t1,2) + t1*(12 - 24*t2) + 4*t2) - 
               10*Power(t1,2)*(8 + 9*t2) - 4*(4 + t2 + 3*Power(t2,2)) + 
               t1*(61 + 80*t2 + 42*Power(t2,2))) - 
            s1*(60 + 4*Power(t1,6) - 68*t2 - 15*Power(t2,2) + 
               Power(t1,5)*(31 + 26*t2 + 4*Power(t2,2)) + 
               5*Power(t1,4)*(29 + 54*t2 + 16*Power(t2,2)) + 
               5*Power(t1,2)*(-73 - 50*t2 + 149*Power(t2,2)) + 
               t1*(202 - 732*t2 + 275*Power(t2,2)) + 
               Power(t1,3)*(-77 + 754*t2 + 423*Power(t2,2))) + 
            Power(s,4)*(-87 - 5*Power(t1,4) + Power(s1,3)*(-4 + 5*t1) + 
               Power(t1,3)*(17 - 101*t2) + 130*t2 + 134*Power(t2,2) + 
               115*Power(t2,3) + 
               Power(t1,2)*(-217 + 662*t2 + 220*Power(t2,2)) - 
               t1*(-309 + 510*t2 + 190*Power(t2,2) + 35*Power(t2,3)) + 
               Power(s1,2)*(12 + 78*Power(t1,2) + 47*t2 - 
                  3*t1*(9 + 5*t2)) - 
               s1*(1 + 156*Power(t1,3) + 64*t2 + 135*Power(t2,2) + 
                  Power(t1,2)*(-59 + 320*t2) + 
                  t1*(48 - 108*t2 - 25*Power(t2,2)))) + 
            Power(s,3)*(-57*Power(t1,5) + 
               Power(s1,3)*(11 - 90*t1 + 15*Power(t1,2)) + 
               Power(t1,4)*(274 + 23*t2) + 
               Power(t1,3)*(-683 + 553*t2 + 199*Power(t2,2)) + 
               2*(22 - 60*t2 - 95*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(947 - 676*t2 - 315*Power(t2,2) + 
                  75*Power(t2,3)) + 
               t1*(-447 + 716*t2 + 1034*Power(t2,2) + 
                  235*Power(t2,3)) + 
               Power(s1,2)*(-81 + 221*Power(t1,3) - 66*t2 + 
                  Power(t1,2)*(-31 + 58*t2) + t1*(377 + 402*t2)) - 
               s1*(169*Power(t1,4) + 64*Power(t1,3)*(5 + 11*t2) + 
                  Power(t1,2)*(133 - 372*t2 + 10*Power(t2,2)) - 
                  2*(20 + 106*t2 + 33*Power(t2,2)) + 
                  2*t1*(23 + 582*t2 + 340*Power(t2,2)))) + 
            Power(s,2)*(50 - 16*Power(t1,6) + 
               Power(s1,3)*(3 + 82*t1 - 364*Power(t1,2) + 
                  2*Power(t1,3)) + 42*t2 + 100*Power(t2,2) - 
               26*Power(t2,3) + 15*Power(t1,5)*(5 + 2*t2) + 
               Power(t1,4)*(-424 + 47*t2 + 89*Power(t2,2)) + 
               Power(t1,3)*(557 - 460*t2 + 112*Power(t2,2) + 
                  55*Power(t2,3)) + 
               Power(t1,2)*(28 + 1740*t2 + 1043*Power(t2,2) + 
                  217*Power(t2,3)) + 
               t1*(-270 - 639*t2 + 25*Power(t2,2) + 292*Power(t2,3)) + 
               Power(s1,2)*(229*Power(t1,4) + 2*(43 + t2) + 
                  4*t1*(-135 + 4*t2) + Power(t1,3)*(259 + 216*t2) + 
                  Power(t1,2)*(1243 + 794*t2)) - 
               s1*(91 + 89*Power(t1,5) + 228*t2 - 16*Power(t2,2) + 
                  17*Power(t1,4)*(29 + 24*t2) + 
                  Power(t1,3)*(302 + 10*t2 + 111*Power(t2,2)) + 
                  t1*(-706 - 824*t2 + 366*Power(t2,2)) + 
                  Power(t1,2)*(491 + 2824*t2 + 828*Power(t2,2)))) - 
            s*(-117 + Power(s1,3)*
                (11 - 18*t1 + 59*Power(t1,2) + 404*Power(t1,3) + 
                  34*Power(t1,4)) + Power(t1,6)*(2 - 6*t2) + 274*t2 - 
               62*Power(t2,2) + 24*Power(t2,3) + 
               Power(t1,5)*(202 - 8*Power(t2,2)) + 
               t1*(33 + 527*t2 + 119*Power(t2,2) - 241*Power(t2,3)) - 
               Power(t1,4)*(54 - 279*t2 + 121*Power(t2,2) + 
                  8*Power(t2,3)) - 
               Power(t1,3)*(707 + 775*t2 + 404*Power(t2,2) + 
                  96*Power(t2,3)) - 
               Power(t1,2)*(-641 + 299*t2 + 896*Power(t2,2) + 
                  281*Power(t2,3)) - 
               Power(s1,2)*(31 + 90*Power(t1,5) + 30*t2 + 
                  39*Power(t1,4)*(7 + 3*t2) + t1*(-78 + 92*t2) + 
                  3*Power(t1,3)*(443 + 235*t2) + 
                  Power(t1,2)*(-273 + 638*t2)) + 
               s1*(29 + 12*Power(t1,6) + 24*t2 - 6*Power(t2,2) + 
                  28*Power(t1,5)*(5 + 2*t2) + 
                  Power(t1,4)*(243 + 224*t2 + 61*Power(t2,2)) + 
                  2*t1*(77 - 246*t2 + 152*Power(t2,2)) + 
                  2*Power(t1,3)*(287 + 883*t2 + 204*Power(t2,2)) + 
                  Power(t1,2)*(-1152 + 1166*t2 + 927*Power(t2,2))))) + 
         Power(s2,2)*Power(t1,2)*
          (-4*Power(s,9)*t1*(t1 + t2) + 
            Power(-1 + t1 - s1*t1 + t2,2)*
             (Power(t1,2)*(-13 + 5*s1 - 2*t2) + 3*(-1 + t2) - 
               t1*(-16 + s1 + 5*t2)) + 
            Power(s,8)*(6 - 26*Power(t1,3) - 15*t2 + 6*Power(t2,2) + 
               2*Power(t1,2)*(-8 + s1 + 7*t2) + 
               t1*(13 + 8*s1*t2 - 4*Power(t2,2))) + 
            Power(s,7)*(-27 - 28*Power(t1,4) + 118*t2 - 66*Power(t2,2) + 
               3*Power(t2,3) + Power(t1,3)*(9 + 24*s1 + 22*t2) + 
               Power(t1,2)*(155 - 149*t2 - 2*Power(t2,2) + 
                  4*s1*(4 + t2)) + 
               t1*(-96 + 148*t2 + 25*Power(t2,2) + 
                  2*s1*(-3 - 36*t2 + Power(t2,2)))) - 
            Power(s,6)*(-19 + 6*Power(t1,5) + 
               Power(t1,4)*(13 - 20*s1 - 8*t2) + 371*t2 - 
               243*Power(t2,2) + 15*Power(t2,3) + 
               Power(t1,3)*(-290 + 8*Power(s1,2) + s1*(95 - 8*t2) + 
                  165*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(478 - 642*t2 - 4*Power(s1,2)*t2 + 
                  13*Power(t2,2) - 2*Power(t2,3) + s1*(137 + 36*t2)) + 
               t1*(-270 + 694*t2 + 111*Power(t2,2) - 5*Power(t2,3) + 
                  s1*(-48 - 282*t2 + 13*Power(t2,2)))) + 
            Power(s,5)*(92 + 2*Power(t1,6) + 
               2*Power(t1,5)*(-11 + s1 - t2) + 595*t2 - 
               426*Power(t2,2) + 27*Power(t2,3) + 
               Power(t1,4)*(257 - 2*Power(s1,2) - 74*t2 + 
                  s1*(-69 + 4*t2)) + 
               Power(t1,3)*(-926 + 53*Power(s1,2) + 2*Power(s1,3) + 
                  464*t2 + 4*Power(t2,2) + 
                  4*s1*(35 - 10*t2 + Power(t2,2))) + 
               Power(t1,2)*(855 - 1587*t2 + 52*Power(t2,2) - 
                  8*Power(t2,3) - Power(s1,2)*(2 + 31*t2) + 
                  3*s1*(138 + 68*t2 + Power(t2,2))) + 
               t1*(-295 + 1490*t2 + 258*Power(t2,2) - 20*Power(t2,3) + 
                  2*s1*(-68 - 300*t2 + 21*Power(t2,2)))) + 
            Power(s,3)*(233 + 2*(-2 + s1)*Power(t1,6) + 164*t2 - 
               126*Power(t2,2) - 15*Power(t2,3) + 
               Power(t1,5)*(26 - 10*Power(s1,2) + s1*(52 - 12*t2) + 
                  24*t2) - Power(t1,4)*
                (-507 + 7*Power(s1,3) + s1*(180 - 48*t2) + 132*t2 + 
                  4*Power(s1,2)*(9 + t2)) + 
               Power(t1,2)*(604 + Power(s1,2)*(62 - 126*t2) - 1953*t2 + 
                  38*Power(t2,2) + s1*(101 + 852*t2 - 42*Power(t2,2))) + 
               Power(t1,3)*(-1427 + 34*Power(s1,3) + 814*t2 - 
                  40*Power(t2,2) + Power(s1,2)*(25 + 42*t2) + 
                  s1*(-210 - 224*t2 + 8*Power(t2,2))) + 
               t1*(323 + 1482*t2 - 11*Power(t2,2) + 
                  2*s1*(-58 - 292*t2 + 55*Power(t2,2)))) + 
            Power(s,2)*(-119 + 2*(-2 + s1)*Power(t1,6) + 41*t2 - 
               39*Power(t2,2) + 27*Power(t2,3) + 
               Power(t1,4)*(-126 - 12*Power(s1,2) + 19*Power(s1,3) + 
                  s1*(61 - 48*t2) + 64*t2) + 
               4*Power(t1,5)*
                (-22 + 2*Power(s1,2) - t2 + s1*(-22 + 3*t2)) + 
               t1*(-159 - 818*t2 + 205*Power(t2,2) - 25*Power(t2,3) + 
                  s1*(16 + 286*t2 - 87*Power(t2,2))) + 
               Power(t1,3)*(921 - 32*Power(s1,3) + 
                  Power(s1,2)*(163 - 72*t2) - 457*t2 + 20*Power(t2,2) + 
                  s1*(-169 + 244*t2 + 8*Power(t2,2))) + 
               Power(t1,2)*(-425 + 1054*t2 - 55*Power(t2,2) - 
                  10*Power(t2,3) + Power(s1,2)*(-76 + 94*t2) + 
                  s1*(298 - 708*t2 + 78*Power(t2,2)))) - 
            Power(s,4)*(231 + 2*(2 + s1)*Power(t1,6) + 496*t2 - 
               375*Power(t2,2) + 15*Power(t2,3) - 
               4*Power(t1,5)*(11 + (-3 + s1)*t2) - 
               Power(t1,4)*(-506 + s1*(182 - 20*t2) + 146*t2 + 
                  Power(s1,2)*(11 + 2*t2)) + 
               Power(t1,2)*(889 + Power(s1,2)*(15 - 89*t2) - 2285*t2 + 
                  57*Power(t2,2) - 10*Power(t2,3) + 
                  s1*(512 + 572*t2 - 3*Power(t2,2))) + 
               Power(t1,3)*(-1527 + 15*Power(s1,3) + 795*t2 - 
                  20*Power(t2,2) + Power(s1,2)*(119 + 9*t2) + 
                  s1*(-77 - 122*t2 + 12*Power(t2,2))) + 
               t1*(50 + 1847*t2 + 250*Power(t2,2) - 25*Power(t2,3) + 
                  s1*(-183 - 754*t2 + 85*Power(t2,2)))) + 
            s*(30 - 2*(-5 + s1)*Power(t1,6) - 45*t2 + 42*Power(t2,2) - 
               15*Power(t2,3) + 
               2*Power(t1,5)*(57 + s1 + Power(s1,2) - 3*t2 - 2*s1*t2) + 
               Power(t1,4)*(22 - 17*Power(s1,3) - 10*t2 + 
                  3*s1*(-31 + 4*t2) + Power(s1,2)*(62 + 4*t2)) + 
               Power(t1,2)*(246 + Power(s1,2)*(36 - 35*t2) - 375*t2 + 
                  56*Power(t2,2) + 8*Power(t2,3) + 
                  s1*(-227 + 316*t2 - 57*Power(t2,2))) + 
               2*Power(t1,3)*(-197 + 6*Power(s1,3) + 72*t2 + 
                  2*Power(t2,2) + Power(s1,2)*(-71 + 27*t2) + 
                  s1*(151 - 76*t2 - 6*Power(t2,2))) + 
               2*t1*(s1*(9 - 44*t2 + 19*Power(t2,2)) + 
                  2*(-7 + 73*t2 - 36*Power(t2,2) + 5*Power(t2,3))))))*
       R1q(s2))/(s*(-1 + s1)*(-1 + s2)*s2*
       Power(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2),2)*
       Power(s2 - t1,3)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*
       (-1 + t2)) + (8*(Power(s2 - t1,2)*Power(-1 + t1,2)*Power(t1,2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          (2*Power(s2,3)*(-1 + t1) + Power(t1,2)*(-15 + 7*s1 - 2*t2) + 
            3*(-1 + t2) - t1*(-18 + s1 + 7*t2) + 
            Power(s2,2)*(-2*Power(t1,2) + 3*s1*(1 + t1) - 7*t2 + 
               t1*(2 + t2)) + 
            s2*(-15 + s1*(1 - 10*t1 - 3*Power(t1,2)) + 4*t2 + 
               6*t1*(3 + t2) + Power(t1,2)*(-3 + 2*t2))) + 
         2*Power(s,5)*(s2 - t1)*(-1 + t1)*Power(t1,3)*
          (2*Power(t1,4) + Power(t1,3)*(5 - 3*s2 - 6*t2) - 
            s2*(1 + s2)*t2 - t1*
             (-4 + s2 + 3*Power(s2,2)*(-1 + t2) + 3*t2 - 4*s2*t2) + 
            Power(t1,2)*(-7 + Power(s2,2) + 5*t2 + s2*(-4 + 5*t2))) - 
         Power(s,4)*Power(t1,2)*
          (2*Power(s2,4)*(-1 + t1)*
             (3*Power(t1,3) + 2*t2 + t1*(-8 + s1 + 4*t2) - 
               Power(t1,2)*(-9 + s1 + 10*t2)) + 
            Power(s2,3)*(-23*Power(t1,5) + 3*t1*(5 - 3*t2)*t2 + 
               (-2 + t2)*t2 + Power(t1,4)*(-46 + 68*t2) + 
               Power(t1,2)*(-62 + 24*t2 - 7*Power(t2,2)) + 
               Power(t1,3)*(131 - 93*t2 + 3*Power(t2,2)) + 
               s1*t1*(2 + t1 + 17*Power(t1,3) + 5*t2 + 14*t1*t2 - 
                  Power(t1,2)*(32 + 7*t2))) + 
            Power(t1,2)*(Power(t1,5)*(45 - 15*s1 - 14*t2) - 
               3*(4 - 5*t2 + Power(t2,2)) + 
               t1*(43 - 4*s1*(-2 + t2) - 60*t2 + 11*Power(t2,2)) + 
               Power(t1,3)*(112 + s1*(17 - 26*t2) - 96*t2 + 
                  19*Power(t2,2)) + 
               Power(t1,2)*(-77 + 99*t2 - 9*Power(t2,2) + 
                  s1*(-22 + 7*t2)) + 
               Power(t1,4)*(-111 + 44*t2 - 6*Power(t2,2) + 
                  s1*(24 + 11*t2))) + 
            Power(s2,2)*(29*Power(t1,6) + 
               Power(t1,5)*(96 - 43*s1 - 87*t2) + Power(t2,2) + 
               t1*(18 + (-13 + 4*s1)*t2 - 11*Power(t2,2)) + 
               Power(t1,3)*(202 - 114*t2 + 5*Power(t2,2) - 
                  3*s1*(1 + 10*t2)) + 
               Power(t1,4)*(-273 + 139*t2 - 4*Power(t2,2) + 
                  s1*(84 + 17*t2)) + 
               Power(t1,2)*(-72 + 39*t2 + 45*Power(t2,2) - 
                  s1*(2 + 27*t2))) + 
            s2*t1*(-12*Power(t1,6) + (3 - 4*t2)*t2 + 
               Power(t1,5)*(-113 + 43*s1 + 59*t2) + 
               t1*(-31 - 8*s1 + 33*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(137 - 137*t2 - 27*Power(t2,2) + 
                  s1*(22 + 15*t2)) + 
               Power(t1,4)*(311 - 148*t2 + 13*Power(t2,2) - 
                  s1*(80 + 21*t2)) + 
               Power(t1,3)*(-292 + 226*t2 - 29*Power(t2,2) + 
                  s1*(-13 + 42*t2)))) + 
         Power(s,3)*t1*(2*Power(s2,5)*Power(-1 + t1,2)*
             (3*Power(t1,3) + Power(t1,2)*(14 - 3*s1 - 12*t2) + t2 + 
               t1*(-7 + 2*s1 + 2*t2)) - 
            Power(s2,4)*(22*Power(t1,6) + 
               Power(t1,5)*(110 - 43*s1 - 99*t2) + 2*t2*(1 + t2) + 
               Power(t1,2)*(-100 + 5*Power(s1,2) + 88*t2 + 
                  15*Power(t2,2) + 2*s1*(-1 + 7*t2)) + 
               Power(t1,4)*(-374 + 9*Power(s1,2) + 214*t2 - 
                  7*Power(t2,2) + 2*s1*(65 + 7*t2)) + 
               t1*(6 - 20*t2 - 23*Power(t2,2) + s1*(4 + 11*t2)) - 
               Power(t1,3)*(-336 + 2*Power(s1,2) + 185*t2 - 
                  25*Power(t2,2) + s1*(89 + 63*t2))) + 
            Power(s2,3)*(28*Power(t1,7) + 
               Power(t1,6)*(296 - 108*s1 - 137*t2) - 
               Power(t2,2)*(2 + 3*t2) + 
               t1*(-12 + (23 - 7*s1)*t2 + 25*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(t1,5)*(-936 + 39*Power(s1,2) + 271*t2 - 
                  17*Power(t2,2) + s1*(341 + 42*t2)) + 
               Power(t1,3)*(-400 + 342*t2 + 82*Power(t2,2) + 
                  7*Power(t2,3) + Power(s1,2)*(11 + 10*t2) + 
                  s1*(-33 + 65*t2 - 16*Power(t2,2))) + 
               Power(t1,2)*(88 - 145*t2 + 3*Power(s1,2)*t2 - 
                  137*Power(t2,2) + 2*Power(t2,3) + 
                  s1*(17 + 67*t2 - 10*Power(t2,2))) - 
               Power(t1,4)*(-936 + 354*t2 - 61*Power(t2,2) + 
                  Power(t2,3) + Power(s1,2)*(38 + t2) + 
                  s1*(217 + 191*t2 - 2*Power(t2,2)))) - 
            Power(s2,2)*t1*(9*Power(t1,7) + 
               Power(t1,6)*(436 - 125*s1 - 97*t2) + 
               t2*(5 - 15*t2 - 6*Power(t2,2)) + 
               Power(t1,5)*(-1356 + 63*Power(s1,2) + 197*t2 - 
                  29*Power(t2,2) + s1*(404 + 46*t2)) + 
               Power(t1,2)*(230 - 395*t2 + 9*Power(s1,2)*t2 - 
                  241*Power(t2,2) + 30*Power(t2,3) + 
                  s1*(68 + 119*t2 - 42*Power(t2,2))) + 
               Power(t1,4)*(1542 - 485*t2 + 106*Power(t2,2) + 
                  2*Power(t2,3) - 3*Power(s1,2)*(34 + t2) + 
                  s1*(-251 - 211*t2 + 2*Power(t2,2))) + 
               t1*(-28 + 71*t2 + 77*Power(t2,2) + 7*Power(t2,3) + 
                  s1*(-15 - 17*t2 + 4*Power(t2,2))) + 
               Power(t1,3)*(-833 + 704*t2 + 66*Power(t2,2) + 
                  3*Power(t2,3) + Power(s1,2)*(3 + 30*t2) - 
                  9*s1*(9 - 15*t2 + 4*Power(t2,2)))) + 
            Power(t1,3)*(-43 + 5*Power(t1,7) + 67*t2 - 21*Power(t2,2) + 
               3*Power(t2,3) + Power(t1,6)*(-93 + 14*s1 + 7*t2) - 
               Power(t1,5)*(-277 + 12*Power(s1,2) + 3*t2 - 
                  3*Power(t2,2) + 4*s1*(12 + t2)) + 
               t1*(93 - 211*t2 + 72*Power(t2,2) - 7*Power(t2,3) - 
                  7*s1*(-5 + 3*t2)) + 
               Power(t1,4)*(-297 + 20*t2 - 29*Power(t2,2) + 
                  Power(t2,3) + Power(s1,2)*(32 + t2) + 
                  s1*(8 + 42*t2 - 2*Power(t2,2))) - 
               Power(t1,2)*(59 - 246*t2 + 3*Power(s1,2)*t2 + 
                  74*Power(t2,2) + 2*Power(t2,3) - 
                  2*s1*(-53 + 27*t2 + 5*Power(t2,2))) + 
               Power(t1,3)*(117 + Power(s1,2)*(4 - 10*t2) - 126*t2 + 
                  73*Power(t2,2) - 7*Power(t2,3) + 
                  s1*(97 - 119*t2 + 16*Power(t2,2)))) - 
            s2*Power(t1,2)*(-25 + 8*Power(t1,7) + 20*t2 + 
               22*Power(t2,2) + Power(t1,6)*(-327 + 68*s1 + 42*t2) - 
               Power(t1,5)*(-1007 + 45*Power(s1,2) + 91*t2 - 
                  22*Power(t2,2) + s1*(225 + 22*t2)) - 
               Power(t1,2)*(207 - 518*t2 + 9*Power(s1,2)*t2 - 
                  53*Power(t2,2) + 30*Power(t2,3) + 
                  s1*(161 + 9*t2 - 42*Power(t2,2))) + 
               t1*(73 - 177*t2 - 42*Power(t2,2) + 5*Power(t2,3) + 
                  s1*(50 - 11*t2 - 4*Power(t2,2))) + 
               Power(t1,4)*(-1201 + 348*t2 - 129*Power(t2,2) + 
                  4*Power(t2,3) + Power(s1,2)*(98 + 3*t2) + 
                  s1*(117 + 125*t2 - 2*Power(t2,2))) + 
               Power(t1,3)*(672 + Power(s1,2)*(7 - 30*t2) - 660*t2 + 
                  134*Power(t2,2) - 15*Power(t2,3) + 
                  s1*(151 - 203*t2 + 36*Power(t2,2))))) + 
         Power(s,2)*(-2*Power(s2,6)*Power(-1 + t1,3)*t1*
             (-2 + s1 - 3*s1*t1 + Power(t1,2) + t1*(9 - 6*t2)) + 
            Power(s2,5)*(-1 + t1)*
             (7*Power(t1,6) + Power(t1,5)*(109 - 35*s1 - 58*t2) + 
               t2*(2 + t2) + 
               Power(t1,2)*(-50 + 10*Power(s1,2) + 73*t2 + 
                  24*Power(t2,2) - s1*(5 + 3*t2)) + 
               Power(t1,4)*(-281 + 20*Power(s1,2) + 105*t2 - 
                  3*Power(t2,2) + s1*(113 + 3*t2)) + 
               t1*(4 - 23*t2 - 14*Power(t2,2) + s1*(2 + 7*t2)) - 
               Power(t1,3)*(-211 + 2*Power(s1,2) + 99*t2 - 
                  20*Power(t2,2) + s1*(75 + 63*t2))) + 
            Power(t1,4)*(43 + 3*Power(t1,8) + 
               Power(t1,7)*(-54 + 2*s1 - t2) - 68*t2 + 24*Power(t2,2) - 
               3*Power(t2,3) + 
               Power(t1,6)*(224 - 13*Power(s1,2) - s1*(-13 + t2) + 
                  7*t2 + 3*Power(t2,2)) + 
               t1*(-52 + 229*t2 - 85*Power(t2,2) + 13*Power(t2,3) + 
                  s1*(-55 + 32*t2 - 7*Power(t2,2))) + 
               Power(t1,2)*(-98 - 228*t2 + 131*Power(t2,2) - 
                  12*Power(t2,3) + Power(s1,2)*(-1 + 11*t2) + 
                  s1*(174 - 147*t2 + Power(t2,2))) + 
               Power(t1,5)*(-382 + Power(s1,3) + 
                  Power(s1,2)*(83 - 3*t2) - 40*t2 - 6*Power(t2,2) - 
                  Power(t2,3) + s1*(-86 - 12*t2 + 3*Power(t2,2))) + 
               Power(t1,3)*(96 + Power(s1,3) + 
                  Power(s1,2)*(41 - 23*t2) + 12*t2 - 121*Power(t2,2) - 
                  2*Power(t2,3) + s1*(-237 + 220*t2 + 18*Power(t2,2))) \
+ Power(t1,4)*(220 + 10*Power(s1,3) + 89*t2 + 54*Power(t2,2) - 
                  7*Power(t2,3) - Power(s1,2)*(110 + 21*t2) + 
                  s1*(189 - 92*t2 + 21*Power(t2,2)))) + 
            s2*Power(t1,3)*(-63 - 9*Power(t1,8) + 
               Power(t1,7)*(252 - 22*s1 - 5*t2) + 87*t2 - 
               16*Power(t2,2) + 6*Power(t2,3) + 
               Power(t1,6)*(-1021 + 65*Power(s1,2) - 22*t2 - 
                  10*Power(t2,2) + s1*(18 + 5*t2)) + 
               Power(t1,4)*(-1448 - 32*Power(s1,3) + 313*t2 - 
                  286*Power(t2,2) + 40*Power(t2,3) + 
                  2*Power(s1,2)*(194 + 55*t2) + 
                  s1*(-500 + 236*t2 - 90*Power(t2,2))) + 
               Power(t1,5)*(1771 - 10*Power(s1,3) - 23*t2 + 
                  80*Power(t2,2) - 4*Power(t2,3) + 
                  4*Power(s1,2)*(-83 + 3*t2) + 
                  s1*(204 + 30*t2 - 9*Power(t2,2))) + 
               t1*(65 - 431*t2 + 36*Power(t2,2) - 24*Power(t2,3) + 
                  s1*(108 - 20*t2 + 11*Power(t2,2))) + 
               Power(t1,3)*(400 - 6*Power(s1,3) - 701*t2 + 
                  292*Power(t2,2) + 36*Power(t2,3) + 
                  4*Power(s1,2)*(-31 + 15*t2) - 
                  2*s1*(-267 + 217*t2 + 57*Power(t2,2))) + 
               Power(t1,2)*(53 + Power(s1,2)*(3 - 38*t2) + 782*t2 - 
                  96*Power(t2,2) - 6*Power(t2,3) + 
                  s1*(-342 + 183*t2 + 58*Power(t2,2)))) + 
            Power(s2,3)*t1*(2*Power(t1,8) + 
               Power(t1,7)*(504 - 107*s1 - 71*t2) + 
               t2*(-1 + 7*t2 + 4*Power(t2,2)) + 
               Power(t1,6)*(-1884 + 151*Power(s1,2) + 55*t2 - 
                  20*Power(t2,2) + 2*s1*(221 + 8*t2)) + 
               Power(t1,5)*(2757 - 22*Power(s1,3) - 139*t2 + 
                  127*Power(t2,2) + 4*Power(t2,3) + 
                  2*Power(s1,2)*(-199 + 6*t2) + 
                  s1*(-394 - 264*t2 + 3*Power(t2,2))) + 
               t1*(7 - 85*t2 - 125*Power(t2,2) - 28*Power(t2,3) + 
                  s1*(6 + 32*t2 + 19*Power(t2,2))) + 
               Power(t1,2)*(-170 + Power(s1,2)*(1 - 26*t2) + 413*t2 + 
                  426*Power(t2,2) - 18*Power(t2,3) + 
                  s1*(-60 - 96*t2 + 50*Power(t2,2))) - 
               Power(t1,3)*(-828 + 10*Power(s1,3) + 809*t2 + 
                  394*Power(t2,2) - 64*Power(t2,3) + 
                  Power(s1,2)*(74 + 4*t2) + 
                  s1*(-271 + 184*t2 + 54*Power(t2,2))) + 
               Power(t1,4)*(-2044 - 16*Power(s1,3) + 637*t2 - 
                  21*Power(t2,2) + 22*Power(t2,3) + 
                  2*Power(s1,2)*(160 + 81*t2) - 
                  2*s1*(79 - 248*t2 + 81*Power(t2,2)))) + 
            Power(s2,2)*Power(t1,2)*
             (14 + 8*Power(t1,8) + 
               12*Power(s1,3)*Power(t1,3)*(1 + 3*t1 + 2*Power(t1,2)) - 
               32*Power(t2,2) + 7*Power(t1,7)*(-69 + 4*t2) + 
               t1*t2*(220 + 207*t2 + 8*Power(t2,2)) + 
               Power(t1,6)*(1879 + 44*t2 + 14*Power(t2,2)) + 
               Power(t1,3)*(-1119 + 1242*t2 + 74*Power(t2,2) - 
                  96*Power(t2,3)) + 
               Power(t1,4)*(2604 - 786*t2 + 270*Power(t2,2) - 
                  50*Power(t2,3)) + 
               Power(t1,5)*(-3070 + 78*t2 - 137*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(167 - 826*t2 - 396*Power(t2,2) + 
                  62*Power(t2,3)) - 
               Power(s1,2)*Power(t1,2)*
                (3 + 137*Power(t1,4) - 48*t2 + 
                  2*Power(t1,3)*(-260 + 9*t2) + 
                  12*Power(t1,2)*(43 + 17*t2) + 2*t1*(-68 + 21*t2)) + 
               s1*t1*(-59 + 71*Power(t1,6) - 42*t2 - 16*Power(t2,2) - 
                  4*Power(t1,5)*(55 + 3*t2) + 
                  t1*(206 + 40*t2 - 102*Power(t2,2)) + 
                  Power(t1,4)*(-7 + 78*t2 + 6*Power(t2,2)) + 
                  2*Power(t1,3)*(239 - 198*t2 + 89*Power(t2,2)) + 
                  Power(t1,2)*(-469 + 332*t2 + 150*Power(t2,2)))) - 
            Power(s2,4)*(9*Power(t1,8) + 
               Power(t1,7)*(309 - 85*s1 - 95*t2) + 
               Power(t2,2)*(1 + t2) + 
               t1*(2 + (-15 + 2*s1)*t2 + (-22 + 7*s1)*Power(t2,2) - 
                  13*Power(t2,3)) + 
               Power(t1,6)*(-1140 + 86*Power(s1,2) + 211*t2 - 
                  16*Power(t2,2) + s1*(381 + 11*t2)) - 
               Power(t1,3)*(-267 + 3*Power(s1,3) + 388*t2 + 
                  151*Power(t2,2) + 10*Power(t2,3) + 
                  Power(s1,2)*(31 + 9*t2) + 4*s1*(-27 + 14*t2)) + 
               Power(t1,4)*(-917 - 2*Power(s1,3) + 467*t2 - 
                  33*Power(t2,2) + 23*Power(t2,3) + 
                  47*Power(s1,2)*(2 + t2) + 
                  s1*(67 + 304*t2 - 53*Power(t2,2))) - 
               Power(t1,5)*(-1510 + 7*Power(s1,3) + 
                  Power(s1,2)*(149 - 3*t2) + 310*t2 - 105*Power(t2,2) + 
                  3*Power(t2,3) - 3*s1*(-149 - 78*t2 + Power(t2,2))) + 
               Power(t1,2)*(-5*Power(s1,2)*t2 + 
                  s1*(-24 - 27*t2 + 7*Power(t2,2)) + 
                  2*(-20 + 65*t2 + 58*Power(t2,2) + 7*Power(t2,3))))) - 
         s*(s2 - t1)*(-1 + t1)*
          (2*Power(s2,6)*Power(-1 + t1,3)*t1*(-2 + s1 + t2) - 
            Power(s2,5)*(-1 + t1)*
             (t2*(2 - s1 + t2) + Power(t1,4)*(-28 + 7*s1 + 9*t2) + 
               t1*(4 + 4*s1 - 5*Power(s1,2) - 14*t2 + 8*s1*t2 - 
                  11*Power(t2,2)) + 
               Power(t1,2)*(-36 + 17*s1 + 13*t2 + 21*s1*t2 - 
                  5*Power(t2,2)) - 
               Power(t1,3)*(-60 + 28*s1 + 13*Power(s1,2) + 10*t2 - 
                  8*s1*t2 + 3*Power(t2,2))) - 
            Power(t1,3)*(-5 + (5 + 3*s1)*Power(t1,7) + 7*t2 + 
               3*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,6)*(-35 + 7*Power(s1,2) + 3*t2 - 
                  s1*(27 + 4*t2)) + 
               Power(t1,3)*(-131 - 4*Power(s1,3) + 97*t2 + 
                  32*Power(t2,2) + 11*Power(t2,3) + 
                  Power(s1,2)*(-73 + 63*t2) + 
                  s1*(214 - 157*t2 - 40*Power(t2,2))) + 
               Power(t1,2)*(161 + Power(s1,2)*(18 - 13*t2) - 104*t2 + 
                  18*Power(t2,2) + 9*Power(t2,3) + 
                  s1*(-128 + 135*t2 - 32*Power(t2,2))) + 
               Power(t1,4)*(-33 - 19*Power(s1,3) - 42*t2 - 
                  23*Power(t2,2) + Power(t2,3) + 
                  3*Power(s1,2)*(31 + 6*t2) + 
                  s1*(-157 + 33*t2 - 9*Power(t2,2))) + 
               Power(t1,5)*(85 + Power(s1,3) + 7*t2 - Power(t2,2) - 
                  Power(s1,2)*(45 + 2*t2) + 
                  s1*(72 + 22*t2 + Power(t2,2))) + 
               t1*(-47 + 32*t2 - 29*Power(t2,2) + 4*Power(t2,3) + 
                  s1*(23 - 29*t2 + 14*Power(t2,2)))) + 
            Power(s2,4)*(Power(t1,6)*(-71 + 13*s1 + 14*t2) + 
               t2*(s1 - s1*t2 + Power(1 + t2,2)) + 
               Power(t1,5)*(230 - 44*Power(s1,2) - 15*t2 + 
                  s1*(-59 + 22*t2)) + 
               Power(t1,4)*(-268 + 10*Power(s1,3) + 
                  Power(s1,2)*(73 - 9*t2) + 13*t2 - 36*Power(t2,2) + 
                  3*Power(t2,3) + s1*(46 + 40*t2)) - 
               t1*(-4 + 11*t2 + 4*Power(s1,2)*t2 + 19*Power(t2,2) + 
                  9*Power(t2,3) + s1*(-9 + t2 - 9*Power(t2,2))) + 
               Power(t1,2)*(-29 + 5*Power(s1,3) + 
                  Power(s1,2)*(9 - 4*t2) + 44*t2 + 20*Power(t2,2) + 
                  5*Power(t2,3) + s1*(-51 + 35*t2 + 6*Power(t2,2))) + 
               Power(t1,3)*(134 + 7*Power(s1,3) - 46*t2 + 
                  33*Power(t2,2) - 22*Power(t2,3) - 
                  Power(s1,2)*(38 + 49*t2) + 
                  s1*(42 - 97*t2 + 52*Power(t2,2)))) + 
            Power(s2,3)*(t2 + 2*Power(t2,2) - Power(t2,3) - 
               2*Power(t1,7)*(-44 + 7*s1 + 3*t2) + 
               Power(t1,6)*(-309 + 61*Power(s1,2) - 34*t2 + 
                  2*Power(t2,2) - 2*s1*(4 + 7*t2)) + 
               Power(t1,5)*(442 - 29*Power(s1,3) + 111*t2 + 
                  7*Power(t2,2) + 5*Power(t2,3) + 
                  7*Power(s1,2)*(-27 + 4*t2) + 
                  s1*(147 - 16*t2 - 6*Power(t2,2))) + 
               t1*(2 - 23*t2 - 34*Power(t2,2) + 3*Power(t2,3) + 
                  s1*(-1 + 4*t2 + 5*Power(t2,2))) + 
               Power(t1,2)*(-49 + 70*t2 + 146*Power(t2,2) + 
                  4*Power(t2,3) + Power(s1,2)*(7 + 10*t2) - 
                  2*s1*(8 + 12*t2 + 15*Power(t2,2))) + 
               Power(t1,4)*(-354 - 40*Power(s1,3) - 69*t2 + 
                  34*Power(t2,2) + 17*Power(t2,3) + 
                  2*Power(s1,2)*(86 + 85*t2) - 
                  2*s1*(116 - 59*t2 + 73*Power(t2,2))) - 
               Power(t1,3)*(-180 + 19*Power(s1,3) + 
                  Power(s1,2)*(51 - 56*t2) + 50*t2 + 157*Power(t2,2) - 
                  60*Power(t2,3) + s1*(-124 + 68*t2 + 87*Power(t2,2)))) + 
            Power(s2,2)*t1*(1 + 4*Power(t1,7)*(-15 + s1 - t2) - 
               13*Power(t2,2) + 6*Power(t2,3) - 
               Power(t1,6)*(-270 + 49*Power(s1,2) + 2*s1*(-55 + t2) - 
                  34*t2 + Power(t2,2)) + 
               t1*(21 + 56*t2 + 67*Power(t2,2) - 12*Power(t2,3) - 
                  12*s1*(1 + Power(t2,2))) + 
               Power(t1,5)*(-549 + 27*Power(s1,3) + 
                  Power(s1,2)*(256 - 27*t2) - 65*t2 - 15*Power(t2,2) + 
                  2*Power(t2,3) + s1*(-351 - 64*t2 + 14*Power(t2,2))) + 
               Power(t1,2)*(30 - 251*t2 - 92*Power(t2,2) - 
                  20*Power(t2,3) + Power(s1,2)*(-32 + 5*t2) + 
                  s1*(103 - 20*t2 + 35*Power(t2,2))) + 
               Power(t1,4)*(603 + 78*Power(s1,3) - 95*t2 + 
                  90*Power(t2,2) - 36*Power(t2,3) - 
                  Power(s1,2)*(343 + 211*t2) + 
                  s1*(435 + 6*t2 + 127*Power(t2,2))) + 
               Power(t1,3)*(-316 + 27*Power(s1,3) + 
                  Power(s1,2)*(168 - 163*t2) + 325*t2 - 36*Power(t2,2) - 
                  72*Power(t2,3) + s1*(-289 + 80*t2 + 232*Power(t2,2)))) \
+ s2*Power(t1,2)*(-17 + 39*t2 - 19*Power(t2,2) + 3*Power(t2,3) + 
               Power(t1,7)*(5*s1 + 3*(8 + t2)) + 
               Power(t1,6)*(-150 + 26*Power(s1,2) + 5*t2 - 
                  4*Power(t2,2) - s1*(99 + 2*t2)) + 
               Power(t1,3)*(8 - 17*Power(s1,3) - 140*t2 + 
                  151*Power(t2,2) + 40*Power(t2,3) + 
                  2*Power(s1,2)*(-97 + 87*t2) + 
                  s1*(426 - 213*t2 - 191*Power(t2,2))) + 
               Power(t1,5)*(365 - 7*Power(s1,3) - 2*t2 + 8*Power(t2,2) + 
                  Power(t2,3) + 2*Power(s1,2)*(-86 + 3*t2) + 
                  s1*(269 + 75*t2 - 7*Power(t2,2))) - 
               Power(t1,4)*(347 + 64*Power(s1,3) - 44*t2 + 
                  87*Power(t2,2) - 9*Power(t2,3) - 
                  27*Power(s1,2)*(11 + 4*t2) + 
                  7*s1*(59 + t2 + 6*Power(t2,2))) + 
               t1*(-37 - 101*t2 + 35*Power(t2,2) - 21*Power(t2,3) + 
                  s1*(36 - 34*t2 + 22*Power(t2,2))) + 
               Power(t1,2)*(Power(s1,2)*(43 - 24*t2) + 
                  s1*(-224 + 181*t2 - 46*Power(t2,2)) + 
                  2*(77 + 76*t2 - 42*Power(t2,2) + 28*Power(t2,3))))))*
       R1q(t1))/(s*(-1 + s1)*(-1 + s2)*Power(s2 - t1,3)*Power(-1 + t1,3)*t1*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(-1 + t2)) + 
    (8*(2*Power(s,8)*Power(t1,2)*(3 + 3*s1 + 4*t1 - 8*t2) - 
         Power(s,7)*(-9*Power(t1,4) - 2*Power(t2,3) + 
            t1*Power(t2,2)*(-2*s1 + t2) + 
            Power(t1,3)*(44 + 5*s1 + 19*t2) + 
            s2*t1*(-12 + 20*t1 + 39*Power(t1,2) + 4*s1*(-3 + 7*t1) + 
               32*t2 - 97*t1*t2 + 2*Power(t2,2)) + 
            Power(t1,2)*(26 + 52*s1 + 6*Power(s1,2) - 106*t2 - 
               19*s1*t2 + 9*Power(t2,2))) + 
         Power(s,6)*(3*Power(t1,5) + 2*(3 - 5*t2)*Power(t2,2) - 
            Power(t1,4)*(45 + 9*s1 + 11*t2) + 
            t1*t2*(6 + 19*t2 + 9*Power(t2,2) - 2*s1*(1 + 8*t2)) + 
            Power(t1,3)*(99 + 95*t2 - 3*Power(t2,2) + s1*(23 + 14*t2)) + 
            Power(t1,2)*(8 - Power(s1,2)*(-48 + t2) - 252*t2 + 
               67*Power(t2,2) - 5*Power(t2,3) + 
               s1*(173 - 147*t2 + 9*Power(t2,2))) + 
            Power(s2,2)*(75*Power(t1,3) - 6*Power(t1,2)*(-5 + 41*t2) - 
               2*(-3 + 8*t2 + Power(t2,2)) + 
               2*t1*(-26 + 83*t2 + 7*Power(t2,2)) + 
               s1*(6 + 49*Power(t1,2) - 4*t1*(11 + t2))) + 
            s2*(-36*Power(t1,4) + (-6 + 8*s1 - 17*t2)*Power(t2,2) + 
               Power(t1,3)*(140 + 20*s1 + 103*t2) + 
               Power(t1,2)*(13 + 15*Power(s1,2) + s1*(158 - 83*t2) - 
                  444*t2 + 46*Power(t2,2)) + 
               t1*(-40 + 4*Power(s1,2)*(-3 + t2) + 170*t2 - 
                  11*Power(t2,2) + 7*Power(t2,3) + 
                  s1*(-92 + 40*t2 - 11*Power(t2,2))))) + 
         Power(-1 + s2,4)*(2*Power(s2,4)*(-1 + t1)*Power(s1 - t2,2) + 
            2*Power(-1 + t2,3) + 
            (-1 + s1)*Power(t1,5)*(-3 + s1 + 2*t2) - 
            t1*Power(-1 + t2,2)*(-19 + 4*s1 + 5*t2) + 
            Power(t1,2)*(-32 + 2*Power(s1,3) + 53*t2 - 46*Power(t2,2) + 
               9*Power(t2,3) + Power(s1,2)*(-23 + 3*t2) + 
               s1*(24 + 11*t2 - Power(t2,2))) - 
            Power(t1,4)*(6 + Power(s1,3) + Power(s1,2)*(7 - 4*t2) + 
               11*t2 - 12*Power(t2,2) + s1*(-24 + 13*t2 + 2*Power(t2,2))\
) - Power(t1,3)*(-18 + 5*Power(s1,3) + 3*t2 - 11*Power(t2,2) + 
               2*Power(t2,3) - Power(s1,2)*(29 + 5*t2) + 
               s1*(40 + 8*t2 + 5*Power(t2,2))) + 
            Power(s2,3)*(-2 + Power(s1,3)*(1 + 3*t1) + 5*t2 - 
               4*Power(t2,2) - 5*Power(t2,3) + 
               Power(t1,2)*(-2 + 5*t2 - 8*Power(t2,2)) + 
               t1*(4 - 10*t2 + 12*Power(t2,2) + Power(t2,3)) - 
               Power(s1,2)*(1 + 5*Power(t1,2) + 7*t2 + 
                  t1*(-6 + 5*t2)) + 
               s1*(-1 + 3*t2 + 11*Power(t2,2) + 
                  Power(t1,2)*(-1 + 11*t2) + 
                  t1*(2 - 14*t2 + Power(t2,2)))) + 
            s2*(Power(s1,3)*t1*(-4 + 11*t1 + 5*Power(t1,2)) + 
               (-15 + t2)*Power(-1 + t2,2) + 
               Power(t1,4)*(-6 + 5*t2 - 2*Power(t2,2)) + 
               t1*(33 - 37*t2 + 47*Power(t2,2) - 11*Power(t2,3)) + 
               Power(t1,3)*(15 + 15*t2 - 19*Power(t2,2) + 
                  2*Power(t2,3)) - 
               Power(t1,2)*(27 + 14*t2 + 9*Power(t2,2) + 
                  4*Power(t2,3)) - 
               Power(s1,2)*t1*
                (-46 + 3*Power(t1,3) + 6*t2 + 
                  Power(t1,2)*(-16 + 13*t2) + t1*(59 + 17*t2)) + 
               s1*(-(Power(t1,4)*(-7 + t2)) + 4*Power(-1 + t2,2) + 
                  2*t1*(-20 - 19*t2 + 5*Power(t2,2)) + 
                  Power(t1,3)*(-46 + 20*t2 + 5*Power(t2,2)) + 
                  Power(t1,2)*(75 + 27*t2 + 17*Power(t2,2)))) - 
            Power(s2,2)*(1 + Power(s1,3)*(-2 + 7*t1 + 7*Power(t1,2)) + 
               16*t2 + Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,3)*(-5 + 8*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(11 - 3*Power(t2,2) + 5*Power(t2,3)) - 
               t1*(7 + 24*t2 - 10*Power(t2,2) + 15*Power(t2,3)) - 
               Power(s1,2)*(-23 + 5*Power(t1,3) + 3*t2 + 
                  Power(t1,2)*(-13 + 14*t2) + t1*(31 + 19*t2)) + 
               s1*(-16 - 27*t2 + 9*Power(t2,2) + 
                  Power(t1,3)*(2 + 8*t2) + 
                  Power(t1,2)*(-20 - 3*t2 + 4*Power(t2,2)) + 
                  t1*(34 + 22*t2 + 23*Power(t2,2))))) - 
         s*Power(-1 + s2,2)*(2*Power(-1 + t2,2)*(-2 + 5*t2) + 
            2*Power(t1,5)*(6 + 2*Power(s1,2) - 3*t2 + s1*(-7 + 4*t2)) - 
            t1*(-1 + t2)*(82 - 119*t2 + 21*Power(t2,2) + 
               s1*(-24 + 26*t2)) + 
            Power(t1,3)*(94 - 17*Power(s1,3) - 39*t2 + 16*Power(t2,2) - 
               6*Power(t2,3) + Power(s1,2)*(104 + 11*t2) + 
               s1*(-170 + 34*t2 - 19*Power(t2,2))) - 
            Power(t1,4)*(44 + 4*Power(s1,3) + 
               Power(s1,2)*(31 - 16*t2) + 36*t2 - 48*Power(t2,2) + 
               s1*(-110 + 61*t2 + 8*Power(t2,2))) + 
            Power(t1,2)*(-140 + 14*Power(s1,3) + 264*t2 - 
               222*Power(t2,2) + 32*Power(t2,3) + 
               Power(s1,2)*(-119 + 2*t2) + 
               s1*(98 + 53*t2 + 16*Power(t2,2))) + 
            Power(s2,5)*(2*Power(s1,2)*(-4 + 5*t1) + 
               t2*(-5 - Power(t1,2) - 12*t2 + 2*t1*(3 + 7*t2)) - 
               s1*(-3 + Power(t1,2) - 20*t2 + t1*(2 + 24*t2))) + 
            Power(s2,4)*(-9 + Power(s1,3)*(7 + 10*t1) + 
               Power(s1,2)*(2 - 27*Power(t1,2) + t1*(7 - 21*t2) - 
                  38*t2) + 28*t2 - 16*Power(t2,2) - 32*Power(t2,3) + 
               Power(t1,3)*(-4 + 8*t2) + 
               Power(t1,2)*(-5 + 6*t2 - 39*Power(t2,2)) + 
               t1*(18 - 42*t2 + 37*Power(t2,2) + 7*Power(t2,3)) + 
               s1*(-22 + 4*Power(t1,3) + 19*t2 + 63*Power(t2,2) + 
                  Power(t1,2)*(-16 + 67*t2) + 
                  t1*(34 - 50*t2 + 4*Power(t2,2)))) - 
            s2*(61 + Power(s1,3)*t1*
                (24 - 33*t1 + Power(t1,2) + 4*Power(t1,3)) + 
               12*Power(t1,5)*(-1 + t2) - 146*t2 + 93*Power(t2,2) - 
               8*Power(t2,3) - 
               5*Power(t1,4)*(-4 - 9*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(149 - 83*t2 + 98*Power(t2,2) - 
                  18*Power(t2,3)) + 
               Power(t1,3)*(-84 - 80*t2 - 29*Power(t2,2) + 
                  4*Power(t2,3)) + 
               2*t1*(-67 + 126*t2 - 130*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-4*Power(t1,4) + Power(t1,3)*(30 - 16*t2) - 
                  2*(95 + 2*t2) + 3*t1*(69 + 10*t2) + 
                  Power(t1,2)*(-181 + 18*t2)) + 
               s1*(Power(t1,5)*(20 - 8*t2) + 
                  t1*(91 + 184*t2 - 9*Power(t2,2)) + 
                  Power(t1,3)*(271 + 70*t2 + 2*Power(t2,2)) - 
                  2*(7 - 15*t2 + 8*Power(t2,2)) + 
                  Power(t1,4)*(-103 + 42*t2 + 8*Power(t2,2)) - 
                  Power(t1,2)*(265 + 42*t2 + 29*Power(t2,2)))) + 
            Power(s2,2)*(Power(s1,3)*
                (10 - 27*t1 + 33*Power(t1,2) + 14*Power(t1,3)) + 
               2*Power(t1,5)*t2 + 
               Power(t1,4)*(-36 + 42*t2 - 8*Power(t2,2)) + 
               t1*(125 - 95*t2 + 157*Power(t2,2) - 22*Power(t2,3)) + 
               2*(-21 + 38*t2 - 35*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(69 + 33*t2 - 82*Power(t2,2) + 
                  10*Power(t2,3)) - 
               Power(t1,2)*(116 + 58*t2 + 161*Power(t2,2) + 
                  20*Power(t2,3)) - 
               Power(s1,2)*(71 + 19*Power(t1,4) + 6*t2 - 
                  t1*(214 + 7*t2) + Power(t1,2)*(351 + 34*t2) + 
                  Power(t1,3)*(-63 + 57*t2)) + 
               s1*(17 + 2*Power(t1,5) + Power(t1,4)*(40 - 9*t2) + 
                  83*t2 - Power(t2,2) + 
                  t1*(-195 - 168*t2 + 8*Power(t2,2)) + 
                  Power(t1,3)*(-213 + 110*t2 + 21*Power(t2,2)) + 
                  Power(t1,2)*(349 + 312*t2 + 62*Power(t2,2)))) + 
            Power(s2,3)*(-16 + 
               Power(s1,3)*(11 - 35*t1 - 20*Power(t1,2)) + 
               Power(t1,4)*(4 - 9*t2) - 55*t2 - 25*Power(t2,2) + 
               12*Power(t2,3) + 
               Power(t1,3)*(33 - 50*t2 + 37*Power(t2,2)) + 
               Power(t1,2)*(-82 + 58*t2 + 40*Power(t2,2) - 
                  30*Power(t2,3)) + 
               t1*(61 + 56*t2 + 32*Power(t2,2) + 78*Power(t2,3)) + 
               Power(s1,2)*(32*Power(t1,3) + 3*(-37 + 4*t2) + 
                  Power(t1,2)*(-36 + 62*t2) + t1*(199 + 74*t2)) - 
               s1*(-88 + 5*Power(t1,4) - 116*t2 + 30*Power(t2,2) + 
                  Power(t1,3)*(2 + 42*t2) + 
                  Power(t1,2)*(-93 + 58*t2 + 11*Power(t2,2)) + 
                  t1*(174 + 184*t2 + 123*Power(t2,2))))) + 
         Power(s,4)*(2 + 3*t1 - 329*Power(t1,2) + 186*Power(t1,3) - 
            106*Power(t1,4) + 29*Power(t1,5) - 
            Power(s1,3)*(Power(s2,3)*(-4 + 5*t1) + 
               2*Power(s2,2)*t1*(-2 + 13*t1) + 
               s2*t1*(12 + 23*t1 - 18*Power(t1,2)) + 
               Power(t1,2)*(-26 - 7*t1 + Power(t1,2))) - 18*t2 - 
            159*t1*t2 + 197*Power(t1,2)*t2 + 123*Power(t1,3)*t2 - 
            57*Power(t1,4)*t2 + 6*Power(t1,5)*t2 + 30*Power(t2,2) + 
            239*t1*Power(t2,2) + 24*Power(t1,2)*Power(t2,2) - 
            119*Power(t1,3)*Power(t2,2) + 12*Power(t1,4)*Power(t2,2) - 
            10*Power(t2,3) + 25*t1*Power(t2,3) - 
            37*Power(t1,2)*Power(t2,3) + 6*Power(t1,3)*Power(t2,3) + 
            Power(s2,4)*(36 + 30*Power(t1,3) + 
               Power(t1,2)*(50 - 260*t2) - 116*t2 - 30*Power(t2,2) + 
               5*t1*(-21 + 76*t2 + 14*Power(t2,2))) - 
            Power(s2,3)*(-17 + 36*Power(t1,4) + 111*t2 + 
               64*Power(t2,2) + 115*Power(t2,3) - 
               Power(t1,3)*(31 + 270*t2) + 
               Power(t1,2)*(70 + 587*t2 - 60*Power(t2,2)) + 
               t1*(3 - 378*t2 + 90*Power(t2,2) - 35*Power(t2,3))) + 
            s2*(2*Power(t1,5)*(7 + 4*t2) + 
               t2*(23 - 33*t2 - 45*Power(t2,2)) - 
               Power(t1,4)*(112 + 67*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(180 + 161*t2 - 123*Power(t2,2) + 
                  10*Power(t2,3)) - 
               Power(t1,2)*(510 + 98*t2 - 171*Power(t2,2) + 
                  80*Power(t2,3)) + 
               t1*(247 + 111*t2 + 129*Power(t2,2) + 99*Power(t2,3))) + 
            Power(s2,2)*(-34 + 12*Power(t1,5) - 96*t2 - 5*Power(t2,2) - 
               86*Power(t2,3) - 2*Power(t1,4)*(20 + 51*t2) + 
               2*Power(t1,3)*(111 + 99*t2 + 5*Power(t2,2)) - 
               Power(t1,2)*(336 + 260*t2 - 349*Power(t2,2) + 
                  75*Power(t2,3)) + 
               t1*(262 + 412*t2 + 44*Power(t2,2) + 145*Power(t2,3))) + 
            Power(s1,2)*(2*Power(s2,4)*(-1 + 5*t1) + 
               s2*t1*(-194 - 3*Power(t1,3) + 
                  Power(t1,2)*(56 - 45*t2) - 25*t1*(-2 + t2) + 94*t2) + 
               Power(s2,3)*(-1 - 63*Power(t1,2) - 47*t2 + 
                  5*t1*(1 + 3*t2)) + 
               Power(t1,2)*(109 + Power(t1,3) + t1*(5 - 31*t2) - 
                  43*t2 + Power(t1,2)*(-17 + 4*t2)) + 
               Power(s2,2)*(40 + 38*Power(t1,3) - 33*t2 + 
                  Power(t1,2)*(-37 + 32*t2) + t1*(11 + 95*t2))) + 
            s1*(Power(s2,4)*(5*(3 + 4*t2) - 4*t1*(8 + 15*t2)) + 
               Power(s2,3)*(48 + 36*Power(t1,3) + 
                  Power(t1,2)*(62 - 15*t2) - 9*t2 + 135*Power(t2,2) + 
                  t1*(-81 + 202*t2 - 25*Power(t2,2))) + 
               Power(s2,2)*(89 - 44*Power(t1,4) - 73*t2 + 
                  99*Power(t2,2) + Power(t1,3)*(57 + 36*t2) + 
                  t1*(-391 + 198*t2 - 215*Power(t2,2)) + 
                  Power(t1,2)*(185 - 591*t2 + 70*Power(t2,2))) + 
               t1*(4*(17 - 25*t2)*t2 + 2*Power(t1,4)*(5 + t2) - 
                  Power(t1,2)*(199 - 316*t2 + Power(t2,2)) - 
                  Power(t1,3)*(30 + 49*t2 + 2*Power(t2,2)) + 
                  t1*(114 - 491*t2 + 133*Power(t2,2))) + 
               s2*(2 + 14*Power(t1,5) - 36*t2 + 52*Power(t2,2) - 
                  3*Power(t1,4)*(17 + 11*t2) + 
                  t1*(-183 + 346*t2 - 212*Power(t2,2)) + 
                  Power(t1,3)*(-27 + 268*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*(254 - 579*t2 + 153*Power(t2,2))))) + 
         Power(s,2)*(6*t2*(2 - 5*t2 + 3*Power(t2,2)) + 
            Power(t1,5)*(29 + 6*Power(s1,2) - 4*t2 + 2*s1*(-7 + 6*t2)) - 
            Power(t1,4)*(127 + 6*Power(s1,3) + 49*t2 - 72*Power(t2,2) - 
               8*Power(s1,2)*(-7 + 3*t2) + 
               s1*(-199 + 114*t2 + 12*Power(t2,2))) - 
            Power(t1,3)*(-213 + 18*Power(s1,3) + 
               6*Power(s1,2)*(-21 + t2) + 87*t2 + 49*Power(t2,2) + 
               4*Power(t2,3) + s1*(304 - 222*t2 + 26*Power(t2,2))) + 
            t1*(122 - 380*t2 + 289*Power(t2,2) - 29*Power(t2,3) - 
               2*s1*(22 - 59*t2 + 36*Power(t2,2))) + 
            Power(t1,2)*(-221 + 36*Power(s1,3) + 562*t2 - 
               397*Power(t2,2) + 33*Power(t2,3) - 
               Power(s1,2)*(198 + 23*t2) + 
               s1*(75 + 19*t2 + 83*Power(t2,2))) + 
            Power(s2,6)*(-Power(t1,3) + 4*Power(s1,2)*(-3 + 5*t1) + 
               Power(t1,2)*(10 - 22*t2) - 
               6*(-1 + 6*t2 + 5*Power(t2,2)) + 
               t1*(-15 + 62*t2 + 42*Power(t2,2)) + 
               s1*(7 - 7*Power(t1,2) + 40*t2 - 4*t1*(1 + 15*t2))) + 
            Power(s2,5)*(-23 + 5*Power(s1,3)*(3 + 2*t1) + 90*t2 - 
               14*Power(t2,2) - 87*Power(t2,3) + 
               Power(t1,3)*(-23 + 59*t2) - 
               Power(t1,2)*(17 + 38*t2 + 66*Power(t2,2)) + 
               t1*(61 - 138*t2 - 15*Power(t2,2) + 21*Power(t2,3)) - 
               Power(s1,2)*(-2 + 67*Power(t1,2) + 82*t2 + 
                  t1*(11 + 30*t2)) + 
               s1*(-53 + 16*Power(t1,3) + 6*t2 + 150*Power(t2,2) + 
                  Power(t1,2)*(-31 + 151*t2) + 
                  3*t1*(33 + 4*t2 + Power(t2,2)))) + 
            Power(s2,2)*(7 + 
               Power(s1,3)*(14 + 27*t1 - 31*Power(t1,2) + 
                  4*Power(t1,3) - 6*Power(t1,4)) + 
               Power(t1,5)*(30 - 20*t2) + 96*t2 - 48*Power(t2,2) - 
               4*Power(t2,3) + 
               2*Power(t1,4)*(9 - 40*t2 + 32*Power(t2,2)) + 
               Power(t1,3)*(70 + 76*t2 + 112*Power(t2,2) - 
                  12*Power(t2,3)) - 
               t1*(39 + 24*t2 - 358*Power(t2,2) + 30*Power(t2,3)) + 
               Power(t1,2)*(-30 + 288*t2 - 208*Power(t2,2) + 
                  34*Power(t2,3)) + 
               Power(s1,2)*(-9 + 6*Power(t1,5) + t1*(98 - 18*t2) - 
                  34*t2 + 12*Power(t1,4)*(-3 + 2*t2) + 
                  3*Power(t1,3)*(45 + 2*t2) + Power(t1,2)*(-94 + 20*t2)\
) + s1*(2*Power(t1,5)*(-17 + 6*t2) + 
                  Power(t1,2)*(-58 + 242*t2 - 8*Power(t2,2)) + 
                  3*(-53 + 34*t2 + 2*Power(t2,2)) - 
                  2*Power(t1,4)*(-40 + 27*t2 + 6*Power(t2,2)) - 
                  2*Power(t1,3)*(190 + 66*t2 + 7*Power(t2,2)) + 
                  t1*(91 - 468*t2 + 22*Power(t2,2)))) - 
            s2*(57 + Power(s1,3)*t1*
                (48 + 4*t1 - 21*Power(t1,2) + 4*Power(t1,3)) - 210*t2 + 
               160*Power(t2,2) - 5*Power(t2,3) + 
               2*Power(t1,5)*(5 + 8*t2) + 
               Power(t1,4)*(-2 + 38*t2 - 36*Power(t2,2)) + 
               Power(t1,2)*(30 + 168*t2 + 16*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,3)*(11 - 101*t2 - 74*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-56 + 304*t2 - 223*Power(t2,2) + 19*Power(t2,3)) - 
               2*Power(s1,2)*t1*
                (88 + 2*Power(t1,4) + Power(t1,2)*(84 - 13*t2) + 
                  34*t2 - t1*(41 + 5*t2) + Power(t1,3)*(-19 + 8*t2)) + 
               s1*(-18 + Power(t1,5)*(26 - 8*t2) + 52*t2 - 
                  32*Power(t2,2) + 
                  Power(t1,3)*(93 + 224*t2 - 6*Power(t2,2)) - 
                  Power(t1,2)*(113 + 79*t2 + 2*Power(t2,2)) + 
                  2*Power(t1,4)*(-70 + 13*t2 + 4*Power(t2,2)) + 
                  t1*(-171 + 236*t2 + 27*Power(t2,2)))) + 
            Power(s2,3)*(Power(s1,3)*
                (-13 + 28*t1 + 26*Power(t1,2) + 25*Power(t1,3)) + 
               Power(t1,5)*(-2 + 8*t2) - 
               2*Power(t1,4)*(51 - 59*t2 + 6*Power(t2,2)) - 
               21*Power(t1,2)*(7 + 10*t2 + 22*Power(t2,2)) + 
               t1*(181 - 414*t2 + 232*Power(t2,2) - 98*Power(t2,3)) - 
               2*(19 - 78*t2 + 97*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,3)*(80 + 80*t2 - 202*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(s1,2)*(t1 - 30*Power(t1,4) + 
                  Power(t1,3)*(74 - 102*t2) + 2*(-22 + t2) - 
                  38*t1*t2 - Power(t1,2)*(457 + 94*t2)) + 
               s1*(-63 + 10*Power(t1,5) + Power(t1,4)*(96 - 30*t2) + 
                  198*t2 + 26*Power(t2,2) + 
                  4*t1*(-5 - 22*t2 + 22*Power(t2,2)) + 
                  Power(t1,3)*(-265 + 272*t2 + 34*Power(t2,2)) + 
                  2*Power(t1,2)*(284 + 349*t2 + 55*Power(t2,2)))) + 
            Power(s2,4)*(-55 + Power(t1,5) - 
               Power(s1,3)*t1*(49 + 27*t1) + Power(t1,4)*(17 - 47*t2) - 
               112*t2 - 4*Power(t2,2) + 74*Power(t2,3) + 
               5*Power(t1,3)*(24 - 33*t2 + 13*Power(t2,2)) + 
               Power(t1,2)*(-237 + 292*t2 + 189*Power(t2,2) - 
                  75*Power(t2,3)) + 
               t1*(162 + 142*t2 + 151*Power(t2,2) + 155*Power(t2,3)) + 
               Power(s1,2)*(-129 + 73*Power(t1,3) + 50*t2 + 
                  2*t1*(202 + 73*t2) + Power(t1,2)*(-30 + 107*t2)) - 
               s1*(19*Power(t1,4) + Power(t1,3)*(30 + 74*t2) + 
                  Power(t1,2)*(-172 + 357*t2 - 5*Power(t2,2)) + 
                  2*(-101 - 61*t2 + 59*Power(t2,2)) + 
                  t1*(453 + 398*t2 + 270*Power(t2,2))))) + 
         Power(s,3)*(-2*Power(t1,5)*
             (20 + s1 + 2*Power(s1,2) + 2*t2 + 4*s1*t2) - 
            2*(2 - 6*t2 + 5*Power(t2,3)) + 
            Power(t1,2)*(301 - 44*Power(s1,3) - 590*t2 + 
               279*Power(t2,2) + 8*Power(t2,3) + 
               Power(s1,2)*(80 + 52*t2) + 
               s1*(10 + 241*t2 - 152*Power(t2,2))) + 
            Power(t1,4)*(109 + 4*Power(s1,3) - 
               16*Power(s1,2)*(-3 + t2) + 52*t2 - 48*Power(t2,2) + 
               2*s1*(-58 + 53*t2 + 4*Power(t2,2))) + 
            Power(t1,3)*(-288 + 2*Power(s1,3) + 27*t2 + 
               136*Power(t2,2) - 4*Power(t2,3) + 
               Power(s1,2)*(-56 + 34*t2) + 
               2*s1*(170 - 198*t2 + 7*Power(t2,2))) + 
            t1*(-68 + 358*t2 - 336*Power(t2,2) + 5*Power(t2,3) + 
               2*s1*(14 - 66*t2 + 55*Power(t2,2))) + 
            Power(s2,5)*(-24 + Power(s1,2)*(8 - 20*t1) - 
               3*Power(t1,3) + 94*t2 + 40*Power(t2,2) + 
               3*Power(t1,2)*(-12 + 37*t2) + 
               t1*(61 - 220*t2 - 70*Power(t2,2)) + 
               s1*(-9 + 14*Power(t1,2) - 40*t2 + t1*(8 + 80*t2))) - 
            Power(s2,2)*(Power(s1,3)*
                (6 + 21*t1 - 37*Power(t1,2) + 31*Power(t1,3)) + 
               12*Power(t1,5)*t2 - 
               2*Power(t1,4)*(55 - 18*t2 + 4*Power(t2,2)) + 
               4*(-21 + t2 - 21*Power(t2,2) - 4*Power(t2,3)) + 
               2*Power(t1,3)*
                (39 - 14*t2 - 90*Power(t2,2) + 10*Power(t2,3)) - 
               4*Power(t1,2)*
                (106 - 52*t2 + 55*Power(t2,2) + 20*Power(t2,3)) + 
               t1*(369 - 422*t2 + 282*Power(t2,2) + 82*Power(t2,3)) + 
               Power(s1,2)*(73 - 16*Power(t1,4) + 
                  Power(t1,3)*(127 - 94*t2) - 48*t2 - 
                  Power(t1,2)*(299 + 20*t2) + 2*t1*(35 + 53*t2)) + 
               s1*(-21 + 18*Power(t1,5) + Power(t1,4)*(44 - 46*t2) - 
                  46*t2 + 62*Power(t2,2) + 
                  2*Power(t1,3)*(-95 + 142*t2 + 13*Power(t2,2)) - 
                  4*t1*(-8 + 28*t2 + 55*Power(t2,2)) + 
                  2*Power(t1,2)*(222 + 53*t2 + 78*Power(t2,2)))) + 
            Power(s2,4)*(13 - 13*Power(s1,3) + 9*Power(t1,4) + 
               Power(t1,3)*(37 - 175*t2) - 16*t2 + 56*Power(t2,2) + 
               130*Power(t2,3) + 
               Power(t1,2)*(36 + 238*t2 + 25*Power(t2,2)) + 
               t1*(-68 + 20*t2 + 70*Power(t2,2) - 35*Power(t2,3)) + 
               Power(s1,2)*(16 + 92*Power(t1,2) + 88*t2 + 
                  t1*(-13 + 10*t2)) - 
               s1*(29*Power(t1,3) + Power(t1,2)*(-17 + 135*t2) + 
                  t1*(83 + 108*t2 - 10*Power(t2,2)) + 
                  2*(-9 + 7*t2 + 95*Power(t2,2)))) + 
            s2*(11 - 26*Power(t1,5) + 
               Power(s1,3)*t1*
                (40 + 20*t1 - 31*Power(t1,2) + 4*Power(t1,3)) - 
               124*t2 + 146*Power(t2,2) + 
               Power(t1,4)*(-8 + 30*t2 - 40*Power(t2,2)) - 
               2*Power(t1,3)*
                (92 - 2*t2 - 51*Power(t2,2) + 4*Power(t2,3)) - 
               2*t1*(140 - 220*t2 + 137*Power(t2,2) + 20*Power(t2,3)) + 
               Power(t1,2)*(354 - 233*t2 + 216*Power(t2,2) + 
                  36*Power(t2,3)) + 
               Power(s1,2)*t1*
                (92 - 4*Power(t1,4) - 124*t2 - 
                  8*Power(t1,3)*(-5 + 2*t2) + 
                  8*Power(t1,2)*(-21 + 8*t2) + t1*(91 + 32*t2)) - 
               s1*(10 - 56*t2 + 48*Power(t2,2) + 
                  Power(t1,5)*(-4 + 8*t2) + 
                  Power(t1,4)*(92 - 88*t2 - 8*Power(t2,2)) + 
                  2*Power(t1,3)*(-125 + 94*t2 + 2*Power(t2,2)) - 
                  3*t1*(-65 - 64*t2 + 66*Power(t2,2)) + 
                  Power(t1,2)*(85 - 68*t2 + 138*Power(t2,2)))) + 
            Power(s2,3)*(100 - 6*Power(t1,5) + 
               Power(s1,3)*(-1 + 9*t1 + 31*Power(t1,2)) + 6*t2 - 
               6*Power(t2,2) + 24*Power(t2,3) + 
               Power(t1,4)*(-20 + 98*t2) - 
               2*Power(t1,3)*(88 - 42*t2 + 25*Power(t2,2)) - 
               4*t1*(76 + 27*t2 + 45*Power(t2,2) + 50*Power(t2,3)) + 
               Power(t1,2)*(333 - 294*t2 - 276*Power(t2,2) + 
                  100*Power(t2,3)) + 
               Power(s1,2)*(-77*Power(t1,3) + 
                  Power(t1,2)*(106 - 88*t2) + 5*(9 + 8*t2) - 
                  3*t1*(107 + 44*t2)) + 
               s1*(36*Power(t1,4) + Power(t1,3)*(13 + 36*t2) - 
                  4*(28 + 13*Power(t2,2)) - 
                  2*Power(t1,2)*(66 - 238*t2 + 25*Power(t2,2)) + 
                  t1*(454 + 256*t2 + 310*Power(t2,2))))) - 
         Power(s,5)*(2*Power(t1,5)*(6 + 2*s1 + t2) - 
            6*t2*(1 - 4*t2 + 3*Power(t2,2)) - 
            Power(t1,4)*(98 + Power(s1,2) + 40*t2 + s1*(42 + 9*t2)) + 
            t1*(-6 - 17*t2 + 100*Power(t2,2) + 25*Power(t2,3) + 
               s1*(4 + 10*t2 - 54*Power(t2,2))) + 
            Power(t1,3)*(122 + 3*Power(s1,3) + 175*t2 - 
               9*Power(s1,2)*t2 - 40*Power(t2,2) + 2*Power(t2,3) + 
               s1*(-15 + 114*t2 + Power(t2,2))) + 
            Power(t1,2)*(-153 + 6*Power(s1,3) + 
               Power(s1,2)*(129 - 14*t2) - 204*t2 + 140*Power(t2,2) - 
               24*Power(t2,3) + s1*(252 - 401*t2 + 56*Power(t2,2))) + 
            Power(s2,3)*(2*Power(s1,2)*t1 + 70*Power(t1,3) - 
               5*Power(t1,2)*(-8 + 67*t2) - 
               3*(-8 + 23*t2 + 4*Power(t2,2)) + 
               t1*(-99 + 350*t2 + 42*Power(t2,2)) + 
               s1*(35*Power(t1,2) + 4*(4 + t2) - 2*t1*(29 + 12*t2))) - 
            Power(s2,2)*(-14 + 2*Power(s1,3)*t1 + 54*Power(t1,4) + 
               70*t2 + 32*Power(t2,2) + 60*Power(t2,3) - 
               5*Power(t1,3)*(29 + 46*t2) + 
               Power(t1,2)*(61 + 728*t2 - 87*Power(t2,2)) + 
               t1*(64 - 474*t2 + 51*Power(t2,2) - 21*Power(t2,3)) + 
               Power(s1,2)*(-6 + 7*Power(t1,2) + 10*t2 - 
                  5*t1*(-4 + 3*t2)) + 
               s1*(-40 - 34*Power(t1,3) + 9*t2 - 51*Power(t2,2) + 
                  Power(t1,2)*(-167 + 119*t2) + 
                  t1*(193 - 150*t2 + 24*Power(t2,2)))) + 
            s2*(10*Power(t1,5) - Power(t1,4)*(92 + 31*s1 + 53*t2) - 
               t2*(-6 + s1*(10 - 32*t2) + t2 + 52*Power(t2,2)) + 
               Power(t1,3)*(229 + 7*Power(s1,2) + 270*t2 - 
                  7*Power(t2,2) + s1*(71 + 46*t2)) + 
               t1*(38 + 370*t2 + 16*Power(t2,2) + 56*Power(t2,3) + 
                  Power(s1,2)*(-82 + 32*t2) + 
                  s1*(-268 + 172*t2 - 87*Power(t2,2))) + 
               Power(t1,2)*(-9*Power(s1,3) + 2*Power(s1,2)*(38 + t2) + 
                  s1*(298 - 468*t2 + 41*Power(t2,2)) - 
                  3*(65 + 193*t2 - 82*Power(t2,2) + 10*Power(t2,3))))))*
       R2q(s))/(s*(-1 + s1)*(-1 + s2)*
       Power(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2),2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(-1 + t2)) - 
    (8*(Power(s,12)*Power(t1,2)*(t1 + t2) + 
         Power(-1 + s2,6)*(s2 - t1)*(-1 + t1)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         Power(s,11)*t1*(8*Power(t1,3) + 
            Power(t1,2)*(-7 + 2*s1 - 8*s2 - t2) + 2*s2*t2 + 
            t1*(1 + s2*(2 + s1 - 9*t2) - (11 + s1)*t2 + Power(t2,2))) + 
         Power(s,10)*(11*Power(t1,5) + t1*(s1 - t2)*Power(t2,2) + 
            Power(t2,3) + Power(t1,4)*(-62 - 4*s1 + t2) + 
            Power(s2,2)*(28*Power(t1,3) - 
               2*Power(t1,2)*(7 + 4*s1 - 18*t2) + 
               t1*(1 + 2*s1 - 16*t2) + t2) - 
            Power(t1,2)*(10 + s1 - 51*t2 - 8*s1*t2 + 7*Power(t2,2)) + 
            Power(t1,3)*(21 - 3*Power(s1,2) + 2*t2 + s1*(-19 + 3*t2)) - 
            s2*t1*(-2 + 56*Power(t1,3) + 
               Power(t1,2)*(-59 + 10*s1 - 8*t2) + 2*(10 + s1)*t2 - 
               Power(t2,2) + t1*
                (16 + Power(s1,2) + s1*(2 - 8*t2) - 72*t2 + 
                  9*Power(t2,2)))) + 
         Power(s,9)*(-((75 + 7*s1)*Power(t1,5)) + 6*Power(t1,6) + 
            (3 - 7*t2)*Power(t2,2) + 
            Power(t1,4)*(182 + 42*s1 - 3*Power(s1,2) - 26*t2 + 
               3*Power(t2,2)) - 
            Power(t1,2)*(-43 + 128*t2 - 19*Power(t2,2) + Power(t2,3) + 
               Power(s1,2)*(2 + t2) + 2*s1*(-3 + 8*t2)) + 
            t1*t2*(3 + 8*t2 + 11*Power(t2,2) - s1*(1 + 10*t2)) + 
            Power(s2,3)*(-56*Power(t1,3) + 
               s1*(1 - 14*t1 + 28*Power(t1,2)) + 
               Power(t1,2)*(42 - 84*t2) - 7*t2 + t1*(-6 + 56*t2)) - 
            Power(t1,3)*(61 + Power(s1,2)*(-28 + t2) - 35*t2 - 
               6*Power(t2,2) + Power(t2,3) + 
               s1*(-73 + 36*t2 - 2*Power(t2,2))) + 
            Power(s2,2)*(1 + 168*Power(t1,4) - (9 + s1)*t2 - 
               t1*(11 + 2*Power(s1,2) + s1*(8 - 12*t2) - 115*t2 + 
                  6*Power(t2,2)) + 
               Power(t1,2)*(71 + 6*Power(s1,2) + s1*(22 - 28*t2) - 
                  198*t2 + 36*Power(t2,2)) + 
               2*Power(t1,3)*(7*s1 - 2*(51 + 7*t2))) - 
            s2*(66*Power(t1,5) + Power(t1,4)*(-329 - 25*s1 + 6*t2) + 
               Power(t2,2)*(3 - 4*s1 + 12*t2) + 
               Power(t1,3)*(199 - 12*Power(s1,2) - 13*t2 + 
                  Power(t2,2) + 2*s1*(-25 + 9*t2)) + 
               Power(t1,2)*(-60 + Power(s1,2)*(1 - 2*t2) + 229*t2 - 
                  34*Power(t2,2) + 3*s1*(6 + 11*t2)) + 
               t1*(18 - 77*t2 - 2*Power(s1,2)*t2 + 5*Power(t2,2) - 
                  10*Power(t2,3) + s1*(2 - 15*t2 + 10*Power(t2,2))))) + 
         s*Power(-1 + s2,4)*(2*Power(s2,6)*(-1 + t1)*Power(s1 - t2,2) + 
            Power(-1 + t2,3) - 
            t1*Power(-1 + t2,2)*(-15 + 2*s1 + 11*t2) + 
            Power(t1,2)*(-1 + t2)*
             (39 - Power(s1,2) - 51*t2 + 10*Power(t2,2) + 
               9*s1*(-2 + 3*t2)) - 
            (-1 + s1)*Power(t1,5)*
             (6*Power(s1,2) + 7*t2 - s1*(4 + 3*t2)) + 
            Power(t1,3)*(37 + 2*Power(s1,3) + Power(s1,2)*(2 - 17*t2) - 
               62*t2 + 17*Power(t2,2) + 3*Power(t2,3) + 
               s1*(-34 + 80*t2 - 28*Power(t2,2))) + 
            Power(t1,4)*(-12 + Power(s1,3) + 3*t2 + 10*Power(t2,2) + 
               Power(s1,2)*(-13 + 24*t2) + 
               s1*(22 - 29*t2 - 6*Power(t2,2))) - 
            Power(s2,3)*(1 + 
               Power(s1,3)*(2 + 19*t1 - 72*Power(t1,2) + 
                  21*Power(t1,3)) + Power(t1,5)*(-1 + t2) + 11*t2 - 
               27*Power(t2,2) + 10*Power(t2,3) + 
               Power(t1,4)*(2 + 4*t2 - Power(t2,2)) - 
               Power(t1,3)*(1 + 40*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(1 + 75*t2 + 2*Power(t2,2) - 
                  8*Power(t2,3)) + 
               t1*(-2 - 51*t2 + 32*Power(t2,2) + 28*Power(t2,3)) + 
               Power(s1,2)*(-2 - 13*t2 + Power(t1,4)*(-15 + 2*t2) - 
                  Power(t1,3)*(61 + 28*t2) + t1*(-101 + 46*t2) + 
                  Power(t1,2)*(179 + 83*t2)) + 
               s1*(-(Power(t1,5)*(-1 + t2)) + 2*Power(t1,4)*(3 + t2) + 
                  t1*(86 + 29*t2 - 86*Power(t2,2)) - 
                  2*(14 - 24*t2 + Power(t2,2)) - 
                  2*Power(t1,2)*(40 + 87*t2 + 3*Power(t2,2)) + 
                  Power(t1,3)*(15 + 96*t2 + 4*Power(t2,2)))) - 
            Power(s2,5)*(3*Power(s1,3)*(-2 + t1) + 
               Power(s1,2)*(-4 + Power(t1,2) + t1*(3 - 16*t2) + 
                  25*t2) - t2*
                (-(Power(t1,3)*(-1 + t2)) - Power(t1,2)*(2 + 5*t2) - 
                  t2*(2 + 13*t2) + t1*(1 + 8*t2 + 10*Power(t2,2))) + 
               s1*(-(Power(t1,3)*(-1 + t2)) + 2*(1 - 16*t2)*t2 - 
                  2*Power(t1,2)*(1 + 3*t2) + 
                  t1*(1 + 5*t2 + 23*Power(t2,2)))) + 
            Power(s2,2)*(1 + 
               3*Power(s1,3)*t1*
                (2 + 7*t1 - 24*Power(t1,2) + 5*Power(t1,3)) - 
               Power(t1,5)*(-2 + t2) - 14*t2 + 27*Power(t2,2) - 
               14*Power(t2,3) + 
               Power(t1,4)*(-15 - 9*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(32 + 22*t2 - 6*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,2)*(-26 - 27*t2 + 50*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(6 + 29*t2 - 76*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s1,2)*(1 - 73*Power(t1,4) + 
                  Power(t1,5)*(-6 + t2) - t2 + 
                  11*Power(t1,2)*(-10 + 7*t2) + 
                  2*Power(t1,3)*(95 + 28*t2) - t1*(2 + 43*t2)) + 
               s1*(4*Power(t1,5) + 
                  Power(t1,4)*(54 + 29*t2 - 6*Power(t2,2)) + 
                  3*(1 - 5*t2 + 4*Power(t2,2)) - 
                  4*Power(t1,3)*(41 + 11*t2 + 5*Power(t2,2)) + 
                  t1*(-50 + 96*t2 + 8*Power(t2,2)) - 
                  3*Power(t1,2)*(-51 + 22*t2 + 28*Power(t2,2)))) + 
            Power(s2,4)*(2 + Power(s1,3)*(6 - 34*t1 + 13*Power(t1,2)) - 
               7*t2 - 22*Power(t2,2) + 29*Power(t2,3) + 
               Power(t1,4)*(-1 + Power(t2,2)) + 
               t1*(-5 + 18*t2 + 45*Power(t2,2) - 7*Power(t2,3)) + 
               Power(t1,3)*(1 + 4*t2 + 5*Power(t2,2) - Power(t2,3)) - 
               Power(t1,2)*(-3 + 15*t2 + 29*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s1,2)*(-35 + Power(t1,3)*(-10 + t2) + 13*t2 - 
                  Power(t1,2)*(9 + 40*t2) + t1*(54 + 71*t2)) + 
               s1*(-3 - 2*Power(t1,4)*(-1 + t2) + 65*t2 - 
                  48*Power(t2,2) - 2*t1*(-5 + 61*t2 + 15*Power(t2,2)) + 
                  Power(t1,2)*(-9 + 59*t2 + 33*Power(t2,2)))) + 
            s2*(-(Power(s1,3)*Power(t1,2)*
                  (6 + 9*t1 - 34*Power(t1,2) + 4*Power(t1,3))) + 
               Power(t1,5)*(7 - 5*t2) + Power(-1 + t2,2)*(-11 + 7*t2) + 
               Power(t1,4)*(-20 + 10*t2 - 17*Power(t2,2)) + 
               Power(t1,2)*(-37 + 29*t2 + 47*Power(t2,2) - 
                  24*Power(t2,3)) + 
               2*t1*(16 - 29*t2 + 8*Power(t2,2) + 5*Power(t2,3)) - 
               Power(t1,3)*(-29 + 5*t2 + 21*Power(t2,2) + 
                  8*Power(t2,3)) - 
               Power(s1,2)*t1*
                (2 + t1*(2 - 47*t2) - 2*t2 + Power(t1,4)*(-26 + 4*t2) + 
                  Power(t1,3)*(79 + 22*t2) + Power(t1,2)*(-57 + 68*t2)) \
+ s1*(2*Power(-1 + t2,2) + Power(t1,5)*(-29 + 9*t2) + 
                  2*Power(t1,4)*(39 + 2*t2 + 6*Power(t2,2)) + 
                  2*Power(t1,2)*(28 - 64*t2 + 9*Power(t2,2)) - 
                  3*t1*(7 - 20*t2 + 13*Power(t2,2)) + 
                  Power(t1,3)*(-86 + 59*t2 + 52*Power(t2,2))))) + 
         Power(s,8)*(-((39 + 5*s1)*Power(t1,6)) + Power(t1,7) + 
            t2*(3 - 18*t2 + 20*Power(t2,2)) + 
            Power(t1,5)*(175 - 4*Power(s1,2) - 9*t2 + Power(t2,2) + 
               s1*(58 + 3*t2)) + 
            Power(s2,4)*(70*Power(t1,3) + 
               s1*(-6 + 42*t1 - 56*Power(t1,2)) + t1*(15 - 112*t2) + 
               21*t2 + 14*Power(t1,2)*(-5 + 9*t2)) - 
            Power(t1,4)*(Power(s1,3) - Power(s1,2)*(15 + 2*t2) + 
               s1*(164 + 6*t2 + Power(t2,2)) + 
               10*(24 - 17*t2 + 2*Power(t2,2))) + 
            Power(t1,2)*(-78 + 201*t2 + Power(t2,2) + 10*Power(t2,3) + 
               2*Power(s1,2)*(9 + 4*t2) - 
               s1*(37 + 44*t2 + 6*Power(t2,2))) - 
            Power(t1,3)*(-219 + 2*Power(s1,3) + 
               Power(s1,2)*(119 - 8*t2) + 244*t2 + 32*Power(t2,2) - 
               8*Power(t2,3) + 2*s1*(58 - 95*t2 + 10*Power(t2,2))) + 
            t1*(3 + t2 - 54*Power(t2,2) - 45*Power(t2,3) + 
               s1*(-2 - 3*t2 + 44*Power(t2,2))) + 
            Power(s2,3)*(-280*Power(t1,4) + 
               Power(s1,2)*(-1 + 9*t1 - 14*Power(t1,2)) + 
               Power(t1,3)*(377 + 56*t2) + 
               Power(t1,2)*(-143 + 294*t2 - 84*Power(t2,2)) + 
               2*(-1 + 22*t2 + Power(t2,2)) + 
               2*t1*(8 - 139*t2 + 6*Power(t2,2)) + 
               2*s1*(7*Power(t1,3) + t1*(25 - 12*t2) + 2*(-1 + t2) + 
                  4*Power(t1,2)*(-11 + 7*t2))) + 
            Power(s2,2)*(-8 + 165*Power(t1,5) + 
               Power(s1,3)*t1*(1 + 2*t1) + 30*t2 + 28*Power(t2,2) + 
               62*Power(t2,3) + Power(t1,4)*(-701 + 14*t2) + 
               Power(t1,2)*(-146 + 445*t2 - 37*Power(t2,2)) + 
               Power(t1,3)*(547 - 137*t2 + 8*Power(t2,2)) - 
               t1*(-53 + 251*t2 + 5*Power(t2,2) + 45*Power(t2,3)) - 
               Power(s1,2)*(12*Power(t1,3) - 5*t2 + 
                  2*Power(t1,2)*(2 + 7*t2) + t1*(-6 + 11*t2)) + 
               s1*(-1 - 66*Power(t1,4) + t2 - 39*Power(t2,2) + 
                  Power(t1,2)*(-23 + 28*t2) + 
                  Power(t1,3)*(46 + 41*t2) + 
                  t1*(-2 - 56*t2 + 44*Power(t2,2)))) + 
            s2*((302 + 34*s1)*Power(t1,5) - 29*Power(t1,6) + 
               Power(t1,4)*(-638 + 15*Power(s1,2) + 4*s1*(-57 + t2) + 
                  166*t2 - 22*Power(t2,2)) + 
               t2*(-3 + s1*(5 - 24*t2) - 4*t2 + 62*Power(t2,2)) + 
               Power(t1,3)*(464 + 3*Power(s1,3) - 355*t2 - 
                  36*Power(t2,2) + 8*Power(t2,3) + 
                  Power(s1,2)*(-79 + 10*t2) + 
                  s1*(-7 + 163*t2 - 14*Power(t2,2))) + 
               Power(t1,2)*(-226 - 3*Power(s1,3) + 
                  Power(s1,2)*(67 - 7*t2) + 375*t2 + 10*Power(t2,2) + 
                  6*Power(t2,3) + 2*s1*(72 - 44*t2 + 3*Power(t2,2))) + 
               t1*(61 - 193*t2 - 78*Power(t2,2) - 86*Power(t2,3) - 
                  Power(s1,2)*(5 + 21*t2) + 
                  s1*(17 + 22*t2 + 82*Power(t2,2))))) + 
         Power(s,2)*Power(-1 + s2,2)*
          (4 - 70*t1 + 16*s1*t1 + 167*Power(t1,2) - 48*s1*Power(t1,2) - 
            6*Power(s1,2)*Power(t1,2) - 151*Power(t1,3) + 
            77*s1*Power(t1,3) + 59*Power(s1,2)*Power(t1,3) - 
            16*Power(s1,3)*Power(t1,3) + 37*Power(t1,4) - 
            29*s1*Power(t1,4) - 15*Power(s1,2)*Power(t1,4) + 
            9*Power(s1,3)*Power(t1,4) + 30*Power(t1,5) - 
            31*s1*Power(t1,5) - 6*Power(s1,2)*Power(t1,5) + 
            15*Power(s1,3)*Power(t1,5) - 10*Power(t1,6) + 
            14*s1*Power(t1,6) + Power(s1,2)*Power(t1,6) - 
            7*Power(t1,7) + s1*Power(t1,7) - 15*t2 + 190*t1*t2 - 
            33*s1*t1*t2 - 357*Power(t1,2)*t2 + 134*s1*Power(t1,2)*t2 + 
            8*Power(s1,2)*Power(t1,2)*t2 + 131*Power(t1,3)*t2 - 
            217*s1*Power(t1,3)*t2 + 47*Power(s1,2)*Power(t1,3)*t2 + 
            109*Power(t1,4)*t2 - 5*s1*Power(t1,4)*t2 - 
            69*Power(s1,2)*Power(t1,4)*t2 - 72*Power(t1,5)*t2 + 
            60*s1*Power(t1,5)*t2 - 15*Power(s1,2)*Power(t1,5)*t2 + 
            14*Power(t1,6)*t2 - 5*s1*Power(t1,6)*t2 + 18*Power(t2,2) - 
            165*t1*Power(t2,2) + 17*s1*t1*Power(t2,2) + 
            246*Power(t1,2)*Power(t2,2) - 
            105*s1*Power(t1,2)*Power(t2,2) - 
            21*Power(t1,3)*Power(t2,2) + 91*s1*Power(t1,3)*Power(t2,2) - 
            46*Power(t1,4)*Power(t2,2) + 31*s1*Power(t1,4)*Power(t2,2) + 
            Power(t1,5)*Power(t2,2) - 7*Power(t2,3) + 
            45*t1*Power(t2,3) - 35*Power(t1,2)*Power(t2,3) - 
            16*Power(t1,3)*Power(t2,3) + 
            Power(s2,7)*(Power(s1,2)*(12 - 15*t1 + Power(t1,2)) + 
               2*s1*(-1 + Power(t1,2) - 13*t2 + 15*t1*t2) + 
               t2*(6 - Power(t1,2)*(-2 + t2) + 14*t2 - t1*(8 + 15*t2))) \
+ Power(s2,6)*(-4 + Power(s1,3)*(-17 - 3*t1 + 2*Power(t1,2)) - 
               Power(t1,4)*(-1 + t2) - 11*t2 + 28*Power(t2,2) + 
               68*Power(t2,3) + 
               Power(t1,3)*(-4 - 23*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(1 + 57*t2 + 48*Power(t2,2)) - 
               t1*(-6 + 22*t2 + 65*Power(t2,2) + 45*Power(t2,3)) + 
               Power(s1,2)*(-14 - 3*Power(t1,3) - 
                  2*Power(t1,2)*(-12 + t2) + 93*t2 - 4*t1*(-3 + 8*t2)) \
+ s1*(13 - 10*Power(t1,3)*(-1 + t2) + 10*t2 - 144*Power(t2,2) - 
                  2*Power(t1,2)*(13 + 19*t2) + t1*(3 + 80*Power(t2,2)))) \
+ Power(s2,2)*(10 - 5*Power(t1,7) - 
               Power(s1,3)*t1*
                (32 + 44*t1 - 108*Power(t1,2) + 33*Power(t1,3) + 
                  9*Power(t1,4)) + Power(t1,6)*(-55 + t2) + 29*t2 - 
               48*Power(t2,2) + 30*Power(t2,3) + 
               Power(t1,5)*(29 + 30*t2 + 6*Power(t2,2)) + 
               5*Power(t1,4)*(35 - 11*t2 + 16*Power(t2,2)) + 
               2*Power(t1,2)*
                (126 - 32*t2 - 4*Power(t2,2) + 9*Power(t2,3)) + 
               Power(t1,3)*(-289 + 179*t2 + 130*Power(t2,2) + 
                  16*Power(t2,3)) - 
               t1*(117 + 120*t2 - 217*Power(t2,2) + 79*Power(t2,3)) + 
               Power(s1,2)*(-5 - 5*Power(t1,6) + 
                  Power(t1,2)*(315 - 174*t2) + 7*t2 + 
                  11*Power(t1,4)*(31 + 4*t2) - 
                  Power(t1,3)*(365 + 4*t2) + Power(t1,5)*(3 + 11*t2) + 
                  t1*(93 + 121*t2)) + 
               s1*(23 + 3*Power(t1,7) + Power(t1,5)*(87 - 63*t2) - 
                  3*Power(t1,6)*(-12 + t2) - 26*t2 - 16*Power(t2,2) + 
                  Power(t1,3)*(497 - 282*t2 - 54*Power(t2,2)) + 
                  t1*(288 - 405*t2 - 2*Power(t2,2)) - 
                  2*Power(t1,4)*(202 + 96*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*(-530 + 217*t2 + 114*Power(t2,2)))) - 
            Power(s2,3)*(-19 + Power(t1,7) + 
               Power(s1,3)*(-8 - 22*t1 + 28*Power(t1,2) - 
                  82*Power(t1,3) - 20*Power(t1,4) + 4*Power(t1,5)) - 
               72*t2 + 108*Power(t2,2) - 38*Power(t2,3) - 
               Power(t1,6)*(35 + t2) + 
               Power(t1,5)*(-2 - 38*t2 + 4*Power(t2,2)) + 
               Power(t1,4)*(43 + 56*t2 + 40*Power(t2,2)) + 
               Power(t1,3)*(117 + 58*t2 + 96*Power(t2,2) + 
                  32*Power(t2,3)) + 
               t1*(100 - 8*t2 - 139*Power(t2,2) + 48*Power(t2,3)) + 
               Power(t1,2)*(-205 + 5*t2 + 321*Power(t2,2) + 
                  58*Power(t2,3)) + 
               Power(s1,2)*(14 + 3*Power(t1,6) + t1*(175 - 79*t2) + 
                  Power(t1,5)*(-16 + t2) + 32*Power(t1,4)*(-2 + t2) + 
                  40*t2 + 52*Power(t1,2)*(-5 + 2*t2) + 
                  Power(t1,3)*(578 + 202*t2)) + 
               s1*(135 + Power(t1,7) + Power(t1,6)*(22 - 3*t2) - 
                  186*t2 + 16*Power(t2,2) + 5*Power(t1,5)*(8 + t2) + 
                  Power(t1,2)*(524 - 269*t2 - 216*Power(t2,2)) + 
                  Power(t1,4)*(171 - 64*t2 - 28*Power(t2,2)) + 
                  t1*(-202 + 113*t2 + 48*Power(t2,2)) - 
                  Power(t1,3)*(691 + 456*t2 + 120*Power(t2,2)))) + 
            s2*(48 + 13*Power(t1,7) + 
               2*Power(s1,3)*Power(t1,2)*
                (20 + 5*t1 - 43*Power(t1,2) + 7*Power(t1,3)) + 
               Power(t1,6)*(29 - 17*t2) - 122*t2 + 95*Power(t2,2) - 
               21*Power(t2,3) + 
               Power(t1,5)*(-79 + 18*t2 - 4*Power(t2,2)) + 
               2*Power(t1,4)*(50 - 19*t2 + 9*Power(t2,2)) - 
               t1*(126 - 201*t2 + 97*Power(t2,2) + 20*Power(t2,3)) + 
               Power(t1,3)*(-174 - 81*t2 - 34*Power(t2,2) + 
                  24*Power(t2,3)) + 
               Power(t1,2)*(189 + 39*t2 - 152*Power(t2,2) + 
                  59*Power(t2,3)) + 
               Power(s1,2)*t1*
                (11 + 7*Power(t1,5) + 5*Power(t1,4)*(-26 + t2) - 
                  15*t2 + 2*Power(t1,3)*(99 + 26*t2) - 
                  2*t1*(69 + 64*t2) + 2*Power(t1,2)*(-61 + 86*t2)) - 
               s1*(11 + 3*Power(t1,7) - 5*Power(t1,6)*(-6 + t2) - 
                  23*t2 + 12*Power(t2,2) - 17*Power(t1,5)*(7 + t2) + 
                  t1*(-2 + 62*t2 - 98*Power(t2,2)) + 
                  4*Power(t1,4)*(56 - 25*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*(187 - 350*t2 + 30*Power(t2,2)) + 
                  Power(t1,3)*(-334 + 85*t2 + 126*Power(t2,2)))) + 
            Power(s2,4)*(12 + 
               Power(s1,3)*(3 + t1 - 138*Power(t1,2) - 
                  16*Power(t1,3) + 10*Power(t1,4)) - 11*t2 - 
               54*Power(t2,2) + 21*Power(t2,3) + Power(t1,6)*(1 + t2) + 
               Power(t1,4)*(97 + 3*t2 - 2*Power(t2,2)) + 
               Power(t1,5)*(-61 - 14*t2 + Power(t2,2)) + 
               Power(t1,3)*(-12 - 55*t2 + 51*Power(t2,2)) + 
               Power(t1,2)*(-16 + 252*t2 + 90*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-21 - 176*t2 + 189*Power(t2,2) + 143*Power(t2,3)) + 
               Power(s1,2)*(-3 + 5*Power(t1,5) - 8*t2 + 
                  Power(t1,4)*(-28 + 5*t2) - 
                  Power(t1,3)*(119 + 11*t2) + t1*(-223 + 123*t2) + 
                  Power(t1,2)*(643 + 336*t2)) + 
               s1*(-22 + 2*Power(t1,6) + Power(t1,5)*(42 - 9*t2) + 
                  76*t2 - 16*Power(t2,2) + 
                  t1*(359 + 26*t2 - 295*Power(t2,2)) + 
                  Power(t1,3)*(104 + 133*t2 - 29*Power(t2,2)) - 
                  Power(t1,4)*(17 - 17*t2 + Power(t2,2)) - 
                  Power(t1,2)*(468 + 793*t2 + 129*Power(t2,2)))) + 
            Power(s2,5)*(-9 - Power(t1,5) + 
               Power(s1,3)*(-10 + 92*t1 + 8*Power(t1,2) - 
                  8*Power(t1,3)) + 52*t2 + 55*Power(t2,2) - 
               129*Power(t2,3) + 
               Power(t1,4)*(33 + 38*t2 - 10*Power(t2,2)) + 
               t1*(28 - 73*t2 - 203*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(-53 - 93*t2 - 38*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(2 + 76*t2 + 98*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(s1,2)*(136 - 2*Power(t1,3)*(-4 + t2) - 52*t2 + 
                  Power(t1,2)*(21 + 64*t2) - t1*(263 + 276*t2)) + 
               s1*(-Power(t1,5) + Power(t1,4)*(-35 + 16*t2) + 
                  Power(t1,2)*(21 - 139*t2 - 66*Power(t2,2)) + 
                  Power(t1,3)*(47 + 5*t2 - 2*Power(t2,2)) + 
                  3*(-14 - 81*t2 + 68*Power(t2,2)) + 
                  t1*(10 + 557*t2 + 150*Power(t2,2))))) - 
         Power(s,7)*(-1 + 5*t1 + 7*Power(t1,2) + 481*Power(t1,3) - 
            111*Power(t1,4) + 122*Power(t1,5) - 83*Power(t1,6) + 
            8*Power(t1,7) + Power(s1,3)*
             (-2*Power(t1,3)*(8 + 3*t1) + 
               s2*Power(t1,2)*(-17 + 28*t1 - 7*Power(t1,2)) + 
               2*Power(s2,3)*(-1 + t1 + 6*Power(t1,2)) + 
               Power(s2,2)*t1*(15 - 16*t1 + 7*Power(t1,2))) + 15*t2 + 
            80*t1*t2 + 184*Power(t1,2)*t2 - 799*Power(t1,3)*t2 + 
            538*Power(t1,4)*t2 - 74*Power(t1,5)*t2 - 42*Power(t2,2) - 
            175*t1*Power(t2,2) + 146*Power(t1,2)*Power(t2,2) - 
            63*Power(t1,3)*Power(t2,2) - 59*Power(t1,4)*Power(t2,2) + 
            6*Power(t1,5)*Power(t2,2) + 28*Power(t2,3) - 
            90*t1*Power(t2,3) + 35*Power(t1,2)*Power(t2,3) + 
            24*Power(t1,3)*Power(t2,3) + 
            Power(s2,5)*(56*Power(t1,3) + 35*t2 - 20*t1*(-1 + 7*t2) + 
               14*Power(t1,2)*(-5 + 9*t2)) + 
            Power(s2,4)*(5 - 280*Power(t1,4) + 91*t2 + 14*Power(t2,2) + 
               10*Power(t1,3)*(40 + 7*t2) - t1*(13 + 373*t2) - 
               14*Power(t1,2)*(10 - 18*t2 + 9*Power(t2,2))) + 
            Power(s2,3)*(-14 + 220*Power(t1,5) + 67*t2 + 
               125*Power(t2,2) + 183*Power(t2,3) + 
               Power(t1,4)*(-739 + 14*t2) + 
               Power(t1,3)*(567 - 438*t2 + 28*Power(t2,2)) + 
               3*Power(t1,2)*(-24 + 206*t2 + 35*Power(t2,2)) - 
               t1*(-22 + 265*t2 + 135*Power(t2,2) + 120*Power(t2,3))) + 
            s2*(3*Power(t1,7) + Power(t1,6)*(-117 + t2) + 
               Power(t1,4)*(-319 + 766*t2 - 97*Power(t2,2)) + 
               Power(t1,5)*(317 - 48*t2 + 6*Power(t2,2)) + 
               t2*(-7 - 26*t2 + 123*Power(t2,2)) + 
               2*Power(t1,3)*
                (411 - 706*t2 - 53*Power(t2,2) + 22*Power(t2,3)) + 
               Power(t1,2)*(-571 + 682*t2 + 423*Power(t2,2) + 
                  46*Power(t2,3)) - 
               t1*(-99 + 239*t2 + 407*Power(t2,2) + 240*Power(t2,3))) + 
            Power(s2,2)*(-22 + 436*Power(t1,5) - 55*Power(t1,6) + 
               43*t2 + 55*Power(t2,2) + 210*Power(t2,3) + 
               Power(t1,4)*(-614 + 480*t2 - 70*Power(t2,2)) + 
               Power(t1,2)*(-560 + 655*t2 + 336*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(t1,3)*(656 - 1229*t2 - 99*Power(t2,2) + 
                  28*Power(t2,3)) - 
               t1*(-175 + 365*t2 + 511*Power(t2,2) + 286*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,4)*(-3 + 10*t1 - 14*Power(t1,2)) + 
               Power(s2,3)*(-1 + 12*Power(t1,3) + t1*(35 - 22*t2) + 
                  38*t2 - 6*Power(t1,2)*(3 + 7*t2)) + 
               Power(t1,2)*(59 - 17*Power(t1,3) + Power(t1,4) + 
                  28*t2 + 4*t1*(-73 + 8*t2) + 2*Power(t1,2)*(17 + 8*t2)\
) + s2*t1*(-23*Power(t1,4) + Power(t1,3)*(43 + 5*t2) + 
                  t1*(410 + 7*t2) - 8*(3 + 10*t2) + 
                  Power(t1,2)*(-258 + 46*t2)) + 
               Power(s2,2)*(30*Power(t1,4) + Power(t1,2)*(95 - 47*t2) + 
                  27*t2 + Power(t1,3)*(-47 + 38*t2) - 2*t1*(48 + 55*t2))\
) - s1*(5*Power(s2,5)*(3 - 14*t1 + 14*Power(t1,2)) + 
               Power(s2,4)*(20 - 130*t1 - 70*Power(t1,3) + 
                  Power(t1,2)*(178 - 70*t2) + t2) + 
               Power(s2,3)*(1 + 95*Power(t1,4) + 25*t2 + 
                  164*Power(t2,2) - 5*Power(t1,3)*(64 + 7*t2) + 
                  Power(t1,2)*(336 + 83*t2) + 
                  t1*(-46 + 60*t2 - 115*Power(t2,2))) + 
               Power(s2,2)*(5 - 64*Power(t1,5) + 
                  Power(t1,4)*(509 - 26*t2) + 7*t2 + 152*Power(t2,2) + 
                  t1*(68 - 416*t2 - 290*Power(t2,2)) + 
                  Power(t1,2)*(-194 + 506*t2 - 39*Power(t2,2)) + 
                  Power(t1,3)*(-642 - 280*t2 + 42*Power(t2,2))) + 
               t1*(4 - Power(t1,6) + 43*t2 - 112*Power(t2,2) + 
                  Power(t1,5)*(33 + t2) - Power(t1,4)*(181 + 23*t2) + 
                  Power(t1,3)*(294 + 68*t2 + 4*Power(t2,2)) + 
                  2*Power(t1,2)*(-73 - 277*t2 + 37*Power(t2,2)) + 
                  t1*(133 + 251*t2 + 45*Power(t2,2))) + 
               s2*(1 + 21*Power(t1,6) - 28*t2 + 62*Power(t2,2) - 
                  Power(t1,5)*(212 + 11*t2) + 
                  Power(t1,4)*(741 + t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*(-102 + 893*t2 + 6*Power(t2,2)) + 
                  Power(t1,3)*(-370 - 599*t2 + 100*Power(t2,2)) - 
                  t1*(40 + 249*t2 + 259*Power(t2,2))))) + 
         Power(s,4)*(-190*t1 + 70*s1*t1 + 632*Power(t1,2) - 
            186*s1*Power(t1,2) + 22*Power(s1,2)*Power(t1,2) - 
            752*Power(t1,3) + 1189*s1*Power(t1,3) + 
            78*Power(s1,2)*Power(t1,3) - 90*Power(s1,3)*Power(t1,3) + 
            232*Power(t1,4) - 501*s1*Power(t1,4) + 
            47*Power(s1,2)*Power(t1,4) + 20*Power(s1,3)*Power(t1,4) + 
            78*Power(t1,5) - 219*s1*Power(t1,5) + 
            27*Power(s1,2)*Power(t1,5) + 15*Power(s1,3)*Power(t1,5) + 
            3*Power(t1,6) + 47*s1*Power(t1,6) + 
            2*Power(s1,2)*Power(t1,6) + 3*Power(t1,7) + 
            16*s1*Power(t1,7) - 15*t2 + 652*t1*t2 - 209*s1*t1*t2 - 
            857*Power(t1,2)*t2 - 34*s1*Power(t1,2)*t2 + 
            56*Power(s1,2)*Power(t1,2)*t2 - 937*Power(t1,3)*t2 + 
            210*s1*Power(t1,3)*t2 + 121*Power(s1,2)*Power(t1,3)*t2 + 
            1101*Power(t1,4)*t2 - 558*s1*Power(t1,4)*t2 - 
            35*Power(s1,2)*Power(t1,4)*t2 - 389*Power(t1,5)*t2 + 
            183*s1*Power(t1,5)*t2 - 30*Power(s1,2)*Power(t1,5)*t2 + 
            72*Power(t1,6)*t2 - 34*s1*Power(t1,6)*t2 + 42*Power(t2,2) - 
            563*t1*Power(t2,2) + 140*s1*t1*Power(t2,2) + 
            781*Power(t1,2)*Power(t2,2) - 
            315*s1*Power(t1,2)*Power(t2,2) + 
            91*Power(t1,3)*Power(t2,2) + 53*s1*Power(t1,3)*Power(t2,2) - 
            120*Power(t1,4)*Power(t2,2) + 
            65*s1*Power(t1,4)*Power(t2,2) + 15*Power(t1,5)*Power(t2,2) - 
            28*Power(t2,3) + 84*t1*Power(t2,3) - 
            29*Power(t1,2)*Power(t2,3) - 24*Power(t1,3)*Power(t2,3) + 
            Power(s2,8)*(-1 + t1)*
             (s1*(6 - 8*t1) + Power(t1,2) - 7*t2 + t1*(-1 + 9*t2)) + 
            Power(s2,7)*(14 - 8*Power(t1,4) + 
               Power(s1,2)*(35 - 73*t1 + 14*Power(t1,2)) + 64*t2 + 
               70*Power(t2,2) + Power(t1,3)*(-1 + 8*t2) + 
               Power(t1,2)*(29 + 18*t2 - 36*Power(t2,2)) - 
               2*t1*(18 + 55*t2 + 42*Power(t2,2)) + 
               2*s1*(13*Power(t1,3) + 4*Power(t1,2)*(-4 + t2) - 
                  4*(4 + 13*t2) + t1*(29 + 84*t2))) - 
            Power(s2,5)*(92*Power(t1,5) + Power(t1,6) + 
               Power(s1,3)*(38 - 178*t1 - 85*Power(t1,2) + 
                  65*Power(t1,3)) + 
               Power(t1,4)*(-764 - 606*t2 + 98*Power(t2,2)) + 
               Power(t1,3)*(748 + 1133*t2 + 216*Power(t2,2) - 
                  56*Power(t2,3)) - 
               Power(t1,2)*(-296 + 479*t2 + 471*Power(t2,2) + 
                  21*Power(t2,3)) + 
               3*(22 - 77*t2 - 67*Power(t2,2) + 71*Power(t2,3)) + 
               t1*(-393 + 735*t2 + 1460*Power(t2,2) + 
                  266*Power(t2,3)) + 
               Power(s1,2)*(-155 - 3*Power(t1,4) + 
                  Power(t1,3)*(127 - 46*t2) - 13*t2 + 
                  Power(t1,2)*(266 + 34*t2) + t1*(531 + 717*t2)) + 
               s1*(-24 + 8*Power(t1,5) + Power(t1,4)*(298 - 100*t2) + 
                  575*t2 - 264*Power(t2,2) + 
                  t1*(201 - 2336*t2 - 660*Power(t2,2)) - 
                  4*Power(t1,2)*(88 - 19*t2 + 15*Power(t2,2)) + 
                  Power(t1,3)*(-671 - 41*t2 + 42*Power(t2,2)))) + 
            Power(s2,6)*(-20 + 11*Power(t1,5) + 
               5*Power(s1,3)*(-7 - 8*t1 + 6*Power(t1,2)) + 
               Power(t1,4)*(61 - 14*t2) - 98*t2 + 308*Power(t2,2) + 
               357*Power(t2,3) + 
               Power(t1,3)*(-267 - 423*t2 + 56*Power(t2,2)) + 
               Power(t1,2)*(292 + 749*t2 + 441*Power(t2,2)) - 
               t1*(65 + 83*t2 + 495*Power(t2,2) + 210*Power(t2,3)) - 
               2*Power(s1,2)*
                (5 + 6*Power(t1,3) + t1*(-41 + t2) - 135*t2 + 
                  Power(t1,2)*(-68 + 21*t2)) - 
               s1*(-29 + 10*Power(t1,4) + 103*t2 + 574*Power(t2,2) + 
                  Power(t1,3)*(-160 + 77*t2) + 
                  Power(t1,2)*(421 + 232*t2) - 
                  5*t1*(22 - 16*t2 + 49*Power(t2,2)))) - 
            Power(s2,3)*(-1 - 8*Power(t1,7) + 
               2*Power(s1,3)*
                (10 - 128*t1 + 261*Power(t1,2) - 195*Power(t1,3) - 
                  4*Power(t1,4) + 6*Power(t1,5)) - 51*t2 - 
               340*Power(t2,2) + 26*Power(t2,3) - 
               2*Power(t1,6)*(116 + 3*t2) + 
               4*Power(t1,5)*(-10 + 4*t2 + 3*Power(t2,2)) + 
               Power(t1,4)*(-411 + 55*t2 + 180*Power(t2,2)) + 
               t1*(242 + 996*t2 + 636*Power(t2,2) - 76*Power(t2,3)) + 
               Power(t1,3)*(913 + 1418*t2 + 172*Power(t2,2) + 
                  48*Power(t2,3)) + 
               Power(t1,2)*(-229 - 1440*t2 - 182*Power(t2,2) + 
                  62*Power(t2,3)) + 
               Power(s1,2)*(-244 + 2*Power(t1,6) + 
                  Power(t1,5)*(72 - 14*t2) - 3*t2 - 
                  10*Power(t1,2)*(221 + 19*t2) + 
                  Power(t1,4)*(-17 + 67*t2) + t1*(1139 + 108*t2) + 
                  2*Power(t1,3)*(809 + 116*t2)) + 
               2*s1*(158 + Power(t1,7) + Power(t1,6)*(33 - 7*t2) + 
                  3*Power(t1,5)*(-25 + t2) + 180*t2 - 4*Power(t2,2) + 
                  Power(t1,4)*(152 - 145*t2 - 22*Power(t2,2)) + 
                  t1*(-815 - 377*t2 + 2*Power(t2,2)) + 
                  3*Power(t1,2)*(363 + 145*t2 + 2*Power(t2,2)) - 
                  Power(t1,3)*(983 + 487*t2 + 64*Power(t2,2)))) + 
            Power(s2,2)*(104 - 28*Power(t1,7) + 
               Power(s1,3)*t1*
                (9 - 412*t1 + 490*Power(t1,2) - 209*Power(t1,3) + 
                  7*Power(t1,4)) - 101*t2 + 300*Power(t2,2) - 
               15*Power(t2,3) + Power(t1,6)*(-54 + 38*t2) + 
               Power(t1,4)*(1037 + 277*t2 - 80*Power(t2,2)) + 
               Power(t1,5)*(-482 + 70*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*(1600 + 1389*t2 + 411*Power(t2,2) - 
                  18*Power(t2,3)) - 
               Power(t1,3)*(1569 + 1189*t2 - 26*Power(t2,2) + 
                  16*Power(t2,3)) + 
               t1*(-412 - 871*t2 - 785*Power(t2,2) + 46*Power(t2,3)) + 
               Power(s1,2)*(-10 + 22*Power(t1,6) - 
                  9*Power(t1,4)*(-110 + t2) + 55*t2 + 
                  Power(t1,5)*(-71 + 14*t2) - 
                  2*Power(t1,3)*(1225 + 33*t2) + t1*(-735 + 88*t2) + 
                  Power(t1,2)*(2198 + 88*t2)) + 
               s1*(60 + 12*Power(t1,7) + Power(t1,5)*(353 - 36*t2) - 
                  329*t2 - 30*Power(t2,2) - 2*Power(t1,6)*(35 + 3*t2) + 
                  t1*(2017 + 764*t2 - 3*Power(t2,2)) + 
                  Power(t1,4)*(-1129 - 150*t2 + 22*Power(t2,2)) + 
                  Power(t1,3)*(3356 + 359*t2 + 38*Power(t2,2)) - 
                  Power(t1,2)*(4047 + 442*t2 + 66*Power(t2,2)))) + 
            s2*(85 - 12*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (97 + 207*t1 - 202*Power(t1,2) + 38*Power(t1,3)) - 
               330*t2 + 293*Power(t2,2) - 33*Power(t2,3) + 
               Power(t1,6)*(93 + 34*t2) + 
               Power(t1,4)*(1171 + 585*t2 - 122*Power(t2,2)) - 
               2*Power(t1,5)*(235 - 24*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(1006 + 1799*t2 + 447*Power(t2,2) - 
                  7*Power(t2,3)) - 
               Power(t1,3)*(1426 + 1641*t2 + 116*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-517 + 473*t2 - 780*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s1,2)*t1*
                (5 + 10*Power(t1,5) + t1*(414 - 188*t2) + 
                  2*Power(t1,4)*(-109 + t2) - 127*t2 + 
                  17*Power(t1,2)*(-83 + 2*t2) + 
                  Power(t1,3)*(974 + 3*t2)) + 
               s1*(-30 + 6*Power(t1,7) + 95*t2 - 64*Power(t2,2) - 
                  2*Power(t1,6)*(53 + 3*t2) + 
                  2*Power(t1,5)*(78 + 43*t2) + 
                  2*Power(t1,4)*(-736 - 11*t2 + 6*Power(t2,2)) - 
                  2*Power(t1,2)*(1419 + 343*t2 + 12*Power(t2,2)) + 
                  Power(t1,3)*(3193 + 473*t2 + 18*Power(t2,2)) + 
                  t1*(-13 + 630*t2 + 200*Power(t2,2)))) + 
            Power(s2,4)*(-134 - 3*Power(t1,7) + 
               Power(s1,3)*(-51 + 221*t1 - 334*Power(t1,2) - 
                  68*Power(t1,3) + 47*Power(t1,4)) + 223*t2 + 
               366*Power(t2,2) - 42*Power(t2,3) + 
               5*Power(t1,6)*(3 + 2*t2) + 
               Power(t1,4)*(540 + 284*t2 - 40*Power(t2,2)) + 
               Power(t1,5)*(-701 - 161*t2 + 15*Power(t2,2)) + 
               Power(t1,3)*(363 - 563*t2 + 331*Power(t2,2) + 
                  40*Power(t2,3)) + 
               Power(t1,2)*(-290 + 2238*t2 + 1143*Power(t2,2) + 
                  95*Power(t2,3)) + 
               t1*(348 - 810*t2 - 317*Power(t2,2) + 240*Power(t2,3)) + 
               Power(s1,2)*(178 - 2*Power(t1,5) + 
                  Power(t1,4)*(97 - 20*t2) + 43*t2 + 
                  Power(t1,3)*(260 + 97*t2) - t1*(697 + 158*t2) + 
                  2*Power(t1,2)*(612 + 349*t2)) + 
               s1*(-33 + 3*Power(t1,6) + Power(t1,5)*(192 - 35*t2) - 
                  448*t2 + 12*Power(t2,2) + 
                  t1*(267 + 949*t2 - 214*Power(t2,2)) + 
                  Power(t1,4)*(-366 + 116*t2 - 15*Power(t2,2)) - 
                  Power(t1,3)*(321 + 316*t2 + 195*Power(t2,2)) - 
                  Power(t1,2)*(914 + 2756*t2 + 411*Power(t2,2))))) + 
         Power(s,6)*(-4 + (19 + 7*s1)*Power(t1,7) + 27*t2 - 
            42*Power(t2,2) + 14*Power(t2,3) + 
            Power(s2,6)*(28*Power(t1,3) + 
               s1*(-20 + 70*t1 - 56*Power(t1,2)) + t1*(15 - 112*t2) + 
               35*t2 + 42*Power(t1,2)*(-1 + 2*t2)) + 
            Power(t1,6)*(-62 + 5*Power(s1,2) + 10*t2 - s1*(80 + 9*t2)) + 
            Power(t1,5)*(-137 + Power(s1,3) - 234*t2 + 15*Power(t2,2) - 
               Power(s1,2)*(14 + 3*t2) + s1*(205 + 74*t2)) + 
            Power(t1,3)*(380 - 52*Power(s1,3) - 1521*t2 - 
               35*Power(t2,2) + 32*Power(t2,3) + 
               11*Power(s1,2)*(-36 + 7*t2) + 
               s1*(940 + 914*t2 - 127*Power(t2,2))) + 
            Power(t1,4)*(-11*Power(s1,3) + Power(s1,2)*(75 + 41*t2) + 
               3*(7 + 346*t2 - 34*Power(t2,2)) + 
               s1*(-341 - 285*t2 + Power(t2,2))) + 
            Power(t1,2)*(293 - 54*t2 + 445*Power(t2,2) + 
               55*Power(t2,3) + Power(s1,2)*(94 + 56*t2) - 
               s1*(253 + 454*t2 + 147*Power(t2,2))) + 
            t1*(-30 + 306*t2 - 367*Power(t2,2) - 84*Power(t2,3) + 
               s1*(12 - 139*t2 + 182*Power(t2,2))) + 
            Power(s2,5)*(20 + Power(s1,2)*(2 - 17*t1) - 
               168*Power(t1,4) + 110*t2 + 42*Power(t2,2) + 
               Power(t1,3)*(237 + 56*t2) - 
               2*t1*(31 + 160*t2 + 21*Power(t2,2)) - 
               2*Power(t1,2)*(23 - 63*t2 + 63*Power(t2,2)) + 
               2*s1*(-21 + 49*Power(t1,3) - 17*t2 + 
                  4*Power(t1,2)*(-25 + 7*t2) + 6*t1*(15 + 7*t2))) + 
            Power(s2,4)*(-355*Power(t1,4) + 165*Power(t1,5) + 
               Power(s1,3)*(-11 - 7*t1 + 30*Power(t1,2)) + 
               Power(s1,2)*(30*Power(t1,3) + Power(t1,2)*(6 - 70*t2) + 
                  t1*(78 - 16*t2) + 125*t2) + 
               Power(t1,3)*(39 - 731*t2 + 56*Power(t2,2)) + 
               t2*(19 + 304*t2 + 343*Power(t2,2)) + 
               Power(t1,2)*(271 + 862*t2 + 385*Power(t2,2)) - 
               t1*(94 + 92*t2 + 425*Power(t2,2) + 210*Power(t2,3)) - 
               s1*(9 + 80*Power(t1,4) + 120*t2 + 392*Power(t2,2) + 
                  Power(t1,3)*(-491 + 21*t2) + 
                  3*Power(t1,2)*(247 + 82*t2) - 
                  t1*(185 + 4*t2 + 203*Power(t2,2)))) - 
            Power(s2,2)*(24 - 2*Power(t1,7) + 
               2*Power(s1,3)*t1*
                (-27 + 78*t1 - 44*Power(t1,2) + 4*Power(t1,3)) + 
               Power(t1,6)*(113 - 5*t2) - 47*t2 - 156*Power(t2,2) - 
               231*Power(t2,3) + 
               Power(t1,5)*(163 + 120*t2 - 15*Power(t2,2)) + 
               2*Power(t1,4)*(-302 - 686*t2 + 91*Power(t2,2)) + 
               Power(t1,3)*(-404 + 2810*t2 + 79*Power(t2,2) - 
                  96*Power(t2,3)) - 
               Power(t1,2)*(-1093 + 2075*t2 + 1428*Power(t2,2) + 
                  93*Power(t2,3)) + 
               2*t1*(-204 + 193*t2 + 726*Power(t2,2) + 
                  225*Power(t2,3)) + 
               Power(s1,2)*(1 + 47*Power(t1,5) + 
                  Power(t1,3)*(90 - 103*t2) + 
                  3*Power(t1,4)*(-20 + t2) - 61*t2 - 
                  Power(t1,2)*(801 + 70*t2) + t1*(485 + 269*t2)) + 
               s1*(5 + 32*Power(t1,6) + 86*t2 + 224*Power(t2,2) - 
                  Power(t1,5)*(298 + 9*t2) + 
                  t1*(2 - 1630*t2 - 553*Power(t2,2)) + 
                  3*Power(t1,2)*(156 + 787*t2 + 5*Power(t2,2)) + 
                  Power(t1,4)*(1239 - 65*t2 + 15*Power(t2,2)) + 
                  Power(t1,3)*(-1142 - 609*t2 + 211*Power(t2,2)))) + 
            s2*(2 + 17*Power(t1,7) - 
               2*Power(s1,3)*Power(t1,2)*
                (11 - 56*t1 + 24*Power(t1,2)) + 
               3*Power(t1,6)*(-23 + t2) - 52*t2 + 33*Power(t2,2) + 
               107*Power(t2,3) + 
               Power(t1,4)*(580 + 1440*t2 - 190*Power(t2,2)) + 
               Power(t1,5)*(-219 - 218*t2 + 18*Power(t2,2)) + 
               Power(t1,3)*(607 - 2937*t2 - 88*Power(t2,2) + 
                  72*Power(t2,3)) + 
               Power(t1,2)*(-673 + 1491*t2 + 1153*Power(t2,2) + 
                  87*Power(t2,3)) - 
               t1*(42 - 27*t2 + 898*Power(t2,2) + 274*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-43 + 5*Power(t1,5) - 157*t2 + 
                  Power(t1,4)*(-74 + 3*t2) + 
                  Power(t1,3)*(82 + 26*t2) + t1*(1013 + 28*t2) + 
                  Power(t1,2)*(-750 + 66*t2)) + 
               s1*(-7 + 3*Power(t1,7) + 69*t2 - 92*Power(t2,2) - 
                  Power(t1,6)*(92 + t2) + Power(t1,5)*(491 + 57*t2) + 
                  Power(t1,3)*(1568 + 1199*t2 - 200*Power(t2,2)) - 
                  2*Power(t1,4)*(511 + 57*t2 + 5*Power(t2,2)) - 
                  Power(t1,2)*(965 + 2130*t2 + 84*Power(t2,2)) + 
                  t1*(68 + 606*t2 + 416*Power(t2,2)))) + 
            Power(s2,3)*(-37 + 223*Power(t1,5) - 50*Power(t1,6) - 
               2*Power(s1,3)*
                (6 - 35*t1 + 9*Power(t1,2) + 5*Power(t1,3)) + 24*t2 + 
               251*Power(t2,2) + 329*Power(t2,3) + 
               Power(t1,4)*(323 + 820*t2 - 126*Power(t2,2)) + 
               Power(t1,2)*(-562 + 1110*t2 + 859*Power(t2,2) + 
                  21*Power(t2,3)) + 
               Power(t1,3)*(-199 - 2113*t2 - 174*Power(t2,2) + 
                  56*Power(t2,3)) - 
               t1*(-290 + 475*t2 + 1472*Power(t2,2) + 518*Power(t2,3)) + 
               2*Power(s1,2)*
                (19 + 15*Power(t1,4) + 59*t2 - 
                  5*Power(t1,2)*(13 + 10*t2) - 7*t1*(16 + 21*t2) + 
                  Power(t1,3)*(8 + 37*t2)) + 
               s1*(22 + 55*Power(t1,5) - 207*t2 - 340*Power(t2,2) + 
                  Power(t1,4)*(-623 + 72*t2) + 
                  Power(t1,3)*(1349 + 219*t2 - 70*Power(t2,2)) + 
                  Power(t1,2)*(-19 - 807*t2 + 102*Power(t2,2)) + 
                  t1*(-120 + 1475*t2 + 598*Power(t2,2))))) + 
         Power(s,3)*(-5 + 157*t1 - 48*s1*t1 - 412*Power(t1,2) + 
            97*s1*Power(t1,2) + 7*Power(s1,2)*Power(t1,2) + 
            494*Power(t1,3) - 410*s1*Power(t1,3) - 
            160*Power(s1,2)*Power(t1,3) + 52*Power(s1,3)*Power(t1,3) - 
            193*Power(t1,4) + 224*s1*Power(t1,4) + 
            30*Power(s1,2)*Power(t1,4) - 24*Power(s1,3)*Power(t1,4) - 
            114*Power(t1,5) + 133*s1*Power(t1,5) - 
            13*Power(s1,2)*Power(t1,5) - 20*Power(s1,3)*Power(t1,5) + 
            61*Power(t1,6) - 71*s1*Power(t1,6) - 
            Power(s1,2)*Power(t1,6) + 12*Power(t1,7) - 
            7*s1*Power(t1,7) + Power(s2,9)*Power(-1 + t1,2)*(s1 - t2) + 
            27*t2 - 456*t1*t2 + 113*s1*t1*t2 + 739*Power(t1,2)*t2 - 
            166*s1*Power(t1,2)*t2 - 28*Power(s1,2)*Power(t1,2)*t2 + 
            118*Power(t1,3)*t2 + 214*s1*Power(t1,3)*t2 - 
            88*Power(s1,2)*Power(t1,3)*t2 - 526*Power(t1,4)*t2 + 
            246*s1*Power(t1,4)*t2 + 88*Power(s1,2)*Power(t1,4)*t2 + 
            232*Power(t1,5)*t2 - 143*s1*Power(t1,5)*t2 + 
            30*Power(s1,2)*Power(t1,5)*t2 - 52*Power(t1,6)*t2 + 
            21*s1*Power(t1,6)*t2 - 42*Power(t2,2) + 389*t1*Power(t2,2) - 
            64*s1*t1*Power(t2,2) - 551*Power(t1,2)*Power(t2,2) + 
            231*s1*Power(t1,2)*Power(t2,2) - 
            31*Power(t1,3)*Power(t2,2) - 
            130*s1*Power(t1,3)*Power(t2,2) + 
            93*Power(t1,4)*Power(t2,2) - 64*s1*Power(t1,4)*Power(t2,2) - 
            6*Power(t1,5)*Power(t2,2) + 20*Power(t2,3) - 
            90*t1*Power(t2,3) + 55*Power(t1,2)*Power(t2,3) + 
            32*Power(t1,3)*Power(t2,3) - 
            Power(s2,8)*(3 + Power(s1,2)*(29 - 46*t1 + 6*Power(t1,2)) + 
               Power(t1,3)*(-3 + t2) + 29*t2 + 42*Power(t2,2) + 
               Power(t1,2)*(7 + 9*t2 - 9*Power(t2,2)) - 
               t1*(7 + 43*t2 + 48*Power(t2,2)) + 
               s1*(-12 + 4*Power(t1,3) - 71*t2 + Power(t1,2)*(2 + t2) + 
                  2*t1*(5 + 48*t2))) + 
            Power(s2,7)*(18 - 
               6*Power(s1,3)*(-5 - 4*t1 + 2*Power(t1,2)) + 71*t2 - 
               103*Power(t2,2) - 197*Power(t2,3) + 
               Power(t1,4)*(-19 + 6*t2) + 
               Power(t1,3)*(67 + 142*t2 - 28*Power(t2,2)) - 
               Power(t1,2)*(64 + 300*t2 + 203*Power(t2,2)) + 
               t1*(-2 + 45*t2 + 205*Power(t2,2) + 120*Power(t2,3)) + 
               Power(s1,2)*(39 + 12*Power(t1,3) - 200*t2 + 
                  2*Power(t1,2)*(-43 + 7*t2) + t1*(-69 + 26*t2)) + 
               s1*(-45 + Power(t1,4) - 39*t2 + 364*Power(t2,2) + 
                  9*Power(t1,2)*(16 + 13*t2) + 
                  Power(t1,3)*(-46 + 39*t2) + 
                  t1*(-18 + 116*t2 - 169*Power(t2,2)))) + 
            Power(s2,2)*(-22 - 42*Power(t1,7) + 
               Power(s1,3)*t1*
                (45 + 258*t1 - 235*Power(t1,2) + 86*Power(t1,3) - 
                  36*Power(t1,4)) - 155*t2 + 87*Power(t2,2) - 
               42*Power(t2,3) + Power(t1,6)*(-153 + 28*t2) + 
               Power(t1,4)*(-204 + 254*t2 - 52*Power(t2,2)) - 
               4*Power(t1,5)*(5 - 31*t2 + 3*Power(t2,2)) - 
               Power(t1,2)*(1174 - 655*t2 + 120*Power(t2,2) + 
                  3*Power(t2,3)) - 
               Power(t1,3)*(-1190 + 515*t2 + 149*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(425 + 745*t2 - 285*Power(t2,2) + 54*Power(t2,3)) + 
               Power(s1,2)*(10 - 6*Power(t1,6) + t1*(130 - 232*t2) + 
                  2*Power(t1,4)*(-414 + t2) - 25*t2 + 
                  2*Power(t1,5)*(55 + 4*t2) - 
                  Power(t1,2)*(1419 + 19*t2) + 
                  Power(t1,3)*(1447 + 46*t2)) + 
               s1*(-79 + 4*Power(t1,7) + 2*Power(t1,5)*(-117 + t2) - 
                  2*Power(t1,6)*(-18 + t2) + 219*t2 - 16*Power(t2,2) + 
                  Power(t1,2)*(1838 - 206*t2 - 39*Power(t2,2)) + 
                  Power(t1,4)*(885 + 150*t2 + 8*Power(t2,2)) + 
                  2*Power(t1,3)*(-1093 + 218*t2 + 9*Power(t2,2)) + 
                  2*t1*(-700 + 244*t2 + 67*Power(t2,2)))) + 
            Power(s2,3)*(-138 + 28*Power(t1,7) + 
               2*Power(s1,3)*
                (-1 - 77*t1 + 89*Power(t1,2) - 110*Power(t1,3) + 
                  81*Power(t1,4) + 3*Power(t1,5)) - 
               14*Power(t1,6)*(-18 + t2) - 147*t2 + 81*Power(t2,2) - 
               19*Power(t2,3) - 
               2*Power(t1,5)*(-125 + 48*t2 + 6*Power(t2,2)) - 
               Power(t1,4)*(645 + 184*t2 + 146*Power(t2,2)) - 
               Power(t1,3)*(-229 + 610*t2 + 192*Power(t2,2) + 
                  40*Power(t2,3)) - 
               Power(t1,2)*(712 - 180*t2 + 65*Power(t2,2) + 
                  52*Power(t2,3)) + 
               t1*(736 - 633*t2 - 261*Power(t2,2) + 56*Power(t2,3)) + 
               Power(s1,2)*(-119 - 8*Power(t1,6) + 74*t2 - 
                  4*Power(t1,5)*(-4 + 3*t2) - 
                  4*Power(t1,4)*(190 + 17*t2) + 
                  2*Power(t1,2)*(-755 + 28*t2) - 
                  4*Power(t1,3)*(-313 + 35*t2) + t1*(713 + 70*t2)) + 
               s1*(497 - 10*Power(t1,7) + 12*Power(t1,6)*(-4 + t2) - 
                  227*t2 - 8*Power(t2,2) + 
                  6*Power(t1,5)*(-44 + 17*t2) + 
                  t1*(-818 + 374*t2 - 55*Power(t2,2)) + 
                  Power(t1,2)*(2138 + 11*t2 + 12*Power(t2,2)) + 
                  Power(t1,4)*(1237 + 282*t2 + 36*Power(t2,2)) + 
                  Power(t1,3)*(-1228 + 457*t2 + 120*Power(t2,2)))) + 
            Power(s2,6)*(20 + 26*Power(t1,5) - Power(t1,6) + 
               Power(s1,3)*(-2 - 167*t1 - 62*Power(t1,2) + 
                  37*Power(t1,3)) - 205*t2 - 5*Power(t2,2) + 
               398*Power(t2,3) + 
               Power(t1,4)*(-244 - 236*t2 + 42*Power(t2,2)) + 
               Power(t1,3)*(258 + 347*t2 + 141*Power(t2,2) - 
                  28*Power(t2,3)) + 
               Power(t1,2)*(122 + 35*t2 - 80*Power(t2,2) - 
                  21*Power(t2,3)) + 
               t1*(-181 + 239*t2 + 501*Power(t2,2) - 14*Power(t2,3)) + 
               Power(s1,2)*(-212 + Power(t1,2)*(111 - 43*t2) + 
                  Power(t1,3)*(55 - 10*t2) + 209*t2 + t1*(554 + 564*t2)\
) + s1*(77 + 6*Power(t1,5) + Power(t1,4)*(137 - 54*t2) + 453*t2 - 
                  632*Power(t2,2) + 
                  2*Power(t1,3)*(-108 - 24*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*(-230 + 16*t2 + 39*Power(t2,2)) - 
                  2*t1*(-23 + 737*t2 + 141*Power(t2,2)))) + 
            s2*(-90 + 3*Power(t1,7) - 
               Power(s1,3)*Power(t1,2)*
                (95 + 110*t1 - 175*Power(t1,2) + 26*Power(t1,3)) + 
               253*t2 - 186*Power(t2,2) + 23*Power(t2,3) + 
               Power(t1,6)*(-13 + 43*t2) + 
               Power(t1,4)*(-539 + 290*t2 - 21*Power(t2,2)) + 
               Power(t1,5)*(253 - 206*t2 + 18*Power(t2,2)) + 
               Power(t1,3)*(714 + 408*t2 + 170*Power(t2,2) - 
                  36*Power(t2,3)) + 
               t1*(229 - 77*t2 + 93*Power(t2,2) + 56*Power(t2,3)) - 
               Power(t1,2)*(557 + 1181*t2 - 487*Power(t2,2) + 
                  78*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-20 + 8*Power(t1,5) + Power(t1,2)*(756 - 102*t2) + 
                  Power(t1,4)*(329 - 20*t2) + 56*t2 - 
                  5*Power(t1,3)*(141 + 19*t2) + t1*(164 + 231*t2)) + 
               s1*(25 + 10*Power(t1,7) + Power(t1,6)*(31 - 20*t2) - 
                  60*t2 + 34*Power(t2,2) + 
                  3*Power(t1,5)*(-116 + 5*t2) + 
                  Power(t1,2)*(1181 - 189*t2 - 150*Power(t2,2)) + 
                  t1*(78 - 267*t2 - 97*Power(t2,2)) + 
                  Power(t1,4)*(927 - 363*t2 + 70*Power(t2,2)) + 
                  Power(t1,3)*(-1434 - 209*t2 + 164*Power(t2,2)))) + 
            Power(s2,4)*(-122 - 139*Power(t1,6) - 2*Power(t1,7) + 
               2*Power(s1,3)*
                (15 - 53*t1 + 62*Power(t1,2) - 119*Power(t1,3) - 
                  21*Power(t1,4) + 6*Power(t1,5)) + 66*t2 + 
               194*Power(t2,2) - 24*Power(t2,3) + 
               2*Power(t1,5)*(-188 - 66*t2 + 9*Power(t2,2)) + 
               Power(t1,4)*(325 + 244*t2 + 109*Power(t2,2)) + 
               Power(t1,3)*(711 + 403*t2 + 391*Power(t2,2) + 
                  72*Power(t2,3)) + 
               Power(t1,2)*(-489 + 700*t2 + 838*Power(t2,2) + 
                  129*Power(t2,3)) + 
               t1*(92 - 91*t2 + 115*Power(t2,2) + 178*Power(t2,3)) + 
               Power(s1,2)*(-109 + 7*Power(t1,6) + t1*(834 - 12*t2) + 
                  Power(t1,5)*(19 - 6*t2) - 8*t2 + 
                  10*Power(t1,4)*(5 + 7*t2) + 
                  2*Power(t1,3)*(593 + 170*t2) + 
                  Power(t1,2)*(-597 + 426*t2)) + 
               s1*(194 + 3*Power(t1,7) + Power(t1,6)*(59 - 11*t2) + 
                  Power(t1,5)*(139 - 3*t2) - 239*t2 + 24*Power(t2,2) - 
                  2*Power(t1,4)*(-51 + 119*t2 + 28*Power(t2,2)) - 
                  3*Power(t1,2)*(-299 + 481*t2 + 141*Power(t2,2)) - 
                  2*Power(t1,3)*(880 + 509*t2 + 143*Power(t2,2)) - 
                  t1*(824 + 103*t2 + 204*Power(t2,2)))) + 
            Power(s2,5)*(22 + Power(t1,7) + 
               Power(s1,3)*(8 + 38*t1 + 249*Power(t1,2) + 
                  74*Power(t1,3) - 37*Power(t1,4)) + 120*t2 + 
               16*Power(t2,2) - 159*Power(t2,3) - 
               Power(t1,6)*(7 + 5*t2) + 
               Power(t1,4)*(-81 + 152*t2 - 25*Power(t2,2)) + 
               Power(t1,5)*(301 + 78*t2 - 6*Power(t2,2)) + 
               t1*(137 + 183*t2 - 805*Power(t2,2) - 360*Power(t2,3)) + 
               2*Power(t1,3)*
                (-233 - 146*t2 - 151*Power(t2,2) + 6*Power(t2,3)) - 
               Power(t1,2)*(-93 + 818*t2 + 315*Power(t2,2) + 
                  30*Power(t2,3)) - 
               Power(s1,2)*(28 + 13*Power(t1,5) - 
                  3*Power(t1,4)*(-9 + t2) + 50*t2 + 
                  Power(t1,3)*(68 + 46*t2) + 
                  13*Power(t1,2)*(88 + 49*t2) + t1*(-52 + 472*t2)) + 
               s1*(22 - 7*Power(t1,6) - 178*t2 + 234*Power(t2,2) + 
                  Power(t1,5)*(-136 + 27*t2) + 
                  Power(t1,4)*(7 - 23*t2 + 6*Power(t2,2)) + 
                  Power(t1,3)*(244 + 129*t2 + 100*Power(t2,2)) + 
                  Power(t1,2)*(976 + 1861*t2 + 330*Power(t2,2)) + 
                  t1*(-524 + 849*t2 + 737*Power(t2,2))))) + 
         Power(s,5)*(5 + 121*t1 - 50*s1*t1 - 594*Power(t1,2) + 
            274*s1*Power(t1,2) - 75*Power(s1,2)*Power(t1,2) + 
            306*Power(t1,3) - 1565*s1*Power(t1,3) + 
            222*Power(s1,2)*Power(t1,3) + 90*Power(s1,3)*Power(t1,3) - 
            88*Power(t1,4) + 466*s1*Power(t1,4) - 
            108*Power(s1,2)*Power(t1,4) + Power(s1,3)*Power(t1,4) + 
            179*Power(t1,5) + 43*s1*Power(t1,5) - 
            14*Power(s1,2)*Power(t1,5) - 6*Power(s1,3)*Power(t1,5) - 
            18*Power(t1,6) + 62*s1*Power(t1,6) - 
            6*Power(s1,2)*Power(t1,6) - 20*Power(t1,7) - 
            16*s1*Power(t1,7) - 15*t2 - 578*t1*t2 + 225*s1*t1*t2 + 
            518*Power(t1,2)*t2 + 361*s1*Power(t1,2)*t2 - 
            70*Power(s1,2)*Power(t1,2)*t2 + 1671*Power(t1,3)*t2 - 
            792*s1*Power(t1,3)*t2 - 118*Power(s1,2)*Power(t1,3)*t2 - 
            1329*Power(t1,4)*t2 + 563*s1*Power(t1,4)*t2 - 
            32*Power(s1,2)*Power(t1,4)*t2 + 391*Power(t1,5)*t2 - 
            144*s1*Power(t1,5)*t2 + 15*Power(s1,2)*Power(t1,5)*t2 - 
            44*Power(t1,6)*t2 + 26*s1*Power(t1,6)*t2 + 
            543*t1*Power(t2,2) - 196*s1*t1*Power(t2,2) - 
            734*Power(t1,2)*Power(t2,2) + 
            273*s1*Power(t1,2)*Power(t2,2) - 55*Power(t1,3)*Power(t2,2) + 
            82*s1*Power(t1,3)*Power(t2,2) + 123*Power(t1,4)*Power(t2,2) - 
            30*s1*Power(t1,4)*Power(t2,2) - 20*Power(t1,5)*Power(t2,2) + 
            14*Power(t2,3) - 29*Power(t1,2)*Power(t2,3) - 
            10*Power(t1,3)*Power(t2,3) + 
            Power(s2,7)*(-8*Power(t1,3) + 
               s1*(15 - 42*t1 + 28*Power(t1,2)) + 
               Power(t1,2)*(14 - 36*t2) - 21*t2 + t1*(-6 + 56*t2)) - 
            Power(s2,6)*(25 - 56*Power(t1,4) + 
               2*Power(s1,2)*(10 - 30*t1 + 7*Power(t1,2)) + 95*t2 + 
               70*Power(t2,2) + 4*Power(t1,3)*(16 + 7*t2) + 
               Power(t1,2)*(29 + 42*t2 - 84*Power(t2,2)) - 
               t1*(71 + 205*t2 + 84*Power(t2,2)) + 
               s1*(-48 + 70*Power(t1,3) - 85*t2 + 28*t1*(5 + 6*t2) + 
                  2*Power(t1,2)*(-61 + 14*t2))) + 
            Power(s2,5)*(-66*Power(t1,5) + 
               Power(s1,3)*(26 + 29*t1 - 40*Power(t1,2)) + 
               7*Power(t1,4)*(1 + 2*t2) + 
               t2*(68 - 415*t2 - 427*Power(t2,2)) + 
               Power(t1,3)*(363 + 718*t2 - 70*Power(t2,2)) - 
               Power(t1,2)*(476 + 1011*t2 + 553*Power(t2,2)) + 
               t1*(142 + 44*t2 + 625*Power(t2,2) + 252*Power(t2,3)) - 
               Power(s1,2)*(4 + 12*Power(t1,3) + 
                  Power(t1,2)*(90 - 70*t2) + 233*t2 + t1*(93 + 2*t2)) + 
               s1*(8 + 39*Power(t1,4) + 192*t2 + 588*Power(t2,2) + 
                  7*Power(t1,3)*(-53 + 11*t2) + 
                  Power(t1,2)*(752 + 307*t2) - 
                  t1*(229 + 10*t2 + 259*Power(t2,2)))) + 
            Power(s2,2)*(-24 - 14*Power(t1,7) + 
               Power(s1,3)*t1*
                (-68 + 377*t1 - 366*Power(t1,2) + 65*Power(t1,3) + 
                  4*Power(t1,4)) + 63*t2 - 381*Power(t2,2) - 
               64*Power(t2,3) - 4*Power(t1,6)*(35 + 2*t2) + 
               Power(t1,5)*(540 + 223*t2 - 12*Power(t2,2)) + 
               Power(t1,4)*(-1363 - 1077*t2 + 241*Power(t2,2)) + 
               Power(t1,3)*(680 + 3364*t2 + 14*Power(t2,2) - 
                  48*Power(t2,3)) - 
               Power(t1,2)*(-291 + 3231*t2 + 1564*Power(t2,2) + 
                  54*Power(t2,3)) + 
               t1*(-211 + 616*t2 + 1730*Power(t2,2) + 156*Power(t2,3)) + 
               Power(s1,2)*(5 - 6*Power(t1,6) - 
                  11*Power(t1,5)*(-10 + t2) - 75*t2 + 
                  6*Power(t1,3)*(206 + 5*t2) + 
                  3*Power(t1,4)*(-37 + 6*t2) - 
                  Power(t1,2)*(1940 + 199*t2) + t1*(922 + 205*t2)) - 
               s1*(17 + 2*Power(t1,7) - 254*t2 - 148*Power(t2,2) + 
                  Power(t1,6)*(-92 + 6*t2) + Power(t1,5)*(482 + 34*t2) + 
                  Power(t1,3)*(2210 + 1060*t2 - 136*Power(t2,2)) + 
                  Power(t1,4)*(-938 + 71*t2 + 6*Power(t2,2)) - 
                  Power(t1,2)*(2849 + 2712*t2 + 120*Power(t2,2)) + 
                  t1*(970 + 2060*t2 + 364*Power(t2,2)))) - 
            s2*(31 + Power(s1,3)*Power(t1,2)*
                (28 + 213*t1 - 140*Power(t1,2) + 10*Power(t1,3)) - 
               203*t2 + 208*Power(t2,2) + 19*Power(t2,3) + 
               Power(t1,6)*(48 + 34*t2) + 
               Power(t1,4)*(1169 + 1424*t2 - 205*Power(t2,2)) + 
               Power(t1,5)*(-703 - 231*t2 + 12*Power(t2,2)) + 
               Power(t1,3)*(-650 - 3252*t2 - 22*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,2)*(123 + 2112*t2 + 1301*Power(t2,2) + 
                  36*Power(t2,3)) - 
               t1*(396 - 443*t2 + 1141*Power(t2,2) + 88*Power(t2,3)) + 
               Power(s1,2)*t1*
                (20*Power(t1,5) + t1*(1123 - 43*t2) + 
                  Power(t1,4)*(-78 + 4*t2) - 30*(1 + 6*t2) + 
                  Power(t1,3)*(497 + 22*t2) + 
                  Power(t1,2)*(-1405 + 28*t2)) + 
               s1*(14*Power(t1,7) - 4*Power(t1,6)*(37 + 3*t2) + 
                  Power(t1,5)*(395 + 93*t2) + 
                  Power(t1,3)*(3184 + 1169*t2 - 100*Power(t2,2)) - 
                  10*(2 - 10*t2 + 9*Power(t2,2)) + 
                  Power(t1,4)*(-1133 - 306*t2 + 16*Power(t2,2)) - 
                  3*Power(t1,2)*(902 + 703*t2 + 46*Power(t2,2)) + 
                  t1*(71 + 791*t2 + 371*Power(t2,2)))) + 
            Power(s2,3)*(83 + 2*Power(t1,7) + 
               Power(s1,3)*(26 - 217*t1 + 338*Power(t1,2) - 
                  49*Power(t1,3) - 18*Power(t1,4)) + 
               Power(t1,6)*(26 - 10*t2) - 78*t2 - 497*Power(t2,2) - 
               106*Power(t2,3) + 
               Power(t1,5)*(763 + 179*t2 - 20*Power(t2,2)) + 
               Power(t1,4)*(-1176 - 1134*t2 + 155*Power(t2,2)) + 
               Power(t1,3)*(199 + 2362*t2 - 136*Power(t2,2) - 
                  100*Power(t2,3)) - 
               Power(t1,2)*(-681 + 3153*t2 + 1842*Power(t2,2) + 
                  116*Power(t2,3)) + 
               t1*(-646 + 1009*t2 + 1818*Power(t2,2) + 
                  252*Power(t2,3)) + 
               Power(s1,2)*(-160 + 38*Power(t1,5) - 117*t2 + 
                  Power(t1,4)*(-89 + 20*t2) - 
                  Power(t1,3)*(195 + 124*t2) - 
                  Power(t1,2)*(965 + 361*t2) + t1*(809 + 410*t2)) + 
               s1*(17 + 18*Power(t1,6) + 558*t2 + 182*Power(t2,2) + 
                  3*Power(t1,5)*(-81 + 5*t2) + 
                  2*Power(t1,4)*(491 - 72*t2 + 10*Power(t2,2)) + 
                  2*Power(t1,2)*(433 + 1484*t2 + 87*Power(t2,2)) + 
                  Power(t1,3)*(-581 - 48*t2 + 252*Power(t2,2)) - 
                  t1*(210 + 2461*t2 + 450*Power(t2,2)))) + 
            Power(s2,4)*(60 + 53*Power(t1,5) + 20*Power(t1,6) + 
               Power(s1,3)*(40 - 140*t1 - 31*Power(t1,2) + 
                  50*Power(t1,3)) - 77*t2 - 381*Power(t2,2) - 
               166*Power(t2,3) + 
               Power(t1,4)*(-1031 - 890*t2 + 140*Power(t2,2)) + 
               Power(t1,3)*(986 + 2037*t2 + 225*Power(t2,2) - 
                  70*Power(t2,3)) - 
               Power(t1,2)*(-356 + 1173*t2 + 938*Power(t2,2) + 
                  21*Power(t2,3)) + 
               t1*(-383 + 819*t2 + 2059*Power(t2,2) + 532*Power(t2,3)) + 
               Power(s1,2)*(-97 - 15*Power(t1,4) + 
                  Power(t1,3)*(56 - 80*t2) - 167*t2 + 
                  Power(t1,2)*(343 + 101*t2) + t1*(340 + 551*t2)) - 
               s1*(39 + 15*Power(t1,5) - 515*t2 - 240*Power(t2,2) + 
                  2*Power(t1,4)*(-247 + 55*t2) + 
                  Power(t1,3)*(1259 + 80*t2 - 70*Power(t2,2)) + 
                  Power(t1,2)*(117 - 499*t2 + 129*Power(t2,2)) + 
                  t1*(-172 + 2415*t2 + 792*Power(t2,2))))))*T2q(s2,s))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*
       Power(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2),2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*(-1 + t2)) - 
    (8*(-((-1 + s2)*Power(s2 - t1,4)*(-1 + t1)*
            Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3)) + 
         Power(s,7)*(s2 - t1)*Power(t1,2)*
          (Power(t1,3) + t1*(Power(s2,2) + 2*s2*(-2 + t2) + 
               4*(-1 + t2)) + Power(s2,2)*t2 + Power(t1,2)*(-2*s2 + t2)) \
+ Power(s,6)*t1*(Power(s2,4)*
             (-3*Power(t1,2) + t1*(2 + s1 - 4*t2) + 2*t2) + 
            Power(s2,3)*t1*(-11 + 9*Power(t1,2) - s1*t2 + Power(t2,2) + 
               t1*(4 - 5*s1 + 3*t2)) - 
            Power(s2,2)*t1*(9*Power(t1,3) - 12*(-1 + t2) + 
               Power(t1,2)*(8 - 9*s1 + t2) + 
               t1*(-33 + s1*(-4 + t2) + 8*t2 - Power(t2,2))) + 
            Power(t1,2)*(Power(t1,3)*(4 + 2*s1 - 7*t2) + 
               2*t1*(1 + 2*s1*(-1 + t2) + t2 - Power(t2,2)) + 
               Power(t1,2)*(19 + (-6 + s1)*t2 - Power(t2,2)) + 
               2*(2 - 3*t2 + Power(t2,2))) + 
            s2*Power(t1,2)*(24 + 3*Power(t1,3) + 
               s1*(4 - 7*Power(t1,2) + t1*(-4 + t2) - 4*t2) - 36*t2 + 
               10*Power(t2,2) + Power(t1,2)*(-4 + 11*t2) + 
               t1*(-45 + 14*t2 + Power(t2,2)))) + 
         Power(s,5)*(Power(s2,5)*
             (3*Power(t1,3) + t1*(1 + 2*s1 - 6*t2) + t2 + 
               Power(t1,2)*(-4 - 3*s1 + 6*t2)) - 
            Power(s2,4)*t1*(10 + 9*Power(t1,3) + t2 + 2*s1*t2 - 
               Power(t2,2) + Power(t1,2)*(-13*s1 + 15*t2) + 
               t1*(-20 + Power(s1,2) - 3*s1*(-5 + t2) - 18*t2 + 
                  4*Power(t2,2))) + 
            Power(s2,3)*(10*Power(t1,5) + Power(t2,3) + 
               Power(t1,4)*(20 - 19*s1 + 12*t2) + 
               Power(t1,3)*(-65 + 4*Power(s1,2) - 4*s1*(-8 + t2) - 
                  55*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(49 + s1*(11 - 3*t2) - 4*t2 + 
                  7*Power(t2,2)) + 
               t1*(-12 + 12*t2 + s1*Power(t2,2) - Power(t2,3))) - 
            Power(t1,3)*(18 + Power(t1,5) - 32*t2 + 16*Power(t2,2) - 
               Power(t2,3) + Power(t1,4)*(2*s1 + 5*t2) + 
               Power(t1,2)*(17 - s1*(-5 + t2) + 11*t2) + 
               Power(t1,3)*(-37 + Power(s1,2) + s1*(9 - 7*t2) - 
                  17*t2 + 6*Power(t2,2)) + 
               t1*(58 - 46*t2 - 8*Power(t2,2) + Power(t2,3) - 
                  s1*(22 - 24*t2 + Power(t2,2)))) - 
            Power(s2,2)*t1*(6*Power(t1,5) + 3*Power(t2,3) + 
               Power(t1,3)*(-111 + 6*Power(s1,2) - 6*s1*(-6 + t2) - 
                  97*t2 + 10*Power(t2,2)) + 
               Power(t1,4)*(-9*s1 + 16*(1 + t2)) + 
               3*t1*(-12 + 24*t2 - 10*Power(t2,2) - Power(t2,3) + 
                  s1*(-4 + 4*t2 + Power(t2,2))) + 
               Power(t1,2)*(111 - 40*t2 + 37*Power(t2,2) - 
                  2*Power(t2,3) + s1*(37 - 25*t2 + 2*Power(t2,2)))) + 
            s2*Power(t1,2)*(3*Power(t1,5) + 2*Power(t1,4)*(s1 + 9*t2) + 
               Power(t1,3)*(-104 + 4*Power(s1,2) + s1*(26 - 12*t2) - 
                  71*t2 + 16*Power(t2,2)) + 
               3*(4 - 6*t2 + 2*Power(t2,2) + Power(t2,3)) + 
               t1*(10 + 70*t2 - 78*Power(t2,2) + 7*Power(t2,3) + 
                  s1*(-34 + 36*t2 + Power(t2,2))) + 
               Power(t1,2)*(83 - 11*t2 + 19*Power(t2,2) + 
                  s1*(31 - 21*t2 + 2*Power(t2,2))))) + 
         Power(s,4)*(-(Power(s2,6)*(-1 + t1)*
               (s1 - 3*s1*t1 + Power(t1,2) - 2*t2 + t1*(-1 + 4*t2))) + 
            Power(s2,5)*(-3 + 3*Power(t1,4) - s1*t2 + 
               Power(t1,3)*(3 - 11*s1 + 17*t2) - 
               t1*(-16 + 2*Power(s1,2) + s1*(13 - 2*t2) - 21*t2 + 
                  Power(t2,2)) + 
               Power(t1,2)*(-17 + Power(s1,2) - 3*s1*(-8 + t2) - 
                  38*t2 + 6*Power(t2,2))) + 
            Power(s2,4)*(-4 - 4*Power(t1,5) + 
               2*Power(t1,4)*(-14 + 7*s1 - 8*t2) + 4*t2 + 
               (-3 + 4*s1)*Power(t2,2) - 7*Power(t2,3) + 
               Power(t1,2)*(-60 + s1*(39 - 5*t2) - 131*t2 - 
                  15*Power(t2,2) + 2*Power(s1,2)*(6 + t2)) + 
               Power(t1,3)*(71 + 129*t2 - 19*Power(t2,2) + 
                  3*s1*(-17 + 4*t2)) + 
               t1*(17 + 3*t2 + 2*Power(s1,2)*t2 + 12*Power(t2,2) + 
                  5*Power(t2,3) + s1*(10 - 4*t2 - 5*Power(t2,2)))) + 
            Power(s2,3)*((24 - 5*s1)*Power(t1,5) + 6*Power(t1,6) + 
               (3 - 2*t2)*Power(t2,2) - 
               Power(t1,4)*(135 + 9*Power(s1,2) + 230*t2 - 
                  32*Power(t2,2) + s1*(-33 + 16*t2)) + 
               t1*(16 - 49*t2 + 47*Power(t2,2) + 27*Power(t2,3) + 
                  s1*(12 - 13*t2 - 17*Power(t2,2))) - 
               Power(t1,2)*(37 - 12*t2 + 85*Power(t2,2) + 
                  10*Power(t2,3) + Power(s1,2)*(2 + 7*t2) + 
                  s1*(65 - 63*t2 - 9*Power(t2,2))) + 
               Power(t1,3)*(154 + 337*t2 + 35*Power(t2,2) - 
                  7*Power(t2,3) - 3*Power(s1,2)*(11 + t2) + 
                  s1*(-31 - 2*t2 + 4*Power(t2,2)))) + 
            s2*Power(t1,2)*(-42 + 7*Power(t1,6) + 
               Power(t1,5)*(4*s1 + 7*(-5 + t2)) + 78*t2 - 
               33*Power(t2,2) - 
               Power(t1,4)*(110 + 12*Power(s1,2) + 76*t2 - 
                  20*Power(t2,2) + s1*(41 + t2)) + 
               Power(t1,3)*(258 + 198*t2 - 58*Power(t2,2) + 
                  3*Power(t2,3) + Power(s1,2)*(-29 + 3*t2) + 
                  4*s1*(-5 + 16*t2)) + 
               t1*(-184 + 45*t2 + 193*Power(t2,2) - 11*Power(t2,3) + 
                  s1*(100 - 119*t2 - 3*Power(t2,2))) - 
               Power(t1,2)*(-164 + 174*t2 + 119*Power(t2,2) - 
                  20*Power(t2,3) + Power(s1,2)*(10 + t2) + 
                  s1*(71 - 90*t2 + 5*Power(t2,2)))) - 
            Power(t1,3)*(-28 + 2*Power(t1,6) + 
               Power(t1,5)*(s1 + 2*(-5 + t2)) + 54*t2 - 
               27*Power(t2,2) + 2*Power(t2,3) - 
               Power(t1,4)*(32 + 14*s1 + 3*Power(s1,2) + 5*t2 - 
                  3*Power(t2,2)) + 
               Power(t1,2)*(106 + Power(s1,2)*(-4 + t2) - 141*t2 + 
                  7*Power(t2,2) + Power(t2,3) + s1*(-11 + 15*t2)) + 
               Power(t1,3)*(92 + Power(s1,2)*(-7 + t2) + 22*t2 - 
                  18*Power(t2,2) + Power(t2,3) + 
                  s1*(-16 + 35*t2 - 2*Power(t2,2))) + 
               t1*(-124 + 105*t2 + 36*Power(t2,2) - 6*Power(t2,3) + 
                  s1*(46 - 57*t2 + 5*Power(t2,2)))) + 
            Power(s2,2)*t1*(-9*Power(t1,6) - 
               2*Power(t1,5)*(-12 + 2*s1 + t2) + 
               3*(4 - 6*t2 - Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,4)*(17*Power(s1,2) + s1*(25 + 8*t2) + 
                  4*(40 + 51*t2 - 9*Power(t2,2))) + 
               Power(t1,2)*(-29 + 217*Power(t2,2) - 20*Power(t2,3) + 
                  Power(s1,2)*(8 + 7*t2) + 
                  s1*(115 - 133*t2 + Power(t2,2))) - 
               Power(t1,3)*(274 + Power(s1,2)*(-45 + t2) + 407*t2 - 
                  27*Power(t2,2) - 3*Power(t2,3) + 
                  s1*(-8 + 24*t2 + 6*Power(t2,2))) + 
               3*t1*(14 + 41*t2 - 73*Power(t2,2) - 3*Power(t2,3) + 
                  s1*(-22 + 25*t2 + 7*Power(t2,2))))) - 
         s*Power(s2 - t1,3)*(2*Power(s2,5)*(-1 + t1)*Power(s1 - t2,2) - 
            Power(-1 + t2,3) + 2*t1*Power(-1 + t2,2)*(-5 + s1 + 3*t2) + 
            (-1 + s1)*Power(t1,5)*
             (-5 + Power(s1,2) - 3*s1*(-2 + t2) + 7*t2) + 
            Power(t1,2)*(-1 + t2)*
             (-19 + Power(s1,2) + s1*(3 - 12*t2) + 26*t2 - 
               5*Power(t2,2)) - 
            Power(t1,3)*(7 + 2*Power(s1,3) - 17*t2 + 2*Power(t2,2) + 
               3*Power(t2,3) - Power(s1,2)*(13 + 2*t2) + 
               s1*(11 + 20*t2 - 13*Power(t2,2))) + 
            Power(t1,4)*(4*Power(s1,3) - Power(s1,2)*(17 + 9*t2) - 
               2*(4 - 6*t2 + 5*Power(t2,2)) + 
               s1*(23 - t2 + 6*Power(t2,2))) + 
            Power(s2,4)*(Power(s1,3)*(1 + 2*t1) - 
               Power(s1,2)*(-2 + t1 + Power(t1,2) + 10*t2 - t1*t2) + 
               t2*(-(Power(t1,3)*(-1 + t2)) - 4*t2*(1 + 2*t2) - 
                  Power(t1,2)*(2 + 5*t2) + 
                  t1*(1 + 10*t2 + 5*Power(t2,2))) + 
               s1*(Power(t1,3)*(-1 + t2) + Power(t1,2)*(2 + 6*t2) + 
                  t2*(2 + 17*t2) - t1*(1 + 9*t2 + 8*Power(t2,2)))) + 
            Power(s2,3)*(2 + Power(s1,3)*(2 - 7*t1 - 7*Power(t1,2)) - 
               7*t2 - 11*Power(t2,2) + 11*Power(t2,3) + 
               Power(t1,4)*(-1 + Power(t2,2)) + 
               Power(t1,3)*(1 + 5*t2 + 4*Power(t2,2) - Power(t2,3)) - 
               Power(t1,2)*(-3 + 17*t2 + 19*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-5 + 19*t2 + 25*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s1,2)*(Power(t1,3)*(-10 + t2) + 3*(-6 + t2) + 
                  5*Power(t1,2)*(1 + t2) + t1*(23 + 27*t2)) - 
               s1*(3 - Power(t1,3)*(-1 + t2) + 
                  2*Power(t1,4)*(-1 + t2) - 37*t2 + 16*Power(t2,2) + 
                  Power(t1,2)*(7 - 35*t2 - 3*Power(t2,2)) + 
                  t1*(-9 + 71*t2 + 23*Power(t2,2)))) + 
            s2*(Power(s1,3)*Power(t1,2)*(6 - 13*t1 - 5*Power(t1,2)) + 
               Power(t1,5)*(3 - 2*t2) - Power(-1 + t2,2)*(-7 + 3*t2) + 
               Power(t1,4)*(-13 + 2*t2 + 7*Power(t2,2)) + 
               t1*(-17 + 39*t2 - 23*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(14 + 7*t2 + 4*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(6 - 29*t2 - Power(t2,2) + 9*Power(t2,3)) + 
               Power(s1,2)*t1*
                (2 + Power(t1,4)*(-6 + t2) + 
                  13*Power(t1,3)*(-1 + t2) - 2*t2 - t1*(44 + t2) + 
                  Power(t1,2)*(61 + 25*t2)) + 
               s1*(-2*Power(-1 + t2,2) + Power(t1,5)*(3 + t2) + 
                  Power(t1,2)*(46 + 23*t2 - 15*Power(t2,2)) + 
                  Power(t1,4)*(20 - 5*t2 - 6*Power(t2,2)) + 
                  t1*(-7 - 4*t2 + 11*Power(t2,2)) - 
                  Power(t1,3)*(60 + 19*t2 + 24*Power(t2,2)))) + 
            Power(s2,2)*(1 + 3*Power(s1,3)*t1*
                (-2 + 5*t1 + 3*Power(t1,2)) - Power(t1,5)*(-1 + t2) - 
               3*t2 + Power(t2,2) + Power(t2,3) + 
               Power(t1,4)*(-3 - 4*t2 + 2*Power(t2,2)) + 
               t1*(-3 + 25*t2 + 8*Power(t2,2) - 15*Power(t2,3)) - 
               Power(t1,3)*(-2 - 30*t2 + 5*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(-2 + 47*t2 + 6*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s1,2)*(-1 + Power(t1,3)*(6 - 16*t2) + 
                  t1*(49 - 4*t2) + Power(t1,4)*(15 - 2*t2) + t2 - 
                  3*Power(t1,2)*(23 + 11*t2)) + 
               s1*(10 + Power(t1,5)*(-1 + t2) - 11*t2 + Power(t2,2) - 
                  4*Power(t1,4)*(1 + t2) + 
                  2*t1*(-16 - 20*t2 + 9*Power(t2,2)) + 
                  Power(t1,3)*(-1 - 35*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*(28 + 89*t2 + 24*Power(t2,2))))) - 
         Power(s,3)*(s2 - t1)*
          (Power(s2,6)*Power(-1 + t1,2)*(s1 - t2) + 
            Power(s2,5)*(-3 + Power(s1,2)*(1 + t1 - Power(t1,2)) - 
               8*t2 - 2*Power(t2,2) + Power(t1,3)*(3 + 5*t2) + 
               t1*(7 + 27*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-7 - 20*t2 + 4*Power(t2,2)) + 
               s1*(3 - 2*Power(t1,3) - Power(t1,2)*(-5 + t2) + t2 - 
                  2*t1*(5 + 3*t2))) - 
            Power(s2,4)*(-3 + 10*Power(t1,4) + 
               Power(s1,3)*t1*(1 + 2*t1) + 4*t2 + 17*Power(t2,2) + 
               17*Power(t2,3) + 
               Power(s1,2)*(-7*Power(t1,3) + Power(t1,2)*(13 - 4*t2) - 
                  t1*(-10 + t2) + 5*t2) + 
               Power(t1,2)*(18 + 138*t2 + 17*Power(t2,2)) + 
               Power(t1,3)*(-26 - 63*t2 + 19*Power(t2,2)) - 
               t1*(-1 + 55*t2 + 24*Power(t2,2) + 10*Power(t2,3)) - 
               s1*(-3 + 2*Power(t1,4) + 8*t2 + 19*Power(t2,2) + 
                  12*Power(t1,2)*(4 + t2) + Power(t1,3)*(-15 + 8*t2) + 
                  t1*(-8 + 17*t2 - 9*Power(t2,2)))) - 
            Power(t1,2)*(16 - (9 + s1)*Power(t1,6) + Power(t1,7) - 
               27*t2 + 11*Power(t2,2) + 
               Power(t1,5)*(5 - 2*Power(s1,2) + s1*(-5 + t2) - 3*t2 + 
                  Power(t2,2)) + 
               t1*(121 - 102*t2 - 22*Power(t2,2) + 5*Power(t2,3) + 
                  s1*(-48 + 56*t2 - 9*Power(t2,2))) + 
               Power(t1,4)*(56 + Power(s1,3) - 
                  2*Power(s1,2)*(-6 + t2) - 9*t2 - 5*Power(t2,2) + 
                  s1*(-2 + 8*t2 + Power(t2,2))) + 
               Power(t1,2)*(-170 + Power(s1,2)*(14 - 3*t2) + 251*t2 + 
                  8*Power(t2,2) - 5*Power(t2,3) + 
                  s1*(-22 - 45*t2 + 6*Power(t2,2))) + 
               Power(t1,3)*(-20 + 2*Power(s1,3) + 
                  Power(s1,2)*(4 - 3*t2) - 28*t2 + 24*Power(t2,2) - 
                  3*Power(t2,3) + s1*(-4 - 65*t2 + 10*Power(t2,2)))) + 
            s2*t1*(26 - 5*(7 + s1)*Power(t1,6) + 3*Power(t1,7) - 
               48*t2 + 28*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,5)*(17 - 10*Power(s1,2) - 26*t2 + 
                  5*Power(t2,2) + s1*(-9 + 4*t2)) + 
               t1*(184 - 63*t2 - 128*Power(t2,2) + 13*Power(t2,3) + 
                  s1*(-88 + 87*t2 - 2*Power(t2,2))) + 
               Power(t1,4)*(128 + 5*Power(s1,3) + 
                  Power(s1,2)*(41 - 5*t2) + 104*t2 - 41*Power(t2,2) + 
                  3*Power(t2,3) + s1*(12 - 13*t2 + 4*Power(t2,2))) + 
               Power(t1,3)*(-24 + 7*Power(s1,3) + 
                  Power(s1,2)*(25 - 16*t2) - 150*t2 + 37*Power(t2,2) + 
                  7*Power(t2,3) + s1*(-105 - 92*t2 + 8*Power(t2,2))) + 
               Power(t1,2)*(-299 + 407*t2 + 192*Power(t2,2) - 
                  13*Power(t2,3) + Power(s1,2)*(33 + 5*t2) - 
                  s1*(29 + 168*t2 + 10*Power(t2,2)))) + 
            Power(s2,2)*(-4 - 3*Power(t1,7) + 3*t2 + Power(t2,2) + 
               Power(t1,6)*(39 + 8*s1 + 7*t2) + 
               Power(t1,5)*(-1 + 19*Power(s1,2) + s1*(-8 + t2) + 
                  60*t2 - 12*Power(t2,2)) + 
               Power(t1,4)*(-80 - 9*Power(s1,3) - 340*t2 + 
                  56*Power(t2,2) + Power(s1,2)*(-57 + 8*t2) + 
                  s1*(1 + 50*t2 - 5*Power(t2,2))) + 
               Power(t1,3)*(-23 - 9*Power(s1,3) + 255*t2 + 
                  30*Power(t2,2) - 21*Power(t2,3) + 
                  Power(s1,2)*(-46 + 24*t2) + 
                  s1*(190 + 20*t2 - Power(t2,2))) + 
               Power(t1,2)*(125 - 119*t2 - 371*Power(t2,2) + 
                  8*Power(t2,3) - 24*Power(s1,2)*(1 + t2) + 
                  17*s1*(-1 + 13*t2 + 3*Power(t2,2))) - 
               t1*(53 + 84*t2 - 142*Power(t2,2) + 11*Power(t2,3) + 
                  s1*(-44 + 30*t2 + 11*Power(t2,2)))) + 
            Power(s2,3)*(Power(t1,6) + 
               Power(s1,3)*Power(t1,2)*(5 + 7*t1) - 
               Power(t1,5)*(6 + 11*t2) + 
               Power(s1,2)*t1*
                (5 - 17*Power(t1,3) + t1*(34 - 12*t2) + 
                  Power(t1,2)*(40 - 9*t2) + 21*t2) + 
               t2*(15 - 6*t2 - 7*Power(t2,2)) + 
               Power(t1,4)*(-30 - 82*t2 + 23*Power(t2,2)) + 
               Power(t1,3)*(25 + 321*t2 + 12*Power(t2,2) - 
                  9*Power(t2,3)) - 
               Power(t1,2)*(-41 + 210*t2 + 35*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(-31 + 63*t2 + 108*Power(t2,2) + 49*Power(t2,3)) - 
               s1*(4 + 5*Power(t1,5) + t2 - 4*Power(t2,2) + 
                  Power(t1,4)*(-24 + 11*t2) + 
                  Power(t1,2)*(84 + 11*t2 - 12*Power(t2,2)) + 
                  Power(t1,3)*(54 + 35*t2 - 2*Power(t2,2)) + 
                  t1*(-27 + 106*t2 + 54*Power(t2,2))))) - 
         Power(s,2)*Power(s2 - t1,2)*
          (Power(s2,5)*(Power(s1,2)*(2 - 5*t1 + Power(t1,2)) + 
               2*s1*(-1 + Power(t1,2) - 3*t2 + 5*t1*t2) + 
               t2*(6 - Power(t1,2)*(-2 + t2) + 4*t2 - t1*(8 + 5*t2))) - 
            t1*((1 + s1)*Power(t1,7) + Power(-1 + t2,2)*(-1 + 2*t2) + 
               Power(t1,5)*(1 - 9*Power(s1,2) + t2 + Power(t2,2) + 
                  2*s1*(3 + t2)) + 
               Power(t1,6)*(Power(s1,2) + 2*(-2 + t2) - s1*(4 + t2)) - 
               t1*(-1 + t2)*(54 - 22*t2 + 5*Power(t2,2) + 
                  s1*(-16 + 7*t2)) + 
               Power(t1,4)*(21 + Power(s1,3) + Power(s1,2)*(8 - 6*t2) - 
                  33*t2 - 6*Power(t2,2) - s1*(16 - 50*t2 + Power(t2,2))) \
- Power(t1,2)*(Power(s1,2)*(-5 + 3*t2) + 
                  s1*(9 + 23*t2 - 15*Power(t2,2)) + 
                  9*(10 - 16*t2 + 3*Power(t2,2))) + 
               Power(t1,3)*(18 + 6*Power(s1,3) - 42*t2 - 
                  23*Power(t2,2) + Power(t2,3) - 
                  Power(s1,2)*(38 + 7*t2) + 
                  s1*(38 + 15*t2 + 4*Power(t2,2)))) + 
            Power(s2,4)*(-4 + Power(s1,3)*(-2 - 3*t1 + 2*Power(t1,2)) - 
               Power(t1,4)*(-1 + t2) - 3*t2 + 16*Power(t2,2) + 
               18*Power(t2,3) + 
               2*Power(t1,3)*(-2 - 5*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(1 + 31*t2 + 13*Power(t2,2)) - 
               t1*(-6 + 17*t2 + 22*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s1,2)*(-2*Power(t1,3) + Power(t1,2)*(21 - 2*t2) + 
                  13*t2 + t1*(-4 + 3*t2)) + 
               s1*(1 - 3*Power(t1,3)*(-3 + t2) - 29*Power(t2,2) - 
                  4*Power(t1,2)*(7 + 6*t2) + 
                  t1*(18 - 3*t2 + 10*Power(t2,2)))) + 
            s2*(Power(t1,7)*(2 + 3*s1 + t2) - 
               Power(-1 + t2,2)*(-5 + 4*t2) + 
               Power(t1,6)*(-15 + 2*Power(s1,2) + s1*(-28 + t2) + 
                  13*t2 - Power(t2,2)) + 
               t1*(-1 + t2)*(-70 + s1*(25 - 7*t2) - 11*t2 + 
                  7*Power(t2,2)) - 
               Power(t1,2)*(155 - 246*t2 + 4*Power(t2,2) + 
                  6*Power(t2,3) + 2*Power(s1,2)*(-5 + 2*t2) + 
                  s1*(36 + 28*t2 - 13*Power(t2,2))) + 
               Power(t1,5)*(11 - 2*Power(s1,3) + 
                  Power(s1,2)*(-32 + t2) - 8*t2 + 
                  s1*(16 + 33*t2 - 3*Power(t2,2))) + 
               Power(t1,4)*(15 + 6*Power(s1,3) + 
                  Power(s1,2)*(17 - 16*t2) - 11*t2 - 45*Power(t2,2) + 
                  s1*(24 + 65*t2 + 9*Power(t2,2))) + 
               Power(t1,3)*(67 + 20*Power(s1,3) - 168*t2 - 
                  53*Power(t2,2) - 11*Power(t2,3) - 
                  Power(s1,2)*(105 + 43*t2) + 
                  s1*(46 + 113*t2 + 40*Power(t2,2)))) + 
            Power(s2,3)*(-9 + 
               2*Power(s1,3)*t1*(6 + 5*t1 - 3*Power(t1,2)) + 30*t2 + 
               8*Power(t2,2) - 2*Power(t2,3) + Power(t1,5)*(-2 + 3*t2) + 
               Power(t1,4)*(9 + 7*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*(-58 + 93*t2 + 12*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,3)*(15 - 59*t2 - 15*Power(t2,2) + 
                  5*Power(t2,3)) - 
               t1*(-45 + 74*t2 + 65*Power(t2,2) + 33*Power(t2,3)) + 
               Power(s1,2)*(Power(t1,4) + Power(t1,2)*(3 - 10*t2) + 
                  2*t2 + Power(t1,3)*(-41 + 5*t2) - t1*(29 + 55*t2)) + 
               s1*(4 + Power(t1,5) - 26*t2 + 5*Power(t2,2) + 
                  Power(t1,4)*(-44 + 9*t2) + Power(t1,3)*(63 + 41*t2) + 
                  Power(t1,2)*(28 - 15*t2 - 7*Power(t2,2)) + 
                  t1*(-52 + 123*t2 + 70*Power(t2,2)))) + 
            Power(s2,2)*(-16 + 
               6*Power(s1,3)*Power(t1,2)*(-4 - 2*t1 + Power(t1,2)) + 
               Power(t1,5)*(6 - 10*t2) - 17*t2 - 3*Power(t1,6)*t2 + 
               45*Power(t2,2) - 12*Power(t2,3) + 
               Power(t1,2)*(-60 + 113*t2 + 169*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(t1,4)*(-32 + 63*t2 - 10*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(58 - 104*t2 + 45*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(44 - 42*t2 - 121*Power(t2,2) + 38*Power(t2,3)) - 
               Power(s1,2)*t1*
                (5 + Power(t1,4) + Power(t1,2)*(10 - 17*t2) + 
                  4*Power(t1,3)*(-12 + t2) + t2 - 6*t1*(16 + 13*t2)) - 
               s1*(3*Power(t1,6) + 9*(-1 + t2) + 
                  Power(t1,5)*(-57 + 8*t2) + 
                  Power(t1,4)*(45 + 58*t2 - 3*Power(t2,2)) + 
                  t1*(-23 - 31*t2 + 3*Power(t2,2)) + 
                  Power(t1,3)*(84 - 9*t2 + 13*Power(t2,2)) + 
                  Power(t1,2)*(-43 + 221*t2 + 77*Power(t2,2))))))*T3q(s2,t1)\
)/(Power(s,2)*(-1 + s1)*(-1 + s2)*Power(s2 - t1,2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*(-1 + t2)) + 
    (8*((-1 + s2)*(s2 - t1)*Power(-1 + t1,4)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         Power(s,7)*(-1 + t1)*Power(t1,2)*
          (Power(t1,3) + Power(t1,2)*(-2 + t2) + t2 + t1*(-7 + 6*t2)) - 
         Power(s,6)*t1*(Power(t1,5)*(4 + 2*s1 + 3*s2 - 7*t2) + 2*s2*t2 + 
            Power(t1,4)*(23 + t2 - Power(t2,2) + s1*(-12 - s2 + t2) + 
               s2*(-11 + 4*t2)) + 
            t1*(1 - (6 + s1)*t2 + Power(t2,2) + s2*(-22 + s1 + 14*t2)) + 
            Power(t1,2)*(23 + s2*(47 - 38*t2) + t2 + 13*Power(t2,2) - 
               s1*(2 + 3*s2 + 15*t2)) + 
            Power(t1,3)*(-51 - t2 - Power(t2,2) + 3*s1*(8 + s2 + t2) + 
               s2*(-17 + 18*t2))) + 
         Power(s,5)*(Power(t1,8) - Power(t2,3) + 
            t1*Power(t2,2)*(-s1 + 4*t2) + 
            Power(t1,7)*(-3 + 2*s1 + 5*t2) + 
            Power(t1,6)*(-58 + Power(s1,2) - 7*s1*(-3 + t2) - 14*t2 + 
               6*Power(t2,2)) + 
            Power(t1,2)*(5 + s1 - 11*t2 - 3*s1*t2 + 2*Power(t2,2) + 
               3*s1*Power(t2,2) - 6*Power(t2,3)) + 
            Power(t1,5)*(155 - 7*Power(s1,2) + 75*t2 - 12*Power(t2,2) + 
               s1*(-88 + 6*t2)) + 
            Power(s2,2)*Power(-1 + t1,2)*
             (3*Power(t1,4) + t1*(23 - 2*s1 - 17*t2) - t2 + 
               Power(t1,3)*(-7 - 3*s1 + 6*t2) + 
               Power(t1,2)*(-43 + 5*s1 + 36*t2)) - 
            Power(t1,3)*(-24 - 62*t2 - 50*Power(t2,2) + 
               10*Power(t2,3) + Power(s1,2)*(1 + 10*t2) + 
               s1*(20 + 60*t2 - 21*Power(t2,2))) + 
            Power(t1,4)*(-124 + Power(s1,2)*(31 - 2*t2) - 117*t2 - 
               22*Power(t2,2) + Power(t2,3) + 
               s1*(84 + 16*t2 + Power(t2,2))) + 
            s2*t1*(-2 + Power(t1,5)*(6 + 4*s1 - 21*t2) + 2*(5 + s1)*t2 - 
               Power(t2,2) + Power(t1,3)*
                (-249 + 13*Power(s1,2) + s1*(121 - 3*t2) - 23*t2 + 
                  Power(t2,2)) - 
               Power(t1,4)*(-87 + Power(s1,2) + s1*(37 - 3*t2) - 
                  19*t2 + 4*Power(t2,2)) + 
               t1*(-53 + Power(s1,2) - 18*t2 - 41*Power(t2,2) + 
                  9*s1*(1 + 5*t2)) - 
               Power(t1,2)*(-211 + Power(s1,2) - 33*t2 - 
                  57*Power(t2,2) + s1*(97 + 71*t2)))) + 
         s*Power(-1 + t1,3)*(2*Power(s2,5)*(-1 + t1)*Power(s1 - t2,2) - 
            Power(-1 + t2,3) + 2*t1*Power(-1 + t2,2)*(-5 + s1 + 3*t2) + 
            (-1 + s1)*Power(t1,5)*
             (-5 + Power(s1,2) - 3*s1*(-2 + t2) + 7*t2) + 
            Power(t1,2)*(-1 + t2)*
             (-19 + Power(s1,2) + s1*(3 - 12*t2) + 26*t2 - 
               5*Power(t2,2)) - 
            Power(t1,3)*(7 + 2*Power(s1,3) - 17*t2 + 2*Power(t2,2) + 
               3*Power(t2,3) - Power(s1,2)*(13 + 2*t2) + 
               s1*(11 + 20*t2 - 13*Power(t2,2))) + 
            Power(t1,4)*(4*Power(s1,3) - Power(s1,2)*(17 + 9*t2) - 
               2*(4 - 6*t2 + 5*Power(t2,2)) + 
               s1*(23 - t2 + 6*Power(t2,2))) + 
            Power(s2,4)*(Power(s1,3)*(1 + 2*t1) - 
               Power(s1,2)*(-2 + t1 + Power(t1,2) + 10*t2 - t1*t2) + 
               t2*(-(Power(t1,3)*(-1 + t2)) - 4*t2*(1 + 2*t2) - 
                  Power(t1,2)*(2 + 5*t2) + 
                  t1*(1 + 10*t2 + 5*Power(t2,2))) + 
               s1*(Power(t1,3)*(-1 + t2) + Power(t1,2)*(2 + 6*t2) + 
                  t2*(2 + 17*t2) - t1*(1 + 9*t2 + 8*Power(t2,2)))) + 
            Power(s2,3)*(2 + Power(s1,3)*(2 - 7*t1 - 7*Power(t1,2)) - 
               7*t2 - 11*Power(t2,2) + 11*Power(t2,3) + 
               Power(t1,4)*(-1 + Power(t2,2)) + 
               Power(t1,3)*(1 + 5*t2 + 4*Power(t2,2) - Power(t2,3)) - 
               Power(t1,2)*(-3 + 17*t2 + 19*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-5 + 19*t2 + 25*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s1,2)*(Power(t1,3)*(-10 + t2) + 3*(-6 + t2) + 
                  5*Power(t1,2)*(1 + t2) + t1*(23 + 27*t2)) - 
               s1*(3 - Power(t1,3)*(-1 + t2) + 
                  2*Power(t1,4)*(-1 + t2) - 37*t2 + 16*Power(t2,2) + 
                  Power(t1,2)*(7 - 35*t2 - 3*Power(t2,2)) + 
                  t1*(-9 + 71*t2 + 23*Power(t2,2)))) + 
            s2*(Power(s1,3)*Power(t1,2)*(6 - 13*t1 - 5*Power(t1,2)) + 
               Power(t1,5)*(3 - 2*t2) - Power(-1 + t2,2)*(-7 + 3*t2) + 
               Power(t1,4)*(-13 + 2*t2 + 7*Power(t2,2)) + 
               t1*(-17 + 39*t2 - 23*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(14 + 7*t2 + 4*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(6 - 29*t2 - Power(t2,2) + 9*Power(t2,3)) + 
               Power(s1,2)*t1*
                (2 + Power(t1,4)*(-6 + t2) + 
                  13*Power(t1,3)*(-1 + t2) - 2*t2 - t1*(44 + t2) + 
                  Power(t1,2)*(61 + 25*t2)) + 
               s1*(-2*Power(-1 + t2,2) + Power(t1,5)*(3 + t2) + 
                  Power(t1,2)*(46 + 23*t2 - 15*Power(t2,2)) + 
                  Power(t1,4)*(20 - 5*t2 - 6*Power(t2,2)) + 
                  t1*(-7 - 4*t2 + 11*Power(t2,2)) - 
                  Power(t1,3)*(60 + 19*t2 + 24*Power(t2,2)))) + 
            Power(s2,2)*(1 + 3*Power(s1,3)*t1*
                (-2 + 5*t1 + 3*Power(t1,2)) - Power(t1,5)*(-1 + t2) - 
               3*t2 + Power(t2,2) + Power(t2,3) + 
               Power(t1,4)*(-3 - 4*t2 + 2*Power(t2,2)) + 
               t1*(-3 + 25*t2 + 8*Power(t2,2) - 15*Power(t2,3)) - 
               Power(t1,3)*(-2 - 30*t2 + 5*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(-2 + 47*t2 + 6*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s1,2)*(-1 + Power(t1,3)*(6 - 16*t2) + 
                  t1*(49 - 4*t2) + Power(t1,4)*(15 - 2*t2) + t2 - 
                  3*Power(t1,2)*(23 + 11*t2)) + 
               s1*(10 + Power(t1,5)*(-1 + t2) - 11*t2 + Power(t2,2) - 
                  4*Power(t1,4)*(1 + t2) + 
                  2*t1*(-16 - 20*t2 + 9*Power(t2,2)) + 
                  Power(t1,3)*(-1 - 35*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*(28 + 89*t2 + 24*Power(t2,2))))) + 
         Power(s,4)*(2*Power(t1,9) + Power(t1,8)*(s1 + 2*(-8 + t2)) + 
            Power(t2,2)*(-3 + 2*t2) + 
            Power(s2,3)*Power(-1 + t1,3)*
             (-8 - Power(t1,3) + s1*(1 - 4*t1 + 3*Power(t1,2)) + 
               t1*(31 - 26*t2) + Power(t1,2)*(2 - 4*t2) + 6*t2) + 
            t1*t2*(-3 + s1 + t2 + 5*s1*t2 - 12*Power(t2,2)) + 
            Power(t1,7)*(-12 + s1 - 3*Power(s1,2) - t2 + 
               3*Power(t2,2)) + 
            Power(t1,6)*(152 + Power(s1,2)*(-10 + t2) + 71*t2 - 
               21*Power(t2,2) + Power(t2,3) + 
               s1*(-37 + 5*t2 - 2*Power(t2,2))) + 
            2*Power(t1,5)*(-123 + Power(s1,3) + 
               Power(s1,2)*(57 - 7*t2) - 119*t2 + 14*Power(t2,2) - 
               Power(t2,3) + s1*(52 - 29*t2 + 6*Power(t2,2))) + 
            Power(t1,2)*(-8 + 12*t2 + 16*Power(t2,2) + 25*Power(t2,3) + 
               Power(s1,2)*(2 + t2) - s1*(1 + 12*t2 + 15*Power(t2,2))) + 
            Power(t1,4)*(144 + 14*Power(s1,3) + 263*t2 + 
               46*Power(t2,2) - 24*Power(t2,3) - 
               2*Power(s1,2)*(71 + 21*t2) + 
               s1*(-107 + 7*t2 + 59*Power(t2,2))) - 
            Power(t1,3)*(4*Power(s1,3) - 3*Power(s1,2)*(13 + 6*t2) + 
               s1*(-39 - 57*t2 + 23*Power(t2,2)) + 
               2*(8 + 53*t2 + 35*Power(t2,2) + Power(t2,3))) - 
            Power(s2,2)*(-1 + t1)*
             (-1 + Power(t1,5)*(1 + 2*s1 - 21*t2) + (4 + s1)*t2 + 
               Power(t1,3)*(-271 + 38*Power(s1,2) + s1*(138 - 28*t2) - 
                  58*t2 + 5*Power(t2,2)) - 
               Power(t1,4)*(-114 + 28*s1 + Power(s1,2) - 14*t2 - 
                  3*s1*t2 + 6*Power(t2,2)) + 
               t1*(-38 + 2*Power(s1,2) - 23*t2 - 47*Power(t2,2) + 
                  10*s1*(1 + 5*t2)) + 
               Power(t1,2)*(195 + Power(s1,2) + 84*t2 + 
                  88*Power(t2,2) - 2*s1*(61 + 53*t2))) - 
            s2*(Power(t1,8) + (-3 + 4*s1 - 7*t2)*Power(t2,2) + 
               Power(t1,7)*(-6 + s1 + 11*t2) + 
               Power(t1,5)*(494 + 202*t2 - 55*Power(t2,2) - 
                  Power(s1,2)*(29 + 2*t2) + 10*s1*(-23 + 5*t2)) - 
               Power(t1,6)*(136 + 3*Power(s1,2) + 26*t2 - 
                  17*Power(t2,2) + s1*(-34 + 13*t2)) + 
               Power(t1,2)*(-36 - 137*t2 - 135*Power(t2,2) + 
                  6*Power(t2,3) + Power(s1,2)*(6 + 26*t2) + 
                  s1*(54 + 136*t2 - 45*Power(t2,2))) + 
               t1*(-8 + 7*t2 + 2*Power(s1,2)*t2 + 9*Power(t2,2) + 
                  26*Power(t2,3) + s1*(-2 + 5*t2 - 17*Power(t2,2))) + 
               Power(t1,4)*(-613 + 10*Power(s1,3) + 
                  Power(s1,2)*(115 - 18*t2) - 493*t2 - 25*Power(t2,2) + 
                  5*Power(t2,3) + s1*(404 + t2 + 7*Power(t2,2))) + 
               Power(t1,3)*(304 + 2*Power(s1,3) + 436*t2 + 
                  192*Power(t2,2) - 42*Power(t2,3) - 
                  Power(s1,2)*(89 + 44*t2) + 
                  s1*(-261 - 179*t2 + 87*Power(t2,2))))) - 
         Power(s,2)*Power(-1 + t1,2)*
          (1 + 9*t1 - 42*Power(t1,2) + 46*Power(t1,3) + 11*Power(t1,4) - 
            46*Power(t1,5) + 25*Power(t1,6) - 5*Power(t1,7) + 
            Power(t1,8) + Power(s1,3)*Power(s2 - t1,2)*
             (t1*(6 - 31*t1 - 5*Power(t1,2)) + 
               s2*(2 + 7*t1 + 19*Power(t1,2) + 2*Power(t1,3))) - 
            30*t1*t2 + 72*Power(t1,2)*t2 - 52*Power(t1,3)*t2 - 
            5*Power(t1,4)*t2 + 30*Power(t1,5)*t2 - 17*Power(t1,6)*t2 + 
            2*Power(t1,7)*t2 - 3*Power(t2,2) + 28*t1*Power(t2,2) - 
            56*Power(t1,2)*Power(t2,2) + 22*Power(t1,3)*Power(t2,2) + 
            13*Power(t1,4)*Power(t2,2) - 5*Power(t1,5)*Power(t2,2) + 
            Power(t1,6)*Power(t2,2) + 2*Power(t2,3) - 7*t1*Power(t2,3) + 
            5*Power(t1,2)*Power(t2,3) + 23*Power(t1,3)*Power(t2,3) + 
            7*Power(t1,4)*Power(t2,3) - 
            Power(s2,4)*(-1 + t1)*
             (t1*(8 + 18*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*(-4 + Power(t2,2)) - 
               2*(2 + 9*t2 + 8*Power(t2,2))) + 
            Power(s2,3)*(Power(t1,5)*(-1 + t2) + 
               Power(t1,3)*(57 + 33*t2 - 29*Power(t2,2)) + 
               Power(t1,4)*(-17 + 5*t2 + 3*Power(t2,2)) - 
               t2*(13 + 28*t2 + 4*Power(t2,2)) + 
               t1*(20 + 72*t2 + 38*Power(t2,2) - 36*Power(t2,3)) + 
               Power(t1,2)*(-59 - 98*t2 + 16*Power(t2,2) + 
                  10*Power(t2,3))) + 
            Power(s2,2)*(-3 + Power(t1,6) - 8*t2 - 10*Power(t2,2) - 
               Power(t1,5)*(-18 + 11*t2 + Power(t2,2)) + 
               Power(t1,4)*(-63 + 3*t2 + 16*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(t1,3)*(74 + 44*t2 - 51*Power(t2,2) + 
                  3*Power(t2,3)) + 
               3*t1*(4 + 11*t2 + 13*Power(t2,2) + 15*Power(t2,3)) + 
               Power(t1,2)*(-39 - 61*t2 + 7*Power(t2,2) + 
                  45*Power(t2,3))) - 
            s2*(Power(t1,7)*(1 + t2) + 
               Power(t1,5)*(13 - 8*t2 - 4*Power(t2,2)) + 
               t2*(-7 + 9*t2 - 2*Power(t2,2)) - 
               Power(t1,6)*(2 + Power(t2,2)) + 
               t1*(-21 + 13*t2 - 36*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,4)*(-16 - 3*t2 - 10*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,3)*(-25 + 24*t2 + 9*Power(t2,2) + 
                  26*Power(t2,3)) + 
               Power(t1,2)*(50 - 20*t2 + 33*Power(t2,2) + 
                  60*Power(t2,3))) + 
            Power(s1,2)*(s2 - t1)*
             (Power(s2,3)*(-4 - 13*t1 + 16*Power(t1,2) + Power(t1,3)) - 
               Power(s2,2)*(3*t2 + Power(t1,3)*(-5 + 2*t2) + 
                  Power(t1,2)*(-7 + 27*t2) + 2*t1*(6 + 29*t2)) + 
               s2*(-2*t2 + Power(t1,4)*(-32 + 7*t2) + 
                  15*Power(t1,2)*(10 + 7*t2) + t1*(-79 + 29*t2) + 
                  Power(t1,3)*(-39 + 41*t2)) - 
               t1*(1 - 6*Power(t1,4) + Power(t1,5) - 3*t2 + 
                  Power(t1,3)*(-45 + 14*t2) + t1*(-71 + 18*t2) + 
                  Power(t1,2)*(120 + 61*t2))) + 
            s1*(2*Power(s2,4)*(-1 + t1)*
                (-7 + 2*Power(t1,2) + t1*(5 - 12*t2) - 10*t2) + 
               Power(s2,2)*(-18*Power(t1,5) + Power(t1,6) + 
                  (22 - 3*t2)*t2 + 
                  Power(t1,2)*(33 - 189*t2 - 166*Power(t2,2)) + 
                  t1*(1 + 23*t2 - 68*Power(t2,2)) + 
                  Power(t1,3)*(-85 + 139*t2 - 35*Power(t2,2)) + 
                  Power(t1,4)*(68 + 5*t2 + 2*Power(t2,2))) + 
               t1*(-6 + Power(t1,7) + 13*t2 - 7*Power(t2,2) - 
                  Power(t1,6)*(5 + t2) + Power(t1,5)*(-2 + 11*t2) + 
                  Power(t1,2)*(47 + 28*t2 - 55*Power(t2,2)) + 
                  Power(t1,4)*(62 - 6*t2 - 3*Power(t2,2)) + 
                  t1*(9 - 12*t2 + 22*Power(t2,2)) - 
                  Power(t1,3)*(106 + 33*t2 + 47*Power(t2,2))) + 
               Power(s2,3)*(5 + 3*Power(t1,4)*(-1 + t2) + 22*t2 + 
                  5*Power(t2,2) + Power(t1,3)*(-37 + 31*t2) + 
                  Power(t1,2)*(88 - 39*t2 - 2*Power(t2,2)) + 
                  t1*(-53 - 17*t2 + 87*Power(t2,2))) + 
               s2*(1 - 2*Power(t1,7) - 2*Power(t1,6)*(-11 + t2) - 3*t2 + 
                  2*Power(t2,2) + t1*(4 - 36*t2 - 6*Power(t2,2)) + 
                  Power(t1,5)*(-32 - 29*t2 + Power(t2,2)) + 
                  Power(t1,4)*(-48 - 84*t2 + 33*Power(t2,2)) + 
                  Power(t1,2)*(-65 - 49*t2 + 106*Power(t2,2)) + 
                  Power(t1,3)*(120 + 203*t2 + 134*Power(t2,2))))) - 
         Power(s,3)*(-1 + t1)*
          ((11 + s1)*Power(t1,8) - Power(t1,9) + 3*(-1 + t2)*t2 + 
            Power(s2,4)*Power(-1 + t1,3)*
             (8 + s1*(-1 + t1) - (7 + t1)*t2) + 
            Power(t1,7)*(-20 + 2*Power(s1,2) + t2 - Power(t2,2) - 
               s1*(3 + t2)) + 
            Power(t1,3)*(63 + 10*Power(s1,3) + 
               3*Power(s1,2)*(-34 + t2) + 22*t2 + 44*Power(t2,2) + 
               24*Power(t2,3) + s1*(-2 + 19*t2 - 31*Power(t2,2))) + 
            t1*(-3 - 10*t2 + 8*Power(t2,2) + 5*Power(t2,3) + 
               s1*(2 + 8*t2 - 9*Power(t2,2))) - 
            Power(t1,6)*(-7 + Power(s1,3) - 2*Power(s1,2)*(-2 + t2) + 
               41*t2 - 5*Power(t2,2) + s1*(9 - 20*t2 + Power(t2,2))) + 
            Power(t1,5)*(29 - 6*Power(s1,3) + 95*t2 - 17*Power(t2,2) + 
               3*Power(t2,3) + Power(s1,2)*(-104 + 23*t2) - 
               7*s1*(-9 - 4*t2 + 2*Power(t2,2))) + 
            Power(t1,2)*(-16 + 18*t2 - 51*Power(t2,2) - 15*Power(t2,3) - 
               Power(s1,2)*(8 + 3*t2) + s1*(18 + 23*t2 + 24*Power(t2,2))) \
- Power(t1,4)*(70 + 37*Power(s1,3) + 82*t2 - 9*Power(t2,2) - 
               17*Power(t2,3) - Power(s1,2)*(216 + 77*t2) + 
               s1*(70 + 97*t2 + 71*Power(t2,2))) - 
            Power(s2,3)*(-1 + t1)*
             (-7 + Power(s1,2)*
                (1 + 6*t1 + 40*Power(t1,2) + Power(t1,3)) - 12*t2 - 
               18*Power(t2,2) - Power(t1,4)*(1 + 7*t2) + 
               Power(t1,3)*(50 + 3*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(-104 - 55*t2 + 9*Power(t2,2)) + 
               t1*(62 + 71*t2 + 61*Power(t2,2)) + 
               s1*(3 + Power(t1,2)*(63 - 43*t2) + 
                  Power(t1,3)*(-1 + t2) + 19*t2 - t1*(65 + 73*t2))) + 
            Power(s2,2)*(3 - Power(s1,3)*t1*
                (1 + 6*t1 + 25*Power(t1,2) + 2*Power(t1,3)) + 
               Power(t1,6)*(4 - 7*t2) - 13*Power(t2,2) - 
               17*Power(t2,3) + 
               Power(t1,5)*(94 + 3*t2 - 15*Power(t2,2)) + 
               Power(t1,4)*(-330 - 150*t2 + 61*Power(t2,2)) + 
               Power(t1,3)*(383 + 402*t2 + 9*Power(t2,2) - 
                  10*Power(t2,3)) + 
               t1*(15 + 83*t2 + 118*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(-169 - 331*t2 - 160*Power(t2,2) + 
                  59*Power(t2,3)) + 
               Power(s1,2)*(5*Power(t1,5) - 5*t2 + 
                  Power(t1,4)*(33 + 4*t2) - t1*(8 + 19*t2) + 
                  Power(t1,3)*(-95 + 41*t2) + Power(t1,2)*(65 + 81*t2)) \
+ s1*(1 + Power(t1,6) + Power(t1,4)*(158 - 62*t2) + 4*t2 + 
                  19*Power(t2,2) + Power(t1,5)*(-11 + 4*t2) + 
                  Power(t1,2)*(204 + 116*t2 - 137*Power(t2,2)) + 
                  Power(t1,3)*(-319 + 35*t2 - 9*Power(t2,2)) + 
                  t1*(-34 - 97*t2 + 25*Power(t2,2)))) + 
            s2*(Power(t1,8) + Power(t1,7)*(-17 - 3*s1 + 2*t2) + 
               Power(t1,6)*(-30 - 6*Power(s1,2) + s1*(17 - 2*t2) + 
                  16*t2 + 5*Power(t2,2)) + 
               t2*(3 + 4*t2 - 7*Power(t2,2) + s1*(-5 + 4*t2)) + 
               Power(t1,5)*(215 + 3*Power(s1,3) + 
                  Power(s1,2)*(11 - 7*t2) + 70*t2 - 33*Power(t2,2) + 
                  3*Power(t2,3) + s1*(-97 + 9*t2 - 4*Power(t2,2))) + 
               Power(t1,2)*(-30 - 9*Power(s1,3) - 145*t2 - 
                  127*Power(t2,2) - 32*Power(t2,3) + 
                  5*Power(s1,2)*(23 + 2*t2) + 
                  s1*(31 + 61*t2 + 6*Power(t2,2))) + 
               Power(t1,4)*(-309 + 31*Power(s1,3) + 
                  Power(s1,2)*(163 - 62*t2) - 298*t2 + 58*Power(t2,2) - 
                  5*Power(t2,3) + 2*s1*(86 - 77*t2 + 22*Power(t2,2))) + 
               t1*(-1 + 17*t2 + 40*Power(t2,2) + 35*Power(t2,3) + 
                  Power(s1,2)*(5 + 11*t2) - 
                  s1*(7 + 52*t2 + 30*Power(t2,2))) + 
               Power(t1,3)*(171 + 43*Power(s1,3) + 335*t2 + 
                  53*Power(t2,2) - 62*Power(t2,3) - 
                  12*Power(s1,2)*(24 + 13*t2) + 
                  s1*(-113 + 143*t2 + 184*Power(t2,2))))))*T4q(t1))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*Power(-1 + t1,2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*(-1 + t2)) + 
    (8*(Power(s,7)*Power(t1,2)*(t1 + t2) + 
         Power(s,6)*t1*(-(Power(t1,2)*(6 + 2*s1 + 3*s2 - 7*t2)) + 
            2*s2*t2 + t1*(1 + s2*(2 + s1 - 4*t2) - (6 + s1)*t2 + 
               Power(t2,2))) + 
         Power(s,5)*(Power(t1,5) + t1*(s1 - t2)*Power(t2,2) + 
            Power(t2,3) + Power(t1,4)*(-6 + 2*s1 + 5*t2) - 
            Power(t1,2)*(5 + s1 - 11*t2 - 3*s1*t2 + 2*Power(t2,2)) + 
            Power(t1,3)*(9 + Power(s1,2) + s1*(11 - 7*t2) - 27*t2 + 
               6*Power(t2,2)) + 
            Power(s2,2)*(3*Power(t1,3) + t1*(1 + 2*s1 - 6*t2) + t2 + 
               Power(t1,2)*(-4 - 3*s1 + 6*t2)) + 
            s2*t1*(2 + Power(t1,2)*(12 + 4*s1 - 21*t2) - 2*(5 + s1)*t2 + 
               Power(t2,2) - t1*
                (13 + Power(s1,2) - 3*s1*(-3 + t2) - 36*t2 + 
                  4*Power(t2,2)))) + 
         Power(s,4)*(2*Power(t1,6) + Power(t1,5)*(s1 + 2*(-8 + t2)) + 
            (3 - 2*t2)*Power(t2,2) + 
            Power(t1,4)*(18 - 10*s1 - 3*Power(s1,2) + t2 + 
               3*Power(t2,2)) - 
            Power(s2,3)*(-1 + t1)*
             (s1 - 3*s1*t1 + Power(t1,2) - 2*t2 + t1*(-1 + 4*t2)) + 
            t1*t2*(3 + 8*t2 + 6*Power(t2,2) - s1*(1 + 5*t2)) - 
            Power(t1,2)*(-8 + 3*t2 + Power(t2,2) + Power(t2,3) + 
               Power(s1,2)*(2 + t2) - s1*(1 + 9*t2)) + 
            Power(t1,3)*(-2 + Power(s1,2)*(-7 + t2) + 4*t2 - 
               12*Power(t2,2) + Power(t2,3) + 
               s1*(-14 + 23*t2 - 2*Power(t2,2))) - 
            Power(s2,2)*(-1 + Power(t1,3)*(7 + 2*s1 - 21*t2) + 
               (4 + s1)*t2 + t1*
                (8 + 2*Power(s1,2) - 2*s1*(-5 + t2) - 39*t2 + 
                  Power(t2,2)) - 
               Power(t1,2)*(16 + Power(s1,2) - 3*s1*(-4 + t2) - 56*t2 + 
                  6*Power(t2,2))) - 
            s2*(Power(t1,5) + Power(t2,2)*(3 - 4*s1 + 7*t2) + 
               Power(t1,4)*(-15 + s1 + 11*t2) + 
               Power(t1,3)*(16 - 3*Power(s1,2) - 13*s1*(-1 + t2) - 
                  53*t2 + 17*Power(t2,2)) + 
               t1*(8 - 7*t2 - 2*Power(s1,2)*t2 - 5*Power(t2,3) + 
                  s1*(2 - 5*t2 + 5*Power(t2,2))) - 
               Power(t1,2)*(s1*(30 - 23*t2) + 2*Power(s1,2)*(3 + t2) + 
                  2*(6 - 31*t2 + 6*Power(t2,2))))) - 
         (s2 - t1)*(-1 + t1)*(2*(-1 + s1)*Power(t1,5)*(1 + s1 - 2*t2) + 
            Power(s2,4)*Power(s1 - t2,3) - 
            3*(-1 + s1)*t1*Power(-1 + t2,2) + Power(-1 + t2,3) - 
            Power(t1,2)*(-1 + t2)*
             (-1 + 7*Power(s1,2) + 24*t2 + 6*Power(t2,2) - 
               4*s1*(5 + 4*t2)) + 
            Power(t1,3)*(-5 + Power(s1,3) - 40*t2 + 42*Power(t2,2) + 
               4*Power(t2,3) + Power(s1,2)*(-9 + 8*t2) + 
               s1*(49 - 36*t2 - 14*Power(t2,2))) + 
            Power(t1,4)*(6 + 14*t2 - 4*Power(s1,2)*t2 - 24*Power(t2,2) + 
               s1*(-26 + 30*t2 + 4*Power(t2,2))) - 
            Power(s2,3)*(s1 - t2)*
             (-6 + Power(s1,2)*(1 + 3*t1) + t1*(12 - 5*t2) + t2 + 
               4*Power(t2,2) + Power(t1,2)*(-6 + 4*t2) + 
               s1*(5 + 2*Power(t1,2) - 5*t2 - t1*(7 + 3*t2))) + 
            s2*(-(Power(s1,3)*Power(t1,2)*(3 + t1)) - 
               Power(-1 + t2,2)*(-1 + 4*t2) + 
               Power(t1,2)*(15 + 68*t2 - 86*Power(t2,2)) + 
               2*Power(t1,4)*(2 - 7*t2 + 2*Power(t2,2)) - 
               Power(t1,3)*(13 + 16*t2 - 46*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-7 - 32*t2 + 27*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-6*Power(t1,3) + t1*(13 - 10*t2) + 14*(-1 + t2) + 
                  Power(t1,2)*(7 + 8*t2)) + 
               s1*(3*Power(-1 + t2,2) + 6*Power(t1,4)*(1 + t2) + 
                  t1*(49 - 26*t2 - 23*Power(t2,2)) + 
                  Power(t1,3)*(43 - 68*t2 - 2*Power(t2,2)) + 
                  Power(t1,2)*(-101 + 94*t2 + 10*Power(t2,2)))) + 
            Power(s2,2)*(2 + 3*Power(s1,3)*t1*(1 + t1) + 25*t2 - 
               27*Power(t2,2) - 
               2*Power(t1,3)*(1 - 8*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(6 - 7*t2 - 20*Power(t2,2) + 6*Power(t2,3)) - 
               t1*(6 + 34*t2 - 55*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s1,2)*(7 + t1 + 6*Power(t1,3) - 7*t2 - 4*t1*t2 - 
                  7*Power(t1,2)*(2 + t2)) + 
               s1*(-29 - 12*Power(t1,3) + 22*t2 + 7*Power(t2,2) + 
                  Power(t1,2)*(-5 + 40*t2 - 2*Power(t2,2)) + 
                  t1*(46 - 62*t2 + 13*Power(t2,2))))) + 
         Power(s,3)*(-((11 + s1)*Power(t1,6)) + Power(t1,7) - 
            Power(s2,4)*Power(-1 + t1,2)*(s1 - t2) - 3*(-1 + t2)*t2 + 
            Power(t1,5)*(25 - 2*Power(s1,2) + s1*(-5 + t2) + 15*t2 + 
               Power(t2,2)) + 
            Power(t1,4)*(-58 + Power(s1,3) - 2*Power(s1,2)*(-7 + t2) - 
               57*t2 + 5*Power(t2,2) + s1*(12 - 16*t2 + Power(t2,2))) + 
            Power(t1,3)*(18 + 2*Power(s1,3) + Power(s1,2)*(14 - 13*t2) + 
               126*t2 - 34*Power(t2,2) + 3*Power(t2,3) + 
               s1*(-96 + 47*t2 + 2*Power(t2,2))) + 
            Power(t1,2)*(22 + 11*t2 + 26*Power(t2,2) + 5*Power(t2,3) + 
               Power(s1,2)*(8 + 3*t2) - s1*(22 + 39*t2 + 6*Power(t2,2))) \
+ t1*(3 + 16*t2 - 14*Power(t2,2) - 5*Power(t2,3) + 
               s1*(-2 - 8*t2 + 9*Power(t2,2))) + 
            Power(s2,3)*(-1 + Power(s1,2)*(-1 - t1 + Power(t1,2)) + 
               Power(t1,3)*(1 - 7*t2) + 12*t2 + 2*Power(t2,2) + 
               Power(t1,2)*(-5 + 28*t2 - 4*Power(t2,2)) + 
               t1*(5 - 37*t2 - 3*Power(t2,2)) + 
               s1*(-3 + Power(t1,2)*(-1 + t2) - t2 + t1*(8 + 6*t2))) + 
            Power(s2,2)*(-3 + Power(s1,3)*t1*(1 + 2*t1) + 
               13*Power(t2,2) + 17*Power(t2,3) + 
               5*Power(t1,3)*t2*(-5 + 3*t2) + 
               Power(s1,2)*(-5*Power(t1,3) + Power(t1,2)*(11 - 4*t2) - 
                  t1*(-8 + t2) + 5*t2) + Power(t1,4)*(-10 + 7*t2) + 
               Power(t1,2)*(10 + 39*t2 - 7*Power(t2,2)) - 
               t1*(-3 + 5*t2 + 2*Power(t2,2) + 10*Power(t2,3)) - 
               s1*(1 + Power(t1,4) + Power(t1,2)*(37 - 6*t2) + 4*t2 + 
                  19*Power(t2,2) + Power(t1,3)*(-9 + 4*t2) + 
                  t1*(-14 + 31*t2 - 9*Power(t2,2)))) - 
            s2*(Power(t1,6) + Power(t1,5)*(-21 - 3*s1 + 2*t2) + 
               Power(t1,4)*(19 - 6*Power(s1,2) + s1*(5 - 2*t2) + 24*t2 + 
                  5*Power(t2,2)) + 
               t2*(3 + 4*t2 - 7*Power(t2,2) + s1*(-5 + 4*t2)) + 
               Power(t1,3)*(-20 + 3*Power(s1,3) + 
                  Power(s1,2)*(25 - 7*t2) - 82*t2 - 3*Power(t2,2) + 
                  3*Power(t2,3) + s1*(-30 + 13*t2 - 4*Power(t2,2))) + 
               Power(t1,2)*(22 + 3*Power(s1,3) + 
                  Power(s1,2)*(11 - 4*t2) + 96*t2 + Power(t2,2) - 
                  7*Power(t2,3) + s1*(-31 - 34*t2 + 6*Power(t2,2))) + 
               t1*(-1 + 23*t2 + 48*Power(t2,2) + 21*Power(t2,3) + 
                  Power(s1,2)*(5 + 11*t2) - 
                  s1*(7 + 62*t2 + 22*Power(t2,2))))) + 
         s*(2*Power(t1,6)*(-5 + 3*Power(s1,2) + s1*(4 - 6*t2) - 6*t2) - 
            2*Power(t1,7)*(1 + s1 - 2*t2) - 
            2*Power(s2,5)*(-1 + t1)*Power(s1 - t2,2) - Power(-1 + t2,3) + 
            2*t1*Power(-1 + t2,2)*(-5 + s1 + 3*t2) + 
            Power(t1,2)*(-1 + t2)*
             (-19 + Power(s1,2) + s1*(3 - 12*t2) + 26*t2 - 5*Power(t2,2)) \
- Power(t1,5)*(-47 + Power(s1,3) - 85*t2 + 74*Power(t2,2) - 
               4*Power(t2,3) + Power(s1,2)*(3 + 11*t2) + 
               s1*(103 - 110*t2 - 6*Power(t2,2))) + 
            Power(t1,3)*(9 + 2*Power(s1,3) + Power(s1,2)*(5 - 20*t2) + 
               97*t2 - 88*Power(t2,2) - Power(t2,3) + 
               s1*(-93 + 80*t2 + 29*Power(t2,2))) - 
            Power(t1,4)*(54 + 4*Power(s1,3) + Power(s1,2)*(7 - 39*t2) + 
               152*t2 - 150*Power(t2,2) + 
               s1*(-191 + 189*t2 + 34*Power(t2,2))) - 
            Power(s2,4)*(-4 + Power(s1,3)*(1 + 2*t1) + 2*t2 - 
               6*Power(t2,2) - 8*Power(t2,3) - 
               Power(s1,2)*(-2 + t1 + Power(t1,2) + 10*t2 - t1*t2) + 
               Power(t1,2)*(-12 + 4*t2 - 11*Power(t2,2)) + 
               Power(t1,3)*(4 - t2 + Power(t2,2)) + 
               t1*(12 - 5*t2 + 16*Power(t2,2) + 5*Power(t2,3)) + 
               s1*(-2 - Power(t1,3)*(-1 + t2) + 4*t2 + 17*Power(t2,2) + 
                  4*Power(t1,2)*(-1 + 3*t2) + 
                  t1*(5 - 15*t2 - 8*Power(t2,2)))) + 
            Power(s2,2)*(1 - 3*Power(s1,3)*t1*
                (-2 + 5*t1 + 3*Power(t1,2)) - 3*t2 + Power(t2,2) + 
               Power(t2,3) + Power(t1,5)*(-17 + 11*t2) + 
               Power(t1,4)*(15 - 34*t2 - 6*Power(t2,2)) + 
               Power(t1,3)*(56 + 162*t2 - 117*Power(t2,2) + 
                  17*Power(t2,3)) - 
               Power(t1,2)*(88 + 269*t2 - 264*Power(t2,2) + 
                  27*Power(t2,3)) + 
               t1*(33 + 133*t2 - 142*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s1,2)*(-1 + Power(t1,4)*(7 - 2*t2) + t2 - 
                  5*t1*(7 + 2*t2) + 2*Power(t1,3)*(-20 + 7*t2) + 
                  Power(t1,2)*(69 + 51*t2)) + 
               s1*(10 + Power(t1,5)*(-7 + t2) - 11*t2 + Power(t2,2) + 
                  2*Power(t1,4)*(-5 + 3*t2) + 
                  Power(t1,3)*(-61 + 141*t2 - 19*Power(t2,2)) + 
                  Power(t1,2)*(190 - 337*t2 - 6*Power(t2,2)) - 
                  2*t1*(61 - 100*t2 + 15*Power(t2,2)))) + 
            Power(s2,3)*(-8 + Power(s1,3)*(-2 + 7*t1 + 7*Power(t1,2)) - 
               21*t2 + 21*Power(t2,2) - 9*Power(t2,3) + 
               Power(t1,4)*(13 - 4*t2 + Power(t2,2)) + 
               t1*(11 + 51*t2 - 45*Power(t2,2) - 9*Power(t2,3)) - 
               Power(t1,3)*(31 - 17*t2 + 8*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(15 - 43*t2 + 31*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(s1,2)*(Power(t1,2)*(13 - 11*t2) - 5*(-4 + t2) + 
                  Power(t1,3)*(2 + t2) - 7*t1*(5 + 3*t2)) + 
               s1*(1 - 2*Power(t1,4)*(-2 + t2) - 33*t2 + 
                  16*Power(t2,2) + Power(t1,3)*(3 + t2) - 
                  Power(t1,2)*(17 + 23*t2 + 3*Power(t2,2)) + 
                  t1*(9 + 57*t2 + 23*Power(t2,2)))) + 
            s2*(Power(s1,3)*Power(t1,2)*(-6 + 13*t1 + 5*Power(t1,2)) - 
               Power(-1 + t2,2)*(-7 + 3*t2) - 2*Power(t1,6)*(-5 + 6*t2) + 
               Power(t1,5)*(15 + 30*t2 + 8*Power(t2,2)) + 
               t1*(-17 + 39*t2 - 23*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(36 + 203*t2 - 203*Power(t2,2) + 
                  15*Power(t2,3)) - 
               Power(t1,4)*(109 + 200*t2 - 165*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(t1,3)*(130 + 363*t2 - 366*Power(t2,2) + 
                  25*Power(t2,3)) + 
               Power(s1,2)*t1*
                (2 + Power(t1,4)*(-14 + t2) - 2*t2 + 
                  9*Power(t1,3)*(3 + t2) + 5*t1*(2 + 7*t2) - 
                  Power(t1,2)*(25 + 79*t2)) + 
               s1*(6*Power(t1,6) - 2*Power(-1 + t2,2) + 
                  Power(t1,5)*(-5 + 13*t2) + 
                  Power(t1,2)*(214 - 247*t2 - 15*Power(t2,2)) + 
                  Power(t1,4)*(186 - 239*t2 + 8*Power(t2,2)) + 
                  t1*(-7 - 4*t2 + 11*Power(t2,2)) + 
                  Power(t1,3)*(-392 + 473*t2 + 34*Power(t2,2))))) - 
         Power(s,2)*(-1 - 10*t1 + 32*Power(t1,2) + 22*Power(t1,3) - 
            95*Power(t1,4) + 53*Power(t1,5) - 2*Power(t1,6) + 
            Power(t1,7) + Power(s1,3)*Power(s2 - t1,2)*
             (t1*(6 + t1) + s2*(-2 - 3*t1 + 2*Power(t1,2))) + 30*t1*t2 - 
            42*Power(t1,2)*t2 + 144*Power(t1,3)*t2 - 169*Power(t1,4)*t2 + 
            49*Power(t1,5)*t2 - 12*Power(t1,6)*t2 + 3*Power(t2,2) - 
            25*t1*Power(t2,2) + 31*Power(t1,2)*Power(t2,2) - 
            127*Power(t1,3)*Power(t2,2) + 88*Power(t1,4)*Power(t2,2) - 
            3*Power(t1,5)*Power(t2,2) - 2*Power(t2,3) + 5*t1*Power(t2,3) + 
            5*Power(t1,3)*Power(t2,3) - 10*Power(t1,4)*Power(t2,3) + 
            Power(s2,4)*t2*(6 - Power(t1,2)*(-2 + t2) + 4*t2 - 
               t1*(8 + 5*t2)) + 
            Power(s2,2)*(3 + Power(t1,5) + 8*t2 + 10*Power(t2,2) - 
               Power(t1,4)*(-25 + 23*t2 + Power(t2,2)) + 
               t1*(21 + 95*t2 - 83*Power(t2,2) - 27*Power(t2,3)) + 
               6*Power(t1,2)*
                (-4 - 34*t2 + 7*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,3)*(26 - 124*t2 + 21*Power(t2,2) + 3*Power(t2,3))) \
+ Power(s2,3)*(Power(t1,4)*(-1 + t2) + 
               Power(t1,3)*(-14 + 2*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(31 - 13*t2 + 14*Power(t2,2)) + 
               t2*(-13 + 14*t2 + 18*Power(t2,2)) - 
               t1*(16 - 23*t2 + 18*Power(t2,2) + 10*Power(t2,3))) - 
            s2*(Power(t1,6)*(1 + t2) + 
               Power(t1,2)*(37 + 242*t2 - 210*Power(t2,2)) + 
               Power(t1,4)*(62 + 147*t2 - 17*Power(t2,2)) - 
               Power(t1,5)*(-7 + 27*t2 + Power(t2,2)) + 
               t2*(7 - 9*t2 + 2*Power(t2,2)) + 
               3*t1*(7 - 2*t2 + 9*Power(t2,2)) - 
               Power(t1,3)*(128 + 364*t2 - 135*Power(t2,2) + 
                  14*Power(t2,3))) + 
            Power(s1,2)*(s2 - t1)*
             (Power(s2,3)*(2 - 5*t1 + Power(t1,2)) + 
               Power(s2,2)*(Power(t1,2)*(11 - 2*t2) + 13*t2 + 3*t1*t2) + 
               s2*(Power(t1,2)*(19 - 16*t2) + 2*t2 + 
                  Power(t1,3)*(-26 + 7*t2) - t1*(35 + 23*t2)) + 
               t1*(1 + 15*Power(t1,3) - Power(t1,4) + 
                  Power(t1,2)*(4 - 12*t2) - 3*t2 + t1*(14 + 31*t2))) + 
            s1*(2*Power(s2,4)*(-1 + Power(t1,2) - 3*t2 + 5*t1*t2) + 
               Power(s2,3)*(1 - 29*Power(t2,2) + Power(t1,3)*(5 + 3*t2) - 
                  2*Power(t1,2)*(8 + 13*t2) + 
                  t1*(10 - 3*t2 + 10*Power(t2,2))) - 
               s2*(1 + 2*Power(t1,6) + 2*Power(t1,5)*(-8 + t2) - 3*t2 + 
                  2*Power(t2,2) + t1*(5 - 39*t2 - 4*Power(t2,2)) + 
                  Power(t1,4)*(14 + 29*t2 - Power(t2,2)) + 
                  Power(t1,3)*(174 - 135*t2 + 14*Power(t2,2)) + 
                  2*Power(t1,2)*(-90 + 148*t2 + 15*Power(t2,2))) + 
               t1*(6 + Power(t1,6) - 13*t2 - Power(t1,5)*t2 + 
                  7*Power(t2,2) + 2*Power(t1,4)*(-6 + 11*t2) + 
                  Power(t1,3)*(154 - 130*t2 + 3*Power(t2,2)) - 
                  t1*(3 + t2 + 15*Power(t2,2)) + 
                  Power(t1,2)*(-146 + 189*t2 + 16*Power(t2,2))) + 
               Power(s2,2)*(-23*Power(t1,4) + Power(t1,5) + 
                  t2*(-22 + 3*t2) + 
                  Power(t1,2)*(8 + 12*t2 - 3*Power(t2,2)) + 
                  Power(t1,3)*(45 + 17*t2 + 2*Power(t2,2)) + 
                  t1*(-31 + 99*t2 + 47*Power(t2,2))))))*T5q(s))/
     (s*(-1 + s1)*(-1 + s2)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*
       (-1 + t2)));
   return a;
};
