#include<stdio.h>
#include<math.h>


long double p_m123(double *);
long double p_m132(double *);
long double p_m213(double *);
long double p_m231(double *);
long double p_m312(double *);
long double p_m321(double *);


double pent(double * vars)
{
    double a;
    long double p123, p312, p231, p132, p213, p321;
    p123=p_m123(vars);
    p132=p_m132(vars);
    p213=p_m213(vars);
    p231=p_m231(vars);
    p312=p_m312(vars);
    p321=p_m321(vars);
    
    a=p123+p132+p213+p231+p312+p321;
    
    
    if( isnanl(a)!=0 )
    { 
        printf("pentagon function is NAN\n");
	//printf("pent function:t1-t2=%e  s1-s2+t1-t2=%e\n",t1-t2,s1-s2+t1-t2);
        //printf("s=%0.6f s1=%0.6f  s2=%0.6f  t1=%0.6f  t2=%0.6f \n ",s,s1,s2,t1,t2);
        // printf("m123=%e m231=%e m312=%e m132=%e m213=%e m321=%e \n",p123,p231,p312,p132,p213,p321);
        // printf("u1=%e  u2=%e\n",-(s+t2-s1-1),-(s+t1-s2-1));
	//printf("\n");
    };


    return a;
};
