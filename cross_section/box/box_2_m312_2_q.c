#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m312_2_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(-(Power(s2,9)*(s1 - t2)*
            (-((-1 + t1)*(-6 + 5*s1 + 6*t1 - 5*t2)*(s1 - t2)) - 
              2*Power(s,2)*(-3 + s1 + 3*t1 - t2) + 
              s*(6 + Power(s1,2) + 6*Power(t1,2) + 
                 2*s1*(-5 + 5*t1 - t2) + 10*t2 + Power(t2,2) - 
                 2*t1*(6 + 5*t2)))) + 
         t1*Power(-1 + s + t1,3)*(-1 + s1 + t1 - t2)*
          (Power(s,4)*(2 + t1 - t2) + 
            Power(s,3)*(1 + t1 + 5*t2 + 2*t1*t2 - 2*Power(t2,2) + 
               s1*(-6 - 3*t1 + 2*t2)) + 
            Power(s,2)*(-10 + 2*Power(t1,2) + 
               Power(s1,2)*(6 + 3*t1 - t2) - 3*t2 + 10*Power(t2,2) - 
               Power(t2,3) + t1*(5 - 8*t2 + Power(t2,2)) + 
               2*s1*(4 + t1 - 8*t2 - 2*t1*t2 + Power(t2,2))) + 
            2*(-3 + Power(s1,3) - 4*Power(t1,3) + 
               Power(s1,2)*(3 + t1 - Power(t1,2) - 3*t2) + t2 + 
               3*Power(t2,2) - Power(t2,3) + 
               Power(t1,2)*(7 + 2*t2 - Power(t2,2)) + 
               t1*(1 - 6*t2 + Power(t2,2)) + 
               s1*(-1 - 2*t1*(-3 + t2) + 2*Power(t1,2)*(-1 + t2) - 
                  6*t2 + 3*Power(t2,2))) - 
            s*(-13 + Power(s1,3)*(2 + t1) + 
               Power(s1,2)*(11 + t1*(3 - 2*t2) - 7*t2) + 3*t2 + 
               13*Power(t2,2) - 3*Power(t2,3) + 4*Power(t1,2)*(2 + t2) + 
               t1*(5 - 18*t2 + Power(t2,2)) + 
               s1*(8*(-3 + t2)*t2 + t1*(11 - 4*t2 + Power(t2,2))))) + 
         Power(s2,8)*(Power(s,3)*
             (2 - 7*Power(s1,2) + 6*Power(t1,2) - 8*t2 - 
               14*Power(t2,2) + 3*t1*(-4 + 7*t2) + 
               s1*(18 - 23*t1 + 17*t2)) + 
            (-1 + t1)*(s1 - t2)*
             (22 + 10*Power(s1,3) + Power(s1,2)*(-28 + 7*t1 - 30*t2) + 
               14*t2 - 32*Power(t2,2) - 10*Power(t2,3) + 
               2*Power(t1,2)*(13 + 6*t2) - 
               2*s1*(2 + 5*Power(t1,2) + 7*t1*(-1 + t2) - 28*t2 - 
                  15*Power(t2,2)) + t1*(-48 - 26*t2 + 11*Power(t2,2))) + 
            Power(s,2)*(-6 + 8*Power(s1,3) + 6*Power(t1,3) + 
               Power(s1,2)*(-52 + 50*t1 - 24*t2) - 69*t2 - 
               28*Power(t2,2) - 12*Power(t2,3) - 
               Power(t1,2)*(26 + 15*t2) + 
               t1*(26 + 80*t2 + 34*Power(t2,2)) + 
               s1*(49 + 11*Power(t1,2) + 88*t2 + 28*Power(t2,2) - 
                  4*t1*(14 + 23*t2))) + 
            s*(16 - 2*Power(s1,4) + t2 - 104*Power(t2,2) - 
               34*Power(t2,3) - 2*Power(t2,4) - 
               2*Power(t1,3)*(10 + 3*t2) + 
               Power(s1,3)*(42 - 39*t1 + 8*t2) + 
               Power(t1,2)*(56 + 5*t2 - 36*Power(t2,2)) + 
               t1*(-52 + 140*Power(t2,2) + 31*Power(t2,3)) - 
               Power(s1,2)*(77 + 25*Power(t1,2) + 126*t2 + 
                  12*Power(t2,2) - 3*t1*(34 + 39*t2)) + 
               s1*(9 + 4*Power(t1,3) + 177*t2 + 118*Power(t2,2) + 
                  8*Power(t2,3) + Power(t1,2)*(9 + 57*t2) - 
                  t1*(22 + 234*t2 + 109*Power(t2,2))))) + 
         Power(s2,7)*(16 + 5*Power(s1,5)*(-1 + t1) - 68*t1 + 
            108*Power(t1,2) - 76*Power(t1,3) + 20*Power(t1,4) + 
            Power(s1,4)*(-1 + t1)*(-16 + 4*t1 - 25*t2) - 25*t2 + 
            53*t1*t2 + 7*Power(t1,2)*t2 - 73*Power(t1,3)*t2 + 
            38*Power(t1,4)*t2 - 185*Power(t2,2) + 409*t1*Power(t2,2) - 
            253*Power(t1,2)*Power(t2,2) + 19*Power(t1,3)*Power(t2,2) + 
            10*Power(t1,4)*Power(t2,2) + 5*Power(t2,3) + 
            39*t1*Power(t2,3) - 64*Power(t1,2)*Power(t2,3) + 
            20*Power(t1,3)*Power(t2,3) + 20*Power(t2,4) - 
            28*t1*Power(t2,4) + 8*Power(t1,2)*Power(t2,4) + 
            5*Power(t2,5) - 5*t1*Power(t2,5) + 
            Power(s,4)*(-20 + 9*Power(s1,2) + 64*t1 - 25*Power(t1,2) + 
               s1*(-8 + 28*t1 - 30*t2) - 24*t2 - 34*t1*t2 + 
               40*Power(t2,2)) - 
            Power(s1,3)*(-1 + t1)*
             (45 + 15*Power(t1,2) - 61*t2 - 50*Power(t2,2) + 
               t1*(-89 + 13*t2)) + 
            Power(s1,2)*(-1 + t1)*
             (154 + 6*Power(t1,3) + 101*t2 - 98*Power(t2,2) - 
               50*Power(t2,3) + 42*Power(t1,2)*(1 + t2) + 
               t1*(-202 - 230*t2 + 26*Power(t2,2))) - 
            s1*(-1 + t1)*(35 + 335*t2 + 51*Power(t2,2) - 
               73*Power(t2,3) - 25*Power(t2,4) + 
               4*Power(t1,3)*(9 + 4*t2) + 
               Power(t1,2)*(-21 + 67*t2 + 47*Power(t2,2)) + 
               t1*(-50 - 418*t2 - 185*Power(t2,2) + 25*Power(t2,3))) + 
            Power(s,3)*(36 - 16*Power(s1,3) - 33*Power(t1,3) + 
               Power(t1,2)*(105 - 22*t2) + 102*t2 + 19*Power(t2,2) + 
               53*Power(t2,3) + Power(s1,2)*(60 - 77*t1 + 62*t2) - 
               t1*(88 + 141*t2 + 22*Power(t2,2)) + 
               s1*(-82 + 18*Power(t1,2) - 100*t2 - 103*Power(t2,2) + 
                  t1*(81 + 148*t2))) + 
            Power(s,2)*(-35 + 8*Power(s1,4) - 8*Power(t1,4) + 
               Power(s1,3)*(-70 + 74*t1 - 37*t2) + 
               Power(t1,3)*(68 - 10*t2) + 201*t2 + 155*Power(t2,2) + 
               50*Power(t2,3) + 20*Power(t2,4) + 
               Power(t1,2)*(-103 - 28*t2 + 55*Power(t2,2)) - 
               t1*(-78 + 147*t2 + 274*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s1,2)*(205 + 18*Power(t1,2) + 220*t2 + 
                  74*Power(t2,2) - t1*(251 + 217*t2)) + 
               s1*(-139 + 16*Power(t1,3) - 379*t2 - 192*Power(t2,2) - 
                  65*Power(t2,3) - 2*Power(t1,2)*(9 + 16*t2) + 
                  t1*(125 + 503*t2 + 150*Power(t2,2)))) + 
            s*(-57 - Power(s1,5) - 272*t2 + 127*Power(t2,2) + 
               54*Power(t2,3) + 14*Power(t2,4) + Power(t2,5) + 
               2*Power(t1,4)*(10 + t2) + 5*Power(s1,4)*(6 - 6*t1 + t2) + 
               Power(t1,3)*(25 + 129*t2 + 49*Power(t2,2)) + 
               Power(t1,2)*(-167 - 504*t2 - 225*Power(t2,2) + 
                  42*Power(t2,3)) + 
               t1*(179 + 645*t2 + 49*Power(t2,2) - 95*Power(t2,3) - 
                  14*Power(t2,4)) - 
               Power(s1,3)*(121 + 15*Power(t1,2) + 118*t2 + 
                  10*Power(t2,2) - t1*(135 + 118*t2)) + 
               Power(s1,2)*(36 + 32*Power(t1,3) + 305*t2 + 
                  152*Power(t2,2) + 10*Power(t2,3) + 
                  3*Power(t1,2)*(-70 + 11*t2) + 
                  t1*(142 - 335*t2 - 152*Power(t2,2))) + 
               s1*(212 + 2*Power(t1,4) - 138*t2 - 242*Power(t2,2) - 
                  78*Power(t2,3) - 5*Power(t2,4) - 
                  Power(t1,3)*(141 + 70*t2) + 
                  Power(t1,2)*(456 + 438*t2 - 64*Power(t2,2)) + 
                  t1*(-529 - 230*t2 + 303*Power(t2,2) + 78*Power(t2,3))))) \
- Power(s2,6)*(Power(s,5)*(-48 + 5*Power(s1,2) + 120*t1 - 
               35*Power(t1,2) + s1*(24 + 6*t1 - 28*t2) - 60*t2 - 
               44*t1*t2 + 60*Power(t2,2)) + 
            Power(s,4)*(26 - 12*Power(s1,3) - 56*Power(t1,3) + 
               Power(t1,2)*(163 - 119*t2) - 74*t2 + 36*Power(t2,2) + 
               110*Power(t2,3) + Power(s1,2)*(-31 - 30*t1 + 78*t2) + 
               t1*(-56 - 73*t2 + 20*Power(t2,2)) + 
               s1*(30 + 40*Power(t1,2) - 13*t2 - 194*Power(t2,2) + 
                  t1*(24 + 131*t2))) + 
            Power(s,3)*(77 + 9*Power(s1,4) - 23*Power(t1,4) + 
               Power(t1,3)*(33 - 101*t2) + 
               Power(s1,3)*(20 + 29*t1 - 70*t2) + 501*t2 - 
               182*Power(t2,2) + 30*Power(t2,3) + 64*Power(t2,4) + 
               Power(t1,2)*(145 - 79*t2 - 43*Power(t2,2)) + 
               t1*(-160 - 539*t2 + 135*Power(t2,2) + 113*Power(t2,3)) - 
               2*Power(s1,2)*
                (-61 + Power(t1,2) - 52*t2 - 102*Power(t2,2) + 
                  t1*(164 + 59*t2)) + 
               s1*(-360 + 51*Power(t1,3) - 43*t2 - 129*Power(t2,2) - 
                  207*Power(t2,3) + Power(t1,2)*(29 + 189*t2) + 
                  t1*(334 + 264*t2 - 69*Power(t2,2)))) + 
            Power(s,2)*(-203 - 2*Power(s1,5) - 2*Power(t1,5) - 208*t2 + 
               376*Power(t2,2) - 178*Power(t2,3) - 54*Power(t2,4) + 
               10*Power(t2,5) - 2*Power(t1,4)*(4 + 9*t2) + 
               2*Power(s1,4)*(-8 + t1 + 10*t2) + 
               Power(t1,3)*(298 + 22*t2 - 14*Power(t2,2)) + 
               Power(t1,2)*(-607 - 1042*t2 + 8*Power(t2,2) + 
                  62*Power(t2,3)) + 
               t1*(522 + 1302*t2 - 591*Power(t2,2) + 233*Power(t2,3) + 
                  72*Power(t2,4)) - 
               Power(s1,3)*(117 + 9*Power(t1,2) - 2*t2 + 
                  68*Power(t2,2) + 2*t1*(-111 + 7*t2)) + 
               Power(s1,2)*(321 + 36*Power(t1,3) + 6*t2 - 
                  55*Power(t2,2) + 94*Power(t2,3) - 
                  Power(t1,2)*(539 + 107*t2) + 
                  t1*(89 - 62*t2 + 155*Power(t2,2))) + 
               s1*(205 + 11*Power(t1,4) - 631*t2 + 296*Power(t2,2) + 
                  123*Power(t2,3) - 54*Power(t2,4) + 
                  Power(t1,3)*(-65 + 51*t2) + 
                  Power(t1,2)*(782 + 699*t2 + 17*Power(t2,2)) - 
                  t1*(989 - 195*t2 + 363*Power(t2,2) + 215*Power(t2,3)))) \
+ s*(78 + Power(s1,5)*(6 - 7*t1) + 2*Power(t1,5)*(-1 + t2) - 484*t2 - 
               258*Power(t2,2) + 89*Power(t2,3) + 16*Power(t2,4) - 
               6*Power(t2,5) + 
               Power(t1,4)*(241 + 107*t2 + 17*Power(t2,2)) + 
               Power(t1,3)*(-685 - 561*t2 - 38*Power(t2,2) + 
                  31*Power(t2,3)) + 
               Power(t1,2)*(733 + 421*t2 - 891*Power(t2,2) + 
                  258*Power(t2,3) + 48*Power(t2,4)) + 
               t1*(-365 + 515*t2 + 1170*Power(t2,2) - 383*Power(t2,3) - 
                  70*Power(t2,4) + 7*Power(t2,5)) + 
               Power(s1,4)*(49 + 17*Power(t1,2) - 34*t2 + 
                  t1*(-72 + 39*t2)) - 
               Power(s1,3)*(33 + 31*Power(t1,3) + 26*t2 - 
                  92*Power(t2,2) + Power(t1,2)*(-288 + 34*t2) + 
                  3*t1*(73 - 28*t2 + 34*Power(t2,2))) + 
               Power(s1,2)*(-461 + 29*Power(t1,4) + 137*t2 - 
                  70*Power(t2,2) - 112*Power(t2,3) + 
                  Power(t1,3)*(-205 + 14*t2) + 
                  2*Power(t1,2)*(-270 - 177*t2 + 53*Power(t2,2)) + 
                  t1*(1177 + 188*t2 - 72*Power(t2,2) + 122*Power(t2,3))) \
+ s1*(399 + 743*t2 - 210*Power(t2,2) + 31*Power(t2,3) + 54*Power(t2,4) - 
                  3*Power(t1,4)*(39 + 11*t2) + 
                  Power(t1,3)*(428 + 340*t2 - 25*Power(t2,2)) - 
                  Power(t1,2)*
                   (218 - 1222*t2 + 187*Power(t2,2) + 137*Power(t2,3)) + 
                  t1*(-492 - 2272*t2 + 437*Power(t2,2) + 
                     130*Power(t2,3) - 59*Power(t2,4)))) + 
            (-1 + t1)*(-34 - 11*Power(s1,5)*(-2 + t1) - 223*t2 + 
               116*Power(t2,2) + 39*Power(t2,3) - 22*Power(t2,4) - 
               14*Power(t2,5) + 
               Power(s1,4)*(-6 + 24*t1 - 104*t2 + 49*t1*t2) + 
               2*Power(t1,4)*(13 + 8*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(-28 + 137*t2 + 53*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-56 - 489*t2 - 463*Power(t2,2) + 
                  53*Power(t2,3) + 16*Power(t2,4)) + 
               t1*(92 + 559*t2 + 290*Power(t2,2) - 105*Power(t2,3) + 
                  24*Power(t2,4) + 3*Power(t2,5)) + 
               Power(s1,3)*(-130 - 17*Power(t1,3) + 98*t2 + 
                  204*Power(t2,2) + Power(t1,2)*(151 + 6*t2) + 
                  t1*(1 - 176*t2 - 94*Power(t2,2))) + 
               Power(s1,2)*(40 + 2*Power(t1,4) + 289*t2 - 
                  209*Power(t2,2) - 198*Power(t2,3) + 
                  Power(t1,3)*(75 + 33*t2) + 
                  Power(t1,2)*(-449 - 329*t2 + 11*Power(t2,2)) + 
                  t1*(332 - 8*t2 + 306*Power(t2,2) + 88*Power(t2,3))) - 
               s1*(-196 + 140*t2 + 201*Power(t2,2) - 139*Power(t2,3) - 
                  90*Power(t2,4) + 6*Power(t1,4)*(2 + t2) + 
                  Power(t1,3)*(158 + 120*t2 + 25*Power(t2,2)) + 
                  Power(t1,2)*
                   (-492 - 912*t2 - 124*Power(t2,2) + 33*Power(t2,3)) + 
                  t1*(518 + 646*t2 - 117*Power(t2,2) + 178*Power(t2,3) + 
                     35*Power(t2,4))))) + 
         s2*Power(-1 + s + t1,2)*
          (-30 + 5*t1 + 205*Power(t1,2) - 211*Power(t1,3) - 
            167*Power(t1,4) + 302*Power(t1,5) - 104*Power(t1,6) + 
            Power(s1,5)*(-16 + 42*t1 - 32*Power(t1,2) + 7*Power(t1,3)) + 
            4*Power(s,6)*Power(t1 - t2,2) + 223*t2 - 668*t1*t2 + 
            231*Power(t1,2)*t2 + 968*Power(t1,3)*t2 - 
            1070*Power(t1,4)*t2 + 316*Power(t1,5)*t2 - 20*Power(t2,2) + 
            568*t1*Power(t2,2) - 1396*Power(t1,2)*Power(t2,2) + 
            1133*Power(t1,3)*Power(t2,2) - 255*Power(t1,4)*Power(t2,2) - 
            26*Power(t1,5)*Power(t2,2) - 230*Power(t2,3) + 
            522*t1*Power(t2,3) - 295*Power(t1,2)*Power(t2,3) - 
            70*Power(t1,3)*Power(t2,3) + 64*Power(t1,4)*Power(t2,3) + 
            50*Power(t2,4) - 141*t1*Power(t2,4) + 
            141*Power(t1,2)*Power(t2,4) - 44*Power(t1,3)*Power(t2,4) + 
            7*Power(t2,5) - 14*t1*Power(t2,5) + 
            6*Power(t1,2)*Power(t2,5) - 
            Power(s1,4)*(14 + Power(t1,4) - 68*t2 + 
               Power(t1,3)*(10 + 17*t2) - Power(t1,2)*(12 + 109*t2) + 
               t1*(-19 + 165*t2)) + 
            Power(s1,3)*(113 + 48*t2 - 121*Power(t2,2) + 
               2*Power(t1,4)*(14 + t2) + 
               Power(t1,2)*(467 - 108*t2 - 155*Power(t2,2)) + 
               Power(t1,3)*(-211 + 69*t2 + 13*Power(t2,2)) + 
               t1*(-388 - 35*t2 + 273*Power(t2,2))) - 
            Power(s1,2)*(-14 + 30*Power(t1,5) + 323*t2 + 
               16*Power(t2,2) - 109*Power(t2,3) + 
               Power(t1,4)*(38 - 50*t2 + Power(t2,2)) + 
               Power(t1,2)*(461 + 840*t2 - 297*Power(t2,2) - 
                  117*Power(t2,3)) + 
               Power(t1,3)*(-365 - 161*t2 + 146*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(-154 - 925*t2 + 98*Power(t2,2) + 233*Power(t2,3))) + 
            s1*(-67 - 180*t2 + 458*Power(t2,2) - 68*Power(t2,3) - 
               47*Power(t2,4) + Power(t1,5)*(-48 + 52*t2) + 
               Power(t1,4)*(66 + 192*t2 - 130*Power(t2,2)) + 
               Power(t1,3)*(212 - 945*t2 + 62*Power(t2,2) + 
                  131*Power(t2,3)) + 
               Power(t1,2)*(-495 + 884*t2 + 766*Power(t2,2) - 
                  342*Power(t2,3) - 45*Power(t2,4)) + 
               t1*(332 - 11*t2 - 1129*Power(t2,2) + 255*Power(t2,3) + 
                  97*Power(t2,4))) - 
            Power(s,5)*(6 + 8*Power(s1,2)*(-1 + t1) - 8*Power(t1,3) - 
               18*t2 - 11*Power(t2,2) - 7*Power(t2,3) + 
               Power(t1,2)*(3 + 9*t2) + 
               2*t1*(2 + 6*t2 + 3*Power(t2,2)) + 
               s1*(12*Power(t1,2) - 5*t1*(7 + 3*t2) + 
                  3*(2 + 11*t2 + Power(t2,2)))) + 
            Power(s,4)*(-4 + 24*Power(s1,3)*(-1 + t1) + 4*Power(t1,4) - 
               94*t2 + 52*Power(t2,2) - Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,3)*(-8 + 7*t2) - 
               8*Power(t1,2)*(8 + 5*t2 + 3*Power(t2,2)) - 
               Power(s1,2)*(-10 + 62*t1 + 3*Power(t1,2) - 102*t2 + 
                  26*t1*t2 + 6*Power(t2,2)) + 
               t1*(82 + 67*t2 + 33*Power(t2,2) + 11*Power(t2,3)) + 
               s1*(33 - 23*Power(t1,3) - 22*t2 - 85*Power(t2,2) + 
                  4*Power(t2,3) + 2*Power(t1,2)*(55 + 8*t2) + 
                  t1*(-76 + 2*t2 + 3*Power(t2,2)))) - 
            Power(s,3)*(-29 + 24*Power(s1,4)*(-1 + t1) + 
               Power(t1,4)*(13 - 8*t2) + 91*t2 + 346*Power(t2,2) - 
               84*Power(t2,3) - 21*Power(t2,4) + Power(t2,5) + 
               2*Power(t1,3)*(63 + 34*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-131 + 14*t2 - 121*Power(t2,2) + 
                  5*Power(t2,3)) - 
               t1*(30 + 204*t2 + 151*Power(t2,2) - 85*Power(t2,3) + 
                  8*Power(t2,4)) - 
               Power(s1,3)*(54 + 41*Power(t1,2) - 105*t2 + 
                  5*Power(t2,2) + t1*(-55 + 39*t2)) + 
               Power(s1,2)*(164 - 14*Power(t1,3) + 282*t2 - 
                  193*Power(t2,2) + 11*Power(t2,3) + 
                  3*Power(t1,2)*(89 + 14*t2) + 
                  t1*(-516 - 143*t2 + 22*Power(t2,2))) + 
               s1*(-8 + 11*Power(t1,4) - 632*t2 - 126*Power(t2,2) + 
                  133*Power(t2,3) - 7*Power(t2,4) + 
                  Power(t1,3)*(-123 + 13*t2) - 
                  Power(t1,2)*(195 + 88*t2 + 18*Power(t2,2)) + 
                  t1*(460 + 663*t2 + Power(t2,2) + Power(t2,3)))) + 
            Power(s,2)*(-20 + 8*Power(s1,5)*(-1 + t1) - 10*Power(t1,5) + 
               611*t2 + 382*Power(t2,2) - 444*Power(t2,3) - 
               22*Power(t2,4) + 9*Power(t2,5) + 
               Power(t1,4)*(-207 - 84*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(31 + 387*t2 + 194*Power(t2,2) - 
                  9*Power(t2,3)) + 
               Power(t1,2)*(717 + 211*t2 - 619*Power(t2,2) - 
                  122*Power(t2,3) + 6*Power(t2,4)) - 
               t1*(479 + 1205*t2 - 128*Power(t2,2) - 556*Power(t2,3) + 
                  3*Power(t2,4) + Power(t2,5)) + 
               Power(s1,4)*(-82 - 45*Power(t1,2) + 36*t2 - 
                  4*t1*(-31 + 5*t2)) + 
               Power(s1,3)*(91 + 16*Power(t1,3) + 342*t2 - 
                  83*Power(t2,2) + 36*Power(t1,2)*(3 + 2*t2) + 
                  t1*(-280 - 338*t2 + 25*Power(t2,2))) + 
               Power(s1,2)*(9*Power(t1,4) - 4*Power(t1,3)*(67 + 3*t2) + 
                  Power(t1,2)*(697 - 165*t2 - 27*Power(t2,2)) + 
                  t1*(-667 + 470*t2 + 375*Power(t2,2) - 
                     23*Power(t2,3)) + 
                  3*(72 - 38*t2 - 172*Power(t2,2) + 33*Power(t2,3))) - 
               s1*(154 + 1112*t2 - 529*Power(t2,2) - 278*Power(t2,3) + 
                  53*Power(t2,4) + 2*Power(t1,4)*(-41 + 7*t2) + 
                  Power(t1,3)*(-377 + t2 - 9*Power(t2,2)) + 
                  Power(t1,2)*
                   (1579 + 252*t2 - 219*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*(-1286 - 1343*t2 + 856*Power(t2,2) + 
                     158*Power(t2,3) - 11*Power(t2,4)))) + 
            s*(31 + 3*Power(s1,5)*(8 - 14*t1 + 5*Power(t1,2)) - 667*t2 - 
               74*Power(t2,2) + 578*Power(t2,3) - 61*Power(t2,4) - 
               15*Power(t2,5) - 4*Power(t1,5)*(63 + 13*t2) + 
               Power(t1,4)*(327 + 788*t2 + 81*Power(t2,2)) + 
               2*Power(t1,3)*(311 - 672*t2 - 498*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(t1,2)*(-1126 - 321*t2 + 1936*Power(t2,2) + 
                  409*Power(t2,3) - 66*Power(t2,4)) + 
               t1*(398 + 1600*t2 - 951*Power(t2,2) - 991*Power(t2,3) + 
                  121*Power(t2,4) + 15*Power(t2,5)) + 
               Power(s1,4)*(62 - 22*Power(t1,3) + 
                  Power(t1,2)*(84 - 37*t2) - 104*t2 + t1*(-130 + 153*t2)) \
- Power(s1,3)*(228 + Power(t1,4) + 245*t2 - 199*Power(t2,2) - 
                  Power(t1,3)*(163 + 35*t2) + 
                  Power(t1,2)*(624 + 148*t2 - 33*Power(t2,2)) + 
                  t1*(-679 - 382*t2 + 250*Power(t2,2))) + 
               Power(s1,2)*(-75 + 599*t2 + 285*Power(t2,2) - 
                  197*Power(t2,3) + Power(t1,4)*(-97 + 4*t2) - 
                  12*Power(t1,3)*(-8 + 13*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (76 + 1048*t2 + 24*Power(t2,2) - 15*Power(t2,3)) + 
                  t1*(-4 - 1459*t2 - 333*Power(t2,2) + 224*Power(t2,3))) \
+ s1*(186 + 40*Power(t1,5) + 697*t2 - 1007*Power(t2,2) - 41*Power(t2,3) + 
                  93*Power(t2,4) + 
                  Power(t1,4)*(37 - 18*t2 - 3*Power(t2,2)) - 
                  Power(t1,3)*
                   (863 - 651*t2 - 17*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (1691 - 784*t2 - 983*Power(t2,2) + 106*Power(t2,3) + 
                     4*Power(t2,4)) - 
                  t1*(1095 + 538*t2 - 1937*Power(t2,2) + 40*Power(t2,3) + 
                     100*Power(t2,4))))) + 
         Power(s2,5)*(Power(s,6)*
             (-44 + Power(s1,2) - 15*Power(t1,2) - 48*t1*(-2 + t2) - 
               46*t2 + 50*Power(t2,2) - 2*s1*(-15 + 5*t1 + 7*t2)) - 
            Power(s,5)*(66 + 3*Power(s1,3) + 25*Power(t1,3) + 
               Power(s1,2)*(76 - 13*t1 - 51*t2) + 214*t2 - 
               124*Power(t2,2) - 117*Power(t2,3) + 
               Power(t1,2)*(-116 + 153*t2) + 
               t1*(-96 + 43*t2 - 6*Power(t2,2)) + 
               s1*(-154 + 14*Power(t1,2) + t1*(2 - 131*t2) + 17*t2 + 
                  195*Power(t2,2))) + 
            Power(s,4)*(263 + 3*Power(s1,4) - 10*Power(t1,4) + 
               Power(s1,3)*(83 - 7*t1 - 65*t2) + 219*t2 - 
               438*Power(t2,2) + 81*Power(t2,3) + 90*Power(t2,4) - 
               11*Power(t1,3)*(5 + 13*t2) + 
               Power(t1,2)*(514 - 353*t2 - 198*Power(t2,2)) + 
               t1*(-508 - 480*t2 + 762*Power(t2,2) + 181*Power(t2,3)) + 
               Power(s1,2)*(-149 + 55*Power(t1,2) + 141*t2 + 
                  272*Power(t2,2) - t1*(341 + 92*t2)) - 
               s1*(274 + 30*Power(t1,3) - 411*t2 + 282*Power(t2,2) + 
                  300*Power(t2,3) - Power(t1,2)*(138 + 377*t2) + 
                  t1*(-449 + 143*t2 + 171*Power(t2,2)))) - 
            Power(s,3)*(236 + Power(s1,5) + 
               Power(s1,4)*(62 - 23*t1 - 33*t2) - 742*t2 + 
               222*Power(t2,2) + 583*Power(t2,3) + 115*Power(t2,4) - 
               22*Power(t2,5) + 3*Power(t1,4)*(35 + 11*t2) + 
               3*Power(t1,3)*(-167 + 202*t2 + 66*Power(t2,2)) - 
               Power(t1,2)*(-656 - 522*t2 + 646*Power(t2,2) + 
                  Power(t2,3)) - 
               2*t1*(332 + 62*t2 - 307*Power(t2,2) + 475*Power(t2,3) + 
                  89*Power(t2,4)) + 
               Power(s1,3)*(-90 + 13*Power(t1,2) - 7*t2 + 
                  153*Power(t2,2) + t1*(-257 + 101*t2)) + 
               Power(s1,2)*(-524 - 133*Power(t1,3) + 915*t2 + 
                  56*Power(t2,2) - 233*Power(t2,3) + 
                  Power(t1,2)*(907 + 402*t2) + 
                  t1*(201 - 886*t2 - 479*Power(t2,2))) + 
               s1*(430 + 40*Power(t1,4) + 185*t2 - 1483*Power(t2,2) - 
                  226*Power(t2,3) + 134*Power(t2,4) - 
                  237*Power(t1,3)*(1 + t2) - 
                  Power(t1,2)*(812 + 903*t2 + 320*Power(t2,2)) + 
                  t1*(660 - 132*t2 + 2114*Power(t2,2) + 579*Power(t2,3)))\
) - Power(s,2)*(48 + Power(t1,5)*(37 - 5*t2) + 1274*t2 - 
               777*Power(t2,2) - 98*Power(t2,3) + 173*Power(t2,4) + 
               45*Power(t2,5) + Power(s1,5)*(-33 + 27*t1 + 5*t2) + 
               Power(t1,4)*(-263 + 229*t2 + 34*Power(t2,2)) + 
               Power(t1,3)*(400 + 727*t2 + 183*Power(t2,2) + 
                  85*Power(t2,3)) - 
               Power(t1,2)*(503 - 558*t2 + 147*Power(t2,2) + 
                  1352*Power(t2,3) + 103*Power(t2,4)) + 
               t1*(281 - 2895*t2 + 949*Power(t2,2) + 1260*Power(t2,3) - 
                  62*Power(t2,4) - 46*Power(t2,5)) + 
               Power(s1,4)*(25 + 135*t2 - 26*Power(t2,2) - 
                  5*t1*(-9 + 32*t2)) + 
               Power(s1,3)*(263 + 71*Power(t1,3) - 736*t2 - 
                  350*Power(t2,2) + 48*Power(t2,3) - 
                  Power(t1,2)*(597 + 116*t2) + 
                  4*t1*(40 + 178*t2 + 115*Power(t2,2))) + 
               Power(s1,2)*(313 - 118*Power(t1,4) - 429*t2 + 
                  1559*Power(t2,2) + 472*Power(t2,3) - 38*Power(t2,4) + 
                  Power(t1,3)*(850 + 233*t2) + 
                  Power(t1,2)*(612 - 220*t2 - 27*Power(t2,2)) - 
                  t1*(1663 - 677*t2 + 1478*Power(t2,2) + 
                     594*Power(t2,3))) + 
               s1*(-1125 + 14*Power(t1,5) + 477*t2 + 355*Power(t2,2) - 
                  1021*Power(t2,3) - 269*Power(t2,4) + 11*Power(t2,5) + 
                  Power(t1,4)*(-10 + 27*t2) - 
                  Power(t1,3)*(1008 + 1614*t2 + 347*Power(t2,2)) + 
                  Power(t1,2)*
                   (340 + 297*t2 + 2272*Power(t2,2) + 246*Power(t2,3)) + 
                  t1*(1901 + 577*t2 - 2333*Power(t2,2) + 
                     783*Power(t2,3) + 313*Power(t2,4)))) + 
            (-1 + t1)*(70 + 8*Power(s1,6)*(-1 + t1) - 169*t2 - 
               214*Power(t2,2) + 129*Power(t2,3) + 125*Power(t2,4) - 
               10*Power(t2,5) + Power(t1,5)*(6 + 4*t2) + 
               Power(t1,4)*(123 + 82*t2 + 8*Power(t2,2) + 
                  4*Power(t2,3)) - 
               Power(t1,3)*(419 + 546*t2 + 214*Power(t2,2) - 
                  32*Power(t2,3) + 2*Power(t2,4)) - 
               t1*(295 + 144*t2 - 772*Power(t2,2) + 371*Power(t2,3) + 
                  212*Power(t2,4)) + 
               Power(t1,2)*(515 + 773*t2 - 352*Power(t2,2) + 
                  231*Power(t2,3) + 113*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s1,5)*(16 + 5*Power(t1,2) + 45*t2 - 
                  t1*(19 + 45*t2)) + 
               2*Power(s1,4)*
                (26 + 4*Power(t1,3) - 65*t2 - 53*Power(t2,2) - 
                  Power(t1,2)*(13 + 10*t2) + 
                  t1*(-5 + 70*t2 + 53*Power(t2,2))) + 
               Power(s1,3)*(74 - 14*Power(t1,4) - 283*t2 + 
                  282*Power(t2,2) + 128*Power(t2,3) + 
                  Power(t1,3)*(137 + 7*t2) + 
                  Power(t1,2)*(-32 - 173*t2 + 2*Power(t2,2)) - 
                  t1*(190 - 353*t2 + 264*Power(t2,2) + 128*Power(t2,3))) \
+ Power(s1,2)*(-350 + 90*t2 + 567*Power(t2,2) - 248*Power(t2,3) - 
                  78*Power(t2,4) + Power(t1,4)*(50 + 27*t2) - 
                  Power(t1,3)*(481 + 397*t2 + 37*Power(t2,2)) + 
                  Power(t1,2)*
                   (168 + 569*t2 + 587*Power(t2,2) + 44*Power(t2,3)) + 
                  t1*(613 - 214*t2 - 973*Power(t2,2) + 
                     184*Power(t2,3) + 78*Power(t2,4))) + 
               s1*(143 - 2*Power(t1,5) + 562*t2 - 301*Power(t2,2) - 
                  461*Power(t2,3) + 90*Power(t2,4) + 19*Power(t2,5) - 
                  3*Power(t1,4)*(37 + 17*t2 + 6*Power(t2,2)) + 
                  Power(t1,3)*
                   (519 + 764*t2 + 223*Power(t2,2) + 24*Power(t2,3)) - 
                  Power(t1,2)*
                   (666 - 23*t2 + 763*Power(t2,2) + 501*Power(t2,3) + 
                     39*Power(t2,4)) + 
                  t1*(117 - 1298*t2 + 784*Power(t2,2) + 
                     842*Power(t2,3) - 41*Power(t2,4) - 19*Power(t2,5)))) \
+ s*(261 + 8*Power(s1,6)*(-1 + t1) - 2*Power(t1,6) + 410*t2 - 
               691*Power(t2,2) + 242*Power(t2,3) + 287*Power(t2,4) + 
               19*Power(t2,5) + 
               Power(s1,5)*(-22 - 21*Power(t1,2) + t1*(45 - 50*t2) + 
                  50*t2) + 5*Power(t1,5)*(20 + t2 + 2*Power(t2,2)) - 
               Power(t1,4)*(10 + 255*t2 + 153*Power(t2,2) + 
                  18*Power(t2,3)) + 
               Power(t1,3)*(-531 - 1242*t2 - 90*Power(t2,2) + 
                  491*Power(t2,3) + 13*Power(t2,4)) + 
               Power(t1,2)*(963 + 3863*t2 - 810*Power(t2,2) - 
                  313*Power(t2,3) + 297*Power(t2,4) + 32*Power(t2,5)) - 
               t1*(781 + 2781*t2 - 1734*Power(t2,2) + 407*Power(t2,3) + 
                  597*Power(t2,4) + 53*Power(t2,5)) + 
               Power(s1,4)*(-12*Power(t1,3) + 
                  Power(t1,2)*(52 + 107*t2) + 
                  2*(50 + t2 - 66*Power(t2,2)) + 
                  t1*(-140 - 119*t2 + 132*Power(t2,2))) + 
               Power(s1,3)*(341 - 76*Power(t1,4) - 817*t2 + 
                  25*Power(t2,2) + 176*Power(t2,3) + 
                  Power(t1,3)*(650 + 159*t2) - 
                  Power(t1,2)*(592 + 1111*t2 + 305*Power(t2,2)) + 
                  t1*(-318 + 1769*t2 + 300*Power(t2,2) - 176*Power(t2,3))\
) + Power(s1,2)*(-523 + 28*Power(t1,5) - 138*t2 + 1694*Power(t2,2) + 
                  51*Power(t2,3) - 116*Power(t2,4) + 
                  Power(t1,4)*(-104 + 53*t2) - 
                  Power(t1,3)*(1274 + 1121*t2 + 217*Power(t2,2)) + 
                  Power(t1,2)*
                   (2003 + 1720*t2 + 2380*Power(t2,2) + 405*Power(t2,3)) \
+ t1*(-130 - 529*t2 - 3857*Power(t2,2) - 476*Power(t2,3) + 
                     116*Power(t2,4))) - 
               s1*(468 - 1216*t2 + 430*Power(t2,2) + 1264*Power(t2,3) + 
                  75*Power(t2,4) - 30*Power(t2,5) + 
                  Power(t1,5)*(35 + 32*t2) - 
                  Power(t1,4)*(248 + 450*t2 + 33*Power(t2,2)) - 
                  Power(t1,3)*
                   (869 + 1171*t2 - 83*Power(t2,2) + 57*Power(t2,3)) + 
                  2*Power(t1,2)*
                   (1517 + 704*t2 + 325*Power(t2,2) + 809*Power(t2,3) + 
                     109*Power(t2,4)) + 
                  t1*(-2420 + 1397*t2 - 1145*Power(t2,2) - 
                     2825*Power(t2,3) - 303*Power(t2,4) + 30*Power(t2,5)))\
)) + Power(s2,4)*(Power(s,7)*(14 - 5*Power(t1,2) + 12*t2 - 
               22*Power(t2,2) + s1*(-10 + 5*t1 + 3*t2) + t1*(-28 + 31*t2)\
) + Power(s,6)*(96 - 16*Power(t1,3) + Power(s1,2)*(19 + 2*t1 - 14*t2) + 
               107*t2 - 144*Power(t2,2) - 62*Power(t2,3) + 
               Power(t1,2)*(-41 + 76*t2) + 
               t1*(-138 + 119*t2 + 26*Power(t2,2)) + 
               s1*(-99 + 57*Power(t1,2) + 75*t2 + 98*Power(t2,2) - 
                  t1*(40 + 121*t2))) - 
            Power(s,5)*(149 + 17*Power(t1,4) + 
               Power(t1,3)*(22 - 43*t2) + 
               Power(s1,3)*(-2 + 36*t1 - 24*t2) - 260*t2 - 
               130*Power(t2,2) + 148*Power(t2,3) + 58*Power(t2,4) - 
               2*Power(t1,2)*(-167 + 317*t2 + 91*Power(t2,2)) + 
               t1*(-318 + 81*t2 + 608*Power(t2,2) + 90*Power(t2,3)) + 
               Power(s1,2)*(-176 + 96*Power(t1,2) + 326*t2 + 
                  163*Power(t2,2) - t1*(278 + 215*t2)) + 
               s1*(87 - 139*Power(t1,3) + 145*t2 - 471*Power(t2,2) - 
                  197*Power(t2,3) + Power(t1,2)*(201 + 266*t2) + 
                  t1*(225 + 3*t2 + 14*Power(t2,2)))) + 
            Power(s,4)*(-153 - 6*Power(t1,5) - 695*t2 + 
               1241*Power(t2,2) + 481*Power(t2,3) + 56*Power(t2,4) - 
               18*Power(t2,5) - Power(t1,4)*(17 + 18*t2) + 
               2*Power(t1,3)*(-43 + 493*t2 + 92*Power(t2,2)) + 
               Power(t1,2)*(29 + 37*t2 - 308*Power(t2,2) + 
                  72*Power(t2,3)) - 
               t1*(139 - 657*t2 + 1124*Power(t2,2) + 1007*Power(t2,3) + 
                  134*Power(t2,4)) + 
               Power(s1,4)*(46*t1 - 3*(7 + 6*t2)) - 
               Power(s1,3)*(233 + 4*Power(t1,2) - 373*t2 - 
                  120*Power(t2,2) + 5*t1*(33 + 35*t2)) - 
               Power(s1,2)*(-43 + 282*Power(t1,3) - 876*t2 + 
                  495*Power(t2,2) + 204*Power(t2,3) - 
                  Power(t1,2)*(1150 + 523*t2) + 
                  t1*(141 + 1142*t2 + 111*Power(t2,2))) + 
               s1*(698 + 127*Power(t1,4) - 1473*t2 - 1250*Power(t2,2) + 
                  87*Power(t2,3) + 120*Power(t2,4) - 
                  Power(t1,3)*(98 + 73*t2) - 
                  2*Power(t1,2)*(615 + 859*t2 + 249*Power(t2,2)) + 
                  t1*(374 + 1941*t2 + 2441*Power(t2,2) + 374*Power(t2,3))\
)) + Power(s,3)*(Power(t1,5)*(1 - 16*t2) + 
               Power(t1,4)*(194 + 525*t2 + 36*Power(t2,2)) + 
               Power(t1,3)*(-245 + 1237*t2 + 666*Power(t2,2) + 
                  162*Power(t2,3)) - 
               Power(t1,2)*(439 + 144*t2 + 3324*Power(t2,2) + 
                  1511*Power(t2,3) + 73*Power(t2,4)) + 
               2*(228 - 147*t2 - 649*Power(t2,2) + 313*Power(t2,3) + 
                  74*Power(t2,4) + 36*Power(t2,5)) - 
               t1*(179 + 1146*t2 - 4120*Power(t2,2) - 717*Power(t2,3) + 
                  233*Power(t2,4) + 49*Power(t2,5)) + 
               Power(s1,5)*(-17*t1 + 5*(2 + t2)) + 
               Power(s1,4)*(254 + 93*Power(t1,2) - 140*t2 - 
                  33*Power(t2,2) + 2*t1*(-88 + 17*t2)) + 
               Power(s1,3)*(-99 + 172*Power(t1,3) - 1599*t2 + 
                  131*Power(t2,2) + 69*Power(t2,3) - 
                  Power(t1,2)*(1174 + 535*t2) + 
                  t1*(794 + 1935*t2 + 202*Power(t2,2))) + 
               Power(s1,2)*(-602 - 268*Power(t1,4) + 1897*t2 + 
                  2568*Power(t2,2) + 190*Power(t2,3) - 59*Power(t2,4) + 
                  Power(t1,3)*(1217 + 225*t2) + 
                  Power(t1,2)*(695 + 653*t2 + 493*Power(t2,2)) - 
                  t1*(1014 + 2018*t2 + 3361*Power(t2,2) + 
                     487*Power(t2,3))) + 
               s1*(-506 + 40*Power(t1,5) + 2018*t2 - 2274*Power(t2,2) - 
                  1371*Power(t2,3) - 263*Power(t2,4) + 18*Power(t2,5) + 
                  Power(t1,4)*(266 + 153*t2) - 
                  3*Power(t1,3)*(885 + 1001*t2 + 170*Power(t2,2)) + 
                  Power(t1,2)*
                   (1876 + 3549*t2 + 2393*Power(t2,2) + 22*Power(t2,3)) \
+ t1*(1281 - 3237*t2 - 13*Power(t2,2) + 1835*Power(t2,3) + 
                     317*Power(t2,4)))) - 
            (-1 + t1)*(68 + 16*Power(s1,6)*(-1 + t1) + 
               Power(t1,5)*(53 - 42*t2) + 280*t2 - 386*Power(t2,2) - 
               127*Power(t2,3) + 72*Power(t2,4) - 109*Power(t2,5) + 
               18*Power(t2,6) - 
               Power(t1,4)*(157 + 178*t2 + 124*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,3)*(197 + 19*t2 - 19*Power(t2,2) + 
                  643*Power(t2,3) + 31*Power(t2,4)) + 
               Power(t1,2)*(-67 + 1044*t2 - 284*Power(t2,2) - 
                  887*Power(t2,3) - 361*Power(t2,4) + 57*Power(t2,5)) + 
               t1*(-94 - 1123*t2 + 813*Power(t2,2) + 412*Power(t2,3) + 
                  252*Power(t2,4) + 62*Power(t2,5) - 18*Power(t2,6)) + 
               Power(s1,5)*(-44 + 11*Power(t1,3) + t1*(60 - 90*t2) + 
                  96*t2 - Power(t1,2)*(37 + 6*t2)) + 
               Power(s1,4)*(150 + 22*Power(t1,4) + 227*t2 - 
                  201*Power(t2,2) - 3*Power(t1,3)*(67 + 21*t2) + 
                  Power(t1,2)*(411 + 305*t2 + 37*Power(t2,2)) + 
                  t1*(-388 - 419*t2 + 164*Power(t2,2))) + 
               Power(s1,3)*(130 - 840*t2 - 289*Power(t2,2) + 
                  169*Power(t2,3) - Power(t1,4)*(43 + 52*t2) + 
                  3*Power(t1,3)*(128 + 161*t2 + 38*Power(t2,2)) + 
                  t1*(303 + 1587*t2 + 770*Power(t2,2) - 
                     94*Power(t2,3)) - 
                  Power(t1,2)*
                   (799 + 1154*t2 + 695*Power(t2,2) + 75*Power(t2,3))) + 
               Power(s1,2)*(-56 + 21*Power(t1,5) - 381*t2 + 
                  1260*Power(t2,2) - 36*Power(t2,3) - 21*Power(t2,4) + 
                  Power(t1,4)*(-325 - 80*t2 + 38*Power(t2,2)) + 
                  Power(t1,3)*
                   (41 + 166*t2 - 243*Power(t2,2) - 83*Power(t2,3)) - 
                  t1*(342 + 83*t2 + 1601*Power(t2,2) + 
                     461*Power(t2,3) + 42*Power(t2,4)) + 
                  Power(t1,2)*
                   (661 + 453*t2 + 510*Power(t2,2) + 680*Power(t2,3) + 
                     63*Power(t2,4))) - 
               s1*(320 - 494*t2 - 398*Power(t2,2) + 642*Power(t2,3) - 
                  251*Power(t2,4) + 45*Power(t2,5) + 
                  2*Power(t1,5)*(-4 + 9*t2) + 
                  Power(t1,4)*
                   (-271 - 580*t2 - 134*Power(t2,2) + 8*Power(t2,3)) + 
                  Power(t1,3)*
                   (302 + 325*t2 + 1222*Power(t2,2) + 70*Power(t2,3) - 
                     21*Power(t2,4)) + 
                  t1*(-1004 + 607*t2 + 711*Power(t2,2) - 
                     150*Power(t2,3) + 12*Power(t2,4) - 64*Power(t2,5)) \
+ Power(t1,2)*(661 + 124*t2 - 1326*Power(t2,2) - 594*Power(t2,3) + 
                     310*Power(t2,4) + 19*Power(t2,5)))) + 
            Power(s,2)*(-488 + 9*Power(t1,6) + 1476*t2 - 
               492*Power(t2,2) - 1381*Power(t2,3) - 10*Power(t2,4) - 
               159*Power(t2,5) + 18*Power(t2,6) + 
               Power(t1,5)*(156 + 72*t2 - 14*Power(t2,2)) + 
               Power(t1,4)*(-308 + 1395*t2 + 606*Power(t2,2) + 
                  58*Power(t2,3)) + 
               Power(t1,3)*(-163 + 181*t2 - 2565*Power(t2,2) - 
                  635*Power(t2,3) + 24*Power(t2,4)) - 
               Power(t1,2)*(443 + 2382*t2 - 3501*Power(t2,2) + 
                  719*Power(t2,3) + 638*Power(t2,4) + 44*Power(t2,5)) + 
               t1*(1237 - 842*t2 - 884*Power(t2,2) + 2485*Power(t2,3) + 
                  869*Power(t2,4) + 82*Power(t2,5)) + 
               Power(s1,5)*(-128 - 45*Power(t1,2) + 6*t2 + 
                  t1*(147 + 16*t2)) + 
               Power(s1,4)*(-121 + 26*Power(t1,3) + 857*t2 + 
                  25*Power(t2,2) + Power(t1,2)*(97 + 185*t2) + 
                  t1*(11 - 961*t2 - 103*Power(t2,2))) + 
               Power(s1,3)*(488 + 212*Power(t1,4) + 471*t2 - 
                  1455*Power(t2,2) - 129*Power(t2,3) - 
                  Power(t1,3)*(1620 + 397*t2) + 
                  Power(t1,2)*(2143 + 1712*t2 - 70*Power(t2,2)) + 
                  t1*(-1155 - 1970*t2 + 1541*Power(t2,2) + 
                     213*Power(t2,3))) + 
               Power(s1,2)*(869 - 84*Power(t1,5) + 
                  Power(t1,4)*(98 - 189*t2) - 3828*t2 - 
                  767*Power(t2,2) + 692*Power(t2,3) + 177*Power(t2,4) + 
                  Power(t1,3)*(3192 + 3094*t2 + 629*Power(t2,2)) - 
                  Power(t1,2)*
                   (5009 + 7534*t2 + 4410*Power(t2,2) + 
                     279*Power(t2,3)) + 
                  t1*(870 + 8261*t2 + 5106*Power(t2,2) - 
                     705*Power(t2,3) - 181*Power(t2,4))) + 
               s1*(-653 - 274*t2 + 4713*Power(t2,2) + 427*Power(t2,3) + 
                  193*Power(t2,4) - 97*Power(t2,5) + 
                  Power(t1,5)*(205 + 84*t2) - 
                  Power(t1,4)*(1801 + 1399*t2 + 72*Power(t2,2)) - 
                  Power(t1,3)*
                   (-403 + 322*t2 + 497*Power(t2,2) + 282*Power(t2,3)) + 
                  Power(t1,2)*
                   (3323 + 2398*t2 + 5495*Power(t2,2) + 
                     3239*Power(t2,3) + 253*Power(t2,4)) + 
                  t1*(-1377 - 575*t2 - 9319*Power(t2,2) - 
                     4016*Power(t2,3) - 104*Power(t2,4) + 55*Power(t2,5))\
)) + s*(108 - 16*Power(s1,6)*(-1 + t1) - 1194*t2 + 1019*Power(t2,2) + 
               701*Power(t2,3) - 148*Power(t2,4) + 232*Power(t2,5) - 
               36*Power(t2,6) + Power(t1,6)*(50 + 6*t2) + 
               Power(t1,5)*(-281 + 451*t2 + 96*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(t1,4)*(659 + 704*t2 - 395*Power(t2,2) + 
                  33*Power(t2,3) + 21*Power(t2,4)) + 
               Power(t1,2)*(1675 - 528*t2 + 636*Power(t2,2) + 
                  3190*Power(t2,3) + 1129*Power(t2,4) - 47*Power(t2,5)) - 
               Power(t1,3)*(1335 + 2406*t2 - 804*Power(t2,2) + 
                  1618*Power(t2,3) + 380*Power(t2,4) + 13*Power(t2,5)) + 
               t1*(-876 + 2967*t2 - 2160*Power(t2,2) - 
                  2303*Power(t2,3) - 628*Power(t2,4) - 170*Power(t2,5) + 
                  36*Power(t2,6)) + 
               Power(s1,5)*(144 - 39*Power(t1,3) - 107*t2 + 
                  Power(t1,2)*(185 + 17*t2) + t1*(-292 + 90*t2)) - 
               Power(s1,4)*(202 + 43*Power(t1,4) + 836*t2 - 
                  209*Power(t2,2) - Power(t1,3)*(475 + 196*t2) + 
                  3*t1*(-245 - 613*t2 + 34*Power(t2,2)) + 
                  Power(t1,2)*(971 + 1189*t2 + 107*Power(t2,2))) + 
               Power(s1,3)*(-378 + 72*Power(t1,5) + 1331*t2 + 
                  1313*Power(t2,2) - 109*Power(t2,3) - 
                  3*Power(t1,4)*(190 + 3*t2) + 
                  Power(t1,3)*(575 - 385*t2 - 266*Power(t2,2)) - 
                  t1*(-56 + 2505*t2 + 3286*Power(t2,2) + 
                     110*Power(t2,3)) + 
                  Power(t1,2)*
                   (246 + 1592*t2 + 2219*Power(t2,2) + 219*Power(t2,3))) \
+ Power(s1,2)*(-401 + 2046*t2 - 2043*Power(t2,2) - 462*Power(t2,3) - 
                  97*Power(t2,4) - 3*Power(t1,5)*(83 + 40*t2) + 
                  5*Power(t1,4)*(490 + 341*t2 + 30*Power(t2,2)) + 
                  Power(t1,3)*
                   (-4143 - 4666*t2 - 1263*Power(t2,2) + 87*Power(t2,3)) \
+ Power(t1,2)*(1523 + 5639*t2 + 1497*Power(t2,2) - 1658*Power(t2,3) - 
                     185*Power(t2,4)) + 
                  t1*(820 - 4607*t2 + 1623*Power(t2,2) + 
                     2053*Power(t2,3) + 282*Power(t2,4))) + 
               s1*(1025 + 12*Power(t1,6) - 794*t2 - 2426*Power(t2,2) + 
                  1062*Power(t2,3) - 391*Power(t2,4) + 124*Power(t2,5) + 
                  Power(t1,5)*(-293 - 18*t2 + 52*Power(t2,2)) - 
                  Power(t1,4)*
                   (1158 + 2284*t2 + 1054*Power(t2,2) + 119*Power(t2,3)) \
+ Power(t1,3)*(3769 + 4616*t2 + 5512*Power(t2,2) + 1553*Power(t2,3) + 
                     35*Power(t2,4)) + 
                  t1*(-1628 + 1998*t2 + 6999*Power(t2,2) + 
                     775*Power(t2,3) - 144*Power(t2,4) - 180*Power(t2,5)) \
+ Power(t1,2)*(-1727 - 3518*t2 - 9080*Power(t2,2) - 3247*Power(t2,3) + 
                     490*Power(t2,4) + 56*Power(t2,5))))) - 
         Power(s2,2)*(-1 + s + t1)*
          (-34 - 8*Power(s1,6)*(-2 + t1)*Power(-1 + t1,2) - 32*t1 + 
            215*Power(t1,2) + 93*Power(t1,3) - 691*Power(t1,4) + 
            591*Power(t1,5) - 126*Power(t1,6) - 16*Power(t1,7) + 
            8*Power(s,7)*Power(t1 - t2,2) - 31*t2 + 480*t1*t2 - 
            1605*Power(t1,2)*t2 + 2330*Power(t1,3)*t2 - 
            1782*Power(t1,4)*t2 + 780*Power(t1,5)*t2 - 
            172*Power(t1,6)*t2 + 256*Power(t2,2) - 439*t1*Power(t2,2) - 
            646*Power(t1,2)*Power(t2,2) + 2111*Power(t1,3)*Power(t2,2) - 
            1823*Power(t1,4)*Power(t2,2) + 545*Power(t1,5)*Power(t2,2) - 
            4*Power(t1,6)*Power(t2,2) - 132*Power(t2,3) + 
            995*t1*Power(t2,3) - 2074*Power(t1,2)*Power(t2,3) + 
            1664*Power(t1,3)*Power(t2,3) - 418*Power(t1,4)*Power(t2,3) - 
            34*Power(t1,5)*Power(t2,3) - 316*Power(t2,4) + 
            755*t1*Power(t2,4) - 477*Power(t1,2)*Power(t2,4) - 
            56*Power(t1,3)*Power(t2,4) + 92*Power(t1,4)*Power(t2,4) + 
            75*Power(t2,5) - 211*t1*Power(t2,5) + 
            197*Power(t1,2)*Power(t2,5) - 60*Power(t1,3)*Power(t2,5) + 
            6*Power(t2,6) - 12*t1*Power(t2,6) + 
            6*Power(t1,2)*Power(t2,6) + 
            Power(s1,5)*(22 + 9*Power(t1,4) + 
               Power(t1,2)*(56 - 138*t2) - 84*t2 + 
               3*Power(t1,3)*(-11 + 9*t2) + 5*t1*(-11 + 39*t2)) + 
            Power(s1,4)*(-134 + 11*Power(t1,5) - 111*t2 + 
               189*Power(t2,2) - Power(t1,4)*(107 + 34*t2) + 
               t1*(464 + 261*t2 - 411*Power(t2,2)) + 
               Power(t1,3)*(382 + 124*t2 - 33*Power(t2,2)) + 
               Power(t1,2)*(-618 - 235*t2 + 255*Power(t2,2))) + 
            Power(s1,3)*(-28 + Power(t1,5)*(84 - 22*t2) + 546*t2 + 
               133*Power(t2,2) - 229*Power(t2,3) + 
               Power(t1,4)*(-196 + 144*t2 + 41*Power(t2,2)) + 
               Power(t1,2)*(173 + 1682*t2 + 164*Power(t2,2) - 
                  263*Power(t2,3)) + 
               Power(t1,3)*(29 - 746*t2 - 96*Power(t2,2) + 
                  17*Power(t2,3)) + 
               t1*(-63 - 1596*t2 - 252*Power(t2,2) + 475*Power(t2,3))) + 
            Power(s1,2)*(12 + 40*Power(t1,6) + 452*t2 - 
               1051*Power(t2,2) + 54*Power(t2,3) + 153*Power(t2,4) + 
               Power(t1,5)*(-328 - 170*t2 + 11*Power(t2,2)) + 
               Power(t1,4)*(1052 + 320*t2 + 81*Power(t2,2) - 
                  16*Power(t2,3)) - 
               Power(t1,3)*(1511 + 124*t2 - 465*Power(t2,2) + 
                  108*Power(t2,3) + 3*Power(t2,4)) + 
               Power(t1,2)*(1001 + 402*t2 - 2250*Power(t2,2) + 
                  350*Power(t2,3) + 159*Power(t2,4)) - 
               t1*(266 + 877*t2 - 2732*Power(t2,2) + 270*Power(t2,3) + 
                  309*Power(t2,4))) + 
            s1*(146 - 528*t2 - 261*Power(t2,2) + 955*Power(t2,3) - 
               173*Power(t2,4) - 51*Power(t2,5) + 
               4*Power(t1,6)*(35 + 2*t2) + 
               Power(t1,5)*(-286 - 296*t2 + 84*Power(t2,2)) + 
               Power(t1,4)*(-63 + 588*t2 + 447*Power(t2,2) - 
                  210*Power(t2,3)) + 
               Power(t1,3)*(378 + 283*t2 - 1847*Power(t2,2) - 
                  45*Power(t2,3) + 173*Power(t2,4)) + 
               Power(t1,2)*(23 - 1736*t2 + 1771*Power(t2,2) + 
                  1663*Power(t2,3) - 532*Power(t2,4) - 51*Power(t2,5)) + 
               t1*(-338 + 1681*t2 - 197*Power(t2,2) - 2355*Power(t2,3) + 
                  527*Power(t2,4) + 102*Power(t2,5))) + 
            Power(s,6)*(-26 - 16*Power(s1,2)*(-1 + t1) + 
               21*Power(t1,3) + Power(t1,2)*(10 - 22*t2) + 24*t2 + 
               52*Power(t2,2) + 20*Power(t2,3) + 
               t1*(14 - 74*t2 - 19*Power(t2,2)) - 
               s1*(2 + 37*Power(t1,2) + 69*t2 + 22*Power(t2,2) - 
                  t1*(71 + 59*t2))) + 
            Power(s,5)*(-10 + 56*Power(s1,3)*(-1 + t1) + 
               18*Power(t1,4) - 201*t2 + 89*Power(t2,2) + 
               27*Power(t2,3) + 16*Power(t2,4) + 
               Power(t1,3)*(61 + 14*t2) - 
               Power(t1,2)*(133 + 308*t2 + 66*Power(t2,2)) + 
               t1*(99 + 232*t2 + 160*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s1,2)*(7 + 22*Power(t1,2) + 268*t2 + 
                  15*Power(t2,2) - 6*t1*(31 + 21*t2)) + 
               s1*(103 - 100*Power(t1,3) - 128*t2 - 230*Power(t2,2) - 
                  31*Power(t2,3) + Power(t1,2)*(242 + 92*t2) + 
                  t1*(-56 + 53*t2 + 39*Power(t2,2)))) + 
            Power(s,4)*(155 - 72*Power(s1,4)*(-1 + t1) + 5*Power(t1,5) - 
               228*t2 - 866*Power(t2,2) + 67*Power(t2,3) - 
               8*Power(t2,4) + 4*Power(t2,5) + 
               Power(t1,4)*(99 + 30*t2) - 
               Power(t1,3)*(417 + 443*t2 + 38*Power(t2,2)) + 
               Power(t1,2)*(296 + 12*t2 + 155*Power(t2,2) - 
                  30*Power(t2,3)) + 
               t1*(37 + 357*t2 + 778*Power(t2,2) + 77*Power(t2,3) + 
                  29*Power(t2,4)) + 
               Power(s1,3)*(114 + 92*Power(t1,2) - 373*t2 + 
                  4*Power(t2,2) + t1*(-5 + 166*t2)) + 
               Power(s1,2)*(-384 + 140*Power(t1,3) - 447*t2 + 
                  493*Power(t2,2) - 4*Power(t2,3) - 
                  4*Power(t1,2)*(221 + 66*t2) + 
                  t1*(970 + 519*t2 - 76*Power(t2,2))) - 
               s1*(96 + 89*Power(t1,4) - 1604*t2 - 255*Power(t2,2) + 
                  184*Power(t2,3) + 4*Power(t2,4) + 
                  Power(t1,3)*(-172 + 35*t2) - 
                  Power(t1,2)*(881 + 831*t2 + 175*Power(t2,2)) + 
                  t1*(968 + 2052*t2 + 575*Power(t2,2) + 47*Power(t2,3)))) \
+ Power(s,3)*(-208 + 40*Power(s1,5)*(-1 + t1) + 1459*t2 + 
               636*Power(t2,2) - 821*Power(t2,3) + 213*Power(t2,4) - 
               Power(t2,5) + 2*Power(t1,5)*(32 + 5*t2) + 
               Power(t1,4)*(-530 - 313*t2 + 6*Power(t2,2)) - 
               Power(t1,3)*(161 + 330*t2 - 58*Power(t2,2) + 
                  34*Power(t2,3)) + 
               Power(t1,2)*(1655 + 1921*t2 + 943*Power(t2,2) + 
                  152*Power(t2,3) + 10*Power(t2,4)) + 
               t1*(-637 - 2890*t2 - 1413*Power(t2,2) + 
                  344*Power(t2,3) - 80*Power(t2,4) + 8*Power(t2,5)) - 
               Power(s1,4)*(233 + 158*Power(t1,2) - 218*t2 + 
                  5*Power(t2,2) + 2*t1*(-146 + 55*t2)) + 
               Power(s1,3)*(371 - 22*Power(t1,3) + 1150*t2 - 
                  446*Power(t2,2) + 15*Power(t2,3) + 
                  Power(t1,2)*(709 + 394*t2) + 
                  t1*(-1157 - 1187*t2 + 99*Power(t2,2))) + 
               Power(s1,2)*(526 + 150*Power(t1,4) - 1817*t2 - 
                  1254*Power(t2,2) + 397*Power(t2,3) - 15*Power(t2,4) - 
                  8*Power(t1,3)*(139 + 12*t2) + 
                  Power(t1,2)*(1154 - 467*t2 - 265*Power(t2,2)) + 
                  t1*(-434 + 2230*t2 + 1236*Power(t2,2) - 
                     20*Power(t2,3))) - 
               s1*(314 + 26*Power(t1,5) + 1858*t2 - 2235*Power(t2,2) - 
                  124*Power(t2,3) + 128*Power(t2,4) - 5*Power(t2,5) + 
                  5*Power(t1,4)*(23 + 22*t2) - 
                  Power(t1,3)*(2296 + 1323*t2 + 129*Power(t2,2)) + 
                  Power(t1,2)*
                   (4006 + 2941*t2 + 453*Power(t2,2) - 19*Power(t2,3)) + 
                  t1*(-1918 - 3303*t2 + 1327*Power(t2,2) + 
                     261*Power(t2,3) + 17*Power(t2,4)))) - 
            Power(s,2)*(-195 + 8*Power(s1,6)*(-1 + t1) - 
               16*Power(t1,6) + 1565*t2 - 727*Power(t2,2) - 
               948*Power(t2,3) + 872*Power(t2,4) - 41*Power(t2,5) - 
               6*Power(t2,6) + 
               Power(s1,5)*(-138 - 89*Power(t1,2) + t1*(212 - 27*t2) + 
                  44*t2) + Power(t1,5)*(415 + 112*t2 - 5*Power(t2,2)) + 
               Power(t1,4)*(427 + 260*t2 + 71*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,3)*(-3011 - 1456*t2 - 1101*Power(t2,2) - 
                  249*Power(t2,3) + 3*Power(t2,4)) + 
               Power(t1,2)*(2467 + 3821*t2 + 609*Power(t2,2) + 
                  807*Power(t2,3) + 97*Power(t2,4) - 4*Power(t2,5)) + 
               t1*(-119 - 4257*t2 + 1127*Power(t2,2) + 
                  322*Power(t2,3) - 930*Power(t2,4) + 51*Power(t2,5)) + 
               Power(s1,4)*(-38 + 89*Power(t1,3) + 715*t2 - 
                  119*Power(t2,2) + Power(t1,2)*(-96 + 254*t2) + 
                  t1*(-10 - 863*t2 + 43*Power(t2,2))) + 
               Power(s1,3)*(746 + 96*Power(t1,4) + 311*t2 - 
                  1325*Power(t2,2) + 179*Power(t2,3) - 
                  Power(t1,3)*(1143 + 268*t2) + 
                  Power(t1,2)*(2650 + 719*t2 - 227*Power(t2,2)) - 
                  t1*(2298 + 622*t2 - 1314*Power(t2,2) + 47*Power(t2,3))\
) + Power(s1,2)*(181 - 48*Power(t1,5) + Power(t1,4)*(446 - 96*t2) - 
                  3918*t2 + 586*Power(t2,2) + 1016*Power(t2,3) - 
                  143*Power(t2,4) + 
                  Power(t1,3)*(640 + 1291*t2 + 231*Power(t2,2)) + 
                  Power(t1,2)*
                   (-3105 - 4717*t2 - 732*Power(t2,2) + 44*Power(t2,3)) \
+ t1*(1895 + 7248*t2 - 156*Power(t2,2) - 836*Power(t2,3) + 
                     33*Power(t2,4))) + 
               s1*(-644 + 391*t2 + 4018*Power(t2,2) - 1731*Power(t2,3) - 
                  227*Power(t2,4) + 53*Power(t2,5) + 
                  2*Power(t1,5)*(86 + 21*t2) + 
                  Power(t1,4)*(-1999 - 880*t2 + Power(t2,2)) + 
                  Power(t1,3)*
                   (3772 + 1491*t2 + 267*Power(t2,2) - 55*Power(t2,3)) + 
                  Power(t1,2)*
                   (-2363 + 751*t2 + 875*Power(t2,2) + 12*Power(t2,3) + 
                     22*Power(t2,4)) + 
                  t1*(1085 - 1818*t2 - 4954*Power(t2,2) + 
                     1718*Power(t2,3) + 122*Power(t2,4) - 10*Power(t2,5))\
)) - s*(72 + 8*Power(s1,6)*(3 - 5*t1 + 2*Power(t1,2)) - 542*t2 + 
               883*Power(t2,2) + 113*Power(t2,3) - 939*Power(t2,4) + 
               129*Power(t2,5) + 12*Power(t2,6) + 
               4*Power(t1,6)*(41 + 2*t2) + 
               Power(t1,5)*(267 + 326*t2 + 86*Power(t2,2)) - 
               Power(t1,4)*(1876 + 1123*t2 + 1434*Power(t2,2) + 
                  113*Power(t2,3)) + 
               Power(t1,3)*(1867 + 2642*t2 + 2423*Power(t2,2) + 
                  1530*Power(t2,3) - 67*Power(t2,4)) + 
               Power(t1,2)*(14 - 3679*t2 - 138*Power(t2,2) - 
                  2743*Power(t2,3) - 668*Power(t2,4) + 110*Power(t2,5)) + 
               t1*(-508 + 2368*t2 - 1823*Power(t2,2) + 
                  1206*Power(t2,3) + 1671*Power(t2,4) - 
                  238*Power(t2,5) - 12*Power(t2,6)) + 
               Power(s1,5)*(110 - 58*Power(t1,3) + 
                  Power(t1,2)*(205 - 54*t2) - 128*t2 + t1*(-255 + 182*t2)\
) + Power(s1,4)*(-229 - 8*Power(t1,4) - 558*t2 + 303*Power(t2,2) + 
                  Power(t1,3)*(231 + 178*t2) + 
                  t1*(719 + 1142*t2 - 374*Power(t2,2)) + 
                  Power(t1,2)*(-716 - 769*t2 + 71*Power(t2,2))) + 
               Power(s1,3)*(-349 + 38*Power(t1,5) + 900*t2 + 
                  916*Power(t2,2) - 393*Power(t2,3) - 
                  Power(t1,4)*(569 + 18*t2) + 
                  Power(t1,3)*(1501 - 239*t2 - 173*Power(t2,2)) + 
                  Power(t1,2)*
                   (-1663 + 1476*t2 + 964*Power(t2,2) - 49*Power(t2,3)) \
+ t1*(1046 - 2107*t2 - 1699*Power(t2,2) + 442*Power(t2,3))) - 
               Power(s1,2)*(23 - 2386*t2 + 2215*Power(t2,2) + 
                  469*Power(t2,3) - 281*Power(t2,4) + 
                  6*Power(t1,5)*(4 + 9*t2) - 
                  Power(t1,4)*(1113 + 743*t2 + 46*Power(t2,2)) + 
                  Power(t1,3)*
                   (3852 + 2236*t2 + 70*Power(t2,2) - 44*Power(t2,3)) + 
                  Power(t1,2)*
                   (-4379 - 4388*t2 + 2013*Power(t2,2) + 
                     331*Power(t2,3) - 21*Power(t2,4)) + 
                  2*t1*(798 + 2621*t2 - 2117*Power(t2,2) - 
                     377*Power(t2,3) + 151*Power(t2,4))) + 
               s1*(481 + 56*Power(t1,6) - 1332*t2 - 2053*Power(t2,2) + 
                  2483*Power(t2,3) - 128*Power(t2,4) - 99*Power(t2,5) + 
                  Power(t1,5)*(-803 - 274*t2 + 16*Power(t2,2)) + 
                  Power(t1,4)*
                   (1331 + 845*t2 + 75*Power(t2,2) - 20*Power(t2,3)) + 
                  Power(t1,3)*
                   (19 + 968*t2 - 1232*Power(t2,2) + 145*Power(t2,3) + 
                     9*Power(t2,4)) - 
                  Power(t1,2)*
                   (604 + 4595*t2 - 589*Power(t2,2) - 1921*Power(t2,3) + 
                     179*Power(t2,4) + 5*Power(t2,5)) + 
                  t1*(-480 + 4394*t2 + 2623*Power(t2,2) - 
                     4517*Power(t2,3) + 296*Power(t2,4) + 104*Power(t2,5))\
))) + Power(s2,3)*(4*Power(s,8)*Power(t1 - t2,2) + 
            Power(s,7)*(-34 - 8*Power(s1,2)*(-1 + t1) + 12*Power(t1,3) + 
               Power(t1,2)*(17 - 11*t2) - 6*t2 + 63*Power(t2,2) + 
               13*Power(t2,3) - 2*t1*(-24 + 46*t2 + 7*Power(t2,2)) + 
               s1*(14 - 26*Power(t1,2) - 39*t2 - 19*Power(t2,2) + 
                  t1*(29 + 45*t2))) + 
            Power(s,6)*(-44 + 32*Power(s1,3)*(-1 + t1) + 12*Power(t1,4) - 
               156*t2 + 109*Power(t2,2) + 96*Power(t2,3) + 
               14*Power(t2,4) + Power(t1,3)*(91 + 15*t2) + 
               Power(t1,2)*(10 - 375*t2 - 52*Power(t2,2)) + 
               t1*(18 + 119*t2 + 116*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s1,2)*(-23 + 40*Power(t1,2) + 180*t2 + 
                  33*Power(t2,2) - 6*t1*(20 + 21*t2)) + 
               s1*(129 - 78*Power(t1,3) - 164*t2 - 251*Power(t2,2) - 
                  47*Power(t2,3) + Power(t1,2)*(64 + 65*t2) + 
                  2*t1*(39 + 88*t2 + 30*Power(t2,2)))) + 
            Power(s,5)*(301 - 48*Power(s1,4)*(-1 + t1) + 4*Power(t1,5) - 
               236*t2 - 770*Power(t2,2) - 94*Power(t2,3) + 
               17*Power(t2,4) + 5*Power(t2,5) + 
               Power(t1,4)*(153 + 31*t2) - 
               Power(t1,3)*(207 + 471*t2 + 32*Power(t2,2)) - 
               Power(t1,2)*(-244 + 450*t2 + 253*Power(t2,2) + 
                  40*Power(t2,3)) + 
               t1*(-251 + 250*t2 + 1407*Power(t2,2) + 374*Power(t2,3) + 
                  32*Power(t2,4)) + 
               Power(s1,3)*(69 + 20*Power(t1,2) - 304*t2 - 
                  25*Power(t2,2) + 4*t1*(19 + 47*t2)) + 
               Power(s1,2)*(-310 + 168*Power(t1,3) + 82*t2 + 
                  424*Power(t2,2) + 55*Power(t2,3) - 
                  Power(t1,2)*(607 + 246*t2) + 
                  t1*(180 + 314*t2 - 126*Power(t2,2))) - 
               s1*(178 + 78*Power(t1,4) - 1187*t2 - 12*Power(t2,2) + 
                  185*Power(t2,3) + 35*Power(t2,4) + 
                  25*Power(t1,3)*(8 + 3*t2) - 
                  Power(t1,2)*(1094 + 1335*t2 + 234*Power(t2,2)) + 
                  t1*(312 + 1862*t2 + 863*Power(t2,2) + 46*Power(t2,3)))) \
+ Power(s,4)*(-345 + 32*Power(s1,5)*(-1 + t1) + 1366*t2 - 
               347*Power(t2,2) - 691*Power(t2,3) + 5*Power(t2,4) - 
               34*Power(t2,5) + Power(t1,5)*(101 + 13*t2) + 
               Power(t1,4)*(-241 - 167*t2 + 16*Power(t2,2)) - 
               Power(t1,3)*(271 + 2072*t2 + 677*Power(t2,2) + 
                  56*Power(t2,3)) + 
               Power(t1,2)*(642 + 2524*t2 + 3110*Power(t2,2) + 
                  377*Power(t2,3) + 12*Power(t2,4)) + 
               t1*(491 - 2145*t2 - 2101*Power(t2,2) + 378*Power(t2,3) + 
                  160*Power(t2,4) + 15*Power(t2,5)) + 
               Power(s1,4)*(-157 - 100*Power(t1,2) + 224*t2 + 
                  7*Power(t2,2) - 2*t1*(-74 + 69*t2)) + 
               Power(s1,3)*(436 - 132*Power(t1,3) + 639*t2 - 
                  357*Power(t2,2) - 21*Power(t2,3) + 
                  Power(t1,2)*(923 + 458*t2) + 
                  t1*(-825 - 1213*t2 + 128*Power(t2,2))) + 
               Power(s1,2)*(422 + 184*Power(t1,4) - 2933*t2 - 
                  808*Power(t2,2) + 136*Power(t2,3) + 21*Power(t2,4) + 
                  Power(t1,3)*(-575 + 18*t2) - 
                  Power(t1,2)*(1034 + 1487*t2 + 498*Power(t2,2)) + 
                  t1*(858 + 3647*t2 + 2035*Power(t2,2) + 45*Power(t2,3))) \
- s1*(467 + 26*Power(t1,5) - 135*t2 - 3102*Power(t2,2) - 
                  321*Power(t2,3) - 63*Power(t2,4) + 7*Power(t2,5) + 
                  3*Power(t1,4)*(194 + 55*t2) - 
                  Power(t1,3)*(2788 + 2013*t2 + 152*Power(t2,2)) + 
                  Power(t1,2)*
                   (1729 + 2405*t2 + 114*Power(t2,2) - 128*Power(t2,3)) + 
                  t1*(323 - 1075*t2 + 2861*Power(t2,2) + 
                     1130*Power(t2,3) + 82*Power(t2,4)))) - 
            Power(s,3)*(-122 + 8*Power(s1,6)*(-1 + t1) - 22*Power(t1,6) + 
               1073*t2 - 2620*Power(t2,2) - 741*Power(t2,3) + 
               254*Power(t2,4) - 174*Power(t2,5) + 18*Power(t2,6) + 
               Power(s1,5)*(-129 - 86*Power(t1,2) + t1*(189 - 39*t2) + 
                  61*t2) + Power(t1,5)*(106 - 39*t2 - 14*Power(t2,2)) + 
               Power(t1,4)*(1139 + 2365*t2 + 454*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(t1,3)*(-2393 - 3156*t2 - 2255*Power(t2,2) + 
                  13*Power(t2,3) + 16*Power(t2,4)) - 
               Power(t1,2)*(213 - 3125*t2 - 187*Power(t2,2) + 
                  1645*Power(t2,3) + 304*Power(t2,4) + 15*Power(t2,5)) + 
               t1*(1373 - 3284*t2 + 4061*Power(t2,2) + 
                  2542*Power(t2,3) + 153*Power(t2,4) + 60*Power(t2,5)) + 
               Power(s1,4)*(162 + 12*Power(t1,3) + 758*t2 - 
                  121*Power(t2,2) + Power(t1,2)*(278 + 383*t2) + 
                  16*t1*(-22 - 73*t2 + 3*Power(t2,2))) + 
               Power(s1,3)*(716 + 196*Power(t1,4) - 1479*t2 - 
                  1159*Power(t2,2) + 59*Power(t2,3) - 
                  3*Power(t1,3)*(497 + 82*t2) + 
                  Power(t1,2)*(1880 + 386*t2 - 502*Power(t2,2)) + 
                  t1*(-1113 + 780*t2 + 2029*Power(t2,2) + 10*Power(t2,3))\
) + Power(s1,2)*(-111 - 64*Power(t1,5) - 3866*t2 + 2502*Power(t2,2) + 
                  386*Power(t2,3) + 59*Power(t2,4) - 
                  27*Power(t1,4)*(11 + 10*t2) + 
                  Power(t1,3)*(4332 + 3419*t2 + 408*Power(t2,2)) + 
                  Power(t1,2)*
                   (-6300 - 8851*t2 - 1977*Power(t2,2) + 
                     184*Power(t2,3)) + 
                  t1*(2164 + 9400*t2 + 7*Power(t2,2) - 
                     1250*Power(t2,3) - 48*Power(t2,4))) + 
               s1*(-1096 + 3771*t2 + 3909*Power(t2,2) - 
                  1439*Power(t2,3) + 318*Power(t2,4) - 68*Power(t2,5) + 
                  Power(t1,5)*(453 + 70*t2) + 
                  3*Power(t1,4)*(-785 - 280*t2 + 21*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (-72 + 1107*t2 + 781*Power(t2,2) + 95*Power(t2,3)) + 
                  Power(t1,2)*
                   (3839 + 7746*t2 + 8117*Power(t2,2) + 
                     1617*Power(t2,3) + 36*Power(t2,4)) + 
                  t1*(-785 - 8242*t2 - 10709*Power(t2,2) - 
                     588*Power(t2,3) + 140*Power(t2,4) + 21*Power(t2,5)))) \
+ Power(s,2)*(219 - 8*Power(s1,6)*(4 - 7*t1 + 3*Power(t1,2)) - 441*t2 - 
               2254*Power(t2,2) + 276*Power(t2,3) + 6*Power(t2,4) - 
               374*Power(t2,5) + 60*Power(t2,6) + 
               2*Power(t1,6)*(-29 + 9*t2) + 
               Power(t1,5)*(-912 - 1050*t2 - 91*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,4)*(2188 + 615*t2 + 311*Power(t2,2) - 
                  145*Power(t2,3) - 10*Power(t2,4)) + 
               Power(t1,3)*(507 - 572*t2 + 3392*Power(t2,2) + 
                  1950*Power(t2,3) + 188*Power(t2,4) + 5*Power(t2,5)) + 
               Power(t1,2)*(-3057 + 2586*t2 - 8896*Power(t2,2) - 
                  3993*Power(t2,3) - 414*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(1113 - 1124*t2 + 7462*Power(t2,2) + 
                  2008*Power(t2,3) + 204*Power(t2,4) + 343*Power(t2,5) - 
                  54*Power(t2,6)) + 
               Power(s1,5)*(-83 + 66*Power(t1,3) + t1*(239 - 353*t2) + 
                  219*t2 + 9*Power(t1,2)*(-23 + 13*t2)) + 
               Power(s1,4)*(533 + 84*Power(t1,4) + 427*t2 - 
                  461*Power(t2,2) - Power(t1,3)*(872 + 321*t2) + 
                  Power(t1,2)*(1813 + 1436*t2 - 186*Power(t2,2)) + 
                  t1*(-1502 - 1664*t2 + 723*Power(t2,2))) + 
               Power(s1,3)*(155 - 76*Power(t1,5) + 
                  Power(t1,4)*(615 - 130*t2) - 3597*t2 - 
                  242*Power(t2,2) + 329*Power(t2,3) + 
                  Power(t1,3)*(39 + 1844*t2 + 488*Power(t2,2)) + 
                  t1*(1042 + 7493*t2 + 2819*Power(t2,2) - 
                     557*Power(t2,3)) + 
                  3*Power(t1,2)*
                   (-593 - 1939*t2 - 923*Power(t2,2) + 32*Power(t2,3))) + 
               Power(s1,2)*(-566 - 68*t2 + 5248*Power(t2,2) - 
                  839*Power(t2,3) + 69*Power(t2,4) + 
                  11*Power(t1,5)*(49 + 12*t2) + 
                  Power(t1,4)*(-4420 - 1959*t2 + 9*Power(t2,2)) + 
                  Power(t1,3)*
                   (7787 + 5275*t2 - 611*Power(t2,2) - 272*Power(t2,3)) \
+ Power(t1,2)*(-4055 - 5570*t2 + 4848*Power(t2,2) + 2070*Power(t2,3) + 
                     18*Power(t2,4)) + 
                  t1*(707 + 2259*t2 - 9270*Power(t2,2) - 
                     1259*Power(t2,3) + 23*Power(t2,4))) - 
               s1*(703 + 106*Power(t1,6) - 4008*t2 + 252*Power(t2,2) + 
                  2190*Power(t2,3) - 1111*Power(t2,4) + 184*Power(t2,5) + 
                  Power(t1,5)*(-546 + 133*t2 + 60*Power(t2,2)) - 
                  Power(t1,4)*
                   (3014 + 4754*t2 + 1273*Power(t2,2) + 47*Power(t2,3)) + 
                  Power(t1,3)*
                   (8336 + 13964*t2 + 7076*Power(t2,2) + 
                     549*Power(t2,3) - 34*Power(t2,4)) + 
                  t1*(-682 + 11571*t2 + 5716*Power(t2,2) - 
                     3075*Power(t2,3) + 478*Power(t2,4) - 162*Power(t2,5)\
) + Power(t1,2)*(-4871 - 16990*t2 - 11665*Power(t2,2) + 430*Power(t2,3) + 
                     542*Power(t2,4) + 21*Power(t2,5)))) - 
            (-1 + t1)*(84 + 8*Power(s1,6)*
                (-1 + 4*t1 - 4*Power(t1,2) + Power(t1,3)) - 319*t2 - 
               15*Power(t2,2) + 97*Power(t2,3) - 221*Power(t2,4) - 
               70*Power(t2,5) + 24*Power(t2,6) + 
               Power(t1,6)*(42 + 20*t2) + 
               Power(t1,5)*(-115 + 274*t2 + 82*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,4)*(75 - 364*t2 - 955*Power(t2,2) - 
                  256*Power(t2,3) + 8*Power(t2,4)) + 
               Power(t1,3)*(-139 - 229*t2 + 2318*Power(t2,2) + 
                  918*Power(t2,3) + 126*Power(t2,4) - 30*Power(t2,5)) + 
               t1*(-334 + 615*t2 + 592*Power(t2,2) + 456*Power(t2,3) + 
                  482*Power(t2,4) + 119*Power(t2,5) - 42*Power(t2,6)) + 
               Power(t1,2)*(387 + 3*t2 - 2022*Power(t2,2) - 
                  1228*Power(t2,3) - 383*Power(t2,4) - 26*Power(t2,5) + 
                  18*Power(t2,6)) + 
               Power(s1,5)*(37 + 10*Power(t1,4) + 57*t2 - 
                  13*Power(t1,3)*(5 + 3*t2) + 
                  24*Power(t1,2)*(5 + 8*t2) - 5*t1*(19 + 42*t2)) + 
               Power(s1,4)*(50 + Power(t1,4)*(64 - 31*t2) - 211*t2 - 
                  118*Power(t2,2) + 
                  Power(t1,3)*(-209 + 197*t2 + 69*Power(t2,2)) - 
                  4*Power(t1,2)*(-67 + 103*t2 + 103*Power(t2,2)) + 
                  t1*(-161 + 422*t2 + 461*Power(t2,2))) + 
               Power(s1,3)*(-79 + 62*Power(t1,5) - 426*t2 + 
                  501*Power(t2,2) + 68*Power(t2,3) + 
                  Power(t1,4)*(-546 - 193*t2 + 32*Power(t2,2)) + 
                  t1*(482 + 1332*t2 - 866*Power(t2,2) - 
                     401*Power(t2,3)) + 
                  Power(t1,3)*
                   (1349 + 889*t2 - 186*Power(t2,2) - 53*Power(t2,3)) + 
                  Power(t1,2)*
                   (-1259 - 1650*t2 + 589*Power(t2,2) + 386*Power(t2,3))) \
+ Power(s1,2)*(-216 + Power(t1,5)*(3 - 62*t2) + 775*t2 + 
                  425*Power(t2,2) - 587*Power(t2,3) + 54*Power(t2,4) + 
                  Power(t1,4)*
                   (422 + 690*t2 + 156*Power(t2,2) - 11*Power(t2,3)) + 
                  Power(t1,2)*
                   (1038 + 2726*t2 + 1813*Power(t2,2) - 
                     448*Power(t2,3) - 126*Power(t2,4)) + 
                  Power(t1,3)*
                   (-1254 - 2115*t2 - 836*Power(t2,2) + 
                     11*Power(t2,3) + 15*Power(t2,4)) + 
                  t1*(7 - 2041*t2 - 1486*Power(t2,2) + 965*Power(t2,3) + 
                     57*Power(t2,4))) + 
               s1*(175 - 8*Power(t1,6) + 227*t2 - 771*Power(t2,2) + 
                  172*Power(t2,3) + 330*Power(t2,4) - 77*Power(t2,5) - 
                  2*Power(t1,5)*(162 + 101*t2 + 2*Power(t2,2)) + 
                  Power(t1,4)*
                   (523 + 766*t2 + 181*Power(t2,2) - 35*Power(t2,3)) + 
                  Power(t1,3)*
                   (292 - 1189*t2 - 365*Power(t2,2) + 30*Power(t2,3) + 
                     73*Power(t2,4)) + 
                  Power(t1,2)*
                   (-610 + 999*t2 + 2*Power(t2,2) - 48*Power(t2,3) + 
                     177*Power(t2,4) - 26*Power(t2,5)) + 
                  t1*(-48 - 601*t2 + 984*Power(t2,2) - 167*Power(t2,3) - 
                     545*Power(t2,4) + 103*Power(t2,5)))) - 
            s*(303 + 24*Power(t1,7) + 
               8*Power(s1,6)*(-4 + 12*t1 - 11*Power(t1,2) + 
                  3*Power(t1,3)) - 865*t2 - 602*Power(t2,2) + 
               432*Power(t2,3) - 415*Power(t2,4) - 287*Power(t2,5) + 
               66*Power(t2,6) + 
               Power(t1,6)*(285 + 202*t2 + 8*Power(t2,2)) + 
               Power(t1,5)*(-838 + 649*t2 + 214*Power(t2,2) + 
                  37*Power(t2,3)) - 
               Power(t1,4)*(181 + 1182*t2 + 3243*Power(t2,2) + 
                  1037*Power(t2,3) + 19*Power(t2,4)) + 
               Power(t1,3)*(1726 - 1013*t2 + 8403*Power(t2,2) + 
                  3288*Power(t2,3) + 374*Power(t2,4) - 68*Power(t2,5)) + 
               t1*(-496 + 1172*t2 + 3633*Power(t2,2) + 788*Power(t2,3) + 
                  1020*Power(t2,4) + 519*Power(t2,5) - 120*Power(t2,6)) + 
               Power(t1,2)*(-823 + 1037*t2 - 8413*Power(t2,2) - 
                  3511*Power(t2,3) - 960*Power(t2,4) - 165*Power(t2,5) + 
                  54*Power(t2,6)) + 
               Power(s1,5)*(39 - 2*Power(t1,4) + 215*t2 - 
                  Power(t1,3)*(25 + 117*t2) + Power(t1,2)*(75 + 523*t2) - 
                  t1*(86 + 621*t2)) + 
               Power(s1,4)*(330 - 44*Power(t1,5) - 258*t2 - 
                  451*Power(t2,2) + 9*Power(t1,4)*(62 + 5*t2) + 
                  Power(t1,2)*(1928 + 297*t2 - 1083*Power(t2,2)) + 
                  Power(t1,3)*(-1577 - 264*t2 + 200*Power(t2,2)) + 
                  t1*(-1195 + 175*t2 + 1334*Power(t2,2))) + 
               Power(s1,3)*(-161 - 2281*t2 + 916*Power(t2,2) + 
                  317*Power(t2,3) + Power(t1,5)*(123 + 106*t2) - 
                  Power(t1,4)*(1633 + 1514*t2 + 107*Power(t2,2)) + 
                  t1*(1803 + 7174*t2 - 793*Power(t2,2) - 
                     1116*Power(t2,3)) + 
                  Power(t1,3)*
                   (4495 + 5480*t2 + 879*Power(t2,2) - 138*Power(t2,3)) + 
                  Power(t1,2)*
                   (-4624 - 8965*t2 - 885*Power(t2,2) + 937*Power(t2,3))) \
+ Power(s1,2)*(-586 - 146*Power(t1,6) + 1884*t2 + 2928*Power(t2,2) - 
                  1501*Power(t2,3) + 85*Power(t2,4) + 
                  Power(t1,5)*(1328 + 99*t2 - 78*Power(t2,2)) + 
                  Power(t1,4)*
                   (-2130 + 763*t2 + 1133*Power(t2,2) + 87*Power(t2,3)) + 
                  Power(t1,2)*
                   (2034 + 6841*t2 + 9915*Power(t2,2) + 
                     414*Power(t2,3) - 223*Power(t2,4)) + 
                  Power(t1,3)*
                   (-393 - 4066*t2 - 5039*Power(t2,2) - 
                     934*Power(t2,3) + 24*Power(t2,4)) + 
                  t1*(-107 - 5530*t2 - 8859*Power(t2,2) + 
                     1924*Power(t2,3) + 114*Power(t2,4))) + 
               s1*(66 + 1607*t2 - 2070*Power(t2,2) - 562*Power(t2,3) + 
                  1091*Power(t2,4) - 200*Power(t2,5) + 
                  Power(t1,6)*(43 + 80*t2) + 
                  Power(t1,5)*
                   (-2144 - 2035*t2 - 213*Power(t2,2) + 16*Power(t2,3)) + 
                  Power(t1,4)*
                   (5376 + 7138*t2 + 2017*Power(t2,2) - 158*Power(t2,3) - 
                     23*Power(t2,4)) + 
                  Power(t1,2)*
                   (-569 + 8989*t2 + 2143*Power(t2,2) - 
                     1918*Power(t2,3) + 264*Power(t2,4) - 120*Power(t2,5)) \
+ Power(t1,3)*(-3534 - 10622*t2 - 4356*Power(t2,2) + 762*Power(t2,3) + 
                     412*Power(t2,4) + 7*Power(t2,5)) + 
                  t1*(762 - 5157*t2 + 2488*Power(t2,2) + 
                     1860*Power(t2,3) - 1739*Power(t2,4) + 313*Power(t2,5))\
)))))/(Power(-1 + s2,2)*s2*(-1 + t1)*Power(-1 + s + t1,2)*
       (-1 + s - s2 + t1)*(s - s2 + t1)*(-1 + s1 + t1 - t2)*(-1 + t2)*
       (-1 + s - s*s2 + s1*s2 + t1 - s2*t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) + 
    (8*(28 - 12*s2 - 15*Power(s2,2) - 5*Power(s2,3) - 118*t1 + 58*s2*t1 + 
         68*Power(s2,2)*t1 + 24*Power(s2,3)*t1 + 187*Power(t1,2) - 
         117*s2*Power(t1,2) - 128*Power(s2,2)*Power(t1,2) - 
         46*Power(s2,3)*Power(t1,2) - 128*Power(t1,3) + 
         128*s2*Power(t1,3) + 132*Power(s2,2)*Power(t1,3) + 
         44*Power(s2,3)*Power(t1,3) + 22*Power(t1,4) - 
         82*s2*Power(t1,4) - 83*Power(s2,2)*Power(t1,4) - 
         21*Power(s2,3)*Power(t1,4) + 14*Power(t1,5) + 
         30*s2*Power(t1,5) + 32*Power(s2,2)*Power(t1,5) + 
         4*Power(s2,3)*Power(t1,5) - 5*Power(t1,6) - 5*s2*Power(t1,6) - 
         6*Power(s2,2)*Power(t1,6) + 
         Power(s1,7)*Power(s2,2)*(Power(s2,2) + s2*(-3 + t1) + t1) + 
         2*Power(s,6)*Power(-1 + s2,2)*Power(t1 - t2,2) - 26*t2 + 
         98*s2*t2 - 39*Power(s2,2)*t2 - 58*Power(s2,3)*t2 - 
         15*Power(s2,4)*t2 + 102*t1*t2 - 316*s2*t1*t2 + 
         157*Power(s2,2)*t1*t2 + 210*Power(s2,3)*t1*t2 + 
         57*Power(s2,4)*t1*t2 - 167*Power(t1,2)*t2 + 
         340*s2*Power(t1,2)*t2 - 268*Power(s2,2)*Power(t1,2)*t2 - 
         300*Power(s2,3)*Power(t1,2)*t2 - 81*Power(s2,4)*Power(t1,2)*t2 + 
         145*Power(t1,3)*t2 - 104*s2*Power(t1,3)*t2 + 
         252*Power(s2,2)*Power(t1,3)*t2 + 
         220*Power(s2,3)*Power(t1,3)*t2 + 51*Power(s2,4)*Power(t1,3)*t2 - 
         63*Power(t1,4)*t2 - 38*s2*Power(t1,4)*t2 - 
         133*Power(s2,2)*Power(t1,4)*t2 - 90*Power(s2,3)*Power(t1,4)*t2 - 
         12*Power(s2,4)*Power(t1,4)*t2 + 5*Power(t1,5)*t2 + 
         20*s2*Power(t1,5)*t2 + 31*Power(s2,2)*Power(t1,5)*t2 + 
         18*Power(s2,3)*Power(t1,5)*t2 + 4*Power(t1,6)*t2 + 
         8*Power(t2,2) - 79*s2*Power(t2,2) + 
         123*Power(s2,2)*Power(t2,2) - 62*Power(s2,3)*Power(t2,2) - 
         83*Power(s2,4)*Power(t2,2) - 15*Power(s2,5)*Power(t2,2) + 
         14*t1*Power(t2,2) + 210*s2*t1*Power(t2,2) - 
         279*Power(s2,2)*t1*Power(t2,2) + 
         196*Power(s2,3)*t1*Power(t2,2) + 
         223*Power(s2,4)*t1*Power(t2,2) + 42*Power(s2,5)*t1*Power(t2,2) - 
         66*Power(t1,2)*Power(t2,2) - 226*s2*Power(t1,2)*Power(t2,2) + 
         151*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         267*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         215*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         39*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         46*Power(t1,3)*Power(t2,2) + 126*s2*Power(t1,3)*Power(t2,2) + 
         43*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         194*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         93*Power(s2,4)*Power(t1,3)*Power(t2,2) + 
         12*Power(s2,5)*Power(t1,3)*Power(t2,2) + 
         10*Power(t1,4)*Power(t2,2) - 19*s2*Power(t1,4)*Power(t2,2) - 
         38*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         61*Power(s2,3)*Power(t1,4)*Power(t2,2) - 
         18*Power(s2,4)*Power(t1,4)*Power(t2,2) - 
         12*Power(t1,5)*Power(t2,2) - 12*s2*Power(t1,5)*Power(t2,2) - 
         20*Power(t2,3) + 15*s2*Power(t2,3) - 
         73*Power(s2,2)*Power(t2,3) + 81*Power(s2,3)*Power(t2,3) - 
         55*Power(s2,4)*Power(t2,3) - 51*Power(s2,5)*Power(t2,3) - 
         5*Power(s2,6)*Power(t2,3) + 25*t1*Power(t2,3) + 
         60*s2*t1*Power(t2,3) + 97*Power(s2,2)*t1*Power(t2,3) - 
         103*Power(s2,3)*t1*Power(t2,3) + 
         123*Power(s2,4)*t1*Power(t2,3) + 89*Power(s2,5)*t1*Power(t2,3) + 
         9*Power(s2,6)*t1*Power(t2,3) + 17*Power(t1,2)*Power(t2,3) - 
         116*s2*Power(t1,2)*Power(t2,3) - 
         57*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         23*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         117*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         44*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         4*Power(s2,6)*Power(t1,2)*Power(t2,3) - 
         34*Power(t1,3)*Power(t2,3) + 5*s2*Power(t1,3)*Power(t2,3) + 
         21*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         45*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         49*Power(s2,4)*Power(t1,3)*Power(t2,3) + 
         6*Power(s2,5)*Power(t1,3)*Power(t2,3) + 
         12*Power(t1,4)*Power(t2,3) + 36*s2*Power(t1,4)*Power(t2,3) + 
         12*Power(s2,2)*Power(t1,4)*Power(t2,3) + 12*Power(t2,4) - 
         44*s2*Power(t2,4) + 15*Power(s2,2)*Power(t2,4) - 
         16*Power(s2,3)*Power(t2,4) + 32*Power(s2,4)*Power(t2,4) - 
         26*Power(s2,5)*Power(t2,4) - 11*Power(s2,6)*Power(t2,4) - 
         24*t1*Power(t2,4) + 19*s2*t1*Power(t2,4) + 
         63*Power(s2,2)*t1*Power(t2,4) - 9*Power(s2,3)*t1*Power(t2,4) - 
         Power(s2,4)*t1*Power(t2,4) + 31*Power(s2,5)*t1*Power(t2,4) + 
         9*Power(s2,6)*t1*Power(t2,4) + 15*Power(t1,2)*Power(t2,4) + 
         55*s2*Power(t1,2)*Power(t2,4) - 
         39*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         4*Power(s2,3)*Power(t1,2)*Power(t2,4) - 
         31*Power(s2,4)*Power(t1,2)*Power(t2,4) - 
         14*Power(s2,5)*Power(t1,2)*Power(t2,4) - 
         4*Power(t1,3)*Power(t2,4) - 36*s2*Power(t1,3)*Power(t2,4) - 
         36*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         4*Power(s2,3)*Power(t1,3)*Power(t2,4) - 2*Power(t2,5) + 
         23*s2*Power(t2,5) - 33*Power(s2,2)*Power(t2,5) + 
         8*Power(s2,3)*Power(t2,5) + 2*Power(s2,4)*Power(t2,5) + 
         5*Power(s2,5)*Power(t2,5) - 5*Power(s2,6)*Power(t2,5) + 
         t1*Power(t2,5) - 31*s2*t1*Power(t2,5) - 
         11*Power(s2,2)*t1*Power(t2,5) + 24*Power(s2,3)*t1*Power(t2,5) - 
         2*Power(s2,4)*t1*Power(t2,5) + 9*Power(s2,5)*t1*Power(t2,5) + 
         12*s2*Power(t1,2)*Power(t2,5) + 
         36*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
         12*Power(s2,3)*Power(t1,2)*Power(t2,5) - s2*Power(t2,6) + 
         16*Power(s2,2)*Power(t2,6) - 10*Power(s2,3)*Power(t2,6) - 
         Power(s2,5)*Power(t2,6) - 12*Power(s2,2)*t1*Power(t2,6) - 
         12*Power(s2,3)*t1*Power(t2,6) + 4*Power(s2,3)*Power(t2,7) + 
         Power(s1,6)*s2*(3*Power(s2,4) + 2*(-1 + t1)*t1 + 
            Power(s2,3)*(-5 + t1 - 6*t2) + 
            Power(s2,2)*(-19 + 2*Power(t1,2) + 21*t2 - t1*(4 + 5*t2)) + 
            s2*(9 + 4*Power(t1,2) - t2 - t1*(11 + 5*t2))) + 
         Power(s,5)*(-1 + s2)*
          (-4 + 2*t1 + 6*Power(t1,2) + 3*Power(t1,3) - 6*t2 - 16*t1*t2 - 
            13*Power(t1,2)*t2 + 10*Power(t2,2) + 17*t1*Power(t2,2) - 
            7*Power(t2,3) + 2*s1*(-1 + s2)*
             (-2 - 3*t1 - 4*Power(t1,2) + t2 + 6*t1*t2 - 
               2*Power(t2,2) + s2*(2 + t1 + t2)) - 
            2*Power(s2,2)*(2 + 3*Power(t1,2) + 5*t2 + 3*Power(t2,2) - 
               3*t1*(1 + 2*t2)) + 
            s2*(8 + 3*Power(t1,3) + 16*t2 - 5*Power(t1,2)*t2 - 
               4*Power(t2,2) + Power(t2,3) + t1*(-8 + 4*t2 + Power(t2,2))\
)) + Power(s1,5)*(4*Power(s2,6) + t1*(1 - 2*t1 + 2*Power(t1,2)) + 
            Power(s2,5)*(-14 + 2*t1 - 15*t2) + 
            s2*(-9 + 4*Power(t1,3) + 2*t2 + t1*(19 + 6*t2) - 
               Power(t1,2)*(16 + 7*t2)) - 
            Power(s2,4)*(Power(t1,2) + 2*t1*(-5 + t2) - 
               3*(-5 + 7*t2 + 5*Power(t2,2))) + 
            Power(s2,3)*(15 + Power(t1,3) + 107*t2 - 64*Power(t2,2) - 
               2*Power(t1,2)*(5 + 3*t2) + 
               t1*(-54 + 27*t2 + 10*Power(t2,2))) + 
            Power(s2,2)*(51 + 5*Power(t1,3) - 55*t2 + 5*Power(t2,2) - 
               3*Power(t1,2)*(7 + 5*t2) + 
               t1*(-36 + 57*t2 + 10*Power(t2,2)))) + 
         Power(s1,4)*(3 - Power(t1,4) + 
            Power(s2,6)*(-10 + 8*t1 - 21*t2) - t2 - t1*(9 + t2) + 
            Power(t1,2)*(12 + t2) - 2*Power(t1,3)*(3 + 2*t2) + 
            Power(s2,5)*(-14 - 5*Power(t1,2) - t1*(-10 + t2) + 63*t2 + 
               29*Power(t2,2)) - 
            Power(s2,4)*(-59 + Power(t1,3) - 73*t2 - 
               12*Power(t1,2)*t2 + 33*Power(t2,2) + 20*Power(t2,3) + 
               t1*(58 + 61*t2 + 3*Power(t2,2))) + 
            s2*(-49 + 5*Power(t1,4) + 47*t2 - 8*Power(t2,2) - 
               2*Power(t1,3)*(11 + 5*t2) + 
               t1*(84 - 83*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*(-24 + 58*t2 + 9*Power(t2,2))) + 
            Power(s2,2)*(-15 + 2*Power(t1,4) - 237*t2 + 
               146*Power(t2,2) - 10*Power(t2,3) - 
               Power(t1,3)*(46 + 9*t2) + 
               Power(t1,2)*(-71 + 93*t2 + 21*Power(t2,2)) + 
               t1*(133 + 149*t2 - 131*Power(t2,2) - 10*Power(t2,3))) + 
            Power(s2,3)*(54 + Power(t1,3)*(-17 + t2) - 44*t2 - 
               246*Power(t2,2) + 110*Power(t2,3) + 
               2*Power(t1,2)*(7 + 27*t2 + 3*Power(t2,2)) - 
               t1*(84 - 225*t2 + 84*Power(t2,2) + 10*Power(t2,3)))) + 
         Power(s1,3)*(16 + 2*Power(t1,5) + Power(t1,4)*(-6 + t2) - 
            13*t2 + 3*Power(t2,2) + 
            Power(t1,3)*(-11 + 22*t2 + 2*Power(t2,2)) + 
            t1*(-44 + 31*t2 + 2*Power(t2,2)) + 
            Power(t1,2)*(43 - 37*t2 + 3*Power(t2,2)) + 
            Power(s2,6)*(5 + 4*Power(t1,2) + 41*t2 + 44*Power(t2,2) - 
               3*t1*(3 + 11*t2)) - 
            Power(s2,5)*(-45 + 4*Power(t1,3) - 66*t2 + 
               110*Power(t2,2) + 26*Power(t2,3) - 
               Power(t1,2)*(34 + 27*t2) + 
               3*t1*(25 + 19*t2 + 6*Power(t2,2))) + 
            s2*(5 - Power(t1,5) + 177*t2 - 108*Power(t2,2) + 
               13*Power(t2,3) - Power(t1,4)*(55 + 6*t2) + 
               4*Power(t1,3)*(7 + 17*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(131 + 44*t2 - 92*Power(t2,2) - 
                  5*Power(t2,3)) + 
               t1*(-108 - 259*t2 + 164*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s2,4)*(30 - 217*t2 - 132*Power(t2,2) + 
               23*Power(t2,3) + 15*Power(t2,4) + 
               Power(t1,3)*(-28 + 11*t2) + 
               Power(t1,2)*(50 + 7*t2 - 31*Power(t2,2)) + 
               t1*(-52 + 199*t2 + 127*Power(t2,2) + 11*Power(t2,3))) + 
            Power(s2,3)*(-121 - 183*t2 + Power(t1,3)*(69 - 5*t2)*t2 + 
               30*Power(t2,2) + 296*Power(t2,3) - 115*Power(t2,4) + 
               Power(t1,4)*(-9 + 2*t2) - 
               Power(t1,2)*(80 + 109*t2 + 126*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(210 + 353*t2 - 363*Power(t2,2) + 142*Power(t2,3) + 
                  5*Power(t2,4))) + 
            Power(s2,2)*(-102 - Power(t1,5) + 29*t2 + 433*Power(t2,2) - 
               214*Power(t2,3) + 10*Power(t2,4) + 
               Power(t1,4)*(-38 + 5*t2) + 
               Power(t1,3)*(53 + 152*t2 + 3*Power(t2,2)) - 
               Power(t1,2)*(184 - 262*t2 + 206*Power(t2,2) + 
                  13*Power(t2,3)) + 
               t1*(272 - 460*t2 - 204*Power(t2,2) + 173*Power(t2,3) + 
                  5*Power(t2,4)))) - 
         Power(s1,2)*(2*Power(t1,5)*(11 + t2) - 
            5*Power(t1,4)*(10 + 3*t2) + 
            t2*(41 - 26*t2 + 5*Power(t2,2)) + 
            Power(t1,3)*(6 + 8*t2 + 24*Power(t2,2)) + 
            Power(t1,2)*(50 + 47*t2 - 41*Power(t2,2) + Power(t2,3)) + 
            t1*(-28 - 83*t2 + 49*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s2,6)*t2*(15 + 12*Power(t1,2) + 63*t2 + 
               46*Power(t2,2) - 3*t1*(9 + 17*t2)) - 
            Power(s2,5)*(-15 - 140*t2 - 116*Power(t2,2) + 
               92*Power(t2,3) + 9*Power(t2,4) + 
               Power(t1,3)*(12 + 13*t2) - 
               Power(t1,2)*(39 + 109*t2 + 53*Power(t2,2)) + 
               t1*(42 + 236*t2 + 115*Power(t2,2) + 40*Power(t2,3))) - 
            s2*(87 - 10*t2 - 248*Power(t2,2) + 134*Power(t2,3) - 
               11*Power(t2,4) + Power(t1,5)*(-31 + 4*t2) + 
               Power(t1,4)*(82 + 126*t2 + Power(t2,2)) + 
               t1*(-332 + 335*t2 + 272*Power(t2,2) - 186*Power(t2,3)) - 
               Power(t1,3)*(229 - 12*t2 + 121*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,2)*(423 - 467*t2 + 60*Power(t2,2) + 
                  86*Power(t2,3) + Power(t2,4))) + 
            Power(s2,4)*(75 + Power(t1,4)*(13 - 3*t2) + 108*t2 - 
               288*Power(t2,2) - 107*Power(t2,3) + 6*Power(t2,4) + 
               6*Power(t2,5) + 
               Power(t1,3)*(-70 - 89*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(176 + 187*t2 + 48*Power(t2,2) - 
                  30*Power(t2,3)) + 
               t1*(-194 - 203*t2 + 222*Power(t2,2) + 113*Power(t2,3) + 
                  10*Power(t2,4))) + 
            Power(s2,3)*(46 + 3*Power(t1,5) - 343*t2 - 
               191*Power(t2,2) - 20*Power(t2,3) + 197*Power(t2,4) - 
               73*Power(t2,5) + 
               2*Power(t1,4)*(18 - 18*t2 + Power(t2,2)) + 
               Power(t1,3)*(-121 + 29*t2 + 100*Power(t2,2) - 
                  3*Power(t2,3)) - 
               Power(t1,2)*(-168 + 251*t2 + 187*Power(t2,2) + 
                  142*Power(t2,3)) + 
               t1*(-132 + 601*t2 + 474*Power(t2,2) - 291*Power(t2,3) + 
                  132*Power(t2,4) + Power(t2,5))) - 
            Power(s2,2)*(149 + 2*Power(t1,5)*(-9 + t2) + 181*t2 + 
               6*Power(t2,2) - 392*Power(t2,3) + 181*Power(t2,4) - 
               5*Power(t2,5) + 
               Power(t1,4)*(31 + 98*t2 - 7*Power(t2,2)) + 
               Power(t1,3)*(-82 - 165*t2 - 233*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,2)*(292 + 491*t2 - 311*Power(t2,2) + 
                  253*Power(t2,3) + 3*Power(t2,4)) - 
               t1*(372 + 607*t2 - 563*Power(t2,2) - 94*Power(t2,3) + 
                  138*Power(t2,4) + Power(t2,5)))) + 
         s1*(-27 - 9*Power(t1,6) + 4*t2 + 46*Power(t2,2) - 
            28*Power(t2,3) + 5*Power(t2,4) + 13*Power(t1,5)*(3 + 2*t2) - 
            4*Power(t1,4)*(29 + 4*t2 + 5*Power(t2,2)) + 
            Power(t1,3)*(231 - 136*t2 + 49*Power(t2,2) + 
               12*Power(t2,3)) - 
            Power(t1,2)*(252 - 220*t2 + 7*Power(t2,2) + 
               31*Power(t2,3) + Power(t2,4)) + 
            t1*(134 - 98*t2 - 68*Power(t2,2) + 51*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,6)*Power(t2,2)*
             (15 + 12*Power(t1,2) + 43*t2 + 24*Power(t2,2) - 
               t1*(27 + 35*t2)) + 
            Power(s2,5)*t2*(30 + 146*t2 + 90*Power(t2,2) - 
               36*Power(t2,3) + Power(t2,4) - 
               3*Power(t1,3)*(8 + 5*t2) + 
               Power(t1,2)*(78 + 119*t2 + 45*Power(t2,2)) - 
               t1*(84 + 250*t2 + 99*Power(t2,2) + 32*Power(t2,3))) - 
            s2*(101 + 9*Power(t1,6) + 30*t2 + 11*Power(t2,2) - 
               164*Power(t2,3) + 87*Power(t2,4) - 5*Power(t2,5) - 
               2*Power(t1,5)*(12 + 23*t2) + 
               Power(t1,4)*(51 + 97*t2 + 120*Power(t2,2)) - 
               Power(t1,3)*(200 + 209*t2 - 23*Power(t2,2) + 
                  111*Power(t2,3)) + 
               t1*(-336 - 213*t2 + 280*Power(t2,2) + 116*Power(t2,3) - 
                  117*Power(t2,4)) + 
               Power(t1,2)*(399 + 341*t2 - 434*Power(t2,2) + 
                  135*Power(t2,3) + 48*Power(t2,4))) + 
            Power(s2,4)*(15 + 157*t2 + 134*Power(t2,2) - 
               162*Power(t2,3) - 35*Power(t2,4) + Power(t2,6) - 
               2*Power(t1,4)*(-6 - 15*t2 + Power(t2,2)) + 
               Power(t1,3)*(-51 - 159*t2 - 114*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(81 + 385*t2 + 260*Power(t2,2) + 
                  72*Power(t2,3) - 10*Power(t2,4)) + 
               t1*(-57 - 413*t2 - 278*Power(t2,2) + 82*Power(t2,3) + 
                  39*Power(t2,4) + 3*Power(t2,5))) + 
            Power(s2,2)*(38 - 2*Power(t1,6) - 287*t2 - 9*Power(t2,2) - 
               35*Power(t2,3) + 178*Power(t2,4) - 83*Power(t2,5) + 
               Power(t2,6) + 2*Power(t1,5)*(-9 + 13*t2) + 
               Power(t1,4)*(100 - 40*t2 - 79*Power(t2,2)) + 
               Power(t1,3)*(-210 + 147*t2 + 109*Power(t2,2) + 
                  163*Power(t2,3)) + 
               Power(t1,2)*(240 - 565*t2 - 272*Power(t2,2) + 
                  159*Power(t2,3) - 155*Power(t2,4)) + 
               t1*(-148 + 719*t2 + 251*Power(t2,2) - 299*Power(t2,3) + 
                  8*Power(t2,4) + 62*Power(t2,5))) + 
            Power(s2,3)*(55 + 102*t2 - 301*Power(t2,2) - 
               46*Power(t2,3) - 29*Power(t2,4) + 69*Power(t2,5) - 
               26*Power(t2,6) + Power(t1,5)*(-15 + 4*t2) + 
               Power(t1,4)*(75 + 87*t2 - 25*Power(t2,2)) + 
               Power(t1,3)*(-190 - 285*t2 - 24*Power(t2,2) + 
                  52*Power(t2,3)) + 
               Power(t1,2)*(270 + 395*t2 - 136*Power(t2,2) - 
                  88*Power(t2,3) - 72*Power(t2,4)) + 
               t1*(-195 - 303*t2 + 486*Power(t2,2) + 214*Power(t2,3) - 
                  123*Power(t2,4) + 63*Power(t2,5)))) + 
         Power(s,4)*(-4 + 9*t1 - 4*Power(t1,2) + 16*Power(t1,3) - 
            6*Power(t1,4) + Power(s1,3)*Power(-1 + s2,2)*
             (4 + t1 + s2*(-4 + t1 - t2) - t2) - 17*t2 - 30*t1*t2 - 
            42*Power(t1,2)*t2 + 10*Power(t1,3)*t2 + 42*Power(t2,2) + 
            52*t1*Power(t2,2) + 6*Power(t1,2)*Power(t2,2) - 
            26*Power(t2,3) - 18*t1*Power(t2,3) + 8*Power(t2,4) - 
            Power(s2,4)*(Power(t1,3) - 2*Power(t1,2)*(4 + t2) - 
               t2*(11 + 7*t2) + t1*(5 + 13*t2 + Power(t2,2))) + 
            Power(s2,2)*(8 + 3*Power(t1,4) + Power(t1,3)*(8 - 10*t2) + 
               10*t2 + 39*Power(t2,2) - 9*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(8 + 7*t2 + 12*Power(t2,2)) - 
               t1*(16 + 21*t2 + 6*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s2,3)*(-6 - 30*t2 + Power(t2,2) + 15*Power(t2,3) + 
               2*Power(t2,4) - Power(t1,3)*(9 + 2*t2) + 
               Power(t1,2)*(-8 + 25*t2 + 6*Power(t2,2)) - 
               t1*(-20 + 5*t2 + 31*Power(t2,2) + 6*Power(t2,3))) + 
            s2*(2 + 3*Power(t1,4) + 2*Power(t1,3)*(-6 + t2) + 26*t2 - 
               91*Power(t2,2) + 18*Power(t2,3) - 11*Power(t2,4) + 
               Power(t1,2)*(-6 + 2*t2 - 24*Power(t2,2)) + 
               t1*(-8 + 73*t2 - 8*Power(t2,2) + 30*Power(t2,3))) + 
            Power(s1,2)*(8 + 13*Power(t1,2) + t1*(17 - 16*t2) - 15*t2 + 
               5*Power(t2,2) + 
               Power(s2,2)*(44 + 11*Power(t1,2) + t1*(51 - 8*t2) - 
                  45*t2 - Power(t2,2)) - 
               s2*(34 + 28*Power(t1,2) + t1*(51 - 34*t2) - 45*t2 + 
                  10*Power(t2,2)) + 
               Power(s2,3)*(-18 + 2*Power(t1,2) + 15*t2 + 
                  4*Power(t2,2) - t1*(17 + 6*t2))) - 
            s1*(6 - 11*Power(t1,3) + 9*t2 - 26*Power(t2,2) + 
               12*Power(t2,3) + 
               Power(s2,4)*(-2 + Power(t1,2) - t1*(-5 + t2) + 5*t2) + 
               Power(t1,2)*(-40 + 42*t2) + 
               t1*(-11 + 74*t2 - 43*Power(t2,2)) + 
               Power(s2,2)*(52 + 10*Power(t1,3) + 
                  Power(t1,2)*(3 - 11*t2) + 74*t2 - 50*Power(t2,2) + 
                  Power(t2,3) + t1*(-50 + 63*t2)) - 
               Power(s2,3)*(20 + Power(t1,3) + 
                  Power(t1,2)*(12 - 7*t2) + 40*t2 - 23*Power(t2,2) - 
                  5*Power(t2,3) + t1*(-12 + 15*t2 + 11*Power(t2,2))) + 
               s2*(-36 + 4*Power(t1,3) - 44*Power(t1,2)*(-1 + t2) - 
                  48*t2 + 49*Power(t2,2) - 20*Power(t2,3) + 
                  t1*(44 - 113*t2 + 60*Power(t2,2))))) + 
         Power(s,3)*(-25 + 72*t1 - 34*Power(t1,2) - 42*Power(t1,3) + 
            31*Power(t1,4) - 2*Power(t1,5) + 30*t2 + 67*t1*t2 + 
            10*Power(t1,2)*t2 - 25*Power(t1,3)*t2 - 5*Power(t1,4)*t2 - 
            93*Power(t2,2) - 34*t1*Power(t2,2) - 
            55*Power(t1,2)*Power(t2,2) + 21*Power(t1,3)*Power(t2,2) + 
            58*Power(t2,3) + 85*t1*Power(t2,3) - 
            17*Power(t1,2)*Power(t2,3) - 36*Power(t2,4) + 
            t1*Power(t2,4) + 2*Power(t2,5) - 
            Power(s1,4)*(-1 + s2)*
             (-4 + Power(s2,3) - 2*t1 + 
               Power(s2,2)*(-16 + 4*t1 - 3*t2) + 
               s2*(19 + 2*t1 - 2*t2) + t2) + 
            Power(s2,5)*(2*Power(t1,3) - 5*t2*(1 + t2) - 
               Power(t1,2)*(4 + 3*t2) + t1*(2 + 4*t2 + Power(t2,2))) + 
            Power(s2,4)*(1 + Power(t1,3)*(-1 + t2) - 26*t2 - 
               57*Power(t2,2) - 52*Power(t2,3) - 8*Power(t2,4) + 
               Power(t1,2)*(3 - 11*t2 - 10*Power(t2,2)) + 
               t1*(-3 + 51*t2 + 68*Power(t2,2) + 17*Power(t2,3))) - 
            Power(s2,3)*(-12 + 15*t2 + 61*Power(t2,2) + 
               11*Power(t2,3) - 21*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,4)*(3 + 2*t2) - 
               Power(t1,3)*(19 + 21*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(14 + 189*t2 + 36*Power(t2,2)) + 
               t1*(14 - 148*t2 - 181*Power(t2,2) + 3*Power(t2,3) + 
                  4*Power(t2,4))) + 
            s2*(76 + Power(t1,5) - 70*t2 + 136*Power(t2,2) - 
               131*Power(t2,3) + 33*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,4)*(-17 + 10*t2) - 
               Power(t1,3)*(30 + 63*t2 + 22*Power(t2,2)) + 
               2*Power(t1,2)*
                (102 + 58*t2 + 69*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-234 - 168*t2 + 69*Power(t2,2) - 91*Power(t2,3) + 
                  13*Power(t2,4))) + 
            Power(s2,2)*(-64 + Power(t1,5) + 90*t2 + 98*Power(t2,2) + 
               154*Power(t2,3) - 4*Power(t2,4) + 4*Power(t2,5) - 
               Power(t1,4)*(5 + t2) + 
               Power(t1,3)*(36 + 42*t2 - 11*Power(t2,2)) + 
               Power(t1,2)*(-141 + 127*t2 - Power(t2,2) + 
                  25*Power(t2,3)) - 
               t1*(-173 + 134*t2 + 337*Power(t2,2) + 32*Power(t2,3) + 
                  18*Power(t2,4))) + 
            Power(s1,3)*(-27 - 8*Power(t1,2) + 19*t2 - 3*Power(t2,2) + 
               Power(s2,4)*(11 - t1 + 8*t2) + t1*(-4 + 9*t2) + 
               Power(s2,2)*(-96 - 9*Power(t1,2) + t1*(-77 + t2) + 
                  151*t2 + 2*Power(t2,2)) + 
               s2*(88 + 29*Power(t1,2) + t1*(43 - 37*t2) - 97*t2 + 
                  14*Power(t2,2)) + 
               Power(s2,3)*(24 - 8*Power(t1,2) - 85*t2 - 
                  13*Power(t2,2) + t1*(43 + 23*t2))) + 
            Power(s1,2)*(-19 - Power(s2,5)*(-1 + t1) - 14*Power(t1,3) + 
               85*t2 - 44*Power(t2,2) + 5*Power(t2,3) + 
               Power(t1,2)*(-42 + 40*t2) + 
               t1*(-34 + 62*t2 - 27*Power(t2,2)) + 
               Power(s2,4)*(-24 + 4*Power(t1,2) - 58*t2 - 
                  21*Power(t2,2) + t1*(19 + 12*t2)) + 
               Power(s2,2)*(43 + 7*Power(t1,3) + 287*t2 - 
                  201*Power(t2,2) + 3*Power(t2,3) + 
                  2*Power(t1,2)*(-6 + 7*t2) + 
                  t1*(-219 + 178*t2 - 20*Power(t2,2))) - 
               Power(s2,3)*(15 + 4*Power(t1,3) + 
                  Power(t1,2)*(2 - 27*t2) + 40*t2 - 143*Power(t2,2) - 
                  19*Power(t2,3) + 2*t1*(-49 + 64*t2 + 21*Power(t2,2))) \
+ s2*(14 + 11*Power(t1,3) + Power(t1,2)*(58 - 79*t2) - 262*t2 + 
                  141*Power(t2,2) - 25*Power(t2,3) + 
                  t1*(125 - 148*t2 + 85*Power(t2,2)))) + 
            s1*(67 + 20*Power(t1,4) + Power(t1,3)*(33 - 31*t2) + 6*t2 - 
               102*Power(t2,2) + 65*Power(t2,3) - 5*Power(t2,4) + 
               Power(s2,5)*(-5 + 8*t1 + Power(t1,2) + 4*t2) + 
               Power(t1,2)*(-44 + 79*t2 - 3*Power(t2,2)) + 
               t1*(-153 + 216*t2 - 169*Power(t2,2) + 19*Power(t2,3)) + 
               Power(s2,4)*(21 + 4*Power(t1,3) + 
                  2*Power(t1,2)*(-16 + t2) + 57*t2 + 99*Power(t2,2) + 
                  22*Power(t2,3) - t1*(8 + 59*t2 + 28*Power(t2,2))) + 
               s2*(-173 - 13*Power(t1,4) + Power(t1,3)*(-31 + t2) + 
                  31*t2 + 303*Power(t2,2) - 100*Power(t2,3) + 
                  20*Power(t2,4) + 
                  Power(t1,2)*(1 - 88*t2 + 57*Power(t2,2)) + 
                  t1*(391 - 473*t2 + 187*Power(t2,2) - 65*Power(t2,3))) \
- Power(s2,2)*(-120 + 11*Power(t1,4) + Power(t1,3)*(15 - 41*t2) + 
                  169*t2 + 350*Power(t2,2) - 89*Power(t2,3) + 
                  8*Power(t2,4) + 
                  Power(t1,2)*(69 - 3*t2 + 57*Power(t2,2)) + 
                  t1*(149 - 557*t2 + 37*Power(t2,2) - 35*Power(t2,3))) + 
               Power(s2,3)*(-30 + 59*t2 + 14*Power(t2,2) - 
                  96*Power(t2,3) - 11*Power(t2,4) + 
                  5*Power(t1,3)*(5 + t2) + 
                  Power(t1,2)*(119 - 48*t2 - 21*Power(t2,2)) + 
                  t1*(-77 - 181*t2 + 103*Power(t2,2) + 27*Power(t2,3))))) \
+ Power(s,2)*(78 - 263*t1 + 267*Power(t1,2) - 35*Power(t1,3) - 
            69*Power(t1,4) + 22*Power(t1,5) - 52*t2 - 12*t1*t2 + 
            18*Power(t1,2)*t2 + 38*Power(t1,3)*t2 + 11*Power(t1,4)*t2 - 
            3*Power(t1,5)*t2 + 99*Power(t2,2) + 10*t1*Power(t2,2) + 
            13*Power(t1,2)*Power(t2,2) - 105*Power(t1,3)*Power(t2,2) + 
            6*Power(t1,4)*Power(t2,2) - 85*Power(t2,3) - 
            74*t1*Power(t2,3) + 99*Power(t1,2)*Power(t2,3) - 
            2*Power(t1,3)*Power(t2,3) + 60*Power(t2,4) - 
            21*t1*Power(t2,4) - 2*Power(t1,2)*Power(t2,4) - 
            6*Power(t2,5) + t1*Power(t2,5) + 
            Power(s2,6)*t2*(t1 - Power(t1,2) + t2*(3 + t2)) + 
            Power(s1,5)*(3*Power(s2,4) + t1 + 
               Power(s2,3)*(-25 + 6*t1 - 3*t2) - 
               Power(s2,2)*(-33 + t2) + s2*(-11 - 5*t1 + 2*t2)) + 
            Power(s2,5)*(-2 + 15*t2 + 48*Power(t2,2) + 52*Power(t2,3) + 
               13*Power(t2,4) + Power(t1,3)*(1 + 4*t2) + 
               Power(t1,2)*(-4 + 8*t2 + 6*Power(t2,2)) - 
               t1*(-5 + 27*t2 + 67*Power(t2,2) + 23*Power(t2,3))) + 
            Power(s2,4)*(10 + 65*t2 + 77*Power(t2,2) - 46*Power(t2,3) - 
               58*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,4)*(-4 + 3*t2) - 2*Power(t1,3)*t2*(20 + 7*t2) + 
               Power(t1,2)*(22 + 156*t2 + 122*Power(t2,2) + 
                  13*Power(t2,3)) + 
               t1*(-28 - 184*t2 - 169*Power(t2,2) - 20*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s2,2)*(46 - 322*t2 - 12*Power(t2,2) - 
               100*Power(t2,3) + 122*Power(t2,4) - 27*Power(t2,5) + 
               Power(t2,6) + Power(t1,5)*(-7 + 2*t2) + 
               Power(t1,4)*(7 + 6*t2 - 9*Power(t2,2)) + 
               Power(t1,3)*(-19 + 272*t2 + 108*Power(t2,2) + 
                  12*Power(t2,3)) - 
               Power(t1,2)*(-91 + 806*t2 + 397*Power(t2,2) + 
                  169*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(-118 + 848*t2 + 552*Power(t2,2) - 100*Power(t2,3) + 
                  89*Power(t2,4) - 2*Power(t2,5))) + 
            s2*(-144 + 247*t2 - 92*Power(t2,2) + 232*Power(t2,3) - 
               94*Power(t2,4) + 30*Power(t2,5) - Power(t2,6) + 
               Power(t1,5)*(2 + 3*t2) - 
               Power(t1,4)*(52 + 101*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(306 + 142*t2 + 137*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(t1,2)*(-608 + 23*t2 + 49*Power(t2,2) - 
                  5*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(496 - 314*t2 - 275*Power(t2,2) + 51*Power(t2,3) - 
                  63*Power(t2,4) + Power(t2,5))) + 
            Power(s2,3)*(6 - 3*Power(t1,5) + 7*t2 - 237*Power(t2,2) - 
               180*Power(t2,3) - 95*Power(t2,4) + 3*Power(t2,5) + 
               Power(t1,4)*(34 + 21*t2 - 2*Power(t2,2)) + 
               Power(t1,3)*(-99 - 112*t2 - 24*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,2)*(114 + 166*t2 - 201*Power(t2,2) - 
                  24*Power(t2,3) - 6*Power(t2,4)) + 
               t1*(-52 - 82*t2 + 357*Power(t2,2) + 406*Power(t2,3) + 
                  27*Power(t2,4) + 2*Power(t2,5))) + 
            Power(s1,4)*(11 + 3*Power(s2,5) - Power(t1,2) + 
               Power(s2,4)*(-32 + 3*t1 - 22*t2) - 3*t2 - t1*(7 + t2) + 
               Power(s2,2)*(139 + 11*Power(t1,2) - 11*t1*(-3 + t2) - 
                  162*t2 + 6*Power(t2,2)) + 
               Power(s2,3)*(-36 - 47*t1 + 12*Power(t1,2) + 134*t2 - 
                  33*t1*t2 + 14*Power(t2,2)) + 
               s2*(-91 - 16*Power(t1,2) + 61*t2 - 8*Power(t2,2) + 
                  5*t1*(2 + 5*t2))) + 
            Power(s1,3)*(58 + 9*Power(t1,3) + 
               Power(s2,5)*(-27 + 4*t1 - 19*t2) - 48*t2 + 
               9*Power(t2,2) - 5*Power(t1,2)*(-3 + 2*t2) + 
               t1*(-37 + 14*t2 + 2*Power(t2,2)) + 
               Power(s2,4)*(63 - 6*Power(t1,2) + 159*t2 + 
                  56*Power(t2,2) - t1*(11 + 29*t2)) - 
               s2*(129 + 6*Power(t1,3) + Power(t1,2)*(9 - 47*t2) - 
                  357*t2 + 143*Power(t2,2) - 13*Power(t2,3) + 
                  2*t1*(5 + 23*Power(t2,2))) + 
               Power(s2,2)*(11 + 9*Power(t1,3) + 
                  Power(t1,2)*(8 - 56*t2) - 591*t2 + 301*Power(t2,2) - 
                  13*Power(t2,3) + t1*(264 - 104*t2 + 47*Power(t2,2))) \
+ Power(s2,3)*(48 + 6*Power(t1,3) + 170*t2 - 255*Power(t2,2) - 
                  24*Power(t2,3) - 3*Power(t1,2)*(6 + 13*t2) + 
                  t1*(-238 + 169*t2 + 61*Power(t2,2)))) + 
            Power(s1,2)*(-23*Power(t1,4) + Power(s2,6)*(3*t1 + t2) + 
               8*Power(t1,3)*(-7 + 3*t2) + 
               Power(t1,2)*(29 - 29*t2 + 3*Power(t2,2)) + 
               3*(5 - 55*t2 + 33*Power(t2,2) - 5*Power(t2,3)) + 
               t1*(58 + 18*t2 + 14*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,5)*(37 - 7*Power(t1,2) + 99*t2 + 
                  42*Power(t2,2) - t1*(43 + 24*t2)) + 
               Power(s2,2)*(-213 + 15*Power(t1,4) + 66*t2 + 
                  894*Power(t2,2) - 275*Power(t2,3) + 13*Power(t2,4) - 
                  Power(t1,3)*(6 + 61*t2) + 
                  Power(t1,2)*(-153 - 20*t2 + 96*Power(t2,2)) + 
                  t1*(599 - 926*t2 + 170*Power(t2,2) - 63*Power(t2,3))) \
+ Power(s2,4)*(-6*Power(t1,3) + 2*Power(t1,2)*(34 + t2) + 
                  t1*(-48 + 35*t2 + 57*Power(t2,2)) - 
                  2*(-1 + 91*t2 + 142*Power(t2,2) + 32*Power(t2,3))) + 
               s2*(157 + 22*Power(t1,4) + Power(t1,3)*(114 - 22*t2) + 
                  361*t2 - 507*Power(t2,2) + 177*Power(t2,3) - 
                  11*Power(t2,4) + 
                  Power(t1,2)*(86 + 25*t2 - 27*Power(t2,2)) + 
                  t1*(-563 + 218*t2 - 136*Power(t2,2) + 38*Power(t2,3))\
) - Power(s2,3)*(34 + 292*t2 + 330*Power(t2,2) - 211*Power(t2,3) - 
                  18*Power(t2,4) + Power(t1,3)*(40 + 3*t2) + 
                  Power(t1,2)*(139 - 38*t2 - 30*Power(t2,2)) + 
                  t1*(-106 - 869*t2 + 161*Power(t2,2) + 45*Power(t2,3)))\
) + s1*(-135 + 6*Power(t1,5) + 9*t2 + 186*Power(t2,2) - 
               122*Power(t2,3) + 15*Power(t2,4) + 
               2*Power(t1,4)*(5 + 4*t2) + 
               Power(t1,3)*(-39 + 110*t2 - 25*Power(t2,2)) + 
               Power(t1,2)*(-278 + 210*t2 - 103*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(436 - 383*t2 + 111*Power(t2,2) + Power(t2,4)) + 
               Power(s2,6)*(2 + 3*Power(t1,2) - 3*t2 - 2*Power(t2,2) - 
                  t1*(5 + 3*t2)) + 
               Power(s2,5)*(-10 - 8*Power(t1,3) - 81*t2 - 
                  124*Power(t2,2) - 39*Power(t2,3) + 
                  5*Power(t1,2)*(1 + t2) + 
                  t1*(13 + 102*t2 + 43*Power(t2,2))) + 
               Power(s2,4)*(-43 - 49*t2 + 167*Power(t2,2) + 
                  215*Power(t2,3) + 33*Power(t2,4) + 
                  Power(t1,3)*(4 + 9*t2) - 
                  Power(t1,2)*(71 + 138*t2 + 7*Power(t2,2)) + 
                  t1*(110 + 146*t2 - 8*Power(t2,2) - 35*Power(t2,3))) + 
               Power(s2,3)*(-21 + 266*t2 + 443*Power(t2,2) + 
                  291*Power(t2,3) - 68*Power(t2,4) - 5*Power(t2,5) + 
                  Power(t1,4)*(-3 + 6*t2) + 
                  Power(t1,3)*(71 - 4*t2 - 13*Power(t2,2)) + 
                  Power(t1,2)*
                   (-152 + 459*t2 + 31*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(105 - 513*t2 - 1079*Power(t2,2) + 
                     12*Power(t2,3) + 9*Power(t2,4))) - 
               s2*(-119 + 3*Power(t1,5) + 252*t2 + 467*Power(t2,2) - 
                  335*Power(t2,3) + 114*Power(t2,4) - 5*Power(t2,5) + 
                  Power(t1,4)*(-28 + 29*t2) + 
                  Power(t1,3)*(21 + 153*t2 - 48*Power(t2,2)) + 
                  Power(t1,2)*
                   (-320 + 488*t2 + 46*Power(t2,2) + 8*Power(t2,3)) + 
                  t1*(443 - 1290*t2 + 237*Power(t2,2) - 
                     189*Power(t2,3) + 13*Power(t2,4))) + 
               Power(s2,2)*(112 - 3*Power(t1,5) + 228*t2 + 
                  17*Power(t2,2) - 564*Power(t2,3) + 130*Power(t2,4) - 
                  6*Power(t2,5) + Power(t1,4)*(-1 + 7*t2) + 
                  5*Power(t1,3)*(-37 - 12*t2 + 4*Power(t2,2)) + 
                  Power(t1,2)*
                   (411 + 430*t2 + 215*Power(t2,2) - 47*Power(t2,3)) + 
                  t1*(-334 - 1089*t2 + 754*Power(t2,2) - 
                     188*Power(t2,3) + 29*Power(t2,4))))) + 
         s*(-83 + 307*t1 - 403*Power(t1,2) + 190*Power(t1,3) + 
            23*Power(t1,4) - 41*Power(t1,5) + 7*Power(t1,6) + 56*t2 - 
            139*t1*t2 + 164*Power(t1,2)*t2 - 125*Power(t1,3)*t2 + 
            34*Power(t1,4)*t2 + 10*Power(t1,5)*t2 - 49*Power(t2,2) - 
            34*t1*Power(t2,2) + 110*Power(t1,2)*Power(t2,2) + 
            25*Power(t1,3)*Power(t2,2) - 52*Power(t1,4)*Power(t2,2) + 
            66*Power(t2,3) - 20*t1*Power(t2,3) - 
            96*Power(t1,2)*Power(t2,3) + 48*Power(t1,3)*Power(t2,3) - 
            44*Power(t2,4) + 44*t1*Power(t2,4) - 
            11*Power(t1,2)*Power(t2,4) + 6*Power(t2,5) - 
            2*t1*Power(t2,5) - Power(s2,7)*Power(t2,2)*(1 - t1 + t2) + 
            Power(s1,6)*s2*(-3*Power(s2,3) + 2*t1 + 
               Power(s2,2)*(15 - 4*t1 + t2) + s2*(-10 - 2*t1 + t2)) - 
            Power(s2,6)*t2*(2 + 11*t2 + 15*Power(t2,2) + 7*Power(t2,3) + 
               Power(t1,2)*(2 + 8*t2) - t1*(4 + 19*t2 + 15*Power(t2,2))) \
+ Power(s2,5)*(-1 - 18*t2 - 33*Power(t2,2) + 23*Power(t2,3) + 
               52*Power(t2,4) + 9*Power(t2,5) + 
               Power(t1,3)*(1 + 16*t2 + 11*Power(t2,2)) - 
               Power(t1,2)*(3 + 50*t2 + 48*Power(t2,2) + 
                  14*Power(t2,3)) + 
               t1*(3 + 52*t2 + 70*Power(t2,2) - 17*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s2,4)*(-8 - 15*t2 + 120*Power(t2,2) + 
               223*Power(t2,3) + 51*Power(t2,4) - Power(t2,5) + 
               Power(t2,6) + 2*Power(t1,4)*(-4 - 11*t2 + Power(t2,2)) + 
               4*Power(t1,3)*
                (8 + 17*t2 + 3*Power(t2,2) - 3*Power(t2,3)) + 
               Power(t1,2)*(-48 - 85*t2 + 101*Power(t2,2) + 
                  104*Power(t2,3) + 19*Power(t2,4)) - 
               t1*(-32 - 54*t2 + 235*Power(t2,2) + 297*Power(t2,3) + 
                  97*Power(t2,4) + 10*Power(t2,5))) + 
            Power(s2,2)*(15 + 2*Power(t1,6) + 234*t2 - 246*Power(t2,2) + 
               60*Power(t2,3) - 104*Power(t2,4) + 64*Power(t2,5) - 
               15*Power(t2,6) - 2*Power(t1,5)*(11 + 13*t2) + 
               Power(t1,4)*(95 + 183*t2 + 98*Power(t2,2)) - 
               Power(t1,3)*(188 + 617*t2 + 194*Power(t2,2) + 
                  112*Power(t2,3)) + 
               Power(t1,2)*(184 + 1023*t2 + 133*Power(t2,2) - 
                  46*Power(t2,3) + 29*Power(t2,4)) + 
               t1*(-86 - 797*t2 + 209*Power(t2,2) + 194*Power(t2,3) - 
                  73*Power(t2,4) + 24*Power(t2,5))) + 
            s2*(82 + 7*Power(t1,6) - 295*t2 + 114*Power(t2,2) - 
               118*Power(t2,3) + 118*Power(t2,4) - 47*Power(t2,5) + 
               2*Power(t2,6) - 6*Power(t1,5)*(7 + 8*t2) + 
               Power(t1,4)*(169 + 121*t2 + 35*Power(t2,2)) + 
               Power(t1,3)*(-407 + 38*t2 - 63*Power(t2,2) + 
                  60*Power(t2,3)) + 
               Power(t1,2)*(522 - 542*t2 - 49*Power(t2,2) + 
                  95*Power(t2,3) - 82*Power(t2,4)) + 
               t1*(-331 + 726*t2 - 37*Power(t2,2) - 88*Power(t2,3) + 
                  20*Power(t2,4) + 26*Power(t2,5))) + 
            Power(s2,3)*(-1 + Power(t1,5)*(11 - 4*t2) + 108*t2 + 
               326*Power(t2,2) + 8*Power(t2,4) - 41*Power(t2,5) + 
               10*Power(t2,6) + 
               Power(t1,4)*(-35 + 24*t2 + 31*Power(t2,2)) - 
               Power(t1,3)*(-40 + 172*t2 + 239*Power(t2,2) + 
                  76*Power(t2,3)) + 
               Power(t1,2)*(-20 + 396*t2 + 687*Power(t2,2) + 
                  199*Power(t2,3) + 77*Power(t2,4)) - 
               t1*(-5 + 352*t2 + 805*Power(t2,2) + 239*Power(t2,3) - 
                  82*Power(t2,4) + 38*Power(t2,5))) + 
            Power(s1,5)*(-6*Power(s2,5) + 2*(-1 + t1)*t1 + 
               Power(s2,4)*(26 - 3*t1 + 20*t2) + 
               s2*(Power(t1,2) - 4*(-5 + t2) - 2*t1*(8 + 3*t2)) + 
               Power(s2,2)*(-81 - 11*Power(t1,2) + 61*t2 - 
                  5*Power(t2,2) + t1*(14 + 15*t2)) + 
               Power(s2,3)*(47 - 8*Power(t1,2) - 87*t2 - 5*Power(t2,2) + 
                  t1*(23 + 21*t2))) - 
            Power(s1,4)*(10 + 4*Power(s2,6) + 5*Power(t1,3) + 
               Power(s2,5)*(-43 + 5*t1 - 34*t2) - 3*t2 - 2*t1*(9 + t2) + 
               Power(t1,2)*(15 + t2) + 
               Power(s2,4)*(18 - 4*Power(t1,2) + t1*(13 - 18*t2) + 
                  115*t2 + 50*Power(t2,2)) + 
               s2*(-117 + 5*Power(t1,3) + Power(t1,2)*(5 - 3*t2) + 
                  105*t2 - 16*Power(t2,2) + 
                  t1*(97 - 67*t2 - 6*Power(t2,2))) + 
               Power(s2,2)*(90 + 14*Power(t1,3) - 391*t2 + 
                  160*Power(t2,2) - 10*Power(t2,3) - 
                  Power(t1,2)*(22 + 51*t2) + 
                  t1*(52 + 48*t2 + 38*Power(t2,2))) + 
               Power(s2,3)*(78 + 4*Power(t1,3) + 264*t2 - 
                  204*Power(t2,2) - 10*Power(t2,3) - 
                  Power(t1,2)*(24 + 25*t2) + 
                  t1*(-199 + 95*t2 + 40*Power(t2,2)))) + 
            Power(s1,3)*(-51 + Power(s2,7) + 10*Power(t1,4) + 
               Power(t1,3)*(-7 + t2) + 43*t2 - 9*Power(t2,2) + 
               Power(s2,6)*(11 - 11*t1 + 19*t2) + 
               t1*(87 - 54*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(-37 + 57*t2 - 3*Power(t2,2)) + 
               Power(s2,5)*(-32 + 11*Power(t1,2) - 185*t2 - 
                  75*Power(t2,2) + t1*(29 + 25*t2)) + 
               Power(s2,4)*(-124 + 4*Power(t1,3) + 6*t2 + 
                  189*Power(t2,2) + 59*Power(t2,3) - 
                  2*Power(t1,2)*(22 + 9*t2) + 
                  t1*(146 + 120*t2 - 25*Power(t2,2))) - 
               s2*(-51 + 17*Power(t1,4) + Power(t1,3)*(69 - 29*t2) + 
                  440*t2 - 237*Power(t2,2) + 26*Power(t2,3) + 
                  Power(t1,2)*(21 - 37*t2 + 12*Power(t2,2)) + 
                  t1*(-107 - 288*t2 + 129*Power(t2,2) + 2*Power(t2,3))) \
+ Power(s2,3)*(18 - Power(t1,3)*(-41 + t2) + 311*t2 + 557*Power(t2,2) - 
                  250*Power(t2,3) - 10*Power(t2,4) - 
                  Power(t1,2)*(5 + 69*t2 + 21*Power(t2,2)) + 
                  t1*(62 - 831*t2 + 173*Power(t2,2) + 34*Power(t2,3))) + 
               Power(s2,2)*(186 - 9*Power(t1,4) + 386*t2 - 
                  735*Power(t2,2) + 229*Power(t2,3) - 10*Power(t2,4) + 
                  Power(t1,3)*(59 + 39*t2) + 
                  Power(t1,2)*(224 - 59*t2 - 72*Power(t2,2)) + 
                  t1*(-556 + 184*t2 + 5*Power(t2,2) + 44*Power(t2,3)))) + 
            Power(s1,2)*(-5 - 6*Power(t1,5) + 
               Power(s2,7)*(-1 + t1 - 3*t2) + 136*t2 - 86*Power(t2,2) + 
               15*Power(t2,3) - Power(t1,4)*(55 + 4*t2) + 
               Power(t1,3)*(93 + 5*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(51 + 12*t2 - 50*Power(t2,2) + Power(t2,3)) + 
               t1*(-78 - 155*t2 + 62*Power(t2,2) + 8*Power(t2,3)) - 
               Power(s2,6)*(10 + 7*Power(t1,2) + 37*t2 + 
                  33*Power(t2,2) - t1*(17 + 37*t2)) + 
               Power(s2,5)*(-23 + 10*Power(t1,3) + 94*t2 + 
                  293*Power(t2,2) + 81*Power(t2,3) - 
                  Power(t1,2)*(36 + 29*t2) + 
                  t1*(49 - 89*t2 - 41*Power(t2,2))) + 
               Power(s2,4)*(91 + Power(t1,3)*(25 - 21*t2) + 465*t2 + 
                  98*Power(t2,2) - 138*Power(t2,3) - 32*Power(t2,4) + 
                  4*Power(t1,2)*(13 + 47*t2 + 12*Power(t2,2)) - 
                  2*t1*(84 + 289*t2 + 154*Power(t2,2) + Power(t2,3))) + 
               s2*(-242 + 3*Power(t1,5) - 132*t2 + 630*Power(t2,2) - 
                  286*Power(t2,3) + 22*Power(t2,4) + 
                  Power(t1,4)*(24 + 25*t2) + 
                  Power(t1,3)*(58 + 172*t2 - 31*Power(t2,2)) + 
                  Power(t1,2)*
                   (-626 + 304*t2 - 182*Power(t2,2) + 11*Power(t2,3)) + 
                  3*t1*(261 - 174*t2 - 73*Power(t2,2) + 49*Power(t2,3))) \
+ Power(s2,2)*(-41 + 3*Power(t1,5) - 11*Power(t1,4)*(-4 + t2) - 
                  477*t2 - 617*Power(t2,2) + 685*Power(t2,3) - 
                  187*Power(t2,4) + 5*Power(t2,5) - 
                  3*Power(t1,3)*(7 + 38*t2 + 4*Power(t2,2)) + 
                  Power(t1,2)*
                   (142 - 804*t2 + 22*Power(t2,2) + 35*Power(t2,3)) + 
                  t1*(-127 + 1694*t2 - 239*Power(t2,2) + 
                     102*Power(t2,3) - 24*Power(t2,4))) - 
               Power(s2,3)*(-201 + 60*t2 + 383*Power(t2,2) + 
                  551*Power(t2,3) - 171*Power(t2,4) - 5*Power(t2,5) + 
                  3*Power(t1,4)*(-5 + 2*t2) + 
                  Power(t1,3)*(121 + 86*t2 - 14*Power(t2,2)) + 
                  Power(t1,2)*
                   (-374 - 59*t2 - 140*Power(t2,2) + Power(t2,3)) + 
                  t1*(469 + 255*t2 - 1153*Power(t2,2) + 
                     191*Power(t2,3) + 12*Power(t2,4)))) + 
            s1*(108 + 5*Power(t1,5)*(-2 + t2) - 10*t2 - 152*Power(t2,2) + 
               97*Power(t2,3) - 15*Power(t2,4) + 
               Power(s2,7)*t2*(2 - 2*t1 + 3*t2) + 
               Power(t1,4)*(36 + 69*t2 - 5*Power(t2,2)) - 
               Power(t1,3)*(249 - 66*t2 + 48*Power(t2,2) + Power(t2,3)) + 
               t1*(-423 + 345*t2 + 90*Power(t2,2) - 70*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(t1,2)*(538 - 475*t2 + 121*Power(t2,2) + 
                  19*Power(t2,3) + Power(t2,4)) + 
               Power(s2,6)*(2 + 21*t2 + 41*Power(t2,2) + 
                  25*Power(t2,3) + Power(t1,2)*(2 + 15*t2) - 
                  t1*(4 + 36*t2 + 41*Power(t2,2))) + 
               Power(s2,5)*(17 + 52*t2 - 85*Power(t2,2) - 
                  203*Power(t2,3) - 43*Power(t2,4) - 
                  Power(t1,3)*(15 + 17*t2) + 
                  Power(t1,2)*(47 + 72*t2 + 32*Power(t2,2)) + 
                  t1*(-49 - 107*t2 + 77*Power(t2,2) + 27*Power(t2,3))) + 
               s2*(130 + Power(t1,5)*(19 - 7*t2) + 225*t2 + 
                  203*Power(t2,2) - 425*Power(t2,3) + 181*Power(t2,4) - 
                  10*Power(t2,5) + 
                  Power(t1,4)*(-36 - 22*t2 + 3*Power(t2,2)) + 
                  Power(t1,3)*
                   (46 - 179*t2 - 200*Power(t2,2) + 7*Power(t2,3)) + 
                  t1*(-229 - 1062*t2 + 480*Power(t2,2) + 
                     8*Power(t2,3) - 95*Power(t2,4)) + 
                  Power(t1,2)*
                   (70 + 1045*t2 - 333*Power(t2,2) + 232*Power(t2,3) - 
                     3*Power(t2,4))) + 
               Power(s2,4)*(13 + Power(t1,4)*(17 - 6*t2) - 221*t2 - 
                  567*Power(t2,2) - 137*Power(t2,3) + 39*Power(t2,4) + 
                  5*Power(t2,5) + 
                  Power(t1,3)*(-51 - 15*t2 + 32*Power(t2,2)) - 
                  Power(t1,2)*
                   (-64 + 195*t2 + 257*Power(t2,2) + 53*Power(t2,3)) + 
                  t1*(-43 + 437*t2 + 738*Power(t2,2) + 298*Power(t2,3) + 
                     22*Power(t2,4))) - 
               Power(s2,2)*(202 - 315*t2 - 240*Power(t2,2) - 
                  425*Power(t2,3) + 324*Power(t2,4) - 82*Power(t2,5) + 
                  Power(t2,6) + Power(t1,5)*(-25 + 4*t2) + 
                  Power(t1,4)*(138 + 98*t2 - 16*Power(t2,2)) + 
                  Power(t1,3)*
                   (-456 - 61*t2 - 170*Power(t2,2) + 13*Power(t2,3)) + 
                  Power(t1,2)*
                   (800 + 67*t2 - 641*Power(t2,2) + 14*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  t1*(-659 + 207*t2 + 1355*Power(t2,2) - 
                     180*Power(t2,3) + 97*Power(t2,4) - 5*Power(t2,5))) + 
               Power(s2,3)*(-70 + 6*Power(t1,5) - 514*t2 + 
                  31*Power(t2,2) + 142*Power(t2,3) + 252*Power(t2,4) - 
                  63*Power(t2,5) - Power(t2,6) + 
                  Power(t1,4)*(-17 - 57*t2 + 4*Power(t2,2)) + 
                  Power(t1,3)*
                   (101 + 380*t2 + 138*Power(t2,2) - 9*Power(t2,3)) + 
                  Power(t1,2)*
                   (-245 - 1055*t2 - 292*Power(t2,2) - 172*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  t1*(225 + 1246*t2 + 467*Power(t2,2) - 603*Power(t2,3) + 
                     128*Power(t2,4) + Power(t2,5))))))*
       B1(1 - s1 - t1 + t2,s2,1 - s + s2 - t1))/
     ((-1 + t1)*(s - s2 + t1)*(-1 + s1 + t1 - t2)*(-1 + t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,3)) + 
    (8*(-(t1*Power(-1 + s + t1,4)*(-1 + s1 + t1 - t2)*
            (s*(3 + t1 + s1*(2 + t1) - 3*t2) - 
              2*(1 + s1 + t1 - Power(t1,2) - t2) + 
              Power(s,2)*(-2 - t1 + t2))) - 
         Power(s2,9)*(s1 - t2)*
          (Power(s,3)*(-6 + 4*s1 + 6*t1 - 4*t2) - 
            (-1 + t1)*(-6 + 5*s1 + 6*t1 - 5*t2)*Power(s1 - t2,2) + 
            s*(s1 - t2)*(14 + Power(s1,2) + 14*Power(t1,2) + 
               s1*(-15 + 15*t1 - 2*t2) + 15*t2 + Power(t2,2) - 
               t1*(28 + 15*t2)) - 
            3*Power(s,2)*(2 + Power(s1,2) + 2*Power(t1,2) + 4*t2 + 
               Power(t2,2) - 4*t1*(1 + t2) + s1*(4*t1 - 2*(2 + t2)))) + 
         s2*Power(-1 + s + t1,3)*
          (5 - 21*t1 - 15*Power(t1,2) - 
            Power(s1,3)*(-6 + t1)*Power(t1,2) + 87*Power(t1,3) - 
            70*Power(t1,4) + 14*Power(t1,5) + 5*t2 + 14*t1*t2 - 
            89*Power(t1,2)*t2 + 84*Power(t1,3)*t2 - 14*Power(t1,4)*t2 - 
            5*Power(t2,2) + 35*t1*Power(t2,2) - 
            28*Power(t1,2)*Power(t2,2) - 5*Power(t2,3) + 
            2*Power(s,4)*(-1 + s1 + t1 - t2)*(-2 + t1 + t2) - 
            Power(s1,2)*(10 + Power(t1,4) - Power(t1,3)*(16 + t2) + 
               Power(t1,2)*(54 + 5*t2) + t1*(-51 + 11*t2)) + 
            s1*(23*Power(t1,4) + 5*Power(1 + t2,2) - 
               Power(t1,3)*(113 + 8*t2) + 
               Power(t1,2)*(130 + 64*t2 - Power(t2,2)) + 
               t1*(-45 - 70*t2 + 11*Power(t2,2))) - 
            Power(s,3)*(16 + 5*Power(t1,3) + Power(t1,2)*(33 - 21*t2) + 
               2*Power(s1,2)*(-2 + t2) + 10*t2 - 9*Power(t2,2) - 
               3*Power(t2,3) + 
               s1*(-12 + 5*Power(t1,2) + t1*(34 - 14*t2) + 8*t2 + 
                  Power(t2,2)) + t1*(-54 - 16*t2 + 19*Power(t2,2))) + 
            Power(s,2)*(24 - 2*Power(s1,3)*t1 - 4*Power(t1,4) + 
               Power(s1,2)*(9*Power(t1,2) + t1*(43 - 7*t2) + 
                  4*(-4 + t2)) + 21*t2 - 14*Power(t2,2) - 
               11*Power(t2,3) + Power(t1,3)*(-17 + 12*t2) + 
               Power(t1,2)*(94 - 46*t2 - 8*Power(t2,2)) + 
               t1*(-97 - 11*t2 + 70*Power(t2,2)) + 
               s1*(-8 + 7*Power(t1,3) + Power(t1,2)*(9 - 12*t2) + 
                  15*t2 + 7*Power(t2,2) + 
                  t1*(16 - 87*t2 + 9*Power(t2,2)))) - 
            s*(16 - 16*Power(t1,4) + 2*Power(s1,3)*t1*(-1 + 3*t1) + 
               17*t2 - 12*Power(t2,2) - 13*Power(t2,3) + 
               Power(t1,3)*(31 + 47*t2) + 
               Power(t1,2)*(40 - 117*t2 - 31*Power(t2,2)) + 
               t1*(-71 + 15*t2 + 84*Power(t2,2)) + 
               Power(s1,2)*(Power(t1,3) + t1*(93 - 18*t2) + 
                  2*(-11 + t2) - Power(t1,2)*(39 + 5*t2)) + 
               s1*(6 - 5*Power(t1,4) + 6*Power(t1,3)*(-7 + t2) + 19*t2 + 
                  11*Power(t2,2) + 
                  Power(t1,2)*(137 + 41*t2 - Power(t2,2)) + 
                  2*t1*(-29 - 70*t2 + 10*Power(t2,2))))) - 
         Power(s2,2)*Power(-1 + s + t1,2)*
          (-32 + 50*t1 + 137*Power(t1,2) - 377*Power(t1,3) + 
            289*Power(t1,4) - 53*Power(t1,5) - 14*Power(t1,6) + 
            Power(s1,4)*t1*(6 - 12*t1 + 5*Power(t1,2)) + 59*t2 - 
            154*t1*t2 + 65*Power(t1,2)*t2 + 178*Power(t1,3)*t2 - 
            230*Power(t1,4)*t2 + 82*Power(t1,5)*t2 - 15*Power(t2,2) + 
            138*t1*Power(t2,2) - 364*Power(t1,2)*Power(t2,2) + 
            327*Power(t1,3)*Power(t2,2) - 86*Power(t1,4)*Power(t2,2) - 
            15*Power(t2,3) + 90*t1*Power(t2,3) - 
            92*Power(t1,2)*Power(t2,3) + 18*Power(t1,3)*Power(t2,3) - 
            5*Power(t2,4) + 4*t1*Power(t2,4) + 
            2*Power(s,5)*(8 + 5*Power(t1,2) + 4*t2 - 3*Power(t2,2) - 
               2*t1*(6 + t2) + 4*s1*(-2 + t1 + t2)) + 
            Power(s1,3)*(32 + 4*Power(t1,4) - 2*t1*(65 + t2) - 
               Power(t1,3)*(53 + 5*t2) + Power(t1,2)*(146 + 11*t2)) - 
            Power(s1,2)*(3 + 6*Power(t1,5) - 4*Power(t1,4)*(-4 + t2) + 
               69*t2 + 10*Power(t2,2) - Power(t1,3)*(131 + 77*t2) + 
               Power(t1,2)*(146 + 316*t2 + Power(t2,2)) - 
               t1*(40 + 307*t2 + 5*Power(t2,2))) + 
            s1*(-17 + 11*Power(t1,5) - 19*t2 + 61*Power(t2,2) + 
               15*Power(t2,3) + Power(t1,4)*(-149 + 106*t2) + 
               Power(t1,3)*(359 - 397*t2 - 75*Power(t2,2)) + 
               Power(t1,2)*(-332 + 363*t2 + 313*Power(t2,2) + 
                  2*Power(t2,3)) - 
               t1*(-128 + 53*t2 + 302*Power(t2,2) + 13*Power(t2,3))) + 
            Power(s,4)*(-70 + 13*Power(t1,3) - 11*t2 + 43*Power(t2,2) + 
               Power(t2,3) - 6*Power(s1,2)*(-5 + 2*t1 + 2*t2) + 
               Power(t1,2)*(-125 + 23*t2) + 
               t1*(175 + 34*t2 - 37*Power(t2,2)) + 
               s1*(40 - 2*Power(t1,2) - 73*t2 + 14*Power(t2,2) + 
                  t1*(-67 + 36*t2))) + 
            Power(s,3)*(119 + 4*Power(t1,4) - 24*t2 - 63*Power(t2,2) + 
               9*Power(t2,3) + Power(t2,4) + 
               2*Power(s1,3)*(-7 + 2*t2) + Power(t1,3)*(-164 + 33*t2) + 
               Power(t1,2)*(340 - 59*t2 - 29*Power(t2,2)) + 
               t1*(-288 + 87*t2 + 166*Power(t2,2) - 9*Power(t2,3)) - 
               Power(s1,2)*(108 + 10*Power(t1,2) - 59*t2 + 
                  2*Power(t2,2) + t1*(-222 + 46*t2)) + 
               s1*(5 + 2*Power(t1,3) + Power(t1,2)*(108 - 23*t2) + 
                  198*t2 - 72*Power(t2,2) - 3*Power(t2,3) + 
                  t1*(-175 - 340*t2 + 72*Power(t2,2)))) + 
            Power(s,2)*(-118 + 4*Power(s1,4)*t1 + Power(t1,5) + 
               109*t2 + 32*Power(t2,2) - 30*Power(t2,3) - 
               7*Power(t2,4) + Power(t1,4)*(-89 + 6*t2) + 
               Power(s1,3)*(48 - 88*t1 - 7*Power(t1,2) - 8*t2 + 
                  7*t1*t2) + Power(t1,3)*(21 - 17*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(64 + 369*t2 + 10*Power(t2,2) - 
                  10*Power(t2,3)) + 
               t1*(121 - 370*t2 - 104*Power(t2,2) + 87*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s1,2)*(-146 + 17*Power(t1,3) + 121*t2 + 
                  6*Power(t2,2) + Power(t1,2)*(-262 + 19*t2) + 
                  t1*(431 - 280*t2 + 10*Power(t2,2))) + 
               s1*(-82 + 17*Power(t1,4) + Power(t1,3)*(281 - 68*t2) - 
                  277*t2 + 139*Power(t2,2) + 21*Power(t2,3) + 
                  Power(t1,2)*(-1018 - 162*t2 + 69*Power(t2,2)) + 
                  t1*(705 + 607*t2 - 348*Power(t2,2) - 2*Power(t2,3)))) \
+ s*(81 - 34*Power(t1,5) + Power(s1,4)*t1*(-10 + 9*t1) - 128*t2 + 
               23*Power(t2,2) + 35*Power(t2,3) + 11*Power(t2,4) + 
               2*Power(t1,4)*(-103 + 65*t2) + 
               Power(t1,3)*(674 - 4*t2 - 175*Power(t2,2)) + 
               Power(t1,2)*(-513 - 412*t2 + 362*Power(t2,2) + 
                  84*Power(t2,3)) - 
               t1*(2 - 414*t2 + 141*Power(t2,2) + 167*Power(t2,3) + 
                  5*Power(t2,4)) - 
               Power(s1,3)*(66 + 3*Power(t1,3) - 4*t2 + 
                  Power(t1,2)*(115 + 2*t2) + t1*(-211 + 5*t2)) + 
               Power(s1,2)*(-51 - 25*Power(t1,4) + 143*t2 + 
                  18*Power(t2,2) + Power(t1,3)*(78 + 19*t2) + 
                  Power(t1,2)*(-133 + 262*t2 - 8*Power(t2,2)) + 
                  t1*(200 - 526*t2 + 5*Power(t2,2))) + 
               s1*(57 + 5*Power(t1,5) - 17*Power(t1,4)*(-9 + t2) + 
                  135*t2 - 142*Power(t2,2) - 33*Power(t2,3) + 
                  Power(t1,3)*(-909 + 163*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*
                   (1298 - 128*t2 - 315*Power(t2,2) + Power(t2,3)) + 
                  t1*(-604 - 291*t2 + 569*Power(t2,2) + 15*Power(t2,3))))\
) + Power(s2,8)*(4*Power(s,4)*
             (1 + Power(s1,2) - Power(t1,2) + s1*(-3 + 2*t1 - 4*t2) + 
               4*t2 - 3*t1*t2 + 3*Power(t2,2)) + 
            Power(s,3)*(-6 - 2*Power(t1,3) + 15*t2 + 14*Power(t2,2) + 
               3*Power(t2,3) + Power(s1,2)*(32 - 15*t1 + 3*t2) - 
               Power(t1,2)*(2 + 13*t2) + 
               t1*(10 - 2*t2 + 3*Power(t2,2)) + 
               s1*(-23 + 5*Power(t1,2) - 46*t2 - 6*Power(t2,2) + 
                  6*t1*(3 + 2*t2))) + 
            (-1 + t1)*Power(s1 - t2,2)*
             (22 + 2*Power(t1,3) - Power(s1,2)*(9 + 7*t1) + 7*t2 - 
               10*Power(t2,2) + Power(t1,2)*(18 + 7*t2) - 
               2*t1*(21 + 7*t2 + 3*Power(t2,2)) + 
               s1*(-7 - 7*Power(t1,2) + 19*t2 + t1*(14 + 13*t2))) - 
            Power(s,2)*(10 + 4*Power(s1,4) - 2*Power(t1,4) + 
               Power(s1,3)*(32 - 24*t1 - 15*t2) - 11*t2 - 
               65*Power(t2,2) - 26*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,3)*(-4 + 3*t2) + 
               Power(t1,2)*(24 - 17*t2 - 19*Power(t2,2)) + 
               t1*(-28 + 25*t2 + 84*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s1,2)*(-53 - 7*Power(t1,2) - 90*t2 + 
                  21*Power(t2,2) + t1*(60 + 66*t2)) + 
               s1*(7 + Power(t1,3) + 118*t2 + 84*Power(t2,2) - 
                  13*Power(t2,3) + Power(t1,2)*(5 + 26*t2) - 
                  t1*(13 + 144*t2 + 60*Power(t2,2)))) + 
            s*(Power(s1,4)*(19 - 17*t1) + 
               Power(s1,2)*(5 + 10*Power(t1,3) + 162*t2 + 
                  108*Power(t2,2) + 3*Power(t1,2)*(-5 + 2*t2) - 
                  24*t1*t2*(7 + 4*t2)) - 
               Power(s1,3)*(53 + Power(t1,2) + 74*t2 - 
                  6*t1*(9 + 11*t2)) + 
               t2*(-36 + 4*Power(t1,4) + 3*t2 + 56*Power(t2,2) + 
                  17*Power(t2,3) + 12*Power(t1,3)*(2 + t2) + 
                  Power(t1,2)*(-96 - 21*t2 + 4*Power(t2,2)) + 
                  t1*(104 + 6*t2 - 60*Power(t2,2) - 15*Power(t2,3))) - 
               s1*(-36 + 4*Power(t1,4) + 8*t2 + 165*Power(t2,2) + 
                  70*Power(t2,3) + Power(t1,3)*(24 + 22*t2) + 
                  Power(t1,2)*(-96 - 36*t2 + 9*Power(t2,2)) - 
                  2*t1*(-52 - 3*t2 + 87*Power(t2,2) + 31*Power(t2,3))))) \
+ Power(s2,7)*(Power(s,5)*(-12 - 5*Power(s1,2) + 6*Power(t1,2) - 
               19*t2 - 11*Power(t2,2) + t1*(8 + 7*t2) + 
               s1*(19 - 11*t1 + 18*t2)) + 
            Power(s,4)*(18 + 8*Power(s1,3) + 9*Power(t1,3) - 7*t2 + 
               7*Power(t2,3) + Power(t1,2)*(46 + 24*t2) - 
               t1*(73 + 42*t2 + 34*Power(t2,2)) + 
               s1*(4 - 33*Power(t1,2) + 22*t2 + 9*Power(t2,2) + 
                  18*t1*(3 + t2)) + 2*Power(s1,2)*(7*t1 - 2*(5 + 6*t2))) \
+ Power(s,3)*(3 - Power(s1,4) + 5*Power(t1,4) + 
               Power(s1,3)*(-44 + 9*t1) - 36*t2 - 28*Power(t2,2) + 
               11*Power(t2,3) + 10*Power(t2,4) + 
               18*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(-48 + 7*t2 - 53*Power(t2,2)) + 
               t1*(22 + 11*t2 + 32*Power(t2,2) + 24*Power(t2,3)) + 
               Power(s1,2)*(-12 + 57*Power(t1,2) + 142*t2 + 
                  13*Power(t2,2) - t1*(94 + 37*t2)) + 
               s1*(44 - 20*Power(t1,3) + Power(t1,2)*(5 - 22*t2) + 
                  22*t2 - 109*Power(t2,2) - 22*Power(t2,3) + 
                  t1*(-29 + 98*t2 + 4*Power(t2,2)))) + 
            (-1 + t1)*(s1 - t2)*
             (-26 + 6*Power(s1,4)*(-1 + t1) - 2*Power(t1,4)*(-2 + t2) + 
               19*t2 + 45*Power(t2,2) - 15*Power(t2,3) - 
               2*Power(t2,4) + 
               Power(s1,3)*(1 + 12*Power(t1,2) + 20*t2 - 20*t1*t2) + 
               Power(t1,3)*(14 + 47*t2 + 17*Power(t2,2)) - 
               3*Power(t1,2)*
                (22 + 23*t2 + 18*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(74 + 5*t2 - 8*Power(t2,2) + 17*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s1,2)*(55 + 8*Power(t1,3) - 10*t2 - 
                  24*Power(t2,2) - 2*Power(t1,2)*(13 + 16*t2) + 
                  t1*(-37 + 3*t2 + 24*Power(t2,2))) + 
               s1*(-19 + 2*Power(t1,4) - 102*t2 + 24*Power(t2,2) + 
                  12*Power(t2,3) - Power(t1,3)*(47 + 23*t2) + 
                  Power(t1,2)*(69 + 74*t2 + 35*Power(t2,2)) - 
                  t1*(5 - 51*t2 + 20*Power(t2,2) + 12*Power(t2,3)))) + 
            Power(s,2)*(37 - 2*Power(s1,5) + 2*Power(t1,5) + 43*t2 - 
               143*Power(t2,2) - 81*Power(t2,3) - 5*Power(t2,4) - 
               2*Power(t2,5) + Power(t1,4)*(-29 + 5*t2) + 
               Power(s1,4)*(43 - 16*t1 + 6*t2) + 
               Power(t1,3)*(38 - 25*t2 - 55*Power(t2,2)) - 
               Power(s1,3)*(-96 + 33*Power(t1,2) + t1*(39 - 32*t2) + 
                  145*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(40 + 78*t2 + 81*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(-88 - 101*t2 + 117*Power(t2,2) + 48*Power(t2,3) + 
                  37*Power(t2,4)) + 
               Power(s1,2)*(-151 + 25*Power(t1,3) - 312*t2 + 
                  156*Power(t2,2) - 4*Power(t2,3) + 
                  Power(t1,2)*(-87 + 36*t2) + 
                  3*t1*(71 + 68*t2 + 7*Power(t2,2))) + 
               s1*(-46 - 2*Power(t1,4) + 316*t2 + 297*Power(t2,2) - 
                  49*Power(t2,3) + 6*Power(t2,4) + 
                  Power(t1,3)*(19 + 8*t2) - 
                  6*Power(t1,2)*(13 - 12*t2 + 2*Power(t2,2)) - 
                  t1*(-107 + 396*t2 + 213*Power(t2,2) + 74*Power(t2,3)))\
) + s*(-22 + 4*Power(s1,5)*(-1 + t1) + 4*Power(t1,5)*(-1 + t2) + 81*t2 + 
               139*Power(t2,2) - 42*Power(t2,3) - 14*Power(t2,4) + 
               4*Power(t2,5) + 
               Power(s1,4)*(-65 - 3*Power(t1,2) + t1*(69 - 20*t2) + 
                  20*t2) - Power(t1,4)*(6 + 84*t2 + 23*Power(t2,2)) + 
               Power(t1,3)*(64 + 147*t2 + 45*Power(t2,2) - 
                  25*Power(t2,3)) + 
               Power(t1,2)*(-116 + 23*t2 + 118*Power(t2,2) + 
                  73*Power(t2,3) + 42*Power(t2,4)) - 
               t1*(-84 + 171*t2 + 279*Power(t2,2) + 6*Power(t2,3) + 
                  27*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s1,3)*(55 - 26*Power(t1,3) + 230*t2 - 
                  40*Power(t2,2) - 6*Power(t1,2)*(-7 + 2*t2) + 
                  t1*(-71 - 222*t2 + 40*Power(t2,2))) + 
               Power(s1,2)*(145 - 11*Power(t1,4) - 143*t2 - 
                  279*Power(t2,2) + 40*Power(t2,3) + 
                  3*Power(t1,3)*(1 + 6*t2) + 
                  Power(t1,2)*(172 + 16*t2 + 75*Power(t2,2)) + 
                  t1*(-309 + 109*t2 + 210*Power(t2,2) - 40*Power(t2,3))) \
- s1*(83 + 4*Power(t1,5) + 292*t2 - 130*Power(t2,2) - 128*Power(t2,3) + 
                  20*Power(t2,4) - 2*Power(t1,4)*(41 + 13*t2) + 
                  Power(t1,3)*(139 + 16*t2 - 33*Power(t2,2)) + 
                  Power(t1,2)*
                   (35 + 338*t2 + 131*Power(t2,2) + 102*Power(t2,3)) + 
                  t1*(-179 - 620*t2 + 32*Power(t2,2) + 30*Power(t2,3) - 
                     20*Power(t2,4))))) + 
         Power(s2,3)*(-1 + s + t1)*
          (53 - 190*t1 + 140*Power(t1,2) + 278*Power(t1,3) - 
            567*Power(t1,4) + 370*Power(t1,5) - 82*Power(t1,6) - 
            2*Power(t1,7) - 2*Power(s1,5)*t1*(2 - 3*t1 + Power(t1,2)) - 
            130*t2 + 256*t1*t2 + 157*Power(t1,2)*t2 - 
            677*Power(t1,3)*t2 + 479*Power(t1,4)*t2 - 
            55*Power(t1,5)*t2 - 30*Power(t1,6)*t2 + 134*Power(t2,2) - 
            383*t1*Power(t2,2) + 347*Power(t1,2)*Power(t2,2) + 
            57*Power(t1,3)*Power(t2,2) - 293*Power(t1,4)*Power(t2,2) + 
            138*Power(t1,5)*Power(t2,2) - 71*Power(t2,3) + 
            309*t1*Power(t2,3) - 548*Power(t1,2)*Power(t2,3) + 
            444*Power(t1,3)*Power(t2,3) - 134*Power(t1,4)*Power(t2,3) - 
            22*Power(t2,4) + 85*t1*Power(t2,4) - 
            92*Power(t1,2)*Power(t2,4) + 28*Power(t1,3)*Power(t2,4) - 
            2*Power(t2,5) + 2*t1*Power(t2,5) + 
            6*Power(s,6)*(4 + 3*Power(t1,2) + 2*t2 - Power(t2,2) - 
               2*t1*(3 + t2) + 2*s1*(-2 + t1 + t2)) + 
            Power(s1,4)*(-34 - 5*Power(t1,4) - 
               4*Power(t1,2)*(28 + 3*t2) + Power(t1,3)*(43 + 3*t2) + 
               t1*(107 + 9*t2)) + 
            Power(s1,3)*(-31 + 13*Power(t1,5) + 
               2*Power(t1,4)*(-32 + t2) + 112*t2 + 5*Power(t2,2) - 
               3*Power(t1,3)*(-45 + 37*t2) + 
               t1*(117 - 362*t2 - 14*Power(t2,2)) + 
               Power(t1,2)*(-170 + 363*t2 + 9*Power(t2,2))) - 
            Power(s,5)*(-53*Power(t1,3) + 
               4*Power(s1,2)*(-16 + 9*t1 + 6*t2) + 
               Power(t1,2)*(166 + 13*t2) + 
               2*s1*(-28 + 2*Power(t1,2) + t1*(9 - 35*t2) + 81*t2 - 
                  15*Power(t2,2)) + 
               t1*(-222 + 14*t2 + 37*Power(t2,2)) + 
               3*(40 - 6*t2 - 28*Power(t2,2) + Power(t2,3))) + 
            Power(s1,2)*(19 + 7*Power(t1,6) + 119*t2 - 
               153*Power(t2,2) - 12*Power(t2,3) - 
               Power(t1,5)*(106 + 15*t2) + 
               Power(t1,4)*(601 + 31*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(837 + 375*t2 - 533*Power(t2,2) - 
                  6*Power(t2,3)) - 
               Power(t1,3)*(1116 + 131*t2 - 154*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-242 - 379*t2 + 523*Power(t2,2) + 19*Power(t2,3))) + 
            s1*(92 + 29*Power(t1,6) - 177*t2 - 20*Power(t2,2) + 
               97*Power(t2,3) + 9*Power(t2,4) - 
               9*Power(t1,5)*(-3 + 2*t2) + 
               Power(t1,4)*(-395 - 458*t2 + 172*Power(t2,2)) + 
               Power(t1,3)*(641 + 1379*t2 - 448*Power(t2,2) - 
                  114*Power(t2,3)) + 
               Power(t1,2)*(-258 - 1489*t2 + 333*Power(t2,2) + 
                  374*Power(t2,3) + 3*Power(t2,4)) - 
               t1*(136 - 763*t2 + 37*Power(t2,2) + 353*Power(t2,3) + 
                  12*Power(t2,4))) + 
            Power(s,4)*(228 + 56*Power(t1,4) + 
               2*Power(s1,3)*(15*t1 + 7*(-4 + t2)) - 161*t2 - 
               106*Power(t2,2) + 50*Power(t2,3) + 
               Power(t1,3)*(-259 + 18*t2) + 
               Power(t1,2)*(329 - 210*t2 - 60*Power(t2,2)) + 
               t1*(-322 + 457*t2 + 275*Power(t2,2) - 14*Power(t2,3)) - 
               Power(s1,2)*(233 + 86*Power(t1,2) - 244*t2 + 
                  30*Power(t2,2) + 18*t1*(-19 + 6*t2)) + 
               s1*(71 - 75*Power(t1,3) + 394*t2 - 257*Power(t2,2) + 
                  16*Power(t2,3) + Power(t1,2)*(391 + 98*t2) + 
                  t1*(-561 - 580*t2 + 105*Power(t2,2)))) - 
            Power(s,3)*(282 - 25*Power(t1,5) - 389*t2 + 
               88*Power(t2,2) + 50*Power(t2,3) - 13*Power(t2,4) + 
               2*Power(s1,4)*(-8 + 2*t1 + t2) - 
               9*Power(t1,4)*(-20 + 3*t2) + 
               Power(t1,3)*(282 + 323*t2 + 33*Power(t2,2)) + 
               t1*(118 + 1142*t2 + 61*Power(t2,2) - 105*Power(t2,3)) + 
               Power(t1,2)*(-837 - 1157*t2 - 289*Power(t2,2) + 
                  19*Power(t2,3)) - 
               Power(s1,3)*(190 + 89*Power(t1,2) - 90*t2 + 
                  3*Power(t2,2) + t1*(-352 + 50*t2)) + 
               Power(s1,2)*(-355 + 63*Power(t1,3) + 669*t2 - 
                  160*Power(t2,2) + Power(t1,2)*(-316 + 153*t2) + 
                  2*t1*(215 - 489*t2 + 51*Power(t2,2))) + 
               s1*(315 + 101*Power(t1,4) + 394*t2 - 571*Power(t2,2) + 
                  99*Power(t2,3) + Power(t2,4) - 
                  8*Power(t1,3)*(114 + 5*t2) + 
                  Power(t1,2)*(2366 + 447*t2 - 102*Power(t2,2)) - 
                  2*t1*(881 + 258*t2 - 396*Power(t2,2) + 28*Power(t2,3))\
)) - Power(s,2)*(-290 + 2*Power(s1,5)*t1 - 4*Power(t1,6) + 
               Power(t1,5)*(67 - 8*t2) + 
               Power(s1,4)*(48 + 13*Power(t1,2) + t1*(-71 + t2) - 
                  4*t2) + 482*t2 - 433*Power(t2,2) + 58*Power(t2,3) + 
               45*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,4)*(815 + 172*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(-2624 - 720*t2 - 247*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(2265 + 1121*t2 - 276*Power(t2,2) + 
                  94*Power(t2,3)) - 
               t1*(229 + 1047*t2 - 821*Power(t2,2) + 116*Power(t2,3) + 
                  64*Power(t2,4)) + 
               Power(s1,3)*(298 - 101*Power(t1,3) + 
                  Power(t1,2)*(574 - 60*t2) - 193*t2 + Power(t2,2) + 
                  t1*(-795 + 307*t2 - 6*Power(t2,2))) + 
               Power(s1,2)*(165 + 5*Power(t1,4) - 982*t2 + 
                  320*Power(t2,2) + 12*Power(t2,3) + 
                  Power(t1,3)*(202 + 93*t2) + 
                  Power(t1,2)*(-874 - 1165*t2 + 111*Power(t2,2)) + 
                  t1*(371 + 2146*t2 - 528*Power(t2,2) + Power(t2,3))) + 
               s1*(-347 + 53*Power(t1,5) + 177*t2 + 668*Power(t2,2) - 
                  220*Power(t2,3) - 11*Power(t2,4) - 
                  2*Power(t1,4)*(374 + 3*t2) + 
                  Power(t1,3)*(2534 - 112*t2 - 9*Power(t2,2)) + 
                  Power(t1,2)*
                   (-3066 + 1364*t2 + 557*Power(t2,2) - 64*Power(t2,3)) \
+ t1*(1574 - 1161*t2 - 1328*Power(t2,2) + 356*Power(t2,3) + 
                     2*Power(t2,4)))) - 
            s*(214 + 18*Power(t1,6) + 2*Power(s1,5)*t1*(-3 + 2*t1) - 
               348*t2 + 411*Power(t2,2) - 157*Power(t2,3) - 
               54*Power(t2,4) - 4*Power(t2,5) + 
               Power(t1,5)*(480 + 63*t2) + 
               Power(t1,4)*(-1909 + 59*t2 - 287*Power(t2,2)) + 
               Power(t1,3)*(2453 - 499*t2 + 70*Power(t2,2) + 
                  283*Power(t2,3)) + 
               Power(t1,2)*(-975 + 221*t2 + 885*Power(t2,2) - 
                  630*Power(t2,3) - 79*Power(t2,4)) + 
               t1*(-281 + 504*t2 - 1079*Power(t2,2) + 482*Power(t2,3) + 
                  149*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s1,4)*(14*Power(t1,3) + 2*(-33 + t2) - 
                  2*Power(t1,2)*(49 + 2*t2) + t1*(159 + 8*t2)) - 
               Power(s1,3)*(170 + 55*Power(t1,4) - 229*t2 - 
                  7*Power(t2,2) + Power(t1,3)*(-342 + 26*t2) + 
                  Power(t1,2)*(723 - 328*t2 + 3*Power(t2,2)) + 
                  t1*(-628 + 574*t2 + 8*Power(t2,2))) + 
               Power(s1,2)*(-15*Power(t1,5) + 
                  Power(t1,4)*(346 + 39*t2) + 
                  t2*(577 - 343*t2 - 24*Power(t2,2)) + 
                  Power(t1,3)*(-1655 - 462*t2 + 36*Power(t2,2)) + 
                  Power(t1,2)*
                   (2085 + 1545*t2 - 522*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(-761 - 1765*t2 + 904*Power(t2,2) + 18*Power(t2,3))\
) + s1*(221 + 11*Power(t1,6) - 424*t2 - 269*Power(t2,2) + 
                  234*Power(t2,3) + 19*Power(t2,4) - 
                  2*Power(t1,5)*(125 + 3*t2) + 
                  Power(t1,4)*(757 - 123*t2 + 18*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (405 - 958*t2 + 75*Power(t2,2) + 12*Power(t2,3)) + 
                  Power(t1,2)*
                   (515 - 3369*t2 - 243*Power(t2,2) + 371*Power(t2,3) + 
                     Power(t2,4)) - 
                  2*t1*(222 - 1003*t2 - 355*Power(t2,2) + 
                     319*Power(t2,3) + 7*Power(t2,4))))) + 
         Power(s2,6)*(Power(s,6)*
             (6 + 2*Power(s1,2) - t1 - 9*Power(t1,2) + 
               s1*(-8 + t1 - 9*t2) + 9*t2 + 6*t1*t2 + 3*Power(t2,2)) + 
            Power(s,5)*(29 - 6*Power(s1,3) - 25*Power(t1,3) + 14*t2 - 
               10*Power(t2,2) - 11*Power(t2,3) - 
               Power(t1,2)*(9 + 7*t2) + 
               Power(s1,2)*(36 + 8*t1 + 19*t2) + t1*t2*(39 + 43*t2) + 
               s1*(-69 + 31*Power(t1,2) + t1*(5 - 62*t2) - 26*t2 + 
                  Power(t2,2))) + 
            Power(s,4)*(6*Power(s1,4) - 23*Power(t1,4) - 
               Power(s1,3)*(24 + 27*t1 + 11*t2) - 
               2*Power(t1,3)*(1 + 26*t2) + 
               Power(t1,2)*(-135 + 149*t2 + 99*Power(t2,2)) + 
               t1*(250 + 5*t2 - 85*Power(t2,2) - 14*Power(t2,3)) - 
               2*(45 + 19*t2 + 10*Power(t2,2) + 24*Power(t2,3) + 
                  5*Power(t2,4)) - 
               Power(s1,2)*(54 + 18*Power(t1,2) + 31*t2 + 
                  17*Power(t2,2) - t1*(37 + 121*t2)) + 
               s1*(172 + 82*Power(t1,3) + Power(t1,2)*(7 - 82*t2) + 
                  95*t2 + 99*Power(t2,2) + 32*Power(t2,3) + 
                  t1*(-325 + 28*t2 - 76*Power(t2,2)))) + 
            Power(s,3)*(-2*Power(s1,5) - 7*Power(t1,5) + 
               Power(t1,4)*(5 - 59*t2) + 
               Power(s1,4)*(-16 + 26*t1 + t2) + 
               Power(t1,3)*(-105 + 303*t2 + 70*Power(t2,2)) + 
               Power(t1,2)*(269 - 497*t2 - 26*Power(t2,2) + 
                  31*Power(t2,3)) + 
               t1*(-208 + 115*t2 + 9*Power(t2,2) - 258*Power(t2,3) - 
                  37*Power(t2,4)) + 
               2*(23 + 69*t2 + 19*Power(t2,2) + 45*Power(t2,3) - 
                  4*Power(t2,4) + Power(t2,5)) + 
               Power(s1,3)*(224 - 31*Power(t1,2) + 67*t2 + 
                  13*Power(t2,2) - t1*(59 + 74*t2)) - 
               Power(s1,2)*(59 + 77*Power(t1,3) + 
                  Power(t1,2)*(31 - 222*t2) + 387*t2 + 
                  62*Power(t2,2) + 19*Power(t2,3) - 
                  t1*(258 - 240*t2 + Power(t2,2))) + 
               s1*(-258 + 75*Power(t1,4) + 65*Power(t2,2) + 
                  19*Power(t2,3) + 5*Power(t2,4) + 
                  3*Power(t1,3)*(-37 + 10*t2) - 
                  5*Power(t1,2)*(11 + 2*t2 + 46*Power(t2,2)) + 
                  t1*(349 - 202*t2 + 573*Power(t2,2) + 84*Power(t2,3)))) \
+ Power(s,2)*(-44 + Power(s1,5)*(12 - 8*t1) - 272*t2 + 
               129*Power(t2,2) - 110*Power(t2,3) + 24*Power(t2,4) - 
               5*Power(t1,5)*(1 + 4*t2) + 2*Power(t1,4)*(55 + 119*t2) + 
               Power(t1,3)*(-256 - 586*t2 + 246*Power(t2,2) + 
                  60*Power(t2,3)) - 
               2*Power(t1,2)*
                (-79 - 133*t2 + 291*Power(t2,2) + 195*Power(t2,3) + 
                  22*Power(t2,4)) + 
               t1*(37 + 374*t2 + 207*Power(t2,2) + 475*Power(t2,3) - 
                  45*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s1,4)*(37*Power(t1,2) - 3*(34 + 7*t2) + 
                  t1*(14 + 9*t2)) + 
               Power(s1,3)*(-262 + 4*Power(t1,3) + 
                  Power(t1,2)*(35 - 148*t2) + 267*t2 - 
                  27*Power(t2,2) + t1*(188 + 99*t2 + 35*Power(t2,2))) + 
               Power(s1,2)*(303 - 78*Power(t1,4) + 507*t2 - 
                  234*Power(t2,2) + 69*Power(t2,3) + 
                  Power(t1,3)*(86 + 133*t2) + 
                  Power(t1,2)*(146 - 529*t2 + 111*Power(t2,2)) - 
                  t1*(457 + 6*t2 + 225*Power(t2,2) + 61*Power(t2,3))) + 
               s1*(313 + 23*Power(t1,5) - 435*t2 - 117*Power(t2,2) + 
                  45*Power(t2,3) - 33*Power(t2,4) + 
                  Power(t1,4)*(-139 + 103*t2) + 
                  Power(t1,3)*(230 - 404*t2 - 215*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (89 + 251*t2 + 469*Power(t2,2) + 22*Power(t2,3)) + 
                  t1*(-605 + 234*t2 - 711*Power(t2,2) + 
                     157*Power(t2,3) + 21*Power(t2,4)))) + 
            s*(63 - 4*Power(t1,6) + 
               Power(s1,5)*(6 + 4*t1 - 10*Power(t1,2)) + 86*t2 - 
               280*Power(t2,2) + 109*Power(t2,3) - 58*Power(t2,4) - 
               6*Power(t2,5) + 
               Power(t1,5)*(76 + 50*t2 - 11*Power(t2,2)) + 
               Power(t1,4)*(-201 + 16*t2 + 252*Power(t2,2) + 
                  26*Power(t2,3)) - 
               Power(t1,3)*(-124 + 434*t2 + 813*Power(t2,2) + 
                  182*Power(t2,3) + 17*Power(t2,4)) + 
               Power(t1,2)*(134 + 706*t2 + 634*Power(t2,2) + 
                  337*Power(t2,3) - 76*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(-192 - 424*t2 + 218*Power(t2,2) - 290*Power(t2,3) + 
                  147*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s1,4)*(118 + 20*Power(t1,3) - 57*t2 + 
                  3*Power(t1,2)*(2 + 5*t2) + 2*t1*(-74 + 21*t2)) + 
               Power(s1,3)*(54 + 23*Power(t1,4) + 
                  Power(t1,3)*(51 - 118*t2) - 287*t2 + 
                  159*Power(t2,2) + 
                  t1*(-69 + 204*t2 - 190*Power(t2,2)) + 
                  Power(t1,2)*(-59 + 217*t2 + 31*Power(t2,2))) - 
               Power(s1,2)*(389 + 25*Power(t1,5) + 64*t2 - 
                  170*Power(t2,2) + 177*Power(t2,3) + 
                  Power(t1,4)*(-153 + 11*t2) + 
                  Power(t1,3)*(323 + 246*t2 - 151*Power(t2,2)) + 
                  Power(t1,2)*
                   (102 - 287*t2 + 504*Power(t2,2) + 65*Power(t2,3)) - 
                  t1*(686 + 34*t2 + 159*Power(t2,2) + 242*Power(t2,3))) \
+ s1*(-87 + 678*t2 - 110*Power(t2,2) + 57*Power(t2,3) + 
                  75*Power(t2,4) + Power(t1,5)*(-30 + 44*t2) - 
                  Power(t1,4)*(97 + 428*t2 + 49*Power(t2,2)) + 
                  Power(t1,3)*
                   (558 + 1148*t2 + 421*Power(t2,2) - 36*Power(t2,3)) + 
                  t1*(448 - 932*t2 + 369*Power(t2,2) - 
                     362*Power(t2,3) - 102*Power(t2,4)) + 
                  Power(t1,2)*
                   (-792 - 510*t2 - 631*Power(t2,2) + 357*Power(t2,3) + 
                     27*Power(t2,4)))) + 
            (-1 + t1)*(10 - 4*Power(s1,5)*(-4 + 3*t1 + Power(t1,2)) + 
               Power(t1,5)*(2 - 4*t2) - 39*t2 - 80*Power(t2,2) + 
               78*Power(t2,3) - 40*Power(t2,4) - 4*Power(t2,5) + 
               Power(t1,4)*(2 + 77*t2 + 55*Power(t2,2)) - 
               2*Power(t1,3)*
                (14 + 84*t2 + 59*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(52 + 82*t2 - 9*Power(t2,2) - 
                  59*Power(t2,3) - 39*Power(t2,4)) + 
               t1*(-38 + 52*t2 + 152*Power(t2,2) - 17*Power(t2,3) + 
                  87*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s1,4)*(18 + 3*Power(t1,3) + 
                  7*Power(t1,2)*(-3 + t2) - 77*t2 + t1*(8 + 70*t2)) + 
               Power(s1,3)*(-62 + 9*Power(t1,4) - 12*t2 + 
                  145*Power(t2,2) - Power(t1,3)*(10 + 33*t2) + 
                  Power(t1,2)*(72 + 152*t2 + 9*Power(t2,2)) - 
                  t1*(9 + 139*t2 + 154*Power(t2,2))) + 
               Power(s1,2)*(-101 + Power(t1,4)*(35 - 24*t2) + 188*t2 - 
                  71*Power(t2,2) - 127*Power(t2,3) + 
                  Power(t1,3)*(-37 + 50*t2 + 58*Power(t2,2)) - 
                  Power(t1,2)*
                   (132 + 263*t2 + 283*Power(t2,2) + 23*Power(t2,3)) + 
                  t1*(235 + 49*t2 + 344*Power(t2,2) + 150*Power(t2,3))) \
+ s1*(39 + 4*Power(t1,5) + 183*t2 - 206*Power(t2,2) + 105*Power(t2,3) + 
                  47*Power(t2,4) + 
                  Power(t1,4)*(-77 - 88*t2 + 13*Power(t2,2)) + 
                  Power(t1,3)*
                   (168 + 147*t2 - 30*Power(t2,2) - 28*Power(t2,3)) + 
                  Power(t1,2)*
                   (-82 + 153*t2 + 238*Power(t2,2) + 191*Power(t2,3) + 
                     11*Power(t2,4)) - 
                  t1*(52 + 395*t2 + 15*Power(t2,2) + 300*Power(t2,3) + 
                     58*Power(t2,4))))) + 
         Power(s2,5)*(2*Power(s,7)*
             (2 + 2*Power(t1,2) + t2 + s1*(-2 + t1 + t2) - 
               t1*(3 + 2*t2)) + 
            Power(s,6)*(-36 + 17*Power(t1,2) + 12*Power(t1,3) - 6*t2 + 
               19*Power(t2,2) + 4*Power(t2,3) - 
               6*Power(s1,2)*(-2 + 2*t1 + t2) - 
               2*t1*(-9 + 28*t2 + 8*Power(t2,2)) - 
               s1*(-24 + 13*Power(t1,2) + 20*t2 + Power(t2,2) - 
                  2*t1*(13 + 17*t2))) + 
            Power(s,5)*(28 + 12*Power(t1,4) + 9*t2 + 60*Power(t2,2) + 
               35*Power(t2,3) + 4*Power(t2,4) + 
               2*Power(s1,3)*(-8 + 12*t1 + 3*t2) + 
               Power(t1,3)*(109 + 24*t2) + 
               t1*(-23 + 93*t2 + 2*Power(t2,2)) - 
               2*Power(t1,2)*(56 + 93*t2 + 20*Power(t2,2)) - 
               Power(s1,2)*(3*Power(t1,2) + 29*t1*(1 + 3*t2) - 
                  2*(-51 + 47*t2 + Power(t2,2))) + 
               s1*(110 - 51*Power(t1,3) - 15*t2 - 115*Power(t2,2) - 
                  12*Power(t2,3) + Power(t1,2)*(69 + 48*t2) + 
                  t1*(-106 + 113*t2 + 55*Power(t2,2)))) - 
            Power(s,4)*(-40 - 4*Power(t1,5) - 25*t2 + 203*Power(t2,2) - 
               46*Power(t2,3) + 15*Power(t2,4) + 
               2*Power(s1,4)*(-6 + 10*t1 + t2) - 
               Power(t1,4)*(161 + 32*t2) + 
               Power(s1,3)*(-86 - 47*Power(t1,2) + t1*(26 - 88*t2) + 
                  120*t2 + Power(t2,2)) + 
               6*Power(t1,3)*(75 + 26*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(-646 + 61*t2 + 212*Power(t2,2) + 
                  24*Power(t2,3)) - 
               t1*(-401 + 111*t2 + 545*Power(t2,2) + 182*Power(t2,3) + 
                  12*Power(t2,4)) - 
               Power(s1,2)*(250 + 63*Power(t1,3) - 127*t2 + 
                  176*Power(t2,2) + 8*Power(t2,3) - 
                  Power(t1,2)*(251 + 177*t2) + 
                  t1*(105 + 215*t2 - 68*Power(t2,2))) + 
               s1*(448 + 55*Power(t1,4) - 32*t2 - 20*Power(t2,2) + 
                  53*Power(t2,3) + 5*Power(t2,4) + 
                  8*Power(t1,3)*(9 + 5*t2) + 
                  Power(t1,2)*(90 - 673*t2 - 152*Power(t2,2)) + 
                  2*t1*(-357 + 469*t2 + 197*Power(t2,2) + 6*Power(t2,3))\
)) + (-1 + t1)*(-18 + 2*Power(t1,6) + 
               2*Power(s1,5)*
                (-11 + 13*t1 - 5*Power(t1,2) + 3*Power(t1,3)) - 63*t2 + 
               166*Power(t2,2) - 143*Power(t2,3) + 46*Power(t2,4) - 
               12*Power(t2,5) - 
               Power(t1,5)*(37 + 59*t2 + 2*Power(t2,2)) + 
               Power(t1,4)*(110 + 106*t2 - 127*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(t1,3)*(-110 + 99*t2 + 457*Power(t2,2) + 
                  131*Power(t2,3) + 16*Power(t2,4)) - 
               Power(t1,2)*(-10 + 343*t2 + 359*Power(t2,2) + 
                  275*Power(t2,3) + 15*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(43 + 260*t2 - 135*Power(t2,2) + 289*Power(t2,3) - 
                  64*Power(t2,4) + 24*Power(t2,5)) + 
               Power(s1,4)*(7*Power(t1,4) + t1*(7 - 161*t2) - 
                  Power(t1,3)*(37 + 25*t2) + 4*(-8 + 29*t2) + 
                  Power(t1,2)*(38 + 70*t2)) + 
               Power(s1,3)*(10 + 67*t2 - 205*Power(t2,2) - 
                  6*Power(t1,4)*(-2 + 3*t2) + 
                  Power(t1,2)*(134 - 115*t2 - 125*Power(t2,2)) + 
                  Power(t1,3)*(-142 + 107*t2 + 32*Power(t2,2)) + 
                  t1*(-14 + 27*t2 + 298*Power(t2,2))) + 
               Power(s1,2)*(174 + 22*Power(t1,5) - 176*t2 - 
                  3*Power(t2,2) + 138*Power(t2,3) + 
                  Power(t1,4)*(-174 - 66*t2 + 11*Power(t2,2)) + 
                  Power(t1,3)*
                   (446 + 548*t2 - 64*Power(t2,2) - 13*Power(t2,3)) + 
                  Power(t1,2)*
                   (-284 - 702*t2 + 56*Power(t2,2) + 68*Power(t2,3)) - 
                  t1*(184 - 396*t2 + 102*Power(t2,2) + 193*Power(t2,3))\
) + s1*(73 + Power(t1,5)*(48 - 22*t2) - 331*t2 + 314*Power(t2,2) - 
                  78*Power(t2,3) - 15*Power(t2,4) + 
                  Power(t1,4)*(-52 + 318*t2 + 61*Power(t2,2)) - 
                  Power(t1,3)*
                   (205 + 951*t2 + 557*Power(t2,2) + 22*Power(t2,3)) + 
                  t1*(-311 + 281*t2 - 691*Power(t2,2) + 
                     132*Power(t2,3) + 6*Power(t2,4)) + 
                  Power(t1,2)*
                   (447 + 705*t2 + 873*Power(t2,2) + 36*Power(t2,3) + 
                     9*Power(t2,4)))) + 
            Power(s,3)*(20 + Power(s1,5)*(-4 + 6*t1) - 218*t2 + 
               385*Power(t2,2) - 269*Power(t2,3) + 66*Power(t2,4) - 
               18*Power(t2,5) + 3*Power(t1,5)*(31 + 4*t2) + 
               Power(s1,4)*(-23 - 53*Power(t1,2) + t1*(59 - 31*t2) + 
                  44*t2) + Power(t1,4)*(-535 + 30*t2 + 8*Power(t2,2)) - 
               Power(t1,3)*(-1229 + 888*t2 + 394*Power(t2,2) + 
                  32*Power(t2,3)) + 
               t1*(398 - 804*t2 - 1223*Power(t2,2) + 199*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(t1,2)*(-1205 + 1868*t2 + 1168*Power(t2,2) + 
                  264*Power(t2,3) + 12*Power(t2,4)) + 
               Power(s1,3)*(-396 - 3*Power(t1,3) + 257*t2 - 
                  70*Power(t2,2) + Power(t1,2)*(163 + 210*t2) + 
                  t1*(38 - 461*t2 + 29*Power(t2,2))) + 
               Power(s1,2)*(-80 + 87*Power(t1,4) + 451*t2 - 
                  411*Power(t2,2) + 6*Power(t2,3) - 
                  Power(t1,3)*(320 + 69*t2) + 
                  Power(t1,2)*(770 - 312*t2 - 205*Power(t2,2)) + 
                  t1*(-513 + 488*t2 + 738*Power(t2,2) + 11*Power(t2,3))\
) - s1*(-713 + 19*Power(t1,5) + 331*t2 - 186*Power(t2,2) - 
                  111*Power(t2,3) - 42*Power(t2,4) + 
                  Power(t1,4)*(267 + 98*t2) - 
                  Power(t1,3)*(718 + 939*t2 + 116*Power(t2,2)) + 
                  Power(t1,2)*
                   (294 + 2405*t2 + 167*Power(t2,2) - 36*Power(t2,3)) + 
                  t1*(851 - 2007*t2 + 657*Power(t2,2) + 
                     341*Power(t2,3) + 15*Power(t2,4)))) + 
            s*(-4 + 6*Power(s1,5)*
                (-4 + 7*t1 - 6*Power(t1,2) + 3*Power(t1,3)) - 429*t2 + 
               379*Power(t2,2) - 441*Power(t2,3) + 109*Power(t2,4) - 
               42*Power(t2,5) + 9*Power(t1,6)*(-3 + 2*t2) - 
               Power(t1,5)*(61 + 537*t2 + 44*Power(t2,2)) + 
               Power(t1,4)*(510 + 1797*t2 - 88*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,3)*(-890 - 1920*t2 + 783*Power(t2,2) + 
                  477*Power(t2,3) + 67*Power(t2,4)) - 
               Power(t1,2)*(-625 - 78*t2 + 747*Power(t2,2) + 
                  1317*Power(t2,3) + 52*Power(t2,4) + 42*Power(t2,5)) + 
               t1*(-153 + 993*t2 - 283*Power(t2,2) + 
                  1280*Power(t2,3) - 123*Power(t2,4) + 84*Power(t2,5)) \
+ Power(s1,4)*(Power(t1,4) + t1*(193 - 333*t2) - 
                  Power(t1,3)*(53 + 77*t2) + 2*(-65 + 88*t2) + 
                  2*Power(t1,2)*(-5 + 117*t2)) + 
               Power(s1,3)*(-165 - 25*Power(t1,5) + 401*t2 - 
                  338*Power(t2,2) + Power(t1,4)*(191 + 16*t2) + 
                  Power(t1,2)*(643 + 271*t2 - 384*Power(t2,2)) + 
                  Power(t1,3)*(-681 + 29*t2 + 95*Power(t2,2)) + 
                  t1*(37 - 721*t2 + 627*Power(t2,2))) + 
               Power(s1,2)*(358 - 170*t2 - 353*Power(t2,2) + 
                  202*Power(t2,3) + Power(t1,5)*(91 + 48*t2) - 
                  Power(t1,4)*(333 + 592*t2 + 41*Power(t2,2)) + 
                  t1*(-380 + 1600*t2 + 896*Power(t2,2) - 
                     339*Power(t2,3)) + 
                  Power(t1,3)*
                   (729 + 2543*t2 + 236*Power(t2,2) - 31*Power(t2,3)) \
+ 3*Power(t1,2)*(-155 - 1143*t2 - 244*Power(t2,2) + 56*Power(t2,3))) + 
               s1*(505 - 40*Power(t1,6) - 704*t2 + 790*Power(t2,2) - 
                  27*Power(t2,3) + 26*Power(t2,4) + 
                  Power(t1,5)*(509 - 32*t2 - 19*Power(t2,2)) + 
                  Power(t1,4)*
                   (-1389 + 394*t2 + 398*Power(t2,2) + 24*Power(t2,3)) \
- Power(t1,3)*(-1008 + 1554*t2 + 2371*Power(t2,2) + 279*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  Power(t1,2)*
                   (820 + 1350*t2 + 4171*Power(t2,2) + 
                     523*Power(t2,3) + 60*Power(t2,4)) - 
                  t1*(1413 - 546*t2 + 2969*Power(t2,2) + 
                     245*Power(t2,3) + 81*Power(t2,4)))) + 
            Power(s,2)*(-94 + 18*Power(t1,6) + 
               6*Power(s1,5)*(1 - 4*t1 + 3*Power(t1,2)) + 530*t2 - 
               420*Power(t2,2) + 566*Power(t2,3) - 88*Power(t2,4) + 
               48*Power(t2,5) + 
               4*Power(t1,5)*(-60 + 19*t2 + 2*Power(t2,2)) - 
               Power(t1,4)*(-566 + 1206*t2 + 241*Power(t2,2) + 
                  12*Power(t2,3)) + 
               2*Power(t1,3)*
                (-172 + 1709*t2 + 360*Power(t2,2) + 60*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,2)*(-234 - 2992*t2 - 839*Power(t2,2) + 
                  497*Power(t2,3) + 71*Power(t2,4)) + 
               t1*(328 + 174*t2 + 772*Power(t2,2) - 1166*Power(t2,3) + 
                  45*Power(t2,4) - 48*Power(t2,5)) + 
               Power(s1,4)*(139 - 39*Power(t1,3) + 
                  Power(t1,2)*(38 - 81*t2) - 102*t2 + 3*t1*(-36 + 61*t2)\
) + Power(s1,3)*(417 - 51*Power(t1,4) - 597*t2 + 204*Power(t2,2) + 
                  2*Power(t1,3)*(176 + 81*t2) + 
                  t1*(-148 + 750*t2 - 297*Power(t2,2)) + 
                  Power(t1,2)*(-575 - 437*t2 + 93*Power(t2,2))) + 
               Power(s1,2)*(-210 + 33*Power(t1,5) - 166*t2 + 
                  763*Power(t2,2) - 78*Power(t2,3) + 
                  Power(t1,4)*(-41 + 75*t2) + 
                  Power(t1,3)*(426 - 959*t2 - 187*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (247 - 848*t2 - 291*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(533 - 1479*t2 - 1263*Power(t2,2) + 93*Power(t2,3))\
) + s1*(-803 + 597*t2 - 818*Power(t2,2) - 217*Power(t2,3) - 
                  78*Power(t2,4) - 14*Power(t1,5)*(14 + 3*t2) + 
                  Power(t1,4)*(1139 + 389*t2 + Power(t2,2)) + 
                  Power(t1,3)*
                   (-2224 - 1428*t2 + 449*Power(t2,2) + 60*Power(t2,3)) \
+ Power(t1,2)*(1012 + 1794*t2 - 2430*Power(t2,2) - 545*Power(t2,3) - 
                     15*Power(t2,4)) + 
                  t1*(1072 - 1310*t2 + 2783*Power(t2,2) + 
                     576*Power(t2,3) + 93*Power(t2,4))))) - 
         Power(s2,4)*(2*Power(s,7)*
             (8 + 7*Power(t1,2) + 4*t2 - Power(t2,2) - 6*t1*(2 + t2) + 
               4*s1*(-2 + t1 + t2)) + 
            Power(s,6)*(-100 + 52*Power(t1,3) + 20*t2 + 72*Power(t2,2) + 
               3*Power(t2,3) - 4*Power(s1,2)*(-13 + 9*t1 + 5*t2) - 
               3*Power(t1,2)*(25 + 7*t2) + 
               t1*(126 - 77*t2 - 34*Power(t2,2)) + 
               s1*(48 + 38*t1 - 17*Power(t1,2) - 128*t2 + 83*t1*t2 + 
                  14*Power(t2,2))) + 
            Power(s,5)*(202 + 72*Power(t1,4) + 
               4*Power(s1,3)*(13*t1 + 4*(-4 + t2)) + 
               21*Power(t1,3)*(-2 + t2) - 124*t2 - 43*Power(t2,2) + 
               50*Power(t2,3) + 3*Power(t2,4) - 
               Power(t1,2)*(17 + 430*t2 + 95*Power(t2,2)) - 
               t1*(183 - 554*t2 - 262*Power(t2,2) + Power(t2,3)) - 
               Power(s1,2)*(228 + 83*Power(t1,2) - 308*t2 + 
                  26*Power(t2,2) + 6*t1*(-28 + 27*t2)) + 
               s1*(110 - 135*Power(t1,3) + 226*t2 - 291*Power(t2,2) + 
                  7*Power(t2,3) + Power(t1,2)*(431 + 192*t2) + 
                  t1*(-503 - 343*t2 + 96*Power(t2,2)))) + 
            Power(s,4)*(-252 + 44*Power(t1,5) + 
               Power(s1,4)*(34 - 28*t1 - 4*t2) + 288*t2 - 
               323*Power(t2,2) - 17*Power(t2,3) - 15*Power(t2,4) + 
               Power(t1,4)*(91 + 69*t2) - 
               Power(t1,3)*(923 + 759*t2 + 101*Power(t2,2)) + 
               Power(t1,2)*(1398 + 1392*t2 + 310*Power(t2,2) - 
                  21*Power(t2,3)) + 
               t1*(-358 - 949*t2 + 308*Power(t2,2) + 213*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(s1,3)*(240 + 173*Power(t1,2) - 219*t2 + 
                  14*Power(t2,2) + t1*(-367 + 113*t2)) + 
               Power(s1,2)*(458 + 9*Power(t1,3) - 864*t2 + 
                  315*Power(t2,2) - 16*Power(t2,3) - 
                  Power(t1,2)*(192 + 409*t2) + 
                  t1*(30 + 1287*t2 - 114*Power(t2,2))) + 
               s1*(-560 - 223*Power(t1,4) + t2 + 660*Power(t2,2) - 
                  115*Power(t2,3) + 6*Power(t2,4) + 
                  2*Power(t1,3)*(435 + 64*t2) + 
                  Power(t1,2)*(-1869 + 199*t2 + 229*Power(t2,2)) + 
                  t1*(1741 - 827*t2 - 1124*Power(t2,2) + 20*Power(t2,3)))\
) + Power(s,3)*(340 + 10*Power(t1,6) + Power(s1,5)*(-6 + 4*t1) - 
               491*t2 + 974*Power(t2,2) - 225*Power(t2,3) + 
               92*Power(t2,4) + 6*Power(t2,5) + 
               3*Power(t1,5)*(45 + 17*t2) + 
               Power(s1,4)*(-104 + 206*t1 - 99*Power(t1,2) + 39*t2 - 
                  22*t1*t2) - 
               Power(t1,4)*(1645 + 606*t2 + 43*Power(t2,2)) + 
               Power(t1,3)*(4045 + 791*t2 + 111*Power(t2,2) - 
                  27*Power(t2,3)) + 
               t1*(520 + 144*t2 - 2225*Power(t2,2) - 24*Power(t2,3) - 
                  88*Power(t2,4)) + 
               Power(t1,2)*(-3405 + 111*t2 + 1246*Power(t2,2) + 
                  362*Power(t2,3) + 9*Power(t2,4)) + 
               Power(s1,3)*(-528 + 191*Power(t1,3) + 631*t2 - 
                  97*Power(t2,2) + Power(t1,2)*(-538 + 280*t2) + 
                  t1*(754 - 915*t2 + 50*Power(t2,2))) + 
               Power(s1,2)*(-320 + 165*Power(t1,4) + 1364*t2 - 
                  860*Power(t2,2) + 107*Power(t2,3) - 
                  Power(t1,3)*(1051 + 437*t2) + 
                  Power(t1,2)*(2326 + 1757*t2 - 208*Power(t2,2)) - 
                  t1*(1057 + 2356*t2 - 1080*Power(t2,2) + 
                     50*Power(t2,3))) + 
               s1*(905 - 149*Power(t1,5) + Power(t1,4)*(644 - 58*t2) - 
                  849*t2 - 659*Power(t2,2) + 241*Power(t2,3) - 
                  49*Power(t2,4) + 
                  Power(t1,3)*(-1541 + 1383*t2 + 251*Power(t2,2)) + 
                  Power(t1,2)*
                   (2081 - 4845*t2 - 1585*Power(t2,2) + 18*Power(t2,3)) \
+ t1*(-1940 + 4243*t2 + 1700*Power(t2,2) - 283*Power(t2,3) + 
                     18*Power(t2,4)))) + 
            Power(s,2)*(-390 + 
               4*Power(s1,5)*(4 - 6*t1 + 3*Power(t1,2)) + 661*t2 - 
               1167*Power(t2,2) + 541*Power(t2,3) - 191*Power(t2,4) - 
               20*Power(t2,5) + 12*Power(t1,6)*(5 + t2) - 
               Power(t1,5)*(1134 + 226*t2 + 5*Power(t2,2)) - 
               Power(t1,4)*(-3644 + 675*t2 + 38*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,3)*(-4338 + 3632*t2 + 1431*Power(t2,2) + 
                  371*Power(t2,3) + 3*Power(t2,4)) - 
               Power(t1,2)*(-1590 + 3734*t2 + 3505*Power(t2,2) + 
                  72*Power(t2,3) + 207*Power(t2,4)) + 
               t1*(568 + 330*t2 + 3284*Power(t2,2) - 788*Power(t2,3) + 
                  376*Power(t2,4) + 24*Power(t2,5)) + 
               Power(s1,4)*(190 - 129*Power(t1,3) + 
                  Power(t1,2)*(387 - 42*t2) - 87*t2 + t1*(-454 + 117*t2)\
) + Power(s1,3)*(478 + 55*Power(t1,4) - 1010*t2 + 203*Power(t2,2) + 
                  Power(t1,3)*(-97 + 322*t2) + 
                  t1*(-676 + 2132*t2 - 261*Power(t2,2)) + 
                  3*Power(t1,2)*(66 - 469*t2 + 22*Power(t2,2))) + 
               Power(s1,2)*(15 + 151*Power(t1,5) - 767*t2 + 
                  1271*Power(t2,2) - 229*Power(t2,3) - 
                  3*Power(t1,4)*(378 + 59*t2) + 
                  Power(t1,3)*(3747 + 692*t2 - 200*Power(t2,2)) - 
                  Power(t1,2)*
                   (4291 + 903*t2 - 1350*Power(t2,2) + 54*Power(t2,3)) \
+ 3*t1*(504 + 427*t2 - 832*Power(t2,2) + 97*Power(t2,3))) + 
               s1*(-917 - 36*Power(t1,6) + Power(t1,5)*(107 - 99*t2) + 
                  1305*t2 - 207*Power(t2,2) - 260*Power(t2,3) + 
                  117*Power(t2,4) + 
                  Power(t1,4)*(523 + 1499*t2 + 129*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (-983 - 3297*t2 - 501*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(1091 - 5723*t2 + 51*Power(t2,2) + 
                     442*Power(t2,3) - 147*Power(t2,4)) + 
                  Power(t1,2)*
                   (1198 + 9612*t2 + 903*Power(t2,2) - 123*Power(t2,3) + 
                     18*Power(t2,4)))) - 
            (-1 + t1)*(-18 - 4*Power(s1,5)*
                (-3 + 4*t1 - 2*Power(t1,2) + Power(t1,3)) + 154*t2 - 
               203*Power(t2,2) + 128*Power(t2,3) - 61*Power(t2,4) - 
               8*Power(t2,5) + Power(t1,6)*(21 + 4*t2) + 
               2*Power(t1,5)*(-18 + 98*t2 + 9*Power(t2,2)) - 
               Power(t1,4)*(84 + 753*t2 + 74*Power(t2,2) + 
                  86*Power(t2,3)) + 
               Power(t1,3)*(276 + 889*t2 + 61*Power(t2,2) + 
                  90*Power(t2,3) + 76*Power(t2,4)) - 
               Power(t1,2)*(279 + 169*t2 + 175*Power(t2,2) - 
                  156*Power(t2,3) + 177*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(120 - 321*t2 + 373*Power(t2,2) - 288*Power(t2,3) + 
                  170*Power(t2,4) + 20*Power(t2,5)) + 
               Power(s1,4)*(51 + 15*Power(t1,4) + 
                  Power(t1,2)*(137 - 29*t2) - 53*t2 + 
                  2*Power(t1,3)*(-31 + 5*t2) + t1*(-133 + 72*t2)) + 
               Power(s1,3)*(9 + 16*Power(t1,5) - 221*t2 + 
                  99*Power(t2,2) - Power(t1,4)*(118 + 37*t2) + 
                  t1*(133 + 590*t2 - 150*Power(t2,2)) + 
                  Power(t1,3)*(389 + 197*t2 - 8*Power(t2,2)) + 
                  Power(t1,2)*(-429 - 561*t2 + 59*Power(t2,2))) + 
               Power(s1,2)*(-82 + Power(t1,5)*(20 - 18*t2) + 128*t2 + 
                  230*Power(t2,2) - 95*Power(t2,3) + 
                  Power(t1,4)*(-124 + 175*t2 + 22*Power(t2,2)) + 
                  Power(t1,2)*
                   (54 + 1255*t2 + 519*Power(t2,2) - 75*Power(t2,3)) + 
                  Power(t1,3)*
                   (78 - 865*t2 - 113*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(54 - 675*t2 - 610*Power(t2,2) + 168*Power(t2,3))) \
+ s1*(-166 + 9*Power(t1,6) + 292*t2 - 262*Power(t2,2) + Power(t2,3) + 
                  45*Power(t2,4) - 2*Power(t1,5)*(117 + 31*t2) + 
                  Power(t1,4)*(763 + 301*t2 + 40*Power(t2,2)) - 
                  Power(t1,3)*
                   (809 + 311*t2 - 362*Power(t2,2) + 98*Power(t2,3)) + 
                  t1*(383 - 479*t2 + 816*Power(t2,2) - 17*Power(t2,3) - 
                     94*Power(t2,4)) + 
                  Power(t1,2)*
                   (54 + 259*t2 - 956*Power(t2,2) + 82*Power(t2,3) + 
                     49*Power(t2,4)))) + 
            s*(7*Power(t1,7) + 
               2*Power(s1,5)*(-11 + 20*t1 - 15*Power(t1,2) + 
                  6*Power(t1,3)) - 2*Power(t1,6)*(153 + 19*t2) + 
               Power(t1,5)*(1131 - 800*t2 - 47*Power(t2,2)) + 
               2*Power(t1,4)*(-707 + 1828*t2 + 314*Power(t2,2) + 
                  129*Power(t2,3)) - 
               Power(t1,3)*(-221 + 5254*t2 + 1729*Power(t2,2) + 
                  241*Power(t2,3) + 210*Power(t2,4)) + 
               2*(95 - 267*t2 + 352*Power(t2,2) - 225*Power(t2,3) + 
                  92*Power(t2,4) + 11*Power(t2,5)) + 
               Power(t1,2)*(954 + 2396*t2 + 2466*Power(t2,2) - 
                  662*Power(t2,3) + 537*Power(t2,4) + 30*Power(t2,5)) - 
               t1*(783 - 574*t2 + 2022*Power(t2,2) - 1095*Power(t2,3) + 
                  513*Power(t2,4) + 52*Power(t2,5)) + 
               Power(s1,4)*(-73*Power(t1,4) + t1*(487 - 188*t2) + 
                  Power(t1,3)*(292 - 34*t2) + 
                  9*Power(t1,2)*(-61 + 13*t2) + 3*(-53 + 35*t2)) + 
               Power(s1,3)*(-165 - 31*Power(t1,5) + 755*t2 - 
                  219*Power(t2,2) + 16*Power(t1,4)*(17 + 11*t2) + 
                  Power(t1,2)*(709 + 2259*t2 - 231*Power(t2,2)) + 
                  Power(t1,3)*(-823 - 945*t2 + 38*Power(t2,2)) + 
                  t1*(38 - 2237*t2 + 412*Power(t2,2))) + 
               Power(s1,2)*(117 + 42*Power(t1,6) - 59*t2 - 
                  858*Power(t2,2) + 233*Power(t2,3) + 
                  Power(t1,5)*(-411 + 11*t2) + 
                  Power(t1,4)*(1823 - 279*t2 - 102*Power(t2,2)) + 
                  t1*(-616 + 982*t2 + 2496*Power(t2,2) - 
                     472*Power(t2,3)) + 
                  Power(t1,3)*
                   (-2963 + 1629*t2 + 720*Power(t2,2) - 22*Power(t2,3)) \
+ Power(t1,2)*(2008 - 2284*t2 - 2268*Power(t2,2) + 261*Power(t2,3))) - 
               s1*(-614 + 879*t2 - 655*Power(t2,2) - 78*Power(t2,3) + 
                  119*Power(t2,4) + Power(t1,6)*(53 + 30*t2) - 
                  Power(t1,5)*(989 + 592*t2 + 25*Power(t2,2)) + 
                  Power(t1,4)*
                   (3422 + 3165*t2 + 290*Power(t2,2) + Power(t2,3)) + 
                  t1*(1069 - 3052*t2 + 2034*Power(t2,2) + 
                     233*Power(t2,3) - 260*Power(t2,4)) - 
                  Power(t1,3)*
                   (4248 + 5968*t2 - 459*Power(t2,2) + 143*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  Power(t1,2)*
                   (1307 + 5538*t2 - 2103*Power(t2,2) - 21*Power(t2,3) + 
                     147*Power(t2,4))))))*R1q(s2))/
     (Power(-1 + s2,3)*s2*(-1 + t1)*Power(-1 + s + t1,3)*(s - s2 + t1)*
       (-1 + s1 + t1 - t2)*(-1 + t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,2)) - 
    (8*(2*Power(s,12)*(-1 + s2)*Power(t1 - t2,2) + 
         Power(s,11)*(t1 - t2)*
          (6*t1 - 7*Power(t1,2) - 14*t2 - 2*t1*t2 + 9*Power(t2,2) - 
            2*s1*(-2 - 6*t1 + s2*(2 + 7*t1 - 5*t2) + 4*t2) + 
            Power(s2,2)*(4 - 18*t1 + 14*t2) + 
            s2*(-4 + 9*Power(t1,2) - 2*t1*(-7 + t2) - 2*t2 - 
               7*Power(t2,2))) + 
         Power(s,10)*(2 - 19*t1 + 21*Power(t1,2) + Power(t1,3) - 
            7*Power(t1,4) + 3*t2 + 10*t1*t2 + 5*Power(t1,2)*t2 - 
            14*Power(t1,3)*t2 - 59*Power(t2,2) - 57*t1*Power(t2,2) + 
            30*Power(t1,2)*Power(t2,2) + 51*Power(t2,3) + 
            10*t1*Power(t2,3) - 19*Power(t2,4) + 
            2*Power(s2,3)*(-2 + 34*Power(t1,2) + 6*t2 + 
               16*Power(t2,2) - t1*(11 + 49*t2)) + 
            Power(s2,2)*(12 - 77*Power(t1,3) + 19*t2 + 34*Power(t2,2) - 
               52*Power(t2,3) + Power(t1,2)*(4 + 90*t2) + 
               t1*(-11 - 68*t2 + 39*Power(t2,2))) + 
            s2*(-10 + 16*Power(t1,4) + Power(t1,3)*(69 - 4*t2) - 
               38*t2 - 25*Power(t2,2) + 30*Power(t2,3) + 
               10*Power(t2,4) - 
               Power(t1,2)*(107 + 52*t2 + 30*Power(t2,2)) + 
               t1*(56 + 188*t2 - 47*Power(t2,2) + 8*Power(t2,3))) - 
            Power(s1,2)*(-2 + 4*Power(s2,3) + 30*Power(t1,2) - 7*t2 + 
               12*Power(t2,2) + Power(s2,2)*(-12 + 9*t1 + 5*t2) - 
               5*t1*(-5 + 8*t2) + 
               s2*(10 - 42*Power(t1,2) + 6*t2 - 20*Power(t2,2) + 
                  t1*(-38 + 60*t2))) + 
            s1*(-6 + 40*Power(t1,3) + 24*t2 - 56*Power(t2,2) + 
               31*Power(t2,3) + Power(s2,3)*(10 - 8*t1 + 14*t2) - 
               Power(t1,2)*(50 + 17*t2) + 
               t1*(14 + 134*t2 - 54*Power(t2,2)) + 
               Power(s2,2)*(-30 + 101*Power(t1,2) + t1*(50 - 152*t2) - 
                  32*t2 + 75*Power(t2,2)) + 
               s2*(26 - 63*Power(t1,3) + 2*t2 - 9*Power(t2,2) - 
                  30*Power(t2,3) + Power(t1,2)*(-49 + 64*t2) + 
                  t1*(-64 + 6*t2 + 29*Power(t2,2))))) + 
         Power(s,9)*(-19 + 127*t1 - 314*Power(t1,2) + 220*Power(t1,3) - 
            36*Power(t1,4) + 2*Power(t1,5) - 73*t2 + 231*t1*t2 - 
            248*Power(t1,2)*t2 + 78*Power(t1,3)*t2 - 19*Power(t1,4)*t2 + 
            99*Power(t2,2) - 54*t1*Power(t2,2) + 
            44*Power(t1,2)*Power(t2,2) - 28*Power(t1,3)*Power(t2,2) - 
            162*Power(t2,3) - 214*t1*Power(t2,3) + 
            82*Power(t1,2)*Power(t2,3) + 128*Power(t2,4) - 
            14*t1*Power(t2,4) - 23*Power(t2,5) + 
            2*Power(s2,4)*(12 - 72*Power(t1,2) + 9*t2 - 3*Power(t2,2) + 
               t1*(25 + 68*t2)) + 
            Power(s2,3)*(-72 + 263*Power(t1,3) - 189*t2 - 
               140*Power(t2,2) + 145*Power(t2,3) - 
               2*Power(t1,2)*(73 + 127*t2) + 
               t1*(100 + 416*t2 - 138*Power(t2,2))) + 
            Power(s1,3)*(-4 + 13*Power(s2,3) + 40*Power(t1,2) + 
               t1*(54 - 40*t2) + t2 + 8*Power(t2,2) + 
               Power(s2,2)*(-34 + 48*t1 + 23*t2) - 
               s2*(-23 + 70*Power(t1,2) + t1*(118 - 80*t2) + 12*t2 + 
                  20*Power(t2,2))) - 
            Power(s2,2)*(-33 + 133*Power(t1,4) + 
               Power(t1,3)*(66 - 59*t2) - 64*t2 + 132*Power(t2,2) - 
               5*Power(t2,3) + 74*Power(t2,4) + 
               Power(t1,2)*(-181 + 82*t2 - 279*Power(t2,2)) + 
               t1*(120 + 303*t2 + 117*Power(t2,2) + 131*Power(t2,3))) + 
            s2*(36 + 14*Power(t1,5) + 210*t2 + 254*Power(t2,2) - 
               99*Power(t2,3) + 79*Power(t2,4) + 10*Power(t2,5) + 
               2*Power(t1,4)*(71 + 7*t2) - 
               Power(t1,3)*(439 + 81*t2 + 40*Power(t2,2)) + 
               Power(t1,2)*(484 + 555*t2 - 221*Power(t2,2) - 
                  8*Power(t2,3)) + 
               t1*(-183 - 614*t2 + 471*Power(t2,2) + 81*Power(t2,3) + 
                  10*Power(t2,4))) + 
            Power(s1,2)*(-27 + 26*Power(s2,4) - 93*Power(t1,3) + 
               Power(s2,3)*(-90 + 77*t1 - 26*t2) - 50*t2 + 
               79*Power(t2,2) - 39*Power(t2,3) + 
               2*Power(t1,2)*(48 + 5*t2) + 
               t1*(78 - 393*t2 + 106*Power(t2,2)) + 
               Power(s2,2)*(63 - 272*Power(t1,2) + 79*t2 - 
                  181*Power(t2,2) + t1*(-269 + 199*t2)) + 
               s2*(34 + 189*Power(t1,3) + Power(t1,2)*(131 - 153*t2) + 
                  3*t2 + 35*Power(t2,2) + 50*Power(t2,3) + 
                  t1*(120 + 312*t2 - 70*Power(t2,2)))) + 
            s1*(64 + 34*Power(t1,4) + 
               Power(s2,4)*(-64 + 50*t1 - 90*t2) - 15*t2 + 
               243*Power(t2,2) - 208*Power(t2,3) + 54*Power(t2,4) + 
               48*Power(t1,3)*(-3 + 2*t2) + 
               Power(t1,2)*(283 + 70*t2 - 132*Power(t2,2)) - 
               2*t1*(83 + 143*t2 - 263*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s2,3)*(177 - 341*Power(t1,2) + 142*t2 - 
                  177*Power(t2,2) + 48*t1*(-3 + 8*t2)) + 
               Power(s2,2)*(-62 + 427*Power(t1,3) + 
                  Power(t1,2)*(94 - 450*t2) + 59*t2 - 114*Power(t2,2) + 
                  232*Power(t2,3) + t1*(68 + 624*t2 + 3*Power(t2,2))) - 
               s2*(121 + 112*Power(t1,4) + Power(t1,3)*(179 - 16*t2) + 
                  144*t2 - 52*Power(t2,2) + 102*Power(t2,3) + 
                  40*Power(t2,4) - 
                  2*Power(t1,2)*(-11 + 9*t2 + 78*Power(t2,2)) + 
                  t1*(-228 + 746*t2 + 193*Power(t2,2) + 20*Power(t2,3))))\
) - Power(s,8)*(-74 + 264*t1 - 934*Power(t1,2) + 1374*Power(t1,3) - 
            629*Power(t1,4) + 35*Power(t1,5) - 8*Power(t1,6) - 237*t2 + 
            931*t1*t2 - 998*Power(t1,2)*t2 + 371*Power(t1,3)*t2 + 
            44*Power(t1,4)*t2 - 24*Power(t1,5)*t2 - 39*Power(t2,2) - 
            644*t1*Power(t2,2) + 1503*Power(t1,2)*Power(t2,2) - 
            745*Power(t1,3)*Power(t2,2) + 107*Power(t1,4)*Power(t2,2) - 
            408*Power(t2,3) - 695*t1*Power(t2,3) + 
            693*Power(t1,2)*Power(t2,3) - 28*Power(t1,3)*Power(t2,3) + 
            386*Power(t2,4) + 162*t1*Power(t2,4) - 
            114*Power(t1,2)*Power(t2,4) - 189*Power(t2,5) + 
            52*t1*Power(t2,5) + 15*Power(t2,6) + 
            2*Power(s2,5)*(30 - 95*Power(t1,2) + 68*t2 + 
               52*Power(t2,2) + t1*(30 + 22*t2)) + 
            Power(s2,4)*(-170 + 481*Power(t1,3) - 531*t2 - 
               298*Power(t2,2) + 171*Power(t2,3) - 
               4*Power(t1,2)*(80 + 71*t2) + 
               t1*(179 + 768*t2 - 269*Power(t2,2))) - 
            Power(s2,3)*(46 + 418*Power(t1,4) + 331*t2 + 
               598*Power(t2,2) - 91*Power(t2,3) + 234*Power(t2,4) - 
               Power(t1,3)*(388 + 197*t2) + 
               Power(t1,2)*(540 + 1091*t2 - 936*Power(t2,2)) + 
               t1*(-416 - 912*t2 + 745*Power(t2,2) + 535*Power(t2,3))) + 
            Power(s2,2)*(247 + 117*Power(t1,5) + 1140*t2 + 
               879*Power(t2,2) + 468*Power(t2,3) + 131*Power(t2,4) + 
               51*Power(t2,5) + Power(t1,4)*(257 + 61*t2) - 
               Power(t1,3)*(1598 + 673*t2 + 588*Power(t2,2)) + 
               Power(t1,2)*(2607 + 3270*t2 + 782*Power(t2,2) + 
                  48*Power(t2,3)) + 
               t1*(-1434 - 3407*t2 + 148*Power(t2,2) + 
                  493*Power(t2,3) + 311*Power(t2,4))) - 
            s2*(3 + 6*Power(t1,6) + 47*t2 - 315*Power(t2,2) + 
               226*Power(t2,3) - 425*Power(t2,4) + 54*Power(t2,5) + 
               10*Power(t2,6) + Power(t1,5)*(159 + 16*t2) - 
               Power(t1,4)*(926 + 163*t2 + 30*Power(t2,2)) + 
               Power(t1,3)*(885 + 495*t2 - 451*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(430 + 162*t2 + 1883*Power(t2,2) - 
                  55*Power(t2,3) - 10*Power(t2,4)) + 
               t1*(-433 - 214*t2 - 901*Power(t2,2) + 845*Power(t2,3) + 
                  456*Power(t2,4))) + 
            Power(s1,4)*(4 + 3*Power(s2,3) + 30*Power(t1,2) + 
               t1*(46 - 20*t2) + 5*t2 + 2*Power(t2,2) + 
               Power(s2,2)*(-6 + 99*t1 + 40*t2) - 
               s2*(5 + 70*Power(t1,2) + 28*t2 + 10*Power(t2,2) - 
                  20*t1*(-8 + 3*t2))) + 
            Power(s1,3)*(-133 + 75*Power(s2,4) - 110*Power(t1,3) + 
               Power(t1,2)*(34 - 30*t2) - 11*t2 + 58*Power(t2,2) - 
               21*Power(t2,3) + 3*Power(s2,3)*(-34 + 90*t1 + 31*t2) + 
               t1*(420 - 529*t2 + 104*Power(t2,2)) - 
               Power(s2,2)*(229 + 501*Power(t1,2) + 72*t2 + 
                  238*Power(t2,2) + t1*(346 + 111*t2)) + 
               s2*(387 + 315*Power(t1,3) + Power(t1,2)*(362 - 190*t2) + 
                  23*t2 + 43*Power(t2,2) + 40*Power(t2,3) + 
                  t1*(-431 + 888*t2 - 90*Power(t2,2)))) + 
            Power(s1,2)*(103 + 74*Power(s2,5) + 57*Power(t1,4) + 
               Power(s2,4)*(-318 + 277*t1 - 244*t2) + 86*t2 + 
               292*Power(t2,2) - 320*Power(t2,3) + 51*Power(t2,4) + 
               Power(t1,3)*(-431 + 270*t2) + 
               Power(t1,2)*(864 + 566*t2 - 228*Power(t2,2)) - 
               t1*(412 + 1783*t2 - 1273*Power(t2,2) + 96*Power(t2,3)) + 
               Power(s2,3)*(66 - 879*Power(t1,2) + 65*t2 - 
                  584*Power(t2,2) + t1*(-457 + 88*t2)) - 
               s2*(645 + 336*Power(t1,4) + Power(t1,3)*(65 - 12*t2) + 
                  909*t2 - 181*Power(t2,2) + 56*Power(t2,3) + 
                  60*Power(t2,4) + 
                  Power(t1,2)*(1235 + 1212*t2 - 330*Power(t2,2)) + 
                  10*t1*(-187 - 15*t2 + 133*Power(t2,2))) + 
               Power(s2,2)*(750 + 1023*Power(t1,3) + 
                  Power(t1,2)*(1031 - 560*t2) + 1015*t2 + 
                  30*Power(t2,2) + 407*Power(t2,3) + 
                  t1*(-1141 + 1875*t2 + 651*Power(t2,2)))) + 
            s1*(155 + 24*Power(t1,5) + 
               8*Power(s2,5)*(-22 + 17*t1 - 31*t2) - 214*t2 + 
               611*Power(t2,2) - 671*Power(t2,3) + 446*Power(t2,4) - 
               47*Power(t2,5) - 7*Power(t1,4)*(-18 + 17*t2) - 
               5*Power(t1,3)*(83 - 104*t2 + 32*Power(t2,2)) + 
               Power(t1,2)*(127 - 1193*t2 - 1076*Power(t2,2) + 
                  342*Power(t2,3)) + 
               t1*(-162 + 675*t2 + 1711*Power(t2,2) - 952*Power(t2,3) - 
                  40*Power(t2,4)) + 
               Power(s2,4)*(452 - 689*Power(t1,2) + 456*t2 - 
                  62*Power(t2,2) + 2*t1*(-58 + 221*t2)) + 
               Power(s2,3)*(159 + 1208*Power(t1,3) + 
                  Power(t1,2)*(241 - 1084*t2) + 367*t2 - 
                  184*Power(t2,2) + 722*Power(t2,3) + 
                  t1*(-775 + 1954*t2 + 427*Power(t2,2))) - 
               Power(s2,2)*(816 + 735*Power(t1,4) + 1477*t2 + 
                  1315*Power(t2,2) + 83*Power(t2,3) + 260*Power(t2,4) - 
                  Power(t1,3)*(263 + 432*t2) + 
                  Power(t1,2)*(1837 + 2622*t2 - 685*Power(t2,2)) + 
                  t1*(-2266 - 1131*t2 + 1709*Power(t2,2) + 
                     950*Power(t2,3))) + 
               s2*(188 + 98*Power(t1,5) + 923*t2 + 813*Power(t2,2) - 
                  624*Power(t2,3) + 95*Power(t2,4) + 40*Power(t2,5) + 
                  Power(t1,4)*(334 + 96*t2) - 
                  2*Power(t1,3)*(519 + 180*t2 + 92*Power(t2,2)) + 
                  Power(t1,2)*
                   (2329 + 4060*t2 + 637*Power(t2,2) - 80*Power(t2,3)) + 
                  2*t1*(-651 - 2141*t2 + 551*Power(t2,2) + 
                     529*Power(t2,3) + 15*Power(t2,4))))) + 
         Power(s,7)*(-78 - 229*t1 + 574*Power(t1,2) + 1008*Power(t1,3) - 
            2249*Power(t1,4) + 920*Power(t1,5) + 38*Power(t1,6) + 
            5*Power(t1,7) + 153*t2 + 92*t1*t2 + 479*Power(t1,2)*t2 - 
            1194*Power(t1,3)*t2 + 498*Power(t1,4)*t2 - 
            179*Power(t1,5)*t2 + 55*Power(t1,6)*t2 - 374*Power(t2,2) - 
            1578*t1*Power(t2,2) + 5629*Power(t1,2)*Power(t2,2) - 
            4627*Power(t1,3)*Power(t2,2) + 
            1101*Power(t1,4)*Power(t2,2) - 45*Power(t1,5)*Power(t2,2) - 
            1005*Power(t2,3) + 160*t1*Power(t2,3) + 
            443*Power(t1,2)*Power(t2,3) + 91*Power(t1,3)*Power(t2,3) - 
            180*Power(t1,4)*Power(t2,3) + 566*Power(t2,4) + 
            1419*t1*Power(t2,4) - 1487*Power(t1,2)*Power(t2,4) + 
            189*Power(t1,3)*Power(t2,4) - 717*Power(t2,5) + 
            308*t1*Power(t2,5) + 33*Power(t1,2)*Power(t2,5) + 
            128*Power(t2,6) - 53*t1*Power(t2,6) - 4*Power(t2,7) + 
            Power(s2,6)*(80 - 162*Power(t1,2) + t1*(40 - 126*t2) + 
               270*t2 + 218*Power(t2,2)) + 
            Power(s2,5)*(-194 + 521*Power(t1,3) + 
               2*Power(t1,2)*(-138 + t2) - 775*t2 - 480*Power(t2,2) + 
               7*Power(t2,3) + t1*(66 + 616*t2 - 272*Power(t2,2))) - 
            Power(s1,5)*(-16 + 40*Power(s2,3) - 12*Power(t1,2) + 
               4*t1*(-1 + t2) + t2 - 10*Power(s2,2)*(8 + 9*t1 + 3*t2) + 
               s2*(61 + 42*Power(t1,2) + t1*(80 - 24*t2) + 12*t2 + 
                  2*Power(t2,2))) - 
            Power(s2,4)*(359 + 671*Power(t1,4) + 1242*t2 + 
               936*Power(t2,2) - 132*Power(t2,3) + 387*Power(t2,4) - 
               Power(t1,3)*(1085 + 241*t2) + 
               Power(t1,2)*(2068 + 2730*t2 - 1626*Power(t2,2)) + 
               t1*(-1818 - 3738*t2 + 1387*Power(t2,2) + 
                  1097*Power(t2,3))) + 
            Power(s2,3)*(614 + 347*Power(t1,5) + 2273*t2 + 
               2005*Power(t2,2) + 1711*Power(t2,3) + 135*Power(t2,4) + 
               155*Power(t2,5) + Power(t1,4)*(-437 + 68*t2) - 
               Power(t1,3)*(1123 + 1281*t2 + 2255*Power(t2,2)) + 
               Power(t1,2)*(4013 + 6676*t2 + 5503*Power(t2,2) + 
                  341*Power(t2,3)) + 
               t1*(-3130 - 7230*t2 - 3413*Power(t2,2) + 
                  1056*Power(t2,3) + 1440*Power(t2,4))) - 
            Power(s2,2)*(-351 + 53*Power(t1,6) - 2218*t2 - 
               2053*Power(t2,2) - 753*Power(t2,3) - 118*Power(t2,4) + 
               125*Power(t2,5) + 17*Power(t2,6) + 
               9*Power(t1,5)*(51 + 13*t2) - 
               Power(t1,4)*(5101 + 2402*t2 + 715*Power(t2,2)) + 
               Power(t1,3)*(10348 + 8510*t2 + 34*Power(t2,2) - 
                  364*Power(t2,3)) + 
               Power(t1,2)*(-6950 - 10030*t2 + 5418*Power(t2,2) + 
                  3071*Power(t2,3) + 645*Power(t2,4)) + 
               t1*(1794 + 6946*t2 - 1287*Power(t2,2) - 
                  509*Power(t2,3) + 873*Power(t2,4) + 247*Power(t2,5))) \
+ s2*(-378 + Power(t1,7) - 2521*t2 - 1245*Power(t2,2) - 
               540*Power(t2,3) + 945*Power(t2,4) - 444*Power(t2,5) - 
               35*Power(t2,6) + 7*Power(t2,7) + 
               5*Power(t1,6)*(21 + t2) - 
               Power(t1,5)*(1230 + 307*t2 + 33*Power(t2,2)) + 
               Power(t1,4)*(-1181 - 955*t2 - 788*Power(t2,2) + 
                  27*Power(t2,3)) + 
               Power(t1,3)*(8549 + 7834*t2 + 3154*Power(t2,2) - 
                  309*Power(t2,3) + 45*Power(t2,4)) - 
               Power(t1,2)*(8860 + 12000*t2 + 2228*Power(t2,2) - 
                  4737*Power(t2,3) - 794*Power(t2,4) + 63*Power(t2,5)) \
+ t1*(3093 + 8457*t2 + 1903*Power(t2,2) - 4698*Power(t2,3) - 
                  1134*Power(t2,4) + 540*Power(t2,5) + 11*Power(t2,6))) \
+ Power(s1,4)*(-135 + 19*Power(s2,4) - 65*Power(t1,3) - 36*t2 + 
               33*Power(t2,2) - 4*Power(t2,3) - 
               Power(t1,2)*(34 + 55*t2) + 
               Power(s2,3)*(193 + 467*t1 + 364*t2) + 
               t1*(526 - 269*t2 + 51*Power(t2,2)) - 
               Power(s2,2)*(705 + 682*Power(t1,2) + 395*t2 + 
                  152*Power(t2,2) + 4*t1*(-43 + 115*t2)) + 
               s2*(606 + 315*Power(t1,3) + Power(t1,2)*(580 - 125*t2) + 
                  133*t2 - 21*Power(t2,2) + 15*Power(t2,3) + 
                  t1*(-1287 + 872*t2 - 65*Power(t2,2)))) + 
            Power(s1,3)*(-456 + 189*Power(s2,5) + 20*Power(t1,4) + 
               498*t2 + 353*Power(t2,2) - 221*Power(t2,3) + 
               16*Power(t2,4) + 4*Power(t1,3)*(-89 + 100*t2) + 
               Power(s2,4)*(-182 + 725*t1 + 105*t2) - 
               2*Power(t1,2)*(61 - 494*t2 + 96*Power(t2,2)) - 
               2*t1*(-582 + 1686*t2 - 553*Power(t2,2) + 
                  38*Power(t2,3)) - 
               Power(s2,3)*(1281 + 1663*Power(t1,2) + 980*t2 + 
                  1181*Power(t2,2) + 3*t1*(-69 + 524*t2)) + 
               Power(s2,2)*(1697 + 1465*Power(t1,3) + 2273*t2 + 
                  475*Power(t2,2) + 293*Power(t2,3) + 
                  Power(t1,2)*(2523 + 650*t2) + 
                  2*t1*(-1946 + 680*t2 + 789*Power(t2,2))) + 
               s2*(93 - 560*Power(t1,4) + Power(t1,3)*(17 - 40*t2) - 
                  1882*t2 + 5*Power(t2,2) + 170*Power(t2,3) - 
                  40*Power(t2,4) + 
                  4*Power(t1,2)*(-231 - 905*t2 + 90*Power(t2,2)) + 
                  2*t1*(762 + 2288*t2 - 1009*Power(t2,2) + 
                     20*Power(t2,3)))) + 
            Power(s1,2)*(1056 + 120*Power(s2,6) + 102*Power(t1,5) + 
               Power(s2,5)*(-680 + 565*t1 - 676*t2) + 
               Power(t1,4)*(608 - 310*t2) + 289*t2 - 17*Power(t2,2) - 
               1379*Power(t2,3) + 475*Power(t2,4) - 24*Power(t2,5) + 
               Power(t1,3)*(-3614 + 508*t2 - 360*Power(t2,2)) + 
               Power(t1,2)*(5352 + 2134*t2 - 3494*Power(t2,2) + 
                  534*Power(t2,3)) - 
               t1*(3338 + 3674*t2 - 6665*Power(t2,2) + 
                  1113*Power(t2,3) + 38*Power(t2,4)) - 
               2*Power(s2,4)*
                (26 + 849*Power(t1,2) + 61*t2 + 432*Power(t2,2) + 
                  t1*(-55 + 376*t2)) + 
               Power(s2,3)*(2538 + 2388*Power(t1,3) + 
                  Power(t1,2)*(3520 - 138*t2) + 4057*t2 + 
                  972*Power(t2,2) + 1585*Power(t2,3) + 
                  t1*(-5582 + 3033*t2 + 4075*Power(t2,2))) - 
               Power(s2,2)*(697 + 1677*Power(t1,4) + 
                  Power(t1,3)*(53 - 691*t2) + 3171*t2 + 
                  2715*Power(t2,2) + 210*Power(t2,3) + 
                  267*Power(t2,4) + 
                  Power(t1,2)*(4016 + 11341*t2 + 1068*Power(t2,2)) + 
                  t1*(-4239 - 9687*t2 + 2715*Power(t2,2) + 
                     2383*Power(t2,3))) + 
               s2*(-2283 + 294*Power(t1,5) - 295*t2 + 
                  2535*Power(t2,2) - 609*Power(t2,3) - 
                  264*Power(t2,4) + 50*Power(t2,5) + 
                  Power(t1,4)*(-241 + 282*t2) + 
                  Power(t1,3)*(2903 + 897*t2 - 320*Power(t2,2)) + 
                  Power(t1,2)*
                   (-4541 + 5284*t2 + 5264*Power(t2,2) - 
                     240*Power(t2,3)) + 
                  t1*(4724 - 7278*t2 - 5533*Power(t2,2) + 
                     2280*Power(t2,3) + 30*Power(t2,4)))) + 
            s1*(-265 - 56*Power(t1,6) + 
               10*Power(s2,6)*(-27 + 21*t1 - 38*t2) - 1642*t2 + 
               1538*Power(t2,2) - 912*Power(t2,3) + 1763*Power(t2,4) - 
               414*Power(t2,5) + 16*Power(t2,6) - 
               4*Power(t1,5)*(44 + 31*t2) + 
               Power(t1,4)*(-586 - 471*t2 + 540*Power(t2,2)) + 
               Power(t1,3)*(4808 + 5552*t2 - 866*Power(t2,2) - 
                  164*Power(t2,3)) - 
               Power(t1,2)*(5544 + 10326*t2 + 1027*Power(t2,2) - 
                  4027*Power(t2,3) + 332*Power(t2,4)) + 
               6*t1*(323 + 1121*t2 + 185*Power(t2,2) - 
                  873*Power(t2,3) - 6*Power(t2,4) + 20*Power(t2,5)) + 
               Power(s2,5)*(667 - 881*Power(t1,2) + 1022*t2 + 
                  435*Power(t2,2) + t1*(92 + 52*t2)) + 
               Power(s2,4)*(752 + 1853*Power(t1,3) + 
                  Power(t1,2)*(619 - 1108*t2) + 756*t2 + 
                  27*Power(t2,2) + 1127*Power(t2,3) + 
                  2*t1*(-1358 + 1122*t2 + 697*Power(t2,2))) - 
               Power(s2,3)*(1888 + 1863*Power(t1,4) + 4714*t2 + 
                  4553*Power(t2,2) + 320*Power(t2,3) + 
                  883*Power(t2,4) - Power(t1,3)*(1933 + 1610*t2) + 
                  Power(t1,2)*(5626 + 11054*t2 - 906*Power(t2,2)) + 
                  t1*(-6246 - 9986*t2 + 3800*Power(t2,2) + 
                     4410*Power(t2,3))) + 
               Power(s2,2)*(-972 + 653*Power(t1,5) + 81*t2 + 
                  759*Power(t2,2) + 1029*Power(t2,3) + 
                  175*Power(t2,4) + 113*Power(t2,5) - 
                  Power(t1,4)*(843 + 26*t2) + 
                  Power(t1,3)*(2944 + 902*t2 - 2035*Power(t2,2)) + 
                  Power(t1,2)*
                   (-1182 + 10520*t2 + 11334*Power(t2,2) + 
                     1745*Power(t2,3)) + 
                  t1*(881 - 8236*t2 - 6218*Power(t2,2) + 
                     2056*Power(t2,3) + 1422*Power(t2,4))) + 
               s2*(1905 - 42*Power(t1,6) + 4322*t2 + 1132*Power(t2,2) - 
                  2204*Power(t2,3) + 976*Power(t2,4) + 162*Power(t2,5) - 
                  30*Power(t2,6) - Power(t1,5)*(393 + 104*t2) + 
                  Power(t1,4)*(4131 + 1434*t2 + 106*Power(t2,2)) - 
                  Power(t1,3)*(13648 + 9594*t2 + 527*Power(t2,2)) + 
                  Power(t1,2)*
                   (14178 + 13988*t2 - 8555*Power(t2,2) - 
                     3018*Power(t2,3) + 110*Power(t2,4)) - 
                  t1*(6853 + 11544*t2 - 9576*Power(t2,2) - 
                     3378*Power(t2,3) + 1594*Power(t2,4) + 40*Power(t2,5)\
)))) + Power(s,6)*(-334 + 2487*t1 - 8472*Power(t1,2) + 
            12140*Power(t1,3) - 6033*Power(t1,4) - 817*Power(t1,5) + 
            953*Power(t1,6) + 75*Power(t1,7) + Power(t1,8) - 2237*t2 + 
            7881*t1*t2 - 14711*Power(t1,2)*t2 + 16578*Power(t1,3)*t2 - 
            8996*Power(t1,4)*t2 + 1307*Power(t1,5)*t2 + 
            45*Power(t1,6)*t2 + 34*Power(t1,7)*t2 + 803*Power(t2,2) + 
            1498*t1*Power(t2,2) - 8572*Power(t1,2)*Power(t2,2) + 
            10377*Power(t1,3)*Power(t2,2) - 
            4648*Power(t1,4)*Power(t2,2) + 490*Power(t1,5)*Power(t2,2) + 
            64*Power(t1,6)*Power(t2,2) + 2421*Power(t2,3) - 
            6570*t1*Power(t2,3) + 8928*Power(t1,2)*Power(t2,3) - 
            5634*Power(t1,3)*Power(t2,3) + 
            1489*Power(t1,4)*Power(t2,3) - 214*Power(t1,5)*Power(t2,3) - 
            135*Power(t2,4) - 4900*t1*Power(t2,4) + 
            6449*Power(t1,2)*Power(t2,4) - 
            2012*Power(t1,3)*Power(t2,4) + 5*Power(t1,4)*Power(t2,4) + 
            1568*Power(t2,5) - 801*t1*Power(t2,5) - 
            528*Power(t1,2)*Power(t2,5) + 182*Power(t1,3)*Power(t2,5) - 
            482*Power(t2,6) + 411*t1*Power(t2,6) - 
            54*Power(t1,2)*Power(t2,6) + 30*Power(t2,7) - 
            18*t1*Power(t2,7) + 
            2*Power(s2,7)*(44*Power(t1,2) + t1*(-7 + 99*t2) - 
               6*(5 + 23*t2 + 18*Power(t2,2))) + 
            Power(s2,6)*(96 - 343*Power(t1,3) + 
               Power(t1,2)*(68 - 296*t2) + 687*t2 + 618*Power(t2,2) + 
               213*Power(t2,3) + t1*(135 - 176*t2 + 61*Power(t2,2))) + 
            Power(s2,5)*(676 + 600*Power(t1,4) + 1695*t2 + 
               384*Power(t2,2) - 359*Power(t2,3) + 328*Power(t2,4) - 
               Power(t1,3)*(1181 + 49*t2) + 
               Power(t1,2)*(2725 + 3227*t2 - 1717*Power(t2,2)) + 
               t1*(-2724 - 5247*t2 + 1621*Power(t2,2) + 
                  1468*Power(t2,3))) - 
            Power(s2,4)*(779 + 504*Power(t1,5) + 2339*t2 + 
               2729*Power(t2,2) + 1890*Power(t2,3) - 132*Power(t2,4) + 
               286*Power(t2,5) - Power(t1,4)*(1744 + 27*t2) + 
               Power(t1,3)*(2393 - 992*t2 - 4172*Power(t2,2)) + 
               Power(t1,2)*(991 + 6900*t2 + 11844*Power(t2,2) + 
                  717*Power(t2,3)) + 
               t1*(-2737 - 8138*t2 - 9003*Power(t2,2) + 
                  1412*Power(t2,3) + 3112*Power(t2,4))) + 
            Power(s2,3)*(-1187 + 152*Power(t1,6) - 6164*t2 - 
               4563*Power(t2,2) - 1393*Power(t2,3) + 893*Power(t2,4) + 
               247*Power(t2,5) + 16*Power(t2,6) + 
               Power(t1,5)*(50 + 257*t2) - 
               Power(t1,4)*(8883 + 7414*t2 + 2877*Power(t2,2)) + 
               Power(t1,3)*(24844 + 25993*t2 + 5532*Power(t2,2) - 
                  1741*Power(t2,3)) + 
               Power(t1,2)*(-23369 - 32638*t2 + 3760*Power(t2,2) + 
                  12338*Power(t2,3) + 3005*Power(t2,4)) + 
               t1*(8605 + 20895*t2 + 512*Power(t2,2) - 
                  7770*Power(t2,3) - 897*Power(t2,4) + 1272*Power(t2,5))\
) + Power(s2,2)*(427 - 10*Power(t1,7) + 2469*t2 + 2227*Power(t2,2) + 
               1005*Power(t2,3) + 448*Power(t2,4) + 149*Power(t2,5) - 
               11*Power(t2,6) + Power(t2,7) - 
               Power(t1,6)*(456 + 73*t2) + 
               Power(t1,5)*(7905 + 3188*t2 + 528*Power(t2,2)) + 
               Power(t1,4)*(-13260 - 3859*t2 + 4640*Power(t2,2) + 
                  933*Power(t2,3)) - 
               2*Power(t1,3)*
                (1068 + 6627*t2 + 13022*Power(t2,2) + 
                  3063*Power(t2,3) + 339*Power(t2,4)) + 
               Power(t1,2)*(13764 + 21657*t2 + 30150*Power(t2,2) + 
                  673*Power(t2,3) - 3779*Power(t2,4) - 609*Power(t2,5)) \
- t1*(6348 + 11072*t2 + 13041*Power(t2,2) - 2958*Power(t2,3) - 
                  4824*Power(t2,4) + 396*Power(t2,5) + 92*Power(t2,6))) \
+ s2*(1178 + 40*Power(t1,7) + 6023*t2 + 2897*Power(t2,2) + 
               552*Power(t2,3) - 853*Power(t2,4) + 1614*Power(t2,5) + 
               36*Power(t2,6) - 54*Power(t2,7) + 2*Power(t2,8) - 
               3*Power(t1,6)*(335 + 103*t2 + 10*Power(t2,2)) - 
               Power(t1,5)*(6865 + 3699*t2 + 1228*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(t1,4)*(28152 + 19725*t2 + 2504*Power(t2,2) - 
                  433*Power(t2,3) + 130*Power(t2,4)) + 
               Power(t1,3)*(-34354 - 32998*t2 + 8140*Power(t2,2) + 
                  9681*Power(t2,3) + 557*Power(t2,4) - 56*Power(t2,5)) \
+ Power(t1,2)*(18157 + 33449*t2 - 12995*Power(t2,2) - 
                  20145*Power(t2,3) + 907*Power(t2,4) + 
                  1308*Power(t2,5) - 46*Power(t2,6)) + 
               t1*(-5281 - 21758*t2 + 685*Power(t2,2) + 
                  11218*Power(t2,3) - 1545*Power(t2,4) - 
                  2712*Power(t2,5) + 119*Power(t2,6) + 20*Power(t2,7))) \
+ Power(s1,6)*(70*Power(s2,3) + 15*t1 - 2*Power(t1,2) + 2*(-7 + t2) - 
               5*Power(s2,2)*(24 + 3*t1 + t2) + 
               s2*(73 + 14*Power(t1,2) - 6*t2 - 2*t1*(13 + 2*t2))) + 
            Power(s1,5)*(-11 + 156*Power(s2,4) + 12*Power(t1,3) + 
               87*t2 - 12*Power(t2,2) + 5*Power(t1,2)*(-6 + 7*t2) - 
               Power(s2,3)*(457 + 420*t1 + 496*t2) - 
               t1*(124 + 27*t2 + 10*Power(t2,2)) + 
               Power(s2,2)*(646 + 613*Power(t1,2) + 567*t2 + 
                  7*Power(t2,2) + t1*(-712 + 278*t2)) - 
               s2*(279 + 189*Power(t1,3) + Power(t1,2)*(389 - 36*t2) + 
                  319*t2 - 64*Power(t2,2) + 2*Power(t2,3) + 
                  t1*(-1039 + 162*t2 - 25*Power(t2,2)))) + 
            Power(s1,4)*(837 - 61*Power(s2,5) + 55*Power(t1,4) - 
               96*t2 - 361*Power(t2,2) + 58*Power(t2,3) - 
               Power(t1,3)*(41 + 330*t2) - 
               Power(s2,4)*(525 + 1136*t1 + 1026*t2) + 
               Power(t1,2)*(1609 - 343*t2 + 78*Power(t2,2)) + 
               t1*(-2797 + 1763*t2 - 212*Power(t2,2) + 
                  22*Power(t2,3)) + 
               3*Power(s2,3)*
                (676 + 761*Power(t1,2) + 608*t2 + 451*Power(t2,2) + 
                  t1*(-653 + 948*t2)) - 
               Power(s2,2)*(442 + 1517*Power(t1,3) + 2336*t2 + 
                  969*Power(t2,2) - 23*Power(t2,3) + 
                  10*Power(t1,2)*(167 + 220*t2) + 
                  t1*(-3507 - 1960*t2 + 1227*Power(t2,2))) + 
               s2*(-1884 + 560*Power(t1,4) + 1647*t2 + 
                  827*Power(t2,2) - 250*Power(t2,3) + 10*Power(t2,4) + 
                  25*Power(t1,3)*(15 + 4*t2) + 
                  Power(t1,2)*(-2977 + 3820*t2 - 210*Power(t2,2)) + 
                  t1*(3663 - 6394*t2 + 709*Power(t2,2) - 40*Power(t2,3))\
)) + Power(s1,3)*(-24 - 269*Power(s2,6) - 220*Power(t1,5) - 3005*t2 - 
               570*Power(t2,2) + 1183*Power(t2,3) - 152*Power(t2,4) + 
               Power(s2,5)*(487 - 1250*t1 + 105*t2) + 
               Power(t1,4)*(-488 + 430*t2) + 
               Power(t1,3)*(3346 + 332*t2 + 400*Power(t2,2)) - 
               2*Power(t1,2)*
                (2065 + 4858*t2 - 1537*Power(t2,2) + 185*Power(t2,3)) + 
               t1*(1180 + 13550*t2 - 5389*Power(t2,2) + 
                  237*Power(t2,3) + 12*Power(t2,4)) + 
               Power(s2,4)*(2149 + 3024*Power(t1,2) + 2225*t2 + 
                  2654*Power(t2,2) + t1*(-1679 + 4643*t2)) - 
               Power(s2,3)*(2369 + 3044*Power(t1,3) + 6135*t2 + 
                  2220*Power(t2,2) + 1799*Power(t2,3) + 
                  8*Power(t1,2)*(833 + 655*t2) + 
                  t1*(-9334 - 2876*t2 + 8343*Power(t2,2))) + 
               Power(s2,2)*(-4258 + 2069*Power(t1,4) + 1317*t2 + 
                  3784*Power(t2,2) + 716*Power(t2,3) - 
                  62*Power(t2,4) + Power(t1,3)*(3528 + 814*t2) + 
                  Power(t1,2)*(-6889 + 14696*t2 + 4990*Power(t2,2)) + 
                  t1*(6719 - 17657*t2 - 3367*Power(t2,2) + 
                     2255*Power(t2,3))) - 
               s2*(-3988 + 490*Power(t1,5) - 5118*t2 + 
                  3929*Power(t2,2) + 1333*Power(t2,3) - 
                  480*Power(t2,4) + 20*Power(t2,5) + 
                  Power(t1,4)*(-1293 + 460*t2) + 
                  Power(t1,3)*(8223 + 5024*t2 - 240*Power(t2,2)) - 
                  16*Power(t1,2)*
                   (1116 + 697*t2 - 538*Power(t2,2) + 20*Power(t2,3)) + 
                  t1*(14639 + 9514*t2 - 13553*Power(t2,2) + 
                     1014*Power(t2,3) + 10*Power(t2,4)))) + 
            Power(s1,2)*(-2777 - 120*Power(s2,7) + 168*Power(t1,6) + 
               2525*t2 + 2874*Power(t2,2) + 3040*Power(t2,3) - 
               2043*Power(t2,4) + 198*Power(t2,5) + 
               Power(t1,5)*(791 + 260*t2) + 
               Power(s2,6)*(932 - 727*t1 + 968*t2) - 
               2*Power(t1,4)*(2843 - 362*t2 + 545*Power(t2,2)) + 
               2*Power(t1,3)*
                (5452 - 3063*t2 - 986*Power(t2,2) + 162*Power(t2,3)) + 
               Power(t1,2)*(-11138 + 9446*t2 + 19689*Power(t2,2) - 
                  5527*Power(t2,3) + 322*Power(t2,4)) + 
               t1*(7734 - 5736*t2 - 22265*Power(t2,2) + 
                  5184*Power(t2,3) + 606*Power(t2,4) - 68*Power(t2,5)) \
+ Power(s2,5)*(-121 + 1951*Power(t1,2) - 525*t2 + 461*Power(t2,2) + 
                  t1*(-866 + 2278*t2)) - 
               Power(s2,4)*(3788 + 2996*Power(t1,3) + 5743*t2 + 
                  2082*Power(t2,2) + 3140*Power(t2,3) + 
                  2*Power(t1,2)*(3039 + 938*t2) + 
                  t1*(-10447 + 1854*t2 + 10008*Power(t2,2))) + 
               Power(s2,3)*(-1581 + 3217*Power(t1,4) + 4157*t2 + 
                  7545*Power(t2,2) + 1043*Power(t2,3) + 
                  1189*Power(t2,4) - 3*Power(t1,3)*(-587 + 739*t2) + 
                  Power(t1,2)*(2418 + 32644*t2 + 8699*Power(t2,2)) + 
                  t1*(-2312 - 28777*t2 - 3066*Power(t2,2) + 
                     11106*Power(t2,3))) + 
               Power(s2,2)*(7123 - 1478*Power(t1,5) + 9339*t2 - 
                  634*Power(t2,2) - 2995*Power(t2,3) - 
                  204*Power(t2,4) + 53*Power(t2,5) + 
                  Power(t1,4)*(3930 + 598*t2) + 
                  2*Power(t1,3)*(-12274 - 9050*t2 + 245*Power(t2,2)) + 
                  Power(t1,2)*
                   (35993 + 17916*t2 - 25437*Power(t2,2) - 
                     6441*Power(t2,3)) + 
                  t1*(-22935 - 12502*t2 + 28405*Power(t2,2) + 
                     3306*Power(t2,3) - 1958*Power(t2,4))) + 
               s2*(817 + 126*Power(t1,6) - 9463*t2 - 5249*Power(t2,2) + 
                  5647*Power(t2,3) + 1132*Power(t2,4) - 
                  490*Power(t2,5) + 20*Power(t2,6) + 
                  3*Power(t1,5)*(-101 + 96*t2) - 
                  Power(t1,4)*(403 + 2499*t2 + 80*Power(t2,2)) + 
                  Power(t1,3)*
                   (14040 + 26949*t2 + 8294*Power(t2,2) - 
                     80*Power(t2,3)) - 
                  Power(t1,2)*
                   (20423 + 59258*t2 + 11451*Power(t2,2) - 
                     8620*Power(t2,3) + 270*Power(t2,4)) + 
                  t1*(6090 + 45363*t2 + 5906*Power(t2,2) - 
                     14792*Power(t2,3) + 798*Power(t2,4) + 
                     80*Power(t2,5)))) - 
            s1*(-2111 + 32*Power(t1,7) + 
               50*Power(s2,7)*(-5 + 4*t1 - 7*t2) - 3864*t2 + 
               5342*Power(t2,2) + 571*Power(t2,3) + 3931*Power(t2,4) - 
               1630*Power(t2,5) + 124*Power(t2,6) + 
               Power(t1,6)*(406 + 291*t2) - 
               3*Power(t1,5)*(-762 + 13*t2 + 86*Power(t2,2)) + 
               Power(s2,6)*(644 - 697*Power(t1,2) + t1*(162 - 532*t2) + 
                  1496*t2 + 894*Power(t2,2)) - 
               Power(t1,4)*(17325 + 7553*t2 - 2670*Power(t2,2) + 
                  600*Power(t2,3)) + 
               Power(t1,3)*(29344 + 23514*t2 - 11291*Power(t2,2) - 
                  3693*Power(t2,3) + 588*Power(t2,4)) + 
               Power(t1,2)*(-21014 - 29329*t2 + 17912*Power(t2,2) + 
                  18031*Power(t2,3) - 3354*Power(t2,4) + 9*Power(t2,5)) \
+ t1*(8283 + 16988*t2 - 13198*Power(t2,2) - 16412*Power(t2,3) + 
                  633*Power(t2,4) + 1030*Power(t2,5) - 62*Power(t2,6)) + 
               Power(s2,5)*(1091 + 1650*Power(t1,3) + 
                  Power(t1,2)*(966 - 416*t2) + 190*t2 - 
                  488*Power(t2,2) + 833*Power(t2,3) + 
                  t1*(-3825 + 1254*t2 + 2651*Power(t2,2))) - 
               Power(s2,4)*(2381 + 2376*Power(t1,4) + 6950*t2 + 
                  5488*Power(t2,2) + 250*Power(t2,3) + 
                  1642*Power(t2,4) - 2*Power(t1,3)*(1723 + 1393*t2) + 
                  Power(t1,2)*(8039 + 19955*t2 + 5*Power(t2,2)) + 
                  t1*(-8810 - 20664*t2 + 4591*Power(t2,2) + 
                     9613*Power(t2,3))) + 
               Power(s2,3)*(-3386 + 1566*Power(t1,5) - 5053*t2 + 
                  437*Power(t2,2) + 4331*Power(t2,3) + 
                  437*Power(t2,4) + 333*Power(t2,5) - 
                  3*Power(t1,4)*(1951 + 452*t2) + 
                  Power(t1,3)*(17985 + 9380*t2 - 6397*Power(t2,2)) + 
                  Power(t1,2)*
                   (-18992 + 5530*t2 + 37729*Power(t2,2) + 
                     8747*Power(t2,3)) + 
                  t1*(10087 - 2954*t2 - 27217*Power(t2,2) - 
                     3046*Power(t2,3) + 6459*Power(t2,4))) + 
               Power(s2,2)*(2705 - 304*Power(t1,6) + 
                  Power(t1,5)*(460 - 294*t2) + 12469*t2 + 
                  6280*Power(t2,2) + 689*Power(t2,3) - 
                  752*Power(t2,4) - 21*Power(t2,5) + 17*Power(t2,6) + 
                  Power(t1,4)*(8139 + 9175*t2 + 3190*Power(t2,2)) - 
                  Power(t1,3)*
                   (38784 + 56776*t2 + 20311*Power(t2,2) + 
                     891*Power(t2,3)) + 
                  Power(t1,2)*
                   (44707 + 79608*t2 + 12005*Power(t2,2) - 
                     16190*Power(t2,3) - 3647*Power(t2,4)) - 
                  t1*(18111 + 47491*t2 + 3317*Power(t2,2) - 
                     19079*Power(t2,3) - 791*Power(t2,4) + 
                     759*Power(t2,5))) + 
               s2*(3911 + 7*Power(t1,7) + 2646*t2 - 4013*Power(t2,2) - 
                  2868*Power(t2,3) + 4700*Power(t2,4) + 
                  416*Power(t2,5) - 256*Power(t2,6) + 10*Power(t2,7) + 
                  Power(t1,6)*(314 + 32*t2) - 
                  Power(t1,5)*(7450 + 2412*t2 + 129*Power(t2,2)) + 
                  Power(t1,4)*
                   (25387 + 8677*t2 - 1479*Power(t2,2) + 
                     150*Power(t2,3)) + 
                  Power(t1,3)*
                   (-24222 + 5930*t2 + 26804*Power(t2,2) + 
                     4202*Power(t2,3) + 15*Power(t2,4)) - 
                  Power(t1,2)*
                   (-10410 + 18840*t2 + 58187*Power(t2,2) + 
                     2369*Power(t2,3) - 4751*Power(t2,4) + 
                     156*Power(t2,5)) + 
                  t1*(-7878 + 3858*t2 + 39024*Power(t2,2) - 
                     1490*Power(t2,3) - 9306*Power(t2,4) + 
                     424*Power(t2,5) + 71*Power(t2,6))))) - 
         Power(s,5)*(-1388 + 6447*t1 - 19134*Power(t1,2) + 
            37380*Power(t1,3) - 40280*Power(t1,4) + 20205*Power(t1,5) - 
            2388*Power(t1,6) - 802*Power(t1,7) - 40*Power(t1,8) - 
            5379*t2 + 22880*t1*t2 - 45375*Power(t1,2)*t2 + 
            55010*Power(t1,3)*t2 - 41119*Power(t1,4)*t2 + 
            15556*Power(t1,5)*t2 - 1264*Power(t1,6)*t2 - 
            264*Power(t1,7)*t2 - 7*Power(t1,8)*t2 + 1612*Power(t2,2) - 
            3009*t1*Power(t2,2) + 1881*Power(t1,2)*Power(t2,2) + 
            379*Power(t1,3)*Power(t2,2) - 2305*Power(t1,4)*Power(t2,2) + 
            1442*Power(t1,5)*Power(t2,2) - 110*Power(t1,6)*Power(t2,2) - 
            62*Power(t1,7)*Power(t2,2) + 4576*Power(t2,3) - 
            20324*t1*Power(t2,3) + 37212*Power(t1,2)*Power(t2,3) - 
            30909*Power(t1,3)*Power(t2,3) + 
            10955*Power(t1,4)*Power(t2,3) - 
            1271*Power(t1,5)*Power(t2,3) + 50*Power(t1,6)*Power(t2,3) + 
            875*Power(t2,4) - 9424*t1*Power(t2,4) + 
            14332*Power(t1,2)*Power(t2,4) - 
            6849*Power(t1,3)*Power(t2,4) + 635*Power(t1,4)*Power(t2,4) + 
            164*Power(t1,5)*Power(t2,4) + 2071*Power(t2,5) - 
            879*t1*Power(t2,5) - 2423*Power(t1,2)*Power(t2,5) + 
            1569*Power(t1,3)*Power(t2,5) - 163*Power(t1,4)*Power(t2,5) - 
            1023*Power(t2,6) + 1365*t1*Power(t2,6) - 
            401*Power(t1,2)*Power(t2,6) - 14*Power(t1,3)*Power(t2,6) + 
            96*Power(t2,7) - 118*t1*Power(t2,7) + 
            32*Power(t1,2)*Power(t2,7) + 
            Power(s1,7)*(-4 + 51*Power(s2,3) + 6*t1 + 
               2*s2*(19 - 21*t1 + Power(t1,2) - 2*t2) + 
               Power(s2,2)*(-78 + 36*t1 + 5*t2)) + 
            2*Power(s2,8)*(-12 + 14*Power(t1,2) - 79*t2 - 
               59*Power(t2,2) + t1*(-1 + 66*t2)) - 
            Power(s2,7)*(4 + 133*Power(t1,3) - 403*t2 - 
               564*Power(t2,2) - 253*Power(t2,3) + 
               Power(t1,2)*(38 + 258*t2) + 
               2*t1*(-84 + 8*t2 + 81*Power(t2,2))) + 
            Power(s2,6)*(629 + 293*Power(t1,4) + 1096*t2 - 
               718*Power(t2,2) - 852*Power(t2,3) + 91*Power(t2,4) + 
               Power(t1,3)*(-654 + 125*t2) + 
               Power(t1,2)*(1827 + 2030*t2 - 1263*Power(t2,2)) + 
               t1*(-2076 - 3577*t2 + 1741*Power(t2,2) + 
                  1474*Power(t2,3))) - 
            Power(s2,5)*(588 + 390*Power(t1,5) + 1894*t2 + 
               2569*Power(t2,2) + 139*Power(t2,3) - 993*Power(t2,4) + 
               276*Power(t2,5) - 4*Power(t1,4)*(523 + 27*t2) + 
               Power(t1,3)*(4673 - 699*t2 - 4492*Power(t2,2)) + 
               Power(t1,2)*(-2816 + 5657*t2 + 13810*Power(t2,2) + 
                  653*Power(t2,3)) + 
               t1*(-697 - 6978*t2 - 11671*Power(t2,2) + 
                  1786*Power(t2,3) + 3995*Power(t2,4))) + 
            Power(s2,4)*(-1785 + 204*Power(t1,6) - 5616*t2 - 
               2218*Power(t2,2) + 1421*Power(t2,3) + 1304*Power(t2,4) - 
               137*Power(t2,5) + 40*Power(t2,6) + 
               Power(t1,5)*(-1033 + 254*t2) - 
               Power(t1,4)*(6434 + 10877*t2 + 5104*Power(t2,2)) + 
               Power(t1,3)*(26248 + 36835*t2 + 12433*Power(t2,2) - 
                  3533*Power(t2,3)) + 
               Power(t1,2)*(-30357 - 43451*t2 - 1066*Power(t2,2) + 
                  22288*Power(t2,3) + 5640*Power(t2,4)) + 
               t1*(13221 + 23165*t2 - 2618*Power(t2,2) - 
                  18833*Power(t2,3) - 4278*Power(t2,4) + 
                  2751*Power(t2,5))) - 
            Power(s2,3)*(-223 + 28*Power(t1,7) + 1896*t2 - 
               2967*Power(t2,2) - 3347*Power(t2,3) - 1635*Power(t2,4) + 
               567*Power(t2,5) + 324*Power(t2,6) - 34*Power(t2,7) + 
               Power(t1,6)*(511 + 204*t2) - 
               2*Power(t1,5)*(7751 + 4624*t2 + 939*Power(t2,2)) + 
               Power(t1,4)*(40300 + 18823*t2 - 8930*Power(t2,2) - 
                  4003*Power(t2,3)) + 
               Power(t1,3)*(-31574 + 4511*t2 + 61192*Power(t2,2) + 
                  25303*Power(t2,3) + 2133*Power(t2,4)) + 
               Power(t1,2)*(2562 - 20294*t2 - 79738*Power(t2,2) - 
                  30207*Power(t2,3) + 6696*Power(t2,4) + 
                  3099*Power(t2,5)) + 
               t1*(3941 + 4775*t2 + 33973*Power(t2,2) + 
                  14276*Power(t2,3) - 8924*Power(t2,4) - 
                  2924*Power(t2,5) + 451*Power(t2,6))) + 
            Power(s2,2)*(1699 + 11294*t2 + 6356*Power(t2,2) - 
               245*Power(t2,3) + 10*Power(t2,4) + 49*Power(t2,5) + 
               390*Power(t2,6) - Power(t2,7) - 5*Power(t2,8) + 
               3*Power(t1,7)*(79 + 6*t2) - 
               Power(t1,6)*(6172 + 1901*t2 + 200*Power(t2,2)) - 
               Power(t1,5)*(2974 + 13067*t2 + 8708*Power(t2,2) + 
                  1008*Power(t2,3)) + 
               Power(t1,4)*(56342 + 75073*t2 + 40321*Power(t2,2) + 
                  3396*Power(t2,3) + 95*Power(t2,4)) + 
               Power(t1,3)*(-92233 - 120163*t2 - 47992*Power(t2,2) + 
                  19452*Power(t2,3) + 8285*Power(t2,4) + 
                  862*Power(t2,5)) + 
               Power(t1,2)*(58904 + 94178*t2 + 25338*Power(t2,2) - 
                  45845*Power(t2,3) - 17374*Power(t2,4) + 
                  1470*Power(t2,5) + 222*Power(t2,6)) + 
               t1*(-15792 - 44888*t2 - 14456*Power(t2,2) + 
                  24454*Power(t2,3) + 9943*Power(t2,4) - 
                  3530*Power(t2,5) - 258*Power(t2,6) + 16*Power(t2,7))) \
+ s2*(1602 - 7*Power(t1,8) + 4461*t2 + 622*Power(t2,2) + 
               1136*Power(t2,3) + 1524*Power(t2,4) + 3648*Power(t2,5) - 
               222*Power(t2,6) - 191*Power(t2,7) + 16*Power(t2,8) + 
               2*Power(t1,7)*(221 + 72*t2 + 5*Power(t2,2)) + 
               Power(t1,6)*(11118 + 5049*t2 + 1194*Power(t2,2) + 
                  50*Power(t2,3)) + 
               Power(t1,5)*(-39217 - 19245*t2 + 1037*Power(t2,2) + 
                  694*Power(t2,3) - 80*Power(t2,4)) - 
               Power(t1,4)*(-42438 - 14602*t2 + 33853*Power(t2,2) + 
                  11070*Power(t2,3) + 290*Power(t2,4) + 56*Power(t2,5)) \
+ Power(t1,3)*(-12050 - 153*t2 + 78350*Power(t2,2) + 
                  28957*Power(t2,3) - 6078*Power(t2,4) - 
                  1247*Power(t2,5) + 94*Power(t2,6)) - 
               Power(t1,2)*(2883 - 9067*t2 + 65357*Power(t2,2) + 
                  30200*Power(t2,3) - 19257*Power(t2,4) - 
                  4842*Power(t2,5) + 637*Power(t2,6) + 10*Power(t2,7)) \
+ t1*(-1443 - 14161*t2 + 17999*Power(t2,2) + 11100*Power(t2,3) - 
                  14709*Power(t2,4) - 7188*Power(t2,5) + 
                  929*Power(t2,6) + 133*Power(t2,7) - 8*Power(t2,8))) + 
            Power(s1,6)*(-66 + 224*Power(s2,4) - 5*Power(t1,3) + 
               t1*(124 - 46*t2) + 36*t2 + Power(t1,2)*(-76 + 8*t2) - 
               Power(s2,3)*(339 + 163*t1 + 330*t2) + 
               Power(s2,2)*(198 + 276*Power(t1,2) + 452*t2 - 
                  43*Power(t2,2) - t1*(487 + 127*t2)) + 
               s2*(102 - 63*Power(t1,3) - Power(t1,2)*(-21 + t2) - 
                  276*t2 + 28*Power(t2,2) + 
                  t1*(49 + 224*t2 + 4*Power(t2,2)))) + 
            Power(s1,5)*(242 + 222*Power(s2,5) + 78*Power(t1,4) + 
               390*t2 - 132*Power(t2,2) - 8*Power(t1,3)*(11 + 18*t2) - 
               Power(s2,4)*(615 + 1080*t1 + 1372*t2) + 
               Power(t1,2)*(777 + 394*t2 + 12*Power(t2,2)) + 
               2*t1*(-549 - 226*t2 + 45*Power(t2,2)) + 
               Power(s2,3)*(979 + 1933*Power(t1,2) + 1422*t2 + 
                  830*Power(t2,2) + 2*t1*(-1291 + 924*t2)) + 
               Power(s2,2)*(1193 - 1227*Power(t1,3) - 1210*t2 - 
                  1175*Power(t2,2) + 145*Power(t2,3) - 
                  2*Power(t1,2)*(-698 + 905*t2) + 
                  3*t1*(-286 + 988*t2 + 47*Power(t2,2))) + 
               s2*(-1877 + 336*Power(t1,4) - 236*t2 + 986*Power(t2,2) - 
                  96*Power(t2,3) + Power(t1,3)*(569 + 96*t2) - 
                  2*Power(t1,2)*(2255 - 519*t2 + 30*Power(t2,2)) - 
                  t1*(-5596 + 1638*t2 + 563*Power(t2,2) + 
                     12*Power(t2,3)))) + 
            Power(s1,4)*(-109*Power(s2,6) - 270*Power(t1,5) + 
               Power(t1,4)*(348 + 335*t2) - 
               Power(s2,5)*(96 + 1883*t1 + 1258*t2) + 
               2*Power(t1,3)*(-842 + 7*t2 + 110*Power(t2,2)) + 
               t1*(-7269 + 8130*t2 + 295*Power(t2,2) - 
                  138*Power(t2,3)) + 
               Power(t1,2)*(6654 - 6482*t2 + 190*Power(t2,2) - 
                  96*Power(t2,3)) + 
               2*(1016 - 733*t2 - 650*Power(t2,2) + 172*Power(t2,3)) + 
               Power(s2,4)*(1976 + 3823*Power(t1,2) + 1926*t2 + 
                  3357*Power(t2,2) + t1*(-3607 + 7068*t2)) - 
               Power(s2,3)*(-1582 + 3258*Power(t1,3) + 3955*t2 + 
                  2356*Power(t2,2) + 1006*Power(t2,3) + 
                  Power(t1,2)*(2012 + 9366*t2) + 
                  t1*(-3588 - 12154*t2 + 6615*Power(t2,2))) + 
               Power(s2,2)*(-6270 + 1599*Power(t1,4) - 3216*t2 + 
                  3652*Power(t2,2) + 1739*Power(t2,3) - 
                  255*Power(t2,4) + Power(t1,3)*(5806 + 3445*t2) + 
                  Power(t1,2)*(-19851 + 66*t2 + 5345*Power(t2,2)) + 
                  t1*(19397 - 3390*t2 - 8248*Power(t2,2) + 
                     2*Power(t2,3))) + 
               s2*(-31 - 490*Power(t1,5) + 8974*t2 + 458*Power(t2,2) - 
                  2135*Power(t2,3) + 200*Power(t2,4) - 
                  50*Power(t1,4)*(-22 + 9*t2) + 
                  Power(t1,3)*(-2951 - 6761*t2 + 40*Power(t2,2)) + 
                  Power(t1,2)*
                   (6066 + 25713*t2 - 3925*Power(t2,2) + 
                     200*Power(t2,3)) + 
                  t1*(-3783 - 27986*t2 + 5461*Power(t2,2) + 
                     985*Power(t2,3)))) + 
            Power(s1,3)*(-2801 - 231*Power(s2,7) + 280*Power(t1,6) - 
               7112*t2 + 1449*Power(t2,2) + 3243*Power(t2,3) - 
               636*Power(t2,4) + 10*Power(t1,5)*(71 + 28*t2) + 
               Power(s2,6)*(995 - 1485*t1 + 401*t2) - 
               4*Power(t1,4)*(2213 + 163*t2 + 275*Power(t2,2)) + 
               2*Power(t1,3)*
                (11996 + 5238*t2 - 1175*Power(t2,2) + 134*Power(t2,3)) \
+ Power(t1,2)*(-27554 - 33124*t2 + 19685*Power(t2,2) - 
                  1773*Power(t2,3) + 104*Power(t2,4)) + 
               2*t1*(6987 + 15483*t2 - 9459*Power(t2,2) - 
                  389*Power(t2,3) + 181*Power(t2,4)) + 
               Power(s2,5)*(1078 + 3068*Power(t1,2) + 262*t2 + 
                  2872*Power(t2,2) + t1*(-1829 + 7986*t2)) - 
               Power(s2,4)*(2164 + 3312*Power(t1,3) + 5302*t2 + 
                  1067*Power(t2,2) + 4119*Power(t2,3) + 
                  Power(t1,2)*(10223 + 11397*t2) + 
                  3*t1*(-4431 - 2914*t2 + 6208*Power(t2,2))) + 
               Power(s2,3)*(-8545 + 2784*Power(t1,4) - 3416*t2 + 
                  7794*Power(t2,2) + 2250*Power(t2,3) + 
                  539*Power(t2,4) + 2*Power(t1,3)*(6903 + 1726*t2) + 
                  Power(t1,2)*
                   (-27184 + 26304*t2 + 22173*Power(t2,2)) + 
                  t1*(21487 - 28026*t2 - 27638*Power(t2,2) + 
                     10838*Power(t2,3))) + 
               Power(s2,2)*(3310 - 1707*Power(t1,5) + 19156*t2 + 
                  3406*Power(t2,2) - 6066*Power(t2,3) - 
                  1516*Power(t2,4) + 255*Power(t2,5) + 
                  Power(t1,4)*(2345 + 513*t2) - 
                  Power(t1,3)*(23315 + 37444*t2 + 6110*Power(t2,2)) + 
                  Power(t1,2)*
                   (44990 + 87481*t2 - 5085*Power(t2,2) - 
                     8157*Power(t2,3)) - 
                  2*t1*(12669 + 36553*t2 - 7624*Power(t2,2) - 
                     6044*Power(t2,3) + 69*Power(t2,4))) + 
               s2*(8583 + 210*Power(t1,6) - 4556*t2 - 
                  18062*Power(t2,2) - 756*Power(t2,3) + 
                  2870*Power(t2,4) - 260*Power(t2,5) + 
                  Power(t1,5)*(-1857 + 440*t2) + 
                  4*Power(t1,4)*(3258 + 180*t2 + 35*Power(t2,2)) + 
                  Power(t1,3)*
                   (-32601 + 8920*t2 + 15538*Power(t2,2) - 
                     160*Power(t2,3)) + 
                  Power(t1,2)*
                   (41469 - 33582*t2 - 50895*Power(t2,2) + 
                     5974*Power(t2,3) - 250*Power(t2,4)) + 
                  2*t1*(-14683 + 14340*t2 + 27063*Power(t2,2) - 
                     3987*Power(t2,3) - 660*Power(t2,4) + 20*Power(t2,5)\
))) - Power(s1,2)*(3178 + 74*Power(s2,8) + 87*Power(t1,7) + 
               Power(s2,7)*(-810 + 607*t1 - 802*t2) - 11942*t2 - 
               7897*Power(t2,2) - 2603*Power(t2,3) + 4839*Power(t2,4) - 
               708*Power(t2,5) + 2*Power(t1,6)*(556 + 315*t2) - 
               2*Power(t1,5)*(2163 + 49*t2 + 291*Power(t2,2)) + 
               Power(s2,6)*(709 - 1206*Power(t1,2) + 
                  t1*(445 - 3476*t2) + 2240*t2 + 319*Power(t2,2)) - 
               2*Power(t1,4)*
                (125 + 8647*t2 - 305*Power(t2,2) + 360*Power(t2,3)) + 
               Power(t1,3)*(11944 + 60488*t2 + 18367*Power(t2,2) - 
                  6579*Power(t2,3) + 609*Power(t2,4)) - 
               Power(t1,2)*(7872 + 80807*t2 + 54591*Power(t2,2) - 
                  25455*Power(t2,3) + 1379*Power(t2,4) + 24*Power(t2,5)\
) + t1*(-3709 + 48232*t2 + 45461*Power(t2,2) - 16959*Power(t2,3) - 
                  3224*Power(t2,4) + 594*Power(t2,5)) + 
               Power(s2,5)*(3249 + 1955*Power(t1,3) + 1811*t2 - 
                  1233*Power(t2,2) + 3134*Power(t2,3) + 
                  4*Power(t1,2)*(1645 + 772*t2) + 
                  t1*(-11086 + 1605*t2 + 14970*Power(t2,2))) + 
               Power(s2,4)*(1475 - 2879*Power(t1,4) - 5702*t2 - 
                  6421*Power(t2,2) + 1321*Power(t2,3) - 
                  2575*Power(t2,4) + Power(t1,3)*(-4097 + 3789*t2) + 
                  Power(t1,2)*(968 - 50417*t2 - 18870*Power(t2,2)) + 
                  t1*(2384 + 46829*t2 + 13139*Power(t2,2) - 
                     23115*Power(t2,3))) + 
               Power(s2,3)*(-7028 + 2657*Power(t1,5) - 19925*t2 - 
                  3289*Power(t2,2) + 8206*Power(t2,3) + 
                  1749*Power(t2,4) + 2*Power(t2,5) - 
                  2*Power(t1,4)*(5371 + 2425*t2) + 
                  Power(t1,3)*(57525 + 60488*t2 + 1673*Power(t2,2)) + 
                  Power(t1,2)*
                   (-80385 - 85917*t2 + 50130*Power(t2,2) + 
                     27079*Power(t2,3)) + 
                  t1*(40088 + 56707*t2 - 53888*Power(t2,2) - 
                     32066*Power(t2,3) + 8859*Power(t2,4))) - 
               Power(s2,2)*(8460 + 711*Power(t1,6) - 11315*t2 - 
                  19464*Power(t2,2) - 1887*Power(t2,3) + 
                  5416*Power(t2,4) + 722*Power(t2,5) - 
                  145*Power(t2,6) - 2*Power(t1,5)*(2729 + 79*t2) + 
                  Power(t1,4)*(27949 + 159*t2 - 4233*Power(t2,2)) + 
                  Power(t1,3)*
                   (-24142 + 70096*t2 + 63613*Power(t2,2) + 
                     7655*Power(t2,3)) + 
                  Power(t1,2)*
                   (3227 - 144020*t2 - 132747*Power(t2,2) + 
                     5858*Power(t2,3) + 6361*Power(t2,4)) + 
                  t1*(-9966 + 84880*t2 + 98538*Power(t2,2) - 
                     20424*Power(t2,3) - 9111*Power(t2,4) + 
                     141*Power(t2,5))) + 
               s2*(6164 + 21*Power(t1,7) + 17967*t2 - 
                  10261*Power(t2,2) - 20358*Power(t2,3) - 
                  350*Power(t2,4) + 2318*Power(t2,5) - 
                  204*Power(t2,6) + Power(t1,6)*(20 + 87*t2) - 
                  Power(t1,5)*(9448 + 5709*t2 + 150*Power(t2,2)) + 
                  Power(t1,4)*
                   (62238 + 45324*t2 + 4540*Power(t2,2) + 
                     330*Power(t2,3)) + 
                  Power(t1,3)*
                   (-121523 - 114337*t2 + 14411*Power(t2,2) + 
                     14316*Power(t2,3) - 225*Power(t2,4)) + 
                  Power(t1,2)*
                   (100216 + 138143*t2 - 66543*Power(t2,2) - 
                     47533*Power(t2,3) + 5067*Power(t2,4) - 
                     123*Power(t2,5)) + 
                  t1*(-37646 - 83204*t2 + 59356*Power(t2,2) + 
                     53866*Power(t2,3) - 6801*Power(t2,4) - 
                     1202*Power(t2,5) + 60*Power(t2,6)))) + 
            s1*(4973 + 6*Power(t1,8) - 
               2*Power(s2,8)*(-70 + 59*t1 - 97*t2) + 3048*t2 - 
               13843*Power(t2,2) - 3692*Power(t2,3) - 4899*Power(t2,4) + 
               3595*Power(t2,5) - 412*Power(t2,6) + 
               8*Power(t1,7)*(53 + 22*t2) + 
               Power(t1,6)*(3365 + 480*t2 + 244*Power(t2,2)) - 
               2*Power(t1,5)*
                (13079 + 2712*t2 - 648*Power(t2,2) + 378*Power(t2,3)) + 
               Power(t1,4)*(52244 + 12312*t2 - 22617*Power(t2,2) + 
                  279*Power(t2,3) + 130*Power(t2,4)) + 
               4*Power(t1,3)*
                (-12072 - 3860*t2 + 18183*Power(t2,2) + 
                  4106*Power(t2,3) - 1431*Power(t2,4) + 71*Power(t2,5)) \
+ Power(t1,2)*(29108 + 17140*t2 - 94637*Power(t2,2) - 
                  42453*Power(t2,3) + 13898*Power(t2,4) + 
                  287*Power(t2,5) - 84*Power(t2,6)) + 
               2*t1*(-7856 - 5978*t2 + 27998*Power(t2,2) + 
                  15594*Power(t2,3) - 2097*Power(t2,4) - 
                  1889*Power(t2,5) + 219*Power(t2,6)) + 
               Power(s2,7)*(-431 + 303*Power(t1,2) - 1370*t2 - 
                  821*Power(t2,2) + 16*t1*(1 + 47*t2)) - 
               Power(s2,6)*(730 + 821*Power(t1,3) + 
                  Power(t1,2)*(890 - 148*t2) - 1341*t2 - 
                  2127*Power(t2,2) + 64*Power(t2,3) + 
                  4*t1*(-679 + 314*t2 + 877*Power(t2,2))) + 
               Power(s2,5)*(2363 + 1596*Power(t1,4) + 6016*t2 + 
                  825*Power(t2,2) - 2392*Power(t2,3) + 
                  1574*Power(t2,4) - Power(t1,3)*(3371 + 3082*t2) + 
                  Power(t1,2)*(8048 + 20890*t2 + 812*Power(t2,2)) + 
                  2*t1*(-4378 - 11500*t2 + 2575*Power(t2,2) + 
                     6431*Power(t2,3))) + 
               Power(s2,4)*(3023 - 1806*Power(t1,5) + 3900*t2 - 
                  5120*Power(t2,2) - 4399*Power(t2,3) + 
                  1214*Power(t2,4) - 705*Power(t2,5) + 
                  Power(t1,4)*(10841 + 3422*t2) + 
                  Power(t1,3)*(-31775 - 18252*t2 + 10397*Power(t2,2)) + 
                  Power(t1,2)*
                   (34054 + 3776*t2 - 62519*Power(t2,2) - 
                     16936*Power(t2,3)) + 
                  t1*(-14793 + 3678*t2 + 52786*Power(t2,2) + 
                     12282*Power(t2,3) - 13230*Power(t2,4))) + 
               Power(s2,3)*(-523 + 718*Power(t1,6) - 13040*t2 - 
                  14539*Power(t2,2) - 3090*Power(t2,3) + 
                  3955*Power(t2,4) + 1096*Power(t2,5) - 
                  116*Power(t2,6) - 2*Power(t1,5)*(2652 + 79*t2) - 
                  2*Power(t1,4)*(-325 + 9795*t2 + 5666*Power(t2,2)) + 
                  Power(t1,3)*
                   (43909 + 122058*t2 + 71853*Power(t2,2) + 
                     3612*Power(t2,3)) + 
                  Power(t1,2)*
                   (-62099 - 168188*t2 - 89114*Power(t2,2) + 
                     32534*Power(t2,3) + 15438*Power(t2,4)) + 
                  t1*(23409 + 82628*t2 + 49316*Power(t2,2) - 
                     38374*Power(t2,3) - 16924*Power(t2,4) + 
                     3402*Power(t2,5))) - 
               Power(s2,2)*(7624 + 60*Power(t1,7) + 16353*t2 - 
                  7772*Power(t2,2) - 6568*Power(t2,3) - 
                  455*Power(t2,4) + 2380*Power(t2,5) + 
                  143*Power(t2,6) - 43*Power(t2,7) + 
                  Power(t1,6)*(583 + 276*t2) - 
                  Power(t1,5)*(26185 + 16134*t2 + 2680*Power(t2,2)) + 
                  Power(t1,4)*
                   (104834 + 79308*t2 + 5855*Power(t2,2) - 
                     2026*Power(t2,3)) + 
                  Power(t1,3)*
                   (-139639 - 99174*t2 + 65611*Power(t2,2) + 
                     40260*Power(t2,3) + 4625*Power(t2,4)) + 
                  Power(t1,2)*
                   (81677 + 59176*t2 - 143686*Power(t2,2) - 
                     82491*Power(t2,3) + 3705*Power(t2,4) + 
                     2237*Power(t2,5)) + 
                  t1*(-28387 - 38378*t2 + 82804*Power(t2,2) + 
                     54772*Power(t2,3) - 12954*Power(t2,4) - 
                     3052*Power(t2,5) + 71*Power(t2,6))) + 
               s2*(-2583 + 156*Power(t1,7) + 9600*t2 + 
                  7086*Power(t2,2) - 7198*Power(t2,3) - 
                  13041*Power(t2,4) + 304*Power(t2,5) + 
                  1026*Power(t2,6) - 88*Power(t2,7) - 
                  4*Power(t1,6)*(1737 + 494*t2 + 35*Power(t2,2)) - 
                  Power(t1,5)*
                   (-14729 + 4312*t2 + 4256*Power(t2,2) + 
                     20*Power(t2,3)) + 
                  Power(t1,4)*
                   (30041 + 78650*t2 + 41121*Power(t2,2) + 
                     3010*Power(t2,3) + 360*Power(t2,4)) - 
                  Power(t1,3)*
                   (87661 + 183790*t2 + 104533*Power(t2,2) - 
                     14520*Power(t2,3) - 6217*Power(t2,4) + 
                     232*Power(t2,5)) + 
                  Power(t1,2)*
                   (60561 + 169008*t2 + 118802*Power(t2,2) - 
                     58284*Power(t2,3) - 22683*Power(t2,4) + 
                     2596*Power(t2,5) - 4*Power(t2,6)) + 
                  t1*(-8059 - 67140*t2 - 59946*Power(t2,2) + 
                     49168*Power(t2,3) + 29318*Power(t2,4) - 
                     3628*Power(t2,5) - 619*Power(t2,6) + 36*Power(t2,7))\
))) + Power(s,4)*(-2406 + 9413*t1 - 19656*Power(t1,2) + 
            37860*Power(t1,3) - 59165*Power(t1,4) + 55922*Power(t1,5) - 
            26660*Power(t1,6) + 4212*Power(t1,7) + 473*Power(t1,8) + 
            7*Power(t1,9) - 6717*t2 + 31549*t1*t2 - 
            65027*Power(t1,2)*t2 + 80206*Power(t1,3)*t2 - 
            69288*Power(t1,4)*t2 + 42314*Power(t1,5)*t2 - 
            14356*Power(t1,6)*t2 + 1147*Power(t1,7)*t2 + 
            170*Power(t1,8)*t2 + 2661*Power(t2,2) - 
            12091*t1*Power(t2,2) + 31200*Power(t1,2)*Power(t2,2) - 
            46229*Power(t1,3)*Power(t2,2) + 
            33701*Power(t1,4)*Power(t2,2) - 
            9623*Power(t1,5)*Power(t2,2) + 131*Power(t1,6)*Power(t2,2) + 
            249*Power(t1,7)*Power(t2,2) + 15*Power(t1,8)*Power(t2,2) + 
            5852*Power(t2,3) - 32597*t1*Power(t2,3) + 
            72241*Power(t1,2)*Power(t2,3) - 
            76084*Power(t1,3)*Power(t2,3) + 
            38343*Power(t1,4)*Power(t2,3) - 
            8102*Power(t1,5)*Power(t2,3) + 281*Power(t1,6)*Power(t2,3) + 
            32*Power(t1,7)*Power(t2,3) + 1476*Power(t2,4) - 
            10553*t1*Power(t2,4) + 17146*Power(t1,2)*Power(t2,4) - 
            9612*Power(t1,3)*Power(t2,4) + 
            1285*Power(t1,4)*Power(t2,4) + 398*Power(t1,5)*Power(t2,4) - 
            104*Power(t1,6)*Power(t2,4) + 1593*Power(t2,5) + 
            348*t1*Power(t2,5) - 5903*Power(t1,2)*Power(t2,5) + 
            5207*Power(t1,3)*Power(t2,5) - 
            1283*Power(t1,4)*Power(t2,5) + 24*Power(t1,5)*Power(t2,5) - 
            1321*Power(t2,6) + 2425*t1*Power(t2,6) - 
            1165*Power(t1,2)*Power(t2,6) - 2*Power(t1,3)*Power(t2,6) + 
            61*Power(t1,4)*Power(t2,6) + 170*Power(t2,7) - 
            320*t1*Power(t2,7) + 180*Power(t1,2)*Power(t2,7) - 
            28*Power(t1,3)*Power(t2,7) + 
            Power(s2,9)*(-4 + 4*Power(t1,2) - 48*t2 + 44*t1*t2 - 
               34*Power(t2,2)) + 
            Power(s1,8)*s2*(8 + 17*Power(s2,2) - 12*t1 + 
               s2*(-26 + 27*t1 + 2*t2)) - 
            Power(s2,8)*(22 + 27*Power(t1,3) - 165*t2 - 
               320*Power(t2,2) - 135*Power(t2,3) + 
               Power(t1,2)*(22 + 68*t2) + 
               t1*(-71 + 68*t2 + 181*Power(t2,2))) + 
            Power(s2,7)*(318 + 64*Power(t1,4) + 293*t2 - 
               1114*Power(t2,2) - 993*Power(t2,3) - 62*Power(t2,4) + 
               Power(t1,3)*(-200 + 87*t2) + 
               Power(t1,2)*(690 + 661*t2 - 740*Power(t2,2)) + 
               t1*(-872 - 1130*t2 + 1597*Power(t2,2) + 1101*Power(t2,3))\
) - Power(s2,6)*(313 + 155*Power(t1,5) + 1756*t2 + 1851*Power(t2,2) - 
               1608*Power(t2,3) - 1764*Power(t2,4) + 103*Power(t2,5) - 
               Power(t1,4)*(1229 + 101*t2) + 
               Power(t1,3)*(3156 - 1001*t2 - 3040*Power(t2,2)) + 
               Power(t1,2)*(-2609 + 5168*t2 + 9902*Power(t2,2) + 
                  116*Power(t2,3)) + 
               t1*(214 - 5935*t2 - 9038*Power(t2,2) + 
                  2217*Power(t2,3) + 3342*Power(t2,4))) + 
            Power(s2,5)*(-1567 + 140*Power(t1,6) + 
               132*Power(t1,5)*(-10 + t2) - 1197*t2 + 
               3591*Power(t2,2) + 5515*Power(t2,3) + 819*Power(t2,4) - 
               1130*Power(t2,5) + 42*Power(t2,6) - 
               3*Power(t1,4)*(580 + 3093*t2 + 1621*Power(t2,2)) + 
               Power(t1,3)*(14519 + 30061*t2 + 11730*Power(t2,2) - 
                  4070*Power(t2,3)) + 
               Power(t1,2)*(-20347 - 31568*t2 + 445*Power(t2,2) + 
                  23432*Power(t2,3) + 5548*Power(t2,4)) + 
               t1*(10315 + 11843*t2 - 10837*Power(t2,2) - 
                  24140*Power(t2,3) - 5173*Power(t2,4) + 
                  3281*Power(t2,5))) - 
            Power(s2,4)*(-808 + 34*Power(t1,7) + 285*t2 - 
               5844*Power(t2,2) - 3414*Power(t2,3) + 2030*Power(t2,4) + 
               1539*Power(t2,5) + 217*Power(t2,6) - 40*Power(t2,7) + 
               Power(t1,6)*(83 + 254*t2) - 
               Power(t1,5)*(13810 + 12089*t2 + 2859*Power(t2,2)) + 
               Power(t1,4)*(43936 + 26154*t2 - 9631*Power(t2,2) - 
                  6655*Power(t2,3)) + 
               Power(t1,3)*(-47300 + 2095*t2 + 74911*Power(t2,2) + 
                  39052*Power(t2,3) + 2730*Power(t2,4)) + 
               Power(t1,2)*(17005 - 31336*t2 - 107624*Power(t2,2) - 
                  52981*Power(t2,3) + 8215*Power(t2,4) + 
                  5415*Power(t2,5)) + 
               t1*(860 + 14786*t2 + 51747*Power(t2,2) + 
                  25573*Power(t2,3) - 13557*Power(t2,4) - 
                  7255*Power(t2,5) + 995*Power(t2,6))) + 
            Power(s2,3)*(1262 + 5157*t2 + 6077*Power(t2,2) - 
               1829*Power(t2,3) - 2989*Power(t2,4) - 2007*Power(t2,5) + 
               248*Power(t2,6) + 252*Power(t2,7) - 20*Power(t2,8) + 
               Power(t1,7)*(361 + 56*t2) - 
               Power(t1,6)*(11150 + 4520*t2 + 517*Power(t2,2)) - 
               2*Power(t1,5)*
                (-7558 + 9191*t2 + 9552*Power(t2,2) + 1675*Power(t2,3)) \
+ Power(t1,4)*(37646 + 120923*t2 + 96879*Power(t2,2) + 
                  14653*Power(t2,3) - 695*Power(t2,4)) + 
               Power(t1,3)*(-96504 - 192770*t2 - 146774*Power(t2,2) + 
                  9178*Power(t2,3) + 22313*Power(t2,4) + 
                  3183*Power(t2,5)) + 
               Power(t1,2)*(74582 + 127520*t2 + 95888*Power(t2,2) - 
                  46720*Power(t2,3) - 49973*Power(t2,4) - 
                  3754*Power(t2,5) + 1240*Power(t2,6)) + 
               t1*(-21313 - 37779*t2 - 31663*Power(t2,2) + 
                  28355*Power(t2,3) + 32615*Power(t2,4) + 
                  1641*Power(t2,5) - 1731*Power(t2,6) + 19*Power(t2,7))) \
+ Power(s2,2)*(1649 - 49*Power(t1,8) + 11535*t2 - 655*Power(t2,2) - 
               5190*Power(t2,3) + 1437*Power(t2,4) + 2007*Power(t2,5) + 
               1356*Power(t2,6) - 220*Power(t2,7) - 23*Power(t2,8) + 
               2*Power(t2,9) + 
               Power(t1,7)*(2202 + 433*t2 + 25*Power(t2,2)) + 
               Power(t1,6)*(20758 + 19922*t2 + 6574*Power(t2,2) + 
                  491*Power(t2,3)) + 
               Power(t1,5)*(-101598 - 86457*t2 - 19343*Power(t2,2) + 
                  3113*Power(t2,3) + 444*Power(t2,4)) - 
               Power(t1,4)*(-158820 - 119421*t2 + 31237*Power(t2,2) + 
                  47736*Power(t2,3) + 8445*Power(t2,4) + 
                  618*Power(t2,5)) + 
               Power(t1,3)*(-108469 - 74262*t2 + 127336*Power(t2,2) + 
                  126396*Power(t2,3) + 16614*Power(t2,4) - 
                  3497*Power(t2,5) - 301*Power(t2,6)) + 
               Power(t1,2)*(31915 + 42639*t2 - 120066*Power(t2,2) - 
                  132939*Power(t2,3) - 8371*Power(t2,4) + 
                  14243*Power(t2,5) + 385*Power(t2,6) - 51*Power(t2,7)) \
+ t1*(-5228 - 33366*t2 + 37140*Power(t2,2) + 56196*Power(t2,3) - 
                  834*Power(t2,4) - 13121*Power(t2,5) - 
                  1026*Power(t2,6) + 249*Power(t2,7) + 8*Power(t2,8))) - 
            s2*(-924 + 2895*t2 + 3296*Power(t2,2) - 4434*Power(t2,3) - 
               5519*Power(t2,4) - 4875*Power(t2,5) + 955*Power(t2,6) + 
               348*Power(t2,7) - 50*Power(t2,8) + 
               4*Power(t1,8)*(19 + 6*t2) + 
               Power(t1,7)*(8882 + 3384*t2 + 582*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(t1,6)*(-21833 - 3601*t2 + 4435*Power(t2,2) + 
                  888*Power(t2,3) + 10*Power(t2,4)) - 
               Power(t1,5)*(12499 + 50312*t2 + 47919*Power(t2,2) + 
                  7516*Power(t2,3) + 249*Power(t2,4) + 84*Power(t2,5)) \
+ Power(t1,4)*(87217 + 143652*t2 + 132799*Power(t2,2) + 
                  11578*Power(t2,3) - 8240*Power(t2,4) - 
                  517*Power(t2,5) + 46*Power(t2,6)) + 
               Power(t1,3)*(-101512 - 155180*t2 - 170712*Power(t2,2) + 
                  1510*Power(t2,3) + 40253*Power(t2,4) + 
                  2633*Power(t2,5) - 833*Power(t2,6) + 20*Power(t2,7)) \
+ Power(t1,2)*(45826 + 79636*t2 + 112744*Power(t2,2) - 
                  13170*Power(t2,3) - 62949*Power(t2,4) - 
                  7643*Power(t2,5) + 2961*Power(t2,6) + 
                  49*Power(t2,7) - 12*Power(t2,8)) + 
               t1*(-5233 - 20538*t2 - 35112*Power(t2,2) + 
                  11128*Power(t2,3) + 36468*Power(t2,4) + 
                  10708*Power(t2,5) - 3242*Power(t2,6) - 
                  371*Power(t2,7) + 56*Power(t2,8))) + 
            Power(s1,7)*(125*Power(s2,4) - 
               2*(10 - 25*t1 + 15*Power(t1,2) + Power(t1,3)) + 
               Power(s2,3)*(18*t1 - 5*(25 + 21*t2)) + 
               s2*(134 - 9*Power(t1,3) - 2*Power(t1,2)*(-72 + t2) - 
                  76*t2 + 2*t1*(-133 + 50*t2)) - 
               Power(s2,2)*(47 + 13*Power(t1,2) - 205*t2 + 
                  16*Power(t2,2) + 3*t1*(-2 + 63*t2))) + 
            Power(s1,6)*(252*Power(s2,5) + 41*Power(t1,4) + 
               t1*(320 - 350*t2) + Power(t1,3)*(73 - 26*t2) + 
               10*Power(t1,2)*(-31 + 24*t2) + 6*(-21 + 25*t2) - 
               Power(s2,4)*(136 + 565*t1 + 780*t2) + 
               Power(s2,3)*(34 + 781*Power(t1,2) + 741*t2 + 
                  243*Power(t2,2) + t1*(-1237 + 200*t2)) + 
               s2*(-432 + 112*Power(t1,4) - 912*t2 + 308*Power(t2,2) + 
                  Power(t1,3)*(139 + 44*t2) + 
                  t1*(1713 + 1512*t2 - 344*Power(t2,2)) - 
                  Power(t1,2)*(1495 + 708*t2 + 6*Power(t2,2))) + 
               Power(s2,2)*(1184 - 677*Power(t1,3) + 
                  Power(t1,2)*(2309 - 200*t2) + 81*t2 - 
                  747*Power(t2,2) + 56*Power(t2,3) + 
                  t1*(-2744 + 457*t2 + 615*Power(t2,2)))) + 
            Power(s1,5)*(820 + 128*Power(s2,6) - 192*Power(t1,5) + 
               Power(s2,5)*(219 - 1766*t1 - 1438*t2) + 716*t2 - 
               480*Power(t2,2) + Power(t1,4)*(538 + 139*t2) + 
               Power(t1,2)*(4653 + 1275*t2 - 510*Power(t2,2)) + 
               Power(t1,3)*(-2137 - 868*t2 + 48*Power(t2,2)) + 
               2*t1*(-1834 - 625*t2 + 450*Power(t2,2)) + 
               Power(s2,4)*(581 + 2955*Power(t1,2) + 5*t2 + 
                  1974*Power(t2,2) + t1*(-3851 + 4604*t2)) - 
               Power(s2,3)*(-2894 + 2788*Power(t1,3) + 1258*t2 + 
                  2184*Power(t2,2) + 215*Power(t2,3) + 
                  Power(t1,2)*(-5371 + 6020*t2) + 
                  t1*(5431 - 9910*t2 + 1234*Power(t2,2))) + 
               Power(s2,2)*(-2604 + 963*Power(t1,4) - 6259*t2 + 
                  345*Power(t2,2) + 1593*Power(t2,3) - 
                  112*Power(t2,4) + Power(t1,3)*(1927 + 3636*t2) + 
                  Power(t1,2)*(-10893 - 11780*t2 + 1191*Power(t2,2)) + 
                  t1*(11161 + 13672*t2 - 2394*Power(t2,2) - 
                     1193*Power(t2,3))) + 
               s2*(-3389 - 294*Power(t1,5) + 3090*t2 + 
                  2948*Power(t2,2) - 730*Power(t2,3) - 
                  4*Power(t1,4)*(-7 + 66*t2) - 
                  8*Power(t1,3)*(-616 + 351*t2 + 5*Power(t2,2)) + 
                  Power(t1,2)*
                   (-12361 + 12376*t2 + 1429*Power(t2,2) + 
                     48*Power(t2,3)) + 
                  4*t1*(2812 - 3173*t2 - 1004*Power(t2,2) + 
                     174*Power(t2,3)))) + 
            Power(s1,4)*(2614 - 107*Power(s2,7) + 280*Power(t1,6) + 
               Power(s2,6)*(945 - 2114*t1 - 646*t2) - 4305*t2 - 
               2190*Power(t2,2) + 990*Power(t2,3) + 
               5*Power(t1,5)*(-65 + 32*t2) - 
               5*Power(t1,4)*(167 + 186*t2 + 111*Power(t2,2)) + 
               6*Power(t1,2)*
                (1741 - 4790*t2 + 15*Power(t2,2) + 70*Power(t2,3)) + 
               Power(t1,3)*(-2222 + 12065*t2 + 955*Power(t2,2) + 
                  80*Power(t2,3)) - 
               2*t1*(4961 - 10840*t2 - 835*Power(t2,2) + 
                  710*Power(t2,3)) + 
               Power(s2,5)*(398 + 3566*Power(t1,2) - 2264*t2 + 
                  3313*Power(t2,2) + 4*t1*(-729 + 2596*t2)) - 
               Power(s2,4)*(-385 + 3495*Power(t1,3) + 3494*t2 - 
                  1420*Power(t2,2) + 2556*Power(t2,3) + 
                  4*Power(t1,2)*(888 + 3971*t2) + 
                  5*t1*(-1269 - 4295*t2 + 2830*Power(t2,2))) + 
               Power(s2,3)*(-6958 + 1610*Power(t1,4) - 9523*t2 + 
                  5548*Power(t2,2) + 4078*Power(t2,3) - 
                  85*Power(t2,4) + Power(t1,3)*(15911 + 10575*t2) + 
                  Power(t1,2)*
                   (-41876 - 17337*t2 + 18625*Power(t2,2)) + 
                  t1*(32041 + 15487*t2 - 30739*Power(t2,2) + 
                     2595*Power(t2,3))) - 
               Power(s2,2)*(6071 + 1030*Power(t1,5) - 16010*t2 - 
                  15437*Power(t2,2) + 1470*Power(t2,3) + 
                  2115*Power(t2,4) - 140*Power(t2,5) + 
                  25*Power(t1,4)*(159 + 49*t2) + 
                  Power(t1,3)*(-14729 + 21285*t2 + 9220*Power(t2,2)) + 
                  Power(t1,2)*
                   (19856 - 71704*t2 - 27385*Power(t2,2) + 
                     2555*Power(t2,3)) + 
                  t1*(-16990 + 68395*t2 + 30984*Power(t2,2) - 
                     5135*Power(t2,3) - 1465*Power(t2,4))) + 
               s2*(7016 + 210*Power(t1,6) + 15191*t2 - 
                  7700*Power(t2,2) - 5700*Power(t2,3) + 
                  1130*Power(t2,4) + 5*Power(t1,5)*(-423 + 80*t2) + 
                  5*Power(t1,4)*(2722 + 979*t2 + 58*Power(t2,2)) - 
                  5*Power(t1,3)*
                   (8017 + 6111*t2 - 1777*Power(t2,2) + 24*Power(t2,3)) \
+ t1*(-33259 - 51295*t2 + 33300*Power(t2,2) + 6635*Power(t2,3) - 
                     980*Power(t2,4)) + 
                  Power(t1,2)*
                   (54839 + 60502*t2 - 33840*Power(t2,2) - 
                     1565*Power(t2,3) - 80*Power(t2,4)))) - 
            Power(s1,3)*(6491 + 117*Power(s2,8) + 130*Power(t1,7) + 
               Power(s2,7)*(-1082 + 1178*t1 - 439*t2) + 8827*t2 - 
               6542*Power(t2,2) - 4855*Power(t2,3) + 1460*Power(t2,4) + 
               2*Power(t1,6)*(573 + 355*t2) - 
               2*Power(t1,5)*(5563 + 838*t2 + 324*Power(t2,2)) + 
               Power(t1,4)*(40788 + 2590*t2 - 1090*Power(t2,2) - 
                  360*Power(t2,3)) + 
               t1*(-34167 - 39008*t2 + 44045*Power(t2,2) + 
                  3135*Power(t2,3) - 1930*Power(t2,4)) + 
               Power(t1,3)*(-75954 - 20168*t2 + 29455*Power(t2,2) - 
                  1615*Power(t2,3) + 210*Power(t2,4)) + 
               Power(t1,2)*(72658 + 48869*t2 - 65360*Power(t2,2) + 
                  3655*Power(t2,3) + 330*Power(t2,4)) - 
               Power(s2,6)*(-915 + 1579*Power(t1,2) - 3990*t2 + 
                  1324*Power(t2,2) + t1*(14 + 8847*t2)) + 
               Power(s2,5)*(3322 + 1556*Power(t1,3) + 212*t2 - 
                  7158*Power(t2,2) + 3873*Power(t2,3) + 
                  Power(t1,2)*(11555 + 12183*t2) + 
                  2*t1*(-7687 - 4263*t2 + 12245*Power(t2,2))) - 
               Power(s2,4)*(-4182 + 1260*Power(t1,4) + 4356*t2 + 
                  9804*Power(t2,2) - 2713*Power(t2,3) + 
                  1709*Power(t2,4) + Power(t1,3)*(20777 + 4679*t2) + 
                  Power(t1,2)*
                   (-35156 + 34771*t2 + 37626*Power(t2,2)) + 
                  t1*(19067 - 43590*t2 - 52103*Power(t2,2) + 
                     21471*Power(t2,3))) + 
               Power(s2,3)*(212 + 1940*Power(t1,5) + 
                  Power(t1,4)*(446 - 4365*t2) - 26564*t2 - 
                  14013*Power(t2,2) + 9684*Power(t2,3) + 
                  4917*Power(t2,4) - 353*Power(t2,5) + 
                  Power(t1,3)*(29137 + 89472*t2 + 21355*Power(t2,2)) + 
                  Power(t1,2)*
                   (-59109 - 193987*t2 - 30292*Power(t2,2) + 
                     28805*Power(t2,3)) + 
                  2*t1*(13812 + 69520*t2 + 9936*Power(t2,2) - 
                     23429*Power(t2,3) + 1335*Power(t2,4))) + 
               s2*(-3942 + 35*Power(t1,7) + 32454*t2 + 
                  29344*Power(t2,2) - 9835*Power(t2,3) - 
                  6870*Power(t2,4) + 1168*Power(t2,5) + 
                  5*Power(t1,6)*(-187 + 26*t2) + 
                  Power(t1,5)*(4019 - 4948*t2 + 30*Power(t2,2)) + 
                  5*Power(t1,4)*
                   (611 + 9808*t2 + 2698*Power(t2,2) + 72*Power(t2,3)) \
- 3*Power(t1,3)*(7620 + 54826*t2 + 20565*Power(t2,2) - 
                     4010*Power(t2,3) + 105*Power(t2,4)) + 
                  t1*(2491 - 150460*t2 - 94760*Power(t2,2) + 
                     44120*Power(t2,3) + 7310*Power(t2,4) - 
                     1012*Power(t2,5)) + 
                  Power(t1,2)*
                   (18116 + 239136*t2 + 111743*Power(t2,2) - 
                     45175*Power(t2,3) - 1090*Power(t2,4) - 
                     30*Power(t2,5))) - 
               Power(s2,2)*(14262 + 850*Power(t1,6) + 11589*t2 - 
                  33868*Power(t2,2) - 21953*Power(t2,3) + 
                  2455*Power(t2,4) + 1751*Power(t2,5) - 
                  112*Power(t2,6) - 5*Power(t1,5)*(1606 + 221*t2) + 
                  Power(t1,4)*(60171 + 30135*t2 + 500*Power(t2,2)) + 
                  Power(t1,3)*
                   (-141441 - 70955*t2 + 50495*Power(t2,2) + 
                     12915*Power(t2,3)) + 
                  Power(t1,2)*
                   (144745 + 70827*t2 - 159062*Power(t2,2) - 
                     34950*Power(t2,3) + 2765*Power(t2,4)) - 
                  t1*(70794 + 43700*t2 - 149170*Power(t2,2) - 
                     39386*Power(t2,3) + 5950*Power(t2,4) + 
                     1127*Power(t2,5)))) + 
            Power(s1,2)*(-1003 - 26*Power(s2,9) + 15*Power(t1,8) + 
               22570*t2 + 10000*Power(t2,2) - 1856*Power(t2,3) - 
               6695*Power(t2,4) + 1410*Power(t2,5) + 
               Power(t1,7)*(921 + 370*t2) + 
               Power(s2,8)*(422 - 319*t1 + 384*t2) + 
               Power(t1,6)*(-382 + 582*t2 + 336*Power(t2,2)) - 
               2*Power(t1,5)*
                (12777 + 10445*t2 + 188*Power(t2,2) + 492*Power(t2,3)) \
+ Power(t1,4)*(90076 + 101214*t2 + 3055*Power(t2,2) - 
                  2825*Power(t2,3) + 245*Power(t2,4)) + 
               Power(t1,3)*(-121694 - 213642*t2 - 32467*Power(t2,2) + 
                  36470*Power(t2,3) - 2990*Power(t2,4) + 
                  102*Power(t2,5)) + 
               Power(t1,2)*(71558 + 220339*t2 + 72661*Power(t2,2) - 
                  69015*Power(t2,3) + 2325*Power(t2,4) + 
                  600*Power(t2,5)) - 
               t1*(13923 + 110645*t2 + 52993*Power(t2,2) - 
                  38070*Power(t2,3) - 7085*Power(t2,4) + 
                  2070*Power(t2,5)) + 
               Power(s2,7)*(-948 + 229*Power(t1,2) - 2911*t2 - 
                  608*Power(t2,2) + t1*(519 + 3146*t2)) - 
               Power(s2,6)*(1880 + 363*Power(t1,3) - 3839*t2 - 
                  7098*Power(t2,2) + 1325*Power(t2,3) + 
                  Power(t1,2)*(4973 + 1620*t2) + 
                  t1*(-7451 + 4171*t2 + 14917*Power(t2,2))) + 
               Power(s2,5)*(3144 + 966*Power(t1,4) + 
                  Power(t1,3)*(3249 - 4993*t2) + 11556*t2 + 
                  388*Power(t2,2) - 9530*Power(t2,3) + 
                  2341*Power(t2,4) + 
                  Power(t1,2)*(2835 + 51461*t2 + 20193*Power(t2,2)) + 
                  t1*(-9956 - 55317*t2 - 14757*Power(t2,2) + 
                     28173*Power(t2,3))) - 
               Power(s2,4)*(-4041 + 2154*Power(t1,5) - 10393*t2 + 
                  12561*Power(t2,2) + 12989*Power(t2,3) - 
                  1609*Power(t2,4) + 456*Power(t2,5) - 
                  Power(t1,4)*(14368 + 9421*t2) + 
                  Power(t1,3)*(70377 + 86022*t2 + 3252*Power(t2,2)) + 
                  Power(t1,2)*
                   (-97424 - 119865*t2 + 65135*Power(t2,2) + 
                     44835*Power(t2,3)) + 
                  t1*(44184 + 58669*t2 - 82035*Power(t2,2) - 
                     62440*Power(t2,3) + 17078*Power(t2,4))) + 
               Power(s2,3)*(6689 + 1310*Power(t1,6) - 6275*t2 - 
                  36155*Power(t2,2) - 13040*Power(t2,3) + 
                  7834*Power(t2,4) + 3633*Power(t2,5) - 
                  315*Power(t2,6) - Power(t1,5)*(15549 + 2981*t2) + 
                  Power(t1,4)*(75195 + 16276*t2 - 12830*Power(t2,2)) + 
                  Power(t1,3)*
                   (-98901 + 79273*t2 + 152796*Power(t2,2) + 
                     25320*Power(t2,3)) + 
                  Power(t1,2)*
                   (45772 - 182967*t2 - 315865*Power(t2,2) - 
                     33811*Power(t2,3) + 23150*Power(t2,4)) + 
                  t1*(-13639 + 97434*t2 + 218334*Power(t2,2) + 
                     16648*Power(t2,3) - 36685*Power(t2,4) + 
                     1402*Power(t2,5))) + 
               s2*(-10676 + 180*Power(t1,7) - 43*t2 + 
                  48237*Power(t2,2) + 31546*Power(t2,3) - 
                  7725*Power(t2,4) - 5024*Power(t2,5) + 
                  776*Power(t2,6) - 
                  Power(t1,6)*(12681 + 4623*t2 + 250*Power(t2,2)) + 
                  Power(t1,5)*
                   (80818 + 27177*t2 - 3040*Power(t2,2) + 
                     120*Power(t2,3)) + 
                  Power(t1,4)*
                   (-179859 - 38693*t2 + 63155*Power(t2,2) + 
                     12700*Power(t2,3) + 300*Power(t2,4)) + 
                  Power(t1,3)*
                   (190892 + 560*t2 - 243994*Power(t2,2) - 
                     54070*Power(t2,3) + 8775*Power(t2,4) - 
                     296*Power(t2,5)) + 
                  t1*(45193 - 4451*t2 - 233731*Power(t2,2) - 
                     91335*Power(t2,3) + 33785*Power(t2,4) + 
                     5186*Power(t2,5) - 720*Power(t2,6)) + 
                  Power(t1,2)*
                   (-113972 + 20027*t2 + 370949*Power(t2,2) + 
                     99067*Power(t2,3) - 33820*Power(t2,4) - 
                     574*Power(t2,5) + 42*Power(t2,6))) - 
               Power(s2,2)*(745 + 150*Power(t1,7) + 39080*t2 + 
                  3432*Power(t2,2) - 32129*Power(t2,3) - 
                  18067*Power(t2,4) + 2223*Power(t2,5) + 
                  865*Power(t2,6) - 56*Power(t2,7) + 
                  Power(t1,6)*(-2077 + 285*t2) - 
                  Power(t1,5)*(7385 + 22739*t2 + 5043*Power(t2,2)) + 
                  Power(t1,4)*
                   (113117 + 186284*t2 + 56760*Power(t2,2) + 
                     1795*Power(t2,3)) + 
                  Power(t1,3)*
                   (-245573 - 444612*t2 - 117567*Power(t2,2) + 
                     48340*Power(t2,3) + 9630*Power(t2,4)) + 
                  Power(t1,2)*
                   (194955 + 462778*t2 + 94062*Power(t2,2) - 
                     160827*Power(t2,3) - 23705*Power(t2,4) + 
                     1602*Power(t2,5)) - 
                  t1*(53678 + 221882*t2 + 36546*Power(t2,2) - 
                     150920*Power(t2,3) - 27814*Power(t2,4) + 
                     3999*Power(t2,5) + 509*Power(t2,6)))) - 
            s1*(-6521 + Power(s2,9)*(-44 + 40*t1 - 60*t2) + 2060*t2 + 
               21643*Power(t2,2) + 5263*Power(t2,3) + 2794*Power(t2,4) - 
               4761*Power(t2,5) + 760*Power(t2,6) + 
               Power(t1,8)*(186 + 35*t2) + 
               Power(t1,7)*(3037 + 1010*t2 + 252*Power(t2,2)) - 
               Power(t1,6)*(19771 + 2079*t2 - 144*Power(t2,2) + 
                  198*Power(t2,3)) + 
               Power(t1,5)*(29938 - 19344*t2 - 19903*Power(t2,2) + 
                  1373*Power(t2,3) - 344*Power(t2,4)) + 
               Power(t1,4)*(-4638 + 82667*t2 + 102724*Power(t2,2) + 
                  915*Power(t2,3) - 3410*Power(t2,4) + 291*Power(t2,5)) \
+ t1*(16829 - 21116*t2 - 108417*Power(t2,2) - 34460*Power(t2,3) + 
                  12385*Power(t2,4) + 7115*Power(t2,5) - 
                  1280*Power(t2,6)) - 
               Power(t1,3)*(14180 + 119418*t2 + 217097*Power(t2,2) + 
                  24133*Power(t2,3) - 22150*Power(t2,4) + 
                  1217*Power(t2,5) + 36*Power(t2,6)) + 
               Power(t1,2)*(-4882 + 76213*t2 + 220552*Power(t2,2) + 
                  51384*Power(t2,3) - 33645*Power(t2,4) - 
                  1440*Power(t2,5) + 570*Power(t2,6)) + 
               Power(s2,8)*(196 - 39*Power(t1,2) + 744*t2 + 
                  402*Power(t2,2) - 2*t1*(64 + 251*t2)) + 
               Power(s2,7)*(205 + 176*Power(t1,3) + 
                  Power(t1,2)*(483 - 454*t2) - 1985*t2 - 
                  2826*Power(t2,2) - 338*Power(t2,3) + 
                  t1*(-953 + 1982*t2 + 3073*Power(t2,2))) - 
               Power(s2,6)*(2140 + 491*Power(t1,4) + 3617*t2 - 
                  4567*Power(t2,2) - 5817*Power(t2,3) + 
                  622*Power(t2,4) - Power(t1,3)*(2253 + 2542*t2) + 
                  3*Power(t1,2)*(2205 + 4780*t2 + 51*Power(t2,2)) + 
                  t1*(-7106 - 15975*t2 + 6413*Power(t2,2) + 
                     11526*Power(t2,3))) + 
               Power(s2,5)*(-138 + 1064*Power(t1,5) + 6204*t2 + 
                  13952*Power(t2,2) + 1393*Power(t2,3) - 
                  5547*Power(t2,4) + 637*Power(t2,5) - 
                  Power(t1,4)*(10069 + 4024*t2) - 
                  3*Power(t1,3)*(-9642 - 4956*t2 + 3571*Power(t2,2)) + 
                  Power(t1,2)*
                   (-28899 + 2970*t2 + 63803*Power(t2,2) + 
                     17124*Power(t2,3)) + 
                  t1*(9108 - 19714*t2 - 64657*Power(t2,2) - 
                     14320*Power(t2,3) + 15582*Power(t2,4))) - 
               Power(s2,4)*(-2312 + 780*Power(t1,6) - 10383*t2 - 
                  9491*Power(t2,2) + 9850*Power(t2,3) + 
                  7637*Power(t2,4) + 32*Power(t2,5) - 56*Power(t2,6) - 
                  Power(t1,5)*(9423 + 1156*t2) + 
                  Power(t1,4)*(12849 - 23732*t2 - 17452*Power(t2,2)) + 
                  Power(t1,3)*
                   (29731 + 144582*t2 + 105014*Power(t2,2) + 
                     4798*Power(t2,3)) + 
                  Power(t1,2)*
                   (-61742 - 203601*t2 - 138566*Power(t2,2) + 
                     42131*Power(t2,3) + 25553*Power(t2,4)) + 
                  t1*(30266 + 95872*t2 + 65316*Power(t2,2) - 
                     58347*Power(t2,3) - 35216*Power(t2,4) + 
                     6713*Power(t2,5))) + 
               Power(s2,3)*(2381 + 140*Power(t1,7) + 15115*t2 - 
                  8766*Power(t2,2) - 19538*Power(t2,3) - 
                  7663*Power(t2,4) + 2722*Power(t2,5) + 
                  1478*Power(t2,6) - 127*Power(t2,7) + 
                  Power(t1,6)*(-958 + 526*t2) - 
                  Power(t1,5)*(36303 + 36068*t2 + 8251*Power(t2,2)) + 
                  Power(t1,4)*
                   (163350 + 178082*t2 + 30500*Power(t2,2) - 
                     7550*Power(t2,3)) + 
                  Power(t1,3)*
                   (-238479 - 259108*t2 + 60206*Power(t2,2) + 
                     101548*Power(t2,3) + 14935*Power(t2,4)) + 
                  Power(t1,2)*
                   (142773 + 159560*t2 - 172909*Power(t2,2) - 
                     213727*Power(t2,3) - 19239*Power(t2,4) + 
                     8971*Power(t2,5)) + 
                  t1*(-32699 - 56444*t2 + 100017*Power(t2,2) + 
                     143950*Power(t2,3) + 8473*Power(t2,4) - 
                     13624*Power(t2,5) + 330*Power(t2,6))) + 
               Power(s2,2)*(7197 - 3785*t2 - 29212*Power(t2,2) + 
                  3523*Power(t2,3) + 13674*Power(t2,4) + 
                  7832*Power(t2,5) - 1079*Power(t2,6) - 
                  227*Power(t2,7) + 16*Power(t2,8) - 
                  Power(t1,7)*(703 + 90*t2) + 
                  Power(t1,6)*(25965 + 10304*t2 + 1012*Power(t2,2)) + 
                  Power(t1,5)*
                   (-91027 - 20498*t2 + 17861*Power(t2,2) + 
                     3352*Power(t2,3)) - 
                  Power(t1,4)*
                   (-76694 + 120773*t2 + 173879*Power(t2,2) + 
                     39045*Power(t2,3) + 2175*Power(t2,4)) + 
                  Power(t1,3)*
                   (37452 + 341242*t2 + 428787*Power(t2,2) + 
                     77955*Power(t2,3) - 20700*Power(t2,4) - 
                     3277*Power(t2,5)) - 
                  Power(t1,2)*
                   (62182 + 299061*t2 + 448467*Power(t2,2) + 
                     51462*Power(t2,3) - 76819*Power(t2,4) - 
                     7054*Power(t2,5) + 465*Power(t2,6)) + 
                  t1*(6469 + 92181*t2 + 204798*Power(t2,2) + 
                     9002*Power(t2,3) - 72105*Power(t2,4) - 
                     9510*Power(t2,5) + 1502*Power(t2,6) + 
                     115*Power(t2,7))) + 
               s2*(-2021 + 35*Power(t1,8) - 17778*t2 + 
                  9107*Power(t2,2) + 28318*Power(t2,3) + 
                  18879*Power(t2,4) - 3887*Power(t2,5) - 
                  2032*Power(t2,6) + 298*Power(t2,7) - 
                  Power(t1,7)*(3193 + 750*t2 + 50*Power(t2,2)) - 
                  2*Power(t1,6)*
                   (5418 + 7273*t2 + 2189*Power(t2,2) + 90*Power(t2,3)) \
+ Power(t1,5)*(124595 + 121828*t2 + 28900*Power(t2,2) + 
                     42*Power(t2,3) + 280*Power(t2,4)) + 
                  Power(t1,4)*
                   (-288561 - 313898*t2 - 46866*Power(t2,2) + 
                     35965*Power(t2,3) + 4650*Power(t2,4) + 
                     32*Power(t2,5)) + 
                  Power(t1,3)*
                   (284352 + 389674*t2 + 10375*Power(t2,2) - 
                     159854*Power(t2,3) - 20635*Power(t2,4) + 
                     3794*Power(t2,5) - 126*Power(t2,6)) + 
                  Power(t1,2)*
                   (-128893 - 266068*t2 + 25621*Power(t2,2) + 
                     249601*Power(t2,3) + 43108*Power(t2,4) - 
                     14565*Power(t2,5) - 233*Power(t2,6) + 
                     44*Power(t2,7)) - 
                  2*t1*(-12281 - 50660*t2 + 11369*Power(t2,2) + 
                     76499*Power(t2,3) + 23665*Power(t2,4) - 
                     7614*Power(t2,5) - 1056*Power(t2,6) + 
                     152*Power(t2,7))))) - 
         (-1 + t1)*(2*Power(s1,9)*Power(s2,2)*(-1 + t1)*
             (2 + 3*Power(s2,2) + s2*(5 - 6*t1) - 5*t1 + 3*Power(t1,2)) \
+ Power(s2,10)*Power(t2,3)*(6 - 6*t1 + 5*t2) + 
            Power(s2,9)*Power(t2,2)*
             (22 + 2*Power(t1,3) + 9*t2 - 32*Power(t2,2) - 
               20*Power(t2,3) + 3*Power(t1,2)*(6 + 5*t2) + 
               t1*(-42 - 24*t2 + 11*Power(t2,2))) + 
            Power(s2,8)*t2*(26 - 4*Power(t1,4) - t2 - 160*Power(t2,2) - 
               130*Power(t2,3) + 10*Power(t2,4) + 30*Power(t2,5) - 
               Power(t1,3)*(14 + 83*t2 + 47*Power(t2,2)) - 
               Power(t1,2)*(-66 - 165*t2 + Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(-74 - 81*t2 + 208*Power(t2,2) + 183*Power(t2,3) + 
                  14*Power(t2,4))) + 
            Power(s2,7)*(10 - 9*t2 - 339*Power(t2,2) - 
               263*Power(t2,3) + 183*Power(t2,4) + 263*Power(t2,5) + 
               60*Power(t2,6) - 20*Power(t2,7) + 
               Power(t1,5)*(2 - 6*Power(t2,2)) + 
               Power(t1,4)*(2 + 121*t2 + 243*Power(t2,2) + 
                  79*Power(t2,3)) + 
               Power(t1,3)*(-28 - 354*t2 - 327*Power(t2,2) + 
                  15*Power(t2,3) + 54*Power(t2,4)) - 
               Power(t1,2)*(-52 - 336*t2 + 390*Power(t2,2) + 
                  736*Power(t2,3) + 257*Power(t2,4) + 11*Power(t2,5)) + 
               t1*(-38 - 94*t2 + 819*Power(t2,2) + 905*Power(t2,3) + 
                  15*Power(t2,4) - 216*Power(t2,5) - 46*Power(t2,6))) - 
            Power(-1 + t1,3)*
             (32*Power(t1,7) + 
               4*Power(t1,6)*(71 - 20*t2 + 5*Power(t2,2)) - 
               Power(t1,5)*(1557 + 615*t2 - 181*Power(t2,2) + 
                  25*Power(t2,3)) + 
               Power(t1,4)*(2922 + 2105*t2 + 327*Power(t2,2) - 
                  353*Power(t2,3) + 39*Power(t2,4)) + 
               2*Power(1 + t2,2)*
                (-30 + 34*t2 - 5*Power(t2,2) + 11*Power(t2,3) - 
                  13*Power(t2,4) + 3*Power(t2,5)) - 
               Power(t1,3)*(2636 + 2287*t2 + 1224*Power(t2,2) - 
                  210*Power(t2,3) - 292*Power(t2,4) + 51*Power(t2,5)) + 
               Power(t1,2)*(1064 + 1005*t2 + 967*Power(t2,2) + 
                  404*Power(t2,3) - 388*Power(t2,4) - 57*Power(t2,5) + 
                  29*Power(t2,6)) - 
               t1*(49 + 78*t2 + 323*Power(t2,2) + 340*Power(t2,3) - 
                  85*Power(t2,4) - 118*Power(t2,5) + 17*Power(t2,6) + 
                  4*Power(t2,7))) + 
            Power(s2,6)*(-4 - 324*t2 - 250*Power(t2,2) + 
               718*Power(t2,3) + 1066*Power(t2,4) + 482*Power(t2,5) - 
               13*Power(t2,6) - 50*Power(t2,7) + 5*Power(t2,8) + 
               4*Power(t1,6)*t2*(3 + t2) - 
               Power(t1,5)*(53 + 357*t2 + 307*Power(t2,2) + 
                  49*Power(t2,3)) + 
               Power(t1,4)*(208 + 817*t2 + 99*Power(t2,2) - 
                  320*Power(t2,3) - 121*Power(t2,4)) + 
               Power(t1,3)*(-302 - 105*t2 + 2018*Power(t2,2) + 
                  1675*Power(t2,3) + 493*Power(t2,4) + 27*Power(t2,5)) \
+ Power(t1,2)*(188 - 1197*t2 - 3375*Power(t2,2) - 1211*Power(t2,3) + 
                  511*Power(t2,4) + 318*Power(t2,5) + 45*Power(t2,6)) + 
               t1*(-37 + 1154*t2 + 1811*Power(t2,2) - 813*Power(t2,3) - 
                  1974*Power(t2,4) - 875*Power(t2,5) - 36*Power(t2,6) + 
                  34*Power(t2,7))) + 
            Power(s2,5)*(-113 - 133*t2 + 1266*Power(t2,2) + 
               2020*Power(t2,3) + 1286*Power(t2,4) + 423*Power(t2,5) - 
               49*Power(t2,6) - 44*Power(t2,7) + 4*Power(t2,8) - 
               2*Power(t1,7)*(3 + 4*t2) + 
               Power(t1,6)*(161 + 385*t2 + 149*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,5)*(-506 - 287*t2 + 1046*Power(t2,2) + 
                  661*Power(t2,3) + 103*Power(t2,4)) + 
               Power(t1,4)*(421 - 2691*t2 - 5290*Power(t2,2) - 
                  2924*Power(t2,3) - 466*Power(t2,4) + 9*Power(t2,5)) + 
               Power(t1,3)*(366 + 6198*t2 + 6352*Power(t2,2) + 
                  1314*Power(t2,3) - 977*Power(t2,4) - 
                  459*Power(t2,5) - 45*Power(t2,6)) + 
               Power(t1,2)*(-853 - 5185*t2 - 497*Power(t2,2) + 
                  5306*Power(t2,3) + 4586*Power(t2,4) + 
                  1518*Power(t2,5) + 21*Power(t2,6) - 34*Power(t2,7)) + 
               t1*(530 + 1721*t2 - 3026*Power(t2,2) - 
                  6385*Power(t2,3) - 4507*Power(t2,4) - 
                  1503*Power(t2,5) + 53*Power(t2,6) + 102*Power(t2,7) - 
                  5*Power(t2,8))) + 
            s2*Power(-1 + t1,2)*
             (17 - 236*t2 - 45*Power(t2,2) + 336*Power(t2,3) + 
               201*Power(t2,4) - 24*Power(t2,5) - 99*Power(t2,6) + 
               4*Power(t2,7) + 6*Power(t2,8) + 
               24*Power(t1,7)*(8 + 5*t2) + 
               Power(t1,6)*(173 + 386*t2 - 151*Power(t2,2) + 
                  56*Power(t2,3)) - 
               Power(t1,5)*(3380 + 5089*t2 + 1589*Power(t2,2) - 
                  369*Power(t2,3) + 63*Power(t2,4)) + 
               Power(t1,4)*(8240 + 12638*t2 + 7199*Power(t2,2) + 
                  470*Power(t2,3) - 829*Power(t2,4) + 82*Power(t2,5)) - 
               Power(t1,3)*(9031 + 13287*t2 + 10162*Power(t2,2) + 
                  3463*Power(t2,3) - 1234*Power(t2,4) - 
                  610*Power(t2,5) + 109*Power(t2,6)) + 
               2*Power(t1,2)*
                (2431 + 3048*t2 + 2937*Power(t2,2) + 
                  2101*Power(t2,3) + 33*Power(t2,4) - 
                  555*Power(t2,5) - 27*Power(t2,6) + 28*Power(t2,7)) - 
               t1*(1073 + 628*t2 + 1128*Power(t2,2) + 
                  1953*Power(t2,3) + 657*Power(t2,4) - 
                  504*Power(t2,5) - 224*Power(t2,6) + 51*Power(t2,7) + 
                  6*Power(t2,8))) + 
            Power(s2,4)*(-34 + 4*Power(t1,8) + 1065*t2 + 
               2125*Power(t2,2) + 1474*Power(t2,3) + 781*Power(t2,4) + 
               451*Power(t2,5) + 51*Power(t2,6) - 70*Power(t2,7) - 
               9*Power(t2,8) + 2*Power(t2,9) - 
               Power(t1,7)*(157 + 151*t2 + 20*Power(t2,2)) - 
               Power(t1,6)*(-201 + 1344*t2 + 1540*Power(t2,2) + 
                  451*Power(t2,3) + 28*Power(t2,4)) + 
               Power(t1,5)*(1176 + 7474*t2 + 6487*Power(t2,2) + 
                  1990*Power(t2,3) + 64*Power(t2,4) - 35*Power(t2,5)) + 
               Power(t1,4)*(-3748 - 12199*t2 - 5753*Power(t2,2) + 
                  1306*Power(t2,3) + 1920*Power(t2,4) + 
                  317*Power(t2,5) + 29*Power(t2,6)) + 
               Power(t1,3)*(4483 + 6393*t2 - 7692*Power(t2,2) - 
                  12531*Power(t2,3) - 7860*Power(t2,4) - 
                  1488*Power(t2,5) + 197*Power(t2,6) + 16*Power(t2,7)) \
+ Power(t1,2)*(-2551 + 2606*t2 + 16640*Power(t2,2) + 
                  16972*Power(t2,3) + 10335*Power(t2,4) + 
                  2910*Power(t2,5) - 500*Power(t2,6) - 
                  173*Power(t2,7) + 7*Power(t2,8)) - 
               t1*(-626 + 3844*t2 + 10247*Power(t2,2) + 
                  8760*Power(t2,3) + 5221*Power(t2,4) + 
                  2131*Power(t2,5) - 197*Power(t2,6) - 
                  243*Power(t2,7) + 3*Power(t2,8) + 2*Power(t2,9))) + 
            Power(s2,2)*(354 - 4*Power(t1,9) + 209*t2 - 
               228*Power(t2,2) + 289*Power(t2,3) + 634*Power(t2,4) + 
               197*Power(t2,5) - 140*Power(t2,6) - 65*Power(t2,7) + 
               12*Power(t2,8) + 2*Power(t2,9) - 
               Power(t1,8)*(589 + 629*t2 + 172*Power(t2,2)) + 
               Power(t1,7)*(2666 + 1461*t2 + 362*Power(t2,2) + 
                  27*Power(t2,3) - 52*Power(t2,4)) + 
               Power(t1,6)*(-3222 + 5917*t2 + 6344*Power(t2,2) + 
                  1568*Power(t2,3) - 114*Power(t2,4) + 51*Power(t2,5)) \
- Power(t1,5)*(3404 + 29291*t2 + 28546*Power(t2,2) + 
                  11097*Power(t2,3) - 289*Power(t2,4) - 
                  564*Power(t2,5) + 47*Power(t2,6)) + 
               Power(t1,4)*(13709 + 51069*t2 + 49922*Power(t2,2) + 
                  26717*Power(t2,3) + 2752*Power(t2,4) - 
                  2592*Power(t2,5) - 304*Power(t2,6) + 65*Power(t2,7)) \
- Power(t1,3)*(16346 + 45546*t2 + 43471*Power(t2,2) + 
                  30160*Power(t2,3) + 8398*Power(t2,4) - 
                  3524*Power(t2,5) - 1348*Power(t2,6) + 
                  106*Power(t2,7) + 29*Power(t2,8)) + 
               Power(t1,2)*(9804 + 21310*t2 + 18444*Power(t2,2) + 
                  16845*Power(t2,3) + 8893*Power(t2,4) - 
                  1568*Power(t2,5) - 1758*Power(t2,6) - 
                  35*Power(t2,7) + 67*Power(t2,8) + 2*Power(t2,9)) - 
               t1*(2968 + 4500*t2 + 2655*Power(t2,2) + 
                  4189*Power(t2,3) + 4004*Power(t2,4) + 
                  176*Power(t2,5) - 901*Power(t2,6) - 141*Power(t2,7) + 
                  50*Power(t2,8) + 4*Power(t2,9))) + 
            Power(s2,3)*(342 + 1302*t2 + 808*Power(t2,2) + 
               295*Power(t2,3) + 726*Power(t2,4) + 474*Power(t2,5) + 
               2*Power(t2,6) - 119*Power(t2,7) - 2*Power(t2,8) + 
               4*Power(t2,9) + Power(t1,8)*(51 + 16*t2) + 
               Power(t1,7)*(564 + 1589*t2 + 785*Power(t2,2) + 
                  112*Power(t2,3)) + 
               Power(t1,6)*(-3614 - 6707*t2 - 2801*Power(t2,2) - 
                  393*Power(t2,3) + 79*Power(t2,4) + 16*Power(t2,5)) - 
               Power(t1,5)*(-7531 - 7041*t2 + 2934*Power(t2,2) + 
                  4426*Power(t2,3) + 928*Power(t2,4) + 
                  51*Power(t2,5) + 13*Power(t2,6)) + 
               Power(t1,4)*(-6707 + 7740*t2 + 23555*Power(t2,2) + 
                  19826*Power(t2,3) + 6068*Power(t2,4) - 
                  400*Power(t2,5) - 155*Power(t2,6) + 4*Power(t2,7)) - 
               Power(t1,3)*(-1202 + 23869*t2 + 37821*Power(t2,2) + 
                  30762*Power(t2,3) + 13560*Power(t2,4) + 
                  114*Power(t2,5) - 1147*Power(t2,6) - 
                  26*Power(t2,7) + 7*Power(t2,8)) + 
               Power(t1,2)*(2264 + 21545*t2 + 26816*Power(t2,2) + 
                  21328*Power(t2,3) + 13027*Power(t2,4) + 
                  1884*Power(t2,5) - 1631*Power(t2,6) - 
                  269*Power(t2,7) + 40*Power(t2,8) + 2*Power(t2,9)) - 
               t1*(1633 + 8657*t2 + 8408*Power(t2,2) + 
                  5980*Power(t2,3) + 5411*Power(t2,4) + 
                  1813*Power(t2,5) - 656*Power(t2,6) - 
                  354*Power(t2,7) + 30*Power(t2,8) + 6*Power(t2,9))) + 
            Power(s1,8)*s2*(5*Power(s2,5) + 
               4*Power(-1 + t1,3)*(-2 + 3*t1) + 
               Power(s2,4)*(-11 + 10*t1) + 
               Power(s2,3)*(-42 - 18*Power(t1,2) + t1*(55 - 50*t2) + 
                  50*t2) + s2*(-1 + t1)*
                (17*Power(t1,3) - 8*Power(t1,2)*(7 + 6*t2) - 
                  2*(9 + 17*t2) + t1*(54 + 82*t2)) + 
               Power(s2,2)*(-16 - 14*Power(t1,3) + t1*(14 - 182*t2) + 
                  84*t2 + Power(t1,2)*(17 + 98*t2))) + 
            Power(s1,7)*(20*Power(s2,7) + 
               2*Power(-1 + t1,3)*
                (2 - 5*t1 + 3*Power(t1,2) + Power(t1,3)) - 
               2*Power(s2,6)*(-15 + 7*t1 + 20*t2) + 
               s2*Power(-1 + t1,2)*
                (-6 + Power(t1,4) - 60*t2 + 2*Power(t1,3)*(14 + t2) + 
                  6*t1*(7 + 24*t2) - 2*Power(t1,2)*(37 + 43*t2)) + 
               Power(s2,5)*(21 + 31*Power(t1,2) + 73*t2 - 
                  t1*(76 + 65*t2)) + 
               Power(s2,2)*(-1 + t1)*
                (14*Power(t1,4) + Power(t1,3)*(6 - 127*t2) - 
                  8*t1*(-34 + 53*t2 + 37*Power(t2,2)) + 
                  2*(-56 + 69*t2 + 64*Power(t2,2)) + 
                  Power(t1,2)*(-193 + 437*t2 + 168*Power(t2,2))) + 
               Power(s2,4)*(139 - 84*Power(t1,3) + 299*t2 - 
                  184*Power(t2,2) + 5*Power(t1,2)*(78 + 23*t2) + 
                  t1*(-461 - 374*t2 + 184*Power(t2,2))) + 
               Power(s2,3)*(242 + 32*Power(t1,4) + 108*t2 - 
                  312*Power(t2,2) + Power(t1,3)*(-364 + 115*t2) + 
                  Power(t1,2)*(908 - 185*t2 - 352*Power(t2,2)) + 
                  t1*(-814 - 46*t2 + 664*Power(t2,2)))) + 
            Power(s1,6)*(30*Power(s2,8) - 
               2*Power(s2,7)*(-25 + 18*t1 + 70*t2) - 
               Power(-1 + t1,3)*
                (-2 + Power(t1,4) + 26*t2 + 
                  3*Power(t1,3)*(-7 + 2*t2) - 2*t1*(5 + 31*t2) + 
                  Power(t1,2)*(30 + 44*t2)) + 
               2*Power(s2,6)*
                (-2 + 33*Power(t1,2) - 115*t2 + 70*Power(t2,2) + 
                  t1*(-33 + 59*t2)) + 
               Power(s2,5)*(125 - 82*Power(t1,3) + 
                  Power(t1,2)*(253 - 245*t2) - 195*t2 - 
                  203*Power(t2,2) + 
                  t1*(-316 + 608*t2 + 175*Power(t2,2))) + 
               s2*Power(-1 + t1,2)*
                (44*Power(t1,4) + 
                  t1*(454 - 330*t2 - 444*Power(t2,2)) + 
                  Power(t1,3)*(3 - 191*t2 - 10*Power(t2,2)) + 
                  4*(-38 + 12*t2 + 49*Power(t2,2)) + 
                  Power(t1,2)*(-387 + 536*t2 + 258*Power(t2,2))) - 
               Power(s2,4)*(-278 + 30*Power(t1,4) + 963*t2 + 
                  925*Power(t2,2) - 392*Power(t2,3) - 
                  Power(t1,3)*(248 + 577*t2) + 
                  Power(t1,2)*(146 + 2686*t2 + 309*Power(t2,2)) + 
                  2*t1*(188 - 1592*t2 - 547*Power(t2,2) + 
                     196*Power(t2,3))) + 
               Power(s2,2)*(-1 + t1)*
                (342 + 10*Power(t1,5) + Power(t1,4)*(171 - 89*t2) + 
                  718*t2 - 454*Power(t2,2) - 280*Power(t2,3) + 
                  Power(t1,3)*(-1199 + 27*t2 + 397*Power(t2,2)) + 
                  Power(t1,2)*
                   (2133 + 1117*t2 - 1451*Power(t2,2) - 
                     336*Power(t2,3)) + 
                  t1*(-1479 - 1682*t2 + 1424*Power(t2,2) + 
                     616*Power(t2,3))) + 
               Power(s2,3)*(-63 + 42*Power(t1,5) - 1598*t2 - 
                  324*Power(t2,2) + 672*Power(t2,3) - 
                  13*Power(t1,4)*(53 + 17*t2) + 
                  Power(t1,3)*(2167 + 2359*t2 - 393*Power(t2,2)) + 
                  t1*(961 + 5375*t2 - 18*Power(t2,2) - 
                     1400*Power(t2,3)) + 
                  Power(t1,2)*
                   (-2412 - 5943*t2 + 763*Power(t2,2) + 728*Power(t2,3))\
)) + Power(s1,5)*(20*Power(s2,9) - 2*Power(s2,8)*(5 + 7*t1 + 90*t2) + 
               2*Power(s2,7)*
                (-139 + 6*Power(t1,2) - 155*t2 + 210*Power(t2,2) + 
                  t1*(115 + 113*t2)) + 
               Power(s2,6)*(-531 + 30*Power(t1,3) + 2*t2 + 
                  750*Power(t2,2) - 280*Power(t2,3) - 
                  7*Power(t1,2)*(71 + 58*t2) + 
                  t1*(1046 + 428*t2 - 414*Power(t2,2))) + 
               Power(-1 + t1,3)*
                (-62 - 6*t2 + 72*Power(t2,2) + 
                  Power(t1,4)*(26 + 7*t2) + 
                  Power(t1,3)*(-3 - 94*t2 + 4*Power(t2,2)) - 
                  8*t1*(-26 + 11*t2 + 19*Power(t2,2)) + 
                  Power(t1,2)*(-183 + 169*t2 + 118*Power(t2,2))) + 
               Power(s2,5)*(-648 - 112*Power(t1,4) - 662*t2 + 
                  702*Power(t2,2) + 301*Power(t2,3) + 
                  Power(t1,3)*(1165 + 488*t2) + 
                  2*Power(t1,2)*(-1447 - 719*t2 + 396*Power(t2,2)) + 
                  t1*(2501 + 1732*t2 - 1998*Power(t2,2) - 
                     245*Power(t2,3))) + 
               s2*Power(-1 + t1,2)*
                (126 + 7*Power(t1,5) + 856*t2 - 144*Power(t2,2) - 
                  366*Power(t2,3) - 
                  2*Power(t1,4)*(-148 + 87*t2 + 5*Power(t2,2)) + 
                  Power(t1,2)*
                   (1934 + 2180*t2 - 1581*Power(t2,2) - 
                     420*Power(t2,3)) + 
                  Power(t1,3)*
                   (-1521 - 72*t2 + 510*Power(t2,2) + 20*Power(t2,3)) \
+ 2*t1*(-452 - 1281*t2 + 518*Power(t2,2) + 383*Power(t2,3))) + 
               Power(s2,4)*(-1164 + 36*Power(t1,5) + 
                  180*Power(t1,4)*(-5 + t2) - 1411*t2 + 
                  2787*Power(t2,2) + 1619*Power(t2,3) - 
                  532*Power(t2,4) + 
                  Power(t1,3)*(3966 - 1559*t2 - 1650*Power(t2,2)) + 
                  Power(t1,2)*
                   (-6754 + 1327*t2 + 7758*Power(t2,2) + 
                     443*Power(t2,3)) + 
                  t1*(4792 + 1619*t2 - 9231*Power(t2,2) - 
                     1782*Power(t2,3) + 532*Power(t2,4))) - 
               Power(s2,2)*(-1 + t1)*
                (-463 + 2007*t2 + 1974*Power(t2,2) - 842*Power(t2,3) - 
                  392*Power(t2,4) + Power(t1,5)*(-245 + 18*t2) + 
                  Power(t1,4)*(1771 + 1058*t2 - 212*Power(t2,2)) + 
                  Power(t1,3)*
                   (-3235 - 7018*t2 + 222*Power(t2,2) + 
                     675*Power(t2,3)) + 
                  Power(t1,2)*
                   (1629 + 12541*t2 + 2715*Power(t2,2) - 
                     2689*Power(t2,3) - 420*Power(t2,4)) + 
                  t1*(561 - 8738*t2 - 4426*Power(t2,2) + 
                     2688*Power(t2,3) + 812*Power(t2,4))) + 
               Power(s2,3)*(-1326 + 28*Power(t1,6) + 540*t2 + 
                  4461*Power(t2,2) + 562*Power(t2,3) - 
                  924*Power(t2,4) - 10*Power(t1,5)*(24 + 29*t2) + 
                  2*Power(t1,4)*(84 + 2023*t2 + 325*Power(t2,2)) + 
                  Power(t1,3)*
                   (2601 - 12950*t2 - 6430*Power(t2,2) + 
                     737*Power(t2,3)) + 
                  Power(t1,2)*
                   (-6102 + 14888*t2 + 16397*Power(t2,2) - 
                     1675*Power(t2,3) - 952*Power(t2,4)) + 
                  t1*(4875 - 6270*t2 - 14994*Power(t2,2) + 
                     320*Power(t2,3) + 1876*Power(t2,4)))) + 
            Power(s1,4)*(5*Power(s2,10) + 
               Power(s2,9)*(-31 + 10*t1 - 100*t2) + 
               Power(s2,8)*(-32*Power(t1,2) + t1*(211 + 70*t2) + 
                  50*(-3 + t2 + 9*Power(t2,2))) + 
               Power(s2,7)*(169 + 78*Power(t1,3) + 1372*t2 + 
                  800*Power(t2,2) - 700*Power(t2,3) - 
                  2*Power(t1,2)*(182 + 31*t2) - 
                  2*t1*(-56 + 565*t2 + 295*Power(t2,2))) - 
               Power(-1 + t1,3)*
                (7*Power(t1,5) + 
                  Power(t1,4)*(-163 + 61*t2 + 15*Power(t2,2)) + 
                  Power(t1,3)*
                   (670 + 51*t2 - 165*Power(t2,2) - 4*Power(t2,3)) + 
                  4*Power(t1,2)*
                   (-159 - 228*t2 + 105*Power(t2,2) + 38*Power(t2,3)) \
+ 2*(8 - 148*t2 - 5*Power(t2,2) + 57*Power(t2,3)) - 
                  2*t1*(-71 - 513*t2 + 145*Power(t2,2) + 
                     96*Power(t2,3))) + 
               Power(s2,6)*(1298 - 91*Power(t1,4) + 
                  Power(t1,3)*(452 - 156*t2) + 2625*t2 + 
                  27*Power(t2,2) - 1350*Power(t2,3) + 
                  350*Power(t2,4) + 
                  Power(t1,2)*(815 + 2451*t2 + 1017*Power(t2,2)) + 
                  t1*(-2499 - 5160*t2 - 1104*Power(t2,2) + 
                     790*Power(t2,3))) + 
               Power(s2,5)*(1969 - 4*Power(t1,5) + 3277*t2 + 
                  1376*Power(t2,2) - 1322*Power(t2,3) - 
                  245*Power(t2,4) + Power(t1,4)*(298 + 632*t2) - 
                  Power(t1,3)*(3763 + 5804*t2 + 1224*Power(t2,2)) + 
                  Power(t1,2)*
                   (8902 + 14201*t2 + 3364*Power(t2,2) - 
                     1372*Power(t2,3)) + 
                  t1*(-7377 - 12366*t2 - 3816*Power(t2,2) + 
                     3534*Power(t2,3) + 175*Power(t2,4))) + 
               s2*Power(-1 + t1,2)*
                (614 - 680*t2 - 1962*Power(t2,2) + 220*Power(t2,3) + 
                  430*Power(t2,4) + Power(t1,5)*(329 + 24*t2) + 
                  Power(t1,4)*
                   (-1777 - 1340*t2 + 250*Power(t2,2) + 
                     20*Power(t2,3)) + 
                  Power(t1,3)*
                   (1682 + 7195*t2 + 126*Power(t2,2) - 
                     690*Power(t2,3) - 20*Power(t2,4)) - 
                  5*t1*(353 - 921*t2 - 1163*Power(t2,2) + 
                     347*Power(t2,3) + 162*Power(t2,4)) + 
                  Power(t1,2)*
                   (869 - 9494*t2 - 4799*Power(t2,2) + 
                     2500*Power(t2,3) + 400*Power(t2,4))) + 
               Power(s2,4)*(1883 + 34*Power(t1,6) + 5728*t2 + 
                  2964*Power(t2,2) - 4368*Power(t2,3) - 
                  1745*Power(t2,4) + 476*Power(t2,5) - 
                  2*Power(t1,5)*(483 + 164*t2) + 
                  Power(t1,4)*(7577 + 4788*t2 - 318*Power(t2,2)) + 
                  Power(t1,3)*
                   (-20381 - 19548*t2 + 3665*Power(t2,2) + 
                     2526*Power(t2,3)) + 
                  Power(t1,2)*
                   (23354 + 32922*t2 - 3942*Power(t2,2) - 
                     12145*Power(t2,3) - 345*Power(t2,4)) - 
                  t1*(11510 + 23442*t2 + 2759*Power(t2,2) - 
                     14547*Power(t2,3) - 1740*Power(t2,4) + 
                     476*Power(t2,5))) + 
               Power(s2,2)*(-1 + t1)*
                (-1819 + 49*Power(t1,6) - 1983*t2 + 4671*Power(t2,2) + 
                  3021*Power(t2,3) - 970*Power(t2,4) - 
                  364*Power(t2,5) - 
                  Power(t1,5)*(142 + 1009*t2 + 25*Power(t2,2)) + 
                  Power(t1,4)*
                   (-2342 + 7928*t2 + 2427*Power(t2,2) - 
                     238*Power(t2,3)) + 
                  Power(t1,3)*
                   (9800 - 15201*t2 - 16230*Power(t2,2) + 
                     563*Power(t2,3) + 675*Power(t2,4)) + 
                  Power(t1,2)*
                   (-13845 + 8330*t2 + 29480*Power(t2,2) + 
                     3509*Power(t2,3) - 3055*Power(t2,4) - 
                     336*Power(t2,5)) + 
                  t1*(8292 + 2025*t2 - 20653*Power(t2,2) - 
                     6400*Power(t2,3) + 3140*Power(t2,4) + 
                     700*Power(t2,5))) + 
               Power(s2,3)*(2150 + Power(t1,6)*(351 - 56*t2) + 
                  6221*t2 - 1454*Power(t2,2) - 6823*Power(t2,3) - 
                  610*Power(t2,4) + 840*Power(t2,5) + 
                  Power(t1,5)*(-4528 + 600*t2 + 690*Power(t2,2)) + 
                  Power(t1,4)*
                   (19040 + 79*t2 - 9212*Power(t2,2) - 
                     1026*Power(t2,3)) + 
                  Power(t1,3)*
                   (-35102 - 12935*t2 + 30714*Power(t2,2) + 
                     9436*Power(t2,3) - 835*Power(t2,4)) + 
                  Power(t1,2)*
                   (31652 + 29152*t2 - 36426*Power(t2,2) - 
                     24587*Power(t2,3) + 2215*Power(t2,4) + 
                     812*Power(t2,5)) - 
                  t1*(13562 + 23081*t2 - 15778*Power(t2,2) - 
                     22860*Power(t2,3) + 700*Power(t2,4) + 
                     1652*Power(t2,5)))) + 
            Power(s1,3)*(Power(s2,10)*(-6 + 6*t1 - 20*t2) + 
               Power(s2,9)*(-9 - 15*Power(t1,2) + t1*(24 - 41*t2) + 
                  125*t2 + 200*Power(t2,2)) + 
               Power(s2,8)*(176 + 32*Power(t1,3) + 587*t2 - 
                  100*Power(t2,2) - 600*Power(t2,3) + 
                  Power(t1,2)*(47 + 127*t2) - 
                  5*t1*(51 + 166*t2 + 28*Power(t2,2))) + 
               Power(s2,7)*(370 - 24*Power(t1,4) - 672*t2 - 
                  2705*Power(t2,2) - 1100*Power(t2,3) + 
                  700*Power(t2,4) - Power(t1,3)*(257 + 335*t2) + 
                  Power(t1,2)*(1162 + 1461*t2 + 131*Power(t2,2)) + 
                  t1*(-1251 - 434*t2 + 2214*Power(t2,2) + 
                     820*Power(t2,3))) + 
               Power(-1 + t1,3)*
                (Power(t1,5)*(118 + 33*t2) + 
                  Power(t1,4)*
                   (-603 - 447*t2 + 40*Power(t2,2) + 13*Power(t2,3)) + 
                  t1*(-811 + 569*t2 + 1887*Power(t2,2) - 
                     459*Power(t2,3) - 138*Power(t2,4)) + 
                  Power(t1,3)*
                   (140 + 2262*t2 + 65*Power(t2,2) - 
                     141*Power(t2,3) - 6*Power(t2,4)) + 
                  4*(54 + 7*t2 - 131*Power(t2,2) - 7*Power(t2,3) + 
                     29*Power(t2,4)) + 
                  Power(t1,2)*
                   (906 - 2301*t2 - 1608*Power(t2,2) + 
                     575*Power(t2,3) + 98*Power(t2,4))) - 
               Power(s2,6)*(765 + 18*Power(t1,5) + 4911*t2 + 
                  5158*Power(t2,2) + 40*Power(t2,3) - 
                  1450*Power(t2,4) + 280*Power(t2,5) - 
                  Power(t1,4)*(736 + 475*t2) + 
                  Power(t1,3)*(2619 + 2081*t2 - 234*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (-1036 + 1343*t2 + 2311*Power(t2,2) + 
                     660*Power(t2,3)) + 
                  t1*(-594 - 9303*t2 - 10026*Power(t2,2) - 
                     1440*Power(t2,3) + 890*Power(t2,4))) + 
               Power(s2,5)*(-2908 + 19*Power(t1,6) - 7294*t2 - 
                  6374*Power(t2,2) - 1357*Power(t2,3) + 
                  1433*Power(t2,4) + 91*Power(t2,5) - 
                  2*Power(t1,5)*(389 + 70*t2) - 
                  2*Power(t1,4)*(-1377 + 222*t2 + 604*Power(t2,2)) + 
                  Power(t1,3)*
                   (730 + 12653*t2 + 10824*Power(t2,2) + 
                     1609*Power(t2,3)) + 
                  Power(t1,2)*
                   (-9439 - 31923*t2 - 26726*Power(t2,2) - 
                     4015*Power(t2,3) + 1383*Power(t2,4)) + 
                  t1*(9622 + 27048*t2 + 23604*Power(t2,2) + 
                     4163*Power(t2,3) - 3656*Power(t2,4) - 
                     35*Power(t2,5))) + 
               s2*Power(-1 + t1,2)*
                (-907 + Power(t1,6) - 2243*t2 + 1259*Power(t2,2) + 
                  2369*Power(t2,3) - 190*Power(t2,4) - 
                  328*Power(t2,5) + 
                  Power(t1,5)*(306 - 815*t2 - 107*Power(t2,2)) + 
                  Power(t1,4)*
                   (-4329 + 5645*t2 + 2064*Power(t2,2) - 
                     146*Power(t2,3) - 15*Power(t2,4)) + 
                  Power(t1,3)*
                   (11071 - 5210*t2 - 12768*Power(t2,2) + 
                     133*Power(t2,3) + 500*Power(t2,4) + 
                     10*Power(t2,5)) - 
                  Power(t1,2)*
                   (11414 + 3904*t2 - 17603*Power(t2,2) - 
                     5241*Power(t2,3) + 2320*Power(t2,4) + 
                     222*Power(t2,5)) + 
                  t1*(5255 + 6719*t2 - 8671*Power(t2,2) - 
                     6837*Power(t2,3) + 1710*Power(t2,4) + 
                     540*Power(t2,5))) + 
               Power(s2,4)*(-3134 + Power(t1,6)*(161 - 66*t2) - 
                  7250*t2 - 10692*Power(t2,2) - 3257*Power(t2,3) + 
                  3997*Power(t2,4) + 1177*Power(t2,5) - 
                  280*Power(t2,6) + 
                  Power(t1,5)*(868 + 3252*t2 + 810*Power(t2,2)) + 
                  Power(t1,4)*
                   (-12741 - 26703*t2 - 9437*Power(t2,2) + 
                     145*Power(t2,3)) + 
                  Power(t1,3)*
                   (34109 + 73404*t2 + 36801*Power(t2,2) - 
                     4318*Power(t2,3) - 2204*Power(t2,4)) + 
                  Power(t1,2)*
                   (-37465 - 85575*t2 - 61683*Power(t2,2) + 
                     5824*Power(t2,3) + 11070*Power(t2,4) + 
                     113*Power(t2,5)) + 
                  t1*(18202 + 42974*t2 + 43961*Power(t2,2) + 
                     2126*Power(t2,3) - 13423*Power(t2,4) - 
                     1010*Power(t2,5) + 280*Power(t2,6))) + 
               Power(s2,2)*(-1 + t1)*
                (1364 + 7083*t2 + 3428*Power(t2,2) - 
                  5531*Power(t2,3) - 2784*Power(t2,4) + 
                  718*Power(t2,5) + 224*Power(t2,6) + 
                  Power(t1,6)*(802 + 9*t2) + 
                  Power(t1,5)*
                   (-8703 - 282*t2 + 1415*Power(t2,2) + 
                     83*Power(t2,3)) + 
                  Power(t1,4)*
                   (30819 + 9885*t2 - 13300*Power(t2,2) - 
                     2596*Power(t2,3) + 122*Power(t2,4)) - 
                  Power(t1,3)*
                   (48207 + 38006*t2 - 27237*Power(t2,2) - 
                     18923*Power(t2,3) + 782*Power(t2,4) + 
                     397*Power(t2,5)) + 
                  Power(t1,2)*
                   (36319 + 53542*t2 - 15892*Power(t2,2) - 
                     35521*Power(t2,3) - 2471*Power(t2,4) + 
                     2191*Power(t2,5) + 168*Power(t2,6)) - 
                  t1*(12395 + 32203*t2 + 3068*Power(t2,2) - 
                     25082*Power(t2,3) - 5460*Power(t2,4) + 
                     2344*Power(t2,5) + 392*Power(t2,6))) + 
               Power(s2,3)*(-1855 + 73*Power(t1,7) - 8621*t2 - 
                  11234*Power(t2,2) + 1750*Power(t2,3) + 
                  6172*Power(t2,4) + 416*Power(t2,5) - 
                  504*Power(t2,6) - 
                  Power(t1,6)*(2636 + 1504*t2 + 27*Power(t2,2)) + 
                  Power(t1,5)*
                   (20595 + 16822*t2 - 67*Power(t2,2) - 
                     737*Power(t2,3)) + 
                  Power(t1,4)*
                   (-59591 - 69900*t2 - 1779*Power(t2,2) + 
                     10405*Power(t2,3) + 904*Power(t2,4)) + 
                  Power(t1,3)*
                   (82178 + 130312*t2 + 24826*Power(t2,2) - 
                     37259*Power(t2,3) - 7884*Power(t2,4) + 
                     589*Power(t2,5)) - 
                  Power(t1,2)*
                   (57066 + 119564*t2 + 53918*Power(t2,2) - 
                     45701*Power(t2,3) - 21458*Power(t2,4) + 
                     1847*Power(t2,5) + 448*Power(t2,6)) + 
                  t1*(18302 + 52451*t2 + 42239*Power(t2,2) - 
                     19980*Power(t2,3) - 20510*Power(t2,4) + 
                     786*Power(t2,5) + 952*Power(t2,6)))) + 
            Power(s1,2)*(6*Power(s2,10)*t2*(3 - 3*t1 + 5*t2) + 
               Power(s2,9)*(22 + 2*Power(t1,3) + 27*t2 - 
                  189*Power(t2,2) - 200*Power(t2,3) + 
                  9*Power(t1,2)*(2 + 5*t2) + 
                  t1*(-42 - 72*t2 + 63*Power(t2,2))) - 
               Power(s2,8)*(1 + 514*t2 + 854*Power(t2,2) - 
                  100*Power(t2,3) - 450*Power(t2,4) + 
                  Power(t1,3)*(83 + 109*t2) + 
                  Power(t1,2)*(-165 + 101*t2 + 182*Power(t2,2)) - 
                  t1*(-81 + 724*t2 + 1210*Power(t2,2) + 
                     140*Power(t2,3))) + 
               Power(s2,7)*(-372 - 6*Power(t1,5) - 1019*t2 + 
                  1019*Power(t2,2) + 2669*Power(t2,3) + 
                  850*Power(t2,4) - 420*Power(t2,5) + 
                  Power(t1,4)*(211 + 117*t2) + 
                  Power(t1,3)*(-198 + 575*t2 + 491*Power(t2,2)) - 
                  Power(t1,2)*
                   (585 + 3138*t2 + 2090*Power(t2,2) + 
                     135*Power(t2,3)) + 
                  t1*(950 + 3465*t2 + 550*Power(t2,2) - 
                     2174*Power(t2,3) - 640*Power(t2,4))) - 
               Power(-1 + t1,3)*
                (110 + 20*Power(t1,6) + 610*t2 + 8*Power(t2,2) - 
                  432*Power(t2,3) - 54*Power(t2,4) + 78*Power(t2,5) + 
                  Power(t1,5)*(-234 + 131*t2 + 45*Power(t2,2)) + 
                  Power(t1,4)*
                   (2080 - 1244*t2 - 363*Power(t2,2) + Power(t2,3) + 
                     4*Power(t2,4)) - 
                  Power(t1,3)*
                   (4133 + 82*t2 - 2805*Power(t2,2) + 
                     86*Power(t2,3) + 58*Power(t2,4) + 2*Power(t2,5)) \
+ Power(t1,2)*(3222 + 2843*t2 - 3097*Power(t2,2) - 1269*Power(t2,3) + 
                     445*Power(t2,4) + 28*Power(t2,5)) - 
                  t1*(1051 + 2360*t2 - 818*Power(t2,2) - 
                     1646*Power(t2,3) + 367*Power(t2,4) + 
                     62*Power(t2,5))) + 
               Power(s2,6)*(-404 + 4*Power(t1,6) + 
                  Power(t1,5)*(-187 + t2) + 2211*t2 + 
                  6975*Power(t2,2) + 5047*Power(t2,3) - 
                  2*Power(t2,4) - 930*Power(t2,5) + 140*Power(t2,6) - 
                  Power(t1,4)*(495 + 1920*t2 + 811*Power(t2,2)) + 
                  Power(t1,3)*
                   (3234 + 7250*t2 + 3357*Power(t2,2) - 
                     93*Power(t2,3)) + 
                  Power(t1,2)*
                   (-4659 - 5738*t2 + 3342*Power(t2,2) + 
                     4197*Power(t2,3) + 928*Power(t2,4)) + 
                  t1*(2507 - 1804*t2 - 13013*Power(t2,2) - 
                     9631*Power(t2,3) - 986*Power(t2,4) + 
                     594*Power(t2,5))) + 
               Power(s2,5)*(1405 + Power(t1,6)*(1 - 36*t2) + 7784*t2 + 
                  9930*Power(t2,2) + 5932*Power(t2,3) + 
                  560*Power(t2,4) - 903*Power(t2,5) + 7*Power(t2,6) + 
                  Power(t1,5)*(1963 + 2371*t2 + 418*Power(t2,2)) + 
                  Power(t1,4)*
                   (-7543 - 8950*t2 - 617*Power(t2,2) + 
                     977*Power(t2,3)) + 
                  Power(t1,3)*
                   (8950 + 596*t2 - 14660*Power(t2,2) - 
                     9355*Power(t2,3) - 1133*Power(t2,4)) + 
                  Power(t1,2)*
                   (-1723 + 23616*t2 + 41364*Power(t2,2) + 
                     23943*Power(t2,3) + 2446*Power(t2,4) - 
                     813*Power(t2,5)) - 
                  t1*(3053 + 25381*t2 + 36285*Power(t2,2) + 
                     21617*Power(t2,3) + 2173*Power(t2,4) - 
                     2220*Power(t2,5) + 35*Power(t2,6))) + 
               s2*Power(-1 + t1,2)*
                (129 + 2774*t2 + 2822*Power(t2,2) - 1006*Power(t2,3) - 
                  1615*Power(t2,4) + 96*Power(t2,5) + 
                  160*Power(t2,6) + Power(t1,6)*(639 + 134*t2) + 
                  Power(t1,5)*
                   (-7031 - 785*t2 + 534*Power(t2,2) + 
                     114*Power(t2,3)) + 
                  Power(t1,4)*
                   (21523 + 10942*t2 - 6606*Power(t2,2) - 
                     1210*Power(t2,3) + 18*Power(t2,4) + 4*Power(t2,5)\
) - Power(t1,3)*(28650 + 29217*t2 - 6361*Power(t2,2) - 
                     10645*Power(t2,3) + 480*Power(t2,4) + 
                     183*Power(t2,5) + 2*Power(t2,6)) + 
                  2*Power(t1,2)*
                   (8931 + 15602*t2 + 2688*Power(t2,2) - 
                     7785*Power(t2,3) - 1462*Power(t2,4) + 
                     642*Power(t2,5) + 33*Power(t2,6)) - 
                  t1*(4474 + 15001*t2 + 8775*Power(t2,2) - 
                     7647*Power(t2,3) - 4431*Power(t2,4) + 
                     1012*Power(t2,5) + 224*Power(t2,6))) + 
               Power(s2,4)*(3238 + 40*Power(t1,7) + 7954*t2 + 
                  9626*Power(t2,2) + 9307*Power(t2,3) + 
                  1927*Power(t2,4) - 2127*Power(t2,5) - 
                  479*Power(t2,6) + 104*Power(t2,7) - 
                  Power(t1,6)*(1893 + 859*t2 + 9*Power(t2,2)) + 
                  Power(t1,5)*
                   (6573 + 448*t2 - 3461*Power(t2,2) - 
                     815*Power(t2,3)) + 
                  Power(t1,4)*
                   (-2388 + 26974*t2 + 32385*Power(t2,2) + 
                     8427*Power(t2,3) + 123*Power(t2,4)) + 
                  Power(t1,3)*
                   (-16786 - 81823*t2 - 93265*Power(t2,2) - 
                     32310*Power(t2,3) + 2834*Power(t2,4) + 
                     1065*Power(t2,5)) + 
                  Power(t1,2)*
                   (26907 + 93358*t2 + 111258*Power(t2,2) + 
                     54526*Power(t2,3) - 4900*Power(t2,4) - 
                     5820*Power(t2,5) + 25*Power(t2,6)) - 
                  t1*(15691 + 46052*t2 + 56588*Power(t2,2) + 
                     38895*Power(t2,3) + 374*Power(t2,4) - 
                     7218*Power(t2,5) - 314*Power(t2,6) + 
                     104*Power(t2,7))) + 
               Power(s2,2)*(-1 + t1)*
                (-493 + 19*Power(t1,7) - 4200*t2 - 9336*Power(t2,2) - 
                  2956*Power(t2,3) + 3507*Power(t2,4) + 
                  1548*Power(t2,5) - 338*Power(t2,6) - 
                  88*Power(t2,7) - 
                  Power(t1,6)*(2957 + 1840*t2 + 244*Power(t2,2)) + 
                  Power(t1,5)*
                   (22219 + 20509*t2 + 1076*Power(t2,2) - 
                     732*Power(t2,3) - 69*Power(t2,4)) - 
                  Power(t1,4)*
                   (57963 + 75708*t2 + 13361*Power(t2,2) - 
                     10515*Power(t2,3) - 1241*Power(t2,4) + 
                     17*Power(t2,5)) + 
                  Power(t1,3)*
                   (70977 + 122097*t2 + 50458*Power(t2,2) - 
                     23788*Power(t2,3) - 11584*Power(t2,4) + 
                     657*Power(t2,5) + 127*Power(t2,6)) - 
                  Power(t1,2)*
                   (42714 + 94715*t2 + 71660*Power(t2,2) - 
                     14857*Power(t2,3) - 23098*Power(t2,4) - 
                     807*Power(t2,5) + 977*Power(t2,6) + 
                     48*Power(t2,7)) + 
                  t1*(10912 + 33860*t2 + 43025*Power(t2,2) + 
                     2284*Power(t2,3) - 16523*Power(t2,4) - 
                     2722*Power(t2,5) + 1104*Power(t2,6) + 
                     136*Power(t2,7))) + 
               Power(s2,3)*(2252 + Power(t1,7)*(491 - 14*t2) + 
                  4913*t2 + 11539*Power(t2,2) + 9583*Power(t2,3) - 
                  981*Power(t2,4) - 3300*Power(t2,5) - 
                  168*Power(t2,6) + 192*Power(t2,7) + 
                  Power(t1,6)*
                   (897 + 5117*t2 + 2070*Power(t2,2) + 126*Power(t2,3)) \
+ Power(t1,5)*(-19871 - 47368*t2 - 21262*Power(t2,2) - 
                     757*Power(t2,3) + 341*Power(t2,4)) + 
                  Power(t1,4)*
                   (61554 + 144192*t2 + 89504*Power(t2,2) + 
                     2249*Power(t2,3) - 6068*Power(t2,4) - 
                     413*Power(t2,5)) - 
                  Power(t1,3)*
                   (84210 + 203746*t2 + 169883*Power(t2,2) + 
                     21365*Power(t2,3) - 24488*Power(t2,4) - 
                     3595*Power(t2,5) + 255*Power(t2,6)) + 
                  Power(t1,2)*
                   (58063 + 143770*t2 + 157878*Power(t2,2) + 
                     46672*Power(t2,3) - 31247*Power(t2,4) - 
                     10745*Power(t2,5) + 965*Power(t2,6) + 
                     152*Power(t2,7)) - 
                  t1*(19176 + 46864*t2 + 69840*Power(t2,2) + 
                     36548*Power(t2,3) - 13557*Power(t2,4) - 
                     10779*Power(t2,5) + 514*Power(t2,6) + 
                     344*Power(t2,7)))) + 
            s1*(-2*Power(s2,10)*Power(t2,2)*(9 - 9*t1 + 10*t2) + 
               Power(s2,9)*t2*
                (-44 - 4*Power(t1,3) - 27*t2 + 127*Power(t2,2) + 
                  100*Power(t2,3) - 9*Power(t1,2)*(4 + 5*t2) + 
                  t1*(84 + 72*t2 - 43*Power(t2,2))) + 
               Power(s2,8)*(-26 + 4*Power(t1,4) + 2*t2 + 
                  498*Power(t2,2) + 547*Power(t2,3) - 50*Power(t2,4) - 
                  180*Power(t2,5) + 
                  2*Power(t1,3)*(7 + 83*t2 + 62*Power(t2,2)) + 
                  Power(t1,2)*
                   (-66 - 330*t2 + 55*Power(t2,2) + 111*Power(t2,3)) + 
                  t1*(74 + 162*t2 - 677*Power(t2,2) - 774*Power(t2,3) - 
                     70*Power(t2,4))) + 
               Power(s2,7)*(9 + 713*t2 + 12*Power(t1,5)*t2 + 
                  910*Power(t2,2) - 699*Power(t2,3) - 
                  1321*Power(t2,4) - 350*Power(t2,5) + 
                  140*Power(t2,6) - 
                  Power(t1,4)*(121 + 452*t2 + 174*Power(t2,2)) + 
                  Power(t1,3)*
                   (354 + 517*t2 - 325*Power(t2,2) - 288*Power(t2,3)) + 
                  Power(t1,2)*
                   (-336 + 987*t2 + 2700*Power(t2,2) + 
                     1250*Power(t2,3) + 65*Power(t2,4)) + 
                  t1*(94 - 1777*t2 - 3111*Power(t2,2) - 
                     243*Power(t2,3) + 1076*Power(t2,4) + 
                     266*Power(t2,5))) + 
               Power(s2,6)*(340 + 665*t2 - 2161*Power(t2,2) - 
                  4428*Power(t2,3) - 2465*Power(t2,4) + 
                  30*Power(t2,5) + 330*Power(t2,6) - 40*Power(t2,7) - 
                  4*Power(t1,6)*(3 + 2*t2) + 
                  Power(t1,5)*(340 + 488*t2 + 70*Power(t2,2)) + 
                  Power(t1,4)*
                   (-733 + 431*t2 + 1491*Power(t2,2) + 548*Power(t2,3)) \
- Power(t1,3)*(61 + 5332*t2 + 6294*Power(t2,2) + 2221*Power(t2,3) + 
                     42*Power(t2,4)) + 
                  Power(t1,2)*
                   (1361 + 8124*t2 + 4879*Power(t2,2) - 
                     1982*Power(t2,3) - 1847*Power(t2,4) - 
                     330*Power(t2,5)) + 
                  t1*(-1235 - 4368*t2 + 2015*Power(t2,2) + 
                     8183*Power(t2,3) + 4594*Power(t2,4) + 
                     324*Power(t2,5) - 218*Power(t2,6))) + 
               Power(-1 + t1,3)*
                (-94 + 266*t2 + 452*Power(t2,2) + 4*Power(t2,3) - 
                  166*Power(t2,4) - 46*Power(t2,5) + 32*Power(t2,6) + 
                  8*Power(t1,6)*(20 + 7*t2) + 
                  Power(t1,5)*
                   (-2030 - 97*t2 - 12*Power(t2,2) + 19*Power(t2,3)) + 
                  Power(t1,4)*
                   (5321 + 2530*t2 - 1007*Power(t2,2) - 
                     40*Power(t2,3) - 4*Power(t2,4)) - 
                  Power(t1,3)*
                   (5795 + 5712*t2 - 39*Power(t2,2) - 
                     1505*Power(t2,3) + 148*Power(t2,4) + 9*Power(t2,5)\
) + Power(t1,2)*(2791 + 4716*t2 + 2266*Power(t2,2) - 
                     1820*Power(t2,3) - 447*Power(t2,4) + 
                     180*Power(t2,5) + 2*Power(t2,6)) - 
                  t1*(355 + 1731*t2 + 1840*Power(t2,2) - 
                     476*Power(t2,3) - 695*Power(t2,4) + 
                     137*Power(t2,5) + 20*Power(t2,6))) + 
               Power(s2,5)*(198 + 8*Power(t1,7) - 2654*t2 - 
                  6873*Power(t2,2) - 5891*Power(t2,3) - 
                  2610*Power(t2,4) + 7*Power(t2,5) + 308*Power(t2,6) - 
                  17*Power(t2,7) + 
                  Power(t1,6)*(-322 - 144*t2 + 7*Power(t2,2)) - 
                  Power(t1,5)*
                   (77 + 3080*t2 + 2271*Power(t2,2) + 377*Power(t2,3)) \
+ Power(t1,4)*(3582 + 13074*t2 + 9231*Power(t2,2) + 1229*Power(t2,3) - 
                     298*Power(t2,4)) + 
                  Power(t1,3)*
                   (-7382 - 15676*t2 - 2874*Power(t2,2) + 
                     6747*Power(t2,3) + 3629*Power(t2,4) + 
                     387*Power(t2,5)) + 
                  Power(t1,2)*
                   (6086 + 2516*t2 - 19247*Power(t2,2) - 
                     22929*Power(t2,3) - 10042*Power(t2,4) - 
                     631*Power(t2,5) + 258*Power(t2,6)) + 
                  t1*(-2093 + 5964*t2 + 22027*Power(t2,2) + 
                     21121*Power(t2,3) + 9381*Power(t2,4) + 
                     357*Power(t2,5) - 734*Power(t2,6) + 25*Power(t2,7))\
) - s2*Power(-1 + t1,2)*(-171 + 56*Power(t1,7) + 363*t2 + 
                  2180*Power(t2,2) + 1394*Power(t2,3) - 
                  325*Power(t2,4) - 603*Power(t2,5) + 28*Power(t2,6) + 
                  46*Power(t2,7) + 
                  Power(t1,6)*(1497 + 544*t2 + 207*Power(t2,2)) + 
                  Power(t1,5)*
                   (-10445 - 8885*t2 - 197*Power(t2,2) - 
                     15*Power(t2,3) + 38*Power(t2,4)) + 
                  Power(t1,4)*
                   (24254 + 29535*t2 + 7246*Power(t2,2) - 
                     3567*Power(t2,3) - 108*Power(t2,4) - 8*Power(t2,5)\
) - Power(t1,3)*(26206 + 40500*t2 + 21713*Power(t2,2) - 
                     4067*Power(t2,3) - 4161*Power(t2,4) + 
                     399*Power(t2,5) + 26*Power(t2,6)) + 
                  Power(t1,2)*
                   (13404 + 25732*t2 + 23956*Power(t2,2) + 
                     2407*Power(t2,3) - 6637*Power(t2,4) - 
                     743*Power(t2,5) + 401*Power(t2,6) + 8*Power(t2,7)) \
- t1*(2389 + 6793*t2 + 11628*Power(t2,2) + 4478*Power(t2,3) - 
                     3181*Power(t2,4) - 1525*Power(t2,5) + 
                     340*Power(t2,6) + 54*Power(t2,7))) - 
               Power(s2,4)*(1141 + 5367*t2 + 6276*Power(t2,2) + 
                  5040*Power(t2,3) + 3630*Power(t2,4) + 
                  552*Power(t2,5) - 605*Power(t2,6) - 105*Power(t2,7) + 
                  22*Power(t2,8) + Power(t1,7)*(-74 + 22*t2) - 
                  Power(t1,6)*
                   (1873 + 3522*t2 + 1186*Power(t2,2) + 69*Power(t2,3)) \
+ Power(t1,5)*(8974 + 13429*t2 + 3515*Power(t2,2) - 1111*Power(t2,3) - 
                     332*Power(t2,4)) + 
                  Power(t1,4)*
                   (-14373 - 8763*t2 + 15055*Power(t2,2) + 
                     15179*Power(t2,3) + 3195*Power(t2,4) + 
                     129*Power(t2,5)) + 
                  Power(t1,3)*
                   (7974 - 23970*t2 - 59659*Power(t2,2) - 
                     48102*Power(t2,3) - 12579*Power(t2,4) + 
                     1067*Power(t2,5) + 246*Power(t2,6)) + 
                  Power(t1,2)*
                   (2225 + 43358*t2 + 72476*Power(t2,2) + 
                     59372*Power(t2,3) + 21921*Power(t2,4) - 
                     2337*Power(t2,5) - 1606*Power(t2,6) + 
                     31*Power(t2,7)) - 
                  t1*(3994 + 25921*t2 + 36477*Power(t2,2) + 
                     30345*Power(t2,3) + 15715*Power(t2,4) - 
                     433*Power(t2,5) - 2077*Power(t2,6) - 
                     34*Power(t2,7) + 22*Power(t2,8))) + 
               Power(s2,2)*(-623 - 566*t2 - 3126*Power(t2,2) - 
                  4706*Power(t2,3) - 1245*Power(t2,4) + 
                  1122*Power(t2,5) + 482*Power(t2,6) - 94*Power(t2,7) - 
                  20*Power(t2,8) + Power(t1,8)*(532 + 173*t2) + 
                  Power(t1,7)*
                   (67 + 2593*t2 + 1048*Power(t2,2) + 238*Power(t2,3)) \
+ Power(t1,6)*(-14021 - 31905*t2 - 14659*Power(t2,2) - 
                     724*Power(t2,3) + 30*Power(t2,4) + 19*Power(t2,5)) \
+ Power(t1,5)*(50659 + 110586*t2 + 68472*Power(t2,2) + 
                     6181*Power(t2,3) - 4017*Power(t2,4) - 
                     157*Power(t2,5) - 4*Power(t2,6)) - 
                  Power(t1,4)*
                   (83080 + 183025*t2 + 146487*Power(t2,2) + 
                     30822*Power(t2,3) - 14481*Power(t2,4) - 
                     3561*Power(t2,5) + 310*Power(t2,6) + 
                     17*Power(t2,7)) + 
                  Power(t1,3)*
                   (73942 + 162549*t2 + 163255*Power(t2,2) + 
                     62613*Power(t2,3) - 17707*Power(t2,4) - 
                     11069*Power(t2,5) + 301*Power(t2,6) + 
                     268*Power(t2,7) + 6*Power(t2,8)) + 
                  2*t1*(4224 + 7886*t2 + 14278*Power(t2,2) + 
                     13595*Power(t2,3) + 952*Power(t2,4) - 
                     3359*Power(t2,5) - 602*Power(t2,6) + 
                     199*Power(t2,7) + 23*Power(t2,8)) - 
                  Power(t1,2)*
                   (35924 + 76177*t2 + 97059*Power(t2,2) + 
                     59970*Power(t2,3) - 6554*Power(t2,4) - 
                     13242*Power(t2,5) - 735*Power(t2,6) + 
                     555*Power(t2,7) + 32*Power(t2,8))) + 
               Power(s2,3)*(-1734 + 15*Power(t1,8) - 3184*t2 - 
                  3365*Power(t2,2) - 5794*Power(t2,3) - 
                  3718*Power(t2,4) + 206*Power(t2,5) + 965*Power(t2,6) + 
                  34*Power(t2,7) - 42*Power(t2,8) - 
                  Power(t1,7)*(1807 + 1341*t2 + 186*Power(t2,2)) + 
                  Power(t1,6)*
                   (6963 + 2161*t2 - 2013*Power(t2,2) - 
                     996*Power(t2,3) - 87*Power(t2,4)) + 
                  Power(t1,5)*
                   (-5584 + 22577*t2 + 31067*Power(t2,2) + 
                     9896*Power(t2,3) + 515*Power(t2,4) - 33*Power(t2,5)\
) + Power(t1,4)*(-13285 - 85591*t2 - 104361*Power(t2,2) - 
                     44712*Power(t2,3) - 317*Power(t2,4) + 
                     1673*Power(t2,5) + 70*Power(t2,6)) + 
                  Power(t1,3)*
                   (32253 + 123374*t2 + 152411*Power(t2,2) + 
                     88233*Power(t2,3) + 6987*Power(t2,4) - 
                     8307*Power(t2,5) - 738*Power(t2,6) + 63*Power(t2,7)\
) - Power(t1,2)*(28127 + 86226*t2 + 108161*Power(t2,2) + 
                     82993*Power(t2,3) + 17688*Power(t2,4) - 
                     11127*Power(t2,5) - 2781*Power(t2,6) + 
                     293*Power(t2,7) + 28*Power(t2,8)) + 
                  2*t1*(5653 + 14115*t2 + 17304*Power(t2,2) + 
                     18181*Power(t2,3) + 7164*Power(t2,4) - 
                     2351*Power(t2,5) - 1525*Power(t2,6) + 
                     94*Power(t2,7) + 35*Power(t2,8))))) - 
         Power(s,3)*(2*Power(s1,9)*Power(s2,2)*(-2 + s2 + 3*t1) + 
            2*Power(s2,10)*(-3 + 3*t1 - 2*t2)*t2 + 
            Power(s1,8)*s2*(29*Power(s2,3) + 
               16*(2 - 5*t1 + 3*Power(t1,2)) + 
               Power(s2,2)*(-34 + 35*t1 - 12*t2) - 
               2*s2*(27 - 57*t1 + 32*Power(t1,2) - 20*t2 + 27*t1*t2)) + 
            Power(s2,9)*(-6 - 2*Power(t1,3) + 45*t2 + 98*Power(t2,2) + 
               34*Power(t2,3) + Power(t1,2)*(-2 + 11*t2) + 
               t1*(10 - 56*t2 - 77*Power(t2,2))) - 
            Power(s2,8)*(-85 + Power(t1,4) + Power(t1,3)*(38 - 7*t2) + 
               2*t2 + 642*Power(t2,2) + 573*Power(t2,3) + 
               55*Power(t2,4) + 
               Power(t1,2)*(-164 - 91*t2 + 346*Power(t2,2)) + 
               t1*(210 + 96*t2 - 922*Power(t2,2) - 539*Power(t2,3))) + 
            Power(s2,7)*(-122 - 25*Power(t1,5) - 1203*t2 - 
               857*Power(t2,2) + 1661*Power(t2,3) + 1461*Power(t2,4) + 
               21*Power(t2,5) + Power(t1,4)*(369 + 62*t2) + 
               Power(t1,3)*(-957 + 851*t2 + 1297*Power(t2,2)) + 
               Power(t1,2)*(785 - 3376*t2 - 4301*Power(t2,2) + 
                  279*Power(t2,3)) + 
               t1*(-50 + 3666*t2 + 4001*Power(t2,2) - 
                  1940*Power(t2,3) - 1834*Power(t2,4))) + 
            Power(s2,6)*(-877 + 47*Power(t1,6) + 780*t2 + 
               5173*Power(t2,2) + 5575*Power(t2,3) + 92*Power(t2,4) - 
               1513*Power(t2,5) - 3*Power(t2,6) + 
               Power(t1,5)*(-664 + 41*t2) - 
               Power(t1,4)*(29 + 4816*t2 + 2608*Power(t2,2)) + 
               Power(t1,3)*(4766 + 14864*t2 + 5067*Power(t2,2) - 
                  2849*Power(t2,3)) + 
               Power(t1,2)*(-7901 - 14010*t2 + 4672*Power(t2,2) + 
                  15175*Power(t2,3) + 2970*Power(t2,4)) + 
               t1*(4658 + 3141*t2 - 12451*Power(t2,2) - 
                  17847*Power(t2,3) - 2749*Power(t2,4) + 
                  2352*Power(t2,5))) - 
            Power(s2,5)*(-1072 + 19*Power(t1,7) - 4393*t2 - 
               6205*Power(t2,2) + 484*Power(t2,3) + 6073*Power(t2,4) + 
               2076*Power(t2,5) - 383*Power(t2,6) - 21*Power(t2,7) + 
               35*Power(t1,6)*(-4 + 5*t2) - 
               Power(t1,5)*(6240 + 8209*t2 + 2166*Power(t2,2)) + 
               Power(t1,4)*(22486 + 16679*t2 - 7080*Power(t2,2) - 
                  5646*Power(t2,3)) + 
               Power(t1,3)*(-27269 + 7730*t2 + 53971*Power(t2,2) + 
                  29713*Power(t2,3) + 1496*Power(t2,4)) + 
               Power(t1,2)*(11554 - 36911*t2 - 81603*Power(t2,2) - 
                  38416*Power(t2,3) + 8253*Power(t2,4) + 
                  4639*Power(t2,5)) + 
               t1*(662 + 24929*t2 + 43090*Power(t2,2) + 
                  14290*Power(t2,3) - 15575*Power(t2,4) - 
                  7150*Power(t2,5) + 1126*Power(t2,6))) + 
            Power(s2,4)*(1959 - 171*t2 - 535*Power(t2,2) - 
               8361*Power(t2,3) - 8059*Power(t2,4) - 1069*Power(t2,5) + 
               583*Power(t2,6) + 216*Power(t2,7) - 18*Power(t2,8) + 
               Power(t1,7)*(232 + 66*t2) - 
               Power(t1,6)*(8487 + 4628*t2 + 585*Power(t2,2)) - 
               Power(t1,5)*(-18187 + 14312*t2 + 19654*Power(t2,2) + 
                  4336*Power(t2,3)) + 
               Power(t1,4)*(8261 + 105665*t2 + 97443*Power(t2,2) + 
                  18012*Power(t2,3) - 1540*Power(t2,4)) + 
               Power(t1,3)*(-53892 - 175325*t2 - 148216*Power(t2,2) + 
                  2848*Power(t2,3) + 26789*Power(t2,4) + 
                  3991*Power(t2,5)) + 
               Power(t1,2)*(53255 + 114471*t2 + 88117*Power(t2,2) - 
                  47014*Power(t2,3) - 59623*Power(t2,4) - 
                  7006*Power(t2,5) + 1975*Power(t2,6)) + 
               t1*(-19515 - 25766*t2 - 16376*Power(t2,2) + 
                  39369*Power(t2,3) + 42767*Power(t2,4) + 
                  4106*Power(t2,5) - 3181*Power(t2,6) + 111*Power(t2,7))\
) - (-1 + t1)*(-2366 - 4963*t2 + 2784*Power(t2,2) + 4768*Power(t2,3) + 
               1120*Power(t2,4) + 579*Power(t2,5) - 1054*Power(t2,6) + 
               180*Power(t2,7) + 11*Power(t1,8)*(14 + 3*t2) + 
               Power(t1,7)*(3457 + 919*t2 + 203*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(t1,6)*(-14046 - 7281*t2 + 270*Power(t2,2) + 
                  30*Power(t2,3) - 11*Power(t2,4)) + 
               Power(t1,5)*(17512 + 11049*t2 - 9649*Power(t2,2) - 
                  2845*Power(t2,3) + 192*Power(t2,4) - 29*Power(t2,5)) \
+ Power(t1,4)*(-3775 - 8814*t2 + 39642*Power(t2,2) + 
                  17965*Power(t2,3) - 1489*Power(t2,4) - 
                  395*Power(t2,5) + 39*Power(t2,6)) + 
               t1*(6145 + 19321*t2 - 14257*Power(t2,2) - 
                  26036*Power(t2,3) - 5451*Power(t2,4) + 
                  2629*Power(t2,5) + 1385*Power(t2,6) - 280*Power(t2,7)\
) - Power(t1,3)*(5590 - 17739*t2 + 59113*Power(t2,2) + 
                  47298*Power(t2,3) - 1913*Power(t2,4) - 
                  3130*Power(t2,5) + 183*Power(t2,6) + 12*Power(t2,7)) \
+ Power(t1,2)*(-1491 - 28011*t2 + 40176*Power(t2,2) + 
                  53267*Power(t2,3) + 3870*Power(t2,4) - 
                  5970*Power(t2,5) - 195*Power(t2,6) + 120*Power(t2,7))) \
+ Power(s2,3)*(-1064 - 73*Power(t1,8) + 2695*t2 - 3848*Power(t2,2) - 
               10730*Power(t2,3) - 3240*Power(t2,4) + 863*Power(t2,5) + 
               1902*Power(t2,6) + 180*Power(t2,7) - 98*Power(t2,8) + 
               4*Power(t2,9) + 
               Power(t1,7)*(3197 + 704*t2 + 27*Power(t2,2)) + 
               Power(t1,6)*(14111 + 28415*t2 + 11796*Power(t2,2) + 
                  1165*Power(t2,3)) + 
               Power(t1,5)*(-95137 - 126416*t2 - 43503*Power(t2,2) + 
                  4252*Power(t2,3) + 1652*Power(t2,4)) + 
               Power(t1,4)*(175910 + 184078*t2 + 6981*Power(t2,2) - 
                  73311*Power(t2,3) - 20667*Power(t2,4) - 
                  1313*Power(t2,5)) + 
               Power(t1,3)*(-143464 - 99757*t2 + 109052*Power(t2,2) + 
                  190136*Power(t2,3) + 54698*Power(t2,4) - 
                  3381*Power(t2,5) - 1278*Power(t2,6)) + 
               Power(t1,2)*(49456 + 8623*t2 - 130933*Power(t2,2) - 
                  195836*Power(t2,3) - 59898*Power(t2,4) + 
                  15935*Power(t2,5) + 3823*Power(t2,6) - 
                  196*Power(t2,7)) + 
               t1*(-2936 + 1658*t2 + 50228*Power(t2,2) + 
                  84294*Power(t2,3) + 28209*Power(t2,4) - 
                  12496*Power(t2,5) - 4612*Power(t2,6) + 
                  252*Power(t2,7) + 35*Power(t2,8))) - 
            s2*(2 + Power(t1,9) + 7593*t2 + 3573*Power(t2,2) - 
               7611*Power(t2,3) - 6906*Power(t2,4) - 3607*Power(t2,5) + 
               1587*Power(t2,6) + 329*Power(t2,7) - 80*Power(t2,8) - 
               Power(t1,8)*(3646 + 1089*t2 + 107*Power(t2,2)) - 
               Power(t1,7)*(2276 + 6816*t2 + 3919*Power(t2,2) + 
                  538*Power(t2,3) + 15*Power(t2,4)) + 
               Power(t1,6)*(70156 + 75216*t2 + 31753*Power(t2,2) + 
                  2491*Power(t2,3) + 31*Power(t2,4) + 21*Power(t2,5)) + 
               Power(t1,5)*(-191132 - 211071*t2 - 87585*Power(t2,2) + 
                  10444*Power(t2,3) + 5155*Power(t2,4) + 
                  183*Power(t2,5) + 11*Power(t2,6)) + 
               Power(t1,4)*(234164 + 286955*t2 + 125259*Power(t2,2) - 
                  66495*Power(t2,3) - 33832*Power(t2,4) + 
                  968*Power(t2,5) + 392*Power(t2,6) - 25*Power(t2,7)) + 
               Power(t1,2)*(43908 + 114973*t2 + 67030*Power(t2,2) - 
                  99237*Power(t2,3) - 93673*Power(t2,4) - 
                  1842*Power(t2,5) + 7021*Power(t2,6) - 
                  8*Power(t2,7) - 72*Power(t2,8)) + 
               Power(t1,3)*(-146740 - 225684*t2 - 111908*Power(t2,2) + 
                  119995*Power(t2,3) + 84331*Power(t2,4) - 
                  3276*Power(t2,5) - 3071*Power(t2,6) + 
                  111*Power(t2,7) + 8*Power(t2,8)) + 
               t1*(-4437 - 40077*t2 - 24094*Power(t2,2) + 
                  40934*Power(t2,3) + 44957*Power(t2,4) + 
                  7491*Power(t2,5) - 5902*Power(t2,6) - 
                  416*Power(t2,7) + 144*Power(t2,8))) + 
            Power(s2,2)*(811 + 3366*t2 - 8825*Power(t2,2) - 
               4026*Power(t2,3) + 6268*Power(t2,4) + 4464*Power(t2,5) + 
               1311*Power(t2,6) - 652*Power(t2,7) - 21*Power(t2,8) + 
               8*Power(t2,9) - Power(t1,8)*(221 + 9*t2) - 
               Power(t1,7)*(17201 + 10682*t2 + 2153*Power(t2,2) + 
                  83*Power(t2,3)) + 
               Power(t1,6)*(66074 + 26921*t2 - 6630*Power(t2,2) - 
                  4510*Power(t2,3) - 338*Power(t2,4)) + 
               Power(t1,5)*(-72882 + 45662*t2 + 108309*Power(t2,2) + 
                  40088*Power(t2,3) + 3418*Power(t2,4) + 
                  138*Power(t2,5)) + 
               Power(t1,4)*(-17082 - 208489*t2 - 309394*Power(t2,2) - 
                  112349*Power(t2,3) + 3709*Power(t2,4) + 
                  3702*Power(t2,5) + 236*Power(t2,6)) + 
               Power(t1,3)*(89361 + 244608*t2 + 393070*Power(t2,2) + 
                  152444*Power(t2,3) - 40131*Power(t2,4) - 
                  18728*Power(t2,5) + 351*Power(t2,6) + 47*Power(t2,7)) \
+ Power(t1,2)*(-60754 - 113043*t2 - 252456*Power(t2,2) - 
                  112185*Power(t2,3) + 62102*Power(t2,4) + 
                  33195*Power(t2,5) - 1664*Power(t2,6) - 
                  565*Power(t2,7) + 6*Power(t2,8)) + 
               t1*(11894 + 11666*t2 + 78150*Power(t2,2) + 
                  40327*Power(t2,3) - 34626*Power(t2,4) - 
                  22978*Power(t2,5) - 218*Power(t2,6) + 
                  1197*Power(t2,7) - 2*Power(t2,8) - 6*Power(t2,9))) + 
            Power(s1,7)*(95*Power(s2,5) + 
               4*(-10 + 35*t1 - 40*Power(t1,2) + 13*Power(t1,3) + 
                  2*Power(t1,4)) + Power(s2,4)*(19 - 117*t1 - 189*t2) + 
               s2*(168 + 16*Power(t1,4) + Power(t1,2)*(412 - 368*t2) - 
                  264*t2 + Power(t1,3)*(-155 + 8*t2) + 
                  6*t1*(-75 + 104*t2)) + 
               Power(s2,3)*(-11*Power(t1,2) + t1*(69 - 252*t2) + 
                  4*(-47 + 78*t2 + 6*Power(t2,2))) + 
               Power(s2,2)*(382 - 163*Power(t1,3) + 399*t2 - 
                  176*Power(t2,2) + Power(t1,2)*(821 + 410*t2) + 
                  2*t1*(-535 - 382*t2 + 108*Power(t2,2)))) + 
            Power(s1,6)*(106*Power(s2,6) + 
               Power(s2,5)*(435 - 881*t1 - 574*t2) - 
               2*(-1 + t1)*(-62 + 37*Power(t1,4) + 140*t2 - 
                  30*t1*(-4 + 11*t2) - Power(t1,3)*(19 + 12*t2) + 
                  10*Power(t1,2)*(-8 + 23*t2)) + 
               Power(s2,4)*(259 + 1128*Power(t1,2) - 167*t2 + 
                  503*Power(t2,2) + t1*(-1897 + 1080*t2)) + 
               Power(s2,3)*(1871 - 1428*Power(t1,3) + 
                  Power(t1,2)*(5162 - 682*t2) + 846*t2 - 
                  1296*Power(t2,2) + 
                  t1*(-5857 + 875*t2 + 852*Power(t2,2))) - 
               s2*(98*Power(t1,5) + Power(t1,4)*(239 + 86*t2) + 
                  Power(t1,2)*(7051 + 1834*t2 - 1152*Power(t2,2)) + 
                  14*(95 + 78*t2 - 68*Power(t2,2)) + 
                  Power(t1,3)*(-3311 - 649*t2 + 16*Power(t2,2)) + 
                  t1*(-5369 - 2426*t2 + 2088*Power(t2,2))) + 
               Power(s2,2)*(910 + 535*Power(t1,4) - 2838*t2 - 
                  1361*Power(t2,2) + 448*Power(t2,3) + 
                  Power(t1,3)*(-1919 + 1323*t2) + 
                  Power(t1,2)*(2672 - 6125*t2 - 1190*Power(t2,2)) + 
                  t1*(-2250 + 7847*t2 + 2348*Power(t2,2) - 
                     504*Power(t2,3)))) + 
            Power(s1,5)*(10*Power(s2,7) + 
               Power(s2,6)*(1057 - 1724*t1 - 558*t2) + 
               Power(s2,5)*(554 + 2565*Power(t1,2) - 3052*t2 + 
                  1422*Power(t2,2) + t1*(-3478 + 5972*t2)) + 
               (-1 + t1)*(-1200 + 168*Power(t1,5) - 669*t2 + 
                  840*Power(t2,2) + Power(t1,4)*(-532 + 44*t2) + 
                  t1*(4852 + 635*t2 - 1660*Power(t2,2)) + 
                  Power(t1,3)*(2338 + 417*t2 - 112*Power(t2,2)) + 
                  Power(t1,2)*(-5682 - 475*t2 + 1100*Power(t2,2))) - 
               Power(s2,4)*(-985 + 2861*Power(t1,3) + 3252*t2 - 
                  288*Power(t2,2) + 677*Power(t2,3) + 
                  Power(t1,2)*(-5092 + 8636*t2) + 
                  2*t1*(1770 - 7573*t2 + 1866*Power(t2,2))) + 
               s2*(-2484 + 126*Power(t1,6) + 8172*t2 + 
                  3394*Power(t2,2) - 2000*Power(t2,3) + 
                  9*Power(t1,5)*(-89 + 24*t2) + 
                  Power(t1,4)*(1183 + 3462*t2 + 190*Power(t2,2)) + 
                  Power(t1,2)*
                   (-8169 + 46046*t2 + 3432*Power(t2,2) - 
                     1992*Power(t2,3)) - 
                  Power(t1,3)*
                   (-1956 + 23786*t2 + 1009*Power(t2,2) + 
                     32*Power(t2,3)) + 
                  t1*(8127 - 33882*t2 - 6196*Power(t2,2) + 
                     4024*Power(t2,3))) - 
               Power(s2,2)*(7769 + 307*Power(t1,5) + 3918*t2 - 
                  8932*Power(t2,2) - 2731*Power(t2,3) + 
                  728*Power(t2,4) + Power(t1,4)*(4647 + 2396*t2) + 
                  Power(t1,3)*(-23988 - 7294*t2 + 4277*Power(t2,2)) + 
                  Power(t1,2)*
                   (42381 + 7570*t2 - 18815*Power(t2,2) - 
                     2034*Power(t2,3)) + 
                  t1*(-31207 - 6834*t2 + 24082*Power(t2,2) + 
                     4268*Power(t2,3) - 756*Power(t2,4))) + 
               Power(s2,3)*(1143*Power(t1,4) + 
                  Power(t1,3)*(3323 + 8758*t2) + 
                  Power(t1,2)*(-13293 - 31314*t2 + 3761*Power(t2,2)) - 
                  2*t1*(-5429 - 17644*t2 + 2727*Power(t2,2) + 
                     870*Power(t2,3)) - 
                  4*(481 + 2817*t2 + 447*Power(t2,2) - 
                     772*Power(t2,3) + 21*Power(t2,4)))) + 
            Power(s1,4)*(-55*Power(s2,8) + 
               Power(s2,7)*(1175 - 1475*t1 - 22*t2) + 
               Power(s2,6)*(-705 + 1820*Power(t1,2) - 5815*t2 + 
                  1177*Power(t2,2) + t1*(-857 + 9278*t2)) - 
               Power(s2,5)*(4089 + 1654*Power(t1,3) + 4240*t2 - 
                  8385*Power(t2,2) + 1827*Power(t2,3) + 
                  Power(t1,2)*(6559 + 14277*t2) + 
                  t1*(-11983 - 20490*t2 + 16368*Power(t2,2))) - 
               (-1 + t1)*(1927 + 115*Power(t1,6) - 6020*t2 - 
                  1937*Power(t2,2) + 1540*Power(t2,3) + 
                  Power(t1,5)*(369 + 435*t2) - 
                  Power(t1,4)*(3895 + 1710*t2 + 357*Power(t2,2)) + 
                  Power(t1,3)*
                   (6782 + 12310*t2 + 808*Power(t2,2) - 
                     60*Power(t2,3)) + 
                  Power(t1,2)*
                   (39 - 31760*t2 + 1668*Power(t2,2) + 
                     1120*Power(t2,3)) - 
                  t1*(5193 - 26465*t2 + 302*Power(t2,2) + 
                     2320*Power(t2,3))) + 
               Power(s2,4)*(-4796 + 285*Power(t1,4) - 2059*t2 + 
                  11527*Power(t2,2) + 354*Power(t2,3) + 
                  425*Power(t2,4) + 7*Power(t1,3)*(2845 + 1773*t2) + 
                  Power(t1,2)*
                   (-45247 - 20330*t2 + 26065*Power(t2,2)) + 
                  t1*(30069 + 11440*t2 - 46167*Power(t2,2) + 
                     6579*Power(t2,3))) + 
               s2*(12203 - 35*Power(t1,7) + 10102*t2 - 
                  19645*Power(t2,2) - 6345*Power(t2,3) + 
                  2720*Power(t2,4) - 5*Power(t1,6)*(-267 + 23*t2) + 
                  Power(t1,5)*(-12540 + 197*t2 - 195*Power(t2,2)) - 
                  5*Power(t1,4)*
                   (-10109 - 361*t2 + 2052*Power(t2,2) + 
                     39*Power(t2,3)) + 
                  t1*(-59944 - 31699*t2 + 83060*Power(t2,2) + 
                     9940*Power(t2,3) - 5040*Power(t2,4)) + 
                  Power(t1,3)*
                   (-105169 - 11308*t2 + 60805*Power(t2,2) + 
                     535*Power(t2,3) + 120*Power(t2,4)) + 
                  Power(t1,2)*
                   (113557 + 31328*t2 - 114335*Power(t2,2) - 
                     3620*Power(t2,3) + 2200*Power(t2,4))) + 
               Power(s2,3)*(-6162 - 365*Power(t1,5) + 16239*t2 + 
                  29096*Power(t2,2) + 2632*Power(t2,3) - 
                  4540*Power(t2,4) + 168*Power(t2,5) - 
                  7*Power(t1,4)*(1917 + 220*t2) + 
                  Power(t1,3)*(40228 - 31387*t2 - 23530*Power(t2,2)) + 
                  Power(t1,2)*
                   (-46156 + 92083*t2 + 81351*Power(t2,2) - 
                     8200*Power(t2,3)) + 
                  t1*(26519 - 76190*t2 - 90418*Power(t2,2) + 
                     11938*Power(t2,3) + 2290*Power(t2,4))) + 
               Power(s2,2)*(6313 + 515*Power(t1,6) + 39164*t2 + 
                  9078*Power(t2,2) - 15640*Power(t2,3) - 
                  3425*Power(t2,4) + 784*Power(t2,5) - 
                  5*Power(t1,5)*(561 + 211*t2) + 
                  Power(t1,4)*(23169 + 33040*t2 + 5555*Power(t2,2)) + 
                  Power(t1,3)*
                   (-67555 - 136236*t2 - 13675*Power(t2,2) + 
                     7185*Power(t2,3)) + 
                  Power(t1,2)*
                   (78915 + 226845*t2 + 7916*Power(t2,2) - 
                     30995*Power(t2,3) - 2190*Power(t2,4)) - 
                  t1*(38231 + 162329*t2 + 9314*Power(t2,2) - 
                     40455*Power(t2,3) - 4880*Power(t2,4) + 
                     756*Power(t2,5)))) + 
            Power(s1,3)*(-31*Power(s2,9) + 
               Power(s2,8)*(606 - 575*t1 + 227*t2) + 
               Power(s2,6)*(-3895 + 235*Power(t1,3) + 2909*t2 + 
                  12782*Power(t2,2) - 1240*Power(t2,3) - 
                  Power(t1,2)*(9193 + 6828*t2) + 
                  t1*(12766 + 2892*t2 - 20022*Power(t2,2))) + 
               Power(s2,7)*(195*Power(t1,2) + t1*(1263 + 6044*t2) - 
                  3*(487 + 1600*t2 + 3*Power(t2,2))) + 
               Power(s2,5)*(3207 - 596*Power(t1,4) + 20340*t2 + 
                  12190*Power(t2,2) - 11576*Power(t2,3) + 
                  1243*Power(t2,4) + Power(t1,3)*(15643 + 1668*t2) + 
                  4*Power(t1,2)*(-5030 + 9498*t2 + 8285*Power(t2,2)) + 
                  t1*(2362 - 58852*t2 - 49520*Power(t2,2) + 
                     23208*Power(t2,3))) + 
               (-1 + t1)*(7282 + 20*Power(t1,7) + 6158*t2 - 
                  10057*Power(t2,2) - 4091*Power(t2,3) + 
                  1960*Power(t2,4) + Power(t1,6)*(964 + 400*t2) + 
                  2*Power(t1,5)*(-3093 - 132*t2 + 92*Power(t2,2)) - 
                  2*Power(t1,4)*
                   (-12179 + 3950*t2 + 939*Power(t2,2) + 
                     278*Power(t2,3)) + 
                  Power(t1,3)*
                   (-57390 + 16158*t2 + 24947*Power(t2,2) - 
                     641*Power(t2,3) + 120*Power(t2,4)) + 
                  Power(t1,2)*
                   (65818 + 5546*t2 - 65793*Power(t2,2) + 
                     6059*Power(t2,3) + 580*Power(t2,4)) - 
                  t1*(35002 + 19522*t2 - 52037*Power(t2,2) + 
                     931*Power(t2,3) + 2380*Power(t2,4))) + 
               Power(s2,4)*(5745 - 636*Power(t1,5) + 24935*t2 + 
                  2058*Power(t2,2) - 18242*Power(t2,3) - 
                  1621*Power(t2,4) + Power(t2,5) + 
                  Power(t1,4)*(-1985 + 7424*t2) - 
                  4*Power(t1,3)*(7707 + 25838*t2 + 6571*Power(t2,2)) + 
                  Power(t1,2)*
                   (70326 + 212250*t2 + 42444*Power(t2,2) - 
                     39634*Power(t2,3)) - 
                  t1*(43195 + 142256*t2 + 20684*Power(t2,2) - 
                     69776*Power(t2,3) + 6501*Power(t2,4))) + 
               s2*(-6787 - 40*Power(t1,7) - 50138*t2 - 
                  19079*Power(t2,2) + 24790*Power(t2,3) + 
                  7400*Power(t2,4) - 2472*Power(t2,5) - 
                  4*Power(t1,6)*(1569 + 1228*t2 + 50*Power(t2,2)) + 
                  4*Power(t1,5)*
                   (13516 + 12597*t2 + 962*Power(t2,2) + 
                     70*Power(t2,3)) + 
                  Power(t1,4)*
                   (-168977 - 206942*t2 - 11735*Power(t2,2) + 
                     12790*Power(t2,3) + 40*Power(t2,4)) + 
                  Power(t1,3)*
                   (246239 + 435728*t2 + 20952*Power(t2,2) - 
                     75880*Power(t2,3) + 395*Power(t2,4) - 
                     120*Power(t2,5)) - 
                  2*Power(t1,2)*
                   (88578 + 236772*t2 + 24571*Power(t2,2) - 
                     72130*Power(t2,3) - 1210*Power(t2,4) + 
                     864*Power(t2,5)) + 
                  2*t1*(29458 + 124806*t2 + 27368*Power(t2,2) - 
                     52740*Power(t2,3) - 5285*Power(t2,4) + 
                     2160*Power(t2,5))) + 
               Power(s2,3)*(1100*Power(t1,6) - 
                  4*Power(t1,5)*(3398 + 1301*t2) + 
                  Power(t1,4)*(97959 + 72568*t2 + 1125*Power(t2,2)) + 
                  4*Power(t1,3)*
                   (-53896 - 46240*t2 + 18123*Power(t2,2) + 
                     8400*Power(t2,3)) + 
                  Power(t1,2)*
                   (195375 + 197924*t2 - 208550*Power(t2,2) - 
                     111612*Power(t2,3) + 9215*Power(t2,4)) - 
                  t1*(72907 + 100808*t2 - 175210*Power(t2,2) - 
                     122708*Power(t2,3) + 13127*Power(t2,4) + 
                     1940*Power(t2,5)) - 
                  2*(-3860 - 8898*t2 + 19071*Power(t2,2) + 
                     20146*Power(t2,3) + 1494*Power(t2,4) - 
                     2092*Power(t2,5) + 84*Power(t2,6))) + 
               Power(s2,2)*(8294 - 200*Power(t1,7) - 37655*t2 - 
                  75465*Power(t2,2) - 14137*Power(t2,3) + 
                  16630*Power(t2,4) + 2669*Power(t2,5) - 
                  560*Power(t2,6) + 2*Power(t1,6)*(2509 + 80*t2) + 
                  4*Power(t1,5)*(-9568 + 772*t2 + 983*Power(t2,2)) - 
                  Power(t1,4)*
                   (-87903 + 79180*t2 + 72675*Power(t2,2) + 
                     7875*Power(t2,3)) + 
                  Power(t1,3)*
                   (-88897 + 266668*t2 + 282948*Power(t2,2) + 
                     16100*Power(t2,3) - 6725*Power(t2,4)) + 
                  Power(t1,2)*
                   (53985 - 340334*t2 - 461494*Power(t2,2) - 
                     3384*Power(t2,3) + 29595*Power(t2,4) + 
                     1454*Power(t2,5)) + 
                  2*t1*(-13779 + 92944*t2 + 162064*Power(t2,2) + 
                     4828*Power(t2,3) - 20245*Power(t2,4) - 
                     1722*Power(t2,5) + 252*Power(t2,6)))) + 
            Power(s1,2)*(-4*Power(s2,10) + 
               Power(s2,9)*(116 - 95*t1 + 96*t2) - 
               2*Power(s2,8)*
                (281 + 77*Power(t1,2) + 871*t2 + 172*Power(t2,2) - 
                  t1*(325 + 823*t2)) + 
               Power(s2,7)*(-722 + 310*Power(t1,3) + 4783*t2 + 
                  7568*Power(t2,2) + 65*Power(t2,3) + 
                  Power(t1,2)*(-2488 + 290*t2) + 
                  t1*(3040 - 5067*t2 - 9529*Power(t2,2))) + 
               Power(s2,6)*(-259*Power(t1,4) + 
                  Power(t1,3)*(459 - 4629*t2) + 
                  Power(t1,2)*(7482 + 35483*t2 + 11432*Power(t2,2)) + 
                  t1*(-12989 - 43715*t2 - 6386*Power(t2,2) + 
                     21458*Power(t2,3)) + 
                  3*(1720 + 4363*t2 - 1151*Power(t2,2) - 
                     4620*Power(t2,3) + 216*Power(t2,4))) - 
               Power(s2,5)*(-3279 + 740*Power(t1,5) + 8158*t2 + 
                  34762*Power(t2,2) + 15952*Power(t2,3) - 
                  8413*Power(t2,4) + 372*Power(t2,5) - 
                  8*Power(t1,4)*(1437 + 1132*t2) + 
                  Power(t1,3)*(52597 + 62925*t2 + 466*Power(t2,2)) + 
                  Power(t1,2)*
                   (-73179 - 75128*t2 + 63768*Power(t2,2) + 
                     38348*Power(t2,3)) + 
                  t1*(34624 + 14518*t2 - 97410*Power(t2,2) - 
                     58632*Power(t2,3) + 17921*Power(t2,4))) - 
               (-1 + t1)*(1392 + 23299*t2 + 6759*Power(t2,2) - 
                  6275*Power(t2,3) - 5481*Power(t2,4) + 
                  1680*Power(t2,5) + Power(t1,7)*(344 + 70*t2) + 
                  2*Power(t1,6)*(1260 + 697*t2 + 194*Power(t2,2)) - 
                  2*Power(t1,5)*
                   (16338 + 6534*t2 + 305*Power(t2,2) + 
                     147*Power(t2,3)) + 
                  Power(t1,4)*
                   (109608 + 64162*t2 - 8887*Power(t2,2) - 
                     1149*Power(t2,3) - 196*Power(t2,4)) + 
                  Power(t1,3)*
                   (-155192 - 165977*t2 + 20190*Power(t2,2) + 
                     25446*Power(t2,3) - 2031*Power(t2,4) + 
                     128*Power(t2,5)) + 
                  Power(t1,2)*
                   (100976 + 198347*t2 + 8132*Power(t2,2) - 
                     65004*Power(t2,3) + 5819*Power(t2,4) + 
                     380*Power(t2,5)) - 
                  t1*(26916 + 108635*t2 + 25108*Power(t2,2) - 
                     46716*Power(t2,3) - 1769*Power(t2,4) + 
                     2020*Power(t2,5))) + 
               Power(s2,4)*(-5303 + 1080*Power(t1,6) - 22767*t2 - 
                  44725*Power(t2,2) - 2948*Power(t2,3) + 
                  14199*Power(t2,4) + 1965*Power(t2,5) - 
                  171*Power(t2,6) - 3*Power(t1,5)*(6073 + 1724*t2) + 
                  Power(t1,4)*(78399 + 20869*t2 - 16707*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (-50041 + 39179*t2 + 87230*Power(t2,2) + 
                     15385*Power(t2,3)) + 
                  Power(t1,2)*
                   (31188 - 206914*t2 - 352546*Power(t2,2) - 
                     51272*Power(t2,3) + 31952*Power(t2,4)) + 
                  t1*(13131 + 137290*t2 + 241172*Power(t2,2) + 
                     25314*Power(t2,3) - 55331*Power(t2,4) + 
                     3606*Power(t2,5))) + 
               s2*(-6853 - 70*Power(t1,8) + 30349*t2 + 
                  69401*Power(t2,2) + 21395*Power(t2,3) - 
                  18270*Power(t2,4) - 5218*Power(t2,5) + 
                  1464*Power(t2,6) + 
                  4*Power(t1,7)*(1663 + 390*t2 + 25*Power(t2,2)) + 
                  Power(t1,6)*
                   (-39200 + 3614*t2 + 5430*Power(t2,2) + 
                     220*Power(t2,3)) - 
                  Power(t1,5)*
                   (-62621 + 97566*t2 + 66519*Power(t2,2) + 
                     5266*Power(t2,3) + 360*Power(t2,4)) + 
                  Power(t1,4)*
                   (906 + 372469*t2 + 292141*Power(t2,2) + 
                     12355*Power(t2,3) - 7845*Power(t2,4) + 
                     104*Power(t2,5)) + 
                  t1*(2060 - 188014*t2 - 359921*Power(t2,2) - 
                     54374*Power(t2,3) + 76365*Power(t2,4) + 
                     7090*Power(t2,5) - 2504*Power(t2,6)) + 
                  Power(t1,3)*
                   (-76971 - 601373*t2 - 634160*Power(t2,2) - 
                     12528*Power(t2,3) + 51005*Power(t2,4) - 
                     785*Power(t2,5) + 32*Power(t2,6)) + 
                  Power(t1,2)*
                   (50853 + 479012*t2 + 693240*Power(t2,2) + 
                     38818*Power(t2,3) - 101465*Power(t2,4) - 
                     1002*Power(t2,5) + 1008*Power(t2,6))) - 
               Power(s2,3)*(959 + 280*Power(t1,7) + 
                  64*Power(t1,6)*(-97 + t2) + 31150*t2 + 
                  20714*Power(t2,2) - 36126*Power(t2,3) - 
                  30915*Power(t2,4) - 2310*Power(t2,5) + 
                  2352*Power(t2,6) - 96*Power(t2,7) + 
                  Power(t1,5)*(7693 - 36028*t2 - 12835*Power(t2,2)) + 
                  Power(t1,4)*
                   (98843 + 291659*t2 + 127374*Power(t2,2) + 
                     3515*Power(t2,3)) + 
                  Power(t1,3)*
                   (-260446 - 659772*t2 - 311246*Power(t2,2) + 
                     67496*Power(t2,3) + 25780*Power(t2,4)) + 
                  Power(t1,2)*
                   (227234 + 624306*t2 + 325510*Power(t2,2) - 
                     209958*Power(t2,3) - 82440*Power(t2,4) + 
                     5594*Power(t2,5)) - 
                  t1*(68155 + 251207*t2 + 153704*Power(t2,2) - 
                     177778*Power(t2,3) - 90641*Power(t2,4) + 
                     7719*Power(t2,5) + 1012*Power(t2,6))) + 
               Power(s2,2)*(-5281 - 18608*t2 + 62797*Power(t2,2) + 
                  68978*Power(t2,3) + 13473*Power(t2,4) - 
                  10822*Power(t2,5) - 1219*Power(t2,6) + 
                  256*Power(t2,7) + 2*Power(t1,7)*(221 + 90*t2) - 
                  2*Power(t1,6)*(14241 + 9242*t2 + 1024*Power(t2,2)) + 
                  Power(t1,5)*
                   (189221 + 135320*t2 + 6539*Power(t2,2) - 
                     3333*Power(t2,3)) + 
                  Power(t1,4)*
                   (-436418 - 329505*t2 + 89052*Power(t2,2) + 
                     68520*Power(t2,3) + 6230*Power(t2,4)) + 
                  Power(t1,3)*
                   (465613 + 381705*t2 - 367362*Power(t2,2) - 
                     271868*Power(t2,3) - 10405*Power(t2,4) + 
                     3413*Power(t2,5)) - 
                  Power(t1,2)*
                   (242802 + 251277*t2 - 505530*Power(t2,2) - 
                     445172*Power(t2,3) + 1724*Power(t2,4) + 
                     16331*Power(t2,5) + 530*Power(t2,6)) + 
                  t1*(57778 + 99829*t2 - 292339*Power(t2,2) - 
                     309075*Power(t2,3) - 7674*Power(t2,4) + 
                     24325*Power(t2,5) + 1364*Power(t2,6) - 
                     216*Power(t2,7)))) + 
            s1*(Power(s2,10)*(6 - 6*t1 + 8*t2) - 
               Power(s2,9)*(53 + 19*Power(t1,2) + 214*t2 + 
                  99*Power(t2,2) - 4*t1*(18 + 43*t2)) + 
               Power(s2,8)*(-4 + 13*Power(t1,3) + 1186*t2 + 
                  1709*Power(t2,2) + 227*Power(t2,3) + 
                  Power(t1,2)*(-137 + 482*t2) - 
                  2*t1*(-64 + 768*t2 + 805*Power(t2,2))) + 
               Power(s2,7)*(1312 + 5*Power(t1,4) + 1466*t2 - 
                  4991*Power(t2,2) - 5404*Power(t2,3) - 
                  65*Power(t2,4) - Power(t1,3)*(1047 + 1474*t2) + 
                  Power(t1,2)*(3676 + 6410*t2 - 772*Power(t2,2)) + 
                  t1*(-3946 - 6682*t2 + 5760*Power(t2,2) + 
                     6794*Power(t2,3))) - 
               Power(s2,6)*(922 + 297*Power(t1,5) + 10238*t2 + 
                  14871*Power(t2,2) - 1157*Power(t2,3) - 
                  7349*Power(t2,4) + 130*Power(t2,5) - 
                  2*Power(t1,4)*(2592 + 1313*t2) + 
                  Power(t1,3)*(14838 + 4710*t2 - 7337*Power(t2,2)) + 
                  Power(t1,2)*
                   (-13736 + 12968*t2 + 41755*Power(t2,2) + 
                     9394*Power(t2,3)) + 
                  t1*(2863 - 25584*t2 - 49094*Power(t2,2) - 
                     7100*Power(t2,3) + 11342*Power(t2,4))) + 
               Power(s2,5)*(-5555 + 424*Power(t1,6) - 8932*t2 + 
                  5366*Power(t2,2) + 24584*Power(t2,3) + 
                  9524*Power(t2,4) - 2988*Power(t2,5) - 8*Power(t2,6) - 
                  3*Power(t1,5)*(2423 + 444*t2) + 
                  Power(t1,4)*(11497 - 18992*t2 - 14349*Power(t2,2)) + 
                  Power(t1,3)*
                   (17546 + 106070*t2 + 77879*Power(t2,2) + 
                     1948*Power(t2,3)) + 
                  Power(t1,2)*
                   (-47164 - 152362*t2 - 94532*Power(t2,2) + 
                     40588*Power(t2,3) + 21559*Power(t2,4)) + 
                  t1*(30521 + 75562*t2 + 26982*Power(t2,2) - 
                     66116*Power(t2,3) - 33274*Power(t2,4) + 
                     7116*Power(t2,5))) + 
               (-1 + t1)*(-5275 + 28*Power(t1,8) + 5966*t2 + 
                  20356*Power(t2,2) + 3648*Power(t2,3) - 
                  459*Power(t2,4) - 3836*Power(t2,5) + 
                  840*Power(t2,6) + 
                  Power(t1,7)*(1640 + 571*t2 + 60*Power(t2,2)) + 
                  Power(t1,6)*
                   (-4316 + 1263*t2 + 574*Power(t2,2) + 92*Power(t2,3)) \
- Power(t1,5)*(24022 + 32253*t2 + 10285*Power(t2,2) - 
                     215*Power(t2,3) + 240*Power(t2,4)) + 
                  Power(t1,4)*
                   (86528 + 126077*t2 + 58457*Power(t2,2) - 
                     6371*Power(t2,3) - 844*Power(t2,4) + 
                     72*Power(t2,5)) + 
                  t1*(5283 - 44664*t2 - 98050*Power(t2,2) - 
                     16230*Power(t2,3) + 18921*Power(t2,4) + 
                     3388*Power(t2,5) - 1140*Power(t2,6)) + 
                  Power(t1,3)*
                   (-97829 - 192098*t2 - 155267*Power(t2,2) + 
                     12727*Power(t2,3) + 13601*Power(t2,4) - 
                     1220*Power(t2,5) + 16*Power(t2,6)) + 
                  Power(t1,2)*
                   (37955 + 135250*t2 + 183747*Power(t2,2) + 
                     6495*Power(t2,3) - 31259*Power(t2,4) + 
                     1548*Power(t2,5) + 340*Power(t2,6))) + 
               Power(s2,4)*(1303 - 136*Power(t1,7) + 
                  Power(t1,6)*(2267 - 434*t2) + 5348*t2 + 
                  25894*Power(t2,2) + 32645*Power(t2,3) + 
                  3033*Power(t2,4) - 5074*Power(t2,5) - 
                  1054*Power(t2,6) + 97*Power(t2,7) + 
                  2*Power(t1,5)*(12800 + 19119*t2 + 5172*Power(t2,2)) + 
                  Power(t1,4)*
                   (-130267 - 175506*t2 - 37435*Power(t2,2) + 
                     10538*Power(t2,3)) + 
                  Power(t1,3)*
                   (203303 + 245832*t2 - 50598*Power(t2,2) - 
                     117812*Power(t2,3) - 18027*Power(t2,4)) - 
                  2*Power(t1,2)*
                   (64208 + 58818*t2 - 92685*Power(t2,2) - 
                     122583*Power(t2,3) - 15536*Power(t2,4) + 
                     6425*Power(t2,5)) - 
                  2*t1*(-13173 - 1885*t2 + 67592*Power(t2,2) + 
                     85876*Power(t2,3) + 8318*Power(t2,4) - 
                     10827*Power(t2,5) + 513*Power(t2,6))) - 
               s2*(-4797 - 10578*t2 + 31298*Power(t2,2) + 
                  38372*Power(t2,3) + 13541*Power(t2,4) - 
                  7870*Power(t2,5) - 2022*Power(t2,6) + 
                  512*Power(t2,7) + Power(t1,8)*(557 + 96*t2) + 
                  2*Power(t1,7)*
                   (9231 + 5282*t2 + 998*Power(t2,2) + 40*Power(t2,3)) \
+ Power(t1,6)*(-129913 - 72942*t2 - 4269*Power(t2,2) + 
                     1822*Power(t2,3) + 10*Power(t2,4)) - 
                  Power(t1,5)*
                   (-310339 - 165538*t2 + 58326*Power(t2,2) + 
                     33826*Power(t2,3) + 2205*Power(t2,4) + 
                     168*Power(t2,5)) + 
                  Power(t1,4)*
                   (-349565 - 167268*t2 + 279992*Power(t2,2) + 
                     169576*Power(t2,3) + 2640*Power(t2,4) - 
                     2484*Power(t2,5) + 94*Power(t2,6)) + 
                  t1*(16229 + 34648*t2 - 171956*Power(t2,2) - 
                     215210*Power(t2,3) - 30701*Power(t2,4) + 
                     31334*Power(t2,5) + 2656*Power(t2,6) - 
                     888*Power(t2,7)) + 
                  Power(t1,3)*
                   (198594 + 90598*t2 - 486899*Power(t2,2) - 
                     387932*Power(t2,3) + 2348*Power(t2,4) + 
                     18526*Power(t2,5) - 481*Power(t2,6) - 
                     16*Power(t2,7)) + 
                  Power(t1,2)*
                   (-59906 - 50660*t2 + 408215*Power(t2,2) + 
                     426926*Power(t2,3) + 14677*Power(t2,4) - 
                     39566*Power(t2,5) - 184*Power(t2,6) + 
                     392*Power(t2,7))) + 
               Power(s2,3)*(4*Power(t1,7)*(183 + 56*t2) - 
                  Power(t1,6)*(35189 + 18996*t2 + 2241*Power(t2,2)) + 
                  Power(t1,5)*
                   (135992 + 54182*t2 - 27040*Power(t2,2) - 
                     8918*Power(t2,3)) + 
                  Power(t1,4)*
                   (-169117 + 85726*t2 + 269731*Power(t2,2) + 
                     88892*Power(t2,3) + 4100*Power(t2,4)) + 
                  Power(t1,3)*
                   (40249 - 359346*t2 - 640176*Power(t2,2) - 
                     221212*Power(t2,3) + 26449*Power(t2,4) + 
                     9658*Power(t2,5)) + 
                  Power(t1,2)*
                   (56933 + 349598*t2 + 630189*Power(t2,2) + 
                     233640*Power(t2,3) - 96133*Power(t2,4) - 
                     29850*Power(t2,5) + 1707*Power(t2,6)) - 
                  2*t1*(15689 + 57966*t2 + 132381*Power(t2,2) + 
                     53812*Power(t2,3) - 40198*Power(t2,4) - 
                     16766*Power(t2,5) + 1136*Power(t2,6) + 
                     146*Power(t2,7)) - 
                  2*(-889 - 2472*t2 - 17215*Power(t2,2) - 
                     6160*Power(t2,3) + 6581*Power(t2,4) + 
                     6112*Power(t2,5) + 502*Power(t2,6) - 
                     368*Power(t2,7) + 15*Power(t2,8))) + 
               Power(s2,2)*(-3780 - 196*Power(t1,8) + 16949*t2 + 
                  13586*Power(t2,2) - 37723*Power(t2,3) - 
                  29372*Power(t2,4) - 6717*Power(t2,5) + 
                  4008*Power(t2,6) + 281*Power(t2,7) - 68*Power(t2,8) + 
                  2*Power(t1,7)*(5203 + 1154*t2 + 50*Power(t2,2)) + 
                  Power(t1,6)*
                   (-8665 + 32856*t2 + 18149*Power(t2,2) + 
                     1711*Power(t2,3)) + 
                  Power(t1,5)*
                   (-139081 - 291634*t2 - 137862*Power(t2,2) - 
                     10240*Power(t2,3) + 625*Power(t2,4)) + 
                  Power(t1,4)*
                   (404582 + 739245*t2 + 354031*Power(t2,2) - 
                     36750*Power(t2,3) - 27940*Power(t2,4) - 
                     2285*Power(t2,5)) + 
                  Power(t1,3)*
                   (-446328 - 863450*t2 - 442077*Power(t2,2) + 
                     208380*Power(t2,3) + 119896*Power(t2,4) + 
                     2254*Power(t2,5) - 803*Power(t2,6)) + 
                  Power(t1,2)*
                   (213277 + 511450*t2 + 304190*Power(t2,2) - 
                     306213*Power(t2,3) - 201337*Power(t2,4) + 
                     3754*Power(t2,5) + 4785*Power(t2,6) + 
                     70*Power(t2,7)) + 
                  t1*(-30215 - 147866*t2 - 109256*Power(t2,2) + 
                     179308*Power(t2,3) + 139047*Power(t2,4) + 
                     2966*Power(t2,5) - 8182*Power(t2,6) - 
                     228*Power(t2,7) + 54*Power(t2,8))))) + 
         Power(s,2)*(2*Power(s1,9)*Power(s2,2)*
             (-6 + Power(s2,2) + 15*t1 - 9*Power(t1,2) + s2*(-3 + 4*t1)) \
+ 3*Power(s2,10)*t2*(2 + 2*Power(t1,2) + 4*t2 + Power(t2,2) - 
               4*t1*(1 + t2)) - 
            Power(s2,9)*(-10 + 2*Power(t1,4) + 9*t2 + 163*Power(t2,2) + 
               155*Power(t2,3) + 14*Power(t2,4) + 
               Power(t1,3)*(4 + 5*t2) + 
               Power(t1,2)*(-24 - t2 + 105*Power(t2,2)) - 
               t1*(-28 + 13*t2 + 268*Power(t2,2) + 144*Power(t2,3))) + 
            Power(s2,8)*(-23 - 384*t2 - 170*Power(t2,2) + 
               741*Power(t2,3) + 593*Power(t2,4) + 24*Power(t2,5) + 
               Power(t1,4)*(47 + 25*t2) + 
               2*Power(t1,3)*(-59 + 142*t2 + 158*Power(t2,2)) + 
               Power(t1,2)*(72 - 1027*t2 - 962*Power(t2,2) + 
                  239*Power(t2,3)) + 
               t1*(22 + 1102*t2 + 816*Power(t2,2) - 948*Power(t2,3) - 
                  610*Power(t2,4))) + 
            Power(s2,7)*(-265 + 6*Power(t1,6) + 
               7*Power(t1,5)*(-19 + t2) + 378*t2 + 2581*Power(t2,2) + 
               2455*Power(t2,3) - 307*Power(t2,4) - 883*Power(t2,5) - 
               20*Power(t2,6) + 
               Power(t1,4)*(15 - 1364*t2 - 746*Power(t2,2)) + 
               Power(t1,3)*(958 + 3969*t2 + 627*Power(t2,2) - 
                  1168*Power(t2,3)) + 
               2*Power(t1,2)*
                (-862 - 1748*t2 + 1949*Power(t2,2) + 
                  2851*Power(t2,3) + 382*Power(t2,4)) + 
               t1*(1143 + 506*t2 - 6360*Power(t2,2) - 
                  7048*Power(t2,3) - 409*Power(t2,4) + 1007*Power(t2,5))\
) - Power(s2,6)*(-409 + 4*Power(t1,7) - 3309*t2 - 2673*Power(t2,2) + 
               2543*Power(t2,3) + 4780*Power(t2,4) + 1253*Power(t2,5) - 
               527*Power(t2,6) - 11*Power(t2,7) + 
               Power(t1,6)*(-55 + 69*t2) - 
               Power(t1,5)*(1396 + 2849*t2 + 807*Power(t2,2)) + 
               Power(t1,4)*(5391 + 4945*t2 - 3501*Power(t2,2) - 
                  2559*Power(t2,3)) + 
               2*Power(t1,3)*
                (-3449 + 2964*t2 + 11132*Power(t2,2) + 
                  5646*Power(t2,3) + 91*Power(t2,4)) + 
               Power(t1,2)*(3049 - 18949*t2 - 32921*Power(t2,2) - 
                  11566*Power(t2,3) + 5515*Power(t2,4) + 
                  2016*Power(t2,5)) + 
               t1*(314 + 14165*t2 + 17638*Power(t2,2) + 
                  260*Power(t2,3) - 10342*Power(t2,4) - 
                  3335*Power(t2,5) + 701*Power(t2,6))) + 
            Power(-1 + t1,2)*
             (20*Power(t1,8) + 
               Power(t1,7)*(1454 + 363*t2 + 45*Power(t2,2)) + 
               Power(t1,6)*(-2200 - 2203*t2 + 181*Power(t2,2) + 
                  56*Power(t2,3) + 4*Power(t2,4)) - 
               Power(t1,5)*(7571 + 4489*t2 + 3326*Power(t2,2) + 
                  510*Power(t2,3) + 24*Power(t2,4) + 10*Power(t2,5)) + 
               Power(t1,4)*(23657 + 18757*t2 + 20679*Power(t2,2) + 
                  2642*Power(t2,3) - 863*Power(t2,4) - 
                  14*Power(t2,5) + 8*Power(t2,6)) + 
               t1*(1960 + 5993*t2 - 8775*Power(t2,2) - 
                  12386*Power(t2,3) - 998*Power(t2,4) + 
                  2199*Power(t2,5) + 357*Power(t2,6) - 142*Power(t2,7)) \
- Power(t1,3)*(23963 + 15925*t2 + 35600*Power(t2,2) + 
                  15046*Power(t2,3) - 4362*Power(t2,4) - 
                  755*Power(t2,5) + 105*Power(t2,6) + 2*Power(t2,7)) + 
               Power(t1,2)*(8015 - 330*t2 + 25204*Power(t2,2) + 
                  22690*Power(t2,3) - 2713*Power(t2,4) - 
                  2976*Power(t2,5) + 232*Power(t2,6) + 42*Power(t2,7)) \
+ 2*(-686 - 1089*t2 + 838*Power(t2,2) + 1175*Power(t2,3) + 
                  224*Power(t2,4) - 19*Power(t2,5) - 252*Power(t2,6) + 
                  57*Power(t2,7))) + 
            Power(s2,5)*(1544 - 397*t2 - 5507*Power(t2,2) - 
               9972*Power(t2,3) - 6091*Power(t2,4) + 880*Power(t2,5) + 
               980*Power(t2,6) - 46*Power(t2,7) - 6*Power(t2,8) + 
               Power(t1,7)*(75 + 36*t2) - 
               Power(t1,6)*(2835 + 2149*t2 + 281*Power(t2,2)) - 
               Power(t1,5)*(-6974 + 6454*t2 + 10324*Power(t2,2) + 
                  2655*Power(t2,3)) + 
               Power(t1,4)*(476 + 48123*t2 + 45930*Power(t2,2) + 
                  8912*Power(t2,3) - 1290*Power(t2,4)) + 
               Power(t1,3)*(-17591 - 82480*t2 - 60126*Power(t2,2) + 
                  8041*Power(t2,3) + 16017*Power(t2,4) + 
                  2169*Power(t2,5)) + 
               Power(t1,2)*(21127 + 56539*t2 + 20376*Power(t2,2) - 
                  41488*Power(t2,3) - 34368*Power(t2,4) - 
                  3391*Power(t2,5) + 1402*Power(t2,6)) + 
               t1*(-9770 - 13218*t2 + 9932*Power(t2,2) + 
                  37209*Power(t2,3) + 25902*Power(t2,4) + 
                  362*Power(t2,5) - 2594*Power(t2,6) + 163*Power(t2,7))) \
+ Power(s2,4)*(-1013 - 40*Power(t1,8) - 5600*t2 - 7702*Power(t2,2) - 
               9307*Power(t2,3) - 2764*Power(t2,4) + 2581*Power(t2,5) + 
               1247*Power(t2,6) + 62*Power(t2,7) - 60*Power(t2,8) + 
               2*Power(t2,9) + 
               Power(t1,7)*(1679 + 439*t2 + 9*Power(t2,2)) + 
               Power(t1,6)*(4207 + 17882*t2 + 8944*Power(t2,2) + 
                  1074*Power(t2,3)) + 
               Power(t1,5)*(-39328 - 77829*t2 - 30303*Power(t2,2) + 
                  3503*Power(t2,3) + 1801*Power(t2,4)) - 
               Power(t1,4)*(-81821 - 108641*t2 + 322*Power(t2,2) + 
                  56412*Power(t2,3) + 18292*Power(t2,4) + 
                  1031*Power(t2,5)) + 
               Power(t1,3)*(-74897 - 40395*t2 + 94073*Power(t2,2) + 
                  143128*Power(t2,3) + 46460*Power(t2,4) - 
                  2335*Power(t2,5) - 1329*Power(t2,6)) - 
               Power(t1,2)*(-29801 + 31461*t2 + 119212*Power(t2,2) + 
                  146391*Power(t2,3) + 49264*Power(t2,4) - 
                  12034*Power(t2,5) - 4248*Power(t2,6) + 
                  293*Power(t2,7)) + 
               t1*(-2230 + 28323*t2 + 54513*Power(t2,2) + 
                  64341*Power(t2,3) + 22108*Power(t2,4) - 
                  11001*Power(t2,5) - 4478*Power(t2,6) + 
                  413*Power(t2,7) + 19*Power(t2,8))) - 
            s2*(-1 + t1)*(-192 - 5589*t2 - 1474*Power(t2,2) + 
               6150*Power(t2,3) + 4400*Power(t2,4) + 1293*Power(t2,5) - 
               1336*Power(t2,6) - 146*Power(t2,7) + 70*Power(t2,8) + 
               Power(t1,8)*(711 + 134*t2) + 
               Power(t1,7)*(8068 + 5007*t2 + 1465*Power(t2,2) + 
                  114*Power(t2,3)) + 
               Power(t1,6)*(-47169 - 32974*t2 - 8069*Power(t2,2) + 
                  146*Power(t2,3) + 106*Power(t2,4) + 4*Power(t2,5)) + 
               Power(t1,5)*(92953 + 67936*t2 + 4070*Power(t2,2) - 
                  10409*Power(t2,3) - 1495*Power(t2,4) - 
                  121*Power(t2,5) - 10*Power(t2,6)) + 
               Power(t1,4)*(-84351 - 64305*t2 + 27096*Power(t2,2) + 
                  54291*Power(t2,3) + 8298*Power(t2,4) - 
                  1402*Power(t2,5) - 38*Power(t2,6) + 8*Power(t2,7)) + 
               t1*(-2035 + 19357*t2 + 550*Power(t2,2) - 
                  35432*Power(t2,3) - 24949*Power(t2,4) + 
                  579*Power(t2,5) + 4318*Power(t2,6) - 
                  58*Power(t2,7) - 106*Power(t2,8)) + 
               Power(t1,3)*(31494 + 38816*t2 - 43261*Power(t2,2) - 
                  101362*Power(t2,3) - 30436*Power(t2,4) + 
                  7936*Power(t2,5) + 1008*Power(t2,6) - 
                  99*Power(t2,7) - 2*Power(t2,8)) + 
               Power(t1,2)*(521 - 28382*t2 + 19617*Power(t2,2) + 
                  86553*Power(t2,3) + 43932*Power(t2,4) - 
                  8103*Power(t2,5) - 4056*Power(t2,6) + 
                  322*Power(t2,7) + 38*Power(t2,8))) + 
            Power(s2,3)*(-2139 - 94*t2 - 5820*Power(t2,2) - 
               6114*Power(t2,3) + 2799*Power(t2,4) + 3752*Power(t2,5) + 
               1746*Power(t2,6) - 304*Power(t2,7) - 118*Power(t2,8) + 
               12*Power(t2,9) + 2*Power(t1,8)*(-100 + 7*t2) - 
               Power(t1,7)*(11420 + 11163*t2 + 2722*Power(t2,2) + 
                  126*Power(t2,3)) + 
               Power(t1,6)*(51186 + 30805*t2 - 5331*Power(t2,2) - 
                  6297*Power(t2,3) - 710*Power(t2,4)) + 
               Power(t1,5)*(-76781 + 26500*t2 + 104070*Power(t2,2) + 
                  54219*Power(t2,3) + 6276*Power(t2,4) + 27*Power(t2,5)\
) + Power(t1,4)*(28451 - 182124*t2 - 294179*Power(t2,2) - 
                  149963*Power(t2,3) - 8246*Power(t2,4) + 
                  5601*Power(t2,5) + 573*Power(t2,6)) + 
               Power(t1,3)*(41112 + 247508*t2 + 362789*Power(t2,2) + 
                  198556*Power(t2,3) - 9626*Power(t2,4) - 
                  25640*Power(t2,5) - 1442*Power(t2,6) + 
                  195*Power(t2,7)) - 
               Power(t1,2)*(48602 + 141883*t2 + 221591*Power(t2,2) + 
                  138009*Power(t2,3) - 23013*Power(t2,4) - 
                  39875*Power(t2,5) - 2701*Power(t2,6) + 
                  1077*Power(t2,7) + 3*Power(t2,8)) + 
               t1*(18393 + 30437*t2 + 62784*Power(t2,2) + 
                  47757*Power(t2,3) - 13614*Power(t2,4) - 
                  23439*Power(t2,5) - 3714*Power(t2,6) + 
                  1249*Power(t2,7) + 101*Power(t2,8) - 10*Power(t2,9))) \
+ Power(s2,2)*(941 - 19*Power(t1,9) - 804*t2 - 6899*Power(t2,2) + 
               988*Power(t2,3) + 7460*Power(t2,4) + 3662*Power(t2,5) + 
               131*Power(t2,6) - 718*Power(t2,7) + 19*Power(t2,8) + 
               12*Power(t2,9) + 
               Power(t1,8)*(5309 + 2297*t2 + 244*Power(t2,2)) + 
               Power(t1,7)*(-10118 + 9759*t2 + 8136*Power(t2,2) + 
                  1792*Power(t2,3) + 69*Power(t2,4)) + 
               Power(t1,6)*(-38980 - 110672*t2 - 70738*Power(t2,2) - 
                  12195*Power(t2,3) - 15*Power(t2,4) + 50*Power(t2,5)) \
+ Power(t1,5)*(162236 + 306768*t2 + 204366*Power(t2,2) + 
                  21315*Power(t2,3) - 10925*Power(t2,4) - 
                  1614*Power(t2,5) - 99*Power(t2,6)) - 
               Power(t1,4)*(240174 + 401519*t2 + 299473*Power(t2,2) - 
                  2251*Power(t2,3) - 58154*Power(t2,4) - 
                  7694*Power(t2,5) + 723*Power(t2,6) + 10*Power(t2,7)) \
+ Power(t1,3)*(176778 + 276309*t2 + 256400*Power(t2,2) - 
                  33786*Power(t2,3) - 118248*Power(t2,4) - 
                  20445*Power(t2,5) + 4872*Power(t2,6) + 
                  322*Power(t2,7) - 16*Power(t2,8)) + 
               t1*(6467 + 15072*t2 + 43733*Power(t2,2) - 
                  7941*Power(t2,3) - 48233*Power(t2,4) - 
                  17616*Power(t2,5) + 2935*Power(t2,6) + 
                  1803*Power(t2,7) - 102*Power(t2,8) - 18*Power(t2,9)) \
+ Power(t1,2)*(-62440 - 97210*t2 - 135769*Power(t2,2) + 
                  27575*Power(t2,3) + 111745*Power(t2,4) + 
                  28251*Power(t2,5) - 7094*Power(t2,6) - 
                  1410*Power(t2,7) + 102*Power(t2,8) + 6*Power(t2,9))) + 
            Power(s1,8)*s2*(11*Power(s2,4) - 
               24*Power(-1 + t1,2)*(-2 + 3*t1) + 
               Power(s2,3)*(-11 + 10*t1 - 14*t2) + 
               Power(s2,2)*(-115 - 107*Power(t1,2) + 
                  t1*(225 - 74*t2) + 60*t2) + 
               3*s2*(-4 + 10*Power(t1,3) + t1*(15 - 86*t2) + 36*t2 + 
                  10*Power(t1,2)*(-2 + 5*t2))) + 
            Power(s1,7)*(17*Power(s2,6) - 
               4*Power(-1 + t1,2)*
                (10 - 25*t1 + 15*Power(t1,2) + 3*Power(t1,3)) + 
               Power(s2,5)*(143 - 184*t1 - 71*t2) + 
               Power(s2,4)*(-22 + 74*Power(t1,2) + 125*t2 + 
                  40*Power(t2,2) - t1*(134 + 77*t2)) - 
               s2*(-1 + t1)*(80 + 14*Power(t1,4) + 
                  Power(t1,2)*(16 - 532*t2) - 376*t2 + 
                  Power(t1,3)*(-7 + 12*t2) + 2*t1*(-65 + 448*t2)) + 
               Power(s2,3)*(370 - 268*Power(t1,3) + 887*t2 - 
                  264*Power(t2,2) + 32*Power(t1,2)*(37 + 22*t2) + 
                  t1*(-1297 - 1592*t2 + 304*Power(t2,2))) + 
               Power(s2,2)*(705 + 207*Power(t1,4) + 65*t2 - 
                  432*Power(t2,2) - 2*Power(t1,3)*(663 + 73*t2) + 
                  Power(t1,2)*(2823 + 222*t2 - 552*Power(t2,2)) + 
                  t1*(-2396 - 165*t2 + 984*Power(t2,2)))) + 
            Power(s1,6)*(2*Power(s2,7) + 
               Power(s2,6)*(554 - 676*t1 - 91*t2) + 
               Power(-1 + t1,2)*
                (-66 + 56*Power(t1,4) + t1*(55 - 640*t2) + 270*t2 + 
                  Power(t1,3)*(-127 + 4*t2) + 
                  10*Power(t1,2)*(7 + 45*t2)) + 
               Power(s2,5)*(708 + 895*Power(t1,2) - 979*t2 + 
                  189*Power(t2,2) + t1*(-1767 + 1342*t2)) - 
               Power(s2,4)*(-1331 + 1238*Power(t1,3) + 193*t2 + 
                  591*Power(t2,2) + 56*Power(t2,3) + 
                  Power(t1,2)*(-4311 + 1118*t2) + 
                  t1*(4640 - 1985*t2 - 283*Power(t2,2))) + 
               s2*(-1 + t1)*(1473 + 42*Power(t1,5) + 474*t2 - 
                  1288*Power(t2,2) + Power(t1,4)*(105 + 64*t2) + 
                  Power(t1,3)*(-2464 + 215*t2 + 44*Power(t2,2)) - 
                  2*Power(t1,2)*(-3160 + 329*t2 + 814*Power(t2,2)) + 
                  t1*(-5362 - 284*t2 + 2872*Power(t2,2))) + 
               Power(s2,3)*(1931 + 773*Power(t1,4) - 2924*t2 - 
                  3085*Power(t2,2) + 672*Power(t2,3) + 
                  Power(t1,3)*(-3581 + 2305*t2) + 
                  Power(t1,2)*(7055 - 9654*t2 - 2040*Power(t2,2)) + 
                  t1*(-6256 + 10402*t2 + 5048*Power(t2,2) - 
                     728*Power(t2,3))) - 
               Power(s2,2)*(682 + 78*Power(t1,5) + 4804*t2 + 
                  199*Power(t2,2) - 1008*Power(t2,3) + 
                  4*Power(t1,4)*(173 + 356*t2) - 
                  2*Power(t1,3)*(2118 + 4433*t2 + 155*Power(t2,2)) + 
                  Power(t1,2)*
                   (6608 + 18864*t2 + 210*Power(t2,2) - 
                     1176*Power(t2,3)) + 
                  t1*(-3846 - 16135*t2 - 183*Power(t2,2) + 
                     2184*Power(t2,3)))) - 
            Power(s1,5)*(16*Power(s2,8) + 
               2*Power(s2,7)*(-409 + 463*t1 - 5*t2) - 
               Power(s2,6)*(513 + 1228*Power(t1,2) - 3390*t2 + 
                  189*Power(t2,2) + t1*(-1807 + 4174*t2)) + 
               Power(-1 + t1,2)*
                (-929 + 60*Power(t1,5) - 327*t2 + 780*Power(t2,2) + 
                  Power(t1,4)*(-166 + 135*t2) + 
                  t1*(3494 - 211*t2 - 1590*Power(t2,2)) + 
                  Power(t1,3)*(1222 - 263*t2 - 78*Power(t2,2)) + 
                  3*Power(t1,2)*(-1255 + 198*t2 + 380*Power(t2,2))) + 
               Power(s2,5)*(1552 + 1367*Power(t1,3) + 5186*t2 - 
                  2790*Power(t2,2) + 259*Power(t2,3) + 
                  Power(t1,2)*(-1031 + 6384*t2) + 
                  t1*(-1820 - 12602*t2 + 4107*Power(t2,2))) - 
               Power(s2,4)*(-2950 + 536*Power(t1,4) - 8792*t2 + 
                  1161*Power(t2,2) + 1525*Power(t2,3) + 
                  28*Power(t2,4) + Power(t1,3)*(4133 + 7998*t2) + 
                  3*Power(t1,2)*(-4476 - 9302*t2 + 1589*Power(t2,2)) + 
                  t1*(11453 + 30192*t2 - 8250*Power(t2,2) - 
                     629*Power(t2,3))) + 
               s2*(-1 + t1)*(-422 + 21*Power(t1,6) + 8653*t2 + 
                  1436*Power(t2,2) - 2550*Power(t2,3) + 
                  Power(t1,5)*(-679 + 60*t2) + 
                  Power(t1,4)*(4267 + 1800*t2 + 147*Power(t2,2)) + 
                  Power(t1,2)*
                   (8350 + 38563*t2 - 3357*Power(t2,2) - 
                     2718*Power(t2,3)) + 
                  Power(t1,3)*
                   (-10198 - 16341*t2 + 1024*Power(t2,2) + 
                     42*Power(t2,3)) + 
                  t1*(-1525 - 32051*t2 + 183*Power(t2,2) + 
                     5226*Power(t2,3))) + 
               Power(s2,3)*(7379 - 178*Power(t1,5) + 10414*t2 - 
                  9182*Power(t2,2) - 6263*Power(t2,3) + 
                  1092*Power(t2,4) + Power(t1,4)*(8779 + 4078*t2) + 
                  2*Power(t1,3)*
                   (-17097 - 9288*t2 + 3895*Power(t2,2)) + 
                  Power(t1,2)*
                   (50959 + 36644*t2 - 31575*Power(t2,2) - 
                     3388*Power(t2,3)) + 
                  t1*(-32601 - 33086*t2 + 33510*Power(t2,2) + 
                     9336*Power(t2,3) - 1120*Power(t2,4))) + 
               Power(s2,2)*(7127 - 108*Power(t1,6) - 5093*t2 - 
                  14068*Power(t2,2) - 421*Power(t2,3) + 
                  1512*Power(t2,4) + 4*Power(t1,5)*(-431 + 17*t2) - 
                  3*Power(t1,4)*(-4033 + 2361*t2 + 1390*Power(t2,2)) + 
                  Power(t1,3)*
                   (-34093 + 32966*t2 + 25102*Power(t2,2) + 
                     394*Power(t2,3)) + 
                  t1*(-29998 + 28578*t2 + 46398*Power(t2,2) - 
                     87*Power(t2,3) - 3108*Power(t2,4)) + 
                  Power(t1,2)*
                   (46679 - 49304*t2 - 53535*Power(t2,2) + 
                     282*Power(t2,3) + 1596*Power(t2,4)))) + 
            Power(s1,4)*(-13*Power(s2,9) + 
               Power(s2,8)*(561 - 574*t1 + 88*t2) + 
               Power(s2,7)*(-678 + 294*t1 + 413*Power(t1,2) - 
                  4164*t2 + 4720*t1*t2 - 80*Power(t2,2)) - 
               Power(s2,6)*(4541 + 76*Power(t1,3) + 3341*t2 - 
                  8571*Power(t2,2) + 175*Power(t2,3) + 
                  Power(t1,2)*(5811 + 6832*t2) + 
                  t1*(-10309 - 10503*t2 + 10661*Power(t2,2))) + 
               Power(-1 + t1,2)*
                (809 + 15*Power(t1,6) - 4554*t2 - 893*Power(t2,2) + 
                  1334*Power(t2,3) + Power(t1,5)*(451 + 230*t2) + 
                  Power(t1,4)*(-3452 - 899*t2 + 16*Power(t2,2)) + 
                  Power(t1,3)*
                   (8308 + 6521*t2 - 230*Power(t2,2) - 
                     114*Power(t2,3)) + 
                  Power(t1,2)*
                   (-5212 - 19929*t2 + 2769*Power(t2,2) + 
                     1302*Power(t2,3)) - 
                  t1*(703 - 18211*t2 + 1842*Power(t2,2) + 
                     2102*Power(t2,3))) + 
               Power(s2,5)*(-2700 - 624*Power(t1,4) + 7852*t2 + 
                  14842*Power(t2,2) - 4276*Power(t2,3) + 
                  175*Power(t2,4) + Power(t1,3)*(13153 + 6477*t2) + 
                  3*Power(t1,2)*(-8974 - 1579*t2 + 6082*Power(t2,2)) + 
                  t1*(17274 - 9300*t2 - 35788*Power(t2,2) + 
                     6851*Power(t2,3))) + 
               Power(s2,4)*(1156 + 516*Power(t1,5) + 18787*t2 + 
                  23591*Power(t2,2) - 2212*Power(t2,3) - 
                  2365*Power(t2,4) + 28*Power(t2,5) - 
                  Power(t1,4)*(12708 + 439*t2) + 
                  Power(t1,3)*(30248 - 28835*t2 - 21775*Power(t2,2)) + 
                  Power(t1,2)*
                   (-24072 + 81448*t2 + 74976*Power(t2,2) - 
                     9661*Power(t2,3)) + 
                  t1*(4889 - 69689*t2 - 80712*Power(t2,2) + 
                     16243*Power(t2,3) + 895*Power(t2,4))) - 
               s2*(-1 + t1)*(9597 + 240*Power(t1,6) + 618*t2 - 
                  20399*Power(t2,2) - 2750*Power(t2,3) + 
                  3230*Power(t2,4) + 
                  Power(t1,5)*(-2613 + 2123*t2 + 50*Power(t2,2)) - 
                  2*Power(t1,4)*
                   (-7288 + 8630*t2 + 2797*Power(t2,2) + 
                     110*Power(t2,3)) + 
                  t1*(-40966 + 12964*t2 + 76211*Power(t2,2) - 
                     1030*Power(t2,3) - 6010*Power(t2,4)) + 
                  Power(t1,3)*
                   (-45737 + 48963*t2 + 39851*Power(t2,2) - 
                     1905*Power(t2,3) + 30*Power(t2,4)) + 
                  Power(t1,2)*
                   (64759 - 46478*t2 - 91829*Power(t2,2) + 
                     6850*Power(t2,3) + 2750*Power(t2,4))) + 
               Power(s2,3)*(5710 + 340*Power(t1,6) + 39964*t2 + 
                  24805*Power(t2,2) - 15192*Power(t2,3) - 
                  8035*Power(t2,4) + 1176*Power(t2,5) - 
                  Power(t1,5)*(1272 + 3161*t2) + 
                  2*Power(t1,4)*(9731 + 26219*t2 + 5095*Power(t2,2)) + 
                  Power(t1,3)*
                   (-61912 - 188066*t2 - 43844*Power(t2,2) + 
                     13665*Power(t2,3)) + 
                  Power(t1,2)*
                   (72614 + 276373*t2 + 84151*Power(t2,2) - 
                     54517*Power(t2,3) - 3490*Power(t2,4)) - 
                  t1*(35043 + 176796*t2 + 76762*Power(t2,2) - 
                     57209*Power(t2,3) - 10930*Power(t2,4) + 
                     1148*Power(t2,5))) + 
               Power(s2,2)*(13976 - 150*Power(t1,7) + 33747*t2 - 
                  12461*Power(t2,2) - 23050*Power(t2,3) - 
                  555*Power(t2,4) + 1512*Power(t2,5) + 
                  Power(t1,6)*(3602 + 525*t2) + 
                  Power(t1,5)*(-31953 - 13650*t2 + 418*Power(t2,2)) + 
                  Power(t1,4)*
                   (114591 + 69819*t2 - 19445*Power(t2,2) - 
                     6590*Power(t2,3)) + 
                  Power(t1,3)*
                   (-199463 - 177745*t2 + 88556*Power(t2,2) + 
                     38590*Power(t2,3) + 330*Power(t2,4)) + 
                  2*Power(t1,2)*
                   (89482 + 116619*t2 - 65780*Power(t2,2) - 
                     41685*Power(t2,3) + 495*Power(t2,4) + 
                     714*Power(t2,5)) - 
                  t1*(79560 + 146024*t2 - 74822*Power(t2,2) - 
                     73965*Power(t2,3) + 555*Power(t2,4) + 
                     2940*Power(t2,5)))) - 
            Power(s1,3)*(3*Power(s2,10) + 
               Power(s2,9)*(-161 + 150*t1 - 53*t2) + 
               Power(s2,8)*(717 + 170*Power(t1,2) + 2255*t2 + 
                  192*Power(t2,2) - t1*(855 + 2311*t2)) + 
               Power(s2,7)*(2047 - 532*Power(t1,3) - 2563*t2 - 
                  8485*Power(t2,2) - 180*Power(t2,3) + 
                  2*Power(t1,2)*(2103 + 847*t2) + 
                  t1*(-5780 + 1004*t2 + 9629*Power(t2,2))) + 
               Power(s2,6)*(-4349 + 849*Power(t1,4) - 19057*t2 - 
                  8390*Power(t2,2) + 11440*Power(t2,3) - 
                  35*Power(t2,4) + 2*Power(t1,3)*(-2921 + 512*t2) + 
                  Power(t1,2)*(3539 - 26290*t2 - 15378*Power(t2,2)) + 
                  t1*(5833 + 43831*t2 + 24428*Power(t2,2) - 
                     14400*Power(t2,3))) + 
               Power(-1 + t1,2)*
                (4554 + 2359*t2 - 7882*Power(t2,2) - 
                  1881*Power(t2,3) + 1536*Power(t2,4) + 
                  Power(t1,6)*(316 + 70*t2) + 
                  2*Power(t1,5)*(-513 + 346*t2 + 136*Power(t2,2)) - 
                  2*Power(t1,4)*
                   (-2413 + 4337*t2 + 671*Power(t2,2) + 
                     97*Power(t2,3)) + 
                  Power(t1,3)*
                   (-21886 + 25974*t2 + 11911*Power(t2,2) - 
                     487*Power(t2,3) - 16*Power(t2,4)) + 
                  Power(t1,2)*
                   (33166 - 17433*t2 - 38706*Power(t2,2) + 
                     5675*Power(t2,3) + 708*Power(t2,4)) - 
                  t1*(20154 + 2124*t2 - 34907*Power(t2,2) + 
                     3353*Power(t2,3) + 1808*Power(t2,4))) + 
               Power(s2,5)*(-9923 - 264*Power(t1,5) - 15305*t2 + 
                  14730*Power(t2,2) + 21470*Power(t2,3) - 
                  3779*Power(t2,4) + 21*Power(t2,5) - 
                  Power(t1,4)*(111 + 5984*t2) + 
                  Power(t1,3)*(25707 + 63316*t2 + 14200*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (-29375 - 61815*t2 - 6692*Power(t2,2) + 
                     13393*Power(t2,3)) + 
                  2*t1*(21694 + 41158*t2 - 7529*Power(t2,2) - 
                     26008*Power(t2,3) + 3367*Power(t2,4))) + 
               Power(s2,4)*(-2474 - 600*Power(t1,6) + 1831*t2 + 
                  41240*Power(t2,2) + 32578*Power(t2,3) - 
                  1868*Power(t2,4) - 2271*Power(t2,5) + 
                  56*Power(t2,6) + 5*Power(t1,5)*(2273 + 1208*t2) - 
                  Power(t1,4)*(76733 + 63460*t2 + 176*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (-81097 - 71541*t2 + 30628*Power(t2,2) + 
                     15523*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (70047 + 60851*t2 - 86681*Power(t2,2) - 
                     52425*Power(t2,3) + 5372*Power(t2,4)) + 
                  t1*(46278 + 34345*t2 - 150642*Power(t2,2) - 
                     111862*Power(t2,3) + 17482*Power(t2,4) + 
                     815*Power(t2,5))) + 
               s2*(-1 + t1)*(-9948 + 70*Power(t1,7) - 37422*t2 - 
                  1039*Power(t2,2) + 25345*Power(t2,3) + 
                  3320*Power(t2,4) - 2708*Power(t2,5) - 
                  4*Power(t1,6)*(1287 + 405*t2 + 25*Power(t2,2)) + 
                  Power(t1,5)*
                   (41316 + 15044*t2 - 2062*Power(t2,2) - 
                     80*Power(t2,3)) + 
                  Power(t1,4)*
                   (-138731 - 68342*t2 + 26093*Power(t2,2) + 
                     7070*Power(t2,3) + 200*Power(t2,4)) + 
                  Power(t1,3)*
                   (221326 + 194296*t2 - 89724*Power(t2,2) - 
                     47155*Power(t2,3) + 1925*Power(t2,4) - 
                     80*Power(t2,5)) - 
                  Power(t1,2)*
                   (176783 + 265660*t2 - 93050*Power(t2,2) - 
                     111365*Power(t2,3) + 7550*Power(t2,4) + 
                     1792*Power(t2,5)) + 
                  t1*(67847 + 164280*t2 - 28078*Power(t2,2) - 
                     94265*Power(t2,3) + 1160*Power(t2,4) + 
                     4580*Power(t2,5))) + 
               Power(s2,3)*(1246 + 280*Power(t1,7) + 29015*t2 + 
                  79835*Power(t2,2) + 32987*Power(t2,3) - 
                  14478*Power(t2,4) - 6605*Power(t2,5) + 
                  840*Power(t2,6) - 22*Power(t1,6)*(353 + 42*t2) + 
                  Power(t1,5)*(56101 + 3492*t2 - 7779*Power(t2,2)) + 
                  Power(t1,4)*
                   (-127907 + 59502*t2 + 109338*Power(t2,2) + 
                     14405*Power(t2,3)) + 
                  2*Power(t1,3)*
                   (60726 - 112602*t2 - 193111*Power(t2,2) - 
                     28664*Power(t2,3) + 6740*Power(t2,4)) + 
                  Power(t1,2)*
                   (-45086 + 283596*t2 + 567074*Power(t2,2) + 
                     105840*Power(t2,3) - 53958*Power(t2,4) - 
                     2232*Power(t2,5)) + 
                  t1*(1703 - 149888*t2 - 360678*Power(t2,2) - 
                     98044*Power(t2,3) + 56381*Power(t2,4) + 
                     8200*Power(t2,5) - 784*Power(t2,6))) + 
               Power(s2,2)*(4545 + Power(t1,7)*(522 - 180*t2) + 
                  61447*t2 + 62566*Power(t2,2) - 13514*Power(t2,3) - 
                  22925*Power(t2,4) - 387*Power(t2,5) + 
                  1008*Power(t2,6) + 
                  2*Power(t1,6)*(748 + 6319*t2 + 1036*Power(t2,2)) - 
                  Power(t1,5)*
                   (49947 + 121256*t2 + 32475*Power(t2,2) + 
                     33*Power(t2,3)) + 
                  Power(t1,4)*
                   (184776 + 444996*t2 + 148502*Power(t2,2) - 
                     23480*Power(t2,3) - 5895*Power(t2,4)) + 
                  2*Power(t1,3)*
                   (-138230 - 396269*t2 - 178921*Power(t2,2) + 
                     57552*Power(t2,3) + 17125*Power(t2,4) + 
                     75*Power(t2,5)) + 
                  Power(t1,2)*
                   (193055 + 730738*t2 + 455946*Power(t2,2) - 
                     172030*Power(t2,3) - 76605*Power(t2,4) + 
                     1350*Power(t2,5) + 840*Power(t2,6)) - 
                  t1*(57988 + 335817*t2 + 278949*Power(t2,2) - 
                     94393*Power(t2,3) - 70720*Power(t2,4) + 
                     945*Power(t2,5) + 1848*Power(t2,6)))) + 
            Power(s1,2)*(Power(s2,10)*(12 - 12*t1 + 9*t2) - 
               Power(s2,9)*(151 + 93*Power(t1,2) + 477*t2 + 
                  81*Power(t2,2) - 4*t1*(61 + 111*t2)) + 
               2*Power(s2,8)*
                (-64 + 95*Power(t1,3) + 1107*t2 + 1710*Power(t2,2) + 
                  104*Power(t2,3) + Power(t1,2)*(-334 + 309*t2) - 
                  3*t1*(-101 + 456*t2 + 585*Power(t2,2))) + 
               Power(s2,7)*(2712 - 283*Power(t1,4) + 6543*t2 - 
                  3369*Power(t2,2) - 8633*Power(t2,3) - 
                  190*Power(t2,4) - Power(t1,3)*(601 + 2439*t2) + 
                  Power(t1,2)*(5096 + 14522*t2 + 2943*Power(t2,2)) + 
                  t1*(-6924 - 18803*t2 + 657*Power(t2,2) + 
                     9809*Power(t2,3))) + 
               Power(s2,6)*(1890 - 38*Power(t1,5) - 11710*t2 - 
                  29281*Power(t2,2) - 10062*Power(t2,3) + 
                  8484*Power(t2,4) + 63*Power(t2,5) + 
                  16*Power(t1,4)*(339 + 293*t2) + 
                  2*Power(t1,3)*
                   (-11533 - 11654*t2 + 992*Power(t2,2)) + 
                  Power(t1,2)*
                   (31353 + 17546*t2 - 40452*Power(t2,2) - 
                     17188*Power(t2,3)) + 
                  t1*(-15563 + 12874*t2 + 66987*Power(t2,2) + 
                     27910*Power(t2,3) - 10834*Power(t2,4))) + 
               Power(-1 + t1,2)*
                (1659 + 42*Power(t1,7) + 13846*t2 + 2366*Power(t2,2) - 
                  5856*Power(t2,3) - 2565*Power(t2,4) + 
                  1194*Power(t2,5) + 
                  3*Power(t1,6)*(566 + 231*t2 + 30*Power(t2,2)) + 
                  2*Power(t1,5)*
                   (-8683 - 1680*t2 + 118*Power(t2,2) + 
                     42*Power(t2,3)) + 
                  Power(t1,4)*
                   (64152 + 15757*t2 - 8823*Power(t2,2) - 
                     665*Power(t2,3) - 168*Power(t2,4)) + 
                  Power(t1,3)*
                   (-97927 - 66842*t2 + 33379*Power(t2,2) + 
                     9902*Power(t2,3) - 806*Power(t2,4) + 
                     48*Power(t2,5)) + 
                  Power(t1,2)*
                   (67935 + 100236*t2 - 24269*Power(t2,2) - 
                     35661*Power(t2,3) + 5301*Power(t2,4) + 
                     222*Power(t2,5)) - 
                  t1*(20109 + 60942*t2 + 1683*Power(t2,2) - 
                     31356*Power(t2,3) + 1942*Power(t2,4) + 
                     1212*Power(t2,5))) + 
               Power(s2,5)*(-9084 + 432*Power(t1,6) - 30855*t2 - 
                  29103*Power(t2,2) + 12992*Power(t2,3) + 
                  16598*Power(t2,4) - 1899*Power(t2,5) - 
                  49*Power(t2,6) - 26*Power(t1,5)*(399 + 142*t2) + 
                  Power(t1,4)*(38317 + 7493*t2 - 11340*Power(t2,2)) + 
                  Power(t1,3)*
                   (-38728 + 66423*t2 + 103710*Power(t2,2) + 
                     16606*Power(t2,3)) + 
                  Power(t1,2)*
                   (-6523 - 168334*t2 - 202542*Power(t2,2) - 
                     20072*Power(t2,3) + 21297*Power(t2,4)) + 
                  t1*(25960 + 129106*t2 + 140328*Power(t2,2) - 
                     9134*Power(t2,3) - 40835*Power(t2,4) + 
                     3900*Power(t2,5))) - 
               Power(s2,4)*(8149 + 204*Power(t1,7) + 16280*t2 + 
                  3233*Power(t2,2) - 40500*Power(t2,3) - 
                  24143*Power(t2,4) + 585*Power(t2,5) + 
                  1325*Power(t2,6) - 40*Power(t2,7) - 
                  Power(t1,6)*(5783 + 222*t2) + 
                  Power(t1,5)*(7777 - 29353*t2 - 12429*Power(t2,2)) + 
                  Power(t1,4)*
                   (65555 + 224074*t2 + 109015*Power(t2,2) + 
                     2210*Power(t2,3)) + 
                  2*Power(t1,3)*
                   (-94050 - 245272*t2 - 123838*Power(t2,2) + 
                     27437*Power(t2,3) + 11949*Power(t2,4)) + 
                  Power(t1,2)*
                   (185035 + 446292*t2 + 227310*Power(t2,2) - 
                     168126*Power(t2,3) - 79089*Power(t2,4) + 
                     6720*Power(t2,5)) - 
                  t1*(72837 + 166335*t2 + 79687*Power(t2,2) - 
                     149030*Power(t2,3) - 83634*Power(t2,4) + 
                     10527*Power(t2,5) + 457*Power(t2,6))) - 
               s2*(-1 + t1)*(-1289 + 34485*t2 + 49799*Power(t2,2) + 
                  3205*Power(t2,3) - 18076*Power(t2,4) - 
                  2390*Power(t2,5) + 1476*Power(t2,6) + 
                  9*Power(t1,7)*(135 + 16*t2) + 
                  Power(t1,6)*
                   (1145 + 9708*t2 + 2496*Power(t2,2) + 
                     120*Power(t2,3)) - 
                  Power(t1,5)*
                   (60154 + 91958*t2 + 23511*Power(t2,2) - 
                     350*Power(t2,3) + 30*Power(t2,4)) + 
                  Power(t1,4)*
                   (200318 + 337535*t2 + 101283*Power(t2,2) - 
                     18876*Power(t2,3) - 4071*Power(t2,4) - 
                     84*Power(t2,5)) + 
                  Power(t1,3)*
                   (-266898 - 571487*t2 - 281348*Power(t2,2) + 
                     81287*Power(t2,3) + 28809*Power(t2,4) - 
                     1213*Power(t2,5) + 48*Power(t2,6)) + 
                  Power(t1,2)*
                   (159688 + 484476*t2 + 379014*Power(t2,2) - 
                     88169*Power(t2,3) - 73951*Power(t2,4) + 
                     4914*Power(t2,5) + 792*Power(t2,6)) - 
                  t1*(34031 + 202750*t2 + 228597*Power(t2,2) - 
                     23943*Power(t2,3) - 65609*Power(t2,4) + 
                     660*Power(t2,5) + 2316*Power(t2,6))) + 
               Power(s2,3)*(2274 - 3326*t2 + 43841*Power(t2,2) + 
                  73046*Power(t2,3) + 25306*Power(t2,4) - 
                  8012*Power(t2,5) - 3367*Power(t2,6) + 
                  384*Power(t2,7) + 6*Power(t1,7)*(5 + 56*t2) - 
                  Power(t1,6)*(27541 + 25088*t2 + 3621*Power(t2,2)) + 
                  Power(t1,5)*
                   (180584 + 180703*t2 + 18534*Power(t2,2) - 
                     6760*Power(t2,3)) + 
                  Power(t1,4)*
                   (-414151 - 432381*t2 + 47662*Power(t2,2) + 
                     102079*Power(t2,3) + 11260*Power(t2,4)) + 
                  Power(t1,3)*
                   (436999 + 469119*t2 - 268622*Power(t2,2) - 
                     370662*Power(t2,3) - 40965*Power(t2,4) + 
                     7403*Power(t2,5)) + 
                  Power(t1,2)*
                   (-213519 - 243636*t2 + 369666*Power(t2,2) + 
                     548740*Power(t2,3) + 73229*Power(t2,4) - 
                     30576*Power(t2,5) - 824*Power(t2,6)) + 
                  t1*(35324 + 54342*t2 - 208087*Power(t2,2) - 
                     344811*Power(t2,3) - 70580*Power(t2,4) + 
                     32196*Power(t2,5) + 3792*Power(t2,6) - 
                     344*Power(t2,7))) - 
               Power(s2,2)*(789 + 294*Power(t1,8) - 17813*t2 - 
                  88488*Power(t2,2) - 56061*Power(t2,3) + 
                  6476*Power(t2,4) + 13900*Power(t2,5) + 
                  85*Power(t2,6) - 432*Power(t2,7) - 
                  2*Power(t1,7)*(7223 + 2163*t2 + 75*Power(t2,2)) + 
                  Power(t1,6)*
                   (91667 + 16092*t2 - 14099*Power(t2,2) - 
                     2187*Power(t2,3)) + 
                  Power(t1,5)*
                   (-202406 + 64013*t2 + 157050*Power(t2,2) + 
                     32510*Power(t2,3) + 757*Power(t2,4)) + 
                  Power(t1,4)*
                   (183971 - 360230*t2 - 606153*Power(t2,2) - 
                     143637*Power(t2,3) + 14220*Power(t2,4) + 
                     2832*Power(t2,5)) + 
                  2*Power(t1,3)*
                   (-20702 + 300286*t2 + 554905*Power(t2,2) + 
                     169633*Power(t2,3) - 40048*Power(t2,4) - 
                     8543*Power(t2,5) + 7*Power(t2,6)) - 
                  Power(t1,2)*
                   (32218 + 451112*t2 + 1040523*Power(t2,2) + 
                     427145*Power(t2,3) - 120650*Power(t2,4) - 
                     41268*Power(t2,5) + 1098*Power(t2,6) + 
                     312*Power(t2,7)) + 
                  t1*(13753 + 152807*t2 + 482511*Power(t2,2) + 
                     257434*Power(t2,3) - 62337*Power(t2,4) - 
                     40641*Power(t2,5) + 915*Power(t2,6) + 
                     744*Power(t2,7)))) - 
            s1*(3*Power(s2,10)*
                (2 + 2*Power(t1,2) + 8*t2 + 3*Power(t2,2) - 
                  4*t1*(1 + 2*t2)) + 
               Power(s2,9)*(-5 - 9*Power(t1,3) + 
                  Power(t1,2)*(13 - 198*t2) - 314*t2 - 
                  471*Power(t2,2) - 55*Power(t2,3) + 
                  t1*(1 + 512*t2 + 438*Power(t2,2))) + 
               Power(s2,8)*(-389 + 30*Power(t1,4) - 276*t2 + 
                  2238*Power(t2,2) + 2319*Power(t2,3) + 
                  112*Power(t2,4) + Power(t1,3)*(274 + 484*t2) + 
                  Power(t1,2)*(-1027 - 1564*t2 + 687*Power(t2,2)) + 
                  t1*(1112 + 1356*t2 - 2829*Power(t2,2) - 
                     2383*Power(t2,3))) + 
               Power(s2,7)*(374 + 28*Power(t1,5) + 5340*t2 + 
                  6969*Power(t2,2) - 1791*Power(t2,3) - 
                  4377*Power(t2,4) - 98*Power(t2,5) - 
                  4*Power(t1,4)*(347 + 233*t2) + 
                  Power(t1,3)*(3919 - 312*t2 - 3093*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (-1706 + 4713*t2 + 8036*Power(t2,2) + 
                     1213*Power(t2,3)) + 
                  t1*(479 - 13522*t2 - 20125*Power(t2,2) - 
                     462*Power(t2,3) + 4981*Power(t2,4))) + 
               Power(s2,6)*(3544 - 112*Power(t1,6) + 4382*t2 - 
                  9838*Power(t2,2) - 19545*Power(t2,3) - 
                  5753*Power(t2,4) + 3306*Power(t2,5) + 
                  49*Power(t2,6) + Power(t1,5)*(2623 + 638*t2) + 
                  8*Power(t1,4)*(-465 + 1172*t2 + 812*Power(t2,2)) + 
                  Power(t1,3)*
                   (-8052 - 45716*t2 - 29118*Power(t2,2) + 
                     702*Power(t2,3)) + 
                  Power(t1,2)*
                   (20876 + 64036*t2 + 26065*Power(t2,2) - 
                     25488*Power(t2,3) - 9430*Power(t2,4)) + 
                  t1*(-15159 - 32716*t2 + 6485*Power(t2,2) + 
                     43807*Power(t2,3) + 15513*Power(t2,4) - 
                     4298*Power(t2,5))) + 
               Power(-1 + t1,2)*
                (-2635 + 4848*t2 + 11368*Power(t2,2) + 
                  1264*Power(t2,3) - 1637*Power(t2,4) - 
                  1820*Power(t2,5) + 556*Power(t2,6) + 
                  Power(t1,7)*(426 + 99*t2) + 
                  Power(t1,6)*
                   (1999 + 1487*t2 + 446*Power(t2,2) + 39*Power(t2,3)) \
- Power(t1,5)*(31758 + 17753*t2 + 2901*Power(t2,2) + 29*Power(t2,3) + 
                     28*Power(t2,4)) + 
                  Power(t1,4)*
                   (83753 + 78601*t2 + 13376*Power(t2,2) - 
                     4464*Power(t2,3) - 70*Power(t2,4) - 29*Power(t2,5)\
) + Power(t1,3)*(-89727 - 130244*t2 - 58925*Power(t2,2) + 
                     20075*Power(t2,3) + 4045*Power(t2,4) - 
                     518*Power(t2,5) + 18*Power(t2,6)) + 
                  Power(t1,2)*
                   (39517 + 96908*t2 + 88086*Power(t2,2) - 
                     14761*Power(t2,3) - 16095*Power(t2,4) + 
                     2103*Power(t2,5) + 108*Power(t2,6)) - 
                  t1*(1587 + 33778*t2 + 52062*Power(t2,2) + 
                     1260*Power(t2,3) - 13365*Power(t2,4) - 
                     192*Power(t2,5) + 598*Power(t2,6))) + 
               Power(s2,5)*(-539 + 57*Power(t1,7) - 14884*t2 - 
                  31025*Power(t2,2) - 22589*Power(t2,3) + 
                  5442*Power(t2,4) + 6472*Power(t2,5) - 
                  488*Power(t2,6) - 31*Power(t2,7) + 
                  10*Power(t1,6)*(-137 + 21*t2) - 
                  2*Power(t1,5)*(5003 + 10429*t2 + 3119*Power(t2,2)) + 
                  Power(t1,4)*
                   (54865 + 83506*t2 + 17049*Power(t2,2) - 
                     7270*Power(t2,3)) + 
                  Power(t1,3)*
                   (-89357 - 95968*t2 + 48209*Power(t2,2) + 
                     69564*Power(t2,3) + 9685*Power(t2,4)) + 
                  Power(t1,2)*
                   (60112 + 10356*t2 - 151076*Power(t2,2) - 
                     140202*Power(t2,3) - 13785*Power(t2,4) + 
                     8670*Power(t2,5)) + 
                  t1*(-13762 + 37638*t2 + 123222*Power(t2,2) + 
                     101188*Power(t2,3) - 1194*Power(t2,4) - 
                     16366*Power(t2,5) + 1231*Power(t2,6))) - 
               Power(s2,4)*(7752 + 15856*t2 + 23537*Power(t2,2) + 
                  6672*Power(t2,3) - 17678*Power(t2,4) - 
                  8942*Power(t2,5) - 79*Power(t2,6) + 431*Power(t2,7) - 
                  14*Power(t2,8) + 3*Power(t1,7)*(101 + 66*t2) - 
                  Power(t1,6)*(20859 + 14851*t2 + 1980*Power(t2,2)) + 
                  Power(t1,5)*
                   (81071 + 37166*t2 - 21553*Power(t2,2) - 
                     8706*Power(t2,3)) + 
                  Power(t1,4)*
                   (-103119 + 69982*t2 + 205367*Power(t2,2) + 
                     76555*Power(t2,3) + 2968*Power(t2,4)) + 
                  Power(t1,3)*
                   (20865 - 287814*t2 - 475532*Power(t2,2) - 
                     181302*Power(t2,3) + 20655*Power(t2,4) + 
                     9196*Power(t2,5)) + 
                  Power(t1,2)*
                   (53572 + 307477*t2 + 456947*Power(t2,2) + 
                     178944*Power(t2,3) - 74818*Power(t2,4) - 
                     29868*Power(t2,5) + 2207*Power(t2,6)) - 
                  t1*(39585 + 128014*t2 + 186594*Power(t2,2) + 
                     72339*Power(t2,3) - 67625*Power(t2,4) - 
                     31410*Power(t2,5) + 3302*Power(t2,6) + 
                     143*Power(t2,7))) + 
               s2*(-1 + t1)*(3545 + 3*Power(t1,8) + 712*t2 - 
                  30512*Power(t2,2) - 26374*Power(t2,3) - 
                  3655*Power(t2,4) + 7286*Power(t2,5) + 
                  924*Power(t2,6) - 478*Power(t2,7) - 
                  Power(t1,7)*(8458 + 2993*t2 + 321*Power(t2,2)) + 
                  Power(t1,6)*
                   (43302 + 8857*t2 - 5026*Power(t2,2) - 
                     1222*Power(t2,3) - 45*Power(t2,4)) + 
                  Power(t1,5)*
                   (-59694 + 48279*t2 + 62887*Power(t2,2) + 
                     12575*Power(t2,3) + 389*Power(t2,4) + 
                     52*Power(t2,5)) - 
                  Power(t1,4)*
                   (9026 + 211785*t2 + 257174*Power(t2,2) + 
                     55815*Power(t2,3) - 7178*Power(t2,4) - 
                     938*Power(t2,5) + Power(t2,6)) + 
                  Power(t1,3)*
                   (79329 + 297518*t2 + 455664*Power(t2,2) + 
                     163225*Power(t2,3) - 38264*Power(t2,4) - 
                     8636*Power(t2,5) + 490*Power(t2,6) - 6*Power(t2,7)\
) - Power(t1,2)*(54820 + 179528*t2 + 395902*Power(t2,2) + 
                     222045*Power(t2,3) - 41350*Power(t2,4) - 
                     26228*Power(t2,5) + 1853*Power(t2,6) + 
                     238*Power(t2,7)) + 
                  t1*(5819 + 38952*t2 + 170231*Power(t2,2) + 
                     130232*Power(t2,3) - 7883*Power(t2,4) - 
                     25184*Power(t2,5) + 251*Power(t2,6) + 
                     722*Power(t2,7))) + 
               Power(s2,3)*(-878 + 219*Power(t1,8) - 4215*t2 - 
                  10672*Power(t2,2) + 23335*Power(t2,3) + 
                  29548*Power(t2,4) + 10387*Power(t2,5) - 
                  2402*Power(t2,6) - 965*Power(t2,7) + 
                  102*Power(t2,8) - 
                  Power(t1,7)*(10763 + 2912*t2 + 81*Power(t2,2)) - 
                  Power(t1,6)*
                   (-19217 + 33188*t2 + 24038*Power(t2,2) + 
                     3067*Power(t2,3)) + 
                  Power(t1,5)*
                   (77732 + 287436*t2 + 181395*Power(t2,2) + 
                     20046*Power(t2,3) - 1937*Power(t2,4)) + 
                  Power(t1,4)*
                   (-285997 - 713095*t2 - 460245*Power(t2,2) - 
                     624*Power(t2,3) + 42001*Power(t2,4) + 
                     4313*Power(t2,5)) + 
                  2*Power(t1,3)*
                   (179047 + 402260*t2 + 276272*Power(t2,2) - 
                     57478*Power(t2,3) - 81976*Power(t2,4) - 
                     6964*Power(t2,5) + 1015*Power(t2,6)) - 
                  Power(t1,2)*
                   (203043 + 439176*t2 + 339898*Power(t2,2) - 
                     181697*Power(t2,3) - 246955*Power(t2,4) - 
                     24652*Power(t2,5) + 9107*Power(t2,6) + 
                     140*Power(t2,7)) + 
                  t1*(45419 + 100630*t2 + 101064*Power(t2,2) - 
                     106856*Power(t2,3) - 151767*Power(t2,4) - 
                     26182*Power(t2,5) + 9868*Power(t2,6) + 
                     968*Power(t2,7) - 88*Power(t2,8))) + 
               Power(s2,2)*(2677 - 7901*t2 + 14624*Power(t2,2) + 
                  48477*Power(t2,3) + 23777*Power(t2,4) - 
                  881*Power(t2,5) - 4774*Power(t2,6) + 41*Power(t2,7) + 
                  108*Power(t2,8) + Power(t1,8)*(1244 + 27*t2) + 
                  Power(t1,7)*
                   (23447 + 23076*t2 + 5721*Power(t2,2) + 
                     249*Power(t2,3)) + 
                  Power(t1,6)*
                   (-170979 - 165398*t2 - 30234*Power(t2,2) + 
                     5048*Power(t2,3) + 798*Power(t2,4)) + 
                  Power(t1,5)*
                   (430115 + 414671*t2 + 6941*Power(t2,2) - 
                     78672*Power(t2,3) - 13575*Power(t2,4) - 
                     551*Power(t2,5)) + 
                  Power(t1,4)*
                   (-525551 - 497744*t2 + 181305*Power(t2,2) + 
                     333902*Power(t2,3) + 60549*Power(t2,4) - 
                     4517*Power(t2,5) - 584*Power(t2,6)) + 
                  Power(t1,3)*
                   (324705 + 314426*t2 - 364583*Power(t2,2) - 
                     634983*Power(t2,3) - 145521*Power(t2,4) + 
                     29690*Power(t2,5) + 4186*Power(t2,6) - 
                     50*Power(t2,7)) + 
                  Power(t1,2)*
                   (-85035 - 114242*t2 + 291307*Power(t2,2) + 
                     600494*Power(t2,3) + 186009*Power(t2,4) - 
                     44578*Power(t2,5) - 11949*Power(t2,6) + 
                     510*Power(t2,7) + 66*Power(t2,8)) - 
                  t1*(623 - 33085*t2 + 105084*Power(t2,2) + 
                     274487*Power(t2,3) + 112127*Power(t2,4) - 
                     20969*Power(t2,5) - 13030*Power(t2,6) + 
                     477*Power(t2,7) + 174*Power(t2,8))))) + 
         s*(-2*Power(s1,9)*Power(s2,2)*(-1 + t1)*
             (6 + 2*Power(s2,2) + s2*(9 - 11*t1) - 15*t1 + 9*Power(t1,2)) \
+ Power(s2,10)*Power(t2,2)*(14 + 14*Power(t1,2) + 15*t2 + Power(t2,2) - 
               t1*(28 + 15*t2)) - 
            Power(s2,9)*t2*(-36 + 4*Power(t1,4) - 3*t2 + 
               138*Power(t2,2) + 104*Power(t2,3) + 4*Power(t2,4) + 
               8*Power(t1,3)*(3 + 4*t2) + 
               Power(t1,2)*(-96 - 67*t2 + 71*Power(t2,2)) + 
               t1*(104 + 38*t2 - 209*Power(t2,2) - 101*Power(t2,3))) + 
            Power(s2,8)*(22 + 4*Power(t1,5) - 45*t2 - 513*Power(t2,2) - 
               407*Power(t2,3) + 202*Power(t2,4) + 230*Power(t2,5) + 
               6*Power(t2,6) + 
               Power(t1,4)*(6 + 144*t2 + 91*Power(t2,2)) + 
               Power(t1,3)*(-64 - 387*t2 + 140*Power(t2,2) + 
                  240*Power(t2,3)) + 
               Power(t1,2)*(116 + 297*t2 - 1066*Power(t2,2) - 
                  1004*Power(t2,3) - 41*Power(t2,4)) - 
               t1*(84 + 9*t2 - 1348*Power(t2,2) - 1171*Power(t2,3) + 
                  160*Power(t2,4) + 230*Power(t2,5))) + 
            Power(s2,7)*(-33 - 706*t2 + 12*Power(t1,6)*t2 - 
               341*Power(t2,2) + 1279*Power(t2,3) + 1491*Power(t2,4) + 
               245*Power(t2,5) - 220*Power(t2,6) - 4*Power(t2,7) - 
               Power(t1,5)*(112 + 398*t2 + 117*Power(t2,2)) + 
               Power(t1,4)*(415 + 487*t2 - 940*Power(t2,2) - 
                  574*Power(t2,3)) + 
               Power(t1,3)*(-540 + 1513*t2 + 4381*Power(t2,2) + 
                  1785*Power(t2,3) - 106*Power(t2,4)) + 
               Power(t1,2)*(250 - 3661*t2 - 5815*Power(t2,2) - 
                  446*Power(t2,3) + 1855*Power(t2,4) + 379*Power(t2,5)) \
+ t1*(20 + 2753*t2 + 2832*Power(t2,2) - 2044*Power(t2,3) - 
                  3235*Power(t2,4) - 612*Power(t2,5) + 230*Power(t2,6))) \
+ Power(s2,6)*(-324 + 170*t2 + 3119*Power(t2,2) + 3875*Power(t2,3) + 
               1125*Power(t2,4) - 1023*Power(t2,5) - 487*Power(t2,6) + 
               87*Power(t2,7) + Power(t2,8) - 4*Power(t1,7)*(3 + 2*t2) + 
               Power(t1,6)*(319 + 370*t2 + 37*Power(t2,2)) + 
               2*Power(t1,5)*
                (-386 + 735*t2 + 1314*Power(t2,2) + 377*Power(t2,3)) + 
               6*Power(t1,4)*
                (-31 - 1563*t2 - 1527*Power(t2,2) - 270*Power(t2,3) + 
                  78*Power(t2,4)) + 
               Power(t1,3)*(2504 + 15564*t2 + 7192*Power(t2,2) - 
                  5602*Power(t2,3) - 4289*Power(t2,4) - 454*Power(t2,5)) \
+ Power(t1,2)*(-3209 - 10506*t2 + 5524*Power(t2,2) + 
                  16784*Power(t2,3) + 8282*Power(t2,4) + 
                  37*Power(t2,5) - 443*Power(t2,6)) + 
               t1*(1680 + 2318*t2 - 9338*Power(t2,2) - 
                  14191*Power(t2,3) - 5591*Power(t2,4) + 
                  1440*Power(t2,5) + 934*Power(t2,6) - 95*Power(t2,7))) + 
            Power(-1 + t1,3)*(-439 - 522*t2 + 527*Power(t2,2) + 
               632*Power(t2,3) + 91*Power(t2,4) - 102*Power(t2,5) - 
               131*Power(t2,6) + 40*Power(t2,7) + 
               8*Power(t1,7)*(40 + 7*t2) + 
               Power(t1,6)*(846 - 473*t2 + 88*Power(t2,2) + 
                  19*Power(t2,3)) - 
               Power(t1,5)*(7221 + 3510*t2 - 6*Power(t2,2) + 
                  82*Power(t2,3) + 17*Power(t2,4)) + 
               Power(t1,4)*(14791 + 11489*t2 + 4865*Power(t2,2) - 
                  1038*Power(t2,3) - 62*Power(t2,4) + 11*Power(t2,5)) - 
               Power(t1,3)*(13638 + 11593*t2 + 10608*Power(t2,2) + 
                  1461*Power(t2,3) - 1917*Power(t2,4) + 
                  70*Power(t2,5) + 19*Power(t2,6)) + 
               t1*(135 + 565*t2 - 2710*Power(t2,2) - 3198*Power(t2,3) + 
                  217*Power(t2,4) + 815*Power(t2,5) - 10*Power(t2,6) - 
                  38*Power(t2,7)) + 
               2*Power(t1,2)*(2603 + 1990*t2 + 3944*Power(t2,2) + 
                  2496*Power(t2,3) - 1001*Power(t2,4) - 
                  355*Power(t2,5) + 76*Power(t2,6) + 3*Power(t2,7))) + 
            Power(s2,5)*(198 + 8*Power(t1,8) + 3442*t2 + 
               4390*Power(t2,2) + 1974*Power(t2,3) - 1386*Power(t2,4) - 
               2207*Power(t2,5) - 356*Power(t2,6) + 153*Power(t2,7) - 
               4*Power(t2,8) + 
               Power(t1,7)*(-261 - 70*t2 + 7*Power(t2,2)) - 
               Power(t1,6)*(634 + 4708*t2 + 2875*Power(t2,2) + 
                  405*Power(t2,3)) + 
               Power(t1,5)*(6303 + 18413*t2 + 7015*Power(t2,2) - 
                  1672*Power(t2,3) - 798*Power(t2,4)) + 
               Power(t1,4)*(-13734 - 20862*t2 + 10753*Power(t2,2) + 
                  20242*Power(t2,3) + 6358*Power(t2,4) + 316*Power(t2,5)\
) + Power(t1,3)*(13273 - 3204*t2 - 48883*Power(t2,2) - 
                  47071*Power(t2,3) - 12603*Power(t2,4) + 
                  1326*Power(t2,5) + 489*Power(t2,6)) + 
               Power(t1,2)*(-5678 + 22744*t2 + 56170*Power(t2,2) + 
                  44264*Power(t2,3) + 7826*Power(t2,4) - 
                  5943*Power(t2,5) - 1514*Power(t2,6) + 176*Power(t2,7)) \
+ t1*(525 - 15755*t2 - 26577*Power(t2,2) - 17332*Power(t2,3) + 
                  602*Power(t2,4) + 6496*Power(t2,5) + 
                  1377*Power(t2,6) - 329*Power(t2,7) + 5*Power(t2,8))) - 
            s2*Power(-1 + t1,2)*
             (-22 + 56*Power(t1,8) - 1863*t2 - 294*Power(t2,2) + 
               2349*Power(t2,3) + 1446*Power(t2,4) + 123*Power(t2,5) - 
               570*Power(t2,6) - 17*Power(t2,7) + 32*Power(t2,8) + 
               Power(t1,7)*(2561 + 1192*t2 + 207*Power(t2,2)) + 
               Power(t1,6)*(-9427 - 3835*t2 - 797*Power(t2,2) + 
                  245*Power(t2,3) + 38*Power(t2,4)) - 
               Power(t1,5)*(-8460 + 5944*t2 + 8749*Power(t2,2) + 
                  1578*Power(t2,3) + 211*Power(t2,4) + 34*Power(t2,5)) + 
               Power(t1,4)*(8470 + 30507*t2 + 35355*Power(t2,2) + 
                  14315*Power(t2,3) - 1653*Power(t2,4) - 
                  240*Power(t2,5) + 14*Power(t2,6)) - 
               Power(t1,3)*(21321 + 35709*t2 + 46968*Power(t2,2) + 
                  33592*Power(t2,3) + 1353*Power(t2,4) - 
                  3911*Power(t2,5) + 110*Power(t2,6) + 26*Power(t2,7)) + 
               Power(t1,2)*(14747 + 13691*t2 + 25672*Power(t2,2) + 
                  31997*Power(t2,3) + 8321*Power(t2,4) - 
                  5481*Power(t2,5) - 1024*Power(t2,6) + 
                  237*Power(t2,7) + 8*Power(t2,8)) - 
               t1*(3524 - 1961*t2 + 4432*Power(t2,2) + 
                  13685*Power(t2,3) + 6732*Power(t2,4) - 
                  1907*Power(t2,5) - 1576*Power(t2,6) + 
                  167*Power(t2,7) + 40*Power(t2,8))) + 
            Power(s2,4)*(1357 + Power(t1,8)*(33 - 22*t2) + 2065*t2 + 
               353*Power(t2,2) - 592*Power(t2,3) - 2475*Power(t2,4) - 
               2395*Power(t2,5) - 470*Power(t2,6) + 154*Power(t2,7) + 
               39*Power(t2,8) - 4*Power(t2,9) + 
               Power(t1,7)*(2658 + 4124*t2 + 1222*Power(t2,2) + 
                  69*Power(t2,3)) + 
               Power(t1,6)*(-12182 - 9604*t2 + 3732*Power(t2,2) + 
                  3497*Power(t2,3) + 474*Power(t2,4)) + 
               Power(t1,5)*(18626 - 17588*t2 - 48289*Power(t2,2) - 
                  26650*Power(t2,3) - 3571*Power(t2,4) + 45*Power(t2,5)) \
+ Power(t1,4)*(-5642 + 85756*t2 + 125081*Power(t2,2) + 
                  64597*Power(t2,3) + 3904*Power(t2,4) - 
                  3030*Power(t2,5) - 338*Power(t2,6)) - 
               Power(t1,3)*(14930 + 119163*t2 + 142476*Power(t2,2) + 
                  69765*Power(t2,3) - 7758*Power(t2,4) - 
                  13139*Power(t2,5) - 983*Power(t2,6) + 158*Power(t2,7)) \
+ Power(t1,2)*(18370 + 76905*t2 + 77226*Power(t2,2) + 
                  33737*Power(t2,3) - 17510*Power(t2,4) - 
                  19713*Power(t2,5) - 1712*Power(t2,6) + 
                  711*Power(t2,7) - 6*Power(t2,8)) + 
               t1*(-8290 - 22473*t2 - 16849*Power(t2,2) - 
                  4893*Power(t2,3) + 11423*Power(t2,4) + 
                  11954*Power(t2,5) + 1527*Power(t2,6) - 
                  699*Power(t2,7) - 34*Power(t2,8) + 4*Power(t2,9))) + 
            Power(s2,2)*(-1010 + 94*t2 + 2024*Power(t2,2) - 
               1601*Power(t2,3) - 3614*Power(t2,4) - 1326*Power(t2,5) + 
               382*Power(t2,6) + 349*Power(t2,7) - 34*Power(t2,8) - 
               8*Power(t2,9) + Power(t1,9)*(504 + 173*t2) + 
               Power(t1,8)*(3656 + 6015*t2 + 2014*Power(t2,2) + 
                  238*Power(t2,3)) + 
               Power(t1,7)*(-33150 - 40740*t2 - 14283*Power(t2,2) - 
                  722*Power(t2,3) + 258*Power(t2,4) + 19*Power(t2,5)) + 
               Power(t1,6)*(93569 + 100375*t2 + 27576*Power(t2,2) - 
                  9742*Power(t2,3) - 3208*Power(t2,4) - 
                  261*Power(t2,5) - 17*Power(t2,6)) + 
               Power(t1,5)*(-132556 - 115658*t2 - 4556*Power(t2,2) + 
                  58287*Power(t2,3) + 19647*Power(t2,4) - 
                  685*Power(t2,5) - 209*Power(t2,6) + 3*Power(t2,7)) + 
               Power(t1,4)*(100340 + 53253*t2 - 42637*Power(t2,2) - 
                  125387*Power(t2,3) - 57841*Power(t2,4) + 
                  2729*Power(t2,5) + 2829*Power(t2,6) - 
                  60*Power(t2,7) - 7*Power(t2,8)) + 
               Power(t1,3)*(-34002 + 10286*t2 + 49245*Power(t2,2) + 
                  132020*Power(t2,3) + 86451*Power(t2,4) + 
                  103*Power(t2,5) - 7489*Power(t2,6) - 
                  262*Power(t2,7) + 106*Power(t2,8) + 2*Power(t2,9)) - 
               Power(t1,2)*(2371 + 18529*t2 + 16611*Power(t2,2) + 
                  71254*Power(t2,3) + 67075*Power(t2,4) + 
                  5824*Power(t2,5) - 7838*Power(t2,6) - 
                  1015*Power(t2,7) + 219*Power(t2,8) + 12*Power(t2,9)) + 
               t1*(5020 + 4731*t2 - 2772*Power(t2,2) + 
                  18161*Power(t2,3) + 25382*Power(t2,4) + 
                  5245*Power(t2,5) - 3334*Power(t2,6) - 
                  1045*Power(t2,7) + 154*Power(t2,8) + 18*Power(t2,9))) + 
            Power(s2,3)*(297 + 15*Power(t1,9) - 1829*t2 + 
               122*Power(t2,2) - 107*Power(t2,3) - 3403*Power(t2,4) - 
               2325*Power(t2,5) - 434*Power(t2,6) + 385*Power(t2,7) + 
               42*Power(t2,8) - 12*Power(t2,9) - 
               Power(t1,8)*(2011 + 1321*t2 + 186*Power(t2,2)) - 
               Power(t1,7)*(-4936 + 6276*t2 + 6257*Power(t2,2) + 
                  1616*Power(t2,3) + 87*Power(t2,4)) + 
               Power(t1,6)*(10471 + 63236*t2 + 47750*Power(t2,2) + 
                  11317*Power(t2,3) + 115*Power(t2,4) - 101*Power(t2,5)) \
+ Power(t1,5)*(-57480 - 169410*t2 - 124239*Power(t2,2) - 
                  21648*Power(t2,3) + 7555*Power(t2,4) + 
                  1704*Power(t2,5) + 113*Power(t2,6)) + 
               Power(t1,4)*(96332 + 215763*t2 + 155117*Power(t2,2) + 
                  6858*Power(t2,3) - 37846*Power(t2,4) - 
                  8702*Power(t2,5) + 468*Power(t2,6) + 48*Power(t2,7)) + 
               Power(t1,3)*(-81352 - 139479*t2 - 99228*Power(t2,2) + 
                  20219*Power(t2,3) + 69706*Power(t2,4) + 
                  21565*Power(t2,5) - 2629*Power(t2,6) - 
                  595*Power(t2,7) + 19*Power(t2,8)) + 
               Power(t1,2)*(35911 + 38203*t2 + 30277*Power(t2,2) - 
                  21383*Power(t2,3) - 59726*Power(t2,4) - 
                  25342*Power(t2,5) + 2688*Power(t2,6) + 
                  1580*Power(t2,7) - 44*Power(t2,8) - 8*Power(t2,9)) + 
               t1*(-7119 + 1113*t2 - 3356*Power(t2,2) + 
                  6360*Power(t2,3) + 23685*Power(t2,4) + 
                  13205*Power(t2,5) - 212*Power(t2,6) - 
                  1414*Power(t2,7) - 18*Power(t2,8) + 20*Power(t2,9))) + 
            Power(s1,8)*s2*(Power(s2,5) - 
               16*Power(-1 + t1,3)*(-2 + 3*t1) + 
               Power(s2,4)*(-6 + 7*t1) + 
               Power(s2,3)*(70 + 57*Power(t1,2) - 36*t2 + 
                  4*t1*(-32 + 9*t2)) + 
               Power(s2,2)*(80 - 41*Power(t1,3) - 156*t2 + 
                  20*t1*(-10 + 17*t2) - 8*Power(t1,2)*(-20 + 23*t2)) - 
               2*s2*(-1 + t1)*
                (-17 + 12*Power(t1,3) - 52*t2 - 
                  Power(t1,2)*(47 + 73*t2) + t1*(49 + 125*t2))) + 
            Power(s1,7)*(4*Power(s2,7) - 
               2*Power(-1 + t1,3)*
                (10 - 25*t1 + 15*Power(t1,2) + 4*Power(t1,3)) + 
               Power(s2,6)*(-95 + 103*t1 - 8*t2) + 
               Power(s2,5)*(-111 - 78*Power(t1,2) + 46*t2 - 
                  27*t1*(-7 + 2*t2)) - 
               s2*Power(-1 + t1,2)*
                (2 + 6*Power(t1,4) - 244*t2 + Power(t1,3)*(69 + 8*t2) + 
                  8*t1*(10 + 73*t2) - 4*Power(t1,2)*(46 + 87*t2)) + 
               Power(s2,4)*(-193 + 166*Power(t1,3) - 517*t2 + 
                  144*Power(t2,2) - 3*Power(t1,2)*(226 + 127*t2) + 
                  t1*(697 + 906*t2 - 144*Power(t2,2))) + 
               Power(s2,2)*(-1 + t1)*
                (466 + 53*Power(t1,4) - 272*t2 - 400*Power(t2,2) + 
                  Power(t1,3)*(-502 + 207*t2) + 
                  Power(t1,2)*(1395 - 821*t2 - 520*Power(t2,2)) + 
                  2*t1*(-693 + 419*t2 + 460*Power(t2,2))) + 
               Power(s2,3)*(-624 - 242*Power(t1,4) - 578*t2 + 
                  600*Power(t2,2) + 4*Power(t1,3)*(381 + 58*t2) + 
                  t1*(2329 + 1334*t2 - 1280*Power(t2,2)) + 
                  Power(t1,2)*(-2991 - 980*t2 + 680*Power(t2,2)))) + 
            Power(s1,6)*(6*Power(s2,8) + 
               Power(s2,6)*(-414 - 330*Power(t1,2) + 
                  t1*(748 - 713*t2) + 657*t2 + 28*Power(t2,2)) + 
               Power(s2,7)*(242*t1 - 4*(58 + 7*t2)) + 
               Power(-1 + t1,3)*
                (17*Power(t1,4) + Power(t1,3)*(-89 + 16*t2) + 
                  6*(-3 + 22*t2) + 2*Power(t1,2)*(52 + 111*t2) - 
                  2*t1*(11 + 157*t2)) + 
               Power(s2,5)*(-416 + 474*Power(t1,3) + 894*t2 - 
                  154*Power(t2,2) + Power(t1,2)*(-1480 + 719*t2) + 
                  t1*(1418 - 1613*t2 + 182*Power(t2,2))) + 
               s2*Power(-1 + t1,2)*
                (756 + 7*Power(t1,5) - 24*t2 - 812*Power(t2,2) + 
                  Power(t1,4)*(-72 + 17*t2) + 
                  Power(t1,2)*(2578 - 1527*t2 - 1052*Power(t2,2)) + 
                  Power(t1,3)*(-653 + 535*t2 + 36*Power(t2,2)) + 
                  2*t1*(-1251 + 405*t2 + 914*Power(t2,2))) + 
               Power(s2,4)*(-1002 - 421*Power(t1,4) + 
                  Power(t1,3)*(1937 - 1395*t2) + 1559*t2 + 
                  1683*Power(t2,2) - 336*Power(t2,3) + 
                  3*Power(t1,2)*(-1293 + 1836*t2 + 365*Power(t2,2)) + 
                  t1*(3355 - 5616*t2 - 2806*Power(t2,2) + 
                     336*Power(t2,3))) + 
               Power(s2,2)*(-1 + t1)*
                (31*Power(t1,5) - Power(t1,4)*(1029 + 344*t2) + 
                  Power(t1,3)*(4864 + 3101*t2 - 705*Power(t2,2)) + 
                  t1*(4919 + 8930*t2 - 2970*Power(t2,2) - 
                     1960*Power(t2,3)) + 
                  4*(-267 - 765*t2 + 228*Power(t2,2) + 
                     224*Power(t2,3)) + 
                  Power(t1,2)*
                   (-7673 - 8809*t2 + 2931*Power(t2,2) + 
                     1064*Power(t2,3))) + 
               Power(s2,3)*(-440 - 9*Power(t1,5) + 4297*t2 + 
                  1870*Power(t2,2) - 1344*Power(t2,3) + 
                  2*Power(t1,4)*(368 + 857*t2) - 
                  2*Power(t1,3)*(987 + 5309*t2 + 276*Power(t2,2)) + 
                  Power(t1,2)*
                   (1172 + 20810*t2 + 2596*Power(t2,2) - 
                     1456*Power(t2,3)) + 
                  t1*(509 - 16175*t2 - 3942*Power(t2,2) + 
                     2800*Power(t2,3)))) + 
            Power(s1,5)*(4*Power(s2,9) + 
               Power(s2,8)*(-238 + 238*t1 - 36*t2) - 
               2*Power(s2,7)*
                (67 + 134*Power(t1,2) - 690*t2 - 42*Power(t2,2) + 
                  15*t1*(-13 + 48*t2)) - 
               Power(-1 + t1,3)*
                (-374 + 6*Power(t1,5) - 76*t2 + 372*Power(t2,2) + 
                  Power(t1,4)*(42 + 64*t2) + 
                  Power(t1,3)*(245 - 336*t2 - 12*Power(t2,2)) + 
                  3*Power(t1,2)*(-433 + 208*t2 + 194*Power(t2,2)) - 
                  2*t1*(-662 + 162*t2 + 387*Power(t2,2))) + 
               Power(s2,6)*(1331 + 262*Power(t1,3) + 2650*t2 - 
                  1947*Power(t2,2) - 56*Power(t2,3) + 
                  Power(t1,2)*(671 + 2186*t2) + 
                  t1*(-2264 - 4860*t2 + 2115*Power(t2,2))) + 
               s2*Power(-1 + t1,2)*
                (-336 + 180*Power(t1,5) - 4332*t2 + 78*Power(t2,2) + 
                  1552*Power(t2,3) - 
                  4*Power(t1,4)*(540 + 6*t2 + 5*Power(t2,2)) + 
                  t1*(3229 + 14480*t2 - 2827*Power(t2,2) - 
                     3220*Power(t2,3)) + 
                  Power(t1,3)*
                   (7265 + 4276*t2 - 1536*Power(t2,2) - 60*Power(t2,3)) \
+ 12*Power(t1,2)*(-666 - 1257*t2 + 406*Power(t2,2) + 144*Power(t2,3))) - 
               Power(s2,5)*(-2439 + 24*Power(t1,4) - 2830*t2 + 
                  2952*Power(t2,2) - 294*Power(t2,3) + 
                  Power(t1,3)*(2614 + 3094*t2) + 
                  Power(t1,2)*(-7875 - 9778*t2 + 2595*Power(t2,2)) + 
                  t1*(7664 + 9490*t2 - 5547*Power(t2,2) + 
                     350*Power(t2,3))) + 
               Power(s2,4)*(3729 - 330*Power(t1,5) + 5646*t2 - 
                  5004*Power(t2,2) - 3149*Power(t2,3) + 
                  504*Power(t2,4) + Power(t1,4)*(5730 + 2414*t2) + 
                  Power(t1,3)*(-20101 - 11002*t2 + 4659*Power(t2,2)) + 
                  Power(t1,2)*
                   (27923 + 22028*t2 - 18063*Power(t2,2) - 
                     1749*Power(t2,3)) + 
                  t1*(-16951 - 19026*t2 + 18240*Power(t2,2) + 
                     4954*Power(t2,3) - 504*Power(t2,4))) + 
               Power(s2,3)*(5363 + 58*Power(t1,6) + 1782*t2 - 
                  12417*Power(t2,2) - 3522*Power(t2,3) + 
                  1932*Power(t2,4) + Power(t1,5)*(-2856 + 302*t2) + 
                  Power(t1,4)*(15691 - 5742*t2 - 5083*Power(t2,2)) + 
                  Power(t1,3)*
                   (-37209 + 15796*t2 + 31017*Power(t2,2) + 
                     696*Power(t2,3)) + 
                  t1*(-24639 + 590*t2 + 47170*Power(t2,2) + 
                     6718*Power(t2,3) - 3920*Power(t2,4)) + 
                  Power(t1,2)*
                   (43588 - 12692*t2 - 60771*Power(t2,2) - 
                     3836*Power(t2,3) + 1988*Power(t2,4))) + 
               Power(s2,2)*(-1 + t1)*
                (-2892 + 60*Power(t1,6) + 6566*t2 + 8637*Power(t2,2) - 
                  1704*Power(t2,3) - 1288*Power(t2,4) - 
                  Power(t1,5)*(577 + 372*t2) + 
                  2*Power(t1,4)*(1234 + 3389*t2 + 510*Power(t2,2)) + 
                  Power(t1,3)*
                   (-663 - 30412*t2 - 8190*Power(t2,2) + 
                     1267*Power(t2,3)) + 
                  Power(t1,2)*
                   (-7143 + 47788*t2 + 23667*Power(t2,2) - 
                     5729*Power(t2,3) - 1372*Power(t2,4)) + 
                  t1*(8783 - 30612*t2 - 24588*Power(t2,2) + 
                     5830*Power(t2,3) + 2660*Power(t2,4)))) + 
            Power(s1,4)*(Power(s2,10) + 
               Power(s2,9)*(-106 + 103*t1 - 20*t2) + 
               Power(s2,8)*(254 + 3*Power(t1,2) + 1182*t2 + 
                  90*Power(t2,2) - 2*t1*(128 + 591*t2)) + 
               Power(s2,6)*(476 + 422*Power(t1,4) - 6373*t2 - 
                  6971*Power(t2,2) + 3205*Power(t2,3) + 
                  70*Power(t2,4) - 3*Power(t1,3)*(1387 + 448*t2) + 
                  Power(t1,2)*(7451 - 2989*t2 - 5911*Power(t2,2)) + 
                  t1*(-4193 + 10706*t2 + 12942*Power(t2,2) - 
                     3485*Power(t2,3))) + 
               Power(s2,7)*(-167*Power(t1,3) + 
                  Power(t1,2)*(2147 + 1460*t2) + 
                  10*t1*(-361 - 219*t2 + 357*Power(t2,2)) + 
                  5*(327 + 158*t2 - 684*Power(t2,2) - 28*Power(t2,3))) + 
               Power(-1 + t1,3)*
                (Power(t1,5)*(144 + 35*t2) + 
                  Power(t1,4)*(-1274 - 43*t2 + 78*Power(t2,2)) + 
                  t1*(441 + 6690*t2 - 1285*Power(t2,2) - 
                     994*Power(t2,3)) + 
                  Power(t1,3)*
                   (3866 + 1484*t2 - 518*Power(t2,2) - 48*Power(t2,3)) \
+ 2*(89 - 903*t2 - 93*Power(t2,2) + 304*Power(t2,3)) + 
                  Power(t1,2)*
                   (-3211 - 6640*t2 + 1791*Power(t2,2) + 
                     714*Power(t2,3))) - 
               Power(s2,5)*(3755 + 442*Power(t1,5) + 12773*t2 + 
                  7636*Power(t2,2) - 5241*Power(t2,3) + 
                  350*Power(t2,4) + Power(t1,4)*(-4685 + 356*t2) - 
                  5*Power(t1,3)*(-1329 + 2918*t2 + 1661*Power(t2,2)) + 
                  Power(t1,2)*
                   (2778 + 41565*t2 + 26306*Power(t2,2) - 
                     4926*Power(t2,3)) - 
                  t1*(8934 + 40044*t2 + 25577*Power(t2,2) - 
                     10167*Power(t2,3) + 420*Power(t2,4))) + 
               s2*Power(-1 + t1,2)*
                (-3782 + 35*Power(t1,6) + 2139*t2 + 10058*Power(t2,2) - 
                  15*Power(t2,3) - 1880*Power(t2,4) - 
                  2*Power(t1,5)*(486 + 420*t2 + 25*Power(t2,2)) + 
                  Power(t1,4)*
                   (3179 + 9551*t2 + 724*Power(t2,2) + 30*Power(t2,3)) \
+ Power(t1,2)*(-15634 + 40454*t2 + 34666*Power(t2,2) - 
                     8135*Power(t2,3) - 1680*Power(t2,4)) + 
                  Power(t1,3)*
                   (3716 - 34713*t2 - 10070*Power(t2,2) + 
                     2200*Power(t2,3) + 40*Power(t2,4)) + 
                  t1*(13602 - 17521*t2 - 33618*Power(t2,2) + 
                     4975*Power(t2,3) + 3520*Power(t2,4))) + 
               Power(s2,4)*(-5726 - 60*Power(t1,6) - 20038*t2 - 
                  13271*Power(t2,2) + 8440*Power(t2,3) + 
                  3695*Power(t2,4) - 504*Power(t2,5) + 
                  Power(t1,5)*(600 + 2458*t2) - 
                  5*Power(t1,4)*(2611 + 6295*t2 + 1237*Power(t2,2)) + 
                  Power(t1,3)*
                   (42560 + 105855*t2 + 27121*Power(t2,2) - 
                     8164*Power(t2,3)) + 
                  Power(t1,2)*
                   (-53558 - 146553*t2 - 53068*Power(t2,2) + 
                     31443*Power(t2,3) + 1665*Power(t2,4)) + 
                  t1*(29242 + 89753*t2 + 45253*Power(t2,2) - 
                     31439*Power(t2,3) - 5430*Power(t2,4) + 
                     504*Power(t2,5))) - 
               Power(s2,2)*(-1 + t1)*
                (-8540 - 13021*t2 + 15546*Power(t2,2) + 
                  13617*Power(t2,3) - 1980*Power(t2,4) - 
                  1232*Power(t2,5) + Power(t1,6)*(-743 + 90*t2) + 
                  Power(t1,5)*(8711 - 1445*t2 - 1048*Power(t2,2)) + 
                  Power(t1,4)*
                   (-39272 + 8940*t2 + 16381*Power(t2,2) + 
                     1697*Power(t2,3)) + 
                  Power(t1,3)*
                   (82634 - 1210*t2 - 73868*Power(t2,2) - 
                     11733*Power(t2,3) + 1325*Power(t2,4)) - 
                  Power(t1,2)*
                   (86350 + 33344*t2 - 116951*Power(t2,2) - 
                     34797*Power(t2,3) + 6835*Power(t2,4) + 
                     1148*Power(t2,5)) + 
                  t1*(43546 + 40170*t2 - 74622*Power(t2,2) - 
                     37468*Power(t2,3) + 7070*Power(t2,4) + 
                     2380*Power(t2,5))) + 
               Power(s2,3)*(-7700 + 140*Power(t1,7) - 26566*t2 - 
                  3512*Power(t2,2) + 19563*Power(t2,3) + 
                  4190*Power(t2,4) - 1848*Power(t2,5) - 
                  Power(t1,6)*(3295 + 956*t2) + 
                  Power(t1,5)*(28015 + 17036*t2 - 667*Power(t2,2)) + 
                  Power(t1,4)*
                   (-94473 - 82950*t2 + 14314*Power(t2,2) + 
                     8080*Power(t2,3)) + 
                  Power(t1,3)*
                   (152092 + 190349*t2 - 42465*Power(t2,2) - 
                     48963*Power(t2,3) - 450*Power(t2,4)) - 
                  4*Power(t1,2)*
                   (31263 + 55111*t2 - 9526*Power(t2,2) - 
                     24096*Power(t2,3) - 835*Power(t2,4) + 
                     448*Power(t2,5)) + 
                  t1*(50272 + 123551*t2 - 5864*Power(t2,2) - 
                     74924*Power(t2,3) - 7150*Power(t2,4) + 
                     3640*Power(t2,5)))) + 
            Power(s1,3)*(Power(s2,10)*(-15 + 15*t1 - 4*t2) + 
               Power(s2,9)*(135 + 68*Power(t1,2) + 422*t2 + 
                  40*Power(t2,2) - t1*(203 + 410*t2)) + 
               Power(s2,8)*(382 - 174*Power(t1,3) - 985*t2 - 
                  2348*Power(t2,2) - 120*Power(t2,3) + 
                  11*Power(t1,2)*(77 + t2) + 
                  t1*(-1055 + 970*t2 + 2348*Power(t2,2))) + 
               Power(s2,7)*(-1661 + 302*Power(t1,4) + 
                  804*Power(t1,3)*(-1 + t2) - 6506*t2 - 
                  1829*Power(t2,2) + 4520*Power(t2,3) + 
                  140*Power(t2,4) - 
                  Power(t1,2)*(1082 + 8800*t2 + 3169*Power(t2,2)) + 
                  t1*(3245 + 14482*t2 + 4878*Power(t2,2) - 
                     4720*Power(t2,3))) - 
               Power(-1 + t1,3)*
                (1529 + 28*Power(t1,6) + 439*t2 - 3175*Power(t2,2) - 
                  413*Power(t2,3) + 652*Power(t2,4) + 
                  Power(t1,5)*(368 + 353*t2 + 60*Power(t2,2)) + 
                  Power(t1,4)*
                   (-1448 - 3425*t2 - 254*Power(t2,2) + 20*Power(t2,3)) \
+ t1*(-6237 + 1903*t2 + 12549*Power(t2,2) - 2141*Power(t2,3) - 
                     766*Power(t2,4)) + 
                  Power(t1,3)*
                   (-3082 + 12774*t2 + 2517*Power(t2,2) - 
                     447*Power(t2,3) - 32*Power(t2,4)) + 
                  Power(t1,2)*
                   (8706 - 11468*t2 - 12257*Power(t2,2) + 
                     2821*Power(t2,3) + 426*Power(t2,4))) + 
               Power(s2,6)*(-4448 - 195*Power(t1,5) - 2814*t2 + 
                  12066*Power(t2,2) + 9640*Power(t2,3) - 
                  3165*Power(t2,4) - 56*Power(t2,5) - 
                  Power(t1,4)*(871 + 2319*t2) + 
                  Power(t1,3)*(10857 + 18608*t2 + 3046*Power(t2,2)) + 
                  Power(t1,2)*
                   (-22630 - 32813*t2 + 4550*Power(t2,2) + 
                     8360*Power(t2,3)) + 
                  t1*(17287 + 19358*t2 - 19662*Power(t2,2) - 
                     18080*Power(t2,3) + 3445*Power(t2,4))) + 
               Power(s2,5)*(108 - 152*Power(t1,6) + 12554*t2 + 
                  25828*Power(t2,2) + 10444*Power(t2,3) - 
                  5439*Power(t2,4) + 266*Power(t2,5) + 
                  Power(t1,5)*(4727 + 2860*t2) + 
                  Power(t1,4)*(-28061 - 22652*t2 + 630*Power(t2,2)) + 
                  Power(t1,3)*
                   (55027 + 35104*t2 - 28580*Power(t2,2) - 
                     11580*Power(t2,3)) + 
                  Power(t1,2)*
                   (-44072 - 948*t2 + 82458*Power(t2,2) + 
                     36564*Power(t2,3) - 5404*Power(t2,4)) - 
                  t1*(-12423 + 26914*t2 + 80216*Power(t2,2) + 
                     35348*Power(t2,3) - 10843*Power(t2,4) + 
                     322*Power(t2,5))) + 
               s2*Power(-1 + t1,2)*
                (Power(t1,6)*(1063 + 96*t2) + 
                  4*Power(t1,5)*
                   (-2572 + 291*t2 + 333*Power(t2,2) + 20*Power(t2,3)) \
+ Power(t1,3)*(-82693 - 21332*t2 + 62646*Power(t2,2) + 
                     10974*Power(t2,3) - 1725*Power(t2,4)) - 
                  Power(t1,4)*
                   (-44342 + 6150*t2 + 15263*Power(t2,2) + 
                     1294*Power(t2,3) + 50*Power(t2,4)) + 
                  2*(2496 + 7112*t2 - 2015*Power(t2,2) - 
                     6156*Power(t2,3) - 85*Power(t2,4) + 
                     746*Power(t2,5)) + 
                  Power(t1,2)*
                   (73579 + 64546*t2 - 76729*Power(t2,2) - 
                     40298*Power(t2,3) + 7920*Power(t2,4) + 
                     988*Power(t2,5)) - 
                  2*t1*(15472 + 26562*t2 - 16952*Power(t2,2) - 
                     20285*Power(t2,3) + 2515*Power(t2,4) + 
                     1240*Power(t2,5))) + 
               Power(s2,4)*(6531 + 136*Power(t1,7) + 21980*t2 + 
                  40285*Power(t2,2) + 16427*Power(t2,3) - 
                  8145*Power(t2,4) - 2775*Power(t2,5) + 
                  336*Power(t2,6) - Power(t1,6)*(4433 + 730*t2) + 
                  Power(t1,5)*(27392 + 1912*t2 - 5262*Power(t2,2)) + 
                  Power(t1,4)*
                   (-52139 + 39671*t2 + 62807*Power(t2,2) + 
                     8619*Power(t2,3)) + 
                  2*Power(t1,3)*
                   (14329 - 73760*t2 - 105250*Power(t2,2) - 
                     17852*Power(t2,3) + 4078*Power(t2,4)) + 
                  Power(t1,2)*
                   (17524 + 193061*t2 + 292884*Power(t2,2) + 
                     67538*Power(t2,3) - 31572*Power(t2,4) - 
                     927*Power(t2,5)) - 
                  t1*(23669 + 108386*t2 + 180214*Power(t2,2) + 
                     56680*Power(t2,3) - 31281*Power(t2,4) - 
                     3758*Power(t2,5) + 336*Power(t2,6))) + 
               Power(s2,3)*(6023 + Power(t1,7)*(692 - 224*t2) + 
                  32660*t2 + 50109*Power(t2,2) + 4678*Power(t2,3) - 
                  18182*Power(t2,4) - 3190*Power(t2,5) + 
                  1176*Power(t2,6) + 
                  Power(t1,6)*(-1377 + 11268*t2 + 2587*Power(t2,2)) + 
                  2*Power(t1,5)*
                   (-14803 - 51166*t2 - 18186*Power(t2,2) + 
                     83*Power(t2,3)) - 
                  2*Power(t1,4)*
                   (-61878 - 176145*t2 - 83443*Power(t2,2) + 
                     8159*Power(t2,3) + 3650*Power(t2,4)) + 
                  2*Power(t1,3)*
                   (-98077 - 288726*t2 - 187773*Power(t2,2) + 
                     27494*Power(t2,3) + 22351*Power(t2,4) + 
                     28*Power(t2,5)) + 
                  t1*(-51521 - 201894*t2 - 238004*Power(t2,2) + 
                     9174*Power(t2,3) + 69981*Power(t2,4) + 
                     4770*Power(t2,5) - 2240*Power(t2,6)) + 
                  Power(t1,2)*
                   (148187 + 485688*t2 + 430300*Power(t2,2) - 
                     52568*Power(t2,3) - 89341*Power(t2,4) - 
                     1580*Power(t2,5) + 1064*Power(t2,6))) + 
               Power(s2,2)*(-1 + t1)*
                (-5617 + 196*Power(t1,7) - 34757*t2 - 
                  23306*Power(t2,2) + 18304*Power(t2,3) + 
                  12988*Power(t2,4) - 1504*Power(t2,5) - 
                  784*Power(t2,6) - 
                  2*Power(t1,6)*(3143 + 1730*t2 + 50*Power(t2,2)) + 
                  Power(t1,5)*
                   (50309 + 35512*t2 - 147*Power(t2,2) - 
                     1205*Power(t2,3)) + 
                  Power(t1,4)*
                   (-155236 - 153760*t2 + 10399*Power(t2,2) + 
                     18991*Power(t2,3) + 1613*Power(t2,4)) + 
                  Power(t1,3)*
                   (224625 + 323454*t2 + 3441*Power(t2,2) - 
                     90375*Power(t2,3) - 9482*Power(t2,4) + 
                     829*Power(t2,5)) - 
                  Power(t1,2)*
                   (160806 + 341806*t2 + 63499*Power(t2,2) - 
                     145741*Power(t2,3) - 29873*Power(t2,4) + 
                     5199*Power(t2,5) + 616*Power(t2,6)) + 
                  t1*(52817 + 174761*t2 + 73572*Power(t2,2) - 
                     92336*Power(t2,3) - 34082*Power(t2,4) + 
                     5538*Power(t2,5) + 1400*Power(t2,6)))) + 
            Power(s1,2)*(Power(s2,10)*
                (14 + 14*Power(t1,2) + 45*t2 + 6*Power(t2,2) - 
                  t1*(28 + 45*t2)) - 
               Power(s2,9)*(-1 + 30*Power(t1,3) + 408*t2 + 
                  630*Power(t2,2) + 40*Power(t2,3) + 
                  Power(t1,2)*(-61 + 207*t2) + 
                  t1*(32 - 615*t2 - 612*Power(t2,2))) + 
               Power(s2,8)*(-533 + 63*Power(t1,4) - 1180*t2 + 
                  1410*Power(t2,2) + 2332*Power(t2,3) + 
                  90*Power(t2,4) + Power(t1,3)*(244 + 597*t2) - 
                  Power(t1,2)*(1210 + 2725*t2 + 72*Power(t2,2)) - 
                  4*t1*(-359 - 827*t2 + 333*Power(t2,2) + 
                     583*Power(t2,3))) + 
               Power(s2,7)*(-320 - 21*Power(t1,5) + 4664*t2 + 
                  9590*Power(t2,2) + 2069*Power(t2,3) - 
                  3360*Power(t2,4) - 84*Power(t2,5) - 
                  Power(t1,4)*(1251 + 1198*t2) + 
                  Power(t1,3)*(4717 + 3390*t2 - 1205*Power(t2,2)) + 
                  Power(t1,2)*
                   (-5917 + 1847*t2 + 12990*Power(t2,2) + 
                     3409*Power(t2,3)) + 
                  t1*(2792 - 8703*t2 - 21345*Power(t2,2) - 
                     5358*Power(t2,3) + 3510*Power(t2,4))) + 
               Power(-1 + t1,3)*
                (703 + 4470*t2 + 350*Power(t2,2) - 2530*Power(t2,3) - 
                  613*Power(t2,4) + 468*Power(t2,5) + 
                  Power(t1,6)*(390 + 99*t2) + 
                  Power(t1,5)*
                   (-3981 + 121*t2 + 283*Power(t2,2) + 39*Power(t2,3)) \
+ Power(t1,3)*(-31559 - 11237*t2 + 16132*Power(t2,2) + 
                     1492*Power(t2,3) - 258*Power(t2,4)) - 
                  Power(t1,4)*
                   (-18592 + 2002*t2 + 3202*Power(t2,2) + 
                     200*Power(t2,3) + 23*Power(t2,4)) + 
                  Power(t1,2)*
                   (23183 + 26624*t2 - 15688*Power(t2,2) - 
                     10500*Power(t2,3) + 2397*Power(t2,4) + 
                     114*Power(t2,5)) - 
                  t1*(7272 + 18483*t2 - 2989*Power(t2,2) - 
                     11139*Power(t2,3) + 1623*Power(t2,4) + 
                     414*Power(t2,5))) + 
               Power(s2,6)*(3865 - 83*Power(t1,6) + 12985*t2 + 
                  5388*Power(t2,2) - 11360*Power(t2,3) - 
                  7384*Power(t2,4) + 1875*Power(t2,5) + 
                  28*Power(t2,6) + 2*Power(t1,5)*(1356 + 577*t2) + 
                  Power(t1,4)*(-7914 + 530*t2 + 3851*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (-1699 + 14407*t2 + 14559*Power(t2,2) + 
                     1781*Power(t2,3)) + 
                  Power(t1,2)*
                   (10378 + 63990*t2 + 51777*Power(t2,2) - 
                     2780*Power(t2,3) - 6524*Power(t2,4)) - 
                  t1*(12356 + 49845*t2 + 31928*Power(t2,2) - 
                     17702*Power(t2,3) - 13968*Power(t2,4) + 
                     2043*Power(t2,5))) + 
               Power(s2,5)*(5344 + 57*Power(t1,7) + 2171*t2 - 
                  14989*Power(t2,2) - 25300*Power(t2,3) - 
                  7576*Power(t2,4) + 3312*Power(t2,5) - 
                  126*Power(t2,6) - Power(t1,6)*(2027 + 105*t2) - 
                  3*Power(t1,5)*(-263 + 4053*t2 + 1760*Power(t2,2)) + 
                  Power(t1,4)*
                   (28193 + 80531*t2 + 38283*Power(t2,2) + 
                     220*Power(t2,3)) + 
                  Power(t1,3)*
                   (-74031 - 163801*t2 - 64616*Power(t2,2) + 
                     25172*Power(t2,3) + 8740*Power(t2,4)) + 
                  Power(t1,2)*
                   (75464 + 137824*t2 + 20100*Power(t2,2) - 
                     77664*Power(t2,3) - 27436*Power(t2,4) + 
                     3459*Power(t2,5)) + 
                  t1*(-33789 - 44461*t2 + 26496*Power(t2,2) + 
                     77452*Power(t2,3) + 26212*Power(t2,4) - 
                     6771*Power(t2,5) + 154*Power(t2,6))) + 
               s2*Power(-1 + t1,2)*
                (3*Power(t1,7) - 
                  Power(t1,6)*(4506 + 2719*t2 + 321*Power(t2,2)) + 
                  Power(t1,5)*
                   (41057 + 24422*t2 + 957*Power(t2,2) - 
                     830*Power(t2,3) - 45*Power(t2,4)) + 
                  Power(t1,4)*
                   (-117953 - 111253*t2 + 3710*Power(t2,2) + 
                     10753*Power(t2,3) + 858*Power(t2,4) + 
                     41*Power(t2,5)) + 
                  Power(t1,3)*
                   (151892 + 217251*t2 + 33707*Power(t2,2) - 
                     54124*Power(t2,3) - 5467*Power(t2,4) + 
                     759*Power(t2,5) - 12*Power(t2,6)) - 
                  Power(t1,2)*
                   (92349 + 201920*t2 + 90579*Power(t2,2) - 
                     69545*Power(t2,3) - 25068*Power(t2,4) + 
                     4629*Power(t2,5) + 348*Power(t2,6)) - 
                  2*(192 + 7991*t2 + 9178*Power(t2,2) - 
                     1432*Power(t2,3) - 4284*Power(t2,4) - 
                     111*Power(t2,5) + 382*Power(t2,6)) + 
                  2*t1*(11123 + 45024*t2 + 35873*Power(t2,2) - 
                     15034*Power(t2,3) - 13636*Power(t2,4) + 
                     1520*Power(t2,5) + 562*Power(t2,6))) + 
               Power(s2,4)*(-2412 + Power(t1,7)*(90 - 198*t2) - 
                  13511*t2 - 29118*Power(t2,2) - 37767*Power(t2,3) - 
                  11085*Power(t2,4) + 4503*Power(t2,5) + 
                  1297*Power(t2,6) - 144*Power(t2,7) + 
                  Power(t1,6)*(12537 + 13541*t2 + 2205*Power(t2,2)) + 
                  Power(t1,5)*
                   (-76409 - 86449*t2 - 10073*Power(t2,2) + 
                     4515*Power(t2,3)) + 
                  Power(t1,4)*
                   (168210 + 177623*t2 - 33576*Power(t2,2) - 
                     57139*Power(t2,3) - 6572*Power(t2,4)) + 
                  Power(t1,3)*
                   (-171448 - 135233*t2 + 171246*Power(t2,2) + 
                     196978*Power(t2,3) + 25351*Power(t2,4) - 
                     4635*Power(t2,5)) + 
                  Power(t1,2)*
                   (78265 + 3032*t2 - 240109*Power(t2,2) - 
                     277514*Power(t2,3) - 46301*Power(t2,4) + 
                     18306*Power(t2,5) + 261*Power(t2,6)) + 
                  t1*(-8833 + 41195*t2 + 139443*Power(t2,2) + 
                     170927*Power(t2,3) + 38457*Power(t2,4) - 
                     18006*Power(t2,5) - 1586*Power(t2,6) + 
                     144*Power(t2,7))) + 
               Power(s2,2)*(-1 + t1)*
                (1531 + 18083*t2 + 47499*Power(t2,2) + 
                  20443*Power(t2,3) - 11276*Power(t2,4) - 
                  7518*Power(t2,5) + 752*Power(t2,6) + 
                  320*Power(t2,7) + Power(t1,7)*(1825 + 27*t2) + 
                  Power(t1,6)*
                   (-3964 + 12121*t2 + 4983*Power(t2,2) + 
                     249*Power(t2,3)) + 
                  Power(t1,5)*
                   (-38418 - 113982*t2 - 48639*Power(t2,2) - 
                     1975*Power(t2,3) + 582*Power(t2,4)) + 
                  Power(t1,4)*
                   (150134 + 372307*t2 + 209289*Power(t2,2) - 
                     4777*Power(t2,3) - 11032*Power(t2,4) - 
                     810*Power(t2,5)) - 
                  Power(t1,3)*
                   (211477 + 557359*t2 + 444357*Power(t2,2) + 
                     6058*Power(t2,3) - 59077*Power(t2,4) - 
                     3975*Power(t2,5) + 307*Power(t2,6)) + 
                  Power(t1,2)*
                   (136456 + 413099*t2 + 472632*Power(t2,2) + 
                     57443*Power(t2,3) - 98302*Power(t2,4) - 
                     14667*Power(t2,5) + 2521*Power(t2,6) + 
                     200*Power(t2,7)) - 
                  t1*(36087 + 144302*t2 + 241323*Power(t2,2) + 
                     65685*Power(t2,3) - 61611*Power(t2,4) - 
                     18474*Power(t2,5) + 2798*Power(t2,6) + 
                     520*Power(t2,7))) + 
               Power(s2,3)*(-5943 + 219*Power(t1,8) - 14660*t2 - 
                  45751*Power(t2,2) - 44297*Power(t2,3) - 
                  4182*Power(t2,4) + 9987*Power(t2,5) + 
                  1498*Power(t2,6) - 480*Power(t2,7) - 
                  Power(t1,7)*(10275 + 3712*t2 + 81*Power(t2,2)) + 
                  Power(t1,6)*
                   (59531 + 16745*t2 - 12282*Power(t2,2) - 
                     2639*Power(t2,3)) + 
                  Power(t1,5)*
                   (-124597 + 34720*t2 + 127682*Power(t2,2) + 
                     34764*Power(t2,3) + 619*Power(t2,4)) + 
                  Power(t1,4)*
                   (99279 - 244456*t2 - 459263*Power(t2,2) - 
                     156388*Power(t2,3) + 9450*Power(t2,4) + 
                     3622*Power(t2,5)) + 
                  2*Power(t1,3)*
                   (5227 + 213722*t2 + 384986*Power(t2,2) + 
                     175223*Power(t2,3) - 19262*Power(t2,4) - 
                     11616*Power(t2,5) + 64*Power(t2,6)) - 
                  2*Power(t1,2)*
                   (31919 + 168207*t2 + 328876*Power(t2,2) + 
                     199481*Power(t2,3) - 19210*Power(t2,4) - 
                     24045*Power(t2,5) - 110*Power(t2,6) + 
                     200*Power(t2,7)) + 
                  t1*(35170 + 120333*t2 + 277469*Power(t2,2) + 
                     217116*Power(t2,3) - 5873*Power(t2,4) - 
                     38383*Power(t2,5) - 1874*Power(t2,6) + 
                     880*Power(t2,7)))) + 
            s1*(-(Power(s2,10)*t2*
                  (28 + 28*Power(t1,2) + 45*t2 + 4*Power(t2,2) - 
                    t1*(56 + 45*t2))) + 
               Power(s2,9)*(-36 + 4*Power(t1,4) - 4*t2 + 
                  411*Power(t2,2) + 418*Power(t2,3) + 20*Power(t2,4) + 
                  Power(t1,3)*(24 + 62*t2) + 
                  2*Power(t1,2)*(-48 - 64*t2 + 105*Power(t2,2)) + 
                  t1*(104 + 70*t2 - 621*Power(t2,2) - 406*Power(t2,3))) + 
               Power(s2,8)*(47 + 1054*t2 + 1205*Power(t2,2) - 
                  881*Power(t2,3) - 1158*Power(t2,4) - 36*Power(t2,5) - 
                  2*Power(t1,4)*(71 + 73*t2) + 
                  Power(t1,3)*(379 - 416*t2 - 663*Power(t2,2)) + 
                  Power(t1,2)*
                   (-285 + 2324*t2 + 2882*Power(t2,2) + 99*Power(t2,3)) \
+ t1*(1 - 2816*t2 - 3424*Power(t2,2) + 778*Power(t2,3) + 
                     1158*Power(t2,4))) + 
               Power(s2,7)*(721 - 12*Power(t1,6) + 660*t2 - 
                  4271*Power(t2,2) - 6210*Power(t2,3) - 
                  1141*Power(t2,4) + 1332*Power(t2,5) + 28*Power(t2,6) + 
                  2*Power(t1,5)*(181 + 57*t2) + 
                  Power(t1,4)*(-328 + 2286*t2 + 1481*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1789 - 9238*t2 - 4415*Power(t2,2) + 
                     674*Power(t2,3)) + 
                  Power(t1,2)*
                   (3895 + 11822*t2 - 253*Power(t2,2) - 
                     8192*Power(t2,3) - 1811*Power(t2,4)) + 
                  t1*(-2849 - 5644*t2 + 7458*Power(t2,2) + 
                     13708*Power(t2,3) + 2892*Power(t2,4) - 
                     1392*Power(t2,5))) - 
               Power(-1 + t1,3)*
                (-751 + 40*Power(t1,7) + 1815*t2 + 3484*Power(t2,2) + 
                  180*Power(t2,3) - 889*Power(t2,4) - 459*Power(t2,5) + 
                  204*Power(t2,6) + 
                  2*Power(t1,6)*(610 + 247*t2 + 45*Power(t2,2)) + 
                  Power(t1,5)*
                   (-13372 - 3759*t2 - 326*Power(t2,2) + 
                     57*Power(t2,3) + 8*Power(t2,4)) - 
                  Power(t1,4)*
                   (-34028 - 23133*t2 + 1700*Power(t2,2) + 
                     1113*Power(t2,3) + 20*Power(t2,4) + 12*Power(t2,5)) \
+ Power(t1,3)*(-36459 - 43236*t2 - 9221*Power(t2,2) + 
                     9141*Power(t2,3) + 144*Power(t2,4) - 
                     101*Power(t2,5) + 4*Power(t2,6)) + 
                  Power(t1,2)*
                   (16945 + 33822*t2 + 22343*Power(t2,2) - 
                     9433*Power(t2,3) - 4294*Power(t2,4) + 
                     999*Power(t2,5) + 18*Power(t2,6)) - 
                  t1*(1659 + 12157*t2 + 15078*Power(t2,2) - 
                     1744*Power(t2,3) - 4771*Power(t2,4) + 
                     475*Power(t2,5) + 170*Power(t2,6))) + 
               Power(s2,6)*(-154 + 8*Power(t1,7) - 7065*t2 - 
                  12404*Power(t2,2) - 4175*Power(t2,3) + 
                  5359*Power(t2,4) + 2966*Power(t2,5) - 
                  617*Power(t2,6) - 8*Power(t2,7) + 
                  10*Power(t1,6)*(-25 + 7*t2) - 
                  5*Power(t1,5)*(401 + 1082*t2 + 350*Power(t2,2)) + 
                  Power(t1,4)*
                   (10334 + 17035*t2 + 2117*Power(t2,2) - 
                     2422*Power(t2,3)) + 
                  Power(t1,3)*
                   (-16438 - 10206*t2 + 23305*Power(t2,2) + 
                     18960*Power(t2,3) + 2052*Power(t2,4)) + 
                  Power(t1,2)*
                   (10942 - 16468*t2 - 57948*Power(t2,2) - 
                     34697*Power(t2,3) + 511*Power(t2,4) + 
                     2662*Power(t2,5)) + 
                  t1*(-2437 + 22044*t2 + 46680*Power(t2,2) + 
                     22354*Power(t2,3) - 7922*Power(t2,4) - 
                     5652*Power(t2,5) + 673*Power(t2,6))) - 
               Power(s2,5)*(3819 + 9830*t2 + 4356*Power(t2,2) - 
                  7576*Power(t2,3) - 12013*Power(t2,4) - 
                  2710*Power(t2,5) + 1098*Power(t2,6) - 34*Power(t2,7) + 
                  Power(t1,7)*(74 + 72*t2) - 
                  3*Power(t1,6)*(1733 + 1630*t2 + 233*Power(t2,2)) + 
                  Power(t1,5)*
                   (18579 + 7344*t2 - 9026*Power(t2,2) - 
                     3660*Power(t2,3)) + 
                  Power(t1,4)*
                   (-19119 + 40602*t2 + 72873*Power(t2,2) + 
                     26674*Power(t2,3) + 786*Power(t2,4)) + 
                  Power(t1,3)*
                   (-7056 - 125458*t2 - 156529*Power(t2,2) - 
                     48760*Power(t2,3) + 9894*Power(t2,4) + 
                     3334*Power(t2,5)) + 
                  Power(t1,2)*
                   (26523 + 133582*t2 + 138877*Power(t2,2) + 
                     24200*Power(t2,3) - 34839*Power(t2,4) - 
                     10394*Power(t2,5) + 1203*Power(t2,6)) + 
                  t1*(-17621 - 61082*t2 - 49852*Power(t2,2) + 
                     9118*Power(t2,3) + 36112*Power(t2,4) + 
                     9746*Power(t2,5) - 2301*Power(t2,6) + 42*Power(t2,7)\
)) + s2*Power(-1 + t1,2)*(2*Power(t1,7)*(675 + 134*t2) + 
                  Power(t1,6)*
                   (-891 + 3502*t2 + 1999*Power(t2,2) + 228*Power(t2,3)) \
+ 2*Power(t1,5)*(-13944 - 24621*t2 - 8138*Power(t2,2) - 
                     680*Power(t2,3) + 62*Power(t2,4) + 4*Power(t2,5)) + 
                  Power(t1,4)*
                   (86032 + 153384*t2 + 82373*Power(t2,2) - 
                     2392*Power(t2,3) - 3121*Power(t2,4) - 
                     178*Power(t2,5) - 12*Power(t2,6)) + 
                  Power(t1,3)*
                   (-103027 - 202708*t2 - 169070*Power(t2,2) - 
                     17444*Power(t2,3) + 22837*Power(t2,4) + 
                     830*Power(t2,5) - 190*Power(t2,6) + 4*Power(t2,7)) \
+ Power(t1,2)*(54489 + 125230*t2 + 160386*Power(t2,2) + 
                     49988*Power(t2,3) - 30759*Power(t2,4) - 
                     7954*Power(t2,5) + 1552*Power(t2,6) + 
                     72*Power(t2,7)) + 
                  2*(-617 + 718*t2 + 6611*Power(t2,2) + 
                     4680*Power(t2,3) - 257*Power(t2,4) - 
                     1654*Power(t2,5) - 53*Power(t2,6) + 116*Power(t2,7)\
) - t1*(8831 + 31882*t2 + 72481*Power(t2,2) + 38956*Power(t2,3) - 
                     12363*Power(t2,4) - 9918*Power(t2,5) + 
                     1055*Power(t2,6) + 308*Power(t2,7))) + 
               Power(s2,4)*(-2590 + 80*Power(t1,8) + 1982*t2 + 
                  7427*Power(t2,2) + 15339*Power(t2,3) + 
                  16186*Power(t2,4) + 3755*Power(t2,5) - 
                  1314*Power(t2,6) - 343*Power(t2,7) + 36*Power(t2,8) - 
                  2*Power(t1,7)*(1909 + 649*t2 + 9*Power(t2,2)) - 
                  Power(t1,6)*
                   (-5767 + 16884*t2 + 12772*Power(t2,2) + 
                     1889*Power(t2,3)) + 
                  2*Power(t1,5)*
                   (15620 + 63672*t2 + 43446*Power(t2,2) + 
                     5566*Power(t2,3) - 713*Power(t2,4)) + 
                  Power(t1,4)*
                   (-110669 - 298082*t2 - 193076*Power(t2,2) + 
                     3056*Power(t2,3) + 23107*Power(t2,4) + 
                     2483*Power(t2,5)) + 
                  Power(t1,3)*
                   (145077 + 318438*t2 + 180195*Power(t2,2) - 
                     74044*Power(t2,3) - 85371*Power(t2,4) - 
                     8686*Power(t2,5) + 1371*Power(t2,6)) - 
                  Power(t1,2)*
                   (92184 + 157816*t2 + 57002*Power(t2,2) - 
                     118116*Power(t2,3) - 122973*Power(t2,4) - 
                     15394*Power(t2,5) + 5655*Power(t2,6) + 
                     15*Power(t2,7)) + 
                  t1*(27097 + 26316*t2 - 11646*Power(t2,2) - 
                     71722*Power(t2,3) - 75469*Power(t2,4) - 
                     12886*Power(t2,5) + 5542*Power(t2,6) + 
                     366*Power(t2,7) - 36*Power(t2,8))) + 
               Power(s2,2)*(-1 + t1)*
                (-2046 + 38*Power(t1,8) - 360*t2 - 14141*Power(t2,2) - 
                  24896*Power(t2,3) - 8592*Power(t2,4) + 
                  3402*Power(t2,5) + 2453*Power(t2,6) - 
                  232*Power(t2,7) - 76*Power(t2,8) - 
                  Power(t1,7)*(8682 + 4137*t2 + 488*Power(t2,2)) + 
                  Power(t1,6)*
                   (42236 + 17417*t2 - 5368*Power(t2,2) - 
                     2524*Power(t2,3) - 138*Power(t2,4)) + 
                  Power(t1,5)*
                   (-69202 + 21319*t2 + 74599*Power(t2,2) + 
                     24788*Power(t2,3) + 1496*Power(t2,4) - 
                     67*Power(t2,5)) + 
                  Power(t1,4)*
                   (27037 - 160531*t2 - 267291*Power(t2,2) - 
                     111498*Power(t2,3) + 1777*Power(t2,4) + 
                     2899*Power(t2,5) + 162*Power(t2,6)) + 
                  Power(t1,3)*
                   (43529 + 246536*t2 + 412986*Power(t2,2) + 
                     244681*Power(t2,3) + 268*Power(t2,4) - 
                     19625*Power(t2,5) - 578*Power(t2,6) + 
                     65*Power(t2,7)) - 
                  Power(t1,2)*
                   (52761 + 159040*t2 + 308989*Power(t2,2) + 
                     262483*Power(t2,3) + 22050*Power(t2,4) - 
                     34283*Power(t2,5) - 3657*Power(t2,6) + 
                     731*Power(t2,7) + 34*Power(t2,8)) + 
                  t1*(19851 + 38796*t2 + 108698*Power(t2,2) + 
                     131876*Power(t2,3) + 27419*Power(t2,4) - 
                     21156*Power(t2,5) - 5512*Power(t2,6) + 
                     850*Power(t2,7) + 110*Power(t2,8))) + 
               Power(s2,3)*(2966 + Power(t1,8)*(691 - 28*t2) + 5860*t2 + 
                  8801*Power(t2,2) + 24194*Power(t2,3) + 
                  17716*Power(t2,4) + 2108*Power(t2,5) - 
                  3009*Power(t2,6) - 390*Power(t2,7) + 114*Power(t2,8) + 
                  2*Power(t1,7)*
                   (6083 + 8527*t2 + 2396*Power(t2,2) + 126*Power(t2,3)) \
+ Power(t1,6)*(-85542 - 109832*t2 - 27625*Power(t2,2) + 
                     4194*Power(t2,3) + 1051*Power(t2,4)) - 
                  2*Power(t1,5)*
                   (-106462 - 126894*t2 - 9403*Power(t2,2) + 
                     30460*Power(t2,3) + 7138*Power(t2,4) + 
                     262*Power(t2,5)) + 
                  Power(t1,4)*
                   (-261372 - 259270*t2 + 111091*Power(t2,2) + 
                     239292*Power(t2,3) + 65463*Power(t2,4) - 
                     2908*Power(t2,5) - 839*Power(t2,6)) + 
                  Power(t1,3)*
                   (162025 + 91220*t2 - 249885*Power(t2,2) - 
                     414318*Power(t2,3) - 149605*Power(t2,4) + 
                     14808*Power(t2,5) + 6165*Power(t2,6) - 
                     88*Power(t2,7)) + 
                  Power(t1,2)*
                   (-38787 + 33110*t2 + 209348*Power(t2,2) + 
                     356842*Power(t2,3) + 170860*Power(t2,4) - 
                     15124*Power(t2,5) - 13761*Power(t2,6) + 
                     124*Power(t2,7) + 86*Power(t2,8)) - 
                  t1*(5071 + 31902*t2 + 75328*Power(t2,2) + 
                     149532*Power(t2,3) + 91229*Power(t2,4) - 
                     1676*Power(t2,5) - 11416*Power(t2,6) - 
                     362*Power(t2,7) + 200*Power(t2,8))))))*
       R1q(1 - s + s2 - t1))/
     ((-1 + t1)*Power(-1 + s + t1,3)*(-1 + s - s2 + t1)*(s - s2 + t1)*
       (-1 + s1 + t1 - t2)*(-1 + t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,2)*
       Power(-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2),2)) + 
    (8*(9 - 51*s2 - 61*Power(s2,2) + Power(s2,3) + 4*Power(s2,4) + 
         2*Power(s2,5) + 45*t1 + 282*s2*t1 + 328*Power(s2,2)*t1 + 
         66*Power(s2,3)*t1 - 61*Power(s2,4)*t1 - 4*Power(s2,5)*t1 - 
         50*Power(t1,2) - 633*s2*Power(t1,2) - 
         945*Power(s2,2)*Power(t1,2) - 129*Power(s2,3)*Power(t1,2) + 
         155*Power(s2,4)*Power(t1,2) + 2*Power(s2,5)*Power(t1,2) - 
         406*Power(t1,3) + 919*s2*Power(t1,3) + 
         1417*Power(s2,2)*Power(t1,3) + 9*Power(s2,3)*Power(t1,3) - 
         147*Power(s2,4)*Power(t1,3) + 885*Power(t1,4) - 
         964*s2*Power(t1,4) - 1002*Power(s2,2)*Power(t1,4) + 
         100*Power(s2,3)*Power(t1,4) + 53*Power(s2,4)*Power(t1,4) - 
         599*Power(t1,5) + 599*s2*Power(t1,5) + 
         259*Power(s2,2)*Power(t1,5) - 47*Power(s2,3)*Power(t1,5) - 
         4*Power(s2,4)*Power(t1,5) + 84*Power(t1,6) - 
         152*s2*Power(t1,6) + 4*Power(s2,2)*Power(t1,6) + 
         32*Power(t1,7) + 2*Power(s,8)*(-1 + s2)*Power(t1 - t2,2) + 
         114*t2 - 13*s2*t2 - 478*Power(s2,2)*t2 - 275*Power(s2,3)*t2 + 
         74*Power(s2,4)*t2 + 14*Power(s2,5)*t2 + 4*Power(s2,6)*t2 - 
         476*t1*t2 + 752*s2*t1*t2 + 2331*Power(s2,2)*t1*t2 + 
         973*Power(s2,3)*t1*t2 - 123*Power(s2,4)*t1*t2 - 
         133*Power(s2,5)*t1*t2 - 4*Power(s2,6)*t1*t2 + 
         1111*Power(t1,2)*t2 - 2011*s2*Power(t1,2)*t2 - 
         4284*Power(s2,2)*Power(t1,2)*t2 - 
         1799*Power(s2,3)*Power(t1,2)*t2 + 
         215*Power(s2,4)*Power(t1,2)*t2 + 
         216*Power(s2,5)*Power(t1,2)*t2 - 1505*Power(t1,3)*t2 + 
         1740*s2*Power(t1,3)*t2 + 4064*Power(s2,2)*Power(t1,3)*t2 + 
         1651*Power(s2,3)*Power(t1,3)*t2 - 
         301*Power(s2,4)*Power(t1,3)*t2 - 
         105*Power(s2,5)*Power(t1,3)*t2 + 923*Power(t1,4)*t2 - 
         542*s2*Power(t1,4)*t2 - 2110*Power(s2,2)*Power(t1,4)*t2 - 
         534*Power(s2,3)*Power(t1,4)*t2 + 
         135*Power(s2,4)*Power(t1,4)*t2 + 8*Power(s2,5)*Power(t1,4)*t2 - 
         79*Power(t1,5)*t2 + 194*s2*Power(t1,5)*t2 + 
         477*Power(s2,2)*Power(t1,5)*t2 - 16*Power(s2,3)*Power(t1,5)*t2 - 
         88*Power(t1,6)*t2 - 120*s2*Power(t1,6)*t2 + 51*Power(t2,2) + 
         111*s2*Power(t2,2) - 433*Power(s2,2)*Power(t2,2) - 
         1002*Power(s2,3)*Power(t2,2) - 349*Power(s2,4)*Power(t2,2) + 
         158*Power(s2,5)*Power(t2,2) + 16*Power(s2,6)*Power(t2,2) + 
         2*Power(s2,7)*Power(t2,2) - 201*t1*Power(t2,2) - 
         164*s2*t1*Power(t2,2) + 2698*Power(s2,2)*t1*Power(t2,2) + 
         3846*Power(s2,3)*t1*Power(t2,2) + 
         698*Power(s2,4)*t1*Power(t2,2) - 
         326*Power(s2,5)*t1*Power(t2,2) - 79*Power(s2,6)*t1*Power(t2,2) + 
         318*Power(t1,2)*Power(t2,2) + 832*s2*Power(t1,2)*Power(t2,2) - 
         4748*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         5014*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         682*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         325*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         59*Power(s2,6)*Power(t1,2)*Power(t2,2) - 
         108*Power(t1,3)*Power(t2,2) - 1319*s2*Power(t1,3)*Power(t2,2) + 
         3236*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         2781*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         257*Power(s2,4)*Power(t1,3)*Power(t2,2) - 
         129*Power(s2,5)*Power(t1,3)*Power(t2,2) - 
         4*Power(s2,6)*Power(t1,3)*Power(t2,2) - 
         183*Power(t1,4)*Power(t2,2) + 329*s2*Power(t1,4)*Power(t2,2) - 
         941*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         561*Power(s2,3)*Power(t1,4)*Power(t2,2) + 
         20*Power(s2,4)*Power(t1,4)*Power(t2,2) + 
         103*Power(t1,5)*Power(t2,2) + 211*s2*Power(t1,5)*Power(t2,2) + 
         172*Power(s2,2)*Power(t1,5)*Power(t2,2) + 
         20*Power(t1,6)*Power(t2,2) - 144*Power(t2,3) - 
         105*s2*Power(t2,3) - 195*Power(s2,2)*Power(t2,3) - 
         827*Power(s2,3)*Power(t2,3) - 777*Power(s2,4)*Power(t2,3) - 
         119*Power(s2,5)*Power(t2,3) + 96*Power(s2,6)*Power(t2,3) + 
         6*Power(s2,7)*Power(t2,3) + 502*t1*Power(t2,3) - 
         152*s2*t1*Power(t2,3) + 703*Power(s2,2)*t1*Power(t2,3) + 
         2881*Power(s2,3)*t1*Power(t2,3) + 
         2091*Power(s2,4)*t1*Power(t2,3) + 
         11*Power(s2,5)*t1*Power(t2,3) - 135*Power(s2,6)*t1*Power(t2,3) - 
         7*Power(s2,7)*t1*Power(t2,3) - 702*Power(t1,2)*Power(t2,3) + 
         656*s2*Power(t1,2)*Power(t2,3) + 
         60*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         3057*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         1555*Power(s2,4)*Power(t1,2)*Power(t2,3) + 
         60*Power(s2,5)*Power(t1,2)*Power(t2,3) + 
         41*Power(s2,6)*Power(t1,2)*Power(t2,3) + 
         498*Power(t1,3)*Power(t2,3) - 183*s2*Power(t1,3)*Power(t2,3) - 
         355*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         1064*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         299*Power(s2,4)*Power(t1,3)*Power(t2,3) - 
         8*Power(s2,5)*Power(t1,3)*Power(t2,3) - 
         123*Power(t1,4)*Power(t2,3) - 168*s2*Power(t1,4)*Power(t2,3) - 
         155*Power(s2,2)*Power(t1,4)*Power(t2,3) - 
         112*Power(s2,3)*Power(t1,4)*Power(t2,3) - 
         33*Power(t1,5)*Power(t2,3) - 56*s2*Power(t1,5)*Power(t2,3) - 
         49*Power(t2,4) - 291*s2*Power(t2,4) - 
         273*Power(s2,2)*Power(t2,4) - 233*Power(s2,3)*Power(t2,4) - 
         495*Power(s2,4)*Power(t2,4) - 219*Power(s2,5)*Power(t2,4) + 
         18*Power(s2,6)*Power(t2,4) + 11*Power(s2,7)*Power(t2,4) + 
         123*t1*Power(t2,4) + 706*s2*t1*Power(t2,4) - 
         346*Power(s2,2)*t1*Power(t2,4) + 
         352*Power(s2,3)*t1*Power(t2,4) + 
         1045*Power(s2,4)*t1*Power(t2,4) + 
         296*Power(s2,5)*t1*Power(t2,4) - 42*Power(s2,6)*t1*Power(t2,4) - 
         122*Power(t1,2)*Power(t2,4) - 751*s2*Power(t1,2)*Power(t2,4) + 
         725*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         126*Power(s2,3)*Power(t1,2)*Power(t2,4) - 
         509*Power(s2,4)*Power(t1,2)*Power(t2,4) - 
         63*Power(s2,5)*Power(t1,2)*Power(t2,4) + 
         18*Power(t1,3)*Power(t2,4) + 308*s2*Power(t1,3)*Power(t2,4) - 
         43*Power(s2,2)*Power(t1,3)*Power(t2,4) + 
         29*Power(s2,3)*Power(t1,3)*Power(t2,4) + 
         28*Power(s2,4)*Power(t1,3)*Power(t2,4) + 
         34*Power(t1,4)*Power(t2,4) + 85*s2*Power(t1,4)*Power(t2,4) + 
         52*Power(s2,2)*Power(t1,4)*Power(t2,4) + 26*Power(t2,5) - 
         71*s2*Power(t2,5) - 112*Power(s2,2)*Power(t2,5) - 
         93*Power(s2,3)*Power(t2,5) - 66*Power(s2,4)*Power(t2,5) - 
         79*Power(s2,5)*Power(t2,5) - 11*Power(s2,6)*Power(t2,5) - 
         56*t1*Power(t2,5) + 160*s2*t1*Power(t2,5) + 
         195*Power(s2,2)*t1*Power(t2,5) - 
         373*Power(s2,3)*t1*Power(t2,5) - 8*Power(s2,4)*t1*Power(t2,5) + 
         108*Power(s2,5)*t1*Power(t2,5) + 63*Power(t1,2)*Power(t2,5) - 
         93*s2*Power(t1,2)*Power(t2,5) - 
         204*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
         170*Power(s2,3)*Power(t1,2)*Power(t2,5) + 
         3*Power(s2,4)*Power(t1,2)*Power(t2,5) - 
         33*Power(t1,3)*Power(t2,5) - 65*s2*Power(t1,3)*Power(t2,5) - 
         71*Power(s2,2)*Power(t1,3)*Power(t2,5) - 
         16*Power(s2,3)*Power(t1,3)*Power(t2,5) - 11*Power(t2,6) + 
         37*s2*Power(t2,6) - 11*Power(s2,2)*Power(t2,6) + 
         44*Power(s2,3)*Power(t2,6) + 18*Power(s2,4)*Power(t2,6) - 
         13*Power(s2,5)*Power(t2,6) + t1*Power(t2,6) - 
         72*s2*t1*Power(t2,6) + 72*Power(s2,2)*t1*Power(t2,6) + 
         4*Power(s2,3)*t1*Power(t2,6) - 62*Power(s2,4)*t1*Power(t2,6) + 
         6*Power(t1,2)*Power(t2,6) + 64*s2*Power(t1,2)*Power(t2,6) + 
         28*Power(s2,2)*Power(t1,2)*Power(t2,6) + 
         19*Power(s2,3)*Power(t1,2)*Power(t2,6) + 4*Power(t2,7) - 
         3*s2*Power(t2,7) + 9*Power(s2,2)*Power(t2,7) + 
         3*Power(s2,3)*Power(t2,7) + 15*Power(s2,4)*Power(t2,7) - 
         2*t1*Power(t2,7) - 8*s2*t1*Power(t2,7) - 
         29*Power(s2,2)*t1*Power(t2,7) + 3*Power(s2,3)*t1*Power(t2,7) + 
         2*s2*Power(t2,8) + 2*Power(s2,2)*Power(t2,8) - 
         2*Power(s2,3)*Power(t2,8) + 
         2*Power(s1,8)*Power(s2,2)*(3 + 4*s2 - 8*t1 + 3*t2) + 
         Power(s1,7)*(30*Power(s2,4) - 2*Power(t1,3) + 
            Power(s2,3)*(39 - 53*t1 - 31*t2) - 
            Power(s2,2)*(14 + 15*t1 + 20*Power(t1,2) + 28*t2 - 
               129*t1*t2 + 42*Power(t2,2)) - 
            s2*(Power(t1,3) + 12*(1 + t2) + 2*Power(t1,2)*(15 + t2) - 
               4*t1*(11 + 3*t2))) + 
         Power(s1,6)*(42*Power(s2,5) + Power(t1,4) + 
            Power(s2,4)*(29 - 58*t1 - 150*t2) + 6*(1 + t2) - 
            4*t1*(7 + 3*t2) + Power(t1,3)*(-19 + 6*t2) + 
            2*Power(t1,2)*(18 + 7*t2) + 
            Power(s2,3)*(-162 - 74*Power(t1,2) - 249*t2 + 
               8*Power(t2,2) + t1*(170 + 411*t2)) + 
            Power(s2,2)*(-157 - 10*Power(t1,3) - 4*t2 + 
               37*Power(t2,2) + 126*Power(t2,3) + 
               3*Power(t1,2)*(-89 + 39*t2) + 
               t1*(366 + 208*t2 - 425*Power(t2,2))) + 
            s2*(4 - 51*Power(t1,3) + 44*t2 + 72*Power(t2,2) + 
               t1*(50 - 274*t2 - 68*Power(t2,2)) + 
               Power(t1,2)*(8 + 213*t2 + 10*Power(t2,2)))) + 
         Power(s1,5)*(2 + 26*Power(s2,6) - 16*t2 - 30*Power(t2,2) - 
            Power(t1,4)*(31 + 7*t2) - 
            Power(s2,5)*(35 + 16*t1 + 210*t2) + 
            Power(t1,2)*(22 - 185*t2 - 58*Power(t2,2)) + 
            Power(t1,3)*(42 + 99*t2 - 4*Power(t2,2)) + 
            t1*(-35 + 133*t2 + 50*Power(t2,2)) + 
            Power(s2,4)*(-359 - 102*Power(t1,2) - 254*t2 + 
               271*Power(t2,2) + t1*(406 + 460*t2)) + 
            Power(s2,3)*(-263 - 28*Power(t1,3) + 611*t2 + 
               640*Power(t2,2) + 147*Power(t2,3) + 
               4*Power(t1,2)*(-82 + 103*t2) + 
               t1*(643 - 703*t2 - 1254*Power(t2,2))) + 
            s2*(197 - 7*Power(t1,4) + 81*t2 - 28*Power(t2,2) - 
               182*Power(t2,3) + 
               Power(t1,3)*(-367 + 211*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(885 + 180*t2 - 584*Power(t2,2) - 
                  20*Power(t2,3)) + 
               t1*(-653 - 556*t2 + 683*Power(t2,2) + 160*Power(t2,3))) + 
            Power(s2,2)*(230 + 810*t2 + 226*Power(t2,2) + 
               23*Power(t2,3) - 210*Power(t2,4) + 
               2*Power(t1,3)*(-158 + 9*t2) + 
               Power(t1,2)*(592 + 1621*t2 - 261*Power(t2,2)) + 
               t1*(-415 - 2235*t2 - 801*Power(t2,2) + 750*Power(t2,3)))) \
+ Power(s1,4)*(-79 + 6*Power(s2,7) + 7*Power(t1,5) + 
            Power(s2,6)*(-39 + 10*t1 - 124*t2) - 46*t2 - 3*Power(t2,2) + 
            64*Power(t2,3) + Power(t1,4)*
             (-174 + 83*t2 + 15*Power(t2,2)) + 
            t1*(340 + 328*t2 - 254*Power(t2,2) - 82*Power(t2,3)) - 
            Power(t1,3)*(-568 + 3*t2 + 195*Power(t2,2) + 
               4*Power(t2,3)) + 
            Power(t1,2)*(-658 - 362*t2 + 377*Power(t2,2) + 
               92*Power(t2,3)) + 
            Power(s2,5)*(-180 - 62*Power(t1,2) + 45*t2 + 
               403*Power(t2,2) + 2*t1*(137 + 93*t2)) + 
            Power(s2,4)*(220 - 34*Power(t1,3) + 1542*t2 + 
               759*Power(t2,2) - 169*Power(t2,3) + 
               4*Power(t1,2)*(8 + 133*t2) - 
               t1*(253 + 1835*t2 + 1343*Power(t2,2))) + 
            Power(s2,3)*(1061 + 1564*t2 - 712*Power(t2,2) - 
               847*Power(t2,3) - 330*Power(t2,4) + 
               8*Power(t1,3)*(-66 + 7*t2) + 
               Power(t1,2)*(2122 + 2031*t2 - 835*Power(t2,2)) + 
               t1*(-2386 - 4061*t2 + 994*Power(t2,2) + 1969*Power(t2,3))\
) + Power(s2,2)*(655 - 49*Power(t1,4) - 478*t2 - 1613*Power(t2,2) - 
               575*Power(t2,3) - 110*Power(t2,4) + 210*Power(t2,5) + 
               Power(t1,3)*(-732 + 1243*t2 + 25*Power(t2,2)) + 
               Power(t1,2)*(2547 - 2118*t2 - 3691*Power(t2,2) + 
                  274*Power(t2,3)) + 
               t1*(-2201 + 720*t2 + 5202*Power(t2,2) + 
                  1395*Power(t2,3) - 770*Power(t2,4))) - 
            s2*(94 + 793*t2 + 304*Power(t2,2) + 91*Power(t2,3) - 
               250*Power(t2,4) + Power(t1,4)*(394 + 24*t2) + 
               Power(t1,3)*(-1009 - 1760*t2 + 313*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(t1,2)*(636 + 4227*t2 + 727*Power(t2,2) - 
                  806*Power(t2,3) - 20*Power(t2,4)) + 
               t1*(-168 - 2995*t2 - 1599*Power(t2,2) + 880*Power(t2,3) + 
                  200*Power(t2,4)))) + 
         Power(s1,3)*(-4 + Power(s2,7)*(-8 + 5*t1 - 27*t2) + 232*t2 + 
            80*Power(t2,2) + 56*Power(t2,3) - 76*Power(t2,4) - 
            Power(t1,5)*(152 + 33*t2) + 
            Power(s2,6)*(-23 - 14*Power(t1,2) + t1*(53 - 9*t2) + 
               108*t2 + 229*Power(t2,2)) + 
            Power(t1,4)*(549 + 581*t2 - 66*Power(t2,2) - 
               13*Power(t2,3)) + 
            Power(t1,2)*(42 + 2223*t2 + 794*Power(t2,2) - 
               387*Power(t2,3) - 68*Power(t2,4)) + 
            Power(t1,3)*(-510 - 1928*t2 - 167*Power(t2,2) + 
               181*Power(t2,3) + 6*Power(t2,4)) + 
            t1*(77 - 1091*t2 - 641*Power(t2,2) + 243*Power(t2,3) + 
               68*Power(t2,4)) + 
            Power(s2,5)*(198 - 19*Power(t1,3) + 829*t2 + 
               163*Power(t2,2) - 356*Power(t2,3) + 
               2*Power(t1,2)*(89 + 153*t2) - 
               t1*(365 + 1201*t2 + 587*Power(t2,2))) + 
            Power(s2,4)*(409 - 182*t2 - 2387*Power(t2,2) - 
               1061*Power(t2,3) - 84*Power(t2,4) + 
               Power(t1,3)*(-334 + 66*t2) + 
               Power(t1,2)*(1130 + 437*t2 - 959*Power(t2,2)) + 
               t1*(-1139 - 365*t2 + 3042*Power(t2,2) + 1911*Power(t2,3))\
) + Power(s2,3)*(-493 - 73*Power(t1,4) - 3580*t2 - 3053*Power(t2,2) + 
               46*Power(t2,3) + 603*Power(t2,4) + 327*Power(t2,5) + 
               Power(t1,3)*(566 + 1865*t2 + 27*Power(t2,2)) + 
               Power(t1,2)*(-1243 - 7461*t2 - 4384*Power(t2,2) + 
                  754*Power(t2,3)) + 
               t1*(1186 + 8161*t2 + 8800*Power(t2,2) - 
                  468*Power(t2,3) - 1701*Power(t2,4))) + 
            s2*(-637 - Power(t1,5) - 125*t2 + 1197*Power(t2,2) + 
               349*Power(t2,3) + 184*Power(t2,4) - 200*Power(t2,5) + 
               Power(t1,4)*(-797 + 1014*t2 + 107*Power(t2,2)) + 
               Power(t1,3)*(3641 - 2552*t2 - 2869*Power(t2,2) + 
                  183*Power(t2,3) + 15*Power(t2,4)) + 
               Power(t1,2)*(-4983 + 482*t2 + 7146*Power(t2,2) + 
                  957*Power(t2,3) - 594*Power(t2,4) - 10*Power(t2,5)) + 
               t1*(2785 + 965*t2 - 4975*Power(t2,2) - 
                  1889*Power(t2,3) + 630*Power(t2,4) + 140*Power(t2,5))) \
+ Power(s2,2)*(-1363 - 2878*t2 + 71*Power(t2,2) + 1560*Power(t2,3) + 
               610*Power(t2,4) + 114*Power(t2,5) - 126*Power(t2,6) - 
               Power(t1,4)*(989 + 9*t2) + 
               Power(t1,3)*(4823 + 3282*t2 - 1622*Power(t2,2) - 
                  83*Power(t2,3)) + 
               Power(t1,2)*(-6991 - 11446*t2 + 2648*Power(t2,2) + 
                  3989*Power(t2,3) - 126*Power(t2,4)) + 
               t1*(4496 + 10256*t2 + 450*Power(t2,2) - 
                  5928*Power(t2,3) - 1205*Power(t2,4) + 461*Power(t2,5)))\
) + Power(s1,2)*(216 + 20*Power(t1,6) + 142*t2 - 264*Power(t2,2) - 
            4*Power(t2,3) - 80*Power(t2,4) + 54*Power(t2,5) + 
            Power(t1,5)*(-348 + 199*t2 + 45*Power(t2,2)) + 
            Power(s2,7)*(2 + (22 - 17*t1)*t2 + 47*Power(t2,2)) + 
            Power(t1,4)*(1764 - 740*t2 - 567*Power(t2,2) + 
               7*Power(t2,3) + 4*Power(t2,4)) + 
            Power(t1,3)*(-3287 + 150*t2 + 2051*Power(t2,2) + 
               142*Power(t2,3) - 78*Power(t2,4) - 2*Power(t2,5)) + 
            Power(t1,2)*(2804 + 1073*t2 - 2461*Power(t2,2) - 
               527*Power(t2,3) + 209*Power(t2,4) + 22*Power(t2,5)) - 
            t1*(1169 + 830*t2 - 1220*Power(t2,2) - 382*Power(t2,3) + 
               115*Power(t2,4) + 32*Power(t2,5)) - 
            Power(s2,6)*(-20 + 4*Power(t1,3) - 136*t2 + 
               81*Power(t2,2) + 201*Power(t2,3) - 
               Power(t1,2)*(55 + 67*t2) + 
               t1*(79 + 233*t2 + 54*Power(t2,2))) + 
            Power(s2,5)*(29 - 531*t2 - 1341*Power(t2,2) - 
               400*Power(t2,3) + 120*Power(t2,4) + 
               Power(t1,3)*(-73 + 36*t2) - 
               3*Power(t1,2)*(-40 + 116*t2 + 163*Power(t2,2)) + 
               t1*(-48 + 803*t2 + 1880*Power(t2,2) + 788*Power(t2,3))) + 
            Power(s2,4)*(-40*Power(t1,4) + 
               Power(t1,3)*(637 + 1059*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(-1381 - 4028*t2 - 1542*Power(t2,2) + 
                  733*Power(t2,3)) + 
               t1*(1068 + 4607*t2 + 2611*Power(t2,2) - 
                  2211*Power(t2,3) - 1405*Power(t2,4)) + 
               2*(-170 - 856*t2 - 408*Power(t2,2) + 759*Power(t2,3) + 
                  367*Power(t2,4) + 88*Power(t2,5))) + 
            Power(s2,3)*(-433 + 284*t2 + 3717*Power(t2,2) + 
               2373*Power(t2,3) + 454*Power(t2,4) - 211*Power(t2,5) - 
               164*Power(t2,6) + 2*Power(t1,4)*(-306 + 7*t2) - 
               Power(t1,3)*(-2109 + 41*t2 + 2072*Power(t2,2) + 
                  126*Power(t2,3)) + 
               Power(t1,2)*(-2700 - 600*t2 + 8589*Power(t2,2) + 
                  4157*Power(t2,3) - 262*Power(t2,4)) + 
               t1*(1686 + 406*t2 - 8737*Power(t2,2) - 
                  8362*Power(t2,3) - 80*Power(t2,4) + 777*Power(t2,5))) \
- s2*(-769 - 2000*t2 - 288*Power(t2,2) + 874*Power(t2,3) + 
               93*Power(t2,4) + 134*Power(t2,5) - 92*Power(t2,6) + 
               Power(t1,5)*(771 + 134*t2) + 
               Power(t1,4)*(-4672 - 2232*t2 + 659*Power(t2,2) + 
                  114*Power(t2,3)) + 
               Power(t1,3)*(8797 + 10496*t2 - 2062*Power(t2,2) - 
                  1861*Power(t2,3) + 16*Power(t2,4) + 4*Power(t2,5)) - 
               Power(t1,2)*(7413 + 15377*t2 + 584*Power(t2,2) - 
                  5244*Power(t2,3) - 429*Power(t2,4) + 
                  221*Power(t2,5) + 2*Power(t2,6)) + 
               t1*(3286 + 9003*t2 + 1945*Power(t2,2) - 
                  3737*Power(t2,3) - 883*Power(t2,4) + 
                  254*Power(t2,5) + 52*Power(t2,6))) + 
            Power(s2,2)*(475 - 19*Power(t1,5) + 3316*t2 + 
               3547*Power(t2,2) + 260*Power(t2,3) - 747*Power(t2,4) - 
               286*Power(t2,5) - 47*Power(t2,6) + 42*Power(t2,7) + 
               4*Power(t1,4)*(268 + 515*t2 + 61*Power(t2,2)) + 
               Power(t1,3)*(-2887 - 11207*t2 - 4561*Power(t2,2) + 
                  708*Power(t2,3) + 69*Power(t2,4)) + 
               Power(t1,2)*(2915 + 16660*t2 + 16208*Power(t2,2) - 
                  1514*Power(t2,3) - 2026*Power(t2,4) + 9*Power(t2,5)) - 
               t1*(1572 + 10723*t2 + 14393*Power(t2,2) + 
                  1205*Power(t2,3) - 3432*Power(t2,4) - 
                  450*Power(t2,5) + 149*Power(t2,6)))) + 
         s1*(-8*Power(t1,6)*(27 + 7*t2) - 
            Power(s2,7)*t2*(4 + (20 - 19*t1)*t2 + 37*Power(t2,2)) + 
            Power(t1,5)*(1604 + 441*t2 + 2*Power(t2,2) - 
               19*Power(t2,3)) + 
            Power(t1,4)*(-3826 - 2496*t2 + 265*Power(t2,2) + 
               126*Power(t2,3) + 7*Power(t2,4)) + 
            Power(t1,3)*(4174 + 5269*t2 - 85*Power(t2,2) - 
               709*Power(t2,3) + 19*Power(t2,4) + 12*Power(t2,5)) - 
            2*Power(t1,2)*(1178 + 2497*t2 + 218*Power(t2,2) - 
               509*Power(t2,3) - 5*Power(t2,4) + 28*Power(t2,5) + 
               Power(t2,6)) + 
            2*t1*(385 + 1134*t2 + 127*Power(t2,2) - 296*Power(t2,3) + 
               11*Power(t2,4) + 10*Power(t2,5) + 5*Power(t2,6)) - 
            2*(75 + 216*t2 - 3*Power(t2,2) - 80*Power(t2,3) + 
               29*Power(t2,4) - 24*Power(t2,5) + 11*Power(t2,6)) + 
            Power(s2,6)*(-4 - 36*t2 + 8*Power(t1,3)*t2 - 
               209*Power(t2,2) - 6*Power(t2,3) + 81*Power(t2,4) - 
               2*Power(t1,2)*t2*(57 + 47*t2) + 
               t1*(4 + 158*t2 + 315*Power(t2,2) + 95*Power(t2,3))) + 
            Power(s2,5)*(-16 - 8*Power(t1,4) - 183*t2 + 
               450*Power(t2,2) + 911*Power(t2,3) + 306*Power(t2,4) + 
               14*Power(t2,5) + 
               Power(t1,3)*(103 + 200*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*(-214 - 437*t2 + 104*Power(t2,2) + 
                  308*Power(t2,3)) + 
               t1*(135 + 364*t2 - 443*Power(t2,2) - 1249*Power(t2,3) - 
                  479*Power(t2,4))) + 
            Power(s2,4)*(-13 + 698*t2 + 2088*Power(t2,2) + 
               1273*Power(t2,3) - 248*Power(t2,4) - 225*Power(t2,5) - 
               89*Power(t2,6) + 2*Power(t1,4)*(-53 + 11*t2) - 
               Power(t1,3)*(-171 + 921*t2 + 1030*Power(t2,2) + 
                  69*Power(t2,3)) + 
               Power(t1,2)*(19 + 2120*t2 + 4473*Power(t2,2) + 
                  1582*Power(t2,3) - 207*Power(t2,4)) + 
               t1*(-71 - 1807*t2 - 5581*Power(t2,2) - 
                  3038*Power(t2,3) + 606*Power(t2,4) + 497*Power(t2,5))) \
+ Power(s2,3)*(242 - 15*Power(t1,5) + 1485*t2 + 1052*Power(t2,2) - 
               965*Power(t2,3) - 528*Power(t2,4) - 281*Power(t2,5) + 
               22*Power(t2,6) + 37*Power(t2,7) + 
               2*Power(t1,4)*(365 + 610*t2 + 93*Power(t2,2)) + 
               Power(t1,3)*(-2084 - 5037*t2 - 1658*Power(t2,2) + 
                  706*Power(t2,3) + 87*Power(t2,4)) + 
               Power(t1,2)*(2168 + 7917*t2 + 5009*Power(t2,2) - 
                  3376*Power(t2,3) - 1646*Power(t2,4) - 14*Power(t2,5)) \
+ t1*(-1041 - 5685*t2 - 4544*Power(t2,2) + 2610*Power(t2,3) + 
                  3353*Power(t2,4) + 83*Power(t2,5) - 152*Power(t2,6))) \
+ s2*(56*Power(t1,6) + Power(t1,5)*(637 + 624*t2 + 207*Power(t2,2)) + 
               2*Power(t1,4)*
                (-1235 - 2736*t2 - 677*Power(t2,2) - 23*Power(t2,3) + 
                  19*Power(t2,4)) + 
               Power(t1,3)*(3031 + 11513*t2 + 7201*Power(t2,2) - 
                  827*Power(t2,3) - 320*Power(t2,4) - 14*Power(t2,5)) - 
               Power(t1,2)*(1688 + 10184*t2 + 11185*Power(t2,2) - 
                  321*Power(t2,3) - 1533*Power(t2,4) + 
                  53*Power(t2,5) + 32*Power(t2,6)) + 
               t1*(610 + 4701*t2 + 6419*Power(t2,2) + 
                  106*Power(t2,3) - 1264*Power(t2,4) - 
                  15*Power(t2,5) + 59*Power(t2,6) + 8*Power(t2,7)) - 
               2*(88 + 591*t2 + 632*Power(t2,2) - 111*Power(t2,3) - 
                  172*Power(t2,4) + 37*Power(t2,5) - 20*Power(t2,6) + 
                  11*Power(t2,7))) + 
            Power(s2,2)*(229 - 134*t2 - 1758*Power(t2,2) - 
               1051*Power(t2,3) + 29*Power(t2,4) + 158*Power(t2,5) + 
               34*Power(t2,6) + 3*Power(t2,7) - 6*Power(t2,8) - 
               Power(t1,5)*(498 + 173*t2) - 
               Power(t1,4)*(-1816 + 90*t2 + 907*Power(t2,2) + 
                  238*Power(t2,3)) + 
               Power(t1,3)*(-2683 - 340*t2 + 6724*Power(t2,2) + 
                  2054*Power(t2,3) + 58*Power(t2,4) - 19*Power(t2,5)) + 
               Power(t1,2)*(2239 + 1680*t2 - 9726*Power(t2,2) - 
                  8034*Power(t2,3) + 596*Power(t2,4) + 
                  346*Power(t2,5) + 7*Power(t2,6)) + 
               t1*(-1103 - 911*t2 + 5527*Power(t2,2) + 
                  6684*Power(t2,3) + 255*Power(t2,4) - 909*Power(t2,5) - 
                  3*Power(t2,6) + 20*Power(t2,7)))) + 
         Power(s,7)*(2 + Power(t1,3) + Power(t1,2)*(5 + 12*s1 - 11*t2) + 
            4*t2 - 3*s1*t2 + 4*Power(t2,2) + 8*s1*Power(t2,2) - 
            9*Power(t2,3) + t1*
             (-10 + s1*(5 - 20*t2) - 7*t2 + 19*Power(t2,2)) + 
            Power(s2,2)*(2 - 7*Power(t1,2) + s1*t2 - 12*Power(t2,2) + 
               t1*(-6 + s1 + 21*t2)) + 
            s2*(-4 + Power(t1,3) + 2*(-2 + s1)*t2 - 
               10*(-1 + s1)*Power(t2,2) + 7*Power(t2,3) + 
               Power(t1,2)*(4 - 14*s1 + 5*t2) + 
               t1*(16 - 18*t2 - 13*Power(t2,2) + 6*s1*(-1 + 4*t2)))) - 
         Power(s,6)*(-8 + 47*t1 - 32*Power(t1,2) - 2*Power(t1,3) - 
            Power(t1,4) - 48*t2 + 99*t1*t2 - 46*Power(t1,2)*t2 - 
            6*Power(t1,3)*t2 - 15*Power(t2,2) + 55*t1*Power(t2,2) + 
            34*Power(t1,2)*Power(t2,2) - 19*Power(t2,3) - 
            46*t1*Power(t2,3) + 19*Power(t2,4) + 
            Power(s2,3)*(6 - 8*Power(t1,2) + 4*t2 - 28*Power(t2,2) + 
               3*t1*(-8 + 15*t2)) + 
            Power(s2,2)*(-26 + 10*Power(t1,3) - 84*t2 - 2*Power(t2,2) + 
               36*Power(t2,3) - Power(t1,2)*(59 + 27*t2) + 
               t1*(131 + 89*t2 - 31*Power(t2,2))) + 
            s2*(28 - 5*Power(t1,3) + 132*t2 + 51*Power(t2,2) - 
               34*Power(t2,3) - 10*Power(t2,4) + 
               Power(t1,2)*(101 + 50*t2 - 10*Power(t2,2)) + 
               t1*(-158 - 241*t2 + 13*Power(t2,2) + 20*Power(t2,3))) + 
            Power(s1,2)*(-6 + Power(s2,3) + 30*Power(t1,2) + 
               t1*(36 - 40*t2) - 7*t2 + 12*Power(t2,2) + 
               Power(s2,2)*(-10 + 22*t1 + 7*t2) + 
               s2*(15 - 42*Power(t1,2) + 4*t2 - 20*Power(t2,2) + 
                  t1*(-62 + 60*t2))) + 
            s1*(21 + 8*Power(t1,3) + Power(t1,2)*(53 - 63*t2) + 33*t2 + 
               28*Power(t2,2) - 31*Power(t2,3) + 
               Power(s2,3)*(2 + t1 + t2) + 
               t1*(-87 - 104*t2 + 86*Power(t2,2)) + 
               Power(s2,2)*(25 - 22*Power(t1,2) + 41*t2 - 
                  57*Power(t2,2) + t1*(-107 + 64*t2)) + 
               s2*(-48 + 7*Power(t1,3) - 83*t2 + 31*Power(t2,2) + 
                  30*Power(t2,3) + Power(t1,2)*(-21 + 32*t2) + 
                  t1*(201 + 28*t2 - 69*Power(t2,2))))) + 
         Power(s,5)*(-18 + 79*t1 - 258*Power(t1,2) + 144*Power(t1,3) + 
            17*Power(t1,4) + 11*t2 + 183*t1*t2 - 164*Power(t1,2)*t2 - 
            6*Power(t1,3)*t2 + 7*Power(t1,4)*t2 + 74*Power(t2,2) - 
            170*t1*Power(t2,2) + 165*Power(t1,2)*Power(t2,2) + 
            2*Power(t1,3)*Power(t2,2) + 12*Power(t2,3) - 
            210*t1*Power(t2,3) - 48*Power(t1,2)*Power(t2,3) + 
            64*Power(t2,4) + 62*t1*Power(t2,4) - 23*Power(t2,5) - 
            2*Power(s2,4)*(-2 + Power(t1,2) + t1*(18 - 25*t2) - 8*t2 + 
               15*Power(t2,2)) + 
            Power(s2,3)*(-48 + 28*Power(t1,3) - 288*t2 - 
               84*Power(t2,2) + 83*Power(t2,3) - 
               2*Power(t1,2)*(110 + 71*t2) + 
               t1*(338 + 424*t2 - 10*Power(t2,2))) + 
            Power(s2,2)*(72 + 594*t2 + 433*Power(t2,2) + 
               25*Power(t2,3) - 27*Power(t2,4) - 
               6*Power(t1,3)*(-5 + 3*t2) + 
               Power(t1,2)*(308 + 466*t2 + 149*Power(t2,2)) - 
               t1*(540 + 1144*t2 + 589*Power(t2,2) + 74*Power(t2,3))) + 
            s2*(-6 + 7*Power(t1,4) - 319*t2 - 372*Power(t2,2) - 
               101*Power(t2,3) + 42*Power(t2,4) + 10*Power(t2,5) - 
               5*Power(t1,3)*(50 + 17*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(208 - 27*t2 - 89*Power(t2,2) + 
                  30*Power(t2,3)) + 
               t1*(143 + 434*t2 + 665*Power(t2,2) + 65*Power(t2,3) - 
                  30*Power(t2,4))) + 
            Power(s1,3)*(-24 - 3*Power(s2,3) + 40*Power(t1,2) + 
               t1*(94 - 40*t2) - 9*t2 + 8*Power(t2,2) + 
               Power(s2,2)*(-36 + 111*t1 + 14*t2) + 
               s2*(61 - 70*Power(t1,2) + 8*t2 - 20*Power(t2,2) + 
                  20*t1*(-11 + 4*t2))) + 
            Power(s1,2)*(59 + 6*Power(s2,4) + 27*Power(t1,3) + 
               Power(t1,2)*(140 - 150*t2) + 110*t2 + 73*Power(t2,2) - 
               39*Power(t2,3) + Power(s2,3)*(-22 + 58*t1 + 35*t2) + 
               t1*(-192 - 393*t2 + 154*Power(t2,2)) + 
               Power(s2,2)*(155 + 15*Power(t1,2) + 163*t2 - 
                  95*Power(t2,2) - t1*(583 + 124*t2)) + 
               s2*(-190 + 21*Power(t1,3) - 324*t2 - Power(t2,2) + 
                  50*Power(t2,3) + 3*Power(t1,2)*(-45 + 29*t2) + 
                  t1*(735 + 528*t2 - 150*Power(t2,2)))) + 
            s1*(6 - 6*Power(t1,4) - 197*t2 - 81*Power(t2,2) - 
               128*Power(t2,3) + 54*Power(t2,4) - 
               2*Power(s2,4)*(-4 + 3*t1 + 5*t2) - 
               3*Power(t1,3)*(17 + 12*t2) + 
               Power(t1,2)*(79 - 226*t2 + 164*Power(t2,2)) + 
               t1*(15 + 314*t2 + 488*Power(t2,2) - 176*Power(t2,3)) + 
               Power(s2,3)*(66 + 34*Power(t1,2) + 146*t2 - 
                  141*Power(t2,2) + t1*(-316 + 62*t2)) + 
               s2*(118 + 26*Power(t1,3) + 738*t2 + 380*Power(t2,2) - 
                  49*Power(t2,3) - 40*Power(t2,4) + 
                  Power(t1,2)*(301 + 299*t2 - 60*Power(t2,2)) + 
                  2*t1*(-341 - 750*t2 - 203*Power(t2,2) + 
                     50*Power(t2,3))) + 
               Power(s2,2)*(60*Power(t1,3) - 
                  6*Power(t1,2)*(73 + 42*t2) + 
                  t1*(1002 + 1122*t2 + 131*Power(t2,2)) + 
                  2*(-104 - 344*t2 - 71*Power(t2,2) + 54*Power(t2,3))))) \
+ Power(s,4)*(-27 + 236*t1 - 342*Power(t1,2) - 126*Power(t1,3) + 
            178*Power(t1,4) + 7*Power(t1,5) - 323*t2 + 845*t1*t2 - 
            646*Power(t1,2)*t2 + 115*Power(t1,3)*t2 + 
            60*Power(t1,4)*t2 - 100*Power(t2,2) + 706*t1*Power(t2,2) - 
            604*Power(t1,2)*Power(t2,2) - 15*Power(t1,3)*Power(t2,2) + 
            15*Power(t1,4)*Power(t2,2) + 102*Power(t2,3) + 
            39*t1*Power(t2,3) + 191*Power(t1,2)*Power(t2,3) - 
            20*Power(t1,3)*Power(t2,3) - 48*Power(t2,4) - 
            302*t1*Power(t2,4) - 20*Power(t1,2)*Power(t2,4) + 
            99*Power(t2,5) + 40*t1*Power(t2,5) - 15*Power(t2,6) - 
            2*Power(s2,5)*(-2 + Power(t1,2) + 12*t2 - 5*Power(t2,2) + 
               3*t1*(-4 + 5*t2)) - 
            2*Power(s2,4)*(-14 + 17*Power(t1,3) - 196*t2 - 
               85*Power(t2,2) + 56*Power(t2,3) - 
               Power(t1,2)*(148 + 107*t2) + 
               t1*(194 + 307*t2 + 6*Power(t2,2))) + 
            Power(s1,4)*(36 + 30*Power(s2,3) + 
               Power(s2,2)*(50 - 260*t1) - 30*Power(t1,2) + 15*t2 - 
               2*Power(t2,2) + 4*t1*(-29 + 5*t2) + 
               s2*(-105 + 70*Power(t1,2) + t1*(380 - 60*t2) - 32*t2 + 
                  10*Power(t2,2))) + 
            Power(s2,3)*(-74 - 980*t2 - 1132*Power(t2,2) - 
               242*Power(t2,3) + 45*Power(t2,4) + 
               4*Power(t1,3)*(-27 + 14*t2) - 
               2*Power(t1,2)*(206 + 532*t2 + 277*Power(t2,2)) + 
               t1*(806 + 2080*t2 + 1788*Power(t2,2) + 383*Power(t2,3))) \
+ Power(s2,2)*(-36 - 49*Power(t1,4) + 286*t2 + 1142*Power(t2,2) + 
               622*Power(t2,3) + 96*Power(t2,4) + 15*Power(t2,5) + 
               Power(t1,3)*(1112 + 631*t2 + 25*Power(t2,2)) + 
               Power(t1,2)*(-1446 - 1256*t2 + 455*Power(t2,2) + 
                  195*Power(t2,3)) + 
               t1*(111 + 306*t2 - 1729*Power(t2,2) - 1263*Power(t2,3) - 
                  195*Power(t2,4))) - 
            s2*(-111 - 668*t2 + 129*Power(t2,2) + 471*Power(t2,3) + 
               152*Power(t2,4) - 11*Power(t2,5) - 10*Power(t2,6) + 
               Power(t1,4)*(145 + 24*t2) + 
               Power(t1,3)*(841 + 824*t2 + 217*Power(t2,2) + 
                  20*Power(t2,3)) - 
               Power(t1,2)*(1926 + 2741*t2 + 770*Power(t2,2) + 
                  17*Power(t2,3) + 50*Power(t2,4)) + 
               t1*(809 + 2609*t2 + 626*Power(t2,2) - 871*Power(t2,3) - 
                  133*Power(t2,4) + 40*Power(t2,5))) + 
            Power(s1,3)*(-37 + 6*Power(s2,4) - 50*Power(t1,3) + 
               Power(s2,3)*(38 - 275*t1 - 161*t2) - 177*t2 - 
               108*Power(t2,2) + 21*Power(t2,3) + 
               10*Power(t1,2)*(-13 + 19*t2) + 
               t1*(34 + 613*t2 - 136*Power(t2,2)) + 
               Power(s2,2)*(-361 - 160*Power(t1,2) - 327*t2 + 
                  8*Power(t2,2) + t1*(1367 + 845*t2)) + 
               s2*(303 - 35*Power(t1,3) + Power(t1,2)*(220 - 130*t2) + 
                  636*t2 + 161*Power(t2,2) - 40*Power(t2,3) + 
                  5*t1*(-205 - 292*t2 + 34*Power(t2,2)))) + 
            Power(s1,2)*(-185 - 14*Power(s2,5) + 15*Power(t1,4) + 
               Power(s2,4)*(4 - 34*t1 - 62*t2) + 162*t2 + 
               172*Power(t2,2) + 270*Power(t2,3) - 51*Power(t2,4) + 
               15*Power(t1,3)*(11 + 6*t2) - 
               4*Power(t1,2)*(185 - 71*t2 + 79*Power(t2,2)) + 
               t1*(648 + 219*t2 - 1153*Power(t2,2) + 252*Power(t2,3)) + 
               Power(s2,3)*(-314 - 290*Power(t1,2) - 366*t2 + 
                  341*Power(t2,2) + t1*(1354 + 613*t2)) - 
               Power(s2,2)*(-556 + 150*Power(t1,3) - 1856*t2 - 
                  657*Power(t2,2) + Power(t2,3) - 
                  5*Power(t1,2)*(179 + 171*t2) + 
                  t1*(2387 + 4154*t2 + 1259*Power(t2,2))) + 
               s2*(28 - 205*Power(t1,3) - 1438*t2 - 1138*Power(t2,2) - 
                  215*Power(t2,3) + 60*Power(t2,4) + 
                  3*Power(t1,2)*(152 - 161*t2 + 50*Power(t2,2)) + 
                  t1*(161 + 2926*t2 + 1930*Power(t2,2) - 
                     200*Power(t2,3)))) + 
            s1*(193 + 486*t2 - 247*Power(t2,2) + 17*Power(t2,3) - 
               276*Power(t2,4) + 47*Power(t2,5) + 
               2*Power(s2,5)*(-6 + 7*t1 + 15*t2) - 
               Power(t1,4)*(99 + 35*t2) - 
               Power(t1,3)*(430 + 59*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(1246 + 1399*t2 - 384*Power(t2,2) + 
                  176*Power(t2,3)) - 
               t1*(805 + 1786*t2 + 233*Power(t2,2) - 958*Power(t2,3) + 
                  176*Power(t2,4)) - 
               2*Power(s2,4)*
                (20 + 47*Power(t1,2) + 105*t2 - 96*Power(t2,2) + 
                  t1*(-181 + 32*t2)) + 
               Power(s2,3)*(298 - 140*Power(t1,3) + 1620*t2 + 
                  556*Power(t2,2) - 255*Power(t2,3) + 
                  4*Power(t1,2)*(292 + 245*t2) - 
                  t1*(1922 + 3080*t2 + 787*Power(t2,2))) + 
               Power(s2,2)*(-91 - 2150*t2 - 2161*Power(t2,2) - 
                  476*Power(t2,3) - 22*Power(t2,4) + 
                  Power(t1,3)*(-436 + 90*t2) + 
                  Power(t1,2)*(116 - 1571*t2 - 857*Power(t2,2)) + 
                  t1*(1156 + 4324*t2 + 4157*Power(t2,2) + 
                     869*Power(t2,3))) + 
               s2*(-389 - 35*Power(t1,4) + 99*t2 + 1586*Power(t2,2) + 
                  759*Power(t2,3) + 75*Power(t2,4) - 40*Power(t2,5) + 
                  Power(t1,3)*(1331 + 551*t2 + 50*Power(t2,2)) - 
                  Power(t1,2)*
                   (2759 + 1640*t2 - 172*Power(t2,2) + 140*Power(t2,3)) \
+ t1*(1368 + 1090*t2 - 2721*Power(t2,2) - 983*Power(t2,3) + 
                     130*Power(t2,4))))) + 
         Power(s,3)*(90 - 399*t1 + 1349*Power(t1,2) - 1675*Power(t1,3) + 
            469*Power(t1,4) + 108*Power(t1,5) + 169*t2 - 1415*t1*t2 + 
            2834*Power(t1,2)*t2 - 1744*Power(t1,3)*t2 + 
            230*Power(t1,4)*t2 + 33*Power(t1,5)*t2 - 434*Power(t2,2) - 
            25*t1*Power(t2,2) + 547*Power(t1,2)*Power(t2,2) - 
            151*Power(t1,3)*Power(t2,2) + 49*Power(t1,4)*Power(t2,2) - 
            254*Power(t2,3) + 798*t1*Power(t2,3) - 
            787*Power(t1,2)*Power(t2,3) + 7*Power(t1,3)*Power(t2,3) + 
            13*Power(t1,4)*Power(t2,3) + 34*Power(t2,4) + 
            429*t1*Power(t2,4) + 34*Power(t1,2)*Power(t2,4) - 
            27*Power(t1,3)*Power(t2,4) - 159*Power(t2,5) - 
            155*t1*Power(t2,5) + 11*Power(t1,2)*Power(t2,5) + 
            62*Power(t2,6) + 7*t1*Power(t2,6) - 4*Power(t2,7) + 
            Power(s2,6)*(-6 + Power(t1,2) + 16*t2 + 8*Power(t2,2) + 
               t1*(-6 + 9*t2)) + 
            Power(s2,5)*(4 + 19*Power(t1,3) - 228*t2 - 
               134*Power(t2,2) + 93*Power(t2,3) - 
               Power(t1,2)*(176 + 135*t2) + 
               t1*(206 + 374*t2 - 19*Power(t2,2))) - 
            Power(s1,5)*(24 + 70*Power(s2,3) - 69*t1 - 12*Power(t1,2) - 
               5*Power(s2,2)*(-8 + 67*t1 - 7*t2) + 16*t2 + 4*t1*t2 + 
               s2*(-99 + 42*Power(t1,2) + t1*(350 - 24*t2) - 58*t2 + 
                  2*Power(t2,2))) + 
            Power(s2,4)*(40 + Power(t1,3)*(101 - 66*t2) + 676*t2 + 
               1262*Power(t2,2) + 341*Power(t2,3) - 84*Power(t2,4) + 
               Power(t1,2)*(281 + 1089*t2 + 698*Power(t2,2)) - 
               t1*(582 + 1763*t2 + 2035*Power(t2,2) + 504*Power(t2,3))) \
+ Power(s2,3)*(73*Power(t1,4) - 
               Power(t1,3)*(1506 + 1121*t2 + 27*Power(t2,2)) + 
               Power(t1,2)*(2178 + 2370*t2 - 770*Power(t2,2) - 
                  678*Power(t2,3)) + 
               t1*(-431 - 1440*t2 + 2150*Power(t2,2) + 
                  2938*Power(t2,3) + 729*Power(t2,4)) - 
               2*(-8 - 148*t2 + 724*Power(t2,2) + 741*Power(t2,3) + 
                  212*Power(t2,4) + 37*Power(t2,5))) + 
            Power(s2,2)*(-53 - 1706*t2 - 919*Power(t2,2) + 
               760*Power(t2,3) + 377*Power(t2,4) + 106*Power(t2,5) + 
               27*Power(t2,6) + Power(t1,4)*(484 + 9*t2) + 
               Power(t1,3)*(931 + 2598*t2 + 1384*Power(t2,2) + 
                  83*Power(t2,3)) + 
               Power(t1,2)*(-3153 - 6775*t2 - 5369*Power(t2,2) - 
                  663*Power(t2,3) + 66*Power(t2,4)) - 
               2*t1*(-698 - 2897*t2 - 2560*Power(t2,2) + 
                  37*Power(t2,3) + 513*Power(t2,4) + 73*Power(t2,5))) + 
            s2*(-102 + Power(t1,5) + 742*t2 + 1530*Power(t2,2) + 
               199*Power(t2,3) - 278*Power(t2,4) - 92*Power(t2,5) - 
               22*Power(t2,6) + 7*Power(t2,7) - 
               Power(t1,4)*(1542 + 752*t2 + 107*Power(t2,2)) + 
               Power(t1,3)*(2935 + 2100*t2 - 379*Power(t2,2) - 
                  119*Power(t2,3) - 15*Power(t2,4)) + 
               Power(t1,2)*(-989 - 1345*t2 + 2784*Power(t2,2) + 
                  1394*Power(t2,3) + 67*Power(t2,4) + 37*Power(t2,5)) - 
               t1*(62 + 849*t2 + 3815*Power(t2,2) + 1892*Power(t2,3) - 
                  398*Power(t2,4) - 121*Power(t2,5) + 29*Power(t2,6))) + 
            Power(s1,4)*(-41 - 84*Power(s2,4) + 55*Power(t1,3) + 
               Power(t1,2)*(5 - 135*t2) + 92*t2 + 89*Power(t2,2) - 
               4*Power(t2,3) + Power(s2,3)*(-1 + 540*t1 + 314*t2) + 
               t1*(254 - 416*t2 + 59*Power(t2,2)) + 
               Power(s2,2)*(490 + 295*Power(t1,2) + 441*t2 + 
                  198*Power(t2,2) - t1*(1568 + 1595*t2)) + 
               s2*(-216 + 35*Power(t1,3) - 636*t2 - 317*Power(t2,2) + 
                  15*Power(t2,3) + 5*Power(t1,2)*(-18 + 23*t2) + 
                  t1*(391 + 1750*t2 - 105*Power(t2,2)))) + 
            Power(s1,3)*(316 - 20*Power(t1,4) + 306*t2 + 
               25*Power(t2,2) - 233*Power(t2,3) + 16*Power(t2,4) - 
               10*Power(t1,3)*(19 + 12*t2) + 
               4*Power(s2,4)*(20 + 49*t1 + 99*t2) + 
               2*Power(t1,2)*(529 + 15*t2 + 152*Power(t2,2)) - 
               2*t1*(520 + 736*t2 - 511*Power(t2,2) + 80*Power(t2,3)) + 
               Power(s2,3)*(680 + 660*Power(t1,2) + 473*t2 - 
                  465*Power(t2,2) - 4*t1*(644 + 543*t2)) + 
               Power(s2,2)*(-735 + 200*Power(t1,3) - 2760*t2 - 
                  1354*Power(t2,2) - 411*Power(t2,3) - 
                  20*Power(t1,2)*(19 + 72*t2) + 
                  2*t1*(913 + 3206*t2 + 1553*Power(t2,2))) + 
               s2*(-520 + 460*Power(t1,3) + 942*t2 + 1333*Power(t2,2) + 
                  625*Power(t2,3) - 40*Power(t2,4) - 
                  2*Power(t1,2)*(1086 + 29*t2 + 100*Power(t2,2)) + 
                  4*t1*(543 - 272*t2 - 789*Power(t2,2) + 50*Power(t2,3))\
)) + Power(s1,2)*(-94 + 16*Power(s2,6) - 1401*t2 - 405*Power(t2,2) - 
               389*Power(t2,3) + 325*Power(t2,4) - 24*Power(t2,5) + 
               Power(s2,5)*(20 - 26*t1 + 46*t2) + 
               Power(t1,4)*(226 + 70*t2) + 
               2*Power(t1,3)*(-68 + 57*t2 + 14*Power(t2,2)) - 
               2*Power(t1,2)*
                (174 + 1251*t2 - 23*Power(t2,2) + 120*Power(t2,3)) + 
               t1*(434 + 3186*t2 + 2503*Power(t2,2) - 
                  1227*Power(t2,3) + 166*Power(t2,4)) + 
               2*Power(s2,4)*
                (104 + 198*Power(t1,2) + 85*t2 - 338*Power(t2,2) - 
                  t1*(629 + 284*t2)) + 
               Power(s2,3)*(280*Power(t1,3) - 
                  4*Power(t1,2)*(464 + 625*t2) + 
                  t1*(3618 + 8351*t2 + 3675*Power(t2,2)) + 
                  2*(-393 - 1772*t2 - 721*Power(t2,2) + 97*Power(t2,3))\
) + Power(s2,2)*(-368 + 2885*t2 + 4523*Power(t2,2) + 
                  1651*Power(t2,3) + 395*Power(t2,4) - 
                  4*Power(t1,3)*(-361 + 45*t2) + 
                  2*Power(t1,2)*(-2049 + 148*t2 + 969*Power(t2,2)) - 
                  t1*(-1741 + 3656*t2 + 9248*Power(t2,2) + 
                     2913*Power(t2,3))) + 
               s2*(1047 + 70*Power(t1,4) + 2346*t2 - 1396*Power(t2,2) - 
                  1246*Power(t2,3) - 553*Power(t2,4) + 50*Power(t2,5) - 
                  2*Power(t1,3)*(1063 + 677*t2 + 50*Power(t2,2)) + 
                  Power(t1,2)*
                   (6018 + 6826*t2 + 602*Power(t2,2) + 260*Power(t2,3)) \
+ t1*(-4522 - 8442*t2 + 1139*Power(t2,2) + 2583*Power(t2,3) - 
                     210*Power(t2,4)))) - 
            s1*(247 + 28*Power(t1,5) - 621*t2 - 1332*Power(t2,2) - 
               106*Power(t2,3) - 455*Power(t2,4) + 227*Power(t2,5) - 
               16*Power(t2,6) + Power(s2,6)*(-8 + 11*t1 + 35*t2) + 
               Power(t1,4)*(688 + 263*t2 + 60*Power(t2,2)) - 
               2*Power(t1,3)*
                (1159 + 312*t2 + 23*Power(t2,2) + 32*Power(t2,3)) + 
               Power(t1,2)*(2170 + 1140*t2 - 2295*Power(t2,2) + 
                  115*Power(t2,3) - 48*Power(t2,4)) + 
               t1*(-831 - 18*t2 + 2974*Power(t2,2) + 1714*Power(t2,3) - 
                  707*Power(t2,4) + 68*Power(t2,5)) - 
               2*Power(s2,5)*
                (-13 + 34*Power(t1,2) + 67*t2 - 75*Power(t2,2) + 
                  t1*(-85 + 50*t2)) + 
               Power(s2,4)*(156 - 136*Power(t1,3) + 1579*t2 + 
                  584*Power(t2,2) - 448*Power(t2,3) + 
                  2*Power(t1,2)*(632 + 587*t2) - 
                  t1*(1719 + 3220*t2 + 920*Power(t2,2))) + 
               Power(s2,3)*(1 - 2636*t2 - 4404*Power(t2,2) - 
                  1394*Power(t2,3) - 101*Power(t2,4) + 
                  4*Power(t1,3)*(-213 + 56*t2) + 
                  Power(t1,2)*(618 - 2881*t2 - 2497*Power(t2,2)) + 
                  4*t1*(273 + 1484*t2 + 2211*Power(t2,2) + 
                     693*Power(t2,3))) + 
               s2*(228 + 3465*t2 + 2024*Power(t2,2) - 948*Power(t2,3) - 
                  542*Power(t2,4) - 209*Power(t2,5) + 30*Power(t2,6) - 
                  Power(t1,4)*(829 + 96*t2) - 
                  4*Power(t1,3)*
                   (-89 + 573*t2 + 241*Power(t2,2) + 20*Power(t2,3)) + 
                  Power(t1,2)*
                   (2658 + 8840*t2 + 6077*Power(t2,2) + 
                     521*Power(t2,3) + 170*Power(t2,4)) - 
                  4*t1*(499 + 2434*t2 + 2050*Power(t2,2) - 
                     210*Power(t2,3) - 237*Power(t2,4) + 30*Power(t2,5))\
) + Power(s2,2)*(-196*Power(t1,4) + 
                  2*Power(t1,3)*(2015 + 1568*t2 + 50*Power(t2,2)) + 
                  Power(t1,2)*
                   (-7737 - 10234*t2 - 910*Power(t2,2) + 
                     859*Power(t2,3)) + 
                  t1*(3631 + 8246*t2 - 1824*Power(t2,2) - 
                     5430*Power(t2,3) - 1213*Power(t2,4)) + 
                  2*(-342 - 823*t2 + 1455*Power(t2,2) + 
                     1315*Power(t2,3) + 402*Power(t2,4) + 87*Power(t2,5))\
))) + Power(s,2)*(-70 + 126*t1 - 490*Power(t1,2) + 1714*Power(t1,3) - 
            1952*Power(t1,4) + 636*Power(t1,5) + 20*Power(t1,6) + 
            465*t2 - 584*t1*t2 - 1399*Power(t1,2)*t2 + 
            2816*Power(t1,3)*t2 - 1437*Power(t1,4)*t2 + 
            207*Power(t1,5)*t2 + 862*Power(t2,2) - 1417*t1*Power(t2,2) + 
            728*Power(t1,2)*Power(t2,2) - 237*Power(t1,3)*Power(t2,2) - 
            49*Power(t1,4)*Power(t2,2) + 45*Power(t1,5)*Power(t2,2) - 
            46*Power(t2,3) - 456*t1*Power(t2,3) + 
            1032*Power(t1,2)*Power(t2,3) - 308*Power(t1,3)*Power(t2,3) - 
            14*Power(t1,4)*Power(t2,3) - 134*Power(t2,4) - 
            18*t1*Power(t2,4) - 146*Power(t1,2)*Power(t2,4) + 
            25*Power(t1,3)*Power(t2,4) + 4*Power(t1,4)*Power(t2,4) + 
            137*Power(t2,5) + 170*t1*Power(t2,5) - 
            45*Power(t1,2)*Power(t2,5) - 10*Power(t1,3)*Power(t2,5) - 
            90*Power(t2,6) - 11*t1*Power(t2,6) + 
            8*Power(t1,2)*Power(t2,6) + 12*Power(t2,7) - 
            2*t1*Power(t2,7) - 
            Power(s2,7)*(-2 + (4 + t1)*t2 + 8*Power(t2,2)) + 
            Power(s2,6)*(-6 - 4*Power(t1,3) + 36*t2 + 38*Power(t2,2) - 
               44*Power(t2,3) + Power(t1,2)*(39 + 31*t2) + 
               t1*(-41 - 77*t2 + 37*Power(t2,2))) + 
            Power(s2,5)*(-18 - 152*t2 - 627*Power(t2,2) - 
               166*Power(t2,3) + 102*Power(t2,4) + 
               Power(t1,3)*(-29 + 36*t2) - 
               Power(t1,2)*(83 + 534*t2 + 380*Power(t2,2)) + 
               t1*(176 + 699*t2 + 1013*Power(t2,2) + 248*Power(t2,3))) + 
            Power(s2,4)*(28 - 40*Power(t1,4) - 394*t2 + 
               1011*Power(t2,2) + 1551*Power(t2,3) + 489*Power(t2,4) + 
               52*Power(t2,5) + 
               Power(t1,3)*(740 + 727*t2 + 9*Power(t2,2)) + 
               2*Power(t1,2)*
                (-577 - 639*t2 + 372*Power(t2,2) + 358*Power(t2,3)) + 
               t1*(306 + 797*t2 - 1637*Power(t2,2) - 2744*Power(t2,3) - 
                  801*Power(t2,4))) + 
            Power(s2,3)*(-17 + 1350*t2 + 1891*Power(t2,2) - 
               98*Power(t2,3) - 621*Power(t2,4) - 284*Power(t2,5) - 
               93*Power(t2,6) + Power(t1,4)*(-381 + 14*t2) - 
               Power(t1,3)*(417 + 2957*t2 + 1980*Power(t2,2) + 
                  126*Power(t2,3)) + 
               Power(t1,2)*(2028 + 6431*t2 + 7541*Power(t2,2) + 
                  1335*Power(t2,3) - 283*Power(t2,4)) + 
               t1*(-1023 - 4581*t2 - 7604*Power(t2,2) - 
                  1612*Power(t2,3) + 1659*Power(t2,4) + 497*Power(t2,5))\
) + Power(s2,2)*(51 - 19*Power(t1,5) + 141*t2 - 1945*Power(t2,2) - 
               1517*Power(t2,3) + 79*Power(t2,4) + 3*Power(t2,5) + 
               43*Power(t2,6) + 9*Power(t2,7) + 
               Power(t1,4)*(2322 + 1825*t2 + 244*Power(t2,2)) + 
               Power(t1,3)*(-5167 - 5577*t2 + 145*Power(t2,2) + 
                  816*Power(t2,3) + 69*Power(t2,4)) - 
               2*Power(t1,2)*
                (-1470 - 3113*t2 + 1215*Power(t2,2) + 
                  2544*Power(t2,3) + 466*Power(t2,4) + 17*Power(t2,5)) \
+ t1*(-299 - 2733*t2 + 3986*Power(t2,2) + 5852*Power(t2,3) + 
                  1405*Power(t2,4) - 347*Power(t2,5) - 32*Power(t2,6))) \
+ s2*(24 - 1353*t2 - 681*Power(t2,2) + 1059*Power(t2,3) + 
               276*Power(t2,4) - 63*Power(t2,5) + 3*Power(t2,6) - 
               19*Power(t2,7) + 2*Power(t2,8) - 
               Power(t1,5)*(619 + 134*t2) - 
               Power(t1,4)*(-43 + 1136*t2 + 762*Power(t2,2) + 
                  114*Power(t2,3)) + 
               Power(t1,3)*(3005 + 5760*t2 + 4528*Power(t2,2) + 
                  561*Power(t2,3) + 48*Power(t2,4) - 4*Power(t2,5)) + 
               Power(t1,2)*(-3038 - 9088*t2 - 7901*Power(t2,2) - 
                  603*Power(t2,3) + 671*Power(t2,4) + Power(t2,5) + 
                  10*Power(t2,6)) + 
               t1*(667 + 5858*t2 + 4836*Power(t2,2) - 724*Power(t2,3) - 
                  1183*Power(t2,4) + 38*Power(t2,5) + 60*Power(t2,6) - 
                  8*Power(t2,7))) + 
            Power(s1,6)*(75*Power(s2,3) + 
               2*s2*(-26 + 7*Power(t1,2) + t1*(83 - 2*t2) - 22*t2) + 
               Power(s2,2)*(30 - 246*t1 + 49*t2) - 
               2*(8*t1 + Power(t1,2) - 3*(1 + t2))) + 
            Power(s1,5)*(50 + 156*Power(s2,4) - 36*Power(t1,3) + 
               Power(s2,3)*(25 - 535*t1 - 311*t2) + 16*t2 - 
               30*Power(t2,2) + Power(t1,2)*(55 + 51*t2) + 
               t1*(-217 + 87*t2 - 10*Power(t2,2)) - 
               Power(s2,2)*(418 + 258*Power(t1,2) + 376*t2 + 
                  295*Power(t2,2) - t1*(853 + 1474*t2)) + 
               s2*(23 - 21*Power(t1,3) + 257*t2 + 250*Power(t2,2) - 
                  2*Power(t2,3) - 3*Power(t1,2)*(33 + 20*t2) + 
                  t1*(306 - 972*t2 + 33*Power(t2,2)))) + 
            Power(s1,4)*(-107 + 84*Power(s2,5) + 15*Power(t1,4) - 
               277*t2 - 175*Power(t2,2) + 72*Power(t2,3) + 
               30*Power(t1,3)*(2 + 3*t2) - 
               Power(s2,4)*(175 + 324*t1 + 716*t2) - 
               2*Power(t1,2)*(168 + 112*t2 + 73*Power(t2,2)) + 
               t1*(147 + 1266*t2 - 262*Power(t2,2) + 38*Power(t2,3)) + 
               Power(s2,3)*(-995 - 700*Power(t1,2) - 661*t2 + 
                  339*Power(t2,2) + t1*(2404 + 2893*t2)) + 
               Power(s2,2)*(341 - 150*Power(t1,3) + 2169*t2 + 
                  1332*Power(t2,2) + 699*Power(t2,3) + 
                  15*Power(t1,2)*(-49 + 87*t2) + 
                  t1*(522 - 4197*t2 - 3539*Power(t2,2))) + 
               s2*(771 - 485*Power(t1,3) + 318*t2 - 376*Power(t2,2) - 
                  579*Power(t2,3) + 10*Power(t2,4) + 
                  Power(t1,2)*(2285 + 832*t2 + 150*Power(t2,2)) - 
                  t1*(2527 + 2081*t2 - 2143*Power(t2,2) + 
                     100*Power(t2,3)))) + 
            Power(s1,3)*(-414 - 6*Power(s2,6) + 193*t2 + 
               330*Power(t2,2) + 459*Power(t2,3) - 108*Power(t2,4) - 
               2*Power(t1,4)*(127 + 35*t2) - 
               2*Power(s2,5)*(69 + 3*t1 + 196*t2) + 
               Power(t1,3)*(1028 + 72*t2 - 32*Power(t2,2)) + 
               2*Power(t1,2)*
                (-1126 + 283*t2 + 171*Power(t2,2) + 72*Power(t2,3)) + 
               t1*(1712 + 118*t2 - 2555*Power(t2,2) + 
                  435*Power(t2,3) - 52*Power(t2,4)) + 
               Power(s2,4)*(-536 - 604*Power(t1,2) + 87*t2 + 
                  1167*Power(t2,2) + 4*t1*(495 + 397*t2)) + 
               Power(s2,3)*(1111 - 280*Power(t1,3) + 4799*t2 + 
                  2292*Power(t2,2) + 192*Power(t2,3) + 
                  760*Power(t1,2)*(1 + 4*t2) - 
                  t1*(2312 + 9861*t2 + 6203*Power(t2,2))) + 
               Power(s2,2)*(1477 - 546*t2 - 3880*Power(t2,2) - 
                  2083*Power(t2,3) - 826*Power(t2,4) + 
                  36*Power(t1,3)*(-56 + 5*t2) + 
                  Power(t1,2)*(7208 + 3878*t2 - 2162*Power(t2,2)) + 
                  t1*(-5907 - 4662*t2 + 7580*Power(t2,2) + 
                     4265*Power(t2,3))) + 
               s2*(-931 - 70*Power(t1,4) - 3908*t2 - 1134*Power(t2,2) + 
                  103*Power(t2,3) + 696*Power(t2,4) - 20*Power(t2,5) + 
                  2*Power(t1,3)*(446 + 803*t2 + 50*Power(t2,2)) - 
                  2*Power(t1,2)*
                   (1129 + 4222*t2 + 974*Power(t2,2) + 120*Power(t2,3)) \
+ t1*(2306 + 10762*t2 + 4537*Power(t2,2) - 2317*Power(t2,3) + 
                     150*Power(t2,4)))) - 
            Power(s1,2)*(-717 + 9*Power(s2,7) - 42*Power(t1,5) - 
               1938*t2 + 208*Power(t2,2) - 108*Power(t2,3) + 
               577*Power(t2,4) - 102*Power(t2,5) + 
               Power(s2,6)*(14 - 32*t1 + 11*t2) - 
               Power(t1,4)*(668 + 429*t2 + 90*Power(t2,2)) + 
               Power(t1,3)*(3354 + 2508*t2 + 242*Power(t2,2) + 
                  72*Power(t2,3)) + 
               Power(t1,2)*(-4896 - 6966*t2 + 321*Power(t2,2) + 
                  277*Power(t2,3) + 36*Power(t2,4)) + 
               t1*(2918 + 6336*t2 + 673*Power(t2,2) - 
                  2350*Power(t2,3) + 363*Power(t2,4) - 28*Power(t2,5)) \
+ Power(s2,5)*(17 + 192*Power(t1,2) - 148*t2 - 656*Power(t2,2) - 
                  4*t1*(119 + 19*t2)) + 
               Power(s2,4)*(-610 + 204*Power(t1,3) - 2969*t2 - 
                  892*Power(t2,2) + 758*Power(t2,3) - 
                  2*Power(t1,2)*(836 + 1119*t2) + 
                  t1*(2738 + 6825*t2 + 3147*Power(t2,2))) + 
               Power(s2,3)*(-434 + Power(t1,3)*(1908 - 336*t2) + 
                  2959*t2 + 7327*Power(t2,2) + 2985*Power(t2,3) + 
                  625*Power(t2,4) + 
                  Power(t1,2)*(-5072 + 539*t2 + 4167*Power(t2,2)) - 
                  t1*(-2489 + 3231*t2 + 14294*Power(t2,2) + 
                     6364*Power(t2,3))) + 
               Power(s2,2)*(1723 + 294*Power(t1,4) + 6385*t2 + 
                  46*Power(t2,2) - 2928*Power(t2,3) - 
                  1567*Power(t2,4) - 499*Power(t2,5) - 
                  2*Power(t1,3)*(1996 + 2811*t2 + 75*Power(t2,2)) + 
                  Power(t1,2)*
                   (9733 + 21752*t2 + 6876*Power(t2,2) - 
                     1407*Power(t2,3)) + 
                  t1*(-6963 - 21581*t2 - 9695*Power(t2,2) + 
                     6328*Power(t2,3) + 2611*Power(t2,4))) + 
               s2*(526 - 3185*t2 - 5735*Power(t2,2) - 
                  1159*Power(t2,3) - 157*Power(t2,4) + 
                  454*Power(t2,5) - 20*Power(t2,6) + 
                  3*Power(t1,4)*(539 + 48*t2) + 
                  2*Power(t1,3)*
                   (-3170 + 176*t2 + 795*Power(t2,2) + 60*Power(t2,3)) \
- Power(t1,2)*(-7244 + 1942*t2 + 10479*Power(t2,2) + 
                     1797*Power(t2,3) + 210*Power(t2,4)) + 
                  3*t1*(-1022 + 1440*t2 + 4994*Power(t2,2) + 
                     1339*Power(t2,3) - 441*Power(t2,4) + 40*Power(t2,5)\
))) + s1*(-182 - 2209*t2 - 1456*Power(t2,2) + 256*Power(t2,3) - 
               348*Power(t2,4) + 361*Power(t2,5) - 54*Power(t2,6) + 
               Power(s2,7)*(-2 + 3*t1 + 19*t2) - 
               Power(t1,5)*(368 + 99*t2) + 
               Power(s2,6)*(33 - 16*Power(t1,2) + t1*(19 - 80*t2) - 
                  29*t2 + 63*Power(t2,2)) - 
               Power(t1,4)*(-811 + 343*t2 + 164*Power(t2,2) + 
                  39*Power(t2,3)) + 
               Power(t1,3)*(344 + 3038*t2 + 1835*Power(t2,2) + 
                  85*Power(t2,3) + 60*Power(t2,4)) - 
               Power(t1,2)*(1673 + 6198*t2 + 5780*Power(t2,2) - 
                  237*Power(t2,3) - 149*Power(t2,4) + 19*Power(t2,5)) - 
               2*t1*(-517 - 2904*t2 - 2525*Power(t2,2) - 
                  213*Power(t2,3) + 507*Power(t2,4) - 65*Power(t2,5) + 
                  Power(t2,6)) + 
               Power(s2,5)*(10 - 57*Power(t1,3) + 649*t2 + 
                  155*Power(t2,2) - 450*Power(t2,3) + 
                  Power(t1,2)*(599 + 576*t2) - 
                  t1*(705 + 1428*t2 + 329*Power(t2,2))) + 
               Power(s2,4)*(-30 - 1725*t2 - 4022*Power(t2,2) - 
                  1293*Power(t2,3) + 99*Power(t2,4) + 
                  2*Power(t1,3)*(-268 + 99*t2) + 
                  Power(t1,2)*(223 - 2558*t2 - 2355*Power(t2,2)) + 
                  t1*(761 + 4438*t2 + 7662*Power(t2,2) + 
                     2684*Power(t2,3))) + 
               Power(s2,3)*(-485 - 219*Power(t1,4) - 2655*t2 + 
                  1948*Power(t2,2) + 4144*Power(t2,3) + 
                  1613*Power(t2,4) + 423*Power(t2,5) + 
                  Power(t1,3)*(4224 + 4107*t2 + 81*Power(t2,2)) + 
                  Power(t1,2)*
                   (-7721 - 13029*t2 - 1690*Power(t2,2) + 
                     2110*Power(t2,3)) + 
                  t1*(3524 + 10890*t2 + 774*Power(t2,2) - 
                     8496*Power(t2,3) - 3016*Power(t2,4))) - 
               Power(s2,2)*(-122 - 4501*t2 - 6471*Power(t2,2) - 
                  172*Power(t2,3) + 802*Power(t2,4) + 513*Power(t2,5) + 
                  135*Power(t2,6) + Power(t1,4)*(1957 + 27*t2) + 
                  Power(t1,3)*
                   (-3261 + 4024*t2 + 4390*Power(t2,2) + 
                     249*Power(t2,3)) + 
                  Power(t1,2)*
                   (-301 - 12114*t2 - 19778*Power(t2,2) - 
                     4665*Power(t2,3) + 258*Power(t2,4)) + 
                  t1*(1235 + 12001*t2 + 21720*Power(t2,2) + 
                     6960*Power(t2,3) - 2439*Power(t2,4) - 
                     689*Power(t2,5))) + 
               s2*(691 - 3*Power(t1,5) + 1812*t2 - 3302*Power(t2,2) - 
                  2874*Power(t2,3) - 303*Power(t2,4) - 92*Power(t2,5) + 
                  150*Power(t2,6) - 10*Power(t2,7) + 
                  Power(t1,4)*(3487 + 2518*t2 + 321*Power(t2,2)) + 
                  Power(t1,3)*
                   (-10663 - 11968*t2 - 1205*Power(t2,2) + 
                     421*Power(t2,3) + 45*Power(t2,4)) + 
                  Power(t1,2)*
                   (10416 + 18368*t2 + 1048*Power(t2,2) - 
                     4991*Power(t2,3) - 583*Power(t2,4) - 84*Power(t2,5)\
) + t1*(-4011 - 10663*t2 + 2697*Power(t2,2) + 7930*Power(t2,3) + 
                     1217*Power(t2,4) - 403*Power(t2,5) + 49*Power(t2,6))\
))) + s*(6 - 30*t1 - 252*Power(t1,2) + 522*Power(t1,3) + 82*Power(t1,4) - 
            576*Power(t1,5) + 248*Power(t1,6) - 488*t2 + 1577*t1*t2 - 
            1859*Power(t1,2)*t2 + 486*Power(t1,3)*t2 + 
            677*Power(t1,4)*t2 - 449*Power(t1,5)*t2 + 56*Power(t1,6)*t2 - 
            470*Power(t2,2) + 2*Power(s2,8)*Power(t2,2) + 
            1155*t1*Power(t2,2) - 1038*Power(t1,2)*Power(t2,2) + 
            461*Power(t1,3)*Power(t2,2) - 124*Power(t1,4)*Power(t2,2) + 
            18*Power(t1,5)*Power(t2,2) + 324*Power(t2,3) - 
            738*t1*Power(t2,3) + 372*Power(t1,2)*Power(t2,3) + 
            59*Power(t1,3)*Power(t2,3) - 67*Power(t1,4)*Power(t2,3) + 
            19*Power(t1,5)*Power(t2,3) + 154*Power(t2,4) - 
            280*t1*Power(t2,4) + 262*Power(t1,2)*Power(t2,4) - 
            59*Power(t1,3)*Power(t2,4) - 20*Power(t1,4)*Power(t2,4) - 
            80*Power(t2,5) + 5*t1*Power(t2,5) - Power(t1,2)*Power(t2,5) + 
            11*Power(t1,3)*Power(t2,5) + 54*Power(t2,6) + 
            3*t1*Power(t2,6) - 12*Power(t1,2)*Power(t2,6) - 
            12*Power(t2,7) + 4*t1*Power(t2,7) + 
            Power(s2,7)*t2*(8 + 9*Power(t2,2) - 2*t1*(2 + 7*t2)) + 
            Power(s2,6)*(6 - 10*t2 - 8*Power(t1,3)*t2 + 
               109*Power(t2,2) + 11*Power(t2,3) - 57*Power(t2,4) + 
               Power(t1,2)*(2 + 104*t2 + 77*Power(t2,2)) - 
               t1*(8 + 106*t2 + 183*Power(t2,2) + 26*Power(t2,3))) + 
            Power(s2,5)*(-10 + 8*Power(t1,4) + 79*t2 - 364*Power(t2,2) - 
               705*Power(t2,3) - 202*Power(t2,4) + 8*Power(t2,5) + 
               Power(t1,3)*(-98 - 158*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(182 + 145*t2 - 365*Power(t2,2) - 
                  304*Power(t2,3)) + 
               t1*(-82 + 6*t2 + 685*Power(t2,2) + 1093*Power(t2,3) + 
                  339*Power(t2,4))) + 
            Power(s2,4)*(-13 + Power(t1,4)*(81 - 22*t2) - 389*t2 - 
               1179*Power(t2,2) - 92*Power(t2,3) + 579*Power(t2,4) + 
               265*Power(t2,5) + 69*Power(t2,6) + 
               Power(t1,3)*(25 + 1248*t2 + 986*Power(t2,2) + 
                  69*Power(t2,3)) + 
               Power(t1,2)*(-378 - 2445*t2 - 3502*Power(t2,2) - 
                  729*Power(t2,3) + 237*Power(t2,4)) + 
               t1*(285 + 1440*t2 + 3766*Power(t2,2) + 1002*Power(t2,3) - 
                  1128*Power(t2,4) - 421*Power(t2,5))) + 
            Power(s2,3)*(-40 + 15*Power(t1,5) - 578*t2 + 
               1002*Power(t2,2) + 1753*Power(t2,3) + 632*Power(t2,4) + 
               144*Power(t2,5) - 34*Power(t2,6) - 31*Power(t2,7) - 
               Power(t1,4)*(865 + 1136*t2 + 186*Power(t2,2)) + 
               Power(t1,3)*(2174 + 2997*t2 - 80*Power(t2,2) - 
                  878*Power(t2,3) - 87*Power(t2,4)) + 
               Power(t1,2)*(-1685 - 3258*t2 + 1042*Power(t2,2) + 
                  4818*Power(t2,3) + 1341*Power(t2,4) + 10*Power(t2,5)) \
+ t1*(401 + 2187*t2 - 1885*Power(t2,2) - 5508*Power(t2,3) - 
                  2559*Power(t2,4) + 173*Power(t2,5) + 119*Power(t2,6))) \
- s2*(-56 + 56*Power(t1,6) - 383*t2 + 448*Power(t2,2) + 
               607*Power(t2,3) - 412*Power(t2,4) - 193*Power(t2,5) + 
               32*Power(t2,6) - 15*Power(t2,7) + 4*Power(t2,8) + 
               3*Power(t1,5)*(391 + 264*t2 + 69*Power(t2,2)) + 
               Power(t1,4)*(-4221 - 4478*t2 - 1117*Power(t2,2) - 
                  10*Power(t2,3) + 38*Power(t2,4)) + 
               Power(t1,3)*(4806 + 9411*t2 + 2911*Power(t2,2) - 
                  939*Power(t2,3) - 141*Power(t2,4) - 40*Power(t2,5)) + 
               Power(t1,2)*(-2109 - 8963*t2 - 3138*Power(t2,2) + 
                  1968*Power(t2,3) + 473*Power(t2,4) - 
                  197*Power(t2,5) + 14*Power(t2,6)) + 
               t1*(351 + 3589*t2 + 815*Power(t2,2) - 1654*Power(t2,3) + 
                  169*Power(t2,4) + 325*Power(t2,5) + 17*Power(t2,6) - 
                  12*Power(t2,7))) + 
            Power(s2,2)*(-37 + 935*t2 + 1575*Power(t2,2) + 
               390*Power(t2,3) - 91*Power(t2,4) + 95*Power(t2,5) - 
               15*Power(t2,6) + 12*Power(t2,7) + 
               Power(t1,5)*(454 + 173*t2) + 
               Power(t1,4)*(-607 + 1651*t2 + 1185*Power(t2,2) + 
                  238*Power(t2,3)) + 
               Power(t1,3)*(-785 - 5446*t2 - 7708*Power(t2,2) - 
                  1580*Power(t2,3) - 38*Power(t2,4) + 19*Power(t2,5)) + 
               Power(t1,2)*(1326 + 5922*t2 + 13401*Power(t2,2) + 
                  4829*Power(t2,3) - 1093*Power(t2,4) - 
                  193*Power(t2,5) - 20*Power(t2,6)) + 
               t1*(-351 - 3367*t2 - 8247*Power(t2,2) - 
                  4139*Power(t2,3) + 957*Power(t2,4) + 539*Power(t2,5) - 
                  107*Power(t2,6) + 3*Power(t2,7))) - 
            Power(s1,7)*s2*(39*Power(s2,2) + s2*(20 - 97*t1 + 28*t2) + 
               2*(16*t1 + Power(t1,2) - 6*(1 + t2))) + 
            Power(s1,6)*(-114*Power(s2,4) + 13*Power(t1,3) - 
               12*(1 + t2) + 4*t1*(11 + 3*t2) - 
               2*Power(t1,2)*(11 + 4*t2) + 
               Power(s2,3)*(-77 + 266*t1 + 155*t2) + 
               Power(s2,2)*(171 + 113*Power(t1,2) + 168*t2 + 
                  181*Power(t2,2) - t1*(155 + 686*t2)) + 
               s2*(48 + 7*Power(t1,3) - 72*Power(t2,2) + 
                  Power(t1,2)*(109 + 17*t2) - 
                  2*t1*(133 - 94*t2 + 2*Power(t2,2)))) + 
            Power(s1,5)*(-112*Power(s2,5) - 6*Power(t1,4) + 
               Power(t1,3)*(33 - 36*t2) + 
               Power(s2,4)*(54 + 226*t1 + 542*t2) + 
               t1*(177 - 216*t2 - 40*Power(t2,2)) + 
               4*(-7 + 4*t2 + 15*Power(t2,2)) + 
               Power(t1,2)*(-129 + 76*t2 + 28*Power(t2,2)) + 
               Power(s2,3)*(731 + 362*Power(t1,2) + 661*t2 - 
                  110*Power(t2,2) - 2*t1*(530 + 881*t2)) + 
               Power(s2,2)*(241 + 60*Power(t1,3) + 
                  Power(t1,2)*(866 - 612*t2) - 657*t2 - 
                  532*Power(t2,2) - 485*Power(t2,3) + 
                  t1*(-1198 + 698*t2 + 1955*Power(t2,2))) + 
               s2*(-325 + 250*Power(t1,3) - 392*t2 - 220*Power(t2,2) + 
                  184*Power(t2,3) - 
                  3*Power(t1,2)*(259 + 251*t2 + 20*Power(t2,2)) + 
                  2*t1*(334 + 888*t2 - 215*Power(t2,2) + 10*Power(t2,3)))\
) + Power(s1,4)*(193 - 36*Power(s2,6) + 216*t2 + 91*Power(t2,2) - 
               132*Power(t2,3) + Power(t1,4)*(141 + 35*t2) + 
               Power(s2,5)*(165 + 34*t1 + 526*t2) + 
               2*Power(t1,3)*(-324 - 110*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(982 + 886*t2 - 111*Power(t2,2) - 
                  32*Power(t2,3)) + 
               t1*(-613 - 1170*t2 + 457*Power(t2,2) + 44*Power(t2,3)) + 
               Power(s2,4)*(780 + 406*Power(t1,2) + 191*t2 - 
                  924*Power(t2,2) - 2*t1*(727 + 733*t2)) + 
               Power(s2,3)*(-355 + 140*Power(t1,3) + 
                  Power(t1,2)*(476 - 1790*t2) - 3250*t2 - 
                  1962*Power(t2,2) - 361*Power(t2,3) + 
                  t1*(-365 + 4869*t2 + 4579*Power(t2,2))) + 
               Power(s2,2)*(-1607 + Power(t1,3)*(1294 - 90*t2) - 
                  1972*t2 + 733*Power(t2,2) + 860*Power(t2,3) + 
                  690*Power(t2,4) + 
                  Power(t1,2)*(-4126 - 4690*t2 + 1193*Power(t2,2)) + 
                  t1*(4049 + 7373*t2 - 1099*Power(t2,2) - 
                     2897*Power(t2,3))) + 
               s2*(-323 + 35*Power(t1,4) + 1155*t2 + 967*Power(t2,2) + 
                  655*Power(t2,3) - 260*Power(t2,4) + 
                  Power(t1,3)*(520 - 929*t2 - 50*Power(t2,2)) + 
                  Power(t1,2)*
                   (-2094 + 3105*t2 + 1847*Power(t2,2) + 
                     110*Power(t2,3)) + 
                  t1*(1718 - 2592*t2 - 4303*Power(t2,2) + 
                     492*Power(t2,3) - 40*Power(t2,4)))) + 
            Power(s1,3)*(159 + 3*Power(s2,7) - 28*Power(t1,5) - 553*t2 - 
               335*Power(t2,2) - 303*Power(t2,3) + 168*Power(t2,4) + 
               Power(s2,6)*(64 - 31*t1 + 170*t2) + 
               Power(t1,4)*(16 - 309*t2 - 60*Power(t2,2)) + 
               2*Power(t1,3)*
                (-263 + 886*t2 + 203*Power(t2,2) + 16*Power(t2,3)) + 
               Power(t1,2)*(1270 - 2954*t2 - 1727*Power(t2,2) + 
                  109*Power(t2,3) + 8*Power(t2,4)) - 
               2*t1*(429 - 911*t2 - 1129*Power(t2,2) + 
                  259*Power(t2,3) + 8*Power(t2,4)) + 
               Power(s2,5)*(179 + 188*Power(t1,2) - 349*t2 - 
                  919*Power(t2,2) - 4*t1*(151 + 83*t2)) + 
               Power(s2,4)*(-841 + 136*Power(t1,3) - 3495*t2 - 
                  1242*Power(t2,2) + 591*Power(t2,3) - 
                  2*Power(t1,2)*(368 + 905*t2) + 
                  2*t1*(865 + 3027*t2 + 1791*Power(t2,2))) + 
               Power(s2,3)*(-1488 + 25*t2 + 5118*Power(t2,2) + 
                  2724*Power(t2,3) + 809*Power(t2,4) - 
                  4*Power(t1,3)*(-423 + 56*t2) + 
                  Power(t1,2)*(-6164 - 3309*t2 + 3059*Power(t2,2)) + 
                  t1*(5262 + 4886*t2 - 8232*Power(t2,2) - 
                     5944*Power(t2,3))) + 
               Power(s2,2)*(1041 + 196*Power(t1,4) + 6429*t2 + 
                  4486*Power(t2,2) + 48*Power(t2,3) - 780*Power(t2,4) - 
                  550*Power(t2,5) - 
                  2*Power(t1,3)*(171 + 2180*t2 + 50*Power(t2,2)) + 
                  Power(t1,2)*
                   (707 + 14892*t2 + 9202*Power(t2,2) - 
                     1017*Power(t2,3)) + 
                  t1*(-1479 - 15448*t2 - 15736*Power(t2,2) + 
                     766*Power(t2,3) + 2363*Power(t2,4))) + 
               s2*(1709 + 2329*t2 - 1525*Power(t2,2) - 997*Power(t2,3) - 
                  840*Power(t2,4) + 220*Power(t2,5) + 
                  Power(t1,4)*(1327 + 96*t2) + 
                  4*Power(t1,3)*
                   (-1538 - 719*t2 + 289*Power(t2,2) + 20*Power(t2,3)) + 
                  Power(t1,2)*
                   (9324 + 10184*t2 - 4445*Power(t2,2) - 
                     2099*Power(t2,3) - 110*Power(t2,4)) + 
                  t1*(-6002 - 9336*t2 + 3616*Power(t2,2) + 
                     4930*Power(t2,3) - 308*Power(t2,4) + 40*Power(t2,5))\
)) + Power(s1,2)*(-719 + 2*Power(s2,8) - 946*t2 + 656*Power(t2,2) + 
               54*Power(t2,3) + 383*Power(t2,4) - 132*Power(t2,5) - 
               Power(s2,7)*(-2 + 8*t1 + t2) + 
               Power(t1,5)*(412 + 99*t2) + 
               Power(t1,4)*(-2789 - 468*t2 + 181*Power(t2,2) + 
                  39*Power(t2,3)) + 
               Power(t1,3)*(6441 + 2794*t2 - 1589*Power(t2,2) - 
                  273*Power(t2,3) - 39*Power(t2,4)) + 
               t1*(3245 + 4057*t2 - 1973*Power(t2,2) - 
                  1709*Power(t2,3) + 312*Power(t2,4) + 4*Power(t2,5)) + 
               Power(t1,2)*(-6588 - 5633*t2 + 3061*Power(t2,2) + 
                  1311*Power(t2,3) - 91*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,6)*(-19 + 29*Power(t1,2) - 133*t2 - 
                  293*Power(t2,2) + t1*(-51 + 80*t2)) + 
               Power(s2,5)*(-216 + 57*Power(t1,3) - 1100*t2 - 
                  13*Power(t2,2) + 716*Power(t2,3) - 
                  Power(t1,2)*(601 + 747*t2) + 
                  t1*(887 + 2293*t2 + 935*Power(t2,2))) + 
               Power(s2,4)*(-195 + Power(t1,3)*(769 - 198*t2) + 
                  1889*t2 + 5268*Power(t2,2) + 1960*Power(t2,3) + 
                  61*Power(t2,4) + 
                  2*Power(t1,2)*(-841 + 516*t2 + 1308*Power(t2,2)) - 
                  t1*(-803 + 2803*t2 + 8930*Power(t2,2) + 
                     4091*Power(t2,3))) + 
               Power(s2,3)*(1259 + 219*Power(t1,4) + 5759*t2 + 
                  1786*Power(t2,2) - 3266*Power(t2,3) - 
                  1855*Power(t2,4) - 667*Power(t2,5) - 
                  Power(t1,3)*(3284 + 4851*t2 + 81*Power(t2,2)) + 
                  Power(t1,2)*
                   (7076 + 18792*t2 + 6844*Power(t2,2) - 
                     2186*Power(t2,3)) + 
                  t1*(-4913 - 18411*t2 - 11626*Power(t2,2) + 
                     6270*Power(t2,3) + 3988*Power(t2,4))) + 
               Power(s2,2)*(850 - 1789*t2 - 8076*Power(t2,2) - 
                  3925*Power(t2,3) - 537*Power(t2,4) + 
                  400*Power(t2,5) + 233*Power(t2,6) + 
                  Power(t1,4)*(2462 + 27*t2) + 
                  Power(t1,3)*
                   (-9439 - 1856*t2 + 4628*Power(t2,2) + 
                     249*Power(t2,3)) + 
                  Power(t1,2)*
                   (10921 + 5677*t2 - 18233*Power(t2,2) - 
                     7991*Power(t2,3) + 318*Power(t2,4)) - 
                  t1*(4872 + 2395*t2 - 19540*Power(t2,2) - 
                     14684*Power(t2,3) + 359*Power(t2,4) + 
                     1004*Power(t2,5))) + 
               s2*(-1141 + 3*Power(t1,5) - 5720*t2 - 3352*Power(t2,2) + 
                  1078*Power(t2,3) + 389*Power(t2,4) + 538*Power(t2,5) - 
                  112*Power(t2,6) - 
                  Power(t1,4)*(1148 + 2780*t2 + 321*Power(t2,2)) + 
                  Power(t1,3)*
                   (4391 + 14324*t2 + 4453*Power(t2,2) - 
                     485*Power(t2,3) - 45*Power(t2,4)) + 
                  Power(t1,2)*
                   (-5838 - 24317*t2 - 14892*Power(t2,2) + 
                     2880*Power(t2,3) + 1110*Power(t2,4) + 
                     57*Power(t2,5)) + 
                  t1*(3675 + 18069*t2 + 13627*Power(t2,2) - 
                     2453*Power(t2,3) - 2742*Power(t2,4) + 
                     124*Power(t2,5) - 20*Power(t2,6)))) + 
            s1*(401 - 40*Power(t1,6) + 1767*t2 - 4*Power(s2,8)*t2 + 
               454*Power(t2,2) - 450*Power(t2,3) + 173*Power(t2,4) - 
               229*Power(t2,5) + 60*Power(t2,6) - 
               2*Power(t1,5)*(304 + 203*t2 + 45*Power(t2,2)) + 
               Power(t1,4)*(3492 + 3153*t2 + 560*Power(t2,2) + 
                  7*Power(t2,3) - 8*Power(t2,4)) + 
               Power(t1,3)*(-6211 - 8668*t2 - 2404*Power(t2,2) + 
                  524*Power(t2,3) + 43*Power(t2,4) + 12*Power(t2,5)) + 
               Power(t1,2)*(4931 + 10880*t2 + 4017*Power(t2,2) - 
                  1351*Power(t2,3) - 340*Power(t2,4) + 51*Power(t2,5) - 
                  4*Power(t2,6)) - 
               t1*(1965 + 6730*t2 + 2442*Power(t2,2) - 
                  1044*Power(t2,3) - 439*Power(t2,4) + 82*Power(t2,5) + 
                  8*Power(t2,6)) + 
               Power(s2,7)*(-8 - 2*t2 - 11*Power(t2,2) + 
                  t1*(4 + 22*t2)) + 
               Power(s2,6)*(8 + 8*Power(t1,3) - 78*t2 + 58*Power(t2,2) + 
                  216*Power(t2,3) - 98*Power(t1,2)*(1 + t2) + 
                  t1*(102 + 214*t2 - 23*Power(t2,2))) + 
               Power(s2,5)*(29 + Power(t1,3)*(102 - 72*t2) + 570*t2 + 
                  1636*Power(t2,2) + 399*Power(t2,3) - 219*Power(t2,4) + 
                  Power(t1,2)*(31 + 1000*t2 + 869*Power(t2,2)) - 
                  2*t1*(117 + 794*t2 + 1399*Power(t2,2) + 
                     488*Power(t2,3))) + 
               Power(s2,4)*(192 + 80*Power(t1,4) + 1416*t2 - 
                  953*Power(t2,2) - 3132*Power(t2,3) - 
                  1228*Power(t2,4) - 225*Power(t2,5) - 
                  Power(t1,3)*(1459 + 1786*t2 + 18*Power(t2,2)) + 
                  Power(t1,2)*
                   (2660 + 5176*t2 + 478*Power(t2,2) - 1449*Power(t2,3)) \
+ t1*(-1305 - 4572*t2 + 34*Power(t2,2) + 5458*Power(t2,3) + 
                     2170*Power(t2,4))) + 
               Power(s2,3)*(89 + Power(t1,4)*(993 - 28*t2) - 2491*t2 - 
                  6068*Power(t2,2) - 2088*Power(t2,3) + 
                  523*Power(t2,4) + 543*Power(t2,5) + 244*Power(t2,6) + 
                  2*Power(t1,3)*
                   (-817 + 1759*t2 + 2026*Power(t2,2) + 126*Power(t2,3)) \
+ Power(t1,2)*(96 - 8423*t2 - 17560*Power(t2,2) - 5352*Power(t2,3) + 
                     545*Power(t2,4)) + 
                  t1*(244 + 7174*t2 + 18800*Power(t2,2) + 
                     9664*Power(t2,3) - 2020*Power(t2,4) - 
                     1246*Power(t2,5))) + 
               Power(s2,2)*(-639 + 38*Power(t1,5) - 2933*t2 + 
                  332*Power(t2,2) + 3345*Power(t2,3) + 
                  1075*Power(t2,4) + 257*Power(t2,5) - 108*Power(t2,6) - 
                  41*Power(t2,7) - 
                  Power(t1,4)*(3658 + 3885*t2 + 488*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (-5016 - 9073*t2 - 1982*Power(t2,2) + 
                     762*Power(t2,3) + 69*Power(t2,4)) + 
                  Power(t1,2)*
                   (-9157 - 26406*t2 - 11490*Power(t2,2) + 
                     8560*Power(t2,3) + 2806*Power(t2,4) + 
                     25*Power(t2,5)) + 
                  t1*(3516 + 14950*t2 + 8157*Power(t2,2) - 
                     9098*Power(t2,3) - 5662*Power(t2,4) + 
                     256*Power(t2,5) + 169*Power(t2,6))) + 
               s2*(-36 + 1969*t2 + 4620*Power(t2,2) + 934*Power(t2,3) - 
                  576*Power(t2,4) + 17*Power(t2,5) - 160*Power(t2,6) + 
                  32*Power(t2,7) + 2*Power(t1,5)*(695 + 134*t2) + 
                  Power(t1,4)*
                   (-5195 - 424*t2 + 1421*Power(t2,2) + 228*Power(t2,3)) \
+ Power(t1,3)*(6726 - 276*t2 - 9136*Power(t2,2) - 2238*Power(t2,3) - 
                     32*Power(t2,4) + 8*Power(t2,5)) + 
                  Power(t1,2)*
                   (-3771 + 1953*t2 + 17059*Power(t2,2) + 
                     7275*Power(t2,3) - 960*Power(t2,4) - 
                     200*Power(t2,5) - 12*Power(t2,6)) + 
                  2*t1*(427 - 1653*t2 - 6887*Power(t2,2) - 
                     2920*Power(t2,3) + 543*Power(t2,4) + 
                     311*Power(t2,5) - 23*Power(t2,6) + 2*Power(t2,7))))))*
       R2q(1 - s1 - t1 + t2))/
     ((-1 + t1)*(s - s2 + t1)*(-1 + s1 + t1 - t2)*(-1 + t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,2)*
       Power(-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2),2)) - 
    (8*(288 + 390*s2 - 105*Power(s2,2) - 291*Power(s2,3) - 
         54*Power(s2,4) + 50*Power(s2,5) + 15*Power(s2,6) - 
         5*Power(s2,7) - 1962*t1 - 2270*s2*t1 + 1010*Power(s2,2)*t1 + 
         1792*Power(s2,3)*t1 + 260*Power(s2,4)*t1 - 282*Power(s2,5)*t1 - 
         76*Power(s2,6)*t1 + 24*Power(s2,7)*t1 + 5857*Power(t1,2) + 
         5417*s2*Power(t1,2) - 3824*Power(s2,2)*Power(t1,2) - 
         4770*Power(s2,3)*Power(t1,2) - 541*Power(s2,4)*Power(t1,2) + 
         719*Power(s2,5)*Power(t1,2) + 148*Power(s2,6)*Power(t1,2) - 
         46*Power(s2,7)*Power(t1,2) - 10016*Power(t1,3) - 
         6680*s2*Power(t1,3) + 7726*Power(s2,2)*Power(t1,3) + 
         7300*Power(s2,3)*Power(t1,3) + 558*Power(s2,4)*Power(t1,3) - 
         1056*Power(s2,5)*Power(t1,3) - 132*Power(s2,6)*Power(t1,3) + 
         44*Power(s2,7)*Power(t1,3) + 10708*Power(t1,4) + 
         4324*s2*Power(t1,4) - 9395*Power(s2,2)*Power(t1,4) - 
         7139*Power(s2,3)*Power(t1,4) - 132*Power(s2,4)*Power(t1,4) + 
         924*Power(s2,5)*Power(t1,4) + 43*Power(s2,6)*Power(t1,4) - 
         21*Power(s2,7)*Power(t1,4) - 7262*Power(t1,5) - 
         1186*s2*Power(t1,5) + 7330*Power(s2,2)*Power(t1,5) + 
         4532*Power(s2,3)*Power(t1,5) - 308*Power(s2,4)*Power(t1,5) - 
         446*Power(s2,5)*Power(t1,5) + 8*Power(s2,6)*Power(t1,5) + 
         4*Power(s2,7)*Power(t1,5) + 2973*Power(t1,6) - 
         131*s2*Power(t1,6) - 3790*Power(s2,2)*Power(t1,6) - 
         1696*Power(s2,3)*Power(t1,6) + 303*Power(s2,4)*Power(t1,6) + 
         91*Power(s2,5)*Power(t1,6) - 6*Power(s2,6)*Power(t1,6) - 
         616*Power(t1,7) + 232*s2*Power(t1,7) + 
         1254*Power(s2,2)*Power(t1,7) + 256*Power(s2,3)*Power(t1,7) - 
         86*Power(s2,4)*Power(t1,7) + 22*Power(t1,8) - 
         136*s2*Power(t1,8) - 206*Power(s2,2)*Power(t1,8) + 
         16*Power(s2,3)*Power(t1,8) + 8*Power(t1,9) + 40*s2*Power(t1,9) - 
         Power(s1,12)*Power(s2,2)*(s2 + t1) + 
         Power(s,10)*(-1 + s2)*(-2 + 2*s1 + 3*t1 - 3*t2)*
          Power(t1 - t2,2) + 366*t2 + 1412*s2*t2 + 1082*Power(s2,2)*t2 - 
         874*Power(s2,3)*t2 - 1261*Power(s2,4)*t2 - 142*Power(s2,5)*t2 + 
         244*Power(s2,6)*t2 + 32*Power(s2,7)*t2 - 15*Power(s2,8)*t2 - 
         1602*t1*t2 - 7152*s2*t1*t2 - 4597*Power(s2,2)*t1*t2 + 
         6252*Power(s2,3)*t1*t2 + 6645*Power(s2,4)*t1*t2 + 
         292*Power(s2,5)*t1*t2 - 1079*Power(s2,6)*t1*t2 - 
         132*Power(s2,7)*t1*t2 + 57*Power(s2,8)*t1*t2 + 
         2540*Power(t1,2)*t2 + 15518*s2*Power(t1,2)*t2 + 
         6530*Power(s2,2)*Power(t1,2)*t2 - 
         18740*Power(s2,3)*Power(t1,2)*t2 - 
         14499*Power(s2,4)*Power(t1,2)*t2 + 
         176*Power(s2,5)*Power(t1,2)*t2 + 
         2050*Power(s2,6)*Power(t1,2)*t2 + 
         186*Power(s2,7)*Power(t1,2)*t2 - 81*Power(s2,8)*Power(t1,2)*t2 - 
         1493*Power(t1,3)*t2 - 19582*s2*Power(t1,3)*t2 - 
         1114*Power(s2,2)*Power(t1,3)*t2 + 
         30418*Power(s2,3)*Power(t1,3)*t2 + 
         17108*Power(s2,4)*Power(t1,3)*t2 - 
         1158*Power(s2,5)*Power(t1,3)*t2 - 
         2116*Power(s2,6)*Power(t1,3)*t2 - 
         86*Power(s2,7)*Power(t1,3)*t2 + 51*Power(s2,8)*Power(t1,3)*t2 - 
         125*Power(t1,4)*t2 + 16618*s2*Power(t1,4)*t2 - 
         6808*Power(s2,2)*Power(t1,4)*t2 - 
         29004*Power(s2,3)*Power(t1,4)*t2 - 
         11809*Power(s2,4)*Power(t1,4)*t2 + 
         1628*Power(s2,5)*Power(t1,4)*t2 + 
         1178*Power(s2,6)*Power(t1,4)*t2 - 
         18*Power(s2,7)*Power(t1,4)*t2 - 12*Power(s2,8)*Power(t1,4)*t2 + 
         181*Power(t1,5)*t2 - 9774*s2*Power(t1,5)*t2 + 
         8209*Power(s2,2)*Power(t1,5)*t2 + 
         16478*Power(s2,3)*Power(t1,5)*t2 + 
         4575*Power(s2,4)*Power(t1,5)*t2 - 
         1094*Power(s2,5)*Power(t1,5)*t2 - 
         277*Power(s2,6)*Power(t1,5)*t2 + 18*Power(s2,7)*Power(t1,5)*t2 + 
         439*Power(t1,6)*t2 + 3320*s2*Power(t1,6)*t2 - 
         4348*Power(s2,2)*Power(t1,6)*t2 - 
         5310*Power(s2,3)*Power(t1,6)*t2 - 
         703*Power(s2,4)*Power(t1,6)*t2 + 
         298*Power(s2,5)*Power(t1,6)*t2 - 418*Power(t1,7)*t2 - 
         220*s2*Power(t1,7)*t2 + 1214*Power(s2,2)*Power(t1,7)*t2 + 
         780*Power(s2,3)*Power(t1,7)*t2 - 56*Power(s2,4)*Power(t1,7)*t2 + 
         120*Power(t1,8)*t2 - 140*s2*Power(t1,8)*t2 - 
         168*Power(s2,2)*Power(t1,8)*t2 - 8*Power(t1,9)*t2 - 
         322*Power(t2,2) + 701*s2*Power(t2,2) + 
         2544*Power(s2,2)*Power(t2,2) + 589*Power(s2,3)*Power(t2,2) - 
         2727*Power(s2,4)*Power(t2,2) - 2146*Power(s2,5)*Power(t2,2) + 
         58*Power(s2,6)*Power(t2,2) + 409*Power(s2,7)*Power(t2,2) + 
         7*Power(s2,8)*Power(t2,2) - 15*Power(s2,9)*Power(t2,2) + 
         2708*t1*Power(t2,2) - 936*s2*t1*Power(t2,2) - 
         9326*Power(s2,2)*t1*Power(t2,2) + 
         725*Power(s2,3)*t1*Power(t2,2) + 
         15019*Power(s2,4)*t1*Power(t2,2) + 
         9014*Power(s2,5)*t1*Power(t2,2) - 
         982*Power(s2,6)*t1*Power(t2,2) - 
         1337*Power(s2,7)*t1*Power(t2,2) - 
         29*Power(s2,8)*t1*Power(t2,2) + 42*Power(s2,9)*t1*Power(t2,2) - 
         7995*Power(t1,2)*Power(t2,2) - 1533*s2*Power(t1,2)*Power(t2,2) + 
         14318*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         10538*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         34550*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         14570*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         2646*Power(s2,6)*Power(t1,2)*Power(t2,2) + 
         1734*Power(s2,7)*Power(t1,2)*Power(t2,2) + 
         19*Power(s2,8)*Power(t1,2)*Power(t2,2) - 
         39*Power(s2,9)*Power(t1,2)*Power(t2,2) + 
         11043*Power(t1,3)*Power(t2,2) + 
         1115*s2*Power(t1,3)*Power(t2,2) - 
         14124*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         24009*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         41742*Power(s2,4)*Power(t1,3)*Power(t2,2) + 
         11325*Power(s2,5)*Power(t1,3)*Power(t2,2) - 
         2956*Power(s2,6)*Power(t1,3)*Power(t2,2) - 
         1093*Power(s2,7)*Power(t1,3)*Power(t2,2) + 
         21*Power(s2,8)*Power(t1,3)*Power(t2,2) + 
         12*Power(s2,9)*Power(t1,3)*Power(t2,2) - 
         7035*Power(t1,4)*Power(t2,2) + 5042*s2*Power(t1,4)*Power(t2,2) + 
         11230*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         25057*Power(s2,3)*Power(t1,4)*Power(t2,2) - 
         27447*Power(s2,4)*Power(t1,4)*Power(t2,2) - 
         4194*Power(s2,5)*Power(t1,4)*Power(t2,2) + 
         1612*Power(s2,6)*Power(t1,4)*Power(t2,2) + 
         287*Power(s2,7)*Power(t1,4)*Power(t2,2) - 
         18*Power(s2,8)*Power(t1,4)*Power(t2,2) + 
         899*Power(t1,5)*Power(t2,2) - 7271*s2*Power(t1,5)*Power(t2,2) - 
         5630*Power(s2,2)*Power(t1,5)*Power(t2,2) + 
         13368*Power(s2,3)*Power(t1,5)*Power(t2,2) + 
         9135*Power(s2,4)*Power(t1,5)*Power(t2,2) + 
         499*Power(s2,5)*Power(t1,5)*Power(t2,2) - 
         378*Power(s2,6)*Power(t1,5)*Power(t2,2) + 
         1158*Power(t1,6)*Power(t2,2) + 3394*s2*Power(t1,6)*Power(t2,2) + 
         624*Power(s2,2)*Power(t1,6)*Power(t2,2) - 
         3384*Power(s2,3)*Power(t1,6)*Power(t2,2) - 
         1172*Power(s2,4)*Power(t1,6)*Power(t2,2) + 
         72*Power(s2,5)*Power(t1,6)*Power(t2,2) - 
         498*Power(t1,7)*Power(t2,2) - 576*s2*Power(t1,7)*Power(t2,2) + 
         364*Power(s2,2)*Power(t1,7)*Power(t2,2) + 
         288*Power(s2,3)*Power(t1,7)*Power(t2,2) + 
         42*Power(t1,8)*Power(t2,2) + 64*s2*Power(t1,8)*Power(t2,2) - 
         472*Power(t2,3) - 1866*s2*Power(t2,3) - 
         381*Power(s2,2)*Power(t2,3) + 1961*Power(s2,3)*Power(t2,3) - 
         1166*Power(s2,4)*Power(t2,3) - 3941*Power(s2,5)*Power(t2,3) - 
         1696*Power(s2,6)*Power(t2,3) + 401*Power(s2,7)*Power(t2,3) + 
         280*Power(s2,8)*Power(t2,3) - 21*Power(s2,9)*Power(t2,3) - 
         5*Power(s2,10)*Power(t2,3) + 2285*t1*Power(t2,3) + 
         10174*s2*t1*Power(t2,3) + 4076*Power(s2,2)*t1*Power(t2,3) - 
         4940*Power(s2,3)*t1*Power(t2,3) + 
         9354*Power(s2,4)*t1*Power(t2,3) + 
         16575*Power(s2,5)*t1*Power(t2,3) + 
         4909*Power(s2,6)*t1*Power(t2,3) - 
         1664*Power(s2,7)*t1*Power(t2,3) - 
         601*Power(s2,8)*t1*Power(t2,3) + 35*Power(s2,9)*t1*Power(t2,3) + 
         9*Power(s2,10)*t1*Power(t2,3) - 3781*Power(t1,2)*Power(t2,3) - 
         18190*s2*Power(t1,2)*Power(t2,3) - 
         3486*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         5259*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         24893*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         27442*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         4610*Power(s2,6)*Power(t1,2)*Power(t2,3) + 
         2247*Power(s2,7)*Power(t1,2)*Power(t2,3) + 
         428*Power(s2,8)*Power(t1,2)*Power(t2,3) - 
         20*Power(s2,9)*Power(t1,2)*Power(t2,3) - 
         4*Power(s2,10)*Power(t1,2)*Power(t2,3) + 
         1820*Power(t1,3)*Power(t2,3) + 
         11057*s2*Power(t1,3)*Power(t2,3) - 
         11044*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         4596*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         30291*Power(s2,4)*Power(t1,3)*Power(t2,3) + 
         21751*Power(s2,5)*Power(t1,3)*Power(t2,3) + 
         1290*Power(s2,6)*Power(t1,3)*Power(t2,3) - 
         1190*Power(s2,7)*Power(t1,3)*Power(t2,3) - 
         107*Power(s2,8)*Power(t1,3)*Power(t2,3) + 
         6*Power(s2,9)*Power(t1,3)*Power(t2,3) + 
         1398*Power(t1,4)*Power(t2,3) + 2461*s2*Power(t1,4)*Power(t2,3) + 
         19201*Power(s2,2)*Power(t1,4)*Power(t2,3) + 
         3078*Power(s2,3)*Power(t1,4)*Power(t2,3) - 
         17702*Power(s2,4)*Power(t1,4)*Power(t2,3) - 
         7839*Power(s2,5)*Power(t1,4)*Power(t2,3) + 
         147*Power(s2,6)*Power(t1,4)*Power(t2,3) + 
         206*Power(s2,7)*Power(t1,4)*Power(t2,3) - 
         1692*Power(t1,5)*Power(t2,3) - 5246*s2*Power(t1,5)*Power(t2,3) - 
         9580*Power(s2,2)*Power(t1,5)*Power(t2,3) - 
         454*Power(s2,3)*Power(t1,5)*Power(t2,3) + 
         4372*Power(s2,4)*Power(t1,5)*Power(t2,3) + 
         896*Power(s2,5)*Power(t1,5)*Power(t2,3) - 
         40*Power(s2,6)*Power(t1,5)*Power(t2,3) + 
         408*Power(t1,6)*Power(t2,3) + 1822*s2*Power(t1,6)*Power(t2,3) + 
         1382*Power(s2,2)*Power(t1,6)*Power(t2,3) - 
         308*Power(s2,3)*Power(t1,6)*Power(t2,3) - 
         256*Power(s2,4)*Power(t1,6)*Power(t2,3) + 
         42*Power(t1,7)*Power(t2,3) - 212*s2*Power(t1,7)*Power(t2,3) - 
         168*Power(s2,2)*Power(t1,7)*Power(t2,3) - 
         8*Power(t1,8)*Power(t2,3) + 8*Power(t2,4) - 
         1667*s2*Power(t2,4) - 3734*Power(s2,2)*Power(t2,4) - 
         1196*Power(s2,3)*Power(t2,4) + 758*Power(s2,4)*Power(t2,4) - 
         2122*Power(s2,5)*Power(t2,4) - 2866*Power(s2,6)*Power(t2,4) - 
         505*Power(s2,7)*Power(t2,4) + 342*Power(s2,8)*Power(t2,4) + 
         59*Power(s2,9)*Power(t2,4) - 11*Power(s2,10)*Power(t2,4) - 
         517*t1*Power(t2,4) + 4818*s2*t1*Power(t2,4) + 
         11245*Power(s2,2)*t1*Power(t2,4) + 
         2056*Power(s2,3)*t1*Power(t2,4) - 
         834*Power(s2,4)*t1*Power(t2,4) + 
         10258*Power(s2,5)*t1*Power(t2,4) + 
         8424*Power(s2,6)*t1*Power(t2,4) + 
         510*Power(s2,7)*t1*Power(t2,4) - 
         693*Power(s2,8)*t1*Power(t2,4) - 62*Power(s2,9)*t1*Power(t2,4) + 
         9*Power(s2,10)*t1*Power(t2,4) + 2155*Power(t1,2)*Power(t2,4) - 
         2393*s2*Power(t1,2)*Power(t2,4) - 
         5175*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         8725*Power(s2,3)*Power(t1,2)*Power(t2,4) - 
         493*Power(s2,4)*Power(t1,2)*Power(t2,4) - 
         17448*Power(s2,5)*Power(t1,2)*Power(t2,4) - 
         8439*Power(s2,6)*Power(t1,2)*Power(t2,4) + 
         217*Power(s2,7)*Power(t1,2)*Power(t2,4) + 
         405*Power(s2,8)*Power(t1,2)*Power(t2,4) + 
         6*Power(s2,9)*Power(t1,2)*Power(t2,4) - 
         3238*Power(t1,3)*Power(t2,4) - 4107*s2*Power(t1,3)*Power(t2,4) - 
         10480*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         20357*Power(s2,3)*Power(t1,3)*Power(t2,4) + 
         954*Power(s2,4)*Power(t1,3)*Power(t2,4) + 
         12139*Power(s2,5)*Power(t1,3)*Power(t2,4) + 
         3258*Power(s2,6)*Power(t1,3)*Power(t2,4) - 
         259*Power(s2,7)*Power(t1,3)*Power(t2,4) - 
         40*Power(s2,8)*Power(t1,3)*Power(t2,4) + 
         1860*Power(t1,4)*Power(t2,4) + 4428*s2*Power(t1,4)*Power(t2,4) + 
         10589*Power(s2,2)*Power(t1,4)*Power(t2,4) + 
         12655*Power(s2,3)*Power(t1,4)*Power(t2,4) - 
         424*Power(s2,4)*Power(t1,4)*Power(t2,4) - 
         2940*Power(s2,5)*Power(t1,4)*Power(t2,4) - 
         366*Power(s2,6)*Power(t1,4)*Power(t2,4) + 
         8*Power(s2,7)*Power(t1,4)*Power(t2,4) - 
         114*Power(t1,5)*Power(t2,4) - 986*s2*Power(t1,5)*Power(t2,4) - 
         2876*Power(s2,2)*Power(t1,5)*Power(t2,4) - 
         2050*Power(s2,3)*Power(t1,5)*Power(t2,4) + 
         20*Power(s2,4)*Power(t1,5)*Power(t2,4) + 
         120*Power(s2,5)*Power(t1,5)*Power(t2,4) - 
         171*Power(t1,6)*Power(t2,4) - 123*s2*Power(t1,6)*Power(t2,4) + 
         410*Power(s2,2)*Power(t1,6)*Power(t2,4) + 
         200*Power(s2,3)*Power(t1,6)*Power(t2,4) + 
         18*Power(t1,7)*Power(t2,4) + 32*s2*Power(t1,7)*Power(t2,4) + 
         116*Power(t2,5) + 331*s2*Power(t2,5) - 
         1566*Power(s2,2)*Power(t2,5) - 2671*Power(s2,3)*Power(t2,5) - 
         100*Power(s2,4)*Power(t2,5) + 171*Power(s2,5)*Power(t2,5) - 
         1507*Power(s2,6)*Power(t2,5) - 961*Power(s2,7)*Power(t2,5) + 
         43*Power(s2,8)*Power(t2,5) + 82*Power(s2,9)*Power(t2,5) - 
         5*Power(s2,10)*Power(t2,5) - 703*t1*Power(t2,5) - 
         2820*s2*t1*Power(t2,5) + 404*Power(s2,2)*t1*Power(t2,5) + 
         1039*Power(s2,3)*t1*Power(t2,5) - 
         3308*Power(s2,4)*t1*Power(t2,5) + 
         814*Power(s2,5)*t1*Power(t2,5) + 
         4573*Power(s2,6)*t1*Power(t2,5) + 
         1541*Power(s2,7)*t1*Power(t2,5) - 
         216*Power(s2,8)*t1*Power(t2,5) - 36*Power(s2,9)*t1*Power(t2,5) + 
         1287*Power(t1,2)*Power(t2,5) + 5726*s2*Power(t1,2)*Power(t2,5) + 
         5645*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
         10445*Power(s2,3)*Power(t1,2)*Power(t2,5) + 
         10120*Power(s2,4)*Power(t1,2)*Power(t2,5) - 
         1720*Power(s2,5)*Power(t1,2)*Power(t2,5) - 
         4303*Power(s2,6)*Power(t1,2)*Power(t2,5) - 
         513*Power(s2,7)*Power(t1,2)*Power(t2,5) + 
         60*Power(s2,8)*Power(t1,2)*Power(t2,5) - 
         697*Power(t1,3)*Power(t2,5) - 3770*s2*Power(t1,3)*Power(t2,5) - 
         5042*Power(s2,2)*Power(t1,3)*Power(t2,5) - 
         11276*Power(s2,3)*Power(t1,3)*Power(t2,5) - 
         8373*Power(s2,4)*Power(t1,3)*Power(t2,5) + 
         770*Power(s2,5)*Power(t1,3)*Power(t2,5) + 
         1046*Power(s2,6)*Power(t1,3)*Power(t2,5) + 
         68*Power(s2,7)*Power(t1,3)*Power(t2,5) - 
         151*Power(t1,4)*Power(t2,5) - 2*s2*Power(t1,4)*Power(t2,5) + 
         518*Power(s2,2)*Power(t1,4)*Power(t2,5) + 
         2794*Power(s2,3)*Power(t1,4)*Power(t2,5) + 
         1814*Power(s2,4)*Power(t1,4)*Power(t2,5) + 
         80*Power(s2,5)*Power(t1,4)*Power(t2,5) - 
         24*Power(s2,6)*Power(t1,4)*Power(t2,5) + 
         151*Power(t1,5)*Power(t2,5) + 572*s2*Power(t1,5)*Power(t2,5) + 
         169*Power(s2,2)*Power(t1,5)*Power(t2,5) - 
         378*Power(s2,3)*Power(t1,5)*Power(t2,5) - 
         112*Power(s2,4)*Power(t1,5)*Power(t2,5) - 
         7*Power(t1,6)*Power(t2,5) - 70*s2*Power(t1,6)*Power(t2,5) - 
         48*Power(s2,2)*Power(t1,6)*Power(t2,5) + 52*Power(t2,6) + 
         483*s2*Power(t2,6) + 1010*Power(s2,2)*Power(t2,6) + 
         133*Power(s2,3)*Power(t2,6) - 197*Power(s2,4)*Power(t2,6) + 
         491*Power(s2,5)*Power(t2,6) - 72*Power(s2,6)*Power(t2,6) - 
         458*Power(s2,7)*Power(t2,6) - 94*Power(s2,8)*Power(t2,6) + 
         24*Power(s2,9)*Power(t2,6) - 105*t1*Power(t2,6) - 
         1504*s2*t1*Power(t2,6) - 3709*Power(s2,2)*t1*Power(t2,6) - 
         3488*Power(s2,3)*t1*Power(t2,6) - 
         3655*Power(s2,4)*t1*Power(t2,6) - 
         2314*Power(s2,5)*t1*Power(t2,6) + 
         657*Power(s2,6)*t1*Power(t2,6) + 
         649*Power(s2,7)*t1*Power(t2,6) + 15*Power(s2,8)*t1*Power(t2,6) - 
         169*Power(t1,2)*Power(t2,6) + 1121*s2*Power(t1,2)*Power(t2,6) + 
         2618*Power(s2,2)*Power(t1,2)*Power(t2,6) + 
         3602*Power(s2,3)*Power(t1,2)*Power(t2,6) + 
         6053*Power(s2,4)*Power(t1,2)*Power(t2,6) + 
         2661*Power(s2,5)*Power(t1,2)*Power(t2,6) - 
         406*Power(s2,6)*Power(t1,2)*Power(t2,6) - 
         172*Power(s2,7)*Power(t1,2)*Power(t2,6) + 
         347*Power(t1,3)*Power(t2,6) + 399*s2*Power(t1,3)*Power(t2,6) + 
         730*Power(s2,2)*Power(t1,3)*Power(t2,6) + 
         233*Power(s2,3)*Power(t1,3)*Power(t2,6) - 
         1887*Power(s2,4)*Power(t1,3)*Power(t2,6) - 
         846*Power(s2,5)*Power(t1,3)*Power(t2,6) - 
         24*Power(s2,6)*Power(t1,3)*Power(t2,6) - 
         117*Power(t1,4)*Power(t2,6) - 430*s2*Power(t1,4)*Power(t2,6) - 
         774*Power(s2,2)*Power(t1,4)*Power(t2,6) - 
         179*Power(s2,3)*Power(t1,4)*Power(t2,6) + 
         164*Power(s2,4)*Power(t1,4)*Power(t2,6) + 
         24*Power(s2,5)*Power(t1,4)*Power(t2,6) - 
         3*Power(t1,5)*Power(t2,6) + 27*s2*Power(t1,5)*Power(t2,6) + 
         102*Power(s2,2)*Power(t1,5)*Power(t2,6) + 
         32*Power(s2,3)*Power(t1,5)*Power(t2,6) - 24*Power(t2,7) + 
         23*s2*Power(t2,7) + 518*Power(s2,2)*Power(t2,7) + 
         932*Power(s2,3)*Power(t2,7) + 629*Power(s2,4)*Power(t2,7) + 
         368*Power(s2,5)*Power(t2,7) + 164*Power(s2,6)*Power(t2,7) - 
         43*Power(s2,7)*Power(t2,7) - 35*Power(s2,8)*Power(t2,7) + 
         159*t1*Power(t2,7) + 134*s2*t1*Power(t2,7) - 
         428*Power(s2,2)*t1*Power(t2,7) - 
         900*Power(s2,3)*t1*Power(t2,7) - 
         1493*Power(s2,4)*t1*Power(t2,7) - 
         1361*Power(s2,5)*t1*Power(t2,7) - 
         347*Power(s2,6)*t1*Power(t2,7) + 88*Power(s2,7)*t1*Power(t2,7) - 
         175*Power(t1,2)*Power(t2,7) - 542*s2*Power(t1,2)*Power(t2,7) - 
         562*Power(s2,2)*Power(t1,2)*Power(t2,7) - 
         1019*Power(s2,3)*Power(t1,2)*Power(t2,7) - 
         90*Power(s2,4)*Power(t1,2)*Power(t2,7) + 
         774*Power(s2,5)*Power(t1,2)*Power(t2,7) + 
         156*Power(s2,6)*Power(t1,2)*Power(t2,7) + 
         34*Power(t1,3)*Power(t2,7) + 255*s2*Power(t1,3)*Power(t2,7) + 
         464*Power(s2,2)*Power(t1,3)*Power(t2,7) + 
         576*Power(s2,3)*Power(t1,3)*Power(t2,7) + 
         133*Power(s2,4)*Power(t1,3)*Power(t2,7) - 
         26*Power(s2,5)*Power(t1,3)*Power(t2,7) + 
         6*Power(t1,4)*Power(t2,7) + 15*s2*Power(t1,4)*Power(t2,7) - 
         37*Power(s2,2)*Power(t1,4)*Power(t2,7) - 
         66*Power(s2,3)*Power(t1,4)*Power(t2,7) - 
         8*Power(s2,4)*Power(t1,4)*Power(t2,7) - 24*Power(t2,8) - 
         47*s2*Power(t2,8) - 93*Power(s2,2)*Power(t2,8) + 
         29*Power(s2,3)*Power(t2,8) + 218*Power(s2,4)*Power(t2,8) + 
         187*Power(s2,5)*Power(t2,8) + 83*Power(s2,6)*Power(t2,8) + 
         8*Power(s2,7)*Power(t2,8) + 3*t1*Power(t2,8) + 
         216*s2*t1*Power(t2,8) + 293*Power(s2,2)*t1*Power(t2,8) + 
         422*Power(s2,3)*t1*Power(t2,8) + 
         417*Power(s2,4)*t1*Power(t2,8) - 
         104*Power(s2,5)*t1*Power(t2,8) - 
         129*Power(s2,6)*t1*Power(t2,8) + 24*Power(t1,2)*Power(t2,8) - 
         84*s2*Power(t1,2)*Power(t2,8) - 
         177*Power(s2,2)*Power(t1,2)*Power(t2,8) - 
         251*Power(s2,3)*Power(t1,2)*Power(t2,8) - 
         263*Power(s2,4)*Power(t1,2)*Power(t2,8) - 
         42*Power(s2,5)*Power(t1,2)*Power(t2,8) - 
         8*Power(t1,3)*Power(t2,8) - 15*s2*Power(t1,3)*Power(t2,8) - 
         28*Power(s2,2)*Power(t1,3)*Power(t2,8) + 
         21*Power(s2,3)*Power(t1,3)*Power(t2,8) + 
         16*Power(s2,4)*Power(t1,3)*Power(t2,8) + 14*Power(t2,9) - 
         27*s2*Power(t2,9) - 40*Power(s2,2)*Power(t2,9) - 
         85*Power(s2,3)*Power(t2,9) - 113*Power(s2,4)*Power(t2,9) - 
         12*Power(s2,5)*Power(t2,9) + 21*Power(s2,6)*Power(t2,9) - 
         11*t1*Power(t2,9) - 16*s2*t1*Power(t2,9) + 
         49*Power(s2,2)*t1*Power(t2,9) + 45*Power(s2,3)*t1*Power(t2,9) + 
         82*Power(s2,4)*t1*Power(t2,9) + 60*Power(s2,5)*t1*Power(t2,9) + 
         Power(t1,2)*Power(t2,9) + 16*s2*Power(t1,2)*Power(t2,9) + 
         17*Power(s2,2)*Power(t1,2)*Power(t2,9) + 
         23*Power(s2,3)*Power(t1,2)*Power(t2,9) - 
         4*Power(s2,4)*Power(t1,2)*Power(t2,9) - 2*Power(t2,10) + 
         12*s2*Power(t2,10) - 6*Power(s2,2)*Power(t2,10) - 
         6*Power(s2,4)*Power(t2,10) - 16*Power(s2,5)*Power(t2,10) + 
         t1*Power(t2,10) - 4*s2*t1*Power(t2,10) - 
         9*Power(s2,2)*t1*Power(t2,10) - 11*Power(s2,3)*t1*Power(t2,10) - 
         7*Power(s2,4)*t1*Power(t2,10) - s2*Power(t2,11) + 
         3*Power(s2,2)*Power(t2,11) + Power(s2,3)*Power(t2,11) + 
         3*Power(s2,4)*Power(t2,11) + 
         Power(s1,11)*s2*(-2*(-1 + t1)*t1 + 
            Power(s2,2)*(-1 - 14*t1 + 15*t2) + 
            s2*(3 - 2*Power(t1,2) + t2 + 2*t1*(-4 + 5*t2))) + 
         Power(s1,10)*(17*Power(s2,5) + t1*(-1 + 2*t1 - 2*Power(t1,2)) - 
            s2*(3 + 4*Power(t1,3) + Power(t1,2)*(8 - 17*t2) + 
               16*t1*(-1 + t2) + 2*t2) + 
            Power(s2,4)*(17 - 45*t1 + 22*t2) + 
            Power(s2,3)*(12 - 27*Power(t1,2) + 23*t2 - 95*Power(t2,2) + 
               10*t1*(-4 + 15*t2)) - 
            Power(s2,2)*(-3 + Power(t1,3) + Power(t1,2)*(57 - 16*t2) + 
               37*t2 + 10*Power(t2,2) + t1*(-37 - 93*t2 + 45*Power(t2,2))\
)) + Power(s1,9)*(1 + 52*Power(s2,6) - Power(t1,4) + 
            Power(t1,2)*(10 - 11*t2) + t2 + t1*(-8 + 6*t2) + 
            2*Power(t1,3)*(-3 + 7*t2) - 
            2*Power(s2,5)*(-12 + 25*t1 + 55*t2) - 
            Power(s2,4)*(75 + 98*Power(t1,2) + 153*t2 + 
               205*Power(t2,2) - t1*(104 + 487*t2)) + 
            s2*(-3 - 2*Power(t1,4) + 29*t2 + 18*Power(t2,2) + 
               Power(t1,3)*(-76 + 26*t2) + 
               Power(t1,2)*(120 + 90*t2 - 64*Power(t2,2)) + 
               2*t1*(-16 - 81*t2 + 28*Power(t2,2))) - 
            Power(s2,3)*(66 + 16*Power(t1,3) + 
               Power(t1,2)*(321 - 239*t2) + 207*t2 + 166*Power(t2,2) - 
               345*Power(t2,3) + t1*(-338 - 511*t2 + 713*Power(t2,2))) + 
            Power(s2,2)*(-36 - 64*t2 + 194*Power(t2,2) + 
               45*Power(t2,3) + Power(t1,3)*(-89 + 4*t2) + 
               Power(t1,2)*(-72 + 524*t2 - 56*Power(t2,2)) + 
               2*t1*(93 - 146*t2 - 237*Power(t2,2) + 60*Power(t2,3)))) + 
         Power(s1,8)*(1 + 73*Power(s2,7) + 
            Power(s2,6)*(-78 + 13*t1 - 389*t2) - 7*t2 - 8*Power(t2,2) + 
            Power(t1,4)*(-35 + 4*t2) + 
            Power(t1,3)*(83 + 46*t2 - 42*Power(t2,2)) + 
            t1*(9 + 69*t2 - 17*Power(t2,2)) + 
            Power(t1,2)*(-63 - 76*t2 + 22*Power(t2,2)) + 
            Power(s2,5)*(-433 - 163*Power(t1,2) - 306*t2 + 
               240*Power(t2,2) + t1*(535 + 606*t2)) + 
            Power(s2,4)*(-281 - 61*Power(t1,3) + 274*t2 + 
               587*Power(t2,2) + 851*Power(t2,3) + 
               Power(t1,2)*(-466 + 854*t2) + 
               t1*(784 - 503*t2 - 2272*Power(t2,2))) + 
            s2*(36 + Power(t1,5) + 59*t2 - 115*Power(t2,2) - 
               73*Power(t2,3) + Power(t1,4)*(-92 + 7*t2) + 
               Power(t1,3)*(-21 + 582*t2 - 70*Power(t2,2)) - 
               2*t1*(126 - 61*t2 - 358*Power(t2,2) + 56*Power(t2,3)) + 
               Power(t1,2)*(368 - 853*t2 - 423*Power(t2,2) + 
                  140*Power(t2,3))) + 
            Power(s2,3)*(75 - 2*Power(t1,4) + 423*t2 + 
               1223*Power(t2,2) + 621*Power(t2,3) - 810*Power(t2,4) + 
               Power(t1,3)*(-505 + 107*t2) + 
               Power(t1,2)*(576 + 2927*t2 - 927*Power(t2,2)) + 
               t1*(-56 - 2974*t2 - 2651*Power(t2,2) + 1984*Power(t2,3))) \
+ Power(s2,2)*(96 + 423*t2 + 397*Power(t2,2) - 577*Power(t2,3) - 
               120*Power(t2,4) - Power(t1,4)*(47 + 2*t2) + 
               Power(t1,3)*(-706 + 657*t2) + 
               Power(t1,2)*(1403 + 945*t2 - 2126*Power(t2,2) + 
                  112*Power(t2,3)) + 
               t1*(-696 - 1919*t2 + 986*Power(t2,2) + 1404*Power(t2,3) - 
                  210*Power(t2,4)))) + 
         Power(s1,7)*(56*Power(s2,8) + 
            Power(s2,7)*(-215 + 86*t1 - 533*t2) - 
            2*Power(t1,5)*(10 + t2) - 
            3*Power(t1,4)*(6 - 70*t2 + Power(t2,2)) + 
            Power(t1,2)*(-269 + 311*t2 + 269*Power(t2,2) - 
               14*Power(t2,3)) + 
            2*(-6 - 9*t2 + 8*Power(t2,2) + 15*Power(t2,3)) + 
            2*t1*(53 + 10*t2 - 136*Power(t2,2) + 17*Power(t2,3)) + 
            Power(t1,3)*(213 - 481*t2 - 154*Power(t2,2) + 
               70*Power(t2,3)) + 
            Power(s2,6)*(-588 - 138*Power(t1,2) + 358*t2 + 
               1234*Power(t2,2) + t1*(715 + 139*t2)) + 
            Power(s2,5)*(277 - 108*Power(t1,3) + 2872*t2 + 
               1476*Power(t2,2) - 12*Power(t2,3) + 
               3*Power(t1,2)*(30 + 463*t2) - 
               t1*(319 + 3805*t2 + 2912*Power(t2,2))) + 
            Power(s2,4)*(1397 - 8*Power(t1,4) + 2360*t2 + 
               348*Power(t2,2) - 1247*Power(t2,3) - 2068*Power(t2,4) + 
               2*Power(t1,3)*(-519 + 208*t2) + 
               Power(t1,2)*(3201 + 4462*t2 - 3183*Power(t2,2)) + 
               t1*(-3326 - 7356*t2 + 423*Power(t2,2) + 6055*Power(t2,3))\
) + Power(s2,3)*(924 + 183*t2 - 973*Power(t2,2) - 3773*Power(t2,3) - 
               1422*Power(t2,4) + 1302*Power(t2,5) + 
               Power(t1,4)*(-268 + 3*t2) + 
               Power(t1,3)*(-1424 + 3703*t2 - 296*Power(t2,2)) + 
               Power(t1,2)*(4629 - 3253*t2 - 11405*Power(t2,2) + 
                  2065*Power(t2,3)) + 
               t1*(-3458 - 1617*t2 + 11162*Power(t2,2) + 
                  7620*Power(t2,3) - 3584*Power(t2,4))) + 
            s2*(-62 - 313*t2 - 290*Power(t2,2) + 233*Power(t2,3) + 
               176*Power(t2,4) - Power(t1,5)*(36 + 7*t2) + 
               Power(t1,4)*(-653 + 548*t2) + 
               Power(t1,3)*(2003 + 529*t2 - 1955*Power(t2,2) + 
                  98*Power(t2,3)) + 
               Power(t1,2)*(-1720 - 3126*t2 + 2646*Power(t2,2) + 
                  1113*Power(t2,3) - 196*Power(t2,4)) + 
               t1*(558 + 2019*t2 + 11*Power(t2,2) - 1816*Power(t2,3) + 
                  140*Power(t2,4))) + 
            Power(s2,2)*(55 + 2*Power(t1,5) - 399*t2 - 
               1900*Power(t2,2) - 1233*Power(t2,3) + 1082*Power(t2,4) + 
               210*Power(t2,5) + 
               Power(t1,4)*(-905 + 245*t2 + 12*Power(t2,2)) + 
               Power(t1,3)*(1460 + 5505*t2 - 2103*Power(t2,2) - 
                  28*Power(t2,3)) + 
               Power(t1,2)*(211 - 10387*t2 - 4555*Power(t2,2) + 
                  4998*Power(t2,3) - 140*Power(t2,4)) + 
               t1*(-694 + 4681*t2 + 8100*Power(t2,2) - 
                  1825*Power(t2,3) - 2688*Power(t2,4) + 252*Power(t2,5)))\
) + Power(s1,6)*(15 + 23*Power(s2,9) - 5*Power(t1,6) + 
            Power(s2,8)*(-201 + 89*t1 - 380*t2) + 75*t2 + 
            59*Power(t2,2) + 9*Power(t2,3) - 70*Power(t2,4) + 
            Power(t1,5)*(-223 + 96*t2 + 10*Power(t2,2)) + 
            Power(s2,7)*(-212 - 49*Power(t1,2) + t1*(338 - 422*t2) + 
               1259*t2 + 1669*Power(t2,2)) + 
            Power(t1,4)*(924 + 169*t2 - 547*Power(t2,2) - 
               10*Power(t2,3)) + 
            Power(t1,3)*(-1189 - 1362*t2 + 1217*Power(t2,2) + 
               294*Power(t2,3) - 70*Power(t2,4)) + 
            Power(t1,2)*(638 + 1633*t2 - 613*Power(t2,2) - 
               577*Power(t2,3) - 14*Power(t2,4)) - 
            t1*(155 + 611*t2 + 266*Power(t2,2) - 620*Power(t2,3) + 
               56*Power(t2,4)) - 
            Power(s2,6)*(-1353 + 107*Power(t1,3) - 4014*t2 + 
               397*Power(t2,2) + 2126*Power(t2,3) - 
               Power(t1,2)*(799 + 1176*t2) + 
               t1*(2067 + 5138*t2 + 1262*Power(t2,2))) - 
            Power(s2,5)*(-2339 + 12*Power(t1,4) + 
               Power(t1,3)*(954 - 719*t2) + 660*t2 + 7828*Power(t2,2) + 
               3786*Power(t2,3) + 966*Power(t2,4) + 
               Power(t1,2)*(-4079 - 780*t2 + 4911*Power(t2,2)) + 
               t1*(5316 + 684*t2 - 11317*Power(t2,2) - 7606*Power(t2,3))\
) + Power(s2,4)*(36 - 7921*t2 - 7952*Power(t2,2) - 3755*Power(t2,3) + 
               1575*Power(t2,4) + 3248*Power(t2,5) + 
               Power(t1,4)*(-589 + 27*t2) + 
               Power(t1,3)*(762 + 7327*t2 - 1154*Power(t2,2)) + 
               2*Power(t1,2)*
                (274 - 10583*t2 - 8627*Power(t2,2) + 3324*Power(t2,3)) \
+ t1*(-362 + 19621*t2 + 27948*Power(t2,2) + 2283*Power(t2,3) - 
                  10227*Power(t2,4))) + 
            s2*(-120 + 6*Power(t1,6) + 141*t2 + 1094*Power(t2,2) + 
               642*Power(t2,3) - 217*Power(t2,4) - 280*Power(t2,5) + 
               Power(t1,5)*(-658 + 145*t2 + 20*Power(t2,2)) + 
               Power(t1,4)*(1379 + 4203*t2 - 1394*Power(t2,2) - 
                  35*Power(t2,3)) + 
               Power(t1,3)*(530 - 12088*t2 - 2637*Power(t2,2) + 
                  3764*Power(t2,3) - 70*Power(t2,4)) - 
               2*t1*(-570 + 1317*t2 + 3306*Power(t2,2) + 
                  426*Power(t2,3) - 1456*Power(t2,4) + 56*Power(t2,5)) \
+ Power(t1,2)*(-2192 + 9578*t2 + 10859*Power(t2,2) - 4667*Power(t2,3) - 
                  1827*Power(t2,4) + 182*Power(t2,5))) + 
            Power(s2,3)*(-2193 - 9*Power(t1,5) - 5853*t2 - 
               2680*Power(t2,2) + 566*Power(t2,3) + 7035*Power(t2,4) + 
               2142*Power(t2,5) - 1470*Power(t2,6) + 
               Power(t1,4)*(-2590 + 1455*t2 + 23*Power(t2,2)) + 
               Power(t1,3)*(9588 + 11290*t2 - 11391*Power(t2,2) + 
                  425*Power(t2,3)) + 
               Power(t1,2)*(-11778 - 34461*t2 + 6858*Power(t2,2) + 
                  24962*Power(t2,3) - 2905*Power(t2,4)) + 
               t1*(7381 + 24544*t2 + 11469*Power(t2,2) - 
                  23401*Power(t2,3) - 13636*Power(t2,4) + 
                  4396*Power(t2,5))) - 
            Power(s2,2)*(1326 + 855*t2 - 276*Power(t2,2) - 
               4493*Power(t2,3) - 2261*Power(t2,4) + 1330*Power(t2,5) + 
               252*Power(t2,6) + Power(t1,5)*(383 + 27*t2) + 
               Power(t1,4)*(2102 - 5446*t2 + 491*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(t1,3)*(-10218 + 7416*t2 + 18019*Power(t2,2) - 
                  3805*Power(t2,3) - 70*Power(t2,4)) + 
               Power(t1,2)*(12522 + 5148*t2 - 32357*Power(t2,2) - 
                  11534*Power(t2,3) + 7504*Power(t2,4) - 112*Power(t2,5)\
) + t1*(-6153 - 7176*t2 + 13048*Power(t2,2) + 18668*Power(t2,3) - 
                  1925*Power(t2,4) - 3486*Power(t2,5) + 210*Power(t2,6)))\
) + Power(s1,5)*(4*Power(s2,10) - 2*Power(t1,7) + 
            2*Power(s2,9)*(-40 + 21*t1 - 70*t2) + 
            Power(t1,6)*(-130 + 17*t2) + 
            Power(t1,5)*(317 + 1100*t2 - 198*Power(t2,2) - 
               20*Power(t2,3)) + 
            Power(t1,4)*(523 - 4351*t2 - 570*Power(t2,2) + 
               810*Power(t2,3) + 25*Power(t2,4)) + 
            4*(12 - 3*t2 - 53*Power(t2,2) - 9*Power(t2,3) - 
               31*Power(t2,4) + 28*Power(t2,5)) + 
            Power(t1,2)*(1503 - 2394*t2 - 4189*Power(t2,2) + 
               581*Power(t2,3) + 809*Power(t2,4) + 28*Power(t2,5)) + 
            2*t1*(-247 + 220*t2 + 736*Power(t2,2) + 356*Power(t2,3) - 
               432*Power(t2,4) + 35*Power(t2,5)) + 
            Power(t1,3)*(-1761 + 5170*t2 + 3697*Power(t2,2) - 
               1767*Power(t2,3) - 350*Power(t2,4) + 42*Power(t2,5)) + 
            Power(s2,8)*(99 + 10*Power(t1,2) + 1133*t2 + 
               1095*Power(t2,2) - t1*(42 + 475*t2)) + 
            Power(s2,7)*(1108 - 64*Power(t1,3) + 1567*t2 - 
               3006*Power(t2,2) - 2913*Power(t2,3) + 
               Power(t1,2)*(751 + 473*t2) + 
               t1*(-1824 - 2519*t2 + 719*Power(t2,2))) + 
            Power(s2,6)*(334 - 8*Power(t1,4) - 7006*t2 - 
               11409*Power(t2,2) - 738*Power(t2,3) + 2074*Power(t2,4) + 
               Power(t1,3)*(-301 + 684*t2) - 
               2*Power(t1,2)*(-663 + 1981*t2 + 1981*Power(t2,2)) + 
               t1*(-1268 + 10388*t2 + 15275*Power(t2,2) + 
                  3899*Power(t2,3))) + 
            Power(s2,5)*(-3933 - 13623*t2 - 1430*Power(t2,2) + 
               11028*Power(t2,3) + 5832*Power(t2,4) + 
               2184*Power(t2,5) + Power(t1,4)*(-672 + 46*t2) + 
               Power(t1,3)*(3618 + 6626*t2 - 1883*Power(t2,2)) + 
               Power(t1,2)*(-9012 - 26724*t2 - 6305*Power(t2,2) + 
                  9467*Power(t2,3)) + 
               t1*(9994 + 32808*t2 + 10024*Power(t2,2) - 
                  18131*Power(t2,3) - 12120*Power(t2,4))) - 
            Power(s2,4)*(5783 + 20*Power(t1,5) + 1705*t2 - 
               17630*Power(t2,2) - 13977*Power(t2,3) - 
               9328*Power(t2,4) + 1141*Power(t2,5) + 3430*Power(t2,6) + 
               Power(t1,4)*(2607 - 3028*t2 - 6*Power(t2,2)) + 
               Power(t1,3)*(-13499 + 913*t2 + 20944*Power(t2,2) - 
                  1649*Power(t2,3)) + 
               Power(t1,2)*(23331 + 12638*t2 - 56833*Power(t2,2) - 
                  35913*Power(t2,3) + 8521*Power(t2,4)) + 
               t1*(-18155 - 9880*t2 + 45546*Power(t2,2) + 
                  57257*Power(t2,3) + 7517*Power(t2,4) - 
                  11417*Power(t2,5))) + 
            s2*(884 + 615*t2 + 142*Power(t2,2) - 1988*Power(t2,3) - 
               688*Power(t2,4) - 49*Power(t2,5) + 308*Power(t2,6) - 
               3*Power(t1,6)*(69 + 8*t2) - 
               2*Power(t1,5)*
                (826 - 1571*t2 + 96*Power(t2,2) + 15*Power(t2,3)) + 
               Power(t1,4)*(9913 - 5643*t2 - 11216*Power(t2,2) + 
                  1970*Power(t2,3) + 70*Power(t2,4)) + 
               Power(t1,3)*(-16776 - 5815*t2 + 30236*Power(t2,2) + 
                  6000*Power(t2,3) - 4545*Power(t2,4) + 14*Power(t2,5)) \
+ Power(t1,2)*(12687 + 13825*t2 - 21829*Power(t2,2) - 
                  20201*Power(t2,3) + 5084*Power(t2,4) + 
                  1953*Power(t2,5) - 112*Power(t2,6)) + 
               t1*(-4818 - 6621*t2 + 4899*Power(t2,2) + 
                  11379*Power(t2,3) + 2081*Power(t2,4) - 
                  3052*Power(t2,5) + 56*Power(t2,6))) - 
            Power(s2,3)*(1583 - 10721*t2 - 14254*Power(t2,2) - 
               8001*Power(t2,3) - 1665*Power(t2,4) + 8407*Power(t2,5) + 
               2184*Power(t2,6) - 1170*Power(t2,7) + 
               Power(t1,5)*(1189 + 8*t2) + 
               Power(t1,4)*(-2137 - 14699*t2 + 3061*Power(t2,2) + 
                  90*Power(t2,3)) + 
               Power(t1,3)*(-4552 + 53605*t2 + 35514*Power(t2,2) - 
                  19064*Power(t2,3) + 310*Power(t2,4)) + 
               Power(t1,2)*(9941 - 61172*t2 - 103025*Power(t2,2) + 
                  5404*Power(t2,3) + 33733*Power(t2,4) - 
                  2667*Power(t2,5)) + 
               t1*(-6057 + 35332*t2 + 69143*Power(t2,2) + 
                  31552*Power(t2,3) - 29960*Power(t2,4) - 
                  15946*Power(t2,5) + 3710*Power(t2,6))) + 
            Power(s2,2)*(1747 + 16*Power(t1,6) + 6476*t2 + 
               2894*Power(t2,2) + 1338*Power(t2,3) - 6210*Power(t2,4) - 
               2597*Power(t2,5) + 1064*Power(t2,6) + 210*Power(t2,7) + 
               Power(t1,5)*(-3106 + 1585*t2 + 110*Power(t2,2)) + 
               Power(t1,4)*(13715 + 13703*t2 - 13439*Power(t2,2) + 
                  425*Power(t2,3) + 40*Power(t2,4)) - 
               Power(t1,3)*(20463 + 59979*t2 - 14512*Power(t2,2) - 
                  32258*Power(t2,3) + 4245*Power(t2,4) + 84*Power(t2,5)) \
+ Power(t1,2)*(15122 + 70475*t2 + 23005*Power(t2,2) - 
                  54908*Power(t2,3) - 17397*Power(t2,4) + 
                  7462*Power(t2,5) - 56*Power(t2,6)) + 
               t1*(-7094 - 32428*t2 - 24852*Power(t2,2) + 
                  19092*Power(t2,3) + 25936*Power(t2,4) - 
                  973*Power(t2,5) - 3108*Power(t2,6) + 120*Power(t2,7)))) \
+ Power(s1,4)*(-225 + Power(s2,10)*(-10 + 8*t1 - 21*t2) - 107*t2 - 
            32*Power(t2,2) + 314*Power(t2,3) - 145*Power(t2,4) + 
            289*Power(t2,5) - 126*Power(t2,6) + 
            2*Power(t1,7)*(-18 + 5*t2) + 
            Power(t1,6)*(-537 + 515*t2 - 12*Power(t2,2)) + 
            Power(t1,5)*(3590 - 1115*t2 - 2273*Power(t2,2) + 
               232*Power(t2,3) + 20*Power(t2,4)) - 
            Power(t1,4)*(7603 + 2495*t2 - 8407*Power(t2,2) - 
               956*Power(t2,3) + 745*Power(t2,4) + 24*Power(t2,5)) + 
            t1*(1389 + 1565*t2 - 465*Power(t2,2) - 1739*Power(t2,3) - 
               900*Power(t2,4) + 718*Power(t2,5) - 56*Power(t2,6)) + 
            Power(t1,3)*(7751 + 7057*t2 - 9001*Power(t2,2) - 
               5394*Power(t2,3) + 1605*Power(t2,4) + 266*Power(t2,5) - 
               14*Power(t2,6)) - 
            Power(t1,2)*(4328 + 5450*t2 - 3451*Power(t2,2) - 
               5631*Power(t2,3) + 185*Power(t2,4) + 745*Power(t2,5) + 
               14*Power(t2,6)) + 
            Power(s2,9)*(61 + 15*Power(t1,2) + 404*t2 + 
               354*Power(t2,2) - t1*(73 + 206*t2)) - 
            Power(s2,8)*(-267 + 23*Power(t1,3) + 388*t2 + 
               2615*Power(t2,2) + 1735*Power(t2,3) - 
               14*Power(t1,2)*(18 + t2) + 
               t1*(482 + 7*t2 - 1024*Power(t2,2))) + 
            Power(s2,7)*(-808 - 2*Power(t1,4) - 5515*t2 - 
               4629*Power(t2,2) + 3691*Power(t2,3) + 3075*Power(t2,4) + 
               Power(t1,3)*(139 + 377*t2) - 
               Power(t1,2)*(816 + 3823*t2 + 1581*Power(t2,2)) + 
               t1*(1458 + 9212*t2 + 7375*Power(t2,2) - 288*Power(t2,3))) \
+ Power(s2,6)*(-2756 - 2852*t2 + 14335*Power(t2,2) + 
               17420*Power(t2,3) + 2575*Power(t2,4) - 992*Power(t2,5) + 
               Power(t1,4)*(-439 + 36*t2) + 
               Power(t1,3)*(3060 + 2413*t2 - 1652*Power(t2,2)) + 
               Power(t1,2)*(-7611 - 9965*t2 + 7262*Power(t2,2) + 
                  7004*Power(t2,3)) + 
               t1*(7757 + 9821*t2 - 19986*Power(t2,2) - 
                  24367*Power(t2,3) - 6340*Power(t2,4))) - 
            Power(s2,5)*(51 + 13*Power(t1,5) - 17123*t2 - 
               31658*Power(t2,2) - 7148*Power(t2,3) + 
               7930*Power(t2,4) + 5634*Power(t2,5) + 2520*Power(t2,6) + 
               Power(t1,4)*(692 - 3179*t2 + 14*Power(t2,2)) + 
               Power(t1,3)*(-3509 + 16196*t2 + 17844*Power(t2,2) - 
                  2476*Power(t2,3)) + 
               Power(t1,2)*(4591 - 39403*t2 - 68806*Power(t2,2) - 
                  16394*Power(t2,3) + 10875*Power(t2,4)) + 
               t1*(-1845 + 43374*t2 + 80482*Power(t2,2) + 
                  28237*Power(t2,3) - 16625*Power(t2,4) - 
                  12310*Power(t2,5))) + 
            Power(s2,4)*(7192 + 26014*t2 + 6722*Power(t2,2) - 
               18621*Power(t2,3) - 13535*Power(t2,4) - 
               12225*Power(t2,5) + 343*Power(t2,6) + 2450*Power(t2,7) + 
               4*Power(t1,5)*(-394 + 3*t2) + 
               Power(t1,4)*(9129 + 14344*t2 - 5816*Power(t2,2) - 
                  142*Power(t2,3)) - 
               Power(t1,3)*(23778 + 71659*t2 + 7081*Power(t2,2) - 
                  31399*Power(t2,3) + 1225*Power(t2,4)) + 
               Power(t1,2)*(33453 + 118251*t2 + 53375*Power(t2,2) - 
                  79782*Power(t2,3) - 44305*Power(t2,4) + 
                  6858*Power(t2,5)) + 
               t1*(-24439 - 86573*t2 - 41280*Power(t2,2) + 
                  51001*Power(t2,3) + 69645*Power(t2,4) + 
                  10641*Power(t2,5) - 8477*Power(t2,6))) + 
            s2*(-650 + 12*Power(t1,7) - 3303*t2 - 774*Power(t2,2) - 
               715*Power(t2,3) + 1965*Power(t2,4) + 208*Power(t2,5) + 
               371*Power(t2,6) - 238*Power(t2,7) + 
               Power(t1,6)*(-1709 + 632*t2 + 56*Power(t2,2)) + 
               Power(t1,5)*(8711 + 8246*t2 - 6021*Power(t2,2) + 
                  38*Power(t2,3) + 25*Power(t2,4)) - 
               Power(t1,4)*(15154 + 44003*t2 - 8594*Power(t2,2) - 
                  15909*Power(t2,3) + 1680*Power(t2,4) + 63*Power(t2,5)\
) + Power(t1,3)*(12323 + 70970*t2 + 17829*Power(t2,2) - 
                  39869*Power(t2,3) - 7490*Power(t2,4) + 
                  3526*Power(t2,5) + 14*Power(t2,6)) - 
               2*t1*(-1337 - 9143*t2 - 6714*Power(t2,2) + 
                  2141*Power(t2,3) + 5370*Power(t2,4) + 
                  1259*Power(t2,5) - 1036*Power(t2,6) + 8*Power(t2,7)) \
+ Power(t1,2)*(-6205 - 50985*t2 - 31782*Power(t2,2) + 
                  25394*Power(t2,3) + 21770*Power(t2,4) - 
                  3435*Power(t2,5) - 1365*Power(t2,6) + 44*Power(t2,7))) \
+ Power(s2,3)*(8889 + 24*Power(t1,6) + 6153*t2 - 20348*Power(t2,2) - 
               16034*Power(t2,3) - 11460*Power(t2,4) - 
               4054*Power(t2,5) + 6517*Power(t2,6) + 1506*Power(t2,7) - 
               645*Power(t2,8) + 
               4*Power(t1,5)*(-968 + 1120*t2 + 51*Power(t2,2)) + 
               Power(t1,4)*(25171 - 4998*t2 - 32537*Power(t2,2) + 
                  2988*Power(t2,3) + 140*Power(t2,4)) + 
               Power(t1,3)*(-56355 - 33831*t2 + 116165*Power(t2,2) + 
                  58212*Power(t2,3) - 18710*Power(t2,4) + 
                  61*Power(t2,5)) + 
               Power(t1,2)*(61222 + 58945*t2 - 119479*Power(t2,2) - 
                  161089*Power(t2,3) - 2315*Power(t2,4) + 
                  28966*Power(t2,5) - 1589*Power(t2,6)) + 
               t1*(-35046 - 30928*t2 + 61750*Power(t2,2) + 
                  99392*Power(t2,3) + 46240*Power(t2,4) - 
                  23863*Power(t2,5) - 12306*Power(t2,6) + 
                  2128*Power(t2,7))) - 
            Power(s2,2)*(-2933 + 7965*t2 + 11507*Power(t2,2) + 
               3608*Power(t2,3) + 3585*Power(t2,4) - 5081*Power(t2,5) - 
               1869*Power(t2,6) + 514*Power(t2,7) + 120*Power(t2,8) + 
               4*Power(t1,6)*(262 + 23*t2) + 
               Power(t1,5)*(-1938 - 13559*t2 + 2304*Power(t2,2) + 
                  210*Power(t2,3)) + 
               Power(t1,4)*(-9940 + 60488*t2 + 34214*Power(t2,2) - 
                  17359*Power(t2,3) + 45*Power(t2,4) + 30*Power(t2,5)) + 
               Power(t1,3)*(29166 - 87669*t2 - 136844*Power(t2,2) + 
                  12624*Power(t2,3) + 34260*Power(t2,4) - 
                  2979*Power(t2,5) - 56*Power(t2,6)) + 
               Power(t1,2)*(-28863 + 61555*t2 + 152152*Power(t2,2) + 
                  45322*Power(t2,3) - 54520*Power(t2,4) - 
                  16322*Power(t2,5) + 4914*Power(t2,6) - 16*Power(t2,7)) \
+ t1*(13481 - 29204*t2 - 63602*Power(t2,2) - 41100*Power(t2,3) + 
                  14855*Power(t2,4) + 22266*Power(t2,5) + 
                  133*Power(t2,6) - 1884*Power(t2,7) + 45*Power(t2,8)))) \
+ Power(s1,3)*(80 - 10*Power(t1,8) + 626*t2 - 28*Power(t2,2) - 
            34*Power(t2,3) - 216*Power(t2,4) + 334*Power(t2,5) - 
            348*Power(t2,6) + 98*Power(t2,7) + 
            Power(t1,7)*(-278 + 90*t2 - 8*Power(t2,2)) - 
            2*Power(t1,6)*(-863 - 907*t2 + 406*Power(t2,2) + 
               8*Power(t2,3)) - 
            2*Power(t1,5)*(1564 + 5529*t2 - 684*Power(t2,2) - 
               1226*Power(t2,3) + 84*Power(t2,4) + 5*Power(t2,5)) + 
            Power(t1,4)*(1970 + 22086*t2 + 4597*Power(t2,2) - 
               8238*Power(t2,3) - 874*Power(t2,4) + 430*Power(t2,5) + 
               11*Power(t2,6)) - 
            Power(t1,2)*(177 - 11385*t2 - 6545*Power(t2,2) + 
               2200*Power(t2,3) + 3879*Power(t2,4) + 167*Power(t2,5) - 
               431*Power(t2,6) + 2*Power(t2,7)) + 
            Power(t1,3)*(-13 - 21295*t2 - 10326*Power(t2,2) + 
               7526*Power(t2,3) + 4391*Power(t2,4) - 903*Power(t2,5) - 
               126*Power(t2,6) + 2*Power(t2,7)) + 
            2*t1*(-85 - 1826*t2 - 648*Power(t2,2) + 205*Power(t2,3) + 
               373*Power(t2,4) + 298*Power(t2,5) - 152*Power(t2,6) + 
               11*Power(t2,7)) + 
            Power(s2,10)*(5 + 4*Power(t1,2) + 41*t2 + 44*Power(t2,2) - 
               3*t1*(3 + 11*t2)) - 
            Power(s2,9)*(-15 + 4*Power(t1,3) + 244*t2 + 
               814*Power(t2,2) + 476*Power(t2,3) + 
               Power(t1,2)*(-10 + 53*t2) - 
               3*t1*(-7 + 95*t2 + 134*Power(t2,2))) + 
            Power(s2,8)*(-257 - 1136*t2 + 526*Power(t2,2) + 
               3151*Power(t2,3) + 1630*Power(t2,4) + 
               4*Power(t1,3)*(29 + 28*t2) - 
               Power(t1,2)*(423 + 1160*t2 + 163*Power(t2,2)) + 
               t1*(564 + 2128*t2 + 491*Power(t2,2) - 1127*Power(t2,3))) \
+ Power(s2,7)*(-267 + 3028*t2 + 10855*Power(t2,2) + 7063*Power(t2,3) - 
               2359*Power(t2,4) - 1983*Power(t2,5) + 
               Power(t1,4)*(-146 + 15*t2) + 
               Power(t1,3)*(674 - 283*t2 - 806*Power(t2,2)) + 
               Power(t1,2)*(-1261 + 2529*t2 + 7455*Power(t2,2) + 
                  2567*Power(t2,3)) - 
               t1*(-1000 + 5173*t2 + 18218*Power(t2,2) + 
                  11040*Power(t2,3) + 608*Power(t2,4))) - 
            Power(s2,6)*(-1891 + 6*Power(t1,5) - 11331*t2 - 
               8144*Power(t2,2) + 14427*Power(t2,3) + 
               15290*Power(t2,4) + 3142*Power(t2,5) + 26*Power(t2,6) + 
               Power(t1,4)*(-649 - 1821*t2 + 16*Power(t2,2)) + 
               Power(t1,3)*(3122 + 13231*t2 + 6605*Power(t2,2) - 
                  1946*Power(t2,3)) + 
               Power(t1,2)*(-6265 - 32641*t2 - 26519*Power(t2,2) + 
                  5598*Power(t2,3) + 7086*Power(t2,4)) + 
               t1*(5677 + 32606*t2 + 26684*Power(t2,2) - 
                  17643*Power(t2,3) - 22453*Power(t2,4) - 
                  6017*Power(t2,5))) + 
            Power(s2,5)*(3735 + 3*Power(t1,5)*(-368 + t2) + 1969*t2 - 
               27816*Power(t2,2) - 37117*Power(t2,3) - 
               10787*Power(t2,4) + 1728*Power(t2,5) + 
               3396*Power(t2,6) + 1740*Power(t2,7) + 
               Power(t1,4)*(6907 + 4858*t2 - 5466*Power(t2,2) - 
                  138*Power(t2,3)) + 
               Power(t1,3)*(-18037 - 21997*t2 + 25543*Power(t2,2) + 
                  24162*Power(t2,3) - 1674*Power(t2,4)) + 
               Power(t1,2)*(23436 + 29715*t2 - 61514*Power(t2,2) - 
                  89525*Power(t2,3) - 21276*Power(t2,4) + 
                  7563*Power(t2,5)) + 
               t1*(-14937 - 14576*t2 + 68763*Power(t2,2) + 
                  100072*Power(t2,3) + 37553*Power(t2,4) - 
                  8315*Power(t2,5) - 7976*Power(t2,6))) + 
            Power(s2,4)*(-961 + 34*Power(t1,6) - 24400*t2 - 
               43424*Power(t2,2) - 10099*Power(t2,3) + 
               7709*Power(t2,4) + 6546*Power(t2,5) + 9496*Power(t2,6) + 
               123*Power(t2,7) - 1156*Power(t2,8) + 
               Power(t1,5)*(-762 + 5226*t2 + 284*Power(t2,2)) + 
               Power(t1,4)*(4612 - 31624*t2 - 29553*Power(t2,2) + 
                  5006*Power(t2,3) + 248*Power(t2,4)) + 
               Power(t1,3)*(-8087 + 82949*t2 + 143140*Power(t2,2) + 
                  20988*Power(t2,3) - 26366*Power(t2,4) + 
                  346*Power(t2,5)) + 
               Power(t1,2)*(4298 - 115159*t2 - 225940*Power(t2,2) - 
                  95830*Power(t2,3) + 62083*Power(t2,4) + 
                  33096*Power(t2,5) - 3369*Power(t2,6)) + 
               t1*(866 + 83084*t2 + 154807*Power(t2,2) + 
                  71835*Power(t2,3) - 24554*Power(t2,4) - 
                  51414*Power(t2,5) - 8427*Power(t2,6) + 
                  4077*Power(t2,7))) + 
            s2*(-2178 + 3425*t2 + 4346*Power(t2,2) - 651*Power(t2,3) + 
               790*Power(t2,4) - 921*Power(t2,5) + 306*Power(t2,6) - 
               445*Power(t2,7) + 128*Power(t2,8) - 
               2*Power(t1,7)*(195 + 11*t2) + 
               Power(t1,6)*(166 + 5406*t2 - 530*Power(t2,2) - 
                  84*Power(t2,3)) + 
               Power(t1,5)*(10180 - 28329*t2 - 15360*Power(t2,2) + 
                  5764*Power(t2,3) + 128*Power(t2,4) - 11*Power(t2,5)) \
+ Power(t1,4)*(-32897 + 50112*t2 + 72056*Power(t2,2) - 
                  5284*Power(t2,3) - 12681*Power(t2,4) + 
                  872*Power(t2,5) + 28*Power(t2,6)) - 
               Power(t1,3)*(-43913 + 43129*t2 + 107885*Power(t2,2) + 
                  24296*Power(t2,3) - 28771*Power(t2,4) - 
                  5361*Power(t2,5) + 1717*Power(t2,6) + 10*Power(t2,7)) \
+ Power(t1,2)*(-30550 + 25788*t2 + 70990*Power(t2,2) + 
                  33195*Power(t2,3) - 14646*Power(t2,4) - 
                  13448*Power(t2,5) + 1318*Power(t2,6) + 
                  603*Power(t2,7) - 10*Power(t2,8)) + 
               t1*(11756 - 13259*t2 - 23299*Power(t2,2) - 
                  10454*Power(t2,3) + 1288*Power(t2,4) + 
                  4857*Power(t2,5) + 1773*Power(t2,6) - 
                  856*Power(t2,7) + 2*Power(t2,8))) - 
            Power(s2,3)*(8571 + 28438*t2 + 6719*Power(t2,2) - 
               18515*Power(t2,3) - 6316*Power(t2,4) - 
               8825*Power(t2,5) - 4157*Power(t2,6) + 3183*Power(t2,7) + 
               681*Power(t2,8) - 235*Power(t2,9) + 
               4*Power(t1,6)*(447 + 50*t2) + 
               2*Power(t1,5)*
                (-6303 - 7775*t2 + 2789*Power(t2,2) + 253*Power(t2,3)) \
+ Power(t1,4)*(37774 + 98081*t2 + 2718*Power(t2,2) - 
                  35496*Power(t2,3) + 1062*Power(t2,4) + 
                  113*Power(t2,5)) - 
               Power(t1,3)*(63516 + 210155*t2 + 90064*Power(t2,2) - 
                  122978*Power(t2,3) - 53668*Power(t2,4) + 
                  10695*Power(t2,5) + 68*Power(t2,6)) + 
               Power(t1,2)*(63749 + 214981*t2 + 132389*Power(t2,2) - 
                  105583*Power(t2,3) - 141311*Power(t2,4) - 
                  7739*Power(t2,5) + 15547*Power(t2,6) - 
                  587*Power(t2,7)) + 
               t1*(-35760 - 115863*t2 - 57726*Power(t2,2) + 
                  43580*Power(t2,3) + 75988*Power(t2,4) + 
                  39089*Power(t2,5) - 11354*Power(t2,6) - 
                  6116*Power(t2,7) + 794*Power(t2,8))) + 
            Power(s2,2)*(-8072 + 72*Power(t1,7) - 6522*t2 + 
               14786*Power(t2,2) + 7915*Power(t2,3) + 667*Power(t2,4) + 
               4029*Power(t2,5) - 2248*Power(t2,6) - 787*Power(t2,7) + 
               107*Power(t2,8) + 45*Power(t2,9) + 
               Power(t1,6)*(-3226 + 2598*t2 + 300*Power(t2,2)) + 
               2*Power(t1,5)*
                (12742 - 1571*t2 - 10922*Power(t2,2) + 
                  565*Power(t2,3) + 105*Power(t2,4)) + 
               Power(t1,4)*(-72151 - 39727*t2 + 96280*Power(t2,2) + 
                  42218*Power(t2,3) - 12291*Power(t2,4) - 
                  193*Power(t2,5) + 12*Power(t2,6)) + 
               Power(t1,3)*(102524 + 100119*t2 - 130805*Power(t2,2) - 
                  153568*Power(t2,3) + 2596*Power(t2,4) + 
                  21701*Power(t2,5) - 1277*Power(t2,6) - 20*Power(t2,7)) \
+ Power(t1,2)*(-82897 - 87703*t2 + 84635*Power(t2,2) + 
                  156208*Power(t2,3) + 46813*Power(t2,4) - 
                  31163*Power(t2,5) - 9445*Power(t2,6) + 
                  2066*Power(t2,7) - 2*Power(t2,8)) + 
               t1*(38266 + 34461*t2 - 44050*Power(t2,2) - 
                  54059*Power(t2,3) - 35110*Power(t2,4) + 
                  4601*Power(t2,5) + 11404*Power(t2,6) + 
                  509*Power(t2,7) - 744*Power(t2,8) + 10*Power(t2,9)))) + 
         s1*(-8*Power(t1,9) + 
            2*Power(t1,8)*(-61 + 8*t2 + 7*Power(t2,2)) + 
            2*Power(t1,7)*(976 + 179*t2 - 259*Power(t2,2) - 
               23*Power(t2,3) + 3*Power(t2,4)) - 
            Power(t1,6)*(8696 + 2787*t2 - 2222*Power(t2,2) - 
               1264*Power(t2,3) + 158*Power(t2,4) + 9*Power(t2,5)) + 
            Power(t1,5)*(19763 + 6712*t2 - 4104*Power(t2,2) - 
               4216*Power(t2,3) - 213*Power(t2,4) + 320*Power(t2,5) - 
               14*Power(t2,6)) + 
            Power(1 + t2,2)*(-789 + 1397*t2 - 861*Power(t2,2) + 
               533*Power(t2,3) - 535*Power(t2,4) + 359*Power(t2,5) - 
               119*Power(t2,6) + 15*Power(t2,7)) + 
            Power(t1,4)*(-27218 - 6889*t2 + 6474*Power(t2,2) + 
               4210*Power(t2,3) + 1478*Power(t2,4) - 547*Power(t2,5) - 
               98*Power(t2,6) + 22*Power(t2,7)) - 
            Power(t1,3)*(-24331 - 2736*t2 + 10461*Power(t2,2) + 
               519*Power(t2,3) + 101*Power(t2,4) + 416*Power(t2,5) - 
               243*Power(t2,6) + 17*Power(t2,7) + 4*Power(t2,8)) - 
            Power(t1,2)*(14163 + 159*t2 - 10981*Power(t2,2) + 
               757*Power(t2,3) + 2420*Power(t2,4) - 410*Power(t2,5) - 
               353*Power(t2,6) + 117*Power(t2,7) - 17*Power(t2,8) + 
               Power(t2,9)) - 
            2*t1*(-2475 - 97*t2 + 2876*Power(t2,2) + 74*Power(t2,3) - 
               879*Power(t2,4) - 195*Power(t2,5) + 274*Power(t2,6) - 
               8*Power(t2,7) - 20*Power(t2,8) + 2*Power(t2,9)) + 
            Power(s2,10)*Power(t2,2)*
             (15 + 12*Power(t1,2) + 43*t2 + 24*Power(t2,2) - 
               t1*(27 + 35*t2)) - 
            Power(s2,9)*t2*(-30 - 56*t2 + 240*Power(t2,2) + 
               410*Power(t2,3) + 144*Power(t2,4) + 
               3*Power(t1,3)*(8 + 5*t2) + 
               Power(t1,2)*(-78 - 47*t2 + 35*Power(t2,2)) + 
               t1*(84 + 88*t2 - 263*Power(t2,2) - 188*Power(t2,3))) + 
            Power(s2,8)*(15 - 23*t2 - 817*Power(t2,2) - 
               1285*Power(t2,3) - 39*Power(t2,4) + 704*Power(t2,5) + 
               275*Power(t2,6) - 
               2*Power(t1,4)*(-6 - 15*t2 + Power(t2,2)) + 
               Power(t1,3)*(-51 - 15*t2 + 336*Power(t2,2) + 
                  145*Power(t2,3)) - 
               Power(t1,2)*(-81 + 83*t2 + 1285*Power(t2,2) + 
                  1463*Power(t2,3) + 225*Power(t2,4)) + 
               t1*(-57 + 91*t2 + 1768*Power(t2,2) + 2547*Power(t2,3) + 
                  783*Power(t2,4) - 178*Power(t2,5))) + 
            Power(s2,7)*(-35 - 774*t2 - 1056*Power(t2,2) + 
               2410*Power(t2,3) + 5071*Power(t2,4) + 2574*Power(t2,5) + 
               52*Power(t2,6) - 139*Power(t2,7) + 
               Power(t1,5)*(-15 + 4*t2) - 
               Power(t1,4)*(-3 + 573*t2 + 547*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,3)*(116 + 2115*t2 + 3008*Power(t2,2) + 
                  546*Power(t2,3) - 380*Power(t2,4)) + 
               Power(t1,2)*(-216 - 3301*t2 - 5683*Power(t2,2) + 
                  406*Power(t2,3) + 3088*Power(t2,4) + 972*Power(t2,5)) \
+ t1*(147 + 2529*t2 + 4278*Power(t2,2) - 3234*Power(t2,3) - 
                  8348*Power(t2,4) - 3793*Power(t2,5) - 461*Power(t2,6))\
) + Power(s2,6)*(-223 - 2*Power(t1,6) - 44*t2 + 5391*Power(t2,2) + 
               11564*Power(t2,3) + 6456*Power(t2,4) - 
               1219*Power(t2,5) - 1889*Power(t2,6) - 638*Power(t2,7) - 
               134*Power(t2,8) + 
               Power(t1,5)*(272 + 659*t2 + 50*Power(t2,2)) + 
               Power(t1,4)*(-1117 - 2569*t2 + 543*Power(t2,2) + 
                  1673*Power(t2,3) + 80*Power(t2,4)) + 
               Power(t1,3)*(1962 + 4457*t2 - 6230*Power(t2,2) - 
                  13634*Power(t2,3) - 4774*Power(t2,4) + 
                  314*Power(t2,5)) + 
               Power(t1,2)*(-1874 - 3887*t2 + 16181*Power(t2,2) + 
                  34330*Power(t2,3) + 19173*Power(t2,4) + 
                  1048*Power(t2,5) - 1262*Power(t2,6)) + 
               t1*(982 + 1384*t2 - 15935*Power(t2,2) - 
                  33977*Power(t2,3) - 19992*Power(t2,4) - 
                  747*Power(t2,5) + 3261*Power(t2,6) + 1017*Power(t2,7))\
) + Power(s2,5)*(135 + 4354*t2 + 11760*Power(t2,2) + 6232*Power(t2,3) - 
               5711*Power(t2,4) - 6296*Power(t2,5) - 2772*Power(t2,6) - 
               972*Power(t2,7) + 216*Power(t2,8) + 166*Power(t2,9) - 
               Power(t1,6)*(257 + 64*t2) - 
               2*Power(t1,5)*
                (-418 + 901*t2 + 1500*Power(t2,2) + 159*Power(t2,3)) + 
               Power(t1,4)*(-1013 + 10313*t2 + 23256*Power(t2,2) + 
                  9698*Power(t2,3) - 1284*Power(t2,4) - 124*Power(t2,5)\
) + Power(t1,3)*(458 - 24463*t2 - 63071*Power(t2,2) - 
                  40080*Power(t2,3) + 2465*Power(t2,4) + 
                  6228*Power(t2,5) + 17*Power(t2,6)) + 
               Power(t1,2)*(199 + 29869*t2 + 79930*Power(t2,2) + 
                  56384*Power(t2,3) - 6304*Power(t2,4) - 
                  21091*Power(t2,5) - 5381*Power(t2,6) + 
                  621*Power(t2,7)) + 
               t1*(-358 - 18207*t2 - 48875*Power(t2,2) - 
                  31944*Power(t2,3) + 10369*Power(t2,4) + 
                  20896*Power(t2,5) + 9542*Power(t2,6) + 
                  203*Power(t2,7) - 686*Power(t2,8))) + 
            s2*(-4*Power(t1,8)*(37 + 18*t2) + 
               2*Power(t1,7)*
                (1141 + 885*t2 + 69*Power(t2,2) - 59*Power(t2,3)) - 
               Power(t1,6)*(11251 + 15482*t2 + 2846*Power(t2,2) - 
                  2310*Power(t2,3) - 253*Power(t2,4) + 20*Power(t2,5)) \
+ 2*Power(t1,5)*(14592 + 24909*t2 + 8300*Power(t2,2) - 
                  4318*Power(t2,3) - 2484*Power(t2,4) + 
                  231*Power(t2,5) + 14*Power(t2,6)) + 
               Power(t1,4)*(-46921 - 81721*t2 - 29966*Power(t2,2) + 
                  10160*Power(t2,3) + 13793*Power(t2,4) + 
                  1351*Power(t2,5) - 994*Power(t2,6) + 34*Power(t2,7)) \
+ Power(t1,3)*(49272 + 80737*t2 + 14850*Power(t2,2) - 
                  9670*Power(t2,3) - 8733*Power(t2,4) - 
                  4761*Power(t2,5) + 894*Power(t2,6) + 
                  382*Power(t2,7) - 59*Power(t2,8)) + 
               Power(1 + t2,2)*
                (-2225 - 170*t2 + 4061*Power(t2,2) - 
                  2442*Power(t2,3) + 553*Power(t2,4) - 
                  464*Power(t2,5) + 369*Power(t2,6) - 
                  108*Power(t2,7) + 10*Power(t2,8)) + 
               t1*(12950 + 22479*t2 - 8887*Power(t2,2) - 
                  17705*Power(t2,3) + 4459*Power(t2,4) + 
                  4107*Power(t2,5) - 593*Power(t2,6) - 
                  783*Power(t2,7) + 167*Power(t2,8) - 2*Power(t2,9)) + 
               Power(t1,2)*(-33143 - 52909*t2 + 8615*Power(t2,2) + 
                  18141*Power(t2,3) - 4371*Power(t2,4) + 
                  96*Power(t2,5) + 1451*Power(t2,6) - 329*Power(t2,7) - 
                  48*Power(t2,8) + 17*Power(t2,9))) + 
            Power(s2,4)*(1246 + 26*Power(t1,7) + 5448*t2 + 
               829*Power(t2,2) - 11491*Power(t2,3) - 8545*Power(t2,4) - 
               1232*Power(t2,5) - 2600*Power(t2,6) - 723*Power(t2,7) + 
               1095*Power(t2,8) + 50*Power(t2,9) - 53*Power(t2,10) + 
               Power(t1,6)*(938 + 2501*t2 + 502*Power(t2,2)) + 
               Power(t1,5)*(-5101 - 17516*t2 - 9240*Power(t2,2) + 
                  1940*Power(t2,3) + 472*Power(t2,4)) + 
               Power(t1,4)*(12148 + 51143*t2 + 38556*Power(t2,2) - 
                  12046*Power(t2,3) - 12314*Power(t2,4) - 
                  226*Power(t2,5) + 66*Power(t2,6)) - 
               Power(t1,3)*(16854 + 78319*t2 + 64903*Power(t2,2) - 
                  32592*Power(t2,3) - 57065*Power(t2,4) - 
                  10909*Power(t2,5) + 2548*Power(t2,6) + 91*Power(t2,7)\
) + Power(t1,2)*(14068 + 66216*t2 + 49546*Power(t2,2) - 
                  46480*Power(t2,3) - 79671*Power(t2,4) - 
                  36908*Power(t2,5) + 4539*Power(t2,6) + 
                  3249*Power(t2,7) - 93*Power(t2,8)) + 
               t1*(-6471 - 29473*t2 - 15290*Power(t2,2) + 
                  35561*Power(t2,3) + 42742*Power(t2,4) + 
                  24777*Power(t2,5) + 5650*Power(t2,6) - 
                  5013*Power(t2,7) - 903*Power(t2,8) + 172*Power(t2,9))) \
+ Power(s2,2)*(-1409 + 106*Power(t1,8) - 8960*t2 - 8404*Power(t2,2) + 
               6673*Power(t2,3) + 7229*Power(t2,4) - 2995*Power(t2,5) - 
               2048*Power(t2,6) + 744*Power(t2,7) + 122*Power(t2,8) + 
               9*Power(t2,9) - 18*Power(t2,10) + Power(t2,11) + 
               Power(t1,7)*(-334 + 610*t2 + 392*Power(t2,2)) + 
               2*Power(t1,6)*
                (398 - 5039*t2 - 3423*Power(t2,2) - 258*Power(t2,3) + 
                  130*Power(t2,4)) + 
               Power(t1,5)*(-822 + 40957*t2 + 49298*Power(t2,2) + 
                  7834*Power(t2,3) - 4382*Power(t2,4) - 
                  459*Power(t2,5) + 22*Power(t2,6)) - 
               Power(t1,4)*(2977 + 83291*t2 + 121619*Power(t2,2) + 
                  44533*Power(t2,3) - 15413*Power(t2,4) - 
                  8039*Power(t2,5) + 557*Power(t2,6) + 29*Power(t2,7)) \
+ Power(t1,3)*(10096 + 104661*t2 + 139780*Power(t2,2) + 
                  67302*Power(t2,3) - 6772*Power(t2,4) - 
                  20405*Power(t2,5) - 2408*Power(t2,6) + 
                  1224*Power(t2,7) - 30*Power(t2,8)) + 
               Power(t1,2)*(-12100 - 85507*t2 - 88397*Power(t2,2) - 
                  22754*Power(t2,3) - 4399*Power(t2,4) + 
                  8005*Power(t2,5) + 6771*Power(t2,6) - 
                  582*Power(t2,7) - 499*Power(t2,8) + 54*Power(t2,9)) + 
               t1*(6644 + 41608*t2 + 35796*Power(t2,2) - 
                  13922*Power(t2,3) - 7732*Power(t2,4) + 
                  7891*Power(t2,5) - 1376*Power(t2,6) - 
                  1382*Power(t2,7) + 198*Power(t2,8) + 85*Power(t2,9) - 
                  18*Power(t2,10))) + 
            Power(s2,3)*(953 - 2718*t2 - 13998*Power(t2,2) - 
               8464*Power(t2,3) + 6180*Power(t2,4) + 944*Power(t2,5) - 
               3646*Power(t2,6) + 495*Power(t2,7) + 689*Power(t2,8) - 
               110*Power(t2,9) - 26*Power(t2,10) + 5*Power(t2,11) - 
               2*Power(t1,7)*(397 + 185*t2) + 
               Power(t1,6)*(4984 + 3678*t2 - 1432*Power(t2,2) - 
                  660*Power(t2,3)) + 
               Power(t1,5)*(-15025 - 14864*t2 + 16790*Power(t2,2) + 
                  12546*Power(t2,3) + 571*Power(t2,4) - 222*Power(t2,5)) \
+ Power(t1,4)*(26811 + 27117*t2 - 55545*Power(t2,2) - 
                  74810*Power(t2,3) - 11891*Power(t2,4) + 
                  4517*Power(t2,5) + 343*Power(t2,6) - 8*Power(t2,7)) + 
               Power(t1,3)*(-29166 - 20909*t2 + 92002*Power(t2,2) + 
                  140878*Power(t2,3) + 58610*Power(t2,4) - 
                  13577*Power(t2,5) - 6898*Power(t2,6) + 
                  346*Power(t2,7) + 10*Power(t2,8)) + 
               Power(t1,2)*(18796 + 1035*t2 - 91319*Power(t2,2) - 
                  112358*Power(t2,3) - 65212*Power(t2,4) - 
                  4175*Power(t2,5) + 15547*Power(t2,6) + 
                  2022*Power(t2,7) - 722*Power(t2,8) + 10*Power(t2,9)) + 
               t1*(-6559 + 7031*t2 + 53502*Power(t2,2) + 
                  42736*Power(t2,3) + 11963*Power(t2,4) + 
                  10424*Power(t2,5) - 1947*Power(t2,6) - 
                  4654*Power(t2,7) + 146*Power(t2,8) + 271*Power(t2,9) - 
                  17*Power(t2,10)))) + 
         Power(s1,2)*(593 - 743*t2 - 617*Power(t2,2) + 301*Power(t2,3) + 
            189*Power(t2,4) + 11*Power(t2,5) - 307*Power(t2,6) + 
            239*Power(t2,7) - 50*Power(t2,8) + 
            10*Power(t1,8)*(-5 + 2*t2) - 
            2*Power(t1,7)*(150 - 334*t2 + 13*Power(t2,2) + 
               3*Power(t2,3)) + 
            Power(t1,6)*(3997 - 4142*t2 - 2370*Power(t2,2) + 
               592*Power(t2,3) + 25*Power(t2,4)) + 
            Power(t1,5)*(-13245 + 8578*t2 + 11798*Power(t2,2) - 
               508*Power(t2,3) - 1373*Power(t2,4) + 72*Power(t2,5) + 
               2*Power(t2,6)) - 
            Power(t1,4)*(-21390 + 9429*t2 + 20553*Power(t2,2) + 
               3952*Power(t2,3) - 3922*Power(t2,4) - 429*Power(t2,5) + 
               145*Power(t2,6) + 2*Power(t2,7)) + 
            Power(t1,3)*(-19711 + 8296*t2 + 17301*Power(t2,2) + 
               5828*Power(t2,3) - 2437*Power(t2,4) - 1822*Power(t2,5) + 
               271*Power(t2,6) + 34*Power(t2,7)) + 
            t1*(-3664 + 3571*t2 + 2928*Power(t2,2) - 830*Power(t2,3) - 
               515*Power(t2,4) + 415*Power(t2,5) - 190*Power(t2,6) + 
               12*Power(t2,7) + Power(t2,8)) + 
            Power(t1,2)*(10990 - 6819*t2 - 8455*Power(t2,2) - 
               1465*Power(t2,3) + 264*Power(t2,4) + 895*Power(t2,5) + 
               229*Power(t2,6) - 139*Power(t2,7) + 4*Power(t2,8)) - 
            Power(s2,10)*t2*(15 + 12*Power(t1,2) + 63*t2 + 
               46*Power(t2,2) - 3*t1*(9 + 17*t2)) + 
            Power(s2,9)*(-15 - 50*t2 + 364*Power(t2,2) + 
               818*Power(t2,3) + 359*Power(t2,4) + 
               Power(t1,3)*(12 + 13*t2) + 
               Power(t1,2)*(-39 - 37*t2 + 67*Power(t2,2)) + 
               t1*(42 + 74*t2 - 413*Power(t2,2) - 390*Power(t2,3))) + 
            Power(s2,8)*(15 + 795*t2 + 1812*Power(t2,2) - 
               241*Power(t2,3) - 2078*Power(t2,4) - 906*Power(t2,5) + 
               Power(t1,4)*(-13 + 3*t2) - 
               Power(t1,3)*(2 + 349*t2 + 194*Power(t2,2)) + 
               2*Power(t1,2)*
                (29 + 643*t2 + 983*Power(t2,2) + 152*Power(t2,3)) + 
               t1*(-58 - 1735*t2 - 3500*Power(t2,2) - 
                  1009*Power(t2,3) + 652*Power(t2,4))) + 
            Power(s2,7)*(365 - 3*Power(t1,5) + 918*t2 - 
               4125*Power(t2,2) - 10558*Power(t2,3) - 
               5905*Power(t2,4) + 621*Power(t2,5) + 743*Power(t2,6) + 
               Power(t1,4)*(282 + 483*t2 - 9*Power(t2,2)) + 
               Power(t1,3)*(-1016 - 2476*t2 - 143*Power(t2,2) + 
                  805*Power(t2,3)) + 
               Power(t1,2)*(1563 + 4673*t2 - 2336*Power(t2,2) - 
                  6958*Power(t2,3) - 2210*Power(t2,4)) + 
               t1*(-1191 - 3598*t2 + 6439*Power(t2,2) + 
                  17637*Power(t2,3) + 8990*Power(t2,4) + 886*Power(t2,5)\
)) + Power(s2,6)*(-1 - 5593*t2 - 17273*Power(t2,2) - 
               10575*Power(t2,3) + 7036*Power(t2,4) + 
               7578*Power(t2,5) + 1977*Power(t2,6) + 286*Power(t2,7) + 
               Power(t1,5)*(-289 + 7*t2) - 
               Power(t1,4)*(-1002 + 1390*t2 + 2689*Power(t2,2) + 
                  68*Power(t2,3)) + 
               Power(t1,3)*(-1601 + 8156*t2 + 20547*Power(t2,2) + 
                  8221*Power(t2,3) - 1161*Power(t2,4)) + 
               Power(t1,2)*(1351 - 17922*t2 - 50921*Power(t2,2) - 
                  32750*Power(t2,3) + 857*Power(t2,4) + 
                  4112*Power(t2,5)) - 
               2*t1*(231 - 8371*t2 - 25201*Power(t2,2) - 
                  16775*Power(t2,3) + 2944*Power(t2,4) + 
                  5926*Power(t2,5) + 1677*Power(t2,6))) + 
            Power(s2,5)*(-2160 + 2*Power(t1,6) - 11523*t2 - 
               6028*Power(t2,2) + 20166*Power(t2,3) + 
               22548*Power(t2,4) + 7856*Power(t2,5) + 
               1348*Power(t2,6) - 1206*Power(t2,7) - 723*Power(t2,8) + 
               Power(t1,5)*(1202 + 3185*t2 + 208*Power(t2,2)) + 
               Power(t1,4)*(-5767 - 22201*t2 - 10924*Power(t2,2) + 
                  4163*Power(t2,3) + 218*Power(t2,4)) + 
               Power(t1,3)*(12540 + 59095*t2 + 46429*Power(t2,2) - 
                  16200*Power(t2,3) - 17372*Power(t2,4) + 
                  479*Power(t2,5)) + 
               Power(t1,2)*(-14757 - 75646*t2 - 64060*Power(t2,2) + 
                  39147*Power(t2,3) + 61794*Power(t2,4) + 
                  14924*Power(t2,5) - 3049*Power(t2,6)) + 
               t1*(8940 + 47090*t2 + 34417*Power(t2,2) - 
                  46566*Power(t2,3) - 65664*Power(t2,4) - 
                  26518*Power(t2,5) + 1675*Power(t2,6) + 
                  3162*Power(t2,7))) - 
            Power(s2,4)*(2734 - 1332*t2 - 27941*Power(t2,2) - 
               31838*Power(t2,3) - 6475*Power(t2,4) - 
               1777*Power(t2,5) + 610*Power(t2,6) + 4373*Power(t2,7) + 
               148*Power(t2,8) - 338*Power(t2,9) + 
               Power(t1,6)*(1301 + 240*t2) + 
               Power(t1,5)*(-8227 - 5376*t2 + 5610*Power(t2,2) + 
                  636*Power(t2,3)) + 
               Power(t1,4)*(23373 + 24816*t2 - 34965*Power(t2,2) - 
                  28316*Power(t2,3) + 1567*Power(t2,4) + 
                  189*Power(t2,5)) + 
               Power(t1,3)*(-36265 - 41839*t2 + 92717*Power(t2,2) + 
                  133672*Power(t2,3) + 22778*Power(t2,4) - 
                  12037*Power(t2,5) - 104*Power(t2,6)) + 
               Power(t1,2)*(31548 + 28331*t2 - 128679*Power(t2,2) - 
                  200571*Power(t2,3) - 85400*Power(t2,4) + 
                  25618*Power(t2,5) + 14432*Power(t2,6) - 
                  908*Power(t2,7)) + 
               t1*(-14464 - 4840*t2 + 93372*Power(t2,2) + 
                  125823*Power(t2,3) + 61195*Power(t2,4) + 
                  1353*Power(t2,5) - 22246*Power(t2,6) - 
                  3817*Power(t2,7) + 1180*Power(t2,8))) + 
            s2*(3931 + 32*Power(t1,8) + 2557*t2 - 6618*Power(t2,2) - 
               1988*Power(t2,3) + 2247*Power(t2,4) - 313*Power(t2,5) + 
               8*Power(t2,6) - 370*Power(t2,7) + 272*Power(t2,8) - 
               46*Power(t2,9) + 
               4*Power(t1,7)*(-323 + 122*t2 + 24*Power(t2,2)) + 
               2*Power(t1,6)*
                (6211 + 350*t2 - 2942*Power(t2,2) - 39*Power(t2,3) + 
                  33*Power(t2,4)) + 
               Power(t1,5)*(-43516 - 21125*t2 + 29240*Power(t2,2) + 
                  13162*Power(t2,3) - 2716*Power(t2,4) - 
                  111*Power(t2,5) + 2*Power(t2,6)) + 
               Power(t1,4)*(78347 + 59875*t2 - 49546*Power(t2,2) - 
                  51757*Power(t2,3) + 33*Power(t2,4) + 
                  5417*Power(t2,5) - 258*Power(t2,6) - 5*Power(t2,7)) + 
               Power(t1,3)*(-83428 - 69482*t2 + 44583*Power(t2,2) + 
                  66194*Power(t2,3) + 16114*Power(t2,4) - 
                  10202*Power(t2,5) - 2109*Power(t2,6) + 
                  480*Power(t2,7) + 2*Power(t2,8)) + 
               2*t1*(-10862 - 6531*t2 + 11736*Power(t2,2) + 
                  4096*Power(t2,3) - 48*Power(t2,4) + 
                  315*Power(t2,5) - 42*Power(t2,6) - 368*Power(t2,7) + 
                  88*Power(t2,8)) + 
               Power(t1,2)*(55228 + 40049*t2 - 35331*Power(t2,2) - 
                  34047*Power(t2,3) - 14263*Power(t2,4) + 
                  2314*Power(t2,5) + 4191*Power(t2,6) - 
                  181*Power(t2,7) - 153*Power(t2,8) + Power(t2,9))) + 
            Power(s2,3)*(1853 + 100*Power(t1,7) + 20572*t2 + 
               29209*Power(t2,2) - 1360*Power(t2,3) - 
               7772*Power(t2,4) + 3107*Power(t2,5) - 3468*Power(t2,6) - 
               2322*Power(t2,7) + 893*Power(t2,8) + 187*Power(t2,9) - 
               51*Power(t2,10) + 
               Power(t1,6)*(-526 + 3526*t2 + 636*Power(t2,2)) + 
               Power(t1,5)*(2741 - 28888*t2 - 22174*Power(t2,2) + 
                  2094*Power(t2,3) + 509*Power(t2,4)) + 
               Power(t1,4)*(-5356 + 90009*t2 + 135065*Power(t2,2) + 
                  14676*Power(t2,3) - 19406*Power(t2,4) - 
                  329*Power(t2,5) + 47*Power(t2,6)) - 
               Power(t1,3)*(-1644 + 150494*t2 + 274321*Power(t2,2) + 
                  108119*Power(t2,3) - 64174*Power(t2,4) - 
                  27426*Power(t2,5) + 3223*Power(t2,6) + 49*Power(t2,7)\
) + Power(t1,2)*(5699 + 149407*t2 + 257392*Power(t2,2) + 
                  138152*Power(t2,3) - 34925*Power(t2,4) - 
                  67943*Power(t2,5) - 5972*Power(t2,6) + 
                  4850*Power(t2,7) - 120*Power(t2,8)) + 
               t1*(-6155 - 84132*t2 - 125609*Power(t2,2) - 
                  45857*Power(t2,3) + 2845*Power(t2,4) + 
                  27500*Power(t2,5) + 18837*Power(t2,6) - 
                  2767*Power(t2,7) - 1820*Power(t2,8) + 174*Power(t2,9))\
) + Power(s2,2)*(6111 + 16820*t2 + 650*Power(t2,2) - 14231*Power(t2,3) + 
               427*Power(t2,4) + 2377*Power(t2,5) - 2406*Power(t2,6) + 
               315*Power(t2,7) + 148*Power(t2,8) + 23*Power(t2,9) - 
               10*Power(t2,10) - 2*Power(t1,7)*(459 + 125*t2) + 
               Power(t1,6)*(8626 + 8356*t2 - 1444*Power(t2,2) - 
                  436*Power(t2,3)) - 
               Power(t1,5)*(31835 + 64173*t2 + 3754*Power(t2,2) - 
                  15604*Power(t2,3) - 329*Power(t2,4) + 107*Power(t2,5)) \
+ Power(t1,4)*(64948 + 172836*t2 + 63731*Power(t2,2) - 
                  65438*Power(t2,3) - 26870*Power(t2,4) + 
                  4424*Power(t2,5) + 135*Power(t2,6) - 2*Power(t2,7)) + 
               Power(t1,2)*(65795 + 173796*t2 + 86769*Power(t2,2) - 
                  39448*Power(t2,3) - 72632*Power(t2,4) - 
                  25768*Power(t2,5) + 8937*Power(t2,6) + 
                  3150*Power(t2,7) - 503*Power(t2,8)) + 
               Power(t1,3)*(-82405 - 229548*t2 - 127775*Power(t2,2) + 
                  75413*Power(t2,3) + 86160*Power(t2,4) + 
                  3416*Power(t2,5) - 7675*Power(t2,6) + 
                  303*Power(t2,7) + 3*Power(t2,8)) - 
               t1*(30322 + 77837*t2 + 18303*Power(t2,2) - 
                  29268*Power(t2,3) - 12550*Power(t2,4) - 
                  14184*Power(t2,5) - 1314*Power(t2,6) + 
                  3020*Power(t2,7) + 310*Power(t2,8) - 173*Power(t2,9) + 
                  Power(t2,10)))) + 
         Power(s,9)*((-2 + 3*s2)*Power(t1,4) + 
            Power(t1,3)*(-3 + Power(s2,3) + 
               s1*(24 - 26*s2 + Power(s2,2)) - 13*t2 - 
               Power(s2,2)*(23 + 2*t2) + s2*(26 + 11*t2)) + 
            Power(t1,2)*(-6 + 
               Power(s1,2)*(17 - 21*s2 + 2*Power(s2,2)) + 25*t2 + 
               51*Power(t2,2) - 2*Power(s2,3)*(1 + t2) + 
               6*Power(s2,2)*(3 + 13*t2 + Power(t2,2)) - 
               s2*(12 + 104*t2 + 51*Power(t2,2)) + 
               s1*(-8 + Power(s2,3) - 81*t2 + 13*s2*(2 + 7*t2) - 
                  Power(s2,2)*(15 + 7*t2))) + 
            t1*(5 + Power(s1,3)*Power(-1 + s2,2) - 41*Power(t2,2) - 
               55*Power(t2,3) + Power(s2,3)*(1 + 3*t2 + Power(t2,2)) - 
               Power(s2,2)*(1 + 50*t2 + 87*Power(t2,2) + 
                  6*Power(t2,3)) + 
               s2*(-5 + 51*t2 + 130*Power(t2,2) + 57*Power(t2,3)) - 
               Power(s1,2)*(-3 + s2*(2 - 42*t2) + 32*t2 + 
                  Power(s2,2)*(1 + 6*t2)) + 
               s1*(-9 + 26*t2 + 90*Power(t2,2) - Power(s2,3)*(1 + t2) + 
                  s2*(9 - 79*t2 - 104*Power(t2,2)) + 
                  Power(s2,2)*(1 + 46*t2 + 11*Power(t2,2)))) + 
            t2*(-1 - Power(s1,3)*Power(-1 + s2,2) + 6*t2 + 
               19*Power(t2,2) + 19*Power(t2,3) - Power(s2,3)*(1 + t2) + 
               Power(s2,2)*(5 + 32*t2 + 32*Power(t2,2) + 
                  2*Power(t2,3)) - 
               s2*(3 + 39*t2 + 52*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s1,2)*(1 + 15*t2 + Power(s2,2)*(5 + 4*t2) - 
                  3*s2*(2 + 7*t2)) + 
               s1*(1 + Power(s2,3) - 18*t2 - 33*Power(t2,2) - 
                  Power(s2,2)*(9 + 31*t2 + 5*Power(t2,2)) + 
                  s2*(7 + 53*t2 + 39*Power(t2,2))))) - 
         Power(s,8)*(-5 + 11*t1 - 2*Power(t1,2) - 16*Power(t1,3) + 
            27*Power(t1,4) - 7*t2 - 28*t1*t2 + 144*Power(t1,2)*t2 - 
            115*Power(t1,3)*t2 + 11*Power(t1,4)*t2 + 26*Power(t2,2) - 
            172*t1*Power(t2,2) + 131*Power(t1,2)*Power(t2,2) + 
            19*Power(t1,3)*Power(t2,2) + 44*Power(t2,3) - 
            25*t1*Power(t2,3) - 123*Power(t1,2)*Power(t2,3) - 
            18*Power(t2,4) + 145*t1*Power(t2,4) - 52*Power(t2,5) + 
            Power(s1,4)*(-1 + s2)*
             (Power(s2,2) - 7*t1 + s2*(-1 + 9*t1 - 8*t2) + 6*t2) + 
            Power(s2,4)*(-1 + t1)*
             (7*Power(t1,2) + 6*t2*(1 + t2) - t1*(7 + 13*t2)) + 
            Power(s2,3)*(1 + 2*Power(t1,4) + 55*t2 + 190*Power(t2,2) + 
               148*Power(t2,3) + 12*Power(t2,4) - 
               2*Power(t1,3)*(52 + 15*t2) + 
               Power(t1,2)*(119 + 375*t2 + 66*Power(t2,2)) - 
               t1*(33 + 319*t2 + 419*Power(t2,2) + 50*Power(t2,3))) + 
            Power(s2,2)*(-5 - 72*t2 - 289*Power(t2,2) - 
               360*Power(t2,3) - 177*Power(t2,4) - 12*Power(t2,5) + 
               2*Power(t1,4)*(9 + t2) + 
               2*Power(t1,3)*(71 + 39*t2 + 3*Power(t2,2)) - 
               Power(t1,2)*(135 + 598*t2 + 387*Power(t2,2) + 
                  30*Power(t2,3)) + 
               t1*(25 + 445*t2 + 816*Power(t2,2) + 468*Power(t2,3) + 
                  34*Power(t2,4))) + 
            s2*(9 - Power(t1,5) + 26*t2 + 65*Power(t2,2) + 
               172*Power(t2,3) + 190*Power(t2,4) + 57*Power(t2,5) - 
               Power(t1,4)*(47 + 16*t2) + 
               Power(t1,3)*(-22 + 60*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*(22 + 82*t2 + 211*Power(t2,2) + 
                  115*Power(t2,3)) - 
               t1*(6 + 93*t2 + 232*Power(t2,2) + 414*Power(t2,3) + 
                  152*Power(t2,4))) + 
            Power(s1,3)*(3 + 61*Power(t1,2) + 
               Power(s2,3)*(-7 + 14*t1 - 9*t2) + 3*t2 + 
               53*Power(t2,2) - 2*t1*(-5 + 57*t2) + 
               Power(s2,2)*(15 + 18*Power(t1,2) + 51*t2 + 
                  38*Power(t2,2) - 2*t1*(22 + 29*t2)) - 
               s2*(11 + 94*Power(t1,2) + 41*t2 + 102*Power(t2,2) - 
                  2*t1*(8 + 99*t2))) + 
            Power(s1,2)*(-13 - Power(s2,4)*(-1 + t1) + 83*Power(t1,3) + 
               Power(t1,2)*(3 - 315*t2) + 17*t2 - 26*Power(t2,2) - 
               140*Power(t2,3) + t1*(-32 + 11*t2 + 372*Power(t2,2)) + 
               Power(s2,3)*(11 + 35*Power(t1,2) + 64*t2 + 
                  27*Power(t2,2) - t1*(56 + 67*t2)) + 
               Power(s2,2)*(-32 + 9*Power(t1,3) - 138*t2 - 
                  263*Power(t2,2) - 64*Power(t2,3) - 
                  2*Power(t1,2)*(89 + 36*t2) + 
                  t1*(98 + 445*t2 + 127*Power(t2,2))) + 
               s2*(-96*Power(t1,3) + 10*Power(t1,2)*(16 + 41*t2) + 
                  t1*(3 - 417*t2 - 533*Power(t2,2)) + 
                  3*(11 + 15*t2 + 90*Power(t2,2) + 73*Power(t2,3)))) + 
            s1*(15 - 16*Power(t1,4) + Power(t1,3)*(2 - 88*t2) + 
               Power(s2,4)*(-1 + t1)*(1 + 6*t1 - 5*t2) - 8*t2 - 
               51*Power(t2,2) + 41*Power(t2,3) + 145*Power(t2,4) + 
               Power(t1,2)*(-56 - 27*t2 + 369*Power(t2,2)) + 
               t1*(5 + 123*t2 - 16*Power(t2,2) - 410*Power(t2,3)) + 
               Power(s2,3)*(-6 + 24*Power(t1,3) - 106*t2 - 
                  190*Power(t2,2) - 31*Power(t2,3) - 
                  Power(t1,2)*(125 + 96*t2) + 
                  t1*(71 + 330*t2 + 103*Power(t2,2))) + 
               Power(s2,2)*(24 + 161*t2 + 480*Power(t2,2) + 
                  391*Power(t2,3) + 46*Power(t2,4) - 
                  Power(t1,3)*(199 + 20*t2) + 
                  Power(t1,2)*(267 + 721*t2 + 86*Power(t2,2)) - 
                  t1*(82 + 770*t2 + 913*Power(t2,2) + 112*Power(t2,3))) \
+ s2*(26*Power(t1,4) + 5*Power(t1,3)*(34 + 15*t2) - 
                  Power(t1,2)*(87 + 611*t2 + 416*Power(t2,2)) + 
                  t1*(-1 + 300*t2 + 861*Power(t2,2) + 503*Power(t2,3)) - 
                  2*(16 + 20*t2 + 111*Power(t2,2) + 210*Power(t2,3) + 
                     94*Power(t2,4))))) + 
         Power(s,7)*(-17 + 39*t1 + 3*Power(t1,2) - Power(t1,3) - 
            38*Power(t1,4) - 6*Power(t1,5) + 5*t2 - 99*t1*t2 - 
            2*Power(t1,2)*t2 + 129*Power(t1,3)*t2 - 59*Power(t1,4)*t2 + 
            Power(t1,5)*t2 + 70*Power(t2,2) - 99*t1*Power(t2,2) - 
            462*Power(t1,2)*Power(t2,2) + 377*Power(t1,3)*Power(t2,2) - 
            29*Power(t1,4)*Power(t2,2) + 2*Power(t2,3) + 
            557*t1*Power(t2,3) - 633*Power(t1,2)*Power(t2,3) + 
            Power(t1,3)*Power(t2,3) - 186*Power(t2,4) + 
            401*t1*Power(t2,4) + 161*Power(t1,2)*Power(t2,4) - 
            80*Power(t2,5) - 214*t1*Power(t2,5) + 80*Power(t2,6) + 
            Power(s1,5)*(8*Power(s2,3) + 
               2*Power(s2,2)*(-7 + 18*t1 - 14*t2) + 3*(7*t1 - 5*t2) + 
               s2*(6 - 56*t1 + 42*t2)) + 
            Power(s2,5)*(20*Power(t1,3) - 2*Power(t1,2)*(20 + 17*t2) - 
               t2*(15 + 16*t2 + Power(t2,2)) + 
               t1*(20 + 49*t2 + 15*Power(t2,2))) + 
            Power(s2,4)*(15 + 8*Power(t1,4) + 205*t2 + 
               501*Power(t2,2) + 320*Power(t2,3) + 17*Power(t2,4) - 
               Power(t1,3)*(280 + 123*t2) + 
               Power(t1,2)*(411 + 1014*t2 + 233*Power(t2,2)) - 
               t1*(174 + 981*t2 + 1049*Power(t2,2) + 135*Power(t2,3))) - 
            Power(s2,3)*(26 + 302*t2 + 944*Power(t2,2) + 
               1276*Power(t2,3) + 714*Power(t2,4) + 74*Power(t2,5) + 
               Power(t1,4)*(-79 + 3*t2) - 
               Power(t1,3)*(380 + 330*t2 + 139*Power(t2,2)) + 
               Power(t1,2)*(578 + 1959*t2 + 1722*Power(t2,2) + 
                  343*Power(t2,3)) - 
               t1*(225 + 1709*t2 + 2938*Power(t2,2) + 
                  2027*Power(t2,3) + 281*Power(t2,4))) + 
            Power(s2,2)*(4 - 2*Power(t1,5) + 64*t2 + 191*Power(t2,2) + 
               776*Power(t2,3) + 975*Power(t2,4) + 420*Power(t2,5) + 
               30*Power(t2,6) - 
               Power(t1,4)*(343 + 116*t2 + 12*Power(t2,2)) + 
               3*Power(t1,3)*
                (74 + 244*t2 + Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(7 - 263*t2 + 625*Power(t2,2) + 
                  770*Power(t2,3) + 54*Power(t2,4)) - 
               t1*(8 + 337*t2 + 1027*Power(t2,2) + 1989*Power(t2,3) + 
                  1075*Power(t2,4) + 78*Power(t2,5))) + 
            s2*(22 + 28*t2 + 177*Power(t2,2) + 201*Power(t2,3) - 
               124*Power(t2,4) - 309*Power(t2,5) - 91*Power(t2,6) + 
               Power(t1,5)*(25 + 7*t2) + 
               4*Power(t1,4)*(61 + 23*t2 + 7*Power(t2,2)) - 
               Power(t1,3)*(281 + 911*t2 + 320*Power(t2,2) + 
                  35*Power(t2,3)) + 
               Power(t1,2)*(157 + 1121*t2 + 1130*Power(t2,2) - 
                  45*Power(t2,3) - 133*Power(t2,4)) + 
               t1*(-87 - 282*t2 - 737*Power(t2,2) - 339*Power(t2,3) + 
                  557*Power(t2,4) + 224*Power(t2,5))) + 
            Power(s1,4)*(14 + 119*Power(t1,2) + t1*(15 - 236*t2) + 
               Power(s2,3)*(-49 + 112*t1 - 78*t2) - 5*t2 + 
               110*Power(t2,2) + 
               Power(s2,2)*(91 + 72*Power(t1,2) + 226*t2 + 
                  154*Power(t2,2) - 8*t1*(34 + 31*t2)) - 
               s2*(58 + 236*Power(t1,2) + 118*t2 + 283*Power(t2,2) - 
                  6*t1*(20 + 91*t2))) + 
            Power(s1,3)*(-50 + 163*Power(t1,3) + 
               Power(t1,2)*(139 - 682*t2) + 27*t2 + 73*Power(t2,2) - 
               320*Power(t2,3) + t1*(-41 - 217*t2 + 842*Power(t2,2)) + 
               Power(s2,4)*(38*t1 - 5*(1 + t2)) + 
               Power(s2,3)*(81 + 244*Power(t1,2) + 442*t2 + 
                  268*Power(t2,2) - 3*t1*(174 + 197*t2)) + 
               s2*(142 - 192*Power(t1,3) + 166*t2 + 637*Power(t2,2) + 
                  688*Power(t2,3) + Power(t1,2)*(407 + 1061*t2) - 
                  t1*(133 + 1124*t2 + 1574*Power(t2,2))) + 
               Power(s2,2)*(36*Power(t1,3) - 
                  Power(t1,2)*(769 + 324*t2) + 
                  t1*(712 + 1995*t2 + 626*Power(t2,2)) - 
                  2*(80 + 342*t2 + 527*Power(t2,2) + 162*Power(t2,3)))) \
- Power(s1,2)*(-33 + 55*Power(t1,4) - 75*t2 + 230*Power(t2,2) + 
               211*Power(t2,3) - 450*Power(t2,4) + 
               Power(s2,5)*(-7 + 8*t1 + t2) + 
               3*Power(t1,3)*(-41 + 85*t2) + 
               Power(t1,2)*(259 + 718*t2 - 1134*Power(t2,2)) + 
               t1*(15 - 398*t2 - 762*Power(t2,2) + 1274*Power(t2,3)) - 
               Power(s2,4)*(22 + 140*Power(t1,2) + 147*t2 + 
                  27*Power(t2,2) - t1*(217 + 193*t2)) + 
               Power(s2,3)*(98 - 156*Power(t1,3) + 742*t2 + 
                  1442*Power(t2,2) + 408*Power(t2,3) + 
                  9*Power(t1,2)*(118 + 95*t2) - 
                  t1*(870 + 2774*t2 + 1143*Power(t2,2))) + 
               Power(s2,2)*(-130 - 643*t2 - 2166*Power(t2,2) - 
                  1906*Power(t2,3) - 328*Power(t2,4) + 
                  44*Power(t1,3)*(15 + 2*t2) - 
                  2*Power(t1,2)*(609 + 1507*t2 + 245*Power(t2,2)) + 
                  t1*(628 + 3904*t2 + 4409*Power(t2,2) + 
                     730*Power(t2,3))) + 
               s2*(106 - 100*Power(t1,4) + 100*t2 + 333*Power(t2,2) + 
                  1253*Power(t2,3) + 786*Power(t2,4) - 
                  Power(t1,3)*(327 + 191*t2) + 
                  Power(t1,2)*(-16 + 1374*t2 + 1463*Power(t2,2)) - 
                  t1*(-24 + 672*t2 + 2457*Power(t2,2) + 
                     1958*Power(t2,3)))) + 
            s1*(21 - 86*t2 - 31*Power(t2,2) + 375*Power(t2,3) + 
               223*Power(t2,4) - 305*Power(t2,5) + 
               5*Power(t1,4)*(37 + 15*t2) + 
               Power(t1,3)*(-94 - 725*t2 + 101*Power(t2,2)) + 
               Power(t1,2)*(-4 + 825*t2 + 1278*Power(t2,2) - 
                  732*Power(t2,3)) + 
               t1*(-23 + 167*t2 - 966*Power(t2,2) - 961*Power(t2,3) + 
                  861*Power(t2,4)) + 
               Power(s2,5)*(-7 + 12*Power(t1,2) + 9*t2 + 
                  2*Power(t2,2) - t1*(5 + 7*t2)) + 
               Power(s2,4)*(-31 + 110*Power(t1,3) - 327*t2 - 
                  442*Power(t2,2) - 39*Power(t2,3) - 
                  Power(t1,2)*(464 + 367*t2) + 
                  2*t1*(165 + 502*t2 + 145*Power(t2,2))) + 
               Power(s2,3)*(80 + 16*Power(t1,4) + 661*t2 + 
                  1960*Power(t2,2) + 1763*Power(t2,3) + 
                  284*Power(t2,4) - Power(t1,3)*(770 + 317*t2) + 
                  Power(t1,2)*(1313 + 3282*t2 + 962*Power(t2,2)) - 
                  t1*(654 + 3663*t2 + 4390*Power(t2,2) + 
                     945*Power(t2,3))) + 
               s2*(-10 - 8*Power(t1,5) - 20*t2 - 210*Power(t2,2) + 
                  349*Power(t2,3) + 1037*Power(t2,4) + 
                  430*Power(t2,5) - Power(t1,4)*(349 + 119*t2) + 
                  Power(t1,3)*(211 + 488*t2 + 24*Power(t2,2)) + 
                  Power(t1,2)*
                   (-333 - 877*t2 + 834*Power(t2,2) + 771*Power(t2,3)) \
+ t1*(194 + 177*t2 - 123*Power(t2,2) - 2010*Power(t2,3) - 
                     1098*Power(t2,4))) + 
               Power(s2,2)*(-45 - 214*t2 - 1351*Power(t2,2) - 
                  2548*Power(t2,3) - 1484*Power(t2,4) - 
                  160*Power(t2,5) + Power(t1,4)*(173 + 16*t2) + 
                  Power(t1,3)*(530 + 469*t2 + 42*Power(t2,2)) - 
                  Power(t1,2)*
                   (513 + 2862*t2 + 2919*Power(t2,2) + 292*Power(t2,3)) \
+ t1*(135 + 2391*t2 + 5301*Power(t2,2) + 3761*Power(t2,3) + 
                     394*Power(t2,4))))) + 
         Power(s,6)*(-33 + 138*t1 - 190*Power(t1,2) + 142*Power(t1,3) + 
            28*Power(t1,4) - 109*Power(t1,5) + 9*Power(t1,6) - 108*t2 + 
            586*t1*t2 - 508*Power(t1,2)*t2 - 320*Power(t1,3)*t2 + 
            368*Power(t1,4)*t2 - 23*Power(t1,5)*t2 - 66*Power(t2,2) - 
            t1*Power(t2,2) + 335*Power(t1,2)*Power(t2,2) - 
            350*Power(t1,3)*Power(t2,2) - 119*Power(t1,4)*Power(t2,2) + 
            5*Power(t1,5)*Power(t2,2) + 257*Power(t2,3) - 
            892*t1*Power(t2,3) - 174*Power(t1,2)*Power(t2,3) + 
            697*Power(t1,3)*Power(t2,3) - 50*Power(t1,4)*Power(t2,3) + 
            373*Power(t2,4) + 443*t1*Power(t2,4) - 
            1226*Power(t1,2)*Power(t2,4) + 45*Power(t1,3)*Power(t2,4) - 
            178*Power(t2,5) + 898*t1*Power(t2,5) + 
            115*Power(t1,2)*Power(t2,5) - 236*Power(t2,6) - 
            190*t1*Power(t2,6) + 75*Power(t2,7) + 
            Power(s1,6)*(-28*Power(s2,3) - 35*t1 + 
               s2*(-15 + 112*t1 - 70*t2) + 20*t2 + 
               Power(s2,2)*(42 - 84*t1 + 56*t2)) + 
            Power(s2,6)*(-30*Power(t1,3) + 15*Power(t1,2)*(4 + 3*t2) + 
               2*t2*(10 + 13*t2 + 3*Power(t2,2)) - 
               t1*(30 + 65*t2 + 21*Power(t2,2))) + 
            Power(s2,5)*(-52 - 12*Power(t1,4) - 357*t2 - 
               622*Power(t2,2) - 284*Power(t2,3) + 27*Power(t2,4) + 
               4*Power(t1,3)*(109 + 59*t2) - 
               Power(t1,2)*(748 + 1535*t2 + 373*Power(t2,2)) + 
               t1*(391 + 1561*t2 + 1353*Power(t2,2) + 122*Power(t2,3))) \
+ Power(s2,4)*(103 + 679*t2 + 1621*Power(t2,2) + 2271*Power(t2,3) + 
               1326*Power(t2,4) + 126*Power(t2,5) + 
               3*Power(t1,4)*(-70 + 9*t2) - 
               Power(t1,3)*(498 + 881*t2 + 557*Power(t2,2)) + 
               Power(t1,2)*(1189 + 3905*t2 + 4407*Power(t2,2) + 
                  1145*Power(t2,3)) - 
               t1*(659 + 3555*t2 + 6166*Power(t2,2) + 
                  4635*Power(t2,3) + 741*Power(t2,4))) + 
            Power(s2,3)*(57 - 9*Power(t1,5) + 58*t2 + 26*Power(t2,2) - 
               1580*Power(t2,3) - 2778*Power(t2,4) - 1530*Power(t2,5) - 
               193*Power(t2,6) + 
               Power(t1,4)*(1228 + 573*t2 + 23*Power(t2,2)) + 
               Power(t1,3)*(-1784 - 3043*t2 - 133*Power(t2,2) + 
                  264*Power(t2,3)) + 
               Power(t1,2)*(937 + 2649*t2 - 1497*Power(t2,2) - 
                  3276*Power(t2,3) - 790*Power(t2,4)) + 
               t1*(-279 - 187*t2 + 2574*Power(t2,2) + 
                  6391*Power(t2,3) + 4375*Power(t2,4) + 696*Power(t2,5))\
) - Power(s2,2)*(246 + 527*t2 + 860*Power(t2,2) + 705*Power(t2,3) - 
               529*Power(t2,4) - 1198*Power(t2,5) - 559*Power(t2,6) - 
               40*Power(t2,7) + Power(t1,5)*(178 + 27*t2) + 
               Power(t1,4)*(992 + 712*t2 + 294*Power(t2,2) + 
                  30*Power(t2,3)) - 
               2*Power(t1,3)*
                (1078 + 2378*t2 + 1642*Power(t2,2) + 211*Power(t2,3) + 
                  25*Power(t2,4)) + 
               Power(t1,2)*(1910 + 6627*t2 + 6470*Power(t2,2) + 
                  1815*Power(t2,3) - 705*Power(t2,4) - 30*Power(t2,5)) \
+ t1*(-1020 - 2887*t2 - 2996*Power(t2,2) - 1057*Power(t2,3) + 
                  1777*Power(t2,4) + 1365*Power(t2,5) + 90*Power(t2,6))) \
+ s2*(177 + 6*Power(t1,6) + 256*t2 - 148*Power(t2,2) - 94*Power(t2,3) + 
               447*Power(t2,4) + 231*Power(t2,5) - 273*Power(t2,6) - 
               90*Power(t2,7) + 
               Power(t1,5)*(247 + 61*t2 + 20*Power(t2,2)) + 
               Power(t1,4)*(32 - 299*t2 + 112*Power(t2,2) + 
                  5*Power(t2,3)) - 
               Power(t1,3)*(506 + 702*t2 + 1697*Power(t2,2) + 
                  622*Power(t2,3) + 45*Power(t2,4)) + 
               Power(t1,2)*(735 + 2085*t2 + 3141*Power(t2,2) + 
                  3198*Power(t2,3) + 361*Power(t2,4) - 95*Power(t2,5)) \
+ t1*(-616 - 1276*t2 - 549*Power(t2,2) - 1630*Power(t2,3) - 
                  1680*Power(t2,4) + 355*Power(t2,5) + 205*Power(t2,6))) \
+ Power(s1,5)*(-25 - 133*Power(t1,2) + 33*t2 - 140*Power(t2,2) + 
               Power(s2,3)*(146 - 392*t1 + 294*t2) + 
               t1*(-32 + 310*t2) - 
               Power(s2,2)*(225 + 168*Power(t1,2) + 564*t2 + 
                  350*Power(t2,2) - 28*t1*(27 + 22*t2)) + 
               s2*(119 + 364*Power(t1,2) + 181*t2 + 480*Power(t2,2) - 
                  6*t1*(45 + 161*t2))) + 
            Power(s1,4)*(47 + 17*Power(s2,5) - 203*Power(t1,3) + 
               23*t2 - 300*Power(t2,2) + 435*Power(t2,3) + 
               Power(s2,4)*(2 - 294*t1 + 52*t2) + 
               Power(t1,2)*(-503 + 879*t2) + 
               t1*(112 + 696*t2 - 1150*Power(t2,2)) - 
               Power(s2,3)*(183 + 812*Power(t1,2) + 1422*t2 + 
                  1136*Power(t2,2) - t1*(1972 + 2345*t2)) + 
               Power(s2,2)*(252 - 84*Power(t1,3) + 1692*t2 + 
                  2537*Power(t2,2) + 880*Power(t2,3) + 
                  21*Power(t1,2)*(79 + 40*t2) - 
                  t1*(1895 + 5108*t2 + 1750*Power(t2,2))) + 
               s2*(-179 + 210*Power(t1,3) - 348*t2 - 796*Power(t2,2) - 
                  1310*Power(t2,3) - 21*Power(t1,2)*(14 + 81*t2) + 
                  t1*(80 + 1661*t2 + 2941*Power(t2,2)))) - 
            Power(s1,3)*(-124 + Power(s2,6) - 105*Power(t1,4) + 
               Power(t1,3)*(412 - 424*t2) + 328*t2 - 147*Power(t2,2) - 
               938*Power(t2,3) + 740*Power(t2,4) + 
               Power(s2,5)*(91 + 2*t1 + 83*t2) + 
               Power(t1,2)*(-693 - 2571*t2 + 1898*Power(t2,2)) + 
               t1*(302 + 581*t2 + 2744*Power(t2,2) - 
                  2130*Power(t2,3)) + 
               Power(s2,4)*(-56 + 812*Power(t1,2) + 590*t2 + 
                  292*Power(t2,2) - t1*(1557 + 1540*t2)) + 
               Power(s2,3)*(1 + 504*Power(t1,3) - 2038*t2 - 
                  5061*Power(t2,2) - 2117*Power(t2,3) - 
                  Power(t1,2)*(3538 + 3465*t2) + 
                  2*t1*(1812 + 5359*t2 + 2732*Power(t2,2))) + 
               s2*(215 + 224*Power(t1,4) - 299*t2 - 74*Power(t2,2) - 
                  1725*Power(t2,3) - 1840*Power(t2,4) + 
                  Power(t1,3)*(-76 + 161*t2) + 
                  Power(t1,2)*(1810 - 493*t2 - 2834*Power(t2,2)) + 
                  t1*(-1241 - 351*t2 + 3495*Power(t2,2) + 
                     4352*Power(t2,3))) + 
               Power(s2,2)*(-28*Power(t1,3)*(35 + 8*t2) + 
                  Power(t1,2)*(1532 + 6787*t2 + 1526*Power(t2,2)) - 
                  t1*(938 + 9559*t2 + 12299*Power(t2,2) + 
                     2484*Power(t2,3)) + 
                  2*(-87 + 536*t2 + 2572*Power(t2,2) + 
                     2603*Power(t2,3) + 570*Power(t2,4)))) + 
            Power(s1,2)*(-311 + 137*t2 + 858*Power(t2,2) - 
               441*Power(t2,3) - 1344*Power(t2,4) + 710*Power(t2,5) + 
               Power(s2,6)*(-19 + 24*t1 + 8*t2) - 
               5*Power(t1,4)*(94 + 43*t2) + 
               Power(t1,3)*(50 + 1987*t2 - 236*Power(t2,2)) + 
               Power(t1,2)*(-120 - 2104*t2 - 4907*Power(t2,2) + 
                  1806*Power(t2,3)) + 
               t1*(740 - 508*t2 + 1409*Power(t2,2) + 
                  4426*Power(t2,3) - 2065*Power(t2,4)) + 
               Power(s2,5)*(76 - 235*Power(t1,2) + 78*t2 + 
                  142*Power(t2,2) + t1*(252 + 131*t2)) + 
               Power(s2,4)*(54 - 574*Power(t1,3) + 1045*t2 + 
                  2500*Power(t2,2) + 554*Power(t2,3) + 
                  Power(t1,2)*(3073 + 2783*t2) - 
                  t1*(2543 + 7234*t2 + 2959*Power(t2,2))) + 
               Power(s2,3)*(174 - 56*Power(t1,4) - 777*t2 - 
                  6581*Power(t2,2) - 7970*Power(t2,3) - 
                  2055*Power(t2,4) + Power(t1,3)*(1931 + 1379*t2) - 
                  Power(t1,2)*(3969 + 12239*t2 + 5313*Power(t2,2)) + 
                  t1*(2303 + 15388*t2 + 20240*Power(t2,2) + 
                     6157*Power(t2,3))) - 
               Power(s2,2)*(759 + 937*t2 - 2079*Power(t2,2) - 
                  7310*Power(t2,3) - 5406*Power(t2,4) - 
                  800*Power(t2,5) + 7*Power(t1,4)*(101 + 8*t2) + 
                  Power(t1,3)*(-829 + 519*t2 + 126*Power(t2,2)) - 
                  Power(t1,2)*
                   (-2607 + 2384*t2 + 8888*Power(t2,2) + 
                     1234*Power(t2,3)) + 
                  t1*(-2150 + 3713*t2 + 14978*Power(t2,2) + 
                     13663*Power(t2,3) + 1852*Power(t2,4))) + 
               s2*(776 + 28*Power(t1,5) + 142*t2 + 412*Power(t2,2) + 
                  651*Power(t2,3) - 1984*Power(t2,4) - 
                  1410*Power(t2,5) + 35*Power(t1,4)*(29 + 11*t2) - 
                  Power(t1,3)*(1765 + 2204*t2 + 277*Power(t2,2)) + 
                  Power(t1,2)*
                   (3375 + 7840*t2 + 522*Power(t2,2) - 
                     2116*Power(t2,3)) + 
                  t1*(-2715 - 3037*t2 - 3029*Power(t2,2) + 
                     3442*Power(t2,3) + 3390*Power(t2,4)))) - 
            s1*(-197 - 216*t2 + 478*Power(t2,2) + 950*Power(t2,3) - 
               474*Power(t2,4) - 909*Power(t2,5) + 360*Power(t2,6) + 
               Power(t1,5)*(-68 + 8*t2) - 
               Power(t1,4)*(29 + 336*t2 + 167*Power(t2,2)) + 
               Power(t1,3)*(-131 - 163*t2 + 2134*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(t1,2)*(-229 - 79*t2 - 1473*Power(t2,2) - 
                  4065*Power(t2,3) + 769*Power(t2,4)) + 
               t1*(604 + 524*t2 - 1644*Power(t2,2) + 1383*Power(t2,3) + 
                  3244*Power(t2,4) - 1000*Power(t2,5)) + 
               Power(s2,6)*(-20 + 5*Power(t1,2) + 7*t2 + 
                  13*Power(t2,2) + 3*t1*(5 + t2)) + 
               Power(s2,5)*(-44 + 228*Power(t1,3) - 326*t2 - 
                  282*Power(t2,2) + 103*Power(t2,3) - 
                  Power(t1,2)*(821 + 618*t2) + 
                  t1*(587 + 1350*t2 + 251*Power(t2,2))) + 
               Power(s2,4)*(192 + 56*Power(t1,4) + 1179*t2 + 
                  3427*Power(t2,2) + 3238*Power(t2,3) + 
                  440*Power(t2,4) - 2*Power(t1,3)*(864 + 577*t2) + 
                  Power(t1,2)*(3300 + 8260*t2 + 3126*Power(t2,2)) - 
                  t1*(1870 + 8745*t2 + 10452*Power(t2,2) + 
                     2454*Power(t2,3))) + 
               Power(s2,3)*(197 + Power(t1,4)*(742 - 21*t2) + 199*t2 - 
                  2530*Power(t2,2) - 7504*Power(t2,3) - 
                  5715*Power(t2,4) - 1001*Power(t2,5) + 
                  2*Power(t1,3)*(14 + 625*t2 + 565*Power(t2,2)) - 
                  Power(t1,2)*
                   (371 + 7307*t2 + 11761*Power(t2,2) + 
                     3450*Power(t2,3)) + 
                  t1*(-96 + 6805*t2 + 18270*Power(t2,2) + 
                     15869*Power(t2,3) + 3342*Power(t2,4))) - 
               Power(s2,2)*(780 + 14*Power(t1,5) + 1379*t2 + 
                  1480*Power(t2,2) - 1788*Power(t2,3) - 
                  4831*Power(t2,4) - 2774*Power(t2,5) - 
                  286*Power(t2,6) + 
                  Power(t1,4)*(2193 + 941*t2 + 84*Power(t2,2)) - 
                  Power(t1,3)*
                   (3690 + 6321*t2 + 797*Power(t2,2) + 64*Power(t2,3)) \
+ Power(t1,2)*(4327 + 9813*t2 + 1499*Power(t2,2) - 4465*Power(t2,3) - 
                     410*Power(t2,4)) + 
                  t1*(-2924 - 3824*t2 + 1236*Power(t2,2) + 
                     9091*Power(t2,3) + 7081*Power(t2,4) + 
                     676*Power(t2,5))) + 
               s2*(665 + 464*t2 - 98*Power(t2,2) + 979*Power(t2,3) + 
                  727*Power(t2,4) - 1162*Power(t2,5) - 560*Power(t2,6) + 
                  Power(t1,5)*(186 + 49*t2) + 
                  Power(t1,4)*(1063 + 624*t2 + 168*Power(t2,2)) - 
                  Power(t1,3)*
                   (1637 + 4686*t2 + 2423*Power(t2,2) + 273*Power(t2,3)) \
+ Power(t1,2)*(2223 + 7995*t2 + 9162*Power(t2,2) + 1082*Power(t2,3) - 
                     714*Power(t2,4)) + 
                  t1*(-2150 - 3078*t2 - 3683*Power(t2,2) - 
                     4278*Power(t2,3) + 1693*Power(t2,4) + 
                     1330*Power(t2,5))))) + 
         Power(s,5)*(223 - 1016*t1 + 1674*Power(t1,2) - 
            1355*Power(t1,3) + 781*Power(t1,4) - 325*Power(t1,5) + 
            10*Power(t1,6) + 2*Power(t1,7) + 266*t2 - 764*t1*t2 + 
            1483*Power(t1,2)*t2 - 1899*Power(t1,3)*t2 + 
            1226*Power(t1,4)*t2 - 334*Power(t1,5)*t2 + 
            47*Power(t1,6)*t2 - 513*Power(t2,2) + 2606*t1*Power(t2,2) - 
            1335*Power(t1,2)*Power(t2,2) - 
            2288*Power(t1,3)*Power(t2,2) + 
            1638*Power(t1,4)*Power(t2,2) - 206*Power(t1,5)*Power(t2,2) - 
            1370*Power(t2,3) + 2812*t1*Power(t2,3) + 
            757*Power(t1,2)*Power(t2,3) - 2319*Power(t1,3)*Power(t2,3) + 
            150*Power(t1,4)*Power(t2,3) + 10*Power(t1,5)*Power(t2,3) - 
            697*Power(t2,4) - 961*t1*Power(t2,4) + 
            1426*Power(t1,2)*Power(t2,4) + 545*Power(t1,3)*Power(t2,4) - 
            60*Power(t1,4)*Power(t2,4) + 555*Power(t2,5) - 
            529*t1*Power(t2,5) - 1250*Power(t1,2)*Power(t2,5) + 
            77*Power(t1,3)*Power(t2,5) + 108*Power(t2,6) + 
            999*t1*Power(t2,6) + 29*Power(t1,2)*Power(t2,6) - 
            287*Power(t2,7) - 99*t1*Power(t2,7) + 43*Power(t2,8) + 
            5*Power(s2,7)*(5*Power(t1,3) - 3*t2*Power(1 + t2,2) - 
               2*Power(t1,2)*(5 + 3*t2) + t1*(5 + 9*t2 + 4*Power(t2,2))) \
+ Power(s2,6)*(78 + 8*Power(t1,4) + 301*t2 + 265*Power(t2,2) - 
               62*Power(t2,3) - 110*Power(t2,4) - 
               Power(t1,3)*(387 + 244*t2) + 
               24*Power(t1,2)*(31 + 53*t2 + 11*Power(t2,2)) + 
               t1*(-449 - 1284*t2 - 748*Power(t2,2) + 82*Power(t2,3))) + 
            Power(s2,5)*(-225 + Power(t1,4)*(295 - 46*t2) - 953*t2 - 
               1456*Power(t2,2) - 1692*Power(t2,3) - 857*Power(t2,4) + 
               35*Power(t2,5) + 
               Power(t1,3)*(315 + 1449*t2 + 983*Power(t2,2)) - 
               Power(t1,2)*(1361 + 4896*t2 + 6361*Power(t2,2) + 
                  1724*Power(t2,3)) + 
               t1*(1012 + 4391*t2 + 7438*Power(t2,2) + 
                  5441*Power(t2,3) + 752*Power(t2,4))) + 
            Power(s2,4)*(-94 + 20*Power(t1,5) + 61*t2 + 
               321*Power(t2,2) + 2341*Power(t2,3) + 4366*Power(t2,4) + 
               2658*Power(t2,5) + 365*Power(t2,6) - 
               2*Power(t1,4)*(1094 + 690*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(4271 + 5714*t2 - 6*Power(t2,2) - 
                  1117*Power(t2,3)) + 
               Power(t1,2)*(-2982 - 5613*t2 + 4323*Power(t2,2) + 
                  8498*Power(t2,3) + 2603*Power(t2,4)) - 
               t1*(-883 - 1013*t2 + 6064*Power(t2,2) + 
                  13750*Power(t2,3) + 9797*Power(t2,4) + 
                  1845*Power(t2,5))) + 
            Power(s2,3)*(561 + 1091*t2 + 768*Power(t2,2) + 
               944*Power(t2,3) - 725*Power(t2,4) - 2820*Power(t2,5) - 
               1856*Power(t2,6) - 275*Power(t2,7) + 
               Power(t1,5)*(617 + 8*t2) + 
               Power(t1,4)*(1433 + 2882*t2 + 1723*Power(t2,2) + 
                  90*Power(t2,3)) + 
               Power(t1,3)*(-5207 - 13292*t2 - 13582*Power(t2,2) - 
                  2600*Power(t2,3) + 215*Power(t2,4)) + 
               Power(t1,2)*(5418 + 18012*t2 + 21202*Power(t2,2) + 
                  8737*Power(t2,3) - 2438*Power(t2,4) - 975*Power(t2,5)\
) + t1*(-2702 - 8231*t2 - 7681*Power(t2,2) - 2623*Power(t2,3) + 
                  4789*Power(t2,4) + 5163*Power(t2,5) + 945*Power(t2,6))\
) + Power(s2,2)*(-146 - 16*Power(t1,6) - 472*t2 + 495*Power(t2,2) + 
               659*Power(t2,3) - 575*Power(t2,4) - 283*Power(t2,5) + 
               735*Power(t2,6) + 455*Power(t2,7) + 30*Power(t2,8) - 
               Power(t1,5)*(1660 + 687*t2 + 110*Power(t2,2)) + 
               Power(t1,4)*(3124 + 3562*t2 - 169*Power(t2,2) - 
                  335*Power(t2,3) - 40*Power(t2,4)) + 
               Power(t1,3)*(-2249 - 2159*t2 + 6627*Power(t2,2) + 
                  5660*Power(t2,3) + 895*Power(t2,4) + 90*Power(t2,5)) \
- Power(t1,2)*(-520 + 4561*t2 + 14533*Power(t2,2) + 
                  15860*Power(t2,3) + 5605*Power(t2,4) - 
                  110*Power(t2,5) + 30*Power(t2,6)) + 
               t1*(337 + 4012*t2 + 5767*Power(t2,2) + 
                  6614*Power(t2,3) + 5346*Power(t2,4) + 
                  82*Power(t2,5) - 1015*Power(t2,6) - 50*Power(t2,7))) + 
            s2*(-402 - 181*t2 + 899*Power(t2,2) + 818*Power(t2,3) + 
               44*Power(t2,4) + 483*Power(t2,5) + 573*Power(t2,6) - 
               112*Power(t2,7) - 58*Power(t2,8) + 
               Power(t1,6)*(79 + 24*t2) + 
               Power(t1,5)*(866 + 302*t2 + 80*Power(t2,2) + 
                  30*Power(t2,3)) - 
               Power(t1,4)*(2410 + 3342*t2 + 1301*Power(t2,2) - 
                  165*Power(t2,3) + 45*Power(t2,4)) + 
               Power(t1,3)*(3595 + 5513*t2 + 2030*Power(t2,2) - 
                  1681*Power(t2,3) - 970*Power(t2,4) + 13*Power(t2,5)) \
+ Power(t1,2)*(-3562 - 1950*t2 + 4620*Power(t2,2) + 5813*Power(t2,3) + 
                  4877*Power(t2,4) + 893*Power(t2,5) - 69*Power(t2,6)) \
+ t1*(1870 - 341*t2 - 5680*Power(t2,2) - 4363*Power(t2,3) - 
                  3162*Power(t2,4) - 2849*Power(t2,5) - 
                  80*Power(t2,6) + 129*Power(t2,7))) + 
            Power(s1,7)*(56*Power(s2,3) + 5*(7*t1 - 3*t2) + 
               s2*(20 - 140*t1 + 70*t2) + 
               14*Power(s2,2)*(9*t1 - 5*(1 + t2))) + 
            Power(s1,6)*(20 + 77*Power(t1,2) + 
               Power(s2,3)*(-239 + 784*t1 - 630*t2) + 
               t1*(75 - 264*t2) - 57*t2 + 107*Power(t2,2) + 
               2*Power(s2,2)*
                (141 + 126*Power(t1,2) + 432*t2 + 245*Power(t2,2) - 
                  7*t1*(83 + 70*t2)) - 
               s2*(112 + 350*Power(t1,2) + 150*t2 + 507*Power(t2,2) - 
                  2*t1*(124 + 567*t2))) + 
            Power(s1,5)*(30 - 102*Power(s2,5) + 175*Power(t1,3) + 
               Power(t1,2)*(841 - 654*t2) + 
               Power(s2,4)*(83 + 910*t1 - 207*t2) - 74*t2 + 
               454*Power(t2,2) - 353*Power(t2,3) + 
               2*t1*(-136 - 546*t2 + 487*Power(t2,2)) + 
               Power(s2,3)*(159 + 1582*Power(t1,2) + 2627*t2 + 
                  2700*Power(t2,2) - 5*t1*(776 + 1057*t2)) + 
               s2*(-40 - 84*Power(t1,3) + 295*t2 + 435*Power(t2,2) + 
                  1543*Power(t2,3) + 7*Power(t1,2)*(-85 + 241*t2) + 
                  t1*(796 - 998*t2 - 3604*Power(t2,2))) + 
               Power(s2,2)*(-40 + 126*Power(t1,3) - 2249*t2 - 
                  3900*Power(t2,2) - 1430*Power(t2,3) - 
                  7*Power(t1,2)*(269 + 198*t2) + 
                  t1*(2118 + 7877*t2 + 3080*Power(t2,2)))) + 
            Power(s1,4)*(-342 - 47*Power(s2,6) - 119*Power(t1,4) + 
               Power(t1,3)*(501 - 479*t2) + 190*t2 + 310*Power(t2,2) - 
               1533*Power(t2,3) + 685*Power(t2,4) + 
               9*Power(s2,5)*(61 + 20*t1 + 60*t2) + 
               Power(t1,2)*(-558 - 4469*t2 + 1825*Power(t2,2)) + 
               t1*(671 + 601*t2 + 4553*Power(t2,2) - 
                  2055*Power(t2,3)) + 
               Power(s2,4)*(2268*Power(t1,2) - 7*t1*(687 + 781*t2) + 
                  2*(-300 + 541*t2 + 630*Power(t2,2))) + 
               Power(s2,3)*(561 + 966*Power(t1,3) - 2993*t2 - 
                  10358*Power(t2,2) - 5895*Power(t2,3) - 
                  Power(t1,2)*(5674 + 7959*t2) + 
                  t1*(6560 + 22809*t2 + 14423*Power(t2,2))) + 
               Power(s2,2)*(-1067 + 299*t2 + 6950*Power(t2,2) + 
                  8815*Power(t2,3) + 2250*Power(t2,4) - 
                  14*Power(t1,3)*(19 + 26*t2) + 
                  Power(t1,2)*(-2453 + 7976*t2 + 2926*Power(t2,2)) - 
                  t1*(-2451 + 10386*t2 + 20751*Power(t2,2) + 
                     5090*Power(t2,3))) + 
               s2*(951 + 322*Power(t1,4) + 68*t2 + 523*Power(t2,2) - 
                  752*Power(t2,3) - 2560*Power(t2,4) - 
                  Power(t1,3)*(1079 + 231*t2) + 
                  Power(t1,2)*(5297 + 3950*t2 - 3151*Power(t2,2)) + 
                  t1*(-4277 - 5265*t2 + 1230*Power(t2,2) + 
                     6005*Power(t2,3)))) + 
            Power(s1,3)*(316 + 5*Power(s2,7) + 876*t2 - 
               1075*Power(t2,2) - 794*Power(t2,3) + 2732*Power(t2,4) - 
               825*Power(t2,5) + Power(s2,6)*(276 - 133*t1 + 236*t2) + 
               Power(t1,4)*(555 + 331*t2) + 
               Power(t1,3)*(1189 - 2601*t2 + 372*Power(t2,2)) + 
               Power(t1,2)*(-1543 + 1397*t2 + 9665*Power(t2,2) - 
                  2350*Power(t2,3)) + 
               t1*(-516 - 693*t2 + 4*Power(t2,2) - 8856*Power(t2,3) + 
                  2535*Power(t2,4)) + 
               Power(s2,5)*(-890 + 1158*Power(t1,2) - 1582*t2 - 
                  1043*Power(t2,2) - 2*t1*(837 + 578*t2)) + 
               Power(s2,4)*(776 + 1526*Power(t1,3) - 470*t2 - 
                  6627*Power(t2,2) - 2903*Power(t2,3) - 
                  Power(t1,2)*(8429 + 9719*t2) + 
                  t1*(8318 + 24592*t2 + 12927*Power(t2,2))) + 
               Power(s2,3)*(-1505 + 112*Power(t1,4) - 822*t2 + 
                  11439*Power(t2,2) + 19558*Power(t2,3) + 
                  7220*Power(t2,4) - Power(t1,3)*(1240 + 3297*t2) + 
                  Power(t1,2)*(818 + 22163*t2 + 15372*Power(t2,2)) - 
                  t1*(1057 + 30276*t2 + 50616*Power(t2,2) + 
                     20344*Power(t2,3))) + 
               Power(s2,2)*(2509 + 3119*t2 - 157*Power(t2,2) - 
                  10911*Power(t2,3) - 11090*Power(t2,4) - 
                  2050*Power(t2,5) + 7*Power(t1,4)*(231 + 16*t2) + 
                  Power(t1,3)*(-6419 - 2367*t2 + 210*Power(t2,2)) + 
                  Power(t1,2)*
                   (15875 + 15646*t2 - 12074*Power(t2,2) - 
                     2952*Power(t2,3)) + 
                  t1*(-11351 - 9957*t2 + 17165*Power(t2,2) + 
                     27626*Power(t2,3) + 4750*Power(t2,4))) - 
               s2*(1637 + 56*Power(t1,5) + 1410*t2 + 351*Power(t2,2) + 
                  2792*Power(t2,3) - 958*Power(t2,4) - 
                  2500*Power(t2,5) + 7*Power(t1,4)*(199 + 101*t2) - 
                  Power(t1,3)*(2711 + 5218*t2 + 990*Power(t2,2)) + 
                  Power(t1,2)*
                   (6356 + 22220*t2 + 8807*Power(t2,2) - 
                     2900*Power(t2,3)) + 
                  t1*(-6328 - 13813*t2 - 13748*Power(t2,2) + 
                     102*Power(t2,3) + 5760*Power(t2,4)))) + 
            Power(s1,2)*(401 - 1255*t2 - 1647*Power(t2,2) + 
               2015*Power(t2,3) + 962*Power(t2,4) - 2683*Power(t2,5) + 
               605*Power(t2,6) - 5*Power(s2,7)*(-5 + 7*t1 + 5*t2) + 
               Power(t1,5)*(-230 + 27*t2) + 
               Power(t1,4)*(641 - 535*t2 - 397*Power(t2,2)) + 
               Power(t1,3)*(-2282 - 4488*t2 + 4184*Power(t2,2) + 
                  70*Power(t2,3)) + 
               Power(t1,2)*(3102 + 2311*t2 + 372*Power(t2,2) - 
                  10537*Power(t2,3) + 1485*Power(t2,4)) - 
               t1*(1578 - 3815*t2 + 1308*Power(t2,2) + 
                  1252*Power(t2,3) - 9045*Power(t2,4) + 
                  1790*Power(t2,5)) + 
               Power(s2,6)*(-308 + 163*Power(t1,2) - 687*t2 - 
                  441*Power(t2,2) + t1*(88 + 298*t2)) + 
               Power(s2,5)*(245 + 948*Power(t1,3) + 1189*t2 + 
                  665*Power(t2,2) + 909*Power(t2,3) - 
                  Power(t1,2)*(4523 + 3969*t2) + 
                  t1*(3397 + 8130*t2 + 2524*Power(t2,2))) + 
               Power(s2,4)*(-381 + 168*Power(t1,4) - 322*t2 + 
                  7485*Power(t2,2) + 12334*Power(t2,3) + 
                  3219*Power(t2,4) - 3*Power(t1,3)*(1134 + 1447*t2) + 
                  Power(t1,2)*(7867 + 27853*t2 + 15318*Power(t2,2)) - 
                  t1*(5004 + 32585*t2 + 45004*Power(t2,2) + 
                     14938*Power(t2,3))) + 
               Power(s2,3)*(2367 + Power(t1,4)*(2793 - 63*t2) + 
                  3288*t2 - 1065*Power(t2,2) - 17355*Power(t2,3) - 
                  19211*Power(t2,4) - 5016*Power(t2,5) + 
                  3*Power(t1,3)*(-2831 - 801*t2 + 1287*Power(t2,2)) + 
                  Power(t1,2)*
                   (13540 + 5444*t2 - 29013*Power(t2,2) - 
                     14170*Power(t2,3)) + 
                  t1*(-8367 + 5296*t2 + 45193*Power(t2,2) + 
                     53488*Power(t2,3) + 15584*Power(t2,4))) - 
               Power(s2,2)*(2743 + 42*Power(t1,5) + 3282*t2 + 
                  3979*Power(t2,2) + 706*Power(t2,3) - 
                  9293*Power(t2,4) - 7890*Power(t2,5) - 
                  1070*Power(t2,6) + 
                  Power(t1,4)*(4915 + 3210*t2 + 252*Power(t2,2)) - 
                  Power(t1,3)*
                   (11403 + 23659*t2 + 6043*Power(t2,2) + 
                     258*Power(t2,3)) + 
                  Power(t1,2)*
                   (17389 + 53799*t2 + 30045*Power(t2,2) - 
                     7862*Power(t2,3) - 1380*Power(t2,4)) + 
                  t1*(-12393 - 27723*t2 - 19313*Power(t2,2) + 
                     11562*Power(t2,3) + 19712*Power(t2,4) + 
                     2456*Power(t2,5))) + 
               s2*(523 + 2127*t2 + 98*Power(t2,2) + 1141*Power(t2,3) + 
                  3968*Power(t2,4) - 882*Power(t2,5) - 
                  1435*Power(t2,6) + 3*Power(t1,5)*(197 + 49*t2) + 
                  2*Power(t1,4)*(149 + 596*t2 + 210*Power(t2,2)) - 
                  Power(t1,3)*
                   (-2090 + 6578*t2 + 7395*Power(t2,2) + 
                     938*Power(t2,3)) + 
                  Power(t1,2)*
                   (-1720 + 20189*t2 + 32085*Power(t2,2) + 
                     9037*Power(t2,3) - 1414*Power(t2,4)) + 
                  t1*(-1631 - 14816*t2 - 17609*Power(t2,2) - 
                     17734*Power(t2,3) - 814*Power(t2,4) + 
                     3220*Power(t2,5)))) + 
            s1*(-648 - 40*Power(t1,6) + 65*t2 + 2341*Power(t2,2) + 
               1810*Power(t2,3) - 1715*Power(t2,4) - 532*Power(t2,5) + 
               1374*Power(t2,6) - 247*Power(t2,7) + 
               Power(t1,5)*(610 + 247*t2 - 35*Power(t2,2)) + 
               Power(t1,4)*(-871 - 2597*t2 + 22*Power(t2,2) + 
                  245*Power(t2,3)) + 
               Power(t1,3)*(1898 + 4674*t2 + 5416*Power(t2,2) - 
                  2629*Power(t2,3) - 215*Power(t2,4)) - 
               Power(t1,2)*(3555 + 784*t2 + 1559*Power(t2,2) + 
                  2637*Power(t2,3) - 5750*Power(t2,4) + 412*Power(t2,5)) \
+ t1*(2599 - 1579*t2 - 6095*Power(t2,2) + 2291*Power(t2,3) + 
                  1448*Power(t2,4) - 4724*Power(t2,5) + 664*Power(t2,6)) \
- 5*Power(s2,7)*(6 + 3*Power(t1,2) - t2 - 7*Power(t2,2) - 
                  3*t1*(3 + t2)) + 
               Power(s2,6)*(16 + 257*Power(t1,3) + 170*t2 + 
                  479*Power(t2,2) + 362*Power(t2,3) - 
                  3*Power(t1,2)*(243 + 154*t2) + 
                  t1*(429 + 550*t2 - 247*Power(t2,2))) + 
               Power(s2,5)*(369 + 72*Power(t1,4) + 841*t2 + 
                  1442*Power(t2,2) + 1225*Power(t2,3) - 
                  339*Power(t2,4) - Power(t1,3)*(2341 + 1899*t2) + 
                  Power(t1,2)*(4671 + 11467*t2 + 4535*Power(t2,2)) - 
                  2*t1*(1412 + 5486*t2 + 5991*Power(t2,2) + 
                     1150*Power(t2,3))) - 
               Power(s2,4)*(-282 + 79*t2 + 2956*Power(t2,2) + 
                  10781*Power(t2,3) + 9530*Power(t2,4) + 
                  1734*Power(t2,5) + Power(t1,4)*(-1639 + 162*t2) + 
                  Power(t1,3)*(1846 - 2634*t2 - 3939*Power(t2,2)) + 
                  Power(t1,2)*
                   (-823 + 13701*t2 + 27727*Power(t2,2) + 
                     10470*Power(t2,3)) - 
                  t1*(-423 + 13222*t2 + 38066*Power(t2,2) + 
                     35018*Power(t2,3) + 8413*Power(t2,4))) + 
               Power(s2,3)*(-1984 + 54*Power(t1,5) - 2637*t2 - 
                  2811*Power(t2,2) + 2051*Power(t2,3) + 
                  11570*Power(t2,4) + 9479*Power(t2,5) + 
                  1840*Power(t2,6) - 
                  3*Power(t1,4)*(2085 + 1440*t2 + 46*Power(t2,2)) + 
                  Power(t1,3)*
                   (13723 + 25757*t2 + 6054*Power(t2,2) - 
                     1745*Power(t2,3)) + 
                  2*Power(t1,2)*
                   (-7493 - 18723*t2 - 7789*Power(t2,2) + 
                     7481*Power(t2,3) + 3075*Power(t2,4)) - 
                  t1*(-8618 - 14736*t2 + 885*Power(t2,2) + 
                     26266*Power(t2,3) + 26964*Power(t2,4) + 
                     6107*Power(t2,5))) + 
               s2*(703 - 36*Power(t1,6) - 747*t2 - 1351*Power(t2,2) + 
                  317*Power(t2,3) - 1301*Power(t2,4) - 
                  2455*Power(t2,5) + 483*Power(t2,6) + 
                  447*Power(t2,7) - 
                  Power(t1,5)*(1459 + 450*t2 + 120*Power(t2,2)) + 
                  Power(t1,4)*
                   (3087 + 3216*t2 - 261*Power(t2,2) + 10*Power(t2,3)) \
+ Power(t1,3)*(-6356 - 6262*t2 + 5233*Power(t2,2) + 4226*Power(t2,3) + 
                     250*Power(t2,4)) + 
                  Power(t1,2)*
                   (7224 - 3207*t2 - 18701*Power(t2,2) - 
                     20039*Power(t2,3) - 4478*Power(t2,4) + 
                     397*Power(t2,5)) + 
                  t1*(-3296 + 6759*t2 + 12524*Power(t2,2) + 
                     11235*Power(t2,3) + 11304*Power(t2,4) + 
                     516*Power(t2,5) - 984*Power(t2,6))) + 
               Power(s2,2)*(1266 + 1714*t2 + 306*Power(t2,2) + 
                  2502*Power(t2,3) + 887*Power(t2,4) - 
                  4100*Power(t2,5) - 2964*Power(t2,6) - 
                  290*Power(t2,7) + Power(t1,5)*(1273 + 162*t2) + 
                  Power(t1,4)*
                   (650 + 3025*t2 + 1961*Power(t2,2) + 180*Power(t2,3)) \
- Power(t1,3)*(2713 + 19490*t2 + 21997*Power(t2,2) + 4305*Power(t2,3) + 
                     320*Power(t2,4)) + 
                  Power(t1,2)*
                   (4799 + 35975*t2 + 53041*Power(t2,2) + 
                     22457*Power(t2,3) - 1991*Power(t2,4) - 
                     190*Power(t2,5)) + 
                  t1*(-4700 - 18440*t2 - 23281*Power(t2,2) - 
                     17153*Power(t2,3) + 2583*Power(t2,4) + 
                     7137*Power(t2,5) + 620*Power(t2,6))))) - 
         Power(s,4)*(193 - 931*t1 + 1939*Power(t1,2) - 
            2074*Power(t1,3) + 1357*Power(t1,4) - 864*Power(t1,5) + 
            431*Power(t1,6) - 50*Power(t1,7) - 117*t2 + 2966*t1*t2 - 
            7276*Power(t1,2)*t2 + 5302*Power(t1,3)*t2 + 
            192*Power(t1,4)*t2 - 1275*Power(t1,5)*t2 + 
            197*Power(t1,6)*t2 - 10*Power(t1,7)*t2 - 1770*Power(t2,2) + 
            11262*t1*Power(t2,2) - 18308*Power(t1,2)*Power(t2,2) + 
            9316*Power(t1,3)*Power(t2,2) - 331*Power(t1,4)*Power(t2,2) - 
            107*Power(t1,5)*Power(t2,2) - 26*Power(t1,6)*Power(t2,2) - 
            2900*Power(t2,3) + 9626*t1*Power(t2,3) - 
            10086*Power(t1,2)*Power(t2,3) + 
            5124*Power(t1,3)*Power(t2,3) - 
            2108*Power(t1,4)*Power(t2,3) + 324*Power(t1,5)*Power(t2,3) - 
            1037*Power(t2,4) + 1551*t1*Power(t2,4) - 
            3636*Power(t1,2)*Power(t2,4) + 
            4252*Power(t1,3)*Power(t2,4) - 615*Power(t1,4)*Power(t2,4) - 
            10*Power(t1,5)*Power(t2,4) + 561*Power(t2,5) + 
            1443*t1*Power(t2,5) - 3427*Power(t1,2)*Power(t2,5) + 
            213*Power(t1,3)*Power(t2,5) + 47*Power(t1,4)*Power(t2,5) - 
            440*Power(t2,6) + 1645*t1*Power(t2,6) + 
            495*Power(t1,2)*Power(t2,6) - 67*Power(t1,3)*Power(t2,6) - 
            402*Power(t2,7) - 563*t1*Power(t2,7) + 
            19*Power(t1,2)*Power(t2,7) + 182*Power(t2,8) + 
            25*t1*Power(t2,8) - 14*Power(t2,9) + 
            Power(s1,8)*(70*Power(s2,3) + 21*t1 + 
               14*Power(s2,2)*(-5 + 9*t1 - 4*t2) - 6*t2 + 
               s2*(15 - 112*t1 + 42*t2)) + 
            Power(s2,8)*(11*Power(t1,3) - Power(t1,2)*(22 + 7*t2) - 
               2*t2*(3 + 13*t2 + 10*Power(t2,2)) + 
               t1*(11 + 13*t2 + 16*Power(t2,2))) + 
            Power(s2,7)*(57 + 2*Power(t1,4) + 89*t2 - 166*Power(t2,2) - 
               328*Power(t2,3) - 138*Power(t2,4) - 
               46*Power(t1,3)*(4 + 3*t2) + 
               Power(t1,2)*(391 + 505*t2 + 12*Power(t2,2)) + 
               t1*(-267 - 445*t2 + 107*Power(t2,2) + 262*Power(t2,3))) + 
            Power(s2,6)*(-243 - 794*t2 - 479*Power(t2,2) + 
               339*Power(t2,3) + 638*Power(t2,4) + 345*Power(t2,5) - 
               4*Power(t1,4)*(-52 + 9*t2) + 
               2*Power(t1,3)*(45 + 719*t2 + 459*Power(t2,2)) - 
               Power(t1,2)*(903 + 3848*t2 + 5107*Power(t2,2) + 
                  1248*Power(t2,3)) + 
               t1*(855 + 3245*t2 + 4874*Power(t2,2) + 
                  2763*Power(t2,3) + 21*Power(t2,4))) + 
            Power(s2,5)*(-25 + 13*Power(t1,5) + 840*t2 + 
               2462*Power(t2,2) + 3229*Power(t2,3) + 3395*Power(t2,4) + 
               1801*Power(t2,5) + 140*Power(t2,6) + 
               Power(t1,4)*(-1993 - 1662*t2 + 14*Power(t2,2)) + 
               Power(t1,3)*(4566 + 4972*t2 - 1063*Power(t2,2) - 
                  1842*Power(t2,3)) + 
               Power(t1,2)*(-3598 - 3886*t2 + 9028*Power(t2,2) + 
                  12621*Power(t2,3) + 3732*Power(t2,4)) - 
               t1*(-1016 + 433*t2 + 11296*Power(t2,2) + 
                  18537*Power(t2,3) + 11750*Power(t2,4) + 
                  2044*Power(t2,5))) + 
            Power(s2,4)*(677 + Power(t1,5)*(956 - 12*t2) + 1302*t2 - 
               1301*Power(t2,2) - 2364*Power(t2,3) - 2824*Power(t2,4) - 
               4611*Power(t2,5) - 3108*Power(t2,6) - 543*Power(t2,7) + 
               Power(t1,4)*(765 + 5747*t2 + 3880*Power(t2,2) + 
                  142*Power(t2,3)) + 
               Power(t1,3)*(-6137 - 21338*t2 - 25512*Power(t2,2) - 
                  5375*Power(t2,3) + 1097*Power(t2,4)) + 
               Power(t1,2)*(7492 + 27318*t2 + 35876*Power(t2,2) + 
                  13543*Power(t2,3) - 6585*Power(t2,4) - 
                  3163*Power(t2,5)) + 
               t1*(-3718 - 12632*t2 - 11017*Power(t2,2) + 
                  586*Power(t2,3) + 12577*Power(t2,4) + 
                  11235*Power(t2,5) + 2467*Power(t2,6))) - 
            Power(s2,3)*(-182 + 24*Power(t1,6) + 1415*t2 + 
               957*Power(t2,2) - 1835*Power(t2,3) - 344*Power(t2,4) + 
               1084*Power(t2,5) - 966*Power(t2,6) - 1329*Power(t2,7) - 
               230*Power(t2,8) + 
               Power(t1,5)*(3862 + 2628*t2 + 204*Power(t2,2)) + 
               Power(t1,4)*(-10243 - 11160*t2 + 137*Power(t2,2) + 
                  2426*Power(t2,3) + 140*Power(t2,4)) + 
               Power(t1,3)*(10744 + 11630*t2 - 15493*Power(t2,2) - 
                  21854*Power(t2,3) - 5589*Power(t2,4) + 6*Power(t2,5)) \
+ Power(t1,2)*(-5649 + 3179*t2 + 35678*Power(t2,2) + 
                  47357*Power(t2,3) + 23900*Power(t2,4) + 
                  1104*Power(t2,5) - 662*Power(t2,6)) + 
               t1*(1479 - 7357*t2 - 19654*Power(t2,2) - 
                  20646*Power(t2,3) - 15475*Power(t2,4) - 
                  3064*Power(t2,5) + 3184*Power(t2,6) + 746*Power(t2,7))\
) + Power(s2,2)*(-1376 - 554*t2 + 5237*Power(t2,2) + 6573*Power(t2,3) + 
               2591*Power(t2,4) + 1224*Power(t2,5) + 1017*Power(t2,6) - 
               109*Power(t2,7) - 227*Power(t2,8) - 12*Power(t2,9) + 
               Power(t1,6)*(688 + 92*t2) + 
               Power(t1,5)*(1038 + 2341*t2 + 1420*Power(t2,2) + 
                  210*Power(t2,3)) + 
               Power(t1,4)*(-7173 - 16000*t2 - 14220*Power(t2,2) - 
                  2017*Power(t2,3) + 90*Power(t2,4) + 30*Power(t2,5)) + 
               Power(t1,3)*(12864 + 30258*t2 + 26466*Power(t2,2) + 
                  4672*Power(t2,3) - 4642*Power(t2,4) - 
                  862*Power(t2,5) - 78*Power(t2,6)) + 
               Power(t1,2)*(-12449 - 20866*t2 - 1965*Power(t2,2) + 
                  13910*Power(t2,3) + 16644*Power(t2,4) + 
                  7035*Power(t2,5) + 387*Power(t2,6) + 54*Power(t2,7)) \
+ t1*(6429 + 4808*t2 - 16280*Power(t2,2) - 21646*Power(t2,3) - 
                  13538*Power(t2,4) - 8356*Power(t2,5) - 
                  1779*Power(t2,6) + 402*Power(t2,7) + 6*Power(t2,8))) - 
            s2*(-553 + 12*Power(t1,7) - 1202*t2 + 784*Power(t2,2) + 
               3778*Power(t2,3) + 3404*Power(t2,4) + 788*Power(t2,5) + 
               173*Power(t2,6) + 497*Power(t2,7) + 28*Power(t2,8) - 
               25*Power(t2,9) + 
               Power(t1,6)*(741 + 90*t2 + 56*Power(t2,2)) + 
               Power(t1,5)*(-1571 - 878*t2 + 685*Power(t2,2) + 
                  90*Power(t2,3) + 25*Power(t2,4)) - 
               Power(t1,4)*(-1099 + 3850*t2 + 8530*Power(t2,2) + 
                  3879*Power(t2,3) - 35*Power(t2,4) + 62*Power(t2,5)) + 
               Power(t1,3)*(1062 + 16828*t2 + 26156*Power(t2,2) + 
                  15447*Power(t2,3) + 2101*Power(t2,4) - 
                  904*Power(t2,5) + 61*Power(t2,6)) - 
               Power(t1,2)*(3123 + 19810*t2 + 24940*Power(t2,2) + 
                  11855*Power(t2,3) + 2076*Power(t2,4) - 
                  2682*Power(t2,5) - 1091*Power(t2,6) + 61*Power(t2,7)) \
+ t1*(2340 + 8777*t2 + 5874*Power(t2,2) - 3576*Power(t2,3) - 
                  3468*Power(t2,4) - 1545*Power(t2,5) - 
                  2188*Power(t2,6) - 396*Power(t2,7) + 62*Power(t2,8))) \
+ Power(s1,7)*(5 + 7*Power(t1,2) + t1*(98 - 142*t2) + 
               10*Power(s2,3)*(-23 + 98*t1 - 84*t2) - 47*t2 + 
               45*Power(t2,2) + 
               2*Power(s2,2)*
                (85 + 126*Power(t1,2) + 415*t2 + 217*Power(t2,2) - 
                  518*t1*(1 + t2)) - 
               s2*(33 + 196*Power(t1,2) + 51*t2 + 326*Power(t2,2) - 
                  18*t1*(1 + 49*t2))) + 
            Power(s1,6)*(65 - 255*Power(s2,5) + 119*Power(t1,3) + 
               Power(t1,2)*(727 - 221*t2) + 
               10*Power(s2,4)*(20 + 154*t1 - 43*t2) - 57*t2 + 
               360*Power(t2,2) - 158*Power(t2,3) + 
               t1*(-245 - 1013*t2 + 500*Power(t2,2)) + 
               Power(s2,3)*(29 + 1946*Power(t1,2) + 2921*t2 + 
                  3950*Power(t2,2) - 3*t1*(1432 + 2457*t2)) + 
               s2*(-268 + 84*Power(t1,3) - 103*t2 - 130*Power(t2,2) + 
                  1099*Power(t2,3) + 7*Power(t1,2)*(-212 + 133*t2) + 
                  t1*(1707 + 661*t2 - 2899*Power(t2,2))) + 
               Power(s2,2)*(321 + 126*Power(t1,3) - 1393*t2 - 
                  3831*Power(t2,2) - 1440*Power(t2,3) - 
                  7*Power(t1,2)*(125 + 216*t2) + 
                  t1*(351 + 7236*t2 + 3556*Power(t2,2)))) + 
            Power(s1,5)*(-180 - 250*Power(s2,6) - 77*Power(t1,4) + 
               Power(t1,3)*(112 - 438*t2) - 181*t2 + 531*Power(t2,2) - 
               1277*Power(t2,3) + 339*Power(t2,4) + 
               2*Power(s2,5)*(701 + 295*t1 + 710*t2) + 
               Power(t1,2)*(726 - 4170*t2 + 941*Power(t2,2)) + 
               t1*(-17 + 77*t2 + 4228*Power(t2,2) - 1130*Power(t2,3)) + 
               Power(s2,4)*(-924 + 3640*Power(t1,2) + 1346*t2 + 
                  2865*Power(t2,2) - 5*t1*(1587 + 2128*t2)) + 
               Power(s2,3)*(520 + 1176*Power(t1,3) - 2371*t2 - 
                  12902*Power(t2,2) - 9830*Power(t2,3) - 
                  3*Power(t1,2)*(1324 + 3759*t2) + 
                  t1*(4633 + 27643*t2 + 23076*Power(t2,2))) + 
               Power(s2,2)*(-1442 - 2235*t2 + 4250*Power(t2,2) + 
                  9382*Power(t2,3) + 2662*Power(t2,4) - 
                  14*Power(t1,3)*(-93 + 28*t2) + 
                  Power(t1,2)*(-9037 + 2923*t2 + 3626*Power(t2,2)) + 
                  t1*(8488 + 408*t2 - 20637*Power(t2,2) - 
                     6656*Power(t2,3))) + 
               s2*(1029 + 308*Power(t1,4) + 1299*t2 + 
                  1348*Power(t2,2) + 918*Power(t2,3) - 
                  2100*Power(t2,4) - Power(t1,3)*(1434 + 749*t2) + 
                  Power(t1,2)*(5661 + 8721*t2 - 1756*Power(t2,2)) + 
                  t1*(-4721 - 10410*t2 - 3801*Power(t2,2) + 
                     5247*Power(t2,3)))) - 
            Power(s1,4)*(53*Power(s2,7) + 
               Power(s2,6)*(-1310 + 305*t1 - 1413*t2) - 
               5*Power(t1,4)*(62 + 57*t2) + 
               Power(t1,3)*(-2923 + 1055*t2 - 525*Power(t2,2)) + 
               Power(s2,5)*(2536 + 4811*t1 - 2685*Power(t1,2) + 
                  5348*t2 + 4060*t1*t2 + 2952*Power(t2,2)) + 
               Power(t1,2)*(4829 + 4108*t2 - 10205*Power(t2,2) + 
                  1675*Power(t2,3)) - 
               t1*(2533 + 357*t2 + 2933*Power(t2,2) - 
                  9305*Power(t2,3) + 1650*Power(t2,4)) + 
               2*(262 - 616*t2 - 64*Power(t2,2) + 1028*Power(t2,3) - 
                  1315*Power(t2,4) + 240*Power(t2,5)) + 
               Power(s2,4)*(-645 - 2380*Power(t1,3) + 286*t2 + 
                  11083*Power(t2,2) + 7703*Power(t2,3) + 
                  65*Power(t1,2)*(174 + 289*t2) - 
                  2*t1*(6767 + 22973*t2 + 14990*Power(t2,2))) + 
               Power(s2,3)*(1312 - 140*Power(t1,4) + 786*t2 - 
                  10455*Power(t2,2) - 28327*Power(t2,3) - 
                  14400*Power(t2,4) + 5*Power(t1,3)*(-619 + 959*t2) + 
                  Power(t1,2)*(14899 - 15850*t2 - 26215*Power(t2,2)) + 
                  t1*(-8907 + 21841*t2 + 70548*Power(t2,2) + 
                     38940*Power(t2,3))) + 
               Power(s2,2)*(-2515 - 5981*t2 - 7344*Power(t2,2) + 
                  6579*Power(t2,3) + 13595*Power(t2,4) + 
                  2980*Power(t2,5) - 35*Power(t1,4)*(65 + 4*t2) + 
                  Power(t1,3)*(11050 + 8665*t2 - 210*Power(t2,2)) + 
                  Power(t1,2)*
                   (-26348 - 49169*t2 + 2125*Power(t2,2) + 
                     4370*Power(t2,3)) + 
                  t1*(19037 + 43006*t2 + 8212*Power(t2,2) - 
                     31310*Power(t2,3) - 7380*Power(t2,4))) + 
               s2*(332 + 70*Power(t1,5) + 4073*t2 + 2532*Power(t2,2) + 
                  4199*Power(t2,3) + 1905*Power(t2,4) - 
                  2485*Power(t2,5) + 35*Power(t1,4)*(19 + 23*t2) - 
                  5*Power(t1,3)*(-359 + 1182*t2 + 369*Power(t2,2)) + 
                  Power(t1,2)*
                   (-2698 + 25259*t2 + 20290*Power(t2,2) - 
                     1695*Power(t2,3)) + 
                  t1*(-390 - 19579*t2 - 26072*Power(t2,2) - 
                     8630*Power(t2,3) + 5780*Power(t2,4)))) + 
            Power(s1,3)*(1713 + 10*Power(s2,8) + 249*t2 - 
               3281*Power(t2,2) + 492*Power(t2,3) + 3879*Power(t2,4) - 
               3305*Power(t2,5) + 451*Power(t2,6) + 
               10*Power(t1,5)*(-34 + 5*t2) + 
               Power(s2,7)*(339 - 226*t1 + 273*t2) + 
               Power(t1,4)*(962 - 170*t2 - 495*Power(t2,2)) - 
               Power(t1,3)*(2909 + 12199*t2 - 2490*Power(t2,2) + 
                  70*Power(t2,3)) + 
               Power(t1,2)*(7384 + 13757*t2 + 10999*Power(t2,2) - 
                  13360*Power(t2,3) + 1465*Power(t2,4)) - 
               2*t1*(3462 + 1659*t2 + 1040*Power(t2,2) + 
                  4228*Power(t2,3) - 5885*Power(t2,4) + 753*Power(t2,5)\
) + Power(s2,6)*(740*Power(t1,2) + 3*t1*(-39 + 341*t2) - 
                  2*(938 + 2542*t2 + 1547*Power(t2,2))) + 
               Power(s2,5)*(970 + 2020*Power(t1,3) + 6641*t2 + 
                  5646*Power(t2,2) + 2746*Power(t2,3) - 
                  Power(t1,2)*(10772 + 11765*t2) + 
                  t1*(10383 + 25595*t2 + 10798*Power(t2,2))) + 
               Power(s2,4)*(-70 + 280*Power(t1,4) + 1557*t2 + 
                  11911*Power(t2,2) + 26281*Power(t2,3) + 
                  10762*Power(t2,4) - 10*Power(t1,3)*(89 + 870*t2) + 
                  Power(t1,2)*(932 + 44630*t2 + 37855*Power(t2,2)) - 
                  t1*(4230 + 61356*t2 + 102111*Power(t2,2) + 
                     43967*Power(t2,3))) + 
               Power(s2,3)*(3542 + 917*t2 + 643*Power(t2,2) - 
                  18395*Power(t2,3) - 34658*Power(t2,4) - 
                  12820*Power(t2,5) - 35*Power(t1,4)*(-160 + 3*t2) + 
                  Power(t1,3)*(-25228 - 19215*t2 + 7220*Power(t2,2)) + 
                  Power(t1,2)*
                   (46013 + 73683*t2 - 21477*Power(t2,2) - 
                     31105*Power(t2,3)) + 
                  t1*(-26944 - 35925*t2 + 32576*Power(t2,2) + 
                     91930*Power(t2,3) + 38120*Power(t2,4))) + 
               s2*(-3529 + 4694*t2 + 7166*Power(t2,2) + 
                  2655*Power(t2,3) + 6531*Power(t2,4) + 
                  1985*Power(t2,5) - 1862*Power(t2,6) + 
                  5*Power(t1,5)*(208 + 49*t2) + 
                  5*Power(t1,4)*(-781 - 36*t2 + 112*Power(t2,2)) + 
                  Power(t1,3)*
                   (18449 + 10229*t2 - 9795*Power(t2,2) - 
                     1820*Power(t2,3)) + 
                  Power(t1,2)*
                   (-28712 - 6026*t2 + 43764*Power(t2,2) + 
                     24475*Power(t2,3) - 980*Power(t2,4)) + 
                  t1*(16368 - 9125*t2 - 31965*Power(t2,2) - 
                     35014*Power(t2,3) - 10360*Power(t2,4) + 
                     4032*Power(t2,5))) - 
               Power(s2,2)*(1101 + 70*Power(t1,5) + 7201*t2 + 
                  11118*Power(t2,2) + 12849*Power(t2,3) - 
                  5726*Power(t2,4) - 12050*Power(t2,5) - 
                  2046*Power(t2,6) + 
                  5*Power(t1,4)*(737 + 1199*t2 + 84*Power(t2,2)) - 
                  Power(t1,3)*
                   (3708 + 40561*t2 + 18365*Power(t2,2) + 
                     540*Power(t2,3)) + 
                  Power(t1,2)*
                   (8973 + 104289*t2 + 99249*Power(t2,2) + 
                     2800*Power(t2,3) - 2640*Power(t2,4)) + 
                  t1*(-10048 - 69909*t2 - 87152*Power(t2,2) - 
                     20457*Power(t2,3) + 27330*Power(t2,4) + 
                     4876*Power(t2,5)))) - 
            Power(s1,2)*(1286 + 65*Power(t1,6) + 2976*t2 + 
               337*Power(t2,2) - 4147*Power(t2,3) + 1409*Power(t2,4) + 
               3861*Power(t2,5) - 2492*Power(t2,6) + 270*Power(t2,7) + 
               5*Power(s2,8)*(-3 + 5*t1 + 8*t2) + 
               Power(t1,5)*(-743 - 662*t2 + 100*Power(t2,2)) + 
               Power(t1,4)*(-942 + 4289*t2 + 1025*Power(t2,2) - 
                  490*Power(t2,3)) + 
               Power(t1,3)*(1080 - 11478*t2 - 19507*Power(t2,2) + 
                  2050*Power(t2,3) + 345*Power(t2,4)) + 
               Power(t1,2)*(5131 + 19448*t2 + 17031*Power(t2,2) + 
                  16005*Power(t2,3) - 9645*Power(t2,4) + 
                  587*Power(t2,5)) - 
               t1*(5824 + 14864*t2 + 1500*Power(t2,2) + 
                  4600*Power(t2,3) + 10675*Power(t2,4) - 
                  8623*Power(t2,5) + 812*Power(t2,6)) + 
               Power(s2,7)*(417 + 11*Power(t1,2) + 988*t2 + 
                  525*Power(t2,2) - t1*(436 + 651*t2)) - 
               Power(s2,6)*(835*Power(t1,3) - 
                  2*Power(t1,2)*(1748 + 1287*t2) + 
                  t1*(2050 + 2392*t2 - 1130*Power(t2,2)) + 
                  9*(66 + 531*t2 + 766*Power(t2,2) + 366*Power(t2,3))) \
+ Power(s2,5)*(-890 - 180*Power(t1,4) - 29*t2 + 1974*Power(t2,2) - 
                  945*Power(t2,3) + 901*Power(t2,4) + 
                  15*Power(t1,3)*(270 + 397*t2) - 
                  Power(t1,2)*(10633 + 35687*t2 + 19321*Power(t2,2)) + 
                  t1*(8129 + 40085*t2 + 48902*Power(t2,2) + 
                     13820*Power(t2,3))) + 
               Power(s2,4)*(-1773 + 3594*t2 + 8324*Power(t2,2) + 
                  23879*Power(t2,3) + 28805*Power(t2,4) + 
                  8268*Power(t2,5) + 5*Power(t1,4)*(-1009 + 81*t2) + 
                  Power(t1,3)*(17823 + 5601*t2 - 11340*Power(t2,2)) + 
                  Power(t1,2)*
                   (-24505 - 8683*t2 + 61438*Power(t2,2) + 
                     37078*Power(t2,3)) - 
                  t1*(-12049 + 16390*t2 + 94588*Power(t2,2) + 
                     109359*Power(t2,3) + 35321*Power(t2,4))) + 
               Power(s2,3)*(3936 - 135*Power(t1,5) + 3098*t2 - 
                  1826*Power(t2,2) + 2092*Power(t2,3) - 
                  15730*Power(t2,4) - 24047*Power(t2,5) - 
                  6790*Power(t2,6) + 
                  5*Power(t1,4)*(2030 + 2601*t2 + 69*Power(t2,2)) + 
                  Power(t1,3)*
                   (-26228 - 80376*t2 - 34277*Power(t2,2) + 
                     4765*Power(t2,3)) + 
                  Power(t1,2)*
                   (36138 + 149625*t2 + 127484*Power(t2,2) - 
                     10188*Power(t2,3) - 19605*Power(t2,4)) + 
                  t1*(-22677 - 72763*t2 - 62751*Power(t2,2) + 
                     15097*Power(t2,3) + 64492*Power(t2,4) + 
                     21495*Power(t2,5))) - 
               Power(s2,2)*(-3301 + 6470*t2 + 9779*Power(t2,2) + 
                  11285*Power(t2,3) + 12018*Power(t2,4) - 
                  2879*Power(t2,5) - 6405*Power(t2,6) - 
                  824*Power(t2,7) + 5*Power(t1,5)*(739 + 81*t2) + 
                  Power(t1,4)*
                   (-12534 - 466*t2 + 5395*Power(t2,2) + 
                     450*Power(t2,3)) + 
                  Power(t1,3)*
                   (31630 + 1786*t2 - 51055*Power(t2,2) - 
                     16805*Power(t2,3) - 850*Power(t2,4)) + 
                  Power(t1,2)*
                   (-33705 + 36544*t2 + 143369*Power(t2,2) + 
                     94174*Power(t2,3) + 5045*Power(t2,4) - 
                     572*Power(t2,5)) + 
                  2*t1*(7094 - 20978*t2 - 47808*Power(t2,2) - 
                     43797*Power(t2,3) - 10722*Power(t2,4) + 
                     6858*Power(t2,5) + 898*Power(t2,6))) + 
               s2*(-5894 + 90*Power(t1,6) - 460*t2 + 
                  11980*Power(t2,2) + 7017*Power(t2,3) + 
                  1743*Power(t2,4) + 5641*Power(t2,5) + 
                  1104*Power(t2,6) - 861*Power(t2,7) + 
                  Power(t1,5)*(2708 + 1335*t2 + 300*Power(t2,2)) - 
                  Power(t1,4)*
                   (8599 + 13221*t2 + 1470*Power(t2,2) + 
                     125*Power(t2,3)) + 
                  Power(t1,3)*
                   (25242 + 53408*t2 + 15425*Power(t2,2) - 
                     8500*Power(t2,3) - 620*Power(t2,4)) - 
                  Power(t1,2)*
                   (39642 + 58757*t2 + 5155*Power(t2,2) - 
                     37077*Power(t2,3) - 16635*Power(t2,4) + 
                     481*Power(t2,5)) + 
                  t1*(25787 + 17549*t2 - 20779*Power(t2,2) - 
                     25622*Power(t2,3) - 27105*Power(t2,4) - 
                     7017*Power(t2,5) + 1787*Power(t2,6)))) + 
            s1*(14 + 10*Power(t1,7) + 1893*t2 + 4343*Power(t2,2) + 
               1649*Power(t2,3) - 2479*Power(t2,4) + 1345*Power(t2,5) + 
               1961*Power(t2,6) - 1035*Power(t2,7) + 93*Power(t2,8) + 
               Power(t1,6)*(-184 + 171*t2) + 
               Power(t1,5)*(445 - 286*t2 - 802*Power(t2,2) + 
                  60*Power(t2,3)) + 
               Power(t1,4)*(-1970 - 417*t2 + 5594*Power(t2,2) + 
                  1500*Power(t2,3) - 250*Power(t2,4)) + 
               Power(t1,3)*(2891 - 8826*t2 - 13759*Power(t2,2) - 
                  14483*Power(t2,3) + 290*Power(t2,4) + 276*Power(t2,5)) \
+ Power(t1,2)*(-839 + 21264*t2 + 22566*Power(t2,2) + 
                  11739*Power(t2,3) + 11815*Power(t2,4) - 
                  3542*Power(t2,5) + 51*Power(t2,6)) - 
               t1*(349 + 13779*t2 + 18100*Power(t2,2) + 
                  2266*Power(t2,3) + 4303*Power(t2,4) + 
                  6629*Power(t2,5) - 3408*Power(t2,6) + 230*Power(t2,7)) \
+ Power(s2,8)*(-25 - 24*Power(t1,2) + 11*t2 + 50*Power(t2,2) + 
                  t1*(49 + 9*t2)) + 
               Power(s2,7)*(94 + 164*Power(t1,3) + 606*t2 + 
                  978*Power(t2,2) + 443*Power(t2,3) - 
                  Power(t1,2)*(283 + 40*t2) + 
                  t1*(17 - 530*t2 - 687*Power(t2,2))) + 
               Power(s2,6)*(400 + 40*Power(t1,4) - 279*t2 - 
                  3227*Power(t2,2) - 3758*Power(t2,3) - 
                  1708*Power(t2,4) - Power(t1,3)*(1931 + 1660*t2) + 
                  Power(t1,2)*(3863 + 8699*t2 + 3072*Power(t2,2)) + 
                  t1*(-2398 - 6928*t2 - 5050*Power(t2,2) + 
                     391*Power(t2,3))) - 
               Power(s2,5)*(386 + 3402*t2 + 4320*Power(t2,2) + 
                  5526*Power(t2,3) + 4446*Power(t2,4) + 
                  198*Power(t2,5) + 2*Power(t1,4)*(-926 + 115*t2) + 
                  Power(t1,3)*(2404 - 4557*t2 - 5815*Power(t2,2)) + 
                  Power(t1,2)*
                   (-309 + 19981*t2 + 37521*Power(t2,2) + 
                     13973*Power(t2,3)) - 
                  t1*(861 + 20324*t2 + 48299*Power(t2,2) + 
                     39868*Power(t2,3) + 8536*Power(t2,4))) + 
               Power(s2,4)*(-2291 + 100*Power(t1,5) + 80*t2 + 
                  5990*Power(t2,2) + 8946*Power(t2,3) + 
                  17789*Power(t2,4) + 15169*Power(t2,5) + 
                  3317*Power(t2,6) - 
                  2*Power(t1,4)*(4593 + 4274*t2 + 15*Power(t2,2)) + 
                  Power(t1,3)*
                   (22885 + 45615*t2 + 11700*Power(t2,2) - 
                     6117*Power(t2,3)) + 
                  Power(t1,2)*
                   (-24521 - 62590*t2 - 23330*Power(t2,2) + 
                     34703*Power(t2,3) + 17531*Power(t2,4)) - 
                  t1*(-12523 - 22171*t2 + 12392*Power(t2,2) + 
                     59343*Power(t2,3) + 56494*Power(t2,4) + 
                     14701*Power(t2,5))) + 
               Power(s2,3)*(1095 + 4362*t2 - 2072*Power(t2,2) - 
                  1775*Power(t2,3) + 2799*Power(t2,4) - 
                  6414*Power(t2,5) - 8834*Power(t2,6) - 
                  1950*Power(t2,7) + Power(t1,5)*(3657 + 40*t2) + 
                  Power(t1,4)*
                   (-5815 + 7489*t2 + 9953*Power(t2,2) + 
                     450*Power(t2,3)) + 
                  Power(t1,3)*
                   (4242 - 41591*t2 - 76342*Power(t2,2) - 
                     23746*Power(t2,3) + 1170*Power(t2,4)) + 
                  Power(t1,2)*
                   (1189 + 75965*t2 + 150215*Power(t2,2) + 
                     92600*Power(t2,3) + 515*Power(t2,4) - 
                     6046*Power(t2,5)) + 
                  t1*(-3928 - 43357*t2 - 66665*Power(t2,2) - 
                     51208*Power(t2,3) - 3335*Power(t2,4) + 
                     22947*Power(t2,5) + 6376*Power(t2,6))) - 
               Power(s2,2)*(-4299 + 80*Power(t1,6) + 1687*t2 + 
                  11944*Power(t2,2) + 7684*Power(t2,3) + 
                  5930*Power(t2,4) + 5616*Power(t2,5) - 
                  814*Power(t2,6) - 1866*Power(t2,7) - 
                  170*Power(t2,8) + 
                  Power(t1,5)*(6634 + 4333*t2 + 550*Power(t2,2)) + 
                  Power(t1,4)*
                   (-20771 - 30785*t2 - 5429*Power(t2,2) + 
                     1765*Power(t2,3) + 200*Power(t2,4)) + 
                  Power(t1,3)*
                   (37111 + 64279*t2 + 9864*Power(t2,2) - 
                     26186*Power(t2,3) - 6665*Power(t2,4) - 
                     444*Power(t2,5)) + 
                  Power(t1,2)*
                   (-38325 - 35706*t2 + 40429*Power(t2,2) + 
                     82072*Power(t2,3) + 42092*Power(t2,4) + 
                     2555*Power(t2,5) + 118*Power(t2,6)) + 
                  t1*(19712 - 3240*t2 - 52925*Power(t2,2) - 
                     58282*Power(t2,3) - 43316*Power(t2,4) - 
                     10219*Power(t2,5) + 3661*Power(t2,6) + 
                     296*Power(t2,7))) + 
               s2*(-3329 - 3440*t2 + 6800*Power(t2,2) + 
                  11022*Power(t2,3) + 3683*Power(t2,4) + 
                  762*Power(t2,5) + 2594*Power(t2,6) + 300*Power(t2,7) - 
                  224*Power(t2,8) + Power(t1,6)*(523 + 120*t2) + 
                  2*Power(t1,5)*
                   (275 + 702*t2 + 256*Power(t2,2) + 75*Power(t2,3)) - 
                  Power(t1,4)*
                   (2793 + 15307*t2 + 12364*Power(t2,2) + 
                     590*Power(t2,3) + 250*Power(t2,4)) + 
                  Power(t1,3)*
                   (11248 + 49644*t2 + 48538*Power(t2,2) + 
                     9092*Power(t2,3) - 4085*Power(t2,4) + 
                     81*Power(t2,5)) - 
                  Power(t1,2)*
                   (20640 + 57870*t2 + 40967*Power(t2,2) + 
                     3903*Power(t2,3) - 15593*Power(t2,4) - 
                     6304*Power(t2,5) + 236*Power(t2,6)) + 
                  t1*(14417 + 25205*t2 - 2364*Power(t2,2) - 
                     15512*Power(t2,3) - 10060*Power(t2,4) - 
                     11648*Power(t2,5) - 2561*Power(t2,6) + 
                     479*Power(t2,7))))) + 
         Power(s,3)*(-567 + 3199*t1 - 7209*Power(t1,2) + 
            8299*Power(t1,3) - 5308*Power(t1,4) + 1922*Power(t1,5) - 
            274*Power(t1,6) - 72*Power(t1,7) + 10*Power(t1,8) - 
            1487*t2 + 10577*t1*t2 - 27476*Power(t1,2)*t2 + 
            33659*Power(t1,3)*t2 - 20305*Power(t1,4)*t2 + 
            5505*Power(t1,5)*t2 - 540*Power(t1,6)*t2 + 
            72*Power(t1,7)*t2 - 2144*Power(t2,2) + 
            15769*t1*Power(t2,2) - 37712*Power(t1,2)*Power(t2,2) + 
            40409*Power(t1,3)*Power(t2,2) - 
            21049*Power(t1,4)*Power(t2,2) + 
            5302*Power(t1,5)*Power(t2,2) - 622*Power(t1,6)*Power(t2,2) + 
            8*Power(t1,7)*Power(t2,2) - 2384*Power(t2,3) + 
            11255*t1*Power(t2,3) - 21057*Power(t1,2)*Power(t2,3) + 
            21089*Power(t1,3)*Power(t2,3) - 
            11018*Power(t1,4)*Power(t2,3) + 
            2024*Power(t1,5)*Power(t2,3) - 44*Power(t1,6)*Power(t2,3) - 
            717*Power(t2,4) + 2806*t1*Power(t2,4) - 
            7694*Power(t1,2)*Power(t2,4) + 
            7907*Power(t1,3)*Power(t2,4) - 
            1648*Power(t1,4)*Power(t2,4) - 86*Power(t1,5)*Power(t2,4) + 
            165*Power(t2,5) + 2465*t1*Power(t2,5) - 
            3724*Power(t1,2)*Power(t2,5) - 637*Power(t1,3)*Power(t2,5) + 
            423*Power(t1,4)*Power(t2,5) + 5*Power(t1,5)*Power(t2,5) - 
            856*Power(t2,6) + 1315*t1*Power(t2,6) + 
            1576*Power(t1,2)*Power(t2,6) - 445*Power(t1,3)*Power(t2,6) - 
            21*Power(t1,4)*Power(t2,6) - 206*Power(t2,7) - 
            1089*t1*Power(t2,7) + 81*Power(t1,2)*Power(t2,7) + 
            31*Power(t1,3)*Power(t2,7) + 314*Power(t2,8) + 
            119*t1*Power(t2,8) - 17*Power(t1,2)*Power(t2,8) - 
            56*Power(t2,9) + 2*Power(t2,10) + 
            Power(s1,9)*(56*Power(s2,3) + 7*t1 + 
               14*Power(s2,2)*(-3 + 6*t1 - 2*t2) - t2 + 
               s2*(6 - 56*t1 + 14*t2)) + 
            Power(s2,9)*(2*Power(t1,3) + 2*Power(t1,2)*(-2 + t2) + 
               t1*(2 - t2 + 11*Power(t2,2)) - 
               t2*(1 + 16*t2 + 15*Power(t2,2))) - 
            Power(s2,8)*(-19 + 29*t2 + 217*Power(t2,2) + 
               252*Power(t2,3) + 83*Power(t2,4) + 
               Power(t1,3)*(38 + 39*t2) + 
               Power(t1,2)*(-91 - 38*t2 + 95*Power(t2,2)) - 
               t1*(-72 + 31*t2 + 327*Power(t2,2) + 217*Power(t2,3))) + 
            Power(s2,7)*(Power(t1,4)*(63 - 15*t2) + 
               Power(t1,3)*(22 + 810*t2 + 467*Power(t2,2)) - 
               Power(t1,2)*(334 + 1803*t2 + 2094*Power(t2,2) + 
                  331*Power(t2,3)) + 
               t1*(373 + 1365*t2 + 1404*Power(t2,2) - 
                  189*Power(t2,3) - 549*Power(t2,4)) + 
               4*(-31 - 88*t2 + 50*Power(t2,2) + 348*Power(t2,3) + 
                  340*Power(t2,4) + 107*Power(t2,5))) + 
            Power(s2,6)*(36 + 6*Power(t1,5) + 1188*t2 + 
               3619*Power(t2,2) + 2998*Power(t2,3) + 579*Power(t2,4) - 
               430*Power(t2,5) - 314*Power(t2,6) + 
               Power(t1,4)*(-895 - 1014*t2 + 16*Power(t2,2)) + 
               Power(t1,3)*(2260 + 1712*t2 - 1919*Power(t2,2) - 
                  1534*Power(t2,3)) + 
               Power(t1,2)*(-1889 + 355*t2 + 9765*Power(t2,2) + 
                  10486*Power(t2,3) + 2646*Power(t2,4)) - 
               t1*(-482 + 2287*t2 + 11633*Power(t2,2) + 
                  14431*Power(t2,3) + 7219*Power(t2,4) + 814*Power(t2,5)\
)) + Power(s2,5)*(494 + Power(t1,5)*(643 - 3*t2) + 1221*t2 - 
               3009*Power(t2,2) - 7852*Power(t2,3) - 7352*Power(t2,4) - 
               5064*Power(t2,5) - 2381*Power(t2,6) - 361*Power(t2,7) + 
               2*Power(t1,4)*
                (107 + 2961*t2 + 2062*Power(t2,2) + 69*Power(t2,3)) + 
               Power(t1,3)*(-4109 - 19545*t2 - 23224*Power(t2,2) - 
                  4463*Power(t2,3) + 1662*Power(t2,4)) + 
               Power(t1,2)*(5635 + 23701*t2 + 30204*Power(t2,2) + 
                  5349*Power(t2,3) - 10385*Power(t2,4) - 
                  4114*Power(t2,5)) + 
               t1*(-2877 - 11181*t2 - 7496*Power(t2,2) + 
                  10967*Power(t2,3) + 20909*Power(t2,4) + 
                  13243*Power(t2,5) + 2675*Power(t2,6))) - 
            Power(s2,4)*(-266 + 34*Power(t1,6) + 3156*t2 + 
               6843*Power(t2,2) + 2112*Power(t2,3) - 1252*Power(t2,4) - 
               69*Power(t2,5) - 1623*Power(t2,6) - 2044*Power(t2,7) - 
               447*Power(t2,8) + 
               Power(t1,5)*(3834 + 3873*t2 + 284*Power(t2,2)) + 
               Power(t1,4)*(-11790 - 13279*t2 + 1641*Power(t2,2) + 
                  4856*Power(t2,3) + 248*Power(t2,4)) + 
               Power(t1,3)*(13759 + 12248*t2 - 26407*Power(t2,2) - 
                  38563*Power(t2,3) - 10804*Power(t2,4) + 
                  445*Power(t2,5)) + 
               Power(t1,2)*(-7700 + 5168*t2 + 57791*Power(t2,2) + 
                  77867*Power(t2,3) + 40334*Power(t2,4) + 
                  1462*Power(t2,5) - 2095*Power(t2,6)) + 
               t1*(2129 - 11036*t2 - 39452*Power(t2,2) - 
                  42152*Power(t2,3) - 24351*Power(t2,4) - 
                  2845*Power(t2,5) + 6295*Power(t2,6) + 1849*Power(t2,7)\
)) + Power(s2,3)*(8*Power(t1,6)*(176 + 25*t2) + 
               Power(t1,5)*(-814 + 6276*t2 + 4856*Power(t2,2) + 
                  506*Power(t2,3)) + 
               Power(t1,4)*(-7297 - 33759*t2 - 40036*Power(t2,2) - 
                  8960*Power(t2,3) + 1419*Power(t2,4) + 113*Power(t2,5)\
) + Power(t1,3)*(16088 + 63232*t2 + 79163*Power(t2,2) + 
                  27034*Power(t2,3) - 11984*Power(t2,4) - 
                  5242*Power(t2,5) - 115*Power(t2,6)) + 
               Power(t1,2)*(-15284 - 50281*t2 - 40938*Power(t2,2) + 
                  2791*Power(t2,3) + 32316*Power(t2,4) + 
                  23663*Power(t2,5) + 3214*Power(t2,6) - 
                  221*Power(t2,7)) + 
               t1*(7363 + 15637*t2 - 11956*Power(t2,2) - 
                  34829*Power(t2,3) - 26808*Power(t2,4) - 
                  19175*Power(t2,5) - 7838*Power(t2,6) + 
                  623*Power(t2,7) + 335*Power(t2,8)) + 
               2*(-732 - 623*t2 + 4573*Power(t2,2) + 7632*Power(t2,3) + 
                  3254*Power(t2,4) + 603*Power(t2,5) + 
                  1165*Power(t2,6) + 364*Power(t2,7) - 
                  260*Power(t2,8) - 56*Power(t2,9))) - 
            Power(s2,2)*(408 + 72*Power(t1,7) - 3778*t2 - 
               6663*Power(t2,2) + 2810*Power(t2,3) + 9117*Power(t2,4) + 
               3862*Power(t2,5) + 615*Power(t2,6) + 776*Power(t2,7) + 
               133*Power(t2,8) - 62*Power(t2,9) - 2*Power(t2,10) + 
               4*Power(t1,6)*(706 + 450*t2 + 75*Power(t2,2)) + 
               2*Power(t1,5)*
                (-5461 - 5545*t2 - 696*Power(t2,2) + 692*Power(t2,3) + 
                  105*Power(t2,4)) + 
               Power(t1,4)*(18447 + 15234*t2 - 12964*Power(t2,2) - 
                  17994*Power(t2,3) - 3973*Power(t2,4) - 
                  162*Power(t2,5) + 12*Power(t2,6)) + 
               Power(t1,3)*(-17630 + 9944*t2 + 60597*Power(t2,2) + 
                  60262*Power(t2,3) + 21984*Power(t2,4) - 
                  256*Power(t2,5) - 397*Power(t2,6) - 34*Power(t2,7)) + 
               Power(t1,2)*(9641 - 33139*t2 - 81459*Power(t2,2) - 
                  59722*Power(t2,3) - 22945*Power(t2,4) + 
                  1255*Power(t2,5) + 4009*Power(t2,6) + 
                  378*Power(t2,7) + 30*Power(t2,8)) - 
               t1*(2840 - 21019*t2 - 41553*Power(t2,2) - 
                  13665*Power(t2,3) + 5289*Power(t2,4) + 
                  3489*Power(t2,5) + 4345*Power(t2,6) + 
                  1597*Power(t2,7) - 33*Power(t2,8) + 6*Power(t2,9))) + 
            s2*(1902 + 690*t2 - 8437*Power(t2,2) - 12541*Power(t2,3) - 
               6598*Power(t2,4) - 1409*Power(t2,5) - 536*Power(t2,6) - 
               379*Power(t2,7) + 92*Power(t2,8) + 55*Power(t2,9) - 
               7*Power(t2,10) + 22*Power(t1,7)*(10 + t2) + 
               Power(t1,6)*(574 + 864*t2 + 392*Power(t2,2) + 
                  84*Power(t2,3)) + 
               Power(t1,5)*(-5279 - 9841*t2 - 7132*Power(t2,2) - 
                  290*Power(t2,3) + 25*Power(t2,4) + 11*Power(t2,5)) - 
               2*Power(t1,4)*
                (-7700 - 14078*t2 - 9654*Power(t2,2) - 
                  811*Power(t2,3) + 1848*Power(t2,4) + 
                  79*Power(t2,5) + 17*Power(t2,6)) + 
               Power(t1,3)*(-24355 - 32907*t2 - 3590*Power(t2,2) + 
                  16869*Power(t2,3) + 14455*Power(t2,4) + 
                  5041*Power(t2,5) - 300*Power(t2,6) + 43*Power(t2,7)) \
- Power(t1,2)*(-21409 - 16749*t2 + 33060*Power(t2,2) + 
                  48221*Power(t2,3) + 26588*Power(t2,4) + 
                  9209*Power(t2,5) + 1430*Power(t2,6) - 
                  609*Power(t2,7) + 35*Power(t2,8)) + 
               t1*(-9871 - 3752*t2 + 32561*Power(t2,2) + 
                  42851*Power(t2,3) + 21554*Power(t2,4) + 
                  6754*Power(t2,5) + 2135*Power(t2,6) - 
                  131*Power(t2,7) - 315*Power(t2,8) + 22*Power(t2,9))) + 
            Power(s1,8)*(-2 - 19*Power(t1,2) + 
               Power(s2,3)*(-127 + 784*t1 - 714*t2) + t1*(65 - 44*t2) - 
               19*t2 + 8*Power(t2,2) + 
               Power(s2,2)*(15 + 168*Power(t1,2) + 486*t2 + 
                  238*Power(t2,2) - 56*t1*(9 + 13*t2)) + 
               s2*(22 - 44*Power(t1,2) + 14*t2 - 117*Power(t2,2) + 
                  6*t1*(-24 + 73*t2))) + 
            Power(s1,7)*(22 - 340*Power(s2,5) + 73*Power(t1,3) + 
               Power(s2,4)*(161 + 1554*t1 - 515*t2) - 27*t2 + 
               147*Power(t2,2) - 30*Power(t2,3) + 
               Power(t1,2)*(293 + 30*t2) + 
               t1*(-27 - 545*t2 + 142*Power(t2,2)) + 
               Power(s2,3)*(-33 + 1540*Power(t1,2) + 1884*t2 + 
                  3652*Power(t2,2) - t1*(2606 + 6517*t2)) + 
               s2*(-202 + 144*Power(t1,3) - 338*t2 - 337*Power(t2,2) + 
                  434*Power(t2,3) + 15*Power(t1,2)*(-89 + 9*t2) + 
                  t1*(1335 + 1548*t2 - 1478*Power(t2,2))) + 
               Power(s2,2)*(408 + 84*Power(t1,3) + 46*t2 - 
                  2298*Power(t2,2) - 884*Power(t2,3) - 
                  21*Power(t1,2)*(-17 + 52*t2) + 
                  t1*(-1392 + 3565*t2 + 2702*Power(t2,2)))) - 
            Power(s1,6)*(-65 + 510*Power(s2,6) + 21*Power(t1,4) + 
               71*t2 - 366*Power(t2,2) + 539*Power(t2,3) - 
               70*Power(t2,4) + Power(t1,3)*(263 + 353*t2) - 
               Power(s2,5)*(1693 + 880*t1 + 1975*t2) + 
               Power(t1,2)*(-1463 + 1892*t2 - 164*Power(t2,2)) + 
               t1*(677 + 736*t2 - 2188*Power(t2,2) + 320*Power(t2,3)) + 
               Power(s2,4)*(8 - 3556*Power(t1,2) - 1499*t2 - 
                  3755*Power(t2,2) + 5*t1*(1495 + 2443*t2)) + 
               s2*(-288 - 196*Power(t1,4) - 1212*t2 - 
                  1519*Power(t2,2) - 1447*Power(t2,3) + 
                  938*Power(t2,4) + 25*Power(t1,3)*(23 + 35*t2) + 
                  Power(t1,2)*(-1450 - 8134*t2 + Power(t2,2)) + 
                  t1*(870 + 8196*t2 + 6303*Power(t2,2) - 
                     2828*Power(t2,3))) + 
               Power(s2,3)*(-924*Power(t1,3) + 
                  Power(t1,2)*(-430 + 10101*t2) + 
                  t1*(1146 - 17980*t2 - 23033*Power(t2,2)) + 
                  2*(53 + 198*t2 + 4667*Power(t2,2) + 5083*Power(t2,3))\
) + Power(s2,2)*(1042 + 3873*t2 + 1010*Power(t2,2) - 5996*Power(t2,3) - 
                  1876*Power(t2,4) + 28*Power(t1,3)*(-77 + 10*t2) + 
                  Power(t1,2)*(10168 + 3994*t2 - 2926*Power(t2,2)) + 
                  t1*(-8918 - 12032*t2 + 10523*Power(t2,2) + 
                     5622*Power(t2,3)))) + 
            Power(s1,5)*(-563 - 262*Power(s2,7) - 221*t2 + 
               387*Power(t2,2) - 1669*Power(t2,3) + 1211*Power(t2,4) - 
               112*Power(t2,5) + Power(t1,4)*(127 + 121*t2) + 
               Power(s2,6)*(2542 - 370*t1 + 3132*t2) + 
               Power(t1,3)*(2126 + 985*t2 + 629*Power(t2,2)) - 
               Power(t1,2)*(3258 + 7859*t2 - 5254*Power(t2,2) + 
                  588*Power(t2,3)) + 
               t1*(2166 + 2815*t2 + 4738*Power(t2,2) - 
                  5159*Power(t2,3) + 520*Power(t2,4)) + 
               Power(s2,5)*(-1989 + 3440*Power(t1,2) - 7161*t2 - 
                  4288*Power(t2,2) - t1*(7369 + 7055*t2)) + 
               Power(s2,4)*(-2487 + 2282*Power(t1,3) - 4150*t2 - 
                  12570*Power(t2,2) - 11497*Power(t2,3) - 
                  Power(t1,2)*(6954 + 21493*t2) + 
                  t1*(10537 + 48468*t2 + 40016*Power(t2,2))) + 
               Power(s2,3)*(-872 + 112*Power(t1,4) + 
                  Power(t1,3)*(7286 - 4375*t2) - 705*t2 + 
                  1294*Power(t2,2) + 23375*Power(t2,3) + 
                  17232*Power(t2,4) + 
                  3*Power(t1,2)*(-9229 - 2440*t2 + 9142*Power(t2,2)) + 
                  t1*(19110 + 13473*t2 - 50646*Power(t2,2) - 
                     45185*Power(t2,3))) - 
               s2*(-1334 + 56*Power(t1,5) + 1588*t2 + 
                  2730*Power(t2,2) + 3387*Power(t2,3) + 
                  3145*Power(t2,4) - 1302*Power(t2,5) + 
                  7*Power(t1,4)*(-79 + 83*t2) + 
                  Power(t1,3)*(7607 - 1820*t2 - 2012*Power(t2,2)) + 
                  Power(t1,2)*
                   (-14799 + 5127*t2 + 20904*Power(t2,2) + 
                     445*Power(t2,3)) + 
                  t1*(9200 - 2799*t2 - 21027*Power(t2,2) - 
                     13650*Power(t2,3) + 3402*Power(t2,4))) + 
               Power(s2,2)*(1653 + 7072*t2 + 14337*Power(t2,2) + 
                  3968*Power(t2,3) - 9642*Power(t2,4) - 
                  2492*Power(t2,5) + 7*Power(t1,4)*(289 + 16*t2) + 
                  3*Power(t1,3)*(-2396 - 4219*t2 + 42*Power(t2,2)) + 
                  Power(t1,2)*
                   (13321 + 59218*t2 + 14973*Power(t2,2) - 
                     4100*Power(t2,3)) + 
                  t1*(-9747 - 52677*t2 - 41211*Power(t2,2) + 
                     16813*Power(t2,3) + 7174*Power(t2,4)))) + 
            Power(s1,4)*(105 - 26*Power(s2,8) + 2602*t2 - 
               24*Power(t2,2) - 1484*Power(t2,3) + 3940*Power(t2,4) - 
               1785*Power(t2,5) + 126*Power(t2,6) + 
               5*Power(t1,5)*(-42 + 11*t2) + 
               Power(s2,7)*(1319 - 554*t1 + 1502*t2) + 
               Power(t1,4)*(-662 + 15*t2 - 335*Power(t2,2)) - 
               Power(t1,3)*(-4167 + 10443*t2 + 1795*Power(t2,2) + 
                  415*Power(t2,3)) + 
               Power(t1,2)*(-3778 + 11948*t2 + 19218*Power(t2,2) - 
                  7875*Power(t2,3) + 760*Power(t2,4)) - 
               t1*(-744 + 7835*t2 + 4527*Power(t2,2) + 
                  12141*Power(t2,3) - 7500*Power(t2,4) + 
                  572*Power(t2,5)) + 
               Power(s2,6)*(-3551 + 1430*Power(t1,2) - 11832*t2 - 
                  7740*Power(t2,2) + 4*t1*(-361 + 293*t2)) + 
               Power(s2,5)*(-2384 + 2460*Power(t1,3) + 3853*t2 + 
                  8803*Power(t2,2) + 3841*Power(t2,3) - 
                  2*Power(t1,2)*(6439 + 9270*t2) + 
                  t1*(17522 + 44575*t2 + 22447*Power(t2,2))) + 
               Power(s2,4)*(2923 + 280*Power(t1,4) + 14908*t2 + 
                  18895*Power(t2,2) + 34114*Power(t2,3) + 
                  19185*Power(t2,4) - 95*Power(t1,3)*(-60 + 107*t2) + 
                  9*Power(t1,2)*(-2308 + 3300*t2 + 5845*Power(t2,2)) - 
                  t1*(-7599 + 50531*t2 + 125517*Power(t2,2) + 
                     70843*Power(t2,3))) + 
               Power(s2,3)*(5209 + 5046*t2 + 7648*Power(t2,2) + 
                  172*Power(t2,3) - 33915*Power(t2,4) - 
                  18566*Power(t2,5) - 35*Power(t1,4)*(-187 + 3*t2) + 
                  Power(t1,3)*(-28722 - 39380*t2 + 8005*Power(t2,2)) + 
                  Power(t1,2)*
                   (53166 + 147953*t2 + 29272*Power(t2,2) - 
                     39715*Power(t2,3)) + 
                  t1*(-35745 - 101525*t2 - 53262*Power(t2,2) + 
                     75527*Power(t2,3) + 53645*Power(t2,4))) + 
               s2*(-4736 - 3564*t2 + 3230*Power(t2,2) + 
                  2461*Power(t2,3) + 4440*Power(t2,4) + 
                  4079*Power(t2,5) - 1204*Power(t2,6) + 
                  5*Power(t1,5)*(219 + 49*t2) + 
                  20*Power(t1,4)*(-294 - 169*t2 + 21*Power(t2,2)) + 
                  Power(t1,3)*
                   (22091 + 36431*t2 - 2490*Power(t2,2) - 
                     2135*Power(t2,3)) + 
                  Power(t1,2)*
                   (-36025 - 64133*t2 + 5832*Power(t2,2) + 
                     29585*Power(t2,3) + 595*Power(t2,4)) + 
                  t1*(23309 + 35790*t2 - 1305*Power(t2,2) - 
                     28979*Power(t2,3) - 17805*Power(t2,4) + 
                     2716*Power(t2,5))) - 
               Power(s2,2)*(-1851 + 70*Power(t1,5) + 11666*t2 + 
                  18547*Power(t2,2) + 27554*Power(t2,3) + 
                  7350*Power(t2,4) - 9972*Power(t2,5) - 
                  2128*Power(t2,6) + 
                  5*Power(t1,4)*(-511 + 1328*t2 + 84*Power(t2,2)) - 
                  Power(t1,3)*
                   (-25184 + 26454*t2 + 28865*Power(t2,2) + 
                     650*Power(t2,3)) + 
                  Power(t1,2)*
                   (-38439 + 58253*t2 + 137287*Power(t2,2) + 
                     27470*Power(t2,3) - 3070*Power(t2,4)) + 
                  t1*(16428 - 47873*t2 - 124749*Power(t2,2) - 
                     73961*Power(t2,3) + 15715*Power(t2,4) + 
                     5734*Power(t2,5)))) + 
            Power(s1,3)*(2796 + 10*Power(s2,9) - 40*Power(t1,6) - 
               2479*t2 - 5247*Power(t2,2) + 1604*Power(t2,3) + 
               2716*Power(t2,4) - 5333*Power(t2,5) + 1729*Power(t2,6) - 
               98*Power(t2,7) + Power(s2,8)*(199 - 164*t1 + 147*t2) - 
               2*Power(t1,5)*(59 - 319*t2 + 75*Power(t2,2)) + 
               2*Power(t1,4)*
                (3205 + 768*t2 - 735*Power(t2,2) + 255*Power(t2,3)) + 
               Power(t1,3)*(-19159 - 10788*t2 + 20574*Power(t2,2) + 
                  2550*Power(t2,3) - 125*Power(t2,4)) + 
               Power(t1,2)*(24295 + 3186*t2 - 15264*Power(t2,2) - 
                  26706*Power(t2,3) + 6545*Power(t2,4) - 
                  426*Power(t2,5)) + 
               t1*(-13961 + 6181*t2 + 10254*Power(t2,2) + 
                  2146*Power(t2,3) + 16849*Power(t2,4) - 
                  6735*Power(t2,5) + 394*Power(t2,6)) + 
               Power(s2,7)*(-1753 + 106*Power(t1,2) - 5516*t2 - 
                  3370*Power(t2,2) + t1*(1094 + 2285*t2)) + 
               Power(s2,6)*(330 + 1370*Power(t1,3) + 11902*t2 + 
                  20596*Power(t2,2) + 9842*Power(t2,3) - 
                  Power(t1,2)*(7513 + 6576*t2) + 
                  t1*(7156 + 10243*t2 - 436*Power(t2,2))) + 
               Power(s2,5)*(4958 + 240*Power(t1,4) + 15148*t2 + 
                  6263*Power(t2,2) + 2008*Power(t2,3) - 
                  84*Power(t2,4) - 10*Power(t1,3)*(151 + 955*t2) + 
                  Power(t1,2)*(6399 + 52829*t2 + 39394*Power(t2,2)) - 
                  t1*(12317 + 79770*t2 + 103604*Power(t2,2) + 
                     36486*Power(t2,3))) - 
               Power(s2,4)*(-3479 + 14734*t2 + 29671*Power(t2,2) + 
                  33328*Power(t2,3) + 46371*Power(t2,4) + 
                  18845*Power(t2,5) + 10*Power(t1,4)*(-799 + 54*t2) + 
                  Power(t1,3)*(38175 + 31124*t2 - 17110*Power(t2,2)) + 
                  Power(t1,2)*
                   (-64521 - 103664*t2 + 44768*Power(t2,2) + 
                     66512*Power(t2,3)) + 
                  t1*(35963 + 36755*t2 - 83710*Power(t2,2) - 
                     166788*Power(t2,3) - 73122*Power(t2,4))) + 
               Power(s2,3)*(-4073 + 180*Power(t1,5) - 22940*t2 - 
                  12784*Power(t2,2) - 19984*Power(t2,3) - 
                  4553*Power(t2,4) + 29762*Power(t2,5) + 
                  12724*Power(t2,6) - 
                  10*Power(t1,4)*(261 + 2028*t2 + 46*Power(t2,2)) + 
                  Power(t1,3)*
                   (-4598 + 105726*t2 + 79228*Power(t2,2) - 
                     6890*Power(t2,3)) + 
                  Power(t1,2)*
                   (-2526 - 214022*t2 - 301718*Power(t2,2) - 
                     51410*Power(t2,3) + 32720*Power(t2,4)) + 
                  t1*(13818 + 145651*t2 + 212120*Power(t2,2) + 
                     98246*Power(t2,3) - 64038*Power(t2,4) - 
                     39199*Power(t2,5))) + 
               s2*(3170 - 120*Power(t1,6) + 16238*t2 + 
                  4473*Power(t2,2) - 2506*Power(t2,3) + 
                  286*Power(t2,4) - 3672*Power(t2,5) - 
                  3299*Power(t2,6) + 742*Power(t2,7) - 
                  2*Power(t1,5)*(591 + 1030*t2 + 200*Power(t2,2)) + 
                  2*Power(t1,4)*
                   (-1267 + 9577*t2 + 3125*Power(t2,2) + 
                     150*Power(t2,3)) + 
                  Power(t1,3)*
                   (3824 - 80536*t2 - 66466*Power(t2,2) + 
                     2600*Power(t2,3) + 880*Power(t2,4)) + 
                  Power(t1,2)*
                   (8837 + 128525*t2 + 111416*Power(t2,2) + 
                     196*Power(t2,3) - 25255*Power(t2,4) - 
                     171*Power(t2,5)) - 
                  t1*(12349 + 80154*t2 + 59374*Power(t2,2) + 
                     6314*Power(t2,3) - 22841*Power(t2,4) - 
                     14664*Power(t2,5) + 1498*Power(t2,6))) + 
               Power(s2,2)*(-10204 + 6910*t2 + 28824*Power(t2,2) + 
                  24244*Power(t2,3) + 30026*Power(t2,4) + 
                  7470*Power(t2,5) - 6646*Power(t2,6) - 
                  1148*Power(t2,7) + 30*Power(t1,5)*(187 + 18*t2) + 
                  2*Power(t1,4)*
                   (-14740 - 8937*t2 + 3925*Power(t2,2) + 
                     300*Power(t2,3)) - 
                  2*Power(t1,3)*
                   (-40754 - 54298*t2 + 18002*Power(t2,2) + 
                     16305*Power(t2,3) + 600*Power(t2,4)) + 
                  Power(t1,2)*
                   (-101095 - 132158*t2 + 92990*Power(t2,2) + 
                     162242*Power(t2,3) + 27535*Power(t2,4) - 
                     988*Power(t2,5)) + 
                  t1*(53144 + 30917*t2 - 86406*Power(t2,2) - 
                     151710*Power(t2,3) - 75654*Power(t2,4) + 
                     8639*Power(t2,5) + 2778*Power(t2,6)))) - 
            Power(s1,2)*(4665 - 20*Power(t1,7) + 
               Power(t1,6)*(482 - 214*t2) + 3001*t2 - 
               3842*Power(t2,2) - 5105*Power(t2,3) + 3433*Power(t2,4) + 
               2511*Power(t2,5) - 4182*Power(t2,6) + 1057*Power(t2,7) - 
               50*Power(t2,8) + Power(s2,9)*(-1 + 6*t1 + 35*t2) - 
               2*Power(t1,5)*
                (769 + 916*t2 - 486*Power(t2,2) + 70*Power(t2,3)) + 
               Power(t1,4)*(8963 + 19878*t2 + 1678*Power(t2,2) - 
                  2810*Power(t2,3) + 415*Power(t2,4)) - 
               Power(t1,3)*(30091 + 41153*t2 + 15678*Power(t2,2) - 
                  18960*Power(t2,3) - 2785*Power(t2,4) + 
                  337*Power(t2,5)) + 
               Power(t1,2)*(40989 + 39674*t2 + 2346*Power(t2,2) - 
                  3992*Power(t2,3) - 21455*Power(t2,4) + 
                  2698*Power(t2,5) - 40*Power(t2,6)) + 
               t1*(-23429 - 18758*t2 + 11600*Power(t2,2) + 
                  3202*Power(t2,3) - 2729*Power(t2,4) + 
                  13474*Power(t2,5) - 3600*Power(t2,6) + 
                  152*Power(t2,7)) + 
               Power(s2,8)*(262 + 82*Power(t1,2) + 623*t2 + 
                  299*Power(t2,2) - t1*(367 + 511*t2)) - 
               Power(s2,7)*(570 + 406*Power(t1,3) + 5168*t2 + 
                  8450*Power(t2,2) + 3710*Power(t2,3) - 
                  Power(t1,2)*(1300 + 413*t2) + 
                  t1*(262 - 2732*t2 - 3473*Power(t2,2))) + 
               Power(s2,6)*(-2036 - 80*Power(t1,4) - 1383*t2 + 
                  12422*Power(t2,2) + 16294*Power(t2,3) + 
                  6756*Power(t2,4) + 10*Power(t1,3)*(317 + 420*t2) - 
                  2*Power(t1,2)*(4514 + 12810*t2 + 5797*Power(t2,2)) + 
                  t1*(8042 + 28108*t2 + 23601*Power(t2,2) + 
                     1978*Power(t2,3))) + 
               Power(s2,5)*(861 + 19159*t2 + 30939*Power(t2,2) + 
                  21193*Power(t2,3) + 12627*Power(t2,4) + 
                  2399*Power(t2,5) + Power(t1,4)*(-4458 + 460*t2) + 
                  Power(t1,3)*(15529 + 1649*t2 - 13430*Power(t2,2)) + 
                  Power(t1,2)*
                   (-18812 + 11380*t2 + 77469*Power(t2,2) + 
                     41042*Power(t2,3)) - 
                  t1*(-6414 + 41522*t2 + 128311*Power(t2,2) + 
                     116202*Power(t2,3) + 32126*Power(t2,4))) - 
               Power(s2,4)*(-5859 + 200*Power(t1,5) - 6766*t2 + 
                  21339*Power(t2,2) + 24635*Power(t2,3) + 
                  28234*Power(t2,4) + 34263*Power(t2,5) + 
                  10885*Power(t2,6) - 
                  Power(t1,4)*(11823 + 20392*t2 + 60*Power(t2,2)) + 
                  Power(t1,3)*
                   (35403 + 121007*t2 + 56080*Power(t2,2) - 
                     13298*Power(t2,3)) + 
                  Power(t1,2)*
                   (-49493 - 213718*t2 - 186964*Power(t2,2) + 
                     26790*Power(t2,3) + 45496*Power(t2,4)) + 
                  t1*(31082 + 111655*t2 + 77268*Power(t2,2) - 
                     55130*Power(t2,3) - 119817*Power(t2,4) - 
                     43877*Power(t2,5))) - 
               Power(s2,3)*(-4408 + 24642*t2 + 37588*Power(t2,2) + 
                  15124*Power(t2,3) + 22704*Power(t2,4) + 
                  6320*Power(t2,5) - 15544*Power(t2,6) - 
                  5322*Power(t2,7) + Power(t1,5)*(8458 + 80*t2) + 
                  Power(t1,4)*
                   (-37044 - 9524*t2 + 22582*Power(t2,2) + 
                     900*Power(t2,3)) + 
                  2*Power(t1,3)*
                   (38884 + 18607*t2 - 68131*Power(t2,2) - 
                     37351*Power(t2,3) + 1265*Power(t2,4)) - 
                  3*Power(t1,2)*
                   (24500 - 7449*t2 - 99866*Power(t2,2) - 
                     97996*Power(t2,3) - 15116*Power(t2,4) + 
                     4985*Power(t2,5)) + 
                  t1*(28688 - 72948*t2 - 211739*Power(t2,2) - 
                     215280*Power(t2,3) - 92706*Power(t2,4) + 
                     30438*Power(t2,5) + 16987*Power(t2,6))) + 
               s2*(-4044 + 17840*t2 + 25447*Power(t2,2) + 
                  4999*Power(t2,3) + 302*Power(t2,4) + 
                  2262*Power(t2,5) - 1943*Power(t2,6) - 
                  1637*Power(t2,7) + 294*Power(t2,8) - 
                  6*Power(t1,6)*(217 + 40*t2) + 
                  Power(t1,5)*
                   (7368 + 742*t2 - 1248*Power(t2,2) - 
                     300*Power(t2,3)) + 
                  Power(t1,4)*
                   (-31816 - 9624*t2 + 25000*Power(t2,2) + 
                     4730*Power(t2,3) + 550*Power(t2,4)) - 
                  Power(t1,3)*
                   (-62433 + 17551*t2 + 109494*Power(t2,2) + 
                     59108*Power(t2,3) - 2435*Power(t2,4) + 
                     151*Power(t2,5)) + 
                  Power(t1,2)*
                   (-54230 + 77226*t2 + 176475*Power(t2,2) + 
                     98838*Power(t2,3) + 6060*Power(t2,4) - 
                     13320*Power(t2,5) + 195*Power(t2,6)) - 
                  t1*(-21648 + 69568*t2 + 113423*Power(t2,2) + 
                     54932*Power(t2,3) + 11832*Power(t2,4) - 
                     10014*Power(t2,5) - 7593*Power(t2,6) + 
                     588*Power(t2,7))) + 
               Power(s2,2)*(-10668 + 160*Power(t1,6) - 13143*t2 + 
                  28200*Power(t2,2) + 33124*Power(t2,3) + 
                  16540*Power(t2,4) + 18597*Power(t2,5) + 
                  4226*Power(t2,6) - 2748*Power(t2,7) - 
                  364*Power(t2,8) + 
                  2*Power(t1,5)*(3418 + 5231*t2 + 550*Power(t2,2)) + 
                  Power(t1,4)*
                   (-22452 - 79658*t2 - 30740*Power(t2,2) + 
                     3710*Power(t2,3) + 400*Power(t2,4)) - 
                  2*Power(t1,3)*
                   (-26410 - 114850*t2 - 80972*Power(t2,2) + 
                     10827*Power(t2,3) + 9435*Power(t2,4) + 
                     438*Power(t2,5)) + 
                  2*Power(t1,2)*
                   (-37405 - 126449*t2 - 86453*Power(t2,2) + 
                     32880*Power(t2,3) + 51332*Power(t2,4) + 
                     7551*Power(t2,5) + 59*Power(t2,6)) + 
                  t1*(47862 + 105158*t2 + 8821*Power(t2,2) - 
                     71670*Power(t2,3) - 99636*Power(t2,4) - 
                     44058*Power(t2,5) + 2665*Power(t2,6) + 
                     722*Power(t2,7)))) + 
            s1*(2809 + 4704*t2 + 2769*Power(t2,2) - 751*Power(t2,3) - 
               2062*Power(t2,4) + 2865*Power(t2,5) + 1147*Power(t2,6) - 
               1771*Power(t2,7) + 369*Power(t2,8) - 15*Power(t2,9) - 
               2*Power(t1,7)*(57 + 20*t2) + 
               Power(t1,6)*(732 + 778*t2 - 66*Power(t2,2)) + 
               Power(s2,9)*(-11 - 14*Power(t1,2) - 5*t1*(-5 + t2) + 
                  15*t2 + 40*Power(t2,2)) - 
               2*Power(t1,5)*
                (843 + 3209*t2 + 1827*Power(t2,2) - 315*Power(t2,3) + 
                  25*Power(t2,4)) + 
               Power(t1,4)*(9315 + 26671*t2 + 24226*Power(t2,2) + 
                  2452*Power(t2,3) - 1905*Power(t2,4) + 161*Power(t2,5)) \
- Power(t1,3)*(25382 + 58101*t2 + 43509*Power(t2,2) + 
                  16964*Power(t2,3) - 7340*Power(t2,4) - 
                  1753*Power(t2,5) + 177*Power(t2,6)) + 
               Power(t1,2)*(29266 + 61805*t2 + 37598*Power(t2,2) + 
                  10632*Power(t2,3) + 6306*Power(t2,4) - 
                  9147*Power(t2,5) + 292*Power(t2,6) + 56*Power(t2,7)) + 
               t1*(-14945 - 29339*t2 - 16856*Power(t2,2) + 
                  1869*Power(t2,3) - 3848*Power(t2,4) - 
                  3801*Power(t2,5) + 5880*Power(t2,6) - 
                  1033*Power(t2,7) + 25*Power(t2,8)) + 
               Power(s2,8)*(85 + 56*Power(t1,3) + 469*t2 + 
                  676*Power(t2,2) + 261*Power(t2,3) + 
                  Power(t1,2)*(-2 + 157*t2) - 
                  4*t1*(35 + 166*t2 + 141*Power(t2,2))) + 
               Power(s2,7)*(210 + 8*Power(t1,4) - 823*t2 - 
                  4810*Power(t2,2) - 5613*Power(t2,3) - 
                  2008*Power(t2,4) - Power(t1,3)*(928 + 791*t2) + 
                  Power(t1,2)*(1819 + 3258*t2 + 630*Power(t2,2)) + 
                  t1*(-1114 - 1559*t2 + 1838*Power(t2,2) + 
                     2291*Power(t2,3))) + 
               Power(s2,6)*(-873 + Power(t1,4)*(1063 - 144*t2) - 
                  5564*t2 - 4753*Power(t2,2) + 3492*Power(t2,3) + 
                  5418*Power(t2,4) + 2346*Power(t2,5) + 
                  Power(t1,3)*(-892 + 4901*t2 + 4406*Power(t2,2)) - 
                  Power(t1,2)*
                   (1491 + 18488*t2 + 28683*Power(t2,2) + 
                     9094*Power(t2,3)) + 
                  t1*(2239 + 19515*t2 + 35473*Power(t2,2) + 
                     22021*Power(t2,3) + 2426*Power(t2,4))) + 
               Power(s2,5)*(-1511 + 52*Power(t1,5) + 4257*t2 + 
                  22100*Power(t2,2) + 25527*Power(t2,3) + 
                  18130*Power(t2,4) + 9665*Power(t2,5) + 
                  1656*Power(t2,6) + 
                  Power(t1,4)*(-7319 - 8165*t2 + 56*Power(t2,2)) + 
                  Power(t1,3)*
                   (19731 + 38498*t2 + 7582*Power(t2,2) - 
                     8002*Power(t2,3)) + 
                  Power(t1,2)*
                   (-21089 - 48675*t2 - 274*Power(t2,2) + 
                     47903*Power(t2,3) + 20862*Power(t2,4)) - 
                  t1*(-10021 - 13020*t2 + 40247*Power(t2,2) + 
                     86972*Power(t2,3) + 63047*Power(t2,4) + 
                     14587*Power(t2,5))) + 
               Power(s2,4)*(1460 + Power(t1,5)*(4444 - 48*t2) + 
                  12489*t2 + 5577*Power(t2,2) - 10780*Power(t2,3) - 
                  7454*Power(t2,4) - 11266*Power(t2,5) - 
                  13140*Power(t2,6) - 3415*Power(t2,7) + 
                  Power(t1,4)*
                   (-9611 + 12303*t2 + 17456*Power(t2,2) + 
                     568*Power(t2,3)) + 
                  Power(t1,3)*
                   (7268 - 61311*t2 - 121705*Power(t2,2) - 
                     41460*Power(t2,3) + 4516*Power(t2,4)) + 
                  Power(t1,2)*
                   (1194 + 109064*t2 + 227458*Power(t2,2) + 
                     144406*Power(t2,3) - 3306*Power(t2,4) - 
                     15747*Power(t2,5)) + 
                  t1*(-4625 - 71307*t2 - 118304*Power(t2,2) - 
                     72463*Power(t2,3) + 8569*Power(t2,4) + 
                     43848*Power(t2,5) + 14092*Power(t2,6))) - 
               Power(s2,3)*(-5838 + 96*Power(t1,6) + 5531*t2 + 
                  35926*Power(t2,2) + 26365*Power(t2,3) + 
                  7720*Power(t2,4) + 11887*Power(t2,5) + 
                  3532*Power(t2,6) - 4419*Power(t2,7) - 
                  1216*Power(t2,8) + 
                  2*Power(t1,5)*(5725 + 6182*t2 + 408*Power(t2,2)) + 
                  2*Power(t1,4)*
                   (-20294 - 39346*t2 - 10343*Power(t2,2) + 
                     5133*Power(t2,3) + 280*Power(t2,4)) + 
                  Power(t1,3)*
                   (68198 + 160183*t2 + 59066*Power(t2,2) - 
                     71242*Power(t2,3) - 32810*Power(t2,4) + 
                     79*Power(t2,5)) + 
                  Power(t1,2)*
                   (-63465 - 112488*t2 + 22644*Power(t2,2) + 
                     171058*Power(t2,3) + 136199*Power(t2,4) + 
                     19534*Power(t2,5) - 3306*Power(t2,6)) + 
                  t1*(30206 + 13299*t2 - 93958*Power(t2,2) - 
                     128641*Power(t2,3) - 104750*Power(t2,4) - 
                     43233*Power(t2,5) + 7278*Power(t2,6) + 
                     3883*Power(t2,7))) + 
               Power(s2,2)*(-2899 - 15708*t2 - 269*Power(t2,2) + 
                  28556*Power(t2,3) + 18175*Power(t2,4) + 
                  5428*Power(t2,5) + 6029*Power(t2,6) + 
                  1220*Power(t2,7) - 636*Power(t2,8) - 56*Power(t2,9) + 
                  8*Power(t1,6)*(389 + 46*t2) + 
                  2*Power(t1,5)*
                   (-5145 + 1121*t2 + 3282*Power(t2,2) + 
                     420*Power(t2,3)) + 
                  Power(t1,4)*
                   (20191 - 30908*t2 - 68054*Power(t2,2) - 
                     19394*Power(t2,3) + 315*Power(t2,4) + 
                     120*Power(t2,5)) + 
                  Power(t1,3)*
                   (-15508 + 112319*t2 + 207372*Power(t2,2) + 
                     100516*Power(t2,3) - 5172*Power(t2,4) - 
                     5021*Power(t2,5) - 290*Power(t2,6)) + 
                  Power(t1,2)*
                   (-5343 - 153292*t2 - 211047*Power(t2,2) - 
                     102132*Power(t2,3) + 18957*Power(t2,4) + 
                     32668*Power(t2,5) + 4079*Power(t2,6) + 
                     164*Power(t2,7)) + 
                  t1*(10727 + 84699*t2 + 65977*Power(t2,2) - 
                     10957*Power(t2,3) - 26879*Power(t2,4) - 
                     33261*Power(t2,5) - 13391*Power(t2,6) + 
                     423*Power(t2,7) + 62*Power(t2,8))) + 
               s2*(-5828 - 48*Power(t1,7) + 5122*t2 + 
                  27354*Power(t2,2) + 20543*Power(t2,3) + 
                  4165*Power(t2,4) + 1414*Power(t2,5) + 
                  1614*Power(t2,6) - 619*Power(t2,7) - 457*Power(t2,8) + 
                  68*Power(t2,9) - 
                  2*Power(t1,6)*(1172 + 451*t2 + 112*Power(t2,2)) - 
                  2*Power(t1,5)*
                   (-5705 - 7228*t2 - 711*Power(t2,2) + 
                     154*Power(t2,3) + 50*Power(t2,4)) + 
                  Power(t1,4)*
                   (-37835 - 48931*t2 - 6784*Power(t2,2) + 
                     15422*Power(t2,3) + 1465*Power(t2,4) + 
                     249*Power(t2,5)) + 
                  Power(t1,3)*
                   (68883 + 54116*t2 - 39792*Power(t2,2) - 
                     65504*Power(t2,3) - 26507*Power(t2,4) + 
                     1380*Power(t2,5) - 220*Power(t2,6)) + 
                  Power(t1,2)*
                   (-63753 - 6789*t2 + 117292*Power(t2,2) + 
                     110563*Power(t2,3) + 45965*Power(t2,4) + 
                     5139*Power(t2,5) - 4154*Power(t2,6) + 
                     161*Power(t2,7)) - 
                  t1*(-29534 + 17057*t2 + 100463*Power(t2,2) + 
                     78132*Power(t2,3) + 28902*Power(t2,4) + 
                     8277*Power(t2,5) - 2117*Power(t2,6) - 
                     2298*Power(t2,7) + 158*Power(t2,8))))) - 
         Power(s,2)*(-1341 + 7976*t1 - 20212*Power(t1,2) + 
            27817*Power(t1,3) - 22144*Power(t1,4) + 10273*Power(t1,5) - 
            2837*Power(t1,6) + 514*Power(t1,7) - 46*Power(t1,8) - 
            2394*t2 + 13418*t1*t2 - 33503*Power(t1,2)*t2 + 
            46668*Power(t1,3)*t2 - 38300*Power(t1,4)*t2 + 
            18461*Power(t1,5)*t2 - 4890*Power(t1,6)*t2 + 
            560*Power(t1,7)*t2 - 20*Power(t1,8)*t2 - 734*Power(t2,2) + 
            5904*t1*Power(t2,2) - 20093*Power(t1,2)*Power(t2,2) + 
            34524*Power(t1,3)*Power(t2,2) - 
            32069*Power(t1,4)*Power(t2,2) + 
            15519*Power(t1,5)*Power(t2,2) - 
            3174*Power(t1,6)*Power(t2,2) + 144*Power(t1,7)*Power(t2,2) + 
            112*Power(t2,3) + 1192*t1*Power(t2,3) - 
            8138*Power(t1,2)*Power(t2,3) + 
            17233*Power(t1,3)*Power(t2,3) - 
            15084*Power(t1,4)*Power(t2,3) + 
            4776*Power(t1,5)*Power(t2,3) - 248*Power(t1,6)*Power(t2,3) + 
            6*Power(t1,7)*Power(t2,3) - 52*Power(t2,4) + 
            1113*t1*Power(t2,4) - 5490*Power(t1,2)*Power(t2,4) + 
            6783*Power(t1,3)*Power(t2,4) - 
            1410*Power(t1,4)*Power(t2,4) - 553*Power(t1,5)*Power(t2,4) + 
            23*Power(t1,6)*Power(t2,4) - 238*Power(t2,5) + 
            2484*t1*Power(t2,5) - 3213*Power(t1,2)*Power(t2,5) - 
            1048*Power(t1,3)*Power(t2,5) + 
            1350*Power(t1,4)*Power(t2,5) - 55*Power(t1,5)*Power(t2,5) - 
            716*Power(t2,6) + 913*t1*Power(t2,6) + 
            1617*Power(t1,2)*Power(t2,6) - 942*Power(t1,3)*Power(t2,6) - 
            31*Power(t1,4)*Power(t2,6) - Power(t1,5)*Power(t2,6) + 
            44*Power(t2,7) - 1052*t1*Power(t2,7) + 
            210*Power(t1,2)*Power(t2,7) + 123*Power(t1,3)*Power(t2,7) + 
            4*Power(t1,4)*Power(t2,7) + 277*Power(t2,8) + 
            143*t1*Power(t2,8) - 78*Power(t1,2)*Power(t2,8) - 
            6*Power(t1,3)*Power(t2,8) - 84*Power(t2,9) + 
            6*t1*Power(t2,9) + 4*Power(t1,2)*Power(t2,9) + 
            6*Power(t2,10) - t1*Power(t2,10) + 
            Power(s1,10)*(28*Power(s2,3) + t1 + 
               2*Power(s2,2)*(-7 + 18*t1 - 4*t2) + s2*(1 - 16*t1 + 2*t2)\
) + Power(s2,10)*t2*(Power(t1,2) - 6*t2*(1 + t2) + t1*(-1 + 5*t2)) - 
            Power(s2,9)*(-2 + 21*t2 + 72*Power(t2,2) + 75*Power(t2,3) + 
               23*Power(t2,4) + Power(t1,3)*(1 + 4*t2) + 
               Power(t1,2)*(-4 + 24*t2 + 51*Power(t2,2)) - 
               t1*(-5 + 49*t2 + 130*Power(t2,2) + 78*Power(t2,3))) + 
            Power(s2,8)*(-25 + Power(t1,4)*(4 - 3*t2) - 71*t2 + 
               183*Power(t2,2) + 834*Power(t2,3) + 814*Power(t2,4) + 
               236*Power(t2,5) + 
               Power(t1,3)*(11 + 219*t2 + 119*Power(t2,2)) + 
               Power(t1,2)*(-59 - 440*t2 - 307*Power(t2,2) + 
                  67*Power(t2,3)) + 
               t1*(69 + 295*t2 - 17*Power(t2,2) - 745*Power(t2,3) - 
                  419*Power(t2,4))) + 
            Power(s2,7)*(17 + 3*Power(t1,5) + 584*t2 + 
               2020*Power(t2,2) + 1398*Power(t2,3) - 840*Power(t2,4) - 
               1242*Power(t2,5) - 397*Power(t2,6) + 
               Power(t1,4)*(-168 - 281*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(448 + 17*t2 - 1311*Power(t2,2) - 
                  660*Power(t2,3)) + 
               Power(t1,2)*(-387 + 1209*t2 + 4973*Power(t2,2) + 
                  4556*Power(t2,3) + 876*Power(t2,4)) + 
               t1*(87 - 1529*t2 - 5672*Power(t2,2) - 5807*Power(t2,3) - 
                  1825*Power(t2,4) + 172*Power(t2,5))) + 
            Power(s2,6)*(Power(t1,5)*(162 - 7*t2) + 
               2*Power(t1,4)*
                (65 + 1475*t2 + 1075*Power(t2,2) + 34*Power(t2,3)) + 
               2*Power(t1,3)*
                (-731 - 4561*t2 - 4869*Power(t2,2) - 572*Power(t2,3) + 
                  582*Power(t2,4)) + 
               Power(t1,2)*(2084 + 10554*t2 + 11198*Power(t2,2) - 
                  3983*Power(t2,3) - 8453*Power(t2,4) - 
                  2535*Power(t2,5)) + 
               t1*(-1112 - 5092*t2 - 1387*Power(t2,2) + 
                  13623*Power(t2,3) + 17427*Power(t2,4) + 
                  8168*Power(t2,5) + 1261*Power(t2,6)) + 
               3*(66 + 239*t2 - 730*Power(t2,2) - 2541*Power(t2,3) - 
                  2489*Power(t2,4) - 1046*Power(t2,5) - 
                  173*Power(t2,6) + 14*Power(t2,7))) - 
            Power(s2,5)*(-67 + 2*Power(t1,6) + 2837*t2 + 
               8870*Power(t2,2) + 6352*Power(t2,3) - 1684*Power(t2,4) - 
               3718*Power(t2,5) - 2679*Power(t2,6) - 1530*Power(t2,7) - 
               319*Power(t2,8) + 
               Power(t1,5)*(1679 + 2469*t2 + 208*Power(t2,2)) + 
               Power(t1,4)*(-5468 - 6351*t2 + 3302*Power(t2,2) + 
                  4247*Power(t2,3) + 218*Power(t2,4)) + 
               Power(t1,3)*(6608 + 2314*t2 - 25503*Power(t2,2) - 
                  32480*Power(t2,3) - 8795*Power(t2,4) + 
                  644*Power(t2,5)) + 
               Power(t1,2)*(-3605 + 8098*t2 + 51194*Power(t2,2) + 
                  64646*Power(t2,3) + 29451*Power(t2,4) - 
                  692*Power(t2,5) - 2309*Power(t2,6)) + 
               t1*(851 - 9367*t2 - 38006*Power(t2,2) - 
                  41668*Power(t2,3) - 16381*Power(t2,4) + 
                  3620*Power(t2,5) + 6700*Power(t2,6) + 1766*Power(t2,7)\
)) + Power(s2,4)*(-678 - 2641*t2 + 4343*Power(t2,2) + 
               15394*Power(t2,3) + 11572*Power(t2,4) + 
               3767*Power(t2,5) + 2871*Power(t2,6) + 1030*Power(t2,7) - 
               638*Power(t2,8) - 200*Power(t2,9) + 
               Power(t1,6)*(979 + 240*t2) + 
               Power(t1,5)*(-1031 + 6961*t2 + 5875*Power(t2,2) + 
                  636*Power(t2,3)) + 
               Power(t1,4)*(-4394 - 33953*t2 - 42795*Power(t2,2) - 
                  10846*Power(t2,3) + 2378*Power(t2,4) + 
                  189*Power(t2,5)) + 
               Power(t1,3)*(10690 + 62755*t2 + 88087*Power(t2,2) + 
                  29123*Power(t2,3) - 17239*Power(t2,4) - 
                  8317*Power(t2,5) - 45*Power(t2,6)) - 
               Power(t1,2)*(9647 + 54585*t2 + 64018*Power(t2,2) + 
                  3205*Power(t2,3) - 44165*Power(t2,4) - 
                  33506*Power(t2,5) - 4999*Power(t2,6) + 
                  691*Power(t2,7)) + 
               t1*(4081 + 21223*t2 + 8541*Power(t2,2) - 
                  30403*Power(t2,3) - 39465*Power(t2,4) - 
                  28085*Power(t2,5) - 10762*Power(t2,6) + 
                  977*Power(t2,7) + 747*Power(t2,8))) + 
            Power(s2,3)*(-913 - 100*Power(t1,7) + 4604*t2 + 
               13986*Power(t2,2) + 5174*Power(t2,3) - 
               8823*Power(t2,4) - 4720*Power(t2,5) + 835*Power(t2,6) - 
               764*Power(t2,7) - 738*Power(t2,8) + 82*Power(t2,9) + 
               29*Power(t2,10) - 
               4*Power(t1,6)*(832 + 943*t2 + 159*Power(t2,2)) + 
               Power(t1,5)*(14525 + 20908*t2 + 4304*Power(t2,2) - 
                  3270*Power(t2,3) - 509*Power(t2,4)) + 
               Power(t1,4)*(-25920 - 39129*t2 + 5939*Power(t2,2) + 
                  34346*Power(t2,3) + 11502*Power(t2,4) + 
                  5*Power(t2,5) - 47*Power(t2,6)) + 
               Power(t1,3)*(25680 + 21425*t2 - 57397*Power(t2,2) - 
                  100768*Power(t2,3) - 55152*Power(t2,4) - 
                  3177*Power(t2,5) + 2179*Power(t2,6) + 76*Power(t2,7)) \
+ Power(t1,2)*(-15373 + 14069*t2 + 95425*Power(t2,2) + 
                  109076*Power(t2,3) + 63557*Power(t2,4) + 
                  11355*Power(t2,5) - 8507*Power(t2,6) - 
                  2044*Power(t2,7) + 18*Power(t2,8)) - 
               t1*(-5429 + 18105*t2 + 61664*Power(t2,2) + 
                  44781*Power(t2,3) + 10098*Power(t2,4) + 
                  3481*Power(t2,5) - 3766*Power(t2,6) - 
                  4483*Power(t2,7) - 287*Power(t2,8) + 76*Power(t2,9))) \
+ s2*(-32*Power(t1,8) - 12*Power(t1,7)*(68 + 31*t2 + 8*Power(t2,2)) + 
               Power(t1,6)*(5176 + 4918*t2 + 978*Power(t2,2) - 
                  368*Power(t2,3) - 66*Power(t2,4)) + 
               Power(t1,5)*(-15793 - 11803*t2 + 1322*Power(t2,2) + 
                  6666*Power(t2,3) + 1459*Power(t2,4) + 
                  55*Power(t2,5) - 2*Power(t2,6)) + 
               Power(t1,4)*(28098 + 1625*t2 - 33120*Power(t2,2) - 
                  32199*Power(t2,3) - 12836*Power(t2,4) - 
                  55*Power(t2,5) + 98*Power(t2,6) + 7*Power(t2,7)) + 
               Power(t1,3)*(-29763 + 26680*t2 + 92009*Power(t2,2) + 
                  69748*Power(t2,3) + 28219*Power(t2,4) + 
                  4400*Power(t2,5) - 1865*Power(t2,6) - 
                  50*Power(t2,7) - 10*Power(t2,8)) + 
               Power(1 + t2,2)*
                (1101 - 5442*t2 - 2607*Power(t2,2) + 
                  1035*Power(t2,3) - 353*Power(t2,4) - 
                  126*Power(t2,5) - 126*Power(t2,6) + 
                  124*Power(t2,7) - 23*Power(t2,8) + Power(t2,9)) + 
               Power(t1,2)*(18745 - 36406*t2 - 107647*Power(t2,2) - 
                  74842*Power(t2,3) - 26096*Power(t2,4) - 
                  6482*Power(t2,5) + 111*Power(t2,6) + 
                  1290*Power(t2,7) - 105*Power(t2,8) + 8*Power(t2,9)) + 
               t1*(-6716 + 18598*t2 + 58866*Power(t2,2) + 
                  40910*Power(t2,3) + 9342*Power(t2,4) + 
                  2905*Power(t2,5) + 1220*Power(t2,6) - 
                  238*Power(t2,7) - 444*Power(t2,8) + 89*Power(t2,9) - 
                  4*Power(t2,10))) + 
            Power(s2,2)*(1674 + 4013*t2 - 5498*Power(t2,2) - 
               19905*Power(t2,3) - 14373*Power(t2,4) - 
               1219*Power(t2,5) + 375*Power(t2,6) - 703*Power(t2,7) + 
               67*Power(t2,8) + 54*Power(t2,9) - 5*Power(t2,10) + 
               10*Power(t1,7)*(98 + 25*t2) + 
               2*Power(t1,6)*
                (-1322 + 576*t2 + 977*Power(t2,2) + 218*Power(t2,3)) + 
               Power(t1,5)*(778 - 18615*t2 - 24694*Power(t2,2) - 
                  7002*Power(t2,3) + 264*Power(t2,4) + 107*Power(t2,5)) \
+ 2*Power(t1,4)*(3903 + 30585*t2 + 38774*Power(t2,2) + 
                  15934*Power(t2,3) - 1841*Power(t2,4) - 
                  1342*Power(t2,5) - 77*Power(t2,6) + Power(t2,7)) - 
               2*Power(t1,3)*
                (8727 + 44787*t2 + 45148*Power(t2,2) + 
                  13593*Power(t2,3) - 7990*Power(t2,4) - 
                  7689*Power(t2,5) - 918*Power(t2,6) + 
                  29*Power(t2,7) + 3*Power(t2,8)) + 
               Power(t1,2)*(17392 + 66675*t2 + 32036*Power(t2,2) - 
                  35285*Power(t2,3) - 35357*Power(t2,4) - 
                  21517*Power(t2,5) - 6420*Power(t2,6) + 
                  533*Power(t2,7) + 145*Power(t2,8) + 6*Power(t2,9)) + 
               t1*(-8532 - 25071*t2 + 9046*Power(t2,2) + 
                  56971*Power(t2,3) + 37289*Power(t2,4) + 
                  9328*Power(t2,5) + 4798*Power(t2,6) + 
                  487*Power(t2,7) - 439*Power(t2,8) - 35*Power(t2,9) - 
                  2*Power(t2,10))) + 
            Power(s1,9)*(-1 - 11*Power(t1,2) + 
               Power(s2,3)*(-34 + 392*t1 - 378*t2) + t1*(20 - 6*t2) - 
               3*t2 + s2*(19 + 14*Power(t1,2) + 17*t2 - 
                  18*Power(t2,2) + 2*t1*(-55 + 63*t2)) + 
               Power(s2,2)*(-39 + 72*Power(t1,2) + 156*t2 + 
                  74*Power(t2,2) - 4*t1*(23 + 82*t2))) + 
            Power(s1,8)*(-5 - 255*Power(s2,5) + 38*Power(t1,3) + 
               2*Power(s2,4)*(5 + 469*t1 - 180*t2) - 13*t2 + 
               24*Power(t2,2) + Power(t1,2)*(19 + 48*t2) + 
               t1*(60 - 146*t2 + 17*Power(t2,2)) + 
               Power(s2,3)*(-35 + 764*Power(t1,2) + 592*t2 + 
                  2088*Power(t2,2) - t1*(692 + 3571*t2)) + 
               s2*(-31 + 93*Power(t1,3) - 188*t2 - 186*Power(t2,2) + 
                  73*Power(t2,3) - 2*Power(t1,2)*(274 + 77*t2) + 
                  t1*(370 + 1021*t2 - 434*Power(t2,2))) + 
               Power(s2,2)*(214 + 36*Power(t1,3) + 
                  Power(t1,2)*(671 - 504*t2) + 562*t2 - 
                  739*Power(t2,2) - 304*Power(t2,3) + 
                  t1*(-1283 + 520*t2 + 1306*Power(t2,2)))) + 
            Power(s1,7)*(42 - 515*Power(s2,6) + 5*Power(t1,4) + 46*t2 + 
               143*Power(t2,2) - 90*Power(t2,3) - 
               4*Power(t1,3)*(53 + 55*t2) + 
               3*Power(s2,5)*(313 + 234*t1 + 513*t2) + 
               Power(t1,2)*(761 - 203*t2 - 56*Power(t2,2)) - 
               t1*(310 + 683*t2 - 540*Power(t2,2) + 34*Power(t2,3)) + 
               Power(s2,4)*(850 + 2100*Power(t1,2) + 1338*t2 + 
                  2862*Power(t2,2) - t1*(3967 + 8288*t2)) + 
               Power(s2,3)*(-13 + 456*Power(t1,3) + 
                  Power(t1,2)*(2578 - 5595*t2) + 1028*t2 - 
                  3253*Power(t2,2) - 6413*Power(t2,3) + 
                  2*t1*(-1839 + 2262*t2 + 7034*Power(t2,2))) + 
               s2*(-175 + 80*Power(t1,4) + 
                  Power(t1,3)*(272 - 549*t2) + 29*t2 + 
                  682*Power(t2,2) + 763*Power(t2,3) - 
                  176*Power(t2,4) + 
                  Power(t1,2)*(-1456 + 3481*t2 + 622*Power(t2,2)) + 
                  t1*(1375 - 1965*t2 - 4059*Power(t2,2) + 
                     858*Power(t2,3))) - 
               Power(s2,2)*(404 + 2370*t2 + 2992*Power(t2,2) - 
                  1986*Power(t2,3) - 728*Power(t2,4) + 
                  4*Power(t1,3)*(-395 + 32*t2) + 
                  Power(t1,2)*(5042 + 5651*t2 - 1490*Power(t2,2)) + 
                  t1*(-3478 - 10949*t2 + 929*Power(t2,2) + 
                     2980*Power(t2,3)))) + 
            Power(s1,6)*(-5 - 418*Power(s2,7) - 181*t2 + 
               14*Power(t2,2) - 613*Power(t2,3) + 210*Power(t2,4) + 
               5*Power(t1,4)*(30 + t2) + 
               Power(s2,6)*(2241 - 250*t1 + 3398*t2) + 
               Power(t1,3)*(-46 + 1075*t2 + 506*Power(t2,2)) + 
               Power(t1,2)*(580 - 4308*t2 + 747*Power(t2,2) - 
                  52*Power(t2,3)) + 
               t1*(-269 + 1092*t2 + 3077*Power(t2,2) - 
                  1288*Power(t2,3) + 56*Power(t2,4)) + 
               Power(s2,5)*(841 + 2517*Power(t1,2) - 3856*t2 - 
                  3410*Power(t2,2) - t1*(6306 + 6547*t2)) + 
               Power(s2,4)*(-3060 + 1330*Power(t1,3) - 6291*t2 - 
                  9096*Power(t2,2) - 9812*Power(t2,3) - 
                  Power(t1,2)*(549 + 14557*t2) + 
                  t1*(2176 + 27690*t2 + 30975*Power(t2,2))) + 
               Power(s2,3)*(-2330 + 56*Power(t1,4) + 
                  Power(t1,3)*(6595 - 2457*t2) - 2925*t2 - 
                  6719*Power(t2,2) + 9072*Power(t2,3) + 
                  12299*Power(t2,4) + 
                  Power(t1,2)*
                   (-20781 - 20593*t2 + 17367*Power(t2,2)) + 
                  t1*(15149 + 31534*t2 - 11668*Power(t2,2) - 
                     31445*Power(t2,3))) + 
               s2*(1095 - 28*Power(t1,5) + 
                  Power(t1,4)*(931 - 259*t2) + 1642*t2 + 
                  442*Power(t2,2) - 1129*Power(t2,3) - 
                  1771*Power(t2,4) + 280*Power(t2,5) + 
                  Power(t1,3)*(-6369 - 2312*t2 + 1299*Power(t2,2)) + 
                  Power(t1,2)*
                   (10435 + 10860*t2 - 9606*Power(t2,2) - 
                     1274*Power(t2,3)) + 
                  t1*(-6243 - 9997*t2 + 3855*Power(t2,2) + 
                     9162*Power(t2,3) - 1078*Power(t2,4))) + 
               Power(s2,2)*(191 + 2397*t2 + 9683*Power(t2,2) + 
                  8388*Power(t2,3) - 3374*Power(t2,4) - 
                  1120*Power(t2,5) + 7*Power(t1,4)*(159 + 8*t2) + 
                  3*Power(t1,3)*(45 - 3257*t2 + 14*Power(t2,2)) + 
                  Power(t1,2)*
                   (-5473 + 31502*t2 + 19786*Power(t2,2) - 
                     2382*Power(t2,3)) + 
                  t1*(4008 - 21579*t2 - 38908*Power(t2,2) - 
                     229*Power(t2,3) + 4284*Power(t2,4)))) - 
            Power(s1,5)*(656 + 138*Power(s2,8) + 
               2*Power(s2,7)*(-968 + 328*t1 - 1309*t2) + 79*t2 - 
               530*Power(t2,2) + 862*Power(t2,3) - 1529*Power(t2,4) + 
               336*Power(t2,5) - 4*Power(t1,5)*(1 + 9*t2) + 
               Power(t1,4)*(1711 + 484*t2 + 109*Power(t2,2)) + 
               Power(s2,6)*(1717 - 1405*Power(t1,2) + 
                  t1*(2975 - 313*t2) + 11553*t2 + 9261*Power(t2,2)) + 
               Power(t1,3)*(-6513 + 171*t2 + 2578*Power(t2,2) + 
                  554*Power(t2,3)) + 
               Power(t1,2)*(7414 + 3871*t2 - 10809*Power(t2,2) + 
                  1277*Power(t2,3) - 186*Power(t2,4)) + 
               t1*(-3670 - 1724*t2 + 686*Power(t2,2) + 
                  7383*Power(t2,3) - 2044*Power(t2,4) + 70*Power(t2,5)) \
- Power(s2,5)*(-6883 + 1740*Power(t1,3) - 9111*t2 + 3166*Power(t2,2) + 
                  2571*Power(t2,3) - Power(t1,2)*(7757 + 16164*t2) + 
                  t1*(14987 + 42234*t2 + 24343*Power(t2,2))) + 
               Power(s2,4)*(753 - 168*Power(t1,4) - 14228*t2 - 
                  17247*Power(t2,2) - 25698*Power(t2,3) - 
                  18930*Power(t2,4) + Power(t1,3)*(-8460 + 6978*t2) + 
                  Power(t1,2)*(30107 + 2756*t2 - 41868*Power(t2,2)) + 
                  t1*(-20332 + 2139*t2 + 79174*Power(t2,2) + 
                     64132*Power(t2,3))) + 
               Power(s2,3)*(-6389 - 16101*t2 - 16468*Power(t2,2) - 
                  20138*Power(t2,3) + 14987*Power(t2,4) + 
                  15477*Power(t2,5) + 21*Power(t1,4)*(-214 + 3*t2) - 
                  18*Power(t1,3)*(-676 - 2150*t2 + 293*Power(t2,2)) + 
                  Power(t1,2)*
                   (-16211 - 124667*t2 - 66801*Power(t2,2) + 
                     29668*Power(t2,3)) + 
                  t1*(16068 + 92699*t2 + 108364*Power(t2,2) - 
                     14413*Power(t2,3) - 43826*Power(t2,4))) + 
               s2*(1186 + 5865*t2 + 5260*Power(t2,2) + 
                  1659*Power(t2,3) - 586*Power(t2,4) - 
                  2625*Power(t2,5) + 308*Power(t2,6) - 
                  3*Power(t1,5)*(230 + 49*t2) + 
                  Power(t1,4)*(2051 + 4568*t2 - 168*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1691 - 33298*t2 - 6975*Power(t2,2) + 
                     1505*Power(t2,3)) + 
                  Power(t1,2)*
                   (1595 + 53279*t2 + 32964*Power(t2,2) - 
                     15100*Power(t2,3) - 1442*Power(t2,4)) - 
                  t1*(1954 + 32464*t2 + 29165*Power(t2,2) - 
                     2306*Power(t2,3) - 13049*Power(t2,4) + 
                     910*Power(t2,5))) + 
               Power(s2,2)*(-4761 + 42*Power(t1,5) + 471*t2 + 
                  4640*Power(t2,2) + 20422*Power(t2,3) + 
                  14054*Power(t2,4) - 3794*Power(t2,5) - 
                  1148*Power(t2,6) + 
                  Power(t1,4)*(-6317 + 4371*t2 + 252*Power(t2,2)) + 
                  Power(t1,3)*
                   (34458 + 6857*t2 - 24943*Power(t2,2) - 
                     456*Power(t2,3)) + 
                  Power(t1,2)*
                   (-52249 - 39039*t2 + 81207*Power(t2,2) + 
                     37795*Power(t2,3) - 2154*Power(t2,4)) + 
                  t1*(28184 + 27798*t2 - 53740*Power(t2,2) - 
                     75649*Power(t2,3) - 3355*Power(t2,4) + 
                     4004*Power(t2,5)))) + 
            Power(s1,4)*(998 - 3*Power(s2,9) + 5*Power(t1,6) + 
               2587*t2 - 70*Power(t2,2) - 1106*Power(t2,3) + 
               2840*Power(t2,4) - 2419*Power(t2,5) + 378*Power(t2,6) + 
               Power(s2,8)*(644 - 342*t1 + 794*t2) + 
               Power(t1,5)*(-347 + 67*t2 - 125*Power(t2,2)) + 
               Power(t1,4)*(2878 + 7222*t2 + 175*Power(t2,2) + 
                  290*Power(t2,3)) + 
               Power(t1,3)*(-6289 - 26316*t2 + 558*Power(t2,2) + 
                  3925*Power(t2,3) + 220*Power(t2,4)) + 
               Power(t1,2)*(7403 + 27519*t2 + 11473*Power(t2,2) - 
                  15166*Power(t2,3) + 955*Power(t2,4) - 164*Power(t2,5)\
) + t1*(-4360 - 13408*t2 - 3891*Power(t2,2) - 3028*Power(t2,3) + 
                  10465*Power(t2,4) - 2100*Power(t2,5) + 56*Power(t2,6)\
) + Power(s2,7)*(-2331 + 234*Power(t1,2) - 9548*t2 - 
                  6694*Power(t2,2) + t1*(722 + 3099*t2)) + 
               Power(s2,6)*(-3858 + 1220*Power(t1,3) + 4590*t2 + 
                  23025*Power(t2,2) + 13406*Power(t2,3) - 
                  3*Power(t1,2)*(2651 + 2743*t2) + 
                  t1*(12501 + 20323*t2 + 2687*Power(t2,2))) + 
               Power(s2,5)*(6322 + 180*Power(t1,4) + 
                  Power(t1,3)*(2900 - 8370*t2) + 35615*t2 + 
                  31266*Power(t2,2) + 8646*Power(t2,3) + 
                  2280*Power(t2,4) + 
                  Power(t1,2)*(-8236 + 36010*t2 + 42011*Power(t2,2)) - 
                  t1*(2967 + 73646*t2 + 112982*Power(t2,2) + 
                     47708*Power(t2,3))) - 
               Power(s2,4)*(-11442 - 8520*t2 + 21877*Power(t2,2) + 
                  22048*Power(t2,3) + 39580*Power(t2,4) + 
                  22440*Power(t2,5) + 5*Power(t1,4)*(-1388 + 81*t2) + 
                  Power(t1,3)*(33517 + 46641*t2 - 14325*Power(t2,2)) + 
                  Power(t1,2)*
                   (-63302 - 167166*t2 - 23159*Power(t2,2) + 
                     64593*Power(t2,3)) + 
                  t1*(47945 + 117449*t2 + 27454*Power(t2,2) - 
                     120113*Power(t2,3) - 80455*Power(t2,4))) + 
               Power(s2,3)*(1230 + 135*Power(t1,5) - 35652*t2 - 
                  40508*Power(t2,2) - 37126*Power(t2,3) - 
                  33435*Power(t2,4) + 15554*Power(t2,5) + 
                  12943*Power(t2,6) - 
                  15*Power(t1,4)*(-596 + 1161*t2 + 23*Power(t2,2)) + 
                  Power(t1,3)*
                   (-51496 + 46847*t2 + 90567*Power(t2,2) - 
                     5570*Power(t2,3)) + 
                  Power(t1,2)*
                   (68143 - 67249*t2 - 297639*Power(t2,2) - 
                     114878*Power(t2,3) + 30180*Power(t2,4)) + 
                  t1*(-26037 + 76894*t2 + 222294*Power(t2,2) + 
                     196159*Power(t2,3) - 6945*Power(t2,4) - 
                     39257*Power(t2,5))) + 
               s2*(-4942 - 90*Power(t1,6) + 7690*t2 + 
                  12044*Power(t2,2) + 7866*Power(t2,3) + 
                  2820*Power(t2,4) + 901*Power(t2,5) - 
                  2597*Power(t2,6) + 238*Power(t2,7) + 
                  Power(t1,5)*(1817 - 1755*t2 - 300*Power(t2,2)) + 
                  Power(t1,4)*
                   (-19736 + 5811*t2 + 9000*Power(t2,2) + 
                     325*Power(t2,3)) + 
                  Power(t1,3)*
                   (51382 - 4490*t2 - 71573*Power(t2,2) - 
                     10270*Power(t2,3) + 745*Power(t2,4)) + 
                  Power(t1,2)*
                   (-56783 + 11412*t2 + 109427*Power(t2,2) + 
                     53666*Power(t2,3) - 14885*Power(t2,4) - 
                     854*Power(t2,5)) - 
                  t1*(-27428 + 15409*t2 + 66804*Power(t2,2) + 
                     44428*Power(t2,3) + 3420*Power(t2,4) - 
                     12270*Power(t2,5) + 546*Power(t2,6))) + 
               Power(s2,2)*(-11052 - 21549*t2 - 711*Power(t2,2) + 
                  1967*Power(t2,3) + 24835*Power(t2,4) + 
                  14744*Power(t2,5) - 2856*Power(t2,6) - 
                  784*Power(t2,7) + 5*Power(t1,5)*(944 + 81*t2) + 
                  Power(t1,4)*
                   (-21616 - 31256*t2 + 6380*Power(t2,2) + 
                     450*Power(t2,3)) + 
                  Power(t1,3)*
                   (48582 + 165070*t2 + 27608*Power(t2,2) - 
                     33720*Power(t2,3) - 950*Power(t2,4)) - 
                  Power(t1,2)*
                   (61977 + 244271*t2 + 109156*Power(t2,2) - 
                     111181*Power(t2,3) - 43000*Power(t2,4) + 
                     982*Power(t2,5)) + 
                  t1*(41809 + 128157*t2 + 78430*Power(t2,2) - 
                     68243*Power(t2,3) - 88030*Power(t2,4) - 
                     5633*Power(t2,5) + 2408*Power(t2,6)))) + 
            Power(s1,2)*(-5727 + Power(s2,10)*(-3 + 2*t1 - 16*t2) - 
               267*t2 + 6176*Power(t2,2) + 3207*Power(t2,3) - 
               3065*Power(t2,4) - 653*Power(t2,5) + 3594*Power(t2,6) - 
               1543*Power(t2,7) + 150*Power(t2,8) - 
               6*Power(t1,7)*(7 + 10*t2) + 
               Power(t1,6)*(-796 + 450*t2 - 42*Power(t2,2)) + 
               Power(t1,5)*(10748 + 5812*t2 - 4620*Power(t2,2) + 
                  56*Power(t2,3) - 90*Power(t2,4)) + 
               Power(t1,4)*(-41672 - 22455*t2 + 17962*Power(t2,2) + 
                  13540*Power(t2,3) - 1190*Power(t2,4) + 
                  205*Power(t2,5)) + 
               Power(t1,3)*(74306 + 27921*t2 - 31072*Power(t2,2) - 
                  35716*Power(t2,3) - 3210*Power(t2,4) + 
                  2557*Power(t2,5) - 150*Power(t2,6)) + 
               Power(t1,2)*(-67632 - 15884*t2 + 41355*Power(t2,2) + 
                  22030*Power(t2,3) + 17002*Power(t2,4) - 
                  4752*Power(t2,5) - 587*Power(t2,6) + 36*Power(t2,7)) \
- t1*(-30836 - 4068*t2 + 27657*Power(t2,2) + 9016*Power(t2,3) - 
                  823*Power(t2,4) + 8788*Power(t2,5) - 
                  4623*Power(t2,6) + 440*Power(t2,7) + Power(t2,8)) - 
               Power(s2,9)*(70 + 43*Power(t1,2) + 166*t2 + 
                  72*Power(t2,2) - t1*(120 + 179*t2)) + 
               Power(s2,8)*(244 + 102*Power(t1,3) + 2461*t2 + 
                  4462*Power(t2,2) + 1990*Power(t2,3) + 
                  Power(t1,2)*(-133 + 321*t2) - 
                  t1*(235 + 2452*t2 + 2337*Power(t2,2))) + 
               Power(s2,7)*(1312 + 12*Power(t1,4) + 1107*t2 - 
                  11025*Power(t2,2) - 16852*Power(t2,3) - 
                  6599*Power(t2,4) - 3*Power(t1,3)*(481 + 515*t2) + 
                  Power(t1,2)*(4143 + 9647*t2 + 2805*Power(t2,2)) + 
                  t1*(-4005 - 10196*t2 - 1622*Power(t2,2) + 
                     4547*Power(t2,3))) + 
               Power(s2,6)*(-1624 - 20205*t2 - 34591*Power(t2,2) - 
                  10200*Power(t2,3) + 8709*Power(t2,4) + 
                  4866*Power(t2,5) - 3*Power(t1,4)*(-647 + 72*t2) + 
                  Power(t1,3)*(-5769 + 3075*t2 + 7710*Power(t2,2)) - 
                  Power(t1,2)*
                   (-5257 + 23038*t2 + 53178*Power(t2,2) + 
                     21448*Power(t2,3)) + 
                  t1*(228 + 42095*t2 + 94948*Power(t2,2) + 
                     62753*Power(t2,3) + 10598*Power(t2,4))) + 
               Power(s2,5)*(-6540 + 78*Power(t1,5) - 12438*t2 + 
                  31272*Power(t2,2) + 62833*Power(t2,3) + 
                  41562*Power(t2,4) + 20552*Power(t2,5) + 
                  5226*Power(t2,6) + 
                  Power(t1,4)*(-7967 - 14523*t2 + 84*Power(t2,2)) + 
                  Power(t1,3)*
                   (27819 + 86594*t2 + 36197*Power(t2,2) - 
                     12954*Power(t2,3)) + 
                  Power(t1,2)*
                   (-41716 - 156126*t2 - 106704*Power(t2,2) + 
                     46718*Power(t2,3) + 41403*Power(t2,4)) + 
                  t1*(28261 + 94742*t2 + 23235*Power(t2,2) - 
                     110276*Power(t2,3) - 115764*Power(t2,4) - 
                     35035*Power(t2,5))) + 
               Power(s2,4)*(-1839 + Power(t1,5)*(7596 - 72*t2) + 
                  38260*t2 + 78064*Power(t2,2) + 34389*Power(t2,3) + 
                  10906*Power(t2,4) + 957*Power(t2,5) - 
                  19048*Power(t2,6) - 7572*Power(t2,7) + 
                  Power(t1,4)*
                   (-35486 - 6979*t2 + 29088*Power(t2,2) + 
                     852*Power(t2,3)) + 
                  Power(t1,3)*
                   (71040 + 19263*t2 - 174618*Power(t2,2) - 
                     98194*Power(t2,3) + 6966*Power(t2,4)) + 
                  Power(t1,2)*
                   (-64083 + 44783*t2 + 375327*Power(t2,2) + 
                     366176*Power(t2,3) + 54191*Power(t2,4) - 
                     28863*Power(t2,5)) + 
                  t1*(22805 - 94762*t2 - 300608*Power(t2,2) - 
                     282130*Power(t2,3) - 99120*Power(t2,4) + 
                     50020*Power(t2,5) + 29093*Power(t2,6))) - 
               Power(s2,3)*(-13104 + 144*Power(t1,6) - 35881*t2 + 
                  28285*Power(t2,2) + 68462*Power(t2,3) + 
                  25161*Power(t2,4) + 26113*Power(t2,5) + 
                  18753*Power(t2,6) - 4108*Power(t2,7) - 
                  2373*Power(t2,8) + 
                  2*Power(t1,5)*(3721 + 10662*t2 + 612*Power(t2,2)) + 
                  2*Power(t1,4)*
                   (-13191 - 73440*t2 - 37160*Power(t2,2) + 
                     8121*Power(t2,3) + 420*Power(t2,4)) + 
                  Power(t1,3)*
                   (59609 + 370845*t2 + 348488*Power(t2,2) - 
                     29578*Power(t2,3) - 67563*Power(t2,4) + 
                     201*Power(t2,5)) + 
                  Power(t1,2)*
                   (-82083 - 407555*t2 - 398721*Power(t2,2) + 
                     26570*Power(t2,3) + 232817*Power(t2,4) + 
                     63381*Power(t2,5) - 6215*Power(t2,6)) + 
                  t1*(54417 + 197728*t2 + 92502*Power(t2,2) - 
                     83097*Power(t2,3) - 163823*Power(t2,4) - 
                     119752*Power(t2,5) - 4666*Power(t2,6) + 
                     7603*Power(t2,7))) - 
               s2*(8720 + 72*Power(t1,7) + 31662*t2 + 
                  8200*Power(t2,2) - 14149*Power(t2,3) - 
                  3444*Power(t2,4) - 1186*Power(t2,5) - 
                  1726*Power(t2,6) - 1393*Power(t2,7) + 
                  738*Power(t2,8) - 46*Power(t2,9) + 
                  6*Power(t1,6)*(126 + 361*t2 + 56*Power(t2,2)) + 
                  2*Power(t1,5)*
                   (644 - 13450*t2 - 5460*Power(t2,2) + 
                     192*Power(t2,3) + 75*Power(t2,4)) + 
                  Power(t1,4)*
                   (2085 + 117375*t2 + 96482*Power(t2,2) - 
                     4508*Power(t2,3) - 4715*Power(t2,4) - 
                     375*Power(t2,5)) + 
                  Power(t1,3)*
                   (-30793 - 246996*t2 - 207471*Power(t2,2) - 
                     9362*Power(t2,3) + 49465*Power(t2,4) + 
                     3312*Power(t2,5) + 271*Power(t2,6)) + 
                  Power(t1,2)*
                   (55283 + 262830*t2 + 187434*Power(t2,2) - 
                     2474*Power(t2,3) - 58907*Power(t2,4) - 
                     29112*Power(t2,5) + 3896*Power(t2,6) - 
                     98*Power(t2,7)) + 
                  t1*(-37333 - 140971*t2 - 68589*Power(t2,2) + 
                     21214*Power(t2,3) + 30685*Power(t2,4) + 
                     17633*Power(t2,5) + 6425*Power(t2,6) - 
                     3154*Power(t2,7) + 98*Power(t2,8))) + 
               Power(s2,2)*(9979 - 28935*t2 - 80345*Power(t2,2) - 
                  32752*Power(t2,3) - 1911*Power(t2,4) - 
                  7069*Power(t2,5) + 7185*Power(t2,6) + 
                  3772*Power(t2,7) - 436*Power(t2,8) - 88*Power(t2,9) + 
                  24*Power(t1,6)*(217 + 23*t2) + 
                  2*Power(t1,5)*
                   (-16589 - 8049*t2 + 5586*Power(t2,2) + 
                     630*Power(t2,3)) + 
                  Power(t1,4)*
                   (104335 + 94296*t2 - 74212*Power(t2,2) - 
                     50096*Power(t2,3) + 405*Power(t2,4) + 
                     180*Power(t2,5)) + 
                  Power(t1,3)*
                   (-166229 - 136131*t2 + 225296*Power(t2,2) + 
                     262104*Power(t2,3) + 35501*Power(t2,4) - 
                     10435*Power(t2,5) - 402*Power(t2,6)) + 
                  Power(t1,2)*
                   (136191 + 15360*t2 - 340530*Power(t2,2) - 
                     380272*Power(t2,3) - 116511*Power(t2,4) + 
                     37008*Power(t2,5) + 11726*Power(t2,6) + 
                     150*Power(t2,7)) + 
                  t1*(-56210 + 70325*t2 + 261010*Power(t2,2) + 
                     192635*Power(t2,3) + 87420*Power(t2,4) - 
                     13561*Power(t2,5) - 25964*Power(t2,6) - 
                     2111*Power(t2,7) + 160*Power(t2,8)))) + 
            Power(s1,3)*(1916 + 5*Power(s2,10) + 20*Power(t1,7) - 
               4494*t2 - 4291*Power(t2,2) + 1450*Power(t2,3) + 
               1274*Power(t2,4) - 4350*Power(t2,5) + 2453*Power(t2,6) - 
               294*Power(t2,7) + Power(s2,9)*(49 - 54*t1 + 29*t2) + 
               Power(t1,6)*(-242 + 86*t2) + 
               2*Power(t1,5)*
                (-1258 + 1043*t2 - 82*Power(t2,2) + 80*Power(t2,3)) + 
               Power(t1,4)*(13823 - 12798*t2 - 13506*Power(t2,2) + 
                  910*Power(t2,3) - 345*Power(t2,4)) + 
               2*Power(t1,3)*
                (-12957 + 13382*t2 + 21891*Power(t2,2) + 
                  377*Power(t2,3) - 2000*Power(t2,4) + 56*Power(t2,5)) \
+ Power(t1,2)*(23887 - 33267*t2 - 39072*Power(t2,2) - 
                  18610*Power(t2,3) + 12099*Power(t2,4) + 
                  51*Power(t2,5) + 32*Power(t2,6)) + 
               t1*(-10842 + 20347*t2 + 18522*Power(t2,2) + 
                  3106*Power(t2,3) + 7982*Power(t2,4) - 
                  9025*Power(t2,5) + 1316*Power(t2,6) - 22*Power(t2,7)) \
- Power(s2,8)*(782 + 102*Power(t1,2) + 2788*t2 + 1792*Power(t2,2) - 
                  t1*(797 + 1470*t2)) + 
               Power(s2,7)*(-49 + 484*Power(t1,3) + 8610*t2 + 
                  18255*Power(t2,2) + 8951*Power(t2,3) - 
                  Power(t1,2)*(2702 + 1299*t2) - 
                  2*t1*(-1252 + 611*t2 + 2762*Power(t2,2))) + 
               Power(s2,6)*(5935 + 80*Power(t1,4) + 19240*t2 + 
                  80*Power(t2,2) - 21648*Power(t2,3) - 
                  10949*Power(t2,4) - 2*Power(t1,3)*(897 + 2540*t2) + 
                  Power(t1,2)*(8308 + 34193*t2 + 18990*Power(t2,2)) - 
                  t1*(12914 + 57327*t2 + 51827*Power(t2,2) + 
                     8700*Power(t2,3))) - 
               Power(s2,5)*(-3748 + 24478*t2 + 69214*Power(t2,2) + 
                  50221*Power(t2,3) + 21989*Power(t2,4) + 
                  6215*Power(t2,5) + Power(t1,4)*(-5212 + 460*t2) + 
                  Power(t1,3)*(26204 + 17799*t2 - 15230*Power(t2,2)) + 
                  Power(t1,2)*
                   (-45242 - 50253*t2 + 61514*Power(t2,2) + 
                     56528*Power(t2,3)) - 
                  t1*(-27671 + 1739*t2 + 133823*Power(t2,2) + 
                     155302*Power(t2,3) + 53772*Power(t2,4))) + 
               Power(s2,4)*(-9944 + 200*Power(t1,5) - 49994*t2 - 
                  26585*Power(t2,2) + 7992*Power(t2,3) + 
                  11792*Power(t2,4) + 35870*Power(t2,5) + 
                  16690*Power(t2,6) - 
                  Power(t1,4)*(2233 + 23688*t2 + 60*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (713 + 64469*t2 + 48990*Power(t2,2) - 
                     7181*Power(t2,3)) + 
                  Power(t1,2)*
                   (-14669 - 260141*t2 - 356870*Power(t2,2) - 
                     52450*Power(t2,3) + 57332*Power(t2,4)) + 
                  t1*(25323 + 201631*t2 + 262532*Power(t2,2) + 
                     84198*Power(t2,3) - 103667*Power(t2,4) - 
                     62448*Power(t2,5))) + 
               Power(s2,3)*(-15765 + 8314*t2 + 72571*Power(t2,2) + 
                  47383*Power(t2,3) + 42779*Power(t2,4) + 
                  32720*Power(t2,5) - 10227*Power(t2,6) - 
                  7063*Power(t2,7) + Power(t1,5)*(9602 + 80*t2) + 
                  Power(t1,4)*
                   (-53048 - 44686*t2 + 25258*Power(t2,2) + 
                     900*Power(t2,3)) + 
                  2*Power(t1,3)*
                   (63260 + 111320*t2 - 31156*Power(t2,2) - 
                     53815*Power(t2,3) + 1360*Power(t2,4)) + 
                  Power(t1,2)*
                   (-144282 - 266999*t2 + 87414*Power(t2,2) + 
                     361232*Power(t2,3) + 113132*Power(t2,4) - 
                     18339*Power(t2,5)) + 
                  2*t1*(38326 + 39123*t2 - 63247*Power(t2,2) - 
                     132972*Power(t2,3) - 101473*Power(t2,4) - 
                     1289*Power(t2,5) + 11168*Power(t2,6))) + 
               s2*(12511 + 10883*t2 - 16136*Power(t2,2) - 
                  11161*Power(t2,3) - 5579*Power(t2,4) - 
                  2809*Power(t2,5) - 1822*Power(t2,6) + 
                  1721*Power(t2,7) - 128*Power(t2,8) + 
                  2*Power(t1,6)*(779 + 120*t2) + 
                  2*Power(t1,5)*
                   (-5635 - 3815*t2 + 736*Power(t2,2) + 
                     150*Power(t2,3)) + 
                  Power(t1,4)*
                   (43678 + 71824*t2 - 6896*Power(t2,2) - 
                     9000*Power(t2,3) - 600*Power(t2,4)) + 
                  Power(t1,3)*
                   (-93584 - 166317*t2 + 872*Power(t2,2) + 
                     80486*Power(t2,3) + 8050*Power(t2,4) + 
                     97*Power(t2,5)) + 
                  Power(t1,2)*
                   (103360 + 164561*t2 - 18406*Power(t2,2) - 
                     113068*Power(t2,3) - 51264*Power(t2,4) + 
                     9513*Power(t2,5) + 154*Power(t2,6)) + 
                  t1*(-56523 - 69873*t2 + 31265*Power(t2,2) + 
                     66838*Power(t2,3) + 37877*Power(t2,4) + 
                     7695*Power(t2,5) - 7701*Power(t2,6) + 
                     254*Power(t2,7))) + 
               Power(s2,2)*(-160*Power(t1,6) - 
                  2*Power(t1,5)*(-1348 + 6129*t2 + 550*Power(t2,2)) + 
                  Power(t1,4)*
                   (-30318 + 67506*t2 + 57956*Power(t2,2) - 
                     3890*Power(t2,3) - 400*Power(t2,4)) + 
                  2*Power(t1,3)*
                   (31795 - 88931*t2 - 151026*Power(t2,2) - 
                     22434*Power(t2,3) + 12785*Power(t2,4) + 
                     432*Power(t2,5)) + 
                  Power(t1,2)*
                   (-39658 + 245309*t2 + 441286*Power(t2,2) + 
                     154384*Power(t2,3) - 86224*Power(t2,4) - 
                     29561*Power(t2,5) + 54*Power(t2,6)) + 
                  t1*(976 - 173741*t2 - 227521*Power(t2,2) - 
                     113380*Power(t2,3) + 45422*Power(t2,4) + 
                     62423*Power(t2,5) + 4685*Power(t2,6) - 
                     876*Power(t2,7)) + 
                  2*(1569 + 24606*t2 + 19284*Power(t2,2) + 
                     1337*Power(t2,3) + 2366*Power(t2,4) - 
                     8907*Power(t2,5) - 4836*Power(t2,6) + 
                     707*Power(t2,7) + 172*Power(t2,8)))) + 
            s1*(4779 + 30*Power(t1,8) + 4798*t2 - 1712*Power(t2,2) - 
               2628*Power(t2,3) - 609*Power(t2,4) + 2485*Power(t2,5) + 
               50*Power(t2,6) - 1554*Power(t2,7) + 548*Power(t2,8) - 
               45*Power(t2,9) + 
               6*Power(t1,7)*(-63 + 9*t2 + 4*Power(t2,2)) + 
               Power(s2,10)*(-2 - 3*Power(t1,2) + t1*(5 - 7*t2) + 
                  9*t2 + 17*Power(t2,2)) + 
               Power(t1,6)*(2352 + 3302*t2 - 158*Power(t2,2) - 
                  72*Power(t2,3)) + 
               2*Power(t1,5)*
                (-7787 - 10369*t2 - 3717*Power(t2,2) + 
                  1717*Power(t2,3) + 46*Power(t2,4) + 10*Power(t2,5)) + 
               Power(t1,4)*(48985 + 54492*t2 + 23237*Power(t2,2) - 
                  6632*Power(t2,3) - 6895*Power(t2,4) + 
                  470*Power(t2,5) - 55*Power(t2,6)) + 
               Power(t1,3)*(-76111 - 78179*t2 - 19562*Power(t2,2) + 
                  3814*Power(t2,3) + 12785*Power(t2,4) + 
                  3057*Power(t2,5) - 890*Power(t2,6) + 54*Power(t2,7)) + 
               Power(t1,2)*(62699 + 62795*t2 + 763*Power(t2,2) - 
                  10001*Power(t2,3) + 150*Power(t2,4) - 
                  8191*Power(t2,5) + 347*Power(t2,6) + 
                  373*Power(t2,7) - 23*Power(t2,8)) + 
               t1*(-26782 - 26566*t2 + 5276*Power(t2,2) + 
                  10557*Power(t2,3) - 2252*Power(t2,4) - 
                  2406*Power(t2,5) + 4790*Power(t2,6) - 
                  1277*Power(t2,7) + 48*Power(t2,8) + 4*Power(t2,9)) + 
               Power(s2,9)*(28 + 8*Power(t1,3) + 138*t2 + 
                  192*Power(t2,2) + 69*Power(t2,3) + 
                  Power(t1,2)*(23 + 90*t2) - 
                  t1*(59 + 242*t2 + 203*Power(t2,2))) - 
               Power(s2,8)*(-48 + 445*t2 + 2515*Power(t2,2) + 
                  3132*Power(t2,3) + 1090*Power(t2,4) + 
                  2*Power(t1,3)*(111 + 95*t2) + 
                  72*Power(t1,2)*(-6 - 5*t2 + 4*Power(t2,2)) - 
                  t1*(-258 + 319*t2 + 2404*Power(t2,2) + 
                     1628*Power(t2,3))) + 
               Power(s2,7)*(-491 + Power(t1,4)*(272 - 45*t2) - 
                  3259*t2 - 2472*Power(t2,2) + 5586*Power(t2,3) + 
                  7451*Power(t2,4) + 2539*Power(t2,5) + 
                  2*Power(t1,3)*(43 + 1365*t2 + 870*Power(t2,2)) - 
                  Power(t1,2)*
                   (1295 + 8923*t2 + 11555*Power(t2,2) + 
                     2616*Power(t2,3)) + 
                  t1*(1428 + 9459*t2 + 13550*Power(t2,2) + 
                     3947*Power(t2,3) - 1638*Power(t2,4))) + 
               Power(s2,6)*(-594 + 18*Power(t1,5) + 3996*t2 + 
                  21931*Power(t2,2) + 26676*Power(t2,3) + 
                  10385*Power(t2,4) - 255*Power(t2,5) - 
                  987*Power(t2,6) + 
                  Power(t1,4)*(-3005 - 3849*t2 + 48*Power(t2,2)) + 
                  Power(t1,3)*
                   (8464 + 14713*t2 - 113*Power(t2,2) - 
                     5014*Power(t2,3)) + 
                  Power(t1,2)*
                   (-8994 - 15411*t2 + 18763*Power(t2,2) + 
                     35391*Power(t2,3) + 11817*Power(t2,4)) - 
                  t1*(-4111 - 485*t2 + 42896*Power(t2,2) + 
                     67549*Power(t2,3) + 36442*Power(t2,4) + 
                     5909*Power(t2,5))) + 
               Power(s2,5)*(1707 + Power(t1,5)*(2390 - 9*t2) + 
                  15342*t2 + 15180*Power(t2,2) - 14800*Power(t2,3) - 
                  26069*Power(t2,4) - 17016*Power(t2,5) - 
                  8988*Power(t2,6) - 2055*Power(t2,7) + 
                  Power(t1,4)*
                   (-4679 + 11510*t2 + 13714*Power(t2,2) + 
                     414*Power(t2,3)) + 
                  Power(t1,3)*
                   (365 - 53618*t2 - 93449*Power(t2,2) - 
                     30093*Power(t2,3) + 4998*Power(t2,4)) + 
                  Power(t1,2)*
                   (6166 + 93267*t2 + 176358*Power(t2,2) + 
                     94138*Power(t2,3) - 14149*Power(t2,4) - 
                     15548*Power(t2,5)) + 
                  t1*(-5949 - 66362*t2 - 109282*Power(t2,2) - 
                     38388*Power(t2,3) + 38732*Power(t2,4) + 
                     44216*Power(t2,5) + 12239*Power(t2,6))) - 
               Power(s2,4)*(-3987 + 102*Power(t1,6) + 3255*t2 + 
                  43837*Power(t2,2) + 51084*Power(t2,3) + 
                  19338*Power(t2,4) + 11060*Power(t2,5) + 
                  3537*Power(t2,6) - 5446*Power(t2,7) - 
                  1902*Power(t2,8) + 
                  2*Power(t1,5)*(4573 + 6486*t2 + 426*Power(t2,2)) + 
                  Power(t1,4)*
                   (-35581 - 77178*t2 - 20277*Power(t2,2) + 
                     14718*Power(t2,3) + 744*Power(t2,4)) + 
                  Power(t1,3)*
                   (59491 + 156997*t2 + 49944*Power(t2,2) - 
                     96436*Power(t2,3) - 46712*Power(t2,4) + 
                     1236*Power(t2,5)) + 
                  Power(t1,2)*
                   (-52569 - 124403*t2 + 27077*Power(t2,2) + 
                     222653*Power(t2,3) + 179871*Power(t2,4) + 
                     26594*Power(t2,5) - 7404*Power(t2,6)) + 
                  t1*(23398 + 28423*t2 - 100138*Power(t2,2) - 
                     186387*Power(t2,3) - 144800*Power(t2,4) - 
                     53101*Power(t2,5) + 11992*Power(t2,6) + 
                     7340*Power(t2,7))) + 
               Power(s2,3)*(-1661 - 27305*t2 - 25536*Power(t2,2) + 
                  27564*Power(t2,3) + 29874*Power(t2,4) + 
                  3680*Power(t2,5) + 7694*Power(t2,6) + 
                  5794*Power(t2,7) - 907*Power(t2,8) - 
                  429*Power(t2,9) + Power(t1,6)*(4604 + 600*t2) + 
                  2*Power(t1,5)*
                   (-9770 + 1348*t2 + 7645*Power(t2,2) + 
                     759*Power(t2,3)) + 
                  Power(t1,4)*
                   (38714 - 33023*t2 - 129252*Power(t2,2) - 
                     50076*Power(t2,3) + 3900*Power(t2,4) + 
                     339*Power(t2,5)) + 
                  Power(t1,3)*
                   (-37296 + 122406*t2 + 346990*Power(t2,2) + 
                     232496*Power(t2,3) + 1232*Power(t2,4) - 
                     20574*Power(t2,5) - 298*Power(t2,6)) - 
                  Power(t1,2)*
                   (-12229 + 183749*t2 + 374359*Power(t2,2) + 
                     263422*Power(t2,3) + 21161*Power(t2,4) - 
                     73845*Power(t2,5) - 18385*Power(t2,6) + 
                     942*Power(t2,7)) + 
                  t1*(2950 + 118461*t2 + 166992*Power(t2,2) + 
                     50391*Power(t2,3) - 13948*Power(t2,4) - 
                     46389*Power(t2,5) - 36940*Power(t2,6) - 
                     2007*Power(t2,7) + 1330*Power(t2,8))) - 
               Power(s2,2)*(8448 + 216*Power(t1,7) + 3067*t2 - 
                  45810*Power(t2,2) - 56558*Power(t2,3) - 
                  12191*Power(t2,4) + 147*Power(t2,5) - 
                  3720*Power(t2,6) + 1378*Power(t2,7) + 
                  763*Power(t2,8) - 74*Power(t2,9) - 10*Power(t2,10) + 
                  Power(t1,6)*(4508 + 6198*t2 + 900*Power(t2,2)) + 
                  Power(t1,5)*
                   (-24646 - 56576*t2 - 20260*Power(t2,2) + 
                     3898*Power(t2,3) + 630*Power(t2,4)) + 
                  Power(t1,4)*
                   (66789 + 179665*t2 + 95200*Power(t2,2) - 
                     32004*Power(t2,3) - 19763*Power(t2,4) - 
                     517*Power(t2,5) + 36*Power(t2,6)) - 
                  Power(t1,3)*
                   (106086 + 247631*t2 + 99087*Power(t2,2) - 
                     111996*Power(t2,3) - 106042*Power(t2,4) - 
                     13355*Power(t2,5) + 1891*Power(t2,6) + 
                     88*Power(t2,7)) + 
                  Power(t1,2)*
                   (94825 + 155315*t2 - 59969*Power(t2,2) - 
                     192555*Power(t2,3) - 152525*Power(t2,4) - 
                     44137*Power(t2,5) + 7751*Power(t2,6) + 
                     2321*Power(t2,7) + 58*Power(t2,8)) + 
                  t1*(-44054 - 39846*t2 + 128556*Power(t2,2) + 
                     166367*Power(t2,3) + 74415*Power(t2,4) + 
                     33478*Power(t2,5) - 256*Power(t2,6) - 
                     5603*Power(t2,7) - 469*Power(t2,8) + 4*Power(t2,9))\
) + s2*(327 + 20692*t2 + 28934*Power(t2,2) + 3149*Power(t2,3) - 
                  4720*Power(t2,4) + 1174*Power(t2,5) + 
                  574*Power(t2,6) - 617*Power(t2,7) - 521*Power(t2,8) + 
                  186*Power(t2,9) - 10*Power(t2,10) + 
                  Power(t1,7)*(830 + 66*t2) + 
                  6*Power(t1,6)*
                   (-981 - 179*t2 + 219*Power(t2,2) + 42*Power(t2,3)) + 
                  Power(t1,5)*
                   (24376 + 153*t2 - 23294*Power(t2,2) - 
                     6566*Power(t2,3) - 78*Power(t2,4) + 33*Power(t2,5)) \
+ Power(t1,4)*(-48449 + 41042*t2 + 107248*Power(t2,2) + 
                     57230*Power(t2,3) - 1317*Power(t2,4) - 
                     1176*Power(t2,5) - 96*Power(t2,6)) + 
                  Power(t1,3)*
                   (44751 - 131396*t2 - 224697*Power(t2,2) - 
                     120755*Power(t2,3) - 11835*Power(t2,4) + 
                     15488*Power(t2,5) + 647*Power(t2,6) + 
                     101*Power(t2,7)) + 
                  Power(t1,2)*
                   (-17097 + 166313*t2 + 235794*Power(t2,2) + 
                     105752*Power(t2,3) + 12597*Power(t2,4) - 
                     12533*Power(t2,5) - 9244*Power(t2,6) + 
                     946*Power(t2,7) - 56*Power(t2,8)) + 
                  t1*(1148 - 95640*t2 - 126157*Power(t2,2) - 
                     35486*Power(t2,3) + 499*Power(t2,4) + 
                     3210*Power(t2,5) + 3879*Power(t2,6) + 
                     2640*Power(t2,7) - 777*Power(t2,8) + 28*Power(t2,9))\
))) + s*(-1047 + 6653*t1 - 18406*Power(t1,2) + 28640*Power(t1,3) - 
            26913*Power(t1,4) + 15229*Power(t1,5) - 4924*Power(t1,6) + 
            862*Power(t1,7) - 102*Power(t1,8) + 8*Power(t1,9) + 
            Power(s1,11)*s2*(8*Power(s2,2) - 2*t1 + 
               s2*(-2 + 9*t1 - t2)) - 1559*t2 + 7662*t1*t2 - 
            16766*Power(t1,2)*t2 + 21996*Power(t1,3)*t2 - 
            19614*Power(t1,4)*t2 + 12520*Power(t1,5)*t2 - 
            5507*Power(t1,6)*t2 + 1364*Power(t1,7)*t2 - 
            96*Power(t1,8)*t2 + 489*Power(t2,2) - 3939*t1*Power(t2,2) + 
            8758*Power(t1,2)*Power(t2,2) - 5268*Power(t1,3)*Power(t2,2) - 
            5820*Power(t1,4)*Power(t2,2) + 9790*Power(t1,5)*Power(t2,2) - 
            4604*Power(t1,6)*Power(t2,2) + 608*Power(t1,7)*Power(t2,2) - 
            14*Power(t1,8)*Power(t2,2) + 1201*Power(t2,3) - 
            5189*t1*Power(t2,3) + 6601*Power(t1,2)*Power(t2,3) + 
            717*Power(t1,3)*Power(t2,3) - 7178*Power(t1,4)*Power(t2,3) + 
            4326*Power(t1,5)*Power(t2,3) - 458*Power(t1,6)*Power(t2,3) - 
            20*Power(t1,7)*Power(t2,3) + 93*Power(t2,4) + 
            607*t1*Power(t2,4) - 3928*Power(t1,2)*Power(t2,4) + 
            5756*Power(t1,3)*Power(t2,4) - 1963*Power(t1,4)*Power(t2,4) - 
            753*Power(t1,5)*Power(t2,4) + 208*Power(t1,6)*Power(t2,4) - 
            6*Power(t1,7)*Power(t2,4) - 309*Power(t2,5) + 
            2009*t1*Power(t2,5) - 3167*Power(t1,2)*Power(t2,5) + 
            552*Power(t1,3)*Power(t2,5) + 1098*Power(t1,4)*Power(t2,5) - 
            246*Power(t1,5)*Power(t2,5) + 9*Power(t1,6)*Power(t2,5) - 
            307*Power(t2,6) + 541*t1*Power(t2,6) + 
            666*Power(t1,2)*Power(t2,6) - 866*Power(t1,3)*Power(t2,6) + 
            60*Power(t1,4)*Power(t2,6) + 6*Power(t1,5)*Power(t2,6) + 
            83*Power(t2,7) - 595*t1*Power(t2,7) + 
            335*Power(t1,2)*Power(t2,7) + 71*Power(t1,3)*Power(t2,7) - 
            14*Power(t1,4)*Power(t2,7) + 126*Power(t2,8) + 
            44*t1*Power(t2,8) - 82*Power(t1,2)*Power(t2,8) + 
            2*Power(t1,3)*Power(t2,8) - 56*Power(t2,9) + 
            17*t1*Power(t2,9) + 5*Power(t1,2)*Power(t2,9) + 
            6*Power(t2,10) - 2*t1*Power(t2,10) - 
            Power(s2,11)*Power(t2,2)*(1 - t1 + t2) - 
            Power(s2,10)*t2*(2 + 5*t2 + 6*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(2 + 8*t2) - t1*(4 + 13*t2 + 10*Power(t2,2))) \
+ Power(s1,10)*(-2*(-1 + t1)*t1 + Power(s2,3)*(-1 + 112*t1 - 114*t2) + 
               s2*(4 - 30*t1 + 11*Power(t1,2) + 4*t2 + 16*t1*t2) + 
               Power(s2,2)*(-20 + 18*Power(t1,2) + t1*(19 - 86*t2) + 
                  19*t2 + 10*Power(t2,2))) + 
            Power(s2,9)*(-1 - 6*t2 + 31*Power(t2,2) + 172*Power(t2,3) + 
               191*Power(t2,4) + 59*Power(t2,5) + 
               Power(t1,3)*(1 + 16*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(-3 - 38*t2 + 20*Power(t2,2) + 
                  46*Power(t2,3)) + 
               t1*(3 + 28*t2 - 62*Power(t2,2) - 224*Power(t2,3) - 
                  116*Power(t2,4))) + 
            Power(s2,8)*(-2 + 80*t2 + 399*Power(t2,2) + 
               266*Power(t2,3) - 475*Power(t2,4) - 584*Power(t2,5) - 
               169*Power(t2,6) + 
               2*Power(t1,4)*(-4 - 11*t2 + Power(t2,2)) - 
               Power(t1,3)*(-26 + 53*t2 + 334*Power(t2,2) + 
                  127*Power(t2,3)) + 
               Power(t1,2)*(-30 + 252*t2 + 984*Power(t2,2) + 
                  834*Power(t2,3) + 89*Power(t2,4)) + 
               t1*(14 - 257*t2 - 1051*Power(t2,2) - 959*Power(t2,3) + 
                  51*Power(t2,4) + 205*Power(t2,5))) + 
            Power(s2,7)*(39 + Power(t1,5)*(11 - 4*t2) + 214*t2 - 
               548*Power(t2,2) - 2811*Power(t2,3) - 2979*Power(t2,4) - 
               849*Power(t2,5) + 236*Power(t2,6) + 130*Power(t2,7) + 
               Power(t1,4)*(23 + 536*t2 + 467*Power(t2,2) + 
                  12*Power(t2,3)) + 
               2*Power(t1,3)*
                (-87 - 817*t2 - 705*Power(t2,2) + 132*Power(t2,3) + 
                  187*Power(t2,4)) + 
               Power(t1,2)*(274 + 1890*t2 + 1120*Power(t2,2) - 
                  3217*Power(t2,3) - 3192*Power(t2,4) - 699*Power(t2,5)) \
+ t1*(-173 - 1002*t2 + 371*Power(t2,2) + 5723*Power(t2,3) + 
                  6364*Power(t2,4) + 2354*Power(t2,5) + 183*Power(t2,6))) \
+ Power(s2,6)*(10 + 2*Power(t1,6) - 797*t2 - 3686*Power(t2,2) - 
               3357*Power(t2,3) + 1559*Power(t2,4) + 3650*Power(t2,5) + 
               1942*Power(t2,6) + 493*Power(t2,7) + 64*Power(t2,8) - 
               Power(t1,5)*(248 + 553*t2 + 50*Power(t2,2)) + 
               Power(t1,4)*(876 + 920*t2 - 2073*Power(t2,2) - 
                  1655*Power(t2,3) - 80*Power(t2,4)) + 
               Power(t1,3)*(-1170 + 919*t2 + 10529*Power(t2,2) + 
                  11944*Power(t2,3) + 2948*Power(t2,4) - 358*Power(t2,5)\
) + Power(t1,2)*(704 - 3183*t2 - 18817*Power(t2,2) - 
                  23228*Power(t2,3) - 8141*Power(t2,4) + 
                  1566*Power(t2,5) + 1074*Power(t2,6)) + 
               t1*(-174 + 2694*t2 + 14097*Power(t2,2) + 
                  16336*Power(t2,3) + 3001*Power(t2,4) - 
                  5176*Power(t2,5) - 3431*Power(t2,6) - 700*Power(t2,7))) \
+ Power(s2,5)*(-197 - 1760*t2 + 203*Power(t2,2) + 8028*Power(t2,3) + 
               10142*Power(t2,4) + 4747*Power(t2,5) + 1265*Power(t2,6) + 
               98*Power(t2,7) - 373*Power(t2,8) - 121*Power(t2,9) + 
               Power(t1,6)*(213 + 64*t2) + 
               Power(t1,5)*(-228 + 2976*t2 + 2936*Power(t2,2) + 
                  318*Power(t2,3)) + 
               Power(t1,4)*(-1179 - 13198*t2 - 17353*Power(t2,2) - 
                  4487*Power(t2,3) + 1570*Power(t2,4) + 124*Power(t2,5)) \
+ Power(t1,3)*(2972 + 23664*t2 + 34847*Power(t2,2) + 7131*Power(t2,3) - 
                  12033*Power(t2,4) - 5128*Power(t2,5) + 31*Power(t2,6)) \
+ Power(t1,2)*(-2757 - 21718*t2 - 28579*Power(t2,2) + 
                  7545*Power(t2,3) + 32410*Power(t2,4) + 
                  20193*Power(t2,5) + 2712*Power(t2,6) - 584*Power(t2,7)\
) + 2*t1*(588 + 4986*t2 + 3973*Power(t2,2) - 9289*Power(t2,3) - 
                  15700*Power(t2,4) - 9740*Power(t2,5) - 
                  2590*Power(t2,6) + 483*Power(t2,7) + 275*Power(t2,8))) \
- Power(s2,2)*(106*Power(t1,8) + 
               8*Power(t1,7)*(116 + 144*t2 + 49*Power(t2,2)) + 
               2*Power(t1,6)*
                (-3271 - 5951*t2 - 2615*Power(t2,2) + 55*Power(t2,3) + 
                  130*Power(t2,4)) + 
               Power(t1,5)*(17714 + 39999*t2 + 19806*Power(t2,2) - 
                  4418*Power(t2,3) - 4132*Power(t2,4) - 
                  311*Power(t2,5) + 22*Power(t2,6)) - 
               Power(t1,4)*(28406 + 61714*t2 + 23355*Power(t2,2) - 
                  23917*Power(t2,3) - 23298*Power(t2,4) - 
                  3810*Power(t2,5) + 491*Power(t2,6) + 41*Power(t2,7)) + 
               Power(t1,3)*(29402 + 46431*t2 - 12657*Power(t2,2) - 
                  51388*Power(t2,3) - 42136*Power(t2,4) - 
                  14789*Power(t2,5) + 1305*Power(t2,6) + 
                  728*Power(t2,7) + 8*Power(t2,8)) - 
               t1*Power(1 + t2,2)*
                (-7228 + 14510*t2 + 16484*Power(t2,2) - 
                  7542*Power(t2,3) + 2147*Power(t2,4) + 
                  1300*Power(t2,5) - 377*Power(t2,6) - 44*Power(t2,7) + 
                  10*Power(t2,8)) + 
               Power(1 + t2,2)*
                (-1194 + 2991*t2 + 4977*Power(t2,2) - 
                  1642*Power(t2,3) - 1834*Power(t2,4) + 
                  1117*Power(t2,5) + 12*Power(t2,6) - 35*Power(t2,7) - 
                  9*Power(t2,8) + Power(t2,9)) + 
               Power(t1,2)*(-19236 - 14515*t2 + 49555*Power(t2,2) + 
                  60532*Power(t2,3) + 26027*Power(t2,4) + 
                  14259*Power(t2,5) + 3025*Power(t2,6) - 
                  1608*Power(t2,7) - 203*Power(t2,8) + 20*Power(t2,9))) - 
            Power(s2,4)*(387 + 26*Power(t1,7) - 1517*t2 - 
               8862*Power(t2,2) - 9813*Power(t2,3) - 753*Power(t2,4) + 
               1874*Power(t2,5) - 310*Power(t2,6) + 710*Power(t2,7) + 
               785*Power(t2,8) - 50*Power(t2,9) - 43*Power(t2,10) + 
               Power(t1,6)*(1256 + 2403*t2 + 502*Power(t2,2)) + 
               Power(t1,5)*(-5749 - 10988*t2 - 1942*Power(t2,2) + 
                  2698*Power(t2,3) + 472*Power(t2,4)) + 
               Power(t1,4)*(10665 + 20042*t2 - 6976*Power(t2,2) - 
                  27572*Power(t2,3) - 10142*Power(t2,4) + 
                  68*Power(t2,5) + 66*Power(t2,6)) + 
               Power(t1,3)*(-10838 - 15629*t2 + 38192*Power(t2,2) + 
                  78978*Power(t2,3) + 46796*Power(t2,4) + 
                  3111*Power(t2,5) - 2426*Power(t2,6) - 85*Power(t2,7)) \
+ Power(t1,2)*(6628 + 1552*t2 - 58786*Power(t2,2) - 94401*Power(t2,3) - 
                  60649*Power(t2,4) - 11271*Power(t2,5) + 
                  8234*Power(t2,6) + 2306*Power(t2,7) - 77*Power(t2,8)) \
+ t1*(-2375 + 4137*t2 + 37872*Power(t2,2) + 50128*Power(t2,3) + 
                  24643*Power(t2,4) + 5499*Power(t2,5) - 
                  3875*Power(t2,6) - 4555*Power(t2,7) - 
                  357*Power(t2,8) + 139*Power(t2,9))) + 
            s2*(4*Power(t1,8)*(65 + 18*t2) + 
               2*Power(t1,7)*
                (-806 - 447*t2 + 98*Power(t2,2) + 59*Power(t2,3)) + 
               Power(t1,6)*(5311 + 372*t2 - 4144*Power(t2,2) - 
                  2344*Power(t2,3) - 83*Power(t2,4) + 20*Power(t2,5)) - 
               2*Power(t1,5)*
                (4908 - 5706*t2 - 10938*Power(t2,2) - 
                  7630*Power(t2,3) - 991*Power(t2,4) + 
                  256*Power(t2,5) + 17*Power(t2,6)) - 
               Power(t1,4)*(-9163 + 38758*t2 + 51999*Power(t2,2) + 
                  32819*Power(t2,3) + 11201*Power(t2,4) - 
                  1708*Power(t2,5) - 645*Power(t2,6) + 3*Power(t2,7)) + 
               Power(t1,3)*(-2557 + 59998*t2 + 69676*Power(t2,2) + 
                  26993*Power(t2,3) + 14169*Power(t2,4) + 
                  3198*Power(t2,5) - 2020*Power(t2,6) - 
                  173*Power(t2,7) + 28*Power(t2,8)) + 
               2*Power(1 + t2,2)*
                (-213 - 1681*t2 + 445*Power(t2,2) + 956*Power(t2,3) - 
                  430*Power(t2,4) - 16*Power(t2,5) - 75*Power(t2,6) + 
                  68*Power(t2,7) - 15*Power(t2,8) + Power(t2,9)) - 
               Power(t1,2)*(2240 + 50584*t2 + 57547*Power(t2,2) + 
                  4783*Power(t2,3) - 677*Power(t2,4) + 
                  6835*Power(t2,5) + 1185*Power(t2,6) - 
                  1107*Power(t2,7) + 41*Power(t2,8) + 9*Power(t2,9)) + 
               t1*(1917 + 22596*t2 + 28202*Power(t2,2) - 
                  2718*Power(t2,3) - 9654*Power(t2,4) + 
                  2856*Power(t2,5) + 2806*Power(t2,6) + 10*Power(t2,7) - 
                  451*Power(t2,8) + 72*Power(t2,9) - 4*Power(t2,10))) + 
            Power(s2,3)*(397 + 4206*t2 + 2891*Power(t2,2) - 
               8694*Power(t2,3) - 9282*Power(t2,4) + 581*Power(t2,5) + 
               1270*Power(t2,6) - 1303*Power(t2,7) - 319*Power(t2,8) + 
               173*Power(t2,9) + 3*Power(t2,10) - 3*Power(t2,11) + 
               Power(t1,7)*(748 + 370*t2) + 
               2*Power(t1,6)*
                (-1296 + 612*t2 + 1189*Power(t2,2) + 330*Power(t2,3)) + 
               Power(t1,5)*(2917 - 14756*t2 - 27578*Power(t2,2) - 
                  10322*Power(t2,3) + 47*Power(t2,4) + 222*Power(t2,5)) \
+ Power(t1,4)*(169 + 45764*t2 + 86485*Power(t2,2) + 48422*Power(t2,3) - 
                  539*Power(t2,4) - 4238*Power(t2,5) - 
                  293*Power(t2,6) + 8*Power(t2,7)) - 
               2*Power(t1,3)*
                (1853 + 34960*t2 + 59686*Power(t2,2) + 
                  36391*Power(t2,3) - 561*Power(t2,4) - 
                  9908*Power(t2,5) - 2021*Power(t2,6) + 
                  132*Power(t2,7) + 8*Power(t2,8)) + 
               Power(t1,2)*(4106 + 57438*t2 + 80436*Power(t2,2) + 
                  30485*Power(t2,3) - 9220*Power(t2,4) - 
                  22931*Power(t2,5) - 11990*Power(t2,6) + 
                  99*Power(t2,7) + 468*Power(t2,8) + 5*Power(t2,9)) + 
               t1*(-2039 - 24326*t2 - 25240*Power(t2,2) + 
                  12353*Power(t2,3) + 17829*Power(t2,4) + 
                  5870*Power(t2,5) + 6782*Power(t2,6) + 
                  2575*Power(t2,7) - 682*Power(t2,8) - 136*Power(t2,9) + 
                  6*Power(t2,10))) - 
            Power(s1,9)*(2 + 102*Power(s2,5) - 13*Power(t1,3) + 
               Power(t1,2)*(17 - 11*t2) + 3*t2 + 3*t1*(-7 + 4*t2) + 
               Power(s2,4)*(47 - 314*t1 + 137*t2) - 
               Power(s2,3)*(-31 + 217*Power(t1,2) + t1*(33 - 1110*t2) + 
                  22*t2 + 676*Power(t2,2)) + 
               Power(s2,2)*(-40 - 9*Power(t1,3) - 272*t2 + 
                  77*Power(t2,2) + 45*Power(t2,3) + 
                  Power(t1,2)*(-326 + 135*t2) + 
                  t1*(415 + 267*t2 - 365*Power(t2,2))) + 
               s2*(-16 - 30*Power(t1,3) + 26*t2 + 36*Power(t2,2) + 
                  Power(t1,2)*(71 + 94*t2) + 
                  t1*(23 - 269*t2 + 56*Power(t2,2)))) + 
            Power(s1,8)*(-4 - 259*Power(s2,6) + 5*Power(t1,4) + 7*t2 + 
               24*Power(t2,2) - 2*Power(t1,3)*(19 + 42*t2) + 
               Power(s2,5)*(159 + 292*t1 + 638*t2) + 
               Power(t1,2)*(88 + 94*t2 - 22*Power(t2,2)) + 
               t1*(4 - 171*t2 + 34*Power(t2,2)) + 
               Power(s2,4)*(530 + 692*Power(t1,2) + 704*t2 + 
                  1182*Power(t2,2) - t1*(1067 + 3085*t2)) + 
               Power(s2,3)*(211 + 129*Power(t1,3) + 852*t2 - 
                  105*Power(t2,2) - 2267*Power(t2,3) - 
                  3*Power(t1,2)*(-524 + 585*t2) + 
                  t1*(-1973 - 862*t2 + 4824*Power(t2,2))) + 
               Power(s2,2)*(19 + Power(t1,3)*(585 - 34*t2) - 382*t2 - 
                  1450*Power(t2,2) + 171*Power(t2,3) + 
                  120*Power(t2,4) + 
                  Power(t1,2)*(-823 - 2818*t2 + 436*Power(t2,2)) + 
                  t1*(20 + 3628*t2 + 1466*Power(t2,2) - 904*Power(t2,3))\
) + s2*(-77 + 19*Power(t1,4) + Power(t1,3)*(309 - 184*t2) - 217*t2 + 
                  46*Power(t2,2) + 146*Power(t2,3) + 
                  Power(t1,2)*(-943 + 402*t2 + 346*Power(t2,2)) + 
                  2*t1*(310 + 237*t2 - 537*Power(t2,2) + 56*Power(t2,3)))\
) - Power(s1,7)*(-38 + 287*Power(s2,7) + 
               Power(s2,6)*(-830 + 89*t1 - 1824*t2) - 59*t2 - 
               25*Power(t2,2) + 90*Power(t2,3) + 
               5*Power(t1,4)*(-25 + 3*t2) + 
               Power(t1,3)*(493 - 213*t2 - 226*Power(t2,2)) + 
               Power(t1,2)*(-737 + 455*t2 + 229*Power(t2,2) - 
                  14*Power(t2,3)) + 
               t1*(317 + 277*t2 - 670*Power(t2,2) + 68*Power(t2,3)) + 
               Power(s2,5)*(-1548 - 990*Power(t1,2) + 242*t2 + 
                  1415*Power(t2,2) + 6*t1*(476 + 521*t2)) + 
               Power(s2,4)*(446 - 434*Power(t1,3) + 2955*t2 + 
                  3633*Power(t2,2) + 4481*Power(t2,3) + 
                  Power(t1,2)*(-1367 + 5413*t2) + 
                  t1*(1709 - 7328*t2 - 12959*Power(t2,2))) + 
               Power(s2,3)*(1479 - 16*Power(t1,4) + 2436*t2 + 
                  5409*Power(t2,2) - 200*Power(t2,3) - 
                  4840*Power(t2,4) + Power(t1,3)*(-2884 + 779*t2) + 
                  Power(t1,2)*(6612 + 13181*t2 - 6128*Power(t2,2)) + 
                  t1*(-4377 - 17426*t2 - 5428*Power(t2,2) + 
                     12102*Power(t2,3))) + 
               s2*(-55 + 8*Power(t1,5) - 572*t2 - 967*Power(t2,2) - 
                  96*Power(t2,3) + 352*Power(t2,4) + 
                  5*Power(t1,4)*(-97 + 13*t2) + 
                  Power(t1,3)*(1715 + 2246*t2 - 462*Power(t2,2)) + 
                  Power(t1,2)*
                   (-1302 - 6920*t2 + 907*Power(t2,2) + 
                     714*Power(t2,3)) + 
                  t1*(334 + 4385*t2 + 2644*Power(t2,2) - 
                     2510*Power(t2,3) + 140*Power(t2,4))) - 
               Power(s2,2)*(-605 - 1051*t2 + 1433*Power(t2,2) + 
                  4207*Power(t2,3) - 216*Power(t2,4) - 210*Power(t2,5) + 
                  Power(t1,4)*(347 + 16*t2) + 
                  Power(t1,3)*(2181 - 3941*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*
                   (-6739 + 4742*t2 + 10532*Power(t2,2) - 
                     784*Power(t2,3)) + 
                  t1*(4505 + 1735*t2 - 13567*Power(t2,2) - 
                     4392*Power(t2,3) + 1442*Power(t2,4)))) + 
            Power(s1,6)*(-43 - 158*Power(s2,8) - 212*t2 - 
               158*Power(t2,2) - 241*Power(t2,3) + 210*Power(t2,4) + 
               Power(t1,5)*(62 + 13*t2) + 
               Power(s2,7)*(1146 - 379*t1 + 1947*t2) - 
               Power(t1,4)*(701 + 621*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(1376 + 2750*t2 - 566*Power(t2,2) - 
                  320*Power(t2,3)) + 
               Power(t1,2)*(-885 - 4217*t2 + 938*Power(t2,2) + 
                  357*Power(t2,3) + 14*Power(t2,4)) + 
               t1*(281 + 1627*t2 + 1578*Power(t2,2) - 
                  1588*Power(t2,3) + 112*Power(t2,4)) + 
               Power(s2,6)*(834 + 695*Power(t1,2) - 4469*t2 - 
                  5395*Power(t2,2) - 6*t1*(406 + 45*t2)) + 
               Power(s2,5)*(-3965 + 668*Power(t1,3) - 10695*t2 - 
                  2398*Power(t2,2) + 683*Power(t2,3) - 
                  Power(t1,2)*(1999 + 7393*t2) + 
                  t1*(5337 + 20230*t2 + 13318*Power(t2,2))) + 
               Power(s2,4)*(-4561 + 56*Power(t1,4) + 
                  Power(t1,3)*(4862 - 2619*t2) - 271*t2 + 
                  5356*Power(t2,2) + 9696*Power(t2,3) + 
                  9779*Power(t2,4) + 
                  Power(t1,2)*(-16582 - 12895*t2 + 17876*Power(t2,2)) + 
                  t1*(15137 + 19213*t2 - 20590*Power(t2,2) - 
                     30692*Power(t2,3))) + 
               Power(s2,3)*(586 + 8112*t2 + 9705*Power(t2,2) + 
                  16471*Power(t2,3) - 98*Power(t2,4) - 
                  6944*Power(t2,5) - 7*Power(t1,4)*(-241 + 3*t2) + 
                  Power(t1,3)*(891 - 18915*t2 + 1915*Power(t2,2)) + 
                  Power(t1,2)*
                   (-8904 + 42858*t2 + 46431*Power(t2,2) - 
                     12060*Power(t2,3)) + 
                  t1*(4539 - 25682*t2 - 63549*Power(t2,2) - 
                     16450*Power(t2,3) + 19362*Power(t2,4))) + 
               s2*(765 + 289*t2 - 1801*Power(t2,2) - 2063*Power(t2,3) - 
                  616*Power(t2,4) + 560*Power(t2,5) + 
                  Power(t1,5)*(241 + 49*t2) + 
                  2*Power(t1,4)*(549 - 1288*t2 + 14*Power(t2,2)) + 
                  Power(t1,3)*
                   (-7400 + 9370*t2 + 6917*Power(t2,2) - 
                     588*Power(t2,3)) + 
                  Power(t1,2)*
                   (10040 - 5361*t2 - 21731*Power(t2,2) + 
                     967*Power(t2,3) + 896*Power(t2,4)) + 
                  t1*(-5111 + 194*t2 + 12951*Power(t2,2) + 
                     7270*Power(t2,3) - 3794*Power(t2,4) + 
                     112*Power(t2,5))) + 
               Power(s2,2)*(2651 - 14*Power(t1,5) + 4624*t2 + 
                  6025*Power(t2,2) - 2764*Power(t2,3) - 
                  7497*Power(t2,4) + 126*Power(t2,5) + 252*Power(t2,6) + 
                  Power(t1,4)*(4047 - 1586*t2 - 84*Power(t2,2)) + 
                  Power(t1,3)*
                   (-14961 - 16325*t2 + 11297*Power(t2,2) + 
                     174*Power(t2,3)) + 
                  Power(t1,2)*
                   (17681 + 47639*t2 - 10693*Power(t2,2) - 
                     22290*Power(t2,3) + 840*Power(t2,4)) - 
                  t1*(9405 + 32567*t2 + 11479*Power(t2,2) - 
                     28430*Power(t2,3) - 8134*Power(t2,4) + 
                     1540*Power(t2,5)))) - 
            Power(s1,5)*(324 + 36*Power(s2,9) - 16*Power(t1,6) + 
               Power(s2,8)*(-661 + 292*t1 - 987*t2) - 47*t2 - 
               593*Power(t2,2) + 81*Power(t2,3) - 781*Power(t2,4) + 
               336*Power(t2,5) + 
               Power(t1,5)*(-236 + 229*t2 + 55*Power(t2,2)) + 
               Power(s2,7)*(691 - 181*Power(t1,2) + 
                  t1*(319 - 1872*t2) + 6284*t2 + 5553*Power(t2,2)) - 
               Power(t1,4)*(-2671 + 3457*t2 + 1264*Power(t2,2) + 
                  85*Power(t2,3)) + 
               Power(t1,3)*(-6075 + 6082*t2 + 6792*Power(t2,2) - 
                  963*Power(t2,3) - 240*Power(t2,4)) + 
               Power(t1,2)*(5542 - 2946*t2 - 10513*Power(t2,2) + 
                  863*Power(t2,3) + 455*Power(t2,4) + 28*Power(t2,5)) + 
               t1*(-2265 + 689*t2 + 3453*Power(t2,2) + 
                  3919*Power(t2,3) - 2388*Power(t2,4) + 140*Power(t2,5)) \
- Power(s2,6)*(-5042 + 565*Power(t1,3) - 6875*t2 + 9215*Power(t2,2) + 
                  8616*Power(t2,3) - 2*Power(t1,2)*(2033 + 2493*t2) + 
                  t1*(9063 + 17125*t2 + 3523*Power(t2,2))) - 
               Power(s2,5)*(-1213 + 72*Power(t1,4) + 
                  Power(t1,3)*(3219 - 3831*t2) + 19872*t2 + 
                  29995*Power(t2,2) + 10428*Power(t2,3) + 
                  2741*Power(t2,4) + 
                  Power(t1,2)*(-12004 + 8156*t2 + 22687*Power(t2,2)) - 
                  2*t1*(-4678 + 11933*t2 + 29484*Power(t2,2) + 
                     15235*Power(t2,3))) + 
               Power(s2,4)*(-9017 - 27776*t2 - 9486*Power(t2,2) + 
                  220*Power(t2,3) + 15385*Power(t2,4) + 
                  13503*Power(t2,5) + Power(t1,4)*(-3155 + 162*t2) + 
                  Power(t1,3)*(11583 + 30198*t2 - 6327*Power(t2,2)) + 
                  Power(t1,2)*
                   (-20726 - 103147*t2 - 47063*Power(t2,2) + 
                     32482*Power(t2,3)) + 
                  t1*(21895 + 93014*t2 + 76424*Power(t2,2) - 
                     30018*Power(t2,3) - 45209*Power(t2,4))) + 
               Power(s2,3)*(-9029 - 54*Power(t1,5) + 856*t2 + 
                  16085*Power(t2,2) + 19051*Power(t2,3) + 
                  29043*Power(t2,4) + 252*Power(t2,5) - 
                  6832*Power(t2,6) + 
                  Power(t1,4)*(-9017 + 7848*t2 + 138*Power(t2,2)) + 
                  Power(t1,3)*
                   (42197 + 12507*t2 - 51086*Power(t2,2) + 
                     2389*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (30016 + 33203*t2 - 56594*Power(t2,2) - 
                     44964*Power(t2,3) + 7305*Power(t2,4)) + 
                  t1*(35758 + 38599*t2 - 57607*Power(t2,2) - 
                     126092*Power(t2,3) - 28826*Power(t2,4) + 
                     20538*Power(t2,5))) + 
               Power(s2,2)*(104 + 13803*t2 + 12704*Power(t2,2) + 
                  15092*Power(t2,3) - 2843*Power(t2,4) - 
                  8603*Power(t2,5) - 42*Power(t2,6) + 210*Power(t2,7) - 
                  Power(t1,5)*(2093 + 162*t2) + 
                  Power(t1,4)*
                   (2226 + 21607*t2 - 2749*Power(t2,2) - 
                     180*Power(t2,3)) + 
                  Power(t1,3)*
                   (10957 - 80410*t2 - 49289*Power(t2,2) + 
                     17837*Power(t2,3) + 400*Power(t2,4)) + 
                  Power(t1,2)*
                   (-17944 + 93871*t2 + 137705*Power(t2,2) - 
                     10937*Power(t2,3) - 29300*Power(t2,4) + 
                     518*Power(t2,5)) + 
                  t1*(5966 - 48279*t2 - 93849*Power(t2,2) - 
                     31125*Power(t2,3) + 36551*Power(t2,4) + 
                     9814*Power(t2,5) - 1106*Power(t2,6))) + 
               s2*(2475 + 36*Power(t1,6) + 3992*t2 + 1936*Power(t2,2) - 
                  3028*Power(t2,3) - 2207*Power(t2,4) - 
                  1372*Power(t2,5) + 616*Power(t2,6) + 
                  Power(t1,5)*(-2161 + 786*t2 + 120*Power(t2,2)) + 
                  Power(t1,4)*
                   (13101 + 7242*t2 - 5763*Power(t2,2) - 
                     170*Power(t2,3)) + 
                  Power(t1,3)*
                   (-25000 - 41690*t2 + 21431*Power(t2,2) + 
                     11738*Power(t2,3) - 350*Power(t2,4)) + 
                  Power(t1,2)*
                   (22795 + 54416*t2 - 6881*Power(t2,2) - 
                     37901*Power(t2,3) + 321*Power(t2,4) + 
                     686*Power(t2,5)) + 
                  t1*(-10876 - 27411*t2 - 3498*Power(t2,2) + 
                     20229*Power(t2,3) + 11742*Power(t2,4) - 
                     3864*Power(t2,5) + 56*Power(t2,6)))) + 
            Power(s1,4)*(875 + Power(s2,10) + 10*Power(t1,7) + 
               Power(t1,6)*(176 - 21*t2) + 1100*t2 - 51*Power(t2,2) - 
               929*Power(t2,3) + 990*Power(t2,4) - 1403*Power(t2,5) + 
               378*Power(t2,6) + Power(s2,9)*(141 - 90*t1 + 204*t2) + 
               Power(t1,5)*(-3601 - 798*t2 + 410*Power(t2,2) + 
                  90*Power(t2,3)) + 
               Power(t1,4)*(11160 + 10620*t2 - 7302*Power(t2,2) - 
                  1360*Power(t2,3) - 145*Power(t2,4)) - 
               Power(t1,3)*(15476 + 23336*t2 - 10678*Power(t2,2) - 
                  9599*Power(t2,3) + 1120*Power(t2,4) + 68*Power(t2,5)) \
+ Power(t1,2)*(11652 + 20008*t2 - 3006*Power(t2,2) - 
                  14287*Power(t2,3) + 10*Power(t2,4) + 
                  497*Power(t2,5) + 14*Power(t2,6)) + 
               t1*(-4781 - 7847*t2 + 671*Power(t2,2) + 
                  3287*Power(t2,3) + 5340*Power(t2,4) - 
                  2246*Power(t2,5) + 112*Power(t2,6)) - 
               Power(s2,8)*(684 + 54*Power(t1,2) + 3340*t2 + 
                  2538*Power(t2,2) - t1*(521 + 1443*t2)) + 
               Power(s2,7)*(-1851 + 281*Power(t1,3) + 2252*t2 + 
                  13858*Power(t2,2) + 8612*Power(t2,3) - 
                  Power(t1,2)*(2386 + 1369*t2) + 
                  t1*(4217 + 3454*t2 - 3477*Power(t2,2))) + 
               Power(s2,6)*(4064 + 40*Power(t1,4) + 
                  Power(t1,3)*(133 - 2980*t2) + 25398*t2 + 
                  21321*Power(t2,2) - 8277*Power(t2,3) - 
                  7865*Power(t2,4) + 
                  Power(t1,2)*(1073 + 19962*t2 + 14166*Power(t2,2)) - 
                  t1*(5514 + 45318*t2 + 47803*Power(t2,2) + 
                     10312*Power(t2,3))) - 
               Power(s2,5)*(-10507 - 10433*t2 + 37841*Power(t2,2) + 
                  43892*Power(t2,3) + 18830*Power(t2,4) + 
                  6233*Power(t2,5) + Power(t1,4)*(-2983 + 230*t2) + 
                  Power(t1,3)*(17012 + 19668*t2 - 8515*Power(t2,2)) + 
                  Power(t1,2)*
                   (-37312 - 73272*t2 + 8900*Power(t2,2) + 
                     37202*Power(t2,3)) + 
                  t1*(33723 + 60262*t2 - 35160*Power(t2,2) - 
                     91658*Power(t2,3) - 41550*Power(t2,4))) + 
               Power(s2,4)*(1002 + 100*Power(t1,5) - 42579*t2 - 
                  65345*Power(t2,2) - 27024*Power(t2,3) - 
                  12915*Power(t2,4) + 15282*Power(t2,5) + 
                  12187*Power(t2,6) + 
                  Power(t1,4)*(5199 - 13492*t2 - 30*Power(t2,2)) + 
                  Power(t1,3)*
                   (-28862 + 48745*t2 + 74550*Power(t2,2) - 
                     7713*Power(t2,3)) + 
                  Power(t1,2)*
                   (39170 - 87288*t2 - 254392*Power(t2,2) - 
                     88948*Power(t2,3) + 35285*Power(t2,4)) + 
                  t1*(-16495 + 96931*t2 + 224357*Power(t2,2) + 
                     152301*Power(t2,3) - 23030*Power(t2,4) - 
                     42844*Power(t2,5))) + 
               Power(s2,3)*(-14984 - 42274*t2 - 2570*Power(t2,2) + 
                  11497*Power(t2,3) + 20170*Power(t2,4) + 
                  31731*Power(t2,5) + 518*Power(t2,6) - 
                  4586*Power(t2,7) + Power(t1,5)*(5373 + 40*t2) + 
                  Power(t1,4)*
                   (-25389 - 45254*t2 + 13967*Power(t2,2) + 
                     450*Power(t2,3)) + 
                  Power(t1,3)*
                   (51495 + 210732*t2 + 46704*Power(t2,2) - 
                     73138*Power(t2,3) + 1455*Power(t2,4)) - 
                  Power(t1,2)*
                   (64462 + 295931*t2 + 192942*Power(t2,2) - 
                     156511*Power(t2,3) - 104470*Power(t2,4) + 
                     11122*Power(t2,5)) + 
                  t1*(47798 + 171640*t2 + 120214*Power(t2,2) - 
                     58573*Power(t2,3) - 149025*Power(t2,4) - 
                     31178*Power(t2,5) + 14490*Power(t2,6))) + 
               Power(s2,2)*(-12349 - 80*Power(t1,6) + 3153*t2 + 
                  27571*Power(t2,2) + 15570*Power(t2,3) + 
                  20540*Power(t2,4) - 1210*Power(t2,5) - 
                  6377*Power(t2,6) - 138*Power(t2,7) + 
                  120*Power(t2,8) + 
                  Power(t1,5)*(7664 - 7027*t2 - 550*Power(t2,2)) + 
                  Power(t1,4)*
                   (-48838 + 1368*t2 + 46253*Power(t2,2) - 
                     2035*Power(t2,3) - 200*Power(t2,4)) + 
                  Power(t1,3)*
                   (105425 + 61159*t2 - 171111*Power(t2,2) - 
                     78254*Power(t2,3) + 16715*Power(t2,4) + 
                     426*Power(t2,5)) + 
                  Power(t1,2)*
                   (-104227 - 86277*t2 + 191311*Power(t2,2) + 
                     210818*Power(t2,3) - 2650*Power(t2,4) - 
                     24586*Power(t2,5) + 140*Power(t2,6)) + 
                  t1*(52704 + 23708*t2 - 91636*Power(t2,2) - 
                     139324*Power(t2,3) - 45565*Power(t2,4) + 
                     29480*Power(t2,5) + 7812*Power(t2,6) - 
                     520*Power(t2,7))) + 
               s2*(-561 + 10971*t2 + 7332*Power(t2,2) + 
                  3558*Power(t2,3) - 2755*Power(t2,4) - 
                  761*Power(t2,5) - 1764*Power(t2,6) + 476*Power(t2,7) + 
                  Power(t1,6)*(907 + 120*t2) + 
                  2*Power(t1,5)*
                   (-1283 - 4464*t2 + 424*Power(t2,2) + 75*Power(t2,3)) \
+ Power(t1,4)*(-4194 + 55878*t2 + 18257*Power(t2,2) - 
                     6995*Power(t2,3) - 325*Power(t2,4)) + 
                  Power(t1,3)*
                   (17272 - 106142*t2 - 94196*Power(t2,2) + 
                     26151*Power(t2,3) + 11915*Power(t2,4)) + 
                  Power(t1,2)*
                   (-16667 + 97328*t2 + 115613*Power(t2,2) + 
                     1153*Power(t2,3) - 39730*Power(t2,4) - 
                     349*Power(t2,5) + 294*Power(t2,6)) + 
                  t1*(5591 - 47158*t2 - 55706*Power(t2,2) - 
                     9242*Power(t2,3) + 16955*Power(t2,4) + 
                     11918*Power(t2,5) - 2674*Power(t2,6) + 
                     16*Power(t2,7)))) + 
            Power(s1,3)*(366 + Power(s2,11) + Power(t1,7)*(58 - 40*t2) - 
               2963*t2 - 1369*Power(t2,2) + 608*Power(t2,3) + 
               696*Power(t2,4) - 1831*Power(t2,5) + 1523*Power(t2,6) - 
               294*Power(t2,7) - Power(s2,10)*(-2 + 6*t1 + t2) + 
               2*Power(t1,6)*(-280 - 323*t2 + 5*Power(t2,2)) - 
               2*Power(t1,5)*
                (290 - 6123*t2 - 600*Power(t2,2) + 241*Power(t2,3) + 
                  35*Power(t2,4)) + 
               Power(t1,4)*(5923 - 35769*t2 - 17632*Power(t2,2) + 
                  8114*Power(t2,3) + 845*Power(t2,4) + 115*Power(t2,5)) \
+ Power(t1,3)*(-10325 + 48593*t2 + 35076*Power(t2,2) - 
                  8436*Power(t2,3) - 8221*Power(t2,4) + 
                  843*Power(t2,5) - 26*Power(t2,6)) + 
               Power(t1,2)*(7865 - 37477*t2 - 26131*Power(t2,2) - 
                  474*Power(t2,3) + 10563*Power(t2,4) + 
                  827*Power(t2,5) - 399*Power(t2,6) + 2*Power(t2,7)) - 
               t1*(2747 - 15997*t2 - 9392*Power(t2,2) + 
                  1230*Power(t2,3) + 63*Power(t2,4) + 
                  4259*Power(t2,5) - 1226*Power(t2,6) + 44*Power(t2,7)) \
- Power(s2,9)*(142 + 44*Power(t1,2) + 618*t2 + 455*Power(t2,2) - 
                  6*t1*(32 + 65*t2)) + 
               Power(s2,8)*(-50 + 80*Power(t1,3) + 2669*t2 + 
                  6637*Power(t2,2) + 3441*Power(t2,3) + 
                  Power(t1,2)*(-405 + 143*t2) + 
                  t1*(361 - 1826*t2 - 2781*Power(t2,2))) + 
               Power(s2,7)*(2356 + 8*Power(t1,4) + 8516*t2 - 
                  1677*Power(t2,2) - 15566*Power(t2,3) - 
                  7813*Power(t2,4) - Power(t1,3)*(838 + 1269*t2) + 
                  Power(t1,2)*(3632 + 10717*t2 + 3768*Power(t2,2)) + 
                  t1*(-5129 - 19314*t2 - 10934*Power(t2,2) + 
                     2806*Power(t2,3))) + 
               Power(s2,6)*(1817 + Power(t1,4)*(1525 - 144*t2) - 
                  15075*t2 - 49587*Power(t2,2) - 32995*Power(t2,3) + 
                  1288*Power(t2,4) + 3864*Power(t2,5) + 
                  Power(t1,3)*(-7847 - 2801*t2 + 5874*Power(t2,2)) + 
                  Power(t1,2)*
                   (14089 + 2300*t2 - 36864*Power(t2,2) - 
                     20606*Power(t2,3)) + 
                  t1*(-9624 + 17045*t2 + 86589*Power(t2,2) + 
                     68450*Power(t2,3) + 14533*Power(t2,4))) + 
               Power(s2,5)*(-8057 + 52*Power(t1,5) - 43890*t2 - 
                  29544*Power(t2,2) + 32676*Power(t2,3) + 
                  35338*Power(t2,4) + 18566*Power(t2,5) + 
                  6227*Power(t2,6) + 
                  Power(t1,4)*(-1949 - 11199*t2 + 56*Power(t2,2)) + 
                  9*Power(t1,3)*
                   (965 + 7696*t2 + 5044*Power(t2,2) - 
                     1030*Power(t2,3)) + 
                  Power(t1,2)*
                   (-21396 - 156472*t2 - 169803*Power(t2,2) - 
                     4958*Power(t2,3) + 35148*Power(t2,4)) + 
                  t1*(22708 + 141407*t2 + 146224*Power(t2,2) - 
                     10474*Power(t2,3) - 81592*Power(t2,4) - 
                     34802*Power(t2,5))) + 
               Power(s2,4)*(-13082 + Power(t1,5)*(5684 - 48*t2) - 
                  2574*t2 + 74842*Power(t2,2) + 74679*Power(t2,3) + 
                  34266*Power(t2,4) + 20485*Power(t2,5) - 
                  9519*Power(t2,6) - 7147*Power(t2,7) + 
                  Power(t1,4)*
                   (-34239 - 27879*t2 + 21328*Power(t2,2) + 
                     568*Power(t2,3)) + 
                  Power(t1,3)*
                   (85103 + 137275*t2 - 71344*Power(t2,2) - 
                     93508*Power(t2,3) + 4772*Power(t2,4)) + 
                  Power(t1,2)*
                   (-102374 - 177372*t2 + 121839*Power(t2,2) + 
                     318348*Power(t2,3) + 95217*Power(t2,4) - 
                     23137*Power(t2,5)) + 
                  t1*(58926 + 70623*t2 - 150855*Power(t2,2) - 
                     270232*Power(t2,3) - 169409*Power(t2,4) + 
                     7012*Power(t2,5) + 25945*Power(t2,6))) + 
               Power(s2,3)*(2631 - 96*Power(t1,6) + 59916*t2 + 
                  72971*Power(t2,2) + 5716*Power(t2,3) + 
                  4387*Power(t2,4) - 10826*Power(t2,5) - 
                  21707*Power(t2,6) - 440*Power(t2,7) + 
                  2032*Power(t2,8) - 
                  2*Power(t1,5)*(-2009 + 8034*t2 + 408*Power(t2,2)) - 
                  2*Power(t1,4)*
                   (16212 - 42173*t2 - 43017*Power(t2,2) + 
                     5695*Power(t2,3) + 280*Power(t2,4)) - 
                  Power(t1,3)*
                   (-64128 + 183825*t2 + 399390*Power(t2,2) + 
                     78022*Power(t2,3) - 59052*Power(t2,4) + 
                     189*Power(t2,5)) + 
                  Power(t1,2)*
                   (-43876 + 240841*t2 + 552872*Power(t2,2) + 
                     281420*Power(t2,3) - 119904*Power(t2,4) - 
                     73917*Power(t2,5) + 5160*Power(t2,6)) + 
                  t1*(5497 - 184660*t2 - 308912*Power(t2,2) - 
                     181220*Power(t2,3) + 18667*Power(t2,4) + 
                     106738*Power(t2,5) + 20972*Power(t2,6) - 
                     6594*Power(t2,7))) - 
               s2*(-9669 + 48*Power(t1,7) + 3344*t2 + 
                  18401*Power(t2,2) + 4620*Power(t2,3) + 
                  2577*Power(t2,4) - 1052*Power(t2,5) + 
                  803*Power(t2,6) - 1424*Power(t2,7) + 
                  256*Power(t2,8) + 
                  2*Power(t1,6)*(-1278 + 993*t2 + 112*Power(t2,2)) + 
                  2*Power(t1,5)*
                   (12471 - 2538*t2 - 7417*Power(t2,2) + 
                     102*Power(t2,3) + 50*Power(t2,4)) - 
                  Power(t1,4)*
                   (81095 + 13709*t2 - 91272*Power(t2,2) - 
                     22944*Power(t2,3) + 4895*Power(t2,4) + 
                     251*Power(t2,5)) + 
                  Power(t1,3)*
                   (125271 + 39154*t2 - 164910*Power(t2,2) - 
                     108468*Power(t2,3) + 17669*Power(t2,4) + 
                     7314*Power(t2,5) + 126*Power(t2,6)) + 
                  Power(t1,2)*
                   (-103844 - 23896*t2 + 148013*Power(t2,2) + 
                     119558*Power(t2,3) + 11772*Power(t2,4) - 
                     25138*Power(t2,5) - 463*Power(t2,6) + 
                     46*Power(t2,7)) + 
                  t1*(46940 - 2713*t2 - 73584*Power(t2,2) - 
                     50956*Power(t2,3) - 9798*Power(t2,4) + 
                     5795*Power(t2,5) + 7724*Power(t2,6) - 
                     1234*Power(t2,7) + 2*Power(t2,8))) + 
               Power(s2,2)*(17845 + 39382*t2 - 12007*Power(t2,2) - 
                  25993*Power(t2,3) - 6545*Power(t2,4) - 
                  16019*Power(t2,5) - 433*Power(t2,6) + 
                  2925*Power(t2,7) + 114*Power(t2,8) - 45*Power(t2,9) + 
                  8*Power(t1,6)*(479 + 46*t2) + 
                  Power(t1,5)*
                   (-23788 - 29558*t2 + 8332*Power(t2,2) + 
                     840*Power(t2,3)) + 
                  Power(t1,4)*
                   (67721 + 181442*t2 + 13836*Power(t2,2) - 
                     50078*Power(t2,3) + 225*Power(t2,4) + 
                     120*Power(t2,5)) - 
                  Power(t1,3)*
                   (114985 + 376907*t2 + 137162*Power(t2,2) - 
                     181788*Power(t2,3) - 70291*Power(t2,4) + 
                     9255*Power(t2,5) + 246*Power(t2,6)) + 
                  Power(t1,2)*
                   (121085 + 354140*t2 + 172842*Power(t2,2) - 
                     184492*Power(t2,3) - 183137*Power(t2,4) - 
                     4792*Power(t2,5) + 12948*Power(t2,6) + 
                     24*Power(t2,7)) + 
                  t1*(-71590 - 170135*t2 - 38073*Power(t2,2) + 
                     74914*Power(t2,3) + 112471*Power(t2,4) + 
                     38745*Power(t2,5) - 14473*Power(t2,6) - 
                     4016*Power(t2,7) + 149*Power(t2,8)))) + 
            Power(s1,2)*(-3077 + 30*Power(t1,8) + 
               Power(s2,11)*(-1 + t1 - 3*t2) + 1560*t2 + 
               3592*Power(t2,2) + 425*Power(t2,3) - 1343*Power(t2,4) - 
               62*Power(t2,5) + 1606*Power(t2,6) - 995*Power(t2,7) + 
               150*Power(t2,8) + 
               4*Power(t1,7)*(-7 - 27*t2 + 6*Power(t2,2)) - 
               4*Power(t1,6)*
                (1079 - 435*t2 - 319*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,5)*(23238 - 2911*t2 - 15686*Power(t2,2) - 
                  1196*Power(t2,3) + 346*Power(t2,4) + 25*Power(t2,5)) + 
               Power(t1,4)*(-52999 + 3515*t2 + 38727*Power(t2,2) + 
                  15186*Power(t2,3) - 4585*Power(t2,4) - 
                  325*Power(t2,5) - 45*Power(t2,6)) + 
               2*Power(t1,3)*
                (32515 - 4107*t2 - 24183*Power(t2,2) - 
                  11946*Power(t2,3) + 710*Power(t2,4) + 
                  2046*Power(t2,5) - 179*Power(t2,6) + 12*Power(t2,7)) + 
               Power(t1,2)*(-45591 + 11537*t2 + 38676*Power(t2,2) + 
                  11239*Power(t2,3) + 3273*Power(t2,4) - 
                  3319*Power(t2,5) - 890*Power(t2,6) + 
                  199*Power(t2,7) - 4*Power(t2,8)) - 
               t1*(-17713 + 7119*t2 + 18156*Power(t2,2) + 
                  2294*Power(t2,3) - 2339*Power(t2,4) + 
                  2751*Power(t2,5) - 1962*Power(t2,6) + 
                  300*Power(t2,7) + 2*Power(t2,8)) - 
               Power(s2,10)*(4 + 7*Power(t1,2) + 10*t2 + 
                  3*Power(t2,2) - 11*t1*(1 + 2*t2)) + 
               Power(s2,9)*(35 + 10*Power(t1,3) + 463*t2 + 
                  1004*Power(t2,2) + 501*Power(t2,3) + 
                  Power(t1,2)*(26 + 141*t2) - 
                  t1*(71 + 622*t2 + 626*Power(t2,2))) - 
               Power(s2,8)*(-291 - 303*t2 + 3756*Power(t2,2) + 
                  6482*Power(t2,3) + 2595*Power(t2,4) + 
                  Power(t1,3)*(300 + 263*t2) + 
                  Power(t1,2)*(-814 - 1533*t2 + 30*Power(t2,2)) + 
                  t1*(805 + 1531*t2 - 2130*Power(t2,2) - 
                     2606*Power(t2,3))) - 
               Power(s2,7)*(520 + 7669*t2 + 14495*Power(t2,2) + 
                  1487*Power(t2,3) - 9200*Power(t2,4) - 
                  4119*Power(t2,5) + 5*Power(t1,4)*(-71 + 9*t2) + 
                  Power(t1,3)*(641 - 2203*t2 - 2079*Power(t2,2)) + 
                  Power(t1,2)*
                   (54 + 11030*t2 + 17525*Power(t2,2) + 
                     4852*Power(t2,3)) + 
                  t1*(-860 - 16454*t2 - 32425*Power(t2,2) - 
                     15136*Power(t2,3) + 591*Power(t2,4))) + 
               Power(s2,6)*(-2903 + 18*Power(t1,5) - 6573*t2 + 
                  19384*Power(t2,2) + 46798*Power(t2,3) + 
                  27299*Power(t2,4) + 3253*Power(t2,5) - 
                  689*Power(t2,6) + 
                  Power(t1,4)*(-2759 - 4656*t2 + 48*Power(t2,2)) + 
                  Power(t1,3)*
                   (10195 + 27453*t2 + 8411*Power(t2,2) - 
                     5426*Power(t2,3)) + 
                  Power(t1,2)*
                   (-15580 - 50765*t2 - 16493*Power(t2,2) + 
                     31672*Power(t2,3) + 16257*Power(t2,4)) - 
                  t1*(-11029 - 34661*t2 + 14101*Power(t2,2) + 
                     78652*Power(t2,3) + 53242*Power(t2,4) + 
                     11038*Power(t2,5))) + 
               Power(s2,5)*(-328 + Power(t1,5)*(2851 - 9*t2) + 
                  25556*t2 + 66691*Power(t2,2) + 37388*Power(t2,3) - 
                  10227*Power(t2,4) - 14695*Power(t2,5) - 
                  10398*Power(t2,6) - 3415*Power(t2,7) + 
                  2*Power(t1,4)*
                   (-6235 + 365*t2 + 7528*Power(t2,2) + 
                     207*Power(t2,3)) + 
                  Power(t1,3)*
                   (23132 - 15350*t2 - 99767*Power(t2,2) - 
                     49792*Power(t2,3) + 5010*Power(t2,4)) + 
                  Power(t1,2)*
                   (-19989 + 58908*t2 + 234081*Power(t2,2) + 
                     187999*Power(t2,3) + 16699*Power(t2,4) - 
                     18997*Power(t2,5)) + 
                  t1*(6804 - 69964*t2 - 213793*Power(t2,2) - 
                     168566*Power(t2,3) - 20933*Power(t2,4) + 
                     41034*Power(t2,5) + 17506*Power(t2,6))) - 
               Power(s2,4)*(-8670 + 102*Power(t1,6) - 36625*t2 - 
                  3568*Power(t2,2) + 59889*Power(t2,3) + 
                  41312*Power(t2,4) + 22083*Power(t2,5) + 
                  14890*Power(t2,6) - 3548*Power(t2,7) - 
                  2601*Power(t2,8) + 
                  Power(t1,5)*(4550 + 14325*t2 + 852*Power(t2,2)) + 
                  3*Power(t1,4)*
                   (-7133 - 33601*t2 - 17157*Power(t2,2) + 
                     4956*Power(t2,3) + 248*Power(t2,4)) + 
                  Power(t1,3)*
                   (48404 + 260987*t2 + 238216*Power(t2,2) - 
                     39674*Power(t2,3) - 62274*Power(t2,4) + 
                     1137*Power(t2,5)) + 
                  Power(t1,2)*
                   (-59717 - 310180*t2 - 302668*Power(t2,2) + 
                     53447*Power(t2,3) + 211432*Power(t2,4) + 
                     57875*Power(t2,5) - 8678*Power(t2,6)) + 
                  t1*(36730 + 172350*t2 + 119398*Power(t2,2) - 
                     92998*Power(t2,3) - 167639*Power(t2,4) - 
                     106355*Power(t2,5) - 1802*Power(t2,6) + 
                     9544*Power(t2,7))) + 
               s2*(-11961 - 17977*t2 + 12239*Power(t2,2) + 
                  13949*Power(t2,3) - 1687*Power(t2,4) + 
                  253*Power(t2,5) + 253*Power(t2,6) + 
                  1043*Power(t2,7) - 716*Power(t2,8) + 92*Power(t2,9) + 
                  2*Power(t1,7)*(500 + 33*t2) + 
                  2*Power(t1,6)*
                   (-4513 - 3672*t2 + 726*Power(t2,2) + 
                     126*Power(t2,3)) + 
                  Power(t1,5)*
                   (41177 + 62063*t2 - 1004*Power(t2,2) - 
                     12040*Power(t2,3) - 231*Power(t2,4) + 
                     33*Power(t2,5)) - 
                  2*Power(t1,4)*
                   (51603 + 90170*t2 + 13289*Power(t2,2) - 
                     34511*Power(t2,3) - 7620*Power(t2,4) + 
                     945*Power(t2,5) + 45*Power(t2,6)) + 
                  Power(t1,3)*
                   (149624 + 252150*t2 + 42615*Power(t2,2) - 
                     108196*Power(t2,3) - 66128*Power(t2,4) + 
                     5876*Power(t2,5) + 2591*Power(t2,6) + 
                     68*Power(t2,7)) + 
                  Power(t1,2)*
                   (-127141 - 192025*t2 + 1323*Power(t2,2) + 
                     88387*Power(t2,3) + 58556*Power(t2,4) + 
                     12661*Power(t2,5) - 8995*Power(t2,6) - 
                     243*Power(t2,7) - 11*Power(t2,8)) - 
                  t1*(-59533 - 83518*t2 + 31469*Power(t2,2) + 
                     46342*Power(t2,3) + 16077*Power(t2,4) + 
                     4818*Power(t2,5) + 1595*Power(t2,6) - 
                     3114*Power(t2,7) + 360*Power(t2,8))) + 
               Power(s2,3)*(8655 - 16222*t2 - 84381*Power(t2,2) - 
                  54655*Power(t2,3) - 1958*Power(t2,4) - 
                  11914*Power(t2,5) + 1641*Power(t2,6) + 
                  8949*Power(t2,7) + 195*Power(t2,8) - 
                  550*Power(t2,9) + 8*Power(t1,6)*(623 + 75*t2) + 
                  2*Power(t1,5)*
                   (-16829 - 9565*t2 + 8006*Power(t2,2) + 
                     759*Power(t2,3)) + 
                  Power(t1,4)*
                   (97597 + 111931*t2 - 92072*Power(t2,2) - 
                     76612*Power(t2,3) + 3543*Power(t2,4) + 
                     339*Power(t2,5)) + 
                  Power(t1,3)*
                   (-146641 - 188817*t2 + 212117*Power(t2,2) + 
                     355188*Power(t2,3) + 65547*Power(t2,4) - 
                     26027*Power(t2,5) - 251*Power(t2,6)) + 
                  Power(t1,2)*
                   (118692 + 99894*t2 - 296163*Power(t2,2) - 
                     481010*Power(t2,3) - 217108*Power(t2,4) + 
                     48508*Power(t2,5) + 30531*Power(t2,6) - 
                     1308*Power(t2,7)) + 
                  t1*(-49629 + 12110*t2 + 243851*Power(t2,2) + 
                     251806*Power(t2,3) + 140281*Power(t2,4) + 
                     12680*Power(t2,5) - 44451*Power(t2,6) - 
                     8398*Power(t2,7) + 1782*Power(t2,8))) - 
               Power(s2,2)*(6911 + 216*Power(t1,7) + 49115*t2 + 
                  42259*Power(t2,2) - 19164*Power(t2,3) - 
                  10818*Power(t2,4) + 3400*Power(t2,5) - 
                  6901*Power(t2,6) - 752*Power(t2,7) + 727*Power(t2,8) + 
                  49*Power(t2,9) - 10*Power(t2,10) + 
                  6*Power(t1,6)*(-257 + 1166*t2 + 150*Power(t2,2)) + 
                  2*Power(t1,5)*
                   (7171 - 27287*t2 - 20356*Power(t2,2) + 
                     1822*Power(t2,3) + 315*Power(t2,4)) + 
                  Power(t1,4)*
                   (-33303 + 175448*t2 + 241656*Power(t2,2) + 
                     26682*Power(t2,3) - 28081*Power(t2,4) - 
                     548*Power(t2,5) + 36*Power(t2,6)) - 
                  Power(t1,3)*
                   (-18805 + 308875*t2 + 484943*Power(t2,2) + 
                     149464*Power(t2,3) - 98383*Power(t2,4) - 
                     35165*Power(t2,5) + 2771*Power(t2,6) + 
                     74*Power(t2,7)) + 
                  Power(t1,2)*
                   (18093 + 325801*t2 + 427985*Power(t2,2) + 
                     172888*Power(t2,3) - 79105*Power(t2,4) - 
                     88487*Power(t2,5) - 4763*Power(t2,6) + 
                     3982*Power(t2,7) + 26*Power(t2,8)) + 
                  t1*(-23522 - 193551*t2 - 189155*Power(t2,2) - 
                     26938*Power(t2,3) + 16641*Power(t2,4) + 
                     46059*Power(t2,5) + 18905*Power(t2,6) - 
                     3862*Power(t2,7) - 1247*Power(t2,8) + 22*Power(t2,9)\
))) - s1*(Power(s2,11)*(-2 + 2*t1 - 3*t2)*t2 + Power(t1,8)*(-4 + 40*t2) - 
               2*Power(t1,7)*(-457 - 182*t2 + 85*Power(t2,2) + 
                  6*Power(t2,3)) + 
               2*Power(t1,6)*(-4484 - 3045*t2 + 571*Power(t2,2) + 
                  507*Power(t2,3) + Power(t2,4)) + 
               Power(t1,5)*(33676 + 21353*t2 + 117*Power(t2,2) - 
                  7794*Power(t2,3) - 804*Power(t2,4) + 113*Power(t2,5) + 
                  3*Power(t2,6)) - 
               Power(t1,4)*(65651 + 36243*t2 - 2940*Power(t2,2) - 
                  12155*Power(t2,3) - 6601*Power(t2,4) + 
                  957*Power(t2,5) + 86*Power(t2,6) + 7*Power(t2,7)) + 
               Power(1 + t2,2)*
                (-3218 + 4472*t2 - 2581*Power(t2,2) + 2287*Power(t2,3) - 
                  2470*Power(t2,4) + 1564*Power(t2,5) - 
                  451*Power(t2,6) + 45*Power(t2,7)) + 
               Power(t1,3)*(74258 + 35896*t2 - 18272*Power(t2,2) - 
                  9493*Power(t2,3) - 5525*Power(t2,4) - 
                  1910*Power(t2,5) + 1006*Power(t2,6) - 61*Power(t2,7) + 
                  5*Power(t2,8)) + 
               t1*(19094 + 9171*t2 - 15155*Power(t2,2) - 
                  6333*Power(t2,3) + 3525*Power(t2,4) + 
                  1913*Power(t2,5) - 2265*Power(t2,6) + 
                  473*Power(t2,7) + 17*Power(t2,8) - 8*Power(t2,9)) - 
               Power(t1,2)*(50101 + 22527*t2 - 26253*Power(t2,2) - 
                  8923*Power(t2,3) + 3593*Power(t2,4) - 
                  2520*Power(t2,5) - 325*Power(t2,6) + 427*Power(t2,7) - 
                  52*Power(t2,8) + Power(t2,9)) - 
               Power(s2,10)*(2 + 9*t2 + 14*Power(t2,2) + 5*Power(t2,3) + 
                  Power(t1,2)*(2 + 15*t2) - 
                  2*t1*(2 + 12*t2 + 13*Power(t2,2))) + 
               Power(s2,9)*(-5 + 70*t2 + 493*Power(t2,2) + 
                  718*Power(t2,3) + 273*Power(t2,4) + 
                  Power(t1,3)*(15 + 17*t2) + 
                  Power(t1,2)*(-35 + 58*t2 + 143*Power(t2,2)) - 
                  t1*(-25 + 145*t2 + 654*Power(t2,2) + 442*Power(t2,3))) \
+ Power(s2,8)*(76 + 676*t2 + 522*Power(t2,2) - 2246*Power(t2,3) - 
                  3108*Power(t2,4) - 1032*Power(t2,5) + 
                  Power(t1,4)*(-17 + 6*t2) - 
                  Power(t1,3)*(64 + 632*t2 + 313*Power(t2,2)) + 
                  Power(t1,2)*
                   (255 + 1768*t2 + 1971*Power(t2,2) + 148*Power(t2,3)) \
+ t1*(-250 - 1818*t2 - 2138*Power(t2,2) + 876*Power(t2,3) + 
                     1181*Power(t2,4))) + 
               Power(s2,7)*(160 - 6*Power(t1,5) - 1112*t2 - 
                  8128*Power(t2,2) - 10809*Power(t2,3) - 
                  2452*Power(t2,4) + 2590*Power(t2,5) + 
                  1155*Power(t2,6) + 
                  Power(t1,4)*(489 + 764*t2 - 18*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1427 - 1833*t2 + 1612*Power(t2,2) + 
                     1465*Power(t2,3)) + 
                  Power(t1,2)*
                   (1571 + 760*t2 - 10606*Power(t2,2) - 
                     12386*Power(t2,3) - 2971*Power(t2,4)) + 
                  t1*(-787 + 1421*t2 + 17053*Power(t2,2) + 
                     23692*Power(t2,3) + 9691*Power(t2,4) + 
                     414*Power(t2,5))) + 
               Power(s2,6)*(-626 - 6556*t2 - 8165*Power(t2,2) + 
                  9932*Power(t2,3) + 21217*Power(t2,4) + 
                  11526*Power(t2,5) + 2333*Power(t2,6) + 
                  160*Power(t2,7) + Power(t1,5)*(-451 + 14*t2) - 
                  Power(t1,4)*
                   (-488 + 5005*t2 + 4839*Power(t2,2) + 136*Power(t2,3)) \
+ Power(t1,3)*(1432 + 20934*t2 + 31761*Power(t2,2) + 8691*Power(t2,3) - 
                     2325*Power(t2,4)) + 
                  3*Power(t1,2)*
                   (-1050 - 11483*t2 - 20073*Power(t2,2) - 
                     7087*Power(t2,3) + 4090*Power(t2,4) + 
                     2200*Power(t2,5)) + 
                  t1*(2307 + 25062*t2 + 41582*Power(t2,2) + 
                     431*Power(t2,3) - 33494*Power(t2,4) - 
                     21337*Power(t2,5) - 4353*Power(t2,6))) + 
               Power(s2,5)*(-1654 + 4*Power(t1,6) + 120*t2 + 
                  25553*Power(t2,2) + 43450*Power(t2,3) + 
                  21811*Power(t2,4) + 1780*Power(t2,5) - 
                  2303*Power(t2,6) - 3088*Power(t2,7) - 
                  997*Power(t2,8) + 
                  Power(t1,5)*(3191 + 5654*t2 + 416*Power(t2,2)) + 
                  Power(t1,4)*
                   (-12625 - 29082*t2 - 5935*Power(t2,2) + 
                     8410*Power(t2,3) + 436*Power(t2,4)) + 
                  Power(t1,3)*
                   (21149 + 56309*t2 + 797*Power(t2,2) - 
                     59548*Power(t2,3) - 25973*Power(t2,4) + 
                     1123*Power(t2,5)) + 
                  Power(t1,2)*
                   (-18861 - 46674*t2 + 44888*Power(t2,2) + 
                     147331*Power(t2,3) + 99657*Power(t2,4) + 
                     11710*Power(t2,5) - 5351*Power(t2,6)) + 
                  t1*(8796 + 13673*t2 - 65848*Power(t2,2) - 
                     137509*Power(t2,3) - 92728*Power(t2,4) - 
                     19956*Power(t2,5) + 10472*Power(t2,6) + 
                     4818*Power(t2,7))) + 
               Power(s2,4)*(696 + 17926*t2 + 33545*Power(t2,2) + 
                  2749*Power(t2,3) - 20483*Power(t2,4) - 
                  8453*Power(t2,5) - 6782*Power(t2,6) - 
                  5394*Power(t2,7) + 696*Power(t2,8) + 524*Power(t2,9) - 
                  120*Power(t1,6)*(19 + 4*t2) - 
                  Power(t1,5)*
                   (-9074 + 3451*t2 + 11485*Power(t2,2) + 
                     1272*Power(t2,3)) + 
                  Power(t1,4)*
                   (-15778 + 31211*t2 + 94934*Power(t2,2) + 
                     38933*Power(t2,3) - 3945*Power(t2,4) - 
                     378*Power(t2,5)) + 
                  Power(t1,3)*
                   (13912 - 91680*t2 - 256569*Power(t2,2) - 
                     176599*Power(t2,3) + 2381*Power(t2,4) + 
                     20406*Power(t2,5) + 149*Power(t2,6)) + 
                  Power(t1,2)*
                   (-4690 + 123309*t2 + 304000*Power(t2,2) + 
                     225115*Power(t2,3) + 13101*Power(t2,4) - 
                     69145*Power(t2,5) - 18377*Power(t2,6) + 
                     1576*Power(t2,7)) + 
                  t1*(-934 - 76835*t2 - 164479*Power(t2,2) - 
                     89913*Power(t2,3) + 11680*Power(t2,4) + 
                     47762*Power(t2,5) + 34882*Power(t2,6) + 
                     1830*Power(t2,7) - 1877*Power(t2,8))) + 
               s2*(-4991 + 64*Power(t1,8) - 17934*t2 - 
                  8010*Power(t2,2) + 12188*Power(t2,3) + 
                  4204*Power(t2,4) - 3276*Power(t2,5) - 
                  554*Power(t2,6) + 364*Power(t2,7) + 467*Power(t2,8) - 
                  206*Power(t2,9) + 20*Power(t2,10) + 
                  4*Power(t1,7)*(-23 + 215*t2 + 48*Power(t2,2)) + 
                  2*Power(t1,6)*
                   (-50 - 6271*t2 - 3688*Power(t2,2) + 
                     145*Power(t2,3) + 66*Power(t2,4)) + 
                  Power(t1,5)*
                   (6491 + 62950*t2 + 53030*Power(t2,2) + 
                     3488*Power(t2,3) - 4485*Power(t2,4) - 
                     166*Power(t2,5) + 4*Power(t2,6)) - 
                  Power(t1,4)*
                   (30233 + 154398*t2 + 133321*Power(t2,2) + 
                     28264*Power(t2,3) - 22235*Power(t2,4) - 
                     5054*Power(t2,5) + 321*Power(t2,6) + 12*Power(t2,7)\
) + Power(t1,3)*(57584 + 215970*t2 + 155381*Power(t2,2) + 
                     34902*Power(t2,3) - 21230*Power(t2,4) - 
                     19586*Power(t2,5) + 409*Power(t2,6) + 
                     462*Power(t2,7) + 12*Power(t2,8)) + 
                  t1*(26097 + 85968*t2 + 34170*Power(t2,2) - 
                     32819*Power(t2,3) - 6184*Power(t2,4) + 
                     4279*Power(t2,5) - 894*Power(t2,6) - 
                     1929*Power(t2,7) + 715*Power(t2,8) - 59*Power(t2,9)\
) - Power(t1,2)*(54820 + 180874*t2 + 93955*Power(t2,2) - 
                     9229*Power(t2,3) - 8072*Power(t2,4) - 
                     9050*Power(t2,5) - 5971*Power(t2,6) + 
                     1481*Power(t2,7) + 68*Power(t2,8) + 4*Power(t2,9))) \
+ Power(s2,3)*(5022 + 200*Power(t1,7) + 11246*t2 - 22279*Power(t2,2) - 
                  48731*Power(t2,3) - 14348*Power(t2,4) + 
                  2188*Power(t2,5) - 6785*Power(t2,6) - 
                  905*Power(t2,7) + 1986*Power(t2,8) + 42*Power(t2,9) - 
                  76*Power(t2,10) + 
                  2*Power(t1,6)*(1770 + 3649*t2 + 636*Power(t2,2)) + 
                  2*Power(t1,5)*
                   (-10102 - 30826*t2 - 13031*Power(t2,2) + 
                     2682*Power(t2,3) + 509*Power(t2,4)) + 
                  Power(t1,4)*
                   (50221 + 185464*t2 + 129420*Power(t2,2) - 
                     33654*Power(t2,3) - 31053*Power(t2,4) - 
                     334*Power(t2,5) + 94*Power(t2,6)) - 
                  Power(t1,3)*
                   (72029 + 266723*t2 + 199262*Power(t2,2) - 
                     80909*Power(t2,3) - 144149*Power(t2,4) - 
                     26655*Power(t2,5) + 5322*Power(t2,6) + 
                     125*Power(t2,7)) + 
                  Power(t1,2)*
                   (60333 + 198086*t2 + 87590*Power(t2,2) - 
                     129004*Power(t2,3) - 186968*Power(t2,4) - 
                     83118*Power(t2,5) + 8272*Power(t2,6) + 
                     6446*Power(t2,7) - 125*Power(t2,8)) + 
                  t1*(-27083 - 73719*t2 + 29687*Power(t2,2) + 
                     124818*Power(t2,3) + 84646*Power(t2,4) + 
                     51997*Power(t2,5) + 11651*Power(t2,6) - 
                     9424*Power(t2,7) - 1765*Power(t2,8) + 
                     232*Power(t2,9))) + 
               Power(s2,2)*(1758 - 17505*t2 - 42735*Power(t2,2) - 
                  15085*Power(t2,3) + 14399*Power(t2,4) + 
                  832*Power(t2,5) - 4166*Power(t2,6) + 1390*Power(t2,7) + 
                  331*Power(t2,8) - 57*Power(t2,9) - 11*Power(t2,10) + 
                  Power(t2,11) - 2*Power(t1,7)*(949 + 250*t2) + 
                  Power(t1,6)*
                   (14160 + 6486*t2 - 3398*Power(t2,2) - 872*Power(t2,3)) \
+ Power(t1,5)*(-47327 - 31764*t2 + 35860*Power(t2,2) + 
                     22950*Power(t2,3) + 65*Power(t2,4) - 214*Power(t2,5)\
) + Power(t1,4)*(84406 + 47623*t2 - 133281*Power(t2,2) - 
                     132350*Power(t2,3) - 17514*Power(t2,4) + 
                     7187*Power(t2,5) + 289*Power(t2,6) - 4*Power(t2,7)) \
+ Power(t1,3)*(-84290 + 8722*t2 + 247655*Power(t2,2) + 
                     255597*Power(t2,3) + 77293*Power(t2,4) - 
                     23562*Power(t2,5) - 8711*Power(t2,6) + 
                     327*Power(t2,7) + 9*Power(t2,8)) - 
                  Power(t1,2)*
                   (-47016 + 79866*t2 + 267257*Power(t2,2) + 
                     204099*Power(t2,3) + 82638*Power(t2,4) - 
                     6709*Power(t2,5) - 20971*Power(t2,6) - 
                     1687*Power(t2,7) + 590*Power(t2,8) + 5*Power(t2,9)) \
+ t1*(-13825 + 66804*t2 + 162796*Power(t2,2) + 75271*Power(t2,3) + 
                     4659*Power(t2,4) + 9881*Power(t2,5) - 
                     6623*Power(t2,6) - 4779*Power(t2,7) + 
                     370*Power(t2,8) + 199*Power(t2,9) - Power(t2,10))))))*
       T2q(1 - s + s2 - t1,1 - s1 - t1 + t2))/
     ((-1 + t1)*(s - s2 + t1)*Power(-1 + s1 + t1 - t2,2)*(-1 + t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,3)*
       Power(-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2),2)) + 
    (8*(Power(s,8)*(-1 + s2)*(-2 + 2*s1 + 3*t1 - 3*t2)*Power(t1 - t2,2) + 
         Power(s,7)*((-11 + 12*s2)*Power(t1,4) + 
            Power(t1,3)*(27 + Power(s2,3) + 
               s1*(3 - 5*s2 + Power(s2,2)) + 29*t2 - 
               2*Power(s2,2)*(4 + t2) - s2*(19 + 31*t2)) + 
            Power(t1,2)*(-22 + 
               Power(s1,2)*(7 - 11*s2 + 2*Power(s2,2)) - 69*t2 - 
               21*Power(t2,2) - 2*Power(s2,3)*(1 + t2) + 
               7*s2*(2 + 5*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(8 + 33*t2 + 6*Power(t2,2)) + 
               s1*(18 + Power(s2,3) - 14*t2 - Power(s2,2)*(5 + 7*t2) + 
                  2*s2*(-5 + 12*t2))) + 
            t2*(-1 - Power(s1,3)*Power(-1 + s2,2) - 10*t2 - 
               15*Power(t2,2) + 4*Power(t2,3) - Power(s2,3)*(1 + t2) + 
               Power(s2,2)*(5 + 22*t2 + 17*Power(t2,2) + 
                  2*Power(t2,3)) - 
               s2*(3 + 13*t2 + 3*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s1,2)*(1 + 5*t2 + Power(s2,2)*(5 + 4*t2) - 
                  s2*(6 + 11*t2)) + 
               s1*(1 + Power(s2,3) + 8*t2 - 8*Power(t2,2) - 
                  Power(s2,2)*(9 + 21*t2 + 5*Power(t2,2)) + 
                  s2*(7 + 17*t2 + 14*Power(t2,2)))) + 
            t1*(5 + Power(s1,3)*Power(-1 + s2,2) + 32*t2 + 
               57*Power(t2,2) - Power(t2,3) + 
               Power(s2,3)*(1 + 3*t2 + Power(t2,2)) + 
               s2*(-5 - t2 - 13*Power(t2,2) + 3*Power(t2,3)) - 
               Power(s2,2)*(1 + 30*t2 + 42*Power(t2,2) + 
                  6*Power(t2,3)) - 
               Power(s1,2)*(-3 + s2*(2 - 22*t2) + 12*t2 + 
                  Power(s2,2)*(1 + 6*t2)) - 
               s1*(9 + 26*t2 - 19*Power(t2,2) + Power(s2,3)*(1 + t2) - 
                  Power(s2,2)*(1 + 26*t2 + 11*Power(t2,2)) + 
                  s2*(-9 + 7*t2 + 33*Power(t2,2))))) + 
         Power(s,6)*(5 - 37*t1 + 123*Power(t1,2) - 155*Power(t1,3) + 
            79*Power(t1,4) - 15*Power(t1,5) + 25*t2 - 162*t1*t2 + 
            285*Power(t1,2)*t2 - 175*Power(t1,3)*t2 + 
            32*Power(t1,4)*t2 + 43*Power(t2,2) - 165*t1*Power(t2,2) + 
            85*Power(t1,2)*Power(t2,2) - 8*Power(t1,3)*Power(t2,2) + 
            35*Power(t2,3) + 39*t1*Power(t2,3) - 
            18*Power(t1,2)*Power(t2,3) - 28*Power(t2,4) + 
            7*t1*Power(t2,4) + 2*Power(t2,5) - 
            Power(s1,4)*(-1 + s2)*
             (Power(s2,2) - 2*t1 + s2*(-1 + 4*t1 - 3*t2) + t2) - 
            Power(s2,4)*(-1 + t1)*
             (-2*t1 + 2*Power(t1,2) + t2 - 3*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(5 + 47*t2 + 11*Power(t2,2) - 10*Power(t2,3) + 
               9*Power(t2,4) + 2*Power(t2,5) - 
               Power(t1,4)*(27 + 8*t2) + 
               Power(t1,3)*(58 + 123*t2 + 22*Power(t2,2)) + 
               t1*t2*(20 + 144*t2 + 51*Power(t2,2) + 2*Power(t2,3)) - 
               6*Power(t1,2)*
                (6 + 32*t2 + 26*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s2,3)*(-1 + Power(t1,4) - 32*t2 - 73*Power(t2,2) - 
               50*Power(t2,3) - 8*Power(t2,4) + 
               Power(t1,3)*(6 + 7*t2) - 
               Power(t1,2)*(12 + 63*t2 + 25*Power(t2,2)) + 
               t1*(6 + 87*t2 + 107*Power(t2,2) + 25*Power(t2,3))) + 
            s2*(-9 + 19*Power(t1,5) - 37*t2 + 38*Power(t2,2) + 
               34*Power(t2,3) + 22*Power(t2,4) - 2*Power(t2,5) - 
               Power(t1,4)*(53 + 38*t2) + 
               Power(t1,3)*(77 + 50*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-63 + 8*t2 + 81*Power(t2,2) + 
                  32*Power(t2,3)) + 
               t1*(29 + 23*t2 - 119*Power(t2,2) - 100*Power(t2,3) - 
                  13*Power(t2,4))) + 
            Power(s1,3)*(-3*Power(t1,2) - 3*Power(-1 + t2,2) + 
               t1*(-7 + 6*t2) + Power(s2,3)*(7 - 5*t1 + 8*t2) + 
               s2*(11 + 13*Power(t1,2) + t1*(7 - 32*t2) + 2*t2 + 
                  17*Power(t2,2)) - 
               Power(s2,2)*(15 + 5*Power(t1,2) + 20*t2 + 
                  13*Power(t2,2) - t1*(9 + 20*t2))) + 
            Power(s1,2)*(13 + Power(s2,4)*(-1 + t1) + 13*Power(t1,3) - 
               2*t2 - 35*Power(t2,2) + 5*Power(t2,3) - 
               2*Power(t1,2)*(19 + 8*t2) + 
               t1*(-15 + 85*t2 - 2*Power(t2,2)) - 
               Power(s2,3)*(11 + 12*Power(t1,2) + 52*t2 + 
                  21*Power(t2,2) - 6*t1*(4 + 5*t2)) + 
               s2*(-33 - 22*Power(t1,3) - 11*t2 + 26*Power(t2,2) - 
                  24*Power(t2,3) + Power(t1,2)*(41 + 21*t2) + 
                  t1*(7 - 88*t2 + 25*Power(t2,2))) + 
               Power(s2,2)*(32 + 2*Power(t1,3) + 77*t2 + 
                  48*Power(t2,2) + 19*Power(t2,3) + 
                  3*Power(t1,2)*(5 + 3*t2) - 
                  t1*(29 + 51*t2 + 30*Power(t2,2)))) + 
            s1*(-15 + 27*Power(t1,4) - Power(s2,4)*(-1 + Power(t1,2)) + 
               Power(t1,3)*(20 - 73*t2) - 29*t2 - 16*Power(t2,2) + 
               57*Power(t2,3) - 5*Power(t2,4) + 
               Power(t1,2)*(-97 + 77*t2 + 60*Power(t2,2)) + 
               t1*(60 + 97*t2 - 154*Power(t2,2) - 9*Power(t2,3)) + 
               Power(s2,3)*(6 - 7*Power(t1,3) + 72*t2 + 
                  95*Power(t2,2) + 22*Power(t2,3) + 
                  Power(t1,2)*(23 + 35*t2) - 
                  t1*(21 + 117*t2 + 50*Power(t2,2))) + 
               Power(s2,2)*(-24 + 3*Power(t1,4) + 
                  Power(t1,3)*(9 - 16*t2) - 101*t2 - 62*Power(t2,2) - 
                  39*Power(t2,3) - 11*Power(t2,4) + 
                  Power(t1,2)*(4 + 27*t2 + 12*Power(t2,2)) + 
                  t1*(18 + 49*t2 + 3*Power(t2,2) + 12*Power(t2,3))) + 
               s2*(32 - 38*Power(t1,4) + 46*t2 - 56*Power(t2,2) - 
                  49*Power(t2,3) + 13*Power(t2,4) + 
                  Power(t1,3)*(1 + 116*t2) + 
                  Power(t1,2)*(44 - 194*t2 - 105*Power(t2,2)) + 
                  t1*(-45 + 37*t2 + 242*Power(t2,2) + 14*Power(t2,3))))) \
+ Power(s,5)*(-31 + 159*t1 - 421*Power(t1,2) + 581*Power(t1,3) - 
            389*Power(t1,4) + 110*Power(t1,5) - 9*Power(t1,6) - 111*t2 + 
            593*t1*t2 - 991*Power(t1,2)*t2 + 682*Power(t1,3)*t2 - 
            186*Power(t1,4)*t2 + 13*Power(t1,5)*t2 - 134*Power(t2,2) + 
            408*t1*Power(t2,2) - 225*Power(t1,2)*Power(t2,2) - 
            30*Power(t1,3)*Power(t2,2) + 8*Power(t1,4)*Power(t2,2) - 
            54*Power(t2,3) - 152*t1*Power(t2,3) + 
            166*Power(t1,2)*Power(t2,3) - 12*Power(t1,3)*Power(t2,3) + 
            84*Power(t2,4) - 48*t1*Power(t2,4) - 
            7*Power(t1,2)*Power(t2,4) - 12*Power(t2,5) + 
            7*t1*Power(t2,5) + 
            Power(s1,5)*(3*Power(s2,3) + t1 + 
               Power(s2,2)*(-4 + 6*t1 - 3*t2) + s2*(1 - 6*t1 + 2*t2)) + 
            Power(s2,5)*(2*Power(t1,3) - Power(t1,2)*(4 + t2) + 
               t2*(2 + 3*t2 + Power(t2,2)) - 
               t1*(-2 + t2 + 2*Power(t2,2))) + 
            Power(s2,4)*(2 + 34*t2 + 72*Power(t2,2) + 55*Power(t2,3) + 
               13*Power(t2,4) - Power(t1,3)*(7 + 9*t2) + 
               Power(t1,2)*(16 + 58*t2 + 27*Power(t2,2)) - 
               t1*(11 + 83*t2 + 103*Power(t2,2) + 31*Power(t2,3))) - 
            Power(s2,3)*(-5 + Power(t1,5) - 44*t2 - 107*Power(t2,2) - 
               39*Power(t2,3) + 27*Power(t2,4) + 6*Power(t2,5) - 
               Power(t1,4)*(17 + 24*t2) + 
               Power(t1,3)*(37 + 198*t2 + 65*Power(t2,2)) - 
               Power(t1,2)*(32 + 389*t2 + 322*Power(t2,2) + 
                  56*Power(t2,3)) + 
               t1*(16 + 259*t2 + 368*Power(t2,2) + 114*Power(t2,3) + 
                  8*Power(t2,4))) + 
            s2*(49 + 15*Power(t1,6) + 140*t2 - 103*Power(t2,2) - 
               135*Power(t2,3) - 60*Power(t2,4) + 12*Power(t2,5) - 
               Power(t2,6) - 8*Power(t1,5)*(9 + 2*t2) + 
               Power(t1,4)*(137 - 36*t2 - 37*Power(t2,2)) + 
               Power(t1,3)*(-133 + 250*t2 + 353*Power(t2,2) + 
                  58*Power(t2,3)) - 
               Power(t1,2)*(-117 + 106*t2 + 754*Power(t2,2) + 
                  298*Power(t2,3) + 17*Power(t2,4)) + 
               t1*(-113 - 232*t2 + 522*Power(t2,2) + 427*Power(t2,3) + 
                  41*Power(t2,4) - 2*Power(t2,5))) - 
            Power(s2,2)*(27 + 136*t2 + 54*Power(t2,2) + 5*Power(t2,3) + 
               43*Power(t2,4) + 13*Power(t2,5) + 
               12*Power(t1,5)*(3 + t2) - 
               Power(t1,4)*(155 + 189*t2 + 28*Power(t2,2)) + 
               Power(t1,3)*(250 + 461*t2 + 218*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(-152 - 223*t2 - 309*Power(t2,2) + 
                  12*Power(t2,4)) - 
               t1*(6 + 197*t2 - 86*Power(t2,2) + 40*Power(t2,3) + 
                  78*Power(t2,4) + 8*Power(t2,5))) + 
            Power(s1,4)*(3 + 3*Power(s2,4) - 7*Power(t1,2) + 
               2*Power(s2,3)*(-12 + 8*t1 - 11*t2) - 6*t2 + 
               t1*(7 + 2*t2) + 
               Power(s2,2)*(31 + 17*t2 + 14*Power(t2,2) - 
                  12*t1*(1 + 2*t2)) + 
               s2*(-15 + 7*Power(t1,2) + 16*t2 - 8*Power(t2,2) + 
                  2*t1*(-8 + 7*t2))) + 
            Power(s1,3)*(-12*Power(t1,3) + 
               Power(s2,4)*(-17 + 2*t1 - 19*t2) + 
               Power(t1,2)*(15 + 22*t2) + 
               t1*(29 - 34*t2 - 7*Power(t2,2)) + 
               3*(1 - 8*t2 + 6*Power(t2,2)) + 
               Power(s2,3)*(57 + 27*Power(t1,2) + 120*t2 + 
                  56*Power(t2,2) - t1*(69 + 71*t2)) - 
               Power(s2,2)*(59 + 15*Power(t1,3) + 74*t2 - 
                  27*Power(t1,2)*t2 + 10*Power(t2,2) + 
                  24*Power(t2,3) + t1*(-77 + 24*t2 - 22*Power(t2,2))) + 
               s2*(24 + 54*Power(t1,3) + 15*t2 - 83*Power(t2,2) + 
                  13*Power(t2,3) - Power(t1,2)*(44 + 97*t2) + 
                  t1*(-57 + 158*t2 + 17*Power(t2,2)))) + 
            Power(s1,2)*(-60 - 8*Power(t1,4) + 31*t2 + 
               114*Power(t2,2) - 30*Power(t2,3) + 
               Power(s2,5)*(2 - t1 + t2) + Power(t1,3)*(-107 + 24*t2) + 
               Power(t1,2)*(134 + 116*t2 - 27*Power(t2,2)) + 
               t1*(68 - 293*t2 + 5*Power(t2,2) + 11*Power(t2,3)) + 
               2*Power(s2,4)*
                (16 + 5*Power(t1,2) + 42*t2 + 21*Power(t2,2) - 
                  t1*(23 + 15*t2)) + 
               Power(s2,3)*(-62 + 2*Power(t1,3) - 136*t2 - 
                  199*Power(t2,2) - 64*Power(t2,3) - 
                  Power(t1,2)*(37 + 42*t2) + 
                  3*t1*(31 + 43*t2 + 30*Power(t2,2))) + 
               s2*(73 + 15*Power(t1,4) + Power(t1,3)*(109 - 103*t2) - 
                  83*t2 - 38*Power(t2,2) + 126*Power(t2,3) - 
                  11*Power(t2,4) + 
                  Power(t1,2)*(-253 + 35*t2 + 153*Power(t2,2)) + 
                  t1*(37 + 249*t2 - 231*Power(t2,2) - 54*Power(t2,3))) \
+ Power(s2,2)*(3 - 6*Power(t1,4) + 29*t2 - 8*Power(t2,2) - 
                  28*Power(t2,3) + 18*Power(t2,4) + 
                  Power(t1,3)*(17 + 60*t2) - 
                  6*Power(t1,2)*(-7 + 33*t2 + 14*Power(t2,2)) + 
                  t1*(-77 + 126*t2 + 200*Power(t2,2) + 12*Power(t2,3)))) \
+ s1*(86 + 43*Power(t1,5) + 108*t2 + 6*Power(t2,2) - 177*Power(t2,3) + 
               30*Power(t2,4) - Power(t1,4)*(33 + 95*t2) + 
               Power(s2,5)*(-2 + t1 + Power(t1,2) - 5*t2 + 3*t1*t2 - 
                  2*Power(t2,2)) + 
               Power(t1,3)*(-134 + 338*t2 + 47*Power(t2,2)) + 
               Power(t1,2)*(303 - 112*t2 - 405*Power(t2,2) + 
                  19*Power(t2,3)) + 
               t1*(-265 - 293*t2 + 491*Power(t2,2) + 70*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s2,4)*(-19 + 11*Power(t1,3) - 102*t2 - 
                  122*Power(t2,2) - 39*Power(t2,3) - 
                  Power(t1,2)*(47 + 35*t2) + 
                  t1*(55 + 145*t2 + 59*Power(t2,2))) + 
               Power(s2,3)*(17 - 13*Power(t1,4) + 17*t2 + 
                  42*Power(t2,2) + 130*Power(t2,3) + 33*Power(t2,4) + 
                  Power(t1,3)*(4 + 46*t2) - 
                  3*Power(t1,2)*(-9 + 63*t2 + 13*Power(t2,2)) + 
                  t1*(-35 + 134*t2 + 50*Power(t2,2) - 27*Power(t2,3))) + 
               Power(s2,2)*(62 + 57*Power(t1,4) + 3*Power(t1,5) + 
                  136*t2 + 70*Power(t2,2) + 94*Power(t2,3) + 
                  38*Power(t2,4) - 5*Power(t2,5) + 
                  Power(t1,3)*(58 - 106*t2 - 43*Power(t2,2)) + 
                  Power(t1,2)*
                   (-193 + 282*t2 + 253*Power(t2,2) + 69*Power(t2,3)) - 
                  t1*(-13 + 270*t2 + 323*Power(t2,2) + 
                     242*Power(t2,3) + 24*Power(t2,4))) + 
               s2*(-136 - 67*Power(t1,5) - 75*t2 + 217*Power(t2,2) + 
                  98*Power(t2,3) - 72*Power(t2,4) + 5*Power(t2,5) + 
                  Power(t1,4)*(71 + 165*t2) - 
                  Power(t1,3)*(143 + 535*t2 + 88*Power(t2,2)) + 
                  Power(t1,2)*
                   (123 + 619*t2 + 488*Power(t2,2) - 46*Power(t2,3)) + 
                  t1*(152 - 136*t2 - 744*Power(t2,2) + 48*Power(t2,3) + 
                     31*Power(t2,4))))) - 
         Power(s,4)*(-86 + 409*t1 - 943*Power(t1,2) + 1327*Power(t1,3) - 
            1125*Power(t1,4) + 514*Power(t1,5) - 98*Power(t1,6) + 
            2*Power(t1,7) + Power(s1,6)*s2*
             (3*Power(s2,2) - 2*t1 + s2*(-2 + 4*t1 - t2)) - 221*t2 + 
            1237*t1*t2 - 2391*Power(t1,2)*t2 + 2084*Power(t1,3)*t2 - 
            840*Power(t1,4)*t2 + 133*Power(t1,5)*t2 - 2*Power(t1,6)*t2 - 
            223*Power(t2,2) + 745*t1*Power(t2,2) - 
            627*Power(t1,2)*Power(t2,2) + 18*Power(t1,3)*Power(t2,2) + 
            84*Power(t1,4)*Power(t2,2) + 3*Power(t1,5)*Power(t2,2) - 
            47*Power(t2,3) - 272*t1*Power(t2,3) + 
            478*Power(t1,2)*Power(t2,3) - 140*Power(t1,3)*Power(t2,3) - 
            17*Power(t1,4)*Power(t2,3) + 140*Power(t2,4) - 
            140*t1*Power(t2,4) - 14*Power(t1,2)*Power(t2,4) + 
            23*Power(t1,3)*Power(t2,4) - 30*Power(t2,5) + 
            35*t1*Power(t2,5) - 9*Power(t1,2)*Power(t2,5) + 
            Power(s2,6)*t2*(2 + 2*Power(t1,2) + 3*t2 + Power(t2,2) - 
               t1*(4 + 3*t2)) + 
            Power(s1,5)*(6*Power(s2,4) - 5*(-1 + t1)*t1 + 
               Power(s2,3)*(-23 + 18*t1 - 20*t2) + 
               s2*(-5 - 19*t1 + 19*Power(t1,2) + 10*t2) + 
               Power(s2,2)*(17 - 10*Power(t1,2) + t1*(2 - 12*t2) - 
                  2*t2 + 5*Power(t2,2))) + 
            Power(s2,5)*(-2 - 2*Power(t1,4) - 2*t2 + 7*Power(t2,2) + 
               13*Power(t2,3) + 7*Power(t2,4) + Power(t1,3)*(8 + t2) + 
               4*Power(t1,2)*(-3 - t2 + Power(t2,2)) + 
               t1*(8 + 5*t2 - 11*Power(t2,2) - 10*Power(t2,3))) + 
            Power(s2,4)*(5 - 4*Power(t1,5) + 76*t2 + 97*Power(t2,2) + 
               30*Power(t2,3) - 20*Power(t2,4) - 9*Power(t2,5) + 
               Power(t1,4)*(21 + 13*t2) - 
               Power(t1,3)*(44 + 99*t2 + 48*Power(t2,2)) + 
               Power(t1,2)*(46 + 235*t2 + 196*Power(t2,2) + 
                  69*Power(t2,3)) - 
               t1*(24 + 225*t2 + 245*Power(t2,2) + 98*Power(t2,3) + 
                  21*Power(t2,4))) + 
            Power(s2,3)*(18 + Power(t1,6) + Power(t1,5)*(4 - 22*t2) + 
               13*t2 + 148*Power(t2,2) + 35*Power(t2,3) - 
               83*Power(t2,4) - 16*Power(t2,5) - Power(t2,6) + 
               Power(t1,4)*(-24 + 242*t2 + 63*Power(t2,2)) - 
               Power(t1,3)*(-20 + 697*t2 + 428*Power(t2,2) + 
                  46*Power(t2,3)) + 
               Power(t1,2)*(29 + 769*t2 + 799*Power(t2,2) + 
                  98*Power(t2,3) - 15*Power(t2,4)) + 
               t1*(-48 - 305*t2 - 582*Power(t2,2) - 80*Power(t2,3) + 
                  100*Power(t2,4) + 20*Power(t2,5))) + 
            Power(s2,2)*(-53 - 93*t2 - 87*Power(t2,2) - 
               179*Power(t2,3) - 175*Power(t2,4) - 42*Power(t2,5) + 
               Power(t2,6) + Power(t1,6)*(26 + 8*t2) - 
               Power(t1,5)*(244 + 155*t2 + 12*Power(t2,2)) + 
               Power(t1,4)*(762 + 572*t2 + 109*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,3)*(-1029 - 720*t2 - 91*Power(t2,2) + 
                  192*Power(t2,3) + 28*Power(t2,4)) - 
               Power(t1,2)*(-565 - 211*t2 + 75*Power(t2,2) + 
                  575*Power(t2,3) + 220*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(-27 + 177*t2 + 156*Power(t2,2) + 583*Power(t2,3) + 
                  380*Power(t2,4) + 47*Power(t2,5))) - 
            s2*(-106 + 6*Power(t1,7) - 97*t2 + 404*Power(t2,2) + 
               382*Power(t2,3) + 112*Power(t2,4) - 35*Power(t2,5) + 
               5*Power(t2,6) + Power(t1,6)*(-53 + 5*t2) - 
               2*Power(t1,5)*(-20 + 79*t2 + 24*Power(t2,2)) + 
               Power(t1,4)*(276 + 881*t2 + 563*Power(t2,2) + 
                  57*Power(t2,3)) - 
               Power(t1,3)*(522 + 1563*t2 + 1917*Power(t2,2) + 
                  416*Power(t2,3) + 23*Power(t2,4)) + 
               Power(t1,2)*(179 + 993*t2 + 2843*Power(t2,2) + 
                  1106*Power(t2,3) + 45*Power(t2,4) + 6*Power(t2,5)) - 
               t1*(-180 + 61*t2 + 1845*Power(t2,2) + 1178*Power(t2,3) + 
                  75*Power(t2,4) - 14*Power(t2,5) + 3*Power(t2,6))) + 
            Power(s1,4)*(19 + 4*Power(s2,5) + 12*Power(t1,3) + 
               Power(s2,4)*(-32 + 6*t1 - 34*t2) + 
               Power(t1,2)*(-7 + t2) - 15*t2 + 2*t1*(-7 + 5*t2) + 
               Power(s2,3)*(39 + 14*Power(t1,2) + 81*t2 + 
                  50*Power(t2,2) - 2*t1*(32 + 31*t2)) + 
               Power(s2,2)*(10 - 20*Power(t1,3) - 34*t2 + 
                  38*Power(t2,2) - 10*Power(t2,3) + 
                  5*Power(t1,2)*(8 + 13*t2) - 
                  2*t1*(-13 + 35*t2 + Power(t2,2))) + 
               s2*(-42 + 25*Power(t1,3) + 60*t2 - 40*Power(t2,2) - 
                  Power(t1,2)*(17 + 80*t2) + 
                  t1*(12 + 49*t2 + 18*Power(t2,2)))) + 
            Power(s1,3)*(-46 - Power(s2,6) + 8*Power(t1,4) - 77*t2 + 
               45*Power(t2,2) - Power(s2,5)*(3*t1 + 19*t2) - 
               Power(t1,3)*(54 + 29*t2) + 
               t1*(167 - 18*t2 - 35*Power(t2,2)) + 
               Power(t1,2)*(-77 + 85*t2 + 6*Power(t2,2)) + 
               Power(s2,4)*(39 + 27*Power(t1,2) + 135*t2 + 
                  75*Power(t2,2) - t1*(67 + 16*t2)) + 
               Power(s2,3)*(-9 - 35*Power(t1,3) - 18*t2 - 
                  82*Power(t2,2) - 59*Power(t2,3) + 
                  Power(t1,2)*(9 + 29*t2) + 
                  t1*(28 + 20*t2 + 51*Power(t2,2))) + 
               s2*(212 - 55*Power(t1,4) + 8*t2 - 225*Power(t2,2) + 
                  65*Power(t2,3) + Power(t1,3)*(126 + 55*t2) + 
                  Power(t1,2)*(131 - 293*t2 + 72*Power(t2,2)) + 
                  t1*(-463 + 355*t2 + 81*Power(t2,2) - 37*Power(t2,3))) \
+ Power(s2,2)*(-157 + 5*Power(t1,4) + 7*t2 + 35*Power(t2,2) - 
                  83*Power(t2,3) + 10*Power(t2,4) + 
                  Power(t1,3)*(44 + 50*t2) - 
                  Power(t1,2)*(147 + 116*t2 + 123*Power(t2,2)) + 
                  2*t1*(123 - 61*t2 + 79*Power(t2,2) + 19*Power(t2,3)))) \
- Power(s1,2)*(97 - 32*Power(t1,5) + 3*Power(s2,6)*(-1 + t1 - t2) - 
               205*t2 - 232*Power(t2,2) + 75*Power(t2,3) + 
               24*Power(t1,4)*(-5 + 2*t2) + 
               Power(t1,3)*(548 - 63*t2 - 20*Power(t2,2)) + 
               t1*(-86 + 861*t2 + 77*Power(t2,2) - 55*Power(t2,3)) + 
               Power(t1,2)*(-407 - 647*t2 + 118*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s2,5)*(5 + 8*Power(t1,2) - 13*t2 - 
                  33*Power(t2,2) + t1*(-13 + 4*t2)) + 
               Power(s2,4)*(-14 - 26*Power(t1,3) + 78*t2 + 
                  194*Power(t2,2) + 81*Power(t2,3) + 
                  5*Power(t1,2)*(7 + 3*t2) + 
                  t1*(5 - 96*t2 + 7*Power(t2,2))) + 
               Power(s2,3)*(67 + 34*Power(t1,4) + 35*t2 + 
                  181*Power(t2,2) + 3*Power(t2,3) - 32*Power(t2,4) - 
                  Power(t1,3)*(52 + 127*t2) + 
                  Power(t1,2)*(85 + 214*t2 + 132*Power(t2,2)) - 
                  t1*(134 + 143*t2 + 286*Power(t2,2) + 32*Power(t2,3))) \
+ s2*(82 + 70*Power(t1,5) + 551*t2 - 38*Power(t2,2) - 
                  325*Power(t2,3) + 55*Power(t2,4) - 
                  5*Power(t1,4)*(-21 + 44*t2) + 
                  Power(t1,3)*(-881 + 525*t2 + 197*Power(t2,2)) + 
                  Power(t1,2)*
                   (1351 + 236*t2 - 702*Power(t2,2) - 14*Power(t2,3)) \
+ t1*(-727 - 1239*t2 + 786*Power(t2,2) + 247*Power(t2,3) - 
                     33*Power(t2,4))) + 
               Power(s2,2)*(-167 - 10*Power(t1,5) - 122*t2 + 
                  247*Power(t2,2) + 78*Power(t2,3) - 71*Power(t2,4) + 
                  5*Power(t2,5) + Power(t1,4)*(-52 + 60*t2) + 
                  Power(t1,3)*(253 - 225*t2 - 18*Power(t2,2)) + 
                  Power(t1,2)*
                   (-586 + 422*t2 + 196*Power(t2,2) - 79*Power(t2,3)) + 
                  t1*(562 - 162*t2 - 632*Power(t2,2) + 67*Power(t2,3) + 
                     42*Power(t2,4)))) + 
            s1*(214 - 27*Power(t1,6) + 120*t2 - 112*Power(t2,2) - 
               314*Power(t2,3) + 75*Power(t2,4) + 
               Power(t1,5)*(64 + 50*t2) - 
               Power(t1,4)*(111 + 374*t2 + 8*Power(t2,2)) + 
               Power(t1,3)*(-116 + 730*t2 + 326*Power(t2,2) - 
                  26*Power(t2,3)) + 
               t1*(-682 - 316*t2 + 1031*Power(t2,2) + 249*Power(t2,3) - 
                  70*Power(t2,4)) + 
               Power(t1,2)*(658 - 210*t2 - 1243*Power(t2,2) + 
                  54*Power(t2,3) + 11*Power(t2,4)) - 
               Power(s2,6)*(2 + 2*Power(t1,2) + 6*t2 + 3*Power(t2,2) - 
                  2*t1*(2 + 3*t2)) - 
               Power(s2,5)*(-4 + 3*Power(t1,3) + 2*t2 + 
                  26*Power(t2,2) + 25*Power(t2,3) - 
                  2*Power(t1,2)*(5 + 2*t2) + 
                  t1*(11 + 2*t2 - 17*Power(t2,2))) + 
               Power(s2,4)*(-32 - 5*Power(t1,4) - 99*t2 + 
                  9*Power(t2,2) + 111*Power(t2,3) + 43*Power(t2,4) + 
                  Power(t1,3)*(31 + 10*t2) - 
                  Power(t1,2)*(79 + 125*t2 + 81*Power(t2,2)) + 
                  t1*(85 + 214*t2 + 69*Power(t2,2) + 38*Power(t2,3))) + 
               Power(s2,3)*(29 + Power(t1,5) + 37*t2 + 18*Power(t2,2) + 
                  243*Power(t2,3) + 43*Power(t2,4) - 5*Power(t2,5) + 
                  2*Power(t1,4)*(38 + 7*t2) + 
                  Power(t1,3)*(-173 + 129*t2 - 55*Power(t2,2)) + 
                  Power(t1,2)*
                   (143 - 231*t2 + 134*Power(t2,2) + 104*Power(t2,3)) - 
                  t1*(76 - 51*t2 + 118*Power(t2,2) + 342*Power(t2,3) + 
                     59*Power(t2,4))) + 
               Power(s2,2)*(38 - Power(t1,6) - 58*t2 + 
                  275*Power(t2,2) + 405*Power(t2,3) + 102*Power(t2,4) - 
                  23*Power(t2,5) + Power(t2,6) - 
                  2*Power(t1,5)*(35 + 13*t2) + 
                  Power(t1,4)*(-231 + 253*t2 + 87*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (-516 + 376*t2 + 291*Power(t2,2) + 38*Power(t2,3)) + 
                  Power(t1,2)*
                   (-1048 + 573*t2 + 1387*Power(t2,2) + 
                     492*Power(t2,3) + Power(t2,4)) + 
                  2*t1*(140 + 5*t2 - 597*Power(t2,2) - 
                     458*Power(t2,3) - 35*Power(t2,4) + 7*Power(t2,5))) \
+ s2*(-204 + 53*Power(t1,6) + 376*t2 + 716*Power(t2,2) + 
                  108*Power(t2,3) - 190*Power(t2,4) + 25*Power(t2,5) - 
                  Power(t1,5)*(141 + 95*t2) + 
                  Power(t1,4)*(812 + 595*t2 - 18*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1831 - 1767*t2 - 282*Power(t2,2) + 94*Power(t2,3)) \
+ Power(t1,2)*(1321 + 2709*t2 + 1466*Power(t2,2) - 347*Power(t2,3) - 
                     19*Power(t2,4)) - 
                  t1*(10 + 1818*t2 + 2029*Power(t2,2) - 
                     344*Power(t2,3) - 150*Power(t2,4) + 15*Power(t2,5)))\
)) + Power(s,3)*(-132 + 622*t1 - 1301*Power(t1,2) + 1688*Power(t1,3) - 
            1562*Power(t1,4) + 1010*Power(t1,5) - 389*Power(t1,6) + 
            64*Power(t1,7) + Power(s1,7)*Power(s2,2)*(s2 + t1) + 
            Power(s1,6)*s2*(3*Power(s2,3) + 8*(-1 + t1)*t1 + 
               Power(s2,2)*(-7 + 8*t1 - 6*t2) + 
               s2*(3 - 10*Power(t1,2) + t1*(9 - 2*t2) - 4*t2)) - 
            240*t2 + 1378*t1*t2 - 3053*Power(t1,2)*t2 + 
            3382*Power(t1,3)*t2 - 2010*Power(t1,4)*t2 + 
            638*Power(t1,5)*t2 - 97*Power(t1,6)*t2 + 2*Power(t1,7)*t2 - 
            181*Power(t2,2) + 668*t1*Power(t2,2) - 
            754*Power(t1,2)*Power(t2,2) + 201*Power(t1,3)*Power(t2,2) + 
            82*Power(t1,4)*Power(t2,2) - 5*Power(t1,5)*Power(t2,2) - 
            11*Power(t1,6)*Power(t2,2) + 5*Power(t2,3) - 
            362*t1*Power(t2,3) + 759*Power(t1,2)*Power(t2,3) - 
            432*Power(t1,3)*Power(t2,3) + 9*Power(t1,4)*Power(t2,3) + 
            21*Power(t1,5)*Power(t2,3) + 140*Power(t2,4) - 
            220*t1*Power(t2,4) + 31*Power(t1,2)*Power(t2,4) + 
            65*Power(t1,3)*Power(t2,4) - 17*Power(t1,4)*Power(t2,4) - 
            40*Power(t2,5) + 70*t1*Power(t2,5) - 
            36*Power(t1,2)*Power(t2,5) + 5*Power(t1,3)*Power(t2,5) + 
            Power(s2,6)*(-1 + t1)*t2*
             (2 + 2*Power(t1,2) + 7*t2 + 5*Power(t2,2) - t1*(4 + 7*t2)) \
+ Power(s2,5)*(-6 + Power(t1,4)*(-6 + t2) - 6*t2 + 31*Power(t2,2) + 
               48*Power(t2,3) + 16*Power(t2,4) - 5*Power(t2,5) + 
               Power(t1,3)*(24 + 3*t2 - 28*Power(t2,2)) + 
               Power(t1,2)*(-36 - 15*t2 + 87*Power(t2,2) + 
                  56*Power(t2,3)) + 
               t1*(24 + 17*t2 - 90*Power(t2,2) - 104*Power(t2,3) - 
                  24*Power(t2,4))) + 
            Power(s2,4)*(5 + 2*Power(t1,6) + 114*t2 + 144*Power(t2,2) + 
               96*Power(t2,3) + 11*Power(t2,4) - 11*Power(t2,5) - 
               Power(t2,6) + Power(t1,5)*(-7 + 3*t2) + 
               Power(t1,4)*(13 + 76*t2 + 42*Power(t2,2)) - 
               Power(t1,3)*(22 + 360*t2 + 310*Power(t2,2) + 
                  75*Power(t2,3)) + 
               Power(t1,2)*(28 + 594*t2 + 638*Power(t2,2) + 
                  255*Power(t2,3) + Power(t2,4)) + 
               t1*(-19 - 427*t2 - 514*Power(t2,2) - 276*Power(t2,3) - 
                  13*Power(t2,4) + 28*Power(t2,5))) - 
            Power(s2,2)*(82 + 63*t2 + 453*Power(t2,2) + 
               704*Power(t2,3) + 380*Power(t2,4) + 52*Power(t2,5) - 
               6*Power(t2,6) + 2*Power(t1,7)*(6 + t2) + 
               2*Power(t1,6)*(-113 - 33*t2 + Power(t2,2)) + 
               Power(t1,5)*(1001 + 261*t2 - 51*Power(t2,2) - 
                  18*Power(t2,3)) + 
               Power(t1,4)*(-1902 - 159*t2 + 691*Power(t2,2) + 
                  321*Power(t2,3) + 22*Power(t2,4)) - 
               Power(t1,3)*(-1698 + 522*t2 + 2275*Power(t2,2) + 
                  1477*Power(t2,3) + 282*Power(t2,4) + 8*Power(t2,5)) + 
               Power(t1,2)*(-554 + 858*t2 + 3124*Power(t2,2) + 
                  2904*Power(t2,3) + 841*Power(t2,4) + 63*Power(t2,5)) \
+ t1*(-111 - 437*t2 - 1944*Power(t2,2) - 2434*Power(t2,3) - 
                  963*Power(t2,4) - 84*Power(t2,5) + 3*Power(t2,6))) + 
            Power(s2,3)*(58 + 3*t2 + 97*Power(t2,2) - 224*Power(t2,3) - 
               308*Power(t2,4) - 56*Power(t2,5) - 4*Power(t2,6) + 
               Power(t1,6)*(-17 + 10*t2) + 
               Power(t1,5)*(37 - 219*t2 - 34*Power(t2,2)) + 
               2*Power(t1,4)*
                (40 + 477*t2 + 181*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,3)*(-350 - 1651*t2 - 970*Power(t2,2) + 
                  73*Power(t2,3) + 43*Power(t2,4)) + 
               Power(t1,2)*(455 + 1269*t2 + 1087*Power(t2,2) - 
                  365*Power(t2,3) - 300*Power(t2,4) - 36*Power(t2,5)) + 
               t1*(-263 - 366*t2 - 542*Power(t2,2) + 504*Power(t2,3) + 
                  550*Power(t2,4) + 105*Power(t2,5) + 5*Power(t2,6))) + 
            s2*(108 + Power(t1,8) - 180*t2 - 746*Power(t2,2) - 
               501*Power(t2,3) - 69*Power(t2,4) + 60*Power(t2,5) - 
               10*Power(t2,6) + Power(t1,7)*(-17 + 7*t2) - 
               Power(t1,6)*(163 + 181*t2 + 28*Power(t2,2)) + 
               Power(t1,5)*(1179 + 1305*t2 + 489*Power(t2,2) + 
                  35*Power(t2,3)) - 
               Power(t1,4)*(2673 + 3786*t2 + 2348*Power(t2,2) + 
                  355*Power(t2,3) + 22*Power(t2,4)) + 
               Power(t1,3)*(2701 + 5413*t2 + 5410*Power(t2,2) + 
                  1243*Power(t2,3) + 64*Power(t2,4) + 10*Power(t2,5)) - 
               Power(t1,2)*(1081 + 4005*t2 + 6338*Power(t2,2) + 
                  2141*Power(t2,3) - 28*Power(t2,4) + 12*Power(t2,5) + 
                  3*Power(t2,6)) + 
               t1*(-55 + 1427*t2 + 3561*Power(t2,2) + 
                  1719*Power(t2,3) - 2*Power(t2,4) - 55*Power(t2,5) + 
                  12*Power(t2,6))) + 
            Power(s1,5)*(4*Power(s2,5) + 
               t1*(10 - 20*t1 + 11*Power(t1,2)) + 
               Power(s2,4)*(-14 + 6*t1 - 15*t2) + 
               s2*(-42 + 36*t1 - 17*Power(t1,3) + 
                  Power(t1,2)*(22 - 19*t2) + 20*t2) - 
               Power(s2,3)*(4 + 15*Power(t1,2) - 20*t2 - 
                  15*Power(t2,2) + t1*(11 + 26*t2)) + 
               Power(s2,2)*(51 - 5*Power(t1,3) - 33*t2 + 
                  20*Power(t2,2) + Power(t1,2)*(39 + 48*t2) - 
                  t1*(63 + 27*t2 + 5*Power(t2,2)))) + 
            Power(s1,4)*(46 - 13*Power(t1,4) + 
               Power(s2,5)*(14 - 22*t1 - 21*t2) - 20*t2 + 
               Power(t1,2)*(85 + 4*t2) - Power(t1,3)*(17 + 9*t2) + 
               2*t1*(-51 + 10*t2) + 
               Power(s2,4)*(28 + 35*Power(t1,2) + 50*t2 + 
                  29*Power(t2,2) - t1*(64 + t2)) + 
               s2*(62 - 55*Power(t1,4) + 208*t2 - 80*Power(t2,2) + 
                  2*Power(t1,3)*(47 + 55*t2) + 
                  Power(t1,2)*(89 - 160*t2 + 3*Power(t2,2)) + 
                  t1*(-191 - 151*t2 + 72*Power(t2,2))) + 
               Power(s2,3)*(-59*Power(t1,3) + 
                  Power(t1,2)*(137 + 84*t2) + 
                  5*t1*(-19 + 11*t2 + 5*Power(t2,2)) - 
                  2*(-1 + 3*t2 + 5*Power(t2,2) + 10*Power(t2,3))) + 
               Power(s2,2)*(-191 + 20*Power(t1,4) + 
                  Power(t1,3)*(10 - 20*t2) - 111*t2 + 124*Power(t2,2) - 
                  40*Power(t2,3) + 
                  Power(t1,2)*(-367 + t2 - 72*Power(t2,2)) + 
                  t1*(530 + 19*t2 - 21*Power(t2,2) + 20*Power(t2,3)))) + 
            Power(s1,3)*(-5*Power(s2,6)*(-1 + t1) + 3*Power(t1,5) + 
               Power(t1,4)*(26 + 31*t2) + 
               t1*(544 + 196*t2 - 70*Power(t2,2)) - 
               2*Power(t1,3)*(-91 + 22*t2 + 2*Power(t2,2)) + 
               3*(-50 - 51*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(-605 - 26*t2 + 24*Power(t2,2)) + 
               Power(s2,5)*(-27 - 35*Power(t1,2) - 58*t2 + 
                  44*Power(t2,2) + t1*(62 + 90*t2)) + 
               Power(s2,4)*(-55 + 15*Power(t1,3) - 70*t2 - 
                  55*Power(t2,2) - 26*Power(t2,3) - 
                  Power(t1,2)*(94 + 81*t2) + 
                  t1*(134 + 155*t2 - 61*Power(t2,2))) + 
               Power(s2,3)*(11 - 27*Power(t1,4) + 264*t2 + 
                  106*Power(t2,2) - 16*Power(t2,3) + 15*Power(t2,4) + 
                  2*Power(t1,3)*(122 + 65*t2) - 
                  Power(t1,2)*(437 + 141*t2 + 118*Power(t2,2)) - 
                  t1*(-209 + 193*t2 + 220*Power(t2,2) + 4*Power(t2,3))) \
+ s2*(321 - 452*t2 - 482*Power(t2,2) + 130*Power(t2,3) + 
                  10*Power(t1,4)*(-10 + 7*t2) + 
                  Power(t1,3)*(128 + 72*t2 - 148*Power(t2,2)) + 
                  t1*(-793 + 1429*t2 + 482*Power(t2,2) - 
                     148*Power(t2,3)) + 
                  Power(t1,2)*
                   (444 - 1115*t2 + 130*Power(t2,2) + 28*Power(t2,3))) \
+ Power(s2,2)*(86 + 10*Power(t1,5) + 691*t2 + 51*Power(t2,2) - 
                  210*Power(t2,3) + 40*Power(t2,4) - 
                  Power(t1,4)*(6 + 95*t2) + 
                  Power(t1,3)*(-421 + 178*t2 + 102*Power(t2,2)) + 
                  Power(t1,2)*
                   (1043 + 871*t2 - 242*Power(t2,2) + 28*Power(t2,3)) + 
                  t1*(-712 - 1653*t2 + 313*Power(t2,2) + 
                     138*Power(t2,3) - 25*Power(t2,4)))) - 
            Power(s1,2)*(55 + 23*Power(t1,6) + 
               Power(t1,5)*(125 - 22*t2) + 
               Power(s2,6)*(-1 + t1)*(-7 + 7*t1 - 15*t2) - 503*t2 - 
               313*Power(t2,2) + 100*Power(t2,3) + 
               Power(t1,4)*(-970 + 143*t2 - 5*Power(t2,2)) + 
               t1*(166 + 1965*t2 + 366*Power(t2,2) - 110*Power(t2,3)) + 
               Power(t1,3)*(1749 + 736*t2 - 64*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(-1148 - 2319*t2 + 22*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(s2,5)*(-9 + 6*Power(t1,3) - 102*t2 - 
                  90*Power(t2,2) + 46*Power(t2,3) - 
                  21*Power(t1,2)*(1 + 6*t2) + 
                  6*t1*(4 + 38*t2 + 23*Power(t2,2))) + 
               Power(s2,4)*(-99 + 34*Power(t1,4) - 155*t2 - 
                  67*Power(t2,2) - 5*Power(t2,3) - 9*Power(t2,4) + 
                  Power(t1,3)*(37 + 54*t2) - 
                  Power(t1,2)*(275 + 290*t2 + 58*Power(t2,2)) + 
                  t1*(303 + 391*t2 + 131*Power(t2,2) - 129*Power(t2,3))\
) + Power(s2,3)*(65 - 14*Power(t1,5) + 266*t2 + 870*Power(t2,2) + 
                  234*Power(t2,3) - 13*Power(t2,4) + 6*Power(t2,5) + 
                  13*Power(t1,4)*(-17 + 2*t2) + 
                  Power(t1,3)*(673 + 119*t2 + 12*Power(t2,2)) - 
                  Power(t1,2)*
                   (562 + 173*t2 - 513*Power(t2,2) + 8*Power(t2,3)) - 
                  t1*(-59 + 238*t2 + 1305*Power(t2,2) + 
                     424*Power(t2,3) + 2*Power(t2,4))) + 
               s2*(399 - 65*Power(t1,6) + 726*t2 - 709*Power(t2,2) - 
                  568*Power(t2,3) + 110*Power(t2,4) + 
                  8*Power(t1,5)*(-7 + 20*t2) + 
                  Power(t1,4)*(1030 - 744*t2 - 83*Power(t2,2)) + 
                  Power(t1,3)*
                   (-2777 + 957*t2 + 662*Power(t2,2) - 44*Power(t2,3)) \
+ t1*(-1943 - 1740*t2 + 2547*Power(t2,2) + 710*Power(t2,3) - 
                     132*Power(t2,4)) + 
                  Power(t1,2)*
                   (3412 + 641*t2 - 2411*Power(t2,2) - 
                     120*Power(t2,3) + 32*Power(t2,4))) + 
               Power(s2,2)*(-123 + 78*Power(t1,5) + 4*Power(t1,6) + 
                  789*t2 + 1201*Power(t2,2) + 25*Power(t2,3) - 
                  171*Power(t2,4) + 20*Power(t2,5) + 
                  Power(t1,4)*(82 + 129*t2 - 88*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (-333 - 840*t2 + 16*Power(t2,2) + 58*Power(t2,3)) + 
                  Power(t1,2)*
                   (631 + 4173*t2 + 1578*Power(t2,2) - 
                     262*Power(t2,3) - 18*Power(t2,4)) - 
                  t1*(6 + 3411*t2 + 2735*Power(t2,2) - 
                     347*Power(t2,3) - 153*Power(t2,4) + 14*Power(t2,5))\
)) + s1*(297 + 6*Power(t1,7) - 41*t2 - 344*Power(t2,2) - 
               346*Power(t2,3) + 100*Power(t2,4) - 
               8*Power(t1,6)*(7 + 2*t2) + 
               Power(t1,5)*(509 + 202*t2 + 15*Power(t2,2)) - 
               Power(t1,4)*(1151 + 1117*t2 + 122*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-991 + 478*t2 + 1788*Power(t2,2) + 492*Power(t2,3) - 
                  140*Power(t2,4)) + 
               Power(t1,3)*(572 + 2088*t2 + 1296*Power(t2,2) - 
                  68*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(814 - 1594*t2 - 2633*Power(t2,2) - 
                  68*Power(t2,3) + 44*Power(t2,4)) - 
               Power(s2,6)*(-1 + t1)*
                (2 + 2*Power(t1,2) + 14*t2 + 15*Power(t2,2) - 
                  2*t1*(2 + 7*t2)) + 
               Power(s2,5)*(10 + 3*Power(t1,4) - 40*t2 - 
                  123*Power(t2,2) - 62*Power(t2,3) + 24*Power(t2,4) + 
                  Power(t1,3)*(-19 + 34*t2) - 
                  3*Power(t1,2)*(-13 + 36*t2 + 49*Power(t2,2)) + 
                  t1*(-33 + 114*t2 + 270*Power(t2,2) + 94*Power(t2,3))) \
+ Power(s2,4)*(-76 - 15*Power(t1,5) - 221*t2 - 196*Power(t2,2) - 
                  36*Power(t2,3) + 25*Power(t2,4) + Power(t2,5) + 
                  2*Power(t1,4)*(5 + 7*t2) + 
                  Power(t1,3)*(136 + 259*t2 + 114*Power(t2,2)) - 
                  Power(t1,2)*
                   (318 + 781*t2 + 451*Power(t2,2) + 13*Power(t2,3)) + 
                  t1*(263 + 729*t2 + 533*Power(t2,2) + 53*Power(t2,3) - 
                     101*Power(t2,4))) + 
               Power(s2,3)*(4 + 4*Power(t1,6) + 71*t2 + 
                  495*Power(t2,2) + 912*Power(t2,3) + 194*Power(t2,4) + 
                  4*Power(t2,5) + Power(t2,6) - 
                  Power(t1,5)*(4 + 27*t2) + 
                  Power(t1,4)*(-195 - 292*t2 + 57*Power(t2,2)) + 
                  Power(t1,3)*
                   (565 + 949*t2 - 262*Power(t2,2) - 102*Power(t2,3)) + 
                  Power(t1,2)*
                   (-545 - 843*t2 + 725*Power(t2,2) + 
                     817*Power(t2,3) + 77*Power(t2,4)) - 
                  t1*(-171 - 142*t2 + 1015*Power(t2,2) + 
                     1567*Power(t2,3) + 353*Power(t2,4) + 10*Power(t2,5)\
)) + s2*(-70 - 20*Power(t1,7) + 1057*t2 + 872*Power(t2,2) - 
                  250*Power(t2,3) - 312*Power(t2,4) + 50*Power(t2,5) + 
                  Power(t1,6)*(140 + 11*t2) + 
                  Power(t1,5)*(-1323 - 313*t2 + 69*Power(t2,2)) + 
                  Power(t1,4)*
                   (4278 + 1882*t2 - 99*Power(t2,2) - 76*Power(t2,3)) + 
                  Power(t1,3)*
                   (-5926 - 5483*t2 - 614*Power(t2,2) + 
                     432*Power(t2,3) + Power(t2,4)) + 
                  Power(t1,2)*
                   (3660 + 7574*t2 + 2358*Power(t2,2) - 
                     1413*Power(t2,3) - 100*Power(t2,4) + 
                     15*Power(t2,5)) - 
                  t1*(739 + 4728*t2 + 2586*Power(t2,2) - 
                     1311*Power(t2,3) - 398*Power(t2,4) + 60*Power(t2,5)\
)) + Power(s2,2)*(30 + 266*t2 + 1466*Power(t2,2) + 1081*Power(t2,3) + 
                  86*Power(t2,4) - 61*Power(t2,5) + 4*Power(t2,6) + 
                  Power(t1,6)*(36 + 23*t2) + 
                  Power(t1,5)*(199 - 257*t2 - 48*Power(t2,2)) + 
                  Power(t1,4)*
                   (-924 + 1675*t2 + 595*Power(t2,2) + 9*Power(t2,3)) + 
                  Power(t1,3)*
                   (1188 - 4049*t2 - 3092*Power(t2,2) - 
                     438*Power(t2,3) + 31*Power(t2,4)) + 
                  Power(t1,2)*
                   (-550 + 4252*t2 + 6468*Power(t2,2) + 
                     1915*Power(t2,3) + 3*Power(t2,4) - 12*Power(t2,5)) \
- t1*(-21 + 1910*t2 + 5389*Power(t2,2) + 2575*Power(t2,3) + 
                     6*Power(t2,4) - 57*Power(t2,5) + 3*Power(t2,6))))) + 
         s*(Power(s1,7)*Power(s2,2)*(-1 + t1)*
             (4*Power(s2,2) + s2*(9 - 5*t1) + 3*(-1 + t1)*t1) + 
            Power(s2,6)*(-1 + t1)*Power(t2,2)*
             (-1 + Power(t1,3) + 5*t2 + 14*Power(t2,2) + 
               8*Power(t2,3) + Power(t1,2)*(-3 + 5*t2) + 
               t1*(3 - 10*t2 - 14*Power(t2,2))) - 
            Power(s2,5)*t2*(-2 + 20*t2 + 75*Power(t2,2) + 
               76*Power(t2,3) + 27*Power(t2,4) + 
               Power(t1,5)*(2 + 8*t2) + 
               Power(t1,4)*(-10 - 12*t2 + Power(t2,2)) - 
               2*Power(t1,3)*
                (-10 + 16*t2 + 39*Power(t2,2) + 11*Power(t2,3)) + 
               Power(t1,2)*(-20 + 88*t2 + 228*Power(t2,2) + 
                  120*Power(t2,3) + 11*Power(t2,4)) + 
               2*t1*(5 - 36*t2 - 113*Power(t2,2) - 87*Power(t2,3) - 
                  19*Power(t2,4) + Power(t2,5))) + 
            Power(-1 + t1,2)*
             (-53 + 5*Power(t1,7) + Power(t1,6)*(7 - 10*t2) - 52*t2 + 
               9*Power(t2,2) + 40*Power(t2,3) + 28*Power(t2,4) - 
               12*Power(t2,5) + 
               Power(t1,5)*(-142 - 19*t2 + 6*Power(t2,2)) + 
               Power(t1,4)*(345 + 198*t2 + 79*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(t1,3)*(-275 - 328*t2 - 331*Power(t2,2) - 
                  78*Power(t2,3) + 5*Power(t2,4)) + 
               Power(t1,2)*(-35 + 136*t2 + 410*Power(t2,2) + 
                  227*Power(t2,3) - 2*Power(t2,5)) + 
               t1*(148 + 75*t2 - 173*Power(t2,2) - 185*Power(t2,3) - 
                  36*Power(t2,4) + 11*Power(t2,5))) - 
            Power(s2,2)*(-1 + t1)*
             (-47 - 56*t2 - 349*Power(t2,2) - 286*Power(t2,3) + 
               63*Power(t2,4) + 105*Power(t2,5) + 10*Power(t2,6) + 
               Power(t1,7)*(-11 + 4*t2) + 
               Power(t1,6)*(21 - 150*t2 - 29*Power(t2,2)) + 
               Power(t1,5)*(107 + 916*t2 + 421*Power(t2,2) + 
                  56*Power(t2,3)) - 
               Power(t1,4)*(465 + 2300*t2 + 1619*Power(t2,2) + 
                  387*Power(t2,3) + 40*Power(t2,4)) + 
               Power(t1,3)*(755 + 2900*t2 + 2970*Power(t2,2) + 
                  844*Power(t2,3) + 76*Power(t2,4) + 8*Power(t2,5)) + 
               t1*(269 + 580*t2 + 1577*Power(t2,2) + 810*Power(t2,3) - 
                  286*Power(t2,4) - 169*Power(t2,5) - 11*Power(t2,6)) + 
               Power(t1,2)*(-629 - 1894*t2 - 2971*Power(t2,2) - 
                  1037*Power(t2,3) + 187*Power(t2,4) + 62*Power(t2,5) + 
                  Power(t2,6))) + 
            s2*Power(-1 + t1,2)*
             (19 + 2*Power(t1,7) - 173*t2 - 214*Power(t2,2) + 
               35*Power(t2,3) + 118*Power(t2,4) + 32*Power(t2,5) - 
               5*Power(t2,6) - Power(t1,6)*(75 + 22*t2) + 
               Power(t1,5)*(387 + 172*t2 + 45*Power(t2,2)) - 
               Power(t1,4)*(819 + 451*t2 + 140*Power(t2,2) + 
                  32*Power(t2,3)) + 
               Power(t1,3)*(836 + 714*t2 + 97*Power(t2,2) - 
                  62*Power(t2,3) + 9*Power(t2,4)) - 
               Power(t1,2)*(381 + 826*t2 + 168*Power(t2,2) - 
                  434*Power(t2,3) - 129*Power(t2,4) + 4*Power(t2,5)) + 
               t1*(31 + 586*t2 + 380*Power(t2,2) - 375*Power(t2,3) - 
                  271*Power(t2,4) - 19*Power(t2,5) + 2*Power(t2,6))) + 
            Power(s2,4)*(-1 + t1)*
             (-1 + 26*t2 + 154*Power(t2,2) + 187*Power(t2,3) + 
               128*Power(t2,4) + 23*Power(t2,5) - 15*Power(t2,6) + 
               Power(t1,5)*(1 + 16*t2 + 11*Power(t2,2)) + 
               Power(t1,4)*(-5 - 38*t2 - 2*Power(t2,2) + 
                  2*Power(t2,3)) - 
               2*Power(t1,3)*
                (-5 + 4*t2 + 107*Power(t2,2) + 81*Power(t2,3) + 
                  20*Power(t2,4)) + 
               Power(t1,2)*(-10 + 92*t2 + 544*Power(t2,2) + 
                  505*Power(t2,3) + 213*Power(t2,4) + 34*Power(t2,5)) - 
               t1*(-5 + 88*t2 + 493*Power(t2,2) + 532*Power(t2,3) + 
                  301*Power(t2,4) + 59*Power(t2,5) + 7*Power(t2,6))) + 
            Power(s2,3)*(-11 - 144*t2 - 195*Power(t2,2) - 
               325*Power(t2,3) - 151*Power(t2,4) + 54*Power(t2,5) + 
               22*Power(t2,6) + 
               2*Power(t1,7)*(-4 - 11*t2 + Power(t2,2)) + 
               Power(t1,6)*(37 + 46*t2 - 77*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,5)*(-54 + 244*t2 + 669*Power(t2,2) + 
                  277*Power(t2,3) + 21*Power(t2,4)) - 
               Power(t1,4)*(5 + 1140*t2 + 2141*Power(t2,2) + 
                  1320*Power(t2,3) + 251*Power(t2,4) + 14*Power(t2,5)) \
+ t1*(58 + 784*t2 + 1161*Power(t2,2) + 1536*Power(t2,3) + 
                  574*Power(t2,4) - 105*Power(t2,5) - 33*Power(t2,6)) + 
               Power(t1,3)*(100 + 1970*t2 + 3344*Power(t2,2) + 
                  2743*Power(t2,3) + 711*Power(t2,4) + 
                  63*Power(t2,5) + 3*Power(t2,6)) + 
               Power(t1,2)*(-117 - 1738*t2 - 2763*Power(t2,2) - 
                  2899*Power(t2,3) - 904*Power(t2,4) + 2*Power(t2,5) + 
                  10*Power(t2,6))) - 
            Power(s1,6)*s2*(2*Power(s2,4)*t1 - 8*Power(-1 + t1,3)*t1 + 
               Power(s2,3)*(-1 + t1)*(17 + t1 + 24*t2) - 
               s2*Power(-1 + t1,2)*
                (49 + 2*Power(t1,2) - 4*t2 - 7*t1*(3 + 2*t2)) - 
               Power(s2,2)*(39 + 54*t2 + 15*Power(t1,2)*(3 + 2*t2) - 
                  2*t1*(41 + 42*t2))) + 
            Power(s1,5)*(-8*Power(s2,6)*(-1 + t1) + 
               Power(-1 + t1,2)*t1*(5 - 10*t1 + 8*Power(t1,2)) + 
               4*Power(s2,5)*(7 + 3*Power(t1,2) + t1*(-10 + 3*t2)) - 
               Power(s2,4)*(-1 + t1)*
                (6 + 30*Power(t1,2) - 99*t2 - 60*Power(t2,2) - 
                  t1*(38 + 13*t2)) + 
               s2*Power(-1 + t1,2)*
                (-71 + 11*Power(t1,3) + 10*t2 + 20*t1*(5 + t2) - 
                  Power(t1,2)*(43 + 27*t2)) - 
               Power(s2,2)*(-1 + t1)*
                (-175 + 5*Power(t1,4) + Power(t1,3)*(22 - 3*t2) - 
                  248*t2 + 20*Power(t2,2) + 
                  t1*(394 + 337*t2 + 5*Power(t2,2)) - 
                  Power(t1,2)*(252 + 86*t2 + 25*Power(t2,2))) + 
               Power(s2,3)*(-37 + 20*Power(t1,4) - 230*t2 - 
                  135*Power(t2,2) + Power(t1,3)*(-57 + 6*t2) - 
                  Power(t1,2)*(15 + 266*t2 + 75*Power(t2,2)) + 
                  t1*(89 + 478*t2 + 210*Power(t2,2)))) - 
            Power(s1,4)*(2*Power(s2,6)*(-1 + t1)*(-7 + 7*t1 - 20*t2) + 
               Power(-1 + t1,2)*
                (-31 + 6*t2 + t1*(75 + 2*t2) - 
                  Power(t1,2)*(73 + 6*t2) + Power(t1,3)*(32 + 13*t2)) + 
               Power(s2,5)*(74 - 20*Power(t1,3) + 139*t2 + 
                  Power(t1,2)*(114 + 59*t2) + 
                  6*t1*(-28 - 33*t2 + 5*Power(t2,2))) + 
               s2*Power(-1 + t1,2)*
                (-249 + 4*Power(t1,4) - 288*t2 + 40*Power(t2,2) + 
                  4*Power(t1,3)*(12 + t2) + 
                  t1*(567 + 347*t2 + 8*Power(t2,2)) - 
                  Power(t1,2)*(355 + 84*t2 + 33*Power(t2,2))) + 
               Power(s2,4)*(-1 + t1)*
                (-99 + 28*Power(t1,3) - 40*t2 + 241*Power(t2,2) + 
                  80*Power(t2,3) - Power(t1,2)*(160 + 147*t2) + 
                  t1*(231 + 197*t2 + 49*Power(t2,2))) + 
               Power(s2,2)*(-1 + t1)*
                (46 + 4*Power(t1,5) + Power(t1,4)*(2 - 28*t2) + 
                  866*t2 + 518*Power(t2,2) - 40*Power(t2,3) + 
                  2*Power(t1,3)*(-85 - 43*t2 + 11*Power(t2,2)) + 
                  t1*(-402 - 1889*t2 - 681*Power(t2,2) + 
                     20*Power(t2,3)) + 
                  Power(t1,2)*
                   (520 + 1167*t2 + 141*Power(t2,2) + 20*Power(t2,3))) \
- Power(s2,3)*(19*Power(t1,5) - Power(t1,4)*(223 + 54*t2) + 
                  Power(t1,3)*(544 + 153*t2 - 25*Power(t2,2)) + 
                  4*(-14 + 55*t2 + 139*Power(t2,2) + 45*Power(t2,3)) + 
                  Power(t1,2)*
                   (-551 + 236*t2 + 636*Power(t2,2) + 100*Power(t2,3)) \
- t1*(-267 + 555*t2 + 1137*Power(t2,2) + 280*Power(t2,3)))) + 
            Power(s1,3)*(-(Power(s2,6)*(-1 + t1)*
                  (5 + 5*Power(t1,2) + 56*t2 + 80*Power(t2,2) - 
                    2*t1*(5 + 28*t2))) + 
               Power(-1 + t1,2)*
                (-109 - 94*t2 + 18*Power(t2,2) + 
                  3*Power(t1,4)*(-7 + 4*t2) + 
                  t1*(356 + 178*t2 + Power(t2,2)) + 
                  Power(t1,3)*(161 + 38*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*(-387 - 122*t2 + 8*Power(t2,2))) - 
               s2*Power(-1 + t1,2)*
                (49 + 12*Power(t1,5) + Power(t1,4)*(84 - 33*t2) + 
                  981*t2 + 483*Power(t2,2) - 65*Power(t2,3) + 
                  Power(t1,3)*(-583 - 80*t2 + 21*Power(t2,2)) + 
                  Power(t1,2)*
                   (1094 + 1263*t2 + 69*Power(t2,2) + 17*Power(t2,3)) \
+ t1*(-656 - 2191*t2 - 519*Power(t2,2) + 18*Power(t2,3))) + 
               Power(s2,5)*(73 - Power(t1,4) + 298*t2 + 
                  276*Power(t2,2) - 2*Power(t1,3)*(35 + 41*t2) + 
                  2*Power(t1,2)*(108 + 231*t2 + 58*Power(t2,2)) + 
                  t1*(-218 - 678*t2 - 392*Power(t2,2) + 40*Power(t2,3))\
) + Power(s2,4)*(-1 + t1)*(-188 + 7*Power(t1,4) - 417*t2 - 
                  107*Power(t2,2) + 314*Power(t2,3) + 60*Power(t2,4) + 
                  4*Power(t1,3)*(34 + 29*t2) - 
                  Power(t1,2)*(481 + 669*t2 + 295*Power(t2,2)) + 
                  t1*(526 + 970*t2 + 422*Power(t2,2) + 86*Power(t2,3))) \
+ Power(s2,2)*(-1 + t1)*(-203 + 328*t2 + 1669*Power(t2,2) + 
                  566*Power(t2,3) - 40*Power(t2,4) + 
                  Power(t1,5)*(28 + 5*t2) + 
                  Power(t1,4)*(-191 + 91*t2 - 36*Power(t2,2)) + 
                  Power(t1,3)*
                   (341 - 894*t2 - 188*Power(t2,2) + 28*Power(t2,3)) + 
                  Power(t1,2)*
                   (-424 + 2447*t2 + 2153*Power(t2,2) + 
                     138*Power(t2,3) + 5*Power(t2,4)) + 
                  t1*(449 - 1977*t2 - 3538*Power(t2,2) - 
                     732*Power(t2,3) + 35*Power(t2,4))) + 
               Power(s2,3)*(122 + 4*Power(t1,6) + 296*t2 - 
                  494*Power(t2,2) - 700*Power(t2,3) - 135*Power(t2,4) - 
                  7*Power(t1,5)*(31 + 5*t2) + 
                  Power(t1,4)*(957 + 725*t2 + 54*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1731 - 1993*t2 - 172*Power(t2,2) + 36*Power(t2,3)) \
+ Power(t1,2)*(1561 + 2247*t2 - 632*Power(t2,2) - 772*Power(t2,3) - 
                     75*Power(t2,4)) + 
                  2*t1*(-348 - 620*t2 + 622*Power(t2,2) + 
                     698*Power(t2,3) + 105*Power(t2,4)))) + 
            Power(s1,2)*(Power(s2,6)*(-1 + t1)*
                (-1 + Power(t1,3) + 15*t2 + 84*Power(t2,2) + 
                  80*Power(t2,3) + 3*Power(t1,2)*(-1 + 5*t2) + 
                  t1*(3 - 30*t2 - 84*Power(t2,2))) + 
               Power(-1 + t1,2)*
                (23 + 321*t2 + 128*Power(t2,2) - 30*Power(t2,3) + 
                  Power(t1,5)*(-63 + 6*t2) + 
                  Power(t1,4)*(404 + 2*t2 - 7*Power(t2,2)) + 
                  Power(t1,3)*
                   (-906 - 391*t2 - 31*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (875 + 1130*t2 + 91*Power(t2,2) + 4*Power(t2,3)) - 
                  t1*(333 + 1068*t2 + 199*Power(t2,2) + 5*Power(t2,3))) \
+ Power(s2,5)*(-21 - 7*Power(t1,5) - 221*t2 - 450*Power(t2,2) - 
                  274*Power(t2,3) + Power(t1,4)*(7 + t2) + 
                  2*Power(t1,3)*(21 + 109*t2 + 63*Power(t2,2)) - 
                  2*Power(t1,2)*
                   (49 + 330*t2 + 351*Power(t2,2) + 57*Power(t2,3)) + 
                  t1*(77 + 662*t2 + 1026*Power(t2,2) + 
                     388*Power(t2,3) - 30*Power(t2,4))) + 
               s2*Power(-1 + t1,2)*
                (-262 + 3*Power(t1,6) + Power(t1,5)*(-25 + t2) + 
                  331*t2 + 1348*Power(t2,2) + 418*Power(t2,3) - 
                  55*Power(t2,4) + 
                  Power(t1,4)*(296 + 221*t2 - 17*Power(t2,2)) + 
                  Power(t1,3)*
                   (-773 - 1543*t2 - 133*Power(t2,2) + 10*Power(t2,3)) \
+ Power(t1,2)*(481 + 3287*t2 + 1746*Power(t2,2) + 54*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  t1*(280 - 2297*t2 - 3034*Power(t2,2) - 
                     416*Power(t2,3) + 22*Power(t2,4))) + 
               Power(s2,4)*(-1 + t1)*
                (151 + 10*Power(t1,5) + 567*t2 + 665*Power(t2,2) + 
                  141*Power(t2,3) - 231*Power(t2,4) - 24*Power(t2,5) - 
                  Power(t1,4)*(1 + 8*t2) - 
                  2*Power(t1,3)*(104 + 225*t2 + 94*Power(t2,2)) + 
                  Power(t1,2)*
                   (530 + 1491*t2 + 1071*Power(t2,2) + 
                     303*Power(t2,3)) - 
                  t1*(482 + 1600*t2 + 1548*Power(t2,2) + 
                     464*Power(t2,3) + 79*Power(t2,4))) - 
               Power(s2,2)*(-1 + t1)*
                (-112 + 6*Power(t1,6)*(-1 + t2) - 639*t2 + 
                  589*Power(t2,2) + 1545*Power(t2,3) + 
                  335*Power(t2,4) - 20*Power(t2,5) - 
                  2*Power(t1,5)*(-175 + 8*t2 + 4*Power(t2,2)) - 
                  8*Power(t1,4)*
                   (166 + 33*t2 - 21*Power(t2,2) + Power(t2,3)) + 
                  2*Power(t1,3)*
                   (1028 + 333*t2 - 635*Power(t2,2) - 
                     99*Power(t2,3) + 6*Power(t2,4)) + 
                  Power(t1,2)*
                   (-1610 - 1145*t2 + 3593*Power(t2,2) + 
                     1875*Power(t2,3) + 83*Power(t2,4) - 2*Power(t2,5)\
) + 2*t1*(325 + 696*t2 - 1536*Power(t2,2) - 1577*Power(t2,3) - 
                     215*Power(t2,4) + 11*Power(t2,5))) + 
               Power(s2,3)*(-167 - 536*t2 - 583*Power(t2,2) + 
                  530*Power(t2,3) + 479*Power(t2,4) + 54*Power(t2,5) - 
                  6*Power(t1,6)*(5 + 2*t2) + 
                  Power(t1,5)*(436 + 638*t2 + 42*Power(t2,2)) - 
                  Power(t1,4)*
                   (1611 + 2989*t2 + 1072*Power(t2,2) + 40*Power(t2,3)) \
+ Power(t1,3)*(2684 + 5795*t2 + 3145*Power(t2,2) + 176*Power(t2,3) - 
                     18*Power(t2,4)) + 
                  t1*(984 + 2755*t2 + 2293*Power(t2,2) - 
                     1284*Power(t2,3) - 916*Power(t2,4) - 
                     84*Power(t2,5)) + 
                  Power(t1,2)*
                   (-2296 - 5651*t2 - 3825*Power(t2,2) + 
                     618*Power(t2,3) + 485*Power(t2,4) + 30*Power(t2,5))\
)) + s1*(-(Power(s2,6)*(-1 + t1)*t2*
                  (-2 + 2*Power(t1,3) + 15*t2 + 56*Power(t2,2) + 
                    40*Power(t2,3) + 3*Power(t1,2)*(-2 + 5*t2) + 
                    t1*(6 - 30*t2 - 56*Power(t2,2)))) - 
               Power(-1 + t1,2)*
                (-109 + 124*t2 + 246*Power(t2,2) + 93*Power(t2,3) - 
                  30*Power(t2,4) + Power(t1,6)*(31 + 3*t2) - 
                  Power(t1,5)*(331 + 40*t2 + 7*Power(t2,2)) + 
                  Power(t1,4)*
                   (979 + 477*t2 + 23*Power(t2,2) + 5*Power(t2,3)) - 
                  Power(t1,3)*
                   (1152 + 1443*t2 + 362*Power(t2,2) + 
                     20*Power(t2,3) + Power(t2,4)) + 
                  Power(t1,2)*
                   (427 + 1724*t2 + 986*Power(t2,2) + 42*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  t1*(155 - 845*t2 - 886*Power(t2,2) - 
                     132*Power(t2,3) + 10*Power(t2,4))) - 
               s2*Power(-1 + t1,2)*
                (-114 - 435*t2 + 330*Power(t2,2) + 734*Power(t2,3) + 
                  184*Power(t2,4) - 25*Power(t2,5) + 
                  Power(t1,6)*(-13 + 4*t2) + 
                  Power(t1,5)*(168 - 49*t2 - 12*Power(t2,2)) + 
                  Power(t1,4)*
                   (-466 + 463*t2 + 122*Power(t2,2) + 12*Power(t2,3)) - 
                  2*Power(t1,3)*
                   (-317 + 617*t2 + 540*Power(t2,2) + 46*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  Power(t1,2)*
                   (-591 + 820*t2 + 2709*Power(t2,2) + 
                     967*Power(t2,3) + 22*Power(t2,4)) + 
                  t1*(382 + 431*t2 - 2069*Power(t2,2) - 
                     1681*Power(t2,3) - 163*Power(t2,4) + 10*Power(t2,5)\
)) + Power(s2,5)*(-2 + 41*t2 + 223*Power(t2,2) + 302*Power(t2,3) + 
                  136*Power(t2,4) + Power(t1,5)*(2 + 15*t2) + 
                  Power(t1,4)*(-10 - 19*t2 + Power(t2,2)) - 
                  2*Power(t1,3)*
                   (-10 + 37*t2 + 113*Power(t2,2) + 43*Power(t2,3)) + 
                  2*Power(t1,2)*
                   (-10 + 93*t2 + 336*Power(t2,2) + 237*Power(t2,3) + 
                     28*Power(t2,4)) + 
                  t1*(10 - 149*t2 - 670*Power(t2,2) - 690*Power(t2,3) - 
                     192*Power(t2,4) + 12*Power(t2,5))) - 
               Power(s2,4)*(-1 + t1)*
                (27 + 309*t2 + 566*Power(t2,2) + 475*Power(t2,3) + 
                  91*Power(t2,4) - 91*Power(t2,5) - 4*Power(t2,6) + 
                  Power(t1,5)*(15 + 17*t2) + 
                  Power(t1,4)*(-33 + 17*t2 + Power(t2,2)) - 
                  2*Power(t1,3)*
                   (9 + 231*t2 + 238*Power(t2,2) + 70*Power(t2,3)) + 
                  Power(t1,2)*
                   (102 + 1114*t2 + 1515*Power(t2,2) + 
                     775*Power(t2,3) + 159*Power(t2,4)) - 
                  t1*(93 + 995*t2 + 1606*Power(t2,2) + 
                     1110*Power(t2,3) + 260*Power(t2,4) + 37*Power(t2,5)\
)) + Power(s2,2)*(-1 + t1)*(-26 + 6*Power(t1,7) - 431*t2 - 
                  731*Power(t2,2) + 370*Power(t2,3) + 672*Power(t2,4) + 
                  98*Power(t2,5) - 4*Power(t2,6) + 
                  Power(t1,6)*(-127 - 39*t2 + 4*Power(t2,2)) + 
                  Power(t1,5)*
                   (741 + 761*t2 + 31*Power(t2,2) - 9*Power(t2,3)) + 
                  Power(t1,4)*
                   (-1840 - 2837*t2 - 525*Power(t2,2) + 
                     39*Power(t2,3) + 5*Power(t2,4)) + 
                  Power(t1,3)*
                   (2280 + 4766*t2 + 1279*Power(t2,2) - 
                     470*Power(t2,3) - 66*Power(t2,4) + Power(t2,5)) - 
                  Power(t1,2)*
                   (1431 + 4301*t2 + 1858*Power(t2,2) - 
                     1853*Power(t2,3) - 699*Power(t2,4) - 
                     24*Power(t2,5) + Power(t2,6)) + 
                  t1*(397 + 2081*t2 + 1800*Power(t2,2) - 
                     1783*Power(t2,3) - 1280*Power(t2,4) - 
                     123*Power(t2,5) + 5*Power(t2,6))) - 
               Power(s2,3)*(-139 - 365*t2 - 745*Power(t2,2) - 
                  494*Power(t2,3) + 273*Power(t2,4) + 166*Power(t2,5) + 
                  9*Power(t2,6) + Power(t1,7)*(-17 + 6*t2) + 
                  Power(t1,6)*(21 - 134*t2 - 26*Power(t2,2)) + 
                  Power(t1,5)*
                   (289 + 1183*t2 + 734*Power(t2,2) + 47*Power(t2,3)) - 
                  Power(t1,4)*
                   (1165 + 3877*t2 + 3442*Power(t2,2) + 
                     821*Power(t2,3) + 34*Power(t2,4)) + 
                  Power(t1,3)*
                   (1945 + 6148*t2 + 6927*Power(t2,2) + 
                     2407*Power(t2,3) + 163*Power(t2,4) + 2*Power(t2,5)) \
+ t1*(759 + 2167*t2 + 3631*Power(t2,2) + 1894*Power(t2,3) - 
                     611*Power(t2,4) - 294*Power(t2,5) - 14*Power(t2,6)) \
+ Power(t1,2)*(-1693 - 5128*t2 - 7079*Power(t2,2) - 3033*Power(t2,3) + 
                     209*Power(t2,4) + 138*Power(t2,5) + 5*Power(t2,6))))\
) - Power(s,2)*(Power(s1,7)*Power(s2,2)*(s2 - 3*t1)*(-1 + t1) - 
            3*Power(s2,6)*(-1 + t1)*Power(t2,2)*
             (3 + 3*Power(t1,2) + 7*t2 + 4*Power(t2,2) - t1*(6 + 7*t2)) \
+ Power(s1,6)*s2*(7*Power(s2,3)*(-1 + t1) - 12*Power(-1 + t1,2)*t1 + 
               Power(s2,2)*(-11 + 21*t1 - 18*Power(t1,2) + 6*t2 - 
                  6*t1*t2) + 
               3*s2*(-1 + t1)*
                (-11 + 3*t1 + 2*Power(t1,2) + 2*t2 + 4*t1*t2)) + 
            Power(s2,5)*t2*(30 + Power(t1,5) + 91*t2 + 70*Power(t2,2) - 
               2*Power(t2,3) - 17*Power(t2,4) + 
               Power(t1,4)*(26 + 24*t2) - 
               Power(t1,3)*(114 + 163*t2 + 30*Power(t2,2)) + 
               Power(t1,2)*(176 + 345*t2 + 130*Power(t2,2) - 
                  18*Power(t2,3)) + 
               t1*(-119 - 297*t2 - 170*Power(t2,2) + 20*Power(t2,3) + 
                  23*Power(t2,4))) - 
            (-1 + t1)*(-115 - 150*t2 - 54*Power(t2,2) + 
               51*Power(t2,3) + 84*Power(t2,4) - 30*Power(t2,5) + 
               Power(t1,7)*(27 + t2) - 
               Power(t1,6)*(116 + 49*t2 + 4*Power(t2,2)) + 
               3*Power(t1,5)*
                (55 + 66*t2 + 7*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,4)*(155 + 485*t2 - 96*Power(t2,2) + 
                  17*Power(t2,3) + 4*Power(t2,4)) + 
               Power(t1,3)*(345 + 892*t2 - 296*Power(t2,2) - 
                  214*Power(t2,3) + 32*Power(t2,4) + Power(t2,5)) - 
               Power(t1,2)*(582 + 1032*t2 - 186*Power(t2,2) - 
                  521*Power(t2,3) + 4*Power(t2,4) + 14*Power(t2,5)) + 
               t1*(431 + 625*t2 + 51*Power(t2,2) - 347*Power(t2,3) - 
                  111*Power(t2,4) + 40*Power(t2,5))) - 
            Power(s2,4)*(-19 - 107*t2 - 23*Power(t2,2) + 
               118*Power(t2,3) + 193*Power(t2,4) + 97*Power(t2,5) + 
               7*Power(t2,6) + Power(t1,6)*(1 + 4*t2) + 
               Power(t1,5)*(14 + 57*t2 + 33*Power(t2,2)) - 
               Power(t1,4)*(85 + 375*t2 + 223*Power(t2,2) + 
                  41*Power(t2,3)) + 
               Power(t1,3)*(180 + 850*t2 + 494*Power(t2,2) + 
                  34*Power(t2,3) - 33*Power(t2,4)) + 
               Power(t1,2)*(-185 - 930*t2 - 474*Power(t2,2) + 
                  173*Power(t2,3) + 244*Power(t2,4) + 42*Power(t2,5)) + 
               t1*(94 + 501*t2 + 193*Power(t2,2) - 284*Power(t2,3) - 
                  404*Power(t2,4) - 135*Power(t2,5) - 5*Power(t2,6))) + 
            Power(s2,2)*(-99 + 3*Power(t1,8) - 133*t2 - 
               708*Power(t2,2) - 803*Power(t2,3) - 241*Power(t2,4) + 
               38*Power(t2,5) + 12*Power(t2,6) + 
               2*Power(t1,7)*(-49 - 3*t2 + Power(t2,2)) - 
               Power(t1,6)*(-490 + 148*t2 + 85*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,5)*(-986 + 1307*t2 + 1026*Power(t2,2) + 
                  219*Power(t2,3) + 6*Power(t2,4)) - 
               Power(t1,4)*(-800 + 3761*t2 + 4143*Power(t2,2) + 
                  1417*Power(t2,3) + 171*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,3)*(122 + 5144*t2 + 7801*Power(t2,2) + 
                  3857*Power(t2,3) + 667*Power(t2,4) + 37*Power(t2,5)) \
+ t1*(450 + 1187*t2 + 3679*Power(t2,2) + 3262*Power(t2,3) + 
                  759*Power(t2,4) - 52*Power(t2,5) - 15*Power(t2,6)) + 
               Power(t1,2)*(-682 - 3590*t2 - 7572*Power(t2,2) - 
                  5112*Power(t2,3) - 1020*Power(t2,4) - 
                  15*Power(t2,5) + 3*Power(t2,6))) + 
            Power(s2,3)*(37 + Power(t1,7)*(4 - 3*t2) - 134*t2 - 
               239*Power(t2,2) - 610*Power(t2,3) - 454*Power(t2,4) - 
               52*Power(t2,5) + 
               Power(t1,6)*(25 + 131*t2 + 8*Power(t2,2)) + 
               Power(t1,5)*(-222 - 642*t2 - 89*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(t1,4)*(595 + 1184*t2 - 52*Power(t2,2) - 
                  349*Power(t2,3) - 49*Power(t2,4)) + 
               Power(t1,3)*(-800 - 831*t2 + 849*Power(t2,2) + 
                  1602*Power(t2,3) + 463*Power(t2,4) + 36*Power(t2,5)) \
+ t1*(-230 + 388*t2 + 988*Power(t2,2) + 2169*Power(t2,3) + 
                  1290*Power(t2,4) + 177*Power(t2,5) + Power(t2,6)) - 
               Power(t1,2)*(-591 + 93*t2 + 1465*Power(t2,2) + 
                  2827*Power(t2,3) + 1250*Power(t2,4) + 
                  161*Power(t2,5) + 7*Power(t2,6))) - 
            s2*(-1 + t1)*(55 - 322*t2 - 615*Power(t2,2) - 
               228*Power(t2,3) + 78*Power(t2,4) + 60*Power(t2,5) - 
               10*Power(t2,6) + 2*Power(t1,7)*(1 + t2) - 
               Power(t1,6)*(200 + 98*t2 + 7*Power(t2,2)) + 
               Power(t1,5)*(1128 + 794*t2 + 225*Power(t2,2) + 
                  10*Power(t2,3)) - 
               Power(t1,4)*(2497 + 2530*t2 + 1129*Power(t2,2) + 
                  162*Power(t2,3) + 8*Power(t2,4)) + 
               Power(t1,3)*(2638 + 4098*t2 + 2748*Power(t2,2) + 
                  427*Power(t2,3) + 39*Power(t2,4) + 4*Power(t2,5)) - 
               Power(t1,2)*(1254 + 3626*t2 + 3589*Power(t2,2) + 
                  567*Power(t2,3) - 159*Power(t2,4) + 14*Power(t2,5) + 
                  Power(t2,6)) + 
               t1*(128 + 1682*t2 + 2367*Power(t2,2) + 520*Power(t2,3) - 
                  277*Power(t2,4) - 41*Power(t2,5) + 8*Power(t2,6))) + 
            Power(s1,5)*(Power(s2,5)*(16 - 22*t1) + 
               t1*(10 - 30*t1 + 33*Power(t1,2) - 13*Power(t1,3)) - 
               s2*(-1 + t1)*(-86 + 3*Power(t1,3) + 20*t2 + 
                  4*t1*(28 + 5*t2) - Power(t1,2)*(32 + 37*t2)) + 
               Power(s2,4)*(64 + 20*Power(t1,2) + 41*t2 - 
                  t1*(80 + 39*t2)) + 
               Power(s2,2)*(-61 + 15*Power(t1,4) - 177*t2 + 
                  30*Power(t2,2) - 2*Power(t1,3)*(22 + 21*t2) - 
                  3*t1*(-46 - 72*t2 + 5*Power(t2,2)) - 
                  3*Power(t1,2)*(18 - t2 + 5*Power(t2,2))) + 
               Power(s2,3)*(48 - 31*Power(t1,3) + 32*t2 - 
                  15*Power(t2,2) + 2*Power(t1,2)*(85 + 38*t2) + 
                  t1*(-187 - 62*t2 + 15*Power(t2,2)))) + 
            Power(s1,4)*(-12*Power(s2,6)*(-1 + t1) + 
               (-1 + t1)*(-54 + 7*Power(t1,4) + t1*(128 - 5*t2) + 
                  15*t2 - Power(t1,2)*(123 + 11*t2) + 
                  Power(t1,3)*(45 + 16*t2)) + 
               Power(s2,5)*(2 - 14*Power(t1,2) - 81*t2 + 
                  3*t1*(4 + 37*t2)) + 
               s2*(-1 + t1)*(-261 + 35*Power(t1,4) - 364*t2 + 
                  80*Power(t2,2) - 5*Power(t1,3)*(3 + 10*t2) + 
                  Power(t1,2)*(-366 + 6*t2 - 37*Power(t2,2)) + 
                  t1*(616 + 387*t2 - 28*Power(t2,2))) - 
               Power(s2,4)*(152 + 23*Power(t1,3) + 340*t2 + 
                  101*Power(t2,2) + Power(t1,2)*(91 + 109*t2) - 
                  t1*(266 + 429*t2 + 91*Power(t2,2))) + 
               Power(s2,3)*(-155 + 8*Power(t1,4) - 204*t2 - 
                  12*Power(t2,2) + 20*Power(t2,3) + 
                  4*Power(t1,3)*(51 + 20*t2) - 
                  Power(t1,2)*(604 + 641*t2 + 125*Power(t2,2)) + 
                  t1*(547 + 765*t2 + 27*Power(t2,2) - 20*Power(t2,3))) \
+ Power(s2,2)*(-179 + 416*t2 + 404*Power(t2,2) - 60*Power(t2,3) - 
                  15*Power(t1,4)*(4 + 3*t2) + 
                  3*Power(t1,2)*(-221 + 192*t2 + 5*Power(t2,2)) + 
                  Power(t1,3)*(365 + 67*t2 + 88*Power(t2,2)) + 
                  t1*(537 - 984*t2 - 507*Power(t2,2) + 60*Power(t2,3)))) \
+ Power(s1,3)*(-3*Power(s2,6)*(-1 + t1)*(-7 + 7*t1 - 16*t2) + 
               Power(s2,5)*(-63 + 23*Power(t1,3) - 4*t2 + 
                  164*Power(t2,2) + Power(t1,2)*(-109 + 60*t2) + 
                  t1*(149 - 56*t2 - 224*Power(t2,2))) - 
               (-1 + t1)*(3*Power(t1,5) + 2*Power(t1,4)*(-9 + 14*t2) + 
                  t1*(635 + 278*t2 - 25*Power(t2,2)) + 
                  Power(t1,3)*(269 + 36*t2 - Power(t2,2)) + 
                  Power(t1,2)*(-700 - 162*t2 + 11*Power(t2,2)) + 
                  3*(-63 - 56*t2 + 15*Power(t2,2))) + 
               s2*(-1 + t1)*(-126 + 27*Power(t1,5) + 
                  Power(t1,4)*(86 - 92*t2) + 1134*t2 + 
                  671*Power(t2,2) - 130*Power(t2,3) + 
                  Power(t1,3)*(-577 - 10*t2 + 97*Power(t2,2)) + 
                  Power(t1,2)*
                   (731 + 1655*t2 - 39*Power(t2,2) + 8*Power(t2,3)) + 
                  t1*(-141 - 2723*t2 - 675*Power(t2,2) + 
                     92*Power(t2,3))) + 
               Power(s2,4)*(129 - 55*Power(t1,4) + 652*t2 + 
                  733*Power(t2,2) + 134*Power(t2,3) + 
                  Power(t1,3)*(65 + 33*t2) + 
                  Power(t1,2)*(164 + 526*t2 + 249*Power(t2,2)) - 
                  t1*(303 + 1211*t2 + 942*Power(t2,2) + 
                     114*Power(t2,3))) + 
               Power(s2,2)*(331 - 6*Power(t1,6) + 526*t2 - 
                  952*Power(t2,2) - 492*Power(t2,3) + 60*Power(t2,4) + 
                  6*Power(t1,5)*(-11 + 6*t2) + 
                  Power(t1,4)*(820 - 78*t2 + 17*Power(t2,2)) - 
                  Power(t1,3)*
                   (2392 + 475*t2 - 148*Power(t2,2) + 72*Power(t2,3)) \
+ t1*(-1636 - 1311*t2 + 2302*Power(t2,2) + 660*Power(t2,3) - 
                     75*Power(t2,4)) + 
                  3*Power(t1,2)*
                   (983 + 434*t2 - 525*Power(t2,2) - 32*Power(t2,3) + 
                     5*Power(t2,4))) + 
               Power(s2,3)*(240 + 26*Power(t1,5) + 
                  Power(t1,4)*(18 - 34*t2) + 862*t2 + 378*Power(t2,2) - 
                  46*Power(t2,3) - 15*Power(t2,4) - 
                  Power(t1,3)*(485 + 841*t2 + 92*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (526 + 1357*t2 + 535*Power(t2,2) + 53*Power(t2,3)) + 
                  t1*(-851 - 2701*t2 - 1356*Power(t2,2) + 
                     80*Power(t2,3) + 15*Power(t2,4)))) + 
            Power(s1,2)*(-9*Power(s2,6)*(-1 + t1)*
                (1 + Power(t1,2) + 7*t2 + 8*Power(t2,2) - 
                  t1*(2 + 7*t2)) + 
               (-1 + t1)*(-12 + 5*Power(t1,6) + 
                  Power(t1,5)*(127 - 6*t2) - 580*t2 - 
                  267*Power(t2,2) + 75*Power(t2,3) + 
                  Power(t1,4)*(-862 + 49*t2) + 
                  t1*(463 + 2020*t2 + 349*Power(t2,2) - 
                     35*Power(t2,3)) + 
                  Power(t1,3)*
                   (1762 + 735*t2 + 31*Power(t2,2) + Power(t2,3)) - 
                  Power(t1,2)*
                   (1483 + 2218*t2 + 95*Power(t2,2) + 11*Power(t2,3))) \
+ Power(s2,5)*(79 + 12*Power(t1,4) + 196*t2 - 166*Power(t2,3) - 
                  Power(t1,3)*(115 + 76*t2) + 
                  Power(t1,2)*(273 + 348*t2 - 96*Power(t2,2)) + 
                  t1*(-249 - 468*t2 + 96*Power(t2,2) + 226*Power(t2,3))\
) - Power(s2,4)*(-22 + 11*Power(t1,5) + 407*t2 + 1041*Power(t2,2) + 
                  799*Power(t2,3) + 101*Power(t2,4) - 
                  2*Power(t1,4)*(67 + 60*t2) + 
                  Power(t1,3)*(358 + 40*t2 - 36*Power(t2,2)) + 
                  Power(t1,2)*
                   (-380 + 687*t2 + 1023*Power(t2,2) + 
                     293*Power(t2,3)) - 
                  t1*(-167 + 1014*t2 + 2028*Power(t2,2) + 
                     1052*Power(t2,3) + 81*Power(t2,4))) - 
               s2*(-1 + t1)*(-482 + 24*Power(t1,6) + 
                  Power(t1,5)*(25 - 43*t2) - 83*t2 + 
                  1606*Power(t2,2) + 654*Power(t2,3) - 
                  110*Power(t2,4) + 
                  Power(t1,4)*(-222 + 463*t2 - 9*Power(t2,2)) + 
                  Power(t1,3)*
                   (784 - 1841*t2 - 303*Power(t2,2) + 36*Power(t2,3)) \
+ Power(t1,2)*(-1604 + 2589*t2 + 2734*Power(t2,2) + 54*Power(t2,3) - 
                     8*Power(t2,4)) + 
                  t1*(1475 - 1085*t2 - 4082*Power(t2,2) - 
                     678*Power(t2,3) + 88*Power(t2,4))) + 
               Power(s2,3)*(-266 + 6*Power(t1,6) - 1044*t2 - 
                  1735*Power(t2,2) - 388*Power(t2,3) + 
                  55*Power(t2,4) + 6*Power(t2,5) - 
                  Power(t1,5)*(195 + 14*t2) + 
                  Power(t1,4)*(365 - 431*t2 - 27*Power(t2,2)) + 
                  Power(t1,3)*
                   (281 + 2526*t2 + 1621*Power(t2,2) + 
                     104*Power(t2,3)) - 
                  Power(t1,2)*
                   (1173 + 4747*t2 + 4998*Power(t2,2) + 
                     1058*Power(t2,3) + 58*Power(t2,4)) + 
                  t1*(982 + 3710*t2 + 5139*Power(t2,2) + 
                     1342*Power(t2,3) - 97*Power(t2,4) - 6*Power(t2,5))\
) + Power(s2,2)*(-105 - 1363*t2 - 749*Power(t2,2) + 938*Power(t2,3) + 
                  327*Power(t2,4) - 30*Power(t2,5) + 
                  Power(t1,6)*(16 + 21*t2) + 
                  Power(t1,5)*(624 + 57*t2 - 66*Power(t2,2)) + 
                  Power(t1,4)*
                   (-2902 - 1929*t2 + 172*Power(t2,2) + 39*Power(t2,3)) \
+ Power(t1,3)*(4907 + 6769*t2 + 494*Power(t2,2) - 284*Power(t2,3) + 
                     18*Power(t2,4)) - 
                  3*Power(t1,2)*
                   (1263 + 3149*t2 + 531*Power(t2,2) - 
                     541*Power(t2,3) - 41*Power(t2,4) + 4*Power(t2,5)) \
+ t1*(1249 + 5892*t2 + 1742*Power(t2,2) - 2256*Power(t2,3) - 
                     468*Power(t2,4) + 42*Power(t2,5)))) + 
            s1*(3*Power(s2,6)*(-1 + t1)*t2*
                (6 + 6*Power(t1,2) + 21*t2 + 16*Power(t2,2) - 
                  3*t1*(4 + 7*t2)) + 
               (-1 + t1)*(-242 + 177*t2 + 428*Power(t2,2) + 
                  237*Power(t2,3) - 75*Power(t2,4) + 
                  Power(t1,6)*(51 + 9*t2) - 
                  2*Power(t1,5)*(302 + 46*t2 + 11*Power(t2,2)) + 
                  Power(t1,4)*
                   (1651 + 941*t2 + 50*Power(t2,2) + 17*Power(t2,3)) + 
                  Power(t1,2)*
                   (160 + 2657*t2 + 2103*Power(t2,2) + 
                     52*Power(t2,3) - Power(t2,4)) - 
                  Power(t1,3)*
                   (1597 + 2490*t2 + 846*Power(t2,2) + 8*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  t1*(581 - 1202*t2 - 1713*Power(t2,2) - 
                     310*Power(t2,3) + 65*Power(t2,4))) - 
               Power(s2,5)*(28 + 3*Power(t1,5) + 170*t2 + 
                  203*Power(t2,2) - 4*Power(t2,3) - 84*Power(t2,4) + 
                  4*Power(t1,4)*(4 + 9*t2) - 
                  Power(t1,3)*(94 + 278*t2 + 83*Power(t2,2)) + 
                  Power(t1,2)*
                   (156 + 618*t2 + 369*Power(t2,2) - 68*Power(t2,3)) + 
                  t1*(-109 - 546*t2 - 489*Power(t2,2) + 
                     72*Power(t2,3) + 114*Power(t2,4))) + 
               Power(s2,4)*(-95 + 8*Power(t1,6) - 29*t2 + 
                  396*Power(t2,2) + 734*Power(t2,3) + 439*Power(t2,4) + 
                  41*Power(t2,5) + Power(t1,5)*(25 + 28*t2) - 
                  Power(t1,4)*(275 + 277*t2 + 106*Power(t2,2)) + 
                  Power(t1,3)*
                   (690 + 692*t2 + 9*Power(t2,2) - 79*Power(t2,3)) + 
                  Power(t1,2)*
                   (-790 - 694*t2 + 696*Power(t2,2) + 
                     832*Power(t2,3) + 175*Power(t2,4)) - 
                  t1*(-437 - 280*t2 + 995*Power(t2,2) + 
                     1487*Power(t2,3) + 594*Power(t2,4) + 31*Power(t2,5)\
)) + s2*(-1 + t1)*(-116 + 3*Power(t1,7) - 1026*t2 - 153*Power(t2,2) + 
                  811*Power(t2,3) + 321*Power(t2,4) - 50*Power(t2,5) + 
                  Power(t1,6)*(-71 + 11*t2) + 
                  Power(t1,5)*(837 + 42*t2 - 45*Power(t2,2)) + 
                  Power(t1,4)*
                   (-2786 - 388*t2 + 187*Power(t2,2) + 40*Power(t2,3)) \
+ Power(t1,3)*(4117 + 1880*t2 - 875*Power(t2,2) - 239*Power(t2,3) - 
                     4*Power(t2,4)) + 
                  Power(t1,2)*
                   (-3003 - 3845*t2 + 1423*Power(t2,2) + 
                     1604*Power(t2,3) + 41*Power(t2,4) - 5*Power(t2,5)) \
+ t1*(1019 + 3326*t2 - 537*Power(t2,2) - 2252*Power(t2,3) - 
                     319*Power(t2,4) + 40*Power(t2,5))) + 
               Power(s2,3)*(128 + 544*t2 + 1428*Power(t2,2) + 
                  1482*Power(t2,3) + 218*Power(t2,4) - 18*Power(t2,5) - 
                  Power(t2,6) + Power(t1,6)*(-69 + 9*t2) + 
                  Power(t1,5)*(383 + 130*t2 - 41*Power(t2,2)) + 
                  2*Power(t1,4)*
                   (-357 + 56*t2 + 416*Power(t2,2) + 51*Power(t2,3)) - 
                  Power(t1,3)*
                   (-406 + 1750*t2 + 3783*Power(t2,2) + 
                     1447*Power(t2,3) + 97*Power(t2,4)) + 
                  Power(t1,2)*
                   (271 + 3143*t2 + 6662*Power(t2,2) + 
                     4138*Power(t2,3) + 620*Power(t2,4) + 
                     26*Power(t2,5)) + 
                  t1*(-405 - 2188*t2 - 5098*Power(t2,2) - 
                     4275*Power(t2,3) - 741*Power(t2,4) + 
                     30*Power(t2,5) + Power(t2,6))) + 
               Power(s2,2)*(80 + 742*t2 + 1867*Power(t2,2) + 
                  643*Power(t2,3) - 379*Power(t2,4) - 107*Power(t2,5) + 
                  6*Power(t2,6) - 3*Power(t1,7)*(5 + 2*t2) + 
                  Power(t1,6)*(95 + 154*t2 + Power(t2,2)) + 
                  2*Power(t1,5)*
                   (-337 - 972*t2 - 146*Power(t2,2) + 12*Power(t2,3)) + 
                  Power(t1,4)*
                   (2126 + 7380*t2 + 2786*Power(t2,2) + 
                     137*Power(t2,3) - 24*Power(t2,4)) + 
                  Power(t1,3)*
                   (-3139 - 12668*t2 - 8654*Power(t2,2) - 
                     1051*Power(t2,3) + 76*Power(t2,4) + 2*Power(t2,5)) \
+ Power(t1,2)*(2291 + 10980*t2 + 11980*Power(t2,2) + 1974*Power(t2,3) - 
                     555*Power(t2,4) - 51*Power(t2,5) + 3*Power(t2,6)) - 
                  t1*(764 + 4638*t2 + 7688*Power(t2,2) + 
                     1727*Power(t2,3) - 852*Power(t2,4) - 
                     156*Power(t2,5) + 9*Power(t2,6))))) + 
         (-1 + t1)*(Power(s1,7)*Power(s2,2)*(-1 + t1)*
             (4*Power(s2,2) + s2*(7 - 3*t1) + (-1 + t1)*t1) + 
            2*Power(s2,6)*Power(t2,4)*Power(1 - t1 + t2,2) + 
            Power(s2,5)*Power(t2,3)*
             (7 - 4*Power(t1,4) + 19*t2 + 15*Power(t2,2) + 
               4*Power(t2,3) + Power(t1,3)*(5 + 7*t2) + 
               Power(t1,2)*(9 + 5*t2 - 3*Power(t2,2)) - 
               t1*(17 + 31*t2 + 12*Power(t2,2))) + 
            Power(s2,4)*Power(t2,2)*
             (9 + 36*t2 + 25*Power(t2,2) + 12*Power(t2,3) + 
               7*Power(t2,4) + 6*Power(t1,5)*(2 + t2) - 
               Power(t1,4)*(39 + 40*t2 + 14*Power(t2,2)) + 
               Power(t1,3)*(36 + 48*t2 + 32*Power(t2,2) + 
                  11*Power(t2,3)) + 
               Power(t1,2)*(6 + 36*t2 + 3*Power(t2,2) + 4*Power(t2,3) - 
                  3*Power(t2,4)) - 
               t1*(24 + 86*t2 + 46*Power(t2,2) + 27*Power(t2,3) + 
                  10*Power(t2,4))) + 
            Power(s2,2)*Power(-1 + t1,2)*
             (1 + 23*t2 - 28*Power(t2,2) - 6*Power(t2,3) + 
               67*Power(t2,4) + 46*Power(t2,5) + 3*Power(t2,6) + 
               2*Power(t1,5)*(2 + 9*t2) - 
               Power(t1,4)*(15 + 58*t2 + 19*Power(t2,2)) + 
               Power(t1,3)*(20 + 43*t2 - 57*Power(t2,2) - 
                  22*Power(t2,3)) - 
               t1*t2*(65 + 39*t2 + 206*Power(t2,2) + 202*Power(t2,3) + 
                  27*Power(t2,4)) + 
               Power(t1,2)*(-10 + 39*t2 + 143*Power(t2,2) + 
                  234*Power(t2,3) + 47*Power(t2,4))) + 
            Power(-1 + t1,2)*(9*Power(t1,6) - Power(t1,5)*(58 + 19*t2) + 
               Power(t1,4)*(132 + 101*t2 + 22*Power(t2,2)) - 
               Power(t1,3)*(128 + 181*t2 + 96*Power(t2,2) + 
                  14*Power(t2,3)) + 
               Power(t1,2)*(37 + 127*t2 + 132*Power(t2,2) + 
                  45*Power(t2,3) + Power(t2,4)) + 
               t1*(18 - 20*t2 - 64*Power(t2,2) - 41*Power(t2,3) - 
                  6*Power(t2,4) + Power(t2,5)) - 
               2*(5 + 4*t2 - 3*Power(t2,2) - 5*Power(t2,3) - 
                  2*Power(t2,4) + Power(t2,5))) - 
            s2*Power(-1 + t1,2)*
             (-6 + 6*Power(t1,6) + 11*Power(t1,5)*(-2 + t2) + 28*t2 + 
               21*Power(t2,2) - 37*Power(t2,3) - 40*Power(t2,4) - 
               7*Power(t2,5) + Power(t2,6) + 
               Power(t1,4)*(22 - 113*t2 - 45*Power(t2,2)) + 
               Power(t1,3)*(12 + 245*t2 + 254*Power(t2,2) + 
                  57*Power(t2,3)) - 
               Power(t1,2)*(38 + 167*t2 + 352*Power(t2,2) + 
                  231*Power(t2,3) + 34*Power(t2,4)) + 
               t1*(26 - 4*t2 + 122*Power(t2,2) + 211*Power(t2,3) + 
                  81*Power(t2,4) + 4*Power(t2,5))) - 
            Power(s2,3)*(-1 + t1)*t2*
             (5 + 38*t2 + 3*Power(t2,2) + 15*Power(t2,3) + 
               42*Power(t2,4) + 15*Power(t2,5) + 
               6*Power(t1,5)*(2 + 3*t2) - 
               Power(t1,4)*(43 + 75*t2 + 35*Power(t2,2)) + 
               Power(t1,3)*(52 + 79*t2 + 34*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(t1,2)*(-18 + 33*t2 + 40*Power(t2,2) + 
                  92*Power(t2,3) + 9*Power(t2,4)) - 
               t1*(8 + 93*t2 + 42*Power(t2,2) + 122*Power(t2,3) + 
                  91*Power(t2,4) + 7*Power(t2,5))) + 
            Power(s1,6)*s2*(4*Power(s2,4) + 2*Power(s2,5) + 
               2*Power(-1 + t1,3)*t1 + 
               s2*Power(-1 + t1,2)*
                (21 + 2*Power(t1,2) - t2 - t1*(9 + 5*t2)) + 
               Power(s2,3)*(13 + 9*Power(t1,2) + 24*t2 - 
                  4*t1*(7 + 6*t2)) - 
               Power(s2,2)*(-1 + t1)*
                (7*Power(t1,2) - 9*t1*(5 + 2*t2) + 6*(6 + 7*t2))) + 
            Power(s1,5)*(Power(-1 + t1,2)*t1*
                (1 - 2*t1 + 2*Power(t1,2)) + 
               2*Power(s2,5)*(-7 + 5*t1 + 2*Power(t1,2) - 12*t2) + 
               4*Power(s2,6)*(-1 + t1 - 3*t2) + 
               s2*Power(-1 + t1,2)*
                (-21 + 4*Power(t1,3) + 2*t2 - 7*Power(t1,2)*(2 + t2) + 
                  6*t1*(5 + t2)) + 
               Power(s2,2)*Power(-1 + t1,2)*
                (-84 + Power(t1,3) - 105*t2 + 5*Power(t2,2) - 
                  3*Power(t1,2)*(7 + 2*t2) + 
                  t1*(108 + 39*t2 + 10*Power(t2,2))) + 
               Power(s2,4)*(-18 + 2*Power(t1,3) - 71*t2 - 
                  60*Power(t2,2) - Power(t1,2)*(36 + 41*t2) + 
                  4*t1*(13 + 37*t2 + 15*Power(t2,2))) - 
               Power(s2,3)*(-1 + t1)*
                (5*Power(t1,3) - 3*Power(t1,2)*(23 + 12*t2) - 
                  3*(15 + 66*t2 + 35*Power(t2,2)) + 
                  t1*(149 + 236*t2 + 45*Power(t2,2)))) + 
            Power(s1,4)*(Power(-1 + t1,2)*
                (7 + Power(t1,4) - t2 - 4*Power(t1,3)*(2 + t2) + 
                  Power(t1,2)*(16 + t2) - t1*(17 + t2)) + 
               2*Power(s2,6)*
                (1 + Power(t1,2) + 10*t2 + 15*Power(t2,2) - 
                  2*t1*(1 + 5*t2)) + 
               Power(s2,5)*(18 + 8*Power(t1,3) + 
                  Power(t1,2)*(2 - 19*t2) + 71*t2 + 60*Power(t2,2) - 
                  4*t1*(7 + 13*t2)) + 
               s2*Power(-1 + t1,2)*
                (76 + 2*Power(t1,4) + 84*t2 - 8*Power(t2,2) - 
                  2*Power(t1,3)*(11 + 3*t2) - 
                  t1*(173 + 106*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*(110 + 35*t2 + 9*Power(t2,2))) + 
               Power(s2,2)*Power(-1 + t1,2)*
                (57 + Power(t1,3)*(-22 + t2) + 393*t2 + 
                  214*Power(t2,2) - 10*Power(t2,3) + 
                  Power(t1,2)*(161 + 76*t2 + 6*Power(t2,2)) - 
                  t1*(284 + 467*t2 + 69*Power(t2,2) + 10*Power(t2,3))) - 
               Power(s2,3)*(-1 + t1)*
                (9 + Power(t1,4) + 225*t2 + 448*Power(t2,2) + 
                  140*Power(t2,3) - 2*Power(t1,3)*(14 + 13*t2) + 
                  Power(t1,2)*(169 + 300*t2 + 75*Power(t2,2)) - 
                  t1*(151 + 699*t2 + 503*Power(t2,2) + 60*Power(t2,3))) \
+ Power(s2,4)*(32 - 7*Power(t1,4) + 84*t2 + 161*Power(t2,2) + 
                  80*Power(t2,3) + Power(t1,3)*(4 + 3*t2) + 
                  Power(t1,2)*(45 + 148*t2 + 71*Power(t2,2)) - 
                  t1*(74 + 235*t2 + 322*Power(t2,2) + 80*Power(t2,3)))) - 
            Power(s1,3)*(8*Power(s2,6)*t2*
                (1 + Power(t1,2) + 5*t2 + 5*Power(t2,2) - t1*(2 + 5*t2)) \
- Power(-1 + t1,2)*(Power(t1,4)*(-6 + t2) + 
                  3*(-8 - 7*t2 + Power(t2,2)) + 
                  Power(t1,3)*(35 + 10*t2 + 2*Power(t2,2)) + 
                  t1*(78 + 43*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*(-83 - 29*t2 + 3*Power(t2,2))) + 
               Power(s2,5)*(7 - 4*Power(t1,4) + 73*t2 + 
                  144*Power(t2,2) + 80*Power(t2,3) + 
                  Power(t1,3)*(5 + 31*t2) + 
                  Power(t1,2)*(9 + 11*t2 - 36*Power(t2,2)) - 
                  t1*(17 + 115*t2 + 108*Power(t2,2))) + 
               s2*Power(-1 + t1,2)*
                (31 + Power(t1,5) + Power(t1,4)*(29 - 3*t2) + 288*t2 + 
                  135*Power(t2,2) - 13*Power(t2,3) - 
                  Power(t1,3)*(183 + 40*t2) - 
                  2*t1*(132 + 316*t2 + 76*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (386 + 359*t2 + 35*Power(t2,2) + 5*Power(t2,3))) - 
               Power(s2,2)*Power(-1 + t1,2)*
                (41 - 265*t2 - 724*Power(t2,2) - 225*Power(t2,3) + 
                  10*Power(t2,4) + Power(t1,4)*(-9 + 2*t2) + 
                  Power(t1,3)*(98 + 72*t2 - 5*Power(t2,2)) - 
                  Power(t1,2)*
                   (324 + 575*t2 + 113*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(194 + 1118*t2 + 790*Power(t2,2) + 
                     63*Power(t2,3) + 5*Power(t2,4))) + 
               Power(s2,4)*(36 + 4*Power(t1,5) + 123*t2 + 
                  156*Power(t2,2) + 194*Power(t2,3) + 60*Power(t2,4) - 
                  Power(t1,4)*(32 + 33*t2) + 
                  4*Power(t1,3)*(9 + 9*t2 + 8*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (22 + 75*t2 + 116*Power(t2,2) + 27*Power(t2,3)) - 
                  4*t1*(22 + 69*t2 + 105*Power(t2,2) + 92*Power(t2,3) + 
                     15*Power(t2,4))) - 
               Power(s2,3)*(-1 + t1)*
                (22 + 11*Power(t1,4)*(-2 + t2) + 44*t2 + 
                  448*Power(t2,2) + 531*Power(t2,3) + 105*Power(t2,4) - 
                  Power(t1,3)*(24 + 95*t2 + 49*Power(t2,2)) + 
                  Power(t1,2)*
                   (136 + 629*t2 + 498*Power(t2,2) + 79*Power(t2,3)) - 
                  t1*(112 + 589*t2 + 1297*Power(t2,2) + 
                     550*Power(t2,3) + 45*Power(t2,4)))) + 
            Power(s1,2)*(2*Power(s2,6)*Power(t2,2)*
                (6 + 6*Power(t1,2) + 20*t2 + 15*Power(t2,2) - 
                  4*t1*(3 + 5*t2)) + 
               Power(s2,5)*t2*
                (21 - 12*Power(t1,4) + 111*t2 + 146*Power(t2,2) + 
                  60*Power(t2,3) + 15*Power(t1,3)*(1 + 3*t2) + 
                  Power(t1,2)*(27 + 21*t2 - 34*Power(t2,2)) - 
                  t1*(51 + 177*t2 + 112*Power(t2,2))) + 
               Power(-1 + t1,2)*
                (6 + 2*Power(t1,5)*(-6 + t2) + 69*t2 + 26*Power(t2,2) - 
                  5*Power(t2,3) + 
                  Power(t1,4)*(76 + 3*t2 - 2*Power(t2,2)) - 
                  2*Power(t1,3)*(93 + 40*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (198 + 233*t2 + 23*Power(t2,2) - Power(t2,3)) - 
                  t1*(82 + 227*t2 + 47*Power(t2,2) + 4*Power(t2,3))) + 
               s2*Power(-1 + t1,2)*
                (-57 + 139*t2 + 390*Power(t2,2) + 109*Power(t2,3) - 
                  11*Power(t2,4) + Power(t1,5)*(-15 + 2*t2) + 
                  Power(t1,4)*(142 + 55*t2 - 5*Power(t2,2)) + 
                  Power(t1,3)*
                   (-419 - 454*t2 - 37*Power(t2,2) + 2*Power(t2,3)) - 
                  t1*(66 + 856*t2 + 839*Power(t2,2) + 
                     110*Power(t2,3)) + 
                  Power(t1,2)*
                   (415 + 1114*t2 + 449*Power(t2,2) + 21*Power(t2,3) + 
                     Power(t2,4))) - 
               Power(s2,2)*Power(-1 + t1,2)*
                (16 + 3*Power(t1,5) + 76*t2 - 428*Power(t2,2) - 
                  651*Power(t2,3) - 126*Power(t2,4) + 5*Power(t2,5) + 
                  Power(t1,4)*(5 - 33*t2 + 2*Power(t2,2)) + 
                  Power(t1,3)*
                   (93 + 275*t2 + 86*Power(t2,2) - 3*Power(t2,3)) - 
                  Power(t1,2)*
                   (197 + 963*t2 + 726*Power(t2,2) + 82*Power(t2,3)) + 
                  t1*(80 + 645*t2 + 1594*Power(t2,2) + 
                     638*Power(t2,3) + 30*Power(t2,4) + Power(t2,5))) + 
               Power(s2,3)*(-1 + t1)*
                (-40 - 53*t2 - 75*Power(t2,2) - 443*Power(t2,3) - 
                  345*Power(t2,4) - 42*Power(t2,5) + 
                  Power(t1,5)*(-13 + 3*t2) + 
                  Power(t1,4)*(53 + 61*t2 - 18*Power(t2,2)) + 
                  Power(t1,3)*
                   (-41 + 56*t2 + 87*Power(t2,2) + 40*Power(t2,3)) - 
                  Power(t1,2)*
                   (65 + 360*t2 + 837*Power(t2,2) + 381*Power(t2,3) + 
                     42*Power(t2,4)) + 
                  t1*(106 + 293*t2 + 843*Power(t2,2) + 
                     1184*Power(t2,3) + 317*Power(t2,4) + 18*Power(t2,5)\
)) + Power(s2,4)*(9 + 109*t2 + 175*Power(t2,2) + 144*Power(t2,3) + 
                  131*Power(t2,4) + 24*Power(t2,5) + 
                  Power(t1,5)*(12 + 13*t2) - 
                  Power(t1,4)*(39 + 99*t2 + 59*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (18 + 55*t2 + 46*Power(t2,2) + 29*Power(t2,3)) + 
                  Power(t1,2)*
                   (6 + 134*t2 + 168*Power(t2,2) + 168*Power(t2,3) + 
                     11*Power(t2,4)) - 
                  t1*(24 + 267*t2 + 376*Power(t2,2) + 370*Power(t2,3) + 
                     232*Power(t2,4) + 24*Power(t2,5)))) - 
            s1*(4*Power(s2,6)*Power(t2,3)*
                (2 + 2*Power(t1,2) + 5*t2 + 3*Power(t2,2) - 
                  t1*(4 + 5*t2)) + 
               Power(s2,5)*Power(t2,2)*
                (21 - 12*Power(t1,4) + 75*t2 + 74*Power(t2,2) + 
                  24*Power(t2,3) + Power(t1,3)*(15 + 29*t2) + 
                  Power(t1,2)*(27 + 17*t2 - 16*Power(t2,2)) - 
                  t1*(51 + 121*t2 + 58*Power(t2,2))) + 
               Power(-1 + t1,2)*
                (-21 + 7*Power(t1,6) + 28*t2 + 54*Power(t2,2) + 
                  16*Power(t2,3) - 5*Power(t2,4) - 
                  Power(t1,5)*(69 + 8*t2) + 
                  Power(t1,4)*(218 + 98*t2 + 4*Power(t2,2)) - 
                  Power(t1,3)*
                   (289 + 322*t2 + 67*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(t1,2)*
                   (144 + 410*t2 + 197*Power(t2,2) + 11*Power(t2,3) + 
                     Power(t2,4)) - 
                  t1*(-10 + 206*t2 + 188*Power(t2,2) + 27*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*Power(-1 + t1,2)*
                (-28 + 2*Power(t1,6) - 68*t2 + 147*Power(t2,2) + 
                  218*Power(t2,3) + 44*Power(t2,4) - 5*Power(t2,5) - 
                  Power(t1,5)*(19 + 22*t2) + 
                  Power(t1,4)*(125 + 225*t2 + 32*Power(t2,2)) - 
                  Power(t1,3)*
                   (253 + 755*t2 + 342*Power(t2,2) + 19*Power(t2,3)) + 
                  Power(t1,2)*
                   (169 + 855*t2 + 975*Power(t2,2) + 234*Power(t2,3) + 
                     7*Power(t2,4)) - 
                  t1*(-4 + 235*t2 + 812*Power(t2,2) + 461*Power(t2,3) + 
                     38*Power(t2,4))) + 
               Power(s2,4)*t2*
                (18 + 109*t2 + 109*Power(t2,2) + 66*Power(t2,3) + 
                  47*Power(t2,4) + 4*Power(t2,5) + 
                  3*Power(t1,5)*(8 + 5*t2) - 
                  Power(t1,4)*(78 + 107*t2 + 47*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (36 + 61*t2 + 46*Power(t2,2) + 21*Power(t2,3)) + 
                  Power(t1,2)*
                   (12 + 126*t2 + 66*Power(t2,2) + 52*Power(t2,3) - 
                     7*Power(t2,4)) - 
                  t1*(48 + 265*t2 + 220*Power(t2,2) + 160*Power(t2,3) + 
                     76*Power(t2,4) + 4*Power(t2,5))) - 
               Power(s2,2)*Power(-1 + t1,2)*
                (-24 + 39*t2 + 42*Power(t2,2) - 287*Power(t2,3) - 
                  282*Power(t2,4) - 34*Power(t2,5) + Power(t2,6) + 
                  Power(t1,5)*(-15 + 4*t2) + 
                  Power(t1,4)*(45 + 15*t2 - 23*Power(t2,2)) + 
                  Power(t1,3)*
                   (-21 + 176*t2 + 195*Power(t2,2) + 36*Power(t2,3)) - 
                  Power(t1,2)*
                   (57 + 374*t2 + 867*Power(t2,2) + 359*Power(t2,3) + 
                     24*Power(t2,4)) + 
                  t1*(72 + 140*t2 + 653*Power(t2,2) + 962*Power(t2,3) + 
                     234*Power(t2,4) + 6*Power(t2,5))) + 
               Power(s2,3)*(5 + 79*t2 + 33*Power(t2,2) + 55*Power(t2,3) + 
                  217*Power(t2,4) + 115*Power(t2,5) + 7*Power(t2,6) + 
                  2*Power(t1,6)*(-6 - 15*t2 + Power(t2,2)) + 
                  Power(t1,5)*
                   (55 + 153*t2 + 77*Power(t2,2) - 8*Power(t2,3)) + 
                  Power(t1,4)*
                   (-95 - 233*t2 - 91*Power(t2,2) + 13*Power(t2,3) + 
                     12*Power(t2,4)) - 
                  Power(t1,3)*
                   (-70 - 2*t2 + 242*Power(t2,2) + 474*Power(t2,3) + 
                     135*Power(t2,4) + 9*Power(t2,5)) + 
                  Power(t1,2)*
                   (-10 + 312*t2 + 472*Power(t2,2) + 996*Power(t2,3) + 
                     651*Power(t2,4) + 95*Power(t2,5) + 3*Power(t2,6)) - 
                  t1*(13 + 283*t2 + 251*Power(t2,2) + 582*Power(t2,3) + 
                     745*Power(t2,4) + 201*Power(t2,5) + 10*Power(t2,6))))\
))*T3q(s2,1 - s + s2 - t1))/
     ((-1 + t1)*Power(-1 + s + t1,2)*(s - s2 + t1)*
       Power(-1 + s1 + t1 - t2,2)*(-1 + t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,3)) - 
    (8*(-10 + 16*s2 + Power(s2,2) - 7*Power(s2,3) - 5*Power(s2,4) + 
         5*Power(s2,5) + 80*t1 - 84*s2*t1 - 8*Power(s2,2)*t1 + 
         20*Power(s2,3)*t1 + 28*Power(s2,4)*t1 - 24*Power(s2,5)*t1 - 
         207*Power(t1,2) + 149*s2*Power(t1,2) + 
         69*Power(s2,2)*Power(t1,2) - 37*Power(s2,3)*Power(t1,2) - 
         56*Power(s2,4)*Power(t1,2) + 46*Power(s2,5)*Power(t1,2) + 
         204*Power(t1,3) - 76*s2*Power(t1,3) - 
         216*Power(s2,2)*Power(t1,3) + 88*Power(s2,3)*Power(t1,3) + 
         44*Power(s2,4)*Power(t1,3) - 44*Power(s2,5)*Power(t1,3) + 
         4*Power(t1,4) - 66*s2*Power(t1,4) + 
         299*Power(s2,2)*Power(t1,4) - 137*Power(s2,3)*Power(t1,4) - 
         Power(s2,4)*Power(t1,4) + 21*Power(s2,5)*Power(t1,4) - 
         156*Power(t1,5) + 88*s2*Power(t1,5) - 
         192*Power(s2,2)*Power(t1,5) + 100*Power(s2,3)*Power(t1,5) - 
         16*Power(s2,4)*Power(t1,5) - 4*Power(s2,5)*Power(t1,5) + 
         109*Power(t1,6) - 27*s2*Power(t1,6) + 
         47*Power(s2,2)*Power(t1,6) - 27*Power(s2,3)*Power(t1,6) + 
         6*Power(s2,4)*Power(t1,6) - 24*Power(t1,7) - 
         Power(s1,7)*(-1 + s2)*Power(s2,2)*
          (Power(s2,3) + Power(s2,2)*(-2 + t1) + t1 + s2*(-7 + 6*t1)) + 
         Power(s,5)*Power(-1 + s2,4)*(-2 + 2*s1 + 3*t1 - 3*t2)*
          Power(t1 - t2,2) - 28*t2 - 14*s2*t2 + 13*Power(s2,2)*t2 + 
         68*Power(s2,3)*t2 - 64*Power(s2,4)*t2 - 2*Power(s2,5)*t2 + 
         15*Power(s2,6)*t2 + 88*t1*t2 + 230*s2*t1*t2 - 
         189*Power(s2,2)*t1*t2 - 230*Power(s2,3)*t1*t2 + 
         200*Power(s2,4)*t1*t2 + 18*Power(s2,5)*t1*t2 - 
         57*Power(s2,6)*t1*t2 + 15*Power(t1,2)*t2 - 
         644*s2*Power(t1,2)*t2 + 557*Power(s2,2)*Power(t1,2)*t2 + 
         344*Power(s2,3)*Power(t1,2)*t2 - 
         281*Power(s2,4)*Power(t1,2)*t2 - 24*Power(s2,5)*Power(t1,2)*t2 + 
         81*Power(s2,6)*Power(t1,2)*t2 - 365*Power(t1,3)*t2 + 
         596*s2*Power(t1,3)*t2 - 667*Power(s2,2)*Power(t1,3)*t2 - 
         332*Power(s2,3)*Power(t1,3)*t2 + 
         283*Power(s2,4)*Power(t1,3)*t2 - 16*Power(s2,5)*Power(t1,3)*t2 - 
         51*Power(s2,6)*Power(t1,3)*t2 + 545*Power(t1,4)*t2 - 
         14*s2*Power(t1,4)*t2 + 354*Power(s2,2)*Power(t1,4)*t2 + 
         212*Power(s2,3)*Power(t1,4)*t2 - 
         203*Power(s2,4)*Power(t1,4)*t2 + 42*Power(s2,5)*Power(t1,4)*t2 + 
         12*Power(s2,6)*Power(t1,4)*t2 - 327*Power(t1,5)*t2 - 
         250*s2*Power(t1,5)*t2 - 68*Power(s2,2)*Power(t1,5)*t2 - 
         62*Power(s2,3)*Power(t1,5)*t2 + 65*Power(s2,4)*Power(t1,5)*t2 - 
         18*Power(s2,5)*Power(t1,5)*t2 + 72*Power(t1,6)*t2 + 
         96*s2*Power(t1,6)*t2 + 2*Power(t2,2) - 103*s2*Power(t2,2) + 
         21*Power(s2,2)*Power(t2,2) + 11*Power(s2,3)*Power(t2,2) + 
         136*Power(s2,4)*Power(t2,2) - 129*Power(s2,5)*Power(t2,2) + 
         23*Power(s2,6)*Power(t2,2) + 15*Power(s2,7)*Power(t2,2) - 
         122*t1*Power(t2,2) + 312*s2*t1*Power(t2,2) + 
         153*Power(s2,2)*t1*Power(t2,2) - 
         386*Power(s2,3)*t1*Power(t2,2) - 
         262*Power(s2,4)*t1*Power(t2,2) + 
         306*Power(s2,5)*t1*Power(t2,2) - 55*Power(s2,6)*t1*Power(t2,2) - 
         42*Power(s2,7)*t1*Power(t2,2) + 428*Power(t1,2)*Power(t2,2) - 
         2*s2*Power(t1,2)*Power(t2,2) - 
         393*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         1029*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         36*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         260*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         59*Power(s2,6)*Power(t1,2)*Power(t2,2) + 
         39*Power(s2,7)*Power(t1,2)*Power(t2,2) - 
         574*Power(t1,3)*Power(t2,2) - 800*s2*Power(t1,3)*Power(t2,2) + 
         95*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         944*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         170*Power(s2,4)*Power(t1,3)*Power(t2,2) + 
         118*Power(s2,5)*Power(t1,3)*Power(t2,2) - 
         45*Power(s2,6)*Power(t1,3)*Power(t2,2) - 
         12*Power(s2,7)*Power(t1,3)*Power(t2,2) + 
         342*Power(t1,4)*Power(t2,2) + 873*s2*Power(t1,4)*Power(t2,2) + 
         272*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         290*Power(s2,3)*Power(t1,4)*Power(t2,2) - 
         80*Power(s2,4)*Power(t1,4)*Power(t2,2) - 
         35*Power(s2,5)*Power(t1,4)*Power(t2,2) + 
         18*Power(s2,6)*Power(t1,4)*Power(t2,2) - 
         76*Power(t1,5)*Power(t2,2) - 280*s2*Power(t1,5)*Power(t2,2) - 
         148*Power(s2,2)*Power(t1,5)*Power(t2,2) + 30*Power(t2,3) + 
         13*s2*Power(t2,3) - 139*Power(s2,2)*Power(t2,3) + 
         74*Power(s2,3)*Power(t2,3) + 68*Power(s2,4)*Power(t2,3) + 
         56*Power(s2,5)*Power(t2,3) - 90*Power(s2,6)*Power(t2,3) + 
         31*Power(s2,7)*Power(t2,3) + 5*Power(s2,8)*Power(t2,3) - 
         141*t1*Power(t2,3) - 364*s2*t1*Power(t2,3) + 
         342*Power(s2,2)*t1*Power(t2,3) - 
         259*Power(s2,3)*t1*Power(t2,3) - 
         370*Power(s2,4)*t1*Power(t2,3) + 32*Power(s2,5)*t1*Power(t2,3) + 
         126*Power(s2,6)*t1*Power(t2,3) - 53*Power(s2,7)*t1*Power(t2,3) - 
         9*Power(s2,8)*t1*Power(t2,3) + 217*Power(t1,2)*Power(t2,3) + 
         986*s2*Power(t1,2)*Power(t2,3) + 
         174*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         425*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         510*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         237*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         19*Power(s2,6)*Power(t1,2)*Power(t2,3) + 
         28*Power(s2,7)*Power(t1,2)*Power(t2,3) + 
         4*Power(s2,8)*Power(t1,2)*Power(t2,3) - 
         138*Power(t1,3)*Power(t2,3) - 919*s2*Power(t1,3)*Power(t2,3) - 
         793*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         348*Power(s2,3)*Power(t1,3)*Power(t2,3) - 
         208*Power(s2,4)*Power(t1,3)*Power(t2,3) + 
         149*Power(s2,5)*Power(t1,3)*Power(t2,3) - 
         17*Power(s2,6)*Power(t1,3)*Power(t2,3) - 
         6*Power(s2,7)*Power(t1,3)*Power(t2,3) + 
         32*Power(t1,4)*Power(t2,3) + 284*s2*Power(t1,4)*Power(t2,3) + 
         416*Power(s2,2)*Power(t1,4)*Power(t2,3) + 
         108*Power(s2,3)*Power(t1,4)*Power(t2,3) + 8*Power(t2,4) + 
         104*s2*Power(t2,4) + 5*Power(s2,2)*Power(t2,4) - 
         42*Power(s2,3)*Power(t2,4) + 89*Power(s2,4)*Power(t2,4) + 
         62*Power(s2,5)*Power(t2,4) - 33*Power(s2,6)*Power(t2,4) - 
         12*Power(s2,7)*Power(t2,4) + 11*Power(s2,8)*Power(t2,4) - 
         18*t1*Power(t2,4) - 351*s2*t1*Power(t2,4) - 
         403*Power(s2,2)*t1*Power(t2,4) + 16*Power(s2,3)*t1*Power(t2,4) - 
         376*Power(s2,4)*t1*Power(t2,4) - 14*Power(s2,5)*t1*Power(t2,4) + 
         64*Power(s2,6)*t1*Power(t2,4) - Power(s2,7)*t1*Power(t2,4) - 
         9*Power(s2,8)*t1*Power(t2,4) + 13*Power(t1,2)*Power(t2,4) + 
         355*s2*Power(t1,2)*Power(t2,4) + 
         812*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         313*Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         326*Power(s2,4)*Power(t1,2)*Power(t2,4) - 
         48*Power(s2,5)*Power(t1,2)*Power(t2,4) - 
         45*Power(s2,6)*Power(t1,2)*Power(t2,4) + 
         14*Power(s2,7)*Power(t1,2)*Power(t2,4) - 
         4*Power(t1,3)*Power(t2,4) - 112*s2*Power(t1,3)*Power(t2,4) - 
         400*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         288*Power(s2,3)*Power(t1,3)*Power(t2,4) - 
         36*Power(s2,4)*Power(t1,3)*Power(t2,4) - 2*Power(t2,5) + 
         25*s2*Power(t2,5) + 127*Power(s2,2)*Power(t2,5) - 
         11*Power(s2,3)*Power(t2,5) + 73*Power(s2,4)*Power(t2,5) + 
         21*Power(s2,5)*Power(t2,5) + 7*Power(s2,6)*Power(t2,5) - 
         17*Power(s2,7)*Power(t2,5) + 5*Power(s2,8)*Power(t2,5) + 
         t1*Power(t2,5) - 31*s2*t1*Power(t2,5) - 
         288*Power(s2,2)*t1*Power(t2,5) - 
         237*Power(s2,3)*t1*Power(t2,5) - 
         111*Power(s2,4)*t1*Power(t2,5) - 93*Power(s2,5)*t1*Power(t2,5) + 
         60*Power(s2,6)*t1*Power(t2,5) - 9*Power(s2,7)*t1*Power(t2,5) + 
         12*s2*Power(t1,2)*Power(t2,5) + 
         144*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
         256*Power(s2,3)*Power(t1,2)*Power(t2,5) + 
         88*Power(s2,4)*Power(t1,2)*Power(t2,5) + 
         4*Power(s2,5)*Power(t1,2)*Power(t2,5) - s2*Power(t2,6) + 
         18*Power(s2,2)*Power(t2,6) + 71*Power(s2,3)*Power(t2,6) + 
         2*Power(s2,4)*Power(t2,6) + 45*Power(s2,5)*Power(t2,6) - 
         16*Power(s2,6)*Power(t2,6) + Power(s2,7)*Power(t2,6) - 
         12*Power(s2,2)*t1*Power(t2,6) - 80*Power(s2,3)*t1*Power(t2,6) - 
         68*Power(s2,4)*t1*Power(t2,6) - 8*Power(s2,5)*t1*Power(t2,6) + 
         4*Power(s2,3)*Power(t2,7) + 16*Power(s2,4)*Power(t2,7) + 
         4*Power(s2,5)*Power(t2,7) - 
         Power(s1,6)*s2*(3*Power(s2,6) - 2*(-1 + t1)*t1 + 
            Power(s2,5)*(-11 + t1 - 6*t2) + 
            Power(s2,4)*(16 + 2*Power(t1,2) + t1*(6 - 5*t2) + 11*t2) + 
            s2*(-21 - 20*Power(t1,2) + t2 + t1*(39 + 5*t2)) + 
            Power(s2,2)*(-1 + 16*Power(t1,2) - 53*t2 + 
               t1*(14 + 23*t2)) + 
            Power(s2,3)*(14 + 16*Power(t1,2) + 59*t2 - t1*(74 + 35*t2))) \
+ Power(s1,5)*(-4*Power(s2,8) + t1*(1 - 2*t1 + 2*Power(t1,2)) + 
            Power(s2,7)*(22 - 2*t1 + 15*t2) + 
            Power(s2,6)*(-31 + Power(t1,2) + 2*t1*(-2 + t2) - 53*t2 - 
               15*Power(t2,2)) + 
            s2*(-21 + 22*Power(t1,3) + 2*t2 + t1*(63 + 6*t2) - 
               Power(t1,2)*(68 + 7*t2)) + 
            Power(s2,5)*(3 - Power(t1,3) + 36*t2 - 2*Power(t2,2) + 
               Power(t1,2)*(-22 + 6*t2) + 
               t1*(54 + 81*t2 - 10*Power(t2,2))) + 
            Power(s2,2)*(-33 - 5*Power(t1,3) - 135*t2 + 5*Power(t2,2) - 
               3*Power(t1,2)*(31 + 25*t2) + 
               5*t1*(27 + 39*t2 + 2*Power(t2,2))) + 
            Power(s2,3)*(12 - 51*Power(t1,3) + 77*t2 - 
               170*Power(t2,2) + Power(t1,2)*(206 + 91*t2) + 
               t1*(-189 + 13*t2 + 42*Power(t2,2))) + 
            Power(s2,4)*(52 - 15*Power(t1,3) + 130*t2 + 
               206*Power(t2,2) + Power(t1,2)*(74 + 81*t2) - 
               t1*(130 + 417*t2 + 90*Power(t2,2)))) - 
         Power(s1,4)*(-7 - 9*Power(t1,4) + 
            Power(s2,8)*(-10 + 8*t1 - 21*t2) + t2 + 
            4*Power(t1,3)*(9 + t2) + t1*(29 + t2) - 
            Power(t1,2)*(48 + t2) + 
            Power(s2,7)*(16 - 5*Power(t1,2) + 107*t2 + 29*Power(t2,2) - 
               t1*(12 + t2)) - 
            Power(s2,6)*(-37 + Power(t1,3) + 148*t2 + 85*Power(t2,2) + 
               20*Power(t2,3) - 6*Power(t1,2)*(3 + 2*t2) + 
               t1*(40 + 67*t2 + 3*Power(t2,2))) + 
            s2*(-43 - 15*Power(t1,4) - 111*t2 + 8*Power(t2,2) + 
               2*Power(t1,3)*(73 + 30*t2) + 
               t1*(208 + 265*t2 + 6*Power(t2,2)) - 
               Power(t1,2)*(292 + 236*t2 + 9*Power(t2,2))) + 
            Power(s2,5)*(-88 - 35*t2 - 73*Power(t2,2) - 
               62*Power(t2,3) + Power(t1,3)*(11 + t2) + 
               Power(t1,2)*(-35 - 154*t2 + 6*Power(t2,2)) + 
               t1*(112 + 392*t2 + 282*Power(t2,2) - 10*Power(t2,3))) + 
            Power(s2,2)*(57*Power(t1,4) - Power(t1,3)*(170 + 37*t2) + 
               Power(t1,2)*(211 - 352*t2 - 105*Power(t2,2)) + 
               2*(-9 - 30*t2 - 182*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(-94 + 482*t2 + 423*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s2,3)*(57 + 37*Power(t1,4) + 295*t2 + 
               246*Power(t2,2) - 294*Power(t2,3) - 
               Power(t1,3)*(259 + 193*t2) + 
               Power(t1,2)*(590 + 876*t2 + 177*Power(t2,2)) + 
               2*t1*(-212 - 537*t2 + 10*Power(t2,2) + 19*Power(t2,3))) + 
            Power(s2,4)*(56 + 2*Power(t1,4) + Power(t1,3)*(45 - 51*t2) + 
               152*t2 + 359*Power(t2,2) + 318*Power(t2,3) + 
               Power(t1,2)*(-139 + 191*t2 + 147*Power(t2,2)) - 
               t1*(-33 + 418*t2 + 824*Power(t2,2) + 110*Power(t2,3)))) + 
         Power(s1,3)*(-16 + 8*Power(t1,5) - 29*t2 + 3*Power(t2,2) - 
            Power(t1,4)*(70 + 13*t2) + 
            Power(t1,3)*(169 + 72*t2 + 2*Power(t2,2)) + 
            t1*(92 + 93*t2 + 2*Power(t2,2)) + 
            Power(t1,2)*(-183 - 119*t2 + 3*Power(t2,2)) - 
            Power(s2,8)*(5 + 4*Power(t1,2) + 41*t2 + 44*Power(t2,2) - 
               3*t1*(3 + 11*t2)) + 
            Power(s2,7)*(-25 + 4*Power(t1,3) + 62*t2 + 
               206*Power(t2,2) + 26*Power(t2,3) - 
               9*Power(t1,2)*(2 + 3*t2) + 
               3*t1*(13 - 13*t2 + 6*Power(t2,2))) - 
            s2*(31 + 15*Power(t1,5) + 115*t2 + 246*Power(t2,2) - 
               13*Power(t2,3) + Power(t1,4)*(23 + 22*t2) - 
               2*Power(t1,3)*(65 + 217*t2 + 27*Power(t2,2)) - 
               t1*(88 + 669*t2 + 482*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(149 + 950*t2 + 338*Power(t2,2) + 
                  5*Power(t2,3))) - 
            Power(s2,6)*(-79 - 146*t2 + 264*Power(t2,2) + 
               31*Power(t2,3) + 15*Power(t2,4) + 
               Power(t1,3)*(-4 + 11*t2) - 
               Power(t1,2)*(34 + 117*t2 + 31*Power(t2,2)) + 
               t1*(117 + 196*t2 + 239*Power(t2,2) + 11*Power(t2,3))) + 
            Power(s2,4)*(-64 + Power(t1,5) + 58*t2 + 37*Power(t2,2) + 
               435*Power(t2,3) + 217*Power(t2,4) + 
               Power(t1,4)*(-64 + 3*t2) + 
               Power(t1,3)*(239 + 311*t2 - 57*Power(t2,2)) + 
               5*Power(t1,2)*
                (-76 - 207*t2 - Power(t2,2) + 23*Power(t2,3)) + 
               t1*(268 + 651*t2 - 289*Power(t2,2) - 642*Power(t2,3) - 
                  65*Power(t2,4))) + 
            Power(s2,5)*(-22 + Power(t1,4)*(9 - 2*t2) - 371*t2 - 
               144*Power(t2,2) - 320*Power(t2,3) - 113*Power(t2,4) + 
               5*Power(t1,3)*(-12 + 9*t2 + Power(t2,2)) + 
               2*Power(t1,2)*
                (33 - 60*t2 - 163*Power(t2,2) + Power(t2,3)) + 
               t1*(7 + 448*t2 + 941*Power(t2,2) + 428*Power(t2,3) - 
                  5*Power(t2,4))) + 
            Power(s2,2)*(-39*Power(t1,5) + 
               2*Power(t1,4)*(123 + 70*t2) - 
               Power(t1,3)*(782 + 336*t2 + 59*Power(t2,2)) + 
               Power(t1,2)*(1041 + 679*t2 - 701*Power(t2,2) - 
                  65*Power(t2,3)) + 
               2*(17 + 127*t2 - 62*Power(t2,2) - 255*Power(t2,3) + 
                  5*Power(t2,4)) + 
               t1*(-500 - 793*t2 + 976*Power(t2,2) + 501*Power(t2,3) + 
                  5*Power(t2,4))) + 
            Power(s2,3)*(50 - 3*Power(t1,5) + 276*t2 + 816*Power(t2,2) + 
               195*Power(t2,3) - 291*Power(t2,4) + 
               Power(t1,4)*(-2 + 86*t2) + 
               Power(t1,3)*(8 - 611*t2 - 233*Power(t2,2)) + 
               Power(t1,2)*(73 + 1839*t2 + 1048*Power(t2,2) + 
                  145*Power(t2,3)) + 
               t1*(-126 - 1586*t2 - 1795*Power(t2,2) + 202*Power(t2,3) + 
                  17*Power(t2,4)))) + 
         Power(s1,2)*(12 + 2*Power(t1,6) + 37*t2 + 52*Power(t2,2) - 
            5*Power(t2,3) - 2*Power(t1,5)*(26 + 3*t2) + 
            Power(t1,4)*(172 + 127*t2 + 4*Power(t2,2)) - 
            2*Power(t1,3)*(119 + 188*t2 + 31*Power(t2,2)) + 
            Power(t1,2)*(170 + 453*t2 + 143*Power(t2,2) - Power(t2,3)) - 
            t1*(66 + 235*t2 + 143*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s2,8)*t2*(15 + 12*Power(t1,2) + 63*t2 + 
               46*Power(t2,2) - 3*t1*(9 + 17*t2)) - 
            Power(s2,7)*(-15 - 80*t2 + 88*Power(t2,2) + 
               196*Power(t2,3) + 9*Power(t2,4) + 
               Power(t1,3)*(12 + 13*t2) - 
               Power(t1,2)*(39 + 61*t2 + 53*Power(t2,2)) + 
               t1*(42 + 128*t2 - 41*Power(t2,2) + 40*Power(t2,3))) + 
            s2*(-19 - 14*Power(t1,6) - 88*t2 + 220*Power(t2,2) + 
               268*Power(t2,3) - 11*Power(t2,4) + 
               Power(t1,5)*(15 + 22*t2) + 
               Power(t1,4)*(-146 + 262*t2 + 7*Power(t2,2)) + 
               t1*(278 + 41*t2 - 1118*Power(t2,2) - 452*Power(t2,3)) - 
               Power(t1,3)*(-543 + 764*t2 + 581*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(-657 + 527*t2 + 1448*Power(t2,2) + 
                  252*Power(t2,3) + Power(t2,4))) + 
            Power(s2,6)*(15 + Power(t1,4)*(13 - 3*t2) - 251*t2 - 
               213*Power(t2,2) + 215*Power(t2,3) - 52*Power(t2,4) + 
               6*Power(t2,5) + 
               Power(t1,3)*(-22 - 13*t2 + 18*Power(t2,2)) - 
               Power(t1,2)*(-20 + 105*t2 + 222*Power(t2,2) + 
                  30*Power(t2,3)) + 
               t1*(-26 + 372*t2 + 333*Power(t2,2) + 353*Power(t2,3) + 
                  10*Power(t2,4))) + 
            Power(s2,5)*(-101 + 3*Power(t1,5) + 97*t2 + 
               535*Power(t2,2) + 192*Power(t2,3) + 402*Power(t2,4) + 
               89*Power(t2,5) + 
               2*Power(t1,4)*(-20 - 12*t2 + Power(t2,2)) + 
               Power(t1,3)*(87 + 290*t2 - 46*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(t1,2)*(-167 - 396*t2 + 66*Power(t2,2) + 
                  282*Power(t2,3)) + 
               t1*(218 + 33*t2 - 557*Power(t2,2) - 1015*Power(t2,3) - 
                  312*Power(t2,4) + Power(t2,5))) + 
            Power(s2,2)*(-9 - 298*t2 - 562*Power(t2,2) + 
               327*Power(t2,3) + 381*Power(t2,4) - 5*Power(t2,5) + 
               Power(t1,5)*(26 + 42*t2) - 
               Power(t1,4)*(227 + 153*t2 + 83*Power(t2,2)) + 
               Power(t1,3)*(578 + 1309*t2 - 311*Power(t2,2) + 
                  27*Power(t2,3)) + 
               Power(t1,2)*(-588 - 2700*t2 + 150*Power(t2,2) + 
                  862*Power(t2,3) + 15*Power(t2,4)) - 
               t1*(-220 - 1800*t2 - 890*Power(t2,2) + 
                  1334*Power(t2,3) + 330*Power(t2,4) + Power(t2,5))) + 
            Power(s2,3)*(19 - 23*t2 - 395*Power(t2,2) - 
               806*Power(t2,3) + 106*Power(t2,4) + 161*Power(t2,5) + 
               Power(t1,5)*(-82 + 4*t2) + 
               Power(t1,4)*(394 + 194*t2 - 49*Power(t2,2)) + 
               Power(t1,3)*(-772 - 601*t2 + 93*Power(t2,2) + 
                  91*Power(t2,3)) + 
               Power(t1,2)*(709 + 518*t2 - 1475*Power(t2,2) - 
                  36*Power(t2,3) - 43*Power(t2,4)) - 
               t1*(268 + 92*t2 - 1820*Power(t2,2) - 887*Power(t2,3) + 
                  406*Power(t2,4) + 3*Power(t2,5))) + 
            Power(s2,4)*(68 - 2*Power(t1,5)*(-3 + t2) + 251*t2 + 
               148*Power(t2,2) + 247*Power(t2,3) - 241*Power(t2,4) - 
               23*Power(t2,5) - 
               Power(t1,4)*(22 - 161*t2 + Power(t2,2)) + 
               Power(t1,3)*(28 - 840*t2 - 527*Power(t2,2) + 
                  21*Power(t2,3)) + 
               Power(t1,2)*(54 + 1534*t2 + 1997*Power(t2,2) + 
                  375*Power(t2,3) - 33*Power(t2,4)) + 
               t1*(-134 - 1104*t2 - 1599*Power(t2,2) - 267*Power(t2,3) + 
                  42*Power(t2,4) + 15*Power(t2,5)))) - 
         s1*(-7 + 45*Power(t1,6) - 10*t2 + 56*Power(t2,2) + 
            38*Power(t2,3) - 5*Power(t2,4) - 
            Power(t1,5)*(157 + 118*t2) + 
            2*Power(t1,4)*(78 + 235*t2 + 51*Power(t2,2)) - 
            Power(t1,3)*(-31 + 696*t2 + 377*Power(t2,2) + 
               30*Power(t2,3)) + 
            Power(t1,2)*(-134 + 444*t2 + 525*Power(t2,2) + 
               85*Power(t2,3) + Power(t2,4)) - 
            t1*(-66 + 90*t2 + 306*Power(t2,2) + 97*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,8)*Power(t2,2)*
             (15 + 12*Power(t1,2) + 43*t2 + 24*Power(t2,2) - 
               t1*(27 + 35*t2)) + 
            Power(s2,7)*t2*(30 + 86*t2 - 54*Power(t2,2) - 
               92*Power(t2,3) + Power(t2,4) - 
               3*Power(t1,3)*(8 + 5*t2) + 
               Power(t1,2)*(78 + 71*t2 + 45*Power(t2,2)) - 
               t1*(84 + 142*t2 - 13*Power(t2,2) + 32*Power(t2,3))) + 
            s2*(-11 + 21*Power(t1,6) + Power(t1,5)*(26 - 262*t2) - 
               160*t2 - 105*Power(t2,2) + 252*Power(t2,3) + 
               137*Power(t2,4) - 5*Power(t2,5) + 
               Power(t1,4)*(-365 + 537*t2 + 552*Power(t2,2)) + 
               Power(t1,3)*(740 + 193*t2 - 1599*Power(t2,2) - 
                  405*Power(t2,3)) + 
               t1*(194 + 801*t2 - 246*Power(t2,2) - 1008*Power(t2,3) - 
                  203*Power(t2,4)) + 
               Power(t1,2)*(-605 - 1109*t2 + 1398*Power(t2,2) + 
                  1145*Power(t2,3) + 94*Power(t2,4))) + 
            Power(s2,5)*(-5 - 232*t2 + 129*Power(t2,2) + 
               314*Power(t2,3) + 107*Power(t2,4) + 220*Power(t2,5) + 
               32*Power(t2,6) + Power(t1,5)*(-15 + 4*t2) + 
               Power(t1,4)*(27 - 81*t2 - 17*Power(t2,2)) + 
               Power(t1,3)*(14 + 219*t2 + 387*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,2)*(-54 - 443*t2 - 579*Power(t2,2) - 
                  67*Power(t2,3) + 92*Power(t2,4)) + 
               t1*(33 + 533*t2 + 80*Power(t2,2) - 235*Power(t2,3) - 
                  505*Power(t2,4) - 99*Power(t2,5))) + 
            Power(s2,6)*(15 + 37*t2 - 261*Power(t2,2) - 
               137*Power(t2,3) + 75*Power(t2,4) - 56*Power(t2,5) + 
               Power(t2,6) - 2*Power(t1,4)*(-6 - 15*t2 + Power(t2,2)) + 
               Power(t1,3)*(-51 - 63*t2 - 30*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(81 + 73*t2 - 84*Power(t2,2) - 
                  168*Power(t2,3) - 10*Power(t2,4)) + 
               t1*(-57 - 77*t2 + 377*Power(t2,2) + 241*Power(t2,3) + 
                  237*Power(t2,4) + 3*Power(t2,5))) + 
            Power(s2,2)*(25 + 33*Power(t1,6) + 3*t2 - 403*Power(t2,2) - 
               285*Power(t2,3) + 357*Power(t2,4) + 139*Power(t2,5) - 
               Power(t2,6) - Power(t1,5)*(163 + 132*t2) + 
               Power(t1,4)*(416 + 76*t2 + 521*Power(t2,2)) - 
               Power(t1,3)*(625 - 649*t2 + 284*Power(t2,2) + 
                  877*Power(t2,3)) + 
               Power(t1,2)*(514 - 995*t2 - 1473*Power(t2,2) + 
                  1430*Power(t2,3) + 564*Power(t2,4)) - 
               t1*(200 - 399*t2 - 1639*Power(t2,2) + 212*Power(t2,3) + 
                  993*Power(t2,4) + 108*Power(t2,5))) + 
            Power(s2,4)*(-49 - 2*Power(t1,6) + 204*t2 + 
               253*Power(t2,2) + 239*Power(t2,3) + 257*Power(t2,4) - 
               47*Power(t2,5) + 44*Power(t2,6) + 
               2*Power(t1,5)*(33 + 5*t2) + 
               Power(t1,4)*(-172 - 118*t2 + 91*Power(t2,2)) - 
               3*Power(t1,3)*
                (-63 - 74*t2 + 265*Power(t2,2) + 99*Power(t2,3)) + 
               Power(t1,2)*(-165 + 74*t2 + 1648*Power(t2,2) + 
                  1427*Power(t2,3) + 341*Power(t2,4)) - 
               t1*(-133 + 392*t2 + 1197*Power(t2,2) + 
                  1357*Power(t2,3) + 379*Power(t2,4) + 187*Power(t2,5))) \
- Power(s2,3)*(Power(t1,6) + 3*Power(t1,5)*(23 + 34*t2) - 
               Power(t1,4)*(214 + 790*t2 + 313*Power(t2,2)) + 
               Power(t1,3)*(250 + 1940*t2 + 983*Power(t2,2) + 
                  547*Power(t2,3)) - 
               Power(t1,2)*(171 + 1974*t2 + 1074*Power(t2,2) + 
                  87*Power(t2,3) + 598*Power(t2,4)) + 
               t1*(97 + 778*t2 + 514*Power(t2,2) - 674*Power(t2,3) + 
                  260*Power(t2,4) + 305*Power(t2,5)) - 
               2*(16 + 28*t2 + 55*Power(t2,2) - 109*Power(t2,3) - 
                  142*Power(t2,4) + 102*Power(t2,5) + 22*Power(t2,6)))) - 
         Power(s,4)*Power(-1 + s2,3)*
          (-4 + 41*t1 - 50*Power(t1,2) - 2*Power(t1,3) + 
            16*Power(t1,4) - 29*t2 + 86*t1*t2 + 4*Power(t1,2)*t2 - 
            52*Power(t1,3)*t2 - 36*Power(t2,2) - 2*t1*Power(t2,2) + 
            60*Power(t1,2)*Power(t2,2) - 28*t1*Power(t2,3) + 
            4*Power(t2,4) + Power(s1,3)*(-1 + s2)*
             (-8 + (7 + s2)*t1 + t2 - s2*t2) - 
            Power(s2,3)*(-1 + t1)*
             (-t1 + Power(t1,2) + t2 - 2*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(4 - 2*Power(t1,3)*(-4 + t2) + 11*t2 + 
               2*Power(t2,2) - Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,2)*(-4 - 17*t2 + 6*Power(t2,2)) + 
               t1*(-7 + 2*t2 + 10*Power(t2,2) - 6*Power(t2,3))) + 
            s2*(3*Power(t1,4) - 2*Power(t1,3)*(23 + 11*t2) + 
               2*Power(t1,2)*(37 + 67*t2 + 24*Power(t2,2)) + 
               t2*(17 + 55*t2 + 42*Power(t2,2) + 13*Power(t2,3)) - 
               t1*(33 + 129*t2 + 130*Power(t2,2) + 42*Power(t2,3))) + 
            Power(s1,2)*(-20 - 5*Power(t1,2) + t1*(63 - 2*t2) - 35*t2 + 
               7*Power(t2,2) + 
               Power(s2,2)*(4 + 2*Power(t1,2) + 7*t2 + 4*Power(t2,2) - 
                  3*t1*(1 + 2*t2)) + 
               s2*(16 + 25*Power(t1,2) + 28*t2 + 11*Power(t2,2) - 
                  12*t1*(5 + 3*t2))) + 
            s1*(16 + 13*Power(t1,3) + Power(t1,2)*(56 - 36*t2) - 
               Power(s2,3)*(-1 + t1)*(t1 - t2) + 65*t2 + 
               30*Power(t2,2) - 10*Power(t2,3) + 
               t1*(-97 - 86*t2 + 33*Power(t2,2)) + 
               Power(s2,2)*(-8 + Power(t1,3) + Power(t1,2)*(1 - 7*t2) - 
                  17*t2 - 7*Power(t2,2) - 5*Power(t2,3) + 
                  t1*(9 + 6*t2 + 11*Power(t2,2))) + 
               s2*(-8 + 27*Power(t1,3) - 47*t2 - 67*Power(t2,2) - 
                  26*Power(t2,3) - 20*Power(t1,2)*(5 + 4*t2) + 
                  t1*(87 + 167*t2 + 79*Power(t2,2))))) + 
         Power(s,3)*Power(-1 + s2,2)*
          (17 - 170*t1 + 300*Power(t1,2) - 152*Power(t1,3) - 
            13*Power(t1,4) + 18*Power(t1,5) + 104*t2 - 369*t1*t2 + 
            250*Power(t1,2)*t2 + 65*Power(t1,3)*t2 - 55*Power(t1,4)*t2 + 
            117*Power(t2,2) - 88*t1*Power(t2,2) - 
            111*Power(t1,2)*Power(t2,2) + 55*Power(t1,3)*Power(t2,2) - 
            10*Power(t2,3) + 79*t1*Power(t2,3) - 
            15*Power(t1,2)*Power(t2,3) - 20*Power(t2,4) - 
            5*t1*Power(t2,4) + 2*Power(t2,5) - 
            Power(s2,5)*(-1 + t1)*
             (-2*t1 + 2*Power(t1,2) + t2 - 3*t1*t2 + Power(t2,2)) + 
            Power(s1,4)*(-1 + s2)*
             (8 + Power(s2,3) - 6*t1 + Power(s2,2)*(-2 + 4*t1 - 3*t2) - 
               t2 + s2*(-31 + 26*t1 + 4*t2)) + 
            Power(s2,4)*(-(Power(t1,3)*(-13 + t2)) + 
               Power(1 + t2,2)*(-1 + 14*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-23 - 9*t2 + 10*Power(t2,2)) - 
               t1*(-11 + 3*t2 + 34*Power(t2,2) + 17*Power(t2,3))) + 
            Power(s2,3)*(2 - 7*t2 - 23*Power(t2,2) + 31*Power(t2,3) + 
               35*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,4)*(3 + 2*t2) - 
               Power(t1,3)*(59 + 65*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(104 + 191*t2 + 156*Power(t2,2)) + 
               t1*(-50 - 130*t2 - 163*Power(t2,2) - 129*Power(t2,3) + 
                  4*Power(t2,4))) + 
            s2*(-10 + Power(t1,5) - 34*t2 - 44*Power(t2,2) + 
               39*Power(t2,3) + 11*Power(t2,4) - 4*Power(t2,5) - 
               Power(t1,4)*(115 + 34*t2) + 
               Power(t1,3)*(312 + 315*t2 + 100*Power(t2,2)) - 
               2*Power(t1,2)*
                (158 + 242*t2 + 137*Power(t2,2) + 53*Power(t2,3)) + 
               t1*(128 + 236*t2 + 133*Power(t2,2) + 63*Power(t2,3) + 
                  43*Power(t2,4))) - 
            Power(s2,2)*(Power(t1,5) + Power(t1,4)*(-41 + 3*t2) - 
               Power(t1,3)*(2 + 22*t2 + 29*Power(t2,2)) + 
               Power(t1,2)*(117 + 293*t2 + 285*Power(t2,2) + 
                  59*Power(t2,3)) - 
               t1*(83 + 366*t2 + 495*Power(t2,2) + 340*Power(t2,3) + 
                  48*Power(t2,4)) + 
               2*(4 + 38*t2 + 67*Power(t2,2) + 102*Power(t2,3) + 
                  59*Power(t2,4) + 7*Power(t2,5))) + 
            Power(s1,3)*(-7 - 22*Power(t1,2) + 
               Power(s2,4)*(-5 + t1 - 8*t2) + 35*t2 - 3*Power(t2,2) - 
               t1*(8 + 5*t2) + 
               Power(s2,3)*(30 + 8*Power(t1,2) + 31*t2 + 
                  13*Power(t2,2) - t1*(1 + 23*t2)) + 
               s2*(-62 - 15*Power(t1,2) - 209*t2 + 26*Power(t2,2) + 
                  t1*(267 + 47*t2)) + 
               Power(s2,2)*(44 + 77*Power(t1,2) + 151*t2 + 
                  12*Power(t2,2) - t1*(259 + 115*t2))) + 
            Power(s1,2)*(53 + Power(s2,5)*(-1 + t1) - 42*Power(t1,3) + 
               67*t2 - 76*Power(t2,2) + 5*Power(t2,3) + 
               2*Power(t1,2)*(71 + 21*t2) - 
               Power(s2,4)*(-8 + t1 + 4*Power(t1,2) - 40*t2 + 
                  12*t1*t2 - 21*Power(t2,2)) + 
               t1*(-198 + 34*t2 - 5*Power(t2,2)) + 
               s2*(4 + 75*Power(t1,3) + Power(t1,2)*(94 - 97*t2) + 
                  282*t2 + 299*Power(t2,2) - 41*Power(t2,3) + 
                  7*t1*(-33 - 86*t2 + 9*Power(t2,2))) + 
               Power(s2,3)*(-47 + 4*Power(t1,3) + 
                  Power(t1,2)*(38 - 27*t2) - 126*t2 - 21*Power(t2,2) - 
                  19*Power(t2,3) + t1*(-22 - 36*t2 + 42*Power(t2,2))) + 
               Power(s2,2)*(-17 + 77*Power(t1,3) - 263*t2 - 
                  367*Power(t2,2) - 59*Power(t2,3) - 
                  2*Power(t1,2)*(207 + 130*t2) + 
                  t1*(451 + 904*t2 + 242*Power(t2,2)))) + 
            s1*(-55 - 6*Power(t1,4) - Power(s2,5)*(-1 + Power(t1,2)) - 
               208*t2 - 48*Power(t2,2) + 69*Power(t2,3) - 
               5*Power(t2,4) + 11*Power(t1,3)*(11 + t2) - 
               3*Power(t1,2)*(142 + 29*t2 + 3*Power(t2,2)) + 
               t1*(371 + 356*t2 - 103*Power(t2,2) + 9*Power(t2,3)) - 
               Power(s2,4)*(3 + 4*Power(t1,3) + 
                  2*Power(t1,2)*(-7 + t2) + 49*t2 + 65*Power(t2,2) + 
                  22*Power(t2,3) + t1*(6 - 37*t2 - 28*Power(t2,2))) + 
               s2*(29 + 79*Power(t1,4) - 29*t2 - 245*Power(t2,2) - 
                  140*Power(t2,3) + 24*Power(t2,4) - 
                  Power(t1,3)*(229 + 231*t2) + 
                  Power(t1,2)*(259 + 142*t2 + 249*Power(t2,2)) + 
                  t1*(-137 + 261*t2 + 227*Power(t2,2) - 121*Power(t2,3))\
) + Power(s2,3)*(18 - 5*Power(t1,3)*(-11 + t2) + 115*t2 + 
                  62*Power(t2,2) - 42*Power(t2,3) + 11*Power(t2,4) + 
                  3*Power(t1,2)*(-41 - 60*t2 + 7*Power(t2,2)) + 
                  t1*(59 + 131*t2 + 167*Power(t2,2) - 27*Power(t2,3))) + 
               Power(s2,2)*(10 + 11*Power(t1,4) + 171*t2 + 
                  440*Power(t2,2) + 363*Power(t2,3) + 54*Power(t2,4) - 
                  3*Power(t1,3)*(57 + 37*t2) + 
                  Power(t1,2)*(421 + 811*t2 + 243*Power(t2,2)) - 
                  t1*(287 + 1073*t2 + 1003*Power(t2,2) + 197*Power(t2,3))\
))) - Power(s,2)*(-1 + s2)*(-24 + 277*t1 - 611*Power(t1,2) + 
            461*Power(t1,3) - 71*Power(t1,4) - 38*Power(t1,5) + 
            6*Power(t1,6) - 140*t2 + 540*t1*t2 - 494*Power(t1,2)*t2 - 
            30*Power(t1,3)*t2 + 141*Power(t1,4)*t2 - 17*Power(t1,5)*t2 - 
            115*Power(t2,2) + 42*t1*Power(t2,2) + 
            263*Power(t1,2)*Power(t2,2) - 183*Power(t1,3)*Power(t2,2) + 
            14*Power(t1,4)*Power(t2,2) + 63*Power(t2,3) - 
            198*t1*Power(t2,3) + 89*Power(t1,2)*Power(t2,3) + 
            36*Power(t2,4) - 3*t1*Power(t2,4) - 
            4*Power(t1,2)*Power(t2,4) - 6*Power(t2,5) + t1*Power(t2,5) + 
            Power(s2,7)*t2*(t1 - Power(t1,2) + t2 + Power(t2,2)) + 
            Power(s1,5)*(-1 + s2)*
             (3*Power(s2,4) - t1 + Power(s2,3)*(-7 + 6*t1 - 3*t2) + 
               s2*(23 - 17*t1 - 2*t2) + Power(s2,2)*(-43 + 36*t1 + 5*t2)\
) + Power(s2,6)*(-2 + 11*t2 + 45*Power(t2,2) + 43*Power(t2,3) + 
               13*Power(t2,4) + Power(t1,3)*(1 + 4*t2) + 
               Power(t1,2)*(-4 + 11*t2 + 6*Power(t2,2)) - 
               t1*(-5 + 26*t2 + 55*Power(t2,2) + 23*Power(t2,3))) + 
            Power(s2,5)*(14 + 30*t2 - 53*Power(t2,2) - 80*Power(t2,3) - 
               29*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,4)*(-4 + 3*t2) - 
               Power(t1,3)*(3 + 76*t2 + 14*Power(t2,2)) + 
               Power(t1,2)*(32 + 140*t2 + 164*Power(t2,2) + 
                  13*Power(t2,3)) + 
               t1*(-39 - 97*t2 - 90*Power(t2,2) - 55*Power(t2,3) + 
                  4*Power(t2,4))) + 
            s2*(26 + 17*t2 - 175*Power(t2,2) - 245*Power(t2,3) + 
               18*Power(t2,4) + 30*Power(t2,5) - Power(t2,6) - 
               2*Power(t1,5)*(34 + 9*t2) + 
               Power(t1,4)*(273 + 228*t2 + 55*Power(t2,2)) - 
               Power(t1,3)*(547 + 234*t2 + 316*Power(t2,2) + 
                  56*Power(t2,3)) + 
               Power(t1,2)*(573 - 89*t2 - 182*Power(t2,2) + 
                  250*Power(t2,3) + 18*Power(t2,4)) + 
               t1*(-257 + 96*t2 + 603*Power(t2,2) + 125*Power(t2,3) - 
                  124*Power(t2,4) + 2*Power(t2,5))) + 
            Power(s2,4)*(-28 - 3*Power(t1,5) - 164*t2 - 
               160*Power(t2,2) - 116*Power(t2,3) - 135*Power(t2,4) - 
               61*Power(t2,5) - 
               2*Power(t1,4)*(-35 - 6*t2 + Power(t2,2)) + 
               Power(t1,3)*(-143 + 48*t2 + 68*Power(t2,2) + 
                  6*Power(t2,3)) - 
               Power(t1,2)*(-60 + 236*t2 + 547*Power(t2,2) + 
                  221*Power(t2,3) + 6*Power(t2,4)) + 
               t1*(44 + 340*t2 + 656*Power(t2,2) + 564*Power(t2,3) + 
                  205*Power(t2,4) + 2*Power(t2,5))) + 
            Power(s2,3)*(24 + 129*t2 + 181*Power(t2,2) + 
               200*Power(t2,3) + 245*Power(t2,4) + 108*Power(t2,5) + 
               5*Power(t2,6) + 2*Power(t1,5)*(1 + t2) - 
               Power(t1,4)*(195 + 133*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(516 + 566*t2 + 264*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-431 - 740*t2 - 122*Power(t2,2) - 
                  29*Power(t2,3) + 22*Power(t2,4)) - 
               t1*(-84 - 176*t2 + 333*Power(t2,2) + 494*Power(t2,3) + 
                  212*Power(t2,4) + 18*Power(t2,5))) + 
            Power(s2,2)*(-10 + Power(t1,5)*(49 - 3*t2) + 117*t2 + 
               316*Power(t2,2) + 260*Power(t2,3) - 10*Power(t2,4) - 
               7*Power(t2,5) + 2*Power(t2,6) + 
               13*Power(t1,4)*(5 + 3*t2 + 2*Power(t2,2)) - 
               Power(t1,3)*(411 + 830*t2 + 399*Power(t2,2) + 
                  62*Power(t2,3)) + 
               Power(t1,2)*(421 + 1787*t2 + 1246*Power(t2,2) + 
                  478*Power(t2,3) + 60*Power(t2,4)) - 
               t1*(114 + 1110*t2 + 1201*Power(t2,2) + 471*Power(t2,3) + 
                  160*Power(t2,4) + 23*Power(t2,5))) + 
            Power(s1,4)*(23 + 3*Power(s2,6) + 23*Power(t1,2) + 
               Power(s2,5)*(-23 + 3*t1 - 22*t2) - 3*t2 - t1*(43 + t2) + 
               Power(s2,4)*(74 + 12*Power(t1,2) + t1*(12 - 33*t2) + 
                  58*t2 + 14*Power(t2,2)) - 
               s2*(40 + 81*Power(t1,2) - 130*t2 + 8*Power(t2,2) + 
                  19*t1*(-1 + 2*t2)) + 
               Power(s2,3)*(35 + 101*Power(t1,2) + 272*t2 - 
                  2*Power(t2,2) - 2*t1*(209 + 84*t2)) + 
               Power(s2,2)*(-72 - 15*Power(t1,2) - 435*t2 + 
                  36*Power(t2,2) + t1*(427 + 160*t2))) + 
            Power(s1,3)*(-26 + 3*Power(t1,3) - 96*t2 + 9*Power(t2,2) - 
               Power(t1,2)*(81 + 44*t2) + 
               t1*(131 + 104*t2 + 2*Power(t2,2)) + 
               Power(s2,5)*(70 + t1 - 6*Power(t1,2) + 140*t2 - 
                  29*t1*t2 + 56*Power(t2,2)) + 
               Power(s2,6)*(4*t1 - 19*(1 + t2)) + 
               s2*(141 - 129*Power(t1,3) + 321*t2 - 308*Power(t2,2) + 
                  13*Power(t2,3) + 13*Power(t1,2)*(46 + 15*t2) + 
                  t1*(-717 - 68*t2 + 34*Power(t2,2))) + 
               Power(s2,4)*(-127 + 6*Power(t1,3) + 
                  Power(t1,2)*(96 - 39*t2) - 261*t2 - 41*Power(t2,2) - 
                  24*Power(t2,3) + t1*(-117 - 164*t2 + 61*Power(t2,2))) \
+ Power(s2,2)*(-8 + 147*Power(t1,3) + 400*t2 + 894*Power(t2,2) - 
                  68*Power(t2,3) - Power(t1,2)*(131 + 99*t2) - 
                  t1*(94 + 1370*t2 + 85*Power(t2,2))) + 
               Power(s2,3)*(-31 + 99*Power(t1,3) - 485*t2 - 
                  770*Power(t2,2) - 47*Power(t2,3) - 
                  Power(t1,2)*(636 + 391*t2) + 
                  t1*(792 + 1847*t2 + 366*Power(t2,2)))) + 
            Power(s1,2)*(-33 - 35*Power(t1,4) + 9*t2 + 
               183*Power(t2,2) - 15*Power(t2,3) + 
               6*Power(t1,3)*(16 + t2) + Power(s2,7)*(-2 + 3*t1 + t2) + 
               Power(t1,2)*(-167 + 171*t2 + 33*Power(t2,2)) - 
               2*t1*(-80 + 143*t2 + 62*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,6)*(29 - 7*Power(t1,2) + 74*t2 + 
                  42*Power(t2,2) - 2*t1*(13 + 12*t2)) + 
               Power(s2,5)*(-57 - 6*Power(t1,3) - 277*t2 - 
                  244*Power(t2,2) - 64*Power(t2,3) + 
                  Power(t1,2)*(65 + 2*t2) + 
                  t1*(5 + 23*t2 + 57*Power(t2,2))) + 
               Power(s2,4)*(24 + Power(t1,3)*(98 - 3*t2) + 246*t2 + 
                  138*Power(t2,2) - 113*Power(t2,3) + 18*Power(t2,4) + 
                  Power(t1,2)*(-295 - 472*t2 + 30*Power(t2,2)) + 
                  t1*(188 + 746*t2 + 530*Power(t2,2) - 45*Power(t2,3))) \
+ s2*(-76 + 13*Power(t1,4) - 798*t2 - 476*Power(t2,2) + 
                  348*Power(t2,3) - 11*Power(t2,4) + 
                  10*Power(t1,3)*(43 + 11*t2) - 
                  Power(t1,2)*(1351 + 1066*t2 + 96*Power(t2,2)) + 
                  t1*(969 + 2042*t2 - 106*Power(t2,2) - 16*Power(t2,3))\
) + Power(s2,2)*(145*Power(t1,4) - Power(t1,3)*(822 + 457*t2) + 
                  Power(t1,2)*(1523 + 809*t2 + 375*Power(t2,2)) + 
                  t2*(299 - 559*t2 - 754*Power(t2,2) + 
                     58*Power(t2,3)) - 
                  t1*(858 + 274*t2 - 1192*Power(t2,2) + 
                     121*Power(t2,3))) + 
               Power(s2,3)*(115 + 15*Power(t1,4) + 446*t2 + 
                  1156*Power(t2,2) + 976*Power(t2,3) + 73*Power(t2,4) - 
                  2*Power(t1,3)*(87 + 104*t2) + 
                  2*Power(t1,2)*(236 + 845*t2 + 243*Power(t2,2)) - 
                  t1*(441 + 2707*t2 + 2683*Power(t2,2) + 
                     366*Power(t2,3)))) + 
            s1*(59 - 20*Power(t1,5) + 229*t2 - 60*Power(t2,2) - 
               146*Power(t2,3) + 15*Power(t2,4) + 
               Power(t1,4)*(74 + 34*t2) + 
               Power(t1,3)*(-433 + 42*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*(842 + 36*t2 - 197*Power(t2,2) - 
                  8*Power(t2,3)) + 
               t1*(-522 - 383*t2 + 383*Power(t2,2) + 66*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,7)*(2 + 3*Power(t1,2) + t2 - 2*Power(t2,2) - 
                  t1*(5 + 3*t2)) - 
               Power(s2,6)*(12 + 8*Power(t1,3) + 
                  Power(t1,2)*(4 - 5*t2) + 70*t2 + 98*Power(t2,2) + 
                  39*Power(t2,3) - t1*(24 + 73*t2 + 43*Power(t2,2))) + 
               Power(s2,5)*(-1 + 120*t2 + 289*Power(t2,2) + 
                  156*Power(t2,3) + 33*Power(t2,4) + 
                  Power(t1,3)*(52 + 9*t2) - 
                  Power(t1,2)*(72 + 197*t2 + 7*Power(t2,2)) + 
                  t1*(21 + 54*t2 + 27*Power(t2,2) - 35*Power(t2,3))) + 
               s2*(-22 + 75*Power(t1,5) + 327*t2 + 893*Power(t2,2) + 
                  177*Power(t2,3) - 177*Power(t2,4) + 5*Power(t2,5) - 
                  Power(t1,4)*(122 + 167*t2) + 
                  Power(t1,3)*(-110 - 97*t2 + 121*Power(t2,2)) + 
                  Power(t1,2)*
                   (226 + 1872*t2 + 117*Power(t2,2) - 36*Power(t2,3)) + 
                  t1*(-47 - 1905*t2 - 1386*Power(t2,2) + 
                     279*Power(t2,3) + 2*Power(t2,4))) + 
               Power(s2,4)*(52 + 159*t2 + 4*Power(t2,2) + 
                  184*Power(t2,3) + 167*Power(t2,4) - 5*Power(t2,5) + 
                  Power(t1,4)*(-3 + 6*t2) - 
                  Power(t1,3)*(73 + 193*t2 + 13*Power(t2,2)) + 
                  Power(t1,2)*
                   (147 + 907*t2 + 612*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(-123 - 909*t2 - 1211*Power(t2,2) - 
                     583*Power(t2,3) + 9*Power(t2,4))) + 
               Power(s2,2)*(9 + 6*Power(t1,5) - 356*t2 - 
                  576*Power(t2,2) + 241*Power(t2,3) + 236*Power(t2,4) - 
                  21*Power(t2,5) - Power(t1,4)*(383 + 152*t2) + 
                  Power(t1,3)*(1370 + 1405*t2 + 366*Power(t2,2)) - 
                  Power(t1,2)*
                   (1721 + 3234*t2 + 1169*Power(t2,2) + 
                     321*Power(t2,3)) + 
                  t1*(719 + 2361*t2 + 883*Power(t2,2) - 
                     89*Power(t2,3) + 122*Power(t2,4))) - 
               Power(s2,3)*(87 + 3*Power(t1,5) + 410*t2 + 
                  610*Power(t2,2) + 951*Power(t2,3) + 550*Power(t2,4) + 
                  37*Power(t2,5) + Power(t1,4)*(-158 + 11*t2) + 
                  Power(t1,3)*(420 + 62*t2 - 113*Power(t2,2)) + 
                  Power(t1,2)*
                   (-419 + 523*t2 + 1012*Power(t2,2) + 218*Power(t2,3)) \
- t1*(-67 + 1032*t2 + 2395*Power(t2,2) + 1466*Power(t2,3) + 
                     156*Power(t2,4))))) + 
         s*(19 - 219*t1 + 561*Power(t1,2) - 506*Power(t1,3) + 
            57*Power(t1,4) + 141*Power(t1,5) - 53*Power(t1,6) + 90*t2 - 
            347*t1*t2 + 244*Power(t1,2)*t2 + 361*Power(t1,3)*t2 - 
            516*Power(t1,4)*t2 + 168*Power(t1,5)*t2 + 33*Power(t2,2) + 
            152*t1*Power(t2,2) - 628*Power(t1,2)*Power(t2,2) + 
            631*Power(t1,3)*Power(t2,2) - 188*Power(t1,4)*Power(t2,2) - 
            80*Power(t2,3) + 286*t1*Power(t2,3) - 
            288*Power(t1,2)*Power(t2,3) + 82*Power(t1,3)*Power(t2,3) - 
            28*Power(t2,4) + 26*t1*Power(t2,4) - 
            7*Power(t1,2)*Power(t2,4) + 6*Power(t2,5) - 
            2*t1*Power(t2,5) + Power(s2,9)*Power(t2,2)*(1 - t1 + t2) + 
            Power(s1,6)*(-1 + s2)*s2*
             (3*Power(s2,4) - 2*t1 + Power(s2,3)*(-8 + 4*t1 - t2) - 
               s2*(-22 + 16*t1 + t2) + Power(s2,2)*(-25 + 22*t1 + 2*t2)) \
+ Power(s2,8)*t2*(2 + 7*t2 + 13*Power(t2,2) + 7*Power(t2,3) + 
               Power(t1,2)*(2 + 8*t2) - t1*(4 + 15*t2 + 15*Power(t2,2))) \
- Power(s2,7)*(-1 - 10*t2 - 2*Power(t2,2) + 72*Power(t2,3) + 
               62*Power(t2,4) + 9*Power(t2,5) + 
               Power(t1,3)*(1 + 16*t2 + 11*Power(t2,2)) - 
               Power(t1,2)*(3 + 42*t2 + 16*Power(t2,2) + 
                  14*Power(t2,3)) + 
               t1*(3 + 36*t2 + 7*Power(t2,2) - 59*Power(t2,3) - 
                  6*Power(t2,4))) - 
            Power(s2,6)*(-4 + 37*t2 + 173*Power(t2,2) + 6*Power(t2,3) - 
               56*Power(t2,4) + 15*Power(t2,5) + Power(t2,6) + 
               2*Power(t1,4)*(-4 - 11*t2 + Power(t2,2)) - 
               4*Power(t1,3)*
                (-7 - t2 + 14*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(-36 + 95*t2 + 219*Power(t2,2) + 
                  220*Power(t2,3) + 19*Power(t2,4)) - 
               t1*(-20 + 114*t2 + 338*Power(t2,2) + 204*Power(t2,3) + 
                  157*Power(t2,4) + 10*Power(t2,5))) + 
            s2*(-38 + 23*Power(t1,6) - 15*t2 + 270*Power(t2,2) + 
               190*Power(t2,3) - 136*Power(t2,4) - 53*Power(t2,5) + 
               2*Power(t2,6) + Power(t1,5)*(-96 + 74*t2) + 
               Power(t1,4)*(-35 + 195*t2 - 363*Power(t2,2)) + 
               Power(t1,3)*(489 - 996*t2 + 271*Power(t2,2) + 
                  442*Power(t2,3)) + 
               Power(t1,2)*(-642 + 1096*t2 + 863*Power(t2,2) - 
                  725*Power(t2,3) - 204*Power(t2,4)) + 
               t1*(299 - 354*t2 - 1041*Power(t2,2) + 56*Power(t2,3) + 
                  408*Power(t2,4) + 26*Power(t2,5))) + 
            Power(s2,4)*(31 - 2*Power(t1,6) + 249*t2 - 46*Power(t2,2) + 
               5*Power(t2,3) - 17*Power(t2,4) - 135*Power(t2,5) - 
               14*Power(t2,6) + 10*Power(t1,5)*(9 + t2) + 
               Power(t1,4)*(-245 + 175*t2 + 72*Power(t2,2)) - 
               Power(t1,3)*(-184 + 639*t2 + 978*Power(t2,2) + 
                  268*Power(t2,3)) + 
               2*Power(t1,2)*
                (24 + 481*t2 + 942*Power(t2,2) + 375*Power(t2,3) + 
                  138*Power(t2,4)) - 
               t1*(106 + 757*t2 + 932*Power(t2,2) + 501*Power(t2,3) - 
                  98*Power(t2,4) + 74*Power(t2,5))) + 
            Power(s2,3)*(-25 + Power(t1,6) - 195*t2 - 158*Power(t2,2) - 
               436*Power(t2,3) - 260*Power(t2,4) + 78*Power(t2,5) - 
               14*Power(t2,6) - Power(t1,5)*(207 + 76*t2) + 
               11*Power(t1,4)*(64 + 37*t2 + 24*Power(t2,2)) - 
               Power(t1,3)*(855 + 878*t2 - 234*Power(t2,2) + 
                  258*Power(t2,3)) + 
               Power(t1,2)*(396 + 644*t2 - 1514*Power(t2,2) - 
                  584*Power(t2,3) + 9*Power(t2,4)) + 
               t1*(-14 + 98*t2 + 1174*Power(t2,2) + 1285*Power(t2,3) + 
                  72*Power(t2,4) + 74*Power(t2,5))) + 
            Power(s2,5)*(-22 - 50*t2 + 343*Power(t2,2) + 
               249*Power(t2,3) + 134*Power(t2,4) + 158*Power(t2,5) + 
               34*Power(t2,6) + Power(t1,5)*(-11 + 4*t2) + 
               Power(t1,4)*(3 - 160*t2 - 23*Power(t2,2)) + 
               Power(t1,3)*(79 + 398*t2 + 246*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(-145 - 382*t2 - 201*Power(t2,2) + 
                  295*Power(t2,3) + 79*Power(t2,4)) - 
               t1*(-96 - 190*t2 + 365*Power(t2,2) + 540*Power(t2,3) + 
                  528*Power(t2,4) + 98*Power(t2,5))) + 
            Power(s2,2)*(30 + 19*Power(t1,6) + 
               Power(t1,5)*(131 - 108*t2) - 54*t2 - 291*Power(t2,2) + 
               88*Power(t2,3) + 234*Power(t2,4) - 78*Power(t2,5) - 
               19*Power(t2,6) + 
               Power(t1,4)*(-564 - 363*t2 + 60*Power(t2,2)) + 
               Power(t1,3)*(686 + 2062*t2 + 31*Power(t2,2) + 
                  226*Power(t2,3)) - 
               Power(t1,2)*(269 + 2657*t2 + 641*Power(t2,2) - 
                  278*Power(t2,3) + 314*Power(t2,4)) + 
               t1*(-33 + 1120*t2 + 841*Power(t2,2) - 546*Power(t2,3) + 
                  Power(t2,4) + 136*Power(t2,5))) + 
            Power(s1,5)*(6*Power(s2,7) + 2*(-1 + t1)*t1 + 
               s2*(44 + 41*Power(t1,2) - 4*t2 - 2*t1*(40 + 3*t2)) + 
               Power(s2,6)*(3*t1 - 4*(8 + 5*t2)) + 
               Power(s2,4)*(5 + 61*Power(t1,2) + 197*t2 - 
                  5*Power(t2,2) - 23*t1*(13 + 5*t2)) + 
               Power(s2,5)*(75 + 8*Power(t1,2) + 51*t2 + 
                  5*Power(t2,2) - 3*t1*(-5 + 7*t2)) - 
               Power(s2,2)*(53 + 95*Power(t1,2) - 149*t2 + 
                  5*Power(t2,2) + t1*(-52 + 51*t2)) + 
               Power(s2,3)*(-45 - 5*Power(t1,2) - 373*t2 + 
                  17*Power(t2,2) + t1*(311 + 169*t2))) + 
            Power(s1,4)*(-22 + 4*Power(s2,8) + 21*Power(t1,3) + 
               Power(s2,7)*(-43 + 5*t1 - 34*t2) + 3*t2 + 
               2*t1*(33 + t2) - Power(t1,2)*(75 + t2) + 
               Power(s2,6)*(114 - 4*Power(t1,2) + t1*(5 - 18*t2) + 
                  169*t2 + 50*Power(t2,2)) + 
               s2*(-35 - 25*Power(t1,3) - 237*t2 + 16*Power(t2,2) - 
                  Power(t1,2)*(103 + 115*t2) + 
                  t1*(191 + 311*t2 + 6*Power(t2,2))) + 
               Power(s2,5)*(-125 + 4*Power(t1,3) + 
                  Power(t1,2)*(82 - 25*t2) - 240*t2 - 38*Power(t2,2) - 
                  10*Power(t2,3) + t1*(-146 - 201*t2 + 40*Power(t2,2))) \
+ Power(s2,3)*(47 + 121*Power(t1,3) + 229*t2 + 1004*Power(t2,2) - 
                  38*Power(t2,3) - Power(t1,2)*(299 + 48*t2) - 
                  2*t1*(-52 + 642*t2 + 125*Power(t2,2))) + 
               Power(s2,2)*(-133*Power(t1,3) + 
                  Power(t1,2)*(755 + 322*t2) + 
                  t1*(-836 - 290*t2 + 74*Power(t2,2)) + 
                  2*(64 + 239*t2 - 208*Power(t2,2) + 5*Power(t2,3))) + 
               Power(s2,4)*(60*Power(t1,3) - 
                  Power(t1,2)*(416 + 277*t2) + 
                  t1*(611 + 1600*t2 + 274*Power(t2,2)) - 
                  2*(34 + 184*t2 + 338*Power(t2,2) + 5*Power(t2,3)))) - 
            Power(s1,3)*(-41 + Power(s2,9) - 16*Power(t1,4) - 91*t2 + 
               9*Power(t2,2) + Power(s2,8)*(9 - 11*t1 + 19*t2) + 
               Power(t1,3)*(151 + 33*t2) + 
               Power(t1,2)*(-299 - 173*t2 + 3*Power(t2,2)) + 
               t1*(205 + 192*t2 + 4*Power(t2,2)) + 
               Power(s2,6)*(47 + 4*Power(t1,3) + 475*t2 + 
                  299*Power(t2,2) + 59*Power(t2,3) - 
                  2*Power(t1,2)*(38 + 9*t2) + 
                  t1*(15 + 82*t2 - 25*Power(t2,2))) + 
               Power(s2,7)*(11*Power(t1,2) + 5*t1*(9 + 5*t2) - 
                  5*(11 + 39*t2 + 15*Power(t2,2))) + 
               Power(s2,3)*(59 - 117*Power(t1,4) - 463*t2 + 
                  442*Power(t2,2) + 1230*Power(t2,3) - 42*Power(t2,4) + 
                  6*Power(t1,3)*(149 + 70*t2) - 
                  3*Power(t1,2)*(648 + 454*t2 + 71*Power(t2,2)) + 
                  t1*(1115 + 1145*t2 - 1820*Power(t2,2) - 
                     116*Power(t2,3))) + 
               s2*(53 + 109*Power(t1,4) + 30*t2 - 531*Power(t2,2) + 
                  26*Power(t2,3) - 5*Power(t1,3)*(83 + 15*t2) + 
                  Power(t1,2)*(691 - 365*t2 - 114*Power(t2,2)) + 
                  t1*(-475 + 562*t2 + 525*Power(t2,2) + 2*Power(t2,3))) \
+ Power(s2,2)*(89 - 39*Power(t1,4) + 947*t2 + 1034*Power(t2,2) - 
                  593*Power(t2,3) + 10*Power(t2,4) - 
                  2*Power(t1,3)*(197 + 130*t2) + 
                  Power(t1,2)*(1287 + 2218*t2 + 345*Power(t2,2)) + 
                  t1*(-897 - 3242*t2 - 367*Power(t2,2) + 56*Power(t2,3))\
) - Power(s2,5)*(Power(t1,3)*(63 + t2) + 
                  Power(t1,2)*(-234 - 475*t2 + 21*Power(t2,2)) + 
                  t1*(265 + 1080*t2 + 657*Power(t2,2) - 
                     34*Power(t2,3)) + 
                  2*(-51 + 132*t2 + 36*Power(t2,2) - 58*Power(t2,3) + 
                     5*Power(t2,4))) - 
               Power(s2,4)*(9*Power(t1,4) - 3*Power(t1,3)*(5 + 57*t2) + 
                  Power(t1,2)*(24 + 1351*t2 + 432*Power(t2,2)) - 
                  4*t1*(67 + 639*t2 + 729*Power(t2,2) + 
                     78*Power(t2,3)) + 
                  2*(132 + 229*t2 + 613*Power(t2,2) + 515*Power(t2,3) + 
                     15*Power(t2,4)))) + 
            Power(s1,2)*(-13 - 6*Power(t1,5) - 78*t2 - 166*Power(t2,2) + 
               15*Power(t2,3) + Power(s2,9)*(1 - t1 + 3*t2) - 
               Power(t1,4)*(59 + 14*t2) + 
               Power(t1,3)*(179 + 301*t2 + 19*Power(t2,2)) + 
               Power(t1,2)*(-133 - 690*t2 - 182*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(32 + 481*t2 + 272*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,8)*(6 + 7*Power(t1,2) + 31*t2 + 
                  33*Power(t2,2) - t1*(13 + 37*t2)) + 
               Power(s2,7)*(-4 - 10*Power(t1,3) - 189*t2 - 
                  323*Power(t2,2) - 81*Power(t2,3) + 
                  Power(t1,2)*(8 + 29*t2) + 
                  t1*(6 + 163*t2 + 41*Power(t2,2))) + 
               Power(s2,6)*(-91 + 112*t2 + 659*Power(t2,2) + 
                  204*Power(t2,3) + 32*Power(t2,4) + 
                  3*Power(t1,3)*(13 + 7*t2) - 
                  Power(t1,2)*(109 + 350*t2 + 48*Power(t2,2)) + 
                  t1*(161 + 187*t2 + 316*Power(t2,2) + 2*Power(t2,3))) + 
               s2*(72 - 45*Power(t1,5) + 616*t2 + 4*Power(t2,2) - 
                  580*Power(t2,3) + 22*Power(t2,4) + 
                  Power(t1,4)*(376 + 147*t2) - 
                  Power(t1,3)*(1444 + 452*t2 + 55*Power(t2,2)) + 
                  Power(t1,2)*
                   (2058 + 1318*t2 - 690*Power(t2,2) - 47*Power(t2,3)) \
+ t1*(-1017 - 1740*t2 + 1029*Power(t2,2) + 463*Power(t2,3))) + 
               Power(s2,5)*(198 + 560*t2 - 2*Power(t2,2) + 
                  434*Power(t2,3) + 231*Power(t2,4) - 5*Power(t2,5) + 
                  3*Power(t1,4)*(-5 + 2*t2) + 
                  Power(t1,3)*(53 - 184*t2 - 14*Power(t2,2)) + 
                  Power(t1,2)*
                   (16 + 976*t2 + 800*Power(t2,2) + Power(t2,3)) + 
                  t1*(-252 - 1334*t2 - 2284*Power(t2,2) - 
                     869*Power(t2,3) + 12*Power(t2,4))) + 
               Power(s2,2)*(26 + 93*Power(t1,5) + 151*t2 + 
                  1763*Power(t2,2) + 768*Power(t2,3) - 
                  443*Power(t2,4) + 5*Power(t2,5) - 
                  Power(t1,4)*(383 + 165*t2) + 
                  Power(t1,3)*(920 - 543*t2 - 61*Power(t2,2)) + 
                  Power(t1,2)*
                   (-1145 + 2960*t2 + 1756*Power(t2,2) + 
                     104*Power(t2,3)) + 
                  t1*(489 - 2265*t2 - 3953*Power(t2,2) + 
                     64*Power(t2,3) + 24*Power(t2,4))) + 
               Power(s2,3)*(-63 + 9*Power(t1,5) - 502*t2 - 
                  1407*Power(t2,2) + 455*Power(t2,3) + 
                  691*Power(t2,4) - 23*Power(t2,5) - 
                  Power(t1,4)*(325 + 201*t2) + 
                  Power(t1,3)*(1277 + 2072*t2 + 445*Power(t2,2)) - 
                  Power(t1,2)*
                   (1774 + 5839*t2 + 1834*Power(t2,2) + 
                     262*Power(t2,3)) + 
                  t1*(876 + 4491*t2 + 2178*Power(t2,2) - 
                     958*Power(t2,3) + 32*Power(t2,4))) - 
               Power(s2,4)*(132 + 3*Power(t1,5) + 704*t2 + 
                  681*Power(t2,2) + 1503*Power(t2,3) + 749*Power(t2,4) + 
                  25*Power(t2,5) + Power(t1,4)*(-190 + 13*t2) + 
                  Power(t1,3)*(726 + 351*t2 - 146*Power(t2,2)) + 
                  Power(t1,2)*
                   (-952 - 732*t2 + 1098*Power(t2,2) + 277*Power(t2,3)) \
- t1*(-281 + 294*t2 + 3265*Power(t2,2) + 2154*Power(t2,3) + 
                     172*Power(t2,4)))) + 
            s1*(-24 - 8*Power(t1,6) - 94*t2 + 
               Power(s2,9)*(-2 + 2*t1 - 3*t2)*t2 + 132*Power(t2,2) + 
               125*Power(t2,3) - 15*Power(t2,4) + 
               Power(t1,5)*(-52 + 11*t2) + 
               3*Power(t1,4)*(18 + 75*t2 + Power(t2,2)) - 
               Power(t1,3)*(-343 + 700*t2 + 262*Power(t2,2) + 
                  7*Power(t2,3)) + 
               t1*(311 + 45*t2 - 612*Power(t2,2) - 172*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(t1,2)*(-624 + 513*t2 + 739*Power(t2,2) + 
                  91*Power(t2,3) + Power(t2,4)) - 
               Power(s2,8)*(2 + 13*t2 + 35*Power(t2,2) + 
                  25*Power(t2,3) + Power(t1,2)*(2 + 15*t2) - 
                  t1*(4 + 28*t2 + 41*Power(t2,2))) + 
               Power(s2,7)*(-9 + 6*t2 + 206*Power(t2,2) + 
                  233*Power(t2,3) + 43*Power(t2,4) + 
                  Power(t1,3)*(15 + 17*t2) - 
                  Power(t1,2)*(39 + 12*t2 + 32*Power(t2,2)) - 
                  t1*(-33 + 11*t2 + 177*Power(t2,2) + 27*Power(t2,3))) + 
               s2*(4 + 20*Power(t1,6) + Power(t1,5)*(37 - 21*t2) - 
                  387*t2 - 757*Power(t2,2) + 197*Power(t2,3) + 
                  295*Power(t2,4) - 10*Power(t2,5) + 
                  Power(t1,4)*(-480 + 18*t2 - 11*Power(t2,2)) + 
                  Power(t1,3)*
                   (996 + 1521*t2 - 482*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(219 + 2360*t2 + 1194*Power(t2,2) - 
                     1066*Power(t2,3) - 195*Power(t2,4)) + 
                  Power(t1,2)*
                   (-796 - 3491*t2 + 167*Power(t2,2) + 632*Power(t2,3) + 
                     7*Power(t2,4))) + 
               Power(s2,6)*(35 + 258*t2 - 56*Power(t2,2) - 
                  354*Power(t2,3) - 27*Power(t2,4) - 5*Power(t2,5) + 
                  Power(t1,4)*(-17 + 6*t2) - 
                  Power(t1,3)*(9 + 101*t2 + 32*Power(t2,2)) + 
                  Power(t1,2)*
                   (104 + 322*t2 + 503*Power(t2,2) + 53*Power(t2,3)) - 
                  t1*(113 + 485*t2 + 385*Power(t2,2) + 396*Power(t2,3) + 
                     22*Power(t2,4))) + 
               Power(s2,4)*(-83 + 144*t2 + 419*Power(t2,2) + 
                  308*Power(t2,3) + 775*Power(t2,4) + 229*Power(t2,5) + 
                  7*Power(t2,6) + Power(t1,5)*(-1 + 4*t2) - 
                  3*Power(t1,4)*(73 + 104*t2) + 
                  Power(t1,3)*
                   (551 + 1882*t2 + 662*Power(t2,2) - 35*Power(t2,3)) + 
                  Power(t1,2)*
                   (-524 - 3080*t2 - 1566*Power(t2,2) - 
                     113*Power(t2,3) + 61*Power(t2,4)) + 
                  t1*(276 + 1362*t2 + 527*Power(t2,2) - 
                     1418*Power(t2,3) - 465*Power(t2,4) - 37*Power(t2,5))\
) + Power(s2,5)*(1 - 6*Power(t1,5) - 556*t2 - 710*Power(t2,2) - 
                  271*Power(t2,3) - 499*Power(t2,4) - 151*Power(t2,5) + 
                  Power(t2,6) + 
                  Power(t1,4)*(133 + 33*t2 - 4*Power(t2,2)) + 
                  Power(t1,3)*
                   (-256 - 269*t2 + 114*Power(t2,2) + 9*Power(t2,3)) + 
                  Power(t1,2)*
                   (138 + 125*t2 - 1040*Power(t2,2) - 486*Power(t2,3) - 
                     5*Power(t2,4)) + 
                  t1*(-10 + 667*t2 + 1616*Power(t2,2) + 
                     1878*Power(t2,3) + 496*Power(t2,4) - Power(t2,5))) - 
               Power(s2,2)*(6 - 319*t2 + 142*Power(t2,2) + 
                  1178*Power(t2,3) + 81*Power(t2,4) - 158*Power(t2,5) + 
                  Power(t2,6) + 3*Power(t1,5)*(71 + 19*t2) - 
                  5*Power(t1,4)*(230 + 83*t2 + 23*Power(t2,2)) + 
                  Power(t1,3)*
                   (2437 + 1497*t2 + 52*Power(t2,2) + 66*Power(t2,3)) - 
                  Power(t1,2)*
                   (2270 + 2512*t2 - 1960*Power(t2,2) + 
                     21*Power(t2,3) + 14*Power(t2,4)) + 
                  t1*(764 + 1692*t2 - 1901*Power(t2,2) - 
                     1546*Power(t2,3) + 329*Power(t2,4) + 5*Power(t2,5))) \
+ Power(s2,3)*(84 + Power(t1,5)*(139 - 9*t2) + 325*t2 + 
                  1006*Power(t2,2) + 1157*Power(t2,3) - 275*Power(t2,4) - 
                  125*Power(t2,5) + 5*Power(t2,6) + 
                  Power(t1,4)*(-405 + 95*t2 + 77*Power(t2,2)) - 
                  Power(t1,3)*
                   (-605 + 1717*t2 + 908*Power(t2,2) + 146*Power(t2,3)) + 
                  Power(t1,2)*
                   (-467 + 3702*t2 + 4485*Power(t2,2) + 
                     762*Power(t2,3) + 102*Power(t2,4)) - 
                  t1*(-44 + 2396*t2 + 4681*Power(t2,2) + 
                     1209*Power(t2,3) - 37*Power(t2,4) + 29*Power(t2,5))))\
))*T4q(-1 + s2))/
     (Power(-1 + s2,2)*(-1 + t1)*(s - s2 + t1)*Power(-1 + s1 + t1 - t2,2)*
       (-1 + t2)*Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,3)) - 
    (8*(-2 - 10*s2 - 5*Power(s2,2) + 4*t1 + 44*s2*t1 + 24*Power(s2,2)*t1 + 
         9*Power(t1,2) - 82*s2*Power(t1,2) - 46*Power(s2,2)*Power(t1,2) - 
         36*Power(t1,3) + 88*s2*Power(t1,3) + 44*Power(s2,2)*Power(t1,3) + 
         44*Power(t1,4) - 62*s2*Power(t1,4) - 21*Power(s2,2)*Power(t1,4) - 
         24*Power(t1,5) + 28*s2*Power(t1,5) + 4*Power(s2,2)*Power(t1,5) + 
         5*Power(t1,6) - 6*s2*Power(t1,6) + 
         Power(s1,7)*Power(s2,2)*(s2 + t1) + 
         Power(s1,6)*s2*(3*Power(s2,3) + 2*(-1 + t1)*t1 + 
            Power(s2,2)*(t1 - 6*t2) + 
            s2*(-3 + 2*Power(t1,2) + t1*(3 - 5*t2) - t2)) + 
         Power(s,5)*(-1 + s2)*(-2 + 2*s1 + 3*t1 - 3*t2)*Power(t1 - t2,2) + 
         18*t2 - 8*s2*t2 - 43*Power(s2,2)*t2 - 15*Power(s2,3)*t2 - 
         58*t1*t2 + 16*s2*t1*t2 + 153*Power(s2,2)*t1*t2 + 
         57*Power(s2,3)*t1*t2 + 79*Power(t1,2)*t2 - s2*Power(t1,2)*t2 - 
         219*Power(s2,2)*Power(t1,2)*t2 - 81*Power(s2,3)*Power(t1,2)*t2 - 
         69*Power(t1,3)*t2 - 13*s2*Power(t1,3)*t2 + 
         169*Power(s2,2)*Power(t1,3)*t2 + 51*Power(s2,3)*Power(t1,3)*t2 + 
         43*Power(t1,4)*t2 + 5*s2*Power(t1,4)*t2 - 
         78*Power(s2,2)*Power(t1,4)*t2 - 12*Power(s2,3)*Power(t1,4)*t2 - 
         13*Power(t1,5)*t2 + s2*Power(t1,5)*t2 + 
         18*Power(s2,2)*Power(t1,5)*t2 - 14*Power(t2,2) + 
         61*s2*Power(t2,2) - 30*Power(s2,2)*Power(t2,2) - 
         68*Power(s2,3)*Power(t2,2) - 15*Power(s2,4)*Power(t2,2) + 
         26*t1*Power(t2,2) - 146*s2*t1*Power(t2,2) + 
         75*Power(s2,2)*t1*Power(t2,2) + 181*Power(s2,3)*t1*Power(t2,2) + 
         42*Power(s2,4)*t1*Power(t2,2) + 4*Power(t1,2)*Power(t2,2) + 
         138*s2*Power(t1,2)*Power(t2,2) - 
         91*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         176*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         39*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         30*Power(t1,3)*Power(t2,2) - 82*s2*Power(t1,3)*Power(t2,2) + 
         77*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         81*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         12*Power(s2,4)*Power(t1,3)*Power(t2,2) + 
         14*Power(t1,4)*Power(t2,2) + 29*s2*Power(t1,4)*Power(t2,2) - 
         31*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         18*Power(s2,3)*Power(t1,4)*Power(t2,2) - 4*Power(t2,3) - 
         31*s2*Power(t2,3) + 80*Power(s2,2)*Power(t2,3) - 
         45*Power(s2,3)*Power(t2,3) - 46*Power(s2,4)*Power(t2,3) - 
         5*Power(s2,5)*Power(t2,3) - 7*t1*Power(t2,3) + 
         19*s2*t1*Power(t2,3) - 112*Power(s2,2)*t1*Power(t2,3) + 
         87*Power(s2,3)*t1*Power(t2,3) + 80*Power(s2,4)*t1*Power(t2,3) + 
         9*Power(s2,5)*t1*Power(t2,3) + 19*Power(t1,2)*Power(t2,3) + 
         47*s2*Power(t1,2)*Power(t2,3) + 
         42*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         81*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         40*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         4*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         8*Power(t1,3)*Power(t2,3) - 35*s2*Power(t1,3)*Power(t2,3) - 
         10*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         39*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         6*Power(s2,4)*Power(t1,3)*Power(t2,3) + 4*Power(t2,4) - 
         2*s2*Power(t2,4) - 17*Power(s2,2)*Power(t2,4) + 
         41*Power(s2,3)*Power(t2,4) - 27*Power(s2,4)*Power(t2,4) - 
         11*Power(s2,5)*Power(t2,4) - 6*t1*Power(t2,4) - 
         21*s2*t1*Power(t2,4) - 16*Power(s2,2)*t1*Power(t2,4) - 
         7*Power(s2,3)*t1*Power(t2,4) + 28*Power(s2,4)*t1*Power(t2,4) + 
         9*Power(s2,5)*t1*Power(t2,4) + Power(t1,2)*Power(t2,4) + 
         16*s2*Power(t1,2)*Power(t2,4) + 
         29*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         15*Power(s2,3)*Power(t1,2)*Power(t2,4) - 
         14*Power(s2,4)*Power(t1,2)*Power(t2,4) - 2*Power(t2,5) + 
         7*s2*Power(t2,5) + 4*Power(s2,2)*Power(t2,5) - 
         2*Power(s2,3)*Power(t2,5) + 4*Power(s2,4)*Power(t2,5) - 
         5*Power(s2,5)*Power(t2,5) + t1*Power(t2,5) - 
         4*s2*t1*Power(t2,5) - 9*Power(s2,2)*t1*Power(t2,5) - 
         7*Power(s2,3)*t1*Power(t2,5) + 9*Power(s2,4)*t1*Power(t2,5) - 
         s2*Power(t2,6) + 3*Power(s2,2)*Power(t2,6) + 
         Power(s2,3)*Power(t2,6) - Power(s2,4)*Power(t2,6) + 
         Power(s1,5)*(4*Power(s2,5) + t1*(1 - 2*t1 + 2*Power(t1,2)) - 
            Power(s2,3)*(17 + Power(t1,2) + 2*t1*(-10 + t2) + 4*t2 - 
               15*Power(t2,2)) + Power(s2,4)*(2*t1 - 5*(2 + 3*t2)) + 
            s2*(3 + 4*Power(t1,3) + 6*t1*(-1 + t2) + 2*t2 - 
               Power(t1,2)*(2 + 7*t2)) + 
            Power(s2,2)*(Power(t1,3) + Power(t1,2)*(3 - 6*t2) + 
               5*t2*(3 + t2) + t1*(-6 - 21*t2 + 10*Power(t2,2)))) + 
         Power(s1,4)*(-1 + Power(t1,4) + 
            Power(s2,5)*(-10 + 8*t1 - 21*t2) - t1*(-3 + t2) - t2 + 
            Power(t1,2)*t2 - 4*Power(t1,3)*(1 + t2) + 
            Power(s2,4)*(-16 - 5*Power(t1,2) - t1*(-8 + t2) + 46*t2 + 
               29*Power(t2,2)) - 
            Power(s2,3)*(-37 + Power(t1,3) - 77*t2 - 18*Power(t2,2) + 
               20*Power(t2,3) - Power(t1,2)*(17 + 12*t2) + 
               t1*(34 + 106*t2 + 3*Power(t2,2))) + 
            s2*(2*Power(t1,4) + Power(t1,3)*(2 - 6*t2) - 
               4*t2*(3 + 2*t2) + t1*(9 + 38*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*(-20 - 13*t2 + 9*Power(t2,2))) + 
            Power(s2,2)*(33 + Power(t1,3)*(-10 + t2) + 15*t2 - 
               26*Power(t2,2) - 10*Power(t2,3) + 
               Power(t1,2)*(53 - 14*t2 + 6*Power(t2,2)) + 
               t1*(-80 + t2 + 51*Power(t2,2) - 10*Power(t2,3)))) + 
         Power(s1,3)*(3*t2*(1 + t2) + Power(t1,4)*(2 + t2) + 
            t1*(-4 - 17*t2 + 2*Power(t2,2)) + 
            Power(t1,3)*(-15 - 2*t2 + 2*Power(t2,2)) + 
            Power(t1,2)*(17 + 19*t2 + 3*Power(t2,2)) + 
            Power(s2,5)*(5 + 4*Power(t1,2) + 41*t2 + 44*Power(t2,2) - 
               3*t1*(3 + 11*t2)) - 
            Power(s2,4)*(-40 + 4*Power(t1,3) - 73*t2 + 82*Power(t2,2) + 
               26*Power(t2,3) - 3*Power(t1,2)*(10 + 9*t2) + 
               6*t1*(11 + 8*t2 + 3*Power(t2,2))) + 
            s2*(-27 - Power(t1,5) + Power(t1,3)*(75 - 26*t2) - 18*t2 + 
               9*Power(t2,2) + 13*Power(t2,3) + 
               Power(t1,4)*(-17 + 3*t2) + 
               Power(t1,2)*(-130 + 37*t2 + 37*Power(t2,2) - 
                  5*Power(t2,3)) + 
               2*t1*(50 + 16*t2 - 32*Power(t2,2) + Power(t2,3))) + 
            Power(s2,3)*(26 - 160*t2 - 128*Power(t2,2) - 
               31*Power(t2,3) + 15*Power(t2,4) + 
               Power(t1,3)*(-20 + 11*t2) + 
               Power(t1,2)*(24 - 60*t2 - 31*Power(t2,2)) + 
               t1*(-30 + 133*t2 + 207*Power(t2,2) + 11*Power(t2,3))) + 
            Power(s2,2)*(-51 - 121*t2 - 52*Power(t2,2) + 15*Power(t2,3) + 
               10*Power(t2,4) + Power(t1,4)*(-9 + 2*t2) + 
               Power(t1,3)*(68 + 42*t2 - 5*Power(t2,2)) - 
               Power(t1,2)*(102 + 257*t2 - 13*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(94 + 350*t2 + 52*Power(t2,2) - 57*Power(t2,3) + 
                  5*Power(t2,4)))) + 
         Power(s1,2)*(8 + 2*Power(t1,5)*(-4 + t2) + 7*t2 + 
            2*Power(t2,2) - 5*Power(t2,3) + 
            Power(t1,4)*(38 - 11*t2 - 2*Power(t2,2)) + 
            Power(t1,3)*(-76 + 20*t2 + 6*Power(t2,2)) - 
            Power(t1,2)*(-78 - 13*t2 + 25*Power(t2,2) + Power(t2,3)) - 
            t1*(40 + 31*t2 - 13*Power(t2,2) + 4*Power(t2,3)) - 
            Power(s2,5)*t2*(15 + 12*Power(t1,2) + 63*t2 + 
               46*Power(t2,2) - 3*t1*(9 + 17*t2)) + 
            Power(s2,4)*(-15 - 125*t2 - 125*Power(t2,2) + 
               70*Power(t2,3) + 9*Power(t2,4) + 
               Power(t1,3)*(12 + 13*t2) - 
               Power(t1,2)*(39 + 97*t2 + 53*Power(t2,2)) + 
               t1*(42 + 209*t2 + 100*Power(t2,2) + 40*Power(t2,3))) + 
            Power(s2,3)*(-60 - 91*t2 + 249*Power(t2,2) + 
               91*Power(t2,3) + 25*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,4)*(-13 + 3*t2) + 
               Power(t1,3)*(58 + 64*t2 - 18*Power(t2,2)) + 
               Power(t1,2)*(-137 - 102*t2 + 51*Power(t2,2) + 
                  30*Power(t2,3)) + 
               t1*(152 + 126*t2 - 168*Power(t2,2) - 183*Power(t2,3) - 
                  10*Power(t2,4))) + 
            s2*(31 + 71*t2 + 36*Power(t2,2) + 13*Power(t2,3) - 
               11*Power(t2,4) + Power(t1,5)*(-15 + 2*t2) + 
               Power(t1,4)*(112 + 37*t2 - 5*Power(t2,2)) + 
               Power(t1,3)*(-211 - 242*t2 + 23*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-94 - 328*t2 - 125*Power(t2,2) + 34*Power(t2,3)) + 
               Power(t1,2)*(177 + 460*t2 + 29*Power(t2,2) - 
                  27*Power(t2,3) + Power(t2,4))) + 
            Power(s2,2)*(-22 - 3*Power(t1,5) + 200*t2 + 128*Power(t2,2) + 
               63*Power(t2,3) + 6*Power(t2,4) - 5*Power(t2,5) + 
               Power(t1,4)*(-11 + 33*t2 - 2*Power(t2,2)) + 
               Power(t1,3)*(27 - 209*t2 - 62*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(-31 + 345*t2 + 396*Power(t2,2) + 
                  4*Power(t2,3)) - 
               t1*(-40 + 369*t2 + 484*Power(t2,2) + 92*Power(t2,3) - 
                  30*Power(t2,4) + Power(t2,5)))) + 
         s1*(-7 - 7*Power(t1,6) - 12*t2 - 2*Power(t2,2) - 8*Power(t2,3) + 
            5*Power(t2,4) + Power(t1,5)*(59 + 6*t2) + 
            2*Power(t1,4)*(-73 - 31*t2 + Power(t2,2)) + 
            Power(t1,3)*(161 + 166*t2 + 11*Power(t2,2)) - 
            Power(t1,2)*(92 + 182*t2 + 51*Power(t2,2) - 5*Power(t2,3) + 
               Power(t2,4)) + 
            t1*(32 + 84*t2 + 40*Power(t2,2) + 7*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,5)*Power(t2,2)*
             (15 + 12*Power(t1,2) + 43*t2 + 24*Power(t2,2) - 
               t1*(27 + 35*t2)) + 
            Power(s2,4)*t2*(30 + 131*t2 + 95*Power(t2,2) - 
               28*Power(t2,3) + Power(t2,4) - 3*Power(t1,3)*(8 + 5*t2) + 
               Power(t1,2)*(78 + 107*t2 + 45*Power(t2,2)) - 
               t1*(84 + 223*t2 + 88*Power(t2,2) + 32*Power(t2,3))) + 
            s2*(10 - 2*Power(t1,6) - 104*t2 - 15*Power(t2,2) - 
               16*Power(t2,3) - 20*Power(t2,4) + 5*Power(t2,5) + 
               Power(t1,5)*(9 + 22*t2) - 
               Power(t1,4)*(23 + 181*t2 + 26*Power(t2,2)) + 
               Power(t1,3)*(25 + 383*t2 + 216*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-22 + 295*t2 + 218*Power(t2,2) + 105*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,2)*(3 - 415*t2 - 393*Power(t2,2) - 
                  62*Power(t2,3) + 5*Power(t2,4))) + 
            Power(s2,2)*(40 + 47*t2 - 228*Power(t2,2) - 23*Power(t2,3) - 
               30*Power(t2,4) - 10*Power(t2,5) + Power(t2,6) + 
               Power(t1,5)*(-15 + 4*t2) + 
               Power(t1,4)*(63 + 33*t2 - 23*Power(t2,2)) + 
               Power(t1,3)*(-139 - 78*t2 + 147*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(t1,2)*(189 + 88*t2 - 279*Power(t2,2) - 
                  221*Power(t2,3) - 6*Power(t2,4)) + 
               t1*(-138 - 94*t2 + 383*Power(t2,2) + 230*Power(t2,3) + 
                  54*Power(t2,4) - 6*Power(t2,5))) + 
            Power(s2,3)*(15 + 127*t2 + 111*Power(t2,2) - 
               167*Power(t2,3) - 21*Power(t2,4) - 9*Power(t2,5) + 
               Power(t2,6) - 2*Power(t1,4)*(-6 - 15*t2 + Power(t2,2)) + 
               Power(t1,3)*(-51 - 135*t2 - 87*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(81 + 307*t2 + 165*Power(t2,2) + 
                  7*Power(t2,3) - 10*Power(t2,4)) + 
               t1*(-57 - 329*t2 - 187*Power(t2,2) + 76*Power(t2,3) + 
                  69*Power(t2,4) + 3*Power(t2,5)))) + 
         Power(s,4)*(-4 + 17*t1 - 24*Power(t1,2) + 14*Power(t1,3) - 
            2*Power(t1,4) + Power(s1,3)*Power(-1 + s2,2)*(t1 - t2) - 
            13*t2 + 28*t1*t2 - 34*Power(t1,2)*t2 + 2*Power(t1,3)*t2 - 
            4*Power(t2,2) + 26*t1*Power(t2,2) + 
            6*Power(t1,2)*Power(t2,2) - 6*Power(t2,3) - 
            10*t1*Power(t2,3) + 4*Power(t2,4) - 
            Power(s2,3)*(-4 + Power(t1,3) - 11*t2 + Power(t2,2) - 
               2*Power(t1,2)*(3 + t2) + t1*(11 + 5*t2 + Power(t2,2))) - 
            Power(s2,2)*(12 + 31*t2 - 22*Power(t2,2) - 17*Power(t2,3) - 
               2*Power(t2,4) + 2*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(16 - 21*t2 - 6*Power(t2,2)) + 
               t1*(-35 + 6*t2 + 36*Power(t2,2) + 6*Power(t2,3))) + 
            s2*(12 + 3*Power(t1,4) + 33*t2 - 19*Power(t2,2) - 
               12*Power(t2,3) - 5*Power(t2,4) - 
               2*Power(t1,3)*(5 + 2*t2) + 
               Power(t1,2)*(32 + 8*t2 - 6*Power(t2,2)) + 
               t1*(-41 - 13*t2 + 14*Power(t2,2) + 12*Power(t2,3))) + 
            Power(s1,2)*((7 - 11*s2 + 2*Power(s2,2))*Power(t1,2) - 
               t1*(-3 + s2*(2 - 22*t2) + 12*t2 + 
                  Power(s2,2)*(1 + 6*t2)) + 
               t2*(1 + 5*t2 + Power(s2,2)*(5 + 4*t2) - s2*(6 + 11*t2))) + 
            s1*(4 + 9*Power(t1,3) + Power(t1,2)*(14 - 26*t2) + 3*t2 + 
               2*Power(t2,2) - 8*Power(t2,3) + 
               t1*(-11 - 16*t2 + 25*Power(t2,2)) - 
               Power(s2,3)*(4 + Power(t1,2) + t2 - t1*(1 + t2)) + 
               s2*(-12 - 11*Power(t1,3) + t2 + 23*Power(t2,2) + 
                  14*Power(t2,3) + 2*Power(t1,2)*(-5 + 18*t2) + 
                  t1*(15 - 13*t2 - 39*Power(t2,2))) + 
               Power(s2,2)*(12 + Power(t1,3) + Power(t1,2)*(1 - 7*t2) - 
                  3*t2 - 21*Power(t2,2) - 5*Power(t2,3) + 
                  t1*(-5 + 20*t2 + 11*Power(t2,2))))) + 
         Power(s,3)*(9 - 46*t1 + 90*Power(t1,2) - 70*Power(t1,3) + 
            17*Power(t1,4) + 10*t2 - 91*t1*t2 + 84*Power(t1,2)*t2 - 
            19*Power(t1,3)*t2 - Power(t1,4)*t2 + 13*Power(t2,2) - 
            20*t1*Power(t2,2) - 29*Power(t1,2)*Power(t2,2) + 
            Power(t1,3)*Power(t2,2) + 6*Power(t2,3) + 47*t1*Power(t2,3) + 
            3*Power(t1,2)*Power(t2,3) - 16*Power(t2,4) - 
            5*t1*Power(t2,4) + 2*Power(t2,5) - 
            Power(s1,4)*(-1 + s2)*
             (Power(s2,2) - 2*t1 + s2*(-1 + 4*t1 - 3*t2) + t2) + 
            Power(s2,4)*(2*Power(t1,3) + (-7 + t2)*t2 - 
               Power(t1,2)*(4 + 3*t2) + t1*(2 + 4*t2 + Power(t2,2))) - 
            Power(s1,3)*(-1 + 6*Power(t1,2) + t1*(8 - 9*t2) + 
               Power(s2,3)*(-3 + t1 - 8*t2) - 3*t2 + 3*Power(t2,2) + 
               Power(s2,2)*(3 + 8*Power(t1,2) + 23*t2 - 23*t1*t2 + 
                  13*Power(t2,2)) - 
               s2*(-1 + 19*Power(t1,2) + t1*(13 - 38*t2) + 8*t2 + 
                  17*Power(t2,2))) + 
            Power(s2,3)*(7 + Power(t1,3)*(-7 + t2) + 49*t2 - 
               28*Power(t2,2) - 48*Power(t2,3) - 8*Power(t2,4) + 
               Power(t1,2)*(27 - 10*Power(t2,2)) + 
               t1*(-27 - 17*t2 + 55*Power(t2,2) + 17*Power(t2,3))) + 
            Power(s2,2)*(-7 - 94*t2 - 31*Power(t2,2) + 35*Power(t2,3) + 
               15*Power(t2,4) + 2*Power(t2,5) - Power(t1,4)*(3 + 2*t2) + 
               Power(t1,3)*(22 + 36*t2 + 4*Power(t2,2)) - 
               3*Power(t1,2)*(15 + 47*t2 + 16*Power(t2,2)) + 
               t1*(33 + 119*t2 + 84*Power(t2,2) - 4*Power(t2,4))) + 
            s2*(-9 + Power(t1,5) + Power(t1,4)*(-14 + t2) + 46*t2 + 
               59*Power(t2,2) + 13*Power(t2,3) + 7*Power(t2,4) - 
               2*Power(t2,5) + 
               Power(t1,3)*(46 - 16*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*(-58 + 80*t2 + 81*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(34 - 39*t2 - 139*Power(t2,2) - 58*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,2)*(-3 - Power(s2,4)*(-1 + t1) - 8*Power(t1,3) - 
               11*t2 - 20*Power(t2,2) + 5*Power(t2,3) + 
               4*Power(t1,2)*(-9 + 5*t2) + 
               t1*(28 + 58*t2 - 17*Power(t2,2)) + 
               Power(s2,3)*(11 + 4*Power(t1,2) - 34*t2 - 
                  21*Power(t2,2) + 2*t1*(-5 + 6*t2)) + 
               Power(s2,2)*(-34 - 4*Power(t1,3) + 44*t2 + 
                  60*Power(t2,2) + 19*Power(t2,3) + 
                  9*Power(t1,2)*(-2 + 3*t2) + 
                  t1*(76 - 36*t2 - 42*Power(t2,2))) + 
               s2*(25 + 11*Power(t1,3) + Power(t1,2)*(50 - 45*t2) + 
                  13*t2 - 7*Power(t2,2) - 24*Power(t2,3) + 
                  t1*(-105 - 46*t2 + 58*Power(t2,2)))) + 
            s1*(-5 + 6*Power(t1,4) + 
               Power(s2,4)*(1 + 4*t1 + Power(t1,2) - 2*t2) + 
               6*Power(t2,2) + 33*Power(t2,3) - 5*Power(t2,4) - 
               Power(t1,3)*(3 + 7*t2) + 
               Power(t1,2)*(-2 + 77*t2 - 9*Power(t2,2)) + 
               t1*(21 - 16*t2 - 107*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s2,3)*(-28 + 4*Power(t1,3) + t2 + 79*Power(t2,2) + 
                  22*Power(t2,3) + Power(t1,2)*(-39 + 2*t2) + 
                  t1*(30 - 25*t2 - 28*Power(t2,2))) + 
               s2*(-22 - 11*Power(t1,4) - 63*t2 - 29*Power(t2,2) - 
                  7*Power(t2,3) + 13*Power(t2,4) + 
                  10*Power(t1,3)*(1 + 2*t2) + 
                  Power(t1,2)*(-117 - 113*t2 + 6*Power(t2,2)) + 
                  t1*(68 + 181*t2 + 110*Power(t2,2) - 28*Power(t2,3))) + 
               Power(s2,2)*(54 + 52*t2 - 83*Power(t2,2) - 
                  54*Power(t2,3) - 11*Power(t2,4) + 
                  Power(t1,3)*(-3 + 5*t2) + 
                  Power(t1,2)*(142 + 12*t2 - 21*Power(t2,2)) + 
                  t1*(-111 - 98*t2 + 45*Power(t2,2) + 27*Power(t2,3))))) + 
         Power(s,2)*(25*t1 - 93*Power(t1,2) + 125*Power(t1,3) - 
            71*Power(t1,4) + 14*Power(t1,5) + 46*t2 + 44*t1*t2 - 
            146*Power(t1,2)*t2 + 72*Power(t1,3)*t2 - 17*Power(t1,4)*t2 + 
            Power(t1,5)*t2 - 35*Power(t2,2) + 42*t1*Power(t2,2) + 
            29*Power(t1,2)*Power(t2,2) - 11*Power(t1,3)*Power(t2,2) - 
            4*Power(t1,4)*Power(t2,2) - 9*Power(t2,3) - 
            54*t1*Power(t2,3) + 11*Power(t1,2)*Power(t2,3) + 
            6*Power(t1,3)*Power(t2,3) + 24*Power(t2,4) + 
            9*t1*Power(t2,4) - 4*Power(t1,2)*Power(t2,4) - 
            6*Power(t2,5) + t1*Power(t2,5) + 
            Power(s2,5)*t2*(t1 - Power(t1,2) + t2 + Power(t2,2)) + 
            Power(s1,5)*(3*Power(s2,3) + t1 + 
               Power(s2,2)*(-4 + 6*t1 - 3*t2) + s2*(1 - 6*t1 + 2*t2)) + 
            Power(s2,4)*(-2 + 11*t2 + 29*Power(t2,2) + 51*Power(t2,3) + 
               13*Power(t2,4) + Power(t1,3)*(1 + 4*t2) + 
               Power(t1,2)*(-4 + 9*t2 + 6*Power(t2,2)) - 
               t1*(-5 + 24*t2 + 55*Power(t2,2) + 23*Power(t2,3))) + 
            Power(s2,3)*(10 + 52*t2 + 190*Power(t2,2) - 21*Power(t2,3) - 
               45*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,4)*(-4 + 3*t2) - 
               Power(t1,3)*(1 + 56*t2 + 14*Power(t2,2)) + 
               Power(t1,2)*(24 + 183*t2 + 134*Power(t2,2) + 
                  13*Power(t2,3)) + 
               t1*(-29 - 182*t2 - 218*Power(t2,2) - 29*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s2,2)*(12 - 3*Power(t1,5) + 49*t2 - 265*Power(t2,2) - 
               65*Power(t2,3) - 16*Power(t2,4) - 7*Power(t2,5) - 
               2*Power(t1,4)*(-25 - 9*t2 + Power(t2,2)) + 
               2*Power(t1,3)*
                (-79 - 46*t2 + 5*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(190 + 103*t2 - 147*Power(t2,2) - 
                  69*Power(t2,3) - 6*Power(t2,4)) + 
               t1*(-91 - 78*t2 + 227*Power(t2,2) + 205*Power(t2,3) + 
                  51*Power(t2,4) + 2*Power(t2,5))) + 
            s2*(-22 + 2*Power(t1,5)*(-2 + t2) - 173*t2 + 
               19*Power(t2,2) - 35*Power(t2,3) - 18*Power(t2,4) + 
               6*Power(t2,5) - Power(t2,6) - 
               Power(t1,4)*(25 + 40*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(123 + 264*t2 + 94*Power(t2,2) + 
                  10*Power(t2,3)) - 
               Power(t1,2)*(177 + 411*t2 + 298*Power(t2,2) + 
                  46*Power(t2,3) + 8*Power(t2,4)) + 
               t1*(105 + 358*t2 + 255*Power(t2,2) + 77*Power(t2,3) - 
                  10*Power(t2,4) + 4*Power(t2,5))) + 
            Power(s1,4)*(-1 + 3*Power(s2,4) - Power(t1,2) + 
               Power(s2,3)*(-11 + 3*t1 - 22*t2) - t1*(-5 + t2) - 3*t2 + 
               Power(s2,2)*(1 + 12*Power(t1,2) + t1*(6 - 33*t2) + 
                  26*t2 + 14*Power(t2,2)) + 
               s2*(6 - 11*Power(t1,2) + 4*t2 - 8*Power(t2,2) + 
                  t1*(-19 + 26*t2))) + 
            Power(s1,3)*(-2 + 3*Power(t1,3) + Power(t1,2)*(15 - 2*t2) + 
               9*Power(t2,2) + t1*(11 - 34*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(2 - 6*Power(t1,2) + t1*(39 - 29*t2) + 
                  78*t2 + 56*Power(t2,2)) + 
               Power(s2,4)*(4*t1 - 19*(1 + t2)) + 
               s2*(-19 + 3*Power(t1,3) + 11*Power(t1,2)*(-4 + t2) - 
                  51*t2 - 32*Power(t2,2) + 13*Power(t2,3) + 
                  t1*(85 + 116*t2 - 34*Power(t2,2))) + 
               Power(s2,2)*(46 + 6*Power(t1,3) + 
                  Power(t1,2)*(36 - 39*t2) - 2*t2 - 49*Power(t2,2) - 
                  24*Power(t2,3) + t1*(-145 - 30*t2 + 61*Power(t2,2)))) + 
            Power(s1,2)*(-36*Power(t1,3) - 5*Power(t1,4) + 
               Power(s2,5)*(-2 + 3*t1 + t2) + 
               Power(t1,2)*(133 - 27*t2 + 9*Power(t2,2)) + 
               3*(5 + 9*t2 + 9*Power(t2,2) - 5*Power(t2,3)) - 
               2*t1*(43 + 50*t2 - 28*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,4)*(19 - 7*Power(t1,2) + 82*t2 + 
                  42*Power(t2,2) - 8*t1*(4 + 3*t2)) - 
               Power(s2,3)*(-55 + 6*Power(t1,3) + 30*t2 + 
                  172*Power(t2,2) + 64*Power(t2,3) - 
                  Power(t1,2)*(105 + 2*t2) + 
                  t1*(62 + 79*t2 - 57*Power(t2,2))) + 
               Power(s2,2)*(-83 + Power(t1,3)*(2 - 3*t2) - 160*t2 - 
                  20*Power(t2,2) + 29*Power(t2,3) + 18*Power(t2,4) + 
                  6*Power(t1,2)*(-25 - 23*t2 + 5*Power(t2,2)) + 
                  t1*(54 + 492*t2 + 104*Power(t2,2) - 45*Power(t2,3))) + 
               s2*(-16 + 15*Power(t1,4) + Power(t1,3)*(46 - 34*t2) + 
                  42*t2 + 76*Power(t2,2) + 54*Power(t2,3) - 
                  11*Power(t2,4) + 
                  Power(t1,2)*(-143 + 140*t2 + 12*Power(t2,2)) + 
                  t1*(161 - 192*t2 - 204*Power(t2,2) + 18*Power(t2,3)))) \
+ s1*(-19 - 10*Power(t1,4) - 23*t2 - 14*Power(t2,2) - 50*Power(t2,3) + 
               15*Power(t2,4) + Power(t1,3)*(95 + 48*t2 + Power(t2,2)) - 
               Power(t1,2)*(84 + 180*t2 + 17*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(18 + 113*t2 + 149*Power(t2,2) - 36*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,5)*(2 + 3*Power(t1,2) + t2 - 2*Power(t2,2) - 
                  t1*(5 + 3*t2)) + 
               Power(s2,4)*(-8 - 8*Power(t1,3) - 44*t2 - 
                  114*Power(t2,2) - 39*Power(t2,3) + 
                  Power(t1,2)*(2 + 5*t2) + 
                  t1*(14 + 79*t2 + 43*Power(t2,2))) + 
               Power(s2,3)*(-31 - 227*t2 + 51*Power(t2,2) + 
                  150*Power(t2,3) + 33*Power(t2,4) + 
                  3*Power(t1,3)*(8 + 3*t2) - 
                  Power(t1,2)*(107 + 199*t2 + 7*Power(t2,2)) + 
                  t1*(114 + 233*t2 + 65*Power(t2,2) - 35*Power(t2,3))) - 
               s2*(-78 + 3*Power(t1,5) + t2 - 7*Power(t2,2) + 
                  13*Power(t2,3) + 33*Power(t2,4) - 5*Power(t2,5) - 
                  Power(t1,4)*(38 + t2) + 
                  Power(t1,3)*(322 + 91*t2 - 9*Power(t2,2)) + 
                  Power(t1,2)*
                   (-442 - 318*t2 + 31*Power(t2,2) + 4*Power(t2,3)) + 
                  t1*(233 + 353*t2 - 28*Power(t2,2) - 117*Power(t2,3) + 
                     8*Power(t2,4))) + 
               Power(s2,2)*(-14 + 337*t2 + 190*Power(t2,2) + 
                  37*Power(t2,3) + 5*Power(t2,4) - 5*Power(t2,5) + 
                  Power(t1,4)*(-3 + 6*t2) + 
                  Power(t1,3)*(103 - 61*t2 - 13*Power(t2,2)) + 
                  Power(t1,2)*
                   (-135 + 372*t2 + 190*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(49 - 300*t2 - 578*Power(t2,2) - 131*Power(t2,3) + 
                     9*Power(t2,4))))) - 
         s*(5 - 9*t1 - 7*Power(t1,2) + 30*Power(t1,3) - 33*Power(t1,4) + 
            19*Power(t1,5) - 5*Power(t1,6) + 
            Power(s1,6)*s2*(3*Power(s2,2) - 2*t1 + s2*(-2 + 4*t1 - t2)) + 
            64*t2 - 83*t1*t2 - 18*Power(t1,2)*t2 + 37*Power(t1,3)*t2 - 
            8*Power(t1,4)*t2 + 8*Power(t1,5)*t2 - 37*Power(t2,2) + 
            74*t1*Power(t2,2) - 2*Power(t1,2)*Power(t2,2) - 
            33*Power(t1,3)*Power(t2,2) - 2*Power(t1,4)*Power(t2,2) - 
            10*Power(t2,3) - 22*t1*Power(t2,3) + 
            30*Power(t1,2)*Power(t2,3) + 2*Power(t1,3)*Power(t2,3) + 
            16*Power(t2,4) - 2*t1*Power(t2,4) - 
            5*Power(t1,2)*Power(t2,4) - 6*Power(t2,5) + 2*t1*Power(t2,5) + 
            Power(s2,6)*Power(t2,2)*(1 - t1 + t2) + 
            Power(s1,5)*(6*Power(s2,4) - 2*(-1 + t1)*t1 + 
               Power(s2,3)*(-8 + 3*t1 - 20*t2) + 
               s2*(4 - 10*t1 + Power(t1,2) + 4*t2 + 6*t1*t2) + 
               Power(s2,2)*(-7 + 8*Power(t1,2) + t1*(8 - 21*t2) + 7*t2 + 
                  5*Power(t2,2))) + 
            Power(s2,5)*t2*(2 + 10*t2 + 16*Power(t2,2) + 7*Power(t2,3) + 
               Power(t1,2)*(2 + 8*t2) - t1*(4 + 18*t2 + 15*Power(t2,2))) - 
            Power(s2,4)*(-1 - 16*t2 - 29*Power(t2,2) + 9*Power(t2,3) + 
               47*Power(t2,4) + 9*Power(t2,5) + 
               Power(t1,3)*(1 + 16*t2 + 11*Power(t2,2)) - 
               Power(t1,2)*(3 + 48*t2 + 40*Power(t2,2) + 
                  14*Power(t2,3)) + 
               t1*(3 + 48*t2 + 58*Power(t2,2) - 14*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s2,2)*(-4 - 99*t2 - 307*Power(t2,2) + 26*Power(t2,3) - 
               48*Power(t2,4) - 9*Power(t2,5) + Power(t2,6) + 
               Power(t1,5)*(-11 + 4*t2) + 
               Power(t1,4)*(27 - 70*t2 - 29*Power(t2,2)) + 
               Power(t1,3)*(-11 + 314*t2 + 270*Power(t2,2) + 
                  50*Power(t2,3)) - 
               Power(t1,2)*(19 + 533*t2 + 628*Power(t2,2) + 
                  209*Power(t2,3) + 28*Power(t2,4)) + 
               t1*(18 + 384*t2 + 694*Power(t2,2) + 234*Power(t2,3) + 
                  29*Power(t2,4) + 2*Power(t2,5))) - 
            s2*(19 + 2*Power(t1,6) + 83*t2 - 121*Power(t2,2) + 
               76*Power(t2,3) + 20*Power(t2,4) - 11*Power(t2,5) + 
               2*Power(t2,6) - Power(t1,5)*(45 + 22*t2) + 
               Power(t1,4)*(188 + 167*t2 + 39*Power(t2,2)) - 
               Power(t1,3)*(333 + 419*t2 + 130*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(t1,2)*(291 + 508*t2 + 251*Power(t2,2) + 
                  Power(t2,3) + 3*Power(t2,4)) - 
               t1*(122 + 317*t2 + 39*Power(t2,2) + 94*Power(t2,3) - 
                  18*Power(t2,4) + 4*Power(t2,5))) - 
            Power(s2,3)*(-7 - 5*t2 + 97*Power(t2,2) + 264*Power(t2,3) + 
               44*Power(t2,4) + 4*Power(t2,5) + Power(t2,6) + 
               2*Power(t1,4)*(-4 - 11*t2 + Power(t2,2)) + 
               Power(t1,3)*(31 + 52*t2 - 11*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,2)*(-45 - 43*t2 + 147*Power(t2,2) + 
                  128*Power(t2,3) + 19*Power(t2,4)) - 
               t1*(-29 - 18*t2 + 235*Power(t2,2) + 295*Power(t2,3) + 
                  99*Power(t2,4) + 10*Power(t2,5))) + 
            Power(s1,4)*(-2 + 4*Power(s2,5) + 3*Power(t1,3) + 
               Power(s2,4)*(-31 + 5*t1 - 34*t2) - 2*t1*(-3 + t2) - 3*t2 + 
               Power(t1,2)*(3 + t2) + 
               Power(s2,3)*(-13 - 4*Power(t1,2) + t1*(50 - 18*t2) + 
                  37*t2 + 50*Power(t2,2)) + 
               s2*(5 + 10*Power(t1,3) - 12*t2 - 16*Power(t2,2) - 
                  2*Power(t1,2)*(4 + 7*t2) + 
                  t1*(-5 + 55*t2 - 6*Power(t2,2))) + 
               Power(s2,2)*(37 + 4*Power(t1,3) + 
                  Power(t1,2)*(22 - 25*t2) + 53*t2 - 4*Power(t2,2) - 
                  10*Power(t2,3) + t1*(-79 - 67*t2 + 40*Power(t2,2)))) - 
            Power(s1,3)*(1 + Power(s2,6) - 5*t2 - 9*Power(t2,2) + 
               9*Power(t1,3)*(1 + t2) + 
               Power(s2,5)*(12 - 11*t1 + 19*t2) + 
               t1*(3 + 42*t2 - 4*Power(t2,2)) - 
               Power(t1,2)*(13 + 7*t2 + 3*Power(t2,2)) + 
               Power(s2,4)*(-16 + 11*Power(t1,2) - 144*t2 - 
                  75*Power(t2,2) + t1*(24 + 25*t2)) + 
               Power(s2,3)*(-100 + 4*Power(t1,3) - 84*t2 + 
                  58*Power(t2,2) + 59*Power(t2,3) - 
                  Power(t1,2)*(89 + 18*t2) + 
                  t1*(100 + 237*t2 - 25*Power(t2,2))) + 
               s2*(50 - 9*Power(t1,4) + 24*Power(t1,3)*(-2 + t2) + 
                  57*t2 + 6*Power(t2,2) - 26*Power(t2,3) + 
                  Power(t1,2)*(206 + 2*t2 - 21*Power(t2,2)) - 
                  t1*(162 + 118*t2 - 87*Power(t2,2) + 2*Power(t2,3))) + 
               Power(s2,2)*(22 - Power(t1,3)*(-13 + t2) + 121*t2 + 
                  115*Power(t2,2) + 11*Power(t2,3) - 10*Power(t2,4) + 
                  Power(t1,2)*(-48 + 119*t2 - 21*Power(t2,2)) + 
                  2*t1*(57 - 167*t2 - 82*Power(t2,2) + 17*Power(t2,3)))) + 
            Power(s1,2)*(21 + 29*Power(t1,4) + 24*t2 + 14*Power(t2,2) - 
               15*Power(t2,3) + Power(s2,6)*(1 - t1 + 3*t2) + 
               Power(t1,3)*(-139 + 23*t2 + Power(t2,2)) - 
               Power(t1,2)*(-175 - 30*t2 + 10*Power(t2,2) + 
                  Power(t2,3)) - 
               t1*(86 + 77*t2 - 52*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,5)*(9 + 7*Power(t1,2) + 40*t2 + 33*Power(t2,2) - 
                  t1*(16 + 37*t2)) + 
               Power(s2,4)*(20 - 10*Power(t1,3) - 48*t2 - 
                  242*Power(t2,2) - 81*Power(t2,3) + 
                  29*Power(t1,2)*(1 + t2) + 
                  t1*(-39 + 76*t2 + 41*Power(t2,2))) + 
               Power(s2,3)*(-63 - 461*t2 - 178*Power(t2,2) + 
                  33*Power(t2,3) + 32*Power(t2,4) + 
                  3*Power(t1,3)*(-1 + 7*t2) - 
                  Power(t1,2)*(91 + 305*t2 + 48*Power(t2,2)) + 
                  t1*(157 + 490*t2 + 433*Power(t2,2) + 2*Power(t2,3))) + 
               s2*(59 - 3*Power(t1,5) + 118*t2 + 88*Power(t2,2) + 
                  43*Power(t2,3) - 22*Power(t2,4) + 
                  Power(t1,4)*(7 + 5*t2) + 
                  Power(t1,3)*(-115 - 153*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*
                   (51 + 590*t2 + 66*Power(t2,2) - 4*Power(t2,3)) + 
                  t1*(1 - 449*t2 - 273*Power(t2,2) + 53*Power(t2,3))) + 
               Power(s2,2)*(-102 + 75*t2 + 85*Power(t2,2) + 
                  90*Power(t2,3) + 17*Power(t2,4) - 5*Power(t2,5) + 
                  3*Power(t1,4)*(-5 + 2*t2) + 
                  Power(t1,3)*(224 + 17*t2 - 14*Power(t2,2)) + 
                  Power(t1,2)*
                   (-373 - 200*t2 + 146*Power(t2,2) + Power(t2,3)) + 
                  t1*(266 + 405*t2 - 406*Power(t2,2) - 157*Power(t2,3) + 
                     12*Power(t2,4)))) + 
            s1*(-30 - 34*t2 + Power(s2,6)*(-2 + 2*t1 - 3*t2)*t2 - 
               10*Power(t2,2) - 33*Power(t2,3) + 15*Power(t2,4) + 
               Power(t1,5)*(16 + 3*t2) - 
               Power(t1,4)*(146 + 25*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(247 + 198*t2 + 2*Power(t2,2) + 
                  5*Power(t2,3)) - 
               Power(t1,2)*(150 + 293*t2 + 85*Power(t2,2) - 
                  5*Power(t2,3) + Power(t2,4)) + 
               t1*(63 + 151*t2 + 100*Power(t2,2) - 14*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s2,5)*(2 + 19*t2 + 44*Power(t2,2) + 25*Power(t2,3) + 
                  Power(t1,2)*(2 + 15*t2) - 
                  t1*(4 + 34*t2 + 41*Power(t2,2))) + 
               Power(s2,4)*(-15 - 45*t2 + 41*Power(t2,2) + 
                  176*Power(t2,3) + 43*Power(t2,4) + 
                  Power(t1,3)*(15 + 17*t2) - 
                  Power(t1,2)*(45 + 57*t2 + 32*Power(t2,2)) + 
                  t1*(45 + 85*t2 - 66*Power(t2,2) - 27*Power(t2,3))) + 
               s2*(28 - 189*t2 + Power(t2,2) - 16*Power(t2,3) - 
                  40*Power(t2,4) + 10*Power(t2,5) + 
                  Power(t1,5)*(-19 + 4*t2) - 
                  3*Power(t1,4)*(-56 + 3*t2 + 4*Power(t2,2)) + 
                  Power(t1,3)*
                   (-385 + 111*t2 + 86*Power(t2,2) + 12*Power(t2,3)) + 
                  t1*(-162 + 25*t2 + 212*Power(t2,2) + 178*Power(t2,3) - 
                     15*Power(t2,4)) + 
                  Power(t1,2)*
                   (370 + 58*t2 - 398*Power(t2,2) - 53*Power(t2,3) - 
                     4*Power(t2,4))) + 
               Power(s2,3)*(-4 + 166*t2 + 628*Power(t2,2) + 
                  151*Power(t2,3) - 5*Power(t2,5) + 
                  Power(t1,4)*(-17 + 6*t2) + 
                  Power(t1,3)*(36 - 26*t2 - 32*Power(t2,2)) + 
                  Power(t1,2)*
                   (-25 + 268*t2 + 353*Power(t2,2) + 53*Power(t2,3)) - 
                  t1*(-10 + 414*t2 + 694*Power(t2,2) + 345*Power(t2,3) + 
                     22*Power(t2,4))) + 
               Power(s2,2)*(62 - 6*Power(t1,5) + 400*t2 - 73*Power(t2,2) + 
                  47*Power(t2,3) - 12*Power(t2,4) - 8*Power(t2,5) + 
                  Power(t2,6) + Power(t1,4)*(58 + 51*t2 - 4*Power(t2,2)) + 
                  Power(t1,3)*
                   (-229 - 506*t2 - 66*Power(t2,2) + 9*Power(t2,3)) + 
                  Power(t1,2)*
                   (370 + 995*t2 + 385*Power(t2,2) - 21*Power(t2,3) - 
                     5*Power(t2,4)) - 
                  t1*(255 + 940*t2 + 545*Power(t2,2) - 122*Power(t2,3) - 
                     50*Power(t2,4) + Power(t2,5))))))*T5q(1 - s1 - t1 + t2))/
     ((-1 + t1)*(s - s2 + t1)*(-1 + s1 + t1 - t2)*(-1 + t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,3)));
   return a;
};
