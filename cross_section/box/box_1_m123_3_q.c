#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m123_3_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(Power(s1,5)*(-6*Power(s2,3) + 6*Power(t1,3) - 
            s2*(18*Power(t1,2) - 16*t1*(-1 + t2) + 5*Power(-1 + t2,2)) + 
            2*Power(s2,2)*(4 + 9*t1 - 4*t2) - 8*Power(t1,2)*(-1 + t2) + 
            4*Power(-1 + t2,2) + t1*Power(-1 + t2,2)) + 
         4*Power(s,5)*s1*(1 - 3*t1 + s1*(-1 + 2*s2 + t1 - 2*t2) + 
            2*s2*(-1 + t1 - t2) + 4*t2) - 
         Power(s,4)*(Power(s1,3)*(-18 + 26*s2 + 5*t1 - 13*t2) + 
            2*(2 + t2)*(-1 + s2 - t1 + t2) + 
            Power(s1,2)*(4 - 41*t1 - 4*Power(t1,2) + 
               s2*(8 + 29*t1 - 73*t2) + 57*t2 + 2*t1*t2 + 18*Power(t2,2)) \
+ s1*(8*Power(s2,2)*(1 + t1 - t2) - 
               s2*(54 + 5*t1 + 8*Power(t1,2) - 47*t2 + 24*t1*t2 - 
                  32*Power(t2,2)) + 
               2*(13 + 4*t1 + 6*Power(t1,2) - 7*t2 + 12*t1*t2 - 
                  26*Power(t2,2)))) + 
         Power(s1,4)*(-2*t1*(-10 + t2)*Power(-1 + t2,2) + 
            Power(s2,3)*(1 + 5*t2) - Power(t1,3)*(1 + 5*t2) - 
            Power(-1 + t2,2)*(-5 + 13*t2) + 
            2*Power(t1,2)*(9 - 16*t2 + 7*Power(t2,2)) + 
            Power(s2,2)*(5 - 6*t2 + Power(t2,2) - 3*t1*(1 + 5*t2)) + 
            s2*(3*Power(t1,2)*(1 + 5*t2) + 
               Power(-1 + t2,2)*(-19 + 13*t2) + 
               t1*(-27 + 46*t2 - 19*Power(t2,2)))) - 
         Power(-1 + t2,2)*(-7 + 9*t2 + Power(t2,2) - 3*Power(t2,3) + 
            Power(t1,2)*(1 + t2) + Power(s2,2)*(2 - t2 + Power(t2,2)) + 
            2*t1*(-3 + t2 + Power(t2,2)) + 
            s2*(5 + t2 - 5*Power(t2,2) + Power(t2,3) - 
               t1*(3 + Power(t2,2)))) - 
         Power(s1,3)*(8*Power(s2,4)*(-1 + t2) - 
            t1*Power(-1 + t2,2)*(7 - 11*t2 + Power(t2,2)) + 
            Power(t1,3)*(-15 + 20*t2 + Power(t2,2)) + 
            Power(s2,3)*(4 - 24*t1*(-1 + t2) - 14*t2 + 4*Power(t2,2)) - 
            Power(-1 + t2,2)*(27 - 5*t2 + 14*Power(t2,2)) + 
            6*Power(t1,2)*(9 - 7*t2 - 3*Power(t2,2) + Power(t2,3)) + 
            Power(s2,2)*(42 + 24*Power(t1,2)*(-1 + t2) - 50*t2 + 
               34*Power(t2,2) - 26*Power(t2,3) + 
               t1*(-11 + 24*t2 + 5*Power(t2,2))) + 
            s2*(-8*Power(t1,3)*(-1 + t2) - 
               2*Power(t1,2)*(-11 + 15*t2 + 5*Power(t2,2)) + 
               Power(-1 + t2,2)*(23 + 7*t2 + 11*Power(t2,2)) + 
               t1*(-95 + 81*t2 + 3*Power(t2,2) + 11*Power(t2,3)))) + 
         s1*(-1 + t2)*(Power(t1,3)*(23 - 27*t2) - 
            8*Power(s2,4)*(-1 + t2) + 
            Power(s2,3)*(-4 + 24*t1*(-1 + t2) + 5*t2 + 3*Power(t2,2)) + 
            Power(-1 + t2,2)*(17 - t2 + 10*Power(t2,2)) + 
            Power(t1,2)*(-42 + 19*t2 + 27*Power(t2,2) - 8*Power(t2,3)) + 
            2*t1*(-6 + 4*t2 + 9*Power(t2,2) - 11*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(s2,2)*(-70 - 24*Power(t1,2)*(-1 + t2) + 95*t2 - 
               33*Power(t2,2) - 4*Power(t2,3) + 8*Power(t2,4) + 
               t1*(35 - 53*t2 + 14*Power(t2,2) - 8*Power(t2,3))) + 
            s2*(8*Power(t1,3)*(-1 + t2) + 
               Power(t1,2)*(-54 + 75*t2 - 17*Power(t2,2) + 
                  8*Power(t2,3)) + 
               2*(8 - 23*t2 + 18*Power(t2,2) + 6*Power(t2,3) - 
                  9*Power(t2,4)) + 
               t1*(103 - 83*t2 - 29*Power(t2,2) + 25*Power(t2,3) - 
                  8*Power(t2,4)))) + 
         Power(s1,2)*(6 - 35*t2 + 8*Power(s2,4)*(-1 + t2)*t2 + 
            42*Power(t2,2) - 8*Power(t2,3) - 5*Power(t2,5) - 
            t1*Power(-1 + t2,2)*(14 - 27*t2 + 17*Power(t2,2)) + 
            Power(t1,3)*(3 - 25*t2 + 28*Power(t2,2)) + 
            Power(s2,3)*(-11 + 6*(5 + 4*t1)*t2 - 
               6*(5 + 4*t1)*Power(t2,2) + 5*Power(t2,3)) + 
            Power(t1,2)*(-29 + 114*t2 - 97*Power(t2,2) + 
               12*Power(t2,3)) + 
            Power(s2,2)*(25 - 20*t2 + 24*Power(t1,2)*(-1 + t2)*t2 - 
               18*Power(t2,2) + 40*Power(t2,3) - 27*Power(t2,4) + 
               t1*(9 - 33*t2 + 32*Power(t2,2) + 10*Power(t2,3))) + 
            s2*(-8*Power(t1,3)*(-1 + t2)*t2 + 
               Power(-1 + t2,2)*
                (-12 + 5*t2 + 44*Power(t2,2) + 3*Power(t2,3)) - 
               Power(t1,2)*(1 - 28*t2 + 30*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(16 - 129*t2 + 144*Power(t2,2) - 53*Power(t2,3) + 
                  22*Power(t2,4)))) + 
         Power(s,3)*(-18 + 2*Power(s2,2) - 17*t1 + Power(t1,2) + 
            Power(s1,4)*(-18 + 25*s2 - 2*t1 - 5*t2) + 21*t2 + 3*t1*t2 + 
            Power(t1,2)*t2 + 2*Power(t2,2) + 4*t1*Power(t2,2) - 
            5*Power(t2,3) - s2*(-16 + t2 + 5*Power(t2,2) + t1*(3 + t2)) + 
            Power(s1,3)*(-33 + 19*Power(s2,2) + 2*Power(t1,2) + 
               s2*(43 - 112*t2) + 81*t2 + 19*Power(t2,2) - t1*(16 + 3*t2)\
) + Power(s1,2)*(13 - 16*Power(s2,3) + Power(t1,3) + 
               Power(s2,2)*(37 + 53*t1 - 50*t2) + 71*t2 - 
               149*Power(t2,2) - 11*Power(t2,3) + 
               Power(t1,2)*(62 + 5*t2) + 
               t1*(-107 + 81*t2 - 19*Power(t2,2)) - 
               s2*(5 + 38*Power(t1,2) + 8*t2 - 159*Power(t2,2) + 
                  t1*(44 + 35*t2))) + 
            s1*(24*Power(s2,3) - Power(t1,3) - Power(t1,2)*(25 + 38*t2) - 
               t1*(-86 + 33*t2 + Power(t2,2)) + 
               8*(9 - 18*t2 + 5*Power(t2,2) + 8*Power(t2,3)) - 
               Power(s2,2)*(58 + 14*t2 - 32*Power(t2,2) + 
                  t1*(21 + 32*t2)) + 
               s2*(-103 + 161*t2 - 98*Power(t2,2) - 48*Power(t2,3) + 
                  Power(t1,2)*(-2 + 32*t2) + 
                  t1*(71 + 20*t2 + 16*Power(t2,2))))) - 
         Power(s,2)*(-31 + 25*s2 + 6*Power(s2,2) + 
            Power(s1,5)*(-4 + 7*s2 - 3*t1) - 28*t1 - 9*s2*t1 + 
            3*Power(t1,2) + 57*t2 - 15*s2*t2 - 5*Power(s2,2)*t2 + 
            24*t1*t2 + 4*s2*t1*t2 + Power(t1,2)*t2 - 19*Power(t2,2) - 
            13*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) + 
            4*t1*Power(t2,2) + s2*t1*Power(t2,2) - 
            2*Power(t1,2)*Power(t2,2) - 11*Power(t2,3) + 
            5*s2*Power(t2,3) - 2*t1*Power(t2,3) + 4*Power(t2,4) + 
            Power(s1,4)*(-35 + 29*Power(s2,2) + 16*Power(t1,2) + 
               s2*(24 - 41*t1 - 44*t2) + t1*(21 - 13*t2) + 39*t2 + 
               4*Power(t2,2)) - 
            Power(s1,3)*(32 + 11*Power(s2,3) + 4*Power(t1,3) - 117*t2 + 
               117*Power(t2,2) + 4*Power(t2,3) - 
               Power(t1,2)*(38 + 7*t2) + 
               Power(s2,2)*(-52 - 30*t1 + 75*t2) + 
               s2*(-55 + 15*Power(t1,2) + t1*(47 - 17*t2) + 84*t2 - 
                  148*Power(t2,2)) + t1*(74 - 4*t2 + 5*Power(t2,2))) + 
            Power(s1,2)*(-8*Power(s2,4) - Power(t1,3)*(27 + 2*t2) + 
               3*Power(s2,3)*(3 + 8*t1 + 9*t2) + 
               Power(t1,2)*(125 - 144*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(17 - 24*Power(t1,2) + t1*(11 - 116*t2) - 
                  138*t2 + 127*Power(t2,2)) + 
               t1*(-83 + 126*t2 - 9*Power(t2,2) + 16*Power(t2,3)) + 
               2*(-6 + 16*t2 - 65*Power(t2,2) + 71*Power(t2,3)) + 
               s2*(28 + 8*Power(t1,3) + 83*t2 - 63*Power(t2,2) - 
                  134*Power(t2,3) + Power(t1,2)*(7 + 91*t2) + 
                  t1*(-119 + 173*t2 - 39*Power(t2,2)))) + 
            s1*(90 + 8*Power(s2,4) + Power(s2,3)*(34 - 24*t1 - 51*t2) - 
               213*t2 + 214*Power(t2,2) - 55*Power(t2,3) - 
               36*Power(t2,4) + Power(t1,3)*(31 + 2*t2) + 
               2*Power(t1,2)*(-55 + 8*t2 + 24*Power(t2,2)) + 
               t1*(87 - 117*t2 + 58*Power(t2,2) - 30*Power(t2,3)) + 
               Power(s2,2)*(24*Power(t1,2) + 
                  t1*(-1 + 20*t2 + 48*Power(t2,2)) + 
                  2*(-96 + 89*t2 + 8*Power(t2,2) - 24*Power(t2,3))) + 
               s2*(-59 - 8*Power(t1,3) + 92*t2 - 168*Power(t2,2) + 
                  105*Power(t2,3) + 32*Power(t2,4) + 
                  Power(t1,2)*(-64 + 29*t2 - 48*Power(t2,2)) + 
                  2*t1*(128 - 61*t2 - 29*Power(t2,2) + 8*Power(t2,3))))) + 
         s*(2*Power(s1,5)*(-4 + 5*Power(s2,2) + 4*t1 - 10*s2*t1 + 
               5*Power(t1,2) + 4*t2 - 4*t1*t2) + 
            Power(s1,4)*(-10 + 11*Power(s2,3) - 11*Power(t1,3) + 
               Power(s2,2)*(6 - 33*t1 - 16*t2) + 
               10*Power(t1,2)*(-2 + t2) + 37*t2 - 28*Power(t2,2) + 
               Power(t2,3) + t1*(9 - 16*t2 + 7*Power(t2,2)) + 
               s2*(12 + 33*Power(t1,2) - 2*t1*(-11 + t2) - 50*t2 + 
                  38*Power(t2,2))) - 
            (-1 + t2)*(-24 + 37*t2 - 5*Power(t2,2) - 9*Power(t2,3) + 
               Power(t2,4) + Power(t1,2)*(3 + 2*t2 - Power(t2,2)) + 
               2*Power(s2,2)*(3 - 2*t2 + Power(t2,2)) + 
               t1*(-21 + 12*t2 + 5*Power(t2,2)) - 
               s2*(-18 + 3*t2 + 14*Power(t2,2) - 3*Power(t2,3) + 
                  t1*(9 - 2*t2 + Power(t2,2)))) + 
            Power(s1,3)*(-44 - 8*Power(s2,4) + 75*t2 - 92*Power(t2,2) + 
               63*Power(t2,3) - 2*Power(t2,4) + 
               Power(t1,3)*(-25 + 3*t2) + 
               Power(s2,3)*(-1 + 24*t1 + 7*t2) - 
               3*Power(t1,2)*(-24 + 5*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(57 + t1 - 24*Power(t1,2) - 97*t2 - 
                  35*t1*t2 + 82*Power(t2,2)) + 
               4*t1*(-18 + 16*t2 + Power(t2,2) + Power(t2,3)) + 
               s2*(73 + 8*Power(t1,3) - 47*t2 + 47*Power(t2,2) - 
                  73*Power(t2,3) + 25*Power(t1,2)*(1 + t2) - 
                  2*t1*(53 - 25*t2 + 14*Power(t2,2)))) + 
            Power(s1,2)*(-35 + 89*t2 - 58*Power(t2,2) + 54*Power(t2,3) - 
               51*Power(t2,4) + Power(t2,5) + 8*Power(s2,4)*(-1 + 2*t2) + 
               Power(t1,3)*(-19 + 55*t2 + Power(t2,2)) + 
               Power(t1,2)*(106 - 235*t2 + 94*Power(t2,2) - 
                  3*Power(t2,3)) + 
               t1*(-13 + 14*t2 + 50*Power(t2,2) - 48*Power(t2,3) - 
                  3*Power(t2,4)) - 
               3*Power(s2,3)*(-8 + 13*t2 + 2*Power(t2,2) + 
                  8*t1*(-1 + 2*t2)) + 
               Power(s2,2)*(-27 - 48*t2 + 141*Power(t2,2) - 
                  104*Power(t2,3) + 24*Power(t1,2)*(-1 + 2*t2) + 
                  t1*(-15 + 21*t2 + 73*Power(t2,2))) + 
               s2*(51 + Power(t1,3)*(8 - 16*t2) - 29*t2 - 
                  166*Power(t2,2) + 101*Power(t2,3) + 43*Power(t2,4) + 
                  Power(t1,2)*(10 - 37*t2 - 68*Power(t2,2)) + 
                  t1*(-98 + 289*t2 - 182*Power(t2,2) + 67*Power(t2,3)))) + 
            s1*(-16*Power(s2,4)*(-1 + t2) - 
               Power(t1,3)*(-55 + 58*t2 + Power(t2,2)) + 
               Power(s2,3)*(6 + 48*t1*(-1 + t2) - 32*t2 + 
                  30*Power(t2,2)) + 
               Power(t1,2)*(-115 + 97*t2 + 44*Power(t2,2) - 
                  30*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (57 - 28*t2 + 51*Power(t2,2) + 8*Power(t2,3)) + 
               3*t1*(3 - 13*t2 + 22*Power(t2,2) - 21*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(s2,2)*(-196 - 48*Power(t1,2)*(-1 + t2) + 331*t2 - 
                  149*Power(t2,2) - 22*Power(t2,3) + 32*Power(t2,4) + 
                  t1*(63 - 66*t2 + 23*Power(t2,2) - 32*Power(t2,3))) + 
               s2*(14 + 16*Power(t1,3)*(-1 + t2) - 77*t2 + 
                  44*Power(t2,2) + 91*Power(t2,3) - 64*Power(t2,4) - 
                  8*Power(t2,5) + 
                  4*Power(t1,2)*
                   (-31 + 39*t2 - 13*Power(t2,2) + 8*Power(t2,3)) + 
                  t1*(275 - 316*t2 - 3*Power(t2,2) + 76*Power(t2,3) - 
                     24*Power(t2,4)))))))/
     ((-1 + s1)*s1*(-1 + s2)*(-s + s2 - t1)*Power(-1 + s + t2,2)*
       (-1 + s - s1 + t2)*(s - s1 + t2)*(-1 + s2 - t1 + t2)) - 
    (8*(32 - 16*s2 - 29*Power(s2,2) + 9*Power(s2,3) + 8*Power(s2,4) + 
         7*t1 + 27*s2*t1 - 2*Power(s2,2)*t1 - 25*Power(s2,3)*t1 + 
         3*Power(t1,2) - 14*s2*Power(t1,2) + 23*Power(s2,2)*Power(t1,2) - 
         2*Power(s2,3)*Power(t1,2) + 7*Power(t1,3) - 3*s2*Power(t1,3) + 
         6*Power(s2,2)*Power(t1,3) - 3*Power(t1,4) - 6*s2*Power(t1,4) + 
         2*Power(t1,5) - 4*Power(s,5)*(-1 + s1)*Power(t1 - t2,2) - 
         114*t2 + 32*s2*t2 + 95*Power(s2,2)*t2 - 12*Power(s2,3)*t2 - 
         18*Power(s2,4)*t2 - 23*t1*t2 - 81*s2*t1*t2 - 
         24*Power(s2,2)*t1*t2 + 63*Power(s2,3)*t1*t2 + 
         3*Power(s2,4)*t1*t2 - 13*Power(t1,2)*t2 + 54*s2*Power(t1,2)*t2 - 
         71*Power(s2,2)*Power(t1,2)*t2 - 8*Power(s2,3)*Power(t1,2)*t2 - 
         18*Power(t1,3)*t2 + 25*s2*Power(t1,3)*t2 + 
         6*Power(s2,2)*Power(t1,3)*t2 + Power(t1,4)*t2 - Power(t1,5)*t2 + 
         146*Power(t2,2) + 3*s2*Power(t2,2) - 
         101*Power(s2,2)*Power(t2,2) + 7*Power(s2,4)*Power(t2,2) - 
         Power(s2,5)*Power(t2,2) + 24*t1*Power(t2,2) + 
         80*s2*t1*Power(t2,2) + 46*Power(s2,2)*t1*Power(t2,2) - 
         32*Power(s2,3)*t1*Power(t2,2) + 2*Power(s2,4)*t1*Power(t2,2) + 
         12*Power(t1,2)*Power(t2,2) - 59*s2*Power(t1,2)*Power(t2,2) + 
         46*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         13*Power(t1,3)*Power(t2,2) - 24*s2*Power(t1,3)*Power(t2,2) - 
         2*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         3*Power(t1,4)*Power(t2,2) + s2*Power(t1,4)*Power(t2,2) - 
         74*Power(t2,3) - 38*s2*Power(t2,3) + 
         27*Power(s2,2)*Power(t2,3) + 3*Power(s2,3)*Power(t2,3) + 
         4*Power(s2,4)*Power(t2,3) - 7*t1*Power(t2,3) - 
         13*s2*t1*Power(t2,3) - 21*Power(s2,2)*t1*Power(t2,3) - 
         10*Power(s2,3)*t1*Power(t2,3) - 3*Power(t1,2)*Power(t2,3) + 
         21*s2*Power(t1,2)*Power(t2,3) + 
         8*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         3*Power(t1,3)*Power(t2,3) - 2*s2*Power(t1,3)*Power(t2,3) + 
         6*Power(t2,4) + 19*s2*Power(t2,4) + 8*Power(s2,2)*Power(t2,4) + 
         Power(s2,3)*Power(t2,4) - t1*Power(t2,4) - 
         13*s2*t1*Power(t2,4) - 2*Power(s2,2)*t1*Power(t2,4) + 
         Power(t1,2)*Power(t2,4) + s2*Power(t1,2)*Power(t2,4) + 
         4*Power(t2,5) + Power(s1,5)*(s2 - t1)*
          (Power(s2,3) + t1*(1 + Power(t1,2) - t1*(-4 + t2) - t2) + 
            Power(s2,2)*(1 - t1 + 2*t2) - 
            s2*(Power(t1,2) - (-1 + t2)*t2 + t1*(5 + t2))) + 
         Power(s1,4)*(Power(s2,5) - 2*Power(s2,4)*t1 + 
            Power(s2,3)*(18 + t1*(-5 + t2) - 4*t2 - 3*Power(t2,2)) + 
            t1*(-Power(-1 + t2,2) + Power(t1,3)*(3 + t2) + 
               t1*(-28 + 34*t2 - 6*Power(t2,2)) - 
               Power(t1,2)*(20 - 10*t2 + Power(t2,2))) + 
            Power(s2,2)*(-19 + 2*Power(t1,3) - Power(t1,2)*(-13 + t2) + 
               14*t2 + 7*Power(t2,2) - 2*Power(t2,3) + 
               t1*(-57 + 20*t2 + 4*Power(t2,2))) + 
            s2*(-Power(t1,4) + Power(t1,2)*(59 - 26*t2) + 
               Power(-1 + t2,2)*t2 - Power(t1,3)*(11 + t2) + 
               t1*(48 - 51*t2 + 2*Power(t2,2) + Power(t2,3)))) + 
         Power(s1,3)*(Power(s2,5)*(6 + t1 - 2*t2) + 
            Power(s2,4)*(30 - 3*Power(t1,2) + 2*t2 - 3*Power(t2,2) + 
               t1*(-34 + 7*t2)) + 
            Power(s2,2)*(-46 + 2*Power(t1,4) + 45*t2 + 15*Power(t2,2) - 
               15*Power(t2,3) + Power(t2,4) - Power(t1,3)*(81 + t2) + 
               t1*(101 - 133*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(113 + 39*t2 - 2*Power(t2,2))) + 
            Power(s2,3)*(-41 + 2*Power(t1,3) + 
               Power(t1,2)*(75 - 7*t2) + 55*t2 + 
               3*t1*(-33 - 5*t2 + 2*Power(t2,2))) + 
            t1*(Power(t1,5) - Power(t1,4)*(9 + 2*t2) + 
               2*Power(-1 + t2,2)*(-21 + 4*t2) + 
               Power(t1,2)*(17 - 19*t2 - 12*Power(t2,2)) + 
               Power(t1,3)*(5 + 15*t2 + Power(t2,2)) + 
               2*t1*(-29 + 42*t2 - 14*Power(t2,2) + Power(t2,3))) + 
            s2*(-3*Power(t1,5) + Power(t1,4)*(43 + 5*t2) - 
               Power(-1 + t2,2)*(-35 + Power(t2,2)) - 
               Power(t1,3)*(49 + 41*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-77 + 97*t2 + 22*Power(t2,2)) + 
               t1*(107 - 139*t2 + 25*Power(t2,2) + 7*Power(t2,3)))) + 
         Power(s1,2)*(8*Power(s2,6) + 2*Power(t1,6) + 
            Power(t1,5)*(9 - 6*t2) - Power(-1 + t2,3)*(-17 + 3*t2) - 
            t1*Power(-1 + t2,2)*(48 - 21*t2 + Power(t2,2)) + 
            Power(t1,4)*(-17 + 16*t2 + 6*Power(t2,2)) + 
            Power(t1,3)*(4 + 15*t2 - 29*Power(t2,2) - 2*Power(t2,3)) + 
            Power(t1,2)*(57 - 113*t2 + 31*Power(t2,2) + 
               25*Power(t2,3)) + 
            Power(s2,5)*(9 + 10*t2 + Power(t2,2) - t1*(43 + t2)) + 
            Power(s2,4)*(-41 + 52*t2 + 4*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(94 + 4*t2) - t1*(26 + 48*t2 + 5*Power(t2,2))\
) + Power(s2,3)*(-79 + 74*t2 + 27*Power(t2,2) - 11*Power(t2,3) + 
               Power(t2,4) - 2*Power(t1,3)*(53 + 3*t2) + 
               3*Power(t1,2)*(5 + 30*t2 + 3*Power(t2,2)) - 
               t1*(-142 + 169*t2 + 24*Power(t2,2) + 5*Power(t2,3))) - 
            s2*(Power(t1,5)*(19 + t2) - 
               2*Power(t1,4)*(-14 + 18*t2 + Power(t2,2)) + 
               Power(-1 + t2,2)*(-34 - 3*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(-94 + 97*t2 + 28*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(t1,2)*(-90 + 58*t2 + 65*Power(t2,2) + 
                  3*Power(t2,3)) + 
               2*t1*(86 - 177*t2 + 72*Power(t2,2) + 19*Power(t2,3))) + 
            Power(s2,2)*(114 - 239*t2 + 113*Power(t2,2) + 
               11*Power(t2,3) + Power(t2,4) + 4*Power(t1,4)*(16 + t2) + 
               Power(t1,3)*(21 - 82*t2 - 7*Power(t2,2)) + 
               2*Power(t1,2)*
                (-89 + 99*t2 + 21*Power(t2,2) + 2*Power(t2,3)) - 
               t1*(-165 + 147*t2 + 63*Power(t2,2) - 10*Power(t2,3) + 
                  Power(t2,4)))) + 
         s1*(Power(t1,6) - 5*Power(t1,5)*t2 - 
            Power(-1 + t2,3)*(-7 + 3*t2) + 
            Power(s2,5)*(-16 - (-18 + t1)*t2 + 2*Power(t2,2)) + 
            Power(t1,4)*(17 - 23*t2 + 8*Power(t2,2)) - 
            2*t1*Power(-1 + t2,2)*(-38 + 28*t2 + 9*Power(t2,2)) - 
            Power(t1,3)*(6 - 9*t2 + 5*Power(t2,2) + 5*Power(t2,3)) + 
            Power(t1,2)*(3 - 7*t2 - 11*Power(t2,2) + 14*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,3)*(64 - 145*t2 + 74*Power(t2,2) + 
               10*Power(t2,3) + 4*Power(t2,4) - 
               2*Power(t1,3)*(2 + 3*t2) + 
               Power(t1,2)*(-108 + 137*t2 + 15*Power(t2,2)) + 
               t1*(27 + 42*t2 - 64*Power(t2,2) - 7*Power(t2,3))) + 
            Power(s2,4)*(Power(t1,2)*(1 + 4*t2) + 
               t1*(68 - 80*t2 - 9*Power(t2,2)) + 
               2*(-9 + t2 + 7*Power(t2,2) + Power(t2,3))) + 
            s2*(-(Power(t1,5)*(4 + t2)) + 
               Power(t1,4)*(-20 + 41*t2 + 3*Power(t2,2)) + 
               Power(-1 + t2,2)*(-105 + 88*t2 + 15*Power(t2,2)) - 
               Power(t1,3)*(43 - 92*t2 + 52*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(-71 + 133*t2 - 18*Power(t2,2) - 45*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*(77 - 153*t2 + 58*Power(t2,2) + 
                  38*Power(t2,3) + Power(t2,4))) + 
            Power(s2,2)*(64 - 108*t2 - Power(t2,2) + 53*Power(t2,3) - 
               8*Power(t2,4) + Power(t1,4)*(6 + 4*t2) + 
               Power(t1,3)*(76 - 111*t2 - 11*Power(t2,2)) + 
               Power(t1,2)*(17 - 113*t2 + 94*Power(t2,2) + 
                  8*Power(t2,3)) - 
               t1*(135 - 289*t2 + 127*Power(t2,2) + 43*Power(t2,3) + 
                  5*Power(t2,4)))) + 
         Power(s,4)*(6 + 8*t1 - 11*Power(t1,2) + 9*Power(t1,3) + 8*t2 + 
            22*t1*t2 - 15*Power(t1,2)*t2 - 11*Power(t2,2) + 
            3*t1*Power(t2,2) + 3*Power(t2,3) + 
            Power(s2,2)*(6 - t1 + t2) + 
            s2*(-12 + t1 - 8*Power(t1,2) - 17*t2 + 24*t1*t2 - 
               16*Power(t2,2)) + 
            Power(s1,3)*(2 + 2*Power(s2,2) + Power(t1,2) + 
               t1*(3 - 2*t2) - 3*t2 + Power(t2,2) + 
               s2*(-4 - 3*t1 + 3*t2)) + 
            Power(s1,2)*(2 + Power(t1,3) + Power(t1,2)*(17 - 3*t2) + 
               Power(s2,2)*(2 + t1 - t2) - 4*t2 + 9*Power(t2,2) - 
               Power(t2,3) - 
               s2*(4 + 2*Power(t1,2) + t1*(13 - 4*t2) + 3*t2 + 
                  2*Power(t2,2)) + t1*(20 - 26*t2 + 3*Power(t2,2))) - 
            s1*(10 + 10*Power(s2,2) + 6*Power(t1,3) + 
               Power(t1,2)*(3 - 6*t2) + t2 - 5*Power(t2,2) + 
               6*Power(t2,3) + t1*(31 + 2*t2 - 6*Power(t2,2)) - 
               s2*(20 + 6*Power(t1,2) + 17*t2 + 14*Power(t2,2) - 
                  5*t1*(-3 + 4*t2)))) - 
         Power(s,3)*(28 + 35*t1 - 2*Power(t1,2) + 22*Power(t1,3) - 
            7*Power(t1,4) - 43*t2 - 66*t1*t2 - 31*Power(t1,2)*t2 + 
            9*Power(t1,3)*t2 + 4*Power(t2,2) - 4*t1*Power(t2,2) + 
            3*Power(t1,2)*Power(t2,2) + 13*Power(t2,3) - 
            5*t1*Power(t2,3) - 
            Power(s2,2)*(-5 + 5*Power(t1,2) + t1*(22 - 29*t2) + 33*t2 + 
               28*Power(t2,2)) + Power(s2,3)*(-t1 + 2*(7 + t2)) + 
            Power(s1,4)*(4 + 5*Power(s2,2) + 7*t1 + 3*Power(t1,2) - 
               6*t2 - 5*t1*t2 + 2*Power(t2,2) + s2*(-9 - 8*t1 + 7*t2)) + 
            s2*(-51 + 13*Power(t1,3) + 63*t2 + 14*Power(t2,2) + 
               10*Power(t2,3) - 2*Power(t1,2)*(7 + 20*t2) + 
               t1*(7 + 64*t2 + 17*Power(t2,2))) + 
            Power(s1,3)*(-1 + 5*Power(s2,3) + 3*Power(t1,3) + 
               Power(t1,2)*(32 - 9*t2) - 12*t2 + 18*Power(t2,2) - 
               3*Power(t2,3) + Power(s2,2)*(-3 - 6*t1 + 4*t2) + 
               t1*(34 - 46*t2 + 9*Power(t2,2)) + 
               s2*(3 - 2*Power(t1,2) - 4*Power(t2,2) + t1*(-28 + 6*t2))) \
+ s1*(-47 + 6*Power(t1,3) + 3*Power(t1,4) + 
               Power(s2,3)*(-37 + t1 - t2) + 97*t2 - 45*Power(t2,2) - 
               10*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,2)*(21 + 2*t2 - 6*Power(t2,2)) + 
               t1*(-59 + 152*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(27 + Power(t1,2) + t1*(83 - 14*t2) + 
                  19*t2 + 17*Power(t2,2)) - 
               s2*(-69 + 5*Power(t1,3) + Power(t1,2)*(52 - 15*t2) + 
                  99*t2 + 42*Power(t2,2) + 19*Power(t2,3) + 
                  t1*(65 + 26*t2 - 9*Power(t2,2)))) + 
            Power(s1,2)*(16 + Power(s2,3)*(18 + 2*t1 - 3*t2) - 34*t2 + 
               21*Power(t2,2) - 6*Power(t2,3) + Power(t2,4) - 
               Power(t1,3)*(25 + t2) + 
               Power(t1,2)*(-46 + 20*t2 + 3*Power(t2,2)) - 
               Power(s2,2)*(34 + 4*Power(t1,2) + t1*(61 - 9*t2) - 
                  16*t2 + 5*Power(t2,2)) - 
               t1*(19 + 43*t2 - 11*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(-12 + 2*Power(t1,3) + Power(t1,2)*(68 - 5*t2) + 
                  23*t2 + 48*Power(t2,2) - Power(t2,3) + 
                  4*t1*(25 - 15*t2 + Power(t2,2))))) + 
         Power(s,2)*(84 + 73*t1 + 21*Power(t1,2) + 24*Power(t1,3) - 
            18*Power(t1,4) + 2*Power(t1,5) - 185*t2 - 161*t1*t2 + 
            Power(t1,2)*t2 + 27*Power(t1,3)*t2 - Power(t1,4)*t2 + 
            128*Power(t2,2) + 88*t1*Power(t2,2) - 
            10*Power(t1,2)*Power(t2,2) - 5*Power(t1,3)*Power(t2,2) - 
            17*Power(t2,3) + 11*t1*Power(t2,3) + 
            5*Power(t1,2)*Power(t2,3) - 10*Power(t2,4) - 
            t1*Power(t2,4) + Power(s2,4)*(8 + t2) - 
            Power(s2,3)*(-35 + 2*Power(t1,2) + t1*(26 - 16*t2) + 
               39*t2 + 25*Power(t2,2)) + 
            Power(s1,5)*(2 + 4*Power(s2,2) + 3*Power(t1,2) + 
               t1*(5 - 4*t2) - 3*t2 + Power(t2,2) + 
               s2*(-6 - 7*t1 + 5*t2)) + 
            Power(s2,2)*(-29 + 6*Power(t1,3) + 
               Power(t1,2)*(10 - 36*t2) + 44*t2 + 8*Power(t2,2) + 
               15*Power(t2,3) + t1*(-39 + 113*t2 + 27*Power(t2,2))) + 
            s2*(-104 - 6*Power(t1,4) + 170*t2 - 50*Power(t2,2) - 
               27*Power(t2,3) + Power(t2,4) + 
               Power(t1,3)*(26 + 20*t2) + 
               Power(t1,2)*(-20 - 101*t2 + 3*Power(t2,2)) + 
               t1*(25 - 68*t2 + 6*Power(t2,2) - 18*Power(t2,3))) + 
            Power(s1,4)*(9*Power(s2,3) + 3*Power(t1,3) + 
               Power(t1,2)*(25 - 9*t2) + 
               Power(s2,2)*(-5 - 14*t1 + 10*t2) + 
               s2*(-21*t1 + 2*Power(t1,2) - (-3 + t2)*t2) + 
               t1*(22 - 32*t2 + 8*Power(t2,2)) - 
               2*(-1 + 6*t2 - 6*Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*(12 + 4*Power(s2,4) - 2*Power(t1,4) + 
               Power(s2,3)*(16 - 3*t1 - 3*t2) - 30*t2 + 
               31*Power(t2,2) - 14*Power(t2,3) + Power(t2,4) + 
               Power(t1,3)*(-43 + 3*t2) + 
               Power(t1,2)*(-27 + 22*t2 + Power(t2,2)) - 
               2*Power(s2,2)*
                (-1 + 4*Power(t1,2) + t1*(44 - 10*t2) - 9*t2 + 
                  7*Power(t2,2)) + 
               t1*(60 - 88*t2 + 23*Power(t2,2) - 3*Power(t2,3)) + 
               s2*(-46 + 9*Power(t1,3) + 21*t2 + 39*Power(t2,2) - 
                  6*Power(t2,3) - 5*Power(t1,2)*(-23 + 4*t2) + 
                  t1*(37 - 56*t2 + 17*Power(t2,2)))) + 
            s1*(-99 - 2*Power(t1,5) + Power(s2,4)*(-43 + t1 - 2*t2) + 
               241*t2 - 185*Power(t2,2) + 36*Power(t2,3) + 
               7*Power(t2,4) + Power(t1,4)*(-9 + 6*t2) - 
               2*Power(t1,3)*(16 - 6*t2 + 5*Power(t2,2)) + 
               2*Power(t1,2)*
                (27 - 50*t2 - 7*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(26 + 77*t2 - 96*Power(t2,2) + 4*Power(t2,3) - 
                  4*Power(t2,4)) - 
               Power(s2,3)*(42 + Power(t1,2) - 23*t2 - 
                  10*Power(t2,2) + t1*(-139 + 3*t2)) - 
               Power(s2,2)*(-214 + 3*Power(t1,3) + 220*t2 + 
                  37*Power(t2,2) + 18*Power(t2,3) - 
                  2*Power(t1,2)*(-79 + 9*t2) + 
                  t1*(-40 + 41*t2 + 9*Power(t2,2))) + 
               s2*(-18 + 5*Power(t1,4) + Power(t1,3)*(71 - 19*t2) - 
                  10*t2 - 25*Power(t2,2) + 36*Power(t2,3) + 
                  10*Power(t2,4) + 
                  Power(t1,2)*(34 + 6*t2 + 9*Power(t2,2)) + 
                  t1*(-298 + 359*t2 + 55*Power(t2,2) - 5*Power(t2,3)))) \
+ Power(s1,2)*(1 - 2*Power(t1,5) + Power(s2,4)*(37 + t1 - 3*t2) - t2 - 
               13*Power(t2,2) + 11*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,4)*(15 + 7*t2) + 
               Power(t1,3)*(34 - 12*t2 - 9*Power(t2,2)) - 
               Power(s2,3)*(38 + Power(t1,2) + t1*(132 - 5*t2) - 
                  49*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-92 + 116*t2 + 5*Power(t2,2) + 
                  5*Power(t2,3)) - 
               t1*(198 - 254*t2 + 53*Power(t2,2) + 10*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(-162 - 3*Power(t1,3) + 110*t2 + 
                  73*Power(t2,2) + 3*Power(t2,3) + 
                  6*Power(t1,2)*(28 + t2) + 
                  t1*(127 - 122*t2 - 6*Power(t2,2))) + 
               s2*(162 + 5*Power(t1,4) - 187*t2 + 51*Power(t2,2) - 
                  21*Power(t2,3) + 3*Power(t2,4) - 
                  Power(t1,3)*(88 + 15*t2) + 
                  Power(t1,2)*(-123 + 85*t2 + 18*Power(t2,2)) - 
                  t1*(-270 + 269*t2 + 48*Power(t2,2) + 11*Power(t2,3))))) \
- s*(93 - 77*s2 - 61*Power(s2,2) + 31*Power(s2,3) + 16*Power(s2,4) + 
            49*t1 + 54*s2*t1 - 23*Power(s2,2)*t1 - 50*Power(s2,3)*t1 + 
            15*Power(t1,2) - 25*s2*Power(t1,2) + 
            38*Power(s2,2)*Power(t1,2) - 4*Power(s2,3)*Power(t1,2) + 
            17*Power(t1,3) + 10*s2*Power(t1,3) + 
            12*Power(s2,2)*Power(t1,3) - 14*Power(t1,4) - 
            12*s2*Power(t1,4) + 4*Power(t1,5) - 259*t2 + 142*s2*t2 + 
            156*Power(s2,2)*t2 - 42*Power(s2,3)*t2 - 19*Power(s2,4)*t2 - 
            122*t1*t2 - 158*s2*t1*t2 + 29*Power(s2,2)*t1*t2 + 
            86*Power(s2,3)*t1*t2 + 3*Power(s2,4)*t1*t2 - 
            10*Power(t1,2)*t2 + 22*s2*Power(t1,2)*t2 - 
            116*Power(s2,2)*Power(t1,2)*t2 - 
            8*Power(s2,3)*Power(t1,2)*t2 - 9*Power(t1,3)*t2 + 
            50*s2*Power(t1,3)*t2 + 6*Power(s2,2)*Power(t1,3)*t2 - 
            Power(t1,4)*t2 - Power(t1,5)*t2 + 247*Power(t2,2) - 
            54*s2*Power(t2,2) - 85*Power(s2,2)*Power(t2,2) + 
            6*Power(s2,3)*Power(t2,2) - 10*Power(s2,4)*Power(t2,2) + 
            102*t1*Power(t2,2) + 107*s2*t1*Power(t2,2) - 
            4*Power(s2,2)*t1*Power(t2,2) + 
            15*Power(s2,3)*t1*Power(t2,2) - 22*Power(t1,2)*Power(t2,2) + 
            10*s2*Power(t1,2)*Power(t2,2) + 
            3*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
            12*Power(t1,3)*Power(t2,2) - 11*s2*Power(t1,3)*Power(t2,2) + 
            3*Power(t1,4)*Power(t2,2) - 85*Power(t2,3) - 
            25*s2*Power(t2,3) - 13*Power(s2,2)*Power(t2,3) + 
            12*Power(s2,3)*Power(t2,3) - 19*t1*Power(t2,3) + 
            2*s2*t1*Power(t2,3) - 23*Power(s2,2)*t1*Power(t2,3) + 
            15*Power(t1,2)*Power(t2,3) + 14*s2*Power(t1,2)*Power(t2,3) - 
            3*Power(t1,3)*Power(t2,3) + 14*s2*Power(t2,4) + 
            2*Power(s2,2)*Power(t2,4) - 10*t1*Power(t2,4) - 
            3*s2*t1*Power(t2,4) + Power(t1,2)*Power(t2,4) + 
            4*Power(t2,5) + Power(s1,6)*(s2 - t1)*(-1 + s2 - t1 + t2) + 
            Power(s1,5)*(4*Power(s2,3) + Power(t1,3) - 
               3*Power(t1,2)*(-2 + t2) + Power(-1 + t2,2) + 
               Power(s2,2)*(-2 - 7*t1 + 5*t2) + 
               2*t1*(2 - 3*t2 + Power(t2,2)) + 
               s2*(-1 + 2*Power(t1,2) + Power(t2,2) - 2*t1*(2 + t2))) + 
            Power(s1,4)*(5*Power(s2,4) - 3*Power(t1,4) + 
               4*Power(t1,3)*(-6 + t2) - 2*Power(-1 + t2,2)*t2 + 
               Power(s2,3)*(5 - 8*t1 + 3*t2) + Power(t1,2)*(1 + 4*t2) + 
               Power(s2,2)*(7 - 2*Power(t1,2) + 4*t1*(-10 + t2) + 
                  4*t2 - 6*Power(t2,2)) - 
               t1*(-24 + 32*t2 - 9*Power(t2,2) + Power(t2,3)) + 
               s2*(-11 + 8*Power(t1,3) + Power(t1,2)*(59 - 11*t2) + t2 + 
                  14*Power(t2,2) - 4*Power(t2,3) + 
                  2*t1*(-3 - 6*t2 + 4*Power(t2,2)))) + 
            Power(s1,3)*(Power(s2,5) - 2*Power(t1,5) + 
               Power(s2,4)*(20 + t1 - 6*t2) + Power(t1,4)*(22 + 6*t2) + 
               Power(-1 + t2,2)*(13 + Power(t2,2)) - 
               Power(t1,3)*(27 + 4*t2 + 6*Power(t2,2)) + 
               4*t1*(-7 + 13*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-138 + 151*t2 - 11*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,3)*(47 - 7*Power(t1,2) + 16*t2 - 
                  12*Power(t2,2) + t1*(-91 + 19*t2)) + 
               Power(s2,2)*(-133 + 5*Power(t1,3) + 116*t2 + 
                  24*Power(t2,2) - 3*Power(t2,3) - 
                  2*Power(t1,2)*(-72 + 7*t2) + 
                  t1*(-125 - 28*t2 + 14*Power(t2,2))) + 
               s2*(2*Power(t1,4) - 5*Power(t1,3)*(19 + t2) + 
                  Power(t1,2)*(105 + 16*t2 + 4*Power(t2,2)) + 
                  t1*(283 - 295*t2 + 7*Power(t2,2) - 3*Power(t2,3)) + 
                  2*(10 - 26*t2 + 27*Power(t2,2) - 12*Power(t2,3) + 
                     Power(t2,4)))) + 
            Power(s1,2)*(Power(t1,6) - Power(s2,5)*(-29 + t2) - 
               3*Power(t1,5)*(3 + t2) + 
               Power(t1,4)*(-38 + 24*t2 + 3*Power(t2,2)) - 
               Power(-1 + t2,2)*(31 - 27*t2 + 6*Power(t2,2)) - 
               Power(t1,3)*(-44 + 15*t2 + 31*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,2)*(35 - 15*t2 - 23*Power(t2,2) + 
                  18*Power(t2,3)) + 
               t1*(-202 + 413*t2 - 215*Power(t2,2) + 6*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,4)*(3 + Power(t1,2) + 40*t2 + Power(t2,2) - 
                  t1*(127 + t2)) + 
               Power(s2,3)*(-196 - 4*Power(t1,3) + 144*t2 + 
                  38*Power(t2,2) + 5*Power(t2,3) + 
                  12*Power(t1,2)*(18 + t2) + 
                  t1*(29 - 138*t2 - 12*Power(t2,2))) + 
               Power(s2,2)*(47 + 6*Power(t1,4) - 81*t2 + 
                  72*Power(t2,2) - 26*Power(t2,3) + 3*Power(t2,4) - 
                  4*Power(t1,3)*(44 + 5*t2) + 
                  3*Power(t1,2)*(-35 + 60*t2 + 8*Power(t2,2)) - 
                  t1*(-454 + 337*t2 + 89*Power(t2,2) + 13*Power(t2,3))) \
+ s2*(188 - 4*Power(t1,5) - 344*t2 + 120*Power(t2,2) + 33*Power(t2,3) + 
                  3*Power(t2,4) + Power(t1,4)*(67 + 13*t2) + 
                  Power(t1,3)*(111 - 106*t2 - 16*Power(t2,2)) + 
                  Power(t1,2)*
                   (-302 + 208*t2 + 82*Power(t2,2) + 9*Power(t2,3)) - 
                  t1*(81 - 82*t2 + 23*Power(t2,2) + 6*Power(t2,3) + 
                     2*Power(t2,4)))) + 
            s1*(Power(t1,6) - Power(s2,5)*(16 + t2) - 
               Power(t1,5)*(1 + 4*t2) + 
               Power(-1 + t2,2)*(-60 + 64*t2 + Power(t2,2)) + 
               Power(s2,4)*(-60 + Power(t1,2) - 2*t1*(-35 + t2) + 
                  39*t2 + 5*Power(t2,2)) + 
               Power(t1,4)*(13 + 17*t2 + 6*Power(t2,2)) - 
               Power(t1,3)*(37 - 76*t2 + 37*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(150 - 252*t2 + 86*Power(t2,2) + 30*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(t1,2)*(46 - 77*t2 + 8*Power(t2,2) + 
                  35*Power(t2,3) + Power(t2,4)) - 
               Power(s2,3)*(-57 + 4*Power(t1,3) + 
                  Power(t1,2)*(113 - 16*t2) + 119*t2 - 24*Power(t2,2) + 
                  3*Power(t2,3) + t1*(-154 + 115*t2 + 21*Power(t2,2))) + 
               Power(s2,2)*(253 + 6*Power(t1,4) + 
                  Power(t1,3)*(79 - 26*t2) - 374*t2 + 67*Power(t2,2) + 
                  56*Power(t2,3) + 11*Power(t2,4) + 
                  Power(t1,2)*(-115 + 130*t2 + 33*Power(t2,2)) - 
                  t1*(161 - 312*t2 + 59*Power(t2,2) + 12*Power(t2,3))) - 
               s2*(192 + 4*Power(t1,5) + Power(t1,4)*(19 - 17*t2) - 
                  388*t2 + 247*Power(t2,2) - 52*Power(t2,3) + 
                  Power(t2,4) + 
                  Power(t1,3)*(-8 + 71*t2 + 23*Power(t2,2)) - 
                  Power(t1,2)*
                   (141 - 269*t2 + 72*Power(t2,2) + 19*Power(t2,3)) + 
                  t1*(318 - 501*t2 + 115*Power(t2,2) + 85*Power(t2,3) + 
                     9*Power(t2,4))))))*
       B1(1 - s2 + t1 - t2,s1,1 - s + s1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(s - s1 + t2)*(-1 + s2 - t1 + t2)*
       Power(-1 + s - s*s1 + s1*s2 - s1*t1 + t2,2)) + 
    (8*(-2*Power(s1,7)*Power(s2 - t1,3)*(-4 + 3*s2 - 3*t1 + 4*t2) + 
         4*Power(s,6)*(-1 + s1)*s1*
          (Power(s1,2)*(-1 + s2 - t1 + t2) - 
            (1 + t2)*(-1 + s2 - t1 + t2) + 
            s1*(-Power(t1,2) + t2 - s2*t2 + t1*(-2 + 2*s2 + t2))) - 
         Power(-1 + t2,3)*(-7 + 9*t2 + Power(t2,2) - 3*Power(t2,3) + 
            Power(t1,2)*(1 + t2) + Power(s2,2)*(2 - t2 + Power(t2,2)) + 
            2*t1*(-3 + t2 + Power(t2,2)) + 
            s2*(5 + t2 - 5*Power(t2,2) + Power(t2,3) - 
               t1*(3 + Power(t2,2)))) + 
         Power(s1,6)*(s2 - t1)*
          (-2*Power(-1 + t2,3) + Power(s2,3)*(11 + t2) - 
            Power(t1,3)*(11 + t2) + 2*t1*Power(-1 + t2,2)*(12 + t2) + 
            Power(t1,2)*(7 - 12*t2 + 5*Power(t2,2)) + 
            Power(s2,2)*(7 - 12*t2 + 5*Power(t2,2) - 3*t1*(11 + t2)) + 
            s2*(3*Power(t1,2)*(11 + t2) - 
               2*Power(-1 + t2,2)*(10 + 3*t2) - 
               2*t1*(7 - 12*t2 + 5*Power(t2,2)))) + 
         Power(s1,5)*(-1 + t2)*
          (-8*Power(s2,5) + 24*Power(t1,4) + 
            Power(s2,4)*(21 + 32*t1 - 13*t2) + 8*t1*Power(-1 + t2,2) - 
            2*Power(-1 + t2,3) + 
            Power(t1,3)*(-55 - 24*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(-55 + 27*t2 + 27*Power(t2,2) + Power(t2,3)) + 
            Power(s2,3)*(58 - 48*Power(t1,2) + 36*t2 - 18*Power(t2,2) + 
               t1*(-87 + 39*t2)) + 
            Power(s2,2)*(-53 + 32*Power(t1,3) + 
               Power(t1,2)*(135 - 39*t2) + 27*t2 + 21*Power(t2,2) + 
               5*Power(t2,3) + t1*(-175 - 88*t2 + 35*Power(t2,2))) - 
            s2*(8*Power(t1,4) + Power(t1,3)*(93 - 13*t2) + 
               4*Power(-1 + t2,2)*(1 + t2) + 
               4*Power(t1,2)*(-43 - 19*t2 + 5*Power(t2,2)) + 
               2*t1*(-55 + 30*t2 + 21*Power(t2,2) + 4*Power(t2,3)))) - 
         s1*Power(-1 + t2,2)*(Power(t1,3)*(t2 - 2*Power(t2,2)) + 
            Power(-1 + t2,2)*(-77 + 20*t2 + 19*Power(t2,2)) + 
            Power(t1,2)*(20 - 20*t2 + 2*Power(t2,2) - 4*Power(t2,3)) + 
            Power(s2,3)*(-6 + 8*t2 - 3*Power(t2,2) + 2*Power(t2,3)) + 
            t1*(-57 + 56*t2 + 25*Power(t2,2) - 30*Power(t2,3) + 
               6*Power(t2,4)) + 
            Power(s2,2)*(7 - 16*t2 + 25*Power(t2,2) - 20*Power(t2,3) + 
               2*Power(t2,4) + 
               t1*(14 - 19*t2 + 6*Power(t2,2) - 4*Power(t2,3))) + 
            s2*(76 - 94*t2 - 15*Power(t2,2) + 48*Power(t2,3) - 
               15*Power(t2,4) + 
               Power(t1,2)*(-8 + 10*t2 - Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-29 + 42*t2 - 33*Power(t2,2) + 26*Power(t2,3) - 
                  2*Power(t2,4)))) + 
         Power(s1,4)*(16*Power(s2,5)*(-1 + t2) + 
            2*Power(-1 + t2,4)*(4 + t2) - 
            t1*Power(-1 + t2,3)*(75 + 17*t2 + 2*Power(t2,2)) - 
            2*Power(t1,2)*Power(-1 + t2,2)*(1 - 5*t2 + 7*Power(t2,2)) + 
            2*Power(t1,4)*(18 - 22*t2 - 3*Power(t2,2) + Power(t2,3)) + 
            Power(s2,4)*(17 - 64*t1*(-1 + t2) - 38*t2 + 7*Power(t2,2) + 
               2*Power(t2,3)) + 
            Power(t1,3)*(-93 + 82*t2 + 9*Power(t2,2) + 4*Power(t2,3) - 
               2*Power(t2,4)) + 
            Power(s2,3)*(110 + 96*Power(t1,2)*(-1 + t2) - 136*t2 + 
               61*Power(t2,2) - 50*Power(t2,3) + 15*Power(t2,4) + 
               t1*(-81 + 140*t2 + 3*Power(t2,2) - 14*Power(t2,3))) + 
            s2*(16*Power(t1,4)*(-1 + t2) + 
               Power(-1 + t2,3)*(71 + 15*t2 + 8*Power(t2,2)) + 
               Power(t1,3)*(-119 + 152*t2 + 29*Power(t2,2) - 
                  14*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (18 - 61*t2 + 52*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(294 - 292*t2 + 31*Power(t2,2) - 
                  50*Power(t2,3) + 17*Power(t2,4))) - 
            Power(s2,2)*(64*Power(t1,3)*(-1 + t2) + 
               Power(-1 + t2,2)*
                (20 - 61*t2 + 46*Power(t2,2) + Power(t2,3)) - 
               3*Power(t1,2)*
                (49 - 70*t2 - 11*Power(t2,2) + 8*Power(t2,3)) + 
               t1*(311 - 346*t2 + 101*Power(t2,2) - 96*Power(t2,3) + 
                  30*Power(t2,4)))) + 
         Power(s1,2)*(-1 + t2)*
          (Power(t1,4)*(-1 + 4*t2 - 2*Power(t2,2)) + 
            7*Power(-1 + t2,3)*(-11 + 2*t2 + Power(t2,2)) + 
            Power(s2,4)*(16 - 17*t2 + 2*Power(t2,2)) + 
            Power(t1,3)*(30 - 40*t2 + 4*Power(t2,2) - 4*Power(t2,3)) - 
            t1*Power(-1 + t2,2)*
             (39 - 27*t2 - 8*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,2)*(-101 + 89*t2 + 53*Power(t2,2) - 
               49*Power(t2,3) + 8*Power(t2,4)) + 
            Power(s2,3)*(5 - 5*t2 + 5*Power(t2,2) + 7*Power(t2,3) - 
               2*Power(t2,4) + 
               t1*(-51 + 57*t2 - 12*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s2,2)*(Power(t1,2)*
                (53 - 59*t2 + 16*Power(t2,2) - 4*Power(t2,3)) + 
               2*t1*(15 - 33*t2 + 21*Power(t2,2) - 23*Power(t2,3) + 
                  5*Power(t2,4)) + 
               2*(-83 + 144*t2 - 80*Power(t2,2) + 17*Power(t2,3) + 
                  5*Power(t2,4) - 3*Power(t2,5))) + 
            s2*(Power(t1,3)*(-17 + 15*t2 - 4*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (64 - 80*t2 + 17*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(-65 + 111*t2 - 51*Power(t2,2) + 
                  43*Power(t2,3) - 8*Power(t2,4)) + 
               t1*(257 - 339*t2 + 53*Power(t2,2) + 49*Power(t2,3) - 
                  26*Power(t2,4) + 6*Power(t2,5)))) + 
         Power(s1,3)*(-8*Power(s2,5)*(-1 + t2) + 
            6*Power(t1,4)*(-3 + 4*t2) + 
            Power(-1 + t2,4)*(17 - 6*t2 + Power(t2,2)) + 
            t1*Power(-1 + t2,3)*(123 + t2 + 4*Power(t2,2)) + 
            Power(s2,4)*(23 + 32*t1*(-1 + t2) - 54*t2 + 
               49*Power(t2,2) - 12*Power(t2,3)) + 
            Power(t1,2)*Power(-1 + t2,2)*
             (-103 - 44*t2 + 9*Power(t2,2) + 4*Power(t2,3)) + 
            Power(t1,3)*(75 - 108*t2 + 15*Power(t2,2) + 
               22*Power(t2,3) - 4*Power(t2,4)) + 
            Power(s2,3)*(-108 - 48*Power(t1,2)*(-1 + t2) + 264*t2 - 
               247*Power(t2,2) + 102*Power(t2,3) - 7*Power(t2,4) - 
               4*Power(t2,5) + 
               3*t1*(-15 + 38*t2 - 37*Power(t2,2) + 4*Power(t2,3) + 
                  2*Power(t2,4))) - 
            s2*(8*Power(t1,4)*(-1 + t2) + 
               Power(-1 + t2,3)*
                (144 - 40*t2 + 21*Power(t2,2) + 3*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-207 - 108*t2 + 25*Power(t2,2) + 22*Power(t2,3)) + 
               Power(t1,3)*(-37 + 42*t2 + 13*Power(t2,2) + 
                  12*Power(t2,3) - 6*Power(t2,4)) + 
               Power(t1,2)*(240 - 410*t2 + 173*Power(t2,2) + 
                  14*Power(t2,3) - 23*Power(t2,4) + 6*Power(t2,5))) + 
            Power(s2,2)*(32*Power(t1,3)*(-1 + t2) + 
               2*Power(-1 + t2,2)*
                (-51 - 34*t2 + 9*Power(t2,2) + 9*Power(t2,3)) + 
               Power(t1,2)*(3 - 42*t2 + 75*Power(t2,2) + 
                  12*Power(t2,3) - 12*Power(t2,4)) + 
               t1*(273 - 566*t2 + 405*Power(t2,2) - 110*Power(t2,3) - 
                  12*Power(t2,4) + 10*Power(t2,5)))) - 
         2*Power(s,5)*(5*Power(s1,5)*(-1 + s2 - t1 + t2) + 
            (2 + t2)*(-1 + s2 - t1 + t2) + 
            Power(s1,4)*(2*Power(s2,2) - 2*Power(t1,2) + 
               s2*(-2 + 3*t1 - 10*t2) + (8 - 5*t2)*t2 + t1*(-1 + 7*t2)) \
+ s1*(3 - t2 + 5*Power(t2,2) - 7*Power(t2,3) + 2*Power(s2,2)*(2 + t2) + 
               Power(t1,2)*(2 + 3*t2) + t1*(5 + 3*t2 + 4*Power(t2,2)) - 
               s2*(7 + 5*Power(t2,2) + t1*(6 + 5*t2))) + 
            Power(s1,3)*(22 + 3*Power(t1,3) + 
               Power(s2,2)*(8 + 8*t1 - 4*t2) - 20*t2 - 8*Power(t2,2) + 
               Power(t1,2)*(5 + 4*t2) + 
               t1*(10 + 13*t2 - 7*Power(t2,2)) - 
               s2*(30 + 11*Power(t1,2) - 18*t2 - 8*Power(t2,2) + 
                  t1*(12 + 7*t2))) - 
            Power(s1,2)*(18 + 3*Power(t1,3) - 3*t2 - 11*Power(t2,2) - 
               7*Power(t2,3) + 2*Power(s2,2)*(7 + 2*t1 + t2) + 
               Power(t1,2)*(1 + 7*t2) + t1*(3 + 30*t2 - 3*Power(t2,2)) + 
               s2*(-32 - 7*Power(t1,2) + t2 + 7*Power(t2,2) - 
                  t1*(7 + 20*t2)))) + 
         Power(s,4)*(-22 + 20*s2 + 2*Power(s2,2) - 21*t1 - 3*s2*t1 + 
            Power(t1,2) + 27*t2 - 3*s2*t2 + 5*t1*t2 - s2*t1*t2 + 
            Power(t1,2)*t2 + 2*Power(t2,2) - 7*s2*Power(t2,2) + 
            6*t1*Power(t2,2) - 7*Power(t2,3) + 
            6*Power(s1,6)*(-1 + s2 - t1 + t2) + 
            Power(s1,5)*(-18 + 10*Power(s2,2) - Power(t1,2) + 
               s2*(12 - 7*t1 - 25*t2) + 37*t2 - 17*Power(t2,2) + 
               t1*(-13 + 22*t2)) + 
            Power(s1,4)*(22 - 4*Power(s2,3) + 8*Power(t1,3) + 
               Power(s2,2)*(28 + 30*t1 - 40*t2) + 17*t2 - 
               50*Power(t2,2) + 9*Power(t2,3) + 
               Power(t1,2)*(13 + 11*t2) + 
               t1*(15 + 3*t2 - 28*Power(t2,2)) + 
               s2*(-58 - 34*Power(t1,2) + 29*t2 + 43*Power(t2,2) + 
                  t1*(-27 + 13*t2))) + 
            2*Power(s1,3)*(77 - Power(t1,4) + Power(t1,3)*(3 - 9*t2) + 
               2*Power(s2,3)*(9 + 2*t1 - t2) - 153*t2 + 
               54*Power(t2,2) + 21*Power(t2,3) + 
               Power(t1,2)*(8 - 39*t2 + Power(t2,2)) + 
               t1*(54 - 28*t2 - 5*Power(t2,2) + 9*Power(t2,3)) - 
               Power(s2,2)*(30 + 9*Power(t1,2) + 9*t2 - 
                  16*Power(t2,2) + 4*t1*(5 + 6*t2)) + 
               s2*(-59 + 6*Power(t1,3) + 109*t2 - 66*Power(t2,2) - 
                  12*Power(t2,3) + Power(t1,2)*(-1 + 35*t2) + 
                  t1*(5 + 56*t2 - 9*Power(t2,2)))) + 
            s1*(128 + 4*Power(s2,3) - 149*t2 - 2*Power(t1,3)*t2 + 
               29*Power(t2,2) - 26*Power(t2,3) + 18*Power(t2,4) + 
               Power(t1,2)*(7 + 6*t2 - 18*Power(t2,2)) + 
               t1*(135 - 20*t2 - 20*Power(t2,2) + 2*Power(t2,3)) - 
               2*Power(s2,2)*(-17 + 6*Power(t2,2) + t1*(5 + t2)) + 
               s2*(-166 + 63*t2 + 6*Power(t2,3) + 
                  Power(t1,2)*(6 + 4*t2) + 
                  t1*(-39 - 8*t2 + 30*Power(t2,2)))) - 
            2*Power(s1,2)*(133 - Power(t1,4) + 
               Power(t1,3)*(11 - 10*t2) - 176*t2 + 8*Power(t2,2) + 
               25*Power(t2,3) + 9*Power(t2,4) + 2*Power(s2,3)*(7 + t2) - 
               2*Power(t1,2)*(-5 + 11*t2 + 4*Power(t2,2)) + 
               t1*(101 + 13*t2 - 54*Power(t2,2) + 10*Power(t2,3)) - 
               Power(s2,2)*(-19 + Power(t1,2) + 29*t2 + 2*Power(t2,2) + 
                  t1*(10 + 21*t2)) + 
               s2*(-164 + 2*Power(t1,3) + 129*t2 - 8*Power(t2,2) - 
                  25*Power(t2,3) + Power(t1,2)*(-15 + 29*t2) + 
                  t1*(-21 + 18*t2 + 34*Power(t2,2))))) + 
         Power(s,3)*(49 - 41*s2 - 8*Power(s2,2) + 45*t1 + 12*s2*t1 - 
            4*Power(t1,2) - 96*t2 + 32*s2*t2 + 7*Power(s2,2)*t2 - 
            44*t1*t2 - 6*s2*t1*t2 - Power(t1,2)*t2 + 38*Power(t2,2) + 
            17*s2*Power(t2,2) - Power(s2,2)*Power(t2,2) - 
            5*t1*Power(t2,2) - 2*s2*t1*Power(t2,2) + 
            3*Power(t1,2)*Power(t2,2) + 18*Power(t2,3) - 
            10*s2*Power(t2,3) + 6*t1*Power(t2,3) - 9*Power(t2,4) + 
            Power(s1,6)*(9*Power(s2,2) + 13*Power(t1,2) + 
               t1*(33 - 25*t2) + s2*(-33 - 22*t1 + 25*t2) + 
               6*(3 - 4*t2 + Power(t2,2))) - 
            Power(s1,5)*(5 + 6*Power(s2,3) + 6*Power(t1,3) + 
               Power(s2,2)*(37 - 2*t1 - 34*t2) + 51*t2 - 
               65*Power(t2,2) + 9*Power(t2,3) + 
               Power(t1,2)*(3 + 2*t2) + 
               t1*(42 + 27*t2 - 33*Power(t2,2)) - 
               s2*(56 + 10*Power(t1,2) + 21*t2 - 41*Power(t2,2) - 
                  4*t1*(-9 + 7*t2))) + 
            Power(s1,4)*(-23 + 4*Power(s2,4) + 2*Power(t1,4) + 78*t2 + 
               11*Power(t2,2) - 70*Power(t2,3) + 4*Power(t2,4) + 
               Power(s2,3)*(14 - 20*t1 + 6*t2) + 
               Power(t1,3)*(-6 + 25*t2) + Power(t1,2)*(-152 + 51*t2) - 
               t1*(63 - 231*t2 + 51*Power(t2,2) + 31*Power(t2,3)) + 
               Power(s2,2)*(-114 + 30*Power(t1,2) + 149*t2 - 
                  100*Power(t2,2) + t1*(-78 + 57*t2)) + 
               s2*(131 - 16*Power(t1,3) + Power(t1,2)*(70 - 88*t2) - 
                  408*t2 + 149*Power(t2,2) + 42*Power(t2,3) + 
                  2*t1*(132 - 94*t2 + 45*Power(t2,2)))) + 
            Power(s1,2)*(628 + 4*Power(s2,4) - 1294*t2 + 
               711*Power(t2,2) + 22*Power(t2,3) - 57*Power(t2,4) - 
               10*Power(t2,5) + Power(t1,4)*(-8 + 6*t2) + 
               2*Power(s2,3)*
                (63 + t1*(-7 + t2) - 34*t2 - 7*Power(t2,2)) + 
               Power(t1,3)*(30 - 53*t2 + 24*Power(t2,2)) + 
               Power(t1,2)*(-123 - 156*t2 + 113*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(417 - 318*t2 - 64*Power(t2,2) + 85*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(s2,2)*(-239 + 96*t2 + 31*Power(t2,2) - 
                  18*Power(t2,3) + 2*Power(t1,2)*(4 + t2) + 
                  t1*(-182 + 7*t2 + 88*Power(t2,2))) + 
               s2*(-509 - 10*Power(t1,3)*(-1 + t2) + 665*t2 - 
                  324*Power(t2,2) + 6*Power(t2,3) + 66*Power(t2,4) + 
                  Power(t1,2)*(26 + 114*t2 - 98*Power(t2,2)) + 
                  t1*(344 + 36*t2 - 50*Power(t2,2) - 38*Power(t2,3)))) + 
            Power(s1,3)*(-274 - 16*Power(s2,4) - 
               6*Power(t1,4)*(-1 + t2) + 660*t2 - 565*Power(t2,2) + 
               142*Power(t2,3) + 37*Power(t2,4) + 
               Power(t1,3)*(14 - 11*t2 - 18*Power(t2,2)) + 
               Power(t1,2)*(236 + 82*t2 - 131*Power(t2,2) + 
                  14*Power(t2,3)) + 
               t1*(-62 - 41*t2 - 86*Power(t2,2) + 59*Power(t2,3) + 
                  10*Power(t2,4)) + 
               2*Power(s2,3)*
                (-50 + 48*t2 - 8*Power(t2,2) + 3*t1*(3 + 5*t2)) + 
               Power(s2,2)*(434 + Power(t1,2)*(18 - 66*t2) - 406*t2 + 
                  69*Power(t2,2) + 48*Power(t2,3) + 
                  t1*(214 - 147*t2 - 38*Power(t2,2))) + 
               2*s2*(-34 + 87*t2 + 118*Power(t2,2) - 103*Power(t2,3) - 
                  8*Power(t2,4) + Power(t1,3)*(-13 + 21*t2) + 
                  Power(t1,2)*(-64 + 31*t2 + 36*Power(t2,2)) + 
                  t1*(-299 + 94*t2 + 61*Power(t2,2) - 29*Power(t2,3)))) \
+ s1*(-385 + 727*t2 + Power(t1,3)*(7 - 6*t2)*t2 - 338*Power(t2,2) + 
               9*Power(t2,3) - 23*Power(t2,4) + 10*Power(t2,5) - 
               2*Power(s2,3)*(9 - 7*t2 + Power(t2,2)) + 
               Power(t1,2)*(25 - 6*t2 + 31*Power(t2,2) - 
                  18*Power(t2,3)) + 
               t1*(-360 + 304*t2 + 61*Power(t2,2) - 55*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,2)*(-45 + 24*t2 + 57*Power(t2,2) - 
                  14*Power(t2,3) + t1*(44 - 29*t2 - 2*Power(t2,2))) + 
               s2*(448 - 461*t2 + 11*Power(t2,2) + 40*Power(t2,3) - 
                  2*Power(t2,4) + 
                  2*Power(t1,2)*(-13 + 4*t2 + 5*Power(t2,2)) + 
                  2*t1*(6 - t2 - 48*Power(t2,2) + 16*Power(t2,3))))) - 
         Power(s,2)*(2*Power(s1,7)*(s2 - t1)*(-7 + 6*s2 - 6*t1 + 7*t2) + 
            Power(s1,6)*(20*Power(s2,3) - 20*Power(t1,3) + 
               24*Power(-1 + t2,2) + Power(t1,2)*(-57 + 17*t2) + 
               Power(s2,2)*(-69 - 60*t1 + 29*t2) + 
               s2*(13 + 60*Power(t1,2) + t1*(126 - 46*t2) + 8*t2 - 
                  21*Power(t2,2)) + t1*(-13 - 8*t2 + 21*Power(t2,2))) + 
            (-1 + t2)*(-55 + 94*t2 - 24*Power(t2,2) - 20*Power(t2,3) + 
               5*Power(t2,4) + Power(t1,2)*(6 + 3*t2 - 3*Power(t2,2)) + 
               3*Power(s2,2)*(4 - 3*t2 + Power(t2,2)) + 
               t1*(-49 + 36*t2 + 9*Power(t2,2) - 2*Power(t2,3)) + 
               s2*(43 + 6*t1*(-3 + t2) - 18*t2 - 27*Power(t2,2) + 
                  8*Power(t2,3))) + 
            Power(s1,5)*(-14*Power(s2,4) + Power(t1,4) + 
               Power(s2,3)*(8 + 41*t1 - 8*t2) + 
               11*Power(t1,3)*(-3 + t2) + 
               Power(t1,2)*(68 + 40*t2 - 12*Power(t2,2)) + 
               Power(-1 + t2,2)*(-39 - 43*t2 + 2*Power(t2,2)) - 
               t1*(17 - 64*t2 + 21*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s2,2)*(47 - 39*Power(t1,2) + 124*t2 - 
                  75*Power(t2,2) + t1*(-61 + 39*t2)) + 
               s2*(42 + 11*Power(t1,3) + Power(t1,2)*(86 - 42*t2) - 
                  111*t2 + 40*Power(t2,2) + 29*Power(t2,3) + 
                  t1*(-115 - 164*t2 + 87*Power(t2,2)))) - 
            Power(s1,4)*(2*Power(s2,4)*(-26 + 5*t2) + 
               Power(t1,4)*(-5 + 6*t2) + 
               Power(t1,3)*(-165 - 8*t2 + 24*Power(t2,2)) + 
               Power(s2,3)*(146 + t1*(179 - 54*t2) - 106*t2 + 
                  39*Power(t2,2)) + 
               Power(t1,2)*(213 - 53*t2 + 43*Power(t2,2) - 
                  17*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (3 - 101*t2 - 35*Power(t2,2) + Power(t2,3)) + 
               t1*(152 - 367*t2 + 326*Power(t2,2) - 97*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s2,2)*(130 + 41*t2 + 112*Power(t2,2) - 
                  97*Power(t2,3) + 3*Power(t1,2)*(-69 + 28*t2) + 
                  t1*(-409 + 108*t2 - 6*Power(t2,2))) + 
               s2*(-187 + Power(t1,3)*(85 - 46*t2) + 557*t2 - 
                  600*Power(t2,2) + 215*Power(t2,3) + 15*Power(t2,4) + 
                  Power(t1,2)*(428 + 6*t2 - 57*Power(t2,2)) + 
                  t1*(-361 + 54*t2 - 185*Power(t2,2) + 120*Power(t2,3)))\
) + Power(s1,2)*(Power(s2,4)*(24 - 10*t2) + 
               Power(t1,4)*(-11 + 18*t2 - 6*Power(t2,2)) + 
               Power(t1,3)*(63 - 48*t2 + 44*Power(t2,2) - 
                  12*Power(t2,3)) + 
               2*Power(-1 + t2,2)*
                (344 - 257*t2 + 5*Power(t2,2) + 18*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*(-354 + 119*t2 + 279*Power(t2,2) - 
                  110*Power(t2,3) + 4*Power(t2,4)) + 
               2*t1*(136 - 169*t2 + 23*Power(t2,2) + 16*Power(t2,3) - 
                  12*Power(t2,4) + 6*Power(t2,5)) + 
               Power(s2,3)*(158 - 240*t2 + 43*Power(t2,2) + 
                  18*Power(t2,3) + t1*(-79 + 42*t2 - 6*Power(t2,2))) + 
               Power(s2,2)*(-621 + 867*t2 - 330*Power(t2,2) - 
                  16*Power(t2,3) + 38*Power(t2,4) + 
                  Power(t1,2)*(75 - 36*t2 + 6*Power(t2,2)) + 
                  t1*(-197 + 276*t2 + 102*Power(t2,2) - 92*Power(t2,3))\
) + s2*(-245 + 314*t2 - 152*Power(t2,2) + 118*Power(t2,3) + 
                  3*Power(t2,4) - 38*Power(t2,5) + 
                  Power(t1,3)*(-9 - 14*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*
                   (-24 + 12*t2 - 189*Power(t2,2) + 86*Power(t2,3)) + 
                  t1*(925 - 876*t2 + 9*Power(t2,2) + 80*Power(t2,3) - 
                     14*Power(t2,4)))) + 
            s1*(Power(t1,3)*t2*(9 - 16*t2 + 6*Power(t2,2)) + 
               Power(s2,3)*(-30 + 48*t2 - 23*Power(t2,2) + 
                  6*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (507 - 329*t2 - 66*Power(t2,2) - 4*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,2)*(75 - 82*t2 + 31*Power(t2,2) - 
                  32*Power(t2,3) + 6*Power(t2,4)) - 
               2*t1*(216 - 335*t2 + 58*Power(t2,2) + 86*Power(t2,3) - 
                  30*Power(t2,4) + 5*Power(t2,5)) + 
               Power(s2,2)*(-13 - 2*t2 + 109*Power(t2,2) - 
                  106*Power(t2,3) + 10*Power(t2,4) + 
                  t1*(72 - 111*t2 + 42*Power(t2,2) - 6*Power(t2,3))) + 
               s2*(550 - 955*t2 + 301*Power(t2,2) + 177*Power(t2,3) - 
                  75*Power(t2,4) + 2*Power(t2,5) - 
                  3*Power(t1,2)*
                   (14 - 18*t2 + Power(t2,2) + 2*Power(t2,3)) - 
                  2*t1*(37 - 60*t2 + 88*Power(t2,2) - 75*Power(t2,3) + 
                     8*Power(t2,4)))) + 
            Power(s1,3)*(Power(s2,4)*(-86 + 44*t2) + 
               Power(t1,4)*(5 - 12*t2 + 6*Power(t2,2)) + 
               Power(t1,3)*(-127 - 76*t2 + 44*Power(t2,2) + 
                  6*Power(t2,3)) - 
               2*Power(-1 + t2,2)*
                (121 - 87*t2 + 57*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(467 - 270*t2 - 69*Power(t2,2) + 
                  64*Power(t2,3) - 10*Power(t2,4)) + 
               t1*(199 - 544*t2 + 369*Power(t2,2) + 42*Power(t2,3) - 
                  64*Power(t2,4) - 2*Power(t2,5)) + 
               Power(s2,3)*(86 + 46*t2 - 77*Power(t2,2) + 
                  24*Power(t2,3) + t1*(217 - 48*t2 - 42*Power(t2,2))) + 
               Power(s2,2)*(642 - 918*t2 + 636*Power(t2,2) - 
                  146*Power(t2,3) - 32*Power(t2,4) + 
                  3*Power(t1,2)*(-57 - 16*t2 + 30*Power(t2,2)) - 
                  t1*(243 + 240*t2 - 186*Power(t2,2) + 14*Power(t2,3))) \
+ s2*(-394 + 1114*t2 - 841*Power(t2,2) - 26*Power(t2,3) + 
                  143*Power(t2,4) + 4*Power(t2,5) + 
                  Power(t1,3)*(35 + 64*t2 - 54*Power(t2,2)) + 
                  Power(t1,2)*
                   (284 + 270*t2 - 153*Power(t2,2) - 16*Power(t2,3)) + 
                  t1*(-1057 + 1028*t2 - 399*Power(t2,2) + 
                     18*Power(t2,3) + 46*Power(t2,4))))) + 
         s*(2*Power(s1,7)*(s2 - t1)*
             (8*Power(s2,2) + 8*Power(t1,2) - 9*t1*(-1 + t2) - 
               Power(-1 + t2,2) + s2*(-9 - 16*t1 + 9*t2)) + 
            Power(s1,6)*(s2 - t1)*
             (7*Power(s2,3) - 7*Power(t1,3) + 
               4*Power(-1 + t2,2)*(11 + 2*t2) + 
               Power(t1,2)*(-41 + 3*t2) + 
               Power(s2,2)*(-41 - 21*t1 + 3*t2) + 
               s2*(-16 + 21*Power(t1,2) + t1*(82 - 6*t2) + 42*t2 - 
                  26*Power(t2,2)) + 2*t1*(2 - 9*t2 + 7*Power(t2,2))) - 
            Power(-1 + t2,2)*(-31 + 46*t2 - 4*Power(t2,2) - 
               12*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(4 + 3*t2 - Power(t2,2)) + 
               Power(s2,2)*(8 - 5*t2 + 3*Power(t2,2)) + 
               t1*(-27 + 14*t2 + 7*Power(t2,2)) + 
               s2*(23 - 2*t2 - 19*Power(t2,2) + 4*Power(t2,3) - 
                  2*t1*(6 - t2 + Power(t2,2)))) + 
            Power(s1,5)*(-8*Power(s2,5) - Power(t1,4)*(-19 + t2) + 
               4*Power(-1 + t2,3)*(3 + t2) + 
               Power(s2,4)*(1 + 32*t1 + t2) - 
               2*Power(t1,3)*(44 - 13*t2 + Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(66 + 65*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(35 - 8*t2 - 41*Power(t2,2) + 
                  14*Power(t2,3)) - 
               2*Power(s2,3)*
                (-39 + 24*Power(t1,2) + 5*t2 + 2*Power(t2,2) + 
                  t1*(11 + t2)) + 
               Power(s2,2)*(39 + 60*Power(t1,2) + 32*Power(t1,3) + 
                  26*t2 - 121*Power(t2,2) + 56*Power(t2,3) + 
                  t1*(-256 + 70*t2 - 6*Power(t2,2))) - 
               s2*(8*Power(t1,4) - 2*Power(t1,3)*(-29 + t2) + 
                  Power(-1 + t2,2)*(50 + 83*t2 + 3*Power(t2,2)) - 
                  2*Power(t1,2)*(133 - 43*t2 + 6*Power(t2,2)) + 
                  2*t1*(35 + 15*t2 - 87*Power(t2,2) + 37*Power(t2,3)))) - 
            s1*(-1 + t2)*(Power(t1,3)*t2*(5 - 9*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(67 - 69*t2 + 13*Power(t2,2) - 
                  15*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-317 + 140*t2 + 64*Power(t2,2) + Power(t2,3)) + 
               2*Power(s2,3)*
                (-11 + 16*t2 - 7*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(-250 + 316*t2 + 25*Power(t2,2) - 116*Power(t2,3) + 
                  27*Power(t2,4) - 2*Power(t2,5)) + 
               Power(s2,2)*(13 - 39*t2 + 91*Power(t2,2) - 
                  75*Power(t2,3) + 6*Power(t2,4) + 
                  t1*(52 - 75*t2 + 27*Power(t2,2) - 10*Power(t2,3))) + 
               s2*(326 - 481*t2 + 46*Power(t2,2) + 163*Power(t2,3) - 
                  54*Power(t2,4) + 
                  2*Power(t1,2)*
                   (-15 + 19*t2 - 2*Power(t2,2) + Power(t2,3)) - 
                  2*t1*(44 - 66*t2 + 64*Power(t2,2) - 49*Power(t2,3) + 
                     3*Power(t2,4)))) + 
            Power(s1,4)*(16*Power(s2,5) - 
               Power(-1 + t2,3)*(6 + 31*t2 + 5*Power(t2,2)) + 
               Power(t1,4)*(-51 - 11*t2 + 6*Power(t2,2)) + 
               Power(s2,4)*(13 - 64*t1 - 45*t2 + 8*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*
                (171 + 9*t2 + 49*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(202 - 141*t2 + 2*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(-118 + 3*t2 + 134*Power(t2,2) - 
                  9*Power(t2,3) - 10*Power(t2,4)) + 
               2*Power(s2,3)*
                (-106 + 48*Power(t1,2) + 135*t2 - 85*Power(t2,2) + 
                  22*Power(t2,3) + t1*(-3 + 91*t2 - 24*Power(t2,2))) - 
               Power(s2,2)*(62 + 64*Power(t1,3) + 201*t2 - 
                  350*Power(t2,2) + 53*Power(t2,3) + 34*Power(t2,4) + 
                  Power(t1,2)*(78 + 240*t2 - 78*Power(t2,2)) + 
                  3*t1*(-202 + 207*t2 - 94*Power(t2,2) + 21*Power(t2,3))\
) + s2*(16*Power(t1,4) + Power(t1,3)*(122 + 114*t2 - 44*Power(t2,2)) + 
                  Power(-1 + t2,2)*(171 - 40*t2 + 99*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (-298 + 246*t2 - 57*Power(t2,2) + 7*Power(t2,3)) + 
                  2*t1*(98 + 71*t2 - 206*Power(t2,2) + 11*Power(t2,3) + 
                     26*Power(t2,4)))) + 
            Power(s1,3)*(-8*Power(s2,5) + 
               Power(s2,4)*(-81 + 32*t1 + 135*t2 - 40*Power(t2,2)) + 
               Power(t1,3)*(-186 + 117*t2 + 84*Power(t2,2) - 
                  31*Power(t2,3)) + 
               Power(t1,4)*(31 - 5*t2 + 6*Power(t2,2) - 2*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (111 - 24*t2 + 24*Power(t2,2) + Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (282 - 175*t2 + 46*Power(t2,2) + 17*Power(t2,3)) + 
               Power(t1,2)*(344 - 362*t2 + 9*Power(t2,2) + 
                  4*Power(t2,3) + 3*Power(t2,4) + 2*Power(t2,5)) - 
               2*Power(s2,3)*
                (-114 + 24*Power(t1,2) + 181*t2 - 78*Power(t2,2) - 
                  5*Power(t2,3) + 8*Power(t2,4) - 
                  t1*(94 - 164*t2 + 21*Power(t2,2) + 13*Power(t2,3))) + 
               Power(s2,2)*(398 + 32*Power(t1,3) - 584*t2 + 
                  393*Power(t2,2) - 308*Power(t2,3) + 93*Power(t2,4) + 
                  8*Power(t2,5) - 
                  6*Power(t1,2)*
                   (17 - 41*t2 - 7*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(-584 + 681*t2 - 84*Power(t2,2) - 91*Power(t2,3) + 
                     30*Power(t2,4))) - 
               2*s2*(4*Power(t1,4) + 
                  Power(t1,3)*
                   (18 + 24*t2 + 25*Power(t2,2) - 15*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (195 - 202*t2 + 72*Power(t2,2) + 20*Power(t2,3)) + 
                  Power(t1,2)*
                   (-271 + 218*t2 + 78*Power(t2,2) - 56*Power(t2,3) + 
                     7*Power(t2,4)) + 
                  t1*(366 - 450*t2 + 161*Power(t2,2) - 120*Power(t2,3) + 
                     37*Power(t2,4) + 6*Power(t2,5)))) + 
            Power(s1,2)*(Power(s2,4)*(36 - 43*t2 + 8*Power(t2,2)) + 
               Power(t1,4)*(-6 + 17*t2 - 12*Power(t2,2) + 
                  2*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*t2*
                (-91 - 2*t2 + 7*Power(t2,2) + 2*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (363 - 160*t2 - 18*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,3)*(79 - 100*t2 + 26*Power(t2,2) - 
                  17*Power(t2,3) + 2*Power(t2,4)) + 
               Power(t1,2)*(-318 + 382*t2 + 91*Power(t2,2) - 
                  200*Power(t2,3) + 47*Power(t2,4) - 2*Power(t2,5)) + 
               Power(s2,3)*(65 - 163*t2 + 112*Power(t2,2) + 
                  6*Power(t2,3) - 10*Power(t2,4) + 
                  2*t1*(-58 + 74*t2 - 21*Power(t2,2) + 3*Power(t2,3))) + 
               Power(s2,2)*(-538 + 1138*t2 - 825*Power(t2,2) + 
                  220*Power(t2,3) + 31*Power(t2,4) - 26*Power(t2,5) - 
                  2*Power(t1,2)*
                   (-59 + 75*t2 - 24*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(-13 + 86*t2 - 6*Power(t2,2) - 145*Power(t2,3) + 
                     48*Power(t2,4))) + 
               s2*(2*Power(t1,3)*
                   (-16 + 14*t2 - Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-131 + 177*t2 - 132*Power(t2,2) + 156*Power(t2,3) - 
                     40*Power(t2,4)) + 
                  2*Power(-1 + t2,2)*
                   (34 - 116*t2 + 25*Power(t2,2) + 11*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  2*t1*(409 - 691*t2 + 277*Power(t2,2) + 36*Power(t2,3) - 
                     42*Power(t2,4) + 11*Power(t2,5))))))*R1q(s1))/
     (Power(-1 + s1,2)*s1*(-1 + s2)*(-s + s2 - t1)*Power(-1 + s + t2,3)*
       (s - s1 + t2)*(-1 + s2 - t1 + t2)*
       (-1 + s - s*s1 + s1*s2 - s1*t1 + t2)) - 
    (8*(2*Power(s1,7)*Power(s2 - t1,3)*(-4 + 3*s2 - 3*t1 + 4*t2) + 
         Power(s,9)*(6 - 6*s2 + 6*t1 + 4*Power(t1,2) - 6*t2 - 8*t1*t2 + 
            4*Power(t2,2) + 6*s1*(-1 + s2 - t1 + t2)) - 
         Power(s,8)*(20 - 16*Power(s2,2) + 13*t1 + 17*Power(t1,2) - 
            10*Power(t1,3) - 55*t2 - 60*t1*t2 + 2*Power(t1,2)*t2 + 
            43*Power(t2,2) + 26*t1*Power(t2,2) - 18*Power(t2,3) + 
            30*Power(s1,2)*(-1 + s2 - t1 + t2) + 
            s2*(-4 + 15*t1 + 10*Power(t1,2) + 27*t2 - 28*t1*t2 + 
               18*Power(t2,2)) + 
            s1*(16 + 22*Power(s2,2) + 37*t1 + 31*Power(t1,2) + 5*t2 - 
               28*t1*t2 - 3*Power(t2,2) - s2*(38 + 35*t1 + 7*t2))) + 
         Power(s1,6)*(s2 - t1)*
          (12*Power(s2,4) + 12*Power(t1,4) + Power(t1,3)*(19 - 7*t2) + 
            2*Power(-1 + t2,3) - 2*t1*Power(-1 + t2,2)*(12 + t2) + 
            Power(s2,3)*(-19 - 48*t1 + 7*t2) + 
            Power(t1,2)*(-17 + 32*t2 - 15*Power(t2,2)) + 
            Power(s2,2)*(-17 + 72*Power(t1,2) + t1*(57 - 21*t2) + 
               32*t2 - 15*Power(t2,2)) + 
            s2*(-48*Power(t1,3) + 2*Power(-1 + t2,2)*(10 + 3*t2) + 
               3*Power(t1,2)*(-19 + 7*t2) + 
               t1*(34 - 64*t2 + 30*Power(t2,2)))) + 
         Power(s1,5)*(6*Power(s2,6) - 2*Power(s2,5)*(-7 + 18*t1 + t2) + 
            Power(s2,4)*(-97 + 90*Power(t1,2) + 112*t2 - 
               15*Power(t2,2) + 6*t1*(-13 + 3*t2)) + 
            Power(s2,3)*(117 - 120*Power(t1,3) + 
               Power(t1,2)*(172 - 52*t2) - 123*t2 - 29*Power(t2,2) + 
               35*Power(t2,3) + t1*(385 - 458*t2 + 73*Power(t2,2))) + 
            2*(3*Power(t1,6) + 4*t1*(-2 + t2)*Power(-1 + t2,3) + 
               Power(-1 + t2,4) + Power(t1,5)*(-11 + 5*t2) + 
               Power(t1,4)*(-47 + 61*t2 - 14*Power(t2,2)) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (-7 - 36*t2 + Power(t2,2)) - 
               Power(t1,3)*(58 - 69*t2 + 2*Power(t2,2) + 9*Power(t2,3))) \
+ Power(s2,2)*(90*Power(t1,4) + 4*Power(t1,3)*(-47 + 17*t2) - 
               2*Power(-1 + t2,2)*(11 + 25*t2 + 6*Power(t2,2)) - 
               3*Power(t1,2)*(191 - 234*t2 + 43*Power(t2,2)) - 
               6*t1*(59 - 66*t2 - 7*Power(t2,2) + 14*Power(t2,3))) + 
            s2*(-36*Power(t1,5) - 4*(-3 + t2)*Power(-1 + t2,3) - 
               6*Power(t1,4)*(-17 + 7*t2) + 
               2*t1*Power(-1 + t2,2)*(19 + 59*t2 + 6*Power(t2,2)) + 
               Power(t1,3)*(379 - 478*t2 + 99*Power(t2,2)) + 
               Power(t1,2)*(353 - 411*t2 - 9*Power(t2,2) + 
                  67*Power(t2,3)))) + 
         Power(s1,4)*(7*Power(s2,6)*(-1 + t2) - 
            9*Power(t1,6)*(-1 + t2) - 10*Power(-1 + t2,4)*t2 + 
            Power(s2,5)*(31 - 26*t1*(-1 + t2) - 46*t2 + 3*Power(t2,2)) + 
            Power(t1,5)*(-33 + 34*t2 + 11*Power(t2,2)) - 
            t1*Power(-1 + t2,3)*(-67 - 47*t2 + 20*Power(t2,2)) - 
            2*Power(t1,2)*Power(-1 + t2,2)*
             (101 - 58*t2 - 51*Power(t2,2) + 5*Power(t2,3)) + 
            Power(t1,4)*(-130 + 319*t2 - 208*Power(t2,2) + 
               31*Power(t2,3)) + 
            Power(t1,3)*(-225 + 691*t2 - 594*Power(t2,2) + 
               119*Power(t2,3) + 9*Power(t2,4)) + 
            Power(s2,4)*(-105 + 25*Power(t1,2)*(-1 + t2) + 307*t2 - 
               227*Power(t2,2) + 37*Power(t2,3) - 
               t1*(157 - 218*t2 + Power(t2,2))) + 
            Power(s2,3)*(225 + 20*Power(t1,3)*(-1 + t2) - 663*t2 + 
               500*Power(t2,2) - 15*Power(t2,3) - 47*Power(t2,4) + 
               Power(t1,2)*(318 - 412*t2 - 26*Power(t2,2)) + 
               t1*(437 - 1216*t2 + 865*Power(t2,2) - 134*Power(t2,3))) + 
            s2*(38*Power(t1,5)*(-1 + t2) + 
               Power(t1,4)*(163 - 182*t2 - 41*Power(t2,2)) + 
               Power(-1 + t2,3)*(-73 - 25*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(487 - 1240*t2 + 827*Power(t2,2) - 
                  122*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (-415 + 266*t2 + 153*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(685 - 2085*t2 + 1748*Power(t2,2) - 
                  293*Power(t2,3) - 55*Power(t2,4))) + 
            Power(s2,2)*(-55*Power(t1,4)*(-1 + t2) + 
               Power(t1,3)*(-322 + 388*t2 + 54*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-205 + 128*t2 + 71*Power(t2,2) + 12*Power(t2,3)) + 
               Power(t1,2)*(-689 + 1830*t2 - 1257*Power(t2,2) + 
                  188*Power(t2,3)) + 
               t1*(-685 + 2057*t2 - 1654*Power(t2,2) + 189*Power(t2,3) + 
                  93*Power(t2,4)))) + 
         Power(-1 + t2,3)*(-70 + 4*Power(s2,6)*(-1 + t2) + 244*t2 - 
            252*Power(t2,2) + 26*Power(t2,3) + 76*Power(t2,4) - 
            24*Power(t2,5) + Power(t1,5)*(-14 + 15*t2) + 
            Power(s2,5)*(8 - 20*t1*(-1 + t2) - 16*t2 + 9*Power(t2,2) - 
               2*Power(t2,3)) + 
            Power(t1,4)*(24 + 6*t2 - 28*Power(t2,2) + 6*Power(t2,3)) + 
            Power(t1,3)*(98 - 204*t2 + 66*Power(t2,2) + 
               25*Power(t2,3) - 6*Power(t2,4)) + 
            2*Power(t1,2)*(-37 - 37*t2 + 157*Power(t2,2) - 
               91*Power(t2,3) + 15*Power(t2,4)) - 
            t1*(204 - 421*t2 + 68*Power(t2,2) + 283*Power(t2,3) - 
               152*Power(t2,4) + 20*Power(t2,5)) + 
            Power(s2,4)*(52 + 40*Power(t1,2)*(-1 + t2) - 39*t2 - 
               20*Power(t2,2) + 13*Power(t2,3) + 2*Power(t2,4) + 
               t1*(-44 + 73*t2 - 30*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s2,3)*(-80 - 40*Power(t1,3)*(-1 + t2) + 227*t2 - 
               151*Power(t2,2) + 21*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,2)*(98 - 138*t2 + 36*Power(t2,2) - 
                  6*Power(t2,3)) + 
               t1*(-182 + 115*t2 + 88*Power(t2,2) - 49*Power(t2,3) - 
                  4*Power(t2,4))) + 
            Power(s2,2)*(-106 + 20*Power(t1,4)*(-1 + t2) - 41*t2 + 
               386*Power(t2,2) - 279*Power(t2,3) + 46*Power(t2,4) + 
               8*Power(t2,5) + 
               2*Power(t1,3)*
                (-55 + 68*t2 - 9*Power(t2,2) + Power(t2,3)) + 
               t1*(248 - 622*t2 + 320*Power(t2,2) + 11*Power(t2,3) - 
                  20*Power(t2,4)) + 
               Power(t1,2)*(232 - 107*t2 - 144*Power(t2,2) + 
                  65*Power(t2,3) + 2*Power(t2,4))) + 
            s2*(200 - 4*Power(t1,5)*(-1 + t2) - 403*t2 + 
               24*Power(t2,2) + 337*Power(t2,3) - 180*Power(t2,4) + 
               24*Power(t2,5) + 
               Power(t1,4)*(62 - 70*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(-126 + 25*t2 + 104*Power(t2,2) - 
                  35*Power(t2,3)) + 
               Power(t1,2)*(-266 + 599*t2 - 235*Power(t2,2) - 
                  57*Power(t2,3) + 22*Power(t2,4)) + 
               t1*(190 + 77*t2 - 646*Power(t2,2) + 427*Power(t2,3) - 
                  68*Power(t2,4) - 8*Power(t2,5)))) + 
         s1*Power(-1 + t2,2)*(Power(t1,6)*(12 - 13*t2) + 
            4*Power(s2,7)*(-1 + t2) + 
            Power(s2,6)*(20 - 24*t1*(-1 + t2) - 28*t2 + 7*Power(t2,2)) + 
            Power(t1,5)*(13 - 53*t2 + 29*Power(t2,2) - 6*Power(t2,3)) + 
            Power(t1,3)*(-116 + 617*t2 - 674*Power(t2,2) + 
               190*Power(t2,3) - 32*Power(t2,4)) + 
            2*Power(-1 + t2,2)*
             (57 - 47*t2 - 60*Power(t2,2) + 29*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(t1,4)*(-184 + 259*t2 - 8*Power(t2,2) - 
               38*Power(t2,3) + 6*Power(t2,4)) + 
            Power(t1,2)*(398 - 326*t2 - 780*Power(t2,2) + 
               972*Power(t2,3) - 298*Power(t2,4) + 32*Power(t2,5)) + 
            t1*(427 - 906*t2 + 153*Power(t2,2) + 724*Power(t2,3) - 
               458*Power(t2,4) + 60*Power(t2,5)) + 
            Power(s2,5)*(52 + 60*Power(t1,2)*(-1 + t2) - 19*t2 - 
               37*Power(t2,2) + 21*Power(t2,3) + 
               t1*(-112 + 153*t2 - 35*Power(t2,2))) + 
            Power(s2,4)*(-235 - 80*Power(t1,3)*(-1 + t2) + 446*t2 - 
               178*Power(t2,2) + 2*Power(t2,3) + 
               5*Power(t1,2)*(52 - 69*t2 + 14*Power(t2,2)) + 
               t1*(-203 + 47*t2 + 153*Power(t2,2) - 82*Power(t2,3))) + 
            Power(s2,3)*(13 + 60*Power(t1,4)*(-1 + t2) - 589*t2 + 
               992*Power(t2,2) - 481*Power(t2,3) + 72*Power(t2,4) + 
               8*Power(t2,5) - 
               10*Power(t1,3)*(32 - 41*t2 + 7*Power(t2,2)) + 
               2*Power(t1,2)*
                (142 + 13*t2 - 133*Power(t2,2) + 63*Power(t2,3)) + 
               t1*(887 - 1581*t2 + 506*Power(t2,2) + 64*Power(t2,3) - 
                  16*Power(t2,4))) + 
            s2*(-421 + 4*Power(t1,6)*(-1 + t2) + 870*t2 - 
               55*Power(t2,2) - 860*Power(t2,3) + 558*Power(t2,4) - 
               100*Power(t2,5) + 8*Power(t2,6) + 
               Power(t1,5)*(-80 + 93*t2 - 7*Power(t2,2)) + 
               Power(t1,4)*(8 + 169*t2 - 129*Power(t2,2) + 
                  37*Power(t2,3)) + 
               Power(t1,3)*(785 - 1207*t2 + 166*Power(t2,2) + 
                  144*Power(t2,3) - 28*Power(t2,4)) + 
               t1*(-887 + 822*t2 + 1579*Power(t2,2) - 
                  2102*Power(t2,3) + 640*Power(t2,4) - 48*Power(t2,5)) \
+ Power(t1,2)*(207 - 1683*t2 + 2148*Power(t2,2) - 745*Power(t2,3) + 
                  110*Power(t2,4) + 8*Power(t2,5))) - 
            Power(s2,2)*(-465 + 24*Power(t1,5)*(-1 + t2) + 402*t2 + 
               939*Power(t2,2) - 1226*Power(t2,3) + 370*Power(t2,4) - 
               18*Power(t2,5) - 
               5*Power(t1,4)*(44 - 54*t2 + 7*Power(t2,2)) + 
               2*Power(t1,3)*
                (77 + 85*t2 - 125*Power(t2,2) + 48*Power(t2,3)) + 
               Power(t1,2)*(1253 - 2083*t2 + 486*Power(t2,2) + 
                  172*Power(t2,3) - 38*Power(t2,4)) + 
               t1*(104 - 1655*t2 + 2466*Power(t2,2) - 1036*Power(t2,3) + 
                  150*Power(t2,4) + 16*Power(t2,5)))) + 
         Power(s1,3)*(8*Power(s2,7)*(-1 + t2) + 
            Power(s2,6)*(-19 - 48*t1*(-1 + t2) + 22*t2 - 
               9*Power(t2,2)) + 
            2*Power(t1,6)*(12 - 16*t2 + Power(t2,2)) + 
            2*Power(-1 + t2,4)*(-20 + 5*t2 + 9*Power(t2,2)) - 
            2*Power(t1,5)*(44 - 49*t2 - 9*Power(t2,2) + 8*Power(t2,3)) + 
            t1*Power(-1 + t2,3)*
             (103 - 215*t2 - 40*Power(t2,2) + 24*Power(t2,3)) + 
            Power(t1,4)*(-182 + 639*t2 - 588*Power(t2,2) + 
               149*Power(t2,3) - 24*Power(t2,4)) + 
            2*Power(t1,2)*Power(-1 + t2,2)*
             (-30 + 293*t2 - 181*Power(t2,2) - 18*Power(t2,3) + 
               3*Power(t2,4)) + 
            Power(t1,3)*(-23 + 721*t2 - 1610*Power(t2,2) + 
               1135*Power(t2,3) - 237*Power(t2,4) + 14*Power(t2,5)) + 
            Power(s2,5)*(149 + 120*Power(t1,2)*(-1 + t2) - 283*t2 + 
               129*Power(t2,2) - 7*Power(t2,3) + 
               t1*(71 - 78*t2 + 43*Power(t2,2))) - 
            Power(s2,4)*(111 + 160*Power(t1,3)*(-1 + t2) - 624*t2 + 
               830*Power(t2,2) - 378*Power(t2,3) + 67*Power(t2,4) + 
               10*Power(t1,2)*(7 - 6*t2 + 8*Power(t2,2)) - 
               2*t1*(-344 + 621*t2 - 255*Power(t2,2) + 8*Power(t2,3))) + 
            Power(s2,3)*(-31 + 120*Power(t1,4)*(-1 + t2) - 654*t2 + 
               1664*Power(t2,2) - 1193*Power(t2,3) + 193*Power(t2,4) + 
               21*Power(t2,5) + 
               10*Power(t1,3)*(-5 + 10*t2 + 7*Power(t2,2)) + 
               2*Power(t1,2)*
                (629 - 1063*t2 + 369*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(493 - 2423*t2 + 2946*Power(t2,2) - 
                  1195*Power(t2,3) + 203*Power(t2,4))) + 
            s2*(8*Power(t1,6)*(-1 + t2) - 
               Power(t1,5)*(101 - 138*t2 + Power(t2,2)) + 
               Power(-1 + t2,3)*
                (-101 + 237*t2 - 12*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,4)*(505 - 687*t2 + 69*Power(t2,2) + 
                  53*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (117 - 1200*t2 + 777*Power(t2,2) + 26*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(t1,3)*(635 - 2453*t2 + 2462*Power(t2,2) - 
                  737*Power(t2,3) + 117*Power(t2,4)) + 
               Power(t1,2)*(5 - 2068*t2 + 4868*Power(t2,2) - 
                  3483*Power(t2,3) + 693*Power(t2,4) - 15*Power(t2,5))) \
- Power(s2,2)*(48*Power(t1,5)*(-1 + t2) + 
               5*Power(t1,4)*(-29 + 42*t2 + 5*Power(t2,2)) + 
               4*Power(t1,3)*
                (284 - 439*t2 + 111*Power(t2,2) + 14*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (51 - 588*t2 + 375*Power(t2,2) + 16*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(t1,2)*(835 - 3613*t2 + 3990*Power(t2,2) - 
                  1405*Power(t2,3) + 229*Power(t2,4)) + 
               t1*(-49 - 2001*t2 + 4922*Power(t2,2) - 3541*Power(t2,3) + 
                  649*Power(t2,4) + 20*Power(t2,5)))) + 
         Power(s1,2)*(-1 + t2)*
          (-12*Power(s2,7)*(-1 + t2) + 
            Power(s2,6)*(-8 + 72*t1*(-1 + t2) + 28*t2 - 
               11*Power(t2,2)) + 
            Power(t1,6)*(-31 + 39*t2 + Power(t2,2)) - 
            2*Power(-1 + t2,3)*
             (-3 - 54*t2 + 22*Power(t2,2) + 7*Power(t2,3)) + 
            Power(t1,5)*(60 - 17*t2 - 64*Power(t2,2) + 13*Power(t2,3)) + 
            Power(t1,4)*(281 - 657*t2 + 355*Power(t2,2) + 
               8*Power(t2,3) + 2*Power(t2,4)) - 
            t1*Power(-1 + t2,2)*
             (201 + 140*t2 - 388*Power(t2,2) + 31*Power(t2,3) + 
               10*Power(t2,4)) - 
            Power(t1,3)*(53 + 639*t2 - 1389*Power(t2,2) + 
               845*Power(t2,3) - 170*Power(t2,4) + 12*Power(t2,5)) - 
            2*Power(t1,2)*(215 - 152*t2 - 624*Power(t2,2) + 
               829*Power(t2,3) - 293*Power(t2,4) + 25*Power(t2,5)) + 
            Power(s2,5)*(-174 - 180*Power(t1,2)*(-1 + t2) + 241*t2 - 
               34*Power(t2,2) - 25*Power(t2,3) + 
               t1*(71 - 179*t2 + 54*Power(t2,2))) + 
            Power(s2,4)*(261 + 240*Power(t1,3)*(-1 + t2) - 817*t2 + 
               722*Power(t2,2) - 207*Power(t2,3) + 30*Power(t2,4) - 
               5*Power(t1,2)*(47 - 95*t2 + 21*Power(t2,2)) + 
               t1*(766 - 1011*t2 + 102*Power(t2,2) + 103*Power(t2,3))) - 
            Power(s2,3)*(-191 + 180*Power(t1,4)*(-1 + t2) - 536*t2 + 
               1736*Power(t2,2) - 1248*Power(t2,3) + 239*Power(t2,4) + 
               10*Power(t2,5) - 
               10*Power(t1,3)*(39 - 67*t2 + 10*Power(t2,2)) + 
               2*Power(t1,2)*
                (657 - 802*t2 + 19*Power(t2,2) + 86*Power(t2,3)) + 
               t1*(1046 - 3032*t2 + 2401*Power(t2,2) - 
                  529*Power(t2,3) + 70*Power(t2,4))) + 
            Power(s2,2)*(-473 + 72*Power(t1,5)*(-1 + t2) + 354*t2 + 
               1357*Power(t2,2) - 1849*Power(t2,3) + 662*Power(t2,4) - 
               57*Power(t2,5) + 6*Power(t2,6) - 
               5*Power(t1,4)*(70 - 106*t2 + 9*Power(t2,2)) + 
               2*Power(t1,3)*
                (543 - 578*t2 - 79*Power(t2,2) + 74*Power(t2,3)) + 
               Power(t1,2)*(1590 - 4270*t2 + 2991*Power(t2,2) - 
                  429*Power(t2,3) + 52*Power(t2,4)) + 
               t1*(-479 - 1549*t2 + 4641*Power(t2,2) - 
                  3213*Power(t2,3) + 624*Power(t2,4) + 6*Power(t2,5))) + 
            s2*(-12*Power(t1,6)*(-1 + t2) + 
               Power(t1,5)*(163 - 223*t2 + 6*Power(t2,2)) + 
               Power(t1,4)*(-424 + 339*t2 + 192*Power(t2,2) - 
                  67*Power(t2,3)) + 
               Power(t1,3)*(-1086 + 2712*t2 - 1667*Power(t2,2) + 
                  99*Power(t2,3) - 14*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (-203 - 156*t2 + 444*Power(t2,2) - 93*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(t1,2)*(341 + 1652*t2 - 4294*Power(t2,2) + 
                  2810*Power(t2,3) - 555*Power(t2,4) + 16*Power(t2,5)) + 
               t1*(917 - 710*t2 - 2537*Power(t2,2) + 3477*Power(t2,3) - 
                  1256*Power(t2,4) + 117*Power(t2,5) - 8*Power(t2,6)))) + 
         Power(s,7)*(-17 - 16*Power(s2,3) - 17*t1 + 47*Power(t1,2) - 
            61*Power(t1,3) + 8*Power(t1,4) - 53*t2 - 31*t1*t2 + 
            52*Power(t1,2)*t2 + 28*Power(t1,3)*t2 + 132*Power(t2,2) + 
            99*t1*Power(t2,2) - 48*Power(t1,2)*Power(t2,2) - 
            90*Power(t2,3) - 20*t1*Power(t2,3) + 32*Power(t2,4) + 
            60*Power(s1,3)*(-1 + s2 - t1 + t2) + 
            Power(s2,2)*(-34 + 8*Power(t1,2) + t1*(21 - 36*t2) + 
               104*t2 + 32*Power(t2,2)) + 
            Power(s1,2)*(-16 + 83*Power(s2,2) + 61*t1 + 
               77*Power(t1,2) + 108*t2 - 13*t1*t2 - 60*Power(t2,2) - 
               s2*(63 + 128*t1 + 49*t2)) + 
            s2*(71 - 16*Power(t1,3) - 60*t2 + 30*Power(t2,2) - 
               80*Power(t2,3) + 8*Power(t1,2)*(7 + t2) + 
               t1*(-3 - 234*t2 + 88*Power(t2,2))) + 
            s1*(141 + 32*Power(s2,3) - 52*Power(t1,3) + 
               Power(t1,2)*(60 - 21*t2) - 284*t2 + 160*Power(t2,2) - 
               49*Power(t2,3) - 3*Power(s2,2)*(11 + 24*t1 + 27*t2) + 
               t1*(157 - 368*t2 + 122*Power(t2,2)) + 
               s2*(-148 + 92*Power(t1,2) + 255*t2 + 46*Power(t2,2) + 
                  t1*(41 + 2*t2)))) - 
         Power(s,6)*(-203 - 12*Power(s2,4) - 193*t1 - 52*Power(t1,2) - 
            207*Power(t1,3) + 49*Power(t1,4) - 2*Power(t1,5) + 506*t2 + 
            472*t1*t2 + 102*Power(t1,2)*t2 + 130*Power(t1,3)*t2 - 
            30*Power(t1,4)*t2 - 358*Power(t2,2) - 294*t1*Power(t2,2) - 
            273*Power(t1,2)*Power(t2,2) - 10*Power(t1,3)*Power(t2,2) + 
            35*Power(t2,3) + 44*t1*Power(t2,3) + 
            90*Power(t1,2)*Power(t2,3) + 50*Power(t2,4) - 
            20*t1*Power(t2,4) - 28*Power(t2,5) + 
            60*Power(s1,4)*(-1 + s2 - t1 + t2) + 
            Power(s2,3)*(6 + 2*Power(t1,2) + t1*(33 - 20*t2) + 104*t2 + 
               28*Power(t2,2)) + 
            2*Power(s1,3)*(-51 + 51*Power(s2,2) + 33*Power(t1,2) + 
               s2*(9 - 70*t1 - 68*t2) + 129*t2 - 64*Power(t2,2) + 
               t1*(-9 + 40*t2)) - 
            Power(s2,2)*(77 + 6*Power(t1,3) - 263*t2 + 
               140*Power(t2,2) + 140*Power(t2,3) - 
               Power(t1,2)*(19 + 10*t2) + 
               t1*(139 + 336*t2 - 108*Power(t2,2))) + 
            s2*(304 + 6*Power(t1,4) - 725*t2 + 474*Power(t2,2) - 
               206*Power(t2,3) + 140*Power(t2,4) + 
               Power(t1,3)*(-89 + 40*t2) + 
               Power(t1,2)*(340 + 102*t2 - 126*Power(t2,2)) + 
               t1*(87 - 355*t2 + 557*Power(t2,2) - 60*Power(t2,3))) + 
            Power(s1,2)*(260 + 90*Power(s2,3) - 76*Power(t1,3) + 
               Power(t1,2)*(109 - 95*t2) - 377*t2 + 97*Power(t2,2) - 
               16*Power(t2,3) - 9*Power(s2,2)*(-6 + 20*t1 + 33*t2) + 
               t1*(399 - 679*t2 + 159*Power(t2,2)) + 
               s2*(-422 + 166*Power(t1,2) + 514*t2 + 101*Power(t2,2) + 
                  t1*(-51 + 244*t2))) + 
            s1*(232 + 28*Power(s2,4) + 36*Power(t1,4) - 934*t2 + 
               1071*Power(t2,2) - 476*Power(t2,3) + 113*Power(t2,4) + 
               Power(t1,3)*(-163 + 130*t2) - 
               Power(s2,3)*(35 + 86*t1 + 140*t2) + 
               Power(t1,2)*(157 + 302*t2 - 171*Power(t2,2)) + 
               t1*(224 - 753*t2 + 701*Power(t2,2) - 108*Power(t2,3)) + 
               Power(s2,2)*(-122 + 124*Power(t1,2) + 291*t2 + 
                  225*Power(t2,2) + 5*t1*(21 + 34*t2)) - 
               s2*(121 + 102*Power(t1,3) - 613*t2 + 346*Power(t2,2) + 
                  222*Power(t2,3) + Power(t1,2)*(-93 + 160*t2) + 
                  t1*(-19 + 871*t2 - 176*Power(t2,2))))) + 
         Power(s,5)*(-570 - 10*Power(s2,5) - 873*t1 - 667*Power(t1,2) - 
            320*Power(t1,3) + 181*Power(t1,4) - 11*Power(t1,5) + 
            2087*t2 + 2152*t1*t2 + 555*Power(t1,2)*t2 + 
            364*Power(t1,3)*t2 - 152*Power(t1,4)*t2 + 8*Power(t1,5)*t2 - 
            2814*Power(t2,2) - 2011*t1*Power(t2,2) - 
            255*Power(t1,2)*Power(t2,2) + 21*Power(t1,3)*Power(t2,2) + 
            40*Power(t1,4)*Power(t2,2) + 1754*Power(t2,3) + 
            909*t1*Power(t2,3) + 292*Power(t1,2)*Power(t2,3) - 
            40*Power(t1,3)*Power(t2,3) - 527*Power(t2,4) - 
            208*t1*Power(t2,4) - 60*Power(t1,2)*Power(t2,4) + 
            58*Power(t2,5) + 40*t1*Power(t2,5) + 12*Power(t2,6) + 
            30*Power(s1,5)*(-1 + s2 - t1 + t2) + 
            Power(s2,4)*(20 + t1*(41 - 4*t2) + 50*t2 + 12*Power(t2,2)) + 
            2*Power(s1,4)*(-70 + 8*Power(s2,2) - 13*Power(t1,2) + 
               s2*(78 + 11*t1 - 99*t2) + 133*t2 - 57*Power(t2,2) + 
               t1*(-77 + 86*t2)) + 
            Power(s2,3)*(143 + 4*Power(t1,2)*(-13 + t2) + 187*t2 - 
               174*Power(t2,2) - 120*Power(t2,3) + 
               7*t1*(-19 - 34*t2 + 8*Power(t2,2))) + 
            Power(s2,2)*(-484 + 529*t2 - 347*Power(t2,2) - 
               74*Power(t2,3) + 240*Power(t2,4) + 
               2*Power(t1,3)*(5 + 6*t2) + 
               Power(t1,2)*(387 + 174*t2 - 108*Power(t2,2)) - 
               t1*(616 + 274*t2 - 903*Power(t2,2) + 60*Power(t2,3))) + 
            s2*(929 + Power(t1,4)*(22 - 20*t2) - 2517*t2 + 
               2396*Power(t2,2) - 915*Power(t2,3) + 218*Power(t2,4) - 
               120*Power(t2,5) + Power(t1,3)*(-455 + 166*t2) + 
               Power(t1,2)*(793 - 277*t2 - 750*Power(t2,2) + 
                  220*Power(t2,3)) + 
               t1*(1081 - 842*t2 + 440*Power(t2,2) - 328*Power(t2,3) - 
                  80*Power(t2,4))) + 
            Power(s1,3)*(138 + 28*Power(s2,3) + 34*Power(t1,3) + 
               Power(s2,2)*(291 + 42*t1 - 454*t2) + 25*t2 - 
               239*Power(t2,2) + 60*Power(t2,3) - 
               34*Power(t1,2)*(-6 + 5*t2) + 
               t1*(400 - 391*t2 - 32*Power(t2,2)) + 
               s2*(-445 - 104*Power(t1,2) + 262*t2 + 238*Power(t2,2) + 
                  t1*(-407 + 520*t2))) + 
            Power(s1,2)*(587 + 78*Power(s2,4) + 28*Power(t1,4) - 
               1844*t2 + 1714*Power(t2,2) - 581*Power(t2,3) + 
               124*Power(t2,4) + 2*Power(t1,3)*(-13 + 92*t2) - 
               2*Power(s2,3)*(46 + 105*t1 + 171*t2) + 
               Power(t1,2)*(388 + 195*t2 - 139*Power(t2,2)) + 
               t1*(737 - 2042*t2 + 1381*Power(t2,2) - 
                  113*Power(t2,3)) + 
               Power(s2,2)*(31 + 214*Power(t1,2) - 209*t2 + 
                  649*Power(t2,2) + 12*t1*(39 + 43*t2)) - 
               s2*(696 + 110*Power(t1,3) - 2017*t2 + 965*Power(t2,2) + 
                  319*Power(t2,3) + Power(t1,2)*(350 + 358*t2) + 
                  t1*(325 + 348*t2 + 242*Power(t2,2)))) + 
            s1*(77 + 22*Power(s2,5) - 10*Power(t1,5) + 
               Power(t1,4)*(69 - 118*t2) - 836*t2 + 1852*Power(t2,2) - 
               1496*Power(t2,3) + 512*Power(t2,4) - 105*Power(t2,5) - 
               Power(s2,4)*(93 + 90*t1 + 106*t2) + 
               Power(t1,3)*(-641 + 255*t2 - 54*Power(t2,2)) + 
               Power(t1,2)*(43 + 742*t2 - 1293*Power(t2,2) + 
                  341*Power(t2,3)) + 
               t1*(140 - 134*t2 + 316*Power(t2,2) - 215*Power(t2,3) - 
                  54*Power(t2,4)) + 
               Power(s2,3)*(96 + 148*Power(t1,2) - t2 + 
                  354*Power(t2,2) + t1*(382 + 256*t2)) - 
               Power(s2,2)*(205 + 124*Power(t1,3) - 1278*t2 + 
                  656*Power(t2,2) + 475*Power(t2,3) + 
                  104*Power(t1,2)*(4 + 3*t2) + 
                  t1*(541 + 587*t2 + 222*Power(t2,2))) + 
               s2*(123 + 54*Power(t1,4) - 224*t2 - 258*Power(t2,2) - 
                  96*Power(t2,3) + 394*Power(t2,4) + 
                  Power(t1,3)*(58 + 280*t2) + 
                  Power(t1,2)*(1086 + 333*t2 - 78*Power(t2,2)) + 
                  t1*(66 - 2052*t2 + 2361*Power(t2,2) - 146*Power(t2,3)))\
)) + Power(s,4)*(1024 + 4*Power(s2,6) + 2225*t1 + 1648*Power(t1,2) - 
            14*Power(t1,3) - 403*Power(t1,4) + 44*Power(t1,5) - 
            4351*t2 - 5919*t1*t2 - 2081*Power(t1,2)*t2 - 
            31*Power(t1,3)*t2 + 491*Power(t1,4)*t2 - 36*Power(t1,5)*t2 + 
            7288*Power(t2,2) + 5783*t1*Power(t2,2) + 
            76*Power(t1,2)*Power(t2,2) - 113*Power(t1,3)*Power(t2,2) - 
            152*Power(t1,4)*Power(t2,2) + 12*Power(t1,5)*Power(t2,2) - 
            6233*Power(t2,3) - 2827*t1*Power(t2,3) + 
            428*Power(t1,2)*Power(t2,3) + 238*Power(t1,3)*Power(t2,3) + 
            20*Power(t1,4)*Power(t2,3) + 2955*Power(t2,4) + 
            824*t1*Power(t2,4) - 7*Power(t1,2)*Power(t2,4) - 
            50*Power(t1,3)*Power(t2,4) - 778*Power(t2,5) - 
            136*t1*Power(t2,5) - 6*Power(t1,2)*Power(t2,5) + 
            93*Power(t2,6) + 22*t1*Power(t2,6) + 2*Power(t2,7) - 
            6*Power(s1,6)*(-1 + s2 - t1 + t2) - 
            Power(s2,5)*(-26 + 20*t1 + 33*t2 + 2*Power(t2,2)) + 
            Power(s1,5)*(82 + 64*Power(s2,2) + 85*Power(t1,2) + 
               t1*(173 - 158*t2) - 129*t2 + 45*Power(t2,2) + 
               s2*(-174 - 151*t1 + 163*t2)) + 
            Power(s2,4)*(-237 + 40*Power(t1,2) + 96*t2 + 
               47*Power(t2,2) + 50*Power(t2,3) - 
               2*t1*(45 - 78*t2 + 5*Power(t2,2))) - 
            Power(s2,3)*(116 + 40*Power(t1,3) + 342*t2 - 
               663*Power(t2,2) + 52*Power(t2,3) + 200*Power(t2,4) + 
               Power(t1,2)*(-70 + 234*t2 - 30*Power(t2,2)) + 
               t1*(-984 + 329*t2 + 499*Power(t2,2) - 20*Power(t2,3))) + 
            Power(s2,2)*(1387 + 20*Power(t1,4) - 1606*t2 + 
               97*Power(t2,2) + 218*Power(t2,3) - 248*Power(t2,4) + 
               200*Power(t2,5) + 
               Power(t1,3)*(70 + 96*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(-1660 + 861*t2 + 705*Power(t2,2) - 
                  170*Power(t2,3)) + 
               t1*(418 + 343*t2 - 1649*Power(t2,2) + 892*Power(t2,3) + 
                  120*Power(t2,4))) - 
            s2*(2100 + 4*Power(t1,5) - 5932*t2 + 6253*Power(t2,2) - 
               3027*Power(t2,3) + 509*Power(t2,4) + 19*Power(t2,5) + 
               50*Power(t2,6) + 
               Power(t1,4)*(120 - 51*t2 + 20*Power(t2,2)) + 
               Power(t1,3)*(-1316 + 1119*t2 + 101*Power(t2,2) - 
                  80*Power(t2,3)) + 
               Power(t1,2)*(288 - 30*t2 - 1099*Power(t2,2) + 
                  1078*Power(t2,3) - 130*Power(t2,4)) + 
               t1*(3049 - 3537*t2 - 277*Power(t2,2) + 986*Power(t2,3) - 
                  255*Power(t2,4) + 140*Power(t2,5))) + 
            Power(s1,4)*(64 + 152*Power(s2,3) - 204*Power(t1,3) - 
               368*t2 + 368*Power(t2,2) - 62*Power(t2,3) + 
               Power(t1,2)*(-325 + 198*t2) + 
               s2*(185 + 586*Power(t1,2) + t1*(762 - 566*t2) + 
                  189*t2 - 312*Power(t2,2)) + 
               t1*(-155 - 139*t2 + 228*Power(t2,2)) + 
               Power(s2,2)*(-534*t1 + 67*(-7 + 6*t2))) - 
            Power(s1,3)*(471 + 10*Power(s2,4) - 95*Power(t1,4) - 
               1197*t2 + 746*Power(t2,2) - 61*Power(t2,3) + 
               39*Power(t2,4) + Power(t1,3)*(193 + 84*t2) + 
               2*Power(t1,2)*(331 - 338*t2 + 78*Power(t2,2)) + 
               t1*(963 - 2379*t2 + 1174*Power(t2,2) + 86*Power(t2,3)) + 
               Power(s2,3)*(99*t1 - 7*(16 + 39*t2)) + 
               Power(s2,2)*(538 - 323*Power(t1,2) - 1423*t2 + 
                  963*Power(t2,2) + t1*(655 + 378*t2)) + 
               s2*(-1011 + 309*Power(t1,3) + 2458*t2 - 
                  922*Power(t2,2) - 365*Power(t2,3) - 
                  Power(t1,2)*(736 + 189*t2) + 
                  t1*(-1134 + 1897*t2 - 985*Power(t2,2)))) + 
            Power(s1,2)*(-648 - 80*Power(s2,5) + 2731*t2 - 
               4087*Power(t2,2) + 2527*Power(t2,3) - 641*Power(t2,4) + 
               116*Power(t2,5) + 2*Power(s2,4)*(160 + 155*t1 + 91*t2) + 
               Power(t1,4)*(253 + 106*t2) + 
               Power(t1,3)*(777 - 348*t2 + 146*Power(t2,2)) - 
               Power(t1,2)*(468 + 742*t2 - 1416*Power(t2,2) + 
                  319*Power(t2,3)) + 
               t1*(-922 + 2276*t2 - 2492*Power(t2,2) + 
                  885*Power(t2,3) + 91*Power(t2,4)) - 
               Power(s2,3)*(251 + 450*Power(t1,2) - 593*t2 + 
                  768*Power(t2,2) + t1*(1439 + 422*t2)) + 
               Power(s2,2)*(81 + 290*Power(t1,3) - 1279*t2 + 
                  40*Power(t2,2) + 919*Power(t2,3) + 
                  Power(t1,2)*(2171 + 404*t2) + 
                  t1*(843 - 436*t2 + 1032*Power(t2,2))) - 
               s2*(-674 + 70*Power(t1,4) + 2148*t2 - 2744*Power(t2,2) + 
                  649*Power(t2,3) + 455*Power(t2,4) + 
                  45*Power(t1,3)*(29 + 6*t2) + 
                  Power(t1,2)*(1369 - 191*t2 + 410*Power(t2,2)) + 
                  t1*(-477 - 2055*t2 + 1812*Power(t2,2) + 
                     370*Power(t2,3)))) - 
            s1*(-318 + 14*Power(s2,6) + Power(t1,6) + 749*t2 - 
               205*Power(t2,2) - 723*Power(t2,3) + 650*Power(t2,4) - 
               199*Power(t2,5) + 46*Power(t2,6) + 
               Power(t1,5)*(28 + 34*t2) - 
               Power(s2,5)*(55 + 71*t1 + 57*t2) + 
               Power(t1,4)*(298 - 244*t2 + 151*Power(t2,2)) - 
               4*Power(t1,3)*
                (382 - 314*t2 - 71*Power(t2,2) + 31*Power(t2,3)) + 
               Power(t1,2)*(-1439 + 2545*t2 - 2646*Power(t2,2) + 
                  1607*Power(t2,3) - 244*Power(t2,4)) + 
               t1*(-668 + 2380*t2 - 3384*Power(t2,2) + 
                  2037*Power(t2,3) - 580*Power(t2,4) + 136*Power(t2,5)) \
+ Power(s2,4)*(-240 + 145*Power(t1,2) + 142*t2 + 229*Power(t2,2) + 
                  6*t1*(48 + 37*t2)) - 
               Power(s2,3)*(-1045 + 150*Power(t1,3) + 579*t2 - 
                  306*Power(t2,2) + 564*Power(t2,3) + 
                  Power(t1,2)*(562 + 358*t2) + 
                  t1*(-772 + 922*t2 + 448*Power(t2,2))) + 
               Power(s2,2)*(-1253 + 80*Power(t1,4) + 3301*t2 - 
                  3623*Power(t2,2) + 786*Power(t2,3) + 
                  579*Power(t2,4) + 4*Power(t1,3)*(127 + 78*t2) + 
                  2*Power(t1,2)*(-263 + 587*t2 + 180*Power(t2,2)) + 
                  2*t1*(-1794 + 827*t2 + 521*Power(t2,2) + 
                     182*Power(t2,3))) - 
               s2*(-899 + 19*Power(t1,5) + 3095*t2 - 3628*Power(t2,2) + 
                  1514*Power(t2,3) - 492*Power(t2,4) + 331*Power(t2,5) + 
                  9*Power(t1,4)*(23 + 17*t2) + 
                  2*Power(t1,3)*(152 + 75*t2 + 146*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (1357 - 777*t2 - 544*Power(t2,2) + 108*Power(t2,3)) + 
                  t1*(-2522 + 5336*t2 - 5969*Power(t2,2) + 
                     2623*Power(t2,3) + 145*Power(t2,4))))) - 
         Power(s,3)*(1245 + 3211*t1 + 1996*Power(t1,2) - 
            695*Power(t1,3) - 517*Power(t1,4) + 100*Power(t1,5) - 
            16*Power(s2,6)*(-1 + t2) - 5853*t2 - 10027*t1*t2 - 
            3328*Power(t1,2)*t2 + 1697*Power(t1,3)*t2 + 
            882*Power(t1,4)*t2 - 135*Power(t1,5)*t2 + 
            11063*Power(t2,2) + 11039*t1*Power(t2,2) + 
            25*Power(t1,2)*Power(t2,2) - 1334*Power(t1,3)*Power(t2,2) - 
            401*Power(t1,4)*Power(t2,2) + 42*Power(t1,5)*Power(t2,2) - 
            11004*Power(t2,3) - 5069*t1*Power(t2,3) + 
            2360*Power(t1,2)*Power(t2,3) + 530*Power(t1,3)*Power(t2,3) + 
            28*Power(t1,4)*Power(t2,3) - 8*Power(t1,5)*Power(t2,3) + 
            6419*Power(t2,4) + 929*t1*Power(t2,4) - 
            1239*Power(t1,2)*Power(t2,4) - 197*Power(t1,3)*Power(t2,4) - 
            2327*Power(t2,5) - 70*t1*Power(t2,5) + 
            180*Power(t1,2)*Power(t2,5) + 20*Power(t1,3)*Power(t2,5) + 
            503*Power(t2,6) - 7*t1*Power(t2,6) - 
            8*Power(t1,2)*Power(t2,6) - 46*Power(t2,7) - 
            4*t1*Power(t2,7) + 
            Power(s2,5)*(10 + 80*t1*(-1 + t2) - 41*t2 + 
               24*Power(t2,2) + 8*Power(t2,3)) + 
            Power(s2,4)*(-523 - 160*Power(t1,2)*(-1 + t2) + 738*t2 - 
               175*Power(t2,2) + 32*Power(t2,3) - 80*Power(t2,4) + 
               t1*(20 + 149*t2 - 174*Power(t2,2))) + 
            Power(s1,6)*(51*Power(s2,2) + 55*Power(t1,2) + 
               t1*(81 - 73*t2) + s2*(-81 - 106*t1 + 73*t2) + 
               6*(3 - 4*t2 + Power(t2,2))) + 
            Power(s1,5)*(87 + 204*Power(s2,3) - 218*Power(t1,3) - 
               243*t2 + 173*Power(t2,2) - 17*Power(t2,3) + 
               Power(t1,2)*(-294 + 167*t2) + 
               Power(s2,2)*(-368 - 630*t1 + 243*t2) + 
               s2*(5 + 644*Power(t1,2) + t1*(658 - 406*t2) + 225*t2 - 
                  194*Power(t2,2)) + 3*t1*(1 - 73*t2 + 60*Power(t2,2))) \
+ Power(s2,3)*(290 + 160*Power(t1,3)*(-1 + t2) - 1399*t2 + 
               1956*Power(t2,2) - 936*Power(t2,3) - 92*Power(t2,4) + 
               160*Power(t2,5) - 
               2*Power(t1,2)*
                (110 + 33*t2 - 168*Power(t2,2) + 20*Power(t2,3)) + 
               t1*(2006 - 2696*t2 + 206*Power(t2,2) + 436*Power(t2,3) + 
                  80*Power(t2,4))) - 
            Power(s2,2)*(-1959 + 80*Power(t1,4)*(-1 + t2) + 2976*t2 + 
               471*Power(t2,2) - 2196*Power(t2,3) + 758*Power(t2,4) - 
               116*Power(t2,5) + 80*Power(t2,6) + 
               Power(t1,3)*(-440 + 286*t2 + 204*Power(t2,2) - 
                  40*Power(t2,3)) + 
               Power(t1,2)*(2960 - 4060*t2 + 288*Power(t2,2) + 
                  940*Power(t2,3) - 80*Power(t2,4)) + 
               t1*(985 - 3535*t2 + 4226*Power(t2,2) - 
                  2202*Power(t2,3) + 283*Power(t2,4) + 180*Power(t2,5))) \
+ s2*(-2985 + 16*Power(t1,5)*(-1 + t2) + 9480*t2 - 10818*Power(t2,2) + 
               5309*Power(t2,3) - 859*Power(t2,4) - 291*Power(t2,5) + 
               154*Power(t2,6) + 8*Power(t2,7) + 
               Power(t1,4)*(-350 + 379*t2 - 24*Power(t2,2)) + 
               Power(t1,3)*(1994 - 2984*t2 + 658*Power(t2,2) + 
                  444*Power(t2,3) - 80*Power(t2,4)) + 
               Power(t1,2)*(1390 - 3833*t2 + 3604*Power(t2,2) - 
                  1796*Power(t2,3) + 572*Power(t2,4)) + 
               t1*(-4081 + 6650*t2 + 266*Power(t2,2) - 
                  4816*Power(t2,3) + 2287*Power(t2,4) - 
                  350*Power(t2,5) + 72*Power(t2,6))) + 
            Power(s1,4)*(-152 + 147*Power(s2,4) + 217*Power(t1,4) + 
               209*t2 + 138*Power(t2,2) - 201*Power(t2,3) + 
               6*Power(t2,4) - Power(t1,3)*(58 + 21*t2) + 
               Power(s2,3)*(-51 - 666*t1 + 99*t2) + 
               Power(t1,2)*(-589 + 1081*t2 - 331*Power(t2,2)) - 
               t1*(694 - 1489*t2 + 518*Power(t2,2) + 191*Power(t2,3)) + 
               Power(s2,2)*(-693 + 1108*Power(t1,2) + 1667*t2 - 
                  777*Power(t2,2) - t1*(44 + 131*t2)) + 
               s2*(753 - 806*Power(t1,3) - 1573*t2 + 457*Power(t2,2) + 
                  277*Power(t2,3) + Power(t1,2)*(153 + 53*t2) + 
                  2*t1*(633 - 1354*t2 + 542*Power(t2,2)))) + 
            Power(s1,3)*(-527 - 60*Power(s2,5) - 56*Power(t1,5) + 
               Power(t1,4)*(556 - 49*t2) + 1983*t2 - 2563*Power(t2,2) + 
               1222*Power(t2,3) - 156*Power(t2,4) + 41*Power(t2,5) + 
               Power(s2,4)*(467 + 180*t1 + 23*t2) + 
               Power(t1,3)*(793 - 868*t2 + 274*Power(t2,2)) - 
               Power(s2,3)*(617 + 124*Power(t1,2) + 
                  t1*(2081 - 104*t2) - 1353*t2 + 758*Power(t2,2)) + 
               Power(t1,2)*(-257 - 462*t2 + 366*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(-897 + 2816*t2 - 3227*Power(t2,2) + 
                  1062*Power(t2,3) + 126*Power(t2,4)) + 
               Power(s2,2)*(88 - 108*Power(t1,3) + 
                  Power(t1,2)*(3317 - 326*t2) - 326*t2 - 
                  1163*Power(t2,2) + 1016*Power(t2,3) + 
                  t1*(1707 - 2870*t2 + 1410*Power(t2,2))) + 
               s2*(837 + 168*Power(t1,4) - 2959*t2 + 3549*Power(t2,2) - 
                  1005*Power(t2,3) - 302*Power(t2,4) + 
                  Power(t1,3)*(-2259 + 248*t2) + 
                  Power(t1,2)*(-1883 + 2385*t2 - 926*Power(t2,2)) + 
                  t1*(225 + 732*t2 + 745*Power(t2,2) - 988*Power(t2,3)))\
) + Power(s1,2)*(-406 - 51*Power(s2,6) + Power(t1,6) + 1787*t2 - 
               3270*Power(t2,2) + 3049*Power(t2,3) - 1366*Power(t2,4) + 
               250*Power(t2,5) - 44*Power(t2,6) - 
               Power(t1,5)*(224 + 13*t2) + 
               Power(s2,5)*(99 + 254*t1 + 125*t2) - 
               2*Power(t1,4)*(-101 + 44*t2 + 91*Power(t2,2)) - 
               2*Power(t1,3)*
                (-1162 + 1236*t2 - 124*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(1109 - 3631*t2 + 4672*Power(t2,2) - 
                  2186*Power(t2,3) + 238*Power(t2,4)) - 
               t1*(326 + 313*t2 - 1504*Power(t2,2) + 836*Power(t2,3) - 
                  205*Power(t2,4) + 138*Power(t2,5)) - 
               Power(s2,4)*(-752 + 505*Power(t1,2) + 420*t2 + 
                  298*Power(t2,2) + 11*t1*(60 + 43*t2)) + 
               Power(s2,3)*(-1745 + 500*Power(t1,3) + 2406*t2 - 
                  1850*Power(t2,2) + 982*Power(t2,3) + 
                  2*Power(t1,2)*(805 + 341*t2) + 
                  t1*(-2842 + 2132*t2 + 676*Power(t2,2))) - 
               Power(s2,2)*(-1297 + 245*Power(t1,4) + 5009*t2 - 
                  5676*Power(t2,2) + 1014*Power(t2,3) + 
                  716*Power(t2,4) + 2*Power(t1,3)*(930 + 229*t2) + 
                  Power(t1,2)*(-3630 + 3092*t2 + 640*Power(t2,2)) + 
                  2*t1*(-2825 + 3154*t2 - 1268*Power(t2,2) + 
                     683*Power(t2,3))) + 
               s2*(26 + 46*Power(t1,5) + 933*t2 - 1367*Power(t2,2) + 
                  56*Power(t2,3) - 48*Power(t2,4) + 304*Power(t2,5) + 
                  Power(t1,4)*(1035 + 137*t2) + 
                  2*Power(t1,3)*(-871 + 734*t2 + 222*Power(t2,2)) + 
                  Power(t1,2)*
                   (-6229 + 6374*t2 - 934*Power(t2,2) + 
                     386*Power(t2,3)) + 
                  t1*(-2226 + 8116*t2 - 9928*Power(t2,2) + 
                     3204*Power(t2,3) + 398*Power(t2,4)))) + 
            s1*(821 - 4*Power(s2,7) - 3018*t2 + 4558*Power(t2,2) - 
               3669*Power(t2,3) + 1641*Power(t2,4) - 371*Power(t2,5) + 
               30*Power(t2,6) + 8*Power(t2,7) + 3*Power(t1,6)*(5 + t2) + 
               Power(s2,6)*(-16 + 24*t1 + 35*t2) + 
               Power(t1,5)*(-90 + 55*t2 + 48*Power(t2,2)) + 
               Power(t1,4)*(-972 + 1141*t2 - 358*Power(t2,2) + 
                  99*Power(t2,3)) + 
               Power(t1,3)*(1465 - 1776*t2 - 306*Power(t2,2) + 
                  984*Power(t2,3) - 176*Power(t2,4)) + 
               Power(t1,2)*(3628 - 6835*t2 + 4612*Power(t2,2) - 
                  2058*Power(t2,3) + 741*Power(t2,4) - 60*Power(t2,5)) + 
               t1*(2441 - 7268*t2 + 9663*Power(t2,2) - 
                  7520*Power(t2,3) + 3225*Power(t2,4) - 
                  655*Power(t2,5) + 78*Power(t2,6)) - 
               Power(s2,5)*(-308 + 60*Power(t1,2) + 218*t2 + 
                  60*Power(t2,2) + t1*(-65 + 178*t2)) + 
               Power(s2,4)*(-301 + 80*Power(t1,3) + 281*t2 - 
                  344*Power(t2,2) + 331*Power(t2,3) + 
                  5*Power(t1,2)*(-17 + 73*t2) + 
                  t1*(-1402 + 1087*t2 + 208*Power(t2,2))) + 
               Power(s2,3)*(-1709 - 60*Power(t1,4) + 
                  Power(t1,3)*(10 - 380*t2) + 1645*t2 + 
                  132*Power(t2,2) + 246*Power(t2,3) - 496*Power(t2,4) - 
                  8*Power(t1,2)*(-306 + 251*t2 + 39*Power(t2,2)) + 
                  t1*(1515 - 824*t2 + 150*Power(t2,2) - 652*Power(t2,3))\
) + Power(s2,2)*(3233 + 24*Power(t1,5) - 6887*t2 + 6745*Power(t2,2) - 
                  4054*Power(t2,3) + 597*Power(t2,4) + 
                  384*Power(t2,5) + 5*Power(t1,4)*(14 + 41*t2) + 
                  2*Power(t1,3)*(-1006 + 841*t2 + 144*Power(t2,2)) + 
                  Power(t1,2)*
                   (-3099 + 1946*t2 + 374*Power(t2,2) + 
                     410*Power(t2,3)) + 
                  t1*(5263 - 5666*t2 - 930*Power(t2,2) + 
                     1492*Power(t2,3) + 396*Power(t2,4))) - 
               s2*(2424 + 4*Power(t1,6) - 7657*t2 + 10445*Power(t2,2) - 
                  7435*Power(t2,3) + 2400*Power(t2,4) - 
                  351*Power(t2,5) + 138*Power(t2,6) + 
                  Power(t1,5)*(59 + 50*t2) + 
                  2*Power(t1,4)*(-374 + 299*t2 + 86*Power(t2,2)) + 
                  Power(t1,3)*
                   (-2857 + 2544*t2 - 178*Power(t2,2) + 188*Power(t2,3)) \
+ Power(t1,2)*(5019 - 5797*t2 - 1104*Power(t2,2) + 2722*Power(t2,3) - 
                     276*Power(t2,4)) + 
                  t1*(6849 - 13422*t2 + 10637*Power(t2,2) - 
                     5592*Power(t2,3) + 1318*Power(t2,4) + 
                     256*Power(t2,5))))) + 
         s*(-2*Power(s1,7)*(s2 - t1)*
             (8*Power(s2,2) + 8*Power(t1,2) - 9*t1*(-1 + t2) - 
               Power(-1 + t2,2) + s2*(-9 - 16*t1 + 9*t2)) - 
            Power(s1,6)*(s2 - t1)*
             (59*Power(s2,3) - 59*Power(t1,3) + 
               4*Power(-1 + t2,2)*(10 + 3*t2) + 
               Power(t1,2)*(-81 + 43*t2) + 
               Power(s2,2)*(-81 - 177*t1 + 43*t2) + 
               s2*(-42 + 177*Power(t1,2) + t1*(162 - 86*t2) + 94*t2 - 
                  52*Power(t2,2)) + 10*t1*(3 - 7*t2 + 4*Power(t2,2))) + 
            Power(s1,5)*(-50*Power(s2,5) + 58*Power(t1,5) - 
               16*Power(-1 + t2,3) + 3*Power(t1,4)*(-9 + 5*t2) + 
               Power(s2,4)*(-9 + 258*t1 + 13*t2) - 
               t1*Power(-1 + t2,2)*(15 + 105*t2 + 16*Power(t2,2)) - 
               2*Power(t1,3)*(132 - 225*t2 + 61*Power(t2,2)) + 
               Power(s2,3)*(280 - 532*Power(t1,2) - 54*t1*(-1 + t2) - 
                  478*t2 + 134*Power(t2,2)) - 
               Power(t1,2)*(274 - 453*t2 + 132*Power(t2,2) + 
                  47*Power(t2,3)) + 
               Power(s2,2)*(-238 + 548*Power(t1,3) + 299*t2 + 
                  68*Power(t2,2) - 129*Power(t2,3) + 
                  12*Power(t1,2)*(-9 + 7*t2) - 
                  2*t1*(406 - 691*t2 + 189*Power(t2,2))) + 
               s2*(-282*Power(t1,4) + Power(t1,3)*(90 - 58*t2) + 
                  Power(-1 + t2,2)*(1 + 119*t2 + 16*Power(t2,2)) + 
                  2*Power(t1,2)*(398 - 677*t2 + 183*Power(t2,2)) + 
                  4*t1*(127 - 185*t2 + 13*Power(t2,2) + 45*Power(t2,3)))\
) + Power(s1,4)*(Power(s2,6) - 15*Power(t1,6) + 
               Power(t1,5)*(145 - 53*t2) + 
               Power(s2,5)*(-85 + 10*t1 + 9*t2) + 
               Power(-1 + t2,3)*(25 + 17*t2) + 
               Power(s2,4)*(124 - 65*Power(t1,2) + t1*(485 - 89*t2) - 
                  195*t2 + 53*Power(t2,2)) + 
               Power(t1,4)*(144 - 229*t2 + 99*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (-199 + 340*t2 + 77*Power(t2,2) + 12*Power(t2,3)) + 
               Power(t1,3)*(-148 + 483*t2 - 524*Power(t2,2) + 
                  121*Power(t2,3)) + 
               Power(t1,2)*(-378 + 1603*t2 - 1872*Power(t2,2) + 
                  651*Power(t2,3) - 4*Power(t2,4)) + 
               Power(s2,3)*(113 + 140*Power(t1,3) - 635*t2 + 
                  873*Power(t2,2) - 283*Power(t2,3) + 
                  2*Power(t1,2)*(-545 + 133*t2) + 
                  t1*(-492 + 766*t2 - 234*Power(t2,2))) + 
               Power(s2,2)*(-374 - 145*Power(t1,4) + 
                  Power(t1,3)*(1210 - 354*t2) + 1491*t2 - 
                  1500*Power(t2,2) + 227*Power(t2,3) + 
                  156*Power(t2,4) + 
                  12*Power(t1,2)*(63 - 98*t2 + 34*Power(t2,2)) + 
                  t1*(-318 + 1585*t2 - 2102*Power(t2,2) + 
                     631*Power(t2,3))) + 
               s2*(74*Power(t1,5) + Power(t1,4)*(-665 + 221*t2) + 
                  Power(t1,3)*(-532 + 834*t2 - 326*Power(t2,2)) + 
                  Power(t1,2)*
                   (353 - 1433*t2 + 1753*Power(t2,2) - 469*Power(t2,3)) \
- 2*Power(-1 + t2,2)*(-89 + 127*t2 + 69*Power(t2,2) + 8*Power(t2,3)) - 
                  2*t1*(-362 + 1495*t2 - 1614*Power(t2,2) + 
                     395*Power(t2,3) + 86*Power(t2,4)))) + 
            Power(-1 + t2,2)*
             (-398 + 16*Power(s2,6)*(-1 + t2) + 1523*t2 - 
               2011*Power(t2,2) + 1021*Power(t2,3) - 133*Power(t2,4) + 
               14*Power(t2,5) - 16*Power(t2,6) + 
               Power(t1,5)*(-65 + 71*t2 - 3*Power(t2,2)) + 
               Power(s2,5)*(28 - 80*t1*(-1 + t2) - 49*t2 + 
                  26*Power(t2,2) - 8*Power(t2,3)) + 
               2*Power(t1,4)*
                (73 - 38*t2 - 37*Power(t2,2) + 14*Power(t2,3)) + 
               Power(t1,3)*(486 - 1071*t2 + 515*Power(t2,2) + 
                  20*Power(t2,3) - 13*Power(t2,4)) + 
               Power(t1,2)*(-480 + 7*t2 + 1351*Power(t2,2) - 
                  1097*Power(t2,3) + 281*Power(t2,4) - 20*Power(t2,5)) \
+ t1*(-1137 + 2698*t2 - 1344*Power(t2,2) - 798*Power(t2,3) + 
                  695*Power(t2,4) - 128*Power(t2,5) + 8*Power(t2,6)) + 
               Power(s2,4)*(261 + 160*Power(t1,2)*(-1 + t2) - 264*t2 - 
                  23*Power(t2,2) + 30*Power(t2,3) + 20*Power(t2,4) + 
                  t1*(-165 + 231*t2 - 71*Power(t2,2) + 20*Power(t2,3))) \
+ Power(s2,3)*(-357 - 160*Power(t1,3)*(-1 + t2) + 1080*t2 - 
                  879*Power(t2,2) + 225*Power(t2,3) + 2*Power(t2,4) - 
                  8*Power(t2,5) + 
                  Power(t1,2)*
                   (392 - 470*t2 + 60*Power(t2,2) - 12*Power(t2,3)) + 
                  t1*(-933 + 860*t2 + 191*Power(t2,2) - 
                     174*Power(t2,3) - 40*Power(t2,4))) + 
               Power(s2,2)*(-615 + 80*Power(t1,4)*(-1 + t2) + 143*t2 + 
                  1642*Power(t2,2) - 1449*Power(t2,3) + 
                  283*Power(t2,4) + 38*Power(t2,5) - 
                  2*Power(t1,3)*
                   (233 - 257*t2 + 7*Power(t2,2) + 2*Power(t2,3)) + 
                  Power(t1,2)*
                   (1229 - 1004*t2 - 387*Power(t2,2) + 
                     286*Power(t2,3) + 20*Power(t2,4)) + 
                  t1*(1130 - 2979*t2 + 1941*Power(t2,2) - 
                     246*Power(t2,3) - 47*Power(t2,4) + 12*Power(t2,5))\
) + s2*(1093 - 16*Power(t1,5)*(-1 + t2) - 2549*t2 + 1121*Power(t2,2) + 
                  981*Power(t2,3) - 776*Power(t2,4) + 152*Power(t2,5) - 
                  16*Power(t2,6) + 
                  Power(t1,3)*
                   (-703 + 484*t2 + 293*Power(t2,2) - 170*Power(t2,3)) \
+ Power(t1,4)*(276 - 297*t2 + 2*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(t1,2)*
                   (-1259 + 2970*t2 - 1577*Power(t2,2) + Power(t2,3) + 
                     58*Power(t2,4) - 4*Power(t2,5)) - 
                  t1*(-1153 + 364*t2 + 2705*Power(t2,2) - 
                     2386*Power(t2,3) + 542*Power(t2,4) + 12*Power(t2,5)\
))) + Power(s1,3)*(8*Power(s2,7) + Power(s2,6)*(51 - 48*t1 - 53*t2) + 
               Power(t1,6)*(-47 + 13*t2) - 
               2*Power(t1,5)*(-155 + 110*t2 + 8*Power(t2,2)) + 
               Power(t1,4)*(384 - 955*t2 + 539*Power(t2,2) - 
                  124*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (15 + 161*t2 - 72*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,3)*(-449 + 182*t2 + 487*Power(t2,2) - 
                  188*Power(t2,3) - 16*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (133 - 859*t2 + 983*Power(t2,2) - 101*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(t1,2)*(-436 + 1601*t2 - 2881*Power(t2,2) + 
                  2442*Power(t2,3) - 775*Power(t2,4) + 49*Power(t2,5)) \
+ 2*Power(s2,5)*(-156 + 60*Power(t1,2) + 146*t2 - 7*Power(t2,2) + 
                  2*t1*(-52 + 63*t2)) + 
               Power(s2,4)*(109 - 160*Power(t1,3) - 675*t2 + 
                  618*Power(t2,2) - 192*Power(t2,3) - 
                  5*Power(t1,2)*(-55 + 93*t2) + 
                  2*t1*(785 - 706*t2 + 26*Power(t2,2))) + 
               Power(s2,3)*(409 + 120*Power(t1,4) - 253*t2 + 
                  165*Power(t2,2) - 635*Power(t2,3) + 
                  298*Power(t2,4) + 40*Power(t1,3)*(-1 + 10*t2) - 
                  4*Power(t1,2)*(787 - 676*t2 + 14*Power(t2,2)) + 
                  t1*(-611 + 2680*t2 - 2093*Power(t2,2) + 
                     600*Power(t2,3))) - 
               Power(s2,2)*(434 + 48*Power(t1,5) - 1751*t2 + 
                  3241*Power(t2,2) - 2554*Power(t2,3) + 
                  529*Power(t2,4) + 101*Power(t2,5) + 
                  5*Power(t1,4)*(43 + 27*t2) + 
                  4*Power(t1,3)*(-786 + 640*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (-1279 + 4290*t2 - 2871*Power(t2,2) + 
                     748*Power(t2,3)) + 
                  t1*(1251 - 544*t2 - 481*Power(t2,2) - 
                     802*Power(t2,3) + 528*Power(t2,4))) + 
               s2*(8*Power(t1,6) - 4*Power(t1,5)*(-46 + 3*t2) + 
                  2*Power(t1,4)*(-782 + 598*t2 + 19*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1161 + 3240*t2 - 1935*Power(t2,2) + 
                     464*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (158 - 819*t2 + 800*Power(t2,2) + 9*Power(t2,3) + 
                     22*Power(t2,4)) + 
                  Power(t1,2)*
                   (1291 - 473*t2 - 1133*Power(t2,2) + 
                     21*Power(t2,3) + 246*Power(t2,4)) + 
                  2*t1*(411 - 1564*t2 + 2855*Power(t2,2) - 
                     2312*Power(t2,3) + 570*Power(t2,4) + 40*Power(t2,5)\
))) - s1*(-1 + t2)*(-12*Power(s2,7)*(-1 + t2) + 
               Power(s2,6)*(-52 + 72*t1*(-1 + t2) + 61*t2 - 
                  7*Power(t2,2)) + 
               Power(t1,6)*(-41 + 42*t2 + Power(t2,2)) + 
               Power(t1,5)*(-22 + 95*t2 - 61*Power(t2,2) + 
                  22*Power(t2,3)) + 
               Power(t1,4)*(807 - 1350*t2 + 460*Power(t2,2) + 
                  18*Power(t2,3) - 5*Power(t2,4)) + 
               Power(t1,3)*(292 - 1911*t2 + 2556*Power(t2,2) - 
                  1222*Power(t2,3) + 341*Power(t2,4) - 26*Power(t2,5)) \
+ Power(-1 + t2,2)*(-543 + 811*t2 - 224*Power(t2,2) + 
                  164*Power(t2,3) - 104*Power(t2,4) + 8*Power(t2,5)) + 
               t1*(-1914 + 5052*t2 - 3987*Power(t2,2) + 
                  684*Power(t2,3) + 35*Power(t2,4) + 166*Power(t2,5) - 
                  36*Power(t2,6)) + 
               Power(t1,2)*(-1931 + 2923*t2 + 776*Power(t2,2) - 
                  2755*Power(t2,3) + 1141*Power(t2,4) - 
                  158*Power(t2,5) + 8*Power(t2,6)) + 
               Power(s2,5)*(-236 - 180*Power(t1,2)*(-1 + t2) + 
                  260*t2 - 4*Power(t2,2) - 54*Power(t2,3) + 
                  t1*(301 - 347*t2 + 34*Power(t2,2))) + 
               Power(s2,4)*(860 + 240*Power(t1,3)*(-1 + t2) - 
                  1751*t2 + 1016*Power(t2,2) - 274*Power(t2,3) + 
                  79*Power(t2,4) + 
                  Power(t1,2)*(-725 + 820*t2 - 65*Power(t2,2)) + 
                  t1*(962 - 1065*t2 + 75*Power(t2,2) + 198*Power(t2,3))\
) - Power(s2,3)*(-181 + 180*Power(t1,4)*(-1 + t2) - 1495*t2 + 
                  3354*Power(t2,2) - 2075*Power(t2,3) + 
                  381*Power(t2,4) + 46*Power(t2,5) - 
                  10*Power(t1,3)*(93 - 103*t2 + 6*Power(t2,2)) + 
                  4*Power(t1,2)*
                   (362 - 385*t2 + 35*Power(t2,2) + 73*Power(t2,3)) + 
                  t1*(3343 - 6387*t2 + 3124*Power(t2,2) - 
                     508*Power(t2,3) + 148*Power(t2,4))) + 
               Power(s2,2)*(-2081 + 72*Power(t1,5)*(-1 + t2) + 
                  3085*t2 + 966*Power(t2,2) - 2819*Power(t2,3) + 
                  847*Power(t2,4) - 18*Power(t2,5) + 24*Power(t2,6) - 
                  5*Power(t1,4)*(134 - 145*t2 + 5*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (466 - 415*t2 + 5*Power(t2,2) + 114*Power(t2,3)) + 
                  Power(t1,2)*
                   (4913 - 8871*t2 + 3660*Power(t2,2) - 
                     176*Power(t2,3) + 54*Power(t2,4)) + 
                  t1*(-282 - 4133*t2 + 8252*Power(t2,2) - 
                     4824*Power(t2,3) + 1031*Power(t2,4) + 
                     46*Power(t2,5))) - 
               s2*(-1851 + 12*Power(t1,6)*(-1 + t2) + 4847*t2 - 
                  3728*Power(t2,2) + 477*Power(t2,3) + 
                  237*Power(t2,4) - 6*Power(t2,5) + 24*Power(t2,6) + 
                  Power(t1,5)*(-257 + 271*t2 - 2*Power(t2,2)) + 
                  2*Power(t1,4)*
                   (94 - 60*Power(t2,2) + 51*Power(t2,3)) + 
                  Power(t1,3)*
                   (3237 - 5585*t2 + 2012*Power(t2,2) + 
                     76*Power(t2,3) - 20*Power(t2,4)) + 
                  Power(t1,2)*
                   (191 - 4549*t2 + 7454*Power(t2,2) - 
                     3971*Power(t2,3) + 991*Power(t2,4) - 
                     26*Power(t2,5)) + 
                  2*t1*(-2058 + 3196*t2 + 613*Power(t2,2) - 
                     2645*Power(t2,3) + 976*Power(t2,4) - 
                     94*Power(t2,5) + 16*Power(t2,6)))) + 
            Power(s1,2)*(-24*Power(s2,7)*(-1 + t2) + 
               Power(t1,6)*(-83 + 91*t2 + Power(t2,2)) + 
               Power(s2,6)*(15 + 144*t1*(-1 + t2) - 35*t2 + 
                  29*Power(t2,2)) + 
               Power(t1,5)*(279 - 342*t2 + 16*Power(t2,2) + 
                  39*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (81 - 40*t2 + 296*Power(t2,2) - 161*Power(t2,3) + 
                  16*Power(t2,4)) + 
               Power(t1,4)*(905 - 2232*t2 + 1619*Power(t2,2) - 
                  361*Power(t2,3) + 58*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (-820 + 1138*t2 - 1199*Power(t2,2) + 946*Power(t2,3) - 
                  157*Power(t2,4) + 8*Power(t2,5)) - 
               Power(t1,3)*(619 + 167*t2 - 2468*Power(t2,2) + 
                  2400*Power(t2,3) - 794*Power(t2,4) + 66*Power(t2,5)) + 
               Power(t1,2)*(-1761 + 3781*t2 - 2038*Power(t2,2) + 
                  35*Power(t2,3) - 145*Power(t2,4) + 140*Power(t2,5) - 
                  12*Power(t2,6)) + 
               Power(s2,5)*(-542 - 360*Power(t1,2)*(-1 + t2) + 908*t2 - 
                  343*Power(t2,2) - 15*Power(t2,3) + 
                  t1*(8 + 84*t2 - 146*Power(t2,2))) + 
               Power(s2,4)*(599 + 480*Power(t1,3)*(-1 + t2) - 1970*t2 + 
                  2101*Power(t2,2) - 969*Power(t2,3) + 
                  228*Power(t2,4) + 
                  5*Power(t1,2)*(-53 + 21*t2 + 59*Power(t2,2)) + 
                  t1*(2487 - 4094*t2 + 1508*Power(t2,2) + 
                     59*Power(t2,3))) - 
               Power(s2,3)*(-958 + 360*Power(t1,4)*(-1 + t2) + 259*t2 + 
                  2617*Power(t2,2) - 2370*Power(t2,3) + 
                  330*Power(t2,4) + 132*Power(t2,5) + 
                  20*Power(t1,3)*(-34 + 28*t2 + 15*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (2244 - 3588*t2 + 1241*Power(t2,2) + 63*Power(t2,3)) \
+ t1*(2578 - 7630*t2 + 7130*Power(t2,2) - 2724*Power(t2,3) + 
                     602*Power(t2,4))) + 
               Power(s2,2)*(-1758 + 144*Power(t1,5)*(-1 + t2) + 
                  3867*t2 - 2608*Power(t2,2) + 1115*Power(t2,3) - 
                  878*Power(t2,4) + 198*Power(t2,5) + 64*Power(t2,6) + 
                  5*Power(t1,4)*(-151 + 147*t2 + 31*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (1981 - 3022*t2 + 914*Power(t2,2) + 87*Power(t2,3)) \
+ 2*Power(t1,2)*(2132 - 5791*t2 + 4788*Power(t2,2) - 
                     1451*Power(t2,3) + 289*Power(t2,4)) + 
                  t1*(-2705 + 927*t2 + 7050*Power(t2,2) - 
                     6940*Power(t2,3) + 1556*Power(t2,4) + 
                     142*Power(t2,5))) - 
               s2*(24*Power(t1,6)*(-1 + t2) + 
                  Power(t1,5)*(-400 + 420*t2 + 34*Power(t2,2)) + 
                  Power(t1,4)*
                   (1698 - 2396*t2 + 527*Power(t2,2) + 131*Power(t2,3)) \
+ 2*Power(t1,3)*(1595 - 4077*t2 + 3083*Power(t2,2) - 754*Power(t2,3) + 
                     131*Power(t2,4)) + 
                  Power(t1,2)*
                   (-2366 + 501*t2 + 6901*Power(t2,2) - 
                     6970*Power(t2,3) + 2020*Power(t2,4) - 
                     56*Power(t2,5)) + 
                  Power(-1 + t2,2)*
                   (-833 + 1181*t2 - 1155*Power(t2,2) + 
                     753*Power(t2,3) - 42*Power(t2,4) + 12*Power(t2,5)) \
+ t1*(-3541 + 7682*t2 - 4558*Power(t2,2) + 890*Power(t2,3) - 
                     781*Power(t2,4) + 244*Power(t2,5) + 64*Power(t2,6)))\
)) + Power(s,2)*(2*Power(s1,7)*(s2 - t1)*(-7 + 6*s2 - 6*t1 + 7*t2) + 
            Power(s1,6)*(98*Power(s2,3) - 98*Power(t1,3) + 
               24*Power(-1 + t2,2) + 5*Power(t1,2)*(-25 + 17*t2) + 
               Power(s2,2)*(-137 - 294*t1 + 97*t2) + 
               s2*(-15 + 294*Power(t1,2) + t1*(262 - 182*t2) + 64*t2 - 
                  49*Power(t2,2)) + t1*(15 - 64*t2 + 49*Power(t2,2))) + 
            Power(s1,5)*(160*Power(s2,4) + 175*Power(t1,4) - 
               6*Power(t1,3)*(-22 + 9*t2) + 
               Power(s2,3)*(-163 - 655*t1 + 63*t2) + 
               Power(s2,2)*(-316 + 1005*Power(t1,2) + 
                  t1*(446 - 168*t2) + 734*t2 - 322*Power(t2,2)) + 
               Power(t1,2)*(-235 + 530*t2 - 199*Power(t2,2)) + 
               Power(-1 + t2,2)*(-19 - 63*t2 + 2*Power(t2,2)) - 
               t1*(267 - 498*t2 + 139*Power(t2,2) + 92*Power(t2,3)) + 
               s2*(286 - 685*Power(t1,3) - 527*t2 + 140*Power(t2,2) + 
                  101*Power(t2,3) + Power(t1,2)*(-415 + 159*t2) + 
                  t1*(551 - 1264*t2 + 521*Power(t2,2)))) + 
            Power(s1,4)*(36*Power(s2,5) - 94*Power(t1,5) + 
               Power(t1,4)*(329 - 89*t2) + 
               Power(s2,4)*(254 - 238*t1 - 71*t2) + 
               Power(t1,3)*(693 - 1055*t2 + 277*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-159 + 250*t2 + 39*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(366 - 753*t2 + 58*Power(t2,2) + 
                  143*Power(t2,3)) + 
               t1*(-275 + 1167*t2 - 1490*Power(t2,2) + 
                  517*Power(t2,3) + 81*Power(t2,4)) + 
               Power(s2,3)*(-715 + 592*Power(t1,2) + 1297*t2 - 
                  427*Power(t2,2) + t1*(-1115 + 326*t2)) + 
               Power(s2,2)*(363 - 708*Power(t1,3) + 
                  Power(t1,2)*(1797 - 528*t2) - 333*t2 - 
                  781*Power(t2,2) + 565*Power(t2,3) + 
                  t1*(2015 - 3433*t2 + 1023*Power(t2,2))) + 
               s2*(317 + 412*Power(t1,4) - 1347*t2 + 1692*Power(t2,2) - 
                  549*Power(t2,3) - 113*Power(t2,4) + 
                  Power(t1,3)*(-1265 + 362*t2) + 
                  Power(t1,2)*(-1993 + 3191*t2 - 873*Power(t2,2)) + 
                  t1*(-705 + 1026*t2 + 771*Power(t2,2) - 720*Power(t2,3))\
)) - Power(s1,3)*(44*Power(s2,6) - 11*Power(t1,6) + 
               Power(t1,5)*(308 - 56*t2) - 
               Power(s2,5)*(133 + 209*t1 + 53*t2) + 
               Power(t1,4)*(-247 + 41*t2 + 146*Power(t2,2)) + 
               Power(t1,3)*(-1442 + 2257*t2 - 1010*Power(t2,2) + 
                  186*Power(t2,3)) + 
               Power(t1,2)*(-775 + 3498*t2 - 4374*Power(t2,2) + 
                  1540*Power(t2,3) - 71*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (211 - 423*t2 + 417*Power(t2,2) - 21*Power(t2,3) + 
                  10*Power(t2,4)) + 
               t1*(56 + 483*t2 - 766*Power(t2,2) - 21*Power(t2,3) + 
                  182*Power(t2,4) + 66*Power(t2,5)) + 
               Power(s2,4)*(-467 + 385*Power(t1,2) + 222*t2 + 
                  138*Power(t2,2) + 12*t1*(71 + 12*t2)) - 
               Power(s2,3)*(-1153 + 330*Power(t1,3) + 2553*t2 - 
                  2273*Power(t2,2) + 790*Power(t2,3) + 
                  Power(t1,2)*(2066 + 58*t2) + 
                  t1*(-1816 + 1043*t2 + 392*Power(t2,2))) + 
               Power(s2,2)*(-934 + 110*Power(t1,4) + 
                  Power(t1,3)*(2416 - 160*t2) + 3959*t2 - 
                  4210*Power(t2,2) + 509*Power(t2,3) + 
                  494*Power(t2,4) + 
                  3*Power(t1,2)*(-826 + 487*t2 + 172*Power(t2,2)) + 
                  t1*(-3576 + 6739*t2 - 4836*Power(t2,2) + 
                     1498*Power(t2,3))) + 
               s2*(30 + 11*Power(t1,5) - 518*t2 + 340*Power(t2,2) + 
                  579*Power(t2,3) - 310*Power(t2,4) - 121*Power(t2,5) + 
                  3*Power(t1,4)*(-459 + 61*t2) + 
                  Power(t1,3)*(1376 - 681*t2 - 408*Power(t2,2)) + 
                  Power(t1,2)*
                   (3865 - 6443*t2 + 3573*Power(t2,2) - 894*Power(t2,3)) \
+ t1*(1605 - 7117*t2 + 8188*Power(t2,2) - 1861*Power(t2,3) - 
                     451*Power(t2,4)))) + 
            Power(s1,2)*(-12*Power(s2,7) - Power(t1,6)*(-53 + t2) + 
               Power(s2,6)*(-74 + 72*t1 + 91*t2) + 
               Power(t1,5)*(-443 + 317*t2 + 39*Power(t2,2)) + 
               Power(t1,4)*(-703 + 1145*t2 - 532*Power(t2,2) + 
                  160*Power(t2,3)) + 
               Power(t1,3)*(2063 - 2758*t2 + 290*Power(t2,2) + 
                  686*Power(t2,3) - 90*Power(t2,4)) + 
               Power(t1,2)*(2552 - 6257*t2 + 6835*Power(t2,2) - 
                  4368*Power(t2,3) + 1264*Power(t2,4) - 88*Power(t2,5)) \
+ Power(-1 + t2,2)*(-24 + 30*t2 + 223*Power(t2,2) - 26*Power(t2,3) + 
                  13*Power(t2,4) + 6*Power(t2,5)) + 
               t1*(786 - 3528*t2 + 7074*Power(t2,2) - 
                  6896*Power(t2,3) + 2996*Power(t2,4) - 
                  490*Power(t2,5) + 58*Power(t2,6)) - 
               Power(s2,5)*(-547 + 180*Power(t1,2) + 433*t2 + 
                  35*Power(t2,2) + t1*(-317 + 454*t2)) + 
               Power(s2,4)*(16 + 240*Power(t1,3) + 335*t2 - 
                  632*Power(t2,2) + 392*Power(t2,3) + 
                  5*Power(t1,2)*(-95 + 181*t2) + 
                  t1*(-2691 + 2169*t2 + 119*Power(t2,2))) - 
               Power(s2,3)*(2079 + 180*Power(t1,4) - 2790*t2 + 
                  1352*Power(t2,2) - 1064*Power(t2,3) + 
                  588*Power(t2,4) + 30*Power(t1,3)*(-7 + 30*t2) + 
                  2*Power(t1,2)*(-2617 + 2113*t2 + 93*Power(t2,2)) + 
                  t1*(-339 + 1178*t2 - 1432*Power(t2,2) + 
                     996*Power(t2,3))) + 
               Power(s2,2)*(2441 + 72*Power(t1,5) - 6889*t2 + 
                  9056*Power(t2,2) - 6040*Power(t2,3) + 
                  1080*Power(t2,4) + 290*Power(t2,5) + 
                  5*Power(t1,4)*(32 + 89*t2) + 
                  2*Power(t1,3)*(-2513 + 1997*t2 + 97*Power(t2,2)) + 
                  Power(t1,2)*
                   (-1429 + 2496*t2 - 1500*Power(t2,2) + 
                     976*Power(t2,3)) + 
                  t1*(6385 - 8478*t2 + 2502*Power(t2,2) - 
                     694*Power(t2,3) + 806*Power(t2,4))) - 
               s2*(911 + 12*Power(t1,6) - 4021*t2 + 7498*Power(t2,2) - 
                  6458*Power(t2,3) + 2235*Power(t2,4) - 
                  263*Power(t2,5) + 98*Power(t2,6) + 
                  Power(t1,5)*(191 + 86*t2) + 
                  Power(t1,4)*(-2379 + 1821*t2 + 131*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1777 + 2798*t2 - 1232*Power(t2,2) + 
                     532*Power(t2,3)) + 
                  Power(t1,2)*
                   (6369 - 8446*t2 + 1440*Power(t2,2) + 
                     1056*Power(t2,3) + 128*Power(t2,4)) + 
                  t1*(4925 - 12770*t2 + 15175*Power(t2,2) - 
                     9824*Power(t2,3) + 2160*Power(t2,4) + 
                     210*Power(t2,5)))) + 
            (-1 + t2)*(-947 + 24*Power(s2,6)*(-1 + t2) + 3997*t2 - 
               6402*Power(t2,2) + 4998*Power(t2,3) - 2175*Power(t2,4) + 
               663*Power(t2,5) - 142*Power(t2,6) + 8*Power(t2,7) + 
               Power(s2,5)*(26 - 120*t1*(-1 + t2) - 31*t2 + 
                  14*Power(t2,2) - 12*Power(t2,3)) + 
               Power(t1,5)*(-116 + 135*t2 - 18*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,4)*(376 - 439*t2 + 48*Power(t2,2) + 
                  41*Power(t2,3) - 2*Power(t2,4)) + 
               Power(t1,3)*(905 - 2106*t2 + 1317*Power(t2,2) - 
                  217*Power(t2,3) + 40*Power(t2,4) - 2*Power(t2,5)) + 
               Power(t1,2)*(-1335 + 1234*t2 + 1798*Power(t2,2) - 
                  2447*Power(t2,3) + 893*Power(t2,4) - 
                  103*Power(t2,5) + 2*Power(t2,6)) + 
               t1*(-2623 + 7125*t2 - 5709*Power(t2,2) + 
                  578*Power(t2,3) + 840*Power(t2,4) - 249*Power(t2,5) + 
                  32*Power(t2,6)) + 
               Power(s2,4)*(527 + 240*Power(t1,2)*(-1 + t2) - 656*t2 + 
                  91*Power(t2,2) + 2*Power(t2,3) + 60*Power(t2,4) + 
                  t1*(-190 + 169*t2 + 16*Power(t2,2) + 20*Power(t2,3))) \
- Power(s2,3)*(562 + 240*Power(t1,3)*(-1 + t2) - 1905*t2 + 
                  1914*Power(t2,2) - 682*Power(t2,3) - 12*Power(t2,4) + 
                  60*Power(t2,5) - 
                  2*Power(t1,2)*
                   (265 - 228*t2 - 57*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(1939 - 2275*t2 + 33*Power(t2,2) + 
                     299*Power(t2,3) + 100*Power(t2,4))) + 
               Power(s2,2)*(-1506 + 120*Power(t1,4)*(-1 + t2) + 
                  1330*t2 + 2273*Power(t2,2) - 2770*Power(t2,3) + 
                  655*Power(t2,4) + 48*Power(t2,5) + 12*Power(t2,6) + 
                  Power(t1,3)*
                   (-710 + 664*t2 + 106*Power(t2,2) - 30*Power(t2,3)) + 
                  3*Power(t1,2)*
                   (891 - 1007*t2 - 53*Power(t2,2) + 211*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  t1*(1829 - 5210*t2 + 4261*Power(t2,2) - 
                     1165*Power(t2,3) + 12*Power(t2,4) + 84*Power(t2,5))\
) - s2*(-2472 + 24*Power(t1,5)*(-1 + t2) + 6687*t2 - 5291*Power(t2,2) + 
                  446*Power(t2,3) + 903*Power(t2,4) - 367*Power(t2,5) + 
                  88*Power(t2,6) + 
                  Power(t1,4)*
                   (-460 + 481*t2 + 4*Power(t2,2) - 10*Power(t2,3)) + 
                  Power(t1,3)*
                   (1637 - 1841*t2 - 53*Power(t2,2) + 377*Power(t2,3) - 
                     24*Power(t2,4)) + 
                  Power(t1,2)*
                   (2172 - 5411*t2 + 3664*Power(t2,2) - 
                     700*Power(t2,3) + 64*Power(t2,4) + 22*Power(t2,5)) \
+ t1*(-2971 + 3012*t2 + 3547*Power(t2,2) - 5031*Power(t2,3) + 
                     1604*Power(t2,4) - 89*Power(t2,5) + 12*Power(t2,6)))\
) + s1*(12*Power(s2,7)*(-1 + t2) + 
               Power(s2,6)*(30 - 72*t1*(-1 + t2) - 10*t2 - 
                  21*Power(t2,2)) + 
               Power(t1,6)*(45 - 43*t2 - 3*Power(t2,2)) + 
               Power(t1,5)*(-43 + 51*t2 + 15*Power(t2,2) - 
                  40*Power(t2,3)) + 
               Power(t1,4)*(-1330 + 2545*t2 - 1359*Power(t2,2) + 
                  210*Power(t2,3) - 31*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (-968 + 2038*t2 - 1711*Power(t2,2) + 1077*Power(t2,3) - 
                  368*Power(t2,4) + 42*Power(t2,5)) + 
               Power(t1,3)*(291 + 709*t2 - 2404*Power(t2,2) + 
                  2230*Power(t2,3) - 943*Power(t2,4) + 102*Power(t2,5)) + 
               Power(t1,2)*(3807 - 8441*t2 + 4989*Power(t2,2) + 
                  212*Power(t2,3) - 626*Power(t2,4) + 69*Power(t2,5) - 
                  12*Power(t2,6)) + 
               t1*(3230 - 10373*t2 + 13189*Power(t2,2) - 
                  9537*Power(t2,3) + 4971*Power(t2,4) - 
                  1730*Power(t2,5) + 266*Power(t2,6) - 16*Power(t2,7)) + 
               Power(s2,5)*(415 + 180*Power(t1,2)*(-1 + t2) - 648*t2 + 
                  192*Power(t2,2) + 58*Power(t2,3) + 
                  3*t1*(-65 + 31*t2 + 36*Power(t2,2))) - 
               Power(s2,4)*(1045 + 240*Power(t1,3)*(-1 + t2) - 2224*t2 + 
                  1629*Power(t2,2) - 744*Power(t2,3) + 259*Power(t2,4) + 
                  15*Power(t1,2)*(-35 + 21*t2 + 15*Power(t2,2)) + 
                  t1*(1783 - 2883*t2 + 993*Power(t2,2) + 192*Power(t2,3))\
) + Power(s2,3)*(-1021 + 180*Power(t1,4)*(-1 + t2) + 228*t2 + 
                  2743*Power(t2,2) - 2454*Power(t2,3) + 
                  303*Power(t2,4) + 216*Power(t2,5) + 
                  10*Power(t1,3)*(-75 + 53*t2 + 24*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (1451 - 2406*t2 + 906*Power(t2,2) + 134*Power(t2,3)) \
+ t1*(4275 - 8377*t2 + 4866*Power(t2,2) - 1442*Power(t2,3) + 
                     538*Power(t2,4))) - 
               Power(s2,2)*(-3734 + 72*Power(t1,5)*(-1 + t2) + 8326*t2 - 
                  5816*Power(t2,2) + 2031*Power(t2,3) - 
                  1128*Power(t2,4) + 185*Power(t2,5) + 138*Power(t2,6) + 
                  15*Power(t1,4)*(-40 + 32*t2 + 9*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (1079 - 1809*t2 + 699*Power(t2,2) + 116*Power(t2,3)) \
+ Power(t1,2)*(6745 - 12627*t2 + 6204*Power(t2,2) - 862*Power(t2,3) + 
                     330*Power(t2,4)) + 
                  t1*(-2779 + 1267*t2 + 6150*Power(t2,2) - 
                     6578*Power(t2,3) + 1799*Power(t2,4) + 
                     186*Power(t2,5))) + 
               s2*(-3095 + 12*Power(t1,6)*(-1 + t2) + 10123*t2 - 
                  13310*Power(t2,2) + 9858*Power(t2,3) - 
                  4711*Power(t2,4) + 1179*Power(t2,5) - 68*Power(t2,6) + 
                  24*Power(t2,7) + 
                  3*Power(t1,5)*(-85 + 75*t2 + 12*Power(t2,2)) + 
                  Power(t1,4)*
                   (667 - 1092*t2 + 372*Power(t2,2) + 138*Power(t2,3)) + 
                  Power(t1,3)*
                   (4845 - 9019*t2 + 4326*Power(t2,2) - 
                     374*Power(t2,3) + 82*Power(t2,4)) - 
                  3*Power(t1,2)*
                   (683 - 110*t2 - 1937*Power(t2,2) + 2118*Power(t2,3) - 
                     813*Power(t2,4) + 44*Power(t2,5)) + 
                  t1*(-7679 + 17177*t2 - 11115*Power(t2,2) + 
                     1679*Power(t2,3) - 252*Power(t2,4) + 
                     54*Power(t2,5) + 140*Power(t2,6))))))*
       R1q(1 - s + s1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*Power(-1 + s + t2,3)*(-1 + s - s1 + t2)*
       (s - s1 + t2)*(-1 + s2 - t1 + t2)*
       (-1 + s - s*s1 + s1*s2 - s1*t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) + 
    (8*(101 - 245*s2 + 97*Power(s2,2) + 67*Power(s2,3) - 14*Power(s2,4) - 
         6*Power(s2,5) - 2*Power(s1,5)*(-1 + s2)*(s2 - t1) + 154*t1 - 
         81*s2*t1 - 192*Power(s2,2)*t1 + 31*Power(s2,3)*t1 + 
         24*Power(s2,4)*t1 - 18*Power(t1,2) + 185*s2*Power(t1,2) - 
         7*Power(s2,2)*Power(t1,2) - 38*Power(s2,3)*Power(t1,2) - 
         60*Power(t1,3) - 23*s2*Power(t1,3) + 
         30*Power(s2,2)*Power(t1,3) + 13*Power(t1,4) - 
         12*s2*Power(t1,4) + 2*Power(t1,5) - 213*t2 + 405*s2*t2 - 
         40*Power(s2,2)*t2 - 89*Power(s2,3)*t2 - 5*Power(s2,4)*t2 + 
         6*Power(s2,5)*t2 - 188*t1*t2 - 62*s2*t1*t2 + 
         204*Power(s2,2)*t1*t2 + 30*Power(s2,3)*t1*t2 - 
         20*Power(s2,4)*t1*t2 + 120*Power(t1,2)*t2 - 
         159*s2*Power(t1,2)*t2 - 64*Power(s2,2)*Power(t1,2)*t2 + 
         24*Power(s2,3)*Power(t1,2)*t2 + 44*Power(t1,3)*t2 + 
         58*s2*Power(t1,3)*t2 - 12*Power(s2,2)*Power(t1,3)*t2 - 
         19*Power(t1,4)*t2 + 2*s2*Power(t1,4)*t2 + 71*Power(t2,2) - 
         61*s2*Power(t2,2) - 116*Power(s2,2)*Power(t2,2) + 
         23*Power(s2,3)*Power(t2,2) + 17*Power(s2,4)*Power(t2,2) - 
         2*Power(s2,5)*Power(t2,2) - 82*t1*Power(t2,2) + 
         209*s2*t1*Power(t2,2) - 2*Power(s2,2)*t1*Power(t2,2) - 
         57*Power(s2,3)*t1*Power(t2,2) + 6*Power(s2,4)*t1*Power(t2,2) - 
         125*Power(t1,2)*Power(t2,2) - 39*s2*Power(t1,2)*Power(t2,2) + 
         69*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         6*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         18*Power(t1,3)*Power(t2,2) - 35*s2*Power(t1,3)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         6*Power(t1,4)*Power(t2,2) + 77*Power(t2,3) - 
         119*s2*Power(t2,3) + 53*Power(s2,2)*Power(t2,3) + 
         3*Power(s2,3)*Power(t2,3) + 2*Power(s2,4)*Power(t2,3) + 
         136*t1*Power(t2,3) - 62*s2*t1*Power(t2,3) - 
         22*Power(s2,2)*t1*Power(t2,3) - 4*Power(s2,3)*t1*Power(t2,3) + 
         25*Power(t1,2)*Power(t2,3) + 25*s2*Power(t1,2)*Power(t2,3) + 
         2*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         6*Power(t1,3)*Power(t2,3) - 36*Power(t2,4) + 20*s2*Power(t2,4) + 
         8*Power(s2,2)*Power(t2,4) - 20*t1*Power(t2,4) - 
         8*s2*t1*Power(t2,4) + 
         Power(s,5)*(4 - 6*s2 + t1 + 4*Power(t1,2) + 
            s1*(-4 + 6*s2 - t1 - t2) + t2 - 8*t1*t2 + 4*Power(t2,2)) + 
         Power(s1,4)*(-5*Power(s2,3) + Power(t1,3) + 
            Power(t1,2)*(13 - 3*t2) - 2*t1*(-4 + t2) + 2*(-1 + t2) + 
            Power(s2,2)*(11 + 9*t1 + 7*t2) - 
            s2*(6 + 5*Power(t1,2) + 4*t1*(6 + t2))) + 
         Power(s1,3)*(-2*Power(s2,4) + Power(t1,3) - 2*Power(t1,4) + 
            Power(s2,3)*(28 + 4*t1 + 21*t2) + 
            2*(4 - 5*t2 + Power(t2,2)) + 
            Power(t1,2)*(-37 - 27*t2 + 6*Power(t2,2)) + 
            2*t1*(-4 - 14*t2 + 7*Power(t2,2)) - 
            Power(s2,2)*(60 + 4*Power(t1,2) + 21*t2 + 5*Power(t2,2) + 
               t1*(59 + 32*t2)) + 
            s2*(10 + 4*Power(t1,3) + 16*t2 - 4*Power(t2,2) + 
               Power(t1,2)*(30 + 11*t2) + t1*(101 + 42*t2 + Power(t2,2)))\
) + Power(s,4)*(9 + 12*Power(s2,2) - 5*t1 - 7*Power(t1,2) + 
            10*Power(t1,3) + 21*t2 + 13*t1*t2 - 18*Power(t1,2)*t2 + 
            2*Power(t2,2) + 6*t1*Power(t2,2) + 2*Power(t2,3) + 
            Power(s1,2)*(16 - 23*s2 + 5*t1 + 2*t2) - 
            s1*(29 + 18*Power(s2,2) + 13*t1 + 12*Power(t1,2) + 12*t2 - 
               15*t1*t2 + 11*Power(t2,2) - s2*(49 + 14*t1 + 17*t2)) + 
            s2*(t1 - 10*Power(t1,2) + 28*t1*t2 - 
               2*(8 + 15*t2 + 9*Power(t2,2)))) + 
         Power(s,3)*(4 + 13*t1 - 40*Power(t1,2) - 23*Power(t1,3) + 
            8*Power(t1,4) + Power(s1,3)*(-22 + 32*s2 - 9*t1 - t2) + 
            52*t2 + 84*t1*t2 + 46*Power(t1,2)*t2 - 12*Power(t1,3)*t2 - 
            14*Power(t2,2) - 26*t1*Power(t2,2) + 15*Power(t2,3) + 
            4*t1*Power(t2,3) + 
            Power(s2,2)*(2 + 8*Power(t1,2) + 80*t2 + 32*Power(t2,2) - 
               3*t1*(7 + 12*t2)) + 
            Power(s1,2)*(58 + 56*Power(s2,2) + 14*Power(t1,2) + 
               t1*(46 - 5*t2) + 19*t2 + 10*Power(t2,2) - 
               s2*(109 + 48*t1 + 41*t2)) - 
            s2*(25 + 16*Power(t1,3) + 110*t2 + 11*Power(t2,2) + 
               8*Power(t2,3) - 4*Power(t1,2)*(11 + 12*t2) + 
               2*t1*(-13 + 50*t2 + 12*Power(t2,2))) + 
            s1*(-33 + 12*Power(s2,3) - 24*Power(t1,3) - 93*t2 + 
               13*Power(t2,2) - 8*Power(t2,3) + 
               Power(t1,2)*(28 + 25*t2) - 
               Power(s2,2)*(65 + 10*t1 + 64*t2) - 
               t1*(26 + 92*t2 + 5*Power(t2,2)) + 
               s2*(90 + 19*t1 + 22*Power(t1,2) + 168*t2 + 13*t1*t2 + 
                  30*Power(t2,2)))) + 
         Power(s1,2)*(-21 + 7*Power(s2,5) + Power(t1,5) + 9*t2 + 
            23*Power(t2,2) - 11*Power(t2,3) + Power(t1,4)*(-19 + 9*t2) + 
            Power(s2,4)*(17 - 29*t1 + 33*t2) + 
            Power(t1,2)*(58 + 64*t2 - 18*Power(t2,2)) + 
            Power(t1,3)*(35 + 50*t2 - 12*Power(t2,2)) - 
            2*t1*(17 - 34*t2 - 5*Power(t2,2) + 5*Power(t2,3)) + 
            Power(s2,3)*(-131 + 44*Power(t1,2) - 54*t2 - 
               4*t1*(8 + 23*t2)) + 
            Power(s2,2)*(87 - 28*Power(t1,3) + 44*t2 + 7*Power(t2,2) + 
               6*Power(t2,3) + Power(t1,2)*(-6 + 94*t2) + 
               t1*(297 + 158*t2 - 18*Power(t2,2))) + 
            s2*(59 + 5*Power(t1,4) + Power(t1,3)*(40 - 44*t2) - 103*t2 + 
               13*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,2)*(-201 - 154*t2 + 30*Power(t2,2)) - 
               t1*(141 + 124*t2 - 25*Power(t2,2) + 8*Power(t2,3)))) + 
         s1*(6 + 6*Power(s2,6) + Power(t1,5)*(15 - 6*t2) + 48*t2 - 
            76*Power(t2,2) + 18*Power(t2,3) + 4*Power(t2,4) + 
            Power(s2,5)*(14 - 30*t1 + 19*t2) + 
            Power(t1,3)*(-37 + 128*t2 - 34*Power(t2,2)) + 
            Power(t1,4)*(-55 - 13*t2 + 6*Power(t2,2)) + 
            2*t1*(79 - 51*t2 - 63*Power(t2,2) + 27*Power(t2,3)) + 
            Power(t1,2)*(185 - 19*t2 - 176*Power(t2,2) + 
               32*Power(t2,3)) + 
            Power(s2,4)*(-74 + 60*Power(t1,2) + 29*t2 + 3*Power(t2,2) - 
               t1*(45 + 76*t2)) + 
            Power(s2,3)*(-114 - 60*Power(t1,3) - 73*t2 + 
               92*Power(t2,2) + 8*Power(t2,3) + 
               12*Power(t1,2)*(3 + 10*t2) + 
               t1*(281 - 64*t2 - 25*Power(t2,2))) + 
            Power(s2,2)*(378 + 30*Power(t1,4) + 
               Power(t1,3)*(22 - 94*t2) - 241*t2 - 157*Power(t2,2) + 
               24*Power(t2,3) + 
               Power(t1,2)*(-395 + 28*t2 + 47*Power(t2,2)) + 
               t1*(193 + 244*t2 - 188*Power(t2,2) - 16*Power(t2,3))) + 
            s2*(-216 - 6*Power(t1,5) + 190*t2 + 92*Power(t2,2) - 
               58*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,4)*(-42 + 37*t2) + 
               Power(t1,3)*(243 + 20*t2 - 31*Power(t2,2)) + 
               t1*(-565 + 268*t2 + 329*Power(t2,2) - 58*Power(t2,3)) + 
               Power(t1,2)*(-42 - 299*t2 + 130*Power(t2,2) + 
                  8*Power(t2,3)))) - 
         Power(s,2)*(-24 + 12*Power(s2,4) + 
            Power(s1,4)*(-12 + 19*s2 - 7*t1) - 149*t1 - 
            123*Power(t1,2) + 39*Power(t1,3) + 19*Power(t1,4) - 
            2*Power(t1,5) + 88*t2 + 131*t1*t2 - 46*Power(t1,2)*t2 - 
            43*Power(t1,3)*t2 + 2*Power(t1,4)*t2 - 143*Power(t2,2) - 
            66*t1*Power(t2,2) + 22*Power(t1,2)*Power(t2,2) + 
            2*Power(t1,3)*Power(t2,2) + 39*Power(t2,3) + 
            2*t1*Power(t2,3) - 2*Power(t1,2)*Power(t2,3) - 
            8*Power(t2,4) + Power(s2,3)*
             (10 + 2*Power(t1,2) + 68*t2 + 28*Power(t2,2) - 
               t1*(43 + 20*t2)) + 
            Power(s1,3)*(41 + 58*Power(s2,2) + 8*Power(t1,2) + 8*t2 + 
               3*Power(t2,2) + 5*t1*(11 + t2) - s2*(91 + 54*t1 + 33*t2)) \
- Power(s2,2)*(6*Power(t1,3) - 3*Power(t1,2)*(23 + 14*t2) + 
               t1*(9 + 161*t2 + 36*Power(t2,2)) + 
               3*(51 + 12*t2 + 11*Power(t2,2) + 4*Power(t2,3))) + 
            s2*(137 + 6*Power(t1,4) - 127*t2 + 105*Power(t2,2) + 
               27*Power(t2,3) - 3*Power(t1,3)*(19 + 8*t2) + 
               2*Power(t1,2)*(-20 + 68*t2 + 3*Power(t2,2)) + 
               t1*(288 + 90*t2 - 19*Power(t2,2) + 12*Power(t2,3))) + 
            Power(s1,2)*(-35 + 36*Power(s2,3) - 20*Power(t1,3) - 
               99*t2 + 24*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,2)*(2 + 13*t2) - 
               Power(s2,2)*(121 + 48*t1 + 109*t2) - 
               t1*(92 + 111*t2 + 2*Power(t2,2)) + 
               s2*(145 + 32*Power(t1,2) + 192*t2 + 20*Power(t2,2) + 
                  t1*(105 + 74*t2))) + 
            s1*(31 - 12*Power(s2,4) + 16*Power(t1,4) + 
               Power(s2,3)*(-21 + 48*t1 - 100*t2) + 42*t2 + 
               47*Power(t2,2) + 4*Power(t2,3) + 
               Power(t1,3)*(-84 + 5*t2) + 
               Power(t1,2)*(40 + 177*t2 - 29*Power(t2,2)) + 
               t1*(183 + 109*t2 - 54*Power(t2,2) + 16*Power(t2,3)) - 
               s2*(221 + 8*Power(t1,3) + 127*t2 + 42*Power(t2,2) + 
                  24*Power(t2,3) + Power(t1,2)*(-145 + 52*t2) + 
                  t1*(271 + 388*t2 - 15*Power(t2,2))) + 
               Power(s2,2)*(-44*Power(t1,2) + t1*(-40 + 147*t2) + 
                  3*(67 + 89*t2 + 8*Power(t2,2))))) + 
         s*(-142 + 6*Power(s2,5) - 310*t1 - 56*Power(t1,2) + 
            114*Power(t1,3) - 2*Power(t1,4) - 4*Power(t1,5) + 
            Power(s1,5)*(4*s2 - 2*(1 + t1)) + 199*t2 + 230*t1*t2 - 
            134*Power(t1,2)*t2 - 56*Power(t1,3)*t2 + 9*Power(t1,4)*t2 - 
            62*Power(t2,2) + 98*t1*Power(t2,2) + 
            134*Power(t1,2)*Power(t2,2) + 12*Power(t1,3)*Power(t2,2) + 
            33*Power(t2,3) - 42*t1*Power(t2,3) - 
            23*Power(t1,2)*Power(t2,3) - 12*Power(t2,4) + 
            8*t1*Power(t2,4) + 
            Power(s2,4)*(26 + 11*t2 + 12*Power(t2,2) - 4*t1*(6 + t2)) + 
            Power(s2,3)*(-59 + 66*t2 - 41*Power(t2,2) - 8*Power(t2,3) + 
               4*Power(t1,2)*(10 + 3*t2) - 
               2*t1*(37 + 27*t2 + 12*Power(t2,2))) + 
            Power(s2,2)*(-12*Power(t1,3)*(3 + t2) + 
               4*Power(t1,2)*(17 + 21*t2 + 3*Power(t2,2)) + 
               3*(-86 + 8*t2 + 40*Power(t2,2) + 3*Power(t2,3)) + 
               2*t1*(105 - 64*t2 + 32*Power(t2,2) + 6*Power(t2,3))) + 
            s2*(427 - 396*t2 - 27*Power(t2,2) + 10*Power(t2,3) - 
               16*Power(t2,4) + 2*Power(t1,4)*(9 + 2*t2) - 
               2*Power(t1,3)*(9 + 25*t2) - 
               Power(t1,2)*(265 - 118*t2 + 35*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(330 + 64*t2 - 232*Power(t2,2) + 24*Power(t2,3))) + 
            Power(s1,4)*(8 + 22*Power(s2,2) + 2*Power(t1,2) + 
               t1*(23 + 3*t2) - s2*(22*t1 + 9*(3 + t2))) + 
            Power(s1,3)*(-15 + 28*Power(s2,3) - 7*Power(t1,3) - 18*t2 + 
               5*Power(t2,2) + Power(t1,2)*(-30 + 8*t2) - 
               Power(s2,2)*(79 + 45*t1 + 53*t2) - 
               t1*(73 + 28*t2 + 3*Power(t2,2)) + 
               s2*(84 + 24*Power(t1,2) + 48*t2 + 8*Power(t2,2) + 
                  t1*(107 + 39*t2))) + 
            Power(s1,2)*(2 - 4*Power(s2,4) + 10*Power(t1,4) + 
               Power(s2,3)*(-53 + 24*t1 - 103*t2) + 79*t2 - 
               30*Power(t2,2) + 5*Power(t2,3) + 
               Power(t1,3)*(-41 + 9*t2) + 
               Power(t1,2)*(86 + 137*t2 - 26*Power(t2,2)) + 
               t1*(109 + 175*t2 - 62*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(228 - 26*Power(t1,2) + 213*t2 + 
                  10*Power(t2,2) + t1*(65 + 171*t2)) - 
               s2*(160 + 4*Power(t1,3) + 139*t2 - 13*Power(t2,2) + 
                  12*Power(t2,3) + Power(t1,2)*(-29 + 77*t2) + 
                  t1*(330 + 322*t2 - 16*Power(t2,2)))) - 
            s1*(-121 + 18*Power(s2,5) + 3*Power(t1,5) + 132*t2 + 
               105*Power(t2,2) - 74*Power(t2,3) + 8*Power(t2,4) + 
               4*Power(t1,4)*(-17 + 5*t2) + 
               Power(s2,4)*(15 - 75*t1 + 71*t2) + 
               Power(t1,3)*(88 + 120*t2 - 29*Power(t2,2)) + 
               t1*(-101 + 40*t2 + 241*Power(t2,2) - 46*Power(t2,3)) + 
               Power(t1,2)*(211 - 104*t2 - Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(184 - 72*Power(t1,3) - 103*t2 + 
                  147*Power(t2,2) + 24*Power(t2,3) + 
                  Power(t1,2)*(-115 + 197*t2) + 
                  t1*(610 + 244*t2 - 45*Power(t2,2))) + 
               Power(s2,3)*(t1 + 114*Power(t1,2) - 195*t1*t2 - 
                  2*(121 + 41*t2 + Power(t2,2))) + 
               s2*(12*Power(t1,4) + Power(t1,3)*(167 - 93*t2) + 
                  Power(t1,2)*(-456 - 282*t2 + 76*Power(t2,2)) + 
                  2*(91 - 71*t2 - 126*Power(t2,2) + 10*Power(t2,3)) - 
                  t1*(401 - 185*t2 + 110*Power(t2,2) + 32*Power(t2,3))))))*
       R2q(1 - s2 + t1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(s - s1 + t2)*(-1 + s2 - t1 + t2)*
       (-1 + s - s*s1 + s1*s2 - s1*t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) - 
    (8*(-156 + 128*s2 + 171*Power(s2,2) - 101*Power(s2,3) - 
         55*Power(s2,4) + 9*Power(s2,5) + 4*Power(s2,6) - 119*t1 - 
         255*s2*t1 + 181*Power(s2,2)*t1 + 206*Power(s2,3)*t1 - 
         12*Power(s2,4)*t1 - 17*Power(s2,5)*t1 + 87*Power(t1,2) - 
         88*s2*Power(t1,2) - 265*Power(s2,2)*Power(t1,2) - 
         31*Power(s2,3)*Power(t1,2) + 19*Power(s2,4)*Power(t1,2) - 
         2*Power(s2,5)*Power(t1,2) + 8*Power(t1,3) + 132*s2*Power(t1,3) + 
         75*Power(s2,2)*Power(t1,3) + 14*Power(s2,3)*Power(t1,3) + 
         10*Power(s2,4)*Power(t1,3) - 18*Power(t1,4) - 
         54*s2*Power(t1,4) - 46*Power(s2,2)*Power(t1,4) - 
         20*Power(s2,3)*Power(t1,4) + 13*Power(t1,5) + 
         35*s2*Power(t1,5) + 20*Power(s2,2)*Power(t1,5) - 9*Power(t1,6) - 
         10*s2*Power(t1,6) + 2*Power(t1,7) + 540*t2 - 178*s2*t2 - 
         602*Power(s2,2)*t2 + 154*Power(s2,3)*t2 + 132*Power(s2,4)*t2 - 
         20*Power(s2,5)*t2 - 10*Power(s2,6)*t2 + 216*t1*t2 + 
         821*s2*t1*t2 - 127*Power(s2,2)*t1*t2 - 488*Power(s2,3)*t1*t2 + 
         36*Power(s2,4)*t1*t2 + 59*Power(s2,5)*t1*t2 + 
         3*Power(s2,6)*t1*t2 - 237*Power(t1,2)*t2 - 
         100*s2*Power(t1,2)*t2 + 608*Power(s2,2)*Power(t1,2)*t2 + 
         28*Power(s2,3)*Power(t1,2)*t2 - 131*Power(s2,4)*Power(t1,2)*t2 - 
         14*Power(s2,5)*Power(t1,2)*t2 + 73*Power(t1,3)*t2 - 
         280*s2*Power(t1,3)*t2 - 100*Power(s2,2)*Power(t1,3)*t2 + 
         134*Power(s2,3)*Power(t1,3)*t2 + 25*Power(s2,4)*Power(t1,3)*t2 + 
         28*Power(t1,4)*t2 + 72*s2*Power(t1,4)*t2 - 
         56*Power(s2,2)*Power(t1,4)*t2 - 20*Power(s2,3)*Power(t1,4)*t2 - 
         16*Power(t1,5)*t2 - s2*Power(t1,5)*t2 + 
         5*Power(s2,2)*Power(t1,5)*t2 + 5*Power(t1,6)*t2 + 
         2*s2*Power(t1,6)*t2 - Power(t1,7)*t2 - 666*Power(t2,2) - 
         147*s2*Power(t2,2) + 669*Power(s2,2)*Power(t2,2) - 
         9*Power(s2,3)*Power(t2,2) - 92*Power(s2,4)*Power(t2,2) + 
         5*Power(s2,5)*Power(t2,2) + Power(s2,6)*Power(t2,2) - 
         Power(s2,7)*Power(t2,2) - 32*t1*Power(t2,2) - 
         803*s2*t1*Power(t2,2) - 282*Power(s2,2)*t1*Power(t2,2) + 
         360*Power(s2,3)*t1*Power(t2,2) + 10*Power(s2,4)*t1*Power(t2,2) - 
         17*Power(s2,5)*t1*Power(t2,2) + 4*Power(s2,6)*t1*Power(t2,2) + 
         163*Power(t1,2)*Power(t2,2) + 454*s2*Power(t1,2)*Power(t2,2) - 
         459*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         67*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         60*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         5*Power(s2,5)*Power(t1,2)*Power(t2,2) - 
         163*Power(t1,3)*Power(t2,2) + 206*s2*Power(t1,3)*Power(t2,2) + 
         91*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         90*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         15*Power(t1,4)*Power(t2,2) - 46*s2*Power(t1,4)*Power(t2,2) + 
         65*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         5*Power(s2,3)*Power(t1,4)*Power(t2,2) + 
         7*Power(t1,5)*Power(t2,2) - 21*s2*Power(t1,5)*Power(t2,2) - 
         4*Power(s2,2)*Power(t1,5)*Power(t2,2) + 
         2*Power(t1,6)*Power(t2,2) + s2*Power(t1,6)*Power(t2,2) + 
         346*Power(t2,3) + 297*s2*Power(t2,3) - 
         199*Power(s2,2)*Power(t2,3) - 70*Power(s2,3)*Power(t2,3) - 
         4*Power(s2,4)*Power(t2,3) + 7*Power(s2,5)*Power(t2,3) + 
         3*Power(s2,6)*Power(t2,3) - 87*t1*Power(t2,3) + 
         167*s2*t1*Power(t2,3) + 306*Power(s2,2)*t1*Power(t2,3) - 
         32*Power(s2,3)*t1*Power(t2,3) - 39*Power(s2,4)*t1*Power(t2,3) - 
         13*Power(s2,5)*t1*Power(t2,3) + 24*Power(t1,2)*Power(t2,3) - 
         340*s2*Power(t1,2)*Power(t2,3) + 
         82*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         80*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         22*Power(s2,4)*Power(t1,2)*Power(t2,3) + 
         104*Power(t1,3)*Power(t2,3) - 52*s2*Power(t1,3)*Power(t2,3) - 
         76*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         18*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         6*Power(t1,4)*Power(t2,3) + 33*s2*Power(t1,4)*Power(t2,3) + 
         7*Power(s2,2)*Power(t1,4)*Power(t2,3) - 
         5*Power(t1,5)*Power(t2,3) - s2*Power(t1,5)*Power(t2,3) - 
         82*Power(t2,4) - 88*s2*Power(t2,4) - 
         80*Power(s2,2)*Power(t2,4) + 30*Power(s2,3)*Power(t2,4) + 
         24*Power(s2,4)*Power(t2,4) + 8*t1*Power(t2,4) + 
         114*s2*t1*Power(t2,4) - 86*Power(s2,2)*t1*Power(t2,4) - 
         66*Power(s2,3)*t1*Power(t2,4) - 48*Power(t1,2)*Power(t2,4) + 
         82*s2*Power(t1,2)*Power(t2,4) + 
         64*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         26*Power(t1,3)*Power(t2,4) - 26*s2*Power(t1,3)*Power(t2,4) + 
         4*Power(t1,4)*Power(t2,4) + 26*Power(t2,5) - 12*s2*Power(t2,5) + 
         42*Power(s2,2)*Power(t2,5) + 14*t1*Power(t2,5) - 
         46*s2*t1*Power(t2,5) - 4*Power(s2,2)*t1*Power(t2,5) + 
         12*Power(t1,2)*Power(t2,5) + 4*s2*Power(t1,2)*Power(t2,5) - 
         8*Power(t2,6) - Power(s1,7)*(s2 - t1)*
          (Power(s2,2) - t1*(1 + t1) + s2*t2) + 
         Power(s,7)*(-2 - 6*t1 + 3*Power(t1,2) + s2*(2 + t1 - t2) + 
            6*t2 - 6*t1*t2 + 3*Power(t2,2) + 
            Power(s1,2)*(-2 + 2*s2 - t1 + t2) + 
            s1*(4 + 7*t1 + Power(t1,2) - 7*t2 - 2*t1*t2 + Power(t2,2) + 
               s2*(-4 - t1 + t2))) + 
         Power(s1,6)*(-2*Power(s2,4) + 3*Power(s2,3)*(1 + t1) + 
            Power(s2,2)*(-9 + Power(t1,2) + 4*t2 + 2*Power(t2,2) + 
               t1*(-8 + 3*t2)) + 
            t1*(-1 + Power(t1,3) + t2 + Power(t1,2)*(-2 + 3*t2) + 
               t1*(-13 + 10*t2)) - 
            s2*(3*Power(t1,3) + (-1 + t2)*t2 + Power(t1,2)*(-7 + 6*t2) + 
               t1*(-23 + 16*t2 + Power(t2,2)))) + 
         Power(s,6)*(6 + 9*t1 - 14*Power(t1,2) + 14*Power(t1,3) + 
            Power(s1,3)*(10 - 11*s2 + 6*t1 - 5*t2) - 3*t2 - 4*t1*t2 - 
            26*Power(t1,2)*t2 + 18*Power(t2,2) + 10*t1*Power(t2,2) + 
            2*Power(t2,3) + Power(s2,2)*(-4 - 4*t1 + 5*t2) - 
            Power(s1,2)*(23 + 9*Power(s2,2) + 7*Power(t1,2) + 
               t1*(38 - 9*t2) - 29*t2 + 2*Power(t2,2) + 
               s2*(-31 - 15*t1 + 5*t2)) - 
            s2*(5 + 10*Power(t1,2) + 28*t2 + 21*Power(t2,2) - 
               t1*(24 + 31*t2)) + 
            s1*(7 + 3*Power(t1,3) + Power(s2,2)*(13 + 5*t1 - 6*t2) + 
               Power(t1,2)*(7 - 5*t2) - 18*t2 - 22*Power(t2,2) + 
               Power(t2,3) + t1*(20 + 15*t2 + Power(t2,2)) - 
               s2*(15 + 8*Power(t1,2) + t1*(37 - 13*t2) - 31*t2 + 
                  5*Power(t2,2)))) + 
         Power(s1,5)*(2*Power(s2,5) + Power(s2,4)*(10 - 10*t1 + 8*t2) + 
            Power(s2,3)*(-68 + 16*Power(t1,2) + 17*t2 + 5*Power(t2,2) - 
               15*t1*(2 + t2)) + 
            t1*(2*Power(t1,4) - Power(t1,3)*(1 + 4*t2) + 
               t1*(38 - 7*t2 - 12*Power(t2,2)) + 
               Power(t1,2)*(42 - 6*t2 - 4*Power(t2,2)) - 
               8*(3 - 5*t2 + 2*Power(t2,2))) - 
            Power(s2,2)*(-41 + 8*Power(t1,3) + 15*t2 + 6*Power(t2,2) + 
               Power(t2,3) - Power(t1,2)*(29 + 2*t2) + 
               2*t1*(-92 + 25*t2 + 5*Power(t2,2))) + 
            s2*(21 - 2*Power(t1,4) - 33*t2 + 11*Power(t2,2) + 
               Power(t2,3) + Power(t1,3)*(-8 + 9*t2) + 
               Power(t1,2)*(-158 + 39*t2 + 9*Power(t2,2)) + 
               t1*(-80 + 23*t2 + 19*Power(t2,2)))) + 
         Power(s1,4)*(10*Power(s2,6) - 2*Power(t1,6) + 
            Power(t1,5)*(26 - 6*t2) + Power(-1 + t2,2)*(-11 + 7*t2) + 
            Power(s2,5)*(8 - 48*t1 + 20*t2) + 
            Power(t1,4)*(-12 - 43*t2 + 12*Power(t2,2)) + 
            Power(t1,3)*(-13 - 27*t2 + 28*Power(t2,2)) + 
            Power(t1,2)*(142 - 193*t2 + 38*Power(t2,2) + 
               8*Power(t2,3)) + 
            t1*(88 - 116*t2 + 13*Power(t2,2) + 15*Power(t2,3)) + 
            Power(s2,4)*(-140 + 90*Power(t1,2) + 23*t2 + 
               10*Power(t2,2) - 2*t1*(3 + 37*t2)) - 
            Power(s2,3)*(-75 + 80*Power(t1,3) + 
               Power(t1,2)*(56 - 108*t2) + 44*t2 + 11*Power(t2,2) + 
               4*t1*(-107 + 6*t2 + 10*Power(t2,2))) + 
            Power(s2,2)*(165 + 30*Power(t1,4) + 
               Power(t1,3)*(124 - 80*t2) - 213*t2 + 29*Power(t2,2) + 
               14*Power(t2,3) + 
               Power(t1,2)*(-448 - 64*t2 + 62*Power(t2,2)) + 
               t1*(-151 + 29*t2 + 74*Power(t2,2) - 4*Power(t2,3))) + 
            s2*(-91 + 32*Power(t1,4)*(-3 + t2) + 126*t2 - 
               24*Power(t2,2) - 11*Power(t2,3) + 
               Power(t1,3)*(172 + 108*t2 - 44*Power(t2,2)) + 
               Power(t1,2)*(89 + 42*t2 - 91*Power(t2,2) + 
                  4*Power(t2,3)) - 
               t1*(317 - 435*t2 + 95*Power(t2,2) + 13*Power(t2,3)))) + 
         Power(s1,3)*(11*Power(s2,7) - Power(t1,7) + 
            Power(t1,6)*(-25 + 12*t2) + 
            Power(s2,6)*(10 - 63*t1 + 21*t2) + 
            Power(t1,5)*(42 + 29*t2 - 12*Power(t2,2)) - 
            Power(-1 + t2,2)*(-47 + 17*t2 + 6*Power(t2,2)) + 
            Power(t1,4)*(103 - 165*t2 + 24*Power(t2,2)) + 
            Power(t1,2)*(-157 + 100*t2 + 88*Power(t2,2) - 
               54*Power(t2,3)) - 
            Power(t1,3)*(175 + 10*t2 - 142*Power(t2,2) + 
               36*Power(t2,3)) + 
            t1*(130 - 355*t2 + 296*Power(t2,2) - 67*Power(t2,3) - 
               4*Power(t2,4)) + 
            Power(s2,5)*(-152 + 151*Power(t1,2) + 57*t2 + 
               14*Power(t2,2) - t1*(13 + 109*t2)) - 
            Power(s2,4)*(57 + 195*Power(t1,3) + 
               Power(t1,2)*(73 - 238*t2) + 87*t2 - 38*Power(t2,2) - 
               4*Power(t2,3) + t1*(-630 + 163*t2 + 72*Power(t2,2))) + 
            Power(s2,3)*(553 + 145*Power(t1,4) - 527*t2 + 
               18*Power(t2,2) + 53*Power(t2,3) - 
               6*Power(t1,3)*(-37 + 47*t2) + 
               2*Power(t1,2)*(-510 + 59*t2 + 72*Power(t2,2)) - 
               4*t1*(-16 - 100*t2 + 27*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,2)*(-307 - 61*Power(t1,5) + 380*t2 - 
               45*Power(t2,2) - 53*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,4)*(-248 + 193*t2) + 
               Power(t1,3)*(800 + 54*t2 - 140*Power(t2,2)) + 
               t1*(-1267 + 1021*t2 + 110*Power(t2,2) - 
                  137*Power(t2,3)) + 
               2*Power(t1,2)*
                (73 - 352*t2 + 63*Power(t2,2) + 10*Power(t2,3))) + 
            s2*(-118 + 13*Power(t1,6) + Power(t1,5)*(127 - 73*t2) + 
               315*t2 - 252*Power(t2,2) + 51*Power(t2,3) + 
               4*Power(t2,4) + 
               Power(t1,4)*(-300 - 95*t2 + 66*Power(t2,2)) - 
               4*Power(t1,3)*
                (64 - 139*t2 + 20*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(889 - 484*t2 - 270*Power(t2,2) + 
                  120*Power(t2,3)) + 
               t1*(457 - 449*t2 - 90*Power(t2,2) + 136*Power(t2,3) - 
                  8*Power(t2,4)))) + 
         Power(s1,2)*(4*Power(s2,8) + Power(t1,8) + 
            Power(t1,7)*(2 - 5*t2) + Power(s2,7)*(9 - 27*t1 + 8*t2) + 
            Power(t1,5)*(-48 + 135*t2 - 50*Power(t2,2)) + 
            Power(t1,6)*(-25 + 13*t2 + 4*Power(t2,2)) + 
            Power(-1 + t2,2)*(23 - 73*t2 + 34*Power(t2,2)) + 
            Power(t1,3)*(14 - 389*t2 + 318*Power(t2,2) - 
               16*Power(t2,3)) + 
            Power(t1,4)*(214 - 119*t2 - 102*Power(t2,2) + 
               36*Power(t2,3)) + 
            Power(t1,2)*(-521 + 695*t2 - 44*Power(t2,2) - 
               151*Power(t2,3) + 28*Power(t2,4)) + 
            t1*(-276 + 513*t2 - 209*Power(t2,2) - 68*Power(t2,3) + 
               40*Power(t2,4)) + 
            Power(s2,6)*(-77 + 79*Power(t1,2) + 57*t2 + 8*Power(t2,2) - 
               t1*(40 + 53*t2)) + 
            Power(s2,5)*(-121 - 131*Power(t1,3) - 61*t2 + 
               57*Power(t2,2) + 4*Power(t2,3) + 
               3*Power(t1,2)*(21 + 50*t2) + 
               t1*(407 - 272*t2 - 47*Power(t2,2))) + 
            Power(s2,4)*(445 + 135*Power(t1,4) - 586*t2 + 
               26*Power(t2,2) + 67*Power(t2,3) - 
               5*Power(t1,3)*(6 + 47*t2) + 
               Power(t1,2)*(-883 + 531*t2 + 112*Power(t2,2)) + 
               t1*(380 + 449*t2 - 280*Power(t2,2) - 16*Power(t2,3))) + 
            Power(s2,3)*(218 - 89*Power(t1,5) + 140*t2 - 
               297*Power(t2,2) - 8*Power(t2,3) + 8*Power(t2,4) + 
               5*Power(t1,4)*(-5 + 44*t2) - 
               2*Power(t1,3)*(-501 + 272*t2 + 69*Power(t2,2)) + 
               t1*(-1501 + 1738*t2 + 118*Power(t2,2) - 
                  238*Power(t2,3)) + 
               2*Power(t1,2)*
                (-183 - 558*t2 + 274*Power(t2,2) + 12*Power(t2,3))) + 
            Power(s2,2)*(-850 + 37*Power(t1,6) + 
               Power(t1,5)*(36 - 123*t2) + 1436*t2 - 515*Power(t2,2) - 
               138*Power(t2,3) + 74*Power(t2,4) + 
               Power(t1,4)*(-623 + 311*t2 + 92*Power(t2,2)) - 
               2*Power(t1,3)*
                (-14 - 632*t2 + 267*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(1881 - 1837*t2 - 416*Power(t2,2) + 
                  311*Power(t2,3)) + 
               t1*(-433 - 613*t2 + 829*Power(t2,2) + 46*Power(t2,3) - 
                  24*Power(t2,4))) + 
            s2*(349 - 9*Power(t1,7) - 725*t2 + 412*Power(t2,2) + 
               10*Power(t2,3) - 50*Power(t2,4) + 4*Power(t2,5) + 
               Power(t1,6)*(-15 + 38*t2) + 
               Power(t1,5)*(199 - 96*t2 - 31*Power(t2,2)) + 
               Power(t1,3)*(-1039 + 804*t2 + 374*Power(t2,2) - 
                  176*Power(t2,3)) + 
               Power(t1,4)*(127 - 671*t2 + 259*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(1379 - 2164*t2 + 609*Power(t2,2) + 256*Power(t2,3) - 
                  94*Power(t2,4)) + 
               Power(t1,2)*(201 + 862*t2 - 850*Power(t2,2) - 
                  22*Power(t2,3) + 16*Power(t2,4)))) + 
         s1*(Power(t1,8) - Power(t1,7)*(4 + 3*t2) + 
            Power(s2,7)*(-8 - (-10 + t1)*t2 + Power(t2,2)) + 
            Power(t1,6)*(13 - 8*t2 + 10*Power(t2,2)) - 
            Power(t1,5)*(50 - 43*t2 + 18*Power(t2,2) + 8*Power(t2,3)) - 
            Power(-1 + t2,2)*
             (127 - 127*t2 + 14*Power(t2,2) + 10*Power(t2,3)) + 
            Power(t1,4)*(-12 + 203*t2 - 212*Power(t2,2) + 
               64*Power(t2,3)) + 
            Power(t1,3)*(272 - 435*t2 + 52*Power(t2,2) + 
               123*Power(t2,3) - 36*Power(t2,4)) - 
            Power(t1,2)*(187 + 20*t2 - 415*Power(t2,2) + 
               187*Power(t2,3) + 16*Power(t2,4)) + 
            t1*(-530 + 1295*t2 - 940*Power(t2,2) + 147*Power(t2,3) + 
               24*Power(t2,4) + 4*Power(t2,5)) + 
            Power(s2,6)*(-18 + 12*t2 + 12*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(1 + 6*t2) + t1*(44 - 63*t2 - 6*Power(t2,2))) \
+ Power(s2,5)*(121 - 189*t2 + 61*Power(t2,2) + 30*Power(t2,3) - 
               3*Power(t1,3)*(2 + 5*t2) + 
               3*Power(t1,2)*(-32 + 56*t2 + 5*Power(t2,2)) - 
               t1*(-52 + 17*t2 + 81*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,4)*(212 - 114*t2 - 127*Power(t2,2) + 
               59*Power(t2,3) + 6*Power(t2,4) + 
               5*Power(t1,4)*(3 + 4*t2) - 
               5*Power(t1,3)*(-20 + 49*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(-15 - 60*t2 + 214*Power(t2,2) + 
                  6*Power(t2,3)) - 
               t1*(550 - 806*t2 + 258*Power(t2,2) + 123*Power(t2,3))) - 
            Power(s2,3)*(474 - 1131*t2 + 643*Power(t2,2) + 
               86*Power(t2,3) - 94*Power(t2,4) + 
               5*Power(t1,5)*(4 + 3*t2) - 
               5*Power(t1,4)*(-8 + 42*t2 + 3*Power(t2,2)) + 
               2*Power(t1,3)*
                (50 - 85*t2 + 143*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,2)*(974 - 1327*t2 + 426*Power(t2,2) + 
                  197*Power(t2,3)) + 
               t1*(548 + 55*t2 - 734*Power(t2,2) + 262*Power(t2,3) + 
                  20*Power(t2,4))) + 
            Power(s2,2)*(-297 + 105*t2 + 510*Power(t2,2) - 
               283*Power(t2,3) - 34*Power(t2,4) + 4*Power(t2,5) + 
               3*Power(t1,6)*(5 + 2*t2) - 
               3*Power(t1,5)*(4 + 35*t2 + 2*Power(t2,2)) + 
               Power(t1,4)*(140 - 160*t2 + 204*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(t1,3)*(832 - 1035*t2 + 340*Power(t2,2) + 
                  153*Power(t2,3)) + 
               t1*(1174 - 2528*t2 + 1146*Power(t2,2) + 
                  354*Power(t2,3) - 214*Power(t2,4)) + 
               Power(t1,2)*(448 + 655*t2 - 1299*Power(t2,2) + 
                  411*Power(t2,3) + 22*Power(t2,4))) - 
            s2*(-591 + 1472*t2 - 1089*Power(t2,2) + 136*Power(t2,3) + 
               90*Power(t2,4) - 18*Power(t2,5) + Power(t1,7)*(6 + t2) - 
               Power(t1,6)*(16 + 28*t2 + Power(t2,2)) + 
               Power(t1,5)*(72 - 63*t2 + 73*Power(t2,2)) - 
               Power(t1,4)*(337 - 368*t2 + 129*Power(t2,2) + 
                  57*Power(t2,3)) + 
               Power(t1,2)*(972 - 1832*t2 + 555*Power(t2,2) + 
                  391*Power(t2,3) - 156*Power(t2,4)) + 
               Power(t1,3)*(100 + 689*t2 - 904*Power(t2,2) + 
                  272*Power(t2,3) + 8*Power(t2,4)) + 
               t1*(-494 + 137*t2 + 827*Power(t2,2) - 386*Power(t2,3) - 
                  82*Power(t2,4) + 8*Power(t2,5)))) + 
         Power(s,5)*(2 + 36*t1 + 21*Power(t1,2) - 44*Power(t1,3) + 
            26*Power(t1,4) + 2*Power(s2,3)*(-2 + 3*t1 - 5*t2) - 8*t2 + 
            40*t1*t2 + 48*Power(t1,2)*t2 - 43*Power(t1,3)*t2 + 
            5*Power(t2,2) - 38*t1*Power(t2,2) + 
            8*Power(t1,2)*Power(t2,2) + 34*Power(t2,3) + 
            9*t1*Power(t2,3) + 5*Power(s1,4)*(-4 + 5*s2 - 3*t1 + 2*t2) + 
            Power(s1,3)*(47 + 43*Power(s2,2) + 20*Power(t1,2) + 
               t1*(83 - 14*t2) - 43*t2 - 2*Power(t2,2) + 
               s2*(-86 - 59*t1 + 11*t2)) + 
            Power(s2,2)*(36 + 14*Power(t1,2) + 55*t2 + 61*Power(t2,2) - 
               t1*(18 + 67*t2)) + 
            Power(s1,2)*(-2 + 11*Power(s2,3) - 16*Power(t1,3) - 26*t2 + 
               75*Power(t2,2) - 4*Power(t2,3) + 
               Power(s2,2)*(-62 - 38*t1 + 2*t2) + 
               3*Power(t1,2)*(-16 + 5*t2) + 
               s2*(39 + 43*Power(t1,2) + t1*(133 - 23*t2) - 57*t2 + 
                  2*Power(t2,2)) + t1*(-8 - 41*t2 + 5*Power(t2,2))) - 
            s2*(25 + 46*Power(t1,3) - 8*t2 + 75*Power(t2,2) + 
               13*Power(t2,3) - 6*Power(t1,2)*(11 + 20*t2) + 
               t1*(94 + 49*t2 + 61*Power(t2,2))) + 
            s1*(Power(t1,4) + Power(t1,3)*(-27 + 2*t2) + 
               Power(s2,3)*(-4 - 10*t1 + 15*t2) + 
               Power(t1,2)*(-38 + 95*t2 - 7*Power(t2,2)) + 
               Power(s2,2)*(-26 + 21*Power(t1,2) + t1*(38 - 34*t2) - 
                  46*t2 + 9*Power(t2,2)) + 
               2*t1*(-57 + 49*t2 - 31*Power(t2,2) + 2*Power(t2,3)) - 
               2*(15 - 40*t2 + 56*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(56 - 12*Power(t1,3) + 9*t2 + 129*Power(t2,2) - 
                  6*Power(t2,3) + Power(t1,2)*(-7 + 17*t2) + 
                  t1*(60 - 94*t2 + Power(t2,2))))) + 
         Power(s,4)*(-29 - 156*t1 - 24*Power(t1,2) + 49*Power(t1,3) - 
            90*Power(t1,4) + 24*Power(t1,5) + 100*t2 + 198*t1*t2 + 
            63*Power(t1,2)*t2 + 141*Power(t1,3)*t2 - 32*Power(t1,4)*t2 - 
            100*Power(t2,2) + 14*t1*Power(t2,2) - 
            96*Power(t1,2)*Power(t2,2) - 8*Power(t1,3)*Power(t2,2) + 
            44*Power(t2,3) + 29*t1*Power(t2,3) + 
            16*Power(t1,2)*Power(t2,3) + 16*Power(t2,4) - 
            10*Power(s1,5)*(-2 + 3*s2 - 2*t1 + t2) + 
            Power(s2,4)*(16 - 4*t1 + 10*t2) - 
            Power(s2,3)*(30 + 12*Power(t1,2) + t1*(30 - 78*t2) + 
               68*t2 + 95*Power(t2,2)) - 
            2*Power(s1,4)*(21 + 41*Power(s2,2) + 15*Power(t1,2) + 
               t1*(46 - 3*t2) - 12*t2 - 4*Power(t2,2) + 
               s2*(-58 - 53*t1 + 7*t2)) + 
            Power(s2,2)*(-102 + 60*Power(t1,3) + 18*t2 + 
               97*Power(t2,2) + 35*Power(t2,3) - 
               2*Power(t1,2)*(39 + 109*t2) + 
               t1*(188 + 223*t2 + 148*Power(t2,2))) - 
            s2*(-127 + 68*Power(t1,4) + 136*t2 - 27*Power(t2,2) + 
               153*Power(t2,3) - 2*Power(t1,3)*(91 + 81*t2) + 
               Power(t1,2)*(207 + 296*t2 + 45*Power(t2,2)) + 
               t1*(-141 + 187*t2 - 107*Power(t2,2) + 49*Power(t2,3))) + 
            Power(s1,3)*(-20 - 51*Power(s2,3) + 31*Power(t1,3) + 
               Power(t1,2)*(75 - 14*t2) + 104*t2 - 101*Power(t2,2) + 
               4*Power(t2,3) + Power(s2,2)*(125 + 117*t1 + 27*t2) - 
               2*t1*(17 - 39*t2 + 8*Power(t2,2)) - 
               s2*(48 + 97*Power(t1,2) - 12*t2 - 22*Power(t2,2) + 
                  t1*(205 + 9*t2))) + 
            Power(s1,2)*(10 + 10*Power(s2,4) - 13*Power(t1,4) + 
               Power(t1,3)*(56 - 15*t2) - 112*t2 + 122*Power(t2,2) + 
               27*Power(t2,3) + Power(s2,3)*(-11 - 5*t1 + 30*t2) + 
               Power(s2,2)*(39 - 33*Power(t1,2) + t1*(22 - 51*t2) - 
                  25*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(207 - 262*t2 + 44*Power(t2,2)) + 
               t1*(159 - 215*t2 + 174*Power(t2,2) - 16*Power(t2,3)) + 
               s2*(-14 + 41*Power(t1,3) + 103*t2 - 297*Power(t2,2) + 
                  20*Power(t2,3) + Power(t1,2)*(-67 + 36*t2) + 
                  t1*(-263 + 346*t2 - 67*Power(t2,2)))) + 
            s1*(77 - 6*Power(t1,5) + 
               2*Power(s2,4)*(-17 + 5*t1 - 10*t2) - 196*t2 + 
               205*Power(t2,2) - 157*Power(t2,3) + 6*Power(t2,4) + 
               2*Power(t1,4)*(-25 + 9*t2) - 
               2*Power(t1,3)*(50 - 60*t2 + 9*Power(t2,2)) + 
               t1*(206 - 382*t2 + 44*Power(t2,2) - 37*Power(t2,3)) + 
               Power(t1,2)*(-74 + 63*t2 - 39*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s2,3)*(100 - 24*Power(t1,2) + 24*t2 - 
                  5*Power(t2,2) + t1*(84 + 45*t2)) + 
               Power(s2,2)*(44 + 12*Power(t1,3) - 16*t2 - 
                  284*Power(t2,2) + 15*Power(t2,3) - 
                  4*Power(t1,2)*(29 + 3*t2) + 
                  t1*(-376 + 221*t2 - 20*Power(t2,2))) + 
               s2*(-191 + 8*Power(t1,4) + Power(t1,3)*(116 - 31*t2) + 
                  145*t2 + 277*Power(t2,2) + 54*Power(t2,3) + 
                  Power(t1,2)*(376 - 365*t2 + 43*Power(t2,2)) + 
                  t1*(112 - 143*t2 + 285*Power(t2,2) - 20*Power(t2,3))))) \
+ Power(s,3)*(183 + 470*t1 + 53*Power(t1,2) - 44*Power(t1,3) + 
            125*Power(t1,4) - 82*Power(t1,5) + 11*Power(t1,6) + 
            Power(s2,5)*(-14 + t1 - 5*t2) - 484*t2 - 760*t1*t2 + 
            10*Power(t1,2)*t2 - 82*Power(t1,3)*t2 + 108*Power(t1,4)*t2 - 
            8*Power(t1,5)*t2 + 463*Power(t2,2) + 382*t1*Power(t2,2) + 
            157*Power(t1,2)*Power(t2,2) - 34*Power(t1,3)*Power(t2,2) - 
            17*Power(t1,4)*Power(t2,2) - 212*Power(t2,3) - 
            60*t1*Power(t2,3) - 18*Power(t1,2)*Power(t2,3) + 
            14*Power(t1,3)*Power(t2,3) + 70*Power(t2,4) + 
            26*t1*Power(t2,4) + 5*Power(s1,6)*(-2 + 4*s2 - 3*t1 + t2) + 
            Power(s2,4)*(-26 + 7*Power(t1,2) + t1*(48 - 52*t2) + 
               64*t2 + 85*Power(t2,2)) + 
            Power(s1,5)*(14 + 78*Power(s2,2) + 25*Power(t1,2) + t2 - 
               7*Power(t2,2) + t1*(53 + 6*t2) + s2*(-82 - 99*t1 + 11*t2)\
) - 2*Power(s2,3)*(-104 + 19*Power(t1,3) + 14*t2 + 11*Power(t2,2) + 
               25*Power(t2,3) - Power(t1,2)*(11 + 97*t2) + 
               t1*(25 + 183*t2 + 91*Power(t2,2))) + 
            Power(s2,2)*(62*Power(t1,4) - 
               2*Power(t1,3)*(107 + 110*t2) + 
               Power(t1,2)*(303 + 648*t2 + 92*Power(t2,2)) + 
               2*t1*(-298 + 142*t2 - 82*Power(t2,2) + 53*Power(t2,3)) + 
               2*(52 - 9*t2 - 46*Power(t2,2) + 124*Power(t2,3))) - 
            s2*(442 + 43*Power(t1,5) - 613*t2 + 124*Power(t2,2) + 
               60*Power(t2,3) + 72*Power(t2,4) - 
               Power(t1,4)*(240 + 91*t2) + 
               Power(t1,3)*(352 + 454*t2 - 22*Power(t2,2)) + 
               2*Power(t1,2)*
                (-216 + 87*t2 - 110*Power(t2,2) + 35*Power(t2,3)) + 
               t1*(103 + 68*t2 + 122*Power(t2,2) + 144*Power(t2,3))) + 
            Power(s1,4)*(32 + 86*Power(s2,3) - 47*Power(t1,2) - 
               26*Power(t1,3) - 90*t2 + 48*Power(t2,2) - 
               Power(s2,2)*(127 + 170*t1 + 38*t2) + 
               2*t1*(22 - 33*t2 + 6*Power(t2,2)) + 
               s2*(26 + 110*Power(t1,2) + 37*t2 - 34*Power(t2,2) + 
                  7*t1*(23 + 6*t2))) + 
            Power(s1,3)*(34 - 6*Power(s2,4) + 28*Power(t1,4) + 
               Power(s2,3)*(13 + 6*t1 - 118*t2) + 
               16*Power(t1,3)*(-5 + t2) + 9*Power(t2,2) - 
               31*Power(t2,3) - 
               10*Power(t1,2)*(20 - 29*t2 + 6*Power(t2,2)) + 
               t1*(34 + 80*t2 - 171*Power(t2,2) + 16*Power(t2,3)) + 
               Power(s2,2)*(57 + 34*Power(t1,2) + 147*t2 - 
                  68*Power(t2,2) + t1*(-101 + 220*t2)) - 
               s2*(138 + 62*Power(t1,3) + 98*t2 - 285*Power(t2,2) + 
                  16*Power(t2,3) + 2*Power(t1,2)*(-84 + 59*t2) - 
                  2*t1*(81 - 226*t2 + 60*Power(t2,2)))) + 
            Power(s1,2)*(-33 - 40*Power(s2,5) + 3*Power(t1,5) + 
               Power(s2,4)*(116 + 135*t1 - 75*t2) + 
               Power(t1,4)*(151 - 65*t2) + 199*t2 - 339*Power(t2,2) + 
               200*Power(t2,3) - 8*Power(t2,4) - 
               2*Power(s2,3)*
                (-54 + 84*Power(t1,2) + t1*(229 - 127*t2) - 35*t2 + 
                  30*Power(t2,2)) + 
               Power(t1,3)*(274 - 358*t2 + 86*Power(t2,2)) + 
               Power(t1,2)*(-300 + 86*t2 + 127*Power(t2,2) - 
                  24*Power(t2,3)) + 
               t1*(-541 + 832*t2 - 224*Power(t2,2) + 98*Power(t2,3)) + 
               Power(s2,2)*(-585 + 94*Power(t1,3) + 
                  Power(t1,2)*(719 - 348*t2) + 268*t2 + 
                  384*Power(t2,2) - 40*Power(t2,3) + 
                  2*t1*(69 - 320*t2 + 109*Power(t2,2))) - 
               2*s2*(-193 + 12*Power(t1,4) + 193*t2 + 31*Power(t2,2) + 
                  74*Power(t2,3) - 3*Power(t1,3)*(-88 + 39*t2) + 
                  2*Power(t1,2)*(130 - 232*t2 + 61*Power(t2,2)) + 
                  t1*(-426 + 171*t2 + 232*Power(t2,2) - 32*Power(t2,3)))\
) - s1*(267 + 9*Power(t1,6) + Power(t1,5)*(20 - 22*t2) + 
               Power(s2,5)*(-56 + 5*t1 - 15*t2) - 464*t2 + 
               215*Power(t2,2) - 28*Power(t2,3) + 54*Power(t2,4) + 
               Power(t1,4)*(77 - 34*t2 + 17*Power(t2,2)) - 
               Power(t1,3)*(226 - 146*t2 + 57*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(-585 + 1020*t2 - 309*Power(t2,2) + 
                  91*Power(t2,3)) + 
               t1*(138 + 128*t2 - 242*Power(t2,2) + 212*Power(t2,3) - 
                  20*Power(t2,4)) + 
               Power(s2,4)*(46 - 11*Power(t1,2) + 11*t2 + 
                  5*Power(t2,2) + 5*t1*(43 + 6*t2)) - 
               Power(s2,3)*(-502 + 6*Power(t1,3) + 
                  Power(t1,2)*(329 - 22*t2) + 190*t2 + 
                  286*Power(t2,2) - 20*Power(t2,3) + 
                  2*t1*(196 - 93*t2 + 25*Power(t2,2))) + 
               s2*(-319 - 31*Power(t1,5) + 107*t2 + 412*Power(t2,2) - 
                  508*Power(t2,3) + 24*Power(t2,4) + 
                  Power(t1,4)*(-107 + 81*t2) + 
                  Power(t1,3)*(-454 + 276*t2 - 74*Power(t2,2)) + 
                  t1*(1133 - 1872*t2 + 344*Power(t2,2) - 
                     234*Power(t2,3)) + 
                  Power(t1,2)*
                   (852 - 432*t2 - 59*Power(t2,2) + 24*Power(t2,3))) + 
               Power(s2,2)*(34*Power(t1,4) + Power(t1,3)*(257 - 96*t2) + 
                  Power(t1,2)*(723 - 439*t2 + 102*Power(t2,2)) + 
                  t1*(-1128 + 476*t2 + 402*Power(t2,2) - 
                     40*Power(t2,3)) + 
                  2*(-239 + 313*t2 + 66*Power(t2,2) + 78*Power(t2,3))))) \
+ Power(s,2)*(-461 - 738*t1 + 42*Power(t1,2) + 82*Power(t1,3) - 
            97*Power(t1,4) + 106*Power(t1,5) - 32*Power(t1,6) + 
            2*Power(t1,7) + 1177*t2 + 1200*t1*t2 - 358*Power(t1,2)*t2 + 
            10*Power(t1,3)*t2 - 55*Power(t1,4)*t2 + 20*Power(t1,5)*t2 + 
            2*Power(t1,6)*t2 - 1134*Power(t2,2) - 626*t1*Power(t2,2) + 
            259*Power(t1,2)*Power(t2,2) + 87*Power(t1,3)*Power(t2,2) + 
            20*Power(t1,4)*Power(t2,2) - 10*Power(t1,5)*Power(t2,2) + 
            526*Power(t2,3) + 110*t1*Power(t2,3) - 
            58*Power(t1,2)*Power(t2,3) - 32*Power(t1,3)*Power(t2,3) + 
            6*Power(t1,4)*Power(t2,3) - 128*Power(t2,4) + 
            32*t1*Power(t2,4) + 28*Power(t1,2)*Power(t2,4) + 
            26*Power(t2,5) - 4*t1*Power(t2,5) + Power(s2,6)*(4 + t2) - 
            Power(s1,7)*(-2 + 7*s2 - 6*t1 + t2) - 
            Power(s2,5)*(-35 + 2*Power(t1,2) + t1*(18 - 19*t2) + 
               40*t2 + 43*Power(t2,2)) - 
            Power(s1,6)*(-1 + 37*Power(s2,2) + 11*Power(t1,2) + 5*t2 - 
               2*Power(t2,2) + 7*t1*(2 + t2) + s2*(-29 - 47*t1 + 5*t2)) \
+ Power(s2,4)*(-44 + 10*Power(t1,3) - 41*t2 - 84*Power(t1,2)*t2 - 
               36*Power(t2,2) + 40*Power(t2,3) + 
               t1*(-87 + 282*t2 + 118*Power(t2,2))) + 
            Power(s2,3)*(-438 - 20*Power(t1,4) + 396*t2 + 
               24*Power(t2,2) - 166*Power(t2,3) + 
               2*Power(t1,3)*(50 + 63*t2) - 
               Power(t1,2)*(55 + 626*t2 + 86*Power(t2,2)) + 
               2*t1*(173 + 5*t2 + 71*Power(t2,2) - 57*Power(t2,3))) + 
            Power(s2,2)*(166 + 20*Power(t1,5) - 623*t2 + 
               312*Power(t2,2) - 112*Power(t2,3) + 120*Power(t2,4) - 
               Power(t1,4)*(180 + 79*t2) + 
               Power(t1,3)*(337 + 586*t2 - 20*Power(t2,2)) + 
               3*Power(t1,2)*
                (-219 + 16*t2 - 52*Power(t2,2) + 38*Power(t2,3)) + 
               2*t1*(491 - 478*t2 + 168*Power(t2,2) + 81*Power(t2,3))) + 
            s2*(738 - 10*Power(t1,6) - 1052*t2 + 297*Power(t2,2) + 
               210*Power(t2,3) - 174*Power(t2,4) + 
               3*Power(t1,5)*(42 + 5*t2) + 
               Power(t1,4)*(-336 - 222*t2 + 41*Power(t2,2)) + 
               Power(t1,3)*(452 + 38*t2 + 30*Power(t2,2) - 
                  46*Power(t2,3)) + 
               Power(t1,2)*(-626 + 550*t2 - 447*Power(t2,2) + 
                  36*Power(t2,3)) + 
               t1*(-264 + 1167*t2 - 772*Power(t2,2) + 212*Power(t2,3) - 
                  118*Power(t2,4))) - 
            Power(s1,5)*(19 + 64*Power(s2,3) - 7*Power(t1,3) - 26*t2 + 
               Power(t2,2) + Power(t2,3) - 7*Power(t1,2)*(1 + t2) - 
               Power(s2,2)*(62 + 117*t1 + 16*t2) + 
               t1*(18 - 23*t2 + Power(t2,2)) + 
               s2*(1 + 60*Power(t1,2) + 31*t2 - 19*Power(t2,2) + 
                  t1*(59 + 29*t2))) + 
            Power(s1,4)*(-16 - 16*Power(s2,4) - 20*Power(t1,4) + 
               37*t2 - 42*Power(t2,2) + 7*Power(t2,3) + 
               4*Power(t1,3)*(10 + t2) + 
               2*Power(s2,3)*(8 + 17*t1 + 49*t2) + 
               4*Power(t1,2)*(-1 - 18*t2 + 6*Power(t2,2)) + 
               t1*(-147 + 94*t2 + 38*Power(t2,2) - 4*Power(t2,3)) - 
               2*Power(s2,2)*
                (60 + 20*Power(t1,2) + 43*t2 - 27*Power(t2,2) + 
                  2*t1*(-7 + 44*t2)) + 
               s2*(137 + 42*Power(t1,3) - 99*Power(t2,2) + 
                  Power(t1,2)*(-84 + 74*t2) + 
                  t1*(124 + 139*t2 - 64*Power(t2,2)))) + 
            Power(s1,3)*(-57 + 59*Power(s2,5) + 12*Power(t1,5) + 
               39*t2 + 60*Power(t2,2) - 33*Power(t2,3) + 
               2*Power(t2,4) + Power(t1,4)*(-181 + 55*t2) + 
               Power(s2,4)*(-97 - 220*t1 + 157*t2) - 
               2*Power(t1,3)*(63 - 180*t2 + 46*Power(t2,2)) + 
               Power(s2,3)*(-418 + 294*Power(t1,2) - 
                  466*t1*(-1 + t2) - 101*t2 + 92*Power(t2,2)) + 
               t1*(330 - 344*t2 + 48*Power(t2,2) - 51*Power(t2,3)) + 
               4*Power(t1,2)*
                (130 - 43*t2 - 42*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*(806 - 152*Power(t1,3) - 391*t2 - 
                  229*Power(t2,2) + 24*Power(t2,3) + 
                  6*Power(t1,2)*(-137 + 86*t2) + 
                  t1*(706 + 565*t2 - 264*Power(t2,2))) + 
               s2*(-215 + 7*Power(t1,4) + Power(t1,3)*(634 - 262*t2) + 
                  162*t2 - 56*Power(t2,2) + 115*Power(t2,3) + 
                  2*Power(t1,2)*(-81 - 412*t2 + 132*Power(t2,2)) + 
                  t1*(-1354 + 616*t2 + 376*Power(t2,2) - 48*Power(t2,3))\
)) + Power(s1,2)*(245 + 43*Power(s2,6) + 11*Power(t1,6) + 
               Power(t1,5)*(114 - 69*t2) - 536*t2 + 423*Power(t2,2) - 
               152*Power(t2,3) + 30*Power(t2,4) + 
               Power(s2,5)*(-85 - 199*t1 + 79*t2) + 
               Power(s2,4)*(-439 + 377*Power(t1,2) + 
                  t1*(468 - 361*t2) + 75*t2 + 70*Power(t2,2)) + 
               Power(t1,4)*(65 - 194*t2 + 74*Power(t2,2)) - 
               2*Power(t1,3)*
                (334 - 303*t2 + 33*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(-159 + 870*t2 - 670*Power(t2,2) + 
                  183*Power(t2,3)) + 
               t1*(848 - 1231*t2 + 227*Power(t2,2) + 298*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(s2,3)*(909 - 378*Power(t1,3) - 798*t2 - 
                  120*Power(t2,2) + 40*Power(t2,3) + 
                  6*Power(t1,2)*(-168 + 113*t2) + 
                  t1*(1118 + 134*t2 - 302*Power(t2,2))) + 
               Power(s2,2)*(298 + 217*Power(t1,4) + 
                  Power(t1,3)*(1066 - 658*t2) + 28*t2 - 
                  280*Power(t2,2) + 282*Power(t2,3) + 
                  Power(t1,2)*(-854 - 687*t2 + 468*Power(t2,2)) + 
                  t1*(-2459 + 2174*t2 + 126*Power(t2,2) - 
                     96*Power(t2,3))) + 
               s2*(-977 - 71*Power(t1,5) + 1311*t2 + 13*Power(t2,2) - 
                  504*Power(t2,3) + 24*Power(t2,4) + 
                  Power(t1,4)*(-555 + 331*t2) + 
                  Power(t1,3)*(110 + 672*t2 - 310*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (1109 - 991*t2 + 30*Power(t2,2) + 36*Power(t2,3)) - 
                  t1*(97 + 970*t2 - 954*Power(t2,2) + 434*Power(t2,3)))) \
+ s1*(285 - 5*Power(t1,7) + Power(s2,6)*(-35 + t1 - 6*t2) - 475*t2 + 
               116*Power(t2,2) + 129*Power(t2,3) - 76*Power(t2,4) + 
               4*Power(t2,5) + Power(t1,6)*(10 + 11*t2) - 
               Power(t1,5)*(44 + 32*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(125 - 756*t2 + 256*Power(t2,2) - 
                  103*Power(t2,3)) + 
               Power(t1,4)*(295 - 171*t2 + 103*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-648 + 1672*t2 - 1235*Power(t2,2) + 
                  284*Power(t2,3) - 118*Power(t2,4)) + 
               Power(t1,2)*(-1246 + 1347*t2 - 258*Power(t2,2) - 
                  33*Power(t2,3) + 22*Power(t2,4)) + 
               Power(s2,5)*(-61 + 31*t2 + 9*Power(t2,2) + 
                  t1*(169 + 7*t2)) - 
               Power(s2,4)*(-529 + 15*Power(t1,3) + 
                  Power(t1,2)*(316 - 43*t2) + 266*t2 + 
                  114*Power(t2,2) - 15*Power(t2,3) + 
                  t1*(-64 + 53*t2 + 55*Power(t2,2))) + 
               Power(s2,3)*(400 + 40*Power(t1,4) + 
                  Power(t1,3)*(274 - 122*t2) + 122*t2 - 
                  58*Power(t2,2) + 204*Power(t2,3) + 
                  Power(t1,2)*(218 + 5*t2 + 118*Power(t2,2)) - 
                  4*t1*(478 - 273*t2 - 29*Power(t2,2) + 10*Power(t2,3))) \
- Power(s2,2)*(1656 + 45*Power(t1,5) + Power(t1,4)*(91 - 128*t2) - 
                  2034*t2 + 98*Power(t2,2) + 486*Power(t2,3) - 
                  36*Power(t2,4) + 
                  Power(t1,3)*(428 + 7*t2 + 114*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (844 - 519*t2 + 71*Power(t2,2) + 12*Power(t2,3)) + 
                  t1*(527 + 1366*t2 - 570*Power(t2,2) + 480*Power(t2,3))\
) + s2*(499 + 24*Power(t1,6) - 1332*t2 + 923*Power(t2,2) - 
                  226*Power(t2,3) + 202*Power(t2,4) - 
                  Power(t1,5)*(11 + 61*t2) + 
                  Power(t1,4)*(251 + 56*t2 + 49*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (722 - 451*t2 + 159*Power(t2,2) + 6*Power(t2,3)) + 
                  Power(t1,2)*
                   (2 + 2000*t2 - 768*Power(t2,2) + 379*Power(t2,3)) + 
                  t1*(2907 - 3443*t2 + 512*Power(t2,2) + 
                     418*Power(t2,3) - 60*Power(t2,4))))) + 
         s*(457 + Power(s1,8)*(s2 - t1) + 504*t1 - 168*Power(t1,2) - 
            64*Power(t1,3) + 57*Power(t1,4) - 60*Power(t1,5) + 
            30*Power(t1,6) - 4*Power(t1,7) - 1328*t2 - 878*t1*t2 + 
            518*Power(t1,2)*t2 - 83*Power(t1,3)*t2 - 44*Power(t1,4)*t2 + 
            12*Power(t1,5)*t2 - 6*Power(t1,6)*t2 + Power(t1,7)*t2 + 
            1387*Power(t2,2) + 356*t1*Power(t2,2) - 
            425*Power(t1,2)*Power(t2,2) + 146*Power(t1,3)*Power(t2,2) + 
            26*Power(t1,4)*Power(t2,2) + 16*Power(t1,5)*Power(t2,2) - 
            2*Power(t1,6)*Power(t2,2) - 668*Power(t2,3) + 
            17*t1*Power(t2,3) + 110*Power(t1,2)*Power(t2,3) - 
            28*Power(t1,3)*Power(t2,3) - 24*Power(t1,4)*Power(t2,3) + 
            Power(t1,5)*Power(t2,3) + 180*Power(t2,4) - 
            32*t1*Power(t2,4) - 24*Power(t1,2)*Power(t2,4) + 
            22*Power(t1,3)*Power(t2,4) - 28*Power(t2,5) + 
            30*t1*Power(t2,5) - 4*Power(t1,2)*Power(t2,5) + 
            Power(s2,6)*(-8 + (11 - 3*t1)*t2 + 11*Power(t2,2)) + 
            Power(s1,7)*(-1 + 7*Power(s2,2) + t1 + 2*Power(t1,2) + t2 + 
               2*t1*t2 + s2*(-4 - 9*t1 + t2)) + 
            Power(s2,5)*(-31 + 56*t2 + 17*Power(t2,2) - 17*Power(t2,3) + 
               2*Power(t1,2)*(2 + 7*t2) + 
               t1*(34 - 89*t2 - 37*Power(t2,2))) + 
            2*Power(s2,3)*(182 - 230*t2 + 30*Power(t2,2) + 
               66*Power(t2,3) - 44*Power(t2,4) + 
               10*Power(t1,4)*(2 + t2) + 
               Power(t1,3)*(-38 - 145*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(33 + 159*t2 - 14*Power(t2,2) - 
                  41*Power(t2,3)) - 
               t1*(245 - 370*t2 + 151*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,4)*(112 - 158*t2 + 31*Power(t2,2) + 
               30*Power(t2,3) - 5*Power(t1,3)*(4 + 5*t2) + 
               Power(t1,2)*(-26 + 240*t2 + 36*Power(t2,2)) + 
               t1*(60 - 222*t2 - 30*Power(t2,2) + 61*Power(t2,3))) + 
            Power(s2,2)*(-371 + 1183*t2 - 935*Power(t2,2) + 
               56*Power(t2,3) + 74*Power(t2,4) - 
               5*Power(t1,5)*(8 + t2) + 
               Power(t1,4)*(164 + 155*t2 - 29*Power(t2,2)) + 
               Power(t1,2)*(701 - 1050*t2 + 537*Power(t2,2) - 
                  98*Power(t2,3)) + 
               2*Power(t1,3)*
                (-124 - 91*t2 + 47*Power(t2,2) + 25*Power(t2,3)) + 
               t1*(-730 + 691*t2 + 74*Power(t2,2) - 216*Power(t2,3) + 
                  158*Power(t2,4))) - 
            s2*(523 + 2*Power(t1,6)*(-10 + t2) - 768*t2 + 
               27*Power(t2,2) + 291*Power(t2,3) - 144*Power(t2,4) + 
               68*Power(t2,5) + 
               Power(t1,5)*(118 + 21*t2 - 15*Power(t2,2)) + 
               Power(t1,4)*(-213 - 18*t2 + 69*Power(t2,2) + 
                  13*Power(t2,3)) - 
               4*Power(t1,3)*
                (-95 + 128*t2 - 73*Power(t2,2) + 25*Power(t2,3)) + 
               2*Power(t1,2)*
                (-215 + 74*t2 + 140*Power(t2,2) - 56*Power(t2,3) + 
                  46*Power(t2,4)) + 
               t1*(-550 + 1751*t2 - 1473*Power(t2,2) + 284*Power(t2,3) + 
                  10*Power(t2,4) - 8*Power(t2,5))) + 
            Power(s1,6)*(19*Power(s2,3) + 2*Power(t1,3) - 
               Power(s2,2)*(11 + 32*t1) + Power(t1,2)*(4 - 3*t2) - 
               2*(-2 + t2 + Power(t2,2)) - 
               t1*(-2 + 3*t2 + Power(t2,2)) + 
               s2*(-3 + 11*Power(t1,2) + 9*t2 - 4*Power(t2,2) + 
                  5*t1*(1 + t2))) + 
            Power(s1,5)*(-4 + 14*Power(s2,4) + 3*Power(t1,4) - 2*t2 + 
               5*Power(t2,2) + Power(t2,3) - Power(t1,3)*(1 + 10*t2) - 
               Power(s2,3)*(17 + 28*t1 + 25*t2) - 
               Power(t1,2)*(-62 + 35*t2 + Power(t2,2)) + 
               t1*(60 - 54*t2 + 11*Power(t2,2)) + 
               Power(s2,2)*(63 + 17*Power(t1,2) + t2 - 17*Power(t2,2) + 
                  t1*(25 + 38*t2)) + 
               s2*(-36 - 6*Power(t1,3) + 13*t2 + 4*Power(t2,2) + 
                  2*Power(t2,3) - Power(t1,2)*(7 + 3*t2) + 
                  t1*(-130 + 46*t2 + 11*Power(t2,2)))) + 
            Power(s1,4)*(47 - 23*Power(s2,5) - 11*Power(t1,5) + 
               Power(s2,4)*(7 + 93*t1 - 76*t2) + 
               Power(t1,4)*(63 - 6*t2) - 78*t2 + 41*Power(t2,2) - 
               10*Power(t2,3) + 
               Power(t1,3)*(-66 - 91*t2 + 32*Power(t2,2)) + 
               t1*(29 - 136*t2 + 82*Power(t2,2) - 7*Power(t2,3)) + 
               Power(t1,2)*(-233 + 60*t2 + 84*Power(t2,2) - 
                  4*Power(t2,3)) + 
               2*Power(s2,3)*
                (154 - 65*Power(t1,2) + t2 - 19*Power(t2,2) + 
                  t1*(-41 + 101*t2)) + 
               Power(s2,2)*(-356 + 62*Power(t1,3) + 
                  Power(t1,2)*(206 - 182*t2) + 177*t2 + 
                  62*Power(t2,2) + t1*(-706 - 51*t2 + 92*Power(t2,2))) + 
               s2*(-24 + 9*Power(t1,4) + 74*t2 + 3*Power(t2,2) - 
                  21*Power(t2,3) + 2*Power(t1,3)*(-97 + 31*t2) + 
                  Power(t1,2)*(464 + 140*t2 - 86*Power(t2,2)) + 
                  t1*(606 - 261*t2 - 143*Power(t2,2) + 8*Power(t2,3)))) + 
            Power(s1,3)*(-58 - 45*Power(s2,6) + 
               Power(s2,5)*(25 + 213*t1 - 93*t2) + 58*t2 + 
               31*Power(t2,2) - 39*Power(t2,3) + 8*Power(t2,4) + 
               2*Power(t1,5)*(-59 + 23*t2) + 
               Power(t1,4)*(14 + 196*t2 - 58*Power(t2,2)) - 
               2*Power(s2,4)*
                (-251 + 201*Power(t1,2) + t1*(115 - 189*t2) + 36*t2 + 
                  29*Power(t2,2)) + 
               Power(t1,2)*(-260 + 76*t2 + 154*Power(t2,2) - 
                  80*Power(t2,3)) + 
               Power(t1,3)*(374 - 262*t2 - 38*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-572 + 780*t2 - 121*Power(t2,2) - 102*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s2,3)*(-507 + 378*Power(t1,3) + 
                  Power(t1,2)*(658 - 622*t2) + 448*t2 + 7*Power(t2,2) - 
                  16*Power(t2,3) + 2*t1*(-743 - 14*t2 + 116*Power(t2,2))\
) + Power(s2,2)*(-568 - 177*Power(t1,4) + 557*t2 + 29*Power(t2,2) - 
                  137*Power(t2,3) + Power(t1,3)*(-844 + 528*t2) + 
                  Power(t1,2)*(1480 + 468*t2 - 348*Power(t2,2)) + 
                  t1*(1373 - 1098*t2 - 97*Power(t2,2) + 48*Power(t2,3))) \
+ s2*(621 + 33*Power(t1,5) + Power(t1,4)*(509 - 237*t2) - 831*t2 + 
                  111*Power(t2,2) + 110*Power(t2,3) - 4*Power(t2,4) + 
                  Power(t1,3)*(-510 - 564*t2 + 232*Power(t2,2)) - 
                  8*Power(t1,2)*
                   (155 - 114*t2 - 16*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(845 - 704*t2 - 100*Power(t2,2) + 188*Power(t2,3)))) \
+ Power(s1,2)*(-248 - 21*Power(s2,7) + 6*Power(t1,7) + 
               Power(t1,6)*(33 - 31*t2) + 
               4*Power(s2,6)*(1 + 30*t1 - 10*t2) + 620*t2 - 
               490*Power(t2,2) + 98*Power(t2,3) + 24*Power(t2,4) - 
               4*Power(t2,5) + 
               Power(t1,5)*(-66 - 15*t2 + 29*Power(t2,2)) - 
               Power(s2,5)*(-353 + 291*Power(t1,2) + t1*(87 - 225*t2) + 
                  149*t2 + 38*Power(t2,2)) - 
               2*Power(t1,4)*
                (197 - 279*t2 + 72*Power(t2,2) + 2*Power(t2,3)) + 
               2*Power(t1,3)*
                (217 + 120*t2 - 277*Power(t2,2) + 74*Power(t2,3)) + 
               Power(t1,2)*(909 - 1377*t2 + 419*Power(t2,2) + 
                  154*Power(t2,3) - 16*Power(t2,4)) + 
               t1*(-158 + 285*t2 - 23*Power(t2,2) - 116*Power(t2,3) + 
                  50*Power(t2,4)) + 
               Power(s2,4)*(-157 + 390*Power(t1,3) + 
                  Power(t1,2)*(341 - 531*t2) + 514*t2 - 
                  99*Power(t2,2) - 20*Power(t2,3) + 
                  t1*(-1396 + 473*t2 + 193*Power(t2,2))) + 
               Power(s2,3)*(-1379 - 315*Power(t1,4) + 1120*t2 + 
                  194*Power(t2,2) - 228*Power(t2,3) + 
                  Power(t1,3)*(-574 + 674*t2) + 
                  Power(t1,2)*(2136 - 510*t2 - 380*Power(t2,2)) + 
                  t1*(894 - 2122*t2 + 444*Power(t2,2) + 64*Power(t2,3))) \
+ Power(s2,2)*(1292 + 156*Power(t1,5) - 486*Power(t1,4)*(-1 + t2) - 
                  2090*t2 + 635*Power(t2,2) + 312*Power(t2,3) - 
                  24*Power(t2,4) + 
                  2*Power(t1,3)*(-781 + 91*t2 + 181*Power(t2,2)) - 
                  Power(t1,2)*
                   (1711 - 3260*t2 + 735*Power(t2,2) + 72*Power(t2,3)) \
+ 2*t1*(1576 - 944*t2 - 496*Power(t2,2) + 287*Power(t2,3))) + 
               s2*(195 - 45*Power(t1,6) - 337*t2 - 34*Power(t2,2) + 
                  242*Power(t2,3) - 104*Power(t2,4) + 
                  7*Power(t1,5)*(-29 + 27*t2) + 
                  Power(t1,4)*(535 + 19*t2 - 166*Power(t2,2)) + 
                  Power(t1,2)*
                   (-2207 + 528*t2 + 1352*Power(t2,2) - 494*Power(t2,3)) \
+ 2*Power(t1,3)*(684 - 1105*t2 + 267*Power(t2,2) + 16*Power(t2,3)) + 
                  t1*(-2223 + 3511*t2 - 1070*Power(t2,2) - 
                     480*Power(t2,3) + 48*Power(t2,4)))) + 
            s1*(51 - Power(t1,8) - 175*t2 + 292*Power(t2,2) - 
               218*Power(t2,3) + 52*Power(t2,4) - 2*Power(t2,5) + 
               2*Power(t1,7)*(4 + t2) + Power(s2,7)*(8 + t2) - 
               Power(t1,6)*(21 + 22*t2 + Power(t2,2)) - 
               Power(s2,6)*(-52 + Power(t1,2) - 2*t1*(-23 + t2) + 
                  32*t2 + 5*Power(t2,2)) + 
               Power(t1,5)*(115 - 66*t2 + 57*Power(t2,2)) - 
               3*Power(t1,4)*(56 + 35*t2 - 15*Power(t2,2) + 
                  17*Power(t2,3)) + 
               Power(t1,2)*(931 - 646*t2 - 486*Power(t2,2) + 
                  399*Power(t2,3) - 120*Power(t2,4)) + 
               Power(t1,3)*(-522 + 1106*t2 - 567*Power(t2,2) + 
                  126*Power(t2,3) + 8*Power(t2,4)) + 
               t1*(1191 - 2622*t2 + 1800*Power(t2,2) - 308*Power(t2,3) - 
                  82*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,5)*(-116 + 6*Power(t1,3) + 89*t2 - 
                  7*Power(t2,2) - 6*Power(t2,3) - 
                  3*Power(t1,2)*(-34 + 9*t2) + 
                  t1*(-204 + 160*t2 + 29*Power(t2,2))) - 
               Power(s2,4)*(672 + 15*Power(t1,4) + 
                  Power(t1,3)*(100 - 70*t2) - 484*t2 + 36*Power(t2,2) + 
                  126*Power(t2,3) + 
                  Power(t1,2)*(-275 + 342*t2 + 67*Power(t2,2)) - 
                  2*t1*(343 - 277*t2 + 72*Power(t2,2) + 10*Power(t2,3))) \
+ Power(s2,3)*(503 + 20*Power(t1,5) + Power(t1,4)*(20 - 85*t2) - 
                  1307*t2 + 496*Power(t2,2) + 76*Power(t2,3) - 
                  24*Power(t2,4) + 
                  2*Power(t1,3)*(-50 + 204*t2 + 39*Power(t2,2)) - 
                  Power(t1,2)*
                   (1477 - 1194*t2 + 447*Power(t2,2) + 24*Power(t2,3)) + 
                  2*t1*(1019 - 543*t2 - 6*Power(t2,2) + 203*Power(t2,3))) \
+ Power(s2,2)*(1423 - 15*Power(t1,6) - 1719*t2 + 219*Power(t2,2) + 
                  380*Power(t2,3) - 242*Power(t2,4) + 
                  6*Power(t1,5)*(7 + 9*t2) - 
                  Power(t1,4)*(90 + 292*t2 + 47*Power(t2,2)) + 
                  Power(t1,2)*
                   (-2228 + 615*t2 + 177*Power(t2,2) - 485*Power(t2,3)) \
+ Power(t1,3)*(1475 - 1148*t2 + 547*Power(t2,2) + 12*Power(t2,3)) + 
                  t1*(-1511 + 3724*t2 - 1664*Power(t2,2) + 
                     56*Power(t2,3) + 60*Power(t2,4))) + 
               s2*(-1249 + 6*Power(t1,7) + 2787*t2 - 1897*Power(t2,2) + 
                  206*Power(t2,3) + 174*Power(t2,4) - 8*Power(t2,5) - 
                  17*Power(t1,6)*(2 + t2) + 
                  Power(t1,5)*(88 + 120*t2 + 13*Power(t2,2)) - 
                  Power(t1,4)*
                   (683 - 485*t2 + 294*Power(t2,2) + 2*Power(t2,3)) + 
                  2*Power(t1,3)*
                   (515 + 46*t2 - 87*Power(t2,2) + 128*Power(t2,3)) + 
                  Power(t1,2)*
                   (1530 - 3523*t2 + 1735*Power(t2,2) - 
                     258*Power(t2,3) - 44*Power(t2,4)) + 
                  t1*(-2360 + 2392*t2 + 207*Power(t2,2) - 
                     710*Power(t2,3) + 332*Power(t2,4))))))*
       T2q(1 - s + s1 - t2,1 - s2 + t1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(s - s1 + t2)*(-1 + s2 - t1 + t2)*
       Power(-1 + s - s*s1 + s1*s2 - s1*t1 + t2,2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) + 
    (8*(-2*Power(s1,5)*Power(s2 - t1,4)*(-1 + s2 - t1 + t2) + 
         Power(s,7)*(2 - 2*t1 + 3*Power(t1,2) + 
            Power(s1,2)*(2 - 2*s2 + 3*t1 - 3*t2) + s2*(-2 + t1 - t2) + 
            2*t2 - 6*t1*t2 + 3*Power(t2,2) + 
            s1*(-4 - t1 + Power(t1,2) + t2 - 2*t1*t2 + Power(t2,2) + 
               s2*(4 - t1 + t2))) - 
         Power(s1,4)*(s2 - t1)*
          (6*Power(s2,4) - 6*Power(s2,3)*(3 + 4*t1 - 3*t2) + 
            Power(s2,2)*(36*Power(t1,2) + Power(-1 + t2,2)*(13 + t2) + 
               2*t1*(28 - 29*t2 + Power(t2,2))) + 
            t1*(6*Power(t1,3) - t1*(-15 + t2)*Power(-1 + t2,2) - 
               Power(-1 + t2,3) + 
               2*Power(t1,2)*(10 - 11*t2 + Power(t2,2))) - 
            s2*(24*Power(t1,3) + 28*t1*Power(-1 + t2,2) - 
               Power(-1 + t2,3)*t2 + 
               Power(t1,2)*(58 - 62*t2 + 4*Power(t2,2)))) + 
         Power(s1,3)*(-4*Power(s2,6)*(-1 + t2) + 
            2*Power(s2,5)*(-1 + 10*t1*(-1 + t2) + 5*t2 - Power(t2,2)) + 
            Power(s2,4)*(5 - 40*Power(t1,2)*(-1 + t2) - 15*t2 + 
               9*Power(t2,2) + Power(t2,3) + 
               2*t1*(6 - 23*t2 + 2*Power(t2,2))) + 
            t1*(Power(-1 + t2,4) + 2*t1*Power(-1 + t2,3)*(-23 + 3*t2) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (54 - 11*t2 + Power(t2,2)) - 
               2*Power(t1,4)*(-3 + 5*t2 + Power(t2,2)) + 
               6*Power(t1,3)*(1 - 3*t2 + 2*Power(t2,2))) + 
            s2*(4*Power(t1,5)*(-1 + t2) - Power(-1 + t2,4)*t2 - 
               Power(t1,2)*Power(-1 + t2,2)*
                (162 - 31*t2 + Power(t2,2)) - 
               t1*Power(-1 + t2,3)*(-86 + 5*t2 + Power(t2,2)) + 
               Power(t1,4)*(-24 + 46*t2 + 8*Power(t2,2)) - 
               3*Power(t1,3)*(7 - 21*t2 + 13*Power(t2,2) + Power(t2,3))) \
+ Power(s2,3)*(40*Power(t1,3)*(-1 + t2) + 
               Power(-1 + t2,2)*(-54 + 9*t2 + Power(t2,2)) + 
               2*Power(t1,2)*(-15 + 44*t2 + Power(t2,2)) - 
               t1*(19 - 57*t2 + 33*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s2,2)*(-20*Power(t1,4)*(-1 + t2) - 
               t1*Power(-1 + t2,2)*(-162 + 29*t2 + Power(t2,2)) + 
               Power(-1 + t2,3)*(-39 - 3*t2 + 2*Power(t2,2)) - 
               2*Power(t1,3)*(-19 + 44*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(29 - 87*t2 + 51*Power(t2,2) + 7*Power(t2,3)))\
) - Power(-1 + t2,2)*(-2*Power(-1 + t2,3)*(1 + 3*t2) + 
            Power(t1,4)*(2 - 3*t2 + Power(t2,2)) - 
            t1*Power(-1 + t2,2)*(-21 - 5*t2 + 5*Power(t2,2)) - 
            Power(s2,4)*(8 - 12*t2 + 3*Power(t2,2) + Power(t2,3)) - 
            Power(t1,3)*(25 - 39*t2 + 10*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,2)*(20 - t2 - 28*Power(t2,2) + 8*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,3)*(20 - 40*t2 + 22*Power(t2,2) - 7*Power(t2,3) + 
               3*Power(t2,4) + 
               t1*(24 - 39*t2 + 14*Power(t2,2) + Power(t2,3))) + 
            Power(s2,2)*(37 - 27*t2 - 34*Power(t2,2) + 30*Power(t2,3) - 
               6*Power(t2,4) + 
               Power(t1,2)*(-22 + 39*t2 - 18*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-65 + 115*t2 - 44*Power(t2,2) + 4*Power(t2,3) - 
                  4*Power(t2,4))) + 
            s2*(-(Power(t1,3)*(-4 + t2)*Power(-1 + t2,2)) + 
               Power(-1 + t2,2)*(-38 + 10*t2 + 7*Power(t2,2)) + 
               t1*(-58 + 35*t2 + 47*Power(t2,2) - 25*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*(70 - 114*t2 + 32*Power(t2,2) + 
                  5*Power(t2,3) + Power(t2,4)))) + 
         Power(s1,2)*(4*Power(s2,6)*(2 - 3*t2 + Power(t2,2)) + 
            Power(s2,5)*(-28 + 58*t2 - 32*Power(t2,2) + 4*Power(t2,3) - 
               20*t1*(2 - 3*t2 + Power(t2,2))) + 
            Power(s2,4)*(-33 + 7*t2 + 45*Power(t2,2) - 21*Power(t2,3) + 
               2*Power(t2,4) + 
               40*Power(t1,2)*(2 - 3*t2 + Power(t2,2)) + 
               t1*(135 - 267*t2 + 137*Power(t2,2) - 15*Power(t2,3))) + 
            Power(s2,3)*(-40*Power(t1,3)*(2 - 3*t2 + Power(t2,2)) + 
               Power(-1 + t2,2)*(46 + 11*t2 + Power(t2,3)) + 
               Power(t1,2)*(-258 + 484*t2 - 226*Power(t2,2) + 
                  20*Power(t2,3)) + 
               t1*(108 + 6*t2 - 172*Power(t2,2) + 62*Power(t2,3) - 
                  4*Power(t2,4))) + 
            Power(s2,2)*(20*Power(t1,4)*(2 - 3*t2 + Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(124 + 49*t2 + Power(t2,3)) - 
               Power(-1 + t2,3)*
                (60 - 8*t2 - 11*Power(t2,2) + Power(t2,3)) - 
               2*Power(t1,3)*
                (-122 + 215*t2 - 88*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(-123 - 70*t2 + 258*Power(t2,2) - 
                  66*Power(t2,3) + Power(t2,4))) + 
            t1*(-(Power(-1 + t2,4)*(-53 + 8*t2)) - 
               2*t1*Power(-1 + t2,3)*(34 - 14*t2 + Power(t2,2)) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (-29 - 34*t2 + 5*Power(t2,2)) + 
               Power(t1,4)*(21 - 31*t2 + 7*Power(t2,2) + Power(t2,3)) - 
               Power(t1,3)*(6 + 37*t2 - 49*Power(t2,2) + 
                  5*Power(t2,3) + Power(t2,4))) + 
            s2*(-4*Power(t1,5)*(2 - 3*t2 + Power(t2,2)) + 
               Power(-1 + t2,4)*(-47 + t2 + Power(t2,2)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (-107 - 72*t2 + 5*Power(t2,2)) - 
               t1*Power(-1 + t2,3)*(-130 + 41*t2 + 5*Power(t2,2)) - 
               2*Power(t1,4)*(57 - 93*t2 + 31*Power(t2,2)) + 
               2*Power(t1,3)*(27 + 47*t2 - 90*Power(t2,2) + 
                  15*Power(t2,3) + Power(t2,4)))) + 
         s1*(-1 + t2)*(Power(t1,5)*Power(-1 + t2,2) + 
            Power(-1 + t2,4)*(-19 + 3*t2) + 
            t1*Power(-1 + t2,3)*(32 - 26*t2 + Power(t2,2)) + 
            8*Power(s2,5)*(2 - 3*t2 + Power(t2,2)) - 
            2*Power(t1,2)*Power(-1 + t2,2)*
             (-25 - 13*t2 + 5*Power(t2,2)) - 
            Power(t1,4)*(45 - 67*t2 + 15*Power(t2,2) + 3*Power(t2,3)) + 
            Power(t1,3)*(33 + 20*t2 - 71*Power(t2,2) + 16*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,4)*(-44 + 90*t2 - 49*Power(t2,2) + 8*Power(t2,3) - 
               Power(t2,4) + 
               t1*(-64 + 97*t2 - 34*Power(t2,2) + Power(t2,3))) + 
            s2*(Power(-1 + t2,3)*(-20 + 5*t2 + 8*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(-134 - 15*t2 + 17*Power(t2,2)) - 
               Power(t1,4)*(-13 + 19*t2 - 7*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,2)*(-139 + 15*t2 + 186*Power(t2,2) - 
                  59*Power(t2,3) - 3*Power(t2,4)) + 
               Power(t1,3)*(182 - 296*t2 + 95*Power(t2,2) + 
                  2*Power(t2,3) + Power(t2,4))) + 
            Power(s2,3)*(-72 + 46*t2 + 66*Power(t2,2) - 
               49*Power(t2,3) + 10*Power(t2,4) - Power(t2,5) + 
               Power(t1,2)*(95 - 145*t2 + 53*Power(t2,2) - 
                  3*Power(t2,3)) + 
               t1*(180 - 342*t2 + 163*Power(t2,2) - 20*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s2,2)*(-(Power(-1 + t2,2)*
                  (-83 + 9*t2 + 8*Power(t2,2))) + 
               Power(t1,3)*(-61 + 93*t2 - 35*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(-273 + 481*t2 - 194*Power(t2,2) + 
                  13*Power(t2,3) - 3*Power(t2,4)) + 
               t1*(178 - 81*t2 - 181*Power(t2,2) + 92*Power(t2,3) - 
                  9*Power(t2,4) + Power(t2,5)))) + 
         Power(s,6)*(-6 + 25*t1 - 16*Power(t1,2) + 5*Power(t1,3) + t2 + 
            20*t1*t2 + Power(t1,2)*t2 - 4*Power(t2,2) - 
            17*t1*Power(t2,2) + 11*Power(t2,3) + 
            Power(s2,2)*(6 - t1 + 2*t2) + 
            Power(s1,3)*(-4 + 3*s2 - 5*t1 + 6*t2) - 
            s2*(3 + 4*Power(t1,2) + t1*(8 - 19*t2) + 16*t2 + 
               15*Power(t2,2)) + 
            Power(s1,2)*(1 + 9*Power(s2,2) + t1 + 5*Power(t1,2) + 
               10*t2 + 5*t1*t2 - 10*Power(t2,2) + 
               s2*(-11 - 15*t1 + 5*t2)) + 
            s1*(9 - 14*t2 - 4*Power(t2,2) + 4*Power(t2,3) + 
               Power(t1,2)*(-15 + 4*t2) + 
               t1*(-24 + 19*t2 - 8*Power(t2,2)) + 
               Power(s2,2)*(2*t1 - 3*(5 + t2)) + 
               s2*(11 - 2*Power(t1,2) + 9*t2 + Power(t2,2) + 
                  t1*(25 + t2)))) + 
         Power(s,5)*(-4 - 86*t1 + 60*Power(t1,2) - 27*Power(t1,3) + 
            2*Power(t1,4) + Power(s1,4)*(2 + t1 - 3*t2) + 14*t2 + 
            48*t1*t2 + 3*Power(t1,2)*t2 + 14*Power(t1,3)*t2 + 
            2*Power(t2,2) + 45*t1*Power(t2,2) - 
            19*Power(t1,2)*Power(t2,2) - 21*Power(t2,3) - 
            12*t1*Power(t2,3) + 15*Power(t2,4) - Power(s2,3)*(8 + t2) + 
            Power(s1,3)*(8 - 7*Power(s2,2) - 5*Power(t1,2) + 
               s2*(3 + 13*t1 - 8*t2) - 5*t1*(-2 + t2) - 23*t2 + 
               14*Power(t2,2)) + 
            Power(s2,2)*(-1 + 2*Power(t1,2) + t1*(21 - 16*t2) + 26*t2 + 
               22*Power(t2,2)) + 
            s2*(31 - 4*Power(t1,3) + 24*t2 - 11*Power(t2,2) - 
               46*Power(t2,3) + Power(t1,2)*(14 + 3*t2) + 
               t1*(-32 - 105*t2 + 47*Power(t2,2))) + 
            Power(s1,2)*(-21 - 20*Power(s2,3) + 10*Power(t1,3) + 
               Power(t1,2)*(43 - 4*t2) + 2*t2 + 29*Power(t2,2) - 
               15*Power(t2,3) + Power(s2,2)*(30 + 49*t1 + 2*t2) + 
               t1*(32 - 42*t2 + 9*Power(t2,2)) - 
               s2*(-15 + 39*Power(t1,2) + 30*t2 - 22*Power(t2,2) + 
                  t1*(67 + 5*t2))) + 
            s1*(16 - 2*Power(t1,4) + 5*Power(t1,3)*(-3 + t2) + 2*t2 - 
               4*Power(t2,2) - 19*Power(t2,3) + 6*Power(t2,4) + 
               2*Power(t1,2)*(-22 - 15*t2 + Power(t2,2)) - 
               Power(s2,2)*(19 + t1*(73 - 5*t2) + 32*t2 + 
                  9*Power(t2,2)) + 
               t1*(46 - 92*t2 + 64*Power(t2,2) - 11*Power(t2,3)) + 
               Power(s2,3)*(-t1 + 3*(9 + t2)) + 
               s2*(-52 + 3*Power(t1,3) + Power(t1,2)*(61 - 13*t2) + 
                  24*t2 + 27*Power(t2,2) - 6*Power(t2,3) + 
                  t1*(87 + 28*t2 + 16*Power(t2,2))))) + 
         Power(s,4)*(35 + 4*Power(s2,4) + 122*t1 - 161*Power(t1,2) + 
            73*Power(t1,3) - 10*Power(t1,4) + Power(s1,5)*(-s2 + t1) - 
            108*t2 - 196*t1*t2 + 76*Power(t1,2)*t2 - 61*Power(t1,3)*t2 + 
            7*Power(t1,4)*t2 + 87*Power(t2,2) + 82*t1*Power(t2,2) + 
            90*Power(t1,2)*Power(t2,2) + 10*Power(t1,3)*Power(t2,2) - 
            Power(t2,3) + 3*t1*Power(t2,3) - 
            32*Power(t1,2)*Power(t2,3) - 22*Power(t2,4) + 
            6*t1*Power(t2,4) + 9*Power(t2,5) + 
            Power(s2,3)*(24 + 3*t1*(-4 + t2) - 28*t2 - 11*Power(t2,2)) - 
            Power(s1,4)*(9 + 10*Power(s2,2) + 9*Power(t1,2) + 
               t1*(15 - 7*t2) - 14*t2 + 5*Power(t2,2) + 
               s2*(-11 - 19*t1 + 3*t2)) + 
            Power(s2,2)*(-90 - 10*t2 + 5*Power(t2,2) + 62*Power(t2,3) + 
               Power(t1,2)*(2 + t2) + t1*(-43 + 135*t2 - 38*Power(t2,2))\
) + Power(s1,3)*(-3 + 15*Power(s2,3) - 9*Power(t1,3) + 36*t2 - 
               47*Power(t2,2) + 14*Power(t2,3) + 
               Power(t1,2)*(-41 + 10*t2) + 
               Power(s2,2)*(-27 - 41*t1 + 11*t2) + 
               s2*(3 + 35*Power(t1,2) + t1*(65 - 18*t2) + 2*t2 - 
                  20*Power(t2,2)) + t1*(-46 + 71*t2 - 10*Power(t2,2))) + 
            s2*(-27 + Power(t1,3)*(16 - 11*t2) - 27*t2 + 
               70*Power(t2,2) + 25*Power(t2,3) - 58*Power(t2,4) + 
               Power(t1,2)*(-54 - 46*t2 + 39*Power(t2,2)) + 
               t1*(244 + t2 - 215*Power(t2,2) + 30*Power(t2,3))) + 
            Power(s1,2)*(13 + 25*Power(s2,4) + 10*Power(t1,4) + 5*t2 - 
               64*Power(t2,2) + 59*Power(t2,3) - 13*Power(t2,4) + 
               Power(t1,3)*(59 + 6*t2) - 
               Power(s2,3)*(42 + 84*t1 + 19*t2) - 
               2*Power(t1,2)*(8 - 39*t2 + 10*Power(t2,2)) + 
               t1*(-117 + 206*t2 - 141*Power(t2,2) + 17*Power(t2,3)) + 
               Power(s2,2)*(-44 + 103*Power(t1,2) + 62*t2 - 
                  21*Power(t2,2) + t1*(154 + 41*t2)) - 
               s2*(-46 + 54*Power(t1,3) + 4*t2 + 37*Power(t2,2) - 
                  30*Power(t2,3) + Power(t1,2)*(171 + 28*t2) + 
                  t1*(-38 + 99*t2 - 22*Power(t2,2)))) - 
            s1*(35 + Power(t1,5) + 4*Power(t1,4)*(-1 + t2) - 28*t2 - 
               24*Power(t2,2) - 2*Power(t2,3) + 23*Power(t2,4) - 
               4*Power(t2,5) + Power(s2,4)*(24 + t2) + 
               Power(t1,3)*(24 + 66*t2 - 14*Power(t2,2)) - 
               Power(s2,3)*(-13 + Power(t1,2) + t1*(81 - 5*t2) + 
                  61*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(-245 + 200*t2 - 22*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-120 + 140*t2 + 48*Power(t2,2) - 63*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,2)*(-189 + 3*Power(t1,3) + 
                  Power(t1,2)*(86 - 9*t2) + 77*t2 + 34*Power(t2,2) + 
                  6*Power(t2,3) + t1*(11 + 182*t2 + 5*Power(t2,2))) - 
               s2*(-40 + 3*Power(t1,4) + 58*t2 - 62*Power(t2,2) + 
                  68*Power(t2,3) - 14*Power(t2,4) + 
                  Power(t1,3)*(25 + t2) + 
                  Power(t1,2)*(48 + 187*t2 - 20*Power(t2,2)) + 
                  t1*(-475 + 384*t2 - 70*Power(t2,2) + 30*Power(t2,3))))) \
+ Power(s,3)*(-45 - 47*t1 + 252*Power(t1,2) - 136*Power(t1,3) + 
            20*Power(t1,4) + 191*t2 + 138*t1*t2 - 296*Power(t1,2)*t2 + 
            156*Power(t1,3)*t2 - 28*Power(t1,4)*t2 - 266*Power(t2,2) - 
            184*t1*Power(t2,2) - 21*Power(t1,2)*Power(t2,2) - 
            31*Power(t1,3)*Power(t2,2) + 9*Power(t1,4)*Power(t2,2) + 
            136*Power(t2,3) + 126*t1*Power(t2,3) + 
            109*Power(t1,2)*Power(t2,3) - 4*Power(t1,3)*Power(t2,3) - 
            11*Power(t2,4) - 43*t1*Power(t2,4) - 
            17*Power(t1,2)*Power(t2,4) - 7*Power(t2,5) + 
            10*t1*Power(t2,5) + 2*Power(t2,6) + 
            Power(s1,5)*(s2 - t1)*(-5 + 8*s2 - 8*t1 + 5*t2) + 
            Power(s2,4)*(-20 + 16*t2 + Power(t2,2)) + 
            Power(s2,3)*(t2*(48 - 11*t2 - 30*Power(t2,2)) + 
               t1*(60 - 60*t2 + 8*Power(t2,2))) - 
            Power(s2,2)*(-247 + 234*t2 + 3*Power(t2,2) + 
               57*Power(t2,3) - 74*Power(t2,4) + 
               2*Power(t1,2)*(20 - 22*t2 + 5*Power(t2,2)) + 
               t1*(64 + 172*t2 - 231*Power(t2,2) + 24*Power(t2,3))) - 
            s2*(85 - 241*t2 + 244*Power(t2,2) - 98*Power(t2,3) - 
               23*Power(t2,4) + 33*Power(t2,5) + 
               4*Power(t1,3)*(5 - 7*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-200 + 32*t2 + 189*Power(t2,2) - 
                  58*Power(t2,3)) + 
               t1*(521 - 574*t2 - 42*Power(t2,2) + 132*Power(t2,3) + 
                  17*Power(t2,4))) + 
            Power(s1,4)*(17*Power(s2,3) - 13*Power(t1,3) - 
               3*(-5 + t2)*Power(-1 + t2,2) - 
               Power(s2,2)*(6 + 47*t1 + t2) - Power(t1,2)*(4 + 3*t2) + 
               t1*(21 - 32*t2 + 11*Power(t2,2)) + 
               s2*(-13 + 43*Power(t1,2) + 16*t2 - 3*Power(t2,2) + 
                  2*t1*(5 + 2*t2))) + 
            Power(s1,3)*(-23*Power(s2,4) - 8*Power(t1,4) + 
               Power(s2,3)*(61 + 81*t1 - 8*t2) - 
               Power(t1,3)*(83 + 4*t2) + 
               Power(-1 + t2,2)*(-3 - 33*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-3 + 13*t2 + 22*Power(t2,2)) + 
               t1*(61 - 144*t2 + 101*Power(t2,2) - 18*Power(t2,3)) + 
               Power(s2,2)*(-6 - 101*Power(t1,2) + 19*t2 + 
                  19*Power(t2,2) + t1*(-205 + 12*t2)) + 
               s2*(1 + 227*Power(t1,2) + 51*Power(t1,3) - 16*t2 + 
                  33*Power(t2,2) - 18*Power(t2,3) + 
                  t1*(15 - 44*t2 - 35*Power(t2,2)))) - 
            Power(s1,2)*(16*Power(s2,5) - 3*Power(t1,5) - 
               Power(t1,4)*(24 + 19*t2) - 
               Power(s2,4)*(8 + 67*t1 + 35*t2) + 
               2*Power(t1,3)*(55 - 78*t2 + 11*Power(t2,2)) + 
               3*Power(-1 + t2,2)*
                (-7 + 7*t2 - 14*Power(t2,2) + 2*Power(t2,3)) + 
               2*Power(t1,2)*
                (95 - 112*t2 + 26*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(-89 + 170*t2 - 181*Power(t2,2) + 112*Power(t2,3) - 
                  12*Power(t2,4)) + 
               4*Power(s2,3)*
                (-35 + 27*Power(t1,2) + 33*t2 - 4*Power(t2,2) + 
                  15*t1*(1 + 2*t2)) - 
               2*Power(s2,2)*
                (-51 + 41*Power(t1,3) + 61*t2 - 13*Power(t2,2) - 
                  9*Power(t2,3) + Power(t1,2)*(60 + 77*t2) - 
                  4*t1*(51 - 55*t2 + 7*Power(t2,2))) + 
               s2*(65 + 28*Power(t1,4) + 6*t2 - 137*Power(t2,2) + 
                  92*Power(t2,3) - 26*Power(t2,4) + 
                  Power(t1,3)*(92 + 88*t2) + 
                  Power(t1,2)*(-378 + 464*t2 - 62*Power(t2,2)) + 
                  2*t1*(-166 + 226*t2 - 85*Power(t2,2) + Power(t2,3)))) \
+ s1*(8*Power(s2,5) + Power(t1,5)*(4 - 3*t2) - Power(t1,4)*(25 + 3*t2) + 
               Power(s2,4)*(60 + t1*(-32 + t2) - 63*t2 - 
                  4*Power(t2,2)) + 
               Power(t1,3)*(213 - 60*t2 - 88*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(-300 + 443*t2 - 292*Power(t2,2) + 
                  84*Power(t2,3) - 11*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (-32 + 67*t2 - 44*Power(t2,2) - 8*Power(t2,3) + 
                  Power(t2,4)) + 
               t1*(-417 + 886*t2 - 587*Power(t2,2) + 104*Power(t2,3) + 
                  13*Power(t2,4) + Power(t2,5)) + 
               Power(s2,3)*(-209 + 44*Power(t1,2) + 84*t2 + 
                  32*Power(t2,2) + 14*Power(t2,3) + 
                  t1*(-173 + 232*t2 - 6*Power(t2,2))) - 
               Power(s2,2)*(246 - 185*t2 - 39*Power(t2,2) + 
                  60*Power(t2,3) - 6*Power(t2,4) + 
                  Power(t1,3)*(20 + 6*t2) + 
                  Power(t1,2)*(-141 + 278*t2 - 24*Power(t2,2)) + 
                  t1*(-675 + 316*t2 + 100*Power(t2,2) + 24*Power(t2,3))) \
+ s2*(290 - 510*t2 + 281*Power(t2,2) - 122*Power(t2,3) + 
                  72*Power(t2,4) - 11*Power(t2,5) + 
                  Power(t1,4)*(-4 + 8*t2) + 
                  Power(t1,3)*(-3 + 112*t2 - 14*Power(t2,2)) + 
                  Power(t1,2)*
                   (-679 + 292*t2 + 156*Power(t2,2) - 2*Power(t2,3)) + 
                  t1*(574 - 748*t2 + 423*Power(t2,2) - 116*Power(t2,3) + 
                     19*Power(t2,4))))) + 
         Power(s,2)*(17 + 192*s2 - 287*Power(s2,2) - 60*Power(s2,3) + 
            36*Power(s2,4) - 62*t1 + 530*s2*t1 + 243*Power(s2,2)*t1 - 
            108*Power(s2,3)*t1 - 219*Power(t1,2) - 344*s2*Power(t1,2) + 
            88*Power(s2,2)*Power(t1,2) + 161*Power(t1,3) + 
            4*s2*Power(t1,3) - 20*Power(t1,4) - 119*t2 - 577*s2*t2 + 
            473*Power(s2,2)*t2 + 80*Power(s2,3)*t2 - 60*Power(s2,4)*t2 + 
            139*t1*t2 - 953*s2*t1*t2 - 266*Power(s2,2)*t1*t2 + 
            198*Power(s2,3)*t1*t2 + 376*Power(t1,2)*t2 + 
            468*s2*Power(t1,2)*t2 - 174*Power(s2,2)*Power(t1,2)*t2 - 
            282*Power(t1,3)*t2 - 6*s2*Power(t1,3)*t2 + 
            42*Power(t1,4)*t2 + 262*Power(t2,2) + 625*s2*Power(t2,2) - 
            137*Power(s2,2)*Power(t2,2) - 27*Power(s2,3)*Power(t2,2) + 
            21*Power(s2,4)*Power(t2,2) - 47*t1*Power(t2,2) + 
            432*s2*t1*Power(t2,2) - 123*Power(s2,2)*t1*Power(t2,2) - 
            96*Power(s2,3)*t1*Power(t2,2) - 
            141*Power(t1,2)*Power(t2,2) + 
            39*s2*Power(t1,2)*Power(t2,2) + 
            102*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
            111*Power(t1,3)*Power(t2,2) - 27*Power(t1,4)*Power(t2,2) - 
            242*Power(t2,3) - 306*s2*Power(t2,3) - 
            40*Power(s2,2)*Power(t2,3) + 43*Power(s2,3)*Power(t2,3) + 
            3*Power(s2,4)*Power(t2,3) - 96*t1*Power(t2,3) + 
            16*s2*t1*Power(t2,3) + 129*Power(s2,2)*t1*Power(t2,3) + 
            6*Power(s2,3)*t1*Power(t2,3) - 54*Power(t1,2)*Power(t2,3) - 
            187*s2*Power(t1,2)*Power(t2,3) - 
            16*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
            15*Power(t1,3)*Power(t2,3) + 2*s2*Power(t1,3)*Power(t2,3) + 
            5*Power(t1,4)*Power(t2,3) + 89*Power(t2,4) + 
            78*s2*Power(t2,4) - 49*Power(s2,2)*Power(t2,4) - 
            34*Power(s2,3)*Power(t2,4) + 90*t1*Power(t2,4) + 
            11*Power(s2,2)*t1*Power(t2,4) + 39*Power(t1,2)*Power(t2,4) + 
            30*s2*Power(t1,2)*Power(t2,4) - 7*Power(t1,3)*Power(t2,4) - 
            7*Power(t2,5) - 5*s2*Power(t2,5) + 
            40*Power(s2,2)*Power(t2,5) - 27*t1*Power(t2,5) - 
            25*s2*t1*Power(t2,5) - Power(t1,2)*Power(t2,5) - 
            7*s2*Power(t2,6) + 3*t1*Power(t2,6) - 
            3*Power(s1,5)*(s2 - t1)*
             (4*Power(s2,2) + 4*Power(t1,2) - 2*t1*(-1 + t2) - 
               Power(-1 + t2,2) + s2*(-2 - 8*t1 + 2*t2)) - 
            Power(s1,4)*(10*Power(s2,4) + 8*Power(t1,4) + 
               Power(s2,3)*(15 - 38*t1 - 5*t2) + 
               (-5 + t2)*Power(-1 + t2,3) - Power(t1,3)*(9 + t2) - 
               t1*Power(-1 + t2,2)*(17 + 9*t2) + 
               Power(t1,2)*(-38 + 35*t2 + 3*Power(t2,2)) + 
               s2*(-34*Power(t1,3) - 3*Power(t1,2)*(-11 + t2) - 
                  82*t1*(-1 + t2) + Power(-1 + t2,2)*(25 + t2)) + 
               Power(s2,2)*(-44 + 54*Power(t1,2) + 47*t2 - 
                  3*Power(t2,2) + t1*(-39 + 9*t2))) + 
            Power(s1,3)*(16*Power(s2,5) - 2*Power(t1,5) - 
               2*Power(t1,4)*(30 + 7*t2) - 
               Power(s2,4)*(35 + 68*t1 + 11*t2) + 
               Power(-1 + t2,3)*(-19 - 15*t2 + 2*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(34 - 29*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(39 - 31*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(9 - 58*t2 + 43*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s2,3)*(-21 + 110*Power(t1,2) + 25*t2 - 
                  24*Power(t2,2) + 53*t1*(3 + t2)) - 
               Power(s2,2)*(18 + 82*Power(t1,3) + 7*t2 - 
                  22*Power(t2,2) - 3*Power(t2,3) + 
                  3*Power(t1,2)*(91 + 29*t2) + 
                  t1*(-87 + 93*t2 - 66*Power(t2,2))) + 
               s2*(26*Power(t1,4) + Power(t1,3)*(209 + 59*t2) - 
                  Power(-1 + t2,2)*(4 - 31*t2 + 13*Power(t2,2)) - 
                  3*Power(t1,2)*(35 - 33*t2 + 18*Power(t2,2)) - 
                  t1*(-1 - 89*t2 + 89*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,2)*(4*Power(s2,6) + Power(t1,5)*(5 + 7*t2) - 
               4*Power(s2,5)*(-6 + 5*t1 + 7*t2) + 
               Power(t1,4)*(-86 + 59*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(10 - 30*t2 + 96*Power(t2,2) - 
                  22*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (-38 - 3*t2 - 14*Power(t2,2) + Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (37 - 59*t2 - 4*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(313 - 536*t2 + 328*Power(t2,2) - 
                  112*Power(t2,3) + 7*Power(t2,4)) + 
               Power(s2,4)*(-130 + 40*Power(t1,2) + 107*t2 - 
                  3*Power(t2,2) + t1*(-89 + 119*t2)) - 
               2*Power(s2,3)*
                (36 + 20*Power(t1,3) - 10*t2 + 6*Power(t2,2) - 
                  5*Power(t2,3) + Power(t1,2)*(-59 + 98*t2) + 
                  t1*(-250 + 205*t2 - 4*Power(t2,2))) + 
               Power(s2,2)*(253 + 20*Power(t1,4) - 352*t2 + 
                  122*Power(t2,2) - 12*Power(t2,3) - 11*Power(t2,4) + 
                  2*Power(t1,3)*(-30 + 77*t2) + 
                  6*Power(t1,2)*(-116 + 93*t2) - 
                  2*t1*(-79 + 38*t2 - 60*Power(t2,2) + 20*Power(t2,3))) \
- s2*(4*Power(t1,5) + Power(t1,4)*(-2 + 56*t2) + 
                  2*Power(t1,3)*(-206 + 157*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*
                   (96 - 86*t2 + 204*Power(t2,2) - 52*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (-12 + 94*t2 - 71*Power(t2,2) + 13*Power(t2,3)) + 
                  t1*(601 - 1012*t2 + 612*Power(t2,2) - 
                     216*Power(t2,3) + 15*Power(t2,4)))) + 
            s1*(8*Power(s2,5)*(-4 + 3*t2) - 
               3*Power(t1,5)*(2 - 3*t2 + Power(t2,2)) - 
               Power(-1 + t2,3)*
                (95 - 63*t2 + 33*Power(t2,2) + Power(t2,3)) + 
               Power(t1,4)*(99 - 50*t2 - 21*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (436 - 337*t2 + 89*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,3)*(-307 + 335*t2 - 52*Power(t2,2) - 
                  36*Power(t2,3) + 2*Power(t2,4)) + 
               Power(t1,2)*(68 - 110*t2 + 147*Power(t2,2) - 
                  158*Power(t2,3) + 57*Power(t2,4) - 4*Power(t2,5)) + 
               Power(s2,4)*(4 + 63*t2 - 45*Power(t2,2) - 
                  6*Power(t2,3) + t1*(128 - 99*t2 + 3*Power(t2,2))) + 
               Power(s2,3)*(401 - 497*t2 + 148*Power(t2,2) + 
                  6*Power(t2,4) - 
                  6*Power(t1,2)*(31 - 24*t2 + Power(t2,2)) + 
                  t1*(-105 - 175*t2 + 198*Power(t2,2) + 2*Power(t2,3))) \
+ Power(s2,2)*(1 + Power(t1,3)*(110 - 78*t2) + 190*t2 - 
                  354*Power(t2,2) + 225*Power(t2,3) - 71*Power(t2,4) + 
                  9*Power(t2,5) + 
                  3*Power(t1,2)*
                   (99 + 37*t2 - 94*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*(-1153 + 1473*t2 - 516*Power(t2,2) + 
                     44*Power(t2,3) - 22*Power(t2,4))) + 
               s2*(2*Power(t1,4)*(-7 + 3*Power(t2,2)) + 
                  Power(t1,3)*
                   (-295 + 51*t2 + 150*Power(t2,2) - 18*Power(t2,3)) - 
                  3*Power(-1 + t2,2)*
                   (113 - 34*t2 - 12*Power(t2,2) - 7*Power(t2,3) + 
                     Power(t2,4)) + 
                  Power(t1,2)*
                   (1059 - 1311*t2 + 420*Power(t2,2) - 8*Power(t2,3) + 
                     14*Power(t2,4)) + 
                  t1*(-72 - 40*t2 + 99*Power(t2,2) + 47*Power(t2,3) - 
                     35*Power(t2,4) + Power(t2,5))))) + 
         s*(Power(s1,5)*(s2 - t1)*
             (8*Power(s2,3) - 8*Power(t1,3) + 12*s2*t1*(1 + 2*t1 - t2) - 
               6*Power(s2,2)*(1 + 4*t1 - t2) + 6*Power(t1,2)*(-1 + t2) + 
               Power(-1 + t2,3)) + 
            Power(s1,4)*(2*Power(s2,5) - 2*Power(t1,5) - 
               Power(t1,2)*(-32 + t2)*Power(-1 + t2,2) + 
               Power(-1 + t2,4) + 2*t1*Power(-1 + t2,4) + 
               2*Power(t1,4)*(7 + t2) - 2*Power(s2,4)*(-9 + 5*t1 + t2) + 
               Power(s2,3)*(-47 + 20*Power(t1,2) + 4*t1*(-17 + t2) + 
                  48*t2 - Power(t2,2)) + 
               Power(t1,3)*(47 - 48*t2 + Power(t2,2)) + 
               Power(s2,2)*(96*Power(t1,2) - 20*Power(t1,3) + 
                  Power(-1 + t2,2)*(30 + t2) + 
                  3*t1*(47 - 48*t2 + Power(t2,2))) + 
               s2*(10*Power(t1,4) - 62*t1*Power(-1 + t2,2) + 
                  Power(-1 + t2,4) - 4*Power(t1,3)*(15 + t2) - 
                  3*Power(t1,2)*(47 - 48*t2 + Power(t2,2)))) - 
            Power(s1,3)*(4*Power(s2,6) + Power(-1 + t2,4)*(1 + 2*t2) + 
               2*Power(t1,5)*(9 + 2*t2) - 2*Power(s2,5)*(10*t1 + 7*t2) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (-16 - 13*t2 + Power(t2,2)) + 
               t1*Power(-1 + t2,3)*(57 - 4*t2 + Power(t2,2)) + 
               2*Power(t1,4)*(-8 + 9*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(39 - 32*t2 + Power(t2,2) - 8*Power(t2,3)) + 
               Power(s2,4)*(-7 + 40*Power(t1,2) + 28*t2 - 
                  13*Power(t2,2) + 2*t1*(7 + 32*t2)) + 
               Power(s2,3)*(-37 - 40*Power(t1,3) + 20*t2 + 
                  17*Power(t2,2) - 4*Power(t1,2)*(15 + 28*t2) + 
                  t1*(37 - 102*t2 + 33*Power(t2,2))) + 
               Power(s2,2)*(20*Power(t1,4) + Power(t1,3)*(96 + 92*t2) - 
                  Power(-1 + t2,2)*(41 - 17*t2 + 4*Power(t2,2)) - 
                  3*Power(t1,2)*(23 - 46*t2 + 7*Power(t2,2)) - 
                  3*t1*(-39 + 28*t2 + 7*Power(t2,2) + 4*Power(t2,3))) + 
               s2*(-4*Power(t1,5) - 34*Power(t1,4)*(2 + t2) + 
                  Power(t1,3)*(55 - 82*t2 - 5*Power(t2,2)) - 
                  2*t1*Power(-1 + t2,2)*(-26 - 3*t2 + Power(t2,2)) + 
                  2*Power(-1 + t2,3)*(-25 - 4*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*
                   (-119 + 96*t2 + 3*Power(t2,2) + 20*Power(t2,3)))) + 
            Power(s1,2)*(4*Power(s2,6)*(-3 + 2*t2) + 
               Power(-1 + t2,4)*(26 + 2*t2 + Power(t2,2)) + 
               2*t1*Power(-1 + t2,3)*(44 - 12*t2 + 3*Power(t2,2)) + 
               Power(t1,5)*(-33 + 12*t2 + 5*Power(t2,2)) + 
               Power(t1,4)*(44 - 35*t2 + 30*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (-221 + 46*t2 - 23*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(46 - 94*t2 + 48*Power(t2,2) + 
                  4*Power(t2,3) - 4*Power(t2,4)) - 
               4*Power(s2,5)*
                (5*t1*(-3 + 2*t2) + 2*(-3 + t2 + Power(t2,2))) + 
               Power(s2,4)*(116 - 147*t2 + 78*Power(t2,2) - 
                  11*Power(t2,3) + 40*Power(t1,2)*(-3 + 2*t2) + 
                  t1*(-133 + 48*t2 + 37*Power(t2,2))) - 
               2*Power(s2,3)*
                (19 - 71*t2 + 88*Power(t2,2) - 38*Power(t2,3) + 
                  2*Power(t2,4) + 20*Power(t1,3)*(-3 + 2*t2) + 
                  2*Power(t1,2)*(-72 + 27*t2 + 17*Power(t2,2)) - 
                  4*t1*(-51 + 64*t2 - 36*Power(t2,2) + 5*Power(t2,3))) + 
               s2*(Power(t1,5)*(12 - 8*t2) - 
                  4*Power(t1,4)*(-40 + 15*t2 + 7*Power(t2,2)) + 
                  2*Power(-1 + t2,3)*
                   (-33 + 5*t2 - 8*Power(t2,2) + Power(t2,3)) + 
                  24*Power(t1,3)*
                   (-11 + 12*t2 - 8*Power(t2,2) + Power(t2,3)) - 
                  t1*Power(-1 + t2,2)*
                   (-439 + 53*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-137 + 354*t2 - 302*Power(t2,2) + 84*Power(t2,3) + 
                     Power(t2,4))) + 
               Power(s2,2)*(20*Power(t1,4)*(-3 + 2*t2) + 
                  2*Power(t1,3)*(-153 + 58*t2 + 31*Power(t2,2)) + 
                  Power(t1,2)*
                   (512 - 618*t2 + 372*Power(t2,2) - 50*Power(t2,3)) - 
                  4*Power(-1 + t2,2)*
                   (51 + 7*t2 - 11*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(129 - 402*t2 + 430*Power(t2,2) - 164*Power(t2,3) + 
                     7*Power(t2,4)))) + 
            (-1 + t2)*(3*Power(-1 + t2,3)*(1 + 9*t2) + 
               Power(t1,4)*(-10 + 18*t2 - 9*Power(t2,2) + Power(t2,3)) + 
               Power(s2,4)*(28 - 44*t2 + 13*Power(t2,2) + 
                  3*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (71 - 9*t2 - 16*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(101 - 161*t2 + 46*Power(t2,2) + 
                  12*Power(t2,3) - 2*Power(t2,4)) + 
               Power(t1,2)*(-101 + 100*t2 + 21*Power(t2,2) - 
                  22*Power(t2,3) + Power(t2,4) + Power(t2,5)) - 
               Power(s2,3)*(64 - 115*t2 + 60*Power(t2,2) - 
                  30*Power(t2,3) + 17*Power(t2,4) + 
                  12*t1*(7 - 12*t2 + 5*Power(t2,2))) + 
               Power(s2,2)*(-162 + 196*t2 + 32*Power(t2,2) - 
                  81*Power(t2,3) + 7*Power(t2,4) + 8*Power(t2,5) + 
                  Power(t1,2)*
                   (74 - 138*t2 + 72*Power(t2,2) - 8*Power(t2,3)) + 
                  t1*(221 - 343*t2 + 78*Power(t2,2) + 16*Power(t2,3) + 
                     16*Power(t2,4))) + 
               s2*(4*Power(t1,3)*(-2 + t2)*Power(-1 + t2,2) - 
                  2*Power(-1 + t2,2)*
                   (-72 + 43*t2 + Power(t2,2) + 3*Power(t2,3)) + 
                  Power(t1,2)*
                   (-258 + 389*t2 - 64*Power(t2,2) - 58*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  t1*(272 - 343*t2 + 32*Power(t2,2) + 40*Power(t2,3) + 
                     6*Power(t2,4) - 7*Power(t2,5)))) + 
            s1*(-(Power(t1,5)*(-4 + t2)*Power(-1 + t2,2)) + 
               8*Power(s2,5)*(5 - 8*t2 + 3*Power(t2,2)) - 
               Power(-1 + t2,4)*(68 - 18*t2 + 7*Power(t2,2)) - 
               t1*Power(-1 + t2,3)*
                (-192 + 104*t2 - 13*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (95 - 33*t2 - 12*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,4)*(-121 + 177*t2 - 37*Power(t2,2) - 
                  17*Power(t2,3) + 2*Power(t2,4)) - 
               Power(t1,3)*(-166 + 207*t2 - 41*Power(t2,2) + 
                  2*Power(t2,3) - 3*Power(t2,4) + Power(t2,5)) + 
               Power(s2,4)*(-84 + 143*t2 - 54*Power(t2,2) + 
                  3*Power(t2,3) - 4*Power(t2,4) + 
                  t1*(-160 + 259*t2 - 102*Power(t2,2) + 3*Power(t2,3))) + 
               Power(s2,3)*(-278 + 447*t2 - 173*Power(t2,2) - 
                  8*Power(t2,3) + 13*Power(t2,4) - Power(t2,5) - 
                  4*Power(t1,2)*
                   (-59 + 96*t2 - 39*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(378 - 606*t2 + 181*Power(t2,2) + 24*Power(t2,3) + 
                     7*Power(t2,4))) + 
               Power(s2,2)*(2*Power(t1,3)*
                   (-74 + 119*t2 - 48*Power(t2,2) + 3*Power(t2,3)) - 
                  Power(t1,2)*
                   (625 - 960*t2 + 237*Power(t2,2) + 74*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (173 - 167*t2 + 73*Power(t2,2) - 22*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  t1*(738 - 1179*t2 + 531*Power(t2,2) - 
                     110*Power(t2,3) + 25*Power(t2,4) - 5*Power(t2,5))) + 
               s2*(4*Power(t1,4)*(7 - 10*t2 + 3*Power(t2,2)) + 
                  Power(-1 + t2,3)*
                   (-146 - 3*t2 + 49*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,3)*
                   (452 - 674*t2 + 147*Power(t2,2) + 64*Power(t2,3) - 
                     5*Power(t2,4)) - 
                  2*t1*Power(-1 + t2,2)*
                   (136 - 101*t2 + 26*Power(t2,2) - 2*Power(t2,3) + 
                     Power(t2,4)) + 
                  Power(t1,2)*
                   (-626 + 939*t2 - 399*Power(t2,2) + 120*Power(t2,3) - 
                     41*Power(t2,4) + 7*Power(t2,5))))))*
       T3q(1 - s + s1 - t2,s1))/
     ((-1 + s2)*(-s + s2 - t1)*Power(-1 + s + t2,2)*(s - s1 + t2)*
       (-1 + s2 - t1 + t2)*Power(-1 + s - s*s1 + s1*s2 - s1*t1 + t2,2)) - 
    (8*(-14 + 74*s2 - 51*Power(s2,2) + 4*Power(s2,3) - 35*t1 + 30*s2*t1 + 
         7*Power(s2,2)*t1 + 12*Power(t1,2) - 14*s2*Power(t1,2) - 
         2*Power(s2,2)*Power(t1,2) + 3*Power(t1,3) + 4*s2*Power(t1,3) - 
         2*Power(t1,4) + 50*t2 - 174*s2*t2 + 98*Power(s2,2)*t2 - 
         10*Power(s2,3)*t2 + 60*t1*t2 - 39*s2*t1*t2 - 
         12*Power(s2,2)*t1*t2 + 3*Power(s2,3)*t1*t2 - 31*Power(t1,2)*t2 + 
         24*s2*Power(t1,2)*t2 - 5*Power(s2,2)*Power(t1,2)*t2 - 
         2*Power(t1,3)*t2 + s2*Power(t1,3)*t2 + Power(t1,4)*t2 - 
         46*Power(t2,2) + 99*s2*Power(t2,2) - 
         34*Power(s2,2)*Power(t2,2) + 4*Power(s2,3)*Power(t2,2) - 
         Power(s2,4)*Power(t2,2) - 22*s2*t1*Power(t2,2) + 
         14*Power(s2,2)*t1*Power(t2,2) + Power(s2,3)*t1*Power(t2,2) + 
         27*Power(t1,2)*Power(t2,2) - 16*s2*Power(t1,2)*Power(t2,2) + 
         Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         2*Power(t1,3)*Power(t2,2) - s2*Power(t1,3)*Power(t2,2) - 
         2*Power(t2,3) + 21*s2*Power(t2,3) - 20*Power(s2,2)*Power(t2,3) + 
         3*Power(s2,3)*Power(t2,3) - 33*t1*Power(t2,3) + 
         37*s2*t1*Power(t2,3) - 12*Power(s2,2)*t1*Power(t2,3) - 
         7*Power(t1,2)*Power(t2,3) + 9*s2*Power(t1,2)*Power(t2,3) + 
         12*Power(t2,4) - 20*s2*Power(t2,4) + 8*Power(s2,2)*Power(t2,4) + 
         8*t1*Power(t2,4) - 8*s2*t1*Power(t2,4) + 
         Power(s1,6)*(s2 - t1)*(Power(s2,2) - t1*(1 + t1) + s2*t2) + 
         Power(s,4)*Power(-1 + s1,2)*
          (6 - 4*t1 - 3*Power(t1,2) + s2*(-6 + 7*t1 - 7*t2) + 4*t2 + 
            6*t1*t2 - 3*Power(t2,2) + 
            Power(s1,2)*(-2 + 2*s2 - t1 + t2) - 
            s1*(4 + 3*t1 + Power(t1,2) - 3*t2 - 2*t1*t2 + Power(t2,2) + 
               s2*(-4 - t1 + t2))) + 
         Power(s1,5)*(Power(s2,4) - Power(s2,3)*(t1 + t2) + 
            Power(s2,2)*(5 - Power(t1,2) + t2 + t1*t2 - 2*Power(t2,2)) - 
            t1*(-1 + t2 + Power(t1,2)*t2 + 2*t1*(-5 + 3*t2)) + 
            s2*(Power(t1,3) + Power(t1,2)*t2 + (-1 + t2)*t2 + 
               t1*(-16 + 7*t2 + Power(t2,2)))) + 
         Power(s1,4)*(Power(s2,4)*(1 + t1 - 2*t2) - 
            Power(s2,3)*(-29 + 2*Power(t1,2) + t1*(10 - 4*t2) + 9*t2 + 
               Power(t2,2)) + 
            Power(s2,2)*(-38 - Power(t1,2)*(-23 + t2) + 29*t2 - 
               7*Power(t2,2) + Power(t2,3) + 
               t1*(-65 + 19*t2 + Power(t2,2))) + 
            t1*(17 - Power(t1,4) - 25*t2 + 8*Power(t2,2) + 
               Power(t1,3)*(6 + t2) - Power(t1,2)*(4 + 3*t2) + 
               t1*(-29 + 12*t2 + 2*Power(t2,2))) + 
            s2*(-13 + 2*Power(t1,4) + Power(t1,2)*(40 - 7*t2) + 16*t2 - 
               2*Power(t2,2) - Power(t2,3) - 2*Power(t1,3)*(10 + t2) + 
               t1*(67 - 40*t2 + 3*Power(t2,2)))) + 
         Power(s1,3)*(4*Power(s2,5) + Power(t1,5) + 
            Power(t1,4)*(-11 + t2) - Power(-1 + t2,2)*(-7 + 3*t2) + 
            Power(t1,3)*(33 - 5*t2 - 2*Power(t2,2)) + 
            2*Power(t1,2)*(-1 - 5*t2 + Power(t2,2)) - 
            t1*(67 - 97*t2 + 29*Power(t2,2) + Power(t2,3)) + 
            Power(s2,4)*(17 - 4*t2 + Power(t2,2) - t1*(14 + t2)) + 
            Power(s2,3)*(-68 + 47*t2 - 2*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(17 + 3*t2) + 
               t1*(-41 + 10*t2 - 3*Power(t2,2))) + 
            s2*(76 + Power(t1,4)*(-1 + t2) - 122*t2 + 52*Power(t2,2) - 
               6*Power(t2,3) - Power(t1,3)*(-15 + Power(t2,2)) + 
               Power(t1,2)*(-133 + 52*t2 + 7*Power(t2,2)) + 
               t1*(38 - 26*t2 + 8*Power(t2,2))) - 
            Power(s2,2)*(Power(t1,3)*(7 + 3*t2) + 
               Power(t1,2)*(-20 + 7*t2 - 3*Power(t2,2)) + 
               2*(16 - 13*t2 + Power(t2,2) + Power(t2,3)) + 
               t1*(-168 + 94*t2 + 3*Power(t2,2) + Power(t2,3)))) + 
         Power(s1,2)*(4*Power(s2,5) + Power(t1,5) - 
            2*Power(t1,4)*(-5 + 6*t2) + 
            2*Power(-1 + t2,2)*(-19 + 9*t2) + 
            Power(t1,3)*(-52 + t2 + 10*Power(t2,2)) + 
            Power(t1,2)*(48 - 41*t2 + 5*Power(t2,2) + Power(t2,3)) + 
            t1*(28 - 67*t2 + 34*Power(t2,2) + 5*Power(t2,3)) + 
            Power(s2,4)*(-59 + 4*t2 + 5*Power(t2,2) - 3*t1*(1 + 2*t2)) + 
            Power(s2,3)*(40 + 11*t2 - 25*Power(t2,2) + Power(t2,3) + 
               2*Power(t1,2)*(-8 + 9*t2) + 
               t1*(152 + 19*t2 - 17*Power(t2,2))) + 
            Power(s2,2)*(119 + Power(t1,3)*(26 - 18*t2) - 178*t2 + 
               69*Power(t2,2) + 3*Power(t2,3) + 
               Power(t1,2)*(-117 - 62*t2 + 19*Power(t2,2)) - 
               t1*(133 + 17*t2 - 57*Power(t2,2) + 2*Power(t2,3))) + 
            s2*(-27 + 6*Power(t1,4)*(-2 + t2) + 70*t2 - 43*Power(t2,2) + 
               Power(t1,3)*(14 + 51*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*(145 + 5*t2 - 42*Power(t2,2) + Power(t2,3)) - 
               t1*(166 - 214*t2 + 67*Power(t2,2) + 7*Power(t2,3)))) + 
         s1*(-Power(t1,5) + Power(t1,4)*(5 + t2) + 
            Power(s2,4)*(-8 - (-10 + t1)*t2 + 3*Power(t2,2)) - 
            Power(-1 + t2,2)*(-29 + 29*t2 + 4*Power(t2,2)) + 
            Power(t1,3)*(19 - 38*t2 + 18*Power(t2,2)) + 
            t1*(8 + 11*Power(t2,2) - 19*Power(t2,3)) + 
            Power(t1,2)*(-80 + 60*t2 + 44*Power(t2,2) - 
               18*Power(t2,3)) + 
            Power(s2,3)*(106 - 102*t2 - 16*Power(t2,2) + 
               11*Power(t2,3) + Power(t1,2)*(1 + 3*t2) + 
               t1*(-4 + 4*t2 - 21*Power(t2,2))) + 
            Power(s2,2)*(-131 + 167*t2 + 8*Power(t2,2) - 
               38*Power(t2,3) - 3*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(37 - 37*t2 + 33*Power(t2,2)) + 
               t1*(-168 + 111*t2 + 83*Power(t2,2) - 25*Power(t2,3))) + 
            s2*(-38 + 83*t2 - 83*Power(t2,2) + 34*Power(t2,3) + 
               4*Power(t2,4) + Power(t1,4)*(3 + t2) + 
               Power(t1,3)*(-30 + 22*t2 - 15*Power(t2,2)) + 
               Power(t1,2)*(43 + 29*t2 - 85*Power(t2,2) + 
                  14*Power(t2,3)) + 
               t1*(208 - 219*t2 - 59*Power(t2,2) + 58*Power(t2,3)))) - 
         Power(s,3)*(-1 + s1)*
          (-30 + 9*t1 + 3*Power(t1,2) - 5*Power(t1,3) + 11*t2 - 
            26*t1*t2 + 8*Power(t1,2)*t2 + 23*Power(t2,2) - 
            t1*Power(t2,2) - 2*Power(t2,3) + 
            Power(s1,4)*(-4 + 5*s2 - 3*t1 + 2*t2) + 
            Power(s2,2)*(2 - 7*t1 + 6*t2) + 
            s2*(25 - 15*t1 + 12*Power(t1,2) - t2 - 12*Power(t2,2)) + 
            Power(s1,3)*(-3 + 5*Power(s2,2) - 3*Power(t1,2) + 7*t2 - 
               3*Power(t2,2) - s2*(2 + t1 + t2) + t1*(-5 + 6*t2)) + 
            Power(s1,2)*(12 + Power(s2,2)*(16 + 2*t1 - 3*t2) - 6*t2 - 
               Power(t2,2) + Power(t2,3) + Power(t1,2)*(3 + t2) + 
               t1*(25 - 2*t2 - 2*Power(t2,2)) - 
               s2*(34 + 2*Power(t1,2) + t1*(19 - 4*t2) - 7*t2 + 
                  2*Power(t2,2))) + 
            s1*(25 + 5*Power(t1,3) + Power(s2,2)*(-23 + 21*t1 - 19*t2) + 
               Power(t1,2)*(13 - 9*t2) - 30*t2 - 3*Power(t2,2) + 
               Power(t2,3) + t1*(-10 - 10*t2 + 3*Power(t2,2)) + 
               s2*(6 - 26*Power(t1,2) + 27*t2 - 2*Power(t2,2) + 
                  t1*(3 + 28*t2)))) + 
         Power(s,2)*(16 + 46*s2 - 57*Power(s2,2) + 4*Power(s2,3) - 
            43*t1 + 42*s2*t1 + 22*Power(s2,2)*t1 + 14*Power(t1,2) - 
            40*s2*Power(t1,2) - 2*Power(s2,2)*Power(t1,2) + 
            14*Power(t1,3) + 4*s2*Power(t1,3) - 2*Power(t1,4) - 47*t2 + 
            35*s2*t2 - 26*Power(s2,2)*t2 + Power(s2,3)*t2 - 3*t1*t2 + 
            52*s2*t1*t2 - 11*Power(s2,2)*t1*t2 - 40*Power(t1,2)*t2 + 
            9*s2*Power(t1,2)*t2 + Power(t1,3)*t2 + 13*Power(t2,2) - 
            24*s2*Power(t2,2) + 8*Power(s2,2)*Power(t2,2) + 
            2*t1*Power(t2,2) + 4*s2*t1*Power(t2,2) + 
            4*Power(t1,2)*Power(t2,2) + 24*Power(t2,3) - 
            17*s2*Power(t2,3) - 3*t1*Power(t2,3) + 
            Power(s1,6)*(-2 + 4*s2 - 3*t1 + t2) + 
            Power(s1,5)*(-4 + 9*Power(s2,2) - 3*Power(t1,2) + 7*t2 - 
               2*Power(t2,2) + s2*(-8 - 5*t1 + t2) + t1*(-1 + 6*t2)) + 
            Power(s1,4)*(4 + 4*Power(s2,3) + 2*Power(t1,3) + 
               Power(s2,2)*(3 + t1 - 7*t2) - Power(t1,2)*(-16 + t2) - 
               2*t2 - 7*Power(t2,2) + Power(t2,3) - 
               2*t1*(-10 + 2*t2 + Power(t2,2)) - 
               s2*(10 + 7*Power(t1,2) + t1*(26 - 12*t2) - 10*t2 + 
                  7*Power(t2,2))) + 
            Power(s1,3)*(63 + 2*Power(t1,4) + 
               Power(s2,3)*(25 + t1 - 3*t2) - 86*t2 + 34*Power(t2,2) - 
               Power(t2,3) - Power(t1,3)*(1 + 5*t2) + 
               Power(s2,2)*(-41 + t1*(-51 + t2) + 8*t2) + 
               Power(t1,2)*(-28 - t2 + 4*Power(t2,2)) - 
               t1*(-39 + 55*t2 - 3*Power(t2,2) + Power(t2,3)) + 
               s2*(-61 - 3*Power(t1,3) + 57*t2 + 3*Power(t2,2) + 
                  3*Power(t2,3) + Power(t1,2)*(27 + 7*t2) + 
                  t1*(71 - 4*t2 - 7*Power(t2,2)))) + 
            Power(s1,2)*(-120 - 6*Power(t1,4) + 122*t2 - 
               2*Power(t2,2) - Power(t2,3) + 
               Power(t1,3)*(-14 + 11*t2) + 
               Power(t1,2)*(10 - 11*t2 - 4*Power(t2,2)) - 
               t1*(186 - 95*t2 - 26*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(22*t1 - 17*(2 + t2)) + 
               Power(s2,2)*(-92 - 50*Power(t1,2) + 51*t2 + 
                  8*Power(t2,2) + 7*t1*(7 + 5*t2)) + 
               s2*(282 + 34*Power(t1,3) - 185*t2 - 33*Power(t2,2) + 
                  Power(t2,3) - Power(t1,2)*(1 + 29*t2) + 
                  t1*(70 - 24*t2 - 6*Power(t2,2)))) + 
            s1*(43 + 6*Power(t1,4) + 13*t2 - 52*Power(t2,2) - 
               15*Power(t2,3) - Power(t1,3)*(9 + 7*t2) + 
               Power(s2,3)*(1 - 15*t1 + 11*t2) + 
               Power(t1,2)*(-25 + 77*t2 - 4*Power(t2,2)) + 
               t1*(166 - 7*t2 - 53*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*(36*Power(t1,2) + t1*(-45 + 7*t2) - 
                  2*(-89 + t2 + 16*Power(t2,2))) + 
               s2*(-253 - 27*Power(t1,3) + Power(t1,2)*(53 - 11*t2) + 
                  58*t2 + 93*Power(t2,2) + 5*Power(t2,3) + 
                  t1*(-128 - 100*t2 + 33*Power(t2,2))))) - 
         s*(-21 + 136*s2 - 105*Power(s2,2) + 8*Power(s2,3) + 
            Power(s1,7)*(s2 - t1) - 70*t1 + 62*s2*t1 + 
            22*Power(s2,2)*t1 + 27*Power(t1,2) - 42*s2*Power(t1,2) - 
            4*Power(s2,2)*Power(t1,2) + 12*Power(t1,3) + 
            8*s2*Power(t1,3) - 4*Power(t1,4) + 28*t2 - 154*s2*t2 + 
            87*Power(s2,2)*t2 - 11*Power(s2,3)*t2 + 42*t1*t2 - 
            4*s2*t1*t2 - 18*Power(s2,2)*t1*t2 + 3*Power(s2,3)*t1*t2 - 
            55*Power(t1,2)*t2 + 29*s2*Power(t1,2)*t2 - 
            5*Power(s2,2)*Power(t1,2)*t2 + s2*Power(t1,3)*t2 + 
            Power(t1,4)*t2 + 21*Power(t2,2) - 33*s2*Power(t2,2) + 
            39*Power(s2,2)*Power(t2,2) - 8*Power(s2,3)*Power(t2,2) + 
            51*t1*Power(t2,2) - 77*s2*t1*Power(t2,2) + 
            28*Power(s2,2)*t1*Power(t2,2) + 32*Power(t1,2)*Power(t2,2) - 
            18*s2*Power(t1,2)*Power(t2,2) - 2*Power(t1,3)*Power(t2,2) - 
            20*Power(t2,3) + 40*s2*Power(t2,3) - 
            16*Power(s2,2)*Power(t2,3) - 20*t1*Power(t2,3) + 
            9*s2*t1*Power(t2,3) + Power(t1,2)*Power(t2,3) - 
            8*Power(t2,4) + 8*s2*Power(t2,4) + 
            Power(s1,6)*(-1 + 4*Power(s2,2) - Power(t1,2) + t2 + 
               2*t1*t2 + s2*(-3 - 3*t1 + t2)) + 
            Power(s1,5)*(3 + 5*Power(s2,3) + t1 + 3*Power(t1,3) - 
               Power(t1,2)*(-6 + t2) - t2 - 2*Power(t2,2) - 
               t1*Power(t2,2) - Power(s2,2)*(3*t1 + 2*t2) - 
               s2*(5 + 5*Power(t1,2) + t1*(8 - 5*t2) - 9*t2 + 
                  4*Power(t2,2))) + 
            Power(s1,4)*(1 + Power(s2,4) + 2*Power(t1,4) + 
               Power(s2,3)*(8 + 2*t1 - 7*t2) - 7*t2 + 5*Power(t2,2) + 
               Power(t2,3) - Power(t1,3)*(13 + 4*t2) + 
               Power(t1,2)*(6 - 7*t2 + 2*Power(t2,2)) + 
               t1*(55 - 46*t2 + 8*Power(t2,2)) - 
               Power(s2,2)*(-24 + 5*Power(t1,2) + 4*t2 + 
                  5*Power(t2,2) - 5*t1*(-7 + 2*t2)) + 
               s2*(-48 + 40*t2 - 11*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t1,2)*(40 + t2) - t1*(34 - 19*t2 + Power(t2,2)))) \
+ s1*(101 + 10*Power(t1,4) - Power(t1,5) - 192*t2 + 56*Power(t2,2) + 
               35*Power(t2,3) - Power(s2,4)*(8 + t2) + 
               Power(t1,3)*(13 - 42*t2 + 3*Power(t2,2)) - 
               Power(t1,2)*(97 - 20*t2 + 6*Power(t2,2) + 
                  2*Power(t2,3)) + 
               2*t1*(73 - 76*t2 - 8*Power(t2,2) + 19*Power(t2,3)) + 
               Power(s2,3)*(108 + Power(t1,2) + 27*t2 - 
                  14*Power(t2,2) + 6*t1*(-3 + 4*t2)) + 
               Power(s2,2)*(20 - 3*Power(t1,3) + 
                  Power(t1,2)*(70 - 45*t2) - 146*t2 + 39*Power(t2,2) + 
                  19*Power(t2,3) + 2*t1*(-97 - 46*t2 + 8*Power(t2,2))) + 
               s2*(-280 + 3*Power(t1,4) + 410*t2 - 81*Power(t2,2) - 
                  65*Power(t2,3) + Power(t1,3)*(-54 + 22*t2) + 
                  Power(t1,2)*(73 + 107*t2 - 5*Power(t2,2)) + 
                  t1*(94 + 85*t2 - 6*Power(t2,2) - 20*Power(t2,3)))) + 
            Power(s1,2)*(2*Power(t1,5) + 
               Power(s2,4)*(-3 + 8*t1 - 6*t2) + Power(t1,4)*(2 - 3*t2) + 
               Power(t1,3)*(-23 + 32*t2) + 
               t1*(-23 + 58*t2 - 57*Power(t2,2)) + 
               Power(s2,3)*(-174 + 24*t1 - 26*Power(t1,2) + 34*t2 + 
                  7*t1*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(160 - 6*t2 - 34*Power(t2,2) + Power(t2,3)) + 
               3*(-41 + 80*t2 - 42*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*(287 + 30*Power(t1,3) + 
                  Power(t1,2)*(-37 + t2) - 131*t2 - 56*Power(t2,2) + 
                  2*Power(t2,3) + t1*(291 + 8*t2 - 28*Power(t2,2))) + 
               s2*(73 - 14*Power(t1,4) - 143*t2 + 90*Power(t2,2) + 
                  2*Power(t2,3) + Power(t1,3)*(14 + t2) + 
                  2*Power(t1,2)*(-47 - 37*t2 + 8*Power(t2,2)) + 
                  t1*(-443 + 127*t2 + 96*Power(t2,2) - 3*Power(t2,3)))) - 
            Power(s1,3)*(-48 + Power(t1,5) + Power(s2,4)*(-18 + t2) - 
               2*Power(t1,4)*(-1 + t2) + 85*t2 - 46*Power(t2,2) + 
               9*Power(t2,3) + Power(t1,2)*(85 - 33*t2 - 6*Power(t2,2)) + 
               Power(t1,3)*(-24 + 2*t2 + Power(t2,2)) + 
               t1*(92 - 80*t2 + Power(t2,2) + 2*Power(t2,3)) - 
               Power(s2,3)*(13 + Power(t1,2) - 3*t2 + 2*Power(t2,2) - 
                  2*t1*(24 + t2)) + 
               Power(s2,2)*(182 + 3*Power(t1,3) - 124*t2 + Power(t2,2) - 
                  3*Power(t2,3) - Power(t1,2)*(40 + 9*t2) + 
                  t1*(9 - 12*t2 + 8*Power(t2,2))) + 
               s2*(-94 - 3*Power(t1,4) + 107*t2 - 31*Power(t2,2) + 
                  3*Power(t2,3) + 8*Power(t1,3)*(1 + t2) - 
                  7*Power(t1,2)*(-4 - t2 + Power(t2,2)) + 
                  2*t1*(-138 + 84*t2 + 2*Power(t2,2) + Power(t2,3))))))*
       T4q(s1))/((-1 + s1)*(-1 + s2)*(-s + s2 - t1)*(s - s1 + t2)*
       (-1 + s2 - t1 + t2)*Power(-1 + s - s*s1 + s1*s2 - s1*t1 + t2,2)) + 
    (8*(14 - 28*s2 + 19*Power(s2,2) + Power(s2,3) + 17*t1 - 13*s2*t1 - 
         8*Power(s2,2)*t1 + 3*Power(s2,3)*t1 - 9*Power(t1,2) + 
         14*s2*Power(t1,2) - 7*Power(s2,2)*Power(t1,2) - 
         2*Power(s2,3)*Power(t1,2) - 7*Power(t1,3) + 5*s2*Power(t1,3) + 
         6*Power(s2,2)*Power(t1,3) - Power(t1,4) - 6*s2*Power(t1,4) + 
         2*Power(t1,5) - 40*t2 + 68*s2*t2 - 33*Power(s2,2)*t2 - 
         6*Power(s2,3)*t2 - 2*Power(s2,4)*t2 - 37*t1*t2 + 27*s2*t1*t2 + 
         16*Power(s2,2)*t1*t2 + 3*Power(s2,3)*t1*t2 + 
         3*Power(s2,4)*t1*t2 + 19*Power(t1,2)*t2 - 22*s2*Power(t1,2)*t2 - 
         Power(s2,2)*Power(t1,2)*t2 - 8*Power(s2,3)*Power(t1,2)*t2 + 
         12*Power(t1,3)*t2 + s2*Power(t1,3)*t2 + 
         6*Power(s2,2)*Power(t1,3)*t2 - Power(t1,4)*t2 - Power(t1,5)*t2 + 
         36*Power(t2,2) - 45*s2*Power(t2,2) - 5*Power(s2,2)*Power(t2,2) + 
         12*Power(s2,3)*Power(t2,2) + Power(s2,4)*Power(t2,2) - 
         Power(s2,5)*Power(t2,2) + 18*t1*Power(t2,2) + 
         2*s2*t1*Power(t2,2) - 12*Power(s2,2)*t1*Power(t2,2) - 
         4*Power(s2,3)*t1*Power(t2,2) + 2*Power(s2,4)*t1*Power(t2,2) - 
         18*Power(t1,2)*Power(t2,2) + 3*s2*Power(t1,2)*Power(t2,2) + 
         8*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         3*Power(t1,3)*Power(t2,2) - 8*s2*Power(t1,3)*Power(t2,2) - 
         2*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         3*Power(t1,4)*Power(t2,2) + s2*Power(t1,4)*Power(t2,2) - 
         8*Power(t2,3) - 2*s2*Power(t2,3) + 25*Power(s2,2)*Power(t2,3) - 
         9*Power(s2,3)*Power(t2,3) + 2*Power(s2,4)*Power(t2,3) + 
         7*t1*Power(t2,3) - 17*s2*t1*Power(t2,3) + 
         5*Power(s2,2)*t1*Power(t2,3) - 6*Power(s2,3)*t1*Power(t2,3) + 
         7*Power(t1,2)*Power(t2,3) + 7*s2*Power(t1,2)*Power(t2,3) + 
         6*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         3*Power(t1,3)*Power(t2,3) - 2*s2*Power(t1,3)*Power(t2,3) - 
         2*Power(t2,4) + 7*s2*Power(t2,4) - 6*Power(s2,2)*Power(t2,4) + 
         3*Power(s2,3)*Power(t2,4) - 5*t1*Power(t2,4) + 
         s2*t1*Power(t2,4) - 4*Power(s2,2)*t1*Power(t2,4) + 
         Power(t1,2)*Power(t2,4) + s2*Power(t1,2)*Power(t2,4) + 
         Power(s1,4)*(s2 - t1)*
          (Power(s2,3) + t1*(1 + Power(t1,2) - t1*(-4 + t2) - t2) + 
            Power(s2,2)*(1 - t1 + 2*t2) - 
            s2*(Power(t1,2) - (-1 + t2)*t2 + t1*(5 + t2))) + 
         Power(s,4)*(2 - 6*t1 - Power(t1,2) + 3*Power(t1,3) + 6*t2 + 
            2*t1*t2 - 9*Power(t1,2)*t2 - Power(t2,2) + 9*t1*Power(t2,2) - 
            3*Power(t2,3) + Power(s2,2)*(2 - t1 + t2) + 
            Power(s1,2)*(2 + 2*Power(s2,2) + Power(t1,2) + t2 + 
               Power(t2,2) - t1*(1 + 2*t2) + s2*(-4 - 3*t1 + 3*t2)) - 
            s2*(4 + 2*Power(t1,2) + 3*t2 + 2*Power(t2,2) - 
               t1*(3 + 4*t2)) + 
            s1*(-4 + Power(t1,3) + Power(t1,2)*(4 - 3*t2) + 
               Power(s2,2)*(-4 + t1 - t2) - 7*t2 + 4*Power(t2,2) - 
               Power(t2,3) - 2*s2*
                (-4 + Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               t1*(7 - 8*t2 + 3*Power(t2,2)))) + 
         Power(s1,3)*(Power(s2,5) - Power(s2,4)*(7 + 2*t1) + 
            Power(s2,3)*(13 - 6*t2 - 3*Power(t2,2) + t1*(19 + t2)) + 
            t1*(-Power(-1 + t2,2) + Power(t1,3)*t2 - 
               Power(t1,2)*(14 - 11*t2 + Power(t2,2)) - 
               3*t1*(9 - 11*t2 + 2*Power(t2,2))) + 
            Power(s2,2)*(-19 + 2*Power(t1,3) + 15*t2 + 6*Power(t2,2) - 
               2*Power(t2,3) - Power(t1,2)*(17 + t2) + 
               t1*(-41 + 25*t2 + 4*Power(t2,2))) + 
            s2*(-Power(t1,4) - Power(t1,3)*(-5 + t2) + 
               Power(-1 + t2,2)*t2 - 6*Power(t1,2)*(-7 + 5*t2) + 
               t1*(47 - 51*t2 + 3*Power(t2,2) + Power(t2,3)))) + 
         Power(s1,2)*(Power(s2,5)*(1 + t1 - 2*t2) + 
            Power(s2,4)*(17 - 3*Power(t1,2) + 4*t2 - 3*Power(t2,2) + 
               t1*(-12 + 7*t2)) + 
            Power(s2,3)*(-14 + 2*Power(t1,3) - 7*Power(t1,2)*(-5 + t2) + 
               13*t2 - 7*Power(t2,2) + 2*t1*(-23 - 8*t2 + 3*Power(t2,2))) \
+ Power(s2,2)*(-15 + 2*Power(t1,4) + 20*t2 + 7*Power(t2,2) - 
               13*Power(t2,3) + Power(t2,4) - Power(t1,3)*(43 + t2) + 
               Power(t1,2)*(32 + 30*t2 - 2*Power(t2,2)) + 
               2*t1*(17 - 9*t2 + 4*Power(t2,2))) + 
            t1*(Power(t1,5) - Power(t1,4)*(5 + 2*t2) + 
               Power(-1 + t2,2)*(-41 + 8*t2) + 
               Power(t1,2)*(5 + 10*t2 - 7*Power(t2,2)) + 
               Power(t1,3)*(-9 + 10*t2 + Power(t2,2)) + 
               t1*(-23 + 49*t2 - 28*Power(t2,2) + 2*Power(t2,3))) + 
            s2*(-3*Power(t1,5) + Power(t1,4)*(24 + 5*t2) - 
               Power(-1 + t2,2)*(-35 + t2 + Power(t2,2)) - 
               2*Power(t1,3)*(-3 + 14*t2 + Power(t2,2)) + 
               Power(t1,2)*(-25 - 5*t2 + 6*Power(t2,2)) + 
               t1*(40 - 76*t2 + 30*Power(t2,2) + 6*Power(t2,3)))) + 
         s1*(Power(t1,6) - 4*Power(t1,5)*t2 + 
            Power(s2,5)*t2*(2 - t1 + t2) - 
            Power(-1 + t2,3)*(-17 + 3*t2) - 
            t1*Power(-1 + t2,2)*(-3 - 19*t2 + Power(t2,2)) + 
            Power(t1,4)*(-10 + 8*t2 + 5*Power(t2,2)) - 
            Power(t1,3)*(17 - 29*t2 + 18*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,2)*(20 - 14*t2 - 17*Power(t2,2) + 11*Power(t2,3)) + 
            Power(s2,4)*(-2 + 8*t2 - 7*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(1 + 4*t2) - t1*(4 + 7*t2 + 5*Power(t2,2))) + 
            Power(s2,3)*(-37 + 29*t2 + 24*Power(t2,2) - 9*Power(t2,3) + 
               Power(t2,4) - 2*Power(t1,3)*(2 + 3*t2) + 
               Power(t1,2)*(12 + 13*t2 + 9*Power(t2,2)) + 
               t1*(20 - 33*t2 + 12*Power(t2,2) - 5*Power(t2,3))) - 
            s2*(Power(t1,5)*(4 + t2) - 
               Power(t1,4)*(4 + 13*t2 + 2*Power(t2,2)) + 
               Power(-1 + t2,2)*(13 + 8*Power(t2,2)) + 
               Power(t1,2)*(7 + 15*t2 - 43*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(t1,3)*(-36 + 33*t2 + 12*Power(t2,2) + 
                  Power(t2,3)) + 
               2*t1*(35 - 49*t2 + 8*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s2,2)*(49 - 81*t2 + 30*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,4)*(6 + 4*t2) - 
               Power(t1,3)*(12 + 17*t2 + 7*Power(t2,2)) + 
               2*Power(t1,2)*
                (-22 + 25*t2 + Power(t2,2) + 2*Power(t2,3)) - 
               t1*(-61 + 43*t2 + 49*Power(t2,2) - 8*Power(t2,3) + 
                  Power(t2,4)))) + 
         Power(s,3)*(6 + t1 - 4*Power(t1,2) - 6*Power(t1,3) + 
            5*Power(t1,4) + 3*t2 - 4*t1*t2 + 13*Power(t1,2)*t2 - 
            13*Power(t1,3)*t2 + 8*Power(t2,2) - 8*t1*Power(t2,2) + 
            9*Power(t1,2)*Power(t2,2) + Power(t2,3) + t1*Power(t2,3) - 
            2*Power(t2,4) + Power(s2,3)*(t1 - 2*(1 + t2)) - 
            Power(s1,3)*(8 + 5*Power(s2,2) + t1 + 3*Power(t1,2) - 4*t2 - 
               5*t1*t2 + 2*Power(t2,2) + s2*(-9 - 8*t1 + 7*t2)) + 
            Power(s2,2)*(-1 + 3*Power(t1,2) + 21*t2 + 10*Power(t2,2) - 
               t1*(4 + 13*t2)) + 
            s2*(1 - 9*Power(t1,3) - 27*t2 + 8*Power(t2,2) + 
               10*Power(t2,3) + 4*Power(t1,2)*(3 + 7*t2) + 
               t1*(3 - 20*t2 - 29*Power(t2,2))) + 
            Power(s1,2)*(21 - 5*Power(s2,3) - 3*Power(t1,3) + 
               2*Power(s2,2)*(7 + 3*t1 - 2*t2) + 14*t2 - 
               10*Power(t2,2) + 3*Power(t2,3) + 
               3*Power(t1,2)*(-7 + 3*t2) + 
               t1*(-29 + 31*t2 - 9*Power(t2,2)) + 
               s2*(-18 + 2*Power(t1,2) + t1*(10 - 6*t2) - 9*t2 + 
                  4*Power(t2,2))) - 
            s1*(19 + Power(s2,3)*(-7 + 2*t1 - 3*t2) + 18*t2 - 
               Power(t1,3)*t2 + 5*Power(t2,2) - 5*Power(t2,3) + 
               Power(t2,4) + Power(t1,2)*(-11 - 5*t2 + 3*Power(t2,2)) + 
               t1*(-26 + 6*t2 + 10*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*(8 - 4*Power(t1,2) + 16*t2 - 5*Power(t2,2) + 
                  t1*(3 + 9*t2)) + 
               s2*(-8 + 2*Power(t1,3) + Power(t1,2)*(4 - 5*t2) - 38*t2 + 
                  18*Power(t2,2) - Power(t2,3) + 
                  2*t1*(8 - 11*t2 + 2*Power(t2,2))))) + 
         Power(s,2)*(8 + 47*t1 + 5*Power(t1,2) - 4*Power(t1,3) - 
            12*Power(t1,4) + 2*Power(t1,5) - 13*t2 + Power(s2,4)*t2 - 
            5*t1*t2 + 13*Power(t1,2)*t2 + 29*Power(t1,3)*t2 - 
            3*Power(t1,4)*t2 - 14*t1*Power(t2,2) - 
            22*Power(t1,2)*Power(t2,2) - 3*Power(t1,3)*Power(t2,2) + 
            5*Power(t2,3) + 5*t1*Power(t2,3) + 
            7*Power(t1,2)*Power(t2,3) - 3*t1*Power(t2,4) + 
            Power(s2,3)*(3 + 2*t1 - 2*Power(t1,2) - 21*t2 + 12*t1*t2 - 
               15*Power(t2,2)) + 
            Power(s1,4)*(2 + 4*Power(s2,2) + 3*Power(t1,2) + 
               t1*(3 - 4*t2) - 3*t2 + Power(t2,2) + s2*(-4 - 7*t1 + 5*t2)\
) + Power(s1,3)*(-2 + 9*Power(s2,3) + 3*Power(t1,3) + 
               Power(t1,2)*(24 - 9*t2) - 7*t2 + 11*Power(t2,2) - 
               2*Power(t2,3) + Power(s2,2)*(-17 - 14*t1 + 10*t2) + 
               s2*(6 - 8*t1 + 2*Power(t1,2) - Power(t2,2)) + 
               t1*(11 - 24*t2 + 8*Power(t2,2))) + 
            Power(s2,2)*(21 + 6*Power(t1,3) + 6*t2 + 12*Power(t2,2) - 
               9*Power(t2,3) - 2*Power(t1,2)*(8 + 15*t2) + 
               t1*(7 + 45*t2 + 33*Power(t2,2))) - 
            s2*(34 + 6*Power(t1,4) - 22*t2 + 34*Power(t2,2) - 
               9*Power(t2,3) - 7*Power(t2,4) - 
               2*Power(t1,3)*(13 + 10*t2) + 
               Power(t1,2)*(6 + 53*t2 + 15*Power(t2,2)) + 
               t1*(37 + 8*t2 - 18*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s1,2)*(12 + 4*Power(s2,4) - 2*Power(t1,4) - 17*t2 + 
               16*Power(t2,2) - 12*Power(t2,3) + Power(t2,4) - 
               3*Power(s2,3)*(3 + t1 + t2) + Power(t1,3)*(-20 + 3*t2) + 
               Power(t1,2)*(-43 + 7*t2 + Power(t2,2)) + 
               t1*(53 - 28*t2 + 25*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*(25 - 8*Power(t1,2) + 30*t2 - 
                  14*Power(t2,2) + t1*(-22 + 20*t2)) + 
               s2*(-32 + 9*Power(t1,3) + Power(t1,2)*(51 - 20*t2) - 
                  27*t2 + 18*Power(t2,2) - 6*Power(t2,3) + 
                  t1*(25 - 48*t2 + 17*Power(t2,2)))) + 
            s1*(-19 - 2*Power(t1,5) + 46*t2 - 41*Power(t2,2) + 
               13*Power(t2,3) + Power(t2,4) + Power(t1,4)*(-5 + 7*t2) + 
               Power(t1,3)*(-6 + 11*t2 - 9*Power(t2,2)) + 
               Power(s2,3)*(-7 + t1 - Power(t1,2) + 32*t2 + 5*t1*t2 - 
                  3*Power(t2,2)) + 
               Power(t1,2)*(-9 + 35*t2 - 6*Power(t2,2) + 5*Power(t2,3)) - 
               t1*(125 - 98*t2 + 42*Power(t2,2) + Power(t2,3) + 
                  Power(t2,4)) + Power(s2,4)*(t1 - 3*(1 + t2)) + 
               Power(s2,2)*(-27 - 3*Power(t1,3) - 50*t2 + 
                  17*Power(t2,2) + 3*Power(t2,3) + 
                  Power(t1,2)*(2 + 6*t2) + 
                  t1*(15 - 46*t2 - 6*Power(t2,2))) + 
               s2*(60 + 5*Power(t1,4) + Power(t1,3)*(5 - 15*t2) - 8*t2 + 
                  33*Power(t2,2) - 17*Power(t2,3) + 3*Power(t2,4) + 
                  Power(t1,2)*(-2 + 3*t2 + 18*Power(t2,2)) + 
                  t1*(51 - 17*t2 + 9*Power(t2,2) - 11*Power(t2,3))))) - 
         s*(29 + 55*t1 - 13*Power(t1,2) - 15*Power(t1,3) - 8*Power(t1,4) + 
            4*Power(t1,5) - 53*t2 - 56*t1*t2 + 24*Power(t1,2)*t2 + 
            19*Power(t1,3)*t2 - 5*Power(t1,4)*t2 - Power(t1,5)*t2 + 
            Power(s2,4)*(-3 + 3*t1 - 8*t2)*t2 + 19*Power(t2,2) - 
            10*t1*Power(t2,2) - 26*Power(t1,2)*Power(t2,2) - 
            6*Power(t1,3)*Power(t2,2) + 3*Power(t1,4)*Power(t2,2) + 
            5*Power(t2,3) + 15*t1*Power(t2,3) + 
            11*Power(t1,2)*Power(t2,3) - 3*Power(t1,3)*Power(t2,3) - 
            4*t1*Power(t2,4) + Power(t1,2)*Power(t2,4) + 
            Power(s1,5)*(s2 - t1)*(-1 + s2 - t1 + t2) + 
            Power(s2,3)*(3 - 18*t2 + 20*Power(t2,2) - 
               4*Power(t1,2)*(1 + 2*t2) + t1*(6 + 22*t2 + 15*Power(t2,2))) \
+ Power(s2,2)*(37 - 20*t2 - 29*Power(t2,2) + Power(t2,3) + 
               8*Power(t2,4) + 6*Power(t1,3)*(2 + t2) - 
               Power(t1,2)*(20 + 40*t2 + 3*Power(t2,2)) - 
               t1*(9 - 17*t2 + 12*Power(t2,2) + 11*Power(t2,3))) - 
            s2*(61 + 12*Power(t1,4) - 74*t2 + 18*Power(t2,2) - 
               11*Power(t2,3) + 6*Power(t2,4) + 
               Power(t1,3)*(-22 - 26*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(-21 + 18*t2 + 2*Power(t2,2) - 
                  14*Power(t2,3)) + 
               t1*(36 - 30*t2 - 25*Power(t2,2) + 6*Power(t2,3) + 
                  7*Power(t2,4))) + 
            Power(s1,4)*(4*Power(s2,3) + Power(t1,3) + 
               Power(t1,2)*(7 - 3*t2) + Power(-1 + t2,2) + 
               Power(s2,2)*(-1 - 7*t1 + 5*t2) + 
               t1*(3 - 5*t2 + 2*Power(t2,2)) + 
               s2*(2*Power(t1,2) + (-1 + t2)*t2 - 2*t1*(3 + t2))) + 
            Power(s1,3)*(5*Power(s2,4) - 3*Power(t1,4) - 
               Power(t1,2)*(2 + t2) - Power(-1 + t2,2)*(1 + 2*t2) + 
               Power(s2,3)*(-13 - 8*t1 + 3*t2) + 
               Power(t1,3)*(-17 + 4*t2) - 
               t1*(-25 + 31*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               s2*(-15 + 8*Power(t1,3) + Power(t1,2)*(27 - 11*t2) + 
                  6*t2 + 13*Power(t2,2) - 4*Power(t2,3) + 
                  2*t1*t2*(-1 + 4*t2)) - 
               Power(s2,2)*(-4 + 2*Power(t1,2) + t2 + 6*Power(t2,2) - 
                  t1*(3 + 4*t2))) + 
            Power(s1,2)*(Power(s2,5) - 2*Power(t1,5) + 
               Power(s2,4)*(1 + t1 - 6*t2) + Power(t1,4)*(7 + 6*t2) + 
               Power(t1,3)*(6 + 6*t2 - 6*Power(t2,2)) + 
               Power(-1 + t2,2)*(16 + 2*t2 + Power(t2,2)) + 
               Power(t1,2)*(-92 + 70*t2 - 19*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-53 + 67*t2 - 19*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,3)*(38 - 7*Power(t1,2) + 25*t2 - 
                  12*Power(t2,2) + t1*(-25 + 19*t2)) + 
               Power(s2,2)*(-63 + 5*Power(t1,3) + 
                  Power(t1,2)*(54 - 14*t2) + 23*t2 + 4*Power(t2,2) - 
                  3*Power(t2,3) + 2*t1*(-38 - 17*t2 + 7*Power(t2,2))) + 
               s2*(43 + 2*Power(t1,4) - 58*t2 + 33*Power(t2,2) - 
                  20*Power(t2,3) + 2*Power(t2,4) - 
                  Power(t1,3)*(37 + 5*t2) + 
                  Power(t1,2)*(32 + 3*t2 + 4*Power(t2,2)) + 
                  t1*(163 - 113*t2 + 31*Power(t2,2) - 3*Power(t2,3)))) - 
            s1*(-Power(t1,6) + Power(s2,5)*t2 + Power(t1,5)*(1 + 3*t2) + 
               Power(t1,4)*(7 - 10*t2 - 3*Power(t2,2)) + 
               Power(-1 + t2,2)*(51 - 17*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(10 - 17*t2 + 19*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(39 + 19*t2 - 20*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(73 - 154*t2 + 96*Power(t2,2) - 17*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s2,4)*(-4 + Power(t1,2) + 18*t2 + Power(t2,2) - 
                  t1*(2 + t2)) + 
               Power(s2,3)*(38 + 4*Power(t1,3) + t2 + 4*Power(t2,2) - 
                  5*Power(t2,3) - Power(t1,2)*(7 + 12*t2) + 
                  t1*(-8 + 41*t2 + 12*Power(t2,2))) + 
               Power(s2,2)*(-18 - 6*Power(t1,4) + 14*t2 - 
                  64*Power(t2,2) + 21*Power(t2,3) - 3*Power(t2,4) + 
                  Power(t1,3)*(9 + 20*t2) + 
                  Power(t1,2)*(11 - 38*t2 - 24*Power(t2,2)) + 
                  t1*(-88 + 17*t2 - 5*Power(t2,2) + 13*Power(t2,3))) + 
               s2*(-47 + 4*Power(t1,5) + 66*t2 + 3*Power(t2,2) - 
                  21*Power(t2,3) - Power(t2,4) - Power(t1,4)*(5 + 13*t2) + 
                  Power(t1,3)*(-14 + 25*t2 + 16*Power(t2,2)) - 
                  Power(t1,2)*
                   (-40 + t2 + 18*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(56 + 13*t2 + 30*Power(t2,2) - Power(t2,3) + 
                     2*Power(t2,4))))))*T5q(1 - s2 + t1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(s - s1 + t2)*(-1 + s2 - t1 + t2)*
       Power(-1 + s - s*s1 + s1*s2 - s1*t1 + t2,2)));
   return a;
};
