#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_1_m231_1_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*((-19 + Power(s,2) - 2*s*(-2 + s1) + 12*s1 + Power(s1,2))*
          Power(t1,5)*t2 - (-1 + s1 - t2)*Power(t2,2)*
          (1 + (3 - s + 2*s1)*t2 + 2*Power(t2,2) + 
            Power(s2,2)*(-2 + s1 + t2) + 
            s2*(1 + s1 + (2 + s)*t2 + 2*Power(t2,2))) - 
         Power(t1,4)*(2 + 9*Power(s1,3)*t2 + 
            (16 + Power(s,2) + 5*s*(-1 + s2) - 47*s2)*t2 + 
            (-59 + 7*Power(s,2) + s*(8 - 3*s2) - 3*s2)*Power(t2,2) + 
            4*(-2 + s)*Power(t2,3) + 
            Power(s1,2)*(-2 + (-52 - 13*s + 7*s2)*t2 + 2*Power(t2,2)) + 
            s1*(-2 + (21 + 6*s + 4*Power(s,2) + 40*s2 - 3*s*s2)*t2 + 
               (33 - 7*s + 5*s2)*Power(t2,2))) - 
         Power(t1,3)*(-4 + 8*Power(s1,4)*t2 + 
            (-48 + 8*Power(s,2) + s*(4 - 8*s2) + 29*s2 + 14*Power(s2,2))*
             t2 + (2 - 23*Power(s,2) + 74*s2 + 5*Power(s2,2) - 
               s*(7 + 6*s2))*Power(t2,2) + 
            (59 - 11*Power(s,2) + 6*s*(-4 + s2) + 6*s2 - 2*Power(s2,2))*
             Power(t2,3) - 8*(-2 + s)*Power(t2,4) - 
            Power(s1,3)*(2 + (37 + 8*s - 3*s2)*t2 + 19*Power(t2,2)) + 
            Power(s1,2)*(2 + (23 + 9*s + 26*s2 + 4*s*s2 - 
                  2*Power(s2,2))*t2 + (62 + 18*s - s2)*Power(t2,2) + 
               7*Power(t2,3)) + 
            s1*(6 + (45 + 17*s - 4*Power(s,2) - 60*s2 + 2*s*s2 - 
                  11*Power(s2,2))*t2 + 
               (16 + 15*s - 73*s2 + 6*s*s2 - 4*Power(s2,2))*Power(t2,2) + 
               4*(-11 + 4*s - s2)*Power(t2,3))) + 
         t1*t2*(-11 - 6*t2 + 6*s*t2 - 8*Power(s1,4)*t2 + 13*Power(t2,2) - 
            6*s*Power(t2,2) - 8*Power(s,2)*Power(t2,2) - 42*Power(t2,3) + 
            17*s*Power(t2,3) + 21*Power(s,2)*Power(t2,3) + 
            14*Power(t2,4) + 20*s*Power(t2,4) + 
            Power(s2,2)*(-10 - 17*t2 + Power(t2,2) + Power(t2,3)) + 
            s2*(29 + 2*(16 + s)*t2 + (-43 + 2*s)*Power(t2,2) - 
               (9 + 4*s)*Power(t2,3) - 2*Power(t2,4)) + 
            Power(s1,3)*(-10 + (30 + 8*s)*t2 + 8*Power(t2,2) + 
               3*s2*(2 + t2)) - 
            Power(s1,2)*(-18 + 2*(-5 + 2*s)*t2 + 
               (19 + 5*s)*Power(t2,2) - 8*(1 + s)*Power(t2,3) + 
               Power(s2,2)*(5 + 3*t2) + 
               s2*(-7 + (26 + 4*s)*t2 + 6*Power(t2,2))) - 
            s1*(-3 + 2*(16 + 5*s)*t2 + 
               (-3 + 9*s - 4*Power(s,2))*Power(t2,2) + 
               (17 + 27*s + 8*Power(s,2))*Power(t2,3) + 
               8*(1 + s)*Power(t2,4) + 
               Power(s2,2)*(-15 - 20*t2 + 2*Power(t2,2)) - 
               s2*(-42 + (-9 + 2*s)*t2 + (43 - 2*s)*Power(t2,2) + 
                  Power(t2,3)))) + 
         Power(t1,2)*(-2 - (2 + 47*s2 - 24*Power(s2,2) + s*(5 + 3*s2))*
             t2 + (-52 + 16*Power(s,2) - 11*s*(-1 + s2) + 38*s2 + 
               24*Power(s2,2))*Power(t2,2) + 
            (58 - 43*Power(s,2) + 46*s2 - 2*Power(s2,2) + s*(-29 + 3*s2))*
             Power(t2,3) + (5 - 5*Power(s,2) + 5*s2 - 2*Power(s2,2) + 
               s*(-40 + 3*s2))*Power(t2,4) - 4*(-2 + s)*Power(t2,5) + 
            8*Power(s1,4)*t2*(1 + t2) - 
            Power(s1,3)*(2 + (26 + 8*s + 3*s2)*t2 + 
               (33 + 8*s + 3*s2)*Power(t2,2) + 16*Power(t2,3)) + 
            s1*(4 + (51 - s*(-9 + s2) + 22*s2 - 26*Power(s2,2))*t2 + 
               (34 - 8*Power(s,2) - 59*s2 - 27*Power(s2,2) + 
                  s*(26 + 4*s2))*Power(t2,2) + 
               (12*Power(s,2) + 3*s*(16 + s2) + 
                  2*(21 - 23*s2 + Power(s2,2)))*Power(t2,3) + 
               (-15 + 19*s + s2)*Power(t2,4)) + 
            Power(s1,2)*t2*(-24 + 22*t2 + 20*Power(t2,2) + 8*Power(t2,3) + 
               Power(s2,2)*(3 + 4*t2) + s2*(26 + 26*t2 + 6*Power(t2,2)) + 
               s*(4 + 14*t2 - 3*Power(t2,2) + 4*s2*(1 + t2))))))/
     ((-1 + s1)*(-1 + t1)*t1*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*(-1 + t2)*
       t2*Power(-t1 + t2,2)) + 
    (8*(1 - 3*Power(t1,2) + 8*Power(s1,6)*Power(t1,2) + 3*Power(t1,4) - 
         Power(t1,6) - 9*t2 + 2*s2*t2 + 2*t1*t2 + 2*s2*t1*t2 + 
         23*Power(t1,2)*t2 - 4*s2*Power(t1,2)*t2 - 10*Power(t1,3)*t2 - 
         4*s2*Power(t1,3)*t2 - 13*Power(t1,4)*t2 + 2*s2*Power(t1,4)*t2 + 
         8*Power(t1,5)*t2 + 2*s2*Power(t1,5)*t2 - Power(t1,6)*t2 + 
         13*Power(t2,2) - 14*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) - 
         68*t1*Power(t2,2) - 3*s2*t1*Power(t2,2) + 
         2*Power(s2,2)*t1*Power(t2,2) + 49*Power(t1,2)*Power(t2,2) + 
         25*s2*Power(t1,2)*Power(t2,2) + 23*Power(t1,3)*Power(t2,2) + 
         s2*Power(t1,3)*Power(t2,2) - 
         2*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         18*Power(t1,4)*Power(t2,2) - 11*s2*Power(t1,4)*Power(t2,2) - 
         Power(s2,2)*Power(t1,4)*Power(t2,2) + Power(t1,5)*Power(t2,2) + 
         2*s2*Power(t1,5)*Power(t2,2) + 15*Power(t2,3) + 
         22*s2*Power(t2,3) - 5*Power(s2,2)*Power(t2,3) - 
         43*t1*Power(t2,3) - 57*s2*t1*Power(t2,3) - 
         Power(s2,2)*t1*Power(t2,3) - 13*Power(t1,2)*Power(t2,3) + 
         6*s2*Power(t1,2)*Power(t2,3) + 
         8*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         21*Power(t1,3)*Power(t2,3) + 15*s2*Power(t1,3)*Power(t2,3) + 
         3*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         2*s2*Power(t1,4)*Power(t2,3) - 
         Power(s2,2)*Power(t1,4)*Power(t2,3) + 26*Power(t2,4) + 
         12*s2*Power(t2,4) + 2*Power(s2,2)*Power(t2,4) - 
         6*t1*Power(t2,4) - 9*s2*t1*Power(t2,4) - 
         8*Power(s2,2)*t1*Power(t2,4) - 11*Power(t1,2)*Power(t2,4) - 
         10*s2*Power(t1,2)*Power(t2,4) - 
         Power(s2,2)*Power(t1,2)*Power(t2,4) - Power(t1,3)*Power(t2,4) + 
         s2*Power(t1,3)*Power(t2,4) + 
         Power(s2,2)*Power(t1,3)*Power(t2,4) + 2*Power(t2,5) + 
         6*s2*Power(t2,5) + 3*t1*Power(t2,5) + s2*t1*Power(t2,5) + 
         Power(t1,2)*Power(t2,5) - s2*Power(t1,2)*Power(t2,5) + 
         Power(s,2)*(-(Power(t1,6)*(1 + t2)) + 
            Power(t2,2)*(3 - 9*t2 - 4*Power(t2,2)) + 
            Power(t1,4)*t2*(15 - 5*t2 - 3*Power(t2,2)) + 
            Power(t1,5)*(-2 + 4*t2 + 3*Power(t2,2)) + 
            t1*t2*(-4 + 21*t2 + 13*Power(t2,2) - 9*Power(t2,3)) + 
            Power(t1,2)*(1 - 12*t2 - 13*Power(t2,2) + 27*Power(t2,3)) + 
            Power(t1,3)*(2 + 2*t2 - 31*Power(t2,2) + 2*Power(t2,3) + 
               Power(t2,4))) + 
         s*(2*Power(t1,6)*(1 + t2) - 
            2*Power(t1,5)*(-1 + (7 + s2)*t2 + (3 + s2)*Power(t2,2)) + 
            Power(t1,4)*(-4 - (11 + 2*s2)*t2 + 
               (31 + 9*s2)*Power(t2,2) + (7 + 4*s2)*Power(t2,3)) - 
            2*t2*(1 + s2 + t2 - 4*s2*t2 - 5*Power(t2,2) + 
               12*Power(t2,3) - 3*s2*Power(t2,3) + Power(t2,4)) + 
            Power(t1,3)*(-4 + (25 + 4*s2)*t2 + 
               (30 + 17*s2)*Power(t2,2) - 2*(15 + 4*s2)*Power(t2,3) - 
               2*(2 + s2)*Power(t2,4)) + 
            t1*(2 - (11 + 2*s2)*t2 + (2 - 15*s2)*Power(t2,2) + 
               6*(12 + s2)*Power(t2,3) + (26 + 17*s2)*Power(t2,4) - 
               Power(t2,5)) + 
            Power(t1,2)*(2 + (11 + 4*s2)*t2 - (71 + 9*s2)*Power(t2,2) - 
               (45 + 34*s2)*Power(t2,3) + (12 + s2)*Power(t2,4) + 
               Power(t2,5))) - 
         Power(s1,5)*(Power(s2,2)*(1 + Power(t1,2)) + 
            s2*t1*(-2 - 2*Power(t1,2) - s*(1 + t1) + t2 + t1*(2 + t2)) + 
            t1*(Power(t1,3) + Power(t1,2)*(-20 + s - t2) + 16*t2 + 
               t1*(-1 + 5*s + 19*t2))) + 
         s1*(-4 + 15*t2 + 3*s*t2 - 17*Power(t2,2) + 20*s*Power(t2,2) - 
            5*Power(s,2)*Power(t2,2) + 6*s*Power(t2,3) + 
            16*Power(s,2)*Power(t2,3) - 3*Power(t2,4) + 
            8*s*Power(t2,4) - 3*Power(t2,5) + s*Power(t2,5) + 
            2*Power(t1,6)*(-2 + s*(3 + t2)) - 
            Power(t1,5)*(-16 - 16*t2 + Power(t2,2) + 
               Power(s,2)*(3 + 2*t2) + s*(3 + 20*t2 + 5*Power(t2,2))) + 
            Power(s2,2)*t2*(-2 + 6*t2 + 4*Power(t2,2) - Power(t2,3) + 
               Power(t1,4)*(2 + t2) + 
               Power(t1,3)*(4 - 6*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*t2*(-13 + 4*t2 + Power(t2,2)) + 
               t1*(-4 + 11*Power(t2,2) + 2*Power(t2,3))) + 
            Power(t1,4)*(-2 - 21*t2 - 28*Power(t2,2) + 3*Power(t2,3) + 
               Power(s,2)*t2*(4 + 5*t2) + 
               s*(-9 - 14*t2 + 17*Power(t2,2) + 4*Power(t2,3))) + 
            Power(t1,3)*(-26 - 72*t2 + 7*Power(t2,2) + 21*Power(t2,3) - 
               2*Power(t2,4) + Power(s,2)*(1 + 15*t2 - 4*Power(t2,3)) + 
               s*(11 + 79*t2 + 55*Power(t2,2) + Power(t2,3) - 
                  Power(t2,4))) + 
            Power(t1,2)*(10 + 94*t2 + 91*Power(t2,2) + 11*Power(t2,3) - 
               9*Power(t2,4) + 
               Power(s,2)*(-2 + 2*t2 - 13*Power(t2,2) - 
                  4*Power(t2,3) + Power(t2,4)) - 
               s*(-3 + 43*t2 + 101*Power(t2,2) + 35*Power(t2,3) + 
                  5*Power(t2,4))) + 
            t1*(10 - 32*t2 + 8*Power(t2,2) - 75*Power(t2,3) + 
               6*Power(t2,4) + Power(t2,5) + 
               Power(s,2)*t2*
                (9 - 27*t2 + 4*Power(t2,2) + 3*Power(t2,3)) + 
               s*(-8 + 25*t2 - 28*Power(t2,2) + 26*Power(t2,3) - 
                  2*Power(t2,4) + Power(t2,5))) - 
            s2*(2 - 6*(1 + 2*s)*t2 + (-1 + 29*s)*Power(t2,2) + 
               (45 + 22*s)*Power(t2,3) - (-13 + s)*Power(t2,4) + 
               Power(t2,5) + Power(t1,5)*(2 + (-3 + s)*t2) + 
               Power(t1,4)*(2 + 2*(1 + s)*t2 + (5 + 4*s)*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(t1,3)*(4 - 2*(7 + 8*s)*t2 + 
                  (-23 + 13*s)*Power(t2,2) + (4 + 7*s)*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*(-4 + 2*(2 + 5*s)*t2 - 
                  (62 + 27*s)*Power(t2,2) + 2*(-16 + s)*Power(t2,3) + 
                  (-3 + 2*s)*Power(t2,4)) + 
               t1*(2 - (11 + s)*t2 - 13*(1 + 5*s)*Power(t2,2) + 
                  (-18 + 31*s)*Power(t2,3) + (13 + 5*s)*Power(t2,4) + 
                  Power(t2,5)))) + 
         Power(s1,4)*(-2*Power(t1,5) + 8*Power(t2,2) + 
            Power(t1,4)*(18 - s + 4*t2) + 
            t1*(-2 + s - t2 + 10*s*t2 + Power(s,2)*t2 + 
               38*Power(t2,2) - s*Power(t2,2)) + 
            Power(s2,2)*(4 - 2*Power(t1,3) + t1*(-1 + 2*t2) + 
               Power(t1,2)*(1 + 2*t2)) + 
            Power(t1,3)*(15 + Power(s,2) - 28*t2 - 2*Power(t2,2) + 
               s*(-8 + 3*t2)) + 
            Power(t1,2)*(-13 + Power(s,2)*(-3 + t2) - 45*t2 + 
               12*Power(t2,2) - s*(8 - 11*t2 + Power(t2,2))) + 
            s2*(2 + 4*Power(t1,4) + Power(t1,3)*(-11 + s - 6*t2) - 
               2*t2 - 3*s*t2 + Power(t2,2) + 
               Power(t1,2)*(22 + s*(7 - 3*t2) - t2 + 3*Power(t2,2)) + 
               t1*(-7 + 7*t2 + 4*Power(t2,2) - 2*s*(1 + t2)))) + 
         Power(s1,3)*(-1 - Power(t1,6) + 2*t2 + s*t2 - 5*s*Power(t2,2) - 
            2*Power(s,2)*Power(t2,2) - 19*Power(t2,3) + s*Power(t2,3) + 
            Power(t1,5)*(4 + s + 4*t2) + 
            Power(t1,4)*(16 + 2*Power(s,2) - 5*t2 - 4*Power(t2,2) + 
               s*(-1 + 6*t2)) - 
            Power(s2,2)*(4 - 3*Power(t1,2) + Power(t1,4) - 
               3*Power(t1,3)*t2 - 2*Power(t2,2) + 
               t1*(2 + 3*t2 + 2*Power(t2,2))) + 
            Power(t1,3)*(7 + Power(s,2)*(-9 + t2) - 61*t2 + 
               4*Power(t2,2) + Power(t2,3) - s*(27 + 7*Power(t2,2))) + 
            Power(t1,2)*(-12 - 19*t2 + 56*Power(t2,2) + Power(t2,3) + 
               Power(s,2)*(3 + 2*t2 - Power(t2,2)) + 
               s*(21 + 26*t2 - 5*Power(t2,2) + 2*Power(t2,3))) + 
            t1*(7 + 23*t2 - Power(s,2)*(-5 + t2)*t2 + 28*Power(t2,2) - 
               27*Power(t2,3) + 
               s*(-8 + 21*t2 - 17*Power(t2,2) + 3*Power(t2,3))) + 
            s2*(-8 + 2*Power(t1,5) + (7 + 12*s)*t2 - 
               (5 + s)*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,3)*(23 - 5*s*(-3 + t2) + 22*t2 + 
                  7*Power(t2,2)) - Power(t1,4)*(s + 7*(2 + t2)) + 
               Power(t1,2)*(-4 + 6*t2 + Power(t2,2) - 3*Power(t2,3) + 
                  s*(-9 - 4*t2 + Power(t2,2))) + 
               t1*(17 - 52*t2 - 15*Power(t2,2) - 6*Power(t2,3) + 
                  s*(3 - 19*t2 + 4*Power(t2,2))))) + 
         Power(s1,2)*(4 - 7*t2 - 2*s*t2 - 10*Power(t2,2) - 
            11*s*Power(t2,2) + 4*Power(s,2)*Power(t2,2) - 3*Power(t2,3) + 
            7*s*Power(t2,3) - 2*Power(s,2)*Power(t2,3) + 14*Power(t2,4) - 
            2*s*Power(t2,4) + Power(t1,6)*(-2 + s + t2) + 
            Power(t1,5)*(-5 + Power(s,2) + 9*t2 - 2*Power(t2,2) + 
               s*(8 + 5*t2)) - 
            Power(t1,4)*(-35 - 2*t2 + 14*Power(t2,2) - Power(t2,3) + 
               Power(s,2)*(8 + t2) + s*(32 + 25*t2 + 11*Power(t2,2))) + 
            Power(s2,2)*(1 + Power(t1,4)*(-1 + t2) + 3*t2 - 
               10*Power(t2,2) - 
               2*Power(t1,2)*t2*(-1 + 2*t2 + Power(t2,2)) + 
               Power(t1,3)*(-2 + 3*t2 + Power(t2,2)) + 
               t1*(2 + 3*t2 + Power(t2,2) - 2*Power(t2,3))) + 
            Power(t1,3)*(-23 - 67*t2 + 2*Power(t2,2) + 10*Power(t2,3) + 
               Power(s,2)*t2*(4 + t2) + 
               s*(5 + 46*t2 + 5*Power(t2,2) + 6*Power(t2,3))) - 
            Power(t1,2)*(-7 + 30*t2 - 92*Power(t2,2) + 16*Power(t2,3) + 
               2*Power(t2,4) + 
               Power(s,2)*(-1 - 22*t2 + 3*Power(t2,2) + Power(t2,3)) + 
               s*(11 - 67*t2 + 5*Power(t2,2) - 11*Power(t2,3) + 
                  Power(t2,4))) + 
            t1*(Power(s,2)*t2*(-11 - 8*t2 + Power(t2,2)) + 
               4*(-4 + 8*t2 + Power(t2,2) - 6*Power(t2,3) + 
                  Power(t2,4)) - 
               s*(-13 + 49*t2 + 28*Power(t2,2) + 6*Power(t2,3) + 
                  3*Power(t2,4))) + 
            s2*(8 - (15 + 19*s)*t2 + 4*(6 + 5*s)*Power(t2,2) + 
               (14 + 3*s)*Power(t2,3) + 3*Power(t2,4) - 
               Power(t1,5)*(5 + s + 2*t2) + 
               Power(t1,4)*(13 - 3*s*(-3 + t2) + 21*t2 + 4*Power(t2,2)) - 
               Power(t1,3)*(-13 + 15*t2 + 16*Power(t2,2) + 
                  4*Power(t2,3) + s*(-3 + 8*t2 + Power(t2,2))) + 
               t1*(-8 + 27*t2 + 50*Power(t2,2) + 18*Power(t2,3) + 
                  4*Power(t2,4) + 
                  s*(-2 + 26*t2 + 3*Power(t2,2) + 2*Power(t2,3))) + 
               Power(t1,2)*(-21 - 64*t2 - 50*Power(t2,2) + Power(t2,4) + 
                  s*(-1 - 44*t2 + 14*Power(t2,2) + 3*Power(t2,3))))))*
       B1(1 - s1 - t1 + t2,t1,t2))/
     ((-1 + s1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*
       (-1 + t2)) - (8*(8*Power(s1,5)*Power(-1 + t1,2)*Power(t1,3)*
          (t1 - t2) + Power(t1,4)*
          (-2 + 2*(12 + 8*s + Power(s,2) - 17*s2 + 10*Power(s2,2))*t2 + 
            (252 - 16*Power(s,2) + 98*s2 + 6*Power(s2,2) + 
               s*(-46 + 49*s2))*Power(t2,2) + 
            (391 - 66*Power(s,2) + 182*s2 + 19*Power(s2,2) + 
               s*(-219 + 13*s2))*Power(t2,3) + 
            (9 + 14*Power(s,2) + 104*s2 + 19*Power(s2,2) - 
               s*(139 + 44*s2))*Power(t2,4) + 
            2*(-4 + Power(s,2) + 5*s2 + Power(s2,2) - 2*s*(5 + s2))*
             Power(t2,5)) + 2*Power(t1,8)*
          (1 - 2*s*(1 + t2) + Power(s,2)*(1 + t2)) + 
         Power(t2,3)*(1 + t2)*
          (1 + Power(s2,2)*(-2 + t2) - (-3 + s)*t2 + 2*Power(t2,2) + 
            s2*(1 + (2 + s)*t2 + 2*Power(t2,2))) + 
         Power(t1,7)*(-4 + (-28 + s2)*t2 - 4*(-3 + s2)*Power(t2,2) - 
            2*Power(s,2)*t2*(7 + 4*t2) + 
            s*(4 + (34 + 5*s2)*t2 + 2*(7 + 2*s2)*Power(t2,2))) - 
         t1*Power(t2,2)*(3 + 27*t2 + 
            (59 + 4*s + 2*Power(s,2))*Power(t2,2) + 
            (59 + 7*s + 2*Power(s,2))*Power(t2,3) + 
            2*(12 + s)*Power(t2,4) + 
            Power(s2,2)*(-8 - 16*t2 + 2*Power(t2,2) + 7*Power(t2,3)) + 
            s2*(8 + (18 + 5*s)*t2 + (37 + 15*s)*Power(t2,2) + 
               (44 + 9*s)*Power(t2,3) + 16*Power(t2,4))) - 
         Power(t1,5)*(-4 + (36 + 34*s2 + 4*Power(s2,2))*t2 + 
            (227 + 78*s2 + 6*Power(s2,2))*Power(t2,2) + 
            (43 + 65*s2 + 13*Power(s2,2))*Power(t2,3) + 
            4*(-7 + 3*s2 + Power(s2,2))*Power(t2,4) + 
            2*Power(s,2)*t2*(-5 - 19*t2 + 16*Power(t2,2) + 
               4*Power(t2,3)) - 
            s*(-4 - 6*(-2 + s2)*t2 + (180 - 7*s2)*Power(t2,2) + 
               3*(49 + 18*s2)*Power(t2,3) + 4*(7 + 3*s2)*Power(t2,4))) + 
         Power(t1,2)*t2*(14 - (-45 + s)*t2 + 
            (148 + 27*s + 6*Power(s,2))*Power(t2,2) + 
            (267 + 2*Power(s,2))*Power(t2,3) + 
            (127 - 19*s - 14*Power(s,2))*Power(t2,4) - 
            2*(1 + 10*s)*Power(t2,5) + 
            Power(s2,2)*(6 - 13*t2 - 27*Power(t2,2) + 18*Power(t2,3) + 
               13*Power(t2,4)) + 
            s2*(-23 + (-8 + 3*s)*t2 + (99 + 35*s)*Power(t2,2) + 
               (145 + 51*s)*Power(t2,3) - (-102 + s)*Power(t2,4) + 
               18*Power(t2,5))) + 
         Power(s1,4)*Power(t1,2)*
          (13*Power(t1,5) - Power(t1,4)*(47 + 8*s - 3*s2 + 25*t2) + 
            Power(t1,3)*(59 + s2*(-1 + t2) + 37*t2 + 14*Power(t2,2) + 
               8*s*(2 + t2)) + t2*(s2 + 2*s2*t2 + 4*t2*(3 + t2)) - 
            Power(t1,2)*(5 + 47*t2 - 8*Power(t2,2) - 4*Power(t2,3) + 
               8*s*(1 + 2*t2) + s2*(7 + t2 - 2*Power(t2,2))) - 
            t1*(12 - (11 + 8*s)*t2 + 10*Power(t2,2) + 16*Power(t2,3) + 
               s2*(-5 + t2 + 4*Power(t2,2)))) + 
         Power(t1,6)*(2*Power(s,2)*
             (-1 - 4*t2 + 16*Power(t2,2) + 6*Power(t2,3)) - 
            s*(-4 + 60*t2 + (97 + 28*s2)*Power(t2,2) + 
               12*(2 + s2)*Power(t2,3)) + 
            t2*(58 + 55*t2 - 32*Power(t2,2) + 
               Power(s2,2)*(-2 + 3*t2 + 2*Power(t2,2)) + 
               s2*(25 + 14*t2 + 10*Power(t2,2)))) + 
         Power(t1,3)*t2*(-32 - 134*t2 - 414*Power(t2,2) - 
            321*Power(t2,3) + 7*Power(t2,4) - 
            2*Power(s,2)*t2*(3 - 4*t2 - 25*Power(t2,2) + Power(t2,3)) + 
            Power(s2,2)*(-20 + 2*t2 + 5*Power(t2,2) - 30*Power(t2,3) - 
               9*Power(t2,4)) - 
            s2*(-65 + 14*t2 + 209*Power(t2,2) + 203*Power(t2,3) + 
               72*Power(t2,4) + 4*Power(t2,5)) + 
            s*(2 - 34*t2 + 37*Power(t2,2) + 116*Power(t2,3) + 
               79*Power(t2,4) + 6*Power(t2,5) + 
               s2*(1 - 21*t2 - 85*Power(t2,2) - 5*Power(t2,3) + 
                  13*Power(t2,4)))) + 
         Power(s1,3)*t1*(5*Power(t1,7) - 
            Power(t1,6)*(41 + 13*s - 6*s2 + 20*t2) + 
            Power(t2,2)*(Power(s2,2)*(1 - 2*t2) - 2*s2*t2 - 
               2*t2*(1 + 2*t2)) + 
            Power(t1,5)*(44 - Power(s2,2) + 23*t2 + 29*Power(t2,2) + 
               7*s*(3 + 2*t2) + s2*(21 + 5*t2)) + 
            t1*t2*(15 - (35 + 11*s)*t2 - 2*(8 + s)*Power(t2,2) + 
               12*Power(t2,3) + Power(s2,2)*(-4 + t2 + 4*Power(t2,2)) + 
               s2*(1 + t2)*(-7 - 2*(-2 + s)*t2 + 4*Power(t2,2))) - 
            Power(t1,4)*(-6 - 40*t2 - 46*Power(t2,2) + 10*Power(t2,3) + 
               Power(s2,2)*(1 + 6*t2) + 
               s*(17 + s2*(2 - 6*t2) + 16*t2 - 19*Power(t2,2)) + 
               s2*(49 + 42*t2 + 19*Power(t2,2))) + 
            Power(t1,2)*(15 + 2*(18 + 5*s)*t2 + 
               (54 + 11*s)*Power(t2,2) + 2*(26 + 7*s)*Power(t2,3) + 
               4*(5 + 2*s)*Power(t2,4) - 
               Power(s2,2)*(3 - 2*t2 + 5*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(7 - 10*t2 + (-13 + 10*s)*Power(t2,2) + 
                  2*(-9 + s)*Power(t2,3) - 4*Power(t2,4))) + 
            Power(t1,3)*(-53 - 46*t2 - 94*Power(t2,2) - 72*Power(t2,3) - 
               4*Power(t2,4) + Power(s2,2)*(5 + 8*t2 + 3*Power(t2,2)) + 
               s2*(15 + 54*t2 + 35*Power(t2,2) + 12*Power(t2,3)) + 
               s*(1 + 8*t2 - 19*Power(t2,2) - 28*Power(t2,3) + 
                  s2*(2 - 6*t2 - 8*Power(t2,2))))) + 
         Power(s1,2)*(-(Power(t2,3)*(s2 + Power(s2,2) + 2*t2)) - 
            Power(t1,8)*(17 + 5*s - 3*s2 + 4*t2) + 
            Power(t1,7)*(11 + 29*s2 - Power(s2,2) - 
               s*(16 + s2 - 5*t2) + 19*t2 + 12*Power(t2,2)) + 
            t1*Power(t2,2)*(-2 + (17 + 2*s)*t2 + 
               2*(5 + s)*Power(t2,2) + 4*Power(t2,3) + 
               Power(s2,2)*(1 + 9*t2) + 
               s2*(2 + 3*(3 + s)*t2 - 2*(1 + s)*Power(t2,2) - 
                  4*Power(t2,3))) - 
            Power(t1,2)*t2*(21 - (-47 + s)*t2 + 
               (8 - 9*s + 2*Power(s,2))*Power(t2,2) + 
               2*(37 + 7*s + Power(s,2))*Power(t2,3) + 
               8*(3 + s)*Power(t2,4) + 
               Power(s2,2)*(-15 + 19*Power(t2,2)) + 
               s2*(14 + (5 + 3*s)*t2 + (21 - 5*s)*Power(t2,2) + 
                  10*Power(t2,3))) + 
            Power(t1,3)*(11 + (61 - 12*s)*t2 + 
               (48 - 28*s + 6*Power(s,2))*Power(t2,2) + 
               (144 - 37*s + 8*Power(s,2))*Power(t2,3) - 
               2*(-7 + 3*s + 3*Power(s,2))*Power(t2,4) - 
               4*(1 + 2*s)*Power(t2,5) + 
               3*Power(s2,2)*(3 - 5*t2 + 5*Power(t2,3)) + 
               s2*(-35 + (5 + s)*t2 + (80 - 15*s)*Power(t2,2) + 
                  (67 - 3*s)*Power(t2,3) + 2*(17 + s)*Power(t2,4) + 
                  4*Power(t2,5))) + 
            Power(t1,6)*(36 + 83*t2 + 23*Power(t2,2) - 12*Power(t2,3) - 
               4*Power(s2,2)*(1 + t2) + Power(s,2)*(-2 + 6*t2) - 
               s2*(94 + 91*t2 + 17*Power(t2,2)) + 
               s*(40 + 103*t2 + 23*Power(t2,2) + s2*(5 + t2))) + 
            Power(t1,5)*(-54 - 52*t2 - 126*Power(t2,2) - 
               69*Power(t2,3) + 4*Power(t2,4) + 
               Power(s,2)*(2 + 8*t2 - 18*Power(t2,2)) + 
               Power(s2,2)*(20 + 23*t2 + 3*Power(t2,2)) + 
               s2*(46 + 167*t2 + 126*Power(t2,2) + 32*Power(t2,3)) - 
               s*(12 + 141*t2 + 180*Power(t2,2) + 49*Power(t2,3) + 
                  s2*(3 + 17*t2 - 3*Power(t2,2)))) + 
            Power(t1,4)*(13 - 14*t2 - 100*Power(t2,2) + 72*Power(t2,3) + 
               48*Power(t2,4) + 
               6*Power(s,2)*t2*(-1 - 2*t2 + 3*Power(t2,2)) - 
               Power(s2,2)*(24 + 19*t2 + 4*Power(t2,2) + 
                  4*Power(t2,3)) - 
               s2*(-51 + 67*t2 + 186*Power(t2,2) + 86*Power(t2,3) + 
                  22*Power(t2,4)) + 
               s*(9 + 29*t2 + 152*Power(t2,2) + 107*Power(t2,3) + 
                  34*Power(t2,4) + 
                  s2*(-1 + 15*t2 + 15*Power(t2,2) - 5*Power(t2,3))))) + 
         s1*(2*Power(t1,9) - Power(t2,3)*
             (1 - 3*Power(s2,2) + t2 - s*t2 + s2*t2*(1 + s + 2*t2)) - 
            Power(t1,8)*(-14 + 5*s2 + 16*t2 + s*(6 + s2 + 2*t2)) + 
            Power(t1,7)*(-38 - Power(s2,2)*(-2 + t2) - 14*t2 + 
               38*Power(t2,2) + 6*Power(s,2)*(1 + t2) + 
               s2*(-17 + 11*t2 - 2*Power(t2,2)) + 
               s*(40 - 4*s2 + 38*t2 + 12*Power(t2,2))) + 
            t1*Power(t2,2)*(5 - 2*(-5 + s)*t2 + 
               (12 + s + 2*Power(s,2))*Power(t2,2) + 20*Power(t2,3) + 
               Power(s2,2)*(-10 - 17*t2 + Power(t2,2) + 2*Power(t2,3)) + 
               s2*(6 + (-1 + 2*s)*t2 + (23 + 8*s)*Power(t2,2) + 
                  2*(12 + s)*Power(t2,3) + 4*Power(t2,4))) + 
            Power(t1,2)*t2*(-8 + (29 + 11*s)*t2 - 
               2*(-1 + 8*s + 2*Power(s,2))*Power(t2,2) + 
               (-36 + 51*s + 8*Power(s,2))*Power(t2,3) + 
               (68 + 36*s + 6*Power(s,2))*Power(t2,4) + 
               8*(1 + s)*Power(t2,5) + 
               Power(s2,2)*(-17 + 4*t2 + 30*Power(t2,2) - 
                  7*Power(t2,3) - 4*Power(t2,4)) - 
               s2*(-43 - 2*(15 + s)*t2 + 4*(13 + 4*s)*Power(t2,2) + 
                  (59 + 8*s)*Power(t2,3) - 2*(-15 + s)*Power(t2,4) + 
                  4*Power(t2,5))) + 
            Power(t1,3)*(-14 - 2*(37 + 4*s)*t2 + 
               (-162 + 13*s)*Power(t2,2) - 
               4*(40 + 25*s + 7*Power(s,2))*Power(t2,3) - 
               (220 + 111*s + 6*Power(s,2))*Power(t2,4) + 
               2*(-18 - 7*s + Power(s,2))*Power(t2,5) + 
               Power(s2,2)*(-6 + 35*t2 + 25*Power(t2,2) - 
                  16*Power(t2,3) + 11*Power(t2,4) + 2*Power(t2,5)) + 
               s2*(23 - (63 + 2*s)*t2 + (-55 + 4*s)*Power(t2,2) - 
                  8*(-9 + s)*Power(t2,3) + (11 - 14*s)*Power(t2,4) - 
                  4*(-1 + s)*Power(t2,5))) + 
            Power(t1,6)*(40 + 184*t2 + 5*Power(t2,2) - 40*Power(t2,3) + 
               Power(s,2)*(4 - 30*t2 - 20*Power(t2,2)) + 
               Power(s2,2)*(4 + 3*t2 - Power(t2,2)) + 
               s2*(34 + 9*t2 + 17*Power(t2,2) + 8*Power(t2,3)) + 
               2*s*(s2*(1 + 14*t2 + 5*Power(t2,2)) - 
                  2*(10 + 31*t2 + 22*Power(t2,2) + 6*Power(t2,3)))) + 
            Power(t1,5)*(-46 - 296*t2 - 413*Power(t2,2) - 
               10*Power(t2,3) + 20*Power(t2,4) + 
               Power(s2,2)*(-20 - 2*t2 + 5*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(s,2)*(-2 - 20*t2 + 48*Power(t2,2) + 
                  24*Power(t2,3)) - 
               s2*(-26 + 12*t2 + 33*Power(t2,2) + 59*Power(t2,3) + 
                  10*Power(t2,4)) + 
               s*(8 + 70*t2 + 89*Power(t2,2) + 88*Power(t2,3) + 
                  20*Power(t2,4) - 
                  2*s2*(-2 + 13*t2 + 27*Power(t2,2) + 10*Power(t2,3)))) + 
            Power(t1,4)*(42 + 224*t2 + 426*Power(t2,2) + 
               407*Power(t2,3) + 33*Power(t2,4) - 4*Power(t2,5) - 
               4*Power(s,2)*t2*
                (-1 - 9*t2 + 6*Power(t2,2) + 3*Power(t2,3)) - 
               Power(s2,2)*(-20 + 18*t2 + 23*Power(t2,2) + 
                  5*Power(t2,3) + 5*Power(t2,4)) + 
               s2*(-61 + 12*t2 + 37*Power(t2,2) + 32*Power(t2,3) + 
                  36*Power(t2,4) + 4*Power(t2,5)) + 
               s*(-2 - 6*t2 + 19*Power(t2,2) + 70*Power(t2,3) - 
                  26*Power(t2,4) - 6*Power(t2,5) + 
                  s2*(-1 + 38*Power(t2,2) + 42*Power(t2,3) + 
                     15*Power(t2,4))))))*R1(t1))/
     ((-1 + s1)*Power(-1 + t1,2)*t1*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       (s1*t1 - t2)*(-1 + t2)*Power(-t1 + t2,3)) + 
    (8*(4*Power(s1,7)*t1*t2*(Power(t1,2) - Power(t2,2)) + 
         Power(s1,6)*(14*Power(t1,4)*t2 + 4*Power(t2,4) + 
            Power(t1,2)*t2*(-8 + s2 - 17*t2 + s2*t2 - 19*Power(t2,2)) + 
            Power(t1,3)*(2 + (-13 - 4*s + 2*s2)*t2 - 3*Power(t2,2)) + 
            t1*Power(t2,2)*(-6 + 4*(5 + s)*t2 + 14*Power(t2,2) + 
               s2*(5 + 3*t2))) + 
         Power(s1,5)*(18*Power(t1,5)*t2 - 
            Power(t1,4)*(-6 + (55 + 14*s - 6*s2)*t2 + Power(t2,2)) - 
            Power(t2,3)*(-6 + 4*(5 + s)*t2 + 2*Power(s2,2)*t2 + 
               14*Power(t2,2) + s2*(5 + 3*t2)) - 
            Power(t1,3)*t2*(33 + s + 45*t2 - s*t2 + 56*Power(t2,2) - 
               s2*(13 + 11*t2)) + 
            t1*Power(t2,2)*(23 + 2*Power(s2,2)*(-3 + t2) - 
               5*(-1 + s)*t2 + (2 - 11*s)*Power(t2,2) - 
               22*Power(t2,3) + s2*(3 - (19 + 4*s)*t2)) + 
            Power(t1,2)*t2*(1 + 2*(12 + 5*s)*t2 - 6*Power(s2,2)*t2 + 
               4*(19 + 6*s)*Power(t2,2) + 73*Power(t2,3) + 
               s2*(2 + (21 + 4*s)*t2 - 5*Power(t2,2)))) + 
         Power(s1,4)*(10*Power(t1,6)*t2 + 
            Power(t1,5)*(6 - 3*(29 + 6*s - 2*s2)*t2 + 7*Power(t2,2)) + 
            Power(t1,4)*(2 + (-26 + 3*s + 29*s2)*t2 + 
               (-21 - 7*s + 37*s2)*Power(t2,2) - 83*Power(t2,3)) + 
            Power(t1,3)*(-4 + (-6 + 4*s - 7*s2)*t2 + 
               (106 + 25*s2 - 20*Power(s2,2) + s*(35 + 12*s2))*
                Power(t2,2) + 5*(26 + 15*s - 14*s2)*Power(t2,3) + 
               140*Power(t2,4)) + 
            Power(t2,3)*(-15 + (8 + 5*s)*t2 + (17 + 11*s)*Power(t2,2) + 
               22*Power(t2,3) + 
               2*Power(s2,2)*(3 + 4*t2 + Power(t2,2)) + 
               s2*(-4 + (22 + 4*s)*t2 - 4*s*Power(t2,2))) - 
            Power(t1,2)*t2*(-14 - (140 + s)*t2 + 
               4*(-31 + 3*s)*Power(t2,2) + (22 + 65*s)*Power(t2,3) + 
               80*Power(t2,4) - 4*Power(s2,2)*t2*(-3 + 4*t2) + 
               s2*(7 + 14*t2 + (67 + 20*s)*Power(t2,2) - 54*Power(t2,3))\
) + t1*Power(t2,2)*(-7 - 2*(9 + 5*s)*t2 - (173 + 37*s)*Power(t2,2) + 
               5*(-20 + 3*s)*Power(t2,3) + 12*Power(t2,4) - 
               2*Power(s2,2)*(-9 - 5*t2 + 8*Power(t2,2)) - 
               s2*(37 + (61 + 4*s)*t2 - (7 + 12*s)*Power(t2,2) + 
                  21*Power(t2,3)))) + 
         Power(s1,3)*(2*Power(t1,7)*t2 + 
            Power(t1,6)*(2 + (-61 - 10*s + 2*s2)*t2 + 5*Power(t2,2)) + 
            Power(t1,5)*(4 + (19 + 15*s + 23*s2)*t2 + 
               (31 - 17*s + 45*s2)*Power(t2,2) - 46*Power(t2,3)) + 
            Power(t1,4)*(-10 + (-8 + s - 22*s2)*t2 + 
               (87 + 2*Power(s,2) + 3*s2 - 22*Power(s2,2) + 
                  s*(27 + 8*s2))*Power(t2,2) + 
               (54 + 102*s - 121*s2)*Power(t2,3) + 93*Power(t2,4)) + 
            Power(t1,3)*(2 + (46 + s - 16*s2)*t2 + 
               (236 + s*(19 - 4*s2) - 88*s2 + 6*Power(s2,2))*
                Power(t2,2) - 
               (-226 + 4*Power(s,2) + 70*s2 - 22*Power(s2,2) + 
                  s*(19 + 16*s2))*Power(t2,3) + 
               (14 - 121*s + 134*s2)*Power(t2,4) - 70*Power(t2,5)) + 
            Power(t2,3)*(4 + (5 + 4*s)*t2 + (100 + 17*s)*Power(t2,2) + 
               (27 - 15*s - 2*Power(s,2))*Power(t2,3) - 
               12*Power(t2,4) + 
               2*Power(s2,2)*
                (-9 - 10*t2 + 3*Power(t2,2) + Power(t2,3)) + 
               s2*(35 + 18*t2 + 6*(-1 + 2*s)*Power(t2,2) + 
                  21*Power(t2,3))) + 
            t1*Power(t2,2)*(-19 + (-142 + 5*s)*t2 + 
               (-34 + 27*s)*Power(t2,2) + 
               (257 + 98*s + 4*Power(s,2))*Power(t2,3) + 
               (138 - 8*s)*Power(t2,4) - 
               2*Power(s2,2)*
                (6 - 14*t2 - 15*Power(t2,2) + Power(t2,3)) + 
               s2*(33 + (42 + 4*s)*t2 + (150 - 4*s)*Power(t2,2) - 
                  (23 + 4*s)*Power(t2,3) + 10*Power(t2,4))) + 
            Power(t1,2)*t2*(-5 - (73 + 10*s)*t2 - 
               4*(67 + 16*s)*Power(t2,2) - 2*(228 + 53*s)*Power(t2,3) + 
               2*(-94 + 27*s)*Power(t2,4) + 16*Power(t2,5) + 
               2*Power(s2,2)*t2*(23 + 12*t2 - 9*Power(t2,2)) - 
               2*s2*(-2 + 2*(23 + s)*t2 + (65 + 2*s)*Power(t2,2) - 
                  (35 + 6*s)*Power(t2,3) + 35*Power(t2,4)))) + 
         t2*(Power(t1,5)*(2 + (-9 + 5*s + 2*Power(s,2) + 8*s2)*t2 + 
               (3 - 14*Power(s,2) + 18*s2 + s*(41 + 8*s2))*
                Power(t2,2) + 
               (44 - 8*Power(s,2) - 8*s2 + 4*s*(4 + s2))*Power(t2,3)) + 
            Power(t1,4)*(-2 + (-2 + 5*s - 4*s2)*t2 + 
               (43 - 16*Power(s,2) + s2 - 16*Power(s2,2) + 
                  s*(2 + 4*s2))*Power(t2,2) + 
               (5 + 32*Power(s,2) - 63*s2 + 10*Power(s2,2) - 
                  s*(59 + 44*s2))*Power(t2,3) + 
               2*(-50 + 6*Power(s,2) + 13*s2 + Power(s2,2) - 
                  2*s*(8 + 3*s2))*Power(t2,4)) + 
            Power(t1,2)*t2*(8 + 
               (57 + 4*s - 50*s2 + 12*Power(s2,2))*t2 + 
               (241 - 63*s2 - 6*Power(s2,2) + s*(17 + 100*s2))*
                Power(t2,2) + 
               (-80*Power(s,2) + 4*s*(33 + 13*s2) + 
                  3*(75 + 66*s2 - 4*Power(s2,2)))*Power(t2,3) + 
               (163 + 14*Power(s,2) + 15*s2 + 44*Power(s2,2) - 
                  s*(59 + 68*s2))*Power(t2,4) + 
               2*(-23 + Power(s,2) + 13*s2 + Power(s2,2) - 
                  2*s*(7 + s2))*Power(t2,5)) + 
            2*Power(t1,6)*t2*
             (-2 - 3*t2 + Power(s,2)*(1 + t2) - s*(5 + 2*t2)) + 
            4*Power(t2,3)*(-5 + (16 + 5*s)*t2 + (34 + s)*Power(t2,2) + 
               (6 + 7*s - 4*Power(s,2))*Power(t2,3) + 
               (9 - 5*s)*Power(t2,4) + 
               2*Power(s2,2)*
                (-6 - 9*t2 + Power(t2,2) + 2*Power(t2,3)) + 
               s2*(26 + (25 + 4*s)*t2 + (3 + 14*s)*Power(t2,2) + 
                  (27 + 2*s)*Power(t2,3) + 7*Power(t2,4))) - 
            Power(t1,3)*t2*(-5 + (23 + 12*s)*t2 + 
               (127 + 65*s - 52*Power(s,2))*Power(t2,2) + 
               (73 - 51*s + 32*Power(s,2))*Power(t2,3) + 
               4*(-25 - 10*s + 2*Power(s,2))*Power(t2,4) + 
               4*Power(s2,2)*t2*
                (-1 - 7*t2 + 9*Power(t2,2) + Power(t2,3)) + 
               s2*(4 + (-3 + 20*s)*t2 + (45 + 28*s)*Power(t2,2) - 
                  6*(11 + 14*s)*Power(t2,3) - 12*(-3 + s)*Power(t2,4))) \
- 2*t1*Power(t2,2)*(18 + 4*(20 + 3*s)*t2 + (171 + 7*s)*Power(t2,2) + 
               (78 + 51*s - 29*Power(s,2))*Power(t2,3) + 
               (65 - 28*s + Power(s,2))*Power(t2,4) - 
               4*(1 + s)*Power(t2,5) + 
               Power(s2,2)*t2*
                (-24 - 12*t2 + 11*Power(t2,2) + 9*Power(t2,3)) + 
               s2*(-8 + (2 + 8*s)*t2 + (7 + 68*s)*Power(t2,2) + 
                  3*(43 + 6*s)*Power(t2,3) + (32 - 10*s)*Power(t2,4) + 
                  4*Power(t2,5)))) - 
         Power(s1,2)*(2*(8 + s)*Power(t1,7)*t2 + 
            Power(t1,6)*(-2 - (26 + 17*s + 6*s2)*t2 + 
               (-32 + 9*s - 18*s2)*Power(t2,2) + 4*Power(t2,3)) + 
            Power(t1,5)*(8 + (-6 + 8*s + 21*s2)*t2 + 
               (-24 + 13*s - 4*Power(s,2) + 28*s2 + 4*s*s2 + 
                  8*Power(s2,2))*Power(t2,2) + 
               (72 - 43*s + 55*s2)*Power(t2,3) - 12*Power(t2,4)) + 
            Power(t1,4)*(-4 + (-22 + 5*s2)*t2 + 
               (-84 + 2*Power(s,2) + 106*s2 - 28*Power(s2,2) + 
                  s*(-33 + 8*s2))*Power(t2,2) + 
               (-76 + 10*Power(s,2) + s*(4 - 20*s2) - 47*s2)*
                Power(t2,3) + (-172 + 47*s - 62*s2)*Power(t2,4) + 
               12*Power(t2,5)) + 
            Power(t1,2)*t2*(2 + 
               (s + 4*(26 - 25*s2 + 6*Power(s2,2)))*t2 + 
               (2*s*(29 + 6*s2) + 5*(78 - 51*s2 + 2*Power(s2,2)))*
                Power(t2,2) + 
               (185 - 24*Power(s,2) + 47*s2 - 22*Power(s2,2) + 
                  2*s*(-67 + 20*s2))*Power(t2,3) + 
               (-203 + 4*Power(s,2) + 138*s2 + 32*Power(s2,2) - 
                  s*(113 + 40*s2))*Power(t2,4) - 
               2*(85 + 10*s - 8*s2)*Power(t2,5)) + 
            Power(t1,3)*t2*(-6 + (60 + 11*s)*t2 + 
               (187 + 115*s + 4*Power(s,2))*Power(t2,2) + 
               (215 + 63*s - 8*Power(s,2))*Power(t2,3) - 
               3*(-82 + s)*Power(t2,4) - 4*Power(t2,5) - 
               4*Power(s2,2)*t2*(9 - 3*t2 + 5*Power(t2,2)) + 
               s2*(-8 + (41 + 4*s)*t2 - (71 + 32*s)*Power(t2,2) + 
                  (-29 + 40*s)*Power(t2,3) + 17*Power(t2,4))) + 
            Power(t2,3)*(-5 + 5*(-9 + s)*t2 + (57 + 40*s)*Power(t2,2) + 
               (179 + 33*s - 10*Power(s,2))*Power(t2,3) + 
               2*(29 - 4*s + Power(s,2))*Power(t2,4) + 
               2*Power(s2,2)*
                (-6 + 3*t2 + 18*Power(t2,2) + 4*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(26 + (9 + 4*s)*t2 + (74 + 28*s)*Power(t2,2) + 
                  (35 - 4*s)*Power(t2,3) + (10 - 4*s)*Power(t2,4))) - 
            t1*Power(t2,2)*(9 + (73 + 6*s)*t2 + 
               (349 + 109*s)*Power(t2,2) + 
               (333 - 11*s - 28*Power(s,2))*Power(t2,3) + 
               (-26 - 58*s + 4*Power(s,2))*Power(t2,4) - 
               8*(5 + s)*Power(t2,5) + 
               4*Power(s2,2)*t2*
                (-30 - 20*t2 + 9*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-4 + (219 + 4*s)*t2 + (69 + 44*s)*Power(t2,2) + 
                  6*(1 + 2*s)*Power(t2,3) + (106 - 20*s)*Power(t2,4) + 
                  8*Power(t2,5)))) + 
         s1*(2*Power(t1,7)*t2*(3 + 3*s + 4*t2) - 
            Power(t1,6)*(2 + (-7 + 5*s + 8*s2)*t2 + 
               (-1 - 2*Power(s,2) + 22*s2 + s*(15 + 4*s2))*Power(t2,2) + 
               4*(12 + s - s2)*Power(t2,3)) + 
            Power(t1,5)*(2 - (2 + s - 4*s2)*t2 - 
               (63 + 3*s2 - 16*Power(s2,2) + s*(11 + 4*s2))*
                Power(t2,2) + 
               (-21 - 4*Power(s,2) + 69*s2 - 8*Power(s2,2) + 
                  20*s*(1 + s2))*Power(t2,3) + 
               2*(53 + 10*s - 9*s2)*Power(t2,4)) + 
            Power(t1,3)*t2*(-12 - 
               (91 + 8*s - 51*s2 + 12*Power(s2,2))*t2 + 
               (-372 + 16*Power(s,2) + 118*s2 - 24*Power(s2,2) - 
                  17*s*(1 + 4*s2))*Power(t2,2) + 
               (-354 + 8*Power(s,2) - 290*s2 + 18*Power(s2,2) - 
                  s*(47 + 36*s2))*Power(t2,3) + 
               (-289 + 4*Power(s,2) - 61*s2 - 26*Power(s2,2) + 
                  s*(94 + 28*s2))*Power(t2,4) + 
               (58 + 28*s - 26*s2)*Power(t2,5)) - 
            2*t1*Power(t2,3)*(-43 + 9*(-7 + 2*s)*t2 + 
               (92 + 50*s - 16*Power(s,2))*Power(t2,2) + 
               2*(40 - s + 6*Power(s,2))*Power(t2,3) + 68*Power(t2,4) - 
               2*Power(s2,2)*
                (18 + 17*t2 - 20*Power(t2,2) + Power(t2,3)) + 
               2*s2*(46 + (76 + 6*s)*t2 - 5*(1 + 2*s)*Power(t2,2) + 
                  (31 - 2*s)*Power(t2,3) + 3*Power(t2,4))) - 
            2*Power(t2,4)*(8 + (71 + 7*s)*t2 + 
               (2 - 21*s + 5*Power(s,2))*Power(t2,2) - 
               (37 + 5*Power(s,2))*Power(t2,3) - 4*(3 + s)*Power(t2,4) + 
               Power(s2,2)*(-36 - 40*t2 - 3*Power(t2,2) + 
                  3*Power(t2,3)) + 
               s2*(70 - 5*t2 + (-7 + 18*s)*Power(t2,2) + 
                  2*(8 + s)*Power(t2,3) + 4*Power(t2,4))) + 
            Power(t1,4)*t2*(5 + (29 - 4*s - 2*Power(s,2))*t2 + 
               (164 + 52*s - 6*Power(s,2))*Power(t2,2) - 
               6*(-22 + 9*s)*Power(t2,3) - 4*(28 + 9*s)*Power(t2,4) + 
               2*Power(s2,2)*t2*(-2 - 11*t2 + 12*Power(t2,2)) + 
               s2*(4 + 2*(11 + 8*s)*t2 + (59 + 24*s)*Power(t2,2) - 
                  (47 + 36*s)*Power(t2,3) + 32*Power(t2,4))) + 
            Power(t1,2)*Power(t2,2)*
             (38 + (226 + 58*s)*t2 + 
               (553 + 80*s - 36*Power(s,2))*Power(t2,2) + 
               (320 + 7*s + 12*Power(s,2))*Power(t2,3) + 
               (283 - 59*s - 2*Power(s,2))*Power(t2,4) - 
               4*(3 + 2*s)*Power(t2,5) + 
               2*Power(s2,2)*t2*
                (-41 + 12*t2 + Power(t2,2) + 5*Power(t2,3)) + 
               s2*(-24 + (83 + 24*s)*t2 + 2*(-5 + 24*s)*Power(t2,2) + 
                  2*(187 + 6*s)*Power(t2,3) + (81 - 8*s)*Power(t2,4) + 
                  8*Power(t2,5)))))*R1(t2))/
     ((-1 + s1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*(-s + s1 - t2)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*(-1 + t2)*t2*Power(-t1 + t2,3)) - 
    (8*(6*Power(s1,6)*t1 + 2*t1*
          (1 - 2*(9 + Power(s,2) - 3*s2 - 2*Power(s2,2) + 
               s*(-1 + 3*s2))*t2 + 
            (-21 + 17*Power(s,2) - 17*s2 + 3*Power(s2,2) - 
               s*(37 + 14*s2))*Power(t2,2) + 
            (-30 + Power(s,2) - 3*s2 - 7*Power(s2,2) + s*(3 + 6*s2))*
             Power(t2,3) + 4*(1 + s - s2)*Power(t2,4)) + 
         2*Power(t1,4)*(1 - 2*t2 - 2*s*(1 + t2) + Power(s,2)*(1 + t2)) + 
         Power(s1,5)*(-2*Power(s2,2) + 7*s2*t1 + 19*Power(t1,2) - 6*t2 - 
            2*t1*(-4 + 3*s + 9*t2)) - 
         4*t2*(-8 + 3*t2 + Power(s2,2)*(3 - 2*t2)*t2 + 
            7*Power(s,2)*(-1 + t2)*t2 - 30*Power(t2,2) - 
            13*Power(t2,3) + s*
             (1 - 2*(13 + 4*s2)*t2 - (24 + 7*s2)*Power(t2,2) + 
               5*Power(t2,3)) - 
            s2*(-5 + 21*t2 + 13*Power(t2,2) + 7*Power(t2,3))) + 
         Power(t1,2)*(-2 + (28 - 21*s2 - 6*Power(s2,2))*t2 + 
            (-12 - 17*s2 + 5*Power(s2,2))*Power(t2,2) + 
            2*(-15 + 3*s2 + Power(s2,2))*Power(t2,3) + 
            2*Power(s,2)*t2*(-9 + 4*t2 + Power(t2,2)) - 
            s*(-4 + (26 - 4*s2)*t2 + (18 + 23*s2)*Power(t2,2) + 
               4*(2 + s2)*Power(t2,3))) + 
         Power(t1,3)*(-2 + (-4 + 11*s2)*t2 + (26 - 4*s2)*Power(t2,2) - 
            2*Power(s,2)*(-1 + 5*t2 + 2*Power(t2,2)) + 
            s*t2*(26 + 6*t2 + s2*(7 + 4*t2))) + 
         Power(s1,4)*(20*Power(t1,3) + Power(t1,2)*(25 - 21*s - 40*t2) + 
            Power(s2,2)*(6 - 7*t1 + 2*t2) + 2*t2*(-4 + 3*s + 9*t2) + 
            t1*(-59 - 16*s - 34*t2 + 15*s*t2 + 12*Power(t2,2)) + 
            s2*(4 + 23*Power(t1,2) - 7*t2 - 4*s*t2 - t1*(10 + 19*t2))) + 
         Power(s1,3)*(-2 + 7*Power(t1,4) + 
            Power(t1,3)*(32 - 24*s - 26*t2) + 59*t2 + 20*s*t2 + 
            15*Power(t2,2) - 15*s*Power(t2,2) - 
            2*Power(s,2)*Power(t2,2) - 12*Power(t2,3) + 
            Power(s2,2)*(-2 - 8*Power(t1,2) + t2 + 2*Power(t2,2) + 
               t1*(18 + t2)) + 
            2*Power(t1,2)*(-57 + Power(s,2) - 40*t2 + 8*Power(t2,2) + 
               7*s*(-3 + 2*t2)) + 
            t1*(28 - 2*Power(s,2)*(-1 + t2) - 7*t2 + 74*Power(t2,2) + 
               s*(52 + 21*t2 - 4*Power(t2,2))) + 
            s2*(-12 + 25*Power(t1,3) + 6*t2 + 16*s*t2 + 19*Power(t2,2) - 
               Power(t1,2)*(35 + 3*s + 38*t2) - 
               t1*(-3 + 8*t2 - 6*Power(t2,2) + s*(4 + t2)))) + 
         s1*(6*Power(t1,5) + 2*
             (-1 + (-18 - 2*s*(-10 + s2) + 8*s2 + 6*Power(s2,2))*t2 - 
               (39 + 11*Power(s,2) + 33*s2 - 9*Power(s2,2) + 
                  s*(35 + 16*s2))*Power(t2,2) + 
               (-42 + 7*Power(s,2) - 9*s2 - Power(s2,2) - 
                  3*s*(1 + 2*s2))*Power(t2,3) + 
               4*(3 + s - s2)*Power(t2,4)) - 
            2*t1*(18 + (-5 + 91*s + 12*Power(s,2))*t2 + 
               (85 + 51*s - 4*Power(s,2))*Power(t2,2) - 
               8*(-7 + s)*Power(t2,3) + Power(s2,2)*t2*(4 + t2) + 
               s2*(-8 + 4*(16 + s)*t2 + (52 + 9*s)*Power(t2,2) + 
                  14*Power(t2,3))) + 
            Power(t1,4)*(-4 + 2*Power(s,2) - 15*s2 - 26*t2 - 
               s*(3*s2 + 2*(5 + t2))) + 
            Power(t1,3)*(-26 - 3*Power(s2,2)*(-2 + t2) - 9*t2 + 
               34*Power(t2,2) + 2*Power(s,2)*(3 + t2) + 
               s2*(25 + 24*t2 - 6*Power(t2,2)) + 
               s*(34 + 15*t2 + 8*Power(t2,2) + s2*(-4 + 7*t2))) + 
            Power(t1,2)*(46 + 107*t2 + 127*Power(t2,2) - 
               12*Power(t2,3) - 
               2*Power(s,2)*(-1 + 7*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(-8 - 5*t2 + 6*Power(t2,2)) + 
               s2*(-8 + 70*t2 + 25*Power(t2,2) + 8*Power(t2,3)) + 
               s*(-20 + 90*t2 - 17*Power(t2,2) - 8*Power(t2,3) + 
                  s2*(8 + 26*t2)))) + 
         Power(s1,2)*(Power(t1,4)*(21 - 9*s - 4*t2) - 
            2*(-3 + (13 + 34*s + Power(s,2))*t2 + 
               (9 - 6*Power(s,2))*Power(t2,2) + 
               (17 - 2*s + Power(s,2))*Power(t2,3)) + 
            Power(t1,3)*(-59 + 4*Power(s,2) - 74*t2 + 4*Power(t2,2) + 
               s*(-36 + 11*t2)) - 
            Power(s2,2)*(3*Power(t1,3) + 2*Power(t1,2)*(-9 + 2*t2) + 
               t1*(10 - 8*Power(t2,2)) + t2*(22 + 7*t2 + 2*Power(t2,2))) \
+ Power(t1,2)*(-4 - 2*Power(s,2)*(-3 + t2) + 16*t2 + 86*Power(t2,2) + 
               s*(90 + 46*t2 + 4*Power(t2,2))) - 
            2*t1*(-15 - 97*t2 + 2*Power(s,2)*(-1 + t2)*t2 - 
               72*Power(t2,2) + 20*Power(t2,3) + 
               s*(12 - 56*t2 + 13*Power(t2,2) + 4*Power(t2,3))) + 
            s2*(4 + 9*Power(t1,4) + (9 - 12*s)*t2 + 
               (-19 + s)*Power(t2,2) + (-6 + 4*s)*Power(t2,3) - 
               Power(t1,3)*(40 + 6*s + 19*t2) + 
               Power(t1,2)*(24 - 8*s + 15*t2 + 10*s*t2) + 
               t1*(-8 + 113*t2 + 60*Power(t2,2) + 8*Power(t2,3) + 
                  s*(8 + 23*t2 - 4*Power(t2,2))))))*R2(1 - s1 - t1 + t2))/
     ((-1 + s1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*(-s + s1 - t2)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*(-1 + t2)) + 
    (8*(4*Power(s1,8)*Power(t1,2) + 
         Power(-1 + s,2)*Power(t1,7)*(1 + t2) + 
         2*t1*t2*(-3 + (24 + 2*Power(s,2) - 5*s2 - 4*Power(s2,2) + 
               s*(7 + 10*s2))*t2 - 
            (-90 + 48*Power(s,2) + 13*s2 + 4*Power(s2,2) + 
               s*(31 + 4*s2))*Power(t2,2) + 
            (-24 + 30*Power(s,2) - s2 + 16*Power(s2,2) - 
               s*(109 + 54*s2))*Power(t2,3) + 
            (-39 + s + 2*Power(s,2) - 9*s2 - 4*s*s2 + 2*Power(s2,2))*
             Power(t2,4)) + Power(t1,3)*
          (1 - 2*(-2 + 3*Power(s,2) - s2 + s*(7 + s2))*t2 + 
            (-99 + 79*Power(s,2) + 12*s2 + 5*Power(s2,2) - 
               4*s*(5 + 3*s2))*Power(t2,2) - 
            4*(14 + 5*Power(s,2) + 7*s2 + 3*Power(s2,2) - 
               2*s*(15 + 7*s2))*Power(t2,3) + 
            (26 - 6*Power(s,2) - 6*Power(s2,2) + 4*s*(4 + 3*s2))*
             Power(t2,4)) + Power(t1,5)*
          (-2 + (9 - 2*s2)*t2 + (10 + 9*s2 + Power(s2,2))*Power(t2,2) + 
            Power(s2,2)*Power(t2,3) + 
            Power(s,2)*(1 - 17*t2 - 3*Power(t2,2) + Power(t2,3)) + 
            s*(2 + (17 + 2*s2)*t2 - (9 + 7*s2)*Power(t2,2) - 
               (3 + 2*s2)*Power(t2,3))) - 
         4*Power(t2,2)*(7 + 15*t2 + 25*Power(t2,2) + 5*Power(t2,3) - 
            4*Power(t2,4) + Power(s,2)*t2*(1 - 14*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*t2*(-9 + 4*t2 + 3*Power(t2,2)) + 
            s2*(-5 + 18*t2 + 7*Power(t2,2) + 8*Power(t2,3)) + 
            s*(-1 + 4*(-1 + 4*s2)*t2 + (7 + 2*s2)*Power(t2,2) - 
               2*(11 + 3*s2)*Power(t2,3))) + 
         Power(t1,4)*(Power(s,2)*t2*(-16 + 33*t2 + 11*Power(t2,2)) + 
            t2*(-1 - s2*(-2 + t2)*Power(-1 + t2,2) + 21*t2 - 
               2*Power(s2,2)*(-1 + t2)*t2 - 27*Power(t2,2) + Power(t2,3)\
) + s*(2 + (3 - 2*s2)*t2 - (83 + 25*s2)*Power(t2,2) - 
               9*(1 + s2)*Power(t2,3) + Power(t2,4))) + 
         Power(t1,6)*(Power(s,2)*(2 - 3*t2 - 2*Power(t2,2)) + 
            2*s*(-1 + (6 + s2)*t2 + (2 + s2)*Power(t2,2)) - 
            t2*(7 + 2*s2*(1 + t2))) - 
         Power(s1,7)*(Power(s2,2)*(1 + t1) - s2*t1*(2 + s + 4*t1 - t2) + 
            t1*(-17*Power(t1,2) + 8*t2 + t1*(-1 + 5*s + 11*t2))) + 
         Power(s1,6)*(28*Power(t1,4) + Power(t1,3)*(5 - 21*s - 37*t2) + 
            4*Power(t2,2) + Power(s2,2)*
             (3 - 4*Power(t1,2) + t1*(-2 + t2) + t2) + 
            t1*(-2 + s - t2 + 10*s*t2 + Power(s,2)*t2 + 
               22*Power(t2,2) - s*Power(t2,2)) + 
            Power(t1,2)*(-34 + Power(s,2) - 48*t2 + 7*Power(t2,2) + 
               2*s*(-5 + 6*t2)) + 
            s2*(2 + 16*Power(t1,3) + Power(t1,2)*(7 + 3*s - 14*t2) - 
               2*t2 - 3*s*t2 + Power(t2,2) - 
               t1*(3 + s + 6*t2 + 2*s*t2 - 2*Power(t2,2)))) - 
         2*Power(t1,2)*t2*(Power(s,2)*t2*(-17 + 52*t2 + 2*Power(t2,2)) + 
            s*(6 - 3*(5 + 3*s2)*t2 - (71 + 43*s2)*Power(t2,2) + 
               (26 + 6*s2)*Power(t2,3) + 4*Power(t2,4)) - 
            t2*(4 + 40*t2 + 54*Power(t2,2) - 6*Power(t2,3) + 
               Power(s2,2)*(-2 - 7*t2 + 8*Power(t2,2)) + 
               s2*(2 + 29*t2 + 11*Power(t2,2) + 4*Power(t2,3)))) - 
         Power(s1,4)*(-3 - 8*Power(t1,6) + 4*t2 + s*t2 + 
            32*Power(t2,2) + 15*s*Power(t2,2) - 
            6*Power(s,2)*Power(t2,2) + 12*Power(t2,3) - 
            11*s*Power(t2,3) + 2*Power(s,2)*Power(t2,3) - 
            7*Power(t2,4) + s*Power(t2,4) + 
            Power(t1,5)*(-25 + 26*s + 26*t2) + 
            Power(t1,4)*(91 - 6*Power(s,2) + s*(60 - 28*t2) + 135*t2 - 
               16*Power(t2,2)) + 
            Power(t1,3)*(73 - 65*t2 - 178*Power(t2,2) + 
               Power(s,2)*(-14 + 5*t2) - 10*s*(10 + 13*t2)) + 
            t1*(12 - 61*t2 + 111*Power(t2,2) + 106*Power(t2,3) + 
               Power(s,2)*t2*(13 + 2*t2 + Power(t2,2)) + 
               s*(-6 + 79*t2 + 59*Power(t2,2) - 7*Power(t2,3))) + 
            Power(s2,2)*(4*Power(t1,4) + Power(t1,3)*(-17 + 6*t2) + 
               t2*(16 + 11*t2 + Power(t2,2)) - 
               Power(t1,2)*(3 + 15*t2 + 4*Power(t2,2)) + 
               t1*(3 + 14*t2 - 2*Power(t2,2) + Power(t2,3))) + 
            Power(t1,2)*(-63 - 276*t2 - 138*Power(t2,2) + 
               56*Power(t2,3) + Power(s,2)*(3 + 10*t2) + 
               2*s*(4 - 44*t2 + 44*Power(t2,2) + Power(t2,3))) - 
            s2*(2 + 16*Power(t1,5) - 2*(9 + 5*s)*t2 + 
               (17 + 23*s)*Power(t2,2) + (-10 + 3*s)*Power(t2,3) + 
               Power(t2,4) - 2*Power(t1,4)*(5 + s + 14*t2) + 
               t1*(-16 + 48*(1 + s)*t2 - 5*(-5 + s)*Power(t2,2) + 
                  2*(-9 + s)*Power(t2,3)) + 
               2*Power(t1,3)*
                (-8 - 23*t2 + 3*Power(t2,2) + 4*s*(-3 + 2*t2)) + 
               2*Power(t1,2)*(4 + 4*t2 + 40*Power(t2,2) + Power(t2,3) + 
                  s*(4 - 7*t2 - 2*Power(t2,2))))) + 
         Power(s1,5)*(-1 + 22*Power(t1,5) + 
            Power(t1,4)*(15 - 34*s - 46*t2) + 2*t2 + s*t2 - 
            5*s*Power(t2,2) - 2*Power(s,2)*Power(t2,2) - 
            11*Power(t2,3) + s*Power(t2,3) + 
            Power(t1,3)*(-97 + 4*Power(s,2) - 111*t2 + 18*Power(t2,2) + 
               s*(-38 + 34*t2)) + 
            Power(t1,2)*(-24 + 4*Power(s,2) + 50*t2 + 109*Power(t2,2) + 
               s*(37 + 62*t2 - 5*Power(t2,2))) + 
            Power(s2,2)*(-1 + 5*Power(t1,2) - 6*Power(t1,3) + 4*t2 + 
               Power(t2,2) + t1*(7 + 9*t2 + Power(t2,2))) + 
            t1*(4 + 66*t2 - 2*Power(s,2)*t2 + 43*Power(t2,2) - 
               14*Power(t2,3) + 
               s*(-7 + 27*t2 - 23*Power(t2,2) + Power(t2,3))) + 
            s2*(-6 + 24*Power(t1,4) + 2*Power(t1,3)*(2 + s - 17*t2) + 
               3*t2 + 9*s*t2 + 2*Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,2)*(-12 + s*(-10 + t2) - 31*t2 + 
                  8*Power(t2,2)) - 
               t1*(-14 + 28*t2 - 24*Power(t2,2) + Power(t2,3) + 
                  s*(-2 + 14*t2 + Power(t2,2))))) - 
         s1*(2*t2*(-3 + (s*(23 + 2*s2) + 3*(4 + s2 + 2*Power(s2,2)))*
                t2 + (-4 - 4*Power(s,2) + s*(31 - 52*s2) + s2 + 
                  16*Power(s2,2))*Power(t2,2) + 
               (32 + 20*Power(s,2) + s*(7 - 14*s2) - 9*s2 + 
                  2*Power(s2,2))*Power(t2,3) + 
               (19 - 2*Power(s,2) + s2 - 2*Power(s2,2) + s*(7 + 4*s2))*
                Power(t2,4)) + 
            Power(t1,2)*(-3 + 
               2*(45 + Power(s,2) - 7*s2 - 8*Power(s2,2) + 
                  s*(-19 + 11*s2))*t2 - 
               (-429 + 95*Power(s,2) + 100*s2 + 45*Power(s2,2) - 
                  26*s*(-9 + 2*s2))*Power(t2,2) + 
               2*(-60 + 16*Power(s,2) - 3*s2 + 22*Power(s2,2) - 
                  2*s*(102 + 29*s2))*Power(t2,3) + 
               2*(-118 + 4*Power(s,2) - 11*s2 + 4*Power(s2,2) - 
                  s*(9 + 8*s2))*Power(t2,4)) + 
            2*Power(t1,7)*(-2 + s*(3 + t2)) - 
            Power(t1,6)*(-14 + s2*(2 - 3*t2) - 18*t2 + Power(t2,2) + 
               Power(s,2)*(5 + 4*t2) + 
               s*(-1 + (10 + s2)*t2 + 3*Power(t2,2))) + 
            Power(t1,5)*(-7 + 21*t2 - 51*Power(t2,2) + 2*Power(t2,3) + 
               Power(s2,2)*t2*(2 + t2) + 
               Power(s,2)*(-5 + 11*t2 + 8*Power(t2,2)) - 
               s2*(2 + 20*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               s*(3 - (81 + 10*s2)*t2 - (13 + 9*s2)*Power(t2,2) + 
                  Power(t2,3))) + 
            2*t1*t2*(-37 - 95*t2 - 137*Power(t2,2) - 71*Power(t2,3) + 
               32*Power(t2,4) + 
               Power(s2,2)*t2*(33 - 2*t2 - 5*Power(t2,2)) + 
               Power(s,2)*t2*(11 + 4*t2 + 3*Power(t2,2)) - 
               2*s2*(-7 + 40*t2 + 25*Power(t2,2) + 14*Power(t2,3) + 
                  2*Power(t2,4)) + 
               2*s*(-1 - 9*(-3 + 2*s2)*t2 + (-11 + 3*s2)*Power(t2,2) + 
                  (37 + s2)*Power(t2,3) + 2*Power(t2,4))) + 
            Power(t1,3)*(3 + 45*t2 + 161*Power(t2,2) + 
               279*Power(t2,3) - 30*Power(t2,4) + 
               Power(s,2)*t2*(14 - 63*t2 - 19*Power(t2,2)) + 
               Power(s2,2)*t2*(-6 - 25*t2 + 7*Power(t2,2)) + 
               2*s*(-3 + 4*(1 + s2)*t2 + 4*(26 + 9*s2)*Power(t2,2) + 
                  6*(-5 + s2)*Power(t2,3) - 7*Power(t2,4)) + 
               2*s2*(1 + 16*t2 + 67*Power(t2,2) + 13*Power(t2,3) + 
                  7*Power(t2,4))) + 
            Power(t1,4)*(-3 - 174*t2 - 109*Power(t2,2) + 
               61*Power(t2,3) - 
               2*Power(s2,2)*t2*(-6 + 5*t2 + 2*Power(t2,2)) + 
               s2*(2 + 27*t2 - 14*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s,2)*(-3 + 54*t2 + Power(t2,2) - 4*Power(t2,3)) + 
               s*t2*(43 + 159*t2 + 29*Power(t2,2) + 
                  s2*(-19 + 38*t2 + 8*Power(t2,2))))) + 
         Power(s1,3)*(-1 + Power(t1,7) + 
            Power(t1,6)*(20 - 9*s + 4*s2 - 7*t2) + 
            2*(7 + s + 17*s2 + s*s2 + 3*Power(s2,2))*t2 + 
            (-35 + Power(s,2) + s*(30 - 68*s2) - 24*s2 + 
               11*Power(s2,2))*Power(t2,2) + 
            2*(28 + 8*s + 7*Power(s,2) - 5*s2 - 3*Power(s2,2))*
             Power(t2,3) - 2*(-17 + s - 5*s2)*Power(t2,4) + 
            Power(t1,5)*(-23 + 4*Power(s,2) - Power(s2,2) - 
               s*(52 + 3*s2) - 89*t2 + 6*Power(t2,2) - s2*(14 + 5*t2)) - 
            t1*(-9 + 151*t2 + 273*Power(t2,2) + 53*Power(t2,3) - 
               58*Power(t2,4) + Power(s2,2)*Power(t2,2)*(25 + 7*t2) + 
               Power(s,2)*t2*(-12 - 9*t2 + Power(t2,2)) + 
               2*s2*(-3 + 20*t2 + 15*Power(t2,2) + 23*Power(t2,3) + 
                  Power(t2,4)) - 
               2*s*(-1 + (8 - 22*s2)*t2 + 6*(-5 + 4*s2)*Power(t2,2) + 
                  (42 + 4*s2)*Power(t2,3) + Power(t2,4))) + 
            Power(t1,3)*(67 + 303*t2 + 312*Power(t2,2) - 
               64*Power(t2,3) + 
               Power(s2,2)*(-3 + 5*t2 + 4*Power(t2,2)) - 
               Power(s,2)*(5 + 19*t2 + 4*Power(t2,2)) + 
               s2*(8 + 88*t2 + 66*Power(t2,2) + 8*Power(t2,3)) + 
               2*s*(-1 + 78*t2 - 41*Power(t2,2) - 4*Power(t2,3) + 
                  s2*(5 + 4*t2))) + 
            Power(t1,4)*(Power(s,2)*(19 - 4*t2) + 
               2*s*(48 + 11*s2*(-1 + t2) + 64*t2 + 7*Power(t2,2)) - 
               2*(52 + 4*Power(s2,2)*(-2 + t2) + 20*t2 - 
                  66*Power(t2,2) + s2*(3 + 13*t2 + 2*Power(t2,2)))) + 
            Power(t1,2)*(2 + 158*t2 - 249*Power(t2,2) - 
               258*Power(t2,3) + 
               Power(s,2)*(-1 - 46*t2 + 13*Power(t2,2)) + 
               Power(s2,2)*(-9 - 52*t2 + 19*Power(t2,2)) + 
               s2*(6 + 46*t2 - 10*Power(t2,2) - 20*Power(t2,3)) - 
               2*s*(9 + 146*t2 + 99*Power(t2,2) + Power(t2,3) + 
                  s2*(-4 - 42*t2 + 25*Power(t2,2))))) + 
         Power(s1,2)*(-(Power(t1,7)*(-6 + s + t2)) + 
            t1*(-3 + 2*(12 + s*(19 - 5*s2) - 9*s2 + 9*Power(s2,2))*t2 + 
               (-97 + 57*Power(s,2) - 64*s2 + 59*Power(s2,2) - 
                  4*s*(-67 + 31*s2))*Power(t2,2) - 
               2*(-116 + Power(s,2) - 4*s2 + 5*Power(s2,2) - 
                  2*s*(32 + 9*s2))*Power(t2,3) - 
               2*(-82 + Power(s,2) - 8*s2 + Power(s2,2) - 2*s*(4 + s2))*
                Power(t2,4)) + 
            Power(t1,6)*(9 + Power(s,2) - 25*t2 + Power(t2,2) + 
               s2*(-5 + 2*t2) - s*(26 + s2 + 8*t2)) + 
            2*t2*(-9 + (41 + 11*s - 2*Power(s,2))*t2 + 
               (47 + 7*s - 16*Power(s,2))*Power(t2,2) + 
               (-1 - 16*s + 5*Power(s,2))*Power(t2,3) - 10*Power(t2,4) + 
               Power(s2,2)*t2*(3 + 17*t2 + 3*Power(t2,2)) + 
               s2*(-6 + (14 + 31*s)*t2 + (13 - 25*s)*Power(t2,2) + 
                  (1 - 8*s)*Power(t2,3))) + 
            Power(t1,2)*(-37 - 179*t2 - 383*Power(t2,2) - 
               321*Power(t2,3) + 96*Power(t2,4) + 
               Power(s2,2)*t2*(30 - 7*t2 - 11*Power(t2,2)) + 
               Power(s,2)*t2*(14 + 25*t2 + 9*Power(t2,2)) - 
               2*s2*(-7 + 40*t2 + 74*Power(t2,2) + 33*Power(t2,3) + 
                  8*Power(t2,4)) + 
               2*s*(-1 - 3*(-9 + 8*s2)*t2 + (-71 + s2)*Power(t2,2) + 
                  (59 + s2)*Power(t2,3) + 8*Power(t2,4))) + 
            Power(t1,5)*(-68 + Power(s2,2)*(5 - 3*t2) - 73*t2 + 
               42*Power(t2,2) + Power(s,2)*(13 + 3*t2) + 
               s2*(3 - 8*t2 - 4*Power(t2,2)) + 
               s*(33 + 62*t2 + 13*Power(t2,2) + s2*(-7 + 10*t2))) + 
            Power(t1,4)*(19 + 95*t2 + 252*Power(t2,2) - 24*Power(t2,3) - 
               2*Power(s2,2)*(1 + 2*t2) + 
               Power(s,2)*(1 - 19*t2 - 10*Power(t2,2)) + 
               s2*(14 + 76*t2 + 9*Power(t2,2) + 6*Power(t2,3)) + 
               s*(s2*(4 + 19*t2 + 10*Power(t2,2)) - 
                  2*(1 - 82*t2 + 4*Power(t2,2) + 3*Power(t2,3)))) + 
            Power(t1,3)*(34 + 352*t2 - 55*Power(t2,2) - 264*Power(t2,3) + 
               s2*(2 - 32*t2 + 24*Power(t2,2)) + 
               Power(s,2)*(1 - 70*t2 + 15*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(-7 - 46*t2 + 27*Power(t2,2) + 
                  4*Power(t2,3)) - 
               2*s*(11 + 128*t2 + 157*Power(t2,2) + 17*Power(t2,3) + 
                  s2*(-4 - 31*t2 + 38*Power(t2,2) + 4*Power(t2,3))))))*
       T2(t2,1 - s1 - t1 + t2))/
     ((-1 + s1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*(-s + s1 - t2)*
       (-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)) + 
    (8*(-(Power(-1 + s,2)*Power(t1,7)*(1 + t2)) + 
         4*Power(s1,6)*Power(t1,2)*(t1 - t2)*(1 + t2) - 
         Power(t1,3)*(1 - 2*(4 - s2 + s*(4 + s2))*t2 + 
            (-32 + 22*Power(s,2) + 4*s2 + Power(s2,2) + s*(33 + 10*s2))*
             Power(t2,2) + (120 - 80*Power(s,2) + 13*s2 - 
               10*Power(s2,2) + s*(64 + 49*s2))*Power(t2,3) + 
            (72 + 15*Power(s,2) + 55*s2 + 9*Power(s2,2) - 
               4*s*(29 + 8*s2))*Power(t2,4) + 
            (-3 + 5*Power(s,2) + 5*s2 + 3*Power(s2,2) - 8*s*(2 + s2))*
             Power(t2,5)) + Power(t1,2)*t2*
          (3 + 2*(-2 + Power(s,2) - 13*s2 + 4*Power(s2,2) - 
               s*(1 + 6*s2))*t2 + 
            (-38 + 32*Power(s,2) - 2*s2 + Power(s2,2) + 5*s*(7 + 4*s2))*
             Power(t2,2) + (104 - 68*Power(s,2) + 17*s2 - 
               18*Power(s2,2) + 13*s*(6 + 5*s2))*Power(t2,3) + 
            (45 + 6*Power(s,2) + 39*s2 + 7*Power(s2,2) - 
               2*s*(33 + 8*s2))*Power(t2,4) + 
            (-3 + Power(s,2) + 3*s2 + Power(s2,2) - 2*s*(3 + s2))*
             Power(t2,5)) - Power(t1,5)*
          (-2 + (15 - 2*s2)*t2 + 
            (36 + 15*s2 + Power(s2,2))*Power(t2,2) + 
            (3 + 6*s2 + Power(s2,2))*Power(t2,3) + 
            Power(s,2)*(1 - 17*t2 + 15*Power(t2,2) + 10*Power(t2,3)) + 
            s*(2 + (11 + 2*s2)*t2 - (63 + 13*s2)*Power(t2,2) - 
               (21 + 8*s2)*Power(t2,3))) + 
         Power(t2,2)*(-4 + t2 + 2*s*t2 + 
            2*(6 + 5*s + Power(s,2))*Power(t2,2) + 
            (5 + 6*s + 5*Power(s,2))*Power(t2,3) + 
            (4 + 12*s - 6*Power(s,2))*Power(t2,4) - 
            2*(-1 + s)*Power(t2,5) - 
            Power(s2,2)*(4 + 18*t2 + 2*Power(t2,2) - 3*Power(t2,3) + 
               4*Power(t2,4)) + 
            2*s2*(5 + (16 + s)*t2 - (1 + 3*s)*Power(t2,2) + 
               (-5 + 3*s)*Power(t2,3) + (2 + 5*s)*Power(t2,4) + 
               Power(t2,5))) + 
         Power(t1,6)*(Power(s,2)*(-2 + 6*t2 + 5*Power(t2,2)) - 
            2*s*(-1 + (9 + s2)*t2 + (5 + s2)*Power(t2,2)) + 
            t2*(10 + 3*t2 + 2*s2*(1 + t2))) + 
         2*Power(s1,5)*t1*(4*Power(t2,2)*(1 + t2) + 
            Power(t1,3)*(4 + 3*t2) - 
            Power(t1,2)*(13 + 16*t2 + 6*Power(t2,2) + 2*s*(1 + t2) - 
               s2*(2 + t2)) + 
            t1*(s2 + 2*s2*t2 + 
               2*(-1 + (1 + s)*t2 + (5 + s)*Power(t2,2) + 2*Power(t2,3)))\
) + Power(t1,4)*(Power(s,2)*t2*
             (7 - 52*t2 + 20*Power(t2,2) + 10*Power(t2,3)) + 
            s*(-2 + (15 + 2*s2)*t2 + (32 + 17*s2)*Power(t2,2) - 
               6*(19 + 5*s2)*Power(t2,3) - 12*(2 + s2)*Power(t2,4)) + 
            t2*(-5 + 68*t2 + 67*Power(t2,2) + 
               Power(s2,2)*t2*(-2 + 5*t2 + 3*Power(t2,2)) + 
               s2*(-2 + t2 + 41*Power(t2,2) + 7*Power(t2,3)))) + 
         t1*Power(t2,2)*(-19 - 20*t2 + 4*Power(t2,2) - 41*Power(t2,3) - 
            15*Power(t2,4) + Power(t2,5) - 
            Power(s,2)*t2*(4 + 21*t2 - 31*Power(t2,2) + Power(t2,3)) - 
            Power(s2,2)*(-4 + 2*t2 + Power(t2,2) - 14*Power(t2,3) + 
               2*Power(t2,4)) - 
            s2*(-4 - 24*t2 - 14*Power(t2,2) + 11*Power(t2,3) + 
               14*Power(t2,4) + Power(t2,5)) + 
            s*(-2 - 14*t2 - 21*Power(t2,2) - 49*Power(t2,3) + 
               19*Power(t2,4) + Power(t2,5) + 
               s2*(-2 + 16*t2 - 18*Power(t2,2) - 41*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(s1,3)*(-Power(t1,7) + Power(t1,6)*(-4 + 2*s2 + 5*t2) + 
            Power(t1,5)*(-18 + Power(s,2) + 7*s2 - Power(s2,2) + 
               2*s*(-2 + t2) + 6*t2 - 9*s2*t2 - 10*Power(t2,2)) + 
            Power(t1,4)*(16 - 2*Power(s,2) + 15*t2 + 10*Power(t2,3) + 
               Power(s2,2)*(-5 + 4*t2) + 
               s*(1 + s2*(5 - 4*t2) + 16*t2 - 5*Power(t2,2)) + 
               s2*(13 + 5*t2 + 17*Power(t2,2))) + 
            Power(t2,2)*(-(Power(s2,2)*t2*(3 + t2)) + 
               s2*(2 + 2*t2 + (2 + 3*s)*Power(t2,2) - Power(t2,3)) + 
               4*(-1 + (3 + s)*t2 + (7 + s)*Power(t2,2) + 2*Power(t2,3))\
) + Power(t1,3)*(30 + 112*t2 - 4*Power(s,2)*(-1 + t2)*t2 + 
               65*Power(t2,2) - 2*Power(t2,3) - 5*Power(t2,4) + 
               Power(s2,2)*(1 + 2*t2 - 8*Power(t2,2)) + 
               s*(4 - 3*(-7 + 4*s2)*t2 + 2*(-3 + 5*s2)*Power(t2,2) + 
                  5*Power(t2,3)) - 
               s2*(24 + 53*t2 + 26*Power(t2,2) + 15*Power(t2,3))) + 
            t1*t2*(-12 - 46*t2 + (26 + 19*s)*Power(t2,2) + 
               (53 + 14*s - Power(s,2))*Power(t2,3) + 
               (10 + s)*Power(t2,4) + 
               Power(s2,2)*(4 + 21*t2 + 2*Power(t2,2) - Power(t2,3)) + 
               s2*(-4 + (30 + 4*s)*t2 + (27 - 4*s)*Power(t2,2) + 
                  (1 + 2*s)*Power(t2,3) - 2*Power(t2,4))) + 
            Power(t1,2)*(2 - 2*(17 + 4*s)*t2 - 
               (124 + 45*s + 2*Power(s,2))*Power(t2,2) + 
               (-105 - 20*s + 4*Power(s,2))*Power(t2,3) - 
               (10 + 3*s)*Power(t2,4) + Power(t2,5) + 
               Power(s2,2)*(6 + 19*t2 + 4*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-14 - 4*(17 + s)*t2 + (11 + 8*s)*Power(t2,2) + 
                  (18 - 8*s)*Power(t2,3) + 7*Power(t2,4)))) - 
         Power(s1,4)*(Power(t1,6) + Power(t1,5)*(-3 + s - 2*s2 - 4*t2) + 
            Power(t2,3)*(4 - Power(s2,2) + 4*t2) - 
            Power(t1,3)*(8 + Power(s2,2)*(-3 + t2) + 38*t2 + 
               47*Power(t2,2) + 10*Power(t2,3) + 
               s2*(18 - 2*t2 + Power(t2,2)) + 
               s*(2 - s2*(-2 + t2) + 22*t2 + 13*Power(t2,2))) + 
            Power(t1,2)*(-6 - 2*(31 + 3*s)*t2 + 
               4*(-9 + s)*Power(t2,2) + (9 + 7*s)*Power(t2,3) + 
               5*Power(t2,4) + Power(s2,2)*(2 + 3*t2 + 3*Power(t2,2)) + 
               s2*(-2 + 2*(9 + s)*t2 - (-12 + s)*Power(t2,2) + 
                  3*Power(t2,3))) + 
            t1*t2*(-(Power(s2,2)*(-3 + t2)*t2) + 
               s2*(4 + 8*t2 + (2 + s)*Power(t2,2) - Power(t2,3)) + 
               4*(-2 + (5 + 2*s)*t2 + (13 + 2*s)*Power(t2,2) + 
                  4*Power(t2,3))) + 
            Power(t1,4)*(44 + Power(s2,2) + s2*(-14 + t2) + 35*t2 + 
               8*Power(t2,2) + s*(-s2 + 5*(2 + t2)))) + 
         Power(s1,2)*(Power(t1,7)*(-2 + s + t2) + 
            Power(t1,6)*(5 + Power(s,2) + 13*t2 - 4*Power(t2,2) + 
               s*(8 - s2 + t2) - s2*(5 + 2*t2)) - 
            Power(t1,5)*(-34 - Power(s2,2)*(-1 + t2) + 6*t2 + 
               32*Power(t2,2) - 6*Power(t2,3) + 2*Power(s,2)*(2 + t2) + 
               s2*(7 - 30*t2 - 8*Power(t2,2)) + 
               s*(2 - 3*s2 + 34*t2 + 13*Power(t2,2))) + 
            Power(t2,2)*(6 + (11 - 2*s)*t2 - (30 + 13*s)*Power(t2,2) + 
               (-32 - 7*s + 2*Power(s,2))*Power(t2,3) - 
               (5 + s)*Power(t2,4) - 
               Power(s2,2)*(2 + 5*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               s2*(2 - 2*(2 + s)*t2 - (17 + 11*s)*Power(t2,2) + 
                  2*Power(t2,3) + 2*Power(t2,4))) + 
            Power(t1,2)*(-4 - 3*(19 + 2*s)*t2 - 
               6*(9 + 5*s + Power(s,2))*Power(t2,2) + 
               2*(-17 + 28*s + 5*Power(s,2))*Power(t2,3) + 
               (19 + 4*s - Power(s,2))*Power(t2,4) + 
               (11 + 7*s)*Power(t2,5) + 
               Power(s2,2)*(-4 - 11*t2 - 10*Power(t2,2) + 
                  4*Power(t2,3) + 2*Power(t2,4)) + 
               s2*(10 + 2*(51 + s)*t2 - (-95 + s)*Power(t2,2) + 
                  (11 - 6*s)*Power(t2,3) - 10*Power(t2,4) - 
                  5*Power(t2,5))) + 
            t1*t2*(-4 + (41 + 10*s)*t2 + 
               4*(23 + 10*s + Power(s,2))*Power(t2,2) + 
               (50 - 6*s - 8*Power(s,2))*Power(t2,3) + 
               2*(-4 + s)*Power(t2,4) - (2 + s)*Power(t2,5) - 
               Power(s2,2)*(12 + 53*t2 + 6*Power(t2,2) - 
                  3*Power(t2,3) + Power(t2,4)) + 
               s2*(28 + 2*(26 + s)*t2 + 3*(-13 + 5*s)*Power(t2,2) + 
                  (-22 + 3*s)*Power(t2,3) + (-1 + s)*Power(t2,4) + 
                  Power(t2,5))) + 
            Power(t1,4)*(24 - 20*t2 + 3*Power(t2,2) + 40*Power(t2,3) - 
               4*Power(t2,4) + Power(s,2)*(2 + 8*t2) + 
               Power(s2,2)*(6 + t2 - 2*Power(t2,2)) - 
               s2*(24 + 27*t2 + 57*Power(t2,2) + 13*Power(t2,3)) + 
               s*(3 + 27*t2 + 49*Power(t2,2) + 24*Power(t2,3) + 
                  s2*(-4 - 6*t2 + 5*Power(t2,2)))) + 
            Power(t1,3)*(-17 - 90*t2 - 12*Power(t2,2) + 
               2*Power(s,2)*(-4 + t2)*Power(t2,2) - 8*Power(t2,3) - 
               28*Power(t2,4) + Power(t2,5) + Power(s2,2)*(3 + 14*t2) + 
               s2*(-2 - 23*t2 + 31*Power(t2,2) + 41*Power(t2,3) + 
                  11*Power(t2,4)) - 
               s*(2 + 68*Power(t2,2) + 28*Power(t2,3) + 19*Power(t2,4) + 
                  s2*(2 - t2 - 6*Power(t2,2) + 5*Power(t2,3))))) + 
         s1*(2*Power(t1,7)*(-2 + s*(3 + t2)) - 
            Power(t1,6)*(-17 + s2*(2 - 3*t2) - 25*t2 + Power(t2,2) + 
               Power(s,2)*(2 + t2) + s*(5 + (34 + s2)*t2 + 9*Power(t2,2))\
) + Power(t1,5)*(-7 - 79*t2 - 66*Power(t2,2) + 5*Power(t2,3) + 
               Power(s2,2)*t2*(2 + t2) + 
               Power(s,2)*(1 + 4*t2 + 5*Power(t2,2)) - 
               s2*(2 + 13*Power(t2,2) + Power(t2,3)) + 
               s*(-3 + 6*t2 + 73*Power(t2,2) + 16*Power(t2,3))) + 
            Power(t1,4)*(-9 - 40*t2 + 136*Power(t2,2) + 
               2*Power(s,2)*(1 - 5*t2)*Power(t2,2) + 93*Power(t2,3) - 
               9*Power(t2,4) + 
               Power(s2,2)*t2*(4 - 9*t2 - 4*Power(t2,2)) + 
               s*(6 - 5*(-1 + s2)*t2 + (34 + 3*s2)*Power(t2,2) + 
                  (-73 + 8*s2)*Power(t2,3) - 14*Power(t2,4)) + 
               s2*(2 + 35*t2 - 7*Power(t2,2) + 21*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(t1,3)*(3 - (17 + 39*s + 4*Power(s,2))*t2 + 
               2*(25 - 9*s + 2*Power(s,2))*Power(t2,2) - 
               (114 + 83*s + 12*Power(s,2))*Power(t2,3) + 
               2*(-37 + 15*s + 5*Power(s,2))*Power(t2,4) + 
               (7 + 6*s)*Power(t2,5) + 
               Power(s2,2)*t2*
                (-14 - 13*t2 + 14*Power(t2,2) + 6*Power(t2,3)) + 
               s2*(2 + 2*(26 + s)*t2 + 2*(-15 + 8*s)*Power(t2,2) + 
                  (37 - 6*s)*Power(t2,3) - 14*(1 + s)*Power(t2,4) - 
                  3*Power(t2,5))) + 
            Power(t2,2)*(2 - (21 + 4*s)*t2 - 
               (14 + 3*s + 2*Power(s,2))*Power(t2,2) + 
               (2 + 11*s - 4*Power(s,2))*Power(t2,3) + 
               (8 + s + 2*Power(s,2))*Power(t2,4) + 
               (1 + s)*Power(t2,5) + 
               Power(s2,2)*(6 + 24*t2 - 2*Power(t2,2) + 5*Power(t2,3) + 
                  Power(t2,4)) - 
               s2*(14 + 28*t2 - 4*(7 + 4*s)*Power(t2,2) + 
                  7*(-1 + s)*Power(t2,3) + 3*(2 + s)*Power(t2,4) + 
                  Power(t2,5))) + 
            t1*t2*(8 + 39*t2 - (25 + 29*s)*Power(t2,2) + 
               3*(-1 - 13*s + 5*Power(s,2))*Power(t2,3) - 
               (13 + 19*s + 8*Power(s,2))*Power(t2,4) + 
               (-8 - 5*s + Power(s,2))*Power(t2,5) + 
               Power(s2,2)*(8 + 34*t2 + 14*Power(t2,2) - 
                  25*Power(t2,3) + Power(t2,5)) + 
               s2*(-20 - 4*(21 + s)*t2 - 2*(24 + 17*s)*Power(t2,2) + 
                  (2 + 20*s)*Power(t2,3) + 3*(7 + 2*s)*Power(t2,4) + 
                  (3 - 2*s)*Power(t2,5))) + 
            Power(t1,2)*t2*(23 + 91*t2 + 2*Power(t2,2) + 45*Power(t2,3) + 
               33*Power(t2,4) - 2*Power(t2,5) + 
               Power(s,2)*t2*
                (6 - 16*t2 + 14*Power(t2,2) - 5*Power(t2,3)) - 
               Power(s2,2)*(8 + 8*t2 - 23*Power(t2,2) + 8*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s2*(-14 - 20*t2 - 43*Power(t2,3) + Power(t2,4) + 
                  Power(t2,5)) + 
               s*(4 + 65*t2 + 44*Power(t2,2) + 66*Power(t2,3) + 
                  2*Power(t2,4) - Power(t2,5) + 
                  s2*(4 + 16*t2 - 24*Power(t2,2) + 9*Power(t2,4))))))*
       T3(t2,t1))/
     ((-1 + s1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*
       (-1 + t2)*Power(-t1 + t2,2)) + 
    (8*(-1 + 4*Power(s1,5)*Power(t1,2)*(1 + t1) + 8*t2 + 2*s*t2 - 
         2*s2*t2 + 2*s*s2*t2 + 5*Power(t2,2) - 30*s*Power(t2,2) - 
         7*Power(s,2)*Power(t2,2) + 12*s2*Power(t2,2) - 
         2*s*s2*Power(t2,2) - Power(s2,2)*Power(t2,2) - 24*Power(t2,3) - 
         36*s*Power(t2,3) - 16*Power(s,2)*Power(t2,3) + 
         2*s2*Power(t2,3) - 8*s*s2*Power(t2,3) + 
         4*Power(s2,2)*Power(t2,3) - 24*Power(t2,4) - 20*s*Power(t2,4) - 
         Power(-1 + s,2)*Power(t1,6)*(1 + t2) + 
         Power(t1,5)*(2 + (9 + 2*s2)*t2 + 2*s2*Power(t2,2) + 
            Power(s,2)*t2*(5 + 2*t2) - 
            2*s*(1 + (8 + s2)*t2 + (2 + s2)*Power(t2,2))) - 
         Power(t1,4)*(-1 + 2*(15 + s2)*t2 + 
            (12 + 13*s2 + Power(s2,2))*Power(t2,2) + 
            Power(s2,2)*Power(t2,3) + 
            Power(s,2)*(-2 - 4*t2 + 7*Power(t2,2) + Power(t2,3)) - 
            s*(-4 + (21 + 2*s2)*t2 + (29 + 11*s2)*Power(t2,2) + 
               (3 + 2*s2)*Power(t2,3))) + 
         t1*(2 + (-15 + 2*s2)*t2 + (5 - 17*s2)*Power(t2,2) + 
            (49 + 4*s2 - 6*Power(s2,2))*Power(t2,3) + 
            (5 + 3*s2)*Power(t2,4) + 
            Power(s,2)*t2*(4 + 11*t2 - 5*Power(t2,2)) + 
            s*(-2 + (5 - 2*s2)*t2 + 3*(21 + 5*s2)*Power(t2,2) + 
               3*(9 + 5*s2)*Power(t2,3) + Power(t2,4))) + 
         Power(t1,3)*(-4 + (38 - 4*s2)*t2 + 3*(13 + 9*s2)*Power(t2,2) + 
            (9 + 8*s2 + 4*Power(s2,2))*Power(t2,3) + 
            (-1 + s2)*Power(t2,4) + 
            Power(s,2)*t2*(-15 - 5*t2 + 3*Power(t2,2)) - 
            s*(-4 - (7 + 4*s2)*t2 + 5*(7 + s2)*Power(t2,2) + 
               (19 + 7*s2)*Power(t2,3) + Power(t2,4))) + 
         Power(t1,2)*(1 + (-9 + 4*s2)*t2 + 
            (-37 - 11*s2 + 2*Power(s2,2))*Power(t2,2) - 
            (34 + 14*s2 + Power(s2,2))*Power(t2,3) - 
            4*(1 + s2)*Power(t2,4) + 
            Power(s,2)*(-1 + 3*t2 + 14*Power(t2,2) + 3*Power(t2,3)) + 
            s*(2 - (21 + 4*s2)*t2 - (23 + 17*s2)*Power(t2,2) + 
               (9 - 2*s2)*Power(t2,3) + 4*Power(t2,4))) + 
         Power(s1,3)*(-Power(t1,6) + 4*Power(t2,2) + 
            2*Power(t1,5)*(1 + t2) + 
            t1*(-2 + s + 79*t2 + 18*s*t2 + Power(s,2)*t2 + 
               18*Power(t2,2) + 15*s*Power(t2,2)) - 
            Power(s2,2)*Power(-1 + t1,2)*
             (-3 + Power(t1,2) - t2 - t1*(1 + t2)) + 
            Power(t1,4)*(17 + Power(s,2) - Power(t2,2) + 2*s*(3 + t2)) + 
            Power(t1,3)*(-46 + Power(s,2)*(-6 + t2) - 23*t2 + 
               4*Power(t2,2) - s*(35 + 8*t2 + Power(t2,2))) + 
            s2*(-1 + t1)*(-2 + 2*Power(t1,4) + 2*t2 + 3*s*t2 - 
               Power(t2,2) - Power(t1,3)*(13 + 3*t2) + 
               t1*(5 + s + 4*t2 - s*t2 - Power(t2,2)) + 
               Power(t1,2)*(8 + s + 5*t2 - 2*s*t2 + 2*Power(t2,2))) + 
            Power(t1,2)*(30 + 38*t2 + 23*Power(t2,2) + 
               Power(s,2)*(5 + 6*t2) + 2*s*(22 + 14*t2 + 5*Power(t2,2)))) \
- Power(s1,4)*(Power(s2,2)*Power(-1 + t1,2)*(1 + t1) - 
            s2*(-1 + t1)*t1*(-2 + s*(-1 + t1) + 2*Power(t1,2) + t2 - 
               t1*(2 + t2)) + 
            t1*(Power(t1,4) + Power(t1,3)*(-5 + s - t2) + 8*t2 + 
               Power(t1,2)*(-11 - 2*s + 10*t2) + 
               t1*(39 + 15*t2 + s*(9 + 8*t2)))) + 
         Power(s1,2)*(-1 + 2*t2 + s*t2 - 40*Power(t2,2) - 
            9*s*Power(t2,2) - 2*Power(s,2)*Power(t2,2) - 7*Power(t2,3) - 
            7*s*Power(t2,3) + Power(t1,6)*(-2 + s + t2) + 
            Power(t1,5)*(1 + Power(s,2) + 5*t2 - Power(t2,2) + 
               s*(6 + 4*t2)) + 
            Power(s2,2)*Power(-1 + t1,2)*
             (-1 + Power(t1,2)*(-1 + t2) - 2*t2 + Power(t2,2) + 
               t1*(-2 + Power(t2,2))) + 
            Power(t1,4)*(14 + Power(s,2)*(-8 + t2) - 22*t2 - 
               7*Power(t2,2) - s*(39 + 24*t2 + 4*Power(t2,2))) - 
            Power(t1,2)*(-1 - 77*t2 - 13*Power(t2,2) + 11*Power(t2,3) + 
               Power(s,2)*(4 + 9*t2 + 2*Power(t2,2)) + 
               s*(18 - 27*t2 + 9*Power(t2,2) + Power(t2,3))) + 
            Power(t1,3)*(-19 - 5*t2 + 4*Power(t2,2) + 2*Power(t2,3) + 
               Power(s,2)*(19 + 2*t2) + 
               s*(57 + 31*t2 + 11*Power(t2,2) + Power(t2,3))) - 
            t1*(-6 + 58*t2 + 113*Power(t2,2) + 16*Power(t2,3) + 
               2*Power(s,2)*t2*(5 + 6*t2) + 
               s*(7 + 87*t2 + 61*Power(t2,2) + 17*Power(t2,3))) - 
            s2*(-1 + t1)*(-6 + 3*t2 + 9*s*t2 + 2*Power(t2,2) - 
               2*Power(t2,3) + Power(t1,4)*(5 + s + 2*t2) + 
               Power(t1,3)*(-20 + 3*s*(-2 + t2) - 17*t2 - 
                  2*Power(t2,2)) + 
               Power(t1,2)*(7 + t2 + 5*Power(t2,2) + Power(t2,3) + 
                  s*(3 - 2*t2 + Power(t2,2))) + 
               t1*(14 + 11*t2 + 7*Power(t2,2) + Power(t2,3) - 
                  s*(-2 + 4*t2 + Power(t2,2))))) + 
         s1*(3 - 4*t2 - s*t2 + 28*Power(t2,2) + 41*s*Power(t2,2) + 
            10*Power(s,2)*Power(t2,2) + 64*Power(t2,3) + 
            31*s*Power(t2,3) + 6*Power(s,2)*Power(t2,3) + 3*Power(t2,4) + 
            7*s*Power(t2,4) + 2*Power(t1,6)*(-2 + s*(3 + t2)) + 
            Power(s2,2)*Power(-1 + t1,2)*t2*
             (2 - 5*t2 - Power(t2,2) + Power(t1,2)*(2 + t2) - 
               t1*(-4 + 3*t2 + Power(t2,2))) - 
            Power(t1,5)*(-25 - 13*t2 + Power(t2,2) + 
               Power(s,2)*(2 + t2) + s*(17 + 20*t2 + 3*Power(t2,2))) + 
            Power(t1,3)*(22 + 24*t2 + 54*Power(t2,2) + 9*Power(t2,3) + 
               s*(7 + 42*t2 + 17*Power(t2,2)) - 
               Power(s,2)*(4 - 10*t2 + 2*Power(t2,2) + Power(t2,3))) + 
            Power(t1,4)*(-45 - 38*t2 - 13*Power(t2,2) + 2*Power(t2,3) + 
               Power(s,2)*(5 + 2*Power(t2,2)) + 
               s*(13 + 25*t2 + 13*Power(t2,2) + Power(t2,3))) - 
            Power(t1,2)*(-14 - 10*t2 + 61*Power(t2,2) + 8*Power(t2,3) + 
               Power(t2,4) + Power(s,2)*(-1 + 28*t2 + 8*Power(t2,2)) + 
               s*(15 + 106*t2 + 60*Power(t2,2) + 14*Power(t2,3) + 
                  Power(t2,4))) + 
            t1*(-15 - 5*t2 - 7*Power(t2,2) + 29*Power(t2,3) + 
               6*Power(t2,4) + 
               3*Power(s,2)*t2*(1 + 10*t2 + Power(t2,2)) + 
               2*s*(3 + 29*t2 + 20*Power(t2,2) + 19*Power(t2,3) + 
                  Power(t2,4))) - 
            s2*(-1 + t1)*(2 - 2*(3 + 5*s)*t2 + (-7 + 3*s)*Power(t2,2) + 
               3*s*Power(t2,3) + Power(t2,4) + 
               Power(t1,4)*(2 + (-3 + s)*t2) - 
               Power(t1,2)*(4 - 9*(1 + s)*t2 + 
                  3*(-5 + 2*s)*Power(t2,2) + (1 + 2*s)*Power(t2,3)) + 
               Power(t1,3)*t2*(9 + 4*t2 + Power(t2,2) + s*(-1 + 3*t2)) - 
               t1*t2*(9 + 12*t2 + 8*Power(t2,2) + Power(t2,3) + 
                  s*(-1 + 6*t2 + Power(t2,2))))))*T4(t1))/
     ((-1 + s1)*(-1 + t1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       Power(s1*t1 - t2,2)*(-1 + t2)) - 
    (8*(1 - Power(s1,5)*(Power(s2,2)*(1 + t1) + 
            Power(t1,2)*(-1 + s + t1 - t2) - s2*t1*(2 + s + 2*t1 - t2)) - 
         7*t2 - 2*s*t2 + 2*s2*t2 - 2*s*s2*t2 - 9*Power(t2,2) + 
         24*s*Power(t2,2) + 7*Power(s,2)*Power(t2,2) - 10*s2*Power(t2,2) - 
         8*s*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) + 21*Power(t2,3) + 
         12*s*Power(t2,3) - Power(s,2)*Power(t2,3) - 10*s2*Power(t2,3) + 
         18*s*s2*Power(t2,3) - 3*Power(s2,2)*Power(t2,3) + 8*Power(t2,4) + 
         12*s*Power(t2,4) - 6*Power(s,2)*Power(t2,4) + 4*s2*Power(t2,4) + 
         10*s*s2*Power(t2,4) - 4*Power(s2,2)*Power(t2,4) + 2*Power(t2,5) - 
         2*s*Power(t2,5) + 2*s2*Power(t2,5) - 
         Power(-1 + s,2)*Power(t1,5)*(1 + t2) + 
         Power(t1,4)*(1 + (9 + 2*s2)*t2 + (1 + 2*s2)*Power(t2,2) - 
            2*s*t2*(8 + s2 + 3*t2 + s2*t2) + 
            Power(s,2)*(-1 + 5*t2 + 3*Power(t2,2))) + 
         Power(t1,3)*(2 - 22*t2 - 
            (19 + 13*s2 + Power(s2,2))*Power(t2,2) - 
            s2*(2 + s2)*Power(t2,3) + 
            Power(s,2)*(1 + 10*t2 - 8*Power(t2,2) - 3*Power(t2,3)) + 
            s*(-4 + 5*t2 + (37 + 11*s2)*Power(t2,2) + 
               (7 + 4*s2)*Power(t2,3))) - 
         t1*(1 - 7*t2 - (-15 + 7*s2 + Power(s2,2))*Power(t2,2) + 
            (38 + 13*s2 - 4*Power(s2,2))*Power(t2,3) + 
            (10 + 11*s2 + 2*Power(s2,2))*Power(t2,4) + 
            (-1 + s2)*Power(t2,5) + 
            Power(s,2)*t2*(4 - 4*t2 - 18*Power(t2,2) + Power(t2,3)) + 
            s*(-2 + 5*t2 + (26 + 15*s2)*Power(t2,2) + 
               6*(2 + 3*s2)*Power(t2,3) - (16 + 3*s2)*Power(t2,4) - 
               Power(t2,5))) + 
         Power(t1,2)*(-2 + (14 - 4*s2)*t2 + 
            (42 + 14*s2 - Power(s2,2))*Power(t2,2) + 
            (21 + 17*s2 + 4*Power(s2,2))*Power(t2,3) + 
            (-1 + s2 + Power(s2,2))*Power(t2,4) + 
            Power(s,2)*(1 - 6*t2 - 21*Power(t2,2) + 5*Power(t2,3) + 
               Power(t2,4)) - 
            s*t2*(-16 + 5*t2 + 37*Power(t2,2) + 4*Power(t2,3) + 
               2*s2*(-2 - 3*t2 + 6*Power(t2,2) + Power(t2,3)))) + 
         Power(s1,4)*(Power(s2,2)*
             (4 + t1 - 2*Power(t1,2) + 2*t2 + 2*t1*t2) + 
            s2*(2 + 4*Power(t1,3) + Power(t1,2)*(-11 + s - 6*t2) - 
               (2 + 3*s)*t2 + Power(t2,2) - 
               t1*(5 + 3*t2 - 3*Power(t2,2) + s*(2 + 3*t2))) + 
            t1*(-2 - 2*Power(t1,3) - t2 - 2*Power(t2,2) + 
               Power(s,2)*(t1 + t2) + Power(t1,2)*(2 + 4*t2) + 
               t1*(11 - 2*Power(t2,2)) - 
               s*(-1 + Power(t1,2) - 2*t2 + Power(t2,2) - t1*(7 + 3*t2)))) \
+ Power(s1,3)*(-1 - Power(t1,5) + 2*t2 + s*t2 - s*Power(t2,2) - 
            2*Power(s,2)*Power(t2,2) + Power(t2,3) + s*Power(t2,3) + 
            Power(t1,4)*(-1 + s + 4*t2) + 
            Power(t1,3)*(-3 + 2*Power(s,2) + 11*t2 - 4*Power(t2,2) + 
               6*s*(2 + t2)) - 
            Power(s2,2)*(4 + Power(t1,3) + 6*t2 + t1*t2 - 
               Power(t1,2)*(1 + 3*t2)) + 
            Power(t1,2)*(-4 + Power(s,2)*(-5 + t2) - 18*t2 - 
               8*Power(t2,2) + Power(t2,3) - 
               s*(27 + 12*t2 + 7*Power(t2,2))) - 
            t1*(-6 + 23*t2 - 4*Power(t2,3) + Power(s,2)*t2*(3 + t2) + 
               s*(8 + 8*t2 + 4*Power(t2,2) - 2*Power(t2,3))) + 
            s2*(-8 + 2*Power(t1,4) + 3*t2 + 12*s*t2 + Power(t2,2) + 
               3*s*Power(t2,2) - 3*Power(t2,3) - 
               Power(t1,3)*(18 + s + 7*t2) + 
               Power(t1,2)*(19 + s*(6 - 5*t2) + 23*t2 + 7*Power(t2,2)) + 
               t1*(13 + 17*t2 - 3*Power(t2,3) + s*(3 + t2 + Power(t2,2))))\
) + Power(s1,2)*(4 - 5*t2 - 2*s*t2 + 12*Power(t2,2) + s*Power(t2,2) + 
            8*Power(s,2)*Power(t2,2) + Power(t2,3) + s*Power(t2,3) - 
            2*Power(t2,4) - 2*s*Power(t2,4) + Power(t1,5)*(-2 + s + t2) + 
            Power(t1,4)*(-5 + Power(s,2) + 8*t2 - 2*Power(t2,2) + 
               s*(13 + 5*t2)) - 
            Power(t1,3)*(-26 - 2*t2 + 12*Power(t2,2) - Power(t2,3) + 
               Power(s,2)*(5 + t2) + s*(21 + 38*t2 + 11*Power(t2,2))) + 
            Power(s2,2)*(1 + Power(t1,3)*(-1 + t2) + 5*t2 - 
               4*Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,2)*(-1 + 2*t2 + Power(t2,2)) + 
               t1*(1 + 4*t2 - 3*Power(t2,2) - 2*Power(t2,3))) + 
            Power(t1,2)*(-9 - 5*t2 - 22*Power(t2,2) + 9*Power(t2,3) + 
               Power(s,2)*(5 - 3*t2 + Power(t2,2)) + 
               6*s*(3 + 3*Power(t2,2) + Power(t2,3))) + 
            t1*(-14 + 11*t2 + 42*Power(t2,2) + 7*Power(t2,3) - 
               2*Power(t2,4) + Power(s,2)*t2*(5 + 2*t2 - Power(t2,2)) + 
               s*(13 + 49*t2 + 21*Power(t2,2) + 5*Power(t2,3) - 
                  Power(t2,4))) - 
            s2*(-8 + (3 + 19*s)*t2 + 2*(5 + 2*s)*Power(t2,2) - 
               3*(2 + s)*Power(t2,3) - 3*Power(t2,4) + 
               Power(t1,4)*(5 + s + 2*t2) + 
               Power(t1,3)*(-18 + 3*s*(-2 + t2) - 25*t2 - 
                  4*Power(t2,2)) + 
               Power(t1,2)*(9 + 18*t2 + 18*Power(t2,2) + 4*Power(t2,3) + 
                  s*(11 - 5*t2 + Power(t2,2))) + 
               t1*(12 + 26*t2 + 24*Power(t2,2) - Power(t2,4) + 
                  s*(2 + 7*t2 - 3*Power(t2,2) - 3*Power(t2,3))))) + 
         s1*(-4 + 9*t2 + 3*s*t2 - 5*Power(t2,2) - 20*s*Power(t2,2) - 
            13*Power(s,2)*Power(t2,2) - 26*Power(t2,3) - 
            12*s*Power(t2,3) + 2*Power(s,2)*Power(t2,3) - 3*Power(t2,4) + 
            2*s*Power(t2,4) + 2*Power(s,2)*Power(t2,4) + Power(t2,5) + 
            s*Power(t2,5) + 2*Power(t1,5)*(-2 + s*(3 + t2)) - 
            Power(t1,4)*(-20 - 16*t2 + Power(t2,2) + 
               Power(s,2)*(3 + 2*t2) + s*(9 + 22*t2 + 5*Power(t2,2))) + 
            Power(s2,2)*t2*(-2 + 2*t2 + 10*Power(t2,2) + Power(t2,3) + 
               Power(t1,3)*(2 + t2) + 
               Power(t1,2)*(2 - 7*t2 - 3*Power(t2,2)) + 
               t1*(-2 - 8*t2 + 5*Power(t2,2) + Power(t2,3))) + 
            Power(t1,3)*(-24 - 35*t2 - 27*Power(t2,2) + 3*Power(t2,3) + 
               Power(s,2)*(1 + 4*t2 + 5*Power(t2,2)) + 
               s*(4 + 22*Power(t2,2) + 4*Power(t2,3))) - 
            Power(t1,2)*(t2*(1 - 36*t2 - 18*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s,2)*(2 - 11*t2 + Power(t2,2) + 4*Power(t2,3)) + 
               s*(-7 - 15*t2 - 45*Power(t2,2) + 3*Power(t2,3) + 
                  Power(t2,4))) + 
            t1*(12 + 11*t2 - 15*Power(t2,2) + 5*Power(t2,3) - 
               7*Power(t2,4) + 
               Power(s,2)*t2*(1 - 8*t2 - 2*Power(t2,2) + Power(t2,3)) - 
               2*s*(4 + 23*t2 + 2*Power(t2,2) + 18*Power(t2,3) + 
                  2*Power(t2,4))) - 
            s2*(2 - 2*(1 + 6*s)*t2 - (21 + 5*s)*Power(t2,2) + 
               (-1 + 18*s)*Power(t2,3) + (7 + 3*s)*Power(t2,4) + 
               Power(t2,5) + Power(t1,4)*(2 + (-3 + s)*t2) + 
               Power(t1,3)*t2*(5 + s + 5*t2 + 4*s*t2 + Power(t2,2)) - 
               Power(t1,2)*(4 - (5 + 11*s)*t2 + 
                  (-18 + 13*s)*Power(t2,2) + (5 + 7*s)*Power(t2,3) + 
                  Power(t2,4)) + 
               t1*t2*(-5 - 26*t2 - 23*Power(t2,2) - 2*Power(t2,3) + 
                  s*(-17 - 10*t2 + 5*Power(t2,2) + 2*Power(t2,3))))))*
       T5(1 - s1 - t1 + t2))/
     ((-1 + s1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*
       (-1 + t2)));
   return a;
};
