#include <tgmath.h>
#include <quadmath.h>


#include "../h/dilog.h"


#define Pi M_PIq

__float128 p128(__float128 x,unsigned int a)
{
    __float128 tmp=1.Q;
    do
    {
        if (a & 1) tmp=tmp*x;
        a>>=1;
        x=x*x;
    }
    while(a);
    return tmp;
};

__float128 Logq(__float128 x)
{
    return logq(fabsq(x));
};



__float128 Sqrtq(__float128 x)
{
    return sqrtq(x);
};


__float128 ln2bq(__float128 x)
{
    if(x<0) 
    {
        return p128( Logq((1.Q+sqrtq(1.Q-4.Q/x))/(sqrtq(1.Q-4.Q/x)-1.Q)) ,2);
    };
    if(x>4)
    {
        return p128( Logq((1.Q+sqrtq(1.Q-4.Q/x))/(1.Q-sqrtq(1.Q-4.Q/x))) ,2)-p128(Pi,2);
    }
    else
    {
        return -p128(Pi-2.Q*atanq(sqrtq(4.Q/x-1.Q)),2);
    };   
    if(x==0) return 0.Q;
    return 0.Q;
};


__float128 Li2q(__float128 x)
{
    return dilogq(x);
   };
   
   
  
__float128 Li2imq(__float128 re,__float128 im)
{
    __float128 result_re, result_im;
    
    dilogcq(re,im,&result_re,&result_im);
       
    return result_im;
};

__float128 Li2req(__float128 re,__float128 im)
{
    __float128 result_re, result_im;
    
    dilogcq(re,im,&result_re,&result_im);
       
    return result_re;
};
   
   
   
__float128 htq(__float128 x)
{
    if (x>0.) return 1.0Q;
    else return 0.0Q;
    };   


    
__float128 Rq(__float128 x,__float128 y)
{
    return dilogq(x/(x-y))-dilogq((x-1.Q)/(x-y));
    };


__float128 S3q(__float128 x,__float128 x1,__float128 x2)
{
    return Rq(x,x1)+Rq(x,x2);
    };
    
    
    
    
__float128 ffq(__float128 x, __float128 y)
{
    __float128 a;
    a=p128(Pi,2)*(htq(-x) + htq(-y))*htq(-1.0Q + x*y) + htq(-(x*y))*(p128(Pi,2)/6.0Q - dilogq(x*y))-(Logq(x) + Logq(y))*Logq(1.0Q-x*y)+
    htq(x*y)*(dilogq(1.0Q-x*y)+Logq(x*y)*Logq(1.0Q- x*y));
    return a;
    };
    
__float128 bbq(__float128 x)
{
    return sqrtq(1.0Q-4.0Q/x);
    };
    
__float128 Xq(__float128 a)
{
    return (bbq(a)-1.0Q)/(bbq(a)+1.0Q);
    };
    


    
__float128 R1q(__float128 x)
{
    return (1.Q/x-1.Q)*Logq(1.Q-x);
    };
    
__float128 R1nq(__float128 t1,__float128 t2)
{
   __float128 a; 
   if(fabsq(t1-t2)>0.0000001Q)
   {
        a=((-1.0Q + 1.Q/t1)*Logq(1.Q - t1) - ((t1 - t2)*(-t2 - Logq(1.Q - t2)))/p128(t2,2) + ((-1.Q + t2)*Logq(1.Q - t2))/t2 - (p128(t1 - t2,2)*(-2.Q*t2 + p128(t2,2) - 2.Q*Logq(1.Q - t2) + 2.Q*t2*Logq(1.Q - t2)))/(2.Q*(-1.Q + t2)*p128(t2,3)))/
   p128(t1 - t2,3);
   }
   else 
   {
      a=-1.Q/(3.Q*p128(-1.Q + t2,3)) + 1.Q/((-1.Q + t2)*p128(t2,3)) + 1.Q/(2.Q*p128(-1.Q + t2,2)*p128(t2,2)) + 1.Q/(3.Q*p128(-1.Q + t2,3)*t2) - Logq(1.Q - t2)/p128(t2,4) + 
   (t1 - t2)*(1.Q/(4.Q*p128(-1.Q + t2,4)) - 1.Q/((-1.Q + t2)*p128(t2,4)) - 1.Q/(2.Q*p128(-1.Q + t2,2)*p128(t2,3)) - 1.Q/(3.Q*p128(-1.Q + t2,3)*p128(t2,2)) - 1.Q/(4.Q*p128(-1.Q + t2,4)*t2) + Logq(1.Q - t2)/p128(t2,5));
    };
    return a;
};
    
    
    
__float128 R2q(__float128 x)
{
    if(x<0.0Q || x>4.0Q)
    { 
        return -sqrtq(1.Q - 4.Q/x)*logq( fabsq( (1.Q + sqrtq(1.Q - 4.Q/x))/(1.Q - sqrtq(1.Q - 4.Q/x)) ) );
    }
    else
    {
        return sqrtq(4.Q/x-1.Q)*(2.Q*atanq(sqrtq(4.Q/x-1.Q))-Pi);
    };
};

__float128 R2nq(__float128 x)
{
    return (R2q(x)+2.0Q)/x;
};

/*
__float128 T1q(__float128 x)
{
    return  (p128(Pi,2)/6.Q-dilogq(1.Q+x))/x;
    };*/




__float128 T3q(__float128 t1,__float128 t2)
{
    return -(dilog(t1)-dilog(t2))/(t1-t2);
    };
    
    
__float128 T3nq(__float128 t1,__float128 t2)
{
    __float128 a;
    if(fabsq(t1-t2)>0.0000001Q)
    {
        a=((t1 - t2)*(t2*(-t1 + t2) + (t1 - 3.Q*t2)*(-1.Q + t2)*Logq(1.Q - t2)) - 2.Q*(-1.Q + t2)*p128(t2,2)*dilogq(t1) + 2.Q*(-1.Q + t2)*p128(t2,2)*dilogq(t2))/(2.Q*p128(t1 - t2,3)*(-1.Q + t2)*p128(t2,2));
    }
    else
    {
        a=(2.Q*t2 - 3.Q*p128(t2,2) + 2.Q*Logq(1.Q - t2) - 4.Q*t2*Logq(1.Q - t2) + 2.Q*p128(t2,2)*Logq(1.Q - t2))/(6.Q*p128(-1.Q + t2,2)*p128(t2,3)) - 
   ((t1 - t2)*(-6.Q*t2 + 15.Q*p128(t2,2) - 11.Q*p128(t2,3) - 6.Q*Logq(1.Q - t2) + 18.Q*t2*Logq(1.Q - t2) - 18.Q*p128(t2,2)*Logq(1.Q - t2) + 6.Q*p128(t2,3)*Logq(1.Q - t2)))/(24.Q*p128(-1.Q + t2,3)*p128(t2,4));
    };
    return a;
};


__float128 T4q(__float128 x)
{
    return (Pi*Pi/6.Q-dilogq(x))/(x-1.Q);
     };

    

     
__float128 T1q(__float128 x)
{
    return  T4q(x);
    };
    
__float128 T5q(__float128 x)
{
    return ln2bq(x)/2.0Q/x;
    };
    
__float128 T5nq(__float128 x)
{
    return (ln2bq(x)/x+1.0Q)/(2.0Q*x);
    };

__float128 T6q(__float128 s,__float128 t)
{
    return (ln2bq(s)-ln2bq(t))/(2.0Q*(s - t));
    };
    
__float128 T7q(__float128 s,__float128 t)
{
    return T6q(s,t);
    };

__float128 T2q(__float128 t1, __float128 t2)
{
    __float128 x1,x2,x3,x31,x32,D3;
    D3=sqrtq(p128((t1+t2-1.Q),2)-4.Q*t1*t2);
    x1=(1.Q-p128(t1,2)-t2+t1*t2+(1.Q+t1)*D3)/2.Q/t1/D3;
    x2=-(-2.Q+2.Q*t1+3.Q*t2+t1*t2-p128(t2,2)+(t2-2.Q)*D3)/(D3*(1.Q+t1-t2+D3));
    x3=-(-2.Q+2.Q*t1+3.Q*t2+t1*t2-p128(t2,2)+(t2-2.Q)*D3)/(D3*(1.Q-t1-t2+D3));
    x31=(-sqrtq((-4.Q+t2)*t2)+t2)/(2.Q*t2);
    x32=(sqrtq((-4.Q+t2)*t2)+t2)/(2.Q*t2);
    if(fabsl(t1-1.Q)<0.0000000000001Q) return  S3q(x3,x31,x32)/D3;
    return (S3q(x1,1.0Q,1.0Q/t1)-S3q(x2,1.0Q,1.0Q)+S3q(x3,x31,x32))/D3;        
    };





__float128 B2q(__float128 s,__float128 t,__float128 m)
{
    return (p128(Pi,2)/6.Q-2.Q*(ffq(Xq(s),1.Q/Xq(m)) + ffq(Xq(s),Xq(m))) + p128(Pi,2)*htq(-Xq(m)) - dilogq(p128(Xq(s),2)) - p128(Logq(Xq(m)),2) - 2.Q*(-(p128(Pi,2)*htq(-1.Q+t)*htq(-Xq(s))) + Logq(1.Q-t)*Logq(Xq(s))) - 2.Q*Logq(Xq(s))*Logq(1.Q-p128(Xq(s),2)))/(s*(-1.Q+t)*bbq(s));    
    };  
    
    
__float128 signq(__float128 x)
{
    if(x<0.) return -1.Q;
    else return 1.Q;
};    
    
    
    
    
__float128 Rv2q(__float128 a,__float128 b,__float128 y)
{
    __float128 re1,re2,im1,im2;
    if(b<0.)
    {
        re1=(a*(a-y)-b)/(p128(a-y,2)-b);
        im1=-y*sqrtq(-b)/(p128(a-y,2)-b);
        re2=((a-1.0Q)*(a-y)-b)/(p128(a-y,2)-b);
        im2=(1.0Q-y)*sqrtq(-b)/(p128(a-y,2)-b);
        return 2.0Q*(Li2imq(re1,im1)-Li2imq(re2,im2));
    }
    else
    {
        return Rq(a+sqrtq(b),y)-Rq(a-sqrtq(b),y);
    };
    return 0.0Q;
};

__float128 Rv3q(__float128 x,__float128 a,__float128 b)
{
    __float128 re1,re2,im1,im2;
    if(b<0.)
    {
        re1=(x*(x-a))/(p128(x-a,2)-b);
        im1=x*sqrtq(-b)/(p128(x-a,2)-b);
        re2=(x-1.0Q)*(x-a)/(p128(x-a,2)-b);
        im2=(x-1.0Q)*sqrtq(-b)/(p128(x-a,2)-b);
              
        return 2.0Q*(Li2req(re1,im1)-Li2req(re2,im2));
    }
    else
    {
        return Rq(x,a+sqrtq(b))+Rq(x,a-sqrtq(b));
    };
    return 0.0Q;
    
};

__float128 reRcq(__float128 a,__float128 b,__float128 y)
{
    __float128 re1,re2,im1,im2;
    
    re1=(a*a+b*b-y*a)/(p128(a-y,2)+b*b);
    im1=-b*y/(p128(a-y,2)+b*b);
    re2=((a-1.0Q)*(a-y)+b*b)/(p128(a-y,2)+b*b);
    im2=b*(1.0Q-y)/(p128(a-y,2)+b*b);
    
    return Li2req(re1,im1)-Li2req(re2,im2);    
};

__float128 imRcq(__float128 a,__float128 b,__float128 y)
{
    __float128 re1,re2,im1,im2;
    
    re1=(a*a+b*b-y*a)/(p128(a-y,2)+b*b);
    im1=-b*y/(p128(a-y,2)+b*b);
    re2=((a-1.0Q)*(a-y)+b*b)/(p128(a-y,2)+b*b);
    im2=b*(1.0Q-y)/(p128(a-y,2)+b*b);
    
    return Li2imq(re1,im1)-Li2imq(re2,im2);    
};



__float128 RDq(__float128 a,__float128 b,__float128 y,__float128 D)
{
    if(D>=0. && b<=0.) return 0.Q;
    if(D>0. && b>0.) return Rv2q(a,b,y)/sqrtq(D);
    if(D<0. && b<0.) return Rv2q(a,b,y)/sqrtq(-D);
    return 0.Q;
};
    

__float128 reS3cq(__float128 a,__float128 b, __float128 x1, __float128 x2)
{
    return reRcq(a,b,x1)+reRcq(a,b,x2);
};

__float128 imS3cq(__float128 a,__float128 b,__float128 x1,__float128 x2)
{
    return imRcq(a,b,x1)+imRcq(a,b,x2);
};


    
__float128 S3v2q(__float128 ya,__float128 yb,__float128 xa,__float128 xb)
{
    __float128 re1,re2,im1,im2,re3,re4,im3,im4;
    if(xb>=0.)
    {
        return Rv2q(ya,yb,xa+sqrtq(xb))+Rv2q(ya,yb,xa-sqrtq(xb));
    };
    if(yb>=0.)
    {
        return Rv3q(ya+sqrtq(yb),xa,xb)-Rv3q(ya-sqrtq(yb),xa,xb);
    };
    if(xb<0. && yb<0.)
    {
        re1=(ya*(ya-xa)-yb-sqrtq(xb*yb))/(p128(ya-xa,2)+p128(sqrtq(-xb)-sqrtq(-yb),2));
        im1=(ya*sqrtq(-xb)-xa*sqrtq(-yb))/(p128(ya-xa,2)+p128(sqrtq(-xb)-sqrtq(-yb),2));
        re2=((ya-1.0Q)*(ya-xa)-yb-sqrtq(xb*yb))/(p128(ya-xa,2)+p128(sqrtq(-xb)-sqrtq(-yb),2));
        im2=((1.0Q-xa)*sqrtq(-yb)+(ya-1.0Q)*sqrtq(-xb))/(p128(ya-xa,2)+p128(sqrtq(-xb)-sqrtq(-yb),2));
        
        re3=(ya*(ya-xa)-yb+sqrtq(xb*yb))/(p128(ya-xa,2)+p128(sqrtq(-xb)+sqrtq(-yb),2));
        im3=(-ya*sqrtq(-xb)-xa*sqrtq(-yb))/(p128(ya-xa,2)+p128(sqrtq(-xb)+sqrtq(-yb),2));
        re4=((ya-1.0Q)*(ya-xa)-yb+sqrtq(xb*yb))/(p128(ya-xa,2)+p128(sqrtq(-xb)+sqrtq(-yb),2));
        im4=((1.0Q-xa)*sqrtq(-yb)+(1.0Q-ya)*sqrtq(-xb))/(p128(ya-xa,2)+p128(sqrtq(-xb)+sqrtq(-yb),2));
        
        return 2.0Q*(Li2imq(re1,im1)-Li2imq(re2,im2)+Li2imq(re3,im3)-Li2imq(re4,im4));
    };
    return 0.Q;
};
    

__float128 S3Dq(__float128 ya,__float128 yb, __float128 xa, __float128 xb,__float128 D)
{
    if(yb>=0. && xb>=0. && D>=0.)
    {
        return S3v2q(ya,yb,xa,xb)/sqrtq(D);
    };
    if(yb>0. && xb>0. && D<0.) return 0.Q;
    if(yb<0. && xb>0. && D>0.) return 0.Q;
    if(yb>0. && xb<0. && D>0.)
    { 
        return S3v2q(ya,yb,xa,xb)/sqrtq(D);
    };
    if(yb<0. && xb<0. && D>0.) return 0.Q;
    if(yb<0. && xb>0. && D<0.) 
    {
        return S3v2q(ya,yb,xa,xb)/sqrtq(-D);
    };
    if(yb>0. && xb<0. && D<0.) return 0.Q;
    if(yb<0. && xb<0. && D<0.) 
    {
        return S3v2q(ya,yb,xa,xb)/sqrtq(-D);
    };
    return 0.Q;
};    
    
    


__float128 B33q(__float128 s, __float128 t,__float128 w)
{
    return   -(2.*p128(Pi,2) - 3.*( -p128(Logq(s),2) + p128(Logq((p128(s,2)*t)/(s - w)),2) + 2.*p128(Logq((s*t)/(t - w)),2) + 
         p128(Logq((s*t)/(s + t - w)),2) - p128(Logq((s*t)/(-s + w)),2) - p128(Logq((s*(-s + w))/(s + t - w)),2) - 2.*p128(Logq((s*t*w)/((s - w)*(-t + w))),2)) - 12.*dilogq(1.Q + s/(t - w)) + 
      6.*dilogq((s + t - w)/t) + 6.*dilogq(t/(-s + w)) + 12.*dilogq(((s + t - w)*w)/((s - w)*(-t + w))))/(6.*s*t);  
};


__float128 h_b3q(__float128 s, __float128 t, __float128 m)
{
    __float128 SS, tmp, D;
    D=(1.0Q+4.0Q*(m-s-t)/(s*t));
    SS=sqrtq(-D);
    
    tmp = 2.Q*Pi*Logq((1. - D)*( p128(2*(m - s) - t,2) - t*t*D)/( 1.-4./t-D )/sqrtq(p128((m-s-t)*sqrtq(1.-4./s)+m-s,2)-t*t*D)/sqrtq(p128((m-s-t)*sqrtq(1.-4./s)-m+s,2)-t*t*D))/(s*t*SS);
    
    return tmp;
    
};

__float128 h2_b3q(__float128 s, __float128 t, __float128 m)
{
    __float128 SS, tmp, D;
    D=(1.0Q+4.0Q*(m-s-t)/(s*t));
    SS=sqrtq(-D);
    
    tmp = 2.Q*Pi*Logq(( p128(2*(m - s) - t,2) - t*t*D)*( 1.-4./m-D )/(1.Q - D)/sqrtq(p128((m-s-t)*sqrtq(1.-4./s)+m-s,2)-t*t*D)/sqrtq(p128((m-s-t)*sqrtq(1.L-4.L/s)-m+s,2)-t*t*D))/(s*t*SS);
    
    return tmp;
    
};

__float128 g_b3q(__float128 s, __float128 t, __float128 m)
{
    __float128 SS, tmp, D;
    D=(1.0Q+4.0Q*(m-s-t)/(s*t));
    SS=sqrtq(-D);
    
    tmp = 2.Q*Pi*Logq((m-s-t)*m/(m-s)/(m-t))/(s*t*SS);
    
    return tmp;
    
};

__float128 g2_b3q(__float128 s, __float128 t, __float128 m)
{
    __float128 SS, tmp, D;
    D=(1.0Q+4.0*(m-s-t)/(s*t));
    SS=sqrtq(-D);
    
    tmp = 2.Q*Pi*Logq((p128(t+2.*(s-m),2)-D*t*t)/(p128(s-m,2)+p128(t*sqrtq(-D)-(s+t-m)*sqrtq(4./s-1.Q),2)))/(s*t*SS);
    
    return tmp;
    
};



__float128 B3q(__float128 s,__float128 t,__float128 m)
{
    
    __float128 a1,a2,a3,b1,b2,b3;
    __float128 y1a,y1b,y2a,y2b;
    __float128 D, tmp;
    D=1.0Q+4.0Q*(m-s-t)/(s*t);
    
    y1a=-1.Q/2.Q;
    y1b=D/4.Q;
    
    y2a=t/(s+t-m)/2.Q;
    y2b=p128(t/(s+t-m)/2.Q,2)*D;
    
    a1=1.Q/2.Q;
    b1=(1.Q-4.Q/m)/4.Q;
    a2=1.Q/2.Q;
    b2=(1.Q-4.Q/t)/4.Q;
    a3=1.Q/2.Q;
    b3=(1.Q-4.Q/s)/4.Q;
    
     tmp=( 
            RDq(-y1a,y1b,1.Q,D)-RDq(1.Q+y1a,y1b,0.Q,D)+signq(y2a)*RDq(y2a,y2b,0.Q,D)-signq(y2a)*RDq(y2a,y2b,-t/(m-s-t),D)+\
            + S3Dq(1.Q+y1a,y1b,a1,b1,D) - S3Dq(-y1a,y1b,a2,b2,D) - signq(y2a)*S3Dq(y2a,y2b,a3,b3,D) 
        )/(s*t);
    
    if( isnan(tmp) ) tmp = B33q(s,t,m);
    
    
    
    if (  (s<0 && t<0) || (s<0 && m<0) || (t<0 && m<0) ) return tmp;
    if ( m>0 && s>0 && t>0 && m>s && m>t ) return tmp;
    if ( (s>0 && t>0) && s>m && t>m && D>0) return tmp - 4.L*Pi*Pi/(s*t*sqrtq(D));
    if ( (s>0 && t>0 && m>0) && D>0 && ( (s>m && m>t) || (t>m && m>s) ) ) return tmp - 4.L*Pi*Pi/(s*t*sqrtq(D));
    if ( ((s>0 && t<0)||(s<0 && t>0)) && D>0) return tmp;
    if ( ((s>0 && s<4 && t<0)||(s<0 && t<4 && t>0)) && s>m && m>0) return tmp;
    if ( ((s>4 && t<0)) && D<0 && m > 4) return tmp - h2_b3q(s,t,m);
    if ( ((s<0 && t>4)) && D<0 && m > 4) return tmp - h2_b3q(t,s,m);
    if ( (s<4 && s>0 && m>t && t<4)  && D<0 && m>0)  return tmp + g_b3q(s,t,m);
    if ( (t<4 && t>0 && m>s && s<4)  && D<0 && m>0)  return tmp + g_b3q(t,s,m);
    if ( (s>0 && s<4 && D<0) && t<0) 
    {
        if((2.Q*(s-m)+t)/t-sqrtq(D/(1.-4./s))<0) return tmp - g2_b3q(s,t,m) + g_b3q(s,t,m);
        else return tmp + g_b3q(s,t,m);
    };
    if ( (t>0 && t<4 && D<0) && s<0) 
    {
        if((2.Q*(t-m)+s)/s-sqrtq(D/(1.-4./t))<0) return tmp - g2_b3q(t,s,m) + g_b3q(t,s,m);
        else return tmp + g_b3q(t,s,m);
    };
    if ( (s>0 && t>0) && D<0) return tmp - h_b3q(s,t,m);
    
    return tmp;
        
};

__float128 B3nq(__float128 s,__float128 t,__float128 m)
{
    return ( B3q(s,t,m)-(s*R2nq(s)+t*R2nq(t)-m*R2nq(m))/(s+t-m) )/(s*t);
};

__float128 B1q(__float128 t,__float128 s,__float128 m)
{
    __float128 D=1.Q+4.Q*(m-t)/(s*(t-1.Q));
    __float128 ya, yb, a, b;
    
    ya=1.Q/2.Q;
    yb=D/4.Q;
    
    a=1.Q/2.Q;
    b=1.Q/4.Q-1.Q/s;
    
    return S3Dq(ya,yb,a,b,D)/(s*(1.Q-t));
    
};
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    