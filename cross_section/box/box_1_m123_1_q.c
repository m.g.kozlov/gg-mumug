#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m123_1_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(2*Power(s,5) + Power(s,4)*(-14 + 2*s1 - 2*s2 + 9*t1 - t2) - 
         2*Power(s1,4)*(-1 + t2) + 
         Power(s1,3)*(2 + 2*Power(s2,2) + 2*Power(t1,2) + 
            s2*(5 - 4*t1 - 5*t2) + 3*t1*(-1 + t2) - 8*t2 + 6*Power(t2,2)) \
+ Power(s,3)*(26 - 12*Power(s1,2) - 30*t1 + 6*Power(t1,2) - 26*t2 + 
            19*t1*t2 - 13*Power(t2,2) + s2*(17 - 6*t1 + 3*t2) + 
            s1*(26 - 3*s2 - 13*t1 + 26*t2)) + 
         s1*(5 - 38*t2 + 47*Power(t2,2) - 16*Power(t2,3) + 
            2*Power(t2,4) + Power(t1,2)*(6 + 6*t2 - 6*Power(t2,2)) + 
            6*Power(s2,2)*(2 - 2*t2 + Power(t2,2)) + 
            s2*(-46 + 6*t1*(-3 + t2) + 94*t2 - 33*Power(t2,2) - 
               15*Power(t2,3)) + 
            t1*(45 - 94*t2 + 38*Power(t2,2) + 11*Power(t2,3))) + 
         Power(s1,2)*(22 - 8*Power(t1,2) - 32*t2 + 16*Power(t2,2) - 
            6*Power(t2,3) - 2*Power(s2,2)*(1 + 3*t2) + 
            t1*(23 - 13*t2 - 10*Power(t2,2)) + 
            s2*(-22 + 7*t2 + 15*Power(t2,2) + 2*t1*(5 + 3*t2))) + 
         Power(s,2)*(-45 + 10*Power(s1,3) + 38*t1 + 
            Power(s1,2)*(-2 + 12*s2 - t1 - 39*t2) - 
            2*Power(s2,2)*(-6 + t2) + 50*t2 - 78*t1*t2 + 
            16*Power(t1,2)*t2 - 6*Power(t2,2) + 7*t1*Power(t2,2) - 
            15*Power(t2,3) + s1*
             (-22 + 2*Power(s2,2) + 71*t1 - 10*Power(t1,2) + 
               s2*(-48 + 8*t1 - 29*t2) + 9*t2 - 7*t1*t2 + 44*Power(t2,2)) \
+ s2*(-33 + 51*t2 + 17*Power(t2,2) - 2*t1*(6 + 7*t2))) + 
         (-1 + t2)*(16 - 11*t2 - 11*Power(t2,2) + 6*Power(t2,3) - 
            2*Power(s2,2)*(8 - 6*t2 + Power(t2,2)) + 
            2*Power(t1,2)*(-8 + 3*t2 + 2*Power(t2,2)) - 
            t1*(20 - 49*t2 + 26*Power(t2,2) + 4*Power(t2,3)) + 
            s2*(20 - 50*t2 + 26*Power(t2,2) + 5*Power(t2,3) - 
               2*t1*(-16 + 9*t2 + Power(t2,2)))) + 
         s*(47 - 2*Power(s1,4) - 37*t1 - 22*Power(t1,2) - 61*t2 + 
            113*t1*t2 + 2*Power(t1,2)*t2 + 7*Power(t2,2) - 
            70*t1*Power(t2,2) + 14*Power(t1,2)*Power(t2,2) + 
            12*Power(t2,3) - 7*t1*Power(t2,3) - 5*Power(t2,4) + 
            Power(s1,3)*(-12 - 7*s2 + 5*t1 + 16*t2) + 
            Power(s2,2)*(-28 + 26*t2 - 4*Power(t2,2)) + 
            Power(s1,2)*(-18 - 4*Power(s2,2) + 2*Power(t1,2) + 31*t2 - 
               31*Power(t2,2) - t1*(38 + 15*t2) + s2*(26 + 2*t1 + 31*t2)) \
+ s2*(38 - 109*t2 + 55*Power(t2,2) + 17*Power(t2,3) - 
               2*t1*(-25 + 14*t2 + 5*Power(t2,2))) + 
            s1*(-1 + Power(t1,2)*(8 - 16*t2) + 26*t2 - 31*Power(t2,2) + 
               22*Power(t2,3) + 2*Power(s2,2)*(-5 + 4*t2) + 
               t1*(-89 + 105*t2 + 17*Power(t2,2)) + 
               s2*(83 - 77*t2 - 41*Power(t2,2) + t1*(2 + 8*t2))))))/
     ((-1 + s1)*Power(-s + s1 - t2,2)*(1 - s + s1 - t2)*(-1 + s + t2)*
       (-1 + s2 - t1 + t2)) - (8*
       (-24 - 28*s2 - 4*Power(s2,2) + 28*t1 + 8*s2*t1 - 4*Power(t1,2) + 
         Power(s1,5)*(-s2 + t1) + 29*s2*t2 + 4*Power(s2,2)*t2 - 
         21*t1*t2 - 9*s2*t1*t2 + 5*Power(t1,2)*t2 + 14*Power(t2,2) - 
         3*s2*Power(t2,2) + 3*Power(s2,2)*Power(t2,2) + t1*Power(t2,2) - 
         5*s2*t1*Power(t2,2) + 2*Power(t1,2)*Power(t2,2) + 
         6*Power(t2,3) - 6*t1*Power(t2,3) + 4*Power(t2,4) + 
         Power(s,3)*(8 - 7*t1 + Power(s1,2)*(-8 + t1 - t2) - 
            2*s1*(t1 - t2) + 7*t2) + 
         Power(s1,4)*(1 - 2*Power(s2,2) - t2 - 2*t1*(1 + t2) + 
            s2*(3 + 2*t1 + t2)) + 
         Power(s1,3)*(-3 - 2*Power(t1,3) + t2 + 2*Power(t2,2) + 
            Power(t1,2)*(11 + 2*t2) + Power(s2,2)*(9 - 2*t1 + 4*t2) + 
            t1*(-23 - 8*t2 + Power(t2,2)) + 
            s2*(28 + 4*Power(t1,2) + t2 + Power(t2,2) - 2*t1*(10 + 3*t2))\
) + Power(s,2)*(-7 - 12*s2 + 11*t1 + s2*t1 - Power(t1,2) + 
            Power(s1,3)*(14 - 3*s2 + t1) + 17*t2 - 6*s2*t2 - 14*t1*t2 + 
            15*Power(t2,2) + s1*
             (-32 - 3*Power(t1,2) + s2*(9 + 3*t1 - 5*t2) - 15*t2 + 
               Power(t2,2) + 2*t1*(6 + t2)) + 
            Power(s1,2)*(17 + 4*Power(t1,2) - 10*t2 - 4*t1*(2 + t2) + 
               s2*(14 - 4*t1 + 3*t2))) + 
         s1*(-8 + 4*Power(s2,3) + 36*t2 - 15*Power(t2,2) - 
            13*Power(t2,3) - 2*Power(t1,3)*(2 + t2) + 
            Power(t1,2)*(31 + 4*t2 + 4*Power(t2,2)) - 
            t1*(23 + 48*t2 - 10*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s2,2)*(32 - 8*t2 + 3*Power(t2,2) - 2*t1*(6 + t2)) + 
            s2*(15 + 48*t2 - Power(t2,2) + Power(t2,3) + 
               4*Power(t1,2)*(3 + t2) + t1*(-63 + 4*t2 - 7*Power(t2,2)))) \
- Power(s1,2)*(26 + 4*Power(s2,3) - 20*t2 - 7*Power(t2,2) + 
            Power(t2,3) - 2*Power(t1,3)*(3 + t2) + 
            Power(t1,2)*(2 + 19*t2 + 2*Power(t2,2)) - 
            3*t1*(1 + 5*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*(-9 + 16*t2 + 2*Power(t2,2) - 2*t1*(7 + t2)) + 
            s2*(1 + 23*t2 + 5*Power(t2,2) + Power(t2,3) + 
               4*Power(t1,2)*(4 + t2) + t1*(7 - 35*t2 - 4*Power(t2,2)))) \
+ s*(28 + 37*s2 + 4*Power(s2,2) - 29*t1 - 9*s2*t1 + 5*Power(t1,2) + 
            11*t2 - 15*s2*t2 + 3*Power(s2,2)*t2 + 12*t1*t2 - 4*s2*t1*t2 + 
            Power(t1,2)*t2 + 15*Power(t2,2) - 6*s2*Power(t2,2) - 
            13*t1*Power(t2,2) + 12*Power(t2,3) + 
            Power(s1,4)*(-6 + 4*s2 - 3*t1 + t2) + 
            Power(s1,3)*(-22 + 2*Power(s2,2) - 4*Power(t1,2) + 
               s2*(-21 + 2*t1 - 6*t2) + 3*t2 - 2*Power(t2,2) + 
               8*t1*(2 + t2)) + 
            Power(s1,2)*(27 + 2*Power(t1,3) + 
               Power(s2,2)*(-1 + 2*t1 - 2*t2) + 27*t2 + 
               2*Power(t1,2)*t2 + 4*Power(t2,2) + Power(t2,3) + 
               t1*(10 - 16*t2 - 5*Power(t2,2)) + 
               s2*(-29 + t1 - 4*Power(t1,2) + 27*t2 + 2*Power(t2,2))) - 
            s1*(-9 + 2*Power(t1,3) + Power(s2,2)*(17 + 2*t1 - 3*t2) - 
               Power(t1,2)*(-5 + t2) + 78*t2 + 29*Power(t2,2) + 
               Power(t2,3) - 2*t1*(7 + 14*t2 + Power(t2,2)) + 
               s2*(15 - 4*Power(t1,2) - 2*t2 + 4*Power(t2,2) + 
                  t1*(-22 + 4*t2)))))*
       B1(1 - s2 + t1 - t2,s1,1 - s + s1 - t2))/
     ((-1 + s1)*Power(-s + s1 - t2,2)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)) + 
    (32*(Power(s1,4)*(s2 - t1) + 
         Power(s,3)*(-2 + s1 + Power(s1,2) + s1*t1 - t2) - 
         Power(-1 + t2,2)*(-6 + t2 + Power(t2,2)) + 
         Power(s1,3)*(2 - 2*t2 - s2*(3 + 2*t2) + t1*(3 + 2*t2)) + 
         Power(s1,2)*(-3 + 2*t2 + Power(t2,2) + 
            s2*(4 + 2*t2 + Power(t2,2)) - t1*(5 + 2*Power(t2,2))) + 
         s1*(-3 + 8*t2 - 5*Power(t2,2) + 
            s2*(-4 + 4*t2 - 3*Power(t2,2)) + 
            t1*(4 - 3*t2 + Power(t2,2) + Power(t2,3))) - 
         Power(s,2)*(Power(s1,3) + Power(s1,2)*(3 + t1 - t2) + 
            3*(-3 + t2 + Power(t2,2)) + s1*(2*s2 - 3*(-1 + t2 + t1*t2))) \
+ s*(-13 + Power(s1,3)*(4 - s2 + t1) + 16*t2 - 3*Power(t2,3) + 
            Power(s1,2)*(-5 + s2 + t1 - 3*t2 + s2*t2 - 3*t1*t2) + 
            s1*(s2*(4 - 5*t2) + 2*(5 - 4*t2 + Power(t2,2)) + 
               t1*(-3 + t2 + 3*Power(t2,2)))))*R1q(s1))/
     (Power(-1 + s1,2)*Power(-s + s1 - t2,2)*Power(-1 + s + t2,2)) - 
    (16*(Power(s,7) + 2*Power(s1,6)*(s2 - t1) - 
         Power(s,6)*(12 + 7*s1 + 2*s2 - 3*t2) + 
         Power(s,5)*(12 + 18*Power(s1,2) + Power(s2,2) - 20*t1 + 
            s1*(54 + 10*s2 - t1 - 19*t2) - 40*t2 - 6*t1*t2 + 
            3*Power(t2,2) - s2*(-26 + t1 + 5*t2)) + 
         Power(s1,5)*(3 + 4*Power(s2,2) + 4*Power(t1,2) - 2*t2 - 
            Power(t2,2) + t1*(5 + 7*t2) - s2*(5 + 8*t1 + 7*t2)) + 
         Power(s1,4)*(2*Power(s2,3) - 2*Power(t1,3) + 
            Power(t1,2)*(2 - 14*t2) - 2*Power(s2,2)*(-1 + 3*t1 + 7*t2) - 
            t1*(5 + 8*t2 + 11*Power(t2,2)) + 
            3*(-3 + 3*t2 - Power(t2,2) + Power(t2,3)) + 
            2*s2*(1 + 3*Power(t1,2) + 7*t2 + 4*Power(t2,2) + 
               2*t1*(-1 + 7*t2))) + 
         Power(s,4)*(52 - 22*Power(s1,3) + 91*t1 - 8*Power(t1,2) + 
            Power(t1,3) - 23*t2 - 56*t1*t2 - 6*Power(t1,2)*t2 - 
            56*Power(t2,2) - 21*t1*Power(t2,2) + Power(t2,3) + 
            Power(s2,2)*(-12 + t1 + t2) + 
            Power(s1,2)*(-100 - 15*s2 + 41*t2) + 
            s2*(-95 - 2*Power(t1,2) + 82*t2 - 3*Power(t2,2) + 
               5*t1*(4 + t2)) + 
            s1*(-29 - 2*Power(s2,2) + 72*t1 + 2*Power(t1,2) + 142*t2 + 
               23*t1*t2 - 20*Power(t2,2) + 2*s2*(-49 + 9*t2))) + 
         Power(s1,3)*(-(Power(s2,3)*(1 + 7*t2)) + 
            Power(t1,3)*(1 + 7*t2) + 
            Power(t1,2)*(2 - 26*t2 + 20*Power(t2,2)) + 
            t1*(-21 + 13*t2 + 17*Power(t2,2) + 11*Power(t2,3)) - 
            3*(7 - 21*t2 + 18*Power(t2,2) - 5*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,2)*(3*t1*(1 + 7*t2) + 2*t2*(-11 + 9*t2)) - 
            s2*(-17 - 4*t2 + 39*Power(t2,2) + 2*Power(t2,3) + 
               3*Power(t1,2)*(1 + 7*t2) + t1*(2 - 48*t2 + 38*Power(t2,2))\
)) + s1*(-1 + t2)*(Power(s2,3)*(-6 + 2*t2 - 5*Power(t2,2)) + 
            Power(t1,3)*(5 + t2 + 3*Power(t2,2)) + 
            Power(t1,2)*t2*(-13 - 15*t2 + 10*Power(t2,2)) + 
            Power(-1 + t2,2)*(-66 + 50*t2 + 11*Power(t2,2)) + 
            t1*(-79 + 138*t2 - 68*Power(t2,2) + 16*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,2)*(1 - 21*t2 + 2*Power(t2,3) + 
               t1*(17 - 3*t2 + 13*Power(t2,2))) + 
            s2*(77 - 132*t2 + 66*Power(t2,2) - 21*Power(t2,3) + 
               Power(t2,4) - Power(t1,2)*(16 + 11*Power(t2,2)) + 
               t1*(-1 + 34*t2 + 15*Power(t2,2) - 12*Power(t2,3)))) + 
         Power(-1 + t2,2)*(32 + Power(t1,3)*(-4 + t2) - 90*t2 + 
            83*Power(t2,2) - 22*Power(t2,3) - 4*Power(t2,4) + 
            Power(s2,3)*(4 - 2*t2 + Power(t2,2)) + 
            t1*(32 - 55*t2 + 21*Power(t2,2) + Power(t2,3)) - 
            Power(t1,2)*(4 - 16*t2 + 4*Power(t2,2) + 3*Power(t2,3)) - 
            Power(s2,2)*(4 - 17*t2 + 8*Power(t2,2) + 
               t1*(12 - 5*t2 + 2*Power(t2,2))) + 
            s2*(-32 + 53*t2 - 18*Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,2)*(12 - 4*t2 + Power(t2,2)) + 
               t1*(8 - 33*t2 + 12*Power(t2,2) + 3*Power(t2,3)))) + 
         Power(s1,2)*(25 - 46*t2 - 2*Power(t2,2) + 39*Power(t2,3) - 
            17*Power(t2,4) + Power(t2,5) + 
            Power(t1,3)*(2 - 8*Power(t2,2)) + 
            Power(s2,3)*(-1 - 2*t2 + 9*Power(t2,2)) + 
            Power(t1,2)*(7 - 25*t2 + 47*Power(t2,2) - 17*Power(t2,3)) + 
            t1*(38 - 65*t2 + 57*Power(t2,2) - 29*Power(t2,3) - 
               7*Power(t2,4)) + 
            Power(s2,2)*(4 - 12*t2 + 30*Power(t2,2) - 10*Power(t2,3) + 
               t1*(4 + 4*t2 - 26*Power(t2,2))) + 
            s2*(-37 + 70*t2 - 79*Power(t2,2) + 54*Power(t2,3) - 
               2*Power(t2,4) + 
               Power(t1,2)*(-5 - 2*t2 + 25*Power(t2,2)) + 
               t1*(-11 + 37*t2 - 77*Power(t2,2) + 27*Power(t2,3)))) + 
         Power(s,3)*(-155 + 13*Power(s1,4) - 185*t1 + 20*Power(t1,2) - 
            Power(t1,3) + Power(s1,3)*(96 + 3*s2 + 8*t1 - 39*t2) + 
            Power(s2,3)*(-2 + t2) + 337*t2 + 276*t1*t2 - 
            16*Power(t1,2)*t2 + 3*Power(t1,3)*t2 - 144*Power(t2,2) - 
            53*t1*Power(t2,2) - 21*Power(t1,2)*Power(t2,2) - 
            50*Power(t2,3) - 27*t1*Power(t2,3) + 
            Power(s2,2)*(26 - 38*t2 - 3*Power(t2,2) + t1*(3 + t2)) - 
            Power(s1,2)*(-22 + 5*Power(s2,2) + 10*Power(t1,2) + 
               196*t2 - 39*Power(t2,2) + s2*(-143 - 15*t1 + 10*t2) + 
               t1*(103 + 45*t2)) + 
            s2*(181 - 286*t2 - 5*Power(t1,2)*t2 + 94*Power(t2,2) + 
               Power(t2,3) + t1*(-46 + 54*t2 + 24*Power(t2,2))) + 
            s1*(-133 - Power(s2,3) - Power(t1,3) + 62*t2 + 
               160*Power(t2,2) - 12*Power(t2,3) + 
               Power(s2,2)*(31 + t1 + 8*t2) + 
               2*Power(t1,2)*(9 + 14*t2) + 
               t1*(-210 + 138*t2 + 65*Power(t2,2)) + 
               s2*(232 + Power(t1,2) - 229*t2 + 7*Power(t2,2) - 
                  t1*(49 + 36*t2)))) - 
         Power(s,2)*(-192 + 3*Power(s1,5) - 209*t1 + 20*Power(t1,2) + 
            5*Power(t1,3) + Power(s1,4)*(48 - 11*s2 + 14*t1 - 16*t2) + 
            614*t2 + 479*t1*t2 - 58*Power(t1,2)*t2 + Power(t1,3)*t2 - 
            643*Power(t2,2) - 301*t1*Power(t2,2) + 
            6*Power(t1,2)*Power(t2,2) - 3*Power(t1,3)*Power(t2,2) + 
            186*Power(t2,3) + 17*t1*Power(t2,3) + 
            27*Power(t1,2)*Power(t2,3) + 36*Power(t2,4) + 
            15*t1*Power(t2,4) + 
            Power(s2,3)*(-8 + 8*t2 - 3*Power(t2,2)) + 
            Power(s1,3)*(2 - 16*Power(s2,2) - 18*Power(t1,2) - 124*t2 + 
               31*Power(t2,2) + s2*(101 + 34*t1 + 21*t2) - 
               t1*(75 + 52*t2)) + 
            Power(s2,2)*(24 - 82*t2 + 48*Power(t2,2) + 5*Power(t2,3) + 
               3*t1*(7 - 5*t2 + Power(t2,2))) + 
            Power(s1,2)*(-101 - 4*Power(s2,3) + 3*Power(t1,3) + 33*t2 + 
               166*Power(t2,2) - 23*Power(t2,3) + 
               2*Power(t1,2)*(5 + 26*t2) + 
               Power(s2,2)*(23 + 11*t1 + 35*t2) - 
               s2*(-184 + 33*t1 + 10*Power(t1,2) + 228*t2 + 87*t1*t2 + 
                  8*Power(t2,2)) + t1*(-150 + 121*t2 + 82*Power(t2,2))) \
+ s2*(203 - 463*t2 + 304*Power(t2,2) - 44*Power(t2,3) - Power(t2,4) + 
               3*Power(t1,2)*(-6 + 2*t2 + Power(t2,2)) - 
               2*t1*(22 - 70*t2 + 27*Power(t2,2) + 16*Power(t2,3))) + 
            s1*(-238 + 522*t2 - 193*Power(t2,2) - 116*Power(t2,3) + 
               5*Power(t2,4) - Power(t1,3)*(3 + t2) + 
               Power(s2,3)*(-2 + 7*t2) + 
               Power(t1,2)*(25 + 4*t2 - 60*Power(t2,2)) + 
               Power(s2,2)*(40 + t1 - 45*t2 - 15*t1*t2 - 
                  24*Power(t2,2)) - 
               t1*(287 - 438*t2 + 89*Power(t2,2) + 59*Power(t2,3)) + 
               s2*(287 - 489*t2 + 201*Power(t2,2) - Power(t2,3) + 
                  Power(t1,2)*(4 + 9*t2) + 
                  t1*(-65 + 41*t2 + 84*Power(t2,2))))) + 
         s*(Power(s1,5)*(10 - 9*s2 + 9*t1 - 2*t2) - 
            Power(s1,4)*(6 + 14*Power(s2,2) + 14*Power(t1,2) + 28*t2 - 
               10*Power(t2,2) - s2*(35 + 28*t1 + 25*t2) + t1*(29 + 31*t2)\
) + Power(s1,3)*(-11 - 5*Power(s2,3) + 5*Power(t1,3) - 15*t2 + 
               65*Power(t2,2) - 15*Power(t2,3) + 
               Power(s2,2)*(2 + 15*t1 + 40*t2) + 
               Power(t1,2)*(-2 + 44*t2) - 
               s2*(-45 + 15*Power(t1,2) + 95*t2 + 84*t1*t2 + 
                  20*Power(t2,2)) + t1*(-26 + 47*t2 + 49*Power(t2,2))) + 
            Power(s1,2)*(-66 + 126*t2 + 8*Power(t2,2) - 84*Power(t2,3) + 
               8*Power(t2,4) - Power(t1,3)*(3 + 11*t2) + 
               Power(s2,3)*(1 + 13*t2) + 
               Power(t1,2)*(3 + 46*t2 - 59*Power(t2,2)) - 
               t1*(85 - 157*t2 + 56*Power(t2,2) + 44*Power(t2,3)) + 
               Power(s2,2)*(14 + 16*t2 - 40*Power(t2,2) - 
                  t1*(5 + 37*t2)) + 
               s2*(93 - 214*t2 + 148*Power(t2,2) + Power(t2,3) + 
                  7*Power(t1,2)*(1 + 5*t2) + 
                  t1*(-17 - 62*t2 + 99*Power(t2,2)))) + 
            s1*(-189 + 585*t2 - 570*Power(t2,2) + 120*Power(t2,3) + 
               55*Power(t2,4) - Power(t2,5) + 
               Power(s2,3)*(-7 + 9*t2 - 11*Power(t2,2)) + 
               Power(t1,3)*(3 + t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(5 - 20*t2 - 47*Power(t2,2) + 
                  44*Power(t2,3)) + 
               t1*(-227 + 494*t2 - 315*Power(t2,2) + 37*Power(t2,3) + 
                  20*Power(t2,4)) + 
               s2*(220 - 487*t2 + 347*Power(t2,2) - 92*Power(t2,3) + 
                  3*Power(t2,4) + 
                  Power(t1,2)*(-13 + 7*t2 - 21*Power(t2,2)) + 
                  t1*(-17 + 78*t2 + 35*Power(t2,2) - 60*Power(t2,3))) + 
               Power(s2,2)*(t1*(17 - 17*t2 + 27*Power(t2,2)) + 
                  2*(6 - 29*t2 + 6*Power(t2,2) + 8*Power(t2,3)))) + 
            (-1 + t2)*(122 - 369*t2 + 372*Power(t2,2) - 109*Power(t2,3) - 
               18*Power(t2,4) + Power(t1,3)*(-9 + 2*t2 + Power(t2,2)) + 
               Power(s2,3)*(10 - 7*t2 + 3*Power(t2,2)) - 
               Power(t1,2)*(12 - 48*t2 + 11*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(127 - 257*t2 + 133*Power(t2,2) - 2*Power(t2,3) - 
                  3*Power(t2,4)) - 
               Power(s2,2)*(13 - 57*t2 + 32*Power(t2,2) + 
                  2*Power(t2,3) + t1*(29 - 16*t2 + 5*Power(t2,2))) + 
               s2*(-125 + 246*t2 - 123*Power(t2,2) + 4*Power(t2,3) + 
                  Power(t1,2)*(28 - 11*t2 + Power(t2,2)) + 
                  t1*(25 - 105*t2 + 43*Power(t2,2) + 17*Power(t2,3))))))*
       R1q(1 - s + s1 - t2))/
     ((-1 + s1)*Power(-s + s1 - t2,2)*(1 - s + s1 - t2)*
       Power(-1 + s + t2,2)*(-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2) + 2*s2 - 2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 
         2*s*t1 - 2*s1*t1 - 2*s2*t1 + Power(t1,2) + 4*t2)) - 
    (16*(64 + Power(s,5) + Power(s1,5) - 68*s2 - 8*Power(s2,2) + 
         12*Power(s2,3) + 68*t1 + 16*s2*t1 - 36*Power(s2,2)*t1 - 
         8*Power(t1,2) + 36*s2*Power(t1,2) - 12*Power(t1,3) + 
         Power(s1,4)*(-3 + 6*s2 - 5*t1 - t2) - 149*t2 + 76*s2*t2 + 
         31*Power(s2,2)*t2 - 6*Power(s2,3)*t2 - 77*t1*t2 - 52*s2*t1*t2 + 
         9*Power(s2,2)*t1*t2 + 21*Power(t1,2)*t2 - 3*Power(t1,3)*t2 + 
         101*Power(t2,2) - 10*s2*Power(t2,2) - 
         14*Power(s2,2)*Power(t2,2) + 3*Power(s2,3)*Power(t2,2) + 
         11*t1*Power(t2,2) + 13*s2*t1*Power(t2,2) - 
         3*Power(s2,2)*t1*Power(t2,2) + Power(t1,2)*Power(t2,2) - 
         3*s2*Power(t1,2)*Power(t2,2) + 3*Power(t1,3)*Power(t2,2) - 
         20*Power(t2,3) + t1*Power(t2,3) + 3*s2*t1*Power(t2,3) - 
         3*Power(t1,2)*Power(t2,3) - 4*Power(t2,4) - 
         Power(s,4)*(3 + s1 + 2*s2 - 5*t1 + t2) + 
         Power(s,3)*(-1 - 4*Power(s1,2) + Power(s2,2) - 3*t1 + 
            10*Power(t1,2) - 11*t2 - 2*t1*t2 - 2*Power(t2,2) + 
            s2*(10 - 11*t1 + 5*t2) + s1*(20 - 4*s2 - 6*t1 + 6*t2)) + 
         Power(s1,3)*(7 + 8*Power(s2,2) + 2*Power(t1,2) + 8*t2 - 
            Power(t2,2) + t1*(13 + 11*t2) - s2*(17 + 10*t1 + 11*t2)) + 
         Power(s1,2)*(-38 + 3*Power(s2,3) + 2*Power(t1,3) + 21*t2 - 
            12*Power(t2,2) + Power(t2,3) - Power(t1,2)*(8 + 5*t2) - 
            Power(s2,2)*(11 + 4*t1 + 16*t2) - 
            2*t1*(18 + 9*t2 + 4*Power(t2,2)) + 
            s2*(34 - Power(t1,2) + 30*t2 + 4*Power(t2,2) + 
               t1*(19 + 21*t2))) + 
         Power(s,2)*(8*Power(s1,3) + Power(s2,2)*(-1 + 6*t1 - 7*t2) + 
            2*Power(s1,2)*(-17 + 10*s2 - 4*t1 - 5*t2) + 
            s1*(11 + 8*Power(s2,2) + 23*t1 - 16*Power(t1,2) + 
               s2*(-41 + 8*t1 - 25*t2) + 42*t2 + 19*t1*t2 + 
               3*Power(t2,2)) + 
            s2*(-25 - 12*Power(t1,2) - t1*(-5 + t2) + 28*t2 + 
               7*Power(t2,2)) + 
            2*(7 + 3*Power(t1,3) - 9*t2 - 8*Power(t2,2) + 
               Power(t1,2)*(-2 + 4*t2) + t1*(8 - 2*t2 - 5*Power(t2,2)))) \
+ s1*(25 + Power(t1,3)*(11 - 5*t2) - 11*t2 + 4*Power(t2,2) + 
            7*Power(t2,3) - 2*Power(s2,3)*(1 + 3*t2) + 
            Power(t1,2)*(7 - 13*t2 + 6*Power(t2,2)) + 
            t1*(21 - 3*t2 + 20*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s2,2)*(-3 + 5*t2 + 8*Power(t2,2) + t1*(15 + 7*t2)) + 
            s2*(-20 + 4*Power(t1,2)*(-6 + t2) + 4*t2 - 29*Power(t2,2) + 
               Power(t2,3) - 2*t1*(2 - 4*t2 + 7*Power(t2,2)))) - 
         s*(81 + 5*Power(s1,4) + 93*t1 + 15*Power(t1,2) + 3*Power(t1,3) + 
            2*Power(s1,3)*(-10 + 10*s2 - 7*t1 - 3*t2) - 
            3*Power(s2,3)*(-2 + t2) - 123*t2 - 63*t1*t2 + 
            3*Power(t1,2)*t2 - 9*Power(t1,3)*t2 + 49*Power(t2,2) + 
            5*Power(t1,2)*Power(t2,2) + 12*Power(t2,3) + 
            3*t1*Power(t2,3) + 
            Power(s2,2)*(5 + 15*t2 + 8*Power(t2,2) - 3*t1*(3 + t2)) + 
            Power(s1,2)*(17 + 17*Power(s2,2) - 4*Power(t1,2) + 39*t2 + 
               t1*(33 + 28*t2) - s2*(48 + 13*t1 + 31*t2)) + 
            s2*(-92 + 71*t2 + 15*Power(t1,2)*t2 - 18*Power(t2,2) - 
               t1*(20 + 18*t2 + 13*Power(t2,2))) + 
            s1*(-12 + 3*Power(s2,3) + 8*Power(t1,3) + 
               Power(s2,2)*(-16 + 2*t1 - 25*t2) + 
               Power(t1,2)*(-16 + t2) - 7*t2 - 36*Power(t2,2) + 
               Power(t2,3) - 6*t1*(2 + 3*t2 + 3*Power(t2,2)) + 
               s2*(1 - 13*Power(t1,2) + 54*t2 + 11*Power(t2,2) + 
                  8*t1*(4 + 3*t2)))))*R2q(1 - s2 + t1 - t2))/
     ((-1 + s1)*Power(-s + s1 - t2,2)*(-1 + s2 - t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) - 
    (8*(-24 + 28*s2 + 24*Power(s2,2) - 32*Power(s2,3) + 4*Power(s2,5) + 
         Power(s1,7)*(s2 - t1) - 28*t1 - 48*s2*t1 + 96*Power(s2,2)*t1 - 
         20*Power(s2,4)*t1 + 24*Power(t1,2) - 96*s2*Power(t1,2) + 
         40*Power(s2,3)*Power(t1,2) + 32*Power(t1,3) - 
         40*Power(s2,2)*Power(t1,3) + 20*s2*Power(t1,4) - 4*Power(t1,5) + 
         Power(s,6)*(3 + s1)*(t1 - t2) + 70*t2 + 17*s2*t2 - 
         167*Power(s2,2)*t2 + 63*Power(s2,3)*t2 + 21*Power(s2,4)*t2 - 
         4*Power(s2,5)*t2 - 11*t1*t2 + 309*s2*t1*t2 - 
         162*Power(s2,2)*t1*t2 - 87*Power(s2,3)*t1*t2 + 
         15*Power(s2,4)*t1*t2 - 142*Power(t1,2)*t2 + 
         135*s2*Power(t1,2)*t2 + 135*Power(s2,2)*Power(t1,2)*t2 - 
         20*Power(s2,3)*Power(t1,2)*t2 - 36*Power(t1,3)*t2 - 
         93*s2*Power(t1,3)*t2 + 10*Power(s2,2)*Power(t1,3)*t2 + 
         24*Power(t1,4)*t2 - Power(t1,5)*t2 - 38*Power(t2,2) - 
         195*s2*Power(t2,2) + 232*Power(s2,2)*Power(t2,2) - 
         20*Power(s2,3)*Power(t2,2) - 14*Power(s2,4)*Power(t2,2) + 
         3*Power(s2,5)*Power(t2,2) + 173*t1*Power(t2,2) - 
         397*s2*t1*Power(t2,2) + 13*Power(s2,2)*t1*Power(t2,2) + 
         53*Power(s2,3)*t1*Power(t2,2) - 10*Power(s2,4)*t1*Power(t2,2) + 
         165*Power(t1,2)*Power(t2,2) + 34*s2*Power(t1,2)*Power(t2,2) - 
         75*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         10*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         27*Power(t1,3)*Power(t2,2) + 47*s2*Power(t1,3)*Power(t2,2) - 
         11*Power(t1,4)*Power(t2,2) - 5*s2*Power(t1,4)*Power(t2,2) + 
         2*Power(t1,5)*Power(t2,2) - 60*Power(t2,3) + 
         212*s2*Power(t2,3) - 76*Power(s2,2)*Power(t2,3) - 
         8*Power(s2,3)*Power(t2,3) - 184*t1*Power(t2,3) + 
         98*s2*t1*Power(t2,3) + 40*Power(s2,2)*t1*Power(t2,3) + 
         6*Power(s2,3)*t1*Power(t2,3) - 22*Power(t1,2)*Power(t2,3) - 
         56*s2*Power(t1,2)*Power(t2,3) - 
         18*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         24*Power(t1,3)*Power(t2,3) + 18*s2*Power(t1,3)*Power(t2,3) - 
         6*Power(t1,4)*Power(t2,3) + 80*Power(t2,4) - 60*s2*Power(t2,4) - 
         16*Power(s2,2)*Power(t2,4) + 44*t1*Power(t2,4) + 
         44*s2*t1*Power(t2,4) + 4*Power(s2,2)*t1*Power(t2,4) - 
         28*Power(t1,2)*Power(t2,4) - 8*s2*Power(t1,2)*Power(t2,4) + 
         4*Power(t1,3)*Power(t2,4) - 28*Power(t2,5) - 4*s2*Power(t2,5) + 
         8*t1*Power(t2,5) + Power(s1,6)*
          (-1 + 5*Power(s2,2) + 3*Power(t1,2) + t2 + 2*t1*(1 + t2) - 
            s2*(3 + 8*t1 + t2)) - 
         Power(s,5)*(5 + 2*t1 - 10*Power(t1,2) + 
            s2*(-8 + 10*t1 - 15*t2) + 
            Power(s1,2)*(-2 + 3*s2 + 2*t1 - 3*t2) + 6*t2 + 7*t1*t2 + 
            3*Power(t2,2) + s1*
             (1 + s2 + 9*t1 + 5*s2*t1 - 5*Power(t1,2) - 12*t2 - 
               4*s2*t2 + 3*t1*t2 + 2*Power(t2,2))) + 
         Power(s1,5)*(3 + 7*Power(s2,3) + Power(t1,3) + 
            Power(t1,2)*(8 - 10*t2) + Power(s2,2)*(3 - 13*t1 - 9*t2) - 
            t2 - 2*Power(t2,2) - t1*(-13 + 13*t2 + Power(t2,2)) + 
            s2*(-14 + 5*Power(t1,2) + 16*t2 - Power(t2,2) + 
               t1*(-11 + 19*t2))) + 
         Power(s1,4)*(9 + 3*Power(s2,4) - 5*Power(t1,4) + 
            Power(s2,3)*(24 - 4*t1 - 15*t2) - 21*t2 + 11*Power(t2,2) + 
            Power(t2,3) + Power(t1,3)*(-11 + 4*t2) + 
            Power(t1,2)*(-18 - 16*t2 + 11*Power(t2,2)) + 
            t1*(-1 - 39*t2 + 26*Power(t2,2)) + 
            s2*(14 + 12*Power(t1,3) - 23*Power(t1,2)*(-2 + t2) + 
               18*t2 - 19*Power(t2,2) + Power(t2,3) + 
               t1*(69 - 5*t2 - 14*Power(t2,2))) + 
            Power(s2,2)*(-6*Power(t1,2) + t1*(-59 + 34*t2) + 
               3*(-17 + 7*t2 + Power(t2,2)))) + 
         Power(s1,3)*(-17 + Power(s2,4)*(23 - 7*t2) + 3*t2 + 
            31*Power(t2,2) - 17*Power(t2,3) + 2*Power(t1,4)*(1 + 6*t2) + 
            Power(t1,3)*(-13 + 22*t2 - 13*Power(t2,2)) + 
            t1*(-41 + 49*t2 + 40*Power(t2,2) - 27*Power(t2,3)) + 
            Power(t1,2)*(-39 + 74*t2 + 15*Power(t2,2) - 4*Power(t2,3)) + 
            Power(s2,3)*(-17 - 18*t2 + 9*Power(t2,2) + 
               t1*(-71 + 9*t2)) + 
            Power(s2,2)*(-41 + 131*t2 - 29*Power(t2,2) + Power(t2,3) + 
               15*Power(t1,2)*(5 + t2) + 
               t1*(21 + 58*t2 - 31*Power(t2,2))) + 
            s2*(52 - 94*t2 + 11*Power(t2,2) + 10*Power(t2,3) - 
               29*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(9 - 62*t2 + 35*Power(t2,2)) + 
               t1*(80 - 205*t2 + 14*Power(t2,2) + 3*Power(t2,3)))) - 
         Power(s1,2)*(8 - 2*Power(t1,6) + Power(s2,5)*(-5 + 2*t1) - 
            58*t2 + 45*Power(t2,2) + 21*Power(t2,3) - 16*Power(t2,4) + 
            Power(t1,5)*(1 + 6*t2) + 
            Power(t1,4)*(-16 - 3*t2 + 3*Power(t2,2)) + 
            Power(t1,3)*(-32 + 60*t2 + 30*Power(t2,2) - 8*Power(t2,3)) + 
            Power(t1,2)*(38 + 9*t2 - 9*Power(t2,2) - 5*Power(t2,3)) + 
            t1*(59 - 150*t2 + 49*Power(t2,2) + 62*Power(t2,3) - 
               12*Power(t2,4)) + 
            Power(s2,4)*(-39 - 10*Power(t1,2) + 32*t2 - 5*Power(t2,2) + 
               3*t1*(7 + 2*t2)) + 
            Power(s2,3)*(86 + 20*Power(t1,3) - 165*t2 + 6*Power(t2,2) + 
               Power(t2,3) - 2*Power(t1,2)*(17 + 12*t2) + 
               t1*(133 - 93*t2 + 12*Power(t2,2))) - 
            Power(s2,2)*(3 + 20*Power(t1,4) - 75*t2 + 16*Power(t2,2) + 
               9*Power(t2,3) - 2*Power(t1,3)*(13 + 18*t2) + 
               3*Power(t1,2)*(55 - 29*t2 + 2*Power(t2,2)) + 
               2*t1*(102 - 195*t2 - 9*Power(t2,2) + 5*Power(t2,3))) + 
            s2*(-47 + 10*Power(t1,5) + 140*t2 - 72*Power(t2,2) - 
               33*Power(t2,3) + 4*Power(t2,4) - 
               3*Power(t1,4)*(3 + 8*t2) + 
               Power(t1,3)*(87 - 23*t2 - 4*Power(t2,2)) + 
               t1*(-35 - 84*t2 + 25*Power(t2,2) + 14*Power(t2,3)) + 
               Power(t1,2)*(150 - 285*t2 - 54*Power(t2,2) + 
                  17*Power(t2,3)))) - 
         s1*(30 + 4*Power(s2,6) - 62*t2 + 29*Power(t2,2) + 
            39*Power(t2,3) - 44*Power(t2,4) + 8*Power(t2,5) + 
            2*Power(t1,6)*(2 + t2) - 
            Power(t1,5)*(5 + 15*t2 + 6*Power(t2,2)) + 
            Power(t1,4)*(-24 + 13*t2 + 23*Power(t2,2) + 4*Power(t2,3)) - 
            Power(t1,3)*(36 - 117*t2 + 49*Power(t2,2) + 
               31*Power(t2,3)) + 
            t1*(41 - 22*t2 - 67*Power(t2,2) + 87*Power(t2,3) - 
               36*Power(t2,4)) + 
            Power(t1,2)*(-10 + 127*t2 - 174*Power(t2,2) + 
               29*Power(t2,3) + 16*Power(t2,4)) - 
            2*Power(s2,5)*(-12*t2 + t1*(12 + t2)) + 
            Power(s2,4)*(-27 + 33*t2 + 15*Power(t2,2) + Power(t2,3) + 
               10*Power(t1,2)*(6 + t2) - t1*(5 + 111*t2 + 6*Power(t2,2))\
) + Power(s2,3)*(63 - 218*t2 + 140*Power(t2,2) + 16*Power(t2,3) - 
               20*Power(t1,3)*(4 + t2) + 
               4*Power(t1,2)*(5 + 51*t2 + 6*Power(t2,2)) - 
               t1*(-105 + 112*t2 + 68*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s2,2)*(-35 + 235*t2 - 296*Power(t2,2) + 
               72*Power(t2,3) + 8*Power(t2,4) + 
               20*Power(t1,4)*(3 + t2) - 
               6*Power(t1,3)*(5 + 31*t2 + 6*Power(t2,2)) + 
               3*Power(t1,2)*
                (-51 + 46*t2 + 38*Power(t2,2) + 5*Power(t2,3)) - 
               t1*(162 - 553*t2 + 329*Power(t2,2) + 63*Power(t2,3))) + 
            s2*(-35 - 12*t2 + 116*Power(t2,2) - 112*Power(t2,3) + 
               40*Power(t2,4) - 2*Power(t1,5)*(12 + 5*t2) + 
               4*Power(t1,4)*(5 + 21*t2 + 6*Power(t2,2)) - 
               Power(t1,3)*(-99 + 72*t2 + 84*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(t1,2)*(135 - 452*t2 + 238*Power(t2,2) + 
                  78*Power(t2,3)) + 
               t1*(45 - 362*t2 + 470*Power(t2,2) - 101*Power(t2,3) - 
                  24*Power(t2,4)))) + 
         Power(s,3)*(3 + 11*t1 - 35*Power(t1,2) - 29*Power(t1,3) + 
            14*Power(t1,4) + 5*t2 - 31*t1*t2 + 10*Power(t1,2)*t2 - 
            8*Power(t1,3)*t2 - 16*Power(t2,2) - 45*t1*Power(t2,2) - 
            16*Power(t1,2)*Power(t2,2) - 16*Power(t2,3) + 
            10*t1*Power(t2,3) - 2*Power(s1,4)*(-6 + 11*s2 - 6*t1 + t2) + 
            Power(s2,3)*(24 - 14*t1 + 30*t2) + 
            Power(s2,2)*(-4 - 77*t1 + 42*Power(t1,2) - 36*t2 - 
               68*t1*t2 - 30*Power(t2,2)) + 
            2*Power(s1,3)*(-4 - 16*Power(s2,2) + 6*Power(t1,2) + t2 + 
               2*s2*(6 + 5*t1 + 7*t2) - t1*(7 + 16*t2)) + 
            Power(s1,2)*(-28 - 9*Power(s2,3) - 24*Power(t1,3) - 12*t2 - 
               3*Power(t2,2) + Power(t2,3) + Power(t1,2)*(4 + 3*t2) + 
               Power(s2,2)*(-41 - 6*t1 + 33*t2) + 
               s2*(100 + 39*Power(t1,2) + t1*(37 - 36*t2) + 16*t2 - 
                  17*Power(t2,2)) + t1*(-54 - 33*t2 + 30*Power(t2,2))) + 
            s2*(-23 - 42*Power(t1,3) + 38*t2 + 64*Power(t2,2) + 
               Power(t1,2)*(82 + 46*t2) + 
               t1*(39 + 26*t2 + 46*Power(t2,2))) + 
            s1*(22 + Power(t1,4) - Power(s2,3)*(26 + t1 - 4*t2) - 
               18*t2 + 66*Power(t2,2) - 8*Power(t2,3) + 
               Power(t1,3)*(-37 + 21*t2) + 
               Power(t1,2)*(49 + 62*t2 - 15*Power(t2,2)) + 
               t1*(101 + 39*t2 + 13*Power(t2,2) - 7*Power(t2,3)) + 
               Power(s2,2)*(108 + 3*Power(t1,2) + 88*t2 - 
                  12*Power(t2,2) + t1*(15 + 13*t2)) - 
               s2*(100 + 3*Power(t1,3) + 88*t2 + 24*Power(t2,2) - 
                  4*Power(t2,3) + Power(t1,2)*(-48 + 38*t2) + 
                  t1*(157 + 150*t2 - 27*Power(t2,2))))) + 
         Power(s,4)*(-3 - 32*t1 - 15*Power(t1,2) + 16*Power(t1,3) - t2 - 
            3*t1*t2 - 12*Power(t1,2)*t2 - 22*Power(t2,2) - 
            4*t1*Power(t2,2) + Power(s1,3)*(13*s2 - 3*t1 - 2*(4 + t2)) + 
            Power(s1,2)*(5 + 9*Power(s2,2) - 15*Power(t1,2) + 
               s2*(-5 + 6*t1 - 17*t2) - 15*t2 + 4*Power(t2,2) + 
               4*t1*(3 + 4*t2)) + 2*Power(s2,2)*(8*t1 - 3*(4 + 5*t2)) + 
            s2*(26 - 32*Power(t1,2) + 24*t2 + 15*Power(t2,2) + 
               t1*(39 + 42*t2)) + 
            s1*(15 + 7*Power(t1,3) + Power(s2,2)*(12 + 7*t1 - 6*t2) + 
               23*t2 + 5*Power(t2,2) - Power(t2,3) + 
               Power(t1,2)*(-26 + 5*t2) + 
               t1*(13 + 36*t2 - 11*Power(t2,2)) + 
               s2*(-14*Power(t1,2) + t1*(14 + t2) + 
                  8*(-5 - 6*t2 + Power(t2,2))))) + 
         Power(s,2)*(-13 + 11*t1 + 62*Power(t1,2) + 12*Power(t1,3) - 
            21*Power(t1,4) + 5*Power(t1,5) + 
            Power(s2,4)*(-4 + 5*t1 - 15*t2) - 2*t2 - 64*t1*t2 - 
            105*Power(t1,2)*t2 - 4*Power(t1,3)*t2 + 7*Power(t1,4)*t2 + 
            68*Power(t2,2) + 63*t1*Power(t2,2) - 
            3*Power(t1,2)*Power(t2,2) - 30*Power(t1,3)*Power(t2,2) - 
            68*Power(t2,3) - 56*t1*Power(t2,3) + 
            14*Power(t1,2)*Power(t2,3) + 4*Power(t2,4) + 
            4*t1*Power(t2,4) + Power(s1,5)*(-8 + 18*s2 - 13*t1 + 3*t2) + 
            Power(s2,3)*(-50 + 33*t1 - 20*Power(t1,2) + 40*t2 + 
               38*t1*t2 + 30*Power(t2,2)) + 
            Power(s1,4)*(4 + 42*Power(s2,2) + 4*Power(t1,2) + 9*t2 - 
               4*Power(t2,2) + 15*t1*(1 + 2*t2) - 
               2*s2*(16 + 23*t1 + 11*t2)) + 
            Power(s2,2)*(75 + 30*Power(t1,3) - 88*t2 - 36*Power(t2,2) - 
               3*Power(t1,2)*(25 + 8*t2) - 
               2*t1*(-56 + 42*t2 + 45*Power(t2,2))) + 
            s2*(-8 - 20*Power(t1,4) + Power(t1,3)*(67 - 6*t2) + 41*t2 - 
               44*Power(t2,2) + 64*Power(t2,3) + 
               Power(t1,2)*(-74 + 48*t2 + 90*Power(t2,2)) + 
               t1*(-137 + 193*t2 + 39*Power(t2,2) - 14*Power(t2,3))) + 
            Power(s1,3)*(34 + 25*Power(s2,3) + 28*Power(t1,3) + 
               Power(s2,2)*(49 - 22*t1 - 57*t2) + 
               Power(t1,2)*(38 - 31*t2) - 22*t2 + Power(t2,2) + 
               Power(t2,3) + t1*(90 - 27*t2 - 28*Power(t2,2)) + 
               s2*(-126 - 31*Power(t1,2) + 68*t2 + 9*Power(t2,2) + 
                  t1*(-87 + 88*t2))) + 
            Power(s1,2)*(-16 + 3*Power(s2,4) - 9*Power(t1,4) + 
               Power(t1,3)*(11 - 36*t2) + Power(s2,3)*(82 - 27*t2) - 
               16*t2 - 23*Power(t2,2) + 9*Power(t2,3) + 
               3*Power(t1,2)*(-21 - 34*t2 + 15*Power(t2,2)) + 
               t1*(-87 - 142*t2 + 24*Power(t2,2) + 10*Power(t2,3)) - 
               Power(s2,2)*(197 + 18*Power(t1,2) + 51*t2 - 
                  27*Power(t2,2) - 9*t1*(-17 + 2*t2)) + 
               s2*(122 + 24*Power(t1,3) + 146*t2 - 24*Power(t2,2) - 
                  3*Power(t2,3) + 15*Power(t1,2)*(4 + 3*t2) + 
                  t1*(260 + 153*t2 - 72*Power(t2,2)))) + 
            s1*(-3 - 4*Power(t1,5) + 22*t2 - 28*Power(t2,2) + 
               56*Power(t2,3) - 8*Power(t2,4) - 
               Power(s2,4)*(-16 + 4*t1 + t2) + 
               2*Power(t1,4)*(-14 + 9*t2) + 
               Power(t1,3)*(35 + 47*t2 + Power(t2,2)) + 
               t1*(-41 + 166*t2 + 62*Power(t2,2) - 17*Power(t2,3)) + 
               Power(t1,2)*(21 + 83*t2 + 36*Power(t2,2) - 
                  15*Power(t2,3)) + 
               Power(s2,2)*(-50 - 24*Power(t1,3) + 258*t2 + 
                  18*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t1,2)*(-36 + 51*t2) + 
                  t1*(107 + 255*t2 - 15*Power(t2,2))) + 
               Power(s2,3)*(16*Power(t1,2) - 5*t1*(4 + 3*t2) + 
                  4*(-9 - 26*t2 + 2*Power(t2,2))) + 
               s2*(73 + 16*Power(t1,4) + Power(t1,3)*(68 - 53*t2) - 
                  234*t2 - 48*Power(t2,2) + 
                  2*Power(t1,2)*(-53 - 99*t2 + 3*Power(t2,2)) + 
                  t1*(29 - 341*t2 - 54*Power(t2,2) + 21*Power(t2,3))))) + 
         s*(42 + 37*t1 - 46*Power(t1,2) - 36*Power(t1,3) + 
            4*Power(t1,4) - Power(t1,5) - 75*t2 + 88*t1*t2 + 
            227*Power(t1,2)*t2 + 25*Power(t1,3)*t2 - 32*Power(t1,4)*t2 + 
            7*Power(t1,5)*t2 - 33*Power(t2,2) - 259*t1*Power(t2,2) - 
            132*Power(t1,2)*Power(t2,2) + 49*Power(t1,3)*Power(t2,2) - 
            13*Power(t1,4)*Power(t2,2) + 140*Power(t2,3) + 
            126*t1*Power(t2,3) - 56*Power(t1,2)*Power(t2,3) - 
            2*Power(t1,3)*Power(t2,3) - 80*Power(t2,4) - 
            4*t1*Power(t2,4) + 8*Power(t1,2)*Power(t2,4) + 
            4*Power(t2,5) - Power(s1,6)*(-2 + 7*s2 - 6*t1 + t2) + 
            Power(s2,5)*(-4 + 3*t2) + 
            Power(s2,4)*(1 - 5*t1*(-3 + t2) - 18*t2 - 15*Power(t2,2)) + 
            Power(s1,5)*(1 - 24*Power(s2,2) - 9*Power(t1,2) - 6*t2 + 
               2*Power(t2,2) + s2*(17 + 33*t1 + 8*t2) - t1*(9 + 13*t2)) + 
            Power(s1,4)*(-19 - 23*Power(s2,3) - 12*Power(t1,3) + 18*t2 + 
               2*Power(t2,2) - Power(t2,3) + Power(t1,2)*(-34 + 33*t2) + 
               Power(s2,2)*(-23 + 34*t1 + 39*t2) + 
               s2*(72 + Power(t1,2) + t1*(57 - 72*t2) - 67*t2 + 
                  Power(t2,2)) + 2*t1*(-30 + 22*t2 + 5*Power(t2,2))) + 
            Power(s2,3)*(63 - 110*t2 + 8*Power(t2,2) - 
               10*Power(t1,2)*(2 + t2) + t1*(-7 + 86*t2 + 58*Power(t2,2))\
) + Power(s2,2)*(-71 + 307*t2 - 200*Power(t2,2) - 40*Power(t2,3) + 
               10*Power(t1,3)*(1 + 3*t2) - 
               3*Power(t1,2)*(-5 + 50*t2 + 28*Power(t2,2)) + 
               t1*(-162 + 245*t2 + 33*Power(t2,2) - 2*Power(t2,3))) + 
            s2*(-31 - 107*t2 - 25*Power(t1,4)*t2 + 276*Power(t2,2) - 
               136*Power(t2,3) + 12*Power(t2,4) + 
               Power(t1,3)*(-13 + 114*t2 + 54*Power(t2,2)) + 
               Power(t1,2)*(135 - 160*t2 - 90*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(117 - 534*t2 + 332*Power(t2,2) + 96*Power(t2,3) - 
                  8*Power(t2,4))) - 
            Power(s1,3)*(6*Power(s2,4) - 13*Power(t1,4) + 
               Power(s2,3)*(80 - 5*t1 - 38*t2) - 
               Power(t1,3)*(21 + 11*t2) + 
               Power(s2,2)*(-164 - 181*t1 - 21*Power(t1,2) + 28*t2 + 
                  65*t1*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(-47 - 68*t2 + 41*Power(t2,2)) + 
               2*(6 - 28*t2 + 16*Power(t2,2) + Power(t2,3)) + 
               t1*(-19 - 145*t2 + 59*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(33*Power(t1,3) - 2*Power(t1,2)*(-61 + 8*t2) + 
                  t1*(211 + 40*t2 - 59*Power(t2,2)) + 
                  2*(31 + 50*t2 - 26*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,2)*(5 + 4*Power(t1,5) + Power(t1,4)*(8 - 32*t2) - 
               12*t2 + 5*Power(t2,2) - 17*Power(t2,3) + 4*Power(t2,4) + 
               Power(s2,4)*(-43 + 4*t1 + 8*t2) + 
               Power(t1,3)*(15 - 49*t2 + 16*Power(t2,2)) + 
               Power(t1,2)*(69 - 183*t2 - 51*Power(t2,2) + 
                  17*Power(t2,3)) + 
               t1*(63 - 196*t2 - 53*Power(t2,2) + 46*Power(t2,3)) + 
               Power(s2,3)*(21 - 16*Power(t1,2) + 82*t2 - 
                  19*Power(t2,2) + t1*(121 + 8*t2)) + 
               Power(s2,2)*(111 + 24*Power(t1,3) - 379*t2 + 
                  33*Power(t2,2) + 3*Power(t2,3) - 
                  3*Power(t1,2)*(35 + 24*t2) + 
                  3*t1*(-9 - 71*t2 + 18*Power(t2,2))) - 
               s2*(94 + 16*Power(t1,4) - 308*t2 + 41*Power(t2,2) + 
                  18*Power(t2,3) - Power(t1,3)*(19 + 88*t2) + 
                  3*Power(t1,2)*(3 - 60*t2 + 17*Power(t2,2)) + 
                  2*t1*(90 - 281*t2 - 9*Power(t2,2) + 10*Power(t2,3)))) + 
            s1*(9 - 2*Power(t1,6) + Power(s2,5)*(3 + 2*t1) + 
               2*Power(t1,5)*(-6 + t2) - 16*t2 - 8*Power(t2,2) + 
               8*Power(t2,3) + 16*Power(t2,4) - 
               Power(s2,4)*(23 + 10*Power(t1,2) - 2*t1*(-12 + t2) - 
                  76*t2 + 2*Power(t2,2)) + 
               Power(t1,4)*(17 + 24*t2 + 13*Power(t2,2)) + 
               Power(t1,3)*(-4 + 14*t2 + 5*Power(t2,2) - 
                  13*Power(t2,3)) + 
               Power(t1,2)*(-24 + 85*Power(t2,2) + 22*Power(t2,3)) + 
               t1*(16 - 72*t2 + 125*Power(t2,2) + 27*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(s2,3)*(20*Power(t1,3) + Power(t1,2)*(66 - 8*t2) + 
                  t1*(52 - 252*t2 - 7*Power(t2,2)) + 
                  4*(24 - 34*t2 + 4*Power(t2,2) + Power(t2,3))) + 
               Power(s2,2)*(-20*Power(t1,4) + 12*Power(t1,3)*(-7 + t2) + 
                  3*Power(t1,2)*(-6 + 100*t2 + 11*Power(t2,2)) + 
                  6*(-13 + 7*t2 + 23*Power(t2,2) + 4*Power(t2,3)) - 
                  t1*(196 - 286*t2 + 27*Power(t2,2) + 21*Power(t2,3))) + 
               s2*(-7 + 10*Power(t1,5) + Power(t1,4)*(51 - 8*t2) + 
                  82*t2 - 172*Power(t2,2) + 16*Power(t2,4) - 
                  Power(t1,3)*(28 + 148*t2 + 37*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (52 - 82*t2 + 3*Power(t2,2) + 15*Power(t2,3)) - 
                  t1*(-102 + 42*t2 + 223*Power(t2,2) + 46*Power(t2,3))))))*
       T2q(1 - s + s1 - t2,1 - s2 + t1 - t2))/
     ((-1 + s1)*Power(-s + s1 - t2,2)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       (-1 + s2 - t1 + t2)*(-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2) + 2*s2 - 2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 
         2*s*t1 - 2*s1*t1 - 2*s2*t1 + Power(t1,2) + 4*t2)) - 
    (8*(Power(s1,4)*(s2 - t1)*
          (2*Power(s2,2) - 4*s2*t1 + 2*Power(t1,2) - Power(-1 + t2,2)) - 
         Power(s,5)*(3 + s1)*(t1 - t2) + 
         Power(s,4)*(-7 + 3*t1 - 3*Power(t1,2) + s2*(4 + 3*t1 - 4*t2) + 
            t2 - 8*t1*t2 + 11*Power(t2,2) - 
            Power(s1,2)*(-2 + s2 - 3*t1 + 4*t2) + 
            s1*(9 + 15*t1 - 14*t2 - 2*t1*t2 + 2*Power(t2,2) - 
               s2*(7 + 3*t2))) + 
         Power(s1,3)*(6*Power(t1,2)*(-1 + t2) - Power(-1 + t2,3) - 
            6*Power(s2,3)*t2 - t1*Power(-1 + t2,2)*(7 + 2*t2) + 
            Power(t1,3)*(2 + 4*t2) + 
            2*Power(s2,2)*(-4 + t1 + 5*t2 + 8*t1*t2 - Power(t2,2)) + 
            s2*(Power(-1 + t2,2)*(8 + t2) - 2*Power(t1,2)*(2 + 7*t2) + 
               2*t1*(7 - 8*t2 + Power(t2,2)))) + 
         Power(s1,2)*(2*Power(-1 + t2,3)*(4 + t2) + 
            2*Power(s2,3)*t2*(-4 + 3*t2) + 
            Power(t1,3)*(2 + 4*t2 - 4*Power(t2,2)) + 
            t1*Power(-1 + t2,2)*(-26 + 6*t2 + Power(t2,2)) + 
            Power(t1,2)*(-19 + 30*t2 - 13*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s2,2)*(-23 + 40*t2 - 21*Power(t2,2) + 4*Power(t2,3) + 
               t1*(2 + 20*t2 - 16*Power(t2,2))) + 
            s2*(Power(-1 + t2,2)*(30 - 12*t2 + Power(t2,2)) + 
               2*Power(t1,2)*(-2 - 8*t2 + 7*Power(t2,2)) + 
               t1*(42 - 70*t2 + 34*Power(t2,2) - 6*Power(t2,3)))) + 
         (-1 + t2)*(-8*Power(-1 + t2,2)*(-3 + 4*t2) + 
            Power(t1,2)*(12 - 9*t2 + 5*Power(t2,2) - 2*Power(t2,3)) + 
            Power(s2,2)*(12 - 8*t2 + Power(t2,2) + Power(t2,3)) + 
            t1*(36 - 75*t2 + 38*Power(t2,2) - Power(t2,3) + 
               2*Power(t2,4)) + 
            s2*(-36 + 75*t2 - 38*Power(t2,2) + Power(t2,3) - 
               2*Power(t2,4) + 
               t1*(-24 + 17*t2 - 6*Power(t2,2) + Power(t2,3)))) - 
         s1*(t1*Power(-1 + t2,2)*(47 - 37*t2 + Power(t2,2)) + 
            Power(-1 + t2,3)*(-24 + 8*t2 + Power(t2,2)) + 
            2*Power(s2,3)*(-6 + 4*t2 - 2*Power(t2,2) + Power(t2,3)) - 
            2*Power(t1,3)*(-6 + 3*t2 - Power(t2,2) + Power(t2,3)) + 
            Power(t1,2)*(37 - 81*t2 + 47*Power(t2,2) - 5*Power(t2,3) + 
               2*Power(t2,4)) + 
            2*Power(s2,2)*(18 - 40*t2 + 24*Power(t2,2) - 
               3*Power(t2,3) + Power(t2,4) + 
               t1*(18 - 11*t2 + 5*Power(t2,2) - 3*Power(t2,3))) + 
            s2*(Power(-1 + t2,2)*
                (-47 + 41*t2 - 6*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-36 + 20*t2 - 8*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-73 + 161*t2 - 95*Power(t2,2) + 11*Power(t2,3) - 
                  4*Power(t2,4)))) + 
         Power(s,3)*(44 - 37*s2 + 32*t1 - 7*s2*t1 + 7*Power(t1,2) - 
            Power(s1,3)*(2 + t1 - 3*t2) - 60*t2 + 15*s2*t2 + 
            Power(s2,2)*t2 + 6*t1*t2 + 10*s2*t1*t2 - 11*Power(t1,2)*t2 + 
            3*Power(t2,2) - 14*s2*Power(t2,2) - 4*t1*Power(t2,2) + 
            15*Power(t2,3) + Power(s1,2)*
             (-28 - 2*Power(s2,2) + 34*t2 - 6*Power(t2,2) + 
               t1*(-31 + 2*t2) + s2*(25 + 2*t1 + 4*t2)) + 
            s1*(-15 + 42*t2 - 32*Power(t2,2) + 2*Power(t2,3) + 
               4*Power(t1,2)*(3 + t2) + Power(s2,2)*(9 + 4*t2) + 
               t1*(-22 + 24*t2 - 6*Power(t2,2)) + 
               s2*(6 + 5*t2 - 4*Power(t2,2) - t1*(21 + 8*t2)))) + 
         Power(s,2)*(-81 + Power(s1,4)*(s2 - t1) - 83*t1 - 
            13*Power(t1,2) + 199*t2 + 101*t1*t2 + 21*Power(t1,2)*t2 - 
            130*Power(t2,2) - 15*Power(t1,2)*Power(t2,2) + 
            3*Power(t2,3) + 6*t1*Power(t2,3) + 9*Power(t2,4) + 
            Power(s2,2)*(-8 + 3*Power(t2,2)) + 
            Power(s1,3)*(23 + 6*Power(s2,2) + 25*t1 + 4*Power(t1,2) - 
               25*t2 + 2*Power(t2,2) - s2*(24 + 10*t1 + t2)) + 
            3*s2*(28 - 37*t2 + 7*Power(t2,2) - 6*Power(t2,3) + 
               t1*(7 - 7*t2 + 4*Power(t2,2))) + 
            Power(s1,2)*(30 + 2*Power(s2,3) - 74*t2 + 48*Power(t2,2) - 
               4*Power(t2,3) - Power(t1,2)*(21 + 10*t2) - 
               Power(s2,2)*(21 + 4*t1 + 12*t2) + 
               t1*(15 - 30*t2 + 8*Power(t2,2)) + 
               s2*(-5 + 2*Power(t1,2) + 12*t2 + t1*(42 + 22*t2))) + 
            s1*(-25 - 2*Power(s2,3)*t2 + 50*Power(t2,2) - 
               27*Power(t2,3) + 2*Power(t2,4) + 2*Power(t1,3)*(1 + t2) + 
               2*Power(s2,2)*
                (-17 + t1 + 6*t2 + 3*t1*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-37 + 17*t2 + 6*Power(t2,2)) - 
               t1*(38 + 7*t2 - 14*Power(t2,2) + 10*Power(t2,3)) - 
               s2*(-52 + 38*t2 - 27*Power(t2,2) + 
                  Power(t1,2)*(4 + 6*t2) + 
                  t1*(-71 + 29*t2 + 12*Power(t2,2))))) + 
         s*(68 - 87*s2 + 20*Power(s2,2) + 87*t1 - 41*s2*t1 + 
            21*Power(t1,2) - 237*t2 + 197*s2*t2 - 17*Power(s2,2)*t2 - 
            196*t1*t2 + 44*s2*t1*t2 - 27*Power(t1,2)*t2 + 
            275*Power(t2,2) - 113*s2*Power(t2,2) + 108*t1*Power(t2,2) - 
            21*s2*t1*Power(t2,2) + 21*Power(t1,2)*Power(t2,2) - 
            109*Power(t2,3) + 13*s2*Power(t2,3) + 
            3*Power(s2,2)*Power(t2,3) - 6*t1*Power(t2,3) + 
            6*s2*t1*Power(t2,3) - 9*Power(t1,2)*Power(t2,3) + 
            Power(t2,4) - 10*s2*Power(t2,4) + 7*t1*Power(t2,4) + 
            2*Power(t2,5) - 2*Power(s1,4)*(s2 - t1)*
             (-1 + 2*s2 - 2*t1 + t2) + 
            Power(s1,3)*(-4*Power(s2,3) + 2*Power(t1,3) + 
               (-12 + t2)*Power(-1 + t2,2) + 10*Power(t1,2)*(2 + t2) + 
               10*Power(s2,2)*(2 + t1 + t2) - 7*t1*(-1 + Power(t2,2)) - 
               2*s2*(4 + 4*Power(t1,2) - t2 - 3*Power(t2,2) + 
                  10*t1*(2 + t2))) + 
            Power(s1,2)*(-4*Power(t1,3)*t2 + Power(s2,3)*(-4 + 8*t2) + 
               Power(t1,2)*(44 - 22*t2 - 8*Power(t2,2)) + 
               Power(s2,2)*(50 + t1*(8 - 20*t2) - 30*t2 - 
                  6*Power(t2,2)) - 
               2*Power(-1 + t2,2)*(-2 - 9*t2 + Power(t2,2)) + 
               t1*(41 - 44*t2 - 7*Power(t2,2) + 10*Power(t2,3)) + 
               s2*(-51 + 70*t2 - 15*Power(t2,2) - 4*Power(t2,3) + 
                  4*Power(t1,2)*(-1 + 4*t2) + 
                  2*t1*(-47 + 26*t2 + 7*Power(t2,2)))) + 
            s1*(-4*Power(s2,3)*(2 - t2 + Power(t2,2)) + 
               Power(t1,3)*(6 + 4*Power(t2,2)) + 
               2*Power(t1,2)*(22 - 42*t2 + 5*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (45 - 5*t2 - 8*Power(t2,2) + Power(t2,3)) + 
               t1*(69 - 122*t2 + 54*Power(t2,2) + 4*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,2)*(43 - 82*t2 + 9*Power(t2,2) + 
                  2*t1*(11 - 4*t2 + 6*Power(t2,2))) - 
               s2*(74 - 149*t2 + 98*Power(t2,2) - 23*Power(t2,3) + 
                  4*Power(t1,2)*(5 - t2 + 3*Power(t2,2)) + 
                  t1*(87 - 166*t2 + 19*Power(t2,2))))))*
       T3q(1 - s + s1 - t2,s1))/
     ((-1 + s1)*Power(-s + s1 - t2,2)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       (-1 + s + t2)*(-1 + s2 - t1 + t2)) - 
    (8*(24 - 36*s2 + 12*Power(s2,2) + 36*t1 - 24*s2*t1 + 12*Power(t1,2) + 
         Power(s1,5)*(-s2 + t1) + 
         Power(s,3)*(-3 + 2*s1 + Power(s1,2))*(t1 - t2) - 58*t2 + 
         59*s2*t2 - 12*Power(s2,2)*t2 - 57*t1*t2 + 21*s2*t1*t2 - 
         9*Power(t1,2)*t2 + 38*Power(t2,2) - 15*s2*Power(t2,2) - 
         3*Power(s2,2)*Power(t2,2) + 13*t1*Power(t2,2) + 
         9*s2*t1*Power(t2,2) - 6*Power(t1,2)*Power(t2,2) - 
         4*Power(t2,3) - 6*s2*Power(t2,3) + 6*t1*Power(t2,3) + 
         Power(s1,4)*(1 - 2*Power(s2,2) - (1 + 2*t1)*t2 + 
            s2*(1 + 2*t1 + t2)) - 
         Power(s1,3)*(1 + 2*Power(t1,3) + 
            Power(s2,2)*(1 + 2*t1 - 4*t2) + t2 - 2*Power(t2,2) - 
            Power(t1,2)*(1 + 2*t2) + t1*(1 + 4*t2 - Power(t2,2)) + 
            s2*(-4 - 4*Power(t1,2) + t2 + 6*t1*t2 - Power(t2,2))) + 
         Power(s,2)*(-7 - 3*t1 - 3*Power(t1,2) + 
            Power(s1,3)*(2 - 3*s2 + t1) + s2*(4 + 3*t1) + 3*t2 - 
            6*t1*t2 + 9*Power(t2,2) + 
            s1*(4 + s2 + 8*t1 - 7*s2*t1 + 7*Power(t1,2) - 11*t2 + 
               5*s2*t2 - 6*t1*t2 - Power(t2,2)) + 
            Power(s1,2)*(1 + 2*t1 + 4*Power(t1,2) - 4*t1*t2 + 
               s2*(-2 - 4*t1 + 3*t2))) - 
         s1*(-2 + 12*Power(s2,3) - 4*t2 + 3*Power(t2,2) + 
            3*Power(t2,3) - 6*Power(t1,3)*(2 + t2) + 
            Power(t1,2)*(-37 + 12*t2 + 8*Power(t2,2)) + 
            t1*(-29 + 32*t2 - 2*Power(t2,3)) + 
            Power(s2,2)*(-40 + 24*t2 + 3*Power(t2,2) - 6*t1*(6 + t2)) + 
            s2*(31 - 36*t2 + 3*Power(t2,2) + Power(t2,3) + 
               12*Power(t1,2)*(3 + t2) + t1*(77 - 36*t2 - 11*Power(t2,2))\
)) - Power(s1,2)*(2 + 4*Power(s2,3) - 2*Power(t1,3)*(-1 + t2) - 
            3*Power(t2,2) + Power(t2,3) + 
            t1*(1 - 7*t2 - 2*Power(t2,2)) + 
            Power(t1,2)*(2 - 3*t2 + 2*Power(t2,2)) - 
            Power(s2,2)*(7 - 2*Power(t2,2) + 2*t1*(3 + t2)) + 
            s2*(1 + 7*t2 + 4*Power(t1,2)*t2 - Power(t2,2) + 
               Power(t2,3) + t1*(5 + 3*t2 - 4*Power(t2,2)))) + 
         s*(-22 - 33*t1 - 9*Power(t1,2) + 19*t2 + 10*t1*t2 - 
            9*Power(t1,2)*t2 - Power(t2,2) + 3*t1*Power(t2,2) + 
            6*Power(t2,3) - 3*Power(s2,2)*(4 + t2) + 
            Power(s1,4)*(-2 + 4*s2 - 3*t1 + t2) + 
            Power(s1,3)*(2 + 2*Power(s2,2) - 4*Power(t1,2) + 
               s2*(-3 + 2*t1 - 6*t2) + t2 + 8*t1*t2 - 2*Power(t2,2)) + 
            s2*(35 - 11*t2 - 6*Power(t2,2) + 3*t1*(7 + 4*t2)) + 
            Power(s1,2)*(-3 + 2*Power(t1,3) + 
               Power(s2,2)*(9 + 2*t1 - 2*t2) + 5*t2 + 2*Power(t1,2)*t2 + 
               Power(t2,3) + t1*(4 + 4*t2 - 5*Power(t2,2)) + 
               s2*(-13 - 9*t1 - 4*Power(t1,2) + 5*t2 + 2*Power(t2,2))) + 
            s1*(33 + 6*Power(t1,3) + Power(s2,2)*(9 + 6*t1 - 3*t2) - 
               Power(t1,2)*(-21 + t2) - 34*t2 - 5*Power(t2,2) + 
               Power(t2,3) + t1*(40 - 22*t2 - 6*Power(t2,2)) + 
               s2*(-39 - 12*Power(t1,2) + 28*t2 + 4*Power(t2,2) + 
                  t1*(-30 + 4*t2)))))*T4q(s1))/
     ((-1 + s1)*Power(-s + s1 - t2,2)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       (-1 + s2 - t1 + t2)) - (8*
       (-16 + 36*s2 - 4*Power(s2,3) - 36*t1 + 12*Power(s2,2)*t1 - 
         12*s2*Power(t1,2) + 4*Power(t1,3) + 54*t2 - 45*s2*t2 - 
         17*Power(s2,2)*t2 + 4*Power(s2,3)*t2 + 43*t1*t2 + 27*s2*t1*t2 - 
         7*Power(s2,2)*t1*t2 - 10*Power(t1,2)*t2 + 2*s2*Power(t1,2)*t2 + 
         Power(t1,3)*t2 - 56*Power(t2,2) - 4*s2*Power(t2,2) + 
         20*Power(s2,2)*Power(t2,2) - 3*Power(s2,3)*Power(t2,2) + 
         8*t1*Power(t2,2) - 28*s2*t1*Power(t2,2) + 
         4*Power(s2,2)*t1*Power(t2,2) + 8*Power(t1,2)*Power(t2,2) + 
         s2*Power(t1,2)*Power(t2,2) - 2*Power(t1,3)*Power(t2,2) + 
         14*Power(t2,3) + 11*s2*Power(t2,3) - Power(s2,2)*Power(t2,3) - 
         13*t1*Power(t2,3) - 3*s2*t1*Power(t2,3) + 
         4*Power(t1,2)*Power(t2,3) + 4*Power(t2,4) + 2*s2*Power(t2,4) - 
         2*t1*Power(t2,4) + Power(s1,4)*(s2 - t1)*(-1 + s2 - t1 + t2) + 
         Power(s1,3)*(2*Power(s2,3) + Power(-1 + t2,2) + 
            Power(s2,2)*(-10 - 4*t1 + t2) - Power(t1,2)*(7 + 2*t2) + 
            2*t1*(-1 + Power(t2,2)) + 
            s2*(3 + 2*Power(t1,2) - 2*t2 - Power(t2,2) + t1*(17 + t2))) + 
         Power(s,3)*(16 + 8*Power(s1,2) - t1 - 5*Power(t1,2) + 
            s2*(-8 + 5*t1 - 5*t2) + t2 + 10*t1*t2 - 5*Power(t2,2) + 
            s1*(-24 - 3*t1 + Power(t1,2) + 3*t2 - 2*t1*t2 + Power(t2,2) + 
               s2*(8 - t1 + t2))) + 
         Power(s1,2)*(-2*Power(t1,4) + Power(s2,3)*(-9 + 2*t1 - 4*t2) - 
            2*Power(-1 + t2,2)*(2 + t2) + Power(t1,3)*(7 + 4*t2) - 
            Power(t1,2)*(-13 + 5*t2 + Power(t2,2)) - 
            t1*(28 - 24*t2 - 5*Power(t2,2) + Power(t2,3)) + 
            Power(s2,2)*(5 - 6*Power(t1,2) + 7*t2 - 5*Power(t2,2) + 
               t1*(25 + 12*t2)) + 
            s2*(32 + 6*Power(t1,3) - 34*t2 + 3*Power(t2,2) - 
               Power(t2,3) - Power(t1,2)*(23 + 12*t2) + 
               2*t1*(-9 - t2 + 3*Power(t2,2)))) + 
         s1*(4*Power(s2,4) + 2*Power(t1,4)*(2 + t2) + 
            Power(-1 + t2,2)*(-22 + Power(t2,2)) - 
            Power(t1,3)*(5 + 13*t2 + 4*Power(t2,2)) + 
            Power(t1,2)*(-34 + 3*t2 + 12*Power(t2,2) + 2*Power(t2,3)) - 
            t1*(7 - 24*t2 + 13*Power(t2,2) + 4*Power(t2,3)) - 
            2*Power(s2,3)*(t1*(8 + t2) - t2*(10 + t2)) + 
            Power(s2,2)*(-27 - t2 + 8*Power(t2,2) + 3*Power(t2,3) + 
               6*Power(t1,2)*(4 + t2) - t1*(5 + 53*t2 + 8*Power(t2,2))) + 
            s2*(9 - 32*t2 + 24*Power(t2,2) - 2*Power(t2,3) + 
               Power(t2,4) - 2*Power(t1,3)*(8 + 3*t2) + 
               2*Power(t1,2)*(5 + 23*t2 + 5*Power(t2,2)) - 
               t1*(-61 + 2*t2 + 20*Power(t2,2) + 5*Power(t2,3)))) + 
         Power(s,2)*(-13 - 8*Power(s1,3) + 2*t1 + 6*Power(t1,2) - 
            5*Power(t1,3) + 46*t2 - 8*t1*t2 + 3*Power(t1,2)*t2 + 
            2*Power(t2,2) + 9*t1*Power(t2,2) - 7*Power(t2,3) + 
            Power(s2,2)*(8 - 5*t1 + 8*t2) + 
            Power(s1,2)*(46 + 3*Power(s2,2) + Power(t1,2) - 
               t1*(-23 + t2) - 2*t2 + s2*(-29 - 4*t1 + 3*t2)) + 
            s2*(9 + 10*Power(t1,2) - 17*t2 + Power(t2,2) - 
               t1*(14 + 11*t2)) + 
            s1*(-29 + 4*Power(t1,3) + Power(t1,2)*(3 - 8*t2) - 47*t2 + 
               12*Power(t2,2) + t1*(-18 - 15*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(4*t1 - 3*(5 + t2)) + 
               s2*(28 - 8*Power(t1,2) + 12*t2 - 3*Power(t2,2) + 
                  t1*(12 + 11*t2)))) + 
         s*(18 + 43*t1 + 2*Power(t1,2) + Power(t1,3) + 
            Power(s2,3)*(4 - 3*t2) - 69*t2 - 2*t1*t2 + 14*Power(t1,2)*t2 - 
            7*Power(t1,3)*t2 + 48*Power(t2,2) - 20*t1*Power(t2,2) + 
            12*Power(t1,2)*Power(t2,2) + 5*Power(t2,3) - 
            3*t1*Power(t2,3) - 2*Power(t2,4) - 
            Power(s2,2)*(5 - 28*t2 - 7*Power(t2,2) + t1*(7 + t2)) - 
            Power(s1,3)*(6 + 4*Power(s2,2) + 3*Power(t1,2) + 
               t1*(21 - 4*t2) - 7*t2 + Power(t2,2) + 
               s2*(-22 - 7*t1 + 5*t2)) + 
            s2*(-45 + 17*t2 + 2*Power(t2,2) + 8*Power(t2,3) + 
               Power(t1,2)*(2 + 11*t2) + t1*(3 - 42*t2 - 19*Power(t2,2))) \
+ Power(s1,2)*(-2*Power(s2,3) - 4*Power(t1,3) + Power(s2,2)*(29 + 4*t2) + 
               Power(t1,2)*(13 + 12*t2) + 
               t1*(53 + 13*t2 - 10*Power(t2,2)) + 
               2*(-12 + 15*t2 - 4*Power(t2,2) + Power(t2,3)) + 
               s2*(-55 + 6*Power(t1,2) - 9*t2 + 8*Power(t2,2) - 
                  2*t1*(21 + 8*t2))) - 
            s1*(-37 - 6*Power(t1,3) - 2*Power(t1,4) + 
               Power(s2,3)*(-1 + 2*t1 - 2*t2) + 14*t2 + 25*Power(t2,2) - 
               3*Power(t2,3) + Power(t2,4) + 
               Power(s2,2)*(1 - 6*Power(t1,2) + 4*t1*(-1 + t2) + 40*t2) + 
               Power(t1,2)*(7 + 18*t2 + 7*Power(t2,2)) + 
               t1*(46 + 26*t2 - 9*Power(t2,2) - 6*Power(t2,3)) + 
               s2*(-31 + 6*Power(t1,3) + Power(t1,2)*(11 - 2*t2) - 52*t2 + 
                  23*Power(t2,2) + 3*Power(t2,3) - 
                  t1*(8 + 58*t2 + 7*Power(t2,2))))))*T5q(1 - s2 + t1 - t2))/
     ((-1 + s1)*Power(-s + s1 - t2,2)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       (-1 + s2 - t1 + t2)));
   return a;
};
