#include "../h/nlo_functions.h"
#include "../h/dilog.h"

#define Pi       3.141592653589793238462643383279502884L
#define Power pq



long double ur_bubl(double *vars)
{
    long double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    long double aG=1/Power(4.*Pi,2);
    long double a=aG*((-16*(s - s1 - s2)*(-4*Power(s1,3)*(s2 + t1) + 
         4*Power(s,3)*(t1 + t2) - 
         Power(s,2)*(5*s2*t1 - 10*Power(t1,2) + 7*s2*t2 + 16*t1*t2 - 
            10*Power(t2,2) + s1*(5*s2 + 7*t1 + 5*t2)) + 
         Power(s1,2)*(5*Power(s2,2) - 30*s2*t1 + t1*(5*t1 + 12*t2)) + 
         t2*(-4*Power(s2,3) + Power(s2,2)*(12*t1 + 5*t2) - 
            4*s2*(3*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
            4*t1*(2*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) - 
         2*s1*(2*Power(s2,3) + 15*Power(s2,2)*t2 - 15*s2*t1*t2 + 
            2*t1*(Power(t1,2) - 5*t1*t2 + 3*Power(t2,2))) + 
         s*(Power(s1,2)*(5*s2 + 7*t1 + 5*t2) + Power(s2,2)*(5*t1 + 7*t2) + 
            s2*(-5*Power(t1,2) + 16*t1*t2 - 15*Power(t2,2)) + 
            4*(Power(t1,3) - 2*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
               Power(t2,3)) + s1*
             (5*Power(s2,2) - 15*Power(t1,2) + 16*t1*t2 - 5*Power(t2,2) + 
               30*s2*(t1 + t2)))))/
     (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) - 
    (8*(Power(s1,3)*(-Power(s2,2) + Power(t1,2)) + Power(s,4)*(t1 + t2) - 
         Power(s1,2)*(s2 + t1)*
          (Power(s2,2) - t1*(t1 - 2*t2) - s2*(t1 + t2)) + 
         Power(s,3)*(s1*(s2 - 3*t1 - 2*t2) + 6*t1*t2 - s2*(t1 + 4*t2)) + 
         Power(s,2)*(Power(t1 + t2,3) + 
            Power(s1,2)*(-2*s2 + 3*t1 + t2) + Power(s2,2)*(t1 + 6*t2) - 
            s2*t1*(t1 + 11*t2) + 
            s1*(-Power(s2,2) + 2*s2*t1 + Power(t1,2) + 7*s2*t2 - 9*t1*t2)\
) + s2*t2*(Power(s2,3) - 4*Power(s2,2)*t1 + 
            s2*(6*Power(t1,2) - 2*t1*t2 + Power(t2,2)) - 
            2*t1*(2*Power(t1,2) - t1*t2 + Power(t2,2))) + 
         s1*(Power(s2,4) + 2*Power(t1,2)*t2*(-t1 + t2) + 
            Power(s2,3)*(t1 + 3*t2) + 
            s2*t1*(2*Power(t1,2) + t1*t2 + Power(t2,2)) - 
            Power(s2,2)*(2*Power(t1,2) + 5*t1*t2 + Power(t2,2))) + 
         s*(Power(s1,3)*(s2 - t1) - Power(s2,3)*(t1 + 4*t2) + 
            Power(s2,2)*t1*(2*t1 + 9*t2) + 
            2*t1*t2*(Power(t1,2) + Power(t2,2)) - 
            s2*(2*Power(t1,3) + 7*Power(t1,2)*t2 + t1*Power(t2,2) + 
               2*Power(t2,3)) + 
            Power(s1,2)*(3*Power(s2,2) - s2*(t1 + 2*t2) + 
               t1*(-2*t1 + 3*t2)) - 
            s1*(Power(s2,3) + Power(s2,2)*(t1 + 8*t2) + 
               s2*(Power(t1,2) - 12*t1*t2 - Power(t2,2)) + 
               t1*(2*Power(t1,2) + t1*t2 + Power(t2,2)))))*Log(-s1))/
     (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) - 
    (8*(Power(s1,4)*(s2 + t1) + Power(s,4)*(t1 + t2) + 
         Power(s,3)*(-2*s2*t1 + s1*(s2 - 4*t1 - t2) - 3*s2*t2 + 
            6*t1*t2) + s2*Power(t2,2)*
          (Power(s2,2) + 2*t1*(t1 - t2) + s2*(-2*t1 + t2)) + 
         Power(s1,3)*(-Power(s2,2) - 4*t1*t2 + s2*(3*t1 + t2)) + 
         Power(s,2)*(s2*t2*(-9*t1 + t2) + Power(t1 + t2,3) + 
            Power(s1,2)*(-s2 + 6*t1 + t2) + Power(s2,2)*(t1 + 3*t2) - 
            s1*(2*Power(s2,2) - 7*s2*t1 - 2*s2*t2 + 11*t1*t2 + 
               Power(t2,2))) + 
         s1*t2*(-(Power(s2,2)*(t1 - 2*t2)) - 
            2*t1*(Power(t1,2) - t1*t2 + 2*Power(t2,2)) + 
            s2*(Power(t1,2) + t1*t2 + 2*Power(t2,2))) - 
         Power(s1,2)*(Power(s2,3) - Power(s2,2)*t1 + 
            s2*(Power(t1,2) + 5*t1*t2 + 2*Power(t2,2)) - 
            t1*(Power(t1,2) - 2*t1*t2 + 6*Power(t2,2))) - 
         s*(Power(s1,3)*(s2 + 4*t1 + t2) + 
            Power(s1,2)*(-3*Power(s2,2) + s2*(8*t1 + t2) - 
               t2*(9*t1 + 2*t2)) + 
            s1*(-Power(s2,3) + 2*Power(t1,3) + Power(t1,2)*t2 + 
               7*t1*Power(t2,2) + 2*Power(t2,3) + 
               Power(s2,2)*(2*t1 + t2) + 
               s2*(-Power(t1,2) - 12*t1*t2 + Power(t2,2))) + 
            t2*(Power(s2,3) + Power(s2,2)*(-3*t1 + 2*t2) - 
               2*t1*(Power(t1,2) + Power(t2,2)) + 
               s2*(Power(t1,2) + t1*t2 + 2*Power(t2,2)))))*Log(-s2))/
     (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) - 
    (8*(Power(s1,4)*(s2 + t1) + Power(s,4)*(t1 + t2) + 
         s2*t2*(Power(s2,3) - Power(s2,2)*t1 + s2*t2*(t1 + t2) - 
            t1*t2*(t1 + t2)) + 
         Power(s1,3)*(Power(s2,2) + s2*(3*t1 + t2) - 2*t1*(t1 + 2*t2)) - 
         Power(s,3)*(s1*(s2 + 4*t1 + t2) - 2*t1*(t1 + 2*t2) + 
            s2*(t1 + 4*t2)) + 
         s1*(Power(s2,4) + Power(s2,3)*(-2*t1 + t2) + 
            Power(s2,2)*t1*(t1 + t2) - 
            t1*t2*(Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
            s2*t2*(Power(t1,2) + 2*t1*t2 + 2*Power(t2,2))) + 
         Power(s1,2)*(Power(s2,3) - Power(s2,2)*t1 + 
            t1*Power(t1 + 2*t2,2) - 
            s2*(Power(t1,2) + 5*t1*t2 + 2*Power(t2,2))) + 
         Power(s,2)*(Power(t1,3) - Power(s2,2)*(t1 - 6*t2) + 
            4*Power(t1,2)*t2 + Power(t2,3) + 
            Power(s1,2)*(s2 + 6*t1 + t2) - s2*t1*(t1 + 9*t2) + 
            s1*(Power(s2,2) - 6*Power(t1,2) - 9*t1*t2 - Power(t2,2) + 
               2*s2*(t1 + t2))) - 
         s*(Power(s2,2)*t1*(t1 - 6*t2) - Power(s2,3)*(t1 - 4*t2) + 
            Power(s1,3)*(s2 + 4*t1 + t2) - 
            t1*t2*(Power(t1,2) + Power(t2,2)) + 
            s2*t2*(4*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
            Power(s1,2)*(Power(s2,2) - 6*Power(t1,2) - 9*t1*t2 - 
               2*Power(t2,2) + s2*(4*t1 + t2)) + 
            s1*(Power(s2,3) + 2*Power(t1,3) + 8*Power(t1,2)*t2 + 
               3*t1*Power(t2,2) + 2*Power(t2,3) + 
               Power(s2,2)*(-3*t1 + 2*t2) - 
               s2*(2*Power(t1,2) + 8*t1*t2 + Power(t2,2)))))*Log(-t1))/
     (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) + 
    (8*(-(Power(s1,4)*(s2 + t1)) + Power(s,3)*(s1 + 2*t1)*(t1 - t2) + 
         Power(s1,3)*(-2*Power(s2,2) - t1*(t1 - 2*t2) + s2*(t1 + t2)) + 
         s2*t1*t2*(Power(s2,2) - s2*t2 + t2*(t1 + t2)) + 
         Power(s1,2)*(-2*Power(s2,3) + 2*t1*(t1 - t2)*t2 + 
            Power(s2,2)*(-t1 + t2) + s2*(Power(t1,2) - 2*Power(t2,2))) + 
         s1*(-(Power(s2,3)*(t1 - 2*t2)) - Power(s2,2)*Power(t1 - t2,2) - 
            s2*t1*t2*(4*t1 + 3*t2) + 
            t1*t2*(Power(t1,2) + t1*t2 + 2*Power(t2,2))) - 
         Power(s,2)*(Power(s1,2)*(s2 + 3*t1 - 2*t2) + 
            s1*(2*s2*t1 + 5*Power(t1,2) - 3*s2*t2 - 6*t1*t2 + 
               Power(t2,2)) + 
            t1*(2*s2*t1 - 4*s2*t2 - t1*t2 + 3*Power(t2,2))) + 
         s*(Power(s1,3)*(2*s2 + 3*t1 - t2) + 
            Power(s1,2)*(2*Power(s2,2) + s2*t1 + 4*Power(t1,2) - 
               3*s2*t2 - 6*t1*t2 + 2*Power(t2,2)) + 
            t1*(Power(s2,2)*(t1 - 3*t2) + s2*t2*(t1 + 4*t2) - 
               t2*(Power(t1,2) + Power(t2,2))) + 
            s1*(2*Power(s2,2)*(t1 - 2*t2) + t1*t2*(-3*t1 + 8*t2) + 
               s2*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)))))*
       Log(s - s2 + t1))/(s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) - 
    (8*(Power(s1,4)*(s2 + t1) + Power(s,4)*(t1 + t2) + 
         Power(s1,3)*(Power(s2,2) + s2*(t1 - 2*t2) - t1*t2) + 
         Power(s1,2)*(Power(s2,3) - Power(s2,2)*t2 + 
            Power(t1,2)*(t1 + t2) + s2*t2*(t1 + t2)) - 
         Power(s,3)*(-2*t2*(2*t1 + t2) + s1*(s2 + 4*t1 + t2) + 
            s2*(t1 + 4*t2)) + 
         s2*t2*(Power(s2,3) - 2*Power(s2,2)*(2*t1 + t2) + 
            s2*Power(2*t1 + t2,2) - 
            t1*(2*Power(t1,2) + t1*t2 + Power(t2,2))) + 
         s1*(Power(s2,4) - Power(t1,2)*t2*(t1 + t2) + 
            Power(s2,3)*(t1 + 3*t2) + 
            s2*t1*(2*Power(t1,2) + 2*t1*t2 + Power(t2,2)) - 
            Power(s2,2)*(2*Power(t1,2) + 5*t1*t2 + Power(t2,2))) + 
         Power(s,2)*(Power(t1,3) + Power(s1,2)*(s2 + 6*t1 - t2) + 
            4*t1*Power(t2,2) + Power(t2,3) + Power(s2,2)*(t1 + 6*t2) - 
            s2*(Power(t1,2) + 9*t1*t2 + 6*Power(t2,2)) + 
            s1*(Power(s2,2) + 2*s2*(t1 + t2) - t2*(9*t1 + t2))) - 
         s*(Power(s1,3)*(s2 + 4*t1 - t2) + Power(s2,3)*(t1 + 4*t2) - 
            t1*t2*(Power(t1,2) + Power(t2,2)) + 
            Power(s1,2)*(Power(s2,2) + 2*s2*t1 - 3*s2*t2 - 6*t1*t2 + 
               Power(t2,2)) - 
            Power(s2,2)*(2*Power(t1,2) + 9*t1*t2 + 6*Power(t2,2)) + 
            s2*(2*Power(t1,3) + 3*Power(t1,2)*t2 + 8*t1*Power(t2,2) + 
               2*Power(t2,3)) + 
            s1*(Power(s2,3) + Power(s2,2)*(t1 + 4*t2) - 
               s2*(Power(t1,2) + 8*t1*t2 + 2*Power(t2,2)) + 
               t1*(2*Power(t1,2) + t1*t2 + 4*Power(t2,2)))))*Log(-t2))/
     (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) - 
    (8*(Power(s1,3)*(s2 - t1)*(2*s2 + t2) + 
         Power(s,3)*(t1 - t2)*(s2 + 2*t2) + 
         Power(s1,2)*(2*Power(s2,3) + s2*Power(t1 - t2,2) + 
            Power(t1,2)*t2 + Power(s2,2)*(-t1 + t2)) + 
         s1*(Power(s2,4) - Power(s2,3)*(t1 + t2) - 
            Power(t1,2)*t2*(t1 + t2) + s2*t1*t2*(3*t1 + 4*t2) + 
            Power(s2,2)*(2*Power(t1,2) - Power(t2,2))) + 
         s2*t2*(Power(s2,3) + 2*s2*t1*(t1 - t2) + 
            Power(s2,2)*(-2*t1 + t2) - 
            t1*(2*Power(t1,2) + t1*t2 + Power(t2,2))) + 
         Power(s,2)*(t1*(3*t1 - t2)*t2 + Power(s2,2)*(-2*t1 + 3*t2) + 
            s2*(Power(t1,2) - 6*t1*t2 + 5*Power(t2,2)) + 
            s1*(Power(s2,2) + 2*t2*(-2*t1 + t2) + s2*(-3*t1 + 2*t2))) - 
         s*(-(Power(s2,3)*(t1 - 3*t2)) + s2*t1*(8*t1 - 3*t2)*t2 - 
            t1*t2*(Power(t1,2) + Power(t2,2)) + 
            Power(s1,2)*(2*Power(s2,2) - 4*s2*t1 + 2*s2*t2 - 3*t1*t2 + 
               Power(t2,2)) + 2*Power(s2,2)*
             (Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
            s1*(2*Power(s2,3) + Power(s2,2)*(-3*t1 + t2) + 
               t1*t2*(4*t1 + t2) + 
               s2*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)))))*
       Log(s - s1 + t2))/(s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2));
       
       return a;
};
