#include <quadmath.h>



#define  Pi                 M_PIq
#define  MY_LDBL_EPSILON    0.00000000000000000000000000000000000001Q





#include "logcq.c"
#include "clausenq.c"


/* Evaluate series for real dilog(x)
 * Sum[ x^k / k^2, {k,1,Infinity}]
 *
 * Converges rapidly for |x| < 1/2.
 */
__float128 
dilog_series_1q(__float128 x)
{
  const int kmax = 1000;
  __float128 sum  = x;
  __float128 term = x;
  int k;
  for(k=2; k<kmax; k++) {
    const __float128 rk = (k-1.0Q)/k;
    term *= x;
    term *= rk*rk;
    sum += term;
    if(fabsq(term/sum) < MY_LDBL_EPSILON) break;
  }
  return sum;
}


/* Compute the associated series
 *
 *   sum_{k=1}{infty} r^k / (k^2 (k+1))
 *
 * This is a series which appears in the one-step accelerated
 * method, which splits out one elementary function from the
 * full definition of Li_2(x). See below.
 */
__float128
series_2q(__float128 r)
{
  static const int kmax = 200;
  __float128 rk = r;
  __float128 sum = 0.5 * r;
  int k;
  for(k=2; k<50; k++)
  {
    __float128 ds;
    rk *= r;
    ds = rk/(k*k*(k+1.0));
    sum += ds;
  }
  for(; k<kmax; k++)
  {
    __float128 ds;
    rk *= r;
    ds = rk/(k*k*(k+1.0));
    sum += ds;
    if(fabsq(ds/sum) < 0.5*MY_LDBL_EPSILON) break;
  }

  
  return sum;
}


/* Compute Li_2(x) using the accelerated series representation.
 *
 * Li_2(x) = 1 + (1-x)ln(1-x)/x + series_2(x)
 *
 * assumes: -1 < x < 1
 */
__float128
dilog_series_2q(__float128 x)
{
  __float128 val=series_2q(x);
  __float128 t;
  if(x > 0.01)
    t = (1.0 - x) * logq(1.0-x) / x;
  else
  {
    static const __float128 c3 = 1.0/3.0;
    static const __float128 c4 = 1.0/4.0;
    static const __float128 c5 = 1.0/5.0;
    static const __float128 c6 = 1.0/6.0;
    static const __float128 c7 = 1.0/7.0;
    static const __float128 c8 = 1.0/8.0;
    const __float128 t68 = c6 + x*(c7 + x*c8);
    const __float128 t38 = c3 + x *(c4 + x *(c5 + x * t68));
    t = (x - 1.0) * (1.0 + x*(0.5 + x*t38));
  }
  val +=1.0+t;
  return val;
}


/* Calculates Li_2(x) for real x. Assumes x >= 0.0.
 */
__float128
dilog_xge0q(const __float128 x)
{
  __float128 val;
  if(x > 2.0) {
    const __float128 log_x = logq(x);
    const __float128 t1 = Pi*Pi/3.0;
    const __float128 t2 = dilog_series_2q(1.0/x);
    const __float128 t3 = 0.5*log_x*log_x;
    val  = t1 - t2 - t3;
    return val;
  }
  else if(x > 1.01) {
    const __float128 log_x    = logq(x);
    const __float128 log_term = log_x * (logq(1.0-1.0/x) + 0.5*log_x);
    const __float128 t1 = Pi*Pi/6.0;
    const __float128 t2 = dilog_series_2q(1.0 - 1.0/x);
    const __float128 t3 = log_term;
    val  = t1 + t2 - t3;
    return val;
  }
  else if(x > 1.0) {
    /* series around x = 1.0 */
    const __float128 eps = x - 1.0Q;
    const __float128 lne = logq(eps);
    const __float128 c0 = Pi*Pi/6.0;
    const __float128 c1 =   1.0Q - lne;
    const __float128 c2 = -(1.0Q - 2.0*lne)/4.0;
    const __float128 c3 =  (1.0Q - 3.0*lne)/9.0;
    const __float128 c4 = -(1.0Q - 4.0*lne)/16.0;
    const __float128 c5 =  (1.0Q - 5.0*lne)/25.0;
    const __float128 c6 = -(1.0Q - 6.0*lne)/36.0;
    const __float128 c7 =  (1.0Q - 7.0*lne)/49.0;
    const __float128 c8 = -(1.0Q - 8.0*lne)/64.0;
    val = c0+eps*(c1+eps*(c2+eps*(c3+eps*(c4+eps*(c5+eps*(c6+eps*(c7+eps*c8)))))));
    return val;
  }
  else if(x == 1.0) {
    val = Pi*Pi/6.0;
    return val;
  }
  else if(x > 0.5) {
    const __float128 log_x = logq(x);
    const __float128 t1 = Pi*Pi/6.0;
    const __float128 t2 = dilog_series_2q(1.0Q-x);
    const __float128 t3 = log_x*logq(1.0Q-x);
    val  = t1 - t2 - t3;
    return val;
  }
  else if(x > 0.25) {
    return dilog_series_2q(x);
  }
  else if(x > 0.0) {
    return dilog_series_1q(x);
  }
  else {
    /* x == 0.0 */
    val = 0.0;
    return val;
  }
}




__float128
dilogq(__float128 x)
{
  if(x >= 0.0) {
    return dilog_xge0q(x);
  }
  else {
    __float128 d1 = dilog_xge0q( -x);
    __float128 d2 = dilog_xge0q(x*x);
    return -d1 + 0.5 * d2;
  }
}


/*
############ COMPLEX DILOG ###############
 */


/* Evaluate the series representation for Li2(z):
 *
 *   Li2(z) = Sum[ |z|^k / k^2 Exp[i k arg(z)], {k,1,Infinity}]
 *   |z|    = r
 *   arg(z) = theta
 *   
 * Assumes 0 < r < 1.
 * It is used only for small r.
 */
static
int
dilogc_series_1q(
  const __float128 r,
  const __float128 x,
  const __float128 y,
  __float128 * real_result,
  __float128 * imag_result
  )
{
  const __float128 cos_theta = x/r;
  const __float128 sin_theta = y/r;
  const __float128 alpha = 1.0Q - cos_theta;
  const __float128 beta  = sin_theta;
  __float128 ck = cos_theta;
  __float128 sk = sin_theta;
  __float128 rk = r;
  __float128 real_sum = r*ck;
  __float128 imag_sum = r*sk;
  const int kmax = 200 + (int)(22.0Q/(-logq(r))); 
  int k;
  for(k=2; k<kmax; k++) {
    __float128 dr, di;
    __float128 ck_tmp = ck;
    ck = ck - (alpha*ck + beta*sk);
    sk = sk - (alpha*sk - beta*ck_tmp);
    rk *= r;
    dr = rk/((__float128)k*k) * ck;
    di = rk/((__float128)k*k) * sk;
    real_sum += dr;
    imag_sum += di;
    if(fabsq((dr*dr + di*di)/(real_sum*real_sum + imag_sum*imag_sum)) < MY_LDBL_EPSILON*MY_LDBL_EPSILON) break;
  }

  (* real_result) = real_sum;
  (* imag_result) = imag_sum;
  

  return 1;
};



/* Compute
 *
 *   sum_{k=1}{infty} z^k / (k^2 (k+1))
 *
 * This is a series which appears in the one-step accelerated
 * method, which splits out one elementary function from the
 * full definition of Li_2.
 */
static int
series_2_cq(
  __float128 r,
  __float128 x,
  __float128 y,
  __float128 * sum_re,
  __float128 * sum_im
  )
{
  const __float128 cos_theta = x/r;
  const __float128 sin_theta = y/r;
  const __float128 alpha = 1.0Q - cos_theta;
  const __float128 beta  = sin_theta;
  __float128 ck = cos_theta;
  __float128 sk = sin_theta;
  __float128 rk = r;
  __float128 real_sum = 0.5Q * r*ck;
  __float128 imag_sum = 0.5Q * r*sk;
  const int kmax = 200 + (int)(18.0Q/(-logq(r))); /* tuned for double-precision */
  int k;
  for(k=2; k<kmax; k++)
  {
    __float128 dr, di;
    const __float128 ck_tmp = ck;
    ck = ck - (alpha*ck + beta*sk);
    sk = sk - (alpha*sk - beta*ck_tmp);
    rk *= r;
    dr = rk/((__float128)k*k*(k+1.0Q)) * ck;
    di = rk/((__float128)k*k*(k+1.0Q)) * sk;
    real_sum += dr;
    imag_sum += di;
    if(fabsq((dr*dr + di*di)/(real_sum*real_sum + imag_sum*imag_sum)) < MY_LDBL_EPSILON*MY_LDBL_EPSILON) break;
  }

  (* sum_re) = real_sum;
  (* sum_im) = imag_sum;
  

  return 1;
};


/* Compute Li_2(z) using the one-step accelerated series.
 *
 * Li_2(z) = 1 + (1-z)ln(1-z)/z + series_2_c(z)
 *
 * z = r exp(i theta)
 * assumes: r < 1
 * assumes: r > epsilon, so that we take no special care with log(1-z)
 */
static
int
dilogc_series_2q(
  const __float128 r,
  const __float128 x,
  const __float128 y,
  __float128 * real_dl,
  __float128 * imag_dl
  )
{
  if(r == 0.0)
  {
    (* real_dl) = 0.0Q;
    (* imag_dl) = 0.0Q;
    return 1;
  }
  else
  {
    __float128 sum_re;
    __float128 sum_im;
    series_2_cq(r, x, y, &sum_re, &sum_im);

    /* t = ln(1-z)/z */
    __float128 ln_omz_r;
    __float128 ln_omz_theta;
    
    logq_c(1.0Q-x, -y, &ln_omz_r, &ln_omz_theta);
    
    const __float128 t_x = ( ln_omz_r * x + ln_omz_theta * y)/(r*r);
    const __float128 t_y = (-ln_omz_r * y + ln_omz_theta * x)/(r*r);

    /* r = (1-z) ln(1-z)/z */
    const __float128 r_x = (1.0Q - x) * t_x + y * t_y;
    const __float128 r_y = (1.0Q - x) * t_y - y * t_x;

    (* real_dl) = sum_re + r_x + 1.0Q;
    (* imag_dl) = sum_im + r_y;
    
    return 1;
  }
};



/* Evaluate a series for Li_2(z) when |z| is near 1.
 * This is uniformly good away from z=1.
 *
 *   Li_2(z) = Sum[ a^n/n! H_n(theta), {n, 0, Infinity}]
 *
 * where
 *   H_n(theta) = Sum[ e^(i m theta) m^n / m^2, {m, 1, Infinity}]
 *   a = ln(r)
 *
 *  H_0(t) = Gl_2(t) + i Cl_2(t)
 *  H_1(t) = 1/2 ln(2(1-c)) + I atan2(-s, 1-c)
 *  H_2(t) = -1/2 + I/2 s/(1-c)
 *  H_3(t) = -1/2 /(1-c)
 *  H_4(t) = -I/2 s/(1-c)^2
 *  H_5(t) = 1/2 (2 + c)/(1-c)^2
 *  H_6(t) = I/2 s/(1-c)^5 (8(1-c) - s^2 (3 + c))
 */
static
int
dilogc_series_3q(
  const __float128 r,
  const __float128 x,
  const __float128 y,
  __float128 * real_result,
  __float128 * imag_result
  )
{
  const __float128 theta = atan2q(y, x);
  const __float128 cos_theta = x/r;
  const __float128 sin_theta = y/r;
  const __float128 a = logq(r);
  const __float128 omc = 1.0Q - cos_theta;
  const __float128 omc2 = omc*omc;
  __float128 H_re[7];
  __float128 H_im[7];
  __float128 an, nfact;
  __float128 sum_re, sum_im;
  __float128 Him0;
  int n;

  H_re[0] = Pi*Pi/6.0Q + 0.25Q*(theta*theta - 2.0Q*Pi*fabsq(theta));
  /*!!!!!!!!!*/
  Him0 = clausenq(theta);
  
  
  H_im[0] = Him0;

  H_re[1] = -0.5Q*logq(2.0Q*omc);
  H_im[1] = -atan2q(-sin_theta, omc);

  H_re[2] = -0.5Q;
  H_im[2] = 0.5Q * sin_theta/omc;

  H_re[3] = -0.5Q/omc;
  H_im[3] = 0.0Q;

  H_re[4] = 0.0Q;
  H_im[4] = -0.5Q*sin_theta/omc2;

  H_re[5] = 0.5Q * (2.0Q + cos_theta)/omc2;
  H_im[5] = 0.0Q;

  H_re[6] = 0.0Q;
  H_im[6] = 0.5Q * sin_theta/(omc2*omc2*omc) * (8.0Q*omc - sin_theta*sin_theta*(3.0Q + cos_theta));

  sum_re = H_re[0];
  sum_im = H_im[0];
  an = 1.0Q;
  nfact = 1.0Q;
  for(n=1; n<=6; n++) {
    double t;
    an *= a;
    nfact *= n;
    t = an/nfact;
    sum_re += t * H_re[n];
    sum_im += t * H_im[n];
  }

  (* real_result) = sum_re;
  (* imag_result) = sum_im;
  

  return 1;
};



/* Calculate complex dilogarithm Li_2(z) in the fundamental region,
 * which we take to be the intersection of the unit disk with the
 * half-space x < MAGIC_SPLIT_VALUE. It turns out that 0.732 is a
 * nice choice for MAGIC_SPLIT_VALUE since then points mapped out
 * of the x > MAGIC_SPLIT_VALUE region and into another part of the
 * unit disk are bounded in radius by MAGIC_SPLIT_VALUE itself.
 *
 * If |z| < 0.98 we use a direct series summation. Otherwise z is very
 * near the unit circle, and the series_2 expansion is used; see above.
 * Because the fundamental region is bounded away from z = 1, this
 * works well.
 */
static
int
dilogc_fundamentalq(__float128 r,__float128 x, __float128 y, __float128 * real_dl, __float128 * imag_dl)
{
  if(r > 0.98)  
    return dilogc_series_3q(r, x, y, real_dl, imag_dl);
  else if(r > 0.25)
    return dilogc_series_2q(r, x, y, real_dl, imag_dl);
  else
    return dilogc_series_1q(r, x, y, real_dl, imag_dl);
};


/* Compute Li_2(z) for z in the unit disk, |z| < 1. If z is outside
 * the fundamental region, which means that it is too close to z = 1,
 * then it is reflected into the fundamental region using the identity
 *
 *   Li2(z) = -Li2(1-z) + zeta(2) - ln(z) ln(1-z).
 */
static
int
dilogc_unitdiskq(__float128 x,__float128 y, __float128 * real_dl, __float128 * imag_dl)
{
  static const __float128 MAGIC_SPLIT_VALUE = 0.732Q;
  static const __float128 zeta2 = Pi*Pi/6.0Q;
  const __float128 r = hypotq(x, y);

  if(x > MAGIC_SPLIT_VALUE)
  {
    /* Reflect away from z = 1 if we are too close. The magic value
     * insures that the reflected value of the radius satisfies the
     * related inequality r_tmp < MAGIC_SPLIT_VALUE.
     */
    const __float128 x_tmp = 1.0Q - x;
    const __float128 y_tmp =     - y;
    const __float128 r_tmp = hypotq(x_tmp, y_tmp);
    /* const double cos_theta_tmp = x_tmp/r_tmp; */
    /* const double sin_theta_tmp = y_tmp/r_tmp; */

    __float128 result_re_tmp;
    __float128 result_im_tmp;

    const int stat_dilog = dilogc_fundamentalq(r_tmp, x_tmp, y_tmp, &result_re_tmp, &result_im_tmp);

    const __float128 lnz    =  logq(r);               /*  log(|z|)   */
    const __float128 lnomz  =  logq(r_tmp);           /*  log(|1-z|) */
    const __float128 argz   =  atan2q(y, x);          /*  arg(z) assuming principal branch */
    const __float128 argomz =  atan2q(y_tmp, x_tmp);  /*  arg(1-z)   */
    (* real_dl)  = -result_re_tmp + zeta2 - lnz*lnomz + argz*argomz;
    (* imag_dl)  = -result_im_tmp - argz*lnomz - argomz*lnz;
    
    return stat_dilog;
  }
  else
  {
    return dilogc_fundamentalq(r, x, y, real_dl, imag_dl);
  }
};



int
dilogcq(
  const __float128 x,
  const __float128 y,
  __float128 * real_dl,
  __float128 * imag_dl
  )
{
  const __float128 zeta2 = Pi*Pi/6.0Q;
  const __float128 r2 = x*x + y*y;

  if(y == 0.0)
  {
    if(x >= 1.0)
    {
      (* imag_dl) = -Pi * logq(x);
    }
    else
    {
      (* imag_dl) = 0.0Q;
    }
    (* real_dl) = dilogq(x);
    return 1;
  }
  else if(fabsq(r2 - 1.0Q) < MY_LDBL_EPSILON)
  {
    /* Lewin A.2.4.1 and A.2.4.2 */

    const __float128 theta = atan2q(y, x);
    const __float128 term1 = theta*theta/4.0Q;
    const __float128 term2 = Pi*fabsq(theta)/2.0Q;
    (* real_dl) = zeta2 + term1 - term2;
    /*!!!!!!!!!!!!!*/
    (* imag_dl) = clausenq(theta);
    return 1;
  }
  else if(r2 < 1.0)
  {
    return dilogc_unitdiskq(x, y, real_dl, imag_dl);
  }
  else
  {
    /* Reduce argument to unit disk. */
    const __float128 r = sqrtq(r2);
    const __float128 x_tmp =  x/r2;
    const __float128 y_tmp = -y/r2;
    /* const double r_tmp = 1.0/r; */
    __float128 result_re_tmp, result_im_tmp;

    const int stat_dilog =
      dilogc_unitdiskq(x_tmp, y_tmp, &result_re_tmp, &result_im_tmp);

    /* Unwind the inversion.
     *
     *  Li_2(z) + Li_2(1/z) = -zeta(2) - 1/2 ln(-z)^2
     */
    const __float128 theta = atan2q(y, x);
    const __float128 theta_abs = fabsq(theta);
    const __float128 theta_sgn = ( theta < 0.0 ? -1.0Q : 1.0Q );
    const __float128 ln_minusz_re = logq(r);
    const __float128 ln_minusz_im = theta_sgn * (theta_abs - Pi);
    const __float128 lmz2_re = ln_minusz_re*ln_minusz_re - ln_minusz_im*ln_minusz_im;
    const __float128 lmz2_im = 2.0Q*ln_minusz_re*ln_minusz_im;
    (* real_dl) = -result_re_tmp - 0.5Q * lmz2_re - zeta2;
    
    (* imag_dl) = -result_im_tmp - 0.5Q * lmz2_im;
    return stat_dilog;
  }
};




