#include <gsl/gsl_math.h>
#include <gsl/gsl_pow_int.h>
//#include "../h/nlo_functions.h"

//#define Pi       3.141592653589793238462643383279502884L
//#define Power pq

#define Power gsl_pow_int


double ur_born(double *vars)
{
    double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    double a=(-16*(-s + s1 + s2)*(Power(s1,3)*(s2 + t1) + 3*Power(s,2)*(s2 - 2*t1)*t2 - 
      3*Power(s1,2)*t1*(s + t2) - Power(s,3)*(t1 + t2) + 
      (s2 - 2*t1)*t2*(Power(s2,2) - s2*t1 + Power(t1,2) + Power(t2,2)) - 
      s*(Power(t1,3) + 3*Power(s2,2)*t2 + 3*Power(t1,2)*t2 + Power(t2,3) + 
         3*t1*t2*(-2*s2 + t2)) + 
      s1*(Power(s2,3) + t1*(3*Power(s,2) + Power(t1,2) + 6*s*t2 + 
            3*Power(t2,2)))))/(s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2);
            
    return a/4.;
};
