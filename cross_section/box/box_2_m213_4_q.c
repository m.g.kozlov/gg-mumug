#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m213_4_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(8*Power(s,4)*s1*(s1 - t2)*(-1 + t2)*t2 + 
         Power(s,3)*t2*(-2*(-1 + t2)*t2 - 8*Power(s1,3)*(-3 + 2*t2) + 
            Power(s1,2)*(-6 - 8*s2*(-1 + t2) + 11*t1*(-1 + t2) - 47*t2 + 
               37*Power(t2,2)) + 
            s1*(10 + (2 - 8*s2)*t2 + (11 + 8*s2)*Power(t2,2) - 
               15*Power(t2,3) + t1*(-6 + 11*t2 - 5*Power(t2,2)))) + 
         Power(s,2)*(-8*Power(s1,4)*(1 + s2 - t1)*t2 - 
            2*Power(t2,2)*(3 - 5*t2 + 2*Power(t2,2)) + 
            Power(s1,3)*(-2 + (18 - 19*s2 + 22*t1)*t2 + 
               (43 + 27*s2 - 30*t1)*Power(t2,2) - 19*Power(t2,3)) + 
            s1*t2*(-12 + 10*(-5 + 2*s2)*t2 + (87 - 39*s2)*Power(t2,2) + 
               (8 + 11*s2)*Power(t2,3) - 9*Power(t2,4) + 
               Power(t1,2)*(-5 + 7*t2 - 2*Power(t2,2)) + 
               t1*(25 - (17 + 4*s2)*t2 + (5 + 4*s2)*Power(t2,2) - 
                  5*Power(t2,3))) + 
            Power(s1,2)*(Power(t1,2)*(-1 - 7*t2 + 8*Power(t2,2)) + 
               t1*(-1 + (17 + 4*s2)*t2 - (51 + 4*s2)*Power(t2,2) + 
                  27*Power(t2,3)) + 
               t2*(42 - 83*t2 - 43*Power(t2,2) + 28*Power(t2,3) + 
                  s2*(-20 + 58*t2 - 30*Power(t2,2))))) - 
         s1*(s1 - t2)*t2*(-4 + 29*t2 + 23*s2*t2 - 4*Power(s2,2)*t2 - 
            17*Power(t2,2) + 7*s2*Power(t2,2) - 
            9*Power(s2,2)*Power(t2,2) - 5*Power(t2,3) - 
            33*s2*Power(t2,3) + 5*Power(s2,2)*Power(t2,3) - 
            3*Power(t2,4) + 3*s2*Power(t2,4) + 
            Power(t1,2)*(-4 + 5*t2 - 9*Power(t2,2)) + 
            t1*(8 + (-66 + 13*s2)*t2 + (22 + 6*s2)*Power(t2,2) - 
               3*(-12 + s2)*Power(t2,3)) + 
            4*Power(s1,3)*(-1 + 2*Power(s2,2) + 2*Power(t1,2) - 
               4*t1*(-1 + t2) + t2 + s2*(-3 - 4*t1 + 3*t2)) - 
            Power(s1,2)*(8*(-1 + t2)*t2 + 3*Power(t1,2)*(1 + 7*t2) + 
               Power(s2,2)*(17 + 7*t2) + 
               t1*(37 - 12*t2 - 25*Power(t2,2)) + 
               s2*(-33 + 16*t2 + 17*Power(t2,2) - 24*t1*(1 + t2))) - 
            s1*(25 - 21*t2 + 3*Power(t2,2) - 7*Power(t2,3) + 
               Power(t1,2)*(1 - 16*t2 - 9*Power(t2,2)) + 
               Power(s2,2)*(-4 - 26*t2 + 6*Power(t2,2)) + 
               t1*(-58 - 7*t2 + 56*Power(t2,2) + 9*Power(t2,3)) + 
               s2*(23 + 40*t2 - 61*Power(t2,2) - 2*Power(t2,3) + 
                  t1*(13 + 30*t2 + 5*Power(t2,2))))) + 
         s*(8*Power(s1,5)*t2*(-1 + s2 - t1 + t2) - 
            2*Power(t2,2)*(-1 + 4*t2 - 4*Power(t2,2) + Power(t2,3)) + 
            Power(s1,4)*(4 + (2 - 13*s2 + 8*Power(s2,2))*t2 + 
               8*Power(t1,2)*t2 + (8 - 11*s2)*Power(t2,2) - 
               14*Power(t2,3) + t1*(2 + (15 - 16*s2)*t2 + 7*Power(t2,2))) \
+ s1*t2*(-9 + (38 - 7*s2)*t2 + 
               (-126 + 37*s2 - 4*Power(s2,2))*Power(t2,2) + 
               (89 - 50*s2 + 4*Power(s2,2))*Power(t2,3) + 
               (11 + 4*s2)*Power(t2,4) - 3*Power(t2,5) + 
               Power(t1,2)*(-5 + 3*t2 + 5*Power(t2,2) - 3*Power(t2,3)) + 
               t1*(10 + (-15 + 7*s2)*t2 + (5 - 10*s2)*Power(t2,2) + 
                  (14 + 3*s2)*Power(t2,3) + 2*Power(t2,4))) + 
            Power(s1,2)*(1 + (-28 + 6*s2)*t2 + 
               (211 - 66*s2 + 8*Power(s2,2))*Power(t2,2) + 
               3*(-52 + 29*s2)*Power(t2,3) - 3*(12 + s2)*Power(t2,4) + 
               8*Power(t2,5) + 
               Power(t1,2)*(-1 - 13*Power(t2,2) + 22*Power(t2,3)) + 
               t1*(2 + 16*Power(t2,2) - 35*Power(t2,3) - 7*Power(t2,4) - 
                  2*s2*t2*(3 - 6*t2 + 11*Power(t2,2)))) + 
            Power(s1,3)*(4 - 91*t2 + 61*Power(t2,2) + 25*Power(t2,3) + 
               Power(t2,4) - 4*Power(s2,2)*t2*(1 + 3*t2) + 
               Power(t1,2)*(3 + 14*t2 - 33*Power(t2,2)) + 
               t1*(3 - 35*t2 + 18*Power(t2,2) + 6*Power(t2,3)) + 
               s2*(1 + 29*t2 - 24*Power(t2,2) + 2*Power(t2,3) + 
                  t1*(-1 - 2*t2 + 35*Power(t2,2)))))))/
     (s*(-1 + s1)*s1*(s - s2 + t1)*Power(s1 - t2,2)*(-s + s1 - t2)*
       (-1 + t2)*t2) + (8*(8*Power(s,6)*Power(t2,2) + 
         2*(-1 + t2)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          (2*Power(s1,3) + t2 - 3*Power(t2,2) - 3*Power(s1,2)*(1 + t2) + 
            s1*(-1 + 6*t2 + Power(t2,2))) + 
         Power(s,5)*(-(Power(t1,2)*(-1 + t2)) + 
            t1*t2*(-3 + s1 + s2 + 7*t2 + s1*t2 + s2*t2) - 
            t2*(s1*(-16 + 29*t2 + Power(t2,2)) + 
               t2*(44 - 24*t2 + s2*(5 + t2)))) - 
         Power(s,3)*(-1 - 5*t2 + 4*s2*t2 + 44*Power(t2,2) + 
            83*s2*Power(t2,2) - 9*Power(s2,2)*Power(t2,2) - 
            28*Power(t2,3) - 95*s2*Power(t2,3) + 
            7*Power(s2,2)*Power(t2,3) + 17*Power(t2,4) + 
            21*s2*Power(t2,4) - 2*Power(s2,2)*Power(t2,4) - 
            7*Power(t2,5) + s2*Power(t2,5) - 
            Power(t1,2)*(-2 - 3*t2 + Power(t2,2) + 4*Power(t2,3)) + 
            Power(s1,3)*(14 - 37*t2 + 18*Power(t2,2) + 5*Power(t2,3) + 
               s2*(-1 + t2 + 2*Power(t2,2)) - 
               t1*(-2 + t2 + 3*Power(t2,2))) + 
            t1*t2*(2 - 19*t2 + 30*Power(t2,2) - 11*Power(t2,3) + 
               s2*(-5 + t2 + 5*Power(t2,2) - Power(t2,3))) + 
            s1*(3 - 76*t2 + 188*Power(t2,2) - 135*Power(t2,3) + 
               27*Power(t2,4) + Power(t2,5) + 
               Power(s2,2)*t2*(9 - 10*t2 + Power(t2,2)) - 
               Power(t1,2)*(4 - 27*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               t1*(8 + 66*t2 - 125*Power(t2,2) + 57*Power(t2,3)) - 
               s2*(1 + (83 + 15*t1)*t2 - 140*Power(t2,2) + 
                  (38 - 7*t1)*Power(t2,3) + 8*Power(t2,4))) + 
            Power(s1,2)*(25 - 145*t2 + 167*Power(t2,2) - 
               37*Power(t2,3) - 6*Power(t2,4) + 
               Power(t1,2)*(-23 + 15*t2) + 
               Power(s2,2)*(-2 + 3*t2 + 3*Power(t2,2)) + 
               t1*(-31 + 73*t2 - 45*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(9 - 29*t2 + 11*Power(t2,2) + 7*Power(t2,3) + 
                  t1*(17 - 12*t2 - 5*Power(t2,2))))) + 
         Power(s,4)*(Power(t1,2)*(-1 + 7*t2) + 
            t1*(2 + (7 - 3*s2)*t2 - (33 + s2)*Power(t2,2) + 
               2*(9 + s2)*Power(t2,3)) + 
            Power(s1,2)*(8 + (-43 + s2)*t2 + (37 + s2)*Power(t2,2) + 
               4*Power(t2,3) + t1*(1 - 2*t2 - 3*Power(t2,2))) - 
            t2*(1 - 75*t2 - Power(s2,2)*(-3 + t2)*t2 + 78*Power(t2,2) - 
               20*Power(t2,3) + 
               s2*(1 - 39*t2 + 20*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(Power(t1,2)*(-8 + 6*t2) + 
               t1*(-5 + 30*t2 - 26*Power(t2,2) + 3*Power(t2,3) + 
                  s2*(3 - 5*t2 - 4*Power(t2,2))) + 
               t2*(-70 + 160*t2 - 57*Power(t2,2) - 3*Power(t2,3) + 
                  Power(s2,2)*(1 + t2) + 2*s2*(-6 + 7*t2 + 2*Power(t2,2))\
))) + Power(s,2)*(1 - 4*t1 + 3*Power(t1,2) - 9*t2 + 5*s2*t2 + 21*t1*t2 - 
            5*s2*t1*t2 - 11*Power(t1,2)*t2 - 11*Power(t2,2) + 
            50*s2*Power(t2,2) - 8*Power(s2,2)*Power(t2,2) + 
            18*t1*Power(t2,2) + 7*s2*t1*Power(t2,2) + 
            18*Power(t1,2)*Power(t2,2) + 69*Power(t2,3) - 
            83*s2*Power(t2,3) + 6*Power(s2,2)*Power(t2,3) - 
            79*t1*Power(t2,3) + 3*s2*t1*Power(t2,3) - 
            10*Power(t1,2)*Power(t2,3) - 60*Power(t2,4) + 
            54*s2*Power(t2,4) - 5*Power(s2,2)*Power(t2,4) + 
            22*t1*Power(t2,4) - s2*t1*Power(t2,4) + 
            2*Power(t1,2)*Power(t2,4) + 8*Power(t2,5) - 
            10*s2*Power(t2,5) + Power(s2,2)*Power(t2,5) + 
            6*t1*Power(t2,5) + 2*Power(t2,6) + 
            Power(s1,4)*(-1 + t2)*
             (s2*(1 + t2) - t1*(1 + t2) + 2*(-3 + 2*t2 + Power(t2,2))) + 
            Power(s1,3)*(23 - 68*t2 + 52*Power(t2,2) - 4*Power(t2,3) - 
               3*Power(t2,4) + 10*Power(t1,2)*(-3 + 2*t2) + 
               2*Power(s2,2)*(-4 + 2*t2 + Power(t2,2)) + 
               t1*(-49 + 83*t2 - 35*Power(t2,2) + Power(t2,3)) + 
               s2*(21 - 21*t2 - 5*Power(t2,2) + 5*Power(t2,3) - 
                  2*t1*(-17 + 10*t2 + Power(t2,2)))) + 
            Power(s1,2)*(19 - 47*t2 + 66*Power(t2,2) - 46*Power(t2,3) + 
               7*Power(t2,4) + Power(t2,5) + 
               Power(t1,2)*(5 + 41*t2 - 14*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(s2,2)*(-2 + 22*t2 - 15*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(14 + 113*t2 - 175*Power(t2,2) + 63*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(27 - 167*t2 + 145*Power(t2,2) - 13*Power(t2,3) - 
                  8*Power(t2,4) + 
                  t1*(-13 - 35*t2 + 7*Power(t2,2) + 5*Power(t2,3)))) - 
            s1*(3 + 6*t2 + 48*Power(t2,2) - 66*Power(t2,3) + 
               Power(t2,4) + 8*Power(t2,5) + 
               2*Power(s2,2)*t2*
                (-5 + 10*t2 - 8*Power(t2,2) + 2*Power(t2,3)) + 
               2*Power(t1,2)*
                (-4 + 9*t2 + 4*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(6 + 39*t2 - 25*Power(t2,2) - 55*Power(t2,3) + 
                  35*Power(t2,4)) + 
               s2*(1 + 79*t2 - 227*Power(t2,2) + 175*Power(t2,3) - 
                  26*Power(t2,4) - 2*Power(t2,5) + 
                  t1*(-1 - 8*t2 - 12*Power(t2,3) + Power(t2,4))))) - 
         s*(1 - 2*t1 + Power(t1,2) - 13*t2 + 2*s2*t2 + 23*t1*t2 - 
            2*s2*t1*t2 - 10*Power(t1,2)*t2 - 4*Power(t2,2) + 
            9*s2*Power(t2,2) - 3*Power(s2,2)*Power(t2,2) - 
            31*t1*Power(t2,2) + 12*s2*t1*Power(t2,2) + 
            25*Power(t1,2)*Power(t2,2) + 46*Power(t2,3) + 
            23*s2*Power(t2,3) - 6*Power(s2,2)*Power(t2,3) - 
            69*t1*Power(t2,3) - 10*s2*t1*Power(t2,3) - 
            20*Power(t1,2)*Power(t2,3) - 35*Power(t2,4) - 
            23*s2*Power(t2,4) + 5*Power(s2,2)*Power(t2,4) + 
            89*t1*Power(t2,4) + 8*s2*t1*Power(t2,4) + 7*Power(t2,5) - 
            13*s2*Power(t2,5) - 6*t1*Power(t2,5) - 2*Power(t2,6) + 
            2*s2*Power(t2,6) - 4*t1*Power(t2,6) + 
            Power(s1,2)*(-(Power(-1 + t2,2)*
                  (28 - 17*t2 + 24*Power(t2,2) + Power(t2,3))) + 
               Power(t1,2)*(7 - 34*t2 + Power(t2,2) + 2*Power(t2,3)) + 
               t1*(11 - 154*t2 + 96*Power(t2,2) + 74*Power(t2,3) - 
                  27*Power(t2,4)) - 
               Power(s2,2)*(3 + 22*t2 + 19*Power(t2,2) - 
                  22*Power(t2,3) + 2*Power(t2,4)) + 
               s2*(21 + (-5 + 66*t1)*t2 - 4*(-27 + 4*t1)*Power(t2,2) - 
                  2*(66 + t1)*Power(t2,3) + 7*Power(t2,4) + Power(t2,5))) \
+ Power(s1,4)*(2*Power(s2,2)*(-5 + 3*t2) + 
               s2*(11 - 5*t2 - 7*Power(t2,2) + Power(t2,3) - 
                  4*t1*(-7 + 5*t2)) + 
               t1*(-27 + 37*t2 - 9*Power(t2,2) - Power(t2,3) + 
                  2*t1*(-9 + 7*t2))) + 
            Power(s1,3)*(Power(-1 + t2,2)*(1 + 14*t2 + Power(t2,2)) + 
               Power(s2,2)*(8 + 27*t2 - 20*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(-18 - 13*t2 + 14*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(17 + 64*t2 - 106*Power(t2,2) + 24*Power(t2,3) + 
                  Power(t2,4)) - 
               2*s2*(-5 + 44*t2 - 36*Power(t2,2) - 4*Power(t2,3) + 
                  Power(t2,4) + t1*(17 + 12*t2 - 13*Power(t2,2)))) + 
            s1*(Power(t1,2)*(8 - 31*t2 + 36*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (11 + 55*t2 - 21*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,2)*t2*
                (6 + 20*t2 - 3*Power(t2,2) - 8*Power(t2,3) + Power(t2,4)) \
+ t1*(-19 + 18*t2 + 206*Power(t2,2) - 220*Power(t2,3) - 3*Power(t2,4) + 
                  18*Power(t2,5)) - 
               2*s2*(1 + 15*t2 + 14*Power(t2,2) + 4*Power(t2,3) - 
                  39*Power(t2,4) + 5*Power(t2,5) + 
                  t1*(-1 + 6*t2 + 11*Power(t2,2) - 2*Power(t2,3) + 
                     2*Power(t2,4))))))*B1(s,t2,s1))/
     (s*(-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) + 
    (8*(4*Power(s,7)*s1*t2*(Power(s1,2) - Power(t2,2)) - 
         2*(-1 + s1)*Power(s1,2)*Power(s1 - t2,2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          (-1 + 2*Power(s1,2) + 3*t2 - s1*(3 + t2)) + 
         Power(s,6)*(Power(s1,4)*(4 - 14*t2) + 2*Power(t2,3) + 
            Power(s1,3)*t2*(-4 - 4*s2 + 7*t1 + 5*t2) + 
            s1*Power(t2,2)*(-8 + t1 + 11*t2 + 4*s2*t2 - 2*t1*t2 - 
               10*Power(t2,2)) + 
            Power(s1,2)*t2*(-6 - 17*t2 + 25*Power(t2,2) + t1*(5 + t2))) + 
         Power(s,5)*(6*(-2 + t2)*Power(t2,3) + 
            2*Power(s1,5)*(-5 + 11*t2) + 
            Power(s1,4)*(2*Power(t1,2) + t1*(7 - 31*t2) + 
               s2*(-4 + 13*t2) - t2*(31 + 30*t2)) + 
            s1*Power(t2,2)*(47 - (8 + 25*s2)*t2 + 
               2*(13 + 5*s2)*Power(t2,2) - 8*Power(t2,3) + 
               t1*(-8 + 6*t2 - 4*Power(t2,2))) + 
            Power(s1,2)*t2*(13 + 10*(8 + s2)*t2 - 
               (96 + 23*s2)*Power(t2,2) + 44*Power(t2,3) + 
               2*Power(t1,2)*(3 + t2) + 
               t1*(-33 + (-17 + 4*s2)*t2 + 10*Power(t2,2))) + 
            Power(s1,3)*(-6 + (-24 + 19*s2)*t2 + 2*Power(t1,2)*t2 + 
               111*Power(t2,2) - 40*Power(t2,3) + 
               t1*(5 - 4*(11 + s2)*t2 + 13*Power(t2,2)))) + 
         Power(s,4)*(-4*Power(s1,6)*(-3 + 7*t2) + 
            2*Power(t2,3)*(13 - 14*t2 + 3*Power(t2,2)) + 
            Power(s1,5)*(-42 - 12*Power(t1,2) + s2*(9 + 4*t1 - 20*t2) + 
               79*t2 + 90*Power(t2,2) + 4*t1*(-7 + 12*t2)) + 
            Power(s1,3)*(15 + (97 - 25*s2)*t2 - 
               (278 + 63*s2)*Power(t2,2) + (237 + 40*s2)*Power(t2,3) - 
               33*Power(t2,4) + 
               Power(t1,2)*(6 - 34*t2 - 4*Power(t2,2)) + 
               t1*(-29 + 8*(10 + 3*s2)*t2 - 20*(-1 + s2)*Power(t2,2) - 
                  5*Power(t2,3))) + 
            s1*Power(t2,2)*(-101 + (8 + 61*s2)*t2 + 
               (13 - 58*s2)*Power(t2,2) + (17 + 8*s2)*Power(t2,3) - 
               2*Power(t2,4) + 
               t1*(18 - 13*t2 + 7*Power(t2,2) - 2*Power(t2,3))) + 
            Power(s1,2)*t2*(18 - 3*(38 + 17*s2)*t2 + 
               5*(17 + 27*s2)*Power(t2,2) - 
               2*(49 + 19*s2)*Power(t2,3) + 19*Power(t2,4) + 
               2*Power(t1,2)*(-6 + 4*t2 + Power(t2,2)) + 
               t1*(53 + (52 - 20*s2)*t2 + 8*(-6 + s2)*Power(t2,2) + 
                  13*Power(t2,3))) + 
            Power(s1,4)*(-17 + Power(t1,2)*(2 - 4*t2) + 180*t2 - 
               265*Power(t2,2) - 40*Power(t2,3) + 
               t1*(-29 + 103*t2 - 48*Power(t2,2)) + 
               s2*(15 - 23*t2 + 10*Power(t2,2) + t1*(-4 + 8*t2)))) + 
         Power(s,3)*(16*Power(s1,7)*(-1 + 2*t2) + 
            2*Power(t2,3)*(-13 + 23*t2 - 10*Power(t2,2) + Power(t2,3)) + 
            Power(s1,6)*(70 + 2*Power(s2,2) + 28*Power(t1,2) + 
               t1*(40 - 28*t2) - 35*t2 - 134*Power(t2,2) + 
               s2*(-15 - 20*t1 + 26*t2)) + 
            s1*Power(t2,2)*(99 - (7 + 73*s2)*t2 + 
               13*(-10 + 9*s2)*Power(t2,2) + (27 - 39*s2)*Power(t2,3) + 
               2*(1 + s2)*Power(t2,4) + 
               t1*(-16 + 19*t2 + Power(t2,2) - 2*Power(t2,3))) + 
            Power(s1,2)*t2*(-63 + (-5 + 94*s2)*t2 + 
               2*Power(t1,2)*(-11 + t2)*t2 - 
               31*(-7 + 9*s2)*Power(t2,2) + 
               (-77 + 175*s2 - 2*Power(s2,2))*Power(t2,3) - 
               (19 + 15*s2)*Power(t2,4) + 
               t1*(-15 + 36*(-2 + s2)*t2 + (131 - 36*s2)*Power(t2,2) + 
                  4*(-2 + s2)*Power(t2,3) + 4*Power(t2,4))) - 
            Power(s1,3)*(4 + (103 + 5*s2)*t2 + 
               (47 - 232*s2)*Power(t2,2) + 
               (27 + 259*s2 - 4*Power(s2,2))*Power(t2,3) - 
               11*(9 + 2*s2)*Power(t2,4) - 4*Power(t2,5) + 
               Power(t1,2)*(6 - 36*t2 + 52*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-31 + (-61 + 52*s2)*t2 + (166 - 92*s2)*Power(t2,2) + 
                  (-81 + 16*s2)*Power(t2,3) + 7*Power(t2,4))) + 
            Power(s1,5)*(39 - 266*t2 - 4*Power(s2,2)*t2 + 
               226*Power(t2,2) + 160*Power(t2,3) - 
               2*Power(t1,2)*(9 + 5*t2) + 
               t1*(15 - 103*t2 + 54*Power(t2,2)) + 
               s2*(1 + 24*t2 - 54*Power(t2,2) + 4*t1*(3 + 7*t2))) + 
            Power(s1,4)*(39 - 65*t2 + 301*Power(t2,2) - 
               257*Power(t2,3) - 62*Power(t2,4) + 
               Power(t1,2)*(-32 + 74*t2 + 6*Power(t2,2)) + 
               t1*(40 + 19*t2 - 56*Power(t2,2) - 23*Power(t2,3)) + 
               s2*(-16 - 71*t2 + 114*Power(t2,2) + 19*Power(t2,3) + 
                  4*t1*(4 - 17*t2 + Power(t2,2))))) - 
         Power(s,2)*(2*Power(s1,8)*(-8 + 11*t2) + 
            Power(s1,7)*(46 + 8*Power(s2,2) + 32*Power(t1,2) - 
               3*s2*(9 + 12*t1 - 8*t2) - t1*(-24 + t2) + 15*t2 - 
               93*Power(t2,2)) + 
            2*Power(t2,3)*(-6 + 16*t2 - 11*Power(t2,2) + 
               2*Power(t2,3)) + 
            Power(s1,2)*t2*(-50 + (-91 + 75*s2)*t2 - 
               5*(-98 + 47*s2)*Power(t2,2) + 
               (-368 + 230*s2 - 4*Power(s2,2))*Power(t2,3) + 
               (33 - 50*s2 + 2*Power(s2,2))*Power(t2,4) - 
               2*Power(t2,5) + 
               2*Power(t1,2)*(-6 - 5*t2 + 2*Power(t2,2)) + 
               t1*(22 + (-71 + 28*s2)*t2 + (126 - 52*s2)*Power(t2,2) + 
                  3*(1 + 4*s2)*Power(t2,3) - 12*Power(t2,4))) + 
            s1*Power(t2,2)*(43 + t2 - 43*s2*t2 + 
               5*(-37 + 20*s2)*Power(t2,2) + 
               (115 - 56*s2)*Power(t2,3) + 2*(-5 + 3*s2)*Power(t2,4) + 
               t1*(-5 + 13*t2 + 7*Power(t2,2) - 14*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s1,3)*(13 + (47 - 29*s2)*t2 + 
               5*(-82 + 43*s2)*Power(t2,2) + 
               (655 - 459*s2 + 16*Power(s2,2))*Power(t2,3) - 
               2*(43 - 65*s2 + 4*Power(s2,2))*Power(t2,4) + 
               (-9 + 6*s2)*Power(t2,5) - 2*Power(t2,6) + 
               Power(t1,2)*(6 + 22*t2 - 8*Power(t2,2) + 
                  20*Power(t2,3)) + 
               t1*(-5 + (105 - 48*s2)*t2 + 
                  24*(-14 + 5*s2)*Power(t2,2) - 
                  6*(-9 + 8*s2)*Power(t2,3) + 
                  (-23 + 4*s2)*Power(t2,4) + 2*Power(t2,5))) - 
            Power(s1,6)*(5 + 206*t2 - 86*Power(t2,2) - 
               137*Power(t2,3) + Power(s2,2)*(4 + 22*t2) + 
               Power(t1,2)*(48 + 34*t2) + 
               t1*(41 + 80*t2 - 21*Power(t2,2)) - 
               s2*(23 + 99*t2 - 78*Power(t2,2) + 4*t1*(6 + 23*t2))) + 
            Power(s1,5)*(-75 + 220*t2 + 292*Power(t2,2) - 
               172*Power(t2,3) - 85*Power(t2,4) + 
               16*Power(s2,2)*t2*(1 + t2) + 
               2*Power(t1,2)*(-19 + 47*t2 + 8*Power(t2,2)) - 
               t1*(23 - 192*t2 + 59*Power(t2,2) + 31*Power(t2,3)) + 
               s2*(29 - 205*t2 - 69*Power(t2,2) + 90*Power(t2,3) - 
                  8*t1*(-4 + 10*t2 + 9*Power(t2,2)))) + 
            Power(s1,4)*(23 + 202*t2 - 633*Power(t2,2) + 
               4*Power(s2,2)*(-6 + t2)*Power(t2,2) - 63*Power(t2,3) + 
               98*Power(t2,4) + 21*Power(t2,5) - 
               2*Power(t1,2)*t2*(-21 + 39*t2 + 4*Power(t2,2)) + 
               t1*(-71 + 226*t2 - 170*Power(t2,2) + 134*Power(t2,3) + 
                  9*Power(t2,4)) + 
               s2*(-3 - 109*t2 + 467*Power(t2,2) - 89*Power(t2,3) - 
                  42*Power(t2,4) + 
                  4*t1*(5 - 25*t2 + 23*Power(t2,2) + 3*Power(t2,3))))) + 
         s*(s1 - t2)*(6*Power(s1,8)*(-1 + t2) - 
            2*Power(t2,2)*(-1 + 4*t2 - 4*Power(t2,2) + Power(t2,3)) - 
            s1*(-1 + t2)*t2*(-4 + (-13 + 10*s2 - 3*t1)*t2 + 
               (71 - 21*s2 - 6*t1)*Power(t2,2) + 
               2*(-9 + 2*s2 + 2*t1)*Power(t2,3)) + 
            Power(s1,7)*(18 + 10*Power(s2,2) + 5*t1 + 18*Power(t1,2) + 
               3*t1*t2 - 18*Power(t2,2) + s2*(-17 - 28*t1 + 9*t2)) + 
            Power(s1,2)*(8 - 4*(-4 + 3*s2)*t2 + 
               (-133 + 33*s2)*Power(t2,2) + (142 - 40*s2)*Power(t2,3) + 
               (-13 + 37*s2)*Power(t2,4) + (-20 + 6*s2)*Power(t2,5) + 
               Power(t1,2)*(6 - 4*t2 + 6*Power(t2,2)) + 
               4*t1*(-3 + (9 - 2*s2)*t2 + (-17 + 7*s2)*Power(t2,2) - 
                  5*s2*Power(t2,3) - 2*(-2 + s2)*Power(t2,4) + 
                  Power(t2,5))) + 
            Power(s1,6)*(27 - 76*t2 + 31*Power(t2,2) + 18*Power(t2,3) - 
               2*Power(t1,2)*(25 + 7*t2) - 2*Power(s2,2)*(7 + 13*t2) + 
               4*t1*(-4 - 11*t2 + Power(t2,2)) + 
               s2*(11 + 70*t2 - 25*Power(t2,2) + t1*(44 + 60*t2))) + 
            Power(s1,5)*(Power(t1,2)*(24 + 46*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(-4 + 42*t2 + 22*Power(t2,2)) + 
               t1*(32 + 142*t2 - 23*Power(t2,2) - 7*Power(t2,3)) - 
               3*(23 + 10*t2 - 51*Power(t2,2) + 16*Power(t2,3) + 
                  2*Power(t2,4)) - 
               s2*(-45 + 122*t2 + 92*Power(t2,2) - 25*Power(t2,3) + 
                  4*t1*(1 + 24*t2 + 9*Power(t2,2)))) + 
            Power(s1,4)*(15 + 174*t2 - 106*Power(t2,2) - 
               106*Power(t2,3) + 23*Power(t2,4) + 
               Power(t1,2)*(20 - 22*t2 - 22*Power(t2,2)) - 
               2*Power(s2,2)*t2*(-4 + 21*t2 + 3*Power(t2,2)) + 
               4*t1*(-11 - 37*t2 - 10*Power(t2,2) + 14*Power(t2,3)) + 
               s2*(-5 - 104*t2 + 248*Power(t2,2) + 48*Power(t2,3) - 
                  11*Power(t2,4) + 
                  4*t1*(1 + 3*t2 + 11*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,3)*(-21 + 46*t2 - 144*Power(t2,2) + 78*Power(t2,3) + 
               41*Power(t2,4) + 2*Power(s2,2)*Power(t2,2)*(-2 + 7*t2) + 
               2*Power(t1,2)*(-9 - 3*t2 + 5*Power(t2,2)) + 
               t1*(11 + 75*t2 + 82*Power(t2,2) - 46*Power(t2,3) - 
                  18*Power(t2,4)) + 
               s2*(2 + 3*t2 + 74*Power(t2,2) - 170*Power(t2,3) - 
                  15*Power(t2,4) + 2*Power(t2,5) + 
                  4*t1*(2 - 8*t2 + 3*Power(t2,2) + 4*Power(t2,3))))))*
       R1q(s1))/(s*(-1 + s1)*s1*
       (1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*(s - s2 + t1)*
       Power(s1 - t2,3)*(-s + s1 - t2)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) - 
    (8*(8*Power(s,5)*(s1 - t2)*Power(-1 + t2,2)*Power(t2,3) - 
         2*Power(s1 - t2,2)*(-1 + t2)*Power(t2,2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          (-1 + 2*Power(s1,2) + 3*t2 - s1*(3 + t2)) + 
         Power(s,4)*Power(t2,2)*
          (4*Power(s1,3)*(1 - 4*t2 + Power(t2,2)) + 
            2*Power(s1,2)*(6 + t1*Power(-1 + t2,2) - 25*t2 + 
               44*Power(t2,2) - 13*Power(t2,3)) + 
            t2*(-12 - 5*t1*Power(-1 + t2,3) + (27 + 8*s2)*t2 - 
               (37 + 16*s2)*Power(t2,2) + (49 + 8*s2)*Power(t2,3) - 
               19*Power(t2,4)) + 
            s1*(t1*Power(-1 + t2,2)*(1 + 9*t2) + 
               t2*(-21 - 8*s2*Power(-1 + t2,2) + 89*t2 - 
                  139*Power(t2,2) + 47*Power(t2,3)))) + 
         Power(s,3)*t2*(-4*Power(s1,4)*
             (-1 + (7 + t1)*t2 + (-9 - 2*s2 + t1)*Power(t2,2) + 
               Power(t2,3)) - 
            2*Power(s1,3)*(-1 + Power(t1,2)*(-1 + t2) + (23 + s2)*t2 - 
               (66 + 7*s2)*Power(t2,2) + (79 + 14*s2)*Power(t2,3) - 
               11*Power(t2,4) - 
               t1*(1 - (6 + s2)*t2 + (8 + s2)*Power(t2,2) + 
                  5*Power(t2,3))) + 
            Power(t2,2)*(33 - (24 + 31*s2)*t2 + 
               (-52 + 79*s2)*Power(t2,2) + (54 - 75*s2)*Power(t2,3) + 
               (27 + 19*s2)*Power(t2,4) - 14*Power(t2,5) + 
               Power(t1,2)*(3 - 7*t2 + 3*Power(t2,2) + Power(t2,3)) + 
               t1*(-27 + 2*(31 + s2)*t2 - 2*(25 + s2)*Power(t2,2) + 
                  36*Power(t2,3) - 13*Power(t2,4))) + 
            s1*t2*(-15 + 6*(1 + 7*s2)*t2 - 20*(1 + 6*s2)*Power(t2,2) + 
               12*(7 + 12*s2)*Power(t2,3) - (193 + 50*s2)*Power(t2,4) + 
               58*Power(t2,5) - 2*Power(t1,2)*(-2 + t2 + Power(t2,2)) + 
               t1*(3 - 44*t2 + (100 - 6*s2)*Power(t2,2) + 
                  2*(-59 + 3*s2)*Power(t2,3) + 43*Power(t2,4))) + 
            Power(s1,2)*(Power(t1,2)*
                (-1 + t2 - 5*Power(t2,2) + 5*Power(t2,3)) - 
               2*t1*(-1 + t2)*t2*
                (2 - 5*t2 + 18*Power(t2,2) + s2*(-1 + 4*t2)) + 
               t2*(-2 + 96*t2 - 224*Power(t2,2) + 288*Power(t2,3) - 
                  62*Power(t2,4) + 
                  s2*(-11 + 43*t2 - 83*Power(t2,2) + 51*Power(t2,3))))) + 
         Power(s,2)*(2*Power(s1,4)*
             (-1 + (-3 + s2*(-1 + t1) - 4*t1 - 4*Power(t1,2))*t2 + 
               (11 - Power(s2,2) + 5*t1 + 2*Power(t1,2) + 
                  s2*(12 + 5*t1))*Power(t2,2) - 
               (54 + 3*Power(s2,2) - 2*s2*(-13 + t1) + t1 + 
                  2*Power(t1,2))*Power(t2,3) + 
               (42 + 31*s2 - 16*t1)*Power(t2,4) + 5*Power(t2,5)) - 
            4*Power(s1,5)*t2*
             (2 - (7 + 2*s2)*t2 + 4*(1 + s2)*Power(t2,2) + 
               Power(t2,3) - t1*(-1 + t2 + 2*Power(t2,2))) + 
            Power(s1,2)*t2*(-2 + (-67 + 32*s2)*t2 + 
               (241 - 176*s2 + 6*Power(s2,2))*Power(t2,2) - 
               2*(203 - 143*s2 + 6*Power(s2,2))*Power(t2,3) - 
               2*(-38 + 129*s2 + 9*Power(s2,2))*Power(t2,4) + 
               (193 + 100*s2)*Power(t2,5) - 35*Power(t2,6) + 
               Power(t1,2)*(4 - 21*t2 + 15*Power(t2,2) - 
                  25*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(2 + (-52 + 9*s2)*t2 + (184 - 33*s2)*Power(t2,2) + 
                  (-214 + 81*s2)*Power(t2,3) + 
                  (168 - 9*s2)*Power(t2,4) - 72*Power(t2,5))) + 
            Power(t2,3)*(-16 + (-33 + 36*s2)*t2 + 
               (229 - 130*s2 + 2*Power(s2,2))*Power(t2,2) - 
               2*(168 - 71*s2 + Power(s2,2))*Power(t2,3) + 
               (166 - 78*s2)*Power(t2,4) + 7*(-1 + 2*s2)*Power(t2,5) - 
               3*Power(t2,6) + 
               Power(t1,2)*t2*
                (5 - 11*t2 + 5*Power(t2,2) + Power(t2,3)) + 
               t1*(16 - (58 + 5*s2)*t2 + (72 + 11*s2)*Power(t2,2) - 
                  (12 + 7*s2)*Power(t2,3) + (8 + s2)*Power(t2,4) - 
                  10*Power(t2,5))) + 
            s1*Power(t2,2)*(24 + (77 - 66*s2)*t2 + 
               (-443 + 274*s2 - 6*Power(s2,2))*Power(t2,2) + 
               (678 - 340*s2 + 8*Power(s2,2))*Power(t2,3) + 
               (-296 + 228*s2 + 6*Power(s2,2))*Power(t2,4) - 
               7*(9 + 8*s2)*Power(t2,5) + 23*Power(t2,6) - 
               Power(t1,2)*(-3 + 5*t2 - 11*Power(t2,2) + Power(t2,3)) + 
               t1*(-29 - (-126 + s2)*t2 + 9*(-26 + s2)*Power(t2,2) + 
                  (166 - 29*s2)*Power(t2,3) + 
                  5*(-23 + s2)*Power(t2,4) + 46*Power(t2,5))) - 
            Power(s1,3)*(Power(t1,2)*
                (1 - 9*t2 + 7*Power(t2,2) - 29*Power(t2,3) + 
                  6*Power(t2,4)) + 
               t1*(1 + (-8 + 3*s2)*t2 + (14 - 11*s2)*Power(t2,2) + 
                  5*(-6 + 11*s2)*Power(t2,3) + (51 + s2)*Power(t2,4) - 
                  60*Power(t2,5)) - 
               t2*(13 - 21*t2 + 62*Power(t2,2) + 128*Power(t2,3) - 
                  191*Power(t2,4) + 9*Power(t2,5) + 
                  2*Power(s2,2)*t2*(-1 + 4*t2 + 9*Power(t2,2)) - 
                  2*s2*(1 - 17*t2 + 56*Power(t2,2) - 76*Power(t2,3) + 
                     52*Power(t2,4))))) + 
         s*(s1 - t2)*(4*Power(s1,5)*(-1 + t2)*t2*
             (-1 + 2*s2*t2 + Power(t2,2) - t1*(1 + t2)) + 
            2*Power(s1,4)*(2 + (-11 + 2*s2)*t2 - 
               (-6 + 5*s2 + Power(s2,2))*Power(t2,2) + 
               (8 + 16*s2 + 5*Power(s2,2))*Power(t2,3) - 
               13*s2*Power(t2,4) - 5*Power(t2,5) + 
               Power(t1,2)*t2*(3 - 3*t2 + 4*Power(t2,2)) + 
               t1*(1 - (4 + s2)*t2 + Power(t2,2) - 
                  (6 + 7*s2)*Power(t2,3) + 8*Power(t2,4))) + 
            Power(t2,2)*(5 + 13*(-4 + s2)*t2 + 
               (197 - 73*s2 + 2*Power(s2,2))*Power(t2,2) - 
               4*(79 - 25*s2 + Power(s2,2))*Power(t2,3) + 
               (227 - 60*s2 + 10*Power(s2,2))*Power(t2,4) + 
               (-64 + 23*s2)*Power(t2,5) - 3*(-1 + s2)*Power(t2,6) + 
               Power(t1,2)*(3 - 10*t2 + 10*Power(t2,2) + 
                  6*Power(t2,3) - Power(t2,4)) + 
               t1*(-6 + (16 - 3*s2)*t2 + 2*(-4 + 7*s2)*Power(t2,2) + 
                  (30 - 28*s2)*Power(t2,3) + 2*(-22 + s2)*Power(t2,4) - 
                  (-10 + s2)*Power(t2,5) + 2*Power(t2,6))) + 
            Power(s1,2)*(1 + (-32 + 3*s2)*t2 + 
               3*(71 - 25*s2 + 2*Power(s2,2))*Power(t2,2) - 
               4*(113 - 20*s2 + Power(s2,2))*Power(t2,3) + 
               3*(119 - 14*s2 + 8*Power(s2,2))*Power(t2,4) + 
               (-54 + 65*s2 + 22*Power(s2,2))*Power(t2,5) - 
               (35 + 31*s2)*Power(t2,6) + 2*Power(t2,7) + 
               Power(t1,2)*(-1 - 10*t2 + 36*Power(t2,2) - 
                  2*Power(t2,3) + 23*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(2 + (-24 + 7*s2)*t2 + (174 - 38*s2)*Power(t2,2) + 
                  (-218 + 30*s2)*Power(t2,3) + 
                  (82 - 90*s2)*Power(t2,4) - (34 + 5*s2)*Power(t2,5) + 
                  18*Power(t2,6))) + 
            Power(s1,3)*(-(t1*Power(-1 + t2,2)*
                  (-3 + 41*t2 + 33*Power(t2,2) + 19*Power(t2,3))) - 
               Power(t1,2)*(-3 + 22*t2 - 24*Power(t2,2) + 
                  36*Power(t2,3) + Power(t2,4)) + 
               2*Power(-1 + t2,2)*
                (2 - 22*t2 + 32*Power(t2,2) + 17*Power(t2,3) + 
                  3*Power(t2,4)) - 
               2*Power(s2,2)*(t2 + 2*Power(t2,3) + 13*Power(t2,4)) + 
               s2*(Power(-1 + t2,2)*
                   (1 + 15*t2 + 21*Power(t2,2) + 37*Power(t2,3)) + 
                  t1*(-1 + 14*t2 - 14*Power(t2,2) + 42*Power(t2,3) + 
                     23*Power(t2,4)))) + 
            s1*t2*(Power(t1,2)*
                (4 - 5*t2 - 4*Power(t2,2) - 22*Power(t2,3) - 
                  6*Power(t2,4) + Power(t2,5)) + 
               t1*(-8 + (49 - 3*s2)*t2 + (-161 + 10*s2)*Power(t2,2) + 
                  2*(60 + 7*s2)*Power(t2,3) + (2 + 46*s2)*Power(t2,4) + 
                  (11 - 3*s2)*Power(t2,5) - 13*Power(t2,6)) + 
               t2*(-2*Power(s2,2)*t2*
                   (3 - 4*t2 + 14*Power(t2,2) + 3*Power(t2,3)) - 
                  2*Power(-1 + t2,2)*
                   (-29 + 113*t2 - 68*Power(t2,2) - 5*Power(t2,3) + 
                     Power(t2,4)) + 
                  s2*(-17 + 135*t2 - 176*Power(t2,2) + 102*Power(t2,3) - 
                     59*Power(t2,4) + 15*Power(t2,5))))))*R1q(t2))/
     (s*(-1 + s1)*(s - s2 + t1)*Power(s1 - t2,3)*(-s + s1 - t2)*
       Power(-1 + t2,2)*t2*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) - 
    (8*(6*Power(s,6)*t2 + Power(s,5)*
          (2*Power(t1,2) + s1*(6 - 18*t2) - t1*t2 + 
            t2*(-38 - 6*s2 + 11*t2)) + 
         Power(s,4)*(12*Power(s1,2)*(-1 + t2) + 
            Power(t1,2)*(-4 + 3*t2) + t1*(4 - 21*t2 - 3*Power(t2,2)) + 
            t2*(25 + s2*(46 - 9*t2) - 45*t2 + 6*Power(t2,2)) + 
            s1*(-32 - 12*Power(t1,2) + 5*t1*(-1 + t2) + 105*t2 - 
               20*Power(t2,2) + s2*(-6 + 4*t1 + 15*t2))) + 
         Power(s,3)*(2 + 78*t2 + 12*Power(s1,3)*t2 - 68*s2*t2 - 
            2*Power(s2,2)*t2 - 20*Power(t2,2) + 58*s2*Power(t2,2) - 
            2*Power(s2,2)*Power(t2,2) - 12*Power(t2,3) - 
            4*s2*Power(t2,3) + Power(t2,4) + 
            Power(t1,2)*(-2 - 4*t2 + Power(t2,2)) + 
            t1*(-4 + 55*t2 + (-16 + s2)*Power(t2,2) - 2*Power(t2,3)) + 
            Power(s1,2)*(58 + 2*Power(s2,2) + s2*(5 - 20*t1) + 
               28*Power(t1,2) - 105*t2 - 4*t1*(-5 + 4*t2)) + 
            s1*(-11 + Power(t1,2)*(7 - 18*t2) + 60*t2 + 
               2*Power(s2,2)*t2 + 69*Power(t2,2) - 8*Power(t2,3) + 
               s2*(44 + (-131 + 11*t1)*t2 + 8*Power(t2,2)) + 
               t1*(-38 + 72*t2 + 17*Power(t2,2)))) + 
         Power(s,2)*(-6*Power(s1,4)*(-2 + 3*t2) + 
            Power(t1,2)*(10 - 2*t2 + Power(t2,2)) + 
            t1*(-8 - (17 + 8*s2)*t2 - 7*(-5 + s2)*Power(t2,2) + 
               s2*Power(t2,3)) + 
            Power(s1,3)*(-50 - 8*Power(s2,2) - 32*Power(t1,2) + 
               s2*(21 + 36*t1 - 26*t2) + 53*t2 + 16*Power(t2,2) + 
               4*t1*(-8 + 7*t2)) - 
            t2*(82 - 90*t2 + 13*Power(t2,2) + Power(t2,3) + 
               2*Power(s2,2)*(-3 - 3*t2 + Power(t2,2)) + 
               s2*(-12 + 54*t2 - 22*Power(t2,2) + Power(t2,3))) + 
            s1*(63 - 239*t2 + 133*Power(t2,2) + 4*Power(t2,3) - 
               2*Power(t2,4) + Power(t1,2)*(9 + 6*t2 - 5*Power(t2,2)) + 
               2*Power(s2,2)*(-1 + 2*t2 + 5*Power(t2,2)) + 
               t1*(33 + 17*t2 + 63*Power(t2,2) + 7*Power(t2,3)) + 
               s2*(-24 + 71*t2 - 109*Power(t2,2) + 3*Power(t2,3) - 
                  8*t1*(1 + 2*t2))) + 
            Power(s1,2)*(129 + Power(s2,2)*(6 - 4*t2) - 111*t2 - 
               50*Power(t2,2) + 2*Power(t2,3) + 
               9*Power(t1,2)*(1 + 4*t2) + 
               t1*(96 - 75*t2 - 35*Power(t2,2)) + 
               s2*(-99 + 118*t2 + 18*Power(t2,2) - t1*(13 + 33*t2)))) - 
         Power(-1 + s1,2)*(-2 + 17*t2 + 10*s2*t2 - 2*Power(s2,2)*t2 - 
            13*Power(t2,2) + s2*Power(t2,2) - 
            4*Power(s2,2)*Power(t2,2) - Power(t2,3) - 
            12*s2*Power(t2,3) + 2*Power(s2,2)*Power(t2,3) - 
            Power(t2,4) + s2*Power(t2,4) + 
            Power(t1,2)*(-2 + t2 - 3*Power(t2,2)) + 
            t1*(4 + (-34 + 8*s2)*t2 + (18 + s2)*Power(t2,2) - 
               (-12 + s2)*Power(t2,3)) + 
            Power(s1,3)*(-2 + 4*Power(s2,2) + 7*t1 + 4*Power(t1,2) + 
               2*t2 - 7*t1*t2 + s2*(-5 - 8*t1 + 5*t2)) + 
            Power(s1,2)*(1 + 2*t2 - 3*Power(t2,2) - 
               4*Power(s2,2)*(2 + t2) - 3*Power(t1,2)*(1 + 3*t2) + 
               2*t1*(-7 + 2*t2 + 5*Power(t2,2)) + 
               s2*(11 - 4*t2 - 7*Power(t2,2) + t1*(13 + 11*t2))) + 
            s1*(-15 + 14*t2 - Power(t2,2) + 2*Power(t2,3) - 
               2*Power(s2,2)*(-1 - 6*t2 + Power(t2,2)) + 
               Power(t1,2)*(1 + 8*t2 + 3*Power(t2,2)) - 
               t1*(-30 + 8*t2 + 19*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(-10 - 12*t2 + 21*Power(t2,2) + Power(t2,3) - 
                  2*t1*(4 + 7*t2 + Power(t2,2))))) + 
         s*(-4 + 12*t1 - 8*Power(t1,2) + 6*Power(s1,5)*(-1 + t2) + 
            32*t2 + 26*s2*t2 - 6*Power(s2,2)*t2 - 50*t1*t2 + 
            16*s2*t1*t2 + 4*Power(t1,2)*t2 - 5*Power(t2,2) + 
            2*s2*Power(t2,2) - 8*Power(s2,2)*Power(t2,2) - 
            6*t1*Power(t2,2) + 7*s2*t1*Power(t2,2) - 
            5*Power(t1,2)*Power(t2,2) - 6*Power(t2,3) - 
            26*s2*Power(t2,3) + 4*Power(s2,2)*Power(t2,3) + 
            22*t1*Power(t2,3) - 2*s2*t1*Power(t2,3) - Power(t2,4) + 
            2*s2*Power(t2,4) + 
            Power(s1,4)*(22 + 10*Power(s2,2) + 18*Power(t1,2) + 
               t1*(24 - 23*t2) - 13*t2 - 7*Power(t2,2) + 
               s2*(-25 - 28*t1 + 22*t2)) + 
            Power(s1,3)*(-49 + 20*t2 + 23*Power(t2,2) - 
               2*Power(s2,2)*(11 + t2) - Power(t1,2)*(23 + 30*t2) + 
               t1*(-82 + 42*t2 + 31*Power(t2,2)) + 
               s2*(88 - 47*t2 - 24*Power(t2,2) + t1*(42 + 33*t2))) + 
            Power(s1,2)*(-16 + 91*t2 - 52*Power(t2,2) - 2*Power(t2,3) + 
               Power(t2,4) + Power(s2,2)*(8 + 10*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(12 + 24*t2 + 7*Power(t2,2)) - 
               t1*(-146 + 67*t2 + 78*Power(t2,2) + 8*Power(t2,3)) - 
               s2*(75 + 30*t2 - 82*Power(t2,2) - 2*Power(t2,3) + 
                  t1*(22 + 20*t2 + 3*Power(t2,2)))) + 
            s1*(-11 - 12*t2 - 19*Power(t2,2) + 12*Power(t2,3) - 
               4*Power(t2,4) + Power(t1,2)*(1 + 2*t2 - 2*Power(t2,2)) + 
               2*Power(s2,2)*(2 + 3*t2 + Power(t2,2) + 2*Power(t2,3)) + 
               t1*(8 - 82*t2 + 89*Power(t2,2) + 22*Power(t2,3)) - 
               s2*(20 - 57*t2 + 16*Power(t2,2) + 20*Power(t2,3) - 
                  2*Power(t2,4) + 
                  t1*(4 + 9*t2 + 8*Power(t2,2) + 2*Power(t2,3))))))*R2q(s))/
     (s*(-1 + s1)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       (s - s2 + t1)*(-s + s1 - t2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) \
+ (8*(4*Power(s,8)*Power(t2,2) + 
         Power(s,7)*(Power(t1,2) + t1*t2*(-3 + s1 + s2 + 2*t2) + 
            t2*(s1*(8 - 21*t2) + t2*(-28 - 5*s2 + 10*t2))) + 
         Power(s,6)*(4*Power(t1,2)*(-1 + t2) + 
            Power(s1,2)*(4 + t1 + (-35 + s2)*t2 - 6*t1*t2 + 
               43*Power(t2,2)) + 
            2*t1*(1 - 3*(-3 + s2)*t2 + (-16 + s2)*Power(t2,2) + 
               2*Power(t2,3)) + 
            t2*(-1 + 40*t2 + Power(s2,2)*t2 - 50*Power(t2,2) + 
               8*Power(t2,3) + s2*(-1 + 43*t2 - 12*Power(t2,2))) + 
            s1*(-9*Power(t1,2) + 
               t1*(-5 + s2*(3 - 7*t2) + 21*t2 - 9*Power(t2,2)) + 
               t2*(-46 + Power(s2,2) + 127*t2 - 41*Power(t2,2) + 
                  s2*(-12 + 25*t2)))) + 
         Power(s,5)*(1 + 9*t2 - s2*t2 + 94*Power(t2,2) - 
            114*s2*Power(t2,2) - 10*Power(s2,2)*Power(t2,2) + 
            2*Power(t2,3) + 82*s2*Power(t2,3) + 
            2*Power(s2,2)*Power(t2,3) - 27*Power(t2,4) - 
            9*s2*Power(t2,4) + 2*Power(t2,5) + 
            Power(t1,2)*(4 - 15*t2 + 4*Power(t2,2)) + 
            t1*(-6 + (-42 + 17*s2)*t2 + (111 - 8*s2)*Power(t2,2) + 
               (-47 + s2)*Power(t2,3) + 2*Power(t2,4)) + 
            Power(s1,3)*(-14 + s2 + 56*t2 - 5*s2*t2 - 40*Power(t2,2) + 
               5*t1*(-1 + 3*t2)) + 
            Power(s1,2)*(-17 + 34*Power(t1,2) + 
               Power(s2,2)*(2 - 6*t2) + 145*t2 - 221*Power(t2,2) + 
               61*Power(t2,3) + t1*(31 - 67*t2 + 14*Power(t2,2)) + 
               s2*(-9 + 47*t2 - 44*Power(t2,2) + t1*(-22 + 20*t2))) + 
            s1*(-3 + 24*t2 - 4*Power(s2,2)*t2 - 86*Power(t2,2) + 
               147*Power(t2,3) - 25*Power(t2,4) - 
               Power(t1,2)*(-23 + 29*t2 + Power(t2,2)) + 
               t1*(5 - 131*t2 + 154*Power(t2,2) - 14*Power(t2,3)) + 
               s2*(1 + 101*t2 - 204*Power(t2,2) + 46*Power(t2,3) + 
                  t1*(-9 + 36*t2 - 11*Power(t2,2))))) + 
         Power(s,4)*(-2 - 31*t2 + 14*s2*t2 - 309*Power(t2,2) + 
            79*s2*Power(t2,2) + 32*Power(s2,2)*Power(t2,2) + 
            310*Power(t2,3) - 154*s2*Power(t2,3) - 
            16*Power(s2,2)*Power(t2,3) - 41*Power(t2,4) + 
            51*s2*Power(t2,4) + Power(s2,2)*Power(t2,4) - 
            4*Power(t2,5) - 2*s2*Power(t2,5) + 
            Power(t1,2)*(5 + 17*t2 - 12*Power(t2,2) + 2*Power(t2,3)) + 
            t1*(2 + (65 - 30*s2)*t2 + 2*(-79 + 9*s2)*Power(t2,2) + 
               (111 - 4*s2)*Power(t2,3) - 24*Power(t2,4)) + 
            2*Power(s1,4)*(8 + t1*(5 - 10*t2) - 17*t2 + 5*Power(t2,2) + 
               s2*(-2 + 5*t2)) + 
            Power(s1,3)*(32 - 70*Power(t1,2) - 162*t2 + 
               180*Power(t2,2) - 34*Power(t2,3) + 
               2*Power(s2,2)*(-6 + 7*t2) - 
               5*t1*(17 - 24*t2 + Power(t2,2)) + 
               s2*(33 + t1*(66 - 30*t2) - 64*t2 + 25*Power(t2,2))) + 
            Power(s1,2)*(-1 + 101*t2 - 21*Power(t2,2) - 
               156*Power(t2,3) + 26*Power(t2,4) + 
               Power(s2,2)*(-4 + 28*t2 - 15*Power(t2,2)) + 
               Power(t1,2)*(-42 + 85*t2 + 5*Power(t2,2)) + 
               t1*(-38 + 347*t2 - 308*Power(t2,2) + 15*Power(t2,3)) + 
               s2*(43 - 350*t2 + 359*Power(t2,2) - 60*Power(t2,3) + 
                  6*t1*(5 - 16*t2 + 4*Power(t2,2)))) + 
            s1*(5 + 230*t2 - 581*Power(t2,2) + 134*Power(t2,3) + 
               48*Power(t2,4) - 4*Power(t2,5) + 
               4*Power(s2,2)*t2*(-2 + 5*t2) + 
               Power(t1,2)*(-11 + 56*t2 - 19*Power(t2,2)) + 
               t1*(-9 + 226*t2 - 335*Power(t2,2) + 164*Power(t2,3) - 
                  6*Power(t2,4)) + 
               s2*(-4 - 180*t2 + 471*Power(t2,2) - 270*Power(t2,3) + 
                  27*Power(t2,4) + 
                  t1*(10 - 62*t2 + 24*Power(t2,2) - 6*Power(t2,3))))) - 
         Power(-1 + s1,3)*(1 - 2*t1 + Power(t1,2) - 10*t2 + 2*s2*t2 + 
            13*t1*t2 - 2*s2*t1*t2 - 3*Power(t1,2)*t2 + 10*Power(t2,2) - 
            7*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) - 
            8*t1*Power(t2,2) + 4*Power(t1,2)*Power(t2,2) + 
            4*Power(t2,3) - 2*s2*Power(t2,3) + 
            3*Power(s2,2)*Power(t2,3) - 3*t1*Power(t2,3) - 
            4*s2*t1*Power(t2,3) + 2*Power(t1,2)*Power(t2,3) - 
            3*Power(t2,4) + 5*s2*Power(t2,4) - 2*s2*t1*Power(t2,4) - 
            2*Power(t2,5) + 2*s2*Power(t2,5) + 
            Power(s1,4)*(4*Power(s2,2) + 
               t1*(3 + 4*t1 - 2*t2 - Power(t2,2)) + 
               s2*(-3 - 8*t1 + 2*t2 + Power(t2,2))) + 
            Power(s1,3)*(Power(-1 + t2,2)*(3 + t2) + 
               Power(s2,2)*(-4 - 13*t2 + Power(t2,2)) - 
               Power(t1,2)*(6 + 9*t2 + Power(t2,2)) + 
               t1*(1 - 9*t2 + 7*Power(t2,2) + Power(t2,3)) - 
               2*s2*(2 - 7*t2 + 4*Power(t2,2) + Power(t2,3) - 
                  t1*(5 + 11*t2))) + 
            Power(s1,2)*(-(Power(-1 + t2,2)*t2*(7 + t2)) + 
               Power(t1,2)*(-1 + 17*t2 + 8*Power(t2,2)) + 
               t1*(7 - 3*t2 + Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,2)*(1 + 11*t2 + 14*Power(t2,2) - 
                  2*Power(t2,3)) + 
               s2*(-5 + 8*t2 - 16*Power(t2,2) + 12*Power(t2,3) + 
                  Power(t2,4) - 2*t1*(1 + 13*t2 + 10*Power(t2,2)))) + 
            s1*(Power(-1 + t2,2)*(9 + 6*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-2 - 10*t2 - 5*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(-2 + 5*t2 + 11*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-11 + 5*t2 + Power(t2,2) + 3*Power(t2,3) + 
                  2*Power(t2,4)) + 
               2*s2*(-1 + 6*t2 - Power(t2,2) - 4*Power(t2,4) + 
                  t1*(1 + t2 + 10*Power(t2,2) + 4*Power(t2,3))))) + 
         Power(s,3)*(-1 + 12*t1 - 15*Power(t1,2) + 59*t2 - 28*s2*t2 - 
            91*t1*t2 + 35*s2*t1*t2 + 2*Power(t1,2)*t2 + 
            253*Power(t2,2) + 95*s2*Power(t2,2) - 
            47*Power(s2,2)*Power(t2,2) + 118*t1*Power(t2,2) - 
            26*s2*t1*Power(t2,2) + 8*Power(t1,2)*Power(t2,2) - 
            476*Power(t2,3) + 12*s2*Power(t2,3) + 
            37*Power(s2,2)*Power(t2,3) - 96*t1*Power(t2,3) + 
            18*s2*t1*Power(t2,3) - 8*Power(t1,2)*Power(t2,3) + 
            212*Power(t2,4) - 76*s2*Power(t2,4) - 
            7*Power(s2,2)*Power(t2,4) + 44*t1*Power(t2,4) - 
            18*Power(t2,5) + 10*s2*Power(t2,5) - 4*t1*Power(t2,5) + 
            Power(s1,5)*(-4 + s2*(6 - 10*t2) - 4*t2 + 11*Power(t2,2) + 
               5*t1*(-2 + 3*t2)) + 
            Power(s1,4)*(85*Power(t1,2) - 4*Power(s2,2)*(-7 + 4*t2) + 
               t1*(122 - 125*t2 - 10*Power(t2,2)) - 
               2*(7 - 38*t2 + 32*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-48 + 32*t2 + 15*Power(t2,2) + t1*(-104 + 25*t2))) + 
            Power(s1,3)*(60 - 254*t2 + 134*Power(t2,2) + 
               81*Power(t2,3) - 8*Power(t2,4) - 
               10*Power(t1,2)*(-1 + 13*t2 + Power(t2,2)) + 
               Power(s2,2)*(10 - 75*t2 + 36*Power(t2,2)) + 
               t1*(38 - 434*t2 + 330*Power(t2,2)) - 
               2*s2*(50 - 231*t2 + 151*Power(t2,2) - 11*Power(t2,3) + 
                  t1*(3 - 77*t2 + 13*Power(t2,2)))) + 
            s1*(-18 - 411*t2 + 1205*Power(t2,2) - 766*Power(t2,3) + 
               108*Power(t2,4) + 2*Power(t2,5) + 
               Power(s2,2)*t2*(40 - 74*t2 + 23*Power(t2,2)) - 
               2*Power(t1,2)*
                (7 + 7*t2 - 11*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(40 - 199*t2 + 140*Power(t2,2) - 142*Power(t2,3) + 
                  70*Power(t2,4)) + 
               2*s2*(4 + 9*t2 - 119*Power(t2,2) + 190*Power(t2,3) - 
                  57*Power(t2,4) + 2*Power(t2,5) + 
                  t1*(-4 + 24*t2 - 12*Power(t2,2) + 3*Power(t2,3)))) + 
            Power(s1,2)*(128 - 752*t2 + 883*Power(t2,2) - 
               263*Power(t2,3) - 29*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,2)*(-2 - 34*t2 + 44*Power(t2,2)) + 
               Power(s2,2)*(-5 + 15*t2 + 19*Power(t2,2) - 
                  16*Power(t2,3)) + 
               t1*(58 - 206*t2 + 338*Power(t2,2) - 238*Power(t2,3) + 
                  6*Power(t2,4)) + 
               2*s2*(-27 + 184*t2 - 335*Power(t2,2) + 165*Power(t2,3) - 
                  13*Power(t2,4) + 
                  t1*(-3 + 19*t2 - 20*Power(t2,2) + 6*Power(t2,3))))) + 
         s*(-4 + 10*t1 - 6*Power(t1,2) - 
            Power(s1,7)*(2 + s2 - t1 - 2*t2)*(-1 + t2) + 40*t2 - 
            11*s2*t2 - 56*t1*t2 + 11*s2*t1*t2 + 13*Power(t1,2)*t2 - 
            53*Power(t2,2) + 72*s2*Power(t2,2) - 
            11*Power(s2,2)*Power(t2,2) + 33*t1*Power(t2,2) - 
            6*s2*t1*Power(t2,2) - 12*Power(t1,2)*Power(t2,2) + 
            74*Power(t2,3) - 98*s2*Power(t2,3) + 
            5*Power(s2,2)*Power(t2,3) - 9*t1*Power(t2,3) + 
            21*s2*t1*Power(t2,3) - 8*Power(t1,2)*Power(t2,3) - 
            57*Power(t2,4) + 35*s2*Power(t2,4) - 
            7*Power(s2,2)*Power(t2,4) + 34*t1*Power(t2,4) - 
            2*s2*Power(t2,5) - 8*t1*Power(t2,5) + 
            Power(s1,6)*(-1 + 24*Power(t1,2) - 
               2*Power(s2,2)*(-9 + t2) + 3*t2 + Power(t2,2) - 
               3*Power(t2,3) + t1*(31 - 21*t2 - 6*Power(t2,2)) + 
               s2*(-15 + 2*t1*(-21 + t2) + t2 + 10*Power(t2,2))) + 
            Power(s1,3)*(-36 - 103*t2 + 72*Power(t2,2) + 
               31*Power(t2,3) + 34*Power(t2,4) + 2*Power(t2,5) - 
               2*Power(t1,2)*
                (-1 + 33*t2 + 31*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(47 + (115 + 74*t1)*t2 + 
                  4*(16 + 27*t1)*Power(t2,2) + 
                  6*(-3 + t1)*Power(t2,3) - 32*Power(t2,4)) + 
               Power(s2,2)*(-8 + 21*t2 - 66*Power(t2,2) - 
                  5*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(-51 - 87*t2 - 130*Power(t2,2) + 58*Power(t2,3) + 
                  34*Power(t2,4))) + 
            Power(s1,5)*(-1 - 20*t2 + 8*Power(t2,2) + 12*Power(t2,3) + 
               Power(t2,4) - Power(t1,2)*(57 + 49*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(-26 - 57*t2 + 12*Power(t2,2)) + 
               t1*(-47 - 67*t2 + 68*Power(t2,2) + 6*Power(t2,3)) + 
               s2*(5 + 93*t2 - 46*Power(t2,2) - 12*Power(t2,3) + 
                  t1*(95 + 90*t2 - 3*Power(t2,2)))) + 
            Power(s1,2)*(-6 + 158*t2 + 13*Power(t2,2) - 
               112*Power(t2,3) - 45*Power(t2,4) - 8*Power(t2,5) + 
               2*Power(t1,2)*
                (4 - 11*t2 + 6*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(-9 + 13*t2 - 68*Power(t2,2) + 
                  33*Power(t2,3) - 3*Power(t2,4)) + 
               t1*(-21 + 53*t2 + 66*Power(t2,2) + 68*Power(t2,3) - 
                  34*Power(t2,4) - 8*Power(t2,5)) + 
               s2*(41 - 165*t2 - 34*Power(t2,2) + 2*Power(t2,3) + 
                  30*Power(t2,4) + 2*Power(t2,5) + 
                  t1*(8 + 30*t2 + 6*Power(t2,2) - 16*Power(t2,3)))) + 
            Power(s1,4)*(53 - 41*t2 + 39*Power(t2,2) - 39*Power(t2,3) - 
               12*Power(t2,4) + 
               Power(t1,2)*(30 + 121*t2 + 40*Power(t2,2)) + 
               t1*(52 + 160*t2 - 21*Power(t2,2) - 67*Power(t2,3)) + 
               Power(s2,2)*(9 + 69*t2 + 47*Power(t2,2) - 
                  14*Power(t2,3)) + 
               s2*(-54 - 57*t2 - 88*Power(t2,2) + 72*Power(t2,3) + 
                  3*Power(t2,4) + 
                  t1*(-54 - 195*t2 - 56*Power(t2,2) + 3*Power(t2,3)))) - 
            s1*(23 - 31*t2 + 178*Power(t2,2) - 101*Power(t2,3) - 
               63*Power(t2,4) - 6*Power(t2,5) + 
               5*Power(s2,2)*t2*
                (-4 + 2*t2 - 9*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,2)*(-1 + 3*t2 + 27*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-27 - 17*t2 + 10*Power(t2,2) + 56*Power(t2,3) + 
                  34*Power(t2,4) - 16*Power(t2,5)) + 
               s2*(-7 + 103*t2 - 214*Power(t2,2) + 74*Power(t2,3) + 
                  4*Power(t2,4) + 
                  t1*(7 + 12*t2 + 49*Power(t2,2) + 14*Power(t2,3))))) + 
         Power(s,2)*(5 - 18*t1 + 14*Power(t1,2) - 66*t2 + 25*s2*t2 + 
            96*t1*t2 - 26*s2*t1*t2 - 18*Power(t1,2)*t2 - 11*Power(t2,2) - 
            163*s2*Power(t2,2) + 34*Power(s2,2)*Power(t2,2) - 
            66*t1*Power(t2,2) + 20*s2*t1*Power(t2,2) + 
            8*Power(t1,2)*Power(t2,2) + 122*Power(t2,3) + 
            172*s2*Power(t2,3) - 31*Power(s2,2)*Power(t2,3) + 
            40*t1*Power(t2,3) - 32*s2*t1*Power(t2,3) + 
            12*Power(t1,2)*Power(t2,3) - 136*Power(t2,4) - 
            2*s2*Power(t2,4) + 13*Power(s2,2)*Power(t2,4) - 
            48*t1*Power(t2,4) + 2*s2*t1*Power(t2,4) + 46*Power(t2,5) - 
            12*s2*Power(t2,5) + 4*t1*Power(t2,5) + 
            Power(s1,6)*(-4 + t1*(5 - 6*t2) + 13*t2 - 9*Power(t2,2) + 
               s2*(-4 + 5*t2)) + 
            Power(s1,5)*(-61*Power(t1,2) + Power(s2,2)*(-32 + 9*t2) + 
               s2*(36 + t1*(91 - 11*t2) - 2*t2 - 25*Power(t2,2)) + 
               t2*(-16 + 5*t2 + 11*Power(t2,2)) + 
               t1*(-91 + 73*t2 + 13*Power(t2,2))) + 
            Power(s1,4)*(-36 + 145*t2 - 74*Power(t2,2) - 
               33*Power(t2,3) - 2*Power(t2,4) + 
               Power(s2,2)*(4 + 95*t2 - 33*Power(t2,2)) + 
               2*Power(t1,2)*(26 + 55*t2 + 5*Power(t2,2)) + 
               t1*(32 + 264*t2 - 202*Power(t2,2) - 10*Power(t2,3)) + 
               s2*(56 - 297*t2 + 139*Power(t2,2) + 14*Power(t2,3) + 
                  2*t1*(-38 - 78*t2 + 7*Power(t2,2)))) - 
            Power(s1,2)*(102 - 630*t2 + 866*Power(t2,2) - 
               359*Power(t2,3) + 77*Power(t2,4) - 
               2*Power(t1,2)*
                (3 - 10*t2 + 7*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(-15 + 63*t2 - 67*Power(t2,2) + 
                  Power(t2,3) + 4*Power(t2,4)) + 
               t1*(41 + 54*t2 - 160*Power(t2,2) + 6*Power(t2,3) + 
                  78*Power(t2,4)) + 
               s2*(26 - 49*t2 - 196*Power(t2,2) + 242*Power(t2,3) - 
                  84*Power(t2,4) + 2*Power(t2,5) + 
                  2*t1*(1 - 7*t2 + 11*Power(t2,2)))) + 
            Power(s1,3)*(-208 + 448*t2 - 409*Power(t2,2) + 
               158*Power(t2,3) + 19*Power(t2,4) + 
               Power(t1,2)*(8 - 84*t2 - 58*Power(t2,2)) + 
               13*Power(s2,2)*
                (1 - 2*t2 - 5*Power(t2,2) + 2*Power(t2,3)) - 
               2*t1*(21 + 60*t2 + 58*Power(t2,2) - 90*Power(t2,3) + 
                  Power(t2,4)) + 
               2*s2*(26 - 91*t2 + 206*Power(t2,2) - 98*Power(t2,3) + 
                  3*Power(t2,4) + 
                  t1*(-3 + 55*t2 + 30*Power(t2,2) - 5*Power(t2,3)))) + 
            s1*(33 + 134*t2 - 540*Power(t2,2) + 631*Power(t2,3) - 
               180*Power(t2,4) + 10*Power(t2,5) + 
               Power(s2,2)*t2*
                (-47 + 77*t2 - 58*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,2)*(13 - 20*t2 - 6*Power(t2,2) + 8*Power(t2,3)) + 
               t1*(-53 + 99*t2 + 35*Power(t2,2) - 60*Power(t2,3) + 
                  12*Power(t2,5)) - 
               s2*(10 - 170*t2 + 271*Power(t2,2) + 68*Power(t2,3) - 
                  96*Power(t2,4) + 10*Power(t2,5) + 
                  t1*(-9 + 11*t2 - 40*Power(t2,2) + 6*Power(t2,3) + 
                     2*Power(t2,4))))))*T2q(s1,s))/
     (s*(-1 + s1)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       (s - s2 + t1)*(-s + s1 - t2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) + 
    (8*(-4*Power(s,6)*(1 + s1)*(s1 - t2)*Power(t2,2) + 
         2*Power(s,5)*t2*(Power(s1,3)*(-4 + 8*t2) + 
            t2*(2 - t1 + (3 - 2*s2)*t2 + 6*Power(t2,2)) + 
            s1*t2*(8 + t1*(-4 + t2) - 2*s2*(-1 + t2) - 16*t2 + 
               7*Power(t2,2)) - 
            2*Power(s1,2)*(2 + (-6 - s2 + t1)*t2 + 8*Power(t2,2))) + 
         Power(s,4)*(Power(s1,4)*(-4 + (24 + t1)*t2 - 25*Power(t2,2)) + 
            Power(s1,3)*(-4 + Power(t1,2) + 4*(5 + 2*s2)*t2 + 
               t1*(s2 + 11*(-1 + t2))*t2 - (68 + 13*s2)*Power(t2,2) + 
               77*Power(t2,3)) + 
            Power(t2,2)*(-14 + 6*(-10 + 3*s2)*t2 + 
               (34 - 10*s2)*Power(t2,2) + (12 + s2)*Power(t2,3) - 
               Power(t1,2)*(2 + t2) - t1*(2 + t2)*(-6 + (-3 + s2)*t2)) - 
            Power(s1,2)*t2*(-12 + 20*t2 - 84*Power(t2,2) + 
               71*Power(t2,3) + Power(t1,2)*(3 + 2*t2) + 
               s2*(-8 + (36 + t1)*t2 - 27*Power(t2,2)) + 
               t1*(16 - 45*t2 + 17*Power(t2,2))) + 
            s1*t2*(8 + (28 - 26*s2)*t2 - 5*Power(t1,2)*t2 + 
               (-36 + 38*s2)*Power(t2,2) - (52 + 15*s2)*Power(t2,3) + 
               19*Power(t2,4) + 
               t1*(-4 + 2*(18 + s2)*t2 + (-29 + s2)*Power(t2,2) + 
                  5*Power(t2,3)))) + 
         Power(s,3)*(Power(s1,5)*
             (8 + t1 + (-27 + s2)*t2 - 3*t1*t2 + 20*Power(t2,2)) + 
            Power(s1,4)*(-6*Power(t1,2) + Power(s2,2)*t2 + 
               t2*(-14 + 93*t2 - 85*Power(t2,2)) + 
               t1*(-9 + 28*t2 - 4*Power(t2,2)) + 
               s2*(4 + t1*(3 - 4*t2) - 20*t2 + 11*Power(t2,2))) - 
            Power(s1,3)*(4*Power(s2,2)*Power(t2,2) + 
               Power(t1,2)*(1 - 18*t2 - 8*Power(t2,2)) + 
               t2*(-55 + 68*t2 + 131*Power(t2,2) - 123*Power(t2,3)) + 
               t1*(6 - 66*t2 + 110*Power(t2,2) - 22*Power(t2,3)) + 
               s2*(-4 + (45 + 8*t1)*t2 - 8*(12 + t1)*Power(t2,2) + 
                  43*Power(t2,3))) + 
            Power(t2,2)*(14 + (90 - 28*s2)*t2 + 
               (-187 + 29*s2 + 2*Power(s2,2))*Power(t2,2) - 
               (-46 + 2*s2 + Power(s2,2))*Power(t2,3) + 
               2*(6 + s2)*Power(t2,4) + 
               Power(t1,2)*(2 - 5*t2 - 2*Power(t2,2)) + 
               t1*(-14 + 4*(-5 + 2*s2)*t2 + (41 - 3*s2)*Power(t2,2) - 
                  2*(-4 + s2)*Power(t2,3))) + 
            s1*t2*(-20 + 2*(-71 + 28*s2)*t2 + 
               (391 - 99*s2 - 4*Power(s2,2))*Power(t2,2) + 
               6*(-29 + 10*s2)*Power(t2,3) - 
               2*(24 + 11*s2)*Power(t2,4) + 13*Power(t2,5) + 
               Power(t1,2)*(-4 + 13*t2 - 10*Power(t2,2)) + 
               t1*(20 - 4*(2 + 3*s2)*t2 + 8*(3 + s2)*Power(t2,2) + 
                  (-49 + 4*s2)*Power(t2,3) + 3*Power(t2,4))) + 
            Power(s1,2)*(4 + (46 - 32*s2)*t2 + 
               (-253 + 111*s2 + 2*Power(s2,2))*Power(t2,2) + 
               2*(102 - 67*s2 + 2*Power(s2,2))*Power(t2,3) + 
               (101 + 51*s2)*Power(t2,4) - 71*Power(t2,5) + 
               Power(t1,2)*t2*(-13 + 6*t2 - 6*Power(t2,2)) - 
               2*t1*(1 - (23 + 2*s2)*t2 + 67*Power(t2,2) + 
                  (-59 + 3*s2)*Power(t2,3) + 9*Power(t2,4)))) + 
         Power(s1 - t2,3)*(1 - 2*t1 + Power(t1,2) - 10*t2 + 2*s2*t2 + 
            13*t1*t2 - 2*s2*t1*t2 - 3*Power(t1,2)*t2 + 10*Power(t2,2) - 
            7*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) - 
            8*t1*Power(t2,2) + 4*Power(t1,2)*Power(t2,2) + 
            4*Power(t2,3) - 2*s2*Power(t2,3) + 
            3*Power(s2,2)*Power(t2,3) - 3*t1*Power(t2,3) - 
            4*s2*t1*Power(t2,3) + 2*Power(t1,2)*Power(t2,3) - 
            3*Power(t2,4) + 5*s2*Power(t2,4) - 2*s2*t1*Power(t2,4) - 
            2*Power(t2,5) + 2*s2*Power(t2,5) + 
            Power(s1,4)*(4*Power(s2,2) + 
               t1*(3 + 4*t1 - 2*t2 - Power(t2,2)) + 
               s2*(-3 - 8*t1 + 2*t2 + Power(t2,2))) + 
            Power(s1,3)*(Power(-1 + t2,2)*(3 + t2) + 
               Power(s2,2)*(-4 - 13*t2 + Power(t2,2)) - 
               Power(t1,2)*(6 + 9*t2 + Power(t2,2)) + 
               t1*(1 - 9*t2 + 7*Power(t2,2) + Power(t2,3)) - 
               2*s2*(2 - 7*t2 + 4*Power(t2,2) + Power(t2,3) - 
                  t1*(5 + 11*t2))) + 
            Power(s1,2)*(-(Power(-1 + t2,2)*t2*(7 + t2)) + 
               Power(t1,2)*(-1 + 17*t2 + 8*Power(t2,2)) + 
               t1*(7 - 3*t2 + Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,2)*(1 + 11*t2 + 14*Power(t2,2) - 
                  2*Power(t2,3)) + 
               s2*(-5 + 8*t2 - 16*Power(t2,2) + 12*Power(t2,3) + 
                  Power(t2,4) - 2*t1*(1 + 13*t2 + 10*Power(t2,2)))) + 
            s1*(Power(-1 + t2,2)*(9 + 6*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-2 - 10*t2 - 5*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(-2 + 5*t2 + 11*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-11 + 5*t2 + Power(t2,2) + 3*Power(t2,3) + 
                  2*Power(t2,4)) + 
               2*s2*(-1 + 6*t2 - Power(t2,2) - 4*Power(t2,4) + 
                  t1*(1 + t2 + 10*Power(t2,2) + 4*Power(t2,3))))) + 
         s*Power(s1 - t2,2)*(-2 + 4*t1 - 2*Power(t1,2) + 15*t2 - 
            4*s2*t2 - 18*t1*t2 + 4*s2*t1*t2 + 3*Power(t1,2)*t2 - 
            80*Power(t2,2) + 7*s2*Power(t2,2) + 
            2*Power(s2,2)*Power(t2,2) + 63*t1*Power(t2,2) - 
            13*s2*t1*Power(t2,2) + 57*Power(t2,3) + 13*s2*Power(t2,3) - 
            4*Power(s2,2)*Power(t2,3) - 49*t1*Power(t2,3) + 
            4*s2*t1*Power(t2,3) + 6*Power(t2,4) - 10*s2*Power(t2,4) + 
            4*Power(s2,2)*Power(t2,4) - 12*t1*Power(t2,4) + 
            3*s2*t1*Power(t2,4) + 2*Power(t2,5) - 2*s2*Power(t2,5) - 
            Power(s2,2)*Power(t2,5) + 8*t1*Power(t2,5) + 2*Power(t2,6) + 
            Power(s1,5)*(-1 + t2)*(-2 + s2 - t1 + 2*t2) + 
            Power(s1,4)*(-5 - 12*Power(t1,2) + 
               2*Power(s2,2)*(-3 + t2) - t2 + 13*Power(t2,2) - 
               7*Power(t2,3) + 
               s2*(9 - 2*t1*(-9 + t2) - 3*t2 - 2*Power(t2,2)) + 
               t1*(-17 + 7*t2 + 6*Power(t2,2))) + 
            Power(s1,3)*(-17 + 59*t2 + Power(s2,2)*(22 - 7*t2)*t2 - 
               31*Power(t2,2) - 19*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,2)*(7 + 26*t2 + 6*Power(t2,2)) - 
               2*t1*(-6 - 29*t2 + 18*Power(t2,2) + Power(t2,3)) - 
               s2*(-7 + 71*t2 - 33*Power(t2,2) + Power(t2,3) + 
                  t1*(11 + 48*t2 - 5*Power(t2,2)))) + 
            Power(s1,2)*(-29 + 64*t2 - 105*Power(t2,2) + 
               63*Power(t2,3) + 10*Power(t2,4) - 3*Power(t2,5) + 
               Power(t1,2)*(20 - 45*t2 - 16*Power(t2,2)) + 
               Power(s2,2)*(2 - 4*t2 - 22*Power(t2,2) + 
                  7*Power(t2,3)) - 
               t1*(8 + 15*t2 + 69*Power(t2,2) - 39*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s2*(-1 - 3*t2 + 113*Power(t2,2) - 57*Power(t2,3) + 
                  4*Power(t2,4) + 
                  t1*(-5 + 28*t2 + 37*Power(t2,2) - 2*Power(t2,3)))) - 
            s1*(13 - 125*t2 + 128*Power(t2,2) - 47*Power(t2,3) + 
               29*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,2)*(1 + 4*t2 - 14*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,2)*t2*(4 - 8*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               t1*(-14 + 87*t2 - 100*Power(t2,2) - 36*Power(t2,3) + 
                  31*Power(t2,4)) + 
               s2*(-4 + 6*t2 + 17*Power(t2,2) + 41*Power(t2,3) - 
                  30*Power(t2,4) + 2*Power(t2,5) + 
                  t1*(4 - 18*t2 + 21*Power(t2,2) + 10*Power(t2,3) + 
                     Power(t2,4))))) - 
         Power(s,2)*(s1 - t2)*
          (Power(s1,5)*(s2*(-1 + 2*t2) - (3 + t1 - 3*t2)*(-2 + 3*t2)) + 
            Power(s1,4)*(-11 - 13*Power(t1,2) - 2*t2 + 51*Power(t2,2) - 
               38*Power(t2,3) + Power(s2,2)*(-2 + 3*t2) + 
               s2*(9 + t1*(13 - 5*t2) - 12*t2 + Power(t2,2)) + 
               t1*(-23 + 22*t2 + 7*Power(t2,2))) + 
            s1*(6 + (82 - 28*s2)*t2 + 
               (-375 + 51*s2 + 8*Power(s2,2))*Power(t2,2) + 
               (353 - 56*s2 - 5*Power(s2,2))*Power(t2,3) - 
               (91 - 48*s2 + Power(s2,2))*Power(t2,4) - 
               2*(13 + 7*s2)*Power(t2,5) + 3*Power(t2,6) - 
               Power(t1,2)*(-2 + 8*t2 - 22*Power(t2,2) + 
                  5*Power(t2,3) + Power(t2,4)) + 
               t1*(-8 + 4*(-5 + 3*s2)*t2 + (102 - 28*s2)*Power(t2,2) + 
                  (8 + s2)*Power(t2,3) + (-53 + 3*s2)*Power(t2,4) + 
                  2*Power(t2,5))) + 
            Power(s1,3)*(-29 + 135*t2 + Power(s2,2)*(9 - 11*t2)*t2 - 
               87*Power(t2,2) - 71*Power(t2,3) + 52*Power(t2,4) + 
               Power(t1,2)*t2*(31 + 11*t2) + 
               t1*(-4 + 94*t2 - 87*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(11 - 76*t2 + 79*Power(t2,2) - 20*Power(t2,3) + 
                  t1*(-2 - 33*t2 + 11*Power(t2,2)))) + 
            t2*(-2 + (-55 + 18*s2)*t2 + 
               (202 - 22*s2 - 4*Power(s2,2))*Power(t2,2) + 
               (-122 - 7*s2 + 5*Power(s2,2))*Power(t2,3) + 
               (8 + 2*s2 - 2*Power(s2,2))*Power(t2,4) + 
               (9 + s2)*Power(t2,5) + 
               Power(t1,2)*(2 + 4*t2 - 5*Power(t2,2) - 2*Power(t2,3)) + 
               t1*t2*(24 - 88*t2 + 29*Power(t2,2) + 15*Power(t2,3) - 
                  s2*(10 - 15*t2 - 2*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,2)*(-21 + 176*t2 - 345*Power(t2,2) + 
               172*Power(t2,3) + 52*Power(t2,4) - 26*Power(t2,5) + 
               Power(s2,2)*t2*(-4 - 7*t2 + 11*Power(t2,2)) - 
               Power(t1,2)*(-10 + 43*t2 + Power(t2,2) + 4*Power(t2,3)) + 
               t1*(-16 + 42*t2 - 128*Power(t2,2) + 89*Power(t2,3) - 
                  9*Power(t2,4)) + 
               s2*(t1*(-2 + 15*t2 + 17*Power(t2,2) - 8*Power(t2,3)) + 
                  2*(5 - 20*t2 + 65*Power(t2,2) - 58*Power(t2,3) + 
                     15*Power(t2,4))))))*T3q(t2,s1))/
     (s*(-1 + s1)*(s - s2 + t1)*Power(s1 - t2,2)*(-s + s1 - t2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) - 
    (8*(4*Power(s,5)*Power(t2,2)*(1 + t2) + 
         Power(s,4)*(Power(t1,2)*Power(-1 + t2,2) + 
            t1*t2*(-3 + s2*Power(-1 + t2,2) + 12*t2 - Power(t2,2) + 
               s1*(1 + 6*t2 + Power(t2,2))) - 
            t2*(s1*(-8 + (-3 + 8*s2)*t2 + 18*Power(t2,2) + 
                  Power(t2,3)) + 
               t2*(-28 + 22*t2 - 18*Power(t2,2) + 
                  s2*(9 - 2*t2 + Power(t2,2))))) + 
         Power(s,2)*(1 - 2*Power(t1,2) + 4*t2 - 4*s2*t2 - 6*t1*t2 + 
            5*s2*t1*t2 + Power(t1,2)*t2 + 25*Power(t2,2) + 
            79*s2*Power(t2,2) - 11*Power(s2,2)*Power(t2,2) - 
            35*t1*Power(t2,2) + 2*s2*t1*Power(t2,2) + 
            4*Power(t1,2)*Power(t2,2) - 54*Power(t2,3) - 
            110*s2*Power(t2,3) + 14*Power(s2,2)*Power(t2,3) + 
            27*t1*Power(t2,3) + 2*s2*t1*Power(t2,3) - 
            13*Power(t1,2)*Power(t2,3) + 21*Power(t2,4) + 
            54*s2*Power(t2,4) - 13*Power(s2,2)*Power(t2,4) + 
            19*t1*Power(t2,4) + 6*s2*t1*Power(t2,4) + 
            2*Power(t1,2)*Power(t2,4) - 6*Power(t2,5) - 
            18*s2*Power(t2,5) + 2*Power(s2,2)*Power(t2,5) - 
            5*t1*Power(t2,5) + s2*t1*Power(t2,5) + 9*Power(t2,6) - 
            s2*Power(t2,6) - Power(s1,3)*(-1 + t2)*
             (2 - 23*t2 + 16*Power(t2,2) + 5*Power(t2,3) + 
               t1*(6 - 19*t2 - 3*Power(t2,2)) + 
               s2*(-7 + 21*t2 + 2*Power(t2,2))) - 
            s1*(Power(s2,2)*t2*
                (-7 + 21*t2 - 31*Power(t2,2) + Power(t2,3)) + 
               t1*(4 - 22*t2 + 13*Power(t2,2) + 8*Power(t2,3) - 
                  3*Power(t2,4)) - 
               Power(t1,2)*(2 - 17*t2 + 23*Power(t2,2) + 
                  7*Power(t2,3) + Power(t2,4)) + 
               Power(-1 + t2,2)*
                (3 + 93*t2 - 17*Power(t2,2) + 32*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(-1 + (44 - 3*t1)*t2 + (-89 + t1)*Power(t2,2) + 
                  (58 + 27*t1)*Power(t2,3) + (-4 + 7*t1)*Power(t2,4) - 
                  8*Power(t2,5))) - 
            Power(s1,2)*(Power(t1,2)*(-13 + 10*t2 + 11*Power(t2,2)) + 
               Power(s2,2)*(-2 - 9*t2 + 16*Power(t2,2) + 
                  3*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (39 - 34*t2 + 45*Power(t2,2) + 6*Power(t2,3)) + 
               t1*(-23 + 46*t2 - 42*Power(t2,2) + 14*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s2*(13 - 24*t2 + 40*Power(t2,2) - 36*Power(t2,3) + 
                  7*Power(t2,4) + 
                  t1*(13 + t2 - 25*Power(t2,2) - 5*Power(t2,3))))) + 
         Power(s,3)*(Power(t1,2)*
             (-1 + 6*t2 - 7*Power(t2,2) + 2*Power(t2,3)) + 
            t1*(2 + (5 - 3*s2)*t2 + 2*(-4 + s2)*Power(t2,2) - 
               (-17 + s2)*Power(t2,3) + 2*s2*Power(t2,4)) + 
            Power(s1,2)*(4 + t2 - 15*s2*t2 + 
               (-34 + 22*s2)*Power(t2,2) + (25 + s2)*Power(t2,3) + 
               4*Power(t2,4) + 
               t1*(1 + 11*t2 - 17*Power(t2,2) - 3*Power(t2,3))) - 
            t2*(1 + 74*t2 - 99*Power(t2,2) + 44*Power(t2,3) - 
               20*Power(t2,4) - 
               Power(s2,2)*t2*(5 - 6*t2 + Power(t2,2)) + 
               s2*(1 + 18*t2 - 13*Power(t2,2) + 8*Power(t2,3) + 
                  2*Power(t2,4))) + 
            s1*(2*Power(t1,2)*(-3 + 6*t2 + Power(t2,2)) + 
               t2*(66 - 120*t2 + 103*Power(t2,2) - 46*Power(t2,3) - 
                  3*Power(t2,4) + 
                  Power(s2,2)*(1 + 6*t2 + Power(t2,2)) + 
                  s2*(-20 + 30*t2 - 22*Power(t2,2) + 4*Power(t2,3))) + 
               t1*(-5 + 33*t2 - 34*Power(t2,2) + 11*Power(t2,3) + 
                  3*Power(t2,4) - 
                  s2*(-3 + 10*t2 + 5*Power(t2,2) + 4*Power(t2,3))))) - 
         Power(-1 + t2,2)*(1 - 2*t1 + Power(t1,2) - 10*t2 + 2*s2*t2 + 
            13*t1*t2 - 2*s2*t1*t2 - 3*Power(t1,2)*t2 + 10*Power(t2,2) - 
            7*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) - 
            8*t1*Power(t2,2) + 4*Power(t1,2)*Power(t2,2) + 
            4*Power(t2,3) - 2*s2*Power(t2,3) + 
            3*Power(s2,2)*Power(t2,3) - 3*t1*Power(t2,3) - 
            4*s2*t1*Power(t2,3) + 2*Power(t1,2)*Power(t2,3) - 
            3*Power(t2,4) + 5*s2*Power(t2,4) - 2*s2*t1*Power(t2,4) - 
            2*Power(t2,5) + 2*s2*Power(t2,5) + 
            Power(s1,4)*(4*Power(s2,2) + 
               t1*(3 + 4*t1 - 2*t2 - Power(t2,2)) + 
               s2*(-3 - 8*t1 + 2*t2 + Power(t2,2))) + 
            Power(s1,3)*(Power(-1 + t2,2)*(3 + t2) + 
               Power(s2,2)*(-4 - 13*t2 + Power(t2,2)) - 
               Power(t1,2)*(6 + 9*t2 + Power(t2,2)) + 
               t1*(1 - 9*t2 + 7*Power(t2,2) + Power(t2,3)) - 
               2*s2*(2 - 7*t2 + 4*Power(t2,2) + Power(t2,3) - 
                  t1*(5 + 11*t2))) + 
            Power(s1,2)*(-(Power(-1 + t2,2)*t2*(7 + t2)) + 
               Power(t1,2)*(-1 + 17*t2 + 8*Power(t2,2)) + 
               t1*(7 - 3*t2 + Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,2)*(1 + 11*t2 + 14*Power(t2,2) - 
                  2*Power(t2,3)) + 
               s2*(-5 + 8*t2 - 16*Power(t2,2) + 12*Power(t2,3) + 
                  Power(t2,4) - 2*t1*(1 + 13*t2 + 10*Power(t2,2)))) + 
            s1*(Power(-1 + t2,2)*(9 + 6*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-2 - 10*t2 - 5*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(-2 + 5*t2 + 11*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-11 + 5*t2 + Power(t2,2) + 3*Power(t2,3) + 
                  2*Power(t2,4)) + 
               2*s2*(-1 + 6*t2 - Power(t2,2) - 4*Power(t2,4) + 
                  t1*(1 + t2 + 10*Power(t2,2) + 4*Power(t2,3))))) + 
         s*(-1 + t2)*(-1 + 4*t1 - 3*Power(t1,2) + 11*t2 - 5*s2*t2 - 
            21*t1*t2 + 5*s2*t1*t2 + 7*Power(t1,2)*t2 - 25*Power(t2,2) + 
            64*s2*Power(t2,2) - 8*Power(s2,2)*Power(t2,2) - 
            26*t1*Power(t2,2) + 5*s2*t1*Power(t2,2) - 
            8*Power(t1,2)*Power(t2,2) + 33*Power(t2,3) - 
            57*s2*Power(t2,3) - 2*Power(s2,2)*Power(t2,3) + 
            27*t1*Power(t2,3) + 13*s2*t1*Power(t2,3) - 
            12*Power(t1,2)*Power(t2,3) - 28*Power(t2,4) + 
            10*s2*Power(t2,4) - 7*Power(s2,2)*Power(t2,4) + 
            20*t1*Power(t2,4) + 9*s2*t1*Power(t2,4) + 8*Power(t2,5) - 
            12*s2*Power(t2,5) + Power(s2,2)*Power(t2,5) - 
            4*t1*Power(t2,5) + 2*Power(t2,6) + 
            Power(s1,4)*(-1 + t2)*
             (s2*(7 + t2) - t1*(7 + t2) + 2*(-3 + 2*t2 + Power(t2,2))) + 
            Power(s1,3)*(4*Power(t1,2)*(1 + 3*t2) + 
               2*Power(s2,2)*(-1 + 8*t2 + Power(t2,2)) - 
               Power(-1 + t2,2)*(-5 + 14*t2 + 3*Power(t2,2)) + 
               t1*(3 - 9*t2 + 5*Power(t2,2) + Power(t2,3)) + 
               s2*(3 + 3*t2 - 11*Power(t2,2) + 5*Power(t2,3) - 
                  2*t1*(1 + 14*t2 + Power(t2,2)))) + 
            Power(s1,2)*(Power(s2,2)*
                (-6 - 2*t2 - 41*Power(t2,2) + Power(t2,3)) + 
               Power(-1 + t2,2)*
                (5 + 5*t2 + 13*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(5 + 23*t2 + 18*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-40 + 49*t2 - 13*Power(t2,2) + 3*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(37 - 65*t2 + 47*Power(t2,2) - 11*Power(t2,3) - 
                  8*Power(t2,4) + 
                  t1*(15 + 21*t2 + 55*Power(t2,2) + 5*Power(t2,3)))) + 
            s1*(2*Power(s2,2)*t2*
                (7 + 3*t2 + 16*Power(t2,2) - 2*Power(t2,3)) + 
               2*Power(t1,2)*
                (-2 + 6*t2 + 17*Power(t2,2) + 3*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-1 - 10*t2 + 23*Power(t2,2) + 8*Power(t2,3)) + 
               t1*(6 + 79*t2 - 73*Power(t2,2) - 15*Power(t2,3) + 
                  3*Power(t2,4)) - 
               s2*(-1 + 99*t2 - 125*Power(t2,2) + 55*Power(t2,3) - 
                  26*Power(t2,4) - 2*Power(t2,5) + 
                  t1*(1 + 22*t2 + 38*Power(t2,2) + 34*Power(t2,3) + 
                     Power(t2,4))))))*T4q(-1 + t2))/
     (s*(-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*(-1 + t2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) - 
    (8*(Power(s,5)*(Power(t1,2) + (-3 + s1 + s2)*t1*t2 - 
            (s1 + s2)*Power(t2,2)) - 
         2*Power(s1 - t2,2)*Power(-1 + t2,2)*
          (-5 + 2*Power(s1,2)*(s2 - t1) + 2*t1 + 3*Power(t1,2) + 4*t2 - 
            Power(s2,2)*t2 - 12*t1*t2 + Power(t2,2) + 
            s2*(3 + t1*(-3 + t2) + 8*t2 - Power(t2,2)) + 
            s1*(2 + Power(s2,2) - 3*Power(t1,2) + s2*(-11 + 2*t1 - t2) - 
               2*t2 + 3*t1*(3 + t2))) + 
         Power(s,4)*(Power(t1,2)*(-1 - 6*s1 + 4*t2) + 
            t2*(-1 + 18*t2 + Power(s2,2)*t2 - 8*Power(t2,2) + 
               Power(s1,2)*(-3 + s2 + 4*t2) + 
               s1*(2 + Power(s2,2) + 4*s2*(-1 + t2) + 4*t2 - 
                  3*Power(t2,2)) - s2*(1 + 4*t2 + 2*Power(t2,2))) + 
            t1*(2 + Power(s1,2)*(1 - 3*t2) - 3*(-3 + s2)*t2 + 
               2*(-7 + s2)*Power(t2,2) + 
               s1*(-5 + s2*(3 - 4*t2) + 11*t2 + 3*Power(t2,2)))) + 
         Power(s,3)*(1 + 6*t2 - 4*s2*t2 - 48*Power(t2,2) - 
            15*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) + 
            56*Power(t2,3) - 2*s2*Power(t2,3) + 
            2*Power(s2,2)*Power(t2,3) - 11*Power(t2,4) - s2*Power(t2,4) + 
            Power(t1,2)*(-2 - 3*t2 + 4*Power(t2,2)) + 
            Power(s1,3)*(-2 + s2 + 7*t2 - 2*s2*t2 - 5*Power(t2,2) + 
               t1*(-2 + 3*t2)) + 
            t1*t2*(-6 + 47*t2 - 27*Power(t2,2) + 
               s2*(5 - 4*t2 + Power(t2,2))) + 
            s1*(-3 + 31*t2 - 55*Power(t2,2) + 16*Power(t2,3) - 
               Power(t2,4) - Power(s2,2)*t2*(1 + t2) + 
               Power(t1,2)*(2 - 17*t2 + Power(t2,2)) + 
               t1*(-4 - 42*t2 + 35*Power(t2,2)) + 
               s2*(1 + 3*(2 + 5*t1)*t2 - 7*t1*Power(t2,2) + 
                  8*Power(t2,3))) + 
            Power(s1,2)*(3 + 13*Power(t1,2) + Power(s2,2)*(2 - 3*t2) + 
               4*t2 - 13*Power(t2,2) + 6*Power(t2,3) + 
               t1*(17 - 14*t2 - 5*Power(t2,2)) + 
               s2*(-5 + 10*t2 - 7*Power(t2,2) + t1*(-13 + 5*t2)))) + 
         s*(-1 + 2*t1 - Power(t1,2) + 10*t2 - 2*s2*t2 - 13*t1*t2 + 
            2*s2*t1*t2 + 3*Power(t1,2)*t2 - 26*Power(t2,2) - 
            11*s2*Power(t2,2) + 3*Power(s2,2)*Power(t2,2) + 
            48*t1*Power(t2,2) - 16*s2*t1*Power(t2,2) + 
            8*Power(t1,2)*Power(t2,2) + 34*Power(t2,3) + 
            60*s2*Power(t2,3) - 5*Power(s2,2)*Power(t2,3) - 
            93*t1*Power(t2,3) + 8*s2*t1*Power(t2,3) - 
            4*Power(t1,2)*Power(t2,3) - 27*Power(t2,4) - 
            51*s2*Power(t2,4) + 6*Power(s2,2)*Power(t2,4) + 
            56*t1*Power(t2,4) - 2*s2*t1*Power(t2,4) - 
            2*Power(t1,2)*Power(t2,4) + 12*Power(t2,5) + 
            4*s2*Power(t2,5) - 2*Power(t2,6) + 
            Power(s1,4)*(4*Power(s2,2) + 
               t1*(5 + 4*t1 - 6*t2 + Power(t2,2)) - 
               s2*(5 + 8*t1 - 6*t2 + Power(t2,2))) - 
            Power(s1,3)*(t1*(-3 + t2)*Power(-1 + t2,2) + 
               Power(-1 + t2,2)*(7 + t2) + 
               Power(t1,2)*(4 + 13*t2 - Power(t2,2)) + 
               Power(s2,2)*(6 + 9*t2 + Power(t2,2)) - 
               2*s2*(2 - 3*t2 + Power(t2,3) + t1*(5 + 11*t2))) + 
            Power(s1,2)*(Power(t1,2)*(1 + 25*t2 - 2*Power(t2,2)) - 
               Power(-1 + t2,2)*(-4 - 17*t2 + Power(t2,2)) + 
               Power(s2,2)*(3 + 7*t2 + 12*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(25 - 81*t2 + 47*Power(t2,2) + 9*Power(t2,3)) - 
               s2*(25 - 74*t2 + 34*Power(t2,2) + 14*Power(t2,3) + 
                  Power(t2,4) + 2*t1*(1 + 17*t2 + 6*Power(t2,2)))) - 
            s1*(Power(t1,2)*(2 + 7*t2 + 19*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(s2,2)*t2*(6 - 4*t2 + 13*Power(t2,2) + Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-9 + 6*t2 - 17*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(-11 + 77*t2 - 175*Power(t2,2) + 99*Power(t2,3) + 
                  10*Power(t2,4)) - 
               2*s2*(1 + 18*t2 - 69*Power(t2,2) + 48*Power(t2,3) + 
                  2*Power(t2,4) + t1*(-1 + 9*t2 + 8*Power(t2,2))))) + 
         Power(s,2)*(1 - 4*t1 + 3*Power(t1,2) - 10*t2 + 5*s2*t2 + 
            17*t1*t2 - 5*s2*t1*t2 - 4*Power(t1,2)*t2 + 43*Power(t2,2) + 
            27*s2*Power(t2,2) - 4*Power(s2,2)*Power(t2,2) - 
            59*t1*Power(t2,2) + 10*s2*t1*Power(t2,2) - 
            4*Power(t1,2)*Power(t2,2) - 70*Power(t2,3) - 
            58*s2*Power(t2,3) + 4*Power(s2,2)*Power(t2,3) + 
            84*t1*Power(t2,3) + s2*t1*Power(t2,3) - 
            2*Power(t1,2)*Power(t2,3) + 42*Power(t2,4) + 
            2*s2*Power(t2,4) + Power(s2,2)*Power(t2,4) - 
            14*t1*Power(t2,4) - 6*Power(t2,5) + 
            Power(s1,4)*(-1 + t2)*(-2 + s2 - t1 + 2*t2) + 
            Power(s1,3)*(-1 - 12*Power(t1,2) + 2*Power(s2,2)*(-3 + t2) - 
               5*t2 + 9*Power(t2,2) - 3*Power(t2,3) + 
               t1*(-17 + 12*t2 + Power(t2,2)) + 
               s2*(11 - 2*t1*(-9 + t2) - 12*t2 + 5*Power(t2,2))) + 
            Power(s1,2)*(Power(t1,2)*(3 + 26*t2 - 2*Power(t2,2)) + 
               Power(-1 + t2,2)*(21 - 6*t2 + Power(t2,2)) + 
               Power(s2,2)*(2 + 8*t2 + Power(t2,2)) + 
               t1*(6 + 55*t2 - 30*Power(t2,2) + Power(t2,3)) - 
               s2*(5 + 24*t2 - 5*Power(t2,2) + 8*Power(t2,3) + 
                  t1*(9 + 34*t2 - 5*Power(t2,2)))) - 
            s1*(4*Power(t1,2)*(-1 + t2 + 2*Power(t2,2)) + 
               2*Power(s2,2)*t2*(-1 + 3*t2 + 2*Power(t2,2)) - 
               Power(-1 + t2,2)*(-1 - 55*t2 + 8*Power(t2,2)) + 
               t1*(6 - 47*t2 + 124*Power(t2,2) - 31*Power(t2,3)) + 
               s2*(1 + 26*t2 - 73*Power(t2,2) - 4*Power(t2,3) - 
                  2*Power(t2,4) + 
                  t1*(-1 - 3*t2 - 13*Power(t2,2) + Power(t2,3))))))*T5q(s))/
     (s*(-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)));
   return a;
};
