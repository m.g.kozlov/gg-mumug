#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m213_6_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(Power(s1,2)*(1 + (s2 - 5*(4 + t1))*t2 + 
            (9 - s2 + t1)*Power(t2,2) - 2*Power(t2,3)) + 
         s1*(-1 + (7 - 6*s - 8*s2)*t2 + (-5 + 5*s + 8*s2)*Power(t2,2) + 
            (-11 + s)*Power(t2,3) + 2*Power(t2,4) + 
            2*Power(t1,2)*t2*(-11 + 3*t2) + 
            t1*(1 + (29 - s + 6*s2)*t2 + (31 + s - 6*s2)*Power(t2,2) - 
               5*Power(t2,3))) + 
         t2*(7 - 16*t2 + 8*s2*t2 + 27*Power(t2,2) - 9*s2*Power(t2,2) + 
            2*Power(t2,3) + s2*Power(t2,3) + 
            Power(t1,2)*(6 + 14*t2 - 4*Power(t2,2)) + 
            t1*(-13 - (13 + 6*s2)*t2 + 6*(-5 + s2)*Power(t2,2) + 
               4*Power(t2,3)) + 
            s*(-1 + t2)*(-4 + 2*Power(t1,2) - 8*t2 - Power(t2,2) + 
               t1*(2 + t2)))))/
     ((-1 + s1)*(s1 - t2)*(-1 + s1 + t1 - t2)*Power(-1 + t2,2)*t2) + 
    (8*(Power(t1,2)*(3 + 3*s*(-1 + t2) - 9*t2 + 2*Power(t2,2)) + 
         (-1 + t2)*(-5 + (29 + s - 15*s2)*t2 - 
            2*(6 + s - 3*s2)*Power(t2,2) + 4*Power(t2,3)) + 
         Power(s1,3)*(-(s2*(-1 + t2)*(1 + 2*t1 + t2)) + 
            t1*(5 + 2*t1*(-3 + t2) + 8*t2 - Power(t2,2))) - 
         t1*(8 + (19 + s2)*t2 - (13 + s2)*Power(t2,2) + 6*Power(t2,3) + 
            s*(-5 + 4*t2 + Power(t2,2))) + 
         s1*(4 - 2*Power(t1,3)*(-3 + t2) - 25*t2 - 8*s*t2 + 
            2*Power(t2,2) + 9*s*Power(t2,2) - Power(t2,3) - 
            s*Power(t2,3) + s2*(-1 + t2)*
             (5 + 2*Power(t1,2) - t1*(-7 + t2) - 10*t2 + Power(t2,2)) + 
            Power(t1,2)*(30 + s*(-1 + t2) - 10*t2 + 4*Power(t2,2)) - 
            2*t1*(-11 + 21*t2 - 3*Power(t2,2) + Power(t2,3))) + 
         Power(s1,2)*(-1 + 2*Power(t1,3)*(-3 + t2) - 5*t2 + s*t2 - 
            7*Power(t2,2) + Power(t2,3) - s*Power(t2,3) + 
            Power(t1,2)*(9 + 9*t2 - 2*Power(t2,2)) + 
            s2*(-1 + t2)*(4 + 3*t1 - 2*Power(t1,2) + 2*t2 + 
               Power(t2,2)) + 
            t1*(21 + 5*t2 - 2*Power(t2,2) + s*(3 - 4*t2 + Power(t2,2)))))*
       B1(1 - s1 - t1 + t2,s1,t2))/((-1 + s1)*(s1*t1 - t2)*Power(-1 + t2,2)) \
+ (32*(-2*Power(s1,4) + Power(s1,3)*
          (1 + s2 - 5*t1 + s*(-1 + t2) + 3*t2 - s2*t2 + t1*t2) - 
         t2*(1 + t2)*(s*(-1 + t2) + (-4 + t2)*t2) + 
         Power(s1,2)*(-3 + (11 + s - 2*s2)*t2 - 
            (4 + s - 2*s2)*Power(t2,2) + 
            t1*(5 + s*(-1 + t2) + 4*t2 - Power(t2,2))) + 
         s1*(3 - 8*t2 - 10*Power(t2,2) + s2*Power(t2,2) + 3*Power(t2,3) - 
            s2*Power(t2,3) + t1*
             (-3 + 2*t2 - 4*Power(t2,2) + Power(t2,3)) + 
            s*(1 + t1 - 3*t2 - t1*t2 + Power(t2,2) + Power(t2,3))))*R1q(s1))/
     (Power(-1 + s1,2)*Power(s1 - t2,2)*Power(-1 + t2,2)) + 
    (16*(Power(s1,4)*(1 - t2 + 7*Power(t2,2) + Power(t2,3)) + 
         Power(s1,2)*(Power(t1,3)*(-1 + t2)*t2 + 
            Power(t1,2)*(1 + (-8 + s + s2)*t2 - 
               (-34 + s + s2)*Power(t2,2) - 7*Power(t2,3)) + 
            t1*t2*(-1 + (-23 + 8*s - 5*s2)*t2 + 
               (-35 - 8*s + 5*s2)*Power(t2,2) + 3*Power(t2,3)) + 
            t2*(-4 + (22 + 4*s2)*t2 + (-69 - 5*s + 2*s2)*Power(t2,2) + 
               (14 + 5*s - 6*s2)*Power(t2,3) + Power(t2,4))) + 
         Power(t2,2)*(-(Power(t1,3)*(3 + s*(-1 + t2) + t2)) + 
            Power(t1,2)*(4 - 4*s*(-1 + t2) + (2 + s2)*t2 - 
               (-13 + s2)*Power(t2,2) - 3*Power(t2,3)) + 
            t1*t2*(9 - (2 + 3*s2)*t2 + (5 + 3*s2)*Power(t2,2) + 
               s*(-4 + 7*t2 - 3*Power(t2,2))) + 
            4*t2*(-4 + (-1 + s2)*t2 - (10 + s2)*Power(t2,2) + 
               Power(t2,3) + 2*s*(-2 + t2 + Power(t2,2)))) + 
         s1*t2*(Power(t1,3)*(-3 + s + 10*t2 - s*t2 - 3*Power(t2,2)) + 
            Power(t1,2)*(1 + (5*s - 2*(7 + s2))*t2 + 
               (-28 - 5*s + 2*s2)*Power(t2,2) + 5*Power(t2,3)) + 
            t2*(-4 + (21 + 4*s - 8*s2)*t2 + 
               (94 - s + 5*s2)*Power(t2,2) + 
               (-11 - 3*s + 3*s2)*Power(t2,3)) + 
            t1*t2*(26 + (-23 + 7*s2)*t2 + (19 - 7*s2)*Power(t2,2) - 
               2*Power(t2,3) + s*(4 - 9*t2 + 5*Power(t2,2)))) + 
         Power(s1,3)*(Power(t1,2)*(-1 + t2)*t2 + 
            t1*(2 + (-6 + s2)*t2 - (-31 + s2)*Power(t2,2) - 
               3*Power(t2,3)) - 
            t2*(4*s*(-1 + t2)*t2 - 3*s2*(-1 + t2)*t2 + 
               2*(1 + 6*Power(t2,2) + Power(t2,3)))))*R1q(t2))/
     ((-1 + s1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       Power(s1 - t2,2)*Power(-1 + t2,2)*t2) + 
    (16*(4*Power(s1,4) - 3*Power(t1,3)*
          (4 + s*(-1 + t2) + t2 - Power(t2,2)) - 
         Power(s1,3)*(20 + s2 - 17*t1 + 2*s*(-1 + t2) + 6*t2 - s2*t2 + 
            t1*t2 + 2*Power(t2,2)) + 
         2*(3 + (-29 + s + 9*s2)*t2 - (13 + 4*s + 8*s2)*Power(t2,2) + 
            (-19 + 3*s - s2)*Power(t2,3) + 2*Power(t2,4)) + 
         Power(s1,2)*(20 - 2*Power(t1,2)*(-11 + t2) - 
            s*(-5 + 7*t1 - 4*t2)*(-1 + t2) - 39*t2 + 5*s2*t2 + 
            14*Power(t2,2) - 5*s2*Power(t2,2) + Power(t2,3) + 
            t1*(-49 + 2*s2*(-1 + t2) - 17*t2 - 2*Power(t2,2))) + 
         Power(t1,2)*(25 + 23*t2 - Power(t2,2) - 3*Power(t2,3) + 
            s2*(3 + 3*t2 - 6*Power(t2,2)) + s*(-8 + 3*t2 + 5*Power(t2,2))\
) + t1*(-11 + 15*t2 + 7*Power(t2,2) + 5*Power(t2,3) + 
            s*(4 + 5*t2 - 6*Power(t2,2) - 3*Power(t2,3)) + 
            s2*(-4 - 17*t2 + 18*Power(t2,2) + 3*Power(t2,3))) + 
         s1*(-13 + 43*t1 - 41*Power(t1,2) + 9*Power(t1,3) + 107*t2 - 
            40*t1*t2 - 14*Power(t1,2)*t2 - Power(t1,3)*t2 + 
            57*Power(t2,2) + 23*t1*Power(t2,2) + 
            3*Power(t1,2)*Power(t2,2) - 7*Power(t2,3) - 
            2*t1*Power(t2,3) + 
            s2*(-1 + t2)*(2 - 3*t1 + Power(t1,2) + 11*t2 - 11*t1*t2 + 
               3*Power(t2,2)) - 
            s*(-1 + t2)*(2 + 8*Power(t1,2) + 7*t2 + 3*Power(t2,2) - 
               t1*(13 + 9*t2))))*R2q(1 - s1 - t1 + t2))/
     ((-1 + s1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       (-1 + s1 + t1 - t2)*Power(-1 + t2,2)) + 
    (8*(Power(t1,5)*(3 + 3*s*(-1 + t2) + 3*t2 - 2*Power(t2,2)) + 
         4*(-1 + t2)*t2*(-3 + (17 - 9*s - 11*s2)*t2 + 
            (5 + 2*s - 2*s2)*Power(t2,2) + (5 + s - s2)*Power(t2,3)) + 
         Power(t1,3)*(5 + (-36 + 13*s + 11*s2)*t2 + 
            (27 - 19*s - 5*s2)*Power(t2,2) + 
            2*(8 + 3*s - 3*s2)*Power(t2,3) - 4*Power(t2,4)) + 
         Power(t1,4)*(-8 + t2 - 5*s2*t2 + (-19 + 5*s2)*Power(t2,2) + 
            6*Power(t2,3) - 5*s*(-1 + Power(t2,2))) + 
         Power(s1,5)*(s2*(-1 + Power(t2,2)) + 
            t1*(7 - 4*t1 + 4*t2 + Power(t2,2))) + 
         2*Power(t1,2)*t2*(31 + (-8 + 5*s2)*t2 + 
            (25 - 7*s2)*Power(t2,2) + 2*(-4 + s2)*Power(t2,3) + 
            s*(-20 + 15*t2 + 7*Power(t2,2) - 2*Power(t2,3))) + 
         4*t1*t2*(-9 + 18*t2 - 25*Power(t2,2) - 10*Power(t2,3) + 
            2*Power(t2,4) - s*(-2 + Power(t2,2) + Power(t2,3)) + 
            s2*(-2 - 12*t2 + 13*Power(t2,2) + Power(t2,3))) - 
         Power(s1,4)*(-1 - 2*Power(t1,3)*(-9 + t2) + 7*t2 + s*t2 + 
            5*Power(t2,2) + Power(t2,3) - s*Power(t2,3) + 
            Power(t1,2)*(-26 - 2*s*(-1 + t2) - 31*t2 + Power(t2,2)) + 
            s2*(-1 + t2)*(5 + 2*Power(t1,2) + t2 + Power(t2,2) - 
               t1*(4 + 7*t2)) + 
            t1*(3*s*Power(-1 + t2,2) + 5*(2 - t2 + 3*Power(t2,2)))) + 
         Power(s1,3)*(-5 + 6*Power(t1,4)*(-5 + t2) + 
            (14 + 9*s - s2)*t2 + (-1 - 11*s + 11*s2)*Power(t2,2) + 
            2*(8 + s - 5*s2)*Power(t2,3) + 
            Power(t1,3)*(38 + 6*s*(-1 + t2) - 6*s2*(-1 + t2) + 69*t2 - 
               11*Power(t2,2)) - 
            Power(t1,2)*(7 - 8*t2 + 45*Power(t2,2) - 4*Power(t2,3) + 
               s2*(8 + 9*t2 - 17*Power(t2,2)) + 
               s*(10 - 21*t2 + 11*Power(t2,2))) + 
            t1*(-9 - 18*t2 - 90*Power(t2,2) + 13*Power(t2,3) + 
               s*(11 - 19*t2 + 3*Power(t2,2) + 5*Power(t2,3)) - 
               s2*(-9 + t2 + 3*Power(t2,2) + 5*Power(t2,3)))) + 
         Power(s1,2)*(Power(t1,5)*(-22 + 6*t2) + 
            Power(t1,4)*(26 + 6*s*(-1 + t2) - 6*s2*(-1 + t2) + 61*t2 - 
               15*Power(t2,2)) + 
            Power(t1,3)*(23 + 4*t2 - 51*Power(t2,2) + 8*Power(t2,3) + 
               s*(-14 + 27*t2 - 13*Power(t2,2)) + 
               s2*(-8 - 9*t2 + 17*Power(t2,2))) - 
            2*t2*(-7 + 8*t2 - 31*Power(t2,2) + 6*Power(t2,3) + 
               s*(8 - 13*t2 + Power(t2,2) + 4*Power(t2,3)) - 
               s2*(-12 + 7*t2 + Power(t2,2) + 4*Power(t2,3))) + 
            t1*(15 - 28*t2 + 61*Power(t2,2) + 116*Power(t2,3) - 
               12*Power(t2,4) + 
               s2*(4 + 17*t2 + Power(t2,2) - 22*Power(t2,3)) + 
               s*(-4 + 23*t2 - 25*Power(t2,2) + 6*Power(t2,3))) - 
            Power(t1,2)*(37 + 42*t2 + 180*Power(t2,2) - 35*Power(t2,3) + 
               s*(-31 + 35*t2 + 3*Power(t2,2) - 7*Power(t2,3)) + 
               s2*(1 - 5*t2 - 3*Power(t2,2) + 7*Power(t2,3)))) + 
         s1*(2*Power(t1,6)*(-3 + t2) + 
            Power(t1,5)*(7 + 2*s*(-1 + t2) - 2*s2*(-1 + t2) + 19*t2 - 
               6*Power(t2,2)) - 
            4*t1*(3 + (-24 + 13*s + 7*s2)*t2 - 
               (-5 + 19*s + s2)*Power(t2,2) + 
               (-27 + 4*s - 4*s2)*Power(t2,3) + 
               (11 + 2*s - 2*s2)*Power(t2,4)) + 
            Power(t1,4)*(23 + 4*t2 - 23*Power(t2,2) + 4*Power(t2,3) - 
               5*s*(2 - 3*t2 + Power(t2,2)) + 
               s2*(-3 - 3*t2 + 6*Power(t2,2))) + 
            4*t2*(t2*(5 - 12*t2 - 19*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-1 + 3*t2 - 4*Power(t2,2) + 2*Power(t2,3)) + 
               s*(1 - 7*t2 + 4*Power(t2,2) + 2*Power(t2,3))) - 
            Power(t1,3)*(39 + 34*t2 + 110*Power(t2,2) - 31*Power(t2,3) + 
               s*(-25 + 17*t2 + 11*Power(t2,2) - 3*Power(t2,3)) + 
               s2*(5 + 3*t2 - 11*Power(t2,2) + 3*Power(t2,3))) + 
            Power(t1,2)*(21 - 74*t2 + 117*Power(t2,2) + 88*Power(t2,3) - 
               16*Power(t2,4) + 
               s2*(8 + 33*t2 - 19*Power(t2,2) - 22*Power(t2,3)) + 
               s*(-8 + 23*t2 - 29*Power(t2,2) + 14*Power(t2,3)))))*
       T2q(t2,1 - s1 - t1 + t2))/
     ((-1 + s1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)) + 
    (8*(Power(s1,4)*(-(s2*(-1 + t2)*(1 + 2*t1 + t2)) + 
            t1*(-11 + 2*t1*(-3 + t2) + 8*t2 - Power(t2,2))) + 
         Power(s1,3)*(-1 + 2*Power(t1,3)*(-3 + t2) + 11*t2 + s*t2 - 
            7*Power(t2,2) + Power(t2,3) - s*Power(t2,3) + 
            Power(t1,2)*(-17 + 19*t2 - 6*Power(t2,2)) + 
            s2*(-1 + t2)*(5 - 2*Power(t1,2) + 5*t2 + 3*Power(t2,2) + 
               t1*(-3 + 4*t2)) + 
            t1*(16 + 51*t2 - 17*Power(t2,2) + 2*Power(t2,3) + 
               s*(-5 + 4*t2 + Power(t2,2)))) + 
         t2*(-6 + t2 - 2*s2*t2 - 38*Power(t2,2) + 5*s2*Power(t2,2) - 
            27*Power(t2,3) - 3*s2*Power(t2,3) - 2*Power(t2,4) + 
            Power(t1,2)*(-6 - 5*t2 + Power(t2,2) - 2*Power(t2,3)) + 
            t1*(12 + 2*(2 + s2)*t2 - 5*(-9 + s2)*Power(t2,2) + 
               3*(-1 + s2)*Power(t2,3) + 2*Power(t2,4)) + 
            s*(-1 + t2)*(4 + Power(t1,2)*(-2 + t2) + 14*t2 + 
               5*Power(t2,2) - t1*(2 + 7*t2 + Power(t2,2)))) - 
         Power(s1,2)*(-5 + (18 + s - 15*s2)*t2 + 
            (45 + s + 8*s2)*Power(t2,2) + 4*(-3 + s2)*Power(t2,3) + 
            (2 - 2*s + 3*s2)*Power(t2,4) + 
            2*Power(t1,3)*(5 - 2*t2 + Power(t2,2)) - 
            Power(t1,2)*(19 + 2*s2*Power(-1 + t2,2) + 69*t2 - 
               22*Power(t2,2) + 6*Power(t2,3) + 
               s*(-5 + t2 + 4*Power(t2,2))) + 
            t1*(14 + 31*t2 + 78*Power(t2,2) - 12*Power(t2,3) + 
               Power(t2,4) + s2*
                (2 + 11*t2 - 15*Power(t2,2) + 2*Power(t2,3)) + 
               s*(3 - 14*t2 + 9*Power(t2,2) + 2*Power(t2,3)))) + 
         s1*(2*Power(t1,3)*(3 + 4*t2 - 2*Power(t2,2) + Power(t2,3) + 
               s*(-1 + Power(t2,2))) - 
            Power(t1,2)*(12 + 2*(4 + s2)*t2 + (35 - 2*s2)*Power(t2,2) - 
               5*Power(t2,3) + 2*Power(t2,4) + 
               2*s*(1 - 3*t2 + Power(t2,2) + Power(t2,3))) + 
            t1*(6 + 4*t2 + (-48 + 13*s2)*Power(t2,2) + 
               (47 - 13*s2)*Power(t2,3) - 5*Power(t2,4) + 
               s*(4 + 2*t2 - 9*Power(t2,2) + 2*Power(t2,3) + Power(t2,4))\
) + t2*(-4 + 63*t2 + 59*Power(t2,2) - 3*Power(t2,3) + Power(t2,4) - 
               s*(-8 + 4*t2 + 3*Power(t2,2) + Power(t2,4)) + 
               s2*(2 - 15*t2 + 10*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4)))))*T3q(t2,s1))/
     ((-1 + s1)*(s1 - t2)*(-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)) \
+ (8*(-5 + 28*t2 + 5*s*t2 + 5*s2*t2 + 51*Power(t2,2) - 5*s*Power(t2,2) - 
         5*s2*Power(t2,2) - 2*Power(t2,3) - 
         3*Power(t1,2)*(1 + s*(-1 + t2) - 7*t2 + 2*Power(t2,2)) + 
         Power(s1,3)*(-(s2*(-1 + t2)*(1 + 2*t1 + t2)) + 
            t1*(-7 + 2*t1*(-3 + t2) + 4*t2 - Power(t2,2))) + 
         t1*(8 - (55 + 3*s2)*t2 + (-19 + 3*s2)*Power(t2,2) + 
            6*Power(t2,3) + s*(-5 + 2*t2 + 3*Power(t2,2))) + 
         Power(s1,2)*(-1 + 2*Power(t1,3)*(-3 + t2) + 7*t2 + s*t2 - 
            3*Power(t2,2) + Power(t2,3) - s*Power(t2,3) + 
            Power(t1,2)*(-17 + 11*t2 - 2*Power(t2,2)) + 
            s2*(-1 + t2)*(6 - 3*t1 - 2*Power(t1,2) + 4*t2 + 
               Power(t2,2)) + 
            t1*(39 + 13*t2 - 4*Power(t2,2) + s*(-1 + Power(t2,2)))) + 
         s1*(6 + 6*Power(t1,3)*(-3 + t2) - 43*t2 - 6*s*t2 - 
            8*Power(t2,2) + 5*s*Power(t2,2) + Power(t2,3) + 
            s*Power(t2,3) + Power(t1,2)*
             (50 + 3*s*(-1 + t2) + 22*t2 - 8*Power(t2,2)) - 
            s2*(-1 + t2)*(5 + 6*Power(t1,2) + 6*t2 + Power(t2,2) - 
               t1*(13 + 5*t2)) - 
            2*t1*(16 + 13*t2 + 4*Power(t2,2) - Power(t2,3) + 
               s*(-3 + t2 + 2*Power(t2,2)))))*T4q(s1))/
     ((-1 + s1)*(-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)) - 
    (8*(-5 + 27*t2 + 5*s*t2 - 7*s2*t2 + 15*Power(t2,2) - 4*s*Power(t2,2) + 
         4*s2*Power(t2,2) + 13*Power(t2,3) - s*Power(t2,3) + 
         3*s2*Power(t2,3) - 2*Power(t2,4) + 
         Power(t1,3)*(3 + 3*s*(-1 + t2) + 3*t2 - 2*Power(t2,2)) + 
         Power(s1,3)*(-(s2*(-1 + t2)*(1 + 2*t1 + t2)) + 
            t1*(-3 + 2*t1*(-3 + t2) + 8*t2 - Power(t2,2))) + 
         Power(t1,2)*(-11 + (9 - 5*s2)*t2 + (-14 + 5*s2)*Power(t2,2) + 
            4*Power(t2,3) - 2*s*(-4 + t2 + 3*Power(t2,2))) + 
         t1*(13 + 3*(-13 + 4*s2)*t2 - (9 + 7*s2)*Power(t2,2) + 
            (13 - 5*s2)*Power(t2,3) - 2*Power(t2,4) + 
            s*(-5 - 4*t2 + 6*Power(t2,2) + 3*Power(t2,3))) + 
         Power(s1,2)*(-1 + 4*Power(t1,3)*(-3 + t2) + 3*t2 + s*t2 - 
            7*Power(t2,2) + Power(t2,3) - s*Power(t2,3) + 
            Power(t1,2)*(-24 + 29*t2 - 5*Power(t2,2)) + 
            s2*(-1 + t2)*(6 - 4*Power(t1,2) + t1*(-4 + t2) + 5*t2 + 
               2*Power(t2,2)) + 
            t1*(27 + 12*t2 - 12*Power(t2,2) + Power(t2,3) + 
               s*(-1 + Power(t2,2)))) - 
         s1*(-6 - 2*Power(t1,4)*(-3 + t2) + 30*t2 + 6*s*t2 + 
            7*Power(t2,2) - 4*s*Power(t2,2) - 8*Power(t2,3) - 
            s*Power(t2,3) + Power(t2,4) - s*Power(t2,4) + 
            Power(t1,3)*(1 - 17*t2 + 4*Power(t2,2)) + 
            s2*(-1 + t2)*(5 + 2*Power(t1,3) + 11*t2 + 4*Power(t2,2) + 
               Power(t2,3) - Power(t1,2)*(1 + 2*t2) - 
               t1*(6 + 13*t2 + Power(t2,2))) - 
            Power(t1,2)*(s*(-8 + 7*t2 + Power(t2,2)) + 
               2*(19 + 5*t2 - 9*Power(t2,2) + Power(t2,3))) + 
            t1*(37 - 21*t2 + 40*Power(t2,2) - 8*Power(t2,3) + 
               2*s*(-3 - 2*t2 + 4*Power(t2,2) + Power(t2,3)))))*
       T5q(1 - s1 - t1 + t2))/
     ((-1 + s1)*(-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)));
   return a;
};
