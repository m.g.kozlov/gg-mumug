#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m213_4_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(2*Power(s1,10)*t1*(1 + (5 + 2*t1)*t2) + 
         Power(t2,3)*(-1 + s + t2)*
          (Power(t1,3) - 4*t1*t2 - Power(t1,2)*(1 + t2) + 4*t2*(1 + t2))*
          (Power(s,2)*(-2 + t1 - t2) + s*(3 - 3*t1 + (-3 + s2)*t2) + 
            2*(t1 + t2 - Power(t2,2))) + 
         Power(s1,9)*(21*Power(t1,3)*t2 + (-2 + Power(s2,3) - 10*t2)*t2 + 
            Power(t1,2)*(8 + (-8 - 4*s + 3*s2)*t2 - 10*Power(t2,2)) - 
            t1*(2 + 4*s*t2 - 3*Power(s2,2)*t2 - 2*s2*(-12 + t2)*t2 + 
               30*Power(t2,2))) + 
         Power(s1,8)*(30*Power(t1,4)*t2 + 
            Power(t1,3)*(12 + (-90 - 32*s + 25*s2)*t2 - 41*Power(t2,2)) + 
            Power(t1,2)*(-8 + 
               (94 + 34*s - 2*Power(s,2) - 95*s2 + 9*s*s2 + 
                  4*Power(s2,2))*t2 + (-32 + 22*s - 23*s2)*Power(t2,2) + 
               12*Power(t2,3)) + 
            t1*(-6 + (8 + 2*Power(s,2)*(-4 + s2) - 23*s2 + 
                  19*Power(s2,2) + Power(s2,3) + 
                  s*(2 - 18*s2 - 5*Power(s2,2)))*t2 - 
               (70 - 73*s2 + 8*Power(s2,2) + s*(52 + 7*s2))*
                Power(t2,2) + (22 + 6*s - 6*s2)*Power(t2,3)) + 
            t2*(2 + 4*s*t2 - 2*s2*(-12 + t2)*t2 + 26*Power(t2,2) - 
               2*Power(s2,3)*(1 + 2*t2) + 3*Power(s2,2)*(-1 + (-1 + s)*t2)\
)) + Power(s1,7)*(13*Power(t1,5)*t2 + 
            Power(t1,4)*(8 + (-98 - 52*s + 41*s2)*t2 - 52*Power(t2,2)) + 
            Power(t1,3)*(-12 + 
               (304 + 181*s + 7*Power(s,2) - 200*s2 + 3*s*s2 - 
                  Power(s2,2))*t2 + (55 + 75*s - 90*s2)*Power(t2,2) + 
               28*Power(t2,3)) - 
            Power(t1,2)*(16 + 
               (15 - 2*Power(s,3) + 108*s2 - 91*Power(s2,2) + 
                  Power(s2,3) + Power(s,2)*(-9 + 2*s2) + 
                  s*(9 + 123*s2 + 11*Power(s2,2)))*t2 + 
               (147 + 13*Power(s,2) - 299*s2 + 27*Power(s2,2) + 
                  3*s*(73 + 3*s2))*Power(t2,2) + 
               (-53 + 37*s - 43*s2)*Power(t2,3) + 10*Power(t2,4)) + 
            t1*(8 + (-20 + 14*Power(s,3) + Power(s,2)*(46 - 13*s2) + 
                  25*s2 + 22*Power(s2,2) - 17*Power(s2,3) + 
                  s*(8 - 11*s2 + 44*Power(s2,2)))*t2 + 
               (-139 + 2*Power(s,3) + 250*s2 - 85*Power(s2,2) + 
                  Power(s2,3) + Power(s,2)*(48 + s2) + 
                  s*(68 + 115*s2 + 6*Power(s2,2)))*Power(t2,2) + 
               (245 - 14*Power(s,2) - 49*s2 + 10*Power(s2,2) + 
                  s*(115 + 14*s2))*Power(t2,3) + 
               (2 - 12*s + 6*s2)*Power(t2,4)) + 
            t2*(Power(s2,3)*(-1 + 7*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(6 - (7 + 4*s)*t2 + (8 - 9*s)*Power(t2,2)) + 
               s2*(3 + (23 + 12*s - 2*Power(s,2))*t2 + 
                  (-76 + 7*s + 3*Power(s,2))*Power(t2,2) + 6*Power(t2,3)) \
+ 2*(3 + (-8 - s + 4*Power(s,2))*t2 + (39 + 28*s)*Power(t2,2) - 
                  3*(2 + s)*Power(t2,3)))) - 
         Power(s1,6)*(Power(t1,5)*
             (-2 + (8 + 24*s - 19*s2)*t2 + 21*Power(t2,2)) + 
            Power(t1,4)*(8 + (-330 - 20*Power(s,2) + 211*s2 + 
                  2*Power(s2,2) + s*(-244 + 21*s2))*t2 + 
               (-104 - 84*s + 103*s2)*Power(t2,2) - 24*Power(t2,3)) + 
            Power(t1,3)*(16 + 
               (79 - 4*Power(s,3) + 128*s2 - 125*Power(s2,2) + 
                  Power(s2,3) + Power(s,2)*(-31 + 10*s2) + 
                  s*(111 + 181*s2 + 7*Power(s2,2)))*t2 + 
               (630 + 36*Power(s,2) + s*(369 - 25*s2) - 497*s2 + 
                  30*Power(s2,2))*Power(t2,2) + 
               (-99 + 95*s - 113*s2)*Power(t2,3) + 12*Power(t2,4)) - 
            Power(t1,2)*(18 + 
               (-28 + 29*Power(s,3) + Power(s,2)*(50 - 18*s2) + 
                  65*s2 + 36*Power(s2,2) - 28*Power(s2,3) + 
                  s*(21 + 41*s2 + 101*Power(s2,2)))*t2 + 
               (-722 + 7*Power(s,3) + 749*s2 - 238*Power(s2,2) + 
                  14*Power(s2,3) + 5*Power(s,2)*(15 + 2*s2) + 
                  s*(-208 + 397*s2 + 5*Power(s2,2)))*Power(t2,2) + 
               (267 + 5*Power(s,2) + s*(396 - 25*s2) - 219*s2 + 
                  56*Power(s2,2))*Power(t2,3) + 
               (7 + 17*s - 29*s2)*Power(t2,4) + 4*Power(t2,5)) + 
            t1*(-2 + (38*Power(s,3) + Power(s,2)*(67 - 20*s2) - 
                  2*(16 - 9*s2 + Power(s2,2)) + 
                  s*(-24 + 4*s2 + 39*Power(s2,2)))*t2 + 
               (-44 + 29*Power(s,3) + Power(s,2)*(132 - 19*s2) - 
                  224*s2 + 267*Power(s2,2) - 59*Power(s2,3) + 
                  s*(85 - 220*s2 + 86*Power(s2,2)))*Power(t2,2) - 
               (363 + 8*Power(s,3) + Power(s,2)*(29 - 8*s2) - 733*s2 + 
                  177*Power(s2,2) - 5*Power(s2,3) + 
                  s*(256 - 226*s2 + 3*Power(s2,2)))*Power(t2,3) + 
               (217 - 17*Power(s,2) + 43*s2 + 8*Power(s2,2) + 
                  s*(50 + 7*s2))*Power(t2,4) + (-6*s + 2*s2)*Power(t2,5)) \
+ t2*(9 + (-28 + 5*s + 46*Power(s,2) + 14*Power(s,3))*t2 + 
               (-45 + 102*s + 49*Power(s,2) + 2*Power(s,3))*Power(t2,2) - 
               (-234 - 137*s + 14*Power(s,2) + Power(s,3))*Power(t2,3) - 
               2*(-7 + 6*s)*Power(t2,4) + 
               Power(s2,3)*(-2 - 23*t2 + 13*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s2,2)*(-3 + (46 + 41*s)*t2 - 
                  21*(3 + s)*Power(t2,2) + (10 - 9*s)*Power(t2,3)) + 
               s2*(6 + (37 - 29*s - 13*Power(s,2))*t2 + 
                  (155 + 106*s + 13*Power(s,2))*Power(t2,2) + 
                  2*(-36 + 7*s + 3*Power(s,2))*Power(t2,3) + 6*Power(t2,4)\
))) + s1*Power(t2,2)*(Power(t1,5)*
             (1 - Power(s,2)*(-1 + t2) - 2*t2 - 6*Power(t2,2) + 
               s*(-2 + 3*t2)) + 
            Power(t1,4)*(7 - Power(s,3)*t2 + (-7 + 3*s2)*t2 + 
               (39 - 15*s2)*Power(t2,2) + 44*Power(t2,3) + 
               Power(s,2)*(-2 + (7 + 2*s2)*t2 + 6*Power(t2,2)) + 
               s*(-15 + (16 - 13*s2)*t2 + 4*(9 + s2)*Power(t2,2))) + 
            2*t1*t2*(20 - 22*t2 + Power(s2,3)*(-1 + t2)*t2 - 
               8*Power(t2,2) + 6*Power(t2,3) + 132*Power(t2,4) - 
               2*Power(s,3)*(-6 + 3*t2 + 2*Power(t2,2)) + 
               2*Power(s2,2)*t2*(-2 - 12*t2 + 5*Power(t2,2)) + 
               Power(s,2)*(15 + (5 + 18*s2)*t2 + 
                  (-1 + 12*s2)*Power(t2,2) + 23*Power(t2,3)) + 
               s2*(-3 + 17*t2 + 57*Power(t2,2) + 97*Power(t2,3)) + 
               s*(-58 + (4 - 29*s2 + 3*Power(s2,2))*t2 + 
                  (-36 + 110*s2 + 17*Power(s2,2))*Power(t2,2) + 
                  (122 - 7*s2)*Power(t2,3))) + 
            Power(t1,3)*(-11 + (2 - 12*s2)*t2 + 
               3*(5 - 9*s2 + 4*Power(s2,2))*Power(t2,2) + 
               (22 - 45*s2 - 3*Power(s2,2))*Power(t2,3) - 
               64*Power(t2,4) + Power(s,3)*(-5 + 7*t2 + 2*Power(t2,2)) - 
               Power(s,2)*(10 + (17 + 11*s2)*t2 + 
                  (-9 + 5*s2)*Power(t2,2) + 9*Power(t2,3)) - 
               s*(-32 - (17 + 19*s2)*t2 + 
                  (3 + 55*s2 + 10*Power(s2,2))*Power(t2,2) + 
                  (62 + s2)*Power(t2,3))) + 
            Power(t1,2)*(3 + 3*(-11 + 5*s2)*t2 - 
               2*(-7 + 10*s2 - 12*Power(s2,2) + Power(s2,3))*
                Power(t2,2) + 
               (-135 + 35*s2 + 30*Power(s2,2) - 4*Power(s2,3))*
                Power(t2,3) - (207 - 62*s2 + Power(s2,2))*Power(t2,4) + 
               26*Power(t2,5) - 
               Power(s,3)*(-10 - 5*t2 + 11*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(11 + (83 - 2*s2)*t2 + 
                  2*(-3 + s2)*Power(t2,2) + (-55 + 3*s2)*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s*(-15 + (26 - 6*s2)*t2 + 
                  2*(8 + 2*s2 + 15*Power(s2,2))*Power(t2,2) + 
                  (-154 + 63*s2 + 2*Power(s2,2))*Power(t2,3) + 
                  (19 - 3*s2)*Power(t2,4))) + 
            4*t2*(-2 + (13 - 10*s2 + 3*Power(s2,2))*t2 + 
               (12 + 13*s2 - 22*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,2) + 
               (-21 + 20*s2 - 35*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,3) + (24 - 67*s2 + Power(s2,2))*Power(t2,4) - 
               26*Power(t2,5) + 
               Power(s,3)*(-10 - 11*t2 + 10*Power(t2,2) + 
                  2*Power(t2,3)) - 
               Power(s,2)*(11 - 2*(-27 + s2)*t2 + 
                  (22 + 5*s2)*Power(t2,2) + (-22 + 6*s2)*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s*(15 + 13*t2 + 
                  (-73 + 42*s2 - 33*Power(s2,2))*Power(t2,2) + 
                  (11 - 67*s2 + Power(s2,2))*Power(t2,3) + 
                  (-14 + 3*s2)*Power(t2,4)))) + 
         Power(s1,3)*(6*Power(t1,6)*Power(t2,2)*(-2 + 3*t2) + 
            Power(t1,5)*(2 - (5 + 5*Power(s,2) + 2*s*(-5 + 4*s2))*t2 + 
               (64 + 9*Power(s,2) + s*(117 - 6*s2) - 30*s2)*
                Power(t2,2) + 
               (248 + 5*Power(s,2) - 7*s*(-8 + s2) - 82*s2)*
                Power(t2,3) - 30*Power(t2,4)) + 
            Power(t1,3)*(-4 + 
               (-41 + 44*Power(s,3) + Power(s,2)*(38 - 9*s2) - 12*s2 + 
                  s*(-18 + 25*s2))*t2 + 
               (-2 + 49*Power(s,3) - 67*s2 + 36*Power(s2,2) - 
                  4*Power(s2,3) - 5*Power(s,2)*(-23 + 9*s2) + 
                  s*(72 - s2 + 68*Power(s2,2)))*Power(t2,2) - 
               (462 + 15*Power(s,3) - 42*s2 - 159*Power(s2,2) + 
                  23*Power(s2,3) + Power(s,2)*(16 + 11*s2) - 
                  2*s*(-367 + 53*s2 + 39*Power(s2,2)))*Power(t2,3) - 
               (1794 + 2*Power(s,3) + Power(s,2)*(59 - 5*s2) - 
                  1004*s2 + 61*Power(s2,2) - 7*Power(s2,3) + 
                  s*(365 - 177*s2 + 10*Power(s2,2)))*Power(t2,4) + 
               (408 + 5*Power(s,2) + s*(19 - 16*s2) + 3*s2 + 
                  11*Power(s2,2))*Power(t2,5) + 4*Power(t2,6)) + 
            Power(t1,2)*(2 - (-32 + 4*Power(s,2) + 20*Power(s,3) - 
                  15*s2 + 6*s*(5 + 2*s2))*t2 + 
               (-47 + 38*Power(s,3) + 36*s2 + 38*Power(s2,2) - 
                  6*Power(s2,3) + Power(s,2)*(-21 + 44*s2) + 
                  2*s*(-48 - 68*s2 + 15*Power(s2,2)))*Power(t2,2) + 
               (-282 + 50*Power(s,3) + 377*s2 - 90*Power(s2,2) - 
                  15*Power(s2,3) + 3*Power(s,2)*(-84 + 37*s2) + 
                  s*(-445 + 814*s2 + 114*Power(s2,2)))*Power(t2,3) + 
               (-883 + Power(s,3) + 1820*s2 - 438*Power(s2,2) + 
                  33*Power(s2,3) + 2*Power(s,2)*(67 + 4*s2) + 
                  s*(241 + 721*s2 - 28*Power(s2,2)))*Power(t2,4) + 
               (1855 + Power(s,3) + Power(s,2)*(67 - 3*s2) - 664*s2 + 
                  132*Power(s2,2) - Power(s2,3) + 
                  s*(661 - 189*s2 + 3*Power(s2,2)))*Power(t2,5) + 
               (-66 + 4*s - 8*s2)*Power(t2,6)) + 
            t2*(-4 + (-7 - Power(s,2) + 10*Power(s,3) - 52*s2 + 
                  12*Power(s2,2) + s*(49 + 24*s2))*t2 - 
               (-125 + 117*Power(s,3) + Power(s,2)*(321 - 6*s2) + 
                  11*s2 + 126*Power(s2,2) - 24*Power(s2,3) + 
                  2*s*(38 - 111*s2 + 60*Power(s2,2)))*Power(t2,2) - 
               (89*Power(s,3) + Power(s,2)*(334 + 30*s2) + 
                  2*s*(169 + 127*s2 + 96*Power(s2,2)) - 
                  2*(62 + 57*s2 - 201*Power(s2,2) + 42*Power(s2,3)))*
                Power(t2,3) + 
               (187 + Power(s,3) - 955*s2 + 366*Power(s2,2) - 
                  102*Power(s2,3) - Power(s,2)*(75 + 23*s2) + 
                  s*(-372 - 727*s2 + 116*Power(s2,2)))*Power(t2,4) - 
               (631 + 28*Power(s,2) + s*(499 - 169*s2) - 232*s2 + 
                  133*Power(s2,2))*Power(t2,5) + 
               2*(-33 + 2*s + 6*s2)*Power(t2,6)) + 
            Power(t1,4)*t2*(4 + 67*t2 + 221*Power(t2,2) - 
               468*Power(t2,3) + 8*Power(t2,4) + 
               Power(s2,2)*t2*(30 + 47*t2 - 15*Power(t2,2)) + 
               Power(s,3)*(-8 - 6*t2 + Power(t2,2)) + 
               s2*(3 - 69*t2 - 282*Power(t2,2) + 77*Power(t2,3)) - 
               Power(s,2)*(-27 - 57*t2 + 19*Power(t2,2) + 
                  10*Power(t2,3) + 2*s2*(1 + 4*t2 + Power(t2,2))) - 
               s*(-22 - 53*t2 + 38*Power(t2,2) + 91*Power(t2,3) + 
                  Power(s2,2)*t2*(26 + t2) + 
                  s2*(5 + 114*t2 + 123*Power(t2,2) - 23*Power(t2,3)))) - 
            t1*t2*(-6 - 101*t2 - 138*Power(t2,2) - 245*Power(t2,3) - 
               1876*Power(t2,4) + 690*Power(t2,5) + 20*Power(t2,6) + 
               Power(s,3)*t2*(203 + 221*t2 - 68*Power(t2,2) - 
                  8*Power(t2,3)) + 
               2*Power(s2,3)*t2*
                (1 - 13*t2 - 64*Power(t2,2) + 22*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-4 + 248*t2 + 946*Power(t2,2) - 333*Power(t2,3) + 
                  58*Power(t2,4)) + 
               s2*(6 + 22*t2 - 244*Power(t2,2) - 861*Power(t2,3) + 
                  2111*Power(t2,4) + 22*Power(t2,5)) + 
               s*(-18 - 2*(78 - 37*s2 + 3*Power(s2,2))*t2 + 
                  (501 - 475*s2 + 386*Power(s2,2))*Power(t2,2) + 
                  (121 + 95*s2 + 290*Power(s2,2))*Power(t2,3) + 
                  (-338 + 693*s2 - 60*Power(s2,2))*Power(t2,4) + 
                  (110 - 80*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(294 + 779*t2 + 273*Power(t2,2) - 
                  201*Power(t2,3) + 22*Power(t2,4) + 
                  3*s2*(-12 - 51*t2 - 9*Power(t2,2) + 8*Power(t2,3))))) + 
         Power(s1,4)*(6*Power(t1,6)*(1 - 6*t2)*t2 - 
            Power(t1,5)*(2 + (27 + 11*Power(s,2) + s*(83 - 11*s2) - 
                  15*s2)*t2 + 
               (316 + 10*Power(s,2) + s*(145 - 22*s2) - 164*s2)*
                Power(t2,2) + (-63 + 13*s - 19*s2)*Power(t2,3)) + 
            Power(t1,4)*(8 + (Power(s,3) + Power(s,2)*(-50 + 8*s2) - 
                  3*(8 - 9*s2 + 5*Power(s2,2)) + 
                  s*(13 + 43*s2 + 13*Power(s2,2)))*t2 + 
               (56 + 3*Power(s,3) + 183*s2 - 103*Power(s2,2) + 
                  Power(s,2)*(-5 + 8*s2) + 
                  s*(184 + 179*s2 + 2*Power(s2,2)))*Power(t2,2) + 
               (969 + 35*Power(s,2) + s*(316 - 51*s2) - 365*s2 + 
                  28*Power(s2,2))*Power(t2,3) + 
               (-71 + 21*s - 21*s2)*Power(t2,4)) + 
            Power(t1,3)*(4 + (37 - 54*Power(s,3) + 20*s2 - 
                  12*Power(s2,2) + 2*Power(s2,3) + 
                  2*Power(s,2)*(-15 + 8*s2) + 
                  s*(6 - 10*s2 - 39*Power(s2,2)))*t2 + 
               (271 - 39*Power(s,3) - 144*s2 - 75*Power(s2,2) + 
                  31*Power(s2,3) + Power(s,2)*(20 + 9*s2) - 
                  8*s*(-59 + 32*s2 + 18*Power(s2,2)))*Power(t2,2) + 
               (1972 + 8*Power(s,3) + Power(s,2)*(19 - 12*s2) - 
                  1662*s2 + 199*Power(s2,2) - 15*Power(s2,3) + 
                  s*(650 - 539*s2 + 13*Power(s2,2)))*Power(t2,3) - 
               (827 + 19*Power(s,2) + s*(236 - 71*s2) - 91*s2 + 
                  54*Power(s2,2))*Power(t2,4) - 
               4*(2 + s - 2*s2)*Power(t2,5)) + 
            Power(t1,2)*(-10 + 
               (-58 + 73*Power(s,3) + Power(s,2)*(66 - 18*s2) - 
                  28*s2 - 7*Power(s2,2) + 2*Power(s2,3) + 
                  14*s*(-1 + 5*s2))*t2 + 
               (69 + 77*Power(s,3) + Power(s,2)*(369 - 127*s2) - 
                  177*s2 + 84*Power(s2,2) + Power(s2,3) + 
                  s*(116 - 347*s2 + 61*Power(s2,2)))*Power(t2,2) + 
               (17 - 43*Power(s,3) + Power(s,2)*(178 - 61*s2) - 
                  1438*s2 + 868*Power(s2,2) - 73*Power(s2,3) + 
                  s*(52 - 991*s2 + 136*Power(s2,2)))*Power(t2,3) - 
               (2578 + 7*Power(s,3) + Power(s,2)*(207 - 16*s2) - 
                  1807*s2 + 287*Power(s2,2) - 14*Power(s2,3) + 
                  s*(1206 - 473*s2 + 23*Power(s2,2)))*Power(t2,4) + 
               2*(154 + Power(s,2) + 55*s2 + 7*Power(s2,2) - 
                  2*s*(15 + 4*s2))*Power(t2,5)) + 
            t1*(2 + (-5 + Power(s,2) - 10*Power(s,3) + 28*s2 - 
                  6*Power(s2,2) - 4*s*(10 + 3*s2))*t2 + 
               (-141 + 201*Power(s,3) + Power(s,2)*(434 - 59*s2) - 
                  38*s2 + 106*Power(s2,2) - 18*Power(s2,3) + 
                  s*(65 - 147*s2 + 210*Power(s2,2)))*Power(t2,2) + 
               (153*Power(s,3) + Power(s,2)*(654 - 54*s2) + 
                  s*(327 + 105*s2 + 463*Power(s2,2)) - 
                  2*(167 + 124*s2 - 349*Power(s2,2) + 83*Power(s2,3)))*
                Power(t2,3) + 
               (-1502 - 22*Power(s,3) + 2222*s2 - 643*Power(s2,2) + 
                  113*Power(s2,3) + Power(s,2)*(68 + 41*s2) - 
                  2*s*(-66 - 629*s2 + 59*Power(s2,2)))*Power(t2,4) + 
               (20*Power(s,2) + s*(731 - 210*s2) + 
                  6*(174 - 43*s2 + 30*Power(s2,2)))*Power(t2,5) - 
               2*(-49 + 3*s + 9*s2)*Power(t2,6)) + 
            t2*(7 - (7 - 11*s + 28*Power(s,2) + 34*Power(s,3))*t2 + 
               (-61 + 164*s + 36*Power(s,2) + 2*Power(s,3))*Power(t2,2) + 
               (-86 + 5*s + 51*Power(s,2) + 32*Power(s,3))*Power(t2,3) + 
               (412 + 494*s + 59*Power(s,2) + 9*Power(s,3))*Power(t2,4) + 
               (3 - 14*s + 3*Power(s,2))*Power(t2,5) - 
               Power(s2,3)*t2*
                (6 + 28*t2 - 117*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(6 + 9*(4 - 7*s + Power(s,2))*t2 + 
                  (-68 + 24*s + 21*Power(s,2))*Power(t2,2) + 
                  (557 + 534*s - 5*Power(s,2))*Power(t2,3) - 
                  (477 + 228*s + 17*Power(s,2))*Power(t2,4) - 
                  32*Power(t2,5)) + 
               Power(s2,2)*t2*
                (28 + 155*t2 - 422*Power(t2,2) + 174*Power(t2,3) - 
                  3*Power(t2,4) + 
                  s*(-6 + 26*t2 - 117*Power(t2,2) + 13*Power(t2,3))))) + 
         Power(s1,2)*t2*(6*Power(t1,6)*Power(t2,2) - 
            Power(t1,5)*(3 - 9*t2 + (29 - 15*s2)*Power(t2,2) + 
               60*Power(t2,3) + Power(s,2)*(-5 + 4*Power(t2,2)) + 
               s*(-8 + (25 - 8*s2)*t2 + (34 + 5*s2)*Power(t2,2))) + 
            Power(t1,4)*(-5 - (7 + 6*s2)*t2 + 
               (-74 + 57*s2 - 15*Power(s2,2))*Power(t2,2) + 
               (-232 + 127*s2 + 3*Power(s2,2))*Power(t2,3) + 
               92*Power(t2,4) + Power(s,3)*(5 + 3*t2 - Power(t2,2)) + 
               Power(s,2)*t2*(-17 - 19*t2 + 3*Power(t2,2)) + 
               s*(-8 + (13 + 18*s2)*t2 + 
                  (-126 + 67*s2 + 13*Power(s2,2))*Power(t2,2) + 
                  (1 + 9*s2)*Power(t2,3))) + 
            Power(t1,3)*(13 + (37 + 24*s2)*t2 + 
               2*(-34 + 37*s2 - 18*Power(s2,2) + Power(s2,3))*
                Power(t2,2) + 
               (220 + 80*s2 - 77*Power(s2,2) + 5*Power(s2,3))*
                Power(t2,3) + 
               2*(302 - 79*s2 + 8*Power(s2,2))*Power(t2,4) - 
               34*Power(t2,5) + 
               Power(s,3)*(-10 - 13*t2 + 3*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s,2)*(-5 + (-50 + 19*s2)*t2 + 
                  (-44 + 34*s2)*Power(t2,2) - (-70 + s2)*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s*(-6 - (101 + 43*s2)*t2 + 
                  (53 + 65*s2 - 19*Power(s2,2))*Power(t2,2) - 
                  4*(-83 - 12*s2 + Power(s2,2))*Power(t2,3) + 
                  (85 - 20*s2)*Power(t2,4))) - 
            Power(t1,2)*(5 + (-1 + 30*s2)*t2 - 
               (95 + 12*s2 - 55*Power(s2,2) + 6*Power(s2,3))*
                Power(t2,2) + 
               (-288 + 239*s2 + 8*Power(s2,2) - 15*Power(s2,3))*
                Power(t2,3) + 
               (-830 + 795*s2 - 16*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,4) + 
               (601 - 18*s2 + 11*Power(s2,2))*Power(t2,5) + 
               4*Power(t2,6) + 
               Power(s,3)*t2*
                (62 + 29*t2 - 18*Power(t2,2) + Power(t2,3)) + 
               s*(-6 - (149 + 17*s2)*t2 + 
                  (149 - 62*s2 + 60*Power(s2,2))*Power(t2,2) + 
                  (-494 + 520*s2 + 99*Power(s2,2))*Power(t2,3) + 
                  (206 + 122*s2 - 7*Power(s2,2))*Power(t2,4) - 
                  8*(-5 + 2*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(88 + 64*t2 - 56*Power(t2,2) + 
                  15*Power(t2,3) + 5*Power(t2,4) - 
                  s2*(3 - 27*t2 - 25*Power(t2,2) + Power(t2,3)))) + 
            t2*(Power(s,3)*t2*
                (130 + 121*t2 - 37*Power(t2,2) - 4*Power(t2,3)) + 
               s*(-24 - 5*(35 + 4*s2)*t2 + 
                  3*(119 - 109*s2 + 86*Power(s2,2))*Power(t2,2) + 
                  (231 + 496*s2 + 114*Power(s2,2))*Power(t2,3) + 
                  (307 + 303*s2 - 52*Power(s2,2))*Power(t2,4) - 
                  8*(-15 + 8*s2)*Power(t2,5)) + 
               2*(6 + (-22 + 43*s2 - 12*Power(s2,2))*t2 - 
                  (87 + 37*s2 - 96*Power(s2,2) + 15*Power(s2,3))*
                   Power(t2,2) + 
                  (17 - 81*s2 + 202*Power(s2,2) - 31*Power(s2,3))*
                   Power(t2,3) + 
                  (-91 + 399*s2 - 60*Power(s2,2) + 18*Power(s2,3))*
                   Power(t2,4) + 
                  (209 - 16*s2 + 22*Power(s2,2))*Power(t2,5) + 
                  8*Power(t2,6)) + 
               Power(s,2)*t2*(243 + 475*t2 + 117*Power(t2,2) - 
                  43*Power(t2,3) + 20*Power(t2,4) + 
                  s2*(-12 + t2 + 53*Power(t2,2) + 20*Power(t2,3)))) + 
            t1*t2*(Power(s,3)*
                (60 + 67*t2 - 42*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s,2)*(42 + (237 - 68*s2)*t2 + 
                  (281 - 109*s2)*Power(t2,2) + 
                  4*(-55 + s2)*Power(t2,3) - 32*Power(t2,4)) + 
               s*(-18 + (86 + 140*s2 - 12*Power(s2,2))*t2 + 
                  (472 - 589*s2 + 142*Power(s2,2))*Power(t2,2) + 
                  (-484 + 67*s2 + 26*Power(s2,2))*Power(t2,3) + 
                  2*(-152 + 53*s2)*Power(t2,4)) - 
               2*(20 + 27*t2 - 22*Power(t2,2) + 33*Power(t2,3) + 
                  580*Power(t2,4) - 90*Power(t2,5) + 
                  Power(s2,3)*t2*(-2 + 7*t2 + 15*Power(t2,2)) + 
                  Power(s2,2)*t2*
                   (-5 - 97*t2 - 186*Power(t2,2) + 41*Power(t2,3)) + 
                  s2*(-6 + 20*t2 + 151*Power(t2,2) + 378*Power(t2,3) - 
                     379*Power(t2,4))))) + 
         Power(s1,5)*(18*Power(t1,6)*t2 + 
            Power(t1,5)*(-2 + 
               (126 + 101*s + 11*Power(s,2) - 82*s2 - 15*s*s2)*t2 + 
               (-25 + 31*s - 38*s2)*Power(t2,2) + 8*Power(t2,3)) - 
            Power(t1,4)*(8 + (81 - 2*Power(s,3) + 28*s2 - 
                  53*Power(s2,2) + Power(s,2)*(-3 + 6*s2) + 
                  s*(183 + 65*s2 + Power(s2,2)))*t2 + 
               (931 + 33*Power(s,2) - 499*s2 + 11*Power(s2,2) - 
                  7*s*(-61 + 7*s2))*Power(t2,2) + 
               (-57 + 65*s - 83*s2)*Power(t2,3) + 2*Power(t2,4)) + 
            Power(t1,3)*(16 + 
               (-33 + 16*Power(s,3) + 67*s2 - 4*Power(s2,2) - 
                  13*Power(s2,3) + Power(s,2)*(-41 + 3*s2) + 
                  s*(16 + 103*s2 + 70*Power(s2,2)))*t2 + 
               (-627 + 8*Power(s,3) + 944*s2 - 279*Power(s2,2) + 
                  9*Power(s2,3) + Power(s,2)*(-39 + 17*s2) + 
                  s*(-397 + 563*s2 + 4*Power(s2,2)))*Power(t2,2) + 
               (767 + 49*Power(s,2) + s*(477 - 83*s2) - 391*s2 + 
                  74*Power(s2,2))*Power(t2,3) + 
               (-60 + 50*s - 56*s2)*Power(t2,4) + 4*Power(t2,5)) + 
            Power(t1,2)*(6 + (44 - 84*Power(s,3) + 4*s2 - 
                  16*Power(s2,2) + 3*Power(s2,3) + 
                  Power(s,2)*(-119 + 38*s2) + 
                  s*(8 - 9*s2 - 78*Power(s2,2)))*t2 + 
               (285 - 70*Power(s,3) + 286*s2 - 481*Power(s2,2) + 
                  75*Power(s2,3) + Power(s,2)*(-333 + 70*s2) + 
                  s*(95 + 354*s2 - 216*Power(s2,2)))*Power(t2,2) + 
               (2043 + 15*Power(s,3) + Power(s,2)*(55 - 21*s2) - 
                  1802*s2 + 313*Power(s2,2) - 26*Power(s2,3) + 
                  s*(658 - 574*s2 + 26*Power(s2,2)))*Power(t2,3) + 
               (-460 + 8*Power(s,2) - 87*s2 - 47*Power(s2,2) + 
                  s*(-137 + 41*s2))*Power(t2,4) + 
               2*(-10 + s + 3*s2)*Power(t2,5)) - 
            t1*(6 + (19 - 34*Power(s,3) + s*(8 - 45*s2) + 18*s2 + 
                  4*Power(s2,2) - 4*Power(s2,3) + 
                  Power(s,2)*(-28 + 9*s2))*t2 + 
               (-95 + 31*Power(s,3) + Power(s,2)*(98 - 3*s2) + 33*s2 + 
                  146*Power(s2,2) - 68*Power(s2,3) + 
                  s*(158 + 59*s2 + 160*Power(s2,2)))*Power(t2,2) + 
               (-504 + 31*Power(s,3) + 1118*s2 - 637*Power(s2,2) + 
                  111*Power(s2,3) + Power(s,2)*(157 + 23*s2) + 
                  s*(-31 + 880*s2 - 100*Power(s2,2)))*Power(t2,3) + 
               (764 + 4*Power(s,3) + Power(s,2)*(67 - 5*s2) - 786*s2 + 
                  233*Power(s2,2) - 3*Power(s2,3) + 
                  s*(965 - 259*s2 + 4*Power(s2,2)))*Power(t2,4) + 
               (38 + 3*s + 3*Power(s,2) - 61*s2 - 3*Power(s2,2))*
                Power(t2,5) + 4*Power(t2,6)) + 
            t2*(Power(s2,3)*t2*
                (-8 - 73*t2 + 13*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(-6 + 4*(-5 + 12*s)*t2 + 
                  (212 + 94*s)*Power(t2,2) - 6*(23 + 5*s)*Power(t2,3) + 
                  (8 - 3*s)*Power(t2,4)) + 
               s2*(-3 - 2*(-21 + s + 10*Power(s,2))*t2 - 
                  (98 + 151*s + 5*Power(s,2))*Power(t2,2) + 
                  (409 + 217*s + 32*Power(s,2))*Power(t2,3) + 
                  s*(7 + 3*s)*Power(t2,4) + 2*Power(t2,5)) - 
               t2*(12 + 41*t2 + 126*Power(t2,2) - 205*Power(t2,3) - 
                  10*Power(t2,4) + 
                  Power(s,3)*
                   (-38 - 27*t2 + 13*Power(t2,2) + Power(t2,3)) + 
                  Power(s,2)*(-67 - 135*t2 + 10*Power(t2,2) + 
                     17*Power(t2,3)) + 
                  s*(33 - 85*t2 + 5*Power(t2,2) - 87*Power(t2,3) + 
                     6*Power(t2,4)))))))/
     (Power(-1 + s1,2)*s1*(s - s2 + t1)*
       (Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*Power(s1 - t2,2)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*(-1 + t2)*t2*(s - s1 + t2)) - 
    (8*(-2 + Power(s1,8)*(Power(s2,3) + s2*Power(t1,2) - 2*Power(t1,3)) - 
         Power(-1 + s,2)*Power(t1,5)*(-1 + s - t2) - 5*s*t2 - 5*s2*t2 + 
         6*s*s2*t2 + 2*Power(t2,2) + 9*s*Power(t2,2) - 
         2*Power(s,2)*Power(t2,2) - 3*s2*Power(t2,2) - 
         9*s*s2*Power(t2,2) + 4*Power(s,2)*s2*Power(t2,2) - 
         3*Power(s2,2)*Power(t2,2) + 4*s*Power(s2,2)*Power(t2,2) + 
         Power(t2,3) - 3*s*Power(t2,3) + 5*Power(s,2)*Power(t2,3) - 
         Power(s,3)*Power(t2,3) + s2*Power(t2,3) - 21*s*s2*Power(t2,3) - 
         10*Power(s,2)*s2*Power(t2,3) - 13*s*Power(s2,2)*Power(t2,3) + 
         Power(t2,4) - 8*s*Power(t2,4) + 4*Power(s,2)*Power(t2,4) + 
         4*Power(s,3)*Power(t2,4) - 4*s2*Power(t2,4) - 
         71*s*s2*Power(t2,4) + 2*Power(s,2)*s2*Power(t2,4) + 
         5*Power(s2,2)*Power(t2,4) - 8*s*Power(s2,2)*Power(t2,4) + 
         2*Power(s2,3)*Power(t2,4) - Power(t2,5) - 12*s*Power(t2,5) + 
         21*Power(s,2)*Power(t2,5) - Power(s,3)*Power(t2,5) - 
         16*s2*Power(t2,5) - 33*s*s2*Power(t2,5) + 
         2*Power(s,2)*s2*Power(t2,5) + 10*Power(s2,2)*Power(t2,5) - 
         s*Power(s2,2)*Power(t2,5) - Power(t2,6) + 3*s*Power(t2,6) - 
         5*s2*Power(t2,6) + Power(t1,4)*
          (-6 + 3*Power(s,3) + t2 - 4*s2*t2 - (-3 + s2)*Power(t2,2) + 
            4*Power(t2,3) - 2*Power(s,2)*(6 + 2*s2*t2 + Power(t2,2)) + 
            s*(15 + (-1 + 8*s2)*t2 + (1 + s2)*Power(t2,2))) + 
         Power(t1,3)*(14 + (-9 + 17*s2)*t2 + 
            (1 - 13*s2 + 5*Power(s2,2))*Power(t2,2) - 
            2*(1 + 2*s2)*Power(t2,3) - 12*Power(t2,4) + 
            Power(s,3)*(-2 + t2 + 3*Power(t2,2)) - 
            Power(s,2)*(-15 + s2*(-12 + t2)*t2 + 9*Power(t2,2) + 
               2*Power(t2,3)) + 
            s*(-27 + (6 - 29*s2)*t2 + 
               (2 + 8*s2 - 5*Power(s2,2))*Power(t2,2) - 
               (-16 + s2)*Power(t2,3))) + 
         Power(t1,2)*(-16 + (11 - 27*s2)*t2 + 
            (-9 + 26*s2 - 13*Power(s2,2))*Power(t2,2) + 
            (16 + s2 + 12*Power(s2,2) - 2*Power(s2,3))*Power(t2,3) + 
            2*(-7 + 8*s2)*Power(t2,4) + 12*Power(t2,5) + 
            Power(s,3)*(t2 - 7*Power(t2,2) - 3*Power(t2,3)) + 
            Power(s,2)*(-6 + (3 - 14*s2)*t2 + 
               2*(11 + 7*s2)*Power(t2,2) + (23 + 8*s2)*Power(t2,3) + 
               6*Power(t2,4)) - 
            s*(-21 + (6 - 40*s2)*t2 + 
               (-2 + 23*s2 - 13*Power(s2,2))*Power(t2,2) + 
               (23 + 21*s2 + 2*Power(s2,2))*Power(t2,3) + 
               (28 + 5*s2)*Power(t2,4))) + 
         Power(s1,7)*(Power(s2,3)*(3*t1 - 4*t2) + 
            Power(s2,2)*(-3 + 2*Power(t1,2) + t1*(4 + s - t2) + 
               3*s*t2) - s2*t1*
             (-2*Power(t1,2) + s*(3 + t1 - 3*t2) + 2*t2 + t1*(10 + t2)) \
+ Power(t1,2)*(-1 - 5*Power(t1,2) + 6*t2 + s*(-3 + 2*t1 + 4*t2) + 
               2*t1*(-2 + 5*t2))) + 
         t1*(9 + (-4 + 19*s2)*t2 + 
            (3 - 9*s2 + 11*Power(s2,2))*Power(t2,2) + 
            (-19 + 2*s2 - 12*Power(s2,2) + 2*Power(s2,3))*Power(t2,3) - 
            (-42 + 46*s2 + Power(s2,2) + Power(s2,3))*Power(t2,4) + 
            (5 + 2*s2 - 2*Power(s2,2))*Power(t2,5) - 4*Power(t2,6) + 
            Power(s,3)*Power(t2,2)*(2 - t2 + 2*Power(t2,2)) + 
            s*(-6 + (8 - 25*s2)*t2 + 
               (-14 + 23*s2 - 12*Power(s2,2))*Power(t2,2) + 
               (89 - 7*s2 + 21*Power(s2,2))*Power(t2,3) + 
               (17 + 62*s2 + 4*Power(s2,2))*Power(t2,4) + 
               5*(2 + s2)*Power(t2,5)) - 
            Power(s,2)*t2*(4 - 13*t2 + 26*Power(t2,2) + 38*Power(t2,3) + 
               3*Power(t2,4) + 
               s2*(-6 + 22*t2 + 12*Power(t2,2) + 5*Power(t2,3)))) + 
         Power(s1,6)*(Power(s2,3)*
             (-4 + 3*Power(t1,2) + t1*(2 - 11*t2) - 2*t2 + 
               6*Power(t2,2)) + 
            s2*(3 + 2*Power(t1,4) + Power(t2,2) + 
               3*Power(s,2)*Power(t2,2) - 3*s*t2*(1 + t2) + 
               Power(t1,3)*(-19 - 3*s + 3*t2) + 
               3*Power(t1,2)*(3 + (8 + 5*s)*t2 - Power(t2,2)) + 
               t1*(-8 + s + 22*t2 + 13*s*t2 - Power(s,2)*t2 + 
                  2*Power(t2,2) - 8*s*Power(t2,2))) + 
            Power(s2,2)*(5*Power(t1,3) + Power(t1,2)*(2 + 3*s - 6*t2) + 
               t1*(-11 - 22*t2 + 3*Power(t2,2) + s*(-4 + 9*t2)) + 
               t2*(8 + t2 - s*(4 + 9*t2))) - 
            t1*(4*Power(t1,4) + Power(t1,3)*(14 - 19*t2) + 
               2*t2*(-1 + 3*t2) + 
               2*Power(t1,2)*(5 - 10*t2 + 7*Power(t2,2)) + 
               t1*(-10 - 13*t2 + 30*Power(t2,2)) + 
               Power(s,2)*(-3*(-1 + t2)*t2 + t1*(-1 + 5*t2)) + 
               s*(-3 - 5*Power(t1,3) - 3*t2 + 8*Power(t2,2) + 
                  Power(t1,2)*(7 + t2) + t1*(1 + 10*t2 + 5*Power(t2,2))))\
) + Power(s1,5)*(-1 - Power(t1,6) + Power(t1,5)*(4*s + 9*(-2 + t2)) - 
            Power(t2,2) + 2*Power(t2,3) + 4*s*Power(t2,3) - 
            3*Power(s,2)*Power(t2,3) + Power(s,3)*Power(t2,3) - 
            Power(t1,4)*(19 - 26*t2 + 14*Power(t2,2) + 5*s*(1 + 3*t2)) + 
            Power(s2,3)*(-2 + Power(t1,3) + Power(t1,2)*(3 - 10*t2) + 
               16*t2 + 7*Power(t2,2) - 4*Power(t2,3) + 
               t1*t2*(-13 + 15*t2)) + 
            Power(t1,3)*(23 + Power(s,2)*(1 - 11*t2) + 68*t2 - 
               65*Power(t2,2) + 6*Power(t2,3) + 
               s*(20 - 35*t2 + 12*Power(t2,2))) + 
            Power(t1,2)*(-11 + Power(s,3)*t2 - 27*Power(t2,2) + 
               42*Power(t2,3) + 
               Power(s,2)*(7 - 9*t2 + 15*Power(t2,2)) + 
               s*(21 + 17*t2 + 12*Power(t2,2) - Power(t2,3))) + 
            t1*(4 - 21*t2 - 14*Power(t2,2) - 2*Power(s,3)*Power(t2,2) + 
               30*Power(t2,3) + 
               Power(s,2)*t2*(5 + 11*t2 - 4*Power(t2,2)) + 
               s*(-2 - 9*t2 + 22*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s2,2)*(12 + 4*Power(t1,4) + 
               3*Power(t1,3)*(-1 + s - 3*t2) - 8*(-1 + s)*t2 + 
               (2 + 3*s)*Power(t2,2) + (-3 + 9*s)*Power(t2,3) + 
               Power(t1,2)*(-19 - 17*t2 + 4*Power(t2,2) + 
                  2*s*(-4 + 5*t2)) + 
               t1*(-21 + 31*t2 + 44*Power(t2,2) - 3*Power(t2,3) + 
                  s*(-6 + 4*t2 - 27*Power(t2,2)))) + 
            s2*(2*Power(t1,5) + Power(t1,4)*(-11 - 4*s + 7*t2) + 
               Power(t1,3)*(3 + 27*t2 - 13*Power(t2,2) + 
                  s*(7 + 24*t2)) + 
               Power(t1,2)*(3 + 24*t2 - 40*Power(t2,2) + 
                  5*Power(t2,3) - 2*Power(s,2)*(1 + 2*t2) + 
                  s*(-8 + t2 - 22*Power(t2,2))) - 
               t2*(4 + 12*t2 + Power(t2,2) + Power(s,2)*t2*(5 + 6*t2) - 
                  s*(5 + 6*t2 + 8*Power(t2,2))) + 
               t1*(13 + 18*t2 - 60*Power(t2,2) + 6*Power(t2,3) + 
                  7*Power(s,2)*t2*(-1 + 2*t2) + 
                  s*(20 - 51*t2 - 50*Power(t2,2) + 7*Power(t2,3))))) + 
         Power(s1,4)*((-12 + s)*Power(t1,6) - 
            Power(t1,5)*(12 - 22*t2 + s*(7 + 10*t2)) + 
            Power(t1,4)*(28 + 93*t2 - 43*Power(t2,2) - 
               2*Power(s,2)*(3 + 2*t2) + s*(41 - 21*t2 + 19*Power(t2,2))\
) + Power(t1,3)*(23 + 19*t2 - 65*Power(t2,2) + 49*Power(t2,3) + 
               Power(s,3)*(-2 + 3*t2) + 
               Power(s,2)*(6 + 9*t2 + 9*Power(t2,2)) + 
               s*(22 + 3*t2 + 59*Power(t2,2) - 12*Power(t2,3))) - 
            t2*(-4*Power(s,2)*Power(t2,3) + 
               Power(s,3)*Power(t2,2)*(1 + t2) + 
               t2*(-11 - 5*t2 + 10*Power(t2,2)) + 
               s*(1 - t2 + 14*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s2,3)*(3 + Power(t1,3)*(1 - 3*t2) + 8*t2 - 
               22*Power(t2,2) - 9*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*t2*(-9 + 11*t2) - 
               t1*(3 + 4*t2 - 29*Power(t2,2) + 9*Power(t2,3))) - 
            Power(t1,2)*(5 + 30*t2 + 89*Power(t2,2) - 76*Power(t2,3) + 
               18*Power(t2,4) + Power(s,3)*t2*(-5 + 7*t2) + 
               Power(s,2)*(6 - 22*t2 - 26*Power(t2,2) + 
                  6*Power(t2,3)) + 
               s*(-13 + 91*t2 - 59*Power(t2,2) + 33*Power(t2,3) - 
                  2*Power(t2,4))) + 
            t1*(-5 + 4*t2 + 33*Power(t2,2) + 14*Power(t2,3) - 
               42*Power(t2,4) + Power(s,3)*Power(t2,2)*(-2 + 5*t2) + 
               Power(s,2)*t2*
                (2 - 24*t2 - 29*Power(t2,2) + Power(t2,3)) + 
               s*(-16 + 7*Power(t2,2) - 28*Power(t2,3) + 2*Power(t2,4))) \
+ Power(s2,2)*(6 + Power(t1,5) + Power(t1,4)*(s - 5*t2) + 
               3*(-11 + 4*s)*t2 + (-21 + 32*s)*Power(t2,2) + 
               (-26 + 9*s)*Power(t2,3) - 3*(-1 + s)*Power(t2,4) + 
               Power(t1,3)*((-8 + t2)*t2 + s*(-8 + 6*t2)) + 
               Power(t1,2)*(s*(2 + 7*t2 - 27*Power(t2,2)) + 
                  t2*(48 + 27*t2 + 2*Power(t2,2))) + 
               t1*(-3 + 114*t2 - 19*Power(t2,2) - 36*Power(t2,3) + 
                  Power(t2,4) + 
                  s*(4 + 18*t2 - 25*Power(t2,2) + 23*Power(t2,3)))) + 
            s2*(-12 + Power(t1,6) - 3*Power(t1,5)*(s - t2) + 
               2*(-5 + 2*s)*t2 + 
               (-9 + 27*s - 2*Power(s,2))*Power(t2,2) + 
               (34 + 17*s + Power(s,2))*Power(t2,3) + 
               (-3 - 7*s + 3*Power(s,2))*Power(t2,4) + 
               Power(t1,4)*(-23 - 10*Power(t2,2) + s*(11 + 13*t2)) - 
               Power(t1,3)*(33 - 33*t2 + 7*Power(s,2)*t2 + 
                  40*Power(t2,2) - 8*Power(t2,3) + 
                  s*(23 + 17*t2 + 12*Power(t2,2))) + 
               Power(t1,2)*(-3 - 37*t2 - 49*Power(t2,2) + 
                  51*Power(t2,3) - 2*Power(t2,4) + 
                  Power(s,2)*t2*(-17 + 23*t2) + 
                  s*(4 - 65*t2 - 62*Power(t2,2) + 4*Power(t2,3))) + 
               t1*(36 - 43*t2 - 55*Power(t2,2) + 77*Power(t2,3) - 
                  10*Power(t2,4) + 
                  Power(s,2)*t2*(12 + 6*t2 - 19*Power(t2,2)) + 
                  s*(6 - 51*t2 + 125*Power(t2,2) + 67*Power(t2,3) - 
                     2*Power(t2,4))))) + 
         Power(s1,3)*(4 - 4*Power(t1,7) - 6*Power(t1,6)*(1 + s - 2*t2) + 
            4*t2 + 4*s*t2 + Power(t2,2) - 9*s*Power(t2,2) - 
            23*Power(t2,3) - 8*s*Power(t2,3) + 
            18*Power(s,2)*Power(t2,3) - 2*Power(t2,4) + 
            17*s*Power(t2,4) + 11*Power(s,2)*Power(t2,4) - 
            Power(s,3)*Power(t2,4) + 14*Power(t2,5) - s*Power(t2,5) - 
            Power(s,2)*Power(t2,5) + 
            t1*(-17 + (23 + 31*s - 14*Power(s,2))*t2 + 
               (23 + 31*s - 43*Power(s,2) + 2*Power(s,3))*Power(t2,2) + 
               (27 - 56*s + 29*Power(s,2) + 5*Power(s,3))*Power(t2,3) + 
               (-33 + 32*s + 15*Power(s,2) - 2*Power(s,3))*
                Power(t2,4) + (18 - 4*s)*Power(t2,5)) + 
            Power(t1,5)*(3 + 48*t2 - 12*Power(t2,2) + 
               Power(s,2)*(-5 + 2*t2) + s*(7 + 10*t2)) + 
            Power(t1,4)*(2 + 2*Power(s,3)*(-1 + t2) + 11*t2 - 
               64*Power(t2,2) + 4*Power(t2,3) + 
               Power(s,2)*(-17 + 19*t2 - 6*Power(t2,2)) + 
               s*(-29 + 8*t2 + 29*Power(t2,2))) - 
            Power(t1,3)*(1 + 127*t2 + 159*Power(t2,2) - 
               77*Power(t2,3) + Power(s,3)*(2 - 7*t2 + 6*Power(t2,2)) - 
               Power(s,2)*(-23 + 43*t2 + 2*Power(t2,2) + 
                  6*Power(t2,3)) + 
               s*(22 + 131*t2 - 32*Power(t2,2) + 60*Power(t2,3))) + 
            Power(s2,3)*(2 - 11*t2 - 11*Power(t2,2) + 9*Power(t2,3) + 
               5*Power(t2,4) + 2*Power(t1,3)*(1 - t2 + Power(t2,2)) - 
               2*Power(t1,2)*t2*(1 - 6*t2 + 2*Power(t2,2)) + 
               t1*(-4 + 11*t2 + 9*Power(t2,2) - 27*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(t1,2)*(19 - 39*t2 + 9*Power(t2,2) + 67*Power(t2,3) - 
               61*Power(t2,4) + 3*Power(s,3)*Power(t2,2)*(-3 + 2*t2) - 
               2*Power(s,2)*(7 - 10*t2 + 33*Power(t2,2) + 
                  15*Power(t2,3) + Power(t2,4)) + 
               s*(-29 - 65*t2 + 51*Power(t2,2) - 62*Power(t2,3) + 
                  31*Power(t2,4))) + 
            s2*(-6 - (-2 + s)*Power(t1,6) - 18*(-1 + s)*t2 + 
               (28 - 19*s + 8*Power(s,2))*Power(t2,2) + 
               (38 - 87*s + 16*Power(s,2))*Power(t2,3) + 
               (-40 - 39*s + 7*Power(s,2))*Power(t2,4) + 
               (5 + 2*s)*Power(t2,5) + 
               Power(t1,5)*(-2 - 9*t2 + s*(6 + t2)) + 
               Power(t1,4)*(-9 + Power(s,2)*(1 - 4*t2) + 22*t2 - 
                  12*Power(t2,2) + s*(16 - 16*t2 + 5*Power(t2,2))) + 
               Power(t1,3)*(18 + 101*t2 - 13*Power(t2,2) + 
                  36*Power(t2,3) + 
                  2*Power(s,2)*(8 - 9*t2 + 7*Power(t2,2)) - 
                  s*(-36 + 43*t2 + 30*Power(t2,2) + 9*Power(t2,3))) + 
               t1*(6 - 125*t2 + 96*Power(t2,2) + 65*Power(t2,3) - 
                  65*Power(t2,4) + 4*Power(t2,5) + 
                  2*Power(s,2)*t2*
                   (7 + t2 - 14*Power(t2,2) + 3*Power(t2,3)) + 
                  s*(-23 + 45*t2 + 199*Power(t2,2) - 37*Power(t2,3) - 
                     23*Power(t2,4))) + 
               Power(t1,2)*(-9 + 129*t2 + 17*Power(t2,2) + 
                  53*Power(t2,3) - 25*Power(t2,4) + 
                  Power(s,2)*
                   (2 + 18*t2 + 22*Power(t2,2) - 16*Power(t2,3)) + 
                  s*(16 + 60*t2 + 131*Power(t2,2) + 45*Power(t2,3) + 
                     4*Power(t2,4)))) - 
            Power(s2,2)*(9 + Power(t1,5)*(-1 + t2) + (21 - 11*s)*t2 + 
               (-1 + 34*s)*Power(t2,2) + (-13 + 30*s)*Power(t2,3) + 
               (-29 + 11*s)*Power(t2,4) + Power(t2,5) + 
               Power(t1,4)*(5 - 2*s*(-2 + t2) + 4*t2) + 
               Power(t1,3)*(3 - 8*t2 - 14*Power(t2,2) - 3*Power(t2,3) + 
                  s*(8 - 9*t2 + 10*Power(t2,2))) + 
               Power(t1,2)*(4 + 10*t2 + 17*Power(t2,2) + 
                  5*Power(t2,3) + 2*Power(t2,4) - 
                  s*(1 + 16*t2 - 22*Power(t2,2) + 14*Power(t2,3))) + 
               t1*(-20 - 12*t2 + 192*Power(t2,2) + 13*Power(t2,3) - 
                  8*Power(t2,4) + 
                  s*(-5 + 31*t2 - 4*Power(t2,2) - 50*Power(t2,3) + 
                     6*Power(t2,4))))) + 
         s1*(-3 - Power(-1 + s,2)*Power(t1,6) - 5*t2 - 4*s*t2 - 
            11*Power(t2,2) + 2*s*Power(t2,2) + 9*Power(t2,3) + 
            19*s*Power(t2,3) - 16*Power(s,2)*Power(t2,3) - 
            Power(s,3)*Power(t2,3) + 17*Power(t2,4) - 12*s*Power(t2,4) - 
            13*Power(s,2)*Power(t2,4) - 6*Power(s,3)*Power(t2,4) - 
            2*Power(t2,5) - 4*s*Power(t2,5) + Power(s,2)*Power(t2,5) + 
            2*Power(s,3)*Power(t2,5) - 5*Power(t2,6) + 7*s*Power(t2,6) + 
            Power(t1,5)*(1 + Power(s,3) + 2*s*(-1 + t2) - 7*t2 + 
               Power(s,2)*t2 - 12*Power(t2,2)) + 
            Power(s2,3)*Power(t2,2)*
             (3 + Power(t1,3) + Power(t1,2)*(7 - 2*t2) - 9*t2 - 
               2*Power(t2,2) - 3*Power(t2,3) + 
               t1*(-11 + 7*t2 + 4*Power(t2,2))) + 
            Power(t1,4)*(-2 - 5*Power(s,3) + 8*t2 - 5*Power(t2,2) + 
               36*Power(t2,3) + s*(6 - 9*t2 - 34*Power(t2,2)) + 
               Power(s,2)*(3 + 8*t2 + 4*Power(t2,2))) - 
            Power(t1,3)*(-13 + t2 + 17*Power(t2,2) - 53*Power(t2,3) + 
               36*Power(t2,4) + Power(s,3)*(-2 + 3*t2) + 
               Power(s,2)*(9 + 45*Power(t2,2) + 4*Power(t2,3)) - 
               4*s*(-3 - 7*t2 + 12*Power(t2,2) + 15*Power(t2,3))) - 
            Power(t1,2)*(22 - t2 - 17*Power(t2,2) + 105*Power(t2,3) + 
               31*Power(t2,4) - 12*Power(t2,5) + 
               Power(s,3)*t2*(1 - 15*t2 + 2*Power(t2,2)) + 
               Power(s,2)*(-7 + 62*t2 + 5*Power(t2,2) - 
                  87*Power(t2,3) + 3*Power(t2,4)) + 
               s*(-4 - 71*t2 + 178*Power(t2,2) + 14*Power(t2,3) + 
                  13*Power(t2,4))) - 
            t1*(-14 - 4*t2 - 28*Power(t2,2) + 61*Power(t2,3) + 
               37*Power(t2,4) - 20*Power(t2,5) + 
               Power(s,3)*Power(t2,3)*(1 + t2) + 
               Power(s,2)*t2*
                (-9 + 13*t2 - 47*Power(t2,2) + 51*Power(t2,3) - 
                  3*Power(t2,4)) + 
               s*(-2 + 32*t2 + 75*Power(t2,2) + 62*Power(t2,3) - 
                  37*Power(t2,4) + 24*Power(t2,5))) + 
            Power(s2,2)*t2*(10 + Power(t1,4)*(-2 + t2) - 12*t2 - 
               15*Power(t2,2) - 54*Power(t2,3) - 7*Power(t2,4) - 
               2*Power(t1,3)*(4 + 6*t2 + Power(t2,2)) + 
               Power(t1,2)*(32 - 27*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               t1*(-32 + 50*t2 - 5*Power(t2,2) - 6*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s*(-6 + 2*Power(t1,4) - Power(t1,3)*(-3 + t2) + 24*t2 + 
                  17*Power(t2,2) + 2*Power(t2,3) + 8*Power(t2,4) + 
                  Power(t1,2)*(-16 - 23*t2 + 8*Power(t2,2)) - 
                  t1*(-17 + 18*t2 + 12*Power(t2,2) + 9*Power(t2,3)))) + 
            s2*(6 + Power(-1 + s,2)*Power(t1,5) + (-11 + 13*s)*t2 - 
               3*(5 - s + Power(s,2))*Power(t2,2) + 
               (-36 + 100*s + 23*Power(s,2))*Power(t2,3) + 
               (39 + 110*s + 6*Power(s,2))*Power(t2,4) + 
               (18 + 4*s - 7*Power(s,2))*Power(t2,5) - 9*Power(t2,6) + 
               Power(t1,4)*(1 + 16*t2 + 2*Power(s,2)*t2 + 
                  12*Power(t2,2) - s*(1 + 7*t2 + 3*Power(t2,2))) + 
               Power(t1,3)*(-15 + t2 - 22*Power(t2,2) - 
                  33*Power(t2,3) + 
                  Power(s,2)*(-1 - 22*t2 + Power(t2,2)) + 
                  s*(14 + 5*t2 + 39*Power(t2,2) + 7*Power(t2,3))) + 
               Power(t1,2)*(29 - 61*t2 + 17*Power(t2,2) + 
                  91*Power(t2,3) - 12*Power(t2,4) + 
                  Power(s,2)*t2*(37 - 28*t2 - 3*Power(t2,2)) + 
                  s*(-17 + 14*t2 + 93*Power(t2,2) - 140*Power(t2,3) + 
                     3*Power(t2,4))) + 
               t1*(-22 + 55*t2 + 8*Power(t2,2) + 114*Power(t2,3) + 
                  39*Power(t2,4) + 22*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-7 + 77*t2 - 4*Power(t2,2) + 6*Power(t2,3)) + 
                  s*(6 - 25*t2 + 18*Power(t2,2) + 80*Power(t2,3) + 
                     42*Power(t2,4) - 7*Power(t2,5))))) + 
         Power(s1,2)*(2 - t2 + 6*s*t2 - 14*Power(t2,2) - 
            5*s*Power(t2,2) + 2*Power(s,2)*Power(t2,2) - 12*Power(t2,3) + 
            22*s*Power(t2,3) + 2*Power(s,2)*Power(t2,3) + 
            2*Power(s,3)*Power(t2,3) + 8*Power(t2,4) + 24*s*Power(t2,4) - 
            32*Power(s,2)*Power(t2,4) + 2*Power(s,3)*Power(t2,4) + 
            3*Power(t2,5) - 11*s*Power(t2,5) - 9*Power(s,2)*Power(t2,5) + 
            Power(s,3)*Power(t2,5) - 6*Power(t2,6) + 2*s*Power(t2,6) + 
            Power(t1,6)*(4 - 3*s + Power(s,2) + 12*t2) + 
            Power(t1,5)*(-9 + 13*t2 - 36*Power(t2,2) - 
               2*Power(s,2)*(4 + t2) + s*(13 + 24*t2)) + 
            Power(t1,4)*(12 + 2*Power(s,3) - 4*t2 - 75*Power(t2,2) + 
               36*Power(t2,3) + 
               Power(s,2)*(14 + 27*t2 - 4*Power(t2,2)) - 
               s*(7 + 26*t2 + 43*Power(t2,2))) + 
            Power(s2,3)*t2*(-5 + Power(t1,3)*(-3 + t2) + 15*t2 + 
               7*Power(t2,2) + 4*Power(t2,3) - Power(t2,4) + 
               Power(t1,2)*(-5 + 4*t2 - 6*Power(t2,2)) + 
               t1*(13 - 14*t2 - 9*Power(t2,2) + 9*Power(t2,3))) + 
            Power(t1,3)*(Power(s,3)*(4 - 5*Power(t2,2)) + 
               s*(6 + 94*t2 - 7*Power(t2,2) - 16*Power(t2,3)) + 
               Power(s,2)*(4 + 39*t2 - 59*Power(t2,2) + 
                  12*Power(t2,3)) + 
               3*(-4 + 4*t2 + 21*Power(t2,2) + 23*Power(t2,3) - 
                  4*Power(t2,4))) + 
            Power(t1,2)*(6 - 56*t2 + 163*Power(t2,2) + 122*Power(t2,3) - 
               62*Power(t2,4) + 
               Power(s,3)*t2*(-6 - 4*t2 + 11*Power(t2,2)) + 
               Power(s,2)*(11 + 49*t2 - 124*Power(t2,2) + 
                  32*Power(t2,3) - 9*Power(t2,4)) + 
               s*(-28 + 136*t2 + 181*Power(t2,2) - 43*Power(t2,3) + 
                  62*Power(t2,4))) + 
            t1*(-3 + 24*t2 + Power(t2,2) - 27*Power(t2,3) - 
               22*Power(t2,4) - 7*Power(s,3)*Power(t2,4) + 
               31*Power(t2,5) + 
               Power(s,2)*t2*
                (5 + 35*t2 + 79*Power(t2,2) + 17*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s*(19 + 3*t2 - 21*Power(t2,2) - 25*Power(t2,3) + 
                  22*Power(t2,4) - 26*Power(t2,5))) + 
            Power(s2,2)*(-6 + (22 - 8*s)*t2 - Power(t1,5)*t2 + 
               (24 - 22*s)*Power(t2,2) + (63 + 22*s)*Power(t2,3) + 
               (7 - 2*s)*Power(t2,4) + (-10 + 3*s)*Power(t2,5) + 
               Power(t1,4)*(3 - s + 5*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(-1 + s + 11*t2 + 18*s*t2 - 2*Power(t2,2) - 
                  8*s*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(13 - (25 + 21*s)*t2 + 
                  (2 + 23*s)*Power(t2,2) + (19 - 26*s)*Power(t2,3) + 
                  7*Power(t2,4)) + 
               t1*(17 - (62 + 13*s)*t2 + (1 + 38*s)*Power(t2,2) + 
                  (111 - 10*s)*Power(t2,3) + (8 - 25*s)*Power(t2,4) + 
                  2*Power(t2,5))) - 
            s2*(-9 + (-1 + s)*Power(t1,6) + (-18 + 7*s)*t2 + 
               (-32 + s + 5*Power(s,2))*Power(t2,2) + 
               (44 + 51*s + 30*Power(s,2))*Power(t2,3) + 
               (42 - 51*s + 4*Power(s,2))*Power(t2,4) + 
               (-27 - 19*s + 3*Power(s,2))*Power(t2,5) + 2*Power(t2,6) + 
               Power(t1,5)*(9 + Power(s,2) + 10*t2 - 5*s*(1 + t2)) + 
               Power(t1,4)*(-6 - 5*Power(s,2) - 23*t2 - 25*Power(t2,2) + 
                  3*s*(2 + 12*t2 + Power(t2,2))) + 
               Power(t1,3)*(2 + 16*t2 + 61*Power(t2,2) - 
                  19*Power(t2,3) - 
                  3*Power(s,2)*(-3 + t2 + 4*Power(t2,2)) + 
                  s*(1 + 36*t2 - 85*Power(t2,2) + 13*Power(t2,3))) + 
               Power(t1,2)*(-26 + 3*t2 + 224*Power(t2,2) + 
                  9*Power(t2,3) + 43*Power(t2,4) + 
                  Power(s,2)*t2*(38 - 29*t2 + 31*Power(t2,2)) - 
                  s*(10 - 159*t2 + 71*Power(t2,2) + 14*Power(t2,3) + 
                     16*Power(t2,4))) + 
               t1*(31 + 12*t2 - 24*Power(t2,2) + 90*Power(t2,3) + 
                  38*Power(t2,4) - 26*Power(t2,5) + 
                  Power(s,2)*t2*
                   (17 + 59*t2 - 15*Power(t2,2) - 23*Power(t2,3)) + 
                  s*(7 - 83*t2 + 209*Power(t2,2) + 262*Power(t2,3) + 
                     31*Power(t2,4) + 4*Power(t2,5))))))*
       B1(1 - s1 - t1 + t2,s1,t2))/
     ((s - s2 + t1)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,3)*(-1 + t2)*
       (s - s1 + t2)) - (8*(4*Power(s1,12)*Power(t1,2) - 
         (-1 + t1 - t2)*Power(t2,4)*(-1 + s + t2)*
          (Power(s,2)*(-2 + t1 - t2) + s*(3 - 3*t1 + (-3 + s2)*t2) + 
            2*(t1 + t2 - Power(t2,2))) - 
         2*Power(s1,11)*(Power(s2,3) - s2*Power(t1,2) + 
            t1*(-8*Power(t1,2) + 4*t2 + t1*(5 + 3*s + 9*t2))) + 
         Power(s1,10)*(14*Power(t1,4) + 
            s2*t1*(17*Power(t1,2) + s*(6 + t1 - 6*t2) - 
               21*t1*(-1 + t2) - 4*t2) + 4*Power(t2,2) + 
            4*t1*t2*(5 + 3*s + 9*t2) + Power(s2,3)*(6 - 3*t1 + 10*t2) - 
            Power(t1,3)*(55 + 23*s + 59*t2) - 
            2*Power(s2,2)*(-3 + 4*Power(t1,2) + t1*(4 + s - t2) + 
               3*s*t2) + 2*Power(t1,2)*
             (2 + Power(s,2) - t2 + 16*Power(t2,2) + s*(7 + 13*t2))) + 
         Power(s1,3)*t2*(10 + 
            (49 - 24*Power(s,3) + s*(22 - 35*s2) + 45*s2 + 
               Power(s,2)*(-81 + 7*s2))*t2 + 2*(-1 + s)*Power(t1,5)*t2 + 
            (86 + 33*Power(s,3) + 70*s2 + 66*Power(s2,2) + 
               Power(s,2)*(-150 + 49*s2) - 
               s*(245 + 42*s2 + 48*Power(s2,2)))*Power(t2,2) + 
            (-395 + 83*Power(s,3) + 229*s2 + 43*Power(s2,2) + 
               26*Power(s2,3) + 12*Power(s,2)*(26 + 3*s2) - 
               s*(224 + 149*s2 + 158*Power(s2,2)))*Power(t2,3) + 
            (-146 + 5*Power(s,3) - 14*s2 - 37*Power(s2,2) + 
               45*Power(s2,3) + Power(s,2)*(401 + 22*s2) + 
               s*(655 - 510*s2 - 81*Power(s2,2)))*Power(t2,4) + 
            (516 - 7*Power(s,3) - 427*s2 + 83*Power(s2,2) - 
               2*Power(s2,3) + 15*Power(s,2)*(3 + s2) - 
               2*s*(-144 + 80*s2 + 3*Power(s2,2)))*Power(t2,5) + 
            (14 - 4*Power(s,2) + s2 - 7*Power(s2,2) + s*(-26 + 11*s2))*
             Power(t2,6) - 2*Power(t2,7) + 
            Power(t1,4)*(-1 + (-17 + 4*s2)*t2 + 
               (8 - 3*s2)*Power(t2,2) + 28*Power(t2,3) - 
               Power(s,2)*(4 + 3*t2) + 
               s*(5 - 4*(-5 + s2)*t2 + (5 + s2)*Power(t2,2))) + 
            Power(t1,3)*(-13 + (-41 + 12*s2)*t2 + 
               (100 + 4*s2 + 3*Power(s2,2))*Power(t2,2) + 
               (70 + 2*s2 + Power(s2,2))*Power(t2,3) - 
               134*Power(t2,4) + Power(s,3)*t2*(11 + 5*t2) - 
               Power(s,2)*(6 + (55 + 4*s2)*t2 + 
                  (13 + 4*s2)*Power(t2,2) - 8*Power(t2,3)) + 
               s*(44 + (54 + 7*s2)*t2 - 
                  3*(17 - s2 + Power(s2,2))*Power(t2,2) - 
                  3*(47 + 8*s2)*Power(t2,3))) + 
            Power(t1,2)*(31 + (-2 + 57*s2)*t2 + 
               (196 - 49*s2 + 16*Power(s2,2))*Power(t2,2) - 
               (28 - 63*s2 + 11*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,3) + 
               (-511 + 230*s2 - 3*Power(s2,2))*Power(t2,4) + 
               138*Power(t2,5) + 
               Power(s,3)*(6 - 59*t2 - 63*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(s,2)*(40 + 7*(25 + 4*s2)*t2 + 
                  (-63 + 26*s2)*Power(t2,2) - 
                  3*(54 + 5*s2)*Power(t2,3) + 4*Power(t2,4)) + 
               s*(-89 - (116 + 71*s2)*t2 + 
                  (-71 + 47*s2 - 17*Power(s2,2))*Power(t2,2) + 
                  (-386 + 172*s2 + 27*Power(s2,2))*Power(t2,3) + 
                  (1 + 18*s2)*Power(t2,4))) - 
            t1*(27 + (-33 + 118*s2)*t2 + 
               (50 + 102*s2 + 85*Power(s2,2))*Power(t2,2) + 
               (455 - 56*s2 + 58*Power(s2,2) + 18*Power(s2,3))*
                Power(t2,3) - 
               2*(-98 + 91*s2 - 60*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,4) + 
               (-533 + 258*s2 - 15*Power(s2,2))*Power(t2,5) + 
               30*Power(t2,6) + 
               Power(s,3)*(12 - 45*t2 - 86*Power(t2,2) - 
                  7*Power(t2,3) + 5*Power(t2,4)) + 
               Power(s,2)*(30 + 5*(29 + 2*s2)*t2 + 
                  (21 + 38*s2)*Power(t2,2) + 
                  (-233 + 26*s2)*Power(t2,3) - 
                  (93 + 4*s2)*Power(t2,4) + 8*Power(t2,5)) + 
               s*(-40 + (14 - 103*s2)*t2 + 
                  (220 - 239*s2 - 48*Power(s2,2))*Power(t2,2) - 
                  (123 + 193*s2 + 34*Power(s2,2))*Power(t2,3) + 
                  (-653 + 37*s2 + 6*Power(s2,2))*Power(t2,4) + 
                  3*(-53 + 2*s2)*Power(t2,5)))) + 
         s1*Power(t2,3)*(3 - 5*Power(t1,3) + 21*t2 + s2*t2 - 
            29*Power(t2,2) + 9*s2*Power(t2,2) + 
            4*Power(s2,2)*Power(t2,2) - 39*Power(t2,3) - 
            s2*Power(t2,3) + 5*Power(s2,2)*Power(t2,3) + 
            30*Power(t2,4) - 9*s2*Power(t2,4) + 
            Power(s2,2)*Power(t2,4) + 14*Power(t2,5) + 
            Power(t1,2)*(5 + (-11 + 9*s2)*t2 + (28 + s2)*Power(t2,2)) - 
            t1*(3 - 35*Power(t2,2) + 42*Power(t2,3) + 14*Power(t2,4) + 
               Power(s2,2)*Power(t2,2)*(4 + t2) + 
               2*s2*t2*(5 + 9*t2 - 4*Power(t2,2))) + 
            Power(s,3)*(8 + 3*Power(t1,3) + 24*t2 + 14*Power(t2,2) - 
               Power(t2,3) - Power(t1,2)*(5 + 3*t2) + 
               t1*(-6 - 13*t2 + Power(t2,2))) + 
            Power(s,2)*(-14 - 11*Power(t1,3) + (-18 + s2)*t2 + 
               (53 + 5*s2)*Power(t2,2) + (53 + 5*s2)*Power(t2,3) + 
               4*Power(t2,4) + 
               Power(t1,2)*(14 + (-3 + 7*s2)*t2 + 8*Power(t2,2)) - 
               t1*(-11 + 2*(-7 + 4*s2)*t2 + (43 + 12*s2)*Power(t2,2) + 
                  12*Power(t2,3))) + 
            s*(2 + 13*Power(t1,3) - (31 + s2)*t2 - 
               4*(10 + 5*s2 + Power(s2,2))*Power(t2,2) + 
               (49 - 26*s2 - 4*Power(s2,2))*Power(t2,3) - 
               5*(-8 + s2)*Power(t2,4) - 
               Power(t1,2)*(14 + (-19 + 16*s2)*t2 + 
                  (31 + s2)*Power(t2,2)) + 
               t1*(-1 - 2*Power(t2,2) + 4*Power(s2,2)*Power(t2,2) - 
                  9*Power(t2,3) + s2*t2*(17 + 37*t2 + 6*Power(t2,2))))) - 
         Power(s1,9)*(15*Power(t1,2) - 87*Power(t1,3) + 36*Power(t1,4) - 
            2*Power(t1,5) + 8*t1*t2 - 148*Power(t1,2)*t2 - 
            143*Power(t1,3)*t2 + 38*Power(t1,4)*t2 + 10*Power(t2,2) + 
            44*t1*Power(t2,2) - 97*Power(t1,2)*Power(t2,2) - 
            87*Power(t1,3)*Power(t2,2) + 18*Power(t2,3) + 
            64*t1*Power(t2,3) + 28*Power(t1,2)*Power(t2,3) + 
            Power(s2,3)*(2 + Power(t1,2) + 29*t2 + 20*Power(t2,2) - 
               2*t1*(1 + 8*t2)) + 
            Power(s,2)*t1*(-7*Power(t1,2) + 2*t2*(-1 + 3*t2) + 
               t1*(-10 + 11*t2)) + 
            s*(19*Power(t1,4) + 6*Power(t2,2) - 
               2*Power(t1,3)*(47 + 37*t2) + 
               Power(t1,2)*(-9 - 10*t2 + 49*Power(t2,2)) + 
               t1*(6 + 22*t2 + 52*Power(t2,2))) + 
            Power(s2,2)*(8*Power(t1,3) + 
               Power(t1,2)*(-34 + 3*s - 28*t2) + 
               t1*(-37 + 7*s*(-2 + t2) - 54*t2 + 8*Power(t2,2)) + 
               2*(9 + (11 - 13*s)*t2 + (1 - 12*s)*Power(t2,2))) + 
            s2*(6 - 15*Power(t1,4) - 2*Power(t2,2) + 
               6*Power(s,2)*Power(t2,2) - 6*s*t2*(1 + t2) + 
               Power(t1,3)*(42 + 8*s + 80*t2) + 
               Power(t1,2)*(95 + 102*t2 - 64*Power(t2,2) + 
                  s*(30 + 13*t2)) - 
               2*t1*(8 - 23*t2 + Power(s,2)*t2 + 21*Power(t2,2) + 
                  s*(-10 - 6*t2 + 11*Power(t2,2))))) + 
         Power(s1,2)*Power(t2,2)*
          (-9 - 53*t2 - 18*s2*t2 + 13*Power(t2,2) - 31*s2*Power(t2,2) - 
            26*Power(s2,2)*Power(t2,2) + 240*Power(t2,3) - 
            71*s2*Power(t2,3) - 16*Power(s2,2)*Power(t2,3) - 
            6*Power(s2,3)*Power(t2,3) - 62*Power(t2,4) + 
            43*s2*Power(t2,4) + 15*Power(s2,2)*Power(t2,4) - 
            8*Power(s2,3)*Power(t2,4) - 167*Power(t2,5) + 
            93*s2*Power(t2,5) - 6*Power(s2,2)*Power(t2,5) + 
            14*Power(t2,6) + Power(t1,4)*(1 + 4*t2) + 
            Power(t1,3)*(12 + (24 - 7*s2)*t2 + 
               2*(-19 + s2)*Power(t2,2) - 18*Power(t2,3)) + 
            Power(t1,2)*(-24 + (15 - 36*s2)*t2 + 
               (-127 + 15*s2 - 5*Power(s2,2))*Power(t2,2) + 
               (37 - 21*s2)*Power(t2,3) + 86*Power(t2,4)) + 
            t1*(20 + (-10 + 61*s2)*t2 + 
               (-18 + 54*s2 + 31*Power(s2,2))*Power(t2,2) + 
               (112 + s2 + 9*Power(s2,2) + 5*Power(s2,3))*Power(t2,3) + 
               2*(53 - 33*s2 + 2*Power(s2,2))*Power(t2,4) - 
               82*Power(t2,5)) - 
            Power(s,3)*(-6 + 27*t2 + 70*Power(t2,2) + 17*Power(t2,3) - 
               7*Power(t2,4) + Power(t1,3)*(6 + 7*t2) - 
               Power(t1,2)*(18 + 39*t2 + 5*Power(t2,2)) + 
               t1*(15 + 10*t2 - 4*Power(t2,2) + 5*Power(t2,3))) + 
            s*(-5 + (32 + 25*s2)*t2 + 
               (216 + 34*s2 + 24*Power(s2,2))*Power(t2,2) + 
               (-68 + 129*s2 + 54*Power(s2,2))*Power(t2,3) + 
               (-301 + 143*s2 + 16*Power(s2,2))*Power(t2,4) + 
               (-32 + 5*s2)*Power(t2,5) - Power(t1,4)*(2 + 5*t2) + 
               Power(t1,3)*(-35 + (-41 + 7*s2)*t2 + 
                  (30 + s2)*Power(t2,2)) + 
               Power(t1,2)*(53 + (23 + 49*s2)*t2 + 
                  (63 - 25*s2 + 5*Power(s2,2))*Power(t2,2) + 
                  (115 + 11*s2)*Power(t2,3)) - 
               t1*(11 + (-19 + 81*s2)*t2 + 
                  (6 + 107*s2 + 24*Power(s2,2))*Power(t2,2) + 
                  (6 + 109*s2 + 25*Power(s2,2))*Power(t2,3) + 
                  (108 + 17*s2)*Power(t2,4))) + 
            Power(s,2)*(15 + (86 - 9*s2)*t2 - (47 + 28*s2)*Power(t2,2) - 
               12*(23 + 2*s2)*Power(t2,3) - (104 + 15*s2)*Power(t2,4) + 
               Power(t2,5) + Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(29 + 23*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(-61 - (63 + 19*s2)*t2 + 
                  (55 - 3*s2)*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(16 + 21*Power(t2,2) + 45*Power(t2,3) + 
                  6*Power(t2,4) + s2*t2*(21 + 41*t2 + 18*Power(t2,2))))) \
+ Power(s1,8)*(2 + 4*Power(t2,2) + 8*s*Power(t2,2) + 
            2*Power(s,2)*Power(t2,2) + 30*Power(t2,3) + 
            26*s*Power(t2,3) + 6*Power(s,2)*Power(t2,3) - 
            2*Power(s,3)*Power(t2,3) + 32*Power(t2,4) - 
            2*Power(t1,5)*(-4 + s + 2*t2) + 
            Power(t1,4)*(91 + 5*Power(s,2) + 63*t2 + 39*Power(t2,2) + 
               s*(92 + 40*t2)) + 
            Power(s2,3)*(-6 + 10*Power(t1,2)*(-1 + t2) + 10*t2 + 
               54*Power(t2,2) + 20*Power(t2,3) + 
               t1*(3 - 13*t2 - 34*Power(t2,2))) - 
            Power(t1,3)*(21 + 215*t2 + 133*Power(t2,2) + 
               61*Power(t2,3) + Power(s,2)*(-19 + 21*t2) + 
               2*s*(31 + 114*t2 + 56*Power(t2,2))) + 
            Power(t1,2)*(95 - 218*t2 - 503*Power(t2,2) - 
               195*Power(t2,3) + 12*Power(t2,4) + 
               Power(s,3)*(6 + 4*t2) + 
               Power(s,2)*(-6 - 46*t2 + 6*Power(t2,2)) + 
               s*(18 - 389*t2 - 113*Power(t2,2) + 55*Power(t2,3))) + 
            t1*(-8 + 32*t2 - 131*Power(t2,2) + 
               4*Power(s,3)*Power(t2,2) - 17*Power(t2,3) + 
               56*Power(t2,4) + 
               2*Power(s,2)*t2*(-26 + 16*t2 + 7*Power(t2,2)) + 
               s*(22 - 8*t2 + 27*Power(t2,2) + 98*Power(t2,3))) - 
            Power(s2,2)*(-6 + Power(t1,3)*(-53 + s - 11*t2) + 
               (-59 + 38*s)*t2 + (-14 + 95*s)*Power(t2,2) + 
               4*(-2 + 9*s)*Power(t2,3) + 
               Power(t1,2)*(-19 + 6*s*(-7 + t2) + 117*t2 + 
                  25*Power(t2,2)) + 
               t1*(28 + 246*t2 + 132*Power(t2,2) - 12*Power(t2,3) + 
                  s*(22 + 19*t2 - 45*Power(t2,2)))) + 
            s2*(18 + (14 - 28*s)*t2 + 
               (25 - 37*s + 28*Power(s,2))*Power(t2,2) + 
               (-21 - 22*s + 18*Power(s,2))*Power(t2,3) - 
               Power(t1,4)*(67 + 9*s + 41*t2) + 
               Power(t1,3)*(-74 + 163*t2 + 137*Power(t2,2) + 
                  s*(-54 + 28*t2)) + 
               Power(t1,2)*(40 + 3*Power(s,2)*(-7 + t2) + 530*t2 + 
                  238*Power(t2,2) - 86*Power(t2,3) + 
                  s*(-11 + 215*t2 + 14*Power(t2,2))) + 
               t1*(-65 + 114*t2 + Power(s,2)*(8 - 21*t2)*t2 + 
                  169*Power(t2,2) - 128*Power(t2,3) + 
                  s*(-4 + 200*t2 + 22*Power(t2,2) - 30*Power(t2,3))))) + 
         Power(s1,7)*(-6 - 2*t2 + 2*s*t2 - 17*Power(t2,2) + 
            23*s*Power(t2,2) + 12*Power(s,2)*Power(t2,2) + 
            38*Power(t2,3) - 14*s*Power(t2,3) - 
            39*Power(s,2)*Power(t2,3) + 8*Power(s,3)*Power(t2,3) - 
            21*Power(t2,4) - 49*s*Power(t2,4) - 
            14*Power(s,2)*Power(t2,4) + 4*Power(s,3)*Power(t2,4) - 
            28*Power(t2,5) + 2*Power(t1,5)*
             (6 - 14*t2 + Power(t2,2) + s*(3 + 2*t2)) - 
            Power(t1,4)*(19 + 294*t2 + 18*Power(t2,2) + 
               15*Power(t2,3) + Power(s,2)*(3 + 4*t2) + 
               4*s*(25 + 45*t2 + 11*Power(t2,2))) + 
            Power(t1,3)*(32 - 194*t2 + 304*Power(t2,2) + 
               35*Power(t2,3) + 17*Power(t2,4) + 
               Power(s,3)*(1 + 5*t2) + 
               Power(s,2)*(-63 - 57*t2 + 26*Power(t2,2)) + 
               s*t2*(-182 + 299*t2 + 87*Power(t2,2))) - 
            Power(s2,3)*(-4 - 29*t2 + 16*Power(t2,2) + 46*Power(t2,3) + 
               10*Power(t2,4) + 
               Power(t1,2)*(1 - 32*t2 + 24*Power(t2,2)) - 
               2*t1*(-1 + 6*t2 + 10*Power(t2,2) + 18*Power(t2,3))) + 
            Power(t1,2)*(-76 - 345*t2 + 881*Power(t2,2) + 
               647*Power(t2,3) + 155*Power(t2,4) - 2*Power(t2,5) + 
               Power(s,3)*(-24 - 17*t2 + 4*Power(t2,2)) + 
               Power(s,2)*(-51 + 17*t2 + 170*Power(t2,2) - 
                  6*Power(t2,3)) + 
               s*(-60 + 326*t2 + 1101*Power(t2,2) + 187*Power(t2,3) - 
                  37*Power(t2,4))) - 
            t1*(-31 + 152*t2 - 167*Power(t2,2) - 521*Power(t2,3) - 
               129*Power(t2,4) + 24*Power(t2,5) + 
               Power(s,3)*t2*(12 + 20*t2 + 11*Power(t2,2)) + 
               2*Power(s,2)*t2*
                (-26 - 77*t2 + 27*Power(t2,2) + 5*Power(t2,3)) + 
               s*(10 + 169*t2 - 500*Power(t2,2) - 34*Power(t2,3) + 
                  110*Power(t2,4))) + 
            Power(s2,2)*(18 + 2*(-4 + 5*s)*t2 + 
               2*(1 + 65*s)*Power(t2,2) + (44 + 123*s)*Power(t2,3) + 
               12*(-1 + 2*s)*Power(t2,4) + 
               Power(t1,3)*(-6 - 157*t2 + 15*Power(t2,2) + 
                  s*(10 + 3*t2)) - 
               Power(t1,2)*(8 + 206*t2 - 169*Power(t2,2) + 
                  13*Power(t2,3) + s*(41 + 115*t2 - 36*Power(t2,2))) + 
               t1*(-17 + 97*t2 + 610*Power(t2,2) + 134*Power(t2,3) - 
                  8*Power(t2,4) + 
                  s*(10 + 3*t2 - 30*Power(t2,2) - 73*Power(t2,3)))) + 
            s2*(-6 + (-31 + 52*s)*t2 + 
               (-79 + 34*s - 56*Power(s,2))*Power(t2,2) + 
               (-84 + 63*s - 75*Power(s,2))*Power(t2,3) + 
               (64 + 30*s - 18*Power(s,2))*Power(t2,4) + 
               Power(t1,4)*(13 + 185*t2 + 37*Power(t2,2) + 
                  3*s*(2 + 5*t2)) + 
               Power(t1,3)*(4 + 458*t2 - 242*Power(t2,2) - 
                  102*Power(t2,3) - 4*Power(s,2)*(1 + t2) + 
                  s*(55 + 227*t2 - 56*Power(t2,2))) + 
               Power(t1,2)*(-5 - 32*t2 - 1247*Power(t2,2) - 
                  309*Power(t2,3) + 54*Power(t2,4) + 
                  Power(s,2)*(49 + 62*t2 - 18*Power(t2,2)) + 
                  s*(19 + 67*t2 - 438*Power(t2,2) + 26*Power(t2,3))) + 
               2*t1*(25 + 114*t2 - 391*Power(t2,2) - 130*Power(t2,3) + 
                  86*Power(t2,4) + 
                  Power(s,2)*t2*(-3 + 10*t2 + 24*Power(t2,2)) + 
                  s*(28 - 62*t2 - 383*Power(t2,2) - 12*Power(t2,3) + 
                     9*Power(t2,4))))) - 
         Power(s1,4)*(4 + (21 + 40*s2 - 2*s*(-5 + 6*s2))*t2 + 
            (86 - 36*Power(s,3) + 101*s2 + 82*Power(s2,2) + 
               Power(s,2)*(-149 + 27*s2) - 
               2*s*(16 + 19*s2 + 20*Power(s2,2)))*Power(t2,2) + 
            (-197 + 17*Power(s,3) + 267*s2 + 101*Power(s2,2) + 
               46*Power(s2,3) + Power(s,2)*(-23 + 32*s2) - 
               s*(373 + 2*s2 + 190*Power(s2,2)))*Power(t2,3) + 
            (-434 + 49*Power(s,3) + Power(s,2)*(416 - 44*s2) + 
               131*s2 - Power(s2,2) + 98*Power(s2,3) + 
               s*(482 - 626*s2 - 137*Power(s2,2)))*Power(t2,4) + 
            (641 + 2*Power(s,3) - 789*s2 + 284*Power(s2,2) - 
               4*Power(s2,3) + Power(s,2)*(151 + 3*s2) - 
               2*s*(-330 + 256*s2 + Power(s2,2)))*Power(t2,5) - 
            (-162 + 2*Power(s,3) + Power(s,2)*(13 - 6*s2) + 7*s2 + 
               7*Power(s2,2) - 2*Power(s2,3) + 
               s*(46 - 21*s2 + 6*Power(s2,2)))*Power(t2,6) + 
            (9 - 11*s + 13*s2)*Power(t2,7) + 
            2*Power(t1,5)*t2*
             (-2 - 2*t2 + 6*Power(t2,2) + s*(2 + 3*t2)) + 
            Power(t1,2)*(14 + 7*(5 + 6*s2)*t2 + 
               2*(16 - 19*s2 + 9*Power(s2,2))*Power(t2,2) - 
               (82 - 52*s2 + 41*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,3) + 
               2*(-452 + 386*s2 - 79*Power(s2,2) + Power(s2,3))*
                Power(t2,4) + 
               (586 - 218*s2 + 22*Power(s2,2))*Power(t2,5) - 
               18*Power(t2,6) + 
               Power(s,3)*(-6 + 7*t2 - 47*Power(t2,2) - 
                  23*Power(t2,3) + 12*Power(t2,4)) + 
               Power(s,2)*(-3 + (77 + 23*s2)*t2 + 
                  (11 + 90*s2)*Power(t2,2) + 
                  (-221 + 4*s2)*Power(t2,3) - 
                  (48 + 23*s2)*Power(t2,4) + 5*Power(t2,5)) - 
               s*(19 + (222 + 59*s2)*t2 + 
                  3*(33 + 9*s2 + 7*Power(s2,2))*Power(t2,2) + 
                  (358 - 505*s2 - 40*Power(s2,2))*Power(t2,3) - 
                  (112 + 224*s2 + 11*Power(s2,2))*Power(t2,4) + 
                  (-197 + 25*s2)*Power(t2,5))) + 
            Power(t1,4)*(Power(s,2)*
                (3 - 7*t2 + 6*Power(t2,2) - 3*Power(t2,3)) + 
               s*(9 + 6*t2 + (25 - 9*s2)*Power(t2,2) - 
                  2*(17 + 3*s2)*Power(t2,3)) + 
               t2*(-14 + 103*Power(t2,2) - 76*Power(t2,3) + 
                  s2*(8 + 7*t2 - 11*Power(t2,2)))) + 
            Power(t1,3)*(-6 + 3*(-14 + s2)*t2 + 
               (158 - 11*s2 + 9*Power(s2,2))*Power(t2,2) + 
               (106 + 60*s2 - 3*Power(s2,2))*Power(t2,3) + 
               2*(-310 + 103*s2 + Power(s2,2))*Power(t2,4) + 
               86*Power(t2,5) + 
               Power(s,3)*(3 - t2 + 3*Power(t2,2) + Power(t2,3)) + 
               s*(-2 + (73 + 19*s2)*t2 + 
                  (-142 + 79*s2 - 9*Power(s2,2))*Power(t2,2) + 
                  (-449 + 7*s2 + 10*Power(s2,2))*Power(t2,3) + 
                  (-132 + s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (-16 - 55*t2 - 53*Power(t2,2) + 10*Power(t2,3) - 
                  4*s2*(1 + 6*t2 + Power(t2,2)))) + 
            t1*(-12 + (14 - 93*s2)*t2 + 
               (68 - 139*s2 - 109*Power(s2,2))*Power(t2,2) - 
               (832 - 262*s2 + 147*Power(s2,2) + 26*Power(s2,3))*
                Power(t2,3) + 
               (-531 + 221*s2 - 323*Power(s2,2) + 45*Power(s2,3))*
                Power(t2,4) + 
               (1415 - 1135*s2 + 139*Power(s2,2) - 7*Power(s2,3))*
                Power(t2,5) - 
               2*(-23 + 13*s2 + 9*Power(s2,2))*Power(t2,6) - 
               4*Power(t2,7) + 
               Power(s,3)*t2*
                (-48 + 43*t2 + 109*Power(t2,2) - 22*Power(t2,3) - 
                  11*Power(t2,4)) + 
               s*(12 + (111 + 52*s2)*t2 + 
                  (-341 + 302*s2 + 40*Power(s2,2))*Power(t2,2) + 
                  (-263 + 209*s2 - 38*Power(s2,2))*Power(t2,3) - 
                  3*(-472 + 159*s2 + 27*Power(s2,2))*Power(t2,4) + 
                  (884 - 260*s2 - 3*Power(s2,2))*Power(t2,5) + 
                  (-26 + 30*s2)*Power(t2,6)) + 
               Power(s,2)*t2*(-154 - 305*t2 + 377*Power(t2,2) + 
                  630*Power(t2,3) + 68*Power(t2,4) - 12*Power(t2,5) + 
                  s2*(2 + 11*t2 + 12*Power(t2,2) + 44*Power(t2,3) + 
                     21*Power(t2,4))))) + 
         Power(s1,6)*(2 + t2 - 14*s*t2 + 77*Power(t2,2) + 
            49*s*Power(t2,2) + 2*Power(s,2)*Power(t2,2) + 
            6*Power(s,3)*Power(t2,2) - 36*Power(t2,3) - 
            241*s*Power(t2,3) - 19*Power(s,2)*Power(t2,3) - 
            10*Power(s,3)*Power(t2,3) - 175*Power(t2,4) + 
            5*s*Power(t2,4) + 66*Power(s,2)*Power(t2,4) - 
            15*Power(s,3)*Power(t2,4) - 21*Power(t2,5) + 
            55*s*Power(t2,5) + 10*Power(s,2)*Power(t2,5) - 
            2*Power(s,3)*Power(t2,5) + 12*Power(t2,6) - 
            2*Power(t1,5)*(s*(3 + 6*t2 + Power(t2,2)) - 
               2*(1 - 9*t2 + 8*Power(t2,2))) + 
            Power(t1,4)*(-1 - t2 + 404*Power(t2,2) - 25*Power(t2,3) + 
               Power(s,2)*(-6 - 11*t2 + 9*Power(t2,2)) + 
               s*(39 + 136*t2 + 169*Power(t2,2) + 17*Power(t2,3))) + 
            Power(t1,3)*(-4 - 116*t2 + 751*Power(t2,2) - 
               333*Power(t2,3) + 12*Power(t2,4) + 
               Power(s,3)*(-5 - 9*t2 + Power(t2,2)) + 
               Power(s,2)*(45 + 145*t2 + 64*Power(t2,2) - 
                  24*Power(t2,3)) + 
               s*(13 + 367*t2 + 459*Power(t2,2) - 194*Power(t2,3) - 
                  26*Power(t2,4))) + 
            Power(s2,3)*t2*(Power(t1,2)*(6 - 36*t2 + 22*Power(t2,2)) - 
               t1*(-9 + 62*t2 + 2*Power(t2,2) + 19*Power(t2,3)) + 
               2*(-10 - 36*t2 + 5*Power(t2,2) + 7*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(t1,2)*(-11 + 337*t2 + 623*Power(t2,2) - 
               1454*Power(t2,3) - 349*Power(t2,4) - 43*Power(t2,5) + 
               Power(s,3)*(36 + 27*t2 - 25*Power(t2,2) - 
                  6*Power(t2,3)) + 
               Power(s,2)*(73 + 139*t2 - 130*Power(t2,2) - 
                  170*Power(t2,3) + 17*Power(t2,4)) + 
               s*(49 + 39*t2 - 657*Power(t2,2) - 1394*Power(t2,3) - 
                  124*Power(t2,4) + 11*Power(t2,5))) + 
            t1*(-24 + 6*t2 + 677*Power(t2,2) - 961*Power(t2,3) - 
               743*Power(t2,4) - 127*Power(t2,5) + 4*Power(t2,6) + 
               Power(s,3)*t2*
                (48 + 30*t2 + 31*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s,2)*t2*
                (138 - 305*t2 - 369*Power(t2,2) + 38*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s*(-34 + 291*t2 - 151*Power(t2,2) - 1446*Power(t2,3) - 
                  56*Power(t2,4) + 74*Power(t2,5))) + 
            Power(s2,2)*(-12 + (-79 + 20*s)*t2 + 
               (-26 + 3*s)*Power(t2,2) - (245 + 156*s)*Power(t2,3) - 
               (79 + 59*s)*Power(t2,4) + (8 - 6*s)*Power(t2,5) + 
               t1*(16 + 11*(8 + s)*t2 + 3*(-5 + 43*s)*Power(t2,2) + 
                  (-739 + 64*s)*Power(t2,3) + 
                  (-30 + 49*s)*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,3)*(-3 + 17*t2 + 153*Power(t2,2) - 
                  31*Power(t2,3) - 3*s*(-1 + 10*t2 + Power(t2,2))) + 
               Power(t1,2)*(-1 + 35*t2 + 507*Power(t2,2) - 
                  145*Power(t2,3) + 29*Power(t2,4) + 
                  s*(2 + 96*t2 + 93*Power(t2,2) - 42*Power(t2,3)))) - 
            s2*(18 + 2*(7 + 16*s)*t2 + 
               (58 + 81*s - 39*Power(s,2))*Power(t2,2) - 
               (354 + 223*s + 139*Power(s,2))*Power(t2,3) - 
               2*(51 - 19*s + 30*Power(s,2))*Power(t2,4) + 
               (86 + 18*s - 6*Power(s,2))*Power(t2,5) + 
               Power(t1,4)*(13 + 15*t2 + 169*Power(t2,2) + 
                  11*Power(t2,3) + s*(13 - 18*t2 + 11*Power(t2,2))) - 
               Power(t1,3)*(25 - 64*t2 - 900*Power(t2,2) + 
                  163*Power(t2,3) + 28*Power(t2,4) + 
                  4*Power(s,2)*(1 + 6*t2 + Power(t2,2)) + 
                  s*(-22 - 213*t2 - 277*Power(t2,2) + 60*Power(t2,3))) + 
               Power(t1,2)*(11 - 21*t2 + 368*Power(t2,2) - 
                  1502*Power(t2,3) - 207*Power(t2,4) + 13*Power(t2,5) + 
                  Power(s,2)*
                   (35 + 144*t2 + 34*Power(t2,2) - 27*Power(t2,3)) + 
                  s*(-42 + 166*t2 + 289*Power(t2,2) - 376*Power(t2,3) + 
                     47*Power(t2,4))) + 
               t1*(-25 + 215*t2 + 316*Power(t2,2) - 1777*Power(t2,3) - 
                  223*Power(t2,4) + 108*Power(t2,5) + 
                  Power(s,2)*t2*
                   (22 + 20*t2 + 85*Power(t2,2) + 41*Power(t2,3)) + 
                  2*s*(25 + 95*t2 - 298*Power(t2,2) - 630*Power(t2,3) + 
                     15*Power(t2,4) + 2*Power(t2,5))))) - 
         Power(s1,5)*(-6 - 12*t2 - 22*s*t2 + 123*s*Power(t2,2) + 
            99*Power(s,2)*Power(t2,2) + 24*Power(s,3)*Power(t2,2) + 
            331*Power(t2,3) - 55*s*Power(t2,3) - 
            107*Power(s,2)*Power(t2,3) - 7*Power(s,3)*Power(t2,3) - 
            331*Power(t2,4) - 616*s*Power(t2,4) - 
            112*Power(s,2)*Power(t2,4) - 25*Power(s,3)*Power(t2,4) - 
            267*Power(t2,5) + 19*s*Power(t2,5) + 
            38*Power(s,2)*Power(t2,5) - 5*Power(s,3)*Power(t2,5) - 
            33*Power(t2,6) + 37*s*Power(t2,6) + 
            2*Power(s,2)*Power(t2,6) + 2*Power(t2,7) - 
            2*Power(t1,5)*(-1 + s - 4*t2 + 6*s*t2 + 18*Power(t2,2) + 
               3*s*Power(t2,2) - 6*Power(t2,3)) + 
            Power(s2,3)*Power(t2,2)*
             (-42 - 110*t2 + 4*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,2)*(12 - 16*t2 + 7*Power(t2,2)) + 
               t1*(20 - 84*t2 + 14*Power(t2,2) - 4*Power(t2,3))) + 
            Power(t1,4)*(1 + 7*t2 - 95*Power(t2,2) + 277*Power(t2,3) - 
               16*Power(t2,4) + 
               Power(s,2)*(-7 - 12*t2 + Power(t2,2) + 4*Power(t2,3)) + 
               s*(-9 + 31*t2 + 82*Power(t2,2) + 63*Power(t2,3))) + 
            Power(t1,3)*(25 - 100*t2 - 138*Power(t2,2) + 
               1022*Power(t2,3) - 243*Power(t2,4) + 2*Power(t2,5) + 
               Power(s,3)*(-7 - 3*t2 + 3*Power(t2,2)) + 
               s*(12 + 86*t2 + 627*Power(t2,2) + 419*Power(t2,3) - 
                  53*Power(t2,4)) + 
               Power(s,2)*(8 + 77*t2 + 119*Power(t2,2) + 
                  28*Power(t2,3) - 12*Power(t2,4))) + 
            Power(t1,2)*(-35 + 54*t2 + 352*Power(t2,2) + 
               852*Power(t2,3) - 1254*Power(t2,4) - 35*Power(t2,5) + 
               2*Power(t2,6) + 
               Power(s,3)*(24 + 13*t2 - 15*Power(t2,2) - 
                  12*Power(t2,3) - 4*Power(t2,4)) + 
               s*(43 + 181*t2 + 138*Power(t2,2) - 532*Power(t2,3) - 
                  834*Power(t2,4) - 26*Power(t2,5)) + 
               Power(s,2)*(31 + 62*t2 + 149*Power(t2,2) - 
                  67*Power(t2,3) - 41*Power(t2,4) + 8*Power(t2,5))) + 
            t1*(11 - 127*t2 + 486*Power(t2,2) + 944*Power(t2,3) - 
               1726*Power(t2,4) - 437*Power(t2,5) - 35*Power(t2,6) + 
               Power(s,3)*t2*
                (72 + t2 - 19*Power(t2,2) + 34*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s,2)*t2*
                (264 + 51*t2 - 796*Power(t2,2) - 343*Power(t2,3) + 
                  30*Power(t2,4)) + 
               s*(-40 + 33*t2 + 604*Power(t2,2) - 1097*Power(t2,3) - 
                  1709*Power(t2,4) + 9*Power(t2,5) + 22*Power(t2,6))) + 
            Power(s2,2)*t2*(-50 - 130*t2 - 48*Power(t2,2) - 
               409*Power(t2,3) - 37*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,3)*(-9 + 15*t2 + 47*Power(t2,2) - 
                  13*Power(t2,3)) + 
               Power(t1,2)*(-8 + 57*t2 + 475*Power(t2,2) - 
                  81*Power(t2,3) + 11*Power(t2,4)) + 
               t1*(67 + 168*t2 + 261*Power(t2,2) - 462*Power(t2,3) + 
                  36*Power(t2,4)) + 
               s*(12 + 102*t2 + 85*Power(t2,2) - 68*Power(t2,3) + 
                  Power(t2,4) - Power(t1,3)*(-9 + 30*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (11 + 42*t2 + 9*Power(t2,2) - 15*Power(t2,3)) + 
                  t1*(-12 + 68*t2 + 185*Power(t2,2) + 32*Power(t2,3) + 
                     12*Power(t2,4)))) - 
            s2*(12 + (71 - 10*s)*t2 + 
               5*(26 + 15*s + 3*Power(s,2))*Power(t2,2) + 
               (182 - 225*s - 90*Power(s,2))*Power(t2,3) - 
               (738 + 586*s + 95*Power(s,2))*Power(t2,4) + 
               (-51 + 16*s - 7*Power(s,2))*Power(t2,5) + 
               (54 + 4*s)*Power(t2,6) + 
               Power(t1,4)*(4 + 23*t2 - 9*Power(t2,2) + 51*Power(t2,3) + 
                  s*(4 + 3*t2 - 30*Power(t2,2) + 5*Power(t2,3))) + 
               Power(t1,3)*(-2 - 42*t2 + 118*Power(t2,2) + 
                  722*Power(t2,3) - 42*Power(t2,4) - 
                  24*Power(s,2)*t2*(1 + t2) + 
                  s*(5 + 97*t2 + 189*Power(t2,2) + 105*Power(t2,3) - 
                     24*Power(t2,4))) + 
               Power(t1,2)*(12 + 6*t2 - 6*Power(t2,2) + 
                  902*Power(t2,3) - 908*Power(t2,4) - 55*Power(t2,5) + 
                  Power(s,2)*
                   (7 + 102*t2 + 114*Power(t2,2) - 30*Power(t2,3) - 
                     12*Power(t2,4)) + 
                  s*(-21 - 90*t2 + 469*Power(t2,2) + 439*Power(t2,3) - 
                     148*Power(t2,4) + 19*Power(t2,5))) + 
               t1*(-26 - 98*t2 + 362*Power(t2,2) + 258*Power(t2,3) - 
                  2002*Power(t2,4) - 112*Power(t2,5) + 26*Power(t2,6) + 
                  2*Power(s,2)*t2*
                   (10 + 21*t2 + 33*Power(t2,2) + 39*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  s*(12 + 184*t2 + 253*Power(t2,2) - 891*Power(t2,3) - 
                     928*Power(t2,4) + 68*Power(t2,5))))))*R1q(s1))/
     (Power(-1 + s1,3)*s1*(s - s2 + t1)*Power(s1 - t2,3)*
       (-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)*(s - s1 + t2)) + 
    (8*(2*Power(s1,11)*Power(t1,2)*
          (-1 + (2 - s2 + t1)*t2 + 3*Power(t2,2)) + 
         2*Power(s1,10)*(6*Power(t1,4)*t2 - Power(s2,3)*Power(t2,2) + 
            2*t1*t2*(1 + (-2 + s2)*t2 - 3*Power(t2,2)) + 
            Power(t1,3)*(-6 - (-15 + s + 6*s2)*t2 + 10*Power(t2,2)) - 
            Power(t1,2)*(1 + (-9 - 5*s + 2*s2)*t2 + 
               2*(8 + 5*s - 6*s2)*Power(t2,2) + 14*Power(t2,3))) + 
         Power(s1,9)*(30*Power(t1,5)*t2 + 
            2*Power(t1,4)*(-15 + (46 - 6*s - 15*s2)*t2 + 
               5*Power(t2,2)) + 
            2*t1*t2*(2 - (18 - 4*s2 + 4*Power(s2,2) + 5*Power(s2,3) + 
                  s*(10 - 3*s2 + Power(s2,2)))*t2 + 
               (29 + s*(20 - 3*s2) - 24*s2 + Power(s2,2))*Power(t2,2) + 
               28*Power(t2,3)) - 
            Power(t1,3)*(12 + (-97 - 56*s + 24*s2)*t2 + 
               2*(128 + 45*s - 62*s2)*Power(t2,2) + 83*Power(t2,3)) + 
            Power(t1,2)*(4 + (56 + 6*s - 3*s2)*t2 + 
               2*(-93 - 23*s + 6*Power(s,2) + 43*s2 - 10*s*s2 + 
                  Power(s2,2))*Power(t2,2) + 
               (-8 + 106*s - 107*s2)*Power(t2,3) + 52*Power(t2,4)) + 
            2*Power(t2,2)*(-1 + 2*t2 - s2*t2 + 3*Power(t2,2) + 
               Power(s2,3)*(1 + 5*t2) + Power(s2,2)*(3 - 3*s*t2))) + 
         Power(s1,8)*(40*Power(t1,6)*t2 - 
            2*Power(t1,5)*(20 + 5*(-15 + 3*s + 4*s2)*t2 + 
               22*Power(t2,2)) - 
            Power(t1,4)*(30 + (-221 - 130*s + 60*s2)*t2 + 
               (743 + 158*s - 266*s2)*Power(t2,2) + 62*Power(t2,3)) + 
            t1*t2*(-8 + (-76 + 22*s2 + 42*Power(s2,2) + Power(s2,3) + 
                  2*s*(-9 - 4*s2 + 5*Power(s2,2)))*t2 + 
               (282 + 2*Power(s,2)*(-9 + s2) - 140*s2 + 
                  38*Power(s2,2) + 51*Power(s2,3) + 
                  s*(104 + 18*s2 - 28*Power(s2,2)))*Power(t2,2) - 
               2*(22 + 3*Power(s,2) + s*(106 - 11*s2) - 107*s2 + 
                  4*Power(s2,2))*Power(t2,3) - 104*Power(t2,4)) + 
            Power(t1,3)*(16 + (195 + 35*s - 13*s2)*t2 + 
               (-685 + 56*Power(s,2) + 293*s2 + 8*Power(s2,2) - 
                  12*s*(11 + 8*s2))*Power(t2,2) + 
               (447 + 381*s - 438*s2)*Power(t2,3) + 107*Power(t2,4)) - 
            Power(t1,2)*(-2 + (-27 + 14*s - 8*s2)*t2 + 
               (421 + 14*s2 + Power(s2,2) + 20*Power(s2,3) - 
                  2*Power(s,2)*(19 + s2) + 
                  s*(127 + 26*s2 + 12*Power(s2,2)))*Power(t2,2) + 
               (-1023 + 92*Power(s,2) + 550*s2 + 31*Power(s2,2) - 
                  2*s*(200 + 53*s2))*Power(t2,3) + 
               3*(-77 + 75*s - 76*s2)*Power(t2,4) + 48*Power(t2,5)) - 
            2*Power(t2,2)*(1 - (9 + 5*s)*t2 + 2*(7 + 5*s)*Power(t2,2) + 
               14*Power(t2,3) + 
               2*Power(s2,3)*(-1 - t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(3 + (11 - 7*s)*t2 + 
                  (1 - 12*s)*Power(t2,2)) + 
               s2*(3 + (2 - 3*s)*t2 + 
                  3*(-4 - s + Power(s,2))*Power(t2,2)))) + 
         Power(s1,7)*(30*Power(t1,7)*t2 - 
            2*Power(t1,6)*(15 + 5*(-14 + 4*s + 3*s2)*t2 + 
               43*Power(t2,2)) + 
            Power(t1,5)*(-40 + 2*(137 + 80*s - 40*s2)*t2 + 
               (-1071 - 128*s + 304*s2)*Power(t2,2) + 47*Power(t2,3)) + 
            Power(t1,4)*(24 + (309 + 83*s - 22*s2)*t2 + 
               (-1178 + 110*Power(s,2) + 435*s2 + 12*Power(s2,2) - 
                  3*s*(25 + 64*s2))*Power(t2,2) + 
               (1561 + 512*s - 747*s2)*Power(t2,3) + 14*Power(t2,4)) + 
            Power(t1,3)*(10 + (80 - 46*s + 24*s2)*t2 + 
               (-1159 + 2*Power(s,3) + 36*s2 + 49*Power(s2,2) - 
                  20*Power(s2,3) + 2*Power(s,2)*(80 + 3*s2) - 
                  14*s*(37 + 9*s2 + 2*Power(s2,2)))*Power(t2,2) + 
               (3280 - 336*Power(s,2) - 1518*s2 - 109*Power(s2,2) + 
                  5*s*(183 + 86*s2))*Power(t2,3) + 
               (-252 - 643*s + 732*s2)*Power(t2,4) - 47*Power(t2,5)) + 
            t1*t2*(-4 + (-26 + s*(38 - 26*s2) - 70*s2 + 
                  7*Power(s2,2) + 14*Power(s2,3))*t2 + 
               (553 + 8*Power(s,2)*(-12 + s2) + 16*s2 - 
                  175*Power(s2,2) + 34*Power(s2,3) + 
                  s*(108 + 178*s2 + 27*Power(s2,2)))*Power(t2,2) + 
               (4*Power(s,3) + Power(s,2)*(182 - 42*s2) + 
                  s*(-552 - 154*s2 + 129*Power(s2,2)) - 
                  2*(663 - 372*s2 + 18*Power(s2,2) + 52*Power(s2,3)))*
                Power(t2,3) + 
               (14*Power(s,2) - 30*s*(-15 + s2) + 
                  3*(-71 - 152*s2 + 4*Power(s2,2)))*Power(t2,4) + 
               96*Power(t2,5)) + 
            Power(t1,2)*(-2 + (-90 - 14*s + 7*s2)*t2 + 
               (-273 + 14*Power(s,2)*(-3 + s2) - 58*s2 + 
                  71*Power(s2,2) - 9*Power(s2,3) + 
                  s*(62 + 66*s2 + 23*Power(s2,2)))*Power(t2,2) + 
               (1889 + 24*Power(s,3) - 212*s2 + 18*Power(s2,2) + 
                  97*Power(s2,3) - 2*Power(s,2)*(119 + 18*s2) + 
                  s*(41 + 420*s2 - 15*Power(s2,2)))*Power(t2,3) + 
               (-2045 + 188*Power(s,2) + 1631*s2 + 111*Power(s2,2) - 
                  2*s*(682 + 129*s2))*Power(t2,4) + 
               (-383 + 243*s - 252*s2)*Power(t2,5) + 22*Power(t2,6)) + 
            Power(t2,2)*(Power(s2,3)*t2*(-29 - 51*t2 + 20*Power(t2,2)) - 
               4*Power(s2,2)*
                (3 + (6 + s)*t2 + (-5 + 2*s)*Power(t2,2) + 
                  (-2 + 9*s)*Power(t2,3)) + 
               s2*(6 + (11 - 16*s)*t2 + 
                  2*(33 - 23*s + 8*Power(s,2))*Power(t2,2) + 
                  (-107 - 22*s + 18*Power(s,2))*Power(t2,3)) + 
               2*(3 + (16 + 3*s)*t2 + 
                  (-63 - 28*s + 6*Power(s,2))*Power(t2,2) + 
                  (16 + 53*s + 3*Power(s,2) - Power(s,3))*Power(t2,3) + 
                  26*Power(t2,4)))) + 
         s1*Power(t2,3)*(-2*(3 - 4*s + Power(s,2))*Power(t1,8)*t2 + 
            Power(t1,7)*(1 - Power(s,3)*(-5 + t2) + (-33 + 10*s2)*t2 - 
               8*(-7 + s2)*Power(t2,2) + 
               Power(s,2)*(-27 + (7 + 2*s2)*t2 + 8*Power(t2,2)) + 
               s*(9 + (39 - 20*s2)*t2 - 2*(13 + 2*s2)*Power(t2,2))) + 
            Power(t1,6)*(23 + (-17 + 33*s2)*t2 + 
               (163 - 25*s2 - 4*Power(s2,2))*Power(t2,2) + 
               (-109 + 26*s2 - 2*Power(s2,2))*Power(t2,3) + 
               Power(s,3)*(-21 - 32*t2 + 7*Power(t2,2)) + 
               Power(s,2)*(42 + (82 + 13*s2)*t2 - 
                  (18 + 11*s2)*Power(t2,2) - 10*Power(t2,3)) + 
               2*s*(-13 - (17 + 26*s2)*t2 + 
                  2*(-35 + 12*s2 + Power(s2,2))*Power(t2,2) + 
                  (7 + 6*s2)*Power(t2,3))) + 
            Power(t1,5)*(-41 - 2*(28 + 41*s2)*t2 + 
               (318 - 106*s2 - 39*Power(s2,2))*Power(t2,2) + 
               (-641 + 130*s2 + 9*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) + (82 + 8*s2 + 4*Power(s2,2))*Power(t2,4) + 
               Power(s,3)*(22 + 22*t2 + 45*Power(t2,2) - 
                  11*Power(t2,3)) + 
               Power(s,2)*(-9 + (159 - 44*s2)*t2 - 
                  (87 + 98*s2)*Power(t2,2) + 
                  (-67 + 24*s2)*Power(t2,3) + 4*Power(t2,4)) + 
               s*(31 + (59 + 128*s2)*t2 + 
                  (-287 + 294*s2 + 37*Power(s2,2))*Power(t2,2) + 
                  (323 + 2*s2 - 15*Power(s2,2))*Power(t2,3) - 
                  2*(17 + 4*s2)*Power(t2,4))) + 
            Power(t1,3)*t2*(269 + 75*t2 - 769*Power(t2,2) + 
               2*Power(s2,3)*(-21 + t2)*Power(t2,2) + 
               1975*Power(t2,3) - 678*Power(t2,4) + 56*Power(t2,5) + 
               Power(s2,2)*t2*
                (-15 + 205*t2 - 140*Power(t2,2) + 38*Power(t2,3)) + 
               2*Power(s,3)*(-112 - 211*t2 - 94*Power(t2,2) + 
                  41*Power(t2,3)) + 
               Power(s,2)*(72 + (-323 + 308*s2)*t2 + 
                  7*(-37 + 58*s2)*Power(t2,2) - 
                  2*(44 + 81*s2)*Power(t2,3) + 38*Power(t2,4)) + 
               2*s2*(-9 + 205*t2 - 118*Power(t2,2) - 239*Power(t2,3) + 
                  109*Power(t2,4)) - 
               2*s*(78 + (46 + 455*s2 - 9*Power(s2,2))*t2 + 
                  (-289 + 309*s2 + 72*Power(s2,2))*Power(t2,2) + 
                  (666 - 230*s2 - 39*Power(s2,2))*Power(t2,3) + 
                  (-13 + 38*s2)*Power(t2,4))) - 
            4*t1*Power(t2,2)*
             (90 - 113*t2 + 117*Power(t2,2) + 433*Power(t2,3) - 
               373*Power(t2,4) + 30*Power(t2,5) + 
               3*Power(s2,3)*t2*(-3 - 10*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-54 + 125*t2 - 43*Power(t2,2) + 28*Power(t2,3)) + 
               Power(s,3)*(-136 - 245*t2 - 46*Power(t2,2) + 
                  35*Power(t2,3)) + 
               Power(s,2)*(36 + (-146 + 105*s2)*t2 + 
                  (-403 + 102*s2)*Power(t2,2) - 
                  5*(31 + 11*s2)*Power(t2,3) + 28*Power(t2,4)) + 
               s2*(-45 + 54*t2 - 336*Power(t2,2) + 72*Power(t2,3) + 
                  191*Power(t2,4)) - 
               s*(5 + (-306 + 332*s2 - 45*Power(s2,2))*t2 + 
                  2*(66 - 35*s2 + 21*Power(s2,2))*Power(t2,2) + 
                  (628 - 270*s2 - 5*Power(s2,2))*Power(t2,3) + 
                  (53 + 56*s2)*Power(t2,4))) - 
            4*Power(t2,2)*(-32 + (-86 - 69*s2 + 36*Power(s2,2))*t2 + 
               (350 - 115*s2 + 28*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,2) + 
               (104 + 211*s2 + 2*Power(s2,2) - 11*Power(s2,3))*
                Power(t2,3) - 
               (458 - 253*s2 + 2*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,4) - 2*(-5 + 8*s2)*Power(t2,5) + 
               Power(s,3)*t2*
                (-48 - 17*t2 + 29*Power(t2,2) + 4*Power(t2,3)) - 
               Power(s,2)*(-24 + 8*(9 + 4*s2)*t2 + 
                  (360 + 67*s2)*Power(t2,2) + 
                  (222 + 97*s2)*Power(t2,3) + 2*(-7 + 6*s2)*Power(t2,4)\
) + s*(56 + (263 + 80*s2)*t2 + 
                  (-59 + 276*s2 + 93*Power(s2,2))*Power(t2,2) + 
                  (-565 + 368*s2 + 79*Power(s2,2))*Power(t2,3) + 
                  3*(-37 - 4*s2 + 4*Power(s2,2))*Power(t2,4) + 
                  16*Power(t2,5))) + 
            Power(t1,4)*(17 + (-49 + 57*s2)*t2 + 
               (173 - 107*s2 + 76*Power(s2,2))*Power(t2,2) + 
               (-1008 + 178*s2 + 76*Power(s2,2) + 13*Power(s2,3))*
                Power(t2,3) - 
               (-1046 + 336*s2 + 46*Power(s2,2) + 5*Power(s2,3))*
                Power(t2,4) - (63 + 38*s2)*Power(t2,5) + 
               Power(s,3)*t2*
                (212 + 212*t2 - 93*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s,2)*(-6 + (-279 + 8*s2)*t2 + 
                  7*(-42 + s2)*Power(t2,2) + 
                  (461 + 194*s2)*Power(t2,3) - 
                  5*(-8 + 3*s2)*Power(t2,4)) + 
               s*(-14 - 4*(9 + 14*s2)*t2 + 
                  (26 + 32*s2 - 39*Power(s2,2))*Power(t2,2) + 
                  (783 - 626*s2 - 114*Power(s2,2))*Power(t2,3) + 
                  (-77 + 6*s2 + 15*Power(s2,2))*Power(t2,4) + 
                  38*Power(t2,5))) - 
            Power(t1,2)*t2*(118 + (289 + 384*s2 + 18*Power(s2,2))*t2 + 
               (-66 + 81*s2 + 275*Power(s2,2) + 14*Power(s2,3))*
                Power(t2,2) + 
               (-2246 - 335*s2 + 22*Power(s2,2) + 17*Power(s2,3))*
                Power(t2,3) - 
               (-2792 + 1211*s2 + 195*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,4) - 3*(61 + 13*s2)*Power(t2,5) + 
               Power(s,3)*t2*
                (560 + 344*t2 - 239*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s,2)*(-48 + (-318 + 64*s2)*t2 + 
                  (455 + 342*s2)*Power(t2,2) + 
                  (1394 + 551*s2)*Power(t2,3) + (5 - 21*s2)*Power(t2,4)) \
+ s*(-112 - 2*(449 + 206*s2)*t2 - 
                  (219 + 938*s2 + 300*Power(s2,2))*Power(t2,2) + 
                  (2441 - 1632*s2 - 329*Power(s2,2))*Power(t2,3) + 
                  (77 + 190*s2 + 21*Power(s2,2))*Power(t2,4) + 
                  39*Power(t2,5)))) + 
         Power(t2,4)*(-2*(-1 + s)*Power(t1,7)*
             (1 + Power(s,2) + 2*t2 - s*(2 + t2)) + 
            Power(t1,6)*(-12 + (13 - 12*s2)*t2 + 
               (-29 + 2*s2)*Power(t2,2) + Power(s,3)*(3 + 5*t2) - 
               Power(s,2)*(9 + (15 + 8*s2)*t2 + 8*Power(t2,2)) + 
               s*(24 + 3*(-3 + 8*s2)*t2 + (19 + 4*s2)*Power(t2,2))) + 
            Power(t1,5)*(16 + (-13 + 22*s2)*t2 + 
               (-36 + 9*s2 + 10*Power(s2,2))*Power(t2,2) + 
               (51 - 9*s2 + 2*Power(s2,2))*Power(t2,3) + 
               Power(s,3)*(8 + 15*t2 - 7*Power(t2,2)) + 
               Power(s,2)*(3 + (-26 + 17*s2)*t2 + 
                  (21 + 17*s2)*Power(t2,2) + 10*Power(t2,3)) - 
               s*(32 + 4*(-5 + 9*s2)*t2 + 
                  (-43 + 40*s2 + 10*Power(s2,2))*Power(t2,2) + 
                  (11 + 12*s2)*Power(t2,3))) + 
            4*t1*Power(t2,2)*
             (-(Power(s2,3)*t2*(3 + 5*t2)) + 
               Power(s,3)*(32 + 5*t2 - 21*Power(t2,2)) - 
               2*Power(s2,2)*t2*(10 - 5*t2 + 7*Power(t2,2)) + 
               Power(s,2)*(12 + (48 + 47*s2)*t2 + 
                  49*(2 + s2)*Power(t2,2) + 18*Power(t2,3)) - 
               s2*(15 - 3*t2 + 59*Power(t2,2) + 45*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s*(-107 + (-13 - 84*s2 + 15*Power(s2,2))*t2 + 
                  (93 - 80*s2 - 23*Power(s2,2))*Power(t2,2) - 
                  (25 + 4*s2)*Power(t2,3) + 4*Power(t2,4)) - 
               2*(-21 + 19*t2 + 48*Power(t2,2) - 71*Power(t2,3) + 
                  5*Power(t2,4))) - 
            Power(t1,3)*t2*(115 - 34*t2 - 170*Power(t2,2) + 
               322*Power(t2,3) - 25*Power(t2,4) - 
               Power(s2,3)*Power(t2,2)*(5 + 3*t2) + 
               Power(s2,2)*(t2 + 56*Power(t2,2) - 33*Power(t2,3)) + 
               Power(s,3)*(64 + 30*t2 - 49*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s,2)*(24 + (61 + 124*s2)*t2 + 
                  (220 + 117*s2)*Power(t2,2) + (31 - 9*s2)*Power(t2,3)) \
- s2*(6 - 127*t2 + 17*Power(t2,2) + 117*Power(t2,3) + 19*Power(t2,4)) + 
               s*(-244 + (-65 - 270*s2 + 6*Power(s2,2))*t2 + 
                  (163 - 252*s2 - 63*Power(s2,2))*Power(t2,2) + 
                  (-33 + 2*s2 + 9*Power(s2,2))*Power(t2,3) + 
                  19*Power(t2,4))) - 
            2*Power(t1,2)*t2*
             (-21 + (16 - 62*s2 - 3*Power(s2,2))*t2 - 
               2*(29 + 17*s2 + 19*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,2) + 
               (166 - 20*s2 + 3*Power(s2,2) - 2*Power(s2,3))*
                Power(t2,3) + 
               (-113 + 60*s2 + 6*Power(s2,2))*Power(t2,4) + 
               10*Power(t2,5) + 
               8*Power(s,3)*(-6 - 12*t2 - 3*Power(t2,2) + 
                  2*Power(t2,3)) + 
               2*s*(28 + 3*(4 + 7*s2)*t2 + 
                  (48 + 22*s2 + 26*Power(s2,2))*Power(t2,2) + 
                  (-135 + 47*s2 + 10*Power(s2,2))*Power(t2,3) - 
                  (5 + 6*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (-71 - 174*t2 - 25*Power(t2,2) + 6*Power(t2,3) + 
                  s2*(-8 + 2*t2 - 34*Power(t2,2)))) + 
            16*Power(t2,2)*(-3 + 3*(-2 - s2 + Power(s2,2))*t2 + 
               (35 - 11*s2 + 5*Power(s2,2))*Power(t2,2) + 
               s2*(16 + s2)*Power(t2,3) + 
               (-28 + 16*s2 + Power(s2,2))*Power(t2,4) + 
               2*Power(t2,5) + 
               2*Power(s,3)*(-6 - 9*t2 + 2*Power(t2,3)) + 
               s*(14 + 39*t2 + 
                  (-21 + 26*s2 + 6*Power(s2,2))*Power(t2,2) + 
                  (-50 + 28*s2 + 4*Power(s2,2))*Power(t2,3) - 
                  2*(2 + s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (-29 - 55*t2 - 19*Power(t2,2) + Power(t2,3) - 
                  2*s2*(1 + 3*t2 + 4*Power(t2,2)))) + 
            Power(t1,4)*(-6 + (71 - 16*s2)*t2 + 
               (-107 + 60*s2 - 15*Power(s2,2))*Power(t2,2) - 
               (-193 + 46*s2 + 7*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,3) - (39 + 4*s2 + 4*Power(s2,2))*Power(t2,4) + 
               Power(s,3)*(-12 - 42*t2 - 35*Power(t2,2) + 
                  7*Power(t2,3)) + 
               s*(14 + 3*(-45 + 4*s2)*t2 + 
                  (87 - 118*s2 + 11*Power(s2,2))*Power(t2,2) + 
                  (-149 + 18*s2 + 15*Power(s2,2))*Power(t2,3) + 
                  (17 + 8*s2)*Power(t2,4)) + 
               Power(s,2)*t2*(34 + 37*t2 + 31*Power(t2,2) - 
                  4*Power(t2,3) - 2*s2*(1 - 24*t2 + 9*Power(t2,2))))) + 
         Power(s1,5)*(2*Power(t1,9)*t2 - 
            2*Power(t1,8)*(1 + (-10 + 6*s + s2)*t2 + 13*Power(t2,2)) + 
            Power(t1,7)*(-12 + (77 + 40*s - 24*s2)*t2 + 
               (-353 + 22*s + 68*s2)*Power(t2,2) + 70*Power(t2,3)) + 
            Power(t1,6)*(4 + (126 + 68*s - 7*s2)*t2 + 
               (-689 + 80*Power(s,2) + 179*s2 + 2*Power(s2,2) - 
                  3*s*(-67 + 44*s2))*Power(t2,2) + 
               (1512 + 37*s - 384*s2)*Power(t2,3) - 95*Power(t2,4)) + 
            Power(t1,5)*(14 + (79 - 20*s + 8*s2)*t2 + 
               (-830 + 12*Power(s,3) + Power(s,2)*(269 - 4*s2) + 
                  268*s2 + 35*Power(s2,2) - 2*Power(s2,3) - 
                  2*s*(437 + 46*s2 + 9*Power(s2,2)))*Power(t2,2) - 
               (-3493 + 309*Power(s,2) + s*(178 - 468*s2) + 1010*s2 + 
                  49*Power(s2,2))*Power(t2,3) + 
               (-2177 - 204*s + 658*s2)*Power(t2,4) + 135*Power(t2,5)) + 
            Power(t1,4)*(-6 + (-127 - 83*s + 29*s2)*t2 + 
               (-816 + 21*Power(s,3) - 195*s2 + 23*Power(s2,2) - 
                  5*Power(s2,3) + 5*Power(s,2)*(7 + 9*s2) + 
                  s*(31 + 138*s2 - 11*Power(s2,2)))*Power(t2,2) + 
               (3750 + 131*Power(s,3) - 594*s2 - 262*Power(s2,2) + 
                  33*Power(s2,3) - 169*Power(s,2)*(6 + s2) + 
                  s*(-56 + 1540*s2 + 125*Power(s2,2)))*Power(t2,3) + 
               (-8351 + 625*Power(s,2) + 3492*s2 + 317*Power(s2,2) - 
                  2*s*(634 + 447*s2))*Power(t2,4) + 
               (1153 + 336*s - 542*s2)*Power(t2,5) - 93*Power(t2,6)) + 
            Power(t2,2)*(-6 + 
               2*(-37 + 6*s*(-2 + s2) - 30*s2 + 39*Power(s2,2))*t2 + 
               (5 - 285*s2 - 103*Power(s2,2) + 126*Power(s2,3) + 
                  Power(s,2)*(-30 + 22*s2) + 
                  2*s*(97 + 77*s2 + 20*Power(s2,2)))*Power(t2,2) + 
               (803 + 20*Power(s,3) + 148*s2 - 286*Power(s2,2) - 
                  32*Power(s2,3) - Power(s,2)*(172 + 101*s2) + 
                  s*(-237 + 584*s2 + 261*Power(s2,2)))*Power(t2,3) + 
               (-1121 + 6*Power(s,3) + Power(s,2)*(202 - 93*s2) + 
                  779*s2 + 9*Power(s2,2) - 136*Power(s2,3) + 
                  s*(-632 - 184*s2 + 223*Power(s2,2)))*Power(t2,4) + 
               (-169 - 2*Power(s,3) - 252*s2 + 8*Power(s2,2) + 
                  2*Power(s2,3) + 2*Power(s,2)*(5 + 3*s2) - 
                  3*s*(-81 + 6*s2 + 2*Power(s2,2)))*Power(t2,5) + 
               22*Power(t2,6)) + 
            Power(t1,2)*t2*(34 + 
               (370 + 84*Power(s,2) + s*(89 - 72*s2) - 47*s2 - 
                  12*Power(s2,2))*t2 - 
               (-2 + 126*Power(s,3) - 583*s2 + 268*Power(s2,2) + 
                  7*Power(s2,3) + Power(s,2)*(-238 + 19*s2) + 
                  s*(461 + 584*s2 + 75*Power(s2,2)))*Power(t2,2) - 
               (5036 + 279*Power(s,3) + 1702*s2 - 647*Power(s2,2) + 
                  221*Power(s2,3) - Power(s,2)*(857 + 392*s2) + 
                  s*(-1521 + 2286*s2 + 289*Power(s2,2)))*Power(t2,3) + 
               (11805 + 27*Power(s,3) - 5564*s2 - 592*Power(s2,2) + 
                  164*Power(s2,3) + Power(s,2)*(-1742 + 49*s2) + 
                  s*(2883 + 2200*s2 - 240*Power(s2,2)))*Power(t2,4) + 
               (-782 + 111*Power(s,2) + 2131*s2 + 115*Power(s2,2) - 
                  s*(1793 + 226*s2))*Power(t2,5) + 
               (-13 + 31*s - 31*s2)*Power(t2,6)) + 
            Power(t1,3)*t2*(-91 + 360*t2 + 2069*Power(t2,2) - 
               9284*Power(t2,3) + 7022*Power(t2,4) - 316*Power(t2,5) + 
               8*Power(t2,6) + 
               6*Power(s2,3)*t2*(1 + 9*t2 - 22*Power(t2,2)) - 
               Power(s,3)*t2*(8 + 11*t2 + 161*Power(t2,2)) - 
               Power(s2,2)*t2*
                (31 + 280*t2 - 416*Power(t2,2) + 449*Power(t2,3)) + 
               s*(30 + 6*(16 + 35*s2 + 2*Power(s2,2))*t2 + 
                  (1155 - 356*s2 + 110*Power(s2,2))*Power(t2,2) + 
                  (789 - 2948*s2 - 66*Power(s2,2))*Power(t2,3) + 
                  (2670 + 934*s2)*Power(t2,4) - 238*Power(t2,5)) + 
               2*s2*(-6 - 43*t2 + 363*Power(t2,2) + 967*Power(t2,3) - 
                  2119*Power(t2,4) + 133*Power(t2,5)) - 
               Power(s,2)*t2*
                (305 + 301*t2 - 2075*Power(t2,2) + 513*Power(t2,3) + 
                  s2*(30 + 126*t2 - 314*Power(t2,2)))) + 
            t1*Power(t2,2)*(53 - 515*t2 - 671*Power(t2,2) + 
               5644*Power(t2,3) - 2604*Power(t2,4) - 301*Power(t2,5) + 
               8*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(-18 + 119*t2 + 25*Power(t2,2)) + 
               2*Power(s2,3)*t2*
                (-16 + 16*t2 + 184*Power(t2,2) - 27*Power(t2,3)) + 
               Power(s2,2)*t2*
                (133 + 730*t2 + 11*Power(t2,2) + 226*Power(t2,3) + 
                  2*Power(t2,4)) - 
               2*s2*(-27 - 89*t2 + 536*Power(t2,2) + 387*Power(t2,3) - 
                  1450*Power(t2,4) + 140*Power(t2,5)) - 
               s*(36 + (-101 + 150*s2 + 54*Power(s2,2))*t2 + 
                  (-457 - 192*s2 + 88*Power(s2,2))*Power(t2,2) + 
                  (581 - 1938*s2 + 145*Power(s2,2))*Power(t2,3) + 
                  (2415 + 528*s2 - 133*Power(s2,2))*Power(t2,4) + 
                  (-270 + 4*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(236 - 199*t2 - 1397*Power(t2,2) + 
                  342*Power(t2,3) + 2*Power(t2,4) - 
                  2*s2*(-18 - 50*t2 + 119*Power(t2,2) + 52*Power(t2,3))))\
) + Power(s1,4)*(-2*Power(t1,9)*t2*(-1 + s + 2*t2) + 
            Power(t1,8)*(-2 + (13 + 6*s - 4*s2)*t2 + 
               2*(9*s + 5*(-7 + s2))*Power(t2,2) + 21*Power(t2,3)) + 
            Power(t1,6)*(4 + 2*(11 + s)*t2 + 
               (-255 + 8*Power(s,3) + Power(s,2)*(151 - 6*s2) + 
                  108*s2 + 4*Power(s2,2) - 
                  s*(341 + 24*s2 + 4*Power(s2,2)))*Power(t2,2) - 
               (-1454 + 93*Power(s,2) + s*(464 - 208*s2) + 288*s2 + 
                  4*Power(s2,2))*Power(t2,3) + 
               (-1240 + 21*s + 296*s2)*Power(t2,4) + 49*Power(t2,5)) + 
            Power(t1,5)*(-2 + (-31 - 51*s + 11*s2)*t2 + 
               (-442 + 27*Power(s,3) - 21*s2 + 9*Power(s2,2) + 
                  Power(s,2)*(109 + 17*s2) + 
                  s*(-181 + 22*s2 - 7*Power(s2,2)))*Power(t2,2) + 
               (93*Power(s,3) - 3*Power(s,2)*(248 + 29*s2) + 
                  s*(80 + 878*s2 + 69*Power(s2,2)) + 
                  4*(483 - 155*s2 - 30*Power(s2,2) + Power(s2,3)))*
                Power(t2,3) + 
               (-5079 + 251*Power(s,2) + 1584*s2 + 95*Power(s2,2) - 
                  4*s*(-73 + 91*s2))*Power(t2,4) + 
               (1189 - 6*s - 222*s2)*Power(t2,5) - 77*Power(t2,6)) + 
            Power(t1,3)*t2*(36 + 
               (394 + 150*Power(s,2) + 59*s2 + 18*Power(s2,2) - 
                  3*s*(1 + 36*s2))*t2 + 
               (529 - 341*Power(s,3) + 375*s2 + 56*Power(s2,2) + 
                  9*Power(s2,3) + Power(s,2)*(252 + 137*s2) + 
                  s*(61 - 1080*s2 - 81*Power(s2,2)))*Power(t2,2) - 
               (4963 + 499*Power(s,3) + 957*s2 - 587*Power(s2,2) + 
                  95*Power(s2,3) - Power(s,2)*(1049 + 571*s2) + 
                  s*(-133 + 1934*s2 + 435*Power(s2,2)))*Power(t2,3) + 
               (13396 + 146*Power(s,3) - 4793*s2 - 732*Power(s2,2) + 
                  86*Power(s2,3) - 2*Power(s,2)*(1109 + 130*s2) + 
                  s*(-261 + 2960*s2 + 28*Power(s2,2)))*Power(t2,4) + 
               (-4223 + 263*Power(s,2) + 2379*s2 + 271*Power(s2,2) - 
                  3*s*(455 + 178*s2))*Power(t2,5) + 
               (333 + 43*s - 43*s2)*Power(t2,6)) + 
            t1*Power(t2,2)*(-64 + 
               (-371 - 192*Power(s,2) - 110*s2 + 24*Power(s2,2) + 
                  8*s*(7 + 24*s2))*t2 + 
               (1022 + 262*Power(s,3) - 740*s2 + 233*Power(s2,2) + 
                  36*Power(s2,3) - 4*Power(s,2)*(97 + 36*s2) + 
                  6*s*(24 + 139*s2 + 45*Power(s2,2)))*Power(t2,2) + 
               (2619 + 309*Power(s,3) + Power(s,2)*(122 - 401*s2) + 
                  2459*s2 - 1016*Power(s2,2) + 94*Power(s2,3) + 
                  s*(-2365 + 1768*s2 + 446*Power(s2,2)))*Power(t2,3) - 
               (9701 + 104*Power(s,3) - 3514*s2 + Power(s2,2) + 
                  349*Power(s2,3) - 2*Power(s,2)*(949 + 21*s2) + 
                  s*(936 + 2006*s2 - 411*Power(s2,2)))*Power(t2,4) + 
               (1130 - 11*Power(s,3) - 2379*s2 - 180*Power(s2,2) + 
                  11*Power(s2,3) + Power(s,2)*(-184 + 33*s2) + 
                  s*(1943 + 364*s2 - 33*Power(s2,2)))*Power(t2,5) + 
               (47 - 62*s + 62*s2)*Power(t2,6)) - 
            Power(t1,7)*t2*(-35 + s2 + 224*t2 - 32*Power(s,2)*t2 - 
               56*s2*t2 - 525*Power(t2,2) + 117*s2*Power(t2,2) + 
               36*Power(t2,3) + 
               s*(-23 + 3*(-35 + 16*s2)*t2 + 40*Power(t2,2))) + 
            Power(t1,4)*t2*(-67 + 170*t2 + 1392*Power(t2,2) + 
               2*Power(s2,3)*(7 - 17*t2)*Power(t2,2) - 
               6855*Power(t2,3) + 7790*Power(t2,4) - 622*Power(t2,5) + 
               28*Power(t2,6) - 
               2*Power(s,3)*t2*(11 + 4*t2 + 95*Power(t2,2)) - 
               Power(s2,2)*t2*
                (31 + 124*t2 - 406*Power(t2,2) + 345*Power(t2,3)) + 
               2*s*(12 + (139 + 95*s2 + 3*Power(s2,2))*t2 + 
                  (585 - 125*s2 + 51*Power(s2,2))*Power(t2,2) + 
                  (883 - 1391*s2 - 99*Power(s2,2))*Power(t2,3) + 
                  (501 + 355*s2)*Power(t2,4) - 27*Power(t2,5)) + 
               2*s2*(-3 - 80*t2 + 104*Power(t2,2) + 597*Power(t2,3) - 
                  1615*Power(t2,4) + 54*Power(t2,5)) - 
               Power(s,2)*t2*
                (343 + 638*t2 - 1722*Power(t2,2) + 377*Power(t2,3) + 
                  s2*(24 + 130*t2 - 392*Power(t2,2)))) + 
            Power(t1,2)*Power(t2,2)*
             (105 - 1144*t2 + 251*Power(t2,2) + 9439*Power(t2,3) - 
               12420*Power(t2,4) + 425*Power(t2,5) - 20*Power(t2,6) + 
               Power(s,3)*t2*
                (104 + 165*t2 + 441*Power(t2,2) + 10*Power(t2,3)) - 
               2*Power(s2,3)*t2*
                (11 + 35*t2 - 163*Power(t2,2) + 35*Power(t2,3)) + 
               Power(s2,2)*t2*
                (83 + 766*t2 - 734*Power(t2,2) + 939*Power(t2,3) - 
                  30*Power(t2,4)) - 
               2*s2*(6 - 68*t2 + 1065*Power(t2,2) - 929*Power(t2,3) - 
                  3505*Power(t2,4) + 452*Power(t2,5)) + 
               s*(-6 + 2*(108 - 161*s2 + 6*Power(s2,2))*t2 - 
                  4*(55 - 454*s2 + 5*Power(s2,2))*Power(t2,2) + 
                  (-5229 + 5288*s2 + 346*Power(s2,2))*Power(t2,3) + 
                  (-3955 - 2174*s2 + 150*Power(s2,2))*Power(t2,4) + 
                  60*(13 + s2)*Power(t2,5)) + 
               Power(s,2)*t2*
                (693 - 691*t2 - 3143*Power(t2,2) + 1303*Power(t2,3) - 
                  30*Power(t2,4) + 
                  s2*(6 + 154*t2 - 946*Power(t2,2) - 90*Power(t2,3)))) + 
            Power(t2,3)*(2 + 305*t2 + 6*Power(t2,2) - 1886*Power(t2,3) + 
               1114*Power(t2,4) + 127*Power(t2,5) - 4*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(14 - 79*t2 - 25*Power(t2,2)) + 
               2*Power(s2,3)*t2*
                (14 - 133*t2 - 43*Power(t2,2) + 36*Power(t2,3)) - 
               Power(s2,2)*t2*
                (362 + 73*t2 - 76*Power(t2,2) + 65*Power(t2,3) + 
                  2*Power(t2,4)) + 
               2*s2*(-42 + 44*t2 + 329*Power(t2,2) - 2*Power(t2,3) - 
                  546*Power(t2,4) + 70*Power(t2,5)) + 
               s*(12 + 4*(-29 + 11*s2 + 21*Power(s2,2))*t2 + 
                  (-471 - 502*s2 + 76*Power(s2,2))*Power(t2,2) + 
                  (553 - 1052*s2 - 245*Power(s2,2))*Power(t2,3) + 
                  (895 + 202*s2 - 169*Power(s2,2))*Power(t2,4) + 
                  (-135 + 4*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(-122 + 229*t2 + 608*Power(t2,2) - 
                  159*Power(t2,3) - 2*Power(t2,4) + 
                  2*s2*(-6 + 2*t2 + 161*Power(t2,2) + 61*Power(t2,3))))) \
+ Power(s1,3)*t2*(2*Power(t1,9)*t2*(-2 + 2*s + t2) + 
            Power(t2,2)*(48 + 
               2*(39 + 48*Power(s,2) + 174*s2 - 24*Power(s2,2) - 
                  2*s*(13 + 48*s2))*t2 + 
               (-643 - 132*Power(s,3) + 64*s2 + 538*Power(s2,2) - 
                  84*Power(s2,3) + 6*Power(s,2)*(23 + 22*s2) + 
                  s*(222 - 460*s2 - 348*Power(s2,2)))*Power(t2,2) - 
               (394 + 88*Power(s,3) + Power(s,2)*(485 - 54*s2) + 
                  999*s2 - 231*Power(s2,2) - 302*Power(s2,3) + 
                  s*(-1149 + 82*s2 + 412*Power(s2,2)))*Power(t2,3) + 
               (3026 + 109*Power(s,3) - 803*s2 + 198*Power(s2,2) + 
                  141*Power(s2,3) - Power(s,2)*(766 + 229*s2) + 
                  s*(-123 + 736*s2 - 21*Power(s2,2)))*Power(t2,4) + 
               (-488 + 15*Power(s,3) + Power(s,2)*(73 - 45*s2) + 
                  877*s2 + 65*Power(s2,2) - 15*Power(s2,3) + 
                  3*s*(-233 - 46*s2 + 15*Power(s2,2)))*Power(t2,5) + 
               (-27 + 31*s - 31*s2)*Power(t2,6)) + 
            Power(t1,4)*(6 + 
               (176 + 84*Power(s,2) + 37*s2 - 3*s*(15 + 8*s2))*t2 + 
               (-337*Power(s,3) + 5*Power(s,2)*(32 + 27*s2) + 
                  s*(361 - 648*s2 - 29*Power(s2,2)) + 
                  3*(121 + 82*s2 + 36*Power(s2,2)))*Power(t2,2) - 
               (2073 + 417*Power(s,3) - 159*s2 - 241*Power(s2,2) + 
                  8*Power(s2,3) - Power(s,2)*(981 + 371*s2) + 
                  s*(539 + 888*s2 + 255*Power(s2,2)))*Power(t2,3) + 
               (156*Power(s,3) - 2*Power(s,2)*(587 + 162*s2) + 
                  s*(-2417 + 1758*s2 + 164*Power(s2,2)) + 
                  4*(1818 - 440*s2 - 93*Power(s2,2) + Power(s2,3)))*
                Power(t2,4) + 
               (-3749 + 143*Power(s,2) + 1259*s2 + 163*Power(s2,2) - 
                  s*(229 + 306*s2))*Power(t2,5) + 
               (257 - 7*s + 7*s2)*Power(t2,6)) + 
            Power(t1,2)*t2*(-42 - 
               (459 + 402*Power(s,2) + 112*s2 + 6*Power(s2,2) - 
                  2*s*(77 + 90*s2))*t2 + 
               (748 + 816*Power(s,3) - 52*s2 - 253*Power(s2,2) + 
                  54*Power(s2,3) - Power(s,2)*(447 + 386*s2) + 
                  2*s*(-731 + 963*s2 + 34*Power(s2,2)))*Power(t2,2) + 
               (992 + 688*Power(s,3) + Power(s,2)*(1730 - 634*s2) + 
                  3091*s2 - 814*Power(s2,2) + 88*Power(s2,3) + 
                  s*(207 - 824*s2 + 490*Power(s2,2)))*Power(t2,3) + 
               (-10403 - 413*Power(s,3) + 611*s2 + 727*Power(s2,2) - 
                  217*Power(s2,3) + Power(s,2)*(3317 + 799*s2) + 
                  s*(5561 - 4494*s2 - 169*Power(s2,2)))*Power(t2,4) + 
               (7247 - 11*Power(s,3) - 4124*s2 - 634*Power(s2,2) + 
                  11*Power(s2,3) + Power(s,2)*(-626 + 33*s2) + 
                  s*(2146 + 1260*s2 - 33*Power(s2,2)))*Power(t2,5) + 
               (-427 - 150*s + 150*s2)*Power(t2,6)) + 
            Power(t1,8)*(5 + 6*Power(s,2)*t2 + (-31 + 8*s2)*t2 + 
               (79 - 14*s2)*Power(t2,2) - 7*Power(t2,3) + 
               s*(3 + (20 - 8*s2)*t2 - 19*Power(t2,2))) + 
            Power(t1,7)*(1 + 2*Power(s,3)*t2 + (-35 + 12*s2)*t2 + 
               Power(s,2)*(47 - 2*s2 - 7*t2)*t2 + 
               (305 - 48*s2)*Power(t2,2) + (-311 + 78*s2)*Power(t2,3) + 
               4*Power(t2,4) + 
               s*(2 - 4*(10 + s2)*t2 + (-217 + 60*s2)*Power(t2,2) + 
                  27*Power(t2,3))) + 
            Power(t1,6)*(-3 + (-101 + 9*s2)*t2 - 
               3*(-159 + 72*s2 + 4*Power(s2,2))*Power(t2,2) + 
               5*(-312 + 61*s2)*Power(t2,3) + 
               (474 - 74*s2)*Power(t2,4) - 13*Power(t2,5) + 
               3*Power(s,3)*t2*(5 + 11*t2) + 
               Power(s,2)*t2*
                (59 + s2 - 284*t2 - 15*s2*t2 + 17*Power(t2,2)) + 
               s*(-11 - (129 + 4*s2)*t2 + 
                  (31 + 232*s2 + 12*Power(s2,2))*Power(t2,2) + 
                  (484 - 56*s2)*Power(t2,3) - 39*Power(t2,4))) + 
            Power(t1,5)*(-9 + (14 - 60*s2)*t2 + 
               (513 - 70*s2 - 37*Power(s2,2))*Power(t2,2) + 
               (-2633 + 620*s2 + 148*Power(s2,2))*Power(t2,3) + 
               (3599 - 1150*s2 - 71*Power(s2,2))*Power(t2,4) - 
               2*(162 + 11*s2)*Power(t2,5) + 16*Power(t2,6) - 
               2*Power(s,3)*t2*(10 - 7*t2 + 47*Power(t2,2)) + 
               s*(6 + 2*(97 + 20*s2)*t2 + 
                  (360 + 42*s2 + 31*Power(s2,2))*Power(t2,2) + 
                  (1144 - 1278*s2 - 99*Power(s2,2))*Power(t2,3) + 
                  2*(-105 + 58*s2)*Power(t2,4) + 66*Power(t2,5)) - 
               Power(s,2)*t2*
                (167 + 596*t2 - 664*Power(t2,2) + 33*Power(t2,3) + 
                  s2*(6 + 50*t2 - 186*Power(t2,2)))) + 
            Power(t1,3)*t2*(3 - 712*t2 - 953*Power(t2,2) + 
               7314*Power(t2,3) - 11211*Power(t2,4) + 
               1435*Power(t2,5) - 112*Power(t2,6) - 
               4*Power(s2,3)*Power(t2,2)*(17 - 22*t2 + 4*Power(t2,2)) + 
               2*Power(s,3)*t2*
                (101 + 164*t2 + 242*Power(t2,2) - 25*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-53 + 74*t2 - 604*Power(t2,2) + 705*Power(t2,3) - 
                  62*Power(t2,4)) + 
               s2*(-6 + 74*t2 - 794*Power(t2,2) + 352*Power(t2,3) + 
                  4910*Power(t2,4) - 468*Power(t2,5)) + 
               2*s*(-6 + (-148 + 3*s2 + 3*Power(s2,2))*t2 + 
                  2*(-72 + 394*s2 + 27*Power(s2,2))*Power(t2,2) + 
                  2*(-803 + 1130*s2 + 141*Power(s2,2))*Power(t2,3) - 
                  (312 + 797*s2 + 9*Power(s2,2))*Power(t2,4) + 
                  (96 + 62*s2)*Power(t2,5)) + 
               Power(s,2)*t2*
                (653 + 412*t2 - 2476*Power(t2,2) + 873*Power(t2,3) - 
                  62*Power(t2,4) + 
                  4*s2*(3 - 29*t2 - 258*Power(t2,2) + 21*Power(t2,3)))) \
+ t1*Power(t2,2)*(24 + 1151*t2 - 2435*Power(t2,2) - 4551*Power(t2,3) + 
               10125*Power(t2,4) - 266*Power(t2,5) + 16*Power(t2,6) - 
               2*Power(s,3)*t2*
                (98 + 185*t2 + 196*Power(t2,2) + Power(t2,3)) + 
               2*Power(s2,3)*t2*
                (6 + 52*t2 - 117*Power(t2,2) + 79*Power(t2,3)) + 
               Power(s2,2)*t2*
                (8 - 1117*t2 + 699*Power(t2,2) - 320*Power(t2,3) + 
                  50*Power(t2,4)) + 
               2*s2*(30 - 67*t2 + 971*Power(t2,2) - 1712*Power(t2,3) - 
                  2579*Power(t2,4) + 505*Power(t2,5)) - 
               2*s*(42 + (334 + 56*s2 + 30*Power(s2,2))*t2 + 
                  (138 + 1013*s2 + 197*Power(s2,2))*Power(t2,2) + 
                  (-3073 + 2355*s2 + 270*Power(s2,2))*Power(t2,3) + 
                  (-1240 - 702*s2 + 159*Power(s2,2))*Power(t2,4) + 
                  (423 + 50*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(-432 + 1335*t2 + 1979*Power(t2,2) - 
                  1108*Power(t2,3) + 50*Power(t2,4) + 
                  2*s2*(42 + 58*t2 + 471*Power(t2,2) + 81*Power(t2,3))))) \
+ Power(s1,6)*(12*Power(t1,8)*t2 - 
            2*Power(t1,7)*(6 + (-37 + 15*s + 6*s2)*t2 + 
               34*Power(t2,2)) + 
            Power(t1,6)*(-30 + 2*(98 + 55*s - 30*s2)*t2 + 
               (-839 - 32*s + 196*s2)*Power(t2,2) + 105*Power(t2,3)) + 
            Power(t1,5)*(16 + 2*(131 + 51*s - 9*s2)*t2 + 
               (-1167 + 120*Power(s,2) + s*(127 - 208*s2) + 355*s2 + 
                  8*Power(s2,2))*Power(t2,2) + 
               (2182 + 297*s - 697*s2)*Power(t2,3) - 107*Power(t2,4)) + 
            Power(t1,4)*(18 + (115 - 52*s + 24*s2)*t2 + 
               (-1381 + 8*Power(s,3) + 230*s2 + 73*Power(s2,2) - 
                  10*Power(s2,3) + Power(s,2)*(281 + 4*s2) - 
                  s*(987 + 166*s2 + 32*Power(s2,2)))*Power(t2,2) + 
               (4633 - 469*Power(s,2) - 1782*s2 - 121*Power(s2,2) + 
                  14*s*(47 + 45*s2))*Power(t2,3) + 
               (-1646 - 623*s + 938*s2)*Power(t2,4) + 91*Power(t2,5)) + 
            Power(t1,3)*(-6 + (-181 - 57*s + 25*s2)*t2 + 
               (-670 + 6*Power(s,3) - 245*s2 + 49*Power(s2,2) - 
                  13*Power(s2,3) + Power(s,2)*(-66 + 43*s2) + 
                  3*s*(64 + 62*s2 + 3*Power(s2,2)))*Power(t2,2) + 
               (3849 + 90*Power(s,3) - 243*s2 - 174*Power(s2,2) + 
                  85*Power(s2,3) - 5*Power(s,2)*(146 + 27*s2) + 
                  s*(-176 + 1272*s2 + 75*Power(s2,2)))*Power(t2,3) + 
               (-6647 + 592*Power(s,2) + 3606*s2 + 341*Power(s2,2) - 
                  2*s*(1171 + 429*s2))*Power(t2,4) + 
               (126 + 549*s - 629*s2)*Power(t2,5) - 7*Power(t2,6)) - 
            t1*t2*(-4 + (-154 + 31*s2 + 42*Power(s2,2) - 
                  4*s*(11 + 3*s2))*t2 + 
               (-123 - 356*s2 + 208*Power(s2,2) + 45*Power(s2,3) + 
                  6*Power(s,2)*(-13 + 9*s2) + 
                  s*(327 + 100*s2 + 27*Power(s2,2)))*Power(t2,2) + 
               (2099 + 52*Power(s,3) + 139*s2 - 200*Power(s2,2) + 
                  185*Power(s2,3) - Power(s,2)*(396 + 107*s2) + 
                  s*(-304 + 956*s2 + 66*Power(s2,2)))*Power(t2,3) + 
               (-2709 + 18*Power(s,3) + Power(s,2)*(372 - 111*s2) + 
                  1972*s2 + 90*Power(s2,2) - 106*Power(s2,3) + 
                  s*(-1615 - 394*s2 + 199*Power(s2,2)))*Power(t2,4) + 
               (-445 + 10*Power(s,2) - 18*s*(-27 + s2) - 504*s2 + 
                  8*Power(s2,2))*Power(t2,5) + 44*Power(t2,6)) + 
            Power(t1,2)*t2*(-37 + 160*t2 + 1583*Power(t2,2) - 
               6325*Power(t2,3) + 1804*Power(t2,4) + 221*Power(t2,5) - 
               4*Power(t2,6) - 2*Power(s,3)*Power(t2,2)*(1 + 22*t2) + 
               2*Power(s2,3)*t2*(8 + 35*t2 - 91*Power(t2,2)) + 
               Power(s2,2)*t2*
                (13 - 346*t2 + 112*Power(t2,2) - 167*Power(t2,3)) + 
               s*(12 + (1 + 34*s2 + 6*Power(s2,2))*t2 + 
                  (426 + 76*s2 + 52*Power(s2,2))*Power(t2,2) + 
                  (-705 - 1364*s2 + 150*Power(s2,2))*Power(t2,3) + 
                  (2163 + 338*s2)*Power(t2,4) - 135*Power(t2,5)) + 
               2*s2*(-3 - 25*t2 + 249*Power(t2,2) + 1000*Power(t2,3) - 
                  1270*Power(t2,4) + 70*Power(t2,5)) - 
               Power(s,2)*t2*
                (96 + 160*t2 - 1107*Power(t2,2) + 189*Power(t2,3) + 
                  s2*(12 + 32*t2 - 42*Power(t2,2)))) + 
            Power(t2,2)*(-2*Power(s2,3)*t2*
                (13 - 33*t2 - 62*Power(t2,2) + 5*Power(t2,3)) - 
               2*s2*(-6 - (22 + 7*s)*t2 + 
                  (19 + 40*s + 8*Power(s,2))*Power(t2,2) + 
                  (159 - 60*s)*Power(t2,3) + 
                  3*(-38 - 5*s + 3*Power(s,2))*Power(t2,4)) + 
               Power(s2,2)*t2*
                (77 + 176*t2 + 7*Power(t2,2) - 12*Power(t2,3) + 
                  3*s*(-4 - 25*t2 - 35*Power(t2,2) + 8*Power(t2,3))) + 
               t2*(1 - 229*t2 + 547*Power(t2,2) + 65*Power(t2,3) - 
                  48*Power(t2,4) + 4*Power(s,3)*Power(t2,2)*(1 + t2) - 
                  2*Power(s,2)*t2*(-20 + 54*t2 + 7*Power(t2,2)) - 
                  s*(12 + 13*t2 - 242*Power(t2,2) + 225*Power(t2,3))))) - 
         Power(s1,2)*Power(t2,2)*
          (2*(-1 + s)*Power(t1,9)*t2 - 
            Power(t1,8)*(-3 + (26 - 4*s2)*t2 + 
               (-29 + 6*s2)*Power(t2,2) + 2*Power(s,2)*(3 + t2) + 
               s*(3 + (-34 + 8*s2)*t2 + 7*Power(t2,2))) + 
            Power(t1,7)*(4 + (-41 + 21*s2)*t2 + 
               (202 - 24*s2)*Power(t2,2) + (-65 + 17*s2)*Power(t2,3) - 
               Power(s,3)*(3 + 5*t2) + 
               Power(s,2)*(-9 + 44*t2 + 9*Power(t2,2)) + 
               s*(29 - 8*(1 + 3*s2)*t2 + 2*(-61 + 4*s2)*Power(t2,2) + 
                  3*Power(t2,3))) + 
            Power(t1,6)*(12 + (-79 + 30*s2)*t2 - 
               2*(-226 + 69*s2 + 6*Power(s2,2))*Power(t2,2) - 
               2*(369 - 81*s2 + 2*Power(s2,2))*Power(t2,3) + 
               (47 + 4*s2)*Power(t2,4) + 
               6*Power(s,3)*(1 - 3*t2 + 3*Power(t2,2)) + 
               Power(s,2)*(33 + 6*(37 + s2)*t2 - 
                  (99 + 32*s2)*Power(t2,2) - 42*Power(t2,3)) + 
               s*(-49 - 2*(13 + 16*s2)*t2 + 
                  (-363 + 260*s2 + 12*Power(s2,2))*Power(t2,2) + 
                  (201 + 32*s2)*Power(t2,3) - 17*Power(t2,4))) - 
            Power(t1,5)*(36 + (72 + 109*s2)*t2 + 
               (-448 + 180*s2 + 57*Power(s2,2))*Power(t2,2) - 
               (-1899 + 371*s2 + 74*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,3) + 
               (-1162 + 293*s2 + 13*Power(s2,2))*Power(t2,4) + 
               (51 + 19*s2)*Power(t2,5) + 
               Power(s,3)*t2*(-143 - 175*t2 + 60*Power(t2,2)) + 
               Power(s,2)*(18 + (142 + 33*s2)*t2 + 
                  (481 + 114*s2)*Power(t2,2) - 
                  (276 + 119*s2)*Power(t2,3) - 9*Power(t2,4)) - 
               s*(29 + 6*(-19 + 22*s2)*t2 + 
                  (131 + 318*s2 + 51*Power(s2,2))*Power(t2,2) + 
                  (1076 - 502*s2 - 63*Power(s2,2))*Power(t2,3) + 
                  (-83 + 4*s2)*Power(t2,4) + 19*Power(t2,5))) + 
            Power(t1,3)*t2*(193 - 306*t2 - 834*Power(t2,2) + 
               5499*Power(t2,3) - 4879*Power(t2,4) + 323*Power(t2,5) + 
               3*Power(s2,3)*Power(t2,2)*(-30 + 13*t2 + Power(t2,2)) - 
               Power(s,3)*t2*
                (742 + 628*t2 - 341*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-51 + 248*t2 - 355*Power(t2,2) + 302*Power(t2,3)) + 
               Power(s,2)*(168 + (659 + 196*s2)*t2 + 
                  8*(-12 + 35*s2)*Power(t2,2) - 
                  (1997 + 697*s2)*Power(t2,3) + 
                  3*(70 + 3*s2)*Power(t2,4)) + 
               s2*(-18 + 404*t2 - 699*Power(t2,2) - 461*Power(t2,3) + 
                  2068*Power(t2,4) + 30*Power(t2,5)) - 
               s*(140 - 2*(404 - 371*s2 + 9*Power(s2,2))*t2 + 
                  (-107 - 340*s2 + 42*Power(s2,2))*Power(t2,2) + 
                  (4157 - 2874*s2 - 317*Power(s2,2))*Power(t2,3) + 
                  (412 + 512*s2 + 9*Power(s2,2))*Power(t2,4) + 
                  30*Power(t2,5))) - 
            t1*Power(t2,2)*(232 - 1137*t2 + 2630*Power(t2,2) + 
               3678*Power(t2,3) - 5902*Power(t2,4) + 227*Power(t2,5) - 
               Power(s2,3)*t2*
                (36 + 208*t2 - 199*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s,3)*t2*
                (-708 - 418*t2 + 379*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-168 + 1253*t2 - 328*Power(t2,2) + 355*Power(t2,3)) + 
               Power(s,2)*(384 + 4*(4 + 37*s2)*t2 + 
                  (-3295 + 204*s2)*Power(t2,2) - 
                  (2660 + 807*s2)*Power(t2,3) + 
                  (419 - 81*s2)*Power(t2,4)) + 
               s2*(-180 + 394*t2 - 2457*Power(t2,2) + 
                  2219*Power(t2,3) + 3299*Power(t2,4) - 171*Power(t2,5)) \
+ s*(12 + 4*(535 - 268*s2 + 45*Power(s2,2))*t2 + 
                  (-457 + 1930*s2 + 102*Power(s2,2))*Power(t2,2) + 
                  (-6577 + 3972*s2 + 229*Power(s2,2))*Power(t2,3) + 
                  (-1561 - 774*s2 + 81*Power(s2,2))*Power(t2,4) + 
                  171*Power(t2,5))) + 
            Power(t1,4)*(17 + (125 + 72*s2)*t2 + 
               2*(245 + 34*s2 + 69*Power(s2,2))*Power(t2,2) + 
               (-2173 + 328*s2 + 209*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,3) - 
               (-4072 + 1292*s2 + 205*Power(s2,2) + 12*Power(s2,3))*
                Power(t2,4) + 
               (-727 + 76*s2 + 26*Power(s2,2))*Power(t2,5) + 
               52*Power(t2,6) + 
               2*Power(s,3)*t2*
                (-68 - 82*t2 - 107*Power(t2,2) + 25*Power(t2,3)) - 
               s*(6 + 4*(-48 + 17*s2)*t2 + 
                  (21 + 544*s2 + 51*Power(s2,2))*Power(t2,2) + 
                  (-1004 + 1608*s2 + 263*Power(s2,2))*Power(t2,3) + 
                  (981 - 364*s2 - 74*Power(s2,2))*Power(t2,4) + 
                  4*(-28 + 13*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(-203 - 776*t2 + 678*Power(t2,2) - 
                  59*Power(t2,3) + 26*Power(t2,4) + 
                  s2*(6 + 166*t2 + 462*Power(t2,2) - 112*Power(t2,3)))) + 
            2*Power(t2,2)*(64 + (161 + 246*s2 - 72*Power(s2,2))*t2 + 
               (-626 + 194*s2 + 111*Power(s2,2) - 42*Power(s2,3))*
                Power(t2,2) + 
               (-396 - 582*s2 + 64*Power(s2,2) + 90*Power(s2,3))*
                Power(t2,3) + 
               (1564 - 724*s2 + 79*Power(s2,2) + 40*Power(s2,3))*
                Power(t2,4) + 
               (-33 + 186*s2 + 10*Power(s2,2))*Power(t2,5) + 
               2*Power(t2,6) + 
               2*Power(s,3)*t2*
                (-24 - 49*t2 - 18*Power(t2,2) + 7*Power(t2,3)) - 
               2*s*(24 + (105 + 128*s2)*t2 + 
                  3*(-6 + 91*s2 + 45*Power(s2,2))*Power(t2,2) + 
                  4*(-142 + 90*s2 + 35*Power(s2,2))*Power(t2,3) + 
                  (-143 - 47*s2 + 33*Power(s2,2))*Power(t2,4) + 
                  2*(38 + 5*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(-16 + 267*t2 + 152*Power(t2,2) - 
                  165*Power(t2,3) + 10*Power(t2,4) + 
                  2*s2*(24 + 73*t2 + 85*Power(t2,2) + 6*Power(t2,3)))) + 
            Power(t1,2)*t2*(-118 - (641 + 384*s2 + 18*Power(s2,2))*t2 + 
               3*(39 + 6*s2 - 127*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,2) + 
               (3231 + 1876*s2 - 331*Power(s2,2) + 14*Power(s2,3))*
                Power(t2,3) + 
               (-7225 + 2306*s2 + 548*Power(s2,2) - 44*Power(s2,3))*
                Power(t2,4) - 
               2*(-996 + 434*s2 + 79*Power(s2,2))*Power(t2,5) - 
               172*Power(t2,6) + 
               2*Power(s,3)*t2*
                (260 + 486*t2 + 209*Power(t2,2) - 73*Power(t2,3)) + 
               2*s*(24 + t2 + 254*s2*t2 + 
                  (-391 + 1191*s2 + 138*Power(s2,2))*Power(t2,2) + 
                  (-714 + 943*s2 + 345*Power(s2,2))*Power(t2,3) + 
                  (943 - 758*s2 - 29*Power(s2,2))*Power(t2,4) + 
                  (143 + 158*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(238 + 403*t2 - 211*Power(t2,2) + 
                  812*Power(t2,3) - 158*Power(t2,4) + 
                  s2*(-48 - 726*t2 - 994*Power(t2,2) + 248*Power(t2,3)))))\
)*R1q(t2))/((s - s2 + t1)*Power(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2,
        2)*Power(s1 - t2,3)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*
       (-1 + t2)*t2*(s - s1 + t2)) - 
    (8*(-4*Power(s1,9)*Power(t1,2) + 
         2*Power(-1 + s,2)*Power(t1,7)*(-1 + s - t2) + 
         Power(t1,3)*(-4 + (111 + 84*Power(s,2) - 10*s2 + 
               2*s*(-107 + 6*s2))*t2 - 
            (28 + 6*Power(s,3) - 163*s2 + 13*Power(s2,2) - 
               6*Power(s2,3) - 3*Power(s,2)*(61 + 38*s2) + 
               s*(207 + 274*s2 + 2*Power(s2,2)))*Power(t2,2) - 
            (38 + 41*Power(s,3) + 131*s2 + 15*Power(s2,3) - 
               Power(s,2)*(128 + 91*s2) + 
               s*(-187 + 168*s2 + 35*Power(s2,2)))*Power(t2,3) + 
            (152 + 3*Power(s,3) + Power(s,2)*(13 - 9*s2) - 195*s2 - 
               51*Power(s2,2) - 3*Power(s2,3) + 
               s*(23 + 38*s2 + 9*Power(s2,2)))*Power(t2,4) + 
            (-33 + 19*s - 19*s2)*Power(t2,5)) - 
         4*t1*t2*(-7 + (12 + 27*Power(s,2) - 43*s2 - 9*Power(s2,2) + 
               s*(-61 + 30*s2))*t2 - 
            (74 + 5*Power(s,3) + 13*s2 + 52*Power(s2,2) + 
               3*Power(s2,3) - Power(s,2)*(76 + 23*s2) + 
               s*(39 + 28*s2 - 25*Power(s2,2)))*Power(t2,2) - 
            (64 + 19*Power(s,3) + 129*s2 + 7*Power(s2,2) + 
               9*Power(s2,3) - Power(s,2)*(57 + 41*s2) + 
               s*(-141 + 54*s2 + 13*Power(s2,2)))*Power(t2,3) + 
            (65 + 12*Power(s,2) - 75*s2 - 20*Power(s2,2) + 
               s*(-5 + 8*s2))*Power(t2,4) + 4*(-3 + s - s2)*Power(t2,5)) \
+ Power(s1,7)*(-66*Power(t1,4) + Power(s2,3)*(-2 + 11*t1 - 6*t2) - 
            4*Power(t2,2) - 4*t1*t2*(8 + 3*s + 6*t2) + 
            Power(t1,3)*(95 + 39*s + 51*t2) + 
            2*Power(s2,2)*(-3 + 4*Power(t1,2) + t1*(4 + s - t2) + 
               3*s*t2) - s2*t1*
             (29*t1 + 33*Power(t1,2) + s*(6 + t1 - 6*t2) - 8*t2 - 
               37*t1*t2) - 2*Power(t1,2)*
             (-13 - s + Power(s,2) - 30*t2 + 16*s*t2 + 6*Power(t2,2))) + 
         Power(t1,6)*(10 + t2 + 8*s2*t2 - 2*(-3 + s2)*Power(t2,2) - 
            Power(s,3)*(4 + 5*t2) + 
            Power(s,2)*(18 + (13 + 8*s2)*t2 + 8*Power(t2,2)) - 
            s*(24 + (9 + 16*s2)*t2 + 4*(3 + s2)*Power(t2,2))) + 
         2*Power(s1,8)*(Power(s2,3) - 2*s2*Power(t1,2) + 
            t1*(-13*Power(t1,2) + 4*t2 + t1*(8 + 3*s + 6*t2))) + 
         Power(t1,4)*(14 + 7*(-7 + 4*s2)*t2 + 
            (46 - 21*s2 + 9*Power(s2,2))*Power(t2,2) + 
            (11 + 58*s2 + 12*Power(s2,2) + 4*Power(s2,3))*Power(t2,3) + 
            (34 + 7*s2 + 4*Power(s2,2))*Power(t2,4) - 
            7*Power(s,3)*t2*(-4 - 5*t2 + Power(t2,2)) - 
            s*(12 + (-135 + 38*s2)*t2 + 
               (22 - 70*s2 + 17*Power(s2,2))*Power(t2,2) + 
               (-55 + 38*s2 + 15*Power(s2,2))*Power(t2,3) + 
               4*(5 + 2*s2)*Power(t2,4)) + 
            Power(s,2)*t2*(-128 - 51*t2 - 16*Power(t2,2) + 
               4*Power(t2,3) + 6*s2*(2 - 7*t2 + 3*Power(t2,2)))) + 
         Power(t1,5)*(-18 + (7 - 26*s2)*t2 - 4*s2*(1 + s2)*Power(t2,2) + 
            (-33 + 12*s2 - 2*Power(s2,2))*Power(t2,3) + 
            Power(s,3)*t2*(-10 + 7*t2) - 
            Power(s,2)*(12 + (-7 + 16*s2)*t2 + 
               (12 + 17*s2)*Power(t2,2) + 10*Power(t2,3)) + 
            s*(30 - 23*Power(t2,2) + 10*Power(s2,2)*Power(t2,2) + 
               11*Power(t2,3) + 3*s2*t2*(14 + 11*t2 + 4*Power(t2,2)))) + 
         2*Power(t1,2)*t2*(-48 - 
            (28 + 101*s2 + 24*Power(s2,2) + 2*Power(s2,3))*t2 - 
            (106 + 49*s2 + 12*Power(s2,2) + 5*Power(s2,3))*Power(t2,2) + 
            (-14 - 23*s2 + 42*Power(s2,2) + 3*Power(s2,3))*Power(t2,3) + 
            (-62 + 77*s2 + 6*Power(s2,2))*Power(t2,4) + 10*Power(t2,5) + 
            Power(s,3)*t2*(-20 - 27*t2 + 11*Power(t2,2)) + 
            s*(42 + t2 + 168*s2*t2 + 
               (205 + 20*s2 + 71*Power(s2,2))*Power(t2,2) + 
               (-117 + 56*s2 + 5*Power(s2,2))*Power(t2,3) - 
               3*(9 + 4*s2)*Power(t2,4)) + 
            Power(s,2)*t2*(88 - 48*t2 - 26*Power(t2,2) + 6*Power(t2,3) - 
               s2*(42 + 15*t2 + 19*Power(t2,2)))) - 
         8*Power(t2,2)*(-8 - 4*t2 + 18*Power(t2,2) + 20*Power(t2,3) - 
            30*Power(t2,4) + 4*Power(t2,5) + 
            Power(s2,3)*t2*(1 + t2 + 2*Power(t2,2)) + 
            Power(s,3)*t2*(1 - 3*t2 + 6*Power(t2,2)) + 
            2*Power(s2,2)*t2*
             (3 + 15*t2 + 12*Power(t2,2) + Power(t2,3)) + 
            s2*(9 + 15*t2 + 15*Power(t2,2) + 17*Power(t2,3) + 
               40*Power(t2,4)) + 
            s*(9 + (19 + 32*s2 - 9*Power(s2,2))*t2 + 
               (47 - 16*s2 + 19*Power(s2,2))*Power(t2,2) + 
               (-43 + 32*s2 + 2*Power(s2,2))*Power(t2,3) - 
               4*(4 + s2)*Power(t2,4)) + 
            Power(s,2)*t2*(-(s2*(9 + 17*t2 + 10*Power(t2,2))) + 
               2*(3 - 11*t2 - 18*Power(t2,2) + Power(t2,3)))) + 
         Power(s1,6)*(45*Power(t1,2) + 96*Power(t1,3) + 
            222*Power(t1,4) - 84*Power(t1,5) - 52*t1*t2 - 
            430*Power(t1,2)*t2 + 132*Power(t1,3)*t2 + 
            84*Power(t1,4)*t2 + 16*Power(t2,2) - 42*t1*Power(t2,2) - 
            169*Power(t1,2)*Power(t2,2) - 17*Power(t1,3)*Power(t2,2) + 
            12*Power(t2,3) + 24*t1*Power(t2,3) + 
            4*Power(t1,2)*Power(t2,3) + 
            Power(s,2)*t1*(-14*Power(t1,2) + 2*t2*(-1 + 3*t2) + 
               5*t1*(-4 + 5*t2)) + 
            Power(s2,3)*(-4 + 24*Power(t1,2) - 9*t2 + 6*Power(t2,2) - 
               t1*(2 + 33*t2)) + 
            s*(96*Power(t1,4) + 6*Power(t2,2) - 
               Power(t1,3)*(16 + 125*t2) + 
               2*t1*(3 - 5*t2 + 32*Power(t2,2)) + 
               Power(t1,2)*(-37 - 158*t2 + 59*Power(t2,2))) + 
            Power(s2,2)*(38*Power(t1,3) + 
               Power(t1,2)*(19 + 9*s - 3*t2) + 
               2*(3 + (5 - 7*s)*t2 + (1 - 6*s)*Power(t2,2)) + 
               t1*(-45 - 42*t2 + 4*Power(t2,2) + 5*s*(-2 + 7*t2))) + 
            s2*(6 - 98*Power(t1,4) - 4*Power(t2,2) + 
               6*Power(s,2)*Power(t2,2) - 6*s*t2*(1 + t2) + 
               Power(t1,3)*(-115 + 5*s + 168*t2) + 
               Power(t1,2)*(26 + Power(s,2) + 145*t2 - 4*s*t2 - 
                  64*Power(t2,2)) - 
               2*t1*(8 - 31*t2 + Power(s,2)*t2 + 37*Power(t2,2) + 
                  s*(-4 - 6*t2 + 5*Power(t2,2))))) + 
         Power(s1,5)*(-56*Power(t1,6) + 
            6*Power(t1,5)*(43 + 19*s + 11*t2) + 
            2*(-1 + (13 + 4*s - Power(s,2))*Power(t2,2) + 
               (4 - 16*s - 3*Power(s,2) + Power(s,3))*Power(t2,3) - 
               6*Power(t2,4)) + 
            Power(t1,4)*(138 - 34*Power(s,2) + 55*t2 + 21*Power(t2,2) - 
               4*s*(22 + 45*t2)) + 
            Power(s2,3)*(26*Power(t1,3) + Power(t1,2)*(12 - 72*t2) + 
               t2*(22 + 39*t2 - 2*Power(t2,2)) + 
               t1*(-16 - 43*t2 + 33*Power(t2,2))) + 
            Power(t1,3)*(159 + Power(s,3) - 1141*t2 - 345*Power(t2,2) - 
               8*Power(t2,3) + Power(s,2)*(-87 + 92*t2) + 
               s*(-117 - 503*t2 + 157*Power(t2,2))) + 
            t1*(8 - 92*t2 + 575*Power(t2,2) - 
               4*Power(s,3)*Power(t2,2) + 185*Power(t2,3) - 
               8*Power(t2,4) - 
               2*Power(s,2)*t2*(-30 + 30*t2 + Power(t2,2)) + 
               s*(-10 + 64*t2 + 209*Power(t2,2) - 118*Power(t2,3))) + 
            Power(t1,2)*(-70 + Power(s,3)*(6 - 7*t2) - 384*t2 + 
               212*Power(t2,2) + 73*Power(t2,3) + 
               Power(s,2)*(21 + 57*t2 - 29*Power(t2,2)) + 
               s*(-64 + 291*t2 + 391*Power(t2,2) - 31*Power(t2,3))) + 
            Power(s2,2)*(12 + 72*Power(t1,4) + (39 + 4*s)*t2 + 
               (16 - 23*s)*Power(t2,2) + (-4 + 6*s)*Power(t2,3) + 
               4*Power(t1,3)*(-1 + 4*s + 2*t2) - 
               Power(t1,2)*(110 + s*(40 - 76*t2) + 195*t2 + 
                  33*Power(t2,2)) - 
               t1*(4 + (-61 + 47*s)*t2 + (-44 + 70*s)*Power(t2,2) + 
                  2*Power(t2,3))) + 
            s2*(-6 - 142*Power(t1,5) + 2*(-1 + 8*s)*t2 + 
               (-33 + 13*s - 16*Power(s,2))*Power(t2,2) + 
               (37 + 10*s - 6*Power(s,2))*Power(t2,3) + 
               2*Power(t1,4)*(-85 + 15*s + 151*t2) + 
               Power(t1,3)*(80 + 2*Power(s,2) + 535*t2 - 
                  185*Power(t2,2) - 8*s*(-8 + 9*t2)) + 
               Power(t1,2)*(-4 + 191*t2 - 553*Power(t2,2) + 
                  31*Power(t2,3) + Power(s,2)*(-2 + 3*t2) + 
                  2*s*(9 - 20*t2 + 31*Power(t2,2))) + 
               t1*(57 - 199*Power(t2,2) + 128*Power(t2,3) + 
                  Power(s,2)*t2*(-14 + 41*t2) + 
                  2*s*(13 - 58*t2 - 12*Power(t2,2) + 2*Power(t2,3))))) + 
         s1*(2*Power(-1 + s,2)*Power(t1,8) + 
            Power(t1,2)*(-16 + 
               (-49 + 132*Power(s,2) - 26*s2 - 72*Power(s2,2) + 
                  s*(-302 + 36*s2))*t2 + 
               (-610 + 54*Power(s,3) - 261*s2 - 137*Power(s2,2) + 
                  2*Power(s2,3) + Power(s,2)*(443 + 166*s2) - 
                  s*(487 + 106*s2 + 78*Power(s2,2)))*Power(t2,2) - 
               (988 + 129*Power(s,3) + 1071*s2 + 310*Power(s2,2) + 
                  47*Power(s2,3) - Power(s,2)*(634 + 267*s2) + 
                  s*(-1959 + 716*s2 + 91*Power(s2,2)))*Power(t2,3) + 
               (1170 + 7*Power(s,3) - 1323*s2 - 233*Power(s2,2) - 
                  7*Power(s2,3) - 3*Power(s,2)*(11 + 7*s2) + 
                  s*(263 + 266*s2 + 21*Power(s2,2)))*Power(t2,4) + 
               (-163 + 39*s - 39*s2)*Power(t2,5)) + 
            4*t2*(-7 + (2 - 9*Power(s,2) - 43*s2 + 27*Power(s2,2) + 
                  s*(11 + 30*s2))*t2 + 
               (44 + 3*Power(s,3) - 9*s2 - 6*Power(s2,2) + 
                  13*Power(s2,3) - Power(s,2)*(6 + 17*s2) + 
                  s*(37 + 40*s2 + 41*Power(s2,2)))*Power(t2,2) - 
               (-50 + 3*Power(s,3) + 13*s2 - 35*Power(s2,2) + 
                  9*Power(s2,3) + Power(s,2)*(21 + 31*s2) + 
                  s*(151 - 94*s2 - 43*Power(s2,2)))*Power(t2,3) + 
               (-189 + 4*Power(s,3) + 97*s2 - 20*Power(s2,2) - 
                  4*Power(s2,3) - 4*Power(s,2)*(1 + 3*s2) + 
                  s*(-41 + 24*s2 + 12*Power(s2,2)))*Power(t2,4) + 
               4*(-3 + 4*s - 4*s2)*Power(t2,5)) + 
            Power(t1,7)*(-3 + Power(s,3) - 8*t2 + s2*(-2 + 8*t2) - 
               Power(s,2)*(7 + 2*s2 + 8*t2) + 
               s*(9 + 12*t2 + 4*s2*(1 + t2))) - 
            Power(t1,6)*(-7 + 7*Power(s,3)*(-2 + t2) + 12*t2 - 
               2*Power(s2,2)*(-4 + t2)*t2 - 64*Power(t2,2) + 
               Power(s,2)*(13 + s2*(2 - 11*t2) - 16*t2 - 
                  10*Power(t2,2)) + s2*(4 - 9*t2 + 32*Power(t2,2)) + 
               s*(12 - 57*t2 + 4*Power(s2,2)*t2 + 14*Power(t2,2) + 
                  6*s2*(-1 + 7*t2 + 2*Power(t2,2)))) + 
            4*t1*t2*(-5 - 49*t2 + 81*Power(t2,2) + 227*Power(t2,3) - 
               140*Power(t2,4) + 30*Power(t2,5) + 
               Power(s,3)*t2*(-1 - 20*t2 + 21*Power(t2,2)) + 
               Power(s2,3)*t2*(-1 - 20*t2 + 29*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-47 + 223*t2 + 120*Power(t2,2) + 28*Power(t2,3)) + 
               s2*(15 + 154*t2 - 16*Power(t2,2) + 94*Power(t2,3) + 
                  257*Power(t2,4)) + 
               s*(15 + (54 + 54*s2 - 15*Power(s2,2))*t2 + 
                  2*(142 - 77*s2 + 24*Power(s2,2))*Power(t2,2) + 
                  (-230 + 116*s2 - 37*Power(s2,2))*Power(t2,3) - 
                  7*(17 + 8*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (73 - 65*t2 - 164*Power(t2,2) + 28*Power(t2,3) - 
                  s2*(15 + 24*t2 + 13*Power(t2,2)))) + 
            Power(t1,3)*(52 + 217*t2 + 443*Power(t2,2) - 
               81*Power(t2,3) + 273*Power(t2,4) - 56*Power(t2,5) + 
               Power(s,3)*t2*(52 + 103*t2 - 55*Power(t2,2)) + 
               Power(s2,3)*t2*(8 + 33*t2 - 29*Power(t2,2)) - 
               Power(s2,2)*t2*
                (-116 + 221*t2 + 97*Power(t2,2) + 38*Power(t2,3)) + 
               s2*(12 - 25*t2 + 403*Power(t2,2) + 249*Power(t2,3) - 
                  351*Power(t2,4)) + 
               s*(-48 - (11 + 136*s2 + 12*Power(s2,2))*t2 + 
                  (-855 + 122*s2 - 67*Power(s2,2))*Power(t2,2) + 
                  (383 - 302*s2 + 3*Power(s2,2))*Power(t2,3) + 
                  (107 + 76*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (-340 + 75*t2 + 167*Power(t2,2) - 38*Power(t2,3) + 
                  s2*(48 - 101*t2 + 81*Power(t2,2)))) + 
            Power(t1,4)*(-49 - 15*t2 + 170*Power(t2,2) - 
               459*Power(t2,3) + 85*Power(t2,4) + 
               Power(s,3)*t2*(-54 + 61*t2 - 5*Power(t2,2)) + 
               Power(s2,3)*t2*(-12 + 14*t2 + 5*Power(t2,2)) + 
               3*Power(s2,2)*t2*(-10 + 29*t2 + 30*Power(t2,2)) + 
               Power(s,2)*(-48 - (141 + 70*s2)*t2 - 
                  (279 + 103*s2)*Power(t2,2) + (4 + 15*s2)*Power(t2,3)) \
+ s2*(-32 - 77*t2 + 152*Power(t2,2) + 535*Power(t2,3) + 
                  38*Power(t2,4)) + 
               s*(130 + 339*t2 - 517*Power(t2,2) - 94*Power(t2,3) - 
                  38*Power(t2,4) + 
                  Power(s2,2)*t2*(42 + 28*t2 - 15*Power(t2,2)) + 
                  s2*(12 + 142*t2 + 367*Power(t2,2) - 94*Power(t2,3)))) \
+ Power(t1,5)*(7 - 53*t2 + 3*Power(t2,2) - 2*Power(s2,3)*Power(t2,2) - 
               65*Power(t2,3) + 
               Power(s2,2)*t2*(34 - 23*t2 - 4*Power(t2,2)) + 
               Power(s,3)*(-16 - 31*t2 + 11*Power(t2,2)) + 
               Power(s,2)*(92 + (69 + 55*s2)*t2 + 
                  (17 - 24*s2)*Power(t2,2) - 4*Power(t2,3)) - 
               s2*(-26 + 17*t2 + 133*Power(t2,2) + 14*Power(t2,3)) + 
               s*(-75 + 25*t2 - 77*Power(t2,2) + 40*Power(t2,3) + 
                  Power(s2,2)*t2*(-8 + 15*t2) + 
                  s2*(-22 - 86*t2 + 62*Power(t2,2) + 8*Power(t2,3))))) + 
         Power(s1,2)*((2 - 6*s2)*Power(t1,8) - 
            t1*(-16 + (3 - 72*Power(s,2) + 10*s2 + 132*Power(s2,2) + 
                  s*(70 + 36*s2))*t2 - 
               (-504 + 10*Power(s,3) + 177*s2 - 519*Power(s2,2) + 
                  6*Power(s2,3) + Power(s,2)*(181 + 18*s2) + 
                  s*(-613 + 10*s2 - 178*Power(s2,2)))*Power(t2,2) + 
               (874 + 67*Power(s,3) + 205*s2 + 412*Power(s2,2) + 
                  133*Power(s2,3) - Power(s,2)*(436 + 249*s2) + 
                  s*(-1973 + 976*s2 + 49*Power(s2,2)))*Power(t2,3) + 
               (-2224 + 27*Power(s,3) + Power(s,2)*(89 - 81*s2) + 
                  1429*s2 + 25*Power(s2,2) - 27*Power(s2,3) + 
                  3*s*(-219 - 38*s2 + 27*Power(s2,2)))*Power(t2,4) + 
               (-101 + 171*s - 171*s2)*Power(t2,5)) + 
            Power(t1,7)*(12 + 2*Power(s,2) + s2 + 6*Power(s2,2) - 
               29*t2 + 20*s2*t2 + s*(-40 + 9*s2 + 3*t2)) + 
            Power(t1,6)*(11 + 4*Power(s,3) - 53*t2 + 28*Power(t2,2) + 
               Power(s2,2)*(-13 + 5*t2) - 
               Power(s,2)*(48 + 7*s2 + 7*t2) + 
               s*(21 + Power(s2,2) + s2*(40 - 12*t2) + 7*t2 - 
                  20*Power(t2,2)) + s2*(-4 + 116*t2 + 7*Power(t2,2))) + 
            Power(t1,2)*(4 + 287*t2 + 59*Power(t2,2) - 
               1987*Power(t2,3) + 281*Power(t2,4) - 172*Power(t2,5) + 
               Power(s2,3)*t2*(32 + 53*t2 - 121*Power(t2,2)) + 
               Power(s,3)*t2*(32 + 81*t2 - 69*Power(t2,2)) - 
               Power(s2,2)*t2*
                (-172 + 691*t2 + 433*Power(t2,2) + 158*Power(t2,3)) + 
               s2*(-24 - 317*t2 + 469*Power(t2,2) + 267*Power(t2,3) - 
                  1267*Power(t2,4)) + 
               s*(-24 + 3*(-67 - 44*s2 + 8*Power(s2,2))*t2 + 
                  (-1339 + 502*s2 - 41*Power(s2,2))*Power(t2,2) + 
                  (367 - 362*s2 + 173*Power(s2,2))*Power(t2,3) + 
                  (685 + 316*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (-332 - 19*t2 + 639*Power(t2,2) - 158*Power(t2,3) + 
                  s2*(24 + 35*t2 + 17*Power(t2,2)))) - 
            Power(t1,5)*(-19 + 174*t2 - 444*Power(t2,2) + 
               71*Power(t2,3) + 4*Power(s,3)*(-9 + 7*t2) + 
               Power(s2,3)*(-6 + 9*t2) + 
               Power(s,2)*(-18 + s2*(8 - 42*t2) - 171*t2 + 
                  17*Power(t2,2)) + 
               Power(s2,2)*(11 + 77*t2 + 39*Power(t2,2)) + 
               s2*(-40 + 8*t2 + 457*Power(t2,2) + 19*Power(t2,3)) + 
               s*(162 - 460*t2 - 91*Power(t2,2) - 19*Power(t2,3) + 
                  5*Power(s2,2)*(2 + t2) - 
                  2*s2*(6 - 141*t2 + 28*Power(t2,2)))) + 
            Power(t1,3)*(21 + 383*t2 + 1244*Power(t2,2) - 
               1787*Power(t2,3) + 227*Power(t2,4) + 
               Power(s,3)*t2*(-90 + 119*t2 - 3*Power(t2,2)) + 
               Power(s2,3)*t2*(-50 + 95*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(24 + 175*t2 + 383*Power(t2,2) + 
                  278*Power(t2,3)) + 
               Power(s,2)*(-60 - (295 + 62*s2)*t2 - 
                  (765 + 197*s2)*Power(t2,2) + 
                  3*(62 + 3*s2)*Power(t2,3)) + 
               s2*(-14 - 91*t2 + 641*Power(t2,2) + 1978*Power(t2,3) + 
                  30*Power(t2,4)) + 
               s*(130 + 767*t2 - 2319*Power(t2,2) - 660*Power(t2,3) - 
                  30*Power(t2,4) + 
                  Power(s2,2)*t2*(134 - 17*t2 - 9*Power(t2,2)) + 
                  s2*(12 + 34*t2 + 1072*Power(t2,2) - 464*Power(t2,3)))) \
+ 2*t2*(-6 + (-42 + 47*s + 30*Power(s,2) - 2*Power(s,3))*t2 + 
               (8 + 155*s - 38*Power(s,2) - 9*Power(s,3))*Power(t2,2) + 
               (312 - 7*s - 94*Power(s,2) + 23*Power(s,3))*
                Power(t2,3) + (14 - 83*s + 10*Power(s,2))*Power(t2,4) + 
               2*Power(t2,5) + 
               Power(s2,3)*t2*(-20 + 49*t2 + 31*Power(t2,2)) + 
               s2*(42 + 3*(15 + 4*s)*t2 + 
                  (1 - 32*s - 37*Power(s,2))*Power(t2,2) - 
                  (29 + 15*Power(s,2))*Power(t2,3) + 
                  (117 - 20*s)*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-(s*(42 + 59*t2 + 39*Power(t2,2))) + 
                  2*(47 + 43*t2 + 51*Power(t2,2) + 5*Power(t2,3)))) + 
            Power(t1,4)*(-101 - 331*t2 + 416*Power(t2,2) - 
               150*Power(t2,3) + 52*Power(t2,4) + 
               Power(s2,3)*(-4 - 8*t2 + 9*Power(t2,2)) + 
               Power(s,3)*(-24 - 55*t2 + 29*Power(t2,2)) + 
               Power(s2,2)*(-26 + 163*t2 + 7*Power(t2,2) + 
                  26*Power(t2,3)) + 
               Power(s,2)*(164 + (179 + 79*s2)*t2 - 
                  (155 + 49*s2)*Power(t2,2) + 26*Power(t2,3)) + 
               s2*(41 - 259*t2 - 572*Power(t2,2) + 240*Power(t2,3)) + 
               s*(-15 + 413*t2 + 30*Power(t2,2) - 52*Power(t2,3) + 
                  Power(s2,2)*t2*(-21 + 11*t2) - 
                  4*s2*(8 + 54*t2 - 62*Power(t2,2) + 13*Power(t2,3))))) + 
         Power(s1,4)*(2 - 18*Power(t1,7) - 2*t2 - 2*s*t2 + 
            47*Power(t2,2) - 39*s*Power(t2,2) - 
            22*Power(s,2)*Power(t2,2) - 240*Power(t2,3) - 
            90*s*Power(t2,3) + 41*Power(s,2)*Power(t2,3) - 
            4*Power(s,3)*Power(t2,3) - 67*Power(t2,4) + 
            59*s*Power(t2,4) + 2*Power(s,2)*Power(t2,4) + 
            4*Power(t2,5) + 2*Power(t1,6)*(76 + 33*s + 12*t2) - 
            Power(t1,5)*(36*Power(s,2) + 2*s*(76 + 55*t2) - 
               5*(20 - 23*t2 + 9*Power(t2,2))) + 
            Power(t1,4)*(200 + 4*Power(s,3) - 1157*t2 - 
               251*Power(t2,2) - 28*Power(t2,3) + 
               2*Power(s,2)*(-74 + 59*t2) + 
               3*s*(-40 - 199*t2 + 39*Power(t2,2))) + 
            Power(s2,3)*(14*Power(t1,4) + Power(t1,3)*(28 - 78*t2) + 
               t2*(28 - 17*t2 - 43*Power(t2,2)) + 
               t1*t2*(18 + 145*t2 - 11*Power(t2,2)) + 
               Power(t1,2)*(-24 - 67*t2 + 61*Power(t2,2))) + 
            Power(t1,3)*(-141 + Power(s,3)*(26 - 28*t2) - 780*t2 + 
               854*Power(t2,2) - 37*Power(t2,3) + 
               Power(s,2)*(68 + 253*t2 - 113*Power(t2,2)) + 
               s*(-266 + 970*t2 + 709*Power(t2,2) - 43*Power(t2,3))) + 
            Power(t1,2)*(-45 - 266*t2 + 2238*Power(t2,2) + 
               466*Power(t2,3) + 20*Power(t2,4) - 
               Power(s,3)*(4 + 8*t2 + Power(t2,2)) + 
               s*(43 + 420*t2 + 656*Power(t2,2) - 480*Power(t2,3)) + 
               Power(s,2)*(34 + 200*t2 - 285*Power(t2,2) + 
                  30*Power(t2,3))) + 
            t1*(-23 + 114*t2 + 484*Power(t2,2) - 556*Power(t2,3) - 
               95*Power(t2,4) + 
               Power(s,3)*t2*(-12 + 18*t2 + 11*Power(t2,2)) + 
               Power(s,2)*t2*(-36 - 123*t2 + 40*Power(t2,2)) + 
               s*(-16 + 209*t2 - 502*Power(t2,2) - 411*Power(t2,3) + 
                  62*Power(t2,4))) + 
            Power(s2,2)*(68*Power(t1,5) + 
               2*Power(t1,4)*(-23 + 7*s + 11*t2) - 
               Power(t1,3)*(120 + s*(60 - 74*t2) + 325*t2 + 
                  121*Power(t2,2)) + 
               Power(t1,2)*(-52 + (187 - 57*s)*t2 - 
                  3*(-45 + 41*s)*Power(t2,2) + 30*Power(t2,3)) + 
               t1*(48 + (269 + 58*s)*t2 + (241 - 89*s)*Power(t2,2) + 
                  (36 + 33*s)*Power(t2,3)) + 
               t2*(-56 - 98*t2 - 35*Power(t2,2) + 2*Power(t2,3) + 
                  2*s*(6 + 43*t2 + 41*Power(t2,2)))) - 
            s2*(12 + 108*Power(t1,6) + (51 + 14*s)*t2 + 
               (8 - 92*s - 19*Power(s,2))*Power(t2,2) + 
               (-87 - 16*s + 35*Power(s,2))*Power(t2,3) + 
               4*(16 + s)*Power(t2,4) - 
               2*Power(t1,5)*(-55 + 25*s + 134*t2) + 
               Power(t1,4)*(-82 + 2*Power(s,2) - 767*t2 + 
                  171*Power(t2,2) + 8*s*(-17 + 16*t2)) - 
               Power(t1,3)*(88 + 170*t2 - 1277*Power(t2,2) + 
                  43*Power(t2,3) + 8*Power(s,2)*(-1 + 4*t2) + 
                  2*s*(6 - 157*t2 + 117*Power(t2,2))) + 
               Power(t1,2)*(-85 + 157*t2 + 774*Power(t2,2) - 
                  604*Power(t2,3) - 7*Power(s,2)*t2*(-5 + 9*t2) + 
                  s*(-64 + 306*t2 - 82*Power(t2,2) + 60*Power(t2,3))) + 
               t1*(-14 + 91*t2 + 113*Power(t2,2) - 606*Power(t2,3) + 
                  62*Power(t2,4) + 
                  Power(s,2)*t2*(-30 + 74*t2 + 33*Power(t2,2)) + 
                  s*(12 + 4*t2 - 247*Power(t2,2) + 76*Power(t2,3))))) + 
         Power(s1,3)*(4 - 2*Power(t1,8) + 
            (21 + 46*s2 - 84*Power(s2,2) - 2*s*(-5 + 6*s2))*t2 + 
            (-50 + 6*Power(s,3) + Power(s,2)*(9 - 10*s2) + 113*s2 - 
               51*Power(s2,2) - 78*Power(s2,3) - 
               s*(133 + 110*s2 + 30*Power(s2,2)))*Power(t2,2) - 
            (196 + 3*Power(s,3) - 31*s2 + 54*Power(s2,2) + 
               45*Power(s2,3) - Power(s,2)*(74 + 113*s2) + 
               s*(-233 + 236*s2 + 65*Power(s2,2)))*Power(t2,3) + 
            (278 - 15*Power(s,3) - 221*s2 - 3*Power(s2,2) + 
               15*Power(s2,3) + Power(s,2)*(-11 + 45*s2) + 
               s*(145 + 14*s2 - 45*Power(s2,2)))*Power(t2,4) + 
            (39 - 31*s + 31*s2)*Power(t2,5) + 
            Power(t1,7)*(39 + 15*s - 41*s2 + 3*t2) + 
            Power(t1,6)*(42 - 14*Power(s,2) + 32*Power(s2,2) + 
               s*(-118 + 35*s2 - 20*t2) - 119*t2 + 19*Power(t2,2) + 
               s2*(-25 + 117*t2)) + 
            Power(t1,5)*(100 + 6*Power(s,3) + 3*Power(s2,3) - 
               2*Power(s,2)*(61 + 4*s2 - 26*t2) - 459*t2 - 
               23*Power(t2,2) - 16*Power(t2,3) + 
               2*Power(s2,2)*(-22 + 9*t2) + 
               s2*(26 + 477*t2 - 43*Power(t2,2)) + 
               s*(-28 + 6*Power(s2,2) + s2*(114 - 82*t2) - 245*t2 - 
                  Power(t2,2))) - 
            t1*(8 - 123*t2 - 93*Power(t2,2) + 1943*Power(t2,3) + 
               233*Power(t2,4) + 16*Power(t2,5) + 
               Power(s,3)*t2*(-8 - 29*t2 + 25*Power(t2,2)) + 
               Power(s2,3)*t2*(-52 - 19*t2 + 131*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-8 + 439*t2 + 359*Power(t2,2) + 50*Power(t2,3)) + 
               s2*(48 + 283*t2 - 45*Power(t2,2) - 395*Power(t2,3) + 
                  653*Power(t2,4)) - 
               s*(12 + (-89 - 64*s2 + 48*Power(s2,2))*t2 + 
                  (-601 + 462*s2 + 111*Power(s2,2))*Power(t2,2) + 
                  (-235 + 14*s2 + 237*Power(s2,2))*Power(t2,3) + 
                  (489 + 100*s2)*Power(t2,4)) + 
               Power(s,2)*t2*(112 + 79*t2 - 369*Power(t2,2) + 
                  50*Power(t2,3) + s2*(12 - 65*t2 + 81*Power(t2,2)))) + 
            Power(t1,4)*(-61 + Power(s2,3)*(22 - 42*t2) + 
               Power(s,3)*(44 - 42*t2) - 612*t2 + 1048*Power(t2,2) - 
               157*Power(t2,3) - 
               Power(s2,2)*(60 + 241*t2 + 125*Power(t2,2)) + 
               s2*(120 + 24*t2 - 1223*Power(t2,2) - 7*Power(t2,3)) + 
               Power(s,2)*(72 + 347*t2 - 105*Power(t2,2) + 
                  2*s2*(-6 + 29*t2)) + 
               s*(-340 + 1076*t2 + 487*Power(t2,2) + 7*Power(t2,3) + 
                  Power(s2,2)*(-40 + 26*t2) + 
                  s2*(8 - 502*t2 + 230*Power(t2,2)))) + 
            Power(t1,3)*(-151 - 443*t2 + 2138*Power(t2,2) + 
               224*Power(t2,3) + 112*Power(t2,4) + 
               Power(s,3)*(-16 - 37*t2 + 21*Power(t2,2)) + 
               Power(s2,3)*(-16 - 41*t2 + 45*Power(t2,2)) + 
               Power(s2,2)*(-68 + 265*t2 + 123*Power(t2,2) + 
                  62*Power(t2,3)) + 
               Power(s,2)*(124 + (263 + 11*s2)*t2 + 
                  (-389 + 3*s2)*Power(t2,2) + 62*Power(t2,3)) + 
               s2*(49 - 391*t2 - 1132*Power(t2,2) + 730*Power(t2,3)) + 
               s*(89 + 735*t2 + 692*Power(t2,2) - 454*Power(t2,3) - 
                  Power(s2,2)*t2*(37 + 69*t2) + 
                  s2*(28 - 342*t2 + 282*Power(t2,2) - 124*Power(t2,3)))) \
+ Power(t1,2)*(31 + 479*t2 + 1316*Power(t2,2) - 2293*Power(t2,3) - 
               37*Power(t2,4) + 
               Power(s2,3)*t2*(-42 + 187*t2 - 11*Power(t2,2)) + 
               Power(s,3)*t2*(-58 + 83*t2 + 11*Power(t2,2)) + 
               Power(s2,2)*(60 + 435*t2 + 549*Power(t2,2) + 
                  226*Power(t2,3)) - 
               Power(s,2)*(24 + (183 - 22*s2)*t2 + 
                  (523 + 169*s2)*Power(t2,2) + (-218 + 33*s2)*Power(t2,3)\
) + s2*(38 - 145*t2 + 269*Power(t2,2) + 2016*Power(t2,3) - 
                  150*Power(t2,4)) + 
               s*(14 + 653*t2 - 2263*Power(t2,2) - 1022*Power(t2,3) + 
                  150*Power(t2,4) + 
                  Power(s2,2)*t2*(146 - 101*t2 + 33*Power(t2,2)) - 
                  2*s2*(6 + 43*t2 - 406*Power(t2,2) + 222*Power(t2,3))))))*
       R2q(1 - s1 - t1 + t2))/
     ((s - s2 + t1)*Power(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2,2)*
       (-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)*(s - s1 + t2)) + 
    (8*(Power(s1,12)*(Power(s2,3) + s2*Power(t1,2) - 2*Power(t1,3)) + 
         Power(-1 + s,2)*Power(t1,10)*(-1 + s - t2) + 
         Power(t1,5)*(2 + (-156 - 60*Power(s,2) + s*(215 - 6*s2) + 
               5*s2)*t2 + (116 - 80*Power(s,3) - 257*s2 + 
               3*Power(s2,2) - 16*Power(s,2)*(-17 + 9*s2) + 
               s*(-209 + 397*s2 - 4*Power(s2,2)))*Power(t2,2) + 
            (-67 - 109*Power(s,3) + 135*s2 - 124*Power(s2,2) - 
               4*Power(s2,3) + 43*Power(s,2)*(3 + 2*s2) + 
               s*(177 - 37*s2 + 147*Power(s2,2)))*Power(t2,3) + 
            (167 + 10*Power(s,3) + Power(s,2)*(184 - 58*s2) + 168*s2 + 
               21*Power(s2,2) - 14*Power(s2,3) + 
               s*(-334 + 71*s2 + 74*Power(s2,2)))*Power(t2,4) + 
            (91 - Power(s,3) + 164*s2 + 2*Power(s2,3) + 
               Power(s,2)*(-29 + 4*s2) + 
               s*(-96 + 43*s2 - 5*Power(s2,2)))*Power(t2,5) + 
            (7 - 9*s + 9*s2)*Power(t2,6)) - 
         4*t1*Power(t2,2)*(-15 + 
            3*(9*Power(s,2) + 2*s*(-19 + 9*s2) - 
               3*(-4 + 8*s2 + Power(s2,2)))*t2 - 
            (31 + 6*Power(s,3) + 2*s2 + 70*Power(s2,2) + 
               4*Power(s2,3) - 2*Power(s,2)*(23 + 36*s2) + 
               s*(112 + 136*s2 - 50*Power(s2,2)))*Power(t2,2) + 
            2*(-35 + 12*Power(s,3) - 69*s2 + 19*Power(s2,2) + 
               Power(s2,3) + 41*Power(s,2)*(1 + s2) - 
               s*(101 + 92*s2 + 70*Power(s2,2)))*Power(t2,3) + 
            (-221 - 30*Power(s,3) + 86*s2 + 120*Power(s2,2) + 
               4*Power(s,2)*(49 + 23*s2) - 
               2*s*(81 + 230*s2 + 39*Power(s2,2)))*Power(t2,4) + 
            (16 - 4*Power(s,3) + 170*s2 + 27*Power(s2,2) + 
               2*Power(s2,3) + Power(s,2)*(91 + 10*s2) - 
               2*s*(110 + 71*s2 + 4*Power(s2,2)))*Power(t2,5) + 
            (27 + 6*Power(s,2) + 20*s2 + 6*Power(s2,2) - 
               2*s*(11 + 6*s2))*Power(t2,6) + 2*Power(t2,7)) + 
         2*Power(t1,3)*t2*(-10 + 
            5*(44 + 18*Power(s,2) - 5*s2 + s*(-68 + 6*s2))*t2 + 
            (7 + 15*Power(s,3) + 342*s2 - 11*Power(s2,2) + 
               4*Power(s2,3) + Power(s,2)*(13 + 230*s2) + 
               s*(-211 - 602*s2 + 15*Power(s2,2)))*Power(t2,2) + 
            (26 + 100*Power(s,3) - 201*s2 + 136*Power(s2,2) + 
               3*Power(s2,3) + 3*Power(s,2)*(4 + 9*s2) - 
               s*(385 + 196*s2 + 242*Power(s2,2)))*Power(t2,3) - 
            (414 + 21*Power(s,3) + Power(s,2)*(38 - 96*s2) + 203*s2 - 
               18*Power(s2,2) - 6*Power(s2,3) + 
               s*(-257 + 316*s2 + 105*Power(s2,2)))*Power(t2,4) - 
            (114 + 6*Power(s,3) + 34*s2 - 32*Power(s2,2) - 
               3*Power(s2,3) - Power(s,2)*(106 + 15*s2) + 
               s*(63 + 170*s2 + 12*Power(s2,2)))*Power(t2,5) + 
            (25 + 9*Power(s,2) + 25*s2 + 9*Power(s2,2) - 
               2*s*(13 + 9*s2))*Power(t2,6) + 4*Power(t2,7)) + 
         Power(t1,9)*(6 + t2 + 4*s2*t2 + (-1 + s2)*Power(t2,2) - 
            Power(s,3)*(3 + 2*t2) + 
            2*Power(s,2)*(6 + (3 + 2*s2)*t2 + 2*Power(t2,2)) - 
            s*(15 + (5 + 8*s2)*t2 + (5 + s2)*Power(t2,2))) + 
         Power(t1,8)*(-14 + (9 - 17*s2)*t2 + 
            (9 + 5*s2 - 5*Power(s2,2))*Power(t2,2) + 
            4*(2 + s2)*Power(t2,3) + 
            Power(s,3)*(2 - 7*t2 + Power(t2,2)) - 
            Power(s,2)*(15 + 12*(-1 + s2)*t2 + 
               (-9 + 7*s2)*Power(t2,2) + 6*Power(t2,3)) + 
            s*(27 + (-12 + 29*s2)*t2 + 
               (-16 + 8*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (-2 + 5*s2)*Power(t2,3))) + 
         Power(t1,7)*(16 + (-53 + 27*s2)*t2 + 
            (1 - 40*s2 + 13*Power(s2,2))*Power(t2,2) + 
            (-6 - 25*s2 - 6*Power(s2,2) + 2*Power(s2,3))*Power(t2,3) + 
            2*(-3 - 11*s2 + Power(s2,2))*Power(t2,4) + 
            Power(s,3)*t2*(29 + 25*t2 - Power(t2,2)) + 
            Power(s,2)*(6 + (-111 + 14*s2)*t2 - 
               2*(28 + 19*s2)*Power(t2,2) + (-51 + 6*s2)*Power(t2,3) + 
               4*Power(t2,4)) + 
            s*(-21 + (126 - 40*s2)*t2 + 
               (18 + 61*s2 - 13*Power(s2,2))*Power(t2,2) + 
               (71 + 9*s2 - 8*Power(s2,2))*Power(t2,3) - 
               7*(-2 + s2)*Power(t2,4))) + 
         Power(s1,11)*(Power(s2,3)*(-1 + 8*t1 - 4*t2) + 
            Power(s2,2)*(-3 + 2*Power(t1,2) + t1*(4 + s - t2) + 
               3*s*t2) - s2*t1*
             (-5*Power(t1,2) + s*(3 + t1 - 3*t2) + 2*t2 + t1*(11 + t2)) \
+ Power(t1,2)*(-1 - 17*Power(t1,2) + 6*t2 + 10*t1*(1 + t2) + 
               s*(-3 + 2*t1 + 4*t2))) + 
         Power(t1,6)*(-9 + (130 - 19*s2)*t2 + 
            (-35 + 151*s2 - 11*Power(s2,2))*Power(t2,2) - 
            (31 + 54*s2 - 58*Power(s2,2) + 2*Power(s2,3))*Power(t2,3) + 
            (-90 + 28*s2 + 5*Power(s2,2) - 3*Power(s2,3))*Power(t2,4) + 
            (3 + 14*s2 - 2*Power(s2,2))*Power(t2,5) + 
            Power(s,3)*t2*(-20 - 2*t2 - 17*Power(t2,2) + 
               2*Power(t2,3)) + 
            s*(6 + (-266 + 25*s2)*t2 + 
               (-58 - 275*s2 + 12*Power(s2,2))*Power(t2,2) + 
               (3 - 71*s2 - 51*Power(s2,2))*Power(t2,3) + 
               (-35 - 62*s2 + 8*Power(s2,2))*Power(t2,4) + 
               3*(-2 + s2)*Power(t2,5)) + 
            Power(s,2)*t2*(154 + 85*t2 + 76*Power(t2,3) - Power(t2,4) + 
               s2*(-6 + 130*t2 + 84*Power(t2,2) - 7*Power(t2,3)))) + 
         2*Power(t1,4)*t2*(45 + 5*(-32 + 19*s2)*t2 + 
            (89 - 173*s2 + 62*Power(s2,2))*Power(t2,2) + 
            (40 + 129*s2 - 104*Power(s2,2) + 18*Power(s2,3))*
             Power(t2,3) + (131 - 270*s2 - 16*Power(s2,2) + 
               4*Power(s2,3))*Power(t2,4) - 
            2*(20 + 49*s2 + 5*Power(s2,2))*Power(t2,5) - 
            3*(3 + s2)*Power(t2,6) + 
            Power(s,3)*t2*(30 + 45*t2 + 46*Power(t2,2) - 
               3*Power(t2,3)) + 
            s*(-30 - 5*(-77 + 25*s2)*t2 + 
               (83 + 343*s2 - 61*Power(s2,2))*Power(t2,2) + 
               (101 + 22*s2 + 60*Power(s2,2))*Power(t2,3) + 
               (238 + 121*s2 - 13*Power(s2,2))*Power(t2,4) + 
               (60 + 23*s2)*Power(t2,5) + 3*Power(t2,6)) - 
            Power(s,2)*t2*(245 + 207*t2 + 80*Power(t2,2) + 
               171*Power(t2,3) + 13*Power(t2,4) - 
               2*s2*(15 - 116*t2 - 80*Power(t2,2) + 6*Power(t2,3)))) + 
         2*Power(t1,2)*Power(t2,2)*
          (-135 - (24 + 307*s2 + 33*Power(s2,2) + 2*Power(s2,3))*t2 - 
            (245 - 29*s2 + 174*Power(s2,2) + 15*Power(s2,3))*
             Power(t2,2) + (108 - 48*s2 + 224*Power(s2,2) - 
               33*Power(s2,3))*Power(t2,3) - 
            (29 - 856*s2 + 34*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
            (60 + 179*s2 + Power(s2,2) - 5*Power(s2,3))*Power(t2,5) + 
            (9 - 5*s2)*Power(t2,6) + 
            Power(s,3)*t2*(-20 - 63*t2 - 121*Power(t2,2) + 
               15*Power(t2,3) + 5*Power(t2,4)) + 
            s*(90 + (-183 + 422*s2)*t2 + 
               (215 + 223*Power(s2,2))*Power(t2,2) + 
               (-420 + 244*s2 - 55*Power(s2,2))*Power(t2,3) + 
               (-646 + 8*s2 + 33*Power(s2,2))*Power(t2,4) + 
               (-53 - 2*s2 + 15*Power(s2,2))*Power(t2,5) + 5*Power(t2,6)\
) + Power(s,2)*t2*(195 + 86*t2 + 244*Power(t2,2) + 226*Power(t2,3) + 
               Power(t2,4) + s2*
                (-90 + 271*t2 + 289*Power(t2,2) - 47*Power(t2,3) - 
                  15*Power(t2,4)))) - 
         8*Power(t2,3)*(Power(s,3)*t2*
             (1 - 7*t2 - 11*Power(t2,2) + 7*Power(t2,3) + 2*Power(t2,4)) \
- 2*Power(s2,2)*t2*(-3 - 5*t2 + Power(t2,2) + 29*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,3)*(t2 - 3*Power(t2,2) - 7*Power(t2,3) + 
               3*Power(t2,4) - 2*Power(t2,5)) - 
            2*(6 + 9*t2 + 6*Power(t2,2) - 36*Power(t2,3) + 
               4*Power(t2,4) + 11*Power(t2,5)) + 
            s2*(9 + 5*t2 + 22*Power(t2,2) + 4*Power(t2,3) + 
               83*Power(t2,4) + 7*Power(t2,5) - 2*Power(t2,6)) + 
            s*(9 + (9 + 44*s2 - 9*Power(s2,2))*t2 + 
               (62 + 52*s2 + 39*Power(s2,2))*Power(t2,2) + 
               (-56 + 248*s2 + 27*Power(s2,2))*Power(t2,3) + 
               (7 + 156*s2 + 9*Power(s2,2))*Power(t2,4) + 
               (31 + 12*s2 + 6*Power(s2,2))*Power(t2,5) + 2*Power(t2,6)) \
- Power(s,2)*t2*(-6 + 38*t2 + 30*Power(t2,2) + 42*Power(t2,3) + 
               8*Power(t2,4) + 
               s2*(9 - 35*t2 - 7*Power(t2,2) + 19*Power(t2,3) + 
                  6*Power(t2,4)))) + 
         Power(s1,10)*(Power(s2,3)*
             (-3 + 28*Power(t1,2) - 8*t2 + 6*Power(t2,2) - 
               t1*(4 + 31*t2)) + 
            t1*(-63*Power(t1,4) + 2*(1 - 3*t2)*t2 + 
               Power(t1,3)*(75 + 67*t2) + 
               Power(s,2)*(t1 - 5*t1*t2 + 3*(-1 + t2)*t2) + 
               t1*(11 - 29*t2 - 30*Power(t2,2)) + 
               Power(t1,2)*(-11 + 32*t2 - 14*Power(t2,2)) + 
               s*(3 + 17*Power(t1,3) + 3*t2 - 8*Power(t2,2) + 
                  Power(t1,2)*(-28 + 9*t2) + 
                  t1*(2 - 14*t2 - 5*Power(t2,2)))) + 
            Power(s2,2)*(3 + 17*Power(t1,3) + 
               Power(t1,2)*(20 + 8*s - 11*t2) + (8 - 7*s)*t2 + 
               (1 - 9*s)*Power(t2,2) + 
               t1*(s*(-5 + 24*t2) + 3*(-10 - 7*t2 + Power(t2,2)))) + 
            s2*(3 + 2*Power(t1,4) - 2*Power(t1,3)*(37 + 4*s - 7*t2) + 
               Power(t2,2) + 3*Power(s,2)*Power(t2,2) - 
               3*s*t2*(1 + t2) + 
               Power(t1,2)*(20 + 11*t2 - 3*Power(t2,2) + 
                  2*s*(-7 + 15*t2)) + 
               t1*(-(Power(s,2)*t2) + s*(4 + 10*t2 - 8*Power(t2,2)) + 
                  2*(-4 + 12*t2 + Power(t2,2))))) + 
         Power(s1,9)*(-1 - 133*Power(t1,6) - Power(t2,2) + 
            2*Power(t2,3) + 4*s*Power(t2,3) - 3*Power(s,2)*Power(t2,3) + 
            Power(s,3)*Power(t2,3) + Power(t1,5)*(239 + 63*s + 192*t2) - 
            Power(t1,4)*(38 - 65*t2 + 66*Power(t2,2) + 
               s*(125 + 36*t2)) + 
            Power(t1,3)*(96 + Power(s,2)*(2 - 24*t2) - 319*t2 - 
               249*Power(t2,2) + 6*Power(t2,3) + 
               s*(21 - 102*t2 + 17*Power(t2,2))) + 
            Power(s2,3)*(1 + 56*Power(t1,3) + 22*t2 + 41*Power(t2,2) - 
               4*Power(t2,3) - Power(t1,2)*(1 + 105*t2) + 
               t1*(-18 - 58*t2 + 45*Power(t2,2))) + 
            Power(t1,2)*(-22 + 7*t2 + Power(s,3)*t2 + 9*Power(t2,2) + 
               42*Power(t2,3) + 
               Power(s,2)*(6 - 19*t2 + 30*Power(t2,2)) - 
               s*(-34 - 88*t2 + 33*Power(t2,2) + Power(t2,3))) + 
            t1*(4 - 23*t2 + 28*Power(t2,2) - 2*Power(s,3)*Power(t2,2) + 
               30*Power(t2,3) - 
               4*Power(s,2)*t2*(-2 - 2*t2 + Power(t2,2)) + 
               s*(-5 - 12*t2 + 30*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s2,2)*(9 + 63*Power(t1,4) + 
               Power(t1,3)*(26 + 28*s - 47*t2) - (-30 + s)*t2 + 
               (1 - 18*s)*Power(t2,2) + (-3 + 9*s)*Power(t2,3) - 
               t1*(-3 + s - 52*t2 + 44*s*t2 - 56*Power(t2,2) + 
                  72*s*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(-116 - 144*t2 + 19*Power(t2,2) + 
                  17*s*(-2 + 5*t2))) - 
            s2*(3 + 42*Power(t1,5) + Power(t1,4)*(202 + 27*s - 122*t2) + 
               (4 - 8*s)*t2 + (13 - 9*s + 8*Power(s,2))*Power(t2,2) + 
               (1 - 8*s + 6*Power(s,2))*Power(t2,3) + 
               Power(t1,3)*(-118 + s*(5 - 111*t2) - 145*t2 + 
                  64*Power(t2,2)) + 
               Power(t1,2)*(57 - 223*t2 + 65*Power(t2,2) - 
                  5*Power(t2,3) + Power(s,2)*(2 + 9*t2) + 
                  s*(-5 - 52*t2 + 62*Power(t2,2))) + 
               t1*(-36 + 6*t2 + Power(s,2)*(6 - 29*t2)*t2 + 
                  43*Power(t2,2) - 6*Power(t2,3) + 
                  s*(-16 + 46*t2 + 87*Power(t2,2) - 7*Power(t2,3))))) + 
         Power(s1,7)*(3 - 147*Power(t1,8) + 14*t2 + 5*s*t2 - 
            11*Power(t2,2) - 10*s*Power(t2,2) - 18*Power(t2,3) + 
            14*s*Power(t2,3) + 15*Power(s,2)*Power(t2,3) + 
            2*Power(s,3)*Power(t2,3) + 22*Power(t2,4) - 
            8*s*Power(t2,4) + 37*Power(s,2)*Power(t2,4) - 
            10*Power(s,3)*Power(t2,4) + 14*Power(t2,5) - s*Power(t2,5) - 
            Power(s,2)*Power(t2,5) + 5*Power(t1,7)*(87 + 35*s + 58*t2) - 
            Power(t1,6)*(18 - 55*t2 + 100*Power(t2,2) + 
               11*s*(49 + 30*t2)) - 
            Power(t1,5)*(-513 + 2211*t2 + 1301*Power(t2,2) + 
               28*Power(t2,3) + Power(s,2)*(3 + 22*t2) + 
               s*(-173 + 430*t2 - 345*Power(t2,2))) + 
            Power(t1,4)*(-308 - 272*t2 + 2035*Power(t2,2) + 
               631*Power(t2,3) + 8*Power(t2,4) + 
               3*Power(s,3)*(4 + t2) + 
               Power(s,2)*(80 + 27*t2 + 51*Power(t2,2)) + 
               s*(55 + 2113*t2 + 757*Power(t2,2) - 162*Power(t2,3))) + 
            Power(s2,3)*(56*Power(t1,5) + Power(t1,4)*(55 - 245*t2) + 
               Power(t1,3)*(-60 - 312*t2 + 263*Power(t2,2)) + 
               Power(t1,2)*(-10 + 95*t2 + 673*Power(t2,2) - 
                  89*Power(t2,3)) + 
               t2*(-15 - 148*t2 - 122*Power(t2,2) + 44*Power(t2,3)) + 
               t1*(10 + 123*t2 + 56*Power(t2,2) - 375*Power(t2,3) + 
                  7*Power(t2,4))) + 
            Power(t1,3)*(14 - 969*t2 + 2465*Power(t2,2) + 
               1018*Power(t2,3) - 38*Power(t2,4) - 
               7*Power(s,3)*t2*(-2 + 7*t2) + 
               Power(s,2)*(-42 + 157*t2 + 152*Power(t2,2) - 
                  28*Power(t2,3)) + 
               s*(-45 - 520*t2 + 271*Power(t2,2) - 540*Power(t2,3) + 
                  20*Power(t2,4))) + 
            Power(t1,2)*(-45 + 385*t2 + 450*Power(t2,2) - 
               1453*Power(t2,3) - 457*Power(t2,4) + 
               Power(s,3)*t2*(-16 - 4*t2 + 41*Power(t2,2)) + 
               Power(s,2)*(-2 - 76*t2 - 129*Power(t2,2) - 
                  313*Power(t2,3) + 3*Power(t2,4)) + 
               s*(-94 - 77*t2 - 1096*Power(t2,2) - 200*Power(t2,3) + 
                  139*Power(t2,4))) + 
            t1*(-5 - 44*t2 + 302*Power(t2,2) - 406*Power(t2,3) - 
               201*Power(t2,4) + 18*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(2 + 12*t2 - 7*Power(t2,2)) + 
               Power(s,2)*t2*
                (-8 - 97*t2 + 13*Power(t2,2) + 74*Power(t2,3)) + 
               s*(11 + 58*t2 + 137*Power(t2,2) - 326*Power(t2,3) - 
                  5*Power(t2,4) - 4*Power(t2,5))) + 
            Power(s2,2)*(-6 + 175*Power(t1,6) + 
               Power(t1,5)*(-170 + 70*s - 139*t2) - 2*(45 + s)*t2 - 
               (77 + 24*s)*Power(t2,2) + (22 - 48*s)*Power(t2,3) + 
               (56 - 98*s)*Power(t2,4) - Power(t2,5) - 
               Power(t1,4)*(288 + s*(154 - 213*t2) + 690*t2 + 
                  35*Power(t2,2)) + 
               t1*(2 + 19*(-7 + 3*s)*t2 + (-870 + 316*s)*Power(t2,2) + 
                  (-567 + 573*s)*Power(t2,3) + (52 - 21*s)*Power(t2,4)) \
+ Power(t1,3)*(-142 + 884*t2 + 800*Power(t2,2) + 13*Power(t2,3) - 
                  s*(6 + 170*t2 + 485*Power(t2,2))) + 
               Power(t1,2)*(152 + 1015*t2 + 562*Power(t2,2) - 
                  268*Power(t2,3) + 3*Power(t2,4) + 
                  s*(25 + 232*t2 - 401*Power(t2,2) + 219*Power(t2,3)))) \
+ s2*(3 - 224*Power(t1,7) + (48 - 14*s)*t2 + 
               (64 - 111*s + 2*Power(s,2))*Power(t2,2) + 
               (131 - 180*s + 83*Power(s,2))*Power(t2,3) + 
               (-43 - 112*s + 64*Power(s,2))*Power(t2,4) + 
               (5 + 2*s)*Power(t2,5) + 
               Power(t1,6)*(-166 - 49*s + 640*t2) + 
               Power(t1,5)*(408 + 963*t2 - 570*Power(t2,2) + 
                  s*(257 + 189*t2)) - 
               Power(t1,4)*(150 - 604*t2 + 2043*Power(t2,2) - 
                  200*Power(t2,3) + Power(s,2)*(67 + 41*t2) + 
                  s*(214 + 164*t2 + 65*Power(t2,2))) + 
               Power(t1,3)*(247 - 1118*t2 - 1542*Power(t2,2) + 
                  1246*Power(t2,3) - 20*Power(t2,4) + 
                  Power(s,2)*(18 - 120*t2 + 271*Power(t2,2)) + 
                  s*(268 - 1081*t2 - 1040*Power(t2,2) + 15*Power(t2,3))) \
+ t1*(-66 - 404*t2 + 143*Power(t2,2) + 737*Power(t2,3) - 
                  238*Power(t2,4) + 4*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-4 + 80*t2 - 210*Power(t2,2) + 21*Power(t2,3)) + 
                  s*(-13 - 137*t2 + 778*Power(t2,2) + 514*Power(t2,3) - 
                     126*Power(t2,4))) + 
               Power(t1,2)*(104 + 146*t2 - 808*Power(t2,2) + 
                  1244*Power(t2,3) - 219*Power(t2,4) + 
                  Power(s,2)*t2*(155 - 25*t2 - 171*Power(t2,2)) + 
                  s*(-35 + 145*t2 + 848*Power(t2,2) + 705*Power(t2,3) - 
                     6*Power(t2,4))))) + 
         Power(s1,8)*(1 - 175*Power(t1,7) - s*t2 + 12*Power(t2,2) + 
            s*Power(t2,2) - 9*Power(t2,3) - 18*s*Power(t2,3) + 
            3*Power(s,2)*Power(t2,3) - 2*Power(s,3)*Power(t2,3) - 
            10*Power(t2,4) - 5*s*Power(t2,4) + 
            4*Power(s,2)*Power(t2,4) - Power(s,3)*Power(t2,4) + 
            Power(t1,6)*(419 + 133*s + 305*t2) - 
            Power(t1,5)*(54 + 329*s - 66*t2 + 181*s*t2 + 
               120*Power(t2,2)) + 
            Power(t1,4)*(309 - 1200*t2 - 792*Power(t2,2) + 
               8*Power(t2,3) - Power(s,2)*(3 + 41*t2) + 
               s*(86 - 292*t2 + 155*Power(t2,2))) + 
            Power(t1,3)*(-134 - 46*t2 + 557*Power(t2,2) + 
               285*Power(t2,3) + Power(s,3)*(2 + 4*t2) + 
               Power(s,2)*(37 - 28*t2 + 78*Power(t2,2)) + 
               s*(88 + 652*t2 + 124*Power(t2,2) - 47*Power(t2,3))) + 
            Power(s2,3)*(2 + 70*Power(t1,4) + 
               Power(t1,3)*(22 - 203*t2) + 28*t2 - 7*Power(t2,2) - 
               65*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(-45 - 181*t2 + 146*Power(t2,2)) + 
               t1*(1 + 84*t2 + 255*Power(t2,2) - 29*Power(t2,3))) + 
            Power(t1,2)*(37 - 278*t2 + Power(s,3)*(4 - 17*t2)*t2 + 
               530*Power(t2,2) + 340*Power(t2,3) - 18*Power(t2,4) + 
               Power(s,2)*(-12 + 62*t2 + 71*Power(t2,2) - 
                  26*Power(t2,3)) + 
               s*(-25 - 154*t2 + 254*Power(t2,2) - 22*Power(t2,3) + 
                  2*Power(t2,4))) + 
            t1*(-14 + 27*t2 + 22*Power(t2,2) - 46*Power(t2,3) + 
               10*Power(s,3)*Power(t2,3) - 42*Power(t2,4) + 
               Power(s,2)*t2*
                (-6 - 2*t2 - 70*Power(t2,2) + Power(t2,3)) + 
               s*(-11 - 18*t2 - 65*Power(t2,2) + 32*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s2,2)*(-3 + 133*Power(t1,5) + 
               2*Power(t1,4)*(-20 + 28*s - 53*t2) + (-57 + 13*s)*t2 + 
               6*(-17 + 14*s)*Power(t2,2) + (-33 + 90*s)*Power(t2,3) - 
               3*(-1 + s)*Power(t2,4) + 
               Power(t1,3)*(-237 - 420*t2 + 29*Power(t2,2) + 
                  2*s*(-49 + 86*t2)) - 
               Power(t1,2)*(43 - 273*t2 - 326*Power(t2,2) + 
                  13*Power(t2,3) + 2*s*(2 + 57*t2 + 126*Power(t2,2))) + 
               t1*(60 + 304*t2 + 165*Power(t2,2) - 78*Power(t2,3) + 
                  Power(t2,4) + 
                  s*(5 + 46*t2 - 130*Power(t2,2) + 68*Power(t2,3)))) + 
            s2*(-9 - 140*Power(t1,6) - 4*(9 + s)*t2 + 
               (4 + 48*s + 6*Power(s,2))*Power(t2,2) + 
               (27 + 39*s - 23*Power(s,2))*Power(t2,3) + 
               (-3 - 7*s + 3*Power(s,2))*Power(t2,4) + 
               Power(t1,5)*(-275 - 49*s + 382*t2) + 
               Power(t1,4)*(294 + 519*t2 - 285*Power(t2,2) + 
                  s*(93 + 203*t2)) - 
               Power(t1,3)*(138 - 630*t2 + 616*Power(t2,2) - 
                  65*Power(t2,3) + Power(s,2)*(18 + 29*t2) + 
                  s*(58 - 53*t2 + 142*Power(t2,2))) + 
               Power(t1,2)*(133 - 244*t2 - 550*Power(t2,2) + 
                  214*Power(t2,3) - 2*Power(t2,4) + 
                  Power(s,2)*(2 - 45*t2 + 123*Power(t2,2)) + 
                  s*(107 - 332*t2 - 476*Power(t2,2) + 39*Power(t2,3))) + 
               t1*(6 + 23*t2 - 292*Power(t2,2) + 94*Power(t2,3) - 
                  10*Power(t2,4) + 
                  Power(s,2)*t2*(18 - 32*t2 - 49*Power(t2,2)) - 
                  2*s*(5 - 5*t2 - 50*Power(t2,2) - 90*Power(t2,3) + 
                     Power(t2,4))))) + 
         s1*(Power(-1 + s,2)*Power(t1,11) + 
            Power(t1,4)*(10 - 
               5*(74 + 36*Power(s,2) + 39*s2 - 3*s*(49 + 2*s2))*t2 - 
               (282 + 180*Power(s,3) + 167*s2 + 329*Power(s2,2) + 
                  24*Power(s2,3) + Power(s,2)*(-542 + 540*s2) - 
                  3*s*(227 + 289*s2 + 60*Power(s2,2)))*Power(t2,2) + 
               (355 - 465*Power(s,3) + 809*s2 + 86*Power(s2,2) - 
                  156*Power(s2,3) + Power(s,2)*(-169 + 862*s2) + 
                  s*(2039 + 351*s2 + 615*Power(s2,2)))*Power(t2,3) + 
               (2603 + 194*Power(s,3) + Power(s,2)*(596 - 418*s2) + 
                  2398*s2 - 77*Power(s2,2) + 54*Power(s2,3) + 
                  s*(-2986 + 1685*s2 + 230*Power(s2,2)))*Power(t2,4) + 
               (367 + 11*Power(s,3) + 926*s2 + 10*Power(s2,2) + 
                  2*Power(s2,3) - Power(s,2)*(391 + 20*s2) + 
                  s*(342 + 419*s2 + 7*Power(s2,2)))*Power(t2,5) - 
               (123 + 34*Power(s,2) + 155*s2 + 34*Power(s2,2) - 
                  s*(101 + 68*s2))*Power(t2,6) - 32*Power(t2,7)) - 
            4*Power(t2,2)*(15 + 
               3*(10 + 3*Power(s,2) + 26*s2 - 18*s*s2 - 9*Power(s2,2))*
                t2 + (1 - 4*Power(s,3) + 148*s2 + 52*Power(s2,2) - 
                  14*Power(s2,3) + 2*Power(s,2)*(4 + 5*s2) - 
                  2*s*(3 + 46*s2 + 36*Power(s2,2)))*Power(t2,2) + 
               2*(-63 + 11*Power(s,3) + Power(s,2)*(40 - 80*s2) + 
                  67*s2 + 54*Power(s2,2) + 22*Power(s2,3) + 
                  s*(47 - 214*s2 - 65*Power(s2,2)))*Power(t2,3) + 
               (5 + 16*Power(s,3) - 298*s2 + 378*Power(s2,2) + 
                  26*Power(s2,3) - 2*Power(s,2)*(29 + 11*s2) - 
                  2*s*(-95 + 328*s2 + 18*Power(s2,2)))*Power(t2,4) - 
               (-102 + 2*Power(s,3) + 28*s2 - 103*Power(s2,2) - 
                  24*Power(s2,3) - 7*Power(s,2)*(-7 + 4*s2) + 
                  2*s*(91 + 7*s2 + 25*Power(s2,2)))*Power(t2,5) - 
               (37 + 6*Power(s,2) - 94*s2 + 6*Power(s2,2) - 
                  4*s*(-8 + 3*s2))*Power(t2,6) + 10*Power(t2,7)) + 
            2*Power(t1,2)*t2*
             (-30 + (-39 + 72*Power(s,2) + 48*s2 - 54*Power(s2,2) + 
                  s*(-339 + 54*s2))*t2 + 
               (-290 + 34*Power(s,3) - 453*s2 + 136*Power(s2,2) - 
                  5*Power(s2,3) + Power(s,2)*(-148 + 287*s2) - 
                  2*s*(125 + 106*s2 + 50*Power(s2,2)))*Power(t2,2) + 
               (-1066 + 239*Power(s,3) + Power(s,2)*(329 - 412*s2) - 
                  1015*s2 - 135*Power(s2,2) + 178*Power(s2,3) - 
                  s*(667 + 738*s2 + 661*Power(s2,2)))*Power(t2,3) - 
               (1774 + 132*Power(s,3) + 665*s2 - 959*Power(s2,2) + 
                  69*Power(s2,3) - Power(s,2)*(627 + 251*s2) + 
                  s*(-415 + 3082*s2 + 122*Power(s2,2)))*Power(t2,4) + 
               (617 - 29*Power(s,3) + 755*s2 + 165*Power(s2,2) + 
                  32*Power(s2,3) + Power(s,2)*(407 + 90*s2) - 
                  s*(1726 + 588*s2 + 93*Power(s2,2)))*Power(t2,5) + 
               (182 + 33*Power(s,2) + 274*s2 + 33*Power(s2,2) - 
                  s*(185 + 66*s2))*Power(t2,6) + 32*Power(t2,7)) - 
            Power(t1,10)*(2 + s2 - 4*t2 + Power(s,2)*(3 + s2 + 4*t2) - 
               s*(5 + 2*s2 + 4*t2)) - 
            Power(t1,9)*(-1 + 22*t2 + Power(s2,2)*(-2 + t2)*t2 + 
               16*Power(t2,2) + Power(s,3)*(-9 + 2*t2) + 
               Power(s,2)*(12 + (15 - 4*s2)*t2 - 8*Power(t2,2)) + 
               s2*(1 + 10*t2 + 17*Power(t2,2)) + 
               s*(2*Power(s2,2)*t2 - t2*(32 + 9*t2) + 
                  s2*(-1 + 5*t2 + 4*Power(t2,2)))) + 
            Power(t1,8)*(13 + 15*t2 - 9*Power(t2,2) - 
               Power(s2,3)*Power(t2,2) + 15*Power(t2,3) + 
               Power(s2,2)*t2*(8 + 15*t2 - 2*Power(t2,2)) + 
               Power(s,3)*(-19 - 16*t2 + 7*Power(t2,2)) + 
               Power(s,2)*(72 + s2 + 50*t2 + 50*s2*t2 + 
                  78*Power(t2,2) - 16*s2*Power(t2,2) - 10*Power(t2,3)) \
+ s2*(15 + 24*t2 + 60*Power(t2,2) + 59*Power(t2,3)) + 
               s*(-60 - 11*t2 - 131*Power(t2,2) - 28*Power(t2,3) + 
                  Power(s2,2)*t2*(-3 + 10*t2) + 
                  2*s2*(-7 - 29*t2 - 2*Power(t2,2) + 6*Power(t2,3)))) + 
            Power(t1,7)*(-50 - 7*t2 + 161*Power(t2,2) + 
               219*Power(t2,3) - 15*Power(t2,4) + 
               Power(s2,3)*Power(t2,2)*(-7 + 6*t2) + 
               Power(s,3)*(10 - 53*t2 + 25*Power(t2,2) - 
                  9*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-32 - 71*t2 - 4*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(-29 - 39*t2 + 122*Power(t2,2) + 14*Power(t2,3) - 
                  34*Power(t2,4)) + 
               Power(s,2)*(-88 - 11*(1 + 9*s2)*t2 + 
                  (50 - 83*s2)*Power(t2,2) + 
                  9*(-14 + 3*s2)*Power(t2,3) + 7*Power(t2,4)) + 
               s*(140 + 79*t2 - 98*Power(t2,2) + 43*Power(t2,3) + 
                  3*Power(t2,4) + 
                  Power(s2,2)*t2*(16 + 63*t2 - 24*Power(t2,2)) + 
                  s2*(17 + 145*t2 + 64*Power(t2,2) + 78*Power(t2,3) - 
                     12*Power(t2,4)))) + 
            Power(t1,5)*(-44 + 173*t2 - 286*Power(t2,2) - 
               657*Power(t2,3) - 808*Power(t2,4) + 272*Power(t2,5) + 
               70*Power(t2,6) + 
               Power(s2,3)*Power(t2,2)*
                (-3 + 11*t2 - 11*Power(t2,2) + Power(t2,3)) - 
               Power(s,3)*t2*
                (60 - 30*t2 + 101*Power(t2,2) - 58*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*t2*
                (-10 + 212*t2 + 445*Power(t2,2) + 112*Power(t2,3) + 
                  69*Power(t2,4)) + 
               s2*(-6 + 189*t2 + 93*Power(t2,2) - 643*Power(t2,3) + 
                  1369*Power(t2,4) + 618*Power(t2,5) + 20*Power(t2,6)) \
- s*(-30 + (841 + 52*s2 - 6*Power(s2,2))*t2 + 
                  (53 + 777*s2 + 102*Power(s2,2))*Power(t2,2) + 
                  (167 - 178*s2 + 305*Power(s2,2))*Power(t2,3) + 
                  (1325 + 479*s2 - 118*Power(s2,2))*Power(t2,4) + 
                  (284 + 110*s2 + 3*Power(s2,2))*Power(t2,5) + 
                  20*Power(t2,6)) + 
               Power(s,2)*t2*
                (600 + 275*t2 - 5*Power(t2,2) + 709*Power(t2,3) + 
                  41*Power(t2,4) + 
                  s2*(-30 + 819*t2 + 407*Power(t2,2) - 
                     165*Power(t2,3) + 3*Power(t2,4)))) - 
            2*Power(t1,3)*t2*
             (-130 - 267*t2 - 729*Power(t2,2) - 101*Power(t2,3) - 
               227*Power(t2,4) + 196*Power(t2,5) + 74*Power(t2,6) + 
               Power(s2,3)*t2*
                (-6 - 63*t2 + 22*Power(t2,2) + 57*Power(t2,3) - 
                  10*Power(t2,4)) + 
               Power(s,3)*t2*
                (-30 - 69*t2 - 138*Power(t2,2) + 83*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-149 + 180*t2 + 791*Power(t2,2) + 90*Power(t2,3) + 
                  112*Power(t2,4)) + 
               s2*(-30 + 54*t2 - 140*Power(t2,2) - 83*Power(t2,3) + 
                  3107*Power(t2,4) + 825*Power(t2,5) - 5*Power(t2,6)) + 
               s*(90 + (-344 + 226*s2 + 30*Power(s2,2))*t2 + 
                  (856 - 114*s2 + 33*Power(s2,2))*Power(t2,2) + 
                  (-1165 + 784*s2 - 390*Power(s2,2))*Power(t2,3) + 
                  (-2531 - 210*s2 + 57*Power(s2,2))*Power(t2,4) + 
                  (-215 - 142*s2 + 30*Power(s2,2))*Power(t2,5) + 
                  5*Power(t2,6)) + 
               Power(s,2)*t2*
                (s2*(-90 + 963*t2 + 506*Power(t2,2) - 
                     197*Power(t2,3) - 30*Power(t2,4)) + 
                  3*(145 - 62*t2 + 67*Power(t2,2) + 208*Power(t2,3) + 
                     10*Power(t2,4)))) + 
            4*t1*Power(t2,2)*
             (3 - 60*t2 - 43*Power(t2,2) + 490*Power(t2,3) - 
               197*Power(t2,4) - 278*Power(t2,5) + 21*Power(t2,6) + 
               Power(s,3)*t2*
                (1 - 53*t2 - 27*Power(t2,2) + 57*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-93 + 262*t2 + 306*Power(t2,2) - 218*Power(t2,3) + 
                  47*Power(t2,4)) + 
               Power(s2,3)*(t2 - 69*Power(t2,2) + 25*Power(t2,3) + 
                  81*Power(t2,4) - 14*Power(t2,5)) + 
               s2*(9 + 281*t2 + 384*Power(t2,2) + 270*Power(t2,3) + 
                  1029*Power(t2,4) + 185*Power(t2,5) - 14*Power(t2,6)) \
+ s*(9 + (-63 + 14*s2 - 9*Power(s2,2))*t2 + 
                  (276 - 8*s2 + 61*Power(s2,2))*Power(t2,2) + 
                  (-786 + 1188*s2 - 149*Power(s2,2))*Power(t2,3) + 
                  (-411 + 704*s2 - 33*Power(s2,2))*Power(t2,4) + 
                  (209 - 10*s2 + 42*Power(s2,2))*Power(t2,5) + 
                  14*Power(t2,6)) - 
               Power(s,2)*t2*
                (-87 + 262*t2 + 206*Power(t2,2) + 206*Power(t2,3) + 
                  37*Power(t2,4) + 
                  s2*(9 - 413*t2 - 135*Power(t2,2) + 105*Power(t2,3) + 
                     42*Power(t2,4)))) + 
            Power(t1,6)*(71 + 7*t2 + 24*Power(t2,2) - 374*Power(t2,3) - 
               205*Power(t2,4) - 27*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(23 + 7*t2 - 7*Power(t2,2)) + 
               Power(s,3)*t2*
                (125 + 147*t2 - 67*Power(t2,2) + 5*Power(t2,3)) - 
               Power(s2,2)*t2*
                (-32 + 16*t2 + 77*Power(t2,2) + 35*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s,2)*(-30 + (448 - 73*s2)*t2 + 
                  3*(44 + 115*s2)*Power(t2,2) + 
                  (412 - 158*s2)*Power(t2,3) + 
                  (-59 + 17*s2)*Power(t2,4) + 2*Power(t2,5)) - 
               s2*(-22 + 29*t2 + 115*Power(t2,2) + 649*Power(t2,3) + 
                  581*Power(t2,4) + 28*Power(t2,5)) + 
               s*(-113 + 182*t2 - 487*Power(t2,2) + 1036*Power(t2,3) + 
                  244*Power(t2,4) + 34*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-17 - 79*t2 - 105*Power(t2,2) + 19*Power(t2,3)) + 
                  s2*(-6 - 60*t2 + 150*Power(t2,2) - 188*Power(t2,3) - 
                     29*Power(t2,4) + 4*Power(t2,5))))) + 
         Power(s1,6)*(-1 - 77*Power(t1,9) - 13*t2 + s*t2 - 
            3*Power(t2,2) + 15*s*Power(t2,2) + 
            2*Power(s,2)*Power(t2,2) - 116*Power(t2,3) + 
            14*s*Power(t2,3) - 9*Power(s,2)*Power(t2,3) + 
            120*Power(t2,4) + 154*s*Power(t2,4) - 
            75*Power(s,2)*Power(t2,4) + 20*Power(s,3)*Power(t2,4) + 
            43*Power(t2,5) + 10*s*Power(t2,5) - 
            48*Power(s,2)*Power(t2,5) + 11*Power(s,3)*Power(t2,5) - 
            6*Power(t2,6) + 2*s*Power(t2,6) + 
            Power(t1,8)*(265 + 147*s + 165*t2) + 
            Power(t1,7)*(34 + 76*t2 - 30*Power(t2,2) - 
               s*(553 + 321*t2)) + 
            Power(t1,6)*(507 - 2200*t2 - 1214*Power(t2,2) - 
               72*Power(t2,3) + Power(s,2)*(31 + 15*t2) + 
               3*s*(56 - 120*t2 + 115*Power(t2,2))) + 
            Power(t1,5)*(-356 - 10*Power(s,3)*(-3 + t2) - 597*t2 + 
               2929*Power(t2,2) + 582*Power(t2,3) + 24*Power(t2,4) + 
               Power(s,2)*(52 + 96*t2 - 75*Power(t2,2)) + 
               s*(-82 + 3645*t2 + 1428*Power(t2,2) - 198*Power(t2,3))) + 
            Power(s2,3)*(28*Power(t1,6) + Power(t1,5)*(64 - 189*t2) + 
               5*Power(t1,4)*(-9 - 63*t2 + 56*Power(t2,2)) - 
               Power(t1,3)*(30 + 27*t2 - 966*Power(t2,2) + 
                  142*Power(t2,3)) + 
               t1*t2*(-15 - 401*t2 - 645*Power(t2,2) + 
                  237*Power(t2,3)) + 
               t2*(-20 - 67*t2 + 209*Power(t2,2) + 232*Power(t2,3) - 
                  11*Power(t2,4)) + 
               Power(t1,2)*(20 + 225*t2 + 294*Power(t2,2) - 
                  884*Power(t2,3) + 20*Power(t2,4))) + 
            Power(t1,4)*(Power(s,3)*(4 + 8*t2 - 60*Power(t2,2)) + 
               Power(s,2)*(-35 + 121*t2 + 79*Power(t2,2) + 
                  62*Power(t2,3)) + 
               2*s*(-43 - 426*t2 - 458*Power(t2,2) - 756*Power(t2,3) + 
                  20*Power(t2,4)) + 
               3*(-61 - 595*t2 + 1623*Power(t2,2) + 512*Power(t2,3) + 
                  38*Power(t2,4))) + 
            Power(t1,3)*(-5 + 1426*t2 + 1455*Power(t2,2) - 
               6162*Power(t2,3) - 1283*Power(t2,4) - 40*Power(t2,5) + 
               Power(s,3)*(-4 - 86*t2 + 11*Power(t2,2) + 
                  78*Power(t2,3)) - 
               Power(s,2)*(6 + 328*t2 + 461*Power(t2,2) + 
                  396*Power(t2,3) + 12*Power(t2,4)) + 
               s*(-128 - 201*t2 - 4877*Power(t2,2) - 900*Power(t2,3) + 
                  641*Power(t2,4))) + 
            t1*(24 + 158*t2 - 350*Power(t2,2) - 650*Power(t2,3) + 
               1291*Power(t2,4) + 323*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(10 - 4*t2 - 85*Power(t2,2)) + 
               Power(s,2)*t2*
                (13 + 84*t2 + 374*Power(t2,2) + 364*Power(t2,3) - 
                  13*Power(t2,4)) + 
               s*(8 + 133*t2 - 249*Power(t2,2) + 773*Power(t2,3) + 
                  220*Power(t2,4) - 137*Power(t2,5))) + 
            Power(t1,2)*(-60 - 20*t2 + 998*Power(t2,2) - 
               2700*Power(t2,3) - 198*Power(t2,4) + 66*Power(t2,5) + 
               Power(s,3)*t2*
                (-2 + 6*t2 + 93*Power(t2,2) - 20*Power(t2,3)) + 
               Power(s,2)*(13 - 23*t2 - 424*Power(t2,2) + 
                  128*Power(t2,3) + 175*Power(t2,4)) + 
               s*(50 + 306*t2 + 722*Power(t2,2) + 161*Power(t2,3) + 
                  633*Power(t2,4) - 70*Power(t2,5))) + 
            Power(s2,2)*(147*Power(t1,7) + 
               2*Power(t1,6)*(-118 + 28*s - 53*t2) + 
               Power(t1,2)*(55 + 10*(-2 + 5*s)*t2 + 
                  10*(-301 + 47*s)*Power(t2,2) + 
                  3*(-606 + 493*s)*Power(t2,3) + 
                  (40 - 60*s)*Power(t2,4)) - 
               t1*(30 + (452 + 43*s)*t2 + 
                  2*(563 + 137*s)*Power(t2,2) + 
                  (-115 + 13*s)*Power(t2,3) + 
                  (-579 + 559*s)*Power(t2,4) + 13*Power(t2,5)) - 
               Power(t1,5)*(223 + 720*t2 + 155*Power(t2,2) - 
                  2*s*(-70 + 79*t2)) + 
               Power(t1,4)*(-182 + 1611*t2 + 1027*Power(t2,2) + 
                  119*Power(t2,3) - s*(5 + 192*t2 + 535*Power(t2,2))) + 
               Power(t1,3)*(189 + 1543*t2 + 699*Power(t2,2) - 
                  215*Power(t2,3) - 12*Power(t2,4) + 
                  s*(50 + 456*t2 - 635*Power(t2,2) + 362*Power(t2,3))) \
+ t2*(40 + 371*t2 + 487*Power(t2,2) + 243*Power(t2,3) - 
                  39*Power(t2,4) + 
                  s*(-6 - 122*t2 - 326*Power(t2,2) - 270*Power(t2,3) + 
                     33*Power(t2,4)))) + 
            s2*(6 - 210*Power(t1,8) + (96 + 7*s)*t2 + 
               (160 + 54*s - 7*Power(s,2))*Power(t2,2) - 
               (77 + 255*s + 77*Power(s,2))*Power(t2,3) + 
               (-331 - 89*s + 18*Power(s,2))*Power(t2,4) + 
               (88 + 87*s - 33*Power(s,2))*Power(t2,5) - 2*Power(t2,6) + 
               Power(t1,7)*(19 - 21*s + 634*t2) + 
               Power(t1,6)*(360 + 1083*t2 - 605*Power(t2,2) + 
                  s*(317 + 63*t2)) - 
               Power(t1,5)*(90 + 307*t2 + 3436*Power(t2,2) - 
                  250*Power(t2,3) + Power(s,2)*(136 + 15*t2) + 
                  s*(303 + 495*t2 - 195*Power(t2,2))) + 
               Power(t1,4)*(311 - 2114*t2 - 1616*Power(t2,2) + 
                  2870*Power(t2,3) - 40*Power(t2,4) + 
                  Power(s,2)*(60 - 131*t2 + 315*Power(t2,2)) - 
                  s*(-316 + 1850*t2 + 1023*Power(t2,2) + 
                     181*Power(t2,3))) + 
               Power(t1,2)*(-166 - 1256*t2 + 1782*Power(t2,2) + 
                  2190*Power(t2,3) - 2099*Power(t2,4) + 
                  70*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-49 + 258*t2 - 688*Power(t2,2) + 60*Power(t2,3)) + 
                  s*(-82 - 483*t2 + 2986*Power(t2,2) + 
                     1368*Power(t2,3) - 215*Power(t2,4))) + 
               Power(t1,3)*(186 + 469*t2 + 280*Power(t2,2) + 
                  4246*Power(t2,3) - 859*Power(t2,4) + 
                  Power(s,2)*
                   (1 + 523*t2 + 3*Power(t2,2) - 298*Power(t2,3)) + 
                  s*(-45 + 925*t2 + 2702*Power(t2,2) + 
                     813*Power(t2,3) + 24*Power(t2,4))) + 
               t1*(-7 - 16*t2 + 479*Power(t2,2) + 733*Power(t2,3) - 
                  1180*Power(t2,4) + 243*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-13 - 213*t2 + 283*Power(t2,2) + 407*Power(t2,3)) + 
                  s*(6 + 30*t2 - 478*Power(t2,2) - 2201*Power(t2,3) - 
                     1078*Power(t2,4) + 26*Power(t2,5))))) + 
         Power(s1,2)*((-3 + s - s2 + s*s2)*Power(t1,11) + 
            2*t1*t2*(30 + 3*(50 + 18*Power(s,2) - 19*s2 - 
                  24*Power(s2,2) - 2*s*(5 + 9*s2))*t2 - 
               (-161 + Power(s,3) - 432*s2 + 487*Power(s2,2) - 
                  26*Power(s2,3) + Power(s,2)*(-121 + 20*s2) + 
                  s*(407 + 58*s2 + 77*Power(s2,2)))*Power(t2,2) + 
               (-1214 + 114*Power(s,3) + Power(s,2)*(1090 - 897*s2) + 
                  593*s2 - 74*Power(s2,2) - 29*Power(s2,3) + 
                  s*(173 - 3352*s2 - 228*Power(s2,2)))*Power(t2,3) - 
               (150 + 97*Power(s,3) + 2569*s2 - 2592*Power(s2,2) + 
                  156*Power(s2,3) - 6*Power(s,2)*(48 + 65*s2) + 
                  s*(-1559 + 5200*s2 + 305*Power(s2,2)))*Power(t2,4) + 
               (1500 - 88*Power(s,3) + 20*s2 + 514*Power(s2,2) + 
                  223*Power(s2,3) + Power(s,2)*(16 + 399*s2) - 
                  s*(1779 + 434*s2 + 534*Power(s2,2)))*Power(t2,5) + 
               (-145 + 15*Power(s,2) + 781*s2 + 15*Power(s2,2) - 
                  10*s*(38 + 3*s2))*Power(t2,6) + 84*Power(t2,7)) - 
            2*Power(t1,3)*(-10 + 
               (-14 + 72*Power(s,2) + 83*s2 - 24*Power(s2,2) - 
                  s*(289 + 6*s2))*t2 + 
               (-173 + 84*Power(s,3) - 185*s2 - 57*Power(s2,2) + 
                  58*Power(s2,3) + 22*Power(s,2)*(-13 + 11*s2) + 
                  s*(-621 + 31*s2 - 180*Power(s2,2)))*Power(t2,2) + 
               (-2314 + 339*Power(s,3) + Power(s,2)*(688 - 1150*s2) - 
                  1060*s2 - 291*Power(s2,2) + 136*Power(s2,3) - 
                  s*(403 + 2237*s2 + 453*Power(s2,2)))*Power(t2,3) - 
               (2854 + 226*Power(s,3) + 3410*s2 - 1577*Power(s2,2) + 
                  248*Power(s2,3) - 18*Power(s,2)*(7 + 26*s2) + 
                  s*(-3585 + 4579*s2 + 126*Power(s2,2)))*Power(t2,4) + 
               (2164 - 49*Power(s,3) - 113*s2 + 9*Power(s2,2) + 
                  102*Power(s2,3) + Power(s,2)*(282 + 200*s2) - 
                  s*(2478 + 233*s2 + 253*Power(s2,2)))*Power(t2,5) + 
               (277 + 62*Power(s,2) + 637*s2 + 62*Power(s2,2) - 
                  2*s*(181 + 62*s2))*Power(t2,6) + 84*Power(t2,7)) + 
            Power(t1,10)*(14 - 2*Power(s,2)*(-6 + t2) + 9*t2 + 
               Power(s2,2)*t2 + 2*s2*(4 + 9*t2) - 
               s*(22 + 10*t2 + s2*(3 + t2))) + 
            Power(t1,9)*(-18 - 2*Power(s,3)*(-1 + t2) + 25*t2 - 
               17*Power(t2,2) - 3*Power(s2,2)*(1 + 5*t2) + 
               Power(s,2)*(-30 + 4*s2*(-3 + t2) - 33*t2 + 
                  6*Power(t2,2)) - s2*(14 + 43*t2 + 54*Power(t2,2)) + 
               s*(23 + Power(s2,2)*(1 - 2*t2) + 74*t2 + 
                  20*Power(t2,2) + s2*(21 + t2 - 5*Power(t2,2)))) + 
            Power(t1,8)*(25 - 203*t2 + Power(s2,3)*(3 - 2*t2)*t2 - 
               153*Power(t2,2) + 28*Power(t2,3) + 
               2*Power(s,3)*(15 - 8*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(1 + 53*t2 - 2*Power(t2,2) - 
                  3*Power(t2,3)) + 
               s2*(8 - 106*t2 - 127*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s,2)*(-5 - 97*t2 + 53*Power(t2,2) - 
                  6*Power(t2,3) + s2*(8 + 35*t2 - 14*Power(t2,2))) + 
               s*(Power(s2,2)*(-1 - 25*t2 + 10*Power(t2,2)) + 
                  2*(-30 + 94*t2 + 7*Power(t2,2) + 4*Power(t2,3)) + 
                  s2*(-2 - 51*t2 - 4*Power(t2,2) + 9*Power(t2,3)))) + 
            Power(t1,4)*(-85 - 449*t2 - 1339*Power(t2,2) - 
               2058*Power(t2,3) - 316*Power(t2,4) + 597*Power(t2,5) + 
               394*Power(t2,6) + 
               Power(s2,3)*t2*
                (-12 + 7*t2 - 33*Power(t2,2) + 204*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s,3)*t2*
                (-60 - 38*t2 - 236*Power(t2,2) + 178*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-78 + 173*t2 + 2443*Power(t2,2) + 845*Power(t2,3) + 
                  515*Power(t2,4)) + 
               s2*(-30 + 308*t2 + 22*Power(t2,2) - 457*Power(t2,3) + 
                  8123*Power(t2,4) + 3378*Power(t2,5) + 20*Power(t2,6)) \
- s*(-60 + (653 + 143*s2 - 30*Power(s2,2))*t2 + 
                  (-671 - 216*s2 + 328*Power(s2,2))*Power(t2,2) + 
                  (2424 - 119*s2 + 666*Power(s2,2))*Power(t2,3) + 
                  (8178 + 881*s2 + 64*Power(s2,2))*Power(t2,4) + 
                  (1044 + 763*s2 - 15*Power(s2,2))*Power(t2,5) + 
                  20*Power(t2,6)) + 
               Power(s,2)*t2*
                (790 - 248*t2 - 89*Power(t2,2) + 1277*Power(t2,3) + 
                  248*Power(t2,4) + 
                  s2*(-60 + 1931*t2 + 713*Power(t2,2) - 
                     318*Power(t2,3) - 15*Power(t2,4)))) + 
            Power(t1,7)*(-35 + 136*t2 + 247*Power(t2,2) + 
               152*Power(t2,3) + 37*Power(t2,4) + 
               Power(s2,3)*t2*(-7 + 4*Power(t2,2)) - 
               Power(s,3)*(50 + 66*t2 - 55*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s2,2)*(13 + 37*t2 + 117*Power(t2,2) + 
                  59*Power(t2,3) + 2*Power(t2,4)) + 
               s2*(46 + 85*t2 + 806*Power(t2,2) + 780*Power(t2,3) + 
                  29*Power(t2,4)) + 
               Power(s,2)*(182 + 194*t2 + 347*Power(t2,2) - 
                  30*Power(t2,3) + 2*Power(t2,4) + 
                  s2*(5 + 243*t2 - 121*Power(t2,2) + 16*Power(t2,3))) - 
               s*(36 - 153*t2 + 1183*Power(t2,2) + 268*Power(t2,3) + 
                  41*Power(t2,4) + 
                  Power(s2,2)*t2*(4 - 69*t2 + 14*Power(t2,2)) + 
                  s2*(85 + 87*t2 - 108*Power(t2,2) + 43*Power(t2,3) + 
                     4*Power(t2,4)))) + 
            2*Power(t2,2)*(-15 + 
               (-132 + 33*s + 39*Power(s,2) - 2*Power(s,3))*t2 - 
               (113 - 237*s + 54*Power(s,2) + 19*Power(s,3))*
                Power(t2,2) + 
               (336 + 252*s - 212*Power(s,2) - 5*Power(s,3))*
                Power(t2,3) + 
               (167 - 316*s - 458*Power(s,2) + 43*Power(s,3))*
                Power(t2,4) + 
               (-428 - 13*s - 131*Power(s,2) + 23*Power(s,3))*
                Power(t2,5) + (-7 + 31*s)*Power(t2,6) + 
               Power(s2,3)*t2*
                (-20 + 93*t2 + 139*Power(t2,2) + 43*Power(t2,3) - 
                  23*Power(t2,4)) + 
               s2*(90 + (173 + 62*s)*t2 + 
                  (127 + 392*s + 19*Power(s,2))*Power(t2,2) + 
                  (-472 + 364*s - 299*Power(s,2))*Power(t2,3) + 
                  (-74 + 928*s - 171*Power(s,2))*Power(t2,4) + 
                  (411 + 270*s - 69*Power(s,2))*Power(t2,5) - 
                  31*Power(t2,6)) + 
               Power(s2,2)*t2*
                (195 + 374*t2 + 528*Power(t2,2) + 82*Power(t2,3) - 
                  139*Power(t2,4) + 
                  s*(-90 - 61*t2 + 245*Power(t2,2) + 85*Power(t2,3) + 
                     69*Power(t2,4)))) + 
            Power(t1,6)*(-30 + 48*t2 + 1222*Power(t2,2) + 
               708*Power(t2,3) - 388*Power(t2,4) - 102*Power(t2,5) - 
               Power(s2,3)*t2*
                (13 + 12*t2 - 4*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s,3)*(20 - 110*t2 + 104*Power(t2,2) - 
                  65*Power(t2,3) + 2*Power(t2,4)) - 
               Power(s2,2)*(17 + 100*t2 + 568*Power(t2,2) + 
                  146*Power(t2,3) + 72*Power(t2,4)) - 
               Power(s,2)*(215 + (129 + 403*s2)*t2 + 
                  (-254 + 280*s2)*Power(t2,2) - 
                  8*(-56 + 19*s2)*Power(t2,3) + 
                  3*(9 + 2*s2)*Power(t2,4)) - 
               s2*(124 + 94*t2 - 584*Power(t2,2) + 960*Power(t2,3) + 
                  769*Power(t2,4) + 22*Power(t2,5)) + 
               s*(284 + 340*t2 - 260*Power(t2,2) + 1369*Power(t2,3) + 
                  247*Power(t2,4) + 22*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (102 + 232*t2 - 91*Power(t2,2) + 6*Power(t2,3)) + 
                  s2*(98 + 235*t2 + 294*Power(t2,2) + 228*Power(t2,3) + 
                     99*Power(t2,4)))) - 
            2*Power(t1,2)*t2*
             (6 - 168*t2 - 167*Power(t2,2) + 955*Power(t2,3) - 
               461*Power(t2,4) - 1359*Power(t2,5) + 186*Power(t2,6) + 
               Power(s2,3)*t2*
                (-18 - 75*t2 + 44*Power(t2,2) + 345*Power(t2,3) - 
                  36*Power(t2,4)) + 
               Power(s,3)*t2*
                (-18 - 159*t2 - 108*Power(t2,2) + 109*Power(t2,3) + 
                  36*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-147 + 291*t2 + 1862*Power(t2,2) + 243*Power(t2,3) + 
                  311*Power(t2,4)) + 
               2*s2*(9 + 213*t2 + 600*Power(t2,2) + 392*Power(t2,3) + 
                  2291*Power(t2,4) + 887*Power(t2,5) - 28*Power(t2,6)) \
+ s*(18 - 6*(34 - 8*s2 + 3*Power(s2,2))*t2 + 
                  (182 + 594*s2 - 5*Power(s2,2))*Power(t2,2) - 
                  2*(1639 - 707*s2 + 188*Power(s2,2))*Power(t2,3) + 
                  (-3920 + 978*s2 - 353*Power(s2,2))*Power(t2,4) + 
                  2*(53 - 165*s2 + 54*Power(s2,2))*Power(t2,5) + 
                  56*Power(t2,6)) - 
               Power(s,2)*t2*
                (-303 + 569*t2 + 272*Power(t2,2) + 281*Power(t2,3) - 
                  19*Power(t2,4) + 
                  s2*(18 - 1183*t2 - 272*Power(t2,2) + 
                     101*Power(t2,3) + 108*Power(t2,4)))) + 
            Power(t1,5)*(112 + 358*t2 - 1026*Power(t2,2) - 
               2926*Power(t2,3) + 253*Power(t2,4) + 73*Power(t2,5) + 
               48*Power(t2,6) - 
               Power(s2,3)*t2*
                (-29 - 105*t2 + 97*Power(t2,2) + Power(t2,3)) + 
               Power(s,3)*t2*
                (200 + 380*t2 - 272*Power(t2,2) + 19*Power(t2,3)) + 
               Power(s2,2)*(6 + 54*t2 - 128*Power(t2,2) + 
                  79*Power(t2,3) - 325*Power(t2,4) + 25*Power(t2,5)) + 
               Power(s,2)*(60 + (-667 + 175*s2)*t2 + 
                  (48 - 1207*s2)*Power(t2,2) + 
                  (-1052 + 579*s2)*Power(t2,3) - 
                  13*(-8 + 3*s2)*Power(t2,4) + 25*Power(t2,5)) + 
               s2*(107 + 34*t2 - 479*Power(t2,2) - 4371*Power(t2,3) - 
                  2540*Power(t2,4) + 165*Power(t2,5)) + 
               s*(-250 - 601*t2 - 1527*Power(t2,2) + 5641*Power(t2,3) - 
                  10*Power(t2,4) - 25*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-113 - 232*t2 - 267*Power(t2,2) + 21*Power(t2,3)) - 
                  s2*(30 - 82*t2 + 940*Power(t2,2) + 1787*Power(t2,3) - 
                     280*Power(t2,4) + 50*Power(t2,5))))) + 
         Power(s1,5)*(-2 - 23*Power(t1,10) - 34*t2 - 5*s*t2 - 35*s2*t2 + 
            6*s*s2*t2 + 60*Power(s2,2)*t2 - 76*Power(t2,2) - 
            51*s*Power(t2,2) - 2*Power(s,2)*Power(t2,2) - 
            283*s2*Power(t2,2) + 143*s*s2*Power(t2,2) + 
            4*Power(s,2)*s2*Power(t2,2) + 267*Power(s2,2)*Power(t2,2) + 
            24*s*Power(s2,2)*Power(t2,2) + 80*Power(s2,3)*Power(t2,2) + 
            119*Power(t2,3) + 145*s*Power(t2,3) - 
            3*Power(s,2)*Power(t2,3) - 5*Power(s,3)*Power(t2,3) - 
            403*s2*Power(t2,3) + 583*s*s2*Power(t2,3) + 
            10*Power(s,2)*s2*Power(t2,3) - 72*Power(s2,2)*Power(t2,3) + 
            299*s*Power(s2,2)*Power(t2,3) + 
            360*Power(s2,3)*Power(t2,3) + 283*Power(t2,4) - 
            234*s*Power(t2,4) - 148*Power(s,2)*Power(t2,4) - 
            18*Power(s,3)*Power(t2,4) - 308*s2*Power(t2,4) + 
            1225*s*s2*Power(t2,4) - 274*Power(s,2)*s2*Power(t2,4) - 
            329*Power(s2,2)*Power(t2,4) + 
            482*s*Power(s2,2)*Power(t2,4) + 30*Power(s2,3)*Power(t2,4) - 
            397*Power(t2,5) - 108*s*Power(t2,5) - 
            117*Power(s,2)*Power(t2,5) + 31*Power(s,3)*Power(t2,5) + 
            430*s2*Power(t2,5) + 491*s*s2*Power(t2,5) - 
            224*Power(s,2)*s2*Power(t2,5) - 
            328*Power(s2,2)*Power(t2,5) + 
            355*s*Power(s2,2)*Power(t2,5) - 
            162*Power(s2,3)*Power(t2,5) - 85*Power(t2,6) + 
            45*s*Power(t2,6) + 10*Power(s,2)*Power(t2,6) - 
            89*s2*Power(t2,6) - 20*s*s2*Power(t2,6) + 
            10*Power(s2,2)*Power(t2,6) + 
            Power(t1,9)*(85 + 77*s - 118*s2 + 52*t2) + 
            Power(t1,8)*(34 + 77*Power(s2,2) + 
               s*(-343 + 7*s2 - 176*t2) + 87*t2 + 6*Power(t2,2) + 
               s2*(86 + 374*t2)) + 
            Power(t1,7)*(335 + 8*Power(s2,3) - 1149*t2 - 
               651*Power(t2,2) - 58*Power(t2,3) + 
               Power(s,2)*(81 + 20*t2) - Power(s2,2)*(170 + 41*t2) + 
               s2*(230 + 791*t2 - 348*Power(t2,2)) + 
               s*(37 + 28*Power(s2,2) + s2*(201 - 35*t2) - 194*t2 + 
                  155*Power(t2,2))) - 
            Power(t1,6)*(242 + 669*t2 - 1869*Power(t2,2) - 
               180*Power(t2,3) - 24*Power(t2,4) + 
               5*Power(s,3)*(-8 + 5*t2) + Power(s2,3)*(-41 + 91*t2) + 
               Power(s,2)*(65 + s2*(165 - 29*t2) - 47*t2 + 
                  132*Power(t2,2)) + 
               Power(s2,2)*(120 + 516*t2 + 187*Power(t2,2)) + 
               s2*(74 + 1129*t2 + 3247*Power(t2,2) - 125*Power(t2,3)) + 
               s*(135 + Power(s2,2)*(70 - 59*t2) - 3582*t2 - 
                  1329*Power(t2,2) + 77*Power(t2,3) + 
                  s2*(181 + 540*t2 - 312*Power(t2,2)))) - 
            Power(t1,5)*(303 + 2097*t2 - 4578*Power(t2,2) - 
               1504*Power(t2,3) - 336*Power(t2,4) + 
               Power(s2,3)*(18 + 178*t2 - 171*Power(t2,2)) + 
               Power(s,3)*(-21 + 30*t2 + 20*Power(t2,2)) - 
               Power(s2,2)*(-105 + 1694*t2 + 723*Power(t2,2) + 
                  191*Power(t2,3)) + 
               s2*(-315 + 2060*t2 + 506*Power(t2,2) - 
                  3214*Power(t2,3) + 20*Power(t2,4)) + 
               Power(s,2)*(-40 + 137*t2 + 56*Power(t2,2) - 
                  148*Power(t2,3) + s2*(-100 + 10*t2 - 163*Power(t2,2))\
) + s*(205 + 692*t2 + 2297*Power(t2,2) + 1658*Power(t2,3) - 
                  20*Power(t2,4) + 
                  Power(s2,2)*(5 + 202*t2 + 314*Power(t2,2)) + 
                  s2*(-165 + 1793*t2 + 339*Power(t2,2) + 
                     339*Power(t2,3)))) + 
            t1*(4 + 65*t2 - 158*Power(t2,2) - 439*Power(t2,3) + 
               1636*Power(t2,4) - 450*Power(t2,5) - 50*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*
                (2 - 49*t2 + 14*Power(t2,2) + 55*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-30 + 928*t2 + 3673*Power(t2,2) + 1708*Power(t2,3) - 
                  203*Power(t2,4)) + 
               Power(s2,3)*t2*
                (-60 - 235*t2 + 295*Power(t2,2) + 981*Power(t2,3) - 
                  55*Power(t2,4)) + 
               s2*(30 + 529*t2 + 1799*Power(t2,2) - 1599*Power(t2,3) - 
                  1901*Power(t2,4) + 1614*Power(t2,5) - 80*Power(t2,6)) \
+ s*(-6 + (-109 + 140*s2 - 30*Power(s2,2))*t2 + 
                  (-287 + 123*s2 - 218*Power(s2,2))*Power(t2,2) + 
                  (157 - 2822*s2 - 733*Power(s2,2))*Power(t2,3) - 
                  (167 + 27*s2 + 1358*Power(s2,2))*Power(t2,4) + 
                  (-280 + 514*s2 + 165*Power(s2,2))*Power(t2,5) + 
                  80*Power(t2,6)) + 
               Power(s,2)*t2*
                (-4 + 179*t2 + 167*Power(t2,2) - 747*Power(t2,3) - 
                  311*Power(t2,4) + 
                  s2*(6 + 43*t2 - 269*Power(t2,2) + 363*Power(t2,3) - 
                     165*Power(t2,4)))) + 
            Power(t1,4)*(24 + 2229*t2 + 2506*Power(t2,2) - 
               9293*Power(t2,3) - 1280*Power(t2,4) - 120*Power(t2,5) + 
               Power(s,3)*(-23 - 192*t2 + 91*Power(t2,2) + 
                  67*Power(t2,3)) - 
               Power(s2,3)*(35 + 148*t2 - 798*Power(t2,2) + 
                  118*Power(t2,3)) + 
               s2*(32 + 776*t2 + 3589*Power(t2,2) + 6802*Power(t2,3) - 
                  1224*Power(t2,4)) + 
               Power(s2,2)*(133 + 1142*t2 + 432*Power(t2,2) + 
                  293*Power(t2,3) - 42*Power(t2,4)) + 
               Power(s,2)*(32 - 508*t2 - 492*Power(t2,2) + 
                  35*Power(t2,3) - 42*Power(t2,4) + 
                  s2*(5 + 936*t2 - 114*Power(t2,2) - 252*Power(t2,3))) \
+ s*(132 - 67*t2 - 9487*Power(t2,2) - 1330*Power(t2,3) + 
                  910*Power(t2,4) + 
                  Power(s2,2)*
                   (50 + 456*t2 - 490*Power(t2,2) + 303*Power(t2,3)) + 
                  s2*(-68 + 1924*t2 + 3997*Power(t2,2) - 
                     190*Power(t2,3) + 84*Power(t2,4)))) + 
            Power(t1,3)*(-22 + 463*t2 + 1703*Power(t2,2) - 
               5579*Power(t2,3) + 959*Power(t2,4) - 160*Power(t2,5) + 
               Power(s2,2)*(110 + 258*t2 - 5165*Power(t2,2) - 
                  2514*Power(t2,3) - 319*Power(t2,4)) + 
               Power(s,3)*(2 + t2 + 41*Power(t2,2) + 149*Power(t2,3) - 
                  26*Power(t2,4)) + 
               Power(s2,3)*(20 + 204*t2 + 485*Power(t2,2) - 
                  1066*Power(t2,3) + 26*Power(t2,4)) + 
               Power(s,2)*(2 + (57 - 301*s2)*t2 + 
                  (-442 + 281*s2)*Power(t2,2) + 
                  (320 - 963*s2)*Power(t2,3) + 
                  (-69 + 78*s2)*Power(t2,4)) + 
               s2*(-141 - 1807*t2 + 4190*Power(t2,2) + 
                  122*Power(t2,3) - 5852*Power(t2,4) + 162*Power(t2,5)) \
+ s*(22 + 531*t2 + 1906*Power(t2,2) + 4745*Power(t2,3) + 
                  2487*Power(t2,4) - 162*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-8 + 427*t2 + 1880*Power(t2,2) - 78*Power(t2,3)) + 
                  s2*(-129 - 889*t2 + 4780*Power(t2,2) + 
                     1342*Power(t2,3) + 388*Power(t2,4)))) + 
            Power(t1,2)*(59 + 239*t2 - 2448*Power(t2,2) - 
               2270*Power(t2,3) + 7679*Power(t2,4) + 1253*Power(t2,5) + 
               72*Power(t2,6) + 
               Power(s,3)*t2*
                (25 + 157*t2 - 79*Power(t2,2) - 229*Power(t2,3)) + 
               Power(s2,3)*t2*
                (40 - 309*t2 - 1307*Power(t2,2) + 481*Power(t2,3)) + 
               Power(s2,2)*(-60 - 792*t2 - 2620*Power(t2,2) + 
                  1209*Power(t2,3) + 1099*Power(t2,4) + 26*Power(t2,5)) \
+ Power(s,2)*(-6 + (28 - 85*s2)*t2 + (810 - 1157*s2)*Power(t2,2) + 
                  2*(628 + 225*s2)*Power(t2,3) + 
                  (847 + 939*s2)*Power(t2,4) + 26*Power(t2,5)) + 
               s2*(-80 - 235*t2 + 155*Power(t2,2) - 1821*Power(t2,3) - 
                  4581*Power(t2,4) + 1422*Power(t2,5)) - 
               s*(-61 - 208*t2 + 105*Power(t2,2) - 5326*Power(t2,3) - 
                  108*Power(t2,4) + 926*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (85 + 653*t2 - 267*Power(t2,2) + 1191*Power(t2,3)) + 
                  s2*(-30 - 152*t2 + 2444*Power(t2,2) + 
                     7600*Power(t2,3) + 2233*Power(t2,4) + 52*Power(t2,5)\
)))) + Power(s1,3)*((-1 + 3*s - 5*s2)*Power(t1,11) + 
            2*t2*(10 + (65 + 70*s2 - 90*Power(s2,2) - 5*s*(-5 + 6*s2))*
                t2 + (86 + 4*Power(s,3) + Power(s,2)*(2 - 21*s2) + 
                  427*s2 - 82*Power(s2,2) - 85*Power(s2,3) - 
                  2*s*(8 + 104*s2 + 25*Power(s2,2)))*Power(t2,2) + 
               (-294 + 15*Power(s,3) + Power(s,2)*(109 - 98*s2) + 
                  387*s2 + 301*Power(s2,2) - 84*Power(s2,3) - 
                  s*(61 + 874*s2 + 457*Power(s2,2)))*Power(t2,3) + 
               (-266 + 22*Power(s,3) - 159*s2 + 555*Power(s2,2) + 
                  135*Power(s2,3) + 21*Power(s,2)*(3 + 7*s2) + 
                  s*(445 - 1458*s2 - 424*Power(s2,2)))*Power(t2,4) + 
               (533 - 9*Power(s,3) - 429*s2 + 381*Power(s2,2) + 
                  106*Power(s2,3) + Power(s,2)*(35 + 124*s2) - 
                  s*(8 + 400*s2 + 221*Power(s2,2)))*Power(t2,5) + 
               (50 - 9*Power(s,2) + 152*s2 - 9*Power(s2,2) + 
                  s*(-65 + 18*s2))*Power(t2,6) + 8*Power(t2,7)) - 
            2*Power(t1,2)*(10 + 
               (30 + 24*Power(s,2) - 107*s2 - 72*Power(s2,2) + 
                  s*(25 + 6*s2))*t2 + 
               (31 + 50*Power(s,3) + 35*s2 - 555*Power(s2,2) + 
                  36*Power(s2,3) - 2*Power(s,2)*(19 + 30*s2) + 
                  s*(-349 + 185*s2 - 62*Power(s2,2)))*Power(t2,2) + 
               (-2449 + 253*Power(s,3) - 507*s2 - 681*Power(s2,2) - 
                  46*Power(s2,3) - 2*Power(s,2)*(-701 + 696*s2) + 
                  s*(350 - 4013*s2 - 359*Power(s2,2)))*Power(t2,3) - 
               (1073 + 188*Power(s,3) + 4465*s2 - 2805*Power(s2,2) + 
                  454*Power(s2,3) - 2*Power(s,2)*(388 + 287*s2) + 
                  s*(-4072 + 7385*s2 + 240*Power(s2,2)))*Power(t2,4) + 
               (4383 - 155*Power(s,3) - 852*s2 + 511*Power(s2,2) + 
                  316*Power(s2,3) + Power(s,2)*(296 + 626*s2) - 
                  s*(2649 + 829*s2 + 787*Power(s2,2)))*Power(t2,5) + 
               (72*Power(s,2) - 3*s*(275 + 48*s2) + 
                  4*(35 + 340*s2 + 18*Power(s2,2)))*Power(t2,6) + 
               144*Power(t2,7)) + 
            Power(t1,10)*(-9 + 3*Power(s2,2) + 2*s*(-7 + 3*s2 - 3*t2) + 
               11*t2 + s2*(5 + 17*t2)) + 
            Power(t1,9)*(63 + Power(s,2)*(47 - 6*t2) + 15*t2 - 
               23*Power(t2,2) + 2*Power(s2,2)*(-5 + 2*t2) + 
               s2*(44 + 119*t2 - 10*Power(t2,2)) + 
               s*(-69 + Power(s2,2) - 42*t2 - 5*Power(t2,2) - 
                  2*s2*(1 + 6*t2))) - 
            Power(t1,8)*(58 + 46*t2 + 65*Power(t2,2) + 21*Power(t2,3) + 
               Power(s2,3)*(-2 + 3*t2) + Power(s,3)*(-12 + 11*t2) + 
               Power(s,2)*(98 + s2*(53 - 21*t2) + 83*t2 - 
                  3*Power(t2,2)) + 
               Power(s2,2)*(17 + 90*t2 + 21*Power(t2,2)) + 
               s2*(54 + 338*t2 + 485*Power(t2,2) + 10*Power(t2,3)) - 
               s*(25 + Power(s2,2)*(2 - 8*t2) + 581*t2 + 
                  171*Power(t2,2) + 16*Power(t2,3) + 
                  s2*(52 - 52*t2 + 23*Power(t2,2)))) + 
            Power(t1,7)*(20 - 781*t2 - 3*Power(t2,2) + 
               332*Power(t2,3) + 66*Power(t2,4) + 
               Power(s2,3)*t2*(4 + t2) + 
               Power(s,3)*(50 - 46*t2 + 23*Power(t2,2)) + 
               Power(s2,2)*(4 + 344*t2 + 38*Power(t2,2) + 
                  23*Power(t2,3)) + 
               s2*(83 - 422*t2 - 124*Power(t2,2) + 446*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s,2)*(46 - 269*t2 + 80*Power(t2,2) + 
                  12*Power(t2,3) + s2*(42 + 100*t2 - 47*Power(t2,2))) + 
               s*(-201 + 296*t2 - 517*Power(t2,2) - 100*Power(t2,3) - 
                  8*Power(t2,4) + 
                  Power(s2,2)*(-4 - 94*t2 + 23*Power(t2,2)) - 
                  s2*(18 + 307*t2 - 78*Power(t2,2) + 35*Power(t2,3)))) + 
            2*Power(t1,3)*(2 - 219*t2 - 620*Power(t2,2) - 
               354*Power(t2,3) + 806*Power(t2,4) - 1429*Power(t2,5) + 
               342*Power(t2,6) - 
               Power(s2,3)*t2*
                (16 + 67*t2 + 111*Power(t2,2) - 493*Power(t2,3) + 
                  25*Power(t2,4)) + 
               Power(s,3)*t2*
                (-16 - 66*t2 - 115*Power(t2,2) + 38*Power(t2,3) + 
                  25*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-110 - 26*t2 + 3125*Power(t2,2) + 986*Power(t2,3) + 
                  429*Power(t2,4)) + 
               s2*(6 + 211*t2 + 1182*Power(t2,2) - 943*Power(t2,3) + 
                  5012*Power(t2,4) + 3522*Power(t2,5) - 70*Power(t2,6)) \
+ s*(6 + (-91 + 16*s2 - 6*Power(s2,2))*t2 + 
                  (-86 + 743*s2 - 62*Power(s2,2))*Power(t2,2) - 
                  (2715 + 1046*s2 + 331*Power(s2,2))*Power(t2,3) + 
                  (-6468 + 173*s2 - 718*Power(s2,2))*Power(t2,4) + 
                  5*(-200 - 126*s2 + 15*Power(s2,2))*Power(t2,5) + 
                  70*Power(t2,6)) + 
               Power(s,2)*t2*
                (220 - 323*t2 - 145*Power(t2,2) - 101*Power(t2,3) + 
                  201*Power(t2,4) + 
                  s2*(-6 + 923*t2 + 161*Power(t2,2) + 187*Power(t2,3) - 
                     75*Power(t2,4)))) + 
            Power(t1,6)*(-135 + 639*t2 + 1482*Power(t2,2) - 
               787*Power(t2,3) + 29*Power(t2,4) - 32*Power(t2,5) + 
               Power(s2,3)*(-4 - 49*t2 + 73*Power(t2,2) + 
                  3*Power(t2,3)) - 
               Power(s,3)*(70 + 160*t2 - 146*Power(t2,2) + 
                  13*Power(t2,3)) + 
               s2*(-20 + 278*t2 + 3144*Power(t2,2) + 
                  3044*Power(t2,3) - 65*Power(t2,4)) + 
               Power(s2,2)*(42 + 85*t2 + 254*Power(t2,2) + 
                  340*Power(t2,3) - 9*Power(t2,4)) + 
               Power(s,2)*(234 + 206*t2 + 527*Power(t2,2) + 
                  123*Power(t2,3) - 9*Power(t2,4) + 
                  s2*(10 + 635*t2 - 301*Power(t2,2) + 29*Power(t2,3))) \
+ s*(198 + 495*t2 - 4754*Power(t2,2) - 690*Power(t2,3) - 
                  45*Power(t2,4) + 
                  Power(s2,2)*
                   (5 + 52*t2 + 115*Power(t2,2) - 19*Power(t2,3)) + 
                  s2*(-171 + 477*t2 + 1006*Power(t2,2) - 
                     503*Power(t2,3) + 18*Power(t2,4)))) - 
            2*t1*t2*(30 - 91*t2 - 255*Power(t2,2) + 905*Power(t2,3) + 
               691*Power(t2,4) - 1530*Power(t2,5) - 6*Power(t2,6) + 
               Power(s2,2)*t2*
                (-15 + 786*t2 + 2863*Power(t2,2) + 552*Power(t2,3) - 
                  170*Power(t2,4)) + 
               Power(s2,3)*t2*
                (-30 - 81*t2 + 150*Power(t2,2) + 479*Power(t2,3) - 
                  54*Power(t2,4)) + 
               Power(s,3)*t2*
                (-6 - 43*t2 - 42*Power(t2,2) + 69*Power(t2,3) + 
                  54*Power(t2,4)) + 
               s2*(90 + 614*t2 + 1506*Power(t2,2) - 1499*Power(t2,3) + 
                  463*Power(t2,4) + 1713*Power(t2,5) - 103*Power(t2,6)) \
+ s*(-30 - 2*(58 - 23*s2 + 45*Power(s2,2))*t2 + 
                  (78 + 62*s2 - 33*Power(s2,2))*Power(t2,2) + 
                  (55 - 892*s2 - 102*Power(s2,2))*Power(t2,3) + 
                  (-1731 + 1750*s2 - 545*Power(s2,2))*Power(t2,4) + 
                  (-255 + 442*s2 + 162*Power(s2,2))*Power(t2,5) + 
                  103*Power(t2,6)) - 
               Power(s,2)*t2*
                (-97 + 8*t2 + 227*Power(t2,2) + 1094*Power(t2,3) + 
                  272*Power(t2,4) + 
                  s2*(-30 - 317*t2 + 198*Power(t2,2) + 3*Power(t2,3) + 
                     162*Power(t2,4)))) + 
            Power(t1,5)*(137 + 684*t2 + 2890*Power(t2,2) - 
               1002*Power(t2,3) - 819*Power(t2,4) - 474*Power(t2,5) + 
               Power(s,3)*(20 - 84*t2 + 172*Power(t2,2) - 
                  96*Power(t2,3) + Power(t2,4)) - 
               Power(s2,3)*(-2 + 15*t2 - 90*Power(t2,2) + 
                  173*Power(t2,3) + Power(t2,4)) - 
               Power(s2,2)*(16 - 23*t2 + 2290*Power(t2,2) + 
                  727*Power(t2,3) + 414*Power(t2,4)) - 
               Power(s,2)*(250 + (98 + 772*s2)*t2 + 
                  (-509 + 358*s2)*Power(t2,2) + 
                  (451 - 98*s2)*Power(t2,3) + 3*(96 + s2)*Power(t2,4)) \
- s2*(132 + 450*t2 - 1621*Power(t2,2) + 4277*Power(t2,3) + 
                  3536*Power(t2,4) + 30*Power(t2,5)) + 
               s*(211 + 440*t2 + 851*Power(t2,2) + 6326*Power(t2,3) + 
                  1327*Power(t2,4) + 30*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (167 + 394*t2 + 171*Power(t2,2) + 3*Power(t2,3)) + 
                  s2*(141 - 167*t2 + 1378*Power(t2,2) + 
                     234*Power(t2,3) + 702*Power(t2,4)))) - 
            Power(t1,4)*(1 - 92*t2 + 4077*Power(t2,2) + 
               5114*Power(t2,3) - 6820*Power(t2,4) - 568*Power(t2,5) - 
               200*Power(t2,6) + 
               2*Power(s,3)*t2*
                (-83 - 278*t2 + 206*Power(t2,2) + 33*Power(t2,3)) - 
               Power(s2,3)*t2*
                (83 + 190*t2 - 590*Power(t2,2) + 158*Power(t2,3)) + 
               s2*(-67 - 116*t2 + 1720*Power(t2,2) + 9093*Power(t2,3) + 
                  4471*Power(t2,4) - 1349*Power(t2,5)) + 
               Power(s2,2)*(6 + 194*t2 + 571*Power(t2,2) - 
                  1490*Power(t2,3) + 596*Power(t2,4) - 121*Power(t2,5)) \
- Power(s,2)*(48 + 32*(-15 + 4*s2)*t2 - 6*(-185 + 376*s2)*Power(t2,2) + 
                  (-343 + 841*s2)*Power(t2,3) + 
                  (-67 + 290*s2)*Power(t2,4) + 121*Power(t2,5)) + 
               s*(176 + 1151*t2 + 574*Power(t2,2) - 11896*Power(t2,3) + 
                  2284*Power(t2,4) + 767*Power(t2,5) + 
                  2*Power(s2,2)*t2*
                   (90 + 228*t2 + 51*Power(t2,2) + 191*Power(t2,3)) + 
                  s2*(24 - 330*t2 + 4291*Power(t2,2) + 
                     6968*Power(t2,3) - 718*Power(t2,4) + 242*Power(t2,5)\
)))) + Power(s1,4)*(-3*Power(t1,11) + 
            Power(t1,10)*(9 + 23*s - 37*s2 + 7*t2) + 
            t1*(-10 + 5*(-40 + 21*s2 + 36*Power(s2,2) - 3*s*(7 + 2*s2))*
                t2 + (-546 - 24*Power(s,3) - 577*s2 + 
                  1067*Power(s2,2) + 60*Power(s2,3) + 
                  4*Power(s,2)*(-23 + 39*s2) + 
                  3*s*(37 + 11*s2 + 20*Power(s2,2)))*Power(t2,2) + 
               (1915 - 69*Power(s,3) - 1683*s2 + 1390*Power(s2,2) + 
                  484*Power(s2,3) + Power(s,2)*(-969 + 942*s2) + 
                  s*(613 + 3547*s2 + 875*Power(s2,2)))*Power(t2,3) + 
               (1677 + 82*Power(s,3) + 1488*s2 - 2683*Power(s2,2) + 
                  550*Power(s2,3) - 2*Power(s,2)*(546 + 395*s2) + 
                  s*(-3108 + 8435*s2 + 778*Power(s2,2)))*Power(t2,4) + 
               (-4551 + 199*Power(s,3) + 2854*s2 - 1626*Power(s2,2) - 
                  566*Power(s2,3) - Power(s,2)*(587 + 964*s2) + 
                  s*(232 + 2315*s2 + 1331*Power(s2,2)))*Power(t2,5) + 
               (-581 + 4*Power(s,2) + s*(577 - 8*s2) - 1067*s2 + 
                  4*Power(s2,2))*Power(t2,6) - 56*Power(t2,7)) + 
            Power(t1,9)*(2 + 23*Power(s2,2) + s*(-115 + 13*s2 - 51*t2) + 
               50*t2 + 4*Power(t2,2) + s2*(43 + 122*t2)) + 
            Power(t1,8)*(167 + Power(s2,3) - 244*t2 - 188*Power(t2,2) - 
               16*Power(t2,3) - 2*Power(s2,2)*(32 + t2) + 
               Power(s,2)*(87 + t2) + 
               s2*(118 + 385*t2 - 99*Power(t2,2)) + 
               s*(-74 + 8*Power(s2,2) + s2*(55 - 39*t2) - 92*t2 + 
                  17*Power(t2,2))) + 
            Power(t1,4)*(163 + 1051*t2 + 2839*Power(t2,2) - 
               4623*Power(t2,3) + 496*Power(t2,4) - 660*Power(t2,5) + 
               Power(s2,2)*(61 + 293*t2 - 4711*Power(t2,2) - 
                  1815*Power(t2,3) - 647*Power(t2,4)) + 
               Power(s,3)*(10 - 17*t2 + 129*Power(t2,2) + 
                  30*Power(t2,3) - 13*Power(t2,4)) + 
               Power(s2,3)*(10 + 72*t2 + 349*Power(t2,2) - 
                  663*Power(t2,3) + 13*Power(t2,4)) + 
               Power(s,2)*(-119 + (80 - 712*s2)*t2 + 
                  (181 - 57*s2)*Power(t2,2) + 
                  (79 - 489*s2)*Power(t2,3) + (-428 + 39*s2)*Power(t2,4)\
) + s*(23 + (475 - 825*s2 + 67*Power(s2,2))*t2 + 
                  (2349 + 3664*s2 + 409*Power(s2,2))*Power(t2,2) + 
                  2*(4639 + 264*s2 + 561*Power(s2,2))*Power(t2,3) + 
                  (2937 + 1075*s2 - 39*Power(s2,2))*Power(t2,4) - 
                  88*Power(t2,5)) + 
               s2*(-69 - 1297*t2 + 3701*Power(t2,2) - 4665*Power(t2,3) - 
                  6789*Power(t2,4) + 88*Power(t2,5))) - 
            Power(t1,7)*(124 + 360*t2 - 385*Power(t2,2) + 
               35*Power(t2,3) - 8*Power(t2,4) + 
               6*Power(s,3)*(-5 + 4*t2) + Power(s2,3)*(-14 + 25*t2) + 
               Power(s,2)*(139 + s2*(122 - 41*t2) + 64*t2 + 
                  60*Power(t2,2)) + 
               Power(s2,2)*(51 + 264*t2 + 101*Power(t2,2)) + 
               s2*(86 + 944*t2 + 1736*Power(t2,2) - 5*Power(t2,3)) + 
               s*(48 + 14*Power(s2,2) - 1994*t2 - 660*Power(t2,2) - 
                  21*Power(t2,3) - 21*s2*t2*(-13 + 8*t2))) + 
            Power(t1,2)*(35 + 28*t2 - 419*Power(t2,2) + 
               721*Power(t2,3) + 3550*Power(t2,4) - 3703*Power(t2,5) + 
               76*Power(t2,6) - 
               Power(s2,3)*t2*
                (60 + 312*t2 + 90*Power(t2,2) - 1523*Power(t2,3) + 
                  88*Power(t2,4)) + 
               Power(s,3)*t2*
                (-12 - 60*t2 - 127*Power(t2,2) - 52*Power(t2,3) + 
                  88*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-230 + 643*t2 + 7692*Power(t2,2) + 2611*Power(t2,3) + 
                  138*Power(t2,4)) + 
               s2*(60 + 723*t2 + 3627*Power(t2,2) - 4316*Power(t2,3) + 
                  1760*Power(t2,4) + 6156*Power(t2,5) - 266*Power(t2,6)) \
+ s*(-30 + (-182 + 247*s2 - 60*Power(s2,2))*t2 + 
                  (-796 + 693*s2 - 6*Power(s2,2))*Power(t2,2) - 
                  (1825 + 5249*s2 + 657*Power(s2,2))*Power(t2,3) + 
                  (-6377 + 1016*s2 - 2350*Power(s2,2))*Power(t2,4) + 
                  (-1804 + 137*s2 + 264*Power(s2,2))*Power(t2,5) + 
                  266*Power(t2,6)) + 
               Power(s,2)*t2*(84 + 25*t2 + 378*Power(t2,2) - 
                  1574*Power(t2,3) - 275*Power(t2,4) + 
                  s2*(30 + 654*t2 - 260*Power(t2,2) + 879*Power(t2,3) - 
                     264*Power(t2,4)))) + 
            2*t2*(5 - 5*(-6 + s)*t2 + 
               (68 - 35*s - 24*Power(s,2))*Power(t2,2) + 
               (43 - 197*s + 74*Power(s,2) + 6*Power(s,3))*Power(t2,3) - 
               4*(55 + 3*s - 62*Power(s,2) + 9*Power(s,3))*Power(t2,4) + 
               (115 + 16*s + 82*Power(s,2) - 20*Power(s,3))*
                Power(t2,5) + (7 - 15*s)*Power(t2,6) + 
               Power(s2,3)*t2*
                (30 - 15*t2 - 278*Power(t2,2) - 143*Power(t2,3) + 
                  20*Power(t2,4)) + 
               s2*(-30 - 5*(33 + 7*s)*t2 + 
                  (-185 - 113*s + 27*Power(s,2))*Power(t2,2) + 
                  (325 + 172*s + 160*Power(s,2))*Power(t2,3) + 
                  (382 - 267*s + 99*Power(s,2))*Power(t2,4) + 
                  (-238 - 157*s + 60*Power(s,2))*Power(t2,5) + 
                  15*Power(t2,6)) + 
               Power(s2,2)*t2*
                (-95 - 497*t2 - 536*Power(t2,2) - 271*Power(t2,3) + 
                  75*Power(t2,4) + 
                  s*(30 + 156*t2 + 172*Power(t2,2) + 80*Power(t2,3) - 
                     60*Power(t2,4)))) + 
            Power(t1,6)*(-142 - 1625*t2 + 1806*Power(t2,2) + 
               980*Power(t2,3) + 268*Power(t2,4) + 
               Power(s,3)*(45 - 60*t2 + 23*Power(t2,2)) + 
               Power(s2,3)*(-3 - 43*t2 + 50*Power(t2,2)) + 
               Power(s2,2)*(-19 + 1031*t2 + 264*Power(t2,2) + 
                  121*Power(t2,3)) + 
               s2*(225 - 1152*t2 + 60*Power(t2,2) + 1798*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s,2)*(87 - 341*t2 - 11*Power(t2,2) + 
                  98*Power(t2,3) + s2*(90 + 109*t2 - 11*Power(t2,2))) - 
               s*(2*Power(s2,2)*(3 + 87*t2 + 31*Power(t2,2)) + 
                  s2*(-11 + 996*t2 - 114*Power(t2,2) + 
                     219*Power(t2,3)) + 
                  2*(144 + 40*t2 + 928*Power(t2,2) + 381*Power(t2,3) + 
                     5*Power(t2,4)))) + 
            Power(t1,5)*(-96 + 1678*t2 + 2705*Power(t2,2) - 
               5460*Power(t2,3) - 420*Power(t2,4) - 112*Power(t2,5) + 
               Power(s,3)*(-55 - 230*t2 + 174*Power(t2,2) + 
                  14*Power(t2,3)) - 
               Power(s2,3)*(19 + 130*t2 - 363*Power(t2,2) + 
                  41*Power(t2,3)) + 
               s2*(-110 + 639*t2 + 5177*Power(t2,2) + 
                  6028*Power(t2,3) - 668*Power(t2,4)) + 
               Power(s2,2)*(74 + 396*t2 + 283*Power(t2,2) + 
                  594*Power(t2,3) - 39*Power(t2,4)) + 
               Power(s,2)*(148 - 194*t2 + 96*Power(t2,2) + 
                  328*Power(t2,3) - 39*Power(t2,4) + 
                  s2*(10 + 988*t2 - 314*Power(t2,2) - 69*Power(t2,3))) + 
               s*(366 + 411*t2 - 9338*Power(t2,2) - 1108*Power(t2,3) + 
                  402*Power(t2,4) + 
                  Power(s2,2)*
                   (25 + 238*t2 - 88*Power(t2,2) + 96*Power(t2,3)) + 
                  s2*(-148 + 1656*t2 + 2947*Power(t2,2) - 
                     918*Power(t2,3) + 78*Power(t2,4)))) - 
            Power(t1,3)*(20 + 71*t2 + 4921*Power(t2,2) + 
               3722*Power(t2,3) - 12658*Power(t2,4) - 1220*Power(t2,5) - 
               256*Power(t2,6) - 
               2*Power(s2,3)*t2*
                (47 + 26*t2 - 636*Power(t2,2) + 220*Power(t2,3)) + 
               Power(s,3)*t2*(-87 - 445*t2 + 279*Power(t2,2) + 
                  234*Power(t2,3)) + 
               s2*(88 + 129*t2 + 1648*Power(t2,2) + 8145*Power(t2,3) + 
                  5862*Power(t2,4) - 2508*Power(t2,5)) + 
               Power(s2,2)*(48 + 646*t2 + 2043*Power(t2,2) - 
                  2456*Power(t2,3) - 268*Power(t2,4) - 132*Power(t2,5)) - 
               Power(s,2)*(6 - 15*(9 + 4*s2)*t2 + 
                  (1712 - 2298*s2)*Power(t2,2) + 
                  83*(15 + 8*s2)*Power(t2,3) + 
                  (404 + 908*s2)*Power(t2,4) + 132*Power(t2,5)) + 
               s*(-35 + 430*t2 - 654*Power(t2,2) - 12227*Power(t2,3) + 
                  2236*Power(t2,4) + 1730*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (128 + 693*t2 - 300*Power(t2,2) + 1114*Power(t2,3)) + 
                  s2*(-24 - 336*t2 + 5339*Power(t2,2) + 
                     10957*Power(t2,3) + 851*Power(t2,4) + 264*Power(t2,5)\
)))))*T2q(t2,1 - s1 - t1 + t2))/
     ((s - s2 + t1)*Power(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2,2)*
       Power(-1 + s1 + t1 - t2,2)*Power(s1*t1 - t2,3)*(-1 + t2)*
       (s - s1 + t2)) - (8*(Power(s1,10)*
          (Power(s2,3) + s2*Power(t1,2) - 2*Power(t1,3)) + 
         Power(s1,9)*(Power(s2,3)*(-1 + 3*t1 - 7*t2) + 
            Power(s2,2)*(-3 + 2*Power(t1,2) + t1*(4 + s - t2) + 
               3*s*t2) - s2*t1*
             (11*t1 - 2*Power(t1,2) + s*(3 + t1 - 3*t2) + 2*t2 + 
               4*t1*t2) + Power(t1,2)*
             (-1 + 6*t1 - 5*Power(t1,2) + 6*t2 + 16*t1*t2 + 
               s*(-3 + 2*t1 + 4*t2))) + 
         s1*Power(t2,2)*(-6 + 
            (-11 + 8*Power(s,3) - 2*Power(s,2)*(-2 + s2) - 21*s2 + 
               s*(-27 + 22*s2))*t2 + Power(-1 + s,2)*Power(t1,6)*t2 + 
            (-37 + 28*Power(s,3) - 33*s2 - 19*Power(s2,2) + 
               14*Power(s,2)*(1 + 3*s2) + 
               2*s*(-1 + 9*s2 + 7*Power(s2,2)))*Power(t2,2) + 
            (-20 + 5*Power(s,3) - 33*s2 - 41*Power(s2,2) - 
               3*Power(s2,3) + Power(s,2)*(109 + 77*s2) + 
               s*(58 + 103*s2 - 37*Power(s2,2)))*Power(t2,3) + 
            (1 - 4*Power(s,3) + 59*s2 - 74*Power(s2,2) + Power(s2,3) + 
               Power(s,2)*(125 + 23*s2) - 
               2*s*(-85 + 14*s2 + 12*Power(s2,2)))*Power(t2,4) + 
            (83 + 3*Power(s,3) + Power(s,2)*(58 - 10*s2) - 69*s2 + 
               23*Power(s2,2) - 8*Power(s2,3) + 
               s*(124 - 110*s2 + 15*Power(s2,2)))*Power(t2,5) + 
            (56 - Power(s,3) - 45*s2 + 9*Power(s2,2) + Power(s2,3) + 
               Power(s,2)*(8 + 3*s2) + s*(27 - 17*s2 - 3*Power(s2,2)))*
             Power(t2,6) + (6 - 2*s + 2*s2)*Power(t2,7) + 
            Power(t1,5)*(3 - 3*Power(s,3) + t2 - s2*t2 + 
               4*Power(t2,2) - 
               Power(s,2)*(-9 + s2*t2 + 4*Power(t2,2)) + 
               s*(-9 + (-1 + 2*s2)*t2 + 4*Power(t2,2))) - 
            Power(t1,4)*(18 + (-3 + 13*s2)*t2 + 
               (28 + s2 - 2*Power(s2,2))*Power(t2,2) + 
               (34 - 7*s2 + Power(s2,2))*Power(t2,3) + 
               Power(s,3)*(-9 - 10*t2 + 2*Power(t2,2)) + 
               Power(s,2)*(30 + 3*(1 + 4*s2)*t2 + 
                  (18 - 4*s2)*Power(t2,2) - 8*Power(t2,3)) + 
               s*(-51 + (36 - 37*s2)*t2 + 
                  (-11 - 10*s2 + 2*Power(s2,2))*Power(t2,2) + 
                  (9 + 4*s2)*Power(t2,3))) + 
            Power(t1,3)*(42 + 2*(-7 + 33*s2)*t2 + 
               (-23 - 19*s2 + 23*Power(s2,2))*Power(t2,2) - 
               (-112 + 45*s2 - 3*Power(s2,2) + Power(s2,3))*
                Power(t2,3) + 
               (57 - 19*s2 - 2*Power(s2,2))*Power(t2,4) + 
               Power(s,3)*t2*(11 - 9*t2 + 7*Power(t2,2)) + 
               Power(s,2)*(39 + (8 + 43*s2)*t2 + 
                  (37 + 47*s2)*Power(t2,2) - 
                  16*(-3 + s2)*Power(t2,3) - 10*Power(t2,4)) + 
               s*(-99 - (39 + 109*s2)*t2 - 
                  2*(-89 + 45*s2 + 9*Power(s2,2))*Power(t2,2) + 
                  2*(14 - 10*s2 + 5*Power(s2,2))*Power(t2,3) + 
                  4*(8 + 3*s2)*Power(t2,4))) - 
            Power(t1,2)*(48 + t2 + 110*s2*t2 + 
               (-77 - 8*s2 + 71*Power(s2,2))*Power(t2,2) + 
               (-56 - 28*Power(s2,2) + 13*Power(s2,3))*Power(t2,3) + 
               (91 - 72*s2 + 8*Power(s2,2) - 6*Power(s2,3))*
                Power(t2,4) + 
               (45 - 32*s2 - 5*Power(s2,2))*Power(t2,5) + 
               3*Power(s,3)*(6 + 29*t2 + 15*Power(t2,2) - 
                  Power(t2,3) + 3*Power(t2,4)) + 
               Power(s,2)*(24 + (44 + 40*s2)*t2 + 
                  (87 + 85*s2)*Power(t2,2) + 
                  (68 + 26*s2)*Power(t2,3) + 
                  (34 - 27*s2)*Power(t2,4) - 7*Power(t2,5)) + 
               s*(-81 - (173 + 121*s2)*t2 + 
                  (27 - 53*s2 - 61*Power(s2,2))*Power(t2,2) + 
                  (263 + 19*s2 - 30*Power(s2,2))*Power(t2,3) + 
                  (113 - 22*s2 + 24*Power(s2,2))*Power(t2,4) + 
                  3*(15 + 4*s2)*Power(t2,5))) + 
            t1*(27 + (21 + 79*s2)*t2 + 
               (7 + 45*s2 + 65*Power(s2,2))*Power(t2,2) + 
               (-114 + 71*s2 + 11*Power(s2,2) + 17*Power(s2,3))*
                Power(t2,3) + 
               2*(-64 + 42*s2 - 28*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,4) - 
               (4 + 25*s2 - 7*Power(s2,2) + 7*Power(s2,3))*Power(t2,5) + 
               (11 - 22*s2 - 2*Power(s2,2))*Power(t2,6) + 
               Power(s,3)*(12 + 58*t2 + 70*Power(t2,2) + 
                  29*Power(t2,3) - 4*Power(t2,4) + 5*Power(t2,5)) + 
               Power(s,2)*(6 + 2*(17 + 6*s2)*t2 + 
                  (68 - 3*s2)*Power(t2,2) + (26 - 29*s2)*Power(t2,3) + 
                  (-33 + s2)*Power(t2,4) - (4 + 17*s2)*Power(t2,5) - 
                  2*Power(t2,6)) + 
               s*(-24 - (68 + 73*s2)*t2 + 
                  (-164 + 9*s2 - 55*Power(s2,2))*Power(t2,2) - 
                  3*(24 - 32*s2 + 9*Power(s2,2))*Power(t2,3) + 
                  (106 + 93*s2 - 7*Power(s2,2))*Power(t2,4) + 
                  (48 + 3*s2 + 19*Power(s2,2))*Power(t2,5) + 
                  (22 + 4*s2)*Power(t2,6)))) + 
         Power(s1,8)*(Power(s2,3)*
             (-3 + t1 + 3*Power(t1,2) + 5*t2 - 20*t1*t2 + 
               21*Power(t2,2)) + 
            Power(s2,2)*(3 + 5*Power(t1,3) + 3*Power(t1,2)*(s - 4*t2) + 
               (17 - 7*s)*t2 + (1 - 18*s)*Power(t2,2) + 
               t1*(-15 - 33*t2 + 6*Power(t2,2) + s*(-5 + 6*t2))) + 
            s2*(3 + 2*Power(t1,4) + Power(t2,2) + 
               3*Power(s,2)*Power(t2,2) - 3*s*t2*(1 + t2) - 
               Power(t1,3)*(11 + 3*s + t2) + 
               Power(t1,2)*(20 + s + 58*t2 + 18*s*t2 + 3*Power(t2,2)) + 
               t1*(-(Power(s,2)*t2) + s*(4 + 19*t2 - 17*Power(t2,2)) + 
                  8*(-1 + 3*t2 + Power(t2,2)))) - 
            t1*(4*Power(t1,4) + 2*t2*(-1 + 3*t2) - 
               Power(t1,3)*(7 + 36*t2) + 
               t1*(-11 + 14*t2 + 48*Power(t2,2)) + 
               Power(t1,2)*(20 + 28*t2 + 50*Power(t2,2)) + 
               Power(s,2)*(-3*(-1 + t2)*t2 + t1*(-1 + 5*t2)) + 
               s*(-3 - 5*Power(t1,3) - 3*t2 + 8*Power(t2,2) + 
                  Power(t1,2)*(25 + 7*t2) + 
                  t1*(-2 + 5*t2 + 17*Power(t2,2))))) + 
         Power(s1,7)*(-1 - Power(t1,6) - Power(t2,2) + 2*Power(t2,3) + 
            4*s*Power(t2,3) - 3*Power(s,2)*Power(t2,3) + 
            Power(s,3)*Power(t2,3) + Power(t1,5)*(-4 + 4*s + 27*t2) - 
            2*Power(t1,4)*(20 + 21*t2 + 46*Power(t2,2) + 
               2*s*(11 + 8*t2)) + 
            Power(t1,3)*(41 + Power(s,2)*(9 - 11*t2) + 88*t2 + 
               9*Power(t2,2) + 80*Power(t2,3) + 
               s*(35 + 77*t2 + 23*Power(t2,2))) + 
            Power(s2,3)*(1 + Power(t1,3) + Power(t1,2)*(4 - 19*t2) + 
               21*t2 - 8*Power(t2,2) - 35*Power(t2,3) + 
               t1*(-3 - 11*t2 + 57*Power(t2,2))) + 
            Power(s2,2)*(9 + 4*Power(t1,4) + 
               Power(t1,3)*(-10 + 3*s - 22*t2) - (9 + s)*t2 + 
               (-32 + 33*s)*Power(t2,2) + (-6 + 45*s)*Power(t2,3) + 
               Power(t1,2)*(-11 + s*(-9 + t2) - 13*t2 + 
                  28*Power(t2,2)) - 
               t1*(12 + s - 97*t2 - 16*s*t2 - 116*Power(t2,2) + 
                  51*s*Power(t2,2) + 15*Power(t2,3))) + 
            Power(t1,2)*(-22 - 4*t2 + Power(s,3)*t2 + 111*Power(t2,2) + 
               150*Power(t2,3) + 
               Power(s,2)*(6 - 7*t2 + 30*Power(t2,2)) + 
               s*(19 + 73*t2 + 50*Power(t2,2) + 26*Power(t2,3))) + 
            t1*(4 - 23*t2 + 10*Power(t2,2) - 2*Power(s,3)*Power(t2,2) + 
               48*Power(t2,3) + 
               Power(s,2)*t2*(8 + 17*t2 - 13*Power(t2,2)) + 
               s*(-5 - 21*t2 + 21*Power(t2,2) + 34*Power(t2,3))) + 
            s2*(-3 + 2*Power(t1,5) + (-13 + 8*s)*t2 + 
               (-13 + 18*s - 8*Power(s,2))*Power(t2,2) + 
               (-4 + 17*s - 15*Power(s,2))*Power(t2,3) + 
               Power(t1,4)*(-4*s + 5*(3 + t2)) + 
               Power(t1,3)*(10 + 31*t2 - 24*Power(t2,2) + 
                  s*(6 + 29*t2)) - 
               Power(t1,2)*(17 + 91*t2 + 151*Power(t2,2) - 
                  10*Power(t2,3) + Power(s,2)*(2 + 4*t2) + 
                  s*(15 + 11*t2 + 70*Power(t2,2))) + 
               t1*(21 + 18*t2 - 134*Power(t2,2) - 6*Power(t2,3) + 
                  Power(s,2)*t2*(-6 + 17*t2) + 
                  s*(16 - 73*t2 - 81*Power(t2,2) + 40*Power(t2,3))))) + 
         Power(t2,3)*(2 + 4*t2 + 5*s2*t2 + 20*Power(t2,2) + 
            7*s2*Power(t2,2) + 3*Power(s2,2)*Power(t2,2) + 
            11*Power(t2,3) + 7*s2*Power(t2,3) + 
            10*Power(s2,2)*Power(t2,3) - 5*Power(t2,4) - 
            10*s2*Power(t2,4) + 17*Power(s2,2)*Power(t2,4) - 
            23*Power(t2,5) + 16*s2*Power(t2,5) - 
            2*Power(s2,2)*Power(t2,5) + 2*Power(s2,3)*Power(t2,5) - 
            9*Power(t2,6) + 7*s2*Power(t2,6) - Power(t1,5)*(1 + t2) + 
            Power(t1,4)*(6 + t2 + 4*s2*t2 + (-1 + s2)*Power(t2,2)) - 
            Power(t1,3)*(14 + t2 + 17*s2*t2 + 
               (-7 - s2 + 5*Power(s2,2))*Power(t2,2) + 
               2*(-7 + 2*s2)*Power(t2,3)) + 
            Power(t1,2)*(16 + (7 + 27*s2)*t2 + 
               (9 + 2*s2 + 13*Power(s2,2))*Power(t2,2) + 
               (-50 + 21*s2 - 2*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) + 2*(-11 + 2*s2 + Power(s2,2))*Power(t2,4)) \
- t1*(9 + (10 + 19*s2)*t2 + (35 + 11*s2 + 11*Power(s2,2))*Power(t2,2) + 
               (-25 + 24*s2 + 8*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) + 
               (-50 + 22*s2 - Power(s2,2) + 3*Power(s2,3))*
                Power(t2,4) + (-19 + 8*s2 + 2*Power(s2,2))*Power(t2,5)) \
+ Power(s,3)*(-4 + Power(t1,5) - 22*t2 - 24*Power(t2,2) - Power(t2,3) + 
               2*Power(t2,4) - Power(t2,5) - Power(t1,4)*(3 + 2*t2) + 
               Power(t1,3)*(2 + 3*t2 + Power(t2,2)) - 
               Power(t1,2)*(2 + 9*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               t1*(6 + 30*t2 + 8*Power(t2,2) - 7*Power(t2,3) + 
                  2*Power(t2,4))) - 
            Power(s,2)*(2 - 2*(-6 + s2)*t2 + 18*(2 + s2)*Power(t2,2) + 
               (63 + 20*s2)*Power(t2,3) + 2*(16 + s2)*Power(t2,4) + 
               (7 - 4*s2)*Power(t2,5) + Power(t1,5)*(3 + t2) - 
               2*Power(t1,4)*(6 + (3 + 2*s2)*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(17 + 2*(11 + 6*s2)*t2 + 
                  (-1 + 7*s2)*Power(t2,2) + 6*Power(t2,3)) - 
               Power(t1,2)*(8 + 3*(9 + 4*s2)*t2 - 12*Power(t2,2) + 
                  3*(-5 + 2*s2)*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(-2 + (-2 + 6*s2)*t2 - (41 + 24*s2)*Power(t2,2) - 
                  2*(27 + 7*s2)*Power(t2,3) + 
                  (-18 + 7*s2)*Power(t2,4) + Power(t2,5))) + 
            s*(2 + (23 - 10*s2)*t2 + 
               (1 - 13*s2 - 2*Power(s2,2))*Power(t2,2) + 
               (-37 - 39*s2 + 9*Power(s2,2))*Power(t2,3) - 
               (66 + s2)*Power(t2,4) + 
               (-30 + 11*s2 - 5*Power(s2,2))*Power(t2,5) - 
               5*Power(t2,6) + Power(t1,5)*(3 + 2*t2) - 
               Power(t1,4)*(15 + (5 + 8*s2)*t2 + (5 + s2)*Power(t2,2)) + 
               Power(t1,3)*(25 + 5*(6 + 5*s2)*t2 + 
                  (6 + 4*s2 + 5*Power(s2,2))*Power(t2,2) + 
                  (4 + 5*s2)*Power(t2,3)) - 
               Power(t1,2)*(15 + 6*(5 + 6*s2)*t2 + 
                  (72 - 5*s2 + 13*Power(s2,2))*Power(t2,2) + 
                  (15 - s2 + 8*Power(s2,2))*Power(t2,3) + 
                  (6 + 7*s2)*Power(t2,4)) + 
               t1*t2*(-20 + 70*t2 + 91*Power(t2,2) + 41*Power(t2,3) + 
                  10*Power(t2,4) + 
                  Power(s2,2)*t2*(10 + 3*t2 + 8*Power(t2,2)) + 
                  s2*(29 + 5*t2 + 7*Power(t2,2) - 8*Power(t2,3) + 
                     3*Power(t2,4))))) - 
         Power(s1,6)*(-1 - 3*t2 + s*t2 - 12*Power(t2,2) - 
            s*Power(t2,2) + 2*Power(t2,3) + 18*s*Power(t2,3) - 
            3*Power(s,2)*Power(t2,3) + 2*Power(s,3)*Power(t2,3) + 
            16*Power(t2,4) + 17*s*Power(t2,4) - 
            13*Power(s,2)*Power(t2,4) + 4*Power(s,3)*Power(t2,4) - 
            Power(t1,6)*(-5 + s + 7*t2) + 
            Power(t1,5)*(30 + 3*t2 + 53*Power(t2,2) + s*(21 + 32*t2)) + 
            Power(t1,4)*(-44 + 6*Power(s,2)*(-4 + t2) - 178*t2 - 
               49*Power(t2,2) - 110*Power(t2,3) - 
               s*(73 + 136*t2 + 87*Power(t2,2))) + 
            Power(t1,2)*(-17 - 13*t2 + 152*Power(t2,2) + 
               253*Power(t2,3) + 240*Power(t2,4) + 
               2*Power(s,3)*t2*(-2 + 5*t2) + 
               s*t2*(203 + 297*t2 + 128*Power(t2,2) + 14*Power(t2,3)) + 
               2*Power(s,2)*(6 + 11*t2 - 16*Power(t2,2) + 
                  33*Power(t2,3))) + 
            Power(s2,3)*(-2 + 5*t2 + Power(t1,2)*(23 - 50*t2)*t2 + 
               62*Power(t2,2) + Power(t2,3) - 35*Power(t2,4) + 
               Power(t1,3)*(-2 + 6*t2) + 
               t1*(4 - 13*t2 - 47*Power(t2,2) + 90*Power(t2,3))) + 
            Power(t1,3)*(-(Power(s,3)*(2 + 3*t2)) + 
               Power(s,2)*(-15 + 39*t2 - 38*Power(t2,2)) + 
               s*(-1 + 12*t2 + 52*Power(t2,2) + 59*Power(t2,3)) + 
               2*(9 + 26*t2 + 42*Power(t2,2) - 58*Power(t2,3) + 
                  35*Power(t2,4))) + 
            t1*(9 - 15*t2 - 74*Power(t2,2) + 118*Power(t2,3) - 
               11*Power(s,3)*Power(t2,3) + 150*Power(t2,4) + 
               2*Power(s,2)*t2*
                (3 + 28*t2 + 29*Power(t2,2) - 11*Power(t2,3)) + 
               s*(11 - 27*t2 + 26*Power(t2,2) + 119*Power(t2,3) + 
                  52*Power(t2,4))) - 
            Power(s2,2)*(-3 + Power(t1,5) + 
               Power(t1,4)*(-6 + s - 15*t2) + (-54 + 13*s)*t2 + 
               (-13 + 17*s)*Power(t2,2) + (1 - 57*s)*Power(t2,3) + 
               (15 - 60*s)*Power(t2,4) + 
               Power(t1,2)*(-13 + s + 74*t2 + 38*s*t2 + 
                  60*Power(t2,2) - 48*s*Power(t2,2) - 30*Power(t2,3)) + 
               Power(t1,3)*(6 + 35*t2 + 35*Power(t2,2) - 
                  s*(3 + 5*t2)) + 
               t1*(15 + 100*t2 - 257*Power(t2,2) - 223*Power(t2,3) + 
                  20*Power(t2,4) + 
                  s*(5 + 4*t2 - 28*Power(t2,2) + 130*Power(t2,3)))) + 
            s2*(9 - Power(t1,6) + Power(t1,5)*(-20 + 3*s - 3*t2) + 
               (-3 + 4*s)*t2 + (-25 + 6*s - 6*Power(s,2))*Power(t2,2) + 
               (-74 + 27*s - 31*Power(s,2))*Power(t2,3) + 
               (-3 + 40*s - 30*Power(s,2))*Power(t2,4) + 
               Power(t1,4)*(24 + s*(11 - 21*t2) + 84*t2 + 
                  39*Power(t2,2)) + 
               Power(t1,3)*(7 + 79*t2 + 54*Power(t2,2) - 
                  66*Power(t2,3) + Power(s,2)*(8 + 5*t2) + 
                  s*(29 + 23*t2 + 79*Power(t2,2))) - 
               Power(t1,2)*(-2 + 61*t2 + 219*Power(t2,2) + 
                  267*Power(t2,3) - 25*Power(t2,4) + 
                  Power(s,2)*(2 - 9*t2 + 35*Power(t2,2)) + 
                  s*(27 + 10*t2 - 11*Power(t2,2) + 116*Power(t2,3))) + 
               t1*(-21 + 100*t2 - 31*Power(t2,2) - 335*Power(t2,3) + 
                  20*Power(t2,4) + 
                  2*Power(s,2)*t2*(-9 - 8*t2 + 32*Power(t2,2)) + 
                  s*(10 + 38*t2 - 350*Power(t2,2) - 219*Power(t2,3) + 
                     50*Power(t2,4))))) + 
         Power(s1,2)*t2*(6 + (9 + 33*s2 - 3*s*(-5 + 6*s2))*t2 + 
            (14 + Power(s,2)*(22 - 28*s2) + 48*s2 + 45*Power(s2,2) + 
               s*(-2 + 6*s2 - 28*Power(s2,2)))*Power(t2,2) + 
            (-4 + 3*Power(s,3) + 37*s2 + 63*Power(s2,2) + 
               14*Power(s2,3) - Power(s,2)*(31 + 101*s2) + 
               s*(-25 - 61*s2 + 57*Power(s2,2)))*Power(t2,3) + 
            (26 + 4*Power(s,3) - 141*s2 + 107*Power(s2,2) - 
               5*Power(s2,3) - Power(s,2)*(171 + 59*s2) + 
               s*(-136 + 119*s2 + 106*Power(s2,2)))*Power(t2,4) + 
            (-101 - 5*Power(s,3) + 118*s2 - 91*Power(s2,2) + 
               4*Power(s2,3) + Power(s,2)*(-143 + 3*s2) + 
               s*(-218 + 354*s2 + 3*Power(s2,2)))*Power(t2,5) + 
            (-142 + 3*Power(s,3) + 132*s2 - 53*Power(s2,2) - 
               7*Power(s2,3) - Power(s,2)*(31 + 13*s2) + 
               s*(-73 + 83*s2 + 17*Power(s2,2)))*Power(t2,6) + 
            (-32 + Power(s,2) + s*(7 - 2*s2) - 11*s2 + Power(s2,2))*
             Power(t2,7) + Power(t1,6)*t2*
             (-3 - 3*Power(s,2) - (3 + s2)*t2 + s*(6 + t2 + s2*t2)) + 
            Power(t1,5)*(-3 + 3*Power(s,3) + 3*(1 + s2)*t2 + 
               (21 - 4*s2)*Power(t2,2) + 
               (27 - 6*s2 + Power(s2,2))*Power(t2,3) + 
               Power(s,2)*(-15 + 3*(-2 + s2)*t2 + 25*Power(t2,2) - 
                  2*Power(t2,3)) - 
               s*(-3 + 9*(-3 + 2*s2)*t2 + 3*(6 + 5*s2)*Power(t2,2) + 
                  (-8 + s2)*Power(t2,3))) + 
            Power(t1,4)*(18 + 15*(-1 + s2)*t2 + 
               (70 + 6*s2 - 9*Power(s2,2))*Power(t2,2) + 
               (-73 + 46*s2)*Power(t2,3) + (-47 + 24*s2)*Power(t2,4) - 
               Power(s,3)*(15 + 42*t2 - 8*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s,2)*(42 + (75 + 6*s2)*t2 - 
                  (39 + 25*s2)*Power(t2,2) + 
                  (-49 + 4*s2)*Power(t2,3) + 6*Power(t2,4)) - 
               s*(27 + 3*(-22 + 9*s2)*t2 + 
                  (161 - 83*s2 - 7*Power(s2,2))*Power(t2,2) + 
                  (11 - 29*s2 + 2*Power(s2,2))*Power(t2,3) + 
                  5*(8 + s2)*Power(t2,4))) + 
            Power(t1,3)*(-42 + (48 - 96*s2)*t2 + 
               (-80 + 78*s2 - 38*Power(s2,2))*Power(t2,2) + 
               (-264 + 101*s2 - 14*Power(s2,2) + 6*Power(s2,3))*
                Power(t2,3) + 
               (14 - 45*s2 + 7*Power(s2,2) - 2*Power(s2,3))*
                Power(t2,4) + 
               (22 - 36*s2 - 3*Power(s2,2))*Power(t2,5) + 
               3*Power(s,3)*(8 + 37*t2 + 19*Power(t2,2) - 
                  9*Power(t2,3) + 2*Power(t2,4)) - 
               Power(s,2)*(39 + (66 + 45*s2)*t2 + 
                  (-43 + 37*s2)*Power(t2,2) - 
                  (25 + 63*s2)*Power(t2,3) + 
                  (-29 + 14*s2)*Power(t2,4) + 6*Power(t2,5)) + 
               s*(63 + 9*(-15 + 17*s2)*t2 + 
                  (-269 + 113*s2 + 17*Power(s2,2))*Power(t2,2) + 
                  (208 - 44*s2 - 33*Power(s2,2))*Power(t2,3) + 
                  (19 - 18*s2 + 10*Power(s2,2))*Power(t2,4) + 
                  (56 + 9*s2)*Power(t2,5))) + 
            Power(t1,2)*(48 + 3*(-13 + 56*s2)*t2 + 
               (-105 - 105*s2 + 148*Power(s2,2))*Power(t2,2) + 
               (140 - 189*s2 - 93*Power(s2,2) + 32*Power(s2,3))*
                Power(t2,3) + 
               (364 - 267*s2 + 36*Power(s2,2) - 31*Power(s2,3))*
                Power(t2,4) + 
               (116 - 79*s2 - 12*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,5) + 
               (19 + 23*s2 + 2*Power(s2,2))*Power(t2,6) - 
               Power(s,3)*(12 + 45*t2 + 39*Power(t2,2) + 
                  5*Power(t2,3) - 33*Power(t2,4) + 6*Power(t2,5)) - 
               s*(57 + 3*(17 + 57*s2)*t2 + 
                  (-326 + 184*s2 + 93*Power(s2,2))*Power(t2,2) + 
                  (-570 - 64*s2 + 51*Power(s2,2))*Power(t2,3) + 
                  (-168 + 95*s2 - 89*Power(s2,2))*Power(t2,4) + 
                  (-99 + 3*s2 + 14*Power(s2,2))*Power(t2,5) + 
                  (29 + 4*s2)*Power(t2,6)) + 
               Power(s,2)*(12 - 39*Power(t2,2) + 32*Power(t2,3) + 
                  87*Power(t2,4) + 5*Power(t2,5) + 2*Power(t2,6) + 
                  s2*t2*(48 + 165*t2 + 27*Power(t2,2) - 
                     94*Power(t2,3) + 16*Power(t2,4)))) + 
            t1*(-27 - 3*(1 + 41*s2)*t2 + 
               (83 - 22*s2 - 146*Power(s2,2))*Power(t2,2) + 
               (174 + 11*s2 + 43*Power(s2,2) - 52*Power(s2,3))*
                Power(t2,3) + 
               (126 - 159*s2 + 270*Power(s2,2) - 25*Power(s2,3))*
                Power(t2,4) + 
               (-163 + 207*s2 - 27*Power(s2,2) + 41*Power(s2,3))*
                Power(t2,5) - 
               (94 - 127*s2 + Power(s2,2) + 2*Power(s2,3))*Power(t2,6) - 
               2*(9 + 2*s2)*Power(t2,7) + 
               Power(s,3)*t2*
                (-24 - 94*t2 - 53*Power(t2,2) - 8*Power(t2,3) - 
                  17*Power(t2,4) + 2*Power(t2,5)) + 
               s*(18 + 9*(8 + 9*s2)*t2 + 
                  (123 - 4*s2 + 97*Power(s2,2))*Power(t2,2) + 
                  (-105 - 377*s2 + 89*Power(s2,2))*Power(t2,3) - 
                  (530 + 259*s2 + 43*Power(s2,2))*Power(t2,4) + 
                  (-294 + 62*s2 - 90*Power(s2,2))*Power(t2,5) + 
                  3*(-34 + 3*s2 + 2*Power(s2,2))*Power(t2,6) + 
                  4*Power(t2,7)) - 
               Power(s,2)*t2*(4*t2*
                   (8 + 35*t2 + 14*Power(t2,2) + 9*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  s2*(12 + 85*t2 + 23*Power(t2,2) - 49*Power(t2,3) - 
                     66*Power(t2,4) + 6*Power(t2,5))))) + 
         Power(s1,5)*(3 + t2 + 5*s*t2 - 14*Power(t2,2) - 
            7*s*Power(t2,2) - 50*Power(t2,3) + 23*s*Power(t2,3) + 
            15*Power(s,2)*Power(t2,3) + 2*Power(s,3)*Power(t2,3) + 
            40*Power(t2,4) + 76*s*Power(t2,4) - 
            2*Power(s,2)*Power(t2,4) + 6*Power(s,3)*Power(t2,4) + 
            50*Power(t2,5) + 26*s*Power(t2,5) - 
            22*Power(s,2)*Power(t2,5) + 6*Power(s,3)*Power(t2,5) + 
            Power(t1,6)*(-11 + s + 7*t2 - 11*s*t2 - 9*Power(t2,2)) + 
            Power(t1,5)*(11 + 97*t2 + 15*Power(t2,2) + 41*Power(t2,3) + 
               Power(s,2)*(19 + 4*t2) + s*(52 + 53*t2 + 62*Power(t2,2))) \
+ Power(s2,3)*t2*(-11 + 11*t2 + 98*Power(t2,2) + 20*Power(t2,3) - 
               21*Power(t2,4) + Power(t1,3)*(-9 + 14*t2) + 
               Power(t1,2)*(-5 + 58*t2 - 70*Power(t2,2)) + 
               5*t1*(5 - 4*t2 - 21*Power(t2,2) + 17*Power(t2,3))) + 
            Power(t1,4)*(10 - 7*t2 - 247*Power(t2,2) + 15*Power(t2,3) - 
               63*Power(t2,4) + Power(s,3)*(2 + 4*t2) + 
               Power(s,2)*(11 - 61*t2 + 4*Power(t2,2)) - 
               s*(48 + 161*t2 + 96*Power(t2,2) + 119*Power(t2,3))) - 
            Power(t1,3)*(13 + 82*t2 + 215*Power(t2,2) + 
               58*Power(t2,3) + 212*Power(t2,4) - 32*Power(t2,5) + 
               Power(s,3)*t2*(-12 + 13*t2) + 
               Power(s,2)*(26 + 66*t2 - 76*Power(t2,2) + 
                  46*Power(t2,3)) + 
               s*(-23 + 219*t2 + 221*Power(t2,2) + 112*Power(t2,3) - 
                  79*Power(t2,4))) + 
            Power(t1,2)*(10 - 39*t2 + 44*Power(t2,2) + 
               349*Power(t2,3) + 229*Power(t2,4) + 210*Power(t2,5) + 
               2*Power(s,3)*t2*(-8 - 9*t2 + 15*Power(t2,2)) + 
               Power(s,2)*(-2 + 26*t2 + 33*Power(t2,2) - 
                  78*Power(t2,3) + 66*Power(t2,4)) + 
               s*(-39 + 29*t2 + 550*Power(t2,2) + 469*Power(t2,3) + 
                  208*Power(t2,4) - 4*Power(t2,5))) + 
            t1*(-10 + 23*t2 - 57*Power(t2,2) - 3*Power(t2,3) + 
               335*Power(t2,4) + 240*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(2 + 2*t2 - 23*Power(t2,2)) + 
               Power(s,2)*t2*
                (-8 + 37*t2 + 162*Power(t2,2) + 116*Power(t2,3) - 
                  18*Power(t2,4)) + 
               s*(11 + 46*t2 + 33*Power(t2,2) + 264*Power(t2,3) + 
                  237*Power(t2,4) + 28*Power(t2,5))) - 
            Power(s2,2)*(6 + (-9 + 2*s)*t2 + 4*Power(t1,5)*t2 + 
               (-121 + 73*s)*Power(t2,2) + (-97 + 63*s)*Power(t2,3) - 
               5*(18 + 7*s)*Power(t2,4) - 5*(-4 + 9*s)*Power(t2,5) + 
               Power(t1,3)*(1 + s + 13*t2 - 20*s*t2 + 46*Power(t2,2) + 
                  13*s*Power(t2,2) + 20*Power(t2,3)) + 
               Power(t1,2)*(13 + 6*(-10 + s)*t2 + 
                  (180 + 77*s)*Power(t2,2) + 
                  (105 - 122*s)*Power(t2,3) - 10*Power(t2,4)) + 
               t1*(-17 + (73 + 33*s)*t2 + (336 - 10*s)*Power(t2,2) - 
                  (357 + 71*s)*Power(t2,3) + 
                  5*(-50 + 33*s)*Power(t2,4) + 15*Power(t2,5)) + 
               Power(t1,4)*(s*(-1 + 3*t2) - 
                  3*(1 + 7*t2 + 7*Power(t2,2)))) + 
            s2*(3 + (45 - 14*s)*t2 + 
               (33 - 19*s + 2*Power(s,2))*Power(t2,2) - 
               (34 + 120*s + 15*Power(s,2))*Power(t2,3) - 
               (183 + 29*s + 41*Power(s,2))*Power(t2,4) + 
               (10 + 50*s - 30*Power(s,2))*Power(t2,5) + 
               Power(t1,6)*(5 - s + t2) - 
               Power(t1,5)*(10 + s*(17 - 6*t2) + 72*t2 + 
                  21*Power(t2,2)) + 
               Power(t1,4)*(1 + 35*t2 + 138*Power(t2,2) + 
                  67*Power(t2,3) - Power(s,2)*(7 + 4*t2) + 
                  s*(3 + 32*t2 - 36*Power(t2,2))) + 
               Power(t1,3)*(-7 + 59*t2 + 279*Power(t2,2) + 
                  122*Power(t2,3) - 74*Power(t2,4) + 
                  Power(s,2)*(8 - t2 + 29*Power(t2,2)) + 
                  s*(13 + 137*t2 + 23*Power(t2,2) + 84*Power(t2,3))) + 
               Power(t1,2)*(29 + 69*t2 - 54*Power(t2,2) - 
                  328*Power(t2,3) - 333*Power(t2,4) + 24*Power(t2,5) + 
                  Power(s,2)*t2*(39 + 52*t2 - 97*Power(t2,2)) + 
                  s*(15 - 78*t2 + 119*Power(t2,2) + 128*Power(t2,3) - 
                     89*Power(t2,4))) + 
               t1*(-21 - 137*t2 + 213*Power(t2,2) - 205*Power(t2,3) - 
                  502*Power(t2,4) + 50*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-4 - 64*t2 - 27*Power(t2,2) + 106*Power(t2,3)) + 
                  s*(-13 + 73*t2 + 54*Power(t2,2) - 741*Power(t2,3) - 
                     337*Power(t2,4) + 35*Power(t2,5))))) + 
         Power(s1,3)*(-2 + (-1 - 23*s2 + s*(-5 + 6*s2))*t2 + 
            (12 - 22*s2 - 51*Power(s2,2) + Power(s,2)*(-2 + 4*s2) + 
               2*s*(-2 - 9*s2 + 11*Power(s2,2)))*Power(t2,2) - 
            (-23 + 5*Power(s,3) + Power(s,2)*(17 - 51*s2) - 24*s2 + 
               40*Power(s2,2) + 26*Power(s2,3) + 
               s*(-7 + 45*s2 + 41*Power(s2,2)))*Power(t2,3) + 
            (-48 - 2*Power(s,3) + 175*s2 - 20*Power(s2,2) + 
               11*Power(s2,3) + Power(s,2)*(99 + 59*s2) + 
               s*(13 - 177*s2 - 187*Power(s2,2)))*Power(t2,4) + 
            (19 + 7*Power(s,3) - 104*s2 + 177*Power(s2,2) + 
               33*Power(s2,3) + 5*Power(s,2)*(31 + s2) + 
               s*(209 - 530*s2 - 63*Power(s2,2)))*Power(t2,5) + 
            (185 - Power(s,3) - 231*s2 + 128*Power(s2,2) + 
               20*Power(s2,3) + 14*Power(s,2)*(3 + s2) + 
               s*(125 - 156*s2 - 33*Power(s2,2)))*Power(t2,6) + 
            (70 + Power(s,3) + 24*s2 - 6*Power(s2,2) - Power(s2,3) - 
               Power(s,2)*(7 + 3*s2) + s*(-4 + 13*s2 + 3*Power(s2,2)))*
             Power(t2,7) + Power(t1,6)*
             (Power(s,2)*(2 + 7*t2 - 2*Power(t2,2)) + 
               t2*(3 + t2 + 7*s2*t2 + (-7 + 3*s2)*Power(t2,2)) + 
               s*(2 + 2*(-9 + 2*s2)*t2 + (-5 + s2)*Power(t2,2) - 
                  3*Power(t2,3))) + 
            Power(t1,5)*(1 - (5 + 3*s2)*t2 - (79 + 2*s2)*Power(t2,2) + 
               (7 - 20*s2 - 4*Power(s2,2))*Power(t2,3) - 
               9*(-1 + s2)*Power(t2,4) + Power(s,3)*(1 + 8*t2) + 
               Power(s,2)*(1 - (38 + s2)*t2 + 
                  (-29 + 2*s2)*Power(t2,2) + 10*Power(t2,3)) + 
               s*(-9 + 7*(-5 + 2*s2)*t2 + (106 - 3*s2)*Power(t2,2) + 
                  (-13 + 2*s2)*Power(t2,3) + 14*Power(t2,4))) + 
            Power(t1,4)*(-6 + (17 - 7*s2)*t2 + 
               3*(16 - 4*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (269 - 126*s2 + 12*Power(s2,2))*Power(t2,3) + 
               (53 - 33*s2 + 3*Power(s2,2))*Power(t2,4) + 
               3*(5 + 4*s2)*Power(t2,5) + 
               Power(s,3)*(-3 - 16*t2 - 28*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s,2)*(-14 + (-25 + 2*s2)*t2 + 
                  (127 + 25*s2)*Power(t2,2) + 
                  (49 - 12*s2)*Power(t2,3) - 16*Power(t2,4)) + 
               s*(21 + (56 - 5*s2)*t2 + 
                  (171 - 169*s2 - 7*Power(s2,2))*Power(t2,2) + 
                  (-16 - 36*s2 + 3*Power(s2,2))*Power(t2,3) + 
                  4*(26 + s2)*Power(t2,4) - 21*Power(t2,5))) - 
            Power(t1,3)*(-14 + (50 - 62*s2)*t2 - 
               2*(34 - 55*s2 + 13*Power(s2,2))*Power(t2,2) + 
               2*(-49 + 3*s2 - 9*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,3) + 
               (355 - 298*s2 + 14*Power(s2,2) - 9*Power(s2,3))*
                Power(t2,4) + 
               (86 - 135*s2 - 10*Power(s2,2))*Power(t2,5) + 
               (35 + 8*s2)*Power(t2,6) + 
               Power(s,3)*(-2 + 17*t2 + 19*Power(t2,2) - 
                  43*Power(t2,3) + 21*Power(t2,4)) + 
               Power(s,2)*(-17 - (96 + 13*s2)*t2 + 
                  3*(29 + 9*s2)*Power(t2,2) + 
                  (178 + 95*s2)*Power(t2,3) + 
                  (19 - 47*s2)*Power(t2,4) - 9*Power(t2,5)) + 
               s*(29 + (-27 + 79*s2)*t2 + 
                  (-143 - 41*s2 + Power(s2,2))*Power(t2,2) + 
                  (573 - 207*s2 - 50*Power(s2,2))*Power(t2,3) + 
                  (234 + 5*s2 + 34*Power(s2,2))*Power(t2,4) + 
                  (197 + 17*s2)*Power(t2,5) - 12*Power(t2,6))) + 
            Power(t1,2)*(-16 + (53 - 114*s2)*t2 + 
               (43 + 207*s2 - 148*Power(s2,2))*Power(t2,2) + 
               (-313 + 359*s2 + 144*Power(s2,2) - 38*Power(s2,3))*
                Power(t2,3) + 
               (-393 + 319*s2 - 121*Power(s2,2) + 68*Power(s2,3))*
                Power(t2,4) - 
               (20 + 21*s2 + 15*Power(s2,2) + 23*Power(s2,3))*
                Power(t2,5) - 
               (43 + 121*s2 + 8*Power(s2,2))*Power(t2,6) + 
               2*(9 + s2)*Power(t2,7) + 
               Power(s,3)*t2*
                (25 + 97*t2 + 19*Power(t2,2) - 43*Power(t2,3) + 
                  25*Power(t2,4)) - 
               Power(s,2)*(6 + 4*(9 + 5*s2)*t2 + 
                  (-95 + 71*s2)*Power(t2,2) - 
                  (170 + 43*s2)*Power(t2,3) + 
                  (57 - 135*s2)*Power(t2,4) + (62 + 71*s2)*Power(t2,5)) \
+ s*(21 + (3 + 103*s2)*t2 + (-442 + 184*s2 + 55*Power(s2,2))*
                   Power(t2,2) + 
                  (-452 - 99*s2 + 40*Power(s2,2))*Power(t2,3) + 
                  (188 + 246*s2 - 134*Power(s2,2))*Power(t2,4) + 
                  (39 + 119*s2 + 69*Power(s2,2))*Power(t2,5) + 
                  (125 + 8*s2)*Power(t2,6) - 2*Power(t2,7))) + 
            t1*(9 + 17*(-1 + 5*s2)*t2 + 
               (-93 - 68*s2 + 158*Power(s2,2))*Power(t2,2) + 
               (-77 - 234*s2 - 130*Power(s2,2) + 78*Power(s2,3))*
                Power(t2,3) + 
               (-71 + 230*s2 - 548*Power(s2,2) + 17*Power(s2,3))*
                Power(t2,4) + 
               (338 - 428*s2 + 115*Power(s2,2) - 101*Power(s2,3))*
                Power(t2,5) + 
               (292 - 326*s2 + 48*Power(s2,2) + 15*Power(s2,3))*
                Power(t2,6) + (96 + 22*s2 - Power(s2,2))*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*
                (2 + 13*t2 + 16*Power(t2,2) + 21*Power(t2,3) - 
                  11*Power(t2,4)) + 
               s*(-6 - (28 + 43*s2)*t2 + 
                  (31 - 36*s2 - 69*Power(s2,2))*Power(t2,2) + 
                  (190 + 491*s2 - 132*Power(s2,2))*Power(t2,3) + 
                  (670 + 297*s2 + 90*Power(s2,2))*Power(t2,4) + 
                  (565 - 389*s2 + 166*Power(s2,2))*Power(t2,5) + 
                  (212 - 111*s2 - 41*Power(s2,2))*Power(t2,6) + 
                  2*(-7 + s2)*Power(t2,7)) + 
               Power(s,2)*t2*(-4 - 82*t2 - 4*Power(t2,2) + 
                  148*Power(t2,3) + 162*Power(t2,4) + 61*Power(t2,5) - 
                  Power(t2,6) + 
                  s2*(6 + 77*t2 + 73*Power(t2,2) - 101*Power(t2,3) - 
                     94*Power(t2,4) + 37*Power(t2,5))))) - 
         Power(s1,4)*(1 + 12*t2 - s*t2 + 12*Power(t2,2) + 
            10*s*Power(t2,2) - 2*Power(s,2)*Power(t2,2) - 
            38*Power(t2,3) - 27*s*Power(t2,3) + 
            21*Power(s,2)*Power(t2,3) - 60*Power(t2,4) + 
            109*s*Power(t2,4) + 78*Power(s,2)*Power(t2,4) + 
            6*Power(s,3)*Power(t2,4) + 127*Power(t2,5) + 
            132*s*Power(t2,5) + 20*Power(s,2)*Power(t2,5) + 
            5*Power(s,3)*Power(t2,5) + 80*Power(t2,6) + 
            14*s*Power(t2,6) - 18*Power(s,2)*Power(t2,6) + 
            4*Power(s,3)*Power(t2,6) - 
            Power(t1,6)*(-1 + 13*t2 + 5*Power(t2,2) + 3*Power(t2,3) + 
               Power(s,2)*(3 + 4*t2) + s*(20 - 3*t2 + 11*Power(t2,2))) + 
            Power(s2,3)*Power(t2,2)*
             (-24 + 16*Power(t1,3)*(-1 + t2) + 14*t2 + 85*Power(t2,2) + 
               29*Power(t2,3) - 7*Power(t2,4) + 
               Power(t1,2)*(-22 + 82*t2 - 55*Power(t2,2)) + 
               t1*(62 - 7*t2 - 135*Power(t2,2) + 48*Power(t2,3))) + 
            Power(t1,5)*(-2 - 43*t2 - 2*Power(s,3)*t2 + 
               101*Power(t2,2) + 17*Power(t2,3) + 11*Power(t2,4) + 
               Power(s,2)*(-13 + 19*t2 + 12*Power(t2,2)) + 
               s*(29 + 108*t2 + 15*Power(t2,2) + 48*Power(t2,3))) + 
            Power(t1,4)*(6 + 99*t2 + 199*Power(t2,2) - 103*Power(t2,3) + 
               44*Power(t2,4) - 14*Power(t2,5) + 
               2*Power(s,3)*(-2 - 8*t2 + 3*Power(t2,2)) + 
               Power(s,2)*(13 + 75*t2 - 29*Power(t2,2) - 
                  12*Power(t2,3)) + 
               s*(-9 + 58*t2 - 94*Power(t2,2) + 60*Power(t2,3) - 
                  80*Power(t2,4))) - 
            Power(t1,3)*(17 + 41*t2 + 140*Power(t2,2) + 
               510*Power(t2,3) + 138*Power(t2,4) + 144*Power(t2,5) - 
               6*Power(t2,6) + 
               Power(s,3)*(-4 - 34*t2 - 45*Power(t2,2) + 
                  25*Power(t2,3)) + 
               2*Power(s,2)*(-8 + 16*t2 + 81*Power(t2,2) - 
                  23*Power(t2,3) + 8*Power(t2,4)) + 
               s*(-3 + 39*t2 + 591*Power(t2,2) + 381*Power(t2,3) + 
                  253*Power(t2,4) - 50*Power(t2,5))) + 
            t1*(-9 - 48*t2 + 22*Power(t2,2) - 61*Power(t2,3) + 
               238*Power(t2,4) + 438*Power(t2,5) + 210*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*
                (-10 + 8*t2 + 11*Power(t2,2) - 23*Power(t2,3)) + 
               Power(s,2)*t2*
                (-13 - 72*t2 + 108*Power(t2,2) + 235*Power(t2,3) + 
                  123*Power(t2,4) - 7*Power(t2,5)) + 
               s*(-8 + 35*t2 + 127*Power(t2,2) + 342*Power(t2,3) + 
                  549*Power(t2,4) + 274*Power(t2,5) - 8*Power(t2,6))) + 
            Power(t1,2)*(20 + 34*t2 - 189*Power(t2,2) - 
               107*Power(t2,3) + 255*Power(t2,4) + 48*Power(t2,5) + 
               96*Power(t2,6) + 
               Power(s,3)*t2*
                (2 - 12*t2 - 33*Power(t2,2) + 40*Power(t2,3)) + 
               Power(s,2)*(-13 - 35*t2 + 133*Power(t2,2) + 
                  17*Power(t2,3) - 102*Power(t2,4) + 27*Power(t2,5)) + 
               s*(5 - 164*t2 - 101*Power(t2,2) + 603*Power(t2,3) + 
                  340*Power(t2,4) + 218*Power(t2,5) - 7*Power(t2,6))) + 
            Power(s2,2)*t2*(-28 - 2*t2 - 6*Power(t1,5)*t2 + 
               106*Power(t2,2) + 185*Power(t2,3) + 157*Power(t2,4) - 
               15*Power(t2,5) + 
               Power(t1,4)*(11 + 26*t2 + 13*Power(t2,2)) + 
               5*Power(t1,3)*(1 - 6*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(71 - 124*t2 + 208*Power(t2,2) + 
                  80*Power(t2,3) + 8*Power(t2,4)) + 
               t1*(83 - 142*t2 - 581*Power(t2,2) + 275*Power(t2,3) + 
                  159*Power(t2,4) - 6*Power(t2,5)) - 
               s*(-6 + 14*t2 + 165*Power(t2,2) + 97*Power(t2,3) + 
                  15*Power(t2,4) - 18*Power(t2,5) + 
                  Power(t1,4)*(1 + t2) + 
                  Power(t1,3)*(-2 - 44*t2 + 39*Power(t2,2)) - 
                  Power(t1,2)*
                   (10 + 6*t2 - 117*Power(t2,2) + 133*Power(t2,3)) + 
                  t1*(17 + 95*t2 - 61*Power(t2,2) - 149*Power(t2,3) + 
                     114*Power(t2,4)))) + 
            s2*(-6 + (3 - 7*s)*t2 + 
               (71 - 56*s + 7*Power(s,2))*Power(t2,2) + 
               (116 - 110*s + 23*Power(s,2))*Power(t2,3) - 
               (58 + 393*s + 7*Power(s,2))*Power(t2,4) - 
               (259 + 131*s + 14*Power(s,2))*Power(t2,5) - 
               5*(-5 - 7*s + 3*Power(s,2))*Power(t2,6) + 
               Power(t1,6)*(s*(4 + t2) + t2*(11 + 5*t2)) + 
               Power(t1,5)*(-1 - 16*t2 - 78*Power(t2,2) - 
                  25*Power(t2,3) + Power(s,2)*(1 + 2*t2) + 
                  s*(-2 - 35*t2 + 4*Power(t2,2))) - 
               Power(t1,4)*(1 + 5*t2 + 62*Power(t2,2) - 60*Power(t2,3) - 
                  47*Power(t2,4) + 3*Power(s,2)*t2*(1 + 4*t2) + 
                  s*(3 + 74*t2 - 10*Power(t2,2) + 20*Power(t2,3))) + 
               Power(t1,3)*(15 - 57*t2 + 98*Power(t2,2) + 
                  444*Power(t2,3) + 187*Power(t2,4) - 39*Power(t2,5) + 
                  Power(s,2)*
                   (-1 - 16*t2 - 57*Power(t2,2) + 57*Power(t2,3)) + 
                  s*(-10 + 81*t2 + 256*Power(t2,2) - 5*Power(t2,3) + 
                     23*Power(t2,4))) + 
               Power(t1,2)*(-29 + 141*t2 + 258*Power(t2,2) + 
                  118*Power(t2,3) - 248*Power(t2,4) - 268*Power(t2,5) + 
                  11*Power(t2,6) + 
                  Power(s,2)*t2*
                   (9 + 91*t2 + 109*Power(t2,2) - 121*Power(t2,3)) + 
                  s*(17 + 73*t2 - 104*Power(t2,2) + 280*Power(t2,3) + 
                     211*Power(t2,4) - 22*Power(t2,5))) + 
               t1*(22 - 77*t2 - 292*Power(t2,2) + 267*Power(t2,3) - 
                  418*Power(t2,4) - 498*Power(t2,5) + 48*Power(t2,6) + 
                  Power(s,2)*t2*
                   (13 + 31*t2 - 104*Power(t2,2) - 62*Power(t2,3) + 
                     89*Power(t2,4)) + 
                  s*(-6 - 39*t2 + 280*Power(t2,2) + 155*Power(t2,3) - 
                     781*Power(t2,4) - 281*Power(t2,5) + 13*Power(t2,6))))\
))*T3q(t2,s1))/((s - s2 + t1)*Power(s1 - t2,2)*Power(-1 + s1 + t1 - t2,2)*
       Power(s1*t1 - t2,3)*(-1 + t2)*(s - s1 + t2)) - 
    (8*(2 + Power(s1,10)*(Power(s2,3) + s2*Power(t1,2) - 2*Power(t1,3)) + 
         Power(-1 + s,2)*Power(t1,5)*(-1 + s - t2) + 4*t2 + 5*s*t2 + 
         5*s2*t2 - 6*s*s2*t2 + 6*Power(t2,2) + s*Power(t2,2) + 
         2*Power(s,2)*Power(t2,2) + 13*s2*Power(t2,2) - 
         3*s*s2*Power(t2,2) - 4*Power(s,2)*s2*Power(t2,2) + 
         3*Power(s2,2)*Power(t2,2) - 4*s*Power(s2,2)*Power(t2,2) + 
         5*Power(t2,3) - 9*s*Power(t2,3) + 11*Power(s,2)*Power(t2,3) + 
         Power(s,3)*Power(t2,3) + 17*s2*Power(t2,3) + s*s2*Power(t2,3) - 
         26*Power(s,2)*s2*Power(t2,3) - 27*s*Power(s2,2)*Power(t2,3) + 
         4*Power(s2,3)*Power(t2,3) - 3*Power(t2,4) + 22*s*Power(t2,4) - 
         14*Power(s,2)*Power(t2,4) + 2*Power(s,3)*Power(t2,4) + 
         18*s2*Power(t2,4) - 83*s*s2*Power(t2,4) - 
         16*Power(s,2)*s2*Power(t2,4) - 17*Power(s2,2)*Power(t2,4) - 
         18*s*Power(s2,2)*Power(t2,4) + 8*Power(s2,3)*Power(t2,4) + 
         23*Power(t2,5) - 40*s*Power(t2,5) - 17*Power(s,2)*Power(t2,5) - 
         Power(s,3)*Power(t2,5) - 42*s2*Power(t2,5) - 
         49*s*s2*Power(t2,5) - 4*Power(s2,2)*Power(t2,5) - 
         s*Power(s2,2)*Power(t2,5) + 2*Power(s2,3)*Power(t2,5) - 
         13*Power(t2,6) - 43*s*Power(t2,6) - 6*Power(s,2)*Power(t2,6) - 
         27*s2*Power(t2,6) + 8*s*s2*Power(t2,6) - 
         2*Power(s2,2)*Power(t2,6) - 24*Power(t2,7) + 
         Power(t1,4)*(6 + t2 + 4*s2*t2 + (-1 + s2)*Power(t2,2) - 
            4*Power(t2,3) - Power(s,3)*(3 + 2*t2) + 
            2*Power(s,2)*(6 + (3 + 2*s2)*t2 + 2*Power(t2,2)) - 
            s*(15 + (5 + 8*s2)*t2 + (5 + s2)*Power(t2,2))) + 
         Power(t1,3)*(-14 - (1 + 17*s2)*t2 + 
            (-1 + 5*s2 - 5*Power(s2,2))*Power(t2,2) + 
            (26 - 14*s2)*Power(t2,3) + 32*Power(t2,4) + 
            Power(s,3)*(2 + 3*t2 + Power(t2,2)) - 
            Power(s,2)*(15 + 6*(3 + 2*s2)*t2 + (1 + 7*s2)*Power(t2,2)) + 
            s*(27 + (18 + 29*s2)*t2 + 
               (4 + 8*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (30 + 7*s2)*Power(t2,3))) - 
         Power(t1,2)*(-16 - (7 + 27*s2)*t2 - 
            (11 + 13*Power(s2,2))*Power(t2,2) + 
            (20 + 19*s2 - 20*Power(s2,2) - 2*Power(s2,3))*Power(t2,3) + 
            (58 - 6*s2 + 4*Power(s2,2))*Power(t2,4) + 76*Power(t2,5) + 
            Power(s,3)*t2*(1 - 5*t2 + Power(t2,2)) + 
            Power(s,2)*(-6 - (9 + 14*s2)*t2 - 2*(2 + s2)*Power(t2,2) + 
               (21 - 2*s2)*Power(t2,3) + 14*Power(t2,4)) + 
            s*(21 + 8*(3 + 5*s2)*t2 + 
               (32 + 19*s2 + 13*Power(s2,2))*Power(t2,2) + 
               (29 + 43*s2 + 18*Power(s2,2))*Power(t2,3) + 
               3*(32 + s2)*Power(t2,4))) + 
         Power(s1,9)*(Power(s2,2)*
             (-3 + 2*Power(t1,2) + t1*(4 + s - t2) + 3*s*t2) + 
            Power(s2,3)*(3*t1 - 4*(1 + t2)) - 
            s2*t1*(-2*Power(t1,2) + s*(3 + t1 - 3*t2) + 2*t2 + 
               t1*(14 + t2)) + 
            Power(t1,2)*(-1 - 5*Power(t1,2) + 6*t2 + 
               s*(-3 + 2*t1 + 4*t2) + 2*t1*(6 + 5*t2))) + 
         t1*(-9 - (10 + 19*s2)*t2 - 
            (15 + 19*s2 + 11*Power(s2,2))*Power(t2,2) - 
            (7 - 16*s2 + 20*Power(s2,2) + 6*Power(s2,3))*Power(t2,3) + 
            (10 + 32*s2 - 3*Power(s2,2) - 5*Power(s2,3))*Power(t2,4) + 
            (47 + 34*s2 + 6*Power(s2,2))*Power(t2,5) + 72*Power(t2,6) + 
            Power(s,3)*Power(t2,2)*(-2 - 7*t2 + 2*Power(t2,2)) + 
            s*(6 + (4 + 25*s2)*t2 + 
               (32 + 15*s2 + 12*Power(s2,2))*Power(t2,2) + 
               (19 + 51*s2 + 41*Power(s2,2))*Power(t2,3) + 
               (67 + 92*s2 + 14*Power(s2,2))*Power(t2,4) + 
               (112 - 11*s2)*Power(t2,5)) + 
            Power(s,2)*t2*(4 - 5*t2 + 16*Power(t2,2) + 36*Power(t2,3) + 
               17*Power(t2,4) + 
               s2*(-6 + 10*t2 + 26*Power(t2,2) + Power(t2,3)))) + 
         Power(s1,8)*(Power(s2,3)*
             (3 + 3*Power(t1,2) + 14*t2 + 6*Power(t2,2) - 
               t1*(8 + 11*t2)) + 
            Power(s2,2)*(12 + 5*Power(t1,3) + (8 - 16*s)*t2 + 
               (1 - 9*s)*Power(t2,2) + 3*Power(t1,2)*(s - 2*(1 + t2)) + 
               t1*(s*(-8 + 9*t2) + 3*(-9 - 6*t2 + Power(t2,2)))) + 
            s2*(3 + 2*Power(t1,4) - 3*Power(t1,3)*(9 + s - t2) + 
               Power(t2,2) + 3*Power(s,2)*Power(t2,2) - 
               3*s*t2*(1 + t2) + 
               Power(t1,2)*(56 + 28*t2 - 3*Power(t2,2) + 
                  s*(4 + 15*t2)) + 
               t1*(-(Power(s,2)*t2) + s*(13 + t2 - 8*Power(t2,2)) + 
                  2*(-4 + 15*t2 + Power(t2,2)))) - 
            t1*(4*Power(t1,4) + 2*t2*(-1 + 3*t2) - 
               Power(t1,3)*(36 + 19*t2) + 
               2*Power(t1,2)*(20 + 22*t2 + 7*Power(t2,2)) + 
               t1*(-14 + 35*t2 + 30*Power(t2,2)) + 
               Power(s,2)*(-3*(-1 + t2)*t2 + t1*(-1 + 5*t2)) + 
               s*(-3 - 5*Power(t1,3) - 3*t2 + 8*Power(t2,2) + 
                  Power(t1,2)*(19 + t2) + 
                  t1*(-11 + 26*t2 + 5*Power(t2,2))))) + 
         Power(s1,7)*(-1 - Power(t1,6) - Power(t2,2) + 2*Power(t2,3) + 
            4*s*Power(t2,3) - 3*Power(s,2)*Power(t2,3) + 
            Power(s,3)*Power(t2,3) + Power(t1,5)*(44 + 4*s + 9*t2) - 
            Power(t1,4)*(5*s*(7 + 3*t2) + 
               2*(48 + 54*t2 + 7*Power(t2,2))) + 
            Power(t1,3)*(111 + Power(s,2)*(5 - 11*t2) + 50*t2 + 
               15*Power(t2,2) + 6*Power(t2,3) + 
               s*(84 - 5*t2 + 12*Power(t2,2))) + 
            Power(s2,3)*(6 + Power(t1,3) - 6*t2 - 17*Power(t2,2) - 
               4*Power(t2,3) - 5*Power(t1,2)*(1 + 2*t2) + 
               t1*(3 + 25*t2 + 15*Power(t2,2))) + 
            Power(t1,2)*(-58 + 86*t2 + Power(s,3)*t2 + 
               165*Power(t2,2) + 42*Power(t2,3) + 
               Power(s,2)*(3 + 11*t2 + 15*Power(t2,2)) + 
               s*(4 + 97*t2 + 32*Power(t2,2) - Power(t2,3))) + 
            t1*(4 - 29*t2 + 34*Power(t2,2) - 2*Power(s,3)*Power(t2,2) + 
               30*Power(t2,3) - 
               Power(s,2)*t2*(-17 + t2 + 4*Power(t2,2)) + 
               s*(-14 - 21*t2 + 54*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s2,2)*(-9 + 4*Power(t1,4) + 
               Power(t1,3)*(-25 + 3*s - 9*t2) + (-24 + 29*s)*t2 + 
               (-2 + 39*s)*Power(t2,2) + (-3 + 9*s)*Power(t2,3) + 
               Power(t1,2)*(-5 + 5*t2 + 4*Power(t2,2) + 
                  2*s*(-9 + 5*t2)) + 
               t1*(45 + 112*t2 + 32*Power(t2,2) - 3*Power(t2,3) + 
                  s*(17 - 26*t2 - 27*Power(t2,2)))) + 
            s2*(-12 + 2*Power(t1,5) + (-4 + 17*s)*t2 + 
               (-16 + 18*s - 17*Power(s,2))*Power(t2,2) + 
               (-1 + 8*s - 6*Power(s,2))*Power(t2,3) + 
               Power(t1,4)*(-15 - 4*s + 7*t2) + 
               Power(t1,3)*(71 + 3*t2 - 13*Power(t2,2) + 
                  s*(17 + 24*t2)) - 
               Power(t1,2)*(111 + 79*t2 + 28*Power(t2,2) - 
                  5*Power(t2,3) + Power(s,2)*(2 + 4*t2) + 
                  s*(21 + 53*t2 + 22*Power(t2,2))) + 
               t1*(45 - 84*t2 - 68*Power(t2,2) + 6*Power(t2,3) + 
                  Power(s,2)*t2*(-3 + 14*t2) + 
                  s*(-5 - 82*t2 - 18*Power(t2,2) + 7*Power(t2,3))))) + 
         Power(s1,6)*(4 + (24 + s)*Power(t1,6) - s*t2 + 15*Power(t2,2) + 
            s*Power(t2,2) - 11*Power(t2,3) - 30*s*Power(t2,3) + 
            12*Power(s,2)*Power(t2,3) - 5*Power(s,3)*Power(t2,3) - 
            10*Power(t2,4) - 5*s*Power(t2,4) + 
            4*Power(s,2)*Power(t2,4) - Power(s,3)*Power(t2,4) - 
            Power(t1,5)*(74 + 68*t2 + s*(33 + 10*t2)) + 
            Power(t1,4)*(204 + Power(s,2)*(6 - 4*t2) + 110*t2 + 
               45*Power(t2,2) + s*(170 + 77*t2 + 19*Power(t2,2))) - 
            Power(s2,3)*(9 + 28*t2 + 2*Power(t2,2) - 7*Power(t2,3) - 
               Power(t2,4) + 3*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(3 - 19*t2 - 11*Power(t2,2)) + 
               t1*(-5 + 5*t2 + 25*Power(t2,2) + 9*Power(t2,3))) + 
            Power(t1,3)*(-165 - 25*t2 + 197*Power(t2,2) + 
               17*Power(t2,3) + Power(s,3)*(-2 + 3*t2) + 
               Power(s,2)*t2*(31 + 9*t2) - 
               s*(119 - 16*t2 + 17*Power(t2,2) + 12*Power(t2,3))) - 
            Power(t1,2)*(-117 + 167*t2 + 347*Power(t2,2) + 
               164*Power(t2,3) + 18*Power(t2,4) + 
               Power(s,3)*t2*(-1 + 7*t2) + 
               Power(s,2)*(27 - 5*t2 + 28*Power(t2,2) + 
                  6*Power(t2,3)) + 
               s*(48 + 333*t2 + 102*Power(t2,2) + 29*Power(t2,3) - 
                  2*Power(t2,4))) + 
            t1*(-21 + 102*t2 - 49*Power(t2,2) - 178*Power(t2,3) - 
               42*Power(t2,4) + Power(s,3)*Power(t2,2)*(6 + 5*t2) + 
               Power(s,2)*t2*
                (-39 - 47*t2 - 13*Power(t2,2) + Power(t2,3)) + 
               s*(13 + 57*t2 - 149*Power(t2,2) - 68*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s2,2)*(-18 + Power(t1,5) + 
               Power(t1,4)*(-18 + s - 5*t2) - (3 + 8*s)*t2 - 
               (22 + 49*s)*Power(t2,2) - (14 + 27*s)*Power(t2,3) - 
               3*(-1 + s)*Power(t2,4) + 
               Power(t1,3)*(81 + 42*t2 + Power(t2,2) + 
                  2*s*(-5 + 3*t2)) + 
               Power(t1,2)*(18 + 62*t2 + 15*Power(t2,2) + 
                  2*Power(t2,3) + s*(37 - 19*t2 - 27*Power(t2,2))) + 
               t1*(2 - 154*t2 - 170*Power(t2,2) - 24*Power(t2,3) + 
                  Power(t2,4) + 
                  s*(-8 + 25*t2 + 71*Power(t2,2) + 23*Power(t2,3)))) + 
            s2*(9 + Power(t1,6) + (6 - 37*s)*t2 + 
               (46 - 18*s + 39*Power(s,2))*Power(t2,2) + 
               (38 - 15*s + 25*Power(s,2))*Power(t2,3) + 
               (-3 - 7*s + 3*Power(s,2))*Power(t2,4) + 
               Power(t1,5)*(2 - 3*s + 3*t2) + 
               Power(t1,4)*(-53 - 76*t2 - 10*Power(t2,2) + 
                  s*(11 + 13*t2)) + 
               Power(t1,2)*(120 + 113*t2 + 126*Power(t2,2) + 
                  31*Power(t2,3) - 2*Power(t2,4) + 
                  Power(s,2)*(8 - 3*t2 + 23*Power(t2,2)) + 
                  4*s*(19 + 7*t2 + 4*Power(t2,2) + Power(t2,3))) - 
               Power(t1,3)*(Power(s,2)*(4 + 7*t2) + 
                  s*(102 + 91*t2 + 12*Power(t2,2)) - 
                  2*(-66 + 13*t2 + 20*Power(t2,2) + 4*Power(t2,3))) + 
               t1*(-66 + 55*t2 + 199*Power(t2,2) + 53*Power(t2,3) - 
                  10*Power(t2,4) + 
                  Power(s,2)*t2*(33 - 44*t2 - 19*Power(t2,2)) + 
                  s*(-43 + 220*t2 + 257*Power(t2,2) + 39*Power(t2,3) - 
                     2*Power(t2,4))))) + 
         Power(s1,5)*(-3 + 4*Power(t1,7) + 
            Power(t1,6)*(3 - 14*s - 8*t2) + 4*t2 + 8*s*t2 - 
            50*Power(t2,2) - 13*s*Power(t2,2) + 3*Power(t2,3) + 
            80*s*Power(t2,3) - 3*Power(s,2)*Power(t2,3) + 
            11*Power(s,3)*Power(t2,3) + 62*Power(t2,4) + 
            37*s*Power(t2,4) - 5*Power(s,2)*Power(t2,4) + 
            3*Power(s,3)*Power(t2,4) + 14*Power(t2,5) - s*Power(t2,5) - 
            Power(s,2)*Power(t2,5) + 
            Power(t1,5)*(151 - 43*t2 + 4*Power(t2,2) + 
               Power(s,2)*(7 + 2*t2) + s*(165 + 58*t2)) + 
            Power(t1,4)*(-157 + 2*Power(s,3)*(-1 + t2) - 203*t2 + 
               136*Power(t2,2) - 
               3*Power(s,2)*(-3 + t2 + 2*Power(t2,2)) - 
               s*(232 + 251*t2 + 59*Power(t2,2))) - 
            Power(t1,3)*(-128 + 291*t2 + 350*Power(t2,2) + 
               149*Power(t2,3) + 
               3*Power(s,3)*(-2 + t2 + 2*Power(t2,2)) + 
               Power(s,2)*(40 + 76*t2 + 6*Power(t2,2) - 
                  6*Power(t2,3)) + 
               4*s*(-17 + 110*t2 + 21*Power(t2,2) + Power(t2,3))) + 
            Power(s2,3)*(-2*Power(t1,4) + 
               Power(t1,3)*(-17 + 20*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(9 + 4*t2 - 20*Power(t2,2) - 
                  4*Power(t2,3)) + 
               t2*(35 + 50*t2 + 11*Power(t2,2) + Power(t2,3)) + 
               t1*(2 - 12*t2 - 14*Power(t2,2) + 7*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(t1,2)*(-118 + 149*t2 + 500*Power(t2,2) + 
               105*Power(t2,3) + 35*Power(t2,4) + 
               Power(s,3)*t2*(-13 + 15*t2 + 6*Power(t2,2)) - 
               Power(s,2)*(-51 + 41*t2 + 33*Power(t2,2) + 
                  8*Power(t2,3) + 2*Power(t2,4)) + 
               s*(16 + 532*t2 + 369*Power(t2,2) + 147*Power(t2,3) + 
                  23*Power(t2,4))) + 
            t1*(29 - 156*t2 + 21*Power(t2,2) + 397*Power(t2,3) + 
               207*Power(t2,4) + 18*Power(t2,5) - 
               Power(s,3)*Power(t2,2)*(4 + 13*t2 + 2*Power(t2,2)) + 
               Power(s,2)*t2*
                (37 + 124*t2 + 105*Power(t2,2) + 11*Power(t2,3)) + 
               s*(26 - 62*t2 + 299*Power(t2,2) + 204*Power(t2,3) + 
                  24*Power(t2,4) - 4*Power(t2,5))) - 
            Power(s2,2)*(-27 + (-73 + 37*s)*t2 + 
               (-81 + 31*s)*Power(t2,2) - (94 + 9*s)*Power(t2,3) - 
               (17 + s)*Power(t2,4) + Power(t2,5) + 
               Power(t1,5)*(3 + t2) - 
               Power(t1,4)*(127 + 2*t2 + 2*s*(1 + t2)) + 
               Power(t1,3)*(62 + 153*t2 - 8*Power(t2,2) - 
                  3*Power(t2,3) + s*(-53 + 35*t2 + 10*Power(t2,2))) + 
               Power(t1,2)*(7 + 225*t2 + 123*Power(t2,2) + 
                  15*Power(t2,3) + 2*Power(t2,4) + 
                  s*(33 + 12*t2 - 58*Power(t2,2) - 14*Power(t2,3))) + 
               t1*(49 + 43*t2 - 180*Power(t2,2) - 102*Power(t2,3) - 
                  4*Power(t2,4) + 
                  s*(13 + 27*t2 + 35*Power(t2,2) + 36*Power(t2,3) + 
                     6*Power(t2,4)))) - 
            s2*(-18 + (-4 + s)*Power(t1,6) - (24 + 25*s)*t2 + 
               (28 + 55*s + 43*Power(s,2))*Power(t2,2) + 
               3*(35 + 31*s + 12*Power(s,2))*Power(t2,3) + 
               (28 + 11*s + 5*Power(s,2))*Power(t2,4) - 
               (5 + 2*s)*Power(t2,5) + 
               Power(t1,5)*(144 - s*(-6 + t2) + 51*t2) + 
               Power(t1,4)*(10 - 349*t2 - 72*Power(t2,2) + 
                  Power(s,2)*(5 + 4*t2) + s*(142 - 2*t2 - 5*Power(t2,2))\
) - Power(t1,3)*(93 + 292*t2 + 82*Power(t2,2) - 12*Power(t2,3) + 
                  2*Power(s,2)*(2 + 9*t2 + 7*Power(t2,2)) + 
                  s*(161 + 311*t2 - 16*Power(t2,2) - 9*Power(t2,3))) + 
               Power(t1,2)*(36 - 95*t2 + 315*Power(t2,2) + 
                  200*Power(t2,3) + 17*Power(t2,4) + 
                  2*Power(s,2)*
                   (6 - 33*t2 + 25*Power(t2,2) + 8*Power(t2,3)) - 
                  s*(-112 + 207*t2 + 185*Power(t2,2) + 33*Power(t2,3) + 
                     4*Power(t2,4))) + 
               t1*(13 + 5*t2 + 194*Power(t2,2) + 237*Power(t2,3) + 
                  25*Power(t2,4) - 4*Power(t2,5) - 
                  Power(s,2)*t2*
                   (-75 + 38*t2 + 42*Power(t2,2) + 6*Power(t2,3)) + 
                  s*(-61 + 180*t2 + 541*Power(t2,2) + 236*Power(t2,3) + 
                     15*Power(t2,4))))) + 
         Power(s1,4)*(-6 + 16*Power(t1,7) + 
            Power(t1,6)*(34 + 58*s + 5*Power(s,2) - 68*t2) - 15*t2 - 
            17*s*t2 + 67*Power(t2,2) + 38*s*Power(t2,2) + 
            2*Power(s,2)*Power(t2,2) + 39*Power(t2,3) - 
            104*s*Power(t2,3) - 50*Power(s,2)*Power(t2,3) - 
            13*Power(s,3)*Power(t2,3) - 136*Power(t2,4) - 
            105*s*Power(t2,4) - 42*Power(s,2)*Power(t2,4) - 
            3*Power(s,3)*Power(t2,4) - 77*Power(t2,5) - 
            7*s*Power(t2,5) - 5*Power(s,2)*Power(t2,5) + 
            Power(s,3)*Power(t2,5) - 6*Power(t2,6) + 2*s*Power(t2,6) - 
            Power(t1,5)*(71 + 169*t2 + 20*Power(s,2)*t2 - 
               88*Power(t2,2) + 2*s*(92 + 79*t2)) + 
            Power(t1,4)*(4 + Power(s,3)*(2 - 4*t2) - 269*t2 + 
               206*Power(t2,2) - 36*Power(t2,3) + 
               Power(s,2)*(-54 - 45*t2 + 28*Power(t2,2)) + 
               s*(128 - 135*t2 + 56*Power(t2,2))) + 
            Power(t1,3)*(-27 + 293*t2 + 930*Power(t2,2) - 
               80*Power(t2,3) + 
               Power(s,3)*(-2 - 3*t2 + 11*Power(t2,2)) + 
               Power(s,2)*(58 + 114*t2 + 48*Power(t2,2) - 
                  14*Power(t2,3)) + 
               s*(-62 + 695*t2 + 860*Power(t2,2) + 124*Power(t2,3))) + 
            Power(s2,3)*(5 - 3*t2 - 55*Power(t2,2) - 36*Power(t2,3) - 
               7*Power(t2,4) - Power(t2,5) + 
               2*Power(t1,4)*(-7 + 5*t2) + 
               Power(t1,3)*(25 + 12*t2 - 17*Power(t2,2)) + 
               Power(t1,2)*(-4 + 34*t2 - 41*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-9 - 19*t2 + 25*Power(t2,2) + 28*Power(t2,3) + 
                  Power(t2,4))) - 
            Power(t1,2)*(-23 + 60*t2 + 12*Power(t2,2) + 
               14*Power(t2,3) - 80*Power(t2,4) + 
               3*Power(s,3)*t2*(-7 + t2 + 3*Power(t2,2)) + 
               Power(s,2)*(23 - 39*t2 - 182*Power(t2,2) - 
                  34*Power(t2,3) + Power(t2,4)) + 
               s*(-93 + 311*t2 + 36*Power(t2,2) + 358*Power(t2,3) + 
                  72*Power(t2,4))) + 
            t1*(8 + 134*t2 + 82*Power(t2,2) - 557*Power(t2,3) - 
               348*Power(t2,4) - 65*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(-6 + 7*t2 + Power(t2,2)) + 
               Power(s,2)*t2*
                (11 - 77*t2 - 200*Power(t2,2) - 32*Power(t2,3) + 
                  2*Power(t2,4)) - 
               s*(53 - 7*t2 + 364*Power(t2,2) + 537*Power(t2,3) + 
                  176*Power(t2,4) + 10*Power(t2,5))) + 
            Power(s2,2)*(Power(t1,5)*(61 + 2*s - 15*t2) + 
               Power(t1,4)*(-101 + s*(40 - 22*t2) - 120*t2 + 
                  33*Power(t2,2)) - 
               Power(t1,3)*(1 + 245*t2 - 95*Power(t2,2) + 
                  21*Power(t2,3) + s*(89 + 18*t2 - 36*Power(t2,2))) + 
               Power(t1,2)*(11 + 129*t2 + 475*Power(t2,2) + 
                  11*Power(t2,3) + Power(t2,4) - 
                  2*s*(-6 + 43*t2 - 27*Power(t2,2) + 9*Power(t2,3))) + 
               t1*(6 + 192*t2 + 218*Power(t2,2) - 49*Power(t2,3) - 
                  13*Power(t2,4) + 2*Power(t2,5) - 
                  s*(-16 - 73*t2 + 19*Power(t2,2) + 41*Power(t2,3) + 
                     Power(t2,4))) + 
               t2*(-71 - 95*t2 - 145*Power(t2,2) - 84*Power(t2,3) - 
                  6*Power(t2,4) + 
                  s*(40 + 140*t2 + 69*Power(t2,2) + 15*Power(t2,3) + 
                     3*Power(t2,4)))) + 
            s2*(-27 - (68 + 7*s)*Power(t1,6) + (-62 + 29*s)*t2 + 
               (-37 + 152*s + 13*Power(s,2))*Power(t2,2) + 
               (76 + 316*s + Power(s,2))*Power(t2,3) + 
               (109 + 148*s - 5*Power(s,2))*Power(t2,4) + 
               (7 + 11*s - 3*Power(s,2))*Power(t2,5) - 2*Power(t2,6) + 
               Power(t1,5)*(71 - 3*Power(s,2) + 221*t2 + 
                  s*(-66 + 37*t2)) + 
               Power(t1,4)*(39 + 281*t2 - 99*Power(t2,2) + 
                  Power(s,2)*(3 + 16*t2) + 
                  s*(185 + 213*t2 - 61*Power(t2,2))) + 
               Power(t1,2)*(-47 - 296*t2 - 317*Power(t2,2) + 
                  310*Power(t2,3) + 91*Power(t2,4) + 
                  Power(s,2)*
                   (8 - 101*t2 - 16*Power(t2,2) + 21*Power(t2,3)) - 
                  s*(-34 + 254*t2 + 855*Power(t2,2) + 34*Power(t2,3))) + 
               Power(t1,3)*(5 + 69*t2 - 1083*Power(t2,2) - 
                  153*Power(t2,3) + 
                  Power(s,2)*(11 + 16*t2 - 30*Power(t2,2)) + 
                  s*(-62 - 9*t2 - 163*Power(t2,2) + 35*Power(t2,3))) + 
               t1*(83 + 131*t2 - 117*Power(t2,2) + 429*Power(t2,3) + 
                  236*Power(t2,4) + 10*Power(t2,5) - 
                  Power(s,2)*t2*
                   (-59 + 21*t2 - 2*Power(t2,2) + Power(t2,3)) + 
                  s*(-1 - 38*t2 + 140*Power(t2,2) + 301*Power(t2,3) + 
                     39*Power(t2,4) - 4*Power(t2,5))))) + 
         Power(s1,3)*(9 + 4*Power(t1,7) + 17*t2 + 8*s*t2 - 
            42*Power(t2,2) - 47*s*Power(t2,2) - 
            8*Power(s,2)*Power(t2,2) - 88*Power(t2,3) + 
            41*s*Power(t2,3) + 82*Power(s,2)*Power(t2,3) + 
            7*Power(s,3)*Power(t2,3) + 177*Power(t2,4) + 
            211*s*Power(t2,4) + 142*Power(s,2)*Power(t2,4) - 
            Power(s,3)*Power(t2,4) + 158*Power(t2,5) + 
            61*s*Power(t2,5) + 28*Power(s,2)*Power(t2,5) - 
            2*Power(s,3)*Power(t2,5) + 27*Power(t2,6) - s*Power(t2,6) - 
            Power(t1,6)*(7 + 38*s + 3*Power(s,2) + 80*t2) - 
            Power(t1,5)*(22 + Power(s,3) + 83*t2 - 256*Power(t2,2) + 
               Power(s,2)*(17 + 5*t2) + s*(-79 + 42*t2)) + 
            Power(t1,4)*(40 + 281*t2 + 471*Power(t2,2) - 
               288*Power(t2,3) + Power(s,3)*(9 + 2*t2) + 
               Power(s,2)*(48 + 89*t2 + 38*Power(t2,2)) + 
               s*(-49 + 359*t2 + 407*Power(t2,2))) + 
            Power(t1,3)*(-42 - 34*t2 - 98*Power(t2,2) - 
               596*Power(t2,3) + 108*Power(t2,4) + 
               2*Power(s,3)*(-6 - 3*t2 + Power(t2,2)) - 
               Power(s,2)*(-6 + 61*t2 + 25*Power(t2,2) + 
                  54*Power(t2,3)) - 
               s*(-64 + 241*t2 + 548*Power(t2,2) + 462*Power(t2,3))) - 
            Power(t1,2)*(-57 - 58*t2 + 216*Power(t2,2) + 
               940*Power(t2,3) - 126*Power(t2,4) + 
               Power(s,3)*t2*(9 + 16*t2 + 8*Power(t2,2)) + 
               s*(81 + 46*t2 + 535*Power(t2,2) + 528*Power(t2,3) - 
                  60*Power(t2,4)) + 
               Power(s,2)*(39 - 10*t2 + 225*Power(t2,2) + 
                  121*Power(t2,3) - 29*Power(t2,4))) + 
            Power(s2,3)*(-2 - 13*t2 + 14*Power(t2,2) + 34*Power(t2,3) + 
               5*Power(t2,4) + Power(t2,5) + 2*Power(t1,4)*(2 + t2) + 
               Power(t1,3)*(-6 + 28*t2 - 27*Power(t2,2)) + 
               Power(t1,2)*t2*(-64 - 17*t2 + 40*Power(t2,2)) + 
               t1*(4 + 35*t2 - 26*Power(t2,2) + 8*Power(t2,3) - 
                  12*Power(t2,4))) + 
            t1*(-39 - 83*t2 - 125*Power(t2,2) + 168*Power(t2,3) + 
               426*Power(t2,4) + 62*Power(t2,5) + 
               7*Power(s,3)*Power(t2,2)*(2 + 2*t2 + Power(t2,2)) + 
               Power(s,2)*t2*
                (-61 - 86*t2 + 96*Power(t2,2) + 46*Power(t2,3) - 
                  5*Power(t2,4)) + 
               s*(14 + 29*t2 + 234*Power(t2,2) + 420*Power(t2,3) + 
                  577*Power(t2,4) + 76*Power(t2,5))) + 
            Power(s2,2)*(-15 - (3 + s)*t2 + (25 - 136*s)*Power(t2,2) + 
               (16 - 139*s)*Power(t2,3) + (93 - 23*s)*Power(t2,4) - 
               4*(-6 + s)*Power(t2,5) + 
               Power(t1,5)*(-23 + 10*s + 3*t2) + 
               Power(t1,4)*(27 - 152*t2 + 43*Power(t2,2) - 
                  4*s*(11 + 3*t2)) + 
               Power(t1,3)*(1 + 278*t2 + 314*Power(t2,2) - 
                  91*Power(t2,3) + s*(44 - 70*t2 + 45*Power(t2,2))) + 
               Power(t1,2)*(-26 + 22*t2 + 128*Power(t2,2) - 
                  249*Power(t2,3) + 49*Power(t2,4) + 
                  s*(-1 + 250*t2 + 7*Power(t2,2) - 70*Power(t2,3))) + 
               t1*(36 - 52*t2 - 320*Power(t2,2) - 430*Power(t2,3) + 
                  24*Power(t2,4) - 4*Power(t2,5) + 
                  s*(-5 - 106*t2 + 70*Power(t2,2) + 30*Power(t2,3) + 
                     31*Power(t2,4)))) + 
            s2*((16 - 5*s)*Power(t1,6) + 
               Power(t1,5)*(-13 + Power(s,2) + 175*t2 + 29*s*(2 + t2)) + 
               Power(t1,4)*(8 + 9*Power(s,2) - 124*t2 - 
                  682*Power(t2,2) + s*(-59 + 5*t2 - 102*Power(t2,2))) - 
               Power(t1,3)*(-5 + 328*t2 + 91*Power(t2,2) - 
                  605*Power(t2,3) + 
                  Power(s,2)*(17 + 26*t2 + 19*Power(t2,2)) + 
                  s*(32 + 330*t2 + 343*Power(t2,2) - 146*Power(t2,3))) + 
               Power(t1,2)*(2 + 224*t2 + 42*Power(t2,2) + 
                  1076*Power(t2,3) - 11*Power(t2,4) + 
                  Power(s,2)*
                   (-2 - 17*t2 + 44*Power(t2,2) + 39*Power(t2,3)) + 
                  s*(69 + 516*Power(t2,2) + 419*Power(t2,3) - 
                     77*Power(t2,4))) - 
               t2*(-37 - 25*t2 - 82*Power(t2,2) + 149*Power(t2,3) + 
                  83*Power(t2,4) + Power(t2,5) - 
                  Power(s,2)*t2*
                   (25 + 79*t2 + 19*Power(t2,2) + 5*Power(t2,3)) + 
                  s*(53 + 150*t2 + 281*Power(t2,2) + 295*Power(t2,3) + 
                     54*Power(t2,4))) + 
               t1*(-18 - 208*t2 + 210*Power(t2,2) + 189*Power(t2,3) - 
                  498*Power(t2,4) - 102*Power(t2,5) + 
                  Power(s,2)*t2*
                   (15 + 55*t2 - 46*Power(t2,2) - 26*Power(t2,3)) + 
                  s*(-47 + 94*t2 + 439*Power(t2,2) + 483*Power(t2,3) - 
                     85*Power(t2,4) + 9*Power(t2,5))))) - 
         s1*(5 - Power(-1 + s,2)*Power(t1,6) + 9*t2 + 16*s*t2 + 
            15*Power(t2,2) + 8*s*Power(t2,2) + 
            8*Power(s,2)*Power(t2,2) + 31*Power(t2,3) + 
            9*s*Power(t2,3) + 16*Power(s,2)*Power(t2,3) + 
            3*Power(s,3)*Power(t2,3) + 17*Power(t2,4) - 
            2*s*Power(t2,4) - 87*Power(s,2)*Power(t2,4) + 
            6*Power(s,3)*Power(t2,4) - 50*Power(t2,5) - 
            194*s*Power(t2,5) - 53*Power(s,2)*Power(t2,5) - 
            2*Power(s,3)*Power(t2,5) - 99*Power(t2,6) - 
            73*s*Power(t2,6) - 6*Power(s,2)*Power(t2,6) + 
            Power(t1,5)*(-1 + 3*Power(s,3) + Power(s,2)*(-6 + t2) - 
               7*t2 - 12*Power(t2,2) + 2*s*(2 + t2)) + 
            Power(s2,3)*Power(t2,2)*
             (3 + Power(t1,3) + 19*t2 + 8*Power(t2,2) - 7*Power(t2,3) - 
               Power(t1,2)*(5 + 6*t2) + t1*(1 - t2 + 8*Power(t2,2))) + 
            Power(t1,4)*(12 + 10*t2 + 67*Power(t2,2) + 
               112*Power(t2,3) - Power(s,3)*(13 + 4*t2) + 
               Power(s,2)*(33 + 18*t2 + 22*Power(t2,2)) + 
               s*(-30 - 17*t2 + 72*Power(t2,2))) - 
            Power(t1,3)*(25 - 7*t2 + 51*Power(t2,2) + 103*Power(t2,3) + 
               284*Power(t2,4) + 
               Power(s,3)*(-10 - 15*t2 + 4*Power(t2,2)) + 
               Power(s,2)*(57 + 44*t2 + 51*Power(t2,2) + 
                  56*Power(t2,3)) + 
               2*s*(-33 + 5*t2 + 36*Power(t2,2) + 123*Power(t2,3))) + 
            Power(t1,2)*(28 - 7*t2 + 9*Power(t2,2) - 103*Power(t2,3) - 
               55*Power(t2,4) + 280*Power(t2,5) + 
               Power(s,3)*t2*(-5 + 15*t2 + 6*Power(t2,2)) + 
               Power(s,2)*(31 + 8*t2 + 5*Power(t2,2) + 
                  19*Power(t2,3) + 41*Power(t2,4)) + 
               s*(-68 - t2 - 10*Power(t2,2) + 78*Power(t2,3) + 
                  185*Power(t2,4))) + 
            t1*(-18 - 12*t2 - 28*Power(t2,2) - 13*Power(t2,3) + 
               261*Power(t2,4) + 198*Power(t2,5) - 96*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(-8 - 23*t2 + Power(t2,2)) + 
               Power(s,2)*t2*
                (25 + 33*t2 + 93*Power(t2,2) + 73*Power(t2,3) - 
                  Power(t2,4)) + 
               s*(26 + 10*t2 + 35*Power(t2,2) + 118*Power(t2,3) + 
                  201*Power(t2,4) + 58*Power(t2,5))) + 
            Power(s2,2)*t2*(10 + Power(t1,4)*(-2 + t2) + 20*t2 + 
               21*Power(t2,2) + 72*Power(t2,3) + 53*Power(t2,4) - 
               14*Power(t2,5) - 
               8*Power(t1,3)*(1 - 6*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(32 - 19*t2 + 42*Power(t2,2) + 
                  7*Power(t2,3)) + 
               t1*(-32 - 50*t2 - 143*Power(t2,2) - 104*Power(t2,3) + 
                  22*Power(t2,4)) + 
               s*(-6 + 2*Power(t1,4) + Power(t1,3)*(3 - 25*t2) - 4*t2 - 
                  17*Power(t2,2) + 12*Power(t2,3) + 4*Power(t2,4) + 
                  Power(t1,2)*(-16 + 69*t2 + 20*Power(t2,2)) - 
                  t1*(-17 + 52*t2 + 34*Power(t2,2) + Power(t2,3)))) + 
            s2*(6 + Power(-1 + s,2)*Power(t1,5) + (21 - 11*s)*t2 + 
               (49 + 5*s - 19*Power(s,2))*Power(t2,2) - 
               5*(-2 + 10*s + 19*Power(s,2))*Power(t2,3) - 
               (95 + 198*s + 38*Power(s,2))*Power(t2,4) + 
               (-38 - 10*s + 5*Power(s,2))*Power(t2,5) + 
               5*(15 + 4*s)*Power(t2,6) + 
               Power(t1,4)*(1 + 22*t2 + 8*Power(s,2)*t2 - 
                  34*Power(t2,2) + s*(-1 - 19*t2 + 7*Power(t2,2))) - 
               Power(t1,3)*(15 + 45*t2 + 52*Power(t2,2) + 
                  43*Power(t2,3) + 
                  Power(s,2)*(1 + 56*t2 + 17*Power(t2,2)) - 
                  s*(14 + 85*t2 - 103*Power(t2,2) + 5*Power(t2,3))) + 
               Power(t1,2)*(29 + 45*t2 + 63*Power(t2,2) + 
                  111*Power(t2,3) + 300*Power(t2,4) + 
                  Power(s,2)*t2*(81 + 50*t2 + 11*Power(t2,2)) + 
                  s*(-17 - 130*t2 + 95*Power(t2,2) + 244*Power(t2,3) - 
                     11*Power(t2,4))) - 
               t1*(22 + 43*t2 + 26*Power(t2,2) - 146*Power(t2,3) + 
                  149*Power(t2,4) + 298*Power(t2,5) + 
                  Power(s,2)*t2*
                   (31 + t2 - 44*Power(t2,2) + 8*Power(t2,3)) + 
                  s*(-6 - 75*t2 - 44*Power(t2,2) + 98*Power(t2,3) + 
                     110*Power(t2,4) + 21*Power(t2,5))))) + 
         Power(s1,2)*(-(Power(t1,6)*(6 - 7*s + 3*Power(s,2) + 12*t2)) + 
            Power(t1,5)*(3*Power(s,3) + Power(s,2)*(7 + 25*t2) + 
               2*s*(-9 + 40*t2) + 12*(1 + 4*t2 + 12*Power(t2,2))) - 
            Power(t1,4)*(8 + 13*t2 - 4*Power(t2,2) + 400*Power(t2,3) + 
               Power(s,3)*(19 + 2*t2) + 
               Power(s,2)*(-12 + 25*t2 + 42*Power(t2,2)) + 
               s*(2 + 107*t2 + 166*Power(t2,2))) + 
            Power(s2,3)*t2*(5 + 7*t2 + 3*Power(t2,2) - 12*Power(t2,3) + 
               3*Power(t2,4) - Power(t1,3)*(9 + 5*t2) + 
               Power(t1,2)*(17 - 10*t2 + 22*Power(t2,2)) + 
               t1*(-13 + 26*t2 + 19*Power(t2,2) - 26*Power(t2,3))) + 
            Power(t1,3)*(14 + t2 - 322*Power(t2,2) - 407*Power(t2,3) + 
               416*Power(t2,4) - 
               3*Power(s,3)*(-6 - 7*t2 + 4*Power(t2,2)) + 
               Power(s,2)*(-71 - 23*t2 - 76*Power(t2,2) + 
                  6*Power(t2,3)) - 
               s*(-21 + 52*t2 + 179*Power(t2,2) + 162*Power(t2,3))) + 
            Power(t1,2)*(-22 - 51*t2 + 10*Power(t2,2) + 
               456*Power(t2,3) + 620*Power(t2,4) - 148*Power(t2,5) + 
               Power(s,3)*t2*(-5 + 21*t2 + 18*Power(t2,2)) + 
               Power(s,2)*(59 - 20*t2 + 90*Power(t2,2) + 
                  141*Power(t2,3) + 29*Power(t2,4)) + 
               s*(-39 + 106*t2 + 299*Power(t2,2) + 756*Power(t2,3) + 
                  448*Power(t2,4))) + 
            t2*(-1 + 20*t2 + 81*Power(t2,2) - 61*Power(t2,3) - 
               216*Power(t2,4) - 47*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(1 + 6*t2) - 
               Power(s,2)*t2*
                (-12 + 33*t2 + 172*Power(t2,2) + 58*Power(t2,3)) + 
               s*(13 + 28*t2 + 27*Power(t2,2) - 162*Power(t2,3) - 
                  219*Power(t2,4) - 19*Power(t2,5))) + 
            t1*(10 + 28*t2 + 30*Power(t2,2) + 86*Power(t2,3) + 
               236*Power(t2,4) - 212*Power(t2,5) - 
               Power(s,3)*Power(t2,2)*(14 + 29*t2 + 7*Power(t2,2)) + 
               Power(s,2)*t2*
                (59 + 122*t2 + 93*Power(t2,2) + 11*Power(t2,3) - 
                  15*Power(t2,4)) - 
               s*(-31 + 7*t2 + 63*Power(t2,2) - 70*Power(t2,3) + 
                  233*Power(t2,4) + 188*Power(t2,5))) + 
            Power(s2,2)*(6 + (30 - 16*s)*t2 + Power(t1,5)*t2 + 
               (29 + 46*s)*Power(t2,2) + (73 + 89*s)*Power(t2,3) + 
               8*(12 + 5*s)*Power(t2,4) - 2*(16 + 3*s)*Power(t2,5) + 
               Power(t1,4)*(-3 + s + 57*t2 - 26*s*t2 - 15*Power(t2,2)) + 
               Power(t1,2)*(13 + (45 - 159*s)*t2 - 
                  5*(63 + 2*s)*Power(t2,2) - (303 + 8*s)*Power(t2,3) + 
                  83*Power(t2,4)) + 
               t1*(-17 + (-68 + 69*s)*t2 - 2*(-9 + 62*s)*Power(t2,2) + 
                  (137 - 51*s)*Power(t2,3) + 3*(69 + 7*s)*Power(t2,4) - 
                  42*Power(t2,5)) + 
               Power(t1,3)*(1 - 65*t2 + 139*Power(t2,2) - 
                  27*Power(t2,3) + s*(-1 + 120*t2 + 19*Power(t2,2)))) + 
            s2*(15 + (-1 + s)*Power(t1,6) + (15 + 17*s)*t2 + 
               (45 + 64*s - 35*Power(s,2))*Power(t2,2) + 
               (-97 + 14*s - 132*Power(s,2))*Power(t2,3) + 
               (-78 + 50*s - 34*Power(s,2))*Power(t2,4) + 
               (183 + 104*s + 3*Power(s,2))*Power(t2,5) + 
               33*Power(t2,6) + 
               Power(t1,5)*(11 + 3*Power(s,2) - 36*t2 + s*(-9 + 5*t2)) - 
               Power(t1,4)*(6 + 23*t2 + 156*Power(t2,2) + 
                  Power(s,2)*(7 + 8*t2) + 
                  2*s*(-4 + 62*t2 + 7*Power(t2,2))) + 
               Power(t1,3)*(-32 + 51*t2 + 144*Power(t2,2) + 
                  723*Power(t2,3) + 
                  5*Power(s,2)*(1 - 9*t2 + 5*Power(t2,2)) + 
                  s*(35 + 151*t2 + 183*Power(t2,2) + 66*Power(t2,3))) + 
               Power(t1,2)*(58 - 66*t2 + 342*Power(t2,2) - 
                  156*Power(t2,3) - 727*Power(t2,4) + 
                  Power(s,2)*t2*(126 + 47*t2 - 35*Power(t2,2)) + 
                  s*(-66 - 33*t2 + 274*Power(t2,2) + 105*Power(t2,3) - 
                     115*Power(t2,4))) + 
               t1*(-45 + 59*t2 - 39*Power(t2,2) - 166*Power(t2,3) - 
                  388*Power(t2,4) + 164*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-53 - 53*t2 + 39*Power(t2,2) + 12*Power(t2,3)) + 
                  s*(31 + 32*t2 - 240*Power(t2,2) - 743*Power(t2,3) - 
                     259*Power(t2,4) + 57*Power(t2,5))))))*T4q(s1))/
     (Power(-1 + s1,2)*(s - s2 + t1)*Power(-1 + s1 + t1 - t2,2)*
       Power(s1*t1 - t2,3)*(-1 + t2)*(s - s1 + t2)) + 
    (8*(-2 + Power(s1,7)*(Power(s2,3) + s2*Power(t1,2) - 2*Power(t1,3)) - 
         Power(-1 + s,2)*Power(t1,5)*(-1 + s - t2) - 4*t2 - 5*s*t2 - 
         5*s2*t2 + 6*s*s2*t2 - 6*Power(t2,2) - s*Power(t2,2) - 
         2*Power(s,2)*Power(t2,2) - 13*s2*Power(t2,2) + 
         3*s*s2*Power(t2,2) + 4*Power(s,2)*s2*Power(t2,2) - 
         3*Power(s2,2)*Power(t2,2) + 4*s*Power(s2,2)*Power(t2,2) + 
         9*Power(t2,3) - s*Power(t2,3) - 7*Power(s,2)*Power(t2,3) - 
         Power(s,3)*Power(t2,3) - 25*s2*Power(t2,3) + 
         39*s*s2*Power(t2,3) + 22*Power(s,2)*s2*Power(t2,3) - 
         8*Power(s2,2)*Power(t2,3) - 13*s*Power(s2,2)*Power(t2,3) - 
         9*Power(t2,4) + 12*s*Power(t2,4) + 20*Power(s,2)*Power(t2,4) - 
         2*Power(s,3)*Power(t2,4) + 20*s2*Power(t2,4) + 
         5*s*s2*Power(t2,4) + 4*Power(s,2)*s2*Power(t2,4) - 
         19*Power(s2,2)*Power(t2,4) - 2*s*Power(s2,2)*Power(t2,4) + 
         7*Power(t2,5) + 14*s*Power(t2,5) + 7*Power(s,2)*Power(t2,5) + 
         Power(s,3)*Power(t2,5) - 10*s2*Power(t2,5) - 9*s*s2*Power(t2,5) - 
         4*Power(s,2)*s2*Power(t2,5) + 5*s*Power(s2,2)*Power(t2,5) - 
         2*Power(s2,3)*Power(t2,5) + 5*Power(t2,6) + 5*s*Power(t2,6) - 
         7*s2*Power(t2,6) + Power(t1,4)*
          (-6 - (1 + 4*s2)*t2 - (-1 + s2)*Power(t2,2) + 
            Power(s,3)*(3 + 2*t2) - 
            2*Power(s,2)*(6 + (3 + 2*s2)*t2 + 2*Power(t2,2)) + 
            s*(15 + (5 + 8*s2)*t2 + (5 + s2)*Power(t2,2))) + 
         Power(t1,3)*(14 + t2 + 17*s2*t2 + 
            (1 - 5*s2 + 5*Power(s2,2))*Power(t2,2) - 8*Power(t2,3) - 
            Power(s,3)*(2 + 3*t2 + Power(t2,2)) + 
            Power(s,2)*(15 + 6*(3 + 2*s2)*t2 + (1 + 7*s2)*Power(t2,2) + 
               6*Power(t2,3)) - 
            s*(27 + (18 + 29*s2)*t2 + 
               (4 + 8*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (2 + 5*s2)*Power(t2,3))) + 
         Power(t1,2)*(-16 - (7 + 27*s2)*t2 - 
            (11 + 13*Power(s2,2))*Power(t2,2) + 
            (-2 + 5*s2 + 2*Power(s2,2) - 2*Power(s2,3))*Power(t2,3) - 
            2*(-7 - s2 + Power(s2,2))*Power(t2,4) + 
            Power(s,3)*t2*(1 - 5*t2 + Power(t2,2)) - 
            Power(s,2)*(6 + (9 + 14*s2)*t2 + 2*(2 + s2)*Power(t2,2) + 
               (-11 + 6*s2)*Power(t2,3) + 4*Power(t2,4)) + 
            s*(21 + 8*(3 + 5*s2)*t2 + 
               (32 + 19*s2 + 13*Power(s2,2))*Power(t2,2) + 
               (-3 + 3*s2 + 8*Power(s2,2))*Power(t2,3) + 
               (2 + 7*s2)*Power(t2,4))) + 
         Power(s1,6)*(Power(s2,3)*(-1 + 3*t1 - 4*t2) + 
            Power(s2,2)*(-3 + 2*Power(t1,2) + t1*(4 + s - t2) + 3*s*t2) - 
            s2*t1*(-2*Power(t1,2) + s*(3 + t1 - 3*t2) + 2*t2 + 
               t1*(11 + t2)) + 
            Power(t1,2)*(-1 - 5*Power(t1,2) + 6*t2 + 
               s*(-3 + 2*t1 + 4*t2) + 2*t1*(1 + 5*t2))) + 
         t1*(9 + (10 + 19*s2)*t2 + 
            (15 + 19*s2 + 11*Power(s2,2))*Power(t2,2) + 
            (1 + 20*s2 + 6*Power(s2,2) + 2*Power(s2,3))*Power(t2,3) + 
            (-6 + Power(s2,2) + 3*Power(s2,3))*Power(t2,4) + 
            (-13 + 6*s2 + 2*Power(s2,2))*Power(t2,5) + 
            Power(s,3)*Power(t2,2)*(2 + 7*t2 - 2*Power(t2,2)) - 
            s*(6 + (4 + 25*s2)*t2 + 
               (32 + 15*s2 + 12*Power(s2,2))*Power(t2,2) + 
               (37 + 11*s2 + Power(s2,2))*Power(t2,3) + 
               (9 - 6*s2 + 8*Power(s2,2))*Power(t2,4) + 
               (8 + 3*s2)*Power(t2,5)) + 
            Power(s,2)*t2*(-4 + 5*t2 - 22*Power(t2,2) - 16*Power(t2,3) + 
               Power(t2,4) + s2*
                (6 - 10*t2 - 14*Power(t2,2) + 7*Power(t2,3)))) + 
         Power(s1,5)*(Power(s2,3)*
             (-3 + t1 + 3*Power(t1,2) + 2*t2 - 11*t1*t2 + 6*Power(t2,2)) \
+ Power(s2,2)*(3 + 5*Power(t1,3) + 3*Power(t1,2)*(s - 2*t2) + 
               (8 - 7*s)*t2 + (1 - 9*s)*Power(t2,2) + 
               t1*(s*(-5 + 9*t2) + 3*(-5 - 7*t2 + Power(t2,2)))) + 
            s2*(3 + 2*Power(t1,4) + Power(t2,2) + 
               3*Power(s,2)*Power(t2,2) - 3*s*t2*(1 + t2) + 
               Power(t1,3)*(-11 - 3*s + 3*t2) + 
               Power(t1,2)*(20 + s + 25*t2 + 15*s*t2 - 3*Power(t2,2)) + 
               t1*(-(Power(s,2)*t2) + s*(4 + 10*t2 - 8*Power(t2,2)) + 
                  2*(-4 + 12*t2 + Power(t2,2)))) - 
            t1*(4*Power(t1,4) + 2*t2*(-1 + 3*t2) - 
               Power(t1,3)*(3 + 19*t2) + 
               2*Power(t1,2)*(8 + t2 + 7*Power(t2,2)) + 
               t1*(-11 + 5*t2 + 30*Power(t2,2)) + 
               Power(s,2)*(-3*(-1 + t2)*t2 + t1*(-1 + 5*t2)) + 
               s*(-3 - 5*Power(t1,3) - 3*t2 + 8*Power(t2,2) + 
                  Power(t1,2)*(13 + t2) + t1*(-2 + 14*t2 + 5*Power(t2,2)))\
)) + Power(s1,4)*(-1 - Power(t1,6) - Power(t2,2) + 2*Power(t2,3) + 
            4*s*Power(t2,3) - 3*Power(s,2)*Power(t2,3) + 
            Power(s,3)*Power(t2,3) + Power(t1,5)*(4*s + 9*t2) - 
            Power(t1,4)*(6 + 15*t2 + 14*Power(t2,2) + 3*s*(4 + 5*t2)) + 
            Power(t1,3)*(25 + Power(s,2)*(5 - 11*t2) + 30*t2 - 
               39*Power(t2,2) + 6*Power(t2,3) + 
               3*s*(13 - 6*t2 + 4*Power(t2,2))) + 
            Power(s2,3)*(1 + Power(t1,3) + Power(t1,2)*(4 - 10*t2) + 
               12*t2 + Power(t2,2) - 4*Power(t2,3) + 
               t1*(-3 - 8*t2 + 15*Power(t2,2))) + 
            Power(t1,2)*(-22 + 17*t2 + Power(s,3)*t2 + 39*Power(t2,2) + 
               42*Power(t2,3) + Power(s,2)*(6 - 4*t2 + 15*Power(t2,2)) + 
               s*(19 + 43*t2 + 17*Power(t2,2) - Power(t2,3))) + 
            t1*(4 - 23*t2 + 4*Power(t2,2) - 2*Power(s,3)*Power(t2,2) + 
               30*Power(t2,3) - 
               4*Power(s,2)*t2*(-2 - 2*t2 + Power(t2,2)) + 
               s*(-5 - 12*t2 + 30*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s2,2)*(9 + 4*Power(t1,4) + 
               Power(t1,3)*(-10 + 3*s - 9*t2) + Power(t2,2) - 
               3*Power(t2,3) + s*t2*(-1 + 12*t2 + 9*Power(t2,2)) - 
               t1*(12 + s - 52*t2 - s*t2 - 41*Power(t2,2) + 
                  27*s*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(-11 - 13*t2 + 4*Power(t2,2) + s*(-9 + 10*t2))\
) + s2*(-3 + 2*Power(t1,5) + (-4 + 8*s)*t2 + 
               (-13 + 9*s - 8*Power(s,2))*Power(t2,2) + 
               (-1 + 8*s - 6*Power(s,2))*Power(t2,3) + 
               Power(t1,4)*(3 - 4*s + 7*t2) + 
               Power(t1,3)*(16 + 6*t2 - 13*Power(t2,2) + 
                  s*(-2 + 24*t2)) - 
               Power(t1,2)*(17 + 31*t2 + 37*Power(t2,2) - 
                  5*Power(t2,3) + Power(s,2)*(2 + 4*t2) + 
                  s*(15 + 8*t2 + 22*Power(t2,2))) + 
               t1*(21 - 6*t2 - 62*Power(t2,2) + 6*Power(t2,3) + 
                  2*Power(s,2)*t2*(-3 + 7*t2) + 
                  s*(16 - 61*t2 - 42*Power(t2,2) + 7*Power(t2,3))))) + 
         Power(s1,3)*(1 + (1 + s)*Power(t1,6) - s*t2 + 12*Power(t2,2) + 
            s*Power(t2,2) - Power(t2,3) - 18*s*Power(t2,3) + 
            3*Power(s,2)*Power(t2,3) - 2*Power(s,3)*Power(t2,3) - 
            10*Power(t2,4) - 5*s*Power(t2,4) + 4*Power(s,2)*Power(t2,4) - 
            Power(s,3)*Power(t2,4) + 
            Power(t1,5)*(12 + s - 7*t2 - 10*s*t2) + 
            Power(t1,4)*(2 + 14*t2 - 4*Power(s,2)*t2 - 15*Power(t2,2) + 
               s*(43 - 10*t2 + 19*Power(t2,2))) + 
            Power(t1,3)*(-24 - 25*t2 + 54*Power(t2,2) + 39*Power(t2,3) + 
               Power(s,3)*(-2 + 3*t2) + 
               Power(s,2)*(11 + 4*t2 + 9*Power(t2,2)) + 
               s*(9 + 27*t2 + 39*Power(t2,2) - 12*Power(t2,3))) + 
            Power(s2,3)*(2 + Power(t1,3)*(2 - 3*t2) - 2*t2 + 
               11*Power(t1,2)*(-1 + t2)*t2 - 17*Power(t2,2) - 
               5*Power(t2,3) + Power(t2,4) + 
               t1*(-4 + 4*t2 + 20*Power(t2,2) - 9*Power(t2,3))) - 
            Power(t1,2)*(-17 + 5*t2 + 80*Power(t2,2) + 2*Power(t2,3) + 
               18*Power(t2,4) + Power(s,3)*t2*(-4 + 7*t2) + 
               2*s*t2*(79 + 3*t2 + 16*Power(t2,2) - Power(t2,3)) + 
               Power(s,2)*(12 - 8*t2 - 17*Power(t2,2) + 6*Power(t2,3))) + 
            t1*(-9 + 27*t2 + 17*Power(t2,2) - 52*Power(t2,3) + 
               5*Power(s,3)*Power(t2,3) - 42*Power(t2,4) + 
               Power(s,2)*t2*
                (-6 - 32*t2 - 25*Power(t2,2) + Power(t2,3)) + 
               s*(-11 + 12*t2 - 35*Power(t2,2) - 38*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s2,2)*(-3 + Power(t1,5) + 
               Power(t1,4)*(-4 + s - 5*t2) + (-27 + 13*s)*t2 + 
               2*(-11 + 7*s)*Power(t2,2) - 23*Power(t2,3) - 
               3*(-1 + s)*Power(t2,4) + 
               Power(t1,2)*(-13 + s + 41*t2 + 11*s*t2 + 
                  27*Power(t2,2) - 27*s*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(4 + 7*t2 + Power(t2,2) + s*(-1 + 6*t2)) + 
               t1*(15 + 64*t2 - 56*Power(t2,2) - 33*Power(t2,3) + 
                  Power(t2,4) + 
                  s*(5 + t2 - 10*Power(t2,2) + 23*Power(t2,3)))) + 
            s2*(-9 + Power(t1,6) - 3*Power(t1,5)*(s - t2) - 
               2*(3 + 2*s)*t2 + 2*(2 + 9*s + 3*Power(s,2))*Power(t2,2) + 
               (35 + 9*s + 7*Power(s,2))*Power(t2,3) + 
               (-3 - 7*s + 3*Power(s,2))*Power(t2,4) - 
               Power(t1,4)*(20 + s*(11 - 13*t2) + 17*t2 + 
                  10*Power(t2,2)) - 
               Power(t1,3)*(-9 + 23*t2 + 7*Power(s,2)*t2 + 
                  21*Power(t2,2) - 8*Power(t2,3) + 
                  s*(29 + 11*t2 + 12*Power(t2,2))) + 
               t1*(21 - 37*t2 + 37*Power(t2,2) + 71*Power(t2,3) - 
                  10*Power(t2,4) + 
                  Power(s,2)*t2*(18 - 2*t2 - 19*Power(t2,2)) + 
                  s*(-10 + 10*t2 + 155*Power(t2,2) + 60*Power(t2,3) - 
                     2*Power(t2,4))) + 
               Power(t1,2)*(Power(s,2)*(2 - 15*t2 + 23*Power(t2,2)) + 
                  s*(27 - 11*t2 - 50*Power(t2,2) + 4*Power(t2,3)) - 
                  2*(1 + 4*t2 - 21*Power(t2,2) - 23*Power(t2,3) + 
                     Power(t2,4))))) + 
         Power(s1,2)*(3 - (-3 + s)*Power(t1,6) + 4*t2 + 5*s*t2 - 
            11*Power(t2,2) - 10*s*Power(t2,2) - 18*Power(t2,3) + 
            14*s*Power(t2,3) + 15*Power(s,2)*Power(t2,3) + 
            2*Power(s,3)*Power(t2,3) + 20*Power(t2,4) + 
            22*s*Power(t2,4) + 7*Power(s,2)*Power(t2,4) + 
            14*Power(t2,5) - s*Power(t2,5) - Power(s,2)*Power(t2,5) + 
            Power(t1,5)*(-2*s*(-6 + t2) - 9*(1 + t2) + 
               Power(s,2)*(-7 + 2*t2)) + 
            Power(t1,4)*(8 + 2*Power(s,3)*(-1 + t2) - 29*t2 + 
               29*Power(t2,2) + 
               Power(s,2)*(15 + 13*t2 - 6*Power(t2,2)) + 
               2*s*(1 - 5*t2 + 14*Power(t2,2))) + 
            Power(s2,3)*t2*(-5 + 2*t2 + 8*Power(t2,2) + 4*Power(t2,3) + 
               Power(t1,3)*(-3 + 2*t2) + 
               Power(t1,2)*(-5 + 13*t2 - 4*Power(t2,2)) + 
               t1*(13 + t2 - 20*Power(t2,2) + 2*Power(t2,3))) - 
            Power(t1,3)*(5 - 8*t2 + 6*Power(s,3)*(-1 + t2)*t2 + 
               23*Power(t2,2) + 10*Power(t2,3) + 
               Power(s,2)*(10 + 19*t2 - 7*Power(t2,2) - 6*Power(t2,3)) + 
               5*s*(-3 + 28*t2 + 5*Power(t2,2) + 10*Power(t2,3))) + 
            Power(t1,2)*(10 + 30*t2 + 35*Power(t2,2) - 39*Power(t2,3) - 
               31*Power(t2,4) + 
               2*Power(s,3)*t2*(-2 - 3*t2 + 3*Power(t2,2)) - 
               Power(s,2)*(2 - 2*t2 + 45*Power(t2,2) + 26*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s*(-39 + 5*t2 + 61*Power(t2,2) - 6*Power(t2,3) + 
                  29*Power(t2,4))) + 
            t1*(-10 - 4*t2 - 36*Power(t2,2) + 55*Power(t2,3) + 
               45*Power(t2,4) + 18*Power(t2,5) + 
               2*Power(s,3)*Power(t2,2)*(1 + t2 - Power(t2,2)) + 
               Power(s,2)*t2*
                (-8 + 7*t2 + 42*Power(t2,2) + 14*Power(t2,3)) + 
               s*(11 + 13*t2 + 96*Power(t2,2) + 30*Power(t2,3) + 
                  30*Power(t2,4) - 4*Power(t2,5))) - 
            Power(s2,2)*(6 + Power(t1,5)*t2 - 13*Power(t2,2) - 
               34*Power(t2,3) - 26*Power(t2,4) + Power(t2,5) + 
               2*s*t2*(1 + 17*t2 + 9*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(13 + 9*(-3 + s)*t2 + 
                  (30 + 23*s)*Power(t2,2) + (9 - 14*s)*Power(t2,3) + 
                  2*Power(t2,4)) + 
               t1*(-17 + 2*(14 + 9*s)*t2 - 4*(-27 + 4*s)*Power(t2,2) - 
                  3*(4 + 11*s)*Power(t2,3) + (-7 + 6*s)*Power(t2,4)) + 
               Power(t1,4)*(s - 2*s*t2 - 3*(1 + t2)) + 
               Power(t1,3)*(1 + t2 - 5*Power(t2,2) - 3*Power(t2,3) + 
                  s*(-1 - 9*t2 + 10*Power(t2,2)))) + 
            s2*(3 - (-1 + s)*Power(t1,6) - 2*(-9 + 7*s)*t2 + 
               (24 - 31*s + 2*Power(s,2))*Power(t2,2) + 
               (-7 - 90*s + 3*Power(s,2))*Power(t2,3) + 
               (-37 - 32*s + 4*Power(s,2))*Power(t2,4) + 
               (5 + 2*s)*Power(t2,5) + 
               Power(t1,5)*(-8 - 6*t2 + s*(3 + t2)) + 
               Power(t1,4)*(9 + Power(s,2)*(7 - 4*t2) + 23*t2 - 
                  6*Power(t2,2) + s*(-11 - 5*t2 + 5*Power(t2,2))) + 
               Power(t1,3)*(-13 + 24*t2 + 36*Power(t2,2) + 
                  30*Power(t2,3) + 
                  Power(s,2)*(-8 - 15*t2 + 14*Power(t2,2)) + 
                  s*(7 + 50*t2 - 24*Power(t2,2) - 9*Power(t2,3))) + 
               t1*(-21 - 74*t2 + 57*Power(t2,2) - 60*Power(t2,3) - 
                  55*Power(t2,4) + 4*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-4 - 10*t2 - 15*Power(t2,2) + 6*Power(t2,3)) + 
                  s*(-13 + 43*t2 + 12*Power(t2,2) - 77*Power(t2,3) - 
                     21*Power(t2,4))) + 
               Power(t1,2)*(29 + 15*t2 + 21*Power(t2,2) - 
                  11*Power(t2,3) - 23*Power(t2,4) + 
                  Power(s,2)*t2*(21 + 19*t2 - 16*Power(t2,2)) + 
                  s*(15 + 3*t2 + 77*Power(t2,2) + 45*Power(t2,3) + 
                     4*Power(t2,4))))) + 
         s1*(-1 - Power(-1 + s,2)*Power(t1,6) - 3*t2 + s*t2 - 
            3*Power(t2,2) + 5*s*Power(t2,2) + 2*Power(s,2)*Power(t2,2) + 
            20*Power(t2,3) - 4*s*Power(t2,3) - 17*Power(s,2)*Power(t2,3) - 
            8*Power(t2,4) - 14*s*Power(t2,4) - 33*Power(s,2)*Power(t2,4) - 
            23*Power(t2,5) - 10*s*Power(t2,5) - 8*Power(s,2)*Power(t2,5) + 
            Power(s,3)*Power(t2,5) - 6*Power(t2,6) + 2*s*Power(t2,6) + 
            Power(t1,5)*(2 - 4*t2 + Power(s,2)*(3 + 4*t2) - 
               s*(5 + 4*t2)) - 
            Power(s2,3)*Power(t2,2)*
             (-3 - Power(t1,3) + t2 - 2*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(-7 + 6*t2) + t1*(11 + 5*t2 - 7*Power(t2,2))) + 
            Power(t1,4)*(-6 + 2*Power(s,3)*(-2 + t2) + 7*t2 + 
               16*Power(t2,2) + s*(15 - 2*t2 + 3*Power(t2,2)) - 
               Power(s,2)*(3 + 8*Power(t2,2))) + 
            Power(t1,3)*(17 + 10*t2 + 18*Power(t2,2) - 35*Power(t2,3) + 
               Power(s,3)*(4 + 6*t2 - 7*Power(t2,2)) + 
               2*Power(s,2)*(-6 + 5*t2 - 9*Power(t2,2) + 5*Power(t2,3)) - 
               s*(15 + 64*t2 - 12*Power(t2,2) + 20*Power(t2,3))) + 
            Power(t1,2)*(-20 - 28*t2 - 6*Power(t2,2) + 15*Power(t2,3) + 
               29*Power(t2,4) + Power(s,3)*t2*(-2 + 9*Power(t2,2)) + 
               Power(s,2)*(13 - 19*t2 + 11*Power(t2,2) + 
                  16*Power(t2,3) - 7*Power(t2,4)) + 
               s*(-5 + 71*t2 + 140*Power(t2,2) + 37*Power(t2,3) + 
                  39*Power(t2,4))) + 
            t1*(9 + 18*t2 - 25*Power(t2,2) + 4*Power(t2,3) - 
               3*Power(t2,4) + Power(t2,5) - 
               Power(s,3)*Power(t2,2)*(2 + 2*t2 + 5*Power(t2,2)) + 
               Power(s,2)*t2*(13 + 36*t2 + 15*Power(t2,2) + 
                  7*Power(t2,3) + 2*Power(t2,4)) - 
               s*(-8 + 2*t2 + 31*Power(t2,2) + 73*Power(t2,3) + 
                  32*Power(t2,4) + 22*Power(t2,5))) + 
            Power(s2,2)*t2*(10 + Power(t1,4)*(-2 + t2) + 11*t2 + 
               23*Power(t2,2) - 15*Power(t2,3) - 9*Power(t2,4) + 
               Power(t1,3)*(-8 - 3*t2 + 2*Power(t2,2)) - 
               Power(t1,2)*(-32 + 16*t2 + 4*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(-32 + 7*t2 + 59*Power(t2,2) + 5*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s*(-6 + 2*Power(t1,4) + Power(t1,3)*(3 - 10*t2) + 8*t2 + 
                  24*Power(t2,2) + 3*Power(t2,4) + 
                  4*Power(t1,2)*(-4 - 3*t2 + 6*Power(t2,2)) + 
                  t1*(17 + 32*t2 - 11*Power(t2,2) - 19*Power(t2,3)))) + 
            s2*(6 + Power(-1 + s,2)*Power(t1,5) + (6 + 7*s)*t2 + 
               (10 + 14*s - 7*Power(s,2))*Power(t2,2) + 
               (-35 + 37*s - 17*Power(s,2))*Power(t2,3) + 
               (13 + 73*s - 2*Power(s,2))*Power(t2,4) + 
               (22 + 17*s - 3*Power(s,2))*Power(t2,5) - 2*Power(t2,6) + 
               Power(t1,4)*(1 + 10*t2 - 4*Power(s,2)*t2 + 5*Power(t2,2) + 
                  s*(-1 + 5*t2 + 4*Power(t2,2))) + 
               Power(t1,3)*(-15 + 6*t2 - 25*Power(t2,2) + Power(t2,3) + 
                  Power(s,2)*(-1 - 20*t2 + 16*Power(t2,2)) - 
                  s*(-14 + 2*t2 + 7*Power(t2,2) + 12*Power(t2,3))) + 
               Power(t1,2)*(29 - 36*t2 - 45*Power(t2,2) - 
                  18*Power(t2,3) - 26*Power(t2,4) + 
                  Power(s,2)*t2*(39 + 8*t2 - 27*Power(t2,2)) + 
                  s*(-17 - 10*t2 + 32*Power(t2,2) + 5*Power(t2,3) + 
                     12*Power(t2,4))) + 
               t1*(-22 + 14*t2 + 55*Power(t2,2) - 36*Power(t2,3) + 
                  35*Power(t2,4) + 22*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-13 - 19*t2 + 14*Power(t2,2) + 17*Power(t2,3)) - 
                  s*(-6 + 121*Power(t2,2) + 79*Power(t2,3) + 
                     18*Power(t2,4) + 4*Power(t2,5))))))*
       T5q(1 - s1 - t1 + t2))/
     ((s - s2 + t1)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,3)*(-1 + t2)*
       (s - s1 + t2)));
   return a;
};
