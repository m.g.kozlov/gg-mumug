
struct my_str
{
    int N;
    double E;
    double omega;
    char file[30];
    char conf[30];
} opt;


struct parameters
{
    int N;
    int nlo1;
    int nlo2;
    int nlo3;
    double E;
    double omega;
    double th_beam;
    double th_max;
    double th_gmu;
    double si_min;
    double om_s; //omega*
    char file[100];
} par;


int get_params (int ac, char *av[],struct my_str *str);
int read_my_config(char *config_file_name,struct parameters *x);