#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_2_m231_2_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-16*((s - s1 + 2*(-9 + s2))*Power(t1,4) - t2*(-1 + s2 + t2) + 
         Power(t1,3)*(1 + 27*s2 + s*(-9 + 6*s2) + 30*t2 - s2*t2 + 
            s1*(-7 + s2 + t2)) + 
         Power(t1,2)*(8 + 8*s2 - 12*Power(s2,2) - 26*t2 - 30*s2*t2 - 
            2*Power(s2,2)*t2 - 12*Power(t2,2) - s2*Power(t2,2) + 
            s1*(4 + s2 + 2*Power(s2,2) + 5*t2 + s2*t2) - 
            s*(-8 - 8*t2 + Power(t2,2) + 6*s2*(1 + t2))) + 
         t1*(-11 + 3*t2 - 8*s*t2 + 25*Power(t2,2) + s*Power(t2,2) + 
            2*Power(s2,2)*(-2 + 9*t2) + 
            s2*(15 + 6*(-4 + s)*t2 + 5*Power(t2,2)) - 
            s1*(-4 + 2*Power(s2,2) + 6*t2 + s2*(2 + t2)))))/
     (Power(-1 + t1,2)*t1*(t1 - t2)*(1 - s2 + t1 - t2)*(-1 + t2)) + 
    (8*(4*Power(s2,3)*(-1 + t2)*t2 + 
         (1 + (-1 + s)*t2)*(-5 - 4*t2 + Power(t2,2)) + 
         Power(t1,3)*(-6 + t2 + Power(t2,2) - 
            s1*(2 + t2 + Power(t2,2)) + s*(6 + t2 + Power(t2,2))) + 
         t1*(20 + 14*t2 + 6*Power(t2,2) + s1*(-1 - 8*t2 + Power(t2,2)) + 
            s*(15 + 15*t2 + 2*Power(t2,2))) + 
         Power(s2,2)*(s1*(-1 + t1)*(3 + t2) + 
            t2*(-17 + 2*s*(-1 + t2) - 7*t2 + 4*Power(t2,2)) + 
            t1*(4 + (-7 + 2*s)*t2 - (9 + 2*s)*Power(t2,2))) + 
         Power(t1,2)*(-9 + 4*t2 + Power(t2,3) + s1*(3 + 9*t2) - 
            s*(21 + 11*t2 - Power(t2,2) + Power(t2,3))) + 
         s2*(3 - 15*t2 - 7*s*t2 - 17*Power(t2,2) - 3*s*Power(t2,2) - 
            11*Power(t2,3) + 2*s*Power(t2,3) - 
            Power(t1,2)*(-3 + s*(-1 + t2) - 15*t2 + Power(t2,2) + 
               Power(t2,3)) + 
            s1*(-1 + t1)*(-5 - 3*Power(t2,2) + t1*(-1 + Power(t2,2))) + 
            t1*(14 + 16*t2 - 6*Power(t2,2) + 
               s*(-1 + 8*t2 + 3*Power(t2,2) - 2*Power(t2,3)))))*
       B1(1 - s2 + t1 - t2,t2,t1))/(Power(-1 + t1,2)*(-1 + t2)*(t1 - s2*t2)) \
+ (16*(-Power(t2,4) + t1*Power(t2,2)*(4 + 2*t2 + Power(t2,2)) - 
         Power(t1,2)*t2*(-4 + (26 - 4*s)*t2 + 
            (1 + 3*s - 4*s1)*Power(t2,2) + 11*Power(t2,3)) + 
         Power(t1,5)*(48 + s1*(8 - 3*t2) + 7*t2 + Power(t2,2) + 
            s*(-4 + 3*t2)) + Power(s2,3)*t1*
          ((2 + s1 - t2)*t2 + Power(t1,2)*(2 - s1 + t2) + 
            t1*(2 + s1 - 7*t2 - s1*t2 + Power(t2,2))) - 
         Power(t1,4)*(24 + 110*t2 + 15*Power(t2,2) + 4*Power(t2,3) + 
            s1*(-8 + t2 - 5*Power(t2,2)) + s*(-4 - 5*t2 + 6*Power(t2,2))) \
+ Power(t1,3)*(32 - (1 + 8*s)*t2 + 2*(36 + s)*Power(t2,2) + 
            (19 + 3*s)*Power(t2,3) + 3*Power(t2,4) - 
            s1*(16 - 4*t2 + 5*Power(t2,2) + 4*Power(t2,3))) - 
         Power(s2,2)*(Power(t1,5) + Power(t2,2) + 
            Power(t1,4)*(12 + s + t2) + 
            t1*t2*(1 - (6 + s + s1)*t2 + Power(t2,2)) - 
            Power(t1,3)*(5 + s + 31*t2 + 2*s*t2 + 5*Power(t2,2) - 
               s1*(4 + 5*t2)) + 
            Power(t1,2)*(8 + (-7 + 2*s)*t2 + (30 + s)*Power(t2,2) - 
               Power(t2,3) + s1*(-4 - 5*t2 + Power(t2,2)))) + 
         s2*(3*(1 + s - s1)*Power(t1,5) - 2*Power(t2,3) + 
            t1*Power(t2,2)*(1 + (5 + s)*t2) - 
            Power(t1,2)*t2*(30 + (-12 + 5*s)*t2 + (34 + s)*Power(t2,2) - 
               4*s1*(1 + 2*t2)) - 
            Power(t1,4)*(10 + 31*t2 + 5*Power(t2,2) - s1*(7 + 5*t2) + 
               s*(3 + 7*t2)) + 
            Power(t1,3)*(-5 + (41 + 7*s)*t2 + (48 + 5*s)*Power(t2,2) + 
               7*Power(t2,3) - s1*(4 + 9*t2 + 8*Power(t2,2)))))*R1(t1))/
     (Power(-1 + t1,2)*t1*Power(t1 - t2,2)*(-1 + t2)*
       (-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) + 
    (32*(Power(t1,3)*(2 + s1 + s*t2 - s1*t2) - 
         Power(t1,2)*(-3 + (5 + s + s1 + 2*s2)*t2 + 
            (1 + 2*s - s1)*Power(t2,2) + Power(t2,3)) + 
         t1*(1 - 11*t2 + (7 + 2*s + 3*s2)*Power(t2,2) + 
            (4 + s + s2)*Power(t2,3) + Power(t2,4) - 
            s1*(-1 + t2)*(-1 + (2 + s2)*t2 + Power(t2,2))) - 
         t2*(-4 + 2*t2 - Power(t2,2) + s*Power(t2,2) + 3*Power(t2,3) - 
            s1*(-1 + t2)*(1 + s2 + t2) + s2*(2 - 5*t2 + 5*Power(t2,2))))*
       R1(t2))/(Power(-1 + t1,2)*Power(t1 - t2,2)*Power(-1 + t2,2)) - 
    (16*(-6 + 13*t2 - 2*s*t2 + 2*s1*t2 - 17*Power(t2,2) - 
         5*s1*Power(t2,2) + 24*Power(t2,3) - s*Power(t2,3) + 
         2*s1*Power(t2,3) - 6*Power(t2,4) - 
         Power(s2,2)*(16 + 17*t1 + 10*Power(t1,2) + Power(t1,3) - 
            s1*(-1 + t1)*(8 + 5*t1 - 8*t2) + 
            s*(-1 + t1)*(3 + 6*t1 - t2) - 39*t2 - 17*t1*t2 + 
            4*Power(t1,2)*t2 + 28*Power(t2,2) - 8*t1*Power(t2,2)) + 
         Power(s2,3)*(6 - 3*s1*(-1 + t1) - 11*t2 + 3*t1*(2 + t2)) + 
         Power(t1,3)*(10 - 3*s1*(-2 + t2) + 5*t2 + Power(t2,2) + 
            s*(-2 + 3*t2)) - Power(t1,2)*
          (-70 + 57*t2 + 3*Power(t2,2) + 3*Power(t2,3) + 
            s1*(8 + 4*t2 - 4*Power(t2,2)) + s*(16 - 8*t2 + 5*Power(t2,2))\
) + s2*(11 - 21*t1 - 15*Power(t1,2) + 9*Power(t1,3) - 35*t2 + 22*t1*t2 - 
            11*Power(t1,2)*t2 + 57*Power(t2,2) + 18*t1*Power(t2,2) - 
            7*Power(t1,2)*Power(t2,2) - 23*Power(t2,3) + 
            7*t1*Power(t2,3) + 
            s*(-1 + t1)*(4 + 3*Power(t1,2) + t1*(21 - 11*t2) - 3*t2 + 
               2*Power(t2,2)) - 
            s1*(-1 + t1)*(4 + 9*t1 + 3*Power(t1,2) - 13*t2 - 9*t1*t2 + 
               7*Power(t2,2))) + 
         t1*(38 - 105*t2 + 23*Power(t2,2) + 7*Power(t2,3) + 
            2*Power(t2,4) + s1*(2 + 5*t2 + Power(t2,2) - 2*Power(t2,3)) + 
            s*(18 - 9*t2 + 5*Power(t2,2) + Power(t2,3))))*
       R2(1 - s2 + t1 - t2))/
     (Power(-1 + t1,2)*(1 - s2 + t1 - t2)*(-1 + t2)*
       (-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) - 
    (8*(4*Power(t1,5)*(5 + s - s1 - t2) - 4*Power(s2,6)*t2 + 
         (-5 + t2)*Power(t2,3)*(1 + (-1 + s)*t2) - 
         4*Power(t1,4)*(-20 + s1 + 14*t2 + 2*s1*t2 + Power(t2,2) - 
            2*s1*Power(t2,2) + s*(-1 + 2*t2 + 2*Power(t2,2))) + 
         Power(s2,5)*((1 - 2*s - 18*t2)*t2 - s1*(-1 + t1)*(3 + 2*t2) + 
            t1*(4 + 19*t2 + 2*s*t2 + 2*Power(t2,2))) + 
         t1*(12 - 22*Power(t2,2) + 6*Power(t2,3) - 8*Power(t2,4) + 
            s1*t2*(-4 + 16*t2 - 9*Power(t2,2) + Power(t2,3)) + 
            s*t2*(4 + 24*t2 + Power(t2,2) + 4*Power(t2,3))) + 
         Power(t1,3)*(-112 - 76*t2 + 38*Power(t2,2) + 6*Power(t2,3) + 
            Power(t2,4) - s1*
             (-44 + 16*t2 - 2*Power(t2,2) + 2*Power(t2,3) + Power(t2,4)) \
+ s*(36 + 16*t2 - 2*Power(t2,2) + 10*Power(t2,3) + Power(t2,4))) + 
         Power(t1,2)*(s1*(-36 + 28*t2 - 26*Power(t2,2) + 
               11*Power(t2,3)) + 
            t2*(40 + 36*t2 + 17*Power(t2,2) - 11*Power(t2,3) + 
               Power(t2,4)) - 
            s*(44 + 12*t2 + 14*Power(t2,2) + 11*Power(t2,3) + 
               Power(t2,5))) + 
         Power(s2,4)*(-3 + 10*t2 + 3*s*t2 + 8*Power(t2,2) - 
            6*s*Power(t2,2) - 30*Power(t2,3) + 
            s1*(-1 + t1)*(5 - 10*t2 - 6*Power(t2,2) + 5*t1*(1 + t2)) - 
            Power(t1,2)*(19 + 28*t2 + 3*Power(t2,2) + s*(5 + 6*t2)) + 
            t1*(2 + 26*t2 + 67*Power(t2,2) + 6*Power(t2,3) + 
               s*(5 + 3*t2 + 6*Power(t2,2)))) + 
         Power(s2,3)*(5 + (-11 + 5*s - 25*s1)*t2 + 
            (25 + 8*s + 14*s1)*Power(t2,2) + 
            (16 - 6*s + 6*s1)*Power(t2,3) - 22*Power(t2,4) + 
            Power(t1,3)*(26 + 15*t2 + 3*s*(2 + t2) - 3*s1*(2 + t2)) - 
            Power(t1,2)*(11 + 128*t2 + 73*Power(t2,2) + 7*Power(t2,3) - 
               s1*(19 + 11*t2 + 13*Power(t2,2)) + 
               s*(-5 + 11*t2 + 17*Power(t2,2))) + 
            t1*(-12 - 28*t2 + 32*Power(t2,2) + 87*Power(t2,3) + 
               6*Power(t2,4) + 
               s*(-11 + 3*t2 + 9*Power(t2,2) + 6*Power(t2,3)) - 
               s1*(13 - 17*t2 + 27*Power(t2,2) + 6*Power(t2,3)))) + 
         Power(s2,2)*(-4*Power(t1,4)*(3 + s - s1 + t2) + 
            Power(t1,3)*(62 + 158*t2 + 27*Power(t2,2) - 
               7*s1*(2 + 2*t2 + Power(t2,2)) + 
               s*(14 + 22*t2 + 7*Power(t2,2))) + 
            t2*(21 - 11*t2 + 15*Power(t2,2) + 12*Power(t2,3) - 
               6*Power(t2,4) + 
               s*(-8 + t2 + 8*Power(t2,2) - 2*Power(t2,3)) + 
               s1*(8 - 31*t2 + 10*Power(t2,2) + 2*Power(t2,3))) + 
            Power(t1,2)*(16 + 47*t2 - 178*Power(t2,2) - 
               57*Power(t2,3) - 5*Power(t2,4) + 
               s1*(-30 + 29*t2 + 3*Power(t2,2) + 11*Power(t2,3)) - 
               s*(10 - 19*t2 + 3*Power(t2,2) + 17*Power(t2,3))) + 
            t1*(14 - (86 + 33*s)*t2 - (62 + 5*s)*Power(t2,2) + 
               (2 + 9*s)*Power(t2,3) + (49 + 2*s)*Power(t2,4) + 
               2*Power(t2,5) + 
               s1*(40 - 23*t2 + 35*Power(t2,2) - 21*Power(t2,3) - 
                  2*Power(t2,4)))) + 
         s2*(4*Power(t1,5) + t2*
             (-12 + (15 - 4*s + 4*s1)*t2 + 
               (7 - 9*s - 11*s1)*Power(t2,2) + 
               (-1 + 4*s + 3*s1)*Power(t2,3) + 3*Power(t2,4)) - 
            4*Power(t1,4)*(23 + s + 10*t2 + 2*s*t2 - Power(t2,2) - 
               s1*(1 + 2*t2)) + 
            Power(t1,3)*(-40 + 8*t2 + 110*Power(t2,2) + 9*Power(t2,3) + 
               s1*(4 + 16*t2 - 6*Power(t2,2) - 5*Power(t2,3)) + 
               s*(-52 - 16*t2 + 22*Power(t2,2) + 5*Power(t2,3))) + 
            t1*(-36 + 20*t2 - 64*Power(t2,2) - 36*Power(t2,3) - 
               8*Power(t2,4) + 10*Power(t2,5) + 
               s1*(-8 + 52*t2 - 23*Power(t2,2) + 19*Power(t2,3) - 
                  6*Power(t2,4)) + 
               s*(8 + 28*t2 - 17*Power(t2,2) + Power(t2,3) + 
                  3*Power(t2,4))) - 
            Power(t1,2)*(-68 + 76*(-2 + s1)*t2 - 
               (87 + 25*s1)*Power(t2,2) + 3*(28 + s1)*Power(t2,3) + 
               (11 - 3*s1)*Power(t2,4) + Power(t2,5) + 
               s*(-48 + 4*t2 + Power(t2,2) - 3*Power(t2,3) + 
                  7*Power(t2,4)))))*T2(t1,1 - s2 + t1 - t2))/
     (Power(-1 + t1,2)*(1 - s2 + t1 - t2)*(-1 + t2)*(t1 - s2*t2)*
       (-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) - 
    (8*(-(Power(t1,5)*(4 + (1 + s - s1)*t2)) + 
         Power(t1,4)*(-39 + 7*s2 - 3*t2 + 8*s2*t2 + Power(t2,2) + 
            s2*Power(t2,2) + s1*(-5 + s2 - s2*t2 - 2*Power(t2,2)) + 
            s*(3 - 3*s2 - 2*t2 + 3*Power(t2,2))) + 
         Power(t1,3)*(-12 + 32*s2 + 94*t2 + 36*s2*t2 - 
            9*Power(s2,2)*t2 + 14*Power(t2,2) - 
            4*Power(s2,2)*Power(t2,2) + Power(t2,3) - 2*s2*Power(t2,3) + 
            s1*(-9 + 3*t2 + Power(t2,3) + Power(s2,2)*(-1 + 2*t2) + 
               2*s2*(3 - t2 + Power(t2,2))) + 
            s*(-5 - 10*t2 + 4*Power(t2,2) - 3*Power(t2,3) + 
               s2*(5 + 13*t2 + 2*Power(t2,2)))) + 
         t2*(-((-5 + t2)*t2*(1 + (-1 + s)*t2)) + 
            2*Power(s2,3)*(2 + s1 - 5*t2 - 2*Power(t2,2)) - 
            Power(s2,2)*(14 + 2*(-6 + s)*t2 + (17 + 2*s)*Power(t2,2) + 
               4*Power(t2,3) - s1*(2 + 5*t2)) + 
            s2*(10 + (-15 + 2*s)*t2 - 3*(-2 + s)*Power(t2,2) - 
               (13 + 2*s)*Power(t2,3) + s1*(-4 + 3*t2 + 5*Power(t2,2)))) \
+ t1*(-10 + 2*t2 - 2*s*t2 + 8*Power(s2,3)*t2 + 4*Power(t2,2) - 
            15*s*Power(t2,2) + 16*Power(t2,3) + 
            s2*(14 + 6*t2 + (-6 + 11*s)*Power(t2,2) + 
               (64 + 7*s)*Power(t2,3) + 2*(4 + s)*Power(t2,4)) + 
            Power(s2,2)*(-4 + 64*Power(t2,2) + 9*Power(t2,3) + 
               2*s*t2*Power(1 + t2,2)) - 
            s1*(-4 + 8*t2 - Power(t2,2) + Power(t2,3) + 
               Power(s2,2)*(2 + 6*t2 + Power(t2,2)) + 
               2*s2*(1 + t2 + 7*Power(t2,2) + 2*Power(t2,3)))) - 
         Power(t1,2)*(7 - 7*s2 + 8*Power(s2,2) - 24*t2 + 56*s2*t2 + 
            29*Power(s2,2)*t2 + 72*Power(t2,2) + 92*s2*Power(t2,2) - 
            2*Power(s2,3)*Power(t2,2) + 7*Power(t2,3) + 
            16*s2*Power(t2,3) - 4*Power(s2,2)*Power(t2,3) + Power(t2,4) - 
            s2*Power(t2,4) + s1*
             (-10 - 4*t2 + 2*Power(s2,3)*t2 - Power(t2,2) + 
               Power(s2,2)*(-3 - 2*t2 + 4*Power(t2,2)) + 
               s2*(5 - 9*t2 - 9*Power(t2,2) + Power(t2,3))) + 
            s*(-2 - 15*t2 - 8*Power(t2,2) + 2*Power(t2,3) - Power(t2,4) + 
               2*Power(s2,2)*t2*(1 + t2) + 
               s2*(2 + 13*t2 + 15*Power(t2,2) + 4*Power(t2,3)))))*T3(t2,t1)\
)/(Power(-1 + t1,2)*(t1 - t2)*(1 - s2 + t1 - t2)*(-1 + t2)*(t1 - s2*t2)) - 
    (8*(4*Power(s2,3)*t2*(3 + t2) + 
         (1 + (-1 + s)*t2)*(5 - 6*t2 + Power(t2,2)) + 
         Power(t1,3)*(-8 + (-1 - s + s1)*t2 + (1 + s - s1)*Power(t2,2)) + 
         t1*((38 + s1*(-5 + t2) - 10*t2)*(-1 + t2) + 
            s*(5 + t2 + 2*Power(t2,2))) + 
         Power(t1,2)*(-31 + 5*s1*(-1 + t2) + 8*t2 - 2*Power(t2,2) + 
            Power(t2,3) - s*(5 + 5*t2 - 3*Power(t2,2) + Power(t2,3))) + 
         Power(s2,2)*(3*s1*(-1 + t1)*(-1 + t2) - 
            t1*(12 + 3*(7 + 2*s)*t2 + (7 + 2*s)*Power(t2,2)) + 
            t2*(-43 + 15*t2 + 4*Power(t2,2) + 2*s*(3 + t2))) + 
         s2*(-3 + 39*t2 - 13*s*t2 - 41*Power(t2,2) + 3*s*Power(t2,2) + 
            5*Power(t2,3) + 2*s*Power(t2,3) + 
            s1*(-1 + t1)*(-1 + t2)*(-5 + t1*(-3 + t2) + t2) + 
            Power(t1,2)*(17 + 15*t2 + Power(t2,2) - Power(t2,3) + 
               s*(3 + 5*t2)) + 
            t1*(46 + 10*t2 - 8*Power(t2,2) - 
               s*(3 - 8*t2 + 3*Power(t2,2) + 2*Power(t2,3)))))*T4(-1 + t2))/
     (Power(-1 + t1,2)*(1 - s2 + t1 - t2)*(-1 + t2)*(t1 - s2*t2)) + 
    (8*(4*Power(s2,4)*t2 - Power(t1,4)*(-4 + (1 + s - s1)*t2) + 
         (1 + (-1 + s)*t2)*(5 - 6*t2 + Power(t2,2)) + 
         t1*(-1 + t2)*((-5 + s1)*(-5 + t2) + s*(7 + t2)) + 
         Power(s2,3)*(3*s1*(-1 + t1) + t2*(3 + 2*s + 8*t2) - 
            t1*(4 + 15*t2 + 2*s*t2)) - 
         Power(t1,3)*(15 + 8*t2 + s*(-3 + 3*t2 - 2*Power(t2,2)) + 
            s1*(1 - t2 + Power(t2,2))) + 
         Power(t1,2)*((-1 + t2)*(17 + 4*s1 + 3*t2 + Power(t2,2)) - 
            s*(-4 + 7*t2 - 3*Power(t2,2) + Power(t2,3))) + 
         Power(s2,2)*(3 + s1*(-1 + t1)*(t1*(-6 + t2) + 8*(-1 + t2)) - 
            31*t2 - s*t2 + 24*Power(t2,2) + 4*s*Power(t2,2) + 
            4*Power(t2,3) + Power(t1,2)*
             (15 + 15*t2 - Power(t2,2) + s*(5 + 2*t2)) - 
            t1*(6 + 16*t2 + 23*Power(t2,2) + s*(5 + t2 + 4*Power(t2,2)))) \
+ s2*(Power(t1,3)*(-15 + s*(-5 + t2) - 3*t2 + Power(t2,2)) + 
            (-1 + t2)*(8 + (-29 + 6*s)*t2 + (1 + 2*s)*Power(t2,2)) - 
            s1*(-1 + t1)*(-5 + 6*t2 - Power(t2,2) + 
               Power(t1,2)*(-3 + 2*t2) - t1*(9 - 10*t2 + Power(t2,2))) + 
            Power(t1,2)*(14 + 34*t2 + 10*Power(t2,2) - Power(t2,3) + 
               s*(-7 + 12*t2 + Power(t2,2))) - 
            t1*(-33 + 20*t2 + 9*Power(t2,2) + 4*Power(t2,3) + 
               s*(-12 + 7*t2 + 5*Power(t2,2) + 2*Power(t2,3)))))*
       T5(1 - s2 + t1 - t2))/
     (Power(-1 + t1,2)*(1 - s2 + t1 - t2)*(-1 + t2)*(t1 - s2*t2)));
   return a;
};
