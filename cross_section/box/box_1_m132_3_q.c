#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_1_m132_3_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(-40*Power(s2,2) - 15*Power(s2,3) + 71*Power(s2,4) + 
         56*Power(s2,5) + 6*Power(s2,6) + 80*s2*t1 + 93*Power(s2,2)*t1 - 
         226*Power(s2,3)*t1 - 268*Power(s2,4)*t1 - 46*Power(s2,5)*t1 - 
         40*Power(t1,2) - 141*s2*Power(t1,2) + 
         244*Power(s2,2)*Power(t1,2) + 509*Power(s2,3)*Power(t1,2) + 
         141*Power(s2,4)*Power(t1,2) + 63*Power(t1,3) - 
         94*s2*Power(t1,3) - 479*Power(s2,2)*Power(t1,3) - 
         224*Power(s2,3)*Power(t1,3) + 5*Power(t1,4) + 
         223*s2*Power(t1,4) + 196*Power(s2,2)*Power(t1,4) - 
         41*Power(t1,5) - 90*s2*Power(t1,5) + 17*Power(t1,6) + 
         Power(s1,5)*(9*Power(s2,3) + Power(s2,2)*(5 - 23*t1) + 
            t1*(4 + t1 - 5*Power(t1,2)) + s2*(-4 - 6*t1 + 19*Power(t1,2))) \
- 24*s2*t2 + 38*Power(s2,2)*t2 + 93*Power(s2,3)*t2 + 8*Power(s2,4)*t2 - 
         31*Power(s2,5)*t2 - 2*Power(s2,6)*t2 + 24*t1*t2 - 68*s2*t1*t2 - 
         279*Power(s2,2)*t1*t2 - 61*Power(s2,3)*t1*t2 + 
         111*Power(s2,4)*t1*t2 + 23*Power(s2,5)*t1*t2 + 
         30*Power(t1,2)*t2 + 295*s2*Power(t1,2)*t2 + 
         153*Power(s2,2)*Power(t1,2)*t2 - 144*Power(s2,3)*Power(t1,2)*t2 - 
         87*Power(s2,4)*Power(t1,2)*t2 - 109*Power(t1,3)*t2 - 
         155*s2*Power(t1,3)*t2 + 76*Power(s2,2)*Power(t1,3)*t2 + 
         158*Power(s2,3)*Power(t1,3)*t2 + 55*Power(t1,4)*t2 - 
         9*s2*Power(t1,4)*t2 - 152*Power(s2,2)*Power(t1,4)*t2 - 
         3*Power(t1,5)*t2 + 75*s2*Power(t1,5)*t2 - 15*Power(t1,6)*t2 - 
         8*Power(t2,2) + 65*s2*Power(t2,2) + 7*Power(s2,2)*Power(t2,2) - 
         97*Power(s2,3)*Power(t2,2) - 29*Power(s2,4)*Power(t2,2) - 
         6*Power(s2,5)*Power(t2,2) - 2*Power(s2,6)*Power(t2,2) - 
         57*t1*Power(t2,2) - 24*s2*t1*Power(t2,2) + 
         225*Power(s2,2)*t1*Power(t2,2) + 78*Power(s2,3)*t1*Power(t2,2) + 
         73*Power(s2,4)*t1*Power(t2,2) + 11*Power(s2,5)*t1*Power(t2,2) + 
         17*Power(t1,2)*Power(t2,2) - 198*s2*Power(t1,2)*Power(t2,2) - 
         93*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         217*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         24*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         70*Power(t1,3)*Power(t2,2) + 68*s2*Power(t1,3)*Power(t2,2) + 
         273*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         26*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         24*Power(t1,4)*Power(t2,2) - 157*s2*Power(t1,4)*Power(t2,2) - 
         14*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         34*Power(t1,5)*Power(t2,2) + 3*s2*Power(t1,5)*Power(t2,2) + 
         24*Power(t2,3) - 87*s2*Power(t2,3) - 40*Power(s2,2)*Power(t2,3) + 
         70*Power(s2,3)*Power(t2,3) - 26*Power(s2,4)*Power(t2,3) - 
         9*Power(s2,5)*Power(t2,3) + 63*t1*Power(t2,3) + 
         103*s2*t1*Power(t2,3) - 146*Power(s2,2)*t1*Power(t2,3) + 
         124*Power(s2,3)*t1*Power(t2,3) + 34*Power(s2,4)*t1*Power(t2,3) - 
         63*Power(t1,2)*Power(t2,3) + 108*s2*Power(t1,2)*Power(t2,3) - 
         190*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         48*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         32*Power(t1,3)*Power(t2,3) + 112*s2*Power(t1,3)*Power(t2,3) + 
         30*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         20*Power(t1,4)*Power(t2,3) - 7*s2*Power(t1,4)*Power(t2,3) - 
         24*Power(t2,4) + 41*s2*Power(t2,4) + 43*Power(s2,2)*Power(t2,4) - 
         40*Power(s2,3)*Power(t2,4) - 8*Power(s2,4)*Power(t2,4) - 
         17*t1*Power(t2,4) - 98*s2*t1*Power(t2,4) + 
         75*Power(s2,2)*t1*Power(t2,4) + 21*Power(s2,3)*t1*Power(t2,4) + 
         55*Power(t1,2)*Power(t2,4) - 33*s2*Power(t1,2)*Power(t2,4) - 
         18*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         2*Power(t1,3)*Power(t2,4) + 5*s2*Power(t1,3)*Power(t2,4) + 
         8*Power(t2,5) + 5*s2*Power(t2,5) - 6*Power(s2,2)*Power(t2,5) - 
         Power(s2,3)*Power(t2,5) - 13*t1*Power(t2,5) + 
         3*s2*t1*Power(t2,5) + 2*Power(s2,2)*t1*Power(t2,5) + 
         3*Power(t1,2)*Power(t2,5) - s2*Power(t1,2)*Power(t2,5) + 
         4*Power(s,5)*(3 - 2*Power(t1,2) + 2*Power(t1,3) + 
            Power(s2,2)*(2 + t1) + Power(s1,2)*(-1 + 2*t1 - t2) + 
            4*t1*t2 - 5*Power(t1,2)*t2 - 2*Power(t2,2) + 
            4*t1*Power(t2,2) - Power(t2,3) - 
            s2*(6 - 3*t1 + 3*Power(t1,2) + 3*t2 - 4*t1*t2 + 
               Power(t2,2)) + 
            s1*(-3 - 3*Power(s2,2) - 3*t1 + 4*Power(t1,2) + 3*t2 - 
               6*t1*t2 + 2*Power(t2,2) + s2*(8 - 3*t1 + t2))) - 
         Power(s1,4)*(s2 - t1)*
          (3 + 6*Power(s2,3) - 7*Power(t1,3) - 15*t2 - t1*(4 + t2) + 
            2*Power(t1,2)*(6 + 7*t2) + Power(s2,2)*(8 - 23*t1 + 33*t2) + 
            s2*(1 + 24*Power(t1,2) + 20*t2 - t1*(24 + 47*t2))) + 
         Power(s1,3)*(7*Power(s2,5) + Power(t1,5) - 8*Power(-1 + t2,2) + 
            Power(t1,4)*(31 + 9*t2) + Power(s2,4)*(31 - 21*t1 + 18*t2) + 
            Power(t1,2)*(-16 + t2 - 6*Power(t2,2)) - 
            4*Power(t1,3)*(2 + 11*t2 + 3*Power(t2,2)) + 
            2*t1*(8 - 3*t2 + 17*Power(t2,2)) + 
            Power(s2,3)*(33 + 20*Power(t1,2) + 44*t2 + 46*Power(t2,2) - 
               t1*(125 + 68*t2)) - 
            s2*(8 + 3*Power(t1,4) + 10*t2 + 26*Power(t2,2) + 
               25*Power(t1,3)*(5 + 2*t2) + 
               t1*(5 - 21*t2 + 30*Power(t2,2)) - 
               Power(t1,2)*(56 + 137*t2 + 70*Power(t2,2))) + 
            Power(s2,2)*(21 - 4*Power(t1,3) - 22*t2 + 36*Power(t2,2) + 
               Power(t1,2)*(188 + 91*t2) - 
               t1*(81 + 137*t2 + 104*Power(t2,2)))) + 
         Power(s1,2)*(2*Power(s2,6) + 3*Power(t1,6) + 
            Power(t1,5)*(38 - 8*t2) + 8*Power(-1 + t2,2)*(-1 + 3*t2) - 
            Power(s2,5)*(22 + 11*t1 + 19*t2) + 
            Power(t1,4)*(-1 - 98*t2 + 3*Power(t2,2)) + 
            2*Power(t1,3)*(-27 + 5*t2 + 25*Power(t2,2) + Power(t2,3)) + 
            Power(t1,2)*(51 - 7*t2 + 65*Power(t2,2) + 14*Power(t2,3)) - 
            t1*(5 - 3*t2 + 14*Power(t2,2) + 52*Power(t2,3)) + 
            Power(s2,4)*(-101 + 27*Power(t1,2) - 78*t2 - 
               26*Power(t2,2) + 2*t1*(78 + 25*t2)) - 
            Power(s2,3)*(25 + 38*Power(t1,3) + 26*t2 + 104*Power(t2,2) + 
               30*Power(t2,3) + Power(t1,2)*(374 + 28*t2) - 
               t1*(301 + 360*t2 + 70*Power(t2,2))) + 
            s2*(13 - 15*Power(t1,5) - 43*t2 + 70*Power(t2,2) + 
               28*Power(t2,3) + Power(t1,4)*(-204 + 31*t2) + 
               Power(t1,3)*(101 + 400*t2 + 12*Power(t2,2)) + 
               t1*(-132 + 97*t2 - 155*Power(t2,2) + 24*Power(t2,3)) - 
               Power(t1,2)*(-110 + 90*t2 + 199*Power(t2,2) + 
                  34*Power(t2,3))) + 
            Power(s2,2)*(81 + 32*Power(t1,4) + 
               Power(t1,3)*(406 - 26*t2) - 90*t2 + 90*Power(t2,2) - 
               38*Power(t2,3) - 
               Power(t1,2)*(300 + 584*t2 + 59*Power(t2,2)) + 
               t1*(-31 + 106*t2 + 253*Power(t2,2) + 62*Power(t2,3)))) + 
         s1*(6*Power(s2,6) - 3*Power(t1,6)*(-6 + t2) - 
            8*Power(-1 + t2,2)*t2*(-2 + 3*t2) + 
            Power(t1,5)*(4 - 78*t2 + 7*Power(t2,2)) + 
            Power(s2,5)*(19 - 45*t1 + 32*t2 + 21*Power(t2,2)) + 
            Power(t1,4)*(-40 + 21*t2 + 87*Power(t2,2) - 5*Power(t2,3)) + 
            Power(t1,2)*(30 - 88*t2 + 86*Power(t2,2) - 117*Power(t2,3) - 
               11*Power(t2,4)) + 
            Power(t1,3)*(28 + 14*t2 + 30*Power(t2,2) - 16*Power(t2,3) + 
               Power(t2,4)) + 
            t1*(-24 + 62*t2 - 82*Power(t2,2) + 34*Power(t2,3) + 
               42*Power(t2,4)) + 
            Power(s2,4)*(-73 - 3*Power(t1,2)*(-46 + t2) + 148*t2 + 
               73*Power(t2,2) + 22*Power(t2,3) - 
               3*t1*(17 + 85*t2 + 21*Power(t2,2))) + 
            Power(s2,3)*(-84 + 116*t2 - 77*Power(t2,2) + 
               108*Power(t2,3) + 9*Power(t2,4) + 
               6*Power(t1,3)*(-37 + 2*t2) + 
               7*Power(t1,2)*(5 + 93*t2 + 8*Power(t2,2)) - 
               t1*(-253 + 433*t2 + 359*Power(t2,2) + 52*Power(t2,3))) + 
            Power(s2,2)*(22 + Power(t1,3)*(11 - 743*t2) - 
               18*Power(t1,4)*(-11 + t2) - 108*t2 + 109*Power(t2,2) - 
               110*Power(t2,3) + 23*Power(t2,4) + 
               Power(t1,2)*(-327 + 443*t2 + 586*Power(t2,2) + 
                  33*Power(t2,3)) + 
               t1*(180 - 152*t2 + 121*Power(t2,2) - 223*Power(t2,3) - 
                  17*Power(t2,4))) + 
            s2*(3*Power(t1,5)*(-31 + 4*t2) - 
               3*Power(t1,4)*(6 - 131*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(187 - 179*t2 - 387*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-52 + 196*t2 - 195*Power(t2,2) + 227*Power(t2,3) - 
                  12*Power(t2,4)) + 
               Power(t1,2)*(-124 + 22*t2 - 74*Power(t2,2) + 
                  131*Power(t2,3) + 7*Power(t2,4)) - 
               2*(-12 + 39*t2 - 69*Power(t2,2) + 49*Power(t2,3) + 
                  9*Power(t2,4)))) + 
         Power(s,4)*(-48 + 66*t1 + 38*Power(t1,2) - 27*Power(t1,3) + 
            19*Power(t1,4) - 2*Power(s2,3)*(10 + 5*t1 - t2) - 6*t2 - 
            84*t1*t2 + 33*Power(t1,2)*t2 - 38*Power(t1,3)*t2 + 
            46*Power(t2,2) + 15*t1*Power(t2,2) + 
            16*Power(t1,2)*Power(t2,2) - 21*Power(t2,3) + 
            6*t1*Power(t2,3) - 3*Power(t2,4) + 
            Power(s1,3)*(2 + 10*s2 - 21*t1 + 9*t2) + 
            Power(s1,2)*(4 + 10*Power(s2,2) + t1 - 23*Power(t1,2) + 
               2*s2*(-9 + 14*t1 - 10*t2) - 21*t2 + 60*t1*t2 - 
               21*Power(t2,2)) + 
            Power(s2,2)*(58 + 39*Power(t1,2) + t1*(11 - 44*t2) + 13*t2 + 
               21*Power(t2,2)) + 
            2*s2*(15 - 24*Power(t1,3) + 16*t2 - 24*Power(t2,2) + 
               8*Power(t2,3) + 2*Power(t1,2)*(9 + 20*t2) - 
               6*t1*(12 - t2 + 4*Power(t2,2))) + 
            s1*(54 + 40*Power(s2,3) + 17*Power(t1,3) - 50*t2 + 
               40*Power(t2,2) + 15*Power(t2,3) - 
               3*Power(s2,2)*(32 + 9*t1 + 7*t2) + 
               Power(t1,2)*(-28 + 13*t2) - 
               3*t1*(6 + 4*t2 + 15*Power(t2,2)) + 
               s2*(-26 - 30*Power(t1,2) + 62*t2 - 6*Power(t2,2) + 
                  2*t1*(57 + 2*t2)))) - 
         Power(s,3)*(-60 + 202*t1 - 55*Power(t1,2) - 68*Power(t1,3) + 
            43*Power(t1,4) - 14*Power(t1,5) - 
            2*Power(s2,4)*(6 + 4*t1 - 3*t2) - 10*t2 - 170*t1*t2 + 
            46*Power(t1,2)*t2 - 49*Power(t1,3)*t2 + 13*Power(t1,4)*t2 + 
            105*Power(t2,2) + 144*t1*Power(t2,2) - 
            7*Power(t1,2)*Power(t2,2) + 22*Power(t1,3)*Power(t2,2) - 
            122*Power(t2,3) - 11*t1*Power(t2,3) - 
            28*Power(t1,2)*Power(t2,3) + 24*Power(t2,4) + 
            8*t1*Power(t2,4) - Power(t2,5) + 
            Power(s1,4)*(-6 + 19*s2 - 18*t1 + 5*t2) + 
            Power(s2,3)*(76 + 38*Power(t1,2) + t1*(24 - 49*t2) + 11*t2 + 
               37*Power(t2,2)) + 
            Power(s1,3)*(-5 + 12*Power(s2,2) + 12*Power(t1,2) + 
               s2*(19 - 13*t1 - 40*t2) + 4*t2 - 14*Power(t2,2) + 
               5*t1*(-3 + 8*t2)) + 
            Power(s2,2)*(145 - 66*Power(t1,3) + 24*t2 - 54*Power(t2,2) + 
               15*Power(t2,3) + 31*Power(t1,2)*(1 + 3*t2) - 
               t1*(278 + t2 + 66*Power(t2,2))) + 
            s2*(-82 + 50*Power(t1,4) + 46*t2 - 126*Power(t2,2) + 
               67*Power(t2,3) - 17*Power(t2,4) - 
               Power(t1,3)*(86 + 63*t2) + 
               Power(t1,2)*(270 + 39*t2 + 7*Power(t2,2)) + 
               t1*(-150 + 64*t2 - 20*Power(t2,2) + 23*Power(t2,3))) + 
            Power(s1,2)*(23 + 43*Power(s2,3) + 64*Power(t1,3) + 
               Power(s2,2)*(-58 + 30*t1 - 39*t2) - 92*t2 + 
               34*Power(t2,2) + 12*Power(t2,3) - 
               Power(t1,2)*(25 + 82*t2) + 
               t1*(54 + 23*t2 - 18*Power(t2,2)) + 
               s2*(-32 - 137*Power(t1,2) + t2 + 6*Power(t2,2) + 
                  t1*(80 + 113*t2))) + 
            s1*(70 + 46*Power(s2,4) + 20*Power(t1,4) + 
               Power(t1,3)*(27 - 104*t2) - 128*t2 + 219*Power(t2,2) - 
               56*Power(t2,3) - 2*Power(t2,4) - 
               Power(s2,3)*(111 + 107*t1 + 56*t2) + 
               Power(s2,2)*(-195 + 96*Power(t1,2) + t1*(293 - 34*t2) + 
                  130*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(111 + 26*t2 + 98*Power(t2,2)) - 
               t1*(104 + 178*t2 - 3*Power(t2,2) + 12*Power(t2,3)) + 
               s2*(108 - 55*Power(t1,3) + 138*t2 - 87*Power(t2,2) + 
                  32*Power(t2,3) + Power(t1,2)*(-209 + 194*t2) + 
                  t1*(10 - 72*t2 - 123*Power(t2,2))))) + 
         Power(s,2)*(-24 + Power(s1,5)*(-4 + 9*s2 - 5*t1) + 192*t1 - 
            242*Power(t1,2) - 3*Power(t1,3) + 84*Power(t1,4) - 
            42*Power(t1,5) + 3*Power(t1,6) - 12*t2 - 130*t1*t2 + 
            186*Power(t1,2)*t2 - 32*Power(t1,3)*t2 + 59*Power(t1,4)*t2 + 
            8*Power(t1,5)*t2 + 84*Power(t2,2) + 81*t1*Power(t2,2) - 
            166*Power(t1,2)*Power(t2,2) - 23*Power(t1,3)*Power(t2,2) - 
            28*Power(t1,4)*Power(t2,2) - 144*Power(t2,3) + 
            44*t1*Power(t2,3) + 31*Power(t1,2)*Power(t2,3) + 
            22*Power(t1,3)*Power(t2,3) + 70*Power(t2,4) - 
            19*t1*Power(t2,4) - 7*Power(t1,2)*Power(t2,4) - 
            6*Power(t2,5) + 2*t1*Power(t2,5) + 
            Power(s2,5)*(4 - 2*t1 + 6*t2) + 
            Power(s1,4)*(1 + 32*Power(s2,2) + 29*Power(t1,2) + 
               s2*(5 - 57*t1 - 23*t2) + 10*t2 + t1*(-1 + 4*t2)) + 
            Power(s2,4)*(98 + 11*Power(t1,2) + 29*t2 + 25*Power(t2,2) - 
               5*t1*(7 + 6*t2)) + 
            Power(s1,3)*(1 + Power(s2,3) + 40*Power(t1,3) + 
               Power(t1,2)*(5 - 98*t2) + 
               Power(s2,2)*(38 + 66*t1 - 89*t2) - 71*t2 - 
               s2*(32 + 107*Power(t1,2) + t1*(39 - 182*t2) + 9*t2 - 
                  18*Power(t2,2)) + 2*t1*(43 - 4*t2 + 8*Power(t2,2))) + 
            Power(s2,3)*(216 - 24*Power(t1,3) + 17*t2 + 62*Power(t2,2) - 
               11*Power(t2,3) + Power(t1,2)*(123 + 46*t2) - 
               t1*(375 + 170*t2 + 41*Power(t2,2))) + 
            Power(s2,2)*(10 + 26*Power(t1,4) + 88*t2 - 244*Power(t2,2) + 
               171*Power(t2,3) - 33*Power(t2,4) - 
               Power(t1,3)*(199 + 18*t2) + 
               Power(t1,2)*(540 + 312*t2 - 21*Power(t2,2)) + 
               t1*(-519 + 114*t2 - 276*Power(t2,2) + 62*Power(t2,3))) + 
            s2*(-144 - 14*Power(t1,5) + Power(t1,4)*(149 - 12*t2) + 
               46*t2 + 4*Power(t2,2) - 45*Power(t2,3) + 8*Power(t2,4) - 
               3*Power(t2,5) + 
               Power(t1,3)*(-347 - 230*t2 + 65*Power(t2,2)) + 
               Power(t1,2)*(306 - 99*t2 + 237*Power(t2,2) - 
                  73*Power(t2,3)) + 
               t1*(208 - 178*t2 + 299*Power(t2,2) - 164*Power(t2,3) + 
                  37*Power(t2,4))) + 
            Power(s1,2)*(20 + 58*Power(s2,4) - 24*Power(t1,4) + 
               Power(t1,3)*(19 - 40*t2) - 118*t2 + 209*Power(t2,2) - 
               20*Power(t2,3) - Power(s2,3)*(57 + 89*t1 + 21*t2) + 
               Power(s2,2)*(-75 - 20*Power(t1,2) + t1*(162 - 32*t2) + 
                  27*t2 + 49*Power(t2,2)) + 
               Power(t1,2)*(61 - 15*t2 + 102*Power(t2,2)) + 
               t1*(21 - 184*t2 - 22*Power(t2,3)) + 
               s2*(64 + 75*Power(t1,3) + 39*t2 + 11*Power(t2,2) - 
                  6*Power(t2,3) + 31*Power(t1,2)*(-4 + 3*t2) + 
                  t1*(-33 + 26*t2 - 156*Power(t2,2)))) + 
            s1*(36 + 20*Power(s2,5) - 27*Power(t1,5) - 104*t2 + 
               261*Power(t2,2) - 209*Power(t2,3) + 20*Power(t2,4) + 
               5*Power(t1,4)*(-5 + 14*t2) - 
               Power(s2,4)*(58 + 81*t1 + 65*t2) - 
               2*Power(t1,3)*(30 + t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(185 + 47*t2 - 21*Power(t2,2) - 
                  26*Power(t2,3)) + 
               t1*(-114 - 74*t2 + 54*Power(t2,2) + 28*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,3)*(-316 + 150*Power(t1,2) + 49*t2 + 
                  31*Power(t2,2) + t1*(311 + 50*t2)) - 
               Power(s2,2)*(53 + 164*Power(t1,3) + 
                  Power(t1,2)*(473 - 165*t2) - 297*t2 + 
                  236*Power(t2,2) - 41*Power(t2,3) + 
                  t1*(-508 + 8*t2 + 96*Power(t2,2))) + 
               s2*(150 + 102*Power(t1,4) - 96*t2 + 38*Power(t2,2) - 
                  15*Power(t2,3) + 5*Power(t2,4) - 
                  5*Power(t1,3)*(-49 + 44*t2) + 
                  3*Power(t1,2)*(-44 - 13*t2 + 29*Power(t2,2)) - 
                  3*t1*(68 + 62*t2 - 59*Power(t2,2) + 2*Power(t2,3))))) + 
         s*(-56*t1 + 195*Power(t1,2) - 115*Power(t1,3) - 69*Power(t1,4) + 
            71*Power(t1,5) - 18*Power(t1,6) - 
            Power(s1,5)*(-4 + s2 + 18*Power(s2,2) + 3*t1 - 28*s2*t1 + 
               10*Power(t1,2)) + 8*t2 - 6*t1*t2 - 177*Power(t1,2)*t2 + 
            161*Power(t1,3)*t2 - 65*Power(t1,4)*t2 + 12*Power(t1,5)*t2 + 
            3*Power(t1,6)*t2 - 9*Power(t2,2) + 77*t1*Power(t2,2) + 
            120*Power(t1,2)*Power(t2,2) - 32*Power(t1,3)*Power(t2,2) + 
            27*Power(t1,4)*Power(t2,2) - 6*Power(t1,5)*Power(t2,2) + 
            23*Power(t2,3) - 135*t1*Power(t2,3) - 
            62*Power(t1,2)*Power(t2,3) - 21*Power(t1,3)*Power(t2,3) + 
            4*Power(t1,4)*Power(t2,3) - 17*Power(t2,4) + 
            93*t1*Power(t2,4) + 3*Power(t1,2)*Power(t2,4) - 
            2*Power(t1,3)*Power(t2,4) - 5*Power(t2,5) - 3*t1*Power(t2,5) + 
            Power(t1,2)*Power(t2,5) - 2*Power(s2,6)*(2 + t2) + 
            Power(s2,5)*(-62 - 17*t2 - 3*Power(t2,2) + 9*t1*(4 + t2)) + 
            Power(s2,4)*(-185 + 6*t2 - 54*Power(t2,2) + 23*Power(t2,3) - 
               Power(t1,2)*(122 + 13*t2) + 
               t1*(305 + 118*t2 - 4*Power(t2,2))) + 
            Power(s2,3)*(-99 - 92*t2 + 101*Power(t2,2) - 57*Power(t2,3) + 
               27*Power(t2,4) + 2*Power(t1,3)*(104 + t2) + 
               Power(t1,2)*(-614 - 264*t2 + 36*Power(t2,2)) + 
               t1*(651 + 11*t2 + 168*Power(t2,2) - 79*Power(t2,3))) + 
            Power(s2,2)*(107 + 12*Power(t1,4)*(-16 + t2) - 133*t2 + 
               158*Power(t2,2) - 147*Power(t2,3) + 56*Power(t2,4) + 
               3*Power(t2,5) + 
               Power(t1,3)*(632 + 254*t2 - 54*Power(t2,2)) + 
               3*Power(t1,2)*(-272 - 35*t2 - 49*Power(t2,2) + 
                  31*Power(t2,3)) + 
               t1*(115 + 231*t2 - 90*Power(t2,2) + 29*Power(t2,3) - 
                  50*Power(t2,4))) + 
            s2*(56 + Power(t1,5)*(92 - 11*t2) + 14*t2 - 131*Power(t2,2) + 
               192*Power(t2,3) - 113*Power(t2,4) + 12*Power(t2,5) + 
               Power(t1,4)*(-332 - 103*t2 + 31*Power(t2,2)) + 
               Power(t1,3)*(419 + 153*t2 + 6*Power(t2,2) - 
                  41*Power(t2,3)) + 
               Power(t1,2)*(99 - 300*t2 + 21*Power(t2,2) + 
                  49*Power(t2,3) + 25*Power(t2,4)) - 
               t1*(302 - 294*t2 + 239*Power(t2,2) - 183*Power(t2,3) + 
                  56*Power(t2,4) + 4*Power(t2,5))) + 
            Power(s1,4)*(-5 - 7*Power(s2,3) + 4*Power(t1,3) - 7*t2 + 
               t1*(-3 + 9*t2) + Power(t1,2)*(5 + 23*t2) + 
               Power(s2,2)*(-3 + 10*t1 + 61*t2) - 
               s2*(7*Power(t1,2) - 10*t2 + t1*(6 + 84*t2))) - 
            Power(s1,3)*(6*Power(s2,4) - 32*Power(t1,4) + 
               Power(s2,3)*(52 + 37*t1 - 22*t2) - 2*t2*(21 + t2) + 
               Power(t1,3)*(-23 + 40*t2) + 
               t1*(63 + 38*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(-73 + 48*t2 + 10*Power(t2,2)) + 
               Power(s2,2)*(6 - 124*Power(t1,2) + 31*t2 + 
                  78*Power(t2,2) + 2*t1*(-62 + 37*t2)) + 
               s2*(-18 + 113*Power(t1,3) + Power(t1,2)*(95 - 92*t2) - 
                  69*t2 + 36*Power(t2,2) + 
                  t1*(60 - 84*t2 - 88*Power(t2,2)))) + 
            Power(s1,2)*(11 - 27*Power(s2,5) + 12*Power(t1,5) + 
               Power(t1,4)*(37 - 66*t2) + 11*t2 - 86*Power(t2,2) - 
               4*Power(t2,3) + Power(s2,4)*(43 + 94*t1 + 25*t2) + 
               Power(t1,3)*(118 - 111*t2 + 66*Power(t2,2)) + 
               t1*(7 + 67*t2 + 178*Power(t2,2) - 6*Power(t2,3)) - 
               2*Power(t1,2)*(53 + 109*t2 - 42*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s2,3)*(140 - 132*Power(t1,2) + 73*t2 + 
                  4*Power(t2,2) + t1*(-228 + 35*t2)) + 
               Power(s2,2)*(-88 + 102*Power(t1,3) + 
                  Power(t1,2)*(364 - 211*t2) - 81*t2 + 127*Power(t2,2) + 
                  48*Power(t2,3) + t1*(-112 - 323*t2 + 68*Power(t2,2))) - 
               s2*(69 + 49*Power(t1,4) + Power(t1,3)*(216 - 217*t2) - 
                  96*t2 + 251*Power(t2,2) - 58*Power(t2,3) + 
                  Power(t1,2)*(146 - 361*t2 + 138*Power(t2,2)) + 
                  t1*(-221 - 255*t2 + 206*Power(t2,2) + 40*Power(t2,3)))) \
+ s1*(-8 - 2*Power(s2,6) + 4*Power(t1,5) - 6*Power(t1,6) - 2*t2 - 
               34*Power(t2,2) + 66*Power(t2,3) + 10*Power(t2,4) + 
               Power(s2,5)*(5 + 13*t1 + 26*t2) + 
               Power(t1,4)*(49 - 78*t2 + 30*Power(t2,2)) + 
               Power(t1,3)*(15 - 128*t2 + 109*Power(t2,2) - 
                  28*Power(t2,3)) + 
               Power(t1,2)*(-96 + 52*t2 + 207*Power(t2,2) - 
                  44*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(74 - 96*t2 + 131*Power(t2,2) - 230*Power(t2,3) + 
                  9*Power(t2,4)) - 
               Power(s2,4)*(-140 + 38*Power(t1,2) + 25*t2 + 
                  42*Power(t2,2) + t1*(77 + 64*t2)) + 
               Power(s2,3)*(236 + 62*Power(t1,3) - 265*t2 + 
                  36*Power(t2,2) - 46*Power(t2,3) + 
                  Power(t1,2)*(197 + 36*t2) + 
                  t1*(-500 + 194*t2 + 81*Power(t2,2))) - 
               Power(s2,2)*(36 + 58*Power(t1,4) + 
                  Power(t1,3)*(179 - 16*t2) + 20*t2 - 234*Power(t2,2) + 
                  149*Power(t2,3) + 16*Power(t2,4) + 
                  Power(t1,2)*(-629 + 391*t2 + 6*Power(t2,2)) - 
                  t1*(-379 + 212*t2 + 170*Power(t2,2) + 46*Power(t2,3))) + 
               s2*(-82 + 29*Power(t1,5) + Power(t1,4)*(50 - 14*t2) + 
                  212*t2 - 306*Power(t2,2) + 295*Power(t2,3) - 
                  43*Power(t2,4) + 
                  Power(t1,3)*(-318 + 300*t2 - 63*Power(t2,2)) + 
                  Power(t1,2)*
                   (128 + 181*t2 - 315*Power(t2,2) + 28*Power(t2,3)) + 
                  2*t1*(74 - 49*t2 - 189*Power(t2,2) + 92*Power(t2,3) + 
                     6*Power(t2,4)))))))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(1 - s + s2 - t1)*(-1 + s - s1 + t2)*
       (s - s1 + t2)*(-1 + s2 - t1 + t2)*Power(-s1 + s2 - t1 + t2,2)) + 
    (8*(24 - 33*s2 + 11*Power(s2,2) - 3*Power(s2,3) - 2*Power(s2,4) - 
         Power(s2,5) + 41*t1 + 25*s2*t1 - 39*Power(s2,2)*t1 + 
         9*Power(s2,3)*t1 + 7*Power(s2,4)*t1 - 2*Power(s2,5)*t1 - 
         36*Power(t1,2) + 85*s2*Power(t1,2) - 
         16*Power(s2,2)*Power(t1,2) - 22*Power(s2,3)*Power(t1,2) + 
         12*Power(s2,4)*Power(t1,2) + Power(s2,5)*Power(t1,2) - 
         43*Power(t1,3) + 13*s2*Power(t1,3) + 
         34*Power(s2,2)*Power(t1,3) - 28*Power(s2,3)*Power(t1,3) - 
         5*Power(s2,4)*Power(t1,3) - 4*Power(t1,4) - 25*s2*Power(t1,4) + 
         32*Power(s2,2)*Power(t1,4) + 10*Power(s2,3)*Power(t1,4) + 
         7*Power(t1,5) - 18*s2*Power(t1,5) - 10*Power(s2,2)*Power(t1,5) + 
         4*Power(t1,6) + 5*s2*Power(t1,6) - Power(t1,7) - 
         2*Power(s,6)*Power(t1 - t2,2)*(1 - s2 + t1 - t2) - 58*t2 - 
         4*Power(s2,2)*t2 + 37*Power(s2,3)*t2 - 9*Power(s2,4)*t2 - 
         Power(s2,5)*t2 + 2*Power(s2,6)*t2 - 10*t1*t2 - 148*s2*t1*t2 - 
         18*Power(s2,2)*t1*t2 + 54*Power(s2,3)*t1*t2 + 
         7*Power(s2,4)*t1*t2 - 13*Power(s2,5)*t1*t2 - 
         2*Power(s2,6)*t1*t2 + 148*Power(t1,2)*t2 - 
         71*s2*Power(t1,2)*t2 - 106*Power(s2,2)*Power(t1,2)*t2 - 
         9*Power(s2,3)*Power(t1,2)*t2 + 31*Power(s2,4)*Power(t1,2)*t2 + 
         10*Power(s2,5)*Power(t1,2)*t2 + 52*Power(t1,3)*t2 + 
         86*s2*Power(t1,3)*t2 - 5*Power(s2,2)*Power(t1,3)*t2 - 
         34*Power(s2,3)*Power(t1,3)*t2 - 20*Power(s2,4)*Power(t1,3)*t2 - 
         25*Power(t1,4)*t2 + 14*s2*Power(t1,4)*t2 + 
         16*Power(s2,2)*Power(t1,4)*t2 + 20*Power(s2,3)*Power(t1,4)*t2 - 
         6*Power(t1,5)*t2 - s2*Power(t1,5)*t2 - 
         10*Power(s2,2)*Power(t1,5)*t2 - Power(t1,6)*t2 + 
         2*s2*Power(t1,6)*t2 + 32*Power(t2,2) + 106*s2*Power(t2,2) + 
         34*Power(s2,2)*Power(t2,2) - 56*Power(s2,3)*Power(t2,2) - 
         8*Power(s2,4)*Power(t2,2) + Power(s2,6)*Power(t2,2) + 
         Power(s2,7)*Power(t2,2) - 126*t1*Power(t2,2) + 
         112*s2*t1*Power(t2,2) + 141*Power(s2,2)*t1*Power(t2,2) + 
         4*Power(s2,3)*t1*Power(t2,2) - 6*Power(s2,4)*t1*Power(t2,2) - 
         3*Power(s2,5)*t1*Power(t2,2) - 5*Power(s2,6)*t1*Power(t2,2) - 
         134*Power(t1,2)*Power(t2,2) - 114*s2*Power(t1,2)*Power(t2,2) + 
         32*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         17*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         2*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         10*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         29*Power(t1,3)*Power(t2,2) - 44*s2*Power(t1,3)*Power(t2,2) - 
         15*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         2*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         10*Power(s2,4)*Power(t1,3)*Power(t2,2) + 
         16*Power(t1,4)*Power(t2,2) + 3*s2*Power(t1,4)*Power(t2,2) - 
         3*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         5*Power(s2,3)*Power(t1,4)*Power(t2,2) + 
         Power(t1,5)*Power(t2,2) + s2*Power(t1,5)*Power(t2,2) - 
         Power(s2,2)*Power(t1,5)*Power(t2,2) + 12*Power(t2,3) - 
         76*s2*Power(t2,3) - 58*Power(s2,2)*Power(t2,3) + 
         Power(s2,3)*Power(t2,3) + 9*Power(s2,4)*Power(t2,3) + 
         Power(s2,5)*Power(t2,3) + 116*t1*Power(t2,3) + 
         38*s2*t1*Power(t2,3) - 36*Power(s2,2)*t1*Power(t2,3) - 
         22*Power(s2,3)*t1*Power(t2,3) - Power(s2,4)*t1*Power(t2,3) + 
         8*Power(t1,2)*Power(t2,3) + 65*s2*Power(t1,2)*Power(t2,3) + 
         18*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         3*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         30*Power(t1,3)*Power(t2,3) - 6*s2*Power(t1,3)*Power(t2,3) + 
         5*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         Power(t1,4)*Power(t2,3) - 2*s2*Power(t1,4)*Power(t2,3) - 
         8*Power(t2,4) - s2*Power(t2,4) + 15*Power(s2,2)*Power(t2,4) + 
         13*Power(s2,3)*Power(t2,4) - 2*Power(s2,4)*Power(t2,4) - 
         Power(s2,5)*Power(t2,4) - 19*t1*Power(t2,4) - 
         25*s2*t1*Power(t2,4) - 24*Power(s2,2)*t1*Power(t2,4) + 
         3*Power(s2,3)*t1*Power(t2,4) + 3*Power(s2,4)*t1*Power(t2,4) + 
         14*Power(t1,2)*Power(t2,4) + 11*s2*Power(t1,2)*Power(t2,4) - 
         3*Power(s2,3)*Power(t1,2)*Power(t2,4) - 
         s2*Power(t1,3)*Power(t2,4) + 
         Power(s2,2)*Power(t1,3)*Power(t2,4) - 2*Power(t2,5) + 
         4*s2*Power(t2,5) + 2*Power(s2,2)*Power(t2,5) - 
         2*t1*Power(t2,5) - 2*s2*t1*Power(t2,5) - 
         2*Power(s1,4)*Power(s2 - t1,2)*
          (3*Power(s2,2) + 3*Power(t1,2) + 2*(-1 + t2) - t1*(3 + t2) + 
            s2*(3 - 6*t1 + t2)) - 
         Power(s1,3)*(s2 - t1)*
          (Power(t1,5) + Power(t1,4)*(5 - 2*t2) + 8*Power(-1 + t2,2) + 
            Power(s2,4)*(6 + t1 + t2) + 
            2*t1*(13 - 14*t2 + Power(t2,2)) + 
            Power(t1,3)*(-15 - 2*t2 + Power(t2,2)) + 
            Power(t1,2)*(-45 + 36*t2 + Power(t2,2)) + 
            Power(s2,3)*(15 - 4*Power(t1,2) + Power(t2,2) - 
               t1*(23 + t2)) + 
            Power(s2,2)*(-35 + 6*Power(t1,3) - 
               3*Power(t1,2)*(-11 + t2) + 20*t2 + 7*Power(t2,2) - 
               t1*(45 + 2*t2 + Power(t2,2))) + 
            s2*(-4*Power(t1,4) + Power(t1,3)*(-21 + 5*t2) + 
               Power(t1,2)*(45 + 4*t2 - Power(t2,2)) - 
               8*t1*(-10 + 7*t2 + Power(t2,2)) + 
               2*(-11 + 10*t2 + Power(t2,2)))) + 
         Power(s,5)*(-7*Power(t1,4) + 
            2*Power(t1,3)*(3 + 2*s1 + 7*s2 + 10*t2) - 
            Power(t1,2)*(-6 + 9*s2 + 7*Power(s2,2) + 24*t2 + 38*s2*t2 + 
               18*Power(t2,2) + 5*s1*(-3 + s2 + 2*t2)) + 
            t2*(-1 + 8*t2 - 12*Power(t2,2) + Power(t2,3) - 
               Power(s2,2)*(7 + 11*t2) + 
               s2*(8 - 7*t2 - 10*Power(t2,2)) + 
               s1*(-3 + 3*Power(s2,2) + 9*t2 + s2*t2 - 2*Power(t2,2))) + 
            t1*(-3 - 14*t2 + 30*Power(t2,2) + 4*Power(t2,3) + 
               3*Power(s2,2)*(1 + 6*t2) + 2*s2*t2*(8 + 17*t2) + 
               s1*(7 + Power(s2,2) + 4*s2*(-2 + t2) - 24*t2 + 
                  8*Power(t2,2)))) - 
         Power(s1,2)*(Power(s2,7) - 6*Power(t1,6) + 
            10*t1*(-3 + t2)*Power(-1 + t2,2) + 4*Power(-1 + t2,3) + 
            19*Power(t1,5)*t2 + Power(s2,6)*(2 - 6*t1 + 5*t2) + 
            Power(t1,4)*(46 + t2 - 17*Power(t2,2)) + 
            4*Power(t1,2)*(11 - 26*t2 + 14*Power(t2,2) + Power(t2,3)) + 
            2*Power(t1,3)*(13 - 52*t2 + 9*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s2,5)*(3 + 15*Power(t1,2) + 7*t2 + 3*Power(t2,2) - 
               t1*(5 + 24*t2)) - 
            Power(s2,4)*(23 + 20*Power(t1,3) + 
               Power(t1,2)*(6 - 46*t2) - 20*t2 - 18*Power(t2,2) + 
               Power(t2,3) + t1*(7 + 7*t2 + 11*Power(t2,2))) + 
            Power(s2,3)*(3 + 15*Power(t1,4) + 
               Power(t1,3)*(34 - 44*t2) + 47*t2 + 9*Power(t2,2) - 
               3*Power(t2,3) + 
               Power(t1,2)*(3 - 40*t2 + 15*Power(t2,2)) + 
               t1*(15 - 61*t2 - 29*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s2,2)*(-6*Power(t1,5) + Power(t1,4)*(-46 + 21*t2) + 
               Power(t1,3)*(3 + 92*t2 - 9*Power(t2,2)) + 
               Power(t1,2)*(85 + 63*t2 - 13*Power(t2,2) - 
                  3*Power(t2,3)) + 
               4*(7 - 17*t2 + 8*Power(t2,2) + 2*Power(t2,3)) + 
               2*t1*(16 - 111*t2 + 6*Power(t2,2) + 5*Power(t2,3))) + 
            s2*(Power(t1,6) + Power(t1,5)*(27 - 4*t2) - 
               2*Power(-1 + t2,2)*(-13 + 3*t2) + 
               Power(t1,4)*(-2 - 71*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(-123 - 23*t2 + 41*Power(t2,2) + 
                  Power(t2,3)) - 
               4*t1*(18 - 43*t2 + 22*Power(t2,2) + 3*Power(t2,3)) - 
               Power(t1,2)*(61 - 279*t2 + 39*Power(t2,2) + 
                  11*Power(t2,3)))) + 
         s1*(6*Power(t1,6) + Power(t1,7) - 2*Power(s2,7)*t2 + 
            2*Power(-1 + t2,3)*(-5 + 3*t2) - 
            Power(t1,5)*(-7 + 28*t2 + Power(t2,2)) + 
            t1*Power(-1 + t2,2)*(19 + 44*t2 + 5*Power(t2,2)) + 
            Power(t1,4)*(-38 - 6*t2 + 48*Power(t2,2)) - 
            Power(t1,3)*(91 - 157*t2 + 13*Power(t2,2) + 
               29*Power(t2,3)) + 
            Power(t1,2)*(22 + 141*t2 - 207*Power(t2,2) + 
               39*Power(t2,3) + 5*Power(t2,4)) + 
            Power(s2,6)*(2 + t2 + Power(t2,2) + t1*(2 + 11*t2)) + 
            Power(s2,5)*(4 + 14*t2 + 9*Power(t2,2) + 3*Power(t2,3) - 
               Power(t1,2)*(11 + 25*t2) - t1*(13 + 9*t2 + 4*Power(t2,2))\
) + Power(s2,4)*(6 - 30*t2 + 24*Power(t2,2) + 20*Power(t2,3) + 
               5*Power(t1,3)*(5 + 6*t2) + 
               Power(t1,2)*(38 + 26*t2 + 6*Power(t2,2)) - 
               2*t1*(7 + 40*t2 + 19*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s2,3)*(-28 + 25*t2 - 35*Power(t2,2) + 
               11*Power(t2,3) + 3*Power(t2,4) - 
               10*Power(t1,4)*(3 + 2*t2) - 
               2*Power(t1,3)*(31 + 17*t2 + 2*Power(t2,2)) + 
               t1*(33 + 81*t2 - 129*Power(t2,2) - 49*Power(t2,3)) + 
               Power(t1,2)*(11 + 184*t2 + 61*Power(t2,2) + 
                  12*Power(t2,3))) + 
            Power(s2,2)*(51 + 47*t2 - 103*Power(t2,2) - 3*Power(t2,3) + 
               8*Power(t2,4) + Power(t1,5)*(20 + 7*t2) + 
               Power(t1,4)*(58 + 21*t2 + Power(t2,2)) - 
               Power(t1,3)*(-11 + 212*t2 + 45*Power(t2,2) + 
                  6*Power(t2,3)) + 
               2*Power(t1,2)*
                (-61 - 39*t2 + 117*Power(t2,2) + 19*Power(t2,3)) + 
               t1*(-39 + 123*t2 + 37*Power(t2,2) - 43*Power(t2,3) - 
                  6*Power(t2,4))) - 
            s2*(Power(t1,6)*(7 + t2) + Power(t1,5)*(29 + 5*t2) + 
               Power(-1 + t2,2)*(25 + 40*t2 + 3*Power(t2,2)) - 
               Power(t1,4)*(-19 + 122*t2 + 14*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,3)*(-121 - 33*t2 + 177*Power(t2,2) + 
                  9*Power(t2,3)) - 
               Power(t1,2)*(158 - 305*t2 + 11*Power(t2,2) + 
                  61*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(69 + 200*t2 - 322*Power(t2,2) + 40*Power(t2,3) + 
                  13*Power(t2,4)))) - 
         Power(s,4)*(-3 - 13*t1 + Power(t1,2) - 2*Power(t1,3) - 
            28*Power(t1,4) + 9*Power(t1,5) + 15*t2 + 21*t1*t2 + 
            5*Power(t1,2)*t2 + 83*Power(t1,3)*t2 - 24*Power(t1,4)*t2 - 
            2*Power(t2,2) + 8*t1*Power(t2,2) - 
            77*Power(t1,2)*Power(t2,2) + 18*Power(t1,3)*Power(t2,2) - 
            11*Power(t2,3) + 17*t1*Power(t2,3) + 5*Power(t2,4) - 
            3*t1*Power(t2,4) + 
            Power(s2,3)*(-9*Power(t1,2) - 25*t2*(1 + t2) + 
               8*t1*(1 + 4*t2)) + 
            Power(s1,2)*(-2 + Power(s2,3) + 3*Power(t1,3) + 
               Power(t1,2)*(26 - 7*t2) + 5*Power(s2,2)*t2 + 
               7*Power(t2,2) - Power(t2,3) + 
               s2*(1 - 4*Power(t1,2) + t1*(-21 + t2) - 9*t2 + 
                  3*Power(t2,2)) + t1*(25 - 33*t2 + 5*Power(t2,2))) + 
            Power(s2,2)*(1 + 27*Power(t1,3) + 8*t2 - 40*Power(t2,2) - 
               20*Power(t2,3) - 44*Power(t1,2)*(1 + 2*t2) + 
               t1*(8 + 108*t2 + 81*Power(t2,2))) + 
            s2*(2 - 27*Power(t1,4) - 2*t2 - 3*Power(t2,2) - 
               40*Power(t2,3) + 5*Power(t2,4) + 
               16*Power(t1,3)*(4 + 5*t2) - 
               2*Power(t1,2)*(3 + 83*t2 + 37*Power(t2,2)) + 
               t1*(1 - 33*t2 + 142*Power(t2,2) + 16*Power(t2,3))) + 
            s1*(7 - 13*Power(t1,4) - 36*t2 + 40*Power(t2,2) - 
               19*Power(t2,3) + Power(s2,3)*(-3 + 3*t1 + 14*t2) + 
               Power(t1,3)*(-19 + 31*t2) + 
               Power(t1,2)*(24 - 7*t2 - 23*Power(t2,2)) + 
               t1*(9 - 82*t2 + 45*Power(t2,2) + 5*Power(t2,3)) - 
               Power(s2,2)*(-5 + 19*Power(t1,2) + 12*t2 - 
                  3*Power(t2,2) + t1*(9 + 2*t2)) + 
               s2*(-9 + 29*Power(t1,3) + Power(t1,2)*(31 - 43*t2) + 
                  42*t2 + 17*Power(t2,2) - 11*Power(t2,3) + 
                  t1*(-11 - 12*t2 + 25*Power(t2,2))))) + 
         Power(s,3)*(4 - 25*t1 - 40*Power(t1,2) - 22*Power(t1,3) - 
            22*Power(t1,4) + 32*Power(t1,5) - 5*Power(t1,6) + 56*t2 + 
            139*t1*t2 + 75*Power(t1,2)*t2 + 38*Power(t1,3)*t2 - 
            81*Power(t1,4)*t2 + 12*Power(t1,5)*t2 - 107*Power(t2,2) - 
            141*t1*Power(t2,2) - 35*Power(t1,2)*Power(t2,2) + 
            55*Power(t1,3)*Power(t2,2) - 6*Power(t1,4)*Power(t2,2) + 
            48*Power(t2,3) + 16*t1*Power(t2,3) + 
            5*Power(t1,2)*Power(t2,3) - 4*Power(t1,3)*Power(t2,3) + 
            3*Power(t2,4) - 11*t1*Power(t2,4) + 
            3*Power(t1,2)*Power(t2,4) + 
            Power(s2,4)*(-5*Power(t1,2) + 7*t1*(1 + 4*t2) - 
               3*t2*(11 + 10*t2)) + 
            Power(s1,3)*(Power(t1,3) + Power(t1,2)*(21 - 2*t2) + 
               Power(s2,2)*(8 + t1 + t2) + 
               s2*(-2*Power(t1,2) + t1*(-29 + t2) + (-3 + t2)*t2) + 
               t1*(20 - 19*t2 + Power(t2,2)) + 2*(-2 + t2 + Power(t2,2))\
) + Power(s2,3)*(14 + 20*Power(t1,3) - 16*t2 - 59*Power(t2,2) - 
               20*Power(t2,3) - Power(t1,2)*(53 + 96*t2) + 
               t1*(15 + 185*t2 + 104*Power(t2,2))) + 
            Power(s2,2)*(-30*Power(t1,4) + 3*Power(t1,3)*(39 + 40*t2) - 
               4*Power(t1,2)*(13 + 88*t2 + 31*Power(t2,2)) + 
               t2*(54 - 25*t2 - 49*Power(t2,2) + 10*Power(t2,3)) + 
               t1*(-27 + 16*t2 + 228*Power(t2,2) + 24*Power(t2,3))) + 
            s2*(-22 + 20*Power(t1,5) - 57*t2 + 82*Power(t2,2) - 
               6*Power(t2,3) + 17*Power(t2,4) - 
               Power(t1,4)*(103 + 64*t2) + 
               Power(t1,2)*(35 - 38*t2 - 224*Power(t2,2)) + 
               Power(t1,3)*(59 + 281*t2 + 56*Power(t2,2)) + 
               t1*(34 - 118*t2 + 73*Power(t2,2) + 29*Power(t2,3) - 
                  12*Power(t2,4))) + 
            Power(s1,2)*(34 + 4*Power(s2,4) - 9*Power(t1,4) - 70*t2 + 
               3*Power(t1,2)*(14 - 5*t2)*t2 + 40*Power(t2,2) + 
               3*Power(t1,3)*(-17 + 7*t2) + 
               Power(s2,3)*(2 - 6*t1 + 20*t2) + 
               t1*(76 - 138*t2 + 9*Power(t2,2) + 3*Power(t2,3)) - 
               Power(s2,2)*(-18 + 9*Power(t1,2) + 17*t2 - 
                  12*Power(t2,2) + 7*t1*(7 + 3*t2)) + 
               s2*(-70 + 20*Power(t1,3) + Power(t1,2)*(98 - 20*t2) + 
                  71*t2 + 45*Power(t2,2) - 4*Power(t2,3) + 
                  t1*(-1 - 39*t2 + 4*Power(t2,2)))) + 
            s1*(-28 + 15*Power(t1,5) - 3*t2 + 77*Power(t2,2) - 
               51*Power(t2,3) - 3*Power(t2,4) + 
               Power(s2,4)*(-8 + 3*t1 + 26*t2) - 
               Power(t1,4)*(17 + 33*t2) + 
               Power(t1,3)*(-55 + 123*t2 + 21*Power(t2,2)) - 
               Power(t1,2)*(8 - 98*t2 + 154*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(-65 + 44*t2 + 40*Power(t2,2) + 51*Power(t2,3)) - 
               Power(s2,3)*(10 + 24*Power(t1,2) + 34*t2 - 
                  2*Power(t2,2) + t1*(-21 + 41*t2)) + 
               Power(s2,2)*(-20 + 54*Power(t1,3) + 28*t2 - 
                  9*Power(t2,2) - 24*Power(t2,3) - 
                  Power(t1,2)*(35 + 29*t2) + 
                  t1*(-9 + 135*t2 + 31*Power(t2,2))) + 
               s2*(78 - 48*Power(t1,4) - 25*t2 - 31*Power(t2,2) - 
                  76*Power(t2,3) + Power(t1,3)*(39 + 77*t2) + 
                  Power(t1,2)*(74 - 224*t2 - 54*Power(t2,2)) + 
                  t1*(34 - 172*t2 + 197*Power(t2,2) + 25*Power(t2,3))))) \
+ Power(s,2)*(-14 + 77*t1 + 63*Power(t1,2) - 38*Power(t1,3) - 
            27*Power(t1,4) - 30*Power(t1,5) + 14*Power(t1,6) - 
            Power(t1,7) - 78*t2 - 186*t1*t2 + 58*Power(t1,2)*t2 + 
            125*Power(t1,3)*t2 + 48*Power(t1,4)*t2 - 29*Power(t1,5)*t2 + 
            2*Power(t1,6)*t2 + 198*Power(t2,2) + 96*t1*Power(t2,2) - 
            172*Power(t1,2)*Power(t2,2) - 39*Power(t1,3)*Power(t2,2) + 
            9*Power(t1,4)*Power(t2,2) - 128*Power(t2,3) + 
            14*t1*Power(t2,3) + 24*Power(t1,2)*Power(t2,3) + 
            13*Power(t1,3)*Power(t2,3) - 2*Power(t1,4)*Power(t2,3) + 
            20*Power(t2,4) - 5*t1*Power(t2,4) - 
            7*Power(t1,2)*Power(t2,4) + Power(t1,3)*Power(t2,4) + 
            2*Power(t2,5) - 2*Power(s1,4)*(s2 - t1)*
             (-1 + 3*s2 - 3*t1 + t2) + 
            Power(s2,5)*(Power(t1,2) - 2*t1*(1 + 6*t2) + 
               t2*(19 + 20*t2)) + 
            Power(s2,4)*(-15 - 5*Power(t1,3) + 26*t2 + 37*Power(t2,2) + 
               10*Power(t2,3) + Power(t1,2)*(22 + 50*t2) - 
               t1*(11 + 132*t2 + 76*Power(t2,2))) + 
            Power(s2,3)*(-20 + 10*Power(t1,4) - 64*t2 + 9*Power(t2,2) + 
               27*Power(t2,3) - 10*Power(t2,4) - 
               4*Power(t1,3)*(17 + 20*t2) + 
               Power(t1,2)*(63 + 311*t2 + 108*Power(t2,2)) - 
               t1*(-53 + 90*t2 + 153*Power(t2,2) + 16*Power(t2,3))) - 
            Power(s2,2)*(-46 + 10*Power(t1,5) - 34*t2 + 
               101*Power(t2,2) + 12*Power(t2,3) + 21*Power(t2,4) - 
               4*Power(t1,4)*(23 + 15*t2) + 
               Power(t1,3)*(123 + 331*t2 + 68*Power(t2,2)) - 
               2*Power(t1,2)*(-44 + 75*t2 + 102*Power(t2,2)) - 
               2*t1*(-7 + 142*t2 - 51*Power(t2,2) - 4*Power(t2,3) + 
                  9*Power(t2,4))) + 
            s2*(11 + 5*Power(t1,6) + 34*t2 - 2*Power(t2,2) - 
               46*Power(t2,3) + 7*Power(t2,4) - 
               2*Power(t1,5)*(29 + 10*t2) + 
               Power(t1,4)*(101 + 162*t2 + 16*Power(t2,2)) + 
               Power(t1,3)*(77 - 134*t2 - 97*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(72 - 345*t2 + 132*Power(t2,2) - 
                  32*Power(t2,3) - 9*Power(t2,4)) + 
               t1*(-102 - 126*t2 + 305*Power(t2,2) - 14*Power(t2,3) + 
                  25*Power(t2,4))) + 
            Power(s1,3)*(6 + 3*Power(t1,4) + Power(t1,3)*(47 - 6*t2) - 
               6*Power(t2,2) - Power(s2,3)*(22 + 3*t1 + 3*t2) + 
               Power(s2,2)*(-19 + 91*t1 + 9*Power(t1,2) + 6*t2 - 
                  3*Power(t2,2)) + 
               Power(t1,2)*(1 - 40*t2 + 3*Power(t2,2)) + 
               t1*(-73 + 72*t2 + 5*Power(t2,2)) + 
               s2*(39 - 9*Power(t1,3) - 32*t2 - 11*Power(t2,2) + 
                  Power(t1,2)*(-116 + 9*t2) + 2*t1*(9 + 17*t2))) - 
            Power(s1,2)*(61 + 6*Power(s2,5) + 9*Power(t1,5) - 113*t2 + 
               55*Power(t2,2) - 3*Power(t2,3) - 
               3*Power(t1,4)*(-6 + 7*t2) + 
               Power(s2,4)*(6 - 18*t1 + 30*t2) + 
               Power(t1,3)*(-45 + 34*t2 + 15*Power(t2,2)) + 
               Power(s2,3)*(30 - 40*t1 + 9*Power(t1,2) - 69*t1*t2 + 
                  18*Power(t2,2)) + 
               2*t1*(-40 + 35*t2 - 9*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,2)*(123 - 210*t2 + 56*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,2)*(-172 + 21*Power(t1,3) + 139*t2 + 
                  87*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t1,2)*(80 + 27*t2) - 
                  2*t1*(39 - 4*t2 + 9*Power(t2,2))) - 
               s2*(-57 + 27*Power(t1,4) + Power(t1,3)*(64 - 33*t2) + 
                  91*t2 - 61*Power(t2,2) + 3*Power(t2,3) + 
                  3*Power(t1,2)*(-31 + 14*t2 + 5*Power(t2,2)) + 
                  t1*(-311 + 373*t2 + 23*Power(t2,2) - 9*Power(t2,3)))) \
+ s1*(82 + 7*Power(t1,6) - 132*t2 + 42*Power(t2,2) + 8*Power(t2,3) - 
               Power(t1,5)*(30 + 13*t2) - 
               Power(s2,5)*(-7 + t1 + 24*t2) + 
               Power(t1,4)*(-21 + 129*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(74 - 34*t2 - 138*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,2)*(24 - 179*t2 + 182*Power(t2,2) + 
                  45*Power(t2,3)) + 
               t1*(36 - 229*t2 + 274*Power(t2,2) - 99*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,4)*(32 + 11*Power(t1,2) + 33*t2 + 
                  2*Power(t2,2) + t1*(-33 + 71*t2)) + 
               Power(s2,3)*(20 - 34*Power(t1,3) + 
                  Power(t1,2)*(87 - 56*t2) + 73*t2 + 42*Power(t2,2) + 
                  26*Power(t2,3) - t1*(78 + 213*t2 + 23*Power(t2,2))) + 
               s2*(-48 - 29*Power(t1,5) + 152*t2 - 173*Power(t2,2) + 
                  84*Power(t2,3) + 9*Power(t2,4) + 
                  2*Power(t1,4)*(51 + 20*t2) + 
                  Power(t1,3)*(28 - 405*t2 - 29*Power(t2,2)) + 
                  t1*(98 + 269*t2 - 382*Power(t2,2) - 
                     149*Power(t2,3)) + 
                  3*Power(t1,2)*
                   (-41 + 23*t2 + 129*Power(t2,2) + 6*Power(t2,3))) + 
               Power(s2,2)*(-113 + 46*Power(t1,4) - 77*t2 + 
                  167*Power(t2,2) + 115*Power(t2,3) - 
                  Power(t1,3)*(133 + 18*t2) + 
                  Power(t1,2)*(39 + 456*t2 + 45*Power(t2,2)) - 
                  t1*(-29 + 108*t2 + 291*Power(t2,2) + 45*Power(t2,3))))) \
+ s*(-17 - 106*t1 + 12*Power(t1,2) + 101*Power(t1,3) + 30*Power(t1,4) - 
            Power(t1,5) - 13*Power(t1,6) + 2*Power(t1,7) + 78*t2 + 
            94*t1*t2 - 244*Power(t1,2)*t2 - 155*Power(t1,3)*t2 + 
            34*Power(t1,4)*t2 + 18*Power(t1,5)*t2 - 3*Power(t1,6)*t2 + 
            Power(s2,6)*(-4 + 2*t1 - 7*t2)*t2 - 120*Power(t2,2) + 
            138*t1*Power(t2,2) + 284*Power(t1,2)*Power(t2,2) - 
            33*Power(t1,3)*Power(t2,2) - 19*Power(t1,4)*Power(t2,2) - 
            Power(t1,5)*Power(t2,2) + 74*Power(t2,3) - 
            152*t1*Power(t2,3) - 44*Power(t1,2)*Power(t2,3) + 
            20*Power(t1,3)*Power(t2,3) + 3*Power(t1,4)*Power(t2,3) - 
            15*Power(t2,4) + 24*t1*Power(t2,4) - 
            8*Power(t1,2)*Power(t2,4) - Power(t1,3)*Power(t2,4) + 
            2*t1*Power(t2,5) + 
            4*Power(s1,4)*(s2 - t1)*
             (-1 + 3*Power(s2,2) + 3*Power(t1,2) + t2 - t1*(1 + t2) + 
               s2*(1 - 6*t1 + t2)) + 
            Power(s2,5)*(-2*Power(t1,2)*(1 + 5*t2) + 
               t1*(4 + 37*t2 + 30*Power(t2,2)) - 
               2*(-1 + 6*t2 + 5*Power(t2,2) + Power(t2,3))) + 
            Power(s2,4)*(15 + 14*t2 + 5*Power(t2,2) - 7*Power(t2,3) + 
               5*Power(t2,4) + 10*Power(t1,3)*(1 + 2*t2) - 
               Power(t1,2)*(29 + 111*t2 + 50*Power(t2,2)) + 
               4*t1*(-2 + 17*t2 + 10*Power(t2,2) + Power(t2,3))) + 
            Power(s2,2)*(-54 - 53*t2 + 109*Power(t2,2) + 
               21*Power(t2,3) - 23*Power(t2,4) + 
               10*Power(t1,5)*(2 + t2) - 
               Power(t1,4)*(94 + 106*t2 + 15*Power(t2,2)) + 
               Power(t1,3)*(-11 + 162*t2 + 37*Power(t2,2) - 
                  4*Power(t2,3)) + 
               t1*(97 - 185*t2 - 119*Power(t2,2) + 20*Power(t2,3) - 
                  17*Power(t2,4)) + 
               Power(t1,2)*(107 + 214*t2 - 120*Power(t2,2) + 
                  30*Power(t2,3) + 9*Power(t2,4))) + 
            Power(s2,3)*(5 + 13*t2 + 41*Power(t2,2) - 2*Power(t2,3) + 
               11*Power(t2,4) - 20*Power(t1,4)*(1 + t2) + 
               Power(t1,2)*(13 - 150*t2 - 59*Power(t2,2)) + 
               2*Power(t1,3)*(38 + 77*t2 + 20*Power(t2,2)) - 
               t1*(61 + 111*t2 - 43*Power(t2,2) + 3*Power(t2,3) + 
                  12*Power(t2,4))) + 
            s2*(49 + 14*t2 - 182*Power(t2,2) + 142*Power(t2,3) - 
               19*Power(t2,4) - 4*Power(t2,5) - 2*Power(t1,6)*(5 + t2) + 
               Power(t1,5)*(56 + 33*t2 + 2*Power(t2,2)) + 
               Power(t1,4)*(5 - 86*t2 - 7*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,3)*(91 + 151*t2 - 91*Power(t2,2) + 
                  23*Power(t2,3) + 2*Power(t2,4)) + 
               Power(t1,2)*(-203 + 327*t2 + 111*Power(t2,2) - 
                  38*Power(t2,3) + 7*Power(t2,4)) + 
               t1*(40 + 317*t2 - 429*Power(t2,2) + 43*Power(t2,3) + 
                  29*Power(t2,4))) + 
            Power(s1,3)*(3*Power(t1,5) + Power(t1,4)*(31 - 6*t2) + 
               4*Power(-1 + t2,2) + Power(s2,4)*(20 + 3*t1 + 3*t2) - 
               4*t1*(-7 + 6*t2 + Power(t2,2)) + 
               2*Power(t1,2)*(-57 + 53*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(-34 - 23*t2 + 3*Power(t2,2)) - 
               Power(s2,3)*(-34 + 12*Power(t1,2) + 3*t2 - 
                  3*Power(t2,2) + t1*(91 + 3*t2)) + 
               Power(s2,2)*(-70 + 18*Power(t1,3) - 
                  9*Power(t1,2)*(-17 + t2) + 50*t2 + 16*Power(t2,2) - 
                  t1*(102 + 17*t2 + 3*Power(t2,2))) + 
               s2*(-12*Power(t1,4) + Power(t1,3)*(-113 + 15*t2) + 
                  Power(t1,2)*(102 + 43*t2 - 3*Power(t2,2)) + 
                  8*(-3 + 2*t2 + Power(t2,2)) - 
                  4*t1*(-46 + 39*t2 + 5*Power(t2,2)))) + 
            Power(s1,2)*(4*Power(s2,6) - 3*Power(t1,6) + 
               20*Power(-1 + t2,2) + Power(t1,5)*(13 + 7*t2) + 
               Power(s2,5)*(6 - 18*t1 + 20*t2) + 
               Power(t1,4)*(20 - 62*t2 - 5*Power(t2,2)) + 
               Power(t1,2)*(2 + 118*t2 - 40*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,3)*(1 - 73*t2 + 57*Power(t2,2) + Power(t2,3)) - 
               t1*(127 - 261*t2 + 133*Power(t2,2) + Power(t2,3)) + 
               Power(s2,4)*(16 + 29*Power(t1,2) + 15*t2 + 
                  12*Power(t2,2) - t1*(17 + 71*t2)) + 
               Power(s2,3)*(-129 - 16*Power(t1,3) + 88*t2 + 
                  67*Power(t2,2) - 4*Power(t2,3) + 
                  Power(t1,2)*(2 + 86*t2) + 
                  t1*(-53 + 7*t2 - 28*Power(t2,2))) + 
               s2*(99 + 10*Power(t1,5) - 201*t2 + 97*Power(t2,2) + 
                  5*Power(t2,3) - 10*Power(t1,4)*(4 + t2) + 
                  Power(t1,3)*(-61 + 161*t2 + 6*Power(t2,2)) - 
                  Power(t1,2)*
                   (139 - 258*t2 + 63*Power(t2,2) + 6*Power(t2,3)) + 
                  2*t1*(-5 - 85*t2 + 11*Power(t2,2) + 7*Power(t2,3))) + 
               Power(s2,2)*(20 - 6*Power(t1,4) + 28*t2 + 
                  30*Power(t2,2) - 6*Power(t2,3) - 
                  4*Power(t1,3)*(-9 + 8*t2) + 
                  Power(t1,2)*(78 - 121*t2 + 15*Power(t2,2)) + 
                  t1*(267 - 273*t2 - 61*Power(t2,2) + 9*Power(t2,3)))) + 
            s1*(Power(t1,7) - Power(t1,6)*(8 + t2) + 
               Power(s2,6)*(-2 + 11*t2) + 
               Power(t1,5)*(9 + 37*t2 - Power(t2,2)) - 
               Power(-1 + t2,2)*(41 - 48*t2 + 3*Power(t2,2)) + 
               Power(t1,4)*(76 - 75*t2 - 39*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(38 - 173*t2 + 150*Power(t2,2) + 
                  13*Power(t2,3)) - 
               Power(t1,2)*(55 + 49*t2 - 148*Power(t2,2) + 
                  77*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(28 + 85*t2 - 193*Power(t2,2) + 75*Power(t2,3) + 
                  5*Power(t2,4)) - 
               Power(s2,5)*(19 + Power(t1,2) + 12*t2 + 3*Power(t2,2) + 
                  t1*(-9 + 47*t2)) + 
               Power(s2,4)*(-11 + 5*Power(t1,3) - 70*t2 - 
                  34*Power(t2,2) - 14*Power(t2,3) + 
                  Power(t1,2)*(-24 + 77*t2) + 
                  t1*(77 + 99*t2 + 13*Power(t2,2))) - 
               Power(s2,3)*(-38 + 10*Power(t1,4) - 96*t2 + 
                  120*Power(t2,2) + 78*Power(t2,3) + 
                  Power(t1,3)*(-46 + 58*t2) + 
                  2*Power(t1,2)*(63 + 131*t2 + 10*Power(t2,2)) - 
                  t1*(-43 + 255*t2 + 177*Power(t2,2) + 35*Power(t2,3))) + 
               Power(s2,2)*(60 + 10*Power(t1,5) - 162*t2 + 
                  119*Power(t2,2) - 44*Power(t2,3) - 9*Power(t2,4) + 
                  Power(t1,4)*(-54 + 17*t2) + 
                  2*Power(t1,3)*(53 + 156*t2 + 6*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (-65 + 125*t2 + 97*Power(t2,2) + 9*Power(t2,3)) + 
                  3*t1*(-16 - 125*t2 + 144*Power(t2,2) + 49*Power(t2,3))) \
- s2*(37 + 5*Power(t1,6) + 19*t2 - 85*Power(t2,2) + 21*Power(t2,3) + 
                  8*Power(t2,4) - Power(t1,5)*(33 + t2) + 
                  Power(t1,4)*(47 + 174*t2 + Power(t2,2)) - 
                  Power(t1,3)*
                   (-217 + 265*t2 + 187*Power(t2,2) + 5*Power(t2,3)) + 
                  Power(t1,2)*
                   (28 - 452*t2 + 462*Power(t2,2) + 82*Power(t2,3)) + 
                  t1*(13 - 219*t2 + 259*Power(t2,2) - 113*Power(t2,3) - 
                     12*Power(t2,4))))))*
       B1(1 - s2 + t1 - t2,1 - s + s2 - t1,1 - s + s1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(s - s1 + t2)*(-1 + s2 - t1 + t2)*
       Power(-1 + s - s*s1 + s1*s2 - s1*t1 + t2,2)) + 
    (16*(20*Power(s2,3) + 3*Power(s2,4) - 27*Power(s2,5) - 
         4*Power(s2,6) + 3*Power(s2,7) - 2*Power(s2,8) - Power(s2,9) - 
         60*Power(s2,2)*t1 - 36*Power(s2,3)*t1 + 133*Power(s2,4)*t1 + 
         61*Power(s2,5)*t1 - 13*Power(s2,6)*t1 + 14*Power(s2,7)*t1 + 
         8*Power(s2,8)*t1 + 60*s2*Power(t1,2) + 
         90*Power(s2,2)*Power(t1,2) - 258*Power(s2,3)*Power(t1,2) - 
         242*Power(s2,4)*Power(t1,2) + 7*Power(s2,5)*Power(t1,2) - 
         48*Power(s2,6)*Power(t1,2) - 29*Power(s2,7)*Power(t1,2) - 
         20*Power(t1,3) - 84*s2*Power(t1,3) + 
         246*Power(s2,2)*Power(t1,3) + 438*Power(s2,3)*Power(t1,3) + 
         55*Power(s2,4)*Power(t1,3) + 106*Power(s2,5)*Power(t1,3) + 
         63*Power(s2,6)*Power(t1,3) + 27*Power(t1,4) - 
         115*s2*Power(t1,4) - 412*Power(s2,2)*Power(t1,4) - 
         135*Power(s2,3)*Power(t1,4) - 160*Power(s2,4)*Power(t1,4) - 
         91*Power(s2,5)*Power(t1,4) + 21*Power(t1,5) + 
         197*s2*Power(t1,5) + 137*Power(s2,2)*Power(t1,5) + 
         162*Power(s2,3)*Power(t1,5) + 91*Power(s2,4)*Power(t1,5) - 
         38*Power(t1,6) - 67*s2*Power(t1,6) - 
         104*Power(s2,2)*Power(t1,6) - 63*Power(s2,3)*Power(t1,6) + 
         13*Power(t1,7) + 38*s2*Power(t1,7) + 
         29*Power(s2,2)*Power(t1,7) - 6*Power(t1,8) - 8*s2*Power(t1,8) + 
         Power(t1,9) + 16*Power(s2,2)*t2 - 47*Power(s2,3)*t2 - 
         56*Power(s2,4)*t2 + 16*Power(s2,5)*t2 + Power(s2,6)*t2 - 
         9*Power(s2,7)*t2 - 5*Power(s2,8)*t2 + Power(s2,9)*t2 - 
         32*s2*t1*t2 + 133*Power(s2,2)*t1*t2 + 312*Power(s2,3)*t1*t2 - 
         7*Power(s2,4)*t1*t2 - 55*Power(s2,5)*t1*t2 + 
         56*Power(s2,6)*t1*t2 + 39*Power(s2,7)*t1*t2 - 
         6*Power(s2,8)*t1*t2 + 16*Power(t1,2)*t2 - 
         125*s2*Power(t1,2)*t2 - 608*Power(s2,2)*Power(t1,2)*t2 - 
         149*Power(s2,3)*Power(t1,2)*t2 + 
         241*Power(s2,4)*Power(t1,2)*t2 - 
         146*Power(s2,5)*Power(t1,2)*t2 - 
         127*Power(s2,6)*Power(t1,2)*t2 + 14*Power(s2,7)*Power(t1,2)*t2 + 
         39*Power(t1,3)*t2 + 504*s2*Power(t1,3)*t2 + 
         329*Power(s2,2)*Power(t1,3)*t2 - 
         434*Power(s2,3)*Power(t1,3)*t2 + 
         205*Power(s2,4)*Power(t1,3)*t2 + 
         223*Power(s2,5)*Power(t1,3)*t2 - 14*Power(s2,6)*Power(t1,3)*t2 - 
         152*Power(t1,4)*t2 - 263*s2*Power(t1,4)*t2 + 
         391*Power(s2,2)*Power(t1,4)*t2 - 
         165*Power(s2,3)*Power(t1,4)*t2 - 
         225*Power(s2,4)*Power(t1,4)*t2 + 74*Power(t1,5)*t2 - 
         175*s2*Power(t1,5)*t2 + 74*Power(s2,2)*Power(t1,5)*t2 + 
         125*Power(s2,3)*Power(t1,5)*t2 + 14*Power(s2,4)*Power(t1,5)*t2 + 
         31*Power(t1,6)*t2 - 16*s2*Power(t1,6)*t2 - 
         29*Power(s2,2)*Power(t1,6)*t2 - 14*Power(s2,3)*Power(t1,6)*t2 + 
         Power(t1,7)*t2 - 3*s2*Power(t1,7)*t2 + 
         6*Power(s2,2)*Power(t1,7)*t2 + 2*Power(t1,8)*t2 - 
         s2*Power(t1,8)*t2 + 12*s2*Power(t2,2) - 
         50*Power(s2,2)*Power(t2,2) - 42*Power(s2,3)*Power(t2,2) + 
         28*Power(s2,4)*Power(t2,2) + 24*Power(s2,5)*Power(t2,2) + 
         2*Power(s2,6)*Power(t2,2) - 8*Power(s2,7)*Power(t2,2) + 
         5*Power(s2,8)*Power(t2,2) - Power(s2,9)*Power(t2,2) - 
         12*t1*Power(t2,2) + 88*s2*t1*Power(t2,2) + 
         176*Power(s2,2)*t1*Power(t2,2) - 
         151*Power(s2,3)*t1*Power(t2,2) - 
         220*Power(s2,4)*t1*Power(t2,2) + 4*Power(s2,5)*t1*Power(t2,2) + 
         44*Power(s2,6)*t1*Power(t2,2) - 35*Power(s2,7)*t1*Power(t2,2) + 
         7*Power(s2,8)*t1*Power(t2,2) - 38*Power(t1,2)*Power(t2,2) - 
         226*s2*Power(t1,2)*Power(t2,2) + 
         309*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         661*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         39*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         89*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         106*Power(s2,6)*Power(t1,2)*Power(t2,2) - 
         21*Power(s2,7)*Power(t1,2)*Power(t2,2) + 
         92*Power(t1,3)*Power(t2,2) - 277*s2*Power(t1,3)*Power(t2,2) - 
         903*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         76*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         65*Power(s2,4)*Power(t1,3)*Power(t2,2) - 
         181*Power(s2,5)*Power(t1,3)*Power(t2,2) + 
         35*Power(s2,6)*Power(t1,3)*Power(t2,2) + 
         91*Power(t1,4)*Power(t2,2) + 583*s2*Power(t1,4)*Power(t2,2) - 
         64*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         30*Power(s2,3)*Power(t1,4)*Power(t2,2) + 
         190*Power(s2,4)*Power(t1,4)*Power(t2,2) - 
         35*Power(s2,5)*Power(t1,4)*Power(t2,2) - 
         145*Power(t1,5)*Power(t2,2) + 24*s2*Power(t1,5)*Power(t2,2) - 
         82*Power(s2,2)*Power(t1,5)*Power(t2,2) - 
         125*Power(s2,3)*Power(t1,5)*Power(t2,2) + 
         21*Power(s2,4)*Power(t1,5)*Power(t2,2) - 
         3*Power(t1,6)*Power(t2,2) + 51*s2*Power(t1,6)*Power(t2,2) + 
         50*Power(s2,2)*Power(t1,6)*Power(t2,2) - 
         7*Power(s2,3)*Power(t1,6)*Power(t2,2) - 
         11*Power(t1,7)*Power(t2,2) - 11*s2*Power(t1,7)*Power(t2,2) + 
         Power(s2,2)*Power(t1,7)*Power(t2,2) + Power(t1,8)*Power(t2,2) + 
         4*Power(t2,3) - 30*s2*Power(t2,3) + 9*Power(s2,2)*Power(t2,3) + 
         84*Power(s2,3)*Power(t2,3) + 66*Power(s2,4)*Power(t2,3) - 
         5*Power(s2,5)*Power(t2,3) - 5*Power(s2,6)*Power(t2,3) + 
         13*Power(s2,7)*Power(t2,3) - Power(s2,8)*Power(t2,3) + 
         26*t1*Power(t2,3) + 14*s2*t1*Power(t2,3) - 
         315*Power(s2,2)*t1*Power(t2,3) - 
         329*Power(s2,3)*t1*Power(t2,3) + 53*Power(s2,4)*t1*Power(t2,3) + 
         11*Power(s2,5)*t1*Power(t2,3) - 82*Power(s2,6)*t1*Power(t2,3) + 
         6*Power(s2,7)*t1*Power(t2,3) - 23*Power(t1,2)*Power(t2,3) + 
         380*s2*Power(t1,2)*Power(t2,3) + 
         565*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         182*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         23*Power(s2,4)*Power(t1,2)*Power(t2,3) + 
         218*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         15*Power(s2,6)*Power(t1,2)*Power(t2,3) - 
         149*Power(t1,3)*Power(t2,3) - 407*s2*Power(t1,3)*Power(t2,3) + 
         278*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         102*Power(s2,3)*Power(t1,3)*Power(t2,3) - 
         315*Power(s2,4)*Power(t1,3)*Power(t2,3) + 
         20*Power(s2,5)*Power(t1,3)*Power(t2,3) + 
         105*Power(t1,4)*Power(t2,3) - 197*s2*Power(t1,4)*Power(t2,3) + 
         133*Power(s2,2)*Power(t1,4)*Power(t2,3) + 
         265*Power(s2,3)*Power(t1,4)*Power(t2,3) - 
         15*Power(s2,4)*Power(t1,4)*Power(t2,3) + 
         53*Power(t1,5)*Power(t2,3) - 77*s2*Power(t1,5)*Power(t2,3) - 
         128*Power(s2,2)*Power(t1,5)*Power(t2,3) + 
         6*Power(s2,3)*Power(t1,5)*Power(t2,3) + 
         17*Power(t1,6)*Power(t2,3) + 32*s2*Power(t1,6)*Power(t2,3) - 
         Power(s2,2)*Power(t1,6)*Power(t2,3) - 
         3*Power(t1,7)*Power(t2,3) - 12*Power(t2,4) + 16*s2*Power(t2,4) + 
         54*Power(s2,2)*Power(t2,4) + 21*Power(s2,3)*Power(t2,4) - 
         33*Power(s2,4)*Power(t2,4) - Power(s2,5)*Power(t2,4) + 
         15*Power(s2,6)*Power(t2,4) + Power(s2,7)*Power(t2,4) - 
         4*t1*Power(t2,4) - 132*s2*t1*Power(t2,4) - 
         41*Power(s2,2)*t1*Power(t2,4) + 180*Power(s2,3)*t1*Power(t2,4) - 
         2*Power(s2,4)*t1*Power(t2,4) - 77*Power(s2,5)*t1*Power(t2,4) - 
         5*Power(s2,6)*t1*Power(t2,4) + 78*Power(t1,2)*Power(t2,4) + 
         15*s2*Power(t1,2)*Power(t2,4) - 
         334*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         29*Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         161*Power(s2,4)*Power(t1,2)*Power(t2,4) + 
         10*Power(s2,5)*Power(t1,2)*Power(t2,4) + 
         5*Power(t1,3)*Power(t2,4) + 260*s2*Power(t1,3)*Power(t2,4) - 
         65*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         174*Power(s2,3)*Power(t1,3)*Power(t2,4) - 
         10*Power(s2,4)*Power(t1,3)*Power(t2,4) - 
         73*Power(t1,4)*Power(t2,4) + 56*s2*Power(t1,4)*Power(t2,4) + 
         101*Power(s2,2)*Power(t1,4)*Power(t2,4) + 
         5*Power(s2,3)*Power(t1,4)*Power(t2,4) - 
         17*Power(t1,5)*Power(t2,4) - 29*s2*Power(t1,5)*Power(t2,4) - 
         Power(s2,2)*Power(t1,5)*Power(t2,4) + 
         3*Power(t1,6)*Power(t2,4) + 12*Power(t2,5) + 10*s2*Power(t2,5) - 
         27*Power(s2,2)*Power(t2,5) - 39*Power(s2,3)*Power(t2,5) - 
         Power(s2,4)*Power(t2,5) + 8*Power(s2,5)*Power(t2,5) + 
         Power(s2,6)*Power(t2,5) - 22*t1*Power(t2,5) + 
         54*s2*t1*Power(t2,5) + 114*Power(s2,2)*t1*Power(t2,5) - 
         6*Power(s2,3)*t1*Power(t2,5) - 32*Power(s2,4)*t1*Power(t2,5) - 
         4*Power(s2,5)*t1*Power(t2,5) - 27*Power(t1,2)*Power(t2,5) - 
         109*s2*Power(t1,2)*Power(t2,5) + 
         26*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
         49*Power(s2,3)*Power(t1,2)*Power(t2,5) + 
         6*Power(s2,4)*Power(t1,2)*Power(t2,5) + 
         34*Power(t1,3)*Power(t2,5) - 30*s2*Power(t1,3)*Power(t2,5) - 
         35*Power(s2,2)*Power(t1,3)*Power(t2,5) - 
         4*Power(s2,3)*Power(t1,3)*Power(t2,5) + 
         11*Power(t1,4)*Power(t2,5) + 11*s2*Power(t1,4)*Power(t2,5) + 
         Power(s2,2)*Power(t1,4)*Power(t2,5) - Power(t1,5)*Power(t2,5) - 
         4*Power(t2,6) - 8*s2*Power(t2,6) - 2*Power(s2,2)*Power(t2,6) + 
         5*Power(s2,3)*Power(t2,6) + 2*Power(s2,4)*Power(t2,6) + 
         12*t1*Power(t2,6) + 8*s2*t1*Power(t2,6) - 
         13*Power(s2,2)*t1*Power(t2,6) - 6*Power(s2,3)*t1*Power(t2,6) - 
         6*Power(t1,2)*Power(t2,6) + 11*s2*Power(t1,2)*Power(t2,6) + 
         6*Power(s2,2)*Power(t1,2)*Power(t2,6) - 
         3*Power(t1,3)*Power(t2,6) - 2*s2*Power(t1,3)*Power(t2,6) + 
         Power(s1,5)*Power(s2 - t1,3)*
          (2*Power(s2,3) + Power(s2,2)*(9 - 4*t1) + 
            Power(t1,2)*(9 - 2*t2) + 2*t2 - t1*(7 + t2) + 
            s2*(5 + 2*Power(t1,2) + 2*t1*(-9 + t2) + 3*t2)) + 
         Power(s1,4)*(s2 - t1)*
          (4*Power(s2,6) + Power(t1,6) - 5*Power(t1,5)*(-6 + t2) + 
            4*Power(-1 + t2,2) - Power(s2,5)*(4 + 19*t1 + 4*t2) + 
            Power(t1,2)*(-15 + Power(t2,2)) + 
            Power(t1,4)*(-37 - 35*t2 + 6*Power(t2,2)) - 
            2*t1*(5 - 12*t2 + 7*Power(t2,2)) + 
            Power(t1,3)*(37 + 9*t2 + 8*Power(t2,2)) + 
            Power(s2,4)*(-23 + 37*Power(t1,2) - 20*t2 - 2*Power(t2,2) + 
               t1*(46 + 13*t2)) - 
            Power(s2,3)*(38*Power(t1,3) + 2*Power(t1,2)*(72 + 5*t2) - 
               t1*(104 + 97*t2) + 2*(21 - 5*t2 + 7*Power(t2,2))) - 
            s2*(7*Power(t1,5) + 4*t1*(-8 + t2) - 
               2*Power(t1,4)*(-62 + 7*t2) - 
               2*(3 - 8*t2 + 5*Power(t2,2)) + 
               2*Power(t1,2)*(58 + 4*t2 + 15*Power(t2,2)) + 
               Power(t1,3)*(-132 - 127*t2 + 16*Power(t2,2))) + 
            Power(s2,2)*(-17 + 22*Power(t1,4) + 
               Power(t1,3)*(196 - 8*t2) + 4*t2 - Power(t2,2) + 
               Power(t1,2)*(-176 - 169*t2 + 12*Power(t2,2)) + 
               t1*(121 - 11*t2 + 36*Power(t2,2)))) + 
         Power(s1,3)*(-2*Power(t1,8) + Power(s2,7)*(7*t1 - 5*t2) + 
            10*t1*Power(-1 + t2,2) + 4*Power(-1 + t2,3) + 
            Power(t1,7)*(-33 + 5*t2) + 
            Power(t1,6)*(46 + 81*t2 - 9*Power(t2,2)) + 
            Power(t1,5)*(-101 - 23*t2 - 59*Power(t2,2) + 
               6*Power(t2,3)) - 
            4*Power(t1,2)*(1 + 2*t2 - 12*Power(t2,2) + 9*Power(t2,3)) + 
            Power(t1,4)*(82 + 74*t2 - 53*Power(t2,2) + 18*Power(t2,3)) + 
            Power(t1,3)*(-14 - 71*t2 + 35*Power(t2,2) + 
               18*Power(t2,3)) + 
            Power(s2,6)*(8 - 44*Power(t1,2) + 4*t2 + 7*Power(t2,2) + 
               2*t1*(-9 + 16*t2)) + 
            Power(s2,5)*(42 + 117*Power(t1,3) + 
               Power(t1,2)*(123 - 90*t2) + 23*t2 + 9*Power(t2,2) + 
               6*Power(t2,3) - t1*(81 + 88*t2 + 32*Power(t2,2))) + 
            Power(s2,4)*(55 - 170*Power(t1,4) + 88*t2 - 
               76*Power(t2,2) + 22*Power(t2,3) + 
               5*Power(t1,3)*(-69 + 29*t2) + 
               Power(t1,2)*(290 + 393*t2 + 49*Power(t2,2)) - 
               t1*(275 + 100*t2 + 100*Power(t2,2) + 18*Power(t2,3))) + 
            Power(s2,3)*(33 + 145*Power(t1,5) + 
               Power(t1,4)*(510 - 145*t2) + 33*t2 - 20*Power(t2,2) - 
               14*Power(t2,3) - 
               2*Power(t1,3)*(255 + 386*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(674 + 185*t2 + 305*Power(t2,2) + 
                  12*Power(t2,3)) - 
               4*t1*(62 + 84*t2 - 70*Power(t2,2) + 21*Power(t2,3))) + 
            s2*(19*Power(t1,7) + Power(t1,6)*(183 - 32*t2) - 
               2*Power(-1 + t2,2)*(3 + 2*t2) + 
               Power(t1,5)*(-233 - 396*t2 + 32*Power(t2,2)) + 
               Power(t1,2)*(63 + 171*t2 - 88*Power(t2,2) - 
                  50*Power(t2,3)) + 
               Power(t1,4)*(452 + 100*t2 + 250*Power(t2,2) - 
                  18*Power(t2,3)) + 
               4*t1*(1 + 4*t2 - 21*Power(t2,2) + 16*Power(t2,3)) - 
               2*Power(t1,3)*
                (151 + 154*t2 - 117*Power(t2,2) + 38*Power(t2,3))) + 
            Power(s2,2)*(-72*Power(t1,6) + 30*Power(t1,5)*(-14 + 3*t2) + 
               Power(t1,4)*(480 + 778*t2 - 31*Power(t2,2)) - 
               4*t2*(2 - 9*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(-792 - 185*t2 - 405*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(-82 - 133*t2 + 73*Power(t2,2) + 46*Power(t2,3)) + 
               Power(t1,2)*(413 + 482*t2 - 385*Power(t2,2) + 
                  120*Power(t2,3)))) - 
         Power(s1,2)*(Power(s2,9) + Power(t1,9) - 
            Power(t1,8)*(-16 + t2) + 12*Power(-1 + t2,3)*t2 + 
            Power(s2,8)*(1 - 6*t1 + 11*t2) + 
            Power(t1,7)*(-23 - 57*t2 + Power(t2,2)) - 
            6*t1*Power(-1 + t2,2)*(-2 - 3*t2 + 4*Power(t2,2)) + 
            Power(t1,6)*(98 - 6*t2 + 73*Power(t2,2) - 3*Power(t2,3)) + 
            Power(t1,2)*(-12 + 67*t2 - 82*Power(t2,2) + 
               51*Power(t2,3) - 24*Power(t2,4)) + 
            2*Power(t1,5)*(-48 - 84*t2 + 65*Power(t2,2) - 
               24*Power(t2,3) + Power(t2,4)) + 
            4*Power(t1,3)*(-8 - 8*t2 - 7*Power(t2,2) + 9*Power(t2,3) + 
               7*Power(t2,4)) + 
            Power(t1,4)*(56 + 145*t2 + 8*Power(t2,2) - 
               116*Power(t2,3) + 16*Power(t2,4)) + 
            Power(s2,7)*(17 + 13*Power(t1,2) - 2*t2 + 11*Power(t2,2) - 
               t1*(22 + 67*t2)) + 
            Power(s2,6)*(21 - 7*Power(t1,3) - 3*Power(t2,2) + 
               9*Power(t2,3) + Power(t1,2)*(127 + 170*t2) - 
               2*t1*(56 + 14*t2 + 33*Power(t2,2))) - 
            Power(s2,5)*(-31 + 21*Power(t1,4) - 68*t2 + 
               38*Power(t2,2) + 14*Power(t2,3) - 6*Power(t2,4) + 
               Power(t1,3)*(356 + 229*t2) - 
               Power(t1,2)*(328 + 227*t2 + 164*Power(t2,2)) + 
               t1*(177 + 8*t2 + 35*Power(t2,2) + 49*Power(t2,3))) + 
            Power(s2,4)*(35 + 49*Power(t1,5) + 98*t2 + 25*Power(t2,2) - 
               101*Power(t2,3) + 12*Power(t2,4) + 
               5*Power(t1,4)*(115 + 34*t2) - 
               5*Power(t1,3)*(111 + 129*t2 + 43*Power(t2,2)) + 
               Power(t1,2)*(596 + 26*t2 + 243*Power(t2,2) + 
                  103*Power(t2,3)) + 
               t1*(-211 - 445*t2 + 291*Power(t2,2) + 3*Power(t2,3) - 
                  22*Power(t2,4))) - 
            Power(s2,3)*(-18 + 49*Power(t1,6) - 87*t2 + 
               60*Power(t2,2) - 11*Power(t2,3) + 28*Power(t2,4) + 
               Power(t1,5)*(566 + 61*t2) - 
               5*Power(t1,4)*(117 + 188*t2 + 31*Power(t2,2)) + 
               2*Power(t1,3)*
                (517 + 12*t2 + 281*Power(t2,2) + 51*Power(t2,3)) - 
               Power(t1,2)*(543 + 1095*t2 - 775*Power(t2,2) + 
                  123*Power(t2,3) + 28*Power(t2,4)) + 
               t1*(171 + 419*t2 + 89*Power(t2,2) - 415*Power(t2,3) + 
                  52*Power(t2,4))) + 
            Power(s2,2)*(27*Power(t1,7) + Power(t1,6)*(337 + 2*t2) - 
               2*Power(t1,5)*(191 + 379*t2 + 28*Power(t2,2)) + 
               t2*(43 - 70*t2 + 51*Power(t2,2) - 24*Power(t2,3)) + 
               Power(t1,4)*(981 - 4*t2 + 623*Power(t2,2) + 
                  43*Power(t2,3)) - 
               Power(t1,3)*(673 + 1295*t2 - 959*Power(t2,2) + 
                  247*Power(t2,3) + 12*Power(t2,4)) + 
               4*t1*(-17 - 53*t2 + 26*Power(t2,2) + 2*Power(t2,3) + 
                  21*Power(t2,4)) + 
               Power(t1,2)*(293 + 689*t2 + 111*Power(t2,2) - 
                  643*Power(t2,3) + 84*Power(t2,4))) - 
            s2*(8*Power(t1,8) + Power(t1,7)*(112 - 5*t2) - 
               6*Power(-1 + t2,2)*(-2 - t2 + 2*Power(t2,2)) - 
               Power(t1,6)*(142 + 323*t2 + 6*Power(t2,2)) + 
               Power(t1,5)*(485 - 16*t2 + 339*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,4)*(-406 - 745*t2 + 567*Power(t2,2) - 
                  183*Power(t2,3) + 2*Power(t2,4)) - 
               2*t1*(6 - 55*t2 + 76*Power(t2,2) - 51*Power(t2,3) + 
                  24*Power(t2,4)) + 
               Power(t1,3)*(213 + 513*t2 + 55*Power(t2,2) - 
                  445*Power(t2,3) + 60*Power(t2,4)) + 
               Power(t1,2)*(-82 - 157*t2 + 16*Power(t2,2) + 
                  55*Power(t2,3) + 84*Power(t2,4)))) + 
         s1*(Power(s2,10) + 12*Power(-1 + t2,3)*Power(t2,2) - 
            Power(t1,9)*(4 + t2) + Power(s2,9)*(2 - 9*t1 + 7*t2) + 
            Power(t1,8)*(14 + 14*t2 + 3*Power(t2,2)) - 
            2*t1*Power(-1 + t2,2)*t2*(-12 - 3*t2 + 16*Power(t2,2)) - 
            Power(t1,7)*(58 - 5*t2 + 20*Power(t2,2) + 3*Power(t2,3)) + 
            Power(t1,6)*(42 + 126*t2 - 96*Power(t2,2) + 
               19*Power(t2,3) + Power(t2,4)) - 
            Power(t1,5)*(34 + 79*t2 + 67*Power(t2,2) - 
               133*Power(t2,3) + 14*Power(t2,4)) + 
            2*Power(t1,2)*(-8 + 13*t2 + 47*Power(t2,2) - 
               81*Power(t2,3) + 27*Power(t2,4) + 2*Power(t2,5)) + 
            Power(t1,4)*(35 + 94*t2 - 81*Power(t2,2) + 44*Power(t2,3) - 
               72*Power(t2,4) + 5*Power(t2,5)) + 
            Power(t1,3)*(11 - 164*t2 + 141*Power(t2,2) + 
               23*Power(t2,3) - 33*Power(t2,4) + 16*Power(t2,5)) + 
            Power(s2,8)*(-2 + 36*Power(t1,2) + 5*t2 + 16*Power(t2,2) - 
               8*t1*(2 + 7*t2)) + 
            Power(s2,7)*(5 - 84*Power(t1,3) + 13*t2 - 6*Power(t2,2) + 
               11*Power(t2,3) + Power(t1,2)*(60 + 197*t2) + 
               t1*(11 - 48*t2 - 111*Power(t2,2))) + 
            Power(s2,6)*(44 + 126*Power(t1,4) - 8*t2 - 4*Power(t2,2) - 
               18*Power(t2,3) + 3*Power(t2,4) - 
               7*Power(t1,3)*(20 + 57*t2) + 
               Power(t1,2)*(-10 + 197*t2 + 333*Power(t2,2)) - 
               t1*(86 + 52*t2 - 15*Power(t2,2) + 70*Power(t2,3))) + 
            Power(s2,5)*(18 - 126*Power(t1,5) + 37*t2 + 8*Power(t2,2) - 
               37*Power(t2,3) - 20*Power(t2,4) + 2*Power(t2,5) + 
               7*Power(t1,4)*(32 + 73*t2) - 
               Power(t1,3)*(59 + 454*t2 + 563*Power(t2,2)) + 
               Power(t1,2)*(413 + 60*t2 + 35*Power(t2,2) + 
                  188*Power(t2,3)) + 
               t1*(-262 - 50*t2 + 89*Power(t2,2) + 80*Power(t2,3) - 
                  20*Power(t2,4))) + 
            Power(s2,4)*(-1 + 84*Power(t1,6) + 88*t2 - 50*Power(t2,2) + 
               12*Power(t2,3) - 39*Power(t2,4) - Power(t2,5) - 
               7*Power(t1,5)*(36 + 61*t2) + 
               5*Power(t1,4)*(40 + 129*t2 + 117*Power(t2,2)) - 
               5*Power(t1,3)*
                (190 - 5*t2 + 38*Power(t2,2) + 55*Power(t2,3)) + 
               Power(t1,2)*(650 + 406*t2 - 412*Power(t2,2) - 
                  121*Power(t2,3) + 51*Power(t2,4)) - 
               t1*(99 + 231*t2 + 80*Power(t2,2) - 266*Power(t2,3) - 
                  63*Power(t2,4) + 8*Power(t2,5))) - 
            Power(s2,3)*(3 + 36*Power(t1,7) - 100*t2 + 40*Power(t2,2) + 
               97*Power(t2,3) - 66*Power(t2,4) + 20*Power(t2,5) - 
               7*Power(t1,6)*(28 + 33*t2) + 
               Power(t1,5)*(283 + 580*t2 + 381*Power(t2,2)) - 
               5*Power(t1,4)*
                (243 - 23*t2 + 64*Power(t2,2) + 47*Power(t2,3)) + 
               Power(t1,3)*(860 + 964*t2 - 838*Power(t2,2) - 
                  44*Power(t2,3) + 64*Power(t2,4)) + 
               2*t1*(12 + 196*t2 - 139*Power(t2,2) + 48*Power(t2,3) - 
                  92*Power(t2,4) + Power(t2,5)) - 
               Power(t1,2)*(223 + 550*t2 + 259*Power(t2,2) - 
                  709*Power(t2,3) - 55*Power(t2,4) + 12*Power(t2,5))) - 
            s2*(Power(t1,9) - 2*Power(t1,8)*(15 + 7*t2) - 
               2*Power(-1 + t2,2)*t2*(-12 + 3*t2 + 10*Power(t2,2)) + 
               Power(t1,7)*(85 + 102*t2 + 33*Power(t2,2)) - 
               Power(t1,6)*(351 - 38*t2 + 115*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(t1,5)*(254 + 586*t2 - 457*Power(t2,2) + 
                  68*Power(t2,3) + 12*Power(t2,4)) + 
               4*t1*(-8 + 19*t2 + 32*Power(t2,2) - 71*Power(t2,3) + 
                  28*Power(t2,4)) - 
               Power(t1,4)*(147 + 357*t2 + 257*Power(t2,2) - 
                  554*Power(t2,3) + 39*Power(t2,4) + 2*Power(t2,5)) + 
               2*Power(t1,3)*
                (48 + 202*t2 - 170*Power(t2,2) + 80*Power(t2,3) - 
                  125*Power(t2,4) + 7*Power(t2,5)) + 
               Power(t1,2)*(25 - 428*t2 + 316*Power(t2,2) + 
                  155*Power(t2,3) - 138*Power(t2,4) + 52*Power(t2,5))) + 
            Power(s2,2)*(-16 + 9*Power(t1,8) + 50*t2 + 34*Power(t2,2) - 
               122*Power(t2,3) + 58*Power(t2,4) - 4*Power(t2,5) - 
               Power(t1,7)*(100 + 77*t2) + 
               Power(t1,6)*(214 + 323*t2 + 151*Power(t2,2)) - 
               Power(t1,5)*(890 - 102*t2 + 269*Power(t2,2) + 
                  116*Power(t2,3)) + 
               Power(t1,4)*(640 + 1076*t2 - 872*Power(t2,2) + 
                  64*Power(t2,3) + 41*Power(t2,4)) - 
               Power(t1,3)*(255 + 634*t2 + 377*Power(t2,2) - 
                  901*Power(t2,3) + 13*Power(t2,4) + 8*Power(t2,5)) + 
               Power(t1,2)*(86 + 614*t2 - 487*Power(t2,2) + 
                  200*Power(t2,3) - 323*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(17 - 364*t2 + 215*Power(t2,2) + 229*Power(t2,3) - 
                  171*Power(t2,4) + 56*Power(t2,5)))) + 
         Power(s,7)*(-6 + Power(s2,3) + Power(t1,2) - Power(t1,3) + 
            2*Power(t1,4) - 2*t1*t2 + 3*Power(t1,2)*t2 - 
            8*Power(t1,3)*t2 + Power(t2,2) - 3*t1*Power(t2,2) + 
            12*Power(t1,2)*Power(t2,2) + Power(t2,3) - 
            8*t1*Power(t2,3) + 2*Power(t2,4) - 
            Power(s1,3)*(-1 + s2 - t1 + t2) + 
            Power(s2,2)*(-7 + 2*Power(t1,2) + t2 + 2*Power(t2,2) - 
               t1*(1 + 4*t2)) + 
            Power(s1,2)*(-7 - 4*Power(s2,2) + 4*Power(t1,2) + t2 + 
               4*Power(t2,2) + s2*(11 - 2*t1 + 2*t2) - t1*(1 + 8*t2)) + 
            s2*(12 - 4*Power(t1,3) + Power(t2,2) + 4*Power(t2,3) - 
               2*t1*t2*(1 + 6*t2) + Power(t1,2)*(1 + 12*t2)) - 
            s1*(-12 + Power(s2,3) - 5*Power(t1,3) + 3*Power(t2,2) + 
               5*Power(t2,3) + Power(s2,2)*(-11 - t1 + t2) + 
               3*Power(t1,2)*(1 + 5*t2) - 3*t1*t2*(2 + 5*t2) + 
               s2*(22 + 5*Power(t1,2) + 2*t2 + 5*Power(t2,2) - 
                  2*t1*(1 + 5*t2)))) + 
         Power(s,6)*(30 - 6*Power(s2,4) - 43*t1 - 16*Power(t1,2) + 
            13*Power(t1,3) - 15*Power(t1,4) + 9*Power(t1,5) + t2 + 
            36*t1*t2 - 30*Power(t1,2)*t2 + 51*Power(t1,3)*t2 - 
            35*Power(t1,4)*t2 - 20*Power(t2,2) + 21*t1*Power(t2,2) - 
            63*Power(t1,2)*Power(t2,2) + 50*Power(t1,3)*Power(t2,2) - 
            4*Power(t2,3) + 33*t1*Power(t2,3) - 
            30*Power(t1,2)*Power(t2,3) - 6*Power(t2,4) + 
            5*t1*Power(t2,4) + Power(t2,5) + 
            Power(s1,4)*(-1 + s2 - t1 + t2) + 
            Power(s1,3)*(-5 + 16*Power(s2,2) + Power(t1,2) + 11*t2 - 
               4*Power(t2,2) + t1*(-2 + 3*t2) + s2*(-11 - 15*t1 + 6*t2)) \
+ Power(s2,3)*(31 - 9*Power(t1,2) - 14*t2 - 13*Power(t2,2) + 
               t1*(19 + 22*t2)) + 
            Power(s2,2)*(-38 + 27*Power(t1,3) - 3*t2 - 30*Power(t2,2) - 
               25*Power(t2,3) - Power(t1,2)*(35 + 79*t2) + 
               t1*(-40 + 65*t2 + 77*Power(t2,2))) - 
            s2*(17 + 27*Power(t1,4) + 4*t2 - 4*Power(t2,2) + 
               28*Power(t2,3) + 11*Power(t2,4) - 
               Power(t1,3)*(37 + 92*t2) + 
               2*Power(t1,2)*(2 + 51*t2 + 57*Power(t2,2)) - 
               3*t1*(28 + 31*Power(t2,2) + 20*Power(t2,3))) + 
            Power(s1,2)*(38 + 18*Power(s2,3) + 14*Power(t1,3) + 
               Power(s2,2)*(-67 + 2*t1 - 30*t2) - 23*t2 - 
               23*Power(t2,2) + 6*Power(t2,3) - 
               2*Power(t1,2)*(16 + 11*t2) + 
               t1*(-32 + 55*t2 + 2*Power(t2,2)) + 
               s2*(11 - 34*Power(t1,2) - 4*t2 - 28*Power(t2,2) + 
                  t1*(87 + 62*t2))) + 
            s1*(-61 + 7*Power(s2,4) + 21*Power(t1,4) + 12*t2 + 
               32*Power(t2,2) + 19*Power(t2,3) - 4*Power(t2,4) + 
               Power(s2,3)*(-53 - 17*t1 + 12*t2) - 
               Power(t1,3)*(46 + 59*t2) + 
               Power(s2,2)*(95 + 34*Power(t1,2) + t1*(32 - 75*t2) + 
                  39*t2 + 41*Power(t2,2)) + 
               Power(t1,2)*(28 + 111*t2 + 51*Power(t2,2)) - 
               t1*(-76 + 60*t2 + 84*Power(t2,2) + 9*Power(t2,3)) + 
               s2*(12 - 45*Power(t1,3) - 4*t2 + 41*Power(t2,2) + 
                  32*Power(t2,3) + Power(t1,2)*(67 + 122*t2) - 
                  t1*(150 + 108*t2 + 109*Power(t2,2))))) - 
         Power(s,5)*(54 - 15*Power(s2,5) - 181*t1 + 54*Power(t1,2) + 
            92*Power(t1,3) - 52*Power(t1,4) + 49*Power(t1,5) - 
            16*Power(t1,6) + t2 + 162*t1*t2 - 192*Power(t1,2)*t2 + 
            109*Power(t1,3)*t2 - 166*Power(t1,4)*t2 + 
            60*Power(t1,5)*t2 - 90*Power(t2,2) + 84*t1*Power(t2,2) - 
            48*Power(t1,2)*Power(t2,2) + 203*Power(t1,3)*Power(t2,2) - 
            80*Power(t1,4)*Power(t2,2) + 16*Power(t2,3) - 
            23*t1*Power(t2,3) - 103*Power(t1,2)*Power(t2,3) + 
            40*Power(t1,3)*Power(t2,3) + 14*Power(t2,4) + 
            16*t1*Power(t2,4) + Power(t2,5) - 4*t1*Power(t2,5) + 
            Power(s2,4)*(45 - 16*Power(t1,2) - 53*t2 - 36*Power(t2,2) + 
               t1*(72 + 50*t2)) + 
            Power(s1,4)*(14*Power(s2,2) + 5*Power(t1,2) - t1*(7 + t2) + 
               s2*(2 - 19*t1 + 6*t2) - 2*(7 - 8*t2 + Power(t2,2))) + 
            Power(s2,3)*(-5 + 64*Power(t1,3) - 50*t2 - 
               128*Power(t2,2) - 66*Power(t2,3) - 
               35*Power(t1,2)*(5 + 6*t2) + 
               t1*(-111 + 298*t2 + 212*Power(t2,2))) + 
            Power(s2,2)*(-150 - 96*Power(t1,4) + 49*t2 - 
               23*Power(t2,2) - 114*Power(t2,3) - 24*Power(t2,4) + 
               3*Power(t1,3)*(81 + 110*t2) + 
               Power(t1,2)*(35 - 603*t2 - 396*Power(t2,2)) + 
               t1*(179 + 99*t2 + 474*Power(t2,2) + 186*Power(t2,3))) + 
            s2*(73 + 64*Power(t1,5) - 60*t2 - 2*Power(t2,2) - 
               40*Power(t2,3) - 23*Power(t2,4) + 6*Power(t2,5) - 
               2*Power(t1,4)*(87 + 115*t2) + 
               Power(t1,3)*(83 + 524*t2 + 300*Power(t2,2)) - 
               Power(t1,2)*(266 + 158*t2 + 549*Power(t2,2) + 
                  160*Power(t2,3)) + 
               t1*(150 + 40*t2 + 115*Power(t2,2) + 222*Power(t2,3) + 
                  20*Power(t2,4))) + 
            Power(s1,3)*(23 + 61*Power(s2,3) + 8*Power(t1,3) + 29*t2 - 
               52*Power(t2,2) + 6*Power(t2,3) + 
               Power(s2,2)*(-44 - 93*t1 + 2*t2) - 
               5*Power(t1,2)*(5 + 3*t2) + 
               t1*(-4 + 40*t2 + Power(t2,2)) + 
               s2*(-48 + 24*Power(t1,2) + 16*t2 - 17*Power(t2,2) + 
                  22*t1*(3 + t2))) + 
            Power(s1,2)*(44 + 30*Power(s2,4) - 15*Power(t1,4) - 
               121*t2 + 10*Power(t2,2) + 57*Power(t2,3) - 
               6*Power(t2,4) + Power(t1,3)*(74 + 15*t2) + 
               Power(t1,2)*(24 - 70*t2 + 9*Power(t2,2)) - 
               t1*(119 - 149*t2 + 61*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,3)*(7*t1 - 2*(83 + 64*t2)) + 
               Power(s2,2)*(-47 - 119*Power(t1,2) + 26*t2 - 
                  88*Power(t2,2) + 3*t1*(126 + 97*t2)) + 
               s2*(151 + 97*Power(t1,3) - 50*t2 - 57*Power(t2,2) + 
                  22*Power(t2,3) - 2*Power(t1,2)*(143 + 89*t2) + 
                  t1*(-17 + 88*t2 + 59*Power(t2,2)))) + 
            s1*(-109 + 21*Power(s2,5) - 34*Power(t1,5) + 100*t2 + 
               82*Power(t2,2) - 39*Power(t2,3) - 22*Power(t2,4) + 
               2*Power(t2,5) + Power(s2,4)*(-101 - 79*t1 + 52*t2) + 
               Power(t1,4)*(141 + 89*t2) - 
               Power(t1,3)*(164 + 314*t2 + 65*Power(t2,2)) + 
               Power(t1,2)*(-122 + 313*t2 + 183*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(278 - 236*t2 - 110*Power(t2,2) + 12*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s2,3)*(145*Power(t1,2) + t1*(158 - 278*t2) + 
                  2*(77 + 78*t2 + 73*Power(t2,2))) + 
               Power(s2,2)*(191 - 171*Power(t1,3) - 5*t2 + 
                  131*Power(t2,2) + 96*Power(t2,3) + 
                  Power(t1,2)*(128 + 489*t2) - 
                  2*t1*(292 + 233*t2 + 207*Power(t2,2))) + 
               s2*(-164 + 118*Power(t1,4) + 14*t2 + 126*Power(t2,2) + 
                  62*Power(t2,3) - 17*Power(t2,4) - 
                  2*Power(t1,3)*(163 + 176*t2) + 
                  Power(t1,2)*(594 + 624*t2 + 333*Power(t2,2)) - 
                  2*t1*(37 + 129*t2 + 180*Power(t2,2) + 41*Power(t2,3))))\
) + Power(s,4)*(42 - 20*Power(s2,6) - 273*t1 + 311*Power(t1,2) + 
            112*Power(t1,3) - 179*Power(t1,4) + 94*Power(t1,5) - 
            69*Power(t1,6) + 14*Power(t1,7) + 3*t2 + 295*t1*t2 - 
            646*Power(t1,2)*t2 + 247*Power(t1,3)*t2 - 
            173*Power(t1,4)*t2 + 227*Power(t1,5)*t2 - 
            50*Power(t1,6)*t2 - 156*Power(t2,2) + 251*t1*Power(t2,2) + 
            153*Power(t1,2)*Power(t2,2) + 40*Power(t1,3)*Power(t2,2) - 
            261*Power(t1,4)*Power(t2,2) + 60*Power(t1,5)*Power(t2,2) + 
            73*Power(t2,3) - 271*t1*Power(t2,3) + 
            43*Power(t1,2)*Power(t2,3) + 109*Power(t1,3)*Power(t2,3) - 
            20*Power(t1,4)*Power(t2,3) + 50*Power(t2,4) + 
            16*t1*Power(t2,4) + 4*Power(t1,2)*Power(t2,4) - 
            10*Power(t1,3)*Power(t2,4) - 20*Power(t2,5) - 
            12*t1*Power(t2,5) + 6*Power(t1,2)*Power(t2,5) + 
            2*Power(t2,6) + Power(s1,5)*
             (-5 + 2*Power(s2,2) + s2*(5 - 2*t1) - 7*t1 + 5*t2 + 
               2*t1*t2) + Power(s2,5)*
             (14 - 14*Power(t1,2) - 91*t2 - 55*Power(t2,2) + 
               60*t1*(2 + t2)) + 
            Power(s2,4)*(100 + 70*Power(t1,3) - 154*t2 - 
               227*Power(t2,2) - 95*Power(t2,3) - 
               Power(t1,2)*(349 + 290*t2) + 
               t1*(-48 + 580*t2 + 325*Power(t2,2))) - 
            Power(s2,3)*(246 + 140*Power(t1,4) - 151*t2 + 
               131*Power(t2,2) + 183*Power(t2,3) + 25*Power(t2,4) - 
               4*Power(t1,3)*(149 + 140*t2) + 
               Power(t1,2)*(34 + 1421*t2 + 705*Power(t2,2)) - 
               t1*(-87 + 517*t2 + 993*Power(t2,2) + 310*Power(t2,3))) + 
            Power(s2,2)*(-67 + 140*Power(t1,5) - 29*t2 + 
               107*Power(t2,2) - 119*Power(t2,3) - 17*Power(t2,4) + 
               15*Power(t2,5) - 54*Power(t1,4)*(11 + 10*t2) + 
               Power(t1,3)*(250 + 1693*t2 + 715*Power(t2,2)) - 
               Power(t1,2)*(305 + 745*t2 + 1566*Power(t2,2) + 
                  355*Power(t2,3)) + 
               t1*(751 - 372*t2 + 459*Power(t2,2) + 484*Power(t2,3) + 
                  25*Power(t2,4))) + 
            s2*(189 - 70*Power(t1,6) - 175*t2 - 130*Power(t2,2) + 
               76*Power(t2,3) + 34*Power(t2,4) + 12*Power(t2,5) + 
               4*Power(t1,5)*(79 + 65*t2) - 
               4*Power(t1,4)*(69 + 247*t2 + 85*Power(t2,2)) + 
               Power(t1,3)*(471 + 555*t2 + 1061*Power(t2,2) + 
                  160*Power(t2,3)) + 
               Power(t1,2)*(-617 - 26*t2 - 368*Power(t2,2) - 
                  410*Power(t2,3) + 10*Power(t2,4)) + 
               t1*(-202 + 552*t2 - 161*Power(t2,2) + 55*Power(t2,3) + 
                  9*Power(t2,4) - 20*Power(t2,5))) + 
            Power(s1,4)*(-20 + 50*Power(s2,3) - 11*Power(t1,3) - 
               Power(t1,2)*(-13 + t2) + 42*t2 - 22*Power(t2,2) + 
               Power(s2,2)*(22 - 109*t1 + 10*t2) + 
               2*t1*(16 - 10*t2 + Power(t2,2)) + 
               s2*(-48 + 70*Power(t1,2) + 44*t2 - 10*Power(t2,2) - 
                  t1*(37 + 7*t2))) + 
            Power(s1,3)*(69 + 104*Power(s2,4) - 24*Power(t1,4) - 
               12*t2 - 85*Power(t2,2) + 34*Power(t2,3) + 
               Power(t1,3)*(107 + 15*t2) - 
               Power(s2,3)*(88 + 219*t1 + 37*t2) + 
               Power(t1,2)*(-4 - 173*t2 + 27*Power(t2,2)) - 
               3*t1*(40 - 9*t2 - 39*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(-190 + 102*Power(t1,2) - 34*t2 - 
                  21*Power(t2,2) + t1*(291 + 122*t2)) + 
               s2*(69 + 37*Power(t1,3) + 60*t2 - 177*Power(t2,2) + 
                  30*Power(t2,3) - 10*Power(t1,2)*(31 + 10*t2) + 
                  t1*(183 + 230*t2 - 12*Power(t2,2)))) + 
            Power(s1,2)*(19*Power(s2,5) - Power(t1,5) + 
               Power(s2,4)*(-219 + 50*t1 - 263*t2) + 
               Power(t1,4)*(-22 + t2) + 
               Power(t1,3)*(5 - 159*t2 + 23*Power(t2,2)) + 
               Power(t1,2)*(128 - 172*t2 + 359*Power(t2,2) - 
                  45*Power(t2,3)) - 
               2*(8 + 92*t2 - 79*Power(t2,2) - 22*Power(t2,3) + 
                  10*Power(t2,4)) + 
               2*t1*(-20 + 172*t2 - 106*Power(t2,2) - 79*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(s2,3)*(-117 - 263*Power(t1,2) + 106*t2 - 
                  163*Power(t2,2) + t1*(681 + 759*t2)) + 
               Power(s2,2)*(246 + 299*Power(t1,3) + 145*t2 - 
                  31*Power(t2,2) + 19*Power(t2,3) - 
                  Power(t1,2)*(727 + 728*t2) + 
                  t1*(83 - 227*t2 + 270*Power(t2,2))) + 
               s2*(151 - 104*Power(t1,4) - 361*t2 + 106*Power(t2,2) + 
                  214*Power(t2,3) - 30*Power(t2,4) + 
                  7*Power(t1,3)*(41 + 33*t2) + 
                  Power(t1,2)*(29 + 280*t2 - 130*Power(t2,2)) + 
                  t1*(-405 + 71*t2 - 366*Power(t2,2) + 33*Power(t2,3)))) \
+ s1*(-87 + 35*Power(s2,6) + 26*Power(t1,6) - 
               5*Power(s2,5)*(18 + 35*t1 - 23*t2) + 214*t2 + 
               42*Power(t2,2) - 176*Power(t2,3) + 24*Power(t2,4) + 
               Power(t2,5) - Power(t1,5)*(178 + 67*t2) + 
               Power(t1,4)*(350 + 316*t2 + 53*Power(t2,2)) + 
               Power(s2,4)*(76 + 376*Power(t1,2) + t1*(253 - 618*t2) + 
                  289*t2 + 290*Power(t2,2)) - 
               Power(t1,3)*(-19 + 575*t2 + 16*Power(t2,2) + 
                  17*Power(t2,3)) + 
               Power(t1,2)*(-417 + 566*t2 + 64*Power(t2,2) - 
                  203*Power(t2,3) + 13*Power(t2,4)) + 
               t1*(343 - 600*t2 + 71*Power(t2,2) + 137*Power(t2,3) + 
                  80*Power(t2,4) - 8*Power(t2,5)) - 
               Power(s2,3)*(-427 + 454*Power(t1,3) + 
                  Power(t1,2)*(41 - 1231*t2) + 48*t2 - 
                  188*Power(t2,2) - 175*Power(t2,3) + 
                  t1*(745 + 950*t2 + 977*Power(t2,2))) + 
               Power(s2,2)*(-112 + 331*Power(t1,4) - 80*t2 + 
                  125*Power(t2,2) + 60*Power(t2,3) - 25*Power(t2,4) - 
                  5*Power(t1,3)*(99 + 227*t2) + 
                  Power(t1,2)*(1612 + 1349*t2 + 1137*Power(t2,2)) - 
                  t1*(886 + 253*t2 + 559*Power(t2,2) + 308*Power(t2,3))) \
+ s2*(-295 - 139*Power(t1,5) + 284*t2 + 192*Power(t2,2) - 
                  152*Power(t2,3) - 98*Power(t2,4) + 10*Power(t2,5) + 
                  Power(t1,4)*(551 + 474*t2) - 
                  Power(t1,3)*(1293 + 1004*t2 + 503*Power(t2,2)) + 
                  Power(t1,2)*
                   (440 + 876*t2 + 387*Power(t2,2) + 150*Power(t2,3)) + 
                  t1*(568 - 512*t2 - 201*Power(t2,2) + 164*Power(t2,3) + 
                     8*Power(t2,4))))) - 
         Power(s,3)*(12 - 15*Power(s2,7) - 175*t1 + 439*Power(t1,2) - 
            110*Power(t1,3) - 284*Power(t1,4) + 161*Power(t1,5) - 
            93*Power(t1,6) + 48*Power(t1,7) - 6*Power(t1,8) + 7*t2 + 
            218*t1*t2 - 950*Power(t1,2)*t2 + 746*Power(t1,3)*t2 - 
            15*Power(t1,4)*t2 + 164*Power(t1,5)*t2 - 
            150*Power(t1,6)*t2 + 20*Power(t1,7)*t2 - 117*Power(t2,2) + 
            372*t1*Power(t2,2) + 166*Power(t1,2)*Power(t2,2) - 
            606*Power(t1,3)*Power(t2,2) - 66*Power(t1,4)*Power(t2,2) + 
            149*Power(t1,5)*Power(t2,2) - 20*Power(t1,6)*Power(t2,2) + 
            88*Power(t2,3) - 454*t1*Power(t2,3) + 
            498*Power(t1,2)*Power(t2,3) + 62*Power(t1,3)*Power(t2,3) - 
            21*Power(t1,4)*Power(t2,3) + 36*Power(t2,4) - 
            3*t1*Power(t2,4) - 110*Power(t1,2)*Power(t2,4) - 
            51*Power(t1,3)*Power(t2,4) + 10*Power(t1,4)*Power(t2,4) - 
            35*Power(t2,5) + 36*t1*Power(t2,5) + 
            31*Power(t1,2)*Power(t2,5) - 4*Power(t1,3)*Power(t2,5) + 
            7*Power(t2,6) - 6*t1*Power(t2,6) - 
            Power(s2,6)*(24 + 6*Power(t1,2) + 79*t2 + 50*Power(t2,2) - 
               t1*(103 + 40*t2)) + 
            Power(s2,5)*(122 + 36*Power(t1,3) - 195*t2 - 
               193*Power(t2,2) - 80*Power(t2,3) - 
               2*Power(t1,2)*(169 + 110*t2) + 
               2*t1*(60 + 283*t2 + 150*Power(t2,2))) - 
            Power(s2,4)*(92 + 90*Power(t1,4) - 82*t2 + 
               198*Power(t2,2) + 117*Power(t2,3) + 10*Power(t2,4) - 
               10*Power(t1,3)*(67 + 50*t2) + 
               Power(t1,2)*(333 + 1624*t2 + 720*Power(t2,2)) - 
               t1*(-393 + 916*t2 + 985*Power(t2,2) + 300*Power(t2,3))) + 
            Power(s2,3)*(-297 + 120*Power(t1,5) + 275*t2 + 
               85*Power(t2,2) - 142*Power(t2,3) + 42*Power(t2,4) + 
               20*Power(t2,5) - 5*Power(t1,4)*(167 + 120*t2) + 
               4*Power(t1,3)*(153 + 604*t2 + 220*Power(t2,2)) - 
               2*Power(t1,2)*
                (-143 + 871*t2 + 973*Power(t2,2) + 210*Power(t2,3)) + 
               t1*(683 - 565*t2 + 854*Power(t2,2) + 368*Power(t2,3))) + 
            Power(s2,2)*(167 - 90*Power(t1,6) - 357*t2 + 
               132*Power(t2,2) + 87*Power(t2,3) + 37*Power(t2,4) + 
               38*Power(t2,5) + 5*Power(t1,5)*(127 + 80*t2) - 
               Power(t1,4)*(678 + 1979*t2 + 570*Power(t2,2)) + 
               2*Power(t1,3)*
                (140 + 840*t2 + 929*Power(t2,2) + 130*Power(t2,3)) + 
               Power(t1,2)*(-1374 + 869*t2 - 1180*Power(t2,2) - 
                  406*Power(t2,3) + 40*Power(t2,4)) - 
               t1*(-593 + 146*t2 + 476*Power(t2,2) - 266*Power(t2,3) + 
                  146*Power(t2,4) + 40*Power(t2,5))) + 
            s2*(151 + 36*Power(t1,7) - 166*t2 - 284*Power(t2,2) + 
               251*Power(t2,3) + 107*Power(t2,4) - 51*Power(t2,5) + 
               8*Power(t2,6) - 4*Power(t1,6)*(67 + 35*t2) + 
               2*Power(t1,5)*(198 + 425*t2 + 90*Power(t2,2)) - 
               Power(t1,4)*(456 + 823*t2 + 853*Power(t2,2) + 
                  60*Power(t2,3)) + 
               Power(t1,3)*(1067 - 371*t2 + 590*Power(t2,2) + 
                  176*Power(t2,3) - 40*Power(t2,4)) + 
               Power(t1,2)*(-186 - 875*t2 + 997*Power(t2,2) - 
                  186*Power(t2,3) + 155*Power(t2,4) + 24*Power(t2,5)) - 
               t1*(594 - 1248*t2 + 223*Power(t2,2) + 617*Power(t2,3) - 
                  74*Power(t2,4) + 68*Power(t2,5))) + 
            2*Power(s1,5)*(4*Power(s2,3) + Power(s2,2)*(12 - 8*t1) + 
               t1*(4 - 8*t2) + Power(t1,2)*(15 - 4*t2) + 4*(-1 + t2) + 
               s2*(-5 + 4*Power(t1,2) + 9*t2 + t1*(-27 + 4*t2))) + 
            Power(s1,4)*(1 + 80*Power(s2,4) + 
               Power(s2,3)*(40 - 246*t1) + 14*Power(t1,4) + 
               Power(t1,3)*(43 - 6*t2) + 18*t2 - 19*Power(t2,2) + 
               Power(t1,2)*(-56 - 71*t2 + 12*Power(t2,2)) + 
               t1*(48 - 90*t2 + 74*Power(t2,2)) + 
               Power(s2,2)*(-89 + 266*Power(t1,2) + 16*t2 - 
                  20*Power(t2,2) + t1*(-43 + 2*t2)) + 
               s2*(-114*Power(t1,3) + 4*Power(t1,2)*(-10 + t2) - 
                  16*(4 - 7*t2 + 5*Power(t2,2)) + 
                  t1*(143 + 57*t2 + 8*Power(t2,2)))) + 
            Power(s1,3)*(53 + 91*Power(s2,5) + 31*Power(t1,5) - 73*t2 + 
               20*Power(t2,2) + 2*Power(t2,3) - 
               Power(t1,4)*(103 + 5*t2) - 
               Power(s2,4)*(95 + 227*t1 + 83*t2) + 
               Power(t1,3)*(12 + 88*t2 - 38*Power(t2,2)) + 
               t1*(-197 + 32*t2 + 240*Power(t2,2) - 120*Power(t2,3)) + 
               2*Power(t1,2)*
                (117 - 78*t2 + 10*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,3)*(-307 + 104*Power(t1,2) - 84*t2 + 
                  6*Power(t2,2) + t1*(445 + 296*t2)) + 
               Power(s2,2)*(47 + 140*Power(t1,3) + 79*t2 - 
                  210*Power(t2,2) + 60*Power(t2,3) - 
                  12*Power(t1,2)*(59 + 29*t2) + 
                  t1*(599 + 338*t2 - 74*Power(t2,2))) + 
               s2*(199 - 139*Power(t1,4) - 282*Power(t2,2) + 
                  124*Power(t2,3) + Power(t1,3)*(461 + 140*t2) + 
                  2*Power(t1,2)*(-152 - 171*t2 + 53*Power(t2,2)) + 
                  t1*(-291 + 94*t2 + 185*Power(t2,2) - 72*Power(t2,3)))) \
- Power(s1,2)*(47 + 4*Power(s2,6) - 14*Power(t1,6) + 
               Power(t1,5)*(57 - 6*t2) + 89*t2 - 199*Power(t2,2) + 
               97*Power(t2,3) - 28*Power(t2,4) + 
               Power(s2,5)*(167 - 104*t1 + 302*t2) + 
               Power(t1,4)*(9 - 305*t2 + 82*Power(t2,2)) + 
               Power(t1,3)*(5 + 28*t2 + 408*Power(t2,2) - 
                  90*Power(t2,3)) + 
               t1*(-157 - 312*t2 + 325*Power(t2,2) + 206*Power(t2,3) - 
                  76*Power(t2,4)) + 
               Power(t1,2)*(55 + 453*t2 - 580*Power(t2,2) - 
                  84*Power(t2,3) + 28*Power(t2,4)) + 
               Power(s2,4)*(97 + 362*Power(t1,2) - 139*t2 + 
                  192*Power(t2,2) - 2*t1*(332 + 549*t2)) - 
               Power(s2,3)*(229 + 488*Power(t1,3) + 457*t2 + 
                  22*Power(t2,2) - 24*Power(t2,3) - 
                  3*Power(t1,2)*(311 + 496*t2) + 
                  t1*(55 - 571*t2 + 542*Power(t2,2))) + 
               Power(s2,2)*(-237 + 272*Power(t1,4) + 387*t2 - 
                  248*Power(t2,2) - 314*Power(t2,3) + 60*Power(t2,4) - 
                  Power(t1,3)*(485 + 896*t2) + 
                  2*Power(t1,2)*(-86 - 515*t2 + 295*Power(t2,2)) + 
                  t1*(576 + 808*t2 + 589*Power(t2,2) - 166*Power(t2,3))\
) + s2*(63 - 32*Power(t1,5) + 471*t2 - 387*Power(t2,2) - 
                  217*Power(t2,3) + 72*Power(t2,4) + 
                  Power(t1,4)*(-8 + 210*t2) + 
                  Power(t1,3)*(121 + 903*t2 - 322*Power(t2,2)) + 
                  Power(t1,2)*
                   (-352 - 379*t2 - 975*Power(t2,2) + 232*Power(t2,3)) \
+ t1*(183 - 851*t2 + 849*Power(t2,2) + 393*Power(t2,3) - 88*Power(t2,4))\
)) + s1*(-31 + 35*Power(s2,7) - 9*Power(t1,7) + 176*t2 - 
               52*Power(t2,2) - 163*Power(t2,3) + 102*Power(t2,4) - 
               26*Power(t2,5) + Power(t1,6)*(107 + 33*t2) + 
               Power(s2,6)*(-27 - 215*t1 + 145*t2) - 
               Power(t1,5)*(388 + 98*t2 + 57*Power(t2,2)) + 
               Power(s2,5)*(-73 + 559*Power(t1,2) + t1*(139 - 842*t2) + 
                  296*t2 + 345*Power(t2,2)) + 
               Power(t1,4)*(158 + 544*t2 - 235*Power(t2,2) + 
                  63*Power(t2,3)) + 
               Power(t1,3)*(309 - 624*t2 + 116*Power(t2,2) + 
                  328*Power(t2,3) - 42*Power(t2,4)) + 
               t1*(176 - 768*t2 + 359*Power(t2,2) + 248*Power(t2,3) + 
                  12*Power(t2,4) - 8*Power(t2,5)) + 
               Power(t1,2)*(-318 + 835*t2 - 384*Power(t2,2) - 
                  258*Power(t2,3) - 94*Power(t2,4) + 12*Power(t2,5)) - 
               Power(s2,4)*(-442 + 795*Power(t1,3) + 
                  Power(t1,2)*(179 - 1951*t2) + 127*t2 - 
                  137*Power(t2,2) - 205*Power(t2,3) + 
                  t1*(179 + 1122*t2 + 1423*Power(t2,2))) + 
               Power(s2,3)*(45 + 665*Power(t1,4) - 182*t2 - 
                  59*Power(t2,2) - 20*Power(t2,3) - 10*Power(t2,4) - 
                  2*Power(t1,3)*(67 + 1142*t2) + 
                  Power(t1,2)*(1363 + 1688*t2 + 2256*Power(t2,2)) - 
                  t1*(1607 - 256*t2 + 402*Power(t2,2) + 592*Power(t2,3))\
) + Power(s2,2)*(-293 - 325*Power(t1,5) + 205*t2 + 174*Power(t2,2) - 
                  275*Power(t2,3) - 182*Power(t2,4) + 20*Power(t2,5) + 
                  Power(t1,4)*(491 + 1391*t2) - 
                  Power(t1,3)*(2285 + 1292*t2 + 1680*Power(t2,2)) + 
                  Power(t1,2)*
                   (2046 + 413*t2 + 158*Power(t2,2) + 632*Power(t2,3)) \
+ t1*(313 - 302*t2 + 201*Power(t2,2) + 440*Power(t2,3) - 38*Power(t2,4))\
) + s2*(-180 + 85*Power(t1,6) + 562*t2 + Power(t2,2) - 430*Power(t2,3) + 
                  14*Power(t2,4) + 2*Power(t2,5) - 
                  Power(t1,5)*(397 + 394*t2) + 
                  Power(t1,4)*(1562 + 528*t2 + 559*Power(t2,2)) - 
                  Power(t1,3)*
                   (1039 + 1086*t2 - 342*Power(t2,2) + 308*Power(t2,3)) \
+ Power(t1,2)*(-667 + 1108*t2 - 258*Power(t2,2) - 748*Power(t2,3) + 
                     90*Power(t2,4)) + 
                  t1*(646 - 1102*t2 + 241*Power(t2,2) + 
                     538*Power(t2,3) + 273*Power(t2,4) - 32*Power(t2,5)))\
)) + Power(s,2)*(-6*Power(s2,8) - 40*t1 + 241*Power(t1,2) - 
            257*Power(t1,3) - 139*Power(t1,4) + 228*Power(t1,5) - 
            84*Power(t1,6) + 54*Power(t1,7) - 16*Power(t1,8) + 
            Power(t1,9) + 4*t2 + 43*t1*t2 - 556*Power(t1,2)*t2 + 
            971*Power(t1,3)*t2 - 277*Power(t1,4)*t2 - 
            103*Power(t1,5)*t2 - 105*Power(t1,6)*t2 + 
            46*Power(t1,7)*t2 - 3*Power(t1,8)*t2 - 32*Power(t2,2) + 
            233*t1*Power(t2,2) - 105*Power(t1,2)*Power(t2,2) - 
            678*Power(t1,3)*Power(t2,2) + 494*Power(t1,4)*Power(t2,2) + 
            94*Power(t1,5)*Power(t2,2) - 29*Power(t1,6)*Power(t2,2) + 
            2*Power(t1,7)*Power(t2,2) + 40*Power(t2,3) - 
            265*t1*Power(t2,3) + 636*Power(t1,2)*Power(t2,3) - 
            204*Power(t1,3)*Power(t2,3) - 124*Power(t1,4)*Power(t2,3) - 
            35*Power(t1,5)*Power(t2,3) + 2*Power(t1,6)*Power(t2,3) - 
            12*Power(t2,4) - 35*t1*Power(t2,4) - 
            182*Power(t1,2)*Power(t2,4) + 99*Power(t1,3)*Power(t2,4) + 
            59*Power(t1,4)*Power(t2,4) - 3*Power(t1,5)*Power(t2,4) + 
            79*t1*Power(t2,5) - Power(t1,2)*Power(t2,5) - 
            31*Power(t1,3)*Power(t2,5) + Power(t1,4)*Power(t2,5) - 
            17*t1*Power(t2,6) + 6*Power(t1,2)*Power(t2,6) - 
            Power(s2,7)*(24 + Power(t1,2) + 32*t2 + 27*Power(t2,2) - 
               t1*(45 + 14*t2)) + 
            Power(s2,6)*(40 + 7*Power(t1,3) - 118*t2 - 68*Power(t2,2) - 
               39*Power(t2,3) - Power(t1,2)*(160 + 87*t2) + 
               t1*(148 + 265*t2 + 167*Power(t2,2))) + 
            Power(s2,5)*(60 - 21*Power(t1,4) - 80*t2 - 
               129*Power(t2,2) + 6*Power(t2,3) + 3*Power(t2,4) + 
               3*Power(t1,3)*(117 + 76*t2) - 
               Power(t1,2)*(434 + 891*t2 + 432*Power(t2,2)) + 
               t1*(-200 + 724*t2 + 405*Power(t2,2) + 168*Power(t2,3))) + 
            Power(s2,4)*(-232 + 35*Power(t1,5) + 316*t2 - 
               47*Power(t2,2) - 72*Power(t2,3) + 88*Power(t2,4) + 
               15*Power(t2,5) - 5*Power(t1,4)*(102 + 65*t2) + 
               10*Power(t1,3)*(79 + 160*t2 + 60*Power(t2,2)) - 
               Power(t1,2)*(-316 + 1821*t2 + 969*Power(t2,2) + 
                  280*Power(t2,3)) + 
               t1*(5 + 79*t2 + 710*Power(t2,2) - 75*Power(t2,3) - 
                  25*Power(t2,4))) + 
            Power(s2,3)*(-37 - 35*Power(t1,6) - 112*t2 + 
               198*Power(t2,2) + 25*Power(t2,3) + 27*Power(t2,4) + 
               52*Power(t2,5) + Power(t1,5)*(491 + 270*t2) - 
               5*Power(t1,4)*(188 + 334*t2 + 95*Power(t2,2)) + 
               2*Power(t1,3)*
                (-32 + 1202*t2 + 593*Power(t2,2) + 110*Power(t2,3)) + 
               Power(t1,2)*(-603 + 346*t2 - 1450*Power(t2,2) + 
                  224*Power(t2,3) + 60*Power(t2,4)) - 
               t1*(-928 + 989*t2 + 59*Power(t2,2) - 246*Power(t2,3) + 
                  332*Power(t2,4) + 40*Power(t2,5))) + 
            Power(s2,2)*(173 + 21*Power(t1,7) - 356*t2 - 
               61*Power(t2,2) + 279*Power(t2,3) + 51*Power(t2,4) - 
               43*Power(t2,5) + 12*Power(t2,6) - 
               3*Power(t1,6)*(100 + 43*t2) + 
               Power(t1,5)*(704 + 1017*t2 + 207*Power(t2,2)) - 
               Power(t1,4)*(304 + 1756*t2 + 774*Power(t2,2) + 
                  75*Power(t2,3)) + 
               Power(t1,3)*(1179 - 712*t2 + 1380*Power(t2,2) - 
                  282*Power(t2,3) - 60*Power(t2,4)) + 
               Power(t1,2)*(-1299 + 753*t2 + 753*Power(t2,2) - 
                  400*Power(t2,3) + 459*Power(t2,4) + 36*Power(t2,5)) - 
               t1*(155 - 1054*t2 + 879*Power(t2,2) + 358*Power(t2,3) - 
                  58*Power(t2,4) + 132*Power(t2,5))) + 
            s2*(40 - 7*Power(t1,8) - 39*t2 - 207*Power(t2,2) + 
               185*Power(t2,3) + 103*Power(t2,4) - 99*Power(t2,5) + 
               19*Power(t2,6) + Power(t1,7)*(105 + 32*t2) - 
               Power(t1,6)*(298 + 335*t2 + 42*Power(t2,2)) + 
               Power(t1,5)*(296 + 672*t2 + 249*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,4)*(-869 + 470*t2 - 605*Power(t2,2) + 
                  162*Power(t2,3) + 25*Power(t2,4)) + 
               Power(t1,3)*(742 + 197*t2 - 1141*Power(t2,2) + 
                  350*Power(t2,3) - 274*Power(t2,4) - 12*Power(t2,5)) + 
               Power(t1,2)*(449 - 1913*t2 + 1359*Power(t2,2) + 
                  537*Power(t2,3) - 184*Power(t2,4) + 111*Power(t2,5)) \
+ t1*(-414 + 904*t2 + 184*Power(t2,2) - 925*Power(t2,3) + 
                  129*Power(t2,4) + 46*Power(t2,5) - 18*Power(t2,6))) + 
            2*Power(s1,5)*(6*Power(s2,4) - 3*Power(s2,3)*(-7 + 6*t1) + 
               t1*(7 - 9*t2) + 6*Power(t1,3)*(-4 + t2) + 2*(-1 + t2) + 
               6*Power(s2,2)*(3*Power(t1,2) + t1*(-11 + t2) + 2*t2) + 
               Power(t1,2)*(3 + 9*t2) - 
               s2*(7 + 6*Power(t1,3) - 9*t2 + 
                  3*Power(t1,2)*(-23 + 4*t2) + 3*t1*(1 + 7*t2))) + 
            Power(s1,4)*(65*Power(s2,5) - 11*Power(t1,5) + 
               Power(s2,4)*(23 - 259*t1 - 15*t2) + 
               Power(t1,4)*(-122 + 19*t2) + 
               Power(t1,2)*(-86 + 45*t2 - 90*Power(t2,2)) + 
               Power(t1,3)*(109 + 173*t2 - 28*Power(t2,2)) + 
               4*(4 - 5*t2 + Power(t2,2)) + 
               t1*(-8 - 13*t2 + 37*Power(t2,2)) + 
               Power(s2,3)*(-113 + 398*Power(t1,2) - 56*t2 - 
                  20*Power(t2,2) + t1*(47 + 38*t2)) + 
               Power(s2,2)*(-119 - 290*Power(t1,3) + 108*t2 - 
                  108*Power(t2,2) - 3*Power(t1,2)*(95 + 4*t2) + 
                  t1*(329 + 291*t2 + 12*Power(t2,2))) + 
               s2*(-2 + 97*Power(t1,4) + Power(t1,3)*(337 - 30*t2) + 
                  25*t2 - 39*Power(t2,2) + 
                  Power(t1,2)*(-325 - 408*t2 + 36*Power(t2,2)) + 
                  t1*(205 - 153*t2 + 198*Power(t2,2)))) + 
            Power(s1,3)*(5 + 40*Power(s2,6) - 23*Power(t1,6) - 50*t2 + 
               81*Power(t2,2) - 36*Power(t2,3) + 
               Power(t1,5)*(-32 + 9*t2) - 
               Power(s2,5)*(53 + 93*t1 + 76*t2) + 
               Power(t1,4)*(75 + 209*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(-333 + 92*t2 - 242*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(-64 + 61*t2 - 37*Power(t2,2) + 14*Power(t2,3)) + 
               2*Power(t1,2)*
                (125 + 55*t2 - 139*Power(t2,2) + 78*Power(t2,3)) + 
               Power(s2,4)*(-203 - 51*Power(t1,2) - 53*t2 + 
                  34*Power(t2,2) + t1*(273 + 331*t2)) + 
               Power(s2,3)*(36 + 334*Power(t1,3) + 117*t2 - 
                  88*Power(t2,2) + 60*Power(t2,3) - 
                  7*Power(t1,2)*(67 + 78*t2) + 
                  t1*(518 + 58*t2 - 140*Power(t2,2))) + 
               Power(s2,2)*(249 - 366*Power(t1,4) + 146*t2 - 
                  385*Power(t2,2) + 168*Power(t2,3) + 
                  Power(t1,3)*(299 + 412*t2) + 
                  4*Power(t1,2)*(-88 + 63*t2 + 45*Power(t2,2)) - 
                  3*t1*(143 + 31*t2 + 27*Power(t2,2) + 36*Power(t2,3))) \
+ s2*(86 + 159*Power(t1,5) - 69*t2 + 19*Power(t2,2) - 10*Power(t2,3) - 
                  2*Power(t1,4)*(9 + 65*t2) - 
                  2*Power(t1,3)*(19 + 233*t2 + 38*Power(t2,2)) + 
                  Power(t1,2)*
                   (726 - 116*t2 + 411*Power(t2,2) + 36*Power(t2,3)) - 
                  2*t1*(252 + 125*t2 - 331*Power(t2,2) + 
                     162*Power(t2,3)))) + 
            Power(s1,2)*(-12*Power(s2,7) - 12*Power(t1,7) + 
               Power(s2,6)*(-73 + 94*t1 - 200*t2) + 
               Power(t1,6)*(22 - 12*t2) + 
               Power(t1,5)*(48 - 65*t2 + 72*Power(t2,2)) + 
               Power(t1,4)*(-224 + 97*t2 + 25*Power(t2,2) - 
                  60*Power(t2,3)) + 
               t1*(129 + 36*t2 - 199*Power(t2,2) + 136*Power(t2,3) - 
                  84*Power(t2,4)) + 
               Power(t1,3)*(179 + 620*t2 - 694*Power(t2,2) + 
                  126*Power(t2,3) + 12*Power(t2,4)) + 
               2*(-9 + 7*t2 + 23*Power(t2,2) - 43*Power(t2,3) + 
                  22*Power(t2,4)) - 
               2*Power(t1,2)*
                (95 + 179*t2 - 57*Power(t2,2) - 198*Power(t2,3) + 
                  54*Power(t2,4)) + 
               Power(s2,5)*(-61 - 278*Power(t1,2) + 80*t2 - 
                  142*Power(t2,2) + 6*t1*(65 + 148*t2)) + 
               Power(s2,4)*(102 + 400*Power(t1,3) + 394*t2 + 
                  27*Power(t2,2) - 56*Power(t2,3) - 
                  4*Power(t1,2)*(202 + 391*t2) + 
                  t1*(97 - 356*t2 + 566*Power(t2,2))) + 
               Power(s2,3)*(119 - 280*Power(t1,4) - 256*t2 + 
                  256*Power(t2,2) + 228*Power(t2,3) - 60*Power(t2,4) + 
                  8*Power(t1,3)*(99 + 172*t2) + 
                  Power(t1,2)*(27 + 653*t2 - 918*Power(t2,2)) + 
                  t1*(-247 - 1129*t2 - 289*Power(t2,2) + 
                     270*Power(t2,3))) + 
               Power(s2,2)*(-14 + 62*Power(t1,5) - 641*t2 + 
                  302*Power(t2,2) + 403*Power(t2,3) - 96*Power(t2,4) - 
                  3*Power(t1,4)*(111 + 208*t2) + 
                  Power(t1,3)*(-53 - 623*t2 + 778*Power(t2,2)) + 
                  Power(t1,2)*
                   (-36 + 1173*t2 + 522*Power(t2,2) - 432*Power(t2,3)) \
+ t1*(-58 + 1141*t2 - 1257*Power(t2,2) - 315*Power(t2,3) + 
                     132*Power(t2,4))) + 
               s2*(-103 + 26*Power(t1,6) - 148*t2 + 305*Power(t2,2) - 
                  156*Power(t2,3) + 84*Power(t2,4) + 
                  2*Power(t1,5)*(5 + 68*t2) + 
                  Power(t1,4)*(-58 + 311*t2 - 356*Power(t2,2)) + 
                  Power(t1,3)*
                   (405 - 535*t2 - 285*Power(t2,2) + 278*Power(t2,3)) - 
                  Power(t1,2)*
                   (240 + 1505*t2 - 1695*Power(t2,2) + 
                     39*Power(t2,3) + 84*Power(t2,4)) + 
                  t1*(208 + 1003*t2 - 428*Power(t2,2) - 
                     795*Power(t2,3) + 204*Power(t2,4)))) + 
            s1*(-4 + 21*Power(s2,8) + Power(t1,8) + 50*t2 - 
               59*Power(t2,2) + 29*Power(t2,4) - 16*Power(t2,5) - 
               Power(t1,7)*(36 + 17*t2) + 
               Power(s2,7)*(13 - 151*t1 + 106*t2) + 
               Power(t1,6)*(256 - t2 + 53*Power(t2,2)) - 
               Power(t1,5)*(273 + 310*t2 - 168*Power(t2,2) + 
                  67*Power(t2,3)) + 
               Power(t1,4)*(-81 + 628*t2 - 240*Power(t2,2) - 
                  171*Power(t2,3) + 38*Power(t2,4)) + 
               Power(t1,3)*(129 - 573*t2 + 108*Power(t2,2) + 
                  394*Power(t2,3) + 22*Power(t2,4) - 8*Power(t2,5)) + 
               Power(t1,2)*(-61 + 734*t2 - 591*Power(t2,2) + 
                  44*Power(t2,3) - 168*Power(t2,4) + 18*Power(t2,5)) + 
               t1*(43 - 414*t2 + 299*Power(t2,2) + 181*Power(t2,3) - 
                  179*Power(t2,4) + 68*Power(t2,5)) + 
               Power(s2,6)*(-103 + 466*Power(t1,2) + 171*t2 + 
                  245*Power(t2,2) - t1*(37 + 683*t2)) + 
               Power(s2,5)*(243 - 801*Power(t1,3) - 97*t2 + 
                  41*Power(t2,2) + 150*Power(t2,3) + 
                  2*Power(t1,2)*(13 + 921*t2) + 
                  t1*(292 - 802*t2 - 1227*Power(t2,2))) + 
               Power(s2,4)*(172 + 830*Power(t1,4) - 136*t2 - 
                  153*Power(t2,2) - 85*Power(t2,3) + 10*Power(t2,4) - 
                  5*Power(t1,3)*(6 + 535*t2) + 
                  Power(t1,2)*(118 + 1497*t2 + 2511*Power(t2,2)) - 
                  t1*(1350 - 459*t2 + 132*Power(t2,2) + 613*Power(t2,3))\
) - Power(s2,3)*(198 + 521*Power(t1,5) - 185*t2 - 78*Power(t2,2) + 
                  287*Power(t2,3) + 178*Power(t2,4) - 20*Power(t2,5) - 
                  5*Power(t1,4)*(29 + 446*t2) + 
                  2*Power(t1,3)*(666 + 694*t2 + 1337*Power(t2,2)) + 
                  Power(t1,2)*
                   (-2865 + 485*t2 + 18*Power(t2,2) - 1006*Power(t2,3)) \
+ 2*t1*(188 + 83*t2 - 321*Power(t2,2) - 258*Power(t2,3) + 
                     46*Power(t2,4))) + 
               Power(s2,2)*(-117 + 186*Power(t1,6) + 430*t2 + 
                  58*Power(t2,2) - 380*Power(t2,3) - 83*Power(t2,4) - 
                  Power(t1,5)*(233 + 1041*t2) + 
                  Power(t1,4)*(1913 + 637*t2 + 1551*Power(t2,2)) - 
                  Power(t1,3)*
                   (2925 + 329*t2 - 436*Power(t2,2) + 840*Power(t2,3)) \
+ Power(t1,2)*(155 + 1368*t2 - 1065*Power(t2,2) - 948*Power(t2,3) + 
                     192*Power(t2,4)) + 
                  t1*(608 - 1107*t2 + 69*Power(t2,2) + 
                     963*Power(t2,3) + 369*Power(t2,4) - 48*Power(t2,5))\
) + s2*(-47 - 31*Power(t1,7) + 362*t2 - 129*Power(t2,2) - 
                  337*Power(t2,3) + 225*Power(t2,4) - 72*Power(t2,5) + 
                  2*Power(t1,6)*(76 + 119*t2) - 
                  Power(t1,5)*(1144 + 114*t2 + 459*Power(t2,2)) + 
                  Power(t1,4)*
                   (1440 + 762*t2 - 495*Power(t2,2) + 364*Power(t2,3)) - 
                  2*Power(t1,3)*
                   (-65 + 847*t2 - 408*Power(t2,2) - 344*Power(t2,3) + 
                     74*Power(t2,4)) + 
                  2*t1*(93 - 593*t2 + 272*Power(t2,2) + 
                     172*Power(t2,3) + 123*Power(t2,4) - 9*Power(t2,5)) \
+ Power(t1,2)*(-539 + 1495*t2 - 255*Power(t2,2) - 1070*Power(t2,3) - 
                     213*Power(t2,4) + 36*Power(t2,5))))) + 
         s*(Power(s2,9) - 44*Power(t1,2) + 135*Power(t1,3) - 
            32*Power(t1,4) - 139*Power(t1,5) + 86*Power(t1,6) - 
            32*Power(t1,7) + 16*Power(t1,8) - 2*Power(t1,9) + 8*t1*t2 + 
            77*Power(t1,2)*t2 - 435*Power(t1,3)*t2 + 451*Power(t1,4)*t2 + 
            21*Power(t1,5)*t2 - 26*Power(t1,6)*t2 - 31*Power(t1,7)*t2 + 
            5*Power(t1,8)*t2 - 34*t1*Power(t2,2) + 
            160*Power(t1,2)*Power(t2,2) + 86*Power(t1,3)*Power(t2,2) - 
            516*Power(t1,4)*Power(t2,2) + 102*Power(t1,5)*Power(t2,2) + 
            29*Power(t1,6)*Power(t2,2) + 3*Power(t1,7)*Power(t2,2) - 
            10*Power(t2,3) + 9*t1*Power(t2,3) - 
            222*Power(t1,2)*Power(t2,3) + 400*Power(t1,3)*Power(t2,3) + 
            92*Power(t1,4)*Power(t2,3) - 29*Power(t1,5)*Power(t2,3) - 
            21*Power(t1,6)*Power(t2,3) + 28*Power(t2,4) + 
            30*t1*Power(t2,4) - 34*Power(t1,2)*Power(t2,4) - 
            208*Power(t1,3)*Power(t2,4) + 2*Power(t1,4)*Power(t2,4) + 
            25*Power(t1,5)*Power(t2,4) - 26*Power(t2,5) - 
            7*t1*Power(t2,5) + 78*Power(t1,2)*Power(t2,5) + 
            26*Power(t1,3)*Power(t2,5) - 12*Power(t1,4)*Power(t2,5) + 
            8*Power(t2,6) - 6*t1*Power(t2,6) - 
            13*Power(t1,2)*Power(t2,6) + 2*Power(t1,3)*Power(t2,6) + 
            Power(s2,8)*(8 + 3*t2 + 8*Power(t2,2) - 2*t1*(4 + t2)) + 
            Power(s2,7)*(5 + 35*t2 - 2*Power(t2,2) + 10*Power(t2,3) + 
               2*Power(t1,2)*(15 + 7*t2) - 
               t1*(59 + 38*t2 + 52*Power(t2,2))) - 
            Power(s2,6)*(36 - 73*t2 - 42*Power(t2,2) + 40*Power(t2,3) + 
               4*Power(t2,4) + 14*Power(t1,3)*(5 + 3*t2) - 
               2*Power(t1,2)*(101 + 85*t2 + 72*Power(t2,2)) + 
               t1*(25 + 263*t2 - 6*Power(t2,2) + 50*Power(t2,3))) + 
            Power(s2,5)*(57 - 74*t2 + 41*Power(t2,2) + 18*Power(t2,3) - 
               61*Power(t2,4) - 6*Power(t2,5) + 
               14*Power(t1,4)*(8 + 5*t2) - 
               Power(t1,3)*(421 + 390*t2 + 220*Power(t2,2)) + 
               Power(t1,2)*(82 + 821*t2 - 3*Power(t2,2) + 
                  100*Power(t2,3)) + 
               t1*(115 - 322*t2 - 265*Power(t2,2) + 230*Power(t2,3) + 
                  20*Power(t2,4))) - 
            Power(s2,4)*(-88 + 53*t2 + 50*Power(t2,2) - 7*Power(t2,3) + 
               9*Power(t2,4) + 33*Power(t2,5) + 
               14*Power(t1,5)*(9 + 5*t2) - 
               20*Power(t1,4)*(29 + 26*t2 + 10*Power(t2,2)) + 
               5*Power(t1,3)*
                (42 + 277*t2 + Power(t2,2) + 20*Power(t2,3)) + 
               Power(t1,2)*(14 - 532*t2 - 669*Power(t2,2) + 
                  541*Power(t2,3) + 40*Power(t2,4)) + 
               t1*(396 - 435*t2 + 166*Power(t2,2) + 69*Power(t2,3) - 
                  270*Power(t2,4) - 20*Power(t2,5))) + 
            Power(s2,3)*(-71 + 227*t2 - 51*Power(t2,2) - 
               167*Power(t2,3) + 39*Power(t2,4) + 13*Power(t2,5) - 
               8*Power(t2,6) + 14*Power(t1,6)*(7 + 3*t2) - 
               Power(t1,5)*(533 + 418*t2 + 108*Power(t2,2)) + 
               5*Power(t1,4)*(69 + 273*t2 + 10*Power(t2,3)) + 
               Power(t1,3)*(-374 - 368*t2 - 866*Power(t2,2) + 
                  664*Power(t2,3) + 40*Power(t2,4)) + 
               Power(t1,2)*(985 - 882*t2 + 150*Power(t2,2) + 
                  128*Power(t2,3) - 469*Power(t2,4) - 24*Power(t2,5)) + 
               t1*(-252 - 193*t2 + 525*Power(t2,2) - 21*Power(t2,3) + 
                  2*Power(t2,4) + 108*Power(t2,5))) + 
            Power(s2,2)*(-44 + 69*t2 + 144*Power(t2,2) - 
               165*Power(t2,3) - 88*Power(t2,4) + 103*Power(t2,5) - 
               17*Power(t2,6) - 2*Power(t1,7)*(25 + 7*t2) + 
               2*Power(t1,6)*(157 + 99*t2 + 16*Power(t2,2)) - 
               Power(t1,5)*(325 + 785*t2 - 12*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,4)*(596 + 37*t2 + 604*Power(t2,2) - 
                  446*Power(t2,3) - 20*Power(t2,4)) + 
               Power(t1,2)*(208 + 996*t2 - 1416*Power(t2,2) + 
                  113*Power(t2,3) + 25*Power(t2,4) - 129*Power(t2,5)) + 
               Power(t1,3)*(-1149 + 776*t2 + 136*Power(t2,2) - 
                  150*Power(t2,3) + 397*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(277 - 873*t2 + 146*Power(t2,2) + 770*Power(t2,3) - 
                  292*Power(t2,4) - 4*Power(t2,5) + 18*Power(t2,6))) + 
            s2*(Power(t1,8)*(15 + 2*t2) + 
               Power(t1,6)*(160 + 243*t2 - 11*Power(t2,2)) - 
               Power(t1,7)*(107 + 50*t2 + 4*Power(t2,2)) + 
               Power(t1,5)*(-373 + 74*t2 - 213*Power(t2,2) + 
                  154*Power(t2,3) + 4*Power(t2,4)) + 
               t2*(-8 + 34*t2 + 3*Power(t2,2) - 58*Power(t2,3) + 
                  27*Power(t2,4) + 2*Power(t2,5)) - 
               Power(t1,4)*(-642 + 276*t2 + 263*Power(t2,2) - 
                  102*Power(t2,3) + 162*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,3)*(-12 - 1201*t2 + 1457*Power(t2,2) - 
                  191*Power(t2,3) - 20*Power(t2,4) + 66*Power(t2,5)) - 
               Power(t1,2)*(341 - 1081*t2 + 181*Power(t2,2) + 
                  1003*Power(t2,3) - 461*Power(t2,4) + 35*Power(t2,5) + 
                  12*Power(t2,6)) + 
               t1*(88 - 146*t2 - 304*Power(t2,2) + 385*Power(t2,3) + 
                  126*Power(t2,4) - 183*Power(t2,5) + 30*Power(t2,6))) - 
            2*Power(s1,5)*(s2 - t1)*
             (4*Power(s2,4) - 4*Power(s2,3)*(-4 + 3*t1) + 
               t1*(3 - 6*t2) + 2*(-1 + t2) + 4*Power(t1,2)*(2 + t2) + 
               Power(t1,3)*(-17 + 4*t2) + 
               Power(s2,2)*(5 + 12*Power(t1,2) + 7*t2 + 
                  t1*(-49 + 4*t2)) - 
               s2*(3 + 4*Power(t1,3) - 6*t2 + Power(t1,2)*(-50 + 8*t2) + 
                  t1*(13 + 11*t2))) + 
            Power(s1,4)*(-26*Power(s2,6) - 5*Power(t1,6) - 
               8*Power(-1 + t2,2) + Power(s2,5)*(2 + 127*t1 + 14*t2) + 
               Power(t1,5)*(-104 + 17*t2) + 
               Power(t1,4)*(108 + 133*t2 - 22*Power(t2,2)) + 
               2*t1*(11 - 20*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(8 + 5*t2 + 17*Power(t2,2)) - 
               Power(t1,3)*(95 + 12*t2 + 46*Power(t2,2)) + 
               Power(s2,4)*(81 - 253*Power(t1,2) + 64*t2 + 
                  10*Power(t2,2) - t1*(110 + 47*t2)) + 
               Power(s2,2)*(20 - 148*Power(t1,4) - 11*t2 + 
                  21*Power(t2,2) + Power(t1,3)*(-626 + 22*t2) + 
                  t1*(-329 + 84*t2 - 174*Power(t2,2)) + 
                  Power(t1,2)*(555 + 603*t2 - 36*Power(t2,2))) + 
               Power(s2,3)*(117 + 262*Power(t1,3) - 48*t2 + 
                  64*Power(t2,2) + Power(t1,2)*(422 + 40*t2) - 
                  t1*(345 + 331*t2 + 8*Power(t2,2))) + 
               s2*(43*Power(t1,5) + Power(t1,4)*(416 - 46*t2) + 
                  t1*(-28 + 6*t2 - 38*Power(t2,2)) - 
                  2*(9 - 16*t2 + 7*Power(t2,2)) + 
                  7*Power(t1,3)*(-57 - 67*t2 + 8*Power(t2,2)) + 
                  Power(t1,2)*(307 - 24*t2 + 156*Power(t2,2)))) - 
            Power(s1,3)*(7*Power(s2,7) + 10*Power(t1,7) + 
               Power(t1,6)*(89 - 13*t2) - 
               2*Power(-1 + t2,2)*(5 + 8*t2) + 
               Power(t1,5)*(-120 - 256*t2 + 21*Power(t2,2)) + 
               Power(t1,2)*(49 + 67*t2 - 18*Power(t2,2) - 
                  34*Power(t2,3)) + 
               Power(t1,4)*(297 + 31*t2 + 216*Power(t2,2) - 
                  18*Power(t2,3)) - 
               4*Power(t1,3)*
                (51 + 51*t2 - 44*Power(t2,2) + 22*Power(t2,3)) + 
               t1*(27 - 2*t2 - 97*Power(t2,2) + 72*Power(t2,3)) + 
               Power(s2,6)*(t1 - 4*(3 + 8*t2)) + 
               Power(s2,5)*(-35 - 120*Power(t1,2) - 4*t2 + 
                  27*Power(t2,2) + t1*(33 + 170*t2)) + 
               Power(s2,4)*(77 + 340*Power(t1,3) + 
                  Power(t1,2)*(77 - 373*t2) + 92*t2 + 6*Power(t2,2) + 
                  30*Power(t2,3) + t1*(25 - 178*t2 - 111*Power(t2,2))) + 
               Power(s2,3)*(-425*Power(t1,4) + 
                  Power(t1,3)*(-398 + 432*t2) + 
                  Power(t1,2)*(255 + 814*t2 + 150*Power(t2,2)) + 
                  2*(87 + 111*t2 - 132*Power(t2,2) + 50*Power(t2,3)) - 
                  t1*(548 + 260*t2 + 249*Power(t2,2) + 72*Power(t2,3))) \
- s2*(15 + 86*Power(t1,6) + Power(t1,5)*(371 - 94*t2) + 14*t2 - 
                  93*Power(t2,2) + 64*Power(t2,3) + 
                  Power(t1,4)*(-440 - 958*t2 + 33*Power(t2,2)) + 
                  t1*(137 + 88*t2 - 37*Power(t2,2) - 60*Power(t2,3)) + 
                  Power(t1,3)*
                   (988 + 138*t2 + 669*Power(t2,2) - 24*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (294 + 311*t2 - 307*Power(t2,2) + 138*Power(t2,3))) + 
               Power(s2,2)*(86 + 273*Power(t1,5) + 
                  Power(t1,4)*(582 - 278*t2) + 25*t2 - 21*Power(t2,2) - 
                  26*Power(t2,3) - 
                  Power(t1,3)*(565 + 1334*t2 + 54*Power(t2,2)) + 
                  Power(t1,2)*
                   (1162 + 275*t2 + 696*Power(t2,2) + 36*Power(t2,3)) - 
                  2*t1*(279 + 320*t2 - 351*Power(t2,2) + 144*Power(t2,3))\
)) + Power(s1,2)*(6*Power(s2,8) - 5*Power(t1,8) - 
               30*Power(-1 + t2,2)*t2 - 3*Power(t1,7)*(10 + t2) + 
               Power(s2,7)*(16 - 39*t1 + 72*t2) + 
               Power(t1,6)*(66 + 122*t2 + 19*Power(t2,2)) - 
               Power(t1,5)*(280 - 29*t2 + 181*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(t1,2)*(54 + 135*t2 - 44*Power(t2,2) + 
                  3*Power(t2,3) - 84*Power(t2,4)) + 
               Power(t1,4)*(224 + 558*t2 - 446*Power(t2,2) + 
                  157*Power(t2,3) - 2*Power(t2,4)) - 
               Power(t1,3)*(153 + 351*t2 + 61*Power(t2,2) - 
                  350*Power(t2,3) + 68*Power(t2,4)) + 
               t1*(-6 + 31*t2 - 28*Power(t2,2) - 65*Power(t2,3) + 
                  68*Power(t2,4)) + 
               Power(s2,6)*(47 + 103*Power(t1,2) - 20*t2 + 
                  60*Power(t2,2) - t1*(137 + 379*t2)) + 
               Power(s2,5)*(15 - 135*Power(t1,3) - 109*t2 - 
                  11*Power(t2,2) + 38*Power(t2,3) + 
                  Power(t1,2)*(475 + 818*t2) + 
                  t1*(-222 + 17*t2 - 303*Power(t2,2))) + 
               Power(s2,4)*(42 + 75*Power(t1,4) + 177*t2 - 
                  142*Power(t2,2) - 85*Power(t2,3) + 30*Power(t2,4) - 
                  5*Power(t1,3)*(176 + 185*t2) + 
                  Power(t1,2)*(484 + 254*t2 + 631*Power(t2,2)) - 
                  t1*(231 - 391*t2 + 30*Power(t2,2) + 189*Power(t2,3))) \
+ Power(s2,3)*(38 + 19*Power(t1,5) + 440*t2 - 48*Power(t2,2) - 
                  331*Power(t2,3) + 56*Power(t2,4) + 
                  10*Power(t1,4)*(95 + 58*t2) - 
                  2*Power(t1,3)*(328 + 393*t2 + 347*Power(t2,2)) + 
                  Power(t1,2)*
                   (883 - 548*t2 + 337*Power(t2,2) + 348*Power(t2,3)) + 
                  t1*(-343 - 1092*t2 + 911*Power(t2,2) + 
                     83*Power(t2,3) - 88*Power(t2,4))) + 
               Power(s2,2)*(38 - 51*Power(t1,6) + 242*t2 - 
                  202*Power(t2,2) + 70*Power(t2,3) - 84*Power(t2,4) - 
                  Power(t1,5)*(601 + 197*t2) + 
                  Power(t1,4)*(579 + 964*t2 + 426*Power(t2,2)) - 
                  Power(t1,3)*
                   (1413 - 388*t2 + 721*Power(t2,2) + 290*Power(t2,3)) \
+ t1*(-243 - 1215*t2 + 41*Power(t2,2) + 1004*Power(t2,3) - 
                     180*Power(t2,4)) + 
                  Power(t1,2)*
                   (784 + 2211*t2 - 1842*Power(t2,2) + 
                     246*Power(t2,3) + 84*Power(t2,4))) + 
               s2*(6 + 27*Power(t1,7) + 5*t2 - 44*Power(t2,2) + 
                  101*Power(t2,3) - 68*Power(t2,4) + 
                  Power(t1,6)*(207 + 34*t2) - 
                  Power(t1,5)*(298 + 551*t2 + 139*Power(t2,2)) + 
                  Power(t1,4)*
                   (1026 - 151*t2 + 606*Power(t2,2) + 102*Power(t2,3)) - 
                  Power(t1,3)*
                   (707 + 1854*t2 - 1519*Power(t2,2) + 
                     401*Power(t2,3) + 24*Power(t2,4)) + 
                  t1*(-92 - 383*t2 + 258*Power(t2,2) - 79*Power(t2,3) + 
                     168*Power(t2,4)) + 
                  Power(t1,2)*
                   (358 + 1126*t2 + 68*Power(t2,2) - 1023*Power(t2,3) + 
                     192*Power(t2,4)))) + 
            s1*(-7*Power(s2,9) + Power(s2,8)*(-11 + 57*t1 - 42*t2) - 
               Power(t1,8)*(13 + 7*t2) - 
               2*Power(-1 + t2,2)*Power(t2,2)*(-15 + 8*t2) + 
               Power(t1,7)*(96 + 22*t2 + 23*Power(t2,2)) - 
               Power(t1,6)*(212 + 83*t2 + Power(t2,2) + 27*Power(t2,3)) + 
               Power(t1,5)*(70 + 472*t2 - 234*Power(t2,2) + 
                  4*Power(t2,3) + 13*Power(t2,4)) - 
               Power(t1,4)*(29 + 301*t2 + 190*Power(t2,2) - 
                  367*Power(t2,3) + 28*Power(t2,4) + 2*Power(t2,5)) - 
               t1*(8 - 40*t2 + 13*Power(t2,2) + 26*Power(t2,3) - 
                  19*Power(t2,4) + 12*Power(t2,5)) + 
               Power(t1,3)*(11 + 438*t2 - 335*Power(t2,2) + 
                  160*Power(t2,3) - 204*Power(t2,4) + 16*Power(t2,5)) + 
               Power(t1,2)*(23 - 282*t2 + 148*Power(t2,2) + 
                  137*Power(t2,3) - 110*Power(t2,4) + 58*Power(t2,5)) + 
               Power(s2,7)*(37 - 203*Power(t1,2) - 50*t2 - 
                  96*Power(t2,2) + t1*(63 + 302*t2)) + 
               Power(s2,6)*(-66 + 413*Power(t1,3) + 4*t2 + 
                  7*Power(t2,2) - 62*Power(t2,3) - 
                  Power(t1,2)*(160 + 937*t2) + 
                  t1*(-171 + 314*t2 + 572*Power(t2,2))) + 
               Power(s2,5)*(-162 - 525*Power(t1,4) + 44*t2 + 
                  67*Power(t2,2) + 66*Power(t2,3) - 11*Power(t2,4) + 
                  Power(t1,3)*(253 + 1632*t2) + 
                  Power(t1,2)*(204 - 842*t2 - 1443*Power(t2,2)) + 
                  t1*(568 - 96*t2 - 2*Power(t2,2) + 326*Power(t2,3))) + 
               Power(s2,4)*(45 + 427*Power(t1,5) - 197*t2 - 
                  22*Power(t2,2) + 162*Power(t2,3) + 92*Power(t2,4) - 
                  10*Power(t2,5) - 5*Power(t1,4)*(60 + 347*t2) + 
                  5*Power(t1,3)*(50 + 250*t2 + 399*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (608 - 87*t2 + 21*Power(t2,2) + 237*Power(t2,3)) + 
                  t1*(714 + 190*t2 - 439*Power(t2,2) - 308*Power(t2,3) + 
                     73*Power(t2,4))) + 
               Power(s2,2)*(31 + 63*Power(t1,7) - 250*t2 + 
                  21*Power(t2,2) + 295*Power(t2,3) - 189*Power(t2,4) + 
                  66*Power(t2,5) - Power(t1,6)*(188 + 447*t2) + 
                  Power(t1,5)*(993 + 590*t2 + 786*Power(t2,2)) - 
                  Power(t1,4)*
                   (2554 + 174*t2 + 103*Power(t2,2) + 536*Power(t2,3)) + 
                  Power(t1,3)*
                   (1056 + 2338*t2 - 1483*Power(t2,2) - 
                     384*Power(t2,3) + 170*Power(t2,4)) + 
                  Power(t1,2)*
                   (158 - 1706*t2 - 426*Power(t2,2) + 
                     1537*Power(t2,3) + 174*Power(t2,4) - 36*Power(t2,5)\
) + t1*(-111 + 1018*t2 - 327*Power(t2,2) - 60*Power(t2,3) - 
                     418*Power(t2,4) + 12*Power(t2,5))) + 
               Power(s2,3)*(53 - 217*Power(t1,6) - 262*t2 - 
                  33*Power(t2,2) + 114*Power(t2,3) + 112*Power(t2,4) + 
                  2*Power(t2,5) + Power(t1,5)*(281 + 1142*t2) - 
                  5*Power(t1,4)*(183 + 222*t2 + 326*Power(t2,2)) + 
                  4*Power(t1,3)*
                   (734 - 41*t2 + 33*Power(t2,2) + 206*Power(t2,3)) + 
                  Power(t1,2)*
                   (-1240 - 1306*t2 + 1149*Power(t2,2) + 
                     524*Power(t2,3) - 166*Power(t2,4)) + 
                  t1*(-161 + 998*t2 + 151*Power(t2,2) - 
                     828*Power(t2,3) - 239*Power(t2,4) + 32*Power(t2,5))) \
+ s2*(8 - 8*Power(t1,8) - 40*t2 - 23*Power(t2,2) + 106*Power(t2,3) - 
                  71*Power(t2,4) + 20*Power(t2,5) + 
                  Power(t1,7)*(75 + 92*t2) - 
                  Power(t1,6)*(494 + 174*t2 + 207*Power(t2,2)) + 
                  6*Power(t1,5)*
                   (192 + 42*t2 + 5*Power(t2,2) + 31*Power(t2,3)) + 
                  Power(t1,4)*
                   (-438 - 1738*t2 + 940*Power(t2,2) + 98*Power(t2,3) - 
                     79*Power(t2,4)) + 
                  Power(t1,2)*
                   (47 - 1194*t2 + 695*Power(t2,2) - 214*Power(t2,3) + 
                     510*Power(t2,4) - 30*Power(t2,5)) + 
                  Power(t1,3)*
                   (-13 + 1206*t2 + 487*Power(t2,2) - 1238*Power(t2,3) + 
                     Power(t2,4) + 16*Power(t2,5)) - 
                  t1*(54 - 532*t2 + 163*Power(t2,2) + 444*Power(t2,3) - 
                     305*Power(t2,4) + 124*Power(t2,5))))))*
       R1(1 - s + s2 - t1))/
     ((-1 + s2)*Power(-s + s2 - t1,3)*(1 - s + s2 - t1)*(s - s1 + t2)*
       (-1 + s2 - t1 + t2)*Power(-s1 + s2 - t1 + t2,3)*
       (-1 + s - s*s1 + s1*s2 - s1*t1 + t2)) + 
    (16*(Power(s1,8)*(s2 - t1)*
          (-3 + 5*Power(s2,2) - 7*t1 + 3*t2 + 2*t1*t2 + 
            s2*(2 - 5*t1 + 3*t2)) + 
         Power(s1,7)*(9*Power(s2,4) + Power(t1,4) + 
            Power(s2,3)*(11 - 23*t1 - 20*t2) + Power(t1,3)*(15 - 4*t2) + 
            3*Power(-1 + t2,2) + 
            Power(t1,2)*(-4 - 43*t2 + 11*Power(t2,2)) + 
            t1*(3 - 23*t2 + 20*Power(t2,2)) + 
            Power(s2,2)*(-35 + 20*Power(t1,2) + 16*t2 - 
               17*Power(t2,2) + t1*(-12 + 41*t2)) + 
            s2*(2 - 7*Power(t1,3) + 13*t2 - 15*Power(t2,2) - 
               Power(t1,2)*(14 + 17*t2) + 
               3*t1*(13 + 9*t2 + 2*Power(t2,2)))) + 
         Power(s1,6)*(7*Power(s2,5) + Power(s2,4)*(9 - 17*t1 - 32*t2) - 
            Power(t1,4)*(11 + t2) - 2*Power(-1 + t2,2)*(2 + 9*t2) + 
            Power(t1,3)*(3 - 39*t2 + 13*Power(t2,2)) + 
            t1*(-36 + 22*t2 + 69*Power(t2,2) - 55*Power(t2,3)) + 
            Power(t1,2)*(-2 + 7*t2 + 98*Power(t2,2) - 24*Power(t2,3)) + 
            Power(s2,3)*(-39 + 9*Power(t1,2) - 20*t2 + 33*Power(t2,2) + 
               t1*(-5 + 84*t2)) + 
            Power(s2,2)*(-35 + 5*Power(t1,3) + 166*t2 - 
               94*Power(t2,2) + 42*Power(t2,3) - 
               Power(t1,2)*(28 + 73*t2) + 
               t1*(56 + 48*t2 - 75*Power(t2,2))) + 
            s2*(62 - 4*Power(t1,4) - 102*t2 + 13*Power(t2,2) + 
               27*Power(t2,3) + Power(t1,3)*(35 + 22*t2) + 
               Power(t1,2)*(-20 + 11*t2 + 29*Power(t2,2)) + 
               t1*(44 - 187*t2 + 3*Power(t2,2) - 18*Power(t2,3)))) + 
         Power(s1,5)*(Power(s2,6) - 2*Power(t1,6) + 
            Power(s2,5)*(-6 + 4*t1 - 33*t2) + 
            2*Power(t1,5)*(-16 + 5*t2) + 
            Power(t1,4)*(-54 + 123*t2 - 17*Power(t2,2)) + 
            Power(-1 + t2,2)*(-32 + 29*t2 + 44*Power(t2,2)) - 
            Power(t1,3)*(68 - 148*t2 + 55*Power(t2,2) + 9*Power(t2,3)) + 
            Power(t1,2)*(9 + 90*t2 - 109*Power(t2,2) - 87*Power(t2,3) + 
               26*Power(t2,4)) + 
            t1*(-7 + 155*t2 - 151*Power(t2,2) - 77*Power(t2,3) + 
               80*Power(t2,4)) + 
            Power(s2,4)*(8 - 28*Power(t1,2) - 33*t2 + 33*Power(t2,2) + 
               t1*(29 + 106*t2)) + 
            Power(s2,3)*(-125 + 52*Power(t1,3) + 232*t2 - 
               36*Power(t2,2) - 31*Power(t2,3) - 
               Power(t1,2)*(19 + 130*t2) - 
               4*t1*(3 - t2 + 19*Power(t2,2))) + 
            Power(s2,2)*(160 - 43*Power(t1,4) - 83*t2 - 
               267*Power(t2,2) + 177*Power(t2,3) - 58*Power(t2,4) + 
               Power(t1,3)*(-57 + 84*t2) + 
               Power(t1,2)*(-54 + 214*t2 + 36*Power(t2,2)) + 
               t1*(157 - 215*t2 - 100*Power(t2,2) + 94*Power(t2,3))) + 
            s2*(12 + 16*Power(t1,5) + Power(t1,4)*(85 - 37*t2) - 
               269*t2 + 430*Power(t2,2) - 159*Power(t2,3) - 
               14*Power(t2,4) + 
               4*Power(t1,3)*(28 - 77*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(36 - 165*t2 + 191*Power(t2,2) - 
                  54*Power(t2,3)) + 
               t1*(-148 - 80*t2 + 459*Power(t2,2) - 121*Power(t2,3) + 
                  32*Power(t2,4)))) + 
         Power(s1,4)*(Power(s2,6)*(t1 - 9*t2) + 
            Power(t1,6)*(-17 + 6*t2) - 
            5*Power(t1,5)*(25 - 32*t2 + 6*Power(t2,2)) + 
            Power(t1,4)*(-129 + 510*t2 - 390*Power(t2,2) + 
               47*Power(t2,3)) - 
            Power(-1 + t2,2)*
             (-12 - 122*t2 + 97*Power(t2,2) + 56*Power(t2,3)) + 
            Power(t1,3)*(-66 + 546*t2 - 835*Power(t2,2) + 
               305*Power(t2,3) - 11*Power(t2,4)) + 
            Power(t1,2)*(10 + 176*t2 - 638*Power(t2,2) + 
               525*Power(t2,3) - 18*Power(t2,4) - 14*Power(t2,5)) - 
            t1*(-111 + 108*t2 + 345*Power(t2,2) - 445*Power(t2,3) + 
               38*Power(t2,4) + 65*Power(t2,5)) + 
            Power(s2,5)*(-35 - 5*Power(t1,2) + 13*t2 + 46*Power(t2,2) + 
               2*t1*(17 + 9*t2)) + 
            Power(s2,4)*(199 + 10*Power(t1,3) - 85*t2 + 
               21*Power(t2,2) - 7*Power(t2,3) + 
               3*Power(t1,2)*(-51 + 8*t2) + 
               t1*(29 + 23*t2 - 170*Power(t2,2))) + 
            Power(s2,3)*(-317 - 10*Power(t1,4) + 
               Power(t1,3)*(272 - 96*t2) + 786*t2 - 535*Power(t2,2) + 
               111*Power(t2,3) + 15*Power(t2,4) + 
               Power(t1,2)*(248 - 307*t2 + 264*Power(t2,2)) + 
               t1*(-588 + 9*t2 + 173*Power(t2,2) - 14*Power(t2,3))) + 
            Power(s2,2)*(364 + 5*Power(t1,5) - 1125*t2 + 
               788*Power(t2,2) + 126*Power(t2,3) - 159*Power(t2,4) + 
               47*Power(t2,5) + Power(t1,4)*(-238 + 99*t2) + 
               Power(t1,3)*(-568 + 653*t2 - 232*Power(t2,2)) + 
               Power(t1,2)*(450 + 747*t2 - 799*Power(t2,2) + 
                  96*Power(t2,3)) + 
               t1*(643 - 1131*t2 + 184*Power(t2,2) + 206*Power(t2,3) - 
                  83*Power(t2,4))) - 
            s2*(235 + Power(t1,6) - 453*t2 - 170*Power(t2,2) + 
               718*Power(t2,3) - 349*Power(t2,4) + 19*Power(t2,5) + 
               6*Power(t1,5)*(-17 + 7*t2) + 
               Power(t1,4)*(-451 + 542*t2 - 122*Power(t2,2)) + 
               Power(t1,3)*(-68 + 1181*t2 - 995*Power(t2,2) + 
                  122*Power(t2,3)) + 
               Power(t1,2)*(260 + 201*t2 - 1186*Power(t2,2) + 
                  622*Power(t2,3) - 79*Power(t2,4)) + 
               t1*(369 - 880*t2 - 31*Power(t2,2) + 826*Power(t2,3) - 
                  235*Power(t2,4) + 33*Power(t2,5)))) + 
         Power(s1,3)*(2*Power(s2,8) + Power(t1,8) + 
            Power(t1,7)*(1 - 6*t2) + Power(s2,7)*(3 - 15*t1 + 11*t2) + 
            Power(t1,6)*(-55 + 42*t2 + 6*Power(t2,2)) + 
            Power(t1,5)*(-152 + 478*t2 - 267*Power(t2,2) + 
               20*Power(t2,3)) + 
            Power(t1,4)*(-252 + 866*t2 - 1276*Power(t2,2) + 
               529*Power(t2,3) - 41*Power(t2,4)) + 
            Power(-1 + t2,2)*
             (105 - 136*t2 - 182*Power(t2,2) + 177*Power(t2,3) + 
               39*Power(t2,4)) + 
            Power(t1,3)*(-20 + 756*t2 - 1831*Power(t2,2) + 
               1640*Power(t2,3) - 420*Power(t2,4) + 17*Power(t2,5)) + 
            Power(t1,2)*(184 - 69*t2 - 1040*Power(t2,2) + 
               1756*Power(t2,3) - 964*Power(t2,4) + 94*Power(t2,5) + 
               3*Power(t2,6)) + 
            t1*(156 - 568*t2 + 291*Power(t2,2) + 739*Power(t2,3) - 
               824*Power(t2,4) + 178*Power(t2,5) + 28*Power(t2,6)) + 
            Power(s2,6)*(-50 + 49*Power(t1,2) + 4*t2 + 38*Power(t2,2) - 
               5*t1*(1 + 15*t2)) - 
            Power(s2,5)*(74 + 91*Power(t1,3) + 26*t2 + 5*Power(t2,2) + 
               Power(t2,3) - 3*Power(t1,2)*(-7 + 72*t2) + 
               2*t1*(-160 + 37*t2 + 89*Power(t2,2))) + 
            Power(s2,4)*(440 + 105*Power(t1,4) + 
               Power(t1,3)*(75 - 340*t2) - 763*t2 + 75*Power(t2,2) + 
               37*Power(t2,3) + 9*Power(t2,4) + 
               Power(t1,2)*(-835 + 298*t2 + 338*Power(t2,2)) + 
               t1*(80 + 618*t2 - 172*Power(t2,2) - 8*Power(t2,3))) + 
            Power(s2,3)*(-659 - 77*Power(t1,5) + 1732*t2 - 
               1683*Power(t2,2) + 545*Power(t2,3) - 93*Power(t2,4) + 
               6*Power(t2,5) + 5*Power(t1,4)*(-19 + 63*t2) - 
               4*Power(t1,3)*(-285 + 133*t2 + 83*Power(t2,2)) + 
               Power(t1,2)*(356 - 2176*t2 + 813*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(-1139 + 1762*t2 + 603*Power(t2,2) - 
                  437*Power(t2,3) - 6*Power(t2,4))) + 
            Power(s2,2)*(555 + 35*Power(t1,6) - 2153*t2 + 
               2972*Power(t2,2) - 1544*Power(t2,3) + 88*Power(t2,4) + 
               67*Power(t2,5) - 21*Power(t2,6) - 
               57*Power(t1,5)*(-1 + 3*t2) + 
               2*Power(t1,4)*(-430 + 244*t2 + 89*Power(t2,2)) + 
               Power(t1,3)*(-808 + 3080*t2 - 1357*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,2)*(706 - 369*t2 - 2707*Power(t2,2) + 
                  1292*Power(t2,3) - 56*Power(t2,4)) + 
               t1*(1459 - 3260*t2 + 2141*Power(t2,2) + 
                  370*Power(t2,3) - 294*Power(t2,4) + 30*Power(t2,5))) + 
            s2*(-328 - 9*Power(t1,7) + 1535*t2 - 2149*Power(t2,2) + 
               707*Power(t2,3) + 559*Power(t2,4) - 357*Power(t2,5) + 
               33*Power(t2,6) + 5*Power(t1,6)*(-3 + 10*t2) + 
               Power(t1,5)*(340 - 226*t2 - 50*Power(t2,2)) + 
               Power(t1,4)*(598 - 1974*t2 + 988*Power(t2,2) - 
                  49*Power(t2,3)) + 
               Power(t1,3)*(245 - 1496*t2 + 3305*Power(t2,2) - 
                  1421*Power(t2,3) + 94*Power(t2,4)) + 
               Power(t1,2)*(-780 + 772*t2 + 1373*Power(t2,2) - 
                  2555*Power(t2,3) + 807*Power(t2,4) - 53*Power(t2,5)) + 
               t1*(-788 + 2369*t2 - 2025*Power(t2,2) - 331*Power(t2,3) + 
                  1050*Power(t2,4) - 221*Power(t2,5) + 18*Power(t2,6)))) \
+ (-1 + t2)*(Power(t1,7)*(-4 + 7*t2 - 2*Power(t2,2)) - 
            Power(s2,7)*(-2 + 5*t2 - 3*Power(t2,2) + Power(t2,3)) + 
            Power(t1,6)*(15 - 11*t2 - 20*Power(t2,2) + 9*Power(t2,3)) + 
            Power(t1,5)*(-12 - 51*t2 + 88*Power(t2,2) + 9*Power(t2,3) - 
               15*Power(t2,4)) + 
            Power(t1,4)*(26 - 22*t2 + 135*Power(t2,2) - 
               212*Power(t2,3) + 36*Power(t2,4) + 11*Power(t2,5)) + 
            t2*(12 - 118*t2 + 293*Power(t2,2) - 285*Power(t2,3) + 
               84*Power(t2,4) + 34*Power(t2,5) - 20*Power(t2,6)) + 
            Power(t1,3)*(52 - 231*t2 + 312*Power(t2,2) - 
               340*Power(t2,3) + 293*Power(t2,4) - 64*Power(t2,5) - 
               3*Power(t2,6)) + 
            Power(t1,2)*(-53 + 91*t2 + 127*Power(t2,2) - 
               384*Power(t2,3) + 409*Power(t2,4) - 239*Power(t2,5) + 
               42*Power(t2,6)) + 
            t1*(-48 + 285*t2 - 556*Power(t2,2) + 399*Power(t2,3) + 
               34*Power(t2,4) - 208*Power(t2,5) + 105*Power(t2,6) - 
               10*Power(t2,7)) + 
            Power(s2,6)*(9 - 8*t2 - 18*Power(t2,2) + 11*Power(t2,3) - 
               Power(t2,4) + 
               t1*(-15 + 34*t2 - 17*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s2,5)*(-12 + 78*t2 - 88*Power(t2,2) - 
               17*Power(t2,3) + 19*Power(t2,4) + Power(t2,5) + 
               Power(t1,2)*(49 - 102*t2 + 42*Power(t2,2) - 
                  10*Power(t2,3)) + 
               t1*(-49 + 28*t2 + 122*Power(t2,2) - 63*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s2,4)*(-6 + 33*t2 + 82*Power(t2,2) - 
               159*Power(t2,3) + 2*Power(t2,4) + 21*Power(t2,5) + 
               Power(t2,6) + 
               5*Power(t1,3)*
                (-18 + 35*t2 - 12*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(121 - 43*t2 - 328*Power(t2,2) + 
                  151*Power(t2,3) - 6*Power(t2,4)) + 
               t1*(35 - 333*t2 + 376*Power(t2,2) + 120*Power(t2,3) - 
                  100*Power(t2,4) - 3*Power(t2,5))) + 
            Power(s2,2)*(-113 + 463*t2 - 804*Power(t2,2) + 
               793*Power(t2,3) - 368*Power(t2,4) + 11*Power(t2,5) + 
               5*Power(t2,6) + 6*Power(t2,7) + 
               Power(t1,5)*(-67 + 120*t2 - 33*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(t1,4)*(-151 + 58*t2 + 338*Power(t2,2) - 
                  141*Power(t2,3) + Power(t2,4)) - 
               Power(t1,3)*(27 + 528*t2 - 688*Power(t2,2) - 
                  251*Power(t2,3) + 193*Power(t2,4) + Power(t2,5)) + 
               t1*(-80 + 290*t2 - 571*Power(t2,2) + 78*Power(t2,3) + 
                  456*Power(t2,4) - 81*Power(t2,5) - 35*Power(t2,6)) + 
               Power(t1,2)*(106 - 105*t2 + 747*Power(t2,2) - 
                  1061*Power(t2,3) + 42*Power(t2,4) + 114*Power(t2,5) + 
                  Power(t2,6))) - 
            Power(s2,3)*(-72 + 280*t2 - 459*Power(t2,2) + 
               203*Power(t2,3) + 98*Power(t2,4) - 15*Power(t2,5) - 
               16*Power(t2,6) + 
               5*Power(t1,4)*
                (-20 + 37*t2 - 11*Power(t2,2) + Power(t2,3)) - 
               2*Power(t1,3)*
                (-87 + 26*t2 + 226*Power(t2,2) - 97*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,2)*(21 - 582*t2 + 688*Power(t2,2) + 
                  267*Power(t2,3) - 201*Power(t2,4) - 3*Power(t2,5)) + 
               t1*(31 + 8*t2 + 429*Power(t2,2) - 663*Power(t2,3) + 
                  6*Power(t2,4) + 83*Power(t2,5) + 2*Power(t2,6))) + 
            s2*(48 - 273*t2 + 612*Power(t2,2) - 725*Power(t2,3) + 
               499*Power(t2,4) - 172*Power(t2,5) + 8*Power(t2,6) + 
               2*Power(t2,7) + 
               Power(t1,6)*(25 - 44*t2 + 12*Power(t2,2)) + 
               Power(t1,5)*(-73 + 40*t2 + 130*Power(t2,2) - 
                  55*Power(t2,3)) + 
               Power(t1,4)*(37 + 252*t2 - 376*Power(t2,2) - 
                  96*Power(t2,3) + 88*Power(t2,4)) - 
               Power(t1,3)*(95 - 102*t2 + 535*Power(t2,2) - 
                  769*Power(t2,3) + 74*Power(t2,4) + 63*Power(t2,5)) + 
               Power(t1,2)*(-44 + 221*t2 - 200*Power(t2,2) + 
                  465*Power(t2,3) - 651*Power(t2,4) + 130*Power(t2,5) + 
                  22*Power(t2,6)) + 
               t1*(166 - 578*t2 + 775*Power(t2,2) - 564*Power(t2,3) + 
                  74*Power(t2,4) + 193*Power(t2,5) - 48*Power(t2,6) - 
                  4*Power(t2,7)))) - 
         Power(s1,2)*(Power(t1,8)*(-5 + 3*t2) + 
            Power(s2,8)*(-5 + 4*t2) - 
            3*Power(t1,7)*(-5 - 6*t2 + 5*Power(t2,2)) + 
            Power(s2,7)*(-14 + t1*(40 - 31*t2) - 18*t2 + 
               23*Power(t2,2)) + 
            Power(t1,6)*(21 - 131*t2 + 12*Power(t2,2) + 
               25*Power(t2,3)) + 
            Power(t1,5)*(127 - 373*t2 + 576*Power(t2,2) - 
               165*Power(t2,3) - 11*Power(t2,4)) + 
            Power(t1,4)*(272 - 1063*t2 + 1565*Power(t2,2) - 
               1251*Power(t2,3) + 315*Power(t2,4) - 8*Power(t2,5)) + 
            Power(-1 + t2,2)*
             (-80 + 389*t2 - 340*Power(t2,2) - 140*Power(t2,3) + 
               179*Power(t2,4) + 14*Power(t2,5)) + 
            Power(t1,3)*(25 - 692*t2 + 2224*Power(t2,2) - 
               2662*Power(t2,3) + 1438*Power(t2,4) - 247*Power(t2,5) + 
               6*Power(t2,6)) + 
            Power(t1,2)*(-278 + 606*t2 + 403*Power(t2,2) - 
               2229*Power(t2,3) + 2232*Power(t2,4) - 820*Power(t2,5) + 
               67*Power(t2,6)) + 
            t1*(-213 + 885*t2 - 1188*Power(t2,2) + 39*Power(t2,3) + 
               1204*Power(t2,4) - 904*Power(t2,5) + 172*Power(t2,6) + 
               5*Power(t2,7)) + 
            Power(s2,6)*(61 - 134*t2 - 30*Power(t2,2) + 
               54*Power(t2,3) + 35*Power(t1,2)*(-4 + 3*t2) + 
               t1*(79 + 148*t2 - 155*Power(t2,2))) + 
            Power(s2,5)*(12 + 44*t2 - 195*Power(t2,2) - 
               25*Power(t2,3) + 38*Power(t2,4) - 
               7*Power(t1,3)*(-40 + 29*t2) + 
               Power(t1,2)*(-200 - 488*t2 + 445*Power(t2,2)) + 
               t1*(-322 + 762*t2 + 171*Power(t2,2) - 293*Power(t2,3))) + 
            Power(s2,4)*(-370 + 939*t2 - 680*Power(t2,2) - 
               95*Power(t2,3) + 29*Power(t2,4) + 22*Power(t2,5) + 
               35*Power(t1,4)*(-10 + 7*t2) + 
               Power(t1,3)*(305 + 850*t2 - 705*Power(t2,2)) + 
               3*Power(t1,2)*
                (233 - 613*t2 - 124*Power(t2,2) + 219*Power(t2,3)) + 
               t1*(155 - 691*t2 + 1381*Power(t2,2) - 2*Power(t2,3) - 
                  185*Power(t2,4))) + 
            Power(s2,3)*(764 - 2426*t2 + 2763*Power(t2,2) - 
               1367*Power(t2,3) + 211*Power(t2,4) - 47*Power(t2,5) + 
               13*Power(t2,6) - 7*Power(t1,5)*(-40 + 27*t2) + 
               5*Power(t1,4)*(-62 - 170*t2 + 133*Power(t2,2)) + 
               Power(t1,3)*(-796 + 2416*t2 + 378*Power(t2,2) - 
                  778*Power(t2,3)) + 
               Power(t1,2)*(-664 + 2182*t2 - 3549*Power(t2,2) + 
                  321*Power(t2,3) + 338*Power(t2,4)) + 
               t1*(790 - 1767*t2 + 747*Power(t2,2) + 1216*Power(t2,3) - 
                  277*Power(t2,4) - 74*Power(t2,5))) + 
            Power(s2,2)*(-641 + 2712*t2 - 4565*Power(t2,2) + 
               3611*Power(t2,3) - 1238*Power(t2,4) + 107*Power(t2,5) - 
               Power(t2,6) - 4*Power(t2,7) + 
               7*Power(t1,6)*(-20 + 13*t2) + 
               Power(t1,5)*(209 + 488*t2 - 373*Power(t2,2)) + 
               Power(t1,4)*(499 - 1824*t2 - 162*Power(t2,2) + 
                  512*Power(t2,3)) + 
               Power(t1,3)*(942 - 2840*t2 + 4311*Power(t2,2) - 
                  701*Power(t2,3) - 284*Power(t2,4)) + 
               Power(t1,2)*(-198 - 346*t2 + 2111*Power(t2,2) - 
                  3398*Power(t2,3) + 782*Power(t2,4) + 74*Power(t2,5)) \
+ t1*(-1583 + 4645*t2 - 4278*Power(t2,2) + 894*Power(t2,3) + 
                  771*Power(t2,4) - 167*Power(t2,5) - 12*Power(t2,6))) + 
            s2*(265 - 1571*t2 + 3561*Power(t2,2) - 3609*Power(t2,3) + 
               1338*Power(t2,4) + 180*Power(t2,5) - 183*Power(t2,6) + 
               19*Power(t2,7) - 5*Power(t1,7)*(-8 + 5*t2) + 
               Power(t1,6)*(-84 - 148*t2 + 115*Power(t2,2)) - 
               3*Power(t1,5)*
                (54 - 250*t2 - Power(t2,2) + 59*Power(t2,3)) + 
               2*Power(t1,4)*
                (-286 + 839*t2 - 1262*Power(t2,2) + 286*Power(t2,3) + 
                  52*Power(t2,4)) - 
               Power(t1,3)*(494 - 2237*t2 + 3743*Power(t2,2) - 
                  3528*Power(t2,3) + 849*Power(t2,4) + 14*Power(t2,5)) + 
               Power(t1,2)*(794 - 1527*t2 - 709*Power(t2,2) + 
                  3135*Power(t2,3) - 2420*Power(t2,4) + 
                  461*Power(t2,5) - 7*Power(t2,6)) + 
               t1*(983 - 3637*t2 + 4753*Power(t2,2) - 1849*Power(t2,3) - 
                  914*Power(t2,4) + 801*Power(t2,5) - 103*Power(t2,6) + 
                  4*Power(t2,7)))) + 
         s1*(Power(t1,8)*(3 - 6*t2 + 2*Power(t2,2)) + 
            Power(s2,8)*(2 - 5*t2 + 2*Power(t2,2)) + 
            Power(t1,7)*(-7 + t2 + 22*Power(t2,2) - 9*Power(t2,3)) + 
            Power(t1,6)*(-3 + 44*t2 - 50*Power(t2,2) - 25*Power(t2,3) + 
               15*Power(t2,4)) - 
            Power(t1,5)*(56 - 193*t2 + 298*Power(t2,2) - 
               208*Power(t2,3) + 10*Power(t2,4) + 11*Power(t2,5)) + 
            Power(t1,3)*(-72 + 508*t2 - 1466*Power(t2,2) + 
               2199*Power(t2,3) - 1655*Power(t2,4) + 543*Power(t2,5) - 
               50*Power(t2,6)) + 
            Power(-1 + t2,2)*
             (12 - 186*t2 + 471*Power(t2,2) - 314*Power(t2,3) - 
               62*Power(t2,4) + 94*Power(t2,5) + 2*Power(t2,6)) + 
            Power(t1,4)*(-126 + 592*t2 - 1130*Power(t2,2) + 
               1049*Power(t2,3) - 460*Power(t2,4) + 53*Power(t2,5) + 
               3*Power(t2,6)) + 
            Power(t1,2)*(158 - 452*t2 + 63*Power(t2,2) + 
               1260*Power(t2,3) - 2036*Power(t2,4) + 1307*Power(t2,5) - 
               317*Power(t2,6) + 16*Power(t2,7)) + 
            t1*(187 - 910*t2 + 1648*Power(t2,2) - 1112*Power(t2,3) - 
               373*Power(t2,4) + 1004*Power(t2,5) - 514*Power(t2,6) + 
               70*Power(t2,7)) + 
            Power(s2,7)*(4 + t2 - 25*Power(t2,2) + 13*Power(t2,3) + 
               t1*(-17 + 41*t2 - 16*Power(t2,2))) + 
            Power(s2,6)*(-28 + 78*t2 - 48*Power(t2,2) - 
               46*Power(t2,3) + 25*Power(t2,4) + 
               7*Power(t1,2)*(9 - 21*t2 + 8*Power(t2,2)) - 
               t1*(23 + 20*t2 - 178*Power(t2,2) + 86*Power(t2,3))) + 
            Power(s2,5)*(52 - 167*t2 + 213*Power(t2,2) - 
               97*Power(t2,3) - 45*Power(t2,4) + 18*Power(t2,5) - 
               7*Power(t1,3)*(19 - 43*t2 + 16*Power(t2,2)) + 
               Power(t1,2)*(62 + 84*t2 - 537*Power(t2,2) + 
                  244*Power(t2,3)) + 
               t1*(123 - 370*t2 + 221*Power(t2,2) + 281*Power(t2,3) - 
                  141*Power(t2,4))) + 
            Power(s2,4)*(134 - 352*t2 + 238*Power(t2,2) + 
               52*Power(t2,3) - 75*Power(t2,4) - 25*Power(t2,5) + 
               9*Power(t2,6) + 
               35*Power(t1,4)*(5 - 11*t2 + 4*Power(t2,2)) - 
               5*Power(t1,3)*
                (21 + 31*t2 - 178*Power(t2,2) + 77*Power(t2,3)) + 
               Power(t1,2)*(-215 + 744*t2 - 454*Power(t2,2) - 
                  689*Power(t2,3) + 329*Power(t2,4)) + 
               t1*(-287 + 887*t2 - 1107*Power(t2,2) + 514*Power(t2,3) + 
                  216*Power(t2,4) - 93*Power(t2,5))) + 
            Power(s2,3)*(-409 + 1659*t2 - 2435*Power(t2,2) + 
               1433*Power(t2,3) - 233*Power(t2,4) + 9*Power(t2,5) - 
               36*Power(t2,6) + 5*Power(t2,7) - 
               7*Power(t1,5)*(21 - 45*t2 + 16*Power(t2,2)) + 
               5*Power(t1,4)*
                (24 + 29*t2 - 175*Power(t2,2) + 73*Power(t2,3)) + 
               Power(t1,3)*(190 - 836*t2 + 566*Power(t2,2) + 
                  866*Power(t2,3) - 406*Power(t2,4)) + 
               Power(t1,2)*(605 - 1852*t2 + 2341*Power(t2,2) - 
                  1168*Power(t2,3) - 368*Power(t2,4) + 182*Power(t2,5)) \
+ t1*(-206 + 269*t2 + 528*Power(t2,2) - 1091*Power(t2,3) + 
                  544*Power(t2,4) + 69*Power(t2,5) - 37*Power(t2,6))) + 
            Power(s2,2)*(406 - 1934*t2 + 3858*Power(t2,2) - 
               3952*Power(t2,3) + 1977*Power(t2,4) - 370*Power(t2,5) + 
               30*Power(t2,6) - 16*Power(t2,7) + 
               7*Power(t1,6)*(11 - 23*t2 + 8*Power(t2,2)) - 
               Power(t1,5)*(89 + 66*t2 - 510*Power(t2,2) + 
                  208*Power(t2,3)) + 
               Power(t1,4)*(-90 + 574*t2 - 464*Power(t2,2) - 
                  584*Power(t2,3) + 279*Power(t2,4)) + 
               Power(t1,3)*(-613 + 1904*t2 - 2511*Power(t2,2) + 
                  1390*Power(t2,3) + 258*Power(t2,4) - 168*Power(t2,5)) \
+ Power(t1,2)*(-116 + 1110*t2 - 2900*Power(t2,2) + 3075*Power(t2,3) - 
                  1323*Power(t2,4) - 10*Power(t2,5) + 50*Power(t2,6)) + 
               t1*(733 - 2856*t2 + 3717*Power(t2,2) - 
                  1207*Power(t2,3) - 799*Power(t2,4) + 
                  422*Power(t2,5) + 20*Power(t2,6) - 9*Power(t2,7))) + 
            s2*(-175 + 1006*t2 - 2544*Power(t2,2) + 3501*Power(t2,3) - 
               2602*Power(t2,4) + 848*Power(t2,5) + 3*Power(t2,6) - 
               41*Power(t2,7) + 4*Power(t2,8) + 
               Power(t1,7)*(-23 + 47*t2 - 16*Power(t2,2)) + 
               Power(t1,6)*(38 + 10*t2 - 163*Power(t2,2) + 
                  66*Power(t2,3)) + 
               Power(t1,5)*(23 - 234*t2 + 229*Power(t2,2) + 
                  197*Power(t2,3) - 101*Power(t2,4)) + 
               Power(t1,4)*(299 - 965*t2 + 1362*Power(t2,2) - 
                  847*Power(t2,3) - 51*Power(t2,4) + 72*Power(t2,5)) + 
               Power(t1,3)*(314 - 1619*t2 + 3264*Power(t2,2) - 
                  3085*Power(t2,3) + 1314*Power(t2,4) - 
                  87*Power(t2,5) - 25*Power(t2,6)) + 
               Power(t1,2)*(-252 + 689*t2 + 184*Power(t2,2) - 
                  2425*Power(t2,3) + 2687*Power(t2,4) - 
                  974*Power(t2,5) + 66*Power(t2,6) + 4*Power(t2,7)) - 
               t1*(588 - 2572*t2 + 4444*Power(t2,2) - 3401*Power(t2,3) + 
                  417*Power(t2,4) + 819*Power(t2,5) - 310*Power(t2,6) + 
                  13*Power(t2,7)))) + 
         Power(s,7)*(-6 + Power(s2,3) + Power(t1,2) - Power(t1,3) + 
            2*Power(t1,4) - 2*t1*t2 + 3*Power(t1,2)*t2 - 
            8*Power(t1,3)*t2 + Power(t2,2) - 3*t1*Power(t2,2) + 
            12*Power(t1,2)*Power(t2,2) + Power(t2,3) - 
            8*t1*Power(t2,3) + 2*Power(t2,4) - 
            Power(s1,3)*(-1 + s2 - t1 + t2) + 
            Power(s2,2)*(-7 + 2*Power(t1,2) + t2 + 2*Power(t2,2) - 
               t1*(1 + 4*t2)) + 
            Power(s1,2)*(-7 - 4*Power(s2,2) + 4*Power(t1,2) + t2 + 
               4*Power(t2,2) + s2*(11 - 2*t1 + 2*t2) - t1*(1 + 8*t2)) + 
            s2*(12 - 4*Power(t1,3) + Power(t2,2) + 4*Power(t2,3) - 
               2*t1*t2*(1 + 6*t2) + Power(t1,2)*(1 + 12*t2)) - 
            s1*(-12 + Power(s2,3) - 5*Power(t1,3) + 3*Power(t2,2) + 
               5*Power(t2,3) + Power(s2,2)*(-11 - t1 + t2) + 
               3*Power(t1,2)*(1 + 5*t2) - 3*t1*t2*(2 + 5*t2) + 
               s2*(22 + 5*Power(t1,2) + 2*t2 + 5*Power(t2,2) - 
                  2*t1*(1 + 5*t2)))) + 
         Power(s,6)*(18 - 4*Power(s2,4) - 35*t1 - 14*Power(t1,2) + 
            11*Power(t1,3) - 9*Power(t1,4) + 5*Power(t1,5) - 7*t2 + 
            32*t1*t2 - 24*Power(t1,2)*t2 + 27*Power(t1,3)*t2 - 
            15*Power(t1,4)*t2 - 18*Power(t2,2) + 15*t1*Power(t2,2) - 
            27*Power(t1,2)*Power(t2,2) + 10*Power(t1,3)*Power(t2,2) - 
            2*Power(t2,3) + 9*t1*Power(t2,3) + 
            10*Power(t1,2)*Power(t2,3) - 15*t1*Power(t2,4) + 
            5*Power(t2,5) + 5*Power(s1,4)*(-1 + s2 - t1 + t2) + 
            Power(s1,3)*(21 + 26*Power(s2,2) - 17*Power(t1,2) + 9*t2 + 
               39*t1*t2 - 22*Power(t2,2) - s2*(47 + t1 + 8*t2)) + 
            Power(s2,3)*(23 - 5*Power(t1,2) - 8*t2 - 9*Power(t2,2) + 
               t1*(13 + 14*t2)) + 
            Power(s1,2)*(-16 + 18*Power(s2,3) - 14*Power(t1,3) - 
               33*t2 - 5*Power(t2,2) + 34*Power(t2,3) - 
               Power(s2,2)*(97 + 6*t1 + 22*t2) + 
               2*Power(t1,2)*(-7 + 31*t2) + 
               t1*(-22 + 19*t2 - 82*Power(t2,2)) + 
               s2*(95 + 2*Power(t1,2) + t1*(61 - 10*t2) + 22*t2 + 
                  8*Power(t2,2))) + 
            Power(s2,2)*(-36 + 15*Power(t1,3) - 13*t2 - 
               18*Power(t2,2) - 13*Power(t2,3) - 
               Power(t1,2)*(23 + 43*t2) + 
               t1*(-30 + 41*t2 + 41*Power(t2,2))) + 
            s2*(-1 - 15*Power(t1,4) + 12*t2 + 4*Power(t2,2) - 
               14*Power(t2,3) + Power(t2,4) + 
               Power(t1,3)*(23 + 44*t2) - 
               2*Power(t1,2)*(2 + 30*t2 + 21*Power(t2,2)) + 
               t1*(68 + 51*Power(t2,2) + 12*Power(t2,3))) + 
            s1*(-17 + 5*Power(s2,4) + 3*Power(t1,4) + 28*t2 + 
               26*Power(t2,2) + Power(t2,3) - 22*Power(t2,4) + 
               Power(s2,3)*(-45 - 11*t1 + 6*t2) + 
               Power(t1,3)*(-28 + 13*t2) + 
               Power(t1,2)*(22 + 57*t2 - 57*Power(t2,2)) + 
               Power(s2,2)*(113 + 10*Power(t1,2) + t1*(30 - 27*t2) + 
                  41*t2 + 17*Power(t2,2)) + 
               t1*(60 - 48*t2 - 30*Power(t2,2) + 63*Power(t2,3)) - 
               s2*(56 + 7*Power(t1,3) + 32*t2 - 17*Power(t2,2) + 
                  6*Power(t2,3) - Power(t1,2)*(43 + 8*t2) + 
                  t1*(122 + 60*t2 - 5*Power(t2,2))))) + 
         Power(s,5)*(24 + 6*Power(s2,5) + 113*t1 - 39*Power(t1,2) - 
            42*Power(t1,3) + 20*Power(t1,4) - 26*Power(t1,5) + 
            4*Power(t1,6) - 5*t2 - 136*t1*t2 + 52*Power(t1,2)*t2 - 
            3*Power(t1,3)*t2 + 83*Power(t1,4)*t2 - 4*Power(t1,5)*t2 + 
            49*Power(t2,2) + 46*t1*Power(t2,2) - 
            78*Power(t1,2)*Power(t2,2) - 101*Power(t1,3)*Power(t2,2) - 
            20*Power(t1,4)*Power(t2,2) - 56*Power(t2,3) + 
            85*t1*Power(t2,3) + 65*Power(t1,2)*Power(t2,3) + 
            40*Power(t1,3)*Power(t2,3) - 24*Power(t2,4) - 
            29*t1*Power(t2,4) - 20*Power(t1,2)*Power(t2,4) + 
            8*Power(t2,5) - 4*t1*Power(t2,5) + 4*Power(t2,6) - 
            10*Power(s1,5)*(-1 + s2 - t1 + t2) + 
            Power(s2,4)*(-6 + 4*Power(t1,2) + 15*t2 + 16*Power(t2,2) - 
               2*t1*(14 + 9*t2)) + 
            Power(s1,4)*(-14 - 70*Power(s2,2) + 27*Power(t1,2) + 
               t1*(11 - 79*t2) - 48*t2 + 50*Power(t2,2) + 
               s2*(82 + 31*t1 + 10*t2)) + 
            Power(s2,3)*(-46 - 16*Power(t1,3) + 108*t2 + 
               41*Power(t2,2) + 10*Power(t2,3) + 
               Power(t1,2)*(74 + 58*t2) + 
               t1*(13 - 110*t2 - 52*Power(t2,2))) + 
            Power(s2,2)*(181 + 24*Power(t1,4) - 177*t2 + 
               36*Power(t2,2) + 27*Power(t2,3) - 24*Power(t2,4) - 
               6*Power(t1,3)*(19 + 11*t2) + 
               6*Power(t1,2)*(2 + 43*t2 + 6*Power(t2,2)) + 
               3*t1*(-11 - 53*t2 - 57*Power(t2,2) + 10*Power(t2,3))) + 
            s2*(-161 - 16*Power(t1,5) + 116*t2 - 43*Power(t2,2) + 
               24*Power(t2,3) + 3*Power(t2,4) - 14*Power(t2,5) + 
               Power(t1,4)*(88 + 30*t2) + 
               Power(t1,3)*(-39 - 246*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(121 + 54*t2 + 231*Power(t2,2) - 
                  80*Power(t2,3)) + 
               t1*(-118 + 150*t2 - 39*Power(t2,2) - 76*Power(t2,3) + 
                  60*Power(t2,4))) + 
            Power(s1,3)*(-72 - 78*Power(s2,3) + 7*Power(t1,3) + 
               Power(t1,2)*(75 - 102*t2) + 119*t2 + 80*Power(t2,2) - 
               93*Power(t2,3) + Power(s2,2)*(241 + 45*t1 + 112*t2) + 
               2*t1*(17 - 59*t2 + 94*Power(t2,2)) + 
               s2*(-83 + 26*Power(t1,2) - 114*t2 + 25*Power(t2,2) - 
                  4*t1*(53 + 20*t2))) + 
            Power(s1,2)*(167 - 36*Power(s2,4) - 23*Power(t1,4) - 
               27*t2 - 202*Power(t2,2) - 50*Power(t2,3) + 
               80*Power(t2,4) + Power(t1,3)*(47 + 25*t2) + 
               Power(s2,3)*(239 + 49*t1 + 56*t2) - 
               Power(s2,2)*(238 + 13*Power(t1,2) + t1*(334 - 13*t2) + 
                  220*t2 + 84*Power(t2,2)) + 
               Power(t1,2)*(-146 - 165*t2 + 99*Power(t2,2)) + 
               t1*(-83 + 165*t2 + 168*Power(t2,2) - 181*Power(t2,3)) + 
               s2*(-144 + 23*Power(t1,3) + Power(t1,2)*(48 - 94*t2) + 
                  168*t2 + 25*Power(t2,2) - 54*Power(t2,3) + 
                  t1*(415 + 182*t2 + 125*Power(t2,2)))) - 
            s1*(115 + 10*Power(s2,5) + 9*Power(t1,5) + 
               Power(t1,4)*(53 - 54*t2) + 54*t2 - 95*Power(t2,2) - 
               115*Power(t2,3) + 31*Power(t2,5) + 
               Power(s2,4)*(-58 - 35*t1 + 14*t2) + 
               Power(s2,3)*(139 + 36*Power(t1,2) + t1*(116 - 42*t2) + 
                  142*t2 + 19*Power(t2,2)) + 
               Power(t1,3)*(-30 - 72*t2 + 77*Power(t2,2)) + 
               Power(t1,2)*(-175 - 31*t2 - 15*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(52 - 6*t2 + 176*Power(t2,2) + 34*Power(t2,3) - 
                  66*Power(t2,4)) + 
               Power(s2,2)*(98 + 2*Power(t1,3) - 213*t2 - 
                  28*Power(t2,2) - 59*Power(t2,3) - 
                  Power(t1,2)*(5 + 12*t2) + 
                  t1*(-442 - 174*t2 + 69*Power(t2,2))) + 
               s2*(-312 - 22*Power(t1,4) + 106*t2 + 97*Power(t2,2) - 
                  18*Power(t2,3) - 43*Power(t2,4) + 
                  2*Power(t1,3)*(-53 + 47*t2) + 
                  Power(t1,2)*(333 + 104*t2 - 165*Power(t2,2)) + 
                  2*t1*(111 + 16*t2 + 10*Power(t2,2) + 68*Power(t2,3))))) \
+ Power(s,4)*(-156 - 4*Power(s2,6) - 46*t1 + 334*Power(t1,2) + 
            65*Power(t1,3) - 47*Power(t1,4) + 52*Power(t1,5) - 
            21*Power(t1,6) + Power(t1,7) + 166*t2 - 127*t1*t2 - 
            436*Power(t1,2)*t2 - 30*Power(t1,3)*t2 - 
            137*Power(t1,4)*t2 + 29*Power(t1,5)*t2 + 5*Power(t1,6)*t2 + 
            63*Power(t2,2) + 132*t1*Power(t2,2) + 
            212*Power(t1,2)*Power(t2,2) + 240*Power(t1,3)*Power(t2,2) + 
            73*Power(t1,4)*Power(t2,2) - 21*Power(t1,5)*Power(t2,2) + 
            29*Power(t2,3) - 86*t1*Power(t2,3) - 
            353*Power(t1,2)*Power(t2,3) - 207*Power(t1,3)*Power(t2,3) + 
            15*Power(t1,4)*Power(t2,3) - 49*Power(t2,4) + 
            274*t1*Power(t2,4) + 208*Power(t1,2)*Power(t2,4) + 
            15*Power(t1,3)*Power(t2,4) - 76*Power(t2,5) - 
            106*t1*Power(t2,5) - 21*Power(t1,2)*Power(t2,5) + 
            24*Power(t2,6) + 5*t1*Power(t2,6) + Power(t2,7) + 
            10*Power(s1,6)*(-1 + s2 - t1 + t2) - 
            Power(s2,5)*(28 + Power(t1,2) + 6*t2 + 14*Power(t2,2) - 
               t1*(23 + 10*t2)) + 
            Power(s1,5)*(-17 + 100*Power(s2,2) - 18*Power(t1,2) + 
               83*t2 - 58*Power(t2,2) + s2*(-75 - 74*t1 + 2*t2) + 
               3*t1*(-9 + 28*t2)) + 
            Power(s2,4)*(135 + 5*Power(t1,3) - 161*t2 - 
               20*Power(t2,2) + 10*Power(t2,3) - 
               Power(t1,2)*(73 + 35*t2) + 
               t1*(112 + 91*t2 + 30*Power(t2,2))) + 
            Power(s2,3)*(-201 - 10*Power(t1,4) + 185*t2 - 
               51*Power(t2,2) + 20*Power(t2,3) + 45*Power(t2,4) + 
               2*Power(t1,3)*(71 + 20*t2) + 
               Power(t1,2)*(-220 - 266*t2 + 15*Power(t2,2)) + 
               t1*(-349 + 539*t2 + 89*Power(t2,2) - 90*Power(t2,3))) + 
            Power(s2,2)*(-98 + 10*Power(t1,5) + 138*t2 + 
               41*Power(t2,2) - 102*Power(t2,3) + 50*Power(t2,4) + 
               5*Power(t2,5) - 2*Power(t1,4)*(79 + 5*t2) + 
               Power(t1,3)*(268 + 312*t2 - 85*Power(t2,2)) + 
               3*Power(t1,2)*
                (82 - 244*t2 - 15*Power(t2,2) + 55*Power(t2,3)) + 
               t1*(697 - 797*t2 + 411*Power(t2,2) - 159*Power(t2,3) - 
                  85*Power(t2,4))) - 
            s2*(-358 + 5*Power(t1,6) + 389*t2 + 82*Power(t2,2) - 
               92*Power(t2,3) + 50*Power(t2,4) - 36*Power(t2,5) + 
               15*Power(t2,6) + Power(t1,5)*(-91 + 10*t2) + 
               Power(t1,4)*(184 + 160*t2 - 75*Power(t2,2)) + 
               Power(t1,3)*(-15 - 491*t2 + 97*Power(t2,2) + 
                  100*Power(t2,3)) + 
               Power(t1,2)*(561 - 642*t2 + 600*Power(t2,2) - 
                  346*Power(t2,3) - 25*Power(t2,4)) + 
               t1*(392 - 648*t2 + 389*Power(t2,2) - 343*Power(t2,3) + 
                  216*Power(t2,4) - 30*Power(t2,5))) + 
            Power(s1,4)*(160*Power(s2,3) + 14*Power(t1,3) + 
               5*Power(t1,2)*(-23 + 15*t2) - 
               Power(s2,2)*(256 + 151*t1 + 213*t2) + 
               t1*(35 + 196*t2 - 229*Power(t2,2)) + 
               2*(94 - 68*t2 - 102*Power(t2,2) + 65*Power(t2,3)) + 
               s2*(-118 - 23*Power(t1,2) + 253*t2 - 93*Power(t2,2) + 
                  t1*(254 + 233*t2))) + 
            Power(s1,3)*(-222 + 98*Power(s2,4) + 33*Power(t1,4) + 
               Power(t1,3)*(54 - 95*t2) - 310*t2 + 497*Power(t2,2) + 
               194*Power(t2,3) - 143*Power(t2,4) - 
               Power(s2,3)*(421 + 113*t1 + 273*t2) + 
               Power(t1,2)*(257 + 125*t2 - 67*Power(t2,2)) + 
               8*t1*(-8 - 39*t2 - 36*Power(t2,2) + 34*Power(t2,3)) + 
               Power(s2,2)*(-33 - 35*Power(t1,2) + 452*t2 + 
                  137*Power(t2,2) + t1*(763 + 289*t2)) + 
               s2*(602 + 17*Power(t1,3) - 124*t2 - 303*Power(t2,2) + 
                  185*Power(t2,3) + Power(t1,2)*(-396 + 79*t2) - 
                  t1*(339 + 190*t2 + 326*Power(t2,2)))) + 
            Power(s1,2)*(-129 + 42*Power(s2,5) + 3*Power(t1,5) + 
               Power(t1,4)*(182 - 88*t2) + 726*t2 - 53*Power(t2,2) - 
               587*Power(t2,3) - 32*Power(t2,4) + 79*Power(t2,5) - 
               Power(s2,4)*(243 + 115*t1 + 47*t2) + 
               Power(t1,3)*(-125 - 449*t2 + 187*Power(t2,2)) + 
               Power(s2,3)*(218 + 90*Power(t1,2) + t1*(633 - 31*t2) + 
                  474*t2 + 206*Power(t2,2)) - 
               Power(t1,2)*(170 + 331*t2 - 315*Power(t2,2) + 
                  43*Power(t2,3)) + 
               t1*(163 - 87*t2 + 708*Power(t2,2) - 16*Power(t2,3) - 
                  138*Power(t2,4)) + 
               Power(s2,2)*(497 - 65*t2 - 250*Power(t2,2) - 
                  20*Power(t2,3) + 5*Power(t1,2)*(-71 + 23*t2) - 
                  t1*(1087 + 740*t2 + 235*Power(t2,2))) + 
               s2*(-381 - 20*Power(t1,4) - 747*t2 + 495*Power(t2,2) + 
                  161*Power(t2,3) - 166*Power(t2,4) + 
                  Power(t1,3)*(-217 + 51*t2) + 
                  Power(t1,2)*(994 + 715*t2 - 158*Power(t2,2)) + 
                  t1*(-3 + 25*t2 - 244*Power(t2,2) + 293*Power(t2,3)))) \
+ s1*(314 + 10*Power(s2,6) - 7*Power(t1,6) - 396*t2 - 425*Power(t2,2) + 
               254*Power(t2,3) + 319*Power(t2,4) - 55*Power(t2,5) - 
               19*Power(t2,6) + Power(t1,5)*(9 + 13*t2) + 
               Power(s2,5)*(-11 - 49*t1 + 13*t2) + 
               Power(s2,4)*(-94 + 89*Power(t1,2) + t1*(55 - 43*t2) + 
                  223*t2 - 12*Power(t2,2)) + 
               2*Power(t1,4)*(71 - 120*t2 + 22*Power(t2,2)) + 
               Power(t1,3)*(215 - 434*t2 + 586*Power(t2,2) - 
                  121*Power(t2,3)) + 
               Power(t1,2)*(-402 + 224*t2 + 701*Power(t2,2) - 
                  543*Power(t2,3) + 74*Power(t2,4)) + 
               t1*(-203 + 552*t2 - 213*Power(t2,2) - 728*Power(t2,3) + 
                  243*Power(t2,4) + 16*Power(t2,5)) - 
               Power(s2,3)*(-510 + 66*Power(t1,3) + 
                  Power(t1,2)*(108 - 38*t2) + 616*t2 + 
                  105*Power(t2,2) + 118*Power(t2,3) + 
                  t1*(19 + 398*t2 - 121*Power(t2,2))) + 
               Power(s2,2)*(-504 + 4*Power(t1,4) + 218*t2 - 
                  56*Power(t2,2) - 8*Power(t2,3) - 7*Power(t2,4) + 
                  2*Power(t1,3)*(52 + 7*t2) + 
                  Power(t1,2)*(462 - 113*t2 - 162*Power(t2,2)) + 
                  t1*(-730 + 1150*t2 + 372*Power(t2,2) + 
                     151*Power(t2,3))) + 
               s2*(-241 + 19*Power(t1,5) + 540*t2 + 287*Power(t2,2) - 
                  240*Power(t2,3) - 74*Power(t2,4) + 77*Power(t2,5) - 
                  7*Power(t1,4)*(7 + 5*t2) + 
                  Power(t1,3)*(-491 + 528*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*
                   (5 - 100*t2 - 853*Power(t2,2) + 88*Power(t2,3)) + 
                  t1*(1036 - 1092*t2 + 61*Power(t2,2) + 
                     448*Power(t2,3) - 158*Power(t2,4))))) - 
         Power(s,3)*(-234 - Power(s2,7) + 238*t1 + 700*Power(t1,2) - 
            6*Power(t1,3) - 116*Power(t1,4) + 54*Power(t1,5) - 
            53*Power(t1,6) + 5*Power(t1,7) + 386*t2 - 1272*t1*t2 - 
            1370*Power(t1,2)*t2 + 252*Power(t1,3)*t2 - 
            10*Power(t1,4)*t2 + 123*Power(t1,5)*t2 + 24*Power(t1,6)*t2 - 
            2*Power(t1,7)*t2 + 332*Power(t2,2) + 1724*t1*Power(t2,2) + 
            514*Power(t1,2)*Power(t2,2) - 48*Power(t1,3)*Power(t2,2) - 
            94*Power(t1,4)*Power(t2,2) - 111*Power(t1,5)*Power(t2,2) + 
            2*Power(t1,6)*Power(t2,2) - 708*Power(t2,3) - 
            580*t1*Power(t2,3) - 156*Power(t1,2)*Power(t2,3) - 
            92*Power(t1,3)*Power(t2,3) + 118*Power(t1,4)*Power(t2,3) + 
            10*Power(t1,5)*Power(t2,3) + 140*Power(t2,4) + 
            146*t1*Power(t2,4) + 330*Power(t1,2)*Power(t2,4) + 
            18*Power(t1,3)*Power(t2,4) - 20*Power(t1,4)*Power(t2,4) + 
            14*Power(t2,5) - 305*t1*Power(t2,5) - 
            117*Power(t1,2)*Power(t2,5) + 10*Power(t1,3)*Power(t2,5) + 
            91*Power(t2,6) + 84*t1*Power(t2,6) + 
            2*Power(t1,2)*Power(t2,6) - 21*Power(t2,7) - 
            2*t1*Power(t2,7) + 5*Power(s1,7)*(-1 + s2 - t1 + t2) + 
            Power(s2,6)*(-23 + 6*t2 - 6*Power(t2,2) + t1*(7 + 2*t2)) + 
            2*Power(s1,6)*(-15 + 40*Power(s2,2) - Power(t1,2) + 33*t2 - 
               17*Power(t2,2) + 3*t1*(-5 + 8*t2) + 
               s2*(-19 - 38*t1 + 8*t2)) + 
            Power(s2,5)*(-22 - 48*t2 + 9*Power(t2,2) + 20*Power(t2,3) - 
               Power(t1,2)*(25 + 8*t2) + t1*(127 - 2*t2 + 8*Power(t2,2))\
) + Power(s2,4)*(324 - 573*t2 + 126*Power(t2,2) + 50*Power(t2,3) + 
               30*Power(t2,4) + 5*Power(t1,3)*(11 + 2*t2) + 
               Power(t1,2)*(-331 - 28*t2 + 30*Power(t2,2)) + 
               t1*(69 + 359*t2 - 119*Power(t2,2) - 90*Power(t2,3))) - 
            Power(s2,3)*(714 + 75*Power(t1,4) - 1614*t2 + 
               1149*Power(t2,2) - 132*Power(t2,3) - 70*Power(t2,4) + 
               20*Power(t2,5) + 
               2*Power(t1,3)*(-257 - 6*t2 + 40*Power(t2,2)) + 
               Power(t1,2)*(129 + 912*t2 - 414*Power(t2,2) - 
                  140*Power(t2,3)) + 
               2*t1*(383 - 737*t2 + 28*Power(t2,2) + 188*Power(t2,3) + 
                  20*Power(t2,4))) + 
            Power(s2,2)*(444 + Power(t1,5)*(61 - 10*t2) - 1398*t2 + 
               1742*Power(t2,2) - 1083*Power(t2,3) + 200*Power(t2,4) + 
               39*Power(t2,5) - 20*Power(t2,6) + 
               Power(t1,4)*(-481 + 62*t2 + 70*Power(t2,2)) + 
               Power(t1,3)*(193 + 1062*t2 - 618*Power(t2,2) - 
                  80*Power(t2,3)) - 
               3*Power(t1,2)*
                (-148 + 413*t2 + 120*Power(t2,2) - 240*Power(t2,3) + 
                  10*Power(t2,4)) + 
               t1*(1682 - 3716*t2 + 2789*Power(t2,2) - 
                  296*Power(t2,3) - 264*Power(t2,4) + 70*Power(t2,5))) + 
            s2*(230 + 2*t2 - 1176*Power(t2,2) + 1342*Power(t2,3) - 
               531*Power(t2,4) + 100*Power(t2,5) - 4*Power(t2,6) + 
               4*Power(t2,7) + Power(t1,6)*(-27 + 8*t2) + 
               Power(t1,5)*(247 - 74*t2 - 24*Power(t2,2)) + 
               Power(t1,4)*(-165 - 584*t2 + 425*Power(t2,2)) + 
               2*Power(t1,3)*
                (57 + 174*t2 + 192*Power(t2,2) - 256*Power(t2,3) + 
                  30*Power(t2,4)) - 
               2*Power(t1,2)*
                (481 - 925*t2 + 796*Power(t2,2) - 128*Power(t2,3) - 
                  88*Power(t2,4) + 30*Power(t2,5)) + 
               t1*(-1378 + 3652*t2 - 3326*Power(t2,2) + 
                  1600*Power(t2,3) - 403*Power(t2,4) + 16*Power(t2,5) + 
                  12*Power(t2,6))) + 
            Power(s1,4)*(-36 + 143*Power(s2,4) + 21*Power(t1,4) + 
               Power(t1,3)*(201 - 128*t2) - 625*t2 + 485*Power(t2,2) + 
               293*Power(t2,3) - 127*Power(t2,4) - 
               Power(s2,3)*(295 + 194*t1 + 440*t2) + 
               Power(t1,2)*(176 - 115*t2 + 44*Power(t2,2)) + 
               t1*(-182 - 423*t2 - 157*Power(t2,2) + 210*Power(t2,3)) + 
               Power(s2,2)*(-527 - 20*Power(t1,2) + 467*t2 + 
                  116*Power(t2,2) + t1*(683 + 644*t2)) + 
               s2*(703 + 50*Power(t1,3) + 194*t2 - 597*Power(t2,2) + 
                  272*Power(t2,3) - 19*Power(t1,2)*(31 + 4*t2) + 
                  t1*(225 + 8*t2 - 404*Power(t2,2)))) + 
            Power(s1,5)*(166 + 181*Power(s2,3) + 19*Power(t1,3) - 
               40*t2 - 220*Power(t2,2) + 92*Power(t2,3) + 
               Power(t1,2)*(-69 + 7*t2) - 
               Power(s2,2)*(115 + 236*t1 + 200*t2) - 
               2*t1*(-55 - 76*t2 + 75*Power(t2,2)) + 
               s2*(36*Power(t1,2) + t1*(107 + 268*t2) - 
                  3*(86 - 93*t2 + 43*Power(t2,2)))) + 
            Power(s1,3)*(-515 + 65*Power(s2,5) + Power(t1,5) + 
               Power(t1,4)*(233 - 99*t2) + 988*t2 + 585*Power(t2,2) - 
               1007*Power(t2,3) - 123*Power(t2,4) + 94*Power(t2,5) - 
               Power(s2,4)*(346 + 125*t1 + 259*t2) + 
               Power(t1,3)*(-107 - 915*t2 + 298*Power(t2,2)) + 
               Power(t1,2)*(106 - 663*t2 + 947*Power(t2,2) - 
                  164*Power(t2,3)) + 
               t1*(-20 + 85*t2 + 1119*Power(t2,2) - 257*Power(t2,3) - 
                  130*Power(t2,4)) + 
               Power(s2,3)*(71 - 16*Power(t1,2) + 508*t2 + 
                  368*Power(t2,2) + t1*(1019 + 422*t2)) + 
               Power(s2,2)*(403 + 148*Power(t1,3) + 1183*t2 - 
                  714*Power(t2,2) + 72*Power(t2,3) - 
                  Power(t1,2)*(767 + 166*t2) - 
                  t1*(1013 + 971*t2 + 568*Power(t2,2))) + 
               s2*(390 - 73*Power(t1,4) - 2326*t2 + 857*Power(t2,2) + 
                  524*Power(t2,3) - 286*Power(t2,4) + 
                  Power(t1,3)*(-139 + 102*t2) + 
                  Power(t1,2)*(1049 + 1378*t2 - 98*Power(t2,2)) + 
                  t1*(-187 - 824*t2 - 527*Power(t2,2) + 390*Power(t2,3))\
)) + Power(s1,2)*(314 + 31*Power(s2,6) - 5*Power(t1,6) + 619*t2 - 
               1882*Power(t2,2) + 70*Power(t2,3) + 972*Power(t2,4) - 
               74*Power(t2,5) - 35*Power(t2,6) + 
               Power(s2,5)*(-69 - 138*t1 + 4*t2) + 
               Power(t1,5)*(-30 + 8*t2) + 
               Power(t1,4)*(327 - 401*t2 + 113*Power(t2,2)) + 
               Power(s2,4)*(-312 + 237*Power(t1,2) + 
                  t1*(397 - 160*t2) + 532*t2 + 227*Power(t2,2)) + 
               Power(t1,3)*(279 - 734*t2 + 1302*Power(t2,2) - 
                  282*Power(t2,3)) + 
               Power(t1,2)*(-660 - 70*t2 + 1580*Power(t2,2) - 
                  1400*Power(t2,3) + 183*Power(t2,4)) + 
               t1*(-91 + 656*t2 + 181*Power(t2,2) - 1780*Power(t2,3) + 
                  603*Power(t2,4) + 18*Power(t2,5)) - 
               Power(s2,3)*(-1055 + 188*Power(t1,3) + 
                  Power(t1,2)*(747 - 448*t2) + 950*t2 + 
                  249*Power(t2,2) + 128*Power(t2,3) + 
                  2*t1*(-90 + 543*t2 + 251*Power(t2,2))) + 
               Power(s2,2)*(-238 + 57*Power(t1,4) + 
                  Power(t1,3)*(549 - 424*t2) - 34*t2 - 
                  1111*Power(t2,2) + 461*Power(t2,3) - 
                  111*Power(t2,4) + 
                  Power(t1,2)*(903 + 175*t2 + 436*Power(t2,2)) + 
                  t1*(-1995 + 2412*t2 + 595*Power(t2,2) + 
                     182*Power(t2,3))) + 
               s2*(-833 + 6*Power(t1,5) + 246*t2 + 2255*Power(t2,2) - 
                  1196*Power(t2,3) - 193*Power(t2,4) + 
                  166*Power(t2,5) + 4*Power(t1,4)*(-25 + 31*t2) - 
                  2*Power(t1,3)*(549 - 390*t2 + 137*Power(t2,2)) + 
                  Power(t1,2)*
                   (661 - 728*t2 - 1648*Power(t2,2) + 228*Power(t2,3)) \
+ t1*(1318 - 1356*t2 + 896*Power(t2,2) + 776*Power(t2,3) - 
                     250*Power(t2,4)))) + 
            s1*(274 + 5*Power(s2,7) + Power(s2,6)*(29 - 32*t1) - 
               Power(t1,7) + 13*Power(t1,6)*(-1 + t2) - 1210*t2 + 
               688*Power(t2,2) + 844*Power(t2,3) - 210*Power(t2,4) - 
               471*Power(t2,5) + 84*Power(t2,6) + 5*Power(t2,7) + 
               Power(t1,5)*(-93 + 79*t2 - 26*Power(t2,2)) + 
               Power(t1,4)*(59 + 7*t2 + 89*Power(t2,2) - 
                  16*Power(t2,3)) + 
               Power(t1,3)*(612 - 1033*t2 + 694*Power(t2,2) - 
                  590*Power(t2,3) + 84*Power(t2,4)) + 
               Power(t1,2)*(214 - 272*t2 + 1019*Power(t2,2) - 
                  1428*Power(t2,3) + 746*Power(t2,4) - 70*Power(t2,5)) + 
               t1*(-102 + 344*t2 - 804*Power(t2,2) - 335*Power(t2,3) + 
                  1291*Power(t2,4) - 395*Power(t2,5) + 11*Power(t2,6)) + 
               Power(s2,5)*(-175 + 86*Power(t1,2) + 167*t2 - 
                  53*Power(t2,2) + 2*t1*(-64 + 3*t2)) - 
               Power(s2,4)*(-479 + 125*Power(t1,3) + 
                  11*Power(t1,2)*(-19 + t2) + 243*t2 + 
                  121*Power(t2,2) + 122*Power(t2,3) + 
                  t1*(-563 + 581*t2 - 232*Power(t2,2))) + 
               Power(s2,3)*(-320 + 105*Power(t1,4) - 90*t2 + 
                  442*Power(t2,2) + 14*Power(t2,3) + 42*Power(t2,4) - 
                  8*Power(t1,3)*(17 + 2*t2) + 
                  Power(t1,2)*(-546 + 662*t2 - 352*Power(t2,2)) + 
                  t1*(-1785 + 1226*t2 + 224*Power(t2,2) + 
                     256*Power(t2,3))) - 
               Power(s2,2)*(338 + 50*Power(t1,5) + 
                  Power(t1,4)*(1 - 54*t2) + 6*t2 - 607*Power(t2,2) - 
                  162*Power(t2,3) + 132*Power(t2,4) - 63*Power(t2,5) - 
                  2*Power(t1,3)*(5 - 85*t2 + 97*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (-1096 + 858*t2 - 2*Power(t2,2) + 81*Power(t2,3)) + 
                  t1*(-1858 + 1731*t2 + 390*Power(t2,2) + 
                     86*Power(t2,3) + 99*Power(t2,4))) + 
               s2*(52 + 12*Power(t1,6) + Power(t1,5)*(40 - 46*t2) + 
                  1160*t2 - 1398*Power(t2,2) - 104*Power(t2,3) + 
                  291*Power(t2,4) + 29*Power(t2,5) - 48*Power(t2,6) + 
                  Power(t1,4)*(241 - 157*t2 + 5*Power(t2,2)) + 
                  Power(t1,3)*
                   (-945 + 726*t2 - 196*Power(t2,2) + 44*Power(t2,3)) + 
                  Power(t1,2)*
                   (-2150 + 2854*t2 - 746*Power(t2,2) + 
                     662*Power(t2,3) - 27*Power(t2,4)) + 
                  t1*(-100 + 364*t2 - 765*Power(t2,2) + 
                     258*Power(t2,3) - 378*Power(t2,4) + 60*Power(t2,5)))\
)) + Power(s,2)*(-150 + 385*t1 + 671*Power(t1,2) - 140*Power(t1,3) - 
            165*Power(t1,4) + 41*Power(t1,5) - 74*Power(t1,6) + 
            12*Power(t1,7) + 317*t2 - 2100*t1*t2 - 1690*Power(t1,2)*t2 + 
            790*Power(t1,3)*t2 + 209*Power(t1,4)*t2 + 
            228*Power(t1,5)*t2 + 50*Power(t1,6)*t2 - 10*Power(t1,7)*t2 + 
            493*Power(t2,2) + 3692*t1*Power(t2,2) + 
            780*Power(t1,2)*Power(t2,2) - 992*Power(t1,3)*Power(t2,2) - 
            512*Power(t1,4)*Power(t2,2) - 279*Power(t1,5)*Power(t2,2) + 
            10*Power(t1,6)*Power(t2,2) + Power(t1,7)*Power(t2,2) - 
            1622*Power(t2,3) - 2284*t1*Power(t2,3) + 
            646*Power(t1,2)*Power(t2,3) + 780*Power(t1,3)*Power(t2,3) + 
            509*Power(t1,4)*Power(t2,3) + 58*Power(t1,5)*Power(t2,3) - 
            3*Power(t1,6)*Power(t2,3) + 1149*Power(t2,4) + 
            78*t1*Power(t2,4) - 548*Power(t1,2)*Power(t2,4) - 
            439*Power(t1,3)*Power(t2,4) - 137*Power(t1,4)*Power(t2,4) + 
            2*Power(t1,5)*Power(t2,4) - 108*Power(t2,5) + 
            168*t1*Power(t2,5) + 102*Power(t1,2)*Power(t2,5) + 
            107*Power(t1,3)*Power(t2,5) + 2*Power(t1,4)*Power(t2,5) - 
            42*Power(t2,6) + 88*t1*Power(t2,6) - 
            19*Power(t1,2)*Power(t2,6) - 3*Power(t1,3)*Power(t2,6) - 
            43*Power(t2,7) - 15*t1*Power(t2,7) + 
            Power(t1,2)*Power(t2,7) + 6*Power(t2,8) + 
            Power(s1,8)*(-1 + s2 - t1 + t2) - 
            Power(s2,7)*(5 - 4*t2 + Power(t2,2)) + 
            Power(s2,6)*(-43 + 26*t2 - 2*Power(t2,2) + 11*Power(t2,3) + 
               t1*(36 - 22*t2 + Power(t2,2))) + 
            Power(s1,7)*(34*Power(s2,2) + 3*Power(t1,2) + 
               t1*(-17 + 13*t2) + s2*(-10 - 37*t1 + 14*t2) - 
               8*(2 - 3*t2 + Power(t2,2))) + 
            Power(s2,5)*(30 - 194*t2 + 135*Power(t2,2) + 
               3*Power(t2,4) + 
               3*Power(t1,2)*(-39 + 20*t2 + 3*Power(t2,2)) - 
               3*t1*(-78 + 29*t2 + 16*Power(t2,2) + 14*Power(t2,3))) + 
            Power(s2,4)*(292 - 731*t2 + 295*Power(t2,2) + 
               113*Power(t2,3) - 10*Power(t2,4) - 25*Power(t2,5) - 
               5*Power(t1,3)*(-44 + 22*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-580 + 138*t2 + 222*Power(t2,2) + 
                  55*Power(t2,3)) + 
               t1*(-131 + 1003*t2 - 699*Power(t2,2) - 34*Power(t2,3) + 
                  15*Power(t2,4))) + 
            Power(s2,3)*(-834 + 2645*t2 - 3076*Power(t2,2) + 
               1161*Power(t2,3) + 70*Power(t2,4) - 44*Power(t2,5) - 
               10*Power(t2,6) + 
               5*Power(t1,4)*(-51 + 28*t2 + 5*Power(t2,2)) - 
               2*Power(t1,3)*
                (-420 + 101*t2 + 184*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,2)*(172 - 2073*t2 + 1566*Power(t2,2) + 
                  44*Power(t2,3) - 65*Power(t2,4)) + 
               t1*(-567 + 1558*t2 + 8*Power(t2,2) - 956*Power(t2,3) + 
                  179*Power(t2,4) + 70*Power(t2,5))) + 
            Power(s2,2)*(793 - 3027*t2 + 4635*Power(t2,2) - 
               3710*Power(t2,3) + 1354*Power(t2,4) - 45*Power(t2,5) - 
               60*Power(t2,6) + 6*Power(t2,7) - 
               3*Power(t1,5)*(-60 + 38*t2 + 3*Power(t2,2)) - 
               3*Power(t1,4)*
                (245 - 86*t2 - 94*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,3)*(-30 + 2141*t2 - 1854*Power(t2,2) + 
                  72*Power(t2,3) + 75*Power(t2,4)) - 
               3*Power(t1,2)*
                (-31 + 238*t2 + 471*Power(t2,2) - 694*Power(t2,3) + 
                  155*Power(t2,4) + 21*Power(t2,5)) + 
               3*t1*(547 - 1664*t2 + 1948*Power(t2,2) - 
                  609*Power(t2,3) - 226*Power(t2,4) + 95*Power(t2,5) + 
                  2*Power(t2,6))) - 
            s2*(85 - 968*t2 + 2885*Power(t2,2) - 3617*Power(t2,3) + 
               2157*Power(t2,4) - 579*Power(t2,5) + 23*Power(t2,6) + 
               26*Power(t2,7) + 
               Power(t1,6)*(71 - 52*t2 + Power(t2,2)) + 
               Power(t1,5)*(-358 + 183*t2 + 96*Power(t2,2) - 
                  14*Power(t2,3)) + 
               Power(t1,4)*(82 + 1105*t2 - 1131*Power(t2,2) + 
                  140*Power(t2,3) + 30*Power(t2,4)) - 
               Power(t1,3)*(347 - 322*t2 + 1622*Power(t2,2) - 
                  1748*Power(t2,3) + 433*Power(t2,4) + 16*Power(t2,5)) \
+ Power(t1,2)*(667 - 1557*t2 + 1776*Power(t2,2) + 114*Power(t2,3) - 
                  1047*Power(t2,4) + 348*Power(t2,5) - 7*Power(t2,6)) + 
               t1*(1614 - 5532*t2 + 6987*Power(t2,2) - 
                  4362*Power(t2,3) + 1148*Power(t2,4) + 
                  153*Power(t2,5) - 125*Power(t2,6) + 6*Power(t2,7))) + 
            Power(s1,6)*(64 + 115*Power(s2,3) + 8*Power(t1,3) + 
               Power(t1,2)*(2 - 25*t2) + 17*t2 - 105*Power(t2,2) + 
               26*Power(t2,3) - 5*Power(s2,2)*(1 + 37*t1 + 18*t2) + 
               t1*(92 + 54*t2 - 45*Power(t2,2)) + 
               s2*(-176 + 62*Power(t1,2) + 160*t2 - 89*Power(t2,2) + 
                  2*t1*(-13 + 73*t2))) + 
            Power(s1,5)*(83 + 119*Power(s2,4) + 9*Power(t1,4) + 
               Power(t1,3)*(208 - 91*t2) - 439*t2 + 209*Power(t2,2) + 
               183*Power(t2,3) - 44*Power(t2,4) - 
               Power(s2,3)*(50 + 210*t1 + 337*t2) + 
               2*Power(t1,2)*(26 - 141*t2 + 56*Power(t2,2)) + 
               t1*(-92 - 398*t2 + 66*Power(t2,2) + 62*Power(t2,3)) + 
               Power(s2,2)*(-541 + 72*Power(t1,2) + 256*t2 + 
                  39*Power(t2,2) + 82*t1*(3 + 7*t2)) + 
               s2*(331 + 10*Power(t1,3) + 341*t2 - 505*Power(t2,2) + 
                  211*Power(t2,3) - 2*Power(t1,2)*(202 + 73*t2) + 
                  t1*(429 + 195*t2 - 268*Power(t2,2)))) + 
            Power(s1,4)*(-388 + 55*Power(s2,5) + 4*Power(t1,5) + 
               Power(t1,4)*(141 - 69*t2) + 337*t2 + 894*Power(t2,2) - 
               750*Power(t2,3) - 122*Power(t2,4) + 41*Power(t2,5) - 
               Power(s2,4)*(175 + 78*t1 + 345*t2) + 
               Power(t1,3)*(63 - 919*t2 + 279*Power(t2,2)) + 
               Power(t1,2)*(242 - 669*t2 + 1182*Power(t2,2) - 
                  253*Power(t2,3)) + 
               t1*(-232 + 153*t2 + 1108*Power(t2,2) - 
                  505*Power(t2,3) - 22*Power(t2,4)) + 
               Power(s2,3)*(-100*Power(t1,2) + t1*(587 + 750*t2) + 
                  3*(-70 + 70*t2 + 99*Power(t2,2))) + 
               Power(s2,2)*(-7 + 218*Power(t1,3) + 1639*t2 - 
                  752*Power(t2,2) + 87*Power(t2,3) - 
                  2*Power(t1,2)*(254 + 267*t2) - 
                  3*t1*(18 + 203*t2 + 169*Power(t2,2))) + 
               s2*(815 - 99*Power(t1,4) - 2139*t2 + 423*Power(t2,2) + 
                  644*Power(t2,3) - 269*Power(t2,4) + 
                  9*Power(t1,3)*(-5 + 22*t2) + 
                  Power(t1,2)*(201 + 1318*t2 - 69*Power(t2,2)) + 
                  t1*(-69 - 1129*t2 - 605*Power(t2,2) + 346*Power(t2,3))\
)) + Power(s1,3)*(-146 + 27*Power(s2,6) - 7*Power(t1,6) + 1533*t2 - 
               1788*Power(t2,2) - 675*Power(t2,3) + 1115*Power(t2,4) - 
               27*Power(t2,5) - 20*Power(t2,6) + 
               Power(t1,5)*(-65 + 21*t2) - 
               Power(s2,5)*(117 + 95*t1 + 84*t2) + 
               Power(t1,4)*(310 - 277*t2 + 98*Power(t2,2)) + 
               Power(t1,3)*(103 - 768*t2 + 1463*Power(t2,2) - 
                  350*Power(t2,3)) + 
               Power(t1,2)*(-241 - 580*t2 + 1896*Power(t2,2) - 
                  1849*Power(t2,3) + 288*Power(t2,4)) + 
               t1*(108 + 465*t2 + 348*Power(t2,2) - 2016*Power(t2,3) + 
                  846*Power(t2,4) - 30*Power(t2,5)) + 
               Power(s2,4)*(-137 + 103*Power(t1,2) + 289*t2 + 
                  388*Power(t2,2) + t1*(646 + 87*t2)) - 
               Power(s2,3)*(-517 + 2*Power(t1,3) + 
                  Power(t1,2)*(1171 - 222*t2) - 293*t2 + 
                  461*Power(t2,2) + 20*Power(t2,3) + 
                  t1*(365 + 503*t2 + 986*Power(t2,2))) + 
               Power(s2,2)*(391 - 67*Power(t1,4) + 
                  Power(t1,3)*(807 - 348*t2) - 189*t2 - 
                  1727*Power(t2,2) + 775*Power(t2,3) - 
                  119*Power(t2,4) + 
                  Power(t1,2)*(1451 - 138*t2 + 906*Power(t2,2)) + 
                  t1*(-1195 + 6*t2 + 1032*Power(t2,2) + 4*Power(t2,3))) \
+ s2*(-555 + 41*Power(t1,5) - 1343*t2 + 3809*Power(t2,2) - 
                  1475*Power(t2,3) - 341*Power(t2,4) + 
                  200*Power(t2,5) + 2*Power(t1,4)*(-50 + 51*t2) + 
                  Power(t1,3)*(-1259 + 629*t2 - 406*Power(t2,2)) + 
                  Power(t1,2)*
                   (575 + 469*t2 - 2034*Power(t2,2) + 366*Power(t2,3)) \
+ t1*(238 - 461*t2 + 967*Power(t2,2) + 917*Power(t2,3) - 
                     314*Power(t2,4)))) + 
            Power(s1,2)*(549 + 14*Power(s2,7) - 5*Power(t1,7) - 
               1030*t2 - 979*Power(t2,2) + 2138*Power(t2,3) + 
               86*Power(t2,4) - 848*Power(t2,5) + 82*Power(t2,6) + 
               4*Power(t2,7) + Power(s2,6)*(44 - 87*t1 + 19*t2) + 
               Power(t1,6)*(-30 + 39*t2) + 
               Power(t1,5)*(-196 + 213*t2 - 86*Power(t2,2)) - 
               Power(t1,4)*(33 - 121*t2 + 9*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,3)*(827 - 710*t2 + 604*Power(t2,2) - 
                  892*Power(t2,3) + 179*Power(t2,4)) + 
               Power(t1,2)*(168 - 472*t2 + 1083*Power(t2,2) - 
                  1822*Power(t2,3) + 1260*Power(t2,4) - 157*Power(t2,5)\
) + t1*(30 + t2 - 499*Power(t2,2) - 806*Power(t2,3) + 
                  1904*Power(t2,4) - 624*Power(t2,5) + 32*Power(t2,6)) \
+ Power(s2,5)*(-456 + 230*Power(t1,2) + 335*t2 + 85*Power(t2,2) - 
                  t1*(113 + 160*t2)) - 
               Power(s2,4)*(-850 + 335*Power(t1,3) + 
                  Power(t1,2)*(18 - 489*t2) + 791*t2 - 6*Power(t2,2) + 
                  204*Power(t2,3) + 
                  t1*(-1647 + 1272*t2 + 250*Power(t2,2))) + 
               Power(s2,3)*(-475 + 290*Power(t1,4) + 359*t2 - 
                  685*Power(t2,2) + 469*Power(t2,3) - 82*Power(t2,4) - 
                  46*Power(t1,3)*(-7 + 16*t2) + 
                  Power(t1,2)*(-2009 + 1593*t2 + 326*Power(t2,2)) + 
                  t1*(-3239 + 3596*t2 - 390*Power(t2,2) + 
                     502*Power(t2,3))) + 
               Power(s2,2)*(229 - 149*Power(t1,5) - 1928*t2 + 
                  1626*Power(t2,2) + 464*Power(t2,3) - 
                  344*Power(t2,4) + 73*Power(t2,5) + 
                  Power(t1,4)*(-388 + 589*t2) + 
                  Power(t1,3)*(705 - 497*t2 - 328*Power(t2,2)) + 
                  Power(t1,2)*
                   (3895 - 4698*t2 + 753*Power(t2,2) - 
                     398*Power(t2,3)) + 
                  t1*(2499 - 2380*t2 + 1449*Power(t2,2) - 
                     879*Power(t2,3) + 129*Power(t2,4))) + 
               s2*(-793 + 42*Power(t1,6) + 2986*t2 - 1441*Power(t2,2) - 
                  1883*Power(t2,3) + 1168*Power(t2,4) + 4*Power(t2,5) - 
                  83*Power(t2,6) - 3*Power(t1,5)*(-61 + 80*t2) + 
                  Power(t1,4)*(309 - 372*t2 + 253*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1473 + 1772*t2 - 360*Power(t2,2) + 
                     106*Power(t2,3)) + 
                  Power(t1,2)*
                   (-2851 + 2731*t2 - 1368*Power(t2,2) + 
                     1302*Power(t2,3) - 226*Power(t2,4)) + 
                  t1*(-455 + 1862*t2 - 995*Power(t2,2) - 
                     182*Power(t2,3) - 556*Power(t2,4) + 148*Power(t2,5)\
))) + s1*(91 + Power(s2,8) + Power(t1,8) + 
               Power(s2,7)*(20 - 8*t1 - 7*t2) - 1366*t2 + 
               2822*Power(t2,2) - 1273*Power(t2,3) - 662*Power(t2,4) + 
               112*Power(t2,5) + 316*Power(t2,6) - 40*Power(t2,7) + 
               Power(t1,7)*(-14 + 3*t2) + 
               Power(t1,6)*(-9 + 77*t2 - 32*Power(t2,2)) + 
               Power(t1,5)*(-250 + 310*t2 - 231*Power(t2,2) + 
                  64*Power(t2,3)) + 
               Power(t1,4)*(-250 + 1091*t2 - 850*Power(t2,2) + 
                  263*Power(t2,3) - 36*Power(t2,4)) + 
               Power(t1,3)*(515 - 667*t2 - 719*Power(t2,2) + 
                  600*Power(t2,3) + 44*Power(t2,4) - 22*Power(t2,5)) + 
               Power(t1,2)*(1008 - 2107*t2 + 1245*Power(t2,2) - 
                  175*Power(t2,3) + 411*Power(t2,4) - 294*Power(t2,5) + 
                  31*Power(t2,6)) + 
               t1*(500 - 1778*t2 + 1571*Power(t2,2) + 39*Power(t2,3) + 
                  253*Power(t2,4) - 778*Power(t2,5) + 195*Power(t2,6) - 
                  9*Power(t2,7)) + 
               Power(s2,6)*(-13 + 28*Power(t1,2) + 47*t2 - 
                  47*Power(t2,2) + 16*t1*(-8 + 3*t2)) - 
               Power(s2,5)*(194 + 56*Power(t1,3) - 372*t2 + 
                  105*Power(t2,2) + 53*Power(t2,3) + 
                  6*Power(t1,2)*(-59 + 23*t2) + 
                  t1*(-80 + 294*t2 - 246*Power(t2,2))) + 
               Power(s2,4)*(1100 + 70*Power(t1,4) - 1782*t2 + 
                  868*Power(t2,2) - 55*Power(t2,3) + 68*Power(t2,4) + 
                  5*Power(t1,3)*(-110 + 43*t2) + 
                  Power(t1,2)*(-199 + 783*t2 - 546*Power(t2,2)) + 
                  t1*(393 - 1022*t2 + 213*Power(t2,2) + 202*Power(t2,3))\
) + Power(s2,3)*(-1810 - 56*Power(t1,5) + 4067*t2 - 2437*Power(t2,2) + 
                  550*Power(t2,3) - 117*Power(t2,4) + 37*Power(t2,5) - 
                  65*Power(t1,4)*(-8 + 3*t2) + 
                  4*Power(t1,3)*(64 - 283*t2 + 166*Power(t2,2)) + 
                  Power(t1,2)*
                   (235 + 524*t2 + 222*Power(t2,2) - 352*Power(t2,3)) - 
                  t1*(3136 - 4723*t2 + 2276*Power(t2,2) + 
                     24*Power(t2,3) + 129*Power(t2,4))) + 
               Power(s2,2)*(1011 + 28*Power(t1,6) - 3819*t2 + 
                  5561*Power(t2,2) - 2920*Power(t2,3) + 
                  210*Power(t2,4) + 130*Power(t2,5) - 30*Power(t2,6) + 
                  6*Power(t1,5)*(-50 + 17*t2) + 
                  Power(t1,4)*(-179 + 933*t2 - 471*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1123 + 840*t2 - 894*Power(t2,2) + 374*Power(t2,3)) \
+ Power(t1,2)*(2722 - 3009*t2 + 1098*Power(t2,2) + 476*Power(t2,3) + 
                     18*Power(t2,4)) - 
                  t1*(-4769 + 10791*t2 - 5940*Power(t2,2) + 
                     648*Power(t2,3) + 84*Power(t2,4) + 21*Power(t2,5))) \
- s2*(184 + 8*Power(t1,7) - 2382*t2 + 5492*Power(t2,2) - 
                  4191*Power(t2,3) + 721*Power(t2,4) + 258*Power(t2,5) - 
                  74*Power(t2,6) - 15*Power(t2,7) + 
                  14*Power(t1,6)*(-7 + 2*t2) - 
                  2*Power(t1,5)*(32 - 207*t2 + 93*Power(t2,2)) + 
                  Power(t1,4)*
                   (-939 + 1024*t2 - 795*Power(t2,2) + 235*Power(t2,3)) \
+ Power(t1,3)*(436 + 1023*t2 - 1160*Power(t2,2) + 660*Power(t2,3) - 
                     79*Power(t2,4)) + 
                  Power(t1,2)*
                   (3474 - 7391*t2 + 2784*Power(t2,2) + 
                     502*Power(t2,3) - 157*Power(t2,4) - 6*Power(t2,5)) \
+ t1*(2426 - 7232*t2 + 7930*Power(t2,2) - 2829*Power(t2,3) - 
                     98*Power(t2,4) + 50*Power(t2,5) + 15*Power(t2,6))))) \
+ s*(36 - 227*t1 - 306*Power(t1,2) + 153*Power(t1,3) + 109*Power(t1,4) - 
            30*Power(t1,5) + 53*Power(t1,6) - 12*Power(t1,7) - 73*t2 + 
            1394*t1*t2 + 869*Power(t1,2)*t2 - 807*Power(t1,3)*t2 - 
            199*Power(t1,4)*t2 - 171*Power(t1,5)*t2 - 57*Power(t1,6)*t2 + 
            20*Power(t1,7)*t2 - 386*Power(t2,2) - 2999*t1*Power(t2,2) - 
            353*Power(t1,2)*Power(t2,2) + 1334*Power(t1,3)*Power(t2,2) + 
            540*Power(t1,4)*Power(t2,2) + 334*Power(t1,5)*Power(t2,2) - 
            25*Power(t1,6)*Power(t2,2) - 7*Power(t1,7)*Power(t2,2) + 
            1353*Power(t2,3) + 2675*t1*Power(t2,3) - 
            1016*Power(t1,2)*Power(t2,3) - 1378*Power(t1,3)*Power(t2,3) - 
            767*Power(t1,4)*Power(t2,3) - 101*Power(t1,5)*Power(t2,3) + 
            22*Power(t1,6)*Power(t2,3) - 1504*Power(t2,4) - 
            555*t1*Power(t2,4) + 1430*Power(t1,2)*Power(t2,4) + 
            1046*Power(t1,3)*Power(t2,4) + 318*Power(t1,4)*Power(t2,4) - 
            13*Power(t1,5)*Power(t2,4) + 574*Power(t2,5) - 
            579*t1*Power(t2,5) - 803*Power(t1,2)*Power(t2,5) - 
            373*Power(t1,3)*Power(t2,5) - 27*Power(t1,4)*Power(t2,5) + 
            63*Power(t2,6) + 316*t1*Power(t2,6) + 
            195*Power(t1,2)*Power(t2,6) + 44*Power(t1,3)*Power(t2,6) - 
            57*Power(t2,7) - 28*t1*Power(t2,7) - 
            23*Power(t1,2)*Power(t2,7) - 6*Power(t2,8) + 
            4*t1*Power(t2,8) + 
            Power(s2,7)*(6 - 12*t2 + 7*Power(t2,2) - 2*Power(t2,3)) - 
            Power(s1,8)*(6*Power(s2,2) + Power(t1,2) + t1*(-4 + t2) + 
               3*(-1 + t2) + s2*(-1 - 7*t1 + 4*t2)) + 
            Power(s2,6)*(33 - 37*t2 - 19*Power(t2,2) + 12*Power(t2,3) + 
               4*Power(t2,4) + 
               t1*(-44 + 80*t2 - 37*Power(t2,2) + 8*Power(t2,3))) + 
            Power(s2,5)*(-42 + 244*t2 - 290*Power(t2,2) + 
               44*Power(t2,3) + 15*Power(t2,4) + 10*Power(t2,5) + 
               Power(t1,2)*(142 - 240*t2 + 87*Power(t2,2) - 
                  10*Power(t2,3)) + 
               t1*(-179 + 152*t2 + 183*Power(t2,2) - 94*Power(t2,3) - 
                  20*Power(t2,4))) - 
            Power(s2,4)*(99 - 342*t2 + 96*Power(t2,2) + 
               294*Power(t2,3) - 100*Power(t2,4) - 21*Power(t2,5) + 
               5*Power(t1,3)*(52 - 84*t2 + 25*Power(t2,2)) + 
               Power(t1,2)*(-439 + 295*t2 + 567*Power(t2,2) - 
                  278*Power(t2,3) - 40*Power(t2,4)) + 
               t1*(-151 + 1095*t2 - 1313*Power(t2,2) + 113*Power(t2,3) + 
                  131*Power(t2,4) + 30*Power(t2,5))) + 
            Power(s2,3)*(415 - 1668*t2 + 2689*Power(t2,2) - 
               1718*Power(t2,3) + 155*Power(t2,4) + 100*Power(t2,5) + 
               12*Power(t2,6) - 4*Power(t2,7) + 
               5*Power(t1,4)*
                (58 - 92*t2 + 25*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(-626 + 400*t2 + 818*Power(t2,2) - 
                  412*Power(t2,3) - 40*Power(t2,4)) + 
               Power(t1,2)*(-171 + 1992*t2 - 2533*Power(t2,2) + 
                  176*Power(t2,3) + 316*Power(t2,4) + 30*Power(t2,5)) + 
               t1*(93 - 500*t2 - 600*Power(t2,2) + 1722*Power(t2,3) - 
                  549*Power(t2,4) - 66*Power(t2,5) + 4*Power(t2,6))) + 
            Power(s2,2)*(-502 + 2256*t2 - 4225*Power(t2,2) + 
               4339*Power(t2,3) - 2378*Power(t2,4) + 483*Power(t2,5) + 
               31*Power(t2,6) - 11*Power(t2,7) - 
               Power(t1,5)*(196 - 312*t2 + 87*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,4)*(539 - 395*t2 - 597*Power(t2,2) + 
                  328*Power(t2,3) + 20*Power(t2,4)) - 
               Power(t1,3)*(-27 + 1846*t2 - 2621*Power(t2,2) + 
                  290*Power(t2,3) + 312*Power(t2,4) + 10*Power(t2,5)) + 
               Power(t1,2)*(220 - 225*t2 + 2028*Power(t2,2) - 
                  3329*Power(t2,3) + 1116*Power(t2,4) + 
                  42*Power(t2,5) - 8*Power(t2,6)) + 
               t1*(-672 + 2565*t2 - 4209*Power(t2,2) + 
                  2282*Power(t2,3) + 630*Power(t2,4) - 585*Power(t2,5) + 
                  40*Power(t2,6) + 6*Power(t2,7))) + 
            s2*(155 - 1026*t2 + 2714*Power(t2,2) - 3734*Power(t2,3) + 
               2884*Power(t2,4) - 1198*Power(t2,5) + 200*Power(t2,6) + 
               16*Power(t2,7) - 12*Power(t2,8) + 
               Power(t1,6)*(74 - 120*t2 + 37*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,5)*(-259 + 232*t2 + 207*Power(t2,2) - 
                  134*Power(t2,3) - 4*Power(t2,4)) + 
               Power(t1,4)*(65 + 876*t2 - 1445*Power(t2,2) + 
                  284*Power(t2,3) + 125*Power(t2,4)) + 
               Power(t1,3)*(-323 + 582*t2 - 1872*Power(t2,2) + 
                  2668*Power(t2,3) - 985*Power(t2,4) + 30*Power(t2,5) + 
                  4*Power(t2,6)) + 
               Power(t1,2)*(104 - 90*t2 + 186*Power(t2,2) + 
                  814*Power(t2,3) - 1831*Power(t2,4) + 
                  858*Power(t2,5) - 96*Power(t2,6) - 2*Power(t2,7)) + 
               t1*(844 - 3420*t2 + 5411*Power(t2,2) - 4426*Power(t2,3) + 
                  1653*Power(t2,4) + 162*Power(t2,5) - 260*Power(t2,6) + 
                  50*Power(t2,7))) - 
            Power(s1,7)*(10 + 38*Power(s2,3) + Power(t1,3) + 
               Power(s2,2)*(11 - 70*t1 - 11*t2) + 7*t2 - 
               17*Power(t2,2) - 7*Power(t1,2)*(-3 + 2*t2) + 
               t1*(32 + 2*t2 - 3*Power(t2,2)) + 
               s2*(-49 + 31*Power(t1,2) + 43*t2 - 25*Power(t2,2) + 
                  t1*(-37 + 30*t2))) - 
            Power(s1,6)*(44 + 52*Power(s2,4) + 4*Power(t1,4) + 
               Power(t1,3)*(94 - 32*t2) - 129*t2 + 49*Power(t2,2) + 
               36*Power(t2,3) - 2*Power(s2,3)*(-15 + 57*t1 + 64*t2) + 
               2*Power(t1,2)*(3 - 95*t2 + 33*Power(t2,2)) + 
               t1*(6 - 195*t2 + 90*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*(-214 + 76*Power(t1,2) + 79*t2 - 
                  22*Power(t2,2) + t1*(9 + 242*t2)) + 
               s2*(44 - 18*Power(t1,3) + 172*t2 - 183*Power(t2,2) + 
                  68*Power(t2,3) - Power(t1,2)*(133 + 82*t2) + 
                  t1*(197 + 144*t2 - 66*Power(t2,2)))) + 
            Power(s1,5)*(87 - 29*Power(s2,5) - 2*Power(t1,5) + 56*t2 - 
               439*Power(t2,2) + 266*Power(t2,3) + 30*Power(t2,4) + 
               Power(t1,4)*(-17 + 20*t2) + 
               2*Power(s2,4)*(5 + 25*t1 + 89*t2) + 
               Power(t1,3)*(-85 + 387*t2 - 113*Power(t2,2)) + 
               Power(t1,2)*(-129 + 317*t2 - 620*Power(t2,2) + 
                  145*Power(t2,3)) + 
               t1*(148 + 15*t2 - 630*Power(t2,2) + 333*Power(t2,3) - 
                  18*Power(t2,4)) + 
               Power(s2,3)*(215 + 26*Power(t1,2) + t2 - 
                  143*Power(t2,2) - 2*t1*(48 + 217*t2)) + 
               Power(s2,2)*(71 - 88*Power(t1,3) - 748*t2 + 
                  381*Power(t2,2) - 88*Power(t2,3) + 
                  Power(t1,2)*(145 + 354*t2) + 
                  t1*(-330 + 95*t2 + 284*Power(t2,2))) + 
               s2*(-388 + 43*Power(t1,4) + 683*t2 + 63*Power(t2,2) - 
                  308*Power(t2,3) + 102*Power(t2,4) - 
                  2*Power(t1,3)*(21 + 59*t2) + 
                  Power(t1,2)*(200 - 483*t2 - 28*Power(t2,2)) + 
                  t1*(10 + 496*t2 + 263*Power(t2,2) - 98*Power(t2,3)))) + 
            Power(s1,4)*(183 - 7*Power(s2,6) + 7*Power(t1,6) + 
               Power(t1,5)*(84 - 32*t2) - 724*t2 + 433*Power(t2,2) + 
               645*Power(t2,3) - 542*Power(t2,4) + 5*Power(t2,5) + 
               Power(s2,5)*(55 + 2*t1 + 112*t2) + 
               2*Power(t1,4)*(-36 - 60*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(29 + 323*t2 - 509*Power(t2,2) + 
                  134*Power(t2,3)) + 
               Power(t1,2)*(-67 + 424*t2 - 1011*Power(t2,2) + 
                  953*Power(t2,3) - 160*Power(t2,4)) + 
               t1*(-92 - 272*t2 - 189*Power(t2,2) + 1186*Power(t2,3) - 
                  530*Power(t2,4) + 27*Power(t2,5)) + 
               Power(s2,4)*(-73 + 69*Power(t1,2) + 9*t2 - 
                  184*Power(t2,2) - t1*(289 + 308*t2)) + 
               Power(s2,3)*(257 - 156*Power(t1,3) - 1018*t2 + 
                  258*Power(t2,2) + 62*Power(t2,3) + 
                  Power(t1,2)*(453 + 284*t2) + 
                  t1*(513 + 40*t2 + 442*Power(t2,2))) + 
               Power(s2,2)*(-551 + 139*Power(t1,4) + 493*t2 + 
                  697*Power(t2,2) - 562*Power(t2,3) + 127*Power(t2,4) - 
                  Power(t1,3)*(175 + 124*t2) - 
                  Power(t1,2)*(879 + 227*t2 + 318*Power(t2,2)) - 
                  t1*(350 - 1729*t2 + 348*Power(t2,2) + 182*Power(t2,3))\
) + s2*(104 - 54*Power(t1,5) + 1114*t2 - 1939*Power(t2,2) + 
                  458*Power(t2,3) + 221*Power(t2,4) - 88*Power(t2,5) + 
                  4*Power(t1,4)*(-32 + 17*t2) + 
                  Power(t1,3)*(511 + 298*t2 + 46*Power(t2,2)) + 
                  Power(t1,2)*
                   (64 - 1034*t2 + 599*Power(t2,2) - 14*Power(t2,3)) + 
                  t1*(474 - 450*t2 - 160*Power(t2,2) - 282*Power(t2,3) + 
                     75*Power(t2,4)))) + 
            Power(s1,3)*(-202 - 9*Power(s2,7) + 3*Power(t1,7) + 
               Power(t1,6)*(55 - 37*t2) + 
               Power(s2,6)*(-5 + 54*t1 - 16*t2) - 64*t2 + 
               1342*Power(t2,2) - 1221*Power(t2,3) - 396*Power(t2,4) + 
               568*Power(t2,5) - 27*Power(t2,6) + 
               Power(t1,5)*(212 - 383*t2 + 125*Power(t2,2)) + 
               Power(t1,4)*(147 - 430*t2 + 580*Power(t2,2) - 
                  129*Power(t2,3)) + 
               Power(t1,3)*(-260 - 316*t2 + 181*Power(t2,2) + 
                  87*Power(t2,3) - 26*Power(t2,4)) + 
               Power(t1,2)*(-190 + 293*t2 - 160*Power(t2,2) + 
                  897*Power(t2,3) - 707*Power(t2,4) + 81*Power(t2,5)) + 
               t1*(-271 + 581*t2 + 76*Power(t2,2) + 309*Power(t2,3) - 
                  1199*Power(t2,4) + 434*Power(t2,5) - 17*Power(t2,6)) - 
               Power(s2,5)*(-328 + 138*Power(t1,2) + 121*t2 + 
                  198*Power(t2,2) - 6*t1*(-17 + 28*t2)) + 
               Power(s2,4)*(-808 + 195*Power(t1,3) + 876*t2 + 
                  15*Power(t2,2) + Power(t2,3) - 
                  9*Power(t1,2)*(-57 + 61*t2) + 
                  t1*(-1124 + 299*t2 + 749*Power(t2,2))) + 
               Power(s2,3)*(751 - 165*Power(t1,4) - 1912*t2 + 
                  2121*Power(t2,2) - 412*Power(t2,3) - 33*Power(t2,4) + 
                  Power(t1,3)*(-932 + 836*t2) - 
                  4*Power(t1,2)*(-298 - 53*t2 + 296*Power(t2,2)) + 
                  t1*(2808 - 3202*t2 - 241*Power(t2,2) + 
                     142*Power(t2,3))) + 
               Power(s2,2)*(-823 + 84*Power(t1,5) + 
                  Power(t1,4)*(813 - 654*t2) + 3019*t2 - 
                  2558*Power(t2,2) + 309*Power(t2,3) + 
                  376*Power(t2,4) - 112*Power(t2,5) + 
                  2*Power(t1,3)*(-56 - 610*t2 + 519*Power(t2,2)) + 
                  Power(t1,2)*
                   (-3045 + 3346*t2 + 1017*Power(t2,2) - 
                     416*Power(t2,3)) + 
                  t1*(-2172 + 4042*t2 - 3705*Power(t2,2) + 
                     265*Power(t2,3) + 205*Power(t2,4))) + 
               s2*(818 - 24*Power(t1,6) - 2114*t2 + 249*Power(t2,2) + 
                  1876*Power(t2,3) - 774*Power(t2,4) - 9*Power(t2,5) + 
                  41*Power(t2,6) + 18*Power(t1,5)*(-19 + 14*t2) + 
                  Power(t1,4)*(-496 + 1213*t2 - 530*Power(t2,2)) + 
                  Power(t1,3)*
                   (898 - 590*t2 - 1371*Power(t2,2) + 402*Power(t2,3)) + 
                  Power(t1,2)*
                   (1681 - 1814*t2 + 1403*Power(t2,2) + 
                     60*Power(t2,3) - 146*Power(t2,4)) + 
                  t1*(999 - 2946*t2 + 1755*Power(t2,2) - 
                     330*Power(t2,3) + 91*Power(t2,4) + 6*Power(t2,5)))) \
+ Power(s1,2)*(-330 - 3*Power(s2,8) - 2*Power(t1,8) + 
               Power(s2,7)*(-28 + 23*t1) + 1531*t2 - 1773*Power(t2,2) - 
               285*Power(t2,3) + 1182*Power(t2,4) - 29*Power(t2,5) - 
               314*Power(t2,6) + 18*Power(t2,7) + 
               Power(t1,7)*(18 + t2) + 
               Power(t1,6)*(45 - 155*t2 + 44*Power(t2,2)) + 
               Power(t1,5)*(305 - 554*t2 + 550*Power(t2,2) - 
                  141*Power(t2,3)) + 
               Power(t1,4)*(480 - 1528*t2 + 1374*Power(t2,2) - 
                  768*Power(t2,3) + 155*Power(t2,4)) - 
               Power(t1,3)*(441 + 293*t2 - 2211*Power(t2,2) + 
                  1378*Power(t2,3) - 321*Power(t2,4) + 52*Power(t2,5)) + 
               Power(t1,2)*(-784 + 1538*t2 + 13*Power(t2,2) - 
                  1384*Power(t2,3) + 193*Power(t2,4) + 
                  188*Power(t2,5) - 9*Power(t2,6)) + 
               t1*(-391 + 1499*t2 - 1364*Power(t2,2) - 
                  331*Power(t2,3) + 259*Power(t2,4) + 549*Power(t2,5) - 
                  172*Power(t2,6) + 4*Power(t2,7)) + 
               Power(s2,6)*(56 - 77*Power(t1,2) - 99*t2 + 
                  31*Power(t2,2) + t1*(164 + 5*t2)) + 
               Power(s2,5)*(334 + 147*Power(t1,3) - 454*t2 - 
                  61*Power(t2,2) + 170*Power(t2,3) - 
                  2*Power(t1,2)*(209 + 13*t2) + 
                  t1*(-386 + 694*t2 - 215*Power(t2,2))) + 
               Power(s2,4)*(-1244 - 175*Power(t1,4) + 2440*t2 - 
                  1293*Power(t2,2) - 159*Power(t2,3) + 95*Power(t2,4) + 
                  Power(t1,3)*(610 + 55*t2) + 
                  3*Power(t1,2)*(343 - 647*t2 + 198*Power(t2,2)) + 
                  t1*(-873 + 1056*t2 + 737*Power(t2,2) - 
                     725*Power(t2,3))) + 
               Power(s2,3)*(1829 + 133*Power(t1,5) - 4342*t2 + 
                  3861*Power(t2,2) - 1908*Power(t2,3) + 
                  192*Power(t2,4) + 36*Power(t2,5) - 
                  20*Power(t1,4)*(28 + 3*t2) - 
                  2*Power(t1,3)*(688 - 1402*t2 + 423*Power(t2,2)) + 
                  Power(t1,2)*
                   (310 + 110*t2 - 2395*Power(t2,2) + 1296*Power(t2,3)) \
+ t1*(3493 - 6762*t2 + 3540*Power(t2,2) + 934*Power(t2,3) - 
                     426*Power(t2,4))) + 
               Power(s2,2)*(-1270 - 63*Power(t1,6) + 5033*t2 - 
                  7139*Power(t2,2) + 3972*Power(t2,3) - 
                  765*Power(t2,4) - 140*Power(t2,5) + 61*Power(t2,6) + 
                  Power(t1,5)*(328 + 35*t2) + 
                  Power(t1,4)*(974 - 2221*t2 + 659*Power(t2,2)) + 
                  Power(t1,3)*
                   (968 - 2126*t2 + 3373*Power(t2,2) - 
                     1238*Power(t2,3)) + 
                  Power(t1,2)*
                   (-2774 + 4676*t2 - 1827*Power(t2,2) - 
                     2159*Power(t2,3) + 722*Power(t2,4)) + 
                  t1*(-4680 + 10317*t2 - 7433*Power(t2,2) + 
                     2778*Power(t2,3) + 257*Power(t2,4) - 
                     204*Power(t2,5))) + 
               s2*(644 + 17*Power(t1,7) - 3804*t2 + 6551*Power(t2,2) - 
                  3606*Power(t2,3) - 245*Power(t2,4) + 504*Power(t2,5) - 
                  89*Power(t2,6) - 8*Power(t2,7) - 
                  2*Power(t1,6)*(57 + 5*t2) - 
                  3*Power(t1,5)*(114 - 306*t2 + 89*Power(t2,2)) + 
                  2*Power(t1,4)*
                   (-522 + 984*t2 - 1102*Power(t2,2) + 319*Power(t2,3)) \
+ Power(t1,3)*(45 + 1174*t2 - 1794*Power(t2,2) + 2152*Power(t2,3) - 
                     546*Power(t2,4)) + 
                  Power(t1,2)*
                   (3292 - 5682*t2 + 1361*Power(t2,2) + 
                     508*Power(t2,3) - 770*Power(t2,4) + 220*Power(t2,5)\
) + t1*(2293 - 7268*t2 + 7551*Power(t2,2) - 2086*Power(t2,3) - 
                     113*Power(t2,4) + 160*Power(t2,5) - 44*Power(t2,6)))\
) + s1*(-23 + 788*t2 - 2682*Power(t2,2) + 3146*Power(t2,3) - 
               994*Power(t2,4) - 469*Power(t2,5) + 157*Power(t2,6) + 
               81*Power(t2,7) - 4*Power(t2,8) + Power(t1,8)*(-5 + 3*t2) + 
               Power(s2,8)*(-4 + 3*t2) + 
               Power(t1,7)*(19 - 2*t2 - 7*Power(t2,2)) + 
               Power(s2,7)*(-18 + t1*(33 - 24*t2) + 5*t2 + 
                  11*Power(t2,2)) + 
               Power(t1,6)*(8 - 100*t2 + 81*Power(t2,2) - 
                  11*Power(t2,3)) + 
               Power(t1,5)*(216 - 463*t2 + 461*Power(t2,2) - 
                  251*Power(t2,3) + 49*Power(t2,4)) + 
               Power(t1,4)*(344 - 1517*t2 + 2069*Power(t2,2) - 
                  1146*Power(t2,3) + 358*Power(t2,4) - 56*Power(t2,5)) + 
               Power(t1,3)*(-53 - 511*t2 + 2402*Power(t2,2) - 
                  3047*Power(t2,3) + 1311*Power(t2,4) - 
                  236*Power(t2,5) + 26*Power(t2,6)) - 
               Power(t1,2)*(744 - 1998*t2 + 1025*Power(t2,2) + 
                  1692*Power(t2,3) - 2088*Power(t2,4) + 
                  585*Power(t2,5) - 40*Power(t2,6) + 4*Power(t2,7)) + 
               t1*(-594 + 2516*t2 - 3667*Power(t2,2) + 
                  1515*Power(t2,3) + 938*Power(t2,4) - 704*Power(t2,5) - 
                  41*Power(t2,6) + 19*Power(t2,7)) + 
               Power(s2,6)*(62 - 129*t2 + 72*Power(t2,2) - 
                  12*Power(t2,3) + 7*Power(t1,2)*(-17 + 12*t2) - 
                  2*t1*(-56 + 10*t2 + 35*Power(t2,2))) - 
               Power(s2,5)*(15 + 11*t2 + 21*Power(t2,2) - 
                  104*Power(t2,3) + 65*Power(t2,4) + 
                  7*Power(t1,3)*(-35 + 24*t2) - 
                  3*Power(t1,2)*(-103 + 9*t2 + 64*Power(t2,2)) + 
                  t1*(294 - 682*t2 + 393*Power(t2,2) - 62*Power(t2,3))) + 
               Power(s2,4)*(-723 + 1651*t2 - 1301*Power(t2,2) + 
                  346*Power(t2,3) + 104*Power(t2,4) - 38*Power(t2,5) + 
                  105*Power(t1,4)*(-3 + 2*t2) - 
                  5*Power(t1,3)*(-99 + 2*t2 + 59*Power(t2,2)) + 
                  Power(t1,2)*
                   (564 - 1538*t2 + 933*Power(t2,2) - 139*Power(t2,3)) + 
                  t1*(382 - 589*t2 + 575*Power(t2,2) - 587*Power(t2,3) + 
                     263*Power(t2,4))) - 
               Power(s2,3)*(-1574 + 5078*t2 - 5472*Power(t2,2) + 
                  2330*Power(t2,3) - 487*Power(t2,4) + 21*Power(t2,5) + 
                  8*Power(t2,6) + 7*Power(t1,5)*(-37 + 24*t2) + 
                  5*Power(t1,4)*(100 + t2 - 55*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (-278 + 956*t2 - 621*Power(t2,2) + 88*Power(t2,3)) + 
                  4*Power(t1,2)*
                   (318 - 574*t2 + 515*Power(t2,2) - 347*Power(t2,3) + 
                     112*Power(t2,4)) + 
                  t1*(-1699 + 3290*t2 - 2047*Power(t2,2) + 
                     210*Power(t2,3) + 573*Power(t2,4) - 158*Power(t2,5))\
) + Power(s2,2)*(-1277 + 5284*t2 - 8902*Power(t2,2) + 7126*Power(t2,3) - 
                  2451*Power(t2,4) + 262*Power(t2,5) + 46*Power(t2,6) - 
                  15*Power(t2,7) + 7*Power(t1,6)*(-19 + 12*t2) - 
                  6*Power(t1,5)*(-53 + 26*Power(t2,2)) + 
                  Power(t1,4)*
                   (294 - 1393*t2 + 978*Power(t2,2) - 134*Power(t2,3)) + 
                  2*Power(t1,3)*
                   (837 - 1622*t2 + 1470*Power(t2,2) - 
                     841*Power(t2,3) + 208*Power(t2,4)) + 
                  Power(t1,2)*
                   (-885 + 110*t2 + 1878*Power(t2,2) - 
                     1764*Power(t2,3) + 1192*Power(t2,4) - 
                     258*Power(t2,5)) + 
                  t1*(-3387 + 10661*t2 - 10386*Power(t2,2) + 
                     2945*Power(t2,3) + 104*Power(t2,4) - 
                     300*Power(t2,5) + 63*Power(t2,6))) + 
               s2*(418 + Power(t1,7)*(39 - 24*t2) - 2628*t2 + 
                  6484*Power(t2,2) - 7486*Power(t2,3) + 
                  3849*Power(t2,4) - 531*Power(t2,5) - 144*Power(t2,6) + 
                  56*Power(t2,7) + 
                  Power(t1,6)*(-117 + 5*t2 + 50*Power(t2,2)) + 
                  Power(t1,5)*
                   (-78 + 566*t2 - 429*Power(t2,2) + 58*Power(t2,3)) + 
                  Power(t1,4)*
                   (-985 + 2011*t2 - 1895*Power(t2,2) + 
                     1028*Power(t2,3) - 215*Power(t2,4)) + 
                  Power(t1,3)*
                   (-435 + 3046*t2 - 4693*Power(t2,2) + 
                     2774*Power(t2,3) - 1081*Power(t2,4) + 
                     194*Power(t2,5)) + 
                  Power(t1,2)*
                   (1866 - 5072*t2 + 2512*Power(t2,2) + 
                     2432*Power(t2,3) - 1902*Power(t2,4) + 
                     557*Power(t2,5) - 81*Power(t2,6)) + 
                  t1*(2220 - 8282*t2 + 11741*Power(t2,2) - 
                     6798*Power(t2,3) + 563*Power(t2,4) + 
                     564*Power(t2,5) - 175*Power(t2,6) + 18*Power(t2,7))))\
))*R1(1 - s + s1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + s - s1 + t2)*(s - s1 + t2)*
       (-1 + s2 - t1 + t2)*Power(-s1 + s2 - t1 + t2,3)*
       (-1 + s - s*s1 + s1*s2 - s1*t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) + 
    (8*(-97 + 193*s2 - 29*Power(s2,2) - 67*Power(s2,3) - 2*Power(s2,4) + 
         2*Power(s2,5) - 86*t1 - 27*s2*t1 + 164*Power(s2,2)*t1 + 
         21*Power(s2,3)*t1 - 8*Power(s2,4)*t1 + 54*Power(t1,2) - 
         137*s2*Power(t1,2) - 41*Power(s2,2)*Power(t1,2) + 
         14*Power(s2,3)*Power(t1,2) + 40*Power(t1,3) + 
         27*s2*Power(t1,3) - 14*Power(s2,2)*Power(t1,3) - 5*Power(t1,4) + 
         8*s2*Power(t1,4) - 2*Power(t1,5) + 
         2*Power(s1,5)*(-s2 + 2*Power(s2,2) + t1 - 3*s2*t1 + 
            Power(t1,2)) + 185*t2 - 285*s2*t2 - 40*Power(s2,2)*t2 + 
         65*Power(s2,3)*t2 + 13*Power(s2,4)*t2 - 2*Power(s2,5)*t2 + 
         16*t1*t2 + 182*s2*t1*t2 - 124*Power(s2,2)*t1*t2 - 
         62*Power(s2,3)*t1*t2 + 4*Power(s2,4)*t1*t2 - 
         148*Power(t1,2)*t2 + 91*s2*Power(t1,2)*t2 + 
         96*Power(s2,2)*Power(t1,2)*t2 - 32*Power(t1,3)*t2 - 
         58*s2*Power(t1,3)*t2 - 4*Power(s2,2)*Power(t1,3)*t2 + 
         11*Power(t1,4)*t2 + 2*s2*Power(t1,4)*t2 - 27*Power(t2,2) + 
         35*s2*Power(t2,2) + 74*Power(s2,2)*Power(t2,2) + 
         Power(s2,3)*Power(t2,2) - 9*Power(s2,4)*Power(t2,2) + 
         2*Power(s2,5)*Power(t2,2) + 184*t1*Power(t2,2) - 
         179*s2*t1*Power(t2,2) - 44*Power(s2,2)*t1*Power(t2,2) + 
         37*Power(s2,3)*t1*Power(t2,2) - 6*Power(s2,4)*t1*Power(t2,2) + 
         121*Power(t1,2)*Power(t2,2) + 51*s2*Power(t1,2)*Power(t2,2) - 
         53*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         6*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         8*Power(t1,3)*Power(t2,2) + 31*s2*Power(t1,3)*Power(t2,2) - 
         2*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         6*Power(t1,4)*Power(t2,2) - 97*Power(t2,3) + 57*s2*Power(t2,3) + 
         3*Power(s2,2)*Power(t2,3) - 5*Power(s2,3)*Power(t2,3) - 
         2*Power(s2,4)*Power(t2,3) - 134*t1*Power(t2,3) + 
         24*s2*t1*Power(t2,3) + 22*Power(s2,2)*t1*Power(t2,3) + 
         4*Power(s2,3)*t1*Power(t2,3) - 31*Power(t1,2)*Power(t2,3) - 
         23*s2*Power(t1,2)*Power(t2,3) - 
         2*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         6*Power(t1,3)*Power(t2,3) + 36*Power(t2,4) - 
         12*Power(s2,2)*Power(t2,4) + 20*t1*Power(t2,4) + 
         8*s2*t1*Power(t2,4) + 
         Power(s,5)*(4 - 2*s2 - t1 - 4*Power(t1,2) - t2 + 8*t1*t2 - 
            4*Power(t2,2) + s1*(-4 + 2*s2 + t1 + t2)) + 
         Power(s1,4)*(2 + 7*Power(s2,3) - 5*Power(t1,3) + 
            Power(t1,2)*(-15 + t2) - 2*t2 - 2*t1*(2 + t2) - 
            Power(s2,2)*(13 + 17*t1 + 9*t2) + 
            s2*(2 + 15*Power(t1,2) + 4*t2 + 4*t1*(7 + 2*t2))) - 
         Power(s,4)*(-19 - 8*Power(s2,2) - 15*t1 - 3*Power(t1,2) + 
            10*Power(t1,3) + 3*t2 + 7*t1*t2 - 18*Power(t1,2)*t2 + 
            4*Power(t2,2) + 6*t1*Power(t2,2) + 2*Power(t2,3) + 
            Power(s1,2)*(-8 + s2 + 5*t1 + 2*t2) + 
            s2*(32 + 13*t1 - 10*Power(t1,2) - 14*t2 + 28*t1*t2 - 
               18*Power(t2,2)) + 
            s1*(31 + 10*Power(s2,2) + 5*t1 - 16*Power(t1,2) - 2*t2 + 
               21*t1*t2 - 13*Power(t2,2) + s2*(-39 - 6*t1 + 9*t2))) + 
         Power(s1,3)*(2*Power(s2,4) + 4*Power(t1,4) + 6*(-1 + t2) + 
            Power(t1,3)*(-1 + 4*t2) - Power(s2,3)*(6*t1 + 19*t2) + 
            Power(t1,2)*(-3 + 41*t2 - 6*Power(t2,2)) - 
            2*t1*(2 - 18*t2 + 5*Power(t2,2)) + 
            Power(s2,2)*(20 + 10*Power(t1,2) + 33*t2 + 7*Power(t2,2) + 
               t1*(-3 + 38*t2)) - 
            s2*(-2 + 10*Power(t1,3) + 24*t2 + Power(t1,2)*(-4 + 23*t2) + 
               t1*(19 + 72*t2 + Power(t2,2)))) + 
         Power(s1,2)*(11 - 3*Power(s2,5) - Power(t1,5) + 
            Power(s2,4)*(-1 + 13*t1 - 21*t2) + 
            Power(t1,4)*(21 - 11*t2) + 9*t2 - 29*Power(t2,2) + 
            9*Power(t2,3) + 3*Power(t1,3)*(1 - 22*t2 + 4*Power(t2,2)) + 
            2*t1*t2*(-1 - 21*t2 + 5*Power(t2,2)) + 
            2*Power(t1,2)*(-11 - 17*t2 + 7*Power(t2,2)) + 
            Power(s2,3)*(79 - 20*Power(t1,2) + 4*t2 + 2*Power(t2,2) + 
               4*t1*(-5 + 17*t2)) + 
            Power(s2,2)*(-79 + 12*Power(t1,3) + 
               Power(t1,2)*(64 - 84*t2) + 34*t2 - 29*Power(t2,2) - 
               8*Power(t2,3) + t1*(-151 - 78*t2 + 14*Power(t2,2))) + 
            s2*(-25 - Power(t1,4) + 37*t2 + 19*Power(t2,2) + 
               3*Power(t2,3) + 16*Power(t1,3)*(-4 + 3*t2) + 
               Power(t1,2)*(69 + 140*t2 - 28*Power(t2,2)) + 
               t1*(99 + 10*t2 + 7*Power(t2,2) + 8*Power(t2,3)))) - 
         s1*(2 + 2*Power(s2,6) + Power(t1,5)*(7 - 6*t2) + 34*t2 - 
            36*Power(t2,2) - 4*Power(t2,3) + 4*Power(t2,4) + 
            Power(s2,5)*(-2 - 10*t1 + 11*t2) + 
            Power(t1,3)*(-73 + 124*t2 - 42*Power(t2,2)) + 
            Power(s2,4)*(-70 + 20*Power(t1,2) + t1*(23 - 48*t2) + t2 + 
               3*Power(t2,2)) + 
            Power(t1,4)*(-35 - 3*t2 + 6*Power(t2,2)) + 
            2*t1*(57 - 33*t2 - 59*Power(t2,2) + 27*Power(t2,3)) + 
            Power(t1,2)*(81 + 129*t2 - 188*Power(t2,2) + 
               32*Power(t2,3)) + 
            Power(s2,3)*(-30 - 20*Power(t1,3) - 89*t2 + 
               24*Power(t2,2) + 10*Power(t2,3) + 
               Power(t1,2)*(-64 + 84*t2) + 
               t1*(241 + 24*t2 - 23*Power(t2,2))) + 
            Power(s2,2)*(274 + 10*Power(t1,4) - 
               74*Power(t1,3)*(-1 + t2) - 171*t2 - 63*Power(t2,2) - 
               4*Power(t2,3) + 
               Power(t1,2)*(-307 - 54*t2 + 43*Power(t2,2)) - 
               t1*(19 - 296*t2 + 78*Power(t2,2) + 18*Power(t2,3))) + 
            s2*(-2*Power(t1,5) + Power(t1,4)*(-38 + 33*t2) + 
               Power(t1,3)*(171 + 32*t2 - 29*Power(t2,2)) + 
               t1*(-353 + 36*t2 + 263*Power(t2,2) - 36*Power(t2,3)) + 
               Power(t1,2)*(122 - 331*t2 + 96*Power(t2,2) + 
                  8*Power(t2,3)) + 
               2*(-88 + 91*t2 + 20*Power(t2,2) - 19*Power(t2,3) + 
                  4*Power(t2,4)))) + 
         Power(s,3)*(-22 - 12*Power(s2,3) + 15*t1 + 38*Power(t1,2) + 
            23*Power(t1,3) - 8*Power(t1,4) + 22*t2 - 64*t1*t2 - 
            58*Power(t1,2)*t2 + 12*Power(t1,3)*t2 + 28*Power(t2,2) + 
            44*t1*Power(t2,2) - 21*Power(t2,3) - 4*t1*Power(t2,3) + 
            Power(s1,3)*(-2 - 8*s2 + 9*t1 + t2) + 
            Power(s1,2)*(38 + 8*Power(s2,2) - 28*Power(t1,2) + t2 - 
               14*Power(t2,2) + t1*(-4 + 21*t2) + 
               s2*(-53 + 6*t1 + 25*t2)) + 
            Power(s2,2)*(38 - 8*Power(t1,2) - 36*t2 - 32*Power(t2,2) + 
               t1*(33 + 36*t2)) + 
            s2*(13 + 16*Power(t1,3) - 10*t2 + 5*Power(t2,2) + 
               8*Power(t2,3) - 4*Power(t1,2)*(11 + 12*t2) + 
               12*t1*(-5 + 6*t2 + 2*Power(t2,2))) + 
            s1*(-33 + 20*Power(s2,3) + 30*Power(t1,3) + t2 - 
               9*Power(t2,2) + 10*Power(t2,3) - 
               Power(t1,2)*(16 + 29*t2) + 
               Power(s2,2)*(-91 - 34*t1 + 32*t2) + 
               t1*(-66 + 46*t2 + Power(t2,2)) + 
               s2*(104 - 16*Power(t1,2) - 46*t2 - 36*Power(t2,2) + 
                  3*t1*(37 + 7*t2)))) + 
         Power(s,2)*(-94 + 8*Power(s2,4) + 
            Power(s1,4)*(-4 + 11*s2 - 7*t1) - 169*t1 - 57*Power(t1,2) + 
            31*Power(t1,3) + 19*Power(t1,4) - 2*Power(t1,5) + 106*t2 + 
            119*t1*t2 - 50*Power(t1,2)*t2 - 45*Power(t1,3)*t2 + 
            2*Power(t1,4)*t2 - 59*Power(t2,2) - 46*t1*Power(t2,2) + 
            16*Power(t1,2)*Power(t2,2) + 2*Power(t1,3)*Power(t2,2) + 
            39*Power(t2,3) + 14*t1*Power(t2,3) - 
            2*Power(t1,2)*Power(t2,3) - 12*Power(t2,4) + 
            Power(s1,3)*(-3 + s2 + 16*Power(s2,2) + 25*t1 - 34*s2*t1 + 
               26*Power(t1,2) - 2*t2 - 25*s2*t2 - 9*t1*t2 + 
               5*Power(t2,2)) + 
            Power(s2,3)*(-6 + 2*Power(t1,2) + 32*t2 + 28*Power(t2,2) - 
               t1*(27 + 20*t2)) - 
            Power(s2,2)*(193 + 6*Power(t1,3) - 54*t2 + 7*Power(t2,2) + 
               12*Power(t2,3) - 7*Power(t1,2)*(7 + 6*t2) + 
               t1*(-39 + 119*t2 + 36*Power(t2,2))) + 
            s2*(267 + 6*Power(t1,4) - 199*t2 + 5*Power(t2,2) + 
               37*Power(t2,3) - Power(t1,3)*(49 + 24*t2) + 
               2*Power(t1,2)*(-32 + 66*t2 + 3*Power(t2,2)) + 
               t1*(238 + 40*t2 - 47*Power(t2,2) + 12*Power(t2,3))) + 
            Power(s1,2)*(1 - 16*Power(s2,3) - 36*Power(t1,3) + 
               Power(s2,2)*(97 + 20*t1 - 65*t2) - 27*t2 + 
               26*Power(t2,2) - 8*Power(t2,3) + 
               Power(t1,2)*(-8 + 17*t2) + 
               t1*(58 - 69*t2 + 6*Power(t2,2)) + 
               s2*(-69 + 32*Power(t1,2) + 76*t2 + 30*Power(t2,2) + 
                  5*t1*(-19 + 6*t2))) + 
            s1*(131 - 20*Power(s2,4) + 18*Power(t1,4) + 
               Power(s2,3)*(79 + 56*t1 - 52*t2) - 92*t2 - 
               27*Power(t2,2) + 20*Power(t2,3) + 
               Power(t1,3)*(-74 + 9*t2) + 
               Power(t1,2)*(-74 + 209*t2 - 37*Power(t2,2)) + 
               t1*(89 + 105*t2 - 114*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s2,2)*(67 - 34*Power(t1,2) + 77*t2 + 
                  30*Power(t2,2) + t1*(-220 + 69*t2)) - 
               s2*(273 + 20*Power(t1,3) - 151*t2 + 2*Power(t2,2) + 
                  30*Power(t2,3) + Power(t1,2)*(-215 + 26*t2) + 
                  t1*(7 + 262*t2 - 21*Power(t2,2))))) - 
         s*(-190 + 2*Power(s2,5) - 226*t1 + 40*Power(t1,2) + 
            86*Power(t1,3) + 6*Power(t1,4) - 4*Power(t1,5) + 
            Power(s1,5)*(4*s2 - 2*(1 + t1)) + 277*t2 + 72*t1*t2 - 
            196*Power(t1,2)*t2 - 58*Power(t1,3)*t2 + 9*Power(t1,4)*t2 - 
            50*Power(t2,2) + 152*t1*Power(t2,2) + 
            142*Power(t1,2)*Power(t2,2) + 14*Power(t1,3)*Power(t2,2) - 
            13*Power(t2,3) - 52*t1*Power(t2,3) - 
            25*Power(t1,2)*Power(t2,3) - 8*Power(t2,4) + 
            8*t1*Power(t2,4) + 
            Power(s2,4)*(6 + 7*t2 + 12*Power(t2,2) - 4*t1*(2 + t2)) + 
            Power(s1,4)*(8 + 18*Power(s2,2) + 12*Power(t1,2) - 
               t1*(-17 + t2) - s2*(17 + 28*t1 + 9*t2)) + 
            Power(s2,3)*(-87 + 46*t2 - 15*Power(t2,2) - 8*Power(t2,3) + 
               4*Power(t1,2)*(4 + 3*t2) - 
               2*t1*(3 + 25*t2 + 12*Power(t2,2))) + 
            Power(s2,2)*(-170 + 42*t2 + 10*Power(t2,2) + 
               11*Power(t2,3) - 4*Power(t1,3)*(5 + 3*t2) + 
               4*Power(t1,2)*t2*(22 + 3*t2) + 
               2*t1*(121 - 53*t2 + 17*Power(t2,2) + 6*Power(t2,3))) + 
            s2*(439 - 464*t2 + 59*Power(t2,2) + 18*Power(t2,3) - 
               24*Power(t2,4) + 2*Power(t1,4)*(7 + 2*t2) - 
               6*Power(t1,3)*(1 + 9*t2) - 
               Power(t1,2)*(241 - 118*t2 + 33*Power(t2,2) + 
                  4*Power(t2,3)) + 
               2*t1*(63 + 80*t2 - 87*Power(t2,2) + 18*Power(t2,3))) + 
            Power(s1,2)*(30 - 12*Power(s2,4) + 14*Power(t1,4) + 
               Power(s2,3)*(51 + 34*t1 - 63*t2) + 51*t2 - 
               26*Power(t2,2) + Power(t2,3) + 
               Power(t1,3)*(-39 + 17*t2) + 
               Power(t1,2)*(-46 + 175*t2 - 32*Power(t2,2)) + 
               t1*(7 + 143*t2 - 76*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(62 - 18*Power(t1,2) + 79*t2 + 
                  18*Power(t2,2) + t1*(-141 + 119*t2)) - 
               s2*(112 + 18*Power(t1,3) + 15*t2 + 7*Power(t2,2) + 
                  16*Power(t2,3) + Power(t1,2)*(-129 + 73*t2) + 
                  t1*(22 + 242*t2 - 20*Power(t2,2)))) + 
            Power(s1,3)*(-17 + 10*Power(s2,3) - 21*Power(t1,3) + 
               Power(s2,2)*(1 - 29*t1 - 43*t2) - 20*t2 + 9*Power(t2,2) + 
               Power(t1,2)*(-34 + 6*t2) + 
               t1*(-1 - 30*t2 + Power(t2,2)) + 
               s2*(40*Power(t1,2) + t1*(29 + 33*t2) + 
                  6*(2 + 7*t2 + 2*Power(t2,2)))) - 
            s1*(-93 + 10*Power(s2,5) + 3*Power(t1,5) + 106*t2 + 
               119*Power(t2,2) - 90*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,4)*(-54 + 22*t2) + 
               Power(s2,4)*(-25 - 39*t1 + 39*t2) + 
               Power(t1,3)*(6 + 124*t2 - 31*Power(t2,2)) + 
               Power(s2,3)*(-218 + 129*t1 + 54*Power(t1,2) - 32*t2 - 
                  117*t1*t2 - 4*Power(t2,2)) + 
               t1*(53 - 216*t2 + 231*Power(t2,2) - 48*Power(t2,3)) + 
               Power(t1,2)*(193 - 68*t2 - 3*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,2)*(352 - 28*Power(t1,3) - 301*t2 + 
                  35*Power(t2,2) + 30*Power(t2,3) + 
                  Power(t1,2)*(-237 + 139*t2) + 
                  t1*(440 + 212*t2 - 45*Power(t2,2))) + 
               s2*(10 + Power(t1,3)*(187 - 83*t2) + 72*t2 - 
                  136*Power(t2,2) - 24*Power(t2,3) + 
                  4*Power(t1,2)*(-57 - 76*t2 + 20*Power(t2,2)) + 
                  t1*(-545 + 339*t2 + 4*Power(t2,2) - 36*Power(t2,3))))))*
       R2(1 - s2 + t1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(s - s1 + t2)*(-1 + s2 - t1 + t2)*
       (-1 + s - s*s1 + s1*s2 - s1*t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) - 
    (8*(183 - 227*s2 - 66*Power(s2,2) + 91*Power(s2,3) + 24*Power(s2,4) - 
         4*Power(s2,5) - Power(s2,6) + 2*Power(s1,7)*Power(s2 - t1,2) + 
         74*t1 + 354*s2*t1 - 293*Power(s2,2)*t1 - 148*Power(s2,3)*t1 + 
         27*Power(s2,4)*t1 + 4*Power(s2,5)*t1 - 2*Power(s2,6)*t1 - 
         282*Power(t1,2) + 312*s2*Power(t1,2) + 
         308*Power(s2,2)*Power(t1,2) - 83*Power(s2,3)*Power(t1,2) - 
         8*Power(s2,4)*Power(t1,2) + 16*Power(s2,5)*Power(t1,2) + 
         Power(s2,6)*Power(t1,2) - 110*Power(t1,3) - 268*s2*Power(t1,3) + 
         127*Power(s2,2)*Power(t1,3) + 12*Power(s2,3)*Power(t1,3) - 
         50*Power(s2,4)*Power(t1,3) - 6*Power(s2,5)*Power(t1,3) + 
         84*Power(t1,4) - 93*s2*Power(t1,4) - 
         13*Power(s2,2)*Power(t1,4) + 80*Power(s2,3)*Power(t1,4) + 
         15*Power(s2,4)*Power(t1,4) + 26*Power(t1,5) + 8*s2*Power(t1,5) - 
         70*Power(s2,2)*Power(t1,5) - 20*Power(s2,3)*Power(t1,5) - 
         2*Power(t1,6) + 32*s2*Power(t1,6) + 15*Power(s2,2)*Power(t1,6) - 
         6*Power(t1,7) - 6*s2*Power(t1,7) + Power(t1,8) + 
         2*Power(s,8)*Power(t1 - t2,2) - 497*t2 + 330*s2*t2 + 
         233*Power(s2,2)*t2 - 20*Power(s2,3)*t2 - 47*Power(s2,4)*t2 - 
         20*Power(s2,5)*t2 + 3*Power(s2,6)*t2 + 2*Power(s2,7)*t2 + 
         227*t1*t2 - 1084*s2*t1*t2 + 15*Power(s2,2)*t1*t2 + 
         300*Power(s2,3)*t1*t2 + 125*Power(s2,4)*t1*t2 - 
         10*Power(s2,5)*t1*t2 - 19*Power(s2,6)*t1*t2 - 
         2*Power(s2,7)*t1*t2 + 847*Power(t1,2)*t2 + 
         11*s2*Power(t1,2)*t2 - 652*Power(s2,2)*Power(t1,2)*t2 - 
         256*Power(s2,3)*Power(t1,2)*t2 + 13*Power(s2,4)*Power(t1,2)*t2 + 
         63*Power(s2,5)*Power(t1,2)*t2 + 12*Power(s2,6)*Power(t1,2)*t2 - 
         6*Power(t1,3)*t2 + 592*s2*Power(t1,3)*t2 + 
         218*Power(s2,2)*Power(t1,3)*t2 - 12*Power(s2,3)*Power(t1,3)*t2 - 
         100*Power(s2,4)*Power(t1,3)*t2 - 30*Power(s2,5)*Power(t1,3)*t2 - 
         193*Power(t1,4)*t2 - 68*s2*Power(t1,4)*t2 + 
         13*Power(s2,2)*Power(t1,4)*t2 + 80*Power(s2,3)*Power(t1,4)*t2 + 
         40*Power(s2,4)*Power(t1,4)*t2 + Power(t1,5)*t2 - 
         10*s2*Power(t1,5)*t2 - 27*Power(s2,2)*Power(t1,5)*t2 - 
         30*Power(s2,3)*Power(t1,5)*t2 + 3*Power(t1,6)*t2 - 
         s2*Power(t1,6)*t2 + 12*Power(s2,2)*Power(t1,6)*t2 + 
         2*Power(t1,7)*t2 - 2*s2*Power(t1,7)*t2 + 281*Power(t2,2) + 
         165*s2*Power(t2,2) - 88*Power(s2,2)*Power(t2,2) - 
         160*Power(s2,3)*Power(t2,2) - 19*Power(s2,4)*Power(t2,2) + 
         4*Power(s2,5)*Power(t2,2) - 3*Power(s2,6)*Power(t2,2) + 
         3*Power(s2,7)*Power(t2,2) + Power(s2,8)*Power(t2,2) - 
         952*t1*Power(t2,2) + 764*s2*t1*Power(t2,2) + 
         704*Power(s2,2)*t1*Power(t2,2) + 23*Power(s2,3)*t1*Power(t2,2) - 
         84*Power(s2,4)*t1*Power(t2,2) + 7*Power(s2,5)*t1*Power(t2,2) - 
         12*Power(s2,6)*t1*Power(t2,2) - 6*Power(s2,7)*t1*Power(t2,2) - 
         698*Power(t1,2)*Power(t2,2) - 875*s2*Power(t1,2)*Power(t2,2) + 
         83*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         247*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         3*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         15*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         15*Power(s2,6)*Power(t1,2)*Power(t2,2) + 
         331*Power(t1,3)*Power(t2,2) - 159*s2*Power(t1,3)*Power(t2,2) - 
         277*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         22*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         20*Power(s2,5)*Power(t1,3)*Power(t2,2) + 
         72*Power(t1,4)*Power(t2,2) + 129*s2*Power(t1,4)*Power(t2,2) + 
         23*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         15*Power(s2,3)*Power(t1,4)*Power(t2,2) + 
         15*Power(s2,4)*Power(t1,4)*Power(t2,2) - 
         19*Power(t1,5)*Power(t2,2) - 9*s2*Power(t1,5)*Power(t2,2) + 
         12*Power(s2,2)*Power(t1,5)*Power(t2,2) - 
         6*Power(s2,3)*Power(t1,5)*Power(t2,2) + 
         Power(t1,6)*Power(t2,2) - 3*s2*Power(t1,6)*Power(t2,2) + 
         Power(s2,2)*Power(t1,6)*Power(t2,2) + 251*Power(t2,3) - 
         324*s2*Power(t2,3) - 211*Power(s2,2)*Power(t2,3) + 
         35*Power(s2,3)*Power(t2,3) + 25*Power(s2,4)*Power(t2,3) + 
         14*Power(s2,5)*Power(t2,3) + 3*Power(s2,6)*Power(t2,3) - 
         Power(s2,7)*Power(t2,3) + 899*t1*Power(t2,3) + 
         208*s2*t1*Power(t2,3) - 346*Power(s2,2)*t1*Power(t2,3) - 
         99*Power(s2,3)*t1*Power(t2,3) - 26*Power(s2,4)*t1*Power(t2,3) - 
         13*Power(s2,5)*t1*Power(t2,3) + 5*Power(s2,6)*t1*Power(t2,3) + 
         29*Power(t1,2)*Power(t2,3) + 536*s2*Power(t1,2)*Power(t2,3) + 
         141*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         10*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         22*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         10*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         225*Power(t1,3)*Power(t2,3) - 85*s2*Power(t1,3)*Power(t2,3) + 
         46*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         18*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         10*Power(s2,4)*Power(t1,3)*Power(t2,3) + 
         18*Power(t1,4)*Power(t2,3) - 28*s2*Power(t1,4)*Power(t2,3) + 
         7*Power(s2,2)*Power(t1,4)*Power(t2,3) - 
         5*Power(s2,3)*Power(t1,4)*Power(t2,3) + 
         4*Power(t1,5)*Power(t2,3) - s2*Power(t1,5)*Power(t2,3) + 
         Power(s2,2)*Power(t1,5)*Power(t2,3) - 264*Power(t2,4) - 
         14*s2*Power(t2,4) + 138*Power(s2,2)*Power(t2,4) + 
         48*Power(s2,3)*Power(t2,4) - 6*Power(s2,4)*Power(t2,4) - 
         6*Power(s2,5)*Power(t2,4) - 238*t1*Power(t2,4) - 
         250*s2*t1*Power(t2,4) - 46*Power(s2,2)*t1*Power(t2,4) + 
         16*Power(s2,3)*t1*Power(t2,4) + 18*Power(s2,4)*t1*Power(t2,4) + 
         112*Power(t1,2)*Power(t2,4) - 28*s2*Power(t1,2)*Power(t2,4) - 
         18*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         18*Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         26*Power(t1,3)*Power(t2,4) + 12*s2*Power(t1,3)*Power(t2,4) + 
         6*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         4*Power(t1,4)*Power(t2,4) + 38*Power(t2,5) + 70*s2*Power(t2,5) - 
         10*Power(s2,2)*Power(t2,5) - 10*Power(s2,3)*Power(t2,5) - 
         10*t1*Power(t2,5) + 16*s2*t1*Power(t2,5) + 
         14*Power(s2,2)*t1*Power(t2,5) - 12*Power(t1,2)*Power(t2,5) - 
         4*s2*Power(t1,2)*Power(t2,5) + 8*Power(t2,6) - 
         Power(s1,6)*(s2 - t1)*
          (4 + Power(t1,3) - Power(t1,2)*(-2 + t2) - 4*t2 + 
            Power(s2,2)*(-8 + t1 + t2) + 
            s2*(7 + 6*t1 - 2*Power(t1,2) + 5*t2) - t1*(5 + 7*t2)) + 
         Power(s,7)*(11*Power(t1,3) - 
            Power(t1,2)*(3 + 10*s1 + 11*s2 + 21*t2) + 
            t2*(1 - 7*s2 + s1*(3 + 3*s2 - 8*t2) - t2 - 15*s2*t2 + 
               Power(t2,2)) + 
            t1*(3 + 3*s2 + 4*t2 + 26*s2*t2 + 9*Power(t2,2) + 
               s1*(-7 + s2 + 18*t2))) - 
         Power(s1,5)*(Power(s2,5) + 3*Power(t1,5) - 
            Power(s2,4)*(7 + t1 - 7*t2) - 2*Power(-1 + t2,2) + 
            Power(t1,4)*(5 + t2) + 2*t1*(6 + t2 - 7*Power(t2,2)) + 
            Power(t1,3)*(-5 + 5*t2 - 4*Power(t2,2)) - 
            Power(t1,2)*(25 + 11*t2 + 6*Power(t2,2)) - 
            Power(s2,3)*(-9 + 6*Power(t1,2) - 28*t2 + Power(t2,2) + 
               7*t1*(-1 + 3*t2)) + 
            Power(s2,2)*(-8 + 14*Power(t1,3) - 35*t2 + Power(t2,2) + 
               2*Power(t1,2)*(6 + 11*t2) - 
               t1*(19 + 55*t2 + 2*Power(t2,2))) + 
            s2*(-11*Power(t1,4) - Power(t1,3)*(17 + 9*t2) + 
               t1*(33 + 46*t2 + 5*Power(t2,2)) + 
               2*(-7 + t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(15 + 22*t2 + 7*Power(t2,2)))) - 
         Power(s,6)*(3 + 9*t1 + 16*Power(t1,2) + 30*Power(t1,3) - 
            25*Power(t1,4) - 7*t2 - 73*t1*t2 - 60*Power(t1,2)*t2 + 
            45*Power(t1,3)*t2 + 29*Power(t2,2) + 42*t1*Power(t2,2) - 
            15*Power(t1,2)*Power(t2,2) - 12*Power(t2,3) - 
            5*t1*Power(t2,3) + 
            Power(s1,2)*(2 + Power(s2,2) - 21*Power(t1,2) + 13*t2 - 
               13*Power(t2,2) + s2*(-7 + 4*t1 + 13*t2) + 
               t1*(-26 + 34*t2)) + 
            Power(s2,2)*(-25*Power(t1,2) + 2*t1*(7 + 36*t2) - 
               t2*(39 + 49*t2)) + 
            s1*(-7 + 48*Power(t1,3) + Power(t1,2)*(15 - 81*t2) + 5*t2 - 
               16*Power(t2,2) + 3*Power(t2,3) + 
               Power(s2,2)*(-3 + 5*t1 + 20*t2) + 
               3*t1*(2 + 9*t2 + 10*Power(t2,2)) - 
               s2*(-18 + 53*Power(t1,2) + t1*(19 - 81*t2) + 25*t2 + 
                  54*Power(t2,2))) + 
            s2*(-7 + 50*Power(t1,3) + 20*t2 - 9*Power(t2,2) + 
               7*Power(t2,3) - Power(t1,2)*(44 + 117*t2) + 
               t1*(7 + 83*t2 + 60*Power(t2,2)))) + 
         Power(s1,4)*(-3*Power(s2,6) + 3*Power(t1,6) + 
            Power(s2,5)*(2 + 12*t1 - 21*t2) - 
            7*Power(-1 + t2,2)*(1 + t2) + Power(t1,5)*(-19 + 9*t2) + 
            Power(t1,4)*(-13 + 61*t2 - 12*Power(t2,2)) - 
            2*Power(t1,3)*(23 + 2*t2 + 5*Power(t2,2)) - 
            Power(t1,2)*(63 + 23*t2 - 2*Power(t2,2) + 8*Power(t2,3)) - 
            t1*(-47 + 13*t2 + 25*Power(t2,2) + 9*Power(t2,3)) - 
            Power(s2,4)*(15*Power(t1,2) + t1*(7 - 89*t2) + 
               2*(-6 + 15*t2 + Power(t2,2))) + 
            Power(s2,3)*(57 + Power(t1,2)*(28 - 150*t2) + 72*t2 + 
               7*Power(t2,2) - 4*Power(t2,3) + 
               2*t1*(-19 + 9*t2 + 10*Power(t2,2))) + 
            s2*(-40 - 12*Power(t1,5) + Power(t1,4)*(58 - 53*t2) - 
               6*t2 + 42*Power(t2,2) + 4*Power(t2,3) + 
               4*Power(t1,3)*(3 - 41*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(154 + 66*t2 + 36*Power(t2,2) - 
                  4*Power(t2,3)) + 
               t1*(125 - 13*t2 + 65*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s2,2)*(-62 + 15*Power(t1,4) + 36*t2 - 67*Power(t2,2) + 
               Power(t2,3) + 2*Power(t1,3)*(-31 + 63*t2) + 
               Power(t1,2)*(27 + 115*t2 - 46*Power(t2,2)) + 
               t1*(-165 - 134*t2 - 33*Power(t2,2) + 8*Power(t2,3)))) + 
         Power(s1,3)*(-3*Power(s2,7) - Power(t1,7) + 
            Power(s2,6)*(-7 + 17*t1 - 27*t2) + 
            Power(t1,6)*(28 - 11*t2) + 
            Power(t1,4)*(-136 + 219*t2 - 36*Power(t2,2)) + 
            4*Power(-1 + t2,2)*(6 + 5*t2 + Power(t2,2)) + 
            Power(t1,5)*(-53 - 56*t2 + 12*Power(t2,2)) + 
            Power(t1,2)*(-128 + 200*t2 - 15*Power(t2,2) + 
               29*Power(t2,3)) + 
            Power(t1,3)*(-59 + 288*t2 - 227*Power(t2,2) + 
               36*Power(t2,3)) + 
            t1*(-143 + 108*t2 + 17*Power(t2,2) + 14*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(s2,5)*(54 - 39*Power(t1,2) - 37*t2 - 3*Power(t2,2) + 
               7*t1*(3 + 20*t2)) + 
            Power(s2,4)*(71 + 45*Power(t1,3) + 144*t2 - 
               37*Power(t2,2) - 9*Power(t2,3) - 
               7*Power(t1,2)*(-2 + 43*t2) + 
               t1*(-267 + 59*t2 + 31*Power(t2,2))) - 
            Power(s2,3)*(128 + 25*Power(t1,4) + 
               Power(t1,3)*(126 - 344*t2) - 63*t2 + 26*Power(t2,2) + 
               19*Power(t2,3) + 
               Power(t1,2)*(-530 - 101*t2 + 87*Power(t2,2)) + 
               t1*(114 + 587*t2 - 145*Power(t2,2) - 26*Power(t2,3))) + 
            Power(s2,2)*(-141 + 3*Power(t1,5) + 
               Power(t1,4)*(189 - 221*t2) + 105*t2 + 66*Power(t2,2) + 
               64*Power(t2,3) - 8*Power(t2,4) + 
               Power(t1,3)*(-528 - 287*t2 + 105*Power(t2,2)) - 
               Power(t1,2)*(121 - 961*t2 + 215*Power(t2,2) + 
                  25*Power(t2,3)) + 
               t1*(211 + 155*t2 - 191*Power(t2,2) + 83*Power(t2,3))) + 
            s2*(151 + 3*Power(t1,6) - 148*t2 + 43*Power(t2,2) - 
               46*Power(t2,3) + Power(t1,5)*(-119 + 76*t2) + 
               Power(t1,4)*(264 + 220*t2 - 58*Power(t2,2)) + 
               Power(t1,3)*(300 - 737*t2 + 143*Power(t2,2) + 
                  8*Power(t2,3)) - 
               2*Power(t1,2)*
                (12 + 253*t2 - 222*Power(t2,2) + 50*Power(t2,3)) + 
               t1*(277 - 325*t2 - 35*Power(t2,2) - 97*Power(t2,3) + 
                  8*Power(t2,4)))) - 
         Power(s1,2)*(Power(s2,8) + Power(t1,7)*(7 - 4*t2) + 
            Power(s2,7)*(4 - 7*t1 + 14*t2) + 
            Power(t1,5)*(-61 + 221*t2 - 48*Power(t2,2)) + 
            4*Power(-1 + t2,2)*(20 + 5*t2 + 3*Power(t2,2)) + 
            Power(t1,6)*(-62 - 8*t2 + 4*Power(t2,2)) + 
            Power(t1,3)*(265 - 995*t2 + 626*Power(t2,2) - 
               35*Power(t2,3)) + 
            Power(t1,4)*(382 - 155*t2 - 209*Power(t2,2) + 
               36*Power(t2,3)) + 
            t1*(105 - 410*t2 + 333*Power(t2,2) - 52*Power(t2,3) + 
               24*Power(t2,4)) + 
            Power(t1,2)*(-84 - 409*t2 + 818*Power(t2,2) - 
               333*Power(t2,3) + 28*Power(t2,4)) + 
            Power(s2,5)*(-105 - 35*Power(t1,3) - 93*t2 + 
               50*Power(t2,2) + 7*Power(t2,3) + 
               Power(t1,2)*(38 + 214*t2) + 
               12*t1*(17 - 7*t2 + Power(t2,2))) + 
            Power(s2,6)*(21*Power(t1,2) - 21*t1*(1 + 4*t2) - 
               5*(6 - 5*t2 + Power(t2,2))) + 
            Power(s2,4)*(165 + 35*Power(t1,4) - 253*t2 + 
               25*Power(t2,2) + 20*Power(t2,3) - 
               15*Power(t1,3)*(1 + 20*t2) + 
               Power(t1,2)*(-578 + 78*t2 + 6*Power(t2,2)) + 
               t1*(394 + 582*t2 - 251*Power(t2,2) - 25*Power(t2,3))) + 
            Power(s2,3)*(375 - 21*Power(t1,5) + 49*t2 - 
               290*Power(t2,2) - 31*Power(t2,3) + 20*Power(t2,4) + 
               10*Power(t1,4)*(-4 + 25*t2) + 
               Power(t1,3)*(872 + 28*t2 - 44*Power(t2,2)) + 
               t1*(-906 + 857*t2 + 238*Power(t2,2) - 106*Power(t2,3)) + 
               Power(t1,2)*(-491 - 1409*t2 + 501*Power(t2,2) + 
                  33*Power(t2,3))) + 
            Power(s2,2)*(-407 + 7*Power(t1,6) + 
               Power(t1,5)*(61 - 124*t2) + 356*t2 + 91*Power(t2,2) - 
               56*Power(t2,3) + 36*Power(t2,4) + 
               Power(t1,4)*(-738 - 87*t2 + 51*Power(t2,2)) + 
               Power(t1,3)*(159 + 1665*t2 - 497*Power(t2,2) - 
                  19*Power(t2,3)) + 
               Power(t1,2)*(1699 - 1110*t2 - 760*Power(t2,2) + 
                  188*Power(t2,3)) + 
               t1*(-477 - 1078*t2 + 1176*Power(t2,2) + 30*Power(t2,3) - 
                  36*Power(t2,4))) + 
            s2*(-83 - Power(t1,7) + 34*Power(t1,6)*(-1 + t2) + 324*t2 - 
               247*Power(t2,2) + 54*Power(t2,3) - 52*Power(t2,4) + 
               4*Power(t2,5) + 
               Power(t1,5)*(332 + 48*t2 - 24*Power(t2,2)) + 
               Power(t1,3)*(-1340 + 661*t2 + 706*Power(t2,2) - 
                  138*Power(t2,3)) + 
               Power(t1,4)*(104 - 966*t2 + 245*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(489 + 75*t2 - 951*Power(t2,2) + 415*Power(t2,3) - 
                  68*Power(t2,4)) + 
               Power(t1,2)*(-163 + 2024*t2 - 1512*Power(t2,2) + 
                  36*Power(t2,3) + 16*Power(t2,4)))) - 
         s1*(Power(t1,8) + 2*Power(s2,8)*t2 + Power(t1,7)*(7 + t2) + 
            Power(t1,6)*(-11 - 29*t2 + 8*Power(t2,2)) + 
            Power(t1,5)*(-153 + 154*t2 + 25*Power(t2,2) - 
               8*Power(t2,3)) - 
            2*Power(-1 + t2,2)*
             (-3 + 71*t2 - 8*Power(t2,2) + 3*Power(t2,3)) + 
            Power(t1,4)*(57 + 326*t2 - 421*Power(t2,2) + 
               63*Power(t2,3)) + 
            Power(t1,3)*(613 - 1015*t2 + 148*Power(t2,2) + 
               268*Power(t2,3) - 36*Power(t2,4)) + 
            Power(t1,2)*(99 - 1159*t2 + 1729*Power(t2,2) - 
               665*Power(t2,3) - 2*Power(t2,4)) + 
            t1*(-315 + 284*t2 + 633*Power(t2,2) - 808*Power(t2,3) + 
               202*Power(t2,4) + 4*Power(t2,5)) - 
            Power(s2,7)*(2 - 3*t2 + 6*Power(t2,2) + t1*(2 + 13*t2)) + 
            Power(s2,6)*(-8 - 34*t2 + 2*Power(t2,2) + 3*Power(t2,3) + 
               Power(t1,2)*(13 + 36*t2) + 
               t1*(11 - 4*t2 + 31*Power(t2,2))) - 
            Power(s2,5)*(-51 + 72*t2 + 9*Power(t2,2) + 11*Power(t2,3) + 
               Power(t1,3)*(36 + 55*t2) + 
               Power(t1,2)*(32 + 26*t2 + 65*Power(t2,2)) + 
               t1*(-48 - 213*t2 + 32*Power(t2,2) + 12*Power(t2,3))) + 
            Power(s2,4)*(189 + 46*t2 - 205*Power(t2,2) - 
               43*Power(t2,3) + 14*Power(t2,4) + 
               Power(t1,4)*(55 + 50*t2) + 
               Power(t1,3)*(65 + 85*t2 + 70*Power(t2,2)) + 
               Power(t1,2)*(-123 - 541*t2 + 116*Power(t2,2) + 
                  18*Power(t2,3)) + 
               t1*(-335 + 374*t2 + 122*Power(t2,2) + 21*Power(t2,3))) + 
            Power(s2,2)*(-529 + 404*t2 + 447*Power(t2,2) - 
               194*Power(t2,3) - 136*Power(t2,4) + 10*Power(t2,5) + 
               Power(t1,6)*(27 + 8*t2) + 
               Power(t1,5)*(77 + 62*t2 + 11*Power(t2,2)) + 
               Power(t1,3)*(-1056 + 1008*t2 + 369*Power(t2,2) - 
                  49*Power(t2,3)) + 
               Power(t1,4)*(-138 - 516*t2 + 146*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(1005 - 1938*t2 + 262*Power(t2,2) + 
                  697*Power(t2,3) - 76*Power(t2,4)) + 
               6*Power(t1,2)*
                (137 + 177*t2 - 331*Power(t2,2) + 25*Power(t2,3) + 
                  5*Power(t2,4))) - 
            Power(s2,3)*(180 - 449*t2 + 88*Power(t2,2) + 
               183*Power(t2,3) - 16*Power(t2,4) + 
               Power(t1,5)*(50 + 27*t2) + 
               5*Power(t1,4)*(18 + 21*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-852 + 844*t2 + 337*Power(t2,2) - 
                  11*Power(t2,3)) + 
               2*Power(t1,3)*
                (-86 - 357*t2 + 92*Power(t2,2) + 6*Power(t2,3)) + 
               t1*(666 + 437*t2 - 1090*Power(t2,2) - 21*Power(t2,3) + 
                  36*Power(t2,4))) - 
            s2*(-473 + 820*t2 - 69*Power(t2,2) - 392*Power(t2,3) + 
               122*Power(t2,4) - 8*Power(t2,5) + Power(t1,7)*(8 + t2) + 
               Power(t1,6)*(36 + 16*t2 + Power(t2,2)) + 
               Power(t1,5)*(-60 - 193*t2 + 56*Power(t2,2)) + 
               Power(t1,4)*(-641 + 620*t2 + 170*Power(t2,2) - 
                  36*Power(t2,3)) + 
               Power(t1,2)*(1438 - 2504*t2 + 322*Power(t2,2) + 
                  782*Power(t2,3) - 96*Power(t2,4)) + 
               Power(t1,3)*(402 + 997*t2 - 1522*Power(t2,2) + 
                  191*Power(t2,3) + 8*Power(t2,4)) + 
               t1*(-430 - 749*t2 + 2172*Power(t2,2) - 871*Power(t2,3) - 
                  126*Power(t2,4) + 8*Power(t2,5)))) + 
         Power(s,5)*(3 + 4*t1 + 12*Power(t1,2) - 41*Power(t1,3) - 
            86*Power(t1,4) + 30*Power(t1,5) - 39*t2 - 42*t1*t2 + 
            151*Power(t1,2)*t2 + 177*Power(t1,3)*t2 - 
            50*Power(t1,4)*t2 + 30*Power(t2,2) + 13*t1*Power(t2,2) - 
            120*Power(t1,2)*Power(t2,2) + 10*Power(t1,3)*Power(t2,2) - 
            39*Power(t2,3) + 23*t1*Power(t2,3) + 
            10*Power(t1,2)*Power(t2,3) + 6*Power(t2,4) + 
            Power(s1,3)*(6 + 3*Power(s2,2) - 24*Power(t1,2) + 23*t2 - 
               11*Power(t2,2) + s2*(-27 + 7*t1 + 22*t2) + 
               t1*(-34 + 35*t2)) + 
            Power(s2,3)*(-30*Power(t1,2) + 2*t1*(13 + 55*t2) - 
               t2*(90 + 91*t2)) + 
            Power(s1,2)*(-31 + 6*Power(s2,3) + 87*Power(t1,3) + 
               Power(t1,2)*(71 - 123*t2) + 18*t2 - 53*Power(t2,2) + 
               7*Power(t2,3) + Power(s2,2)*(-46 + 10*t1 + 79*t2) + 
               t1*(-34 + 60*t2 + 29*Power(t2,2)) - 
               s2*(-97 + 103*Power(t1,2) + t1*(51 - 75*t2) + 28*t2 + 
                  70*Power(t2,2))) + 
            Power(s2,2)*(-8 + 90*Power(t1,3) + 40*t2 - 33*Power(t2,2) + 
               21*Power(t2,3) - 6*Power(t1,2)*(23 + 45*t2) + 
               t1*(20 + 333*t2 + 171*Power(t2,2))) + 
            s2*(-5 - 90*Power(t1,4) + 56*t2 + 76*Power(t2,2) - 
               63*Power(t2,3) + 6*Power(t1,3)*(33 + 35*t2) - 
               3*Power(t1,2)*(-7 + 140*t2 + 30*Power(t2,2)) + 
               t1*(13 - 268*t2 + 189*Power(t2,2) - 30*Power(t2,3))) + 
            s1*(20 - 93*Power(t1,4) - 31*t2 + 123*Power(t2,2) - 
               32*Power(t2,3) + Power(s2,3)*(-14 + 10*t1 + 57*t2) + 
               2*Power(t1,3)*(29 + 72*t2) - 
               3*Power(t1,2)*(-34 + 93*t2 + 13*Power(t2,2)) + 
               t1*(106 - 310*t2 + 181*Power(t2,2) - 12*Power(t2,3)) + 
               Power(s2,2)*(65 - 113*Power(t1,2) - 158*t2 - 
                  156*Power(t2,2) + 2*t1*(8 + 61*t2)) + 
               s2*(-69 + 196*Power(t1,3) + 51*t2 - 77*Power(t2,2) + 
                  18*Power(t2,3) - Power(t1,2)*(60 + 323*t2) + 
                  t1*(-93 + 409*t2 + 181*Power(t2,2))))) - 
         Power(s,4)*(13 - 96*t1 - 168*Power(t1,2) - 140*Power(t1,3) - 
            25*Power(t1,4) + 110*Power(t1,5) - 20*Power(t1,6) + 91*t2 + 
            472*t1*t2 + 400*Power(t1,2)*t2 + 22*Power(t1,3)*t2 - 
            212*Power(t1,4)*t2 + 30*Power(t1,5)*t2 - 150*Power(t2,2) - 
            405*t1*Power(t2,2) - 289*Power(t1,2)*Power(t2,2) + 
            116*Power(t1,3)*Power(t2,2) + 115*Power(t2,3) + 
            162*t1*Power(t2,3) + 4*Power(t1,2)*Power(t2,3) - 
            10*Power(t1,3)*Power(t2,3) - 10*Power(t2,4) - 
            18*t1*Power(t2,4) + 
            Power(s1,4)*(6 + 3*Power(s2,2) - 16*Power(t1,2) + 21*t2 - 
               5*Power(t2,2) + 7*t1*(-2 + 3*t2) + 
               s2*(-41 + 7*t1 + 18*t2)) + 
            Power(s2,4)*(-20*Power(t1,2) - 5*t2*(22 + 21*t2) + 
               4*t1*(6 + 25*t2)) + 
            Power(s2,3)*(20 + 80*Power(t1,3) - 8*t2 - 65*Power(t2,2) + 
               35*Power(t2,3) - 2*Power(t1,2)*(91 + 165*t2) + 
               t1*(46 + 584*t2 + 270*Power(t2,2))) - 
            Power(s2,2)*(83 + 120*Power(t1,4) - 269*t2 - 
               49*Power(t2,2) + 135*Power(t2,3) - 
               6*Power(t1,3)*(67 + 65*t2) + 
               3*Power(t1,2)*(39 + 350*t2 + 75*Power(t2,2)) + 
               3*t1*(19 + 75*t2 - 116*Power(t2,2) + 25*Power(t2,3))) + 
            s2*(27 + 80*Power(t1,5) - 298*t2 + 194*Power(t2,2) - 
               158*Power(t2,3) + 30*Power(t2,4) - 
               2*Power(t1,4)*(177 + 95*t2) + 
               Power(t1,3)*(96 + 788*t2 + 60*Power(t2,2)) + 
               Power(t1,2)*(177 + 211*t2 - 399*Power(t2,2) + 
                  50*Power(t2,3)) + 
               t1*(209 - 631*t2 + 301*Power(t2,2) + 105*Power(t2,3))) + 
            Power(s1,3)*(-35 + 15*Power(s2,3) + 85*Power(t1,3) + 
               Power(t1,2)*(99 - 96*t2) + 13*t2 - 64*Power(t2,2) + 
               9*Power(t2,3) + Power(s2,2)*(-118 + 8*t1 + 115*t2) + 
               t1*(-91 + 37*t2 + 2*Power(t2,2)) + 
               s2*(161 - 108*Power(t1,2) + 22*t2 - 41*Power(t2,2) + 
                  t1*(-17 + 4*t2))) + 
            Power(s1,2)*(24 + 15*Power(s2,4) - 141*Power(t1,4) - 9*t2 + 
               155*Power(t2,2) - 23*Power(t2,3) + 
               Power(t1,3)*(34 + 157*t2) + 
               Power(s2,3)*(-107 - 7*t1 + 200*t2) + 
               Power(t1,2)*(255 - 539*t2 + 9*Power(t2,2)) + 
               t1*(240 - 423*t2 + 333*Power(t2,2) - 25*Power(t2,3)) - 
               Power(s2,2)*(-240 + 172*Power(t1,2) + 214*t2 + 
                  155*Power(t2,2) + t1*(-111 + 124*t2)) + 
               s2*(-145 + 305*Power(t1,3) - 52*t2 - 185*Power(t2,2) + 
                  35*Power(t2,3) - Power(t1,2)*(38 + 233*t2) + 
                  t1*(-395 + 746*t2 + 128*Power(t2,2)))) + 
            s1*(17 + 92*Power(t1,5) - 254*t2 + 249*Power(t2,2) - 
               177*Power(t2,3) + 14*Power(t2,4) + 
               2*Power(s2,4)*(-13 + 5*t1 + 45*t2) - 
               Power(t1,4)*(205 + 126*t2) + 
               Power(t1,3)*(-282 + 665*t2 + 16*Power(t2,2)) + 
               Power(t1,2)*(-20 + 635*t2 - 406*Power(t2,2) + 
                  18*Power(t2,3)) + 
               t1*(126 - 467*t2 + 84*Power(t2,2) + 42*Power(t2,3)) + 
               Power(s2,3)*(60 - 122*Power(t1,2) - 319*t2 - 
                  250*Power(t2,2) + 5*t1*(21 + 5*t2)) + 
               Power(s2,2)*(40 + 306*Power(t1,3) - 48*t2 - 
                  146*Power(t2,2) + 45*Power(t2,3) - 
                  Power(t1,2)*(337 + 446*t2) + 
                  t1*(-233 + 1297*t2 + 455*Power(t2,2))) + 
               s2*(-86 - 286*Power(t1,4) + 473*t2 + 205*Power(t2,2) - 
                  139*Power(t2,3) + Power(t1,3)*(463 + 457*t2) + 
                  Power(t1,2)*(455 - 1643*t2 - 221*Power(t2,2)) + 
                  t1*(24 - 788*t2 + 619*Power(t2,2) - 60*Power(t2,3))))) \
+ Power(s,3)*(-100 - 453*t1 - 173*Power(t1,2) + 213*Power(t1,3) + 
            170*Power(t1,4) + 105*Power(t1,5) - 69*Power(t1,6) + 
            7*Power(t1,7) + 405*t2 + 729*t1*t2 - 379*Power(t1,2)*t2 - 
            511*Power(t1,3)*t2 - 197*Power(t1,4)*t2 + 
            114*Power(t1,5)*t2 - 9*Power(t1,6)*t2 - 566*Power(t2,2) - 
            493*t1*Power(t2,2) + 404*Power(t1,2)*Power(t2,2) + 
            388*Power(t1,3)*Power(t2,2) - 33*Power(t1,4)*Power(t2,2) - 
            3*Power(t1,5)*Power(t2,2) + 333*Power(t2,3) + 
            145*t1*Power(t2,3) - 140*Power(t1,2)*Power(t2,3) - 
            30*Power(t1,3)*Power(t2,3) + 5*Power(t1,4)*Power(t2,3) - 
            128*Power(t2,4) - 26*t1*Power(t2,4) + 
            18*Power(t1,2)*Power(t2,4) + 10*Power(t2,5) + 
            Power(s1,5)*(2 + Power(s2,2) - 6*Power(t1,2) + 10*t2 - 
               Power(t2,2) + 7*t1*(1 + t2) + s2*(-31 + 4*t1 + 7*t2)) + 
            Power(s2,5)*(-7*Power(t1,2) + t1*(11 + 54*t2) - 
               t2*(75 + 77*t2)) + 
            Power(s1,4)*(-3 + 12*Power(s2,3) + 48*Power(t1,3) + 
               Power(t1,2)*(58 - 44*t2) - 15*t2 - 28*Power(t2,2) + 
               4*Power(t2,3) + Power(s2,2)*(-126 + 8*t1 + 75*t2) + 
               s2*(108 - 68*Power(t1,2) - 23*t1*(-2 + t2) + 70*t2 - 
                  13*Power(t2,2)) - t1*(74 + 27*t2 + 8*Power(t2,2))) + 
            Power(s2,4)*(38 + 35*Power(t1,3) - 81*t2 - 75*Power(t2,2) + 
               35*Power(t2,3) - Power(t1,2)*(113 + 225*t2) + 
               t1*(49 + 526*t2 + 255*Power(t2,2))) - 
            Power(s2,3)*(70*Power(t1,4) - 18*Power(t1,3)*(19 + 20*t2) + 
               6*Power(t1,2)*(42 + 207*t2 + 50*Power(t2,2)) + 
               2*(44 - 174*t2 + 8*Power(t2,2) + 75*Power(t2,3)) + 
               t1*(135 - 188*t2 - 342*Power(t2,2) + 100*Power(t2,3))) + 
            Power(s2,2)*(-235 + 70*Power(t1,5) - 170*t2 + 
               238*Power(t2,2) - 254*Power(t2,3) + 60*Power(t2,4) - 
               2*Power(t1,4)*(229 + 135*t2) + 
               2*Power(t1,3)*(231 + 660*t2 + 70*Power(t2,2)) + 
               2*Power(t1,2)*
                (163 - 165*t2 - 246*Power(t2,2) + 50*Power(t2,3)) + 
               t1*(406 - 1401*t2 + 694*Power(t2,2) + 190*Power(t2,3))) - 
            s2*(-364 + 35*Power(t1,6) + 549*t2 - 231*Power(t2,2) - 
               140*Power(t2,3) + 24*Power(t2,4) - 
               Power(t1,5)*(287 + 90*t2) + 
               Power(t1,4)*(364 + 643*t2 + 15*Power(t2,2)) + 
               Power(t1,2)*(531 - 1564*t2 + 1066*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,3)*(399 - 420*t2 - 258*Power(t2,2) + 
                  40*Power(t2,3)) + 
               t1*(-362 - 734*t2 + 815*Power(t2,2) - 404*Power(t2,3) + 
                  72*Power(t2,4))) + 
            Power(s1,3)*(5 + 30*Power(s2,4) - 113*Power(t1,4) + 4*t2 + 
               101*Power(t2,2) - 4*Power(t2,3) + 
               2*Power(t1,3)*(-6 + 35*t2) + 
               Power(s2,3)*(-188 - 35*t1 + 240*t2) + 
               Power(t1,2)*(268 - 451*t2 + 69*Power(t2,2)) + 
               t1*(210 - 199*t2 + 289*Power(t2,2) - 26*Power(t2,3)) - 
               Power(s2,2)*(-298 + 133*Power(t1,2) + 48*t2 + 
                  54*Power(t2,2) + t1*(-222 + 338*t2)) + 
               s2*(-113 + 251*Power(t1,3) - 189*t2 - 180*Power(t2,2) + 
                  36*Power(t2,3) + Power(t1,2)*(-22 + 28*t2) + 
                  t1*(-492 + 501*t2 - 25*Power(t2,2)))) + 
            Power(s1,2)*(-55 + 20*Power(s2,5) + 111*Power(t1,5) - 
               3*Power(s2,4)*(37 + 16*t1 - 90*t2) - 163*t2 + 
               260*Power(t2,2) - 238*Power(t2,3) + 20*Power(t2,4) - 
               Power(t1,4)*(244 + 69*t2) + 
               Power(t1,3)*(-431 + 1022*t2 - 75*Power(t2,2)) + 
               t1*(48 - 815*t2 + 294*Power(t2,2) - 58*Power(t2,3)) + 
               Power(t1,2)*(-53 + 547*t2 - 480*Power(t2,2) + 
                  33*Power(t2,3)) - 
               Power(s2,3)*(-91 + 87*Power(t1,2) + 319*t2 + 
                  180*Power(t2,2) + t1*(-340 + 566*t2)) + 
               Power(s2,2)*(30 + 349*Power(t1,3) - 578*t2 - 
                  187*Power(t2,2) + 70*Power(t2,3) + 
                  Power(t1,2)*(-591 + 253*t2) + 
                  t1*(-407 + 1734*t2 + 222*Power(t2,2))) + 
               s2*(79 - 345*Power(t1,4) + 650*t2 + 131*Power(t2,2) - 
                  49*Power(t2,3) + 2*Power(t1,3)*(303 + 56*t2) + 
                  Power(t1,2)*(747 - 2437*t2 + 33*Power(t2,2)) + 
                  t1*(59 - 163*t2 + 718*Power(t2,2) - 100*Power(t2,3)))) \
+ s1*(191 - 48*Power(t1,6) - 255*t2 + 52*Power(t2,2) + 161*Power(t2,3) - 
               2*Power(t2,4) + Power(t1,5)*(227 + 54*t2) + 
               Power(s2,5)*(-24 + 5*t1 + 85*t2) + 
               Power(t1,4)*(153 - 661*t2 + 6*Power(t2,2)) - 
               2*Power(t1,3)*
                (336 - 7*t2 - 160*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(-668 + 2224*t2 - 1171*Power(t2,2) + 
                  50*Power(t2,3)) + 
               t1*(233 + 1028*t2 - 1371*Power(t2,2) + 596*Power(t2,3) - 
                  36*Power(t2,4)) - 
               Power(s2,4)*(26 + 68*Power(t1,2) + 301*t2 + 
                  240*Power(t2,2) + 2*t1*(-66 + 65*t2)) + 
               Power(s2,3)*(352 + 222*Power(t1,3) - 404*t2 - 
                  134*Power(t2,2) + 60*Power(t2,3) - 
                  Power(t1,2)*(479 + 174*t2) + 
                  t1*(-11 + 1703*t2 + 610*Power(t2,2))) - 
               Power(s2,2)*(63 + 308*Power(t1,4) - 1373*t2 + 
                  88*Power(t2,2) + 236*Power(t2,3) - 
                  Power(t1,3)*(885 + 452*t2) + 
                  Power(t1,2)*(-253 + 3164*t2 + 494*Power(t2,2)) + 
                  t1*(1139 - 274*t2 - 739*Power(t2,2) + 120*Power(t2,3))\
) + s2*(-458 + 197*Power(t1,5) - 390*t2 + 686*Power(t2,2) - 
                  562*Power(t2,3) + 56*Power(t2,4) - 
                  Power(t1,4)*(741 + 287*t2) + 
                  Power(t1,3)*(-369 + 2423*t2 + 118*Power(t2,2)) + 
                  Power(t1,2)*
                   (1459 + 116*t2 - 925*Power(t2,2) + 72*Power(t2,3)) + 
                  t1*(652 - 3536*t2 + 1387*Power(t2,2) + 147*Power(t2,3))\
))) - Power(s,2)*(-425 - 703*t1 + 422*Power(t1,2) + 726*Power(t1,3) + 
            84*Power(t1,4) - 39*Power(t1,5) - 76*Power(t1,6) + 
            20*Power(t1,7) - Power(t1,8) + 855*t2 + 196*t1*t2 - 
            1979*Power(t1,2)*t2 - 720*Power(t1,3)*t2 + 
            164*Power(t1,4)*t2 + 111*Power(t1,5)*t2 - 
            24*Power(t1,6)*t2 + Power(t1,7)*t2 - 576*Power(t2,2) + 
            1131*t1*Power(t2,2) + 1791*Power(t1,2)*Power(t2,2) + 
            58*Power(t1,3)*Power(t2,2) - 167*Power(t1,4)*Power(t2,2) - 
            6*Power(t1,5)*Power(t2,2) + Power(t1,6)*Power(t2,2) + 
            152*Power(t2,3) - 778*t1*Power(t2,3) - 
            441*Power(t1,2)*Power(t2,3) + 6*Power(t1,3)*Power(t2,3) + 
            16*Power(t1,4)*Power(t2,3) - Power(t1,5)*Power(t2,3) - 
            18*Power(t2,4) + 142*t1*Power(t2,4) + 
            56*Power(t1,2)*Power(t2,4) - 6*Power(t1,3)*Power(t2,4) + 
            26*Power(t2,5) - 14*t1*Power(t2,5) + 
            Power(s1,6)*(-Power(t1,2) + 2*t2 + t1*(8 + t2) + 
               s2*(-12 + t1 + t2)) - 
            Power(s2,6)*(Power(t1,2) - 2*t1*(1 + 8*t2) + 
               t2*(27 + 35*t2)) + 
            Power(s1,5)*(3*Power(s2,3) + 15*Power(t1,3) + 
               Power(t1,2)*(8 - 13*t2) + 
               Power(s2,2)*(-69 + 7*t1 + 21*t2) + 
               s2*(31 - 25*Power(t1,2) - 7*t1*(-8 + t2) + 45*t2 - 
                  3*Power(t2,2)) - 2*(-5 + 8*t2 + Power(t2,2)) - 
               t1*(23 + 36*t2 + 2*Power(t2,2))) + 
            Power(s2,5)*(19 + 6*Power(t1,3) - 74*t2 - 51*Power(t2,2) + 
               21*Power(t2,3) - 3*Power(t1,2)*(10 + 27*t2) + 
               t1*(23 + 243*t2 + 144*Power(t2,2))) - 
            Power(s2,4)*(-31 + 15*Power(t1,4) - 161*t2 + 
               17*Power(t2,2) + 90*Power(t2,3) - 
               15*Power(t1,3)*(8 + 11*t2) + 
               3*Power(t1,2)*(56 + 242*t2 + 75*Power(t2,2)) + 
               t1*(74 - 363*t2 - 198*Power(t2,2) + 75*Power(t2,3))) + 
            Power(s2,3)*(-345 + 20*Power(t1,5) + 352*t2 + 
               74*Power(t2,2) - 204*Power(t2,3) + 60*Power(t2,4) - 
               10*Power(t1,4)*(22 + 17*t2) + 
               2*Power(t1,3)*(221 + 507*t2 + 80*Power(t2,2)) + 
               Power(t1,2)*(147 - 756*t2 - 282*Power(t2,2) + 
                  100*Power(t2,3)) + 
               t1*(-65 - 971*t2 + 530*Power(t2,2) + 170*Power(t2,3))) + 
            s2*(889 + 6*Power(t1,7) - 608*t2 - 706*Power(t2,2) + 
               643*Power(t2,3) - 240*Power(t2,4) + 30*Power(t2,5) - 
               3*Power(t1,6)*(34 + 7*t2) + 
               3*Power(t1,5)*(109 + 77*t2) + 
               Power(t1,4)*(134 - 474*t2 - 27*Power(t2,2) + 
                  15*Power(t2,3)) - 
               Power(t1,3)*(171 + 977*t2 - 830*Power(t2,2) + 
                  42*Power(t2,3)) + 
               t1*(-258 + 2902*t2 - 2809*Power(t2,2) + 
                  597*Power(t2,3) - 68*Power(t2,4)) + 
               Power(t1,2)*(-1721 + 1619*t2 - 9*Power(t2,2) - 
                  150*Power(t2,3) + 54*Power(t2,4))) - 
            Power(s2,2)*(169 + 15*Power(t1,6) + 861*t2 - 
               827*Power(t2,2) - 6*Power(t2,3) + 12*Power(t2,4) - 
               30*Power(t1,5)*(7 + 3*t2) + 
               Power(t1,4)*(548 + 711*t2 + 45*Power(t2,2)) + 
               Power(t1,2)*(-121 - 1623*t2 + 1176*Power(t2,2) + 
                  54*Power(t2,3)) + 
               Power(t1,3)*(187 - 830*t2 - 168*Power(t2,2) + 
                  60*Power(t2,3)) + 
               t1*(-1340 + 1251*t2 + 123*Power(t2,2) - 
                  348*Power(t2,3) + 108*Power(t2,4))) + 
            Power(s1,4)*(-7 + 18*Power(s2,4) - 51*Power(t1,4) - 15*t2 + 
               49*Power(t2,2) - Power(t2,3) + 
               4*Power(t1,3)*(-9 + 4*t2) + 
               Power(s2,3)*(-132 - 17*t1 + 117*t2) + 
               Power(s2,2)*(154 - 71*Power(t1,2) + t1*(155 - 198*t2) + 
                  105*t2 - 9*Power(t2,2)) + 
               Power(t1,2)*(122 - 117*t2 + 43*Power(t2,2)) + 
               t1*(73 + 13*t2 + 103*Power(t2,2) - 8*Power(t2,3)) + 
               s2*(-19 + 121*Power(t1,3) - 156*t2 - 64*Power(t2,2) + 
                  12*Power(t2,3) + 13*Power(t1,2)*(1 + 5*t2) + 
                  t1*(-247 + 5*t2 - 36*Power(t2,2)))) + 
            Power(s1,3)*(-44 + 30*Power(s2,5) + 68*Power(t1,5) + 
               41*t2 + 27*Power(t2,2) - 96*Power(t2,3) + 
               8*Power(t2,4) + Power(t1,4)*(-151 + 19*t2) + 
               Power(s2,4)*(-123 - 83*t1 + 250*t2) + 
               Power(t1,3)*(-265 + 705*t2 - 112*Power(t2,2)) + 
               Power(s2,3)*(75 + Power(t1,2) + t1*(339 - 688*t2) - 
                  33*t2 - 26*Power(t2,2)) + 
               Power(t1,2)*(-175 + 143*t2 - 284*Power(t2,2) + 
                  25*Power(t2,3)) - 
               t1*(157 + 330*t2 - 116*Power(t2,2) + 66*Power(t2,3)) + 
               Power(s2,2)*(-121 + 195*Power(t1,3) - 609*t2 - 
                  131*Power(t2,2) + 54*Power(t2,3) + 
                  5*Power(t1,2)*(-92 + 129*t2) + 
                  t1*(-277 + 835*t2 - 87*Power(t2,2))) + 
               s2*(215 - 211*Power(t1,4) + Power(t1,3)*(395 - 226*t2) + 
                  159*t2 + 156*Power(t2,2) + 11*Power(t2,3) + 
                  Power(t1,2)*(467 - 1507*t2 + 225*Power(t2,2)) + 
                  t1*(310 + 382*t2 + 444*Power(t2,2) - 78*Power(t2,3)))) \
+ Power(s1,2)*(-106 + 15*Power(s2,6) - 42*Power(t1,6) + 116*t2 - 
               3*Power(t2,2) + 57*Power(t2,3) + 22*Power(t2,4) - 
               3*Power(t1,5)*(-81 + 5*t2) + 
               Power(s2,5)*(-46 - 62*t1 + 205*t2) + 
               2*Power(t1,4)*(55 - 378*t2 + 38*Power(t2,2)) + 
               Power(t1,3)*(-768 + 483*t2 + 149*Power(t2,2) - 
                  19*Power(t2,3)) + 
               Power(t1,2)*(-298 + 2278*t2 - 1433*Power(t2,2) + 
                  205*Power(t2,3)) + 
               t1*(225 + 624*t2 - 819*Power(t2,2) + 510*Power(t2,3) - 
                  36*Power(t2,4)) + 
               Power(s2,4)*(-196 + 56*Power(t1,2) - 148*t2 - 
                  115*Power(t2,2) - 42*t1*(-6 + 17*t2)) + 
               Power(s2,3)*(237 + 96*Power(t1,3) - 1041*t2 + 
                  19*Power(t2,2) + 70*Power(t2,3) + 
                  Power(t1,2)*(-723 + 927*t2) + 
                  t1*(567 + 1386*t2 + 188*Power(t2,2))) - 
               Power(s2,2)*(-477 + 229*Power(t1,4) - 1025*t2 + 
                  206*Power(t2,2) + 9*Power(t2,3) + 
                  Power(t1,3)*(-1117 + 547*t2) + 
                  Power(t1,2)*(436 + 3084*t2 - 45*Power(t2,2)) + 
                  t1*(1053 - 2125*t2 - 186*Power(t2,2) + 
                     150*Power(t2,3))) + 
               s2*(-327 + 166*Power(t1,5) - 103*t2 + 271*Power(t2,2) - 
                  531*Power(t2,3) + 60*Power(t2,4) + 
                  3*Power(t1,4)*(-281 + 48*t2) + 
                  Power(t1,3)*(-45 + 2602*t2 - 194*Power(t2,2)) + 
                  Power(t1,2)*
                   (1584 - 1567*t2 - 354*Power(t2,2) + 99*Power(t2,3)) \
- t1*(265 + 3222*t2 - 1710*Power(t2,2) + 222*Power(t2,3)))) + 
            s1*(516 + 12*Power(t1,7) - 555*t2 - 359*Power(t2,2) + 
               592*Power(t2,3) - 260*Power(t2,4) + 10*Power(t2,5) - 
               Power(t1,6)*(103 + 9*t2) + 
               Power(s2,6)*(-11 + t1 + 48*t2) + 
               Power(t1,5)*(81 + 291*t2 - 6*Power(t2,2)) + 
               Power(t1,3)*(140 - 1911*t2 + 1415*Power(t2,2) - 
                  106*Power(t2,3)) + 
               Power(t1,4)*(808 - 625*t2 - 58*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(31 + 1432*t2 - 2473*Power(t2,2) + 940*Power(t2,3) - 
                  100*Power(t2,4)) + 
               Power(t1,2)*(-1089 + 1073*t2 + 180*Power(t2,2) - 
                  426*Power(t2,3) + 30*Power(t2,4)) - 
               Power(s2,5)*(64 + 17*Power(t1,2) + 131*t2 + 
                  138*Power(t2,2) + t1*(-64 + 157*t2)) + 
               Power(s2,4)*(291 + 70*Power(t1,3) - 519*t2 - 
                  56*Power(t2,2) + 45*Power(t2,3) + 
                  Power(t1,2)*(-249 + 139*t2) + 
                  2*t1*(131 + 506*t2 + 230*Power(t2,2))) - 
               Power(s2,3)*(-463 + 130*Power(t1,4) - 899*t2 + 
                  308*Power(t2,2) + 194*Power(t2,3) - 
                  18*Power(t1,3)*(32 + 3*t2) + 
                  21*Power(t1,2)*(23 + 121*t2 + 26*Power(t2,2)) + 
                  t1*(1441 - 1765*t2 - 313*Power(t2,2) + 
                     120*Power(t2,3))) + 
               Power(s2,2)*(-769 + 125*Power(t1,5) + 988*t2 - 
                  20*Power(t2,2) - 636*Power(t2,3) + 84*Power(t2,4) - 
                  Power(t1,4)*(709 + 146*t2) + 
                  Power(t1,3)*(517 + 2861*t2 + 258*Power(t2,2)) + 
                  3*Power(t1,2)*
                   (939 - 866*t2 - 172*Power(t2,2) + 36*Power(t2,3)) + 
                  t1*(-836 - 3940*t2 + 2436*Power(t2,2) + 
                     189*Power(t2,3))) - 
               s2*(489 + 61*Power(t1,6) + 494*t2 - 1446*Power(t2,2) + 
                  229*Power(t2,3) - 12*Power(t2,4) - 
                  Power(t1,5)*(432 + 71*t2) + 
                  Power(t1,4)*(313 + 1492*t2 + 28*Power(t2,2)) + 
                  Power(t1,3)*
                   (2475 - 1977*t2 - 317*Power(t2,2) + 36*Power(t2,3)) - 
                  Power(t1,2)*
                   (233 + 4952*t2 - 3543*Power(t2,2) + 111*Power(t2,3)) \
+ t1*(-1845 + 1874*t2 + 367*Power(t2,2) - 1069*Power(t2,3) + 
                     108*Power(t2,4))))) + 
         s*(-495 - 2*Power(s1,7)*(s2 - t1) - 418*t1 + 714*Power(t1,2) + 
            543*Power(t1,3) - 137*Power(t1,4) - 88*Power(t1,5) - 
            24*Power(t1,6) + 19*Power(t1,7) - 2*Power(t1,8) + 1069*t2 - 
            319*t1*t2 - 2255*Power(t1,2)*t2 - 319*Power(t1,3)*t2 + 
            407*Power(t1,4)*t2 + 29*Power(t1,5)*t2 - 13*Power(t1,6)*t2 + 
            Power(t1,7)*t2 + Power(s2,7)*(-4 + 2*t1 - 9*t2)*t2 - 
            411*Power(t2,2) + 2115*t1*Power(t2,2) + 
            1877*Power(t1,2)*Power(t2,2) - 544*Power(t1,3)*Power(t2,2) - 
            166*Power(t1,4)*Power(t2,2) + 27*Power(t1,5)*Power(t2,2) + 
            2*Power(t1,6)*Power(t2,2) - 395*Power(t2,3) - 
            1716*t1*Power(t2,3) - 308*Power(t1,2)*Power(t2,3) + 
            259*Power(t1,3)*Power(t2,3) + 15*Power(t1,4)*Power(t2,3) - 
            Power(t1,5)*Power(t2,3) + 270*Power(t2,4) + 
            372*t1*Power(t2,4) - 18*Power(t1,2)*Power(t2,4) - 
            24*Power(t1,3)*Power(t2,4) - 38*Power(t2,5) - 
            32*t1*Power(t2,5) + 4*Power(t1,2)*Power(t2,5) + 
            Power(s1,6)*(2 + 2*Power(t1,3) - 2*t2 - 
               2*Power(t1,2)*(3 + t2) + 2*Power(s2,2)*(-10 + t1 + t2) - 
               3*t1*(1 + 3*t2) + s2*(5 + 26*t1 - 4*Power(t1,2) + 7*t2)) + 
            Power(s2,6)*(2 - 24*t2 - 19*Power(t2,2) + 7*Power(t2,3) - 
               2*Power(t1,2)*(1 + 6*t2) + t1*(4 + 49*t2 + 45*Power(t2,2))\
) - Power(s2,4)*(34 - 187*t2 + 4*Power(t2,2) + 83*Power(t2,3) - 
               30*Power(t2,4) + 10*Power(t1,4)*(3 + 4*t2) - 
               5*Power(t1,3)*(27 + 67*t2 + 18*Power(t2,2)) + 
               t1*(116 + 165*t2 - 117*Power(t2,2) - 75*Power(t2,3)) + 
               Power(t1,2)*(52 + 429*t2 + 84*Power(t2,2) - 
                  50*Power(t2,3))) + 
            Power(s2,5)*(21 + 16*t2 + 4*Power(t2,2) - 27*Power(t2,3) + 
               6*Power(t1,3)*(2 + 5*t2) - 
               3*Power(t1,2)*(13 + 62*t2 + 30*Power(t2,2)) + 
               t1*(2 + 164*t2 + 69*Power(t2,2) - 30*Power(t2,3))) + 
            Power(s2,3)*(-328 + 112*t2 + 329*Power(t2,2) - 
               44*Power(t2,3) + 8*Power(t2,4) + 
               10*Power(t1,5)*(4 + 3*t2) - 
               5*Power(t1,4)*(46 + 64*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(310 + 370*t2 - 402*Power(t2,2) - 
                  62*Power(t2,3)) + 
               Power(t1,3)*(148 + 556*t2 + 26*Power(t2,2) - 
                  40*Power(t2,3)) + 
               t1*(250 - 1024*t2 + 307*Power(t2,2) + 132*Power(t2,3) - 
                  72*Power(t2,4))) + 
            Power(s2,2)*(57 - 752*t2 + 628*Power(t2,2) + 
               211*Power(t2,3) - 160*Power(t2,4) + 30*Power(t2,5) - 
               6*Power(t1,6)*(5 + 2*t2) + 
               3*Power(t1,5)*(70 + 53*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(-444 - 280*t2 + 464*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,4)*(-182 - 374*t2 + 21*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(1164 - 369*t2 - 1431*Power(t2,2) + 455*Power(t2,3) - 
                  58*Power(t2,4)) + 
               Power(t1,2)*(-535 + 1894*t2 - 768*Power(t2,2) + 
                  54*Power(t2,4))) + 
            s2*(777 - 716*t2 - 918*Power(t2,2) + 951*Power(t2,3) - 
               132*Power(t2,4) + 36*Power(t2,5) + 
               2*Power(t1,7)*(6 + t2) - Power(t1,6)*(99 + 34*t2) + 
               Power(t1,5)*(106 + 120*t2 - 15*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(t1,4)*(317 + 30*t2 - 210*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(t1,3)*(456 - 1464*t2 + 631*Power(t2,2) - 
                  64*Power(t2,3) - 12*Power(t2,4)) + 
               Power(t1,2)*(-1379 + 576*t2 + 1646*Power(t2,2) - 
                  670*Power(t2,3) + 74*Power(t2,4)) - 
               2*t1*(387 - 1491*t2 + 1240*Power(t2,2) - 74*Power(t2,3) - 
                  62*Power(t2,4) + 14*Power(t2,5))) + 
            Power(s1,5)*(-5 + 3*Power(s2,4) - 12*Power(t1,4) + 
               5*Power(t1,3)*(-4 + t2) - 4*t2 + 9*Power(t2,2) + 
               Power(s2,3)*(-45 + 2*t1 + 21*t2) + 
               7*Power(t1,2)*(4 + 3*t2 + Power(t2,2)) + 
               t1*(2 + 30*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*(38 - 25*Power(t1,2) + 63*t2 - 
                  3*Power(t2,2) - 7*t1*(-8 + 5*t2)) + 
               s2*(15 + 32*Power(t1,3) - 54*t2 - Power(t2,2) + 
                  9*Power(t1,2)*(1 + t2) - 
                  2*t1*(31 + 44*t2 + 2*Power(t2,2)))) + 
            Power(s1,4)*(5 + 12*Power(s2,5) + 22*Power(t1,5) + 24*t2 - 
               23*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,4)*(-27 + 16*t2) + 
               Power(s2,4)*(-49 - 30*t1 + 81*t2) + 
               Power(s2,3)*(37 - 4*Power(t1,2) + t1*(103 - 243*t2) + 
                  86*t2 + Power(t2,2)) - 
               2*Power(t1,3)*(34 - 92*t2 + 21*Power(t2,2)) + 
               Power(t1,2)*(-155 + 3*t2 - 85*Power(t2,2) + 
                  4*Power(t2,3)) - 
               t1*(83 - 22*t2 + 54*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(-89 + 72*Power(t1,3) - 221*t2 - 
                  43*Power(t2,2) + 12*Power(t2,3) + 
                  Power(t1,2)*(-86 + 259*t2) - 
                  2*t1*(49 - 8*t2 + 24*Power(t2,2))) + 
               s2*(56 - 72*Power(t1,4) + Power(t1,3)*(59 - 113*t2) - 
                  41*t2 + 109*Power(t2,2) - 2*Power(t2,3) + 
                  Power(t1,2)*(129 - 286*t2 + 89*Power(t2,2)) + 
                  t1*(249 + 204*t2 + 137*Power(t2,2) - 16*Power(t2,3)))) \
+ Power(s1,2)*(118 + 6*Power(s2,7) + 6*Power(t1,7) - 7*t2 - 
               212*Power(t2,2) + 159*Power(t2,3) - 62*Power(t2,4) + 
               4*Power(t2,5) + 5*Power(t1,6)*(-17 + 4*t2) + 
               Power(s2,6)*(1 - 34*t1 + 83*t2) + 
               Power(s2,5)*(-172 + 74*Power(t1,2) + t1*(27 - 397*t2) + 
                  23*t2 - 38*Power(t2,2)) + 
               Power(t1,5)*(154 + 208*t2 - 30*Power(t2,2)) + 
               Power(t1,4)*(614 - 818*t2 + 99*Power(t2,2) + 
                  4*Power(t2,3)) - 
               Power(t1,3)*(188 + 1084*t2 - 1141*Power(t2,2) + 
                  160*Power(t2,3)) + 
               t1*(548 - 1097*Power(t2,2) + 449*Power(t2,3) - 
                  72*Power(t2,4)) + 
               Power(t1,2)*(-259 + 729*t2 - 358*Power(t2,2) - 
                  189*Power(t2,3) + 16*Power(t2,4)) + 
               Power(s2,4)*(-30 - 70*Power(t1,3) - 626*t2 + 
                  124*Power(t2,2) + 35*Power(t2,3) + 
                  Power(t1,2)*(-203 + 778*t2) + 
                  t1*(821 + 254*t2 + 77*Power(t2,2))) + 
               Power(s2,3)*(578 + 10*Power(t1,4) + 
                  Power(t1,3)*(522 - 802*t2) + 49*t2 - 
                  157*Power(t2,2) + 37*Power(t2,3) + 
                  Power(t1,2)*(-1585 - 1108*t2 + 27*Power(t2,2)) - 
                  t1*(362 - 2455*t2 + 450*Power(t2,2) + 100*Power(t2,3))\
) + Power(s2,2)*(208 + 34*Power(t1,5) + 220*t2 - 363*Power(t2,2) - 
                  324*Power(t2,3) + 60*Power(t2,4) + 
                  Power(t1,4)*(-633 + 463*t2) + 
                  Power(t1,3)*(1549 + 1570*t2 - 163*Power(t2,2)) + 
                  Power(t1,2)*
                   (1428 - 3850*t2 + 627*Power(t2,2) + 99*Power(t2,3)) \
- t1*(1417 + 1236*t2 - 1654*Power(t2,2) + 270*Power(t2,3))) + 
               s2*(-772 - 26*Power(t1,6) + Power(t1,5)*(371 - 145*t2) + 
                  608*t2 + 325*Power(t2,2) - 47*Power(t2,3) + 
                  58*Power(t2,4) + 
                  Power(t1,4)*(-767 - 947*t2 + 127*Power(t2,2)) - 
                  Power(t1,3)*
                   (1650 - 2839*t2 + 400*Power(t2,2) + 38*Power(t2,3)) + 
                  Power(t1,2)*
                   (1027 + 2271*t2 - 2638*Power(t2,2) + 393*Power(t2,3)) \
+ t1*(37 - 850*t2 + 613*Power(t2,2) + 532*Power(t2,3) - 72*Power(t2,4)))) \
+ Power(s1,3)*(-94 + 15*Power(s2,6) - 17*Power(t1,6) + 
               Power(t1,5)*(126 - 39*t2) + 121*t2 - 64*Power(t2,2) + 
               37*Power(t2,3) + Power(s2,5)*(-19 - 64*t1 + 130*t2) + 
               Power(s2,4)*(-122 + 89*Power(t1,2) + t1*(113 - 521*t2) + 
                  51*t2 + Power(t2,2)) + 
               Power(t1,4)*(24 - 370*t2 + 64*Power(t2,2)) + 
               Power(t1,3)*(-200 + 281*t2 + 23*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,2)*(51 + 757*t2 - 471*Power(t2,2) + 
                  106*Power(t2,3)) - 
               2*t1*(37 - 147*t2 + 70*Power(t2,2) - 71*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,3)*(-107 - 16*Power(t1,3) - 551*t2 + 
                  22*Power(t2,2) + 36*Power(t2,3) + 
                  Power(t1,2)*(-351 + 822*t2) + 
                  t1*(400 + 312*t2 - 91*Power(t2,2))) + 
               Power(s2,2)*(411 - 71*Power(t1,4) + 
                  Power(t1,3)*(565 - 640*t2) + 89*t2 + 81*Power(t2,2) + 
                  34*Power(t2,3) + 
                  Power(t1,2)*(-410 - 1147*t2 + 243*Power(t2,2)) + 
                  t1*(81 + 1223*t2 + 10*Power(t2,2) - 78*Power(t2,3))) + 
               s2*(64*Power(t1,5) + 62*Power(t1,4)*(-7 + 4*t2) + 
                  Power(t1,3)*(108 + 1154*t2 - 217*Power(t2,2)) + 
                  Power(t1,2)*
                   (226 - 953*t2 - 55*Power(t2,2) + 50*Power(t2,3)) - 
                  t1*(496 + 807*t2 - 394*Power(t2,2) + 
                     149*Power(t2,3)) + 
                  2*(8 - 12*t2 - 31*Power(t2,2) - 80*Power(t2,3) + 
                     8*Power(t2,4)))) + 
            s1*(321 + 14*Power(t1,7) - Power(t1,8) - 739*t2 + 
               229*Power(t2,2) + 329*Power(t2,3) - 148*Power(t2,4) + 
               8*Power(t2,5) + Power(s2,7)*(-2 + 15*t2) + 
               Power(t1,5)*(-228 + 320*t2 - 29*Power(t2,2)) + 
               Power(t1,6)*(-69 - 47*t2 + Power(t2,2)) + 
               Power(t1,4)*(598 + 139*t2 - 496*Power(t2,2) + 
                  54*Power(t2,3)) + 
               Power(t1,3)*(1146 - 2398*t2 + 1171*Power(t2,2) - 
                  36*Power(t2,3) - 8*Power(t2,4)) + 
               Power(t1,2)*(-401 - 1313*t2 + 2576*Power(t2,2) - 
                  1201*Power(t2,3) + 128*Power(t2,4)) - 
               2*t1*(242 - 299*t2 + 66*Power(t2,2) + 58*Power(t2,3) - 
                  95*Power(t2,4) + 4*Power(t2,5)) - 
               Power(s2,6)*(27 + Power(t1,2) + 16*t2 + 44*Power(t2,2) + 
                  t1*(-7 + 74*t2)) + 
               Power(s2,5)*(33 + 6*Power(t1,3) - 243*t2 - 
                  5*Power(t2,2) + 18*Power(t2,3) + 
                  Power(t1,2)*(-19 + 145*t2) + 
                  5*t1*(30 + 44*t2 + 37*Power(t2,2))) - 
               Power(s2,4)*(-485 + 15*Power(t1,4) + 42*t2 + 
                  147*Power(t2,2) + 76*Power(t2,3) + 
                  20*Power(t1,3)*(-3 + 7*t2) + 
                  Power(t1,2)*(399 + 767*t2 + 299*Power(t2,2)) + 
                  2*t1*(152 - 613*t2 + 10*Power(t2,2) + 30*Power(t2,3))) \
+ Power(s2,3)*(-8 + 20*Power(t1,5) + 1168*t2 - 662*Power(t2,2) - 
                  294*Power(t2,3) + 56*Power(t2,4) + 
                  5*Power(t1,4)*(-24 + 13*t2) + 
                  2*Power(t1,3)*(318 + 594*t2 + 113*Power(t2,2)) + 
                  Power(t1,2)*
                   (942 - 2540*t2 + 119*Power(t2,2) + 72*Power(t2,3)) + 
                  t1*(-2014 - 325*t2 + 1255*Power(t2,2) + 
                     105*Power(t2,3))) + 
               s2*(443 + 6*Power(t1,7) - 556*t2 + 143*Power(t2,2) + 
                  298*Power(t2,3) - 396*Power(t2,4) + 20*Power(t2,5) - 
                  Power(t1,6)*(67 + t2) + 
                  Power(t1,5)*(318 + 344*t2 + 5*Power(t2,2)) + 
                  Power(t1,4)*
                   (889 - 1457*t2 + 122*Power(t2,2) + 6*Power(t2,3)) - 
                  Power(t1,3)*
                   (2240 + 687*t2 - 1953*Power(t2,2) + 155*Power(t2,3)) \
+ t1*(1684 + 883*t2 - 3896*Power(t2,2) + 1885*Power(t2,3) - 
                     176*Power(t2,4)) + 
                  Power(t1,2)*
                   (-2241 + 5786*t2 - 2923*Power(t2,2) - 
                     164*Power(t2,3) + 60*Power(t2,4))) - 
               Power(s2,2)*(1245 + 15*Power(t1,6) - 397*t2 - 
                  1222*Power(t2,2) + 573*Power(t2,3) - 30*Power(t2,4) + 
                  Power(t1,5)*(-127 + 10*t2) + 
                  Power(t1,4)*(609 + 922*t2 + 74*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (1057 + 305*t2 - 855*Power(t2,2) + 24*Power(t2,3)) + 
                  Power(t1,3)*
                   (1332 - 2694*t2 + 187*Power(t2,2) + 36*Power(t2,3)) + 
                  t1*(-1103 + 4556*t2 - 2414*Power(t2,2) - 
                     494*Power(t2,3) + 108*Power(t2,4))))))*
       T2(1 - s + s1 - t2,1 - s2 + t1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(s - s1 + t2)*(-1 + s2 - t1 + t2)*
       Power(-1 + s - s*s1 + s1*s2 - s1*t1 + t2,2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) - 
    (8*(32*s2 - 22*Power(s2,2) + 17*Power(s2,3) - 8*Power(s2,4) + 
         Power(s2,5) + Power(s2,6) - 2*Power(s1,7)*Power(s2 - t1,2) - 
         32*t1 + 4*s2*t1 - 47*Power(s2,2)*t1 + 41*Power(s2,3)*t1 + 
         2*Power(s2,4)*t1 - 10*Power(s2,5)*t1 + 2*Power(s2,6)*t1 + 
         18*Power(t1,2) + 51*s2*Power(t1,2) - 
         81*Power(s2,2)*Power(t1,2) - 20*Power(s2,3)*Power(t1,2) + 
         38*Power(s2,4)*Power(t1,2) - 13*Power(s2,5)*Power(t1,2) - 
         Power(s2,6)*Power(t1,2) - 21*Power(t1,3) + 71*s2*Power(t1,3) + 
         38*Power(s2,2)*Power(t1,3) - 72*Power(s2,3)*Power(t1,3) + 
         35*Power(s2,4)*Power(t1,3) + 6*Power(s2,5)*Power(t1,3) - 
         23*Power(t1,4) - 29*s2*Power(t1,4) + 
         73*Power(s2,2)*Power(t1,4) - 50*Power(s2,3)*Power(t1,4) - 
         15*Power(s2,4)*Power(t1,4) + 8*Power(t1,5) - 38*s2*Power(t1,5) + 
         40*Power(s2,2)*Power(t1,5) + 20*Power(s2,3)*Power(t1,5) + 
         8*Power(t1,6) - 17*s2*Power(t1,6) - 15*Power(s2,2)*Power(t1,6) + 
         3*Power(t1,7) + 6*s2*Power(t1,7) - Power(t1,8) + 8*t2 - 
         86*s2*t2 + 161*Power(s2,2)*t2 - 85*Power(s2,3)*t2 - 
         12*Power(s2,4)*t2 + 4*Power(s2,5)*t2 + 3*Power(s2,6)*t2 - 
         2*Power(s2,7)*t2 + 94*t1*t2 - 238*s2*t1*t2 + 
         242*Power(s2,2)*t1*t2 + 64*Power(s2,3)*t1*t2 - 
         57*Power(s2,4)*t1*t2 - 11*Power(s2,5)*t1*t2 + 
         13*Power(s2,6)*t1*t2 + 2*Power(s2,7)*t1*t2 + 61*Power(t1,2)*t2 - 
         251*s2*Power(t1,2)*t2 - 100*Power(s2,2)*Power(t1,2)*t2 + 
         199*Power(s2,3)*Power(t1,2)*t2 + 2*Power(s2,4)*Power(t1,2)*t2 - 
         36*Power(s2,5)*Power(t1,2)*t2 - 12*Power(s2,6)*Power(t1,2)*t2 + 
         94*Power(t1,3)*t2 + 56*s2*Power(t1,3)*t2 - 
         295*Power(s2,2)*Power(t1,3)*t2 + 42*Power(s2,3)*Power(t1,3)*t2 + 
         55*Power(s2,4)*Power(t1,3)*t2 + 30*Power(s2,5)*Power(t1,3)*t2 - 
         8*Power(t1,4)*t2 + 201*s2*Power(t1,4)*t2 - 
         73*Power(s2,2)*Power(t1,4)*t2 - 50*Power(s2,3)*Power(t1,4)*t2 - 
         40*Power(s2,4)*Power(t1,4)*t2 - 52*Power(t1,5)*t2 + 
         49*s2*Power(t1,5)*t2 + 27*Power(s2,2)*Power(t1,5)*t2 + 
         30*Power(s2,3)*Power(t1,5)*t2 - 12*Power(t1,6)*t2 - 
         8*s2*Power(t1,6)*t2 - 12*Power(s2,2)*Power(t1,6)*t2 + 
         Power(t1,7)*t2 + 2*s2*Power(t1,7)*t2 - 40*Power(t2,2) + 
         235*s2*Power(t2,2) - 341*Power(s2,2)*Power(t2,2) + 
         27*Power(s2,3)*Power(t2,2) + 40*Power(s2,4)*Power(t2,2) + 
         20*Power(s2,5)*Power(t2,2) - 6*Power(s2,6)*Power(t2,2) - 
         Power(s2,8)*Power(t2,2) - 251*t1*Power(t2,2) + 
         615*s2*t1*Power(t2,2) - 31*Power(s2,2)*t1*Power(t2,2) - 
         266*Power(s2,3)*t1*Power(t2,2) - 62*Power(s2,4)*t1*Power(t2,2) + 
         35*Power(s2,5)*t1*Power(t2,2) + 3*Power(s2,6)*t1*Power(t2,2) + 
         6*Power(s2,7)*t1*Power(t2,2) - 218*Power(t1,2)*Power(t2,2) + 
         13*s2*Power(t1,2)*Power(t2,2) + 
         539*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         32*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         78*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         15*Power(s2,5)*Power(t1,2)*Power(t2,2) - 
         15*Power(s2,6)*Power(t1,2)*Power(t2,2) - 
         9*Power(t1,3)*Power(t2,2) - 440*s2*Power(t1,3)*Power(t2,2) + 
         76*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         82*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         30*Power(s2,4)*Power(t1,3)*Power(t2,2) + 
         20*Power(s2,5)*Power(t1,3)*Power(t2,2) + 
         127*Power(t1,4)*Power(t2,2) - 100*s2*Power(t1,4)*Power(t2,2) - 
         38*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         30*Power(s2,3)*Power(t1,4)*Power(t2,2) - 
         15*Power(s2,4)*Power(t1,4)*Power(t2,2) + 
         34*Power(t1,5)*Power(t2,2) + 3*s2*Power(t1,5)*Power(t2,2) + 
         15*Power(s2,2)*Power(t1,5)*Power(t2,2) + 
         6*Power(s2,3)*Power(t1,5)*Power(t2,2) + 
         2*Power(t1,6)*Power(t2,2) - 3*s2*Power(t1,6)*Power(t2,2) - 
         Power(s2,2)*Power(t1,6)*Power(t2,2) + 135*Power(t2,3) - 
         409*s2*Power(t2,3) + 199*Power(s2,2)*Power(t2,3) + 
         83*Power(s2,3)*Power(t2,3) + 33*Power(s2,4)*Power(t2,3) - 
         15*Power(s2,5)*Power(t2,3) - 2*Power(s2,7)*Power(t2,3) + 
         372*t1*Power(t2,3) - 342*s2*t1*Power(t2,3) - 
         369*Power(s2,2)*t1*Power(t2,3) - 23*Power(s2,3)*t1*Power(t2,3) + 
         67*Power(s2,4)*t1*Power(t2,3) - 2*Power(s2,5)*t1*Power(t2,3) + 
         10*Power(s2,6)*t1*Power(t2,3) + 77*Power(t1,2)*Power(t2,3) + 
         456*s2*Power(t1,2)*Power(t2,3) - 
         125*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         109*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         8*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         20*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         170*Power(t1,3)*Power(t2,3) + 187*s2*Power(t1,3)*Power(t2,3) + 
         75*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         12*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         20*Power(s2,4)*Power(t1,3)*Power(t2,3) - 
         72*Power(t1,4)*Power(t2,3) - 16*s2*Power(t1,4)*Power(t2,3) + 
         8*Power(s2,2)*Power(t1,4)*Power(t2,3) - 
         10*Power(s2,3)*Power(t1,4)*Power(t2,3) - 
         2*Power(t1,5)*Power(t2,3) - 2*s2*Power(t1,5)*Power(t2,3) + 
         2*Power(s2,2)*Power(t1,5)*Power(t2,3) - 193*Power(t2,4) + 
         274*s2*Power(t2,4) + 34*Power(s2,2)*Power(t2,4) + 
         7*Power(s2,3)*Power(t2,4) - 31*Power(s2,4)*Power(t2,4) - 
         171*t1*Power(t2,4) - 132*s2*t1*Power(t2,4) + 
         94*Power(s2,2)*t1*Power(t2,4) + 94*Power(s2,3)*t1*Power(t2,4) - 
         4*Power(s2,4)*t1*Power(t2,4) + 124*Power(t1,2)*Power(t2,4) - 
         190*s2*Power(t1,2)*Power(t2,4) - 
         96*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         12*Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         89*Power(t1,3)*Power(t2,4) + 34*s2*Power(t1,3)*Power(t2,4) - 
         12*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         Power(t1,4)*Power(t2,4) + 4*s2*Power(t1,4)*Power(t2,4) + 
         99*Power(t2,5) - 43*s2*Power(t2,5) - 
         12*Power(s2,2)*Power(t2,5) - 33*Power(s2,3)*Power(t2,5) + 
         2*Power(s2,5)*Power(t2,5) - 30*t1*Power(t2,5) + 
         72*s2*t1*Power(t2,5) + 62*Power(s2,2)*t1*Power(t2,5) - 
         6*Power(s2,4)*t1*Power(t2,5) - 58*Power(t1,2)*Power(t2,5) - 
         30*s2*Power(t1,2)*Power(t2,5) + 
         6*Power(s2,3)*Power(t1,2)*Power(t2,5) + 
         Power(t1,3)*Power(t2,5) - 
         2*Power(s2,2)*Power(t1,3)*Power(t2,5) - 7*Power(t2,6) - 
         s2*Power(t2,6) - 15*Power(s2,2)*Power(t2,6) + 
         Power(s2,4)*Power(t2,6) + 18*t1*Power(t2,6) + 
         13*s2*t1*Power(t2,6) + Power(s2,2)*t1*Power(t2,6) - 
         2*Power(s2,3)*t1*Power(t2,6) - s2*Power(t1,2)*Power(t2,6) + 
         Power(s2,2)*Power(t1,2)*Power(t2,6) - 2*Power(t2,7) - 
         2*s2*Power(t2,7) - Power(s1,6)*(s2 - t1)*
          (2*Power(s2,3) + Power(t1,3) + s2*(-5 + 10*t1 - 7*t2) + 
            4*(-1 + t2) - Power(t1,2)*(8 + t2) + 
            Power(s2,2)*(-2 - 3*t1 + t2) + t1*(3 + 9*t2)) - 
         Power(s1,5)*(3*Power(s2,5) - 3*Power(t1,5) + 
            Power(s2,4)*(3 - 11*t1 - 5*t2) + 2*Power(-1 + t2,2) + 
            Power(t1,4)*(2 + 6*t2) - 
            Power(t1,3)*(9 + 16*t2 + 3*Power(t2,2)) + 
            2*t1*(-4 - 5*t2 + 9*Power(t2,2)) + 
            Power(t1,2)*(44 - 13*t2 + 11*Power(t2,2)) + 
            Power(s2,3)*(-11 + 18*Power(t1,2) + 11*t2 - 4*Power(t2,2) + 
               6*t1*(-1 + 2*t2)) + 
            s2*(10 + 11*Power(t1,4) + 6*t2 - 16*Power(t2,2) - 
               2*Power(t1,3)*(2 + 5*t2) + 
               t1*(-73 + 6*t2 - 17*Power(t2,2)) + 
               Power(t1,2)*(11 + 39*t2 + 2*Power(t2,2))) + 
            Power(s2,2)*(29 - 18*Power(t1,3) + Power(t1,2)*(5 - 3*t2) + 
               7*t2 + 6*Power(t2,2) + t1*(9 - 34*t2 + 5*Power(t2,2)))) + 
         2*Power(s,5)*(-2 - Power(t1,5) + Power(s2,3)*Power(t1 - t2,2) + 
            5*Power(t1,4)*t2 - 10*Power(t1,3)*Power(t2,2) + 
            10*Power(t1,2)*Power(t2,3) - 5*t1*Power(t2,4) + 
            Power(t2,5) + Power(s1,3)*
             (2 - 4*s2 + 2*Power(s2,2) - Power(t1,2) + 2*t1*t2 - 
               Power(t2,2)) + 
            Power(s2,2)*(-2 - 3*Power(t1,3) + 9*Power(t1,2)*t2 - 
               9*t1*Power(t2,2) + 3*Power(t2,3)) + 
            s2*(4 + 3*Power(t1,4) - 12*Power(t1,3)*t2 + 
               18*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
               3*Power(t2,4)) - 
            3*Power(s1,2)*(2 + 2*Power(s2,2) + Power(t1,3) - 
               3*Power(t1,2)*t2 + 3*t1*Power(t2,2) - Power(t2,3) - 
               s2*(4 + Power(t1,2) - 2*t1*t2 + Power(t2,2))) - 
            3*s1*(-2 + Power(t1,4) - 4*Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
               Power(t2,4) + Power(s2,2)*
                (-2 + Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               s2*(4 - 2*Power(t1,3) + 6*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + 2*Power(t2,3)))) + 
         Power(s1,4)*(3*Power(s2,6) + 3*Power(t1,6) + 
            Power(t1,5)*(17 - 9*t2) + 
            Power(s2,4)*(1 + 45*Power(t1,2) + t1*(73 - 53*t2) - 5*t2) + 
            Power(-1 + t2,2)*(5 + 9*t2) + 
            Power(s2,5)*(-15 - 18*t1 + 14*t2) + 
            Power(t1,4)*(38 - 35*t2 + 9*Power(t2,2)) - 
            Power(t1,2)*(2 - 144*t2 + 49*Power(t2,2) + Power(t2,3)) - 
            Power(t1,3)*(102 + 24*t2 - 17*Power(t2,2) + 3*Power(t2,3)) + 
            t1*(-61 + 57*t2 - 21*Power(t2,2) + 25*Power(t2,3)) + 
            Power(s2,3)*(89 - 60*Power(t1,3) - 73*t2 + 30*Power(t2,2) - 
               6*Power(t2,3) + 2*Power(t1,2)*(-73 + 42*t2) + 
               t1*(-31 + 55*t2)) + 
            Power(s2,2)*(-20 + 45*Power(t1,4) + 
               Power(t1,3)*(150 - 74*t2) + 128*t2 - 15*Power(t2,2) - 
               Power(t2,3) + 
               Power(t1,2)*(97 - 130*t2 + 9*Power(t2,2)) + 
               t1*(-271 + 100*t2 - 30*Power(t2,2) + 9*Power(t2,3))) - 
            s2*(18*Power(t1,5) + Power(t1,4)*(79 - 38*t2) + 
               Power(t1,2)*(-284 + 3*t2 + 17*Power(t2,2)) + 
               Power(t1,3)*(105 - 115*t2 + 18*Power(t2,2)) - 
               2*t1*(11 - 136*t2 + 32*Power(t2,2) + Power(t2,3)) + 
               2*(-26 + 16*t2 + Power(t2,2) + 9*Power(t2,3)))) + 
         Power(s1,3)*(-3*Power(s2,7) + Power(t1,7) + 
            Power(s2,6)*(5 + 19*t1 - 11*t2) + Power(t1,6)*(21 - 4*t2) + 
            Power(t1,5)*(51 - 72*t2 + 6*Power(t2,2)) - 
            Power(-1 + t2,2)*(25 + 10*t2 + 13*Power(t2,2)) - 
            Power(t1,4)*(75 + 142*t2 - 92*Power(t2,2) + 4*Power(t2,3)) + 
            t1*(-18 + 270*t2 - 293*Power(t2,2) + 48*Power(t2,3) - 
               7*Power(t2,4)) + 
            Power(t1,3)*(24 + 137*t2 + 102*Power(t2,2) - 
               50*Power(t2,3) + Power(t2,4)) + 
            Power(t1,2)*(-199 + 225*t2 - 129*Power(t2,2) + 
               8*Power(t2,3) + 9*Power(t2,4)) + 
            Power(s2,5)*(31 - 51*Power(t1,2) + 16*t2 - 25*Power(t2,2) + 
               t1*(-58 + 59*t2)) + 
            Power(s2,4)*(-41 + 75*Power(t1,3) + 
               Power(t1,2)*(203 - 130*t2) + 12*t2 - 11*Power(t2,2) - 
               11*Power(t2,3) + t1*(-72 - 147*t2 + 92*Power(t2,2))) + 
            Power(s2,3)*(-15 - 65*Power(t1,4) - 156*t2 + 
               78*Power(t2,2) - 53*Power(t2,3) + 4*Power(t2,4) + 
               2*Power(t1,3)*(-166 + 75*t2) - 
               3*Power(t1,2)*(7 - 139*t2 + 44*Power(t2,2)) + 
               t1*(228 + 67*t2 - 62*Power(t2,2) + 25*Power(t2,3))) + 
            Power(s2,2)*(-177 + 33*Power(t1,5) + 
               Power(t1,4)*(283 - 95*t2) + 327*t2 - 269*Power(t2,2) + 
               34*Power(t2,3) - Power(t2,4) + 
               Power(t1,3)*(185 - 529*t2 + 94*Power(t2,2)) - 
               3*Power(t1,2)*
                (136 + 104*t2 - 83*Power(t2,2) + 7*Power(t2,3)) + 
               t1*(23 + 479*t2 - 33*Power(t2,2) + 36*Power(t2,3) - 
                  7*Power(t2,4))) - 
            s2*(-15 + 9*Power(t1,6) + Power(t1,5)*(122 - 31*t2) + 
               242*t2 - 239*Power(t2,2) + 12*Power(t2,3) + 
               Power(t1,4)*(174 - 315*t2 + 35*Power(t2,2)) - 
               Power(t1,3)*(296 + 375*t2 - 268*Power(t2,2) + 
                  11*Power(t2,3)) + 
               Power(t1,2)*(32 + 460*t2 + 147*Power(t2,2) - 
                  67*Power(t2,3) - 2*Power(t2,4)) + 
               t1*(-372 + 544*t2 - 394*Power(t2,2) + 42*Power(t2,3) + 
                  8*Power(t2,4)))) + 
         Power(s1,2)*(Power(s2,8) + 9*Power(t1,7) + 
            Power(t1,6)*(20 - 34*t2) + Power(s2,7)*(1 - 7*t1 + t2) + 
            Power(s2,6)*(-2 + 21*Power(t1,2) + t1*(5 - 8*t2) + 8*t2 + 
               8*Power(t2,2)) + 
            Power(t1,5)*(27 - 133*t2 + 58*Power(t2,2)) + 
            Power(t1,4)*(40 - 102*t2 + 252*Power(t2,2) - 
               54*Power(t2,3)) + 
            Power(-1 + t2,2)*
             (-4 + 117*t2 - 6*Power(t2,2) + 5*Power(t2,3)) + 
            Power(t1,3)*(-233 + 161*t2 + 134*Power(t2,2) - 
               194*Power(t2,3) + 25*Power(t2,4)) - 
            Power(t1,2)*(10 - 457*t2 + 434*Power(t2,2) + 
               53*Power(t2,3) - 64*Power(t2,4) + 4*Power(t2,5)) - 
            t1*(107 - 296*t2 + 520*Power(t2,2) - 375*Power(t2,3) + 
               35*Power(t2,4) + 9*Power(t2,5)) + 
            Power(s2,5)*(7 - 35*Power(t1,3) - 13*t2 + 33*Power(t2,2) + 
               22*Power(t2,3) + Power(t1,2)*(-49 + 25*t2) + 
               t1*(-17 + 9*t2 - 40*Power(t2,2))) + 
            Power(s2,4)*(-93 + 35*Power(t1,4) + 71*t2 + 
               42*Power(t2,2) + 58*Power(t2,3) + 13*Power(t2,4) - 
               5*Power(t1,3)*(-27 + 8*t2) + 
               2*Power(t1,2)*(54 - 75*t2 + 40*Power(t2,2)) - 
               t1*(-19 + 111*t2 + 56*Power(t2,2) + 76*Power(t2,3))) - 
            Power(s2,3)*(-159 + 21*Power(t1,5) + 268*t2 - 
               135*Power(t2,2) - 16*Power(t2,3) - 50*Power(t2,4) + 
               Power(t2,5) - 5*Power(t1,4)*(-37 + 7*t2) + 
               Power(t1,3)*(222 - 350*t2 + 80*Power(t2,2)) + 
               Power(t1,2)*(126 - 544*t2 + 88*Power(t2,2) - 
                  96*Power(t2,3)) + 
               t1*(-260 + 151*t2 + 372*Power(t2,2) + 109*Power(t2,3) + 
                  29*Power(t2,4))) + 
            Power(s2,2)*(39 + 7*Power(t1,6) + 
               Power(t1,5)*(139 - 16*t2) + 355*t2 - 630*Power(t2,2) + 
               285*Power(t2,3) - 35*Power(t2,4) + 6*Power(t2,5) + 
               Power(t1,4)*(218 - 360*t2 + 40*Power(t2,2)) + 
               Power(t1,3)*(194 - 836*t2 + 270*Power(t2,2) - 
                  52*Power(t2,3)) + 
               Power(t1,2)*(-201 - 13*t2 + 870*Power(t2,2) - 
                  10*Power(t2,3) + 19*Power(t2,4)) + 
               t1*(-591 + 830*t2 - 244*Power(t2,2) - 229*Power(t2,3) - 
                  57*Power(t2,4) + 2*Power(t2,5))) - 
            s2*(-99 + Power(t1,7) + Power(t1,6)*(55 - 3*t2) + 303*t2 - 
               554*Power(t2,2) + 391*Power(t2,3) - 33*Power(t2,4) - 
               8*Power(t2,5) + 
               Power(t1,5)*(105 - 177*t2 + 8*Power(t2,2)) + 
               Power(t1,4)*(121 - 549*t2 + 217*Power(t2,2) - 
                  10*Power(t2,3)) + 
               Power(t1,3)*(6 - 195*t2 + 792*Power(t2,2) - 
                  115*Power(t2,3) + 3*Power(t2,4)) + 
               Power(t1,2)*(-665 + 723*t2 + 25*Power(t2,2) - 
                  407*Power(t2,3) + 18*Power(t2,4) + Power(t2,5)) + 
               t1*(17 + 834*t2 - 1070*Power(t2,2) + 226*Power(t2,3) + 
                  31*Power(t2,4) + 2*Power(t2,5)))) + 
         s1*(Power(t1,8) + Power(t1,6)*(23 - 43*t2) - 
            2*Power(t1,7)*(-2 + t2) + 2*Power(s2,8)*t2 + 
            Power(t1,5)*(6 - 89*t2 + 117*Power(t2,2) + 2*Power(t2,3)) - 
            Power(t1,4)*(21 + 65*t2 - 214*Power(t2,2) + 
               150*Power(t2,3) + Power(t2,4)) + 
            Power(-1 + t2,2)*
             (-8 + 28*t2 - 171*Power(t2,2) + 22*Power(t2,3) + 
               3*Power(t2,4)) + 
            Power(t1,3)*(-6 + 116*t2 + 61*Power(t2,2) - 
               275*Power(t2,3) + 106*Power(t2,4)) + 
            Power(t1,2)*(-171 + 422*t2 - 465*Power(t2,2) + 
               115*Power(t2,3) + 140*Power(t2,4) - 39*Power(t2,5)) + 
            t1*(14 + 226*t2 - 582*Power(t2,2) + 470*Power(t2,3) - 
               117*Power(t2,4) - 16*Power(t2,5) + 5*Power(t2,6)) - 
            Power(s2,7)*(2 + 3*t2 - 6*Power(t2,2) + t1*(2 + 13*t2)) - 
            Power(s2,5)*(-13 - 4*t2 + 27*Power(t2,2) + 39*Power(t2,3) + 
               10*Power(t2,4) + Power(t1,3)*(36 + 55*t2) + 
               t1*(7 - 61*t2 - 91*Power(t2,2)) + 
               Power(t1,2)*(59 + 8*t2 - 70*Power(t2,2))) + 
            Power(s2,6)*(-2 - 5*t2 - 18*Power(t2,2) + 
               Power(t1,2)*(13 + 36*t2) + 
               t1*(17 + 11*t2 - 32*Power(t2,2))) + 
            Power(s2,4)*(-29 + 109*t2 - 38*Power(t2,2) - 
               44*Power(t2,3) - 39*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,4)*(55 + 50*t2) + 
               Power(t1,2)*(71 - 237*t2 - 184*Power(t2,2)) - 
               10*Power(t1,3)*(-11 + 2*t2 + 8*Power(t2,2)) + 
               t1*(-43 - 115*t2 + 235*Power(t2,2) + 163*Power(t2,3) + 
                  32*Power(t2,4))) + 
            Power(s2,3)*(81 - 248*t2 + 264*Power(t2,2) - 
               90*Power(t2,3) + Power(t2,4) - 18*Power(t2,5) - 
               Power(t1,5)*(50 + 27*t2) + 
               5*Power(t1,4)*(-24 + 9*t2 + 10*Power(t2,2)) + 
               6*Power(t1,3)*(-29 + 73*t2 + 31*Power(t2,2)) + 
               Power(t1,2)*(45 + 410*t2 - 660*Power(t2,2) - 
                  257*Power(t2,3) - 36*Power(t2,4)) + 
               t1*(86 - 234*t2 - 124*Power(t2,2) + 313*Power(t2,3) + 
                  110*Power(t2,4) + 13*Power(t2,5))) + 
            Power(s2,2)*(-135 + 328*t2 - 419*Power(t2,2) + 
               301*Power(t2,3) - 103*Power(t2,4) + 33*Power(t2,5) - 
               3*Power(t2,6) + Power(t1,6)*(27 + 8*t2) + 
               Power(t1,4)*(196 - 427*t2 - 94*Power(t2,2)) + 
               Power(t1,5)*(77 - 37*t2 - 16*Power(t2,2)) + 
               Power(t1,3)*(-7 - 580*t2 + 840*Power(t2,2) + 
                  183*Power(t2,3) + 16*Power(t2,4)) - 
               2*Power(t1,2)*
                (53 - 38*t2 - 288*Power(t2,2) + 322*Power(t2,3) + 
                  52*Power(t2,4) + 4*Power(t2,5)) + 
               t1*(-174 + 656*t2 - 556*Power(t2,2) - 41*Power(t2,3) + 
                  109*Power(t2,4) + 28*Power(t2,5))) + 
            s2*(-22 - 202*t2 + 629*Power(t2,2) - 626*Power(t2,3) + 
               237*Power(t2,4) - 16*Power(t2,5) - Power(t1,7)*(8 + t2) + 
               Power(t1,6)*(-27 + 14*t2 + 2*Power(t2,2)) + 
               Power(t1,5)*(-107 + 213*t2 + 19*Power(t2,2)) - 
               Power(t1,4)*(14 - 370*t2 + 505*Power(t2,2) + 
                  52*Power(t2,3) + 2*Power(t2,4)) + 
               Power(t1,2)*(99 - 524*t2 + 231*Power(t2,2) + 
                  406*Power(t2,3) - 216*Power(t2,4) - 10*Power(t2,5)) + 
               Power(t1,3)*(70 + 114*t2 - 628*Power(t2,2) + 
                  525*Power(t2,3) + 34*Power(t2,4) + Power(t2,5)) + 
               t1*(322 - 818*t2 + 976*Power(t2,2) - 456*Power(t2,3) - 
                  41*Power(t2,4) + 10*Power(t2,5) + 3*Power(t2,6)))) + 
         Power(s,4)*(32 - 20*t1 - 18*Power(t1,2) - 3*Power(t1,4) + 
            9*Power(t1,5) - 5*Power(t1,6) + 40*t1*t2 + 
            8*Power(t1,3)*t2 - 43*Power(t1,4)*t2 + 24*Power(t1,5)*t2 - 
            22*Power(t2,2) - 6*Power(t1,2)*Power(t2,2) + 
            82*Power(t1,3)*Power(t2,2) - 45*Power(t1,4)*Power(t2,2) - 
            78*Power(t1,2)*Power(t2,3) + 40*Power(t1,3)*Power(t2,3) + 
            Power(t2,4) + 37*t1*Power(t2,4) - 
            15*Power(t1,2)*Power(t2,4) - 7*Power(t2,5) + Power(t2,6) + 
            Power(s1,4)*(4 - 8*Power(s2,2) + 4*Power(t1,2) + 
               t1*(5 - 6*t2) + s2*(4 + t1 - t2) - 5*t2 + 2*Power(t2,2)) \
+ Power(s2,4)*(-5*Power(t1,2) - t2*(7 + 9*t2) + t1*(3 + 14*t2)) + 
            Power(s2,3)*(8 + 20*Power(t1,3) + t2 - 28*Power(t2,2) - 
               26*Power(t2,3) - 6*Power(t1,2)*(3 + 11*t2) + 
               t1*(3 + 46*t2 + 72*Power(t2,2))) + 
            Power(s2,2)*(4 - 30*Power(t1,4) + 10*t2 + 3*Power(t2,2) - 
               42*Power(t2,3) - 24*Power(t2,4) + 
               6*Power(t1,3)*(6 + 19*t2) - 
               3*Power(t1,2)*(3 + 38*t2 + 54*Power(t2,2)) + 
               2*t1*(-13 + 3*t2 + 60*Power(t2,2) + 51*Power(t2,3))) + 
            s2*(-44 + 20*Power(t1,5) - 10*t2 + 22*Power(t2,2) + 
               3*Power(t2,3) - 28*Power(t2,4) - 6*Power(t2,5) - 
               2*Power(t1,4)*(15 + 43*t2) + 
               Power(t1,3)*(9 + 118*t2 + 144*Power(t2,2)) - 
               Power(t1,2)*(-18 + 15*t2 + 174*Power(t2,2) + 
                  116*Power(t2,3)) + 
               t1*(46 - 40*t2 + 3*Power(t2,2) + 114*Power(t2,3) + 
                  44*Power(t2,4))) + 
            Power(s1,3)*(-40 - 12*Power(s2,3) + 7*Power(t1,3) + 21*t2 + 
               18*Power(t2,2) - 7*Power(t2,3) - 
               7*Power(t1,2)*(-4 + 3*t2) + 
               Power(s2,2)*(32 + 13*t1 + 15*t2) + 
               t1*(7 - 46*t2 + 21*Power(t2,2)) - 
               2*s2*(-10 + 4*Power(t1,2) + 6*t2 - 5*Power(t2,2) + 
                  t1*(22 + t2))) - 
            Power(s1,2)*(-100 - 48*Power(t1,3) + 3*Power(t1,4) + 
               38*t2 + 23*Power(t2,2) + 30*Power(t2,3) - 
               9*Power(t2,4) + Power(s2,3)*(-32 + 3*t1 + 9*t2) + 
               Power(t1,2)*(23 + 126*t2 - 18*Power(t2,2)) + 
               2*t1*(17 - 23*t2 - 54*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s2,2)*(36 - 3*Power(t1,2) + 32*t2 + 
                  39*Power(t2,2) - 4*t1*(-4 + 9*t2)) + 
               s2*(96 - 3*Power(t1,3) - 43*t2 + 34*Power(t2,2) + 
                  21*Power(t2,3) + Power(t1,2)*(64 + 27*t2) - 
                  t1*(89 + 98*t2 + 45*Power(t2,2)))) + 
            s1*(-96 - 11*Power(t1,5) + 18*t2 + 46*Power(t2,2) - 
               3*Power(t2,3) + 24*Power(t2,4) - 5*Power(t2,5) + 
               Power(s2,4)*(t1 + 3*t2) + Power(t1,4)*(34 + 39*t2) - 
               Power(t1,3)*(9 + 126*t2 + 46*Power(t2,2)) + 
               Power(t1,2)*(34 + 15*t2 + 174*Power(t2,2) + 
                  14*Power(t2,3)) + 
               t1*(46 - 80*t2 - 3*Power(t2,2) - 106*Power(t2,3) + 
                  9*Power(t2,4)) + 
               2*Power(s2,3)*
                (-14 + 4*Power(t1,2) + 12*t2 + 17*Power(t2,2) - 
                  t1*(8 + 21*t2)) + 
               Power(s2,2)*(8 - 30*Power(t1,3) - 17*t2 + 
                  72*Power(t2,2) + 54*Power(t2,3) + 
                  6*Power(t1,2)*(11 + 19*t2) + 
                  t1*(53 - 138*t2 - 138*Power(t2,2))) + 
               2*s2*(58 + 16*Power(t1,4) - 2*t2 - 26*Power(t2,2) + 
                  36*Power(t2,3) + 9*Power(t2,4) - 
                  3*Power(t1,3)*(14 + 19*t2) + 
                  Power(t1,2)*(-8 + 120*t2 + 75*Power(t2,2)) - 
                  t1*(54 - 34*t2 + 114*Power(t2,2) + 43*Power(t2,3))))) + 
         Power(s,3)*(-76 + 106*t1 + 74*Power(t1,2) - 23*Power(t1,3) + 
            10*Power(t1,4) - 8*Power(t1,5) + 18*Power(t1,6) - 
            4*Power(t1,7) + 22*t2 - 244*t1*t2 - 9*Power(t1,2)*t2 - 
            34*Power(t1,3)*t2 + 14*Power(t1,4)*t2 - 81*Power(t1,5)*t2 + 
            18*Power(t1,6)*t2 + 130*Power(t2,2) + 103*t1*Power(t2,2) + 
            42*Power(t1,2)*Power(t2,2) + 8*Power(t1,3)*Power(t2,2) + 
            141*Power(t1,4)*Power(t2,2) - 30*Power(t1,5)*Power(t2,2) - 
            71*Power(t2,3) - 22*t1*Power(t2,3) - 
            28*Power(t1,2)*Power(t2,3) - 114*Power(t1,3)*Power(t2,3) + 
            20*Power(t1,4)*Power(t2,3) + 4*Power(t2,4) + 
            16*t1*Power(t2,4) + 36*Power(t1,2)*Power(t2,4) - 
            2*Power(t2,5) + 3*t1*Power(t2,5) - 
            6*Power(t1,2)*Power(t2,5) - 3*Power(t2,6) + 
            2*t1*Power(t2,6) + 
            Power(s1,5)*(-10 + 7*Power(s2,2) - 3*Power(t1,2) + 6*t2 - 
               Power(t2,2) + s2*(7 - 3*t1 + 2*t2) + t1*(-9 + 4*t2)) + 
            Power(s2,5)*(4*Power(t1,2) + 2*t2*(9 + 8*t2) - 
               t1*(5 + 18*t2)) + 
            Power(s2,4)*(-5 - 20*Power(t1,3) + 4*t2 + 69*Power(t2,2) + 
               44*Power(t2,3) + Power(t1,2)*(38 + 90*t2) - 
               t1*(7 + 125*t2 + 114*Power(t2,2))) + 
            Power(s2,3)*(-47 + 40*Power(t1,4) + 13*t2 + 
               10*Power(t2,2) + 96*Power(t2,3) + 36*Power(t2,4) - 
               6*Power(t1,3)*(17 + 30*t2) + 
               Power(t1,2)*(29 + 348*t2 + 276*Power(t2,2)) - 
               t1*(-19 + 23*t2 + 342*Power(t2,2) + 172*Power(t2,3))) + 
            Power(s2,2)*(78 - 40*Power(t1,5) - 103*t2 + 
               61*Power(t2,2) + 6*Power(t2,3) + 54*Power(t2,4) + 
               4*Power(t2,5) + 4*Power(t1,4)*(32 + 45*t2) - 
               Power(t1,3)*(45 + 474*t2 + 304*Power(t2,2)) + 
               Power(t1,2)*(-13 + 48*t2 + 618*Power(t2,2) + 
                  232*Power(t2,3)) - 
               t1*(-97 + 72*t2 + 9*Power(t2,2) + 326*Power(t2,3) + 
                  72*Power(t2,4))) + 
            s2*(46 + 20*Power(t1,6) + 68*t2 - 175*Power(t2,2) + 
               87*Power(t2,3) - 2*Power(t2,4) + 6*Power(t2,5) - 
               4*Power(t2,6) - Power(t1,5)*(77 + 90*t2) + 
               Power(t1,4)*(31 + 314*t2 + 156*Power(t2,2)) - 
               Power(t1,3)*(11 + 43*t2 + 486*Power(t2,2) + 
                  124*Power(t2,3)) + 
               Power(t1,2)*(-27 + 93*t2 - 9*Power(t2,2) + 
                  344*Power(t2,3) + 36*Power(t2,4)) + 
               t1*(-228 + 266*t2 - 169*Power(t2,2) + 23*Power(t2,3) - 
                  101*Power(t2,4) + 6*Power(t2,5))) + 
            Power(s1,4)*(39 + 19*Power(s2,3) + 11*t2 - 29*Power(t2,2) + 
               3*Power(t2,3) + Power(t1,2)*(-42 + 9*t2) - 
               2*Power(s2,2)*(11 + 10*t1 + 9*t2) + 
               t1*(-10 + 69*t2 - 12*Power(t2,2)) + 
               s2*(-56 + Power(t1,2) - 3*t2 - 14*Power(t2,2) + 
                  15*t1*(4 + t2))) + 
            Power(s1,3)*(-3 + 15*Power(s2,4) + 14*Power(t1,4) - 
               109*t2 + 13*Power(t2,2) + 56*Power(t2,3) - 
               3*Power(t2,4) - Power(t1,3)*(34 + 27*t2) - 
               Power(s2,3)*(18 + 29*t1 + 33*t2) + 
               Power(t1,2)*(-40 + 156*t2 + 9*Power(t2,2)) + 
               t1*(-43 + 99*t2 - 178*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(-90 + 27*Power(t1,2) + 95*t2 + 
                  10*Power(t2,2) + 3*t1*(1 + 9*t2)) + 
               s2*(136 - 27*Power(t1,3) + 7*t2 + Power(t2,2) + 
                  35*Power(t2,3) + Power(t1,2)*(49 + 33*t2) + 
                  t1*(69 - 186*t2 - 41*Power(t2,2)))) - 
            Power(s1,2)*(158 + Power(s2,5) - 12*Power(t1,5) + 
               Power(s2,4)*(42 - 8*t1 - 26*t2) - 287*t2 + 
               27*Power(t2,2) + 7*Power(t2,3) + 52*Power(t2,4) - 
               Power(t2,5) + Power(t1,4)*(-32 + 47*t2) - 
               4*Power(t1,3)*(-26 + 4*t2 + 17*Power(t2,2)) + 
               3*Power(t1,2)*
                (-9 - 67*t2 + 60*Power(t2,2) + 14*Power(t2,3)) + 
               t1*(-139 + 168*t2 + 90*Power(t2,2) - 184*Power(t2,3) - 
                  8*Power(t2,4)) + 
               Power(s2,3)*(30 + 30*Power(t1,2) - 17*t2 - 
                  73*Power(t2,2) + t1*(-121 + 79*t2)) + 
               Power(s2,2)*(-234 - 52*Power(t1,3) + 
                  Power(t1,2)*(84 - 33*t2) + 5*t2 + 36*Power(t2,2) - 
                  5*Power(t2,3) + t1*(121 - 36*t2 + 90*Power(t2,2))) + 
               s2*(43 + 41*Power(t1,4) + Power(t1,3)*(27 - 67*t2) + 
                  225*t2 - 186*Power(t2,2) + 27*Power(t2,3) + 
                  40*Power(t2,4) + 
                  3*Power(t1,2)*(-85 + 23*t2 + 17*Power(t2,2)) + 
                  t1*(207 + 153*t2 - 123*Power(t2,2) - 65*Power(t2,3)))) \
- s1*(-206 + 3*Power(t1,6) + 200*t2 + 137*Power(t2,2) - 93*Power(t2,3) + 
               5*Power(t2,4) - 22*Power(t2,5) - 
               3*Power(t1,5)*(17 + t2) + 
               Power(s2,5)*(-3 + 2*t1 + 11*t2) + 
               Power(t1,4)*(32 + 150*t2 - 18*Power(t2,2)) + 
               Power(t1,3)*(-111 - 53*t2 - 122*Power(t2,2) + 
                  42*Power(t2,3)) - 
               3*Power(t1,2)*
                (-23 - 59*t2 - 5*Power(t2,2) + 8*Power(t2,3) + 
                  11*Power(t2,4)) + 
               t1*(200 - 342*t2 + 27*Power(t2,2) + Power(t2,3) + 
                  69*Power(t2,4) + 9*Power(t2,5)) - 
               Power(s2,4)*(22 + 5*Power(t1,2) - 41*t2 - 
                  72*Power(t2,2) + 5*t1*(2 + 15*t2)) + 
               Power(s2,3)*(-96 + 23*t2 + 119*Power(t2,2) + 
                  90*Power(t2,3) + 9*Power(t1,2)*(11 + 18*t2) + 
                  t1*(85 - 258*t2 - 252*Power(t2,2))) + 
               Power(s2,2)*(227 + 10*Power(t1,4) - 105*t2 + 
                  69*Power(t2,2) + 81*Power(t2,3) + 8*Power(t2,4) - 
                  Power(t1,3)*(207 + 146*t2) + 
                  3*Power(t1,2)*(-24 + 181*t2 + 90*Power(t2,2)) - 
                  t1*(-33 + 21*t2 + 417*Power(t2,2) + 142*Power(t2,3))) \
+ s2*(80 - 10*Power(t1,5) - 90*t2 - 110*Power(t2,2) + 149*Power(t2,3) - 
                  16*Power(t2,4) - 21*Power(t2,5) + 
                  Power(t1,4)*(172 + 51*t2) - 
                  Power(t1,3)*(23 + 476*t2 + 72*Power(t2,2)) + 
                  Power(t1,2)*
                   (174 + 51*t2 + 420*Power(t2,2) + 10*Power(t2,3)) + 
                  t1*(-370 + 144*t2 - 177*Power(t2,2) - 
                     100*Power(t2,3) + 42*Power(t2,4))))) - 
         Power(s,2)*(-72 + 184*t1 + 76*Power(t1,2) - 46*Power(t1,3) + 
            50*Power(t1,4) + 5*Power(t1,5) + 13*Power(t1,6) - 
            11*Power(t1,7) + Power(t1,8) + 44*t2 - 490*t1*t2 - 
            92*Power(t1,2)*t2 - 132*Power(t1,3)*t2 - 67*Power(t1,4)*t2 - 
            31*Power(t1,5)*t2 + 45*Power(t1,6)*t2 - 4*Power(t1,7)*t2 + 
            222*Power(t2,2) + 490*t1*Power(t2,2) + 
            222*Power(t1,2)*Power(t2,2) + 200*Power(t1,3)*Power(t2,2) + 
            19*Power(t1,4)*Power(t2,2) - 66*Power(t1,5)*Power(t2,2) + 
            5*Power(t1,6)*Power(t2,2) - 312*Power(t2,3) - 
            272*t1*Power(t2,3) - 248*Power(t1,2)*Power(t2,3) - 
            2*Power(t1,3)*Power(t2,3) + 34*Power(t1,4)*Power(t2,3) + 
            132*Power(t2,4) + 139*t1*Power(t2,4) + 
            7*Power(t1,2)*Power(t2,4) + 9*Power(t1,3)*Power(t2,4) - 
            5*Power(t1,4)*Power(t2,4) - 29*Power(t2,5) - 
            7*t1*Power(t2,5) - 15*Power(t1,2)*Power(t2,5) + 
            4*Power(t1,3)*Power(t2,5) + Power(t2,6) + 4*t1*Power(t2,6) - 
            Power(t1,2)*Power(t2,6) + 
            Power(s1,6)*(2*Power(s2,2) - Power(t1,2) + t1*(-8 + t2) + 
               2*(-2 + t2) + s2*(8 - t1 + t2)) + 
            Power(s2,6)*(Power(t1,2) - 2*t1*(1 + 5*t2) + 
               t2*(15 + 14*t2)) + 
            Power(s2,5)*(-13 - 6*Power(t1,3) + 9*t2 + 54*Power(t2,2) + 
               36*Power(t2,3) + 3*Power(t1,2)*(7 + 18*t2) - 
               t1*(5 + 114*t2 + 90*Power(t2,2))) + 
            Power(s2,4)*(7 + 15*Power(t1,4) - 48*t2 + 13*Power(t2,2) + 
               66*Power(t2,3) + 24*Power(t2,4) - 
               15*Power(t1,3)*(5 + 8*t2) + 
               3*Power(t1,2)*(11 + 117*t2 + 75*Power(t2,2)) - 
               t1*(-51 + 38*t2 + 312*Power(t2,2) + 144*Power(t2,3))) - 
            Power(s2,3)*(66 + 20*Power(t1,5) - 187*t2 + 
               119*Power(t2,2) + 14*Power(t2,3) - 24*Power(t2,4) + 
               4*Power(t2,5) - 10*Power(t1,4)*(13 + 14*t2) + 
               Power(t1,3)*(82 + 564*t2 + 280*Power(t2,2)) - 
               Power(t1,2)*(-80 + 91*t2 + 678*Power(t2,2) + 
                  216*Power(t2,3)) + 
               t1*(37 - 151*t2 + 19*Power(t2,2) + 268*Power(t2,3) + 
                  52*Power(t2,4))) + 
            Power(s2,2)*(174 + 15*Power(t1,6) - 362*t2 + 
               455*Power(t2,2) - 219*Power(t2,3) - 30*Power(t2,4) - 
               9*Power(t2,5) - 6*Power(t2,6) - 
               30*Power(t1,5)*(4 + 3*t2) + 
               Power(t1,4)*(98 + 501*t2 + 180*Power(t2,2)) - 
               Power(t1,3)*(-64 + 135*t2 + 702*Power(t2,2) + 
                  144*Power(t2,3)) + 
               Power(t1,2)*(103 - 225*t2 + 18*Power(t2,2) + 
                  372*Power(t2,3) + 27*Power(t2,4)) + 
               t1*(100 - 516*t2 + 396*Power(t2,2) + 49*Power(t2,3) - 
                  42*Power(t2,4) + 18*Power(t2,5))) + 
            s2*(-40 - 6*Power(t1,7) + 204*t2 - 492*Power(t2,2) + 
               519*Power(t2,3) - 204*Power(t2,4) - 11*Power(t2,5) - 
               6*Power(t2,6) + Power(t1,6)*(57 + 30*t2) - 
               3*Power(t1,5)*(19 + 78*t2 + 18*Power(t2,2)) + 
               Power(t1,4)*(-27 + 104*t2 + 348*Power(t2,2) + 
                  36*Power(t2,3)) + 
               Power(t1,3)*(-123 + 189*t2 - 31*Power(t2,2) - 
                  204*Power(t2,3) + 6*Power(t2,4)) + 
               Power(t1,2)*(12 + 461*t2 - 477*Power(t2,2) - 
                  33*Power(t2,3) + 9*Power(t2,4) - 18*Power(t2,5)) + 
               t1*(-322 + 696*t2 - 913*Power(t2,2) + 519*Power(t2,3) + 
                  28*Power(t2,4) + 30*Power(t2,5) + 6*Power(t2,6))) + 
            Power(s1,5)*(2 + 17*Power(s2,3) + 3*Power(t1,3) + 16*t2 - 
               10*Power(t2,2) - Power(s2,2)*(5 + 25*t1 + t2) - 
               Power(t1,2)*(29 + 2*t2) + t1*(2 + 42*t2 - Power(t2,2)) + 
               s2*(-34 + 5*Power(t1,2) - 15*t2 - 6*Power(t2,2) + 
                  t1*(35 + 6*t2))) + 
            Power(s1,4)*(18 + 11*Power(s2,4) + 9*Power(t1,4) + 
               Power(s2,3)*(6 - 15*t1 - 53*t2) + 
               Power(t1,3)*(4 - 27*t2) - 31*t2 - 28*Power(t2,2) + 
               15*Power(t2,3) + 
               Power(s2,2)*(-112 + t1 + 6*Power(t1,2) + 44*t2 + 
                  79*t1*t2 - 22*Power(t2,2)) + 
               Power(t1,2)*(-21 + 52*t2 + 21*Power(t2,2)) - 
               t1*(76 - 69*t2 + 65*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(87 - 11*Power(t1,3) + Power(t1,2)*(-11 + t2) + 
                  59*t2 - 37*Power(t2,2) + 12*Power(t2,3) + 
                  t1*(101 - 75*t2 - 8*Power(t2,2)))) + 
            Power(s1,3)*(-58 + 13*Power(s2,5) + 
               Power(t1,4)*(59 - 21*t2) + 82*t2 + 8*Power(t2,2) + 
               37*Power(t2,3) - 5*Power(t2,4) - 
               2*Power(s2,4)*(-6 + 19*t1 + 5*t2) + 
               Power(t1,3)*(38 - 166*t2 + 47*Power(t2,2)) + 
               Power(t1,2)*(-19 + 30*t2 + 114*Power(t2,2) - 
                  31*Power(t2,3)) + 
               t1*(-48 + 227*t2 - 193*Power(t2,2) - 2*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,3)*(-106 + 36*Power(t1,2) + 99*t2 + 
                  67*Power(t2,2) + t1*(-53 + 3*t2)) - 
               s2*(-163 + Power(t1,4) + Power(t1,3)*(147 - 25*t2) + 
                  501*t2 - 119*Power(t2,2) - 136*Power(t2,3) + 
                  10*Power(t2,4) + 
                  Power(t1,2)*(80 - 350*t2 - 3*Power(t2,2)) + 
                  t1*(-7 + 143*t2 + 187*Power(t2,2) + 17*Power(t2,3))) \
+ Power(s2,2)*(-10*Power(t1,3) + 3*Power(t1,2)*(43 + t2) + 
                  t1*(148 - 283*t2 - 117*Power(t2,2)) + 
                  2*(8 + 97*t2 + 8*Power(t2,2) + 30*Power(t2,3)))) - 
            Power(s1,2)*(56 + 3*Power(s2,6) + 9*Power(t1,6) - 238*t2 + 
               304*Power(t2,2) - 74*Power(t2,3) + 33*Power(t2,4) + 
               5*Power(t2,5) - 2*Power(s2,5)*(-17 + 7*t1 + 12*t2) - 
               Power(t1,5)*(31 + 22*t2) + 
               Power(t1,4)*(-146 + 225*t2 + 10*Power(t2,2)) + 
               Power(t1,3)*(75 + 216*t2 - 422*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(54 - 170*t2 + 39*Power(t2,2) + 
                  298*Power(t2,3) - 11*Power(t2,4)) + 
               t1*(-240 + 320*t2 - 23*Power(t2,2) - 142*Power(t2,3) - 
                  75*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,4)*(98 + 35*Power(t1,2) + 57*t2 - 
                  45*Power(t2,2) + 9*t1*(-21 + 10*t2)) + 
               Power(s2,3)*(-262 - 60*Power(t1,3) + 
                  Power(t1,2)*(394 - 104*t2) + 44*t2 + 
                  185*Power(t2,2) + 51*Power(t2,3) + 
                  t1*(-94 - 381*t2 + 77*Power(t2,2))) + 
               Power(s2,2)*(-165 + 65*Power(t1,4) + 53*t2 + 
                  91*Power(t2,2) + 184*Power(t2,3) + 66*Power(t2,4) + 
                  4*Power(t1,3)*(-97 + 3*t2) - 
                  3*Power(t1,2)*(84 - 272*t2 + 3*Power(t2,2)) + 
                  t1*(631 + 5*t2 - 696*Power(t2,2) - 134*Power(t2,3))) \
+ s2*(306 - 38*Power(t1,5) - 141*t2 - 587*Power(t2,2) + 
                  310*Power(t2,3) + 147*Power(t2,4) - 3*Power(t2,5) + 
                  12*Power(t1,4)*(15 + 4*t2) + 
                  Power(t1,3)*(394 - 717*t2 - 33*Power(t2,2)) + 
                  Power(t1,2)*
                   (-444 - 265*t2 + 933*Power(t2,2) + 71*Power(t2,3)) + 
                  t1*(5 + 311*t2 - 127*Power(t2,2) - 543*Power(t2,3) - 
                     45*Power(t2,4)))) + 
            s1*(172 - 3*Power(t1,7) - 382*t2 + 204*Power(t2,2) + 
               72*Power(t2,3) - 24*Power(t2,4) + 11*Power(t2,5) + 
               3*Power(t2,6) + Power(t1,6)*(-14 + 15*t2) - 
               Power(s2,6)*(-5 + t1 + 15*t2) - 
               2*Power(t1,5)*(-27 + 7*t2 + 15*Power(t2,2)) + 
               Power(t1,4)*(-57 - 110*t2 + 163*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(t1,3)*(290 - 141*t2 + 67*Power(t2,2) - 
                  268*Power(t2,3) - 15*Power(t2,4)) + 
               Power(t1,2)*(176 - 700*t2 + 501*Power(t2,2) - 
                  9*Power(t2,3) + 176*Power(t2,4) + 3*Power(t2,5)) - 
               t1*(256 - 244*t2 - 194*Power(t2,2) + 279*Power(t2,3) + 
                  13*Power(t2,4) + 46*Power(t2,5)) + 
               Power(s2,5)*(22 + 8*Power(t1,2) - 12*t2 - 
                  72*Power(t2,2) + t1*(-22 + 85*t2)) + 
               Power(s2,4)*(98 - 25*Power(t1,3) + 
                  Power(t1,2)*(24 - 175*t2) - 14*Power(t2,2) - 
                  66*Power(t2,3) + t1*(-82 + 77*t2 + 266*Power(t2,2))) + 
               Power(s2,3)*(-153 + 40*Power(t1,4) + 61*t2 - 
                  19*Power(t2,2) + 83*Power(t2,3) + 24*Power(t2,4) + 
                  6*Power(t1,3)*(4 + 25*t2) + 
                  Power(t1,2)*(60 - 145*t2 - 336*Power(t2,2)) + 
                  t1*(-279 + 119*t2 - 34*Power(t2,2) + 122*Power(t2,3))) \
+ Power(s2,2)*(-210 - 35*Power(t1,5) - 92*t2 + 116*Power(t2,2) + 
                  49*Power(t2,3) + 138*Power(t2,4) + 33*Power(t2,5) - 
                  Power(t1,4)*(71 + 25*t2) + 
                  Power(t1,3)*(92 + 93*t2 + 132*Power(t2,2)) + 
                  Power(t1,2)*
                   (207 - 348*t2 + 273*Power(t2,2) - 16*Power(t2,3)) + 
                  t1*(732 - 491*t2 + 231*Power(t2,2) - 
                     433*Power(t2,3) - 89*Power(t2,4))) + 
               s2*(110 + 16*Power(t1,6) + Power(t1,5)*(54 - 35*t2) + 
                  256*t2 - 521*Power(t2,2) - 3*Power(t2,3) + 
                  177*Power(t2,4) + 61*Power(t2,5) + 
                  Power(t1,4)*(-146 + t2 + 40*Power(t2,2)) + 
                  Power(t1,3)*
                   (31 + 339*t2 - 388*Power(t2,2) - 70*Power(t2,3)) + 
                  Power(t1,2)*
                   (-869 + 571*t2 - 279*Power(t2,2) + 618*Power(t2,3) + 
                     80*Power(t2,4)) - 
                  t1*(-8 - 706*t2 + 407*Power(t2,2) + 91*Power(t2,3) + 
                     346*Power(t2,4) + 31*Power(t2,5))))) + 
         s*(-24 + 2*Power(s1,7)*(s2 - t1) + 130*t1 + 2*Power(t1,2) - 
            2*Power(t1,3) + 69*Power(t1,4) - 4*Power(t1,5) - 
            9*Power(t1,6) - 10*Power(t1,7) + 2*Power(t1,8) + 14*t2 - 
            380*t1*t2 - 144*Power(t1,2)*t2 - 191*Power(t1,3)*t2 - 
            43*Power(t1,4)*t2 + 78*Power(t1,5)*t2 + 34*Power(t1,6)*t2 - 
            7*Power(t1,7)*t2 + 150*Power(t2,2) + 652*t1*Power(t2,2) + 
            397*Power(t1,2)*Power(t2,2) + 145*Power(t1,3)*Power(t2,2) - 
            200*Power(t1,4)*Power(t2,2) - 58*Power(t1,5)*Power(t2,2) + 
            7*Power(t1,6)*Power(t2,2) - 378*Power(t2,3) - 
            625*t1*Power(t2,3) - 227*Power(t1,2)*Power(t2,3) + 
            218*Power(t1,3)*Power(t2,3) + 78*Power(t1,4)*Power(t2,3) + 
            2*Power(t1,5)*Power(t2,3) + 330*Power(t2,4) + 
            227*t1*Power(t2,4) - 99*Power(t1,2)*Power(t2,4) - 
            78*Power(t1,3)*Power(t2,4) - 8*Power(t1,4)*Power(t2,4) - 
            98*Power(t2,5) + 8*t1*Power(t2,5) + 
            46*Power(t1,2)*Power(t2,5) + 5*Power(t1,3)*Power(t2,5) + 
            4*Power(t2,6) - 14*t1*Power(t2,6) - Power(t1,2)*Power(t2,6) + 
            2*Power(t2,7) + 2*Power(s2,7)*t2*(2 - t1 + 3*t2) + 
            Power(s1,6)*(-2 + 4*Power(s2,3) + 5*t1 + 2*Power(t1,3) + 
               2*t2 + 7*t1*t2 - 2*Power(t1,2)*t2 + 
               Power(s2,2)*(6 - 6*t1 + 2*t2) - s2*(7 + 6*t1 + 5*t2)) + 
            Power(s2,6)*(-2 + 6*t2 + 13*Power(t2,2) + 14*Power(t2,3) + 
               2*Power(t1,2)*(1 + 6*t2) - t1*(4 + 37*t2 + 36*Power(t2,2))\
) + Power(s2,5)*(-12 - 11*t2 + 6*Power(t2,2) + 12*Power(t2,3) + 
               6*Power(t2,4) - 6*Power(t1,3)*(2 + 5*t2) + 
               6*Power(t1,2)*(5 + 22*t2 + 15*Power(t2,2)) - 
               t1*(-16 + 50*t2 + 93*Power(t2,2) + 60*Power(t2,3))) + 
            Power(s2,4)*(23 - 29*t2 - 59*Power(t2,2) - 8*Power(t2,3) - 
               2*Power(t2,4) - 6*Power(t2,5) + 
               10*Power(t1,4)*(3 + 4*t2) - 
               5*Power(t1,3)*(18 + 49*t2 + 24*Power(t2,2)) + 
               Power(t1,2)*(-53 + 174*t2 + 249*Power(t2,2) + 
                  100*Power(t2,3)) + 
               t1*(43 + 122*t2 - 83*Power(t2,2) - 54*Power(t2,3) - 
                  14*Power(t2,4))) + 
            Power(s2,3)*(-44 + 249*t2 - 165*Power(t2,2) - 
               119*Power(t2,3) + 2*Power(t2,4) - 8*Power(t2,5) - 
               4*Power(t2,6) - 10*Power(t1,5)*(4 + 3*t2) + 
               10*Power(t1,4)*(14 + 26*t2 + 9*Power(t2,2)) - 
               2*Power(t1,3)*
                (-46 + 158*t2 + 163*Power(t2,2) + 40*Power(t2,3)) + 
               Power(t1,2)*(-53 - 378*t2 + 271*Power(t2,2) + 
                  88*Power(t2,3) + 6*Power(t2,4)) + 
               t1*(-112 + 59*t2 + 437*Power(t2,2) - 81*Power(t2,3) + 
                  26*Power(t2,4) + 18*Power(t2,5))) + 
            s2*(-82 + 232*t2 - 580*Power(t2,2) + 827*Power(t2,3) - 
               449*Power(t2,4) + 38*Power(t2,5) + 16*Power(t2,6) - 
               2*Power(t1,7)*(6 + t2) + 
               Power(t1,6)*(54 + 52*t2 + 6*Power(t2,2)) - 
               Power(t1,5)*(-44 + 162*t2 + 69*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,4)*(5 - 323*t2 + 239*Power(t2,2) + 
                  12*Power(t2,3) - 4*Power(t2,4)) + 
               Power(t1,3)*(-204 + 87*t2 + 719*Power(t2,2) - 
                  253*Power(t2,3) + 38*Power(t2,4) + 6*Power(t2,5)) - 
               Power(t1,2)*(30 - 607*t2 + 394*Power(t2,2) + 
                  623*Power(t2,3) - 183*Power(t2,4) + 24*Power(t2,5) + 
                  2*Power(t2,6)) + 
               t1*(-144 + 708*t2 - 1358*Power(t2,2) + 775*Power(t2,3) + 
                  145*Power(t2,4) - 77*Power(t2,5) + 3*Power(t2,6))) + 
            Power(s2,2)*(118 - 430*t2 + 731*Power(t2,2) - 
               411*Power(t2,3) - 55*Power(t2,4) + 24*Power(t2,5) - 
               3*Power(t2,6) + 6*Power(t1,6)*(5 + 2*t2) - 
               3*Power(t1,5)*(40 + 53*t2 + 12*Power(t2,2)) + 
               Power(t1,4)*(-88 + 314*t2 + 219*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(t1,3)*(21 + 512*t2 - 375*Power(t2,2) - 
                  60*Power(t2,3) + 6*Power(t2,4)) - 
               Power(t1,2)*(-224 + 74*t2 + 897*Power(t2,2) - 
                  264*Power(t2,3) + 54*Power(t2,4) + 18*Power(t2,5)) + 
               t1*(76 - 665*t2 + 414*Power(t2,2) + 524*Power(t2,3) - 
                  107*Power(t2,4) + 27*Power(t2,5) + 6*Power(t2,6))) + 
            Power(s1,5)*(7 + 13*Power(s2,4) + 3*Power(t1,4) + 
               Power(t1,3)*(36 - 8*t2) - 7*Power(t2,2) - 
               Power(s2,3)*(9 + 33*t1 + 8*t2) - 
               t1*(33 + 6*t2 + Power(t2,2)) + 
               Power(t1,2)*(9 - 32*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(-41 + 30*Power(t1,2) - 10*t2 - 
                  9*Power(t2,2) + 2*t1*(30 + 7*t2)) + 
               s2*(18 - 13*Power(t1,3) + 26*t2 - 4*Power(t2,2) + 
                  Power(t1,2)*(-87 + 2*t2) + 
                  t1*(28 + 46*t2 + 4*Power(t2,2)))) + 
            Power(s1,4)*(-24 - 3*Power(s2,5) - 2*Power(t1,5) + 
               Power(s2,4)*(39 + 22*t1 - 48*t2) + 11*t2 + 
               10*Power(t2,2) + 3*Power(t2,3) + 
               3*Power(t1,4)*(20 + t2) + 
               Power(t1,3)*(60 - 161*t2 + 2*Power(t2,2)) - 
               Power(s2,3)*(54 + 46*Power(t1,2) + t1*(160 - 123*t2) - 
                  57*t2 + 10*Power(t2,2)) + 
               t1*(13 + 115*t2 + 10*Power(t2,2) - 16*Power(t2,3)) - 
               Power(t1,2)*(27 + 100*t2 - 111*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,2)*(-8 + 36*Power(t1,3) + 
                  Power(t1,2)*(263 - 99*t2) + 120*t2 - 38*Power(t2,2) + 
                  15*Power(t2,3) + t1*(126 - 259*t2 + 4*Power(t2,2))) + 
               s2*(25 - 7*Power(t1,4) - 127*t2 - 36*Power(t2,2) + 
                  16*Power(t2,3) + Power(t1,3)*(-202 + 21*t2) + 
                  Power(t1,2)*(-132 + 363*t2 + 4*Power(t2,2)) + 
                  2*t1*(13 + t2 - 43*Power(t2,2) - 6*Power(t2,3)))) + 
            Power(s1,3)*(12 + 9*Power(s2,6) - 4*Power(t1,6) + 102*t2 - 
               125*Power(t2,2) + 11*Power(t2,4) + 
               Power(t1,5)*(24 + 19*t2) + 
               Power(s2,5)*(1 - 41*t1 + 19*t2) + 
               Power(t1,4)*(44 - 108*t2 - 27*Power(t2,2)) + 
               Power(s2,4)*(-78 + 70*Power(t1,2) + t1*(57 - 85*t2) + 
                  74*Power(t2,2)) + 
               Power(t1,3)*(6 - 232*t2 + 174*Power(t2,2) + 
                  13*Power(t2,3)) - 
               Power(t1,2)*(-211 + 121*t2 - 300*Power(t2,2) + 
                  104*Power(t2,3) + Power(t2,4)) + 
               t1*(9 - 145*t2 - 21*Power(t2,2) - 71*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,3)*(-88 - 50*Power(t1,3) + 144*t2 + 
                  8*Power(t2,2) + 43*Power(t2,3) + 
                  Power(t1,2)*(-201 + 122*t2) + 
                  t1*(148 + 135*t2 - 189*Power(t2,2))) + 
               Power(s2,2)*(271 + 5*Power(t1,4) + 
                  Power(t1,3)*(251 - 46*t2) - 320*t2 + 40*Power(t2,2) + 
                  133*Power(t2,3) - 11*Power(t2,4) + 
                  3*Power(t1,2)*(-6 - 126*t2 + 43*Power(t2,2)) + 
                  t1*(152 - 400*t2 + 104*Power(t2,2) - 49*Power(t2,3))) \
+ s2*(3 + 11*Power(t1,5) - 57*t2 + 243*Power(t2,2) + 29*Power(t2,3) - 
                  4*Power(t2,4) - Power(t1,4)*(132 + 29*t2) + 
                  Power(t1,3)*(-96 + 351*t2 + 13*Power(t2,2)) - 
                  Power(t1,2)*
                   (70 - 488*t2 + 286*Power(t2,2) + 7*Power(t2,3)) + 
                  t1*(-443 + 403*t2 - 361*Power(t2,2) - 9*Power(t2,3) + 
                     12*Power(t2,4)))) - 
            Power(s1,2)*(-14 + 3*Power(s2,7) + Power(t1,6)*(6 - 7*t2) + 
               Power(s2,6)*(13 - 16*t1 - 6*t2) + 150*t2 - 
               38*Power(t2,2) - 125*Power(t2,3) + 16*Power(t2,4) + 
               11*Power(t2,5) + 
               Power(t1,5)*(53 - 49*t2 + 22*Power(t2,2)) + 
               Power(t1,4)*(-102 + 42*t2 + 76*Power(t2,2) - 
                  24*Power(t2,3)) + 
               Power(t1,3)*(-257 + 293*t2 - 318*Power(t2,2) - 
                  30*Power(t2,3) + 10*Power(t2,4)) - 
               Power(t1,2)*(-25 - 428*t2 + 302*Power(t2,2) - 
                  309*Power(t2,3) + 2*Power(t2,4) + Power(t2,5)) - 
               t1*(248 - 495*t2 + 524*Power(t2,2) - 203*Power(t2,3) + 
                  97*Power(t2,4) + Power(t2,5)) + 
               Power(s2,5)*(47 + 35*Power(t1,2) + 50*t2 + 
                  3*Power(t2,2) + t1*(-76 + 27*t2)) - 
               Power(s2,4)*(176 + 40*Power(t1,3) - 44*t2 - 
                  148*Power(t2,2) - 63*Power(t2,3) + 
                  5*Power(t1,2)*(-36 + 11*t2) + 
                  t1*(133 + 225*t2 + 26*Power(t2,2))) + 
               Power(s2,3)*(-31 + 25*Power(t1,4) - 282*t2 + 
                  266*Power(t2,2) + 185*Power(t2,3) + 48*Power(t2,4) + 
                  10*Power(t1,3)*(-22 + 7*t2) + 
                  Power(t1,2)*(64 + 424*t2 + 38*Power(t2,2)) - 
                  t1*(-730 + 276*t2 + 499*Power(t2,2) + 169*Power(t2,3))\
) + Power(s2,2)*(47 - 8*Power(t1,5) + Power(t1,4)*(145 - 60*t2) + 
                  452*t2 - 631*Power(t2,2) + 309*Power(t2,3) + 
                  145*Power(t2,4) - 3*Power(t2,5) + 
                  2*Power(t1,3)*(68 - 211*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*
                   (-1034 + 462*t2 + 630*Power(t2,2) + 125*Power(t2,3)) \
- t1*(286 - 993*t2 + 829*Power(t2,2) + 450*Power(t2,3) + 66*Power(t2,4))\
) + s2*(288 + Power(t1,6) - 601*t2 + 328*Power(t2,2) + 143*Power(t2,3) + 
                  3*Power(t2,4) + 11*Power(t2,5) + 
                  Power(t1,5)*(-48 + 31*t2) + 
                  Power(t1,4)*(-167 + 222*t2 - 49*Power(t2,2)) + 
                  Power(t1,3)*
                   (582 - 272*t2 - 355*Power(t2,2) + 5*Power(t2,3)) + 
                  Power(t1,2)*
                   (574 - 1004*t2 + 881*Power(t2,2) + 295*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  t1*(-90 - 759*t2 + 807*Power(t2,2) - 613*Power(t2,3) - 
                     125*Power(t2,4) + 4*Power(t2,5)))) + 
            s1*(58 + Power(t1,8) + Power(s2,7)*(2 - 9*t2) - 236*t2 + 
               540*Power(t2,2) - 446*Power(t2,3) + 80*Power(t2,4) + 
               4*Power(t2,5) - Power(t1,7)*(2 + 3*t2) + 
               Power(t1,6)*(-31 + 36*t2 + 2*Power(t2,2)) + 
               Power(t1,5)*(23 + 54*t2 - 111*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,4)*(-48 + 77*t2 - 3*Power(t2,2) + 
                  140*Power(t2,3) - 3*Power(t2,4)) + 
               Power(t1,3)*(187 - 237*t2 - 129*Power(t2,2) - 
                  53*Power(t2,3) - 84*Power(t2,4) + Power(t2,5)) + 
               Power(t1,2)*(382 - 980*t2 + 866*Power(t2,2) - 
                  109*Power(t2,3) + 54*Power(t2,4) + 24*Power(t2,5)) - 
               t1*(116 + 264*t2 - 807*Power(t2,2) + 585*Power(t2,3) - 
                  134*Power(t2,4) + 21*Power(t2,5) + 3*Power(t2,6)) + 
               Power(s2,6)*(15 + Power(t1,2) + 8*t2 - 34*Power(t2,2) + 
                  t1*(-13 + 53*t2)) - 
               Power(s2,5)*(10 + 6*Power(t1,3) - 30*t2 - 
                  51*Power(t2,2) + 18*Power(t2,3) + 
                  Power(t1,2)*(-37 + 127*t2) + 
                  t1*(56 + 49*t2 - 148*Power(t2,2))) + 
               Power(s2,4)*(82 + 15*Power(t1,4) - 188*t2 + 
                  99*Power(t2,2) + 131*Power(t2,3) + 30*Power(t2,4) + 
                  5*Power(t1,3)*(-12 + 31*t2) + 
                  Power(t1,2)*(43 + 152*t2 - 250*Power(t2,2)) + 
                  t1*(29 - 18*t2 - 304*Power(t2,2) + 42*Power(t2,3))) - 
               Power(s2,3)*(295 + 20*Power(t1,5) - 407*t2 + 
                  275*Power(t2,2) - 215*Power(t2,3) - 137*Power(t2,4) - 
                  23*Power(t2,5) + Power(t1,4)*(-60 + 95*t2) + 
                  Power(t1,3)*(-88 + 278*t2 - 200*Power(t2,2)) + 
                  Power(t1,2)*
                   (50 + 180*t2 - 717*Power(t2,2) + 20*Power(t2,3)) + 
                  t1*(179 - 469*t2 + 231*Power(t2,2) + 580*Power(t2,3) + 
                     88*Power(t2,4))) + 
               Power(s2,2)*(132 + 15*Power(t1,6) - 524*t2 + 
                  622*Power(t2,2) - 258*Power(t2,3) + 166*Power(t2,4) + 
                  57*Power(t2,5) + Power(t1,5)*(-37 + 19*t2) + 
                  Power(t1,4)*(-187 + 292*t2 - 70*Power(t2,2)) + 
                  Power(t1,3)*
                   (76 + 348*t2 - 837*Power(t2,2) - 12*Power(t2,3)) + 
                  Power(t1,2)*
                   (64 - 297*t2 + 162*Power(t2,2) + 907*Power(t2,3) + 
                     83*Power(t2,4)) - 
                  t1*(-865 + 1285*t2 - 631*Power(t2,2) + 
                     525*Power(t2,3) + 382*Power(t2,4) + 35*Power(t2,5))) \
+ s2*(120 - 6*Power(t1,7) + 376*t2 - 1175*Power(t2,2) + 775*Power(t2,3) - 
                  29*Power(t2,4) - 25*Power(t2,5) + 6*Power(t2,6) + 
                  Power(t1,6)*(13 + 7*t2) + 
                  Power(t1,5)*(128 - 161*t2 + 4*Power(t2,2)) + 
                  Power(t1,4)*
                   (-68 - 234*t2 + 484*Power(t2,2) + 6*Power(t2,3)) - 
                  Power(t1,3)*
                   (-81 + 61*t2 + 27*Power(t2,2) + 598*Power(t2,3) + 
                     22*Power(t2,4)) + 
                  Power(t1,2)*
                   (-757 + 1115*t2 - 227*Power(t2,2) + 363*Power(t2,3) + 
                     329*Power(t2,4) + 11*Power(t2,5)) - 
                  t1*(576 - 1644*t2 + 1519*Power(t2,2) - 
                     297*Power(t2,3) + 205*Power(t2,4) + 73*Power(t2,5))))\
))*T3(1 - s + s1 - t2,1 - s + s2 - t1))/
     ((-1 + s2)*(-s + s2 - t1)*(s - s1 + t2)*(-1 + s2 - t1 + t2)*
       Power(-s1 + s2 - t1 + t2,2)*
       Power(-1 + s - s*s1 + s1*s2 - s1*t1 + t2,2)) - 
    (8*(8 - 2*t1 + 20*s1*t1 - 11*Power(t1,2) + 9*s1*Power(t1,2) + 
         14*Power(s1,2)*Power(t1,2) + 35*Power(t1,3) - 
         15*s1*Power(t1,3) + 26*Power(s1,2)*Power(t1,3) - 
         12*Power(t1,4) + 42*s1*Power(t1,4) + 
         15*Power(s1,3)*Power(t1,4) - 2*Power(s1,4)*Power(t1,4) - 
         8*Power(t1,5) - 22*s1*Power(t1,5) + 10*Power(s1,2)*Power(t1,5) + 
         4*Power(s1,3)*Power(t1,5) - 3*Power(t1,6) - 7*s1*Power(t1,6) - 
         10*Power(s1,2)*Power(t1,6) - Power(s1,3)*Power(t1,6) + 
         Power(t1,7) - s1*Power(t1,7) + 2*Power(s,7)*Power(t1 - t2,2) - 
         32*t2 + 12*t1*t2 - 60*s1*t1*t2 + 9*Power(t1,2)*t2 - 
         11*s1*Power(t1,2)*t2 - 28*Power(s1,2)*Power(t1,2)*t2 - 
         36*Power(t1,3)*t2 - 17*s1*Power(t1,3)*t2 - 
         30*Power(s1,2)*Power(t1,3)*t2 + 36*Power(t1,4)*t2 + 
         4*s1*Power(t1,4)*t2 - 22*Power(s1,2)*Power(t1,4)*t2 - 
         5*Power(s1,3)*Power(t1,4)*t2 + 3*Power(t1,5)*t2 + 
         30*s1*Power(t1,5)*t2 + 17*Power(s1,2)*Power(t1,5)*t2 + 
         Power(s1,3)*Power(t1,5)*t2 + 2*Power(t1,6)*t2 - 
         s1*Power(t1,6)*t2 + 48*Power(t2,2) - 24*t1*Power(t2,2) + 
         60*s1*t1*Power(t2,2) + 13*Power(t1,2)*Power(t2,2) - 
         5*s1*Power(t1,2)*Power(t2,2) + 
         14*Power(s1,2)*Power(t1,2)*Power(t2,2) - 
         15*Power(t1,3)*Power(t2,2) + 27*s1*Power(t1,3)*Power(t2,2) + 
         4*Power(s1,2)*Power(t1,3)*Power(t2,2) - 
         20*Power(t1,4)*Power(t2,2) - 32*s1*Power(t1,4)*Power(t2,2) - 
         4*Power(s1,2)*Power(t1,4)*Power(t2,2) + 
         Power(t1,5)*Power(t2,2) - 32*Power(t2,3) + 20*t1*Power(t2,3) - 
         20*s1*t1*Power(t2,3) - 9*Power(t1,2)*Power(t2,3) + 
         7*s1*Power(t1,2)*Power(t2,3) + 16*Power(t1,3)*Power(t2,3) + 
         5*s1*Power(t1,3)*Power(t2,3) + 8*Power(t2,4) - 
         6*t1*Power(t2,4) - 2*Power(t1,2)*Power(t2,4) + 
         Power(s2,7)*(Power(s1,2) + 2*s1*t2 - Power(t2,2)) + 
         Power(s,6)*(9*Power(t1,3) - 
            Power(t1,2)*(9 + 4*s1 + 9*s2 + 17*t2) + 
            t2*(1 - 7*s2 + s1*(3 + 3*s2 - 2*t2) - 7*t2 - 13*s2*t2 + 
               Power(t2,2)) + 
            t1*(3 + 3*s2 + 16*t2 + 22*s2*t2 + 7*Power(t2,2) + 
               s1*(-7 + s2 + 6*t2))) - 
         Power(s2,6)*(Power(s1,2)*(7 + 6*t1 - 4*t2) - 
            t2*(-2 + Power(t2,2) + t1*(2 + 5*t2)) + 
            s1*(2 + 3*t2 + 3*Power(t2,2) + t1*(2 + 11*t2))) + 
         Power(s2,5)*(1 + 3*t2 + Power(s1,3)*(-6 + t1 + t2) + 
            Power(s1,2)*(4 + 15*Power(t1,2) + t1*(44 - 19*t2) - 6*t2 - 
               Power(t2,2)) - Power(t1,2)*(1 + 10*t2 + 10*Power(t2,2)) + 
            t1*(2 + 11*t2 - 3*Power(t2,2) - 4*Power(t2,3)) + 
            s1*(14 - 21*t2 - 9*Power(t2,2) + Power(t1,2)*(11 + 25*t2) + 
               t1*(15 + 20*t2 + 13*Power(t2,2)))) - 
         Power(s2,4)*(7 + 2*Power(s1,4) - 17*t2 - 3*Power(t2,2) + 
            9*Power(t2,3) - 5*Power(t1,3)*(1 + 4*t2 + 2*Power(t2,2)) + 
            Power(t1,2)*(11 + 22*t2 - 12*Power(t2,2) - 6*Power(t2,3)) - 
            t1*(-9 - 14*t2 + 2*Power(t2,2) + Power(t2,3)) + 
            Power(s1,3)*(-9 + 5*Power(t1,2) - t2 + t1*(-28 + 3*t2)) + 
            Power(s1,2)*(9 + 20*Power(t1,3) + 8*t2 + 9*Power(t2,2) - 
               4*Power(t1,2)*(-29 + 9*t2) + 
               t1*(10 - 45*t2 - 4*Power(t2,2))) + 
            s1*(8 - 47*t2 + 22*Power(t2,2) + 3*Power(t2,3) + 
               5*Power(t1,3)*(5 + 6*t2) + 
               t1*(75 - 116*t2 - 31*Power(t2,2)) + 
               Power(t1,2)*(47 + 51*t2 + 22*Power(t2,2)))) + 
         Power(s2,2)*(-12*Power(s1,4)*Power(t1,2) - 
            2*Power(s1,3)*(-6 + t1)*Power(t1,2)*(6 + 5*t1 - t2) + 
            Power(t1,5)*(10 + 10*t2 + Power(t2,2)) - 
            Power(-1 + t2,2)*(21 - 3*t2 + 8*Power(t2,2)) + 
            Power(t1,4)*(-26 - 2*t2 + 12*Power(t2,2) + Power(t2,3)) + 
            Power(t1,3)*(-43 - 9*t2 + 9*Power(t2,2) + 3*Power(t2,3)) - 
            Power(t1,2)*(53 - 155*t2 + 55*Power(t2,2) + 
               23*Power(t2,3)) + 
            t1*(27 + 46*t2 - 115*Power(t2,2) + 38*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(s1,2)*(-6*Power(t1,5) + 14*Power(-1 + t2,2) + 
               Power(t1,4)*(-131 + 16*t2) + 
               2*Power(t1,3)*(16 + 69*t2 + 2*Power(t2,2)) + 
               t1*(74 - 82*t2 + 8*Power(t2,2)) - 
               3*Power(t1,2)*(9 + 30*t2 + 13*Power(t2,2))) - 
            s1*(Power(-1 + t2,2)*(-21 + 5*t2) + 
               Power(t1,5)*(20 + 7*t2) + 
               Power(t1,3)*(179 - 270*t2 - 21*Power(t2,2)) + 
               Power(t1,4)*(72 + 41*t2 + 7*Power(t2,2)) + 
               t1*(87 - 59*t2 + 13*Power(t2,2) - 41*Power(t2,3)) + 
               3*Power(t1,2)*(-34 - 51*t2 + 54*Power(t2,2) + 
                  3*Power(t2,3)))) + 
         Power(s2,3)*(4 + 8*Power(s1,4)*t1 - 41*t2 + 50*Power(t2,2) - 
            11*Power(t2,3) - 2*Power(t2,4) + 
            2*Power(s1,3)*t1*
             (-21 + 5*Power(t1,2) + t1*(-26 + t2) + t2) - 
            5*Power(t1,4)*(2 + 4*t2 + Power(t2,2)) + 
            Power(t1,2)*(29 + 21*t2 - 7*Power(t2,2) - 3*Power(t2,3)) - 
            2*Power(t1,3)*(-12 - 9*t2 + 9*Power(t2,2) + 2*Power(t2,3)) + 
            t1*(31 - 85*t2 + 13*Power(t2,2) + 25*Power(t2,3)) + 
            Power(s1,2)*(15*Power(t1,4) + Power(t1,3)*(164 - 34*t2) - 
               2*(12 - 13*t2 + Power(t2,2)) - 
               2*Power(t1,2)*(2 + 58*t2 + 3*Power(t2,2)) + 
               t1*(27 + 46*t2 + 31*Power(t2,2))) + 
            s1*(36 - 38*t2 + 20*Power(t2,2) - 18*Power(t2,3) + 
               10*Power(t1,4)*(3 + 2*t2) + 
               Power(t1,2)*(163 - 252*t2 - 39*Power(t2,2)) + 
               2*Power(t1,3)*(39 + 32*t2 + 9*Power(t2,2)) + 
               t1*(-18 - 145*t2 + 98*Power(t2,2) + 9*Power(t2,3)))) + 
         s2*(8*Power(s1,4)*Power(t1,3) - 2*(-3 + t2)*Power(-1 + t2,3) - 
            Power(t1,6)*(5 + 2*t2) + 
            Power(t1,5)*(14 - 5*t2 - 3*Power(t2,2)) + 
            2*t1*Power(-1 + t2,2)*(16 + 5*t2 + 5*Power(t2,2)) - 
            Power(t1,4)*(-30 + 4*t2 + 5*Power(t2,2) + Power(t2,3)) + 
            Power(t1,3)*(41 - 123*t2 + 59*Power(t2,2) + 7*Power(t2,3)) + 
            Power(t1,2)*(-66 + 31*t2 + 80*Power(t2,2) - 
               43*Power(t2,3) - 2*Power(t2,4)) + 
            Power(s1,3)*Power(t1,3)*
             (-54 + 5*Power(t1,2) + 14*t2 - t1*(22 + 3*t2)) + 
            Power(s1,2)*t1*(Power(t1,5) + Power(t1,4)*(56 - 3*t2) - 
               28*Power(-1 + t2,2) - 
               Power(t1,3)*(32 + 78*t2 + Power(t2,2)) - 
               2*t1*(38 - 43*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(9 + 74*t2 + 21*Power(t2,2))) + 
            s1*(20*Power(-1 + t2,3) + Power(t1,6)*(7 + t2) - 
               2*t1*Power(-1 + t2,2)*(15 + t2) + 
               Power(t1,4)*(99 - 143*t2 - 4*Power(t2,2)) + 
               Power(t1,5)*(35 + 12*t2 + Power(t2,2)) + 
               Power(t1,3)*(-118 - 59*t2 + 118*Power(t2,2) + 
                  3*Power(t2,3)) - 
               2*Power(t1,2)*(-33 + 2*t2 + 17*Power(t2,2) + 
                  14*Power(t2,3)))) - 
         Power(s,5)*(3 + 18*t1 - 14*Power(t1,2) + 36*Power(t1,3) - 
            16*Power(t1,4) - 4*t2 - 12*t1*t2 - 59*Power(t1,2)*t2 + 
            28*Power(t1,3)*t2 + 2*Power(t2,2) + 20*t1*Power(t2,2) - 
            8*Power(t1,2)*Power(t2,2) + 3*Power(t2,3) - 
            4*t1*Power(t2,3) + 
            Power(s1,2)*(2 + Power(s2,2) - 3*Power(t1,2) + 4*t2 - 
               Power(t2,2) + t1*(-5 + 4*t2) + s2*(-7 + t1 + 4*t2)) + 
            Power(s2,2)*(-16*Power(t1,2) - 4*t2*(8 + 9*t2) + 
               t1*(11 + 50*t2)) + 
            s2*(-7 + 32*Power(t1,3) - 2*t2 - 29*Power(t2,2) + 
               6*Power(t2,3) - Power(t1,2)*(47 + 78*t2) + 
               t1*(13 + 102*t2 + 40*Power(t2,2))) + 
            s1*(-7 + 17*Power(t1,3) + Power(t1,2)*(11 - 24*t2) + 11*t2 - 
               13*Power(t2,2) + Power(s2,2)*(-3 + 4*t1 + 17*t2) + 
               t1*(-24 + 24*t2 + 7*Power(t2,2)) + 
               s2*(18 - 21*Power(t1,2) + 2*t2 - 13*Power(t2,2) + 
                  6*t1*(-3 + 2*t2)))) + 
         Power(s,4)*(38*t1 - 31*Power(t1,2) + 32*Power(t1,3) - 
            56*Power(t1,4) + 14*Power(t1,5) - 36*t2 - 54*t1*t2 + 
            29*Power(t1,2)*t2 + 82*Power(t1,3)*t2 - 22*Power(t1,4)*t2 + 
            23*Power(t2,2) - 16*Power(t1,2)*Power(t2,2) + 
            2*Power(t1,3)*Power(t2,2) - Power(t2,3) - 
            10*t1*Power(t2,3) + 6*Power(t1,2)*Power(t2,3) + 
            Power(s1,3)*(-Power(t1,2) + 2*t2 + t1*(2 + t2) + 
               s2*(-6 + t1 + t2)) + 
            Power(s2,3)*(-14*Power(t1,2) + 15*t1*(1 + 4*t2) - 
               t2*(58 + 55*t2)) + 
            Power(s1,2)*(5*Power(s2,3) + 12*Power(t1,3) + 
               Power(t1,2)*(1 - 16*t2) + 
               Power(s2,2)*(-35 - 2*t1 + 20*t2) + 
               s2*(40 - 15*Power(t1,2) + t1*(29 - 3*t2) + 9*t2 - 
                  5*Power(t2,2)) + 2*t1*(-29 + 8*t2 + 2*Power(t2,2)) - 
               2*(8 - 12*t2 + 5*Power(t2,2))) + 
            Power(s2,2)*(-9 + 42*Power(t1,3) - 16*t2 - 46*Power(t2,2) + 
               15*Power(t2,3) - 2*Power(t1,2)*(43 + 71*t2) + 
               t1*(22 + 241*t2 + 95*Power(t2,2))) + 
            s2*(-9 - 42*Power(t1,4) + 23*t2 + 18*Power(t2,2) + 
               12*Power(t2,3) + Power(t1,3)*(127 + 104*t2) - 
               Power(t1,2)*(54 + 265*t2 + 42*Power(t2,2)) + 
               t1*(46 - 18*t2 + 56*Power(t2,2) - 20*Power(t2,3))) + 
            s1*(14 - 28*Power(t1,4) + 5*t2 - 2*Power(t2,2) - 
               3*Power(t2,3) + 18*Power(t1,3)*(1 + 2*t2) + 
               Power(s2,3)*(-11 + 6*t1 + 40*t2) + 
               Power(t1,2)*(79 - 116*t2 - 8*Power(t2,2)) + 
               t1*(34 - 29*t2 + 51*Power(t2,2)) - 
               Power(s2,2)*(-54 + 40*Power(t1,2) + 23*t2 + 
                  35*Power(t2,2) + t1*(-4 + 23*t2)) + 
               s2*(-27 + 62*Power(t1,3) + 9*t2 - 60*Power(t2,2) - 
                  Power(t1,2)*(11 + 53*t2) + 
                  t1*(-137 + 153*t2 + 41*Power(t2,2))))) + 
         Power(s,3)*(-6 + 2*Power(s1,4)*(s2 - t1) - 35*t1 + 
            123*Power(t1,2) - 3*Power(t1,3) + 44*Power(t1,4) - 
            42*Power(t1,5) + 6*Power(t1,6) + 20*t2 - 93*t1*t2 - 
            173*Power(t1,2)*t2 + 22*Power(t1,3)*t2 + 52*Power(t1,4)*t2 - 
            8*Power(t1,5)*t2 - 8*Power(t2,2) + 76*t1*Power(t2,2) + 
            22*Power(t1,2)*Power(t2,2) + 2*Power(t1,3)*Power(t2,2) - 
            2*Power(t1,4)*Power(t2,2) - 8*Power(t2,3) - 
            10*t1*Power(t2,3) - 12*Power(t1,2)*Power(t2,3) + 
            4*Power(t1,3)*Power(t2,3) + 2*Power(t2,4) + 
            Power(s1,3)*(-4*Power(t1,3) + 
               s2*(-19 - 34*t1 + 8*Power(t1,2) - 7*t2) - 14*(-1 + t2) + 
               t1*(25 + t2) - 4*Power(s2,2)*(-6 + t1 + t2) + 
               2*Power(t1,2)*(5 + 2*t2)) + 
            Power(s2,4)*(6*Power(t1,2) + 2*t2*(26 + 25*t2) - 
               t1*(9 + 40*t2)) - 
            Power(s2,3)*(5 + 24*Power(t1,3) - 28*t2 - 34*Power(t2,2) + 
               20*Power(t2,3) - Power(t1,2)*(69 + 128*t2) + 
               3*t1*(7 + 91*t2 + 40*Power(t2,2))) + 
            s2*(23 - 24*Power(t1,5) + 103*t2 - 76*Power(t2,2) + 
               12*Power(t2,3) + Power(t1,4)*(135 + 64*t2) - 
               Power(t1,3)*(109 + 273*t2 + 16*Power(t2,2)) + 
               Power(t1,2)*(16 - 22*t2 + 9*Power(t2,2) - 
                  24*Power(t2,3)) + 
               t1*(-159 + 261*t2 + 13*Power(t2,2) + 29*Power(t2,3))) + 
            Power(s2,2)*(35 + 36*Power(t1,4) - 79*t2 - 42*Power(t2,2) - 
               18*Power(t2,3) - 9*Power(t1,3)*(17 + 16*t2) + 
               Power(t1,2)*(86 + 442*t2 + 88*Power(t2,2)) + 
               t1*(-8 - 28*t2 - 45*Power(t2,2) + 40*Power(t2,3))) + 
            Power(s1,2)*(-17 - 10*Power(s2,4) + 18*Power(t1,4) + 
               2*Power(s2,3)*(35 + 9*t1 - 20*t2) + 14*t2 + 
               3*Power(t2,2) - Power(t1,3)*(37 + 24*t2) + 
               t1*(-47 + 33*t2 - 34*Power(t2,2)) + 
               Power(t1,2)*(-134 + 89*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(-104 + 12*Power(t1,2) + 3*t2 + 
                  10*Power(t2,2) + t1*(-161 + 52*t2)) + 
               s2*(64 - 38*Power(t1,3) - 55*t2 + 39*Power(t2,2) + 
                  4*Power(t1,2)*(32 + 3*t2) - 
                  2*t1*(-121 + 48*t2 + 8*Power(t2,2)))) - 
            s1*(4 + 22*Power(t1,5) - 40*t2 + 54*Power(t2,2) - 
               18*Power(t2,3) - Power(t1,4)*(49 + 24*t2) + 
               Power(s2,4)*(-15 + 4*t1 + 50*t2) + 
               Power(t1,2)*(51 - 4*t2 - 75*Power(t2,2)) + 
               Power(t1,3)*(-95 + 175*t2 + 2*Power(t2,2)) + 
               t1*(26 - 196*t2 + 55*Power(t2,2) + 9*Power(t2,3)) - 
               Power(s2,3)*(-55 + 34*Power(t1,2) + 54*t2 + 
                  50*Power(t2,2) + t1*(-45 + 92*t2)) + 
               Power(s2,2)*(3 + 78*Power(t1,3) - 54*t2 - 
                  111*Power(t2,2) + 2*Power(t1,2)*(-47 + 5*t2) + 
                  t1*(-219 + 329*t2 + 94*Power(t2,2))) + 
               s2*(24 - 70*Power(t1,4) + 119*t2 - 25*Power(t2,2) - 
                  12*Power(t2,3) + Power(t1,3)*(113 + 56*t2) + 
                  Power(t1,2)*(259 - 450*t2 - 46*Power(t2,2)) + 
                  t1*(-51 + 60*t2 + 181*Power(t2,2))))) - 
         Power(s,2)*(-11 + 6*Power(s1,4)*Power(s2 - t1,2) + 21*t1 + 
            168*Power(t1,2) - 86*Power(t1,3) - 16*Power(t1,4) - 
            34*Power(t1,5) + 15*Power(t1,6) - Power(t1,7) + 3*t2 - 
            166*t1*t2 - 66*Power(t1,2)*t2 + 171*Power(t1,3)*t2 - 
            2*Power(t1,4)*t2 - 14*Power(t1,5)*t2 + Power(t1,6)*t2 + 
            35*Power(t2,2) + 163*t1*Power(t2,2) - 
            57*Power(t1,2)*Power(t2,2) - 37*Power(t1,3)*Power(t2,2) - 
            7*Power(t1,4)*Power(t2,2) + Power(t1,5)*Power(t2,2) - 
            35*Power(t2,3) - 14*t1*Power(t2,3) + 
            17*Power(t1,2)*Power(t2,3) + 6*Power(t1,3)*Power(t2,3) - 
            Power(t1,4)*Power(t2,3) + 8*Power(t2,4) - 4*t1*Power(t2,4) - 
            Power(s1,3)*(s2 - t1)*
             (6*Power(t1,3) + 28*(-1 + t2) - 6*Power(t1,2)*(3 + t2) + 
               6*Power(s2,2)*(-6 + t1 + t2) + t1*(-65 + 9*t2) + 
               s2*(47 + 54*t1 - 12*Power(t1,2) + 9*t2)) + 
            Power(s2,5)*(Power(t1,2) - 2*t1*(1 + 7*t2) + 
               t2*(23 + 27*t2)) - 
            Power(s2,4)*(9 + 5*Power(t1,3) - 23*t2 - 11*Power(t2,2) + 
               15*Power(t2,3) - Power(t1,2)*(23 + 57*t2) + 
               t1*(13 + 153*t2 + 85*Power(t2,2))) + 
            s2*(14 + 5*Power(t1,6) + 81*t2 - 96*Power(t2,2) - 
               5*Power(t2,3) + 6*Power(t2,4) - 
               2*Power(t1,5)*(31 + 9*t2) + 
               Power(t1,4)*(115 + 126*t2 + Power(t2,2)) + 
               t1*(-223 + 3*t2 + 141*Power(t2,2) - 45*Power(t2,3)) + 
               Power(t1,2)*(203 - 441*t2 + 51*Power(t2,2) - 
                  21*Power(t2,3)) + 
               Power(t1,3)*(46 - 23*t2 + 37*Power(t2,2) + 
                  12*Power(t2,3))) + 
            Power(s2,2)*(57 - 10*Power(t1,5) + 61*t2 - 86*Power(t2,2) + 
               30*Power(t2,3) + Power(t1,4)*(98 + 62*t2) - 
               Power(t1,3)*(141 + 317*t2 + 36*Power(t2,2)) + 
               t1*(-143 + 346*t2 + 24*Power(t2,2) + 27*Power(t2,3)) - 
               Power(t1,2)*(53 - 75*t2 + 42*Power(t2,2) + 
                  36*Power(t2,3))) + 
            Power(s2,3)*(10*Power(t1,4) - 8*Power(t1,3)*(9 + 11*t2) + 
               Power(t1,2)*(73 + 335*t2 + 92*Power(t2,2)) - 
               2*(-13 + 38*t2 + 19*Power(t2,2) + 6*Power(t2,3)) + 
               t1*(32 - 73*t2 + Power(t2,2) + 40*Power(t2,3))) + 
            Power(s1,2)*(-10*Power(s2,5) - 12*Power(t1,5) + 
               Power(s2,4)*(70 + 32*t1 - 40*t2) - 34*Power(-1 + t2,2) + 
               Power(t1,4)*(67 + 16*t2) + 
               Power(t1,3)*(92 - 131*t2 - 4*Power(t2,2)) + 
               14*Power(t1,2)*(3 + 2*t2 + 3*Power(t2,2)) - 
               2*t1*(8 - 13*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(-100 - 24*Power(t1,2) + 23*t2 + 
                  10*Power(t2,2) + 7*t1*(-37 + 14*t2)) + 
               Power(s2,2)*(85 - 20*Power(t1,3) + 
                  Power(t1,2)*(375 - 60*t2) - 30*t2 + 57*Power(t2,2) + 
                  t1*(304 - 189*t2 - 24*Power(t2,2))) + 
               s2*(14 + 34*Power(t1,4) - 22*t2 + 8*Power(t2,2) - 
                  Power(t1,3)*(253 + 14*t2) + 
                  t1*(-127 + 2*t2 - 99*Power(t2,2)) + 
                  Power(t1,2)*(-296 + 297*t2 + 18*Power(t2,2)))) - 
            s1*(-8*Power(t1,6) + Power(t1,5)*(34 + 6*t2) - 
               Power(-1 + t2,2)*(29 + 7*t2) + 
               Power(s2,5)*(-9 + t1 + 35*t2) + 
               2*Power(t1,4)*(21 - 57*t2 + Power(t2,2)) + 
               Power(t1,3)*(-190 + 81*t2 + 49*Power(t2,2)) - 
               Power(t1,2)*(2 - 351*t2 + 136*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(1 + 3*t2 - 45*Power(t2,2) + 41*Power(t2,3)) - 
               Power(s2,4)*(-18 + 12*Power(t1,2) + 49*t2 + 
                  40*Power(t2,2) + 3*t1*(-13 + 36*t2)) + 
               Power(s2,3)*(69 + 38*Power(t1,3) - 112*t2 - 
                  103*Power(t2,2) + Power(t1,2)*(-97 + 108*t2) + 
                  t1*(-114 + 315*t2 + 106*Power(t2,2))) + 
               Power(s2,2)*(28 - 52*Power(t1,4) + 
                  Power(t1,3)*(147 - 26*t2) + 260*t2 - 66*Power(t2,2) - 
                  18*Power(t2,3) - 
                  3*Power(t1,2)*(-72 + 199*t2 + 30*Power(t2,2)) + 
                  t1*(-319 + 311*t2 + 240*Power(t2,2))) + 
               s2*(33*Power(t1,5) - 3*Power(t1,4)*(38 + 5*t2) + 
                  Power(t1,3)*(-162 + 445*t2 + 22*Power(t2,2)) - 
                  2*Power(t1,2)*(-220 + 140*t2 + 93*Power(t2,2)) - 
                  6*(-8 + 19*t2 - 20*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(-26 - 611*t2 + 202*Power(t2,2) + 27*Power(t2,3))))) \
+ s*(-10 + 6*Power(s1,4)*Power(s2 - t1,3) + 32*t1 + 76*Power(t1,2) - 
            121*Power(t1,3) - 4*Power(t1,4) + 3*Power(t1,5) + 
            12*Power(t1,6) - 2*Power(t1,7) + 28*t2 - 82*t1*t2 + 
            30*Power(t1,2)*t2 + 151*Power(t1,3)*t2 - 53*Power(t1,4)*t2 + 
            Power(t1,6)*t2 - 24*Power(t2,2) + 58*t1*Power(t2,2) - 
            146*Power(t1,2)*Power(t2,2) - 16*Power(t1,3)*Power(t2,2) + 
            18*Power(t1,4)*Power(t2,2) + 2*Power(t1,5)*Power(t2,2) + 
            4*Power(t2,3) + 2*t1*Power(t2,3) + 
            38*Power(t1,2)*Power(t2,3) - 8*Power(t1,3)*Power(t2,3) - 
            Power(t1,4)*Power(t2,3) + 2*Power(t2,4) - 10*t1*Power(t2,4) + 
            2*Power(t1,2)*Power(t2,4) + 
            2*Power(s2,6)*t2*(2 - t1 + 4*t2) - 
            Power(s1,3)*Power(s2 - t1,2)*
             (4*Power(t1,3) + 14*(-1 + t2) + 
               4*Power(s2,2)*(-6 + t1 + t2) - 2*Power(t1,2)*(7 + 2*t2) + 
               s2*(37 + 38*t1 - 8*Power(t1,2) + 5*t2) + t1*(-55 + 13*t2)) \
+ Power(s2,5)*(-2 + 10*t2 + Power(t2,2) - 6*Power(t2,3) + 
               2*Power(t1,2)*(1 + 5*t2) - t1*(4 + 37*t2 + 32*Power(t2,2))\
) + Power(s2,4)*(2 - 27*t2 - 12*Power(t2,2) - 3*Power(t2,3) - 
               10*Power(t1,3)*(1 + 2*t2) + 
               Power(t1,2)*(28 + 109*t2 + 48*Power(t2,2)) + 
               t1*(10 - 50*t2 + 13*Power(t2,2) + 20*Power(t2,3))) + 
            Power(s2,3)*(25 - 23*t2 - 36*Power(t2,2) + 28*Power(t2,3) + 
               20*Power(t1,4)*(1 + t2) - 
               2*Power(t1,3)*(36 + 73*t2 + 16*Power(t2,2)) + 
               t1*(-9 + 153*t2 + 9*Power(t2,2) + 7*Power(t2,3)) - 
               Power(t1,2)*(21 - 90*t2 + 47*Power(t2,2) + 24*Power(t2,3))\
) + s2*(2*Power(t1,6)*(5 + t2) - Power(t1,5)*(52 + 25*t2) + 
               2*Power(-1 + t2,2)*(1 - 11*t2 + 8*Power(t2,2)) - 
               Power(t1,4)*(13 - 20*t2 + 22*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,3)*(3 + 205*t2 - 51*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(263 - 321*t2 + 40*Power(t2,3)) - 
               4*t1*(31 + 25*t2 - 71*Power(t2,2) + 13*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s2,2)*(-10*Power(t1,5)*(2 + t2) + 
               Power(t1,4)*(88 + 94*t2 + 8*Power(t2,2)) + 
               t1*(-167 + 193*t2 + 52*Power(t2,2) - 60*Power(t2,3)) + 
               Power(t1,2)*(8 - 278*t2 + 36*Power(t2,2) - 
                  6*Power(t2,3)) + 
               Power(t1,3)*(23 - 70*t2 + 53*Power(t2,2) + 
                  12*Power(t2,3)) + 
               2*(24 + 35*t2 - 69*Power(t2,2) + 7*Power(t2,3) + 
                  3*Power(t2,4))) - 
            Power(s1,2)*(s2 - t1)*
             (5*Power(s2,5) + 3*Power(t1,5) + 48*Power(-1 + t2,2) - 
               4*Power(t1,4)*(11 + t2) + 
               Power(s2,4)*(-35 - 18*t1 + 20*t2) + 
               Power(s2,3)*(38 + 21*Power(t1,2) + t1*(141 - 52*t2) - 
                  21*t2 - 5*Power(t2,2)) + 
               Power(t1,3)*(-4 + 79*t2 + Power(t2,2)) + 
               t1*(59 - 70*t2 + 11*Power(t2,2)) - 
               Power(t1,2)*(11 + 59*t2 + 22*Power(t2,2)) - 
               Power(s2,2)*(46 + 5*Power(t1,3) + 
                  Power(t1,2)*(221 - 40*t2) + 9*t2 + 37*Power(t2,2) + 
                  t1*(92 - 133*t2 - 11*Power(t2,2))) + 
               s2*(-55 - 6*Power(t1,4) + Power(t1,3)*(159 - 4*t2) + 
                  62*t2 - 7*Power(t2,2) + 
                  Power(t1,2)*(58 - 191*t2 - 7*Power(t2,2)) + 
                  t1*(57 + 68*t2 + 59*Power(t2,2)))) + 
            s1*(6*Power(t1,6) - Power(t1,7) + Power(s2,6)*(2 - 13*t2) - 
               4*t1*Power(-1 + t2,2) - 28*Power(-1 + t2,3) + 
               Power(t1,5)*(-5 - 29*t2 + Power(t2,2)) + 
               Power(t1,4)*(-134 + 89*t2 + 12*Power(t2,2)) + 
               Power(t1,3)*(80 + 164*t2 - 115*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(t1,2)*(-2 - 62*t2 + 36*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(s2,5)*(3 + Power(t1,2) + 20*t2 + 17*Power(t2,2) + 
                  7*t1*(-1 + 8*t2)) - 
               Power(s2,4)*(60 + 5*Power(t1,3) - 81*t2 - 
                  48*Power(t2,2) + 2*Power(t1,2)*(-7 + 47*t2) + 
                  t1*(7 + 135*t2 + 59*Power(t2,2))) + 
               Power(s2,3)*(-10 + 10*Power(t1,4) - 193*t2 + 
                  65*Power(t2,2) + 12*Power(t2,3) + 
                  Power(t1,3)*(-26 + 76*t2) + 
                  t1*(305 - 338*t2 - 141*Power(t2,2)) + 
                  Power(t1,2)*(8 + 314*t2 + 74*Power(t2,2))) + 
               s2*(5*Power(t1,6) + 4*Power(-1 + t2,2)*(-2 + 3*t2) + 
                  Power(t1,5)*(-23 + 4*t2) + 
                  Power(t1,3)*(453 - 354*t2 - 69*Power(t2,2)) + 
                  Power(t1,4)*(13 + 162*t2 + 5*Power(t2,2)) + 
                  t1*(74 - 42*t2 + 50*Power(t2,2) - 82*Power(t2,3)) + 
                  Power(t1,2)*
                   (-170 - 521*t2 + 295*Power(t2,2) + 18*Power(t2,3))) - 
               Power(s2,2)*(72 + 10*Power(t1,5) - 104*t2 + 
                  86*Power(t2,2) - 54*Power(t2,3) + 
                  Power(t1,4)*(-34 + 29*t2) + 
                  2*Power(t1,3)*(6 + 166*t2 + 19*Power(t2,2)) - 
                  6*Power(t1,2)*(-94 + 87*t2 + 25*Power(t2,2)) + 
                  t1*(-100 - 550*t2 + 245*Power(t2,2) + 27*Power(t2,3)))))\
)*T4(1 - s + s2 - t1))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(s - s1 + t2)*(-1 + s2 - t1 + t2)*
       Power(-1 + s - s*s1 + s1*s2 - s1*t1 + t2,2)) + 
    (8*(-21 + 31*s2 - Power(s2,2) - 4*Power(s2,3) + Power(s2,4) - 26*t1 + 
         2*s2*t1 + 23*Power(s2,2)*t1 - 10*Power(s2,3)*t1 + 
         2*Power(s2,4)*t1 - 5*Power(t1,2) - 35*s2*Power(t1,2) + 
         28*Power(s2,2)*Power(t1,2) - 8*Power(s2,3)*Power(t1,2) - 
         Power(s2,4)*Power(t1,2) + 16*Power(t1,3) - 30*s2*Power(t1,3) + 
         12*Power(s2,2)*Power(t1,3) + 4*Power(s2,3)*Power(t1,3) + 
         11*Power(t1,4) - 8*s2*Power(t1,4) - 6*Power(s2,2)*Power(t1,4) + 
         2*Power(t1,5) + 4*s2*Power(t1,5) - Power(t1,6) - 
         2*Power(s,5)*Power(t1 - t2,2)*(1 - s2 + t1 - t2) + 68*t2 - 
         50*s2*t2 - 33*Power(s2,2)*t2 + 7*Power(s2,3)*t2 + 
         5*Power(s2,4)*t2 - 2*Power(s2,5)*t2 + 32*t1*t2 + 87*s2*t1*t2 - 
         51*Power(s2,2)*t1*t2 - 16*Power(s2,3)*t1*t2 + 
         7*Power(s2,4)*t1*t2 + 2*Power(s2,5)*t1*t2 - 40*Power(t1,2)*t2 + 
         83*s2*Power(t1,2)*t2 + 9*Power(s2,2)*Power(t1,2)*t2 - 
         8*Power(s2,3)*Power(t1,2)*t2 - 8*Power(s2,4)*Power(t1,2)*t2 - 
         39*Power(t1,3)*t2 + 10*s2*Power(t1,3)*t2 + 
         2*Power(s2,2)*Power(t1,3)*t2 + 12*Power(s2,3)*Power(t1,3)*t2 - 
         8*Power(t1,4)*t2 + 2*s2*Power(t1,4)*t2 - 
         8*Power(s2,2)*Power(t1,4)*t2 - Power(t1,5)*t2 + 
         2*s2*Power(t1,5)*t2 - 76*Power(t2,2) - 2*s2*Power(t2,2) + 
         36*Power(s2,2)*Power(t2,2) + 10*Power(s2,3)*Power(t2,2) - 
         2*Power(s2,4)*Power(t2,2) + Power(s2,5)*Power(t2,2) - 
         Power(s2,6)*Power(t2,2) + 28*t1*Power(t2,2) - 
         127*s2*t1*Power(t2,2) - 2*Power(s2,2)*t1*Power(t2,2) + 
         12*Power(s2,3)*t1*Power(t2,2) - 4*Power(s2,4)*t1*Power(t2,2) + 
         4*Power(s2,5)*t1*Power(t2,2) + 73*Power(t1,2)*Power(t2,2) - 
         26*s2*Power(t1,2)*Power(t2,2) - 
         17*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         6*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         6*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         18*Power(t1,3)*Power(t2,2) + 6*s2*Power(t1,3)*Power(t2,2) - 
         4*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         4*Power(s2,3)*Power(t1,3)*Power(t2,2) + Power(t1,4)*Power(t2,2) + 
         s2*Power(t1,4)*Power(t2,2) - 
         Power(s2,2)*Power(t1,4)*Power(t2,2) + 30*Power(t2,3) + 
         28*s2*Power(t2,3) + 9*Power(s2,2)*Power(t2,3) - 
         9*Power(s2,3)*Power(t2,3) - Power(s2,4)*Power(t2,3) - 
         48*t1*Power(t2,3) + 29*s2*t1*Power(t2,3) + 
         17*Power(s2,2)*t1*Power(t2,3) - 28*Power(t1,2)*Power(t2,3) - 
         9*s2*Power(t1,2)*Power(t2,3) + 
         3*Power(s2,2)*Power(t1,2)*Power(t2,3) + Power(t1,3)*Power(t2,3) - 
         2*s2*Power(t1,3)*Power(t2,3) + Power(t2,4) - 5*s2*Power(t2,4) - 
         11*Power(s2,2)*Power(t2,4) + Power(s2,4)*Power(t2,4) + 
         14*t1*Power(t2,4) + 9*s2*t1*Power(t2,4) + 
         Power(s2,2)*t1*Power(t2,4) - 2*Power(s2,3)*t1*Power(t2,4) - 
         s2*Power(t1,2)*Power(t2,4) + 
         Power(s2,2)*Power(t1,2)*Power(t2,4) - 2*Power(t2,5) - 
         2*s2*Power(t2,5) + 2*Power(s1,4)*Power(s2 - t1,2)*
          (-1 + 2*s2 - 2*t1 + t2) + 
         Power(s1,3)*(s2 - t1)*
          (-Power(t1,4) + 4*Power(-1 + t2,2) + 
            Power(s2,3)*(-2 + t1 + t2) + Power(t1,3)*(-3 + 2*t2) + 
            Power(s2,2)*(-1 + t1 - 3*Power(t1,2) - 8*t2 + Power(t2,2)) + 
            3*t1*(5 - 6*t2 + Power(t2,2)) - 
            Power(t1,2)*(3 + 4*t2 + Power(t2,2)) + 
            s2*(-13 + 3*Power(t1,3) + Power(t1,2)*(4 - 3*t2) + 14*t2 - 
               Power(t2,2) + 4*t1*(1 + 3*t2))) + 
         Power(s,4)*(-5*Power(t1,4) + 
            2*Power(t1,3)*(2 + 2*s1 + 5*s2 + 7*t2) - 
            Power(t1,2)*(-6 + 7*s2 + 5*Power(s2,2) + 16*t2 + 28*s2*t2 + 
               12*Power(t2,2) + s1*(-11 + 5*s2 + 10*t2)) + 
            t2*(3 + 4*Power(s1,2) + 8*t2 - 8*Power(t2,2) + Power(t2,3) - 
               Power(s2,2)*(7 + 9*t2) + s2*(8 - 5*t2 - 8*Power(t2,2)) + 
               s1*(-11 + 3*Power(s2,2) + 5*t2 + s2*t2 - 2*Power(t2,2))) + 
            t1*(-7 - 4*Power(s1,2) - 14*t2 + 20*Power(t2,2) + 
               2*Power(t2,3) + 2*s2*t2*(6 + 13*t2) + 
               Power(s2,2)*(3 + 14*t2) + 
               s1*(15 + Power(s2,2) + 4*s2*(-2 + t2) - 16*t2 + 
                  8*Power(t2,2)))) + 
         Power(s1,2)*(Power(s2,6) + 6*Power(t1,5) + 
            Power(t1,4)*(18 - 19*t2) + 2*Power(-1 + t2,3) + 
            2*t1*Power(-1 + t2,2)*(-8 + 3*t2) + 
            Power(s2,5)*(-4 - 5*t1 + 5*t2) + 
            Power(t1,3)*t2*(-31 + 17*t2) + 
            Power(s2,4)*(-1 + 10*Power(t1,2) + t1*(25 - 19*t2) - 9*t2 + 
               3*Power(t2,2)) - 
            4*Power(t1,2)*(4 - 8*t2 + 3*Power(t2,2) + Power(t2,3)) - 
            Power(s2,3)*(-35 + 10*Power(t1,3) + 
               Power(t1,2)*(57 - 27*t2) + 18*t2 + 2*Power(t2,2) + 
               Power(t2,3) + t1*(24 - 54*t2 + 8*Power(t2,2))) + 
            Power(s2,2)*(-19 + 5*Power(t1,4) + 
               Power(t1,3)*(61 - 17*t2) + 39*t2 - 17*Power(t2,2) - 
               3*Power(t2,3) + 
               Power(t1,2)*(69 - 100*t2 + 7*Power(t2,2)) + 
               t1*(-66 - 3*t2 + 25*Power(t2,2) + 2*Power(t2,3))) - 
            s2*(Power(t1,5) + Power(t1,4)*(31 - 4*t2) + 
               2*Power(-1 + t2,2)*(-7 + 2*t2) + 
               2*Power(t1,3)*(31 - 37*t2 + Power(t2,2)) + 
               Power(t1,2)*(-31 - 52*t2 + 40*Power(t2,2) + Power(t2,3)) - 
               t1*(35 - 71*t2 + 29*Power(t2,2) + 7*Power(t2,3)))) + 
         s1*(8*Power(t1,5) + Power(t1,6) + 2*Power(s2,6)*t2 - 
            Power(-1 + t2,3)*(-5 + 3*t2) - 
            Power(t1,4)*(-21 + 28*t2 + Power(t2,2)) + 
            t1*Power(-1 + t2,2)*(-40 + 17*t2 + 5*Power(t2,2)) + 
            2*Power(t1,3)*(8 - 38*t2 + 23*Power(t2,2)) - 
            Power(t1,2)*(25 + 21*t2 - 75*Power(t2,2) + 29*Power(t2,3)) - 
            Power(s2,5)*(2 + 5*t2 + Power(t2,2) + t1*(2 + 9*t2)) + 
            Power(s2,4)*(8 - 12*t2 - 9*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,2)*(9 + 16*t2) + t1*(15 + 22*t2 + 3*Power(t2,2))) \
- Power(s2,3)*(2*Power(t1,3)*(8 + 7*t2) + 
               Power(t1,2)*(41 + 36*t2 + 3*Power(t2,2)) + 
               2*(-1 - 21*t2 + 9*Power(t2,2) + 6*Power(t2,3)) - 
               t1*(-48 + 68*t2 + 29*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s2,2)*(-64 + 67*t2 + 13*Power(t2,2) - 13*Power(t2,3) - 
               3*Power(t2,4) + 2*Power(t1,4)*(7 + 3*t2) + 
               Power(t1,3)*(53 + 26*t2 + Power(t2,2)) - 
               Power(t1,2)*(-93 + 128*t2 + 32*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(21 - 183*t2 + 101*Power(t2,2) + 19*Power(t2,3))) - 
            s2*(Power(t1,5)*(6 + t2) + Power(t1,4)*(33 + 7*t2) + 
               Power(-1 + t2,2)*(-41 + 17*t2 + 6*Power(t2,2)) - 
               Power(t1,3)*(-74 + 100*t2 + 13*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,2)*(39 - 217*t2 + 129*Power(t2,2) + 
                  7*Power(t2,3)) + 
               t1*(-89 + 46*t2 + 88*Power(t2,2) - 42*Power(t2,3) - 
                  3*Power(t2,4)))) + 
         Power(s,3)*(11 + 7*t1 + 10*Power(t1,3) + 14*Power(t1,4) - 
            4*Power(t1,5) - 9*t2 - 32*t1*t2 - 29*Power(t1,2)*t2 - 
            39*Power(t1,3)*t2 + 10*Power(t1,4)*t2 + 16*Power(t2,2) + 
            18*t1*Power(t2,2) + 33*Power(t1,2)*Power(t2,2) - 
            6*Power(t1,3)*Power(t2,2) + Power(t2,3) - 5*t1*Power(t2,3) - 
            2*Power(t1,2)*Power(t2,3) - 3*Power(t2,4) + 
            2*t1*Power(t2,4) + Power(s1,3)*(6*t1 - 2*(2 + t2)) + 
            Power(s2,3)*(4*Power(t1,2) + 2*t2*(9 + 8*t2) - 
               t1*(5 + 18*t2)) - 
            Power(s1,2)*(-18 + Power(s2,3) + 3*Power(t1,3) + 
               Power(t1,2)*(26 - 7*t2) - 12*t2 + 5*Power(s2,2)*t2 + 
               3*Power(t2,2) - Power(t2,3) + 
               s2*(5 - 4*Power(t1,2) + t1*(-27 + t2) + 9*t2 + 
                  3*Power(t2,2)) + t1*(37 - 29*t2 + 5*Power(t2,2))) - 
            Power(s2,2)*(1 + 12*Power(t1,3) + 14*t2 - 17*Power(t2,2) - 
               12*Power(t2,3) - 2*Power(t1,2)*(12 + 23*t2) + 
               t1*(2 + 61*t2 + 46*Power(t2,2))) + 
            s2*(-6 + 12*Power(t1,4) - t2 - 13*Power(t2,2) + 
               16*Power(t2,3) - 4*Power(t2,4) - 
               Power(t1,3)*(33 + 38*t2) + 
               Power(t1,2)*(-8 + 82*t2 + 36*Power(t2,2)) + 
               t1*(2 + 55*t2 - 65*Power(t2,2) - 6*Power(t2,3))) + 
            s1*(-27 + 9*Power(t1,4) + Power(t1,3)*(6 - 21*t2) + 
               Power(s2,3)*(3 - 2*t1 - 11*t2) + 12*t2 - 36*Power(t2,2) + 
               13*Power(t2,3) + 
               Power(t1,2)*(1 + 21*t2 + 15*Power(t2,2)) + 
               t1*(11 + 49*t2 - 40*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*(-5 + 13*Power(t1,2) + 18*t2 - 
                  2*Power(t2,2) + 3*t1*(1 + t2)) - 
               s2*(-17 + 20*Power(t1,3) + Power(t1,2)*(12 - 29*t2) + 
                  9*t2 - 8*Power(t2,2) - 9*Power(t2,3) + 
                  2*t1*(5 + 12*t2 + 9*Power(t2,2))))) + 
         Power(s,2)*(-22 + 2*Power(s1,4)*(s2 - t1) - 14*t1 - 
            41*Power(t1,2) - 18*Power(t1,3) - 2*Power(t1,4) + 
            10*Power(t1,5) - Power(t1,6) + 70*t2 + 120*t1*t2 + 
            45*Power(t1,2)*t2 - 7*Power(t1,3)*t2 - 22*Power(t1,4)*t2 + 
            2*Power(t1,5)*t2 - 85*Power(t2,2) - 89*t1*Power(t2,2) + 
            Power(t1,2)*Power(t2,2) + 10*Power(t1,3)*Power(t2,2) + 
            38*Power(t2,3) + 9*t1*Power(t2,3) + 
            6*Power(t1,2)*Power(t2,3) - 2*Power(t1,3)*Power(t2,3) - 
            Power(t2,4) - 4*t1*Power(t2,4) + Power(t1,2)*Power(t2,4) - 
            Power(s2,4)*(Power(t1,2) - 2*t1*(1 + 5*t2) + 
               t2*(15 + 14*t2)) + 
            Power(s2,3)*(9 + 4*Power(t1,3) + 6*t2 - 10*Power(t2,2) - 
               8*Power(t2,3) - 16*Power(t1,2)*(1 + 2*t2) + 
               t1*(3 + 70*t2 + 42*Power(t2,2))) + 
            Power(s2,2)*(-20 - 6*Power(t1,4) + 37*t2 + 14*Power(t2,2) - 
               9*Power(t2,3) + 6*Power(t2,4) + 36*Power(t1,3)*(1 + t2) - 
               Power(t1,2)*(8 + 117*t2 + 42*Power(t2,2)) + 
               t1*(-18 - 53*t2 + 54*Power(t2,2) + 6*Power(t2,3))) + 
            s2*(31 + 4*Power(t1,5) - 102*t2 + 36*Power(t2,2) + 
               7*Power(t2,3) + 6*Power(t2,4) - 16*Power(t1,4)*(2 + t2) + 
               7*Power(t1,3)*(1 + 12*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(27 + 54*t2 - 54*Power(t2,2) + 
                  4*Power(t2,3)) - 
               t1*(-45 + 61*t2 + 14*Power(t2,2) + 4*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,3)*(Power(t1,3) + Power(t1,2)*(25 - 2*t2) + 
               Power(s2,2)*(2 + t1 + t2) + 
               t1*(10 - 15*t2 + Power(t2,2)) + 
               2*(-3 + 2*t2 + Power(t2,2)) + 
               s2*(-2*Power(t1,2) + t1*(-27 + t2) + t2*(3 + t2))) + 
            Power(s1,2)*(14 + 3*Power(s2,4) - 6*Power(t1,4) - 22*t2 + 
               12*Power(t2,2) - 4*Power(t2,3) + 
               7*Power(t1,3)*(-3 + 2*t2) + 
               Power(s2,3)*(-4 - 5*t1 + 15*t2) + 
               Power(t1,2)*(-33 + t2 - 10*Power(t2,2)) + 
               2*t1*(31 - 24*t2 + 12*Power(t2,2) + Power(t2,3)) - 
               Power(s2,2)*(-17 + 5*Power(t1,2) + 4*t2 - 
                  9*Power(t2,2) + t1*(16 + 17*t2)) + 
               s2*(-30 + 13*Power(t1,3) + Power(t1,2)*(41 - 12*t2) - 
                  9*t2 + 2*Power(t2,2) - 3*Power(t2,3) + 
                  t1*(15 + 3*t2 + 2*Power(t2,2)))) + 
            s1*(16 + 6*Power(t1,5) - 45*t2 + 47*Power(t2,2) - 
               15*Power(t2,3) - 3*Power(t2,4) - 
               Power(t1,4)*(13 + 12*t2) + Power(s2,4)*(-5 + t1 + 15*t2) + 
               Power(t1,2)*(3 + 97*t2 - 84*Power(t2,2)) + 
               6*Power(t1,3)*(-5 + 12*t2 + Power(t2,2)) + 
               t1*(-70 + 20*t2 - 34*Power(t2,2) + 28*Power(t2,3)) - 
               Power(s2,3)*(1 + 9*Power(t1,2) + 38*t2 + 
                  t1*(-17 + 27*t2)) + 
               Power(s2,2)*(1 + 21*Power(t1,3) + 7*t2 - 37*Power(t2,2) - 
                  15*Power(t2,3) - Power(t1,2)*(32 + 3*t2) + 
                  5*t1*(-4 + 27*t2 + 3*Power(t2,2))) + 
               s2*(-7 - 19*Power(t1,4) + 77*t2 + 23*Power(t2,2) - 
                  37*Power(t2,3) + 3*Power(t1,3)*(11 + 9*t2) + 
                  Power(t1,2)*(51 - 169*t2 - 21*Power(t2,2)) + 
                  t1*(16 - 138*t2 + 137*Power(t2,2) + 13*Power(t2,3))))) + 
         s*(32 + 43*t1 + 43*Power(t1,2) - 13*Power(t1,3) - 
            19*Power(t1,4) - 8*Power(t1,5) + 2*Power(t1,6) - 114*t2 - 
            104*t1*t2 + 24*Power(t1,2)*t2 + 64*Power(t1,3)*t2 + 
            13*Power(t1,4)*t2 - 3*Power(t1,5)*t2 + 140*Power(t2,2) + 
            47*t1*Power(t2,2) - 89*Power(t1,2)*Power(t2,2) - 
            22*Power(t1,3)*Power(t2,2) - Power(t1,4)*Power(t2,2) - 
            64*Power(t2,3) + 24*t1*Power(t2,3) + 
            25*Power(t1,2)*Power(t2,3) + 3*Power(t1,3)*Power(t2,3) + 
            4*Power(t2,4) - 10*t1*Power(t2,4) - Power(t1,2)*Power(t2,4) + 
            2*Power(t2,5) - 2*Power(s1,4)*(s2 - t1)*
             (-1 + 3*s2 - 3*t1 + t2) + 2*Power(s2,5)*t2*(2 - t1 + 3*t2) - 
            Power(s1,3)*(-2*Power(t1,4) + 2*Power(-1 + t2,2) + 
               2*Power(s2,3)*(t1 + t2) + Power(t1,3)*(-22 + 4*t2) - 
               Power(s2,2)*(3 + 22*t1 + 6*Power(t1,2) + 7*t2 - 
                  2*Power(t2,2)) + 
               Power(t1,2)*(-15 + 9*t2 - 2*Power(t2,2)) + 
               t1*(21 - 22*t2 + Power(t2,2)) + 
               s2*(-19 + 6*Power(t1,3) + Power(t1,2)*(44 - 6*t2) - 
                  2*t1*(-9 + t2) + 18*t2 + Power(t2,2))) + 
            Power(s2,4)*(-2 + 2*t2 - Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(2 + 8*t2) - t1*(4 + 27*t2 + 20*Power(t2,2))) - 
            Power(s2,3)*(2 + 21*t2 + 7*Power(t2,2) - 2*Power(t2,3) + 
               4*Power(t2,4) + 4*Power(t1,3)*(2 + 3*t2) - 
               4*Power(t1,2)*(5 + 15*t2 + 6*Power(t2,2)) + 
               t1*(-16 + 5*t2 + 5*Power(t2,2) + 2*Power(t2,3))) - 
            s2*(59 - 132*t2 + 53*Power(t2,2) + 32*Power(t2,3) - 
               12*Power(t2,4) + 2*Power(t1,5)*(4 + t2) - 
               2*Power(t1,4)*(14 + 12*t2 + Power(t2,2)) + 
               Power(t1,3)*(-50 + 27*t2 + 5*Power(t2,2) - 
                  2*Power(t2,3)) + 
               t1*(51 + 74*t2 - 150*Power(t2,2) + 32*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(t1,2)*(-30 + 167*t2 - 57*Power(t2,2) + 
                  14*Power(t2,3) + 2*Power(t2,4))) + 
            Power(s2,2)*(23 + 12*t2 - 31*Power(t2,2) + Power(t2,3) - 
               3*Power(t2,4) + 4*Power(t1,4)*(3 + 2*t2) - 
               2*Power(t1,3)*(18 + 29*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(-45 + 17*t2 + 12*Power(t2,2) - 
                  2*Power(t2,3)) + 
               t1*(-15 + 124*t2 - 28*Power(t2,2) + 9*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,2)*(-3*Power(s2,5) - 3*Power(t1,5) + 
               Power(s2,4)*(8 + 10*t1 - 15*t2) + 
               (-13 + t2)*Power(-1 + t2,2) + 7*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(20 - 43*t2 - 5*Power(t2,2)) - 
               Power(s2,3)*(11 + 9*Power(t1,2) + t1*(32 - 37*t2) - 
                  18*t2 + 9*Power(t2,2)) - 
               8*t1*(-1 + t2 - Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(51 - 92*t2 + 44*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(-14 - 3*Power(t1,3) + 
                  Power(t1,2)*(47 - 22*t2) + 12*t2 + 3*Power(t2,2) + 
                  3*Power(t2,3) + t1*(52 - 87*t2 + 11*Power(t2,2))) + 
               s2*(-3 + 8*Power(t1,4) - 3*t2 - Power(t2,2) + 
                  7*Power(t2,3) - Power(t1,3)*(30 + 7*t2) + 
                  Power(t1,2)*(-61 + 112*t2 + 3*Power(t2,2)) - 
                  t1*(41 - 88*t2 + 51*Power(t2,2) + 4*Power(t2,3)))) + 
            s1*(Power(t1,6) + Power(s2,5)*(2 - 9*t2) + 
               6*Power(-1 + t2,2)*t2*(1 + t2) - Power(t1,5)*(7 + t2) - 
               Power(t1,4)*(9 - 35*t2 + Power(t2,2)) + 
               Power(t1,3)*(25 + 11*t2 - 40*Power(t2,2) + Power(t2,3)) + 
               3*Power(t1,2)*
                (6 - 33*t2 + 16*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(58 - 160*t2 + 151*Power(t2,2) - 46*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,4)*(5 + Power(t1,2) + 25*t2 + 2*Power(t2,2) + 
                  t1*(-9 + 29*t2)) + 
               Power(s2,3)*(-31 - 4*Power(t1,3) + 
                  Power(t1,2)*(22 - 32*t2) + 16*t2 + 33*Power(t2,2) + 
                  11*Power(t2,3) + t1*(1 - 114*t2 - 8*Power(t2,2))) + 
               Power(s2,2)*(70 + 6*Power(t1,4) - 146*t2 + 
                  22*Power(t2,2) + 36*Power(t2,3) + 
                  4*Power(t1,3)*(-8 + 3*t2) + 
                  Power(t1,2)*(-26 + 188*t2 + 9*Power(t2,2)) + 
                  t1*(70 + 9*t2 - 123*Power(t2,2) - 17*Power(t2,3))) + 
               s2*(-18 - 4*Power(t1,5) + 65*t2 - 78*Power(t2,2) + 
                  25*Power(t2,3) + 6*Power(t2,4) + Power(t1,4)*(24 + t2) + 
                  Power(t1,3)*(29 - 134*t2 - 2*Power(t2,2)) + 
                  Power(t1,2)*
                   (-64 - 36*t2 + 130*Power(t2,2) + 5*Power(t2,3)) - 
                  t1*(97 - 268*t2 + 89*Power(t2,2) + 46*Power(t2,3))))))*
       T5(1 - s2 + t1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(s - s1 + t2)*(-1 + s2 - t1 + t2)*
       Power(-1 + s - s*s1 + s1*s2 - s1*t1 + t2,2)));
   return a;
};
