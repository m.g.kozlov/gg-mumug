#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m123_5_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(-2*Power(s,10)*s1*t2*(t1 + t2) + 
         Power(s,9)*(-2*t2*(1 + t2) + 
            2*Power(s1,2)*(t2*(-1 - s2 + 6*t2) + t1*(-1 + 7*t2)) + 
            s1*(-Power(t1,3) + Power(t1,2)*t2 + t1*(25 + s2 - 9*t2)*t2 + 
               t2*(2 + s2*(-14 + t2) + 13*t2 - 11*Power(t2,2)))) + 
         Power(s,8)*(2*t2*(7 + 2*t2 - 5*Power(t2,2)) - 
            2*Power(s1,3)*(s2 - 6*s2*t2 + 5*t2*(-1 + 3*t2) + 
               3*t1*(-2 + 7*t2)) + 
            Power(s1,2)*(2 + 8*Power(t1,3) + Power(t1,2)*(4 - 11*t2) + 
               5*t2 + Power(s2,2)*t2 - 72*Power(t2,2) + 59*Power(t2,3) + 
               s2*(-14 + t1 - 3*Power(t1,2) + 106*t2 + 3*t1*t2 - 
                  7*Power(t2,2)) + t1*(23 - 171*t2 + 50*Power(t2,2))) + 
            s1*(-2 + Power(t1,3)*(7 - 11*t2) + 
               (33 + 73*s2 + 14*Power(s2,2))*t2 - 
               (38 + 100*s2 + Power(s2,2))*Power(t2,2) + 
               (65 + 9*s2)*Power(t2,3) - 25*Power(t2,4) + 
               Power(t1,2)*(-3 + (17 + s2)*t2 + 5*Power(t2,2)) + 
               t1*t2*(-143 + 2*Power(s2,2) + 161*t2 - 21*Power(t2,2) + 
                  s2*(-31 + 4*t2)))) + 
         Power(s,7)*(2*t2*(-19 + 13*t2 + 22*Power(t2,2) - 
               10*Power(t2,3)) + 
            10*Power(s1,4)*(s2 - 3*s2*t2 + 2*t2*(-1 + 2*t2) + 
               t1*(-3 + 7*t2)) + 
            Power(s1,3)*(-4 - 28*Power(t1,3) - 47*t2 + 165*Power(t2,2) - 
               130*Power(t2,3) + Power(s2,2)*(1 - 3*t1 + 3*t2) + 
               Power(t1,2)*(-28 + 53*t2) + 
               t1*(-132 + 497*t2 - 111*Power(t2,2)) + 
               s2*(89 + 21*Power(t1,2) + t1*(10 - 55*t2) - 325*t2 + 
                  16*Power(t2,2))) + 
            s1*(12 + (-303 - 122*s2 - 124*Power(s2,2) + 14*Power(s2,3))*
                t2 + (269 + 479*s2 + 85*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,2) - 
               (207 + 306*s2 + 8*Power(s2,2))*Power(t2,3) + 
               2*(67 + 15*s2)*Power(t2,4) - 30*Power(t2,5) + 
               Power(t1,3)*(-19 + 44*t2 - 40*Power(t2,2)) + 
               Power(t1,2)*(18 + 4*(-57 + 13*s2)*t2 - 
                  (-136 + s2)*Power(t2,2) + 10*Power(t2,3)) + 
               t1*(-3 + (553 + 199*s2 - 45*Power(s2,2))*t2 + 
                  2*(-451 - 96*s2 + 6*Power(s2,2))*Power(t2,2) + 
                  (470 + 11*s2)*Power(t2,3) - 40*Power(t2,4))) + 
            Power(s1,2)*(35 - 200*t2 + 2*Power(s2,3)*t2 + 
               262*Power(t2,2) - 281*Power(t2,3) + 118*Power(t2,4) + 
               7*Power(t1,3)*(-7 + 10*t2) + 
               Power(t1,2)*(21 - 113*t2 - 53*Power(t2,2)) + 
               Power(s2,2)*(14 + t1*(2 - 23*t2) - 121*t2 + 
                  15*Power(t2,2)) + 
               t1*(-114 + 893*t2 - 891*Power(t2,2) + 85*Power(t2,3)) + 
               s2*(59 - 499*t2 + 585*Power(t2,2) - 28*Power(t2,3) - 
                  2*Power(t1,2)*(-8 + 9*t2) + 
                  t1*(-36 + 231*t2 + 14*Power(t2,2))))) + 
         Power(s,6)*(-2*t2*(-26 + 51*t2 + 19*Power(t2,2) - 
               48*Power(t2,3) + 10*Power(t2,4)) + 
            10*Power(s1,5)*(t1*(4 - 7*t2) + (2 - 3*t2)*t2 + 
               s2*(-2 + 4*t2)) - 
            Power(s1,4)*(8 + Power(s2,3) - 56*Power(t1,3) - 106*t2 + 
               200*Power(t2,2) - 150*Power(t2,3) + 
               11*Power(t1,2)*(-8 + 13*t2) + 
               Power(s2,2)*(-7 - 18*t1 + 38*t2) + 
               t1*(-319 + 795*t2 - 120*Power(t2,2)) + 
               s2*(225 + 63*Power(t1,2) + t1*(81 - 211*t2) - 529*t2 + 
                  10*Power(t2,2))) + 
            s1*(-27 + (1004 - 10*s2 + 391*Power(s2,2) - 60*Power(s2,3))*
                t2 + (-1410 - 856*s2 - 602*Power(s2,2) + 
                  55*Power(s2,3))*Power(t2,2) + 
               (783 + 1303*s2 + 222*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,3) - 
               (415 + 512*s2 + 25*Power(s2,2))*Power(t2,4) + 
               2*(73 + 25*s2)*Power(t2,5) - 20*Power(t2,6) + 
               Power(t1,3)*(23 - 50*t2 + 112*Power(t2,2) - 
                  70*Power(t2,3)) + 
               Power(t1,2)*(-39 + (894 - 279*s2)*t2 + 
                  (-1266 + 275*s2)*Power(t2,2) + 
                  (410 - 20*s2)*Power(t2,3) + 10*Power(t2,4)) + 
               t1*(15 + (-1534 - 447*s2 + 165*Power(s2,2))*t2 + 
                  (2968 + 1110*s2 - 218*Power(s2,2))*Power(t2,2) + 
                  (-2361 - 533*s2 + 30*Power(s2,2))*Power(t2,3) + 
                  (776 + 30*s2)*Power(t2,4) - 60*Power(t2,5))) - 
            Power(s1,3)*(141 - 522*t2 + 641*Power(t2,2) - 
               492*Power(t2,3) + 224*Power(t2,4) + 
               Power(s2,3)*(-2 + 22*t2) + 4*Power(t1,3)*(-35 + 46*t2) + 
               Power(t1,2)*(103 - 298*t2 - 219*Power(t2,2)) + 
               2*Power(s2,2)*
                (54 + t1*(6 - 50*t2) - 205*t2 + 20*Power(t2,2)) + 
               2*t1*(-249 + 1137*t2 - 1028*Power(t2,2) + 
                  52*Power(t2,3)) + 
               2*s2*(Power(t1,2)*(40 - 33*t2) + 
                  t1*(-107 + 337*t2 + 93*Power(t2,2)) + 
                  2*(65 - 338*t2 + 339*Power(t2,2) + Power(t2,3)))) - 
            Power(s1,2)*(257 - 1269*t2 + 1353*Power(t2,2) - 
               976*Power(t2,3) + 456*Power(t2,4) - 122*Power(t2,5) + 
               Power(t1,3)*(-93 + 226*t2 - 218*Power(t2,2)) + 
               Power(s2,3)*(-14 + 83*t2 + 7*Power(t2,2)) + 
               3*Power(t1,2)*(84 - 461*t2 + 269*Power(t2,2) + 
                  36*Power(t2,3)) + 
               t1*(-421 + 2870*t2 - 4156*Power(t2,2) + 
                  2113*Power(t2,3) - 132*Power(t2,4)) + 
               Power(s2,2)*(110 - 828*t2 + 626*Power(t2,2) - 
                  71*Power(t2,3) + t1*(43 - 310*t2 + 102*Power(t2,2))) + 
               s2*(66 - 932*t2 + 2314*Power(t2,2) - 1393*Power(t2,3) + 
                  90*Power(t2,4) + 
                  Power(t1,2)*(-29 + 215*t2 + 25*Power(t2,2)) + 
                  t1*(-193 + 1630*t2 - 1286*Power(t2,2) + 5*Power(t2,3))))\
) - Power(-1 + s1,2)*s1*(-1 + t2)*
          (s1 + Power(s1,2) - 2*s1*t2 + (-1 + t2)*t2)*
          (-10 + 56*t2 - 22*s2*t2 - 73*Power(t2,2) + 7*s2*Power(t2,2) - 
            31*Power(s2,2)*Power(t2,2) + 4*Power(s2,3)*Power(t2,2) + 
            15*Power(t2,3) + 52*s2*Power(t2,3) + 
            34*Power(s2,2)*Power(t2,3) - 7*Power(s2,3)*Power(t2,3) + 
            15*Power(t2,4) - 37*s2*Power(t2,4) - 
            6*Power(s2,2)*Power(t2,4) - 2*Power(s2,3)*Power(t2,4) - 
            3*Power(t2,5) + 3*Power(s2,2)*Power(t2,5) + 
            Power(s1,4)*Power(s2 - t1,2)*(-4 + 5*s2 - 5*t1 + 4*t2) + 
            Power(t1,3)*(10 - 14*t2 + 3*Power(t2,2) + 6*Power(t2,3)) + 
            Power(t1,2)*(-30 + (84 - 22*s2)*t2 + 
               (-145 + 46*s2)*Power(t2,2) + (127 - 45*s2)*Power(t2,3) + 
               6*(-6 + s2)*Power(t2,4)) + 
            t1*(30 + 2*(-63 + 22*s2)*t2 + 
               (183 + 15*s2 - 9*Power(s2,2))*Power(t2,2) + 
               2*(-51 - 53*s2 + 13*Power(s2,2))*Power(t2,3) + 
               (9 + 53*s2 - 2*Power(s2,2))*Power(t2,4) - 
               6*(-1 + s2)*Power(t2,5)) - 
            Power(s1,3)*(4*Power(-1 + t2,2) + t1*Power(-1 + t2,2) - 
               2*Power(t1,3)*(2 + 3*t2) + Power(s2,3)*(1 + 9*t2) + 
               3*Power(t1,2)*(3 - 5*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(14 - 25*t2 + 11*Power(t2,2) - 
                  6*t1*(1 + 4*t2)) + 
               s2*(-9*Power(-1 + t2,2) + 3*Power(t1,2)*(3 + 7*t2) + 
                  t1*(-19 + 32*t2 - 13*Power(t2,2)))) + 
            Power(s1,2)*(-1 + t2)*
             (-7 + Power(s2,3)*(-4 + t2) + 6*t2 + Power(t2,2) + 
               Power(t1,3)*(-7 + 5*t2) + 
               Power(t1,2)*(73 - 56*t2 - 4*Power(t2,2)) + 
               Power(s2,2)*(31 + t1*(9 - 5*t2) - 31*t2 + 
                  13*Power(t2,2)) + t1*(-27 + 11*t2 + 16*Power(t2,2)) - 
               s2*(Power(t1,2)*(-6 + 5*t2) + 
                  9*(-5 + 3*t2 + 2*Power(t2,2)) + 
                  t1*(119 - 101*t2 + 8*Power(t2,2)))) + 
            s1*(2*Power(-1 + t2,2)*(-13 - 8*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*t2*(-8 + 13*t2 + 5*Power(t2,2)) - 
               3*t1*Power(-1 + t2,2)*(-12 + 16*t2 + 7*Power(t2,2)) - 
               2*Power(t1,3)*
                (8 - 10*t2 + 4*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(6 + 128*t2 - 217*Power(t2,2) + 
                  77*Power(t2,3) + 6*Power(t2,4)) + 
               Power(s2,2)*t2*
                (62 - 82*t2 + 29*Power(t2,2) - 9*Power(t2,3) - 
                  2*t1*(-9 + 23*t2 + Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(22 + 82*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*
                   (22 - 40*t2 + 43*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(-44 - 134*t2 + 307*Power(t2,2) - 138*Power(t2,3) + 
                     9*Power(t2,4))))) + 
         Power(s,4)*(-2*t2*(-7 + 49*t2 - 63*Power(t2,2) - 
               28*Power(t2,3) + 68*Power(t2,4) - 22*Power(t2,5) + 
               Power(t2,6)) + 
            2*Power(s1,7)*(6*t1 + t2 - 7*t1*t2 - Power(t2,2) + 
               s2*(-5 + 6*t2)) + 
            s1*(-16 + (2039 - 581*s2 + 496*Power(s2,2) - 
                  80*Power(s2,3))*t2 + 
               (-5267 + 410*s2 - 2125*Power(s2,2) + 342*Power(s2,3))*
                Power(t2,2) + 
               (5115 + 2095*s2 + 2622*Power(s2,2) - 326*Power(s2,3))*
                Power(t2,3) + 
               (-2498 - 3128*s2 - 1243*Power(s2,2) + 34*Power(s2,3))*
                Power(t2,4) + 
               2*(388 + 759*s2 + 143*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,5) - 
               (213 + 276*s2 + 35*Power(s2,2))*Power(t2,6) + 
               (29 + 21*s2)*Power(t2,7) - Power(t2,8) + 
               Power(t1,3)*(-19 + 95*t2 - 178*Power(t2,2) + 
                  20*Power(t2,3) + 107*Power(t2,4) - 31*Power(t2,5)) + 
               Power(t1,2)*(15 + (2007 - 725*s2)*t2 + 
                  3*(-2149 + 724*s2)*Power(t2,2) + 
                  (7284 - 2066*s2)*Power(t2,3) + 
                  (-3321 + 658*s2)*Power(t2,4) + 
                  (505 - 55*s2)*Power(t2,5) + Power(t2,6)) + 
               t1*(6 + (-3533 + 259*s2 + 180*Power(s2,2))*t2 + 
                  (9792 + 1936*s2 - 944*Power(s2,2))*Power(t2,2) + 
                  (-11095 - 4881*s2 + 1194*Power(s2,2))*Power(t2,3) + 
                  (6857 + 3346*s2 - 424*Power(s2,2))*Power(t2,4) + 
                  (-2390 - 801*s2 + 30*Power(s2,2))*Power(t2,5) + 
                  (401 + 56*s2)*Power(t2,6) - 29*Power(t2,7))) + 
            Power(s1,6)*(-38 - 10*Power(s2,3) + 56*Power(t1,3) + 
               Power(t1,2)*(180 - 241*t2) + 
               Power(s2,2)*(94 + 60*t1 - 155*t2) + 77*t2 - 
               48*Power(t2,2) + 31*Power(t2,3) + 
               t1*(309 - 425*t2 - 6*Power(t2,2)) + 
               s2*(-212 - 105*Power(t1,2) + 268*t2 + 29*Power(t2,2) + 
                  t1*(-305 + 449*t2))) - 
            Power(s1,5)*(194 - 621*t2 + 600*Power(t2,2) - 
               209*Power(t2,3) + 109*Power(t2,4) + 
               10*Power(t1,3)*(-15 + 17*t2) + 
               Power(s2,3)*(-61 + 145*t2) + 
               Power(t1,2)*(413 - 325*t2 - 575*Power(t2,2)) - 
               t1*(706 - 2363*t2 + 1737*Power(t2,2) + 155*Power(t2,3)) + 
               Power(s2,2)*(452 - 746*t2 - 71*Power(t2,2) - 
                  5*t1*(-31 + 73*t2)) + 
               s2*(382 - 1379*t2 + 932*Power(t2,2) + 203*Power(t2,3) + 
                  Power(t1,2)*(61 + 30*t2) + 
                  t1*(-828 + 899*t2 + 840*Power(t2,2)))) + 
            Power(s1,4)*(-716 + 2150*t2 - 2564*Power(t2,2) + 
               1646*Power(t2,3) - 376*Power(t2,4) + 147*Power(t2,5) + 
               Power(s2,3)*(153 - 403*t2 + 172*Power(t2,2)) + 
               Power(t1,3)*(107 - 481*t2 + 500*Power(t2,2)) - 
               Power(t1,2)*(1733 - 4927*t2 + 2751*Power(t2,2) + 
                  634*Power(t2,3)) + 
               t1*(1411 - 5339*t2 + 7231*Power(t2,2) - 
                  3565*Power(t2,3) - 92*Power(t2,4)) + 
               Power(s2,2)*(-1095 + 3614*t2 - 2803*Power(t2,2) + 
                  342*Power(t2,3) + 
                  t1*(-509 + 1405*t2 - 750*Power(t2,2))) + 
               s2*(-281 + 2126*t2 - 4075*Power(t2,2) + 
                  1982*Power(t2,3) + 137*Power(t2,4) + 
                  Power(t1,2)*(311 - 573*t2 + 37*Power(t2,2)) + 
                  t1*(2415 - 7755*t2 + 5273*Power(t2,2) + 
                     413*Power(t2,3)))) + 
            Power(s1,3)*(-1047 + 4398*t2 - 5943*Power(t2,2) + 
               3932*Power(t2,3) - 1959*Power(t2,4) + 345*Power(t2,5) - 
               85*Power(t2,6) + 
               Power(t1,3)*(21 - 174*t2 + 539*Power(t2,2) - 
                  616*Power(t2,3)) + 
               Power(s2,3)*(123 - 548*t2 + 598*Power(t2,2) + 
                  57*Power(t2,3)) + 
               Power(t1,2)*(-1931 + 8838*t2 - 11799*Power(t2,2) + 
                  4255*Power(t2,3) + 374*Power(t2,4)) + 
               t1*(2081 - 9330*t2 + 14467*Power(t2,2) - 
                  10568*Power(t2,3) + 3867*Power(t2,4) - 183*Power(t2,5)\
) + Power(s2,2)*(-827 + 4398*t2 - 6755*Power(t2,2) + 3152*Power(t2,3) - 
                  422*Power(t2,4) + 
                  t1*(-383 + 1950*t2 - 2620*Power(t2,2) + 
                     487*Power(t2,3))) + 
               s2*(-242 + 2100*t2 - 5562*Power(t2,2) + 
                  6224*Power(t2,3) - 2338*Power(t2,4) + 
                  131*Power(t2,5) + 
                  Power(t1,2)*
                   (471 - 1844*t2 + 1970*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(1674 - 9845*t2 + 15580*Power(t2,2) - 
                     7149*Power(t2,3) + 160*Power(t2,4)))) + 
            Power(s1,2)*(-1034 + 4761*t2 - 8052*Power(t2,2) + 
               6221*Power(t2,3) - 2626*Power(t2,4) + 1024*Power(t2,5) - 
               158*Power(t2,6) + 19*Power(t2,7) + 
               Power(s2,3)*(54 - 411*t2 + 703*Power(t2,2) - 
                  296*Power(t2,3) - 98*Power(t2,4)) + 
               Power(t1,3)*(-42 + 109*t2 + 67*Power(t2,2) - 
                  328*Power(t2,3) + 268*Power(t2,4)) - 
               Power(t1,2)*(1101 - 7068*t2 + 13917*Power(t2,2) - 
                  10509*Power(t2,3) + 2492*Power(t2,4) + 83*Power(t2,5)) \
+ t1*(1815 - 9313*t2 + 17813*Power(t2,2) - 16167*Power(t2,3) + 
                  7575*Power(t2,4) - 1989*Power(t2,5) + 166*Power(t2,6)) \
+ Power(s2,2)*(-323 + 2692*t2 - 5799*Power(t2,2) + 4828*Power(t2,3) - 
                  1502*Power(t2,4) + 195*Power(t2,5) + 
                  t1*(-128 + 1179*t2 - 2551*Power(t2,2) + 
                     1782*Power(t2,3) - 168*Power(t2,4))) + 
               s2*(266 + 328*t2 - 4094*Power(t2,2) + 6900*Power(t2,3) - 
                  4771*Power(t2,4) + 1288*Power(t2,5) - 127*Power(t2,6) + 
                  Power(t1,2)*
                   (405 - 2075*t2 + 3347*Power(t2,2) - 
                     1962*Power(t2,3) + 123*Power(t2,4)) + 
                  t1*(25 - 4083*t2 + 12451*Power(t2,2) - 
                     11981*Power(t2,3) + 3897*Power(t2,4) - 
                     223*Power(t2,5))))) - 
         Power(s,5)*(2*t2*(19 - 72*t2 + 30*Power(t2,2) + 
               69*Power(t2,3) - 47*Power(t2,4) + 5*Power(t2,5)) + 
            2*Power(s1,6)*((5 - 6*t2)*t2 + 5*s2*(-2 + 3*t2) - 
               3*t1*(-5 + 7*t2)) + 
            s1*(-30 + (1819 - 342*s2 + 604*Power(s2,2) - 
                  100*Power(s2,3))*t2 + 
               2*(-1812 - 271*s2 - 806*Power(s2,2) + 114*Power(s2,3))*
                Power(t2,2) + 
               (2660 + 2316*s2 + 1194*Power(s2,2) - 76*Power(s2,3))*
                Power(t2,3) - 
               (1095 + 1879*s2 + 324*Power(s2,2) + 20*Power(s2,3))*
                Power(t2,4) + 
               (415 + 498*s2 + 40*Power(s2,2))*Power(t2,5) - 
               (89 + 45*s2)*Power(t2,6) + 7*Power(t2,7) + 
               Power(t1,3)*(5 + 25*t2 + 32*Power(t2,2) - 
                  148*Power(t2,3) + 65*Power(t2,4)) - 
               Power(t1,2)*(30 + 2*(-876 + 305*s2)*t2 + 
                  (4009 - 1205*s2)*Power(t2,2) + 
                  (-2870 + 592*s2)*Power(t2,3) + 
                  (620 - 50*s2)*Power(t2,4) + 5*Power(t2,5)) + 
               t1*(24 + (-2892 - 315*s2 + 250*Power(s2,2))*t2 + 
                  (6602 + 2320*s2 - 706*Power(s2,2))*Power(t2,2) + 
                  (-6371 - 2625*s2 + 426*Power(s2,2))*Power(t2,3) + 
                  (3229 + 844*s2 - 40*Power(s2,2))*Power(t2,4) - 
                  (749 + 55*s2)*Power(t2,5) + 57*Power(t2,6))) + 
            Power(s1,5)*(-32 - 5*Power(s2,3) + 70*Power(t1,3) + 
               Power(s2,2)*(46 + 45*t1 - 110*t2) + 120*t2 - 
               135*Power(t2,2) + 95*Power(t2,3) - 
               5*Power(t1,2)*(-32 + 47*t2) + 
               t1*(416 - 755*t2 + 55*Power(t2,2)) + 
               s2*(-294 - 105*Power(t1,2) + 496*t2 + 15*Power(t2,2) + 
                  5*t1*(-44 + 81*t2))) - 
            Power(s1,4)*(232 - 734*t2 + 814*Power(t2,2) - 
               442*Power(t2,3) + 216*Power(t2,4) + 
               5*Power(t1,3)*(-41 + 50*t2) + Power(s2,3)*(-19 + 80*t2) + 
               Power(t1,2)*(277 - 408*t2 - 471*Power(t2,2)) + 
               Power(s2,2)*(305 + t1*(55 - 240*t2) - 724*t2 + 
                  15*Power(t2,2)) - 
               4*t1*(211 - 768*t2 + 632*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(Power(t1,2)*(139 - 80*t2) + 
                  2*t1*(-280 + 511*t2 + 286*Power(t2,2)) + 
                  2*(225 - 935*t2 + 790*Power(t2,2) + 61*Power(t2,3)))) + 
            Power(s1,3)*(-656 + 2313*t2 - 2581*Power(t2,2) + 
               1747*Power(t2,3) - 601*Power(t2,4) + 193*Power(t2,5) + 
               Power(s2,3)*(69 - 233*t2 + 33*Power(t2,2)) + 
               Power(t1,3)*(160 - 467*t2 + 474*Power(t2,2)) - 
               Power(t1,2)*(999 - 3519*t2 + 2007*Power(t2,2) + 
                  382*Power(t2,3)) + 
               t1*(1252 - 5639*t2 + 7647*Power(t2,2) - 
                  3851*Power(t2,3) + 107*Power(t2,4)) + 
               Power(s2,2)*(-549 + 2353*t2 - 1813*Power(t2,2) + 
                  219*Power(t2,3) + t1*(-233 + 883*t2 - 369*Power(t2,2))\
) + s2*(-265 + 2118*t2 - 4363*Power(t2,2) + 2438*Power(t2,3) - 
                  41*Power(t2,4) + 
                  Power(t1,2)*(138 - 417*t2 - 71*Power(t2,2)) + 
                  t1*(1154 - 4927*t2 + 3534*Power(t2,2) + 
                     153*Power(t2,3)))) + 
            Power(s1,2)*(-693 + 3357*t2 - 4646*Power(t2,2) + 
               2938*Power(t2,3) - 1452*Power(t2,4) + 374*Power(t2,5) - 
               68*Power(t2,6) + 
               Power(t1,3)*(47 - 192*t2 + 402*Power(t2,2) - 
                  332*Power(t2,3)) + 
               Power(s2,3)*(46 - 281*t2 + 268*Power(t2,2) + 
                  52*Power(t2,3)) + 
               Power(t1,2)*(-744 + 4473*t2 - 6079*Power(t2,2) + 
                  2028*Power(t2,3) + 122*Power(t2,4)) + 
               t1*(1107 - 6333*t2 + 10812*Power(t2,2) - 
                  8070*Power(t2,3) + 2768*Power(t2,4) - 188*Power(t2,5)) \
+ Power(s2,2)*(-281 + 2076*t2 - 3167*Power(t2,2) + 1336*Power(t2,3) - 
                  160*Power(t2,4) + 
                  t1*(-122 + 897*t2 - 1190*Power(t2,2) + 182*Power(t2,3))\
) + s2*(Power(t1,2)*(220 - 1085*t2 + 1102*Power(t2,2) - 32*Power(t2,3)) + 
                  2*t1*(151 - 1905*t2 + 3433*Power(t2,2) - 
                     1528*Power(t2,3) + 56*Power(t2,4)) + 
                  2*(32 + 440*t2 - 1922*Power(t2,2) + 2315*Power(t2,3) - 
                     899*Power(t2,4) + 75*Power(t2,5))))) + 
         Power(s,3)*(-2*Power(s1,8)*(s2 - t1)*(-1 + t2) + 
            2*t2*(-1 + 16*t2 - 41*Power(t2,2) + 12*Power(t2,3) + 
               38*Power(t2,4) - 28*Power(t2,5) + 4*Power(t2,6)) + 
            Power(s1,7)*(20 + 10*Power(s2,3) - 28*Power(t1,3) - 27*t2 + 
               7*Power(t2,2) - 4*Power(t2,3) + 
               Power(s2,2)*(-91 - 45*t1 + 119*t2) + 
               Power(t1,2)*(-124 + 151*t2) + 
               s2*(81 + 63*Power(t1,2) + t1*(234 - 293*t2) - 77*t2 - 
                  18*Power(t2,2)) + t1*(-124 + 131*t2 + 15*Power(t2,2))) \
+ Power(s1,6)*(101 - 328*t2 + 262*Power(t2,2) - 45*Power(t2,3) + 
               26*Power(t2,4) + Power(t1,3)*(-27 + 26*t2) + 
               2*Power(s2,3)*(-47 + 75*t2) + 
               Power(t1,2)*(359 - 177*t2 - 395*Power(t2,2)) + 
               Power(s2,2)*(400 + t1*(240 - 367*t2) - 481*t2 - 
                  103*Power(t2,2)) - 
               t1*(308 - 1005*t2 + 619*Power(t2,2) + 139*Power(t2,3)) + 
               s2*(149 - 473*t2 + 201*Power(t2,2) + 148*Power(t2,3) + 
                  2*Power(t1,2)*(-53 + 87*t2) + 
                  t1*(-732 + 507*t2 + 646*Power(t2,2)))) - 
            Power(s1,5)*(-360 + 1063*t2 - 1483*Power(t2,2) + 
               883*Power(t2,3) - 100*Power(t2,4) + 53*Power(t2,5) + 
               Power(t1,3)*(15 - 234*t2 + 220*Power(t2,2)) + 
               Power(s2,3)*(175 - 444*t2 + 297*Power(t2,2)) - 
               Power(t1,2)*(1556 - 4125*t2 + 2274*Power(t2,2) + 
                  526*Power(t2,3)) + 
               t1*(709 - 2631*t2 + 3722*Power(t2,2) - 
                  1668*Power(t2,3) - 198*Power(t2,4)) + 
               Power(s2,2)*(-1093 + 3251*t2 - 2576*Power(t2,2) + 
                  321*Power(t2,3) + 
                  t1*(-546 + 1375*t2 - 930*Power(t2,2))) + 
               s2*(-114 + 1131*t2 - 1895*Power(t2,2) + 609*Power(t2,3) + 
                  207*Power(t2,4) + 
                  Power(t1,2)*(347 - 614*t2 + 323*Power(t2,2)) + 
                  t1*(2446 - 7101*t2 + 4664*Power(t2,2) + 
                     407*Power(t2,3)))) + 
            Power(s1,4)*(780 - 2919*t2 + 3613*Power(t2,2) - 
               2661*Power(t2,3) + 1380*Power(t2,4) - 127*Power(t2,5) + 
               46*Power(t2,6) + 
               Power(s2,3)*(-103 + 594*t2 - 741*Power(t2,2) + 
                  58*Power(t2,3)) + 
               Power(t1,3)*(25 - 65*t2 - 354*Power(t2,2) + 
                  544*Power(t2,3)) + 
               Power(t1,2)*(1931 - 8656*t2 + 12116*Power(t2,2) - 
                  4803*Power(t2,3) - 478*Power(t2,4)) + 
               t1*(-1673 + 6378*t2 - 9089*Power(t2,2) + 
                  6872*Power(t2,3) - 2559*Power(t2,4) + 21*Power(t2,5)) \
+ Power(s2,2)*(853 - 4661*t2 + 7439*Power(t2,2) - 3893*Power(t2,3) + 
                  590*Power(t2,4) + 
                  t1*(375 - 2147*t2 + 2959*Power(t2,2) - 
                     737*Power(t2,3))) + 
               s2*(327 - 1351*t2 + 3686*Power(t2,2) - 4139*Power(t2,3) + 
                  1271*Power(t2,4) + 8*Power(t2,5) + 
                  Power(t1,2)*
                   (-377 + 1830*t2 - 1939*Power(t2,2) + 54*Power(t2,3)) \
+ t1*(-2104 + 11013*t2 - 17824*Power(t2,2) + 8787*Power(t2,3) - 
                     150*Power(t2,4)))) + 
            Power(s1,3)*(724 - 3611*t2 + 6522*Power(t2,2) - 
               4798*Power(t2,3) + 1984*Power(t2,4) - 996*Power(t2,5) + 
               92*Power(t2,6) - 17*Power(t2,7) + 
               Power(t1,3)*(51 - 128*t2 + 170*Power(t2,2) + 
                  173*Power(t2,3) - 424*Power(t2,4)) + 
               Power(s2,3)*(-95 + 457*t2 - 826*Power(t2,2) + 
                  523*Power(t2,3) + 151*Power(t2,4)) + 
               Power(t1,2)*(1669 - 9164*t2 + 17783*Power(t2,2) - 
                  15003*Power(t2,3) + 4243*Power(t2,4) + 225*Power(t2,5)\
) + t1*(-1791 + 9072*t2 - 16690*Power(t2,2) + 13948*Power(t2,3) - 
                  6291*Power(t2,4) + 1950*Power(t2,5) - 164*Power(t2,6)) \
+ Power(s2,2)*(624 - 3558*t2 + 7680*Power(t2,2) - 7417*Power(t2,3) + 
                  2669*Power(t2,4) - 400*Power(t2,5) + 
                  t1*(264 - 1578*t2 + 3413*Power(t2,2) - 
                     2900*Power(t2,3) + 294*Power(t2,4))) + 
               s2*(225 - 2321*t2 + 5223*Power(t2,2) - 6383*Power(t2,3) + 
                  4486*Power(t2,4) - 1167*Power(t2,5) + 
                  119*Power(t2,6) + 
                  Power(t1,2)*
                   (-458 + 2027*t2 - 3798*Power(t2,2) + 
                     2781*Power(t2,3) - 81*Power(t2,4)) + 
                  t1*(-1160 + 8611*t2 - 19424*Power(t2,2) + 
                     19305*Power(t2,3) - 7184*Power(t2,4) + 
                     369*Power(t2,5)))) + 
            Power(s1,2)*(941 - 4106*t2 + 7290*Power(t2,2) - 
               6776*Power(t2,3) + 3117*Power(t2,4) - 711*Power(t2,5) + 
               306*Power(t2,6) - 31*Power(t2,7) + 2*Power(t2,8) - 
               Power(s2,3)*(26 - 317*t2 + 766*Power(t2,2) - 
                  599*Power(t2,3) + 114*Power(t2,4) + 82*Power(t2,5)) + 
               Power(t1,3)*(35 - 231*t2 + 274*Power(t2,2) - 
                  48*Power(t2,3) - 97*Power(t2,4) + 110*Power(t2,5)) + 
               Power(t1,2)*(990 - 6381*t2 + 15457*Power(t2,2) - 
                  16868*Power(t2,3) + 8577*Power(t2,4) - 
                  1599*Power(t2,5) - 33*Power(t2,6)) + 
               t1*(-1784 + 8673*t2 - 17786*Power(t2,2) + 
                  18971*Power(t2,3) - 10569*Power(t2,4) + 
                  3072*Power(t2,5) - 659*Power(t2,6) + 73*Power(t2,7)) + 
               Power(s2,2)*(173 - 2001*t2 + 5475*Power(t2,2) - 
                  6223*Power(t2,3) + 3558*Power(t2,4) - 
                  929*Power(t2,5) + 131*Power(t2,6) + 
                  t1*(52 - 820*t2 + 2478*Power(t2,2) - 
                     2730*Power(t2,3) + 1272*Power(t2,4) - 
                     87*Power(t2,5))) + 
               s2*(-321 + 353*t2 + 3194*Power(t2,2) - 7113*Power(t2,3) + 
                  5930*Power(t2,4) - 2507*Power(t2,5) + 
                  457*Power(t2,6) - 52*Power(t2,7) + 
                  Power(t1,2)*
                   (-362 + 2089*t2 - 4144*Power(t2,2) + 
                     3795*Power(t2,3) - 1652*Power(t2,4) + 
                     134*Power(t2,5)) + 
                  t1*(326 + 1935*t2 - 11184*Power(t2,2) + 
                     16415*Power(t2,3) - 10344*Power(t2,4) + 
                     2759*Power(t2,5) - 194*Power(t2,6)))) + 
            s1*(Power(t1,3)*(23 - 54*t2 + 172*Power(t2,2) - 
                  217*Power(t2,3) + 37*Power(t2,4) + 40*Power(t2,5) - 
                  6*Power(t2,6)) + 
               Power(t1,2)*(-42 + 4*(-361 + 126*s2)*t2 + 
                  (5946 - 2043*s2)*Power(t2,2) + 
                  (-9244 + 2966*s2)*Power(t2,3) + 
                  (6567 - 1764*s2)*Power(t2,4) + 
                  (-2046 + 392*s2)*Power(t2,5) + 
                  (212 - 29*s2)*Power(t2,6)) + 
               t1*(21 + (2697 - 559*s2 - 57*Power(s2,2))*t2 + 
                  (-9223 - 148*s2 + 596*Power(s2,2))*Power(t2,2) + 
                  (12511 + 4035*s2 - 1365*Power(s2,2))*Power(t2,3) + 
                  (-8979 - 5224*s2 + 998*Power(s2,2))*Power(t2,4) + 
                  (3756 + 2409*s2 - 221*Power(s2,2))*Power(t2,5) + 
                  (-879 - 440*s2 + 12*Power(s2,2))*Power(t2,6) + 
                  (100 + 29*s2)*Power(t2,7) - 6*Power(t2,8)) + 
               t2*(-1445 + 4679*t2 - 5653*Power(t2,2) + 
                  3361*Power(t2,3) - 1146*Power(t2,4) + 248*Power(t2,5) - 
                  48*Power(t2,6) + 4*Power(t2,7) + 
                  2*Power(s2,3)*
                   (15 - 119*t2 + 218*Power(t2,2) - 104*Power(t2,3) - 
                     7*Power(t2,4) + 5*Power(t2,5)) + 
                  Power(s2,2)*
                   (-208 + 1469*t2 - 2840*Power(t2,2) + 
                     2119*Power(t2,3) - 728*Power(t2,4) + 
                     153*Power(t2,5) - 16*Power(t2,6)) + 
                  s2*(480 - 1009*t2 - 782*Power(t2,2) + 
                     2949*Power(t2,3) - 2220*Power(t2,4) + 
                     658*Power(t2,5) - 78*Power(t2,6) + 4*Power(t2,7))))) \
- Power(s,2)*(2*Power(-1 + t2,2)*Power(t2,2)*
             (2 - 7*t2 - 5*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s1,8)*(4 + 5*Power(s2,3) - 8*Power(t1,3) - 4*t2 + 
               Power(s2,2)*(-43 - 18*t1 + 48*t2) + 
               Power(t1,2)*(-48 + 53*t2) + 
               s2*(13 + 21*Power(t1,2) + t1*(95 - 105*t2) - 9*t2 - 
                  4*Power(t2,2)) + t1*(-21 + 17*t2 + 4*Power(t2,2))) + 
            s1*(-5 + (-610 + 202*s2 - 35*Power(s2,2) + 4*Power(s2,3))*
                t2 + (2501 - 740*s2 + 504*Power(s2,2) - 75*Power(s2,3))*
                Power(t2,2) + 
               (-3726 + 208*s2 - 1559*Power(s2,2) + 252*Power(s2,3))*
                Power(t2,3) + 
               (2577 + 1544*s2 + 1770*Power(s2,2) - 249*Power(s2,3))*
                Power(t2,4) + 
               (-912 - 1868*s2 - 877*Power(s2,2) + 48*Power(s2,3))*
                Power(t2,5) + 
               (189 + 772*s2 + 240*Power(s2,2) + 17*Power(s2,3))*
                Power(t2,6) - 
               2*(8 + 63*s2 + 23*Power(s2,2) + Power(s2,3))*
                Power(t2,7) + (2 + 8*s2 + 3*Power(s2,2))*Power(t2,8) + 
               Power(t1,3)*(11 + 24*t2 - 32*Power(t2,2) - 
                  91*Power(t2,3) + 121*Power(t2,4) - 22*Power(t2,5) - 
                  6*Power(t2,6)) + 
               Power(t1,2)*(-27 + (-676 + 197*s2)*t2 + 
                  (3252 - 1043*s2)*Power(t2,2) + 
                  (-6349 + 2092*s2)*Power(t2,3) + 
                  (6202 - 1913*s2)*Power(t2,4) + 
                  (-2992 + 761*s2)*Power(t2,5) + 
                  (626 - 115*s2)*Power(t2,6) + 6*(-6 + s2)*Power(t2,7)) \
+ t1*(21 + (1222 - 331*s2 - 5*Power(s2,2))*t2 + 
                  (-5181 + 702*s2 + 166*Power(s2,2))*Power(t2,2) + 
                  (8554 + 1255*s2 - 711*Power(s2,2))*Power(t2,3) + 
                  (-7257 - 3728*s2 + 928*Power(s2,2))*Power(t2,4) + 
                  (3434 + 2915*s2 - 415*Power(s2,2))*Power(t2,5) + 
                  (-897 - 930*s2 + 54*Power(s2,2))*Power(t2,6) + 
                  (110 + 123*s2 - 2*Power(s2,2))*Power(t2,7) - 
                  6*(1 + s2)*Power(t2,8))) + 
            Power(s1,7)*(39 + Power(t1,3)*(36 - 40*t2) - 104*t2 + 
               65*Power(t2,2) - 2*Power(t2,3) + 2*Power(t2,4) + 
               Power(s2,3)*(-76 + 92*t2) + 
               Power(t1,2)*(181 - 88*t2 - 133*Power(t2,2)) + 
               Power(s2,2)*(212 + t1*(202 - 238*t2) - 206*t2 - 
                  46*Power(t2,2)) - 
               2*t1*(34 - 98*t2 + 39*Power(t2,2) + 25*Power(t2,3)) + 
               2*s2*(3 - 6*t2 - 22*Power(t2,2) + 25*Power(t2,3) + 
                  Power(t1,2)*(-79 + 91*t2) + 
                  t1*(-187 + 110*t2 + 117*Power(t2,2)))) + 
            Power(s1,6)*(59 - 299*t2 + 511*Power(t2,2) - 
               264*Power(t2,3) - 7*Power(t2,5) + 
               Power(s2,3)*(-91 + 293*t2 - 252*Power(t2,2)) + 
               Power(t1,3)*(-7 + 20*t2 + 30*Power(t2,2)) + 
               Power(t1,2)*(762 - 2022*t2 + 1137*Power(t2,2) + 
                  184*Power(t2,3)) + 
               t1*(-177 + 734*t2 - 968*Power(t2,2) + 299*Power(t2,3) + 
                  112*Power(t2,4)) + 
               Power(s2,2)*(557 - 1715*t2 + 1423*Power(t2,2) - 
                  204*Power(t2,3) + t1*(261 - 820*t2 + 702*Power(t2,2))) \
- s2*(-90 + 377*t2 - 282*Power(t2,2) - 114*Power(t2,3) + 
                  109*Power(t2,4) + 
                  Power(t1,2)*(144 - 441*t2 + 433*Power(t2,2)) + 
                  t1*(1309 - 3740*t2 + 2436*Power(t2,2) + 
                     117*Power(t2,3)))) + 
            Power(s1,5)*(339 - 888*t2 + 1013*Power(t2,2) - 
               983*Power(t2,3) + 513*Power(t2,4) - 3*Power(t2,5) + 
               9*Power(t2,6) + 
               Power(s2,3)*(-21 + 339*t2 - 488*Power(t2,2) + 
                  153*Power(t2,3)) + 
               Power(t1,3)*(32 - 85*t2 - 168*Power(t2,2) + 
                  206*Power(t2,3)) + 
               Power(t1,2)*(814 - 4520*t2 + 7005*Power(t2,2) - 
                  2983*Power(t2,3) - 260*Power(t2,4)) - 
               t1*(682 - 2234*t2 + 3167*Power(t2,2) - 
                  2361*Power(t2,3) + 691*Power(t2,4) + 55*Power(t2,5)) + 
               Power(s2,2)*(346 - 2523*t2 + 4447*Power(t2,2) - 
                  2682*Power(t2,3) + 468*Power(t2,4) + 
                  t1*(105 - 1156*t2 + 1729*Power(t2,2) - 
                     659*Power(t2,3))) + 
               s2*(6 - 526*t2 + 1830*Power(t2,2) - 1471*Power(t2,3) + 
                  102*Power(t2,4) + 59*Power(t2,5) + 
                  Power(t1,2)*
                   (-141 + 918*t2 - 958*Power(t2,2) + 194*Power(t2,3)) - 
                  2*t1*(399 - 3099*t2 + 5612*Power(t2,2) - 
                     2936*Power(t2,3) + 80*Power(t2,4)))) + 
            Power(s1,4)*(235 - 1620*t2 + 2670*Power(t2,2) - 
               1458*Power(t2,3) + 653*Power(t2,4) - 491*Power(t2,5) + 
               16*Power(t2,6) - 5*Power(t2,7) + 
               Power(t1,3)*t2*
                (-137 + 386*t2 + 7*Power(t2,2) - 314*Power(t2,3)) + 
               Power(s2,3)*(-30 + 108*t2 - 489*Power(t2,2) + 
                  448*Power(t2,3) + 84*Power(t2,4)) + 
               Power(t1,2)*(875 - 4432*t2 + 10273*Power(t2,2) - 
                  10802*Power(t2,3) + 3648*Power(t2,4) + 229*Power(t2,5)\
) + t1*(-799 + 4170*t2 - 7214*Power(t2,2) + 5718*Power(t2,3) - 
                  2568*Power(t2,4) + 751*Power(t2,5) - 58*Power(t2,6)) + 
               Power(s2,2)*(274 - 1480*t2 + 4431*Power(t2,2) - 
                  5355*Power(t2,3) + 2340*Power(t2,4) - 
                  419*Power(t2,5) + 
                  t1*(120 - 587*t2 + 2078*Power(t2,2) - 
                     2187*Power(t2,3) + 276*Power(t2,4))) + 
               s2*(409 - 1021*t2 + 1666*Power(t2,2) - 3114*Power(t2,3) + 
                  2377*Power(t2,4) - 354*Power(t2,5) + 37*Power(t2,6) + 
                  Power(t1,2)*
                   (-114 + 840*t2 - 2392*Power(t2,2) + 
                     1902*Power(t2,3) + Power(t2,4)) + 
                  t1*(-953 + 4126*t2 - 11382*Power(t2,2) + 
                     14836*Power(t2,3) - 6536*Power(t2,4) + 
                     327*Power(t2,5)))) + 
            Power(s1,3)*(210 - 895*t2 + 2283*Power(t2,2) - 
               2544*Power(t2,3) + 598*Power(t2,4) + 176*Power(t2,5) + 
               184*Power(t2,6) - 13*Power(t2,7) + Power(t2,8) - 
               Power(s2,3)*(47 - 240*t2 + 314*Power(t2,2) - 
                  321*Power(t2,3) + 195*Power(t2,4) + 111*Power(t2,5)) + 
               Power(t1,3)*(43 - 25*t2 + 2*Power(t2,2) - 
                  223*Power(t2,3) + 121*Power(t2,4) + 144*Power(t2,5)) + 
               Power(t1,2)*(698 - 4635*t2 + 10347*Power(t2,2) - 
                  12150*Power(t2,3) + 8134*Power(t2,4) - 
                  2139*Power(t2,5) - 79*Power(t2,6)) + 
               t1*(-651 + 3777*t2 - 8824*Power(t2,2) + 
                  9556*Power(t2,3) - 4628*Power(t2,4) + 
                  1026*Power(t2,5) - 315*Power(t2,6) + 59*Power(t2,7)) + 
               Power(s2,2)*(305 - 1753*t2 + 3462*Power(t2,2) - 
                  4367*Power(t2,3) + 3406*Power(t2,4) - 
                  1072*Power(t2,5) + 195*Power(t2,6) + 
                  t1*(113 - 749*t2 + 1449*Power(t2,2) - 
                     1890*Power(t2,3) + 1438*Power(t2,4) - 
                     87*Power(t2,5))) + 
               s2*(62 - 1819*t2 + 4432*Power(t2,2) - 4278*Power(t2,3) + 
                  3026*Power(t2,4) - 1610*Power(t2,5) + 
                  228*Power(t2,6) - 41*Power(t2,7) + 
                  Power(t1,2)*
                   (-231 + 914*t2 - 1798*Power(t2,2) + 
                     2554*Power(t2,3) - 1763*Power(t2,4) + 
                     94*Power(t2,5)) - 
                  2*t1*(246 - 2440*t2 + 5475*Power(t2,2) - 
                     6505*Power(t2,3) + 5099*Power(t2,4) - 
                     1825*Power(t2,5) + 126*Power(t2,6)))) + 
            Power(s1,2)*(501 - 2253*t2 + 3718*Power(t2,2) - 
               3142*Power(t2,3) + 1484*Power(t2,4) - 60*Power(t2,5) - 
               245*Power(t2,6) - 5*Power(t2,7) + 2*Power(t2,8) - 
               Power(t1,3)*(27 + 69*t2 - 206*Power(t2,2) + 
                  75*Power(t2,3) + 24*Power(t2,4) + 18*Power(t2,5) + 
                  18*Power(t2,6)) + 
               Power(s2,3)*(-4 + 122*t2 - 462*Power(t2,2) + 
                  476*Power(t2,3) - 128*Power(t2,4) + Power(t2,5) + 
                  31*Power(t2,6)) + 
               Power(t1,2)*(577 - 3398*t2 + 9283*Power(t2,2) - 
                  12366*Power(t2,3) + 8420*Power(t2,4) - 
                  3076*Power(t2,5) + 505*Power(t2,6) + 6*Power(t2,7)) + 
               t1*(-1015 + 4902*t2 - 10150*Power(t2,2) + 
                  11634*Power(t2,3) - 7494*Power(t2,4) + 
                  2240*Power(t2,5) - 128*Power(t2,6) + 23*Power(t2,7) - 
                  12*Power(t2,8)) + 
               Power(s2,2)*(35 - 809*t2 + 3038*Power(t2,2) - 
                  4098*Power(t2,3) + 2779*Power(t2,4) - 
                  1235*Power(t2,5) + 286*Power(t2,6) - 45*Power(t2,7) + 
                  t1*(5 - 279*t2 + 1340*Power(t2,2) - 1895*Power(t2,3) + 
                     1122*Power(t2,4) - 416*Power(t2,5) + 26*Power(t2,6))\
) + s2*(-174 + 558*t2 + 1402*Power(t2,2) - 5121*Power(t2,3) + 
                  4976*Power(t2,4) - 2151*Power(t2,5) + 547*Power(t2,6) - 
                  45*Power(t2,7) + 8*Power(t2,8) + 
                  Power(t1,2)*
                   (-169 + 1154*t2 - 2692*Power(t2,2) + 
                     2852*Power(t2,3) - 1637*Power(t2,4) + 
                     643*Power(t2,5) - 65*Power(t2,6)) + 
                  t1*(275 + 30*t2 - 5582*Power(t2,2) + 
                     11670*Power(t2,3) - 9552*Power(t2,4) + 
                     4166*Power(t2,5) - 988*Power(t2,6) + 79*Power(t2,7)))\
)) + s*(2*Power(-1 + t2,3)*Power(t2,3)*(1 + t2) + 
            Power(s1,9)*Power(s2 - t1,2)*(-8 + s2 - t1 + 8*t2) - 
            s1*(-1 + t2)*(-2 + (-133 + 34*s2)*t2 + 
               (576 - 195*s2 + 66*Power(s2,2) - 8*Power(s2,3))*
                Power(t2,2) + 
               (-773 + 61*s2 - 326*Power(s2,2) + 52*Power(s2,3))*
                Power(t2,3) + 
               (359 + 415*s2 + 365*Power(s2,2) - 58*Power(s2,3))*
                Power(t2,4) - 
               (22 + 405*s2 + 135*Power(s2,2))*Power(t2,5) + 
               (-9 + 94*s2 + 36*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,6) + (4 - 4*s2 - 6*Power(s2,2))*Power(t2,7) + 
               Power(t1,3)*(2 + 37*t2 - 63*Power(t2,2) + 6*Power(t2,3) + 
                  34*Power(t2,4) - 6*Power(t2,5)) + 
               Power(t1,2)*(-6 + (-207 + 34*s2)*t2 + 
                  (806 - 229*s2)*Power(t2,2) + 
                  (-1476 + 481*s2)*Power(t2,3) + 
                  (1351 - 435*s2)*Power(t2,4) + 
                  (-540 + 131*s2)*Power(t2,5) - 12*(-6 + s2)*Power(t2,6)) \
+ t1*(6 + (303 - 68*s2)*t2 + (-1249 + 288*s2 + 14*Power(s2,2))*
                   Power(t2,2) + 
                  (1895 + 213*s2 - 139*Power(s2,2))*Power(t2,3) + 
                  3*(-447 - 305*s2 + 73*Power(s2,2))*Power(t2,4) + 
                  (434 + 617*s2 - 68*Power(s2,2))*Power(t2,5) + 
                  (-42 - 147*s2 + 4*Power(s2,2))*Power(t2,6) + 
                  6*(-1 + 2*s2)*Power(t2,7))) + 
            Power(s1,8)*(Power(t1,3)*(25 - 26*t2) + 8*Power(-1 + t2,2) - 
               6*t1*Power(-1 + t2,2)*(1 + t2) + 
               Power(s2,3)*(-31 + 32*t2) + 
               Power(t1,2)*(47 - 38*t2 - 9*Power(t2,2)) + 
               Power(s2,2)*(57 + t1*(87 - 90*t2) - 58*t2 + Power(t2,2)) + 
               s2*(2*Power(-1 + t2,2)*(-5 + 3*t2) + 
                  Power(t1,2)*(-81 + 84*t2) + 
                  16*t1*(-6 + 5*t2 + Power(t2,2)))) - 
            Power(s1,7)*(Power(t1,3)*(22 + 25*t2 - 62*Power(t2,2)) + 
               Power(-1 + t2,2)*(-8 + 49*t2 + 3*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(-44 + 29*t2 + 21*Power(t2,2)) + 
               Power(s2,3)*(8 - 101*t2 + 108*Power(t2,2)) + 
               Power(t1,2)*(-179 + 484*t2 - 311*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s2,2)*(-130 + 468*t2 - 426*Power(t2,2) + 
                  88*Power(t2,3) + t1*(-13 + 265*t2 - 297*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(-49 - 61*t2 + 20*Power(t2,2)) + 
                  Power(t1,2)*(-25 - 173*t2 + 243*Power(t2,2)) + 
                  t1*(328 - 949*t2 + 674*Power(t2,2) - 53*Power(t2,3)))) + 
            Power(s1,6)*(-(Power(t1,2)*Power(-1 + t2,2)*
                  (-169 + 961*t2 + 32*Power(t2,2))) + 
               Power(t1,3)*(2 + 61*t2 - 92*Power(t2,2) + 4*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (25 + 26*t2 + 102*Power(t2,2) + 7*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (122 - 281*t2 + 31*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s2,3)*(5 + 51*t2 - 135*Power(t2,2) + 
                  104*Power(t2,3)) + 
               s2*(-(t1*Power(-1 + t2,2)*
                     (158 - 1637*t2 + 130*Power(t2,2))) + 
                  Power(-1 + t2,2)*
                   (39 - 417*t2 - 113*Power(t2,2) + 20*Power(t2,3)) + 
                  Power(t1,2)*
                   (-3 + 19*t2 - 119*Power(t2,2) + 178*Power(t2,3))) + 
               Power(s2,2)*(Power(-1 + t2,2)*
                   (52 - 533*t2 + 204*Power(t2,2)) - 
                  t1*(9 + 160*t2 - 419*Power(t2,2) + 325*Power(t2,3)))) + 
            Power(s1,5)*(Power(t1,3)*
                (28 - 149*t2 + 162*Power(t2,2) + 67*Power(t2,3) - 
                  103*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (209 - 594*t2 + 569*Power(t2,2) + 19*Power(t2,3) + 
                  3*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (-136 + 261*t2 + 168*Power(t2,2) + 96*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,3)*(9 - 31*t2 - 123*Power(t2,2) + 
                  135*Power(t2,3) + 5*Power(t2,4)) + 
               Power(t1,2)*(1 - 463*t2 + 2906*Power(t2,2) - 
                  3977*Power(t2,3) + 1443*Power(t2,4) + 90*Power(t2,5)) + 
               Power(s2,2)*(-68 + 48*t2 + 1011*Power(t2,2) - 
                  1754*Power(t2,3) + 983*Power(t2,4) - 220*Power(t2,5) + 
                  t1*(-4 + 18*t2 + 495*Power(t2,2) - 636*Power(t2,3) + 
                     142*Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (-92 + 123*t2 + 974*Power(t2,2) + 81*Power(t2,3)) + 
                  Power(t1,2)*
                   (-65 + 307*t2 - 681*Power(t2,2) + 421*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  4*t1*(65 - 127*t2 - 731*Power(t2,2) + 
                     1444*Power(t2,3) - 692*Power(t2,4) + 41*Power(t2,5)))\
) + Power(s1,4)*(-(Power(-1 + t2,2)*
                  (96 + 171*t2 - 473*Power(t2,2) - 301*Power(t2,3) - 
                    32*Power(t2,4) + 3*Power(t2,5))) + 
               t1*Power(-1 + t2,2)*
                (9 + 390*t2 - 791*Power(t2,2) + 406*Power(t2,3) + 
                  69*Power(t2,4) + 15*Power(t2,5)) - 
               Power(s2,3)*(16 + 13*t2 - 109*Power(t2,2) - 
                  83*Power(t2,3) + 124*Power(t2,4) + 60*Power(t2,5)) + 
               Power(t1,3)*(-44 + 128*t2 + 32*Power(t2,2) - 
                  290*Power(t2,3) + 113*Power(t2,4) + 82*Power(t2,5)) + 
               Power(t1,2)*(241 - 616*t2 + 867*Power(t2,2) - 
                  2797*Power(t2,3) + 3637*Power(t2,4) - 
                  1269*Power(t2,5) - 63*Power(t2,6)) - 
               Power(s2,2)*(-115 + 79*t2 + 190*Power(t2,2) + 
                  648*Power(t2,3) - 1188*Power(t2,4) + 519*Power(t2,5) - 
                  133*Power(t2,6) + 
                  t1*(-46 + 38*t2 + 135*Power(t2,2) + 449*Power(t2,3) - 
                     672*Power(t2,4) + 33*Power(t2,5))) + 
               s2*(Power(t1,2)*
                   (48 - 95*t2 - 255*Power(t2,2) + 1067*Power(t2,3) - 
                     834*Power(t2,4) + 6*Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (-243 + 35*t2 + 536*Power(t2,2) + 911*Power(t2,3) + 
                     25*Power(t2,4) + 10*Power(t2,5)) + 
                  t1*(-546 + 853*t2 + 614*Power(t2,2) + 
                     1344*Power(t2,3) - 4254*Power(t2,4) + 
                     2135*Power(t2,5) - 146*Power(t2,6)))) + 
            Power(s1,3)*(Power(t1,3)*
                (45 - 49*t2 - 168*Power(t2,2) + 256*Power(t2,3) + 
                  15*Power(t2,4) - 100*Power(t2,5) - 18*Power(t2,6)) + 
               Power(-1 + t2,2)*
                (38 + 271*t2 - 218*Power(t2,2) - 216*Power(t2,3) - 
                  254*Power(t2,4) + 13*Power(t2,5) + 2*Power(t2,6)) - 
               t1*Power(-1 + t2,2)*
                (73 - 86*t2 + 156*Power(t2,2) - 236*Power(t2,3) - 
                  13*Power(t2,4) + 66*Power(t2,5) + 6*Power(t2,6)) + 
               Power(s2,3)*(-8 + 92*t2 - 111*Power(t2,2) - 
                  87*Power(t2,3) + 43*Power(t2,4) + 60*Power(t2,5) + 
                  30*Power(t2,6)) + 
               Power(t1,2)*(56 - 1008*t2 + 2364*Power(t2,2) - 
                  1935*Power(t2,3) + 1416*Power(t2,4) - 
                  1423*Power(t2,5) + 518*Power(t2,6) + 12*Power(t2,7)) + 
               Power(s2,2)*(66 - 622*t2 + 1053*Power(t2,2) - 
                  472*Power(t2,3) + 332*Power(t2,4) - 446*Power(t2,5) + 
                  133*Power(t2,6) - 44*Power(t2,7) + 
                  t1*(14 - 245*t2 + 446*Power(t2,2) - 35*Power(t2,3) + 
                     60*Power(t2,4) - 313*Power(t2,5) + 16*Power(t2,6))) \
+ s2*(Power(t1,2)*(-69 + 162*t2 + 17*Power(t2,2) - 195*Power(t2,3) - 
                     313*Power(t2,4) + 498*Power(t2,5) - 43*Power(t2,6)) \
+ Power(-1 + t2,2)*(-35 - 752*t2 + 751*Power(t2,2) + 441*Power(t2,3) + 
                     327*Power(t2,4) + 14*Power(t2,5) + 4*Power(t2,6)) + 
                  t1*(-32 + 1921*t2 - 4710*Power(t2,2) + 
                     3184*Power(t2,3) - 1096*Power(t2,4) + 
                     1450*Power(t2,5) - 788*Power(t2,6) + 71*Power(t2,7)))\
) - Power(s1,2)*(Power(t1,3)*(35 - 25*t2 - 104*Power(t2,2) + 
                  106*Power(t2,3) + 53*Power(t2,4) - 58*Power(t2,5) - 
                  12*Power(t2,6)) + 
               Power(s2,3)*t2*
                (-16 + 136*t2 - 225*Power(t2,2) + 54*Power(t2,3) + 
                  50*Power(t2,4) + 2*Power(t2,5) + 4*Power(t2,6)) + 
               Power(-1 + t2,2)*
                (-131 + 453*t2 - 129*Power(t2,2) - 234*Power(t2,3) + 
                  26*Power(t2,4) - 91*Power(t2,5) + 10*Power(t2,6)) - 
               t1*Power(-1 + t2,2)*
                (-297 + 941*t2 - 966*Power(t2,2) + 331*Power(t2,3) + 
                  35*Power(t2,4) - 93*Power(t2,5) + 24*Power(t2,6)) + 
               Power(t1,2)*(-201 + 979*t2 - 2809*Power(t2,2) + 
                  4306*Power(t2,3) - 3109*Power(t2,4) + 987*Power(t2,5) - 
                  213*Power(t2,6) + 60*Power(t2,7)) + 
               Power(s2,2)*t2*
                (132 - 899*t2 + 1597*Power(t2,2) - 1062*Power(t2,3) + 
                  359*Power(t2,4) - 143*Power(t2,5) + 22*Power(t2,6) - 
                  6*Power(t2,7) + 
                  t1*(28 - 352*t2 + 762*Power(t2,2) - 448*Power(t2,3) + 
                     31*Power(t2,4) - 40*Power(t2,5) + 4*Power(t2,6))) + 
               s2*(Power(-1 + t2,2)*
                   (34 - 196*t2 - 609*Power(t2,2) + 939*Power(t2,3) - 
                     23*Power(t2,4) + 26*Power(t2,5) + 8*Power(t2,6)) + 
                  Power(t1,2)*
                   (34 - 332*t2 + 920*Power(t2,2) - 1059*Power(t2,3) + 
                     420*Power(t2,4) - 26*Power(t2,5) + 70*Power(t2,6) - 
                     12*Power(t2,7)) + 
                  t1*(-68 + 324*t2 + 1300*Power(t2,2) - 4725*Power(t2,3) + 
                     4664*Power(t2,4) - 1815*Power(t2,5) + 
                     422*Power(t2,6) - 114*Power(t2,7) + 12*Power(t2,8)))))\
))/(s*(-1 + s1)*s1*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       (-1 + s2)*(-1 + t1)*Power(-s + s1 - t2,2)*(1 - s + s1 - t2)*
       Power(-1 + s + t2,2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) + 
    (8*(Power(s,8)*(Power(t1,3) + t1*Power(t2,2) + 2*Power(t2,3)) + 
         Power(s,7)*(Power(t1,3)*(-8 - 7*s1 + 4*t2) + 
            t1*t2*(3 + s1*(-1 + 3*s2 - 6*t2) - 2*t2 + 6*Power(t2,2) - 
               s2*(3 + t2)) + 
            Power(t1,2)*(3 + s2*t2 + s1*(-3 + 3*s2 + t2)) + 
            Power(t2,2)*(4 + s1*(2 + 4*s2 - 8*t2) - 2*t2 + 
               6*Power(t2,2) - s2*(3 + 4*t2))) + 
         (s1 - t2)*(-1 + t2)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,
           2)*(-6 + 23*t2 - 2*s2*t2 - 17*Power(t2,2) + 
            5*s2*Power(t2,2) + Power(s1,3)*(-2 + 3*s2 - 3*t1 + 2*t2) + 
            t1*(6 - 11*t2 + 2*Power(t2,2)) - 
            Power(s1,2)*(1 + s2 + 2*t1 - 3*t2 + 2*s2*t2 - 5*t1*t2 + 
               2*Power(t2,2)) - 
            s1*(11 + t1 - 12*t2 - 6*t1*t2 + Power(t2,2) + 
               2*t1*Power(t2,2) + s2*(-2 + 4*t2 + Power(t2,2)))) + 
         Power(s,6)*(Power(t1,3)*
             (21*Power(s1,2) + s1*(43 - 24*t2) + 
               6*(4 - 5*t2 + Power(t2,2))) + 
            Power(t1,2)*(-21 + (7 - 3*s2)*t2 + (3 + 4*s2)*Power(t2,2) - 
               2*Power(s1,2)*(-8 + 9*s2 + 3*t2) + 
               s1*(3 - 23*t2 + 4*Power(t2,2) + s2*(-17 + 7*t2))) + 
            t1*(3 + (-21 + 20*s2)*t2 + (2 - 11*s2)*Power(t2,2) - 
               2*(7 + 3*s2)*Power(t2,3) + 12*Power(t2,4) + 
               Power(s1,2)*(1 + 3*Power(s2,2) + 15*Power(t2,2) - 
                  s2*(3 + 13*t2)) - 
               s1*(3 - 6*t2 + Power(s2,2)*t2 - 9*Power(t2,2) + 
                  28*Power(t2,3) + s2*(-3 + t2 - 19*Power(t2,2)))) + 
            t2*(3 - 22*t2 - 35*Power(t2,2) + 5*Power(t2,3) + 
               6*Power(t2,4) + Power(s2,2)*t2*(1 + 2*t2) + 
               Power(s1,2)*(1 + 3*Power(s2,2) + s2*(2 - 15*t2) - 7*t2 + 
                  12*Power(t2,2)) + 
               s2*(-3 + 16*t2 + 10*Power(t2,2) - 12*Power(t2,3)) + 
               s1*(2 + 7*t2 - 3*Power(t2,2) - 18*Power(t2,3) - 
                  Power(s2,2)*(3 + 5*t2) + 
                  s2*(3 - 12*t2 + 26*Power(t2,2))))) + 
         Power(s,5)*(1 - 18*t2 + 16*s2*t2 + 33*Power(t2,2) - 
            15*s2*Power(t2,2) - 13*Power(s2,2)*Power(t2,2) + 
            153*Power(t2,3) + 24*s2*Power(t2,3) - 
            10*Power(s2,2)*Power(t2,3) - 152*Power(t2,4) + 
            38*s2*Power(t2,4) + 6*Power(s2,2)*Power(t2,4) + 
            24*Power(t2,5) - 12*s2*Power(t2,5) + 2*Power(t2,6) + 
            Power(t1,3)*(-30 + 78*t2 - 41*Power(t2,2) + 4*Power(t2,3)) + 
            3*Power(t1,2)*(17 - (13 + 3*s2)*t2 - 
               2*(3 + 2*s2)*Power(t2,2) + 2*(2 + s2)*Power(t2,3)) + 
            t1*(-18 + (50 - 37*s2)*t2 + 
               (-42 + 89*s2 + 2*Power(s2,2))*Power(t2,2) - 
               2*(13 + s2)*Power(t2,3) - 3*(9 + 4*s2)*Power(t2,4) + 
               10*Power(t2,5)) + 
            Power(s1,3)*(Power(s2,3) - 35*Power(t1,3) + 
               5*Power(t1,2)*(-7 + 3*t2) - 
               Power(s2,2)*(15*t1 + 11*t2) + 
               t2*(-3 + 9*t2 - 8*Power(t2,2)) - 
               2*t1*(3 - 5*t2 + 10*Power(t2,2)) + 
               s2*(1 + 45*Power(t1,2) - 6*t2 + 20*Power(t2,2) + 
                  2*t1*(7 + 10*t2))) + 
            Power(s1,2)*(1 - 
               (-18 + 16*s2 + 3*Power(s2,2) + 2*Power(s2,3))*t2 + 
               (-59 + 51*s2 + 25*Power(s2,2))*Power(t2,2) + 
               (27 - 50*s2)*Power(t2,3) + 18*Power(t2,4) + 
               Power(t1,3)*(-95 + 60*t2) + 
               Power(t1,2)*(-46 + s2*(77 - 50*t2) + 101*t2 - 
                  20*Power(t2,2)) + 
               t1*(3*s2*(3 - 19*t2)*t2 + Power(s2,2)*(-13 + 16*t2) + 
                  2*(7 - 9*t2 - 17*Power(t2,2) + 26*Power(t2,3)))) + 
            s1*(Power(t1,3)*(-95 + 136*t2 - 30*Power(t2,2)) + 
               Power(t1,2)*(40 + 92*t2 - 73*Power(t2,2) + 
                  6*Power(t2,3) + s2*(31 - 52*t2 + 2*Power(t2,2))) - 
               t2*(11 + 102*t2 - Power(s2,3)*t2 - 222*Power(t2,2) + 
                  53*Power(t2,3) + 12*Power(t2,4) + 
                  Power(s2,2)*(-13 - 19*t2 + 20*Power(t2,2)) + 
                  s2*(3 + 20*t2 + 74*Power(t2,2) - 42*Power(t2,3))) + 
               t1*(3 - 5*t2 + Power(s2,2)*(13 - 5*t2)*t2 + 
                  49*Power(t2,2) + 50*Power(t2,3) - 42*Power(t2,4) + 
                  s2*(-13 - 74*t2 - 21*Power(t2,2) + 46*Power(t2,3))))) + 
         Power(s,4)*(-5 + 29*t2 - 19*s2*t2 + 30*Power(t2,2) - 
            57*s2*Power(t2,2) + 44*Power(s2,2)*Power(t2,2) - 
            361*Power(t2,3) - 37*s2*Power(t2,3) - 
            15*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) + 
            471*Power(t2,4) + 20*s2*Power(t2,4) - 
            31*Power(s2,2)*Power(t2,4) - 205*Power(t2,5) + 
            36*s2*Power(t2,5) + 6*Power(s2,2)*Power(t2,5) + 
            29*Power(t2,6) - 4*s2*Power(t2,6) + 
            Power(t1,3)*(3 - 69*t2 + 87*Power(t2,2) - 24*Power(t2,3) + 
               Power(t2,4)) + 
            Power(t1,2)*(-39 + (37 + 51*s2)*t2 + 
               (59 - 32*s2)*Power(t2,2) - (75 + 16*s2)*Power(t2,3) + 
               (19 + 4*s2)*Power(t2,4)) + 
            t1*(33 - 2*(11 + 8*s2)*t2 + 
               (77 - 142*s2 - 10*Power(s2,2))*Power(t2,2) + 
               (57 + 132*s2 + 4*Power(s2,2))*Power(t2,3) + 
               3*(-19 + 8*s2)*Power(t2,4) - 2*(12 + 5*s2)*Power(t2,5) + 
               3*Power(t2,6)) + 
            Power(s1,4)*(-4*Power(s2,3) + 35*Power(t1,3) - 
               20*Power(t1,2)*(-2 + t2) + 
               Power(s2,2)*(1 + 30*t1 + 14*t2) + 
               t2*(3 - 5*t2 + 2*Power(t2,2)) + 
               t1*(12 - 20*t2 + 15*Power(t2,2)) - 
               s2*(3 + 60*Power(t1,2) - 6*t2 + 10*Power(t2,2) + 
                  2*t1*(13 + 5*t2))) + 
            Power(s1,3)*(5 + Power(t1,3)*(110 - 80*t2) - 51*t2 + 
               92*Power(t2,2) - 41*Power(t2,3) - 6*Power(t2,4) + 
               2*Power(s2,3)*(-2 + 5*t2) + 
               Power(t1,2)*(95 - 174*t2 + 40*Power(t2,2)) - 
               t1*(10 + 19*t2 - 69*Power(t2,2) + 48*Power(t2,3)) + 
               Power(s2,2)*(-1 + 16*t2 - 40*Power(t2,2) - 
                  6*t1*(-8 + 9*t2)) + 
               s2*(1 + 22*t2 - 49*Power(t2,2) + 34*Power(t2,3) + 
                  2*Power(t1,2)*(-69 + 55*t2) + 
                  t1*(-28 + 29*t2 + 58*Power(t2,2)))) + 
            Power(s1,2)*(2 - 116*t2 + 4*Power(s2,3)*(3 - 2*t2)*t2 + 
               405*Power(t2,2) - 421*Power(t2,3) + 110*Power(t2,4) + 
               6*Power(t2,5) + 
               Power(t1,3)*(148 - 247*t2 + 60*Power(t2,2)) + 
               Power(t1,2)*(38 - 366*t2 + 231*Power(t2,2) - 
                  24*Power(t2,3)) + 
               t1*(-40 + 79*t2 - 62*Power(t2,2) - 91*Power(t2,3) + 
                  54*Power(t2,4)) + 
               Power(s2,2)*(2*t1*(9 - 45*t2 + 16*Power(t2,2)) + 
                  t2*(-23 - 74*t2 + 44*Power(t2,2))) + 
               s2*(-1 - 3*t2 + 5*Power(t2,2) + 107*Power(t2,3) - 
                  42*Power(t2,4) + 
                  Power(t1,2)*(-103 + 224*t2 - 48*Power(t2,2)) + 
                  t1*(6 + 175*t2 + 69*Power(t2,2) - 86*Power(t2,3)))) + 
            s1*(-4 + 10*t2 + 358*Power(t2,2) + 
               2*Power(s2,3)*(-5 + t2)*Power(t2,2) - 792*Power(t2,3) + 
               512*Power(t2,4) - 91*Power(t2,5) - 2*Power(t2,6) + 
               Power(t1,3)*(85 - 244*t2 + 151*Power(t2,2) - 
                  16*Power(t2,3)) + 
               Power(t1,2)*(-101 - 117*t2 + 336*Power(t2,2) - 
                  111*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(23 - 44*t2 - 80*Power(t2,2) + 124*Power(t2,3) + 
                  71*Power(t2,4) - 24*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-18 + 25*t2 + 88*Power(t2,2) - 24*Power(t2,3) + 
                  t1*(-38 + 48*t2 - 8*Power(t2,2))) + 
               s2*(1 - 18*t2 + 97*Power(t2,2) - 76*Power(t2,3) - 
                  96*Power(t2,4) + 22*Power(t2,5) - 
                  Power(t1,2)*
                   (17 - 103*t2 + 48*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*(16 + 232*t2 - 315*Power(t2,2) - 84*Power(t2,3) + 
                     48*Power(t2,4))))) + 
         Power(s,3)*(6 - 6*t1 - 30*Power(t1,2) + 30*Power(t1,3) + 
            17*t2 - 24*s2*t2 - 128*t1*t2 + 116*s2*t1*t2 + 
            126*Power(t1,2)*t2 - 84*s2*Power(t1,2)*t2 - 
            33*Power(t1,3)*t2 - 184*Power(t2,2) + 173*s2*Power(t2,2) - 
            52*Power(s2,2)*Power(t2,2) + 184*t1*Power(t2,2) - 
            105*s2*t1*Power(t2,2) + 18*Power(s2,2)*t1*Power(t2,2) - 
            205*Power(t1,2)*Power(t2,2) + 
            157*s2*Power(t1,2)*Power(t2,2) - 
            37*Power(t1,3)*Power(t2,2) + 579*Power(t2,3) - 
            189*s2*Power(t2,3) + 134*Power(s2,2)*Power(t2,3) - 
            10*Power(s2,3)*Power(t2,3) - 77*t1*Power(t2,3) - 
            189*s2*t1*Power(t2,3) - 26*Power(s2,2)*t1*Power(t2,3) + 
            193*Power(t1,2)*Power(t2,3) - 
            46*s2*Power(t1,2)*Power(t2,3) + 36*Power(t1,3)*Power(t2,3) - 
            731*Power(t2,4) - 72*s2*Power(t2,4) + 
            4*Power(s2,2)*Power(t2,4) + 6*Power(s2,3)*Power(t2,4) + 
            149*t1*Power(t2,4) + 85*s2*t1*Power(t2,4) + 
            Power(s2,2)*t1*Power(t2,4) - 87*Power(t1,2)*Power(t2,4) - 
            8*s2*Power(t1,2)*Power(t2,4) - 5*Power(t1,3)*Power(t2,4) + 
            425*Power(t2,5) + 25*s2*Power(t2,5) - 
            30*Power(s2,2)*Power(t2,5) - 33*t1*Power(t2,5) + 
            27*s2*t1*Power(t2,5) + 14*Power(t1,2)*Power(t2,5) + 
            s2*Power(t1,2)*Power(t2,5) - 128*Power(t2,6) + 
            11*s2*Power(t2,6) + 2*Power(s2,2)*Power(t2,6) - 
            13*t1*Power(t2,6) - 3*s2*t1*Power(t2,6) + 16*Power(t2,7) + 
            Power(s1,5)*(6*Power(s2,3) - 21*Power(t1,3) + 
               s2*(3 + 45*Power(t1,2) + t1*(24 - 5*t2) - 2*t2) + 
               (-1 + t2)*t2 - 3*Power(s2,2)*(1 + 10*t1 + 2*t2) + 
               5*Power(t1,2)*(-5 + 3*t2) + 
               t1*(-10 + 15*t2 - 6*Power(t2,2))) - 
            Power(s1,4)*(Power(t1,3)*(70 - 60*t2) + 
               Power(s2,3)*(-11 + 18*t2) - 
               Power(-1 + t2,2)*(-9 + 23*t2) + 
               Power(s2,2)*(4 + t1*(66 - 76*t2) - 20*Power(t2,2)) + 
               2*Power(t1,2)*(42 - 73*t2 + 20*Power(t2,2)) + 
               t1*(11 - 55*t2 + 63*Power(t2,2) - 22*Power(t2,3)) + 
               s2*(Power(t1,2)*(-122 + 115*t2) + 
                  t2*(10 - 9*t2 + 2*Power(t2,2)) + 
                  t1*(-55 + 86*t2 + 7*Power(t2,2)))) + 
            Power(s1,3)*(-31 + 202*t2 - 407*Power(t2,2) + 
               316*Power(t2,3) - 80*Power(t2,4) + 
               3*Power(s2,3)*(2 - 13*t2 + 6*Power(t2,2)) - 
               3*Power(t1,3)*(39 - 76*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(-140 + 482*t2 - 287*Power(t2,2) + 
                  36*Power(t2,3)) + 
               t1*(24 - 9*t2 - 56*Power(t2,2) + 94*Power(t2,3) - 
                  30*Power(t2,4)) + 
               Power(s2,2)*(-7 + 38*t2 + 57*Power(t2,2) - 
                  26*Power(t2,3) + t1*(-50 + 191*t2 - 66*Power(t2,2))) \
+ s2*(-10 + 28*t2 - 13*Power(t2,2) - 34*Power(t2,3) + 6*Power(t2,4) + 
                  Power(t1,2)*(136 - 339*t2 + 92*Power(t2,2)) + 
                  t1*(67 - 290*t2 + 28*Power(t2,2) + 42*Power(t2,3)))) + 
            Power(s1,2)*(-12 + 272*t2 - 878*Power(t2,2) + 
               1078*Power(t2,3) - 559*Power(t2,4) + 99*Power(t2,5) + 
               Power(s2,3)*t2*(-26 + 51*t2 - 6*Power(t2,2)) + 
               Power(t1,3)*(-85 + 279*t2 - 215*Power(t2,2) + 
                  24*Power(t2,3)) + 
               Power(t1,2)*(-29 + 536*t2 - 785*Power(t2,2) + 
                  233*Power(t2,3) - 12*Power(t2,4)) + 
               t1*(16 - 74*t2 + 158*Power(t2,2) - 71*Power(t2,3) - 
                  78*Power(t2,4) + 18*Power(t2,5)) + 
               Power(s2,2)*(2*t2*
                   (47 - 35*t2 - 62*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(-10 + 136*t2 - 185*Power(t2,2) + 24*Power(t2,3))) \
+ s2*(-4 + 57*t2 - 173*Power(t2,2) + 101*Power(t2,3) + 56*Power(t2,4) - 
                  6*Power(t2,5) + 
                  Power(t1,2)*
                   (32 - 226*t2 + 225*Power(t2,2) - 18*Power(t2,3)) + 
                  t1*(47 - 519*t2 + 530*Power(t2,2) + 127*Power(t2,3) - 
                     46*Power(t2,4)))) + 
            s1*(8 + (44 + 5*s2 + 16*Power(s2,2))*t2 + 
               (-616 - 58*s2 - 169*Power(s2,2) + 30*Power(s2,3))*
                Power(t2,2) + 
               (1306 + 283*s2 + 16*Power(s2,2) - 29*Power(s2,3))*
                Power(t2,3) + 
               2*(-553 - 61*s2 + 50*Power(s2,2))*Power(t2,4) + 
               (423 - 40*s2 - 8*Power(s2,2))*Power(t2,5) + 
               (-59 + 2*s2)*Power(t2,6) + 
               Power(t1,3)*(-17 + 138*t2 - 200*Power(t2,2) + 
                  68*Power(t2,3) - 3*Power(t2,4)) + 
               Power(t1,2)*(53 + 86*t2 - 573*Power(t2,2) + 
                  460*Power(t2,3) - 84*Power(t2,4) + Power(t2,5) + 
                  s2*(4 - 67*t2 + 104*Power(t2,2) - 10*Power(t2,3) - 
                     5*Power(t2,4))) - 
               t1*(26 - 84*t2 + 91*Power(t2,2) + 169*Power(t2,3) - 
                  87*Power(t2,4) - 49*Power(t2,5) + 4*Power(t2,6) + 
                  Power(s2,2)*t2*
                   (-28 + 112*t2 - 75*Power(t2,2) + 4*Power(t2,3)) + 
                  s2*(12 + 238*t2 - 873*Power(t2,2) + 436*Power(t2,3) + 
                     104*Power(t2,4) - 19*Power(t2,5))))) + 
         Power(s,2)*(4 - 36*t1 + 60*Power(t1,2) - 28*Power(t1,3) - 
            86*t2 + 52*s2*t2 + 271*t1*t2 - 112*s2*t1*t2 - 
            278*Power(t1,2)*t2 + 60*s2*Power(t1,2)*t2 + 
            93*Power(t1,3)*t2 + 307*Power(t2,2) - 208*s2*Power(t2,2) + 
            18*Power(s2,2)*Power(t2,2) - 599*t1*Power(t2,2) + 
            362*s2*t1*Power(t2,2) - 14*Power(s2,2)*t1*Power(t2,2) + 
            440*Power(t1,2)*Power(t2,2) - 
            204*s2*Power(t1,2)*Power(t2,2) - 
            76*Power(t1,3)*Power(t2,2) - 603*Power(t2,3) + 
            428*s2*Power(t2,3) - 178*Power(s2,2)*Power(t2,3) + 
            14*Power(s2,3)*Power(t2,3) + 410*t1*Power(t2,3) - 
            219*s2*t1*Power(t2,3) + 51*Power(s2,2)*t1*Power(t2,3) - 
            314*Power(t1,2)*Power(t2,3) + 
            170*s2*Power(t1,2)*Power(t2,3) + 9*Power(t1,3)*Power(t2,3) + 
            690*Power(t2,4) - 236*s2*Power(t2,4) + 
            187*Power(s2,2)*Power(t2,4) - 26*Power(s2,3)*Power(t2,4) - 
            119*t1*Power(t2,4) - 112*s2*t1*Power(t2,4) - 
            22*Power(s2,2)*t1*Power(t2,4) + 
            150*Power(t1,2)*Power(t2,4) - 
            33*s2*Power(t1,2)*Power(t2,4) + 3*Power(t1,3)*Power(t2,4) - 
            427*Power(t2,5) - 55*s2*Power(t2,5) + 
            7*Power(s2,2)*Power(t2,5) + 6*Power(s2,3)*Power(t2,5) + 
            74*t1*Power(t2,5) + 26*s2*t1*Power(t2,5) - 
            2*Power(s2,2)*t1*Power(t2,5) - 40*Power(t1,2)*Power(t2,5) - 
            s2*Power(t1,2)*Power(t2,5) + 153*Power(t2,6) + 
            21*s2*Power(t2,6) - 12*Power(s2,2)*Power(t2,6) + 
            3*t1*Power(t2,6) + 11*s2*t1*Power(t2,6) + 
            4*Power(t1,2)*Power(t2,6) - 42*Power(t2,7) - 
            2*s2*Power(t2,7) - 4*t1*Power(t2,7) + 4*Power(t2,8) + 
            Power(s1,6)*(-4*Power(s2,3) + 
               Power(s2,2)*(3 + 15*t1 - t2) + 
               t1*(3 + 7*Power(t1,2) + t1*(8 - 6*t2) - 4*t2 + 
                  Power(t2,2)) + 
               s2*(-1 - 18*Power(t1,2) + Power(t2,2) + t1*(-11 + 7*t2))) \
+ Power(s1,5)*(Power(t1,3)*(23 - 24*t2) - 
               Power(-1 + t2,2)*(-3 + 4*t2) + 
               2*Power(s2,3)*(-5 + 7*t2) + 
               Power(s2,2)*(11 + t1*(40 - 49*t2) - 21*t2 + 
                  5*Power(t2,2)) + 
               Power(t1,2)*(34 - 59*t2 + 20*Power(t2,2)) + 
               t1*(11 - 29*t2 + 22*Power(t2,2) - 4*Power(t2,3)) + 
               s2*(-2 + 3*t2 + 3*Power(t2,2) - 4*Power(t2,3) + 
                  Power(t1,2)*(-53 + 59*t2) + 
                  t1*(-41 + 72*t2 - 21*Power(t2,2)))) + 
            Power(s1,4)*(Power(s2,3)*(-11 + 43*t2 - 16*Power(t2,2)) + 
               Power(-1 + t2,2)*(21 - 50*t2 + 18*Power(t2,2)) + 
               Power(t1,3)*(53 - 112*t2 + 30*Power(t2,2)) + 
               Power(t1,2)*(113 - 273*t2 + 160*Power(t2,2) - 
                  24*Power(t2,3)) + 
               t1*(20 - 70*t2 + 89*Power(t2,2) - 45*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,2)*(24 - 64*t2 + 26*Power(t2,2) - 
                  10*Power(t2,3) + t1*(56 - 173*t2 + 56*Power(t2,2))) + 
               s2*(3 - 4*t2 + 2*Power(t2,2) - 7*Power(t2,3) + 
                  6*Power(t2,4) + 
                  Power(t1,2)*(-96 + 238*t2 - 68*Power(t2,2)) + 
                  t1*(-107 + 263*t2 - 128*Power(t2,2) + 20*Power(t2,3)))\
) + Power(s1,3)*(Power(t1,3)*
                (31 - 142*t2 + 149*Power(t2,2) - 16*Power(t2,3)) + 
               Power(s2,3)*(-6 + 51*t2 - 71*Power(t2,2) + 
                  4*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-50 + 166*t2 - 157*Power(t2,2) + 32*Power(t2,3)) + 
               Power(t1,2)*(121 - 554*t2 + 646*Power(t2,2) - 
                  185*Power(t2,3) + 12*Power(t2,4)) + 
               t1*(-14 + t2 + 56*Power(t2,2) - 83*Power(t2,3) + 
                  44*Power(t2,4) - 4*Power(t2,5)) + 
               Power(s2,2)*(37 - 189*t2 + 166*Power(t2,2) + 
                  16*Power(t2,3) + 10*Power(t2,4) - 
                  6*t1*(-2 + 27*t2 - 40*Power(t2,2) + 4*Power(t2,3))) + 
               s2*(2 - 11*t2 + 29*Power(t2,2) - 17*Power(t2,3) + 
                  Power(t2,4) - 4*Power(t2,5) + 
                  Power(t1,2)*
                   (-16 + 205*t2 - 285*Power(t2,2) + 30*Power(t2,3)) + 
                  t1*(-117 + 547*t2 - 525*Power(t2,2) + 
                     19*Power(t2,3) - 4*Power(t2,4)))) + 
            Power(s1,2)*(Power(s2,3)*t2*
                (26 - 95*t2 + 59*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(8 - 71*t2 + 144*Power(t2,2) - 
                  71*Power(t2,3) + 3*Power(t2,4)) + 
               2*Power(-1 + t2,2)*
                (8 - 123*t2 + 162*Power(t2,2) - 101*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(t1,2)*(35 - 427*t2 + 956*Power(t2,2) - 
                  640*Power(t2,3) + 102*Power(t2,4) - 2*Power(t2,5)) + 
               t1*(13 - 98*t2 + 93*Power(t2,2) - 9*Power(t2,3) + 
                  33*Power(t2,4) - 33*Power(t2,5) + Power(t2,6)) + 
               Power(s2,2)*(2 - 156*t2 + 373*Power(t2,2) - 
                  137*Power(t2,3) - 53*Power(t2,4) - 5*Power(t2,5) + 
                  t1*(2 - 69*t2 + 254*Power(t2,2) - 163*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(13 - 13*t2 + 112*Power(t2,2) - 196*Power(t2,3) + 
                  78*Power(t2,4) + 5*Power(t2,5) + Power(t2,6) + 
                  Power(t1,2)*
                   (21 - 11*t2 - 140*Power(t2,2) + 100*Power(t2,3) - 
                     2*Power(t2,4)) + 
                  t1*(-84 + 665*t2 - 1235*Power(t2,2) + 
                     521*Power(t2,3) + 88*Power(t2,4) - 3*Power(t2,5)))) \
+ s1*(-(Power(s2,3)*Power(t2,2)*
                  (34 - 81*t2 + 27*Power(t2,2) + 2*Power(t2,3))) + 
               Power(t1,3)*(-14 + 41*Power(t2,2) - 50*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(t1,2)*(47 - 145*t2 + 394*Power(t2,2) - 
                  577*Power(t2,3) + 250*Power(t2,4) - 28*Power(t2,5)) - 
               Power(-1 + t2,2)*
                (-13 + 103*t2 - 406*Power(t2,2) + 258*Power(t2,3) - 
                  126*Power(t2,4) + 16*Power(t2,5)) + 
               t1*(-46 + 130*t2 + 124*Power(t2,2) - 195*Power(t2,3) - 
                  13*Power(t2,4) - 20*Power(t2,5) + 20*Power(t2,6)) + 
               Power(s2,2)*t2*
                (-20 + 297*t2 - 395*Power(t2,2) + 17*Power(t2,3) + 
                  41*Power(t2,4) + Power(t2,5) + 
                  t1*(12 + 6*t2 - 126*Power(t2,2) + 58*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(-12 + 75*t2 - 293*Power(t2,2) + 84*Power(t2,3) + 
                  232*Power(t2,4) - 86*Power(t2,5) - 
                  Power(t1,2)*
                   (20 - 63*t2 + 19*Power(t2,2) - 16*Power(t2,3) - 
                     5*Power(t2,4) + Power(t2,5)) + 
                  t1*(32 - 38*t2 - 577*Power(t2,2) + 1003*Power(t2,3) - 
                     252*Power(t2,4) - 51*Power(t2,5) + Power(t2,6))))) + 
         s*(-8 + 24*t1 - 24*Power(t1,2) + 8*Power(t1,3) + 67*t2 - 
            16*s2*t2 - 183*t1*t2 + 32*s2*t1*t2 + 165*Power(t1,2)*t2 - 
            16*s2*Power(t1,2)*t2 - 49*Power(t1,3)*t2 - 216*Power(t2,2) + 
            101*s2*Power(t2,2) - 4*Power(s2,2)*Power(t2,2) + 
            510*t1*Power(t2,2) - 202*s2*t1*Power(t2,2) + 
            4*Power(s2,2)*t1*Power(t2,2) - 372*Power(t1,2)*Power(t2,2) + 
            101*s2*Power(t1,2)*Power(t2,2) + 78*Power(t1,3)*Power(t2,2) + 
            381*Power(t2,3) - 327*s2*Power(t2,3) + 
            79*Power(s2,2)*Power(t2,3) - 4*Power(s2,3)*Power(t2,3) - 
            567*t1*Power(t2,3) + 392*s2*t1*Power(t2,3) - 
            45*Power(s2,2)*t1*Power(t2,3) + 322*Power(t1,2)*Power(t2,3) - 
            149*s2*Power(t1,2)*Power(t2,3) - 42*Power(t1,3)*Power(t2,3) - 
            399*Power(t2,4) + 409*s2*Power(t2,4) - 
            194*Power(s2,2)*Power(t2,4) + 21*Power(s2,3)*Power(t2,4) + 
            231*t1*Power(t2,4) - 208*s2*t1*Power(t2,4) + 
            56*Power(s2,2)*t1*Power(t2,4) - 119*Power(t1,2)*Power(t2,4) + 
            68*s2*Power(t1,2)*Power(t2,4) + 7*Power(t1,3)*Power(t2,4) + 
            240*Power(t2,5) - 157*s2*Power(t2,5) + 
            120*Power(s2,2)*Power(t2,5) - 21*Power(s2,3)*Power(t2,5) - 
            26*t1*Power(t2,5) - 20*s2*t1*Power(t2,5) - 
            8*Power(s2,2)*t1*Power(t2,5) + 34*Power(t1,2)*Power(t2,5) - 
            10*s2*Power(t1,2)*Power(t2,5) - 79*Power(t2,6) - 
            16*s2*Power(t2,6) + Power(s2,2)*Power(t2,6) + 
            2*Power(s2,3)*Power(t2,6) + 7*t1*Power(t2,6) + 
            4*s2*t1*Power(t2,6) - Power(s2,2)*t1*Power(t2,6) - 
            6*Power(t1,2)*Power(t2,6) + 20*Power(t2,7) + 
            8*s2*Power(t2,7) - 2*Power(s2,2)*Power(t2,7) + 
            4*t1*Power(t2,7) + 2*s2*t1*Power(t2,7) - 6*Power(t2,8) - 
            2*s2*Power(t2,8) + 
            Power(s1,7)*Power(s2 - t1,2)*(-1 + s2 - t1 + t2) - 
            Power(s1,6)*(s2 - t1)*
             (-Power(-1 + t2,2) + Power(s2,2)*(-3 + 4*t2) + 
               Power(t1,2)*(-3 + 4*t2) + 
               t1*(-5 + 9*t2 - 4*Power(t2,2)) + 
               s2*(6 + t1*(6 - 8*t2) - 11*t2 + 5*Power(t2,2))) + 
            Power(s1,5)*(Power(s2,3)*(8 - 19*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(-19 + 40*t2 - 31*Power(t2,2) + 
                  10*Power(t2,3) + t1*(-33 + 68*t2 - 17*Power(t2,2))) + 
               s2*(-2*(-3 + t2)*Power(-1 + t2,2) + 
                  Power(t1,2)*(41 - 77*t2 + 18*Power(t2,2)) + 
                  t1*(51 - 102*t2 + 67*Power(t2,2) - 16*Power(t2,3))) - 
               2*(-2*Power(-1 + t2,3) - 
                  t1*Power(-1 + t2,2)*(-5 + 3*t2) + 
                  Power(t1,3)*(8 - 14*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (16 - 31*t2 + 18*Power(t2,2) - 3*Power(t2,3)))) + 
            Power(s1,4)*(-(Power(-1 + t2,3)*(-9 + 22*t2)) - 
               5*t1*Power(-1 + t2,2)*(1 - 8*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(3 - 31*t2 + 40*Power(t2,2)) + 
               Power(t1,3)*(3 + 33*t2 - 52*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(-45 + 184*t2 - 190*Power(t2,2) + 
                  55*Power(t2,3) - 4*Power(t2,4)) + 
               Power(s2,2)*(-36 + 135*t2 - 125*Power(t2,2) + 
                  36*Power(t2,3) - 10*Power(t2,4) + 
                  t1*(2 + 89*t2 - 135*Power(t2,2) + 8*Power(t2,3))) + 
               s2*(-(Power(-1 + t2,2)*(-9 + 34*t2)) - 
                  Power(t1,2)*
                   (8 + 91*t2 - 147*Power(t2,2) + 12*Power(t2,3)) + 
                  2*t1*(31 - 131*t2 + 129*Power(t2,2) - 36*Power(t2,3) + 
                     7*Power(t2,4)))) + 
            Power(s1,3)*(Power(-1 + t2,3)*
                (29 - 32*t2 + 44*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (-15 + 22*t2 - 50*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,3)*(4 - 30*t2 + 66*Power(t2,2) - 
                  38*Power(t2,3) - 5*Power(t2,4)) - 
               Power(t1,3)*(-6 + 6*t2 + 30*Power(t2,2) - 
                  34*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(-56 + 283*t2 - 458*Power(t2,2) + 
                  269*Power(t2,3) - 39*Power(t2,4) + Power(t2,5)) + 
               Power(s2,2)*(-33 + 190*t2 - 309*Power(t2,2) + 
                  162*Power(t2,3) - 15*Power(t2,4) + 5*Power(t2,5) + 
                  t1*(-1 + 50*t2 - 163*Power(t2,2) + 120*Power(t2,3) + 
                     3*Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (18 - 25*t2 + 38*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(t1,2)*
                   (-20 + 19*t2 + 94*Power(t2,2) - 105*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  t1*(86 - 416*t2 + 614*Power(t2,2) - 284*Power(t2,3) + 
                     6*Power(t2,4) - 6*Power(t2,5)))) + 
            Power(s1,2)*(-(Power(-1 + t2,3)*
                  (-10 + 93*t2 - 35*Power(t2,2) + 44*Power(t2,3))) + 
               Power(t1,3)*(-8 - 12*t2 + 30*Power(t2,2) + 
                  6*Power(t2,3) - 7*Power(t2,4)) + 
               Power(s2,3)*t2*
                (-12 + 72*t2 - 92*Power(t2,2) + 19*Power(t2,3) + 
                  4*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (-12 - 161*t2 + 60*Power(t2,2) - 32*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(t1,2)*(6 + 34*t2 - 324*Power(t2,2) + 
                  439*Power(t2,3) - 169*Power(t2,4) + 14*Power(t2,5)) - 
               Power(s2,2)*(4 - 145*t2 + 466*Power(t2,2) - 
                  409*Power(t2,3) + 78*Power(t2,4) + 5*Power(t2,5) + 
                  Power(t2,6) + 
                  t1*(-4 + 43*t2 + 50*Power(t2,2) - 183*Power(t2,3) + 
                     63*Power(t2,4) + 4*Power(t2,5))) + 
               s2*(-(Power(-1 + t2,2)*
                     (1 + 147*t2 - 9*Power(t2,2) - 6*Power(t2,3) + 
                       7*Power(t2,4))) + 
                  Power(t1,2)*
                   (-1 + 109*t2 - 148*Power(t2,2) - 11*Power(t2,3) + 
                     24*Power(t2,4)) + 
                  t1*(2 - 216*t2 + 842*Power(t2,2) - 822*Power(t2,3) + 
                     167*Power(t2,4) + 26*Power(t2,5) + Power(t2,6)))) + 
            s1*(Power(t1,3)*(11 + 2*t2 - 20*Power(t2,2) - 
                  2*Power(t2,3) + Power(t2,4)) - 
               Power(s2,3)*Power(t2,2)*
                (-12 + 66*t2 - 70*Power(t2,2) + 7*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(-1 + t2,3)*
                (29 - 67*t2 + 119*Power(t2,2) - 14*Power(t2,3) + 
                  24*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (69 - 168*t2 - 164*Power(t2,2) + 28*Power(t2,3) - 
                  16*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,2)*(-51 + 150*t2 - 96*Power(t2,2) + 
                  109*Power(t2,3) - 149*Power(t2,4) + 39*Power(t2,5) - 
                  2*Power(t2,6)) + 
               Power(s2,2)*t2*
                (8 - 191*t2 + 506*Power(t2,2) - 336*Power(t2,3) + 
                  6*Power(t2,4) + 7*Power(t2,5) + 
                  t1*(-8 + 89*t2 - 58*Power(t2,2) - 68*Power(t2,3) + 
                     20*Power(t2,4) + Power(t2,5))) + 
               s2*(Power(t1,2)*
                   (16 - 100*t2 + 60*Power(t2,2) + 69*Power(t2,3) - 
                     23*Power(t2,4) + 2*Power(t2,5)) + 
                  Power(-1 + t2,2)*
                   (16 - 68*t2 + 302*Power(t2,2) + 13*Power(t2,3) - 
                     20*Power(t2,4) + 6*Power(t2,5)) - 
                  t1*(32 - 200*t2 + 262*Power(t2,2) + 280*Power(t2,3) - 
                     439*Power(t2,4) + 54*Power(t2,5) + 11*Power(t2,6)))))\
)*B1(s,1 - s + s1 - t2,s1))/
     (s*(-1 + s1)*(-1 + s2)*(-1 + t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) + 
    (8*(-2*Power(-1 + s,3)*s*(1 - 3*s + Power(s,2))*Power(t2,2)*(1 + t2)*
          Power(-1 + s + t2,4) + 
         Power(s1,10)*Power(s2 - t1,2)*
          (2*Power(s,2)*(-1 + s2 - t1 + t2) - 
            Power(-1 + t2,2)*(-2 + 3*s2 - 3*t1 + 2*t2) + 
            s*(-1 + t2)*(-12 + 5*s2 - 5*t1 + 12*t2)) + 
         (-1 + s)*s*s1*t2*Power(-1 + s + t2,3)*
          (4*Power(s,6)*(-1 + t2)*t2 - 
            Power(s,4)*(4 + (113 - 36*s2 + 25*t1)*t2 - 
               (99 + 29*s2)*Power(t2,2) + 4*(6 + s2)*Power(t2,3)) + 
            Power(s,3)*(20 + (240 - 110*s2 + 67*t1)*t2 - 
               (151 + 66*s2 + 8*t1)*Power(t2,2) + 
               (46 + 30*s2 - 2*t1)*Power(t2,3) - 2*Power(t2,4)) + 
            2*(-2 + (-24 + 7*s2 - 2*t1)*t2 + 2*(5 + t1)*Power(t2,2) - 
               (-15 + 7*s2 + t1)*Power(t2,3) + (1 + s2)*Power(t2,4)) + 
            Power(s,2)*(-32 + (-319 + 142*s2 - 75*t1)*t2 + 
               (109 + 55*s2 + 20*t1)*Power(t2,2) + 
               (-6 - 66*s2 + 2*t1)*Power(t2,3) + 2*(3 + s2)*Power(t2,4)) \
- 2*s*(-10 + (-106 + 39*s2 - 17*t1)*t2 + 
               (35 + 7*s2 + 8*t1)*Power(t2,2) - 
               (-35 + 27*s2 + t1)*Power(t2,3) + (3 + 2*s2)*Power(t2,4)) \
+ Power(s,5)*t2*(32 + 3*t1 - 31*t2 + 4*Power(t2,2) - 4*s2*(1 + t2))) - 
         Power(s1,2)*Power(-1 + s + t2,2)*
          (-(Power(-1 + t1 + t2 - s2*t2,2)*
               (-4 + 19*t2 + 3*(-5 + s2)*Power(t2,2) + 
                 t1*(4 - 9*t2 + 2*Power(t2,2)))) + 
            Power(s,8)*(2*Power(t1,3) + 13*t1*Power(t2,2) + 
               t2*(8 - 2*(17 + 6*s2)*t2 + 19*Power(t2,2))) + 
            Power(s,6)*(2 + (294 - 86*s2)*t2 - 
               (837 + 62*s2 + 120*Power(s2,2))*Power(t2,2) + 
               (615 + 170*s2 + 12*Power(s2,2))*Power(t2,3) - 
               (170 + 19*s2)*Power(t2,4) + 14*Power(t2,5) + 
               Power(t1,3)*(38 - 25*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-36 + (2 - 4*s2)*t2 + 
                  (-11 + 6*s2)*Power(t2,2)) + 
               t1*(6 + (20 + 34*s2)*t2 - 
                  2*(-1 - 56*s2 + Power(s2,2))*Power(t2,2) - 
                  (73 + 16*s2)*Power(t2,3) + 2*Power(t2,4))) - 
            Power(s,5)*(10 + (724 - 318*s2)*t2 + 
               (-1535 + 464*s2 - 440*Power(s2,2))*Power(t2,2) + 
               (1143 + 338*s2 + 114*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) + 
               (-341 - 155*s2 + 4*Power(s2,2))*Power(t2,4) + 
               (40 + 6*s2)*Power(t2,5) + 
               Power(t1,3)*(46 - 56*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-78 + (-5 + 18*s2)*t2 + 
                  (-68 + 27*s2)*Power(t2,2) - 4*(-2 + s2)*Power(t2,3)) \
+ 2*t1*(15 + (60 + 26*s2)*t2 + 
                  (-236 + 215*s2 - 13*Power(s2,2))*Power(t2,2) + 
                  (5 - 66*s2)*Power(t2,3) + 2*(10 + s2)*Power(t2,4))) + 
            Power(s,4)*(18 - 24*(-47 + 22*s2)*t2 - 
               2*(859 - 630*s2 + 356*Power(s2,2))*Power(t2,2) + 
               (983 + 98*s2 + 336*Power(s2,2) + 16*Power(s2,3))*
                Power(t2,3) - 
               (216 + 382*s2 - 9*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,4) + (6 + 48*s2 - 4*Power(s2,2))*Power(t2,5) + 
               2*Power(t2,6) + 
               5*Power(t1,3)*(2 - 9*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-60 + (-85 + 80*s2)*t2 + 
                  (-125 + 27*s2)*Power(t2,2) - 
                  4*(-7 + 3*s2)*Power(t2,3)) + 
               t1*(48 + (313 - 28*s2)*t2 + 
                  (-1093 + 862*s2 - 88*Power(s2,2))*Power(t2,2) + 
                  (355 - 362*s2 + 9*Power(s2,2))*Power(t2,3) + 
                  2*(25 + 18*s2 + Power(s2,2))*Power(t2,4) - 
                  12*Power(t2,5))) + 
            s*(-12 + (7 - 14*s2)*t2 + 
               (-218 + 203*s2 - 20*Power(s2,2))*Power(t2,2) + 
               (399 - 328*s2 + 111*Power(s2,2) - 10*Power(s2,3))*
                Power(t2,3) + 
               (-114 + 107*s2 - 62*Power(s2,2) + 13*Power(s2,3))*
                Power(t2,4) + 
               (-60 + 38*s2 - 6*Power(s2,2))*Power(t2,5) - 
               2*(1 + 3*s2)*Power(t2,6) + 
               Power(t1,3)*(22 - 40*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-54 + (193 - 46*s2)*t2 + 
                  (-124 + 81*s2)*Power(t2,2) + (8 - 12*s2)*Power(t2,3)) \
+ 2*t1*(21 + (-131 + 44*s2)*t2 + 
                  (188 - 167*s2 + 17*Power(s2,2))*Power(t2,2) + 
                  (-76 + 106*s2 - 27*Power(s2,2))*Power(t2,3) + 
                  2*(1 - 5*s2 + Power(s2,2))*Power(t2,4) + 
                  4*(-1 + s2)*Power(t2,5))) + 
            Power(s,3)*(-18 + Power(t1,3)*(38 - 20*t2) + 
               (-995 + 408*s2)*t2 + 
               2*(531 - 534*s2 + 256*Power(s2,2))*Power(t2,2) - 
               (148 - 191*s2 + 317*Power(s2,2) + 36*Power(s2,3))*
                Power(t2,3) + 
               (-230 + 376*s2 + 10*Power(s2,2) + 11*Power(s2,3))*
                Power(t2,4) + 
               2*(45 - 40*s2 + 7*Power(s2,2))*Power(t2,5) + 
               2*(-3 + s2)*Power(t2,6) + 
               2*Power(t1,2)*
                (-15 + (125 - 65*s2)*t2 + (20 + 21*s2)*Power(t2,2) + 
                  4*(-4 + s2)*Power(t2,3)) + 
               t1*(-12 + 2*(-241 + 81*s2)*t2 + 
                  (1191 - 1016*s2 + 132*Power(s2,2))*Power(t2,2) + 
                  (-505 + 488*s2 - 42*Power(s2,2))*Power(t2,3) - 
                  4*(1 + 18*s2 + Power(s2,2))*Power(t2,4) + 
                  8*s2*Power(t2,5))) + 
            Power(s,2)*(16 + (381 - 114*s2)*t2 + 
               (-104 + 119*s2 - 116*Power(s2,2))*Power(t2,2) + 
               (-494 + 158*s2 - 3*Power(s2,2) + 32*Power(s2,3))*
                Power(t2,3) + 
               (273 - 156*s2 + 26*Power(s2,2) - 19*Power(s2,3))*
                Power(t2,4) + 
               (-78 + 32*s2 - 4*Power(s2,2))*Power(t2,5) + 
               (6 + 4*s2)*Power(t2,6) + 
               Power(t1,3)*(-46 + 61*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(84 + 4*(-80 + 27*s2)*t2 + 
                  (115 - 108*s2)*Power(t2,2) + 8*(1 + s2)*Power(t2,3)) \
+ t1*(-42 - 26*(-18 + 7*s2)*t2 + 
                  (-806 + 748*s2 - 98*Power(s2,2))*Power(t2,2) + 
                  (327 - 396*s2 + 72*Power(s2,2))*Power(t2,3) + 
                  2*(-5 + 28*s2)*Power(t2,4) - 4*(-5 + 4*s2)*Power(t2,5)\
)) + Power(s,7)*(2*Power(t1,3)*(-7 + 2*t2) + 2*Power(t1,2)*(3 + s2*t2) - 
               3*t1*t2*((21 - 5*t2)*t2 + s2*(2 + 4*t2)) + 
               t2*(-72 + 257*t2 + 12*Power(s2,2)*t2 - 166*Power(t2,2) + 
                  33*Power(t2,3) + s2*(8 + 73*t2 - 25*Power(t2,2))))) + 
         Power(s1,9)*(Power(s2,3)*
             (-10*Power(s,3) + Power(s,2)*(15 - 28*t2) + 
               2*Power(-1 + t2,2)*(5 + 4*t2) + 
               s*(39 - 59*t2 + 20*Power(t2,2))) + 
            Power(s2,2)*(4*Power(s,3)*(2 + 9*t1 - t2) + 
               Power(-1 + t2,2)*
                (1 - 7*t2 + 6*Power(t2,2) - 3*t1*(11 + 7*t2)) - 
               s*(-1 + t2)*(-71 + 70*t2 + Power(t2,2) + 
                  3*t1*(-35 + 16*t2)) + 
               Power(s,2)*(t1*(-60 + 99*t2) - 
                  13*(4 - 9*t2 + 5*Power(t2,2)))) + 
            t1*(2*Power(s,3)*
                (4 + 9*t1 + 8*Power(t1,2) - 6*t2 - 7*t1*t2 + 
                  2*Power(t2,2)) + 
               Power(s,2)*(-30*Power(-1 + t2,2) + 
                  Power(t1,2)*(-30 + 43*t2) + 
                  t1*(-76 + 165*t2 - 89*Power(t2,2))) + 
               Power(-1 + t2,2)*
                (4*Power(-1 + t2,2) - Power(t1,2)*(13 + 5*t2) + 
                  t1*(-3 + t2 + 2*Power(t2,2))) - 
               s*(-1 + t2)*(2*Power(-1 + t2,2)*(3 + 2*t2) + 
                  Power(t1,2)*(-27 + 8*t2) + 
                  t1*(-53 + 34*t2 + 19*Power(t2,2)))) + 
            s2*(-2*Power(s,3)*
                (2 + 21*Power(t1,2) + t1*(13 - 9*t2) - 2*t2) + 
               2*Power(-1 + t2,2)*
                (-2*Power(-1 + t2,2) + 9*Power(t1,2)*(2 + t2) + 
                  t1*(1 + 3*t2 - 4*Power(t2,2))) + 
               s*(-1 + t2)*(2*Power(-1 + t2,2)*(1 + 4*t2) + 
                  Power(t1,2)*(-93 + 36*t2) + 
                  4*t1*(-31 + 26*t2 + 5*Power(t2,2))) + 
               Power(s,2)*(Power(t1,2)*(75 - 114*t2) + 
                  2*Power(-1 + t2,2)*(11 + 4*t2) + 
                  2*t1*(64 - 141*t2 + 77*Power(t2,2))))) + 
         Power(s1,8)*(Power(s,4)*
             (2 + 20*Power(s2,3) - 56*Power(t1,3) - 6*t2 + 
               4*Power(t2,2) - 2*Power(s2,2)*(6 + 45*t1 + 5*t2) + 
               6*Power(t1,2)*(-11 + 7*t2) + 
               t1*(-45 + 80*t2 - 37*Power(t2,2)) + 
               s2*(19 + 126*Power(t1,2) + t1*(70 - 24*t2) - 30*t2 + 
                  13*Power(t2,2))) + 
            Power(s,3)*(Power(t1,3)*(93 - 161*t2) + 
               2*Power(-1 + t2,2)*(3 + 4*t2) + 
               Power(s2,3)*(-25 + 66*t2) + 
               Power(s2,2)*(106 + t1*(122 - 272*t2) - 277*t2 + 
                  146*Power(t2,2)) + 
               Power(t1,2)*(206 - 513*t2 + 282*Power(t2,2)) + 
               t1*(117 - 325*t2 + 240*Power(t2,2) - 32*Power(t2,3)) + 
               s2*(-75 + 187*t2 - 90*Power(t2,2) - 22*Power(t2,3) + 
                  Power(t1,2)*(-190 + 367*t2) + 
                  t1*(-290 + 746*t2 - 406*Power(t2,2)))) - 
            Power(-1 + t2,2)*
             (2*Power(-1 + t2,3) + t1*Power(-1 + t2,2)*(11 + 4*t2) - 
               Power(t1,2)*(6 - 7*t2 + Power(t2,2)) + 
               6*Power(s2,3)*(2 + 4*t2 + Power(t2,2)) - 
               Power(t1,3)*(19 + 21*t2 + 2*Power(t2,2)) - 
               Power(s2,2)*(-6 + 11*t2 + Power(t2,2) - 6*Power(t2,3) + 
                  t1*(46 + 63*t2 + 17*Power(t2,2))) + 
               s2*(-(Power(-1 + t2,2)*(7 + 8*t2)) + 
                  Power(t1,2)*(53 + 60*t2 + 13*Power(t2,2)) - 
                  2*t1*(1 - 5*t2 + 2*Power(t2,2) + 2*Power(t2,3)))) - 
            s*(-1 + t2)*(2*Power(-1 + t2,3)*(2 + t2) + 
               Power(t1,3)*(23 + 12*t2 - 20*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(67 + 26*t2 + 4*Power(t2,2)) + 
               5*Power(s2,3)*(-9 - 5*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(19 + 126*t2 - 132*Power(t2,2) - 
                  13*Power(t2,3)) + 
               s2*(Power(-1 + t2,2)*(73 + 14*t2 + 10*Power(t2,2)) + 
                  Power(t1,2)*(-76 - 79*t2 + 110*Power(t2,2)) - 
                  2*t1*(28 + 129*t2 - 165*Power(t2,2) + 8*Power(t2,3))) \
+ Power(s2,2)*(t1*(98 + 92*t2 - 145*Power(t2,2)) + 
                  3*(13 + 42*t2 - 64*Power(t2,2) + 9*Power(t2,3)))) - 
            Power(s,2)*(-2*Power(-1 + t2,3)*(3 + t2) - 
               t1*Power(-1 + t2,2)*(90 + 110*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(-53 + 53*t2 + 27*Power(t2,2)) + 
               Power(s2,3)*(70 - 160*t2 + 63*Power(t2,2)) + 
               Power(t1,2)*(-141 + 186*t2 + 112*Power(t2,2) - 
                  157*Power(t2,3)) - 
               Power(s2,2)*(179 - 380*t2 + 162*Power(t2,2) + 
                  39*Power(t2,3) + t1*(220 - 427*t2 + 126*Power(t2,2))) \
+ s2*(Power(t1,2)*(203 - 320*t2 + 36*Power(t2,2)) + 
                  Power(-1 + t2,2)*(108 + 56*t2 + 45*Power(t2,2)) + 
                  2*t1*(169 - 310*t2 + 52*Power(t2,2) + 89*Power(t2,3))))\
) - Power(s1,7)*(Power(s,5)*(12 + 20*Power(s2,3) - 112*Power(t1,3) - 
               31*t2 + 18*Power(t2,2) + Power(t2,3) - 
               8*Power(s2,2)*(1 + 15*t1 + 5*t2) + 
               10*Power(t1,2)*(-13 + 7*t2) - 
               2*t1*(50 - 110*t2 + 69*Power(t2,2)) + 
               s2*(45 + 210*Power(t1,2) - 104*t2 + 77*Power(t2,2) + 
                  10*t1*(10 + t2))) + 
            Power(s,4)*(3 + Power(t1,3)*(208 - 343*t2) - 12*t2 - 
               14*Power(t2,2) + 19*Power(t2,3) + 4*Power(t2,4) + 
               7*Power(s2,3)*(-5 + 12*t2) + 
               Power(s2,2)*(122 + t1*(188 - 400*t2) - 385*t2 + 
                  184*Power(t2,2)) + 
               Power(t1,2)*(334 - 930*t2 + 495*Power(t2,2)) + 
               t1*(213 - 802*t2 + 791*Power(t2,2) - 201*Power(t2,3)) + 
               s2*(-149 + 512*t2 - 420*Power(t2,2) + 56*Power(t2,3) + 
                  Power(t1,2)*(-350 + 648*t2) - 
                  2*t1*(175 - 539*t2 + 274*Power(t2,2)))) + 
            Power(s,3)*(Power(t1,3)*(-15 + 115*t2 - 162*Power(t2,2)) + 
               Power(s2,3)*(-53 + 192*t2 - 123*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (67 + 74*t2 - 28*Power(t2,2) + 5*Power(t2,3)) + 
               2*Power(t1,2)*
                (25 + 52*t2 - 304*Power(t2,2) + 215*Power(t2,3)) + 
               t1*(133 - 93*t2 - 560*Power(t2,2) + 580*Power(t2,3) - 
                  60*Power(t2,4)) + 
               Power(s2,2)*(137 - 385*t2 + 85*Power(t2,2) + 
                  139*Power(t2,3) + 
                  2*t1*(78 - 241*t2 + 116*Power(t2,2))) + 
               s2*(-140 + 247*t2 + 170*Power(t2,2) - 234*Power(t2,3) - 
                  43*Power(t2,4) + 
                  Power(t1,2)*(-106 + 211*t2 + 35*Power(t2,2)) + 
                  t1*(-288 + 634*t2 + 120*Power(t2,2) - 418*Power(t2,3))\
)) - Power(-1 + t2,2)*(Power(-1 + t2,3)*(5 + 2*t2) + 
               t1*Power(-1 + t2,2)*(31 + 9*t2) + 
               6*Power(s2,3)*(1 + 4*t2 + 2*Power(t2,2)) - 
               3*Power(t1,3)*(3 + 7*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(-43 + 45*t2 + 2*Power(t2,2) - 
                  4*Power(t2,3)) + 
               2*s2*(-(Power(-1 + t2,2)*(9 + 9*t2 + 2*Power(t2,2))) - 
                  t1*(-18 + 17*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (20 + 19*t2 + 22*Power(t2,2) + 2*Power(t2,3))) - 
               Power(s2,2)*(4 + 14*t2 - 5*Power(t2,2) - 
                  11*Power(t2,3) - 2*Power(t2,4) + 
                  t1*(34 + 50*t2 + 35*Power(t2,2) + 7*Power(t2,3)))) - 
            Power(s,2)*(Power(t1,3)*
                (41 - 124*t2 + 113*Power(t2,2) - 17*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (108 + 21*t2 - 45*Power(t2,2) + 2*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (-50 + 527*t2 + 201*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,3)*(-61 + 182*t2 - 296*Power(t2,2) + 
                  162*Power(t2,3)) + 
               Power(t1,2)*(193 - 740*t2 + 965*Power(t2,2) - 
                  325*Power(t2,3) - 93*Power(t2,4)) + 
               Power(s2,2)*(162 - 740*t2 + 1165*Power(t2,2) - 
                  601*Power(t2,3) + 14*Power(t2,4) + 
                  t1*(138 - 536*t2 + 876*Power(t2,2) - 439*Power(t2,3))\
) + s2*(Power(-1 + t2,2)*(-15 + 530*t2 + 148*Power(t2,2) + 
                     18*Power(t2,3)) + 
                  Power(t1,2)*
                   (-126 + 502*t2 - 717*Power(t2,2) + 302*Power(t2,3)) \
+ 2*t1*(-201 + 844*t2 - 1236*Power(t2,2) + 587*Power(t2,3) + 
                     6*Power(t2,4)))) + 
            s*(-1 + t2)*(-3*Power(-1 + t2,3)*
                (-7 + 5*t2 + 4*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(69 + 220*t2 + 42*Power(t2,2)) + 
               Power(t1,3)*(49 - 52*t2 + 24*Power(t2,2) + 
                  8*Power(t2,3)) - 
               Power(s2,3)*(13 - 37*t2 + 18*Power(t2,2) + 
                  35*Power(t2,3)) + 
               Power(t1,2)*(56 - 125*t2 - 82*Power(t2,2) + 
                  147*Power(t2,3) + 4*Power(t2,4)) + 
               Power(s2,2)*(45 - 149*t2 - 20*Power(t2,2) + 
                  139*Power(t2,3) - 15*Power(t2,4) + 
                  3*t1*(40 - 60*t2 + 11*Power(t2,2) + 38*Power(t2,3))) + 
               s2*(Power(-1 + t2,2)*
                   (-115 - 186*t2 - 34*Power(t2,2) + 4*Power(t2,3)) - 
                  Power(t1,2)*
                   (160 - 207*t2 + 51*Power(t2,2) + 83*Power(t2,3)) + 
                  2*t1*(-38 + 97*t2 + 96*Power(t2,2) - 163*Power(t2,3) + 
                     8*Power(t2,4))))) + 
         Power(s1,6)*(Power(s,6)*
             (28 + 10*Power(s2,3) - 140*Power(t1,3) - 81*t2 + 
               50*Power(t2,2) + Power(t2,3) + 
               10*Power(t1,2)*(-15 + 7*t2) - 
               2*Power(s2,2)*(1 + 45*t1 + 25*t2) - 
               5*t1*(22 - 64*t2 + 55*Power(t2,2)) + 
               s2*(61 + 210*Power(t1,2) - 196*t2 + 190*Power(t2,2) + 
                  20*t1*(4 + 3*t2))) + 
            Power(s,5)*(-41 + 88*t2 - 80*Power(t2,2) + 33*Power(t2,3) + 
               9*Power(t2,4) + Power(s2,3)*(-30 + 61*t2) - 
               5*Power(t1,3)*(-69 + 91*t2) + 
               10*Power(t1,2)*(37 - 105*t2 + 52*Power(t2,2)) + 
               Power(s2,2)*(62 + t1*(207 - 335*t2) - 285*t2 + 
                  144*Power(t2,2)) + 
               t1*(248 - 1198*t2 + 1465*Power(t2,2) - 
                  525*Power(t2,3)) + 
               s2*(-224 + 928*t2 - 1012*Power(t2,2) + 317*Power(t2,3) + 
                  25*Power(t1,2)*(-19 + 27*t2) + 
                  t1*(-224 + 818*t2 - 360*Power(t2,2)))) + 
            Power(s,4)*(103 - 316*t2 - 19*Power(t2,2) + 
               408*Power(t2,3) - 193*Power(t2,4) + 17*Power(t2,5) + 
               Power(t1,3)*(-223 + 565*t2 - 335*Power(t2,2)) + 
               Power(s2,3)*(-21 + 134*t2 - 170*Power(t2,2)) + 
               Power(t1,2)*(-212 + 852*t2 - 1211*Power(t2,2) + 
                  600*Power(t2,3)) + 
               t1*(30 + 302*t2 - 1465*Power(t2,2) + 1503*Power(t2,3) - 
                  295*Power(t2,4)) + 
               Power(s2,2)*(69 - 250*t2 + 39*Power(t2,2) + 
                  223*Power(t2,3) + 15*t1*(-3 - 8*t2 + 21*Power(t2,2))) \
+ s2*(69 - 334*t2 + 1059*Power(t2,2) - 1020*Power(t2,3) + 
                  151*Power(t2,4) + 
                  Power(t1,2)*(213 - 398*t2 + 85*Power(t2,2)) + 
                  t1*(-52 + 250*t2 + 84*Power(t2,2) - 392*Power(t2,3)))) \
+ Power(-1 + t2,3)*(-(Power(-1 + t2,2)*(14 + 5*t2)) + 
               5*Power(t1,3)*(-1 + 6*t2) + 
               5*Power(t1,2)*(-18 - 15*t2 + 4*Power(t2,2)) + 
               Power(s2,3)*(1 + 9*t2 + 9*Power(t2,2) + Power(t2,3)) + 
               t1*(82 - 47*t2 - 37*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-44 - 16*t2 + 49*Power(t2,2) + 11*Power(t2,3) - 
                  5*Power(t1,2)*(-7 + 9*t2 + 4*Power(t2,2)) + 
                  t1*(64 + 208*t2 + 22*Power(t2,2) - 4*Power(t2,3))) + 
               Power(s2,2)*(-8 - 76*t2 - 54*Power(t2,2) - 
                  7*Power(t2,3) + 
                  t1*(-21 - 11*t2 + 15*Power(t2,2) + 2*Power(t2,3)))) + 
            Power(s,3)*(Power(s2,3)*
                (152 - 566*t2 + 657*Power(t2,2) - 296*Power(t2,3)) - 
               2*Power(t1,3)*
                (23 - 50*t2 - 6*Power(t2,2) + 20*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (32 + 531*t2 + 183*Power(t2,2) - 257*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(t1,2)*(-290 + 1083*t2 - 1136*Power(t2,2) + 
                  192*Power(t2,3) + 202*Power(t2,4)) + 
               t1*(-510 + 1748*t2 - 2159*Power(t2,2) + 
                  329*Power(t2,3) + 639*Power(t2,4) - 47*Power(t2,5)) + 
               s2*(357 - 1284*t2 + 1815*Power(t2,2) - 
                  419*Power(t2,3) - 518*Power(t2,4) + 49*Power(t2,5) + 
                  t1*(900 - 3538*t2 + 4638*Power(t2,2) - 
                     2102*Power(t2,3)) + 
                  Power(t1,2)*
                   (234 - 874*t2 + 1021*Power(t2,2) - 486*Power(t2,3))) \
+ Power(s2,2)*(-587 + 2009*t2 - 2410*Power(t2,2) + 972*Power(t2,3) + 
                  67*Power(t2,4) + 
                  2*t1*(-142 + 570*t2 - 729*Power(t2,2) + 
                     367*Power(t2,3)))) + 
            s*(-1 + t2)*(2*t1*Power(-1 + t2,2)*
                (15 + 89*t2 + 135*Power(t2,2) + 12*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (-55 - 59*t2 + 33*Power(t2,2) + 14*Power(t2,3)) + 
               Power(t1,3)*(91 - 8*t2 - 52*Power(t2,2) + 
                  24*Power(t2,3)) - 
               Power(s2,3)*(62 + 7*t2 + 30*Power(t2,2) - 
                  54*Power(t2,3) + 10*Power(t2,4)) + 
               Power(t1,2)*(32 + 317*t2 - 555*Power(t2,2) + 
                  166*Power(t2,3) + 40*Power(t2,4)) + 
               Power(s2,2)*(127 - 143*t2 + 90*Power(t2,2) - 
                  129*Power(t2,3) + 60*Power(t2,4) - 5*Power(t2,5) + 
                  t1*(219 + 172*t2 - 320*Power(t2,2) + 
                     58*Power(t2,3) + 36*Power(t2,4))) - 
               s2*(-(Power(-1 + t2,2)*
                     (-155 - 125*t2 - 216*Power(t2,2) - 
                       12*Power(t2,3) + 6*Power(t2,4))) + 
                  Power(t1,2)*
                   (273 + 88*t2 - 345*Power(t2,2) + 129*Power(t2,3) + 
                     20*Power(t2,4)) - 
                  2*t1*(-30 - 210*t2 + 305*Power(t2,2) + 
                     7*Power(t2,3) - 74*Power(t2,4) + 2*Power(t2,5)))) + 
            Power(s,2)*(Power(s2,3)*
                (20 + 144*t2 - 461*Power(t2,2) + 388*Power(t2,3) - 
                  116*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (-262 + 229*t2 - 1040*Power(t2,2) - 185*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(-1 + t2,3)*
                (191 + 361*t2 - 31*Power(t2,2) - 111*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,3)*(-42 + 88*t2 + 14*Power(t2,2) - 
                  45*Power(t2,3) + 10*Power(t2,4)) + 
               Power(t1,2)*(-256 + 229*t2 + 578*Power(t2,2) - 
                  948*Power(t2,3) + 381*Power(t2,4) + 16*Power(t2,5)) + 
               Power(s2,2)*(97 - 850*t2 + 1842*Power(t2,2) - 
                  1670*Power(t2,3) + 584*Power(t2,4) - 3*Power(t2,5) + 
                  t1*(-166 + 248*t2 + 348*Power(t2,2) - 
                     653*Power(t2,3) + 298*Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (-270 + 50*t2 - 901*Power(t2,2) - 166*Power(t2,3) + 
                     31*Power(t2,4)) - 
                  3*Power(t1,2)*
                   (-52 + 120*t2 + 23*Power(t2,2) - 138*Power(t2,3) + 
                     72*Power(t2,4)) + 
                  2*t1*(130 + 233*t2 - 1311*Power(t2,2) + 
                     1572*Power(t2,3) - 648*Power(t2,4) + 24*Power(t2,5))\
))) - Power(s1,5)*(Power(s,7)*
             (32 + 2*Power(s2,3) - 112*Power(t1,3) - 125*t2 + 
               104*Power(t2,2) - 10*Power(t2,3) + 
               6*Power(t1,2)*(-17 + 7*t2) - 
               4*Power(s2,2)*(9*t1 + 7*t2) - 
               20*t1*(3 - 13*t2 + 16*Power(t2,2)) + 
               s2*(43 + 126*Power(t1,2) - 204*t2 + 250*Power(t2,2) + 
                  t1*(34 + 66*t2))) + 
            Power(s,6)*(-107 + 373*t2 - 407*Power(t2,2) + 
               160*Power(t2,3) - 20*Power(t2,4) + 
               2*Power(s2,3)*(-5 + 12*t2) - 
               5*Power(t1,3)*(-78 + 77*t2) + 
               Power(s2,2)*(-6 + t1*(128 - 155*t2) - 64*t2 + 
                  53*Power(t2,2)) + 
               Power(t1,2)*(272 - 729*t2 + 327*Power(t2,2)) + 
               t1*(166 - 1132*t2 + 1697*Power(t2,2) - 
                  730*Power(t2,3)) + 
               s2*(-211 + 1088*t2 - 1435*Power(t2,2) + 
                  552*Power(t2,3) + 5*Power(t1,2)*(-83 + 82*t2) + 
                  t1*(-56 + 230*t2 - 34*Power(t2,2)))) + 
            Power(s,5)*(150 - 764*t2 + 874*Power(t2,2) + 
               32*Power(t2,3) - 220*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,3)*(-459 + 939*t2 - 372*Power(t2,2)) + 
               Power(s2,3)*(-2 + 73*t2 - 165*Power(t2,2)) + 
               Power(t1,2)*(-279 + 1237*t2 - 1193*Power(t2,2) + 
                  463*Power(t2,3)) + 
               t1*(-28 + 730*t2 - 2299*Power(t2,2) + 
                  2321*Power(t2,3) - 540*Power(t2,4)) + 
               s2*(175 - 1120*t2 + 2312*Power(t2,2) - 
                  1971*Power(t2,3) + 414*Power(t2,4) + 
                  Power(t1,2)*(451 - 809*t2 + 26*Power(t2,2)) - 
                  4*t1*(19 - 55*t2 + 156*Power(t2,2) + 6*Power(t2,3))) \
+ Power(s2,2)*(t1*(-149 + 179*t2 + 284*Power(t2,2)) + 
                  4*(37 - 106*t2 + 99*Power(t2,2) + 33*Power(t2,3)))) + 
            Power(-1 + t2,2)*
             (-2*Power(-1 + t2,3)*(14 + 9*t2) + 
               6*Power(s2,3)*Power(t2,2)*(2 + 4*t2 + Power(t2,2)) + 
               2*t1*Power(-1 + t2,2)*(-49 - 65*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(23 - 105*t2 + 40*Power(t2,2)) + 
               Power(t1,2)*(57 + 173*t2 - 270*Power(t2,2) + 
                  40*Power(t2,3)) - 
               2*s2*(-(Power(-1 + t2,2)*
                     (17 + 72*t2 + 21*Power(t2,2))) + 
                  Power(t1,2)*
                   (22 - 65*t2 - 40*Power(t2,2) + 20*Power(t2,3)) + 
                  t1*(5 + 171*t2 - 102*Power(t2,2) - 82*Power(t2,3) + 
                     8*Power(t2,4))) + 
               Power(s2,2)*(-3 + 83*t2 + 60*Power(t2,2) - 
                  110*Power(t2,3) - 30*Power(t2,4) + 
                  t1*(13 - 15*t2 - 114*Power(t2,2) - 18*Power(t2,3) + 
                     8*Power(t2,4)))) + 
            Power(s,4)*(-255 + 1005*t2 - 1979*Power(t2,2) + 
               861*Power(t2,3) + 910*Power(t2,4) - 550*Power(t2,5) + 
               8*Power(t2,6) + 
               Power(s2,3)*(104 - 606*t2 + 866*Power(t2,2) - 
                  354*Power(t2,3)) + 
               Power(t1,3)*(167 - 453*t2 + 433*Power(t2,2) - 
                  105*Power(t2,3)) + 
               Power(t1,2)*(25 + 251*t2 - 231*Power(t2,2) - 
                  199*Power(t2,3) + 208*Power(t2,4)) - 
               t1*(408 - 2064*t2 + 2331*Power(t2,2) + 
                  190*Power(t2,3) - 1251*Power(t2,4) + 138*Power(t2,5)) \
+ Power(s2,2)*(-633 + 2944*t2 - 3704*Power(t2,2) + 1412*Power(t2,3) + 
                  63*Power(t2,4) + 
                  t1*(-71 + 799*t2 - 1387*Power(t2,2) + 
                     707*Power(t2,3))) + 
               s2*(546 - 1866*t2 + 1695*Power(t2,2) + 306*Power(t2,3) - 
                  1081*Power(t2,4) + 152*Power(t2,5) - 
                  2*Power(t1,2)*
                   (37 + 117*t2 - 351*Power(t2,2) + 247*Power(t2,3)) + 
                  2*t1*(287 - 1809*t2 + 2749*Power(t2,2) - 
                     1375*Power(t2,3) + 80*Power(t2,4)))) + 
            Power(s,3)*(-2*Power(t1,3)*
                (-5 + 9*t2 - 22*Power(t2,2) + 8*Power(t2,3)) - 
               2*Power(s2,3)*
                (67 - 296*t2 + 613*Power(t2,2) - 496*Power(t2,3) + 
                  111*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (451 - 5*t2 + 1167*Power(t2,2) + 17*Power(t2,3) - 
                  336*Power(t2,4) + 4*Power(t2,5)) + 
               2*Power(t1,2)*
                (96 - 257*t2 + 668*Power(t2,2) - 646*Power(t2,3) + 
                  169*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(218 - 2444*t2 + 5037*Power(t2,2) - 
                  4327*Power(t2,3) + 1105*Power(t2,4) + 
                  419*Power(t2,5) - 8*Power(t2,6)) + 
               Power(s2,2)*(658 - 3243*t2 + 6044*Power(t2,2) - 
                  4520*Power(t2,3) + 1154*Power(t2,4) - 
                  9*Power(t2,5) + 
                  2*t1*(81 - 288*t2 + 685*Power(t2,2) - 
                     660*Power(t2,3) + 190*Power(t2,4))) - 
               s2*(713 - 3712*t2 + 5809*Power(t2,2) - 
                  4195*Power(t2,3) + 1078*Power(t2,4) + 
                  359*Power(t2,5) - 52*Power(t2,6) + 
                  Power(t1,2)*
                   (84 - 310*t2 + 802*Power(t2,2) - 832*Power(t2,3) + 
                     294*Power(t2,4)) + 
                  t1*(630 - 2934*t2 + 6856*Power(t2,2) - 
                     6480*Power(t2,3) + 2208*Power(t2,4) - 
                     112*Power(t2,5)))) - 
            s*(-1 + t2)*(Power(t1,3)*
                (-79 + 32*t2 + 48*Power(t2,2) - 16*Power(t2,3)) + 
               Power(t1,2)*(-35 - 520*t2 + 529*Power(t2,2) + 
                  114*Power(t2,3) - 88*Power(t2,4)) - 
               2*t1*Power(-1 + t2,2)*
                (-19 - 70*t2 + 177*Power(t2,2) + 52*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(-1 + t2,3)*
                (-99 - 142*t2 - 41*Power(t2,2) + 34*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,3)*(38 + 85*t2 - 36*Power(t2,2) - 
                  10*Power(t2,3) - 67*Power(t2,4) + 5*Power(t2,5)) - 
               Power(s2,2)*(86 + 135*t2 - 177*Power(t2,2) + 
                  146*Power(t2,3) - 209*Power(t2,4) + 25*Power(t2,5) - 
                  6*Power(t2,6) + 
                  t1*(121 + 348*t2 - 274*Power(t2,2) - 
                     196*Power(t2,3) + 42*Power(t2,4) + 4*Power(t2,5))) \
+ s2*(-(Power(-1 + t2,2)*(-47 - 109*t2 - 31*Power(t2,2) - 
                       111*Power(t2,3) + 12*Power(t2,4))) + 
                  Power(t1,2)*
                   (179 + 238*t2 - 410*Power(t2,2) - 10*Power(t2,3) + 
                     48*Power(t2,4)) + 
                  4*t1*(16 + 131*t2 + 3*Power(t2,2) - 193*Power(t2,3) + 
                     37*Power(t2,4) + 6*Power(t2,5)))) + 
            Power(s,2)*(Power(t1,3)*
                (60 - 43*t2 + 54*Power(t2,2) - 46*Power(t2,3) + 
                  8*Power(t2,4)) + 
               2*t1*Power(-1 + t2,2)*
                (86 + 209*t2 - 24*Power(t2,2) + 347*Power(t2,3) + 
                  51*Power(t2,4)) - 
               Power(-1 + t2,3)*
                (-398 - 432*t2 - 459*Power(t2,2) + 129*Power(t2,3) + 
                  74*Power(t2,4)) + 
               Power(s2,3)*(2 - 130*t2 + 282*Power(t2,2) - 
                  516*Power(t2,3) + 383*Power(t2,4) - 54*Power(t2,5)) + 
               Power(t1,2)*(-130 + 43*t2 + 455*Power(t2,2) - 
                  334*Power(t2,3) - 106*Power(t2,4) + 72*Power(t2,5)) + 
               Power(s2,2)*(-78 + 679*t2 - 2064*Power(t2,2) + 
                  2792*Power(t2,3) - 1626*Power(t2,4) + 
                  318*Power(t2,5) - 21*Power(t2,6) + 
                  t1*(74 + 21*t2 + 334*Power(t2,2) - 232*Power(t2,3) - 
                     160*Power(t2,4) + 62*Power(t2,5))) + 
               s2*(Power(-1 + t2,2)*
                   (79 - 1268*t2 + 537*Power(t2,2) - 672*Power(t2,3) - 
                     26*Power(t2,4) + 12*Power(t2,5)) - 
                  Power(t1,2)*
                   (139 - 46*t2 + 302*Power(t2,2) - 356*Power(t2,3) + 
                     20*Power(t2,4) + 40*Power(t2,5)) + 
                  2*t1*(50 + 111*t2 - 231*Power(t2,2) - 
                     412*Power(t2,3) + 718*Power(t2,4) - 
                     244*Power(t2,5) + 8*Power(t2,6))))) - 
         Power(s1,3)*(-1 + s + t2)*
          (Power(s,8)*(4 - 16*Power(t1,3) - 24*(2 + s2)*t2 + 
               2*t1*(10 + 3*s2 - 41*t2)*t2 + (98 + 73*s2)*Power(t2,2) - 
               35*Power(t2,3) + 2*Power(t1,2)*(-3 + 3*s2 + t2)) + 
            Power(s,7)*(-36 + Power(t1,3)*(92 - 45*t2) + 325*t2 - 
               641*Power(t2,2) + 353*Power(t2,3) - 85*Power(t2,4) + 
               5*Power(t1,2)*t2*(-8 + 3*t2) - 
               2*Power(s2,2)*t2*(-9 + t1 + 19*t2) - 
               t1*(9 + 122*t2 - 415*Power(t2,2) + 155*Power(t2,3)) + 
               s2*(4 + 160*t2 - 450*Power(t2,2) + 147*Power(t2,3) + 
                  Power(t1,2)*(-34 + 6*t2) + 
                  2*t1*(3 - 8*t2 + 29*Power(t2,2)))) + 
            Power(s,6)*(145 - 958*t2 + 1858*Power(t2,2) - 
               24*Power(s2,3)*Power(t2,2) - 1328*Power(t2,3) + 
               343*Power(t2,4) - 66*Power(t2,5) + 
               Power(t1,3)*(-197 + 220*t2 - 37*Power(t2,2)) + 
               Power(t1,2)*(80 + 122*t2 - 31*Power(t2,2) + 
                  17*Power(t2,3)) + 
               t1*(28 + 76*t2 - 451*Power(t2,2) + 624*Power(t2,3) - 
                  81*Power(t2,4)) + 
               2*Power(s2,2)*t2*
                (-109 + 233*t2 - 35*Power(t2,2) + t1*(11 + 19*t2)) - 
               s2*(40 + 157*t2 - 748*Power(t2,2) + 743*Power(t2,3) - 
                  90*Power(t2,4) + 
                  Power(t1,2)*(-74 + 67*t2 + 47*Power(t2,2)) + 
                  t1*(26 - 96*t2 + 538*Power(t2,2) - 116*Power(t2,3)))) \
- Power(s,5)*(363 - 1628*t2 + 3048*Power(t2,2) - 2504*Power(t2,3) + 
               769*Power(t2,4) - 41*Power(t2,5) + 16*Power(t2,6) + 
               2*Power(s2,3)*Power(t2,2)*(-71 + 20*t2) + 
               Power(t1,3)*(-184 + 353*t2 - 124*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(139 + 126*t2 + 160*Power(t2,2) - 
                  39*Power(t2,3) - 4*Power(t2,4)) + 
               t1*(52 - 741*t2 + 1100*Power(t2,2) + 77*Power(t2,3) - 
                  364*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-880 + 1976*t2 - 734*Power(t2,2) + 29*Power(t2,3) + 
                  t1*(36 + 240*t2 - 51*Power(t2,2))) + 
               s2*(-148 + 939*t2 - 894*Power(t2,2) - 669*Power(t2,3) + 
                  473*Power(t2,4) - 16*Power(t2,5) + 
                  Power(t1,2)*
                   (78 - 165*t2 - 177*Power(t2,2) + 67*Power(t2,3)) + 
                  t1*(-44 + 610*t2 - 2090*Power(t2,2) + 
                     952*Power(t2,3) - 80*Power(t2,4)))) + 
            (-1 + t2)*(2*Power(s2,3)*Power(t2,3)*(4 + 5*t2) - 
               Power(-1 + t2,3)*(-1 + 44*t2) + 
               t1*Power(-1 + t2,2)*(21 - 149*t2 + 8*Power(t2,2)) + 
               Power(t1,3)*(19 - 49*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(-39 + 193*t2 - 174*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-8 + 47*t2 + 25*Power(t2,2) - 64*Power(t2,3) + 
                  t1*(8 - 17*t2 - 53*Power(t2,2) + 8*Power(t2,3))) - 
               2*s2*(-(Power(-1 + t2,2)*
                     (-4 + 15*t2 + 49*Power(t2,2))) + 
                  Power(t1,2)*
                   (4 + 5*t2 - 46*Power(t2,2) + 10*Power(t2,3)) + 
                  t1*(-8 + 18*t2 + 91*Power(t2,2) - 109*Power(t2,3) + 
                     8*Power(t2,4)))) + 
            Power(s,4)*(575 - 1702*t2 + 3022*Power(t2,2) - 
               2468*Power(t2,3) + 942*Power(t2,4) - 11*Power(t2,5) - 
               62*Power(t2,6) + 
               Power(s2,3)*Power(t2,2)*
                (-224 + 165*t2 - 19*Power(t2,2)) + 
               Power(t1,3)*(-65 + 180*t2 - 107*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(5 + 118*t2 + 361*Power(t2,2) - 
                  240*Power(t2,3) + 20*Power(t2,4)) + 
               t1*(159 - 1872*t2 + 3058*Power(t2,2) - 
                  1856*Power(t2,3) + 79*Power(t2,4) + 140*Power(t2,5)) \
+ Power(s2,2)*t2*(-1500 + 3534*t2 - 2154*Power(t2,2) + 
                  295*Power(t2,3) + 5*Power(t2,4) + 
                  t1*(-44 + 431*t2 - 228*Power(t2,2) + 7*Power(t2,3))) \
+ s2*(-252 + 2588*t2 - 4098*Power(t2,2) + 1806*Power(t2,3) + 
                  431*Power(t2,4) - 155*Power(t2,5) - 
                  2*Power(t1,2)*
                   (-25 + 54*t2 + 98*Power(t2,2) - 86*Power(t2,3) + 
                     10*Power(t2,4)) + 
                  2*t1*(-26 + 701*t2 - 1853*Power(t2,2) + 
                     1176*Power(t2,3) - 249*Power(t2,4) + 8*Power(t2,5))\
)) + Power(s,3)*(-494 + 863*t2 - 1592*Power(t2,2) + 1504*Power(t2,3) - 
               353*Power(t2,4) + 58*Power(t2,5) + 28*Power(t2,6) - 
               14*Power(t2,7) + 
               Power(t1,3)*(36 - 15*t2 + 8*Power(t2,2)) + 
               Power(s2,3)*Power(t2,2)*
                (60 - 137*t2 + 53*Power(t2,2) - 3*Power(t2,3)) + 
               Power(t1,2)*(106 - 28*t2 - 165*Power(t2,2) + 
                  110*Power(t2,3) - 8*Power(t2,4)) + 
               t1*(-289 + 1800*t2 - 2451*Power(t2,2) + 
                  1723*Power(t2,3) - 630*Power(t2,4) + 
                  24*Power(t2,5) + 24*Power(t2,6)) + 
               Power(s2,2)*t2*
                (1122 - 2607*t2 + 2036*Power(t2,2) - 597*Power(t2,3) + 
                  9*Power(t2,4) + 2*Power(t2,5) - 
                  2*t1*(-83 + 129*t2 - 111*Power(t2,2) + 
                     13*Power(t2,3) + 2*Power(t2,4))) + 
               s2*(200 - 2282*t2 + 4181*Power(t2,2) - 
                  3164*Power(t2,3) + 638*Power(t2,4) + 
                  240*Power(t2,5) - 14*Power(t2,6) + 
                  2*Power(t1,2)*
                   (-23 - 44*t2 + 49*Power(t2,2) - 41*Power(t2,3) + 
                     8*Power(t2,4)) - 
                  2*t1*(-39 + 722*t2 - 1459*Power(t2,2) + 
                     1008*Power(t2,3) - 294*Power(t2,4) + 52*Power(t2,5)\
))) + s*(Power(t1,3)*(72 - 195*t2 + 140*Power(t2,2) - 24*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (-58 + 89*t2 - 25*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-95 + 594*t2 - 706*Power(t2,2) + 
                  203*Power(t2,3) + 4*Power(t2,4)) - 
               Power(-1 + t2,3)*
                (-11 - 185*t2 - 307*Power(t2,2) - 34*Power(t2,3) + 
                  6*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (-18 - 367*t2 + 248*Power(t2,2) + 52*Power(t2,3) + 
                  28*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-36 + 317*t2 - 334*Power(t2,2) + 60*Power(t2,3) - 
                  13*Power(t2,4) + 6*Power(t2,5) + 
                  t1*(64 - 14*t2 - 153*Power(t2,2) + 86*Power(t2,3) - 
                     4*Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (-34 - 51*t2 + 253*Power(t2,2) - 163*Power(t2,3) + 
                     16*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (-16 + 181*t2 + 37*Power(t2,2) - 129*Power(t2,3) - 
                     22*Power(t2,4) + 6*Power(t2,5)) - 
                  2*t1*(-32 + 117*t2 + 189*Power(t2,2) - 
                     404*Power(t2,3) + 134*Power(t2,4) - 
                     12*Power(t2,5) + 8*Power(t2,6)))) + 
            Power(s,2)*(Power(t1,3)*
                (-87 + 140*t2 - 67*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (104 - 69*t2 - 7*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(10 - 410*t2 + 319*Power(t2,2) + 
                  65*Power(t2,3) - 40*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (-179 - 450*t2 - 675*Power(t2,2) - 115*Power(t2,3) - 
                  6*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(202 - 524*t2 + 165*Power(t2,2) + 156*Power(t2,3) + 
                  53*Power(t2,4) - 80*Power(t2,5) + 28*Power(t2,6)) - 
               Power(s2,2)*t2*
                (274 - 359*t2 + 234*Power(t2,2) - 134*Power(t2,3) + 
                  37*Power(t2,4) + 4*Power(t2,5) + 
                  6*t1*(27 - 3*t2 - 12*Power(t2,2) + Power(t2,3))) + 
               s2*(-52 + 495*t2 - 1023*Power(t2,2) + 1159*Power(t2,3) - 
                  659*Power(t2,4) + 26*Power(t2,5) + 52*Power(t2,6) + 
                  2*Power(t2,7) + 
                  Power(t1,2)*
                   (54 + 141*t2 - 183*Power(t2,2) + 28*Power(t2,3) + 
                     8*Power(t2,4)) - 
                  2*t1*(49 - 374*t2 + 295*Power(t2,2) - 46*Power(t2,3) + 
                     20*Power(t2,4) - 8*Power(t2,5) + 8*Power(t2,6))))) + 
         Power(s1,4)*(Power(s,8)*
             (18 - 56*Power(t1,3) - 109*t2 + 136*Power(t2,2) - 
               30*Power(t2,3) - 6*Power(s2,2)*(t1 + t2) + 
               2*Power(t1,2)*(-19 + 7*t2) + 
               t1*(-13 + 112*t2 - 219*Power(t2,2)) + 
               s2*(12 + 42*Power(t1,2) - 110*t2 + 185*Power(t2,2) + 
                  t1*(6 + 32*t2))) + 
            Power(s,7)*(-109 + Power(t1,3)*(275 - 203*t2) + 566*t2 + 
               4*Power(s2,3)*t2 - 825*Power(t2,2) + 411*Power(t2,3) - 
               90*Power(t2,4) + 
               Power(t1,2)*(106 - 285*t2 + 114*Power(t2,2)) + 
               t1*(35 - 613*t2 + 1238*Power(t2,2) - 570*Power(t2,3)) - 
               2*Power(s2,2)*
                (6 - 19*t2 + 13*Power(t2,2) + t1*(-16 + 17*t2)) + 
               s2*(-79 + 713*t2 - 1205*Power(t2,2) + 480*Power(t2,3) + 
                  Power(t1,2)*(-200 + 129*t2) + 
                  2*t1*(5 - 25*t2 + 53*Power(t2,2)))) + 
            Power(s,6)*(255 - 1337*t2 + 24*Power(s2,3)*(1 - 4*t2)*t2 + 
               2342*Power(t2,2) - 1384*Power(t2,3) + 255*Power(t2,4) - 
               94*Power(t2,5) + 
               Power(t1,3)*(-501 + 805*t2 - 237*Power(t2,2)) + 
               Power(t1,2)*(-75 + 770*t2 - 566*Power(t2,2) + 
                  189*Power(t2,3)) + 
               t1*(22 + 628*t2 - 2089*Power(t2,2) + 2230*Power(t2,3) - 
                  501*Power(t2,4)) + 
               Power(s2,2)*(122 - 550*t2 + 721*Power(t2,2) - 
                  60*Power(t2,3) + t1*(-72 + 133*t2 + 148*Power(t2,2))) \
+ s2*(104 - 1096*t2 + 2548*Power(t2,2) - 2180*Power(t2,3) + 
                  429*Power(t2,4) + 
                  Power(t1,2)*(359 - 524*t2 - 46*Power(t2,2)) + 
                  2*t1*(-63 + 182*t2 - 522*Power(t2,2) + 119*Power(t2,3))\
)) + Power(-1 + t2,2)*(-4*Power(-1 + t2,3)*(5 + 11*t2) + 
               6*Power(s2,3)*Power(t2,2)*(1 + 4*t2 + 2*Power(t2,2)) + 
               3*Power(t1,3)*(11 - 35*t2 + 10*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(-31 - 210*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(-22 + 299*t2 - 317*Power(t2,2) + 
                  40*Power(t2,3)) + 
               Power(s2,2)*(-4 + 19*t2 + 121*Power(t2,2) - 
                  68*Power(t2,3) - 68*Power(t2,4) + 
                  t1*(4 + 11*t2 - 91*Power(t2,2) - 62*Power(t2,3) + 
                     12*Power(t2,4))) - 
               s2*(-(Power(-1 + t2,2)*(-3 + 132*t2 + 100*Power(t2,2))) + 
                  Power(t1,2)*
                   (31 - 52*t2 - 145*Power(t2,2) + 40*Power(t2,3)) + 
                  t1*(-34 + 250*t2 + 56*Power(t2,2) - 296*Power(t2,3) + 
                     24*Power(t2,4)))) + 
            Power(s,5)*(-315 + 1995*t2 - 3956*Power(t2,2) + 
               3220*Power(t2,3) - 437*Power(t2,4) - 279*Power(t2,5) - 
               38*Power(t2,6) + 
               Power(s2,3)*t2*(-246 + 597*t2 - 239*Power(t2,2)) + 
               Power(t1,3)*(407 - 995*t2 + 640*Power(t2,2) - 
                  100*Power(t2,3)) + 
               Power(t1,2)*(75 - 457*t2 + 294*Power(t2,2) - 
                  221*Power(t2,3) + 105*Power(t2,4)) - 
               t1*(347 - 1457*t2 + 1285*Power(t2,2) + 
                  1137*Power(t2,3) - 1568*Power(t2,4) + 162*Power(t2,5)) \
+ Power(s2,2)*(-464 + 2607*t2 - 4130*Power(t2,2) + 1709*Power(t2,3) - 
                  64*Power(t2,4) + 
                  t1*(74 + 98*t2 - 803*Power(t2,2) + 373*Power(t2,3))) + 
               s2*(366 - 1415*t2 + 520*Power(t2,2) + 1791*Power(t2,3) - 
                  1494*Power(t2,4) + 156*Power(t2,5) + 
                  Power(t1,2)*
                   (-294 + 493*t2 + 315*Power(t2,2) - 314*Power(t2,3)) + 
                  t1*(376 - 2362*t2 + 4900*Power(t2,2) - 
                     2698*Power(t2,3) + 288*Power(t2,4)))) + 
            Power(s,4)*(194 - 2254*t2 + 4514*Power(t2,2) - 
               4497*Power(t2,3) + 1985*Power(t2,4) + 445*Power(t2,5) - 
               383*Power(t2,6) - 4*Power(t2,7) + 
               2*Power(s2,3)*t2*
                (234 - 585*t2 + 492*Power(t2,2) - 100*Power(t2,3)) + 
               Power(t1,3)*(-125 + 379*t2 - 381*Power(t2,2) + 
                  127*Power(t2,3) - 10*Power(t2,4)) + 
               Power(t1,2)*(-218 - 127*t2 + 851*Power(t2,2) - 
                  799*Power(t2,3) + 127*Power(t2,4) + 16*Power(t2,5)) + 
               t1*(820 - 3376*t2 + 6557*Power(t2,2) - 
                  4810*Power(t2,3) + 531*Power(t2,4) + 
                  622*Power(t2,5) - 12*Power(t2,6)) + 
               Power(s2,2)*(776 - 4511*t2 + 8424*Power(t2,2) - 
                  6236*Power(t2,3) + 1398*Power(t2,4) - 
                  39*Power(t2,5) + 
                  t1*(-2 - 583*t2 + 1429*Power(t2,2) - 
                     1231*Power(t2,3) + 237*Power(t2,4))) + 
               s2*(-1159 + 5190*t2 - 8603*Power(t2,2) + 
                  4874*Power(t2,3) - 174*Power(t2,4) - 486*Power(t2,5) + 
                  26*Power(t2,6) + 
                  Power(t1,2)*
                   (85 + 82*t2 - 678*Power(t2,2) + 810*Power(t2,3) - 
                     221*Power(t2,4)) + 
                  2*t1*(-310 + 2213*t2 - 4461*Power(t2,2) + 
                     3817*Power(t2,3) - 1164*Power(t2,4) + 74*Power(t2,5)\
))) + s*(-1 + t2)*(Power(t1,3)*
                (99 - 220*t2 + 108*Power(t2,2) - 16*Power(t2,3)) + 
               Power(s2,3)*t2*
                (-86 + 45*t2 + 28*Power(t2,2) + 33*Power(t2,3) + 
                  9*Power(t2,4)) - 
               Power(-1 + t2,3)*
                (-61 - 228*t2 - 179*Power(t2,2) + 20*Power(t2,3) + 
                  10*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (-123 - 338*t2 + 214*Power(t2,2) + 132*Power(t2,3) + 
                  24*Power(t2,4)) + 
               Power(t1,2)*(-19 + 676*t2 - 658*Power(t2,2) - 
                  63*Power(t2,3) + 64*Power(t2,4)) + 
               Power(s2,2)*(-16 + 269*t2 - 86*Power(t2,2) - 
                  49*Power(t2,3) - 73*Power(t2,4) - 39*Power(t2,5) - 
                  6*Power(t2,6) + 
                  t1*(30 + 208*t2 - 139*Power(t2,2) - 228*Power(t2,3) + 
                     38*Power(t2,4) + 4*Power(t2,5))) - 
               s2*(Power(t1,2)*
                   (106 + 57*t2 - 300*Power(t2,2) + 26*Power(t2,3) + 
                     24*Power(t2,4)) - 
                  Power(-1 + t2,2)*
                   (40 + 77*t2 + 55*Power(t2,2) - 85*Power(t2,3) + 
                     2*Power(t2,4) + 2*Power(t2,5)) + 
                  2*t1*(-22 + 319*t2 + 59*Power(t2,2) - 
                     390*Power(t2,3) + 14*Power(t2,4) + 16*Power(t2,5) + 
                     4*Power(t2,6)))) + 
            Power(s,3)*(Power(t1,3)*(-39 - t2 + 8*Power(t2,2)) + 
               Power(s2,3)*t2*
                (-228 + 814*t2 - 982*Power(t2,2) + 547*Power(t2,3) - 
                  72*Power(t2,4)) + 
               Power(t1,2)*(184 - 123*t2 - 278*Power(t2,2) + 
                  504*Power(t2,3) - 338*Power(t2,4) + 48*Power(t2,5)) - 
               Power(-1 + t2,2)*
                (-75 - 1877*t2 + 365*Power(t2,2) - 865*Power(t2,3) + 
                  78*Power(t2,4) + 138*Power(t2,5)) + 
               t1*(-723 + 2251*t2 - 4834*Power(t2,2) + 
                  5628*Power(t2,3) - 2942*Power(t2,4) + 
                  436*Power(t2,5) + 184*Power(t2,6)) + 
               Power(s2,2)*(-580 + 3016*t2 - 6267*Power(t2,2) + 
                  6650*Power(t2,3) - 3230*Power(t2,4) + 
                  431*Power(t2,5) - 23*Power(t2,6) + 
                  t1*(-76 + 338*t2 - 912*Power(t2,2) + 
                     806*Power(t2,3) - 386*Power(t2,4) + 40*Power(t2,5))\
) + s2*(1081 - 4517*t2 + 9409*Power(t2,2) - 9070*Power(t2,3) + 
                  3494*Power(t2,4) - 328*Power(t2,5) - 73*Power(t2,6) + 
                  4*Power(t2,7) + 
                  Power(t1,2)*
                   (68 - 13*t2 + 258*Power(t2,2) - 332*Power(t2,3) + 
                     202*Power(t2,4) - 40*Power(t2,5)) + 
                  t1*(554 - 3118*t2 + 5786*Power(t2,2) - 
                     5844*Power(t2,3) + 3336*Power(t2,4) - 
                     732*Power(t2,5) + 24*Power(t2,6)))) - 
            Power(s,2)*(Power(t1,3)*
                (-105 + 133*t2 - 25*Power(t2,2) - 14*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(t1,2)*(31 - 560*t2 + 812*Power(t2,2) - 
                  429*Power(t2,3) + 170*Power(t2,4) - 24*Power(t2,5)) + 
               Power(s2,3)*t2*
                (108 + 20*t2 - 208*Power(t2,2) + 200*Power(t2,3) - 
                  124*Power(t2,4) + 11*Power(t2,5)) + 
               Power(-1 + t2,3)*
                (-199 - 1113*t2 - 442*Power(t2,2) - 140*Power(t2,3) + 
                  74*Power(t2,4) + 14*Power(t2,5)) - 
               t1*Power(-1 + t2,2)*
                (114 - 114*t2 + 579*Power(t2,2) - 6*Power(t2,3) + 
                  184*Power(t2,4) + 20*Power(t2,5)) + 
               Power(s2,2)*(-146 + 336*t2 - 844*Power(t2,2) + 
                  1495*Power(t2,3) - 1431*Power(t2,4) + 
                  620*Power(t2,5) - 38*Power(t2,6) + 8*Power(t2,7) + 
                  t1*(-76 - 223*t2 + 100*Power(t2,2) + 168*Power(t2,3) + 
                     14*Power(t2,4) - 4*Power(t2,5))) + 
               s2*(-(Power(-1 + t2,2)*
                     (-282 + 484*t2 - 1310*Power(t2,2) + 
                       374*Power(t2,3) - 55*Power(t2,4) + 12*Power(t2,5))\
) + Power(t1,2)*(135 + 232*t2 - 498*Power(t2,2) + 212*Power(t2,3) - 
                     84*Power(t2,4) + 24*Power(t2,5)) + 
                  2*t1*(95 - 172*t2 + 392*Power(t2,2) - 369*Power(t2,3) + 
                     156*Power(t2,4) - 146*Power(t2,5) + 44*Power(t2,6))))\
))*R1q(s1))/(s*(-1 + s1)*s1*Power(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2),2)*(-1 + s2)*(-1 + t1)*Power(-1 + s + t2,3)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) + 
    (8*(-2*Power(s,11)*(Power(t1,3) + 5*t1*Power(t2,2) + 
            Power(t2,2)*(-5 - s2 + 2*t2)) - 
         Power(-1 + t2,2)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          Power(s1 + Power(s1,2) - 2*s1*t2 + (-1 + t2)*t2,2)*
          (6 + Power(s1,3)*(2 - 3*s2 + 3*t1 - 2*t2) - 23*t2 + 2*s2*t2 + 
            17*Power(t2,2) - 5*s2*Power(t2,2) + 
            t1*(-6 + 11*t2 - 2*Power(t2,2)) + 
            Power(s1,2)*(1 + s2 + 2*t1 - 3*t2 + 2*s2*t2 - 5*t1*t2 + 
               2*Power(t2,2)) + 
            s1*(11 + t1 - 12*t2 - 6*t1*t2 + Power(t2,2) + 
               2*t1*Power(t2,2) + s2*(-2 + 4*t2 + Power(t2,2)))) + 
         Power(s,10)*(Power(t1,3)*(16 + 18*s1 - 17*t2) + 
            t1*t2*(-6 + 6*s2 - 2*s1*(7 + 3*s2 - 37*t2) + 62*t2 + 
               10*s2*t2 - 81*Power(t2,2)) + 
            Power(t1,2)*(-6 - 2*s2*t2 + Power(t2,2) - 
               2*s1*(-3 + 3*s2 + t2)) + 
            t2*(s1*(20 + 4*s2 - 70*t2 - 24*s2*t2 + 24*Power(t2,2)) - 
               t2*(76 + 22*s2 + 2*Power(s2,2) - 96*t2 - 20*s2*t2 + 
                  31*Power(t2,2)))) + 
         Power(s,9)*(-2*Power(t1,3)*(26 - 63*t2 + 32*Power(t2,2)) + 
            Power(t1,2)*(42 + (-41 + 6*s2)*t2 - 
               (6 + 17*s2)*Power(t2,2) + 8*Power(t2,3)) - 
            2*t1*(3 + (-21 + 20*s2)*t2 + 3*(28 + 5*s2)*Power(t2,2) - 
               (221 + 41*s2)*Power(t2,3) + 144*Power(t2,4)) + 
            t2*(-6 + 260*t2 + 2*Power(s2,2)*(15 - 8*t2)*t2 - 
               567*Power(t2,2) + 374*Power(t2,3) - 104*Power(t2,4) + 
               3*s2*(2 + 20*t2 - 64*Power(t2,2) + 29*Power(t2,3))) - 
            2*Power(s1,2)*(-5 + 36*Power(t1,3) + 
               Power(t1,2)*(22 - 8*t2) + 57*t2 - 104*Power(t2,2) + 
               30*Power(t2,3) + 3*Power(s2,2)*(t1 + t2) + 
               t1*(5 - 48*t2 + 119*Power(t2,2)) - 
               s2*(1 + 24*Power(t1,2) - 16*t2 + 51*Power(t2,2) + 
                  t1*(3 + 19*t2))) + 
            s1*(3*Power(t1,3)*(-41 + 47*t2) + 
               Power(t1,2)*(6 + s2*(34 - 37*t2) + 75*t2 - 
                  26*Power(t2,2)) + 
               t2*(-120 + 576*t2 - 581*Power(t2,2) + 166*Power(t2,3) + 
                  2*Power(s2,2)*(1 + 9*t2) + 
                  s2*(-58 + 228*t2 - 189*Power(t2,2))) + 
               t1*(6 + 100*t2 + 2*Power(s2,2)*t2 - 515*Power(t2,2) + 
                  535*Power(t2,3) - 2*s2*(3 - 3*t2 + 52*Power(t2,2))))) + 
         Power(s,7)*(10 - 79*t2 + 50*s2*t2 + 1189*Power(t2,2) - 
            945*s2*Power(t2,2) + 509*Power(s2,2)*Power(t2,2) - 
            24*Power(s2,3)*Power(t2,2) - 3595*Power(t2,3) + 
            901*s2*Power(t2,3) - 1245*Power(s2,2)*Power(t2,3) + 
            47*Power(s2,3)*Power(t2,3) + 4794*Power(t2,4) + 
            1260*s2*Power(t2,4) + 763*Power(s2,2)*Power(t2,4) - 
            2781*Power(t2,5) - 1575*s2*Power(t2,5) - 
            112*Power(s2,2)*Power(t2,5) + 833*Power(t2,6) + 
            336*s2*Power(t2,6) - 224*Power(t2,7) + 
            Power(t1,3)*(-56 + 504*t2 - 1097*Power(t2,2) + 
               847*Power(t2,3) - 196*Power(t2,4)) + 
            Power(t1,2)*(138 - (519 + 98*s2)*t2 + 
               (557 + 91*s2)*Power(t2,2) + (-79 + 147*s2)*Power(t2,3) - 
               7*(19 + 20*s2)*Power(t2,4) + 56*Power(t2,5)) + 
            t1*(-78 - 8*(-31 + 3*s2)*t2 + 
               (-617 + 356*s2 - 47*Power(s2,2))*Power(t2,2) + 
               (701 + 506*s2 + 23*Power(s2,2))*Power(t2,3) - 
               (1919 + 1400*s2)*Power(t2,4) + 
               7*(343 + 88*s2)*Power(t2,5) - 756*Power(t2,6)) + 
            2*Power(s1,4)*(42 + 6*Power(s2,3) - 126*Power(t1,3) - 
               170*t2 + 165*Power(t2,2) - 30*Power(t2,3) + 
               14*Power(t1,2)*(-9 + 4*t2) - 
               Power(s2,2)*(1 + 63*t1 + 39*t2) - 
               5*t1*(14 - 46*t2 + 49*Power(t2,2)) + 
               s2*(20 + 57*t1 + 168*Power(t1,2) - 90*t2 + 63*t1*t2 + 
                  135*Power(t2,2))) + 
            Power(s1,3)*(Power(s2,3)*(8 - 37*t2) + 
               14*Power(t1,3)*(-59 + 78*t2) + 
               Power(t1,2)*(-482 + 1459*t2 - 530*Power(t2,2)) + 
               t1*(-128 + 1628*t2 - 3605*Power(t2,2) + 
                  2355*Power(t2,3)) + 
               2*(98 - 680*t2 + 1395*Power(t2,2) - 1019*Power(t2,3) + 
                  210*Power(t2,4)) + 
               Power(s2,2)*(6 - 33*t2 + 244*Power(t2,2) + 
                  t1*(-163 + 351*t2)) + 
               s2*(118 + Power(t1,2)*(723 - 1106*t2) - 980*t2 + 
                  1867*Power(t2,2) - 1147*Power(t2,3) - 
                  2*t1*(-23 + 60*t2 + 292*Power(t2,2)))) + 
            Power(s1,2)*(106 - 1656*t2 + 5749*Power(t2,2) - 
               7360*Power(t2,3) + 4125*Power(t2,4) - 930*Power(t2,5) - 
               4*Power(s2,3)*t2*(3 + t2) - 
               2*Power(t1,3)*(495 - 1331*t2 + 797*Power(t2,2)) + 
               Power(t1,2)*(97 + 1528*t2 - 2283*Power(t2,2) + 
                  694*Power(t2,3)) + 
               t1*(56 + 810*t2 - 4872*Power(t2,2) + 8376*Power(t2,3) - 
                  4060*Power(t2,4)) + 
               Power(s2,2)*(28 - 193*t2 + 674*Power(t2,2) - 
                  435*Power(t2,3) - 2*t1*(24 - 152*t2 + 87*Power(t2,2))\
) + s2*(44 - 894*t2 + 3455*Power(t2,2) - 4722*Power(t2,3) + 
                  1781*Power(t2,4) + 
                  2*Power(t1,2)*(227 - 755*t2 + 456*Power(t2,2)) + 
                  2*t1*(-65 + 38*t2 - 737*Power(t2,2) + 727*Power(t2,3))\
)) + s1*(12 - 779*t2 + 4491*Power(t2,2) - 9089*Power(t2,3) + 
               7719*Power(t2,4) - 3269*Power(t2,5) + 798*Power(t2,6) + 
               3*Power(s2,3)*t2*(4 - 21*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(-485 + 2145*t2 - 2682*Power(t2,2) + 
                  952*Power(t2,3)) + 
               Power(t1,2)*(490 - 530*t2 - 936*Power(t2,2) + 
                  1161*Power(t2,3) - 336*Power(t2,4)) + 
               t1*(-121 + 327*t2 - 1289*Power(t2,2) + 
                  5305*Power(t2,3) - 7662*Power(t2,4) + 2954*Power(t2,5)\
) + Power(s2,2)*t2*(-300 + 1351*t2 - 1420*Power(t2,2) + 
                  385*Power(t2,3) + t1*(94 - 101*t2 - 37*Power(t2,2))) - 
               s2*(2 - 333*t2 - 285*Power(t2,2) + 3797*Power(t2,3) - 
                  4604*Power(t2,4) + 1232*Power(t2,5) + 
                  Power(t1,2)*
                   (-78 + 527*t2 - 570*Power(t2,2) + 14*Power(t2,3)) + 
                  2*t1*(22 + 91*t2 + 409*Power(t2,2) - 
                     1451*Power(t2,3) + 798*Power(t2,4))))) + 
         Power(s,8)*(-2 + 36*t2 - 32*s2*t2 - 599*Power(t2,2) + 
            118*s2*Power(t2,2) - 178*Power(s2,2)*Power(t2,2) + 
            6*Power(s2,3)*Power(t2,2) + 1722*Power(t2,3) + 
            424*s2*Power(t2,3) + 229*Power(s2,2)*Power(t2,3) - 
            1745*Power(t2,4) - 729*s2*Power(t2,4) - 
            56*Power(s2,2)*Power(t2,4) + 763*Power(t2,5) + 
            216*s2*Power(t2,5) - 196*Power(t2,6) + 
            Power(t1,3)*(84 - 368*t2 + 433*Power(t2,2) - 
               140*Power(t2,3)) + 
            Power(t1,2)*(-114 + (249 + 14*s2)*t2 + 
               5*(-20 + 9*s2)*Power(t2,2) - (43 + 64*s2)*Power(t2,3) + 
               28*Power(t2,4)) + 
            t1*(36 + (-139 + 86*s2)*t2 + 
               (313 - 56*s2 + 3*Power(s2,2))*Power(t2,2) - 
               7*(121 + 56*s2)*Power(t2,3) + 
               (1367 + 296*s2)*Power(t2,4) - 588*Power(t2,5)) - 
            2*Power(s1,3)*(23 + Power(s2,3) - 84*Power(t1,3) - 135*t2 + 
               170*Power(t2,2) - 40*Power(t2,3) + 
               14*Power(t1,2)*(-5 + 2*t2) - 
               Power(s2,2)*(21*t1 + 17*t2) + 
               t1*(-29 + 141*t2 - 217*Power(t2,2)) + 
               s2*(7 + 84*Power(t1,2) - 52*t2 + 110*Power(t2,2) + 
                  t1*(20 + 49*t2))) + 
            Power(s1,2)*(-50 + Power(t1,3)*(418 - 516*t2) + 636*t2 + 
               4*Power(s2,3)*t2 - 1746*Power(t2,2) + 1477*Power(t2,3) - 
               365*Power(t2,4) + 
               Power(s2,2)*(-2 + t1*(26 - 63*t2) + 6*t2 - 
                  93*Power(t2,2)) + 
               3*Power(t1,2)*(38 - 167*t2 + 56*Power(t2,2)) + 
               t1*(2 - 572*t2 + 1826*Power(t2,2) - 1509*Power(t2,3)) + 
               s2*(-24 + 364*t2 - 905*Power(t2,2) + 658*Power(t2,3) + 
                  3*Power(t1,2)*(-79 + 110*t2) + 
                  t1*(20 - 58*t2 + 372*Power(t2,2)))) + 
            s1*(Power(t1,3)*(343 - 876*t2 + 484*Power(t2,2)) - 
               Power(t1,2)*(167 + 125*t2 - 399*Power(t2,2) + 
                  128*Power(t2,3) + s2*(74 - 222*t2 + 78*Power(t2,2))) + 
               t2*(350 - 2031*t2 + 4*Power(s2,3)*t2 + 
                  3200*Power(t2,2) - 1933*Power(t2,3) + 
                  490*Power(t2,4) + 
                  Power(s2,2)*(34 - 206*t2 + 127*Power(t2,2)) - 
                  2*s2*(-63 + 421*t2 - 762*Power(t2,2) + 
                     322*Power(t2,3))) + 
               t1*(Power(s2,2)*t2*(-26 + 3*t2) + 
                  s2*(26 + 62*t2 + 480*Power(t2,2) - 572*Power(t2,3)) + 
                  2*(3 - 112*t2 + 677*Power(t2,2) - 1469*Power(t2,3) + 
                     837*Power(t2,4))))) + 
         Power(s,6)*(-16 + 53*t2 + 8*s2*t2 - 1965*Power(t2,2) + 
            2077*s2*Power(t2,2) - 800*Power(s2,2)*Power(t2,2) + 
            36*Power(s2,3)*Power(t2,2) + 6311*Power(t2,3) - 
            5630*s2*Power(t2,3) + 3157*Power(s2,2)*Power(t2,3) - 
            166*Power(s2,3)*Power(t2,3) - 9493*Power(t2,4) + 
            3325*s2*Power(t2,4) - 3759*Power(s2,2)*Power(t2,4) + 
            161*Power(s2,3)*Power(t2,4) + 7224*Power(t2,5) + 
            2016*s2*Power(t2,5) + 1449*Power(s2,2)*Power(t2,5) - 
            2217*Power(t2,6) - 2121*s2*Power(t2,6) - 
            140*Power(s2,2)*Power(t2,6) + 343*Power(t2,7) + 
            336*s2*Power(t2,7) - 154*Power(t2,8) - 
            Power(t1,3)*(28 + 210*t2 - 1152*Power(t2,2) + 
               1766*Power(t2,3) - 1029*Power(t2,4) + 182*Power(t2,5)) + 
            Power(t1,2)*(-30 + 5*(53 + 42*s2)*t2 - 
               (776 + 499*s2)*Power(t2,2) + 
               (559 + 248*s2)*Power(t2,3) + (86 + 273*s2)*Power(t2,4) - 
               7*(33 + 28*s2)*Power(t2,5) + 70*Power(t2,6)) + 
            t1*(60 - (103 + 190*s2)*t2 + 
               2*(581 - 328*s2 + 86*Power(s2,2))*Power(t2,2) + 
               (-1087 + 820*s2 - 311*Power(s2,2))*Power(t2,3) + 
               (305 + 1968*s2 + 77*Power(s2,2))*Power(t2,4) - 
               3*(793 + 868*s2)*Power(t2,5) + 
               203*(13 + 4*s2)*Power(t2,6) - 630*Power(t2,7)) - 
            2*Power(s1,5)*(38 + 15*Power(s2,3) - 126*Power(t1,3) + 
               70*Power(t1,2)*(-2 + t2) - 120*t2 + 95*Power(t2,2) - 
               12*Power(t2,3) - 5*Power(s2,2)*(1 + 21*t1 + 9*t2) - 
               5*t1*(18 - 45*t2 + 35*Power(t2,2)) + 
               s2*(30 + 210*Power(t1,2) - 90*t2 + 96*Power(t2,2) + 
                  5*t1*(18 + 7*t2))) + 
            Power(s1,3)*(-364 + 3103*t2 - 8012*Power(t2,2) + 
               8925*Power(t2,3) - 4676*Power(t2,4) + 900*Power(t2,5) + 
               Power(s2,3)*(-10 + 94*t2 - 69*Power(t2,2)) + 
               Power(t1,3)*(1641 - 4628*t2 + 2981*Power(t2,2)) + 
               Power(t1,2)*(539 - 4478*t2 + 5843*Power(t2,2) - 
                  1913*Power(t2,3)) + 
               t1*(-77 - 2230*t2 + 9865*Power(t2,2) - 
                  13220*Power(t2,3) + 5266*Power(t2,4)) + 
               Power(s2,2)*(-97 + 414*t2 - 1111*Power(t2,2) + 
                  705*Power(t2,3) + 
                  2*t1*(124 - 599*t2 + 447*Power(t2,2))) + 
               s2*(-196 + 2268*t2 - 6798*Power(t2,2) + 
                  7506*Power(t2,3) - 2372*Power(t2,4) + 
                  Power(t1,2)*(-1203 + 4124*t2 - 2876*Power(t2,2)) + 
                  2*t1*(75 + 167*t2 + 524*Power(t2,2) - 706*Power(t2,3))\
)) + Power(s1,4)*(-294 + 1496*t2 - 2560*Power(t2,2) + 
               1642*Power(t2,3) - 265*Power(t2,4) - 
               210*Power(t1,3)*(-5 + 7*t2) + 
               Power(s2,3)*(-43 + 130*t2) + 
               Power(s2,2)*(18 + t1*(440 - 895*t2) - 23*t2 - 
                  303*Power(t2,2)) + 
               Power(t1,2)*(924 - 2429*t2 + 982*Power(t2,2)) + 
               t1*(400 - 2642*t2 + 4320*Power(t2,2) - 
                  2195*Power(t2,3)) + 
               s2*(5*Power(t1,2)*(-253 + 406*t2) + 
                  4*t1*(-83 + 222*t2 + 64*Power(t2,2)) + 
                  2*(-127 + 738*t2 - 1107*Power(t2,2) + 540*Power(t2,3))\
)) - Power(s1,2)*(220 - 2982*t2 + 11478*Power(t2,2) - 
               18124*Power(t2,3) + 13479*Power(t2,4) - 
               5617*Power(t2,5) + 1275*Power(t2,6) + 
               2*Power(s2,3)*
                (-3 + 41*t2 - 96*Power(t2,2) + 52*Power(t2,3)) + 
               Power(t1,3)*(-1193 + 5376*t2 - 7128*Power(t2,2) + 
                  2760*Power(t2,3)) + 
               Power(t1,2)*(623 + 1133*t2 - 6077*Power(t2,2) + 
                  5497*Power(t2,3) - 1532*Power(t2,4)) + 
               t1*(1 - 418*t2 - 3135*Power(t2,2) + 14879*Power(t2,3) - 
                  18062*Power(t2,4) + 5999*Power(t2,5)) + 
               Power(s2,2)*(146 - 1566*t2 + 4077*Power(t2,2) - 
                  3745*Power(t2,3) + 995*Power(t2,4) + 
                  t1*(-55 + 532*t2 - 718*Power(t2,2) + 42*Power(t2,3))) \
+ s2*(-169 + 730*t2 + 3017*Power(t2,2) - 11684*Power(t2,3) + 
                  11243*Power(t2,4) - 2580*Power(t2,5) + 
                  Power(t1,2)*
                   (405 - 2580*t2 + 3774*Power(t2,2) - 
                     1226*Power(t2,3)) - 
                  2*t1*(86 - 267*t2 + 1806*Power(t2,2) - 
                     3420*Power(t2,3) + 1520*Power(t2,4)))) + 
            s1*(-41 + (1597 - 1710*s2 + 794*Power(s2,2) - 
                  36*Power(s2,3))*t2 + 
               (-8068 + 5836*s2 - 4539*Power(s2,2) + 272*Power(s2,3))*
                Power(t2,2) - 
               8*(-2219 + 265*s2 - 926*Power(s2,2) + 51*Power(s2,3))*
                Power(t2,3) + 
               (-18680 - 8320*s2 - 4081*Power(s2,2) + 77*Power(s2,3))*
                Power(t2,4) + 
               (9093 + 7884*s2 + 651*Power(s2,2))*Power(t2,5) - 
               119*(23 + 12*s2)*Power(t2,6) + 770*Power(t2,7) + 
               3*Power(t1,3)*
                (105 - 832*t2 + 1853*Power(t2,2) - 1528*Power(t2,3) + 
                  392*Power(t2,4)) + 
               Power(t1,2)*(-557 + 1602*t2 + 6*Power(t2,2) - 
                  2561*Power(t2,3) + 2031*Power(t2,4) - 
                  532*Power(t2,5) + 
                  s2*(-50 + 692*t2 - 1539*Power(t2,2) + 
                     670*Power(t2,3) + 238*Power(t2,4))) + 
               t1*(253 - 884*t2 + 204*Power(t2,2) - 1414*Power(t2,3) + 
                  9831*Power(t2,4) - 11350*Power(t2,5) + 
                  3206*Power(t2,6) - 
                  Power(s2,2)*t2*
                   (220 - 553*t2 + 46*Power(t2,2) + 175*Power(t2,3)) + 
                  s2*(52 + 482*t2 - 6*Power(t2,2) - 5762*Power(t2,3) + 
                     7672*Power(t2,4) - 2632*Power(t2,5))))) + 
         Power(s,5)*(4 + 30*t1 - 114*Power(t1,2) + 84*Power(t1,3) + 
            94*t2 - 102*s2*t2 - 452*t1*t2 + 344*s2*t1*t2 + 
            615*Power(t1,2)*t2 - 238*s2*Power(t1,2)*t2 - 
            308*Power(t1,3)*t2 + 2284*Power(t2,2) - 
            2320*s2*Power(t2,2) + 732*Power(s2,2)*Power(t2,2) - 
            24*Power(s2,3)*Power(t2,2) - 1268*t1*Power(t2,2) + 
            710*s2*t1*Power(t2,2) - 278*Power(s2,2)*t1*Power(t2,2) - 
            438*Power(t1,2)*Power(t2,2) + 
            763*s2*Power(t1,2)*Power(t2,2) + 8*Power(t1,3)*Power(t2,2) - 
            9174*Power(t2,3) + 10710*s2*Power(t2,3) - 
            4282*Power(s2,2)*Power(t2,3) + 218*Power(s2,3)*Power(t2,3) + 
            3848*t1*Power(t2,3) - 3754*s2*t1*Power(t2,3) + 
            966*Power(s2,2)*t1*Power(t2,3) - 
            351*Power(t1,2)*Power(t2,3) - 
            982*s2*Power(t1,2)*Power(t2,3) + 
            1142*Power(t1,3)*Power(t2,3) + 15297*Power(t2,4) - 
            15346*s2*Power(t2,4) + 8295*Power(s2,2)*Power(t2,4) - 
            492*Power(s2,3)*Power(t2,4) - 999*t1*Power(t2,4) + 
            1856*s2*t1*Power(t2,4) - 879*Power(s2,2)*t1*Power(t2,4) + 
            219*Power(t1,2)*Power(t2,4) + 
            361*s2*Power(t1,2)*Power(t2,4) - 
            1615*Power(t1,3)*Power(t2,4) - 14135*Power(t2,5) + 
            6859*s2*Power(t2,5) - 6377*Power(s2,2)*Power(t2,5) + 
            315*Power(s2,3)*Power(t2,5) - 973*t1*Power(t2,5) + 
            3086*s2*t1*Power(t2,5) + 147*Power(s2,2)*t1*Power(t2,5) + 
            229*Power(t1,2)*Power(t2,5) + 
            315*s2*Power(t1,2)*Power(t2,5) + 
            791*Power(t1,3)*Power(t2,5) + 6280*Power(t2,6) + 
            1820*s2*Power(t2,6) + 1715*Power(s2,2)*Power(t2,6) - 
            1751*t1*Power(t2,6) - 2884*s2*t1*Power(t2,6) - 
            245*Power(t1,2)*Power(t2,6) - 
            182*s2*Power(t1,2)*Power(t2,6) - 
            112*Power(t1,3)*Power(t2,6) - 331*Power(t2,7) - 
            1827*s2*Power(t2,7) - 112*Power(s2,2)*Power(t2,7) + 
            1897*t1*Power(t2,7) + 700*s2*t1*Power(t2,7) + 
            56*Power(t1,2)*Power(t2,7) - 259*Power(t2,8) + 
            210*s2*Power(t2,8) - 336*t1*Power(t2,8) - 56*Power(t2,9) + 
            2*Power(s1,6)*(17 + 20*Power(s2,3) - 84*Power(t1,3) - 
               45*t2 + 30*Power(t2,2) - 2*Power(t2,3) + 
               14*Power(t1,2)*(-7 + 4*t2) - 
               5*Power(s2,2)*(2 + 21*t1 + 5*t2) + 
               t1*(-65 + 132*t2 - 77*Power(t2,2)) + 
               s2*(25 + 168*Power(t1,2) + t1*(85 - 7*t2) - 52*t2 + 
                  37*Power(t2,2))) + 
            Power(s1,5)*(206 + Power(s2,3)*(97 - 235*t2) - 888*t2 + 
               1356*Power(t2,2) - 769*Power(t2,3) + 86*Power(t2,4) + 
               14*Power(t1,3)*(-64 + 93*t2) + 
               Power(t1,2)*(-1042 + 2545*t2 - 1146*Power(t2,2)) + 
               Power(s2,2)*(-104 - 665*t1 + 256*t2 + 1275*t1*t2 + 
                  102*Power(t2,2)) + 
               t1*(-572 + 2564*t2 - 3197*Power(t2,2) + 
                  1221*Power(t2,3)) + 
               s2*(316 - 1354*t2 + 1538*Power(t2,2) - 515*Power(t2,3) - 
                  9*Power(t1,2)*(-155 + 252*t2) + 
                  2*t1*(351 - 893*t2 + 240*Power(t2,2)))) + 
            Power(s1,4)*(478 - 2851*t2 + 6036*Power(t2,2) - 
               6158*Power(t2,3) + 3012*Power(t2,4) - 460*Power(t2,5) + 
               Power(s2,3)*(41 - 266*t2 + 252*Power(t2,2)) - 
               2*Power(t1,3)*(859 - 2522*t2 + 1726*Power(t2,2)) + 
               Power(t1,2)*(-1297 + 6695*t2 - 8495*Power(t2,2) + 
                  3073*Power(t2,3)) - 
               2*t1*(74 - 1905*t2 + 5971*Power(t2,2) - 
                  6202*Power(t2,3) + 1930*Power(t2,4)) + 
               Power(s2,2)*(119 - 318*t2 + 649*Power(t2,2) - 
                  387*Power(t2,3) - 
                  2*t1*(268 - 1175*t2 + 978*Power(t2,2))) + 
               s2*(331 - 2948*t2 + 7401*Power(t2,2) - 
                  6558*Power(t2,3) + 1505*Power(t2,4) + 
                  Power(t1,2)*(1779 - 6148*t2 + 4611*Power(t2,2)) + 
                  t1*(194 - 2094*t2 + 2310*Power(t2,2) - 
                     466*Power(t2,3)))) + 
            Power(s1,3)*(517 - 4637*t2 + 13576*Power(t2,2) - 
               17524*Power(t2,3) + 11869*Power(t2,4) - 
               4934*Power(t2,5) + 1000*Power(t2,6) + 
               Power(s2,3)*(-39 + 206*t2 - 249*Power(t2,2) + 
                  140*Power(t2,3)) + 
               Power(t1,3)*(-1627 + 7533*t2 - 10537*Power(t2,2) + 
                  4403*Power(t2,3)) + 
               Power(t1,2)*(-41 + 4887*t2 - 13544*Power(t2,2) + 
                  11880*Power(t2,3) - 3575*Power(t2,4)) + 
               t1*(525 - 1523*t2 - 6308*Power(t2,2) + 
                  23404*Power(t2,3) - 22861*Power(t2,4) + 
                  6195*Power(t2,5)) + 
               Power(s2,2)*(537 - 3320*t2 + 6436*Power(t2,2) - 
                  4990*Power(t2,3) + 1069*Power(t2,4) + 
                  6*t1*(-24 + 196*t2 - 338*Power(t2,2) + 
                     103*Power(t2,3))) + 
               s2*(-438 + 541*t2 + 6478*Power(t2,2) - 
                  17200*Power(t2,3) + 13519*Power(t2,4) - 
                  2355*Power(t2,5) + 
                  Power(t1,2)*
                   (883 - 5495*t2 + 8796*Power(t2,2) - 
                     3641*Power(t2,3)) - 
                  2*t1*(220 - 945*t2 + 2288*Power(t2,2) - 
                     2773*Power(t2,3) + 867*Power(t2,4)))) - 
            Power(s1,2)*(-514 + 4937*t2 - 17836*Power(t2,2) + 
               31135*Power(t2,3) - 26067*Power(t2,4) + 
               10527*Power(t2,5) - 3345*Power(t2,6) + 
               1000*Power(t2,7) + 
               Power(s2,3)*(12 - 269*t2 + 1065*Power(t2,2) - 
                  1224*Power(t2,3) + 316*Power(t2,4)) + 
               Power(t1,3)*(689 - 5161*t2 + 11808*Power(t2,2) - 
                  10346*Power(t2,3) + 2910*Power(t2,4)) + 
               Power(t1,2)*(-725 + 559*t2 + 5672*Power(t2,2) - 
                  11204*Power(t2,3) + 7691*Power(t2,4) - 
                  2010*Power(t2,5)) + 
               t1*(363 + 1019*t2 - 5995*Power(t2,2) - 
                  1249*Power(t2,3) + 22140*Power(t2,4) - 
                  21860*Power(t2,5) + 5250*Power(t2,6)) + 
               Power(s2,2)*(-277 + 4004*t2 - 13781*Power(t2,2) + 
                  17759*Power(t2,3) - 8836*Power(t2,4) + 
                  1287*Power(t2,5) + 
                  t1*(79 - 765*t2 + 1188*Power(t2,2) + 
                     188*Power(t2,3) - 630*Power(t2,4))) - 
               s2*(-648 + 6289*t2 - 12601*Power(t2,2) + 
                  29*Power(t2,3) + 19345*Power(t2,4) - 
                  14816*Power(t2,5) + 2075*Power(t2,6) + 
                  Power(t1,2)*
                   (144 - 2125*t2 + 5736*Power(t2,2) - 
                     4530*Power(t2,3) + 636*Power(t2,4)) + 
                  2*t1*(75 - 106*t2 - 2918*Power(t2,2) + 
                     8418*Power(t2,3) - 7283*Power(t2,4) + 
                     1875*Power(t2,5)))) + 
            s1*(28 - 2545*t2 + 12815*Power(t2,2) - 28460*Power(t2,3) + 
               34205*Power(t2,4) - 19984*Power(t2,5) + 
               3880*Power(t2,6) - 455*Power(t2,7) + 434*Power(t2,8) + 
               Power(s2,3)*t2*
                (36 - 448*t2 + 1314*Power(t2,2) - 1125*Power(t2,3) + 
                  119*Power(t2,4)) + 
               Power(t1,3)*(37 + 1013*t2 - 4841*Power(t2,2) + 
                  7619*Power(t2,3) - 4740*Power(t2,4) + 938*Power(t2,5)) \
+ Power(t1,2)*(92 - 1085*t2 + 1064*Power(t2,2) + 1974*Power(t2,3) - 
                  3568*Power(t2,4) + 2211*Power(t2,5) - 532*Power(t2,6)) \
+ t1*(-102 + 1890*t2 - 2629*Power(t2,2) - 3891*Power(t2,3) + 
                  2924*Power(t2,4) + 9968*Power(t2,5) - 
                  10364*Power(t2,6) + 2184*Power(t2,7)) + 
               Power(s2,2)*t2*
                (-980 + 7724*t2 - 18880*Power(t2,2) + 
                  18131*Power(t2,3) - 6454*Power(t2,4) + 
                  665*Power(t2,5) + 
                  t1*(354 - 1596*t2 + 1434*Power(t2,2) + 
                     389*Power(t2,3) - 357*Power(t2,4))) + 
               s2*(20 + 2850*t2 - 16109*Power(t2,2) + 
                  26793*Power(t2,3) - 10708*Power(t2,4) - 
                  10054*Power(t2,5) + 8244*Power(t2,6) - 
                  994*Power(t2,7) + 
                  Power(t1,2)*
                   (46 - 571*t2 + 2114*Power(t2,2) - 2437*Power(t2,3) + 
                     198*Power(t2,4) + 504*Power(t2,5)) - 
                  2*t1*(39 + 569*t2 - 2080*Power(t2,2) - 
                     1101*Power(t2,3) + 7003*Power(t2,4) - 
                     5607*Power(t2,5) + 1358*Power(t2,6))))) - 
         s*(s1 - t2)*(-1 + t2)*
          (Power(s1,8)*Power(s2 - t1,2)*(5*s2 - 5*t1 + 12*(-1 + t2)) - 
            Power(-1 + t2,2)*
             (10 + (60 - 18*s2)*t2 + 
               (-421 + 258*s2 - 24*Power(s2,2))*Power(t2,2) + 
               (792 - 863*s2 + 195*Power(s2,2) - 4*Power(s2,3))*
                Power(t2,3) + 
               (-676 + 1072*s2 - 434*Power(s2,2) + 33*Power(s2,3))*
                Power(t2,4) + 
               (301 - 487*s2 + 282*Power(s2,2) - 41*Power(s2,3))*
                Power(t2,5) + 
               (-56 + 33*s2 - 19*Power(s2,2))*Power(t2,6) + 
               (-17 + 4*s2)*Power(t2,7) + (7 + s2)*Power(t2,8) + 
               Power(t1,3)*(-10 + 12*t2 - 22*Power(t2,2) + 
                  47*Power(t2,3) - 16*Power(t2,4) + Power(t2,5)) + 
               Power(t1,2)*(30 - 18*(-2 + s2)*t2 + 
                  (-283 + 130*s2)*Power(t2,2) + 
                  (250 - 163*s2)*Power(t2,3) + 
                  (-41 + 14*s2)*Power(t2,4) + 7*Power(t2,5) + 
                  (1 + s2)*Power(t2,6)) + 
               t1*(-30 + 36*(-3 + s2)*t2 + 
                  (726 - 388*s2 + 24*Power(s2,2))*Power(t2,2) + 
                  (-1037 + 924*s2 - 141*Power(s2,2))*Power(t2,3) + 
                  2*(229 - 315*s2 + 85*Power(s2,2))*Power(t2,4) + 
                  (-2 + 52*s2 - 17*Power(s2,2))*Power(t2,5) + 
                  (2 + 8*s2)*Power(t2,6) - (9 + 2*s2)*Power(t2,7))) + 
            Power(s1,7)*(Power(s2,3)*(-10 + t2) + 
               Power(s2,2)*(5 + 38*t2 - 43*Power(t2,2) + 
                  9*t1*(2 + t2)) + 
               t1*(-2*Power(-1 + t2,2)*(3 + 2*t2) + 
                  Power(t1,2)*(-2 + 11*t2) + 
                  t1*(-13 + 74*t2 - 61*Power(t2,2))) + 
               s2*(2*Power(-1 + t2,2)*(1 + 4*t2) - 
                  3*Power(t1,2)*(2 + 7*t2) + 
                  8*t1*(1 - 14*t2 + 13*Power(t2,2)))) + 
            Power(s1,6)*(2*(-4 + t2)*Power(-1 + t2,3) + 
               Power(s2,3)*(-32 + 97*t2 - 73*Power(t2,2)) - 
               4*Power(t1,3)*(3 - 6*t2 + Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(30 + 30*t2 + 17*Power(t2,2)) + 
               Power(t1,2)*(26 + 25*t2 - 197*Power(t2,2) + 
                  146*Power(t2,3)) + 
               Power(s2,2)*(106 - 239*t2 + 91*Power(t2,2) + 
                  42*Power(t2,3) + t1*(67 - 200*t2 + 157*Power(t2,2))) \
- s2*(Power(-1 + t2,2)*(58 - 18*t2 + 37*Power(t2,2)) + 
                  Power(t1,2)*(23 - 79*t2 + 80*Power(t2,2)) + 
                  2*t1*(67 - 110*t2 - 50*Power(t2,2) + 93*Power(t2,3)))) \
+ Power(s1,5)*(Power(-1 + t2,3)*(-36 + 25*t2 + 5*Power(t2,2)) + 
               2*Power(t1,3)*
                (-22 + 65*t2 - 55*Power(t2,2) + Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (-126 + 170*t2 + 96*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s2,3)*(-35 + 203*t2 - 325*Power(t2,2) + 
                  179*Power(t2,3)) + 
               Power(t1,2)*(63 - 264*t2 + 115*Power(t2,2) + 
                  292*Power(t2,3) - 206*Power(t2,4)) + 
               Power(s2,2)*(214 - 936*t2 + 1235*Power(t2,2) - 
                  536*Power(t2,3) + 23*Power(t2,4) + 
                  t1*(62 - 435*t2 + 750*Power(t2,2) - 443*Power(t2,3))) \
+ s2*(Power(-1 + t2,2)*(-149 + 309*t2 - 63*Power(t2,2) + 
                     70*Power(t2,3)) + 
                  3*Power(t1,2)*
                   (7 + 30*t2 - 101*Power(t2,2) + 86*Power(t2,3)) + 
                  2*t1*(-151 + 638*t2 - 714*Power(t2,2) + 
                     136*Power(t2,3) + 91*Power(t2,4)))) - 
            Power(s1,4)*(Power(-1 + t2,3)*
                (59 - 164*t2 - 3*Power(t2,2) + 47*Power(t2,3)) + 
               Power(t1,3)*(39 - 254*t2 + 428*Power(t2,2) - 
                  234*Power(t2,3) + 18*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (258 - 770*t2 + 391*Power(t2,2) + 194*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s2,3)*(21 - 217*t2 + 593*Power(t2,2) - 
                  575*Power(t2,3) + 181*Power(t2,4)) + 
               Power(t1,2)*(101 + 14*t2 - 609*Power(t2,2) + 
                  430*Power(t2,3) + 238*Power(t2,4) - 174*Power(t2,5)) \
+ Power(s2,2)*(-241 + 1587*t2 - 3396*Power(t2,2) + 2899*Power(t2,3) - 
                  921*Power(t2,4) + 72*Power(t2,5) + 
                  t1*(33 + 198*t2 - 1105*Power(t2,2) + 
                     1412*Power(t2,3) - 547*Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (318 - 897*t2 + 631*Power(t2,2) - 32*Power(t2,3) + 
                     71*Power(t2,4)) + 
                  Power(t1,2)*
                   (-100 + 324*t2 - 27*Power(t2,2) - 506*Power(t2,3) + 
                     318*Power(t2,4)) + 
                  2*t1*(68 - 843*t2 + 2140*Power(t2,2) - 
                     1802*Power(t2,3) + 384*Power(t2,4) + 53*Power(t2,5)\
))) + Power(s1,3)*(Power(-1 + t2,3)*
                (-166 + 369*t2 - 328*Power(t2,2) - 89*Power(t2,3) + 
                  98*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (-47 + 918*t2 - 1642*Power(t2,2) + 481*Power(t2,3) + 
                  225*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,3)*(43 - 17*t2 - 342*Power(t2,2) + 
                  542*Power(t2,3) - 235*Power(t2,4) + 22*Power(t2,5)) + 
               Power(s2,3)*(-4 + 104*t2 - 552*Power(t2,2) + 
                  996*Power(t2,3) - 636*Power(t2,4) + 79*Power(t2,5)) + 
               Power(t1,2)*(-204 + 986*t2 - 1132*Power(t2,2) - 
                  127*Power(t2,3) + 469*Power(t2,4) + 91*Power(t2,5) - 
                  83*Power(t2,6)) + 
               Power(s2,2)*(135 - 1405*t2 + 4718*Power(t2,2) - 
                  6627*Power(t2,3) + 3922*Power(t2,4) - 
                  794*Power(t2,5) + 51*Power(t2,6) - 
                  t1*(81 - 409*t2 + 172*Power(t2,2) + 
                     1193*Power(t2,3) - 1449*Power(t2,4) + 
                     373*Power(t2,5))) + 
               s2*(Power(-1 + t2,2)*
                   (-418 + 1831*t2 - 2248*Power(t2,2) + 
                     652*Power(t2,3) + 76*Power(t2,4) + 44*Power(t2,5)) \
+ Power(t1,2)*(14 - 425*t2 + 1086*Power(t2,2) - 551*Power(t2,3) - 
                     370*Power(t2,4) + 207*Power(t2,5)) + 
                  t1*(302 - 520*t2 - 2312*Power(t2,2) + 
                     6240*Power(t2,3) - 4598*Power(t2,4) + 
                     860*Power(t2,5) + 28*Power(t2,6)))) - 
            Power(s1,2)*(Power(s2,3)*t2*
                (-12 + 186*t2 - 704*Power(t2,2) + 980*Power(t2,3) - 
                  463*Power(t2,4) + 7*Power(t2,5)) + 
               Power(-1 + t2,3)*
                (212 - 746*t2 + 790*Power(t2,2) - 374*Power(t2,3) - 
                  119*Power(t2,4) + 92*Power(t2,5)) + 
               t1*Power(-1 + t2,2)*
                (285 - 553*t2 - 703*Power(t2,2) + 1488*Power(t2,3) - 
                  317*Power(t2,4) - 147*Power(t2,5) + 3*Power(t2,6)) + 
               Power(t1,3)*(-45 + 281*t2 - 455*Power(t2,2) + 
                  106*Power(t2,3) + 209*Power(t2,4) - 99*Power(t2,5) + 
                  9*Power(t2,6)) + 
               Power(t1,2)*(-28 - 384*t2 + 1763*Power(t2,2) - 
                  2201*Power(t2,3) + 704*Power(t2,4) + 
                  154*Power(t2,5) + 12*Power(t2,6) - 20*Power(t2,7)) + 
               Power(s2,2)*(-24 + 513*t2 - 2935*Power(t2,2) + 
                  6876*Power(t2,3) - 7297*Power(t2,4) + 
                  3242*Power(t2,5) - 389*Power(t2,6) + 
                  14*Power(t2,7) + 
                  t1*(24 - 351*t2 + 1195*Power(t2,2) - 
                     1188*Power(t2,3) - 272*Power(t2,4) + 
                     753*Power(t2,5) - 143*Power(t2,6))) + 
               s2*(Power(-1 + t2,2)*
                   (204 - 1651*t2 + 3768*Power(t2,2) - 
                     2888*Power(t2,3) + 390*Power(t2,4) + 
                     102*Power(t2,5) + 19*Power(t2,6)) + 
                  Power(t1,2)*
                   (76 - 239*t2 - 242*Power(t2,2) + 1165*Power(t2,3) - 
                     773*Power(t2,4) - 77*Power(t2,5) + 72*Power(t2,6)) \
- 2*t1*(140 - 996*t2 + 1995*Power(t2,2) - 659*Power(t2,3) - 
                     1686*Power(t2,4) + 1457*Power(t2,5) - 
                     256*Power(t2,6) + 5*Power(t2,7)))) + 
            s1*(-1 + t2)*(Power(s2,3)*Power(t2,2)*
                (12 - 132*t2 + 313*Power(t2,2) - 208*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(t1,3)*(-4 + 85*t2 - 288*Power(t2,2) + 
                  281*Power(t2,3) - 49*Power(t2,4) - 8*Power(t2,5) + 
                  Power(t2,6)) + 
               Power(-1 + t2,2)*
                (-68 + 551*t2 - 1021*Power(t2,2) + 715*Power(t2,3) - 
                  240*Power(t2,4) - 60*Power(t2,5) + 41*Power(t2,6)) + 
               Power(t1,2)*(-60 + 329*t2 - 247*Power(t2,2) - 
                  415*Power(t2,3) + 400*Power(t2,4) - 6*Power(t2,5) + 
                  Power(t2,6) - 2*Power(t2,7)) + 
               t1*(132 - 1101*t2 + 2570*Power(t2,2) - 
                  2016*Power(t2,3) - 73*Power(t2,4) + 573*Power(t2,5) - 
                  32*Power(t2,6) - 54*Power(t2,7) + Power(t2,8)) + 
               Power(s2,2)*t2*
                (48 - 573*t2 + 2046*Power(t2,2) - 2830*Power(t2,3) + 
                  1423*Power(t2,4) - 115*Power(t2,5) + Power(t2,6) + 
                  t1*(-48 + 411*t2 - 884*Power(t2,2) + 
                     494*Power(t2,3) + 106*Power(t2,4) - 25*Power(t2,5))\
) + s2*(18 - 480*t2 + 2558*Power(t2,2) - 5423*Power(t2,3) + 
                  5202*Power(t2,4) - 2026*Power(t2,5) + 
                  110*Power(t2,6) + 35*Power(t2,7) + 6*Power(t2,8) + 
                  Power(t1,2)*
                   (18 - 224*t2 + 470*Power(t2,2) - 83*Power(t2,3) - 
                     274*Power(t2,4) + 27*Power(t2,5) + 12*Power(t2,6)) \
- 2*t1*(18 - 352*t2 + 1361*Power(t2,2) - 1739*Power(t2,3) + 
                     502*Power(t2,4) + 274*Power(t2,5) - 
                     69*Power(t2,6) + 5*Power(t2,7))))) + 
         Power(s,4)*(14 - 84*t1 + 138*Power(t1,2) - 68*Power(t1,3) - 
            234*t2 + 112*s2*t2 + 895*t1*t2 - 270*s2*t1*t2 - 
            1149*Power(t1,2)*t2 + 154*s2*Power(t1,2)*t2 + 
            504*Power(t1,3)*t2 - 1647*Power(t2,2) + 
            1480*s2*Power(t2,2) - 390*Power(s2,2)*Power(t2,2) + 
            6*Power(s2,3)*Power(t2,2) + 423*t1*Power(t2,2) - 
            600*s2*t1*Power(t2,2) + 227*Power(s2,2)*t1*Power(t2,2) + 
            2106*Power(t1,2)*Power(t2,2) - 
            505*s2*Power(t1,2)*Power(t2,2) - 
            1177*Power(t1,3)*Power(t2,2) + 9561*Power(t2,3) - 
            10590*s2*Power(t2,3) + 3279*Power(s2,2)*Power(t2,3) - 
            128*Power(s2,3)*Power(t2,3) - 6650*t1*Power(t2,3) + 
            5852*s2*t1*Power(t2,3) - 1286*Power(s2,2)*t1*Power(t2,3) - 
            1120*Power(t1,2)*Power(t2,3) + 
            806*s2*Power(t1,2)*Power(t2,3) + 
            993*Power(t1,3)*Power(t2,3) - 19074*Power(t2,4) + 
            23734*s2*Power(t2,4) - 9448*Power(s2,2)*Power(t2,4) + 
            550*Power(s2,3)*Power(t2,4) + 7977*t1*Power(t2,4) - 
            9334*s2*t1*Power(t2,4) + 2250*Power(s2,2)*t1*Power(t2,4) - 
            93*Power(t1,2)*Power(t2,4) - 
            855*s2*Power(t1,2)*Power(t2,4) + 
            190*Power(t1,3)*Power(t2,4) + 20730*Power(t2,5) - 
            23112*s2*Power(t2,5) + 11935*Power(s2,2)*Power(t2,5) - 
            810*Power(s2,3)*Power(t2,5) - 994*t1*Power(t2,5) + 
            3420*s2*t1*Power(t2,5) - 1375*Power(s2,2)*t1*Power(t2,5) + 
            67*Power(t1,2)*Power(t2,5) + 
            290*s2*Power(t1,2)*Power(t2,5) - 
            772*Power(t1,3)*Power(t2,5) - 12898*Power(t2,6) + 
            8451*s2*Power(t2,6) - 6615*Power(s2,2)*Power(t2,6) + 
            385*Power(s2,3)*Power(t2,6) - 1523*t1*Power(t2,6) + 
            2500*s2*t1*Power(t2,6) + 175*Power(s2,2)*t1*Power(t2,6) + 
            184*Power(t1,2)*Power(t2,6) + 
            231*s2*Power(t1,2)*Power(t2,6) + 
            371*Power(t1,3)*Power(t2,6) + 3050*Power(t2,7) + 
            840*s2*Power(t2,7) + 1295*Power(s2,2)*Power(t2,7) - 
            853*t1*Power(t2,7) - 1960*s2*t1*Power(t2,7) - 
            161*Power(t1,2)*Power(t2,7) - 
            112*s2*Power(t1,2)*Power(t2,7) - 
            44*Power(t1,3)*Power(t2,7) + 933*Power(t2,8) - 
            987*s2*Power(t2,8) - 56*Power(s2,2)*Power(t2,8) + 
            917*t1*Power(t2,8) + 392*s2*t1*Power(t2,8) + 
            28*Power(t1,2)*Power(t2,8) - 431*Power(t2,9) + 
            72*s2*Power(t2,9) - 108*t1*Power(t2,9) - 4*Power(t2,10) - 
            2*Power(s1,7)*(3 + 15*Power(s2,3) - 36*Power(t1,3) - 7*t2 + 
               4*Power(t2,2) + 14*Power(t1,2)*(-3 + 2*t2) - 
               Power(s2,2)*(10 + 63*t1 + 3*t2) + 
               t1*(-25 + 43*t2 - 19*Power(t2,2)) + 
               s2*(11 + 84*Power(t1,2) + t1*(48 - 21*t2) - 16*t2 + 
                  6*Power(t2,2))) + 
            Power(s1,6)*(-64 + Power(t1,3)*(518 - 756*t2) + 268*t2 - 
               386*Power(t2,2) + 193*Power(t2,3) - 11*Power(t2,4) + 
               2*Power(s2,3)*(-59 + 120*t2) + 
               Power(s2,2)*(186 + t1*(610 - 1077*t2) - 456*t2 + 
                  157*Power(t2,2)) + 
               Power(t1,2)*(746 - 1735*t2 + 856*Power(t2,2)) + 
               t1*(440 - 1472*t2 + 1410*Power(t2,2) - 
                  375*Power(t2,3)) + 
               s2*(-244 + 764*t2 - 597*Power(t2,2) + 74*Power(t2,3) + 
                  Power(t1,2)*(-999 + 1582*t2) - 
                  2*t1*(386 - 927*t2 + 418*Power(t2,2)))) - 
            Power(s1,5)*(294 - 1335*t2 + 2497*Power(t2,2) - 
               2431*Power(t2,3) + 1089*Power(t2,4) - 114*Power(t2,5) + 
               Power(t1,3)*(-1166 + 3526*t2 - 2519*Power(t2,2)) + 
               Power(s2,3)*(62 - 366*t2 + 375*Power(t2,2)) + 
               Power(t1,2)*(-1329 + 5878*t2 - 7575*Power(t2,2) + 
                  3008*Power(t2,3)) - 
               2*t1*(190 - 1846*t2 + 4290*Power(t2,2) - 
                  3429*Power(t2,3) + 759*Power(t2,4)) + 
               Power(s2,2)*(54 + 173*t2 - 562*Power(t2,2) + 
                  349*Power(t2,3) + 
                  t1*(-609 + 2572*t2 - 2250*Power(t2,2))) + 
               s2*(265 - 2158*t2 + 4577*Power(t2,2) - 
                  3008*Power(t2,3) + 252*Power(t2,4) + 
                  Power(t1,2)*(1570 - 5428*t2 + 4233*Power(t2,2)) - 
                  4*t1*(-148 + 902*t2 - 1337*Power(t2,2) + 
                     582*Power(t2,3)))) + 
            Power(s1,4)*(-513 + 3531*t2 - 8084*Power(t2,2) + 
               8770*Power(t2,3) - 5653*Power(t2,4) + 2339*Power(t2,5) - 
               390*Power(t2,6) + 
               Power(t1,3)*(1331 - 6374*t2 + 9317*Power(t2,2) - 
                  4135*Power(t2,3)) + 
               Power(s2,3)*(96 - 298*t2 + 193*Power(t2,2) - 
                  80*Power(t2,3)) + 
               Power(t1,2)*(676 - 6447*t2 + 15495*Power(t2,2) - 
                  14212*Power(t2,3) + 4710*Power(t2,4)) + 
               t1*(-779 + 982*t2 + 8294*Power(t2,2) - 
                  21342*Power(t2,3) + 16311*Power(t2,4) - 
                  3290*Power(t2,5)) + 
               Power(s2,2)*(-826 + 3811*t2 - 5737*Power(t2,2) + 
                  3090*Power(t2,3) - 120*Power(t2,4) + 
                  t1*(83 - 1252*t2 + 2753*Power(t2,2) - 
                     1255*Power(t2,3))) + 
               s2*(482 - 298*t2 - 5879*Power(t2,2) + 
                  12968*Power(t2,3) - 8069*Power(t2,4) + 
                  620*Power(t2,5) + 
                  Power(t1,2)*
                   (-987 + 6164*t2 - 10326*Power(t2,2) + 
                     4770*Power(t2,3)) + 
                  t1*(726 - 2048*t2 + 728*Power(t2,2) + 
                     2034*Power(t2,3) - 1880*Power(t2,4)))) + 
            Power(s1,3)*(-794 + 5828*t2 - 17549*Power(t2,2) + 
               24533*Power(t2,3) - 15691*Power(t2,4) + 
               4909*Power(t2,5) - 1836*Power(t2,6) + 600*Power(t2,7) + 
               2*Power(s2,3)*
                (34 - 377*t2 + 1029*Power(t2,2) - 968*Power(t2,3) + 
                  290*Power(t2,4)) + 
               2*Power(t1,3)*
                (390 - 2869*t2 + 6733*Power(t2,2) - 6204*Power(t2,3) + 
                  1880*Power(t2,4)) - 
               Power(t1,2)*(139 + 2364*t2 - 11406*Power(t2,2) + 
                  18496*Power(t2,3) - 13347*Power(t2,4) + 
                  3810*Power(t2,5)) + 
               2*t1*(-210 + 2865*t2 - 6031*Power(t2,2) - 
                  1432*Power(t2,3) + 13450*Power(t2,4) - 
                  10735*Power(t2,5) + 2015*Power(t2,6)) + 
               Power(s2,2)*(-898 + 7793*t2 - 20685*Power(t2,2) + 
                  22441*Power(t2,3) - 9603*Power(t2,4) + 
                  930*Power(t2,5) + 
                  t1*(133 - 516*t2 + 170*Power(t2,2) + 
                     1004*Power(t2,3) - 975*Power(t2,4))) - 
               s2*(-1559 + 9060*t2 - 13797*Power(t2,2) - 
                  2326*Power(t2,3) + 20218*Power(t2,4) - 
                  12692*Power(t2,5) + 940*Power(t2,6) + 
                  Power(t1,2)*
                   (260 - 3292*t2 + 9049*Power(t2,2) - 
                     8340*Power(t2,3) + 2015*Power(t2,4)) + 
                  2*t1*(34 + 1678*t2 - 7468*Power(t2,2) + 
                     10584*Power(t2,3) - 5402*Power(t2,4) + 
                     535*Power(t2,5)))) - 
            Power(s1,2)*(791 - 7063*t2 + 23971*Power(t2,2) - 
               43509*Power(t2,3) + 41904*Power(t2,4) - 
               16660*Power(t2,5) - 206*Power(t2,6) + 337*Power(t2,7) + 
               435*Power(t2,8) + 
               2*Power(s2,3)*
                (-3 + 129*t2 - 881*Power(t2,2) + 1899*Power(t2,3) - 
                  1410*Power(t2,4) + 220*Power(t2,5)) + 
               Power(t1,3)*(-23 + 1846*t2 - 8267*Power(t2,2) + 
                  13290*Power(t2,3) - 8720*Power(t2,4) + 
                  1892*Power(t2,5)) + 
               Power(t1,2)*(226 - 333*t2 - 3319*Power(t2,2) + 
                  9401*Power(t2,3) - 10609*Power(t2,4) + 
                  6379*Power(t2,5) - 1616*Power(t2,6)) + 
               t1*(-773 + 1990*t2 + 7086*Power(t2,2) - 
                  21903*Power(t2,3) + 8899*Power(t2,4) + 
                  17739*Power(t2,5) - 15806*Power(t2,6) + 
                  2723*Power(t2,7)) + 
               Power(s2,2)*(232 - 4583*t2 + 22120*Power(t2,2) - 
                  41972*Power(t2,3) + 34542*Power(t2,4) - 
                  11175*Power(t2,5) + 981*Power(t2,6) + 
                  t1*(-105 + 1240*t2 - 2799*Power(t2,2) + 
                     315*Power(t2,3) + 2890*Power(t2,4) - 
                     1344*Power(t2,5))) + 
               s2*(-846 + 10790*t2 - 36694*Power(t2,2) + 
                  46940*Power(t2,3) - 13752*Power(t2,4) - 
                  16978*Power(t2,5) + 11387*Power(t2,6) - 
                  802*Power(t2,7) + 
                  Power(t1,2)*
                   (-41 - 784*t2 + 4548*Power(t2,2) - 
                     6488*Power(t2,3) + 2300*Power(t2,4) + 
                     342*Power(t2,5)) - 
                  2*t1*(-295 + 2016*t2 - 764*Power(t2,2) - 
                     9923*Power(t2,3) + 16211*Power(t2,4) - 
                     8502*Power(t2,5) + 1394*Power(t2,6)))) + 
            s1*(52 + 2597*t2 - 15574*Power(t2,2) + 37388*Power(t2,3) - 
               49819*Power(t2,4) + 37067*Power(t2,5) - 
               10552*Power(t2,6) - 2454*Power(t2,7) + 1169*Power(t2,8) + 
               126*Power(t2,9) + 
               Power(s2,3)*t2*
                (-12 + 318*t2 - 1654*Power(t2,2) + 2910*Power(t2,3) - 
                  1710*Power(t2,4) + 105*Power(t2,5)) + 
               Power(t1,3)*(-227 + 748*t2 + 305*Power(t2,2) - 
                  4060*Power(t2,3) + 5775*Power(t2,4) - 
                  2988*Power(t2,5) + 476*Power(t2,6)) + 
               Power(t1,2)*(407 - 803*t2 + 546*Power(t2,2) - 
                  1742*Power(t2,3) + 3172*Power(t2,4) - 
                  2679*Power(t2,5) + 1485*Power(t2,6) - 336*Power(t2,7)) \
+ 2*t1*(-124 - 1013*t2 + 4476*Power(t2,2) - 2643*Power(t2,3) - 
                  5207*Power(t2,4) + 4137*Power(t2,5) + 
                  2935*Power(t2,6) - 3015*Power(t2,7) + 455*Power(t2,8)) \
+ Power(s2,2)*t2*(624 - 6970*t2 + 24607*Power(t2,2) - 
                  36981*Power(t2,3) + 24440*Power(t2,4) - 
                  6083*Power(t2,5) + 413*Power(t2,6) + 
                  t1*(-334 + 2399*t2 - 4622*Power(t2,2) + 
                     2165*Power(t2,3) + 920*Power(t2,4) - 
                     413*Power(t2,5))) + 
               s2*(-40 - 2440*t2 + 19791*Power(t2,2) - 
                  51736*Power(t2,3) + 56776*Power(t2,4) - 
                  20564*Power(t2,5) - 6733*Power(t2,6) + 
                  5308*Power(t2,7) - 364*Power(t2,8) + 
                  Power(t1,2)*
                   (-54 + 242*t2 - 1212*Power(t2,2) + 
                     3136*Power(t2,3) - 2365*Power(t2,4) - 
                     366*Power(t2,5) + 518*Power(t2,6)) - 
                  2*t1*(-49 - 763*t2 + 4952*Power(t2,2) - 
                     6670*Power(t2,3) - 2107*Power(t2,4) + 
                     8663*Power(t2,5) - 4858*Power(t2,6) + 
                     882*Power(t2,7))))) - 
         Power(s,2)*(2*Power(s1,9)*Power(s2 - t1,2)*
             (-1 + s2 - t1 + t2) - 
            Power(-1 + t2,2)*
             (4 + (-71 + 8*s2)*t2 + 
               (-293 + 155*s2 - 14*Power(s2,2))*Power(t2,2) + 
               (2048 - 1628*s2 + 275*Power(s2,2) - 2*Power(s2,3))*
                Power(t2,3) + 
               (-3825 + 4584*s2 - 1303*Power(s2,2) + 53*Power(s2,3))*
                Power(t2,4) + 
               (3503 - 5260*s2 + 2210*Power(s2,2) - 180*Power(s2,3))*
                Power(t2,5) + 
               (-1845 + 2380*s2 - 1325*Power(s2,2) + 147*Power(s2,3))*
                Power(t2,6) + 
               (385 - 182*s2 + 159*Power(s2,2))*Power(t2,7) + 
               (158 - 53*s2 - 2*Power(s2,2))*Power(t2,8) - 
               (65 + 4*s2)*Power(t2,9) + Power(t2,10) - 
               Power(t1,3)*(4 - 83*t2 + 244*Power(t2,2) - 
                  377*Power(t2,3) + 303*Power(t2,4) - 69*Power(t2,5) - 
                  5*Power(t2,6) + Power(t2,7)) + 
               Power(t1,2)*(12 + (-237 + 8*s2)*t2 + 
                  (277 + 63*s2)*Power(t2,2) - 
                  6*(-63 + 59*s2)*Power(t2,3) + 
                  (-527 + 320*s2)*Power(t2,4) + 
                  2*(67 + 10*s2)*Power(t2,5) + 
                  (-27 + 7*s2)*Power(t2,6) - 
                  (11 + 10*s2)*Power(t2,7) + Power(t2,8)) - 
               t1*(12 + (-225 + 16*s2)*t2 - 
                  2*(130 - 109*s2 + 7*Power(s2,2))*Power(t2,2) + 
                  (2727 - 1848*s2 + 215*Power(s2,2))*Power(t2,3) + 
                  (-3895 + 3698*s2 - 675*Power(s2,2))*Power(t2,4) + 
                  (1551 - 2302*s2 + 591*Power(s2,2))*Power(t2,5) + 
                  (158 + 126*s2 - 63*Power(s2,2))*Power(t2,6) + 
                  118*s2*Power(t2,7) - (69 + 26*s2)*Power(t2,8) + 
                  Power(t2,9))) + 
            Power(s1,8)*(Power(s2,3)*(31 - 42*t2) + 
               Power(s2,2)*(-70 + 151*t2 - 81*Power(t2,2) + 
                  3*t1*(-36 + 47*t2)) + 
               t1*(-30*Power(-1 + t2,2) + Power(t1,2)*(-46 + 57*t2) + 
                  t1*(-94 + 199*t2 - 105*Power(t2,2))) + 
               s2*(2*Power(-1 + t2,2)*(11 + 4*t2) - 
                  3*Power(t1,2)*(-41 + 52*t2) + 
                  2*t1*(82 - 175*t2 + 93*Power(t2,2)))) + 
            Power(s1,7)*(2*Power(-1 + t2,3)*(-3 + 7*t2) + 
               Power(t1,3)*(-109 + 352*t2 - 259*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(-83 + 252*t2 + 10*Power(t2,2)) + 
               Power(s2,3)*(-1 - 74*t2 + 91*Power(t2,2)) + 
               Power(t1,2)*(-189 + 897*t2 - 1277*Power(t2,2) + 
                  569*Power(t2,3)) + 
               Power(s2,2)*(-1 + 277*t2 - 601*Power(t2,2) + 
                  325*Power(t2,3) + t1*(-80 + 446*t2 - 414*Power(t2,2))\
) + s2*(-(Power(-1 + t2,2)*(-19 + 114*t2 + 84*Power(t2,2))) + 
                  2*Power(t1,2)*(95 - 362*t2 + 291*Power(t2,2)) - 
                  4*t1*(-43 + 280*t2 - 456*Power(t2,2) + 
                     219*Power(t2,3)))) + 
            Power(s1,6)*(Power(-1 + t2,3)*
                (-81 + 56*t2 - 56*Power(t2,2) + 3*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (-120 - 361*t2 + 962*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s2,3)*(-85 + 272*t2 - 276*Power(t2,2) + 
                  92*Power(t2,3)) + 
               Power(t1,3)*(-166 + 835*t2 - 1254*Power(t2,2) + 
                  582*Power(t2,3)) + 
               Power(t1,2)*(-84 + 1064*t2 - 3165*Power(t2,2) + 
                  3581*Power(t2,3) - 1396*Power(t2,4)) + 
               Power(s2,2)*(369 - 1228*t2 + 951*Power(t2,2) + 
                  413*Power(t2,3) - 505*Power(t2,4) + 
                  3*t1*(46 - 69*t2 - 36*Power(t2,2) + 56*Power(t2,3))) \
+ s2*(-3*Power(t1,2)*(-35 + 292*t2 - 538*Power(t2,2) + 
                     278*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (-205 + 144*t2 + 275*Power(t2,2) + 288*Power(t2,3)) \
+ 2*t1*(-200 + 313*t2 + 759*Power(t2,2) - 1764*Power(t2,3) + 
                     892*Power(t2,4)))) + 
            Power(s1,5)*(Power(s2,3)*
                (-105 + 746*t2 - 1596*Power(t2,2) + 1448*Power(t2,3) - 
                  515*Power(t2,4)) - 
               Power(-1 + t2,3)*
                (159 - 550*t2 + 18*Power(t2,2) + 25*Power(t2,3) + 
                  14*Power(t2,4)) - 
               2*t1*Power(-1 + t2,2)*
                (-310 + 792*t2 + 209*Power(t2,2) - 1086*Power(t2,3) + 
                  18*Power(t2,4)) - 
               2*Power(t1,3)*
                (98 - 627*t2 + 1422*Power(t2,2) - 1312*Power(t2,3) + 
                  408*Power(t2,4)) + 
               Power(t1,2)*(-57 + 298*t2 - 2104*Power(t2,2) + 
                  5430*Power(t2,3) - 5493*Power(t2,4) + 
                  1926*Power(t2,5)) + 
               Power(s2,2)*(701 - 4295*t2 + 8834*Power(t2,2) - 
                  7322*Power(t2,3) + 1779*Power(t2,4) + 
                  303*Power(t2,5) + 
                  t1*(133 - 1200*t2 + 2676*Power(t2,2) - 
                     2518*Power(t2,3) + 975*Power(t2,4))) + 
               2*s2*(3*Power(t1,2)*
                   (21 - 108*t2 + 260*Power(t2,2) - 239*Power(t2,3) + 
                     55*Power(t2,4)) - 
                  Power(-1 + t2,2)*
                   (302 - 918*t2 + 429*Power(t2,2) + 322*Power(t2,3) + 
                     242*Power(t2,4)) + 
                  t1*(-388 + 2515*t2 - 4676*Power(t2,2) + 
                     2440*Power(t2,3) + 1066*Power(t2,4) - 
                     957*Power(t2,5)))) + 
            Power(s1,4)*(Power(-1 + t2,3)*
                (-352 + 1289*t2 - 1668*Power(t2,2) - 490*Power(t2,3) + 
                  444*Power(t2,4) + 25*Power(t2,5)) + 
               t1*Power(-1 + t2,2)*
                (493 - 3920*t2 + 6216*Power(t2,2) - 437*Power(t2,3) - 
                  3091*Power(t2,4) + 155*Power(t2,5)) + 
               Power(s2,3)*(-39 + 648*t2 - 2632*Power(t2,2) + 
                  4156*Power(t2,3) - 2800*Power(t2,4) + 678*Power(t2,5)\
) + Power(t1,3)*(-55 + 957*t2 - 3514*Power(t2,2) + 5222*Power(t2,3) - 
                  3373*Power(t2,4) + 752*Power(t2,5)) + 
               Power(t1,2)*(-364 + 1511*t2 - 1953*Power(t2,2) + 
                  2382*Power(t2,3) - 4862*Power(t2,4) + 
                  4868*Power(t2,5) - 1582*Power(t2,6)) + 
               Power(s2,2)*(597 - 5779*t2 + 18878*Power(t2,2) - 
                  27426*Power(t2,3) + 18261*Power(t2,4) - 
                  4594*Power(t2,5) + 63*Power(t2,6) + 
                  t1*(-153 + 74*t2 + 2700*Power(t2,2) - 
                     6820*Power(t2,3) + 6065*Power(t2,4) - 
                     1899*Power(t2,5))) + 
               s2*(Power(-1 + t2,2)*
                   (-1161 + 5286*t2 - 7078*Power(t2,2) + 
                     1704*Power(t2,3) + 1373*Power(t2,4) + 
                     460*Power(t2,5)) + 
                  Power(t1,2)*
                   (125 - 1116*t2 + 2484*Power(t2,2) - 
                     1830*Power(t2,3) - 104*Power(t2,4) + 
                     474*Power(t2,5)) + 
                  2*t1*(119 + 1409*t2 - 8385*Power(t2,2) + 
                     14532*Power(t2,3) - 9477*Power(t2,4) + 
                     1290*Power(t2,5) + 512*Power(t2,6)))) - 
            Power(s1,3)*(Power(-1 + t2,3)*
                (655 - 2634*t2 + 4186*Power(t2,2) - 2975*Power(t2,3) - 
                  1202*Power(t2,4) + 896*Power(t2,5) + 20*Power(t2,6)) \
+ t1*Power(-1 + t2,2)*(582 + 312*t2 - 7677*Power(t2,2) + 
                  10676*Power(t2,3) - 1496*Power(t2,4) - 
                  2780*Power(t2,5) + 190*Power(t2,6)) + 
               Power(s2,3)*(2 - 174*t2 + 1602*Power(t2,2) - 
                  4830*Power(t2,3) + 6071*Power(t2,4) - 
                  3078*Power(t2,5) + 403*Power(t2,6)) + 
               Power(t1,3)*(-122 + 358*t2 + 856*Power(t2,2) - 
                  4062*Power(t2,3) + 5121*Power(t2,4) - 
                  2584*Power(t2,5) + 437*Power(t2,6)) + 
               Power(t1,2)*(119 - 1915*t2 + 5972*Power(t2,2) - 
                  6540*Power(t2,3) + 2974*Power(t2,4) - 
                  2306*Power(t2,5) + 2461*Power(t2,6) - 765*Power(t2,7)\
) + Power(s2,2)*(-195 + 3400*t2 - 17956*Power(t2,2) + 
                  41482*Power(t2,3) - 46570*Power(t2,4) + 
                  24691*Power(t2,5) - 5041*Power(t2,6) + 
                  189*Power(t2,7) - 
                  3*t1*(-45 + 440*t2 - 970*Power(t2,2) - 
                     160*Power(t2,3) + 2325*Power(t2,4) - 
                     2122*Power(t2,5) + 528*Power(t2,6))) + 
               s2*(Power(-1 + t2,2)*
                   (893 - 7192*t2 + 17155*Power(t2,2) - 
                     14612*Power(t2,3) + 1871*Power(t2,4) + 
                     1810*Power(t2,5) + 268*Power(t2,6)) + 
                  Power(t1,2)*
                   (119 + 128*t2 - 2670*Power(t2,2) + 
                     5076*Power(t2,3) - 2327*Power(t2,4) - 
                     1032*Power(t2,5) + 694*Power(t2,6)) + 
                  2*t1*(-439 + 2491*t2 - 1936*Power(t2,2) - 
                     9020*Power(t2,3) + 19093*Power(t2,4) - 
                     12989*Power(t2,5) + 2772*Power(t2,6) + 
                     28*Power(t2,7)))) + 
            Power(s1,2)*(Power(s2,3)*t2*
                (6 - 288*t2 + 1968*Power(t2,2) - 4883*Power(t2,3) + 
                  5178*Power(t2,4) - 2088*Power(t2,5) + 104*Power(t2,6)\
) + Power(-1 + t2,3)*(-307 + 2839*t2 - 6328*Power(t2,2) + 
                  6501*Power(t2,3) - 3220*Power(t2,4) - 
                  1200*Power(t2,5) + 820*Power(t2,6) + 5*Power(t2,7)) + 
               t1*Power(-1 + t2,2)*
                (-428 + 3493*t2 - 4322*Power(t2,2) - 
                  4610*Power(t2,3) + 8610*Power(t2,4) - 
                  1316*Power(t2,5) - 1536*Power(t2,6) + 105*Power(t2,7)\
) + Power(t1,3)*(104 - 803*t2 + 2058*Power(t2,2) - 1570*Power(t2,3) - 
                  1198*Power(t2,4) + 2340*Power(t2,5) - 
                  1072*Power(t2,6) + 144*Power(t2,7)) + 
               Power(t1,2)*(17 - 14*t2 - 2074*Power(t2,2) + 
                  6958*Power(t2,3) - 7679*Power(t2,4) + 
                  2960*Power(t2,5) - 647*Power(t2,6) + 
                  683*Power(t2,7) - 204*Power(t2,8)) + 
               Power(s2,2)*(14 - 693*t2 + 6876*Power(t2,2) - 
                  26470*Power(t2,3) + 48713*Power(t2,4) - 
                  45071*Power(t2,5) + 19605*Power(t2,6) - 
                  3079*Power(t2,7) + 105*Power(t2,8) - 
                  t1*(14 - 513*t2 + 3300*Power(t2,2) - 
                     7222*Power(t2,3) + 4950*Power(t2,4) + 
                     1962*Power(t2,5) - 3166*Power(t2,6) + 
                     666*Power(t2,7))) + 
               s2*(Power(-1 + t2,2)*
                   (-161 + 3382*t2 - 15443*Power(t2,2) + 
                     26116*Power(t2,3) - 16689*Power(t2,4) + 
                     1410*Power(t2,5) + 1285*Power(t2,6) + 
                     104*Power(t2,7)) + 
                  Power(t1,2)*
                   (-69 + 698*t2 - 1110*Power(t2,2) - 
                     1650*Power(t2,3) + 4546*Power(t2,4) - 
                     2172*Power(t2,5) - 630*Power(t2,6) + 
                     378*Power(t2,7)) + 
                  t1*(230 - 4000*t2 + 16662*Power(t2,2) - 
                     24384*Power(t2,3) + 4406*Power(t2,4) + 
                     20014*Power(t2,5) - 16768*Power(t2,6) + 
                     4104*Power(t2,7) - 264*Power(t2,8)))) - 
            s1*(-1 + t2)*(Power(s2,3)*Power(t2,2)*
                (-6 + 204*t2 - 993*Power(t2,2) + 1591*Power(t2,3) - 
                  821*Power(t2,4) + 7*Power(t2,5)) + 
               Power(t1,3)*(65 - 395*t2 + 1182*Power(t2,2) - 
                  1756*Power(t2,3) + 975*Power(t2,4) + 95*Power(t2,5) - 
                  170*Power(t2,6) + 22*Power(t2,7)) + 
               Power(t1,2)*(-183 + 389*t2 + 265*Power(t2,2) - 
                  242*Power(t2,3) - 1158*Power(t2,4) + 
                  916*Power(t2,5) - 46*Power(t2,6) + 85*Power(t2,7) - 
                  26*Power(t2,8)) - 
               Power(-1 + t2,2)*
                (53 + 671*t2 - 3866*Power(t2,2) + 6183*Power(t2,3) - 
                  4811*Power(t2,4) + 1923*Power(t2,5) + 
                  550*Power(t2,6) - 365*Power(t2,7) + 2*Power(t2,8)) + 
               t1*(171 + 571*t2 - 6374*Power(t2,2) + 
                  13650*Power(t2,3) - 9336*Power(t2,4) - 
                  1526*Power(t2,5) + 3241*Power(t2,6) + 
                  87*Power(t2,7) - 508*Power(t2,8) + 24*Power(t2,9)) + 
               Power(s2,2)*t2*
                (-28 + 773*t2 - 5167*Power(t2,2) + 13516*Power(t2,3) - 
                  15715*Power(t2,4) + 7626*Power(t2,5) - 
                  1030*Power(t2,6) + 25*Power(t2,7) + 
                  t1*(28 - 593*t2 + 2659*Power(t2,2) - 
                     4016*Power(t2,3) + 1696*Power(t2,4) + 
                     397*Power(t2,5) - 117*Power(t2,6))) + 
               s2*(8 + 308*t2 - 4433*Power(t2,2) + 18113*Power(t2,3) - 
                  32899*Power(t2,4) + 28807*Power(t2,5) - 
                  10634*Power(t2,6) + 290*Power(t2,7) + 
                  412*Power(t2,8) + 28*Power(t2,9) + 
                  Power(t1,2)*
                   (8 + 124*t2 - 927*Power(t2,2) + 1277*Power(t2,3) + 
                     273*Power(t2,4) - 851*Power(t2,5) - 
                     54*Power(t2,6) + 96*Power(t2,7)) - 
                  2*t1*(8 + 216*t2 - 2479*Power(t2,2) + 
                     7286*Power(t2,3) - 7722*Power(t2,4) + 
                     1645*Power(t2,5) + 1596*Power(t2,6) - 
                     623*Power(t2,7) + 73*Power(t2,8))))) + 
         Power(s,3)*(2*Power(s1,8)*
             (6*Power(s2,3) + 
               s2*(2 + 24*Power(t1,2) + t1*(15 - 11*t2) - 2*t2) + 
               Power(s2,2)*(-5 - 21*t1 + 3*t2) - 
               t1*(4 + 10*t1 + 9*Power(t1,2) - 6*t2 - 8*t1*t2 + 
                  2*Power(t2,2))) + 
            (-1 + t2)*(14 + (-191 + 50*s2)*t2 + 
               (-858 + 621*s2 - 113*Power(s2,2))*Power(t2,2) + 
               (5543 - 5368*s2 + 1288*Power(s2,2) - 31*Power(s2,3))*
                Power(t2,3) + 
               (-10573 + 13390*s2 - 4527*Power(s2,2) + 241*Power(s2,3))*
                Power(t2,4) + 
               (10511 - 14241*s2 + 6445*Power(s2,2) - 499*Power(s2,3))*
                Power(t2,5) + 
               (-6053 + 6064*s2 - 3670*Power(s2,2) + 301*Power(s2,3))*
                Power(t2,6) + 
               (1264 - 219*s2 + 593*Power(s2,2))*Power(t2,7) + 
               (582 - 303*s2 - 16*Power(s2,2))*Power(t2,8) + 
               3*(-81 + 2*s2)*Power(t2,9) + 4*Power(t2,10) - 
               Power(t1,3)*(26 - 286*t2 + 825*Power(t2,2) - 
                  1076*Power(t2,3) + 576*Power(t2,4) + 20*Power(t2,5) - 
                  83*Power(t2,6) + 10*Power(t2,7)) + 
               Power(t1,2)*(66 + (-745 + 54*s2)*t2 + 
                  (1290 - 39*s2)*Power(t2,2) - 
                  3*(107 + 62*s2)*Power(t2,3) + 
                  (-422 + 20*s2)*Power(t2,4) + 
                  (179 + 170*s2)*Power(t2,5) + 61*s2*Power(t2,6) - 
                  11*(5 + 4*s2)*Power(t2,7) + 8*Power(t2,8)) + 
               t1*(-54 - 26*(-25 + 4*s2)*t2 + 
                  (345 - 520*s2 + 91*Power(s2,2))*Power(t2,2) + 
                  (-5544 + 4394*s2 - 736*Power(s2,2))*Power(t2,3) + 
                  (7541 - 7842*s2 + 1628*Power(s2,2))*Power(t2,4) - 
                  2*(1138 - 2117*s2 + 576*Power(s2,2))*Power(t2,5) + 
                  (-865 + 358*s2 + 133*Power(s2,2))*Power(t2,6) - 
                  4*(17 + 164*s2)*Power(t2,7) + 
                  17*(17 + 8*s2)*Power(t2,8) - 18*Power(t2,9))) + 
            Power(s1,7)*(Power(s2,3)*(82 - 139*t2) - 
               2*Power(-1 + t2,2)*(-3 + 10*t2) + 
               6*Power(t1,3)*(-33 + 46*t2) + 
               Power(t1,2)*(-342 + 761*t2 - 398*Power(t2,2)) + 
               Power(s2,2)*(-162 - 341*t1 + 375*t2 + 533*t1*t2 - 
                  192*Power(t2,2)) + 
               t1*(-178 + 460*t2 - 331*Power(t2,2) + 49*Power(t2,3)) + 
               s2*(110 + Power(t1,2)*(457 - 670*t2) - 248*t2 + 
                  111*Power(t2,2) + 27*Power(t2,3) + 
                  2*t1*(241 - 546*t2 + 284*Power(t2,2)))) + 
            Power(s1,6)*(Power(s2,3)*(37 - 256*t2 + 276*Power(t2,2)) - 
               2*Power(t1,3)*(247 - 763*t2 + 557*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (-80 + 147*t2 - 181*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,2)*(-717 + 3091*t2 - 4145*Power(t2,2) + 
                  1768*Power(t2,3)) - 
               4*t1*(72 - 472*t2 + 847*Power(t2,2) - 508*Power(t2,3) + 
                  61*Power(t2,4)) + 
               Power(s2,2)*(t2*(462 - 1088*t2 + 623*Power(t2,2)) - 
                  2*t1*(181 - 782*t2 + 699*Power(t2,2))) + 
               s2*(103 - 870*t2 + 1482*Power(t2,2) - 534*Power(t2,3) - 
                  181*Power(t2,4) + 
                  Power(t1,2)*(801 - 2798*t2 + 2218*Power(t2,2)) + 
                  t1*(512 - 2910*t2 + 4562*Power(t2,2) - 
                     2158*Power(t2,3)))) + 
            Power(s1,5)*(Power(s2,3)*
                (-121 + 325*t2 - 219*Power(t2,2) + 55*Power(t2,3)) + 
               Power(t1,3)*(-648 + 3216*t2 - 4836*Power(t2,2) + 
                  2230*Power(t2,3)) + 
               Power(t1,2)*(-491 + 3995*t2 - 9797*Power(t2,2) + 
                  9777*Power(t2,3) - 3538*Power(t2,4)) + 
               3*Power(-1 + t2,2)*
                (93 - 280*t2 + 182*Power(t2,2) - 149*Power(t2,3) + 
                  22*Power(t2,4)) + 
               2*t1*(217 + 47*t2 - 2844*Power(t2,2) + 
                  5296*Power(t2,3) - 3080*Power(t2,4) + 364*Power(t2,5)) \
+ Power(s2,2)*(709 - 2682*t2 + 2934*Power(t2,2) - 326*Power(t2,3) - 
                  689*Power(t2,4) + 
                  t1*(89 + 511*t2 - 1693*Power(t2,2) + 975*Power(t2,3))) \
+ s2*(-362 + 566*t2 + 2040*Power(t2,2) - 4422*Power(t2,3) + 
                  1764*Power(t2,4) + 414*Power(t2,5) + 
                  Power(t1,2)*
                   (550 - 3648*t2 + 6330*Power(t2,2) - 3116*Power(t2,3)) \
+ 2*t1*(-349 + 573*t2 + 1251*Power(t2,2) - 3093*Power(t2,3) + 
                     1672*Power(t2,4)))) + 
            Power(s1,4)*(Power(t1,3)*
                (-507 + 3639*t2 - 8637*Power(t2,2) + 8195*Power(t2,3) - 
                  2600*Power(t2,4)) + 
               Power(s2,3)*(-129 + 1035*t2 - 2303*Power(t2,2) + 
                  1991*Power(t2,3) - 660*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (-460 + 2385*t2 - 3202*Power(t2,2) + 310*Power(t2,3) - 
                  95*Power(t2,4) + 160*Power(t2,5)) + 
               Power(t1,2)*(-270 + 2405*t2 - 8605*Power(t2,2) + 
                  14781*Power(t2,3) - 12111*Power(t2,4) + 
                  3830*Power(t2,5)) + 
               t1*(1087 - 6341*t2 + 9124*Power(t2,2) + 
                  3986*Power(t2,3) - 17335*Power(t2,4) + 
                  10789*Power(t2,5) - 1310*Power(t2,6)) + 
               Power(s2,2)*(1133 - 7762*t2 + 17564*Power(t2,2) - 
                  16412*Power(t2,3) + 5447*Power(t2,4) + 
                  60*Power(t2,5) + 
                  t1*(21 - 751*t2 + 2051*Power(t2,2) - 
                     2149*Power(t2,3) + 1050*Power(t2,4))) + 
               s2*(-1379 + 6741*t2 - 9772*Power(t2,2) + 
                  368*Power(t2,3) + 8701*Power(t2,4) - 
                  4239*Power(t2,5) - 420*Power(t2,6) + 
                  Power(t1,2)*
                   (275 - 2455*t2 + 6573*Power(t2,2) - 
                     6449*Power(t2,3) + 1810*Power(t2,4)) - 
                  2*t1*(324 - 3322*t2 + 8106*Power(t2,2) - 
                     6578*Power(t2,3) + 425*Power(t2,4) + 
                     1075*Power(t2,5)))) + 
            Power(s1,3)*(Power(s2,3)*
                (-27 + 629*t2 - 3083*Power(t2,2) + 5403*Power(t2,3) - 
                  3654*Power(t2,4) + 723*Power(t2,5)) + 
               2*Power(t1,3)*
                (-29 + 861*t2 - 3659*Power(t2,2) + 5867*Power(t2,3) - 
                  3956*Power(t2,4) + 909*Power(t2,5)) + 
               Power(t1,2)*(-143 + 1941*t2 - 6161*Power(t2,2) + 
                  10139*Power(t2,3) - 11630*Power(t2,4) + 
                  8223*Power(t2,5) - 2336*Power(t2,6)) + 
               Power(-1 + t2,2)*
                (957 - 4369*t2 + 8815*Power(t2,2) - 6708*Power(t2,3) - 
                  1335*Power(t2,4) + 1126*Power(t2,5) + 180*Power(t2,6)) \
+ t1*(-300 - 3488*t2 + 19537*Power(t2,2) - 29829*Power(t2,3) + 
                  8323*Power(t2,4) + 15675*Power(t2,5) - 
                  11287*Power(t2,6) + 1369*Power(t2,7)) + 
               Power(s2,2)*(623 - 7600*t2 + 28664*Power(t2,2) - 
                  46258*Power(t2,3) + 33687*Power(t2,4) - 
                  9577*Power(t2,5) + 494*Power(t2,6) - 
                  t1*(227 - 1151*t2 + 163*Power(t2,2) + 
                     4665*Power(t2,3) - 6041*Power(t2,4) + 
                     2133*Power(t2,5))) + 
               s2*(-1732 + 13664*t2 - 36855*Power(t2,2) + 
                  41295*Power(t2,3) - 11867*Power(t2,4) - 
                  10857*Power(t2,5) + 6177*Power(t2,6) + 
                  175*Power(t2,7) + 
                  Power(t1,2)*
                   (-32 - 1194*t2 + 5110*Power(t2,2) - 
                     6572*Power(t2,3) + 2575*Power(t2,4) + 
                     132*Power(t2,5)) - 
                  2*t1*(-489 + 775*t2 + 6181*Power(t2,2) - 
                     18407*Power(t2,3) + 17197*Power(t2,4) - 
                     5338*Power(t2,5) + 114*Power(t2,6)))) + 
            Power(s1,2)*(Power(s2,3)*t2*
                (85 - 1143*t2 + 4285*Power(t2,2) - 6182*Power(t2,3) + 
                  3300*Power(t2,4) - 316*Power(t2,5)) + 
               Power(t1,3)*(211 - 717*t2 - 753*Power(t2,2) + 
                  5695*Power(t2,3) - 7950*Power(t2,4) + 
                  4218*Power(t2,5) - 726*Power(t2,6)) - 
               Power(-1 + t2,2)*
                (-676 + 5452*t2 - 13004*Power(t2,2) + 
                  15972*Power(t2,3) - 8367*Power(t2,4) - 
                  2458*Power(t2,5) + 1773*Power(t2,6) + 90*Power(t2,7)) \
+ Power(t1,2)*(-68 + 608*t2 - 3809*Power(t2,2) + 7855*Power(t2,3) - 
                  7226*Power(t2,4) + 4840*Power(t2,5) - 
                  3001*Power(t2,6) + 778*Power(t2,7)) - 
               t1*(771 - 5199*t2 + 5332*Power(t2,2) + 
                  15168*Power(t2,3) - 32941*Power(t2,4) + 
                  15173*Power(t2,5) + 7808*Power(t2,6) - 
                  6896*Power(t2,7) + 784*Power(t2,8)) + 
               Power(s2,2)*(89 - 2573*t2 + 17538*Power(t2,2) - 
                  47820*Power(t2,3) + 60665*Power(t2,4) - 
                  35475*Power(t2,5) + 7986*Power(t2,6) - 
                  433*Power(t2,7) + 
                  t1*(-67 + 1207*t2 - 4651*Power(t2,2) + 
                     4973*Power(t2,3) + 1660*Power(t2,4) - 
                     4504*Power(t2,5) + 1302*Power(t2,6))) + 
               s2*(-532 + 8819*t2 - 40793*Power(t2,2) + 
                  81738*Power(t2,3) - 76129*Power(t2,4) + 
                  24431*Power(t2,5) + 7433*Power(t2,6) - 
                  4970*Power(t2,7) + 3*Power(t2,8) + 
                  Power(t1,2)*
                   (-96 + 345*t2 + 1557*Power(t2,2) - 
                     5553*Power(t2,3) + 4302*Power(t2,4) + 
                     198*Power(t2,5) - 680*Power(t2,6)) + 
                  2*t1*(283 - 3185*t2 + 7962*Power(t2,2) - 
                     1658*Power(t2,3) - 14335*Power(t2,4) + 
                     16014*Power(t2,5) - 5659*Power(t2,6) + 
                     601*Power(t2,7)))) + 
            s1*(Power(s2,3)*Power(t2,2)*
                (-89 + 915*t2 - 2856*Power(t2,2) + 3520*Power(t2,3) - 
                  1545*Power(t2,4) + 49*Power(t2,5)) + 
               Power(t1,3)*(177 - 1077*t2 + 2502*Power(t2,2) - 
                  2102*Power(t2,3) - 775*Power(t2,4) + 
                  2223*Power(t2,5) - 1086*Power(t2,6) + 144*Power(t2,7)) \
+ Power(t1,2)*(-430 + 1410*t2 - 1648*Power(t2,2) + 2093*Power(t2,3) - 
                  3142*Power(t2,4) + 2266*Power(t2,5) - 
                  1000*Power(t2,6) + 579*Power(t2,7) - 128*Power(t2,8)) + 
               Power(-1 + t2,2)*
                (-94 - 1734*t2 + 8954*Power(t2,2) - 15155*Power(t2,3) + 
                  13707*Power(t2,4) - 5708*Power(t2,5) - 
                  1561*Power(t2,6) + 1077*Power(t2,7) + 10*Power(t2,8)) + 
               t1*(347 + 1117*t2 - 11122*Power(t2,2) + 
                  20670*Power(t2,3) - 8120*Power(t2,4) - 
                  10660*Power(t2,5) + 7643*Power(t2,6) + 
                  2169*Power(t2,7) - 2258*Power(t2,8) + 214*Power(t2,9)) \
+ Power(s2,2)*t2*(-202 + 3351*t2 - 16886*Power(t2,2) + 
                  37181*Power(t2,3) - 39404*Power(t2,4) + 
                  19229*Power(t2,5) - 3416*Power(t2,6) + 
                  147*Power(t2,7) + 
                  t1*(158 - 1807*t2 + 5843*Power(t2,2) - 
                     6928*Power(t2,3) + 2090*Power(t2,4) + 
                     949*Power(t2,5) - 287*Power(t2,6))) + 
               s2*(30 + 1159*t2 - 13120*Power(t2,2) + 47262*Power(t2,3) - 
                  78877*Power(t2,4) + 64238*Power(t2,5) - 
                  20495*Power(t2,6) - 2177*Power(t2,7) + 
                  2004*Power(t2,8) - 24*Power(t2,9) + 
                  Power(t1,2)*
                   (34 + 59*t2 - 504*Power(t2,2) - 436*Power(t2,3) + 
                     2514*Power(t2,4) - 1569*Power(t2,5) - 
                     418*Power(t2,6) + 302*Power(t2,7)) - 
                  2*t1*(32 + 547*t2 - 5197*Power(t2,2) + 
                     12977*Power(t2,3) - 10888*Power(t2,4) - 
                     1271*Power(t2,5) + 5939*Power(t2,6) - 
                     2485*Power(t2,7) + 346*Power(t2,8))))))*
       R1q(1 - s + s1 - t2))/
     (s*(-1 + s1)*(-1 + s2)*(-1 + t1)*Power(-s + s1 - t2,3)*
       (1 - s + s1 - t2)*Power(-1 + s + t2,3)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) - 
    (8*(-2*Power(s,8)*(Power(t1,3) + 5*t1*Power(t2,2) + 
            (1 - 3*s2 - 2*t2)*Power(t2,2)) + 
         Power(s,7)*(Power(t1,3)*(14 + 16*s1 - 3*t2) - 
            Power(t1,2)*(6 + 2*s2*t2 + Power(t2,2) + 
               2*s1*(-3 + 3*s2 + t2)) + 
            t1*t2*(-6 + 6*s2 + 50*t2 + 11*s2*t2 - 13*Power(t2,2) + 
               s1*(-14 - 6*s2 + 61*t2)) + 
            t2*(s1*(-4 + 12*s2 + 24*t2 - 36*s2*t2 - 19*Power(t2,2)) + 
               t2*(22 - 38*s2 - 6*Power(s2,2) - 44*t2 + 3*s2*t2 + 
                  5*Power(t2,2)))) + 
         Power(s,6)*(Power(t1,3)*(-38 + 19*t2 + Power(t2,2)) - 
            Power(t1,2)*(-36 - (1 + 4*s2)*t2 + (1 + 3*s2)*Power(t2,2) + 
               Power(t2,3)) - 
            t1*(6 + (-36 + 34*s2)*t2 + 
               (48 + 61*s2 + Power(s2,2))*Power(t2,2) - 
               (33 + 14*s2)*Power(t2,3) + Power(t2,4)) + 
            t2*(-6 - 116*t2 + 2*Power(s2,2)*(23 - 3*t2)*t2 + 
               157*Power(t2,2) - 53*Power(t2,3) + Power(t2,4) + 
               s2*(6 + 79*t2 + 5*Power(t2,2) - 5*Power(t2,3))) + 
            Power(s1,2)*(-2 - 56*Power(t1,3) + 26*t2 - 74*Power(t2,2) + 
               35*Power(t2,3) - 6*Power(s2,2)*(t1 + t2) + 
               2*Power(t1,2)*(-19 + 7*t2) - 
               2*t1*(5 - 38*t2 + 78*Power(t2,2)) + 
               s2*(6 + 42*Power(t1,2) - 48*t2 + 89*Power(t2,2) + 
                  t1*(6 + 32*t2))) + 
            s1*(3*Power(t1,3)*(-25 + 7*t2) + 
               Power(t1,2)*(6 + 27*t2 + 2*Power(t2,2) + 
                  s2*(28 + 3*t2)) + 
               t1*(6 + 76*t2 + 2*Power(s2,2)*t2 - 176*Power(t2,2) + 
                  69*Power(t2,3) + s2*(-6 + 8*t2 - 61*Power(t2,2))) + 
               t2*(52 - 207*t2 + 188*Power(t2,2) - 20*Power(t2,3) + 
                  Power(s2,2)*(-6 + 23*t2) - 
                  2*s2*(41 - 65*t2 + 14*Power(t2,2))))) + 
         Power(s,5)*(-2 + 30*t2 - 26*s2*t2 + 327*Power(t2,2) + 
            40*s2*Power(t2,2) - 131*Power(s2,2)*Power(t2,2) - 
            6*Power(s2,3)*Power(t2,2) - 286*Power(t2,3) - 
            63*s2*Power(t2,3) + 30*Power(s2,2)*Power(t2,3) - 
            Power(s2,3)*Power(t2,3) + 152*Power(t2,4) + 
            42*s2*Power(t2,4) + Power(s2,2)*Power(t2,4) - 
            16*Power(t2,5) - 2*s2*Power(t2,5) + 
            Power(t1,3)*(46 - 43*t2 + 2*Power(t2,3)) + 
            Power(t1,2)*(-78 + 2*(-10 + 9*s2)*t2 + 
               (74 - 16*s2)*Power(t2,2) + (-20 + s2)*Power(t2,3)) + 
            t1*(30 + (-61 + 52*s2)*t2 + 
               (-220 + 95*s2 + 22*Power(s2,2))*Power(t2,2) - 
               (-75 + 46*s2 + Power(s2,2))*Power(t2,3) + 
               (-12 + s2)*Power(t2,4) + 2*Power(t2,5)) + 
            Power(s1,3)*(8 - 2*Power(s2,3) + 112*Power(t1,3) - 55*t2 + 
               96*Power(t2,2) - 30*Power(t2,3) - 
               6*Power(t1,2)*(-17 + 7*t2) + 
               4*Power(s2,2)*(9*t1 + 7*t2) + 
               5*t1*(9 - 34*t2 + 43*Power(t2,2)) - 
               s2*(18 + 126*Power(t1,2) - 74*t2 + 115*Power(t2,2) + 
                  t1*(34 + 66*t2))) + 
            Power(s1,2)*(24 + Power(t1,3)*(156 - 63*t2) - 240*t2 + 
               4*Power(s2,3)*t2 + 521*Power(t2,2) - 313*Power(t2,3) + 
               30*Power(t2,4) + 
               Power(s2,2)*(-6 + t1*(20 - 19*t2) + 8*t2 - 
                  41*Power(t2,2)) + 
               Power(t1,2)*(26 - 135*t2 + 9*Power(t2,2)) - 
               2*t1*(2 + 74*t2 - 96*Power(t2,2) + 75*Power(t2,3)) + 
               s2*(-32 + 181*t2 - 137*Power(t2,2) + 78*Power(t2,3) + 
                  Power(t1,2)*(-113 + 24*t2) + 
                  t1*(21 - 24*t2 + 133*Power(t2,2)))) + 
            s1*(-2*Power(t1,3)*(-58 + 40*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-71 - 128*t2 + 54*Power(t2,2) + 
                  5*Power(t2,3) + s2*(-46 + 33*t2 + 17*Power(t2,2))) + 
               t1*(6 - 48*t2 - 26*Power(s2,2)*t2 - 107*Power(t2,2) - 
                  18*Power(t2,3) + 5*Power(t2,4) + 
                  s2*(20 + 26*t2 + 168*Power(t2,2) - 58*Power(t2,3))) + 
               t2*(-224 + 631*t2 + 7*Power(s2,3)*t2 - 523*Power(t2,2) + 
                  168*Power(t2,3) - 4*Power(t2,4) + 
                  Power(s2,2)*(64 - 102*t2 + 23*Power(t2,2)) + 
                  s2*(128 - 48*t2 - 41*Power(t2,2) + 9*Power(t2,3))))) + 
         Power(s,4)*(8 - 35*t2 + 24*s2*t2 - 475*Power(t2,2) - 
            414*s2*Power(t2,2) + 145*Power(s2,2)*Power(t2,2) + 
            26*Power(s2,3)*Power(t2,2) + 388*Power(t2,3) + 
            193*s2*Power(t2,3) - 59*Power(s2,2)*Power(t2,3) + 
            Power(s2,3)*Power(t2,3) - 180*Power(t2,4) - 
            90*s2*Power(t2,4) - 9*Power(s2,2)*Power(t2,4) - 
            Power(s2,3)*Power(t2,4) + 45*Power(t2,5) + 
            12*s2*Power(t2,5) + Power(s2,2)*Power(t2,5) - 
            Power(t1,3)*(10 - 35*t2 + 7*Power(t2,2) + 6*Power(t2,3)) + 
            Power(t1,2)*(60 + (109 - 80*s2)*t2 + 
               (-334 + 113*s2)*Power(t2,2) + 
               (143 - 23*s2)*Power(t2,3) + 2*(-6 + s2)*Power(t2,4)) + 
            t1*(-48 + (-41 + 28*s2)*t2 + 
               (769 + 33*s2 - 83*Power(s2,2))*Power(t2,2) + 
               (-423 - 8*s2 + 15*Power(s2,2))*Power(t2,3) + 
               (59 + 11*s2)*Power(t2,4) - 2*(3 + s2)*Power(t2,5)) + 
            Power(s1,4)*(-12 + 10*Power(s2,3) - 140*Power(t1,3) + 
               49*t2 - 54*Power(t2,2) + 10*Power(t2,3) + 
               10*Power(t1,2)*(-15 + 7*t2) - 
               2*Power(s2,2)*(1 + 45*t1 + 25*t2) - 
               10*t1*(8 - 20*t2 + 17*Power(t2,2)) + 
               s2*(21 + 210*Power(t1,2) - 56*t2 + 80*Power(t2,2) + 
                  20*t1*(4 + 3*t2))) - 
            Power(s1,3)*(65 - 403*t2 + 610*Power(t2,2) - 
               251*Power(t2,3) + 20*Power(t2,4) + 
               Power(s2,3)*(-6 + 19*t2) - 5*Power(t1,3)*(-29 + 21*t2) + 
               10*Power(t1,2)*(4 - 27*t2 + 4*Power(t2,2)) + 
               t1*(-37 + 10*t2 + 7*Power(t2,2) - 170*Power(t2,3)) - 
               Power(s2,2)*(3 + 45*t2 + 49*Power(t2,2) + 
                  t1*(-53 + 65*t2)) + 
               s2*(-30 + 87*t2 - 10*Power(t2,2) + 92*Power(t2,3) + 
                  5*Power(t1,2)*(-29 + 19*t2) + 
                  t1*(29 + 102*t2 + 135*Power(t2,2)))) + 
            Power(s1,2)*(-104 + 550*t2 - 860*Power(t2,2) + 
               662*Power(t2,3) - 204*Power(t2,4) + 6*Power(t2,5) - 
               2*Power(s2,3)*t2*(1 + 8*t2) + 
               Power(t1,3)*(-98 + 115*t2 + 15*Power(t2,2)) + 
               Power(t1,2)*(3 + 412*t2 - 211*Power(t2,2) - 
                  10*Power(t2,3)) - 
               2*t1*(-14 + 136*t2 - 278*Power(t2,2) + 93*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,2)*(36 - 9*t2 + 15*Power(t2,2) - 
                  30*Power(t2,3) + t1*(-29 + 105*t2 + 10*Power(t2,2))) \
+ s2*(37 + 3*t2 - 259*Power(t2,2) + 121*Power(t2,3) - 2*Power(t2,4) + 
                  Power(t1,2)*(79 - 133*t2 - 40*Power(t2,2)) + 
                  t1*(-33 - 246*t2 + 5*Power(t2,2) + 88*Power(t2,3)))) + 
            s1*(10 + 461*t2 - 896*Power(t2,2) + 518*Power(t2,3) - 
               246*Power(t2,4) + 40*Power(t2,5) + 
               Power(s2,3)*t2*(-12 - 13*t2 + 12*Power(t2,2)) - 
               Power(t1,3)*(55 - 81*t2 + 8*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,2)*(53 + 395*t2 - 483*Power(t2,2) + 
                  115*Power(t2,3) + 2*Power(t2,4)) - 
               t1*(31 + 487*t2 - 972*Power(t2,2) + 409*Power(t2,3) - 
                  53*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-198 + 158*t2 - 24*Power(t2,2) + Power(t2,3) + 
                  t1*(106 - 91*t2 - 3*Power(t2,2))) - 
               s2*(2 - 229*t2 + 502*Power(t2,2) - 353*Power(t2,3) + 
                  89*Power(t2,4) - 8*Power(t2,5) + 
                  Power(t1,2)*
                   (-32 + 148*t2 - 99*Power(t2,2) + Power(t2,3)) + 
                  t1*(24 + 78*t2 - 157*Power(t2,2) + 28*Power(t2,3) + 
                     Power(t2,4))))) + 
         Power(-1 + s1,4)*(-4 + 25*t2 - 8*s2*t2 - 34*Power(t2,2) + 
            4*s2*Power(t2,2) - 15*Power(s2,2)*Power(t2,2) + 
            2*Power(s2,3)*Power(t2,2) + 8*Power(t2,3) + 
            16*s2*Power(t2,3) + 17*Power(s2,2)*Power(t2,3) - 
            3*Power(s2,3)*Power(t2,3) + 6*Power(t2,4) - 
            12*s2*Power(t2,4) - 3*Power(s2,2)*Power(t2,4) - 
            Power(s2,3)*Power(t2,4) - Power(t2,5) + 
            Power(s2,2)*Power(t2,5) + 
            2*Power(s1,4)*Power(s2 - t1,2)*(-1 + s2 - t1 + t2) + 
            Power(t1,3)*(4 - 7*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,2)*(-12 + (39 - 8*s2)*t2 + 
               (-68 + 19*s2)*Power(t2,2) + (53 - 19*s2)*Power(t2,3) + 
               2*(-6 + s2)*Power(t2,4)) + 
            t1*(12 + (-57 + 16*s2)*t2 + 
               (83 + 11*s2 - 5*Power(s2,2))*Power(t2,2) + 
               (-41 - 44*s2 + 11*Power(s2,2))*Power(t2,3) + 
               (1 + 19*s2)*Power(t2,4) - 2*(-1 + s2)*Power(t2,5)) + 
            Power(s1,3)*(-2*Power(-1 + t2,2) + t1*Power(-1 + t2,2) - 
               4*Power(t1,2)*Power(-1 + t2,2) - 
               Power(s2,3)*(1 + 3*t2) + Power(t1,3)*(1 + 3*t2) + 
               s2*(3*Power(-1 + t2,2) + 7*t1*Power(-1 + t2,2) - 
                  3*Power(t1,2)*(1 + 3*t2)) + 
               Power(s2,2)*(-5*Power(-1 + t2,2) + t1*(3 + 9*t2))) + 
            Power(s1,2)*(-1 + t2)*
             (-3 + Power(t1,2)*(29 - 21*t2) + Power(t1,3)*(-2 + t2) + 
               2*t2 + Power(t2,2) - Power(s2,3)*(2 + t2) + 
               4*t1*(-2 + t2 + Power(t2,2)) + 
               Power(s2,2)*(15 + 5*t1 - 12*t2 + 5*Power(t2,2)) + 
               s2*(Power(t1,2)*(1 - 2*t2) + 
                  t1*(-51 + 39*t2 - 4*Power(t2,2)) - 
                  2*(-8 + 5*t2 + 3*Power(t2,2)))) + 
            s1*(Power(-1 + t2,2)*(-13 - 7*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*t2*(-4 + 5*t2 + 3*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(-21 + 13*t2 + 7*Power(t2,2)) - 
               Power(t1,3)*(5 - 7*t2 + 4*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(-3 + 61*t2 - 87*Power(t2,2) + 
                  27*Power(t2,3) + 2*Power(t2,4)) + 
               Power(s2,2)*t2*
                (t1*(10 - 19*t2 - 3*Power(t2,2)) - 
                  3*(-10 + 13*t2 - 4*Power(t2,2) + Power(t2,3))) + 
               s2*(Power(-1 + t2,2)*(8 + 28*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (8 - 18*t2 + 19*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(-16 - 62*t2 + 127*Power(t2,2) - 52*Power(t2,3) + 
                     3*Power(t2,4))))) + 
         Power(s,2)*(-4 + 42*t1 - 84*Power(t1,2) + 46*Power(t1,3) + 
            128*t2 - 70*s2*t2 - 386*t1*t2 + 182*s2*t1*t2 + 
            299*Power(t1,2)*t2 - 108*s2*Power(t1,2)*t2 - 
            47*Power(t1,3)*t2 - 245*Power(t2,2) - 307*s2*Power(t2,2) - 
            112*Power(s2,2)*Power(t2,2) + 36*Power(s2,3)*Power(t2,2) + 
            902*t1*Power(t2,2) + 241*s2*t1*Power(t2,2) - 
            103*Power(s2,2)*t1*Power(t2,2) - 
            653*Power(t1,2)*Power(t2,2) + 
            223*s2*Power(t1,2)*Power(t2,2) + 3*Power(t1,3)*Power(t2,2) + 
            223*Power(t2,3) + 322*s2*Power(t2,3) + 
            48*Power(s2,2)*Power(t2,3) - 14*Power(s2,3)*Power(t2,3) - 
            593*t1*Power(t2,3) - 314*s2*t1*Power(t2,3) + 
            70*Power(s2,2)*t1*Power(t2,3) + 
            413*Power(t1,2)*Power(t2,3) - 
            118*s2*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) - 
            105*Power(t2,4) - 45*s2*Power(t2,4) - 
            20*Power(s2,2)*Power(t2,4) - 6*Power(s2,3)*Power(t2,4) + 
            85*t1*Power(t2,4) + 98*s2*t1*Power(t2,4) - 
            72*Power(t1,2)*Power(t2,4) + 12*s2*Power(t1,2)*Power(t2,4) + 
            19*Power(t2,5) + 12*s2*Power(t2,5) + 
            6*Power(s2,2)*Power(t2,5) + 4*t1*Power(t2,5) - 
            12*s2*t1*Power(t2,5) + 
            Power(s1,6)*(-2 + 20*Power(s2,3) - 56*Power(t1,3) + t2 + 
               2*Power(t2,2) - Power(t2,3) - 
               2*Power(s2,2)*(6 + 45*t1 + 5*t2) + 
               6*Power(t1,2)*(-11 + 7*t2) + 
               s2*(9 + 126*Power(t1,2) + t1*(70 - 24*t2) - 8*t2 + 
                  Power(t2,2)) - 2*t1*(15 - 22*t2 + 8*Power(t2,2))) + 
            Power(s1,5)*(-61 + 199*t2 - 153*Power(t2,2) + 
               13*Power(t2,3) - Power(s2,3)*(9 + 34*t2) + 
               Power(t1,3)*(51 + 63*t2) + 
               Power(t1,2)*(46 + 135*t2 - 54*Power(t2,2)) + 
               t1*(132 - 224*t2 + 78*Power(t2,2) + 33*Power(t2,3)) + 
               Power(s2,2)*(-14 + 115*t2 + 5*Power(t2,2) + 
                  2*t1*(41 + 50*t2)) - 
               s2*(43 - 47*t2 + 19*Power(t2,2) + 
                  Power(t1,2)*(130 + 123*t2) + 
                  t1*(36 + 228*t2 - 29*Power(t2,2)))) + 
            Power(s1,4)*(23 - 134*t2 - 57*Power(t2,2) + 
               210*Power(t2,3) - 35*Power(t2,4) + Power(t2,5) + 
               Power(s2,3)*(4 - 9*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(22 - 55*t2 + 15*Power(t2,2)) - 
               Power(t1,2)*(190 - 317*t2 + 231*Power(t2,2) + 
                  5*Power(t2,3)) - 
               t1*(78 + 32*t2 - 296*Power(t2,2) + 195*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,2)*(-117 + 236*t2 - 204*Power(t2,2) + 
                  19*Power(t2,3) + t1*(-30 + 14*t2 + 15*Power(t2,2))) + 
               s2*(36 + 155*t2 - 210*Power(t2,2) + 24*Power(t2,3) - 
                  9*Power(t2,4) + 
                  Power(t1,2)*(10 + 42*t2 - 35*Power(t2,2)) + 
                  t1*(338 - 638*t2 + 489*Power(t2,2) - 2*Power(t2,3)))) \
+ Power(s1,3)*(36 + 103*t2 + 149*Power(t2,2) - 308*Power(t2,3) - 
               24*Power(t2,4) + 16*Power(t2,5) - 
               2*Power(t1,3)*
                (9 - 5*t2 + 24*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,3)*(-13 + 32*t2 + 6*Power(t2,2) + 
                  17*Power(t2,3)) + 
               2*Power(t1,2)*
                (-99 + 268*t2 - 357*Power(t2,2) + 133*Power(t2,3) + 
                  6*Power(t2,4)) + 
               t1*(112 - 438*t2 + 146*Power(t2,2) + 40*Power(t2,3) + 
                  42*Power(t2,4) - 8*Power(t2,5)) - 
               Power(s2,2)*(4 + 19*t2 + 209*Power(t2,2) - 
                  110*Power(t2,3) + 18*Power(t2,4) + 
                  2*t1*(17 - 33*t2 + 81*Power(t2,2) + 13*Power(t2,3))) \
+ s2*(-137 + 186*t2 - 148*Power(t2,2) + 263*Power(t2,3) - 
                  2*Power(t2,4) + 8*Power(t2,5) + 
                  2*Power(t1,2)*
                   (35 - 68*t2 + 125*Power(t2,2) + 7*Power(t2,3)) + 
                  2*t1*(111 - 204*t2 + 415*Power(t2,2) - 
                     232*Power(t2,3) + 7*Power(t2,4)))) + 
            Power(s1,2)*(-43 - 181*t2 + 214*Power(t2,2) - 
               86*Power(t2,3) + 168*Power(t2,4) - 16*Power(t2,5) + 
               2*Power(t1,3)*
                (-2 + 3*t2 + 11*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(-268 + 614*t2 - 572*Power(t2,2) + 
                  328*Power(t2,3) - 64*Power(t2,4)) + 
               Power(s2,3)*(14 + 27*t2 - 39*Power(t2,2) + 
                  8*Power(t2,3) - 6*Power(t2,4)) + 
               2*t1*(181 - 89*t2 + 89*Power(t2,2) - 14*Power(t2,3) - 
                  56*Power(t2,4) + 10*Power(t2,5)) + 
               Power(s2,2)*(19 + 48*t2 - 30*Power(t2,2) + 
                  67*Power(t2,3) - 24*Power(t2,4) + 6*Power(t2,5) + 
                  t1*(-56 + 34*t2 - 32*Power(t2,2) + 58*Power(t2,3))) + 
               s2*(-315 + 377*t2 - 398*Power(t2,2) + 36*Power(t2,3) - 
                  62*Power(t2,4) - 4*Power(t2,5) + 
                  2*Power(t1,2)*
                   (28 - 47*t2 + 34*Power(t2,2) - 49*Power(t2,3) + 
                     6*Power(t2,4)) - 
                  2*t1*(-96 + 316*t2 - 323*Power(t2,2) + 
                     186*Power(t2,3) - 67*Power(t2,4) + 6*Power(t2,5)))) \
+ s1*(-45 + 92*t2 + 58*Power(t2,2) - 243*Power(t2,3) + 124*Power(t2,4) - 
               36*Power(t2,5) - 
               Power(s2,3)*t2*
                (48 + 8*t2 - 21*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(-41 + 23*t2 + 8*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(-40 + 617*t2 - 656*Power(t2,2) + 
                  278*Power(t2,3) - 36*Power(t2,4)) + 
               t1*(132 - 802*t2 + 400*Power(t2,2) + 167*Power(t2,3) - 
                  106*Power(t2,4) + 16*Power(t2,5)) + 
               2*Power(s2,2)*t2*
                (31 + 3*t2 + 38*Power(t2,2) - 17*Power(t2,3) + 
                  2*Power(t2,4) + t1*(85 - 51*t2 + 13*Power(t2,2))) + 
               s2*(18 + 753*t2 - 647*Power(t2,2) + 219*Power(t2,3) - 
                  26*Power(t2,4) - 16*Power(t2,5) + 
                  Power(t1,2)*
                   (28 - 93*t2 + 70*Power(t2,2) - 54*Power(t2,3) + 
                     8*Power(t2,4)) - 
                  t1*(50 + 716*t2 - 773*Power(t2,2) + 448*Power(t2,3) - 
                     106*Power(t2,4) + 8*Power(t2,5))))) - 
         s*Power(-1 + s1,2)*(-10 + 42*t1 - 54*Power(t1,2) + 
            22*Power(t1,3) + 98*t2 - 42*s2*t2 - 239*t1*t2 + 
            88*s2*t1*t2 + 172*Power(t1,2)*t2 - 46*s2*Power(t1,2)*t2 - 
            31*Power(t1,3)*t2 - 146*Power(t2,2) - 38*s2*Power(t2,2) - 
            77*Power(s2,2)*Power(t2,2) + 14*Power(s2,3)*Power(t2,2) + 
            408*t1*Power(t2,2) + 95*s2*t1*Power(t2,2) - 
            38*Power(s2,2)*t1*Power(t2,2) - 
            332*Power(t1,2)*Power(t2,2) + 
            104*s2*Power(t1,2)*Power(t2,2) + 8*Power(t1,3)*Power(t2,2) + 
            70*Power(t2,3) + 120*s2*Power(t2,3) + 
            58*Power(s2,2)*Power(t2,3) - 11*Power(s2,3)*Power(t2,3) - 
            245*t1*Power(t2,3) - 194*s2*t1*Power(t2,3) + 
            45*Power(s2,2)*t1*Power(t2,3) + 
            236*Power(t1,2)*Power(t2,3) - 
            77*s2*Power(t1,2)*Power(t2,3) + 6*Power(t1,3)*Power(t2,3) - 
            12*Power(t2,4) - 42*s2*Power(t2,4) - 
            11*Power(s2,2)*Power(t2,4) - 4*Power(s2,3)*Power(t2,4) + 
            28*t1*Power(t2,4) + 71*s2*t1*Power(t2,4) - 
            48*Power(t1,2)*Power(t2,4) + 8*s2*Power(t1,2)*Power(t2,4) + 
            2*s2*Power(t2,5) + 4*Power(s2,2)*Power(t2,5) + 
            6*t1*Power(t2,5) - 8*s2*t1*Power(t2,5) + 
            Power(s1,5)*(10*Power(s2,3) - 
               4*Power(s2,2)*(2 + 9*t1 - t2) + 
               s2*(3 + 42*Power(t1,2) + t1*(26 - 18*t2) - 2*t2 - 
                  Power(t2,2)) - 
               t1*(5 + 16*Power(t1,2) - 6*t2 + Power(t2,2) - 
                  2*t1*(-9 + 7*t2))) - 
            Power(s1,4)*(25 - 54*t2 + 29*Power(t2,2) + 
               Power(s2,3)*(1 + 16*t2) - Power(t1,3)*(8 + 21*t2) + 
               Power(t1,2)*(10 - 55*t2 + 23*Power(t2,2)) - 
               2*t1*(17 - 26*t2 + 7*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(14 - 47*t2 + 11*Power(t2,2) - 
                  t1*(16 + 47*t2)) + 
               s2*(6 + 4*t2 - 6*Power(t2,2) - 4*Power(t2,3) + 
                  Power(t1,2)*(23 + 52*t2) + 
                  t1*(-23 + 96*t2 - 29*Power(t2,2)))) + 
            Power(s1,3)*(36 - 49*t2 - 26*Power(t2,2) + 43*Power(t2,3) - 
               4*Power(t2,4) + 
               Power(s2,3)*(9 - 19*t2 + 2*Power(t2,2)) + 
               2*Power(t1,3)*(6 - 7*t2 + 3*Power(t2,2)) - 
               Power(t1,2)*(119 - 220*t2 + 116*Power(t2,2) + 
                  Power(t2,3)) - 
               t1*(31 + 18*t2 - 72*Power(t2,2) + 22*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(-91 + 151*t2 - 94*Power(t2,2) + 
                  18*Power(t2,3) + 4*t1*(-3 + 7*t2 + Power(t2,2))) - 
               s2*(28 - 114*t2 + 65*Power(t2,2) + 16*Power(t2,3) + 
                  5*Power(t2,4) + 
                  Power(t1,2)*(6 - 3*t2 + 13*Power(t2,2)) + 
                  2*t1*(-116 + 216*t2 - 123*Power(t2,2) + 7*Power(t2,3))\
)) + Power(s1,2)*(-44 + 41*t2 + 98*Power(t2,2) - 95*Power(t2,3) - 
               4*Power(t2,4) + 4*Power(t2,5) - 
               2*Power(t1,3)*
                (5 - 7*t2 + 10*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,3)*(10 - 17*t2 + 25*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-162 + 415*t2 - 439*Power(t2,2) + 
                  134*Power(t2,3) + 8*Power(t2,4)) + 
               t1*(186 - 267*t2 + 74*Power(t2,2) + 15*Power(t2,3) - 
                  6*Power(t2,4) - 2*Power(t2,5)) - 
               Power(s2,2)*(43 - 170*t2 + 229*Power(t2,2) - 
                  73*Power(t2,3) + 15*Power(t2,4) + 
                  t1*(32 - 47*t2 + 78*Power(t2,2) + 15*Power(t2,3))) + 
               s2*(-83 + 97*t2 - 133*Power(t2,2) + 101*Power(t2,3) + 
                  16*Power(t2,4) + 2*Power(t2,5) + 
                  Power(t1,2)*
                   (41 - 80*t2 + 106*Power(t2,2) + 11*Power(t2,3)) + 
                  t1*(135 - 416*t2 + 612*Power(t2,2) - 
                     254*Power(t2,3) + 11*Power(t2,4)))) - 
            s1*(57 - 116*t2 + 69*Power(t2,2) + 62*Power(t2,3) - 
               84*Power(t2,4) + 12*Power(t2,5) - 
               2*Power(t1,3)*
                (-8 + 5*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,3)*t2*
                (24 - 19*t2 + Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(37 - 404*t2 + 530*Power(t2,2) - 
                  271*Power(t2,3) + 40*Power(t2,4)) + 
               t1*(-110 + 438*t2 - 425*Power(t2,2) + 40*Power(t2,3) + 
                  69*Power(t2,4) - 12*Power(t2,5)) - 
               Power(s2,2)*t2*
                (120 - 137*t2 + 103*Power(t2,2) - 22*Power(t2,3) + 
                  4*Power(t2,4) + t1*(70 - 80*t2 + 34*Power(t2,2))) + 
               s2*(-22 - 185*t2 + 261*Power(t2,2) - 99*Power(t2,3) + 
                  41*Power(t2,4) + 4*Power(t2,5) + 
                  Power(t1,2)*
                   (-26 + 81*t2 - 91*Power(t2,2) + 62*Power(t2,3) - 
                     8*Power(t2,4)) + 
                  t1*(48 + 358*t2 - 522*Power(t2,2) + 338*Power(t2,3) - 
                     94*Power(t2,4) + 8*Power(t2,5))))) + 
         Power(s,3)*(-8 + 12*t1 + 30*Power(t1,2) - 38*Power(t1,3) - 
            44*t2 + 32*s2*t2 + 276*t1*t2 - 162*s2*t1*t2 - 
            256*Power(t1,2)*t2 + 130*s2*Power(t1,2)*t2 + 
            15*Power(t1,3)*t2 + 377*Power(t2,2) + 592*s2*Power(t2,2) - 
            4*Power(s2,2)*Power(t2,2) - 44*Power(s2,3)*Power(t2,2) - 
            1118*t1*Power(t2,2) - 235*s2*t1*Power(t2,2) + 
            132*Power(s2,2)*t1*Power(t2,2) + 
            651*Power(t1,2)*Power(t2,2) - 
            232*s2*Power(t1,2)*Power(t2,2) + 8*Power(t1,3)*Power(t2,2) - 
            404*Power(t2,3) - 356*s2*Power(t2,3) + 
            28*Power(s2,2)*Power(t2,3) + 6*Power(s2,3)*Power(t2,3) + 
            717*t1*Power(t2,3) + 204*s2*t1*Power(t2,3) - 
            50*Power(s2,2)*t1*Power(t2,3) - 352*Power(t1,2)*Power(t2,3) + 
            82*s2*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) + 
            139*Power(t2,4) + 68*s2*Power(t2,4) + 
            20*Power(s2,2)*Power(t2,4) + 4*Power(s2,3)*Power(t2,4) - 
            104*t1*Power(t2,4) - 58*s2*t1*Power(t2,4) + 
            48*Power(t1,2)*Power(t2,4) - 8*s2*Power(t1,2)*Power(t2,4) - 
            48*Power(t2,5) - 20*s2*Power(t2,5) - 
            4*Power(s2,2)*Power(t2,5) + 4*t1*Power(t2,5) + 
            8*s2*t1*Power(t2,5) + 
            Power(s1,5)*(8 - 20*Power(s2,3) + 112*Power(t1,3) + 
               Power(t1,2)*(130 - 70*t2) - 17*t2 + 8*Power(t2,2) + 
               Power(t2,3) + 8*Power(s2,2)*(1 + 15*t1 + 5*t2) + 
               5*t1*(14 - 26*t2 + 15*Power(t2,2)) - 
               s2*(15 + 210*Power(t1,2) - 24*t2 + 26*Power(t2,2) + 
                  10*t1*(10 + t2))) + 
            Power(s1,4)*(79 - 364*t2 + 400*Power(t2,2) - 
               95*Power(t2,3) + 5*Power(t2,4) + 
               9*Power(s2,3)*(-1 + 4*t2) - 15*Power(t1,3)*(-2 + 7*t2) + 
               5*Power(t1,2)*(-2 - 54*t2 + 13*Power(t2,2)) - 
               t1*(128 - 250*t2 + 122*Power(t2,2) + 105*Power(t2,3)) + 
               Power(s2,2)*(16 - 125*t2 - 36*Power(t2,2) - 
                  2*t1*(-6 + 55*t2)) + 
               s2*(30 - 53*t2 + 59*Power(t2,2) + 43*Power(t2,3) + 
                  10*Power(t1,2)*(-1 + 15*t2) + 
                  t1*(30 + 278*t2 + 45*Power(t2,2)))) + 
            Power(s1,3)*(108 - 266*t2 + 425*Power(t2,2) - 
               464*Power(t2,3) + 120*Power(t2,4) - 4*Power(t2,5) - 
               4*Power(t1,3)*(2 + 10*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(-1 + 13*t2 + 7*Power(t2,2)) + 
               2*Power(t1,2)*
                (65 - 254*t2 + 162*Power(t2,2) + 5*Power(t2,3)) + 
               2*t1*(-29 + 206*t2 - 325*Power(t2,2) + 166*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,2)*(33 - 229*t2 + 170*Power(t2,2) + 
                  7*Power(t2,3) - 4*t1*(-8 + 32*t2 + 5*Power(t2,2))) + 
               s2*(14 - 206*t2 + 376*Power(t2,2) - 127*Power(t2,3) + 
                  2*Power(t2,4) + 
                  2*Power(t1,2)*(-4 + 61*t2 + 25*Power(t2,2)) - 
                  4*t1*(40 - 173*t2 + 107*Power(t2,2) + 13*Power(t2,3)))) \
- Power(s1,2)*(-142 + 401*t2 - 380*Power(t2,2) + 18*Power(t2,3) - 
               120*Power(t2,4) + 36*Power(t2,5) - 
               2*Power(t1,3)*t2*(-11 + 16*t2 + 10*Power(t2,2)) + 
               Power(s2,3)*(6 + 13*t2 - 25*Power(t2,2) + 
                  23*Power(t2,3)) + 
               Power(t1,2)*(-224 + 782*t2 - 920*Power(t2,2) + 
                  252*Power(t2,3) + 8*Power(t2,4)) - 
               2*t1*(-110 + 457*t2 - 500*Power(t2,2) + 
                  186*Power(t2,3) - 40*Power(t2,4) + 6*Power(t2,5)) - 
               Power(s2,2)*(-77 + 54*t2 + 131*Power(t2,2) - 
                  71*Power(t2,3) + 4*Power(t2,4) + 
                  2*t1*(22 - 89*t2 + 82*Power(t2,2) + 9*Power(t2,3))) + 
               s2*(-153 + 642*t2 - 656*Power(t2,2) + 446*Power(t2,3) - 
                  72*Power(t2,4) + 12*Power(t2,5) + 
                  Power(t1,2)*
                   (42 - 212*t2 + 228*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*(70 - 620*t2 + 930*Power(t2,2) - 348*Power(t2,3) + 
                     6*Power(t2,4)))) + 
            s1*(-5 - 432*t2 + 478*Power(t2,2) - 144*Power(t2,3) - 
               24*Power(t2,4) - 16*Power(t2,5) + 
               4*Power(t1,3)*t2*(-2 + 3*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*t2*
                (40 + 8*t2 - 27*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(-60 + 1142*t2 - 1297*Power(t2,2) + 508*Power(t2,3) - 
                  34*Power(t2,4)) + 
               2*Power(t1,2)*(36 - 377*t2 + 516*Power(t2,2) - 
                  207*Power(t2,3) + 20*Power(t2,4)) - 
               Power(s2,2)*t2*
                (-168 + 89*t2 + 56*Power(t2,2) - 16*Power(t2,3) + 
                  4*Power(t2,4) + 4*t1*(48 - 47*t2 + 8*Power(t2,2))) + 
               s2*(-2 - 815*t2 + 1003*Power(t2,2) - 510*Power(t2,3) + 
                  122*Power(t2,4) - 16*Power(t2,5) - 
                  2*Power(t1,2)*
                   (9 - 101*t2 + 123*Power(t2,2) - 34*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  t1*(28 + 358*t2 - 788*Power(t2,2) + 460*Power(t2,3) - 
                     80*Power(t2,4) + 8*Power(t2,5))))))*R2q(s))/
     (s*(-1 + s1)*Power(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2),
        2)*(-1 + s2)*(-1 + t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) + 
    (8*(Power(-1 + s1,6)*(s1 - t2)*(-1 + t2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         Power(s,12)*(Power(t1,3) + t1*Power(t2,2) + 2*Power(t2,3)) + 
         Power(s,11)*(Power(t1,3)*(-11 - 11*s1 + 3*t2) + 
            Power(t2,2)*(4 - 3*s2 + 2*s1*(1 + 2*s2 - 8*t2) - 16*t2 + 
               8*Power(t2,2)) + 
            t1*t2*(3 + s1*(-1 + 3*s2 - 10*t2) - 5*t2 + Power(t2,2) - 
               s2*(3 + t2)) + 
            Power(t1,2)*(3 + s2*t2 + s1*(-3 + 3*s2 + t2))) + 
         Power(s,10)*(Power(t1,3)*
             (52 + 55*Power(s1,2) + s1*(95 - 30*t2) - 31*t2 + 
               3*Power(t2,2)) + 
            Power(t1,2)*(-30 + (4 - 6*s2)*t2 + 3*(1 + s2)*Power(t2,2) - 
               2*Power(s1,2)*(-14 + 15*s2 + 5*t2) + 
               s1*(-26*s2 + t2*(-23 + 3*t2))) + 
            t2*(3 - 34*t2 + 59*Power(t2,2) - 57*Power(t2,3) + 
               10*Power(t2,4) + Power(s2,2)*(t2 - 2*Power(t2,2)) + 
               Power(s1,2)*(1 + 3*Power(s2,2) + s2*(2 - 31*t2) - 
                  15*t2 + 56*Power(t2,2)) - 
               s2*(3 - 25*t2 + 7*Power(t2,2) + 4*Power(t2,3)) + 
               s1*(2 - 39*t2 + 101*Power(t2,2) - 56*Power(t2,3) - 
                  Power(s2,2)*(3 + 5*t2) + s2*(3 + 10*Power(t2,2)))) + 
            t1*(3 + (-30 + 29*s2)*t2 + (9 - 5*s2)*Power(t2,2) - 
               (-5 + s2)*Power(t2,3) - Power(t2,4) + 
               Power(s1,2)*(1 + 3*Power(s2,2) + 4*t2 + 45*Power(t2,2) - 
                  s2*(3 + 25*t2)) + 
               s1*(-3 - 3*t2 - Power(s2,2)*t2 + 23*Power(t2,2) - 
                  11*Power(t2,3) + s2*(3 + 2*t2 + 20*Power(t2,2))))) + 
         Power(s,9)*(1 - 27*t2 + 25*s2*t2 + 112*Power(t2,2) - 
            72*s2*Power(t2,2) - 16*Power(s2,2)*Power(t2,2) - 
            120*Power(t2,3) + 49*s2*Power(t2,3) + 
            19*Power(s2,2)*Power(t2,3) + 148*Power(t2,4) + 
            29*s2*Power(t2,4) - 4*Power(s2,2)*Power(t2,4) - 
            61*Power(t2,5) - 8*s2*Power(t2,5) + 4*Power(t2,6) + 
            Power(t1,3)*(-136 + 134*t2 - 28*Power(t2,2) + Power(t2,3)) + 
            Power(t1,2)*(126 + (-30 + 4*s2)*t2 - 
               (31 + 18*s2)*Power(t2,2) + (5 + 3*s2)*Power(t2,3)) - 
            t1*(27 + (-122 + 109*s2)*t2 + 
               (28 - 89*s2 - 2*Power(s2,2))*Power(t2,2) + 
               3*(19 + 5*s2)*Power(t2,3) - (24 + s2)*Power(t2,4) + 
               Power(t2,5)) + 
            Power(s1,3)*(Power(s2,3) - 165*Power(t1,3) + 
               9*Power(t1,2)*(-13 + 5*t2) - 
               Power(s2,2)*(27*t1 + 23*t2) - 
               7*t2*(1 - 7*t2 + 16*Power(t2,2)) - 
               2*t1*(5 - 2*t2 + 60*Power(t2,2)) + 
               s2*(1 + 135*Power(t1,2) - 14*t2 + 104*Power(t2,2) + 
                  t1*(26 + 90*t2))) + 
            Power(s1,2)*(1 - (17 + 22*s2 + 2*Power(s2,3))*t2 + 
               3*(45 + 16*s2 + 14*Power(s2,2))*Power(t2,2) - 
               (262 + 67*s2)*Power(t2,3) + 168*Power(t2,4) + 
               45*Power(t1,3)*(-8 + 3*t2) + 
               Power(t1,2)*(-85 + s2*(196 - 45*t2) + 185*t2 - 
                  27*Power(t2,2)) + 
               t1*(23 - 36*t2 - 29*Power(t2,2) + 52*Power(t2,3) + 
                  Power(s2,2)*(-22 + 17*t2) + 
                  s2*(-3 + 34*t2 - 117*Power(t2,2)))) + 
            s1*(Power(s2,3)*Power(t2,2) + 
               Power(t1,3)*(-336 + 238*t2 - 27*Power(t2,2)) + 
               Power(s2,2)*t2*
                (22 - 4*t1*(-4 + t2) + 21*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*(100 + 134*t2 - 74*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*t2*(57 + 80*t2 - 50*Power(t2,2) + 4*Power(t2,3)) + 
               t2*(-29 + 195*t2 - 288*Power(t2,2) + 276*Power(t2,3) - 
                  60*Power(t2,4)) - 
               s2*(Power(t2,2)*(116 + 5*t2 - 28*Power(t2,2)) + 
                  Power(t1,2)*(-94 + 36*t2 + 15*Power(t2,2)) + 
                  t1*(22 + 139*t2 + 20*Power(t2,2) - 27*Power(t2,3))))) + 
         Power(s,8)*(-8 + 94*t2 - 79*s2*t2 - 138*Power(t2,2) + 
            33*s2*Power(t2,2) + 87*Power(s2,2)*Power(t2,2) + 
            68*Power(t2,3) - 63*s2*Power(t2,3) - 
            97*Power(s2,2)*Power(t2,3) - 2*Power(s2,3)*Power(t2,3) - 
            41*Power(t2,4) - 85*s2*Power(t2,4) + 
            24*Power(s2,2)*Power(t2,4) + 113*Power(t2,5) + 
            55*s2*Power(t2,5) - 2*Power(s2,2)*Power(t2,5) - 
            20*Power(t2,6) - 4*s2*Power(t2,6) + 
            Power(t1,3)*(206 - 309*t2 + 106*Power(t2,2) - 
               8*Power(t2,3)) + 
            Power(t1,2)*(-282 + (62 + 64*s2)*t2 + 
               (155 + 18*s2)*Power(t2,2) - 2*(25 + 8*s2)*Power(t2,3) + 
               (2 + s2)*Power(t2,4)) + 
            t1*(99 + (-235 + 181*s2)*t2 - 
               4*(-25 + 87*s2 + 4*Power(s2,2))*Power(t2,2) + 
               3*(39 + 53*s2 + 2*Power(s2,2))*Power(t2,3) - 
               (121 + 21*s2)*Power(t2,4) + (18 + s2)*Power(t2,5)) + 
            Power(s1,4)*(-8*Power(s2,3) + 330*Power(t1,3) - 
               24*Power(t1,2)*(-12 + 5*t2) + 
               Power(s2,2)*(1 + 108*t1 + 76*t2) + 
               14*t1*(3 - 4*t2 + 15*Power(t2,2)) + 
               7*t2*(3 - 13*t2 + 20*Power(t2,2)) - 
               s2*(360*Power(t1,2) + 20*t1*(5 + 9*t2) + 
                  7*(1 - 6*t2 + 28*Power(t2,2)))) + 
            Power(s1,3)*(-7 + 45*t2 - 202*Power(t2,2) + 
               343*Power(t2,3) - 280*Power(t2,4) - 
               60*Power(t1,3)*(-13 + 6*t2) + Power(s2,3)*(-7 + 17*t2) + 
               Power(t1,2)*(341 - 652*t2 + 108*Power(t2,2)) - 
               t1*(71 - 117*t2 + 20*Power(t2,2) + 140*Power(t2,3)) + 
               Power(s2,2)*(-1 + 40*t2 - 150*Power(t2,2) - 
                  2*t1*(-71 + 50*t2)) + 
               s2*(2 + 59*t2 - 122*Power(t2,2) + 189*Power(t2,3) + 
                  8*Power(t1,2)*(-79 + 30*t2) + 
                  t1*(-49 - 38*t2 + 336*Power(t2,2)))) + 
            Power(s1,2)*(-1 + 97*t2 - 406*Power(t2,2) + 
               483*Power(t2,3) - 483*Power(t2,4) + 150*Power(t2,5) - 
               2*Power(s2,3)*t2*(-9 + 5*t2) + 
               Power(t1,3)*(902 - 789*t2 + 108*Power(t2,2)) - 
               Power(t1,2)*(28 + 904*t2 - 433*Power(t2,2) + 
                  24*Power(t2,3)) + 
               3*t1*(-23 + 34*t2 - 165*Power(t2,2) + 62*Power(t2,3)) + 
               Power(s2,2)*(t1*(69 - 167*t2 + 36*Power(t2,2)) + 
                  t2*(-63 - 163*t2 + 63*Power(t2,2))) + 
               s2*(-1 - 28*t2 + 330*Power(t2,2) + 64*Power(t2,3) - 
                  81*Power(t2,4) + 
                  Power(t1,2)*(-501 + 392*t2 + 12*Power(t2,2)) + 
                  t1*(43 + 504*t2 + 129*Power(t2,2) - 152*Power(t2,3)))) \
+ s1*(-8 + 120*t2 - 352*Power(t2,2) + 348*Power(t2,3) - 
               423*Power(t2,4) + 214*Power(t2,5) - 20*Power(t2,6) + 
               Power(s2,3)*Power(t2,2)*(-13 + 5*t2) + 
               Power(t1,3)*(614 - 734*t2 + 186*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-374 - 420*t2 + 492*Power(t2,2) - 
                  76*Power(t2,3) + Power(t2,4)) + 
               t1*(71 - 173*t2 - 248*Power(t2,2) + 417*Power(t2,3) - 
                  132*Power(t2,4) + 5*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-69 + 73*t2 + 16*Power(t2,2) + 9*Power(t2,3) + 
                  t1*(-81 + 39*t2 - 8*Power(t2,2))) + 
               s2*(1 - 58*t2 + 340*Power(t2,2) - 197*Power(t2,3) - 
                  116*Power(t2,4) + 34*Power(t2,5) - 
                  2*Power(t1,2)*
                   (92 - 121*t2 - 15*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(67 + 582*t2 - 499*Power(t2,2) + 7*Power(t2,3) + 
                     10*Power(t2,4))))) + 
         Power(s,7)*(25 - 183*t1 + 336*Power(t1,2) - 154*Power(t1,3) - 
            140*t2 + 103*s2*t2 + 90*t1*t2 - 27*s2*t1*t2 + 
            91*Power(t1,2)*t2 - 266*s2*Power(t1,2)*t2 + 
            384*Power(t1,3)*t2 - 182*Power(t2,2) + 322*s2*Power(t2,2) - 
            238*Power(s2,2)*Power(t2,2) - 21*t1*Power(t2,2) + 
            551*s2*t1*Power(t2,2) + 56*Power(s2,2)*t1*Power(t2,2) - 
            514*Power(t1,2)*Power(t2,2) + 
            129*s2*Power(t1,2)*Power(t2,2) - 
            203*Power(t1,3)*Power(t2,2) + 424*Power(t2,3) - 
            373*s2*Power(t2,3) + 294*Power(s2,2)*Power(t2,3) + 
            16*Power(s2,3)*Power(t2,3) + 32*t1*Power(t2,3) - 
            497*s2*t1*Power(t2,3) - 50*Power(s2,2)*t1*Power(t2,3) + 
            213*Power(t1,2)*Power(t2,3) + 
            14*s2*Power(t1,2)*Power(t2,3) + 24*Power(t1,3)*Power(t2,3) - 
            682*Power(t2,4) + 161*s2*Power(t2,4) - 
            66*Power(s2,2)*Power(t2,4) - 4*Power(s2,3)*Power(t2,4) + 
            217*t1*Power(t2,4) + 118*s2*t1*Power(t2,4) + 
            7*Power(s2,2)*t1*Power(t2,4) - 16*Power(t1,2)*Power(t2,4) - 
            4*s2*Power(t1,2)*Power(t2,4) + 139*Power(t2,5) - 
            132*s2*Power(t2,5) - 90*t1*Power(t2,5) - 
            6*s2*t1*Power(t2,5) + 30*Power(t2,6) + 20*s2*Power(t2,6) + 
            4*t1*Power(t2,6) + 
            7*Power(s1,5)*(4*Power(s2,3) - 66*Power(t1,3) + 
               6*Power(t1,2)*(-11 + 5*t2) - 
               Power(s2,2)*(1 + 36*t1 + 20*t2) + 
               t2*(-5 + 15*t2 - 16*Power(t2,2)) - 
               2*t1*(7 - 11*t2 + 18*Power(t2,2)) + 
               s2*(3 + 90*Power(t1,2) - 10*t2 + 32*Power(t2,2) + 
                  t1*(32 + 30*t2))) - 
            Power(s1,4)*(-17 + 26*t2 - 59*Power(t2,2) + 
               196*Power(t2,3) - 280*Power(t2,4) - 
               210*Power(t1,3)*(-5 + 3*t2) + 
               Power(s2,3)*(-38 + 63*t2) + 
               Power(s2,2)*(3 + t1*(378 - 308*t2) + 69*t2 - 
                  294*Power(t2,2)) + 
               7*Power(t1,2)*(91 - 188*t2 + 36*Power(t2,2)) - 
               14*t1*(9 - 11*t2 + 5*Power(t2,2) + 17*Power(t2,3)) + 
               s2*(12 + 70*t2 - 103*Power(t2,2) + 287*Power(t2,3) + 
                  70*Power(t1,2)*(-16 + 9*t2) + 
                  2*t1*(-93 + 112*t2 + 273*Power(t2,2)))) + 
            Power(s1,3)*(5 - 121*t2 + 279*Power(t2,2) - 
               119*Power(t2,3) + 256*Power(t2,4) - 200*Power(t2,5) + 
               Power(s2,3)*(22 - 108*t2 + 41*Power(t2,2)) - 
               3*Power(t1,3)*(425 - 488*t2 + 84*Power(t2,2)) + 
               Power(t1,2)*(-331 + 2391*t2 - 1211*Power(t2,2) + 
                  84*Power(t2,3)) + 
               t1*(173 - 340*t2 + 957*Power(t2,2) - 343*Power(t2,3) - 
                  28*Power(t2,4)) + 
               Power(s2,2)*(-8 + 135*t2 + 400*Power(t2,2) - 
                  197*Power(t2,3) + 
                  t1*(-301 + 638*t2 - 140*Power(t2,2))) + 
               s2*(-25 + 211*t2 - 722*Power(t2,2) - 3*Power(t2,3) + 
                  122*Power(t2,4) + 
                  Power(t1,2)*(1052 - 1235*t2 + 84*Power(t2,2)) + 
                  t1*(130 - 1505*t2 - 80*Power(t2,2) + 392*Power(t2,3)))\
) + Power(s1,2)*(8 - 173*t2 + 222*Power(t2,2) - 23*Power(t2,3) + 
               141*Power(t2,4) - 187*Power(t2,5) + 40*Power(t2,6) + 
               Power(s2,3)*t2*(-70 + 108*t2 - 25*Power(t2,2)) + 
               Power(t1,3)*(-1050 + 1594*t2 - 519*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,2)*(170 + 2173*t2 - 2022*Power(t2,2) + 
                  345*Power(t2,3) - 7*Power(t2,4)) + 
               t1*(8 - 83*t2 + 676*Power(t2,2) - 986*Power(t2,3) + 
                  297*Power(t2,4) - 9*Power(t2,5)) + 
               Power(s2,2)*(t2*
                   (276 - 141*t2 - 287*Power(t2,2) + 12*Power(t2,3)) + 
                  t1*(-122 + 542*t2 - 315*Power(t2,2) + 48*Power(t2,3))\
) + s2*(-5 + 281*t2 - 885*Power(t2,2) + 689*Power(t2,3) + 
                  62*Power(t2,4) - 49*Power(t2,5) + 
                  3*Power(t1,2)*
                   (212 - 370*t2 + 59*Power(t2,2) + 14*Power(t2,3)) + 
                  t1*(-4 - 2037*t2 + 1745*Power(t2,2) + 
                     93*Power(t2,3) - 76*Power(t2,4)))) + 
            s1*(39 - 113*t2 - 64*Power(t2,2) + 507*Power(t2,3) - 
               444*Power(t2,4) - 117*Power(t2,5) + 38*Power(t2,6) + 
               Power(s2,3)*Power(t2,2)*(52 - 42*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(-585 + 1080*t2 - 466*Power(t2,2) + 
                  44*Power(t2,3)) + 
               Power(t1,2)*(510 + 1043*t2 - 1605*Power(t2,2) + 
                  470*Power(t2,3) - 23*Power(t2,4)) + 
               t1*(-179 + 63*t2 + 44*Power(t2,2) - 493*Power(t2,3) + 
                  477*Power(t2,4) - 79*Power(t2,5)) + 
               Power(s2,2)*t2*
                (128 - 566*t2 + 165*Power(t2,2) + 7*Power(t2,3) + 
                  11*Power(t2,4) + 
                  t1*(196 - 147*t2 + 46*Power(t2,2) - 8*Power(t2,3))) + 
               s2*(-3 + 88*t2 - 31*Power(t2,2) + 321*Power(t2,3) + 
                  104*Power(t2,4) - 113*Power(t2,5) + 12*Power(t2,6) + 
                  Power(t1,2)*
                   (216 - 655*t2 + 174*Power(t2,2) + 48*Power(t2,3) - 
                     6*Power(t2,4)) + 
                  t1*(-118 - 1087*t2 + 1958*Power(t2,2) - 
                     736*Power(t2,3) + 24*Power(t2,4))))) + 
         s*Power(-1 + s1,4)*(Power(s1,7)*Power(s2 - t1,2)*
             (-1 + s2 - t1 + t2) - 
            Power(s1,6)*(s2 - t1)*
             (-Power(-1 + t2,2) + 3*Power(s2,2)*t2 + 3*Power(t1,2)*t2 + 
               t1*(-2 + 5*t2 - 3*Power(t2,2)) + 
               s2*(3 - (7 + 6*t1)*t2 + 4*Power(t2,2))) + 
            Power(s1,5)*(s2 - t1)*(-1 + t2)*
             (-1 + 4*t2 - 3*Power(t2,2) + Power(s2,2)*(-5 + 2*t2) + 
               Power(t1,2)*(-7 + 3*t2) + 
               t1*(-11 + 8*t2 - 3*Power(t2,2)) + 
               s2*(5 + t1*(13 - 6*t2) - 5*t2 + 6*Power(t2,2))) - 
            (-1 + t2)*(-4 + (31 - 8*s2)*t2 + 
               (-58 + 39*s2 - 2*Power(s2,2))*Power(t2,2) + 
               (37 - 36*s2 + 9*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) + (-4 + s2 + 3*Power(s2,3))*Power(t2,4) - 
               2*Power(-1 + s2,2)*Power(t2,5) + 
               Power(t1,3)*(4 - 16*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-12 + (63 - 8*s2)*t2 + 
                  (-58 + 37*s2)*Power(t2,2) - 6*(-2 + s2)*Power(t2,3)) \
+ t1*(12 + 2*(-39 + 8*s2)*t2 + 
                  (113 - 76*s2 + 2*Power(s2,2))*Power(t2,2) - 
                  6*(9 - 10*s2 + 4*Power(s2,2))*Power(t2,3) + 
                  (7 - 10*s2 + 3*Power(s2,2))*Power(t2,4))) + 
            Power(s1,4)*(-Power(-1 + t2,3) - 
               t1*Power(-1 + t2,2)*(13 - 13*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(20 - 22*t2 - 9*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(6 - 27*t2 + 29*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,2)*(1 + 41*t2 - 47*Power(t2,2) + 
                  6*Power(t2,3) - Power(t2,4)) + 
               Power(s2,2)*(-25 + 84*t2 - 58*Power(t2,2) + 
                  3*Power(t2,3) - 4*Power(t2,4) + 
                  t1*(10 + 31*t2 - 71*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(5 - 5*t2 + 3*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (12 - 6*t2 - 17*Power(t2,2) + Power(t2,3)) + 
                  t1*(31 - 146*t2 + 126*Power(t2,2) - 16*Power(t2,3) + 
                     5*Power(t2,4)))) + 
            Power(s1,3)*(-5*Power(-1 + t2,3)*t2 + 
               Power(t1,3)*(-7 - 4*t2 + 26*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (4 + 29*t2 - 13*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(8 - 57*t2 + 10*Power(t2,2) + 
                  38*Power(t2,3) + Power(t2,4)) - 
               Power(s2,3)*(2 + 19*t2 - 54*Power(t2,2) + 
                  45*Power(t2,3) + 3*Power(t2,4)) + 
               Power(s2,2)*(6 + 45*t2 - 185*Power(t2,2) + 
                  131*Power(t2,3) + 2*Power(t2,4) + Power(t2,5) + 
                  t1*(9 - 18*t2 - 17*Power(t2,2) + 68*Power(t2,3) + 
                     3*Power(t2,4))) - 
               s2*(Power(-1 + t2,2)*
                   (-13 + 44*t2 - 11*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-5 - 26*t2 + 48*Power(t2,2) + 28*Power(t2,3)) + 
                  t1*(36 - 71*t2 - 130*Power(t2,2) + 168*Power(t2,3) - 
                     4*Power(t2,4) + Power(t2,5)))) + 
            Power(s1,2)*(Power(-1 + t2,3)*(-7 + 3*t2 + 8*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (-28 - 5*t2 - 18*Power(t2,2) + Power(t2,3)) - 
               Power(t1,3)*(8 + t2 - 6*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(29 - 41*t2 + 59*Power(t2,2) - 
                  36*Power(t2,3) - 11*Power(t2,4)) + 
               Power(s2,3)*t2*
                (6 + 21*t2 - 50*Power(t2,2) + 31*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(Power(t1,2)*t2*
                   (47 - 48*t2 + 22*Power(t2,2) + 6*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (-2 - 3*t2 - 56*Power(t2,2) + 11*Power(t2,3)) + 
                  t1*(-2 + 8*t2 - 85*Power(t2,2) + 4*Power(t2,3) + 
                     75*Power(t2,4))) - 
               Power(s2,2)*(2 + t2 + 24*Power(t2,2) - 136*Power(t2,3) + 
                  111*Power(t2,4) - 2*Power(t2,5) + 
                  t1*(-2 + 44*t2 - 33*Power(t2,2) - 10*Power(t2,3) + 
                     27*Power(t2,4) + Power(t2,5)))) - 
            s1*(Power(t1,3)*(1 - 34*t2 + 39*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (-14 - 5*t2 + 6*Power(t2,2) + 2*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (27 + 26*t2 - 13*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (6 + 9*t2 - 21*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(12 + 43*t2 - 113*Power(t2,2) + 
                  81*Power(t2,3) - 23*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-4 + 16*t2 - 13*Power(t2,2) + 28*Power(t2,3) - 
                  29*Power(t2,4) + 2*Power(t2,5) + 
                  t1*(4 - 61*t2 + 52*Power(t2,2) + 3*Power(t2,3) - 
                     4*Power(t2,4))) + 
               s2*(-(Power(-1 + t2,2)*
                     (8 - 33*t2 - 11*Power(t2,2) - 13*Power(t2,3) + 
                       4*Power(t2,4))) + 
                  Power(t1,2)*
                   (-8 + 45*t2 + 9*Power(t2,2) - 52*Power(t2,3) + 
                     12*Power(t2,4)) + 
                  t1*(16 - 94*t2 + 108*Power(t2,2) - 53*Power(t2,3) + 
                     14*Power(t2,4) + 9*Power(t2,5))))) - 
         Power(s,6)*(36 - 153*t1 + 126*Power(t1,2) + 28*Power(t1,3) + 
            5*t2 - 13*s2*t2 - 602*t1*t2 + 433*s2*t1*t2 + 
            675*Power(t1,2)*t2 - 532*s2*Power(t1,2)*t2 + 
            168*Power(t1,3)*t2 - 1005*Power(t2,2) + 989*s2*Power(t2,2) - 
            377*Power(s2,2)*Power(t2,2) + 771*t1*Power(t2,2) + 
            112*s2*t1*Power(t2,2) + 112*Power(s2,2)*t1*Power(t2,2) - 
            1211*Power(t1,2)*Power(t2,2) + 
            525*s2*Power(t1,2)*Power(t2,2) - 
            181*Power(t1,3)*Power(t2,2) + 1613*Power(t2,3) - 
            1597*s2*Power(t2,3) + 500*Power(s2,2)*Power(t2,3) + 
            52*Power(s2,3)*Power(t2,3) + 238*t1*Power(t2,3) - 
            583*s2*t1*Power(t2,3) - 171*Power(s2,2)*t1*Power(t2,3) + 
            533*Power(t1,2)*Power(t2,3) - 
            93*s2*Power(t1,2)*Power(t2,3) + 32*Power(t1,3)*Power(t2,3) - 
            1854*Power(t2,4) + 383*s2*Power(t2,4) - 
            107*Power(s2,2)*Power(t2,4) - 24*Power(s2,3)*Power(t2,4) + 
            64*t1*Power(t2,4) + 248*s2*t1*Power(t2,4) + 
            47*Power(s2,2)*t1*Power(t2,4) - 54*Power(t1,2)*Power(t2,4) + 
            s2*Power(t1,2)*Power(t2,4) + 894*Power(t2,5) - 
            49*s2*Power(t2,5) - 35*Power(s2,2)*Power(t2,5) + 
            2*Power(s2,3)*Power(t2,5) - 192*t1*Power(t2,5) - 
            23*s2*t1*Power(t2,5) - 3*Power(s2,2)*t1*Power(t2,5) - 
            58*Power(t2,6) + 26*s2*Power(t2,6) + 
            6*Power(s2,2)*Power(t2,6) + 26*t1*Power(t2,6) - 
            2*s2*t1*Power(t2,6) - 6*Power(t2,7) + 2*s2*Power(t2,7) + 
            7*Power(s1,6)*(8*Power(s2,3) - 66*Power(t1,3) + 
               36*Power(t1,2)*(-2 + t2) - 
               Power(s2,2)*(3 + 54*t1 + 22*t2) + 
               t1*(-20 + 32*t2 - 30*Power(t2,2)) + 
               t2*(-5 + 11*t2 - 8*Power(t2,2)) + 
               s2*(5 + 108*Power(t1,2) - 10*t2 + 22*Power(t2,2) + 
                  2*t1*(23 + 9*t2))) + 
            Power(s1,5)*(15 + Power(s2,3)*(81 - 133*t2) + 86*t2 - 
               236*Power(t2,2) + 49*Power(t2,3) + 168*Power(t2,4) + 
               126*Power(t1,3)*(-7 + 6*t2) - 
               7*Power(t1,2)*(95 - 238*t2 + 54*Power(t2,2)) + 
               t1*(159 - 105*t2 + 14*Power(t2,2) + 266*Power(t2,3)) + 
               Power(s2,2)*(-29 + 48*t2 + 336*Power(t2,2) + 
                  14*t1*(-37 + 41*t2)) - 
               s2*(33 + 27*t2 + 12*Power(t2,2) + 245*Power(t2,3) + 
                  28*Power(t1,2)*(-41 + 36*t2) + 
                  t1*(-297 + 742*t2 + 504*Power(t2,2)))) + 
            Power(s1,4)*(-10 - 6*t2 - 214*Power(t2,2) + 
               680*Power(t2,3) - 295*Power(t2,4) - 150*Power(t2,5) + 
               4*Power(s2,3)*(19 - 65*t2 + 23*Power(t2,2)) - 
               7*Power(t1,3)*(139 - 234*t2 + 54*Power(t2,2)) + 
               Power(t1,2)*(-684 + 3370*t2 - 1939*Power(t2,2) + 
                  168*Power(t2,3)) + 
               t1*(109 - 216*t2 + 787*Power(t2,2) - 322*Power(t2,3) - 
                  70*Power(t2,4)) + 
               Power(s2,2)*(-77 + 382*t2 + 365*Power(t2,2) - 
                  279*Power(t2,3) + 
                  t1*(-491 + 1220*t2 - 308*Power(t2,2))) + 
               s2*(-64 + 456*t2 - 1053*Power(t2,2) + 277*Power(t2,3) + 
                  95*Power(t2,4) + 
                  Power(t1,2)*(1058 - 1953*t2 + 294*Power(t2,2)) + 
                  t1*(565 - 2793*t2 + 479*Power(t2,2) + 546*Power(t2,3))\
)) - Power(s1,3)*(-11 + 8*t2 + 487*Power(t2,2) - 928*Power(t2,3) + 
               772*Power(t2,4) - 190*Power(t2,5) - 40*Power(t2,6) + 
               Power(t1,3)*(793 - 1646*t2 + 777*Power(t2,2) - 
                  56*Power(t2,3)) + 
               Power(s2,3)*(-42 + 284*t2 - 324*Power(t2,2) + 
                  48*Power(t2,3)) + 
               Power(t1,2)*(604 - 3884*t2 + 3821*Power(t2,2) - 
                  754*Power(t2,3) + 21*Power(t2,4)) + 
               t1*(56 - 250*t2 + 130*Power(t2,2) + 895*Power(t2,3) - 
                  342*Power(t2,4) + 5*Power(t2,5)) + 
               Power(s2,2)*(78 - 725*t2 + 513*Power(t2,2) + 
                  543*Power(t2,3) - 51*Power(t2,4) - 
                  2*t1*(-168 + 624*t2 - 457*Power(t2,2) + 
                     60*Power(t2,3))) + 
               s2*(81 - 582*t2 + 1211*Power(t2,2) - 1543*Power(t2,3) + 
                  332*Power(t2,4) + 15*Power(t2,5) - 
                  2*Power(t1,2)*
                   (376 - 895*t2 + 345*Power(t2,2) + 21*Power(t2,3)) + 
                  t1*(-645 + 4203*t2 - 3933*Power(t2,2) - 
                     65*Power(t2,3) + 190*Power(t2,4)))) + 
            s1*(42 + 417*t2 - 1439*Power(t2,2) + 2637*Power(t2,3) - 
               2616*Power(t2,4) + 765*Power(t2,5) - 6*Power(t2,6) + 
               4*Power(t2,7) + 
               Power(s2,3)*Power(t2,2)*
                (82 - 128*t2 + 43*Power(t2,2) - 3*Power(t2,3)) + 
               Power(t1,3)*(-233 + 694*t2 - 483*Power(t2,2) + 
                  72*Power(t2,3)) + 
               Power(t1,2)*(113 + 2048*t2 - 3091*Power(t2,2) + 
                  1224*Power(t2,3) - 109*Power(t2,4)) + 
               t1*(-16 - 669*t2 - 338*Power(t2,2) + 775*Power(t2,3) + 
                  337*Power(t2,4) - 261*Power(t2,5) + 16*Power(t2,6)) + 
               Power(s2,2)*t2*
                (169 - 1279*t2 + 929*Power(t2,2) + 12*Power(t2,3) - 
                  12*Power(t2,4) - 4*Power(t2,5) + 
                  t1*(251 - 246*t2 + 64*Power(t2,2) - 26*Power(t2,3) + 
                     3*Power(t2,4))) - 
               s2*(-8 + 197*t2 - 1587*Power(t2,2) + 890*Power(t2,3) + 
                  76*Power(t2,4) + 43*Power(t2,5) - 2*Power(t2,6) + 
                  2*Power(t1,2)*
                   (-88 + 444*t2 - 330*Power(t2,2) + 22*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  t1*(161 + 1142*t2 - 3017*Power(t2,2) + 
                     2015*Power(t2,3) - 323*Power(t2,4) - 13*Power(t2,5)\
))) + Power(s1,2)*(-11 + 160*t2 - 992*Power(t2,2) + 2049*Power(t2,3) - 
               1588*Power(t2,4) + 312*Power(t2,5) - 38*Power(t2,6) + 
               2*Power(s2,3)*t2*
                (-72 + 178*t2 - 103*Power(t2,2) + 12*Power(t2,3)) + 
               3*Power(t1,3)*
                (-175 + 414*t2 - 247*Power(t2,2) + 32*Power(t2,3)) + 
               Power(t1,2)*(-350 + 3433*t2 - 4200*Power(t2,2) + 
                  1385*Power(t2,3) - 80*Power(t2,4)) + 
               t1*(51 + 74*t2 - 1000*Power(t2,2) + 202*Power(t2,3) + 
                  621*Power(t2,4) - 138*Power(t2,5)) + 
               Power(s2,2)*(-2 + 725*t2 - 1424*Power(t2,2) + 
                  175*Power(t2,3) + 165*Power(t2,4) + 26*Power(t2,5) + 
                  t1*(-133 + 771*t2 - 836*Power(t2,2) + 
                     307*Power(t2,3) - 37*Power(t2,4))) + 
               s2*(-20 + 56*t2 - 112*Power(t2,2) + 912*Power(t2,3) - 
                  549*Power(t2,4) + 86*Power(t2,5) + 8*Power(t2,6) - 
                  Power(t1,2)*
                   (-438 + 1349*t2 - 759*Power(t2,2) + Power(t2,3) + 
                     15*Power(t2,4)) + 
                  t1*(242 - 3355*t2 + 5141*Power(t2,2) - 
                     1893*Power(t2,3) - 67*Power(t2,4) + 14*Power(t2,5)))\
)) - Power(s,2)*Power(-1 + s1,2)*
          (-20 + 72*t1 - 84*Power(t1,2) + 32*Power(t1,3) + 190*t2 - 
            62*s2*t2 - 500*t1*t2 + 128*s2*t1*t2 + 421*Power(t1,2)*t2 - 
            66*s2*Power(t1,2)*t2 - 111*Power(t1,3)*t2 - 
            487*Power(t2,2) + 262*s2*Power(t2,2) - 
            18*Power(s2,2)*Power(t2,2) + 970*t1*Power(t2,2) - 
            469*s2*t1*Power(t2,2) + 16*Power(s2,2)*t1*Power(t2,2) - 
            614*Power(t1,2)*Power(t2,2) + 
            234*s2*Power(t1,2)*Power(t2,2) + 
            92*Power(t1,3)*Power(t2,2) + 552*Power(t2,3) - 
            348*s2*Power(t2,3) - 2*Power(s2,2)*Power(t2,3) + 
            16*Power(s2,3)*Power(t2,3) - 753*t1*Power(t2,3) + 
            574*s2*t1*Power(t2,3) - 105*Power(s2,2)*t1*Power(t2,3) + 
            331*Power(t1,2)*Power(t2,3) - 
            191*s2*Power(t1,2)*Power(t2,3) - 
            16*Power(t1,3)*Power(t2,3) - 295*Power(t2,4) + 
            125*s2*Power(t2,4) + 57*Power(s2,2)*Power(t2,4) - 
            14*Power(s2,3)*Power(t2,4) + 225*t1*Power(t2,4) - 
            266*s2*t1*Power(t2,4) + 97*Power(s2,2)*t1*Power(t2,4) - 
            58*Power(t1,2)*Power(t2,4) + 31*s2*Power(t1,2)*Power(t2,4) + 
            54*Power(t2,5) + 69*s2*Power(t2,5) - 
            51*Power(s2,2)*Power(t2,5) - 12*t1*Power(t2,5) + 
            39*s2*t1*Power(t2,5) - 15*Power(s2,2)*t1*Power(t2,5) + 
            4*Power(t2,6) - 50*s2*Power(t2,6) + 
            10*Power(s2,2)*Power(t2,6) - 2*t1*Power(t2,6) + 
            2*s2*t1*Power(t2,6) + 2*Power(t2,7) + 4*s2*Power(t2,7) + 
            Power(s1,8)*(8*Power(s2,3) + 
               Power(s2,2)*(-7 - 27*t1 + 5*t2) + 
               s2*(1 + 30*Power(t1,2) + t1*(19 - 15*t2) - 
                  Power(t2,2)) - 
               t1*(3 + 11*Power(t1,2) - 4*t2 + Power(t2,2) - 
                  2*t1*(-6 + 5*t2))) + 
            Power(s1,7)*(Power(-1 + t2,2)*(-3 + 4*t2) - 
               Power(s2,3)*(1 + 23*t2) + Power(t1,3)*(3 + 30*t2) + 
               Power(t1,2)*(-11 + 51*t2 - 27*Power(t2,2)) + 
               t1*t2*(14 - 17*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(-17 + 51*t2 - 21*Power(t2,2) + 
                  t1*(8 + 73*t2)) + 
               s2*(1 - 10*t2 + 6*Power(t2,2) + 3*Power(t2,3) - 
                  10*Power(t1,2)*(1 + 8*t2) + 
                  t1*(24 - 94*t2 + 44*Power(t2,2)))) + 
            Power(s1,6)*(4*Power(s2,3)*(3 - 6*t2 + 4*Power(t2,2)) - 
               3*Power(t1,3)*(6 - 13*t2 + 9*Power(t2,2)) - 
               Power(-1 + t2,2)*(-10 - t2 + 14*Power(t2,2)) + 
               2*Power(t1,2)*
                (-38 + 69*t2 - 50*Power(t2,2) + 12*Power(t2,3)) + 
               t1*(-25 + 46*t2 - 51*Power(t2,2) + 33*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,2)*(-46 + 81*t2 - 82*Power(t2,2) + 
                  33*Power(t2,3) + t1*(-48 + 94*t2 - 60*Power(t2,2))) + 
               s2*(3 + 7*t2 + 5*Power(t2,2) - 12*Power(t2,3) - 
                  3*Power(t2,4) + 
                  Power(t1,2)*(52 - 105*t2 + 69*Power(t2,2)) + 
                  t1*(116 - 193*t2 + 148*Power(t2,2) - 43*Power(t2,3)))) \
+ Power(s1,4)*(5*Power(t1,3)*(-3 - t2 + 22*Power(t2,2)) + 
               Power(t1,2)*(28 - 141*t2 + 70*Power(t2,2) + 
                  195*Power(t2,3) - 2*Power(t2,4)) - 
               2*Power(s2,3)*
                (5 + t2 - 40*Power(t2,2) + 61*Power(t2,3) + 
                  8*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (9 - 22*t2 + 50*Power(t2,2) + 4*Power(t2,3) + 
                  10*Power(t2,4)) + 
               t1*(28 - 48*t2 - 233*Power(t2,2) + 313*Power(t2,3) - 
                  64*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s2,2)*(49 + 58*t2 - 588*Power(t2,2) + 
                  636*Power(t2,3) - 11*Power(t2,4) + 6*Power(t2,5) + 
                  t1*(7 + 23*t2 - 68*Power(t2,2) + 261*Power(t2,3) + 
                     7*Power(t2,4))) + 
               s2*(-24 - 203*t2 + 707*Power(t2,2) - 516*Power(t2,3) + 
                  32*Power(t2,4) + 4*Power(t2,5) - 
                  Power(t1,2)*
                   (-14 - 8*t2 + 168*Power(t2,2) + 103*Power(t2,3) + 
                     Power(t2,4)) + 
                  t1*(-72 + 128*t2 + 423*Power(t2,2) - 
                     790*Power(t2,3) + 11*Power(t2,4)))) + 
            Power(s1,5)*(Power(t1,3)*
                (66 - 118*t2 - 33*Power(t2,2) + 8*Power(t2,3)) + 
               2*Power(-1 + t2,2)*
                (-3 - 3*t2 + Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,3)*(15 - 65*t2 + 98*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,2)*(37 + 121*t2 - 260*Power(t2,2) + 
                  58*Power(t2,3) - 7*Power(t2,4)) + 
               t1*(-38 + 226*t2 - 275*Power(t2,2) + 110*Power(t2,3) - 
                  24*Power(t2,4) + Power(t2,5)) + 
               Power(s2,2)*(-101 + 426*t2 - 400*Power(t2,2) + 
                  47*Power(t2,3) - 23*Power(t2,4) + 
                  t1*(20 + 49*t2 - 270*Power(t2,2) + 8*Power(t2,3))) + 
               s2*(84 - 252*t2 + 175*Power(t2,2) - 14*Power(t2,3) + 
                  6*Power(t2,4) + Power(t2,5) - 
                  2*Power(t1,2)*
                   (55 - 80*t2 - 90*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(75 - 579*t2 + 675*Power(t2,2) - 83*Power(t2,3) + 
                     14*Power(t2,4)))) + 
            Power(s1,3)*(-(Power(t1,3)*
                  (17 + 6*t2 - 46*Power(t2,2) + 32*Power(t2,3))) + 
               Power(t1,2)*(83 - 311*t2 + 297*Power(t2,2) - 
                  176*Power(t2,3) - 46*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (23 - 15*t2 + 2*Power(t2,2) + 61*Power(t2,3) + 
                  6*Power(t2,4) + 2*Power(t2,5)) - 
               t1*(75 - 210*t2 + 101*Power(t2,2) - 123*Power(t2,3) + 
                  155*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,3)*(-8 + 2*t2 - 6*Power(t2,2) - 
                  32*Power(t2,3) + 51*Power(t2,4) + 5*Power(t2,5)) + 
               Power(s2,2)*(20 + 9*t2 - 127*Power(t2,2) + 
                  300*Power(t2,3) - 351*Power(t2,4) - 4*Power(t2,5) - 
                  t1*(-32 + 102*t2 - 60*Power(t2,2) - 82*Power(t2,3) + 
                     104*Power(t2,4) + Power(t2,5))) + 
               s2*(75 - 86*t2 + 140*Power(t2,2) - 611*Power(t2,3) + 
                  510*Power(t2,4) - 20*Power(t2,5) - 8*Power(t2,6) + 
                  2*Power(t1,2)*
                   (5 + 50*t2 - 78*Power(t2,2) + 24*Power(t2,3) + 
                     14*Power(t2,4)) + 
                  t1*(-139 + 245*t2 - 8*Power(t2,2) - 132*Power(t2,3) + 
                     335*Power(t2,4) + 5*Power(t2,5)))) + 
            Power(s1,2)*(Power(t1,3)*
                (-4 + 13*t2 - 63*Power(t2,2) + 16*Power(t2,3)) - 
               2*Power(s2,3)*t2*
                (-16 - 6*t2 + 21*Power(t2,2) - 12*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*(48 - 84*t2 + 284*Power(t2,2) - 
                  254*Power(t2,3) + 68*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (-1 - 29*t2 - 13*Power(t2,2) + 10*Power(t2,3) + 
                  18*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(-84 + 130*t2 - 65*Power(t2,2) - 81*Power(t2,3) + 
                  78*Power(t2,4) + 16*Power(t2,5) + 6*Power(t2,6)) + 
               Power(s2,2)*(-10 - 98*t2 + 36*Power(t2,2) + 
                  257*Power(t2,3) - 196*Power(t2,4) + 67*Power(t2,5) + 
                  6*Power(t2,6) + 
                  t1*(8 - 113*t2 + 136*Power(t2,2) - 108*Power(t2,3) - 
                     20*Power(t2,4) + 11*Power(t2,5))) + 
               s2*(-6 - 128*t2 + 315*Power(t2,2) - 232*Power(t2,3) + 
                  268*Power(t2,4) - 235*Power(t2,5) + 14*Power(t2,6) + 
                  4*Power(t2,7) + 
                  Power(t1,2)*
                   (-32 + 75*t2 - 15*Power(t2,2) + 94*Power(t2,3) - 
                     22*Power(t2,4)) + 
                  t1*(65 + 196*t2 - 682*Power(t2,2) + 299*Power(t2,3) + 
                     39*Power(t2,4) - 35*Power(t2,5) - 6*Power(t2,6)))) \
- s1*(Power(t1,3)*(36 - 158*t2 + 125*Power(t2,2) - 24*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (40 - 10*t2 - 35*Power(t2,2) + 19*Power(t2,3)) + 
               Power(t1,2)*(13 + 205*t2 - 350*Power(t2,2) + 
                  178*Power(t2,3) - 45*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (76 - 55*t2 + 26*Power(t2,2) + 53*Power(t2,3) - 
                  14*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(-125 + 82*t2 + 227*Power(t2,2) - 252*Power(t2,3) + 
                  57*Power(t2,4) + 7*Power(t2,5) + 4*Power(t2,6)) + 
               Power(s2,2)*t2*
                (-28 - 80*t2 + 151*Power(t2,2) + 36*Power(t2,3) - 
                  94*Power(t2,4) + 16*Power(t2,5) + 
                  t1*(24 - 186*t2 + 138*Power(t2,2) - 20*Power(t2,3) - 
                     5*Power(t2,4))) + 
               s2*(-42 + 146*t2 - 151*Power(t2,2) + 30*Power(t2,3) + 
                  58*Power(t2,4) - 5*Power(t2,5) - 44*Power(t2,6) + 
                  8*Power(t2,7) + 
                  2*Power(t1,2)*
                   (-23 + 46*t2 + 72*Power(t2,2) - 85*Power(t2,3) + 
                     18*Power(t2,4)) + 
                  t1*(88 - 184*t2 + 131*Power(t2,2) - 175*Power(t2,3) + 
                     133*Power(t2,4) + 9*Power(t2,5) - 4*Power(t2,6))))) \
+ Power(s,5)*(15 + 27*t1 - 210*Power(t1,2) + 188*Power(t1,3) + 348*t2 - 
            217*s2*t2 - 1528*t1*t2 + 841*s2*t1*t2 + 
            1439*Power(t1,2)*t2 - 644*s2*Power(t1,2)*t2 - 
            210*Power(t1,3)*t2 - 1865*Power(t2,2) + 
            1483*s2*Power(t2,2) - 366*Power(s2,2)*Power(t2,2) + 
            2225*t1*Power(t2,2) - 921*s2*t1*Power(t2,2) + 
            140*Power(s2,2)*t1*Power(t2,2) - 
            1984*Power(t1,2)*Power(t2,2) + 
            945*s2*Power(t1,2)*Power(t2,2) + 
            13*Power(t1,3)*Power(t2,2) + 2895*Power(t2,3) - 
            2688*s2*Power(t2,3) + 446*Power(s2,2)*Power(t2,3) + 
            90*Power(s2,3)*Power(t2,3) - 232*t1*Power(t2,3) + 
            127*s2*t1*Power(t2,3) - 318*Power(s2,2)*t1*Power(t2,3) + 
            885*Power(t1,2)*Power(t2,3) - 
            328*s2*Power(t1,2)*Power(t2,3) + 
            10*Power(t1,3)*Power(t2,3) - 2577*Power(t2,4) + 
            846*s2*Power(t2,4) - 57*Power(s2,2)*Power(t2,4) - 
            59*Power(s2,3)*Power(t2,4) - 191*t1*Power(t2,4) + 
            139*s2*t1*Power(t2,4) + 133*Power(s2,2)*t1*Power(t2,4) - 
            108*Power(t1,2)*Power(t2,4) + 
            30*s2*Power(t1,2)*Power(t2,4) + 1401*Power(t2,5) + 
            271*s2*Power(t2,5) - 100*Power(s2,2)*Power(t2,5) + 
            9*Power(s2,3)*Power(t2,5) - 184*t1*Power(t2,5) - 
            34*s2*t1*Power(t2,5) - 15*Power(s2,2)*t1*Power(t2,5) - 
            236*Power(t2,6) - 64*s2*Power(t2,6) + 
            26*Power(s2,2)*Power(t2,6) + 60*t1*Power(t2,6) - 
            4*s2*t1*Power(t2,6) - 16*Power(t2,7) + 6*s2*Power(t2,7) + 
            Power(s1,7)*(70*Power(s2,3) - 330*Power(t1,3) + 
               42*Power(t1,2)*(-9 + 5*t2) - 
               7*Power(s2,2)*(5 + 54*t1 + 14*t2) + 
               t2*(-21 + 35*t2 - 16*Power(t2,2)) + 
               7*s2*(5 + 44*t1 + 90*Power(t1,2) - 6*t2 + 
                  8*Power(t2,2)) - 2*t1*(63 - 98*t2 + 60*Power(t2,2))) - 
            Power(s1,6)*(5 - 187*t2 + 371*Power(t2,2) - 
               154*Power(t2,3) - 56*Power(t2,4) - 
               210*Power(t1,3)*(-2 + 3*t2) + 
               5*Power(s2,3)*(-16 + 35*t2) + 
               Power(s2,2)*(65 + t1*(350 - 686*t2) - 253*t2 - 
                  210*Power(t2,2)) + 
               7*Power(t1,2)*(55 - 194*t2 + 54*Power(t2,2)) - 
               2*t1*(79 - 21*t2 - 49*Power(t2,2) + 98*Power(t2,3)) + 
               s2*(51 - 12*t2 + 65*Power(t2,2) + 105*Power(t2,3) + 
                  14*Power(t1,2)*(-44 + 75*t2) + 
                  2*t1*(-121 + 511*t2 + 105*Power(t2,2)))) + 
            Power(s1,5)*(-34 + 61*t2 - 451*Power(t2,2) + 
               1026*Power(t2,3) - 548*Power(t2,4) - 60*Power(t2,5) - 
               7*Power(t1,3)*(49 - 156*t2 + 54*Power(t2,2)) + 
               Power(s2,3)*(91 - 311*t2 + 125*Power(t2,2)) + 
               Power(t1,2)*(-695 + 2741*t2 - 1883*Power(t2,2) + 
                  210*Power(t2,3)) + 
               t1*(-121 + 178*t2 + 171*Power(t2,2) - 105*Power(t2,3) - 
                  84*Power(t2,4)) - 
               Power(s2,2)*(205 - 656*t2 + 66*Power(t2,2) + 
                  183*Power(t2,3) + 
                  2*t1*(179 - 629*t2 + 210*Power(t2,2))) + 
               s2*(-31 + 463*t2 - 929*Power(t2,2) + 422*Power(t2,3) + 
                  24*Power(t2,4) + 
                  Power(t1,2)*(468 - 1687*t2 + 462*Power(t2,2)) + 
                  2*t1*(439 - 1477*t2 + 555*Power(t2,2) + 
                     203*Power(t2,3)))) + 
            Power(s1,4)*(-4 + 230*t2 - 833*Power(t2,2) + 
               1163*Power(t2,3) - 1158*Power(t2,4) + 485*Power(t2,5) + 
               20*Power(t2,6) + 
               Power(s2,3)*(88 - 409*t2 + 481*Power(t2,2) - 
                  40*Power(t2,3)) + 
               Power(t1,3)*(-176 + 622*t2 - 651*Power(t2,2) + 
                  70*Power(t2,3)) + 
               Power(t1,2)*(-933 + 3223*t2 - 3828*Power(t2,2) + 
                  905*Power(t2,3) - 35*Power(t2,4)) + 
               t1*(-334 + 1211*t2 - 1676*Power(t2,2) - 
                  58*Power(t2,3) + 195*Power(t2,4) + 5*Power(t2,5)) + 
               Power(s2,2)*(-275 + 1228*t2 - 1344*Power(t2,2) - 
                  308*Power(t2,3) + 40*Power(t2,4) + 
                  t1*(-308 + 1221*t2 - 1359*Power(t2,2) + 
                     160*Power(t2,3))) + 
               s2*(-43 + 150*t2 - 536*Power(t2,2) + 1740*Power(t2,3) - 
                  640*Power(t2,4) + 30*Power(t2,5) + 
                  Power(t1,2)*(296 - 1032*t2 + 1011*Power(t2,2)) + 
                  t1*(1249 - 4649*t2 + 5203*Power(t2,2) - 
                     265*Power(t2,3) - 230*Power(t2,4)))) + 
            Power(s1,3)*(4 + 300*t2 - 1444*Power(t2,2) + 
               2541*Power(t2,3) - 1709*Power(t2,4) + 558*Power(t2,5) - 
               152*Power(t2,6) + 
               4*Power(t1,3)*
                (-29 + 93*t2 - 104*Power(t2,2) + 25*Power(t2,3)) + 
               Power(t1,2)*(-1071 + 3652*t2 - 4354*Power(t2,2) + 
                  1860*Power(t2,3) - 125*Power(t2,4)) + 
               Power(s2,3)*(51 - 377*t2 + 686*Power(t2,2) - 
                  365*Power(t2,3) + 23*Power(t2,4)) + 
               t1*(-138 + 1289*t2 - 3101*Power(t2,2) + 
                  2080*Power(t2,3) + 201*Power(t2,4) - 121*Power(t2,5)) \
+ Power(s2,2)*(-230 + 1474*t2 - 2175*Power(t2,2) + 769*Power(t2,3) + 
                  194*Power(t2,4) + 35*Power(t2,5) + 
                  t1*(-198 + 910*t2 - 1387*Power(t2,2) + 
                     680*Power(t2,3) - 65*Power(t2,4))) - 
               s2*(2 + 292*t2 - 364*Power(t2,2) - 553*Power(t2,3) + 
                  1225*Power(t2,4) - 391*Power(t2,5) + 8*Power(t2,6) + 
                  2*Power(t1,2)*
                   (-124 + 419*t2 - 459*Power(t2,2) + 84*Power(t2,3) + 
                     10*Power(t2,4)) + 
                  t1*(-1014 + 4481*t2 - 6534*Power(t2,2) + 
                     3118*Power(t2,3) + 104*Power(t2,4) - 35*Power(t2,5)\
))) - Power(s1,2)*(184 - 909*t2 + 2293*Power(t2,2) - 3752*Power(t2,3) + 
               3329*Power(t2,4) - 1018*Power(t2,5) + 56*Power(t2,6) - 
               14*Power(t2,7) + 
               Power(t1,3)*(64 - 222*t2 + 234*Power(t2,2) - 
                  48*Power(t2,3)) + 
               Power(s2,3)*t2*
                (148 - 513*t2 + 508*Power(t2,2) - 133*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(t1,2)*(984 - 3532*t2 + 4274*Power(t2,2) - 
                  1866*Power(t2,3) + 217*Power(t2,4)) + 
               t1*(-413 - 121*t2 + 2595*Power(t2,2) - 
                  2890*Power(t2,3) + 424*Power(t2,4) + 
                  267*Power(t2,5) - 24*Power(t2,6)) + 
               Power(s2,2)*(10 - 1145*t2 + 2748*Power(t2,2) - 
                  1800*Power(t2,3) + 75*Power(t2,4) + 20*Power(t2,5) + 
                  12*Power(t2,6) + 
                  t1*(88 - 441*t2 + 709*Power(t2,2) - 
                     454*Power(t2,3) + 139*Power(t2,4) - 11*Power(t2,5)\
)) + s2*(2 + 1277*t2 - 2594*Power(t2,2) + 1219*Power(t2,3) + 
                  444*Power(t2,4) - 365*Power(t2,5) + 98*Power(t2,6) - 
                  2*Power(t1,2)*
                   (94 - 353*t2 + 378*Power(t2,2) - 92*Power(t2,3) + 
                     3*Power(t2,4)) - 
                  t1*(447 - 2816*t2 + 5000*Power(t2,2) - 
                     3242*Power(t2,3) + 557*Power(t2,4) + 68*Power(t2,5)\
))) - s1*(76 - 1246*t2 + 2914*Power(t2,2) - 4221*Power(t2,3) + 
               3691*Power(t2,4) - 1554*Power(t2,5) + 136*Power(t2,6) + 
               14*Power(t2,7) + 
               Power(t1,3)*(19 - 88*t2 + 126*Power(t2,2) - 
                  28*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (-27 + 177*t2 - 123*Power(t2,2) + 14*Power(t2,3)) + 
               Power(t1,2)*(368 - 2405*t2 + 3459*Power(t2,2) - 
                  1570*Power(t2,3) + 187*Power(t2,4)) + 
               t1*(-441 + 1425*t2 + 358*Power(t2,2) - 
                  1949*Power(t2,3) + 497*Power(t2,4) + 
                  265*Power(t2,5) - 60*Power(t2,6)) + 
               Power(s2,2)*t2*
                (-178 + 1295*t2 - 1628*Power(t2,2) + 346*Power(t2,3) + 
                  71*Power(t2,4) - 2*Power(t2,5) + 
                  t1*(-156 + 137*t2 + 16*Power(t2,2) + 9*Power(t2,3) - 
                     4*Power(t2,4))) + 
               s2*(-50 + 609*t2 - 3393*Power(t2,2) + 2999*Power(t2,3) - 
                  83*Power(t2,4) - 299*Power(t2,5) + 54*Power(t2,6) - 
                  10*Power(t2,7) + 
                  Power(t1,2)*
                   (-146 + 571*t2 - 708*Power(t2,2) + 216*Power(t2,3) - 
                     16*Power(t2,4)) + 
                  t1*(218 + 855*t2 - 2308*Power(t2,2) + 
                     1844*Power(t2,3) - 518*Power(t2,4) + 
                     5*Power(t2,5) + 12*Power(t2,6))))) + 
         Power(s,3)*(-37 + 171*t1 - 243*Power(t1,2) + 109*Power(t1,3) + 
            456*t2 - 191*s2*t2 - 1285*t1*t2 + 434*s2*t1*t2 + 
            1113*Power(t1,2)*t2 - 239*s2*Power(t1,2)*t2 - 
            291*Power(t1,3)*t2 - 1292*Power(t2,2) + 767*s2*Power(t2,2) - 
            82*Power(s2,2)*Power(t2,2) + 2310*t1*Power(t2,2) - 
            1162*s2*t1*Power(t2,2) + 56*Power(s2,2)*t1*Power(t2,2) - 
            1510*Power(t1,2)*Power(t2,2) + 
            627*s2*Power(t1,2)*Power(t2,2) + 
            199*Power(t1,3)*Power(t2,2) + 1714*Power(t2,3) - 
            1169*s2*Power(t2,3) - 2*Power(s2,2)*Power(t2,3) + 
            52*Power(s2,3)*Power(t2,3) - 1433*t1*Power(t2,3) + 
            1145*s2*t1*Power(t2,3) - 246*Power(s2,2)*t1*Power(t2,3) + 
            747*Power(t1,2)*Power(t2,3) - 
            414*s2*Power(t1,2)*Power(t2,3) - 
            32*Power(t1,3)*Power(t2,3) - 1168*Power(t2,4) + 
            519*s2*Power(t2,4) + 152*Power(s2,2)*Power(t2,4) - 
            50*Power(s2,3)*Power(t2,4) + 241*t1*Power(t2,4) - 
            428*s2*t1*Power(t2,4) + 185*Power(s2,2)*t1*Power(t2,4) - 
            120*Power(t1,2)*Power(t2,4) + 
            64*s2*Power(t1,2)*Power(t2,4) + 355*Power(t2,5) + 
            234*s2*Power(t2,5) - 122*Power(s2,2)*Power(t2,5) + 
            10*Power(s2,3)*Power(t2,5) - 6*t1*Power(t2,5) + 
            46*s2*t1*Power(t2,5) - 30*Power(s2,2)*t1*Power(t2,5) - 
            28*Power(t2,6) - 184*s2*Power(t2,6) + 
            28*Power(s2,2)*Power(t2,6) + 16*t1*Power(t2,6) + 
            4*s2*t1*Power(t2,6) + 10*s2*Power(t2,7) + 
            Power(s1,9)*(28*Power(s2,3) - 55*Power(t1,3) + 
               (-1 + t2)*t2 + Power(s2,2)*(-21 - 108*t1 + 4*t2) + 
               9*Power(t1,2)*(-7 + 5*t2) + 
               t1*(-22 + 31*t2 - 10*Power(t2,2)) + 
               s2*(7 + 135*Power(t1,2) + t1*(80 - 45*t2) - 2*t2 - 
                  4*Power(t2,2))) + 
            Power(s1,8)*(-13 + 64*t2 - 83*Power(t2,2) + 
               32*Power(t2,3) + 45*Power(t1,3)*(1 + 3*t2) - 
               Power(s2,3)*(18 + 77*t2) + 
               Power(t1,2)*(14 + 212*t2 - 108*Power(t2,2)) + 
               t1*(46 + 3*t2 - 65*Power(t2,2) + 25*Power(t2,3)) + 
               Power(s2,2)*(-29 + 165*t2 - 30*Power(t2,2) + 
                  t1*(98 + 260*t2)) - 
               s2*(18 + 14*t2 - 12*Power(t2,2) - 11*Power(t2,3) + 
                  Power(t1,2)*(128 + 315*t2) + 
                  t1*(6 + 317*t2 - 99*Power(t2,2)))) + 
            Power(s1,7)*(29 - 107*t2 - 7*Power(t2,2) + 
               197*Power(t2,3) - 112*Power(t2,4) + 
               Power(t1,3)*(-17 + 24*t2 - 108*Power(t2,2)) + 
               Power(s2,3)*(20 - 18*t2 + 55*Power(t2,2)) + 
               Power(t1,2)*(-217 + 329*t2 - 353*Power(t2,2) + 
                  84*Power(t2,3)) + 
               t1*(-143 + 156*t2 - 85*Power(t2,2) + 79*Power(t2,3) - 
                  20*Power(t2,4)) + 
               Power(s2,2)*(-116 + 159*t2 - 232*Power(t2,2) + 
                  57*Power(t2,3) + t1*(-89 + 82*t2 - 196*Power(t2,2))) \
+ s2*(31 + 99*t2 - 124*Power(t2,2) + 21*Power(t2,3) - 14*Power(t2,4) + 
                  19*Power(t1,2)*(4 - 3*t2 + 12*Power(t2,2)) + 
                  t1*(350 - 449*t2 + 436*Power(t2,2) - 48*Power(t2,3)))) \
+ Power(s1,6)*(-22 + 117*t2 - 12*Power(t2,2) - 59*Power(t2,3) - 
               155*Power(t2,4) + 131*Power(t2,5) + 
               Power(s2,3)*(14 - 72*t2 + 190*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(t1,3)*(126 - 386*t2 - 21*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,2)*(152 - 13*t2 - 562*Power(t2,2) + 
                  191*Power(t2,3) - 21*Power(t2,4)) + 
               t1*(-20 + 583*t2 - 840*Power(t2,2) + 230*Power(t2,3) - 
                  33*Power(t2,4) + 5*Power(t2,5)) + 
               Power(s2,2)*(-110 + 738*t2 - 941*Power(t2,2) + 
                  101*Power(t2,3) - 44*Power(t2,4) + 
                  t1*(48 - 96*t2 - 529*Power(t2,2) + 48*Power(t2,3))) + 
               s2*(145 - 773*t2 + 585*Power(t2,2) + 201*Power(t2,3) - 
                  94*Power(t2,4) + 11*Power(t2,5) + 
                  Power(t1,2)*
                   (-196 + 618*t2 + 255*Power(t2,2) - 42*Power(t2,3)) + 
                  t1*(6 - 911*t2 + 1651*Power(t2,2) - 217*Power(t2,3) - 
                     20*Power(t2,4)))) - 
            Power(s1,5)*(-33 + 48*t2 + 205*Power(t2,2) - 
               273*Power(t2,3) - 14*Power(t2,4) + 5*Power(t2,5) + 
               62*Power(t2,6) + 
               3*Power(t1,3)*
                (27 - 48*t2 - 98*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(-16 + 129*t2 - 471*Power(t2,2) - 
                  422*Power(t2,3) + 17*Power(t2,4)) + 
               Power(s2,3)*(-4 - 14*t2 - 8*Power(t2,2) + 
                  236*Power(t2,3) + 27*Power(t2,4)) + 
               t1*(-105 + 319*t2 + 278*Power(t2,2) - 935*Power(t2,3) + 
                  123*Power(t2,4) + 9*Power(t2,5)) + 
               Power(s2,2)*(-61 + 232*t2 + 324*Power(t2,2) - 
                  1247*Power(t2,3) + 3*Power(t2,4) - 17*Power(t2,5) + 
                  t1*(8 - 60*t2 - 163*Power(t2,2) - 550*Power(t2,3) + 
                     10*Power(t2,4))) + 
               s2*(80 - 158*t2 - 1009*Power(t2,2) + 1377*Power(t2,3) + 
                  124*Power(t2,4) - 107*Power(t2,5) + 4*Power(t2,6) + 
                  Power(t1,2)*
                   (-112 + 275*t2 + 510*Power(t2,2) + 
                     184*Power(t2,3) + 6*Power(t2,4)) + 
                  t1*(214 - 537*t2 + 86*Power(t2,2) + 
                     1812*Power(t2,3) - 32*Power(t2,4) - 14*Power(t2,5))\
)) + Power(s1,4)*(-84 - 16*t2 + 123*Power(t2,2) + 230*Power(t2,3) - 
               336*Power(t2,4) + 11*Power(t2,5) + 62*Power(t2,6) + 
               10*Power(t2,7) - 
               Power(t1,3)*(6 - 28*t2 + 15*Power(t2,2) + 
                  72*Power(t2,3)) + 
               Power(t1,2)*(95 - 361*t2 + 394*Power(t2,2) - 
                  563*Power(t2,3) - 82*Power(t2,4)) + 
               Power(s2,3)*(16 - 117*t2 + 132*Power(t2,2) - 
                  18*Power(t2,3) + 88*Power(t2,4) + 6*Power(t2,5)) + 
               t1*(87 - 172*t2 + 221*Power(t2,2) - 184*Power(t2,3) - 
                  411*Power(t2,4) - 24*Power(t2,5) + 4*Power(t2,6)) + 
               Power(s2,2)*(-101 + 617*t2 - 710*Power(t2,2) + 
                  140*Power(t2,3) - 568*Power(t2,4) - 22*Power(t2,5) - 
                  4*Power(t2,6) + 
                  t1*(8 - 88*t2 + 226*Power(t2,2) - 142*Power(t2,3) - 
                     197*Power(t2,4) + 6*Power(t2,5))) + 
               s2*(152 - 713*t2 + 563*Power(t2,2) - 714*Power(t2,3) + 
                  1255*Power(t2,4) - 6*Power(t2,5) - 58*Power(t2,6) + 
                  Power(t1,2)*
                   (-16 + 62*t2 - 123*Power(t2,2) + 166*Power(t2,3) + 
                     56*Power(t2,4)) + 
                  t1*(-106 + 471*t2 - 493*Power(t2,2) + 
                     595*Power(t2,3) + 676*Power(t2,4) + 22*Power(t2,5))\
)) + s1*(-157 + 375*t2 - 410*Power(t2,2) + 125*Power(t2,3) + 
               278*Power(t2,4) - 389*Power(t2,5) + 180*Power(t2,6) - 
               2*Power(t2,7) + 
               2*Power(t1,3)*
                (-92 + 232*t2 - 129*Power(t2,2) + 18*Power(t2,3)) - 
               Power(s2,3)*Power(t2,2)*
                (90 + 36*t2 - 125*Power(t2,2) + 46*Power(t2,3)) + 
               Power(t1,2)*(109 - 840*t2 + 1069*Power(t2,2) - 
                  450*Power(t2,3) + 75*Power(t2,4)) + 
               t1*(239 + 558*t2 - 2218*Power(t2,2) + 
                  1757*Power(t2,3) - 321*Power(t2,4) - 9*Power(t2,5) - 
                  36*Power(t2,6)) + 
               Power(s2,2)*t2*
                (84 + 152*t2 + 113*Power(t2,2) - 565*Power(t2,3) + 
                  287*Power(t2,4) - 20*Power(t2,5) + 
                  t1*(-44 + 235*t2 - 2*Power(t2,2) - 80*Power(t2,3) + 
                     20*Power(t2,4))) + 
               s2*(89 - 182*t2 + 563*Power(t2,2) - 433*Power(t2,3) + 
                  2*Power(t2,4) - 81*Power(t2,5) + 78*Power(t2,6) - 
                  6*Power(t2,7) + 
                  Power(t1,2)*
                   (109 + 123*t2 - 774*Power(t2,2) + 472*Power(t2,3) - 
                     70*Power(t2,4)) + 
                  2*t1*(-101 - 176*t2 + 539*Power(t2,2) - 
                     310*Power(t2,3) + 52*Power(t2,4) - 
                     13*Power(t2,5) + 2*Power(t2,6)))) + 
            Power(s1,2)*(-24 + 279*t2 - 800*Power(t2,2) + 
               699*Power(t2,3) + 31*Power(t2,4) - 53*Power(t2,5) - 
               146*Power(t2,6) + 14*Power(t2,7) + 
               3*Power(t1,3)*
                (26 - 42*t2 - Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,3)*t2*
                (38 + 158*t2 - 209*Power(t2,2) + 94*Power(t2,3) - 
                  28*Power(t2,4)) + 
               Power(t1,2)*(-82 + 329*t2 + 90*Power(t2,2) - 
                  151*Power(t2,3) + 31*Power(t2,4)) + 
               t1*(-236 + 407*t2 + 358*Power(t2,2) - 
                  1006*Power(t2,3) + 395*Power(t2,4) + 9*Power(t2,5) - 
                  4*Power(t2,6)) + 
               Power(s2,2)*(-20 + 68*t2 - 1061*Power(t2,2) + 
                  1601*Power(t2,3) - 736*Power(t2,4) + 
                  156*Power(t2,5) - 16*Power(t2,6) + 
                  t1*(6 - 140*t2 + 103*Power(t2,2) - 76*Power(t2,3) + 
                     28*Power(t2,4) - 8*Power(t2,5))) + 
               s2*(-59 - 921*t2 + 1889*Power(t2,2) - 1873*Power(t2,3) + 
                  1348*Power(t2,4) - 395*Power(t2,5) + 
                  106*Power(t2,6) - 18*Power(t2,7) + 
                  Power(t1,2)*
                   (-124 + 34*t2 + 201*Power(t2,2) - 30*Power(t2,3) - 
                     8*Power(t2,4)) + 
                  t1*(362 + 259*t2 - 1135*Power(t2,2) + 
                     461*Power(t2,3) - 196*Power(t2,4) + 
                     28*Power(t2,5) + 12*Power(t2,6)))) + 
            Power(s1,3)*(-45 + 481*t2 - 515*Power(t2,2) - 
               11*Power(t2,3) - 152*Power(t2,4) + 270*Power(t2,5) - 
               6*Power(t2,6) - 22*Power(t2,7) + 
               Power(t1,3)*(-15 + 8*t2 - 88*Power(t2,2) + 
                  40*Power(t2,3)) + 
               Power(s2,3)*t2*
                (-88 + 187*t2 - 208*Power(t2,2) + 90*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(t1,2)*(219 - 685*t2 + 509*Power(t2,2) - 
                  280*Power(t2,3) + 134*Power(t2,4)) + 
               t1*(-227 + 38*t2 + 607*Power(t2,2) - 403*Power(t2,3) + 
                  272*Power(t2,4) + 34*Power(t2,5) + 20*Power(t2,6)) + 
               Power(s2,2)*(-112 + 637*t2 - 1252*Power(t2,2) + 
                  1223*Power(t2,3) - 476*Power(t2,4) + 
                  132*Power(t2,5) + 12*Power(t2,6) + 
                  t1*(45 - 34*t2 - 58*Power(t2,2) - 132*Power(t2,3) + 
                     74*Power(t2,4) + 12*Power(t2,5))) + 
               s2*(437 - 981*t2 + 1780*Power(t2,2) - 1707*Power(t2,3) + 
                  628*Power(t2,4) - 574*Power(t2,5) + 62*Power(t2,6) + 
                  14*Power(t2,7) + 
                  Power(t1,2)*
                   (32 + 49*t2 + 96*Power(t2,2) + 32*Power(t2,3) - 
                     36*Power(t2,4)) - 
                  t1*(270 - 373*t2 + 388*Power(t2,2) - 496*Power(t2,3) + 
                     168*Power(t2,4) + 84*Power(t2,5) + 20*Power(t2,6))))\
) + Power(s,4)*(24 - 183*t1 + 354*Power(t1,2) - 199*Power(t1,3) - 
            573*t2 + 295*s2*t2 + 1848*t1*t2 - 801*s2*t1*t2 - 
            1655*Power(t1,2)*t2 + 496*s2*Power(t1,2)*t2 + 
            396*Power(t1,3)*t2 + 1988*Power(t2,2) - 1348*s2*Power(t2,2) + 
            221*Power(s2,2)*Power(t2,2) - 3032*t1*Power(t2,2) + 
            1526*s2*t1*Power(t2,2) - 112*Power(s2,2)*t1*Power(t2,2) + 
            2171*Power(t1,2)*Power(t2,2) - 
            987*s2*Power(t1,2)*Power(t2,2) - 
            197*Power(t1,3)*Power(t2,2) - 2950*Power(t2,3) + 
            2372*s2*Power(t2,3) - 168*Power(s2,2)*Power(t2,3) - 
            90*Power(s2,3)*Power(t2,3) + 1210*t1*Power(t2,3) - 
            1051*s2*t1*Power(t2,3) + 355*Power(s2,2)*t1*Power(t2,3) - 
            1003*Power(t1,2)*Power(t2,3) + 
            497*s2*Power(t1,2)*Power(t2,3) + 24*Power(t1,3)*Power(t2,3) + 
            2257*Power(t2,4) - 986*s2*Power(t2,4) - 
            96*Power(s2,2)*Power(t2,4) + 75*Power(s2,3)*Power(t2,4) + 
            62*t1*Power(t2,4) + 237*s2*t1*Power(t2,4) - 
            205*Power(s2,2)*t1*Power(t2,4) + 
            142*Power(t1,2)*Power(t2,4) - 65*s2*Power(t1,2)*Power(t2,4) - 
            986*Power(t2,5) - 406*s2*Power(t2,5) + 
            143*Power(s2,2)*Power(t2,5) - 15*Power(s2,3)*Power(t2,5) + 
            66*t1*Power(t2,5) - s2*t1*Power(t2,5) + 
            30*Power(s2,2)*t1*Power(t2,5) + 218*Power(t2,6) + 
            212*s2*Power(t2,6) - 40*Power(s2,2)*Power(t2,6) - 
            56*t1*Power(t2,6) + 12*Power(t2,7) - 10*s2*Power(t2,7) + 
            Power(s1,8)*(-56*Power(s2,3) + 165*Power(t1,3) + 
               7*Power(s2,2)*(5 + 36*t1 + 4*t2) - 
               24*Power(t1,2)*(-8 + 5*t2) + 
               t2*(7 - 9*t2 + 2*Power(t2,2)) - 
               s2*(21 + 360*Power(t1,2) + t1*(196 - 60*t2) - 14*t2 + 
                  4*Power(t2,2)) + t1*(70 - 104*t2 + 45*Power(t2,2))) + 
            Power(s1,7)*(19 - 159*t2 + 250*Power(t2,2) - 
               103*Power(t2,3) - 8*Power(t2,4) - 
               60*Power(t1,3)*(-1 + 6*t2) + Power(s2,3)*(-25 + 147*t2) + 
               Power(s2,2)*(65 + t1*(42 - 532*t2) - 300*t2 - 
                  42*Power(t2,2)) + 
               Power(t1,2)*(95 - 700*t2 + 252*Power(t2,2)) + 
               t1*(-113 + 11*t2 + 124*Power(t2,2) - 92*Power(t2,3)) + 
               s2*(44 - 3*t2 + 22*Power(t2,2) + 7*Power(t2,3) + 
                  8*Power(t1,2)*(-7 + 90*t2) + 
                  t1*(-87 + 766*t2 - 48*Power(t2,2)))) + 
            Power(s1,6)*(7 + 51*t2 + 226*Power(t2,2) - 657*Power(t2,3) + 
               359*Power(t2,4) + 10*Power(t2,5) + 
               Power(s2,3)*(-45 + 175*t2 - 106*Power(t2,2)) + 
               21*Power(t1,3)*(1 - 18*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(460 - 1284*t2 + 1099*Power(t2,2) - 
                  168*Power(t2,3)) + 
               t1*(235 - 314*t2 + 135*Power(t2,2) - 70*Power(t2,3) + 
                  56*Power(t2,4)) + 
               Power(s2,2)*(232 - 531*t2 + 359*Power(t2,2) + 
                  13*Power(t2,3) + 2*t1*(65 - 323*t2 + 182*Power(t2,2))) \
- s2*(33 + 262*t2 - 472*Power(t2,2) + 230*Power(t2,3) - 17*Power(t2,4) + 
                  Power(t1,2)*(64 - 721*t2 + 420*Power(t2,2)) + 
                  t1*(735 - 1694*t2 + 1025*Power(t2,2) + 112*Power(t2,3))\
)) + Power(s1,5)*(6 - 218*t2 + 382*Power(t2,2) - 426*Power(t2,3) + 
               669*Power(t2,4) - 374*Power(t2,5) - 4*Power(t2,6) + 
               Power(t1,3)*(-109 + 286*t2 + 273*Power(t2,2) - 
                  56*Power(t2,3)) + 
               Power(s2,3)*(-58 + 246*t2 - 393*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(393 - 1138*t2 + 2061*Power(t2,2) - 
                  600*Power(t2,3) + 35*Power(t2,4)) - 
               t1*(-273 + 1293*t2 - 1816*Power(t2,2) + 
                  401*Power(t2,3) + 24*Power(t2,4) + 9*Power(t2,5)) + 
               Power(s2,2)*(297 - 1209*t2 + 1634*Power(t2,2) - 
                  82*Power(t2,3) + 19*Power(t2,4) + 
                  t1*(97 - 397*t2 + 1128*Power(t2,2) - 120*Power(t2,3))) \
+ s2*(-107 + 688*t2 - 468*Power(t2,2) - 927*Power(t2,3) + 
                  436*Power(t2,4) - 32*Power(t2,5) + 
                  Power(t1,2)*
                   (80 - 284*t2 - 732*Power(t2,2) + 42*Power(t2,3)) + 
                  t1*(-789 + 2716*t2 - 3907*Power(t2,2) + 
                     445*Power(t2,3) + 134*Power(t2,4)))) + 
            Power(s1,4)*(-18 - 127*t2 + 821*Power(t2,2) - 
               1228*Power(t2,3) + 599*Power(t2,4) - 257*Power(t2,5) + 
               148*Power(t2,6) - 
               5*Power(t1,3)*
                (4 - 24*t2 + 29*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,3)*(-57 + 273*t2 - 458*Power(t2,2) + 
                  344*Power(t2,3) + 8*Power(t2,4)) + 
               Power(t1,2)*(434 - 1359*t2 + 1497*Power(t2,2) - 
                  1243*Power(t2,3) + 90*Power(t2,4)) + 
               t1*(66 - 1056*t2 + 2429*Power(t2,2) - 2306*Power(t2,3) + 
                  171*Power(t2,4) + 54*Power(t2,5)) + 
               Power(s2,2)*(287 - 1095*t2 + 1687*Power(t2,2) - 
                  1480*Power(t2,3) - 40*Power(t2,4) - 30*Power(t2,5) + 
                  t1*(71 - 354*t2 + 798*Power(t2,2) - 789*Power(t2,3) + 
                     50*Power(t2,4))) + 
               s2*(-102 + 572*t2 - 1178*Power(t2,2) + 1016*Power(t2,3) + 
                  797*Power(t2,4) - 351*Power(t2,5) + 12*Power(t2,6) + 
                  Power(t1,2)*
                   (-40 + 86*t2 - 147*Power(t2,2) + 265*Power(t2,3) + 
                     15*Power(t2,4)) + 
                  t1*(-538 + 2133*t2 - 3444*Power(t2,2) + 
                     3093*Power(t2,3) + Power(t2,4) - 35*Power(t2,5)))) + 
            Power(s1,3)*(92 - 457*t2 + 972*Power(t2,2) - 
               1881*Power(t2,3) + 1727*Power(t2,4) - 374*Power(t2,5) - 
               20*Power(t2,6) - 18*Power(t2,7) + 
               2*Power(t1,3)*
                (-7 + 26*t2 - 67*Power(t2,2) + 24*Power(t2,3)) + 
               Power(t1,2)*(377 - 1340*t2 + 1386*Power(t2,2) - 
                  660*Power(t2,3) + 178*Power(t2,4)) + 
               Power(s2,3)*(-31 + 249*t2 - 578*Power(t2,2) + 
                  494*Power(t2,3) - 130*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(124 - 891*t2 + 2306*Power(t2,2) - 2278*Power(t2,3) + 
                  879*Power(t2,4) + 114*Power(t2,5) - 16*Power(t2,6)) + 
               Power(s2,2)*(296 - 1498*t2 + 2644*Power(t2,2) - 
                  1872*Power(t2,3) + 503*Power(t2,4) + 16*Power(t2,5) + 
                  12*Power(t2,6) + 
                  t1*(21 - 140*t2 + 274*Power(t2,2) - 428*Power(t2,3) + 
                     221*Power(t2,4) - 14*Power(t2,5))) - 
               s2*(364 - 1567*t2 + 2556*Power(t2,2) - 1721*Power(t2,3) + 
                  634*Power(t2,4) + 290*Power(t2,5) - 130*Power(t2,6) + 
                  4*Power(t1,2)*
                   (10 - 27*t2 + 3*Power(t2,2) - 26*Power(t2,3) + 
                     11*Power(t2,4)) + 
                  t1*(397 - 1600*t2 + 2262*Power(t2,2) - 
                     1950*Power(t2,3) + 784*Power(t2,4) + 70*Power(t2,5))\
)) + s1*(205 - 1256*t2 + 2588*Power(t2,2) - 2966*Power(t2,3) + 
               1966*Power(t2,4) - 598*Power(t2,5) + 68*Power(t2,6) + 
               14*Power(t2,7) + 
               Power(t1,3)*(111 - 138*t2 + 5*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (71 + 113*t2 - 168*Power(t2,2) + 40*Power(t2,3)) + 
               Power(t1,2)*(255 - 974*t2 + 1509*Power(t2,2) - 
                  796*Power(t2,3) + 115*Power(t2,4)) + 
               t1*(-580 + 853*t2 + 930*Power(t2,2) - 1701*Power(t2,3) + 
                  593*Power(t2,4) + 39*Power(t2,5) - 40*Power(t2,6)) - 
               Power(s2,2)*t2*
                (147 - 456*t2 + 1106*Power(t2,2) - 776*Power(t2,3) + 
                  74*Power(t2,4) + 4*Power(t2,5) + 
                  t1*(11 + 122*t2 - 76*Power(t2,2) + 13*Power(t2,3) + 
                     2*Power(t2,4))) + 
               s2*(-95 + 550*t2 - 2814*Power(t2,2) + 2839*Power(t2,3) - 
                  428*Power(t2,4) - 356*Power(t2,5) + 66*Power(t2,6) - 
                  4*Power(t2,7) + 
                  Power(t1,2)*
                   (-144 + 56*t2 + 96*Power(t2,2) + 22*Power(t2,3) - 
                     12*Power(t2,4)) + 
                  t1*(257 + 614*t2 - 1263*Power(t2,2) + 
                     597*Power(t2,3) - 158*Power(t2,4) + 6*Power(t2,5) + 
                     8*Power(t2,6)))) + 
            Power(s1,2)*(289 - 1060*t2 + 2318*Power(t2,2) - 
               2463*Power(t2,3) + 1759*Power(t2,4) - 957*Power(t2,5) + 
               70*Power(t2,6) + 24*Power(t2,7) + 
               Power(t1,3)*(-15 + 22*t2 - 54*Power(t2,2) + 
                  16*Power(t2,3)) + 
               5*Power(t1,2)*(128 - 334*t2 + 309*Power(t2,2) - 
                  130*Power(t2,3) + 16*Power(t2,4)) + 
               Power(s2,3)*t2*
                (46 - 360*t2 + 510*Power(t2,2) - 249*Power(t2,3) + 
                  21*Power(t2,4)) + 
               t1*(-308 - 270*t2 + 1551*Power(t2,2) - 1658*Power(t2,3) + 
                  727*Power(t2,4) + 56*Power(t2,5) - 48*Power(t2,6)) + 
               Power(s2,2)*(20 - 832*t2 + 2513*Power(t2,2) - 
                  2505*Power(t2,3) + 966*Power(t2,4) - 71*Power(t2,5) + 
                  t1*(27 + 32*t2 - 26*Power(t2,2) - 118*Power(t2,3) + 
                     75*Power(t2,4) - 14*Power(t2,5))) + 
               s2*(-10 + 2003*t2 - 4062*Power(t2,2) + 2994*Power(t2,3) - 
                  850*Power(t2,4) + 75*Power(t2,5) + 28*Power(t2,6) - 
                  18*Power(t2,7) + 
                  Power(t1,2)*
                   (-16 + 145*t2 - 102*Power(t2,2) + 94*Power(t2,3) - 
                     22*Power(t2,4)) + 
                  t1*(-427 + 882*t2 - 1129*Power(t2,2) + 
                     838*Power(t2,3) - 358*Power(t2,4) + 36*Power(t2,5) + 
                     24*Power(t2,6))))))*T2q(s1,s))/
     (Power(s,2)*(-1 + s1)*Power(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2),2)*(-1 + s2)*(-1 + t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) - 
    (8*((-1 + s1)*(s1 - t2)*Power(-1 + t2,4)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         Power(s,10)*(Power(t1,3) + t1*Power(t2,2) + 2*Power(t2,3)) + 
         Power(s,9)*(Power(t1,3)*(-9 - 6*s1 + 6*t2) + 
            t1*t2*(3 + s1*(-1 + 3*s2 - 5*t2) - 3*t2 + 8*Power(t2,2) - 
               s2*(3 + t2)) + 
            Power(t1,2)*(3 + s2*t2 + s1*(-3 + 3*s2 + t2)) + 
            Power(t2,2)*(4 + s1*(2 + 4*s2 - 6*t2) - 8*t2 + 
               10*Power(t2,2) - s2*(3 + 4*t2))) + 
         Power(s,8)*(Power(t1,3)*
             (33 + 15*Power(s1,2) + s1*(43 - 33*t2) - 49*t2 + 
               15*Power(t2,2)) + 
            Power(t1,2)*(-24 + Power(s1,2)*(13 - 15*s2 - 5*t2) + 
               (13 - 4*s2)*t2 + (3 + 6*s2)*Power(t2,2) + 
               s1*(9 - 30*t2 + 6*Power(t2,2) + 2*s2*(-10 + 7*t2))) + 
            t1*(3 + (-24 + 23*s2)*t2 + (11 - 16*s2)*Power(t2,2) - 
               (31 + 8*s2)*Power(t2,3) + 25*Power(t2,4) + 
               Power(s1,2)*(1 + 3*Power(s2,2) - t2 + 10*Power(t2,2) - 
                  s2*(3 + 10*t2)) + 
               s1*(-3 + 10*t2 - Power(s2,2)*t2 + 12*Power(t2,2) - 
                  33*Power(t2,3) + s2*(3 - 7*t2 + 24*Power(t2,2)))) + 
            t2*(3 - 26*t2 + 13*Power(t2,2) - 23*Power(t2,3) + 
               20*Power(t2,4) + Power(s2,2)*t2*(1 + 2*t2) + 
               Power(s1,2)*(1 + 3*Power(s2,2) + s2*(2 - 11*t2) - 5*t2 + 
                  6*Power(t2,2)) + 
               s2*(-3 + 19*t2 + 16*Power(t2,2) - 20*Power(t2,3)) + 
               s1*(2 - 3*t2 + 21*Power(t2,2) - 26*Power(t2,3) - 
                  Power(s2,2)*(3 + 5*t2) + 
                  s2*(3 - 19*t2 + 30*Power(t2,2))))) + 
         Power(s,7)*(1 - 21*t2 + 19*s2*t2 + 65*Power(t2,2) - 
            40*s2*Power(t2,2) - 14*Power(s2,2)*Power(t2,2) + 
            8*Power(t2,3) - 9*s2*Power(t2,3) - 
            18*Power(s2,2)*Power(t2,3) - 59*Power(t2,4) + 
            99*s2*Power(t2,4) + 10*Power(s2,2)*Power(t2,4) - 
            4*Power(t2,5) - 40*s2*Power(t2,5) + 20*Power(t2,6) + 
            Power(t1,3)*(-61 + 156*t2 - 109*Power(t2,2) + 
               20*Power(t2,3)) + 
            Power(t1,2)*(75 - 5*(17 + s2)*t2 - 
               (4 + 21*s2)*Power(t2,2) + (14 + 15*s2)*Power(t2,3)) + 
            t1*(-21 + (80 - 60*s2)*t2 + 
               2*(-42 + 68*s2 + Power(s2,2))*Power(t2,2) - 
               5*(-7 + 2*s2)*Power(t2,3) - (97 + 25*s2)*Power(t2,4) + 
               40*Power(t2,5)) + 
            Power(s1,3)*(Power(s2,3) - 20*Power(t1,3) - 
               2*Power(-1 + t2,2)*t2 - 4*Power(s2,2)*(3*t1 + 2*t2) + 
               2*Power(t1,2)*(-11 + 5*t2) + 
               t1*(-5 + 9*t2 - 10*Power(t2,2)) + 
               s2*(1 + 30*Power(t1,2) - 4*t2 + 9*Power(t2,2) + 
                  t1*(11 + 10*t2))) + 
            Power(s1,2)*(1 - 
               (-7 + 15*s2 + 9*Power(s2,2) + 2*Power(s2,3))*t2 + 
               (-13 + 59*s2 + 26*Power(s2,2))*Power(t2,2) - 
               2*(8 + 27*s2)*Power(t2,3) + 22*Power(t2,4) + 
               Power(t1,3)*(-85 + 75*t2) + 
               Power(t1,2)*(-59 + s2*(81 - 75*t2) + 114*t2 - 
                  27*Power(t2,2)) + 
               t1*(10 - 8*t2 - 46*Power(t2,2) + 54*Power(t2,3) + 
                  Power(s2,2)*(-16 + 21*t2) + 
                  s2*(6 + 15*t2 - 62*Power(t2,2)))) + 
            s1*(Power(t1,3)*(-124 + 209*t2 - 75*Power(t2,2)) + 
               Power(t1,2)*(16 + 129*t2 - 122*Power(t2,2) + 
                  17*Power(t2,3) + s2*(51 - 92*t2 + 24*Power(t2,2))) + 
               t1*(9 - 36*t2 - 7*Power(s2,2)*(-2 + t2)*t2 + 
                  29*Power(t2,2) + 115*Power(t2,3) - 84*Power(t2,4) + 
                  s2*(-16 - 47*t2 - 54*Power(t2,2) + 78*Power(t2,3))) + 
               t2*(-10 - 25*t2 + Power(s2,3)*t2 + 78*Power(t2,2) + 
                  22*Power(t2,3) - 44*Power(t2,4) + 
                  Power(s2,2)*(16 + 19*t2 - 28*Power(t2,2)) + 
                  s2*(-9 + 39*t2 - 153*Power(t2,2) + 80*Power(t2,3))))) + 
         Power(s,6)*(-6 + 52*t2 - 38*s2*t2 - 54*Power(t2,2) + 
            58*Power(s2,2)*Power(t2,2) - 176*Power(t2,3) - 
            8*s2*Power(t2,3) + 48*Power(s2,2)*Power(t2,3) - 
            2*Power(s2,3)*Power(t2,3) + 391*Power(t2,4) - 
            158*s2*Power(t2,4) - 90*Power(s2,2)*Power(t2,4) - 
            273*Power(t2,5) + 178*s2*Power(t2,5) + 
            20*Power(s2,2)*Power(t2,5) + 56*Power(t2,6) - 
            40*s2*Power(t2,6) + 10*Power(t2,7) + 
            Power(t1,3)*(51 - 230*t2 + 290*Power(t2,2) - 
               126*Power(t2,3) + 15*Power(t2,4)) + 
            Power(t1,2)*(-108 + 2*(88 + 29*s2)*t2 - 
               3*(2 + 11*s2)*Power(t2,2) - (85 + 43*s2)*Power(t2,3) + 
               4*(7 + 5*s2)*Power(t2,4)) + 
            t1*(54 + (-123 + 38*s2)*t2 + 
               (215 - 309*s2 - 12*Power(s2,2))*Power(t2,2) + 
               (-21 + 205*s2 + 12*Power(s2,2))*Power(t2,3) + 
               (48 + 65*s2)*Power(t2,4) - 2*(71 + 20*s2)*Power(t2,5) + 
               35*Power(t2,6)) + 
            Power(s1,4)*(-3*Power(s2,3) + 15*Power(t1,3) - 
               (-1 + t2)*t2 - 2*Power(t1,2)*(-9 + 5*t2) + 
               Power(s2,2)*(1 + 18*t1 + 6*t2) - 
               s2*(2 + 15*t1 + 30*Power(t1,2) - 2*t2 + Power(t2,2)) + 
               t1*(7 - 11*t2 + 5*Power(t2,2))) + 
            Power(s1,3)*(2 - 90*Power(t1,3)*(-1 + t2) - 15*t2 + 
               17*Power(t2,2) + 2*Power(t2,3) - 6*Power(t2,4) + 
               5*Power(s2,3)*(-1 + 2*t2) + 
               Power(s2,2)*(-1 + t1*(53 - 66*t2) + 24*t2 - 
                  39*Power(t2,2)) + 
               Power(t1,2)*(97 - 168*t2 + 48*Power(t2,2)) + 
               t1*(8 - 52*t2 + 84*Power(t2,2) - 44*Power(t2,3)) + 
               s2*(4*Power(t1,2)*(-33 + 35*t2) + 
                  t2*(19 - 46*t2 + 31*Power(t2,2)) + 
                  t1*(-42 + 35*t2 + 46*Power(t2,2)))) + 
            Power(s1,2)*(1 - 53*t2 + Power(s2,3)*(14 - 11*t2)*t2 + 
               233*Power(t2,2) - 241*Power(t2,3) + 30*Power(t2,4) + 
               30*Power(t2,5) + 
               Power(t1,3)*(192 - 364*t2 + 153*Power(t2,2)) + 
               Power(t1,2)*(116 - 477*t2 + 384*Power(t2,2) - 
                  70*Power(t2,3)) + 
               t1*(-50 + 83*t2 + 65*Power(t2,2) - 228*Power(t2,3) + 
                  107*Power(t2,4)) + 
               Power(s2,2)*(t1*(34 - 117*t2 + 61*Power(t2,2)) + 
                  t2*(-7 - 90*t2 + 72*Power(t2,2))) - 
               s2*(1 - 36*t2 + 177*Power(t2,2) - 258*Power(t2,3) + 
                  93*Power(t2,4) + 
                  Power(t1,2)*(169 - 360*t2 + 153*Power(t2,2)) + 
                  t1*(7 - 104*t2 - 117*Power(t2,2) + 142*Power(t2,3)))) \
+ s1*(-3 + 8*t2 + 159*Power(t2,2) - 524*Power(t2,3) + 471*Power(t2,4) - 
               75*Power(t2,5) - 36*Power(t2,6) + 
               Power(s2,3)*Power(t2,2)*(-11 + 4*t2) + 
               Power(t1,3)*(183 - 504*t2 + 409*Power(t2,2) - 
                  90*Power(t2,3)) + 
               Power(t1,2)*(-96 - 201*t2 + 555*Power(t2,2) - 
                  259*Power(t2,3) + 28*Power(t2,4)) + 
               t1*(2 + 16*t2 - 111*Power(t2,2) - 68*Power(t2,3) + 
                  291*Power(t2,4) - 106*Power(t2,5)) - 
               Power(s2,2)*t2*
                (34 + 12*t2 - 155*Power(t2,2) + 61*Power(t2,3) + 
                  t1*(52 - 80*t2 + 17*Power(t2,2))) + 
               s2*(1 + t2 - 31*Power(t2,2) + 279*Power(t2,3) - 
                  374*Power(t2,4) + 100*Power(t2,5) + 
                  Power(t1,2)*
                   (-62 + 212*t2 - 165*Power(t2,2) + 13*Power(t2,3)) + 
                  t1*(32 + 253*t2 - 292*Power(t2,2) - 201*Power(t2,3) + 
                     133*Power(t2,4))))) + 
         Power(s,5)*(12 - 54*t1 + 45*Power(t1,2) + 9*Power(t1,3) - 
            36*t2 + 8*s2*t2 - 11*t1*t2 + 109*s2*t1*t2 - 
            21*Power(t1,2)*t2 - 145*s2*Power(t1,2)*t2 + 
            104*Power(t1,3)*t2 - 142*Power(t2,2) + 191*s2*Power(t2,2) - 
            108*Power(s2,2)*Power(t2,2) + 10*t1*Power(t2,2) + 
            91*s2*t1*Power(t2,2) + 30*Power(s2,2)*t1*Power(t2,2) - 
            189*Power(t1,2)*Power(t2,2) + 
            273*s2*Power(t1,2)*Power(t2,2) - 
            306*Power(t1,3)*Power(t2,2) + 665*Power(t2,3) - 
            138*s2*Power(t2,3) - 56*Power(s2,2)*Power(t2,3) + 
            12*Power(s2,3)*Power(t2,3) - 208*t1*Power(t2,3) - 
            282*s2*t1*Power(t2,3) - 68*Power(s2,2)*t1*Power(t2,3) + 
            291*Power(t1,2)*Power(t2,3) - 
            93*s2*Power(t1,2)*Power(t2,3) + 
            263*Power(t1,3)*Power(t2,3) - 1053*Power(t2,4) + 
            45*s2*Power(t2,4) + 306*Power(s2,2)*Power(t2,4) - 
            10*Power(s2,3)*Power(t2,4) + 352*t1*Power(t2,4) - 
            28*s2*t1*Power(t2,4) + 31*Power(s2,2)*t1*Power(t2,4) - 
            158*Power(t1,2)*Power(t2,4) - 
            42*s2*Power(t1,2)*Power(t2,4) - 79*Power(t1,3)*Power(t2,4) + 
            875*Power(t2,5) - 233*s2*Power(t2,5) - 
            162*Power(s2,2)*Power(t2,5) + 8*t1*Power(t2,5) + 
            145*s2*t1*Power(t2,5) + 32*Power(t1,2)*Power(t2,5) + 
            15*s2*Power(t1,2)*Power(t2,5) + 6*Power(t1,3)*Power(t2,5) - 
            407*Power(t2,6) + 147*s2*Power(t2,6) + 
            20*Power(s2,2)*Power(t2,6) - 113*t1*Power(t2,6) - 
            35*s2*t1*Power(t2,6) + 84*Power(t2,7) - 20*s2*Power(t2,7) + 
            16*t1*Power(t2,7) + 2*Power(t2,8) + 
            Power(s1,5)*(3*Power(s2,3) - 2*Power(s2,2)*(1 + 6*t1) + 
               s2*(1 + 15*Power(t1,2) + t1*(9 - 5*t2) - Power(t2,2)) - 
               t1*(3 + 6*Power(t1,2) + t1*(7 - 5*t2) - 4*t2 + 
                  Power(t2,2))) + 
            Power(s1,4)*(Power(s2,3)*(12 - 17*t2) + 
               (-3 + t2)*Power(-1 + t2,2) + Power(t1,3)*(-55 + 60*t2) + 
               Power(t1,2)*(-71 + 120*t2 - 42*Power(t2,2)) + 
               t1*(-25 + 70*t2 - 63*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s2,2)*(-6 - 4*t2 + 17*Power(t2,2) + 
                  t1*(-67 + 82*t2)) + 
               s2*(5 - 12*t2 + 7*Power(t2,2) - 
                  5*Power(t1,2)*(-22 + 25*t2) + 
                  t1*(61 - 84*t2 + 9*Power(t2,2)))) + 
            Power(s1,3)*(Power(s2,3)*(11 - 46*t2 + 30*Power(t2,2)) - 
               2*Power(t1,3)*(80 - 167*t2 + 81*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (9 - 104*t2 + 50*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(-200 + 588*t2 - 486*Power(t2,2) + 
                  113*Power(t2,3)) + 
               t1*(-4 + 113*t2 - 293*Power(t2,2) + 247*Power(t2,3) - 
                  63*Power(t2,4)) + 
               Power(s2,2)*(-6 + 2*t2 + 77*Power(t2,2) - 
                  58*Power(t2,3) + t1*(-93 + 261*t2 - 146*Power(t2,2))) \
+ s2*(-3 - 63*t2 + 175*Power(t2,2) - 133*Power(t2,3) + 24*Power(t2,4) + 
                  Power(t1,2)*(228 - 521*t2 + 264*Power(t2,2)) + 
                  3*t1*(32 - 89*t2 + 31*Power(t2,2) + 16*Power(t2,3)))) \
+ Power(s1,2)*(Power(s2,3)*t2*(-40 + 72*t2 - 19*Power(t2,2)) + 
               Power(t1,3)*(-216 + 657*t2 - 615*Power(t2,2) + 
                  160*Power(t2,3)) + 
               Power(t1,2)*(-149 + 950*t2 - 1404*Power(t2,2) + 
                  672*Power(t2,3) - 102*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (-16 + 140*t2 - 441*Power(t2,2) + 184*Power(t2,3) + 
                  18*Power(t2,4)) + 
               t1*(73 - 194*t2 + 31*Power(t2,2) + 383*Power(t2,3) - 
                  389*Power(t2,4) + 96*Power(t2,5)) + 
               Power(s2,2)*(t1*
                   (-38 + 247*t2 - 328*Power(t2,2) + 79*Power(t2,3)) + 
                  t2*(73 + 50*t2 - 245*Power(t2,2) + 89*Power(t2,3))) + 
               s2*(-2 - 51*t2 + 324*Power(t2,2) - 608*Power(t2,3) + 
                  399*Power(t2,4) - 62*Power(t2,5) + 
                  Power(t1,2)*
                   (162 - 610*t2 + 639*Power(t2,2) - 150*Power(t2,3)) + 
                  t1*(56 - 493*t2 + 403*Power(t2,2) + 257*Power(t2,3) - 
                     157*Power(t2,4)))) + 
            s1*(Power(s2,3)*Power(t2,2)*(29 - 24*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(-145 + 579*t2 - 783*Power(t2,2) + 
                  403*Power(t2,3) - 60*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (-8 - 60*t2 + 337*Power(t2,2) - 540*Power(t2,3) + 
                  215*Power(t2,4) + 14*Power(t2,5)) + 
               Power(t1,2)*(137 + 136*t2 - 1056*Power(t2,2) + 
                  1011*Power(t2,3) - 306*Power(t2,4) + 27*Power(t2,5)) + 
               t1*(-19 + 120*t2 + 82*Power(t2,2) - 214*Power(t2,3) - 
                  232*Power(t2,4) + 332*Power(t2,5) - 69*Power(t2,6)) + 
               Power(s2,2)*t2*
                (44 + 25*t2 - 402*Power(t2,2) + 346*Power(t2,3) - 
                  64*Power(t2,4) + 
                  t1*(78 - 228*t2 + 139*Power(t2,2) - 19*Power(t2,3))) - 
               s2*(1 - 7*t2 + 125*Power(t2,2) + 93*Power(t2,3) - 
                  558*Power(t2,4) + 406*Power(t2,5) - 60*Power(t2,6) + 
                  Power(t1,2)*
                   (-41 + 213*t2 - 321*Power(t2,2) + 112*Power(t2,3) + 
                     13*Power(t2,4)) + 
                  t1*(38 + 500*t2 - 1211*Power(t2,2) + 315*Power(t2,3) + 
                     383*Power(t2,4) - 127*Power(t2,5))))) - 
         s*Power(-1 + t2,3)*(-4 + 12*t1 - 12*Power(t1,2) + 
            4*Power(t1,3) + 33*t2 - 8*s2*t2 - 84*t1*t2 + 16*s2*t1*t2 + 
            69*Power(t1,2)*t2 - 8*s2*Power(t1,2)*t2 - 
            18*Power(t1,3)*t2 - 78*Power(t2,2) + 41*s2*Power(t2,2) - 
            2*Power(s2,2)*Power(t2,2) + 164*t1*Power(t2,2) - 
            80*s2*t1*Power(t2,2) + 2*Power(s2,2)*t1*Power(t2,2) - 
            100*Power(t1,2)*Power(t2,2) + 
            39*s2*Power(t1,2)*Power(t2,2) + 14*Power(t1,3)*Power(t2,2) + 
            74*Power(t2,3) - 48*s2*Power(t2,3) + 
            5*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) - 
            131*t1*Power(t2,3) + 94*s2*t1*Power(t2,3) - 
            20*Power(s2,2)*t1*Power(t2,3) + 55*Power(t1,2)*Power(t2,3) - 
            28*s2*Power(t1,2)*Power(t2,3) - 3*Power(t1,3)*Power(t2,3) - 
            24*Power(t2,4) + s2*Power(t2,4) + 
            12*Power(s2,2)*Power(t2,4) - Power(s2,3)*Power(t2,4) + 
            46*t1*Power(t2,4) - 40*s2*t1*Power(t2,4) + 
            12*Power(s2,2)*t1*Power(t2,4) - 12*Power(t1,2)*Power(t2,4) + 
            6*s2*Power(t1,2)*Power(t2,4) - 3*Power(t2,5) + 
            18*s2*Power(t2,5) - 17*Power(s2,2)*Power(t2,5) + 
            2*Power(s2,3)*Power(t2,5) - 7*t1*Power(t2,5) + 
            10*s2*t1*Power(t2,5) - 3*Power(s2,2)*t1*Power(t2,5) + 
            2*Power(t2,6) - 4*s2*Power(t2,6) + 
            2*Power(s2,2)*Power(t2,6) - 
            Power(s1,6)*Power(s2 - t1,2)*(-1 + s2 - t1 + t2) + 
            Power(s1,5)*(s2 - t1)*
             (s2*(t1*(8 - 6*t2) + 4*Power(-1 + t2,2)) - 
               Power(-1 + t2,2) - 3*t1*Power(-1 + t2,2) + 
               Power(s2,2)*(-4 + 3*t2) + Power(t1,2)*(-4 + 3*t2)) + 
            Power(s1,4)*(Power(s2,3)*(2 + 17*t2 - 2*Power(t2,2)) + 
               t1*(-(Power(-1 + t2,2)*(-2 + 3*t2)) + 
                  Power(t1,2)*(-9 - 11*t2 + 3*Power(t2,2)) + 
                  t1*(23 - 34*t2 + 14*Power(t2,2) - 3*Power(t2,3))) + 
               Power(s2,2)*(t1*(-12 - 47*t2 + 8*Power(t2,2)) + 
                  3*(6 - 9*t2 + 5*Power(t2,2) - 2*Power(t2,3))) + 
               s2*(Power(-1 + t2,2)*(-2 + 3*t2) + 
                  Power(t1,2)*(19 + 41*t2 - 9*Power(t2,2)) + 
                  t1*(-41 + 61*t2 - 29*Power(t2,2) + 9*Power(t2,3)))) + 
            Power(s1,3)*(Power(-1 + t2,3) + 
               t1*Power(-1 + t2,2)*(24 - 16*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(11 + 21*t2 + 7*Power(t2,2) - Power(t2,3)) - 
               Power(s2,3)*(2 + 5*t2 + 29*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,2)*(-29 - 18*t2 + 55*Power(t2,2) - 
                  9*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(10 - 90*t2 + 85*Power(t2,2) - 
                  9*Power(t2,3) + 4*Power(t2,4) + 
                  t1*(5 + 48*t2 + 61*Power(t2,2))) + 
               s2*(-(Power(-1 + t2,2)*(16 - 8*t2 + 3*Power(t2,2))) + 
                  Power(t1,2)*
                   (-14 - 64*t2 - 39*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(12 + 129*t2 - 161*Power(t2,2) + 25*Power(t2,3) - 
                     5*Power(t2,4)))) + 
            s1*(-8*Power(-1 + t2,3)*(-2 + Power(t2,2)) - 
               Power(s2,3)*Power(t2,2)*
                (6 - t2 + 11*Power(t2,2) + Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (-33 - 8*t2 - 19*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(1 + 26*t2 - 15*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(-18 - 16*t2 + 34*Power(t2,2) - 
                  11*Power(t2,3) + 11*Power(t2,4)) + 
               Power(s2,2)*t2*
                (4 - 78*Power(t2,2) + 77*Power(t2,3) - 3*Power(t2,4) + 
                  t1*(-4 + 45*t2 + 9*Power(t2,3) + Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (8 - 29*t2 - 49*Power(t2,2) + 12*Power(t2,3)) + 
                  Power(t1,2)*
                   (8 - 41*t2 - 21*Power(t2,2) + 9*Power(t2,3) - 
                     6*Power(t2,4)) + 
                  t1*(-16 + 86*t2 - 50*Power(t2,2) + 33*Power(t2,3) - 
                     54*Power(t2,4) + Power(t2,5)))) + 
            Power(s1,2)*(Power(-1 + t2,3)*(-2 + 5*t2) + 
               Power(t1,2)*(32 + 6*t2 - 6*Power(t2,2) - 
                  32*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (22 + 39*t2 - 16*Power(t2,2) + Power(t2,3)) - 
               Power(t1,3)*(12 + 15*t2 + 9*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*t2*
                (6 + 3*t2 + 25*Power(t2,2) + 3*Power(t2,3)) - 
               Power(s2,2)*(2 + 15*t2 - 138*Power(t2,2) + 
                  122*Power(t2,3) - 2*Power(t2,4) + Power(t2,5) + 
                  t1*(-2 + 30*t2 + 48*Power(t2,2) + 32*Power(t2,3) + 
                     3*Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (4 + 55*t2 - 14*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (2 + 63*t2 + 30*Power(t2,2) + 16*Power(t2,3)) + 
                  t1*(-6 - 56*t2 - 81*Power(t2,2) + 151*Power(t2,3) - 
                     9*Power(t2,4) + Power(t2,5))))) + 
         Power(s,2)*Power(-1 + t2,2)*
          (12 - 48*t1 + 60*Power(t1,2) - 24*Power(t1,3) - 123*t2 + 
            46*s2*t2 + 341*t1*t2 - 96*s2*t1*t2 - 304*Power(t1,2)*t2 + 
            50*s2*Power(t1,2)*t2 + 86*Power(t1,3)*t2 + 348*Power(t2,2) - 
            199*s2*Power(t2,2) + 14*Power(s2,2)*Power(t2,2) - 
            701*t1*Power(t2,2) + 351*s2*t1*Power(t2,2) - 
            12*Power(s2,2)*t1*Power(t2,2) + 
            472*Power(t1,2)*Power(t2,2) - 
            179*s2*Power(t1,2)*Power(t2,2) - 
            80*Power(t1,3)*Power(t2,2) - 429*Power(t2,3) + 
            182*s2*Power(t2,3) + 31*Power(s2,2)*Power(t2,3) - 
            12*Power(s2,3)*Power(t2,3) + 668*t1*Power(t2,3) - 
            433*s2*t1*Power(t2,3) + 68*Power(s2,2)*t1*Power(t2,3) - 
            300*Power(t1,2)*Power(t2,3) + 
            159*s2*Power(t1,2)*Power(t2,3) + 
            22*Power(t1,3)*Power(t2,3) + 251*Power(t2,4) + 
            66*s2*Power(t2,4) - 144*Power(s2,2)*Power(t2,4) + 
            19*Power(s2,3)*Power(t2,4) - 364*t1*Power(t2,4) + 
            250*s2*t1*Power(t2,4) - 63*Power(s2,2)*t1*Power(t2,4) + 
            78*Power(t1,2)*Power(t2,4) - 40*s2*Power(t1,2)*Power(t2,4) - 
            Power(t1,3)*Power(t2,4) - 68*Power(t2,5) - 
            110*s2*Power(t2,5) + 115*Power(s2,2)*Power(t2,5) - 
            10*Power(s2,3)*Power(t2,5) + 107*t1*Power(t2,5) - 
            77*s2*t1*Power(t2,5) + 16*Power(s2,2)*t1*Power(t2,5) - 
            8*Power(t1,2)*Power(t2,5) + s2*Power(t1,2)*Power(t2,5) + 
            15*Power(t2,6) + 13*s2*Power(t2,6) - 
            16*Power(s2,2)*Power(t2,6) + t1*Power(t2,6) + 
            5*s2*t1*Power(t2,6) + 2*Power(t1,2)*Power(t2,6) - 
            8*Power(t2,7) + 2*s2*Power(t2,7) - 4*t1*Power(t2,7) + 
            2*Power(t2,8) - Power(s1,6)*Power(s2 - t1,2)*
             (-5 + 3*s2 - 3*t1 + 5*t2) + 
            Power(s1,5)*(Power(s2,3)*(-7 + 4*t2) + 
               Power(s2,2)*(14 + 24*t1 - 24*t2 - 15*t1*t2 + 
                  10*Power(t2,2)) + 
               s2*(9*Power(t1,2)*(-3 + 2*t2) - 
                  Power(-1 + t2,2)*(2 + 3*t2) + 
                  t1*(-34 + 60*t2 - 26*Power(t2,2))) + 
               t1*(Power(t1,2)*(10 - 7*t2) + 
                  Power(-1 + t2,2)*(4 + t2) + 
                  4*t1*(5 - 9*t2 + 4*Power(t2,2)))) + 
            Power(s1,4)*(Power(-1 + t2,3)*(-11 + 4*t2) + 
               3*Power(t1,3)*(1 - 6*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(9 - 2*t2 + 2*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(-10 + 13*t2 + 3*Power(t2,2)) - 
               Power(t1,2)*(12 + 35*t2 - 68*Power(t2,2) + 
                  21*Power(t2,3)) - 
               Power(s2,2)*(29 - 19*t2 - 11*Power(t2,2) + 
                  Power(t2,3) + t1*(19 + 6*t2 + 2*Power(t2,2))) + 
               s2*(Power(t1,2)*(7 + 26*t2 - 6*Power(t2,2)) + 
                  Power(-1 + t2,2)*(-5 + 2*t2 + 9*Power(t2,2)) + 
                  t1*(45 + 4*t2 - 67*Power(t2,2) + 18*Power(t2,3)))) + 
            Power(s1,3)*(4*Power(s2,3)*(1 - 5*t2 + 10*Power(t2,2)) - 
               Power(-1 + t2,3)*(4 - 43*t2 + 14*Power(t2,2)) - 
               Power(t1,3)*(7 + 25*t2 - 11*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-72 + 15*t2 + 13*Power(t2,2) + 3*Power(t2,3)) + 
               2*Power(t1,2)*
                (32 - 9*t2 - Power(t2,2) - 29*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s2,2)*(-27 + 214*t2 - 214*Power(t2,2) + 
                  34*Power(t2,3) - 7*Power(t2,4) + 
                  t1*(-13 + 24*t2 - 93*Power(t2,2) + 10*Power(t2,3))) + 
               s2*(Power(t1,2)*
                   (18 + 15*t2 + 48*Power(t2,2) - 9*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (-37 - 19*t2 + 6*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(2 - 323*t2 + 363*Power(t2,2) - 45*Power(t2,3) + 
                     3*Power(t2,4)))) + 
            Power(s1,2)*(-(Power(s2,3)*t2*
                  (20 - 32*t2 + 56*Power(t2,2) + 7*Power(t2,3))) + 
               Power(-1 + t2,3)*
                (-7 - 3*t2 - 48*Power(t2,2) + 18*Power(t2,3)) + 
               Power(t1,3)*(12 + 18*t2 + 24*Power(t2,2) - 
                  4*Power(t2,3) + Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (-37 - 187*t2 + 43*Power(t2,2) + 14*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*(-17 - 130*t2 + 102*Power(t2,2) + 
                  19*Power(t2,3) + 30*Power(t2,4) - 4*Power(t2,5)) + 
               Power(s2,2)*(6 + 125*t2 - 541*Power(t2,2) + 
                  454*Power(t2,3) - 46*Power(t2,4) + 2*Power(t2,5) + 
                  t1*(-4 + 54*t2 + 2*Power(t2,2) + 100*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (11 - 204*t2 + 8*Power(t2,2) + 16*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  Power(t1,2)*
                   (21 - 143*t2 + 41*Power(t2,2) - 81*Power(t2,3) + 
                     9*Power(t2,4)) + 
                  t1*(-59 + 163*t2 + 393*Power(t2,2) - 
                     583*Power(t2,3) + 92*Power(t2,4) - 6*Power(t2,5)))) \
+ s1*(Power(t1,3)*(3 - 54*t2 + 39*Power(t2,2) - 15*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (28 - 40*t2 + 35*Power(t2,2) + 4*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (43 - 41*t2 - 9*Power(t2,2) - 18*Power(t2,3) + 
                  10*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (-92 - 12*t2 - 169*Power(t2,2) + 15*Power(t2,3) + 
                  14*Power(t2,4)) - 
               2*Power(t1,2)*
                (-23 + 13*t2 - 11*Power(t2,2) + 6*Power(t2,3) + 
                  12*Power(t2,4) + 3*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-20 - 129*t2 + 500*Power(t2,2) - 388*Power(t2,3) + 
                  36*Power(t2,4) + Power(t2,5) + 
                  t1*(16 - 109*t2 + 56*Power(t2,2) - 41*Power(t2,3) - 
                     3*Power(t2,4))) + 
               s2*(-(Power(-1 + t2,2)*
                     (26 - 66*t2 - 255*Power(t2,2) + 39*Power(t2,3) + 
                       12*Power(t2,4))) + 
                  Power(t1,2)*
                   (-30 + 88*t2 + 56*Power(t2,2) - 73*Power(t2,3) + 
                     43*Power(t2,4) - 3*Power(t2,5)) + 
                  t1*(56 - 152*t2 + 88*Power(t2,2) - 265*Power(t2,3) + 
                     307*Power(t2,4) - 35*Power(t2,5) + Power(t2,6))))) + 
         Power(s,3)*(-1 + t2)*
          (9 - 63*t1 + 111*Power(t1,2) - 57*Power(t1,3) - 171*t2 + 
            91*s2*t2 + 561*t1*t2 - 226*s2*t1*t2 - 562*Power(t1,2)*t2 + 
            131*s2*Power(t1,2)*t2 + 179*Power(t1,3)*t2 + 
            647*Power(t2,2) - 440*s2*Power(t2,2) + 
            52*Power(s2,2)*Power(t2,2) - 1240*t1*Power(t2,2) + 
            688*s2*t1*Power(t2,2) - 30*Power(s2,2)*t1*Power(t2,2) + 
            950*Power(t1,2)*Power(t2,2) - 
            418*s2*Power(t1,2)*Power(t2,2) - 
            156*Power(t1,3)*Power(t2,2) - 1082*Power(t2,3) + 
            418*s2*Power(t2,3) + 85*Power(s2,2)*Power(t2,3) - 
            26*Power(s2,3)*Power(t2,3) + 1372*t1*Power(t2,3) - 
            814*s2*t1*Power(t2,3) + 132*Power(s2,2)*t1*Power(t2,3) - 
            707*Power(t1,2)*Power(t2,3) + 
            391*s2*Power(t1,2)*Power(t2,3) + 
            24*Power(t1,3)*Power(t2,3) + 969*Power(t2,4) + 
            145*s2*Power(t2,4) - 404*Power(s2,2)*Power(t2,4) + 
            45*Power(s2,3)*Power(t2,4) - 1004*t1*Power(t2,4) + 
            539*s2*t1*Power(t2,4) - 135*Power(s2,2)*t1*Power(t2,4) + 
            249*Power(t1,2)*Power(t2,4) - 
            108*s2*Power(t1,2)*Power(t2,4) + 
            14*Power(t1,3)*Power(t2,4) - 525*Power(t2,5) - 
            226*s2*Power(t2,5) + 329*Power(s2,2)*Power(t2,5) - 
            20*Power(s2,3)*Power(t2,5) + 406*t1*Power(t2,5) - 
            225*s2*t1*Power(t2,5) + 36*Power(s2,2)*t1*Power(t2,5) - 
            51*Power(t1,2)*Power(t2,5) - 3*Power(t1,3)*Power(t2,5) + 
            215*Power(t2,6) - s2*Power(t2,6) - 
            64*Power(s2,2)*Power(t2,6) - 13*t1*Power(t2,6) + 
            41*s2*t1*Power(t2,6) + 10*Power(t1,2)*Power(t2,6) + 
            s2*Power(t1,2)*Power(t2,6) - 78*Power(t2,7) + 
            13*s2*Power(t2,7) + 2*Power(s2,2)*Power(t2,7) - 
            19*t1*Power(t2,7) - 3*s2*t1*Power(t2,7) + 16*Power(t2,8) - 
            3*Power(s1,6)*Power(s2 - t1,2)*(-1 + s2 - t1 + t2) + 
            Power(s1,4)*(Power(-1 + t2,3)*(-29 + 11*t2) - 
               t1*Power(-1 + t2,2)*(-53 + 72*t2) - 
               Power(t1,2)*Power(-1 + t2,2)*(-112 + 81*t2) + 
               Power(s2,3)*(-14 + 52*t2 - 21*Power(t2,2)) + 
               Power(t1,3)*(50 - 124*t2 + 57*Power(t2,2)) + 
               Power(s2,2)*(-(Power(-1 + t2,2)*(-38 + 21*t2)) + 
                  t1*(67 - 206*t2 + 88*Power(t2,2))) - 
               s2*(-2*t1*Power(-1 + t2,2)*(-67 + 43*t2) - 
                  Power(-1 + t2,2)*(-37 + 34*t2 + 22*Power(t2,2)) + 
                  Power(t1,2)*(103 - 278*t2 + 124*Power(t2,2)))) + 
            Power(s1,5)*(Power(s2,2)*
                (t1*(48 - 63*t2) + 20*Power(-1 + t2,2)) + 
               t1*(Power(t1,2)*(22 - 27*t2) + 34*t1*Power(-1 + t2,2) - 
                  (-16 + t2)*Power(-1 + t2,2)) + 
               Power(s2,3)*(-13 + 18*t2) + 
               s2*(-54*t1*Power(-1 + t2,2) + 
                  Power(t1,2)*(-57 + 72*t2) - 5*(2 - 3*t2 + Power(t2,3))\
)) + Power(s1,3)*(-2*Power(-1 + t2,3)*(9 - 77*t2 + 29*Power(t2,2)) + 
               Power(t1,3)*(45 - 179*t2 + 201*Power(t2,2) - 
                  51*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-51 - 93*t2 + 102*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(-10 + 29*t2 - 42*Power(t2,2) + 
                  7*Power(t2,3)) + 
               3*Power(t1,2)*
                (57 - 180*t2 + 227*Power(t2,2) - 130*Power(t2,3) + 
                  26*Power(t2,4)) + 
               2*Power(s2,2)*
                (12 + t2 - 23*Power(t2,2) + 13*Power(t2,3) - 
                  3*Power(t2,4) + 
                  t1*(13 - 62*t2 + 88*Power(t2,2) - 15*Power(t2,3))) + 
               s2*(-(Power(-1 + t2,2)*
                     (-4 - 137*t2 + 71*Power(t2,2) + 29*Power(t2,3))) + 
                  Power(t1,2)*
                   (-52 + 247*t2 - 308*Power(t2,2) + 65*Power(t2,3)) + 
                  t1*(-103 + 213*t2 - 212*Power(t2,2) + 
                     125*Power(t2,3) - 23*Power(t2,4)))) + 
            Power(s1,2)*(-(Power(s2,3)*t2*
                  (-8 + 9*t2 + 16*Power(t2,2) + 8*Power(t2,3))) + 
               Power(-1 + t2,3)*
                (-27 + 70*t2 - 238*Power(t2,2) + 96*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (25 + 278*t2 - 16*Power(t2,2) - 84*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,3)*(31 - 124*t2 + 233*Power(t2,2) - 
                  136*Power(t2,3) + 21*Power(t2,4)) + 
               Power(t1,2)*(68 - 598*t2 + 950*Power(t2,2) - 
                  628*Power(t2,3) + 239*Power(t2,4) - 31*Power(t2,5)) + 
               Power(s2,2)*(6 + 95*t2 - 568*Power(t2,2) + 
                  620*Power(t2,3) - 165*Power(t2,4) + 12*Power(t2,5) + 
                  t1*(4 - 56*t2 + 128*Power(t2,2) - 2*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (29 - 43*t2 - 91*Power(t2,2) + 21*Power(t2,3) + 
                     9*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (26 - 237*t2 - 105*Power(t2,2) + 99*Power(t2,3) + 
                     12*Power(t2,4)) - 
                  2*t1*(80 - 302*t2 + 61*Power(t2,2) + 
                     259*Power(t2,3) - 112*Power(t2,4) + 14*Power(t2,5))\
)) + s1*(Power(s2,3)*Power(t2,2)*
                (28 - 51*t2 + 39*Power(t2,2) + 7*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (40 - 126*t2 + 100*Power(t2,2) - 136*Power(t2,3) + 
                  65*Power(t2,4)) + 
               Power(t1,3)*(14 - 103*t2 + 157*Power(t2,2) - 
                  125*Power(t2,3) + 37*Power(t2,4) - 3*Power(t2,5)) - 
               t1*Power(-1 + t2,2)*
                (97 + 81*t2 + 383*Power(t2,2) - 61*Power(t2,3) - 
                  64*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,2)*(36 - 116*t2 + 418*Power(t2,2) - 
                  486*Power(t2,3) + 203*Power(t2,4) - 58*Power(t2,5) + 
                  3*Power(t2,6)) - 
               Power(s2,2)*t2*
                (40 + 256*t2 - 982*Power(t2,2) + 842*Power(t2,3) - 
                  160*Power(t2,4) + 4*Power(t2,5) + 
                  t1*(-8 + 50*t2 - 14*Power(t2,2) + 36*Power(t2,3) + 
                     5*Power(t2,4))) + 
               s2*(-(Power(-1 + t2,2)*
                     (29 - 76*t2 - 492*Power(t2,2) + 36*Power(t2,3) + 
                       65*Power(t2,4))) + 
                  Power(t1,2)*
                   (-41 + 93*t2 + 48*Power(t2,2) - 105*Power(t2,3) + 
                     88*Power(t2,4) - 14*Power(t2,5)) + 
                  t1*(74 + 48*t2 - 475*Power(t2,2) - 24*Power(t2,3) + 
                     531*Power(t2,4) - 170*Power(t2,5) + 16*Power(t2,6)))\
)) - Power(s,4)*(Power(s1,6)*Power(s2 - t1,2)*(-1 + s2 - t1 + t2) - 
            (-1 + t2)*(6 + (81 - 67*s2)*t2 + 
               (-466 + 402*s2 - 103*Power(s2,2))*Power(t2,2) + 
               (907 - 150*s2 - 149*Power(s2,2) + 26*Power(s2,3))*
                Power(t2,3) - 
               (895 + 171*s2 - 380*Power(s2,2) + 20*Power(s2,3))*
                Power(t2,4) + 
               (545 - 66*s2 - 133*Power(s2,2))*Power(t2,5) + 
               (-231 + 56*s2 + 10*Power(s2,2))*Power(t2,6) + 
               (53 - 4*s2)*Power(t2,7) + 
               Power(t1,3)*(61 - 83*t2 - 58*Power(t2,2) + 
                  90*Power(t2,3) - 24*Power(t2,4) + Power(t2,5)) + 
               Power(t1,2)*(-72 + (362 - 184*s2)*t2 + 
                  (-534 + 371*s2)*Power(t2,2) + 
                  (339 - 149*s2)*Power(t2,3) - 
                  (113 + 12*s2)*Power(t2,4) + (23 + 6*s2)*Power(t2,5)) + 
               t1*(9 + (-370 + 253*s2)*t2 + 
                  (584 - 339*s2 + 40*Power(s2,2))*Power(t2,2) + 
                  (-676 + 189*s2 - 109*Power(s2,2))*Power(t2,3) + 
                  (570 - 203*s2 + 44*Power(s2,2))*Power(t2,4) + 
                  2*(-34 + 53*s2)*Power(t2,5) - 
                  4*(13 + 4*s2)*Power(t2,6) + 3*Power(t2,7))) + 
            Power(s1,5)*(-2*Power(s2,3)*(-5 + 6*t2) + 
               Power(s2,2)*(-10 - 39*t1 + 14*t2 + 45*t1*t2 - 
                  4*Power(t2,2)) + 
               t1*(Power(-1 + t2,2)*(-10 + 3*t2) + 
                  Power(t1,2)*(-19 + 21*t2) - 
                  6*t1*(4 - 7*t2 + 3*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(4 + 3*t2) - 
                  6*Power(t1,2)*(-8 + 9*t2) + 
                  t1*(34 - 56*t2 + 22*Power(t2,2)))) + 
            Power(s1,4)*(-3*Power(-1 + t2,3)*(-7 + 3*t2) + 
               Power(t1,3)*(-78 + 173*t2 - 93*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(43 - 90*t2 + 16*Power(t2,2)) + 
               Power(s2,3)*(19 - 56*t2 + 35*Power(t2,2)) + 
               Power(t1,2)*(-121 + 340*t2 - 308*Power(t2,2) + 
                  89*Power(t2,3)) + 
               s2*(-(Power(-1 + t2,2)*(-17 + 34*t2 + 14*Power(t2,2))) + 
                  2*Power(t1,2)*(79 - 184*t2 + 102*Power(t2,2)) + 
                  t1*(118 - 322*t2 + 272*Power(t2,2) - 68*Power(t2,3))) \
+ Power(s2,2)*(t1*(-99 + 251*t2 - 146*Power(t2,2)) + 
                  3*(-7 + 18*t2 - 12*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,3)*(Power(s2,3)*
                (15 - 88*t2 + 113*Power(t2,2) - 30*Power(t2,3)) + 
               2*Power(-1 + t2,3)*
                (11 - 100*t2 + 43*Power(t2,2) + Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (3 + 163*t2 - 205*Power(t2,2) + 31*Power(t2,3)) + 
               Power(t1,3)*(-133 + 453*t2 - 469*Power(t2,2) + 
                  139*Power(t2,3)) - 
               2*Power(t1,2)*
                (121 - 494*t2 + 659*Power(t2,2) - 355*Power(t2,3) + 
                  69*Power(t2,4)) + 
               Power(s2,2)*(-35 + 142*t2 - 79*Power(t2,2) - 
                  58*Power(t2,3) + 30*Power(t2,4) + 
                  t1*(-77 + 390*t2 - 475*Power(t2,2) + 132*Power(t2,3))) \
+ s2*(Power(t1,2)*(179 - 707*t2 + 783*Power(t2,2) - 225*Power(t2,3)) + 
                  2*Power(-1 + t2,2)*
                   (7 - 81*t2 + 69*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(150 - 650*t2 + 719*Power(t2,2) - 228*Power(t2,3) + 
                     9*Power(t2,4)))) + 
            Power(s1,2)*(Power(s2,3)*t2*
                (-50 + 117*t2 - 80*Power(t2,2) + 14*Power(t2,3)) + 
               Power(t1,3)*(-121 + 528*t2 - 807*Power(t2,2) + 
                  486*Power(t2,3) - 87*Power(t2,4)) - 
               Power(-1 + t2,3)*
                (-33 + 164*t2 - 469*Power(t2,2) + 197*Power(t2,3) + 
                  4*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (-30 + 186*t2 + 61*Power(t2,2) - 213*Power(t2,3) + 
                  36*Power(t2,4)) + 
               3*Power(t1,2)*
                (-47 + 366*t2 - 731*Power(t2,2) + 592*Power(t2,3) - 
                  207*Power(t2,4) + 27*Power(t2,5)) + 
               Power(s2,2)*(-2 + 66*t2 + 107*Power(t2,2) - 
                  416*Power(t2,3) + 296*Power(t2,4) - 51*Power(t2,5) + 
                  t1*(-23 + 246*t2 - 495*Power(t2,2) + 
                     308*Power(t2,3) - 39*Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (-17 + 52*t2 + 252*Power(t2,2) - 250*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  Power(t1,2)*
                   (50 - 387*t2 + 738*Power(t2,2) - 455*Power(t2,3) + 
                     57*Power(t2,4)) + 
                  t1*(153 - 911*t2 + 1147*Power(t2,2) - 
                     161*Power(t2,3) - 318*Power(t2,4) + 90*Power(t2,5)))\
) + s1*(Power(s2,3)*Power(t2,2)*
                (13 - 14*t2 + Power(t2,2) - 8*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (7 - 143*t2 + 302*Power(t2,2) - 390*Power(t2,3) + 
                  170*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,3)*(-60 + 331*t2 - 639*Power(t2,2) + 
                  555*Power(t2,3) - 200*Power(t2,4) + 21*Power(t2,5)) + 
               t1*Power(-1 + t2,2)*
                (24 + 158*t2 + 357*Power(t2,2) - 96*Power(t2,3) - 
                  157*Power(t2,4) + 21*Power(t2,5)) + 
               Power(t1,2)*(58 + 101*t2 - 1018*Power(t2,2) + 
                  1514*Power(t2,3) - 838*Power(t2,4) + 197*Power(t2,5) - 
                  14*Power(t2,6)) + 
               Power(s2,2)*t2*
                (47 + 151*t2 - 883*Power(t2,2) + 1000*Power(t2,3) - 
                  346*Power(t2,4) + 31*Power(t2,5) + 
                  t1*(43 - 190*t2 + 231*Power(t2,2) - 71*Power(t2,3) + 
                     11*Power(t2,4))) + 
               s2*(2*Power(t1,2)*
                   (16 - 62*t2 + 93*Power(t2,2) - 51*Power(t2,3) - 
                     20*Power(t2,4) + 12*Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (-11 + 32*t2 + 406*Power(t2,2) + 57*Power(t2,3) - 
                     191*Power(t2,4) + 14*Power(t2,5)) - 
                  t1*(53 + 414*t2 - 1552*Power(t2,2) + 1091*Power(t2,3) + 
                     307*Power(t2,4) - 379*Power(t2,5) + 66*Power(t2,6))))\
))*T3q(1 - s + s1 - t2,s1))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*(-1 + t1)*Power(-1 + s + t2,2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) - 
    (8*((-1 + s1)*Power(s1 - t2,4)*(-1 + t2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         Power(s,10)*(Power(t1,3) + t1*Power(t2,2) + 2*Power(t2,3)) + 
         Power(s,9)*(Power(t1,3)*(-6 - 9*s1 + 6*t2) + 
            t1*t2*(3 + s1*(-1 + 3*s2 - 8*t2) + 8*Power(t2,2) - 
               s2*(3 + t2)) + 
            Power(t1,2)*(3 + s2*t2 + s1*(-3 + 3*s2 + t2)) + 
            Power(t2,2)*(4 + 2*s1*(1 + 2*s2 - 6*t2) - 2*t2 + 
               10*Power(t2,2) - s2*(3 + 4*t2))) + 
         Power(s,8)*(Power(t1,3)*
             (12 + 36*Power(s1,2) + s1*(43 - 48*t2) - 34*t2 + 
               15*Power(t2,2)) - 
            Power(t1,2)*(15 + (-13 + s2)*t2 - 
               3*(1 + 2*s2)*Power(t2,2) + 
               Power(s1,2)*(-22 + 24*s2 + 8*t2) + 
               s1*(9 - 11*s2*(-1 + t2) + 27*t2 - 6*Power(t2,2))) + 
            t1*(3 + (-15 + 14*s2)*t2 + (8 - 19*s2)*Power(t2,2) - 
               2*(5 + 4*s2)*Power(t2,3) + 25*Power(t2,4) + 
               Power(s1,2)*(1 + 3*Power(s2,2) + 2*t2 + 
                  28*Power(t2,2) - s2*(3 + 19*t2)) - 
               s1*(3 + 2*t2 + Power(s2,2)*t2 + 3*Power(t2,2) + 
                  54*Power(t2,3) - s2*(3 + 11*t2 + 27*Power(t2,2)))) + 
            t2*(3 - 14*t2 + 9*Power(t2,2) + Power(t2,3) + 
               20*Power(t2,4) + Power(s2,2)*t2*(1 + 2*t2) + 
               Power(s1,2)*(1 + 3*Power(s2,2) + s2*(2 - 23*t2) - 
                  11*t2 + 30*Power(t2,2)) - 
               s2*(3 - 10*t2 + 4*Power(t2,2) + 20*Power(t2,3)) + 
               s1*(2 - 9*t2 + 9*Power(t2,2) - 50*Power(t2,3) - 
                  Power(s2,2)*(3 + 5*t2) + s2*(3 + 2*t2 + 42*Power(t2,2))\
))) + Power(s,7)*(1 - 12*t2 + 10*s2*t2 + 11*Power(t2,2) - 
            s2*Power(t2,2) - 11*Power(s2,2)*Power(t2,2) - 
            29*Power(t2,3) + 48*s2*Power(t2,3) - 
            4*Power(s2,2)*Power(t2,3) - 32*Power(t2,4) + 
            13*s2*Power(t2,4) + 10*Power(s2,2)*Power(t2,4) + 
            32*Power(t2,5) - 40*s2*Power(t2,5) + 20*Power(t2,6) + 
            Power(t1,3)*(-6 + 60*t2 - 79*Power(t2,2) + 20*Power(t2,3)) + 
            Power(t1,2)*(21 - 11*(5 + s2)*t2 + (5 - 6*s2)*Power(t2,2) + 
               (16 + 15*s2)*Power(t2,3)) + 
            t1*(-12 + (26 - 9*s2)*t2 + 
               (-68 + 91*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (3 - 41*s2)*Power(t2,3) - (41 + 25*s2)*Power(t2,4) + 
               40*Power(t2,5)) + 
            Power(s1,3)*(Power(s2,3) - 84*Power(t1,3) + 
               14*Power(t1,2)*(-5 + 2*t2) - 
               Power(s2,2)*(21*t1 + 17*t2) + 
               t1*(-8 + 9*t2 - 56*Power(t2,2)) - 
               5*t2*(1 - 5*t2 + 8*Power(t2,2)) + 
               s2*(1 + 84*Power(t1,2) - 10*t2 + 54*Power(t2,2) + 
                  t1*(20 + 49*t2))) + 
            Power(s1,2)*(1 + 
               (4 - 18*s2 + 9*Power(s2,2) - 2*Power(s2,3))*t2 + 
               (-25 + 38*s2 + 41*Power(s2,2))*Power(t2,2) - 
               (7 + 144*s2)*Power(t2,3) + 100*Power(t2,4) + 
               7*Power(t1,3)*(-19 + 24*t2) + 
               Power(t1,2)*(-11 + s2*(69 - 105*t2) + 171*t2 - 
                  42*Power(t2,2)) + 
               t1*(22 - 23*t2 - 10*Power(t2,2) + 156*Power(t2,3) + 
                  Power(s2,2)*(-7 + 24*t2) - 
                  2*s2*(6 + 15*t2 + 64*Power(t2,2)))) + 
            s1*(Power(t1,3)*(-67 + 212*t2 - 105*Power(t2,2)) + 
               Power(t1,2)*(70 + 24*t2 - 116*Power(t2,2) + 
                  15*Power(t2,3) + s2*(9 - 56*t2 + 9*Power(t2,2))) + 
               t1*(-9 + 33*t2 + Power(s2,2)*(11 - 7*t2)*t2 + 
                  23*Power(t2,2) + 53*Power(t2,3) - 140*Power(t2,4) + 
                  s2*(-7 - 92*t2 + 57*Power(t2,2) + 101*Power(t2,3))) + 
               t2*(-13 + 44*t2 + Power(s2,3)*t2 + 52*Power(t2,2) - 
                  47*Power(t2,3) - 80*Power(t2,4) + 
                  Power(s2,2)*(7 + t2 - 34*Power(t2,2)) + 
                  s2*(9 - 48*t2 - 36*Power(t2,2) + 130*Power(t2,3))))) + 
         Power(s,6)*(-3 + 7*t2 + s2*t2 + 16*Power(t2,2) - 
            27*s2*Power(t2,2) + 22*Power(s2,2)*Power(t2,2) - 
            6*Power(t2,3) - 14*s2*Power(t2,3) - 
            45*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) + 
            25*Power(t2,4) + 126*s2*Power(t2,4) - 
            28*Power(s2,2)*Power(t2,4) - 136*Power(t2,5) + 
            30*s2*Power(t2,5) + 20*Power(s2,2)*Power(t2,5) + 
            80*Power(t2,6) - 40*s2*Power(t2,6) + 10*Power(t2,7) + 
            Power(t1,3)*(-9 - 17*t2 + 119*Power(t2,2) - 
               96*Power(t2,3) + 15*Power(t2,4)) + 
            Power(t1,2)*(3 + (35 + 29*s2)*t2 - 
               30*(1 + 2*s2)*Power(t2,2) - 11*(6 + s2)*Power(t2,3) + 
               4*(9 + 5*s2)*Power(t2,4)) + 
            t1*(9 - 2*(3 + 17*s2)*t2 + 
               (50 - 40*s2 - 6*Power(s2,2))*Power(t2,2) + 
               2*(-67 + 129*s2 + 2*Power(s2,2))*Power(t2,3) - 
               (25 + 41*s2)*Power(t2,4) - 4*(17 + 10*s2)*Power(t2,5) + 
               35*Power(t2,6)) + 
            Power(s1,4)*(-6*Power(s2,3) + 126*Power(t1,3) - 
               14*Power(t1,2)*(-9 + 4*t2) + 
               Power(s2,2)*(1 + 63*t1 + 39*t2) + 
               10*t2*(1 - 3*t2 + 3*Power(t2,2)) + 
               5*t1*(5 - 8*t2 + 14*Power(t2,2)) - 
               s2*(5 + 168*Power(t1,2) - 20*t2 + 65*Power(t2,2) + 
                  t1*(57 + 63*t2))) + 
            Power(s1,3)*(-1 + Power(t1,3)*(231 - 336*t2) - 45*t2 + 
               121*Power(t2,2) - 26*Power(t2,3) - 100*Power(t2,4) + 
               2*Power(s2,3)*(-1 + 8*t2) + 
               Power(t1,2)*(88 - 459*t2 + 126*Power(t2,2)) + 
               t1*(-55 + 29*t2 + 96*Power(t2,2) - 250*Power(t2,3)) - 
               Power(s2,2)*(1 + 9*t2 + 123*Power(t2,2) + 
                  t1*(-38 + 123*t2)) + 
               s2*(3 + 43*t2 - 109*Power(t2,2) + 230*Power(t2,3) + 
                  Power(t1,2)*(-183 + 343*t2) + 
                  t1*(9 + 110*t2 + 257*Power(t2,2)))) - 
            Power(s1,2)*(-4 - 19*t2 - 47*Power(t2,2) + 
               375*Power(t2,3) - 210*Power(t2,4) - 120*Power(t2,5) + 
               2*Power(s2,3)*t2*(-4 + 7*t2) + 
               Power(t1,3)*(-156 + 559*t2 - 315*Power(t2,2)) + 
               Power(t1,2)*(103 + 345*t2 - 570*Power(t2,2) + 
                  90*Power(t2,3)) + 
               t1*(5 - 32*t2 + 121*Power(t2,2) + 179*Power(t2,3) - 
                  325*Power(t2,4)) + 
               Power(s2,2)*(t2*(37 + 39*t2 - 149*Power(t2,2)) + 
                  t1*(-4 + 96*t2 - 79*Power(t2,2))) + 
               s2*(1 + 30*t2 - 105*Power(t2,2) - 173*Power(t2,3) + 
                  305*Power(t2,4) + 
                  Power(t1,2)*(43 - 321*t2 + 180*Power(t2,2)) + 
                  t1*(-14 - 299*t2 + 93*Power(t2,2) + 349*Power(t2,3)))) \
+ s1*(-6 + 26*t2 - 28*Power(t2,2) + 
               4*Power(s2,3)*(-2 + t2)*Power(t2,2) - 22*Power(t2,3) + 
               381*Power(t2,4) - 234*Power(t2,5) - 60*Power(t2,6) + 
               Power(t1,3)*(27 - 276*t2 + 418*Power(t2,2) - 
                  120*Power(t2,3)) + 
               Power(t1,2)*(-87 + 145*t2 + 315*Power(t2,2) - 
                  262*Power(t2,3) + 20*Power(t2,4)) + 
               t1*(47 - 102*t2 + 187*Power(t2,2) + 76*Power(t2,3) + 
                  190*Power(t2,4) - 180*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-4 + 81*t2 + 75*Power(t2,2) - 85*Power(t2,3) + 
                  t1*(-16 + 56*t2 - 19*Power(t2,2))) + 
               s2*(1 - 38*t2 + 45*Power(t2,2) - 277*Power(t2,3) - 
                  114*Power(t2,4) + 180*Power(t2,5) + 
                  Power(t1,2)*
                   (1 + 77*t2 - 111*Power(t2,2) - 15*Power(t2,3)) + 
                  t1*(2 + 118*t2 - 586*Power(t2,2) + 89*Power(t2,3) + 
                     195*Power(t2,4))))) + 
         s*Power(s1 - t2,3)*(Power(s1,6)*Power(s2 - t1,2)*
             (-1 + s2 - t1 + t2) - 
            Power(s1,5)*(s2 - t1)*
             (s2*(t1*(2 - 6*t2) + 4*Power(-1 + t2,2)) - 
               Power(-1 + t2,2) - 3*t1*Power(-1 + t2,2) + 
               Power(s2,2)*(-1 + 3*t2) + Power(t1,2)*(-1 + 3*t2)) + 
            (-1 + t2)*(-7 + (35 - 17*s2)*t2 + 
               (-52 + 42*s2 - 11*Power(s2,2))*Power(t2,2) - 
               (-25 + 15*s2 - 3*Power(s2,2) + Power(s2,3))*
                Power(t2,3) + 
               (1 - 14*s2 + 15*Power(s2,2) - 2*Power(s2,3))*
                Power(t2,4) - 2*Power(-1 + s2,2)*Power(t2,5) + 
               Power(t1,3)*(7 - 11*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-21 + (57 - 17*s2)*t2 + 
                  (-43 + 22*s2)*Power(t2,2) - 6*(-2 + s2)*Power(t2,3)) \
+ t1*(21 + (-81 + 34*s2)*t2 + 
                  (92 - 64*s2 + 11*Power(s2,2))*Power(t2,2) + 
                  (-39 + 30*s2 - 9*Power(s2,2))*Power(t2,3) + 
                  (7 - 10*s2 + 3*Power(s2,2))*Power(t2,4))) + 
            Power(s1,4)*(2*Power(s2,3)*(2 - 4*t2 + Power(t2,2)) + 
               s2*(-(Power(-1 + t2,2)*(-2 + 3*t2)) + 
                  Power(t1,2)*(17 - 32*t2 + 9*Power(t2,2)) + 
                  t1*(23 - 43*t2 + 29*Power(t2,2) - 9*Power(t2,3))) + 
               Power(s2,2)*(t1*(-15 + 29*t2 - 8*Power(t2,2)) + 
                  3*(-3 + 6*t2 - 5*Power(t2,2) + 2*Power(t2,3))) + 
               t1*(Power(-1 + t2,2)*(-2 + 3*t2) + 
                  Power(t1,2)*(-6 + 11*t2 - 3*Power(t2,2)) + 
                  t1*(-14 + 25*t2 - 14*Power(t2,2) + 3*Power(t2,3)))) + 
            Power(s1,3)*(-Power(-1 + t2,3) - 
               t1*Power(-1 + t2,2)*(15 - 16*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(19 - 21*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(-1 - 13*t2 + 20*Power(t2,2) + 
                  2*Power(t2,3)) - 
               Power(t1,2)*(7 - 54*t2 + 55*Power(t2,2) - 
                  9*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(-28 + 90*t2 - 67*Power(t2,2) + 
                  9*Power(t2,3) - 4*Power(t2,4) + 
                  t1*(22 + 6*t2 - 52*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(7 - 8*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (-40 + 28*t2 + 39*Power(t2,2) - 3*Power(t2,3)) + 
                  t1*(42 - 165*t2 + 143*Power(t2,2) - 25*Power(t2,3) + 
                     5*Power(t2,4)))) + 
            s1*(2*Power(-1 + t2,3)*(-5 + 4*Power(t2,2)) + 
               Power(t1,3)*(14 - 26*t2 + 15*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (-3 - 7*t2 + 11*Power(t2,2) + Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-6 - 8*t2 - 19*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(-18 + 52*t2 - 34*Power(t2,2) + 
                  11*Power(t2,3) - 11*Power(t2,4)) - 
               Power(s2,2)*t2*
                (22 - 96*Power(t2,2) + 77*Power(t2,3) - 
                  3*Power(t2,4) + 
                  t1*(-22 + 18*t2 + 9*Power(t2,3) + Power(t2,4))) + 
               s2*(-(Power(-1 + t2,2)*
                     (17 - 11*t2 - 49*Power(t2,2) + 12*Power(t2,3))) + 
                  Power(t1,2)*
                   (-17 + 5*t2 + 21*Power(t2,2) - 9*Power(t2,3) + 
                     6*Power(t2,4)) - 
                  t1*(-34 + 50*t2 + 4*Power(t2,2) + 33*Power(t2,3) - 
                     54*Power(t2,4) + Power(t2,5)))) + 
            Power(s1,2)*(-(Power(-1 + t2,3)*(1 + 5*t2)) + 
               Power(s2,3)*t2*
                (3 + 15*t2 - 22*Power(t2,2) - 3*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-5 + 39*t2 - 16*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(-18 + 15*t2 + 9*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(22 - 60*t2 + 6*Power(t2,2) + 
                  32*Power(t2,3)) + 
               Power(s2,2)*(11 + 42*t2 - 165*Power(t2,2) + 
                  113*Power(t2,3) - 2*Power(t2,4) + Power(t2,5) + 
                  t1*(-11 - 24*t2 + 21*Power(t2,2) + 32*Power(t2,3) + 
                     3*Power(t2,4))) - 
               s2*(Power(-1 + t2,2)*
                   (-14 + 46*t2 - 14*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-34 + 9*t2 + 30*Power(t2,2) + 16*Power(t2,3)) + 
                  t1*(48 - 56*t2 - 135*Power(t2,2) + 151*Power(t2,3) - 
                     9*Power(t2,4) + Power(t2,5))))) + 
         Power(s,5)*(Power(t1,3)*
             (12 - 53*t2 - 3*Power(t2,2) + 110*Power(t2,3) - 
               64*Power(t2,4) + 6*Power(t2,5)) + 
            Power(t1,2)*(-24 + (100 - 26*s2)*t2 + 
               3*(-40 + 47*s2)*Power(t2,2) + 
               (130 - 131*s2)*Power(t2,3) - (155 + 4*s2)*Power(t2,4) + 
               (44 + 15*s2)*Power(t2,5)) + 
            t1*(12 + (-68 + 48*s2)*t2 + 
               3*(68 - 67*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (149 - 210*s2 + 2*Power(s2,2))*Power(t2,3) + 
               (-183 + 456*s2 - 7*Power(s2,2))*Power(t2,4) - 
               (45 + 29*s2)*Power(t2,5) - (62 + 35*s2)*Power(t2,6) + 
               16*Power(t2,7)) + 
            t2*(21 - 87*t2 + 23*Power(t2,2) - 16*Power(t2,3) + 
               96*Power(t2,4) - 188*Power(t2,5) + 90*Power(t2,6) + 
               2*Power(t2,7) + 2*Power(s2,3)*Power(t2,2)*(-3 + 5*t2) + 
               Power(s2,2)*t2*
                (-8 + 90*t2 - 93*Power(t2,2) - 50*Power(t2,3) + 
                  20*Power(t2,4)) + 
               s2*(-22 + 75*t2 - 57*Power(t2,2) - 123*Power(t2,3) + 
                  211*Power(t2,4) + 15*Power(t2,5) - 20*Power(t2,6))) + 
            Power(s1,5)*(15*Power(s2,3) - 126*Power(t1,3) + 
               70*Power(t1,2)*(-2 + t2) - 
               5*Power(s2,2)*(1 + 21*t1 + 9*t2) + 
               t1*(-40 + 65*t2 - 56*Power(t2,2)) - 
               2*t2*(5 - 10*t2 + 6*Power(t2,2)) + 
               5*s2*(2 + 42*Power(t1,2) - 4*t2 + 8*Power(t2,2) + 
                  t1*(18 + 7*t2))) + 
            Power(s1,4)*(-6 + Power(s2,3)*(9 - 50*t2) + 101*t2 - 
               194*Power(t2,2) + 64*Power(t2,3) + 50*Power(t2,4) + 
               35*Power(t1,3)*(-7 + 12*t2) - 
               5*Power(t1,2)*(34 - 135*t2 + 42*Power(t2,2)) + 
               t1*(59 + 51*t2 - 225*Power(t2,2) + 240*Power(t2,3)) + 
               Power(s2,2)*(5*t1*(-17 + 58*t2) + t2*(17 + 175*t2)) - 
               s2*(5*Power(t1,2)*(-53 + 119*t2) + 
                  t1*(-31 + 275*t2 + 240*Power(t2,2)) + 
                  2*(5 + 27*t2 - 61*Power(t2,2) + 90*Power(t2,3)))) + 
            Power(s1,3)*(-7 + 11*t2 - 331*Power(t2,2) + 
               797*Power(t2,3) - 384*Power(t2,4) - 80*Power(t2,5) + 
               Power(t1,3)*(-196 + 806*t2 - 525*Power(t2,2)) + 
               Power(s2,3)*(2 - 43*t2 + 60*Power(t2,2)) + 
               Power(t1,2)*(-2 + 914*t2 - 1215*Power(t2,2) + 
                  225*Power(t2,3)) + 
               t1*(29 - 112*t2 + 65*Power(t2,2) + 392*Power(t2,3) - 
                  400*Power(t2,4)) + 
               Power(s2,2)*(-9 + 95*t2 + 82*Power(t2,2) - 
                  275*Power(t2,3) - 2*t1*(9 - 150*t2 + 145*Power(t2,2))\
) + s2*(-2 + 93*t2 - 144*Power(t2,2) - 250*Power(t2,3) + 
                  320*Power(t2,4) + 
                  Power(t1,2)*(87 - 733*t2 + 555*Power(t2,2)) + 
                  t1*(36 - 614*t2 + 259*Power(t2,2) + 510*Power(t2,3)))) \
- Power(s1,2)*(-5 + 4*t2 + 16*Power(t2,2) - 461*Power(t2,3) + 
               1164*Power(t2,4) - 586*Power(t2,5) - 60*Power(t2,6) + 
               Power(s2,3)*t2*(10 - 69*t2 + 30*Power(t2,2)) + 
               Power(t1,3)*(45 - 501*t2 + 909*Power(t2,2) - 
                  300*Power(t2,3)) + 
               Power(t1,2)*(-110 - 105*t2 + 1431*Power(t2,2) - 
                  958*Power(t2,3) + 100*Power(t2,4)) + 
               t1*(76 - 162*t2 + 214*Power(t2,2) + 123*Power(t2,3) + 
                  427*Power(t2,4) - 320*Power(t2,5)) + 
               Power(s2,2)*(t2*
                   (-64 + 283*t2 + 245*Power(t2,2) - 225*Power(t2,3)) \
+ t1*(2 - 82*t2 + 352*Power(t2,2) - 130*Power(t2,3))) + 
               s2*(8 - 65*t2 + 260*Power(t2,2) - 655*Power(t2,3) - 
                  229*Power(t2,4) + 280*Power(t2,5) + 
                  Power(t1,2)*
                   (12 + 181*t2 - 588*Power(t2,2) + 150*Power(t2,3)) + 
                  t1*(-35 + 524*t2 - 1651*Power(t2,2) + 
                     116*Power(t2,3) + 475*Power(t2,4)))) + 
            s1*(8 + 10*t2 - 20*Power(t2,2) + 19*Power(t2,3) - 
               332*Power(t2,4) + 759*Power(t2,5) - 376*Power(t2,6) - 
               20*Power(t2,7) + 
               Power(s2,3)*Power(t2,2)*(14 - 45*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(27 + 60*t2 - 429*Power(t2,2) + 
                  416*Power(t2,3) - 75*Power(t2,4)) + 
               Power(t1,2)*(-19 - 86*t2 - 213*Power(t2,2) + 
                  826*Power(t2,3) - 327*Power(t2,4) + 15*Power(t2,5)) + 
               t1*(-16 + 28*t2 - 338*Power(t2,2) + 470*Power(t2,3) + 
                  102*Power(t2,4) + 257*Power(t2,5) - 120*Power(t2,6)) + 
               Power(s2,2)*t2*
                (8 - 145*t2 + 281*Power(t2,2) + 201*Power(t2,3) - 
                  100*Power(t2,4) - 
                  t1*(4 + 66*t2 - 144*Power(t2,2) + 25*Power(t2,3))) + 
               s2*(2 - 17*t2 - 6*Power(t2,2) + 290*Power(t2,3) - 
                  678*Power(t2,4) - 96*Power(t2,5) + 120*Power(t2,6) + 
                  Power(t1,2)*
                   (6 - 79*t2 + 225*Power(t2,2) - 126*Power(t2,3) - 
                     35*Power(t2,4)) + 
                  t1*(-8 + 66*t2 + 698*Power(t2,2) - 1504*Power(t2,3) + 
                     71*Power(t2,4) + 205*Power(t2,5))))) + 
         Power(s,3)*(s1 - t2)*
          (-13 + 39*t1 - 39*Power(t1,2) + 13*Power(t1,3) + 94*t2 - 
            27*s2*t2 - 270*t1*t2 + 54*s2*t1*t2 + 258*Power(t1,2)*t2 - 
            27*s2*Power(t1,2)*t2 - 82*Power(t1,3)*t2 - 6*Power(t2,2) + 
            15*s2*Power(t2,2) + 5*Power(s2,2)*Power(t2,2) + 
            211*t1*Power(t2,2) - 90*s2*t1*Power(t2,2) - 
            5*Power(s2,2)*t1*Power(t2,2) - 340*Power(t1,2)*Power(t2,2) + 
            75*s2*Power(t1,2)*Power(t2,2) + 
            135*Power(t1,3)*Power(t2,2) - 335*Power(t2,3) + 
            186*s2*Power(t2,3) + 6*Power(s2,2)*Power(t2,3) - 
            5*Power(s2,3)*Power(t2,3) + 409*t1*Power(t2,3) - 
            239*s2*t1*Power(t2,3) + 42*Power(s2,2)*t1*Power(t2,3) + 
            114*Power(t1,2)*Power(t2,3) - 
            71*s2*Power(t1,2)*Power(t2,3) - 88*Power(t1,3)*Power(t2,3) + 
            347*Power(t2,4) - 222*s2*Power(t2,4) - 
            154*Power(s2,2)*Power(t2,4) + 29*Power(s2,3)*Power(t2,4) - 
            603*t1*Power(t2,4) + 637*s2*t1*Power(t2,4) - 
            90*Power(s2,2)*t1*Power(t2,4) - 69*Power(t1,2)*Power(t2,4) + 
            60*s2*Power(t1,2)*Power(t2,4) + 10*Power(t1,3)*Power(t2,4) - 
            108*Power(t2,5) + 146*s2*Power(t2,5) + 
            135*Power(s2,2)*Power(t2,5) - 20*Power(s2,3)*Power(t2,5) + 
            206*t1*Power(t2,5) - 363*s2*t1*Power(t2,5) + 
            32*Power(s2,2)*t1*Power(t2,5) + 69*Power(t1,2)*Power(t2,5) - 
            10*s2*Power(t1,2)*Power(t2,5) + 3*Power(t1,3)*Power(t2,5) + 
            5*Power(t2,6) - 93*s2*Power(t2,6) + 
            4*Power(s2,2)*Power(t2,6) - 23*t1*Power(t2,6) + 
            23*s2*t1*Power(t2,6) - 12*Power(t1,2)*Power(t2,6) - 
            s2*Power(t1,2)*Power(t2,6) + 32*Power(t2,7) + 
            9*s2*Power(t2,7) - 2*Power(s2,2)*Power(t2,7) + 
            17*t1*Power(t2,7) + 3*s2*t1*Power(t2,7) - 16*Power(t2,8) + 
            Power(s1,6)*(15*Power(s2,3) - 36*Power(t1,3) + 
               (-1 + t2)*t2 + 14*Power(t1,2)*(-3 + 2*t2) - 
               Power(s2,2)*(10 + 63*t1 + 3*t2) + 
               t1*(-16 + 23*t2 - 8*Power(t2,2)) + 
               s2*(5 + 84*Power(t1,2) + t1*(48 - 21*t2) - 2*t2 - 
                  2*Power(t2,2))) + 
            Power(s1,5)*(-11 + Power(s2,3)*(14 - 55*t2) + 51*t2 - 
               66*Power(t2,2) + 26*Power(t2,3) + 
               3*Power(t1,3)*(-21 + 44*t2) + 
               Power(t1,2)*(-91 + 255*t2 - 98*Power(t2,2)) + 
               t1*(-8 + 87*t2 - 109*Power(t2,2) + 36*Power(t2,3)) + 
               Power(s2,2)*(-20 + 65*t2 + 12*Power(t2,2) + 
                  t1*(-65 + 213*t2)) + 
               s2*(-3 + Power(t1,2)*(111 - 287*t2) - 17*t2 + 
                  8*Power(t2,2) + 6*Power(t2,3) + 
                  t1*(78 - 248*t2 + 47*Power(t2,2)))) + 
            Power(s1,4)*(3 + 51*t2 - 248*Power(t2,2) + 
               321*Power(t2,3) - 127*Power(t2,4) + 
               Power(t1,3)*(-67 + 281*t2 - 183*Power(t2,2)) + 
               2*Power(s2,3)*(5 - 38*t2 + 35*Power(t2,2)) + 
               Power(t1,2)*(-182 + 665*t2 - 603*Power(t2,2) + 
                  127*Power(t2,3)) + 
               t1*(-22 + 77*t2 - 191*Power(t2,2) + 208*Power(t2,3) - 
                  64*Power(t2,4)) - 
               Power(s2,2)*(54 - 164*t2 + 88*Power(t2,2) + 
                  20*Power(t2,3) + t1*(43 - 322*t2 + 262*Power(t2,2))) \
+ s2*(-8 + 63*t2 - 47*Power(t2,2) - 12*Power(t2,3) - 4*Power(t2,4) + 
                  Power(t1,2)*(79 - 483*t2 + 352*Power(t2,2)) + 
                  t1*(184 - 608*t2 + 415*Power(t2,2)))) - 
            Power(s1,3)*(7 - 12*t2 + 125*Power(t2,2) - 
               482*Power(t2,3) + 593*Power(t2,4) - 231*Power(t2,5) + 
               Power(s2,3)*(-5 + 59*t2 - 164*Power(t2,2) + 
                  30*Power(t2,3)) - 
               Power(t1,3)*(16 + 119*t2 - 384*Power(t2,2) + 
                  117*Power(t2,3)) + 
               Power(t1,2)*(64 - 708*t2 + 1426*Power(t2,2) - 
                  625*Power(t2,3) + 73*Power(t2,4)) + 
               t1*(45 - 280*t2 + 419*Power(t2,2) - 271*Power(t2,3) + 
                  213*Power(t2,4) - 56*Power(t2,5)) + 
               Power(s2,2)*(60 - 409*t2 + 559*Power(t2,2) + 
                  30*Power(t2,3) - 20*Power(t2,4) - 
                  2*t1*(6 + 63*t2 - 278*Power(t2,2) + 69*Power(t2,3))) \
+ s2*(-21 + 27*t2 + 249*Power(t2,2) - 330*Power(t2,3) + Power(t2,4) + 
                  4*Power(t2,5) + 
                  Power(t1,2)*
                   (76 + 34*t2 - 615*Power(t2,2) + 173*Power(t2,3)) + 
                  t1*(-179 + 1179*t2 - 1829*Power(t2,2) + 
                     304*Power(t2,3) + 75*Power(t2,4)))) + 
            Power(s1,2)*(39 - 128*t2 + 160*Power(t2,2) + 
               60*Power(t2,3) - 449*Power(t2,4) + 524*Power(t2,5) - 
               206*Power(t2,6) - 
               Power(s2,3)*t2*
                (15 - 117*t2 + 176*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,3)*(3 + 14*t2 - 134*Power(t2,2) + 
                  216*Power(t2,3) - 33*Power(t2,4)) + 
               Power(t1,2)*(-31 + 167*t2 - 879*Power(t2,2) + 
                  1245*Power(t2,3) - 293*Power(t2,4) + 17*Power(t2,5)) \
+ t1*(-11 + 247*t2 - 896*Power(t2,2) + 827*Power(t2,3) - 
                  200*Power(t2,4) + 140*Power(t2,5) - 24*Power(t2,6)) + 
               Power(s2,2)*(5 + 126*t2 - 810*Power(t2,2) + 
                  841*Power(t2,3) + 118*Power(t2,4) - 15*Power(t2,5) + 
                  t1*(-5 + 18*t2 - 213*Power(t2,2) + 438*Power(t2,3) - 
                     27*Power(t2,4))) + 
               s2*(-48 + 99*t2 + 9*Power(t2,2) + 395*Power(t2,3) - 
                  569*Power(t2,4) + 25*Power(t2,5) + 6*Power(t2,6) + 
                  Power(t1,2)*
                   (12 + 36*t2 + 44*Power(t2,2) - 307*Power(t2,3) + 
                     17*Power(t2,4)) + 
                  t1*(36 - 507*t2 + 2137*Power(t2,2) - 
                     2365*Power(t2,3) + 138*Power(t2,4) + 70*Power(t2,5)\
))) + s1*(-38 - 85*t2 + 454*Power(t2,2) - 493*Power(t2,3) + 
               129*Power(t2,4) + 159*Power(t2,5) - 217*Power(t2,6) + 
               91*Power(t2,7) + 
               Power(s2,3)*Power(t2,2)*
                (15 - 97*t2 + 94*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,3)*(26 - 86*t2 + 74*Power(t2,2) + 
                  43*Power(t2,3) - 49*Power(t2,4) + 3*Power(t2,5)) - 
               Power(t1,2)*(90 - 215*t2 + 265*Power(t2,2) - 
                  509*Power(t2,3) + 474*Power(t2,4) - 70*Power(t2,5) + 
                  Power(t2,6)) + 
               t1*(102 - 44*t2 - 563*Power(t2,2) + 1154*Power(t2,3) - 
                  671*Power(t2,4) + 72*Power(t2,5) - 66*Power(t2,6) + 
                  4*Power(t2,7)) + 
               Power(s2,2)*t2*
                (-10 - 72*t2 + 609*Power(t2,2) - 561*Power(t2,3) - 
                  59*Power(t2,4) + 8*Power(t2,5) + 
                  t1*(10 - 72*t2 + 220*Power(t2,2) - 171*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(27 + 33*t2 - 306*Power(t2,2) + 248*Power(t2,3) - 
                  352*Power(t2,4) + 391*Power(t2,5) - 27*Power(t2,6) - 
                  2*Power(t2,7) + 
                  Power(t1,2)*
                   (27 - 87*t2 + 111*Power(t2,2) - 149*Power(t2,3) + 
                     74*Power(t2,4) + 8*Power(t2,5)) + 
                  t1*(-54 + 54*t2 + 567*Power(t2,2) - 1779*Power(t2,3) + 
                     1429*Power(t2,4) - 72*Power(t2,5) - 24*Power(t2,6)))\
)) - Power(s,2)*Power(s1 - t2,2)*
          (-15 + 45*t1 - 45*Power(t1,2) + 15*Power(t1,3) - 10*t2 + 
            7*s2*t2 - 18*t1*t2 - 14*s2*t1*t2 + 66*Power(t1,2)*t2 + 
            7*s2*Power(t1,2)*t2 - 38*Power(t1,3)*t2 + 189*Power(t2,2) - 
            111*s2*Power(t2,2) + 15*Power(s2,2)*Power(t2,2) - 
            280*t1*Power(t2,2) + 154*s2*t1*Power(t2,2) - 
            15*Power(s2,2)*t1*Power(t2,2) + 45*Power(t1,2)*Power(t2,2) - 
            43*s2*Power(t1,2)*Power(t2,2) + 46*Power(t1,3)*Power(t2,2) - 
            317*Power(t2,3) + 201*s2*Power(t2,3) - 
            10*Power(s2,2)*Power(t2,3) - 3*Power(s2,3)*Power(t2,3) + 
            495*t1*Power(t2,3) - 321*s2*t1*Power(t2,3) + 
            48*Power(s2,2)*t1*Power(t2,3) - 
            106*Power(t1,2)*Power(t2,3) + 
            48*s2*Power(t1,2)*Power(t2,3) - 33*Power(t1,3)*Power(t2,3) + 
            187*Power(t2,4) - 93*s2*Power(t2,4) - 
            78*Power(s2,2)*Power(t2,4) + 15*Power(s2,3)*Power(t2,4) - 
            317*t1*Power(t2,4) + 300*s2*t1*Power(t2,4) - 
            56*Power(s2,2)*t1*Power(t2,4) + 26*Power(t1,2)*Power(t2,4) - 
            s2*Power(t1,2)*Power(t2,4) + 7*Power(t1,3)*Power(t2,4) - 
            41*Power(t2,5) + 3*s2*Power(t2,5) + 
            75*Power(s2,2)*Power(t2,5) - 10*Power(s2,3)*Power(t2,5) + 
            84*t1*Power(t2,5) - 118*s2*t1*Power(t2,5) + 
            16*Power(s2,2)*t1*Power(t2,5) + 12*Power(t1,2)*Power(t2,5) - 
            3*s2*Power(t1,2)*Power(t2,5) + 7*Power(t2,6) - 
            9*s2*Power(t2,6) - 6*Power(s2,2)*Power(t2,6) - 
            13*t1*Power(t2,6) + 7*s2*t1*Power(t2,6) - 
            2*Power(t1,2)*Power(t2,6) + 2*Power(t2,7) + 
            2*s2*Power(t2,7) + 4*t1*Power(t2,7) - 2*Power(t2,8) + 
            Power(s1,6)*(6*Power(s2,3) + 
               Power(s2,2)*(-5 - 21*t1 + 3*t2) + 
               s2*(1 + 24*Power(t1,2) + t1*(15 - 11*t2) - 
                  Power(t2,2)) - 
               t1*(3 + 9*Power(t1,2) + t1*(10 - 8*t2) - 4*t2 + 
                  Power(t2,2))) + 
            Power(s1,5)*(Power(s2,3)*(6 - 20*t2) + 
               Power(-1 + t2,2)*(-3 + 4*t2) + 
               Power(t1,3)*(-13 + 30*t2) + 
               Power(t1,2)*(-26 + 61*t2 - 26*Power(t2,2)) + 
               t1*(-7 + 25*t2 - 22*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(-15 + 37*t2 - 13*Power(t2,2) + 
                  t1*(-22 + 67*t2)) + 
               s2*(2 + Power(t1,2)*(29 - 77*t2) - 7*t2 + Power(t2,2) + 
                  4*Power(t2,3) + t1*(37 - 90*t2 + 35*Power(t2,2)))) + 
            Power(s1,4)*(Power(t1,3)*(-22 + 71*t2 - 36*Power(t2,2)) - 
               Power(-1 + t2,2)*(-1 - 16*t2 + 18*Power(t2,2)) + 
               Power(s2,3)*(8 - 34*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(-83 + 203*t2 - 144*Power(t2,2) + 
                  30*Power(t2,3)) + 
               t1*(-16 + 48*t2 - 69*Power(t2,2) + 43*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,2)*(-36 + 86*t2 - 66*Power(t2,2) + 
                  22*Power(t2,3) + t1*(-33 + 130*t2 - 72*Power(t2,2))) \
+ s2*(1 - 2*t2 + 10*Power(t2,2) - 3*Power(t2,3) - 6*Power(t2,4) + 
                  Power(t1,2)*(45 - 163*t2 + 86*Power(t2,2)) + 
                  t1*(105 - 247*t2 + 168*Power(t2,2) - 38*Power(t2,3)))) \
+ Power(s1,3)*(Power(-1 + t2,2)*t2*(2 - 29*t2 + 32*Power(t2,2)) + 
               Power(s2,3)*(3 - 39*t2 + 76*Power(t2,2)) + 
               Power(t1,3)*(33 - 20*t2 - 79*Power(t2,2) + 
                  18*Power(t2,3)) + 
               Power(t1,2)*(-36 + 296*t2 - 423*Power(t2,2) + 
                  129*Power(t2,3) - 14*Power(t2,4)) + 
               t1*(-36 + 179*t2 - 234*Power(t2,2) + 127*Power(t2,3) - 
                  40*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s2,2)*(-66 + 295*t2 - 291*Power(t2,2) + 
                  32*Power(t2,3) - 18*Power(t2,4) + 
                  t1*(28 + 46*t2 - 226*Power(t2,2) + 24*Power(t2,3))) + 
               s2*(26 - 59*t2 + Power(t2,2) + 27*Power(t2,3) + 
                  Power(t2,4) + 4*Power(t2,5) + 
                  Power(t1,2)*
                   (-75 + 41*t2 + 206*Power(t2,2) - 36*Power(t2,3)) + 
                  t1*(121 - 641*t2 + 739*Power(t2,2) - 
                     137*Power(t2,3) + 14*Power(t2,4)))) + 
            Power(s1,2)*(-(Power(s2,3)*t2*
                  (9 - 69*t2 + 84*Power(t2,2) + 10*Power(t2,3))) + 
               Power(t1,3)*(-18 + 9*t2 + 12*Power(t2,2) + 
                  31*Power(t2,3) - 3*Power(t2,4)) + 
               Power(t1,2)*t2*
                (9 - 249*t2 + 328*Power(t2,2) - 42*Power(t2,3) + 
                  2*Power(t2,4)) - 
               2*Power(-1 + t2,2)*
                (-8 + 13*t2 + 11*Power(t2,2) - 10*Power(t2,3) + 
                  14*Power(t2,4)) + 
               t1*(2 + 157*t2 - 460*Power(t2,2) + 384*Power(t2,3) - 
                  105*Power(t2,4) + 23*Power(t2,5) - Power(t2,6)) + 
               Power(s2,2)*(15 + 122*t2 - 560*Power(t2,2) + 
                  459*Power(t2,3) + 5*Power(t2,4) + 7*Power(t2,5) + 
                  t1*(-15 - 8*t2 - 49*Power(t2,2) + 166*Power(t2,3) + 
                     5*Power(t2,4))) + 
               s2*(-36 - 38*t2 + 199*Power(t2,2) - 59*Power(t2,3) - 
                  70*Power(t2,4) + 5*Power(t2,5) - Power(t2,6) + 
                  Power(t1,2)*
                   (32 + 11*t2 - 41*Power(t2,2) - 100*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  t1*(4 - 189*t2 + 913*Power(t2,2) - 887*Power(t2,3) + 
                     62*Power(t2,4) + Power(t2,5)))) + 
            s1*(Power(s2,3)*Power(t2,2)*
                (9 - 53*t2 + 46*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(14 - 52*t2 + 57*Power(t2,2) - 
                  16*Power(t2,3) - 4*Power(t2,4)) + 
               Power(t1,2)*(6 + 27*t2 - 65*Power(t2,2) + 
                  127*Power(t2,3) - 112*Power(t2,4) + 8*Power(t2,5)) + 
               Power(-1 + t2,2)*
                (34 - 113*t2 + 49*Power(t2,2) + 14*Power(t2,3) - 
                  2*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(-54 + 206*t2 - 418*Power(t2,2) + 497*Power(t2,3) - 
                  257*Power(t2,4) + 38*Power(t2,5) - 12*Power(t2,6)) - 
               Power(s2,2)*t2*
                (30 + 46*t2 - 379*Power(t2,2) + 314*Power(t2,3) - 
                  3*Power(t2,4) + Power(t2,5) + 
                  t1*(-30 + 68*t2 - 92*Power(t2,2) + 64*Power(t2,3) + 
                     3*Power(t2,4))) + 
               s2*(-7 + 147*t2 - 189*Power(t2,2) - 48*Power(t2,3) + 
                  55*Power(t2,4) + 48*Power(t2,5) - 6*Power(t2,6) + 
                  Power(t1,2)*
                   (-7 + 11*t2 + 16*Power(t2,2) - 44*Power(t2,3) + 
                     31*Power(t2,4) + Power(t2,5)) - 
                  t1*(-14 + 158*t2 - 389*Power(t2,2) + 677*Power(t2,3) - 
                     476*Power(t2,4) + 25*Power(t2,5) + Power(t2,6))))) + 
         Power(s,4)*(4 - 12*t1 + 12*Power(t1,2) - 4*Power(t1,3) - 30*t2 + 
            8*s2*t2 + 111*t1*t2 - 16*s2*t1*t2 - 132*Power(t1,2)*t2 + 
            8*s2*Power(t1,2)*t2 + 51*Power(t1,3)*t2 + 150*Power(t2,2) - 
            98*s2*Power(t2,2) + 2*Power(s2,2)*Power(t2,2) - 
            404*t1*Power(t2,2) + 206*s2*t1*Power(t2,2) - 
            2*Power(s2,2)*t1*Power(t2,2) + 373*Power(t1,2)*Power(t2,2) - 
            108*s2*Power(t1,2)*Power(t2,2) - 
            119*Power(t1,3)*Power(t2,2) - 2*Power(t2,3) + 
            41*s2*Power(t2,3) - 16*Power(s2,2)*Power(t2,3) + 
            2*Power(s2,3)*Power(t2,3) + 156*t1*Power(t2,3) - 
            161*s2*t1*Power(t2,3) - 5*Power(s2,2)*t1*Power(t2,3) - 
            299*Power(t1,2)*Power(t2,3) + 
            203*s2*Power(t1,2)*Power(t2,3) + 60*Power(t1,3)*Power(t2,3) - 
            188*Power(t2,4) + 107*s2*Power(t2,4) + 
            159*Power(s2,2)*Power(t2,4) - 22*Power(s2,3)*Power(t2,4) + 
            454*t1*Power(t2,4) - 556*s2*t1*Power(t2,4) + 
            54*Power(s2,2)*t1*Power(t2,4) + 203*Power(t1,2)*Power(t2,4) - 
            137*s2*Power(t1,2)*Power(t2,4) + 36*Power(t1,3)*Power(t2,4) + 
            62*Power(t2,5) - 232*s2*Power(t2,5) - 
            136*Power(s2,2)*Power(t2,5) + 20*Power(s2,3)*Power(t2,5) - 
            231*t1*Power(t2,5) + 530*s2*t1*Power(t2,5) - 
            28*Power(s2,2)*t1*Power(t2,5) - 151*Power(t1,2)*Power(t2,5) + 
            9*s2*Power(t1,2)*Power(t2,5) - 22*Power(t1,3)*Power(t2,5) + 
            60*Power(t2,6) + 203*s2*Power(t2,6) - 
            35*Power(s2,2)*Power(t2,6) - 12*t1*Power(t2,6) - 
            29*s2*t1*Power(t2,6) + 31*Power(t1,2)*Power(t2,6) + 
            6*s2*Power(t1,2)*Power(t2,6) + Power(t1,3)*Power(t2,6) - 
            119*Power(t2,7) - 8*s2*Power(t2,7) + 
            10*Power(s2,2)*Power(t2,7) - 38*t1*Power(t2,7) - 
            16*s2*t1*Power(t2,7) + 53*Power(t2,8) - 4*s2*Power(t2,8) + 
            3*t1*Power(t2,8) + 
            Power(s1,6)*(-20*Power(s2,3) + 84*Power(t1,3) + 
               Power(t1,2)*(98 - 56*t2) + 
               5*Power(s2,2)*(2 + 21*t1 + 5*t2) + 
               t2*(5 - 7*t2 + 2*Power(t2,2)) - 
               s2*(10 + 168*Power(t1,2) + t1*(85 - 7*t2) - 10*t2 + 
                  9*Power(t2,2)) + t1*(35 - 54*t2 + 28*Power(t2,2))) + 
            Power(s1,5)*(14 - 103*t2 + 157*Power(t2,2) - 
               59*Power(t2,3) - 10*Power(t2,4) + 
               16*Power(s2,3)*(-1 + 5*t2) - 
               7*Power(t1,3)*(-23 + 48*t2) + 
               Power(t1,2)*(167 - 585*t2 + 210*Power(t2,2)) - 
               t1*(21 + 132*t2 - 245*Power(t2,2) + 138*Power(t2,3)) + 
               Power(s2,2)*(10 - 53*t2 - 115*Power(t2,2) - 
                  25*t1*(-4 + 15*t2)) + 
               s2*(11 + 41*t2 - 60*Power(t2,2) + 54*Power(t2,3) + 
                  3*Power(t1,2)*(-75 + 203*t2) + 
                  t1*(-79 + 383*t2 + 61*Power(t2,2)))) + 
            Power(s1,4)*(1 - 62*t2 + 484*Power(t2,2) - 796*Power(t2,3) + 
               347*Power(t2,4) + 20*Power(t2,5) + 
               Power(s2,3)*(-7 + 89*t2 - 120*Power(t2,2)) + 
               5*Power(t1,3)*(29 - 137*t2 + 105*Power(t2,2)) + 
               Power(t1,2)*(163 - 1141*t2 + 1375*Power(t2,2) - 
                  300*Power(t2,3)) + 
               t1*(-10 + 29*t2 + 195*Power(t2,2) - 488*Power(t2,3) + 
                  275*Power(t2,4)) + 
               Power(s2,2)*(36 - 163*t2 + 5*Power(t2,2) + 
                  220*Power(t2,3) + t1*(36 - 463*t2 + 510*Power(t2,2))) \
- s2*(-10 + 131*t2 - 118*Power(t2,2) - 138*Power(t2,3) + 
                  130*Power(t2,4) + 
                  Power(t1,2)*(101 - 880*t2 + 810*Power(t2,2)) + 
                  t1*(145 - 822*t2 + 581*Power(t2,2) + 290*Power(t2,3)))) \
+ Power(s1,3)*(6 - 16*t2 + 142*Power(t2,2) - 939*Power(t2,3) + 
               1521*Power(t2,4) - 670*Power(t2,5) - 20*Power(t2,6) + 
               Power(t1,3)*(26 - 444*t2 + 1040*Power(t2,2) - 
                  400*Power(t2,3)) + 
               Power(s2,3)*(-2 + 43*t2 - 191*Power(t2,2) + 
                  80*Power(t2,3)) + 
               Power(t1,2)*(-12 - 713*t2 + 2483*Power(t2,2) - 
                  1532*Power(t2,3) + 200*Power(t2,4)) + 
               t1*(65 - 229*t2 + 328*Power(t2,2) - 180*Power(t2,3) + 
                  566*Power(t2,4) - 280*Power(t2,5)) + 
               Power(s2,2)*(19 - 267*t2 + 588*Power(t2,2) + 
                  238*Power(t2,3) - 230*Power(t2,4) + 
                  t1*(2 - 162*t2 + 794*Power(t2,2) - 330*Power(t2,3))) + 
               s2*(5 - 51*t2 + 485*Power(t2,2) - 759*Power(t2,3) - 
                  146*Power(t2,4) + 160*Power(t2,5) + 
                  Power(t1,2)*
                   (43 + 196*t2 - 1137*Power(t2,2) + 450*Power(t2,3)) + 
                  t1*(-131 + 1149*t2 - 2610*Power(t2,2) + 
                     380*Power(t2,3) + 450*Power(t2,4)))) - 
            Power(s1,2)*(5 - 47*t2 + 90*Power(t2,2) + 85*Power(t2,3) - 
               918*Power(t2,4) + 1426*Power(t2,5) - 623*Power(t2,6) - 
               10*Power(t2,7) + 
               Power(s2,3)*t2*
                (-6 + 87*t2 - 199*Power(t2,2) + 20*Power(t2,3)) + 
               Power(t1,3)*(27 + 66*t2 - 549*Power(t2,2) + 
                  710*Power(t2,3) - 150*Power(t2,4)) + 
               Power(t1,2)*(-34 + 66*t2 - 1089*Power(t2,2) + 
                  2317*Power(t2,3) - 840*Power(t2,4) + 60*Power(t2,5)) + 
               t1*(2 + 170*t2 - 864*Power(t2,2) + 923*Power(t2,3) - 
                  76*Power(t2,4) + 416*Power(t2,5) - 150*Power(t2,6)) + 
               Power(s2,2)*(-2 + 54*t2 - 585*Power(t2,2) + 
                  863*Power(t2,3) + 364*Power(t2,4) - 145*Power(t2,5) + 
                  t1*(2 + 9*t2 - 270*Power(t2,2) + 627*Power(t2,3) - 
                     105*Power(t2,4))) + 
               s2*(1 + 62*t2 - 132*Power(t2,2) + 810*Power(t2,3) - 
                  1253*Power(t2,4) - 60*Power(t2,5) + 105*Power(t2,6) + 
                  Power(t1,2)*
                   (11 - 24*t2 + 273*Power(t2,2) - 586*Power(t2,3) + 
                     60*Power(t2,4)) + 
                  t1*(-12 - 287*t2 + 2325*Power(t2,2) - 
                     3514*Power(t2,3) + 153*Power(t2,4) + 325*Power(t2,5)\
))) + s1*(13 - 117*t2 - 59*Power(t2,2) + 285*Power(t2,3) - 
               70*Power(t2,4) - 420*Power(t2,5) + 658*Power(t2,6) - 
               287*Power(t2,7) - 2*Power(t2,8) + 
               Power(s2,3)*Power(t2,2)*(-6 + 73*t2 - 101*Power(t2,2)) - 
               Power(t1,3)*(34 - 118*t2 + 12*Power(t2,2) + 
                  278*Power(t2,3) - 215*Power(t2,4) + 24*Power(t2,5)) + 
               Power(t1,2)*(81 - 323*t2 + 353*Power(t2,2) - 
                  766*Power(t2,3) + 962*Power(t2,4) - 227*Power(t2,5) + 
                  6*Power(t2,6)) + 
               t1*(-60 + 322*t2 - 27*Power(t2,2) - 1055*Power(t2,3) + 
                  815*Power(t2,4) + 18*Power(t2,5) + 185*Power(t2,6) - 
                  38*Power(t2,7)) + 
               Power(s2,2)*t2*
                (-4 + 51*t2 - 513*Power(t2,2) + 564*Power(t2,3) + 
                  199*Power(t2,4) - 55*Power(t2,5) + 
                  t1*(4 + 12*t2 - 198*Power(t2,2) + 224*Power(t2,3) - 
                     15*Power(t2,4))) + 
               s2*(-8 + 99*t2 + 16*Power(t2,2) - 198*Power(t2,3) + 
                  677*Power(t2,4) - 846*Power(t2,5) + 6*Power(t2,6) + 
                  34*Power(t2,7) - 
                  Power(t1,2)*
                   (8 - 119*t2 + 270*Power(t2,2) - 315*Power(t2,3) + 
                     113*Power(t2,4) + 27*Power(t2,5)) + 
                  t1*(16 - 218*t2 + 5*Power(t2,2) + 1877*Power(t2,3) - 
                     2177*Power(t2,4) + 85*Power(t2,5) + 113*Power(t2,6)))\
)))*T4q(-s + s1 - t2))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*(-1 + t1)*Power(-s + s1 - t2,2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) + 
    (8*(Power(s,7)*(Power(t1,3) + t1*Power(t2,2) + 2*Power(t2,3)) + 
         Power(s,6)*(Power(t1,3)*(-6 - 6*s1 + 3*t2) + 
            t1*t2*(3 + s1*(-1 + 3*s2 - 5*t2) + 5*Power(t2,2) - 
               s2*(3 + t2)) + 
            Power(t1,2)*(3 + s2*t2 + s1*(-3 + 3*s2 + t2)) + 
            Power(t2,2)*(4 + s1*(2 + 4*s2 - 6*t2) - 2*t2 + 
               4*Power(t2,2) - s2*(3 + 4*t2))) + 
         Power(s,5)*(Power(t1,3)*
             (12 + 15*Power(s1,2) - 16*t2 + 3*Power(t2,2) - 
               5*s1*(-5 + 3*t2)) + 
            Power(t1,2)*(-15 + Power(s1,2)*(13 - 15*s2 - 5*t2) - 
               (-4 + s2)*t2 + 3*(1 + s2)*Power(t2,2) + 
               s1*(3*(-6 + t2)*t2 + s2*(-11 + 5*t2))) + 
            t1*(3 + (-15 + 14*s2)*t2 - (1 + 10*s2)*Power(t2,2) - 
               (2 + 5*s2)*Power(t2,3) + 7*Power(t2,4) + 
               Power(s1,2)*(1 + 3*Power(s2,2) - t2 + 10*Power(t2,2) - 
                  s2*(3 + 10*t2)) + 
               s1*(-3 + 7*t2 - Power(s2,2)*t2 - 18*Power(t2,3) + 
                  s2*(3 + 2*t2 + 15*Power(t2,2)))) + 
            t2*(3 - 14*t2 - 3*Power(t2,2) + 7*Power(t2,3) + 
               2*Power(t2,4) + Power(s2,2)*t2*(1 + 2*t2) + 
               Power(s1,2)*(1 + 3*Power(s2,2) + s2*(2 - 11*t2) - 5*t2 + 
                  6*Power(t2,2)) + 
               s2*(-3 + 10*t2 + 5*Power(t2,2) - 8*Power(t2,3)) + 
               s1*(2 + 3*t2 - 3*Power(t2,2) - 8*Power(t2,3) - 
                  Power(s2,2)*(3 + 5*t2) + s2*(3 - 7*t2 + 18*Power(t2,2)))\
)) + Power(s,4)*(1 - 12*t2 + 10*s2*t2 + 2*Power(t2,2) + 
            8*s2*Power(t2,2) - 11*Power(s2,2)*Power(t2,2) + 
            13*Power(t2,3) + 22*s2*Power(t2,3) - 
            7*Power(s2,2)*Power(t2,3) - 27*Power(t2,4) + 
            5*s2*Power(t2,4) + 4*Power(s2,2)*Power(t2,4) + 
            17*Power(t2,5) - 4*s2*Power(t2,5) + 
            Power(t1,3)*(-6 + 24*t2 - 13*Power(t2,2) + Power(t2,3)) + 
            Power(t1,2)*(21 - (10 + 11*s2)*t2 - 
               (16 + 3*s2)*Power(t2,2) + (13 + 3*s2)*Power(t2,3)) + 
            t1*(-12 + (17 - 9*s2)*t2 + 
               (-23 + 49*s2 + 2*Power(s2,2))*Power(t2,2) - 
               (43 + 8*s2)*Power(t2,3) + (3 - 7*s2)*Power(t2,4) + 
               3*Power(t2,5)) + 
            Power(s1,3)*(Power(s2,3) - 20*Power(t1,3) - 
               2*Power(-1 + t2,2)*t2 - 4*Power(s2,2)*(3*t1 + 2*t2) + 
               2*Power(t1,2)*(-11 + 5*t2) + 
               t1*(-5 + 9*t2 - 10*Power(t2,2)) + 
               s2*(1 + 30*Power(t1,2) - 4*t2 + 9*Power(t2,2) + 
                  t1*(11 + 10*t2))) + 
            Power(s1,2)*(1 + (10 - 9*s2 - 2*Power(s2,3))*t2 + 
               (-31 + 20*s2 + 17*Power(s2,2))*Power(t2,2) + 
               (17 - 21*s2)*Power(t2,3) + 4*Power(t2,4) + 
               10*Power(t1,3)*(-4 + 3*t2) - 
               2*Power(t1,2)*
                (10 - 30*t2 + 6*Power(t2,2) + 3*s2*(-6 + 5*t2)) + 
               t1*(13 - 14*t2 - 13*Power(t2,2) + 24*Power(t2,3) + 
                  Power(s2,2)*(-7 + 12*t2) - 
                  s2*(3 + 6*t2 + 32*Power(t2,2)))) + 
            s1*(Power(s2,3)*Power(t2,2) + 
               Power(s2,2)*t2*
                (7 + t1*(11 - 4*t2) + 13*t2 - 13*Power(t2,2)) + 
               Power(t1,3)*(-31 + 53*t2 - 12*Power(t2,2)) + 
               t1*t2*(-3 + 41*t2 + 2*Power(t2,2) - 17*Power(t2,3)) + 
               Power(t1,2)*(25 + 54*t2 - 44*Power(t2,2) + 
                  3*Power(t2,3)) - 
               t2*(4 + 4*t2 - 58*Power(t2,2) + 35*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*(Power(t1,2)*(9 - 26*t2) + 
                  Power(t2,2)*(-27 - 18*t2 + 16*Power(t2,2)) + 
                  t1*(-7 - 59*t2 + 3*Power(t2,2) + 26*Power(t2,3))))) - 
         (s1 - t2)*(-1 + t2)*(-1 + Power(s1,4)*Power(s2 - t1,3) + 3*t1 - 
            3*Power(t1,2) + Power(t1,3) + 3*t2 - 3*s2*t2 - 6*t1*t2 + 
            6*s2*t1*t2 + 3*Power(t1,2)*t2 - 3*s2*Power(t1,2)*t2 - 
            Power(t2,2) - 20*s2*Power(t2,2) + 7*Power(s2,2)*Power(t2,2) + 
            25*t1*Power(t2,2) + 4*s2*t1*Power(t2,2) - 
            7*Power(s2,2)*t1*Power(t2,2) - 18*Power(t1,2)*Power(t2,2) + 
            16*s2*Power(t1,2)*Power(t2,2) - 6*Power(t1,3)*Power(t2,2) - 
            5*Power(t2,3) + 49*s2*Power(t2,3) - 
            9*Power(s2,2)*Power(t2,3) + Power(s2,3)*Power(t2,3) - 
            40*t1*Power(t2,3) - 36*s2*t1*Power(t2,3) + 
            8*Power(s2,2)*t1*Power(t2,3) + 42*Power(t1,2)*Power(t2,3) - 
            14*s2*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) + 
            6*Power(t2,4) - 26*s2*Power(t2,4) + 14*t1*Power(t2,4) + 
            30*s2*t1*Power(t2,4) - 4*Power(s2,2)*t1*Power(t2,4) - 
            24*Power(t1,2)*Power(t2,4) + 4*s2*Power(t1,2)*Power(t2,4) - 
            2*Power(t2,5) + 2*Power(s2,2)*Power(t2,5) + 
            4*t1*Power(t2,5) - 4*s2*t1*Power(t2,5) - 
            Power(s1,3)*(s2 - t1)*
             (4*Power(t1,2) - 6*Power(-1 + t2,2) + 
               Power(s2,2)*(1 + 3*t2) + t1*(1 - 5*t2 + 4*Power(t2,2)) + 
               s2*(5 - 7*t2 + 2*Power(t2,2) - t1*(5 + 3*t2))) + 
            Power(s1,2)*(-2*Power(-1 + t2,3) + 
               6*Power(t1,3)*(-2 + t2)*t2 + 3*Power(s2,3)*t2*(1 + t2) + 
               t1*Power(-1 + t2,2)*(25 + 16*t2) - 
               Power(t1,2)*(27 - 55*t2 + 20*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,2)*(7 + t2 - 14*Power(t2,2) + 6*Power(t2,3) - 
                  t1*(7 + 4*t2 + 7*Power(t2,2))) + 
               s2*(-(Power(-1 + t2,2)*(29 + 12*t2)) + 
                  Power(t1,2)*(7 + 13*t2 - 2*Power(t2,2)) + 
                  t1*(22 - 62*t2 + 40*Power(t2,2)))) + 
            s1*(-(Power(s2,3)*Power(t2,2)*(3 + t2)) + 
               Power(-1 + t2,3)*(-1 + 4*t2) - 
               2*t1*Power(-1 + t2,2)*(3 + 22*t2 + 7*Power(t2,2)) - 
               4*Power(t1,3)*(1 - 3*t2 + Power(t2,3)) + 
               Power(t1,2)*(9 + 27*t2 - 86*Power(t2,2) + 
                  46*Power(t2,3) + 4*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-14 + 13*t2 + 7*Power(t2,2) - 6*Power(t2,3) + 
                  2*t1*(7 - 5*t2 + 4*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(3 + 55*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*
                   (3 - 23*t2 + 10*Power(t2,2) - 2*Power(t2,3)) + 
                  t1*(-6 - 26*t2 + 94*Power(t2,2) - 68*Power(t2,3) + 
                     6*Power(t2,4))))) + 
         Power(s,3)*(-3 + 4*t2 + s2*t2 + 52*Power(t2,2) - 
            57*s2*Power(t2,2) + 22*Power(s2,2)*Power(t2,2) - 
            61*Power(t2,3) - 31*s2*Power(t2,3) - 
            16*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) + 
            14*Power(t2,4) + 50*s2*Power(t2,4) - 
            8*Power(s2,2)*Power(t2,4) - 16*Power(t2,5) - 
            9*s2*Power(t2,5) + 2*Power(s2,2)*Power(t2,5) + 
            10*Power(t2,6) + Power(t1,3)*
             (-9 + t2 + 11*Power(t2,2) + 3*Power(t2,3)) + 
            Power(t1,2)*(3 + (-28 + 29*s2)*t2 - 
               9*(-5 + 3*s2)*Power(t2,2) - (65 + 7*s2)*Power(t2,3) + 
               (16 + s2)*Power(t2,4)) + 
            t1*(9 + (30 - 34*s2)*t2 - 
               (10 + 13*s2 + 6*Power(s2,2))*Power(t2,2) + 
               (100 + 85*s2 + 2*Power(s2,2))*Power(t2,3) - 
               2*(47 + 3*s2)*Power(t2,4) + (11 - 3*s2)*Power(t2,5)) + 
            Power(s1,4)*(-3*Power(s2,3) + 15*Power(t1,3) - 
               (-1 + t2)*t2 - 2*Power(t1,2)*(-9 + 5*t2) + 
               Power(s2,2)*(1 + 18*t1 + 6*t2) - 
               s2*(2 + 15*t1 + 30*Power(t1,2) - 2*t2 + Power(t2,2)) + 
               t1*(7 - 11*t2 + 5*Power(t2,2))) + 
            Power(s1,3)*(2 - 30*Power(t1,3)*(-1 + t2) - 21*t2 + 
               35*Power(t2,2) - 16*Power(t2,3) + 
               Power(s2,3)*(-2 + 7*t2) + 
               Power(t1,2)*(31 - 72*t2 + 18*Power(t2,2)) - 
               t1*(7 + 10*t2 - 27*Power(t2,2) + 14*Power(t2,3)) - 
               Power(s2,2)*(1 + 15*Power(t2,2) + t1*(-17 + 30*t2)) + 
               s2*(3 + 4*t2 - 7*Power(t2,2) + 4*Power(t2,3) + 
                  Power(t1,2)*(-42 + 50*t2) + 
                  t1*(-9 + 32*t2 + 16*Power(t2,2)))) - 
            Power(s1,2)*(-4 + 5*t2 - 59*Power(t2,2) + 100*Power(t2,3) - 
               42*Power(t2,4) + Power(s2,3)*t2*(-8 + 5*t2) + 
               Power(t1,3)*(-27 + 64*t2 - 18*Power(t2,2)) + 
               Power(t1,2)*(-17 + 144*t2 - 93*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(14 - 35*t2 + 43*Power(t2,2) + 8*Power(t2,3) - 
                  13*Power(t2,4)) + 
               Power(s2,2)*(t2*(16 + 21*t2 - 14*Power(t2,2)) - 
                  2*t1*(2 - 21*t2 + 8*Power(t2,2))) + 
               s2*(1 + 21*t2 - 39*Power(t2,2) - 5*Power(t2,3) + 
                  5*Power(t2,4) + 
                  2*Power(t1,2)*(8 - 36*t2 + 9*Power(t2,2)) + 
                  t1*(7 - 107*t2 + 6*Power(t2,2) + 28*Power(t2,3)))) + 
            s1*(-3 - 10*t2 + 8*Power(t2,2) + 
               Power(s2,3)*(-8 + t2)*Power(t2,2) - 33*Power(t2,3) + 
               73*Power(t2,4) - 35*Power(t2,5) + 
               Power(t1,3)*(9 - 39*t2 + 31*Power(t2,2) - 3*Power(t2,3)) + 
               Power(t1,2)*(-24 - 50*t2 + 156*Power(t2,2) - 
                  56*Power(t2,3) + Power(t2,4)) - 
               t1*(-11 + 33*t2 + 50*Power(t2,2) - 122*Power(t2,3) + 
                  12*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-4 + 27*t2 + 28*Power(t2,2) - 7*Power(t2,3) + 
                  t1*(-16 + 29*t2 - 4*Power(t2,2))) + 
               s2*(1 - 8*t2 + 63*Power(t2,2) - 101*Power(t2,3) + 
                  9*Power(t2,4) + 2*Power(t2,5) + 
                  Power(t1,2)*
                   (1 + 17*t2 - 15*Power(t2,2) - 3*Power(t2,3)) + 
                  t1*(2 + 112*t2 - 205*Power(t2,2) + 5*Power(t2,3) + 
                     15*Power(t2,4))))) + 
         Power(s,2)*(Power(t1,3)*
             (12 - 26*t2 + 12*Power(t2,2) - 5*Power(t2,3) + 
               10*Power(t2,4)) + 
            Power(t1,2)*(-24 + (91 - 26*s2)*t2 + 
               9*(-11 + 6*s2)*Power(t2,2) + (130 - 33*s2)*Power(t2,3) - 
               (100 + 3*s2)*Power(t2,4) + 6*Power(t2,5)) + 
            t1*(12 + (-95 + 48*s2)*t2 + 
               3*(50 - 33*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (-195 - 76*s2 + 8*Power(s2,2))*Power(t2,3) + 
               (183 + 132*s2 - 7*Power(s2,2))*Power(t2,4) - 
               13*(5 + s2)*Power(t2,5) + 10*Power(t2,6)) + 
            Power(s1,5)*(3*Power(s2,3) - 2*Power(s2,2)*(1 + 6*t1) + 
               s2*(1 + 15*Power(t1,2) + t1*(9 - 5*t2) - Power(t2,2)) - 
               t1*(3 + 6*Power(t1,2) + t1*(7 - 5*t2) - 4*t2 + 
                  Power(t2,2))) + 
            t2*(2*Power(s2,3)*Power(t2,2)*(-3 + 2*t2) + 
               2*Power(s2,2)*t2*
                (-4 + 18*t2 - 15*Power(t2,2) + 3*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (30 - 42*t2 - 21*Power(t2,2) + 2*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*(-22 + 72*t2 + 26*Power(t2,2) - 101*Power(t2,3) + 
                  33*Power(t2,4) - 8*Power(t2,5))) - 
            Power(s1,4)*(-5*Power(t1,3)*(-2 + 3*t2) - 
               Power(-1 + t2,2)*(-3 + 4*t2) + Power(s2,3)*(-3 + 8*t2) + 
               Power(s2,2)*(3 + t1*(13 - 28*t2) - 11*t2 + Power(t2,2)) + 
               Power(t1,2)*(17 - 36*t2 + 12*Power(t2,2)) + 
               t1*(4 - 16*t2 + 15*Power(t2,2) - 3*Power(t2,3)) + 
               s2*(1 + 2*Power(t2,2) - 3*Power(t2,3) + 
                  5*Power(t1,2)*(-4 + 7*t2) + 
                  t1*(-16 + 39*t2 - 9*Power(t2,2)))) + 
            Power(s1,3)*(-2*Power(t1,3)*(5 - 17*t2 + 6*Power(t2,2)) + 
               Power(s2,3)*(2 - 13*t2 + 6*Power(t2,2)) - 
               Power(-1 + t2,2)*(-5 - 21*t2 + 14*Power(t2,2)) + 
               Power(t1,2)*(-41 + 111*t2 - 66*Power(t2,2) + 
                  9*Power(t2,3)) - 
               t1*(2 + t2 + 5*Power(t2,2) - 11*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,2)*(-9 + 29*t2 - 10*Power(t2,2) + 
                  3*Power(t2,3) + t1*(-6 + 48*t2 - 20*Power(t2,2))) + 
               s2*(-5 + 18*t2 - 14*Power(t2,2) + 4*Power(t2,3) - 
                  3*Power(t2,4) + 
                  Power(t1,2)*(12 - 65*t2 + 24*Power(t2,2)) - 
                  2*t1*(-18 + 51*t2 - 21*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,2)*(Power(s2,3)*t2*(-10 + 21*t2) + 
               3*Power(t1,3)*t2*(3 - 8*t2 + Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-7 - 24*t2 - 24*Power(t2,2) + 18*Power(t2,3)) + 
               Power(t1,2)*(-25 + 147*t2 - 213*Power(t2,2) + 
                  58*Power(t2,3) - 2*Power(t2,4)) + 
               t1*(-7 - 27*t2 + 107*Power(t2,2) - 84*Power(t2,3) + 
                  10*Power(t2,4) + Power(t2,5)) + 
               Power(s2,2)*(t2*
                   (52 - 85*t2 + Power(t2,2) - 3*Power(t2,3)) + 
                  t1*(-2 + 22*t2 - 58*Power(t2,2) + 4*Power(t2,3))) + 
               s2*(-5 + 26*t2 - 65*Power(t2,2) + 53*Power(t2,3) - 
                  10*Power(t2,4) + Power(t2,5) + 
                  Power(t1,2)*
                   (-9 + 5*t2 + 42*Power(t2,2) - 3*Power(t2,3)) + 
                  t1*(41 - 200*t2 + 244*Power(t2,2) - 12*Power(t2,3) - 
                     3*Power(t2,4)))) + 
            s1*(-2*Power(t1,3)*Power(t2,2)*(-6 + 7*t2) - 
               Power(s2,3)*Power(t2,2)*(-14 + 15*t2 + Power(t2,2)) + 
               Power(t1,2)*(-10 + 28*t2 - 186*Power(t2,2) + 
                  203*Power(t2,3) - 22*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (1 - 35*t2 - 45*Power(t2,2) - 4*Power(t2,3) + 
                  10*Power(t2,4)) + 
               t1*(11 + 13*t2 + 106*Power(t2,2) - 241*Power(t2,3) + 
                  131*Power(t2,4) - 20*Power(t2,5)) + 
               Power(s2,2)*t2*
                (8 - 79*t2 + 89*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t2,4) + t1*(-4 - 24*t2 + 30*Power(t2,2))) + 
               s2*(2 - 17*t2 - 87*Power(t2,2) + 159*Power(t2,3) - 
                  73*Power(t2,4) + 16*Power(t2,5) + 
                  Power(t1,2)*
                   (6 + 5*t2 - 24*Power(t2,2) + 16*Power(t2,3) - 
                     Power(t2,4)) + 
                  t1*(-8 - 42*t2 + 320*Power(t2,2) - 310*Power(t2,3) + 
                     13*Power(t2,4) + Power(t2,5))))) + 
         s*(-(Power(s1,6)*Power(s2 - t1,2)*(-1 + s2 - t1 + t2)) + 
            Power(s1,5)*(s2 - t1)*
             (s2*(t1*(2 - 6*t2) + 4*Power(-1 + t2,2)) - 
               Power(-1 + t2,2) - 3*t1*Power(-1 + t2,2) + 
               Power(s2,2)*(-1 + 3*t2) + Power(t1,2)*(-1 + 3*t2)) + 
            (-1 + t2)*(-4 + (26 - 8*s2)*t2 + 
               (-43 + 24*s2 - 2*Power(s2,2))*Power(t2,2) + 
               2*(5 + 28*s2 + Power(s2,2) - Power(s2,3))*Power(t2,3) + 
               (23 - 72*s2 - 13*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,4) + 2*(-5 + s2 + 4*Power(s2,2))*Power(t2,5) - 
               2*(1 + s2)*Power(t2,6) + 
               Power(t1,3)*(4 - 11*t2 + 3*Power(t2,2) + 4*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(t1,2)*(-12 - 8*(-6 + s2)*t2 + 
                  (-43 + 22*s2)*Power(t2,2) + (76 - 28*s2)*Power(t2,3) + 
                  (-74 + 6*s2)*Power(t2,4)) + 
               t1*(12 + (-63 + 16*s2)*t2 + 
                  (83 - 46*s2 + 2*Power(s2,2))*Power(t2,2) + 
                  (-107 - 44*s2 + 13*Power(s2,2))*Power(t2,3) + 
                  (79 + 96*s2 - 11*Power(s2,2))*Power(t2,4) - 
                  4*(2 + 3*s2)*Power(t2,5) + 4*Power(t2,6))) - 
            Power(s1,4)*(4*Power(-1 + t2,3) + 
               t1*Power(-1 + t2,2)*(-2 + 3*t2) + 
               Power(t1,3)*(-3 + 8*t2 - 3*Power(t2,2)) + 
               Power(s2,3)*(1 - 5*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-14 + 25*t2 - 14*Power(t2,2) + 
                  3*Power(t2,3)) + 
               s2*(-(Power(-1 + t2,2)*(-2 + 3*t2)) + 
                  Power(t1,2)*(8 - 23*t2 + 9*Power(t2,2)) + 
                  t1*(23 - 43*t2 + 29*Power(t2,2) - 9*Power(t2,3))) + 
               Power(s2,2)*(t1*(-6 + 20*t2 - 8*Power(t2,2)) + 
                  3*(-3 + 6*t2 - 5*Power(t2,2) + 2*Power(t2,3)))) - 
            Power(s1,3)*(-(Power(-1 + t2,3)*(5 + 14*t2)) + 
               t1*Power(-1 + t2,2)*(17 - 2*t2 + Power(t2,2)) + 
               Power(t1,3)*(7 - 9*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(2 - 7*t2 + 11*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,2)*(12 - 66*t2 + 64*Power(t2,2) - 
                  11*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(-19 + 78*t2 - 70*Power(t2,2) + 
                  15*Power(t2,3) - 4*Power(t2,4) + 
                  t1*(4 + 6*t2 - 34*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(9 - 26*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (-13 + 10*t2 + 30*Power(t2,2) - 3*Power(t2,3)) + 
                  t1*(20 - 111*t2 + 101*Power(t2,2) - 15*Power(t2,3) + 
                     5*Power(t2,4)))) + 
            Power(s1,2)*(Power(t1,3)*t2*(21 - 45*t2 + 17*Power(t2,2)) - 
               Power(-1 + t2,3)*(1 + 29*t2 + 18*Power(t2,2)) + 
               Power(s2,3)*t2*
                (6 - 15*t2 + 13*Power(t2,2) + 3*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (4 + 102*t2 - 4*Power(t2,2) + 11*Power(t2,3)) - 
               Power(t1,2)*(5 + 90*t2 - 237*Power(t2,2) + 
                  136*Power(t2,3) + 6*Power(t2,4)) - 
               Power(s2,2)*(-2 + 42*t2 - 144*Power(t2,2) + 
                  123*Power(t2,3) - 20*Power(t2,4) + Power(t2,5) + 
                  t1*(2 + 3*t2 - 18*Power(t2,2) + 31*Power(t2,3) + 
                     3*Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (7 - 39*t2 + 57*Power(t2,2) - 4*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (-5 + 67*t2 + 46*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(-12 + 164*t2 - 387*Power(t2,2) + 235*Power(t2,3) - 
                     Power(t2,4) + Power(t2,5)))) + 
            s1*(-(Power(s2,3)*Power(t2,2)*
                  (6 - 13*t2 + 8*Power(t2,2) + Power(t2,3))) + 
               Power(-1 + t2,3)*
                (-13 + 3*t2 + 38*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,3)*(2 - 14*t2 + 9*Power(t2,2) + 25*Power(t2,3) - 
                  20*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (24 - 10*t2 + 157*Power(t2,2) - 4*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(t1,2)*(9 - 2*t2 + 131*Power(t2,2) - 
                  311*Power(t2,3) + 165*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-4 + 27*t2 - 90*Power(t2,2) + 88*Power(t2,3) - 
                  21*Power(t2,4) + 
                  t1*(4 + 18*t2 - 42*Power(t2,2) + 25*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (-8 + 11*t2 + 148*Power(t2,2) + 22*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  Power(t1,2)*
                   (-8 + 23*t2 - 24*Power(t2,2) - 5*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  t1*(16 - 50*t2 - 142*Power(t2,2) + 439*Power(t2,3) - 
                     278*Power(t2,4) + 15*Power(t2,5))))))*T5q(s))/
     (s*(-1 + s1)*(-1 + s2)*(-1 + t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)));
   return a;
};
