#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m312_4_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(2*Power(s,6)*(1 + s2*(2 + t1 + t2)) + 
         Power(s,5)*(10*(-1 + t1) + 
            2*Power(s2,2)*(-2 + s1 - 10*t1 - 3*t2) + 
            s2*(-78 + 41*t1 + 13*Power(t1,2) - t2 + 10*t1*t2 - 
               3*Power(t2,2) + s1*(8 - 3*t1 + t2))) + 
         Power(s,4)*(4*(4 - 10*t1 + 5*Power(t1,2)) + 
            Power(s2,3)*(-8*s1 + 46*t1 + 6*(-2 + t2)) + 
            Power(s2,2)*(154 + Power(s1,2) - 80*Power(t1,2) + 
               s1*(-1 + 43*t1 - 14*t2) - 15*t2 + 13*Power(t2,2) - 
               15*t1*(4 + 3*t2)) + 
            s2*(283 + Power(s1,2)*t1 + 28*Power(t1,3) - 73*t2 + 
               4*Power(t1,2)*(33 + 7*t2) + 
               t1*(-389 + 28*t2 - 16*Power(t2,2)) + 
               s1*(-29 - 21*Power(t1,2) + 20*t2 + t1*(11 + 5*t2)))) + 
         s2*(1 + s2 - t1)*(-1 + t1)*
          ((-1 + s1)*Power(t1,4) - 8*Power(-1 + t2,2) + 
            4*Power(s2,3)*(-1 + 2*Power(s1,2) + t1 - t2 + t1*t2 + 
               2*Power(t2,2) - 2*s1*(-1 + t1 + 2*t2)) + 
            Power(t1,3)*(1 - 5*Power(s1,2) - 44*t2 + s1*(41 + 7*t2)) + 
            Power(t1,2)*(-39 + Power(s1,2) + 102*t2 + 5*Power(t2,2) - 
               s1*(47 + 22*t2)) + 
            t1*(47 - 4*Power(s1,2) - 74*t2 - 5*Power(t2,2) + 
               s1*(5 + 31*t2)) - 
            Power(s2,2)*(-4 + Power(s1,2)*(7 + 17*t1) - 49*t2 + 
               21*Power(t2,2) + t1*t2*(32 + 3*t2) + 
               Power(t1,2)*(4 + 17*t2) - 
               s1*(-57 + 21*Power(t1,2) + 32*t2 + 4*t1*(9 + 4*t2))) + 
            s2*(-31 + 2*Power(s1,2)*(2 + 3*t1 + 7*Power(t1,2)) + 42*t2 + 
               21*Power(t2,2) + Power(t1,3)*(1 + 13*t2) + 
               Power(t1,2)*(3 + 80*t2 - 5*Power(t2,2)) + 
               t1*(27 - 135*t2 + 8*Power(t2,2)) - 
               s1*(5 + 14*Power(t1,3) + 31*t2 + 2*t1*(-52 + 5*t2) + 
                  Power(t1,2)*(85 + 7*t2)))) + 
         Power(s,3)*(-10 + 48*t1 - 60*Power(t1,2) + 20*Power(t1,3) + 
            2*Power(s2,4)*(10 + 5*s1 - 20*t1 - t2) + 
            Power(s2,3)*(-108 - 6*Power(s1,2) + 117*Power(t1,2) + 5*t2 - 
               21*Power(t2,2) + s1*(16 - 117*t1 + 33*t2) + t1*(7 + 88*t2)\
) - Power(s2,2)*(112*Power(t1,3) + Power(s1,2)*(9 + 8*t1) + 
               Power(t1,2)*(187 + 128*t2) + 
               t1*(-651 + 95*t2 - 52*Power(t2,2)) + 
               4*(111 - 48*t2 + 8*Power(t2,2)) + 
               s1*(45 - 148*Power(t1,2) - 23*t2 + 4*t1*(-7 + 11*t2))) + 
            s2*(-407 + 26*Power(t1,4) + 
               Power(s1,2)*(14 - 3*t1 + 8*Power(t1,2)) + 255*t2 + 
               32*Power(t2,2) + Power(t1,3)*(199 + 44*t2) + 
               Power(t1,2)*(-715 + 118*t2 - 30*Power(t2,2)) + 
               t1*(939 - 413*t2 + 3*Power(t2,2)) + 
               s1*(13 - 46*Power(t1,3) + 2*Power(t1,2)*(-19 + t2) - 
                  102*t2 + t1*(31 + 80*t2)))) + 
         s*(2*Power(-1 + t1,2)*t1*(1 - 3*t1 + Power(t1,2)) - 
            4*Power(s2,5)*(2 + Power(s1,2) + s1*(-9 + 9*t1 - 2*t2) + 
               7*t2 + Power(t2,2) - t1*(2 + 7*t2)) - 
            2*Power(s2,4)*(23 + 5*Power(t1,3) + 
               Power(s1,2)*(-2 + 8*t1) - 16*t2 + 8*Power(t2,2) - 
               2*s1*(-6 + 33*Power(t1,2) + t1*(-27 + t2) + 5*t2) + 
               Power(t1,2)*(21 + 50*t2) - t1*(49 + 34*t2 + 2*Power(t2,2))\
) - Power(s2,2)*(208 + 12*Power(t1,5) + 
               Power(s1,2)*(-15 + 22*t1 - 35*Power(t1,2) + 
                  48*Power(t1,3)) - 300*t2 - 42*Power(t2,2) + 
               Power(t1,4)*(95 + 82*t2) - 
               3*Power(t1,3)*(123 - 79*t2 + 12*Power(t2,2)) + 
               t1*(-623 + 1013*t2 + 28*Power(t2,2)) + 
               Power(t1,2)*(677 - 1032*t2 + 70*Power(t2,2)) + 
               s1*(83 - 98*Power(t1,4) + 103*t2 - 
                  4*Power(t1,3)*(50 + 3*t2) + 
                  Power(t1,2)*(717 + 19*t2) - 2*t1*(251 + 75*t2))) + 
            Power(s2,3)*(-124 + 21*Power(t1,4) + 
               2*Power(s1,2)*(1 - 9*t1 + 26*Power(t1,2)) + 259*t2 - 
               23*Power(t2,2) + Power(t1,3)*(85 + 144*t2) + 
               Power(t1,2)*(-285 + 59*t2 - 29*Power(t2,2)) + 
               t1*(303 - 462*t2 + 88*Power(t2,2)) - 
               s1*(200 + 179*Power(t1,3) - 23*t2 + 
                  Power(t1,2)*(18 + 5*t2) + t1*(-397 + 90*t2))) + 
            s2*(-1 + t1)*(89 + Power(t1,5) + 
               Power(s1,2)*(-4 + 23*t1 - 9*Power(t1,2) + 
                  16*Power(t1,3)) - 122*t2 + 3*Power(t2,2) + 
               5*Power(t1,4)*(9 + 2*t2) + 
               Power(t1,3)*(-136 + 157*t2 - 7*Power(t2,2)) - 
               2*Power(t1,2)*(-162 + 241*t2 + 7*Power(t2,2)) + 
               t1*(-323 + 441*t2 + 44*Power(t2,2)) - 
               s1*(3 + 15*Power(t1,4) - 39*t2 + 
                  Power(t1,3)*(129 + 19*t2) + 5*t1*(13 + 33*t2) - 
                  Power(t1,2)*(208 + 93*t2)))) + 
         Power(s,2)*(2 - 4*Power(s2,5)*(2 + s1 - 3*t1) - 20*t1 + 
            48*Power(t1,2) - 40*Power(t1,3) + 10*Power(t1,4) + 
            Power(s2,4)*(38 + 9*Power(s1,2) - 50*Power(t1,2) + 
               t1*(4 - 81*t2) + s1*(-59 + 113*t1 - 28*t2) + 39*t2 + 
               15*Power(t2,2)) - 
            Power(s2,2)*(-451 + 64*Power(t1,4) + 
               Power(s1,2)*(15 + 2*t1 + 38*Power(t1,2)) + 437*t2 + 
               22*Power(t2,2) + 2*Power(t1,3)*(112 + 79*t2) + 
               Power(t1,2)*(-861 + 219*t2 - 70*Power(t2,2)) + 
               t1*(1056 - 804*t2 + 89*Power(t2,2)) - 
               s1*(146 + 190*Power(t1,3) + 87*t2 - 
                  4*Power(t1,2)*(-33 + 8*t2) + t1*(-422 + 37*t2))) + 
            Power(s2,3)*(229 + 92*Power(t1,3) + 
               Power(s1,2)*(13 + 15*t1) - 155*t2 + 56*Power(t2,2) + 
               Power(t1,2)*(95 + 196*t2) + 
               t1*(-380 + 7*t2 - 48*Power(t2,2)) + 
               s1*(106 - 253*Power(t1,2) - 79*t2 + t1*(55 + 51*t2))) + 
            s2*(279 + 10*Power(t1,5) + 
               Power(s1,2)*(-18 + 35*t1 - 17*Power(t1,2) + 
                  18*Power(t1,3)) - 289*t2 - 34*Power(t2,2) + 
               Power(t1,4)*(147 + 34*t2) + 
               Power(t1,2)*(1074 - 789*t2 + Power(t2,2)) - 
               6*Power(t1,3)*(97 - 32*t2 + 4*Power(t2,2)) + 
               t1*(-928 + 856*t2 + 75*Power(t2,2)) + 
               s1*(5 - 42*Power(t1,4) + 120*t2 - 
                  2*Power(t1,3)*(58 + 7*t2) + Power(t1,2)*(269 + 136*t2) - 
                  2*t1*(60 + 139*t2))))))/
     (s*(-1 + s2)*s2*Power(-s + s2 - t1,2)*(1 - s + s2 - t1)*
       Power(-1 + s + t1,2)*(s - s1 + t2)) - 
    (8*(-4*(-1 + t1)*(Power(s2,3) + t1 - 2*Power(t1,2) - 
            Power(s2,2)*(1 + 2*t1) + s2*(-1 + 3*t1 + Power(t1,2)))*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) + 
         Power(s,7)*(Power(t1,2) + 2*t1*t2 - Power(t2,2)) - 
         Power(s,6)*(7*Power(t1,2) - 4*Power(t1,3) + 2*t2 + 4*t1*t2 - 
            9*Power(t1,2)*t2 - 5*Power(t2,2) + 3*t1*Power(t2,2) + 
            s1*(Power(t1,2) + t1*(-2 + t2) + 2*s2*t2) + 
            s2*(4*Power(t1,2) - t2*(4 + 7*t2) + t1*(-2 + 11*t2))) + 
         Power(s,5)*(-1 + 6*Power(t1,4) + 8*t2 - 5*Power(t2,2) + 
            Power(t1,3)*(-19 - 4*s1 + 17*t2) + 
            Power(t1,2)*(29 + s1*(14 - 3*t2) - 18*t2 - 3*Power(t2,2)) + 
            t1*(6 - 27*t2 + 13*Power(t2,2) + s1*(-7 + 4*t2)) + 
            Power(s2,2)*(-Power(s1,2) + 6*Power(t1,2) + 
               s1*(2 + t1 + 12*t2) - t2*(19 + 20*t2) + t1*(-7 + 24*t2)) \
+ s2*(2 - 12*Power(t1,3) + 4*Power(t1,2)*(8 + s1 - 10*t2) + 
               (2 + 5*s1)*t2 - 26*Power(t2,2) + 
               t1*(-17 + Power(s1,2) + 42*t2 + 19*Power(t2,2) - 
                  3*s1*(3 + t2)))) + 
         Power(s,4)*(3 + 4*Power(t1,5) - 2*t2 - 13*Power(t2,2) + 
            Power(t1,4)*(-18 - 6*s1 + 17*t2) - 
            t1*(26 + s1 - 82*t2 - 3*s1*t2 + 4*Power(t2,2)) - 
            Power(t1,3)*(-64 + 32*t2 + Power(t2,2) + s1*(-29 + 3*t2)) + 
            Power(t1,2)*(-24 - 71*t2 + 10*Power(t2,2) + 
               3*s1*(-19 + 4*t2)) + 
            Power(s2,3)*(5*Power(s1,2) - 4*Power(t1,2) + 
               t1*(8 - 26*t2) + 3*t2*(11 + 10*t2) - 
               s1*(7 + 3*t1 + 28*t2)) + 
            s2*(5 - 12*Power(t1,4) + 2*Power(s1,2)*t1*(-1 + 2*t1) + 
               Power(t1,3)*(52 - 57*t2) - 57*t2 + 20*Power(t2,2) + 
               t1*(56 + 18*t2 - 61*Power(t2,2)) + 
               6*Power(t1,2)*(-22 + 21*t2 + 3*Power(t2,2)) + 
               s1*(-1 + 12*Power(t1,3) - t1*(-30 + t2) + 7*t2 - 
                  Power(t1,2)*(52 + t2))) + 
            Power(s2,2)*(-15 + Power(s1,2)*(2 - 9*t1) + 12*Power(t1,3) + 
               22*t2 + 51*Power(t2,2) + 6*Power(t1,2)*(-8 + 11*t2) + 
               t1*(43 - 119*t2 - 46*Power(t2,2)) + 
               s1*(-3*Power(t1,2) - 24*t2 + t1*(27 + 31*t2)))) + 
         Power(s,3)*(2 + Power(t1,6) + 
            Power(t1,4)*(44 - s1*(-26 + t2) - 30*t2) - 28*t2 + 
            34*Power(t2,2) + Power(t1,5)*(-7 - 4*s1 + 9*t2) + 
            t1*(14 - 30*s1*(-1 + t2) - 11*t2 - 47*Power(t2,2)) + 
            Power(t1,3)*(-28 + 13*s1*(-9 + t2) - 83*t2 + Power(t2,2)) + 
            2*Power(t1,2)*(-5 + 2*Power(s1,2) + 88*t2 + 6*Power(t2,2) + 
               s1*(18 + 7*t2)) + 
            Power(s2,4)*(-9*Power(s1,2) + Power(t1,2) - 
               25*t2*(1 + t2) + t1*(-3 + 14*t2) + s1*(8 + 3*t1 + 32*t2)) \
+ s2*(-33 - 4*Power(t1,5) + Power(s1,2)*Power(t1,2)*(-5 + 6*t1) + 
               Power(t1,4)*(22 - 38*t2) + 81*t2 + 20*Power(t2,2) + 
               Power(t1,2)*(159 - 10*t2 - 48*Power(t2,2)) + 
               Power(t1,3)*(-152 + 157*t2 + 7*Power(t2,2)) + 
               2*t1*(5 - 104*t2 + 9*Power(t2,2)) + 
               2*s1*(1 + 6*Power(t1,4) + Power(t1,2)*(94 - 13*t2) - 
                  9*t2 - Power(t1,3)*(37 + t2) + t1*(-41 + 21*t2))) + 
            Power(s2,2)*(38 + 6*Power(t1,4) + 
               Power(s1,2)*(4 + 14*t1 - 21*Power(t1,2)) + 46*t2 - 
               26*Power(t2,2) + Power(t1,3)*(-38 + 63*t2) - 
               6*Power(t1,2)*(-26 + 41*t2 + 6*Power(t2,2)) + 
               t1*(-152 + 135*t2 + 99*Power(t2,2)) - 
               s1*(9*Power(t1,3) + 22*t1*(3 + 2*t2) + 2*(5 + 8*t2) - 
                  Power(t1,2)*(83 + 36*t2))) + 
            Power(s2,3)*(14 - 4*Power(t1,3) + 
               3*Power(s1,2)*(-3 + 8*t1) + Power(t1,2)*(26 - 48*t2) - 
               48*t2 - 47*Power(t2,2) + 
               2*t1*(-15 + 67*t2 + 27*Power(t2,2)) + 
               s1*(11 - 2*Power(t1,2) + 41*t2 - t1*(43 + 65*t2)))) + 
         Power(s,2)*(Power(t1,5)*(7 + 11*s1 - 16*t2) - 
            Power(t1,6)*(1 + s1 - 2*t2) + 
            t1*(-1 + t2)*(-61 + 40*s1 + 73*t2) - 
            Power(t1,4)*(-19 + s1*(98 - 6*t2) + 44*t2 + Power(t2,2)) - 
            4*(3 - 10*t2 + 7*Power(t2,2)) + 
            Power(s2,5)*(7*Power(s1,2) + t2*(7 - 3*t1 + 11*t2) - 
               s1*(3 + t1 + 18*t2)) - 
            Power(t1,2)*(56 + 4*Power(s1,2) - 24*t2 + 58*Power(t2,2) + 
               s1*(-47 + 79*t2)) + 
            Power(t1,3)*(14*Power(s1,2) + s1*(51 + 23*t2) + 
               2*(-9 + 79*t2 + 7*Power(t2,2))) + 
            Power(s2,2)*(Power(s1,2)*
                (-4 + 18*t1 + 28*Power(t1,2) - 19*Power(t1,3)) + 
               21*Power(t1,4)*t2 + 
               Power(t1,3)*(71 - 193*t2 - 11*Power(t2,2)) + 
               t1*(53 + 16*t2 - 7*Power(t2,2)) - 
               2*(-8 + 28*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(-140 + 260*t2 + 63*Power(t2,2)) + 
               s1*(-37 - 5*Power(t1,4) + 41*t2 - 31*t1*(-5 + 3*t2) + 
                  Power(t1,3)*(58 + 24*t2) - Power(t1,2)*(219 + 26*t2))) \
+ Power(s2,4)*(-1 + Power(s1,2)*(12 - 25*t1) + 32*t2 + 20*Power(t2,2) + 
               Power(t1,2)*(-3 + 13*t2) + 
               t1*(4 - 59*t2 - 31*Power(t2,2)) + 
               s1*(-17 + 2*Power(t1,2) - 30*t2 + t1*(29 + 54*t2))) + 
            Power(s2,3)*(-38 + 
               Power(s1,2)*(-6 - 32*t1 + 33*Power(t1,2)) + 
               Power(t1,3)*(4 - 23*t2) + 32*t2 + 16*Power(t2,2) + 
               t1*(86 - 194*t2 - 71*Power(t2,2)) + 
               Power(t1,2)*(-52 + 173*t2 + 30*Power(t2,2)) + 
               s1*(-4 + Power(t1,3) + 6*t2 - Power(t1,2)*(61 + 57*t2) + 
                  t1*(76 + 81*t2))) + 
            s2*(2*Power(s1,2)*t1*
                (4 - 13*t1 - 4*Power(t1,2) + 2*Power(t1,3)) - 
               10*Power(t1,5)*t2 + 
               Power(t1,2)*(55 - 266*t2 - 15*Power(t2,2)) + 
               Power(t1,3)*(20 - 34*t2 - 15*Power(t2,2)) + 
               5*(3 + 2*t2 - 5*Power(t2,2)) + 
               Power(t1,4)*(-30 + 88*t2 + Power(t2,2)) + 
               12*t1*(-5 + 13*t2 + 4*Power(t2,2)) + 
               s1*(8 + 4*Power(t1,5) + Power(t1,3)*(258 - 31*t2) + 
                  t1*(46 - 18*t2) - 8*t2 - Power(t1,4)*(34 + 3*t2) + 
                  Power(t1,2)*(-226 + 88*t2)))) - 
         s*(2*Power(s2,6)*Power(s1 - t2,2) - 8*Power(-1 + t2,2) + 
            Power(t1,6)*(2 - 2*s1 + 4*t2) + 
            Power(t1,5)*(-17 - s1*(-31 + t2) + 7*t2) + 
            4*t1*(-1 + t2)*(-16 + 4*s1 + 9*t2) - 
            Power(s2,5)*(s1 - t2)*
             (6 - 6*t1 + s1*(-5 + 9*t1) + 3*t2 - 7*t1*t2) + 
            Power(t1,2)*(-113 + s1*(44 - 64*t2) + 168*t2 - 
               51*Power(t2,2)) - 
            Power(t1,4)*(-19 + 18*Power(s1,2) + 56*t2 + 3*Power(t2,2) + 
               2*s1*(-1 + 6*t2)) + 
            Power(t1,3)*(53 + 16*Power(s1,2) - 39*t2 + 24*Power(t2,2) + 
               s1*(-59 + 65*t2)) + 
            s2*(-36 - Power(s1,2)*Power(t1,2)*
                (44 - 48*t1 - 9*Power(t1,2) + Power(t1,3)) + 44*t2 - 
               8*Power(t2,2) - Power(t1,5)*(12 + 19*t2) + 
               Power(t1,2)*(34 - 117*t2 - 7*Power(t2,2)) + 
               Power(t1,4)*(66 + 15*t2 + 2*Power(t2,2)) + 
               2*t1*(29 - 40*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(-110 + 157*t2 + 11*Power(t2,2)) + 
               s1*(Power(t1,3)*(107 - 73*t2) + 
                  Power(t1,2)*(13 - 11*t2) - 16*(-1 + t2) + 
                  Power(t1,5)*(3 + t2) + 8*t1*(-3 + 8*t2) + 
                  Power(t1,4)*(-115 + 11*t2))) + 
            Power(s2,4)*(-4 + 
               2*Power(s1,2)*(1 - 12*t1 + 8*Power(t1,2)) + 33*t2 + 
               9*Power(t2,2) - 4*t1*(-2 + 17*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(-4 + 35*t2 + 9*Power(t2,2)) - 
               s1*(13 + 11*t2 + 5*Power(t1,2)*(3 + 5*t2) - 
                  4*t1*(7 + 12*t2))) + 
            Power(s2,3)*(-4 - 
               2*Power(s1,2)*
                (6 - 4*t1 - 21*Power(t1,2) + 7*Power(t1,3)) + 5*t2 - 
               15*Power(t2,2) - Power(t1,3)*t2*(63 + 5*t2) + 
               t1*(8 - 117*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(-4 + 175*t2 + 33*Power(t2,2)) + 
               s1*(-25 + t1*(105 - 43*t2) + 39*t2 - 
                  7*Power(t1,2)*(13 + 9*t2) + Power(t1,3)*(11 + 19*t2))) \
+ Power(s2,2)*(19 + 2*Power(s1,2)*t1*
                (20 - 20*t1 - 16*Power(t1,2) + 3*Power(t1,3)) - 16*t2 + 
               Power(t2,2) + Power(t1,2)*(83 - 9*t2 - 32*Power(t2,2)) + 
               Power(t1,4)*(14 + 49*t2 + Power(t2,2)) + 
               3*t1*(-21 + 37*t2 + 6*Power(t2,2)) - 
               Power(t1,3)*(53 + 135*t2 + 14*Power(t2,2)) + 
               s1*(-20 + t1*(71 - 93*t2) - Power(t1,4)*(3 + 7*t2) + 
                  Power(t1,3)*(153 + 13*t2) + Power(t1,2)*(-201 + 139*t2))\
)))*B1(s,1 - s + s2 - t1,s2))/
     (s*(-1 + s2)*(-s + s2 - t1)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(s - s1 + t2)) + 
    (16*(-(Power(s,8)*t1*(-1 + Power(s2,2) + s2*(2 + t1 - t2))) - 
         2*(-1 + s2)*Power(s2,2)*Power(-1 + t1,2)*
          (-1 + Power(s2,2) + 2*t1 - s2*(1 + t1))*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) + 
         Power(s,7)*(t1*(-8 + 5*t1) + Power(s2,3)*(-1 + 5*t1) - 
            Power(s2,2)*(2 - 16*t1 + Power(t1,2) - t2 + Power(t2,2)) + 
            s2*(1 - 4*Power(t1,3) + Power(t1,2)*(-5 + s1 + 4*t2) - 
               t1*(-13 + s1 + 7*t2))) + 
         Power(s,5)*(2*Power(s2,5)*(-3 + 5*t1) + 
            t1*(-45 + 92*t1 - 56*Power(t1,2) + 10*Power(t1,3)) - 
            Power(s2,4)*(22 + s1 + Power(s1,2) - 29*t1 - 11*s1*t1 + 
               8*Power(t1,2) + 12*t2 - 10*s1*t2 - 3*t1*t2 + 
               14*Power(t2,2)) + 
            s2*(19 - 4*Power(t1,5) + Power(t1,4)*(-2 + 6*s1 + 4*t2) - 
               6*Power(t1,3)*(-17 + 5*s1 + 5*t2) - 
               3*t1*(-29 + 11*s1 + 9*t2) + 
               Power(t1,2)*(-206 + 56*s1 + 54*t2)) + 
            Power(s2,2)*(-42 - Power(s1,2)*(-9 + t1)*t1 + 
               12*Power(t1,4) + 
               s1*(9 - 31*t1 - 11*Power(t1,3) + 
                  Power(t1,2)*(7 - 4*t2)) + 19*t2 - 3*Power(t2,2) + 
               Power(t1,3)*(57 + 9*t2) + 
               Power(t1,2)*(-153 + 17*t2 - 5*Power(t2,2)) + 
               t1*(185 - 54*t2 + 12*Power(t2,2))) + 
            Power(s2,3)*(-33 + 3*Power(s1,2)*t1 - 3*Power(t1,3) + 8*t2 - 
               16*Power(t2,2) + 
               s1*(-7 + 4*Power(t1,2) - 8*t1*(-3 + t2) + 2*t2) - 
               Power(t1,2)*(107 + 26*t2) + 
               t1*(96 + 15*t2 + 20*Power(t2,2)))) + 
         Power(s,6)*(Power(s2,4)*(4 - 10*t1) + 
            2*t1*(13 - 17*t1 + 5*Power(t1,2)) + 
            s2*(-7 - 6*Power(t1,4) + Power(t1,2)*(55 - 11*s1 - 24*t2) + 
               Power(t1,3)*(-4 + 4*s1 + 6*t2) + t1*(-42 + 10*s1 + 19*t2)\
) + Power(s2,3)*(14 + 9*Power(t1,2) + (3 - 2*s1)*t2 + 6*Power(t2,2) - 
               t1*(3*s1 + 5*(8 + t2))) + 
            Power(s2,2)*(6*Power(t1,3) + Power(t1,2)*(47 + t2) + 
               t1*(-65 + 12*t2 - 3*Power(t2,2)) + 
               4*(3 - 2*t2 + Power(t2,2)) - 
               s1*(1 + 2*Power(t1,2) + t1*(3 + 2*t2)))) + 
         Power(s,4)*(Power(s2,6)*(4 - 5*t1) + 
            t1*(45 - 128*t1 + 121*Power(t1,2) - 44*Power(t1,3) + 
               5*Power(t1,4)) + 
            Power(s2,5)*(4*Power(s1,2) - 6*Power(t1,2) + 
               s1*(2 - 15*t1 - 18*t2) + 2*t1*(7 + 3*t2) + t2*(9 + 16*t2)\
) - s2*(26 + Power(t1,6) + Power(t1,3)*(371 - 104*s1 - 54*t2) + 
               t1*(116 - 52*s1 - 23*t2) - 
               Power(t1,5)*(-2 + 4*s1 + t2) + 
               2*Power(t1,4)*(-50 + 17*s1 + 8*t2) + 
               2*Power(t1,2)*(-213 + 65*s1 + 30*t2)) - 
            Power(s2,2)*(-7*Power(t1,5) + 
               Power(s1,2)*t1*(30 - 36*t1 + Power(t1,2)) - 
               Power(t1,4)*(48 + 17*t2) + 
               Power(t1,2)*(-382 + 68*t2 - 20*Power(t2,2)) + 
               t1*(422 - 98*t2 + 4*Power(t2,2)) + 
               2*(-45 + 7*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(130 + 16*t2 + 5*Power(t2,2)) + 
               s1*(24 + 17*Power(t1,4) + 4*Power(t1,3)*(-10 + t2) - 
                  12*t1*(12 + t2) + 4*Power(t1,2)*(41 + 3*t2))) + 
            Power(s2,3)*(66 - 11*Power(t1,4) + 
               Power(s1,2)*(9 - 35*t1 + 4*Power(t1,2)) - 36*t2 + 
               13*Power(t2,2) - Power(t1,3)*(139 + 56*t2) + 
               Power(t1,2)*(164 + 79*t2 + 28*Power(t2,2)) - 
               t1*(50 + 28*t2 + 59*Power(t2,2)) + 
               s1*(30*Power(t1,3) + 2*(-8 + t2) - 
                  4*Power(t1,2)*(-7 + 3*t2) + 3*t1*(5 + 16*t2))) + 
            Power(s2,4)*(5 + Power(s1,2)*(3 - 14*t1) + 7*Power(t1,3) + 
               6*t2 + 25*Power(t2,2) + Power(t1,2)*(83 + 47*t2) - 
               2*t1*(58 + 39*t2 + 24*Power(t2,2)) + 
               s1*(23 - 20*Power(t1,2) - 16*t2 + 2*t1*(5 + 24*t2)))) + 
         s*(-1 + t1)*(Power(-1 + t1,2)*t1*(1 - 3*t1 + Power(t1,2)) + 
            s2*(-1 + t1)*(1 + 3*(-15 + 4*s1)*Power(t1,2) + 
               (-11 + 2*s1)*Power(t1,4) - t1*(-9 + 3*s1 + t2) - 
               Power(t1,3)*(-46 + 9*s1 + t2)) + 
            Power(s2,7)*(1 + 6*Power(s1,2) + Power(t1,2) + 
               2*s1*(-5 + 5*t1 - 6*t2) + 10*t2 + 6*Power(t2,2) - 
               2*t1*(1 + 5*t2)) + 
            Power(s2,6)*(13 + 2*Power(s1,2)*(-8 + t1) - 2*Power(t1,3) - 
               7*t2 - 8*Power(t2,2) + 17*Power(t1,2)*(1 + t2) - 
               2*t1*(14 + 5*t2 + 3*Power(t2,2)) + 
               s1*(5 - 19*Power(t1,2) + 24*t2 + 2*t1*(7 + 2*t2))) + 
            Power(s2,4)*(Power(s1,2)*
                (20 - 14*t1 + 15*Power(t1,2) + 15*Power(t1,3)) + 
               Power(t1,4)*(10 + 7*t2) + 
               Power(t1,3)*(50 + 11*t2 - 2*Power(t2,2)) + 
               5*(-3 + 10*t2 + 3*Power(t2,2)) - 
               Power(t1,2)*(145 + 17*t2 + 4*Power(t2,2)) + 
               t1*(100 - 51*t2 + 27*Power(t2,2)) - 
               2*s1*(33 + 2*Power(t1,4) + 13*t2 + 6*t1*(-3 + 2*t2) + 
                  Power(t1,3)*(32 + 3*t2) + Power(t1,2)*(-49 + 8*t2))) + 
            Power(s2,5)*(Power(t1,4) + 
               Power(s1,2)*(-1 + 17*t1 - 20*Power(t1,2)) - 
               2*Power(t1,3)*(11 + 6*t2) + 
               t1*(40 + 66*t2 + Power(t2,2)) - 
               3*(10 + 12*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(11 - 18*t2 + 4*Power(t2,2)) + 
               s1*(57 + 13*Power(t1,3) + 6*t2 - t1*(107 + 10*t2) + 
                  Power(t1,2)*(37 + 12*t2))) + 
            Power(s2,3)*(48 + 
               Power(s1,2)*(3 - 38*t1 + 23*Power(t1,2) - 
                  19*Power(t1,3) - 3*Power(t1,4)) - 22*t2 + 
               7*Power(t2,2) - Power(t1,5)*(3 + 2*t2) - 
               Power(t1,4)*(43 + 3*t2) + 
               t1*(-113 + 13*t2 - 49*Power(t2,2)) + 
               Power(t1,3)*(78 - t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(33 + 15*t2 + 4*Power(t2,2)) + 
               s1*(-17 + 28*Power(t1,2)*(-7 + t2) - 
                  8*Power(t1,3)*(-5 + t2) - 4*t2 + 
                  Power(t1,4)*(27 + 2*t2) + 2*t1*(73 + 25*t2))) + 
            Power(s2,2)*(6 + Power(s1,2)*t1*
                (-3 + 18*t1 - 8*Power(t1,2) + 3*Power(t1,3)) + 17*t2 - 
               11*Power(t2,2) + Power(t1,5)*(15 + 2*t2) - 
               Power(t1,4)*(2 + 7*t2) + 
               Power(t1,2)*(170 + 28*t2 - 4*Power(t2,2)) + 
               Power(t1,3)*(-109 + t2 - 2*Power(t2,2)) + 
               t1*(-80 - 41*t2 + 27*Power(t2,2)) + 
               s1*(-3 - 4*Power(t1,5) + 2*Power(t1,4)*(-15 + t2) + 
                  12*t1*(2 + t2) + 2*Power(t1,3)*(45 + 7*t2) - 
                  Power(t1,2)*(77 + 48*t2)))) + 
         Power(s,3)*(Power(s2,7)*(-1 + t1) + 
            t1*(-26 + 97*t1 - 127*Power(t1,2) + 71*Power(t1,3) - 
               16*Power(t1,4) + Power(t1,5)) + 
            Power(s2,6)*(14 - 5*Power(s1,2) + 11*Power(t1,2) + t2 - 
               9*Power(t2,2) - t1*(25 + 7*t2) + s1*(-3 + 9*t1 + 14*t2)) \
+ s2*(19 + (-1 + s1)*Power(t1,6) + Power(t1,3)*(629 - 170*s1 - 44*t2) + 
               t1*(93 - 43*s1 - 13*t2) + 
               Power(t1,5)*(49 - 17*s1 - 3*t2) + 
               Power(t1,4)*(-304 + 82*s1 + 22*t2) + 
               Power(t1,2)*(-485 + 149*s1 + 36*t2)) + 
            Power(s2,5)*(-53 - 9*Power(t1,3) + 
               Power(s1,2)*(-8 + 26*t1) - 36*t2 - 24*Power(t2,2) - 
               2*Power(t1,2)*(16 + 25*t2) + 
               s1*(21 - 92*t1 + 41*Power(t1,2) + 28*t2 - 76*t1*t2) + 
               2*t1*(47 + 58*t2 + 27*Power(t2,2))) + 
            Power(s2,2)*(-102 + Power(t1,6) + 
               Power(s1,2)*t1*
                (36 - 94*t1 + 44*Power(t1,2) + Power(t1,3)) - 11*t2 + 
               25*Power(t2,2) + Power(t1,5)*(29 + 11*t2) + 
               t1*(593 - 64*t2 - 38*Power(t2,2)) - 
               2*Power(t1,4)*(17 + 15*t2 + Power(t2,2)) - 
               2*Power(t1,2)*(388 - 50*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(289 - 4*t2 + 16*Power(t2,2)) - 
               s1*(-28 + 9*Power(t1,5) + Power(t1,4)*(-35 + 4*t2) + 
                  2*Power(t1,3)*(139 + 7*t2) - 
                  8*Power(t1,2)*(55 + 8*t2) + t1*(218 + 20*t2))) + 
            Power(s2,4)*(69 + 5*Power(t1,4) + 
               Power(s1,2)*(-19 + 38*t1 - 5*Power(t1,2)) - 44*t2 - 
               27*Power(t2,2) + Power(t1,3)*(151 + 80*t2) + 
               4*t1*(11 + 28*t2 + 20*Power(t2,2)) - 
               Power(t1,2)*(269 + 192*t2 + 51*Power(t2,2)) + 
               s1*(9 - 61*Power(t1,3) + t1*(25 - 98*t2) + 36*t2 + 
                  Power(t1,2)*(71 + 46*t2))) - 
            Power(s2,3)*(124 + 4*Power(t1,5) + 
               Power(s1,2)*(21 - 86*t1 + 73*Power(t1,2) + 
                  6*Power(t1,3)) - 58*t2 + Power(t2,2) + 
               4*Power(t1,4)*(28 + 13*t2) - 
               Power(t1,3)*(89 + 128*t2 + 18*Power(t2,2)) - 
               2*t1*(17 + 8*t2 + 23*Power(t2,2)) + 
               Power(t1,2)*(-117 + 130*t2 + 73*Power(t2,2)) + 
               s1*(-70 - 32*Power(t1,4) + 6*t2 + 
                  Power(t1,3)*(-15 + 2*t2) - 
                  2*Power(t1,2)*(48 + 43*t2) + t1*(233 + 54*t2)))) + 
         Power(s,2)*(-(Power(-1 + t1,2)*t1*
               (-8 + 22*t1 - 13*Power(t1,2) + 2*Power(t1,3))) + 
            2*Power(s2,7)*(-2 + Power(s1,2) - 2*Power(t1,2) - t2 + 
               Power(t2,2) + t1*(4 + t2) - s1*(-1 + t1 + 2*t2)) - 
            s2*(-1 + t1)*(-7 + 3*(-3 + s1)*Power(t1,5) + 
               Power(t1,2)*(231 - 65*s1 - 7*t2) + 
               Power(t1,4)*(98 - 23*s1 - 3*t2) + 
               t1*(-47 + 18*s1 + 5*t2) + 
               Power(t1,3)*(-266 + 63*s1 + 9*t2)) + 
            Power(s2,3)*(126 + 
               Power(s1,2)*(15 - 99*t1 + 122*Power(t1,2) - 
                  46*Power(t1,3) - 10*Power(t1,4)) - 51*t2 + 
               Power(t2,2) - Power(t1,5)*(42 + 19*t2) + 
               t1*(-192 + 13*t2 - 41*Power(t2,2)) + 
               4*Power(t1,4)*(-8 + 14*t2 + Power(t2,2)) - 
               2*Power(t1,3)*(-125 + 51*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(-110 + 103*t2 + 42*Power(t2,2)) + 
               s1*(9*Power(t1,5) + 4*(-17 + t2) + 
                  6*Power(t1,4)*(7 + t2) + 4*t1*(93 + 14*t2) + 
                  Power(t1,3)*(83 + 26*t2) - 2*Power(t1,2)*(219 + 28*t2))\
) + Power(s2,5)*(37 + 2*Power(t1,4) + 
               Power(s1,2)*(9 + 5*t1 + 2*Power(t1,2)) - 9*t2 + 
               15*Power(t2,2) - Power(t1,3)*(95 + 56*t2) - 
               t1*(165 + 78*t2 + 35*Power(t2,2)) + 
               Power(t1,2)*(221 + 143*t2 + 36*Power(t2,2)) + 
               s1*(30 + 57*Power(t1,3) - 24*t2 - 
                  2*Power(t1,2)*(62 + 19*t2) + t1*(37 + 30*t2))) + 
            Power(s2,2)*(52 + 
               Power(s1,2)*t1*
                (-18 + 82*t1 - 77*Power(t1,2) + 20*Power(t1,3) + 
                  Power(t1,4)) + Power(t1,5)*(23 - 6*t2) + 26*t2 - 
               24*Power(t2,2) + Power(t1,6)*(7 + 2*t2) + 
               Power(t1,2)*(783 - 17*t2 - 22*Power(t2,2)) + 
               Power(t1,3)*(-525 + 20*t2 - 13*Power(t2,2)) + 
               Power(t1,4)*(64 + 5*t2 + 4*Power(t2,2)) + 
               t1*(-404 - 30*t2 + 63*Power(t2,2)) + 
               s1*(-15 - 166*Power(t1,4) - Power(t1,6) + 
                  Power(t1,5)*(1 - 2*t2) + 12*Power(t1,3)*(37 + 5*t2) + 
                  t1*(139 + 18*t2) - 2*Power(t1,2)*(201 + 46*t2))) + 
            Power(s2,6)*(19 + Power(s1,2)*(9 - 21*t1) + 2*Power(t1,3) + 
               44*t2 + 17*Power(t2,2) + Power(t1,2)*(15 + 34*t2) - 
               t1*(36 + 78*t2 + 29*Power(t2,2)) + 
               s1*(-44 - 34*Power(t1,2) - 26*t2 + t1*(78 + 50*t2))) + 
            Power(s2,4)*(-99 + 
               Power(s1,2)*(15 - 46*t1 + 14*Power(t1,2) + 
                  21*Power(t1,3)) + 60*t2 + 5*Power(t2,2) + 
               Power(t1,4)*(96 + 43*t2) + 
               t1*(248 - 76*t2 - 30*Power(t2,2)) - 
               Power(t1,3)*(142 + 110*t2 + 19*Power(t2,2)) + 
               Power(t1,2)*(-103 + 83*t2 + 48*Power(t2,2)) - 
               s1*(49 + 34*Power(t1,4) + t1*(50 - 64*t2) - 
                  2*Power(t1,3)*(-6 + t2) + 12*t2 + 
                  Power(t1,2)*(-145 + 62*t2)))))*R1q(s2))/
     (s*(-1 + s2)*s2*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2))*
       (-s + s2 - t1)*Power(-1 + s + t1,3)*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*(s - s1 + t2)) + 
    (16*(Power(s,10)*t1 + 2*Power(1 + s2 - t1,2)*Power(-1 + t1,2)*
          (Power(s2,3) + t1 - 2*Power(t1,2) - Power(s2,2)*(1 + 2*t1) + 
            s2*(-1 + 3*t1 + Power(t1,2)))*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) + 
         Power(s,9)*(s2 - 7*s2*t1 + 8*Power(t1,2) + Power(t2,2) - 
            t1*(6 + s1 + 4*t2)) + 
         Power(s,8)*(27*Power(t1,3) + 3*Power(s2,2)*(-2 + 7*t1) + 
            (2 - 5*t2)*t2 - Power(t1,2)*(42 + 9*s1 + 29*t2) - 
            s2*(5 + s1 - 44*t1 - 6*s1*t1 + 48*Power(t1,2) + 6*t2 - 
               2*s1*t2 - 21*t1*t2 + 8*Power(t2,2)) + 
            t1*(13 + 19*t2 + 6*Power(t2,2) + 2*s1*(3 + t2))) + 
         Power(s,7)*(1 + 50*Power(t1,4) - 5*Power(s2,3)*(-3 + 7*t1) - 
            8*t2 + 7*Power(t2,2) - Power(t1,3)*(124 + 33*s1 + 93*t2) - 
            t1*(30 + 5*Power(s1,2) + 16*t2 + 25*Power(t2,2) + 
               s1*(-2 + 9*t2)) + 
            Power(t1,2)*(82 + Power(s1,2) + 121*t2 + 15*Power(t2,2) + 
               s1*(43 + 12*t2)) + 
            Power(s2,2)*(27 + Power(s1,2) + 117*Power(t1,2) + 
               s1*(3 - 17*t1 - 14*t2) + 29*t2 + 27*Power(t2,2) - 
               t1*(121 + 43*t2)) + 
            s2*(6 - Power(s1,2)*t1 - 136*Power(t1,3) + 9*t2 + 
               33*Power(t2,2) + Power(t1,2)*(237 + 134*t2) - 
               t1*(100 + 128*t2 + 47*Power(t2,2)) + 
               s1*(7 + 43*Power(t1,2) - 4*t2 + t1*(-45 + 8*t2)))) + 
         Power(s,6)*(-3 + 55*Power(t1,5) + 5*Power(s2,4)*(-4 + 7*t1) + 
            6*t2 + 7*Power(t2,2) - Power(t1,4)*(202 + 65*s1 + 173*t2) + 
            Power(t1,3)*(217 + 6*Power(s1,2) + 335*t2 + 
               20*Power(t2,2) + 2*s1*(62 + 15*t2)) - 
            Power(s2,3)*(49 + 6*Power(s1,2) + 144*Power(t1,2) + 
               s1*(2 - 30*t1 - 40*t2) + 54*t2 + 50*Power(t2,2) - 
               2*t1*(79 + 20*t2)) + 
            t1*(113 + 21*Power(s1,2) - 75*t2 + 14*Power(t2,2) + 
               s1*(-61 + 33*t2)) - 
            Power(t1,2)*(172 + 38*Power(s1,2) + 108*t2 + 
               49*Power(t2,2) + s1*(-21 + 46*t2)) + 
            Power(s2,2)*(-29 + 262*Power(t1,3) + 
               Power(s1,2)*(-2 + 15*t1) - 63*t2 - 93*Power(t2,2) + 
               s1*(-35 - 80*Power(t1,2) + t1*(89 - 90*t2) + 34*t2) - 
               Power(t1,2)*(499 + 248*t2) + 
               t1*(279 + 342*t2 + 151*Power(t2,2))) + 
            s2*(-22 - 206*Power(t1,4) - 
               5*Power(s1,2)*(1 - 6*t1 + 2*Power(t1,2)) + 29*t2 - 
               42*Power(t2,2) + Power(t1,3)*(561 + 375*t2) - 
               4*Power(t1,2)*(109 + 154*t2 + 30*Power(t2,2)) + 
               t1*(123 + 199*t2 + 165*Power(t2,2)) + 
               s1*(3 + 120*Power(t1,3) + t1*(69 - 19*t2) - 7*t2 + 
                  2*Power(t1,2)*(-115 + 9*t2)))) + 
         Power(s,5)*(36*Power(t1,6) - 3*Power(s2,5)*(-5 + 7*t1) + 
            5*(4 - 7*t2)*t2 - 5*Power(t1,5)*(40 + 15*s1 + 41*t2) - 
            Power(t1,3)*(409 + 119*Power(s1,2) + 95*s1*(-1 + t2) + 
               242*t2 + 45*Power(t2,2)) + 
            t1*(-275 - 37*Power(s1,2) + s1*(157 - 84*t2) + 208*t2 + 
               84*Power(t2,2)) + 
            Power(t1,4)*(316 + 15*Power(s1,2) + 525*t2 + 
               15*Power(t2,2) + 5*s1*(37 + 8*t2)) + 
            Power(s2,4)*(31 + 14*Power(s1,2) + 86*Power(t1,2) + 46*t2 + 
               55*Power(t2,2) - 10*t1*(9 + t2) - 5*s1*(7*t1 + 12*t2)) + 
            Power(t1,2)*(539 + 129*Power(s1,2) - 321*t2 - 
               29*Power(t2,2) + s1*(-349 + 161*t2)) + 
            s2*(102 - 180*Power(t1,5) + 
               Power(s1,2)*(16 - 120*t1 + 184*Power(t1,2) - 
                  33*Power(t1,3)) - 112*t2 - 15*Power(t2,2) + 
               20*Power(t1,4)*(37 + 30*t2) - 
               2*t1*(143 - 66*t2 + 61*Power(t2,2)) - 
               Power(t1,3)*(881 + 1410*t2 + 175*Power(t2,2)) + 
               Power(t1,2)*(473 + 822*t2 + 350*Power(t2,2)) + 
               s1*(-58 + 165*Power(t1,4) + Power(t1,2)*(179 - 87*t2) + 
                  t1*(148 - 80*t2) + 46*t2 + Power(t1,3)*(-474 + 40*t2))\
) + Power(s2,3)*(63 + Power(s1,2)*(15 - 60*t1) - 225*Power(t1,3) + 
               135*t2 + 148*Power(t2,2) + Power(t1,2)*(477 + 242*t2) - 
               2*t1*(177 + 241*t2 + 130*Power(t2,2)) + 
               s1*(36 + 60*Power(t1,2) - 104*t2 + t1*(-17 + 252*t2))) + 
            Power(s2,2)*(15 + 304*Power(t1,4) + 
               Power(s1,2)*(20 - 92*t1 + 62*Power(t1,2)) + 14*t2 + 
               111*Power(t2,2) - Power(t1,3)*(955 + 623*t2) + 
               Power(t1,2)*(940 + 1315*t2 + 364*Power(t2,2)) - 
               t1*(273 + 650*t2 + 452*Power(t2,2)) + 
               s1*(35 - 117*Power(t1,3) - 22*t2 - 
                  3*Power(t1,2)*(-93 + 88*t2) + t1*(-202 + 265*t2)))) - 
         s*(-1 + t1)*(Power(s2,7)*
             (1 + 6*Power(s1,2) + Power(t1,2) + 
               2*s1*(-5 + 5*t1 - 6*t2) + 10*t2 + 6*Power(t2,2) - 
               2*t1*(1 + 5*t2)) + 
            Power(-1 + t1,2)*
             (-2*Power(-1 + t2,2) - t1*(-1 + t2)*(-25 + 8*s1 + t2) + 
               Power(t1,6)*(2 + 3*t2) - 
               Power(t1,5)*(2 - s1 + Power(s1,2) + 6*t2) + 
               Power(t1,4)*(29 + 30*Power(s1,2) + s1*(-60 + t2) - 
                  17*t2 - Power(t2,2)) + 
               Power(t1,3)*(-99 - 24*Power(s1,2) + s1*(106 - 58*t2) + 
                  95*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(96 + 6*Power(s1,2) - 101*t2 + 
                  3*Power(t2,2) + s1*(-59 + 43*t2))) + 
            Power(s2,6)*(17 + Power(s1,2)*(4 - 18*t1) - 6*Power(t1,3) + 
               27*t2 + 12*Power(t2,2) + Power(t1,2)*(29 + 51*t2) - 
               2*t1*(20 + 39*t2 + 13*Power(t2,2)) + 
               s1*(-29 - 53*Power(t1,2) - 16*t2 + t1*(82 + 44*t2))) + 
            s2*(-1 + t1)*(-25 + Power(t1,7) + 
               Power(s1,2)*t1*
                (12 - 80*t1 + 181*Power(t1,2) - 130*Power(t1,3) - 
                  5*Power(t1,4)) + 26*t2 - Power(t2,2) - 
               Power(t1,6)*(19 + 20*t2) + 
               Power(t1,3)*(224 - 142*t2 - 87*Power(t2,2)) - 
               2*Power(t1,4)*(14 + 30*t2 + 5*Power(t2,2)) + 
               Power(t1,5)*(14 + 66*t2 + 5*Power(t2,2)) - 
               2*t1*(-88 + 85*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-343 + 298*t2 + 81*Power(t2,2)) + 
               s1*(8 + 5*Power(t1,6) + Power(t1,2)*(379 - 223*t2) + 
                  Power(t1,5)*(10 - 8*t2) - 8*t2 + 2*t1*(-59 + 43*t2) + 
                  2*Power(t1,3)*(-218 + 57*t2) + 
                  Power(t1,4)*(154 + 83*t2))) + 
            Power(s2,5)*(14 + 15*Power(t1,4) + 
               Power(s1,2)*(4 - 11*t1 + 3*Power(t1,2)) + 6*t2 + 
               26*Power(t2,2) - 4*Power(t1,3)*(27 + 28*t2) + 
               Power(t1,2)*(185 + 224*t2 + 57*Power(t2,2)) - 
               t1*(106 + 118*t2 + 87*Power(t2,2)) + 
               s1*(11 + 117*Power(t1,3) - 34*t2 - 
                  Power(t1,2)*(217 + 64*t2) + t1*(89 + 106*t2))) + 
            Power(s2,4)*(-20*Power(t1,5) + 
               Power(s1,2)*(19 - 68*t1 + 38*Power(t1,2) + 
                  47*Power(t1,3)) + 3*(-9 + 7*t2) + 
               Power(t1,4)*(184 + 141*t2) + 
               t1*(22 - 90*t2 - 109*Power(t2,2)) - 
               2*Power(t1,3)*(170 + 177*t2 + 40*Power(t2,2)) + 
               Power(t1,2)*(181 + 282*t2 + 225*Power(t2,2)) + 
               s1*(13 - 138*Power(t1,4) - 26*t2 + 
                  Power(t1,3)*(251 + 56*t2) + t1*(-75 + 214*t2) - 
                  Power(t1,2)*(51 + 316*t2))) + 
            Power(s2,2)*(57 - 6*Power(t1,7) + 
               Power(s1,2)*(6 - 76*t1 + 289*Power(t1,2) - 
                  427*Power(t1,3) + 182*Power(t1,4) + 36*Power(t1,5)) - 
               47*t2 - 12*Power(t2,2) + Power(t1,6)*(81 + 61*t2) + 
               Power(t1,2)*(205 - 45*t2 - 227*Power(t2,2)) - 
               2*Power(t1,5)*(67 + 118*t2 + 15*Power(t2,2)) + 
               Power(t1,3)*(11 - 259*t2 + 43*Power(t2,2)) + 
               t1*(-213 + 141*t2 + 116*Power(t2,2)) + 
               Power(t1,4)*(-1 + 385*t2 + 120*Power(t2,2)) + 
               s1*(-51 - 33*Power(t1,6) + t1*(322 - 186*t2) + 35*t2 + 
                  2*Power(t1,5)*(5 + 14*t2) + 
                  8*Power(t1,2)*(-79 + 22*t2) + 
                  Power(t1,3)*(446 + 264*t2) - Power(t1,4)*(62 + 337*t2)\
)) + Power(s2,3)*(6 + 15*Power(t1,6) + 
               Power(s1,2)*(20 - 131*t1 + 265*Power(t1,2) - 
                  120*Power(t1,3) - 68*Power(t1,4)) + 12*t2 - 
               34*Power(t2,2) - 2*Power(t1,5)*(83 + 57*t2) + 
               Power(t1,4)*(301 + 360*t2 + 68*Power(t2,2)) + 
               t1*(68 - 112*t2 + 83*Power(t2,2)) + 
               Power(t1,2)*(-140 + 302*t2 + 103*Power(t2,2)) - 
               2*Power(t1,3)*(42 + 224*t2 + 127*Power(t2,2)) + 
               s1*(-57 + 92*Power(t1,5) + 29*t2 + 27*t1*(5 + t2) - 
                  2*Power(t1,4)*(61 + 22*t2) - 
                  Power(t1,2)*(19 + 415*t2) + Power(t1,3)*(-29 + 471*t2))\
)) + Power(s,4)*(10 + 13*Power(t1,7) + Power(s2,6)*(-6 + 7*t1) - 50*t2 + 
            49*Power(t2,2) - 3*Power(t1,6)*(42 + 17*s1 + 53*t2) + 
            Power(t1,2)*(-1078 - 184*Power(s1,2) + s1*(727 - 364*t2) + 
               889*t2 + 258*Power(t2,2)) + 
            Power(t1,5)*(279 + 20*Power(s1,2) + 505*t2 + 
               6*Power(t2,2) + 30*s1*(5 + t2)) - 
            Power(s2,5)*(-6 + 16*Power(s1,2) + 12*Power(t1,2) + 
               s1*(3 - 26*t1 - 50*t2) + 14*t2 + 36*Power(t2,2) + 
               t1*(2 + 11*t2)) - 
            Power(t1,4)*(524 + 200*Power(s1,2) + 278*t2 + 
               15*Power(t2,2) + 2*s1*(-109 + 50*t2)) + 
            t1*(363 + 35*Power(s1,2) - 217*t2 - 187*Power(t2,2) + 
               2*s1*(-101 + 61*t2)) + 
            2*Power(t1,3)*(532 + 162*Power(s1,2) - 347*t2 - 
               58*Power(t2,2) + s1*(-419 + 161*t2)) + 
            Power(s2,4)*(-70 + 59*Power(t1,3) + 
               Power(s1,2)*(-39 + 106*t1) - 159*t2 - 147*Power(t2,2) - 
               3*Power(t1,2)*(58 + 51*t2) + 
               t1*(206 + 409*t2 + 260*Power(t2,2)) + 
               s1*(37 + 15*Power(t1,2) + 160*t2 - t1*(137 + 338*t2))) + 
            s2*(-205 - 92*Power(t1,6) + 
               Power(s1,2)*(-21 + 200*t1 - 532*Power(t1,2) + 
                  450*Power(t1,3) - 50*Power(t1,4)) + 166*t2 + 
               89*Power(t2,2) + Power(t1,5)*(606 + 595*t2) + 
               t1*(765 - 759*t2 - 212*Power(t2,2)) + 
               Power(t1,2)*(-871 + 310*t2 - 82*Power(t2,2)) - 
               Power(t1,4)*(1011 + 1830*t2 + 160*Power(t2,2)) + 
               2*Power(t1,3)*(399 + 769*t2 + 205*Power(t2,2)) + 
               s1*(119 + 110*Power(t1,5) + Power(t1,3)*(102 - 250*t2) + 
                  Power(t1,2)*(716 - 186*t2) - 88*t2 + 
                  5*Power(t1,4)*(-93 + 14*t2) + t1*(-600 + 362*t2))) - 
            Power(s2,3)*(27 + 176*Power(t1,4) + 
               Power(s1,2)*(42 - 183*t1 + 166*Power(t1,2)) + 137*t2 + 
               171*Power(t2,2) - 60*Power(t1,3)*(13 + 10*t2) + 
               2*Power(t1,2)*(540 + 788*t2 + 277*Power(t2,2)) - 
               t1*(473 + 1022*t2 + 666*Power(t2,2)) + 
               s1*(51 + 92*Power(t1,3) - 129*t2 - 
                  Power(t1,2)*(227 + 620*t2) + t1*(9 + 663*t2))) + 
            Power(s2,2)*(-61 + 201*Power(t1,5) + 
               2*Power(s1,2)*
                (-22 + 131*t1 - 196*Power(t1,2) + 53*Power(t1,3)) + 
               130*t2 - Power(t2,2) - 2*Power(t1,4)*(539 + 436*t2) + 
               2*Power(t1,3)*(810 + 1246*t2 + 243*Power(t2,2)) + 
               t1*(128 + 304*t2 + 381*Power(t2,2)) - 
               Power(t1,2)*(785 + 2051*t2 + 924*Power(t2,2)) + 
               s1*(61 - 8*Power(t1,4) + Power(t1,3)*(230 - 432*t2) + 
                  t1*(28 - 213*t2) - 67*t2 + Power(t1,2)*(-310 + 836*t2))\
)) + Power(s,2)*(-2*Power(s2,7)*
             (-2 + Power(s1,2) - 2*Power(t1,2) - t2 + Power(t2,2) + 
               t1*(4 + t2) - s1*(-1 + t1 + 2*t2)) + 
            Power(s2,6)*(-9 - 12*Power(t1,3) + 
               3*Power(s1,2)*(-9 + 13*t1) + Power(t1,2)*(15 - 42*t2) - 
               52*t2 - 35*Power(t2,2) + 
               t1*(6 + 94*t2 + 47*Power(t2,2)) + 
               s1*(52 + 42*Power(t1,2) + 62*t2 - 2*t1*(47 + 43*t2))) + 
            Power(s2,5)*(-75 + 2*Power(t1,4) + 
               Power(s1,2)*(-31 + 125*t1 - 110*Power(t1,2)) - 110*t2 - 
               77*Power(t2,2) + 11*Power(t1,3)*(11 + 19*t2) - 
               Power(t1,2)*(323 + 568*t2 + 184*Power(t2,2)) + 
               t1*(275 + 469*t2 + 245*Power(t2,2)) + 
               s1*(83 - 204*Power(t1,3) + 108*t2 - 10*t1*(41 + 37*t2) + 
                  3*Power(t1,2)*(177 + 98*t2))) - 
            Power(-1 + t1,2)*
             (-9 + 22*t2 - 13*Power(t2,2) + 
               Power(t1,6)*(14 + 3*s1 + 23*t2) + 
               t1*(-118 - 4*Power(s1,2) + s1*(55 - 43*t2) + 79*t2 + 
                  37*Power(t2,2)) + 
               Power(t1,3)*(-384 + 387*s1 - 107*Power(s1,2) + 388*t2 - 
                  175*s1*t2 + 64*Power(t2,2)) - 
               Power(t1,5)*(23 + 6*Power(s1,2) + 55*t2 + 
                  2*s1*(1 + t2)) + 
               Power(t1,4)*(112 + 98*Power(s1,2) - 69*t2 - 
                  5*Power(t2,2) + 2*s1*(-96 + 5*t2)) + 
               Power(t1,2)*(405 + 40*Power(s1,2) - 376*t2 - 
                  62*Power(t2,2) + s1*(-263 + 168*t2))) - 
            s2*(-1 + t1)*(-106 + 6*Power(t1,7) + 
               Power(s1,2)*(-4 + 72*t1 - 323*Power(t1,2) + 
                  581*Power(t1,3) - 364*Power(t1,4) + 6*Power(t1,5)) + 
               75*t2 + 29*Power(t2,2) - 3*Power(t1,6)*(37 + 43*t2) + 
               Power(t1,3)*(637 - 537*t2 - 211*Power(t2,2)) + 
               Power(t1,5)*(183 + 459*t2 + 32*Power(t2,2)) - 
               Power(t1,4)*(158 + 384*t2 + 81*Power(t2,2)) - 
               2*t1*(-316 + 273*t2 + 86*Power(t2,2)) + 
               Power(t1,2)*(-1083 + 1058*t2 + 371*Power(t2,2)) + 
               s1*(51 + 12*Power(t1,6) + Power(t1,2)*(1168 - 622*t2) + 
                  Power(t1,5)*(32 - 38*t2) - 39*t2 + 
                  2*Power(t1,3)*(-569 + 108*t2) + 
                  Power(t1,4)*(339 + 241*t2) + t1*(-460 + 306*t2))) + 
            Power(s2,4)*(5 + 32*Power(t1,5) + 
               Power(s1,2)*(-28 + 167*t1 - 247*Power(t1,2) + 
                  104*Power(t1,3)) - 57*t2 - 58*Power(t2,2) - 
               Power(t1,4)*(448 + 415*t2) + 
               Power(t1,3)*(1100 + 1351*t2 + 334*Power(t2,2)) + 
               t1*(290 + 567*t2 + 421*Power(t2,2)) - 
               Power(t1,2)*(979 + 1446*t2 + 701*Power(t2,2)) + 
               s1*(-41 + 361*Power(t1,4) + 86*t2 - 15*t1*(9 + 40*t2) - 
                  Power(t1,3)*(1007 + 450*t2) + 
                  Power(t1,2)*(822 + 972*t2))) + 
            Power(s2,2)*(-148 + 28*Power(t1,7) - 
               Power(s1,2)*(28 - 245*t1 + 792*Power(t1,2) - 
                  1068*Power(t1,3) + 488*Power(t1,4) + 13*Power(t1,5)) \
+ 133*t2 + 70*Power(t2,2) - 5*Power(t1,6)*(77 + 64*t2) + 
               t1*(436 - 490*t2 - 350*Power(t2,2)) + 
               Power(t1,5)*(923 + 1378*t2 + 163*Power(t2,2)) + 
               Power(t1,3)*(87 + 1144*t2 + 319*Power(t2,2)) + 
               Power(t1,2)*(-283 + 265*t2 + 365*Power(t2,2)) - 
               Power(t1,4)*(658 + 2110*t2 + 575*Power(t2,2)) + 
               s1*(150 + 120*Power(t1,6) + 
                  Power(t1,2)*(1121 - 155*t2) - 103*t2 + 
                  143*t1*(-5 + 3*t2) - 3*Power(t1,5)*(73 + 62*t2) - 
                  Power(t1,3)*(674 + 987*t2) + 
                  Power(t1,4)*(217 + 1018*t2))) + 
            Power(s2,3)*(-48*Power(t1,6) + 
               Power(s1,2)*(-41 + 268*t1 - 568*Power(t1,2) + 
                  377*Power(t1,3) - 18*Power(t1,4)) + 
               Power(t1,5)*(602 + 464*t2) + 
               13*(3 + t2 + 4*Power(t2,2)) + 
               t1*(-196 + 228*t2 + 57*Power(t2,2)) - 
               Power(t1,4)*(1467 + 1770*t2 + 326*Power(t2,2)) + 
               2*Power(t1,3)*(601 + 1180*t2 + 474*Power(t2,2)) - 
               Power(t1,2)*(132 + 1295*t2 + 713*Power(t2,2)) + 
               s1*(t1 - 306*Power(t1,5) - 43*(-1 + t2) - 245*t1*t2 + 
                  Power(t1,4)*(803 + 384*t2) + 
                  3*Power(t1,2)*(6 + 419*t2) - 
                  Power(t1,3)*(559 + 1389*t2)))) + 
         Power(s,3)*(-(Power(s2,7)*(-1 + t1)) + 
            Power(s2,6)*(-14 + 9*Power(s1,2) - 11*Power(t1,2) + 
               s1*(5 - 11*t1 - 22*t2) - 3*t2 + 13*Power(t2,2) + 
               t1*(25 + 9*t2)) + 
            (-1 + t1)*(15 + 2*Power(t1,7) - 48*t2 + 35*Power(t2,2) - 
               Power(t1,6)*(50 + 19*s1 + 79*t2) + 
               Power(t1,4)*(-288 - 180*Power(s1,2) + s1*(314 - 43*t2) + 
                  40*t2 + 6*Power(t2,2)) + 
               Power(t1,5)*(104 + 15*Power(s1,2) + 220*t2 + 
                  Power(t2,2) + 6*s1*(7 + 2*t2)) + 
               Power(t1,2)*(-869 - 116*Power(s1,2) + 767*t2 + 
                  203*Power(t2,2) - 7*s1*(-83 + 47*t2)) + 
               Power(t1,3)*(812 + 246*Power(s1,2) - 754*t2 - 
                  133*Power(t2,2) + 5*s1*(-152 + 59*t2)) + 
               t1*(277 + 18*Power(s1,2) - 158*t2 - 129*Power(t2,2) + 
                  s1*(-146 + 99*t2))) + 
            Power(s2,5)*(38 + Power(s1,2)*(47 - 93*t1) + 
               26*Power(t1,3) + 118*t2 + 93*Power(t2,2) + 
               Power(t1,2)*(-17 + 86*t2) - 
               t1*(47 + 234*t2 + 151*Power(t2,2)) + 
               s1*(-87 - 61*Power(t1,2) - 136*t2 + 2*t1*(89 + 120*t2))) + 
            Power(s2,2)*(151 + 85*Power(t1,6) + 
               Power(s1,2)*(47 - 344*t1 + 825*Power(t1,2) - 
                  650*Power(t1,3) + 69*Power(t1,4)) - 204*t2 - 
               100*Power(t2,2) - 23*Power(t1,5)*(35 + 31*t2) + 
               t1*(-230 + 403*t2 + 156*Power(t2,2)) + 
               Power(t1,4)*(1632 + 2567*t2 + 379*Power(t2,2)) + 
               Power(t1,2)*(207 + 922*t2 + 510*Power(t2,2)) - 
               Power(t1,3)*(1040 + 2977*t2 + 998*Power(t2,2)) + 
               s1*(-167 + 133*Power(t1,5) + t1*(474 - 230*t2) + 141*t2 - 
                  Power(t1,4)*(119 + 398*t2) + 
                  2*Power(t1,3)*(-13 + 649*t2) - 
                  Power(t1,2)*(293 + 705*t2))) + 
            Power(s2,4)*(91 + 20*Power(t1,4) + 
               Power(s1,2)*(52 - 215*t1 + 203*Power(t1,2)) + 185*t2 + 
               158*Power(t2,2) - 3*Power(t1,3)*(116 + 139*t2) + 
               Power(t1,2)*(748 + 1195*t2 + 451*Power(t2,2)) - 
               t1*(511 + 919*t2 + 557*Power(t2,2)) + 
               s1*(-41 + 273*Power(t1,3) - 188*t2 - 
                  Power(t1,2)*(715 + 632*t2) + t1*(439 + 728*t2))) + 
            s2*(206 - 28*Power(t1,7) + 
               Power(s1,2)*(14 - 173*t1 + 652*Power(t1,2) - 
                  1004*Power(t1,3) + 560*Power(t1,4) - 35*Power(t1,5)) - 
               139*t2 - 85*Power(t2,2) + Power(t1,6)*(329 + 366*t2) + 
               4*Power(t1,3)*(-290 + 92*t2 + 19*Power(t2,2)) - 
               Power(t1,5)*(710 + 1396*t2 + 93*Power(t2,2)) - 
               2*Power(t1,2)*(-863 + 858*t2 + 282*Power(t2,2)) + 
               Power(t1,4)*(710 + 1549*t2 + 285*Power(t2,2)) + 
               t1*(-1073 + 970*t2 + 395*Power(t2,2)) + 
               s1*(-113 + 21*Power(t1,6) + t1*(807 - 522*t2) + 82*t2 + 
                  3*Power(t1,5)*(-67 + 24*t2) - 
                  2*Power(t1,3)*(-727 + 66*t2) - 
                  Power(t1,4)*(199 + 370*t2) + 
                  Power(t1,2)*(-1771 + 842*t2))) - 
            Power(s2,3)*(49 + 93*Power(t1,5) + 
               Power(s1,2)*(-50 + 299*t1 - 453*Power(t1,2) + 
                  168*Power(t1,3)) - 23*t2 - 43*Power(t2,2) - 
               17*Power(t1,4)*(51 + 44*t2) + 
               t1*(143 + 790*t2 + 577*Power(t2,2)) + 
               Power(t1,3)*(1763 + 2428*t2 + 600*Power(t2,2)) - 
               Power(t1,2)*(1181 + 2427*t2 + 1166*Power(t2,2)) + 
               s1*(336*Power(t1,4) + t1*(67 - 700*t2) + 31*(-1 + t2) - 
                  7*Power(t1,3)*(113 + 104*t2) + 
                  Power(t1,2)*(399 + 1465*t2)))))*R1q(1 - s + s2 - t1))/
     (s*(-1 + s2)*Power(-s + s2 - t1,3)*(1 - s + s2 - t1)*
       Power(-1 + s + t1,3)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*
       (s - s1 + t2)) + (8*(2*Power(s,6)*t1 + 
         Power(s,5)*(s2*(2 - 10*t1) + 7*Power(t1,2) + 2*Power(t2,2) - 
            t1*(2 + 2*s1 + 9*t2)) + 
         Power(s,4)*(-55*t1 + 11*Power(t1,2) + 6*Power(t1,3) + 
            4*Power(s2,2)*(-2 + 5*t1) + 4*t2 + 15*t1*t2 - 
            11*Power(t1,2)*t2 - 4*Power(t2,2) - t1*Power(t2,2) + 
            s1*(t1*(14 - 9*t1 + 4*t2) + s2*(-2 + 11*t1 + 4*t2)) + 
            s2*(t1 - 28*Power(t1,2) + 29*t1*t2 - t2*(13 + 12*t2))) + 
         Power(s,3)*(2 + Power(t1,4) - 4*Power(s2,3)*(-3 + 5*t1) - 
            4*t2 - 4*Power(t2,2) - 2*Power(t1,3)*(-5 + 4*s1 + t2) + 
            t1*(150 - 10*Power(s1,2) - 3*t2 + 2*s1*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*(-10 + 2*Power(s1,2) + 44*Power(t1,2) + 
               t1*(23 - 32*t2) + s1*(5 - 24*t1 - 20*t2) + 36*t2 + 
               28*Power(t2,2)) + 
            Power(t1,2)*(-144 + 2*Power(s1,2) + 32*t2 - 3*Power(t2,2) + 
               s1*(12 + t2)) - 
            s2*(59 + 2*Power(s1,2)*t1 + 18*Power(t1,3) + 
               Power(t1,2)*(47 - 17*t2) + 10*t2 - 5*Power(t2,2) + 
               2*t1*(-90 + 17*t2) + 
               s1*(-34*Power(t1,2) - 4*(4 + t2) + t1*(35 + 3*t2)))) - 
         Power(-1 + s2,2)*((-1 + s1)*Power(t1,4) - 4*Power(-1 + t2,2) + 
            Power(s2,3)*(-2 + 4*Power(s1,2) + s1*(3 - 3*t1 - 8*t2) - 
               t2 + 4*Power(t2,2) + t1*(2 + t2)) + 
            Power(t1,2)*(-15 + 44*t2 + Power(t2,2) - 3*s1*(7 + 3*t2)) + 
            Power(t1,3)*(-1 - 2*Power(s1,2) - 16*t2 + s1*(16 + 3*t2)) - 
            t1*(-21 + 2*Power(s1,2) + 36*t2 + Power(t2,2) - 
               2*s1*(2 + 7*t2)) + 
            s2*(-13 + Power(s1,2)*(2 + 4*t1 + 6*Power(t1,2)) + 20*t2 + 
               9*Power(t2,2) + Power(t1,3)*(2 + 5*t2) + 
               Power(t1,2)*(1 + 31*t2 - Power(t2,2)) + 
               2*t1*(5 - 28*t2 + 2*Power(t2,2)) - 
               s1*(4 + 7*Power(t1,3) + 14*t2 + 
                  Power(t1,2)*(33 + 4*t2) + t1*(-44 + 6*t2))) + 
            Power(s2,2)*(1 - 4*Power(s1,2)*(1 + 2*t1) + 20*t2 - 
               9*Power(t2,2) - 3*Power(t1,2)*(1 + 2*t2) + 
               t1*(2 - 14*t2 - 3*Power(t2,2)) + 
               s1*(-23 + 9*Power(t1,2) + 15*t2 + t1*(14 + 9*t2)))) + 
         Power(s,2)*(-((1 + s1)*Power(t1,4)) + 
            2*Power(s2,4)*(-4 + 5*t1) + 4*t2*(-3 + 4*t2) + 
            Power(t1,3)*(-61 + 2*Power(s1,2) + s1*(4 - 3*t2) + 16*t2) - 
            Power(s2,3)*(-34 + 8*Power(s1,2) + 32*Power(t1,2) + 
               s1*(3 - 26*t1 - 36*t2) + t1*(39 - 12*t2) + 32*t2 + 
               32*Power(t2,2)) + 
            t1*(-104 + 22*Power(s1,2) + 33*t2 - 6*s1*(9 + 5*t2)) + 
            Power(t1,2)*(190 - 12*Power(s1,2) - 71*t2 + 5*Power(t2,2) + 
               s1*(62 + 7*t2)) + 
            s2*(87 - 2*Power(t1,4) - 
               2*Power(s1,2)*(5 - 9*t1 + 4*Power(t1,2)) + 11*t2 + 
               9*Power(t2,2) - Power(t1,3)*(34 + 5*t2) + 
               t1*(-335 + 133*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(143 - 51*t2 + 7*Power(t2,2)) + 
               s1*(16 + 21*Power(t1,3) - 6*t2 + 
                  Power(t1,2)*(11 + 6*t2) - 3*t1*(13 + 8*t2))) + 
            Power(s2,2)*(109 + 20*Power(t1,3) + 
               2*Power(s1,2)*(1 + 5*t1) + 30*t2 + 19*Power(t2,2) + 
               Power(t1,2)*(92 + 5*t2) + 
               t1*(-133 + 43*t2 + 6*Power(t2,2)) - 
               s1*(27 + 50*Power(t1,2) + 19*t2 + t1*(14 + 15*t2)))) + 
         s*(-6 - 2*Power(s2,5)*(-1 + t1) + (-1 + 2*s1)*Power(t1,4) + 
            20*t2 - 14*Power(t2,2) + 
            Power(t1,3)*(20 - 4*Power(s1,2) - 30*t2 + 6*s1*(4 + t2)) + 
            Power(t1,2)*(-27 + 10*Power(s1,2) + 94*t2 - Power(t2,2) - 
               17*s1*(6 + t2)) + 
            Power(s2,4)*(-26 + 10*Power(s1,2) + 9*Power(t1,2) + 
               s1*(3 - 14*t1 - 28*t2) + 8*t2 + 18*Power(t2,2) + 
               t1*(19 + t2)) - 
            2*t1*(-15 + 7*Power(s1,2) + 36*t2 + Power(t2,2) - 
               s1*(23 + 19*t2)) + 
            s2*(-3 - 4*Power(t1,4) - 
               2*Power(s1,2)*
                (-6 + 4*t1 - 5*Power(t1,2) + 2*Power(t1,3)) + 16*t2 + 
               15*Power(t2,2) - 2*Power(t1,3)*(24 + 13*t2) + 
               2*t1*(23 - 96*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-25 + 129*t2 + 6*Power(t2,2)) + 
               s1*(2*Power(t1,4) + t1*(159 - 7*t2) - 
                  6*Power(t1,2)*(5 + 4*t2) + Power(t1,3)*(32 + 6*t2) - 
                  2*(25 + 8*t2))) + 
            Power(s2,2)*(Power(t1,4) + 
               2*Power(s1,2)*(2 - t1 + 6*Power(t1,2)) + 
               4*Power(t1,3)*(10 + 3*t2) + 
               Power(t1,2)*(38 + 70*t2 - 5*Power(t2,2)) + 
               t1*(5 - 117*t2 + 6*Power(t2,2)) + 
               2*(-31 + 77*t2 + 9*Power(t2,2)) - 
               s1*(43 + 20*Power(t1,3) + 30*t2 - 14*t1*(1 + t2) + 
                  Power(t1,2)*(98 + 11*t2))) - 
            Power(s2,3)*(-31 + 8*Power(t1,3) + 2*Power(s1,2)*(5 + 8*t1) + 
               10*t2 + 37*Power(t2,2) + Power(t1,2)*(55 + 17*t2) + 
               t1*(-26 + 40*t2 + 8*Power(t2,2)) - 
               s1*(-22 + 34*Power(t1,2) + 46*t2 + t1*(63 + 23*t2)))))*R2q(s)\
)/(s*(-1 + s2)*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2))*
       (-s + s2 - t1)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*(s - s1 + t2)) \
+ (8*(Power(s,8)*(-Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
         Power(s,7)*(8*Power(t1,2) - 3*Power(t1,3) + 2*t2 + 6*t1*t2 - 
            3*Power(t1,2)*t2 - 6*Power(t2,2) + 2*t1*Power(t2,2) + 
            s1*(Power(t1,2) + t1*(-2 + t2) + 2*s2*t2) + 
            s2*(6*Power(t1,2) - t2*(4 + 9*t2) + t1*(-2 + 15*t2))) + 
         Power(s,6)*(1 - 3*Power(t1,4) - 10*t2 + 11*Power(t2,2) + 
            Power(t1,3)*(11 + 3*s1 + 2*t2) + 
            t1*(-6 + s1*(9 - 5*t2) + 19*t2 - 10*Power(t2,2)) + 
            Power(t1,2)*(-17 + t2 + Power(t2,2) + s1*(-17 + 2*t2)) + 
            Power(s2,2)*(Power(s1,2) - 15*Power(t1,2) + 
               t1*(11 - 48*t2) - s1*(2 + t1 + 16*t2) + t2*(27 + 35*t2)) \
+ s2*(-2 + 15*Power(t1,3) - (2 + 7*s1)*t2 + 38*Power(t2,2) + 
               Power(t1,2)*(-35 - 6*s1 + 25*t2) - 
               t1*(-19 + Power(s1,2) + s1*(-13 + t2) + 39*t2 + 
                  17*Power(t2,2)))) + 
         Power(s,5)*(-Power(t1,5) + 
            Power(t1,3)*(20 + s1*(-28 + t2) - 28*t2) + 
            Power(t1,4)*(1 + 3*s1 + 5*t2) + 
            Power(t1,2)*(-79 + s1*(83 - 6*t2) + 53*t2 - 3*Power(t2,2)) + 
            4*(-1 + 3*t2 + Power(t2,2)) + 
            t1*(31 + 2*s1*(-4 + t2) - 97*t2 + 11*Power(t2,2)) + 
            Power(s2,3)*(-7*Power(s1,2) + 20*Power(t1,2) + 
               s1*(11 + 5*t1 + 54*t2) - t2*(75 + 77*t2) + 
               t1*(-24 + 85*t2)) + 
            Power(s2,2)*(19 - 30*Power(t1,3) + 
               Power(s1,2)*(-3 + 10*t1) + Power(t1,2)*(56 - 80*t2) - 
               11*t2 - 93*Power(t2,2) + 
               t1*(-58 + 121*t2 + 60*Power(t2,2)) + 
               s1*(2 + 13*Power(t1,2) + 36*t2 - 22*t1*(2 + t2))) + 
            s2*(-5 - 3*Power(s1,2)*(-1 + t1)*t1 + 12*Power(t1,4) + 
               61*t2 - 42*Power(t2,2) + Power(t1,3)*(-29 + 5*t2) + 
               Power(t1,2)*(48 - 16*t2 - 9*Power(t2,2)) + 
               3*t1*(-7 - 21*t2 + 19*Power(t2,2)) + 
               s1*(1 - 15*Power(t1,3) + Power(t1,2)*(80 - 9*t2) + 
                  t1*(-51 + 8*t2)))) + 
         Power(-1 + s2,3)*(2*Power(s2,5)*Power(s1 - t2,2) - 
            t1*(8*s1 + 9*(-3 + t2))*(-1 + t2) + 4*Power(-1 + t2,2) + 
            (-1 + s1)*Power(t1,4)*(1 + t2) - 
            Power(t1,3)*(17 + 2*Power(s1,2) - 8*t2 + Power(t2,2) + 
               4*s1*(-5 + 2*t2)) - 
            Power(s2,4)*(s1 - t2)*
             (6 + s1*(-1 + 7*t1) - t2 - t1*(6 + 5*t2)) + 
            Power(t1,2)*(41 + 4*Power(s1,2) - 35*t2 + 8*Power(t2,2) + 
               s1*(-29 + 11*t2)) + 
            s2*(Power(s1,2)*t1*
                (-8 - 2*t1 + 3*Power(t1,2) + Power(t1,3)) + 
               Power(t1,4)*(2 + t2) + 
               Power(t1,2)*(11 + 13*t2 - 8*Power(t2,2)) - 
               3*(-5 + 4*t2 + Power(t2,2)) + 
               Power(t1,3)*(-1 + 2*t2 + 2*Power(t2,2)) + 
               t1*(-27 - 4*t2 + 3*Power(t2,2)) - 
               s1*(8 + Power(t1,2)*(23 - 9*t2) + 2*t1*(-19 + t2) - 
                  8*t2 + 2*Power(t1,3)*(2 + t2) + Power(t1,4)*(3 + t2))) \
- Power(s2,2)*(2 + Power(s1,2)*
                (-4 - 10*t1 + 5*Power(t1,2) + 5*Power(t1,3)) - 15*t2 - 
               Power(t2,2) + t1*(2 + 8*t2 - 8*Power(t2,2)) + 
               Power(t1,3)*(6 + 6*t2 + Power(t2,2)) + 
               Power(t1,2)*(-10 + t2 + 4*Power(t2,2)) + 
               s1*(-5*Power(t1,2)*(-1 + t2) + 9*(1 + t2) - 
                  6*Power(t1,3)*(2 + t2) + 2*t1*(-1 + 5*t2))) + 
            Power(s2,3)*(4 + Power(s1,2)*(-6 + t1 + 9*Power(t1,2)) - 
               5*t2 - 3*Power(t2,2) + t1*(-8 - 6*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(4 + 11*t2 + 4*Power(t2,2)) + 
               s1*(1 + t1*(14 - 4*t2) + 9*t2 - Power(t1,2)*(15 + 13*t2)))\
) + Power(s,3)*(12 + Power(t1,5)*(18 - 3*s1 - 10*t2) - 70*t2 + 
            74*Power(t2,2) + t1*
             (-27 + s1*(74 - 75*t2) + 44*t2 - 80*Power(t2,2)) + 
            Power(t1,4)*(-197 + 58*t2 + s1*(47 + 3*t2)) - 
            Power(t1,3)*(-494 + 18*Power(s1,2) + 121*t2 + 
               4*Power(t2,2) + s1*(83 + 20*t2)) + 
            Power(t1,2)*(-345 + 40*Power(s1,2) + 111*t2 + 
               34*Power(t2,2) + s1*(-34 + 48*t2)) + 
            Power(s2,5)*(-30*Power(s1,2) + 6*Power(t1,2) + 
               2*s1*(13 + 5*t1 + 55*t2) + t1*(-14 + 57*t2) - 
               t2*(90 + 91*t2)) + 
            Power(s2,4)*(20 - 15*Power(t1,3) + 
               4*Power(s1,2)*(-4 + 17*t1) + 2*t2 - 40*Power(t2,2) - 
               Power(t1,2)*(7 + 115*t2) + 
               s1*(12 - 139*t1 + 3*Power(t1,2) + 28*t2 - 155*t1*t2) + 
               2*t1*(13 + 120*t2 + 65*Power(t2,2))) + 
            s2*(-40 - Power(s1,2)*t1*
                (23 - 18*t1 - 16*Power(t1,2) + Power(t1,3)) + 
               Power(t1,3)*(717 - 193*t2) + Power(t1,5)*(7 - 2*t2) + 
               74*t2 + 43*Power(t2,2) + Power(t1,4)*(-56 + 55*t2) + 
               t1*(454 - 22*t2 - 46*Power(t2,2)) + 
               Power(t1,2)*(-1145 + 204*t2 + 10*Power(t2,2)) + 
               s1*(-5 - 3*Power(t1,5) - 3*Power(t1,4)*(-13 + t2) - 
                  18*t2 + t1*(-109 + 2*t2) + Power(t1,2)*(319 + 3*t2) + 
                  Power(t1,3)*(-336 + 9*t2))) + 
            Power(s2,3)*(12*Power(t1,4) + 
               Power(s1,2)*(6 + 29*t1 - 51*Power(t1,2)) + 
               10*Power(t1,3)*(4 + 7*t2) + 
               2*(-8 + 47*t2 + 8*Power(t2,2)) - 
               Power(t1,2)*(9 + 123*t2 + 50*Power(t2,2)) + 
               t1*(-36 - 88*t2 + 70*Power(t2,2)) + 
               s1*(15 - 27*Power(t1,3) + 3*t1*(-43 + t2) - 52*t2 + 
                  2*Power(t1,2)*(94 + 19*t2))) + 
            Power(s2,2)*(-103 - 3*Power(t1,5) + 
               Power(s1,2)*(7 + 18*t1 - 45*Power(t1,2) + 
                  14*Power(t1,3)) + 10*t2 + 30*Power(t2,2) - 
               Power(t1,4)*(34 + 9*t2) + 
               Power(t1,2)*(-602 + 183*t2 - 26*Power(t2,2)) + 
               t1*(667 - 79*t2 - 10*Power(t2,2)) + 
               4*Power(t1,3)*(12 - 14*t2 + Power(t2,2)) + 
               s1*(36 + 18*Power(t1,4) + Power(t1,2)*(472 - 29*t2) - 
                  32*t2 + 5*Power(t1,3)*(-26 + 3*t2) + t1*(-357 + 29*t2))\
)) + Power(s,4)*(2 + Power(t1,4)*(50 - 16*s1 - 33*t2) + 20*t2 - 
            45*Power(t2,2) + Power(t1,5)*(-2 + s1 + 2*t2) + 
            Power(t1,3)*(-264 + 91*t2 + Power(t2,2) + s1*(109 + 2*t2)) - 
            Power(t1,2)*(-327 + 12*Power(s1,2) + 143*t2 + 
               6*Power(t2,2) + s1*(118 + 7*t2)) + 
            t1*(-42 + 112*t2 + 25*Power(t2,2) + s1*(-26 + 30*t2)) + 
            Power(s2,4)*(20*Power(s1,2) - 15*Power(t1,2) + 
               t1*(26 - 90*t2) + 5*t2*(22 + 21*t2) - 
               2*s1*(12 + 5*t1 + 50*t2)) - 
            s2*(-37 - 3*Power(t1,5) + 
               Power(s1,2)*t1*(3 - 14*t1 + 3*Power(t1,2)) + 116*t2 + 
               17*Power(t2,2) + Power(t1,4)*(-8 + 7*t2) + 
               Power(t1,3)*(74 - 68*t2 + Power(t2,2)) - 
               2*Power(t1,2)*(209 - 85*t2 + 10*Power(t2,2)) + 
               t1*(205 - 187*t2 + 32*Power(t2,2)) + 
               s1*(3 + 12*Power(t1,4) + Power(t1,2)*(310 - 21*t2) - 
                  22*t2 + 3*t1*(-41 + 2*t2) + Power(t1,3)*(-103 + 9*t2))\
) + Power(s2,2)*(-12 - 18*Power(t1,4) + 
               Power(s1,2)*(-1 - 19*t1 + 20*Power(t1,2)) + 
               Power(t1,3)*(6 - 40*t2) - 113*t2 + 40*Power(t2,2) + 
               t1*(26 + 74*t2 - 110*Power(t2,2)) + 
               6*Power(t1,2)*(-8 + 11*t2 + 5*Power(t2,2)) + 
               s1*(2 + 29*Power(t1,3) + 3*Power(t1,2)*(-53 + t2) + 
                  11*t2 + t1*(128 + 13*t2))) + 
            Power(s2,3)*(-38 + Power(s1,2)*(13 - 37*t1) + 
               30*Power(t1,3) + 16*t2 + 105*Power(t2,2) + 
               Power(t1,2)*(-33 + 130*t2) + 
               t1*(42 - 220*t2 - 115*Power(t2,2)) + 
               s1*(-8 - 12*Power(t1,2) - 62*t2 + t1*(99 + 90*t2)))) + 
         s*(16 - 96*t1 + 40*s1*t1 + 120*Power(t1,2) - 
            142*s1*Power(t1,2) + 24*Power(s1,2)*Power(t1,2) - 
            85*Power(t1,3) + 135*s1*Power(t1,3) - 
            22*Power(s1,2)*Power(t1,3) + 46*Power(t1,4) - 
            40*s1*Power(t1,4) + 8*Power(s1,2)*Power(t1,4) - 
            Power(t1,5) + s1*Power(t1,5) - 40*t2 + 143*t1*t2 - 
            40*s1*t1*t2 - 97*Power(t1,2)*t2 + 46*s1*Power(t1,2)*t2 + 
            Power(t1,3)*t2 - 29*s1*Power(t1,3)*t2 + Power(t1,4)*t2 + 
            5*s1*Power(t1,4)*t2 - 2*Power(t1,5)*t2 + 24*Power(t2,2) - 
            45*t1*Power(t2,2) + 33*Power(t1,2)*Power(t2,2) - 
            4*Power(t1,3)*Power(t2,2) + 
            Power(s2,7)*(-11*Power(s1,2) + (-7 + 3*t1 - 15*t2)*t2 + 
               s1*(3 + t1 + 26*t2)) - 
            Power(s2,3)*(-28 + 
               Power(s1,2)*(-17 + 16*t1 + 31*Power(t1,2) - 
                  2*Power(t1,3) + 3*Power(t1,4)) - 129*t2 - 
               5*Power(t2,2) - Power(t1,5)*(1 + 2*t2) - 
               Power(t1,4)*(22 + 15*t2) + 
               Power(t1,2)*(308 + 67*t2 - 14*Power(t2,2)) + 
               Power(t1,3)*(-23 + 63*t2 + 8*Power(t2,2)) - 
               2*t1*(117 - 50*t2 + 9*Power(t2,2)) + 
               s1*(99 + Power(t1,5) + Power(t1,3)*(23 - 25*t2) + 
                  Power(t1,2)*(-317 + t2) + 10*t2 + 
                  Power(t1,4)*(7 + t2) + t1*(103 + 11*t2))) + 
            s2*(17 + Power(s1,2)*t1*
                (-40 + 25*t1 - 38*Power(t1,2) + 17*Power(t1,3)) + 
               21*t2 - 40*Power(t2,2) + Power(t1,5)*(7 + 6*t2) - 
               Power(t1,4)*(8 + 11*t2) + 
               Power(t1,2)*(143 + 64*t2 - 41*Power(t2,2)) + 
               2*Power(t1,3)*(-92 + 19*t2 + 4*Power(t2,2)) + 
               t1*(25 - 142*t2 + 53*Power(t2,2)) + 
               s1*(-7*Power(t1,5) + 24*(-1 + t2) - 
                  Power(t1,4)*(29 + 3*t2) + 6*t1*(16 + 9*t2) - 
                  3*Power(t1,2)*(51 + 10*t2) + Power(t1,3)*(141 + 11*t2)\
)) + Power(s2,2)*(20 + Power(s1,2)*
                (16 - 20*t1 + 79*Power(t1,2) - 24*Power(t1,3) - 
                  6*Power(t1,4)) + 3*Power(t1,4)*(-13 + t2) - 27*t2 + 
               15*Power(t2,2) + 3*Power(t1,3)*(61 + t2) - 
               Power(t1,5)*(7 + 6*t2) + 
               Power(t1,2)*(121 + 45*t2 - 22*Power(t2,2)) + 
               t1*(-278 + 32*t2 + 6*Power(t2,2)) + 
               s1*(-22 + 7*Power(t1,5) + t1*(229 - 43*t2) - 
                  Power(t1,4)*(-41 + t2) - 32*t2 + 
                  Power(t1,3)*(-91 + 5*t2) + Power(t1,2)*(-214 + 27*t2))\
) + Power(s2,6)*(-7 + Power(s1,2)*(11 + 34*t1) - 9*t2 + 
               27*Power(t2,2) - 5*Power(t1,2)*(1 + 2*t2) + 
               t1*(12 + 49*t2 + 32*Power(t2,2)) - 
               s1*(-14 + Power(t1,2) + 40*t2 + t1*(43 + 64*t2))) + 
            Power(s2,4)*(-79 + 
               Power(s1,2)*(-33 + 22*t1 + 37*Power(t1,2) + 
                  18*Power(t1,3)) - 108*t2 - 18*Power(t2,2) - 
               Power(t1,4)*(5 + 8*t2) + 
               t1*(50 + 92*t2 - 9*Power(t2,2)) + 
               2*Power(t1,3)*(-5 + 4*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(44 + 122*t2 + 37*Power(t2,2)) + 
               s1*(3*Power(t1,4) - 4*Power(t1,3)*(8 + 3*t2) + 
                  8*(16 + 7*t2) - t1*(134 + 9*t2) - 
                  Power(t1,2)*(71 + 93*t2))) + 
            Power(s2,5)*(21 - 
               2*Power(s1,2)*(-8 + 22*t1 + 19*Power(t1,2)) + 41*t2 + 
               2*Power(t2,2) + Power(t1,3)*(9 + 13*t2) - 
               11*t1*(1 + 7*t2 + 5*Power(t2,2)) - 
               Power(t1,2)*(19 + 57*t2 + 21*Power(t2,2)) + 
               s1*(-2*Power(t1,3) - 8*(4 + 3*t2) + 
                  Power(t1,2)*(72 + 51*t2) + t1*(42 + 113*t2)))) + 
         Power(s,2)*(-23 + 113*t1 - 79*s1*t1 + 28*Power(t1,2) + 
            198*s1*Power(t1,2) - 48*Power(s1,2)*Power(t1,2) - 
            190*Power(t1,3) - 116*s1*Power(t1,3) + 
            38*Power(s1,2)*Power(t1,3) + 154*Power(t1,4) - 
            9*s1*Power(t1,4) - 8*Power(s1,2)*Power(t1,4) - 
            38*Power(t1,5) + 5*s1*Power(t1,5) + 78*t2 - 189*t1*t2 + 
            79*s1*t1*t2 + 43*Power(t1,2)*t2 - 72*s1*Power(t1,2)*t2 + 
            63*Power(t1,3)*t2 + 38*s1*Power(t1,3)*t2 - 
            32*Power(t1,4)*t2 - 7*s1*Power(t1,4)*t2 + 10*Power(t1,5)*t2 - 
            59*Power(t2,2) + 88*t1*Power(t2,2) - 
            51*Power(t1,2)*Power(t2,2) + 6*Power(t1,3)*Power(t2,2) + 
            Power(s2,6)*(25*Power(s1,2) - Power(t1,2) + t1*(3 - 20*t2) + 
               t2*(39 + 49*t2) - s1*(14 + 5*t1 + 72*t2)) + 
            Power(s2,3)*(259 + Power(t1,5) + 
               Power(s1,2)*(9 - 27*t1 + 26*Power(t1,2) - 
                  24*Power(t1,3)) + 106*t2 - 4*Power(t2,2) + 
               Power(t1,4)*(30 + 19*t2) + 
               Power(t1,2)*(195 - 154*t2 - 12*Power(t2,2)) + 
               Power(t1,3)*(10 + 14*t2 - 6*Power(t2,2)) + 
               t1*(-563 - 79*t2 + 56*Power(t2,2)) - 
               s1*(157 + 12*Power(t1,4) + Power(t1,2)*(230 - 63*t2) + 
                  Power(t1,3)*(-75 + t2) + 14*t2 + 6*t1*(-71 + 9*t2))) + 
            Power(s2,2)*(73 + 
               Power(s1,2)*(-16 + 45*t1 - 5*Power(t1,2) - 
                  32*Power(t1,3) + 3*Power(t1,4)) + 141*t2 - 
               5*Power(t2,2) - 2*Power(t1,5)*(5 + t2) - 
               2*Power(t1,4)*(10 + 19*t2) + 
               Power(t1,2)*(1176 - 71*t2 - 34*Power(t2,2)) + 
               Power(t1,3)*(-457 + 121*t2 + 2*Power(t2,2)) + 
               2*t1*(-328 - 50*t2 + 13*Power(t2,2)) + 
               s1*(-17 + 3*Power(t1,5) + Power(t1,3)*(330 - 16*t2) + 
                  t1*(89 - 5*t2) + Power(t1,4)*(-13 + 5*t2) + 
                  Power(t1,2)*(-483 + 26*t2))) + 
            s2*(-4 + Power(s1,2)*t1*
                (56 - 68*t1 + 34*Power(t1,2) + 5*Power(t1,3)) + 
               Power(t1,4)*(231 - 45*t2) - 22*t2 + 12*Power(t2,2) + 
               Power(t1,5)*(7 + 8*t2) + 
               Power(t1,2)*(577 + 11*t2 - 12*Power(t2,2)) + 
               Power(t1,3)*(-705 + 108*t2 - 2*Power(t2,2)) + 
               3*t1*(-72 - 16*t2 + 5*Power(t2,2)) + 
               s1*(23 + Power(t1,2)*(102 - 9*t2) + 
                  2*Power(t1,4)*(-59 + t2) - 15*t2 + 
                  11*Power(t1,3)*(9 + t2) - t1*(58 + 35*t2))) + 
            Power(s2,4)*(-17 - 3*Power(t1,4) + 
               9*Power(s1,2)*(-2 + t1 + 7*Power(t1,2)) - 50*t2 - 
               33*Power(t2,2) - Power(t1,3)*(37 + 50*t2) + 
               t1*(56 + 123*t2 + 30*Power(t2,2)) + 
               Power(t1,2)*(41 + 118*t2 + 45*Power(t2,2)) + 
               s1*(-5 + 12*Power(t1,3) + t1*(5 - 92*t2) + 68*t2 - 
                  Power(t1,2)*(149 + 72*t2))) + 
            Power(s2,5)*(-67*Power(s1,2)*t1 + 3*Power(t1,3) + 
               Power(t1,2)*(16 + 53*t2) - 4*(-2 + t2 + 6*Power(t2,2)) - 
               t1*(41 + 151*t2 + 87*Power(t2,2)) + 
               s1*(-14 + 2*Power(t1,2) + 33*t2 + t1*(110 + 139*t2)))))*
       T2q(s2,s))/
     (s*(-1 + s2)*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2))*
       (-s + s2 - t1)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*
       (s - s1 + t2)) + (8*(Power(s,8)*
          (Power(t1,2) + 2*t1*t2 - Power(t2,2)) - 
         Power(s,7)*(8*Power(t1,2) - 6*Power(t1,3) + 2*t2 + 6*t1*t2 - 
            13*Power(t1,2)*t2 - 6*Power(t2,2) + 5*t1*Power(t2,2) + 
            s1*(Power(t1,2) + t1*(-2 + t2) + 2*s2*t2) + 
            s2*(3*Power(t1,2) - 2*t2*(2 + 3*t2) + t1*(-2 + 9*t2))) + 
         Power(s,6)*(-1 + 15*Power(t1,4) + 10*t2 - 11*Power(t2,2) + 
            Power(t1,3)*(-40 - 6*s1 + 37*t2) + 
            Power(t1,2)*(21 + s1*(19 - 5*t2) - 37*t2 - 10*Power(t2,2)) + 
            t1*(6 - 25*t2 + 25*Power(t2,2) + s1*(-9 + 5*t2)) + 
            Power(s2,2)*(-Power(s1,2) + 3*Power(t1,2) + 
               5*t1*(-1 + 3*t2) + s1*(2 + t1 + 10*t2) - t2*(15 + 14*t2)) \
+ s2*(2 - 15*Power(t1,3) + Power(t1,2)*(38 + 3*s1 - 51*t2) + 
               (-4 + 7*s1)*t2 - 29*Power(t2,2) + 
               t1*(-19 + Power(s1,2) + 59*t2 + 29*Power(t2,2) - 
                  s1*(7 + 8*t2)))) - 
         Power(-1 + t1,3)*(2*Power(s2,5)*Power(s1 - t2,2) - 
            t1*(8*s1 + 9*(-3 + t2))*(-1 + t2) + 4*Power(-1 + t2,2) + 
            (-1 + s1)*Power(t1,4)*(1 + t2) - 
            Power(t1,3)*(17 + 2*Power(s1,2) - 8*t2 + Power(t2,2) + 
               4*s1*(-5 + 2*t2)) - 
            Power(s2,4)*(s1 - t2)*
             (6 + s1*(-1 + 7*t1) - t2 - t1*(6 + 5*t2)) + 
            Power(t1,2)*(41 + 4*Power(s1,2) - 35*t2 + 8*Power(t2,2) + 
               s1*(-29 + 11*t2)) + 
            s2*(Power(s1,2)*t1*
                (-8 - 2*t1 + 3*Power(t1,2) + Power(t1,3)) + 
               Power(t1,4)*(2 + t2) + 
               Power(t1,2)*(11 + 13*t2 - 8*Power(t2,2)) - 
               3*(-5 + 4*t2 + Power(t2,2)) + 
               Power(t1,3)*(-1 + 2*t2 + 2*Power(t2,2)) + 
               t1*(-27 - 4*t2 + 3*Power(t2,2)) - 
               s1*(8 + Power(t1,2)*(23 - 9*t2) + 2*t1*(-19 + t2) - 
                  8*t2 + 2*Power(t1,3)*(2 + t2) + Power(t1,4)*(3 + t2))) \
- Power(s2,2)*(2 + Power(s1,2)*
                (-4 - 10*t1 + 5*Power(t1,2) + 5*Power(t1,3)) - 15*t2 - 
               Power(t2,2) + t1*(2 + 8*t2 - 8*Power(t2,2)) + 
               Power(t1,3)*(6 + 6*t2 + Power(t2,2)) + 
               Power(t1,2)*(-10 + t2 + 4*Power(t2,2)) + 
               s1*(-5*Power(t1,2)*(-1 + t2) + 9*(1 + t2) - 
                  6*Power(t1,3)*(2 + t2) + 2*t1*(-1 + 5*t2))) + 
            Power(s2,3)*(4 + Power(s1,2)*(-6 + t1 + 9*Power(t1,2)) - 
               5*t2 - 3*Power(t2,2) + t1*(-8 - 6*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(4 + 11*t2 + 4*Power(t2,2)) + 
               s1*(1 + t1*(14 - 4*t2) + 9*t2 - Power(t1,2)*(15 + 13*t2)))\
) + Power(s,5)*(20*Power(t1,5) - 3*Power(t1,4)*(27 + 5*s1 - 20*t2) + 
            t1*(-34 - 2*s1*(-4 + t2) + 121*t2 - 29*Power(t2,2)) - 
            4*(-1 + 3*t2 + Power(t2,2)) - 
            2*Power(t1,3)*(-45 + 49*t2 + 5*Power(t2,2) + 
               s1*(-32 + 5*t2)) + 
            Power(t1,2)*(25 - 80*t2 + 39*Power(t2,2) + 
               4*s1*(-17 + 5*t2)) + 
            Power(s2,3)*(4*Power(s1,2) - Power(t1,2) + t1*(3 - 11*t2) + 
               2*t2*(9 + 8*t2) - s1*(5 + 2*t1 + 18*t2)) + 
            s2*(2 - 32*Power(t1,4) + 3*Power(s1,2)*t1*(-1 + 2*t1) - 
               49*t2 + 48*Power(t2,2) - 8*Power(t1,3)*(-17 + 15*t2) + 
               t1*(53 - 58*t2 - 114*Power(t2,2)) + 
               Power(t1,2)*(-173 + 242*t2 + 57*Power(t2,2)) + 
               s1*(-1 + 15*Power(t1,3) + t1*(34 + 13*t2) - 
                  Power(t1,2)*(55 + 13*t2))) + 
            Power(s2,2)*(-13 + Power(s1,2)*(3 - 10*t1) + 
               16*Power(t1,3) + 45*t2 + 54*Power(t2,2) + 
               Power(t1,2)*(-58 + 72*t2) + 
               t1*(43 - 144*t2 - 63*Power(t2,2)) + 
               s1*(-2 + 2*Power(t1,2) - 33*t2 + t1*(23 + 49*t2)))) + 
         s*Power(-1 + t1,2)*(2*Power(t1,5)*(-1 + 5*s1 - 5*t2) + 
            2*Power(s2,5)*(-4 + 3*s1 + 4*t1 - 3*t2)*(s1 - t2) + 
            Power(t1,6)*(1 - s1 + 2*t2) - 8*(2 - 5*t2 + 3*Power(t2,2)) + 
            Power(t1,3)*(46 + 8*Power(s1,2) + 44*s1*(-2 + t2) + 11*t2 + 
               7*Power(t2,2)) + 
            t1*(100 + 40*s1*(-1 + t2) - 159*t2 + 57*Power(t2,2)) + 
            Power(t1,4)*(12 + 2*Power(s1,2) + 6*t2 + Power(t2,2) - 
               s1*(29 + 8*t2)) - 
            Power(t1,2)*(141 + 20*Power(s1,2) - 110*t2 + 
               51*Power(t2,2) + 4*s1*(-37 + 14*t2)) + 
            s2*(-29 + Power(s1,2)*t1*(32 + t1 - 27*Power(t1,2)) + 
               27*t2 + 4*Power(t2,2) - Power(t1,5)*(7 + 10*t2) + 
               2*t1*(8 + 45*t2 + 3*Power(t2,2)) + 
               Power(t1,4)*(10 + 54*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(51 - 97*t2 + 14*Power(t2,2)) - 
               Power(t1,3)*(41 + 64*t2 + 21*Power(t2,2)) + 
               s1*(7*Power(t1,5) - 24*(-1 + t2) - 
                  Power(t1,4)*(36 + t2) + Power(t1,2)*(24 + 7*t2) + 
                  3*Power(t1,3)*(35 + 8*t2) - 2*t1*(62 + 9*t2))) + 
            Power(s2,4)*(12 - 6*Power(s1,2)*(-3 + 2*t1) + 31*t2 + 
               20*Power(t2,2) + Power(t1,2)*(12 + 25*t2) - 
               2*t1*(12 + 28*t2 + 7*Power(t2,2)) + 
               s1*(-31 - 25*Power(t1,2) - 38*t2 + t1*(56 + 26*t2))) + 
            Power(s2,2)*(22 + 6*Power(s1,2)*(-2 - t1 + 9*Power(t1,2)) - 
               46*t2 - 6*Power(t2,2) + Power(t1,4)*(21 + 23*t2) - 
               t1*(31 + 25*t2 + 2*Power(t2,2)) - 
               Power(t1,3)*(29 + 117*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(17 + 165*t2 + 54*Power(t2,2)) + 
               s1*(12 - 20*Power(t1,4) + t1*(52 - 36*t2) + 38*t2 + 
                  Power(t1,3)*(70 + 6*t2) - 2*Power(t1,2)*(57 + 40*t2))) \
+ Power(s2,3)*(-7 + Power(s1,2)*(-3 - 47*t1 + 6*Power(t1,2)) + 37*t2 - 
               4*Power(t2,2) - Power(t1,3)*(27 + 32*t2) + 
               Power(t1,2)*(47 + 119*t2 + 15*Power(t2,2)) - 
               t1*(13 + 124*t2 + 55*Power(t2,2)) + 
               s1*(-12 + 31*Power(t1,3) + 9*t2 - 
                  Power(t1,2)*(92 + 19*t2) + t1*(73 + 98*t2)))) + 
         Power(s,4)*(-2 + 15*Power(t1,6) - 20*t2 + 45*Power(t2,2) + 
            Power(t1,5)*(-82 - 20*s1 + 60*t2) + 
            t1*(51 + s1*(26 - 30*t2) - 124*t2 - 55*Power(t2,2)) + 
            Power(t1,4)*(146 - 10*s1*(-11 + t2) - 146*t2 - 
               5*Power(t2,2)) + 
            Power(t1,3)*(33 + 2*Power(s1,2) - 97*t2 + 28*Power(t2,2) + 
               5*s1*(-37 + 5*t2)) + 
            Power(t1,2)*(-161 + 4*Power(s1,2) + 357*t2 - 
               13*Power(t2,2) + s1*(39 + 9*t2)) + 
            Power(s2,4)*(-5*Power(s1,2) + (-7 + 3*t1 - 9*t2)*t2 + 
               s1*(3 + t1 + 14*t2)) + 
            s2*(-34 - 36*Power(t1,5) + 
               Power(s1,2)*t1*(3 - 26*t1 + 15*Power(t1,2)) + 140*t2 - 
               25*Power(t2,2) - 7*Power(t1,4)*(-29 + 22*t2) + 
               Power(t1,2)*(233 - 275*t2 - 179*Power(t2,2)) + 
               Power(t1,3)*(-408 + 469*t2 + 60*Power(t2,2)) + 
               2*t1*(21 - 103*t2 + 67*Power(t2,2)) + 
               s1*(3 + 34*Power(t1,4) - 22*t2 + 
                  6*Power(t1,2)*(40 + t2) - 3*Power(t1,3)*(54 + 5*t2) + 
                  t1*(-89 + 49*t2))) + 
            Power(s2,3)*(5 - 15*Power(t1,3) + 
               Power(s1,2)*(-13 + 25*t1) + Power(t1,2)*(47 - 43*t2) - 
               66*t2 - 51*Power(t2,2) + 
               t1*(-37 + 127*t2 + 65*Power(t2,2)) + 
               s1*(20 - 7*Power(t1,2) + 56*t2 - t1*(31 + 82*t2))) + 
            Power(s2,2)*(43 + 34*Power(t1,4) + 
               Power(s1,2)*(1 + 25*t1 - 35*Power(t1,2)) - 32*t2 - 
               73*Power(t2,2) + Power(t1,3)*(-173 + 141*t2) + 
               Power(t1,2)*(283 - 428*t2 - 114*Power(t2,2)) + 
               t1*(-187 + 317*t2 + 188*Power(t2,2)) + 
               s1*(-9 - 12*Power(t1,3) + 22*t2 + 
                  Power(t1,2)*(85 + 103*t2) - t1*(62 + 117*t2)))) + 
         Power(s,2)*(-1 + t1)*
          (-23 + Power(t1,7) + 
            Power(t1,5)*(16 - s1*(-49 + t2) - 52*t2) + 78*t2 - 
            59*Power(t2,2) + Power(t1,6)*(-5 - 6*s1 + 13*t2) + 
            2*Power(s2,5)*(-2 + 3*Power(s1,2) + 4*t1 - 2*Power(t1,2) - 
               6*s1*t2 + 3*Power(t2,2)) + 
            t1*(123 + 79*s1*(-1 + t2) - 249*t2 + 146*Power(t2,2)) + 
            Power(t1,3)*(-40 + 12*Power(s1,2) + 165*t2 + 
               22*Power(t2,2) + 2*s1*(-51 + 43*t2)) - 
            2*Power(t1,2)*(57 + 16*Power(s1,2) - 24*t2 + 
               63*Power(t2,2) + s1*(-131 + 61*t2)) + 
            Power(t1,4)*(6*Power(s1,2) - 2*s1*(62 + 7*t2) + 
               3*(14 - t2 + Power(t2,2))) + 
            Power(s2,4)*(-8 + Power(s1,2)*(18 - 36*t1) + 
               12*Power(t1,3) + 33*t2 + 24*Power(t2,2) + 
               Power(t1,2)*(-32 + 31*t2) + 
               t1*(28 - 64*t2 - 42*Power(t2,2)) + 
               s1*(-25 - 23*Power(t1,2) - 42*t2 + 6*t1*(8 + 13*t2))) + 
            s2*(11 - 5*Power(t1,6) + 
               Power(s1,2)*t1*
                (32 + 10*t1 - 65*Power(t1,2) + 9*Power(t1,3)) + 
               Power(t1,5)*(18 - 51*t2) - 28*t2 - 9*Power(t2,2) + 
               Power(t1,4)*(-61 + 246*t2 + 15*Power(t2,2)) + 
               t1*(-164 + 305*t2 + 22*Power(t2,2)) + 
               Power(t1,2)*(255 - 208*t2 + 29*Power(t2,2)) - 
               Power(t1,3)*(54 + 264*t2 + 71*Power(t2,2)) + 
               s1*(23 + 27*Power(t1,5) - 15*t2 - 
                  5*Power(t1,4)*(31 + 2*t2) + 
                  5*Power(t1,2)*(-38 + 15*t2) + 
                  Power(t1,3)*(372 + 46*t2) - t1*(77 + 68*t2))) + 
            Power(s2,3)*(-21 - 15*Power(t1,4) + 
               Power(s1,2)*(3 - 81*t1 + 54*Power(t1,2)) + 
               Power(t1,3)*(38 - 81*t2) + 97*t2 + 24*Power(t2,2) + 
               t1*(50 - 305*t2 - 121*Power(t2,2)) + 
               Power(t1,2)*(-52 + 289*t2 + 73*Power(t2,2)) + 
               s1*(-39 + 55*Power(t1,3) - 19*t2 - 
                  Power(t1,2)*(179 + 119*t2) + t1*(163 + 186*t2))) - 
            Power(s2,2)*(-39 - 11*Power(t1,5) + 
               Power(s1,2)*(8 + 9*t1 - 114*Power(t1,2) + 
                  33*Power(t1,3)) + Power(t1,4)*(29 - 88*t2) + 38*t2 - 
               2*Power(t2,2) - 
               6*Power(t1,2)*(-15 + 95*t2 + 27*Power(t2,2)) + 
               t1*(17 + 199*t2 + 47*Power(t2,2)) + 
               Power(t1,3)*(-86 + 421*t2 + 53*Power(t2,2)) + 
               s1*(22 + 53*Power(t1,4) - 51*t2 + t1*(-196 + 55*t2) - 
                  Power(t1,3)*(228 + 65*t2) + Power(t1,2)*(349 + 189*t2))\
)) + Power(s,3)*(-12 + 6*Power(t1,7) + 2*Power(s2,5)*Power(s1 - t2,2) + 
            70*t2 - 74*Power(t2,2) + Power(t1,6)*(-40 - 15*s1 + 37*t2) + 
            2*Power(t1,4)*(12 + 3*Power(s1,2) - 9*t2 + 5*Power(t2,2) + 
               s1*(-125 + 2*t2)) - 
            Power(t1,5)*(-102 + 130*t2 + Power(t2,2) + 
               s1*(-106 + 5*t2)) + 
            Power(t1,3)*(-218 + 8*Power(s1,2) + 414*t2 + 
               20*Power(t2,2) + s1*(56 + 62*t2)) + 
            t1*(30 - 116*t2 + 185*Power(t2,2) + s1*(-74 + 75*t2)) - 
            Power(t1,2)*(-108 + 20*Power(s1,2) + 257*t2 + 
               146*Power(t2,2) + s1*(-177 + 124*t2)) + 
            Power(s2,4)*(-8 + Power(s1,2)*(16 - 22*t1) + 
               12*Power(t1,3) + 27*t2 + 26*Power(t2,2) + 
               Power(t1,2)*(-32 + 9*t2) - 
               4*t1*(-7 + 9*t2 + 8*Power(t2,2)) + 
               3*s1*(-5 + Power(t1,2) - 14*t2 + 2*t1*(2 + 9*t2))) + 
            s2*(55 - 21*Power(t1,6) + 
               Power(s1,2)*t1*
                (7 + 31*t1 - 70*Power(t1,2) + 18*Power(t1,3)) + 
               Power(t1,5)*(133 - 117*t2) - 134*t2 - 10*Power(t2,2) + 
               Power(t1,3)*(258 - 548*t2 - 155*Power(t2,2)) + 
               t1*(-255 + 479*t2 - 28*Power(t2,2)) + 
               Power(t1,4)*(-359 + 504*t2 + 38*Power(t2,2)) + 
               Power(t1,2)*(189 - 184*t2 + 141*Power(t2,2)) + 
               s1*(5 + 42*Power(t1,5) + t1*(72 - 111*t2) + 18*t2 + 
                  12*Power(t1,2)*(-35 + 9*t2) - 
                  Power(t1,4)*(243 + 16*t2) + Power(t1,3)*(544 + 29*t2))) \
+ Power(s2,3)*(-35 - 29*Power(t1,4) + 
               Power(s1,2)*(9 - 65*t1 + 60*Power(t1,2)) + 
               Power(t1,3)*(122 - 81*t2) + 89*t2 + 54*Power(t2,2) + 
               t1*(134 - 297*t2 - 153*Power(t2,2)) + 
               Power(t1,2)*(-192 + 289*t2 + 103*Power(t2,2)) + 
               s1*(-19 + 19*Power(t1,3) - 51*t2 - 
                  Power(t1,2)*(95 + 151*t2) + t1*(95 + 194*t2))) - 
            Power(s2,2)*(15 - 32*Power(t1,5) + 
               Power(s1,2)*(7 + 24*t1 - 105*Power(t1,2) + 
                  54*Power(t1,3)) + Power(t1,4)*(181 - 149*t2) + 13*t2 - 
               36*Power(t2,2) - 
               4*Power(t1,2)*(-113 + 209*t2 + 69*Power(t2,2)) + 
               Power(t1,3)*(-426 + 639*t2 + 107*Power(t2,2)) + 
               t1*(-190 + 333*t2 + 185*Power(t2,2)) + 
               s1*(8 + 46*Power(t1,4) - 31*t2 - 15*t1*(11 + 3*t2) - 
                  Power(t1,3)*(223 + 117*t2) + Power(t1,2)*(334 + 233*t2))\
)))*T3q(s2,1 - s + s2 - t1))/
     (s*(-1 + s2)*(-s + s2 - t1)*Power(-1 + s + t1,2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(s - s1 + t2)) - 
    (8*(Power(s,7)*(Power(t1,2) + 2*t1*t2 - Power(t2,2)) + 
         Power(s,6)*(3*Power(t1,2) + 5*Power(t1,3) - 2*t2 + 
            11*Power(t1,2)*t2 + 3*Power(t2,2) - 4*t1*Power(t2,2) - 
            s1*(Power(t1,2) + t1*(-2 + t2) + 2*s2*t2) + 
            s2*(2*t1 - 5*Power(t1,2) + 4*t2 - 13*t1*t2 + 8*Power(t2,2))) \
+ Power(s,5)*(-1 + 10*Power(t1,4) + 4*t2 + Power(t2,2) + 
            Power(t1,3)*(10 - 5*s1 + 26*t2) - 
            Power(t1,2)*(5 + 2*t2 + 6*Power(t2,2) + 2*s1*(-5 + 2*t2)) + 
            t1*(6 - 29*t2 + 10*Power(t2,2) + s1*(-3 + 2*t2)) + 
            Power(s2,2)*(-Power(s1,2) + 10*Power(t1,2) + 
               s1*(2 + t1 + 14*t2) - t2*(23 + 27*t2) + t1*(-9 + 35*t2)) \
+ s2*(2 - 20*Power(t1,3) + Power(t1,2)*(-7 + 5*s1 - 60*t2) + 
               (12 + s1)*t2 - 17*Power(t2,2) + 
               t1*(3 + Power(s1,2) + 28*t2 + 29*Power(t2,2) - 
                  s1*(11 + 4*t2)))) + 
         Power(s2 - t1,2)*(2*Power(s2,5)*Power(s1 - t2,2) - 
            t1*(8*s1 + 9*(-3 + t2))*(-1 + t2) + 4*Power(-1 + t2,2) + 
            (-1 + s1)*Power(t1,4)*(1 + t2) - 
            Power(t1,3)*(17 + 2*Power(s1,2) - 8*t2 + Power(t2,2) + 
               4*s1*(-5 + 2*t2)) - 
            Power(s2,4)*(s1 - t2)*
             (6 + s1*(-1 + 7*t1) - t2 - t1*(6 + 5*t2)) + 
            Power(t1,2)*(41 + 4*Power(s1,2) - 35*t2 + 8*Power(t2,2) + 
               s1*(-29 + 11*t2)) + 
            s2*(Power(s1,2)*t1*
                (-8 - 2*t1 + 3*Power(t1,2) + Power(t1,3)) + 
               Power(t1,4)*(2 + t2) + 
               Power(t1,2)*(11 + 13*t2 - 8*Power(t2,2)) - 
               3*(-5 + 4*t2 + Power(t2,2)) + 
               Power(t1,3)*(-1 + 2*t2 + 2*Power(t2,2)) + 
               t1*(-27 - 4*t2 + 3*Power(t2,2)) - 
               s1*(8 + Power(t1,2)*(23 - 9*t2) + 2*t1*(-19 + t2) - 
                  8*t2 + 2*Power(t1,3)*(2 + t2) + Power(t1,4)*(3 + t2))) \
- Power(s2,2)*(2 + Power(s1,2)*
                (-4 - 10*t1 + 5*Power(t1,2) + 5*Power(t1,3)) - 15*t2 - 
               Power(t2,2) + t1*(2 + 8*t2 - 8*Power(t2,2)) + 
               Power(t1,3)*(6 + 6*t2 + Power(t2,2)) + 
               Power(t1,2)*(-10 + t2 + 4*Power(t2,2)) + 
               s1*(-5*Power(t1,2)*(-1 + t2) + 9*(1 + t2) - 
                  6*Power(t1,3)*(2 + t2) + 2*t1*(-1 + 5*t2))) + 
            Power(s2,3)*(4 + Power(s1,2)*(-6 + t1 + 9*Power(t1,2)) - 
               5*t2 - 3*Power(t2,2) + t1*(-8 - 6*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(4 + 11*t2 + 4*Power(t2,2)) + 
               s1*(1 + t1*(14 - 4*t2) + 9*t2 - Power(t1,2)*(15 + 13*t2)))\
) + Power(s,4)*(1 + 10*Power(t1,5) + 6*t2 - 11*Power(t2,2) + 
            Power(t1,4)*(13 - 10*s1 + 34*t2) + 
            t1*(-15 + 7*s1*(-1 + t2) + 32*t2 + 9*Power(t2,2)) + 
            Power(t1,2)*(42 + 8*s1*(-4 + t2) - 86*t2 + 11*Power(t2,2)) - 
            Power(t1,3)*(13 + 12*t2 + 4*Power(t2,2) + s1*(-23 + 6*t2)) + 
            Power(s2,3)*(6*Power(s1,2) - 10*Power(t1,2) + 
               t1*(15 - 50*t2) + 2*t2*(26 + 25*t2) - 
               s1*(9 + 4*t1 + 40*t2)) + 
            s2*(5*Power(s1,2)*Power(t1,2) - 30*Power(t1,4) - 
               Power(t1,3)*(31 + 114*t2) + 
               t1*(-6 + 103*t2 - 48*Power(t2,2)) - 
               5*(-2 + 9*t2 + Power(t2,2)) + 
               4*Power(t1,2)*(7 + 21*t2 + 10*Power(t2,2)) + 
               s1*(-1 + 20*Power(t1,3) + t1*(11 - 10*t2) + 9*t2 - 
                  Power(t1,2)*(53 + t2))) + 
            Power(s2,2)*(-9 - 11*Power(s1,2)*t1 + 30*Power(t1,3) - 
               18*t2 + 37*Power(t2,2) + 5*Power(t1,2)*(1 + 26*t2) - 
               t1*(18 + 132*t2 + 85*Power(t2,2)) + 
               s1*(4 - 6*Power(t1,2) - 5*t2 + t1*(40 + 46*t2)))) + 
         s*(s2 - t1)*(Power(t1,6)*(-1 + s1 - 2*t2) + 
            8*Power(-1 + t2,2) - 4*t1*(-1 + t2)*(5 - 2*s1 + 5*t2) + 
            Power(t1,5)*(3 - 5*s1 + 6*t2) + 
            Power(t1,4)*(-20 + s1*(27 - 5*t2) + 17*t2 + Power(t2,2)) + 
            Power(t1,2)*(-149 - 8*Power(s1,2) + s1*(88 - 24*t2) + 
               67*t2 + 20*Power(t2,2)) + 
            Power(s2,5)*(-9*Power(s1,2) + (-7 + 3*t1 - 13*t2)*t2 + 
               s1*(3 + t1 + 22*t2)) + 
            Power(t1,3)*(139 + 8*Power(s1,2) - 66*t2 - 11*Power(t2,2) + 
               s1*(-109 + 23*t2)) + 
            Power(s2,3)*(8 + Power(s1,2)*(16 - 3*t1 - 42*Power(t1,2)) + 
               25*t2 + 11*Power(t2,2) + Power(t1,3)*(14 + 23*t2) + 
               2*t1*(8 + 16*t2 + Power(t2,2)) - 
               2*Power(t1,2)*(19 + 56*t2 + 17*Power(t2,2)) - 
               s1*(13 + Power(t1,3) + t1*(42 - 15*t2) + 35*t2 - 
                  2*Power(t1,2)*(44 + 35*t2))) - 
            s2*(Power(s1,2)*t1*(-16 + 9*Power(t1,2) + 5*Power(t1,3)) - 
               2*Power(t1,5)*(3 + 5*t2) + 
               Power(t1,2)*(178 - 59*t2 - 39*Power(t2,2)) + 
               Power(t1,4)*(14 + 33*t2 + Power(t2,2)) - 
               4*(-7 + 4*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(-33 + 38*t2 + 5*Power(t2,2)) + 
               t1*(-181 + 26*t2 + 31*Power(t2,2)) + 
               s1*(4*Power(t1,5) + Power(t1,3)*(50 - 17*t2) - 
                  16*t1*(-9 + t2) + 8*(-1 + t2) - 
                  Power(t1,4)*(33 + 4*t2) + Power(t1,2)*(-169 + 45*t2))) \
+ Power(s2,2)*(-40 + Power(s1,2)*
                (-8 - 24*t1 + 15*Power(t1,2) + 24*Power(t1,3)) - 
               25*t2 + 3*Power(t2,2) - 7*Power(t1,4)*(2 + 3*t2) + 
               t1*(39 - 34*t2 - 31*Power(t2,2)) + 
               Power(t1,2)*(-22 + 13*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(37 + 87*t2 + 12*Power(t2,2)) + 
               s1*(5*Power(t1,4) + Power(t1,2)*(48 - 29*t2) + 
                  8*(7 + t2) - 2*Power(t1,3)*(41 + 15*t2) + 
                  t1*(-47 + 57*t2))) + 
            Power(s2,4)*(-7 + Power(s1,2)*(-3 + 32*t1) - 24*t2 - 
               Power(t2,2) - Power(t1,2)*(5 + 13*t2) + 
               t1*(12 + 59*t2 + 36*Power(t2,2)) + 
               s1*(17 - 2*Power(t1,2) + 2*t2 - t1*(37 + 66*t2)))) - 
         Power(s,3)*(-5*Power(t1,6) + Power(t1,5)*(-9 + 10*s1 - 26*t2) + 
            Power(t1,3)*(-88 + s1*(84 - 13*t2) + 108*t2 - 
               3*Power(t2,2)) - 4*(1 - 4*t2 + 3*Power(t2,2)) + 
            t1*(13 + 16*s1*(-1 + t2) - 47*t2 + 40*Power(t2,2)) + 
            Power(t1,4)*(14 + 24*t2 + Power(t2,2) + s1*(-29 + 4*t2)) + 
            Power(t1,2)*(138 + 4*Power(s1,2) - 84*t2 - 28*Power(t2,2) - 
               s1*(55 + 7*t2)) + 
            Power(s2,4)*(14*Power(s1,2) - 5*Power(t1,2) + 
               t1*(11 - 40*t2) - 3*s1*(5 + 2*t1 + 20*t2) + 
               t2*(58 + 55*t2)) + 
            Power(s2,2)*(5 - 30*Power(t1,4) + 
               2*Power(s1,2)*(-2 + t1 + 17*Power(t1,2)) - 93*t2 - 
               12*Power(t2,2) - 6*Power(t1,3)*(8 + 31*t2) + 
               t1*(15 + 99*t2 - 79*Power(t2,2)) + 
               Power(t1,2)*(77 + 287*t2 + 100*Power(t2,2)) + 
               s1*(5 + 24*Power(t1,3) + t1*(14 - 19*t2) + 35*t2 - 
                  4*Power(t1,2)*(36 + 17*t2))) + 
            s2*(20*Power(t1,5) + Power(t1,4)*(40 - 30*s1 + 112*t2) + 
               3*(6 + t2 - 9*Power(t2,2)) - 
               Power(t1,3)*(50 - 105*s1 + 10*Power(s1,2) + 137*t2 + 
                  26*Power(t2,2)) + 
               t1*(-117 + s1*(46 - 24*t2) + 159*t2 + 32*Power(t2,2)) + 
               Power(t1,2)*(79 - 3*Power(s1,2) - 211*t2 + 
                  43*Power(t2,2) + s1*(-97 + 37*t2))) + 
            Power(s2,3)*(-5 + Power(s1,2)*(1 - 38*t1) + 20*Power(t1,3) + 
               4*t2 + 38*Power(t2,2) + 2*Power(t1,2)*(3 + 70*t2) - 
               13*t1*(3 + 18*t2 + 10*Power(t2,2)) + 
               s1*(3 + 2*Power(t1,2) - 9*t2 + t1*(83 + 124*t2)))) + 
         Power(s,2)*(Power(t1,7) - 4*Power(-1 + t2,2) + 
            Power(t1,6)*(4 - 5*s1 + 11*t2) - 
            Power(t1,5)*(9 + s1*(-19 + t2) + 20*t2) + 
            t1*(-1 + t2)*(-35 + 8*s1 + 33*t2) + 
            Power(t1,3)*(-246 - 10*Power(s1,2) + s1*(151 - 15*t2) + 
               114*t2 + 32*Power(t2,2)) + 
            Power(t1,2)*(102 + 4*Power(s1,2) + 17*t2 - 57*Power(t2,2) - 
               s1*(43 + 3*t2)) + 
            Power(t1,4)*(73 - 65*t2 - 2*Power(t2,2) + s1*(-83 + 11*t2)) + 
            Power(s2,5)*(16*Power(s1,2) - Power(t1,2) + t1*(3 - 17*t2) + 
               4*t2*(8 + 9*t2) - s1*(11 + 4*t1 + 50*t2)) + 
            s2*(-23 - 5*Power(t1,6) + 
               Power(s1,2)*t1*
                (-8 + 6*t1 + 9*Power(t1,2) + 10*Power(t1,3)) + 44*t2 - 
               21*Power(t2,2) - Power(t1,5)*(25 + 57*t2) + 
               Power(t1,2)*(385 - 218*t2 - 71*Power(t2,2)) + 
               Power(t1,3)*(-125 + 171*t2 - 8*Power(t2,2)) + 
               Power(t1,4)*(40 + 115*t2 + 8*Power(t2,2)) + 
               t1*(-181 - 16*t2 + 73*Power(t2,2)) + 
               s1*(8 + 20*Power(t1,5) + Power(t1,3)*(169 - 44*t2) - 
                  8*t2 + 2*t1*(45 + t2) - 2*Power(t1,4)*(49 + 2*t2) + 
                  Power(t1,2)*(-260 + 58*t2))) + 
            Power(s2,2)*(75 + 10*Power(t1,5) + 
               Power(s1,2)*(4 + 18*t1 - 15*Power(t1,2) - 
                  46*Power(t1,3)) + 7*t2 - 20*Power(t2,2) + 
               2*Power(t1,4)*(27 + 61*t2) + 
               Power(t1,2)*(43 - 123*t2 + 42*Power(t2,2)) + 
               t1*(-127 + 184*t2 + 51*Power(t2,2)) - 
               Power(t1,3)*(95 + 290*t2 + 54*Power(t2,2)) + 
               s1*(-47 - 26*Power(t1,4) + t1*(91 - 95*t2) + t2 + 
                  Power(t1,2)*(-113 + 60*t2) + Power(t1,3)*(203 + 62*t2))\
) + Power(s2,4)*(9 + Power(s1,2)*(3 - 58*t1) + 5*Power(t1,3) + 30*t2 + 
               17*Power(t2,2) + 5*Power(t1,2)*(2 + 15*t2) - 
               2*t1*(18 + 95*t2 + 55*Power(t2,2)) + 
               s1*(-12 + 7*Power(t1,2) - 7*t2 + t1*(86 + 151*t2))) + 
            Power(s2,3)*(-10*Power(t1,4) + 
               Power(s1,2)*(-14 + 3*t1 + 78*Power(t1,2)) - 
               t1*t2*(13 + 49*t2) - 2*Power(t1,3)*(23 + 67*t2) - 
               8*(2 + 9*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(100 + 353*t2 + 120*Power(t2,2)) + 
               s1*(18 + 8*Power(t1,3) + t1*(39 - 20*t2) + 52*t2 - 
                  Power(t1,2)*(199 + 158*t2)))))*T4q(1 - s + s2 - t1))/
     (s*(-1 + s2)*Power(-s + s2 - t1,2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(s - s1 + t2)) - 
    (8*(Power(s,6)*(Power(t1,2) + 2*t1*t2 - Power(t2,2)) - 
         2*Power(s2 - t1,2)*Power(-1 + t1,2)*
          (-5 + Power(s1,2)*(s2 - t1) + 4*t1 + Power(t1,2) + 2*t2 - 
            2*Power(s2,2)*t2 - 12*t1*t2 + 3*Power(t2,2) + 
            s1*(3 + 2*Power(s2,2) - Power(t1,2) - s2*(11 + t1 - 2*t2) - 
               3*t2 + t1*(8 + t2)) + 
            s2*(2 + 9*t2 - 3*Power(t2,2) + t1*(-2 + 3*t2))) - 
         Power(s,5)*(Power(t1,2) - 3*Power(t1,3) + 2*t2 - 
            7*Power(t1,2)*t2 - 3*Power(t2,2) + 2*t1*Power(t2,2) + 
            s1*(Power(t1,2) + t1*(-2 + t2) + 2*s2*t2) + 
            s2*(3*Power(t1,2) - 2*t2*(2 + 3*t2) + t1*(-2 + 9*t2))) + 
         Power(s,4)*(-1 + 3*Power(t1,4) + 4*t2 + Power(t2,2) + 
            Power(t1,3)*(-8 - 3*s1 + 10*t2) - 
            Power(t1,2)*(5 + 2*s1*(-5 + t2) - 2*t2 + Power(t2,2)) + 
            t1*(6 - 25*t2 + 4*Power(t2,2) + s1*(-3 + 2*t2)) + 
            Power(s2,2)*(-Power(s1,2) + 3*Power(t1,2) + 
               5*t1*(-1 + 3*t2) + s1*(2 + t1 + 10*t2) - t2*(15 + 14*t2)) \
+ s2*(2 - 6*Power(t1,3) + Power(t1,2)*(11 + 3*s1 - 24*t2) + (8 + s1)*t2 - 
               11*Power(t2,2) + 
               t1*(-5 + Power(s1,2) + 20*t2 + 11*Power(t2,2) - 
                  s1*(7 + 2*t2)))) + 
         Power(s,3)*(1 + Power(t1,5) + 
            Power(t1,2)*(6 - 4*Power(s1,2) + 6*s1*(-1 + t2) - 46*t2) + 
            Power(t1,3)*(17 - s1*(-19 + t2) - 2*t2) + 6*t2 - 
            11*Power(t2,2) + Power(t1,4)*(-12 - 3*s1 + 7*t2) + 
            t1*(-13 + 7*s1*(-1 + t2) + 24*t2 + 7*Power(t2,2)) + 
            Power(s2,3)*(4*Power(s1,2) - Power(t1,2) + t1*(3 - 11*t2) + 
               2*t2*(9 + 8*t2) - s1*(5 + 2*t1 + 18*t2)) + 
            s2*(8 + 3*Power(s1,2)*Power(t1,2) - 3*Power(t1,4) + 
               Power(t1,3)*(19 - 23*t2) - 37*t2 - 3*Power(t2,2) + 
               t1*(-6 + 41*t2 - 12*Power(t2,2)) + 
               Power(t1,2)*(-20 + 36*t2 + 6*Power(t2,2)) + 
               s1*(-1 + 6*Power(t1,3) + t1*(13 - 8*t2) + 9*t2 - 
                  Power(t1,2)*(31 + t2))) + 
            Power(s2,2)*(-9 - 7*Power(s1,2)*t1 + 3*Power(t1,3) + 
               12*Power(t2,2) + Power(t1,2)*(-14 + 27*t2) - 
               3*t1*(-4 + 18*t2 + 7*Power(t2,2)) + 
               s1*(4 - Power(t1,2) - 3*t2 + t1*(20 + 19*t2)))) + 
         s*(2*(5 + 3*s1)*Power(t1,5) - 2*Power(t1,6) + 
            2*Power(s2,5)*Power(s1 - t2,2) + 
            t1*(8*s1 + 9*(-3 + t2))*(-1 + t2) - 4*Power(-1 + t2,2) + 
            Power(t1,4)*(-29 + s1*(-41 + t2) + 51*t2 - 2*Power(t2,2)) + 
            Power(t1,2)*(-57 + s1*(13 - 29*t2) + 73*t2 + 6*Power(t2,2)) - 
            Power(s2,4)*(s1 - t2)*
             (6 + s1*(-1 + 7*t1) - t2 - t1*(6 + 5*t2)) + 
            Power(t1,3)*(55 + 2*Power(s1,2) - 96*t2 - 7*Power(t2,2) + 
               2*s1*(15 + 8*t2)) + 
            Power(s2,2)*(6 - 2*Power(t1,4) + 
               Power(s1,2)*t1*(14 - 5*t1 - 5*Power(t1,2)) + 15*t2 + 
               Power(t2,2) + Power(t1,2)*
                (-28 + 39*t2 - 16*Power(t2,2)) + 
               Power(t1,3)*(20 + 2*t2 - Power(t2,2)) + 
               4*t1*(1 - 14*t2 + 5*Power(t2,2)) + 
               s1*(-19 + t1*(60 - 42*t2) + 6*Power(t1,3)*(-1 + t2) + 
                  3*t2 + 5*Power(t1,2)*(-7 + 5*t2))) + 
            Power(s2,3)*(-8 + Power(s1,2)*(-6 + t1 + 9*Power(t1,2)) + 
               3*t2 - 3*Power(t2,2) + t1*(16 - 14*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-8 + 11*t2 + 4*Power(t2,2)) - 
               s1*(-5 - 9*t2 + t1*(2 + 4*t2) + Power(t1,2)*(3 + 13*t2))) \
+ s2*(4*Power(t1,5) + Power(s1,2)*Power(t1,2)*
                (-10 + 3*t1 + Power(t1,2)) - Power(t1,4)*(22 + 7*t2) + 
               t1*(39 - 64*t2 - 19*Power(t2,2)) + 
               Power(t1,2)*(-47 + 141*t2 - 6*Power(t2,2)) + 
               3*(-5 + 4*t2 + Power(t2,2)) + 
               Power(t1,3)*(41 - 82*t2 + 16*Power(t2,2)) + 
               s1*(8 + Power(t1,3)*(84 - 22*t2) - 8*t2 - 
                  Power(t1,4)*(3 + t2) + Power(t1,2)*(-95 + 17*t2) + 
                  t1*(6 + 26*t2)))) + 
         Power(s,2)*(Power(t1,4)*(29 + 15*s1 - 4*t2) - 
            Power(t1,5)*(7 + s1 - 2*t2) + 
            t1*(-15 - 16*s1*(-1 + t2) + 35*t2 - 18*Power(t2,2)) - 
            Power(t1,3)*(36 + 6*Power(s1,2) + s1*(31 - 7*t2) - 13*t2 + 
               3*Power(t2,2)) + 4*(1 - 4*t2 + 3*Power(t2,2)) + 
            Power(s2,4)*(-5*Power(s1,2) + (-7 + 3*t1 - 9*t2)*t2 + 
               s1*(3 + t1 + 14*t2)) + 
            Power(t1,2)*(25 + 4*Power(s1,2) - 16*t2 + Power(t2,2) + 
               s1*(-13 + 19*t2)) + 
            s2*(-16 + Power(t1,4)*(11 + 3*s1 - 8*t2) + 9*t2 + 
               5*Power(t2,2) + 
               Power(t1,2)*(35 + 9*Power(s1,2) + s1*(53 - 28*t2) + t2 + 
                  6*Power(t2,2)) - 
               t1*(-17 + 8*Power(s1,2) + s1*(10 - 24*t2) + 45*t2 + 
                  10*Power(t2,2)) + 
               Power(t1,3)*(-47 + 3*Power(s1,2) + 25*t2 + Power(t2,2) - 
                  2*s1*(14 + t2))) + 
            Power(s2,2)*(4 + Power(s1,2)*(4 - 2*t1 - 11*Power(t1,2)) + 
               19*t2 + 5*Power(t2,2) + Power(t1,3)*(-7 + 13*t2) + 
               Power(t1,2)*(26 - 58*t2 - 9*Power(t2,2)) + 
               t1*(-23 + 16*t2 + 5*Power(t2,2)) + 
               s1*(-3 - 2*Power(t1,3) - 17*t2 + 16*Power(t1,2)*(2 + t2) + 
                  t1*(-17 + 9*t2))) + 
            Power(s2,3)*(1 + Power(s1,2)*(-1 + 13*t1) + 
               Power(t1,2)*(3 - 10*t2) - 12*t2 - 3*Power(t2,2) + 
               t1*(-4 + 40*t2 + 17*Power(t2,2)) - 
               s1*(-5 + Power(t1,2) - 2*t2 + t1*(22 + 28*t2)))))*T5q(s))/
     (s*(-1 + s2)*(-s + s2 - t1)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(s - s1 + t2)));
   return a;
};
