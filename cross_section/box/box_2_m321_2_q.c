#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m321_2_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(-(Power(t1,2)*(1 + t1)*(1 + s*(-1 + t1) - 4*t1 + Power(t1,2))*
            (1 + t1 - t2)) - 4*Power(s2,5)*t1*t2 + 
         Power(s2,4)*(2*Power(t1,4) + Power(t1,3)*(3 + 2*s1 - 4*t2) - 
            t2 + t1*(13 + s1*(-3 + t2) + (3 - 2*s)*t2 - 9*Power(t2,2)) + 
            Power(t1,2)*(-2 + s1 + 2*(7 + s)*t2 - s1*t2 + Power(t2,2))) + 
         s2*t1*(1 - Power(t2,2) + Power(t1,5)*(-9 + 4*t2) + 
            t1*(-2 + 4*s1*Power(-1 + t2,2) - 8*t2 + 25*Power(t2,2) - 
               12*Power(t2,3)) - 
            Power(t1,2)*(6 + s1 + 20*t2 + s1*t2 - 42*Power(t2,2) + 
               8*Power(t2,3)) + 
            Power(t1,4)*(29 + 3*t2 - 8*Power(t2,2) + 2*s1*(-5 + 2*t2)) + 
            Power(t1,3)*(-21 - 47*t2 + 14*Power(t2,2) + 4*Power(t2,3) + 
               s1*(7 + 5*t2 - 4*Power(t2,2))) + 
            s*(-1 + t1)*(-5 + Power(t1,2) + Power(t1,3) + 11*t2 - 
               6*Power(t2,2) + t1*(-10 + 15*t2 - 3*Power(t2,2)))) - 
         Power(s2,3)*(4*Power(t1,5) + Power(t1,4)*(15 + 4*s1 - 12*t2) + 
            (-2 + t2)*t2 + Power(t1,3)*
             (-26 - 6*s1*(-2 + t2) + 3*t2 + 15*Power(t2,2) + s*(2 + t2)) \
+ t1*(14 - 54*Power(t2,2) + 24*Power(t2,3) + 
               s1*(-7 + 17*t2 - 8*Power(t2,2)) + 
               s*(-7 + 3*t2 + 3*Power(t2,2))) - 
            Power(t1,2)*(-17 - 99*t2 + 50*Power(t2,2) + 8*Power(t2,3) + 
               s1*(9 + 11*t2 - 8*Power(t2,2)) + 
               s*(-5 + 4*t2 + 3*Power(t2,2)))) - 
         Power(s2,2)*(-14*Power(t1,2) - 23*Power(t1,3) + 52*Power(t1,4) - 
            21*Power(t1,5) - 2*Power(t1,6) + t2 - 9*t1*t2 - 
            24*Power(t1,2)*t2 - 142*Power(t1,3)*t2 + 10*Power(t1,4)*t2 + 
            12*Power(t1,5)*t2 - Power(t2,2) + 24*t1*Power(t2,2) + 
            96*Power(t1,2)*Power(t2,2) + 55*Power(t1,3)*Power(t2,2) - 
            22*Power(t1,4)*Power(t2,2) - 12*t1*Power(t2,3) - 
            32*Power(t1,2)*Power(t2,3) + 12*Power(t1,3)*Power(t2,3) + 
            s*(-1 + t1)*t1*(-12 + Power(t1,2)*(-1 + t2) + 16*t2 - 
               3*Power(t2,2) + t1*(-13 + 15*t2 - 3*Power(t2,2))) - 
            s1*(-1 + t1)*t1*(2*Power(t1,3) + Power(t1,2)*(23 - 9*t2) + 
               4*Power(-1 + t2,2) + 2*t1*(5 - 13*t2 + 6*Power(t2,2))))))/
     ((-1 + s2)*s2*Power(-1 + t1,2)*t1*Power(-s2 + t1,2)*(1 - s2 + t1 - t2)*
       (-1 + t2)) - (8*(Power(t1,3)*(5 - 7*s + 3*s1 - 4*t2) + 
         2*t2*(1 + (-1 + s)*t2) - 
         2*Power(s2,4)*(s1*(-1 + t1) - (-5 + t1)*t2) + 
         Power(t1,2)*(34 + s1 + 7*t2 - 10*s1*t2 + 8*Power(t2,2) + 
            s*(17 + 9*t2)) - t1*
          (-1 + s1*(4 - 10*t2) + 5*t2 + 22*Power(t2,2) + 
            s*(10 + 9*t2 + 2*Power(t2,2))) + 
         Power(s2,3)*(-2 + s1*(-1 + t1)*(1 + 2*t1 - 6*t2) + 23*t2 - 
            2*s*t2 - 22*Power(t2,2) - 2*Power(t1,2)*(1 + s + 2*t2) + 
            t1*(12 + 2*s + 21*t2 + 2*s*t2 + 6*Power(t2,2))) + 
         Power(s2,2)*(1 + 4*Power(t1,3) + 44*t2 + 7*s*t2 + 
            36*Power(t2,2) - 2*s*Power(t2,2) - 12*Power(t2,3) + 
            Power(t1,2)*(-23 + s - 9*t2 - 2*s*t2 - 4*Power(t2,2)) + 
            s1*(-1 + t1)*(3 + 6*t2 - 4*Power(t2,2) + 2*t1*(1 + t2)) + 
            t1*(-22 + 13*t2 + 16*Power(t2,2) + 4*Power(t2,3) + 
               s*(-1 - 5*t2 + 2*Power(t2,2)))) + 
         s2*(3 + (-5 + 13*s + 2*s1)*t2 + (4 + 6*s - 8*s1)*Power(t2,2) + 
            24*Power(t2,3) + Power(t1,3)*(3 - s + s1 + 4*t2) + 
            Power(t1,2)*(13 - 24*t2 + s1*(-11 + 2*t2) + s*(10 + 7*t2)) - 
            t1*(51 + 63*t2 - 4*Power(t2,2) + 8*Power(t2,3) - 
               2*s1*(5 - 2*t2 + 4*Power(t2,2)) + 
               s*(9 + 20*t2 + 6*Power(t2,2)))))*B1(1 - s2 + t1 - t2,s2,t1))/
     (Power(-1 + t1,2)*(-1 + t2)*(t1 - s2*t2)) + 
    (16*(Power(t1,2)*(1 + t1)*(1 + s*(-1 + t1) - 4*t1 + Power(t1,2)) + 
         Power(s2,5)*(-8 - 25*t1 - 8*Power(t1,2) + Power(t1,3) + 
            s1*(-1 + t1)*(13 + 8*t1 - 8*t2) - 
            s*(-1 + t1)*(3 + 4*t1 - 5*t2) + 52*t2 + 57*t1*t2 - 
            13*Power(t1,2)*t2 - 24*Power(t2,2) + 8*t1*Power(t2,2)) + 
         Power(s2,6)*(8 - 3*s1*(-1 + t1) - 19*t2 + t1*(4 + 3*t2)) + 
         Power(s2,3)*(-4 - 12*t1 + 147*Power(t1,2) - 17*Power(t1,3) - 
            3*Power(t1,4) + Power(t1,5) - 
            s*(-1 + t1)*(Power(t1,3) + Power(t1,2)*(28 - 8*t2) + 
               t1*(16 - 5*t2) + 7*(-1 + t2)) + 10*t2 + 24*t1*t2 + 
            78*Power(t1,2)*t2 + 6*Power(t1,3)*t2 - 6*Power(t1,4)*t2 - 
            12*Power(t2,2) - 20*t1*Power(t2,2) - 
            28*Power(t1,2)*Power(t2,2) + 12*Power(t1,3)*Power(t2,2) + 
            s1*(-1 + t1)*(3 + 2*Power(t1,3) + Power(t1,2)*(25 - 12*t2) - 
               4*t2 - 8*t1*t2)) + 
         s2*t1*(-3 + Power(t1,2)*(32 + s1 - 10*t2) + 
            t1*(5 + s1 - 5*t2) + Power(t1,3)*(31 - 2*s1 - 4*t2) + t2 + 
            Power(t1,4)*(-9 + 2*t2) + 
            s*(-1 + t1)*(-4 + 2*t1*(-4 + t2) + t2 + 
               Power(t1,2)*(-7 + 2*t2))) - 
         Power(s2,2)*(-2 + Power(t1,5)*(1 - 2*t2) + t2 + 
            Power(t1,3)*(149 - 15*s1 - 6*t2 + 4*s1*t2 - 
               12*Power(t2,2)) + 
            t1*(-2 - 2*s1 + 5*t2 + 4*s1*t2 - 12*Power(t2,2)) + 
            Power(t1,2)*(27 + 9*s1 - 8*t2 - 4*s1*t2 + 4*Power(t2,2)) + 
            Power(t1,4)*(-33 + 8*s1 + 10*t2 - 4*s1*t2 + 4*Power(t2,2)) + 
            s*(-1 + t1)*(3 + t1*(-6 + t2) + 10*Power(t1,2)*(-3 + t2) - 
               5*t2 + Power(t1,3)*(-7 + 2*t2))) + 
         Power(s2,4)*(-(s1*(-1 + t1)*
               (5 + 7*Power(t1,2) + t1*(30 - 16*t2) - 8*t2)) + 
            s*(-1 + t1)*(-1 + 5*Power(t1,2) + t1*(18 - 5*t2) - 3*t2) - 
            2*(-5 + Power(t1,4) + 11*t2 - 12*Power(t2,2) - 
               Power(t1,3)*(4 + 7*t2) + 
               t1*(11 + 66*t2 - 20*Power(t2,2)) + 
               Power(t1,2)*(-5 + 18*t2 + 8*Power(t2,2)))))*R1q(s2))/
     (Power(-1 + s2,2)*s2*Power(-1 + t1,2)*Power(-s2 + t1,3)*(-1 + t2)) + 
    (16*(-4*Power(s2,5)*t1*(-1 + t2) + 
         Power(s2,4)*(Power(t1,4) + t2 + 
            Power(t1,3)*(3 + 2*s - 5*s1 + 4*t2) + 
            Power(t1,2)*(-5 - 2*s1*(-3 + t2) + 2*s*(-1 + t2) + t2 + 
               2*Power(t2,2)) - 
            t1*(-1 + s1 + 2*(-3 + s)*t2 - 2*s1*t2 + 14*Power(t2,2))) + 
         Power(s2,3)*(-2*Power(t1,5) + 2*Power(t2,2) - 
            Power(t1,4)*(9 + 11*s - 14*s1 + 15*t2) - 
            t1*(-1 + (3 + s + 2*s1)*t2 + 
               (-2 + 4*s - 4*s1)*Power(t2,2) + 16*Power(t2,3)) + 
            Power(t1,3)*(3 + 39*t2 + 4*Power(t2,2) + s*(8 + t2) - 
               2*s1*(4 + 3*t2)) + 
            Power(t1,2)*(-25 + 39*t2 + 8*Power(t2,2) + 4*Power(t2,3) + 
               s1*(-6 + 8*t2 - 4*Power(t2,2)) + s*(3 + 4*Power(t2,2)))) + 
         s2*t1*(-2*Power(t1,5)*(8 + 2*s - 2*s1 + t2) - 
            Power(t2,2)*(-1 + (3 + s)*t2) + 
            t1*(-4 + 2*(5 + 8*s)*t2 + (5 - 11*s - 2*s1)*Power(t2,2) + 
               13*Power(t2,3)) + 
            2*Power(t1,4)*(35 + (11 - 7*s1)*t2 + 10*Power(t2,2) + 
               s*(-6 + 7*t2)) + 
            Power(t1,3)*(126 + 8*t2 - 55*Power(t2,2) - 9*Power(t2,3) + 
               2*s1*(-6 + 4*t2 + 3*Power(t2,2)) + 
               s*(12 - 32*t2 + 7*Power(t2,2))) + 
            Power(t1,2)*(-4 - 46*t2 - 67*Power(t2,2) + 27*Power(t2,3) + 
               s1*(8 + 6*t2 - 4*Power(t2,2)) + 
               s*(4 + 2*t2 + 4*Power(t2,2) + Power(t2,3)))) + 
         Power(s2,2)*(Power(t1,6) + Power(t2,3) + 
            Power(t1,5)*(18 + 13*s - 13*s1 + 13*t2) - 
            t1*t2*(2 + (7 + 2*s + s1)*t2 + 2*(s - s1)*Power(t2,2) + 
               6*Power(t2,3)) - 
            Power(t1,4)*(60 + (38 + 7*s)*t2 + 23*Power(t2,2) - 
               2*s1*(1 + 9*t2)) + 
            Power(t1,3)*(14 - 123*t2 + 49*Power(t2,2) + 
               s*(-19 + 6*t2) - s1*(-7 + 6*t2 + Power(t2,2))) + 
            Power(t1,2)*(3 - 2*t2 + 57*Power(t2,2) + 7*Power(t2,3) + 
               2*Power(t2,4) - 
               2*s1*(-2 + 6*t2 - Power(t2,2) + Power(t2,3)) + 
               s*(6 + t2 + 2*Power(t2,2) + 2*Power(t2,3)))) + 
         Power(t1,2)*(4*Power(t1,5) + 
            Power(t2,2)*(-1 + s*(6 - 5*t2) + 2*t2) - 
            Power(t1,4)*(12 + 4*(5 + s - s1)*t2 + 3*Power(t2,2)) + 
            Power(t1,3)*(-116 + 70*t2 + 12*Power(t2,2) + 5*Power(t2,3) + 
               s*(28 - 8*t2 + Power(t2,2)) - 
               s1*(-12 + 12*t2 + Power(t2,2))) + 
            t1*(4 - 8*t2 + 3*s1*Power(t2,2) - (13 + 2*s1)*Power(t2,3) + 
               6*Power(t2,4) + 
               s*(-24 + 20*t2 + Power(t2,2) + 2*Power(t2,3))) + 
            Power(t1,2)*(s*(-4 - 8*t2 - 8*Power(t2,2) + 3*Power(t2,3)) + 
               2*(s1*(-6 + 4*t2 - Power(t2,2) + Power(t2,3)) - 
                  t2*(-25 - 4*t2 + 9*Power(t2,2) + Power(t2,3))))))*R1q(t1))/
     (Power(-1 + t1,2)*t1*Power(-s2 + t1,3)*(-1 + t2)*
       (-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) - 
    (16*(4 + 4*Power(t1,4) - 12*t2 + 2*s*t2 - 2*s1*t2 + 16*Power(t2,2) - 
         s*Power(t2,2) + 9*s1*Power(t2,2) - 34*Power(t2,3) + 
         s*Power(t2,3) - 6*s1*Power(t2,3) + 18*Power(t2,4) + 
         Power(s2,2)*(6 + 9*t1 + 4*Power(t1,2) + Power(t1,3) - 
            3*s1*(-1 + t1)*(3 + 2*t1 - 4*t2) + 
            s*(-1 + t1)*(1 + 6*t1 - t2) - 34*t2 - 35*t1*t2 + 
            9*Power(t1,2)*t2 + 40*Power(t2,2) - 12*t1*Power(t2,2)) + 
         Power(s2,3)*(3*s1*(-1 + t1) + 11*t2 - t1*(4 + 3*t2)) - 
         Power(t1,3)*(4*s*(-1 + t2) - 4*s1*(-1 + t2) + 
            3*(4 + 8*t2 + Power(t2,2))) + 
         Power(t1,2)*(-128 + 104*t2 + 22*Power(t2,2) + 9*Power(t2,3) + 
            2*s*(6 - 5*t2 + 3*Power(t2,2)) - 
            2*s1*(-10 + 5*t2 + 3*Power(t2,2))) + 
         s2*(-8 + 20*t1 + 48*Power(t1,2) - 12*Power(t1,3) + 22*t2 + 
            6*t1*t2 + 14*Power(t1,2)*t2 - 2*Power(t1,3)*t2 - 
            68*Power(t2,2) - 58*t1*Power(t2,2) + 
            18*Power(t1,2)*Power(t2,2) + 47*Power(t2,3) - 
            15*t1*Power(t2,3) - 
            2*s*(-1 + t1)*(1 + 2*Power(t1,2) + t1*(9 - 6*t2) - t2 + 
               Power(t2,2)) + 
            s1*(-1 + t1)*(2 + 6*t1 + 4*Power(t1,2) - 18*t2 - 12*t1*t2 + 
               15*Power(t2,2))) - 
         t1*(12 - 108*t2 + 31*Power(t2,2) + 27*Power(t2,3) + 
            6*Power(t2,4) + s1*
             (16 - 8*t2 + 3*Power(t2,2) - 6*Power(t2,3)) + 
            s*(16 - 12*t2 + 5*Power(t2,2) + Power(t2,3))))*
       R2q(1 - s2 + t1 - t2))/
     (Power(-1 + t1,2)*(1 - s2 + t1 - t2)*(-1 + t2)*
       (-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) + 
    (8*(8*Power(t1,5)*(2 + t2) - 2*Power(t2,4)*(1 + (-1 + s)*t2) - 
         2*Power(s2,6)*(s1*(-1 + t1) - (-5 + t1)*t2) + 
         t1*(8 + 4*(-5 + s - s1)*t2 + (42 + 4*s1)*Power(t2,2) + 
            (-29 + 4*s + 8*s1)*Power(t2,3) + 
            (-17 + 7*s - 6*s1)*Power(t2,4) + 2*(5 + s)*Power(t2,5)) - 
         4*Power(t1,4)*(16 + 12*t2 + 7*Power(t2,2) + Power(t2,3) + 
            s*(-6 + 4*t2 + Power(t2,2)) - s1*(-6 + 4*t2 + Power(t2,2))) + 
         Power(t1,3)*(-200 + 12*t2 + 86*Power(t2,2) + 29*Power(t2,3) + 
            8*Power(t2,4) + s1*
             (8 + 32*t2 - 18*Power(t2,2) - 9*Power(t2,3)) + 
            s*(8 - 16*t2 + 22*Power(t2,2) + 9*Power(t2,3))) + 
         Power(t1,2)*(-80 + 80*t2 + 108*Power(t2,2) - 36*Power(t2,3) - 
            21*Power(t2,4) - 4*Power(t2,5) + 
            s1*(16 - 44*t2 + 10*Power(t2,2) + Power(t2,3) + 
               6*Power(t2,4)) - 
            s*(32 - 28*t2 + 18*Power(t2,2) + 13*Power(t2,3) + 
               7*Power(t2,4))) + 
         Power(s2,5)*(-2 + s1*(-1 + t1)*(3 + 2*t1 - 12*t2) + 21*t2 - 
            2*s*t2 - 52*Power(t2,2) - 2*Power(t1,2)*(1 + s + 2*t2) + 
            t1*(12 + 23*t2 + 12*Power(t2,2) + 2*s*(1 + t2))) + 
         Power(s2,4)*(3 + 4*Power(t1,3) + (1 + 5*s - 15*s1)*t2 + 
            (79 - 8*s + 28*s1)*Power(t2,2) - 108*Power(t2,3) + 
            Power(t1,2)*(-25 + s*(3 - 12*t2) - 31*t2 - 20*Power(t2,2) + 
               12*s1*(1 + t2)) + 
            t1*(-22 + 94*t2 + 93*Power(t2,2) + 28*Power(t2,3) + 
               s1*(-12 + 3*t2 - 28*Power(t2,2)) + 
               s*(-3 + 7*t2 + 8*Power(t2,2)))) + 
         Power(s2,3)*(Power(t1,3)*
             (19 + 48*t2 + 4*Power(t2,2) + s*(15 + 4*t2) - 
               s1*(15 + 4*t2)) - 
            Power(t1,2)*(42 + 240*t2 + 95*Power(t2,2) + 
               36*Power(t2,3) + s1*(7 - 48*t2 - 24*Power(t2,2)) + 
               s*(13 + 24*Power(t2,2))) + 
            t2*(-13 + (33 + 13*s)*t2 - 3*(-37 + 4*s)*Power(t2,2) - 
               112*Power(t2,3) + s1*(-4 - 27*t2 + 32*Power(t2,2))) + 
            t1*(-1 - 187*t2 + 226*Power(t2,2) + 141*Power(t2,3) + 
               32*Power(t2,4) + 
               s1*(22 - 40*t2 + 3*Power(t2,2) - 32*Power(t2,3)) + 
               s*(-2 - 4*t2 + 11*Power(t2,2) + 12*Power(t2,3)))) + 
         Power(s2,2)*(-4*Power(t1,4)*(7 + s - s1 + 3*t2) + 
            Power(t1,3)*(150 + 131*t2 + 92*Power(t2,2) + 
               8*Power(t2,3) + s*(-18 + 35*t2 + 8*Power(t2,2)) - 
               s1*(10 + 35*t2 + 8*Power(t2,2))) + 
            t2*(12 - 53*t2 + 57*Power(t2,2) + 69*Power(t2,3) - 
               58*Power(t2,4) + 
               s*(-4 + 9*Power(t2,2) - 8*Power(t2,3)) + 
               s1*(4 - 8*t2 - 21*Power(t2,2) + 18*Power(t2,3))) + 
            Power(t1,2)*(112 - 76*t2 - 382*Power(t2,2) - 
               109*Power(t2,3) - 28*Power(t2,4) + 
               s*(22 + 9*t2 - 16*Power(t2,2) - 20*Power(t2,3)) + 
               s1*(2 - 45*t2 + 66*Power(t2,2) + 20*Power(t2,3))) + 
            t1*(6 - (111 + 40*s)*t2 + (-321 + 8*s)*Power(t2,2) + 
               (228 + 11*s)*Power(t2,3) + (95 + 8*s)*Power(t2,4) + 
               18*Power(t2,5) + 
               s1*(4 + 76*t2 - 50*Power(t2,2) + Power(t2,3) - 
                  18*Power(t2,4)))) + 
         s2*(8*Power(t1,5) - t2*
             (8 + 4*(-5 + s - s1)*t2 + (39 + 4*s1)*Power(t2,2) + 
               (-29 + s + 6*s1)*Power(t2,3) + 
               2*(s - 2*(4 + s1))*Power(t2,4) + 12*Power(t2,5)) - 
            8*Power(t1,4)*(8 + 9*t2 + 2*Power(t2,2) + s*(1 + t2) - 
               s1*(1 + t2)) + 
            Power(t1,3)*(-52 + 308*t2 + 125*Power(t2,2) + 
               56*Power(t2,3) + 4*Power(t2,4) + 
               s*(-16 - 20*t2 + 29*Power(t2,2) + 4*Power(t2,3)) - 
               s1*(-48 + 20*t2 + 29*Power(t2,2) + 4*Power(t2,3))) - 
            Power(t1,2)*(-88 - 404*t2 + 118*Power(t2,2) + 
               188*Power(t2,3) + 47*Power(t2,4) + 8*Power(t2,5) + 
               s1*(52 - 20*t2 + 37*Power(t2,2) - 36*Power(t2,3) - 
                  6*Power(t2,4)) + 
               s*(-20 + 4*t2 - 9*Power(t2,2) + 20*Power(t2,3) + 
                  6*Power(t2,4))) + 
            t1*(-12 + 136*t2 - 147*Power(t2,2) - 173*Power(t2,3) + 
               94*Power(t2,4) + 24*Power(t2,5) + 4*Power(t2,6) - 
               2*s1*(2 + 4*t2 - 31*Power(t2,2) + 14*Power(t2,3) + 
                  2*Power(t2,5)) + 
               s*(4 + 32*t2 - 34*Power(t2,2) + 16*Power(t2,3) + 
                  7*Power(t2,4) + 2*Power(t2,5)))))*T2q(t1,1 - s2 + t1 - t2)\
)/(Power(-1 + t1,2)*(1 - s2 + t1 - t2)*(-1 + t2)*(t1 - s2*t2)*
       (-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) - 
    (8*(2*Power(s2,6)*(s1*(-1 + t1) - (-5 + t1)*t2) + 
         Power(s2,5)*(2 - s1*(-1 + t1)*(3 + 8*t1 - 6*t2) - 25*t2 + 
            2*s*t2 + 22*Power(t2,2) + 2*Power(t1,2)*(1 + s + 5*t2) - 
            t1*(12 + 49*t2 + 6*Power(t2,2) + 2*s*(1 + t2))) + 
         Power(s2,4)*(-3 + (9 - 5*s + 6*s1)*t2 + 
            2*(s - 2*(10 + s1))*Power(t2,2) + 12*Power(t2,3) - 
            2*Power(t1,3)*(5 + 3*s - 6*s1 + 9*t2) + 
            Power(t1,2)*(57 + 92*t2 + 22*Power(t2,2) + s*(3 + 8*t2) - 
               s1*(3 + 20*t2)) + 
            t1*(20 + 69*t2 - 78*Power(t2,2) - 4*Power(t2,3) + 
               s*(3 - 3*t2 - 2*Power(t2,2)) + 
               s1*(-9 + 14*t2 + 4*Power(t2,2)))) + 
         2*Power(s2,3)*((s*(2 - 5*t2) + (-5 + 2*s1 - 12*t2)*(-1 + t2))*
             t2 + Power(t1,4)*(9 + 3*s - 4*s1 + 7*t2) - 
            Power(t1,3)*(49 + s1 + 43*t2 - 12*s1*t2 + 15*Power(t2,2) + 
               s*(-2 + 6*t2)) + 
            t1*(3 - 4*t2 + 63*Power(t2,2) - 18*Power(t2,3) + 
               s*(1 + 5*t2 + 2*Power(t2,2)) + 
               s1*(-2 - 6*t2 + 4*Power(t2,2))) - 
            Power(t1,2)*(47 + 21*t2 - 53*Power(t2,2) - 6*Power(t2,3) + 
               s*(6 + t2 - 3*Power(t2,2)) + 
               s1*(-7 + 4*t2 + 6*Power(t2,2)))) + 
         s2*(4*Power(t1,6) - 4*s*Power(-1 + t2,2)*t2 - 
            Power(t1,5)*(18 + s1*(3 - 2*t2) + 2*s*(-3 + t2) + 17*t2 + 
               4*Power(t2,2)) + 
            4*t1*(-1 + t2)*(s*(1 + 6*t2 - Power(t2,2)) - 
               (-1 + s1 - 3*t2)*(-1 + Power(t2,2))) + 
            Power(t1,4)*(-88 + 47*t2 + 20*Power(t2,2) + 8*Power(t2,3) + 
               s1*(15 - 2*t2 - 4*Power(t2,2)) + 
               2*s*(-2 - 4*t2 + Power(t2,2))) - 
            2*Power(t1,3)*(29 + 4*(-17 + s1)*t2 + 
               (-17 + 2*s1)*Power(t2,2) - 2*(-5 + s1)*Power(t2,3) + 
               2*Power(t2,4) + 3*s*(-1 - 5*t2 + 2*Power(t2,2))) + 
            2*Power(t1,2)*(s*
                (-2 + 2*t2 - 13*Power(t2,2) + 4*Power(t2,3)) + 
               (-1 + t2)*(2 - 31*t2 - 26*Power(t2,2) + 4*Power(t2,3) + 
                  2*s1*(2 + t2)))) - 
         2*Power(s2,2)*(Power(t1,5)*(7 + s - s1 + 2*t2) + 
            2*(-1 + t2)*t2*(s - (-1 + s1 - 3*t2)*(-1 + t2) + 2*s*t2) - 
            Power(t1,4)*(36 + s1*(2 - 6*t2) + 23*t2 + 9*Power(t2,2) + 
               s*(-5 + 4*t2)) + 
            Power(t1,3)*(-71 + 21*t2 + 31*Power(t2,2) + 8*Power(t2,3) + 
               s1*(9 - 6*Power(t2,2)) + s*(-7 - 4*t2 + 3*Power(t2,2))) - 
            Power(t1,2)*(10 - 29*t2 - 69*Power(t2,2) + 20*Power(t2,3) + 
               2*Power(t2,4) + 
               2*s1*(2 + t2 + Power(t2,2) - Power(t2,3)) + 
               s*(1 - 16*t2 - 2*Power(t2,2) + 2*Power(t2,3))) + 
            t1*(-(s*(-2 + 6*t2 + 3*Power(t2,2) + 2*Power(t2,3))) + 
               (-1 + t2)*(-6 - 7*t2 - 42*Power(t2,2) + 4*Power(t2,3) + 
                  s1*(2 + 4*t2)))) - 
         t1*(s*(-1 + t1)*(Power(t1,4) + Power(t1,3)*(2 - 3*t2) + 
               4*Power(-1 + t2,2) + 
               2*Power(t1,2)*(8 - 6*t2 + Power(t2,2)) + 
               4*t1*(5 - 7*t2 + 2*Power(t2,2))) + 
            t1*(Power(t1,4)*(1 - 4*t2) + 4*Power(-1 + t2,2)*(1 + 3*t2) + 
               Power(t1,3)*(-18 + 7*t2 + 8*Power(t2,2)) + 
               2*t1*(-8 + 37*t2 - 33*Power(t2,2) + 4*Power(t2,3)) - 
               Power(t1,2)*(35 - 79*t2 + 18*Power(t2,2) + 
                  4*Power(t2,3)) - 
               s1*(-1 + t1)*(Power(t1,3) + 2*Power(t1,2)*(-2 + t2) - 
                  4*Power(-1 + t2,2) - 4*t1*(2 - 3*t2 + Power(t2,2))))))*
       T3q(s2,t1))/
     (Power(-1 + t1,2)*Power(-s2 + t1,2)*(1 - s2 + t1 - t2)*(-1 + t2)*
       (t1 - s2*t2)) + (8*(4*Power(t1,4)*(-5 + 2*t2) + 
         2*t2*(1 + (-1 + s)*t2) - 
         2*Power(s2,5)*(s1*(-1 + t1) - (-5 + t1)*t2) + 
         Power(t1,2)*(50 + s + s1 - 153*t2 + s*t2 + 22*s1*t2 + 
            24*Power(t2,2) - 8*s1*Power(t2,2) + 8*Power(t2,3)) + 
         Power(t1,3)*(57 + s + 20*t2 - 16*Power(t2,2) + 
            s1*(-21 + 8*t2)) - 
         t1*(23 + 37*t2 - 90*Power(t2,2) + 24*Power(t2,3) + 
            s1*(-20 + 30*t2 - 8*Power(t2,2)) + s*(2 + t2 + 2*Power(t2,2))\
) + Power(s2,4)*(-2 + s1*(-1 + t1)*(7 + 2*t1 - 6*t2) + 45*t2 - 2*s*t2 - 
            22*Power(t2,2) - 2*Power(t1,2)*(1 + s + 2*t2) + 
            t1*(12 + 2*s + 15*t2 + 2*s*t2 + 6*Power(t2,2))) + 
         Power(s2,2)*(-8 - (5 + s)*Power(t1,3) + 7*t2 - 12*s*t2 - 
            116*Power(t2,2) + 8*s*Power(t2,2) + 60*Power(t2,3) + 
            s1*(-1 + t1)*(3 + Power(t1,2) - 36*t2 + 20*Power(t2,2) - 
               4*t1*(1 + 2*t2)) + 
            Power(t1,2)*(70 - 13*t2 + 20*Power(t2,2) + s*(-5 + 9*t2)) + 
            t1*(55 + 22*t2 - 40*Power(t2,2) - 20*Power(t2,3) + 
               s*(6 + 3*t2 - 8*Power(t2,2)))) + 
         Power(s2,3)*(7 + 4*Power(t1,3) - 57*t2 + 9*s*t2 + 
            82*Power(t2,2) - 2*s*Power(t2,2) - 12*Power(t2,3) - 
            Power(t1,2)*(17 - 7*s + t2 + 2*s*t2 + 4*Power(t2,2)) + 
            t1*(-50 - 7*s - 38*t2 - 7*s*t2 + 2*Power(t2,2) + 
               2*s*Power(t2,2) + 4*Power(t2,3)) + 
            2*s1*(Power(t1,2)*(-2 + t2) + 
               t1*(-2 + 9*t2 - 2*Power(t2,2)) + 
               2*(2 - 5*t2 + Power(t2,2)))) + 
         s2*(3 + 4*Power(t1,4) + (13 + 5*s - 22*s1)*t2 + 
            (42 - 8*s + 32*s1)*Power(t2,2) - 8*(11 + s1)*Power(t2,3) + 
            24*Power(t2,4) + 4*Power(t1,3)*
             (2 + s1 - t2 - 2*Power(t2,2)) - 
            Power(t1,2)*(117 + s - 3*t2 + 8*s*t2 - 16*Power(t2,3) + 
               s1*(-22 - 8*t2 + 8*Power(t2,2))) + 
            t1*(6 + 68*t2 + 86*Power(t2,2) - 24*Power(t2,3) - 
               8*Power(t2,4) + s*(1 + 3*t2 + 8*Power(t2,2)) + 
               2*s1*(-13 + 7*t2 - 12*Power(t2,2) + 4*Power(t2,3)))))*
       T4q(-1 + s2))/
     ((-1 + s2)*Power(-1 + t1,2)*(1 - s2 + t1 - t2)*(-1 + t2)*(t1 - s2*t2)) \
+ (8*(Power(t1,4)*(-1 - s + s1 + 4*t2) - 
         2*(-1 + t2)*t2*(1 + (-1 + s)*t2) + 
         2*Power(s2,4)*(s1*(-1 + t1) - (-5 + t1)*t2) + 
         Power(t1,3)*(21 + s1*(-4 + t2) - 10*t2 - 8*Power(t2,2) + 
            s*(-2 + 4*t2)) + Power(t1,2)*
          (49 - 84*t2 + 25*Power(t2,2) + 4*Power(t2,3) + 
            s*(5 + t2 - 5*Power(t2,2)) + s1*(-5 + 9*t2 - 2*Power(t2,2))) + 
         t1*((-1 + t2)*(5 + 2*s1*(-4 + t2) + 37*t2 - 14*Power(t2,2)) + 
            s*(-2 - 5*t2 + 3*Power(t2,2) + 2*Power(t2,3))) - 
         Power(s2,3)*(-2 + s1*(-1 + t1)*(5 + 4*t1 - 8*t2) + 35*t2 - 
            2*s*t2 - 32*Power(t2,2) - 2*Power(t1,2)*(1 + s + 3*t2) + 
            t1*(12 + 27*t2 + 8*Power(t2,2) + 2*s*(1 + t2))) + 
         Power(s2,2)*(-5 + 32*t2 - 7*s*t2 - 67*Power(t2,2) + 
            4*s*Power(t2,2) + 34*Power(t2,3) - 
            2*Power(t1,3)*(3 + s + 2*t2) + 
            s1*(-1 + t1)*(3 + 2*Power(t1,2) + t1*(5 - 10*t2) - 15*t2 + 
               10*Power(t2,2)) + 
            Power(t1,2)*(31 + 22*t2 + 14*Power(t2,2) + s*(-3 + 6*t2)) + 
            t1*(36 + 30*t2 - 51*Power(t2,2) - 10*Power(t2,3) + 
               s*(5 + t2 - 4*Power(t2,2)))) + 
         s2*(3 + 4*Power(t1,4) + (-6 + 5*s - 10*s1)*t2 + 
            (43 - 5*s + 14*s1)*Power(t2,2) + 
            2*(s - 2*(13 + s1))*Power(t2,3) + 12*Power(t2,4) - 
            Power(t1,3)*(16 + s1 + 2*s*(-2 + t2) + 13*t2 - 2*s1*t2 + 
               4*Power(t2,2)) + 
            Power(t1,2)*(-73 + 41*t2 + 17*Power(t2,2) + 8*Power(t2,3) + 
               s1*(5 + 4*t2 - 6*Power(t2,2)) + 
               s*(-3 - 7*t2 + 4*Power(t2,2))) + 
            t1*(-22 + 18*t2 + 40*Power(t2,2) - 20*Power(t2,3) - 
               4*Power(t2,4) + 
               s*(-1 + 4*t2 + Power(t2,2) - 2*Power(t2,3)) + 
               4*s1*(-1 + t2 - 2*Power(t2,2) + Power(t2,3)))))*
       T5q(1 - s2 + t1 - t2))/
     (Power(-1 + t1,2)*(1 - s2 + t1 - t2)*(-1 + t2)*(t1 - s2*t2)));
   return a;
};
