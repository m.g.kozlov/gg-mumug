#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_2_m132_3_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(-16*Power(s2,2) - 8*Power(s2,3) + 8*Power(s2,4) + 
         4*Power(s1,5)*s2*(1 + s2 - t1) + 32*s2*t1 + 64*Power(s2,2)*t1 - 
         8*Power(s2,3)*t1 - 16*Power(s2,4)*t1 - 16*Power(t1,2) - 
         104*s2*Power(t1,2) - 56*Power(s2,2)*Power(t1,2) + 
         40*Power(s2,3)*Power(t1,2) + 8*Power(s2,4)*Power(t1,2) + 
         48*Power(t1,3) + 104*s2*Power(t1,3) - 
         16*Power(s2,2)*Power(t1,3) - 24*Power(s2,3)*Power(t1,3) - 
         48*Power(t1,4) - 24*s2*Power(t1,4) + 24*Power(s2,2)*Power(t1,4) + 
         16*Power(t1,5) - 8*s2*Power(t1,5) - 
         2*Power(s,6)*Power(s1 - s2 + t1 - t2,2) - 36*s2*t2 + 
         15*Power(s2,2)*t2 + 40*Power(s2,3)*t2 - 10*Power(s2,4)*t2 - 
         2*Power(s2,6)*t2 + 36*t1*t2 + 66*s2*t1*t2 - 
         110*Power(s2,2)*t1*t2 - 42*Power(s2,3)*t1*t2 + 
         2*Power(s2,4)*t1*t2 + 8*Power(s2,5)*t1*t2 - 81*Power(t1,2)*t2 + 
         16*s2*Power(t1,2)*t2 + 145*Power(s2,2)*Power(t1,2)*t2 + 
         26*Power(s2,3)*Power(t1,2)*t2 - 20*Power(s2,4)*Power(t1,2)*t2 + 
         54*Power(t1,3)*t2 - 100*s2*Power(t1,3)*t2 - 
         74*Power(s2,2)*Power(t1,3)*t2 + 32*Power(s2,3)*Power(t1,3)*t2 + 
         7*Power(t1,4)*t2 + 62*s2*Power(t1,4)*t2 - 
         26*Power(s2,2)*Power(t1,4)*t2 - 16*Power(t1,5)*t2 + 
         8*s2*Power(t1,5)*t2 - 32*Power(t2,2) + 57*s2*Power(t2,2) + 
         36*Power(s2,2)*Power(t2,2) - 47*Power(s2,3)*Power(t2,2) + 
         4*Power(s2,4)*Power(t2,2) - Power(s2,5)*Power(t2,2) + 
         2*Power(s2,6)*Power(t2,2) + 23*t1*Power(t2,2) - 
         130*s2*t1*Power(t2,2) + 33*Power(s2,2)*t1*Power(t2,2) + 
         29*Power(s2,3)*t1*Power(t2,2) + 21*Power(s2,4)*t1*Power(t2,2) - 
         8*Power(s2,5)*t1*Power(t2,2) + 30*Power(t1,2)*Power(t2,2) + 
         84*s2*Power(t1,2)*Power(t2,2) - 
         47*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         83*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         12*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         54*Power(t1,3)*Power(t2,2) - 17*s2*Power(t1,3)*Power(t2,2) + 
         109*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         8*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         31*Power(t1,4)*Power(t2,2) - 48*s2*Power(t1,4)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         2*Power(t1,5)*Power(t2,2) + 40*Power(t2,3) - 36*s2*Power(t2,3) - 
         58*Power(s2,2)*Power(t2,3) + 9*Power(s2,3)*Power(t2,3) - 
         7*Power(s2,4)*Power(t2,3) + Power(s2,5)*Power(t2,3) - 
         44*t1*Power(t2,3) + 94*s2*t1*Power(t2,3) + 
         56*Power(s2,2)*t1*Power(t2,3) + 53*Power(s2,3)*t1*Power(t2,3) - 
         5*Power(s2,4)*t1*Power(t2,3) + 20*Power(t1,2)*Power(t2,3) - 
         69*s2*Power(t1,2)*Power(t2,3) - 
         105*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         9*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         12*Power(t1,3)*Power(t2,3) + 63*s2*Power(t1,3)*Power(t2,3) - 
         7*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         4*Power(t1,4)*Power(t2,3) + 2*s2*Power(t1,4)*Power(t2,3) - 
         16*Power(t2,4) - 9*s2*Power(t2,4) - 11*Power(s2,2)*Power(t2,4) - 
         16*Power(s2,3)*Power(t2,4) + Power(s2,4)*Power(t2,4) + 
         17*t1*Power(t2,4) + 22*s2*t1*Power(t2,4) + 
         39*Power(s2,2)*t1*Power(t2,4) - 6*Power(s2,3)*t1*Power(t2,4) - 
         3*Power(t1,2)*Power(t2,4) - 25*s2*Power(t1,2)*Power(t2,4) + 
         9*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         2*Power(t1,3)*Power(t2,4) - 4*s2*Power(t1,3)*Power(t2,4) - 
         4*s2*Power(t2,5) - 2*Power(s2,2)*Power(t2,5) + 
         2*Power(s2,3)*Power(t2,5) + 2*s2*t1*Power(t2,5) - 
         4*Power(s2,2)*t1*Power(t2,5) + 2*s2*Power(t1,2)*Power(t2,5) + 
         Power(s,5)*(6 + 4*Power(s1,3) + 10*Power(s2,3) - 2*t1 + 
            3*Power(t1,2) - 6*Power(t1,3) + 
            2*Power(s1,2)*(3 + t1 - 6*t2) + 2*t2 - 6*t1*t2 + 
            8*Power(t1,2)*t2 + 3*Power(t2,2) + 2*t1*Power(t2,2) - 
            4*Power(t2,3) + Power(s2,2)*(8 - 27*t1 + 17*t2) + 
            s2*(-10 - 5*t1 + 23*Power(t1,2) + 5*t2 - 26*t1*t2 + 
               3*Power(t2,2)) - 
            s1*(8 + 20*Power(s2,2) - 9*t1 + 8*Power(t1,2) + 9*t2 + 
               4*t1*t2 - 12*Power(t2,2) + s2*(-4 - 23*t1 + 3*t2))) + 
         Power(s1,4)*(3*Power(s2,4) + 
            2*(-8 + 13*t1 - 7*Power(t1,2) + 2*Power(t1,3)) + 
            Power(s2,3)*(-6 - 9*t1 + 2*t2) + 
            Power(s2,2)*(9*Power(t1,2) - 4*t1*(-6 + t2) - 6*(2 + 3*t2)) + 
            s2*(-3*Power(t1,3) + 2*Power(t1,2)*(-11 + t2) + 
               2*t1*(17 + 9*t2) - 2*(9 + 10*t2))) + 
         Power(s1,3)*(Power(s2,5) + Power(s2,4)*(5 - 10*t2) - 
            Power(s2,3)*(18 + 6*Power(t1,2) + t1*(30 - 33*t2) - 34*t2 + 
               8*Power(t2,2)) + 
            (-1 + t1)*(40 + 5*Power(t1,3) + Power(t1,2)*(7 - 14*t2) - 
               64*t2 + t1*(-5 + 31*t2)) + 
            s2*(35 - 3*Power(t1,4) + 63*t2 + 40*Power(t2,2) + 
               Power(t1,3)*(-46 + 13*t2) + 
               Power(t1,2)*(65 + 91*t2 - 8*Power(t2,2)) - 
               4*t1*(24 + 31*t2 + 8*Power(t2,2))) + 
            Power(s2,2)*(52 + 8*Power(t1,3) + Power(t1,2)*(66 - 36*t2) + 
               47*t2 + 32*Power(t2,2) + t1*(-33 - 111*t2 + 16*Power(t2,2))\
)) + Power(s1,2)*(2*Power(s2,6) - Power(s2,5)*(-1 + 8*t1 + t2) + 
            Power(s2,4)*(2 + 12*Power(t1,2) - 5*t1*(-3 + t2) - 17*t2 + 
               12*Power(t2,2)) + 
            Power(s2,3)*(-56 - 8*Power(t1,3) + 45*t2 - 66*Power(t2,2) + 
               12*Power(t2,3) + Power(t1,2)*(-76 + 21*t2) + 
               t1*(43 + 113*t2 - 45*Power(t2,2))) + 
            (-1 + t1)*(Power(t1,4) + Power(t1,3)*(39 - 14*t2) + 
               t1*(21 + 14*t2 - 33*Power(t2,2)) + 
               2*Power(t1,2)*(-20 - 15*t2 + 9*Power(t2,2)) + 
               8*(4 - 15*t2 + 12*Power(t2,2))) + 
            Power(s2,2)*(43 + 2*Power(t1,4) + 
               Power(t1,3)*(104 - 23*t2) - 162*t2 - 69*Power(t2,2) - 
               28*Power(t2,3) + 
               Power(t1,2)*(-62 - 237*t2 + 54*Power(t2,2)) + 
               t1*(38 + 122*t2 + 189*Power(t2,2) - 24*Power(t2,3))) + 
            s2*(69 - 106*t2 - 81*Power(t2,2) - 40*Power(t2,3) + 
               Power(t1,4)*(-45 + 8*t2) + 
               Power(t1,3)*(-21 + 155*t2 - 21*Power(t2,2)) + 
               Power(t1,2)*(113 - 199*t2 - 141*Power(t2,2) + 
                  12*Power(t2,3)) + 
               2*t1*(-84 + 143*t2 + 84*Power(t2,2) + 14*Power(t2,3)))) - 
         s1*(Power(s2,6)*(-2 + 4*t2) + 
            Power(s2,5)*(t1*(8 - 16*t2) + Power(t2,2)) + 
            Power(s2,4)*(-10 + 6*t2 - 19*Power(t2,2) + 6*Power(t2,3) + 
               4*Power(t1,2)*(-5 + 6*t2) + 
               t1*(2 + 36*t2 - 10*Power(t2,2))) + 
            Power(s2,3)*(40 - 16*Power(t1,3)*(-2 + t2) - 103*t2 + 
               36*Power(t2,2) - 54*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,2)*(26 - 159*t2 + 24*Power(t2,2)) + 
               t1*(-42 + 72*t2 + 136*Power(t2,2) - 27*Power(t2,3))) + 
            (-1 + t1)*(Power(t1,4)*(-16 + 3*t2) + 
               Power(t1,3)*(-9 + 72*t2 - 13*Power(t2,2)) + 
               8*t2*(8 - 15*t2 + 8*Power(t2,2)) + 
               t1*(-36 + 30*t2 + 13*Power(t2,2) - 13*Power(t2,3)) + 
               Power(t1,2)*(45 - 61*t2 - 39*Power(t2,2) + 10*Power(t2,3))\
) + Power(s2,2)*(15 + 79*t2 - 168*Power(t2,2) - 45*Power(t2,3) - 
               12*Power(t2,4) + Power(t1,4)*(-26 + 4*t2) + 
               Power(t1,3)*(-74 + 213*t2 - 22*Power(t2,2)) + 
               Power(t1,2)*(145 - 109*t2 - 276*Power(t2,2) + 
                  36*Power(t2,3)) + 
               t1*(-110 + 71*t2 + 145*Power(t2,2) + 141*Power(t2,3) - 
                  16*Power(t2,4))) + 
            s2*(-36 + 8*Power(t1,5) + 126*t2 - 107*Power(t2,2) - 
               45*Power(t2,3) - 20*Power(t2,4) + 
               Power(t1,4)*(62 - 93*t2 + 7*Power(t2,2)) - 
               Power(t1,3)*(100 + 38*t2 - 172*Power(t2,2) + 
                  15*Power(t2,3)) + 
               2*t1*(33 - 149*t2 + 142*Power(t2,2) + 50*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(t1,2)*(16 + 197*t2 - 203*Power(t2,2) - 
                  97*Power(t2,3) + 8*Power(t2,4)))) - 
         Power(s,4)*(24 + 2*Power(s1,4) + 12*Power(s2,4) - 11*t1 + 
            9*Power(t1,2) - 2*Power(t1,3) + 6*Power(t1,4) + 
            Power(s1,3)*(17 + 8*s2 - 8*t1 - 8*t2) - 19*t2 - 12*t1*t2 - 
            8*Power(t1,2)*t2 + 3*Power(t2,2) + 22*t1*Power(t2,2) - 
            16*Power(t1,2)*Power(t2,2) - 12*Power(t2,3) + 
            8*t1*Power(t2,3) + 2*Power(t2,4) + 
            Power(s2,3)*(20 - 50*t1 + 13*t2) + 
            Power(s1,2)*(9 - 31*Power(s2,2) - 16*Power(t1,2) + 
               s2*(17 + 33*t1 - 32*t2) - 46*t2 + 12*Power(t2,2) + 
               t1*(17 + 24*t2)) + 
            Power(s2,2)*(32 + 70*Power(t1,2) + 5*t2 - 17*Power(t2,2) - 
               t1*(52 + 33*t2)) + 
            s2*(-37 - 38*Power(t1,3) + 71*t2 + 3*Power(t2,2) - 
               16*Power(t2,3) + Power(t1,2)*(34 + 20*t2) + 
               t1*(-17 - 37*t2 + 34*Power(t2,2))) - 
            s1*(5 + 21*Power(s2,3) + Power(t1,2)*(2 - 32*t2) + 12*t2 - 
               41*Power(t2,2) + 8*Power(t2,3) - 
               2*Power(s2,2)*(6 + 17*t1 + 24*t2) + 
               3*t1*(-16 + 13*t2 + 8*Power(t2,2)) + 
               s2*(59 + 13*Power(t1,2) + 20*t2 - 40*Power(t2,2) + 
                  t1*(-6 + 67*t2)))) + 
         Power(s,3)*(30 + 2*Power(s2,5) + 
            Power(s1,4)*(11 + 4*s2 - 6*t1) - 18*t1 + 53*Power(t1,2) - 
            15*Power(t1,3) - 5*Power(t1,4) - 2*Power(t1,5) + 
            Power(s2,4)*(20 - 23*t1 - 5*t2) - 78*t2 - 70*t1*t2 + 
            5*Power(t1,2)*t2 + 32*Power(t1,3)*t2 - 8*Power(t1,4)*t2 + 
            77*Power(t2,2) + 31*t1*Power(t2,2) - 
            40*Power(t1,2)*Power(t2,2) + 16*Power(t1,3)*Power(t2,2) - 
            21*Power(t2,3) + 4*t1*Power(t2,3) + 9*Power(t2,4) - 
            6*t1*Power(t2,4) + 
            Power(s2,3)*(14 + 59*Power(t1,2) + 20*t1*(-4 + t2) + 6*t2 - 
               31*Power(t2,2)) + 
            Power(s1,3)*(44 - 10*Power(s2,2) + s2*(30 + t1 - 23*t2) - 
               42*t2 + 6*t1*(-3 + 4*t2)) + 
            Power(s2,2)*(89 - 59*Power(t1,3) + 
               Power(t1,2)*(103 - 33*t2) - 73*t2 + 3*Power(t2,2) - 
               13*Power(t2,3) + t1*(-75 + 2*t2 + 85*Power(t2,2))) + 
            s2*(-42 + 23*Power(t1,4) + 268*t2 - 132*Power(t2,2) - 
               34*Power(t2,3) + 11*Power(t2,4) + 
               Power(t1,3)*(-38 + 26*t2) + 
               Power(t1,2)*(76 - 32*t2 - 70*Power(t2,2)) + 
               2*t1*(-56 - 30*t2 + 52*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s1,2)*(63 - 54*Power(s2,3) - 44*Power(t1,2) + 
               16*Power(t1,3) - 109*t2 + 60*Power(t2,2) + 
               Power(s2,2)*(23 + 95*t1 + 7*t2) + 
               t1*(53 + 40*t2 - 36*Power(t2,2)) + 
               s2*(-92 - 57*Power(t1,2) - 94*t2 + 45*Power(t2,2) + 
                  8*t1*(5 + t2))) - 
            s1*(-48 + 2*Power(s2,4) - 8*Power(t1,4) + 
               Power(s2,3)*(11*t1 - 85*t2) + Power(t1,2)*(66 - 84*t2) + 
               140*t2 - 86*Power(t2,2) + 38*Power(t2,3) + 
               4*Power(t1,3)*(5 + 8*t2) + 
               t1*(-152 + 84*t2 + 26*Power(t2,2) - 24*Power(t2,3)) - 
               2*Power(s2,2)*(56 + 18*Power(t1,2) - 13*t2 + 
                  8*Power(t2,2) - 5*t1*(5 + 18*t2)) + 
               s2*(290 + 31*Power(t1,3) - 224*t2 - 98*Power(t2,2) + 
                  37*Power(t2,3) - Power(t1,2)*(62 + 127*t2) + 
                  t1*(-52 + 144*t2 + 19*Power(t2,2))))) + 
         Power(s,2)*(-12 + 2*Power(s2,6) + 5*t1 - 92*Power(t1,2) + 
            103*Power(t1,3) - 16*Power(t1,4) - 4*Power(t1,5) + 85*t2 + 
            134*t1*t2 - 143*Power(t1,2)*t2 - 14*Power(t1,3)*t2 + 
            16*Power(t1,4)*t2 - 4*Power(t1,5)*t2 - 186*Power(t2,2) + 
            19*t1*Power(t2,2) + 72*Power(t1,2)*Power(t2,2) + 
            2*Power(t1,4)*Power(t2,2) + 81*Power(t2,3) - 
            34*t1*Power(t2,3) - 32*Power(t1,2)*Power(t2,3) + 
            8*Power(t1,3)*Power(t2,3) - 8*Power(t2,4) + 
            20*t1*Power(t2,4) - 6*Power(t1,2)*Power(t2,4) + 
            Power(s2,5)*(-6 - 4*t1 + t2) + 
            Power(s1,4)*(-19 + Power(s2,2) + 26*t1 - 6*Power(t1,2) + 
               s2*(-17 + 5*t1 + 2*t2)) - 
            Power(s2,4)*(-12 + 4*Power(t1,2) + t2 - 11*Power(t2,2) + 
               t1*(-35 + 19*t2)) + 
            Power(s2,3)*(-22 + 16*Power(t1,3) + 86*t2 + 10*Power(t2,2) - 
               5*Power(t2,3) + Power(t1,2)*(-65 + 55*t2) - 
               t1*(18 + 47*t2 + 44*Power(t2,2))) + 
            Power(s2,2)*(-126 - 14*Power(t1,4) + 
               Power(t1,3)*(45 - 61*t2) + 92*t2 + 30*Power(t2,2) + 
               37*Power(t2,3) - 15*Power(t2,4) + 
               Power(t1,2)*(-32 + 137*t2 + 57*Power(t2,2)) + 
               t1*(190 - 250*t2 - 97*Power(t2,2) + 23*Power(t2,3))) + 
            s2*(19 + 4*Power(t1,5) - 336*t2 + 333*Power(t2,2) - 
               28*Power(t2,3) - 28*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,4)*(-5 + 28*t2) + 
               Power(t1,3)*(54 - 105*t2 - 26*Power(t2,2)) + 
               Power(t1,2)*(-271 + 162*t2 + 111*Power(t2,2) - 
                  26*Power(t2,3)) + 
               t1*(206 + 184*t2 - 312*Power(t2,2) + 27*Power(t2,3) + 
                  18*Power(t2,4))) + 
            Power(s1,3)*(-100 + 29*Power(s2,3) - 8*Power(t1,3) + 
               t1*(70 - 98*t2) + 65*t2 + 4*Power(t1,2)*(5 + 6*t2) + 
               Power(s2,2)*(-19 - 44*t1 + 12*t2) + 
               s2*(-12 + 23*Power(t1,2) + 79*t2 - 8*Power(t2,2) - 
                  3*t1*(2 + 11*t2))) + 
            Power(s1,2)*(-157 + 27*Power(s2,4) + 2*Power(t1,4) + 
               Power(s2,3)*(1 - 65*t1 - 63*t2) + 281*t2 - 
               81*Power(t2,2) + 12*Power(t1,3)*(-1 + 2*t2) - 
               3*Power(t1,2)*(-47 + 24*t2 + 12*Power(t2,2)) + 
               t1*(-61 - 174*t2 + 138*Power(t2,2)) + 
               Power(s2,2)*(-34 + 51*Power(t1,2) + 75*t2 - 
                  42*Power(t2,2) + t1*(-1 + 111*t2)) + 
               s2*(339 - 15*Power(t1,3) + Power(t1,2)*(36 - 72*t2) - 
                  4*t2 - 135*Power(t2,2) + 12*Power(t2,3) + 
                  t1*(-272 + 39*t2 + 69*Power(t2,2)))) + 
            s1*(-73 + Power(s2,5) + 4*Power(t1,5) + 
               Power(s2,4)*(5 + 14*t1 - 38*t2) + 343*t2 - 
               262*Power(t2,2) + 43*Power(t2,3) - 
               2*Power(t1,4)*(5 + 2*t2) - 
               12*Power(t1,3)*(2 - t2 + 2*Power(t2,2)) + 
               t1*(-195 + 42*t2 + 138*Power(t2,2) - 86*Power(t2,3)) + 
               Power(t1,2)*(226 - 213*t2 + 84*Power(t2,2) + 
                  24*Power(t2,3)) + 
               Power(s2,3)*(-109 - 52*Power(t1,2) - 11*t2 + 
                  39*Power(t2,2) + t1*(56 + 109*t2)) + 
               Power(s2,2)*(-103 + 62*Power(t1,3) + 4*t2 - 
                  93*Power(t2,2) + 44*Power(t2,3) - 
                  Power(t1,2)*(161 + 108*t2) + 
                  t1*(303 + 98*t2 - 90*Power(t2,2))) + 
               s2*(373 - 29*Power(t1,4) - 672*t2 + 44*Power(t2,2) + 
                  101*Power(t2,3) - 8*Power(t2,4) + 
                  Power(t1,3)*(110 + 41*t2) + 
                  Power(t1,2)*(-154 - 147*t2 + 75*Power(t2,2)) - 
                  t1*(244 - 584*t2 + 60*Power(t2,2) + 59*Power(t2,3))))) + 
         s*(-4*Power(s1,5)*s2 + 4*t1 + 63*Power(t1,2) - 130*Power(t1,3) + 
            71*Power(t1,4) - 8*Power(t1,5) + 
            Power(s2,5)*(4 + t1*(4 - 12*t2) - 3*t2) - 28*t2 - 116*t1*t2 + 
            202*Power(t1,2)*t2 - 47*Power(t1,3)*t2 - 25*Power(t1,4)*t2 - 
            2*Power(t1,5)*t2 + 143*Power(t2,2) - 60*t1*Power(t2,2) - 
            112*Power(t1,2)*Power(t2,2) + 69*Power(t1,3)*Power(t2,2) + 
            17*Power(t1,4)*Power(t2,2) - 2*Power(t1,5)*Power(t2,2) - 
            108*Power(t2,3) + 101*t1*Power(t2,3) - 
            25*Power(t1,2)*Power(t2,3) - 28*Power(t1,3)*Power(t2,3) + 
            4*Power(t1,4)*Power(t2,3) + 17*Power(t2,4) - 
            11*t1*Power(t2,4) + 13*Power(t1,2)*Power(t2,4) - 
            2*Power(t1,3)*Power(t2,4) + Power(s2,6)*(-2 + 4*t2) + 
            Power(s2,4)*(-30 + 20*t2 - 12*Power(t2,2) + 5*Power(t2,3) + 
               Power(t1,2)*(-4 + 8*t2) + t1*(6 + 24*t2 - Power(t2,2))) + 
            Power(s2,3)*(24 - 137*t2 + 61*Power(t2,2) - 8*Power(t2,3) + 
               5*Power(t2,4) + 8*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(-42 - 76*t2 + 5*Power(t2,2)) + 
               t1*(66 + 75*t2 + 14*Power(t2,2) - 20*Power(t2,3))) + 
            Power(s2,2)*(79 - 58*t2 - 131*Power(t2,2) + 16*Power(t2,3) + 
               35*Power(t2,4) - 4*Power(t2,5) - 
               2*Power(t1,4)*(5 + 6*t2) + 
               Power(t1,3)*(42 + 90*t2 - 9*Power(t2,2)) + 
               Power(t1,2)*(37 - 283*t2 + 25*Power(t2,2) + 
                  29*Power(t2,3)) + 
               t1*(-198 + 391*t2 + 37*Power(t2,2) - 72*Power(t2,3) - 
                  6*Power(t2,4))) + 
            s2*(-4 + 180*t2 - 250*Power(t2,2) + 65*Power(t2,3) + 
               7*Power(t2,4) + 2*Power(t2,5) + 4*Power(t1,5)*(1 + t2) + 
               Power(t1,4)*(-2 - 33*t2 + 7*Power(t2,2)) - 
               Power(t1,3)*(144 - 213*t2 + 44*Power(t2,2) + 
                  18*Power(t2,3)) + 
               Power(t1,2)*(304 - 199*t2 - 207*Power(t2,2) + 
                  124*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(-142 - 188*t2 + 450*Power(t2,2) - 77*Power(t2,3) - 
                  53*Power(t2,4) + 4*Power(t2,5))) + 
            Power(s1,4)*(26 - 6*Power(s2,3) - 33*t1 + 19*Power(t1,2) - 
               2*Power(t1,3) + 2*Power(s2,2)*(6 + 5*t1 - 2*t2) + 
               s2*(19 - 2*Power(t1,2) + 18*t2 + t1*(-39 + 4*t2))) + 
            Power(s1,3)*(109 - 16*Power(s2,4) - 4*Power(t1,4) + 
               Power(t1,2)*(28 - 70*t2) - 95*t2 + 
               Power(t1,3)*(26 + 8*t2) + 
               Power(s2,3)*(1 + 35*t1 + 13*t2) + 2*t1*(-56 + 55*t2) + 
               Power(s2,2)*(10 - 26*Power(t1,2) + t1*(39 - 24*t2) - 
                  71*t2 + 16*Power(t2,2)) + 
               s2*(11*Power(t1,3) + Power(t1,2)*(-82 + 3*t2) + 
                  t1*(33 + 170*t2 - 16*Power(t2,2)) - 
                  8*(5 + 8*t2 + 4*Power(t2,2)))) - 
            Power(s1,2)*(-131 + 4*Power(s2,5) + 2*Power(t1,5) + 
               Power(s2,4)*(14 - 9*t1 - 37*t2) + 326*t2 - 
               129*Power(t2,2) - 2*Power(t1,4)*(5 + 6*t2) + 
               t1*t2*(-325 + 132*t2) + 
               Power(t1,2)*(203 + 81*t2 - 96*Power(t2,2)) + 
               Power(t1,3)*(-117 + 80*t2 + 12*Power(t2,2)) + 
               Power(s2,3)*(-93 + Power(t1,2) + 10*t2 + 3*Power(t2,2) + 
                  18*t1*(1 + 5*t2)) + 
               Power(s2,2)*(114 + 11*Power(t1,3) + 4*t2 - 
                  141*Power(t2,2) + 24*Power(t2,3) - 
                  Power(t1,2)*(88 + 81*t2) + 
                  t1*(39 + 150*t2 - 12*Power(t2,2))) - 
               s2*(-286 + 9*Power(t1,4) + 145*t2 + 78*Power(t2,2) + 
                  28*Power(t2,3) - 2*Power(t1,3)*(33 + 20*t2) + 
                  Power(t1,2)*(-211 + 288*t2 + 3*Power(t2,2)) + 
                  t1*(512 - 143*t2 - 276*Power(t2,2) + 24*Power(t2,3)))) + 
            s1*(28 - 4*Power(s2,6) - 274*t2 + 325*Power(t2,2) - 
               77*Power(t2,3) + Power(t1,5)*(3 + 4*t2) + 
               Power(s2,5)*(1 + 12*t1 + 4*t2) - 
               3*Power(t1,4)*(-6 + 9*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(-233 + 315*t2 + 78*Power(t2,2) - 
                  58*Power(t2,3)) + 
               2*Power(t1,3)*(36 - 93*t2 + 41*Power(t2,2) + 
                  4*Power(t2,3)) + 
               2*t1*(64 + 30*t2 - 157*Power(t2,2) + 33*Power(t2,3)) - 
               2*Power(s2,4)*(9 + 4*Power(t1,2) - 13*t2 + 
                  13*Power(t2,2) + t1*(9 + 4*t2)) + 
               Power(s2,3)*(146 - 8*Power(t1,3) + 
                  Power(t1,2)*(69 - 4*t2) - 154*t2 + 17*Power(t2,2) - 
                  9*Power(t2,3) + t1*(-89 + 4*t2 + 75*Power(t2,2))) + 
               Power(s2,2)*(51 + 12*Power(t1,4) + 245*t2 - 
                  22*Power(t2,2) - 117*Power(t2,3) + 16*Power(t2,4) + 
                  5*Power(t1,3)*(-17 + 4*t2) + 
                  Power(t1,2)*(298 - 113*t2 - 84*Power(t2,2)) + 
                  t1*(-396 + 2*t2 + 183*Power(t2,2) + 8*Power(t2,3))) - 
               s2*(4*Power(t1,5) + 2*Power(t1,4)*(-15 + 8*t2) + 
                  Power(t1,3)*(209 - 110*t2 - 47*Power(t2,2)) + 
                  Power(t1,2)*
                   (-170 - 418*t2 + 330*Power(t2,2) + 7*Power(t2,3)) + 
                  2*(96 - 268*t2 + 85*Power(t2,2) + 20*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  t1*(-226 + 962*t2 - 187*Power(t2,2) - 198*Power(t2,3) + 
                     16*Power(t2,4)))))))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(1 - s + s2 - t1)*(-1 + s1 + t1 - t2)*
       (-1 + s - s1 + t2)*(s - s1 + t2)*Power(-s1 + s2 - t1 + t2,2)) - 
    (8*(-24 + 22*s2 - 22*Power(s2,2) + 4*Power(s2,3) - 30*t1 + 
         10*Power(s2,2)*t1 - 4*Power(s2,3)*t1 + 46*Power(t1,2) - 
         10*s2*Power(t1,2) + 12*Power(s2,2)*Power(t1,2) + 4*Power(t1,3) - 
         12*s2*Power(t1,3) + 4*Power(t1,4) + 
         2*Power(s1,4)*(Power(s2,2) + Power(t1,2) - 2*s2*(2 + t1)) + 
         2*Power(s,5)*(t1 - t2) + 88*t2 - 6*s2*t2 + 33*Power(s2,2)*t2 - 
         22*Power(s2,3)*t2 + 4*Power(s2,4)*t2 - 90*t1*t2 - 96*s2*t1*t2 + 
         13*Power(s2,2)*t1*t2 - 12*Power(s2,3)*t1*t2 + 7*Power(t1,2)*t2 + 
         12*s2*Power(t1,2)*t2 + 12*Power(s2,2)*Power(t1,2)*t2 - 
         3*Power(t1,3)*t2 - 4*s2*Power(t1,3)*t2 - 16*Power(t2,2) + 
         87*s2*Power(t2,2) + 11*Power(s2,2)*Power(t2,2) + 
         9*Power(s2,3)*Power(t2,2) + 33*t1*Power(t2,2) + 
         5*s2*t1*Power(t2,2) - 18*Power(s2,2)*t1*Power(t2,2) - 
         16*Power(t1,2)*Power(t2,2) + s2*Power(t1,2)*Power(t2,2) - 
         8*Power(t2,3) - 9*s2*Power(t2,3) + 3*Power(s2,2)*Power(t2,3) - 
         2*Power(s2,3)*Power(t2,3) + t1*Power(t2,3) + 
         11*s2*t1*Power(t2,3) + 2*Power(s2,2)*t1*Power(t2,3) + 
         2*Power(t1,2)*Power(t2,3) - 10*s2*Power(t2,4) + 
         2*Power(s2,2)*Power(t2,4) + 2*t1*Power(t2,4) - 
         2*s2*t1*Power(t2,4) + 
         2*Power(s,4)*(-3 - 4*t1 + Power(t1,2) + 6*t2 + 2*t1*t2 - 
            3*Power(t2,2) + s2*(4 - 2*t1 + t2) + 
            s1*(-2 + s2 - 4*t1 + 3*t2)) - 
         Power(s,3)*(-28 + 6*t1 + 9*Power(t1,2) + 26*t2 - 
            6*Power(t1,2)*t2 - 25*Power(t2,2) + 6*Power(t2,3) - 
            2*Power(s2,2)*(-8 + t1 + t2) + 
            2*Power(s1,2)*(-4 + 3*s2 - 6*t1 + 3*t2) + 
            s2*(14 + 2*Power(t1,2) + 3*t2 - 4*Power(t2,2) + 
               5*t1*(-5 + 2*t2)) + 
            s1*(-16 + 4*Power(s2,2) + 8*Power(t1,2) + 33*t2 - 
               12*Power(t2,2) - 2*s2*(-6 + 8*t1 + t2) + 3*t1*(-5 + 4*t2))\
) + Power(s1,3)*(8 + 2*Power(s2,3) + Power(s2,2)*(5 - 4*t1 - 8*t2) + 
            Power(t1,2)*(3 - 6*t2) - 2*t1*(3 + t2) + 
            2*s2*(7 + Power(t1,2) + 17*t2 + t1*(-12 + 7*t2))) + 
         Power(s,2)*(-56 + 45*t1 + 13*Power(t1,2) - Power(t1,3) - 
            2*Power(s2,3)*(-2 + t2) + 47*t2 - 62*t1*t2 - 
            13*Power(t1,2)*t2 - 11*Power(t2,2) + 23*t1*Power(t2,2) + 
            6*Power(t1,2)*Power(t2,2) + 15*Power(t2,3) - 
            4*t1*Power(t2,3) - 2*Power(t2,4) + 
            2*Power(s1,3)*(-2 + 3*s2 - 4*t1 + t2) + 
            Power(s2,2)*(40 - 26*t2 + 6*Power(t2,2) + t1*(-9 + 6*t2)) + 
            Power(s1,2)*(-4 + 10*Power(s2,2) + 12*Power(t1,2) + 23*t2 - 
               6*Power(t2,2) + 6*t1*(-1 + 2*t2) - s2*(3 + 24*t1 + 10*t2)\
) + s1*(-53 + 2*Power(s2,3) + t1*(39 - 17*t2) + 
               Power(s2,2)*(33 - 8*t1 - 16*t2) + 15*t2 - 
               34*Power(t2,2) + 6*Power(t2,3) - 
               3*Power(t1,2)*(-7 + 6*t2) + 
               2*s2*(19 + 3*Power(t1,2) + 17*t1*(-2 + t2) + 19*t2 + 
                  Power(t2,2))) - 
            s2*(13 + 11*t2 + 35*Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,2)*(2 + 4*t2) + t1*(21 - 55*t2 + 10*Power(t2,2)))\
) - Power(s1,2)*(Power(t1,3) + 8*(2 + 3*t2) + Power(s2,3)*(-9 + 6*t2) + 
            Power(s2,2)*(-26 + t1*(17 - 10*t2) + 7*t2 - 
               12*Power(t2,2)) + 
            Power(t1,2)*(19 + 4*t2 - 6*Power(t2,2)) - 
            t1*(37 + 13*t2 + 6*Power(t2,2)) + 
            s2*(-83 + 37*t2 + 54*Power(t2,2) + Power(t1,2)*(-1 + 4*t2) + 
               t1*(7 - 59*t2 + 18*Power(t2,2)))) - 
         s1*(4*Power(s2,4) - Power(t1,3)*(-5 + t2) - 
            2*Power(s2,3)*(11 + 6*t1 - 9*t2 + 3*Power(t2,2)) - 
            8*(-11 + 4*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(9 - 35*t2 + Power(t2,2) + 2*Power(t2,3)) + 
            2*t1*(-50 + 35*t2 + 4*Power(t2,2) + 3*Power(t2,3)) + 
            Power(s2,2)*(31 + 12*Power(t1,2) + 37*t2 + Power(t2,2) + 
               8*Power(t2,3) + t1*(15 - 35*t2 + 8*Power(t2,2))) - 
            2*s2*(-2 + 2*Power(t1,3) - 85*t2 + 16*Power(t2,2) + 
               19*Power(t2,3) + Power(t1,2)*(-1 - t2 + Power(t2,2)) + 
               t1*(48 + t2 - 23*Power(t2,2) + 5*Power(t2,3)))) + 
         s*(58 + 4*Power(s2,4) - 2*Power(s1,4)*(s2 - t1) + 6*t1 + 
            13*Power(t1,2) + 17*Power(t1,3) - 122*t2 + 78*t1*t2 - 
            39*Power(t1,2)*t2 - Power(t1,3)*t2 + 9*Power(t2,2) - 
            27*t1*Power(t2,2) - 2*Power(t1,2)*Power(t2,2) + Power(t2,3) + 
            17*t1*Power(t2,3) + 2*Power(t1,2)*Power(t2,3) + 
            2*Power(t2,4) - 2*t1*Power(t2,4) - 
            Power(s2,3)*(28 + 12*t1 - 13*t2 + 4*Power(t2,2)) - 
            Power(s1,3)*(6 + 8*Power(s2,2) + t1 + 8*Power(t1,2) + 2*t2 + 
               4*t1*t2 - s2*(15 + 16*t1 + 6*t2)) + 
            Power(s2,2)*(45 + 12*Power(t1,2) + 35*t2 - 7*Power(t2,2) + 
               6*Power(t2,3) + t1*(41 - 27*t2 + 6*Power(t2,2))) - 
            s2*(10 + 4*Power(t1,3) - 80*t2 + 24*Power(t2,2) + 
               34*Power(t2,3) + Power(t1,2)*(30 + t2 + 2*Power(t2,2)) + 
               t1*(112 - 32*t2 - 41*Power(t2,2) + 6*Power(t2,3))) + 
            s1*(2*Power(t1,3) + Power(s2,3)*(-13 + 8*t2) + 
               Power(s2,2)*(-52 + t1*(26 - 16*t2) + 29*t2 - 
                  20*Power(t2,2)) + 
               Power(t1,2)*(18 + 17*t2 - 12*Power(t2,2)) - 
               2*(-66 + 11*t2 + 4*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(-86 + 50*t2 - 35*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-76 + 70*t2 + 83*Power(t2,2) + 2*Power(t2,3) + 
                  4*t1*t2*(-27 + 7*t2) + Power(t1,2)*(1 + 8*t2))) + 
            Power(s1,2)*(13 - 4*Power(s2,3) + 
               2*Power(s2,2)*(5*t1 + 11*(-1 + t2)) + 13*t2 + 
               6*Power(t2,2) + 3*Power(t1,2)*(-5 + 6*t2) + 
               t1*(-23 + 19*t2) - 
               s2*(46 + 6*Power(t1,2) + 64*t2 + 6*Power(t2,2) + 
                  t1*(-67 + 38*t2)))))*
       B1(1 - s1 - t1 + t2,1 - s + s1 - t2,1 - s + s2 - t1))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(1 - s + s*s2 - s1*s2 - t1 + s2*t2)) - 
    (16*(-12*s2 - 104*Power(s2,2) - 175*Power(s2,3) - 55*Power(s2,4) + 
         50*Power(s2,5) + 17*Power(s2,6) - 5*Power(s2,7) - 
         4*Power(s1,5)*(-1 + s2)*(1 + s2 - t1) + 12*t1 + 252*s2*t1 + 
         729*Power(s2,2)*t1 + 529*Power(s2,3)*t1 - 97*Power(s2,4)*t1 - 
         126*Power(s2,5)*t1 + 6*Power(s2,6)*t1 - Power(s2,7)*t1 - 
         148*Power(t1,2) - 977*s2*Power(t1,2) - 
         1373*Power(s2,2)*Power(t1,2) - 245*Power(s2,3)*Power(t1,2) + 
         286*Power(s2,4)*Power(t1,2) + 40*Power(s2,5)*Power(t1,2) + 
         5*Power(s2,6)*Power(t1,2) + 423*Power(t1,3) + 
         1383*s2*Power(t1,3) + 821*Power(s2,2)*Power(t1,3) - 
         229*Power(s2,3)*Power(t1,3) - 120*Power(s2,4)*Power(t1,3) - 
         10*Power(s2,5)*Power(t1,3) - 484*Power(t1,4) - 
         761*s2*Power(t1,4) - 12*Power(s2,2)*Power(t1,4) + 
         139*Power(s2,3)*Power(t1,4) + 10*Power(s2,4)*Power(t1,4) + 
         232*Power(t1,5) + 95*s2*Power(t1,5) - 
         82*Power(s2,2)*Power(t1,5) - 5*Power(s2,3)*Power(t1,5) - 
         31*Power(t1,6) + 26*s2*Power(t1,6) + Power(s2,2)*Power(t1,6) - 
         4*Power(t1,7) - 48*t2 - 238*s2*t2 - 359*Power(s2,2)*t2 - 
         206*Power(s2,3)*t2 - 53*Power(s2,4)*t2 - 3*Power(s2,5)*t2 + 
         22*Power(s2,6)*t2 + 9*Power(s2,7)*t2 + 342*t1*t2 + 
         1156*s2*t1*t2 + 1193*Power(s2,2)*t1*t2 + 480*Power(s2,3)*t1*t2 + 
         61*Power(s2,4)*t1*t2 - 98*Power(s2,5)*t1*t2 - 
         46*Power(s2,6)*t1*t2 - 853*Power(t1,2)*t2 - 
         1922*s2*Power(t1,2)*t2 - 1299*Power(s2,2)*Power(t1,2)*t2 - 
         302*Power(s2,3)*Power(t1,2)*t2 + 
         160*Power(s2,4)*Power(t1,2)*t2 + 96*Power(s2,5)*Power(t1,2)*t2 + 
         927*Power(t1,3)*t2 + 1316*s2*Power(t1,3)*t2 + 
         499*Power(s2,2)*Power(t1,3)*t2 - 
         122*Power(s2,3)*Power(t1,3)*t2 - 
         104*Power(s2,4)*Power(t1,3)*t2 - 436*Power(t1,4)*t2 - 
         313*s2*Power(t1,4)*t2 + 56*Power(s2,2)*Power(t1,4)*t2 + 
         61*Power(s2,3)*Power(t1,4)*t2 + 58*Power(t1,5)*t2 - 
         28*s2*Power(t1,5)*t2 - 18*Power(s2,2)*Power(t1,5)*t2 + 
         10*Power(t1,6)*t2 + 2*s2*Power(t1,6)*t2 - 80*Power(t2,2) - 
         258*s2*Power(t2,2) - 319*Power(s2,2)*Power(t2,2) - 
         192*Power(s2,3)*Power(t2,2) - 37*Power(s2,4)*Power(t2,2) + 
         18*Power(s2,5)*Power(t2,2) + 3*Power(s2,6)*Power(t2,2) + 
         318*t1*Power(t2,2) + 752*s2*t1*Power(t2,2) + 
         729*Power(s2,2)*t1*Power(t2,2) + 
         315*Power(s2,3)*t1*Power(t2,2) - 16*Power(s2,4)*t1*Power(t2,2) - 
         9*Power(s2,5)*t1*Power(t2,2) - 381*Power(t1,2)*Power(t2,2) - 
         720*s2*Power(t1,2)*Power(t2,2) - 
         544*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         47*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         3*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         155*Power(t1,3)*Power(t2,2) + 261*s2*Power(t1,3)*Power(t2,2) + 
         56*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         18*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         Power(t1,4)*Power(t2,2) - s2*Power(t1,4)*Power(t2,2) - 
         27*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         10*Power(t1,5)*Power(t2,2) + 15*s2*Power(t1,5)*Power(t2,2) - 
         3*Power(t1,6)*Power(t2,2) - 8*Power(t2,3) - 22*s2*Power(t2,3) - 
         79*Power(s2,2)*Power(t2,3) - 98*Power(s2,3)*Power(t2,3) - 
         36*Power(s2,4)*Power(t2,3) - 5*Power(s2,5)*Power(t2,3) - 
         46*t1*Power(t2,3) + 26*s2*t1*Power(t2,3) + 
         191*Power(s2,2)*t1*Power(t2,3) + 
         106*Power(s2,3)*t1*Power(t2,3) + 26*Power(s2,4)*t1*Power(t2,3) + 
         85*Power(t1,2)*Power(t2,3) - 48*s2*Power(t1,2)*Power(t2,3) - 
         100*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         58*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         33*Power(t1,3)*Power(t2,3) + 36*s2*Power(t1,3)*Power(t2,3) + 
         66*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         6*Power(t1,4)*Power(t2,3) - 37*s2*Power(t1,4)*Power(t2,3) + 
         8*Power(t1,5)*Power(t2,3) + 20*Power(t2,4) + 12*s2*Power(t2,4) - 
         11*Power(s2,2)*Power(t2,4) - 5*Power(s2,3)*Power(t2,4) - 
         3*Power(s2,4)*Power(t2,4) - 20*t1*Power(t2,4) + 
         6*s2*t1*Power(t2,4) + 16*Power(s2,2)*t1*Power(t2,4) + 
         20*Power(s2,3)*t1*Power(t2,4) - 7*Power(t1,2)*Power(t2,4) - 
         25*s2*Power(t1,2)*Power(t2,4) - 
         38*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         14*Power(t1,3)*Power(t2,4) + 28*s2*Power(t1,3)*Power(t2,4) - 
         7*Power(t1,4)*Power(t2,4) - 4*Power(t2,5) - 2*s2*Power(t2,5) - 
         2*Power(s2,2)*Power(t2,5) - 4*Power(s2,3)*Power(t2,5) + 
         6*t1*Power(t2,5) + 6*s2*t1*Power(t2,5) + 
         10*Power(s2,2)*t1*Power(t2,5) - 4*Power(t1,2)*Power(t2,5) - 
         8*s2*Power(t1,2)*Power(t2,5) + 2*Power(t1,3)*Power(t2,5) + 
         Power(s,6)*(Power(s1,2) - 3*Power(s2,2) + 5*s2*(2 + t1 - t2) + 
            s1*(2 - 4*s2 - t1 + t2) - 
            2*(3 + 2*t1 + Power(t1,2) - 2*t2 - 2*t1*t2 + Power(t2,2))) + 
         Power(s1,4)*(3*Power(s2,4) + 
            2*(-1 + t1)*(2*Power(t1,3) + Power(t1,2)*(-5 + t2) - 
               t1*(-3 + t2) + 10*(-1 + t2)) + 
            Power(s2,3)*(8 - 13*t1 - 4*t2) - 
            s2*(15*Power(t1,3) + 2*(-9 + t2) + 
               8*Power(t1,2)*(-6 + t2) + 10*t1*(3 + t2)) + 
            Power(s2,2)*(2 + 21*Power(t1,2) + 14*t2 + 2*t1*(-21 + 5*t2))) \
+ Power(s,5)*(12 - 2*Power(s1,3) + 15*Power(s2,3) + t1 - 
            21*Power(t1,2) - 5*Power(t1,3) + 
            Power(s1,2)*(-7 + 9*t1 - 2*t2) - 37*t2 + 14*t1*t2 + 
            4*Power(t1,2)*t2 + 7*Power(t2,2) + 7*t1*Power(t2,2) - 
            6*Power(t2,3) + Power(s2,2)*(-37 - 33*t1 + 12*t2) + 
            s2*(-1 + 46*t1 + 23*Power(t1,2) + 18*t2 - 14*t1*t2 - 
               9*Power(t2,2)) + 
            s1*(25 + 35*Power(s2,2) + 8*t1 + 6*Power(t1,2) - 16*t1*t2 + 
               10*Power(t2,2) + s2*(-40 - 31*t1 + 9*t2))) + 
         Power(s1,3)*(-3*Power(s2,5) + Power(s2,4)*(7 + 15*t1 - 6*t2) + 
            Power(s2,3)*(71 - 32*Power(t1,2) - 19*t2 + 16*Power(t2,2) + 
               t1*(-5 + 19*t2)) + 
            (-1 + t1)*(-8 + 5*Power(t1,4) + 80*t2 - 40*Power(t2,2) - 
               Power(t1,3)*(21 + 5*t2) + 
               Power(t1,2)*(27 + 23*t2 - 8*Power(t2,2)) + 
               2*t1*(-22 - 9*t2 + 4*Power(t2,2))) + 
            Power(s2,2)*(83 + 36*Power(t1,3) + 5*t2 - 16*Power(t2,2) - 
               Power(t1,2)*(49 + 25*t2) - 
               2*t1*(68 - 55*t2 + 20*Power(t2,2))) + 
            s2*(32 - 21*Power(t1,4) - 66*t2 + 8*Power(t2,2) + 
               Power(t1,3)*(73 + 17*t2) + t1*(-44 + 84*t2) + 
               Power(t1,2)*(5 - 119*t2 + 32*Power(t2,2)))) + 
         Power(s,4)*(36 + Power(s1,4) - 30*Power(s2,4) + 7*t1 + 
            44*Power(t1,2) - 54*Power(t1,3) - 3*Power(t1,4) + 
            Power(s1,3)*(10 + 10*s2 - 15*t1 - t2) + 53*t2 - 83*t1*t2 + 
            46*Power(t1,2)*t2 - 14*Power(t1,3)*t2 - 51*Power(t2,2) - 
            10*t1*Power(t2,2) + 35*Power(t1,2)*Power(t2,2) + 
            18*Power(t2,3) - 16*t1*Power(t2,3) - 2*Power(t2,4) + 
            Power(s2,3)*(45 + 81*t1 + 11*t2) - 
            Power(s1,2)*(9 + 28*Power(s2,2) - 9*Power(t1,2) + 
               t1*(24 - 14*t2) + 2*t2 + 3*Power(t2,2) + 
               s2*(-46 - 8*t1 + 7*t2)) - 
            2*Power(s2,2)*(-52 + 38*Power(t1,2) + 35*t2 - 
               28*Power(t2,2) + t1*(67 + 20*t2)) + 
            s1*(-89 - 109*Power(s2,3) + 22*Power(t1,3) + 
               Power(t1,2)*(2 - 44*t2) + 
               Power(s2,2)*(91 + 191*t1 - 28*t2) + 60*t2 - 
               26*Power(t2,2) + 5*Power(t2,3) - 
               s2*(-25 + 109*Power(t1,2) + t1*(85 - 75*t2) + 105*t2 + 
                  16*Power(t2,2)) + t1*(65 + 34*t2 + 17*Power(t2,2))) + 
            s2*(-103 + 28*Power(t1,3) + 89*t2 + 59*Power(t2,2) + 
               13*Power(t2,3) + Power(t1,2)*(149 + 42*t2) - 
               t1*(94 + 38*t2 + 83*Power(t2,2)))) + 
         Power(s1,2)*(-15*Power(s2,6) + Power(s2,5)*(-35 + 68*t1 + t2) - 
            2*Power(s2,4)*(32 + 61*Power(t1,2) + 2*t1*(-35 + t2) + 
               25*t2) + Power(s2,3)*
             (107*Power(t1,3) + 2*Power(t1,2)*(-97 + 3*t2) + 
               t1*(314 + 116*t2 + 21*Power(t2,2)) - 
               3*(47 + 80*t2 - 3*Power(t2,2) + 8*Power(t2,3))) + 
            (-1 + t1)*(Power(t1,5) - Power(t1,4)*(19 + 2*t2) + 
               Power(t1,3)*(-7 + 44*t2 - 9*Power(t2,2)) + 
               8*(10 + 3*t2 - 15*Power(t2,2) + 5*Power(t2,3)) - 
               2*t1*(110 - 71*t2 - 9*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(122 - 85*t2 - 9*Power(t2,2) + 
                  12*Power(t2,3))) + 
            s2*(5*Power(t1,5) + 5*Power(t1,4)*(5 + t2) + 
               Power(t1,3)*(190 - 110*t2 + 39*Power(t2,2)) - 
               2*(120 + 43*t2 - 45*Power(t2,2) + 6*Power(t2,3)) + 
               2*t1*(326 + 57*t2 - 36*Power(t2,2) + 10*Power(t2,3)) - 
               Power(t1,2)*(583 + 58*t2 - 69*Power(t2,2) + 
                  48*Power(t2,3))) - 
            Power(s2,2)*(258 + 44*Power(t1,4) + 
               6*Power(t1,3)*(-14 + t2) + 245*t2 + 27*Power(t2,2) - 
               4*Power(t2,3) + 
               Power(t1,2)*(456 + 2*t2 + 51*Power(t2,2)) - 
               t1*(567 + 463*t2 - 78*Power(t2,2) + 60*Power(t2,3)))) + 
         s1*(-9*Power(s2,7) + Power(s2,6)*(-19 + 39*t1 + 12*t2) + 
            Power(s2,5)*(40 - 64*Power(t1,2) + t1*(45 - 59*t2) + 
               17*t2 + 7*Power(t2,2)) + 
            Power(s2,4)*(118 + 46*Power(t1,3) + 101*t2 + 
               79*Power(t2,2) + 6*Power(t2,3) + 
               Power(t1,2)*(11 + 119*t2) - 
               t1*(263 + 124*t2 + 37*Power(t2,2))) + 
            Power(s2,3)*(201 - 9*Power(t1,4) + 333*t2 + 
               267*Power(t2,2) + 7*Power(t2,3) + 16*Power(t2,4) - 
               Power(t1,3)*(107 + 125*t2) + 
               Power(t1,2)*(657 + 241*t2 + 84*Power(t2,2)) - 
               t1*(637 + 629*t2 + 217*Power(t2,2) + 47*Power(t2,3))) + 
            (-1 + t1)*(2*Power(t1,5)*(-3 + t2) + 
               Power(t1,4)*(-59 + 32*t2 - 11*Power(t2,2)) + 
               Power(t1,3)*(295 + 19*t2 - 25*Power(t2,2) + 
                  17*Power(t2,3)) + 
               2*t1*(129 + 229*t2 - 76*Power(t2,2) - 3*Power(t2,3) + 
                  4*Power(t2,4)) - 
               4*(12 + 40*t2 + 6*Power(t2,2) - 20*Power(t2,3) + 
                  5*Power(t2,4)) - 
               Power(t1,2)*(463 + 265*t2 - 89*Power(t2,2) + 
                  11*Power(t2,3) + 8*Power(t2,4))) + 
            Power(s2,2)*(287 - 5*Power(t1,5) + 577*t2 + 
               241*Power(t2,2) + 31*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,4)*(90 + 71*t2) - 
               2*Power(t1,3)*(373 + 70*t2 + 48*Power(t2,2)) + 
               Power(t1,2)*(1324 + 1000*t2 + 151*Power(t2,2) + 
                  93*Power(t2,3)) - 
               2*t1*(513 + 648*t2 + 259*Power(t2,2) + 3*Power(t2,3) + 
                  20*Power(t2,4))) + 
            s2*(202 + 2*Power(t1,6) + 498*t2 + 76*Power(t2,2) - 
               54*Power(t2,3) + 8*Power(t2,4) - 
               2*Power(t1,5)*(7 + 10*t2) + 
               Power(t1,4)*(365 - 24*t2 + 53*Power(t2,2)) + 
               Power(t1,3)*(-1167 - 451*t2 + Power(t2,2) - 
                  69*Power(t2,3)) - 
               4*t1*(238 + 351*t2 + 24*Power(t2,2) - 3*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(t1,2)*(1591 + 1303*t2 + 101*Power(t2,2) + 
                  27*Power(t2,3) + 32*Power(t2,4)))) + 
         Power(s,3)*(-120 + 30*Power(s2,5) + 69*t1 + 155*Power(t1,2) + 
            16*Power(t1,3) - 75*Power(t1,4) + Power(t1,5) + 75*t2 - 
            258*t1*t2 + 99*Power(t1,2)*t2 + 88*Power(t1,3)*t2 - 
            26*Power(t1,4)*t2 + 223*Power(t2,2) - 236*t1*Power(t2,2) - 
            68*Power(t1,2)*Power(t2,2) + 40*Power(t1,3)*Power(t2,2) + 
            Power(t2,3) + 52*t1*Power(t2,3) - 
            4*Power(t1,2)*Power(t2,3) + 3*Power(t2,4) - 
            13*t1*Power(t2,4) + 2*Power(t2,5) + 
            Power(s1,4)*(-9 - 6*s2 + 7*t1 + 2*t2) - 
            2*Power(s2,4)*(4 + 47*t1 + 32*t2) - 
            Power(s1,3)*(43 + 15*Power(s2,2) + 28*Power(t1,2) + 
               s2*(32 - 45*t1 - 13*t2) + 8*t1*(-3 + t2) - 24*t2 + 
               8*Power(t2,2)) + 
            Power(s2,3)*(-271 + 105*Power(t1,2) + 38*t2 - 
               97*Power(t2,2) + 42*t1*(4 + 5*t2)) + 
            Power(s1,2)*(151 + 89*Power(s2,3) - 16*Power(t1,3) + 
               87*t2 - 18*Power(t2,2) + 12*Power(t2,3) + 
               4*Power(t1,2)*(-5 + 13*t2) + 
               Power(s2,2)*(-63 - 155*t1 + 30*t2) + 
               s2*(-33 + 83*Power(t1,2) + t1*(96 - 76*t2) + 46*t2 - 
                  3*Power(t2,2)) - 2*t1*(76 - 2*t2 + 9*Power(t2,2))) + 
            Power(s2,2)*(129 - 47*Power(t1,3) - 100*t2 - 
               239*Power(t2,2) - Power(t1,2)*(397 + 254*t2) + 
               t1*(535 + 190*t2 + 211*Power(t2,2))) + 
            s2*(159 + 5*Power(t1,4) - 68*t2 + 52*Power(t2,2) - 
               18*Power(t2,3) + 5*Power(t2,4) + 
               2*Power(t1,3)*(156 + 67*t2) - 
               2*Power(t1,2)*(152 + 151*t2 + 79*Power(t2,2)) + 
               2*t1*(-184 + 121*t2 + 124*Power(t2,2) + 7*Power(t2,3))) + 
            s1*(45 + 166*Power(s2,4) + 20*Power(t1,4) - 
               8*Power(s2,3)*(4 + 52*t1 - t2) - 374*t2 - 
               45*Power(t2,2) - 8*Power(t2,4) - 
               8*Power(t1,3)*(1 + 3*t2) + 
               Power(t1,2)*(-93 + 88*t2 - 20*Power(t2,2)) + 
               Power(s2,2)*(-193 + 373*Power(t1,2) + t1*(70 - 56*t2) + 
                  302*t2 - 15*Power(t2,2)) + 
               t1*(42 + 388*t2 - 80*Power(t2,2) + 32*Power(t2,3)) + 
               s2*(56 - 143*Power(t1,3) - 19*t2 + 4*Power(t2,2) - 
                  9*Power(t2,3) + 5*Power(t1,2)*(-4 + 15*t2) + 
                  t1*(129 - 344*t2 + 17*Power(t2,2))))) + 
         Power(s,2)*(114 - 15*Power(s2,6) - 131*t1 - 432*Power(t1,2) + 
            609*Power(t1,3) - 95*Power(t1,4) - 56*Power(t1,5) + 
            Power(t1,6) - 229*t2 + 1039*t1*t2 - 1263*Power(t1,2)*t2 + 
            343*Power(t1,3)*t2 + 84*Power(t1,4)*t2 - 14*Power(t1,5)*t2 - 
            391*Power(t2,2) + 813*t1*Power(t2,2) - 
            282*Power(t1,2)*Power(t2,2) - 88*Power(t1,3)*Power(t2,2) + 
            10*Power(t1,4)*Power(t2,2) - 39*Power(t2,3) - 
            75*t1*Power(t2,3) + 44*Power(t1,2)*Power(t2,3) + 
            24*Power(t1,3)*Power(t2,3) + 19*Power(t2,4) + 
            20*t1*Power(t2,4) - 27*Power(t1,2)*Power(t2,4) - 
            4*Power(t2,5) + 6*t1*Power(t2,5) + 
            Power(s2,5)*(-26 + 51*t1 + 81*t2) + 
            Power(s1,4)*(34 + 12*Power(s2,2) + 15*Power(t1,2) + 
               s2*(18 - 27*t1 - 8*t2) - 4*t2 + t1*(-32 + 6*t2)) + 
            Power(s2,4)*(281 - 59*Power(t1,2) + 64*t2 + 
               75*Power(t2,2) - 2*t1*(46 + 155*t2)) + 
            Power(s2,3)*(63 + 20*Power(t1,3) + 84*t2 + 
               291*Power(t2,2) - 20*Power(t2,3) + 
               Power(t1,2)*(481 + 462*t2) - 
               2*t1*(470 + 212*t2 + 105*Power(t2,2))) + 
            Power(s2,2)*(-447 + 9*Power(t1,4) - 6*t2 + 38*Power(t2,2) - 
               56*Power(t2,3) - 7*Power(t2,4) - 
               2*Power(t1,3)*(315 + 166*t2) + 
               Power(t1,2)*(979 + 680*t2 + 213*Power(t2,2)) + 
               t1*(436 - 336*t2 - 478*Power(t2,2) + 42*Power(t2,3))) - 
            s2*(67 + 7*Power(t1,5) + 424*t2 + 522*Power(t2,2) + 
               52*Power(t2,3) + 15*Power(t2,4) + 8*Power(t2,5) - 
               Power(t1,4)*(323 + 113*t2) + 
               Power(t1,3)*(225 + 396*t2 + 88*Power(t2,2)) + 
               2*Power(t1,2)*
                (539 + 63*t2 - 129*Power(t2,2) + 24*Power(t2,3)) - 
               t1*(933 + 910*t2 + 393*Power(t2,2) + 20*Power(t2,3) + 
                  38*Power(t2,4))) + 
            Power(s1,3)*(71 + 5*Power(s2,3) - 14*Power(t1,3) + 
               Power(s2,2)*(45 - 30*t1 - 29*t2) - 121*t2 + 
               16*Power(t2,2) - 2*Power(t1,2)*(4 + 9*t2) + 
               t1*(-6 + 76*t2 - 24*Power(t2,2)) + 
               s2*(105 + 39*Power(t1,2) - 39*t2 + 32*Power(t2,2) + 
                  t1*(-71 + 43*t2))) - 
            Power(s1,2)*(332 + 114*Power(s2,4) + 27*Power(t1,4) + 
               Power(t1,3)*(16 - 52*t2) + 181*t2 - 159*Power(t2,2) + 
               24*Power(t2,3) + Power(s2,3)*(19 - 318*t1 + 30*t2) + 
               3*Power(t1,2)*(79 - 20*t2 + 12*Power(t2,2)) + 
               t1*(-661 + 63*t2 + 36*Power(t2,2) - 36*Power(t2,3)) + 
               Power(s2,2)*(-55 + 324*Power(t1,2) + 146*t2 - 
                  15*Power(t2,2) - 2*t1*(14 + 51*t2)) + 
               s2*(331 - 147*Power(t1,3) + 262*t2 - 9*Power(t2,2) + 
                  48*Power(t2,3) + 21*Power(t1,2)*(1 + 6*t2) - 
                  t1*(244 + 162*t2 + 33*Power(t2,2)))) + 
            s1*(115 - 134*Power(s2,5) + 3*Power(t1,5) + 
               Power(s2,4)*(427*t1 + 39*(-2 + t2)) + 723*t2 + 
               149*Power(t2,2) - 91*Power(t2,3) + 16*Power(t2,4) + 
               Power(t1,4)*(-6 + 17*t2) - 
               2*Power(t1,3)*(161 - 52*t2 + 31*Power(t2,2)) + 
               Power(t1,2)*(863 + 519*t2 - 96*Power(t2,2) + 
                  66*Power(t2,3)) - 
               2*t1*(310 + 737*t2 - 72*Power(t2,2) + 14*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,3)*(245 - 507*Power(t1,2) - 272*t2 + 
                  45*Power(t2,2) - 12*t1*(-11 + 9*t2)) + 
               Power(s2,2)*(197 + 272*Power(t1,3) - 93*t2 + 
                  157*Power(t2,2) + 9*Power(t2,3) + 
                  37*Power(t1,2)*(-1 + 3*t2) - 
                  2*t1*(263 - 225*t2 + 57*Power(t2,2))) + 
               s2*(203 - 61*Power(t1,4) + 853*t2 + 209*Power(t2,2) + 
                  27*Power(t2,3) + 32*Power(t2,4) - 
                  Power(t1,3)*(19 + 59*t2) + 
                  Power(t1,2)*(608 - 237*t2 + 135*Power(t2,2)) - 
                  t1*(755 + 637*t2 + 111*Power(t2,2) + 87*Power(t2,3))))) \
+ s*(-36 + 4*Power(s1,5)*(-1 + s2) + 3*Power(s2,7) + 46*t1 + 
            404*Power(t1,2) - 969*Power(t1,3) + 677*Power(t1,4) - 
            105*Power(t1,5) - 22*Power(t1,6) + 
            Power(s2,6)*(21 - 9*t1 - 44*t2) + 182*t2 - 1058*t1*t2 + 
            1971*Power(t1,2)*t2 - 1368*Power(t1,3)*t2 + 
            256*Power(t1,4)*t2 + 42*Power(t1,5)*t2 - 2*Power(t1,6)*t2 + 
            294*Power(t2,2) - 892*t1*Power(t2,2) + 
            705*Power(t1,2)*Power(t2,2) - 96*Power(t1,3)*Power(t2,2) - 
            47*Power(t1,4)*Power(t2,2) - 7*Power(t1,5)*Power(t2,2) + 
            34*Power(t2,3) + 86*t1*Power(t2,3) - 
            109*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) + 
            26*Power(t1,4)*Power(t2,3) - 40*Power(t2,4) + 
            12*t1*Power(t2,4) + 31*Power(t1,2)*Power(t2,4) - 
            23*Power(t1,3)*Power(t2,4) + 6*Power(t2,5) - 
            8*t1*Power(t2,5) + 6*Power(t1,2)*Power(t2,5) + 
            2*Power(s2,5)*(-62 + 2*Power(t1,2) - 38*t2 - 
               13*Power(t2,2) + t1*(5 + 98*t2)) + 
            Power(s2,4)*(-151 + 14*Power(t1,3) - 33*t2 - 
               136*Power(t2,2) + 18*Power(t2,3) - 
               14*Power(t1,2)*(18 + 25*t2) + 
               4*t1*(156 + 89*t2 + 21*Power(t2,2))) + 
            Power(s2,3)*(303 - 21*Power(t1,4) + 70*t2 - 2*Power(t2,2) + 
               92*Power(t2,3) + 7*Power(t2,4) + 
               4*Power(t1,3)*(124 + 79*t2) - 
               Power(t1,2)*(1019 + 586*t2 + 93*Power(t2,2)) + 
               t1*(36 + 122*t2 + 256*Power(t2,2) - 66*Power(t2,3))) + 
            Power(s2,2)*(358 + 11*Power(t1,5) + 527*t2 + 
               471*Power(t2,2) + 153*Power(t2,3) + 17*Power(t2,4) + 
               10*Power(t2,5) - Power(t1,4)*(403 + 148*t2) + 
               Power(t1,3)*(541 + 442*t2 + 31*Power(t2,2)) + 
               Power(t1,2)*(1028 + 237*t2 - 137*Power(t2,2) + 
                  108*Power(t2,3)) - 
               t1*(1473 + 1026*t2 + 458*Power(t2,2) + 180*Power(t2,3) + 
                  45*Power(t2,4))) + 
            s2*(14 - 2*Power(t1,6) + 628*t2 + 678*Power(t2,2) + 
               78*Power(t2,3) + 6*Power(t2,5) + 
               2*Power(t1,5)*(75 + 16*t2) + 
               Power(t1,4)*(83 - 178*t2 + 11*Power(t2,2)) - 
               2*Power(t1,3)*
                (795 + 299*t2 - 34*Power(t2,2) + 43*Power(t2,3)) + 
               Power(t1,2)*(2127 + 2354*t2 + 606*Power(t2,2) + 
                  74*Power(t2,3) + 61*Power(t2,4)) - 
               2*t1*(387 + 1132*t2 + 663*Power(t2,2) + 48*Power(t2,3) + 
                  20*Power(t2,4) + 8*Power(t2,5))) - 
            Power(s1,4)*(46 + 10*Power(s2,3) - 13*Power(t1,3) + 
               Power(s2,2)*(17 - 33*t1 - 10*t2) + 
               Power(t1,2)*(37 - 6*t2) - 22*t2 + t1*(-50 + 8*t2) + 
               2*s2*(14 + 18*Power(t1,2) + 5*t2 + t1*(-33 + 8*t2))) + 
            Power(s1,3)*(-44 + 5*Power(s2,4) + 6*Power(t1,4) + 178*t2 - 
               48*Power(t2,2) - 16*Power(t1,3)*(3 + t2) + 
               Power(s2,3)*(-30 - 15*t1 + 23*t2) + 
               Power(s2,2)*(-137 + 21*Power(t1,2) + t1*(56 - 54*t2) + 
                  34*t2 - 40*Power(t2,2)) + 
               Power(t1,2)*(85 + 80*t2 - 24*Power(t2,2)) + 
               2*t1*(-20 - 81*t2 + 16*Power(t2,2)) + 
               s2*(-114 - 17*Power(t1,3) + 84*t2 + 
                  Power(t1,2)*(34 + 47*t2) + 
                  2*t1*(59 - 79*t2 + 32*Power(t2,2)))) + 
            Power(s1,2)*(276 + 67*Power(s2,5) - 9*Power(t1,5) + 122*t2 - 
               258*Power(t2,2) + 52*Power(t2,3) + 
               Power(s2,4)*(78 - 248*t1 + 8*t2) + 
               Power(t1,4)*(-33 + 14*t2) - 
               2*Power(t1,3)*(41 - 50*t2 + 15*Power(t2,2)) - 
               2*t1*(397 - 83*t2 - 93*Power(t2,2) + 24*Power(t2,3)) + 
               Power(t1,2)*(599 - 279*t2 - 18*Power(t2,2) + 
                  36*Power(t2,3)) + 
               Power(s2,3)*(51 + 354*Power(t1,2) + 152*t2 - 
                  9*Power(t2,2) - 12*t1*(20 + 3*t2)) + 
               Power(s2,2)*(301 - 241*Power(t1,3) + 427*t2 + 
                  60*Power(t2,3) + Power(t1,2)*(245 + 66*t2) - 
                  4*t1*(98 + 73*t2 + 9*Power(t2,2))) + 
               s2*(558 + 77*Power(t1,4) + 306*t2 - 84*Power(t2,2) + 
                  20*Power(t2,3) - 2*Power(t1,3)*(23 + 26*t2) + 
                  Power(t1,2)*(479 + 6*t2 + 75*Power(t2,2)) - 
                  2*t1*(508 + 166*t2 - 39*Power(t2,2) + 48*Power(t2,3)))) \
+ s1*(55*Power(s2,6) - 2*Power(t1,6) + 
               Power(s2,5)*(76 - 209*t1 - 41*t2) + 
               8*Power(t1,5)*(-1 + 2*t2) + 
               Power(t1,4)*(-242 + 80*t2 - 46*Power(t2,2)) + 
               2*Power(t1,3)*(533 + 89*t2 - 28*Power(t2,2) + 
                  28*Power(t2,3)) - 
               2*(73 + 285*t2 + 56*Power(t2,2) - 83*Power(t2,3) + 
                  14*Power(t2,4)) + 
               2*t1*(406 + 843*t2 - 106*Power(t2,2) - 43*Power(t2,3) + 
                  16*Power(t2,4)) - 
               Power(t1,2)*(1503 + 1304*t2 - 303*Power(t2,2) + 
                  56*Power(t2,3) + 24*Power(t2,4)) + 
               Power(s2,4)*(-142 + 301*Power(t1,2) + 58*t2 - 
                  31*Power(t2,2) + 2*t1*(-85 + 82*t2)) - 
               Power(s2,3)*(278 + 197*Power(t1,3) + 49*t2 + 
                  214*Power(t2,2) + 11*Power(t2,3) + 
                  Power(t1,2)*(-46 + 261*t2) + 
                  t1*(-589 + 16*t2 - 117*Power(t2,2))) + 
               Power(s2,2)*(-421 + 50*Power(t1,4) - 772*t2 - 
                  443*Power(t2,2) - 34*Power(t2,3) - 40*Power(t2,4) + 
                  6*Power(t1,3)*(19 + 35*t2) - 
                  Power(t1,2)*(1066 + 108*t2 + 195*Power(t2,2)) + 
                  2*t1*(619 + 425*t2 + 208*Power(t2,2) + 51*Power(t2,3))) \
+ s2*(2*Power(t1,5) - 2*Power(t1,4)*(29 + 44*t2) + 
                  Power(t1,3)*(877 - 22*t2 + 155*Power(t2,2)) - 
                  Power(t1,2)*
                   (2044 + 1085*t2 + 114*Power(t2,2) + 147*Power(t2,3)) - 
                  2*(221 + 618*t2 + 135*Power(t2,2) - 14*Power(t2,3) + 
                     10*Power(t2,4)) + 
                  2*t1*(851 + 1171*t2 + 155*Power(t2,2) + 
                     27*Power(t2,3) + 32*Power(t2,4))))))*
       R1(1 - s + s2 - t1))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(1 - s + s2 - t1)*
       Power(-s1 + s2 - t1 + t2,3)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) + 
    (16*(-16*Power(s2,3) + 8*Power(s2,4) + 48*Power(s2,2)*t1 - 
         8*Power(s2,3)*t1 - 8*Power(s2,4)*t1 - 48*s2*Power(t1,2) - 
         24*Power(s2,2)*Power(t1,2) + 24*Power(s2,3)*Power(t1,2) + 
         16*Power(t1,3) + 40*s2*Power(t1,3) - 
         24*Power(s2,2)*Power(t1,3) - 16*Power(t1,4) + 8*s2*Power(t1,4) + 
         Power(s1,6)*(-12 - 3*Power(s2,2) + 4*t1 - 3*Power(t1,2) + 
            6*s2*(2 + t1)) - 48*Power(s2,2)*t2 + 32*Power(s2,3)*t2 - 
         12*Power(s2,4)*t2 + 2*Power(s2,5)*t2 + 96*s2*t1*t2 - 
         24*Power(s2,2)*t1*t2 - 8*Power(s2,3)*t1*t2 + 
         8*Power(s2,4)*t1*t2 - 48*Power(t1,2)*t2 - 48*s2*Power(t1,2)*t2 + 
         72*Power(s2,2)*Power(t1,2)*t2 - 36*Power(s2,3)*Power(t1,2)*t2 + 
         40*Power(t1,3)*t2 - 72*s2*Power(t1,3)*t2 + 
         40*Power(s2,2)*Power(t1,3)*t2 + 20*Power(t1,4)*t2 - 
         14*s2*Power(t1,4)*t2 - 52*s2*Power(t2,2) + 
         22*Power(s2,2)*Power(t2,2) - 22*Power(s2,3)*Power(t2,2) + 
         4*Power(s2,4)*Power(t2,2) + 52*t1*Power(t2,2) + 
         40*s2*t1*Power(t2,2) - 36*Power(s2,2)*t1*Power(t2,2) + 
         44*Power(s2,3)*t1*Power(t2,2) - 8*Power(s2,4)*t1*Power(t2,2) - 
         62*Power(t1,2)*Power(t2,2) + 106*s2*Power(t1,2)*Power(t2,2) - 
         108*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         24*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         48*Power(t1,3)*Power(t2,2) + 68*s2*Power(t1,3)*Power(t2,2) - 
         24*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         8*Power(t1,4)*Power(t2,2) + 8*s2*Power(t1,4)*Power(t2,2) - 
         32*Power(t2,3) - 4*s2*Power(t2,3) + 49*Power(s2,2)*Power(t2,3) - 
         20*Power(s2,3)*Power(t2,3) + 5*Power(s2,4)*Power(t2,3) - 
         2*Power(s2,5)*Power(t2,3) + 52*t1*Power(t2,3) - 
         186*s2*t1*Power(t2,3) + 93*Power(s2,2)*t1*Power(t2,3) - 
         44*Power(s2,3)*t1*Power(t2,3) + 8*Power(s2,4)*t1*Power(t2,3) + 
         121*Power(t1,2)*Power(t2,3) - 62*s2*Power(t1,2)*Power(t2,3) + 
         78*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         12*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         11*Power(t1,3)*Power(t2,3) - 44*s2*Power(t1,3)*Power(t2,3) + 
         8*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         5*Power(t1,4)*Power(t2,3) - 2*s2*Power(t1,4)*Power(t2,3) + 
         16*Power(t2,4) + 85*s2*Power(t2,4) - 
         62*Power(s2,2)*Power(t2,4) + 41*Power(s2,3)*Power(t2,4) - 
         3*Power(s2,4)*Power(t2,4) - 149*t1*Power(t2,4) + 
         91*s2*t1*Power(t2,4) - 119*Power(s2,2)*t1*Power(t2,4) + 
         8*Power(s2,3)*t1*Power(t2,4) + 3*Power(t1,2)*Power(t2,4) + 
         83*s2*Power(t1,2)*Power(t2,4) - 
         6*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         5*Power(t1,3)*Power(t2,4) + Power(t1,4)*Power(t2,4) + 
         32*Power(t2,5) - 34*s2*Power(t2,5) + 
         61*Power(s2,2)*Power(t2,5) - 5*Power(s2,3)*Power(t2,5) + 
         34*t1*Power(t2,5) - 80*s2*t1*Power(t2,5) + 
         8*Power(s2,2)*t1*Power(t2,5) + 3*Power(t1,2)*Power(t2,5) - 
         s2*Power(t1,2)*Power(t2,5) - 2*Power(t1,3)*Power(t2,5) - 
         12*Power(t2,6) + 19*s2*Power(t2,6) - 4*Power(s2,2)*Power(t2,6) - 
         3*t1*Power(t2,6) + 3*s2*t1*Power(t2,6) + 
         Power(t1,2)*Power(t2,6) + 
         Power(s1,5)*(5*Power(s2,3) - 3*Power(t1,3) + 
            s2*(21 + 11*Power(t1,2) + t1*(47 - 33*t2) - 79*t2) + 
            8*(-4 + 9*t2) + Power(t1,2)*(5 + 14*t2) - t1*(21 + 17*t2) + 
            Power(s2,2)*(-36 - 13*t1 + 19*t2)) + 
         Power(s,6)*(-Power(s1,2) + 3*Power(s2,2) - 5*s2*(2 + t1 - t2) + 
            s1*(-2 + 4*s2 + t1 - t2) + 
            2*(3 + 2*t1 + Power(t1,2) - 2*t2 - 2*t1*t2 + Power(t2,2))) + 
         Power(s,5)*(-24 + 3*Power(s1,3) - 4*Power(s2,3) - 17*t1 + 
            11*Power(t1,2) + Power(t1,3) + 53*t2 + 6*t1*t2 + 
            8*Power(t1,2)*t2 - 17*Power(t2,2) - 19*t1*Power(t2,2) + 
            10*Power(t2,3) + Power(s1,2)*(11 - 7*s2 - 11*t1 + 4*t2) + 
            3*Power(s2,2)*(-1 + 3*t1 + 4*t2) - 
            s2*(-41 + 8*t1 + 6*Power(t1,2) + 56*t2 + 20*t1*t2 - 
               26*Power(t2,2)) - 
            s1*(29 + 28*Power(s2,2) + 14*t1 + 13*Power(t1,2) - 6*t2 - 
               30*t1*t2 + 17*Power(t2,2) + s2*(-40 - 41*t1 + 19*t2))) + 
         Power(s1,4)*(4*Power(s2,4) + Power(s2,3)*(31 - 12*t1 - 25*t2) + 
            2*Power(t1,3)*(3 + 5*t2) - Power(t1,2)*t2*(17 + 25*t2) + 
            4*(4 + 40*t2 - 45*Power(t2,2)) + 
            t1*(-129 + 118*t2 + 25*Power(t2,2)) + 
            Power(s2,2)*(-41 + 12*Power(t1,2) + 205*t2 - 
               50*Power(t2,2) + t1*(-88 + 60*t2)) + 
            s2*(65 - 4*Power(t1,3) + Power(t1,2)*(51 - 45*t2) - 118*t2 + 
               215*Power(t2,2) + t1*(73 - 268*t2 + 75*Power(t2,2)))) - 
         Power(s1,3)*(Power(t1,4)*(1 + t2) + Power(s2,4)*(-7 + 9*t2) + 
            Power(t1,3)*(-23 + 13*t2 + 10*Power(t2,2)) + 
            Power(t1,2)*(131 + 3*t2 - 18*Power(t2,2) - 20*Power(t2,3)) + 
            16*(-2 + 4*t2 + 20*Power(t2,2) - 15*Power(t2,3)) + 
            2*t1*(20 - 268*t2 + 131*Power(t2,2) + 5*Power(t2,3)) - 
            2*Power(s2,3)*(7 - 67*t2 + 25*Power(t2,2) + 
               2*t1*(1 + 7*t2)) + 
            Power(s2,2)*(47 - 185*t2 + 460*Power(t2,2) - 
               70*Power(t2,3) + 30*Power(t1,2)*(1 + t2) + 
               t1*(69 - 383*t2 + 110*Power(t2,2))) - 
            2*s2*(-4 - 140*t2 + 131*Power(t2,2) - 155*Power(t2,3) + 
               2*Power(t1,3)*(5 + 3*t2) + 
               Power(t1,2)*(16 - 118*t2 + 35*Power(t2,2)) + 
               t1*(97 - 155*t2 + 301*Power(t2,2) - 45*Power(t2,3)))) + 
         Power(s1,2)*(-2*Power(s2,5)*(1 + t2) + 
            3*Power(t1,3)*(-16 - 19*t2 + Power(t2,2)) + 
            Power(t1,4)*(-4 + 7*t2 + 3*Power(t2,2)) + 
            Power(s2,4)*(8 + (-9 + 8*t1)*t2 + 3*Power(t2,2)) + 
            4*t2*(-24 + 24*t2 + 80*Power(t2,2) - 45*Power(t2,3)) + 
            t1*(52 + 132*t2 - 834*Power(t2,2) + 288*Power(t2,3) - 
               10*Power(t2,4)) + 
            Power(t1,2)*(-62 + 383*t2 + 9*Power(t2,2) - 2*Power(t2,3) - 
               5*Power(t2,4)) - 
            2*Power(s2,3)*(11 + 6*Power(t1,2)*(-1 + t2) + 24*t2 - 
               108*Power(t2,2) + 25*Power(t2,3) + 
               2*t1*(-7 + 13*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(22 + 8*Power(t1,3)*(-2 + t2) + 143*t2 - 
               309*Power(t2,2) + 510*Power(t2,3) - 55*Power(t2,4) + 
               6*Power(t1,2)*(-14 + 23*t2 + 3*Power(t2,2)) + 
               t1*(-36 + 231*t2 - 621*Power(t2,2) + 100*Power(t2,3))) - 
            2*s2*(26 + Power(t1,4)*(-3 + t2) - 6*t2 - 225*Power(t2,2) + 
               144*Power(t2,3) - 125*Power(t2,4) + 
               Power(t1,3)*(-26 + 42*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(-53 + 63*t2 - 201*Power(t2,2) + 
                  25*Power(t2,3)) + 
               t1*(-20 + 287*t2 - 246*Power(t2,2) + 334*Power(t2,3) - 
                  30*Power(t2,4)))) + 
         s1*(2*Power(s2,5)*(-1 + t2 + 2*Power(t2,2)) - 
            Power(t1,4)*(20 - 12*t2 + 11*Power(t2,2) + 3*Power(t2,3)) + 
            8*Power(t2,2)*(12 - 8*t2 - 20*Power(t2,2) + 9*Power(t2,3)) + 
            Power(t1,3)*(-40 + 96*t2 + 45*Power(t2,2) + 9*Power(t2,3) + 
               5*Power(t2,4)) + 
            t1*t2*(-104 - 144*t2 + 576*Power(t2,2) - 157*Power(t2,3) + 
               11*Power(t2,4)) - 
            Power(t1,2)*(-48 - 124*t2 + 373*Power(t2,2) + 
               9*Power(t2,3) + 7*Power(t2,4) + 2*Power(t2,5)) + 
            Power(s2,4)*(12 - 12*t2 - 3*Power(t2,2) + 5*Power(t2,3) - 
               8*t1*(1 - t2 + 2*Power(t2,2))) + 
            Power(s2,3)*(-32 + 44*t2 + 54*Power(t2,2) - 
               154*Power(t2,3) + 25*Power(t2,4) + 
               12*Power(t1,2)*(3 - 3*t2 + 2*Power(t2,2)) + 
               t1*(8 - 72*t2 + 92*Power(t2,2) - 12*Power(t2,3))) + 
            Power(s2,2)*(48 - 44*t2 - 145*Power(t2,2) + 
               227*Power(t2,3) - 280*Power(t2,4) + 23*Power(t2,5) - 
               8*Power(t1,3)*(5 - 5*t2 + 2*Power(t2,2)) + 
               6*Power(t1,2)*
                (-12 + 32*t2 - 31*Power(t2,2) + Power(t2,3)) + 
               t1*(24 + 72*t2 - 255*Power(t2,2) + 445*Power(t2,3) - 
                  45*Power(t2,4))) + 
            s2*(104*t2 - 320*Power(t2,3) + 157*Power(t2,4) - 
               107*Power(t2,5) + 
               2*Power(t1,4)*(7 - 7*t2 + 2*Power(t2,2)) + 
               4*Power(t1,3)*
                (18 - 30*t2 + 27*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(48 - 212*t2 + 156*Power(t2,2) - 
                  300*Power(t2,3) + 15*Power(t2,4)) - 
               t1*(96 + 80*t2 - 566*Power(t2,2) + 346*Power(t2,3) - 
                  367*Power(t2,4) + 21*Power(t2,5)))) + 
         Power(s,4)*(30 - 3*Power(s1,4) - Power(s2,4) + 25*t1 - 
            68*Power(t1,2) + 10*Power(t1,3) - Power(t1,4) + 
            Power(s2,3)*(18 + 4*t1 - 29*t2) - 145*t2 + 43*t1*t2 + 
            32*Power(t1,2)*t2 + 10*Power(t1,3)*t2 + 115*Power(t2,2) - 
            14*t1*Power(t2,2) + Power(t1,2)*Power(t2,2) - 
            28*Power(t2,3) - 28*t1*Power(t2,3) + 18*Power(t2,4) - 
            Power(s1,3)*(23 + 4*s2 - 30*t1 + 9*t2) - 
            Power(s2,2)*(31 + 6*Power(t1,2) + t1*(34 - 68*t2) - 18*t2 + 
               2*Power(t2,2)) + 
            Power(s1,2)*(34 + 61*Power(s2,2) + 23*Power(t1,2) + 
               t1*(24 - 88*t2) + 18*t2 + 45*Power(t2,2) + 
               s2*(-71 - 84*t1 + 52*t2)) + 
            s2*(-55 + 4*Power(t1,3) + Power(t1,2)*(6 - 49*t2) + 
               114*t2 - 118*Power(t2,2) + 44*Power(t2,3) + 
               t1*(99 - 58*t2 + Power(t2,2))) + 
            s1*(115 + 37*Power(s2,3) - 11*Power(t1,3) + 
               Power(s2,2)*(16 - 85*t1 - 59*t2) - 149*t2 + 
               33*Power(t2,2) - 51*Power(t2,3) - 
               3*Power(t1,2)*(11 + 8*t2) + 
               2*t1*(-2 - 5*t2 + 43*Power(t2,2)) + 
               s2*(-123 + 59*Power(t1,2) + 189*t2 - 92*Power(t2,2) + 
                  t1*(25 + 83*t2)))) + 
         Power(s,3)*(-12 + Power(s1,5) + 2*Power(s2,5) - 16*t1 + 
            93*Power(t1,2) - 29*Power(t1,3) + 3*Power(t1,4) + 136*t2 - 
            82*t1*t2 - 195*Power(t1,2)*t2 + 33*Power(t1,3)*t2 - 
            2*Power(t1,4)*t2 - 251*Power(t2,2) + 275*t1*Power(t2,2) + 
            21*Power(t1,2)*Power(t2,2) + 22*Power(t1,3)*Power(t2,2) + 
            69*Power(t2,3) - 33*t1*Power(t2,3) - 
            24*Power(t1,2)*Power(t2,3) - 24*Power(t2,4) - 
            10*t1*Power(t2,4) + 14*Power(t2,5) + 
            Power(s1,4)*(25 + 16*s2 - 34*t1 + 10*t2) + 
            Power(s2,4)*(3 - 8*t1 + 10*t2) + 
            Power(s2,3)*(-22 + 12*Power(t1,2) + 87*t2 - 
               44*Power(t2,2) - 4*t1*(3 + 7*t2)) - 
            Power(s1,3)*(-30 + 50*Power(s2,2) + 8*Power(t1,2) + 51*t2 + 
               50*Power(t2,2) - 8*t1*(-3 + 14*t2) + 
               s2*(-51 - 58*t1 + 78*t2)) + 
            Power(s2,2)*(57 - 8*Power(t1,3) - 161*t2 + 
               121*Power(t2,2) - 36*Power(t2,3) + 
               6*Power(t1,2)*(3 + 4*t2) + 
               t1*(31 - 173*t2 + 110*Power(t2,2))) + 
            s2*(28 + 2*Power(t1,4) - 74*t2 + 50*Power(t2,2) - 
               107*Power(t2,3) + 30*Power(t2,4) - 
               4*Power(t1,3)*(3 + t2) + 
               Power(t1,2)*(20 + 53*t2 - 88*Power(t2,2)) + 
               6*t1*(-25 + 62*t2 - 29*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s1,2)*(-171 - 80*Power(s2,3) + 30*Power(t1,3) - 
               8*Power(t1,2)*(-5 + t2) + 9*t2 + 3*Power(t2,2) + 
               80*Power(t2,3) + 2*Power(s2,2)*(13 + 95*t1 + 32*t2) + 
               t1*(149 + 15*t2 - 132*Power(t2,2)) - 
               s2*(-92 + 140*Power(t1,2) + 209*t2 - 138*Power(t2,2) + 
                  14*t1*(7 + 4*t2))) + 
            s1*(-124 - 9*Power(s2,4) + 3*Power(t1,4) + 422*t2 - 
               108*Power(t2,2) + 47*Power(t2,3) - 55*Power(t2,4) - 
               4*Power(t1,3)*(7 + 13*t2) + 
               Power(s2,3)*(-105 + 24*t1 + 124*t2) + 
               Power(s2,2)*(140 - 18*Power(t1,2) + t1*(214 - 300*t2) - 
                  147*t2 + 22*Power(t2,2)) + 
               Power(t1,2)*(210 - 61*t2 + 40*Power(t2,2)) + 
               t1*(42 - 424*t2 + 42*Power(t2,2) + 64*Power(t2,3)) + 
               s2*(102 - 142*t2 + 265*Power(t2,2) - 106*Power(t2,3) + 
                  3*Power(t1,2)*(-27 + 76*t2) + 
                  t1*(-366 + 272*t2 - 62*Power(t2,2))))) + 
         Power(s,2)*(4*t1 - 38*Power(t1,2) + 24*Power(t1,3) + 
            4*Power(t1,4) + 2*Power(s2,5)*(-4 + t2) - 40*t2 + 28*t1*t2 + 
            211*Power(t1,2)*t2 - 117*Power(t1,3)*t2 + 
            11*Power(t1,4)*t2 + 190*Power(t2,2) - 332*t1*Power(t2,2) - 
            111*Power(t1,2)*Power(t2,2) + 31*Power(t1,3)*Power(t2,2) - 
            143*Power(t2,3) + 339*t1*Power(t2,3) - 
            7*Power(t1,2)*Power(t2,3) + 16*Power(t1,3)*Power(t2,3) - 
            25*Power(t2,4) - 23*t1*Power(t2,4) - 
            28*Power(t1,2)*Power(t2,4) - 12*Power(t2,5) + 
            8*t1*Power(t2,5) + 4*Power(t2,6) - 
            Power(s1,5)*(15 + 12*s2 - 17*t1 + 4*t2) + 
            Power(s2,4)*(20 - 8*t1*(-3 + t2) - 21*t2 + 20*Power(t2,2)) + 
            Power(s1,4)*(-80 + 11*Power(s2,2) - 14*Power(t1,2) + 
               t1*(16 - 60*t2) + 48*t2 + 20*Power(t2,2) + 
               s2*(7 + 3*t1 + 55*t2)) + 
            Power(s2,3)*(2 + 12*Power(t1,2)*(-2 + t2) + 8*t2 + 
               113*Power(t2,2) - 22*Power(t2,3) - 
               4*t1*(16 - 7*t2 + 15*Power(t2,2))) + 
            Power(s1,3)*(73 + 70*Power(s2,3) - 34*Power(t1,3) - 
               2*Power(s2,2)*(50 + 87*t1 - 2*t2) + 265*t2 - 
               42*Power(t2,2) - 40*Power(t2,3) + 
               10*Power(t1,2)*(-2 + 7*t2) + 
               2*s2*(69*Power(t1,2) + t1*(88 - 37*t2) + t2 - 
                  50*Power(t2,2)) + t1*(-207 - 25*t2 + 70*Power(t2,2))) \
+ Power(s2,2)*(-26 - 8*Power(t1,3)*(-1 + t2) + 163*t2 - 
               191*Power(t2,2) + 205*Power(t2,3) - 37*Power(t2,4) + 
               6*Power(t1,2)*(12 + 3*t2 + 10*Power(t2,2)) + 
               t1*(12 - 85*t2 - 267*Power(t2,2) + 60*Power(t2,3))) + 
            s2*(-4 + 20*t2 + 2*Power(t1,4)*t2 + 78*Power(t2,2) - 
               54*Power(t2,3) - 23*Power(t2,4) + 7*Power(t2,5) - 
               4*Power(t1,3)*(8 + 9*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-38 + 194*t2 + 123*Power(t2,2) - 
                  54*Power(t2,3)) + 
               t1*(64 - 382*t2 + 350*Power(t2,2) - 254*Power(t2,3) + 
                  65*Power(t2,4))) + 
            Power(s1,2)*(166 + 25*Power(s2,4) - 3*Power(t1,4) + 
               Power(s2,3)*(151 - 72*t1 - 162*t2) - 289*t2 - 
               315*Power(t2,2) - 12*Power(t2,3) + 40*Power(t2,4) + 
               4*Power(t1,3)*(8 + 21*t2) - 
               3*Power(t1,2)*(56 - 11*t2 + 42*Power(t2,2)) - 
               t1*(232 - 753*t2 + 21*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,2)*(-152 + 66*Power(t1,2) + 405*t2 - 
                  78*Power(t2,2) + 6*t1*(-57 + 68*t2)) + 
               s2*(2 - 16*Power(t1,3) + Power(t1,2)*(159 - 330*t2) - 
                  54*t2 - 48*Power(t2,2) + 90*Power(t2,3) + 
                  t1*(368 - 606*t2 + 204*Power(t2,2)))) + 
            s1*(40 - 4*Power(s2,5) + Power(s2,4)*(25 + 16*t1 - 45*t2) - 
               356*t2 + 359*Power(t2,2) + 155*Power(t2,3) + 
               33*Power(t2,4) - 20*Power(t2,5) + 
               Power(t1,4)*(-7 + 3*t2) + 
               Power(t1,3)*(113 - 63*t2 - 66*Power(t2,2)) - 
               2*Power(s2,3)*
                (-1 + 12*Power(t1,2) + t1*(22 - 66*t2) + 132*t2 - 
                  57*Power(t2,2)) + 
               Power(t1,2)*(-221 + 279*t2 - 6*Power(t2,2) + 
                  98*Power(t2,3)) + 
               t1*(-16 + 564*t2 - 885*Power(t2,2) + 53*Power(t2,3) - 
                  15*Power(t2,4)) + 
               Power(s2,2)*(-161 + 16*Power(t1,3) + 
                  Power(t1,2)*(6 - 126*t2) + 343*t2 - 510*Power(t2,2) + 
                  100*Power(t2,3) + t1*(61 + 609*t2 - 294*Power(t2,2))) \
- 2*s2*(16 + 2*Power(t1,4) + 40*t2 - 54*Power(t2,2) - 31*Power(t2,3) + 
                  20*Power(t2,4) - 2*Power(t1,3)*(5 + 9*t2) + 
                  Power(t1,2)*(88 + 141*t2 - 123*Power(t2,2)) + 
                  t1*(-195 + 359*t2 - 342*Power(t2,2) + 99*Power(t2,3))))\
) + s*(Power(s1,6)*(4 + 3*s2 - 3*t1) - 24*Power(t1,3) + 12*Power(t1,4) + 
            8*t1*t2 - 4*Power(t1,2)*t2 + 8*Power(t1,3)*t2 - 
            4*Power(t1,4)*t2 - 44*Power(t2,2) + 32*t1*Power(t2,2) + 
            191*Power(t1,2)*Power(t2,2) - 99*Power(t1,3)*Power(t2,2) + 
            13*Power(t1,4)*Power(t2,2) + 116*Power(t2,3) - 
            342*t1*Power(t2,3) + 19*Power(t1,2)*Power(t2,3) + 
            3*Power(t1,3)*Power(t2,3) + 2*Power(t1,4)*Power(t2,3) + 
            11*Power(t2,4) + 158*t1*Power(t2,4) - 
            4*Power(t1,2)*Power(t2,4) + Power(t1,3)*Power(t2,4) - 
            38*Power(t2,5) - 9*t1*Power(t2,5) - 
            8*Power(t1,2)*Power(t2,5) - 3*Power(t2,6) + 
            5*t1*Power(t2,6) - 2*Power(s2,5)*(-3 + 4*t2 + Power(t2,2)) + 
            Power(s1,5)*(6*Power(s2,2) + 13*Power(t1,2) - 17*(-3 + t2) + 
               10*t1*(-1 + t2) - s2*(29 + 19*t1 + 15*t2)) + 
            Power(s2,4)*(-28 + 40*t2 - 19*Power(t2,2) + 6*Power(t2,3) + 
               8*t1*(-1 + 2*t2 + Power(t2,2))) - 
            Power(s2,3)*(-24 + 76*t2 - 34*Power(t2,2) - 85*Power(t2,3) + 
               8*Power(t2,4) + 12*Power(t1,2)*(1 + Power(t2,2)) + 
               4*t1*(-18 + 17*t2 + Power(t2,2) + 5*Power(t2,3))) - 
            s2*(2*Power(t1,4)*(5 - 4*t2 + Power(t2,2)) + 
               4*Power(t1,3)*
                (2 - 5*t2 + 17*Power(t2,2) + 3*Power(t2,3)) + 
               t2*(8 - 28*t2 - 142*Power(t2,2) + 61*Power(t2,3) - 
                  31*Power(t2,4)) + 
               2*t1*t2*(8 + 145*t2 - 76*Power(t2,2) + 105*Power(t2,3) - 
                  12*Power(t2,4)) + 
               Power(t1,2)*(-72 + 76*t2 - 136*Power(t2,2) - 
                  159*Power(t2,3) + 10*Power(t2,4))) + 
            Power(s2,2)*(8*Power(t1,3)*(3 - 2*t2 + Power(t2,2)) + 
               6*Power(t1,2)*
                (-8 + 2*t2 + 13*Power(t2,2) + 4*Power(t2,3)) + 
               t2*(20 + 83*t2 - 107*Power(t2,2) + 166*Power(t2,3) - 
                  16*Power(t2,4)) + 
               t1*(-72 + 144*t2 - 71*Power(t2,2) - 247*Power(t2,3) + 
                  17*Power(t2,4))) + 
            Power(s1,4)*(31 - 28*Power(s2,3) + 17*Power(t1,3) + 
               Power(s2,2)*(97 + 73*t1 - 40*t2) - 242*t2 + 
               25*Power(t2,2) - 3*Power(t1,2)*(1 + 20*t2) + 
               t1*(100 + 31*t2 - 5*Power(t2,2)) + 
               s2*(-62*Power(t1,2) + 2*t1*(-71 + 50*t2) + 
                  3*(-9 + 49*t2 + 10*Power(t2,2)))) - 
            Power(s1,3)*(19*Power(s2,4) - Power(t1,4) + 
               Power(s2,3)*(95 - 56*t1 - 92*t2) + 
               4*Power(t1,3)*(5 + 13*t2) - 
               Power(t1,2)*(26 + 13*t2 + 110*Power(t2,2)) + 
               2*(52 + 52*t2 - 229*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(-262 + 458*t2 + 24*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,2)*(-68 + 54*Power(t1,2) + 457*t2 - 
                  100*Power(t2,2) + t1*(-250 + 236*t2)) + 
               s2*(74 - 16*Power(t1,3) + Power(t1,2)*(135 - 196*t2) - 
                  142*t2 + 298*Power(t2,2) + 30*Power(t2,3) + 
                  2*t1*(79 - 318*t2 + 105*Power(t2,2)))) + 
            Power(s1,2)*(2*Power(s2,5) + 5*Power(t1,4) + 
               Power(s2,4)*(-35 - 8*t1 + 44*t2) + 
               Power(s2,3)*(30 + 12*Power(t1,2) + t1*(52 - 132*t2) + 
                  275*t2 - 108*Power(t2,2)) + 
               Power(t1,3)*(-107 + 43*t2 + 54*Power(t2,2)) - 
               Power(t1,2)*(-211 + 33*t2 + 21*Power(t2,2) + 
                  100*Power(t2,3)) - 
               2*(22 - 162*t2 - 63*Power(t2,2) + 216*Power(t2,3) + 
                  5*Power(t2,4)) + 
               t1*(8 - 866*t2 + 774*Power(t2,2) - 14*Power(t2,3) + 
                  35*Power(t2,4)) + 
               Power(s2,2)*(79 - 8*Power(t1,3) - 243*t2 + 
                  789*Power(t2,2) - 120*Power(t2,3) + 
                  6*Power(t1,2)*(1 + 22*t2) + 
                  t1*(-71 - 747*t2 + 270*Power(t2,2))) + 
               s2*(52 + 2*Power(t1,4) + 290*t2 - 264*Power(t2,2) + 
                  302*Power(t2,3) + 15*Power(t2,4) - 
                  4*Power(t1,3)*(7 + 11*t2) + 
                  Power(t1,2)*(148 + 429*t2 - 216*Power(t2,2)) + 
                  2*t1*(-153 + 234*t2 - 528*Power(t2,2) + 
                     110*Power(t2,3)))) + 
            s1*(10*Power(s2,5) - 3*Power(t1,4)*t2*(6 + t2) - 
               Power(s2,4)*(44 + 24*t1 - 54*t2 + 31*Power(t2,2)) - 
               2*Power(t1,3)*(4 - 103*t2 + 13*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t2*(88 - 336*t2 - 64*Power(t2,2) + 203*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(t1,2)*(4 - 402*t2 - 12*Power(t2,2) + 
                  15*Power(t2,3) + 45*Power(t2,4)) - 
               2*t1*(4 + 20*t2 - 473*Power(t2,2) + 287*Power(t2,3) - 
                  13*Power(t2,4) + 11*Power(t2,5)) + 
               Power(s2,3)*(76 + 12*Power(t1,2) - 64*t2 - 
                  265*Power(t2,2) + 52*Power(t2,3) + 
                  12*t1*(7 - 4*t2 + 8*Power(t2,2))) + 
               Power(s2,2)*(-20 + 8*Power(t1,3) - 162*t2 + 
                  282*Power(t2,2) - 595*Power(t2,3) + 70*Power(t2,4) - 
                  6*Power(t1,2)*(6 + 14*t2 + 17*Power(t2,2)) - 
                  2*t1*(72 - 71*t2 - 372*Power(t2,2) + 62*Power(t2,3))) + 
               s2*(8 - 6*Power(t1,4) - 80*t2 - 358*Power(t2,2) + 
                  210*Power(t2,3) - 153*Power(t2,4) - 3*Power(t2,5) + 
                  Power(t1,3)*(-4 + 96*t2 + 40*Power(t2,2)) + 
                  Power(t1,2)*
                   (76 - 284*t2 - 453*Power(t2,2) + 92*Power(t2,3)) + 
                  t1*(16 + 596*t2 - 462*Power(t2,2) + 772*Power(t2,3) - 
                     115*Power(t2,4))))))*R1(1 - s + s1 - t2))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(-1 + s - s1 + t2)*
       Power(s - s1 + t2,2)*Power(-s1 + s2 - t1 + t2,3)) + 
    (16*(80 + Power(s,5) + 25*s2 - 49*Power(s2,2) + 10*Power(s2,3) - 
         3*Power(s2,4) + Power(s2,5) - 189*t1 - 7*s2*t1 + 
         36*Power(s2,2)*t1 + 5*Power(s2,3)*t1 - Power(s2,4)*t1 + 
         132*Power(t1,2) + s2*Power(t1,2) - 16*Power(s2,2)*Power(t1,2) - 
         Power(s2,3)*Power(t1,2) - 27*Power(t1,3) + 6*s2*Power(t1,3) + 
         Power(s2,2)*Power(t1,3) - 4*Power(t1,4) + 
         Power(s1,3)*(4 + 3*Power(s2,2) - 4*t1 + 3*Power(t1,2) - 
            2*s2*(2 + 3*t1)) + 76*t2 + 45*s2*t2 - 36*Power(s2,2)*t2 + 
         5*Power(s2,3)*t2 - 4*Power(s2,4)*t2 - 101*t1*t2 - 51*s2*t1*t2 + 
         7*Power(s2,2)*t1*t2 + 9*Power(s2,3)*t1*t2 + 35*Power(t1,2)*t2 + 
         18*s2*Power(t1,2)*t2 - 7*Power(s2,2)*Power(t1,2)*t2 + 
         2*Power(t1,3)*t2 + 2*s2*Power(t1,3)*t2 - 8*Power(t2,2) + 
         31*s2*Power(t2,2) - 5*Power(s2,2)*Power(t2,2) + 
         Power(s2,3)*Power(t2,2) + 5*t1*Power(t2,2) - 
         25*s2*t1*Power(t2,2) - 3*Power(s2,2)*t1*Power(t2,2) + 
         2*Power(t1,2)*Power(t2,2) + 5*s2*Power(t1,2)*Power(t2,2) - 
         3*Power(t1,3)*Power(t2,2) - 4*Power(t2,3) + 11*s2*Power(t2,3) + 
         2*Power(s2,2)*Power(t2,3) - 3*t1*Power(t2,3) - 
         5*s2*t1*Power(t2,3) + 3*Power(t1,2)*Power(t2,3) + 
         Power(s,4)*(1 - 2*s1 - 5*s2 + 4*t2) + 
         Power(s,3)*(16 + Power(s1,2) + 8*Power(s2,2) - 7*t1 + 
            s1*(5*s2 + 3*t1 - 10*t2) + 7*t2 - 3*t1*t2 + 9*Power(t2,2) - 
            s2*(8 + t1 + 12*t2)) + 
         Power(s,2)*(11 - 4*Power(s2,3) + 17*t1 - 18*Power(t1,2) + 
            Power(t1,3) + 24*t2 + 13*t1*t2 - 9*Power(t1,2)*t2 - 
            Power(t2,2) + 6*t1*Power(t2,2) + 6*Power(t2,3) + 
            Power(s1,2)*(-1 + 3*s2 - 6*t1 + 6*t2) + 
            Power(s2,2)*(10 + t1 + 8*t2) + 
            s1*(-30 + Power(s2,2) + 8*t1 + 3*Power(t1,2) - 
               2*s2*(3 + 5*t1 - 8*t2) + 2*t2 - 12*Power(t2,2)) - 
            s2*(24 - 7*t1 + Power(t1,2) + 13*t2 - 11*t1*t2 + 
               19*Power(t2,2))) + 
         Power(s1,2)*(7*Power(s2,3) + Power(t1,3) + 5*t1*(3 + t2) - 
            3*Power(t1,2)*(4 + t2) - 4*(2 + 3*t2) - 
            Power(s2,2)*(7 + 13*t1 + 4*t2) + 
            s2*(21 + 5*Power(t1,2) + 19*t2 + t1*(-9 + 7*t2))) + 
         s1*(5*Power(s2,4) + 2*Power(t1,3)*(-1 + t2) - 
            2*Power(s2,3)*(5 + 4*t1 + 4*t2) + 
            Power(t1,2)*(-31 + 10*t2 - 3*Power(t2,2)) + 
            2*t1*(49 - 10*t2 + Power(t2,2)) + 
            4*(-19 + 4*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*(33 + Power(t1,2) + 12*t2 - Power(t2,2) + 
               8*t1*(1 + 2*t2)) + 
            2*s2*(-21 + Power(t1,3) - 26*t2 - 13*Power(t2,2) - 
               Power(t1,2)*(14 + 5*t2) + t1*(25 + 17*t2 + 2*Power(t2,2)))\
) + s*(-113 - Power(s2,4) + 151*t1 - 30*Power(t1,2) - 14*Power(t1,3) + 
            Power(s1,3)*(-4 - 3*s2 + 3*t1) - 117*t2 + 71*t1*t2 + 
            8*Power(t1,2)*t2 - 2*Power(t1,3)*t2 - 7*Power(t2,2) + 
            t1*Power(t2,2) - 6*Power(t1,2)*Power(t2,2) - 3*Power(t2,3) + 
            9*t1*Power(t2,3) + Power(s2,3)*(t1 + 4*t2) + 
            Power(s1,2)*(3 - 11*Power(s2,2) - 6*Power(t1,2) + 
               s2*(4 + 17*t1 - 2*t2) + 5*t2 + t1*(-13 + 3*t2)) + 
            Power(s2,2)*(-2 + 2*Power(t1,2) + t2 + 9*Power(t2,2) - 
               t1*(5 + 17*t2)) + 
            s2*(50 - 2*Power(t1,3) + 20*t2 + 2*Power(t2,2) - 
               8*Power(t2,3) + 2*Power(t1,2)*(13 + 8*t2) - 
               t1*(63 + 16*t2 + 5*Power(t2,2))) + 
            s1*(-9*Power(s2,3) - 2*Power(t1,3) + 
               6*Power(t1,2)*(1 + 2*t2) + 
               t1*(-73 + 12*t2 - 15*Power(t2,2)) + 
               2*(57 + 2*t2 + Power(t2,2)) + 
               Power(s2,2)*(15*t1 + 2*(8 + t2)) - 
               s2*(11 + 4*Power(t1,2) + 6*t2 - 13*Power(t2,2) + 
                  4*t1*(5 + 3*t2)))))*R2(1 - s1 - t1 + t2))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(-1 + s1 + t1 - t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) - 
    (8*(168 + 188*s2 + Power(s2,2) - 13*Power(s2,3) - 9*Power(s2,4) + 
         Power(s2,5) - 636*t1 - 546*s2*t1 - 7*Power(s2,2)*t1 + 
         59*Power(s2,3)*t1 + 11*Power(s2,4)*t1 - Power(s2,5)*t1 + 
         857*Power(t1,2) + 541*s2*Power(t1,2) - 
         23*Power(s2,2)*Power(t1,2) - 61*Power(s2,3)*Power(t1,2) - 
         2*Power(s2,4)*Power(t1,2) - 457*Power(t1,3) - 
         175*s2*Power(t1,3) + 41*Power(s2,2)*Power(t1,3) + 
         15*Power(s2,3)*Power(t1,3) + 44*Power(t1,4) - 
         16*s2*Power(t1,4) - 12*Power(s2,2)*Power(t1,4) + 
         24*Power(t1,5) + 8*s2*Power(t1,5) - 
         2*Power(s1,6)*(Power(s2,2) + Power(t1,2) - 2*s2*(2 + t1)) - 
         2*Power(s,7)*(t1 - t2) + 256*t2 + 509*s2*t2 + 
         239*Power(s2,2)*t2 - 17*Power(s2,3)*t2 - 18*Power(s2,4)*t2 - 
         10*Power(s2,5)*t2 + Power(s2,6)*t2 - 621*t1*t2 - 1166*s2*t1*t2 - 
         485*Power(s2,2)*t1*t2 + 40*Power(s2,3)*t1*t2 + 
         52*Power(s2,4)*t1*t2 + 4*Power(s2,5)*t1*t2 + 
         375*Power(t1,2)*t2 + 797*s2*Power(t1,2)*t2 + 
         256*Power(s2,2)*Power(t1,2)*t2 - 55*Power(s2,3)*Power(t1,2)*t2 - 
         21*Power(s2,4)*Power(t1,2)*t2 + 57*Power(t1,3)*t2 - 
         106*s2*Power(t1,3)*t2 + 5*Power(s2,2)*Power(t1,3)*t2 + 
         20*Power(s2,3)*Power(t1,3)*t2 - 60*Power(t1,4)*t2 - 
         32*s2*Power(t1,4)*t2 - 12*Power(s2,2)*Power(t1,4)*t2 - 
         8*Power(t1,5)*t2 - 8*Power(t2,2) + 376*s2*Power(t2,2) + 
         408*Power(s2,2)*Power(t2,2) + 37*Power(s2,3)*Power(t2,2) - 
         9*Power(s2,4)*Power(t2,2) - 4*Power(s2,5)*Power(t2,2) - 
         2*Power(s2,6)*Power(t2,2) + 192*t1*Power(t2,2) - 
         469*s2*t1*Power(t2,2) - 446*Power(s2,2)*t1*Power(t2,2) - 
         82*Power(s2,3)*t1*Power(t2,2) + 17*Power(s2,4)*t1*Power(t2,2) + 
         6*Power(s2,5)*t1*Power(t2,2) - 291*Power(t1,2)*Power(t2,2) - 
         32*s2*Power(t1,2)*Power(t2,2) + 
         98*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         15*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         8*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         89*Power(t1,3)*Power(t2,2) + 77*s2*Power(t1,3)*Power(t2,2) - 
         4*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         4*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         20*Power(t1,4)*Power(t2,2) + 16*s2*Power(t1,4)*Power(t2,2) - 
         104*Power(t2,3) - 52*s2*Power(t2,3) + 
         152*Power(s2,2)*Power(t2,3) + 67*Power(s2,3)*Power(t2,3) - 
         10*Power(s2,4)*Power(t2,3) + 4*Power(s2,5)*Power(t2,3) + 
         156*t1*Power(t2,3) + 249*s2*t1*Power(t2,3) - 
         19*Power(s2,2)*t1*Power(t2,3) - 29*Power(s2,3)*t1*Power(t2,3) - 
         8*Power(s2,4)*t1*Power(t2,3) - 25*Power(t1,2)*Power(t2,3) - 
         131*s2*Power(t1,2)*Power(t2,3) - 
         17*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         12*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         21*Power(t1,3)*Power(t2,3) - 20*s2*Power(t1,3)*Power(t2,3) - 
         8*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         4*Power(t1,4)*Power(t2,3) - 126*s2*Power(t2,4) - 
         43*Power(s2,2)*Power(t2,4) + 30*Power(s2,3)*Power(t2,4) - 
         4*Power(s2,4)*Power(t2,4) - 18*t1*Power(t2,4) + 
         85*s2*t1*Power(t2,4) + 21*Power(s2,2)*t1*Power(t2,4) + 
         6*Power(t1,2)*Power(t2,4) + 25*s2*Power(t1,2)*Power(t2,4) + 
         4*Power(t1,3)*Power(t2,4) + 4*s2*Power(t1,3)*Power(t2,4) + 
         8*Power(t2,5) - 9*s2*Power(t2,5) - 27*Power(s2,2)*Power(t2,5) + 
         4*Power(s2,3)*Power(t2,5) + t1*Power(t2,5) - 
         11*s2*t1*Power(t2,5) - 2*Power(t1,2)*Power(t2,5) - 
         4*s2*Power(t1,2)*Power(t2,5) + 10*s2*Power(t2,6) - 
         2*Power(s2,2)*Power(t2,6) - 2*t1*Power(t2,6) + 
         2*s2*t1*Power(t2,6) - 
         2*Power(s,6)*(1 + t1 - 5*s2*t1 + Power(t1,2) + t2 + 4*s2*t2 + 
            4*t1*t2 - 5*Power(t2,2) + s1*(-2 + s2 - 6*t1 + 5*t2)) + 
         Power(s1,5)*(-8 - 8*Power(s2,3) + 
            s2*(2 - 8*Power(t1,2) + t1*(32 - 22*t2) - 50*t2) + 
            2*t1*(3 + t2) + 5*Power(t1,2)*(-1 + 2*t2) + 
            Power(s2,2)*(13 + 16*t1 + 12*t2)) + 
         Power(s,5)*(6 + Power(t1,2)*(-13 + 8*s2 - 10*t2) - 16*t2 + 
            5*s2*t2 + 10*Power(s2,2)*t2 - 17*Power(t2,2) - 
            36*s2*Power(t2,2) + 20*Power(t2,3) + 
            2*Power(s1,2)*(-8 + 5*s2 - 15*t1 + 10*t2) + 
            s1*(4 + 10*Power(s2,2) + 7*t1 + 12*Power(t1,2) - 
               2*s2*(7 + 27*t1 - 13*t2) + 33*t2 + 40*t1*t2 - 
               40*Power(t2,2)) + 
            t1*(4 - 20*Power(s2,2) + 6*t2 - 10*Power(t2,2) + 
               5*s2*(3 + 8*t2))) - 
         Power(s1,4)*(12*Power(s2,4) + 11*Power(t1,3) - 40*t2 - 
            4*Power(s2,3)*(1 + 6*t1 + 9*t2) + 
            2*Power(t1,2)*(-15 - 9*t2 + 10*Power(t2,2)) + 
            t1*(27 + 23*t2 + 10*Power(t2,2)) + 
            Power(s2,2)*(59 + 12*Power(t1,2) + 79*t2 + 30*Power(t2,2) + 
               t1*(-53 + 64*t2)) - 
            s2*(-117 - 17*t2 + 130*Power(t2,2) + 
               Power(t1,2)*(34 + 28*t2) + 
               t1*(77 - 139*t2 + 50*Power(t2,2)))) + 
         Power(s1,3)*(104 - 8*Power(s2,5) - 80*Power(t2,2) + 
            4*Power(s2,4)*(-1 + 4*t1 + 10*t2) + 
            Power(t1,3)*(10 + 29*t2) + 
            Power(t1,2)*(63 - 96*t2 - 22*Power(t2,2) + 20*Power(t2,3)) + 
            t1*(-179 + 99*t2 + 32*Power(t2,2) + 20*Power(t2,3)) - 
            Power(s2,3)*(41 + 8*Power(t1,2) + 42*t2 + 64*Power(t2,2) + 
               t1*(-25 + 72*t2)) + 
            Power(s2,2)*(-89 + 220*t2 + 186*Power(t2,2) + 
               40*Power(t2,3) + Power(t1,2)*(70 + 36*t2) + 
               t1*(-107 - 180*t2 + 96*Power(t2,2))) - 
            s2*(-75 - 477*t2 - 48*Power(t2,2) + 180*Power(t2,3) + 
               Power(t1,3)*(11 + 4*t2) + 
               Power(t1,2)*(-242 + 127*t2 + 32*Power(t2,2)) + 
               t1*(350 + 316*t2 - 236*Power(t2,2) + 60*Power(t2,3)))) - 
         Power(s1,2)*(8 + 2*Power(s2,6) + 4*Power(t1,4)*(-1 + t2) + 
            312*t2 - 80*Power(t2,3) - 4*Power(s2,5)*(-1 + t1 + 5*t2) + 
            Power(t1,3)*(-160 + 41*t2 + 21*Power(t2,2)) + 
            Power(s2,4)*(-11 + t1 + 2*Power(t1,2) + 2*t2 + 40*t1*t2 + 
               48*Power(t2,2)) + 
            Power(t1,2)*(381 + 151*t2 - 108*Power(t2,2) - 
               8*Power(t2,3) + 10*Power(t2,4)) + 
            t1*(-227 - 514*t2 + 135*Power(t2,2) + 18*Power(t2,3) + 
               20*Power(t2,4)) - 
            Power(s2,3)*(62 + 149*t2 + 102*Power(t2,2) + 
               56*Power(t2,3) + Power(t1,2)*(54 + 28*t2) + 
               t1*(-154 - 79*t2 + 72*Power(t2,2))) + 
            Power(s2,2)*(-382 - 330*t2 + 306*Power(t2,2) + 
               214*Power(t2,3) + 30*Power(t2,4) + 
               Power(t1,3)*(17 + 8*t2) + 
               Power(t1,2)*(-132 + 157*t2 + 36*Power(t2,2)) + 
               t1*(433 - 195*t2 - 222*Power(t2,2) + 64*Power(t2,3))) - 
            s2*(341 + 8*Power(t1,4) - 202*t2 - 729*Power(t2,2) - 
               62*Power(t2,3) + 140*Power(t2,4) + 
               Power(t1,3)*(111 + 2*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(-141 - 615*t2 + 177*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-353 + 949*t2 + 486*Power(t2,2) - 194*Power(t2,3) + 
                  40*Power(t2,4)))) + 
         s1*(8*Power(t1,5) + Power(s2,6)*(-1 + 4*t2) + 
            8*Power(t1,4)*(9 - 3*t2 + Power(t2,2)) - 
            Power(t1,3)*(94 + 249*t2 - 52*Power(t2,2) + Power(t2,3)) - 
            8*(32 - 2*t2 - 39*Power(t2,2) + 5*Power(t2,4)) + 
            Power(t1,2)*(-338 + 672*t2 + 113*Power(t2,2) - 
               48*Power(t2,3) + 3*Power(t2,4) + 2*Power(t2,5)) + 
            t1*(609 - 419*t2 - 491*Power(t2,2) + 81*Power(t2,3) + 
               2*Power(t2,4) + 10*Power(t2,5)) + 
            Power(s2,5)*(11 + 8*t2 - 16*Power(t2,2) - 5*t1*(1 + 2*t2)) + 
            Power(s2,4)*(17 - 2*t2 + 16*Power(t2,2) + 24*Power(t2,3) + 
               Power(t1,2)*(23 + 10*t2) + 
               t1*(-53 - 16*t2 + 32*Power(t2,2))) - 
            Power(s2,3)*(4 + 99*t2 + 175*Power(t2,2) + 94*Power(t2,3) + 
               24*Power(t2,4) + Power(t1,3)*(17 + 4*t2) + 
               Power(t1,2)*(-26 + 69*t2 + 32*Power(t2,2)) + 
               t1*(-7 - 236*t2 - 83*Power(t2,2) + 24*Power(t2,3))) + 
            Power(s2,2)*(-246 + 8*Power(t1,4) - 790*t2 - 
               393*Power(t2,2) + 188*Power(t2,3) + 121*Power(t2,4) + 
               12*Power(t2,5) + 
               Power(t1,3)*(48 + 21*t2 + 16*Power(t2,2)) + 
               Power(t1,2)*(-359 - 230*t2 + 104*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(546 + 879*t2 - 69*Power(t2,2) - 116*Power(t2,3) + 
                  16*Power(t2,4))) + 
            s2*(-497 + Power(t1,4)*(8 - 24*t2) - 717*t2 + 
               179*Power(t2,2) + 495*Power(t2,3) + 38*Power(t2,4) - 
               58*Power(t2,5) + 
               Power(t1,3)*(151 - 188*t2 + 29*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,2)*(-800 + 173*t2 + 504*Power(t2,2) - 
                  109*Power(t2,3) + 8*Power(t2,4)) - 
               2*t1*(-568 - 411*t2 + 424*Power(t2,2) + 166*Power(t2,3) - 
                  38*Power(t2,4) + 7*Power(t2,5)))) + 
         Power(s,4)*(-26 + 57*t1 + 20*Power(s2,3)*t1 - 12*Power(t1,2) - 
            11*Power(t1,3) - 15*t2 + 30*t1*t2 - 33*Power(t1,2)*t2 - 
            48*Power(t2,2) + 29*t1*Power(t2,2) - 
            20*Power(t1,2)*Power(t2,2) - 45*Power(t2,3) + 
            20*Power(t2,4) - 4*Power(s1,3)*(-6 + 5*s2 - 10*t1 + 5*t2) + 
            Power(s2,2)*(20 - 12*Power(t1,2) + t2 + 42*Power(t2,2) - 
               40*t1*(1 + 2*t2)) - 
            Power(s1,2)*(-2 + 42*Power(s2,2) + 30*Power(t1,2) + 93*t2 - 
               60*Power(t2,2) + s2*(-51 - 120*t1 + 20*t2) + 
               t1*(8 + 80*t2)) + 
            s2*(3 + 64*t2 + 64*Power(t2,2) - 60*Power(t2,3) + 
               Power(t1,2)*(67 + 40*t2) + 
               t1*(-66 - 31*t2 + 50*Power(t2,2))) + 
            s1*(3 - 20*Power(s2,3) + 3*Power(s2,2)*(5 + 32*t1) + 46*t2 + 
               114*Power(t2,2) - 60*Power(t2,3) + 
               Power(t1,2)*(47 + 50*t2) + 
               t1*(-40 - 21*t2 + 40*Power(t2,2)) - 
               s2*(40*Power(t1,2) + t1*(27 + 170*t2) + 
                  5*(2 + 23*t2 - 20*Power(t2,2))))) + 
         Power(s,3)*(6 - 71*t1 + 123*Power(t1,2) - 46*Power(t1,3) - 
            41*t2 + 4*t1*t2 + 85*Power(t1,2)*t2 - 45*Power(t1,3)*t2 - 
            19*Power(t2,2) - 48*t1*Power(t2,2) + 
            Power(t1,2)*Power(t2,2) - 31*Power(t2,3) + 
            13*t1*Power(t2,3) - 20*Power(t1,2)*Power(t2,3) - 
            49*Power(t2,4) + 10*t1*Power(t2,4) + 10*Power(t2,5) - 
            10*Power(s2,4)*(t1 + t2) + 
            2*Power(s1,4)*(-8 + 10*s2 - 15*t1 + 5*t2) + 
            2*Power(s2,3)*(-20 + 4*Power(t1,2) - 7*t2 - 4*Power(t2,2) + 
               5*t1*(5 + 8*t2)) + 
            Power(s1,3)*(-10 + 68*Power(s2,2) + 40*Power(t1,2) + 
               97*t2 - 40*Power(t2,2) - s2*(69 + 140*t1 + 16*t2) + 
               t1*(2 + 80*t2)) - 
            Power(s2,2)*(26 + 62*t2 + 82*Power(t2,2) - 56*Power(t2,3) + 
               Power(t1,2)*(119 + 60*t2) + 
               t1*(-151 - 45*t2 + 96*Power(t2,2))) + 
            s2*(93 + 167*t2 + 251*Power(t2,2) + 157*Power(t2,3) - 
               44*Power(t2,4) + Power(t1,3)*(43 + 4*t2) + 
               Power(t1,2)*(27 + 133*t2 + 68*Power(t2,2)) + 
               t1*(-189 - 286*t2 - 133*Power(t2,2) + 12*Power(t2,3))) + 
            Power(s1,2)*(-51 + 68*Power(s2,3) - 11*t2 - 
               195*Power(t2,2) + 60*Power(t2,3) - 
               2*Power(t1,2)*(29 + 50*t2) - 
               Power(s2,2)*(49 + 184*t1 + 80*t2) + 
               t1*(70 + 9*t2 - 60*Power(t2,2)) + 
               s2*(67 + 80*Power(t1,2) + 295*t2 - 72*Power(t2,2) + 
                  t1*(-9 + 292*t2))) + 
            s1*(71 + 20*Power(s2,4) + 44*Power(t1,3) + 70*t2 + 
               52*Power(t2,2) + 163*Power(t2,3) - 40*Power(t2,4) - 
               12*Power(s2,3)*(7*t1 + 5*t2) - 
               2*t1*(42 + 11*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(-46 + 57*t2 + 80*Power(t2,2)) + 
               Power(s2,2)*(-29 + 48*Power(t1,2) + 131*t2 - 
                  44*Power(t2,2) + 4*t1*(13 + 70*t2)) - 
               s2*(123 + 318*t2 + 383*Power(t2,2) - 112*Power(t2,3) + 
                  Power(t1,2)*(187 + 148*t2) + 
                  2*t1*(-159 - 71*t2 + 82*Power(t2,2))))) - 
         Power(s,2)*(-228 + 435*t1 - 161*Power(t1,2) - 88*Power(t1,3) + 
            36*Power(t1,4) - 453*t2 + 543*t1*t2 - 104*Power(t1,2)*t2 - 
            31*Power(t1,3)*t2 + 4*Power(t1,4)*t2 - 202*Power(t2,2) + 
            64*t1*Power(t2,2) - 36*Power(t1,2)*Power(t2,2) + 
            53*Power(t1,3)*Power(t2,2) - 4*Power(t2,3) + 
            65*t1*Power(t2,3) - 47*Power(t1,2)*Power(t2,3) - 
            4*Power(t2,4) + 29*t1*Power(t2,4) + 
            10*Power(t1,2)*Power(t2,4) + 21*Power(t2,5) - 
            8*t1*Power(t2,5) - 2*Power(t2,6) + 
            2*Power(s1,5)*(-2 + 5*s2 - 6*t1 + t2) - 
            2*Power(s2,5)*(t1 + 4*t2) + 
            2*Power(s2,4)*(-15 + Power(t1,2) - 8*t2 + 9*Power(t2,2) + 
               5*t1*(3 + 4*t2)) + 
            Power(s1,4)*(52*Power(s2,2) + 30*Power(t1,2) + 
               (37 - 10*t2)*t2 - s2*(49 + 90*t1 + 28*t2) + 
               t1*(-2 + 40*t2)) + 
            Power(s2,3)*(-20 + 14*t2 - 36*Power(t2,2) + 8*Power(t2,3) - 
               Power(t1,2)*(89 + 40*t2) + 
               t1*(121 + 17*t2 - 88*Power(t2,2))) + 
            Power(s2,2)*(105 + 279*t2 + 347*Power(t2,2) + 
               185*Power(t2,3) - 24*Power(t2,4) + 
               Power(t1,3)*(49 + 8*t2) + 
               12*Power(t1,2)*(4 + 16*t2 + 7*Power(t2,2)) + 
               2*t1*(-115 - 271*t2 - 96*Power(t2,2) + 16*Power(t2,3))) + 
            s2*(255 - 8*Power(t1,4) + 333*t2 + 23*Power(t2,2) - 
               211*Power(t2,3) - 144*Power(t2,4) + 12*Power(t2,5) - 
               Power(t1,3)*(119 + 98*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(514 + 315*t2 - 42*Power(t2,2) - 
                  44*Power(t2,3)) + 
               t1*(-620 - 453*t2 + 27*Power(t2,2) + 92*Power(t2,3) + 
                  14*Power(t2,4))) + 
            Power(s1,3)*(-61 + 84*Power(s2,3) + 4*t2 - 
               108*Power(t2,2) + 20*Power(t2,3) - 
               2*Power(t1,2)*(11 + 50*t2) - 
               2*Power(s2,2)*(29 + 88*t1 + 66*t2) + 
               t1*(70 - 23*t2 - 40*Power(t2,2)) + 
               s2*(118 + 80*Power(t1,2) + 291*t2 + 12*Power(t2,2) + 
                  t1*(-71 + 256*t2))) + 
            Power(s1,2)*(52*Power(s2,4) + 66*Power(t1,3) - 
               Power(s2,3)*(5 + 132*t1 + 160*t2) + 
               3*Power(t1,2)*(-26 - t2 + 40*Power(t2,2)) + 
               t1*(30 - 75*t2 + 81*Power(t2,2)) - 
               2*(79 - 59*t2 + 6*Power(t2,2) - 71*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,2)*(109 + 72*Power(t1,2) + 301*t2 + 
                  84*Power(t2,2) + t1*(-37 + 384*t2)) - 
               s2*(97 + 447*t2 + 579*Power(t2,2) - 32*Power(t2,3) + 
                  3*Power(t1,2)*(69 + 68*t2) + 
                  3*t1*(-131 - 78*t2 + 76*Power(t2,2)))) + 
            s1*(451 + 10*Power(s2,5) + 360*t2 - 53*Power(t2,2) + 
               12*Power(t2,3) - 88*Power(t2,4) + 10*Power(t2,5) - 
               2*Power(s2,4)*(-5 + 18*t1 + 35*t2) - 
               Power(t1,3)*(22 + 119*t2) + 
               Power(t1,2)*(207 + 114*t2 + 72*Power(t2,2) - 
                  60*Power(t2,3)) + 
               t1*(-589 - 94*t2 - 60*Power(t2,2) - 85*Power(t2,3) + 
                  20*Power(t2,4)) + 
               Power(s2,3)*(-83 + 24*Power(t1,2) + 41*t2 + 
                  68*Power(t2,2) + 4*t1*(14 + 55*t2)) - 
               Power(s2,2)*(226 + 456*t2 + 428*Power(t2,2) - 
                  20*Power(t2,3) + 52*Power(t1,2)*(5 + 3*t2) + 
                  t1*(-577 - 229*t2 + 240*Power(t2,2))) + 
               s2*(-237 + 74*t2 + 540*Power(t2,2) + 481*Power(t2,3) - 
                  38*Power(t2,4) + Power(t1,3)*(97 + 12*t2) + 
                  3*Power(t1,2)*(-60 + 83*t2 + 56*Power(t2,2)) + 
                  3*t1*(67 - 140*t2 - 85*Power(t2,2) + 16*Power(t2,3))))) \
+ s*(-380 + 2*Power(s1,6)*(s2 - t1) + 1093*t1 - 1002*Power(t1,2) + 
            250*Power(t1,3) + 48*Power(t1,4) - 8*Power(t1,5) - 637*t2 - 
            2*Power(s2,6)*t2 + 1140*t1*t2 - 445*Power(t1,2)*t2 - 
            15*Power(t1,3)*t2 - 16*Power(t1,4)*t2 - 120*Power(t2,2) - 
            89*t1*Power(t2,2) + 124*Power(t1,2)*Power(t2,2) + 
            56*Power(t1,3)*Power(t2,2) - 8*Power(t1,4)*Power(t2,2) + 
            156*Power(t2,3) - 101*t1*Power(t2,3) - 
            55*Power(t1,2)*Power(t2,3) - 15*Power(t1,3)*Power(t2,3) + 
            22*Power(t2,4) + 10*t1*Power(t2,4) + 
            24*Power(t1,2)*Power(t2,4) + Power(t2,5) - 
            23*t1*Power(t2,5) - 2*Power(t1,2)*Power(t2,5) - 
            2*Power(t2,6) + 2*t1*Power(t2,6) + 
            Power(s1,5)*(6 + 18*Power(s2,2) - t1 + 12*Power(t1,2) + 
               2*t2 + 8*t1*t2 - 5*s2*(5 + 6*t1 + 2*t2)) + 
            Power(s2,5)*(-8 - 7*t2 + 12*Power(t2,2) + t1*(7 + 8*t2)) - 
            Power(s2,4)*(4 - 38*t2 - 3*Power(t2,2) + 12*Power(t2,3) + 
               2*Power(t1,2)*(12 + 5*t2) + 
               t1*(-33 + 7*t2 + 38*Power(t2,2))) + 
            Power(s2,3)*(47 + 145*t2 + 153*Power(t2,2) + 
               83*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,3)*(17 + 4*t2) + 
               Power(t1,2)*(35 + 113*t2 + 44*Power(t2,2)) + 
               t1*(-109 - 338*t2 - 105*Power(t2,2) + 28*Power(t2,3))) - 
            Power(s2,2)*(-238 + 8*Power(t1,4) - 351*t2 + 3*Power(t2,2) + 
               239*Power(t2,3) + 125*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,3)*(100 + 69*t2 + 16*Power(t2,2)) + 
               Power(t1,2)*(-436 - 293*t2 + 66*Power(t2,2) + 
                  36*Power(t2,3)) + 
               t1*(556 + 453*t2 - 153*Power(t2,2) - 112*Power(t2,3) - 
                  4*Power(t2,4))) + 
            s2*(-37 - 420*t2 - 650*Power(t2,2) - 276*Power(t2,3) + 
               47*Power(t2,4) + 56*Power(t2,5) + 
               8*Power(t1,4)*(4 + 3*t2) + 
               Power(t1,3)*(-191 + 20*t2 + 35*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(275 - 387*t2 - 249*Power(t2,2) + 
                  Power(t2,3) + 4*Power(t2,4)) - 
               2*t1*(42 - 335*t2 - 382*Power(t2,2) - 71*Power(t2,3) + 
                  8*Power(t2,4) + 2*Power(t2,5))) + 
            Power(s1,4)*(13 + 44*Power(s2,3) + Power(t1,2)*(7 - 50*t2) - 
               23*t2 - 10*Power(t2,2) - 
               Power(s2,2)*(37 + 84*t1 + 74*t2) + 
               t1*(30 - 19*t2 - 10*Power(t2,2)) + 
               s2*(67 + 40*Power(t1,2) + 156*t2 + 20*Power(t2,2) + 
                  2*t1*(-41 + 58*t2))) + 
            Power(s1,3)*(-179 + 44*Power(s2,4) + 44*Power(t1,3) - 
               61*t2 + 32*Power(t2,2) + 20*Power(t2,3) - 
               Power(s2,3)*(9 + 92*t1 + 136*t2) + 
               5*Power(t1,2)*(-10 - 9*t2 + 16*Power(t2,2)) + 
               2*t1*(102 - 50*t2 + 43*Power(t2,2)) + 
               Power(s2,2)*(161 + 48*Power(t1,2) + 236*t2 + 
                  116*Power(t2,2) + 2*t1*(-51 + 124*t2)) - 
               s2*(-148 + 248*t2 + 374*Power(t2,2) + 20*Power(t2,3) + 
                  Power(t1,2)*(121 + 124*t2) + 
                  2*t1*(-52 - 131*t2 + 82*Power(t2,2)))) + 
            Power(s1,2)*(-85 + 18*Power(s2,5) + 
               Power(t1,3)*(14 - 103*t2) + 
               Power(s2,4)*(13 - 42*t1 - 100*t2) + 514*t2 + 
               105*Power(t2,2) - 18*Power(t2,3) - 20*Power(t2,4) + 
               Power(t1,2)*(261 + 45*t2 + 93*Power(t2,2) - 
                  60*Power(t2,3)) + 
               t1*(-223 - 509*t2 + 120*Power(t2,2) - 134*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,3)*(29 + 24*Power(t1,2) + 101*t2 + 
                  144*Power(t2,2) + t1*(-19 + 212*t2)) - 
               Power(s2,2)*(116 + 561*t2 + 486*Power(t2,2) + 
                  84*Power(t2,3) + Power(t1,2)*(211 + 132*t2) + 
                  t1*(-473 - 316*t2 + 240*Power(t2,2))) + 
               s2*(-580 - 572*t2 + 342*Power(t2,2) + 436*Power(t2,3) + 
                  10*Power(t2,4) + Power(t1,3)*(65 + 12*t2) + 
                  3*Power(t1,2)*(-123 + 81*t2 + 44*Power(t2,2)) + 
                  t1*(748 - 66*t2 - 294*Power(t2,2) + 96*Power(t2,3)))) + 
            s1*(625 + 2*Power(s2,6) + 205*t2 - 491*Power(t2,2) - 
               79*Power(t2,3) + 2*Power(t2,4) + 10*Power(t2,5) + 
               8*Power(t1,4)*(4 + t2) - 6*Power(s2,5)*(-1 + t1 + 5*t2) + 
               Power(t1,3)*(-8 - 70*t2 + 74*Power(t2,2)) + 
               Power(t1,2)*(424 - 385*t2 + 60*Power(t2,2) - 
                  79*Power(t2,3) + 20*Power(t2,4)) + 
               t1*(-1101 + 312*t2 + 406*Power(t2,2) - 60*Power(t2,3) + 
                  91*Power(t2,4) - 8*Power(t2,5)) + 
               Power(s2,4)*(-59 + 4*Power(t1,2) - 16*t2 + 
                  68*Power(t2,2) + t1*(29 + 80*t2)) - 
               Power(s2,3)*(123 + 182*t2 + 175*Power(t2,2) + 
                  56*Power(t2,3) + Power(t1,2)*(143 + 68*t2) + 
                  4*t1*(-88 - 31*t2 + 37*Power(t2,2))) + 
               Power(s2,2)*(-264 + 119*t2 + 639*Power(t2,2) + 
                  412*Power(t2,3) + 26*Power(t2,4) + 
                  2*Power(t1,3)*(33 + 8*t2) + 
                  Power(t1,2)*(-168 + 277*t2 + 120*Power(t2,2)) + 
                  t1*(234 - 626*t2 - 326*Power(t2,2) + 72*Power(t2,3))) - 
               s2*(-425 + 16*Power(t1,4) - 1230*t2 - 700*Power(t2,2) + 
                  208*Power(t2,3) + 249*Power(t2,4) + 2*Power(t2,5) + 
                  2*Power(t1,3)*(75 + 50*t2 + 12*Power(t2,2)) + 
                  Power(t1,2)*
                   (-631 - 618*t2 + 123*Power(t2,2) + 52*Power(t2,3)) + 
                  2*t1*(396 + 756*t2 + 90*Power(t2,2) - 65*Power(t2,3) + 
                     7*Power(t2,4))))))*
       T2(1 - s + s2 - t1,1 - s1 - t1 + t2))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(-1 + s1 + t1 - t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) + 
    (8*(8*s2 - 4*Power(s2,2) - 28*Power(s2,3) - 14*Power(s2,4) + 
         Power(s2,5) - 8*t1 - 32*s2*t1 + 56*Power(s2,2)*t1 + 
         104*Power(s2,3)*t1 + 21*Power(s2,4)*t1 - Power(s2,5)*t1 + 
         36*Power(t1,2) + 44*s2*Power(t1,2) - 
         152*Power(s2,2)*Power(t1,2) - 106*Power(s2,3)*Power(t1,2) - 
         7*Power(s2,4)*Power(t1,2) - 72*Power(t1,3) - 8*s2*Power(t1,3) + 
         130*Power(s2,2)*Power(t1,3) + 30*Power(s2,3)*Power(t1,3) + 
         70*Power(t1,4) - 15*s2*Power(t1,4) - 
         30*Power(s2,2)*Power(t1,4) - 31*Power(t1,5) + 3*s2*Power(t1,5) + 
         5*Power(t1,6) - 2*Power(s1,6)*
          (Power(s2,2) + Power(t1,2) - 2*s2*(2 + t1)) + 32*t2 - 
         20*s2*t2 - 48*Power(s2,2)*t2 - 30*Power(s2,3)*t2 - 
         30*Power(s2,4)*t2 - 15*Power(s2,5)*t2 + Power(s2,6)*t2 - 
         92*t1*t2 + 60*s2*t1*t2 + 114*Power(s2,2)*t1*t2 + 
         82*Power(s2,3)*t1*t2 + 77*Power(s2,4)*t1*t2 + 
         9*Power(s2,5)*t1*t2 + 132*Power(t1,2)*t2 - 
         14*s2*Power(t1,2)*t2 - 42*Power(s2,2)*Power(t1,2)*t2 - 
         94*Power(s2,3)*Power(t1,2)*t2 - 34*Power(s2,4)*Power(t1,2)*t2 - 
         150*Power(t1,3)*t2 - 102*s2*Power(t1,3)*t2 - 
         22*Power(s2,2)*Power(t1,3)*t2 + 30*Power(s2,3)*Power(t1,3)*t2 + 
         108*Power(t1,4)*t2 + 85*s2*Power(t1,4)*t2 + 
         Power(s2,2)*Power(t1,4)*t2 - 31*Power(t1,5)*t2 - 
         7*s2*Power(t1,5)*t2 + 8*Power(t2,2) + 16*s2*Power(t2,2) - 
         26*Power(s2,2)*Power(t2,2) - 48*Power(s2,3)*Power(t2,2) - 
         24*Power(s2,4)*Power(t2,2) - 4*Power(s2,5)*Power(t2,2) - 
         2*Power(s2,6)*Power(t2,2) - 24*t1*Power(t2,2) - 
         120*s2*t1*Power(t2,2) - 14*Power(s2,2)*t1*Power(t2,2) + 
         64*Power(s2,3)*t1*Power(t2,2) + 12*Power(s2,4)*t1*Power(t2,2) + 
         4*Power(s2,5)*t1*Power(t2,2) + 122*Power(t1,2)*Power(t2,2) + 
         276*s2*Power(t1,2)*Power(t2,2) + 
         88*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         10*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         174*Power(t1,3)*Power(t2,2) - 212*s2*Power(t1,3)*Power(t2,2) - 
         46*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         4*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         68*Power(t1,4)*Power(t2,2) + 26*s2*Power(t1,4)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         2*Power(t1,5)*Power(t2,2) - 8*Power(t2,3) + 54*s2*Power(t2,3) + 
         14*Power(s2,2)*Power(t2,3) - 6*Power(s2,3)*Power(t2,3) + 
         4*Power(s2,4)*Power(t2,3) - 70*t1*Power(t2,3) - 
         158*s2*t1*Power(t2,3) - 100*Power(s2,2)*t1*Power(t2,3) - 
         58*Power(s2,3)*t1*Power(t2,3) - 2*Power(s2,4)*t1*Power(t2,3) + 
         152*Power(t1,2)*Power(t2,3) + 190*s2*Power(t1,2)*Power(t2,3) + 
         116*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         2*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         68*Power(t1,3)*Power(t2,3) - 58*s2*Power(t1,3)*Power(t2,3) + 
         2*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         4*Power(t1,4)*Power(t2,3) - 2*s2*Power(t1,4)*Power(t2,3) + 
         24*Power(t2,4) + 15*s2*Power(t2,4) + 
         40*Power(s2,2)*Power(t2,4) + 30*Power(s2,3)*Power(t2,4) - 
         63*t1*Power(t2,4) - 87*s2*t1*Power(t2,4) - 
         98*Power(s2,2)*t1*Power(t2,4) + 6*Power(s2,3)*t1*Power(t2,4) + 
         31*Power(t1,2)*Power(t2,4) + 76*s2*Power(t1,2)*Power(t2,4) - 
         12*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         6*s2*Power(t1,3)*Power(t2,4) + 8*Power(t2,5) + 
         21*s2*Power(t2,5) + 27*Power(s2,2)*Power(t2,5) - 
         4*Power(s2,3)*Power(t2,5) - 5*t1*Power(t2,5) - 
         47*s2*t1*Power(t2,5) + 10*Power(s2,2)*t1*Power(t2,5) + 
         4*Power(t1,2)*Power(t2,5) - 6*s2*Power(t1,2)*Power(t2,5) + 
         10*s2*Power(t2,6) - 2*Power(s2,2)*Power(t2,6) - 
         2*t1*Power(t2,6) + 2*s2*t1*Power(t2,6) + 
         Power(s1,5)*(-8 + 4*Power(s2,3) - 6*Power(t1,3) + 
            2*s2*(-11 + 8*Power(t1,2) + t1*(20 - 11*t2) - 25*t2) + 
            2*t1*(3 + t2) + Power(t1,2)*(1 + 10*t2) + 
            Power(s2,2)*(-25 - 14*t1 + 12*t2)) + 
         2*Power(s,4)*(-2 + Power(t1,4) + Power(s1,3)*(t1 - t2) - 
            4*Power(t1,3)*t2 + 6*Power(t1,2)*Power(t2,2) - 
            4*t1*Power(t2,3) + Power(t2,4) + Power(s2,3)*(2 - t1 + t2) - 
            3*Power(s1,2)*(t1 - t2)*(s2 - t1 + t2) + 
            3*Power(s2,2)*(-2 + Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
            s2*(6 - 3*Power(t1,3) + 9*Power(t1,2)*t2 - 
               9*t1*Power(t2,2) + 3*Power(t2,3)) + 
            s1*(2 - 2*Power(s2,3) + 3*Power(t1,3) + 
               3*Power(s2,2)*(2 + t1 - t2) - 9*Power(t1,2)*t2 + 
               9*t1*Power(t2,2) - 3*Power(t2,3) - 
               6*s2*(1 + Power(t1,2) - 2*t1*t2 + Power(t2,2)))) + 
         Power(s1,4)*(-6*Power(t1,4) + Power(s2,3)*(24 + 6*t1 - 20*t2) + 
            8*(3 + 5*t2) + 4*Power(t1,3)*(1 + 6*t2) - 
            5*Power(t1,2)*(-7 + 4*Power(t2,2)) - 
            t1*(65 + 29*t2 + 10*Power(t2,2)) + 
            Power(s2,2)*(42 - 18*Power(t1,2) + 127*t2 - 
               30*Power(t2,2) + t1*(-90 + 66*t2)) + 
            s2*(17 + 18*Power(t1,3) - 70*Power(t1,2)*(-1 + t2) + 
               109*t2 + 130*Power(t2,2) + 
               t1*(-93 - 207*t2 + 50*Power(t2,2)))) - 
         2*Power(s1,3)*(-4 + 2*Power(s2,5) - 7*Power(s2,4)*(-1 + t1) + 
            Power(t1,5) + 48*t2 + 40*Power(t2,2) - 
            3*Power(t1,4)*(1 + 3*t2) + 
            Power(t1,3)*(-35 + 6*t2 + 18*Power(t2,2)) + 
            Power(t1,2)*(77 + 68*t2 + 5*Power(t2,2) - 10*Power(t2,3)) - 
            t1*(35 + 129*t2 + 28*Power(t2,2) + 10*Power(t2,3)) + 
            Power(s2,3)*(-7 + 8*Power(t1,2) + 51*t2 - 20*Power(t2,2) + 
               3*t1*(-11 + 4*t2)) - 
            Power(s2,2)*(6 + 2*Power(t1,3) - 83*t2 - 129*Power(t2,2) + 
               20*Power(t2,3) + Power(t1,2)*(-56 + 33*t2) + 
               t1*(35 + 184*t2 - 62*Power(t2,2))) + 
            s2*(-2*Power(t1,4) + 3*Power(t1,3)*(-9 + 10*t2) + 
               Power(t1,2)*(85 + 143*t2 - 60*Power(t2,2)) + 
               t1*(-67 - 183*t2 - 214*Power(t2,2) + 30*Power(t2,3)) + 
               3*(9 + 11*t2 + 36*Power(t2,2) + 30*Power(t2,3)))) - 
         2*Power(s1,2)*(Power(s2,6) - 2*Power(t1,5)*(1 + t2) - 
            Power(s2,5)*(-2 + t1 + 4*t2) + 
            Power(s2,4)*(5 + t1 - 4*Power(t1,2) - 16*t2 + 15*t1*t2) + 
            Power(t1,4)*(-32 + 8*t2 + 9*Power(t2,2)) - 
            2*Power(t1,3)*(-39 - 52*t2 + 3*Power(t2,2) + 
               6*Power(t2,3)) - 
            4*(1 - 3*t2 + 18*Power(t2,2) + 10*Power(t2,3)) + 
            Power(t1,2)*(-43 - 230*t2 - 99*Power(t2,2) - 
               10*Power(t2,3) + 5*Power(t2,4)) + 
            t1*(2 + 105*t2 + 192*Power(t2,2) + 27*Power(t2,3) + 
               10*Power(t2,4)) + 
            Power(s2,3)*(19 + 8*Power(t1,3) + 17*t2 - 81*Power(t2,2) + 
               20*Power(t2,3) - 17*Power(t1,2)*(1 + t2) + 
               t1*(-15 + 95*t2 - 18*Power(t2,2))) + 
            Power(s2,2)*(29 - 5*Power(t1,4) + 5*t2 - 123*Power(t2,2) - 
               131*Power(t2,3) + 15*Power(t2,4) + 
               Power(t1,3)*(25 + 3*t2) + 
               Power(t1,2)*(-37 - 170*t2 + 45*Power(t2,2)) - 
               2*t1*(9 - 60*t2 - 141*Power(t2,2) + 29*Power(t2,3))) + 
            s2*(2 + Power(t1,5) - 81*t2 - 48*Power(t2,2) - 
               107*Power(t2,3) - 70*Power(t2,4) + 
               Power(t1,4)*(-9 + 5*t2) + 
               Power(t1,3)*(87 + 83*t2 - 36*Power(t2,2)) + 
               Power(t1,2)*(-99 - 265*t2 - 219*Power(t2,2) + 
                  50*Power(t2,3)) + 
               t1*(26 + 213*t2 + 270*Power(t2,2) + 221*Power(t2,3) - 
                  20*Power(t2,4)))) + 
         s1*(Power(t1,6) + Power(s2,6)*(-1 + 4*t2) - 
            2*Power(t1,5)*(-14 + 3*t2 + Power(t2,2)) - 
            2*Power(s2,5)*(-8 + 5*t1 - 4*t2 + 3*t1*t2 + 2*Power(t2,2)) + 
            2*Power(t1,4)*(-43 - 66*t2 + 7*Power(t2,2) + 
               3*Power(t2,3)) + 
            Power(t1,3)*(86 + 330*t2 + 206*Power(t2,2) - 
               4*Power(t2,3) - 6*Power(t2,4)) - 
            8*(4 + 2*t2 - 3*Power(t2,2) + 12*Power(t2,3) + 
               5*Power(t2,4)) + 
            Power(t1,2)*(-64 - 208*t2 - 458*Power(t2,2) - 
               128*Power(t2,3) - 15*Power(t2,4) + 2*Power(t2,5)) + 
            2*t1*(34 + 14*t2 + 105*Power(t2,2) + 127*Power(t2,3) + 
               13*Power(t2,4) + 5*Power(t2,5)) + 
            Power(s2,4)*(32 + Power(t1,2)*(39 - 8*t2) + 34*t2 - 
               22*Power(t2,2) + 2*t1*(-42 - 5*t2 + 9*Power(t2,2))) + 
            2*Power(s2,3)*(9 + 10*Power(t1,3)*(-2 + t2) + 43*t2 + 
               13*Power(t2,2) - 57*Power(t2,3) + 10*Power(t2,4) - 
               2*Power(t1,2)*(-25 + 11*t2 + 5*Power(t2,2)) - 
               t1*(33 + 47*t2 - 91*Power(t2,2) + 12*Power(t2,3))) + 
            Power(s2,2)*(44 + Power(t1,4)*(9 - 12*t2) + 84*t2 - 
               16*Power(t2,2) - 162*Power(t2,3) - 133*Power(t2,4) + 
               12*Power(t2,5) + 24*Power(t1,3)*(1 + 4*t2) + 
               2*Power(t1,2)*
                (1 - 81*t2 - 172*Power(t2,2) + 27*Power(t2,3)) + 
               t1*(-82 - 22*t2 + 270*Power(t2,2) + 384*Power(t2,3) - 
                  54*Power(t2,4))) + 
            2*s2*(22 - 6*t2 - 81*Power(t2,2) - 31*Power(t2,3) - 
               53*Power(t2,4) - 29*Power(t2,5) + Power(t1,5)*(1 + t2) + 
               Power(t1,4)*(-42 - 22*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(51 + 193*t2 + 85*Power(t2,2) - 
                  18*Power(t2,3)) + 
               Power(t1,2)*(29 - 237*t2 - 275*Power(t2,2) - 
                  149*Power(t2,3) + 20*Power(t2,4)) + 
               t1*(-62 + 86*t2 + 225*Power(t2,2) + 177*Power(t2,3) + 
                  114*Power(t2,4) - 7*Power(t2,5)))) + 
         2*Power(s,3)*(14 - 4*t1 - 8*Power(t1,2) - Power(t1,3) - 
            2*Power(t1,4) + Power(t1,5) + 
            Power(s2,4)*(-4 + 2*t1 - 3*t2) - 4*t2 + 14*t1*t2 + 
            3*Power(t1,2)*t2 + 10*Power(t1,3)*t2 - 2*Power(t1,4)*t2 - 
            6*Power(t2,2) - 3*t1*Power(t2,2) - 
            18*Power(t1,2)*Power(t2,2) - 2*Power(t1,3)*Power(t2,2) + 
            Power(t2,3) + 14*t1*Power(t2,3) + 
            8*Power(t1,2)*Power(t2,3) - 4*Power(t2,4) - 
            7*t1*Power(t2,4) + 2*Power(t2,5) + 
            Power(s1,4)*(-2 + s2 - 3*t1 + 2*t2) - 
            Power(s1,3)*(3 + 3*Power(s2,2) + 8*Power(t1,2) + 
               t1*(8 - 16*t2) - 2*s2*(5 + 5*t1 - 4*t2) - 10*t2 + 
               8*Power(t2,2)) - 
            Power(s2,3)*(7 + 7*Power(t1,2) + 8*t2 + 3*Power(t2,2) - 
               10*t1*(2 + t2)) + 
            Power(s2,2)*(34 + 9*Power(t1,3) - 3*t2 - 22*Power(t2,2) + 
               3*Power(t2,3) - Power(t1,2)*(22 + 15*t2) + 
               t1*(-27 + 44*t2 + 3*Power(t2,2))) + 
            Power(s1,2)*(2 + 5*Power(s2,3) - 6*Power(t1,3) + 7*t2 - 
               18*Power(t2,2) + 12*Power(t2,3) + 
               12*Power(t1,2)*(-1 + 2*t2) + 
               Power(s2,2)*(-20 - 9*t1 + 9*t2) + 
               t1*(-7 + 30*t2 - 30*Power(t2,2)) + 
               s2*(7 + 12*Power(t1,2) + t1*(28 - 30*t2) - 34*t2 + 
                  18*Power(t2,2))) + 
            s2*(-38 - 5*Power(t1,4) + Power(t1,2)*(21 - 30*t2) + 
               10*t2 + 15*Power(t2,2) - 14*Power(t2,3) + 
               5*Power(t2,4) + 2*Power(t1,3)*(4 + 5*t2) - 
               2*t1*(-8 + 18*t2 - 18*Power(t2,2) + 5*Power(t2,3))) + 
            s1*(-10 + 5*Power(s2,4) + 8*Power(t1,3)*(-1 + t2) + 4*t2 - 
               5*Power(t2,2) + 14*Power(t2,3) - 8*Power(t2,4) - 
               2*Power(s2,3)*(1 + 3*t1 + t2) + 
               Power(t1,2)*(-5 + 30*t2 - 24*Power(t2,2)) + 
               Power(s2,2)*(-5 + 3*Power(t1,2) + 6*t1*(-3 + t2) + 
                  42*t2 - 9*Power(t2,2)) + 
               2*t1*(1 + 5*t2 - 18*Power(t2,2) + 12*Power(t2,3)) - 
               2*s2*(-8 + Power(t1,3) + 11*t2 - 19*Power(t2,2) + 
                  8*Power(t2,3) + Power(t1,2)*(-13 + 6*t2) + 
                  t1*(-2 + 32*t2 - 15*Power(t2,2))))) - 
         Power(s,2)*(48 - 32*t1 - 64*Power(t1,2) + 32*Power(t1,3) + 
            8*Power(t1,4) + 5*Power(t1,5) + 
            2*Power(s2,5)*(-2 + t1 - 3*t2) - 52*t2 + 140*t1*t2 - 
            32*Power(t1,2)*t2 - 22*Power(t1,3)*t2 - 23*Power(t1,4)*t2 - 
            4*Power(t1,5)*t2 - 52*Power(t2,2) - 20*t1*Power(t2,2) + 
            18*Power(t1,2)*Power(t2,2) + 30*Power(t1,3)*Power(t2,2) + 
            14*Power(t1,4)*Power(t2,2) + 20*Power(t2,3) - 
            2*t1*Power(t2,3) - 2*Power(t1,2)*Power(t2,3) - 
            16*Power(t1,3)*Power(t2,3) - 2*Power(t2,4) - 
            19*t1*Power(t2,4) + 4*Power(t1,2)*Power(t2,4) + 
            9*Power(t2,5) + 4*t1*Power(t2,5) - 2*Power(t2,6) + 
            2*Power(s1,5)*(-2 + 2*s2 - 3*t1 + t2) - 
            Power(s1,4)*(2 + 8*Power(s2,2) + 12*Power(t1,2) + 
               t1*(11 - 28*t2) - 2*s2*(6 + 9*t1 - 10*t2) - 25*t2 + 
               10*Power(t2,2)) + 
            Power(s2,4)*(-42 - 8*Power(t1,2) - 21*t2 + 4*Power(t2,2) + 
               5*t1*(11 + 2*t2)) + 
            2*Power(s2,3)*(22 + 6*Power(t1,3) + 
               Power(t1,2)*(-63 + t2) - 18*t2 - 21*Power(t2,2) + 
               7*Power(t2,3) + t1*(35 + 60*t2 - 14*Power(t2,2))) + 
            2*Power(s2,2)*(34 - 4*Power(t1,4) + 
               Power(t1,3)*(50 - 7*t2) + 4*t2 - 24*Power(t2,2) + 
               2*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(31 - 98*t2 + 27*Power(t2,2)) + 
               t1*(-116 + 47*t2 + 46*Power(t2,2) - 17*Power(t2,3))) + 
            2*s2*(-56 + Power(t1,5) + 50*t2 + 58*Power(t2,2) - 
               22*Power(t2,3) + 7*Power(t2,4) - 2*Power(t2,5) + 
               3*Power(t1,4)*(-5 + 2*t2) + 
               Power(t1,3)*(-49 + 56*t2 - 22*Power(t2,2)) + 
               Power(t1,2)*(54 + 58*t2 - 60*Power(t2,2) + 
                  20*Power(t2,3)) + 
               t1*(70 - 154*t2 + 13*Power(t2,2) + 12*Power(t2,3) - 
                  3*Power(t2,4))) + 
            2*Power(s1,3)*(2*Power(t1,2)*(-1 + 8*t2) + 
               Power(s2,2)*(2 + 2*t1 + 11*t2) + 
               t1*(7 + 26*t2 - 26*Power(t2,2)) + 
               2*(-6 + 2*t2 - 15*Power(t2,2) + 5*Power(t2,3)) - 
               s2*(-6 + 2*Power(t1,2) + 25*t2 - 20*Power(t2,2) + 
                  3*t1*(1 + 8*t2))) + 
            2*Power(s1,2)*(-8 + 10*Power(s2,4) + 6*Power(t1,4) + 
               Power(t1,3)*(7 - 8*t2) + 34*t2 - 6*Power(t2,2) + 
               35*Power(t2,3) - 10*Power(t2,4) + 
               Power(s2,3)*(-16 - 24*t1 + 7*t2) + 
               Power(s2,2)*(-50 + 30*Power(t1,2) + t1*(48 - 21*t2) - 
                  2*t2 - 9*Power(t2,2)) + 
               3*Power(t1,2)*(7 + t2 - 4*Power(t2,2)) + 
               t1*(-38 - 15*t2 - 45*Power(t2,2) + 24*Power(t2,3)) + 
               s2*(70 - 22*Power(t1,3) - 34*t2 + 39*Power(t2,2) - 
                  20*Power(t2,3) + 3*Power(t1,2)*(-13 + 8*t2) + 
                  t1*(5 + 18*t2 + 18*Power(t2,2)))) + 
            2*s1*(2 + 4*Power(s2,5) + 3*Power(t1,5) + 
               Power(t1,4)*(8 - 13*t2) + 34*t2 - 32*Power(t2,2) + 
               4*Power(t2,3) - 20*Power(t2,4) + 5*Power(t2,5) - 
               3*Power(s2,4)*(-2 + t1 + 4*t2) - 
               2*Power(t1,2)*(11 + 15*t2) - 
               Power(s2,3)*(6 + 12*Power(t1,2) + t1*(25 - 38*t2) - 
                  37*t2 + 14*Power(t2,2)) + 
               Power(t1,3)*(17 - 22*t2 + 16*Power(t2,2)) + 
               t1*(-10 + 48*t2 + 9*Power(t2,2) + 34*Power(t2,3) - 
                  11*Power(t2,4)) + 
               Power(s2,2)*(28 + 20*Power(t1,3) + 
                  Power(t1,2)*(60 - 57*t2) + 74*t2 - 2*Power(t2,2) + 
                  Power(t2,3) + t1*(-49 - 94*t2 + 36*Power(t2,2))) - 
               s2*(38 + 12*Power(t1,4) + Power(t1,3)*(45 - 44*t2) + 
                  128*t2 - 50*Power(t2,2) + 27*Power(t2,3) - 
                  10*Power(t2,4) + t1*(-88 + 18*t2 + 27*Power(t2,2)) + 
                  Power(t1,2)*(14 - 99*t2 + 42*Power(t2,2))))) + 
         s*(24 + 2*Power(s1,6)*(s2 - t1) - 20*t1 - 76*Power(t1,2) + 
            114*Power(t1,3) - 39*Power(t1,4) - Power(t1,5) - 
            Power(t1,6) - 76*t2 - 2*Power(s2,6)*t2 + 208*t1*t2 - 
            186*Power(t1,2)*t2 + 72*Power(t1,3)*t2 - 15*Power(t1,4)*t2 + 
            3*Power(t1,5)*t2 - 48*Power(t2,2) + 30*t1*Power(t2,2) - 
            62*Power(t1,2)*Power(t2,2) + 56*Power(t1,3)*Power(t2,2) + 
            8*Power(t1,4)*Power(t2,2) + 2*Power(t1,5)*Power(t2,2) + 
            26*Power(t2,3) + 52*t1*Power(t2,3) - 
            68*Power(t1,2)*Power(t2,3) - 30*Power(t1,3)*Power(t2,3) - 
            8*Power(t1,4)*Power(t2,3) - 23*Power(t2,4) + 
            33*t1*Power(t2,4) + 27*Power(t1,2)*Power(t2,4) + 
            12*Power(t1,3)*Power(t2,4) - 5*Power(t2,5) - 
            5*t1*Power(t2,5) - 8*Power(t1,2)*Power(t2,5) - 
            2*Power(t2,6) + 2*t1*Power(t2,6) + 
            Power(s2,5)*(-16 - 6*t2 + 6*Power(t2,2) + t1*(15 + 2*t2)) + 
            Power(s1,5)*(6 + 5*t1 + 2*t2 + 8*t1*t2 - s2*(11 + 10*t2)) + 
            Power(s2,4)*(-13 + 17*t2 + 14*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(-53 + 8*t2) + t1*(71 - 5*t2 - 16*Power(t2,2))\
) - 2*Power(s2,3)*(-33 + 7*t2 - 21*Power(t2,2) - 30*Power(t2,3) + 
               5*Power(t2,4) + Power(t1,3)*(-31 + 8*t2) + 
               Power(t1,2)*(18 - 20*t2 - 7*Power(t2,2)) + 
               t1*(51 + 16*t2 + 61*Power(t2,2) - 6*Power(t2,3))) + 
            2*Power(s2,2)*(8 + 29*t2 + 16*Power(t2,2) + 36*Power(t2,3) + 
               20*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,4)*(-13 + 5*t2) - 
               Power(t1,3)*(51 + 19*t2 + Power(t2,2)) + 
               Power(t1,2)*(156 + 43*t2 + 97*Power(t2,2) - 
                  15*Power(t2,3)) + 
               t1*(-95 - 50*t2 - 70*Power(t2,2) - 85*Power(t2,3) + 
                  13*Power(t2,4))) + 
            s2*(Power(t1,5)*(3 - 2*t2) + 
               Power(t1,4)*(84 + 6*t2 - 4*Power(t2,2)) + 
               2*Power(t1,3)*
                (-79 - 40*t2 - 43*Power(t2,2) + 12*Power(t2,3)) - 
               2*Power(t1,2)*
                (7 - 123*t2 + 17*Power(t2,2) - 78*Power(t2,3) + 
                  14*Power(t2,4)) + 
               2*(-26 + 50*t2 + 23*Power(t2,2) - 49*Power(t2,3) + 
                  11*Power(t2,4) + 7*Power(t2,5)) + 
               t1*(132 - 284*t2 + 70*Power(t2,2) + 8*Power(t2,3) - 
                  93*Power(t2,4) + 10*Power(t2,5))) + 
            Power(s1,4)*(-25 - 10*Power(s2,3) + 12*Power(t1,3) + 
               Power(t1,2)*(19 - 8*t2) + 
               Power(s2,2)*(40 + 30*t1 - 4*t2) - 29*t2 - 
               10*Power(t2,2) + t1*(37 - 25*t2 - 10*Power(t2,2)) + 
               s2*(20 - 32*Power(t1,2) + 58*t2 + 20*Power(t2,2) + 
                  t1*(-77 + 10*t2))) + 
            2*Power(s1,3)*(-13 + 5*Power(s2,4) + 8*Power(t1,4) + 
               Power(t1,3)*(13 - 24*t2) + 49*t2 + 28*Power(t2,2) + 
               10*Power(t2,3) + Power(s2,3)*(-17 - 20*t1 + 20*t2) + 
               Power(s2,2)*(-54 + 33*Power(t1,2) + t1*(77 - 58*t2) - 
                  80*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(31 - 42*t2 + 16*Power(t2,2)) + 
               t1*(-25 - 72*t2 + 25*Power(t2,2)) - 
               s2*(-34 + 26*Power(t1,3) + Power(t1,2)*(75 - 62*t2) + 
                  41*t2 + 61*Power(t2,2) + 10*Power(t2,3) + 
                  t1*(-25 - 162*t2 + 20*Power(t2,2)))) + 
            2*Power(s1,2)*(-14 + 6*Power(s2,5) + 3*Power(t1,5) + 
               Power(t1,4)*(7 - 20*t2) + 
               Power(s2,4)*(10 - 12*t1 - 9*t2) + 39*t2 - 
               72*Power(t2,2) - 27*Power(t2,3) - 10*Power(t2,4) + 
               Power(t1,3)*(18 - 41*t2 + 36*Power(t2,2)) - 
               Power(t1,2)*(2 + 96*t2 - 69*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(-21 + 76*t2 + 105*Power(t2,2) - 25*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,3)*(-4 + 3*Power(t1,2) + 64*t2 - 
                  30*Power(t2,2) + t1*(-45 + 46*t2)) + 
               Power(s2,2)*(9*Power(t1,3) + Power(t1,2)*(87 - 81*t2) + 
                  t1*(-54 - 239*t2 + 84*Power(t2,2)) + 
                  3*(5 + 48*t2 + 40*Power(t2,2) - 4*Power(t2,3))) + 
               s2*(57 - 9*Power(t1,4) - 117*t2 + 63*Power(t2,2) + 
                  64*Power(t2,3) + 5*Power(t2,4) + 
                  Power(t1,3)*(-55 + 64*t2) + 
                  Power(t1,2)*(20 + 228*t2 - 90*Power(t2,2)) + 
                  t1*(-19 - 46*t2 - 255*Power(t2,2) + 30*Power(t2,3)))) + 
            s1*(52 + 2*Power(s2,6) + Power(s2,5)*(5 - 18*t2) + 
               Power(t1,5)*(1 - 8*t2) + 76*t2 - 78*Power(t2,2) + 
               94*Power(t2,3) + 26*Power(t2,4) + 10*Power(t2,5) + 
               Power(t1,4)*(4 - 22*t2 + 32*Power(t2,2)) - 
               2*Power(t1,3)*(1 + 46*t2 - 43*Power(t2,2) + 
                  24*Power(t2,3)) + 
               2*Power(t1,2)*(15 + 33*t2 + 99*Power(t2,2) - 
                  50*Power(t2,3) + 16*Power(t2,4)) - 
               t1*(92 - 12*t2 + 154*Power(t2,2) + 136*Power(t2,3) - 
                  25*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,4)*(-38 - 16*Power(t1,2) - 34*t2 + 
                  6*Power(t2,2) + t1*(29 + 40*t2)) + 
               2*Power(s2,3)*(10 + 14*Power(t1,3) - 17*t2 - 
                  77*Power(t2,2) + 20*Power(t2,3) - 
                  Power(t1,2)*(47 + 10*t2) + 
                  t1*(39 + 106*t2 - 32*Power(t2,2))) - 
               2*Power(s2,2)*(-3 + 9*Power(t1,4) + 31*t2 + 
                  126*Power(t2,2) + 80*Power(t2,3) - 8*Power(t2,4) + 
                  Power(t1,3)*(-41 + 8*t2) + 
                  Power(t1,2)*(33 + 184*t2 - 63*Power(t2,2)) + 
                  t1*(15 - 124*t2 - 247*Power(t2,2) + 54*Power(t2,3))) + 
               s2*(-144 + 4*Power(t1,5) - 160*t2 + 264*Power(t2,2) - 
                  86*Power(t2,3) - 67*Power(t2,4) - 2*Power(t2,5) + 
                  Power(t1,4)*(-23 + 22*t2) + 
                  Power(t1,3)*(46 + 196*t2 - 100*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (-84 - 3*t2 - 231*Power(t2,2) + 58*Power(t2,3)) + 
                  t1*(304 - 32*t2 + 34*Power(t2,2) + 356*Power(t2,3) - 
                     40*Power(t2,4))))))*
       T3(1 - s + s1 - t2,1 - s + s2 - t1))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(-1 + s1 + t1 - t2)*
       Power(-s1 + s2 - t1 + t2,2)*(1 - s + s*s2 - s1*s2 - t1 + s2*t2)) - 
    (8*(-32 + 16*s2 + 96*t1 - 48*s2*t1 - 96*Power(t1,2) + 
         48*s2*Power(t1,2) + 32*Power(t1,3) - 16*s2*Power(t1,3) - 
         2*Power(s1,5)*(Power(s2,2) + Power(t1,2) - 2*s2*(2 + t1)) + 
         2*Power(s,6)*(t1 - t2) - 8*t2 - 6*s2*t2 + 16*Power(s2,2)*t2 + 
         14*t1*t2 + 12*s2*t1*t2 - 32*Power(s2,2)*t1*t2 - 
         4*Power(t1,2)*t2 - 6*s2*Power(t1,2)*t2 + 
         16*Power(s2,2)*Power(t1,2)*t2 - 2*Power(t1,3)*t2 + 
         8*Power(t2,2) + 4*s2*Power(t2,2) + 27*Power(s2,2)*Power(t2,2) + 
         12*t1*Power(t2,2) - 10*s2*t1*Power(t2,2) - 
         27*Power(s2,2)*t1*Power(t2,2) - 25*Power(t1,2)*Power(t2,2) + 
         6*s2*Power(t1,2)*Power(t2,2) + 5*Power(t1,3)*Power(t2,2) - 
         24*Power(t2,3) + 9*s2*Power(t2,3) + 9*Power(s2,2)*Power(t2,3) + 
         Power(s2,3)*Power(t2,3) + 39*t1*Power(t2,3) + 
         23*s2*t1*Power(t2,3) - 2*Power(s2,2)*t1*Power(t2,3) - 
         16*Power(t1,2)*Power(t2,3) - 7*s2*Power(t1,2)*Power(t2,3) - 
         8*Power(t2,4) - 21*s2*Power(t2,4) + 3*Power(s2,2)*Power(t2,4) - 
         2*Power(s2,3)*Power(t2,4) + 5*t1*Power(t2,4) + 
         11*s2*t1*Power(t2,4) + 2*Power(s2,2)*t1*Power(t2,4) + 
         2*Power(t1,2)*Power(t2,4) - 10*s2*Power(t2,5) + 
         2*Power(s2,2)*Power(t2,5) + 2*t1*Power(t2,5) - 
         2*s2*t1*Power(t2,5) + 
         2*Power(s,5)*(1 + Power(t1,2) + 4*t2 + s2*t2 - 4*Power(t2,2) + 
            t1*(-2 - 2*s2 + 3*t2) + s1*(-2 + s2 - 5*t1 + 4*t2)) + 
         Power(s,4)*(-4 + 6*t1 - 5*Power(t1,2) - 4*t2 + 
            8*Power(t1,2)*t2 + 25*Power(t2,2) + 4*t1*Power(t2,2) - 
            12*Power(t2,3) + 2*Power(s2,2)*(t1 + t2) - 
            4*Power(s1,2)*(-3 + 2*s2 - 5*t1 + 3*t2) - 
            s2*(2 - 5*t1 + 2*Power(t1,2) + 11*t2 + 14*t1*t2 - 
               6*Power(t2,2)) + 
            s1*(2 - 4*Power(s2,2) + 7*t1 - 10*Power(t1,2) - 37*t2 - 
               24*t1*t2 + 24*Power(t2,2) + 2*s2*(2 + 10*t1 + t2))) + 
         Power(s1,4)*(-8 - 2*Power(s2,3) + 2*t1*(3 + t2) + 
            Power(t1,2)*(1 + 8*t2) + Power(s2,2)*(-1 + 4*t1 + 10*t2) - 
            2*s2*(11 + Power(t1,2) + 21*t2 + t1*(-8 + 9*t2))) + 
         Power(s1,3)*(Power(t1,3) + 8*(3 + 4*t2) + 
            Power(s2,3)*(-1 + 8*t2) + 
            Power(s2,2)*(-8 + t1 - 14*t1*t2 - 20*Power(t2,2)) + 
            Power(t1,2)*(17 - 5*t2 - 12*Power(t2,2)) - 
            t1*(41 + 23*t2 + 8*Power(t2,2)) + 
            s2*(-7 + 87*t2 + 88*Power(t2,2) + Power(t1,2)*(7 + 6*t2) + 
               t1*(-25 - 59*t2 + 32*Power(t2,2)))) + 
         Power(s1,2)*(Power(t1,3)*(7 - 2*t2) + 
            3*Power(s2,3)*(1 - 4*t2)*t2 - 
            8*(-1 + 9*t2 + 6*Power(t2,2)) + 
            Power(t1,2)*(-29 - 50*t2 + 9*Power(t2,2) + 8*Power(t2,3)) + 
            t1*(14 + 121*t2 + 33*Power(t2,2) + 12*Power(t2,3)) + 
            Power(s2,2)*(27 + 25*t2 + 6*Power(t2,2) + 20*Power(t2,3) + 
               t1*(-27 - 4*t2 + 18*Power(t2,2))) + 
            s2*(2 + 23*t2 - 129*Power(t2,2) - 92*Power(t2,3) + 
               Power(t1,2)*(4 - 21*t2 - 6*Power(t2,2)) + 
               t1*(-6 + 73*t2 + 81*Power(t2,2) - 28*Power(t2,3)))) + 
         s1*(Power(s2,3)*Power(t2,2)*(-3 + 8*t2) + 
            Power(t1,3)*(2 - 12*t2 + Power(t2,2)) + 
            8*(1 - 2*t2 + 9*Power(t2,2) + 4*Power(t2,3)) + 
            Power(t1,2)*(4 + 54*t2 + 49*Power(t2,2) - 7*Power(t2,3) - 
               2*Power(t2,4)) - 
            t1*(14 + 26*t2 + 119*Power(t2,2) + 21*Power(t2,3) + 
               8*Power(t2,4)) - 
            Power(s2,2)*(16*Power(t1,2) + 
               t1*(-32 - 54*t2 - 5*Power(t2,2) + 10*Power(t2,3)) + 
               2*(8 + 27*t2 + 13*Power(t2,2) + 4*Power(t2,3) + 
                  5*Power(t2,4))) + 
            s2*(6 - 6*t2 - 25*Power(t2,2) + 85*Power(t2,3) + 
               48*Power(t2,4) + 
               Power(t1,2)*(6 - 10*t2 + 21*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-12 + 16*t2 - 71*Power(t2,2) - 49*Power(t2,3) + 
                  12*Power(t2,4)))) + 
         Power(s,3)*(4 - 19*t1 + 15*Power(t1,2) - Power(t1,3) + 11*t2 - 
            2*Power(s2,3)*t2 - 24*t1*t2 - 10*Power(t1,2)*t2 - 
            3*Power(t2,2) + 23*t1*Power(t2,2) + 
            12*Power(t1,2)*Power(t2,2) + 28*Power(t2,3) - 
            4*t1*Power(t2,3) - 8*Power(t2,4) + 
            4*Power(s1,3)*(-3 + 3*s2 - 5*t1 + 2*t2) + 
            Power(s2,2)*(-8 - 6*t2 + 8*Power(t2,2) + t1*(7 + 8*t2)) + 
            Power(s1,2)*(14*Power(s2,2) + 20*Power(t1,2) + 
               4*(13 - 6*t2)*t2 - s2*(19 + 40*t1 + 18*t2) + 
               t1*(3 + 36*t2)) + 
            s2*(11 + 5*t2 - 42*Power(t2,2) + 6*Power(t2,3) - 
               6*Power(t1,2)*(1 + t2) + t1*(-3 + 24*t2 - 20*Power(t2,2))\
) + s1*(-9 + 2*Power(s2,3) + Power(t1,2)*(14 - 32*t2) + 
               Power(s2,2)*(9 - 10*t1 - 22*t2) + 3*t2 - 68*Power(t2,2) + 
               24*Power(t2,3) + t1*(19 - 26*t2 - 12*Power(t2,2)) + 
               s2*(-4 + 8*Power(t1,2) + 61*t2 + t1*(-29 + 60*t2)))) - 
         Power(s,2)*(58 - 56*t1 + 9*Power(t1,2) - 11*Power(t1,3) + 
            32*t2 + 37*t1*t2 + 4*Power(t1,2)*t2 + 2*Power(t1,3)*t2 - 
            46*Power(t2,2) + 43*t1*Power(t2,2) + 
            3*Power(t1,2)*Power(t2,2) - 8*Power(t2,3) - 
            32*t1*Power(t2,3) - 8*Power(t1,2)*Power(t2,3) - 
            13*Power(t2,4) + 6*t1*Power(t2,4) + 2*Power(t2,5) + 
            2*Power(s1,4)*(-2 + 4*s2 - 5*t1 + t2) + 
            Power(s2,3)*(-8 + 8*t1 - 9*t2 + 6*Power(t2,2)) + 
            Power(s1,3)*(10 + 18*Power(s2,2) + 20*Power(t1,2) + 25*t2 - 
               8*Power(t2,2) - 2*s2*(17 + 20*t1 + 11*t2) + 
               t1*(11 + 24*t2)) + 
            Power(s2,2)*(31 - 8*Power(t1,2) + 41*t2 + Power(t2,2) - 
               12*Power(t2,3) + t1*(-23 + 4*t2 - 12*Power(t2,2))) + 
            s2*(-68 - 33*t2 + 45*Power(t2,2) + 61*Power(t2,3) - 
               2*Power(t2,4) + 
               Power(t1,2)*(34 + 19*t2 + 6*Power(t2,2)) + 
               t1*(34 - 97*t2 - 44*Power(t2,2) + 16*Power(t2,3))) + 
            Power(s1,2)*(-46 + 6*Power(s2,3) + 
               Power(t1,2)*(12 - 48*t2) + 
               Power(s2,2)*(11 - 18*t1 - 48*t2) - 28*t2 - 
               51*Power(t2,2) + 12*Power(t2,3) - 
               2*t1*(-19 + 27*t2 + 6*Power(t2,2)) + 
               s2*(46 + 12*Power(t1,2) + 129*t2 + 18*Power(t2,2) + 
                  t1*(-59 + 96*t2))) - 
            s1*(30 + 3*Power(t1,3) - 92*t2 - 26*Power(t2,2) - 
               43*Power(t2,3) + 8*Power(t2,4) + 
               3*Power(s2,3)*(-3 + 4*t2) + 
               Power(t1,2)*(-1 + 15*t2 - 36*Power(t2,2)) + 
               3*Power(s2,2)*
                (14 + t1 + 4*t2 - 10*t1*t2 - 14*Power(t2,2)) + 
               t1*(43 + 81*t2 - 75*Power(t2,2) + 8*Power(t2,3)) + 
               s2*(-35 + 91*t2 + 156*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t1,2)*(19 + 18*t2) + 
                  t1*(-95 - 103*t2 + 72*Power(t2,2))))) + 
         s*(88 + 2*Power(s1,5)*(s2 - t1) - 162*t1 + 60*Power(t1,2) + 
            14*Power(t1,3) + 30*t2 + 36*t1*t2 - 82*Power(t1,2)*t2 + 
            16*Power(t1,3)*t2 - 60*Power(t2,2) + 69*t1*Power(t2,2) - 
            35*Power(t1,2)*Power(t2,2) - Power(t1,3)*Power(t2,2) + 
            7*Power(t2,3) - 8*t1*Power(t2,3) + 
            4*Power(t1,2)*Power(t2,3) + 5*Power(t2,4) + 
            15*t1*Power(t2,4) + 2*Power(t1,2)*Power(t2,4) + 
            2*Power(t2,5) - 2*t1*Power(t2,5) + 
            2*Power(s2,3)*t2*(4 - 4*t1 + 5*t2 - 3*Power(t2,2)) + 
            Power(s1,4)*(6 + 10*Power(s2,2) + 5*t1 + 10*Power(t1,2) + 
               2*t2 + 6*t1*t2 - s2*(27 + 20*t1 + 8*t2)) + 
            Power(s2,2)*(8*Power(t1,2)*(3 + t2) + 
               t1*(-48 - 20*t2 - 13*Power(t2,2) + 8*Power(t2,3)) + 
               4*(6 + 3*t2 - 4*Power(t2,2) + 2*Power(t2,3) + 
                  2*Power(t2,4))) - 
            s2*(8*Power(t1,3) + 
               2*Power(t1,2)*
                (27 + 2*t2 + 10*Power(t2,2) + Power(t2,3)) + 
               t1*(-132 - 4*t2 - 99*Power(t2,2) - 36*Power(t2,3) + 
                  8*Power(t2,4)) + 
               5*(14 - 3*Power(t2,2) + 13*Power(t2,3) + 8*Power(t2,4))) + 
            Power(s1,3)*(-9 + 6*Power(s2,3) + 
               Power(s2,2)*(3 - 14*t1 - 38*t2) + 
               Power(t1,2)*(2 - 32*t2) - 23*t2 - 8*Power(t2,2) + 
               t1*(7 - 30*t2 - 4*Power(t2,2)) + 
               s2*(66 + 8*Power(t1,2) + 121*t2 + 12*Power(t2,2) + 
                  17*t1*(-3 + 4*t2))) + 
            Power(s1,2)*(-58 - 3*Power(t1,3) + 25*t2 + 33*Power(t2,2) + 
               12*Power(t2,3) - 2*Power(s2,3)*(-5 + 9*t2) + 
               Power(t1,2)*(-31 + 36*Power(t2,2)) + 
               t1*(65 - 22*t2 + 60*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,2)*(t1*(-11 + 36*t2) + 
                  2*(-9 + t2 + 27*Power(t2,2))) - 
               s2*(-15 + 197*t2 + 201*Power(t2,2) + 8*Power(t2,3) + 
                  2*Power(t1,2)*(10 + 9*t2) + 
                  3*t1*(-33 - 46*t2 + 28*Power(t2,2)))) + 
            s1*(-30 + 118*t2 - 23*Power(t2,2) - 21*Power(t2,3) - 
               8*Power(t2,4) + 2*Power(t1,3)*(-9 + 2*t2) + 
               2*Power(s2,3)*(-4 + 4*t1 - 10*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(86 + 66*t2 - 6*Power(t2,2) - 
                  16*Power(t2,3)) + 
               t1*(-38 - 134*t2 + 23*Power(t2,2) - 50*Power(t2,3) + 
                  6*Power(t2,4)) - 
               Power(s2,2)*(12 + 8*Power(t1,2) - 34*t2 + 
                  13*Power(t2,2) + 34*Power(t2,3) + 
                  t1*(-20 - 24*t2 + 30*Power(t2,2))) + 
               s2*(2 - 30*t2 + 196*Power(t2,2) + 147*Power(t2,3) + 
                  2*Power(t2,4) + 
                  2*Power(t1,2)*(3 + 20*t2 + 6*Power(t2,2)) + 
                  t1*(-8 - 198*t2 - 123*Power(t2,2) + 44*Power(t2,3))))))*
       T4(-s + s1 - t2))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(-1 + s1 + t1 - t2)*(s - s1 + t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)) - 
    (8*(8 - 26*s2 - 3*Power(s2,2) - 22*t1 + 58*s2*t1 + 6*Power(s2,2)*t1 + 
         25*Power(t1,2) - 38*s2*Power(t1,2) - 3*Power(s2,2)*Power(t1,2) - 
         16*Power(t1,3) + 6*s2*Power(t1,3) + 5*Power(t1,4) - 
         2*Power(s1,4)*(Power(s2,2) + Power(t1,2) - 2*s2*(2 + t1)) + 
         2*Power(s,4)*(t1 - t2)*(-1 + s1 + t1 - t2) + 16*t2 + 5*s2*t2 - 
         30*Power(s2,2)*t2 - 3*Power(s2,3)*t2 - 69*t1*t2 - 4*s2*t1*t2 + 
         34*Power(s2,2)*t1*t2 + 3*Power(s2,3)*t1*t2 + 74*Power(t1,2)*t2 + 
         6*s2*Power(t1,2)*t2 - 4*Power(s2,2)*Power(t1,2)*t2 - 
         21*Power(t1,3)*t2 - 7*s2*Power(t1,3)*t2 + 32*Power(t2,2) + 
         12*s2*Power(t2,2) - 2*Power(s2,2)*Power(t2,2) - 
         5*Power(s2,3)*Power(t2,2) - 52*t1*Power(t2,2) - 
         49*s2*t1*Power(t2,2) + 9*Power(s2,2)*t1*Power(t2,2) - 
         2*Power(s2,3)*t1*Power(t2,2) + 19*Power(t1,2)*Power(t2,2) + 
         18*s2*Power(t1,2)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         2*Power(t1,3)*Power(t2,2) + 8*Power(t2,3) + 31*s2*Power(t2,3) - 
         5*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) - 
         7*t1*Power(t2,3) - 19*s2*t1*Power(t2,3) - 
         2*s2*Power(t1,2)*Power(t2,3) + 10*s2*Power(t2,4) - 
         2*Power(s2,2)*Power(t2,4) - 2*t1*Power(t2,4) + 
         2*s2*t1*Power(t2,4) + 
         Power(s1,3)*(-8 - 2*Power(s2,3) - 2*Power(t1,3) + 
            2*s2*(-15 + Power(t1,2) + t1*(10 - 7*t2) - 17*t2) + 
            2*t1*(3 + t2) + Power(t1,2)*(3 + 6*t2) + 
            Power(s2,2)*(1 + 2*t1 + 8*t2)) + 
         2*Power(s,3)*(7 + 4*Power(s2,2) + t1 - 3*Power(t1,2) + 
            Power(t1,3) - 3*t2 + 5*t1*t2 - 2*Power(t2,2) - 
            3*t1*Power(t2,2) + 2*Power(t2,3) + 
            Power(s1,2)*(-2 + s2 - 3*t1 + 2*t2) - 
            s1*(s2*(-3 + t1) + t1 + 2*Power(t1,2) + Power(1 - 2*t2,2) - 
               6*t1*t2) - s2*(12 + 2*Power(t1,2) + 3*t2 + Power(t2,2) - 
               t1*(4 + 3*t2))) - 
         Power(s1,2)*(Power(s2,3)*(5 + 2*t1 - 6*t2) - 
            2*Power(t1,3)*(1 + 2*t2) - 8*(4 + 3*t2) + 
            Power(t1,2)*(-22 + 6*t2 + 6*Power(t2,2)) + 
            t1*(55 + 19*t2 + 6*Power(t2,2)) + 
            Power(s2,2)*(3 - 4*Power(t1,2) + 4*t1*(-2 + t2) + 7*t2 + 
               12*Power(t2,2)) + 
            s2*(-15 + 2*Power(t1,3) - 91*t2 - 54*Power(t2,2) + 
               Power(t1,2)*(-19 + 6*t2) + t1*(51 + 59*t2 - 18*Power(t2,2))\
)) - Power(s,2)*(16 + 8*Power(s2,3) - 52*t1 + 3*Power(t1,2) + 
            5*Power(t1,3) + 10*t2 + 2*t1*t2 - 9*Power(t1,2)*t2 - 
            4*Power(t1,3)*t2 + 7*Power(t2,2) - 3*t1*Power(t2,2) + 
            6*Power(t1,2)*Power(t2,2) + 7*Power(t2,3) - 2*Power(t2,4) + 
            2*Power(s1,3)*(-2 + 2*s2 - 3*t1 + t2) - 
            2*Power(s2,2)*(22 - t1 + Power(t1,2) + 9*t2 - Power(t2,2)) + 
            Power(s1,2)*(2 + 4*Power(s2,2) + 3*t1 + 15*t2 + 12*t1*t2 - 
               6*Power(t2,2) - 2*s2*(2 + 4*t1 + 3*t2)) + 
            s1*(-18 + 17*t1 + 6*Power(t1,3) + 
               2*Power(s2,2)*(10 + t1 - 3*t2) - 9*t2 - 
               6*Power(t1,2)*t2 - 18*Power(t2,2) - 6*t1*Power(t2,2) + 
               6*Power(t2,3) + 
               s2*(-6 - 15*t1 - 10*Power(t1,2) + 15*t2 + 16*t1*t2)) + 
            s2*(22 + 2*Power(t1,3) - 3*t2 - 11*Power(t2,2) + 
               2*Power(t2,3) + Power(t1,2)*(-17 + 4*t2) + 
               t1*(57 + 22*t2 - 8*Power(t2,2)))) + 
         s1*(Power(t1,4) + Power(t1,3)*(17 - 4*t2 - 2*Power(t2,2)) - 
            8*(2 + 8*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(-69 - 41*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
            t1*(67 + 107*t2 + 20*Power(t2,2) + 6*Power(t2,3)) + 
            Power(s2,3)*(3 + 10*t2 - 6*Power(t2,2) + t1*(-3 + 4*t2)) + 
            Power(s2,2)*(31 + Power(t1,2)*(5 - 6*t2) + 5*t2 + 
               11*Power(t2,2) + 8*Power(t2,3) + 
               t1*(-36 - 17*t2 + 2*Power(t2,2))) + 
            s2*(-3 - 27*t2 - 92*Power(t2,2) - 38*Power(t2,3) + 
               Power(t1,3)*(5 + 2*t2) + Power(t1,2)*t2*(-37 + 6*t2) - 
               2*t1*(1 - 50*t2 - 29*Power(t2,2) + 5*Power(t2,3)))) + 
         s*(-6 + 2*Power(s1,4)*(s2 - t1) - 23*t1 + 30*Power(t1,2) - 
            Power(t1,4) - 5*t2 + 40*t1*t2 - 17*Power(t1,2)*t2 + 
            Power(t1,3)*t2 - 28*Power(t2,2) + 12*t1*Power(t2,2) + 
            9*Power(t1,2)*Power(t2,2) + 2*Power(t1,3)*Power(t2,2) - 
            7*Power(t2,3) - 7*t1*Power(t2,3) - 4*Power(t1,2)*Power(t2,3) - 
            2*Power(t2,4) + 2*t1*Power(t2,4) - 
            2*Power(s2,3)*(2 + t1*(-2 + t2) + 7*t2 - Power(t2,2)) + 
            Power(s1,3)*(6 + 6*Power(s2,2) + 7*t1 + 4*Power(t1,2) + 
               2*t2 + 4*t1*t2 - s2*(13 + 10*t1 + 6*t2)) + 
            Power(s2,2)*(Power(t1,2)*(-11 + 4*t2) + t1*(43 + 21*t2) - 
               2*(16 - 15*t2 + Power(t2,2) + 2*Power(t2,3))) + 
            s2*(67 + 24*t2 - 2*Power(t1,3)*t2 + 49*Power(t2,2) + 
               20*Power(t2,3) + Power(t1,2)*(9 + 14*t2 - 2*Power(t2,2)) + 
               t1*(-76 - 94*t2 - 28*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,2)*(-31 + 2*Power(s2,3) + 6*Power(t1,3) + 
               t1*(20 - 21*t2) + Power(s2,2)*(5 - 16*t2) + 
               Power(t1,2)*(3 - 12*t2) - 19*t2 - 6*Power(t2,2) + 
               s2*(45 - 8*Power(t1,2) + 46*t2 + 6*Power(t2,2) + 
                  4*t1*(-7 + 6*t2))) + 
            s1*(3 + Power(t1,3)*(3 - 8*t2) + 
               2*Power(s2,3)*(7 + t1 - 2*t2) + 59*t2 + 20*Power(t2,2) + 
               6*Power(t2,3) - 
               Power(s2,2)*(31 + 18*t1 + 6*Power(t1,2) + 3*t2 - 
                  14*Power(t2,2)) + 
               2*Power(t1,2)*(1 - 6*t2 + 6*Power(t2,2)) - 
               t1*(27 + 32*t2 - 21*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-33 + 4*Power(t1,3) - 94*t2 - 53*Power(t2,2) - 
                  2*Power(t2,3) + Power(t1,2)*(-21 + 10*t2) + 
                  t1*(108 + 56*t2 - 18*Power(t2,2))))))*T5(1 - s1 - t1 + t2)\
)/((-1 + s2)*Power(-s + s2 - t1,2)*(-1 + s1 + t1 - t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)));
   return a;
};
