#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m213_5_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(8*Power(s,6)*s1*(-1 + t1)*(-1 + t2)*Power(t2,2)*
          (s1 + Power(s1,2) - 3*s1*t2 + t2*(-1 + 2*t2)) - 
         Power(s,5)*t2*(2*Power(-1 + t2,2)*Power(t2,2)*(1 + t2) + 
            8*Power(s1,4)*(-1 + t1)*(1 - 5*t2 + 4*Power(t2,2)) + 
            Power(s1,3)*(-8 + (98 + 4*s2)*t2 + 
               (-209 + 13*s2)*Power(t2,2) + (118 - 5*s2)*Power(t2,3) + 
               Power(t2,4) + Power(t1,2)*(1 + 17*t2 - 6*Power(t2,2)) + 
               t1*(10 - (100 + 21*s2)*t2 + 9*(21 + s2)*Power(t2,2) - 
                  111*Power(t2,3))) + 
            s1*t2*(10 - 2*(17 + 4*s2)*t2 + 4*(22 + 5*s2)*Power(t2,2) + 
               (-104 + 5*s2)*Power(t2,3) - 5*(-8 + s2)*Power(t2,4) + 
               Power(t1,2)*(5 - 15*t2 + 34*Power(t2,2) - 
                  12*Power(t2,3)) + 
               t1*(-18 + (55 + 8*s2)*t2 - (105 + 37*s2)*Power(t2,2) + 
                  (88 + 17*s2)*Power(t2,3) - 32*Power(t2,4))) + 
            Power(s1,2)*(Power(t1,2)*
                (1 + 8*t2 - 57*Power(t2,2) + 24*Power(t2,3)) + 
               t1*t2*(-41 + 209*t2 - 261*Power(t2,2) + 117*Power(t2,3) + 
                  s2*(-8 + 58*t2 - 26*Power(t2,2))) - 
               t2*(-26 + 182*t2 - 289*Power(t2,2) + 132*Power(t2,3) + 
                  Power(t2,4) - 
                  2*s2*(4 - 12*t2 - 9*Power(t2,2) + 5*Power(t2,3))))) + 
         Power(s,4)*(-2*Power(-1 + t2,2)*Power(t2,3)*
             (-5 - 4*t2 + Power(t2,2)) + 
            24*Power(s1,5)*(-1 + t1)*t2*(1 - 3*t2 + 2*Power(t2,2)) + 
            s1*Power(t2,2)*(42 - 2*(17 + 22*s2)*t2 + 
               (29 + 113*s2 + 4*Power(s2,2))*Power(t2,2) + 
               2*(-78 - 23*s2 + 13*Power(s2,2))*Power(t2,3) + 
               (148 - 7*s2 - 6*Power(s2,2))*Power(t2,4) + 
               (-30 + 8*s2)*Power(t2,5) + Power(t2,6) + 
               Power(t1,3)*(-5 + 10*t2 - 22*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(40 - 2*(50 + s2)*t2 + 
                  (169 + 34*s2)*Power(t2,2) - 
                  4*(19 + 2*s2)*Power(t2,3) + 15*Power(t2,4)) + 
               t1*(-97 + (160 + 49*s2)*t2 - 
                  (105 + 210*s2 + 13*Power(s2,2))*Power(t2,2) + 
                  (105 + 113*s2 + Power(s2,2))*Power(t2,3) - 
                  2*(53 + 12*s2)*Power(t2,4) + 19*Power(t2,5))) - 
            Power(s1,2)*t2*(10 - 26*(1 + 2*s2)*t2 + 
               2*(109 + 87*s2 + 4*Power(s2,2))*Power(t2,2) + 
               (-580 - 70*s2 + 47*Power(s2,2))*Power(t2,3) + 
               (565 - 62*s2 - 19*Power(s2,2))*Power(t2,4) + 
               2*(-94 + 17*s2)*Power(t2,5) + Power(t2,6) + 
               Power(t1,3)*t2*(3 - 40*t2 + 13*Power(t2,2)) + 
               Power(t1,2)*(-3 - 74*t2 + (369 + 60*s2)*Power(t2,2) - 
                  (301 + 12*s2)*Power(t2,3) + 69*Power(t2,4)) + 
               t1*(-19 + 2*(68 + 29*s2)*t2 - 
                  2*(210 + 188*s2 + 13*Power(s2,2))*Power(t2,2) + 
                  (561 + 308*s2 + 2*Power(s2,2))*Power(t2,3) - 
                  (423 + 86*s2)*Power(t2,4) + 141*Power(t2,5))) - 
            Power(s1,4)*(Power(t1,2)*
                (1 + 13*t2 - 70*Power(t2,2) + 20*Power(t2,3)) + 
               2*t1*(1 - 10*(3 + s2)*t2 + (118 + 45*s2)*Power(t2,2) - 
                  (174 + 11*s2)*Power(t2,3) + 97*Power(t2,4)) + 
               t2*(50 - 265*t2 + 437*Power(t2,2) - 219*Power(t2,3) - 
                  3*Power(t2,4) - Power(s2,2)*t2*(5 + 7*t2) + 
                  2*s2*(2 + 9*t2 - 32*Power(t2,2) + 9*Power(t2,3)))) + 
            Power(s1,3)*(Power(t1,3)*
                (-1 - t2 - 12*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(-1 + (-3 + 2*s2)*t2 + 
                  (159 + 26*s2)*Power(t2,2) - (265 + 4*s2)*Power(t2,3) + 
                  86*Power(t2,4)) + 
               t1*t2*(32 - 261*t2 + Power(s2,2)*(-13 + t2)*t2 + 
                  602*Power(t2,2) - 629*Power(t2,3) + 280*Power(t2,4) - 
                  3*s2*(-3 + 62*t2 - 95*Power(t2,2) + 28*Power(t2,3))) - 
               t2*(10 - 181*t2 + 635*Power(t2,2) - 808*Power(t2,3) + 
                  341*Power(t2,4) + 3*Power(t2,5) + 
                  4*Power(s2,2)*t2*(-1 - 4*t2 + 5*Power(t2,2)) + 
                  s2*(8 - 65*t2 + 6*Power(t2,2) + 119*Power(t2,3) - 
                     44*Power(t2,4))))) + 
         Power(-1 + s1,2)*s1*(s1 - t2)*(-1 + t2)*t2*
          (-5 + 15*t1 - 15*Power(t1,2) + 5*Power(t1,3) + 
            4*Power(s1,4)*Power(s2 - t1,2)*(-1 + t2) + 41*t2 - 11*s2*t2 - 
            92*t1*t2 + 22*s2*t1*t2 + 61*Power(t1,2)*t2 - 
            11*s2*Power(t1,2)*t2 - 10*Power(t1,3)*t2 - 68*Power(t2,2) + 
            67*s2*Power(t2,2) - 54*Power(s2,2)*Power(t2,2) + 
            4*Power(s2,3)*Power(t2,2) + 98*t1*Power(t2,2) + 
            11*s2*t1*Power(t2,2) + 14*Power(s2,2)*t1*Power(t2,2) - 
            83*Power(t1,2)*Power(t2,2) - 10*s2*Power(t1,2)*Power(t2,2) + 
            21*Power(t1,3)*Power(t2,2) + 40*Power(t2,3) - 
            109*s2*Power(t2,3) + 67*Power(s2,2)*Power(t2,3) - 
            12*Power(s2,3)*Power(t2,3) + 14*t1*Power(t2,3) + 
            2*s2*t1*Power(t2,3) + 6*Power(s2,2)*t1*Power(t2,3) + 
            Power(t1,2)*Power(t2,3) - 3*s2*Power(t1,2)*Power(t2,3) - 
            6*Power(t1,3)*Power(t2,3) - 15*Power(t2,4) + 
            61*s2*Power(t2,4) - 14*Power(s2,2)*Power(t2,4) - 
            2*Power(s2,3)*Power(t2,4) - 29*t1*Power(t2,4) - 
            41*s2*t1*Power(t2,4) + 10*Power(s2,2)*t1*Power(t2,4) + 
            36*Power(t1,2)*Power(t2,4) - 6*s2*Power(t1,2)*Power(t2,4) + 
            7*Power(t2,5) - 8*s2*Power(t2,5) + Power(s2,2)*Power(t2,5) - 
            6*t1*Power(t2,5) + 6*s2*t1*Power(t2,5) + 
            Power(s1,3)*(2*Power(t1,3)*(-6 + t2) - 4*Power(-1 + t2,2) + 
               13*t1*Power(-1 + t2,2) + Power(s2,3)*(4 + 6*t2) - 
               2*Power(t1,2)*(5 - 14*t2 + 9*Power(t2,2)) - 
               Power(s2,2)*(1 - 10*t2 + 9*Power(t2,2) + 
                  10*t1*(2 + t2)) + 
               s2*(-5*Power(-1 + t2,2) + 2*Power(t1,2)*(14 + t2) + 
                  t1*(7 - 30*t2 + 23*Power(t2,2)))) + 
            Power(s1,2)*(Power(-1 + t2,2)*(-3 + 11*t2) - 
               2*t1*Power(-1 + t2,2)*(23 + 12*t2) + 
               Power(t1,3)*(7 + 36*t2 - 13*Power(t2,2)) - 
               2*Power(s2,3)*(-2 + 10*t2 + 7*Power(t2,2)) + 
               2*Power(t1,2)*
                (5 - 23*t2 + 8*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,2)*(-54 + 69*t2 - 22*Power(t2,2) + 
                  7*Power(t2,3) + 2*t1*(7 + 23*t2 + 15*Power(t2,2))) - 
               s2*(-2*Power(-1 + t2,2)*(24 + t2) + 
                  Power(t1,2)*(29 + 54*t2 + 7*Power(t2,2)) + 
                  t1*(-49 + 22*t2 + 11*Power(t2,2) + 16*Power(t2,3)))) + 
            s1*(2*Power(t1,3)*t2*(-14 - 4*t2 + 3*Power(t2,2)) + 
               2*Power(s2,3)*t2*(-4 + 14*t2 + 5*Power(t2,2)) - 
               Power(-1 + t2,2)*(31 - 9*t2 + 14*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(62 + 72*t2 + 17*Power(t2,2)) + 
               Power(t1,2)*(-31 + 73*t2 + 25*Power(t2,2) - 
                  61*Power(t2,3) - 6*Power(t2,4)) - 
               Power(s2,2)*t2*
                (t1*(28 + 32*t2 + 30*Power(t2,2)) + 
                  3*(-36 + 45*t2 - 10*Power(t2,2) + Power(t2,3))) + 
               s2*(Power(-1 + t2,2)*(11 - 93*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*
                   (11 + 39*t2 + 29*Power(t2,2) + 11*Power(t2,3)) + 
                  t1*(-22 - 60*t2 + 13*Power(t2,2) + 74*Power(t2,3) - 
                     5*Power(t2,4))))) + 
         Power(s,3)*(8*Power(-1 + t2,2)*Power(t2,3)*
             (-2 - t2 + Power(t2,2)) - 
            8*Power(s1,6)*(-1 + t1)*t2*(3 - 7*t2 + 4*Power(t2,2)) + 
            Power(s1,2)*t2*(34 + (82 - 128*s2)*t2 + 
               (-322 + 515*s2 - 54*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,2) + 
               (65 - 579*s2 + 267*Power(s2,2) - 31*Power(s2,3))*
                Power(t2,3) + 
               (392 + 262*s2 - 236*Power(s2,2) - 9*Power(s2,3))*
                Power(t2,4) + 
               (-339 - 3*s2 + 47*Power(s2,2))*Power(t2,5) + 
               (90 - 19*s2)*Power(t2,6) - 2*Power(t2,7) + 
               Power(t1,3)*(-5 + 38*t2 - 144*Power(t2,2) + 
                  157*Power(t2,3) - 34*Power(t2,4)) + 
               Power(t1,2)*(23 - (208 + 19*s2)*t2 + 
                  (778 + 169*s2)*Power(t2,2) - 
                  (789 + 238*s2)*Power(t2,3) + 
                  7*(43 + 4*s2)*Power(t2,4) - 33*Power(t2,5)) + 
               t1*(-79 + 3*(67 + 49*s2)*t2 - 
                  (161 + 835*s2 + 48*Power(s2,2))*Power(t2,2) + 
                  (69 + 898*s2 + 113*Power(s2,2))*Power(t2,3) + 
                  (-243 - 363*s2 + 19*Power(s2,2))*Power(t2,4) + 
                  57*(4 + s2)*Power(t2,5) - 63*Power(t2,6))) + 
            s1*Power(t2,2)*(-60 + (-56 + 92*s2)*t2 + 
               (292 - 287*s2 + 29*Power(s2,2) - 2*Power(s2,3))*
                Power(t2,2) + 
               (-112 + 286*s2 - 130*Power(s2,2) + 13*Power(s2,3))*
                Power(t2,3) + 
               (-130 - 108*s2 + 63*Power(s2,2) + Power(s2,3))*
                Power(t2,4) + (74 + 5*s2 - 10*Power(s2,2))*Power(t2,5) - 
               8*Power(t2,6) + 
               Power(t1,3)*(10 - 25*t2 + 53*Power(t2,2) - 
                  32*Power(t2,3) + 6*Power(t2,4)) + 
               Power(t1,2)*(-75 + (163 + 17*s2)*t2 - 
                  2*(129 + 44*s2)*Power(t2,2) + 
                  2*(91 + 32*s2)*Power(t2,3) - (72 + 5*s2)*Power(t2,4)) \
+ t1*(162 - (166 + 109*s2)*t2 + 
                  (-156 + 420*s2 + 19*Power(s2,2))*Power(t2,2) + 
                  (135 - 311*s2 - 25*Power(s2,2))*Power(t2,3) + 
                  (79 + 115*s2 - 6*Power(s2,2))*Power(t2,4) - 
                  (48 + 7*s2)*Power(t2,5) + 6*Power(t2,6))) + 
            Power(s1,5)*(Power(t1,2)*
                (3 + 41*t2 - 100*Power(t2,2) + 20*Power(t2,3)) - 
               t2*(-54 + 266*t2 - 405*Power(t2,2) + 190*Power(t2,3) + 
                  3*Power(t2,4) + 
                  Power(s2,2)*(-5 + 4*t2 + 25*Power(t2,2)) + 
                  s2*(9 - 85*t2 + 112*Power(t2,2) - 24*Power(t2,3))) - 
               t1*(-6 + 55*t2 - 165*Power(t2,2) + 260*Power(t2,3) - 
                  156*Power(t2,4) + 
                  s2*(1 + 57*t2 - 122*Power(t2,2) + 4*Power(t2,3)))) + 
            Power(s1,4)*(Power(t1,3)*
                (3 - 9*t2 + 50*Power(t2,2) - 8*Power(t2,3)) + 
               Power(t1,2)*(6 + 73*t2 - 332*Power(t2,2) + 
                  369*Power(t2,3) - 92*Power(t2,4) + 
                  s2*(-1 + 17*t2 - 110*Power(t2,2) + 10*Power(t2,3))) + 
               t2*(11 - 183*t2 + 657*Power(t2,2) - 878*Power(t2,3) + 
                  390*Power(t2,4) + 3*Power(t2,5) - 
                  Power(s2,3)*t2*(5 + 7*t2) + 
                  Power(s2,2)*
                   (4 - 3*t2 - 102*Power(t2,2) + 77*Power(t2,3)) + 
                  s2*(29 - 5*t2 - 132*Power(t2,2) + 223*Power(t2,3) - 
                     67*Power(t2,4))) + 
               t1*(5 - 55*t2 + 166*Power(t2,2) - 415*Power(t2,3) + 
                  547*Power(t2,4) - 296*Power(t2,5) + 
                  Power(s2,2)*t2*(-10 + 63*t2 + 7*Power(t2,2)) + 
                  s2*(1 - 105*t2 + 406*Power(t2,2) - 361*Power(t2,3) + 
                     59*Power(t2,4)))) + 
            Power(s1,3)*(Power(t1,3)*
                (1 - 4*t2 + 76*Power(t2,2) - 187*Power(t2,3) + 
                  54*Power(t2,4)) + 
               Power(t1,2)*(4 + 3*(7 + s2)*t2 - 
                  2*(244 + 49*s2)*Power(t2,2) + 
                  (922 + 284*s2)*Power(t2,3) - 
                  3*(186 + 11*s2)*Power(t2,4) + 99*Power(t2,5)) + 
               t1*(1 - (64 + 39*s2)*t2 + 
                  (252 + 521*s2 + 39*Power(s2,2))*Power(t2,2) - 
                  (327 + 936*s2 + 151*Power(s2,2))*Power(t2,3) + 
                  (474 + 487*s2 - 20*Power(s2,2))*Power(t2,4) - 
                  (487 + 105*s2)*Power(t2,5) + 223*Power(t2,6)) + 
               t2*(-20 + 41*t2 + 200*Power(t2,2) - 679*Power(t2,3) + 
                  764*Power(t2,4) - 308*Power(t2,5) + 2*Power(t2,6) + 
                  Power(s2,3)*t2*(-2 + 23*t2 + 15*Power(t2,2)) + 
                  Power(s2,2)*t2*
                   (21 - 139*t2 + 279*Power(t2,2) - 89*Power(t2,3)) + 
                  s2*(36 - 257*t2 + 307*Power(t2,2) - 107*Power(t2,3) - 
                     113*Power(t2,4) + 62*Power(t2,5))))) - 
         s*(-2*Power(-1 + t2,3)*Power(t2,3)*(1 + t2) + 
            s1*Power(t2,2)*(-4 + (128 - 44*s2)*t2 + 
               (-352 + 243*s2 - 139*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,2) + 
               (368 - 493*s2 + 310*Power(s2,2) - 45*Power(s2,3))*
                Power(t2,3) + 
               (-180 + 449*s2 - 211*Power(s2,2) + 19*Power(s2,3))*
                Power(t2,4) + 
               (48 - 171*s2 + 42*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,5) - 2*(4 - 8*s2 + Power(s2,2))*Power(t2,6) + 
               Power(t1,3)*(10 - 25*t2 + 53*Power(t2,2) - 
                  32*Power(t2,3) + 6*Power(t2,4)) + 
               Power(t1,2)*(-20 + (144 - 35*s2)*t2 + 
                  (-266 + 22*s2)*Power(t2,2) - 
                  34*(-3 + s2)*Power(t2,3) - (-112 + s2)*Power(t2,4) + 
                  12*(-6 + s2)*Power(t2,5)) + 
               t1*(8 + (-235 + 79*s2)*t2 + 
                  (527 - 133*s2 + 35*Power(s2,2))*Power(t2,2) + 
                  (-313 + 118*s2 + 7*Power(s2,2))*Power(t2,3) + 
                  (-49 - 153*s2 + 14*Power(s2,2))*Power(t2,4) + 
                  (56 + 101*s2 - 20*Power(s2,2))*Power(t2,5) + 
                  (6 - 12*s2)*Power(t2,6))) + 
            Power(s1,2)*t2*(-4 + (-226 + 80*s2)*t2 + 
               (644 - 501*s2 + 360*Power(s2,2) - 26*Power(s2,3))*
                Power(t2,2) + 
               (-616 + 1119*s2 - 857*Power(s2,2) + 129*Power(s2,3))*
                Power(t2,3) + 
               (230 - 1138*s2 + 654*Power(s2,2) - 39*Power(s2,3))*
                Power(t2,4) - 
               (60 - 539*s2 + 173*Power(s2,2) + 8*Power(s2,3))*
                Power(t2,5) + 
               (58 - 115*s2 + 18*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,6) - 2*(13 - 8*s2 + Power(s2,2))*Power(t2,7) + 
               Power(t1,3)*(-5 + 18*t2 - 114*Power(t2,2) + 
                  87*Power(t2,3) - 58*Power(t2,4) + 12*Power(t2,5)) + 
               Power(t1,2)*(4 + 21*(-10 + 3*s2)*t2 + 
                  (503 + 13*s2)*Power(t2,2) + 
                  (-335 + 74*s2)*Power(t2,3) + 
                  (-73 + 68*s2)*Power(t2,4) + 
                  (171 - 50*s2)*Power(t2,5) + 12*(-5 + s2)*Power(t2,6)) \
+ t1*(11 + (406 - 143*s2)*t2 + 
                  (-954 + 159*s2 - 88*Power(s2,2))*Power(t2,2) + 
                  (426 + 54*s2 - 61*Power(s2,2))*Power(t2,3) + 
                  (384 + 53*s2 - 87*Power(s2,2))*Power(t2,4) + 
                  (-302 - 213*s2 + 76*Power(s2,2))*Power(t2,5) + 
                  (5 + 102*s2 - 20*Power(s2,2))*Power(t2,6) - 
                  12*(-2 + s2)*Power(t2,7))) + 
            Power(s1,7)*(-1 + t2)*
             (Power(t1,2)*(1 + 5*t2 + 10*Power(t2,2)) - 
               t1*(-2 + s2 - 5*t2 + 2*s2*t2 + 2*Power(t2,2) + 
                  29*s2*Power(t2,2) + 5*Power(t2,3)) + 
               t2*(10*Power(-1 + t2,2) + Power(s2,2)*(-3 + 19*t2) + 
                  s2*(-17 + 20*t2 - 3*Power(t2,2)))) + 
            Power(s1,6)*(Power(t1,3)*
                (-1 + 35*t2 - 54*Power(t2,2) + 8*Power(t2,3)) + 
               t2*(s2*Power(-1 + t2,2)*(-27 - 37*t2 + 9*Power(t2,2)) + 
                  Power(s2,3)*(-8 + t2 + 19*Power(t2,2)) - 
                  Power(-1 + t2,2)*(-31 - 3*t2 + 38*Power(t2,2)) + 
                  Power(s2,2)*
                   (20 - 57*t2 + 100*Power(t2,2) - 63*Power(t2,3))) + 
               Power(t1,2)*(2*t2*
                   (12 - 27*t2 + 41*Power(t2,2) - 26*Power(t2,3)) + 
                  s2*(1 - 77*t2 + 110*Power(t2,2) + 2*Power(t2,3))) + 
               t1*(Power(s2,2)*t2*(50 - 57*t2 - 29*Power(t2,2)) + 
                  Power(-1 + t2,2)*
                   (3 - 19*t2 + 56*Power(t2,2) + 23*Power(t2,3)) + 
                  s2*(-3 - 17*t2 + 44*Power(t2,2) - 117*Power(t2,3) + 
                     93*Power(t2,4)))) - 
            Power(s1,3)*(Power(t1,3)*
                (-1 + 14*t2 - 98*Power(t2,2) + 80*Power(t2,3) - 
                  105*Power(t2,4) - 28*Power(t2,5) + 18*Power(t2,6)) + 
               Power(t1,2)*(2 + (-136 + 29*s2)*t2 + 
                  (332 + 62*s2)*Power(t2,2) + 
                  (-507 + 55*s2)*Power(t2,3) + 
                  (342 + 167*s2)*Power(t2,4) + 
                  (145 + 18*s2)*Power(t2,5) + 
                  (-166 + 29*s2)*Power(t2,6) - 12*Power(t2,7)) + 
               t1*(1 + (244 - 65*s2)*t2 - 
                  (498 + 20*s2 + 71*Power(s2,2))*Power(t2,2) + 
                  (74 + 540*s2 - 93*Power(s2,2))*Power(t2,3) + 
                  (453 - 499*s2 - 169*Power(s2,2))*Power(t2,4) + 
                  (-264 - 206*s2 + 59*Power(s2,2))*Power(t2,5) + 
                  (-102 + 279*s2 - 86*Power(s2,2))*Power(t2,6) + 
                  (86 - 29*s2)*Power(t2,7) + 6*Power(t2,8)) + 
               t2*(Power(s2,3)*t2*
                   (-22 + 133*t2 - 35*Power(t2,2) + 17*Power(t2,3) + 
                     27*Power(t2,4)) + 
                  s2*Power(-1 + t2,2)*
                   (36 - 233*t2 + 404*Power(t2,2) - 274*Power(t2,3) + 
                     69*Power(t2,4)) + 
                  Power(s2,2)*t2*
                   (303 - 891*t2 + 918*Power(t2,2) - 411*Power(t2,3) + 
                     97*Power(t2,4) - 16*Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (126 - 53*t2 + 37*Power(t2,2) - 114*Power(t2,3) + 
                     110*Power(t2,4) + 6*Power(t2,5)))) - 
            Power(s1,5)*(Power(t1,3)*
                (-3 + 6*t2 + 104*Power(t2,2) - 225*Power(t2,3) + 
                  58*Power(t2,4)) + 
               t2*(s2*Power(-1 + t2,2)*
                   (99 - 210*t2 + 14*Power(t2,2) + 9*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (38 - 149*t2 + 75*Power(t2,2) + 52*Power(t2,3)) + 
                  Power(s2,3)*
                   (10 - 30*t2 + 23*Power(t2,2) + 57*Power(t2,3)) + 
                  Power(s2,2)*
                   (-107 + 283*t2 - 296*Power(t2,2) + 201*Power(t2,3) - 
                     81*Power(t2,4))) + 
               Power(t1,2)*(-1 - 36*t2 - 51*Power(t2,2) + 
                  165*Power(t2,3) + 2*Power(t2,4) - 79*Power(t2,5) + 
                  s2*(2 - 37*t2 - 88*Power(t2,2) + 294*Power(t2,3) + 
                     9*Power(t2,4))) + 
               t1*(Power(s2,2)*t2*
                   (8 + 63*t2 - 147*Power(t2,2) - 104*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (1 - 63*t2 + 22*Power(t2,2) + 127*Power(t2,3) + 
                     37*Power(t2,4)) + 
                  s2*(-3 + 168*t2 - 241*Power(t2,2) + 24*Power(t2,3) - 
                     42*Power(t2,4) + 94*Power(t2,5)))) + 
            Power(s1,4)*(Power(t1,3)*
                (-3 - 10*t2 + 32*Power(t2,2) - 14*Power(t2,3) - 
                  187*Power(t2,4) + 62*Power(t2,5)) + 
               t2*(s2*Power(-1 + t2,2)*
                   (-47 + 285*t2 - 390*Power(t2,2) + 87*Power(t2,3) + 
                     3*Power(t2,4)) - 
                  Power(-1 + t2,2)*
                   (17 + 97*t2 - 240*Power(t2,2) + 152*Power(t2,3) + 
                     30*Power(t2,4)) + 
                  Power(s2,3)*
                   (-6 + 59*t2 - 37*Power(t2,2) + 43*Power(t2,3) + 
                     61*Power(t2,4)) + 
                  Power(s2,2)*
                   (82 - 451*t2 + 738*Power(t2,2) - 522*Power(t2,3) + 
                     204*Power(t2,4) - 51*Power(t2,5))) + 
               Power(t1,2)*(2 + 4*t2 - 310*Power(t2,2) + 
                  319*Power(t2,3) + 197*Power(t2,4) - 163*Power(t2,5) - 
                  49*Power(t2,6) + 
                  s2*(1 + 29*t2 - 23*Power(t2,2) + 89*Power(t2,3) + 
                     240*Power(t2,4) + 24*Power(t2,5))) - 
               t1*(Power(s2,2)*t2*
                   (18 + 31*t2 + 83*Power(t2,2) + 87*Power(t2,3) + 
                     141*Power(t2,4)) - 
                  Power(-1 + t2,2)*
                   (1 + 22*t2 - 64*Power(t2,2) + 54*Power(t2,3) + 
                     152*Power(t2,4) + 25*Power(t2,5)) + 
                  s2*(1 + 49*t2 - 539*Power(t2,2) + 624*Power(t2,3) + 
                     115*Power(t2,4) - 237*Power(t2,5) - 13*Power(t2,6))))\
) + Power(s,2)*(8*Power(s1,7)*(-1 + t1)*Power(-1 + t2,2)*t2 - 
            2*Power(-1 + t2,2)*Power(t2,3)*(-5 - t2 + 4*Power(t2,2)) + 
            s1*Power(t2,2)*(29 + (142 - 89*s2)*t2 + 
               (-518 + 371*s2 - 118*Power(s2,2) + 8*Power(s2,3))*
                Power(t2,2) + 
               (470 - 568*s2 + 293*Power(s2,2) - 42*Power(s2,3))*
                Power(t2,3) + 
               (-93 + 385*s2 - 187*Power(s2,2) + 8*Power(s2,3))*
                Power(t2,4) + 
               (-36 - 107*s2 + 37*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,5) + (6 + 8*s2 - Power(s2,2))*Power(t2,6) + 
               Power(t1,3)*t2*
                (5 - 9*t2 + 22*Power(t2,2) - 6*Power(t2,3)) + 
               Power(t1,2)*(35 - (10 + 39*s2)*t2 + 
                  (1 + 75*s2)*Power(t2,2) - (100 + 97*s2)*Power(t2,3) + 
                  (134 + 7*s2)*Power(t2,4) + 6*(-6 + s2)*Power(t2,5)) + 
               t1*(-90 + 25*(-3 + 5*s2)*t2 + 
                  (517 - 369*s2 + 15*Power(s2,2))*Power(t2,2) + 
                  (-397 + 342*s2 + 39*Power(s2,2))*Power(t2,3) + 
                  (-11 - 201*s2 + 16*Power(s2,2))*Power(t2,4) + 
                  (62 + 61*s2 - 10*Power(s2,2))*Power(t2,5) - 
                  6*(1 + s2)*Power(t2,6))) + 
            Power(s1,2)*t2*(-33 + (-261 + 142*s2)*t2 + 
               (931 - 730*s2 + 272*Power(s2,2) - 18*Power(s2,3))*
                Power(t2,2) + 
               (-942 + 1333*s2 - 815*Power(s2,2) + 112*Power(s2,3))*
                Power(t2,3) + 
               (326 - 1129*s2 + 645*Power(s2,2) - 30*Power(s2,3))*
                Power(t2,4) - 
               (37 - 451*s2 + 208*Power(s2,2) + 16*Power(s2,3))*
                Power(t2,5) + (6 - 67*s2 + 22*Power(s2,2))*Power(t2,6) + 
               10*Power(t2,7) + 
               Power(t1,3)*(5 - 22*t2 + 59*Power(t2,2) - 
                  114*Power(t2,3) + 90*Power(t2,4) - 18*Power(t2,5)) + 
               Power(t1,2)*(-36 + (9 + 60*s2)*t2 - 
                  5*(35 + 29*s2)*Power(t2,2) + 
                  (417 + 259*s2)*Power(t2,3) - 
                  (456 + 119*s2)*Power(t2,4) + 
                  (151 - 7*s2)*Power(t2,5) + 6*Power(t2,6)) + 
               t1*(86 + (193 - 196*s2)*t2 - 
                  3*(258 - 233*s2 + 8*Power(s2,2))*Power(t2,2) - 
                  2*(-277 + 335*s2 + 62*Power(s2,2))*Power(t2,3) + 
                  15*(1 + 36*s2)*Power(t2,4) + 
                  (-71 - 230*s2 + 52*Power(s2,2))*Power(t2,5) + 
                  (9 + 25*s2)*Power(t2,6) - 12*Power(t2,7))) + 
            Power(s1,6)*(Power(t1,2)*(-3 - 35*t2 + 50*Power(t2,2)) + 
               t2*(Power(-1 + t2,2)*(-22 + 77*t2 + Power(t2,2)) + 
                  3*Power(s2,2)*(-2 - 5*t2 + 11*Power(t2,2)) - 
                  2*s2*(-15 + 50*t2 - 42*Power(t2,2) + 7*Power(t2,3))) - 
               2*t1*(3 - t2 + 7*Power(t2,2) - 36*Power(t2,3) + 
                  27*Power(t2,4) + 
                  s2*(-1 - 23*t2 + 21*Power(t2,2) + 15*Power(t2,3)))) + 
            Power(s1,5)*(3*Power(t1,3)*
                (-1 + 11*t2 - 26*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(-2 + (-57 + 90*s2 + 40*Power(s2,2))*t2 + 
                  (140 - 173*s2 - 97*Power(s2,2))*Power(t2,2) + 
                  (-63 + 70*s2 - 27*Power(s2,2))*Power(t2,3) + 
                  (-158 + 61*s2)*Power(t2,4) + 140*Power(t2,5)) - 
               Power(t1,2)*(5 + 47*t2 - 153*Power(t2,2) + 
                  125*Power(t2,3) + 
                  2*s2*(-1 + 34*t2 - 84*Power(t2,2) + 3*Power(t2,3))) - 
               t2*(-4*Power(s2,3)*(-1 + 2*t2 + 5*Power(t2,2)) + 
                  Power(-1 + t2,2)*
                   (-26 - 9*t2 + 205*Power(t2,2) + Power(t2,3)) + 
                  Power(s2,2)*
                   (13 + 71*t2 - 167*Power(t2,2) + 107*Power(t2,3)) + 
                  s2*(-27 + 40*t2 - 143*Power(t2,2) + 172*Power(t2,3) - 
                     42*Power(t2,4)))) - 
            Power(s1,4)*(Power(t1,3)*
                (-2 - 29*t2 + 164*Power(t2,2) - 313*Power(t2,3) + 
                  84*Power(t2,4)) + 
               Power(t1,2)*(7 + 82*t2 - 384*Power(t2,2) + 
                  613*Power(t2,3) - 295*Power(t2,4) + Power(t2,5) + 
                  s2*(-2 + 31*t2 - 209*Power(t2,2) + 441*Power(t2,3) - 
                     21*Power(t2,4))) + 
               t1*(6 - 39*t2 - 97*Power(t2,2) + 313*Power(t2,3) - 
                  206*Power(t2,4) - 128*Power(t2,5) + 151*Power(t2,6) - 
                  2*Power(s2,2)*t2*
                   (3 - 63*t2 + 113*Power(t2,2) + 43*Power(t2,3)) + 
                  s2*(2 - 89*t2 + 284*Power(t2,2) - 410*Power(t2,3) + 
                     146*Power(t2,4) + 19*Power(t2,5))) + 
               t2*(Power(s2,3)*
                   (2 - 36*t2 + 30*Power(t2,2) + 52*Power(t2,3)) + 
                  Power(s2,2)*
                   (-36 + 203*t2 - 431*Power(t2,2) + 423*Power(t2,3) - 
                     135*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (-51 + 139*t2 - 104*Power(t2,2) - 210*Power(t2,3) + 
                     Power(t2,4)) + 
                  s2*(84 - 247*t2 + 433*Power(t2,2) - 271*Power(t2,3) - 
                     41*Power(t2,4) + 42*Power(t2,5)))) + 
            Power(s1,3)*(Power(t1,3)*
                (1 + 21*t2 - 112*Power(t2,2) + 235*Power(t2,3) - 
                  289*Power(t2,4) + 72*Power(t2,5)) - 
               Power(t1,2)*(5 + 23*(2 + s2)*t2 - 
                  3*(131 + 33*s2)*Power(t2,2) + 
                  (663 + 303*s2)*Power(t2,3) - 
                  (691 + 385*s2)*Power(t2,4) + 
                  (269 + 14*s2)*Power(t2,5) + 5*Power(t2,6)) + 
               t1*(-2 + (-22 + 73*s2)*t2 + 
                  (64 - 419*s2 + 3*Power(s2,2))*Power(t2,2) + 
                  (-203 + 520*s2 + 171*Power(s2,2))*Power(t2,3) + 
                  (329 - 622*s2 - 145*Power(s2,2))*Power(t2,4) + 
                  (-206 + 287*s2 - 101*Power(s2,2))*Power(t2,5) - 
                  (29 + 31*s2)*Power(t2,6) + 69*Power(t2,7)) + 
               t2*(2*Power(s2,3)*t2*
                   (6 - 51*t2 + 22*Power(t2,2) + 23*Power(t2,3)) - 
                  2*Power(s2,2)*t2*
                   (95 - 369*t2 + 406*Power(t2,2) - 221*Power(t2,3) + 
                     41*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (95 - 212*t2 + 162*Power(t2,2) - 123*Power(t2,3) - 
                     84*Power(t2,4) + Power(t2,5)) + 
                  s2*(-53 + 443*t2 - 1039*Power(t2,2) + 1187*Power(t2,3) - 
                     658*Power(t2,4) + 106*Power(t2,5) + 14*Power(t2,6)))))\
))/(s*(-1 + s1)*s1*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       (-1 + s2)*(-1 + t1)*Power(s1 - t2,2)*Power(-1 + t2,2)*t2*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) + 
    (8*(-(Power(s,7)*Power(t2,2)*((-3 + t2)*t2 + t1*(1 + t2))) + 
         (s1 - t2)*(-1 + t2)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,
           2)*(-3 + 3*t1 + 2*Power(s1,3)*(-1 + t2) + 18*t2 - s2*t2 - 
            7*t1*t2 - 15*Power(t2,2) + 7*s2*Power(t2,2) - 
            2*t1*Power(t2,2) + 
            Power(s1,2)*(t1*(-7 + t2) - 2*(-1 + t2)*t2 + 
               3*s2*(1 + t2)) + 
            s1*(s2*(1 - 10*t2 - 3*Power(t2,2)) - 
               3*(5 - 6*t2 + Power(t2,2)) + 2*t1*(2 + 3*t2 + Power(t2,2))\
)) + Power(s,6)*t2*(t2*(-1 + (-35 + 2*s2)*t2 + 2*(7 + s2)*Power(t2,2) - 
               Power(t1,2)*(1 + t2) + 
               t1*(6 + s2 + 3*t2 + s2*t2 - 7*Power(t2,2))) + 
            s1*(2*t1*(-1 + t2 + 2*Power(t2,2)) + 
               t2*(10 - 15*t2 + 3*Power(t2,2) - s2*(1 + t2)))) - 
         Power(s,5)*(Power(t1,3)*Power(-1 + t2,2) + 
            t1*Power(t2,2)*(11 + 2*t2 - 24*Power(t2,2) + 
               5*Power(t2,3) + s2*(3 + 2*t2 - 7*Power(t2,2))) + 
            Power(t1,2)*t2*(s2 - s2*t2 + 
               2*t2*(-1 - 4*t2 + 3*Power(t2,2))) + 
            Power(t2,2)*(-5 - 136*t2 + 107*Power(t2,2) - 
               13*Power(t2,3) + Power(t2,4) + Power(s2,2)*t2*(1 + t2) + 
               s2*(-1 + 6*t2 + Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,2)*(t1*(1 - 5*t2 + 6*Power(t2,3)) + 
               t2*(-11 + 33*t2 - 25*Power(t2,2) + 3*Power(t2,3) - 
                  s2*(-2 + t2 + 3*Power(t2,2)))) - 
            s1*t2*(-2 + (-97 + 9*s2 + Power(s2,2))*t2 + 
               (153 + s2 + Power(s2,2))*Power(t2,2) - 
               4*(9 + s2)*Power(t2,3) + 
               Power(t1,2)*(-1 + t2 + 4*Power(t2,2)) - 
               t1*(-10 + 14*t2 + 29*Power(t2,2) - 21*Power(t2,3) + 
                  s2*(-2 + t2 + 3*Power(t2,2))))) + 
         Power(s,4)*(-(Power(t1,3)*
               (-2 + 10*t2 - 9*Power(t2,2) + Power(t2,4))) - 
            Power(t1,2)*(3 - 5*(-1 + s2)*t2 + (-9 + 7*s2)*Power(t2,2) + 
               (35 + 2*s2)*Power(t2,3) - (33 + 2*s2)*Power(t2,4) + 
               3*Power(t2,5)) + 
            Power(s1,3)*(-1 + t2)*
             (-4 + 19*t2 - 16*Power(t2,2) + Power(t2,3) + 
               s2*(1 - 2*t2 - 3*Power(t2,2)) + 
               2*t1*(-1 + t2 + 2*Power(t2,2))) + 
            t1*t2*(-3 + 6*t2 + 11*Power(t2,2) - 7*Power(t2,3) + 
               Power(t2,4) - 2*Power(t2,5) + 
               s2*(1 - 10*t2 + 8*Power(t2,2) - 19*Power(t2,3)) + 
               Power(s2,2)*(t2 + 4*Power(t2,2) - Power(t2,3))) + 
            Power(t2,2)*(Power(s2,2)*(2 + t2 + 3*Power(t2,3)) + 
               s2*(-1 + t2 - 24*Power(t2,2) + 25*Power(t2,3) - 
                  Power(t2,4)) - 
               2*(4 + 123*t2 - 163*Power(t2,2) + 69*Power(t2,3) - 
                  11*Power(t2,4) + Power(t2,5))) + 
            Power(s1,2)*(-1 + (-89 + 10*s2 + 2*Power(s2,2))*t2 + 
               (268 - 14*s2 + Power(s2,2))*Power(t2,2) - 
               (215 + Power(s2,2))*Power(t2,3) + 
               (31 + 2*s2)*Power(t2,4) + 
               Power(t1,2)*t2*(-3 + 5*t2 - 6*Power(t2,2)) + 
               t1*(4 - 23*t2 - 9*Power(t2,2) + 51*Power(t2,3) - 
                  21*Power(t2,4) + 
                  s2*(1 - t2 - 3*Power(t2,2) + 3*Power(t2,3)))) + 
            s1*(Power(t1,3)*(9 - 14*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(3 - 3*t2 + 30*Power(t2,2) - 
                  47*Power(t2,3) + 17*Power(t2,4) + 
                  s2*(-4 + 11*t2 - 5*Power(t2,2))) + 
               t1*t2*(-7 + 3*t2 + 37*Power(t2,2) - 44*Power(t2,3) + 
                  11*Power(t2,4) - 2*Power(s2,2)*(1 + Power(t2,2)) + 
                  s2*(-5 + 2*t2 + 28*Power(t2,2) - 13*Power(t2,3))) + 
               t2*(8 + 308*t2 - 578*Power(t2,2) + 295*Power(t2,3) - 
                  30*Power(t2,4) + Power(t2,5) + 
                  Power(s2,2)*t2*(-6 + t2 - 3*Power(t2,2)) + 
                  s2*(2 - 16*t2 + 21*Power(t2,2) - 14*Power(t2,3) + 
                     Power(t2,4))))) + 
         Power(s,3)*(Power(t1,3)*t2*
             (9 - 20*t2 + 4*Power(t2,2) + 5*Power(t2,3)) - 
            Power(t1,2)*(-3 + (-5 + 9*s2)*t2 + 
               (56 - 25*s2)*Power(t2,2) - 4*(19 + s2)*Power(t2,3) + 
               (69 + 13*s2)*Power(t2,4) + (-12 + s2)*Power(t2,5) + 
               Power(t2,6)) - 
            t1*(3 + (13 - 8*s2)*t2 + 
               (1 - 33*s2 + 3*Power(s2,2))*Power(t2,2) + 
               (25 + 38*s2 + 14*Power(s2,2))*Power(t2,3) + 
               (41 - 5*s2 - 15*Power(s2,2))*Power(t2,4) - 
               (38 + 27*s2 + 2*Power(s2,2))*Power(t2,5) + 
               (-1 + s2)*Power(t2,6)) + 
            Power(s1,4)*Power(-1 + t2,2)*
             (4*(-1 + t2) + s2*(1 + t2) - t1*(1 + t2)) - 
            t2*(3 + 19*t2 - 234*Power(t2,2) + 468*Power(t2,3) - 
               365*Power(t2,4) + 124*Power(t2,5) - 16*Power(t2,6) + 
               Power(t2,7) + 
               Power(s2,3)*Power(t2,2)*(2 + t2 + Power(t2,2)) - 
               2*Power(s2,2)*t2*
                (-4 + 12*t2 + 2*Power(t2,2) - 13*Power(t2,3) + 
                  Power(t2,4)) - 
               s2*(2 + 20*t2 + 80*Power(t2,3) - 63*Power(t2,4) + 
                  4*Power(t2,5) + Power(t2,6))) + 
            Power(s1,3)*(-27 + 148*t2 - 236*Power(t2,2) + 
               124*Power(t2,3) - 9*Power(t2,4) - 
               Power(s2,2)*(-1 - 2*t2 + 2*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-4 + 17*t2 - 13*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-8 + 15*t2 + 15*Power(t2,2) - 25*Power(t2,3) + 
                  7*Power(t2,4)) + 
               s2*(3 - 17*t2 + 15*Power(t2,2) - 5*Power(t2,3) - 
                  t1*(-1 + 13*t2 - 9*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,2)*(3 + 216*t2 - 681*Power(t2,2) + 
               730*Power(t2,3) - 286*Power(t2,4) + 18*Power(t2,5) + 
               Power(t1,3)*(-29 + 37*t2 - 10*Power(t2,2)) - 
               Power(s2,3)*t2*(1 + t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-19 + 57*t2 - 121*Power(t2,2) + 
                  80*Power(t2,3) - 15*Power(t2,4)) + 
               t1*t2*(-39 + 10*t2 + 16*Power(t2,2) + 12*Power(t2,3) - 
                  7*Power(t2,4)) + 
               Power(s2,2)*(t1*
                   (-5 + 10*t2 - 5*Power(t2,2) + 4*Power(t2,3)) + 
                  t2*(-8 + 13*t2 - 18*Power(t2,2) + 5*Power(t2,3))) + 
               s2*(1 - t2 + 51*Power(t2,2) - 50*Power(t2,3) + 
                  6*Power(t2,4) + Power(t2,5) + 
                  Power(t1,2)*(27 - 36*t2 + 11*Power(t2,2)) + 
                  t1*(4 + 9*t2 + 41*Power(t2,2) - 32*Power(t2,3) + 
                     4*Power(t2,4)))) + 
            s1*(Power(t1,3)*(-11 + 50*t2 - 34*Power(t2,2) - 
                  4*Power(t2,3) + 3*Power(t2,4)) + 
               Power(t1,2)*(10 + 66*t2 - 151*Power(t2,2) + 
                  182*Power(t2,3) - 68*Power(t2,4) + 5*Power(t2,5) + 
                  s2*(4 - 44*t2 + 22*Power(t2,2) + 14*Power(t2,3) - 
                     4*Power(t2,4))) + 
               t1*(3 + 26*t2 + 65*Power(t2,2) + 44*Power(t2,3) - 
                  110*Power(t2,4) + 18*Power(t2,5) + 2*Power(t2,6) - 
                  Power(s2,2)*t2*
                   (-11 + 3*t2 + 5*Power(t2,2) + 7*Power(t2,3)) - 
                  s2*(5 + 59*t2 - 84*Power(t2,2) + 83*Power(t2,3) - 
                     9*Power(t2,4) + 2*Power(t2,5))) + 
               t2*(-1 - 401*t2 + 978*Power(t2,2) - 845*Power(t2,3) + 
                  298*Power(t2,4) - 30*Power(t2,5) + Power(t2,6) + 
                  Power(s2,3)*t2*(3 + 2*t2 + 3*Power(t2,2)) + 
                  Power(s2,2)*
                   (5 - 10*t2 - 24*Power(t2,2) + 47*Power(t2,3) - 
                     6*Power(t2,4)) - 
                  s2*(7 + 48*t2 + 77*Power(t2,2) - 91*Power(t2,3) + 
                     4*Power(t2,4) + 3*Power(t2,5))))) + 
         Power(s,2)*(-1 + 3*Power(t1,2) - 2*Power(t1,3) - 9*t2 + 
            6*s2*t2 + 24*t1*t2 - 13*s2*t1*t2 - 22*Power(t1,2)*t2 + 
            7*s2*Power(t1,2)*t2 + 7*Power(t1,3)*t2 + 36*Power(t2,2) - 
            5*s2*Power(t2,2) - Power(s2,2)*Power(t2,2) - 
            104*t1*Power(t2,2) + 28*s2*t1*Power(t2,2) + 
            3*Power(s2,2)*t1*Power(t2,2) + 90*Power(t1,2)*Power(t2,2) - 
            40*s2*Power(t1,2)*Power(t2,2) + Power(t1,3)*Power(t2,2) - 
            190*Power(t2,3) + 151*s2*Power(t2,3) - 
            83*Power(s2,2)*Power(t2,3) + 6*Power(s2,3)*Power(t2,3) + 
            16*t1*Power(t2,3) + 14*s2*t1*Power(t2,3) + 
            20*Power(s2,2)*t1*Power(t2,3) - 
            112*Power(t1,2)*Power(t2,3) + 
            25*s2*Power(t1,2)*Power(t2,3) - 7*Power(t1,3)*Power(t2,3) + 
            405*Power(t2,4) - 176*s2*Power(t2,4) + 
            34*Power(s2,2)*Power(t2,4) - 3*Power(s2,3)*Power(t2,4) + 
            45*t1*Power(t2,4) + 65*s2*t1*Power(t2,4) - 
            41*Power(s2,2)*t1*Power(t2,4) + 41*Power(t1,2)*Power(t2,4) + 
            21*s2*Power(t1,2)*Power(t2,4) - 3*Power(t1,3)*Power(t2,4) - 
            376*Power(t2,5) + 40*s2*Power(t2,5) + 
            41*Power(s2,2)*Power(t2,5) + 3*Power(s2,3)*Power(t2,5) + 
            2*t1*Power(t2,5) - 62*s2*t1*Power(t2,5) + 
            2*Power(s2,2)*t1*Power(t2,5) - 27*Power(t1,2)*Power(t2,5) + 
            s2*Power(t1,2)*Power(t2,5) + 181*Power(t2,6) - 
            8*s2*Power(t2,6) - 16*Power(s2,2)*Power(t2,6) + 
            15*t1*Power(t2,6) + 17*s2*t1*Power(t2,6) + 
            3*Power(t1,2)*Power(t2,6) - 49*Power(t2,7) - 
            9*s2*Power(t2,7) + Power(s2,2)*Power(t2,7) + 
            2*t1*Power(t2,7) - s2*t1*Power(t2,7) + 3*Power(t2,8) + 
            s2*Power(t2,8) + Power(s1,4)*
             (-2*t1*(-4 + t2)*Power(-1 + t2,2) - 
               10*Power(-1 + t2,2)*(-2 + 3*t2) - 
               Power(t1,2)*(-10 + 25*t2 - 14*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,2)*(2 - 7*t2 + 2*Power(t2,2) + Power(t2,3)) - 
               2*s2*(3*Power(-1 + t2,2) + 
                  t1*(5 - 14*t2 + 7*Power(t2,2)))) - 
            Power(s1,2)*(-3 + 
               (190 + 27*s2 + 57*Power(s2,2) - 10*Power(s2,3))*t2 + 
               (-627 - 59*s2 + 60*Power(s2,2) + 5*Power(s2,3))*
                Power(t2,2) - 
               (-884 + 29*s2 + 108*Power(s2,2) + 9*Power(s2,3))*
                Power(t2,3) + 
               (-635 + 20*s2 + 29*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,4) + 2*(97 + 21*s2)*Power(t2,5) - 
               (3 + s2)*Power(t2,6) + 
               Power(t1,3)*(-10 + 73*t2 - 45*Power(t2,2) - 
                  9*Power(t2,3) + 3*Power(t2,4)) + 
               Power(t1,2)*(-8 + 241*t2 - 351*Power(t2,2) + 
                  199*Power(t2,3) - 44*Power(t2,4) + Power(t2,5) + 
                  s2*(5 - 88*t2 + 38*Power(t2,2) + 13*Power(t2,3) - 
                     2*Power(t2,4))) + 
               t1*(-2 + 7*t2 - 71*Power(t2,2) + 254*Power(t2,3) - 
                  207*Power(t2,4) + 19*Power(t2,5) + 
                  Power(s2,2)*
                   (-2 + 52*t2 - 35*Power(t2,2) + 26*Power(t2,3) - 
                     9*Power(t2,4)) + 
                  s2*(12 - 269*t2 + 209*Power(t2,2) - 29*Power(t2,3) + 
                     Power(t2,4)))) + 
            Power(s1,3)*(2*Power(t1,3)*(21 - 24*t2 + 5*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (-44 + 128*t2 - 115*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(-2 + t2 - 3*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(46 - 118*t2 + 134*Power(t2,2) - 
                  55*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(-23 + 22*t2 + 74*Power(t2,2) - 87*Power(t2,3) + 
                  13*Power(t2,4) + Power(t2,5)) + 
               Power(s2,2)*(t1*
                   (23 - 20*t2 + 7*Power(t2,2) - 2*Power(t2,3)) + 
                  t2*(26 - 16*t2 + Power(t2,2) - Power(t2,3))) + 
               s2*(5 + 7*t2 - 50*Power(t2,2) + 25*Power(t2,3) + 
                  13*Power(t2,4) + 
                  Power(t1,2)*(-59 + 59*t2 - 10*Power(t2,2)) + 
                  t1*(-16 + 7*t2 - 33*Power(t2,2) + 19*Power(t2,3) + 
                     3*Power(t2,4)))) + 
            s1*(Power(s2,3)*Power(t2,2)*
                (-14 + 7*t2 - 9*Power(t2,2) + 2*Power(t2,3)) - 
               Power(-1 + t2,2)*t2*
                (15 - 278*t2 + 242*Power(t2,2) - 148*Power(t2,3) + 
                  5*Power(t2,4)) - 
               2*Power(t1,3)*
                (1 + 7*t2 - 20*Power(t2,2) + Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(t1,2)*(3 - 68*t2 + 275*Power(t2,2) - 
                  245*Power(t2,3) + 94*Power(t2,4) - 6*Power(t2,5) + 
                  Power(t2,6)) - 
               t1*(1 - 51*t2 - 72*Power(t2,2) + 200*Power(t2,3) - 
                  215*Power(t2,4) + 138*Power(t2,5) - 2*Power(t2,6) + 
                  Power(t2,7)) + 
               Power(s2,2)*t2*
                (1 + 140*t2 - 2*Power(t2,2) - 126*Power(t2,3) + 
                  42*Power(t2,4) - Power(t2,5) + 
                  t1*(-5 + 9*t2 + 26*Power(t2,2) + 17*Power(t2,3) - 
                     7*Power(t2,4))) + 
               s2*(-1 - 8*t2 - 119*Power(t2,2) + 118*Power(t2,3) - 
                  38*Power(t2,4) + 12*Power(t2,5) + 38*Power(t2,6) - 
                  2*Power(t2,7) + 
                  Power(t1,2)*
                   (-2 + 32*t2 - 44*Power(t2,2) - 40*Power(t2,3) + 
                     15*Power(t2,4) + Power(t2,5)) + 
                  t1*(3 + 10*t2 - 287*Power(t2,2) + 143*Power(t2,3) + 
                     52*Power(t2,4) - 27*Power(t2,5) - 2*Power(t2,6))))) \
- s*(1 - 3*t1 + 3*Power(t1,2) - Power(t1,3) - 9*t2 + 2*s2*t2 + 29*t1*t2 - 
            4*s2*t1*t2 - 31*Power(t1,2)*t2 + 2*s2*Power(t1,2)*t2 + 
            11*Power(t1,3)*t2 + 49*Power(t2,2) - 17*s2*Power(t2,2) - 
            Power(s2,2)*Power(t2,2) - 137*t1*Power(t2,2) + 
            45*s2*t1*Power(t2,2) + Power(s2,2)*t1*Power(t2,2) + 
            109*Power(t1,2)*Power(t2,2) - 28*s2*Power(t1,2)*Power(t2,2) - 
            21*Power(t1,3)*Power(t2,2) - 160*Power(t2,3) + 
            168*s2*Power(t2,3) - 38*Power(s2,2)*Power(t2,3) + 
            2*Power(s2,3)*Power(t2,3) + 166*t1*Power(t2,3) - 
            165*s2*t1*Power(t2,3) + 21*Power(s2,2)*t1*Power(t2,3) - 
            89*Power(t1,2)*Power(t2,3) + 49*s2*Power(t1,2)*Power(t2,3) + 
            6*Power(t1,3)*Power(t2,3) + 263*Power(t2,4) - 
            284*s2*Power(t2,4) + 108*Power(s2,2)*Power(t2,4) - 
            12*Power(s2,3)*Power(t2,4) - 53*t1*Power(t2,4) + 
            141*s2*t1*Power(t2,4) - 37*Power(s2,2)*t1*Power(t2,4) + 
            14*Power(t1,2)*Power(t2,4) - 19*s2*Power(t1,2)*Power(t2,4) + 
            7*Power(t1,3)*Power(t2,4) - 211*Power(t2,5) + 
            118*s2*Power(t2,5) - 48*Power(s2,2)*Power(t2,5) + 
            9*Power(s2,3)*Power(t2,5) + 25*t1*Power(t2,5) - 
            45*s2*t1*Power(t2,5) + 22*Power(s2,2)*t1*Power(t2,5) - 
            12*Power(t1,2)*Power(t2,5) - 10*s2*Power(t1,2)*Power(t2,5) + 
            81*Power(t2,6) + 19*s2*Power(t2,6) - 
            23*Power(s2,2)*Power(t2,6) - Power(s2,3)*Power(t2,6) - 
            23*t1*Power(t2,6) + 30*s2*t1*Power(t2,6) - 
            Power(s2,2)*t1*Power(t2,6) + 6*Power(t1,2)*Power(t2,6) - 
            20*Power(t2,7) - 8*s2*Power(t2,7) + 
            2*Power(s2,2)*Power(t2,7) - 4*t1*Power(t2,7) - 
            2*s2*t1*Power(t2,7) + 6*Power(t2,8) + 2*s2*Power(t2,8) - 
            2*Power(s1,5)*Power(-1 + t2,2)*
             (-2*Power(s2,2) - 4*Power(t1,2) + t1*(-3 + t2) + 
               2*(-1 + t2) + s2*(1 + 6*t1 + t2)) + 
            Power(s1,4)*(Power(s2,3)*(-5 + 3*t2) + 
               2*Power(-1 + t2,3)*(-1 + 11*t2) + 
               t1*Power(-1 + t2,2)*(-2 - 33*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(28 - 31*t2 + 5*Power(t2,2)) - 
               Power(t1,2)*(-28 + 58*t2 - 49*Power(t2,2) + 
                  18*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(-8 + 11*t2 + 20*Power(t2,2) - 
                  23*Power(t2,3) - 3*t1*(-10 + 7*t2 + Power(t2,2))) + 
               s2*(Power(t1,2)*(-53 + 49*t2 - 2*Power(t2,2)) + 
                  Power(-1 + t2,2)*(8 + 17*t2 + 5*Power(t2,2)) + 
                  t1*(-4 - t2 - 21*Power(t2,2) + 25*Power(t2,3) + 
                     Power(t2,4)))) + 
            Power(s1,3)*(-(Power(-1 + t2,3)*
                  (20 - 7*t2 + 45*Power(t2,2))) + 
               Power(s2,3)*(-2 + 27*t2 - 18*Power(t2,2) + Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (-29 + 24*t2 - 71*Power(t2,2) + 4*Power(t2,3)) - 
               Power(t1,3)*(17 + 26*t2 - 30*Power(t2,2) - 
                  6*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(45 - 224*t2 + 224*Power(t2,2) - 
                  57*Power(t2,3) + 11*Power(t2,4) + Power(t2,5)) + 
               Power(s2,2)*(24 - 37*t2 - 58*Power(t2,2) + 
                  29*Power(t2,3) + 42*Power(t2,4) + 
                  t1*(-7 - 100*t2 + 90*Power(t2,2) - 8*Power(t2,3) + 
                     Power(t2,4))) - 
               s2*(Power(-1 + t2,2)*
                   (1 + 54*t2 + 11*Power(t2,2) + 6*Power(t2,3)) + 
                  Power(t1,2)*
                   (-34 - 75*t2 + 78*Power(t2,2) + 7*Power(t2,3)) + 
                  t1*(85 - 286*t2 + 144*Power(t2,2) + 28*Power(t2,3) + 
                     27*Power(t2,4) + 2*Power(t2,5)))) + 
            s1*(Power(s2,3)*Power(t2,2)*
                (-6 + 41*t2 - 30*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,3)*(-7 + 18*t2 - 14*Power(t2,2) - 
                  6*Power(t2,3) + Power(t2,4)) - 
               Power(-1 + t2,3)*
                (5 - 29*t2 + 123*Power(t2,2) - 15*Power(t2,3) + 
                  24*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (17 - 108*t2 - 175*Power(t2,2) - 14*Power(t2,3) - 
                  18*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,2)*(19 - 116*t2 + 78*Power(t2,2) - 
                  39*Power(t2,3) + 53*Power(t2,4) + 3*Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s2,2)*t2*
                (2 + 100*t2 - 269*Power(t2,2) + 72*Power(t2,3) + 
                  91*Power(t2,4) + 4*Power(t2,5) + 
                  t1*(-2 - 49*t2 + 34*Power(t2,2) + 4*Power(t2,3) - 
                     12*Power(t2,4) + Power(t2,5))) + 
               s2*(Power(t1,2)*
                   (-2 + 31*t2 - 14*Power(t2,2) - 13*Power(t2,3) + 
                     20*Power(t2,4) + 2*Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (2 - 5*t2 + 275*Power(t2,2) + 43*Power(t2,3) - 
                     25*Power(t2,4) + 6*Power(t2,5)) + 
                  t1*(4 - 40*t2 + 145*Power(t2,2) + 36*Power(t2,3) - 
                     52*Power(t2,4) - 84*Power(t2,5) - 9*Power(t2,6)))) + 
            Power(s1,2)*(-3*Power(s2,3)*t2*
                (-2 + 17*t2 - 12*Power(t2,2) + Power(t2,3)) + 
               Power(-1 + t2,3)*
                (-1 + 85*t2 - 17*Power(t2,2) + 45*Power(t2,3)) + 
               Power(t1,2)*(25 - 43*t2 + 212*Power(t2,2) - 
                  197*Power(t2,3) + 3*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (-23 - 174*t2 + 49*Power(t2,2) - 61*Power(t2,3) + 
                  3*Power(t2,4)) - 
               Power(t1,3)*(3 - 28*t2 + 6*Power(t2,3) + 7*Power(t2,4)) + 
               Power(s2,2)*(-1 - 86*t2 + 206*Power(t2,2) + 
                  19*Power(t2,3) - 109*Power(t2,4) - 29*Power(t2,5) + 
                  t1*(1 + 35*t2 + 73*Power(t2,2) - 95*Power(t2,3) + 
                     24*Power(t2,4) - 2*Power(t2,5))) + 
               s2*(Power(-1 + t2,2)*
                   (8 + 136*t2 + 80*Power(t2,2) - 25*Power(t2,3) + 
                     7*Power(t2,4)) + 
                  Power(t1,2)*
                   (-3 - 69*t2 + 10*Power(t2,2) + 19*Power(t2,3) + 
                     7*Power(t2,4)) + 
                  t1*(-5 + 105*t2 - 459*Power(t2,2) + 254*Power(t2,3) + 
                     79*Power(t2,4) + 25*Power(t2,5) + Power(t2,6))))))*
       B1(s,t2,s1))/
     (s*(-1 + s1)*(-1 + s2)*(-1 + t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) + 
    (8*(-2*Power(s1,12)*Power(s2 - t1,2)*(-1 + t2) - 
         2*Power(-1 + s,3)*s*(1 - 3*s + Power(s,2))*Power(t2,4)*(1 + t2)*
          Power(-1 + s + t2,2) - 
         Power(s1,11)*(s2 - t1)*
          (4 - 10*t1 + 5*Power(t1,2) - 8*t2 + 4*t1*t2 - Power(t1,2)*t2 + 
            4*Power(t2,2) + 6*t1*Power(t2,2) + Power(s2,2)*(1 + 3*t2) + 
            2*s*(-2 - 6*t1 + s2*(4 - 5*t2) + 2*t2 + 7*t1*t2) - 
            2*s2*(-3 - 2*t2 + 5*Power(t2,2) + t1*(3 + t2))) + 
         (-1 + s)*s*s1*Power(t2,3)*(-1 + s + t2)*
          (2*Power(s,6)*t2*(2 - t1 + t2) + 
            2*(-1 + t2)*(5 + (22 - 4*s2)*t2 + (21 - 2*s2)*Power(t2,2) + 
               2*Power(t2,3) + t1*(-1 + t2 - 2*Power(t2,2))) + 
            Power(s,5)*(8 - (7 + 4*s2)*t2 - 2*(-2 + s2)*Power(t2,2) + 
               2*Power(t2,3) + t1*(-3 + 12*t2 - 2*Power(t2,2))) + 
            Power(s,4)*(-51 + 5*(-6 + 5*s2)*t2 + 
               (-35 + 8*s2)*Power(t2,2) - 2*(-1 + s2)*Power(t2,3) + 
               t1*(20 - 29*t2 + 14*Power(t2,2))) + 
            Power(s,3)*(121 + (121 - 65*s2)*t2 + 
               (69 - 11*s2)*Power(t2,2) + 10*(-3 + s2)*Power(t2,3) + 
               t1*(-44 + 45*t2 - 32*Power(t2,2) + 4*Power(t2,3))) - 
            Power(s,2)*(131 + (196 - 79*s2)*t2 + 
               (73 + 4*s2)*Power(t2,2) + (-84 + 22*s2)*Power(t2,3) - 
               4*Power(t2,4) + 
               3*t1*(-14 + 15*t2 - 12*Power(t2,2) + 4*Power(t2,3))) + 
            s*(63 + (142 - 43*s2)*t2 + (11 + 13*s2)*Power(t2,2) + 
               2*(-58 + 9*s2)*Power(t2,3) - 8*Power(t2,4) + 
               t1*(-17 + 23*t2 - 22*Power(t2,2) + 12*Power(t2,3)))) + 
         Power(s1,2)*Power(t2,2)*
          (-4*Power(s,9)*t2*(t1 + t2) - 
            Power(-1 + t1 + t2 - s2*t2,2)*
             (3 + (-16 + s2)*t2 + (13 - 5*s2)*Power(t2,2) + 
               t1*(-3 + 5*t2 + 2*Power(t2,2))) + 
            Power(s,8)*(6 + Power(t1,2)*(6 - 4*t2) + 13*t2 + 
               2*(-8 + s2)*Power(t2,2) - 26*Power(t2,3) + 
               t1*(-15 + 8*s2*t2 + 14*Power(t2,2))) + 
            Power(s,7)*(-27 + 3*Power(t1,3) - 6*(16 + s2)*t2 + 
               (155 + 16*s2)*Power(t2,2) + 3*(3 + 8*s2)*Power(t2,3) - 
               28*Power(t2,4) + 
               Power(t1,2)*(-66 + (25 + 2*s2)*t2 - 2*Power(t2,2)) + 
               t1*(118 + (148 - 72*s2)*t2 + (-149 + 4*s2)*Power(t2,2) + 
                  22*Power(t2,3))) + 
            Power(s,6)*(19 + 6*(45 + 8*s2)*t2 - 
               (478 + 137*s2)*Power(t2,2) + 
               (290 - 95*s2 - 8*Power(s2,2))*Power(t2,3) + 
               (-13 + 20*s2)*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,3)*(-15 + 5*t2 + 2*Power(t2,2)) - 
               Power(t1,2)*(-243 + (111 + 13*s2)*t2 + 13*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-371 + (-694 + 282*s2)*t2 + 
                  (642 - 36*s2 + 4*Power(s2,2))*Power(t2,2) + 
                  (-165 + 8*s2)*Power(t2,3) + 8*Power(t2,4))) + 
            Power(s,5)*(92 - (295 + 136*s2)*t2 + 
               (855 + 414*s2 - 2*Power(s2,2))*Power(t2,2) + 
               (-926 + 140*s2 + 53*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) + 
               (257 - 69*s2 - 2*Power(s2,2))*Power(t2,4) + 
               2*(-11 + s2)*Power(t2,5) + 2*Power(t2,6) + 
               Power(t1,3)*(27 - 20*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(-426 + 6*(43 + 7*s2)*t2 + 
                  (52 + 3*s2)*Power(t2,2) + 4*(1 + s2)*Power(t2,3)) + 
               t1*(595 + (1490 - 600*s2)*t2 + 
                  (-1587 + 204*s2 - 31*Power(s2,2))*Power(t2,2) - 
                  8*(-58 + 5*s2)*Power(t2,3) + 
                  (-74 + 4*s2)*Power(t2,4) - 2*Power(t2,5))) - 
            Power(s,2)*(119 + (159 - 16*s2)*t2 + 
               (425 - 298*s2 + 76*Power(s2,2))*Power(t2,2) + 
               (-921 + 169*s2 - 163*Power(s2,2) + 32*Power(s2,3))*
                Power(t2,3) + 
               (126 - 61*s2 + 12*Power(s2,2) - 19*Power(s2,3))*
                Power(t2,4) + 
               (88 + 88*s2 - 8*Power(s2,2))*Power(t2,5) - 
               2*(-2 + s2)*Power(t2,6) + 
               Power(t1,3)*(-27 + 25*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(39 + (-205 + 87*s2)*t2 + 
                  (55 - 78*s2)*Power(t2,2) - 4*(5 + 2*s2)*Power(t2,3)) \
+ t1*(-41 + (818 - 286*s2)*t2 - 
                  2*(527 - 354*s2 + 47*Power(s2,2))*Power(t2,2) + 
                  (457 - 244*s2 + 72*Power(s2,2))*Power(t2,3) + 
                  16*(-4 + 3*s2)*Power(t2,4) + (4 - 12*s2)*Power(t2,5))) \
+ Power(s,4)*(-231 + (-50 + 183*s2)*t2 - 
               (889 + 512*s2 + 15*Power(s2,2))*Power(t2,2) + 
               (1527 + 77*s2 - 119*Power(s2,2) - 15*Power(s2,3))*
                Power(t2,3) + 
               (-506 + 182*s2 + 11*Power(s2,2))*Power(t2,4) + 
               44*Power(t2,5) - 2*(2 + s2)*Power(t2,6) + 
               5*Power(t1,3)*(-3 + 5*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(375 - 5*(50 + 17*s2)*t2 + 
                  3*(-19 + s2)*Power(t2,2) - 4*(-5 + 3*s2)*Power(t2,3)) \
+ t1*(-496 + (-1847 + 754*s2)*t2 + 
                  (2285 - 572*s2 + 89*Power(s2,2))*Power(t2,2) + 
                  (-795 + 122*s2 - 9*Power(s2,2))*Power(t2,3) + 
                  2*(73 - 10*s2 + Power(s2,2))*Power(t2,4) + 
                  4*(-3 + s2)*Power(t2,5))) + 
            Power(s,3)*(233 - 15*Power(t1,3) + (323 - 116*s2)*t2 + 
               (604 + 101*s2 + 62*Power(s2,2))*Power(t2,2) + 
               (-1427 - 210*s2 + 25*Power(s2,2) + 34*Power(s2,3))*
                Power(t2,3) - 
               (-507 + 180*s2 + 36*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,4) + 
               (26 + 52*s2 - 10*Power(s2,2))*Power(t2,5) + 
               2*(-2 + s2)*Power(t2,6) + 
               Power(t1,2)*(-126 + 11*(-1 + 10*s2)*t2 + 
                  (38 - 42*s2)*Power(t2,2) + 8*(-5 + s2)*Power(t2,3)) + 
               t1*(164 + (1482 - 584*s2)*t2 - 
                  3*(651 - 284*s2 + 42*Power(s2,2))*Power(t2,2) + 
                  (814 - 224*s2 + 42*Power(s2,2))*Power(t2,3) - 
                  4*(33 - 12*s2 + Power(s2,2))*Power(t2,4) - 
                  12*(-2 + s2)*Power(t2,5))) + 
            s*(30 + 2*(-14 + 9*s2)*t2 + 
               (246 - 227*s2 + 36*Power(s2,2))*Power(t2,2) + 
               2*(-197 + 151*s2 - 71*Power(s2,2) + 6*Power(s2,3))*
                Power(t2,3) + 
               (22 - 93*s2 + 62*Power(s2,2) - 17*Power(s2,3))*
                Power(t2,4) + 2*(57 + s2 + Power(s2,2))*Power(t2,5) - 
               2*(-5 + s2)*Power(t2,6) + 
               Power(t1,3)*(-15 + 20*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(42 + 2*(-72 + 19*s2)*t2 + 
                  (56 - 57*s2)*Power(t2,2) + (4 - 12*s2)*Power(t2,3)) - 
               t1*(45 + 4*(-73 + 22*s2)*t2 + 
                  (375 - 316*s2 + 35*Power(s2,2))*Power(t2,2) - 
                  2*(72 - 76*s2 + 27*Power(s2,2))*Power(t2,3) - 
                  2*(-5 + 6*s2 + 2*Power(s2,2))*Power(t2,4) + 
                  (6 + 4*s2)*Power(t2,5)))) + 
         Power(s1,3)*t2*(6 - 18*t1 + 18*Power(t1,2) - 6*Power(t1,3) - 
            48*t2 + 21*s2*t2 + 93*t1*t2 - 42*s2*t1*t2 - 
            42*Power(t1,2)*t2 + 21*s2*Power(t1,2)*t2 - 
            3*Power(t1,3)*t2 + 72*Power(t2,2) - 141*s2*Power(t2,2) + 
            20*Power(s2,2)*Power(t2,2) - 11*t1*Power(t2,2) + 
            162*s2*t1*Power(t2,2) - 20*Power(s2,2)*t1*Power(t2,2) - 
            94*Power(t1,2)*Power(t2,2) - 21*s2*Power(t1,2)*Power(t2,2) + 
            33*Power(t1,3)*Power(t2,2) + 12*Power(t2,3) + 
            129*s2*Power(t2,3) - 112*Power(s2,2)*Power(t2,3) + 
            5*Power(s2,3)*Power(t2,3) - 177*t1*Power(t2,3) + 
            74*s2*t1*Power(t2,3) + 47*Power(s2,2)*t1*Power(t2,3) + 
            98*Power(t1,2)*Power(t2,3) - 88*s2*Power(t1,2)*Power(t2,3) + 
            12*Power(t1,3)*Power(t2,3) - 78*Power(t2,4) + 
            81*s2*Power(t2,4) + 20*Power(s2,2)*Power(t2,4) - 
            23*Power(s2,3)*Power(t2,4) + 105*t1*Power(t2,4) - 
            178*s2*t1*Power(t2,4) + 73*Power(s2,2)*t1*Power(t2,4) + 
            20*Power(t1,2)*Power(t2,4) - 20*s2*Power(t1,2)*Power(t2,4) + 
            36*Power(t2,5) - 90*s2*Power(t2,5) + 
            72*Power(s2,2)*Power(t2,5) - 18*Power(s2,3)*Power(t2,5) + 
            8*t1*Power(t2,5) - 16*s2*t1*Power(t2,5) + 
            8*Power(s2,2)*t1*Power(t2,5) + 
            2*Power(s,9)*t2*(-2 + 3*t1 + t2) + 
            Power(s,8)*t2*(18 + 10*Power(t1,2) - 20*t2 + 
               39*Power(t2,2) + s2*(4 - 8*t1 + 2*t2) + t1*(-40 + 23*t2)) \
+ Power(s,7)*(12 - (26 + 35*s2)*t2 - 2*(41 + 14*s2)*Power(t2,2) - 
               (53 + 31*s2)*Power(t2,3) + 105*Power(t2,4) + 
               5*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(12 - 123*t2 + 35*Power(t2,2)) + 
               t1*(-30 + 2*(61 + 44*s2)*t2 + 
                  (113 - 40*s2)*Power(t2,2) - 37*Power(t2,3))) + 
            Power(s,6)*(-66 + 3*(-36 + 43*s2)*t2 + 
               (677 + 172*s2)*Power(t2,2) + 
               (-368 + 75*s2 + 24*Power(s2,2))*Power(t2,3) + 
               (4 - 81*s2)*Power(t2,4) + 62*Power(t2,5) + 
               Power(t1,3)*(-14 - 33*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-105 + (631 - 6*s2)*t2 - 
                  (144 + 5*s2)*Power(t2,2) + 25*Power(t2,3)) - 
               t1*(-215 + (60 + 412*s2)*t2 + 
                  2*(554 - 127*s2 + 6*Power(s2,2))*Power(t2,2) + 
                  (-515 + 44*s2)*Power(t2,3) + 42*Power(t2,4))) + 
            Power(s,5)*(141 + (563 - 273*s2)*t2 + 
               2*(-783 - 301*s2 + 3*Power(s2,2))*Power(t2,2) - 
               (-1482 + 2*s2 + 173*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,3) + 
               (-690 + 152*s2 + 37*Power(s2,2))*Power(t2,4) + 
               (167 - 28*s2)*Power(t2,5) - 2*Power(t2,6) + 
               Power(t1,3)*(1 + 73*t2 - 32*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,2)*(321 + 3*(-430 + s2)*t2 + 
                  (502 - 24*s2)*Power(t2,2) + 
                  (41 - 9*s2)*Power(t2,3) + 24*Power(t2,4)) - 
               t1*(581 + (785 - 1006*s2)*t2 + 
                  (-3187 + 866*s2 - 103*Power(s2,2))*Power(t2,2) + 
                  (1743 - 224*s2 + 15*Power(s2,2))*Power(t2,3) + 
                  4*(-83 + 15*s2)*Power(t2,4) + 6*Power(t2,5))) + 
            Power(s,4)*(-138 + (-923 + 365*s2)*t2 + 
               (1856 + 809*s2 + 49*Power(s2,2))*Power(t2,2) + 
               (-2031 - 814*s2 + 515*Power(s2,2) + 34*Power(s2,3))*
                Power(t2,3) + 
               (1232 - 307*s2 - 160*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,4) + 
               3*(-141 - 19*s2 + 5*Power(s2,2))*Power(t2,5) + 
               (78 + 8*s2)*Power(t2,6) - 6*Power(t2,7) + 
               Power(t1,3)*(30 - 65*t2 + 31*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(-456 + 3*(354 + 23*s2)*t2 + 
                  (-626 + 77*s2)*Power(t2,2) - 
                  4*(5 + s2)*Power(t2,3) + (4 - 20*s2)*Power(t2,4)) + 
               t1*(756 + (2359 - 1426*s2)*t2 - 
                  2*(2304 - 835*s2 + 153*Power(s2,2))*Power(t2,2) + 
                  (2875 - 886*s2 + 121*Power(s2,2))*Power(t2,3) + 
                  (-615 + 202*s2 + Power(s2,2))*Power(t2,4) - 
                  28*(-4 + s2)*Power(t2,5) + 8*Power(t2,6))) + 
            Power(s,2)*(30 + (-355 + 209*s2)*t2 + 
               (1365 - 1006*s2 + 267*Power(s2,2))*Power(t2,2) + 
               (-270 - 88*s2 - 503*Power(s2,2) + 129*Power(s2,3))*
                Power(t2,3) - 
               (801 - 75*s2 + 74*Power(s2,2) + 64*Power(s2,3))*
                Power(t2,4) + 
               (-11 + 210*s2 + 33*Power(s2,2) - 8*Power(s2,3))*
                Power(t2,5) + 
               4*(12 - 19*s2 + 3*Power(s2,2))*Power(t2,6) + 
               (-6 + 4*s2)*Power(t2,7) + 
               Power(t1,3)*(-10 + 5*t2 + 11*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-33 + (-397 + 204*s2)*t2 + 
                  (56 - 99*s2)*Power(t2,2) - 
                  5*(11 + 12*s2)*Power(t2,3) + 8*(-7 + s2)*Power(t2,4)) \
+ t1*(71 - 8*(-237 + 88*s2)*t2 - 
                  2*(954 - 877*s2 + 167*Power(s2,2))*Power(t2,2) + 
                  (511 - 632*s2 + 240*Power(s2,2))*Power(t2,3) + 
                  (10 + 408*s2 - 30*Power(s2,2))*Power(t2,4) + 
                  (92 - 84*s2)*Power(t2,5) + 8*s2*Power(t2,6))) + 
            s*(-27 + (170 - 93*s2)*t2 + 
               (-691 + 754*s2 - 133*Power(s2,2))*Power(t2,2) + 
               (409 - 718*s2 + 558*Power(s2,2) - 59*Power(s2,3))*
                Power(t2,3) + 
               2*(258 + 73*s2 - 78*Power(s2,2) + 37*Power(s2,3))*
                Power(t2,4) + 
               (-189 - 83*s2 + 31*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,5) + 
               2*(-91 - 6*s2 + 3*Power(s2,2))*Power(t2,6) + 
               6*(-1 + s2)*Power(t2,7) + 
               Power(t1,3)*(19 + 3*t2 - 64*Power(t2,2) - 
                  24*Power(t2,3)) + 
               Power(t1,2)*(-51 - 3*(-76 + 35*s2)*t2 + 
                  4*(39 + 22*s2)*Power(t2,2) + 
                  (-51 + 151*s2)*Power(t2,3) + 8*(3 + 2*s2)*Power(t2,4)\
) + t1*(51 + 3*(-207 + 82*s2)*t2 + 
                  (439 - 850*s2 + 129*Power(s2,2))*Power(t2,2) + 
                  (231 - 32*s2 - 145*Power(s2,2))*Power(t2,3) - 
                  2*(73 - 16*s2 + 41*Power(s2,2))*Power(t2,4) - 
                  2*(-21 + 6*s2 + 2*Power(s2,2))*Power(t2,5) + 
                  4*(1 + s2)*Power(t2,6))) + 
            Power(s,3)*(42 + (713 - 327*s2)*t2 + 
               (-1565 + 40*s2 - 209*Power(s2,2))*Power(t2,2) + 
               (944 + 1305*s2 - 309*Power(s2,2) - 105*Power(s2,3))*
                Power(t2,3) + 
               (-522 - 86*s2 + 381*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,4) + 
               (186 - 226*s2 + 13*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,5) + 
               2*(-23 - 18*s2 + 5*Power(s2,2))*Power(t2,6) + 
               (2 + 6*s2)*Power(t2,7) + 
               Power(t1,3)*(-25 + 15*t2 + 16*Power(t2,2)) + 
               Power(t1,2)*(294 - (79 + 186*s2)*t2 + 
                  (115 - 16*s2)*Power(t2,2) + 
                  2*(-19 + 5*s2)*Power(t2,3) + 16*(-1 + s2)*Power(t2,4)) \
+ t1*(-464 + 2*(-1485 + 626*s2)*t2 + 
                  (3873 - 2084*s2 + 440*Power(s2,2))*Power(t2,2) + 
                  (-2079 + 1296*s2 - 248*Power(s2,2))*Power(t2,3) + 
                  (636 - 500*s2 + 38*Power(s2,2))*Power(t2,4) - 
                  4*(26 - 19*s2 + Power(s2,2))*Power(t2,5) - 
                  4*(-5 + 3*s2)*Power(t2,6)))) + 
         Power(s1,10)*(2 - 16*t1 + 43*Power(t1,2) - 27*Power(t1,3) - 
            6*t2 + 20*t1*t2 - 18*Power(t1,2)*t2 - 9*Power(t1,3)*t2 + 
            6*Power(t2,2) + 8*t1*Power(t2,2) - 
            19*Power(t1,2)*Power(t2,2) - 2*Power(t2,3) - 
            12*t1*Power(t2,3) - 6*Power(t1,2)*Power(t2,3) + 
            Power(s2,3)*(2 + 19*t2 + 15*Power(t2,2)) - 
            Power(s2,2)*(-21 - 12*t2 + 13*Power(t2,2) + 
               20*Power(t2,3) + t1*(23 + 63*t2 + 22*Power(t2,2))) + 
            s2*(4*Power(-1 + t2,2)*(3 + 4*t2) + 
               Power(t1,2)*(48 + 53*t2 + 7*Power(t2,2)) + 
               t1*(-62 + 38*Power(t2,2) + 24*Power(t2,3))) + 
            Power(s,2)*(-4*Power(s2,2)*(-3 + 5*t2) - 
               6*Power(t1,2)*(-5 + 7*t2) + 
               t1*(25 - 38*t2 + 9*Power(t2,2)) + 
               2*(-1 + 5*t2 - 6*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-7 + 6*t2 + 5*Power(t2,2) + 20*t1*(-2 + 3*t2))) + 
            s*(2*Power(-1 + t2,3) + Power(t1,3)*(-32 + 7*t2) + 
               Power(s2,3)*(7 + 10*t2) + 
               t1*(4 + 32*t2 - 36*Power(t2,2)) + 
               Power(t1,2)*(28 - 19*t2 - 35*Power(t2,2)) - 
               Power(s2,2)*(-22 + 3*t2 + 45*Power(t2,2) + 
                  t1*(38 + 21*t2)) + 
               s2*(-2 - 30*t2 + 26*Power(t2,2) + 6*Power(t2,3) + 
                  Power(t1,2)*(63 + 4*t2) + 
                  26*t1*(-1 - t2 + 4*Power(t2,2))))) + 
         Power(s1,4)*(-3 + 9*t1 - 9*Power(t1,2) + 3*Power(t1,3) + 
            30*t2 - 21*s2*t2 - 39*t1*t2 + 42*s2*t1*t2 - 
            12*Power(t1,2)*t2 - 21*s2*Power(t1,2)*t2 + 
            21*Power(t1,3)*t2 + 30*Power(t2,2) + 153*s2*Power(t2,2) - 
            30*Power(s2,2)*Power(t2,2) - 255*t1*Power(t2,2) - 
            126*s2*t1*Power(t2,2) + 30*Power(s2,2)*t1*Power(t2,2) + 
            270*Power(t1,2)*Power(t2,2) - 
            27*s2*Power(t1,2)*Power(t2,2) - 45*Power(t1,3)*Power(t2,2) - 
            208*Power(t2,3) + 87*s2*Power(t2,3) + 
            168*Power(s2,2)*Power(t2,3) - 10*Power(s2,3)*Power(t2,3) + 
            413*t1*Power(t2,3) - 542*s2*t1*Power(t2,3) - 
            38*Power(s2,2)*t1*Power(t2,3) - 2*Power(t1,2)*Power(t2,3) + 
            225*s2*Power(t1,2)*Power(t2,3) - 
            93*Power(t1,3)*Power(t2,3) + 189*Power(t2,4) - 
            461*s2*Power(t2,4) + 180*Power(s2,2)*Power(t2,4) + 
            40*Power(s2,3)*Power(t2,4) + 38*t1*Power(t2,4) + 
            306*s2*t1*Power(t2,4) - 270*Power(s2,2)*t1*Power(t2,4) - 
            207*Power(t1,2)*Power(t2,4) + 
            215*s2*Power(t1,2)*Power(t2,4) - 
            30*Power(t1,3)*Power(t2,4) - 6*Power(t2,5) + 
            154*s2*Power(t2,5) - 238*Power(s2,2)*Power(t2,5) + 
            90*Power(s2,3)*Power(t2,5) - 154*t1*Power(t2,5) + 
            296*s2*t1*Power(t2,5) - 142*Power(s2,2)*t1*Power(t2,5) - 
            40*Power(t1,2)*Power(t2,5) + 40*s2*Power(t1,2)*Power(t2,5) - 
            32*Power(t2,6) + 88*s2*Power(t2,6) - 
            80*Power(s2,2)*Power(t2,6) + 24*Power(s2,3)*Power(t2,6) - 
            12*t1*Power(t2,6) + 24*s2*t1*Power(t2,6) - 
            12*Power(s2,2)*t1*Power(t2,6) - 
            Power(s,8)*t2*(8 + 2*(-13 + s2)*t2 + 17*Power(t2,2) + 
               3*t1*(-4 + 9*t2)) - 
            Power(s,5)*(31 + (114 - 202*s2)*t2 + 
               (-987 - 306*s2 + 6*Power(s2,2))*Power(t2,2) + 
               (1605 + 82*s2 - 201*Power(s2,2))*Power(t2,3) + 
               (-850 + 146*s2 + 101*Power(s2,2))*Power(t2,4) + 
               (299 - 124*s2)*Power(t2,5) + 42*Power(t2,6) + 
               Power(t1,3)*(16 - 35*t2 - 65*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,2)*(27 + (-891 + 76*s2)*t2 - 
                  3*(-517 + 46*s2)*Power(t2,2) - 
                  2*(127 + 3*s2)*Power(t2,3) + 95*Power(t2,4)) + 
               t1*(-88 + (207 + 452*s2)*t2 + 
                  (1812 - 1022*s2 + 123*Power(s2,2))*Power(t2,2) + 
                  (-2755 + 514*s2 - 45*Power(s2,2))*Power(t2,3) + 
                  (920 - 204*s2)*Power(t2,4) - 22*Power(t2,5))) + 
            Power(s,4)*(61 - 9*(-61 + 39*s2)*t2 - 
               (1484 + 645*s2 + 57*Power(s2,2))*Power(t2,2) + 
               (2088 + 1153*s2 - 843*Power(s2,2) - 8*Power(s2,3))*
                Power(t2,3) + 
               (-2110 + 244*s2 + 435*Power(s2,2) - 14*Power(s2,3))*
                Power(t2,4) + 
               (1117 + 204*s2 - 93*Power(s2,2))*Power(t2,5) + 
               (-385 + 16*s2)*Power(t2,6) + 22*Power(t2,7) + 
               Power(t1,3)*(-3 + 55*t2 - 69*Power(t2,2) + 
                  39*Power(t2,3) + 10*Power(t2,4)) + 
               Power(t1,2)*(99 + (-1184 + 53*s2)*t2 - 
                  3*(-562 + 93*s2)*Power(t2,2) + 
                  (-834 + 197*s2)*Power(t2,3) + 
                  (-143 + 49*s2)*Power(t2,4) - 56*Power(t2,5)) - 
               t1*(193 + (581 - 790*s2)*t2 - 
                  2*(1845 - 895*s2 + 192*Power(s2,2))*Power(t2,2) + 
                  (4891 - 2098*s2 + 356*Power(s2,2))*Power(t2,3) + 
                  10*(-200 + 61*s2)*Power(t2,4) + 
                  (354 - 176*s2)*Power(t2,5) + 16*Power(t2,6))) + 
            Power(s,3)*(-54 + (-853 + 362*s2)*t2 + 
               (858 + 190*s2 + 255*Power(s2,2))*Power(t2,2) + 
               (-79 - 2070*s2 + 858*Power(s2,2) + 100*Power(s2,3))*
                Power(t2,3) + 
               (543 + 1180*s2 - 1262*Power(s2,2) + 63*Power(s2,3))*
                Power(t2,4) + 
               (-699 + 380*s2 + 97*Power(s2,2) - 36*Power(s2,3))*
                Power(t2,5) + 
               (338 + 239*s2 - 65*Power(s2,2))*Power(t2,6) - 
               2*(35 + 8*s2)*Power(t2,7) + 6*Power(t2,8) + 
               Power(t1,3)*(22 - 41*t2 + 34*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(-126 + (326 + 78*s2)*t2 + 
                  3*(-143 + 48*s2)*Power(t2,2) - 
                  2*(-73 + 74*s2)*Power(t2,3) + 
                  6*(-29 + 7*s2)*Power(t2,4) + 
                  8*(-9 + 5*s2)*Power(t2,5)) - 
               t1*(-192 + 4*(-461 + 210*s2)*t2 + 
                  6*(515 - 320*s2 + 94*Power(s2,2))*Power(t2,2) + 
                  (-3767 + 2916*s2 - 558*Power(s2,2))*Power(t2,3) + 
                  4*(582 - 509*s2 + 57*Power(s2,2))*Power(t2,4) + 
                  2*(-191 + 148*s2 + 8*Power(s2,2))*Power(t2,5) - 
                  20*(-5 + 3*s2)*Power(t2,6) + 12*Power(t2,7))) + 
            s*(5 + 5*(-41 + 20*s2)*t2 + 
               (464 - 828*s2 + 183*Power(s2,2))*Power(t2,2) + 
               (435 + 476*s2 - 853*Power(s2,2) + 116*Power(s2,3))*
                Power(t2,3) - 
               (754 + 243*s2 - 87*Power(s2,2) + 129*Power(s2,3))*
                Power(t2,4) + 
               2*(-259 + 129*s2 - 117*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,5) + 
               (431 + 243*s2 - 133*Power(s2,2) + 13*Power(s2,3))*
                Power(t2,6) - 18*(-8 + Power(s2,2))*Power(t2,7) - 
               2*(1 + 3*s2)*Power(t2,8) + 
               Power(t1,3)*(-4 - 11*t2 + 61*Power(t2,2) + 
                  36*Power(t2,3) + 16*Power(t2,4)) + 
               Power(t1,2)*(9 + 3*(-63 + 32*s2)*t2 + 
                  (-441 + 30*s2)*Power(t2,2) - 
                  2*(4 + 141*s2)*Power(t2,3) - 
                  5*(47 + 10*s2)*Power(t2,4) + 
                  8*(-13 + 3*s2)*Power(t2,5)) + 
               t1*(-8 + (555 - 228*s2)*t2 + 
                  (356 + 750*s2 - 177*Power(s2,2))*Power(t2,2) + 
                  (-853 + 822*s2 + 81*Power(s2,2))*Power(t2,3) + 
                  (22 + 216*s2 + 208*Power(s2,2))*Power(t2,4) + 
                  (-44 + 368*s2 - 38*Power(s2,2))*Power(t2,5) - 
                  4*(5 - s2 + Power(s2,2))*Power(t2,6) + 
                  4*(-2 + s2)*Power(t2,7))) + 
            Power(s,2)*(16 + (608 - 238*s2)*t2 + 
               (-651 + 865*s2 - 345*Power(s2,2))*Power(t2,2) + 
               (-1540 + 829*s2 + 445*Power(s2,2) - 198*Power(s2,3))*
                Power(t2,3) + 
               (1229 - 651*s2 + 489*Power(s2,2) + 56*Power(s2,3))*
                Power(t2,4) + 
               2*(99 + 209*s2 - 245*Power(s2,2) + 44*Power(s2,3))*
                Power(t2,5) + 
               (92 + 351*s2 - 52*Power(s2,2) - 11*Power(s2,3))*
                Power(t2,6) + 
               (48 + 16*s2 - 20*Power(s2,2))*Power(t2,7) - 
               6*s2*Power(t2,8) + 
               Power(t1,3)*(-11 - 23*t2 - 16*Power(t2,2) + 
                  30*Power(t2,3) + 4*Power(t2,4)) + 
               Power(t1,2)*(54 + (384 - 155*s2)*t2 + 70*Power(t2,2) + 
                  (85 + 2*s2)*Power(t2,3) + 
                  2*(-69 + 16*s2)*Power(t2,4) + 
                  24*(-2 + s2)*Power(t2,5)) + 
               t1*(-73 + 2*(-842 + 283*s2)*t2 + 
                  (701 - 1540*s2 + 438*Power(s2,2))*Power(t2,2) + 
                  (-517 + 1224*s2 - 290*Power(s2,2))*Power(t2,3) + 
                  (647 - 1192*s2 + 98*Power(s2,2))*Power(t2,4) + 
                  (-622 + 536*s2 - 68*Power(s2,2))*Power(t2,5) - 
                  8*(2 + 5*s2)*Power(t2,6) + 4*(-5 + 3*s2)*Power(t2,7))) \
- Power(s,6)*(-6 + (35 + 62*s2)*t2 + (207 + 47*s2)*Power(t2,2) + 
               (-170 + 3*s2 + 24*Power(s2,2))*Power(t2,3) - 
               (171 + 115*s2)*Power(t2,4) + 182*Power(t2,5) + 
               Power(t1,3)*(-11 + 37*t2 + 30*Power(t2,2)) - 
               t1*(-15 + 6*(26 + 23*s2)*t2 + 
                  (271 - 272*s2 + 12*Power(s2,2))*Power(t2,2) + 
                  (-607 + 116*s2)*Power(t2,3) + 45*Power(t2,4)) + 
               Power(t1,2)*t2*
                (236 - 454*t2 + 121*Power(t2,2) + 3*s2*(-9 + 2*t2))) + 
            Power(s,7)*(Power(t1,3)*(-2 + t2) + 
               Power(t1,2)*(20 - 2*s2 - 59*t2)*t2 + 
               t1*t2*(-56 + 70*t2 - 43*Power(t2,2) + 4*s2*(-4 + 9*t2)) + 
               t2*(28 - 55*t2 + 146*Power(t2,2) - 130*Power(t2,3) + 
                  s2*(8 + 8*t2 + 6*Power(t2,2))))) + 
         Power(s1,9)*(-6 + 55*t1 - 126*Power(t1,2) + 57*Power(t1,3) + 
            12*t2 - 69*t1*t2 + 22*Power(t1,2)*t2 + 69*Power(t1,3)*t2 - 
            15*t1*Power(t2,2) + 80*Power(t1,2)*Power(t2,2) + 
            15*Power(t1,3)*Power(t2,2) - 12*Power(t2,3) + 
            17*t1*Power(t2,3) + 22*Power(t1,2)*Power(t2,3) + 
            3*Power(t1,3)*Power(t2,3) + 6*Power(t2,4) + 
            12*t1*Power(t2,4) + 2*Power(t1,2)*Power(t2,4) - 
            2*Power(s2,3)*t2*(17 + 40*t2 + 15*Power(t2,2)) + 
            Power(s,3)*(4 - 23*t2 + 34*Power(t2,2) - 13*Power(t2,3) + 
               4*Power(s2,2)*(-2 + 5*t2) + 10*Power(t1,2)*(-4 + 7*t2) + 
               s2*(-1 + t1*(40 - 80*t2) + 12*t2 - 23*Power(t2,2)) - 
               2*t1*(27 - 59*t2 + 24*Power(t2,2))) + 
            2*Power(s2,2)*(-21 - 40*t2 + 25*Power(t2,2) + 
               26*Power(t2,3) + 10*Power(t2,4) + 
               t1*(11 + 91*t2 + 95*Power(t2,2) + 19*Power(t2,3))) - 
            s2*(3*Power(-1 + t2,2)*(13 + 15*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(75 + 229*t2 + 113*Power(t2,2) + 
                  15*Power(t2,3)) + 
               2*t1*(-77 - 47*t2 + 77*Power(t2,2) + 39*Power(t2,3) + 
                  8*Power(t2,4))) - 
            Power(s,2)*(19 - 23*t2 + 19*Power(t2,2) - 29*Power(t2,3) + 
               14*Power(t2,4) + 5*Power(s2,3)*(3 + 2*t2) + 
               3*Power(t1,3)*(-29 + 7*t2) - 
               Power(t1,2)*(4 + 15*t2 + 94*Power(t2,2)) + 
               t1*(18 + 73*t2 - 147*Power(t2,2) + 12*Power(t2,3)) - 
               Power(s2,2)*(-14 + 19*t2 + 88*Power(t2,2) + 
                  t1*(82 + 30*t2)) + 
               s2*(-11 - 46*t2 + 64*Power(t2,2) + 37*Power(t2,3) + 
                  3*Power(t1,2)*(50 + t2) + 
                  t1*(24 - 94*t2 + 276*Power(t2,2)))) + 
            s*(Power(s2,3)*(7 - 82*t2 - 39*Power(t2,2)) - 
               2*Power(-1 + t2,2)*(-11 + 6*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(100 + 57*t2 + 5*Power(t2,2)) + 
               3*Power(t1,2)*
                (-41 + 71*t2 + 14*Power(t2,2) + 12*Power(t2,3)) + 
               2*t1*(-35 + 6*t2 + 4*Power(t2,2) + 24*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(-99 + 75*t2 + 102*Power(t2,2) + 
                  90*Power(t2,3) + t1*(58 + 241*t2 + 91*Power(t2,2))) - 
               s2*(10 - 68*t2 - 52*Power(t2,2) + 88*Power(t2,3) + 
                  22*Power(t2,4) + 
                  Power(t1,2)*(137 + 272*t2 + 29*Power(t2,2)) + 
                  2*t1*(-87 + 119*t2 + 50*Power(t2,2) + 86*Power(t2,3))))\
) - Power(s1,8)*(-19 + 131*t1 - 195*Power(t1,2) + 55*Power(t1,3) + 
            42*t2 - 125*t1*t2 - 142*Power(t1,2)*t2 + 
            169*Power(t1,3)*t2 - 18*Power(t2,2) - 107*t1*Power(t2,2) + 
            238*Power(t1,2)*Power(t2,2) + 87*Power(t1,3)*Power(t2,2) - 
            8*Power(t2,3) + 69*t1*Power(t2,3) + 
            86*Power(t1,2)*Power(t2,3) + 23*Power(t1,3)*Power(t2,3) - 
            3*Power(t2,4) + 28*t1*Power(t2,4) + 
            13*Power(t1,2)*Power(t2,4) + 2*Power(t1,3)*Power(t2,4) + 
            6*Power(t2,5) + 4*t1*Power(t2,5) - 
            2*Power(s2,3)*(-1 + 9*t2 + 70*Power(t2,2) + 
               75*Power(t2,3) + 15*Power(t2,4)) + 
            Power(s,4)*(-4 + 5*t2 + 6*Power(t2,2) - 3*Power(t2,3) + 
               2*Power(s2,2)*(-1 + 5*t2) + 10*Power(t1,2)*(-3 + 7*t2) + 
               t1*(-46 + 160*t2 - 99*Power(t2,2)) + 
               s2*(-5 + t1*(20 - 60*t2) + 28*t2 - 40*Power(t2,2))) + 
            2*Power(s2,2)*(-15 - 109*t2 - 15*Power(t2,2) + 
               100*Power(t2,3) + 34*Power(t2,4) + 5*Power(t2,5) + 
               t1*(-1 + 99*t2 + 255*Power(t2,2) + 135*Power(t2,3) + 
                  16*Power(t2,4))) - 
            s2*(Power(-1 + t2,2)*
                (78 + 151*t2 + 63*Power(t2,2) + 16*Power(t2,3)) + 
               Power(t1,2)*(40 + 385*t2 + 439*Power(t2,2) + 
                  131*Power(t2,3) + 13*Power(t2,4)) + 
               2*t1*(-88 - 237*t2 + 131*Power(t2,2) + 159*Power(t2,3) + 
                  33*Power(t2,4) + 2*Power(t2,5))) - 
            Power(s,3)*(81 + 13*Power(s2,3) - 196*t2 + 
               154*Power(t2,2) - 99*Power(t2,3) + 42*Power(t2,4) + 
               5*Power(t1,3)*(-26 + 7*t2) + 
               Power(t1,2)*(-96 + 70*t2 - 165*Power(t2,2)) + 
               t1*(-66 + 268*t2 - 398*Power(t2,2) + 57*Power(t2,3)) + 
               Power(s2,2)*(-14 + t2 - 102*Power(t2,2) - 
                  2*t1*(44 + 5*t2)) + 
               s2*(-16 - 10*Power(t1,2)*(-19 + t2) + 10*t2 + 
                  35*Power(t2,2) + 102*Power(t2,3) + 
                  4*t1*(19 - 33*t2 + 97*Power(t2,2)))) + 
            Power(s,2)*(11 - 180*t2 + 279*Power(t2,2) - 
               138*Power(t2,3) + 48*Power(t2,4) - 20*Power(t2,5) + 
               Power(s2,3)*(20 - 130*t2 - 23*Power(t2,2)) + 
               Power(t1,3)*(119 + 155*t2 + 30*Power(t2,2)) + 
               Power(t1,2)*(-150 + 644*t2 - 44*Power(t2,2) + 
                  105*Power(t2,3)) + 
               t1*(-161 + 278*t2 - 47*Power(t2,2) + 151*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(s2,2)*(-115 + 272*t2 + 159*Power(t2,2) + 
                  179*Power(t2,3) + 2*t1*(4 + 203*t2 + 66*Power(t2,2))) \
- s2*(-52 - 60*t2 - 8*Power(t2,2) + 263*Power(t2,3) + 89*Power(t2,4) + 
                  Power(t1,2)*(92 + 585*t2 + 40*Power(t2,2)) + 
                  t1*(-268 + 882*t2 - 48*Power(t2,2) + 484*Power(t2,3)))\
) + s*(Power(s2,3)*(3 + 2*t2 - 271*Power(t2,2) - 56*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-86 + 5*t2 + 105*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,3)*(84 + 233*t2 + 97*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(t1,2)*(-189 + 279*t2 + 431*Power(t2,2) + 
                  72*Power(t2,3) + 23*Power(t2,4)) + 
               t1*(-126 - 365*t2 + 566*Power(t2,2) - 87*Power(t2,3) + 
                  6*Power(t2,4) + 6*Power(t2,5)) + 
               Power(s2,2)*(-29 - 332*t2 + 628*Power(t2,2) + 
                  239*Power(t2,3) + 110*Power(t2,4) + 
                  t1*(26 + 312*t2 + 589*Power(t2,2) + 151*Power(t2,3))) \
- s2*(119 - 346*t2 - 11*Power(t2,2) + 52*Power(t2,3) + 158*Power(t2,4) + 
                  28*Power(t2,5) + 
                  Power(t1,2)*
                   (62 + 600*t2 + 462*Power(t2,2) + 66*Power(t2,3)) + 
                  2*t1*(-106 - 56*t2 + 581*Power(t2,2) + 
                     129*Power(t2,3) + 68*Power(t2,4))))) + 
         Power(s1,7)*(-38 + 154*t1 - 142*Power(t1,2) + 15*Power(t1,3) + 
            66*t2 + 52*t1*t2 - 438*Power(t1,2)*t2 + 195*Power(t1,3)*t2 + 
            18*Power(t2,2) - 449*t1*Power(t2,2) + 
            244*Power(t1,2)*Power(t2,2) + 207*Power(t1,3)*Power(t2,2) - 
            72*Power(t2,3) + 139*t1*Power(t2,3) + 
            278*Power(t1,2)*Power(t2,3) + 75*Power(t1,3)*Power(t2,3) + 
            18*Power(t2,4) + 91*t1*Power(t2,4) + 
            54*Power(t1,2)*Power(t2,4) + 12*Power(t1,3)*Power(t2,4) + 
            6*Power(t2,5) + 13*t1*Power(t2,5) + 
            4*Power(t1,2)*Power(t2,5) + 2*Power(t2,6) - 
            Power(s2,3)*(-1 - 5*t2 + 90*Power(t2,2) + 260*Power(t2,3) + 
               145*Power(t2,4) + 15*Power(t2,5)) + 
            Power(s,5)*(-16 + 61*t2 + 2*Power(s2,2)*t2 - 
               80*Power(t2,2) + 40*Power(t2,3) + 
               6*Power(t1,2)*(-2 + 7*t2) + 
               t1*(-4 + 80*t2 - 90*Power(t2,2)) + 
               s2*(1 + t1*(4 - 24*t2) + 12*t2 - 30*Power(t2,2))) + 
            Power(s2,2)*(2*t2*
                (-96 - 190*t2 + 130*Power(t2,2) + 135*Power(t2,3) + 
                  20*Power(t2,4) + Power(t2,5)) + 
               t1*(-13 + 57*t2 + 580*Power(t2,2) + 680*Power(t2,3) + 
                  195*Power(t2,4) + 13*Power(t2,5))) - 
            s2*(Power(-1 + t2,2)*
                (60 + 322*t2 + 219*Power(t2,2) + 39*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(t1,2)*(-15 + 275*t2 + 745*Power(t2,2) + 
                  423*Power(t2,3) + 80*Power(t2,4) + 4*Power(t2,5)) + 
               t1*(-68 - 732*t2 - 250*Power(t2,2) + 754*Power(t2,3) + 
                  270*Power(t2,4) + 26*Power(t2,5))) + 
            Power(s,4)*(-87 + 309*t2 - 217*Power(t2,2) + 
               17*Power(t2,3) - 5*Power(t2,4) + 
               Power(s2,3)*(-4 + 5*t2) - 5*Power(t1,3)*(-23 + 7*t2) + 
               Power(s2,2)*(12 + 47*t1 - 27*t2 - 15*t1*t2 + 
                  78*Power(t2,2)) + 
               2*Power(t1,2)*(67 - 95*t2 + 110*Power(t2,2)) + 
               t1*(130 - 510*t2 + 662*Power(t2,2) - 101*Power(t2,3)) + 
               s2*(-1 - 48*t2 + 59*Power(t2,2) - 156*Power(t2,3) + 
                  5*Power(t1,2)*(-27 + 5*t2) - 
                  4*t1*(16 - 27*t2 + 80*Power(t2,2)))) + 
            Power(s,3)*(44 - 447*t2 + 947*Power(t2,2) - 
               683*Power(t2,3) + 274*Power(t2,4) - 57*Power(t2,5) + 
               Power(s2,3)*(11 - 90*t2 + 15*Power(t2,2)) + 
               Power(t1,3)*(16 + 235*t2 + 75*Power(t2,2)) + 
               Power(t1,2)*(-190 + 1034*t2 - 315*Power(t2,2) + 
                  199*Power(t2,3)) + 
               t1*(-120 + 716*t2 - 676*Power(t2,2) + 553*Power(t2,3) + 
                  23*Power(t2,4)) + 
               Power(s2,2)*(-81 + 377*t2 - 31*Power(t2,2) + 
                  221*Power(t2,3) + t1*(-66 + 402*t2 + 58*Power(t2,2))) \
- s2*(-40 + 46*t2 + 133*Power(t2,2) + 320*Power(t2,3) + 
                  169*Power(t2,4) + 
                  2*Power(t1,2)*(-33 + 340*t2 + 5*Power(t2,2)) + 
                  4*t1*(-53 + 291*t2 - 93*Power(t2,2) + 176*Power(t2,3))\
)) + Power(s,2)*(50 - 270*t2 + 28*Power(t2,2) + 557*Power(t2,3) - 
               424*Power(t2,4) + 75*Power(t2,5) - 16*Power(t2,6) + 
               Power(s2,3)*(3 + 82*t2 - 364*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,3)*(-26 + 292*t2 + 217*Power(t2,2) + 
                  55*Power(t2,3)) + 
               Power(t1,2)*(100 + 25*t2 + 1043*Power(t2,2) + 
                  112*Power(t2,3) + 89*Power(t2,4)) + 
               t1*(42 - 639*t2 + 1740*Power(t2,2) - 460*Power(t2,3) + 
                  47*Power(t2,4) + 30*Power(t2,5)) + 
               Power(s2,2)*(86 - 540*t2 + 1243*Power(t2,2) + 
                  259*Power(t2,3) + 229*Power(t2,4) + 
                  2*t1*(1 + 8*t2 + 397*Power(t2,2) + 108*Power(t2,3))) \
- s2*(91 - 706*t2 + 491*Power(t2,2) + 302*Power(t2,3) + 
                  493*Power(t2,4) + 89*Power(t2,5) + 
                  Power(t1,2)*
                   (-16 + 366*t2 + 828*Power(t2,2) + 111*Power(t2,3)) + 
                  2*t1*(114 - 412*t2 + 1412*Power(t2,2) + 
                     5*Power(t2,3) + 204*Power(t2,4)))) - 
            s*(Power(-1 + t2,2)*
                (-117 - 201*t2 + 356*Power(t2,2) + 206*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(t1,3)*(-24 + 241*t2 + 281*Power(t2,2) + 
                  96*Power(t2,3) + 8*Power(t2,4)) + 
               Power(s2,3)*(11 - 18*t2 + 59*Power(t2,2) + 
                  404*Power(t2,3) + 34*Power(t2,4)) - 
               Power(t1,2)*(62 - 119*t2 + 896*Power(t2,2) + 
                  404*Power(t2,3) + 121*Power(t2,4) + 8*Power(t2,5)) + 
               t1*(274 + 527*t2 - 299*Power(t2,2) - 775*Power(t2,3) + 
                  279*Power(t2,4) - 6*Power(t2,6)) - 
               Power(s2,2)*(31 - 78*t2 - 273*Power(t2,2) + 
                  1329*Power(t2,3) + 273*Power(t2,4) + 90*Power(t2,5) + 
                  t1*(30 + 92*t2 + 638*Power(t2,2) + 705*Power(t2,3) + 
                     117*Power(t2,4))) + 
               s2*(29 + 154*t2 - 1152*Power(t2,2) + 574*Power(t2,3) + 
                  243*Power(t2,4) + 140*Power(t2,5) + 12*Power(t2,6) + 
                  Power(t1,2)*
                   (-6 + 304*t2 + 927*Power(t2,2) + 408*Power(t2,3) + 
                     61*Power(t2,4)) + 
                  2*t1*(12 - 246*t2 + 583*Power(t2,2) + 
                     883*Power(t2,3) + 112*Power(t2,4) + 28*Power(t2,5)))\
)) + Power(s1,6)*(30 + 6*s2 - 5*Power(s2,2) - 70*t1 + 18*s2*t1 + 
            5*Power(s2,2)*t1 + 25*Power(t1,2) - 24*s2*Power(t1,2) + 
            15*Power(t1,3) + 18*t2 + 258*s2*t2 + 28*Power(s2,2)*t2 - 
            5*Power(s2,3)*t2 - 330*t1*t2 - 404*s2*t1*t2 + 
            37*Power(s2,2)*t1*t2 + 466*Power(t1,2)*t2 + 
            31*s2*Power(t1,2)*t2 - 99*Power(t1,3)*t2 - 195*Power(t2,2) - 
            36*s2*Power(t2,2) + 445*Power(s2,2)*Power(t2,2) + 
            5*Power(s2,3)*Power(t2,2) + 531*t1*Power(t2,2) - 
            974*s2*t1*Power(t2,2) - 250*Power(s2,2)*t1*Power(t2,2) + 
            124*Power(t1,2)*Power(t2,2) + 
            605*s2*Power(t1,2)*Power(t2,2) - 
            255*Power(t1,3)*Power(t2,2) + 180*Power(t2,3) - 
            585*s2*Power(t2,3) + 180*Power(s2,2)*Power(t2,3) + 
            180*Power(s2,3)*Power(t2,3) + 165*t1*Power(t2,3) + 
            570*s2*t1*Power(t2,3) - 780*Power(s2,2)*t1*Power(t2,3) - 
            450*Power(t1,2)*Power(t2,3) + 
            675*s2*Power(t1,2)*Power(t2,3) - 
            135*Power(t1,3)*Power(t2,3) + 225*s2*Power(t2,4) - 
            475*Power(s2,2)*Power(t2,4) + 250*Power(s2,3)*Power(t2,4) - 
            255*t1*Power(t2,4) + 680*s2*t1*Power(t2,4) - 
            455*Power(s2,2)*t1*Power(t2,4) - 
            145*Power(t1,2)*Power(t2,4) + 
            205*s2*Power(t1,2)*Power(t2,4) - 
            30*Power(t1,3)*Power(t2,4) - 30*Power(t2,5) + 
            123*s2*Power(t2,5) - 164*Power(s2,2)*Power(t2,5) + 
            71*Power(s2,3)*Power(t2,5) - 39*t1*Power(t2,5) + 
            106*s2*t1*Power(t2,5) - 67*Power(s2,2)*t1*Power(t2,5) - 
            20*Power(t1,2)*Power(t2,5) + 20*s2*Power(t1,2)*Power(t2,5) - 
            3*Power(t2,6) + 9*s2*Power(t2,6) - 
            9*Power(s2,2)*Power(t2,6) + 3*Power(s2,3)*Power(t2,6) - 
            2*t1*Power(t2,6) + 4*s2*t1*Power(t2,6) - 
            2*Power(s2,2)*t1*Power(t2,6) + 
            Power(s,6)*(14 + Power(t1,2)*(2 - 14*t2) - 73*t2 + 
               120*Power(t2,2) - 70*Power(t2,3) + 
               s2*(-2 + (6 + 4*t1)*t2 + 5*Power(t2,2)) + 
               t1*(-15 + 26*t2 + 15*Power(t2,2))) + 
            Power(s,5)*(17 - 156*t2 - 2*Power(s2,3)*t2 + 
               59*Power(t2,2) + 243*Power(t2,3) - 124*Power(t2,4) + 
               3*Power(t1,3)*(-20 + 7*t2) + 
               Power(t1,2)*(-76 + 201*t2 - 221*Power(t2,2)) + 
               t1*(-34 + 340*t2 - 572*Power(t2,2) + 70*Power(t2,3)) + 
               Power(s2,2)*((14 - 37*t2)*t2 + 5*t1*(-2 + 3*t2)) + 
               s2*(8 + Power(t1,2)*(51 - 24*t2) - 8*t2 - 
                  57*Power(t2,2) + 134*Power(t2,3) + 
                  2*t1*(15 - 41*t2 + 88*Power(t2,2)))) + 
            Power(s,4)*(Power(s2,3)*(23 - 18*t2)*t2 + 
               Power(t1,3)*(71 - 215*t2 - 100*Power(t2,2)) + 
               Power(t1,2)*(207 - 1110*t2 + 651*Power(t2,2) - 
                  260*Power(t2,3)) + 
               t1*(134 - 1002*t2 + 1609*Power(t2,2) - 
                  1166*Power(t2,3) - 16*Power(t2,4)) + 
               3*(-25 + 182*t2 - 432*Power(t2,2) + 333*Power(t2,3) - 
                  113*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s2,2)*(-4 - 194*t2 + 196*Power(t2,2) - 
                  189*Power(t2,3) + t1*(39 - 235*t2 + 38*Power(t2,2))) \
+ s2*(-27 + 90*t2 + 177*Power(t2,2) + 64*Power(t2,3) + 
                  218*Power(t2,4) - 
                  5*Power(t1,2)*(20 - 91*t2 + 5*Power(t2,2)) + 
                  2*t1*(-51 + 386*t2 - 243*Power(t2,2) + 
                     292*Power(t2,3)))) + 
            Power(s,3)*(22 - 181*t2 + 862*Power(t2,2) - 
               1655*Power(t2,3) + 1168*Power(t2,4) - 436*Power(t2,5) + 
               46*Power(t2,6) + 
               Power(s2,3)*t2*(-30 + 203*t2 - 52*Power(t2,2)) - 
               Power(t1,3)*(-60 + 154*t2 + 195*Power(t2,2) + 
                  80*Power(t2,3)) - 
               Power(t1,2)*(258 - 884*t2 + 1600*Power(t2,2) + 
                  55*Power(t2,3) + 168*Power(t2,4)) - 
               t1*(160 - 986*t2 + 2756*Power(t2,2) - 
                  1434*Power(t2,3) + 397*Power(t2,4) + 54*Power(t2,5)) \
+ Power(s2,2)*(23 + 477*t2 - 1320*Power(t2,2) + 184*Power(t2,3) - 
                  261*Power(t2,4) - 
                  2*t1*(31 - 160*t2 + 352*Power(t2,2) + 55*Power(t2,3))\
) + s2*(61 - 438*t2 + 816*Power(t2,2) + 304*Power(t2,3) + 
                  660*Power(t2,4) + 120*Power(t2,5) + 
                  Power(t1,2)*
                   (34 - 228*t2 + 848*Power(t2,2) + 86*Power(t2,3)) + 
                  4*t1*(37 - 335*t2 + 812*Power(t2,2) - 
                     166*Power(t2,3) + 150*Power(t2,4)))) - 
            Power(s,2)*(-121 + 230*t2 - 605*Power(t2,2) + 
               863*Power(t2,3) + 68*Power(t2,4) - 489*Power(t2,5) + 
               62*Power(t2,6) - 8*Power(t2,7) + 
               Power(s2,3)*t2*
                (42 + 119*t2 - 470*Power(t2,2) + 44*Power(t2,3)) + 
               Power(t1,3)*(-50 + 78*t2 + 269*Power(t2,2) + 
                  133*Power(t2,3) + 10*Power(t2,4)) + 
               Power(t1,2)*(218 - 238*t2 + 427*Power(t2,2) + 
                  889*Power(t2,3) + 303*Power(t2,4) + 36*Power(t2,5)) + 
               t1*(-51 + 1036*t2 - 1082*Power(t2,2) + 
                  2234*Power(t2,3) - 626*Power(t2,4) + 
                  45*Power(t2,5) + 24*Power(t2,6)) + 
               Power(s2,2)*(39 + 244*t2 - 1040*Power(t2,2) + 
                  2014*Power(t2,3) + 205*Power(t2,4) + 
                  193*Power(t2,5) + 
                  2*t1*(-26 + 15*t2 - 15*Power(t2,2) + 
                     383*Power(t2,3) + 78*Power(t2,4))) - 
               s2*(3 + 412*t2 - 1694*Power(t2,2) + 1700*Power(t2,3) + 
                  684*Power(t2,4) + 447*Power(t2,5) + 28*Power(t2,6) + 
                  Power(t1,2)*
                   (4 - 78*t2 + 472*Power(t2,2) + 533*Power(t2,3) + 
                     114*Power(t2,4)) + 
                  2*t1*(-46 + 526*t2 - 650*Power(t2,2) + 
                     1743*Power(t2,3) - 18*Power(t2,4) + 90*Power(t2,5))\
)) + s*(Power(s2,3)*t2*(56 - 59*t2 + 116*Power(t2,2) + 
                  301*Power(t2,3) + 6*Power(t2,4)) - 
               Power(t1,3)*(-56 + 19*t2 + 269*Power(t2,2) + 
                  164*Power(t2,3) + 24*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (145 + 244*t2 - 244*Power(t2,2) - 726*Power(t2,3) - 
                  153*Power(t2,4) + 4*Power(t2,5)) - 
               3*Power(t1,2)*
                (86 + 19*t2 + 117*Power(t2,2) + 288*Power(t2,3) + 
                  114*Power(t2,4) + 20*Power(t2,5)) + 
               t1*(446 + 384*t2 + 227*Power(t2,2) - 1094*Power(t2,3) - 
                  97*Power(t2,4) + 154*Power(t2,5) - 18*Power(t2,6) - 
                  2*Power(t2,7)) - 
               Power(s2,2)*(-25 + 225*t2 - 30*Power(t2,2) + 
                  220*Power(t2,3) + 1305*Power(t2,4) + 
                  192*Power(t2,5) + 45*Power(t2,6) + 
                  t1*(24 + 107*t2 + 50*Power(t2,2) + 618*Power(t2,3) + 
                     421*Power(t2,4) + 40*Power(t2,5))) + 
               s2*(-49 + 4*t2 - 263*Power(t2,2) - 1128*Power(t2,3) + 
                  1132*Power(t2,4) + 260*Power(t2,5) + 46*Power(t2,6) - 
                  2*Power(t2,7) + 
                  Power(t1,2)*
                   (35 - 28*t2 + 404*Power(t2,2) + 626*Power(t2,3) + 
                     203*Power(t2,4) + 20*Power(t2,5)) + 
                  2*t1*(-1 + 175*t2 - 16*Power(t2,2) + 
                     1152*Power(t2,3) + 570*Power(t2,4) + 
                     44*Power(t2,5) + 8*Power(t2,6))))) + 
         Power(s1,5)*(-4 + 7*s2 - 5*t1 - 14*s2*t1 + 22*Power(t1,2) + 
            7*s2*Power(t1,2) - 13*Power(t1,3) - 84*t2 - 63*s2*t2 + 
            20*Power(s2,2)*t2 + 267*t1*t2 + 6*s2*t1*t2 - 
            20*Power(s2,2)*t1*t2 - 182*Power(t1,2)*t2 + 
            57*s2*Power(t1,2)*t2 - Power(t1,3)*t2 + 174*Power(t2,2) - 
            345*s2*Power(t2,2) - 112*Power(s2,2)*Power(t2,2) + 
            10*Power(s2,3)*Power(t2,2) - 33*t1*Power(t2,2) + 
            762*s2*t1*Power(t2,2) - 18*Power(s2,2)*t1*Power(t2,2) - 
            416*Power(t1,2)*Power(t2,2) - 
            187*s2*Power(t1,2)*Power(t2,2) + 
            165*Power(t1,3)*Power(t2,2) + 12*Power(t2,3) + 
            509*s2*Power(t2,3) - 460*Power(s2,2)*Power(t2,3) - 
            30*Power(s2,3)*Power(t2,3) - 593*t1*Power(t2,3) + 
            286*s2*t1*Power(t2,3) + 390*Power(s2,2)*t1*Power(t2,3) + 
            306*Power(t1,2)*Power(t2,3) - 
            565*s2*Power(t1,2)*Power(t2,3) + 
            145*Power(t1,3)*Power(t2,3) - 180*Power(t2,4) + 
            200*s2*Power(t2,4) + 190*Power(s2,2)*Power(t2,4) - 
            180*Power(s2,3)*Power(t2,4) + 250*t1*Power(t2,4) - 
            780*s2*t1*Power(t2,4) + 510*Power(s2,2)*t1*Power(t2,4) + 
            230*Power(t1,2)*Power(t2,4) - 
            280*s2*Power(t1,2)*Power(t2,4) + 40*Power(t1,3)*Power(t2,4) + 
            72*Power(t2,5) - 274*s2*Power(t2,5) + 
            324*Power(s2,2)*Power(t2,5) - 122*Power(s2,3)*Power(t2,5) + 
            106*t1*Power(t2,5) - 244*s2*t1*Power(t2,5) + 
            138*Power(s2,2)*t1*Power(t2,5) + 40*Power(t1,2)*Power(t2,5) - 
            40*s2*Power(t1,2)*Power(t2,5) + 10*Power(t2,6) - 
            34*s2*Power(t2,6) + 38*Power(s2,2)*Power(t2,6) - 
            14*Power(s2,3)*Power(t2,6) + 8*t1*Power(t2,6) - 
            16*s2*t1*Power(t2,6) + 8*Power(s2,2)*t1*Power(t2,6) + 
            Power(s,7)*(-4 + (38 - 4*s2)*t2 + 2*Power(t1,2)*t2 + 
               (-78 + 5*s2)*Power(t2,2) + 51*Power(t2,3) + 
               6*t1*(1 - 7*t2 + 6*Power(t2,2))) + 
            Power(s,6)*(Power(t1,3)*(17 - 7*t2) - 
               4*Power(s2,2)*(t1 - 2*t2)*t2 + 
               Power(t1,2)*(16 - 101*t2 + 150*Power(t2,2)) + 
               t1*(-20 + 7*t2 + 163*Power(t2,2) + 10*Power(t2,3)) + 
               2*(5 - 7*t2 + 33*Power(t2,2) - 159*Power(t2,3) + 
                  98*Power(t2,4)) + 
               s2*(4 + 14*t2 - 7*Power(t2,2) - 57*Power(t2,3) + 
                  Power(t1,2)*(-8 + 11*t2) + 
                  t1*(-8 + 54*t2 - 84*Power(t2,2)))) + 
            Power(s,5)*(-4 - 242*t2 + 779*Power(t2,2) + 
               4*Power(s2,3)*Power(t2,2) - 557*Power(t2,3) + 
               15*Power(t2,4) + 126*Power(t2,5) + 
               Power(t1,3)*(-52 + 119*t2 + 75*Power(t2,2)) + 
               Power(s2,2)*t2*
                (2 + t1*(61 - 45*t2) - 95*t2 + 101*Power(t2,2)) + 
               Power(t1,2)*(-87 + 749*t2 - 740*Power(t2,2) + 
                  226*Power(t2,3)) - 
               2*t1*(-27 - 203*t2 + 687*Power(t2,2) - 611*Power(t2,3) + 
                  9*Power(t2,4)) + 
               s2*(-27 - 62*t2 + 14*Power(t2,2) + 96*Power(t2,3) - 
                  202*Power(t2,4) + 
                  Power(t1,2)*(31 - 168*t2 + 23*Power(t2,2)) + 
                  t1*(46 - 390*t2 + 408*Power(t2,2) - 300*Power(t2,3)))) \
+ Power(s,4)*(-59 + 503*t2 - 1525*Power(t2,2) + 2329*Power(t2,3) - 
               1489*Power(t2,4) + 633*Power(t2,5) - 26*Power(t2,6) + 
               6*Power(s2,3)*Power(t2,2)*(-5 + 4*t2) + 
               Power(t1,3)*(9 + 5*t2 + 25*Power(t2,2) + 
                  65*Power(t2,3)) + 
               2*Power(t1,2)*
                (141 - 719*t2 + 947*Power(t2,2) - 71*Power(t2,3) + 
                  86*Power(t2,4)) + 
               t1*(-43 - 1003*t2 + 3479*Power(t2,2) - 
                  2870*Power(t2,3) + 882*Power(t2,4) + 32*Power(t2,5)) + 
               Power(s2,2)*t2*
                (27 + 629*t2 - 457*Power(t2,2) + 199*Power(t2,3) + 
                  t1*(-206 + 432*t2 - 26*Power(t2,2))) - 
               s2*(-69 - 221*t2 + 584*Power(t2,2) + 221*Power(t2,3) + 
                  242*Power(t2,4) + 124*Power(t2,5) + 
                  Power(t1,2)*
                   (37 - 299*t2 + 501*Power(t2,2) + 29*Power(t2,3)) + 
                  2*t1*(59 - 397*t2 + 1021*Power(t2,2) - 
                     413*Power(t2,3) + 238*Power(t2,4)))) + 
            Power(s,3)*(191 - 198*t2 + 247*Power(t2,2) - 
               770*Power(t2,3) + 1357*Power(t2,4) - 904*Power(t2,5) + 
               295*Power(t2,6) - 24*Power(t2,7) + 
               2*Power(s2,3)*Power(t2,2)*(-5 - 96*t2 + 33*Power(t2,2)) + 
               Power(t1,3)*(32 - 50*t2 + 86*Power(t2,2) + 
                  48*Power(t2,3)) + 
               2*Power(t1,2)*
                (-109 + 165*t2 - 339*Power(t2,2) + 507*Power(t2,3) + 
                  155*Power(t2,4) + 32*Power(t2,5)) + 
               2*t1*(-128 + 702*t2 - 1665*Power(t2,2) + 
                  1854*Power(t2,3) - 580*Power(t2,4) + 
                  102*Power(t2,5) + 17*Power(t2,6)) + 
               Power(s2,2)*t2*
                (-131 - 970*t2 + 1874*Power(t2,2) - 246*Power(t2,3) + 
                  177*Power(t2,4) + 
                  t1*(312 - 606*t2 + 584*Power(t2,2) + 82*Power(t2,3))) \
- 2*s2*(46 + 118*t2 - 709*Power(t2,2) + 854*Power(t2,3) + 
                  193*Power(t2,4) + 296*Power(t2,5) + 10*Power(t2,6) + 
                  Power(t1,2)*
                   (1 + 60*t2 - 146*Power(t2,2) + 208*Power(t2,3) + 
                     53*Power(t2,4)) + 
                  2*t1*(-43 + 209*t2 - 743*Power(t2,2) + 
                     936*Power(t2,3) - 154*Power(t2,4) + 63*Power(t2,5)))\
) + Power(s,2)*(-219 - 287*t2 + 1282*Power(t2,2) - 734*Power(t2,3) + 
               539*Power(t2,4) - 361*Power(t2,5) - 236*Power(t2,6) + 
               18*Power(t2,7) - 2*Power(t2,8) + 
               Power(s2,3)*Power(t2,2)*
                (140 + 46*t2 - 301*Power(t2,2) + 40*Power(t2,3)) + 
               Power(t1,3)*(19 - 5*t2 + 66*Power(t2,2) + 
                  62*Power(t2,3) + 8*Power(t2,4)) + 
               Power(t1,2)*(-120 + 57*t2 - 388*Power(t2,2) + 
                  536*Power(t2,3) + 462*Power(t2,4) + 120*Power(t2,5)) + 
               t1*(484 + 253*t2 + 1409*Power(t2,2) - 1399*Power(t2,3) + 
                  1396*Power(t2,4) - 167*Power(t2,5) + 60*Power(t2,6) + 
                  8*Power(t2,7)) + 
               Power(s2,2)*t2*
                (193 + 53*t2 - 1018*Power(t2,2) + 1506*Power(t2,3) + 
                  114*Power(t2,4) + 95*Power(t2,5) + 
                  2*t1*(-125 + 75*t2 - 53*Power(t2,2) + 
                     182*Power(t2,3) + 21*Power(t2,4))) - 
               s2*(-72 + 232*t2 + 902*Power(t2,2) - 1577*Power(t2,3) + 
                  1700*Power(t2,4) + 690*Power(t2,5) + 179*Power(t2,6) - 
                  10*Power(t2,7) + 
                  Power(t1,2)*
                   (-38 - 17*t2 - 112*Power(t2,2) + 238*Power(t2,3) + 
                     164*Power(t2,4) + 40*Power(t2,5)) + 
                  2*t1*(74 - 293*t2 + 830*Power(t2,2) - 
                     788*Power(t2,3) + 992*Power(t2,4) - 
                     36*Power(t2,5) + 26*Power(t2,6)))) + 
            s*(Power(s2,3)*Power(t2,2)*
                (-114 + 116*t2 - 79*Power(t2,2) - 106*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,3)*(-12 - 61*t2 + 31*Power(t2,2) + 
                  96*Power(t2,3) + 16*Power(t2,4)) + 
               Power(t1,2)*(105 + 487*t2 + 50*Power(t2,2) + 
                  492*Power(t2,3) + 474*Power(t2,4) + 128*Power(t2,5)) + 
               Power(-1 + t2,2)*
                (89 + 270*t2 - 200*Power(t2,2) - 888*Power(t2,3) - 
                  531*Power(t2,4) - 36*Power(t2,5) + 2*Power(t2,6)) + 
               2*t1*(-110 - 438*t2 + 189*Power(t2,2) + 152*Power(t2,3) + 
                  254*Power(t2,4) - 56*Power(t2,5) + 5*Power(t2,6) + 
                  4*Power(t2,7)) + 
               Power(s2,2)*t2*
                (-111 + 631*t2 + 26*Power(t2,2) + 461*Power(t2,3) + 
                  634*Power(t2,4) + 85*Power(t2,5) + 10*Power(t2,6) + 
                  t1*(107 + 87*t2 - 146*Power(t2,2) + 276*Power(t2,3) + 
                     106*Power(t2,4) + 4*Power(t2,5))) - 
               s2*(33 - 362*t2 + 35*Power(t2,2) - 484*Power(t2,3) - 
                  155*Power(t2,4) + 842*Power(t2,5) + 101*Power(t2,6) - 
                  8*Power(t2,7) - 2*Power(t2,8) + 
                  Power(t1,2)*
                   (29 + 96*t2 - 165*Power(t2,2) + 128*Power(t2,3) + 
                     186*Power(t2,4) + 48*Power(t2,5)) + 
                  2*t1*(-35 + 107*t2 + 482*Power(t2,2) + 
                     254*Power(t2,3) + 776*Power(t2,4) + 
                     140*Power(t2,5) + 10*Power(t2,6) + 2*Power(t2,7))))))*
       R1q(s1))/(s*(-1 + s1)*s1*
       Power(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2),2)*
       (-1 + s2)*(-1 + t1)*Power(s1 - t2,3)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) - 
    (8*(-(Power(s1 - t2,2)*Power(-1 + t2,2)*Power(t2,2)*
            Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
            (-3 + 3*t1 + 2*Power(s1,3)*(-1 + t2) + 18*t2 - s2*t2 - 
              7*t1*t2 - 15*Power(t2,2) + 7*s2*Power(t2,2) - 
              2*t1*Power(t2,2) + 
              Power(s1,2)*(t1*(-7 + t2) - 2*(-1 + t2)*t2 + 
                 3*s2*(1 + t2)) + 
              s1*(s2*(1 - 10*t2 - 3*Power(t2,2)) - 
                 3*(5 - 6*t2 + Power(t2,2)) + 
                 2*t1*(2 + 3*t2 + Power(t2,2))))) + 
         2*Power(s,5)*(s1 - t2)*(-1 + t2)*Power(t2,3)*
          (s1*(1 + t2)*(t1 + t2 - 5*t1*t2 + 3*Power(t2,2)) + 
            Power(s1,2)*(t1 + 3*t1*t2 - t2*(3 + t2)) + 
            t2*(-4 + 7*t2 - 5*Power(t2,2) - 2*Power(t2,3) + 
               t1*(3 - 5*t2 + 6*Power(t2,2)))) + 
         Power(s,4)*Power(t2,2)*
          (2*Power(s1,4)*(-1 + t2)*
             (t1*(2 + 4*t2 - 10*Power(t2,2)) + 
               t2*(-8 + s2 + 9*t2 - s2*t2 + 3*Power(t2,2))) + 
            Power(t2,2)*(-12 + (43 + 8*s2)*t2 - 
               11*(7 + 2*s2)*Power(t2,2) + (112 + 17*s2)*Power(t2,3) + 
               3*(-37 + 8*s2)*Power(t2,4) - 15*(-3 + s2)*Power(t2,5) + 
               Power(t1,2)*(-3 + 11*t2 - 9*Power(t2,2) + 
                  19*Power(t2,3) - 6*Power(t2,4)) + 
               t1*(15 - 4*(15 + s2)*t2 + (99 + 7*s2)*Power(t2,2) - 
                  2*(48 + 13*s2)*Power(t2,3) + 
                  11*(4 + s2)*Power(t2,4) - 14*Power(t2,5))) + 
            Power(s1,2)*(Power(t1,2)*
                (1 - 11*t2 + 45*Power(t2,2) + 5*Power(t2,3) - 
                  4*Power(t2,4)) + 
               t2*(18 - 2*(36 + s2)*t2 + (202 - 3*s2)*Power(t2,2) + 
                  21*(-13 + 4*s2)*Power(t2,3) + 
                  (96 - 43*s2)*Power(t2,4) + 29*Power(t2,5)) + 
               t1*t2*(-13 + 39*t2 - 114*Power(t2,2) + 139*Power(t2,3) - 
                  87*Power(t2,4) + 
                  s2*(4 - 27*t2 - 30*Power(t2,2) + 17*Power(t2,3)))) + 
            Power(s1,3)*(Power(t1,2)*
                (1 - 9*t2 - 7*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(-2 + 5*(3 + s2)*t2 + 2*(12 + 7*s2)*Power(t2,2) - 
                  (93 + 7*s2)*Power(t2,3) + 68*Power(t2,4)) + 
               t2*(s2*(2 + t2 - 32*Power(t2,2) + 17*Power(t2,3)) - 
                  t2*(62 - 131*t2 + 46*Power(t2,2) + 23*Power(t2,3)))) + 
            s1*t2*(Power(t1,2)*
                (-4 + 11*t2 - 27*Power(t2,2) - 29*Power(t2,3) + 
                  13*Power(t2,4)) + 
               t1*(3 + 33*t2 + (-137 + 15*s2)*Power(t2,2) + 
                  (226 + 42*s2)*Power(t2,3) - 
                  (148 + 21*s2)*Power(t2,4) + 59*Power(t2,5)) - 
               t2*(31 - 137*t2 + 292*Power(t2,2) - 311*Power(t2,3) + 
                  113*Power(t2,4) + 12*Power(t2,5) + 
                  s2*(8 - 22*t2 + 13*Power(t2,2) + 80*Power(t2,3) - 
                     43*Power(t2,4))))) - 
         Power(s,2)*(-2*Power(s1,6)*Power(-1 + t2,3)*t2*
             (-2 + s2 - 3*s2*t2 + (9 - 6*t1)*t2 + Power(t2,2)) + 
            s1*Power(t2,3)*(-63 + (65 + 108*s2)*t2 + 
               (53 - 342*s2 + 3*Power(s2,2))*Power(t2,2) + 
               (400 + 534*s2 - 124*Power(s2,2) - 6*Power(s2,3))*
                Power(t2,3) - 
               4*(362 + 125*s2 - 97*Power(s2,2) + 8*Power(s2,3))*
                Power(t2,4) + 
               (1771 + 204*s2 - 332*Power(s2,2) - 10*Power(s2,3))*
                Power(t2,5) + 
               (-1021 + 18*s2 + 65*Power(s2,2))*Power(t2,6) + 
               (252 - 22*s2)*Power(t2,7) - 9*Power(t2,8) + 
               Power(t1,3)*(6 - 24*t2 - 6*Power(t2,2) + 
                  36*Power(t2,3) + 40*Power(t2,4) - 4*Power(t2,5)) - 
               Power(t1,2)*(16 - (36 + 11*s2)*t2 + 
                  (96 - 58*s2)*Power(t2,2) + 
                  2*(-146 + 57*s2)*Power(t2,3) + 
                  (286 + 90*s2)*Power(t2,4) + 
                  (-80 + 9*s2)*Power(t2,5) + 10*Power(t2,6)) + 
               t1*(87 - (431 + 20*s2)*t2 + 
                  (782 + 183*s2 - 38*Power(s2,2))*Power(t2,2) + 
                  (-701 - 434*s2 + 60*Power(s2,2))*Power(t2,3) + 
                  (313 + 236*s2 + 110*Power(s2,2))*Power(t2,4) + 
                  (-23 + 30*s2 + 12*Power(s2,2))*Power(t2,5) + 
                  (-22 + 5*s2)*Power(t2,6) - 5*Power(t2,7))) + 
            Power(t2,4)*(43 - (52 + 55*s2)*t2 - 
               (98 - 174*s2 + Power(s2,2))*Power(t2,2) + 
               (96 - 237*s2 + 41*Power(s2,2) + Power(s2,3))*
                Power(t2,3) + 
               (220 + 189*s2 - 110*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,4) + 
               (-382 - 86*s2 + 83*Power(s2,2) + Power(s2,3))*
                Power(t2,5) + 
               (224 + 13*s2 - 13*Power(s2,2))*Power(t2,6) + 
               2*(-27 + s2)*Power(t2,7) + 3*Power(t2,8) - 
               Power(t1,3)*(3 - 13*t2 + 12*Power(t2,2) + 
                  2*Power(t2,3) + 7*Power(t2,4) + Power(t2,5)) + 
               Power(t1,2)*(24 - (85 + 7*s2)*t2 + 
                  (131 + s2)*Power(t2,2) + 
                  (-121 + 18*s2)*Power(t2,3) + 
                  3*(18 + 7*s2)*Power(t2,4) + 
                  3*(-2 + s2)*Power(t2,5) + 3*Power(t2,6)) - 
               t1*(68 - (229 + 32*s2)*t2 + 
                  (228 + 147*s2 - 11*Power(s2,2))*Power(t2,2) + 
                  (-12 - 220*s2 + 23*Power(s2,2))*Power(t2,3) + 
                  (-89 + 92*s2 + 21*Power(s2,2))*Power(t2,4) + 
                  (40 + 12*s2 + 3*Power(s2,2))*Power(t2,5) + 
                  (-7 + s2)*Power(t2,6) + Power(t2,7))) + 
            Power(s1,5)*(-1 + t2)*
             (Power(t1,2)*(1 - 14*t2 + 24*Power(t2,2) + 
                  20*Power(t2,3) - 3*Power(t2,4)) + 
               t1*(2 + (-23 + 7*s2)*t2 + (73 - 3*s2)*Power(t2,2) - 
                  9*(11 + 7*s2)*Power(t2,3) + 
                  3*(35 + s2)*Power(t2,4) - 58*Power(t2,5)) + 
               t2*(2*Power(s2,2)*t2*(5 - t2 + 10*Power(t2,2)) + 
                  Power(-1 + t2,2)*
                   (4 - 42*t2 + 123*Power(t2,2) + 7*Power(t2,3)) + 
                  s2*(2 - 5*t2 - 75*Power(t2,2) + 113*Power(t2,3) - 
                     35*Power(t2,4)))) + 
            Power(s1,3)*t2*(2*Power(t1,3)*
                (2 - 14*t2 - 9*Power(t2,2) + 32*Power(t2,3) + 
                  11*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,2)*(7 + (-125 + 19*s2)*t2 + 
                  (426 + 50*s2)*Power(t2,2) - 
                  2*(197 + 27*s2)*Power(t2,3) - 
                  3*(7 + 54*s2)*Power(t2,4) + 
                  (127 + 3*s2)*Power(t2,5) - 20*Power(t2,6)) - 
               t1*(1 + (85 - 32*s2)*t2 + 
                  (-413 + 96*s2 + 26*Power(s2,2))*Power(t2,2) + 
                  (809 + 184*s2 + 4*Power(s2,2))*Power(t2,3) - 
                  (637 + 496*s2 + 162*Power(s2,2))*Power(t2,4) + 
                  (139 + 264*s2 - 12*Power(s2,2))*Power(t2,5) - 
                  (55 + 16*s2)*Power(t2,6) + 71*Power(t2,7)) + 
               t2*(-2*Power(s2,3)*Power(t2,2)*
                   (5 + 8*t2 + 11*Power(t2,2)) + 
                  Power(-1 + t2,3)*
                   (-7 + 149*t2 - 360*Power(t2,2) + 510*Power(t2,3) + 
                     2*Power(t2,4)) - 
                  s2*Power(-1 + t2,2)*
                   (-6 + 48*t2 - 169*Power(t2,2) - 228*Power(t2,3) + 
                     107*Power(t2,4)) + 
                  Power(s2,2)*t2*
                   (1 - 74*t2 + 320*Power(t2,2) - 398*Power(t2,3) + 
                     151*Power(t2,4)))) + 
            Power(s1,4)*(Power(t1,3)*
                (-1 + 13*t2 - 14*Power(t2,2) + 10*Power(t2,3) - 
                  23*Power(t2,4) + 3*Power(t2,5)) + 
               Power(t1,2)*(-1 + (22 - 7*s2)*t2 - 
                  (116 + 7*s2)*Power(t2,2) + 151*Power(t2,3) + 
                  (33 + 53*s2)*Power(t2,4) - 3*(35 + s2)*Power(t2,5) + 
                  16*Power(t2,6)) + 
               t2*(-2 + 8*(5 + 3*s2)*t2 + 
                  (-267 - 108*s2 + 31*Power(s2,2) + 3*Power(s2,3))*
                   Power(t2,2) + 
                  (917 - 67*s2 - 94*Power(s2,2) + 2*Power(s2,3))*
                   Power(t2,3) + 
                  (-1510 + 447*s2 + 149*Power(s2,2) + 7*Power(s2,3))*
                   Power(t2,4) + 
                  (1140 - 381*s2 - 86*Power(s2,2))*Power(t2,5) + 
                  (-309 + 85*s2)*Power(t2,6) - 9*Power(t2,7)) + 
               t1*t2*(Power(s2,2)*t2*
                   (5 + 9*t2 - 47*Power(t2,2) - 3*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (15 - 100*t2 + 173*Power(t2,2) - 21*Power(t2,3) + 
                     95*Power(t2,4)) + 
                  s2*(-2 + 27*t2 + 56*Power(t2,2) - 304*Power(t2,3) + 
                     234*Power(t2,4) - 11*Power(t2,5)))) + 
            Power(s1,2)*Power(t2,2)*
             (12*Power(s2,3)*Power(t2,3)*(1 + 3*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*Power(t2,2)*
                (-3 + 136*t2 - 516*Power(t2,2) + 520*Power(t2,3) - 
                  137*Power(t2,4)) + 
               2*Power(t1,3)*t2*
                (4 + 31*t2 - 48*Power(t2,2) - 25*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*Power(-1 + t2,2)*t2*
                (-59 + 88*t2 - 234*Power(t2,2) - 78*Power(t2,3) + 
                  71*Power(t2,4)) + 
               Power(-1 + t2,3)*
                (-14 - 42*t2 - 251*Power(t2,2) + 478*Power(t2,3) - 
                  459*Power(t2,4) + 8*Power(t2,5)) + 
               Power(t1,2)*(-32 + (207 - 16*s2)*t2 - 
                  6*(66 + 17*s2)*Power(t2,2) + 
                  2*(37 + 75*s2)*Power(t2,3) + 
                  2*(135 + 89*s2)*Power(t2,4) + 
                  (-137 + 6*s2)*Power(t2,5) + 14*Power(t2,6)) + 
               2*t1*t2*(-3*Power(s2,2)*t2*
                   (-8 + 7*t2 + 34*Power(t2,2) + 3*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (110 - 193*t2 + 125*Power(t2,2) + 50*Power(t2,3) + 
                     14*Power(t2,4)) + 
                  s2*(-21 + 20*t2 + 166*Power(t2,2) - 198*Power(t2,3) + 
                     39*Power(t2,4) - 6*Power(t2,5))))) + 
         s*(s1 - t2)*(-1 + t2)*
          (2*Power(s1,6)*(-2 + s2 + t1)*Power(-1 + t2,3)*t2 - 
            Power(s1,2)*t2*(-1 + 3*(-7 + 4*s2)*t2 + 
               (-30 - 103*s2 + 32*Power(s2,2))*Power(t2,2) + 
               (316 + 289*s2 - 168*Power(s2,2) - 27*Power(s2,3))*
                Power(t2,3) - 
               (603 + 435*s2 - 343*Power(s2,2) + 78*Power(s2,3))*
                Power(t2,4) + 
               (549 + 351*s2 - 256*Power(s2,2) - 27*Power(s2,3))*
                Power(t2,5) + 
               (-270 - 110*s2 + 49*Power(s2,2))*Power(t2,6) - 
               4*(-15 + s2)*Power(t2,7) + 
               Power(t1,3)*(-6 + 12*t2 + 20*Power(t2,2) + 
                  72*Power(t2,3) + 36*Power(t2,4) - 2*Power(t2,5)) + 
               Power(t1,2)*(13 + (-67 + 12*s2)*t2 + 
                  (92 - 35*s2)*Power(t2,2) - 
                  4*(-9 + 58*s2)*Power(t2,3) - 
                  (90 + 127*s2)*Power(t2,4) + 
                  (15 - 14*s2)*Power(t2,5) + Power(t2,6)) + 
               t1*t2*(-56 + (251 + 20*s2 - 5*Power(s2,2))*t2 + 
                  (-325 - 80*s2 + 163*Power(s2,2))*Power(t2,2) + 
                  (95 - 6*s2 + 211*Power(s2,2))*Power(t2,3) + 
                  (65 + 64*s2 + 27*Power(s2,2))*Power(t2,4) + 
                  2*(-17 + s2)*Power(t2,5) + 4*Power(t2,6))) - 
            Power(t2,3)*(-5 + (-47 + 23*s2)*t2 + 
               (161 - 128*s2 + 18*Power(s2,2))*Power(t2,2) - 
               (131 - 214*s2 + 73*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,3) - 
               (33 + 157*s2 - 93*Power(s2,2) + 19*Power(s2,3))*
                Power(t2,4) + 
               (85 + 72*s2 - 45*Power(s2,2) + Power(s2,3))*
                Power(t2,5) + 
               (-35 - 27*s2 + 7*Power(s2,2))*Power(t2,6) + 
               (5 + 3*s2)*Power(t2,7) + 
               Power(t1,3)*(-3 + 4*t2 + 9*Power(t2,2) + 
                  11*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(3 + (-29 + 14*s2)*t2 + 
                  (18 - 32*s2)*Power(t2,2) - 
                  8*(-4 + 5*s2)*Power(t2,3) - 
                  (23 + 9*s2)*Power(t2,4) + (-1 + s2)*Power(t2,5)) + 
               t1*(7 + (32 - 29*s2)*t2 + 
                  (-104 + 135*s2 - 13*Power(s2,2))*Power(t2,2) + 
                  (97 - 157*s2 + 63*Power(s2,2))*Power(t2,3) + 
                  3*(-14 + 11*s2 + 6*Power(s2,2))*Power(t2,4) + 
                  (7 + 22*s2 - 2*Power(s2,2))*Power(t2,5) + 
                  (3 - 4*s2)*Power(t2,6))) + 
            s1*Power(t2,2)*(-17 + (-37 + 36*s2)*t2 + 
               (154 - 224*s2 + 43*Power(s2,2))*Power(t2,2) + 
               (8 + 426*s2 - 194*Power(s2,2) - 17*Power(s2,3))*
                Power(t2,3) - 
               (347 + 413*s2 - 297*Power(s2,2) + 64*Power(s2,3))*
                Power(t2,4) + 
               (365 + 269*s2 - 172*Power(s2,2) - 7*Power(s2,3))*
                Power(t2,5) + 
               (-150 - 99*s2 + 26*Power(s2,2))*Power(t2,6) + 
               (24 + 5*s2)*Power(t2,7) + 
               Power(t1,3)*(3 - 21*t2 + 56*Power(t2,2) + 
                  40*Power(t2,3) + 9*Power(t2,4) + Power(t2,5)) - 
               Power(t1,2)*(19 - (35 + 22*s2)*t2 + 
                  (84 + 46*s2)*Power(t2,2) + 
                  (-151 + 191*s2)*Power(t2,3) + 
                  (87 + 42*s2)*Power(t2,4) + (-8 + 7*s2)*Power(t2,5) + 
                  4*Power(t2,6)) + 
               t1*(39 - (101 + 34*s2)*t2 + 
                  (152 + 181*s2 - 24*Power(s2,2))*Power(t2,2) + 
                  (-140 - 213*s2 + 174*Power(s2,2))*Power(t2,3) + 
                  (44 - 7*s2 + 108*Power(s2,2))*Power(t2,4) + 
                  (-2 + 75*s2 + 6*Power(s2,2))*Power(t2,5) + 
                  (5 - 2*s2)*Power(t2,6) + 3*Power(t2,7))) - 
            Power(s1,5)*(-1 + t2)*
             (-(Power(t1,2)*(-1 + 11*t2 + 5*Power(t2,2) + 
                    3*Power(t2,3))) + 
               t2*(-4*Power(-1 + t2,2)*(-1 + 7*t2) - 
                  Power(s2,2)*(5 + 13*Power(t2,2)) + 
                  s2*(4 + 17*t2 - 28*Power(t2,2) + 7*Power(t2,3))) + 
               t1*(2 - 14*t2 + 13*Power(t2,2) - 10*Power(t2,3) + 
                  9*Power(t2,4) + 
                  s2*(-1 + 8*t2 + 21*Power(t2,2) + 8*Power(t2,3)))) + 
            Power(s1,3)*(Power(t1,3)*
                (-1 + 3*t2 + 4*Power(t2,2) + 60*Power(t2,3) + 
                  17*Power(t2,4) + 5*Power(t2,5)) + 
               Power(t1,2)*(2 + (-34 + 5*s2)*t2 + 
                  (146 - 30*s2)*Power(t2,2) - 
                  (157 + 87*s2)*Power(t2,3) + 
                  (34 - 146*s2)*Power(t2,4) + (7 - 6*s2)*Power(t2,5) + 
                  2*Power(t2,6)) + 
               t1*(1 + (-23 + 4*s2)*t2 + 
                  2*(35 - 12*s2 + 5*Power(s2,2))*Power(t2,2) + 
                  (-50 - 68*s2 + 56*Power(s2,2))*Power(t2,3) + 
                  (-69 + 118*s2 + 170*Power(s2,2))*Power(t2,4) + 
                  (111 - 16*s2 + 28*Power(s2,2))*Power(t2,5) - 
                  2*(17 + 7*s2)*Power(t2,6) - 6*Power(t2,7)) - 
               t2*(Power(s2,3)*Power(t2,2)*
                   (19 + 40*t2 + 29*Power(t2,2)) - 
                  Power(-1 + t2,3)*
                   (-2 + 43*t2 - 45*Power(t2,2) + 88*Power(t2,3)) + 
                  Power(s2,2)*t2*
                   (-7 + 51*t2 - 172*Power(t2,2) + 189*Power(t2,3) - 
                     61*Power(t2,4)) + 
                  s2*Power(-1 + t2,2)*
                   (1 + 18*t2 - 89*Power(t2,2) + 36*Power(t2,3) + 
                     14*Power(t2,4)))) + 
            Power(s1,4)*(Power(t1,3)*
                (1 - 9*t2 + 5*Power(t2,2) - 22*Power(t2,3) + 
                  3*Power(t2,4)) + 
               t2*(Power(s2,3)*t2*(5 + 7*t2 + 10*Power(t2,2)) - 
                  Power(-1 + t2,3)*(4 - 17*t2 + 71*Power(t2,2)) + 
                  Power(s2,2)*t2*
                   (9 - 38*t2 + 73*Power(t2,2) - 44*Power(t2,3)) + 
                  s2*Power(-1 + t2,2)*
                   (9 - 33*t2 - 33*Power(t2,2) + 13*Power(t2,3))) + 
               Power(t1,2)*(2 - 19*t2 + 20*Power(t2,2) + 
                  33*Power(t2,3) - 36*Power(t2,4) + 
                  s2*(-1 + 9*t2 + 6*Power(t2,2) + 52*Power(t2,3))) + 
               t1*(-(Power(s2,2)*t2*
                     (4 + 4*t2 + 49*Power(t2,2) + 9*Power(t2,3))) + 
                  Power(-1 + t2,2)*
                   (1 - 9*t2 + 25*Power(t2,2) + 13*Power(t2,3) + 
                     14*Power(t2,4)) + 
                  s2*(1 - t2 + 35*Power(t2,2) - 97*Power(t2,3) + 
                     40*Power(t2,4) + 22*Power(t2,5))))) + 
         Power(s,3)*t2*(-2*Power(s1,5)*Power(-1 + t2,2)*
             (t1*(1 + 2*t2 - 12*Power(t2,2)) + 
               t2*(-7 + s2*(2 - 3*t2) + 14*t2 + 3*Power(t2,2))) - 
            Power(t2,3)*(-43 + (93 + 35*s2)*t2 - 
               (59 + 106*s2)*Power(t2,2) + 
               (117 + 97*s2 + 4*Power(s2,2))*Power(t2,3) + 
               (-297 + 8*s2 + 32*Power(s2,2))*Power(t2,4) + 
               (277 - 48*s2 - 12*Power(s2,2))*Power(t2,5) + 
               (-93 + 14*s2)*Power(t2,6) + 5*Power(t2,7) + 
               Power(t1,3)*(3 - 7*t2 - 2*Power(t2,2) - 7*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*(-21 + 72*t2 + 2*(-37 + 5*s2)*Power(t2,2) + 
                  (73 + 16*s2)*Power(t2,3) - (29 + 2*s2)*Power(t2,4) + 
                  3*Power(t2,5)) + 
               t1*(67 - (211 + 21*s2)*t2 - 
                  3*(-82 - 18*s2 + Power(s2,2))*Power(t2,2) - 
                  (126 + 119*s2 + 10*Power(s2,2))*Power(t2,3) + 
                  (20 + 42*s2 + Power(s2,2))*Power(t2,4) - 
                  (3 + 4*s2)*Power(t2,5) + 7*Power(t2,6))) + 
            s1*Power(t2,2)*(-25 + (73 + 50*s2)*t2 - 
               23*(9 + 7*s2)*Power(t2,2) + 
               (672 + 151*s2 + 7*Power(s2,2))*Power(t2,3) + 
               (-1201 + 117*s2 + 98*Power(s2,2))*Power(t2,4) + 
               (1007 - 225*s2 - 45*Power(s2,2))*Power(t2,5) + 
               (-327 + 68*s2)*Power(t2,6) + 8*Power(t2,7) + 
               Power(t1,3)*t2*
                (5 - 30*t2 - 15*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(22 - 2*(21 + 2*s2)*t2 + 
                  (53 + 42*s2)*Power(t2,2) + 
                  2*(67 + 18*s2)*Power(t2,3) - 
                  (129 + 2*s2)*Power(t2,4) + 22*Power(t2,5)) + 
               t1*(20 - (177 + 11*s2)*t2 + 
                  (518 - 9*s2 - 9*Power(s2,2))*Power(t2,2) - 
                  (660 + 203*s2 + 30*Power(s2,2))*Power(t2,3) + 
                  (348 + 125*s2 + 3*Power(s2,2))*Power(t2,4) - 
                  (91 + 22*s2)*Power(t2,5) + 42*Power(t2,6))) + 
            Power(s1,4)*(Power(t1,2)*
                (2 - 23*t2 + 15*Power(t2,2) + 25*Power(t2,3) - 
                  7*Power(t2,4)) + 
               t1*(2 + (-20 + 11*s2)*t2 + 2*(44 + 7*s2)*Power(t2,2) - 
                  (185 + 63*s2)*Power(t2,3) + 
                  2*(107 + 7*s2)*Power(t2,4) - 99*Power(t2,5)) + 
               t2*(Power(s2,2)*t2*(5 - 2*t2 + 9*Power(t2,2)) + 
                  2*Power(-1 + t2,2)*
                   (3 - 44*t2 + 77*Power(t2,2) + 11*Power(t2,3)) + 
                  s2*(4 - 2*t2 - 89*Power(t2,2) + 130*Power(t2,3) - 
                     43*Power(t2,4)))) + 
            Power(s1,3)*(Power(t1,3)*
                (3 - 7*t2 - 2*Power(t2,2) - 7*Power(t2,3) + Power(t2,4)) \
+ Power(t1,2)*(2 - 25*t2 + (137 + 10*s2)*Power(t2,2) + 
                  2*(-41 + 8*s2)*Power(t2,3) - 
                  (61 + 2*s2)*Power(t2,4) + 17*Power(t2,5)) + 
               t2*(12 - (88 + 17*s2)*t2 + 
                  (400 + 33*s2 - 11*Power(s2,2))*Power(t2,2) + 
                  (-936 + 217*s2 + 38*Power(s2,2))*Power(t2,3) + 
                  (936 - 341*s2 - 39*Power(s2,2))*Power(t2,4) + 
                  4*(-74 + 27*s2)*Power(t2,5) - 28*Power(t2,6)) + 
               t1*t2*(-23 + 145*t2 - 342*Power(t2,2) + 354*Power(t2,3) - 
                  271*Power(t2,4) + 137*Power(t2,5) + 
                  Power(s2,2)*t2*(-3 - 10*t2 + Power(t2,2)) + 
                  s2*(7 - 67*t2 - 65*Power(t2,2) + 191*Power(t2,3) - 
                     42*Power(t2,4)))) + 
            Power(s1,2)*t2*(Power(t1,3)*
                (-6 + 7*t2 + 30*Power(t2,2) + 3*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,2)*(-15 + (77 + 4*s2)*t2 - 
                  (241 + 42*s2)*Power(t2,2) + (66 - 36*s2)*Power(t2,3) + 
                  2*(53 + s2)*Power(t2,4) - 29*Power(t2,5)) + 
               t1*(5 + (71 - 17*s2)*t2 + 
                  (-395 + 119*s2 + 9*Power(s2,2))*Power(t2,2) + 
                  (704 + 135*s2 + 30*Power(s2,2))*Power(t2,3) - 
                  (485 + 211*s2 + 3*Power(s2,2))*Power(t2,4) + 
                  (197 + 46*s2)*Power(t2,5) - 97*Power(t2,6)) + 
               t2*(3*Power(s2,2)*Power(t2,2)*
                   (1 - 34*t2 + 21*Power(t2,2)) + 
                  Power(-1 + t2,2)*
                   (-28 + 174*t2 - 457*Power(t2,2) + 454*Power(t2,3) + 
                     9*Power(t2,4)) - 
                  s2*(15 - 68*t2 + 81*Power(t2,2) + 251*Power(t2,3) - 
                     404*Power(t2,4) + 125*Power(t2,5))))))*R1q(t2))/
     (s*(-1 + s1)*(-1 + s2)*(-1 + t1)*Power(s1 - t2,3)*Power(-1 + t2,3)*t2*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) + 
    (8*(2*Power(s,8)*Power(t2,2)*(3 + 3*s2 - 8*t1 + 4*t2) - 
         Power(s,7)*(Power(t1,3)*(-2 + t2) + 
            Power(t1,2)*t2*(2*s1 - 2*s2 + 9*t2) + 
            t1*t2*(s1*(32 - 97*t2) + t2*(-106 - 19*s2 + 19*t2)) + 
            t2*(t2*(26 + 6*Power(s2,2) + 44*t2 - 9*Power(t2,2) + 
                  s2*(52 + 5*t2)) + 
               s1*(-12 + 20*t2 + 39*Power(t2,2) + 4*s2*(-3 + 7*t2)))) + 
         Power(s,6)*(Power(t1,3)*(-10 + 9*t2 - 5*Power(t2,2)) + 
            Power(t1,2)*(6 + (19 - 16*s2)*t2 + 
               (67 + 9*s2)*Power(t2,2) - 3*Power(t2,3)) - 
            t1*t2*(-6 + 252*t2 + Power(s2,2)*t2 - 95*Power(t2,2) + 
               11*Power(t2,3) + s2*(2 + 147*t2 - 14*Power(t2,2))) + 
            Power(t2,2)*(8 + 48*Power(s2,2) + 99*t2 - 45*Power(t2,2) + 
               3*Power(t2,3) + s2*(173 + 23*t2 - 9*Power(t2,2))) + 
            Power(s1,2)*(6 - 52*t2 + 30*Power(t2,2) + 75*Power(t2,3) + 
               2*Power(t1,2)*(-1 + 7*t2) + 
               s2*(6 - 4*(11 + t1)*t2 + 49*Power(t2,2)) - 
               2*t1*(8 - 83*t2 + 123*Power(t2,2))) + 
            s1*(Power(t1,3)*(-17 + 7*t2) + 
               Power(t1,2)*(-6 + s2*(8 - 11*t2) - 11*t2 + 
                  46*Power(t2,2)) + 
               t1*t2*(170 + 4*Power(s2,2) + s2*(40 - 83*t2) - 444*t2 + 
                  103*Power(t2,2)) + 
               t2*(-40 + 13*t2 + 140*Power(t2,2) - 36*Power(t2,3) + 
                  3*Power(s2,2)*(-4 + 5*t2) + 
                  2*s2*(-46 + 79*t2 + 10*Power(t2,2))))) - 
         Power(s,5)*(Power(t1,3)*
             (-18 + 25*t2 - 24*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,2)*(24 + (100 - 54*s2)*t2 + 
               28*(5 + 2*s2)*Power(t2,2) + (-40 + s2)*Power(t2,3)) + 
            t1*(-6 + (-17 + 10*s2)*t2 - 
               (204 + 401*s2 + 14*Power(s2,2))*Power(t2,2) + 
               (175 + 114*s2 - 9*Power(s2,2))*Power(t2,3) - 
               (40 + 9*s2)*Power(t2,4) + 2*Power(t2,5)) + 
            Power(s1,3)*(24 - 99*t2 + 2*Power(s2,2)*t2 + 
               40*Power(t2,2) + 70*Power(t2,3) + 
               6*Power(t1,2)*(-2 + 7*t2) + 
               t1*(-69 + 350*t2 - 335*Power(t2,2)) + 
               s2*(16 + t1*(4 - 24*t2) - 58*t2 + 35*Power(t2,2))) + 
            t2*(-6 - 153*t2 + 122*Power(t2,2) - 98*Power(t2,3) + 
               12*Power(t2,4) + 3*Power(s2,3)*t2*(2 + t2) - 
               Power(s2,2)*t2*(-129 + Power(t2,2)) + 
               s2*(4 + 252*t2 - 15*Power(t2,2) - 42*Power(t2,3) + 
                  4*Power(t2,4))) - 
            Power(s1,2)*(-14 + Power(t1,3)*(60 - 21*t2) + 64*t2 + 
               2*Power(s2,3)*t2 + 61*Power(t2,2) - 145*Power(t2,3) + 
               54*Power(t2,4) + 
               Power(t1,2)*(32 + 51*t2 - 87*Power(t2,2)) + 
               t1*(70 - 474*t2 + 728*Power(t2,2) - 230*Power(t2,3)) + 
               Power(s2,2)*(-6 + 20*t2 + 7*Power(t2,2) - 
                  5*t1*(-2 + 3*t2)) + 
               s2*(-40 + 193*t2 - 167*Power(t2,2) - 34*Power(t2,3) + 
                  3*Power(t1,2)*(-17 + 8*t2) + 
                  t1*(9 - 150*t2 + 119*Power(t2,2)))) + 
            s1*(Power(t1,3)*(-52 + 56*t2 - 30*Power(t2,2)) + 
               Power(t1,2)*(-1 + 16*t2 + 246*Power(t2,2) - 
                  7*Power(t2,3) + s2*(32 - 87*t2 + 41*Power(t2,2))) + 
               t2*(38 - 195*t2 - 9*Power(s2,3)*t2 + 229*Power(t2,2) - 
                  92*Power(t2,3) + 10*Power(t2,4) + 
                  Power(s2,2)*(-82 + 76*t2 + 7*Power(t2,2)) + 
                  s2*(-268 + 298*t2 + 71*Power(t2,2) - 31*Power(t2,3))) \
+ t1*(6 + 370*t2 - 579*Power(t2,2) + 270*Power(t2,3) - 53*Power(t2,4) + 
                  2*Power(s2,2)*t2*(16 + t2) + 
                  2*s2*(-5 + 86*t2 - 234*Power(t2,2) + 23*Power(t2,3))))) \
+ Power(s,4)*(2 + 3*t2 - 329*Power(t2,2) + 114*s2*Power(t2,2) + 
            109*Power(s2,2)*Power(t2,2) + 26*Power(s2,3)*Power(t2,2) + 
            186*Power(t2,3) - 199*s2*Power(t2,3) + 
            5*Power(s2,2)*Power(t2,3) + 7*Power(s2,3)*Power(t2,3) - 
            106*Power(t2,4) - 30*s2*Power(t2,4) - 
            17*Power(s2,2)*Power(t2,4) - Power(s2,3)*Power(t2,4) + 
            29*Power(t2,5) + 10*s2*Power(t2,5) + 
            Power(s2,2)*Power(t2,5) + 
            Power(t1,3)*(-10 + 25*t2 - 37*Power(t2,2) + 6*Power(t2,3)) + 
            Power(t1,2)*(30 + (239 - 100*s2)*t2 + 
               (24 + 133*s2)*Power(t2,2) - (119 + s2)*Power(t2,3) - 
               2*(-6 + s2)*Power(t2,4)) + 
            t1*(-18 + (-159 + 68*s2)*t2 + 
               (197 - 491*s2 - 43*Power(s2,2))*Power(t2,2) + 
               (123 + 316*s2 - 31*Power(s2,2))*Power(t2,3) + 
               (-57 - 49*s2 + 4*Power(s2,2))*Power(t2,4) + 
               2*(3 + s2)*Power(t2,5)) + 
            Power(s1,4)*(36 + s2*(15 + t1*(20 - 60*t2) - 32*t2) - 
               105*t2 + 50*Power(t2,2) + 30*Power(t2,3) + 
               2*Power(s2,2)*(-1 + 5*t2) + 10*Power(t1,2)*(-3 + 7*t2) - 
               4*t1*(29 - 95*t2 + 65*Power(t2,2))) - 
            Power(s1,3)*(-17 + 3*t2 + 70*Power(t2,2) - 31*Power(t2,3) + 
               36*Power(t2,4) + Power(s2,3)*(-4 + 5*t2) - 
               5*Power(t1,3)*(-23 + 7*t2) + 
               Power(t1,2)*(64 + 90*t2 - 60*Power(t2,2)) + 
               Power(s2,2)*(1 + t1*(47 - 15*t2) - 5*t2 + 
                  63*Power(t2,2)) + 
               t1*(111 - 378*t2 + 587*Power(t2,2) - 270*Power(t2,3)) + 
               s2*(-48 + 81*t2 - 62*Power(t2,2) - 36*Power(t2,3) + 
                  5*Power(t1,2)*(-27 + 5*t2) + 
                  t1*(9 - 202*t2 + 15*Power(t2,2)))) + 
            Power(s1,2)*(-34 + 262*t2 + 2*Power(s2,3)*(2 - 13*t2)*t2 - 
               336*Power(t2,2) + 222*Power(t2,3) - 40*Power(t2,4) + 
               12*Power(t2,5) + 
               Power(t1,3)*(-86 + 145*t2 - 75*Power(t2,2)) + 
               Power(t1,2)*(-5 + 44*t2 + 349*Power(t2,2) + 
                  10*Power(t2,3)) - 
               2*t1*(48 - 206*t2 + 130*Power(t2,2) - 99*Power(t2,3) + 
                  51*Power(t2,4)) + 
               Power(s2,2)*(40 + 11*t2 - 37*Power(t2,2) + 
                  38*Power(t2,3) + t1*(-33 + 95*t2 + 32*Power(t2,2))) + 
               s2*(89 - 391*t2 + 185*Power(t2,2) + 57*Power(t2,3) - 
                  44*Power(t2,4) + 
                  Power(t1,2)*(99 - 215*t2 + 70*Power(t2,2)) + 
                  t1*(-73 + 198*t2 - 591*Power(t2,2) + 36*Power(t2,3)))) \
+ s1*(Power(s2,3)*t2*(-12 - 23*t2 + 18*Power(t2,2)) + 
               Power(t1,3)*(-45 + 99*t2 - 80*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,2)*(-33 + 129*t2 + 171*Power(t2,2) - 
                  123*Power(t2,3) - 2*Power(t2,4)) + 
               t2*(247 - 510*t2 + 180*Power(t2,2) - 112*Power(t2,3) + 
                  14*Power(t2,4)) + 
               t1*(23 + 111*t2 - 98*Power(t2,2) + 161*Power(t2,3) - 
                  67*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-194 + 50*t2 + 56*Power(t2,2) - 3*Power(t2,3) + 
                  t1*(94 - 25*t2 - 45*Power(t2,2))) + 
               s2*(2 - 183*t2 + 254*Power(t2,2) - 27*Power(t2,3) - 
                  51*Power(t2,4) + 14*Power(t2,5) + 
                  Power(t1,2)*
                   (52 - 212*t2 + 153*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(-36 + 346*t2 - 579*Power(t2,2) + 268*Power(t2,3) - 
                     33*Power(t2,4))))) + 
         Power(-1 + s1,4)*(-2 + 
            2*Power(s1,4)*Power(s2 - t1,2)*(-1 + t2) + 19*t2 - 4*s2*t2 - 
            32*Power(t2,2) + 24*s2*Power(t2,2) - 
            23*Power(s2,2)*Power(t2,2) + 2*Power(s2,3)*Power(t2,2) + 
            18*Power(t2,3) - 40*s2*Power(t2,3) + 
            29*Power(s2,2)*Power(t2,3) - 5*Power(s2,3)*Power(t2,3) - 
            6*Power(t2,4) + 24*s2*Power(t2,4) - 
            7*Power(s2,2)*Power(t2,4) - Power(s2,3)*Power(t2,4) + 
            3*Power(t2,5) - 4*s2*Power(t2,5) + Power(s2,2)*Power(t2,5) + 
            Power(t1,3)*(2 - 5*t2 + 9*Power(t2,2) - 2*Power(t2,3)) - 
            Power(t1,2)*(6 + (-29 + 4*s2)*t2 + (46 + s2)*Power(t2,2) + 
               (-11 + 5*s2)*Power(t2,3) + 2*(-6 + s2)*Power(t2,4)) + 
            t1*(6 + (-43 + 8*s2)*t2 + 
               (53 + 11*s2 + 3*Power(s2,2))*Power(t2,2) + 
               (-3 - 8*s2 + 5*Power(s2,2))*Power(t2,3) + 
               (-11 - 13*s2 + 4*Power(s2,2))*Power(t2,4) + 
               2*(-1 + s2)*Power(t2,5)) + 
            Power(s1,3)*(Power(t1,3)*(-5 + t2) - 2*Power(-1 + t2,2) + 
               5*t1*Power(-1 + t2,2) + Power(s2,3)*(1 + 3*t2) - 
               4*Power(t1,2)*(1 - 3*t2 + 2*Power(t2,2)) - 
               Power(s2,2)*(1 - 6*t2 + 5*Power(t2,2) + t1*(7 + 5*t2)) + 
               s2*(-Power(-1 + t2,2) + Power(t1,2)*(11 + t2) + 
                  t1*(3 - 14*t2 + 11*Power(t2,2)))) - 
            Power(s1,2)*(-(Power(-1 + t2,2)*(-1 + 5*t2)) + 
               Power(t1,3)*(-2 - 15*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(-2 + 7*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(1 + 10*t2 - 3*Power(t2,2) - 
                  8*Power(t2,3)) + 8*t1*(2 - 3*t2 + Power(t2,3)) - 
               Power(s2,2)*(-23 + 31*t2 - 13*Power(t2,2) + 
                  5*Power(t2,3) + t1*(3 + 19*t2 + 14*Power(t2,2))) + 
               s2*(2*(-8 + t2)*Power(-1 + t2,2) + 
                  Power(t1,2)*(9 + 23*t2 + 4*Power(t2,2)) + 
                  t1*(-27 + 22*t2 - 3*Power(t2,2) + 8*Power(t2,3)))) + 
            s1*(-3*Power(-1 + t2,2)*(5 - t2 + 2*Power(t2,2)) + 
               Power(s2,3)*t2*(-4 + 11*t2 + 5*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(31 + 25*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(1 - 11*t2 - 4*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,2)*(17 - 47*t2 + 9*Power(t2,2) + 
                  19*Power(t2,3) + 2*Power(t2,4)) - 
               Power(s2,2)*t2*
                (-46 + 59*t2 - 16*Power(t2,2) + 3*Power(t2,3) + 
                  t1*(6 + 17*t2 + 13*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(4 - 32*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*
                   (4 + 10*t2 + 17*Power(t2,2) + 5*Power(t2,3)) - 
                  t1*(8 + 38*t2 - 27*Power(t2,2) - 20*Power(t2,3) + 
                     Power(t2,4))))) - 
         s*Power(-1 + s1,2)*(-4 + 18*t1 - 24*Power(t1,2) + 
            10*Power(t1,3) + 82*t2 - 24*s2*t2 - 201*t1*t2 + 
            50*s2*t1*t2 + 140*Power(t1,2)*t2 - 26*s2*Power(t1,2)*t2 - 
            21*Power(t1,3)*t2 - 140*Power(t2,2) + 98*s2*Power(t2,2) - 
            119*Power(s2,2)*Power(t2,2) + 14*Power(s2,3)*Power(t2,2) + 
            264*t1*Power(t2,2) + 53*s2*t1*Power(t2,2) + 
            2*Power(s2,2)*t1*Power(t2,2) - 222*Power(t1,2)*Power(t2,2) + 
            16*s2*Power(t1,2)*Power(t2,2) + 32*Power(t1,3)*Power(t2,2) + 
            94*Power(t2,3) - 170*s2*Power(t2,3) + 
            104*Power(s2,2)*Power(t2,3) - 17*Power(s2,3)*Power(t2,3) - 
            39*t1*Power(t2,3) + 34*s2*t1*Power(t2,3) + 
            11*Power(s2,2)*t1*Power(t2,3) + 16*Power(t1,2)*Power(t2,3) - 
            19*s2*Power(t1,2)*Power(t2,3) - 6*Power(t1,3)*Power(t2,3) - 
            44*Power(t2,4) + 110*s2*Power(t2,4) - 
            31*Power(s2,2)*Power(t2,4) - 4*Power(s2,3)*Power(t2,4) - 
            36*t1*Power(t2,4) - 61*s2*t1*Power(t2,4) + 
            16*Power(s2,2)*t1*Power(t2,4) + 48*Power(t1,2)*Power(t2,4) - 
            8*s2*Power(t1,2)*Power(t2,4) + 12*Power(t2,5) - 
            14*s2*Power(t2,5) + 4*Power(s2,2)*Power(t2,5) - 
            6*t1*Power(t2,5) + 8*s2*t1*Power(t2,5) + 
            Power(s1,5)*(2*Power(s2,2)*(-4 + 5*t2) - 
               s2*(-3 + 2*t2 + Power(t2,2) + 4*t1*(-5 + 6*t2)) + 
               t1*(-5 + 6*t2 - Power(t2,2) + 2*t1*(-6 + 7*t2))) + 
            Power(s1,4)*(-9 + 18*t2 - 5*Power(t2,2) - 4*Power(t2,3) + 
               Power(t1,3)*(-32 + 7*t2) + Power(s2,3)*(7 + 10*t2) + 
               Power(t1,2)*(-16 + 37*t2 - 39*Power(t2,2)) + 
               t1*(28 - 42*t2 + 6*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(2 + 7*t2 - 27*Power(t2,2) - 
                  t1*(38 + 21*t2)) + 
               s2*(-22 + 34*t2 - 16*Power(t2,2) + 4*Power(t2,3) + 
                  Power(t1,2)*(63 + 4*t2) + 
                  t1*(19 - 50*t2 + 67*Power(t2,2)))) + 
            Power(s1,3)*(-16 + 61*t2 - 82*Power(t2,2) + 
               33*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,3)*(12 + 78*t2 - 30*Power(t2,2)) + 
               Power(s2,3)*(11 - 35*t2 - 20*Power(t2,2)) + 
               Power(t1,2)*(-25 + 32*t2 + 40*Power(t2,2) + 
                  37*Power(t2,3)) + 
               t1*(-55 + 56*t2 + 58*Power(t2,2) - 50*Power(t2,3) - 
                  9*Power(t2,4)) + 
               Power(s2,2)*(-111 + 199*t2 - 36*Power(t2,2) + 
                  32*Power(t2,3) + 2*t1*(6 + 37*t2 + 31*Power(t2,2))) - 
               s2*(-88 + 174*t2 - 93*Power(t2,2) + 2*Power(t2,3) + 
                  5*Power(t2,4) + 
                  Power(t1,2)*(30 + 123*t2 + 11*Power(t2,2)) + 
                  2*t1*(-58 + 92*t2 + 29*Power(t2,2) + 21*Power(t2,3)))) \
+ Power(s1,2)*(-42 + 125*t2 - 116*Power(t2,2) + 69*Power(t2,3) - 
               36*Power(t2,4) + 
               2*Power(t1,3)*
                (1 - 11*t2 - 10*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,3)*(10 - 27*t2 + 33*Power(t2,2) + 
                  14*Power(t2,3)) - 
               Power(t1,2)*(70 - 157*t2 + 161*Power(t2,2) + 
                  82*Power(t2,3) + 8*Power(t2,4)) + 
               t1*(76 - 95*t2 - 58*Power(t2,2) + 33*Power(t2,3) + 
                  42*Power(t2,4) + 2*Power(t2,5)) - 
               Power(s2,2)*(71 - 214*t2 + 351*Power(t2,2) - 
                  63*Power(t2,3) + 19*Power(t2,4) + 
                  t1*(6 - 7*t2 + 34*Power(t2,2) + 57*Power(t2,3))) + 
               s2*(17 - 195*t2 + 349*Power(t2,2) - 213*Power(t2,3) + 
                  40*Power(t2,4) + 2*Power(t2,5) + 
                  Power(t1,2)*
                   (-1 + 8*t2 + 62*Power(t2,2) + 21*Power(t2,3)) + 
                  t1*(83 - 168*t2 + 312*Power(t2,2) + 110*Power(t2,3) - 
                     9*Power(t2,4)))) - 
            s1*(61 - 134*t2 + 149*Power(t2,2) - 84*Power(t2,3) + 
               20*Power(t2,4) - 12*Power(t2,5) + 
               2*Power(t1,3)*
                (-4 + 21*t2 - 9*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,3)*t2*
                (24 - 33*t2 + Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(93 - 260*t2 + 98*Power(t2,2) - 
                  29*Power(t2,3) - 40*Power(t2,4)) + 
               t1*(-146 + 252*t2 - 83*Power(t2,2) - 80*Power(t2,3) + 
                  45*Power(t2,4) + 12*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-190 + 207*t2 - 181*Power(t2,2) + 30*Power(t2,3) - 
                  4*Power(t2,4) - 
                  2*t1*(2 - 15*t2 - 9*Power(t2,2) + 8*Power(t2,3))) + 
               s2*(-14 + 91*t2 - 265*Power(t2,2) + 271*Power(t2,3) - 
                  103*Power(t2,4) + 20*Power(t2,5) + 
                  Power(t1,2)*
                   (-16 - 9*t2 - 29*Power(t2,2) + 2*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  t1*(30 + 184*t2 - 42*Power(t2,2) + 70*Power(t2,3) + 
                     42*Power(t2,4) - 8*Power(t2,5))))) + 
         Power(s,3)*(-4 + 12*t1 - 10*Power(t1,3) - 68*t2 + 28*s2*t2 + 
            358*t1*t2 - 132*s2*t1*t2 - 336*Power(t1,2)*t2 + 
            110*s2*Power(t1,2)*t2 + 5*Power(t1,3)*t2 + 301*Power(t2,2) + 
            10*s2*Power(t2,2) + 80*Power(s2,2)*Power(t2,2) - 
            44*Power(s2,3)*Power(t2,2) - 590*t1*Power(t2,2) + 
            241*s2*t1*Power(t2,2) + 52*Power(s2,2)*t1*Power(t2,2) + 
            279*Power(t1,2)*Power(t2,2) - 
            152*s2*Power(t1,2)*Power(t2,2) + 8*Power(t1,3)*Power(t2,2) - 
            288*Power(t2,3) + 340*s2*Power(t2,3) - 
            56*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) + 
            27*t1*Power(t2,3) - 396*s2*t1*Power(t2,3) + 
            34*Power(s2,2)*t1*Power(t2,3) + 
            136*Power(t1,2)*Power(t2,3) + 
            14*s2*Power(t1,2)*Power(t2,3) - 4*Power(t1,3)*Power(t2,3) + 
            109*Power(t2,4) - 116*s2*Power(t2,4) + 
            48*Power(s2,2)*Power(t2,4) + 4*Power(s2,3)*Power(t2,4) + 
            52*t1*Power(t2,4) + 106*s2*t1*Power(t2,4) - 
            16*Power(s2,2)*t1*Power(t2,4) - 48*Power(t1,2)*Power(t2,4) + 
            8*s2*Power(t1,2)*Power(t2,4) - 40*Power(t2,5) - 
            2*s2*Power(t2,5) - 4*Power(s2,2)*Power(t2,5) - 
            4*t1*Power(t2,5) - 8*s2*t1*Power(t2,5) + 
            Power(s1,5)*(-24 + Power(t1,2)*(40 - 70*t2) + 
               Power(s2,2)*(8 - 20*t2) + 61*t2 - 36*Power(t2,2) - 
               3*Power(t2,3) + t1*(94 - 220*t2 + 111*Power(t2,2)) + 
               s2*(-9 + 8*t2 + 14*Power(t2,2) + 40*t1*(-1 + 2*t2))) + 
            Power(s1,3)*(100 - 304*t2 + 333*Power(t2,2) - 
               176*Power(t2,3) - 20*Power(t2,4) - 6*Power(t2,5) + 
               4*Power(t1,3)*(6 - 50*t2 + 25*Power(t2,2)) + 
               Power(s2,3)*(-1 + 9*t2 + 31*Power(t2,2)) - 
               2*Power(t1,2)*
                (3 + 90*t2 + 138*Power(t2,2) + 25*Power(t2,3)) + 
               2*t1*(3 - 54*t2 - 147*Power(t2,2) + 42*Power(t2,3) + 
                  49*Power(t2,4)) + 
               s2*(-112 + 454*t2 - 132*Power(t2,2) + 13*Power(t2,3) + 
                  36*Power(t2,4) + 
                  Power(t1,2)*(-52 + 310*t2 - 50*Power(t2,2)) + 
                  4*t1*t2*(64 + 119*t2 + 9*Power(t2,2))) + 
               Power(s2,2)*(45 - 321*t2 + 106*Power(t2,2) - 
                  77*Power(t2,3) - 4*t1*(-10 + 33*t2 + 22*Power(t2,2)))) \
+ Power(s1,4)*(13 - 13*Power(s2,3) - 68*t2 + 36*Power(t2,2) + 
               37*Power(t2,3) + 9*Power(t2,4) - 
               5*Power(t1,3)*(-26 + 7*t2) + 
               Power(t1,2)*(56 + 70*t2 + 25*Power(t2,2)) + 
               t1*(-16 + 20*t2 + 238*Power(t2,2) - 175*Power(t2,3)) + 
               Power(s2,2)*(16 - 13*t2 + 92*Power(t2,2) + 
                  2*t1*(44 + 5*t2)) + 
               s2*(18 + 10*Power(t1,2)*(-19 + t2) - 83*t2 + 
                  17*Power(t2,2) - 29*Power(t2,3) - 
                  t1*(14 + 108*t2 + 135*Power(t2,2)))) - 
            Power(s1,2)*(-84 + 369*t2 - 424*Power(t2,2) + 
               78*Power(t2,3) - 110*Power(t2,4) + 
               2*Power(t1,3)*
                (-8 + 41*t2 - 40*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,3)*(6 + 21*t2 - 37*Power(t2,2) + 
                  31*Power(t2,3)) - 
               2*Power(t1,2)*
                (42 - 141*t2 + 110*Power(t2,2) + 90*Power(t2,3) + 
                  4*Power(t2,4)) + 
               2*t1*(2 - 211*t2 + 104*Power(t2,2) - 14*Power(t2,3) + 
                  18*Power(t2,4) + 6*Power(t2,5)) + 
               Power(s2,2)*(73 + 70*t2 - 299*Power(t2,2) + 
                  127*Power(t2,3) - 16*Power(t2,4) - 
                  2*t1*(24 - 53*t2 + 10*Power(t2,2) + 47*Power(t2,3))) \
+ s2*(-21 + 32*t2 + 444*Power(t2,2) - 190*Power(t2,3) + 
                  44*Power(t2,4) + 18*Power(t2,5) + 
                  2*Power(t1,2)*
                   (31 - 110*t2 + 78*Power(t2,2) + 13*Power(t2,3)) - 
                  2*t1*(23 + 56*t2 - 53*Power(t2,2) - 142*Power(t2,3) + 
                     23*Power(t2,4)))) + 
            s1*(11 - 280*t2 + 354*Power(t2,2) - 184*Power(t2,3) - 
               8*Power(t2,4) - 26*Power(t2,5) - 
               4*Power(t1,3)*t2*(10 - 9*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*t2*
                (40 + 20*t2 - 31*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(146 - 274*t2 + 216*Power(t2,2) + 
                  102*Power(t2,3) - 40*Power(t2,4)) + 
               t1*(-124 + 440*t2 - 233*Power(t2,2) + 4*Power(t2,3) + 
                  30*Power(t2,4)) + 
               Power(s2,2)*t2*
                (92 + 91*t2 - 168*Power(t2,2) + 40*Power(t2,3) - 
                  4*Power(t2,4) - 
                  4*t1*(31 - 8*t2 - 16*Power(t2,2) + 4*Power(t2,3))) + 
               s2*(-10 - 195*t2 - 85*Power(t2,2) + 250*Power(t2,3) - 
                  92*Power(t2,4) + 4*Power(t2,5) + 
                  2*Power(t1,2)*
                   (-24 + 99*t2 - 69*Power(t2,2) - 2*Power(t2,3) + 
                     4*Power(t2,4)) - 
                  4*t1*(-14 + 48*t2 - 17*Power(t2,2) + 47*Power(t2,3) - 
                     22*Power(t2,4) + 2*Power(t2,5))))) + 
         Power(s,2)*(12*t1 - 30*Power(t1,2) + 18*Power(t1,3) + 122*t2 - 
            44*s2*t2 - 380*t1*t2 + 118*s2*t1*t2 + 289*Power(t1,2)*t2 - 
            72*s2*Power(t1,2)*t2 - 29*Power(t1,3)*t2 - 221*Power(t2,2) + 
            75*s2*Power(t2,2) - 198*Power(s2,2)*Power(t2,2) + 
            36*Power(s2,3)*Power(t2,2) + 562*t1*Power(t2,2) + 
            19*s2*t1*Power(t2,2) - 23*Power(s2,2)*t1*Power(t2,2) - 
            397*Power(t1,2)*Power(t2,2) + 83*s2*Power(t1,2)*Power(t2,2) + 
            33*Power(t1,3)*Power(t2,2) + 213*Power(t2,3) - 
            304*s2*Power(t2,3) + 126*Power(s2,2)*Power(t2,3) - 
            18*Power(s2,3)*Power(t2,3) - 87*t1*Power(t2,3) + 
            222*s2*t1*Power(t2,3) - 6*Power(s2,2)*t1*Power(t2,3) - 
            49*Power(t1,2)*Power(t2,3) - 26*s2*Power(t1,2)*Power(t2,3) - 
            4*Power(t1,3)*Power(t2,3) - 127*Power(t2,4) + 
            199*s2*Power(t2,4) - 56*Power(s2,2)*Power(t2,4) - 
            6*Power(s2,3)*Power(t2,4) - 49*t1*Power(t2,4) - 
            114*s2*t1*Power(t2,4) + 24*Power(s2,2)*t1*Power(t2,4) + 
            72*Power(t1,2)*Power(t2,4) - 12*s2*Power(t1,2)*Power(t2,4) + 
            29*Power(t2,5) - 14*s2*Power(t2,5) + 
            6*Power(s2,2)*Power(t2,5) - 4*t1*Power(t2,5) + 
            12*s2*t1*Power(t2,5) + 
            Power(s1,6)*(6 - 15*t2 + 10*Power(t2,2) - Power(t2,3) + 
               4*Power(s2,2)*(-3 + 5*t2) + 6*Power(t1,2)*(-5 + 7*t2) + 
               t1*(-36 + 62*t2 - 22*Power(t2,2)) + 
               s2*(7 + t1*(40 - 60*t2) - 4*t2 - 7*Power(t2,2))) + 
            Power(s1,5)*(-23 + 61*t2 - 17*Power(t2,2) - 23*Power(t2,3) + 
               5*Power(s2,3)*(3 + 2*t2) + 3*Power(t1,3)*(-29 + 7*t2) - 
               Power(t1,2)*(14 + 15*t2 + 66*Power(t2,2)) + 
               t1*(90 - 138*t2 - 38*Power(t2,2) + 59*Power(t2,3)) - 
               Power(s2,2)*(-2 + 11*t2 + 67*Power(t2,2) + 
                  t1*(82 + 30*t2)) + 
               s2*(-53 + 99*t2 - 31*Power(t2,2) + 16*Power(t2,3) + 
                  3*Power(t1,2)*(50 + t2) + 
                  t1*(6 + 12*t2 + 151*Power(t2,2)))) + 
            Power(s1,4)*(-55 + 162*t2 - 237*Power(t2,2) + 
               120*Power(t2,3) + 17*Power(t2,4) + Power(t2,5) - 
               Power(s2,3)*t2*(49 + 27*t2) + 
               Power(t1,3)*(74 + 155*t2 - 75*Power(t2,2)) + 
               Power(t1,2)*(-4 + 151*t2 + 189*Power(t2,2) + 
                  65*Power(t2,3)) + 
               t1*(-112 + 142*t2 + 292*Power(t2,2) - 165*Power(t2,3) - 
                  47*Power(t2,4)) + 
               Power(s2,2)*(-129 + 404*t2 - 30*Power(t2,2) + 
                  73*Power(t2,3) + t1*(50 + 146*t2 + 107*Power(t2,2))) + 
               s2*(202 - 453*t2 + 172*Power(t2,2) - 30*Power(t2,3) - 
                  19*Power(t2,4) + 
                  Power(t1,2)*(-118 - 270*t2 + 5*Power(t2,2)) - 
                  t1*(-122 + 398*t2 + 357*Power(t2,2) + 74*Power(t2,3)))) \
+ Power(s1,3)*(-38 + 181*t2 - 147*Power(t2,2) + 80*Power(t2,3) - 
               102*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,3)*(-6 - 98*t2 + 20*Power(t2,3)) + 
               Power(s2,3)*(-13 + 28*t2 + 26*Power(t2,2) + 
                  25*Power(t2,3)) - 
               2*Power(t1,2)*
                (97 - 116*t2 + 231*Power(t2,2) + 101*Power(t2,3) + 
                  6*Power(t2,4)) + 
               2*t1*(78 - 207*t2 - 105*Power(t2,2) + 40*Power(t2,3) + 
                  59*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s2,2)*(-44 + t2 - 457*Power(t2,2) + 
                  74*Power(t2,3) - 30*Power(t2,4) - 
                  2*t1*(-1 + 19*t2 + 47*Power(t2,2) + 51*Power(t2,3))) + 
               s2*(-63 - 20*t2 + 568*Power(t2,2) - 265*Power(t2,3) + 
                  96*Power(t2,4) + 10*Power(t2,5) + 
                  2*Power(t1,2)*
                   (13 + 44*t2 + 55*Power(t2,2) + 17*Power(t2,3)) + 
                  t1*(198 - 88*t2 + 698*Power(t2,2) + 272*Power(t2,3) - 
                     30*Power(t2,4)))) - 
            s1*(57 - 56*t2 + 30*Power(t2,2) + 11*Power(t2,3) - 
               2*Power(t2,4) + 10*Power(t2,5) + 
               Power(s2,3)*t2*
                (48 + 4*t2 - 21*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(-5 + 19*t2 - 8*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(160 - 223*t2 + 16*Power(t2,2) - 
                  74*Power(t2,3) - 36*Power(t2,4)) + 
               t1*(-210 + 304*t2 + 168*Power(t2,2) - 101*Power(t2,3) + 
                  38*Power(t2,4) + 16*Power(t2,5)) - 
               2*Power(s2,2)*t2*
                (88 - 41*t2 + 84*Power(t2,2) - 19*Power(t2,3) + 
                  2*Power(t2,4) + 
                  t1*(34 - 5*t2 - 13*Power(t2,2) + 8*Power(t2,3))) + 
               s2*(-18 - 171*t2 - 113*Power(t2,2) + 93*Power(t2,3) - 
                  140*Power(t2,4) + 26*Power(t2,5) + 
                  Power(t1,2)*
                   (-32 + 27*t2 - 2*Power(t2,2) - 6*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  t1*(52 + 236*t2 - 79*Power(t2,2) + 224*Power(t2,3) + 
                     26*Power(t2,4) - 8*Power(t2,5)))) + 
            Power(s1,2)*(7 - 39*t2 - 30*Power(t2,2) + 70*Power(t2,3) + 
               18*Power(t2,4) + 30*Power(t2,5) - 
               2*Power(t1,3)*
                (2 + 15*t2 - 17*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,3)*(14 + 27*t2 - 31*Power(t2,2) + 
                  4*Power(t2,3) - 6*Power(t2,4)) + 
               2*Power(t1,2)*(-24 + 179*t2 - 104*Power(t2,2) + 
                  56*Power(t2,3) + 32*Power(t2,4)) - 
               4*t1*(-24 + 6*t2 - 72*Power(t2,2) - 19*Power(t2,3) + 
                  20*Power(t2,4) + 5*Power(t2,5)) + 
               Power(s2,2)*(-9 + 98*t2 - 94*Power(t2,2) + 
                  135*Power(t2,3) - 36*Power(t2,4) + 6*Power(t2,5) + 
                  2*t1*(-17 - 9*t2 + 10*Power(t2,2) + 3*Power(t2,3) + 
                     12*Power(t2,4))) - 
               s2*(159 - 91*t2 + 58*Power(t2,2) + 380*Power(t2,3) - 
                  80*Power(t2,4) + 34*Power(t2,5) + 
                  2*Power(t1,2)*
                   (-3 - 11*t2 + 4*Power(t2,2) + 7*Power(t2,3) + 
                     6*Power(t2,4)) - 
                  2*t1*(51 - 234*t2 + 121*Power(t2,2) - 66*Power(t2,3) - 
                     27*Power(t2,4) + 6*Power(t2,5))))))*R2q(s))/
     (s*(-1 + s1)*Power(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2),
        2)*(-1 + s2)*(-1 + t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) - 
    (8*(Power(s,12)*Power(t2,2)*(t1 + t2) + 
         Power(-1 + s1,6)*(s1 - t2)*(-1 + t2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         Power(s,11)*t2*(s1*(t1*(2 - 9*t2) + (2 + s2 - 8*t2)*t2) + 
            t2*(1 + Power(t1,2) - 7*t2 + 2*s2*t2 + 8*Power(t2,2) - 
               t1*(11 + s2 + t2))) + 
         Power(s,10)*(-(Power(t1,3)*(-1 + t2)) + 
            Power(t1,2)*(s2 - 7*t2)*t2 + 
            t1*Power(t2,2)*(51 + 2*t2 + Power(t2,2) + s2*(8 + 3*t2)) - 
            s1*t2*(-2 + (16 + 2*s2 + Power(s2,2))*t2 + 
               (-59 + 10*s2)*Power(t2,2) + 56*Power(t2,3) + 
               Power(t1,2)*(-1 + 9*t2) + 
               t1*(20 + s2*(2 - 8*t2) - 72*t2 - 8*Power(t2,2))) + 
            Power(t2,2)*(-10 + 21*t2 - 3*Power(s2,2)*t2 - 
               62*Power(t2,2) + 11*Power(t2,3) - 
               s2*(1 + 19*t2 + 4*Power(t2,2))) + 
            Power(s1,2)*(t2*(1 + s2*(2 - 8*t2) - 14*t2 + 
                  28*Power(t2,2)) + t1*(1 - 16*t2 + 36*Power(t2,2)))) + 
         Power(s,9)*(-(Power(t1,3)*
               (7 - 11*t2 + Power(t2,2) + Power(t2,3))) + 
            Power(t1,2)*(3 + (8 - 10*s2)*t2 + 19*Power(t2,2) + 
               2*(3 + s2)*Power(t2,3) + 3*Power(t2,4)) + 
            Power(s1,3)*(-7*t1*(1 - 8*t2 + 12*Power(t2,2)) - 
               2*t2*(3 - 21*t2 + 28*Power(t2,2)) + 
               s2*(1 - 14*t2 + 28*Power(t2,2))) - 
            t1*t2*(-3 + 128*t2 - 35*Power(t2,2) + 26*Power(t2,3) + 
               Power(s2,2)*t2*(1 + t2) + s2*(1 + 16*t2 + 36*Power(t2,2))\
) + Power(t2,2)*(43 - 61*t2 + 182*Power(t2,2) - 75*Power(t2,3) + 
               6*Power(t2,4) + 
               Power(s2,2)*(-2 + 28*t2 - 3*Power(t2,2)) + 
               s2*(6 + 73*t2 + 42*Power(t2,2) - 7*Power(t2,3))) + 
            Power(s1,2)*(1 - (11 + 8*s2 + 2*Power(s2,2))*t2 + 
               (71 + 22*s2 + 6*Power(s2,2))*Power(t2,2) + 
               2*(-102 + 7*s2)*Power(t2,3) + 168*Power(t2,4) + 
               6*Power(t1,2)*t2*(-1 + 6*t2) - 
               t1*(9 + s2 - 115*t2 - 12*s2*t2 + 198*Power(t2,2) + 
                  28*s2*Power(t2,2) + 28*Power(t2,3))) + 
            s1*(2*Power(t1,3)*(-6 + 5*t2) - 
               Power(t1,2)*(3 + 5*t2 - 34*Power(t2,2) + Power(t2,3) + 
                  2*s2*(-2 + 5*t2)) + 
               t1*t2*(77 - 229*t2 + 13*Power(t2,2) - 6*Power(t2,3) + 
                  2*Power(s2,2)*(1 + t2) - 
                  3*s2*(-5 + 11*t2 + 6*Power(t2,2))) - 
               t2*(18 - 60*t2 + 199*Power(t2,2) - 329*Power(t2,3) + 
                  66*Power(t2,4) + Power(s2,2)*(t2 - 12*Power(t2,2)) + 
                  s2*(2 + 18*t2 - 50*Power(t2,2) - 25*Power(t2,3))))) + 
         Power(s,8)*(Power(t1,3)*
             (20 - 45*t2 + 10*Power(t2,2) + 8*Power(t2,3)) + 
            Power(t1,2)*(-18 + (-54 + 44*s2)*t2 + 
               (1 - 6*s2)*Power(t2,2) - 4*(8 + 5*s2)*Power(t2,3) - 
               (20 + s2)*Power(t2,4) + Power(t2,5)) + 
            t1*(3 + t2 - 3*s2*t2 + 
               (201 - 44*s2 + 8*Power(s2,2))*Power(t2,2) + 
               2*(-122 + 95*s2 + 4*Power(s2,2))*Power(t2,3) + 
               2*(85 - 3*s2 + Power(s2,2))*Power(t2,4) + 
               3*(-3 + s2)*Power(t2,5)) + 
            Power(s1,4)*(s2*(-6 + 42*t2 - 56*Power(t2,2)) + 
               5*t2*(3 - 14*t2 + 14*Power(t2,2)) + 
               7*t1*(3 - 16*t2 + 18*Power(t2,2))) + 
            t2*(3 - 78*t2 + 219*Power(t2,2) - 240*Power(t2,3) + 
               175*Power(t2,4) - 39*Power(t2,5) + Power(t2,6) - 
               Power(s2,3)*Power(t2,2)*(2 + t2) + 
               Power(s2,2)*t2*
                (18 - 119*t2 + 15*Power(t2,2) - 4*Power(t2,3)) - 
               s2*(2 + 37*t2 + 116*Power(t2,2) + 164*Power(t2,3) - 
                  58*Power(t2,4) + 5*Power(t2,5))) + 
            Power(s1,3)*(-2 + 16*t2 - 143*Power(t2,2) + 
               377*Power(t2,3) - 280*Power(t2,4) + 
               Power(t1,2)*(2 + 12*t2 - 84*Power(t2,2)) + 
               Power(s2,2)*(-1 + 9*t2 - 14*Power(t2,2)) + 
               t1*(44 - 278*t2 + 294*Power(t2,2) + 56*Power(t2,3)) + 
               2*s2*(-2 + 25*t2 - 44*Power(t2,2) + 7*Power(t2,3) + 
                  2*t1*(1 - 6*t2 + 14*Power(t2,2)))) + 
            Power(s1,2)*(-8 + Power(t1,3)*(62 - 45*t2) + 53*t2 - 
               146*Power(t2,2) + 547*Power(t2,3) - 701*Power(t2,4) + 
               165*Power(t2,5) + Power(s2,3)*t2*(1 + 2*t2) + 
               Power(t1,2)*(28 - 5*t2 - 37*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(30 - 251*t2 + 445*Power(t2,2) - 137*Power(t2,3) + 
                  14*Power(t2,4)) - 
               Power(s2,2)*(2*t2*(-3 + 2*t2 + 6*Power(t2,2)) + 
                  t1*(-5 + 11*t2 + 14*Power(t2,2))) + 
               s2*(-1 - 2*t2 - 23*Power(t2,2) + 46*Power(t2,3) - 
                  66*Power(t2,4) + Power(t1,2)*(-39 + 44*t2) + 
                  t1*(1 - 56*t2 + 28*Power(t2,2) + 41*Power(t2,3)))) + 
            s1*(Power(t1,3)*(62 - 86*t2 + 6*Power(t2,2) + 
                  8*Power(t2,3)) - 
               2*Power(t1,2)*
                (2 + 39*t2 - 5*Power(t2,2) + 18*Power(t2,3) + 
                  11*Power(t2,4) + 
                  s2*(12 - 41*t2 - 3*Power(t2,2) + 7*Power(t2,3))) + 
               t1*(-3 - 193*t2 + 375*Power(t2,2) - 355*Power(t2,3) + 
                  166*Power(t2,4) + 
                  Power(s2,2)*t2*(-21 - 7*t2 + 10*Power(t2,2)) + 
                  s2*(5 + 22*t2 - 88*Power(t2,2) + 163*Power(t2,3) + 
                     4*Power(t2,4))) + 
               t2*(61 - 226*t2 + 3*Power(s2,3)*(-1 + t2)*t2 + 
                  464*Power(t2,2) - 638*Power(t2,3) + 302*Power(t2,4) - 
                  29*Power(t2,5) + 
                  Power(s2,2)*
                   (-5 + 67*t2 - 79*Power(t2,2) + 15*Power(t2,3)) + 
                  s2*(17 + 144*t2 - 7*Power(t2,2) - 228*Power(t2,3) + 
                     34*Power(t2,4))))) + 
         Power(s,7)*(1 - 15*t1 + 42*Power(t1,2) - 28*Power(t1,3) - 
            5*t2 + 4*s2*t2 - 80*t1*t2 + 43*s2*t1*t2 + 
            175*Power(t1,2)*t2 - 112*s2*Power(t1,2)*t2 + 
            90*Power(t1,3)*t2 - 7*Power(t2,2) + 133*s2*Power(t2,2) - 
            59*Power(s2,2)*Power(t2,2) - 184*t1*Power(t2,2) + 
            251*s2*t1*Power(t2,2) - 28*Power(s2,2)*t1*Power(t2,2) - 
            146*Power(t1,2)*Power(t2,2) + 
            45*s2*Power(t1,2)*Power(t2,2) - 35*Power(t1,3)*Power(t2,2) - 
            481*Power(t2,3) - 146*s2*Power(t2,3) + 
            292*Power(s2,2)*Power(t2,3) + 16*Power(s2,3)*Power(t2,3) + 
            799*t1*Power(t2,3) - 554*s2*t1*Power(t2,3) - 
            32*Power(s2,2)*t1*Power(t2,3) + 63*Power(t1,2)*Power(t2,3) + 
            74*s2*Power(t1,2)*Power(t2,3) - 24*Power(t1,3)*Power(t2,3) + 
            111*Power(t2,4) + 294*s2*Power(t2,4) - 
            34*Power(s2,2)*Power(t2,4) + 6*Power(s2,3)*Power(t2,4) - 
            538*t1*Power(t2,4) + 68*s2*t1*Power(t2,4) - 
            16*Power(s2,2)*t1*Power(t2,4) + 59*Power(t1,2)*Power(t2,4) + 
            4*s2*Power(t1,2)*Power(t2,4) - 122*Power(t2,5) - 
            181*s2*Power(t2,5) + 17*Power(s2,2)*Power(t2,5) + 
            74*t1*Power(t2,5) - 23*s2*t1*Power(t2,5) - 
            6*Power(t1,2)*Power(t2,5) + 83*Power(t2,6) + 
            33*s2*Power(t2,6) - Power(s2,2)*Power(t2,6) + 
            s2*t1*Power(t2,6) - 8*Power(t2,7) - s2*Power(t2,7) + 
            Power(s1,5)*(5*s2*(3 - 14*t2 + 14*Power(t2,2)) - 
               7*t1*(5 - 20*t2 + 18*Power(t2,2)) - 
               2*t2*(10 - 35*t2 + 28*Power(t2,2))) + 
            Power(s1,4)*(-5 + 13*t2 + 140*Power(t2,2) - 
               400*Power(t2,3) + 280*Power(t2,4) + 
               14*Power(t1,2)*(-1 + 9*Power(t2,2)) + 
               Power(s2,2)*(3 - 10*t2 + 14*Power(t2,2)) + 
               s2*(20 + t1 - 130*t2 + 178*Power(t2,2) - 
                  70*t1*Power(t2,2) - 70*Power(t2,3)) - 
               t1*(91 - 373*t2 + 252*Power(t2,2) + 70*Power(t2,3))) + 
            Power(s1,3)*(14 - 22*t2 + 72*Power(t2,2) - 
               567*Power(t2,3) + 739*Power(t2,4) - 220*Power(t2,5) + 
               3*Power(t1,3)*(-61 + 40*t2) - 
               2*Power(s2,3)*(-1 + t2 + 6*Power(t2,2)) - 
               Power(t1,2)*(125 - 135*t2 + 105*Power(t2,2) + 
                  28*Power(t2,3)) + 
               t1*(-67 + 265*t2 - 618*Power(t2,2) + 438*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s2,2)*(1 - 35*t2 + 18*Power(t2,2) - 
                  12*Power(t2,3) + t1*(-38 + 22*t2 + 42*Power(t2,2))) + 
               s2*(1 + Power(t1,2)*(164 - 115*t2) - 46*t2 + 
                  336*Power(t2,2) - 320*Power(t2,3) + 95*Power(t2,4) + 
                  t1*(25 + 60*t2 + 83*Power(t2,2) - 35*Power(t2,3)))) - 
            Power(s1,2)*(-22 + 175*t2 - 560*Power(t2,2) + 
               656*Power(t2,3) - 614*Power(t2,4) + 436*Power(t2,5) - 
               55*Power(t2,6) + 
               Power(s2,3)*t2*(15 - 16*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(210 - 286*t2 + 15*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,2)*(55 - 511*t2 + 336*Power(t2,2) - 
                  99*Power(t2,3) - 70*Power(t2,4)) + 
               t1*(43 - 365*t2 + 655*Power(t2,2) - 1229*Power(t2,3) + 
                  480*Power(t2,4)) + 
               Power(s2,2)*(t2*
                   (-96 + 95*t2 - 47*Power(t2,2) + 30*Power(t2,3)) + 
                  t1*(27 - 110*t2 - 47*Power(t2,2) + 38*Power(t2,3))) + 
               s2*(-5 - 68*t2 + 194*Power(t2,2) + 642*Power(t2,3) - 
                  509*Power(t2,4) + 64*Power(t2,5) + 
                  Power(t1,2)*
                   (-152 + 290*t2 + 39*Power(t2,2) - 42*Power(t2,3)) + 
                  t1*(-7 + 416*t2 - 506*Power(t2,2) + 280*Power(t2,3) + 
                     26*Power(t2,4)))) + 
            s1*(Power(s2,3)*Power(t2,2)*(17 - 28*t2 + 7*Power(t2,2)) - 
               Power(t1,3)*(123 - 240*t2 + 46*Power(t2,2) + 
                  44*Power(t2,3)) + 
               Power(t1,2)*(26 + 407*t2 - 423*Power(t2,2) + 
                  106*Power(t2,3) + 97*Power(t2,4) - 6*Power(t2,5)) + 
               t2*(-99 + 571*t2 - 822*Power(t2,2) + 319*Power(t2,3) - 
                  317*Power(t2,4) + 117*Power(t2,5) - 3*Power(t2,6)) + 
               t1*(7 + 239*t2 - 682*Power(t2,2) + 1412*Power(t2,3) - 
                  766*Power(t2,4) + 48*Power(t2,5) - Power(t2,6)) + 
               Power(s2,2)*t2*
                (24 - 410*t2 + 258*Power(t2,2) - 43*Power(t2,3) + 
                  23*Power(t2,4) - 
                  t1*(-80 + 7*t2 + 46*Power(t2,2) + 5*Power(t2,3))) + 
               s2*(1 - 40*t2 - 102*Power(t2,2) - 370*Power(t2,3) + 
                  741*Power(t2,4) - 212*Power(t2,5) + 21*Power(t2,6) + 
                  Power(t1,2)*
                   (62 - 259*t2 + 6*Power(t2,2) + 100*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  t1*(-28 - 249*t2 + 893*Power(t2,2) - 599*Power(t2,3) + 
                     Power(t2,4) - 11*Power(t2,5))))) + 
         s*Power(-1 + s1,4)*(-1 + 3*t1 - 3*Power(t1,2) + Power(t1,3) + 
            2*Power(s1,6)*Power(s2 - t1,2)*(-1 + t2) + 15*t2 - 2*s2*t2 - 
            41*t1*t2 + 4*s2*t1*t2 + 37*Power(t1,2)*t2 - 
            2*s2*Power(t1,2)*t2 - 11*Power(t1,3)*t2 - 39*Power(t2,2) + 
            18*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) + 
            90*t1*Power(t2,2) - 45*s2*t1*Power(t2,2) - 
            Power(s2,2)*t1*Power(t2,2) - 61*Power(t1,2)*Power(t2,2) + 
            27*s2*Power(t1,2)*Power(t2,2) + 10*Power(t1,3)*Power(t2,2) + 
            37*Power(t2,3) - 34*s2*Power(t2,3) + 
            2*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) - 
            62*t1*Power(t2,3) + 80*s2*t1*Power(t2,3) - 
            17*Power(s2,2)*t1*Power(t2,3) + 17*Power(t1,2)*Power(t2,3) - 
            28*s2*Power(t1,2)*Power(t2,3) + 3*Power(t1,3)*Power(t2,3) - 
            12*Power(t2,4) + 22*s2*Power(t2,4) - 
            13*Power(s2,2)*Power(t2,4) + Power(s2,3)*Power(t2,4) + 
            3*t1*Power(t2,4) - 29*s2*t1*Power(t2,4) + 
            24*Power(s2,2)*t1*Power(t2,4) + 10*Power(t1,2)*Power(t2,4) - 
            6*s2*Power(t1,2)*Power(t2,4) - 4*s2*Power(t2,5) + 
            10*Power(s2,2)*Power(t2,5) - 6*Power(s2,3)*Power(t2,5) + 
            7*t1*Power(t2,5) - 10*s2*t1*Power(t2,5) + 
            3*Power(s2,2)*t1*Power(t2,5) - 
            Power(s1,5)*(3*Power(s2,3)*(-2 + t2) + 
               Power(s2,2)*(-4 + t1*(25 - 16*t2) + 3*t2 + 
                  Power(t2,2)) - 
               t1*(Power(-1 + t2,2)*t2 + Power(t1,2)*(-13 + 10*t2) - 
                  t1*(2 - 8*t2 + 5*Power(t2,2) + Power(t2,3))) + 
               s2*(Power(-1 + t2,2)*t2 + Power(t1,2)*(-32 + 23*t2) - 
                  t1*(-2 - 5*t2 + 6*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,4)*(-(Power(-1 + t2,3)*(2 + t2)) + 
               t1*Power(-1 + t2,2)*(-7 + 4*t2) + 
               Power(s2,3)*(6 - 34*t2 + 13*Power(t2,2)) - 
               Power(t1,3)*(-29 + 7*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-22 + 45*t2 - 29*Power(t2,2) + 
                  5*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(-35 + 54*t2 - 9*Power(t2,2) - 
                  10*Power(t2,3) + 
                  t1*(13 + 71*t2 - 40*Power(t2,2) + Power(t2,3))) + 
               s2*(Power(-1 + t2,2)*(-3 + 4*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*(-48 - 30*t2 + 33*Power(t2,2)) + 
                  t1*(65 - 122*t2 + 59*Power(t2,2) - 2*Power(t2,4)))) - 
            Power(s1,3)*(Power(t1,3)*(10 + 28*t2 - 8*Power(t2,2)) - 
               Power(-1 + t2,3)*(1 + t2 + Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (11 - 29*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(2 + 19*t2 - 72*Power(t2,2) + 
                  21*Power(t2,3)) - 
               Power(t1,2)*(27 - 32*t2 - 2*Power(t2,2) + 
                  6*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(-2 - 101*t2 + 179*Power(t2,2) - 
                  61*Power(t2,3) - 15*Power(t2,4) + 
                  t1*(-13 + 46*t2 + 83*Power(t2,2) - 28*Power(t2,3) + 
                     2*Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (-28 + 30*t2 + 8*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-2 - 86*t2 - 6*Power(t2,2) + 4*Power(t2,3)) + 
                  t1*(48 + 29*t2 - 174*Power(t2,2) + 96*Power(t2,3) + 
                     2*Power(t2,4) - Power(t2,5)))) + 
            Power(s1,2)*(Power(-1 + t2,3)*(-1 - 9*t2 + 2*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*
                (14 - t2 + 11*Power(t2,2) + Power(t2,3)) + 
               2*Power(t1,3)*
                (-7 + 13*t2 + 6*Power(t2,2) + 3*Power(t2,3)) + 
               3*Power(s2,3)*t2*
                (2 + 7*t2 - 24*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(27 - 76*t2 + 50*Power(t2,2) - 
                  6*Power(t2,3) + 5*Power(t2,4)) + 
               s2*(Power(-1 + t2,2)*
                   (3 - 44*t2 + 62*Power(t2,2) + 4*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (-6 - 4*t2 + 42*Power(t2,2) + 10*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  t1*(-15 + 96*t2 - 66*Power(t2,2) - 44*Power(t2,3) + 
                     29*Power(t2,4))) + 
               Power(s2,2)*(1 - 2*t2 - 110*Power(t2,2) + 
                  190*Power(t2,3) - 73*Power(t2,4) - 6*Power(t2,5) + 
                  t1*(-1 - 43*t2 + 77*Power(t2,2) + 56*Power(t2,3) + 
                     Power(t2,5)))) + 
            s1*(Power(-1 + t2,3)*(11 + t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(7 + 10*t2 - 24*Power(t2,2) - 
                  8*Power(t2,3)) - 
               Power(s2,3)*Power(t2,2)*
                (6 + 9*t2 - 34*Power(t2,2) + 4*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*(-29 + 5*Power(t2,3)) + 
               Power(t1,2)*(-25 + 16*t2 + 47*Power(t2,2) - 
                  21*Power(t2,3) - 17*Power(t2,4)) - 
               Power(s2,2)*t2*
                (2 + 2*t2 - 57*Power(t2,2) + 79*Power(t2,3) - 
                  26*Power(t2,4) + 
                  t1*(-2 - 47*t2 + 68*Power(t2,2) + 22*Power(t2,3) + 
                     4*Power(t2,4))) + 
               s2*(-(Power(-1 + t2,2)*
                     (-2 + 17*t2 - 20*Power(t2,2) + 29*Power(t2,3))) + 
                  Power(t1,2)*
                   (2 - 39*t2 + 18*Power(t2,2) + 52*Power(t2,3) + 
                     12*Power(t2,4)) + 
                  t1*(-4 + 60*t2 - 128*Power(t2,2) + 59*Power(t2,3) + 
                     4*Power(t2,4) + 9*Power(t2,5))))) + 
         Power(s,6)*(-4 + 27*t1 - 42*Power(t1,2) + 14*Power(t1,3) - 
            30*t2 + 12*s2*t2 + 306*t1*t2 - 139*s2*t1*t2 - 
            367*Power(t1,2)*t2 + 182*s2*Power(t1,2)*t2 - 
            84*Power(t1,3)*t2 + 293*Power(t2,2) - 253*s2*Power(t2,2) + 
            94*Power(s2,2)*Power(t2,2) - 54*t1*Power(t2,2) - 
            454*s2*t1*Power(t2,2) + 56*Power(s2,2)*t1*Power(t2,2) + 
            445*Power(t1,2)*Power(t2,2) - 
            147*s2*Power(t1,2)*Power(t2,2) + 
            55*Power(t1,3)*Power(t2,2) + 380*Power(t2,3) + 
            940*s2*Power(t2,3) - 396*Power(s2,2)*Power(t2,3) - 
            52*Power(s2,3)*Power(t2,3) - 1521*t1*Power(t2,3) + 
            914*s2*t1*Power(t2,3) + 77*Power(s2,2)*t1*Power(t2,3) - 
            35*Power(t1,2)*Power(t2,3) - 
            127*s2*Power(t1,2)*Power(t2,3) + 
            32*Power(t1,3)*Power(t2,3) + 21*Power(t2,4) - 
            341*s2*Power(t2,4) + 75*Power(s2,2)*Power(t2,4) - 
            11*Power(s2,3)*Power(t2,4) + 1038*t1*Power(t2,4) - 
            285*s2*t1*Power(t2,4) + 41*Power(s2,2)*t1*Power(t2,4) - 
            102*Power(t1,2)*Power(t2,4) + s2*Power(t1,2)*Power(t2,4) - 
            137*Power(t2,5) + 205*s2*Power(t2,5) - 
            14*Power(s2,2)*Power(t2,5) + Power(s2,3)*Power(t2,5) - 
            234*t1*Power(t2,5) + 74*s2*t1*Power(t2,5) - 
            3*Power(s2,2)*t1*Power(t2,5) + 15*Power(t1,2)*Power(t2,5) - 
            62*Power(t2,6) - 80*s2*Power(t2,6) + 
            5*Power(s2,2)*Power(t2,6) + 10*t1*Power(t2,6) - 
            9*s2*t1*Power(t2,6) + 19*Power(t2,7) + 7*s2*Power(t2,7) + 
            Power(s1,6)*(s2*(-20 + 70*t2 - 56*Power(t2,2)) + 
               7*t1*(5 - 16*t2 + 12*Power(t2,2)) + 
               t2*(15 - 42*t2 + 28*Power(t2,2))) + 
            Power(s1,5)*(20 + Power(s2,2)*(2 - 17*t2) - 62*t2 - 
               46*Power(t2,2) + 237*Power(t2,3) - 168*Power(t2,4) - 
               42*Power(t1,2)*(-1 + t2 + 3*Power(t2,2)) + 
               2*t1*(55 - 160*t2 + 63*Power(t2,2) + 28*Power(t2,3)) + 
               2*s2*(-21 + 90*t2 - 100*Power(t2,2) + 49*Power(t2,3) + 
                  t1*(-17 + 42*t2 + 28*Power(t2,2)))) + 
            Power(s1,4)*(-7*Power(t1,3)*(-49 + 30*t2) + 
               Power(s2,3)*(-11 - 7*t2 + 30*Power(t2,2)) + 
               t1*(19 - 92*t2 + 862*Power(t2,2) - 731*Power(t2,3)) + 
               Power(t1,2)*(304 - 425*t2 + 385*Power(t2,2) + 
                  56*Power(t2,3)) + 
               t2*(-94 + 271*t2 + 39*Power(t2,2) - 355*Power(t2,3) + 
                  165*Power(t2,4)) + 
               Power(s2,2)*(t1*(125 - 16*t2 - 70*Power(t2,2)) + 
                  6*t2*(13 + t2 + 5*Power(t2,2))) + 
               s2*(-9 + 185*t2 - 741*Power(t2,2) + 491*Power(t2,3) - 
                  80*Power(t2,4) + 7*Power(t1,2)*(-56 + 29*t2) - 
                  t1*(120 - 4*t2 + 246*Power(t2,2) + 21*Power(t2,3)))) + 
            Power(s1,3)*(-37 + 290*t2 - 562*Power(t2,2) - 
               199*Power(t2,3) + 323*Power(t2,4) + 223*Power(t2,5) - 
               50*Power(t2,6) - 
               2*Power(s2,3)*
                (6 - 35*t2 + 9*Power(t2,2) + 5*Power(t2,3)) + 
               7*Power(t1,3)*
                (47 - 74*t2 + 3*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(251 - 1472*t2 + 859*Power(t2,2) - 
                  174*Power(t2,3) - 126*Power(t2,4)) + 
               t1*(24 - 475*t2 + 1110*Power(t2,2) - 2113*Power(t2,3) + 
                  820*Power(t2,4)) + 
               2*Power(s2,2)*
                (19 - 112*t2 - 65*Power(t2,2) + 8*Power(t2,3) + 
                  15*Power(t2,4) + 
                  t1*(59 - 147*t2 - 50*Power(t2,2) + 37*Power(t2,3))) + 
               s2*(22 - 120*t2 - 19*Power(t2,2) + 1349*Power(t2,3) - 
                  623*Power(t2,4) + 55*Power(t2,5) + 
                  Power(t1,2)*
                   (-340 + 598*t2 + 102*Power(t2,2) - 70*Power(t2,3)) + 
                  t1*(-207 + 1475*t2 - 807*Power(t2,2) + 
                     219*Power(t2,3) + 72*Power(t2,4)))) - 
            Power(s1,2)*(24 - 408*t2 + 1093*Power(t2,2) - 
               404*Power(t2,3) - 604*Power(t2,4) + 163*Power(t2,5) + 
               113*Power(t2,6) - 2*Power(t2,7) + 
               2*Power(s2,3)*t2*
                (-27 + 78*t2 - 44*Power(t2,2) + 4*Power(t2,3)) - 
               3*Power(t1,3)*
                (77 - 150*t2 + 31*Power(t2,2) + 32*Power(t2,3)) + 
               Power(t1,2)*(-156 + 1452*t2 - 1428*Power(t2,2) + 
                  79*Power(t2,3) + 182*Power(t2,4) - 15*Power(t2,5)) + 
               t1*(-47 + 386*t2 - 2075*Power(t2,2) + 
                  2810*Power(t2,3) - 1372*Power(t2,4) + 
                  120*Power(t2,5) - 5*Power(t2,6)) + 
               Power(s2,2)*(1 + 485*t2 - 801*Power(t2,2) + 
                  90*Power(t2,3) - 60*Power(t2,4) + 47*Power(t2,5) + 
                  t1*(-61 + 269*t2 - 70*Power(t2,2) - 
                     103*Power(t2,3) + 3*Power(t2,4))) + 
               s2*(5 + 2*t2 + 468*Power(t2,2) - 1142*Power(t2,3) + 
                  1239*Power(t2,4) - 298*Power(t2,5) + 32*Power(t2,6) + 
                  Power(t1,2)*
                   (224 - 553*t2 + 15*Power(t2,2) + 211*Power(t2,3) + 
                     15*Power(t2,4)) - 
                  t1*(-86 + 1630*t2 - 2361*Power(t2,2) + 
                     609*Power(t2,3) + 65*Power(t2,4) + 9*Power(t2,5)))) \
+ s1*(2 - 42*t2 - 673*Power(t2,2) + 607*Power(t2,3) + 580*Power(t2,4) - 
               219*Power(t2,5) - 69*Power(t2,6) + 17*Power(t2,7) - 
               2*Power(s2,3)*Power(t2,2)*(11 - 56*t2 + 24*Power(t2,2)) + 
               Power(t1,3)*(107 - 274*t2 + 87*Power(t2,2) + 
                  72*Power(t2,3)) + 
               Power(t1,2)*(33 - 898*t2 + 1153*Power(t2,2) - 
                  88*Power(t2,3) - 190*Power(t2,4) + 18*Power(t2,5)) + 
               t1*(-52 + 27*t2 + 1491*Power(t2,2) - 2937*Power(t2,3) + 
                  1440*Power(t2,4) - 218*Power(t2,5) + 3*Power(t2,6)) + 
               Power(s2,2)*t2*
                (-43 + 1013*t2 - 750*Power(t2,2) + 82*Power(t2,3) - 
                  74*Power(t2,4) + 5*Power(t2,5) + 
                  t1*(-157 + 28*t2 + 66*Power(t2,2) + 26*Power(t2,3) + 
                     3*Power(t2,4))) - 
               s2*(7 - 68*t2 + 965*Power(t2,2) - 1568*Power(t2,3) + 
                  1022*Power(t2,4) - 491*Power(t2,5) + 92*Power(t2,6) - 
                  3*Power(t2,7) + 
                  2*Power(t1,2)*
                   (46 - 208*t2 + 42*Power(t2,2) + 100*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  t1*(-69 - 606*t2 + 2130*Power(t2,2) - 
                     1199*Power(t2,3) + 114*Power(t2,4) - 
                     57*Power(t2,5) + Power(t2,6))))) + 
         Power(s,2)*Power(-1 + s1,2)*
          (4 - 15*t1 + 18*Power(t1,2) - 7*Power(t1,3) - 70*t2 + 
            16*s2*t2 + 190*t1*t2 - 33*s2*t1*t2 - 165*Power(t1,2)*t2 + 
            17*s2*Power(t1,2)*t2 + 45*Power(t1,3)*t2 + 167*Power(t2,2) - 
            48*s2*Power(t2,2) - 6*Power(s2,2)*Power(t2,2) - 
            357*t1*Power(t2,2) + 134*s2*t1*Power(t2,2) + 
            8*Power(s2,2)*t1*Power(t2,2) + 246*Power(t1,2)*Power(t2,2) - 
            105*s2*Power(t1,2)*Power(t2,2) - 
            35*Power(t1,3)*Power(t2,2) - 151*Power(t2,3) + 
            77*s2*Power(t2,3) + 59*Power(s2,2)*Power(t2,3) - 
            16*Power(s2,3)*Power(t2,3) + 131*t1*Power(t2,3) - 
            217*s2*t1*Power(t2,3) + 47*Power(s2,2)*t1*Power(t2,3) - 
            21*Power(t1,2)*Power(t2,3) + 91*s2*Power(t1,2)*Power(t2,3) - 
            16*Power(t1,3)*Power(t2,3) + 37*Power(t2,4) - 
            29*s2*Power(t2,4) - 15*Power(s2,2)*Power(t2,4) + 
            9*Power(s2,3)*Power(t2,4) + 109*t1*Power(t2,4) - 
            5*s2*t1*Power(t2,4) - 69*Power(s2,2)*t1*Power(t2,4) - 
            46*Power(t1,2)*Power(t2,4) + 31*s2*Power(t1,2)*Power(t2,4) + 
            30*Power(t2,5) - 31*s2*Power(t2,5) - 
            6*Power(s2,2)*Power(t2,5) + 15*Power(s2,3)*Power(t2,5) - 
            72*t1*Power(t2,5) + 60*s2*t1*Power(t2,5) - 
            15*Power(s2,2)*t1*Power(t2,5) + Power(t1,2)*Power(t2,5) - 
            10*Power(t2,6) + 14*s2*Power(t2,6) + 
            Power(s2,2)*Power(t2,6) + 14*t1*Power(t2,6) - 
            5*s2*t1*Power(t2,6) - 7*Power(t2,7) + s2*Power(t2,7) + 
            Power(s1,7)*(Power(s2,2)*(12 - 15*t2 + Power(t2,2)) + 
               2*s2*(-1 + Power(t2,2) + t1*(-13 + 15*t2)) + 
               t1*(2*(3 - 4*t2 + Power(t2,2)) - 
                  t1*(-14 + 15*t2 + Power(t2,2)))) + 
            Power(s1,6)*(Power(t1,3)*(68 - 45*t2) + 
               Power(-1 + t2,2)*(-4 - 2*t2 + Power(t2,2)) + 
               Power(s2,3)*(-17 - 3*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(28 - 65*t2 + 48*Power(t2,2) + 
                  8*Power(t2,3)) - 
               t1*(11 + 22*t2 - 57*Power(t2,2) + 23*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(-14 + 12*t2 + 24*Power(t2,2) - 
                  3*Power(t2,3) + t1*(93 - 32*t2 - 2*Power(t2,2))) + 
               s2*(13 + 3*t2 - 26*Power(t2,2) + 10*Power(t2,3) + 
                  16*Power(t1,2)*(-9 + 5*t2) - 
                  2*t1*(-5 + 19*Power(t2,2) + 5*Power(t2,3)))) + 
            Power(s1,5)*(Power(s2,3)*
                (-10 + 92*t2 + 8*Power(t2,2) - 8*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (9 - 10*t2 - 31*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(-129 + 4*t2 + 15*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(55 - 203*t2 + 98*Power(t2,2) - 
                  38*Power(t2,3) - 10*Power(t2,4)) + 
               t1*(52 - 73*t2 + 76*Power(t2,2) - 93*Power(t2,3) + 
                  38*Power(t2,4)) + 
               Power(s2,2)*(136 - 263*t2 + 21*Power(t2,2) + 
                  8*Power(t2,3) - 
                  2*t1*(26 + 138*t2 - 32*Power(t2,2) + Power(t2,3))) + 
               s2*(-42 + 10*t2 + 21*Power(t2,2) + 47*Power(t2,3) - 
                  35*Power(t2,4) - Power(t2,5) - 
                  2*Power(t1,2)*
                   (-102 - 75*t2 + 33*Power(t2,2) + Power(t2,3)) + 
                  t1*(-243 + 557*t2 - 139*Power(t2,2) + 5*Power(t2,3) + 
                     16*Power(t2,4)))) + 
            Power(s1,4)*(Power(t1,3)*(21 + 143*t2 + Power(t2,2)) + 
               Power(-1 + t2,2)*
                (12 + 3*t2 - 22*Power(t2,2) - 59*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,3)*(3 + t2 - 138*Power(t2,2) - 
                  16*Power(t2,3) + 10*Power(t2,4)) + 
               Power(t1,2)*(-54 + 189*t2 + 90*Power(t2,2) + 
                  51*Power(t2,3) - 2*Power(t2,4) + Power(t2,5)) + 
               t1*(-11 - 176*t2 + 252*Power(t2,2) - 55*Power(t2,3) + 
                  3*Power(t2,4) - 14*Power(t2,5) + Power(t2,6)) + 
               Power(s2,2)*(-3 - 223*t2 + 643*Power(t2,2) - 
                  119*Power(t2,3) - 28*Power(t2,4) + 5*Power(t2,5) + 
                  t1*(-8 + 123*t2 + 336*Power(t2,2) - 11*Power(t2,3) + 
                     5*Power(t2,4))) - 
               s2*(22 - 359*t2 + 468*Power(t2,2) - 104*Power(t2,3) + 
                  17*Power(t2,4) - 42*Power(t2,5) - 2*Power(t2,6) + 
                  Power(t1,2)*
                   (16 + 295*t2 + 129*Power(t2,2) + 29*Power(t2,3) + 
                     Power(t2,4)) + 
                  t1*(-76 - 26*t2 + 793*Power(t2,2) - 133*Power(t2,3) - 
                     17*Power(t2,4) + 9*Power(t2,5)))) - 
            Power(s1,3)*(Power(t1,3)*
                (-38 + 48*t2 + 58*Power(t2,2) + 32*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-19 + 62*t2 - 62*Power(t2,2) - 69*Power(t2,3) - 
                  33*Power(t2,4) + Power(t2,5)) + 
               Power(s2,3)*(-8 - 22*t2 + 28*Power(t2,2) - 
                  82*Power(t2,3) - 20*Power(t2,4) + 4*Power(t2,5)) + 
               Power(t1,2)*(108 - 139*t2 + 321*Power(t2,2) + 
                  96*Power(t2,3) + 40*Power(t2,4) + 4*Power(t2,5)) - 
               t1*(72 + 8*t2 - 5*Power(t2,2) - 58*Power(t2,3) - 
                  56*Power(t2,4) + 38*Power(t2,5) + Power(t2,6)) + 
               Power(s2,2)*(14 + 175*t2 - 260*Power(t2,2) + 
                  578*Power(t2,3) - 64*Power(t2,4) - 16*Power(t2,5) + 
                  3*Power(t2,6) + 
                  t1*(40 - 79*t2 + 104*Power(t2,2) + 202*Power(t2,3) + 
                     32*Power(t2,4) + Power(t2,5))) + 
               s2*(135 - 202*t2 + 524*Power(t2,2) - 691*Power(t2,3) + 
                  171*Power(t2,4) + 40*Power(t2,5) + 22*Power(t2,6) + 
                  Power(t2,7) - 
                  4*Power(t1,2)*
                   (-4 - 12*t2 + 54*Power(t2,2) + 30*Power(t2,3) + 
                     7*Power(t2,4)) - 
                  t1*(186 - 113*t2 + 269*Power(t2,2) + 
                     456*Power(t2,3) + 64*Power(t2,4) - 5*Power(t2,5) + 
                     3*Power(t2,6)))) + 
            Power(s1,2)*(Power(t1,3)*
                (30 - 79*t2 + 18*Power(t2,2) + 16*Power(t2,3)) - 
               Power(s2,3)*t2*
                (32 + 44*t2 - 108*Power(t2,2) + 33*Power(t2,3) + 
                  9*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (-10 + 97*t2 - 48*Power(t2,2) + 96*Power(t2,3) + 
                  65*Power(t2,4) + 5*Power(t2,5)) + 
               Power(t1,2)*(-48 + 217*t2 - 8*Power(t2,2) + 
                  130*Power(t2,3) + 80*Power(t2,4) + 6*Power(t2,5)) + 
               t1*(29 - 120*t2 - 64*Power(t2,2) + 179*Power(t2,3) - 
                  55*Power(t2,4) + 30*Power(t2,5) + Power(t2,6)) + 
               Power(s2,2)*(-5 + 93*t2 + 315*Power(t2,2) - 
                  365*Power(t2,3) + 341*Power(t2,4) + 3*Power(t2,5) - 
                  5*Power(t2,6) + 
                  t1*(7 + 121*t2 - 174*Power(t2,2) - 4*Power(t2,3) + 
                     44*Power(t2,4) + 11*Power(t2,5))) - 
               s2*(-23 - 288*t2 + 530*Power(t2,2) - 497*Power(t2,3) + 
                  404*Power(t2,4) - 87*Power(t2,5) - 36*Power(t2,6) - 
                  3*Power(t2,7) + 
                  2*Power(t1,2)*
                   (8 + t2 - 57*Power(t2,2) + 27*Power(t2,3) + 
                     11*Power(t2,4)) + 
                  t1*(26 + 405*t2 - 217*Power(t2,2) + 282*Power(t2,3) + 
                     192*Power(t2,4) + 63*Power(t2,5) + 3*Power(t2,6)))) \
+ s1*(2*Power(s2,3)*Power(t2,2)*
                (20 + 5*t2 - 43*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,3)*(-21 - 20*t2 + 59*Power(t2,2) + 
                  24*Power(t2,3)) - 
               Power(t1,2)*(-95 + 97*t2 + 152*Power(t2,2) + 
                  34*Power(t2,3) - 18*Power(t2,4) + 4*Power(t2,5)) + 
               Power(-1 + t2,2)*
                (48 - 30*t2 + 81*Power(t2,2) + 18*Power(t2,3) + 
                  55*Power(t2,4) + 13*Power(t2,5)) + 
               t1*(-122 + 201*t2 + 39*Power(t2,2) - 81*Power(t2,3) - 
                  38*Power(t2,4) + 18*Power(t2,5) - 17*Power(t2,6)) + 
               Power(s2,2)*t2*
                (11 - 138*t2 - 122*Power(t2,2) + 198*Power(t2,3) - 
                  130*Power(t2,4) + 7*Power(t2,5) + 
                  t1*(-15 - 128*t2 + 172*Power(t2,2) + 52*Power(t2,3) + 
                     5*Power(t2,4))) - 
               s2*(11 - 2*t2 + 187*Power(t2,2) - 334*Power(t2,3) + 
                  224*Power(t2,4) - 119*Power(t2,5) + 30*Power(t2,6) + 
                  3*Power(t2,7) + 
                  2*Power(t1,2)*
                   (6 - 49*t2 + 15*Power(t2,2) + 63*Power(t2,3) + 
                     18*Power(t2,4)) - 
                  t1*(23 - 62*t2 + 350*Power(t2,2) - 85*Power(t2,3) + 
                     100*Power(t2,4) + 17*Power(t2,5) + 5*Power(t2,6))))) \
+ Power(s,5)*(5 - 15*t1 + 14*Power(t1,3) + 121*t2 - 50*s2*t2 - 
            578*t1*t2 + 225*s2*t1*t2 + 543*Power(t1,2)*t2 - 
            196*s2*Power(t1,2)*t2 - 594*Power(t2,2) + 
            274*s2*Power(t2,2) - 75*Power(s2,2)*Power(t2,2) + 
            518*t1*Power(t2,2) + 361*s2*t1*Power(t2,2) - 
            70*Power(s2,2)*t1*Power(t2,2) - 
            734*Power(t1,2)*Power(t2,2) + 
            273*s2*Power(t1,2)*Power(t2,2) - 
            29*Power(t1,3)*Power(t2,2) + 306*Power(t2,3) - 
            1565*s2*Power(t2,3) + 222*Power(s2,2)*Power(t2,3) + 
            90*Power(s2,3)*Power(t2,3) + 1671*t1*Power(t2,3) - 
            792*s2*t1*Power(t2,3) - 118*Power(s2,2)*t1*Power(t2,3) - 
            55*Power(t1,2)*Power(t2,3) + 82*s2*Power(t1,2)*Power(t2,3) - 
            10*Power(t1,3)*Power(t2,3) - 88*Power(t2,4) + 
            466*s2*Power(t2,4) - 108*Power(s2,2)*Power(t2,4) + 
            Power(s2,3)*Power(t2,4) - 1329*t1*Power(t2,4) + 
            563*s2*t1*Power(t2,4) - 32*Power(s2,2)*t1*Power(t2,4) + 
            123*Power(t1,2)*Power(t2,4) - 
            30*s2*Power(t1,2)*Power(t2,4) + 179*Power(t2,5) + 
            43*s2*Power(t2,5) - 14*Power(s2,2)*Power(t2,5) - 
            6*Power(s2,3)*Power(t2,5) + 391*t1*Power(t2,5) - 
            144*s2*t1*Power(t2,5) + 15*Power(s2,2)*t1*Power(t2,5) - 
            20*Power(t1,2)*Power(t2,5) - 18*Power(t2,6) + 
            62*s2*Power(t2,6) - 6*Power(s2,2)*Power(t2,6) - 
            44*t1*Power(t2,6) + 26*s2*t1*Power(t2,6) - 20*Power(t2,7) - 
            16*s2*Power(t2,7) + 
            Power(s1,7)*(t1*(-21 + 56*t2 - 36*Power(t2,2)) - 
               2*t2*(3 - 7*t2 + 4*Power(t2,2)) + 
               s2*(15 - 42*t2 + 28*Power(t2,2))) - 
            Power(s1,6)*(25 - 71*t2 + 29*Power(t2,2) + 64*Power(t2,3) - 
               56*Power(t2,4) - 
               14*Power(t1,2)*(-5 + 6*t2 + 6*Power(t2,2)) + 
               2*Power(s2,2)*(10 - 30*t2 + 7*Power(t2,2)) + 
               t1*(95 - 205*t2 + 42*Power(t2,2) + 28*Power(t2,3)) + 
               s2*(t1*(-85 + 168*t2 + 28*Power(t2,2)) + 
                  2*(-24 + 70*t2 - 61*Power(t2,2) + 35*Power(t2,3)))) + 
            Power(s1,5)*(7*Power(t1,3)*(-61 + 36*t2) + 
               Power(s2,3)*(26 + 29*t2 - 40*Power(t2,2)) - 
               Power(t1,2)*(415 - 625*t2 + 553*Power(t2,2) + 
                  70*Power(t2,3)) + 
               t2*(142 - 476*t2 + 363*Power(t2,2) + 7*Power(t2,3) - 
                  66*Power(t2,4)) + 
               t1*(68 + 44*t2 - 1011*Power(t2,2) + 718*Power(t2,3) + 
                  14*Power(t2,4)) - 
               Power(s2,2)*(4 + 93*t2 + 90*Power(t2,2) + 
                  12*Power(t2,3) + t1*(233 + 2*t2 - 70*Power(t2,2))) + 
               s2*(8 - 229*t2 + 752*Power(t2,2) - 371*Power(t2,3) + 
                  39*Power(t2,4) - 7*Power(t1,2)*(-84 + 37*t2) + 
                  t1*(192 - 10*t2 + 307*Power(t2,2) + 77*Power(t2,3)))) \
+ Power(s1,4)*(60 - 383*t2 + 356*Power(t2,2) + 986*Power(t2,3) - 
               1031*Power(t2,4) + 53*Power(t2,5) + 20*Power(t2,6) + 
               Power(s2,3)*(40 - 140*t2 - 31*Power(t2,2) + 
                  50*Power(t2,3)) - 
               Power(t1,3)*(166 - 532*t2 + 21*Power(t2,2) + 
                  70*Power(t2,3)) + 
               t1*(-77 + 819*t2 - 1173*Power(t2,2) + 
                  2037*Power(t2,3) - 890*Power(t2,4)) + 
               Power(t1,2)*(-381 + 2059*t2 - 938*Power(t2,2) + 
                  225*Power(t2,3) + 140*Power(t2,4)) + 
               Power(s2,2)*(-97 + 340*t2 + 343*Power(t2,2) + 
                  56*Power(t2,3) - 15*Power(t2,4) + 
                  t1*(-167 + 551*t2 + 101*Power(t2,2) - 80*Power(t2,3))\
) + s2*(-39 + 172*t2 - 117*Power(t2,2) - 1259*Power(t2,3) + 
                  494*Power(t2,4) - 15*Power(t2,5) + 
                  Power(t1,2)*
                   (240 - 792*t2 - 129*Power(t2,2) + 70*Power(t2,3)) + 
                  t1*(515 - 2415*t2 + 499*Power(t2,2) - 
                     80*Power(t2,3) - 110*Power(t2,4)))) + 
            Power(s1,3)*(83 - 646*t2 + 681*Power(t2,2) + 
               199*Power(t2,3) - 1176*Power(t2,4) + 763*Power(t2,5) + 
               26*Power(t2,6) + 2*Power(t2,7) - 
               2*Power(t1,3)*
                (53 - 126*t2 + 58*Power(t2,2) + 50*Power(t2,3)) + 
               Power(s2,3)*(26 - 217*t2 + 338*Power(t2,2) - 
                  49*Power(t2,3) - 18*Power(t2,4)) - 
               Power(t1,2)*(497 - 1818*t2 + 1842*Power(t2,2) + 
                  136*Power(t2,3) - 155*Power(t2,4) + 20*Power(t2,5)) + 
               t1*(-78 + 1009*t2 - 3153*Power(t2,2) + 
                  2362*Power(t2,3) - 1134*Power(t2,4) + 
                  179*Power(t2,5) - 10*Power(t2,6)) + 
               Power(s2,2)*(-160 + 809*t2 - 965*Power(t2,2) - 
                  195*Power(t2,3) - 89*Power(t2,4) + 38*Power(t2,5) + 
                  t1*(-117 + 410*t2 - 361*Power(t2,2) - 
                     124*Power(t2,3) + 20*Power(t2,4))) + 
               s2*(17 - 210*t2 + 866*Power(t2,2) - 581*Power(t2,3) + 
                  982*Power(t2,4) - 243*Power(t2,5) + 18*Power(t2,6) + 
                  2*Power(t1,2)*
                   (91 - 225*t2 + 87*Power(t2,2) + 126*Power(t2,3) + 
                     10*Power(t2,4)) + 
                  t1*(558 - 2461*t2 + 2968*Power(t2,2) - 
                     48*Power(t2,3) - 144*Power(t2,4) + 15*Power(t2,5)))\
) - s1*(31 - 396*t2 + 123*Power(t2,2) - 650*Power(t2,3) + 
               1169*Power(t2,4) - 703*Power(t2,5) + 48*Power(t2,6) + 
               Power(s2,3)*Power(t2,2)*
                (28 + 213*t2 - 140*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,3)*(19 - 88*t2 + 36*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,2)*(208 - 1141*t2 + 1301*Power(t2,2) - 
                  22*Power(t2,3) - 205*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(-203 + 443*t2 + 2112*Power(t2,2) - 
                  3252*Power(t2,3) + 1424*Power(t2,4) - 
                  231*Power(t2,5) + 34*Power(t2,6)) + 
               Power(s2,2)*t2*
                (-30 + 1123*t2 - 1405*Power(t2,2) + 497*Power(t2,3) - 
                  78*Power(t2,4) + 20*Power(t2,5) + 
                  t1*(-180 - 43*t2 + 28*Power(t2,2) + 22*Power(t2,3) + 
                     4*Power(t2,4))) + 
               s2*(-20 + 71*t2 - 2706*Power(t2,2) + 3184*Power(t2,3) - 
                  1133*Power(t2,4) + 395*Power(t2,5) - 
                  148*Power(t2,6) + 14*Power(t2,7) + 
                  Power(t1,2)*
                   (-90 + 371*t2 - 138*Power(t2,2) - 100*Power(t2,3) + 
                     16*Power(t2,4)) + 
                  t1*(100 + 791*t2 - 2109*Power(t2,2) + 
                     1169*Power(t2,3) - 306*Power(t2,4) + 
                     93*Power(t2,5) - 12*Power(t2,6)))) + 
            Power(s1,2)*(-24 - 211*t2 + 291*Power(t2,2) + 
               680*Power(t2,3) - 1363*Power(t2,4) + 540*Power(t2,5) - 
               140*Power(t2,6) - 14*Power(t2,7) - 
               2*Power(t1,3)*
                (32 - 78*t2 + 27*Power(t2,2) + 24*Power(t2,3)) + 
               Power(s2,3)*t2*
                (-68 + 377*t2 - 366*Power(t2,2) + 65*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(t1,2)*(-381 + 1730*t2 - 1564*Power(t2,2) + 
                  14*Power(t2,3) + 241*Power(t2,4) - 12*Power(t2,5)) + 
               t1*(63 + 616*t2 - 3231*Power(t2,2) + 3364*Power(t2,3) - 
                  1077*Power(t2,4) + 223*Power(t2,5) - 8*Power(t2,6)) + 
               Power(s2,2)*(5 + 922*t2 - 1940*Power(t2,2) + 
                  1236*Power(t2,3) - 111*Power(t2,4) + 
                  110*Power(t2,5) - 6*Power(t2,6) + 
                  t1*(-75 + 205*t2 - 199*Power(t2,2) + 30*Power(t2,3) + 
                     18*Power(t2,4) - 11*Power(t2,5))) - 
               s2*(17 + 970*t2 - 2849*Power(t2,2) + 2210*Power(t2,3) - 
                  938*Power(t2,4) + 482*Power(t2,5) - 92*Power(t2,6) + 
                  2*Power(t2,7) + 
                  2*Power(t1,2)*
                   (-74 + 182*t2 - 60*Power(t2,2) - 68*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  t1*(-254 + 2060*t2 - 2712*Power(t2,2) + 
                     1060*Power(t2,3) + 71*Power(t2,4) + 
                     34*Power(t2,5) + 6*Power(t2,6))))) + 
         Power(s,4)*(-15*t1 + 42*Power(t1,2) - 28*Power(t1,3) - 190*t2 + 
            70*s2*t2 + 652*t1*t2 - 209*s2*t1*t2 - 563*Power(t1,2)*t2 + 
            140*s2*Power(t1,2)*t2 + 84*Power(t1,3)*t2 + 
            632*Power(t2,2) - 186*s2*Power(t2,2) + 
            22*Power(s2,2)*Power(t2,2) - 857*t1*Power(t2,2) - 
            34*s2*t1*Power(t2,2) + 56*Power(s2,2)*t1*Power(t2,2) + 
            781*Power(t1,2)*Power(t2,2) - 
            315*s2*Power(t1,2)*Power(t2,2) - 
            29*Power(t1,3)*Power(t2,2) - 752*Power(t2,3) + 
            1189*s2*Power(t2,3) + 78*Power(s2,2)*Power(t2,3) - 
            90*Power(s2,3)*Power(t2,3) - 937*t1*Power(t2,3) + 
            210*s2*t1*Power(t2,3) + 121*Power(s2,2)*t1*Power(t2,3) + 
            91*Power(t1,2)*Power(t2,3) + 53*s2*Power(t1,2)*Power(t2,3) - 
            24*Power(t1,3)*Power(t2,3) + 232*Power(t2,4) - 
            501*s2*Power(t2,4) + 47*Power(s2,2)*Power(t2,4) + 
            20*Power(s2,3)*Power(t2,4) + 1101*t1*Power(t2,4) - 
            558*s2*t1*Power(t2,4) - 35*Power(s2,2)*t1*Power(t2,4) - 
            120*Power(t1,2)*Power(t2,4) + 
            65*s2*Power(t1,2)*Power(t2,4) + 78*Power(t2,5) - 
            219*s2*Power(t2,5) + 27*Power(s2,2)*Power(t2,5) + 
            15*Power(s2,3)*Power(t2,5) - 389*t1*Power(t2,5) + 
            183*s2*t1*Power(t2,5) - 30*Power(s2,2)*t1*Power(t2,5) + 
            15*Power(t1,2)*Power(t2,5) + 3*Power(t2,6) + 
            47*s2*Power(t2,6) + 2*Power(s2,2)*Power(t2,6) + 
            72*t1*Power(t2,6) - 34*s2*t1*Power(t2,6) + 3*Power(t2,7) + 
            16*s2*Power(t2,7) + 
            Power(s1,8)*(-1 + t2)*
             (s2*(6 - 8*t2) + (-1 + t2)*t2 + t1*(-7 + 9*t2)) + 
            Power(s1,7)*(14 - 36*t2 + 29*Power(t2,2) - Power(t2,3) - 
               8*Power(t2,4) + 
               Power(t1,2)*(70 - 84*t2 - 36*Power(t2,2)) + 
               Power(s2,2)*(35 - 73*t2 + 14*Power(t2,2)) + 
               2*t1*(32 - 55*t2 + 9*Power(t2,2) + 4*Power(t2,3)) + 
               2*s2*(-16 + 29*t2 - 16*Power(t2,2) + 13*Power(t2,3) + 
                  4*t1*(-13 + 21*t2 + Power(t2,2)))) + 
            Power(s1,6)*(-20 + Power(t1,3)*(357 - 210*t2) - 65*t2 + 
               292*Power(t2,2) - 267*Power(t2,3) + 61*Power(t2,4) + 
               11*Power(t2,5) + 
               5*Power(s2,3)*(-7 - 8*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(308 - 495*t2 + 441*Power(t2,2) + 
                  56*Power(t2,3)) - 
               t1*(98 + 83*t2 - 749*Power(t2,2) + 423*Power(t2,3) + 
                  14*Power(t2,4)) - 
               2*Power(s2,2)*
                (5 - 41*t2 - 68*Power(t2,2) + 6*Power(t2,3) + 
                  t1*(-135 + t2 + 21*Power(t2,2))) + 
               s2*(29 + 110*t2 - 421*Power(t2,2) + 160*Power(t2,3) - 
                  10*Power(t2,4) + 7*Power(t1,2)*(-82 + 35*t2) - 
                  t1*(103 + 80*t2 + 232*Power(t2,2) + 77*Power(t2,3)))) \
- Power(s1,5)*(66 - 393*t2 + 296*Power(t2,2) + 748*Power(t2,3) - 
               764*Power(t2,4) + 92*Power(t2,5) + Power(t2,6) + 
               Power(t1,3)*(213 + 266*t2 - 21*Power(t2,2) - 
                  56*Power(t2,3)) + 
               Power(s2,3)*(38 - 178*t2 - 85*Power(t2,2) + 
                  65*Power(t2,3)) + 
               t1*(-231 + 735*t2 - 479*Power(t2,2) + 
                  1133*Power(t2,3) - 606*Power(t2,4)) + 
               Power(t1,2)*(-201 + 1460*t2 - 471*Power(t2,2) + 
                  216*Power(t2,3) + 98*Power(t2,4)) + 
               Power(s2,2)*(-155 + 531*t2 + 266*Power(t2,2) + 
                  127*Power(t2,3) - 3*Power(t2,4) + 
                  t1*(-13 + 717*t2 + 34*Power(t2,2) - 46*Power(t2,3))) \
+ s2*(-24 + 201*t2 - 352*Power(t2,2) - 671*Power(t2,3) + 
                  298*Power(t2,4) + 8*Power(t2,5) + 
                  6*Power(t1,2)*
                   (-44 - 110*t2 - 10*Power(t2,2) + 7*Power(t2,3)) + 
                  t1*(575 - 2336*t2 + 76*Power(t2,2) - 41*Power(t2,3) - 
                     100*Power(t2,4)))) + 
            Power(s1,4)*(-134 + 348*t2 - 290*Power(t2,2) + 
               363*Power(t2,3) + 540*Power(t2,4) - 701*Power(t2,5) + 
               15*Power(t2,6) - 3*Power(t2,7) + 
               Power(t1,3)*(-42 + 240*t2 + 95*Power(t2,2) + 
                  40*Power(t2,3)) + 
               Power(s2,3)*(-51 + 221*t2 - 334*Power(t2,2) - 
                  68*Power(t2,3) + 47*Power(t2,4)) + 
               Power(t1,2)*(366 - 317*t2 + 1143*Power(t2,2) + 
                  331*Power(t2,3) - 40*Power(t2,4) + 15*Power(t2,5)) + 
               t1*(223 - 810*t2 + 2238*Power(t2,2) - 563*Power(t2,3) + 
                  284*Power(t2,4) - 161*Power(t2,5) + 10*Power(t2,6)) + 
               Power(s2,2)*(178 - 697*t2 + 1224*Power(t2,2) + 
                  260*Power(t2,3) + 97*Power(t2,4) - 2*Power(t2,5) + 
                  t1*(43 - 158*t2 + 698*Power(t2,2) + 97*Power(t2,3) - 
                     20*Power(t2,4))) - 
               s2*(33 - 267*t2 + 914*Power(t2,2) + 321*Power(t2,3) + 
                  366*Power(t2,4) - 192*Power(t2,5) - 3*Power(t2,6) + 
                  Power(t1,2)*
                   (-12 + 214*t2 + 411*Power(t2,2) + 195*Power(t2,3) + 
                     15*Power(t2,4)) + 
                  t1*(448 - 949*t2 + 2756*Power(t2,2) + 
                     316*Power(t2,3) - 116*Power(t2,4) + 35*Power(t2,5))\
)) + s1*(85 - 517*t2 + 1006*Power(t2,2) - 1426*Power(t2,3) + 
               1171*Power(t2,4) - 470*Power(t2,5) + 93*Power(t2,6) - 
               12*Power(t2,7) - 
               Power(t1,3)*(33 - 30*t2 + 7*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (97 + 207*t2 - 202*Power(t2,2) + 38*Power(t2,3)) - 
               Power(t1,2)*(-293 + 780*t2 - 447*Power(t2,2) + 
                  116*Power(t2,3) + 122*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(-330 + 473*t2 + 1799*Power(t2,2) - 
                  1641*Power(t2,3) + 585*Power(t2,4) + 
                  48*Power(t2,5) + 34*Power(t2,6)) + 
               Power(s2,2)*t2*
                (5 + 414*t2 - 1411*Power(t2,2) + 974*Power(t2,3) - 
                  218*Power(t2,4) + 10*Power(t2,5) + 
                  t1*(-127 - 188*t2 + 34*Power(t2,2) + 3*Power(t2,3) + 
                     2*Power(t2,4))) + 
               s2*(-30 - 13*t2 - 2838*Power(t2,2) + 3193*Power(t2,3) - 
                  1472*Power(t2,4) + 156*Power(t2,5) - 
                  106*Power(t2,6) + 6*Power(t2,7) + 
                  2*Power(t1,2)*
                   (-32 + 100*t2 - 12*Power(t2,2) + 9*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  t1*(95 + 630*t2 - 686*Power(t2,2) + 473*Power(t2,3) - 
                     22*Power(t2,4) + 86*Power(t2,5) - 6*Power(t2,6)))) \
+ Power(s1,2)*(104 - 412*t2 + 1600*Power(t2,2) - 1569*Power(t2,3) + 
               1037*Power(t2,4) - 482*Power(t2,5) - 54*Power(t2,6) - 
               28*Power(t2,7) - 
               Power(t1,3)*(15 - 46*t2 + 18*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(s2,3)*t2*
                (9 - 412*t2 + 490*Power(t2,2) - 209*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(t1,2)*(300 - 785*t2 + 411*Power(t2,2) + 
                  26*Power(t2,3) - 80*Power(t2,4) - 6*Power(t2,5)) + 
               t1*(-101 - 871*t2 + 1389*Power(t2,2) - 
                  1189*Power(t2,3) + 277*Power(t2,4) + 
                  70*Power(t2,5) + 38*Power(t2,6)) + 
               Power(s2,2)*(-10 - 735*t2 + 2198*Power(t2,2) - 
                  2450*Power(t2,3) + 990*Power(t2,4) - 
                  71*Power(t2,5) + 22*Power(t2,6) + 
                  t1*(55 + 88*t2 + 88*Power(t2,2) - 66*Power(t2,3) - 
                     9*Power(t2,4) + 14*Power(t2,5))) + 
               s2*(60 + 2017*t2 - 4047*Power(t2,2) + 3356*Power(t2,3) - 
                  1129*Power(t2,4) + 353*Power(t2,5) - 70*Power(t2,6) + 
                  12*Power(t2,7) + 
                  Power(t1,2)*
                   (-30 - 3*t2 - 66*Power(t2,2) + 38*Power(t2,3) + 
                     22*Power(t2,4)) - 
                  t1*(329 - 764*t2 + 442*Power(t2,2) - 
                     359*Power(t2,3) + 150*Power(t2,4) + 
                     36*Power(t2,5) + 6*Power(t2,6)))) - 
            Power(s1,3)*(-1 + 242*t2 - 229*Power(t2,2) + 
               913*Power(t2,3) - 411*Power(t2,4) - 40*Power(t2,5) - 
               232*Power(t2,6) - 8*Power(t2,7) + 
               Power(t1,3)*(26 - 76*t2 + 62*Power(t2,2) + 
                  48*Power(t2,3)) + 
               2*Power(s2,3)*
                (10 - 128*t2 + 261*Power(t2,2) - 195*Power(t2,3) - 
                  4*Power(t2,4) + 6*Power(t2,5)) + 
               2*Power(t1,2)*
                (-170 + 318*t2 - 91*Power(t2,2) + 86*Power(t2,3) + 
                  90*Power(t2,4) + 6*Power(t2,5)) + 
               t1*(-51 + 996*t2 - 1440*Power(t2,2) + 1418*Power(t2,3) + 
                  55*Power(t2,4) + 16*Power(t2,5) - 6*Power(t2,6)) + 
               Power(s2,2)*(-244 + 1139*t2 - 2210*Power(t2,2) + 
                  1618*Power(t2,3) - 17*Power(t2,4) + 72*Power(t2,5) + 
                  2*Power(t2,6) + 
                  t1*(-3 + 108*t2 - 190*Power(t2,2) + 232*Power(t2,3) + 
                     67*Power(t2,4) - 14*Power(t2,5))) - 
               2*s2*(-158 + 815*t2 - 1089*Power(t2,2) + 
                  983*Power(t2,3) - 152*Power(t2,4) + 75*Power(t2,5) - 
                  33*Power(t2,6) - Power(t2,7) + 
                  Power(t1,2)*
                   (4 - 2*t2 - 6*Power(t2,2) + 64*Power(t2,3) + 
                     22*Power(t2,4)) + 
                  t1*(-180 + 377*t2 - 435*Power(t2,2) + 
                     487*Power(t2,3) + 145*Power(t2,4) - 3*Power(t2,5) + 
                     7*Power(t2,6))))) + 
         Power(s,3)*(-5 + 27*t1 - 42*Power(t1,2) + 20*Power(t1,3) + 
            Power(s1,9)*(s2 - t1)*Power(-1 + t2,2) + 157*t2 - 48*s2*t2 - 
            456*t1*t2 + 113*s2*t1*t2 + 389*Power(t1,2)*t2 - 
            64*s2*Power(t1,2)*t2 - 90*Power(t1,3)*t2 - 412*Power(t2,2) + 
            97*s2*Power(t2,2) + 7*Power(s2,2)*Power(t2,2) + 
            739*t1*Power(t2,2) - 166*s2*t1*Power(t2,2) - 
            28*Power(s2,2)*t1*Power(t2,2) - 551*Power(t1,2)*Power(t2,2) + 
            231*s2*Power(t1,2)*Power(t2,2) + 55*Power(t1,3)*Power(t2,2) + 
            494*Power(t2,3) - 410*s2*Power(t2,3) - 
            160*Power(s2,2)*Power(t2,3) + 52*Power(s2,3)*Power(t2,3) + 
            118*t1*Power(t2,3) + 214*s2*t1*Power(t2,3) - 
            88*Power(s2,2)*t1*Power(t2,3) - 31*Power(t1,2)*Power(t2,3) - 
            130*s2*Power(t1,2)*Power(t2,3) + 32*Power(t1,3)*Power(t2,3) - 
            193*Power(t2,4) + 224*s2*Power(t2,4) + 
            30*Power(s2,2)*Power(t2,4) - 24*Power(s2,3)*Power(t2,4) - 
            526*t1*Power(t2,4) + 246*s2*t1*Power(t2,4) + 
            88*Power(s2,2)*t1*Power(t2,4) + 93*Power(t1,2)*Power(t2,4) - 
            64*s2*Power(t1,2)*Power(t2,4) - 114*Power(t2,5) + 
            133*s2*Power(t2,5) - 13*Power(s2,2)*Power(t2,5) - 
            20*Power(s2,3)*Power(t2,5) + 232*t1*Power(t2,5) - 
            143*s2*t1*Power(t2,5) + 30*Power(s2,2)*t1*Power(t2,5) - 
            6*Power(t1,2)*Power(t2,5) + 61*Power(t2,6) - 
            71*s2*Power(t2,6) - Power(s2,2)*Power(t2,6) - 
            52*t1*Power(t2,6) + 21*s2*t1*Power(t2,6) + 12*Power(t2,7) - 
            7*s2*Power(t2,7) - 
            Power(s1,8)*(3 - 7*t2 + 7*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,2)*(42 - 48*t2 - 9*Power(t2,2)) + 
               Power(s2,2)*(29 - 46*t2 + 6*Power(t2,2)) + 
               t1*(29 - 43*t2 + 9*Power(t2,2) + Power(t2,3)) + 
               s2*(t1*(-71 + 96*t2 + Power(t2,2)) + 
                  2*(-6 + 5*t2 + Power(t2,2) + 2*Power(t2,3)))) + 
            Power(s1,7)*(18 - 2*t2 - 64*Power(t2,2) + 67*Power(t2,3) - 
               19*Power(t2,4) + Power(t1,3)*(-197 + 120*t2) - 
               6*Power(s2,3)*(-5 - 4*t2 + 2*Power(t2,2)) - 
               Power(t1,2)*(103 - 205*t2 + 203*Power(t2,2) + 
                  28*Power(t2,3)) + 
               t1*(71 + 45*t2 - 300*Power(t2,2) + 142*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,2)*(39 - 69*t2 - 86*Power(t2,2) + 
                  12*Power(t2,3) + 2*t1*(-100 + 13*t2 + 7*Power(t2,2))) \
+ s2*(-45 + Power(t1,2)*(364 - 169*t2) - 18*t2 + 144*Power(t2,2) - 
                  46*Power(t2,3) + Power(t2,4) + 
                  t1*(-39 + 116*t2 + 117*Power(t2,2) + 39*Power(t2,3)))) \
+ Power(s1,6)*(20 - 181*t2 + 122*Power(t2,2) + 258*Power(t2,3) - 
               244*Power(t2,4) + 26*Power(t2,5) - Power(t2,6) - 
               Power(t1,3)*(-398 + 14*t2 + 21*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(s2,3)*(-2 - 167*t2 - 62*Power(t2,2) + 
                  37*Power(t2,3)) + 
               t1*(-205 + 239*t2 + 35*Power(t2,2) + 347*Power(t2,3) - 
                  236*Power(t2,4)) + 
               Power(t1,2)*(-5 + 501*t2 - 80*Power(t2,2) + 
                  141*Power(t2,3) + 42*Power(t2,4)) + 
               Power(s2,2)*(-212 + 554*t2 + 111*Power(t2,2) + 
                  55*Power(t2,3) + 
                  t1*(209 + 564*t2 - 43*Power(t2,2) - 10*Power(t2,3))) + 
               s2*(77 + 46*t2 - 230*Power(t2,2) - 216*Power(t2,3) + 
                  137*Power(t2,4) + 6*Power(t2,5) + 
                  Power(t1,2)*
                   (-632 - 282*t2 + 39*Power(t2,2) + 14*Power(t2,3)) + 
                  t1*(453 - 1474*t2 + 16*Power(t2,2) - 48*Power(t2,3) - 
                     54*Power(t2,4)))) + 
            Power(s1,5)*(22 + 137*t2 + 93*Power(t2,2) - 
               466*Power(t2,3) - 81*Power(t2,4) + 301*Power(t2,5) - 
               7*Power(t2,6) + Power(t2,7) + 
               3*Power(t1,3)*
                (-53 - 120*t2 - 10*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,3)*(8 + 38*t2 + 249*Power(t2,2) + 
                  74*Power(t2,3) - 37*Power(t2,4)) - 
               Power(t1,2)*(-16 + 805*t2 + 315*Power(t2,2) + 
                  302*Power(t2,3) + 25*Power(t2,4) + 6*Power(t2,5)) + 
               t1*(120 + 183*t2 - 818*Power(t2,2) - 292*Power(t2,3) + 
                  152*Power(t2,4) + 78*Power(t2,5) - 5*Power(t2,6)) - 
               Power(s2,2)*(28 - 52*t2 + 1144*Power(t2,2) + 
                  68*Power(t2,3) + 27*Power(t2,4) + 13*Power(t2,5) + 
                  t1*(50 + 472*t2 + 637*Power(t2,2) + 46*Power(t2,3) - 
                     3*Power(t2,4))) + 
               s2*(22 - 524*t2 + 976*Power(t2,2) + 244*Power(t2,3) + 
                  7*Power(t2,4) - 136*Power(t2,5) - 7*Power(t2,6) + 
                  Power(t1,2)*
                   (234 + 737*t2 + 330*Power(t2,2) + 100*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  t1*(-178 + 849*t2 + 1861*Power(t2,2) + 
                     129*Power(t2,3) - 23*Power(t2,4) + 27*Power(t2,5)))) \
+ Power(s1,2)*(-22 + 425*t2 - 1174*Power(t2,2) + 1190*Power(t2,3) - 
               204*Power(t2,4) - 20*Power(t2,5) - 153*Power(t2,6) - 
               42*Power(t2,7) - 
               3*Power(t1,3)*
                (14 - 18*t2 + Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,3)*t2*
                (45 + 258*t2 - 235*Power(t2,2) + 86*Power(t2,3) - 
                  36*Power(t2,4)) - 
               Power(t1,2)*(-87 + 285*t2 + 120*Power(t2,2) + 
                  149*Power(t2,3) + 52*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(-155 + 745*t2 + 655*Power(t2,2) - 515*Power(t2,3) + 
                  254*Power(t2,4) + 124*Power(t2,5) + 28*Power(t2,6)) + 
               Power(s2,2)*(10 + 130*t2 - 1419*Power(t2,2) + 
                  1447*Power(t2,3) - 828*Power(t2,4) + 
                  110*Power(t2,5) - 6*Power(t2,6) + 
                  t1*(-25 - 232*t2 - 19*Power(t2,2) + 46*Power(t2,3) + 
                     2*Power(t2,4) + 8*Power(t2,5))) + 
               s2*(-79 - 1400*t2 + 1838*Power(t2,2) - 2186*Power(t2,3) + 
                  885*Power(t2,4) - 234*Power(t2,5) + 36*Power(t2,6) + 
                  4*Power(t2,7) + 
                  Power(t1,2)*
                   (-16 + 134*t2 - 39*Power(t2,2) + 18*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  t1*(219 + 488*t2 - 206*Power(t2,2) + 436*Power(t2,3) + 
                     150*Power(t2,4) + 2*Power(t2,5) - 2*Power(t2,6)))) + 
            Power(s1,4)*(-122 + 92*t2 - 489*Power(t2,2) + 
               711*Power(t2,3) + 325*Power(t2,4) - 376*Power(t2,5) - 
               139*Power(t2,6) - 2*Power(t2,7) + 
               Power(t1,3)*(-24 + 178*t2 + 129*Power(t2,2) + 
                  72*Power(t2,3)) + 
               t1*(66 - 91*t2 + 700*Power(t2,2) + 403*Power(t2,3) + 
                  244*Power(t2,4) - 132*Power(t2,5)) + 
               2*Power(s2,3)*
                (15 - 53*t2 + 62*Power(t2,2) - 119*Power(t2,3) - 
                  21*Power(t2,4) + 6*Power(t2,5)) + 
               Power(t1,2)*(194 + 115*t2 + 838*Power(t2,2) + 
                  391*Power(t2,3) + 109*Power(t2,4) + 18*Power(t2,5)) + 
               Power(s2,2)*(-109 + 834*t2 - 597*Power(t2,2) + 
                  1186*Power(t2,3) + 50*Power(t2,4) + 19*Power(t2,5) + 
                  7*Power(t2,6) + 
                  t1*(-8 - 12*t2 + 426*Power(t2,2) + 340*Power(t2,3) + 
                     70*Power(t2,4) - 6*Power(t2,5))) - 
               s2*(-194 + 824*t2 - 897*Power(t2,2) + 1760*Power(t2,3) - 
                  102*Power(t2,4) - 139*Power(t2,5) - 59*Power(t2,6) - 
                  3*Power(t2,7) + 
                  Power(t1,2)*
                   (-24 + 204*t2 + 423*Power(t2,2) + 286*Power(t2,3) + 
                     56*Power(t2,4)) + 
                  t1*(239 + 103*t2 + 1443*Power(t2,2) + 
                     1018*Power(t2,3) + 238*Power(t2,4) + 
                     3*Power(t2,5) + 11*Power(t2,6)))) + 
            Power(s1,3)*(-138 + 736*t2 - 712*Power(t2,2) + 
               229*Power(t2,3) - 645*Power(t2,4) + 250*Power(t2,5) + 
               252*Power(t2,6) + 28*Power(t2,7) - 
               Power(t1,3)*(19 - 56*t2 + 52*Power(t2,2) + 
                  40*Power(t2,3)) + 
               2*Power(s2,3)*
                (-1 - 77*t2 + 89*Power(t2,2) - 110*Power(t2,3) + 
                  81*Power(t2,4) + 3*Power(t2,5)) - 
               Power(t1,2)*(-81 + 261*t2 + 65*Power(t2,2) + 
                  192*Power(t2,3) + 146*Power(t2,4) + 12*Power(t2,5)) - 
               t1*(147 + 633*t2 - 180*Power(t2,2) + 610*Power(t2,3) + 
                  184*Power(t2,4) + 96*Power(t2,5) + 14*Power(t2,6)) + 
               Power(s2,2)*(-119 + 713*t2 - 1510*Power(t2,2) + 
                  1252*Power(t2,3) - 760*Power(t2,4) + 16*Power(t2,5) - 
                  8*Power(t2,6) - 
                  2*t1*(-37 - 35*t2 - 28*Power(t2,2) + 70*Power(t2,3) + 
                     34*Power(t2,4) + 6*Power(t2,5))) + 
               s2*(497 - 818*t2 + 2138*Power(t2,2) - 1228*Power(t2,3) + 
                  1237*Power(t2,4) - 264*Power(t2,5) - 48*Power(t2,6) - 
                  10*Power(t2,7) + 
                  Power(t1,2)*
                   (-8 - 55*t2 + 12*Power(t2,2) + 120*Power(t2,3) + 
                     36*Power(t2,4)) + 
                  t1*(-227 + 374*t2 + 11*Power(t2,2) + 457*Power(t2,3) + 
                     282*Power(t2,4) + 102*Power(t2,5) + 12*Power(t2,6)))\
) + s1*(-90 + 229*t2 - 557*Power(t2,2) + 714*Power(t2,3) - 
               539*Power(t2,4) + 253*Power(t2,5) - 13*Power(t2,6) + 
               3*Power(t2,7) + 
               Power(t1,3)*(23 + 56*t2 - 78*Power(t2,2) - 
                  36*Power(t2,3)) - 
               Power(s2,3)*Power(t2,2)*
                (95 + 110*t2 - 175*Power(t2,2) + 26*Power(t2,3)) + 
               Power(t1,2)*(-186 + 93*t2 + 487*Power(t2,2) + 
                  170*Power(t2,3) - 21*Power(t2,4) + 18*Power(t2,5)) + 
               t1*(253 - 77*t2 - 1181*Power(t2,2) + 408*Power(t2,3) + 
                  290*Power(t2,4) - 206*Power(t2,5) + 43*Power(t2,6)) + 
               Power(s2,2)*t2*
                (-20 + 164*t2 + 756*Power(t2,2) - 705*Power(t2,3) + 
                  329*Power(t2,4) + 8*Power(t2,5) + 
                  t1*(56 + 231*t2 - 102*Power(t2,2) - 95*Power(t2,3) - 
                     20*Power(t2,4))) + 
               s2*(25 + 78*t2 + 1181*Power(t2,2) - 1434*Power(t2,3) + 
                  927*Power(t2,4) - 348*Power(t2,5) + 31*Power(t2,6) + 
                  10*Power(t2,7) + 
                  Power(t1,2)*
                   (34 - 97*t2 - 150*Power(t2,2) + 164*Power(t2,3) + 
                     70*Power(t2,4)) - 
                  t1*(60 + 267*t2 + 189*Power(t2,2) + 209*Power(t2,3) + 
                     363*Power(t2,4) - 15*Power(t2,5) + 20*Power(t2,6)))))\
)*T2q(s1,s))/(Power(s,2)*(-1 + s1)*
       Power(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2),2)*
       (-1 + s2)*(-1 + t1)*Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) - 
    (8*(-((-1 + s1)*Power(s1 - t2,4)*(-1 + t2)*
            Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3)) + 
         Power(s,7)*(s1 - t2)*Power(t2,2)*
          (-2*s1*t2*(2 - t1 + t2) + Power(s1,2)*(t1 + t2) + 
            t2*(-4 + Power(t2,2) + t1*(4 + t2))) + 
         Power(s,6)*t2*(Power(s1,4)*
             (t1*(2 - 4*t2) + (2 + s2 - 3*t2)*t2) + 
            Power(s1,3)*t2*(-11 + Power(t1,2) + 4*t2 + 3*t1*t2 + 
               9*Power(t2,2) - s2*(t1 + 5*t2)) - 
            Power(s1,2)*t2*(12 - (33 + 4*s2)*t2 - Power(t1,2)*t2 + 
               (8 - 9*s2)*Power(t2,2) + 9*Power(t2,3) + 
               t1*(-12 + (8 + s2)*t2 + Power(t2,2))) + 
            s1*Power(t2,2)*(24 - 45*t2 - 4*Power(t2,2) + 
               3*Power(t2,3) + Power(t1,2)*(10 + t2) + 
               s2*(4 + t1*(-4 + t2) - 4*t2 - 7*Power(t2,2)) + 
               t1*(-36 + 14*t2 + 11*Power(t2,2))) + 
            Power(t2,2)*(4 + (2 - 4*s2)*t2 + 19*Power(t2,2) + 
               2*(2 + s2)*Power(t2,3) - 
               Power(t1,2)*(-2 + 2*t2 + Power(t2,2)) + 
               t1*(-6 + (2 + 4*s2)*t2 + (-6 + s2)*Power(t2,2) - 
                  7*Power(t2,3)))) + 
         Power(s,5)*(Power(s1,5)*
             (t2*(1 + s2*(2 - 3*t2) - 4*t2 + 3*Power(t2,2)) + 
               t1*(1 - 6*t2 + 6*Power(t2,2))) - 
            Power(s1,4)*t2*(10 + (-20 + 15*s2 + Power(s2,2))*t2 - 
               13*s2*Power(t2,2) + 9*Power(t2,3) + 
               Power(t1,2)*(-1 + 4*t2) + 
               t1*(1 + s2*(2 - 3*t2) - 18*t2 + 15*Power(t2,2))) - 
            Power(t2,3)*(18 + Power(t1,3)*(-1 + t2) + (58 - 22*s2)*t2 + 
               (17 + 5*s2)*Power(t2,2) + 
               (-37 + 9*s2 + Power(s2,2))*Power(t2,3) + 
               2*s2*Power(t2,4) + Power(t2,5) + 
               Power(t1,2)*(16 - (8 + s2)*t2 + 6*Power(t2,3)) + 
               t1*(-32 + (-46 + 24*s2)*t2 - (-11 + s2)*Power(t2,2) - 
                  (17 + 7*s2)*Power(t2,3) + 5*Power(t2,4))) + 
            s1*Power(t2,2)*(12 + (10 - 34*s2)*t2 + 
               (83 + 31*s2)*Power(t2,2) + 
               2*(-52 + 13*s2 + 2*Power(s2,2))*Power(t2,3) + 
               2*s2*Power(t2,4) + 3*Power(t2,5) + 
               Power(t1,3)*(3 + 7*t2) + 
               Power(t1,2)*(6 + (-78 + s2)*t2 + 
                  (19 + 2*s2)*Power(t2,2) + 16*Power(t2,3)) + 
               t1*(-18 + (70 + 36*s2)*t2 - (11 + 21*s2)*Power(t2,2) - 
                  (71 + 12*s2)*Power(t2,3) + 18*Power(t2,4))) + 
            Power(s1,3)*(-(Power(t1,3)*(-1 + t2)) + 
               t1*t2*(12 - (4 + 3*s2)*t2 - (55 + 4*s2)*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t2*(-12 + (49 + 11*s2)*t2 + 
                  (-65 + 32*s2 + 4*Power(s2,2))*Power(t2,2) + 
                  (20 - 19*s2)*Power(t2,3) + 10*Power(t2,4)) + 
               Power(t1,2)*t2*(s2 + t2*(7 + 4*t2))) - 
            Power(s1,2)*t2*(Power(t1,3)*(3 - 3*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*t2*
                (-30 + 37*t2 + 10*Power(t2,2) + s2*(3 + 2*t2)) + 
               t1*t2*(72 - 40*t2 - 97*Power(t2,2) + 16*Power(t2,3) + 
                  s2*(12 - 25*t2 - 6*Power(t2,2))) + 
               t2*(-36 + 111*t2 - 111*Power(t2,2) + 
                  6*Power(s2,2)*Power(t2,2) + 16*Power(t2,3) + 
                  6*Power(t2,4) + 
                  s2*(-12 + 37*t2 + 36*Power(t2,2) - 9*Power(t2,3))))) + 
         Power(s,4)*(-(Power(s1,6)*(-1 + t2)*
               (s2 - 3*s2*t2 + (-1 + t2)*t2 + t1*(-2 + 4*t2))) - 
            Power(t2,3)*(-28 + 2*(-62 + 23*s2)*t2 + 
               (106 - 11*s2 - 4*Power(s2,2))*Power(t2,2) + 
               (92 - 16*s2 - 7*Power(s2,2))*Power(t2,3) - 
               (32 + 14*s2 + 3*Power(s2,2))*Power(t2,4) + 
               (-10 + s2)*Power(t2,5) + 2*Power(t2,6) + 
               Power(t1,3)*(2 - 6*t2 + Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-27 + (36 + 5*s2)*t2 + 7*Power(t2,2) - 
                  2*(9 + s2)*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(54 + (105 - 57*s2)*t2 + 
                  (-141 + 15*s2 + Power(s2,2))*Power(t2,2) + 
                  (22 + 35*s2 + Power(s2,2))*Power(t2,3) - 
                  5*Power(t2,4) + 2*Power(t2,5))) + 
            Power(s1,2)*t2*(12 + (42 - 66*s2)*t2 + 
               (-29 + 115*s2 + 8*Power(s2,2))*Power(t2,2) + 
               (-274 + 8*s2 + 45*Power(s2,2))*Power(t2,3) + 
               (160 + 25*s2 + 17*Power(s2,2))*Power(t2,4) - 
               4*(-6 + s2)*Power(t2,5) - 9*Power(t2,6) + 
               Power(t1,3)*(6 - 9*t2 - 20*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(-3 + 3*(-73 + 7*s2)*t2 + 
                  (217 + s2)*Power(t2,2) + (27 - 6*s2)*Power(t2,3) - 
                  36*Power(t2,4)) - 
               t1*(18 - 3*(41 + 25*s2)*t2 - 
                  7*(-19 + s2)*s2*Power(t2,2) + 
                  (407 + 24*s2 + Power(s2,2))*Power(t2,3) - 
                  4*(51 + 2*s2)*Power(t2,4) + 2*Power(t2,5))) + 
            s1*Power(t2,2)*(-42 + 4*(-46 + 25*s2)*t2 + 
               (164 - 71*s2 - 10*Power(s2,2))*Power(t2,2) + 
               (258 - 20*s2 - 29*Power(s2,2))*Power(t2,3) - 
               (110 + 41*s2 + 12*Power(s2,2))*Power(t2,4) + 
               (-35 + 4*s2)*Power(t2,5) + 7*Power(t2,6) + 
               Power(t1,3)*t2*(-11 + 20*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-33 + (193 - 3*s2)*t2 - 
                  (119 + 5*s2)*Power(t2,2) - 58*Power(t2,3) + 
                  20*Power(t2,4)) + 
               t1*(78 + (45 - 119*s2)*t2 - 
                  (174 - 90*s2 + Power(s2,2))*Power(t2,2) + 
                  (198 + 64*s2 + 3*Power(s2,2))*Power(t2,3) - 
                  (76 + s2)*Power(t2,4) + 7*Power(t2,5))) + 
            Power(s1,4)*(-4 + (17 + 10*s2)*t2 + 
               3*(-20 + 13*s2 + 4*Power(s2,2))*Power(t2,2) + 
               (71 - 51*s2)*Power(t2,3) + 14*(-2 + s2)*Power(t2,4) - 
               4*Power(t2,5) + Power(t1,3)*(-7 + 5*t2) + 
               t1*(4 + (3 - 4*s2 + 2*Power(s2,2))*t2 + 
                  (-131 - 5*s2 + 2*Power(s2,2))*Power(t2,2) + 
                  3*(43 + 4*s2)*Power(t2,3) - 16*Power(t2,4)) - 
               Power(t1,2)*(3 - 12*t2 + 15*Power(t2,2) + 
                  19*Power(t2,3) + s2*(-4 + 5*t2))) + 
            Power(s1,5)*(-3 + (16 + 21*t1 - Power(t1,2))*t2 + 
               Power(s2,2)*(-2 + t2)*t2 + 
               (-17 - 38*t1 + 6*Power(t1,2))*Power(t2,2) + 
               (3 + 17*t1)*Power(t2,3) + 3*Power(t2,4) - 
               s2*(t1*(1 - 2*t2 + 3*Power(t2,2)) + 
                  t2*(13 - 24*t2 + 11*Power(t2,2)))) - 
            Power(s1,3)*(Power(t1,3)*
                (2 - 27*t2 + 10*Power(t2,2) + 7*Power(t2,3)) - 
               Power(t1,2)*(3 + (47 - 17*s2)*t2 + 
                  (-85 + 9*s2)*Power(t2,2) + (35 + 4*s2)*Power(t2,3) + 
                  32*Power(t2,4)) + 
               t1*t2*(49 - 12*t2 - 337*Power(t2,2) + 230*Power(t2,3) + 
                  Power(s2,2)*t2*(7 + 3*t2) + 
                  s2*(13 - 63*t2 + 2*Power(t2,2) + 16*Power(t2,3))) + 
               t2*(-16 + 37*t2 - 154*Power(t2,2) + 135*Power(t2,3) - 
                  24*Power(t2,4) - 6*Power(t2,5) + 
                  Power(s2,2)*t2*(2 + 33*t2 + 9*Power(t2,2)) + 
                  s2*(-12 + 65*t2 + 31*Power(t2,2) - 33*Power(t2,3) + 
                     5*Power(t2,4))))) - 
         Power(s,3)*(s1 - t2)*
          (Power(s1,6)*(s2 - t1)*Power(-1 + t2,2) - 
            Power(t2,2)*(16 + (121 - 48*s2)*t2 + 
               2*(-85 - 11*s2 + 7*Power(s2,2))*Power(t2,2) + 
               2*(-10 - 2*s2 + 2*Power(s2,2) + Power(s2,3))*
                Power(t2,3) + 
               (56 - 2*s2 + 12*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
               (5 - 5*s2 - 2*Power(s2,2))*Power(t2,5) - 
               (9 + s2)*Power(t2,6) + Power(t2,7) + 
               Power(t1,3)*t2*(5 - 5*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*(11 - (22 + 9*s2)*t2 + 
                  (8 + 6*s2)*Power(t2,2) + 2*(12 + 5*s2)*Power(t2,3) + 
                  (-5 + s2)*Power(t2,4) + Power(t2,5)) + 
               t1*(-27 + 2*(-51 + 28*s2)*t2 + 
                  (251 - 45*s2 - 3*Power(s2,2))*Power(t2,2) - 
                  (28 + 65*s2 + 3*Power(s2,2))*Power(t2,3) + 
                  (-9 + 8*s2 - 2*Power(s2,2))*Power(t2,4) + 
                  (-3 + s2)*Power(t2,5))) + 
            s1*t2*(26 + (184 - 88*s2)*t2 + 
               (-299 - 29*s2 + 33*Power(s2,2))*Power(t2,2) + 
               (-24 - 105*s2 + 25*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,3) + 
               (128 + 12*s2 + 41*Power(s2,2) + 5*Power(s2,3))*
                Power(t2,4) + 
               (17 - 9*s2 - 10*Power(s2,2))*Power(t2,5) - 
               5*(7 + s2)*Power(t2,6) + 3*Power(t2,7) + 
               Power(t1,3)*(-6 + 13*t2 - 13*Power(t2,2) + 
                  7*Power(t2,3) + 3*Power(t2,4)) + 
               Power(t1,2)*(28 - 2*(64 + s2)*t2 - 
                  2*(-96 + 5*s2)*Power(t2,2) + 
                  (37 + 8*s2)*Power(t2,3) + (-41 + 4*s2)*Power(t2,4) + 
                  5*Power(t2,5)) + 
               t1*(-48 + (-63 + 87*s2)*t2 + 
                  (407 - 168*s2 + 5*Power(s2,2))*Power(t2,2) - 
                  2*(75 + 46*s2 + 8*Power(s2,2))*Power(t2,3) + 
                  (104 - 13*s2 - 5*Power(s2,2))*Power(t2,4) + 
                  (-26 + 4*s2)*Power(t2,5))) + 
            Power(s1,2)*(-4 + (-53 + 44*s2)*t2 + 
               (125 - 17*s2 - 24*Power(s2,2))*Power(t2,2) - 
               (23 - 190*s2 + 46*Power(s2,2) + 9*Power(s2,3))*
                Power(t2,3) + 
               (-80 + s2 - 57*Power(s2,2) - 9*Power(s2,3))*
                Power(t2,4) + 
               (-1 - 8*s2 + 19*Power(s2,2))*Power(t2,5) + 
               (39 + 8*s2)*Power(t2,6) - 3*Power(t2,7) + 
               Power(t1,3)*t2*(-11 + 8*t2 - 21*Power(t2,2)) + 
               Power(t1,2)*(1 + (142 - 11*s2)*t2 + 
                  (-371 + 51*s2)*Power(t2,2) - 
                  (-30 + s2)*Power(t2,3) + (56 - 5*s2)*Power(t2,4) - 
                  12*Power(t2,5)) + 
               t1*(3 - 6*(14 + 5*s2)*t2 + 
                  (-119 + 221*s2 - 24*Power(s2,2))*Power(t2,2) + 
                  (255 + 20*s2 + 24*Power(s2,2))*Power(t2,3) + 
                  (-340 + 50*s2 + 8*Power(s2,2))*Power(t2,4) + 
                  (60 + s2)*Power(t2,5) + 7*Power(t2,6))) + 
            Power(s1,5)*(-3 + 7*t2 - 7*Power(t2,2) + 3*Power(t2,3) + 
               Power(s2,2)*(1 + t2 - Power(t2,2)) + 
               Power(t1,2)*(-2 + 3*t2 + 4*Power(t2,2)) + 
               t1*(-8 + 27*t2 - 20*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(3 - 10*t2 + 5*Power(t2,2) - 2*Power(t2,3) - 
                  t1*(-1 + 6*t2 + Power(t2,2)))) - 
            Power(s1,4)*(-3 + Power(t1,3)*(17 - 10*t2) + t2 + 
               18*Power(t2,2) - 26*Power(t2,3) + 10*Power(t2,4) + 
               Power(s2,3)*t2*(1 + 2*t2) + 
               t1*(4 - 55*t2 + 138*Power(t2,2) - 63*Power(t2,3)) + 
               Power(t1,2)*(17 - 24*t2 + 17*Power(t2,2) + 
                  19*Power(t2,3)) + 
               Power(s2,2)*(t2*(10 + 13*t2 - 7*Power(t2,2)) - 
                  t1*(-5 + t2 + 4*Power(t2,2))) + 
               s2*(3 + 8*t2 - 48*Power(t2,2) + 15*Power(t2,3) - 
                  2*Power(t2,4) + Power(t1,2)*(-19 + 9*t2) - 
                  t1*(8 + 17*t2 + 12*Power(t2,2) + 8*Power(t2,3)))) + 
            Power(s1,3)*(Power(s2,3)*Power(t2,2)*(5 + 7*t2) - 
               Power(t1,3)*(7 - 49*t2 + 9*Power(t2,2) + 9*Power(t2,3)) + 
               Power(t1,2)*(-6 + 108*t2 - 35*Power(t2,2) + 
                  12*Power(t2,3) + 23*Power(t2,4)) + 
               t1*(15 + 63*t2 - 210*Power(t2,2) + 321*Power(t2,3) - 
                  82*Power(t2,4) - 11*Power(t2,5)) + 
               t2*(-31 + 41*t2 + 25*Power(t2,2) - 30*Power(t2,3) - 
                  6*Power(t2,4) + Power(t2,5)) + 
               Power(s2,2)*t2*
                (5 + 34*t2 + 40*Power(t2,2) - 17*Power(t2,3) - 
                  3*t1*(-7 + 4*t2 + 3*Power(t2,2))) + 
               s2*(-4 + 27*t2 - 84*Power(t2,2) - 54*Power(t2,3) + 
                  24*Power(t2,4) - 5*Power(t2,5) + 
                  2*Power(t1,2)*
                   (2 - 27*t2 + 6*Power(t2,2) + Power(t2,3)) - 
                  t1*(1 + 106*t2 + 11*Power(t2,2) + 35*Power(t2,3) + 
                     11*Power(t2,4))))) - 
         s*Power(s1 - t2,3)*(1 - 3*t1 + 3*Power(t1,2) - Power(t1,3) + 
            2*Power(s1,5)*Power(s2 - t1,2)*(-1 + t2) - 10*t2 + 2*s2*t2 + 
            26*t1*t2 - 4*s2*t1*t2 - 22*Power(t1,2)*t2 + 
            2*s2*Power(t1,2)*t2 + 6*Power(t1,3)*t2 + 19*Power(t2,2) - 
            3*s2*Power(t2,2) - Power(s2,2)*Power(t2,2) - 
            45*t1*Power(t2,2) + 15*s2*t1*Power(t2,2) + 
            Power(s2,2)*t1*Power(t2,2) + 31*Power(t1,2)*Power(t2,2) - 
            12*s2*Power(t1,2)*Power(t2,2) - 5*Power(t1,3)*Power(t2,2) - 
            7*Power(t2,3) - 11*s2*Power(t2,3) + 
            13*Power(s2,2)*Power(t2,3) - 2*Power(s2,3)*Power(t2,3) + 
            17*t1*Power(t2,3) - 20*s2*t1*Power(t2,3) + 
            2*Power(s2,2)*t1*Power(t2,3) - 2*Power(t1,2)*Power(t2,3) + 
            13*s2*Power(t1,2)*Power(t2,3) - 3*Power(t1,3)*Power(t2,3) - 
            8*Power(t2,4) + 23*s2*Power(t2,4) - 
            17*Power(s2,2)*Power(t2,4) + 4*Power(s2,3)*Power(t2,4) + 
            12*t1*Power(t2,4) - s2*t1*Power(t2,4) - 
            9*Power(s2,2)*t1*Power(t2,4) - 10*Power(t1,2)*Power(t2,4) + 
            6*s2*Power(t1,2)*Power(t2,4) + 5*Power(t2,5) - 
            11*s2*Power(t2,5) + 5*Power(s2,2)*Power(t2,5) + 
            Power(s2,3)*Power(t2,5) - 7*t1*Power(t2,5) + 
            10*s2*t1*Power(t2,5) - 3*Power(s2,2)*t1*Power(t2,5) + 
            Power(s1,4)*(Power(s2,3)*(1 + 2*t2) + 
               Power(s2,2)*(2 + t1*(-10 + t2) - t2 - Power(t2,2)) + 
               t1*(Power(-1 + t2,2)*t2 + Power(t1,2)*(-8 + 5*t2) - 
                  t1*(4 - 10*t2 + 5*Power(t2,2) + Power(t2,3))) - 
               s2*(Power(-1 + t2,2)*t2 + Power(t1,2)*(-17 + 8*t2) - 
                  t1*(2 - 9*t2 + 6*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,3)*(-(Power(-1 + t2,3)*(2 + t2)) + 
               t1*Power(-1 + t2,2)*(-7 + 5*t2) + 
               Power(s2,3)*(2 - 7*t2 - 7*Power(t2,2)) - 
               Power(t1,3)*(-11 - 3*t2 + Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-11 + 25*t2 - 19*Power(t2,2) + 
                  4*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(-18 + 23*t2 + 5*Power(t2,2) - 
                  10*Power(t2,3) + 
                  t1*(3 + 27*t2 + 5*Power(t2,2) + Power(t2,3))) + 
               s2*(Power(-1 + t2,2)*(-3 + 3*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*(-16 - 23*t2 + 3*Power(t2,2)) + 
                  t1*(37 - 71*t2 + 35*Power(t2,2) + Power(t2,3) - 
                     2*Power(t2,4)))) + 
            s1*(Power(s2,3)*Power(t2,2)*(6 - 13*t2 - 5*Power(t2,2)) + 
               Power(-1 + t2,3)*(-7 - 4*t2 + 3*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*
                (17 - 5*t2 + 2*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(-3 + t2 + 9*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(13 - 23*t2 - Power(t2,2) + 4*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s2,2)*t2*
                (2 - 44*t2 + 61*Power(t2,2) - 13*Power(t2,3) - 
                  6*Power(t2,4) + 
                  t1*(-2 - t2 + 25*Power(t2,2) + 13*Power(t2,3) + 
                     Power(t2,4))) - 
               s2*(-(Power(-1 + t2,2)*
                     (-2 - 11*t2 + 26*Power(t2,2) + 3*Power(t2,3))) + 
                  Power(t1,2)*
                   (2 - 11*t2 + 15*Power(t2,2) + 24*Power(t2,3) + 
                     6*Power(t2,4)) - 
                  t1*(4 - 4*t2 + 23*Power(t2,2) - 19*Power(t2,3) - 
                     5*Power(t2,4) + Power(t2,5)))) + 
            Power(s1,2)*(Power(-1 + t2,4)*(1 + t2) + 
               3*Power(s2,3)*t2*(-2 + 5*t2 + 3*Power(t2,2)) - 
               Power(t1,3)*(-1 + 15*t2 + 3*Power(t2,2) + Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (3 - 19*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(1 + 8*t2 - 6*Power(t2,2) - 5*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,2)*(-1 + 49*t2 - 69*Power(t2,2) + 
                  6*Power(t2,3) + 15*Power(t2,4) - 
                  t1*(-1 + 4*t2 + 33*Power(t2,2) + 16*Power(t2,3) + 
                     2*Power(t2,4))) + 
               s2*(-(Power(-1 + t2,2)*
                     (-10 + 12*t2 + 6*Power(t2,2) + Power(t2,3))) + 
                  Power(t1,2)*
                   (1 + 18*t2 + 24*Power(t2,2) + 11*Power(t2,3)) + 
                  t1*(-11 - 40*t2 + 89*Power(t2,2) - 35*Power(t2,3) - 
                     4*Power(t2,4) + Power(t2,5))))) - 
         Power(s,2)*Power(s1 - t2,2)*
          (-(t2*(-1 + (54 - 16*s2)*t2 + 
                 (-90 - 9*s2 + 5*Power(s2,2))*Power(t2,2) + 
                 2*(9 + 19*s2 - 19*Power(s2,2) + 3*Power(s2,3))*
                  Power(t2,3) + 
                 (21 - 16*s2 + 8*Power(s2,2) + Power(s2,3))*
                  Power(t2,4) + (1 + 6*s2 - 9*Power(s2,2))*Power(t2,5) + 
                 (-4 - 4*s2 + Power(s2,2))*Power(t2,6) + 
                 (1 + s2)*Power(t2,7) + 
                 Power(t1,3)*(2 - 5*t2 + Power(t2,3)) + 
                 Power(t1,2)*
                  (-5 + (27 - 7*s2)*t2 + 3*(-9 + 5*s2)*Power(t2,2) + 
                    (-23 + 4*s2)*Power(t2,3) - (6 + s2)*Power(t2,4) + 
                    Power(t2,5)) + 
                 t1*(4 + (-76 + 23*s2)*t2 + 
                    (144 - 23*s2 - 3*Power(s2,2))*Power(t2,2) + 
                    (-42 + 15*s2 - 7*Power(s2,2))*Power(t2,3) + 
                    (-33 + 50*s2 - 6*Power(s2,2))*Power(t2,4) + 
                    (1 + 2*s2)*Power(t2,5) - (-2 + s2)*Power(t2,6)))) + 
            s1*(5 + (70 - 25*s2)*t2 + 
               (-155 - 36*s2 + 10*Power(s2,2))*Power(t2,2) + 
               (67 + 46*s2 - 105*Power(s2,2) + 20*Power(s2,3))*
                Power(t2,3) + 
               (15 + 24*s2 + 17*Power(s2,2) + 6*Power(s2,3))*
                Power(t2,4) + 
               (11 + 16*s2 - 32*Power(s2,2) - 2*Power(s2,3))*
                Power(t2,5) + 
               (-15 - 28*s2 + 2*Power(s2,2))*Power(t2,6) + 
               (2 + 3*s2)*Power(t2,7) - 
               Power(t1,3)*(4 - 7*t2 + 6*Power(t2,2) + 11*Power(t2,3)) - 
               Power(t1,2)*(-13 + (18 + 7*s2)*t2 + 
                  (4 - 13*s2)*Power(t2,2) + (53 - 40*s2)*Power(t2,3) - 
                  9*(-5 + s2)*Power(t2,4) + 3*s2*Power(t2,5) + 
                  Power(t2,6)) + 
               t1*(-14 + (-59 + 32*s2)*t2 + 
                  (246 - 28*s2 - 4*Power(s2,2))*Power(t2,2) + 
                  (-168 + 113*s2 - 43*Power(s2,2))*Power(t2,3) + 
                  (-11 + 65*s2 - 16*Power(s2,2))*Power(t2,4) + 
                  (-8 + 33*s2 + Power(s2,2))*Power(t2,5) + 
                  (13 + s2)*Power(t2,6) + Power(t2,7))) + 
            Power(s1,5)*(Power(s2,2)*(2 - 5*t2 + Power(t2,2)) + 
               2*s2*(-1 + Power(t2,2) + t1*(-3 + 5*t2)) + 
               t1*(2*(3 - 4*t2 + Power(t2,2)) - 
                  t1*(-4 + 5*t2 + Power(t2,2)))) + 
            Power(s1,4)*(-2*Power(t1,3)*(-9 + 5*t2) + 
               Power(-1 + t2,2)*(-4 - 2*t2 + Power(t2,2)) + 
               Power(s2,3)*(-2 - 3*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(16 - 22*t2 + 13*Power(t2,2) + 
                  8*Power(t2,3)) - 
               t1*(3 + 17*t2 - 31*Power(t2,2) + 10*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(t1*(13 + 3*t2 - 2*Power(t2,2)) + 
                  t2*(-4 + 21*t2 - 2*Power(t2,2))) + 
               s2*(1 + 18*t2 - 28*Power(t2,2) + 9*Power(t2,3) + 
                  Power(t1,2)*(-29 + 10*t2) - 
                  3*t1*t2*(1 + 8*t2 + Power(t2,2)))) + 
            Power(s1,3)*(2*Power(s2,3)*t2*(6 + 5*t2 - 3*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (9 - 27*t2 - 5*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(-2 - 33*t2 + 4*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(8 - 65*t2 + 12*Power(t2,2) - 
                  15*Power(t2,3) - 6*Power(t2,4)) + 
               t1*(30 - 74*t2 + 93*Power(t2,2) - 59*Power(t2,3) + 
                  7*Power(t2,4) + 3*Power(t2,5)) + 
               Power(s2,2)*(t2*
                   (-29 + 3*t2 - 41*Power(t2,2) + Power(t2,3)) + 
                  t1*(2 - 55*t2 - 10*Power(t2,2) + 5*Power(t2,3))) + 
               s2*(4 - 52*t2 + 28*Power(t2,2) + 63*Power(t2,3) - 
                  44*Power(t2,4) + Power(t2,5) + 
                  Power(t1,2)*(5 + 70*t2 - 7*Power(t2,2)) + 
                  t1*(-26 + 123*t2 - 15*Power(t2,2) + 41*Power(t2,3) + 
                     9*Power(t2,4)))) + 
            Power(s1,2)*(6*Power(s2,3)*Power(t2,2)*
                (-4 - 2*t2 + Power(t2,2)) + 
               2*Power(-1 + t2,2)*
                (-8 + 6*t2 - 10*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(45 - 121*t2 + 169*Power(t2,2) + 
                  45*Power(t2,3) - 10*Power(t2,4)) + 
               Power(t1,3)*(-12 + 38*t2 - 3*Power(t2,2) + 
                  6*Power(t2,3) + Power(t2,4)) - 
               t1*(17 + 42*t2 - 113*Power(t2,2) + 104*Power(t2,3) - 
                  63*Power(t2,4) + 10*Power(t2,5) + 3*Power(t2,6)) - 
               Power(s2,2)*t2*
                (5 - 96*t2 + 10*Power(t2,2) - 48*Power(t2,3) + 
                  Power(t2,4) + 
                  t1*(1 - 78*t2 - 17*Power(t2,2) + 4*Power(t2,3))) + 
               s2*(9 + 23*t2 + 43*Power(t2,2) - 84*Power(t2,3) - 
                  45*Power(t2,4) + 57*Power(t2,5) - 3*Power(t2,6) + 
                  Power(t1,2)*t2*
                   (-3 - 77*t2 - 13*Power(t2,2) + 3*Power(t2,3)) - 
                  t1*(9 - 31*t2 + 221*Power(t2,2) - 9*Power(t2,3) + 
                     58*Power(t2,4) + 8*Power(t2,5))))))*T3q(t2,s1))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*(-1 + t1)*Power(s1 - t2,2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) + 
    (8*((-1 + s1)*(s1 - t2)*Power(-1 + t2,4)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         Power(s,7)*(-1 + t2)*Power(t2,2)*
          (t2*(-7 - 2*t2 + Power(t2,2)) + t1*(1 + 6*t2 + Power(t2,2))) - 
         Power(s,6)*t2*(s1*(-1 + t2)*
             (2*t1*(-1 - 8*t2 + 11*Power(t2,2) + 2*Power(t2,3)) + 
               t2*(22 - s2*Power(-1 + t2,2) - 25*t2 - 8*Power(t2,2) + 
                  3*Power(t2,3))) + 
            t2*(1 + (23 - 2*s2)*t2 + 3*(-17 + 8*s2)*Power(t2,2) + 
               (23 - 12*s2)*Power(t2,3) + 2*(2 + s2)*Power(t2,4) - 
               Power(t1,2)*(-1 - 13*t2 + Power(t2,2) + Power(t2,3)) + 
               t1*(-6 + t2 - Power(t2,2) + Power(t2,3) - 7*Power(t2,4) + 
                  s2*(-1 - 15*t2 + 3*Power(t2,2) + Power(t2,3))))) + 
         Power(s,5)*(Power(t1,3)*
             (-1 + 4*t2 - 6*Power(t2,2) - 10*Power(t2,3) + Power(t2,4)) \
+ t1*Power(t2,2)*(-11 + 62*t2 - 117*Power(t2,2) + 75*Power(t2,3) - 
               14*Power(t2,4) + 5*Power(t2,5) - 
               2*Power(s2,2)*t2*(5 + t2) + 
               s2*(-3 - 60*t2 + 16*Power(t2,2) + 6*Power(t2,3) - 
                  7*Power(t2,4))) + 
            Power(t1,2)*t2*(s2*
                (-1 + 3*t2 + 21*Power(t2,2) + Power(t2,3)) + 
               2*t2*(1 + 25*t2 - 11*Power(t2,2) - 6*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(t2,2)*(Power(s2,2)*t2*
                (-1 + 31*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               Power(-1 + t2,2)*
                (5 + 34*t2 - 61*Power(t2,2) - Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(1 - 20*t2 + 84*Power(t2,2) - 88*Power(t2,3) + 
                  21*Power(t2,4) + 2*Power(t2,5))) + 
            Power(s1,2)*Power(-1 + t2,2)*
             (t1*(-1 - 17*t2 + 36*Power(t2,2) + 6*Power(t2,3)) + 
               t2*(23 - 43*t2 - 7*Power(t2,2) + 3*Power(t2,3) + 
                  s2*(-2 + 5*t2 - 3*Power(t2,2)))) + 
            s1*t2*(-2 + (-53 + 9*s2 + Power(s2,2))*t2 - 
               (-211 + 97*s2 + Power(s2,2))*Power(t2,2) + 
               (-249 + 121*s2 + 13*Power(s2,2))*Power(t2,3) - 
               (-87 + 37*s2 + Power(s2,2))*Power(t2,4) + 
               (6 + 4*s2)*Power(t2,5) + 
               Power(t1,2)*(-1 - 41*t2 + 57*Power(t2,2) + Power(t2,3) - 
                  4*Power(t2,4)) + 
               t1*(10 - 18*t2 + 33*Power(t2,2) - 23*Power(t2,3) + 
                  19*Power(t2,4) - 21*Power(t2,5) + 
                  s2*(2 + 45*t2 - 71*Power(t2,2) - 3*Power(t2,3) + 
                     3*Power(t2,4))))) + 
         Power(s,4)*(Power(t1,3)*
             (2 - 12*t2 + 25*Power(t2,2) - 2*Power(t2,3) - 
               24*Power(t2,4) - 2*Power(t2,5) + Power(t2,6)) + 
            Power(t1,2)*(-3 + t2 + 5*s2*t2 + (16 - 15*s2)*Power(t2,2) - 
               (70 + 23*s2)*Power(t2,3) + (46 + 59*s2)*Power(t2,4) + 
               4*(7 + 3*s2)*Power(t2,5) - (21 + 2*s2)*Power(t2,6) + 
               3*Power(t2,7)) + 
            Power(s1,3)*Power(-1 + t2,3)*
             (-8 + 31*t2 + 2*Power(t2,2) - Power(t2,3) + 
               t1*(6 - 26*t2 - 4*Power(t2,2)) + 
               s2*(1 - 4*t2 + 3*Power(t2,2))) + 
            Power(t2,2)*(2*Power(s2,3)*t2*(-2 + 7*t2 + Power(t2,2)) + 
               2*Power(-1 + t2,3)*
                (4 + 20*t2 - 24*Power(t2,2) - 5*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*Power(-1 + t2,2)*
                (-1 + 37*t2 - 32*Power(t2,2) + 3*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(2 + 39*t2 - 142*Power(t2,2) + 
                  114*Power(t2,3) - 10*Power(t2,4) - 3*Power(t2,5))) + 
            t1*t2*(Power(s2,2)*t2*
                (1 + 18*t2 - 42*Power(t2,2) - 14*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(-1 + t2,2)*
                (-3 + 6*t2 - 91*Power(t2,2) + 75*Power(t2,3) + 
                  3*Power(t2,4) + 2*Power(t2,5)) + 
               s2*(1 - 12*t2 + 57*Power(t2,2) + 7*Power(t2,3) - 
                  58*Power(t2,4) + 5*Power(t2,5))) + 
            Power(s1,2)*(-1 + t2)*
             (1 - 2*(-19 + 5*s2 + Power(s2,2))*t2 - 
               (195 - 122*s2 + Power(s2,2))*Power(t2,2) + 
               (271 - 138*s2 - 38*Power(s2,2))*Power(t2,3) + 
               (-114 + 28*s2 + Power(s2,2))*Power(t2,4) - 
               (1 + 2*s2)*Power(t2,5) + 
               Power(t1,2)*t2*
                (47 - 88*t2 - 5*Power(t2,2) + 6*Power(t2,3)) - 
               t1*(4 - 23*t2 + 84*Power(t2,2) - 58*Power(t2,3) + 
                  14*Power(t2,4) - 21*Power(t2,5) + 
                  s2*(1 + 50*t2 - 106*Power(t2,2) - 28*Power(t2,3) + 
                     3*Power(t2,4)))) - 
            s1*(Power(t1,3)*(-7 + 26*t2 + 6*Power(t2,2) - 
                  42*Power(t2,3) + 5*Power(t2,4)) + 
               t2*(2*Power(s2,3)*Power(t2,2)*(1 + 5*t2) + 
                  Power(s2,2)*t2*
                   (6 - 89*t2 + 115*Power(t2,2) - 29*Power(t2,3) - 
                     3*Power(t2,4)) + 
                  Power(-1 + t2,3)*
                   (8 + 60*t2 - 148*Power(t2,2) - 3*Power(t2,3) + 
                     Power(t2,4)) + 
                  s2*Power(-1 + t2,2)*
                   (-2 + 50*t2 - 159*Power(t2,2) + 36*Power(t2,3) + 
                     Power(t2,4))) + 
               Power(t1,2)*(-3 + 9*t2 - 135*Power(t2,2) + 
                  192*Power(t2,3) - 25*Power(t2,4) - 55*Power(t2,5) + 
                  17*Power(t2,6) + 
                  s2*(4 - 17*t2 - 45*Power(t2,2) + 87*Power(t2,3) + 
                     7*Power(t2,4))) + 
               t1*t2*(-2*Power(s2,2)*
                   (-1 - 13*t2 + 22*Power(t2,2) + 9*Power(t2,3) + 
                     Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (7 - 123*t2 + 183*Power(t2,2) - 4*Power(t2,3) + 
                     11*Power(t2,4)) + 
                  s2*(5 + 136*t2 - 179*Power(t2,2) + Power(t2,3) + 
                     50*Power(t2,4) - 13*Power(t2,5))))) + 
         s*Power(-1 + t2,3)*(1 - 3*t1 + 3*Power(t1,2) - Power(t1,3) + 
            2*Power(s1,5)*Power(s2 - t1,2)*(-1 + t2) - 10*t2 + 2*s2*t2 + 
            26*t1*t2 - 4*s2*t1*t2 - 22*Power(t1,2)*t2 + 
            2*s2*Power(t1,2)*t2 + 6*Power(t1,3)*t2 + 19*Power(t2,2) - 
            3*s2*Power(t2,2) - Power(s2,2)*Power(t2,2) - 
            45*t1*Power(t2,2) + 15*s2*t1*Power(t2,2) + 
            Power(s2,2)*t1*Power(t2,2) + 31*Power(t1,2)*Power(t2,2) - 
            12*s2*Power(t1,2)*Power(t2,2) - 5*Power(t1,3)*Power(t2,2) - 
            7*Power(t2,3) - 11*s2*Power(t2,3) + 
            13*Power(s2,2)*Power(t2,3) - 2*Power(s2,3)*Power(t2,3) + 
            17*t1*Power(t2,3) - 20*s2*t1*Power(t2,3) + 
            2*Power(s2,2)*t1*Power(t2,3) - 2*Power(t1,2)*Power(t2,3) + 
            13*s2*Power(t1,2)*Power(t2,3) - 3*Power(t1,3)*Power(t2,3) - 
            8*Power(t2,4) + 23*s2*Power(t2,4) - 
            17*Power(s2,2)*Power(t2,4) + 4*Power(s2,3)*Power(t2,4) + 
            12*t1*Power(t2,4) - s2*t1*Power(t2,4) - 
            9*Power(s2,2)*t1*Power(t2,4) - 10*Power(t1,2)*Power(t2,4) + 
            6*s2*Power(t1,2)*Power(t2,4) + 5*Power(t2,5) - 
            11*s2*Power(t2,5) + 5*Power(s2,2)*Power(t2,5) + 
            Power(s2,3)*Power(t2,5) - 7*t1*Power(t2,5) + 
            10*s2*t1*Power(t2,5) - 3*Power(s2,2)*t1*Power(t2,5) + 
            Power(s1,4)*(Power(s2,3)*(1 + 2*t2) + 
               Power(s2,2)*(2 + t1*(-10 + t2) - t2 - Power(t2,2)) + 
               t1*(Power(-1 + t2,2)*t2 + Power(t1,2)*(-8 + 5*t2) - 
                  t1*(4 - 10*t2 + 5*Power(t2,2) + Power(t2,3))) - 
               s2*(Power(-1 + t2,2)*t2 + Power(t1,2)*(-17 + 8*t2) - 
                  t1*(2 - 9*t2 + 6*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,3)*(-(Power(-1 + t2,3)*(2 + t2)) + 
               t1*Power(-1 + t2,2)*(-7 + 5*t2) + 
               Power(s2,3)*(2 - 7*t2 - 7*Power(t2,2)) - 
               Power(t1,3)*(-11 - 3*t2 + Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-11 + 25*t2 - 19*Power(t2,2) + 
                  4*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(-18 + 23*t2 + 5*Power(t2,2) - 
                  10*Power(t2,3) + 
                  t1*(3 + 27*t2 + 5*Power(t2,2) + Power(t2,3))) + 
               s2*(Power(-1 + t2,2)*(-3 + 3*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*(-16 - 23*t2 + 3*Power(t2,2)) + 
                  t1*(37 - 71*t2 + 35*Power(t2,2) + Power(t2,3) - 
                     2*Power(t2,4)))) + 
            s1*(Power(s2,3)*Power(t2,2)*(6 - 13*t2 - 5*Power(t2,2)) + 
               Power(-1 + t2,3)*(-7 - 4*t2 + 3*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*
                (17 - 5*t2 + 2*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(-3 + t2 + 9*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(13 - 23*t2 - Power(t2,2) + 4*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s2,2)*t2*
                (2 - 44*t2 + 61*Power(t2,2) - 13*Power(t2,3) - 
                  6*Power(t2,4) + 
                  t1*(-2 - t2 + 25*Power(t2,2) + 13*Power(t2,3) + 
                     Power(t2,4))) - 
               s2*(-(Power(-1 + t2,2)*
                     (-2 - 11*t2 + 26*Power(t2,2) + 3*Power(t2,3))) + 
                  Power(t1,2)*
                   (2 - 11*t2 + 15*Power(t2,2) + 24*Power(t2,3) + 
                     6*Power(t2,4)) - 
                  t1*(4 - 4*t2 + 23*Power(t2,2) - 19*Power(t2,3) - 
                     5*Power(t2,4) + Power(t2,5)))) + 
            Power(s1,2)*(Power(-1 + t2,4)*(1 + t2) + 
               3*Power(s2,3)*t2*(-2 + 5*t2 + 3*Power(t2,2)) - 
               Power(t1,3)*(-1 + 15*t2 + 3*Power(t2,2) + Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (3 - 19*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(1 + 8*t2 - 6*Power(t2,2) - 5*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,2)*(-1 + 49*t2 - 69*Power(t2,2) + 
                  6*Power(t2,3) + 15*Power(t2,4) - 
                  t1*(-1 + 4*t2 + 33*Power(t2,2) + 16*Power(t2,3) + 
                     2*Power(t2,4))) + 
               s2*(-(Power(-1 + t2,2)*
                     (-10 + 12*t2 + 6*Power(t2,2) + Power(t2,3))) + 
                  Power(t1,2)*
                   (1 + 18*t2 + 24*Power(t2,2) + 11*Power(t2,3)) + 
                  t1*(-11 - 40*t2 + 89*Power(t2,2) - 35*Power(t2,3) - 
                     4*Power(t2,4) + Power(t2,5))))) - 
         Power(s,3)*(-1 + t2)*
          (Power(t1,3)*t2*(5 - 15*t2 + 24*Power(t2,2) + 
               17*Power(t2,3) + 3*Power(t2,4)) - 
            Power(t1,2)*(-3 + (-8 + 9*s2)*t2 + 
               (51 - 24*s2)*Power(t2,2) + (-44 + 31*s2)*Power(t2,3) + 
               (-9 + 71*s2)*Power(t2,4) + (17 + 14*s2)*Power(t2,5) + 
               (-5 + s2)*Power(t2,6) + Power(t2,7)) + 
            t1*(-3 + 2*(-5 + 4*s2)*t2 + 
               (18 + 23*s2 - 3*Power(s2,2))*Power(t2,2) + 
               (22 + 19*s2 + 3*Power(s2,2))*Power(t2,3) + 
               (-82 - 97*s2 + 77*Power(s2,2))*Power(t2,4) + 
               (95 + 28*s2 + 23*Power(s2,2))*Power(t2,5) + 
               (-41 + 20*s2 + 2*Power(s2,2))*Power(t2,6) - 
               (-1 + s2)*Power(t2,7)) - 
            Power(s1,4)*Power(-1 + t2,3)*
             (-8 + s2 - s2*t2 + t1*(7 + t2)) - 
            t2*(Power(s2,3)*Power(t2,2)*
                (-10 + 37*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               Power(-1 + t2,3)*
                (-3 - 25*t2 - 3*Power(t2,2) - 7*Power(t2,3) - 
                  8*Power(t2,4) + Power(t2,5)) - 
               2*Power(s2,2)*t2*
                (-4 - 51*t2 + 108*Power(t2,2) - 52*Power(t2,3) - 
                  2*Power(t2,4) + Power(t2,5)) - 
               s2*Power(-1 + t2,2)*
                (2 + 22*t2 + 40*Power(t2,2) - 12*Power(t2,3) - 
                  Power(t2,4) + Power(t2,5))) - 
            Power(s1,3)*(-1 + t2)*
             (-(Power(-1 + t2,2)*(7 - 48*t2 + Power(t2,2))) + 
               Power(t1,2)*(-18 + 61*t2 + 9*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(s2,2)*(1 + 6*t2 + 40*Power(t2,2) + Power(t2,3)) + 
               t1*(-12 + 71*t2 - 55*Power(t2,2) + 3*Power(t2,3) - 
                  7*Power(t2,4)) + 
               s2*(3 - 65*t2 + 63*Power(t2,2) - Power(t2,3) + 
                  t1*(19 - 73*t2 - 43*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,2)*(Power(t1,3)*
                (-17 + 2*t2 + 59*Power(t2,2) - 10*Power(t2,3)) - 
               Power(s2,3)*t2*
                (1 + 6*t2 + 25*Power(t2,2) + 2*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (-3 - 24*t2 + 106*Power(t2,2) + 4*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*t2*
                (-83 + 165*t2 + 11*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,2)*(-13 + 118*t2 - 160*Power(t2,2) + 
                  9*Power(t2,3) + 61*Power(t2,4) - 15*Power(t2,5)) + 
               Power(s2,2)*(t1*
                   (-5 - 19*t2 + 81*Power(t2,2) + 41*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  t2*(-8 + 65*t2 - 95*Power(t2,2) + 33*Power(t2,3) + 
                     5*Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (19 + 25*t2 - 137*Power(t2,2) - 9*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (1 - 32*t2 + 139*Power(t2,2) - 9*Power(t2,3) + 
                     Power(t2,4)) + 
                  t1*(4 - 97*t2 + 116*Power(t2,2) + 35*Power(t2,3) - 
                     62*Power(t2,4) + 4*Power(t2,5)))) + 
            s1*(Power(t1,3)*(-7 + 35*t2 - 32*Power(t2,2) - 
                  62*Power(t2,3) - 5*Power(t2,4) + 3*Power(t2,5)) + 
               t2*(Power(s2,3)*t2*
                   (-9 + 43*t2 + 31*Power(t2,2) + 3*Power(t2,3)) + 
                  Power(-1 + t2,3)*
                   (1 + 33*t2 - 75*Power(t2,2) - 14*Power(t2,3) + 
                     Power(t2,4)) - 
                  s2*Power(-1 + t2,2)*
                   (7 - 17*t2 + 72*Power(t2,2) - 11*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  Power(s2,2)*
                   (5 + 115*t2 - 288*Power(t2,2) + 163*Power(t2,3) + 
                     11*Power(t2,4) - 6*Power(t2,5))) + 
               Power(t1,2)*(4 + 40*t2 - 127*Power(t2,2) + 
                  53*Power(t2,3) + 58*Power(t2,4) - 33*Power(t2,5) + 
                  5*Power(t2,6) + 
                  s2*(4 - 30*t2 + 6*Power(t2,2) + 184*Power(t2,3) + 
                     44*Power(t2,4) - 4*Power(t2,5))) + 
               t1*(Power(s2,2)*t2*
                   (11 + 10*t2 - 156*Power(t2,2) - 62*Power(t2,3) - 
                     7*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (3 + 23*t2 - 102*Power(t2,2) + 108*Power(t2,3) + 
                     20*Power(t2,4) + 2*Power(t2,5)) + 
                  s2*(-5 - 52*t2 + 61*Power(t2,2) + 143*Power(t2,3) - 
                     154*Power(t2,4) + 9*Power(t2,5) - 2*Power(t2,6))))) \
- Power(s,2)*Power(-1 + t2,2)*
          (1 - 3*Power(t1,2) + 2*Power(t1,3) + 9*t2 - 6*s2*t2 - 
            30*t1*t2 + 13*s2*t1*t2 + 28*Power(t1,2)*t2 - 
            7*s2*Power(t1,2)*t2 - 7*Power(t1,3)*t2 - 42*Power(t2,2) + 
            9*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) + 
            72*t1*Power(t2,2) - 12*s2*t1*Power(t2,2) - 
            3*Power(s2,2)*t1*Power(t2,2) - 56*Power(t1,2)*Power(t2,2) + 
            22*s2*Power(t1,2)*Power(t2,2) + 5*Power(t1,3)*Power(t2,2) + 
            46*Power(t2,3) + 47*s2*Power(t2,3) - 
            71*Power(s2,2)*Power(t2,3) + 6*Power(s2,3)*Power(t2,3) - 
            52*t1*Power(t2,3) + 28*s2*t1*Power(t2,3) + 
            18*Power(s2,2)*t1*Power(t2,3) + 22*Power(t1,2)*Power(t2,3) - 
            55*s2*Power(t1,2)*Power(t2,3) + 23*Power(t1,3)*Power(t2,3) + 
            11*Power(t2,4) - 106*s2*Power(t2,4) + 
            120*Power(s2,2)*Power(t2,4) - 31*Power(s2,3)*Power(t2,4) - 
            5*t1*Power(t2,4) - 33*s2*t1*Power(t2,4) + 
            61*Power(s2,2)*t1*Power(t2,4) + 13*Power(t1,2)*Power(t2,4) - 
            47*s2*Power(t1,2)*Power(t2,4) + 7*Power(t1,3)*Power(t2,4) - 
            46*Power(t2,5) + 62*s2*Power(t2,5) - 
            45*Power(s2,2)*Power(t2,5) - 5*Power(s2,3)*Power(t2,5) + 
            30*t1*Power(t2,5) - 6*s2*t1*Power(t2,5) + 
            14*Power(s2,2)*t1*Power(t2,5) - 5*Power(t1,2)*Power(t2,5) - 
            3*s2*Power(t1,2)*Power(t2,5) + 25*Power(t2,6) - 
            2*s2*Power(t2,6) - 6*Power(s2,2)*Power(t2,6) - 
            17*t1*Power(t2,6) + 11*s2*t1*Power(t2,6) + 
            Power(t1,2)*Power(t2,6) - 5*Power(t2,7) - 5*s2*Power(t2,7) + 
            Power(s2,2)*Power(t2,7) + 2*t1*Power(t2,7) - 
            s2*t1*Power(t2,7) + Power(t2,8) + s2*Power(t2,8) + 
            Power(s1,4)*(-1 + t2)*
             (-18*t1*(-1 + t2) + 4*Power(-1 + t2,2) + 
               Power(t1,2)*(16 + 7*t2 - Power(t2,2)) + 
               Power(s2,2)*(4 + 17*t2 + Power(t2,2)) - 
               2*s2*(7 - 5*t2 - 2*Power(t2,2) + 2*t1*(5 + 6*t2))) + 
            Power(s1,3)*(-(Power(-1 + t2,3)*t2*(20 + t2)) + 
               2*Power(t1,3)*(-2 - 18*t2 + 5*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (-13 + 46*t2 + 7*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(2 + 7*t2 + 19*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(-28 + 38*t2 + 16*Power(t2,2) - 
                  29*Power(t2,3) + 3*Power(t2,4)) - 
               Power(s2,2)*(t2*
                   (8 - 20*t2 + 11*Power(t2,2) + Power(t2,3)) + 
                  t1*(3 + 58*t2 + 27*Power(t2,2) + 2*Power(t2,3))) + 
               s2*(Power(t1,2)*(5 + 87*t2 - 2*Power(t2,2)) - 
                  Power(-1 + t2,2)*(-5 + 43*t2 + 3*Power(t2,2)) + 
                  t1*(22 - 17*t2 - 39*Power(t2,2) + 31*Power(t2,3) + 
                     3*Power(t2,4)))) + 
            Power(s1,2)*(-3 + 
               (12 + s2 - 79*Power(s2,2) + 2*Power(s2,3))*t2 + 
               (-39 + 33*s2 + 162*Power(s2,2) - 45*Power(s2,3))*
                Power(t2,2) - 
               (-74 + 85*s2 + 46*Power(s2,2) + 43*Power(s2,3))*
                Power(t2,3) - 
               (63 - 68*s2 + 37*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,4) - 18*(-1 + s2)*Power(t2,5) + 
               (1 + s2)*Power(t2,6) + 
               3*Power(t1,3)*t2*
                (15 + 15*t2 + Power(t2,2) - Power(t2,3)) - 
               Power(t1,2)*(10 - 39*t2 - 7*Power(t2,2) + 
                  51*Power(t2,3) - 16*Power(t2,4) + Power(t2,5) + 
                  s2*(3 + 68*t2 + 166*Power(t2,2) + 35*Power(t2,3) - 
                     2*Power(t2,4))) + 
               t1*(-(Power(-1 + t2,2)*
                     (8 - 17*t2 + 19*Power(t2,2) + 11*Power(t2,3))) + 
                  s2*(22 + 23*t2 - 189*Power(t2,2) + 139*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  Power(s2,2)*
                   (-2 + 32*t2 + 163*Power(t2,2) + 68*Power(t2,3) + 
                     9*Power(t2,4)))) + 
            s1*(-(Power(-1 + t2,3)*t2*
                  (21 + 13*t2 + Power(t2,2) + Power(t2,3))) + 
               Power(s2,3)*Power(t2,2)*
                (-10 + 69*t2 + 29*Power(t2,2) + 2*Power(t2,3)) - 
               2*Power(t1,3)*(-1 + t2 + 30*Power(t2,2) + 
                  13*Power(t2,3) + 2*Power(t2,4)) + 
               Power(t1,2)*(-9 + 36*t2 - 33*Power(t2,2) - 
                  9*Power(t2,3) + 10*Power(t2,4) + 4*Power(t2,5) + 
                  Power(t2,6)) + 
               t1*(7 - 13*t2 + 20*Power(t2,2) - 24*Power(t2,3) + 
                  3*Power(t2,4) + 8*Power(t2,5) - Power(t2,7)) - 
               Power(s2,2)*t2*
                (1 - 150*t2 + 270*Power(t2,2) - 84*Power(t2,3) - 
                  38*Power(t2,4) + Power(t2,5) + 
                  t1*(-5 + 47*t2 + 166*Power(t2,2) + 55*Power(t2,3) + 
                     7*Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (2 - 6*t2 + 106*Power(t2,2) + 134*Power(t2,3) + 
                     33*Power(t2,4) + Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (-1 - 6*t2 + 54*Power(t2,2) - 6*Power(t2,3) - 
                     18*Power(t2,4) + 2*Power(t2,5)) - 
                  t1*(3 + 36*t2 + 49*Power(t2,2) - 203*Power(t2,3) + 
                     84*Power(t2,4) + 29*Power(t2,5) + 2*Power(t2,6))))))*
       T4q(-1 + t2))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*(-1 + t1)*Power(-1 + t2,2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) + 
    (8*(Power(s,7)*Power(t2,2)*(t1 + t2) + 
         Power(s,6)*t2*(t2*(1 + Power(t1,2) - t1*(6 + s2 - 7*t2) - 
               2*(3 + s2)*t2) + s1*(t1*(2 - 4*t2) + (2 + s2 - 3*t2)*t2)) + 
         Power(s,5)*(-(Power(t1,3)*(-1 + t2)) + 
            t1*Power(t2,2)*(11 + s2*(3 - 7*t2) - 27*t2 + 5*Power(t2,2)) + 
            Power(t1,2)*t2*(s2 + 2*t2*(-1 + 3*t2)) + 
            Power(t2,2)*(-5 + 9*t2 + Power(s2,2)*t2 - 6*Power(t2,2) + 
               Power(t2,3) + s2*(-1 + 11*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(t2*(1 + s2*(2 - 3*t2) - 4*t2 + 3*Power(t2,2)) + 
               t1*(1 - 6*t2 + 6*Power(t2,2))) + 
            s1*t2*(2 + Power(t1,2)*(1 - 4*t2) - 
               (13 + 9*s2 + Power(s2,2))*t2 + 4*(3 + s2)*Power(t2,2) + 
               t1*(-10 + 36*t2 - 21*Power(t2,2) + s2*(-2 + 3*t2)))) + 
         Power(s,4)*(Power(t1,3)*
             (-2 + 6*t2 - Power(t2,2) + Power(t2,3)) + 
            Power(t1,2)*(3 + (8 - 5*s2)*t2 - Power(t2,2) - 
               2*(6 + s2)*Power(t2,3) + 3*Power(t2,4)) - 
            Power(s1,3)*(-1 + t2)*
             (s2 - 3*s2*t2 + (-1 + t2)*t2 + t1*(-2 + 4*t2)) + 
            t1*t2*(3 - 3*t2 + Power(s2,2)*(-1 + t2)*t2 + 4*Power(t2,2) + 
               Power(t2,3) + 2*Power(t2,4) + 
               s2*(-1 + 9*t2 + 23*Power(t2,2))) + 
            Power(s1,2)*(1 - 2*(4 + 5*s2 + Power(s2,2))*t2 + 
               (16 + 12*s2 + Power(s2,2))*Power(t2,2) - 
               (7 + 2*s2)*Power(t2,3) + Power(t1,2)*t2*(-1 + 6*t2) - 
               t1*(4 + s2 - 39*t2 - 2*s2*t2 + 56*Power(t2,2) + 
                  3*s2*Power(t2,2) - 21*Power(t2,3))) + 
            Power(t2,2)*(-(Power(s2,2)*(2 + 7*t2 + 3*Power(t2,2))) + 
               s2*(1 - 14*t2 - 10*Power(t2,2) + Power(t2,3)) + 
               2*(4 - t2 + 9*Power(t2,2) - 8*Power(t2,3) + Power(t2,4))) \
+ s1*(Power(t1,3)*(-7 + 5*t2) + 
               Power(t1,2)*(-3 + s2*(4 - 5*t2) + 12*Power(t2,2) - 
                  17*Power(t2,3)) + 
               t1*t2*(7 - 62*t2 + 53*Power(t2,2) - 11*Power(t2,3) + 
                  2*Power(s2,2)*(1 + t2) + 
                  s2*(5 - 23*t2 + 13*Power(t2,2))) - 
               t2*(8 - 12*t2 + 16*Power(t2,2) - 15*Power(t2,3) + 
                  Power(t2,4) - 3*Power(s2,2)*t2*(2 + t2) + 
                  s2*(2 - 30*t2 + 13*Power(t2,2) + Power(t2,3))))) + 
         Power(s,3)*(-(Power(s1,4)*(s2 - t1)*Power(-1 + t2,2)) + 
            Power(t1,3)*t2*(-5 + 5*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(-3 + (-14 + 9*s2)*t2 + 
               (26 - 6*s2)*Power(t2,2) + 2*(-17 + s2)*Power(t2,3) + 
               (5 + s2)*Power(t2,4) + Power(t2,5)) + 
            t1*(3 - 8*(-2 + s2)*t2 + 
               (11 - 39*s2 + 3*Power(s2,2))*Power(t2,2) + 
               (126 + 47*s2 - 13*Power(s2,2))*Power(t2,3) - 
               (57 + 16*s2 + 2*Power(s2,2))*Power(t2,4) + 
               (15 + s2)*Power(t2,5)) + 
            t2*(3 + 22*t2 + 18*Power(t2,2) - 58*Power(t2,3) + 
               25*Power(t2,4) - 11*Power(t2,5) + Power(t2,6) + 
               Power(s2,3)*Power(t2,2)*(2 + t2) - 
               2*Power(s2,2)*t2*
                (-4 - 7*t2 - 7*Power(t2,2) + Power(t2,3)) - 
               s2*(2 + 22*t2 + 96*Power(t2,2) - 12*Power(t2,3) + 
                  5*Power(t2,4) + Power(t2,5))) + 
            Power(s1,3)*(-1 + 5*t2 - 5*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(2 - 3*t2 - 4*Power(t2,2)) + 
               Power(s2,2)*(-1 - t2 + Power(t2,2)) + 
               t1*(12 - 37*t2 + 28*Power(t2,2) - 7*Power(t2,3)) + 
               s2*(-3 + 8*t2 - Power(t2,2) + 
                  t1*(-1 + 6*t2 + Power(t2,2)))) + 
            Power(s1,2)*(-3 + Power(t1,3)*(17 - 10*t2) + 3*t2 + 
               10*Power(t2,2) - 10*Power(t2,4) + 
               Power(s2,3)*t2*(1 + 2*t2) + 
               t1*t2*(-5 + 39*t2 - 25*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,2)*(13 - 2*t2 - 7*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(s2,2)*(t2*(8 + 11*t2 - 5*Power(t2,2)) - 
                  t1*(-5 + t2 + 4*Power(t2,2))) + 
               s2*(-1 + 14*t2 - 37*Power(t2,2) + 9*Power(t2,3) - 
                  Power(t2,4) + Power(t1,2)*(-19 + 9*t2) - 
                  t1*(4 + 31*t2 - 6*Power(t2,2) + 4*Power(t2,3)))) - 
            s1*(Power(t1,3)*(-7 + 21*t2 - 7*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(4 + 48*t2 + Power(t2,2) - 3*Power(t2,3) + 
                  5*Power(t2,4) + 
                  s2*(4 - 22*t2 + 6*Power(t2,2) - 4*Power(t2,3))) + 
               t1*(3 + 23*t2 + 96*Power(t2,2) - 82*Power(t2,3) + 
                  24*Power(t2,4) + 2*Power(t2,5) + 
                  Power(s2,2)*t2*(11 - 4*t2 - 7*Power(t2,2)) - 
                  s2*(5 + 62*t2 + 34*Power(t2,2) - 13*Power(t2,3) + 
                     2*Power(t2,4))) + 
               t2*(-1 + 22*t2 - 20*Power(t2,2) + 19*Power(t2,3) - 
                  21*Power(t2,4) + Power(t2,5) + 
                  3*Power(s2,3)*t2*(1 + t2) + 
                  Power(s2,2)*
                   (5 + 11*t2 + 25*Power(t2,2) - 6*Power(t2,3)) - 
                  s2*(7 + 31*t2 + 30*Power(t2,2) - 5*Power(t2,3) + 
                     3*Power(t2,4))))) - 
         (s1 - t2)*(-1 + t2)*(-1 + Power(s1,4)*Power(s2 - t1,3) + 3*t1 - 
            3*Power(t1,2) + Power(t1,3) + 3*t2 - 3*s2*t2 - 6*t1*t2 + 
            6*s2*t1*t2 + 3*Power(t1,2)*t2 - 3*s2*Power(t1,2)*t2 - 
            Power(t2,2) - 20*s2*Power(t2,2) + 7*Power(s2,2)*Power(t2,2) + 
            25*t1*Power(t2,2) + 4*s2*t1*Power(t2,2) - 
            7*Power(s2,2)*t1*Power(t2,2) - 18*Power(t1,2)*Power(t2,2) + 
            16*s2*Power(t1,2)*Power(t2,2) - 6*Power(t1,3)*Power(t2,2) - 
            5*Power(t2,3) + 49*s2*Power(t2,3) - 
            9*Power(s2,2)*Power(t2,3) + Power(s2,3)*Power(t2,3) - 
            40*t1*Power(t2,3) - 36*s2*t1*Power(t2,3) + 
            8*Power(s2,2)*t1*Power(t2,3) + 42*Power(t1,2)*Power(t2,3) - 
            14*s2*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) + 
            6*Power(t2,4) - 26*s2*Power(t2,4) + 14*t1*Power(t2,4) + 
            30*s2*t1*Power(t2,4) - 4*Power(s2,2)*t1*Power(t2,4) - 
            24*Power(t1,2)*Power(t2,4) + 4*s2*Power(t1,2)*Power(t2,4) - 
            2*Power(t2,5) + 2*Power(s2,2)*Power(t2,5) + 
            4*t1*Power(t2,5) - 4*s2*t1*Power(t2,5) - 
            Power(s1,3)*(s2 - t1)*
             (4*Power(t1,2) - 6*Power(-1 + t2,2) + 
               Power(s2,2)*(1 + 3*t2) + t1*(1 - 5*t2 + 4*Power(t2,2)) + 
               s2*(5 - 7*t2 + 2*Power(t2,2) - t1*(5 + 3*t2))) + 
            Power(s1,2)*(-2*Power(-1 + t2,3) + 
               6*Power(t1,3)*(-2 + t2)*t2 + 3*Power(s2,3)*t2*(1 + t2) + 
               t1*Power(-1 + t2,2)*(25 + 16*t2) - 
               Power(t1,2)*(27 - 55*t2 + 20*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,2)*(7 + t2 - 14*Power(t2,2) + 6*Power(t2,3) - 
                  t1*(7 + 4*t2 + 7*Power(t2,2))) + 
               s2*(-(Power(-1 + t2,2)*(29 + 12*t2)) + 
                  Power(t1,2)*(7 + 13*t2 - 2*Power(t2,2)) + 
                  t1*(22 - 62*t2 + 40*Power(t2,2)))) + 
            s1*(-(Power(s2,3)*Power(t2,2)*(3 + t2)) + 
               Power(-1 + t2,3)*(-1 + 4*t2) - 
               2*t1*Power(-1 + t2,2)*(3 + 22*t2 + 7*Power(t2,2)) - 
               4*Power(t1,3)*(1 - 3*t2 + Power(t2,3)) + 
               Power(t1,2)*(9 + 27*t2 - 86*Power(t2,2) + 
                  46*Power(t2,3) + 4*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-14 + 13*t2 + 7*Power(t2,2) - 6*Power(t2,3) + 
                  2*t1*(7 - 5*t2 + 4*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(3 + 55*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*
                   (3 - 23*t2 + 10*Power(t2,2) - 2*Power(t2,3)) + 
                  t1*(-6 - 26*t2 + 94*Power(t2,2) - 68*Power(t2,3) + 
                     6*Power(t2,4))))) - 
         Power(s,2)*(-1 + 3*Power(t1,2) - 2*Power(t1,3) - 10*t2 + 
            6*s2*t2 + 30*t1*t2 - 13*s2*t1*t2 - 25*Power(t1,2)*t2 + 
            7*s2*Power(t1,2)*t2 + 5*Power(t1,3)*t2 + 32*Power(t2,2) - 
            3*s2*Power(t2,2) - Power(s2,2)*Power(t2,2) - 
            42*t1*Power(t2,2) - s2*t1*Power(t2,2) + 
            3*Power(s2,2)*t1*Power(t2,2) + 31*Power(t1,2)*Power(t2,2) - 
            15*s2*Power(t1,2)*Power(t2,2) + 22*Power(t2,3) - 
            146*s2*Power(t2,3) - 14*Power(s2,2)*Power(t2,3) + 
            6*Power(s2,3)*Power(t2,3) + 144*t1*Power(t2,3) + 
            189*s2*t1*Power(t2,3) - 31*Power(s2,2)*t1*Power(t2,3) - 
            127*Power(t1,2)*Power(t2,3) + 16*s2*Power(t1,2)*Power(t2,3) + 
            5*Power(t1,3)*Power(t2,3) - 95*Power(t2,4) + 
            154*s2*Power(t2,4) - 4*Power(s2,2)*Power(t2,4) + 
            Power(s2,3)*Power(t2,4) - 169*t1*Power(t2,4) - 
            130*s2*t1*Power(t2,4) + 12*Power(s2,2)*t1*Power(t2,4) + 
            88*Power(t1,2)*Power(t2,4) + 3*s2*Power(t1,2)*Power(t2,4) - 
            10*Power(t1,3)*Power(t2,4) + 53*Power(t2,5) - 
            12*s2*Power(t2,5) - 15*Power(s2,2)*Power(t2,5) + 
            49*t1*Power(t2,5) + 22*s2*t1*Power(t2,5) - 
            3*Power(t1,2)*Power(t2,5) - 2*Power(t2,6) + 
            Power(s2,2)*Power(t2,6) - 12*t1*Power(t2,6) - 
            s2*t1*Power(t2,6) + Power(t2,7) + s2*Power(t2,7) + 
            Power(s1,4)*(Power(s2,2)*(2 - 5*t2 + Power(t2,2)) + 
               2*s2*(-1 + Power(t2,2) + t1*(-3 + 5*t2)) + 
               t1*(2*(3 - 4*t2 + Power(t2,2)) - 
                  t1*(-4 + 5*t2 + Power(t2,2)))) + 
            Power(s1,3)*(-(Power(-1 + t2,2)*t2*(16 + t2)) - 
               2*Power(t1,3)*(-9 + 5*t2) + 
               Power(s2,3)*(-2 - 3*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(14 - 18*t2 + 14*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(-13 + 23*t2 - 13*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s2,2)*(t2*(2 - 16*t2 + Power(t2,2)) + 
                  t1*(-13 - 3*t2 + 2*Power(t2,2))) + 
               s2*(1 + 10*t2 - 16*Power(t2,2) + 5*Power(t2,3) + 
                  Power(t1,2)*(-29 + 10*t2) + 
                  t1*t2*(-3 - 26*t2 + 3*Power(t2,2)))) + 
            Power(s1,2)*(3 + (21 - 31*s2 - 35*Power(s2,2) + 
                  10*Power(s2,3))*t2 + 
               (-24 + 8*s2 + 19*Power(s2,2) + 7*Power(s2,3))*
                Power(t2,2) - 
               (26 - 45*s2 + 37*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,3) + (25 - 23*s2)*Power(t2,4) + 
               (1 + s2)*Power(t2,5) - 
               3*Power(t1,3)*t2*(9 - 4*t2 + Power(t2,2)) + 
               Power(t1,2)*(10 - 83*t2 + 42*Power(t2,2) - 
                  21*Power(t2,3) - Power(t2,4) + 
                  s2*(3 + 47*t2 - 3*Power(t2,2) + 2*Power(t2,3))) + 
               t1*(8 + 95*t2 - 204*Power(t2,2) + 124*Power(t2,3) - 
                  23*Power(t2,4) + 
                  Power(s2,2)*
                   (2 - 36*t2 - 19*Power(t2,2) + 9*Power(t2,3)) + 
                  s2*(-22 + 99*t2 + 12*Power(t2,2) + 17*Power(t2,3)))) + 
            s1*(Power(s2,3)*Power(t2,2)*(-14 - 5*t2 + 2*Power(t2,2)) - 
               Power(-1 + t2,2)*t2*
                (21 + 79*t2 + 9*Power(t2,2) + Power(t2,3)) + 
               2*Power(t1,3)*(-1 + 7*Power(t2,3)) + 
               Power(t1,2)*(9 - 27*t2 + 210*Power(t2,2) - 
                  135*Power(t2,3) + 17*Power(t2,4) + Power(t2,5)) - 
               t1*(7 - 6*t2 + 242*Power(t2,2) - 364*Power(t2,3) + 
                  147*Power(t2,4) - 27*Power(t2,5) + Power(t2,6)) + 
               Power(s2,2)*t2*
                (1 + 49*t2 - 15*Power(t2,2) + 41*Power(t2,3) - 
                  Power(t2,4) + 
                  t1*(-5 + 54*t2 + 4*Power(t2,2) - 7*Power(t2,3))) + 
               s2*(-1 - 5*t2 + 180*Power(t2,2) - 174*Power(t2,3) - 
                  14*Power(t2,4) + 16*Power(t2,5) - 2*Power(t2,6) + 
                  Power(t1,2)*
                   (-2 + 4*t2 - 30*Power(t2,2) - 14*Power(t2,3) + 
                     Power(t2,4)) + 
                  t1*(3 + 39*t2 - 296*Power(t2,2) + 135*Power(t2,3) - 
                     29*Power(t2,4) - 2*Power(t2,5))))) + 
         s*(1 - 3*t1 + 3*Power(t1,2) - Power(t1,3) - 
            2*Power(s1,5)*Power(s2 - t1,2)*(-1 + t2) - 10*t2 + 2*s2*t2 + 
            26*t1*t2 - 4*s2*t1*t2 - 22*Power(t1,2)*t2 + 
            2*s2*Power(t1,2)*t2 + 6*Power(t1,3)*t2 + 19*Power(t2,2) - 
            3*s2*Power(t2,2) - Power(s2,2)*Power(t2,2) - 
            45*t1*Power(t2,2) + 15*s2*t1*Power(t2,2) + 
            Power(s2,2)*t1*Power(t2,2) + 31*Power(t1,2)*Power(t2,2) - 
            12*s2*Power(t1,2)*Power(t2,2) - 5*Power(t1,3)*Power(t2,2) + 
            9*Power(t2,3) - 93*s2*Power(t2,3) + 
            5*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) + 
            97*t1*Power(t2,3) + 80*s2*t1*Power(t2,3) - 
            20*Power(s2,2)*t1*Power(t2,3) - 88*Power(t1,2)*Power(t2,3) + 
            29*s2*Power(t1,2)*Power(t2,3) - Power(t1,3)*Power(t2,3) - 
            54*Power(t2,4) + 191*s2*Power(t2,4) - 
            7*Power(s2,2)*Power(t2,4) - 4*Power(s2,3)*Power(t2,4) - 
            152*t1*Power(t2,4) - 189*s2*t1*Power(t2,4) + 
            39*Power(s2,2)*t1*Power(t2,4) + 150*Power(t1,2)*Power(t2,4) - 
            34*s2*Power(t1,2)*Power(t2,4) + 47*Power(t2,5) - 
            103*s2*Power(t2,5) - 3*Power(s2,2)*Power(t2,5) - 
            Power(s2,3)*Power(t2,5) + 85*t1*Power(t2,5) + 
            110*s2*t1*Power(t2,5) - 11*Power(s2,2)*t1*Power(t2,5) - 
            74*Power(t1,2)*Power(t2,5) + 6*s2*Power(t1,2)*Power(t2,5) + 
            4*Power(t1,3)*Power(t2,5) - 10*Power(t2,6) + 
            8*s2*Power(t2,6) + 6*Power(s2,2)*Power(t2,6) - 
            12*t1*Power(t2,6) - 12*s2*t1*Power(t2,6) - 2*Power(t2,7) - 
            2*s2*Power(t2,7) + 4*t1*Power(t2,7) - 
            Power(s1,4)*(-(t1*(-2 + t2)*Power(-1 + t2,2)) + 
               4*Power(-1 + t2,3) + Power(s2,3)*(1 + 2*t2) + 
               Power(t1,3)*(-8 + 5*t2) + 
               Power(s2,2)*(2 + t1*(-10 + t2) - t2 - Power(t2,2)) + 
               Power(t1,2)*(-6 + 16*t2 - 11*Power(t2,2) + Power(t2,3)) + 
               s2*(Power(t1,2)*(17 - 8*t2) + (-2 + t2)*Power(-1 + t2,2) - 
                  t1*(-4 + 15*t2 - 12*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,3)*(Power(-1 + t2,3)*(8 + 13*t2) - 
               t1*Power(-1 + t2,2)*(21 - 9*t2 + 4*Power(t2,2)) + 
               Power(s2,3)*(-2 + 7*t2 + 7*Power(t2,2)) - 
               Power(t1,3)*(9 + 9*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(21 - 45*t2 + 31*Power(t2,2) - 
                  8*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(20 - 35*t2 + 13*Power(t2,2) + 
                  2*Power(t2,3) + 
                  t1*(-5 - 21*t2 - 11*Power(t2,2) + Power(t2,3))) + 
               s2*(Power(t1,2)*(16 + 23*t2 - 3*Power(t2,2)) + 
                  Power(-1 + t2,2)*(1 + 11*t2 + 4*Power(t2,2)) + 
                  t1*(-33 + 57*t2 - 23*Power(t2,2) + Power(t2,3) - 
                     2*Power(t2,4)))) + 
            Power(s1,2)*(-3*Power(s2,3)*t2*(-2 + 5*t2 + 3*Power(t2,2)) - 
               Power(-1 + t2,3)*(1 + 36*t2 + 17*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (-3 + 127*t2 - 12*Power(t2,2) + 11*Power(t2,3)) + 
               Power(t1,3)*(1 + 27*t2 - 27*Power(t2,2) + 
                  17*Power(t2,3)) + 
               Power(t1,2)*(1 - 142*t2 + 264*Power(t2,2) - 
                  117*Power(t2,3) - 6*Power(t2,4)) + 
               Power(s2,2)*(-1 - 35*t2 + 69*Power(t2,2) - 
                  40*Power(t2,3) + 7*Power(t2,4) + 
                  t1*(1 - 10*t2 + 51*Power(t2,2) + 14*Power(t2,3) - 
                     2*Power(t2,4))) + 
               s2*(-(Power(-1 + t2,2)*
                     (-10 + 102*t2 + 24*Power(t2,2) + 7*Power(t2,3))) - 
                  Power(t1,2)*
                   (-1 + 30*t2 + 6*Power(t2,2) + 19*Power(t2,3)) + 
                  t1*(-11 + 200*t2 - 337*Power(t2,2) + 141*Power(t2,3) + 
                     6*Power(t2,4) + Power(t2,5)))) + 
            s1*(Power(s2,3)*Power(t2,2)*(-6 + 13*t2 + 5*Power(t2,2)) + 
               Power(-1 + t2,3)*
                (-7 - 4*t2 + 45*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,3)*(-3 + t2 - 15*Power(t2,2) + 25*Power(t2,3) - 
                  20*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (17 - 5*t2 + 176*Power(t2,2) - 6*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(t1,2)*(13 - 23*t2 + 203*Power(t2,2) - 
                  366*Power(t2,3) + 165*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,2)*t2*
                (2 + 10*t2 - 25*Power(t2,2) + 27*Power(t2,3) - 
                  14*Power(t2,4) + 
                  t1*(-2 + 35*t2 - 79*Power(t2,2) + 9*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (-2 - 11*t2 + 194*Power(t2,2) + 7*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  Power(t1,2)*
                   (-2 + 11*t2 - 15*Power(t2,2) + 34*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  t1*(4 - 4*t2 - 247*Power(t2,2) + 473*Power(t2,3) - 
                     239*Power(t2,4) + 13*Power(t2,5))))))*T5q(s))/
     (s*(-1 + s1)*(-1 + s2)*(-1 + t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)));
   return a;
};
