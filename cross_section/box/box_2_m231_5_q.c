#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_2_m231_5_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-16*(-(s2*(-1 + t2)*Power(t2,2)*(-1 + s2 + t2)) - 
         2*Power(t1,5)*(-1 + (-6 + 4*s2 + s*(-5 + 2*s2))*t2 + 
            (-1 + s)*Power(t2,2)) - 
         Power(t1,4)*(-4 + (3 + 7*s)*t2 + (29 + 21*s)*Power(t2,2) - 
            4*(-1 + s)*Power(t2,3) + 2*Power(s2,2)*t2*(-9 - 2*s + t2) + 
            s2*(2 + (48 + 5*s)*t2 - (32 + 9*s)*Power(t2,2) - 
               2*Power(t2,3))) + 
         Power(t1,3)*(2 + (-26 + s)*t2 - 8*Power(s2,3)*t2 + 
            (18 + 13*s)*Power(t2,2) + 4*(5 + 3*s)*Power(t2,3) - 
            2*(-1 + s)*Power(t2,4) - 
            Power(s2,2)*t2*(-37 + 4*(13 + 3*s)*t2 + Power(t2,2)) + 
            s2*(-2 + (5 + s)*t2 + (96 + 17*s)*Power(t2,2) - 
               3*(13 + 2*s)*Power(t2,3) - 4*Power(t2,4))) - 
         t1*t2*(8*Power(s2,3)*t2*(-1 + 2*t2) - 
            (-1 + t2)*(-6 + (5 - 4*s)*t2 + (17 + 3*s)*Power(t2,2)) + 
            Power(s2,2)*(-5 + (17 - 4*s)*t2 + (-39 + 8*s)*Power(t2,2) + 
               11*Power(t2,3)) + 
            s2*(11 + 2*(-9 + 4*s)*t2 - (3 + 17*s)*Power(t2,2) + 
               (4 + s)*Power(t2,3) + 6*Power(t2,4))) - 
         s1*(-1 + t1)*t1*(-1 + t2)*
          (Power(t1,3) + Power(t1,2)*(2 + t2 - s2*(1 + t2)) + 
            t1*(1 + 11*t2 + 3*Power(s2,2)*t2 - 2*Power(t2,2) - 
               s2*(1 + 16*t2 + Power(t2,2))) + 
            t2*(5 - 7*t2 + 3*Power(s2,2)*(2 + t2) + 
               s2*(-11 + 5*t2 + 2*Power(t2,2)))) + 
         Power(t1,2)*t2*(3 + 28*t2 - 28*Power(t2,2) - 3*Power(t2,3) + 
            8*Power(s2,3)*(-1 + 3*t2) + 
            Power(s2,2)*(12 - 82*t2 + 51*Power(t2,2) + 3*Power(t2,3)) + 
            s2*(-6 + 7*t2 - 56*Power(t2,2) + 21*Power(t2,3) + 
               2*Power(t2,4)) + 
            s*(-4 + 6*t2 - 9*Power(t2,2) - Power(t2,3) + 
               Power(s2,2)*(-4 + 8*t2 + 8*Power(t2,2)) + 
               s2*(8 - 18*t2 - 11*Power(t2,2) + Power(t2,3))))))/
     ((-1 + s2)*Power(-1 + t1,2)*t1*Power(t1 - t2,2)*(-1 + t2)*t2*
       (-1 + s2 - t1 + t2)) + (8*
       (4*Power(t1,4) - 8*Power(s2,3)*(-2 + t2)*t2 - 
         t2*(1 + (-1 + s)*t2)*(-3 - t2 + 2*Power(t2,2)) - 
         Power(t1,3)*(17 + 3*t2 + s*(3 + t2) - s1*(7 + t2)) - 
         2*Power(s2,2)*(s1*(-1 + t1)*(-1 - 3*t2 + Power(t2,2)) + 
            t2*(-3 + 2*s*(-2 + t2) - 19*t2 + 9*Power(t2,2)) - 
            t1*(-8 + t2 - 4*s*t2 + (5 + 2*s)*Power(t2,2) + Power(t2,3))) \
- Power(t1,2)*(s1*(17 + 10*t2 + Power(t2,2) - 2*Power(t2,3)) + 
            2*(-33 - 14*t2 + 5*Power(t2,2) + Power(t2,3)) + 
            s*(1 - 11*t2 + 2*Power(t2,2) + 2*Power(t2,3))) + 
         t1*(-13 - 60*t2 - 28*Power(t2,2) + 13*Power(t2,3) - 
            2*Power(t2,4) + s1*
             (10 + 9*t2 + Power(t2,2) - 2*Power(t2,3)) + 
            s*(4 - 10*t2 - Power(t2,2) + Power(t2,3) + 2*Power(t2,4))) + 
         s2*(2 + (10 - 2*s - 13*s1)*t2 - 4*Power(t1,3)*t2 + 
            (55 + 6*s - 7*s1)*Power(t2,2) + 
            (33 - 6*s + 2*s1)*Power(t2,3) - 10*Power(t2,4) + 
            t1*(-8 - 101*t2 - 8*Power(t2,2) + 7*Power(t2,3) + 
               2*Power(t2,4) + 
               s1*(9 + 20*t2 + 5*Power(t2,2) - 2*Power(t2,3)) + 
               2*s*(-5 + 2*t2 - 2*Power(t2,2) + 3*Power(t2,3))) + 
            Power(t1,2)*(-2*s*(-5 + t2 + Power(t2,2)) + 
               (1 + t2)*(6 + t2 + s1*(-9 + 2*t2)))))*
       B1(1 - s2 + t1 - t2,t2,t1))/
     ((-1 + s2)*Power(-1 + t1,2)*(-t1 + s2*t2)) + 
    (16*(-4*Power(s2,4)*(Power(t1,3) - t1*Power(t2,2)) + 
         Power(s2,3)*(Power(t1,5) - Power(t2,2) + 
            Power(t1,4)*(15 + 2*s + 3*s1 + t2) + 
            Power(t1,3)*(5 - 2*s - 26*t2 + s1*(2 + t2)) - 
            Power(t1,2)*(-3 + 7*t2 + (9 + 2*s)*Power(t2,2) + 
               2*Power(t2,3) + s1*(5 - 2*Power(t2,2))) + 
            t1*t2*(-(s1*(1 + 2*t2)) + 2*(2 + t2 + s*t2 + 7*Power(t2,2)))) \
- s2*(Power(t2,4) - 2*Power(t1,6)*(2*s - 2*s1 + t2) + 
            t1*Power(t2,2)*(-2 + (-6 + 2*s + s1)*t2 + 
               (4 - 2*s + 2*s1)*Power(t2,2) - 6*Power(t2,3)) + 
            Power(t1,5)*(62 + s1*(8 - 14*t2) + 38*t2 + 3*Power(t2,2) + 
               2*s*(6 + 7*t2)) - 
            Power(t1,4)*(-18 + 44*t2 + 57*Power(t2,2) + 3*Power(t2,3) - 
               s1*(8 + 32*t2 + 7*Power(t2,2)) + 
               2*s*(4 + 4*t2 + 9*Power(t2,2))) - 
            Power(t1,3)*(-12 + (26 + 6*s)*t2 + 
               (103 - 6*s)*Power(t2,2) - 6*(4 + s)*Power(t2,3) + 
               s1*(20 + 2*t2 + 6*Power(t2,2) + Power(t2,3))) + 
            Power(t1,2)*t2*(26 + (7 + 12*s)*t2 + 
               (45 - 8*s)*Power(t2,2) + (7 + 2*s)*Power(t2,3) + 
               2*Power(t2,4) - s1*(16 + t2 + 2*Power(t2,3)))) - 
         Power(s2,2)*(Power(t1,6) + Power(t1,5)*(10 + s - s1 - 6*t2) + 
            2*Power(t2,3) + Power(t1,4)*
             (2 + s1*(8 - 7*t2) + s*(2 - 6*t2) - 42*t2 + 3*Power(t2,2)) \
+ t1*t2*(1 + (s + 2*(-5 + s1))*t2 + (-2 - 4*s + 4*s1)*Power(t2,2) - 
               16*Power(t2,3)) - 
            Power(t1,3)*(2 + s1 + 47*t2 + 4*s1*t2 - 42*Power(t2,2) + 
               2*Power(t2,3) - s*(-3 + 4*t2 + Power(t2,2))) + 
            Power(t1,2)*(5 + 2*(-1 + s)*t2 + (41 - 2*s)*Power(t2,2) + 
               2*(9 + 2*s)*Power(t2,3) + 4*Power(t2,4) - 
               s1*(6 - 11*t2 + 2*Power(t2,2) + 4*Power(t2,3)))) + 
         t1*(-4*Power(t1,6) + 
            Power(t1,5)*(52 + 4*(1 + s - s1)*t2 + 3*Power(t2,2)) - 
            Power(t2,3)*(1 + s*t2 + 4*Power(t2,2)) + 
            t1*t2*(4 + (-13 + 4*s + 6*s1)*t2 + 
               (22 - 6*s + 3*s1)*Power(t2,2) + 
               (-3 + 6*s - 2*s1)*Power(t2,3) + 2*Power(t2,4)) + 
            Power(t1,4)*(60 - 86*t2 - 4*Power(t2,2) - 8*Power(t2,3) + 
               s*(12 - 13*Power(t2,2)) + s1*(28 - 12*t2 + 13*Power(t2,2))\
) + Power(t1,3)*(-8 - 110*t2 + 34*Power(t2,2) + 6*Power(t2,3) + 
               7*Power(t2,4) + s1*(-4 + 12*t2 - 11*Power(t2,3)) + 
               2*s*(-6 - 6*t2 + Power(t2,2) + 7*Power(t2,3))) + 
            Power(t1,2)*(20 + 8*(2 + s)*t2 + (4 + 7*s)*Power(t2,2) + 
               (13 - 8*s)*Power(t2,3) - (4 + 5*s)*Power(t2,4) - 
               2*Power(t2,5) + 
               s1*(-24 + 4*t2 - 19*Power(t2,2) + 8*Power(t2,3) + 
                  2*Power(t2,4)))))*R1(t1))/
     ((-1 + s2)*Power(-1 + t1,2)*t1*Power(t1 - t2,3)*
       (Power(s2,2) - 4*t1 + 2*s2*t2 + Power(t2,2))) - 
    (16*(Power(t1,4)*(2 + (-23 - 2*s + 4*s2)*t2 + 
            (13 + 4*s*(-2 + s2) + 2*s2)*Power(t2,2) + 
            (-9 + 2*s + 2*s2)*Power(t2,3) + Power(t2,4)) + 
         Power(t1,3)*(2 + (-27 + s + 8*s2)*t2 + 
            (79 + s*(15 - 4*s2) + 4*s2 - 8*Power(s2,2))*Power(t2,2) - 
            (15 + 16*s2 + s*(-23 + 12*s2))*Power(t2,3) + 
            (11 - 7*s - 4*s2)*Power(t2,4) - 2*Power(t2,5)) - 
         t1*t2*(1 + (-4 + 3*s2 + 8*Power(s2,2) + s*(-2 + 4*s2))*t2 + 
            (15 + 14*s2 - 16*Power(s2,2) - s*(3 + 4*s2))*Power(t2,2) + 
            (1 - 120*s2 + 32*Power(s2,2) + s*(-25 + 8*s2))*Power(t2,3) + 
            (-28 + 58*s2 + s*(-5 + 8*s2))*Power(t2,4) + 
            (-1 + 3*s + 5*s2)*Power(t2,5)) + 
         Power(t1,2)*t2*(-5 + 49*t2 - 84*Power(t2,2) + 
            24*Power(s2,2)*Power(t2,2) - 6*Power(t2,3) - 3*Power(t2,4) + 
            Power(t2,5) + s2*
             (4 - 9*t2 - 71*Power(t2,2) + 45*Power(t2,3) + 
               7*Power(t2,4)) + 
            s*(1 + (-9 + 4*s2)*t2 + (-25 + 4*s2)*Power(t2,2) + 
               (-23 + 16*s2)*Power(t2,3) + 8*Power(t2,4))) + 
         Power(t2,2)*(-5 + (11 - 3*s)*t2 + (-9 + 5*s)*Power(t2,2) + 
            (17 - 13*s)*Power(t2,3) + (-14 + 3*s)*Power(t2,4) + 
            8*Power(s2,2)*t2*(1 - 2*t2 + 2*Power(t2,2)) + 
            s2*(6 + (-13 + 4*s)*t2 + (15 - 8*s)*Power(t2,2) + 
               (-45 + 8*s)*Power(t2,3) + 21*Power(t2,4))) - 
         s1*(-1 + t1)*(-1 + t2)*
          (Power(t1,3)*(1 + 2*(-3 + s2)*t2 + Power(t2,2)) + 
            Power(t1,2)*(1 + (-7 + 2*s2)*t2 + (23 - 8*s2)*Power(t2,2) - 
               5*Power(t2,3)) + 
            Power(t2,2)*(-3 + 4*t2 + 3*Power(t2,2) + 
               s2*(5 - 2*t2 - 5*Power(t2,2))) + 
            t1*t2*(s2 + 5*s2*Power(t2,2) + 
               2*(-2 + t2 - 7*Power(t2,2) + 2*Power(t2,3)))))*R1(t2))/
     ((-1 + s2)*Power(-1 + t1,2)*Power(t1 - t2,3)*Power(-1 + t2,2)*t2) - 
    (16*(4 + 12*Power(s2,4) + 4*Power(t1,4) - 8*t2 + 2*s*t2 - 2*s1*t2 + 
         16*Power(t2,2) - 9*s*Power(t2,2) + s1*Power(t2,2) - 
         4*Power(t2,3) + 3*s*Power(t2,3) - 
         Power(s2,3)*(30 + s1 + 6*s*(-1 + t1) + 20*t1 - s1*t1 + 
            2*Power(t1,2) - 33*t2 + t1*t2) + 
         Power(t1,3)*(-44 - 4*s*(-1 + t2) + 4*s1*(-1 + t2) + 4*t2 - 
            3*Power(t2,2)) + Power(s2,2)*
          (22 - 39*t1 + 20*Power(t1,2) + Power(t1,3) + 
            3*s*(-1 + t1)*(3 + 2*t1 - 5*t2) - 
            s1*(-1 + t1)*(1 + 6*t1 - 2*t2) - 64*t2 - 43*t1*t2 - 
            Power(t1,2)*t2 + 30*Power(t2,2) - 2*t1*Power(t2,2)) + 
         Power(t1,2)*(-64 + 36*t2 - 2*Power(t2,2) + 3*Power(t2,3) - 
            2*s1*(6 - 7*t2 + 3*Power(t2,2)) + 
            s*(-20 - 2*t2 + 6*Power(t2,2))) + 
         t1*(-44 + 16*t2 + 9*Power(t2,2) - 3*Power(t2,3) + 
            s1*(16 - 16*t2 + 5*Power(t2,2)) + 
            s*(16 + 4*t2 + 3*Power(t2,2) - 3*Power(t2,3))) + 
         s2*(-12 + 112*t1 + 84*Power(t1,2) - 8*Power(t1,3) + 38*t2 - 
            18*t1*t2 + 22*Power(t1,2)*t2 - 2*Power(t1,3)*t2 - 
            38*Power(t2,2) - 26*t1*Power(t2,2) + 
            4*Power(t1,2)*Power(t2,2) + 9*Power(t2,3) - t1*Power(t2,3) + 
            s1*(-1 + t1)*(2 + 14*t1 + 4*Power(t1,2) - 2*t2 - 12*t1*t2 + 
               Power(t2,2)) - 
            2*s*(-1 + t1)*(1 + 2*Power(t1,2) - 9*t2 + 6*Power(t2,2) - 
               3*t1*(1 + 2*t2))))*R2(1 - s2 + t1 - t2))/
     ((-1 + s2)*Power(-1 + t1,2)*(-1 + s2 - t1 + t2)*
       (Power(s2,2) - 4*t1 + 2*s2*t2 + Power(t2,2))) + 
    (8*(8*Power(s2,6)*t2 - 8*Power(t1,5)*(2 + t2) + 
         Power(t2,4)*(-3 + 2*t2)*(1 + (-1 + s)*t2) + 
         t1*(-8 - 4*(-3 + s - s1)*t2 + 2*(-7 + 2*s)*Power(t2,2) + 
            (-19 + 22*s - 2*s1)*Power(t2,3) - 
            3*(-13 + 4*s + s1)*Power(t2,4) + 
            (-15 + s + 2*s1)*Power(t2,5) - 2*(-1 + s)*Power(t2,6)) + 
         4*Power(t1,4)*(20 + 18*t2 + 2*Power(t2,2) + Power(t2,3) + 
            s*(-6 + 2*t2 + Power(t2,2)) - s1*(-6 + 2*t2 + Power(t2,2))) + 
         2*Power(s2,5)*(s1*(-1 + t1)*(1 + t2) + t2*(-5 + 2*s + 21*t2) - 
            t1*(4 + 11*t2 + 2*s*t2 + Power(t2,2))) - 
         Power(t1,3)*(-168 + 20*t2 + 82*Power(t2,2) + 21*Power(t2,3) + 
            4*Power(t2,4) + s1*
             (-8 + 16*t2 + 18*Power(t2,2) - 15*Power(t2,3)) + 
            s*(-8 - 48*t2 + 10*Power(t2,2) + 15*Power(t2,3))) + 
         Power(t1,2)*(s1*(-32 + 20*t2 + 22*Power(t2,2) - 
               13*Power(t2,3) + 3*Power(t2,4) - 2*Power(t2,5)) + 
            2*(48 - 12*t2 - 76*Power(t2,2) + 30*Power(t2,3) + 
               4*Power(t2,4) + Power(t2,5)) + 
            s*(16 - 52*t2 + 2*Power(t2,2) - 7*Power(t2,3) + 
               12*Power(t2,4) + 2*Power(t2,5))) + 
         Power(s2,4)*(2 - (18 + 6*s + s1)*t2 + 
            (-57 + 18*s - 8*s1)*Power(t2,2) + 88*Power(t2,3) + 
            Power(t1,2)*(22 + 25*t2 + 4*Power(t2,2) + 6*s*(1 + t2) - 
               s1*(7 + 6*t2)) + 
            t1*(8 - 87*t2 - 83*Power(t2,2) - 8*Power(t2,3) - 
               6*s*(1 + 3*Power(t2,2)) + s1*(7 + 7*t2 + 8*Power(t2,2)))) \
+ Power(s2,3)*(-(Power(t1,3)*
               (23 + 20*t2 + 4*Power(t2,2) + s*(9 + 4*t2) - 
                 s1*(9 + 4*t2))) + 
            Power(t1,2)*(50 + 210*t2 + 75*Power(t2,2) + 
               12*Power(t2,3) - s1*(13 + 20*t2 + 20*Power(t2,2)) + 
               s*(1 + 36*t2 + 20*Power(t2,2))) + 
            t2*(39 + (-37 + 9*s1)*t2 - (113 + 12*s1)*Power(t2,2) + 
               92*Power(t2,3) + s*(-4 - 21*t2 + 32*Power(t2,2))) + 
            t1*(13 + 115*t2 - 218*Power(t2,2) - 115*Power(t2,3) - 
               12*Power(t2,4) + 
               s*(8 - 28*t2 + Power(t2,2) - 32*Power(t2,3)) + 
               s1*(4 + 16*t2 + 11*Power(t2,2) + 12*Power(t2,3)))) + 
         Power(s2,2)*(4*Power(t1,4)*(4 + s - s1 + 3*t2) - 
            Power(t1,3)*(130 + 155*t2 + 36*Power(t2,2) + 
               8*Power(t2,3) + s*(18 + 29*t2 + 8*Power(t2,2)) - 
               s1*(22 + 29*t2 + 8*Power(t2,2))) + 
            Power(t1,2)*(-52 + 172*t2 + 326*Power(t2,2) + 
               77*Power(t2,3) + 12*Power(t2,4) - 
               s1*(18 - 9*t2 + 16*Power(t2,2) + 24*Power(t2,3)) + 
               s*(10 - 37*t2 + 66*Power(t2,2) + 24*Power(t2,3))) + 
            t2*(-20 + 61*t2 - 15*Power(t2,2) - 97*Power(t2,3) + 
               48*Power(t2,4) + 
               s1*(-4 + 13*Power(t2,2) - 8*Power(t2,3)) + 
               s*(4 - 8*t2 - 27*Power(t2,2) + 28*Power(t2,3))) + 
            t1*(-42 + (127 - 34*s1)*t2 + (313 + 8*s1)*Power(t2,2) + 
               (-222 + 11*s1)*Power(t2,3) + (-67 + 8*s1)*Power(t2,4) - 
               8*Power(t2,5) + 
               s*(4 + 62*t2 - 50*Power(t2,2) + 3*Power(t2,3) - 
                  28*Power(t2,4)))) - 
         s2*(8*Power(t1,5) + t2*
             (-8 - 4*(-3 + s - s1)*t2 + (-21 + 4*s)*Power(t2,2) + 
               (-9 + 15*s - 5*s1)*Power(t2,3) + 
               (33 - 12*s + 2*s1)*Power(t2,4) - 10*Power(t2,5)) - 
            8*Power(t1,4)*(11 + 5*t2 + 2*Power(t2,2) + s*(2 + t2) - 
               s1*(2 + t2)) + 
            Power(t1,3)*(52 + 284*t2 + 137*Power(t2,2) + 
               20*Power(t2,3) + 4*Power(t2,4) + 
               s1*(16 + 20*t2 - 35*Power(t2,2) - 4*Power(t2,3)) + 
               s*(-32 + 20*t2 + 35*Power(t2,2) + 4*Power(t2,3))) - 
            Power(t1,2)*(-80 - 372*t2 + 150*Power(t2,2) + 
               146*Power(t2,3) + 29*Power(t2,4) + 4*Power(t2,5) + 
               s1*(28 - 4*t2 + 9*Power(t2,2) - 12*Power(t2,4)) + 
               s*(-44 + 20*t2 - 45*Power(t2,2) + 48*Power(t2,3) + 
                  12*Power(t2,4))) + 
            t1*(-20 + 160*t2 - 39*Power(t2,2) - 245*Power(t2,3) + 
               98*Power(t2,4) + 11*Power(t2,5) + 2*Power(t2,6) - 
               s1*(4 + 32*t2 - 40*Power(t2,2) - 4*Power(t2,3) + 
                  7*Power(t2,4) + 2*Power(t2,5)) + 
               s*(4 + 8*t2 - 76*Power(t2,2) + 40*Power(t2,3) - 
                  3*Power(t2,4) + 12*Power(t2,5)))))*
       T2(t1,1 - s2 + t1 - t2))/
     ((-1 + s2)*Power(-1 + t1,2)*(-1 + s2 - t1 + t2)*(-t1 + s2*t2)*
       (Power(s2,2) - 4*t1 + 2*s2*t2 + Power(t2,2))) + 
    (8*((-5 + s - s1)*Power(t1,6) + 
         Power(t1,5)*(-30 + 26*s2 + 27*t2 + 7*s2*t2 + 2*Power(t2,2) + 
            s*(1 + t2)*(-5 + 2*s2 + 2*t2) + 
            s1*(-1 + 3*s2 + 6*t2 - 2*s2*t2 - 2*Power(t2,2))) - 
         Power(t1,4)*(17 - 76*s2 + 28*Power(s2,2) - 97*t2 + 39*s2*t2 + 
            30*Power(s2,2)*t2 + 66*Power(t2,2) + 30*s2*Power(t2,2) + 
            2*Power(s2,2)*Power(t2,2) + 4*Power(t2,3) + 
            2*s2*Power(t2,3) + 
            s1*(14 - 2*Power(s2,2)*(-1 + t2) + 4*t2 + 10*Power(t2,2) - 
               6*Power(t2,3) + s2*(-9 + 8*t2 - 8*Power(t2,2))) + 
            s*(4 - 15*t2 - 4*Power(t2,2) + 8*Power(t2,3) + 
               4*Power(s2,2)*(1 + t2) + 2*s2*(-5 + t2 + 6*Power(t2,2)))) \
+ 2*Power(t1,3)*(2 + 15*s2 - 24*Power(s2,2) + 4*Power(s2,3) + 22*t2 - 
            83*s2*t2 - 13*Power(s2,2)*t2 + 14*Power(s2,3)*t2 - 
            67*Power(t2,2) - 3*s2*Power(t2,2) + 
            34*Power(s2,2)*Power(t2,2) + 2*Power(s2,3)*Power(t2,2) + 
            42*Power(t2,3) + 29*s2*Power(t2,3) + 
            3*Power(s2,2)*Power(t2,3) + 3*s2*Power(t2,4) + 
            s1*(-2 + 3*t2 + 7*Power(t2,2) + 2*Power(t2,3) - 
               3*Power(t2,4) - 3*Power(s2,2)*Power(1 + t2,2) + 
               s2*(8 + 15*t2 + 4*Power(t2,2) - 6*Power(t2,3))) + 
            s*(2 + 2*Power(s2,3)*t2 - 9*Power(t2,2) - Power(t2,3) + 
               6*Power(t2,4) + 2*Power(s2,2)*t2*(-1 + 3*t2) + 
               2*s2*(-1 - 2*t2 + 6*Power(t2,3)))) + 
         t2*(8*Power(s2,4)*t2 - 
            Power(t2,3)*(-3 + 2*t2)*(1 + (-1 + s)*t2) - 
            4*Power(s2,3)*(-1 + s1 + t2 - s*t2 + 2*s1*t2 - 
               7*Power(t2,2) + 2*Power(t2,3)) + 
            2*Power(s2,2)*(-4 - 2*(1 + 2*s)*t2 + 
               2*(-2 + s)*Power(t2,2) + (23 - 2*s)*Power(t2,3) - 
               9*Power(t2,4) + 
               s1*(4 + 2*t2 - 5*Power(t2,2) + Power(t2,3))) + 
            s2*(4 - 14*Power(t2,2) - 14*Power(t2,3) + 31*Power(t2,4) - 
               10*Power(t2,5) + 
               2*s*t2*(2 - 2*t2 + 3*Power(t2,2) - 3*Power(t2,3)) + 
               s1*(-4 + 4*t2 + 4*Power(t2,2) - 5*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         Power(t1,2)*(-12 + 20*s2 - 16*Power(s2,2) + 8*Power(s2,3) - 
            8*t2 - 62*s2*t2 + 56*Power(s2,2)*t2 + 40*Power(s2,3)*t2 - 
            8*Power(s2,4)*t2 - 26*Power(t2,2) + 90*s2*Power(t2,2) + 
            158*Power(s2,2)*Power(t2,2) - 48*Power(s2,3)*Power(t2,2) + 
            88*Power(t2,3) + 68*s2*Power(t2,3) - 
            76*Power(s2,2)*Power(t2,3) - 4*Power(s2,3)*Power(t2,3) - 
            51*Power(t2,4) - 64*s2*Power(t2,4) - 
            6*Power(s2,2)*Power(t2,4) + 4*Power(t2,5) - 
            6*s2*Power(t2,5) + 
            s*(4 - 8*t2 + 8*Power(t2,2) - 4*Power(s2,3)*Power(t2,2) + 
               14*Power(t2,3) - 3*Power(t2,4) - 8*Power(t2,5) + 
               4*Power(s2,2)*(1 + t2 + Power(t2,2) - 3*Power(t2,3)) - 
               4*s2*(2 - t2 - Power(t2,2) + 2*Power(t2,3) + 
                  5*Power(t2,4))) + 
            s1*(16 - 4*t2 + 2*Power(t2,2) - 12*Power(t2,3) + 
               3*Power(t2,4) + 2*Power(t2,5) + 
               4*Power(s2,3)*t2*(2 + t2) + 
               Power(s2,2)*(4 - 26*t2 - 4*Power(t2,2) + 6*Power(t2,3)) + 
               s2*(-20 + 4*t2 - 32*Power(t2,2) - 2*Power(t2,3) + 
                  8*Power(t2,4)))) + 
         t1*(-4 - 4*s*t2 + 8*Power(s2,4)*(-1 + t2)*t2 + 16*Power(t2,2) + 
            4*s*Power(t2,2) - 4*s*Power(t2,3) - 16*Power(t2,4) - 
            9*s*Power(t2,4) + 9*Power(t2,5) + 5*s*Power(t2,5) - 
            2*Power(t2,6) + 2*s*Power(t2,6) + 
            4*Power(s2,3)*t2*(2 - s - 22*t2 + 6*Power(t2,2)) + 
            2*Power(s2,2)*(-2 + 2*(-4 + s)*t2 + (18 - 4*s)*Power(t2,2) + 
               (-69 + 4*s)*Power(t2,3) + 2*(14 + s)*Power(t2,4) + 
               Power(t2,5)) + 
            s2*(8 + 4*(4 + s)*t2 + 2*(5 + 2*s)*Power(t2,2) + 
               (2 - 12*s)*Power(t2,3) + 2*(-40 + 7*s)*Power(t2,4) + 
               (39 + 6*s)*Power(t2,5) + 2*Power(t2,6)) + 
            s1*(4 - 4*t2 + 4*Power(s2,3)*(-1 + t2)*t2 - 4*Power(t2,2) + 
               2*Power(t2,3) + 3*Power(t2,4) - 2*Power(t2,5) + 
               Power(s2,2)*(4 + 28*t2 + 6*Power(t2,2) + 4*Power(t2,3) - 
                  2*Power(t2,4)) - 
               s2*(8 + 20*t2 - 12*Power(t2,2) - 10*Power(t2,3) + 
                  3*Power(t2,4) + 2*Power(t2,5)))))*T3(t2,t1))/
     ((-1 + s2)*Power(-1 + t1,2)*Power(t1 - t2,2)*(-1 + s2 - t1 + t2)*
       (-t1 + s2*t2)) - (8*(-16*Power(s2,4)*t2 + 
         Power(-1 + t2,2)*t2*(-3 + 2*t2)*(1 + (-1 + s)*t2) + 
         8*Power(s2,3)*(t1*(2 + (4 + s)*t2) + 
            t2*(8 - s - 5*t2 + Power(t2,2))) + 
         Power(t1,3)*(-35 + s1 + 8*t2 - 5*Power(t2,2) - s1*Power(t2,2) + 
            s*(-21 + 4*t2 + Power(t2,2))) - 
         t1*(-1 + t2)*(s*(20 - 6*t2 + Power(t2,2) - 3*Power(t2,3) + 
               2*Power(t2,4)) - 
            (-1 + t2)*(5 + 31*t2 - 15*Power(t2,2) + 2*Power(t2,3) + 
               s1*(-2 - 3*t2 + 2*Power(t2,2)))) + 
         2*Power(s2,2)*(-4*Power(t1,2)*(4 + s + 2*t2 + s*t2) + 
            s1*(-1 + t1)*(-1 + 4*t2 - 4*Power(t2,2) + Power(t2,3)) + 
            t2*(-23 + 50*t2 - 36*Power(t2,2) + 9*Power(t2,3) + 
               2*s*(8 - 5*t2 + Power(t2,2))) - 
            t1*(32 + 29*t2 - 18*Power(t2,2) + 4*Power(t2,3) + 
               Power(t2,4) + 2*s*
                (-2 + 6*t2 - 5*Power(t2,2) + Power(t2,3)))) + 
         Power(t1,2)*(s*(1 + 22*t2 - 5*Power(t2,2) - 4*Power(t2,3) + 
               2*Power(t2,4)) - 
            (-1 + t2)*(-2*(17 - 18*t2 + 5*Power(t2,2) + Power(t2,3)) + 
               s1*(1 - 5*Power(t2,2) + 2*Power(t2,3)))) + 
         s2*(8*(2 + s)*Power(t1,3) + 
            (-1 + t2)*(2 + (-2 + 22*s - 5*s1)*t2 + 
               (31 - 14*s + 7*s1)*Power(t2,2) + 
               (-41 + 6*s - 2*s1)*Power(t2,3) + 10*Power(t2,4)) + 
            Power(t1,2)*(98 + s1 - 9*t2 - 8*s1*t2 + 9*s1*Power(t2,2) + 
               7*Power(t2,3) - 2*s1*Power(t2,3) + 
               2*s*(11 + 4*t2 - 4*Power(t2,2) + Power(t2,3))) - 
            t1*(2*s*(15 - 7*t2 + 14*Power(t2,2) - 9*Power(t2,3) + 
                  3*Power(t2,4)) + 
               (-1 + t2)*(48 - 27*t2 - 10*Power(t2,2) + 7*Power(t2,3) + 
                  2*Power(t2,4) + 
                  s1*(-1 + 2*t2 + 5*Power(t2,2) - 2*Power(t2,3))))))*
       T4(-1 + t2))/
     ((-1 + s2)*Power(-1 + t1,2)*(-1 + t2)*(-1 + s2 - t1 + t2)*
       (-t1 + s2*t2)) + (8*((5 - s + s1)*Power(t1,4) + 8*Power(s2,4)*t2 + 
         t2*(1 + (-1 + s)*t2)*(3 - 5*t2 + 2*Power(t2,2)) + 
         Power(t1,3)*(15 + 2*s1*Power(-1 + t2,2) - 17*t2 - 
            2*Power(t2,2) + s*(4 + t2 - 2*Power(t2,2))) + 
         2*Power(s2,3)*(s1*(-1 + t1)*(-1 + t2) + t2*(-21 + 2*s + 13*t2) - 
            t1*(4 + 11*t2 + 2*s*t2 + Power(t2,2))) + 
         Power(t1,2)*(43 - 61*t2 + 25*Power(t2,2) + 
            s1*(-5 + 3*t2 + 3*Power(t2,2) - 2*Power(t2,3)) + 
            s*(5 - 5*t2 - 3*Power(t2,2) + 4*Power(t2,3))) + 
         t1*(s*(-8 + 4*t2 + 2*Power(t2,2) + Power(t2,3) - 
               2*Power(t2,4)) + 
            (-1 + t2)*(-1 + 28*t2 - 13*Power(t2,2) + 2*Power(t2,3) + 
               s1*(-2 - 3*t2 + 2*Power(t2,2)))) + 
         Power(s2,2)*(-2 + 44*t2 - 14*s*t2 - 63*Power(t2,2) + 
            10*s*Power(t2,2) + 28*Power(t2,3) - 
            s1*(-1 + t1)*(-2 + 5*t2 - 4*Power(t2,2) + t1*(-5 + 4*t2)) + 
            Power(t1,2)*(22 + 21*t2 + 2*Power(t2,2) + s*(2 + 6*t2)) + 
            t1*(44 + 31*t2 - 43*Power(t2,2) - 4*Power(t2,3) - 
               2*s*(1 - 4*t2 + 5*Power(t2,2)))) - 
         s2*(-2 + (11 - 10*s + 5*s1)*t2 + 
            (15*s - 7*(6 + s1))*Power(t2,2) + 
            (43 - 8*s + 2*s1)*Power(t2,3) - 10*Power(t2,4) + 
            Power(t1,3)*(19 + s - 2*s1*(-2 + t2) + 7*t2 + 2*s*t2) + 
            Power(t1,2)*(s*(9 + 4*t2 - 10*Power(t2,2)) + 
               s1*(1 - 7*t2 + 6*Power(t2,2)) - 
               2*(-32 + 18*t2 + 9*Power(t2,2) + Power(t2,3))) + 
            t1*(39 - 22*t2 - 20*Power(t2,2) + 15*Power(t2,3) + 
               2*Power(t2,4) + 
               s1*(-5 + 4*t2 + Power(t2,2) - 2*Power(t2,3)) + 
               s*(-10 + 4*t2 - 5*Power(t2,2) + 8*Power(t2,3)))))*
       T5(1 - s2 + t1 - t2))/
     ((-1 + s2)*Power(-1 + t1,2)*(-1 + s2 - t1 + t2)*(-t1 + s2*t2)));
   return a;
};
