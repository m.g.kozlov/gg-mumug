#include "../h/nlo_functions.h"
#include "../h/dilog.h"

#define Pi       3.141592653589793238462643383279502884L
#define Power pq




long double ct_bubl(double *vars)
{
    long double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    long double aG=1/Power(4.*Pi,2);
    long double a=aG*((-3*(32*((-2*(2 + s*(-3 + s1) - s2 - Power(s1,2)*s2 + 
                 2*s1*(3 + s2))*(s - s1 + t2))/(-1 + t2) + 
            2*(4 - Power(s1,3) + 2*s2 - 2*t1 + 3*t2 - t1*t2 + 
               3*Power(t2,2) + 
               s*(1 + Power(s1,2) - t1 + s1*(-8 + t1 - t2) + 3*t2) + 
               s1*(1 - 2*s2 - 13*t2 - Power(t2,2) + t1*(3 + t2)) + 
               Power(s1,2)*(-t1 + 2*(5 + t2))) + 
            ((-1 + s1)*(-s + s1 - t2)*
               (2 + 7*s2 + 2*Power(s2,2) - 4*t1 - 5*s2*t1 + 
                 2*Power(t1,2) + 2*Power(s1,2)*(1 + t1) + 5*t2 + 
                 7*s2*t2 - Power(s2,2)*t2 - 5*t1*t2 + 3*Power(t2,2) - 
                 s2*Power(t2,2) + Power(s,2)*(-2 + t1 + t2) + 
                 s*(1 + s2*(2 + s1 - t1) - 3*t2 + Power(t2,2) + 
                    t1*(-3 - 3*s1 + t2)) - 
                 s1*(Power(s2,2) + t1*(-2 + t2) + s2*(2 - 3*t1 + t2))))/
             ((-1 + t1)*(-1 + t2)) + 
            (2*(-1 + s1)*(-s + s1 - t2)*
               (5 + 2*Power(s1,2) + 2*s2 + Power(s2,2) - 6*t1 + 
                 Power(t1,2) + 6*t2 + 2*s2*t2 - 2*t1*t2 + Power(t2,2) - 
                 2*s1*(1 + t2) - Power(s,2)*(-2 + s2 - t1 + t2) + 
                 s*(-4 + Power(s2,2) + s2*(-1 - t1 + t2) + 
                    s1*(-3 + s2 - t1 + t2))))/((-1 + s2)*(-1 + t1)) + 
            ((-1 + s1)*(-s + s1 - t2)*
               (10 + 4*s2 + 2*Power(s2,2) + 2*Power(s1,2)*(1 + s2) - 
                 t1 - 4*s2*t1 + Power(t1,2) - t2 - 2*s2*t2 + 2*t1*t2 - 
                 s2*t1*t2 + Power(t2,2) + s2*Power(t2,2) - 
                 Power(s,2)*(-6 + t1 + t2) - 
                 s*(15 + s1*(4 + 3*s2 - t1) - 5*t1 + Power(t1,2) - 
                    s2*(-4 + t2) - 5*t2 + Power(t2,2)) + 
                 s1*(4 + 2*Power(s2,2) + Power(t1,2) - 4*t2 - 
                    t1*(2 + t2) - s2*(-2 + t1 + t2))))/
             ((-1 + s2)*(-s + s2 - t1)) + 
            (2*(-1 + s1)*(-6 - 2*s2 + 2*t1 + Power(s,2)*t1 + 
                 Power(s1,2)*t1 + 5*t2 - s2*t2 + t1*t2 - Power(t2,2) + 
                 t1*Power(t2,2) - 
                 s*(-7 + s2 + t1*(-1 + 2*s1 - 2*t2) + t2) + 
                 s1*(-7 + s2 + t2 - t1*(1 + 2*t2))))/(s - s2 + t1)) + 
         3*((-8*(s - s1 + t2)*
               (5 + 2*s2 - t1 + Power(s1,2)*(4 + 3*s2 + t1 - 2*t2) + 
                 5*t2 - s2*t2 + 2*t1*t2 - 6*Power(t2,2) + 
                 s*(4 + t1 - s1*t1 + (-5 + s1)*t2) + 
                 s1*(-13 + s2*(-5 + t2) + (5 - 2*t1)*t2 + 2*Power(t2,2)))\
)/(-1 + t2) - 16*(-3*Power(s1,3) + 4*s2 - 4*t1 + 9*t2 - 3*t1*t2 + 
               7*Power(t2,2) + Power(s1,2)*(16 - 3*t1 + 6*t2) + 
               s*(5 + 3*Power(s1,2) - 3*t1 + 3*s1*(-4 + t1 - t2) + 
                  7*t2) - s1*(9 + 4*s2 - 7*t1 + 23*t2 - 3*t1*t2 + 
                  3*Power(t2,2))) - 
            (8*(-1 + s1)*(-4*s2 + 4*t1 + Power(s1,2)*(2 - s2 + 5*t1) + 
                 t2 - t1*t2 + 3*Power(t2,2) - s2*Power(t2,2) + 
                 4*t1*Power(t2,2) + Power(s,2)*(4 - 2*s1 + 3*t1 + t2) + 
                 s*(5 + 2*Power(s1,2) - t1 + 
                    s1*(-10 + s2 - 8*t1 - 3*t2) + 7*t2 - s2*t2 + 
                    7*t1*t2 + Power(t2,2)) + 
                 s1*(-1 - 3*t1 - 5*t2 - 9*t1*t2 + 2*s2*(2 + t2))))/
             (s - s2 + t1) + (8*(-1 + s1)*(-s + s1 - t2)*
               (-1 + s2 - 2*Power(s2,2) + 4*t1 + s2*t1 - 3*Power(t1,2) + 
                 2*Power(s1,2)*(-3 + s2 + t1) - Power(s2,2)*t2 + 
                 2*t1*t2 - s2*t1*t2 - 3*Power(t2,2) + s2*Power(t2,2) + 
                 Power(s,2)*(-4 + 2*s2 + t1 + t2) - 
                 s*(-3 + 2*Power(s2,2) + 2*t1 + Power(t1,2) - 
                    s2*(2 + t1) + 3*s1*(-2 + s2 + t1) + 4*t2 - 
                    4*t1*t2 + Power(t2,2)) + 
                 s1*(2 + 3*Power(s2,2) + Power(t1,2) + 6*t2 - 
                    s2*(3 + t2) - t1*(3 + t2))))/((-1 + s2)*(-1 + t1)) - 
            (8*(-1 + s1)*(-s + s1 - t2)*
               (-2 + 3*t1 - 2*s2*t1 + Power(t1,2) + 3*t2 - 10*s2*t2 + 
                 6*t1*t2 - s2*t1*t2 + Power(t2,2) + s2*Power(t2,2) - 
                 2*Power(s,2)*(-2 + t1 + t2) + 
                 s*(2*s1*(-3 + t1) + 6*t1 - Power(t1,2) + 
                    2*s2*(-3 + t2) + 6*t2 - 2*t1*t2 - Power(t2,2)) + 
                 s1*(Power(t1,2) - 2*t2 - t1*(10 + t2) + 
                    s2*(10 + t1 + t2))))/((-1 + s2)*(-s + s2 - t1)) - 
            (8*(-1 + s1)*(-s + s1 - t2)*
               (1 + 4*s2 + 4*Power(s2,2) - 4*t1 - 8*s2*t1 + 
                 3*Power(t1,2) + 5*t2 + 7*s2*t2 - 2*Power(s2,2)*t2 - 
                 7*t1*t2 + s2*t1*t2 - 4*Power(t2,2) - 2*s2*Power(t2,2) + 
                 2*t1*Power(t2,2) + Power(s,2)*(-2 + t1 + t2) + 
                 s*(3 + s1*(2 + s2 - t1) - 2*t1 + Power(t1,2) - 8*t2 + 
                    t1*t2 + 2*Power(t2,2) + s2*(2 - 2*t1 + t2)) - 
                 s1*(8 + 2*Power(s2,2) - 7*t1 + Power(t1,2) - 6*t2 + 
                    s2*(5 - 5*t1 + 2*t2))))/((-1 + t1)*(-1 + t2)))))/
     (Power(-1 + s1,2)*Power(s - s1 + t2,2)) - 
    (3*(24*(-18 + 8*s + 6*s1 + 10*s2 - 6*s1*s2 + 40*t1 - 8*s*t1 - 
            6*s1*t1 - 24*s2*t1 + 6*s1*s2*t1 - 14*Power(t1,2) + 
            6*s2*Power(t1,2) - 14*t2 + 14*s2*t2 + 6*t1*t2 - 6*s2*t1*t2 + 
            ((-1 + s2)*(11 - 4*s2 - 14*t1 + 7*Power(t1,2) + 
                 s1*(-1 + t1)*(-2 + s2 + 3*t1) - 7*t2 + 5*s2*t2 + 
                 7*t1*t2 - s2*t1*t2 - 4*Power(t1,2)*t2 - 
                 s*(-1 + t1)*(-2 + 3*t1 + t2)))/(-1 + t2) - 
            ((-1 + t1)*(5 - 13*s2 + 4*Power(s2,2) + 5*t1 + 5*s2*t1 - 
                 2*Power(s2,2)*t1 - 6*Power(t1,2) + 2*s2*Power(t1,2) + 
                 s1*(-1 + s2)*(-2 + 3*s2 + t1) - t2 + Power(s2,2)*t2 + 
                 2*t1*t2 - 2*s2*t1*t2 + 
                 s*(4 + (-5 + s2)*t1 + t2 - s2*t2)))/(s - s2 + t1) + 
            ((-1 + s2)*(-1 + t1)*
               (-1 + s2 - 2*Power(s2,2) + 4*t1 + s2*t1 - 
                 3*Power(t1,2) + 2*Power(s1,2)*(-3 + s2 + t1) - 
                 Power(s2,2)*t2 + 2*t1*t2 - s2*t1*t2 - 3*Power(t2,2) + 
                 s2*Power(t2,2) + Power(s,2)*(-4 + 2*s2 + t1 + t2) - 
                 s*(-3 + 2*Power(s2,2) + 2*t1 + Power(t1,2) - 
                    s2*(2 + t1) + 3*s1*(-2 + s2 + t1) + 4*t2 - 
                    4*t1*t2 + Power(t2,2)) + 
                 s1*(2 + 3*Power(s2,2) + Power(t1,2) + 6*t2 - 
                    s2*(3 + t2) - t1*(3 + t2))))/
             ((-1 + s1)*(-s + s1 - t2)) + 
            ((-1 + s2)*(-1 + t1)*
               (4 + 2*s2 - 4*Power(s2,2) - 6*t1 + 2*s2*t1 + 
                 2*Power(t1,2) - Power(s1,2)*(-2 + s2 + t1) + 
                 s1*(2*Power(s2,2) - 2*s2*(2 + t1 - 2*t2) - 
                    (1 + t1)*(-1 + t2)) - 3*t2 + 2*s2*t2 + 
                 2*Power(s2,2)*t2 + 3*t1*t2 - 4*s2*t1*t2 + 
                 2*Power(t1,2)*t2 - 3*Power(t2,2) + s2*Power(t2,2) - 
                 s*(6 - 4*t1 + 2*s2*(-3 + t2) + 2*t2 - 3*t1*t2 + 
                    Power(t2,2) + s1*(2 - t1 + t2))))/
             ((s - s2 + t1)*(s - s1 + t2)) - 
            ((-1 + s2)*(-1 + t1)*
               (12 - s2 - 2*Power(s2,2) - 11*t1 + s2*t1 + Power(t1,2) + 
                 Power(s1,2)*(-2 + s2 + t1) - 11*t2 + 3*s2*t2 + 
                 Power(s2,2)*t2 + 6*t1*t2 - s2*t1*t2 + Power(t2,2) + 
                 s2*Power(t2,2) - 2*Power(s,2)*(-2 + t1 + t2) + 
                 s1*(-1 + Power(s2,2) + Power(t1,2) - t1*(-3 + t2) + 
                    t2 - 2*s2*(t1 + t2)) + 
                 s*(-14 + 6*t1 - Power(t1,2) + 6*t2 - 2*t1*t2 - 
                    Power(t2,2) + s2*(2 + t1 + t2) + 
                    s1*(2 - 2*s2 + t1 + t2))))/((-1 + s1)*(-1 + t2))) + 
         32*((-2*(2 + s*(-3 + s2) - s1*Power(-1 + s2,2) + 6*s2)*
               (-1 + t1))/(s - s2 + t1) + 
            2*(7 - s2 + 2*s*(-1 + t1) - 8*t1 + 8*s2*t1 + 3*Power(t1,2) - 
               s2*Power(t1,2) + s1*(-1 + s2 + t1 - s2*t1) + 3*t2 - 
               3*s2*t2 - t1*t2 + s2*t1*t2) - 
            (2*(-1 + s2)*(2 - 3*s2 - s*Power(-1 + t1,2) + 
                 s1*Power(-1 + t1,2) - 6*t1 + s2*t1 - 4*t2 + 3*t1*t2 - 
                 Power(t1,2)*t2))/(-1 + t2) + 
            ((-1 + s2)*(-1 + t1)*
               (12 + s2 - 2*Power(s2,2) - 3*t1 + 3*s2*t1 + Power(t1,2) + 
                 Power(s1,2)*(-2 + s2 + t1) - 3*t2 - s2*t2 + 
                 Power(s2,2)*t2 + 2*t1*t2 - s2*t1*t2 + Power(t2,2) + 
                 s2*Power(t2,2) - Power(s,2)*(-2 + t1 + t2) + 
                 s1*(1 + Power(s2,2) + Power(t1,2) + 3*t2 - 
                    t1*(1 + t2)) + 
                 s*(-13 + t1 - Power(t1,2) + s2*(2 + t1) + t2 - 
                    Power(t2,2) + s1*(2 - s2 + t2))))/
             ((-1 + s1)*(-1 + t2)) + 
            (2*(-1 + s2)*(-1 + t1)*
               (5 + 2*Power(s1,2) + 2*s2 + Power(s2,2) - 6*t1 + 
                 Power(t1,2) + 6*t2 + 2*s2*t2 - 2*t1*t2 + Power(t2,2) - 
                 2*s1*(1 + t2) - Power(s,2)*(-2 + s2 - t1 + t2) + 
                 s*(-4 + Power(s2,2) + s2*(-1 - t1 + t2) + 
                    s1*(-3 + s2 - t1 + t2))))/((-1 + s1)*(-s + s1 - t2)) - 
            ((-1 + s2)*(-1 + t1)*
               (-3 - 7*s2 - 6*Power(s2,2) + 6*t1 + 5*s2*t1 - 
                 3*Power(t1,2) - 2*Power(s,2)*(1 + t1) - 
                 Power(s1,2)*(-2 + s2 + t1) - 5*t2 - 4*s2*t2 + 
                 Power(s2,2)*t2 + 5*t1*t2 + s2*t1*t2 - 2*Power(t2,2) - 
                 s*(-4 + t1 - 3*s1*t1 + Power(t1,2) + t2 + s1*t2 + 
                    t1*t2 + s2*(-4 - 3*t1 + t2)) + 
                 s1*(-3 + Power(s2,2) + Power(t1,2) - t2 + 
                    s2*(-1 - 4*t1 + 3*t2))))/((s - s2 + t1)*(s - s1 + t2)))\
))/(Power(-1 + s2,2)*Power(-1 + t1,2)) - 
    (3*(32*((-2*(2 + s*(-3 + s2) - s1*Power(-1 + s2,2) + 6*s2)*
               (s - s2 + t1))/(-1 + t1) + 
            (2*(-1 + s2)*(-s + s2 - t1)*
               (5 + Power(s1,2) - 2*s2 + 2*Power(s2,2) + 6*t1 - 
                 2*s2*t1 + Power(t1,2) + 2*s1*(1 + t1) + 
                 s*(-4 + Power(s1,2) + s2*(-3 + t1 - t2) + 
                    s1*(-1 + s2 + t1 - t2)) - 
                 Power(s,2)*(-2 + s1 + t1 - t2) - 6*t2 - 2*t1*t2 + 
                 Power(t2,2)))/((-1 + s1)*(-1 + t2)) + 
            ((-1 + s2)*(-s + s2 - t1)*
               (2 + 2*Power(s2,2) + 5*t1 + 3*Power(t1,2) - 
                 Power(s1,2)*(-2 + s2 + t1) - 4*t2 + 2*s2*t2 + 
                 2*Power(s2,2)*t2 - 5*t1*t2 - s2*t1*t2 + 
                 2*Power(t2,2) + Power(s,2)*(-2 + t1 + t2) - 
                 s1*(-7 - 7*t1 + Power(t1,2) + s2*(2 + t1 - 3*t2) + 
                    5*t2) + s*
                  (1 + Power(t1,2) + s1*(2 + s2 - t2) + t1*(-3 + t2) - 
                    3*t2 - 3*s2*t2)))/((-1 + t1)*(-1 + t2)) + 
            (2*(-1 + s2)*(-6 - 7*s2 + s1*(-2 + s2 - t1) + 5*t1 + 
                 s2*t1 - Power(t1,2) + 2*t2 + Power(s,2)*t2 - s2*t2 + 
                 Power(s2,2)*t2 + t1*t2 - 2*s2*t1*t2 + Power(t1,2)*t2 - 
                 s*(-7 + s1 + t1 - t2 + 2*s2*t2 - 2*t1*t2)))/
             (s - s1 + t2) + 2*
             (4 - 2*s1*(-1 + s2) + s2 + 10*Power(s2,2) - Power(s2,3) + 
               3*t1 - 13*s2*t1 + 2*Power(s2,2)*t1 + 3*Power(t1,2) - 
               s2*Power(t1,2) - 2*t2 + 3*s2*t2 - Power(s2,2)*t2 - 
               t1*t2 + s2*t1*t2 + 
               s*(1 + Power(s2,2) + 3*t1 - t2 + s2*(-8 - t1 + t2))) + 
            ((-1 + s2)*(-s + s2 - t1)*
               (10 + 4*s2 + 2*Power(s2,2) + 2*Power(s1,2)*(1 + s2) - 
                 t1 - 4*s2*t1 + Power(t1,2) - t2 - 2*s2*t2 + 2*t1*t2 - 
                 s2*t1*t2 + Power(t2,2) + s2*Power(t2,2) - 
                 Power(s,2)*(-6 + t1 + t2) - 
                 s*(15 + s1*(4 + 3*s2 - t1) - 5*t1 + Power(t1,2) - 
                    s2*(-4 + t2) - 5*t2 + Power(t2,2)) + 
                 s1*(4 + 2*Power(s2,2) + Power(t1,2) - 4*t2 - 
                    t1*(2 + t2) - s2*(-2 + t1 + t2))))/
             ((-1 + s1)*(-s + s1 - t2))) + 
         3*(-16*(-4*s1*(-1 + s2) - 9*s2 + 16*Power(s2,2) - 
               3*Power(s2,3) + 9*t1 - 23*s2*t1 + 6*Power(s2,2)*t1 + 
               7*Power(t1,2) - 3*s2*Power(t1,2) + 
               s*(5 + 3*Power(s2,2) + 7*t1 - 3*s2*(4 + t1 - t2) - 3*t2) - 
               4*t2 + 7*s2*t2 - 3*Power(s2,2)*t2 - 3*t1*t2 + 3*s2*t1*t2) - 
            (8*(s - s2 + t1)*(5 - 13*s2 + 4*Power(s2,2) + 5*t1 + 
                 5*s2*t1 - 2*Power(s2,2)*t1 - 6*Power(t1,2) + 
                 2*s2*Power(t1,2) + s1*(-1 + s2)*(-2 + 3*s2 + t1) - t2 + 
                 Power(s2,2)*t2 + 2*t1*t2 - 2*s2*t1*t2 + 
                 s*(4 + (-5 + s2)*t1 + t2 - s2*t2)))/(-1 + t1) + 
            (8*(-1 + s2)*(-s + s2 - t1)*
               (-1 + 2*s2 - 6*Power(s2,2) + 
                 Power(s1,2)*(-2 + 3*s2 - t1) + 6*s2*t1 - 
                 3*Power(t1,2) + 4*t2 - 3*s2*t2 + 2*Power(s2,2)*t2 + 
                 2*t1*t2 - s2*t1*t2 - 3*Power(t2,2) + s2*Power(t2,2) + 
                 Power(s,2)*(-4 + 2*s1 + t1 + t2) + 
                 s1*(1 + 2*Power(s2,2) + Power(t1,2) - s2*(3 + t1) + 
                    t2 - t1*t2) - 
                 s*(-3 + 2*Power(s1,2) + 4*t1 + Power(t1,2) + 
                    s1*(-2 + 3*s2 - t2) + 3*s2*(-2 + t2) + 2*t2 - 
                    4*t1*t2 + Power(t2,2))))/((-1 + s1)*(-1 + t2)) - 
            (8*(-1 + s2)*(-s + s2 - t1)*
               (-2 + 3*t1 - 2*s2*t1 + Power(t1,2) + 3*t2 - 10*s2*t2 + 
                 6*t1*t2 - s2*t1*t2 + Power(t2,2) + s2*Power(t2,2) - 
                 2*Power(s,2)*(-2 + t1 + t2) + 
                 s*(2*s1*(-3 + t1) + 6*t1 - Power(t1,2) + 
                    2*s2*(-3 + t2) + 6*t2 - 2*t1*t2 - Power(t2,2)) + 
                 s1*(Power(t1,2) - 2*t2 - t1*(10 + t2) + 
                    s2*(10 + t1 + t2))))/((-1 + s1)*(-s + s1 - t2)) - 
            (8*(-1 + s2)*(-s + s2 - t1)*
               (1 - 8*s2 + 5*t1 + 6*s2*t1 - 4*Power(t1,2) - 
                 2*Power(s1,2)*(-2 + s2 + t1) - 4*t2 + 7*s2*t2 - 
                 7*t1*t2 + 2*Power(t1,2)*t2 + 3*Power(t2,2) - 
                 s2*Power(t2,2) + Power(s,2)*(-2 + t1 + t2) + 
                 s*(3 - 8*t1 + 2*Power(t1,2) + 
                    s1*(2 + s2 + t1 - 2*t2) - s2*(-2 + t2) - 2*t2 + 
                    t1*t2 + Power(t2,2)) + 
                 s1*(4 - 2*Power(t1,2) - 8*t2 + t1*(7 + t2) + 
                    s2*(-5 - 2*t1 + 5*t2))))/((-1 + t1)*(-1 + t2)) - 
            (8*(-1 + s2)*(-s2 + 2*Power(s2,2) + t1 - 5*s2*t1 + 
                 3*Power(t1,2) - 
                 s1*(4 + Power(s2,2) + Power(t1,2) - 2*s2*(2 + t1)) + 
                 4*t2 - 3*s2*t2 + 5*Power(s2,2)*t2 - t1*t2 - 9*s2*t1*t2 + 
                 4*Power(t1,2)*t2 + Power(s,2)*(4 - 2*s2 + t1 + 3*t2) + 
                 s*(5 + 2*Power(s2,2) + Power(t1,2) + 
                    s2*(-10 + s1 - 3*t1 - 8*t2) - t2 + t1*(7 - s1 + 7*t2))\
))/(s - s1 + t2))))/(Power(-1 + s2,2)*Power(s - s2 + t1,2)) - 
    (3*(32*((2*(s - s2 + t1)*(s - s1 + t2)*
               (2*Power(s,2) + s1 + 2*Power(s1,2) + 5*s2 + 2*s1*s2 - 
                 Power(s1,2)*s2 + 4*Power(s2,2) - s1*Power(s2,2) - 
                 4*t1 + 2*s1*t1 - 6*s2*t1 + 4*Power(t1,2) + 
                 s*(1 + s1*(-1 + s2) - 5*s2 + 4*t1) + 
                 (2 - s + s1 + s2)*Power(s1 - s2 + t1 - t2,2) + 
                 (-2 + 2*s1 + Power(s1,2) - 4*s2 - Power(s2,2) + 
                    s*(2 - s1 + s2) + 4*t1)*(-s1 + s2 - t1 + t2)))/
             ((-1 + t1)*(-1 + t2)) + 
            (2*(s - s1 + t2)*
               (-6 - 7*s2 + s1*(-2 + s2 - t1) + 5*t1 + s2*t1 - 
                 Power(t1,2) + 2*t2 + Power(s,2)*t2 - s2*t2 + 
                 Power(s2,2)*t2 + t1*t2 - 2*s2*t1*t2 + Power(t1,2)*t2 - 
                 s*(-7 + s1 + t1 - t2 + 2*s2*t2 - 2*t1*t2)))/(-1 + s2) + 
            2*(8 + Power(s,3) + 6*s2 + s1*(6 + 4*s2 - 6*t1) - 6*t1 - 
               6*t2 - 6*s2*t2 + 8*t1*t2 + 
               Power(s,2)*(8 - s1 - s2 + t1 + t2) + 
               s*(-12 + s1*(-6 + s2 - t1) + 8*t1 + 8*t2 + t1*t2 - 
                  s2*(6 + t2))) + 
            ((s - s2 + t1)*(s - s1 + t2)*
               (3 + 3*s2 - 2*Power(s2,2) + 5*t1 + s2*t1 + 
                 2*Power(t1,2) - Power(s1,2)*(-6 + s2 + t1) - 6*t2 + 
                 Power(s2,2)*t2 - 5*t1*t2 + 3*Power(t2,2) - 
                 s2*Power(t2,2) + 2*Power(s,2)*(1 + t2) + 
                 s1*(7 + s2 + Power(s2,2) + 4*t1 - 3*s2*t1 - 5*t2 + 
                    4*s2*t2 - t1*t2) + 
                 s*(-4 + s1*(-4 + t1 - 3*t2) + t2 - 3*s2*t2 + 
                    Power(t2,2) + t1*(1 + s2 + t2))))/
             ((-1 + s1)*(-1 + t2)) + 
            ((s - s2 + t1)*(s - s1 + t2)*
               (3 + 7*s2 + 6*Power(s2,2) - 6*t1 - 5*s2*t1 + 
                 3*Power(t1,2) + 2*Power(s,2)*(1 + t1) + 
                 Power(s1,2)*(-2 + s2 + t1) + 5*t2 + 4*s2*t2 - 
                 Power(s2,2)*t2 - 5*t1*t2 - s2*t1*t2 + 2*Power(t2,2) + 
                 s1*(3 + s2 - Power(s2,2) + 4*s2*t1 - Power(t1,2) + 
                    t2 - 3*s2*t2) + 
                 s*(-4 + t1 - 3*s1*t1 + Power(t1,2) + t2 + s1*t2 + 
                    t1*t2 + s2*(-4 - 3*t1 + t2))))/((-1 + s2)*(-1 + t1)) \
+ (2*(s - s2 + t1)*(-6 - 2*s2 + 2*t1 + Power(s,2)*t1 + Power(s1,2)*t1 + 
                 5*t2 - s2*t2 + t1*t2 - Power(t2,2) + t1*Power(t2,2) - 
                 s*(-7 + s2 + t1*(-1 + 2*s1 - 2*t2) + t2) + 
                 s1*(-7 + s2 + t2 - t1*(1 + 2*t2))))/(-1 + s1)) + 
         24*(-2*((s - s2 + t1)*
                (-8 + 3*Power(s,2) + 4*s1 - 6*s2 + 10*t1 + 
                  s*(10 - 3*s2 + 3*t1)) + 
               (-4 + 3*Power(s,2) - 6*s2 + 10*t1 + s*(10 - 3*s2 + 3*t1))*
                (-s1 + s2 - t1 + t2)) - 
            ((s - s1 + t2)*((s - s2 + t1)*
                  (5 - 5*s2 + 5*Power(s2,2) + 2*t1 - 9*s2*t1 + 
                    4*Power(t1,2) + s1*(-1 - 4*s2 + 3*t1) + 
                    s*(4 + 3*s1 - 5*s2 + 4*t1)) + 
                 (4 + 3*Power(s,2) + 5*Power(s2,2) - t1 + 
                    4*Power(t1,2) - 3*s2*(1 + 3*t1) + 
                    s*(-1 - 8*s2 + 7*t1))*(-s1 + s2 - t1 + t2)))/(-1 + s2) \
- ((s - s2 + t1)*(-4*s2 + 4*t1 + Power(s1,2)*(2 - s2 + 5*t1) + t2 - 
                 t1*t2 + 3*Power(t2,2) - s2*Power(t2,2) + 
                 4*t1*Power(t2,2) + Power(s,2)*(4 - 2*s1 + 3*t1 + t2) + 
                 s*(5 + 2*Power(s1,2) - t1 + 
                    s1*(-10 + s2 - 8*t1 - 3*t2) + 7*t2 - s2*t2 + 
                    7*t1*t2 + Power(t2,2)) + 
                 s1*(-1 - 3*t1 - 5*t2 - 9*t1*t2 + 2*s2*(2 + t2))))/
             (-1 + s1) - ((s - s2 + t1)*(s - s1 + t2)*
               (-4 - 2*s2 + 4*Power(s2,2) + 6*t1 - 2*s2*t1 - 
                 2*Power(t1,2) + Power(s1,2)*(-2 + s2 + t1) + 
                 s1*(-2*Power(s2,2) + 2*s2*(2 + t1 - 2*t2) + 
                    (1 + t1)*(-1 + t2)) + 3*t2 - 2*s2*t2 - 
                 2*Power(s2,2)*t2 - 3*t1*t2 + 4*s2*t1*t2 - 
                 2*Power(t1,2)*t2 + 3*Power(t2,2) - s2*Power(t2,2) + 
                 s*(6 - 4*t1 + 2*s2*(-3 + t2) + 2*t2 - 3*t1*t2 + 
                    Power(t2,2) + s1*(2 - t1 + t2))))/
             ((-1 + s2)*(-1 + t1)) - 
            ((s - s2 + t1)*(s - s1 + t2)*
               (-4 - s2 - 2*Power(s2,2) + 3*t1 + s2*t1 + 3*Power(t1,2) - 
                 2*Power(s1,2)*(-2 + s2 + t1) + 6*t2 - s2*t2 + 
                 Power(s2,2)*t2 - 3*t1*t2 + s2*t1*t2 - 2*Power(t2,2) - 
                 2*t1*Power(t2,2) + 
                 s*(6 + 2*s1*(-3 + t1) + 2*t1 + Power(t1,2) + 
                    s2*(2 + t1 - t2) - 4*t2 - 3*t1*t2) + 
                 s1*(Power(s2,2) - Power(t1,2) - 2*(1 + t2) + 
                    s2*(4 - 4*t1 + 2*t2) + t1*(-2 + 4*t2))))/
             ((-1 + s1)*(-1 + t2)) + 
            ((s - s2 + t1)*(s - s1 + t2)*
               (-s2 - 4*Power(s2,2) + 2*t1 + 5*s2*t1 + 2*Power(s2,2)*t1 - 
                 2*Power(t1,2) - 2*s2*Power(t1,2) + 2*t2 + s2*t2 + 
                 Power(s2,2)*t2 - 2*s2*t1*t2 - 2*Power(t2,2) + 
                 2*Power(s,2)*(-3 + t1 + t2) + 
                 Power(s1,2)*(-4 + s2 + t1 + 2*t2) + 
                 s1*(-1 + Power(s2,2) + t1 + 5*t2 - 2*t1*t2 - 
                    2*Power(t2,2) + s2*(-4 + t1 + t2)) - 
                 s*(3*s1*(-2 + t1 + t2) + 3*s2*(-2 + t1 + t2) + 
                    4*(-1 + t1 + t2 - t1*t2))))/((-1 + t1)*(-1 + t2)))))/
     (Power(s - s2 + t1,2)*Power(s - s1 + t2,2)) - 
    (3*(32*((2*(-1 + t2)*(-2 + 3*s2 + s*Power(-1 + t1,2) - 
                 s1*Power(-1 + t1,2) + 6*t1 - s2*t1 + 4*t2 - 3*t1*t2 + 
                 Power(t1,2)*t2))/(-1 + s2) + 
            (2*(-1 + t1)*(-2 - s2 + 4*t1 - s1*(-3 + t2) + 
                 s*Power(-1 + t2,2) + 6*t2 + 2*s2*t2 - 3*t1*t2 - 
                 s2*Power(t2,2) + t1*Power(t2,2)))/(-1 + s1) + 
            (2*(-1 + t1)*(-1 + t2)*
               (2*Power(s,2) + 3*s1 + 2*Power(s1,2) + 3*s2 + 4*s1*s2 - 
                 Power(s1,2)*s2 + 2*Power(s2,2) - s1*Power(s2,2) - 
                 4*t1 - 2*s1*t1 - 2*s2*t1 + 4*Power(t1,2) + 
                 s*(1 + s1*(-3 + s2) - 3*s2 + 4*t1) + 
                 (2 - s + s1 + s2)*Power(t1 - t2,2) + 
                 (-2 - 2*s1 - Power(s1,2) + s*(2 + s1 - s2) + 
                    Power(s2,2) + 4*t1)*(-t1 + t2)))/
             ((s - s2 + t1)*(s - s1 + t2)) + 
            ((-1 + t1)*(-1 + t2)*
               (2 + 2*Power(s2,2) + 5*t1 + 3*Power(t1,2) - 
                 Power(s1,2)*(-2 + s2 + t1) - 4*t2 + 2*s2*t2 + 
                 2*Power(s2,2)*t2 - 5*t1*t2 - s2*t1*t2 + 
                 2*Power(t2,2) + Power(s,2)*(-2 + t1 + t2) - 
                 s1*(-7 - 7*t1 + Power(t1,2) + s2*(2 + t1 - 3*t2) + 
                    5*t2) + s*
                  (1 + Power(t1,2) + s1*(2 + s2 - t2) + t1*(-3 + t2) - 
                    3*t2 - 3*s2*t2)))/((-1 + s2)*(-s + s2 - t1)) + 
            2*(s*(-1 + t1)*(-1 + t2) - 
               2*(-2 + s1*(-1 + t1) + t1 + s2*(-1 + t2) + t2 - 4*t1*t2)) \
+ ((-1 + t1)*(-1 + t2)*(2 + 7*s2 + 2*Power(s2,2) - 4*t1 - 5*s2*t1 + 
                 2*Power(t1,2) + 2*Power(s1,2)*(1 + t1) + 5*t2 + 
                 7*s2*t2 - Power(s2,2)*t2 - 5*t1*t2 + 3*Power(t2,2) - 
                 s2*Power(t2,2) + Power(s,2)*(-2 + t1 + t2) + 
                 s*(1 + s2*(2 + s1 - t1) - 3*t2 + Power(t2,2) + 
                    t1*(-3 - 3*s1 + t2)) - 
                 s1*(Power(s2,2) + t1*(-2 + t2) + s2*(2 - 3*t1 + t2))))/
             ((-1 + s1)*(-s + s1 - t2))) + 
         24*(-4 - 6*s - 8*s1 - 8*s2 + 12*t1 + 6*s*t1 + 8*s1*t1 + 12*t2 + 
            6*s*t2 + 8*s2*t2 - 20*t1*t2 - 6*s*t1*t2 - 
            ((-1 + t2)*(-11 + 4*s2 + 14*t1 - 7*Power(t1,2) - 
                 s1*(-1 + t1)*(-2 + s2 + 3*t1) + 7*t2 - 5*s2*t2 - 
                 7*t1*t2 + s2*t1*t2 + 4*Power(t1,2)*t2 + 
                 s*(-1 + t1)*(-2 + 3*t1 + t2)))/(-1 + s2) - 
            ((-1 + t1)*(-11 - 2*s2 + 7*t1 + 14*t2 + 5*s2*t2 - 7*t1*t2 - 
                 7*Power(t2,2) - 3*s2*Power(t2,2) + 4*t1*Power(t2,2) + 
                 s*(-1 + t2)*(-2 + t1 + 3*t2) + 
                 s1*(4 + s2 + t1*(-5 + t2) - s2*t2)))/(-1 + s1) - 
            ((-1 + t1)*(-1 + t2)*
               (1 + 4*s2 + 4*Power(s2,2) - 4*t1 - 8*s2*t1 + 
                 3*Power(t1,2) + 5*t2 + 7*s2*t2 - 2*Power(s2,2)*t2 - 
                 7*t1*t2 + s2*t1*t2 - 4*Power(t2,2) - 2*s2*Power(t2,2) + 
                 2*t1*Power(t2,2) + Power(s,2)*(-2 + t1 + t2) + 
                 s*(3 + s1*(2 + s2 - t1) - 2*t1 + Power(t1,2) - 8*t2 + 
                    t1*t2 + 2*Power(t2,2) + s2*(2 - 2*t1 + t2)) - 
                 s1*(8 + 2*Power(s2,2) - 7*t1 + Power(t1,2) - 6*t2 + 
                    s2*(5 - 5*t1 + 2*t2))))/((-1 + s1)*(-s + s1 - t2)) - 
            ((-1 + t1)*(-1 + t2)*
               (1 - 8*s2 + 5*t1 + 6*s2*t1 - 4*Power(t1,2) - 
                 2*Power(s1,2)*(-2 + s2 + t1) - 4*t2 + 7*s2*t2 - 
                 7*t1*t2 + 2*Power(t1,2)*t2 + 3*Power(t2,2) - 
                 s2*Power(t2,2) + Power(s,2)*(-2 + t1 + t2) + 
                 s*(3 - 8*t1 + 2*Power(t1,2) + 
                    s1*(2 + s2 + t1 - 2*t2) - s2*(-2 + t2) - 2*t2 + 
                    t1*t2 + Power(t2,2)) + 
                 s1*(4 - 2*Power(t1,2) - 8*t2 + t1*(7 + t2) + 
                    s2*(-5 - 2*t1 + 5*t2))))/((-1 + s2)*(-s + s2 - t1)) - 
            ((-1 + t1)*(-1 + t2)*
               (s2 + 4*Power(s2,2) - 2*t1 - 5*s2*t1 - 2*Power(s2,2)*t1 + 
                 2*Power(t1,2) + 2*s2*Power(t1,2) - 2*t2 - s2*t2 - 
                 Power(s2,2)*t2 + 2*s2*t1*t2 + 2*Power(t2,2) - 
                 2*Power(s,2)*(-3 + t1 + t2) - 
                 Power(s1,2)*(-4 + s2 + t1 + 2*t2) - 
                 s1*(-1 + Power(s2,2) + t1 + 5*t2 - 2*t1*t2 - 
                    2*Power(t2,2) + s2*(-4 + t1 + t2)) + 
                 s*(3*s1*(-2 + t1 + t2) + 3*s2*(-2 + t1 + t2) + 
                    4*(-1 + t1 + t2 - t1*t2))))/
             ((s - s2 + t1)*(s - s1 + t2)))))/
     (Power(-1 + t1,2)*Power(-1 + t2,2)) + 
    (8*(-6 - 33*s2 + 67*Power(s2,2) - 40*Power(s2,3) + 12*Power(s2,4) + 
         24*t1 - 20*s2*t1 - 6*Power(s2,2)*t1 + 18*Power(s2,3)*t1 - 
         16*Power(s2,4)*t1 - 31*Power(t1,2) + 49*s2*Power(t1,2) - 
         54*Power(s2,2)*Power(t1,2) + 32*Power(s2,3)*Power(t1,2) + 
         4*Power(s2,4)*Power(t1,2) + 15*Power(t1,3) + 9*s2*Power(t1,3) - 
         14*Power(s2,2)*Power(t1,3) - 10*Power(s2,3)*Power(t1,3) - 
         3*Power(t1,4) - 4*s2*Power(t1,4) + 7*Power(s2,2)*Power(t1,4) + 
         Power(t1,5) - s2*Power(t1,5) + 
         2*Power(s1,4)*(-1 + t1)*(4 + s2*(-3 + t1) - 3*t1 + Power(t1,2))*
          (-1 + t2) + 35*t2 + 57*s2*t2 - 174*Power(s2,2)*t2 + 
         77*Power(s2,3)*t2 - 18*Power(s2,4)*t2 - 226*t1*t2 + 
         348*s2*t1*t2 - 69*Power(s2,2)*t1*t2 - 25*Power(s2,3)*t1*t2 + 
         24*Power(s2,4)*t1*t2 + 15*Power(t1,2)*t2 - 
         106*s2*Power(t1,2)*t2 + 116*Power(s2,2)*Power(t1,2)*t2 - 
         51*Power(s2,3)*Power(t1,2)*t2 - 6*Power(s2,4)*Power(t1,2)*t2 - 
         19*Power(t1,3)*t2 - 29*s2*Power(t1,3)*t2 + 
         33*Power(s2,2)*Power(t1,3)*t2 + 15*Power(s2,3)*Power(t1,3)*t2 + 
         28*Power(t1,4)*t2 - 15*s2*Power(t1,4)*t2 - 
         10*Power(s2,2)*Power(t1,4)*t2 - Power(t1,5)*t2 + 
         s2*Power(t1,5)*t2 - 55*Power(t2,2) + 292*s2*Power(t2,2) - 
         87*Power(s2,2)*Power(t2,2) - 40*Power(s2,3)*Power(t2,2) + 
         6*Power(s2,4)*Power(t2,2) - 24*t1*Power(t2,2) - 
         381*s2*t1*Power(t2,2) + 221*Power(s2,2)*t1*Power(t2,2) + 
         11*Power(s2,3)*t1*Power(t2,2) - 8*Power(s2,4)*t1*Power(t2,2) + 
         207*Power(t1,2)*Power(t2,2) - s2*Power(t1,2)*Power(t2,2) - 
         145*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         18*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         2*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         99*Power(t1,3)*Power(t2,2) + 81*s2*Power(t1,3)*Power(t2,2) + 
         8*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         5*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         3*Power(t1,4)*Power(t2,2) - 7*s2*Power(t1,4)*Power(t2,2) + 
         3*Power(s2,2)*Power(t1,4)*Power(t2,2) + 29*Power(t2,3) - 
         105*s2*Power(t2,3) - 26*Power(s2,2)*Power(t2,3) + 
         3*Power(s2,3)*Power(t2,3) + 2*t1*Power(t2,3) + 
         149*s2*t1*Power(t2,3) - 10*Power(s2,2)*t1*Power(t2,3) - 
         4*Power(s2,3)*t1*Power(t2,3) - 54*Power(t1,2)*Power(t2,3) - 
         13*s2*Power(t1,2)*Power(t2,3) + 
         15*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         35*Power(t1,3)*Power(t2,3) - 17*s2*Power(t1,3)*Power(t2,3) - 
         3*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         4*Power(t1,4)*Power(t2,3) + 2*s2*Power(t1,4)*Power(t2,3) - 
         3*Power(t2,4) + 9*s2*Power(t2,4) + 4*t1*Power(t2,4) - 
         12*s2*t1*Power(t2,4) - Power(t1,2)*Power(t2,4) + 
         3*s2*Power(t1,2)*Power(t2,4) - 
         Power(s,3)*(-1 + t1)*(-1 + t2)*
          (-16 + 23*t1 - 6*Power(t1,2) + 
            2*s2*(8 + 2*Power(t1,2) + t1*(-9 + t2) - 3*t2) + 15*t2 - 
            6*t1*t2 + s1*(20 + 10*s2*(-2 + t1) - 15*t1 + 2*Power(t1,2) - 
               9*t2 + 4*t1*t2)) + 
         Power(s1,3)*(-1 + t1)*
          (34 + Power(t1,3)*(-1 + t2) - 12*t2 - 22*Power(t2,2) + 
            Power(s2,2)*(-25 - 2*t1*(-5 + t2) + 9*t2) + 
            Power(t1,2)*(18 - 5*t2 - 5*Power(t2,2)) + 
            t1*(-36 + 3*t2 + 17*Power(t2,2)) + 
            s2*(56 - 61*t2 + 21*Power(t2,2) + Power(t1,2)*(-19 + 11*t2) + 
               t1*(31 - 16*t2 - 7*Power(t2,2)))) + 
         Power(s1,2)*(74 - 2*Power(s2,3)*(13 - 6*t1 + Power(t1,2))*
             (-1 + t2) - 7*t2 - 50*Power(t2,2) - 17*Power(t2,3) + 
            Power(t1,4)*(19 - 4*t2 - 3*Power(t2,2)) + 
            Power(t1,3)*(-47 - 46*t2 + 42*Power(t2,2) + 3*Power(t2,3)) - 
            Power(t1,2)*(12 - 168*t2 + 121*Power(t2,2) + 
               15*Power(t2,3)) + 
            t1*(-202 - 79*t2 + 140*Power(t2,2) + 29*Power(t2,3)) + 
            Power(s2,2)*(-123 + 20*t2 - 9*Power(t2,2) + 
               Power(t1,3)*(7 + 5*t2) + 
               t1*(38 + 2*t2 - 20*Power(t2,2)) + 
               Power(t1,2)*(-26 - 27*t2 + 5*Power(t2,2))) + 
            s2*(63 + 3*Power(t1,4)*(-5 + t2) + 130*t2 - 105*Power(t2,2) + 
               24*Power(t2,3) + 
               Power(t1,3)*(42 + 21*t2 - 27*Power(t2,2)) + 
               t1*(174 - 127*t2 + 77*Power(t2,2) - 32*Power(t2,3)) + 
               Power(t1,2)*(-8 - 43*t2 + 71*Power(t2,2) + 8*Power(t2,3)))) \
+ s1*(-42 + 3*Power(t1,5)*(-1 + t2) + 
            2*Power(s2,4)*(3 - 4*t1 + Power(t1,2))*(-1 + t2) - 32*t2 + 
            83*Power(t2,2) - 12*Power(t2,3) + 3*Power(t2,4) + 
            Power(t1,3)*(-96 + 204*t2 + 23*Power(t2,2) - 
               23*Power(t2,3)) + 
            Power(t1,4)*(2 - 39*t2 - Power(t2,2) + 2*Power(t2,3)) + 
            t1*(89 + 385*t2 - 136*Power(t2,2) - 18*Power(t2,3) - 
               4*Power(t2,4)) + 
            Power(t1,2)*(218 - 385*t2 - 9*Power(t2,2) + 43*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,3)*(-49 - 7*Power(t1,3)*(-1 + t2) - 4*t2 + 
               53*Power(t2,2) + 
               Power(t1,2)*(-25 + 14*t2 + 11*Power(t2,2)) - 
               3*t1*(-17 + t2 + 16*Power(t2,2))) + 
            Power(s2,2)*(55 + 8*Power(t1,4)*(-1 + t2) + 207*t2 + 
               18*Power(t2,2) + 36*Power(t2,3) - 
               3*Power(t1,3)*(-8 + 11*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(-107 + 103*t2 + 109*Power(t2,2) + 
                  3*Power(t2,3)) - 
               t1*(-140 + 181*t2 + 76*Power(t2,2) + 15*Power(t2,3))) + 
            s2*(65 - 3*Power(t1,5)*(-1 + t2) - 333*t2 - 107*Power(t2,2) + 
               68*Power(t2,3) - 9*Power(t2,4) + 
               Power(t1,4)*(3 + 28*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(65 - 151*t2 + 9*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(-52 + 229*t2 - 162*Power(t2,2) + 
                  12*Power(t2,3) - 3*Power(t2,4)) + 
               t1*(-340 - 10*t2 + 255*Power(t2,2) - 101*Power(t2,3) + 
                  12*Power(t2,4)))) + 
         Power(s,2)*(-93 - 71*t1 - 38*Power(t1,2) + 34*Power(t1,3) - 
            131*t2 + 156*t1*t2 + 37*Power(t1,2)*t2 - 30*Power(t1,3)*t2 + 
            7*Power(t2,2) + 48*t1*Power(t2,2) - 
            67*Power(t1,2)*Power(t2,2) + 20*Power(t1,3)*Power(t2,2) - 
            3*Power(t2,3) + 3*t1*Power(t2,3) + 
            2*Power(s2,2)*(1 + Power(t1,3)*(-1 + t2) - 10*t2 + 
               9*Power(t2,2) - 6*t1*(3 - 5*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(10 - 13*t2 + 3*Power(t2,2))) + 
            s2*(62 - Power(t1,4)*(-1 + t2) + 157*t2 - 2*Power(t2,2) + 
               3*Power(t2,3) + 
               Power(t1,3)*(-21 + 7*t2 - 10*Power(t2,2)) - 
               4*t1*(-11 + 46*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(2 + 37*t2 + 28*Power(t2,2) + Power(t2,3))) + 
            Power(s1,2)*(-1 + t1)*
             (-4 + 6*Power(t1,2)*(-1 + t2) + 23*t2 - 3*Power(t2,2) + 
               2*t1*(7 - 12*t2 + Power(t2,2)) + 
               s2*(7 - 23*t2 + 4*t1*(-1 + 3*t2))) + 
            s1*(49 + Power(t1,4)*(-1 + t2) + 
               2*Power(s2,2)*(3 - 17*t1 + 6*Power(t1,2))*(-1 + t2) + 
               71*t2 - 24*Power(t2,2) + 
               Power(t1,3)*(-11 + 9*t2 - 10*Power(t2,2)) - 
               Power(t1,2)*(3 - 19*t2 - 25*Power(t2,2) + Power(t2,3)) + 
               t1*(134 - 132*t2 + Power(t2,2) + Power(t2,3)) + 
               s2*(-11 - 83*t2 - 2*Power(t2,2) + 6*Power(t1,3)*(1 + t2) + 
                  Power(t1,2)*(15 - 61*t2 + 6*Power(t2,2)) - 
                  2*t1*(49 - 61*t2 + 14*Power(t2,2))))) - 
         s*(75 + 42*t1 + 35*Power(t1,2) + 39*Power(t1,3) - 
            20*Power(t1,4) - 3*Power(t1,5) + 47*t2 + 308*t1*t2 - 
            226*Power(t1,2)*t2 + 3*Power(t1,3)*t2 + Power(t1,4)*t2 + 
            3*Power(t1,5)*t2 + 104*Power(t2,2) - 263*t1*Power(t2,2) + 
            117*Power(t1,2)*Power(t2,2) + 7*Power(t1,3)*Power(t2,2) - 
            5*Power(t1,4)*Power(t2,2) - 15*Power(t2,3) + 
            9*t1*Power(t2,3) + 3*Power(t1,2)*Power(t2,3) - 
            5*Power(t1,3)*Power(t2,3) + 9*Power(t2,4) - 
            12*t1*Power(t2,4) + 3*Power(t1,2)*Power(t2,4) - 
            2*Power(s2,3)*(1 + Power(t1,3)*(-1 + t2) + 8*t2 - 
               9*Power(t2,2) + Power(t1,2)*(-1 + 4*t2 - 3*Power(t2,2)) + 
               3*t1*(3 - 7*t2 + 4*Power(t2,2))) - 
            s2*(100 + 3*Power(t1,5)*(-1 + t2) + 309*t2 + 2*Power(t2,2) + 
               29*Power(t2,3) + 
               Power(t1,4)*(-22 + 6*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(32 - 182*t2 + 165*Power(t2,2) - 
                  15*Power(t2,3)) + 
               Power(t1,3)*(30 - t2 - 12*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(119 - 63*t2 - 115*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s2,2)*(50 + 5*Power(t1,4)*(-1 + t2) + 191*t2 - 
               27*Power(t2,2) + 6*Power(t2,3) + 
               Power(t1,3)*(-11 + 2*t2 - 15*Power(t2,2)) + 
               Power(t1,2)*(27 - 13*t2 + 52*Power(t2,2) + 
                  2*Power(t2,3)) - 
               t1*(-43 + 169*t2 + 2*Power(t2,2) + 8*Power(t2,3))) + 
            Power(s1,3)*(-1 + t1)*
             (6*Power(t1,2)*(-1 + t2) + t1*(5 - 11*t2 - 2*Power(t2,2)) + 
               2*(4 + t2 + 3*Power(t2,2)) + s2*(-7 - 9*t2 + 4*t1*(1 + t2))\
) + Power(s1,2)*(39 + 2*Power(t1,4)*(-1 + t2) + 82*t2 - 24*Power(t2,2) + 
               15*Power(t2,3) + Power(t1,3)*(2 + 9*t2 - 15*Power(t2,2)) + 
               Power(t1,2)*(-28 + 12*t2 + 35*Power(t2,2) + 
                  5*Power(t2,3)) - 
               t1*(-157 + 137*t2 + 4*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,2)*(25 - 9*t2 + 8*Power(t1,2)*t2 - 
                  3*t1*(3 + 5*t2)) + 
               s2*(-97 - 19*t2 - 12*Power(t2,2) + 
                  Power(t1,3)*(-9 + 13*t2) + 
                  t1*(-23 + 68*t2 - 17*Power(t2,2)) + 
                  Power(t1,2)*(41 - 78*t2 + 5*Power(t2,2)))) + 
            s1*(-73 + 4*Power(s2,3)*(-2 - 3*t1 + Power(t1,2))*(-1 + t2) - 
               206*t2 - 15*Power(t2,2) - 13*Power(t2,3) - 9*Power(t2,4) + 
               Power(t1,4)*(19 - 4*t2 - 3*Power(t2,2)) + 
               Power(t1,3)*(-35 - 18*t2 + 41*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(44 + 23*t2 - 149*Power(t2,2) - 
                  27*Power(t2,3) - 3*Power(t2,4)) + 
               t1*(-291 + 101*t2 + 174*Power(t2,2) + 40*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,2)*(-98 - 40*t2 + 42*Power(t2,2) + 
                  Power(t1,3)*(7 + 5*t2) + 
                  t1*(-22 + 81*t2 - 63*Power(t2,2)) + 
                  Power(t1,2)*(-7 - 46*t2 + 13*Power(t2,2))) + 
               s2*(174 + 2*Power(t1,4)*(-7 + t2) + 280*t2 - 
                  78*Power(t2,2) + 36*Power(t2,3) + 
                  Power(t1,3)*(41 - 19*t2 - 30*Power(t2,2)) + 
                  t1*(236 - 330*t2 + 76*Power(t2,2) - 14*Power(t2,3)) + 
                  Power(t1,2)*(-93 + 155*t2 + 88*Power(t2,2) + 
                     2*Power(t2,3)))))))/
     ((-1 + s1)*(-1 + s2)*(-s + s2 - t1)*Power(-1 + t1,3)*(-s + s1 - t2)*
       Power(-1 + t2,2)) + (8*(-140 + 194*s2 - 62*Power(s2,2) + 
         28*Power(s2,3) - 20*Power(s2,4) + 110*t1 - 161*s2*t1 + 
         18*Power(s2,2)*t1 + 15*Power(s2,3)*t1 + 18*Power(s2,4)*t1 + 
         23*Power(t1,2) - 2*s2*Power(t1,2) + 17*Power(s2,2)*Power(t1,2) - 
         40*Power(s2,3)*Power(t1,2) + 2*Power(s2,4)*Power(t1,2) + 
         4*Power(t1,3) - 27*s2*Power(t1,3) + 26*Power(s2,2)*Power(t1,3) - 
         3*Power(s2,3)*Power(t1,3) + 3*Power(t1,4) - 4*s2*Power(t1,4) + 
         Power(s2,2)*Power(t1,4) + 
         2*Power(s1,4)*(-1 + s2)*
          (-3*Power(s2,2) + Power(s2,3) + t1*(-4 + 3*t1) - 
            s2*(-4 + Power(t1,2))) + 76*t2 - 207*s2*t2 + 
         161*Power(s2,2)*t2 - 146*Power(s2,3)*t2 + 44*Power(s2,4)*t2 - 
         85*t1*t2 + 235*s2*t1*t2 - 28*Power(s2,2)*t1*t2 + 
         95*Power(s2,3)*t1*t2 - 67*Power(s2,4)*t1*t2 + 
         2*Power(s2,5)*t1*t2 - 168*Power(t1,2)*t2 + 
         115*s2*Power(t1,2)*t2 - 102*Power(s2,2)*Power(t1,2)*t2 + 
         58*Power(s2,3)*Power(t1,2)*t2 + 9*Power(s2,4)*Power(t1,2)*t2 + 
         47*Power(t1,3)*t2 - 31*s2*Power(t1,3)*t2 + 
         Power(s2,2)*Power(t1,3)*t2 - 9*Power(s2,3)*Power(t1,3)*t2 - 
         6*Power(t1,4)*t2 + 8*s2*Power(t1,4)*t2 - 
         2*Power(s2,2)*Power(t1,4)*t2 + 48*Power(t2,2) + 
         52*s2*Power(t2,2) - 141*Power(s2,2)*Power(t2,2) + 
         187*Power(s2,3)*Power(t2,2) - 88*Power(s2,4)*Power(t2,2) + 
         14*Power(s2,5)*Power(t2,2) - 24*t1*Power(t2,2) + 
         28*s2*t1*Power(t2,2) - 192*Power(s2,2)*t1*Power(t2,2) + 
         18*Power(s2,3)*t1*Power(t2,2) + 24*Power(s2,4)*t1*Power(t2,2) - 
         6*Power(s2,5)*t1*Power(t2,2) + 53*Power(t1,2)*Power(t2,2) - 
         25*s2*Power(t1,2)*Power(t2,2) + 
         102*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         50*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         8*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         2*Power(t1,3)*Power(t2,2) - 20*s2*Power(t1,3)*Power(t2,2) + 
         12*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         2*Power(s2,3)*Power(t1,3)*Power(t2,2) + 16*Power(t2,3) - 
         39*s2*Power(t2,3) + 50*Power(s2,2)*Power(t2,3) - 
         35*Power(s2,3)*Power(t2,3) + 8*Power(s2,4)*Power(t2,3) - 
         t1*Power(t2,3) - 8*s2*t1*Power(t2,3) + 
         9*Power(s2,2)*t1*Power(t2,3) + 2*Power(s2,3)*t1*Power(t2,3) - 
         2*Power(s2,4)*t1*Power(t2,3) - 10*Power(t1,2)*Power(t2,3) + 
         18*s2*Power(t1,2)*Power(t2,3) - 
         10*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         2*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         Power(s,4)*(-1 + s2)*
          (s2*(8 + 5*Power(t1,2) + t1*(-14 + s1 - 11*t2) + 12*t2 - 
               s1*t2) - 3*(4 + 5*Power(t1,2) + t1*(-10 + s1 - 11*t2) + 
               12*t2 - s1*t2)) + 
         Power(s1,3)*(-1 + s2)*
          (-3*Power(t1,3) + Power(t1,2)*(20 - 9*t2) + 
            Power(s2,3)*(-3 + 11*t1 - 4*t2) + 16*(-1 + t2) + 
            2*t1*(27 + t2) + Power(s2,2)*
             (14 - 12*Power(t1,2) + 15*t2 + t1*(-27 + 2*t2)) + 
            s2*(-30 + Power(t1,3) + 2*t1*(-21 + t2) - 26*t2 + 
               Power(t1,2)*(33 + 2*t2))) + 
         Power(s1,2)*(-9*Power(t1,4) + 13*Power(t1,3)*(-6 + t2) + 
            Power(s2,5)*(-4 + 6*t1 + 8*t2) + 
            Power(t1,2)*(-72 + 69*t2 - 11*Power(t2,2)) + 
            t1*(156 + 11*t2 - 9*Power(t2,2)) + 
            16*(-5 + 3*t2 + 2*Power(t2,2)) - 
            Power(s2,4)*(-38 + 9*Power(t1,2) + 48*t2 - 8*Power(t2,2) + 
               t1*(53 + 14*t2)) + 
            Power(s2,3)*(6*Power(t1,3) + Power(t1,2)*(89 - 3*t2) + 
               t1*(94 + 96*t2 - 4*Power(t2,2)) - 
               10*(15 - 8*t2 + 4*Power(t2,2))) - 
            Power(s2,2)*(-228 + 3*Power(t1,4) + 
               Power(t1,3)*(44 - 9*t2) + 33*t2 - 71*Power(t2,2) + 
               t1*(-109 + 149*t2 - 9*Power(t2,2)) + 
               Power(t1,2)*(259 + 6*t2 + 4*Power(t2,2))) + 
            s2*(-104 + 12*Power(t1,4) + Power(t1,3)*(124 - 30*t2) + 
               17*t2 - 71*Power(t2,2) + 
               4*t1*(-40 - 24*t2 + Power(t2,2)) + 
               Power(t1,2)*(163 + 28*t2 + 15*Power(t2,2)))) + 
         s1*(6*Power(t1,4)*(1 + t2) + 
            Power(t1,3)*(-73 + 92*t2 - 10*Power(t2,2)) + 
            t1*(-265 + 98*t2 + 54*Power(t2,2) + 3*Power(t2,3)) - 
            4*(-51 + 27*t2 + 20*Power(t2,2) + 4*Power(t2,3)) + 
            Power(t1,2)*(264 - 84*t2 - 55*Power(t2,2) + 8*Power(t2,3)) + 
            2*Power(s2,5)*(4 - 9*t2 - 4*Power(t2,2) + t1*(-5 + 4*t2)) + 
            Power(s2,4)*(-56 + Power(t1,2)*(5 - 15*t2) + 88*t2 + 
               37*Power(t2,2) - 6*Power(t2,3) + 
               t1*(65 + 7*t2 + 11*Power(t2,2))) + 
            Power(s2,3)*(152 - 89*t2 - 44*Power(t2,2) + 29*Power(t2,3) + 
               Power(t1,3)*(3 + 5*t2) + 
               Power(t1,2)*(-62 + 13*t2 + 5*Power(t2,2)) + 
               t1*(-91 - 121*t2 - 70*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s2,2)*(-185 + t2 + 25*Power(t2,2) - 44*Power(t2,3) + 
               2*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(-3 + 6*t2 - 8*Power(t2,2)) + 
               t1*(56 + 61*t2 + 131*Power(t2,2) - 9*Power(t2,3)) + 
               Power(t1,2)*(100 + 118*t2 - 5*Power(t2,2) + 4*Power(t2,3))\
) - s2*(51 - 126*t2 + 2*Power(t2,2) - 37*Power(t2,3) + 
               8*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(-65 + 103*t2 - 26*Power(t2,2)) - 
               t1*(93 - 53*t2 + 26*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(219 + 32*t2 + 33*Power(t2,2) + 12*Power(t2,3))\
)) + Power(s,3)*(80 - 31*t1 + 31*Power(t1,2) - 24*Power(t1,3) + 
            Power(s1,2)*(-1 + s2)*
             (4 + Power(s2,2) + s2*(-7 + t2) - 3*t2) + 2*t2 - 119*t1*t2 + 
            48*Power(t1,2)*t2 - 6*Power(t2,2) + 18*t1*Power(t2,2) + 
            Power(s2,3)*(20 + 13*Power(t1,2) + 38*t2 - 
               2*t1*(17 + 14*t2)) + 
            s2*(-46 + 32*Power(t1,3) + 113*t2 + 7*Power(t2,2) - 
               Power(t1,2)*(17 + 64*t2) + 
               t1*(11 + 35*t2 - 24*Power(t2,2))) - 
            Power(s2,2)*(46 + 8*Power(t1,3) + Power(t1,2)*(27 - 16*t2) + 
               161*t2 + Power(t2,2) - 2*t1*(39 + 44*t2 + 3*Power(t2,2))) \
+ s1*(-29 - 53*t1 + 30*Power(t1,2) + 75*t2 - 10*Power(s2,3)*t2 - 
               9*t1*t2 - 12*Power(t2,2) + 
               Power(s2,2)*(15 + 10*Power(t1,2) + 68*t2 - 
                  5*Power(t2,2) - 2*t1*(7 + 6*t2)) + 
               s2*(6 - 40*Power(t1,2) - 125*t2 + 17*Power(t2,2) + 
                  t1*(43 + 45*t2)))) - 
         s*(94 + 2*Power(s1,4)*(-1 + s2)*
             (4 + Power(s2,2) + s2*(-3 + t1) - 3*t1) - 111*t1 + 
            238*Power(t1,2) - 91*Power(t1,3) + 6*Power(t1,4) - 23*t2 + 
            128*t1*t2 - 135*Power(t1,2)*t2 + 28*Power(t1,3)*t2 + 
            6*Power(t1,4)*t2 - 56*Power(t2,2) - 28*t1*Power(t2,2) + 
            87*Power(t1,2)*Power(t2,2) - 18*Power(t1,3)*Power(t2,2) - 
            15*Power(t2,3) - 11*t1*Power(t2,3) + 
            6*Power(t1,2)*Power(t2,3) + 
            2*Power(s2,5)*(-7*t2 + t1*(-1 + 3*t2)) + 
            Power(s1,3)*(-1 + s2)*
             (Power(s2,3) + 12*Power(t1,2) + 
               Power(s2,2)*(-2 + 13*t1 - 6*t2) + 3*t1*(-8 + t2) - 
               2*(19 + 9*t2) + s2*(18 - 40*t1 - 4*Power(t1,2) + 21*t2)) - 
            Power(s2,4)*(12 - 91*t2 - 29*Power(t2,2) + 
               Power(t1,2)*(10 + t2) + 2*t1*(-18 + 16*t2 + 9*Power(t2,2))\
) + Power(s2,3)*(51 + Power(t1,3)*(10 - 7*t2) - 123*t2 - 
               154*Power(t2,2) + 16*Power(t2,3) + 
               Power(t1,2)*(6 + 45*t2 + 20*Power(t2,2)) - 
               t1*(65 + 88*t2 - 55*Power(t2,2) + 4*Power(t2,3))) - 
            s2*(-50 - 121*t2 + 102*Power(t2,2) - 63*Power(t2,3) + 
               8*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(-106 + 37*t2 - 24*Power(t2,2)) + 
               t1*(145 + 68*t2 + 150*Power(t2,2) - 5*Power(t2,3)) + 
               Power(t1,2)*(123 - 39*t2 + 7*Power(t2,2) + 8*Power(t2,3))) \
+ Power(s2,2)*(-111 - 60*t2 + 219*Power(t2,2) - 64*Power(t2,3) + 
               2*Power(t1,4)*(1 + t2) - 
               3*Power(t1,3)*(11 - 8*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-23 - 68*t2 - 68*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(135 + 302*t2 + 45*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s1,2)*(-44 + 30*Power(t1,3) - 91*t2 - 23*Power(t2,2) + 
               Power(s2,4)*(-7 + 18*t1 + 16*t2) + 
               Power(t1,2)*(35 + 17*t2) - 
               t1*(39 + 4*t2 + 10*Power(t2,2)) - 
               Power(s2,3)*(-48 + 22*Power(t1,2) + 97*t2 - 
                  16*Power(t2,2) + t1*(99 + 14*t2)) + 
               Power(s2,2)*(-224 + 10*Power(t1,3) + 
                  Power(t1,2)*(105 - 9*t2) + 142*t2 - 69*Power(t2,2) + 
                  t1*(132 + 107*t2 - 4*Power(t2,2))) + 
               s2*(291 - 40*Power(t1,3) - 34*t2 + 76*Power(t2,2) + 
                  6*Power(t1,2)*(-25 + 4*t2) + 
                  t1*(84 - 185*t2 + 14*Power(t2,2)))) + 
            s1*(-35 - 18*Power(t1,4) + 74*t2 + 58*Power(t2,2) + 
               13*Power(t2,3) + Power(s2,5)*(-4 + 6*t1 + 8*t2) + 
               Power(t1,3)*(-83 + 22*t2) - 
               Power(s2,4)*(-35 + 17*Power(t1,2) - 22*t1*(-2 + t2) + 
                  96*t2 + 11*Power(t2,2)) - 
               2*Power(t1,2)*(14 + 63*t2 + 17*Power(t2,2)) + 
               t1*(-2 + 89*t2 + 24*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,3)*(-190 + 17*Power(t1,3) - 
                  43*Power(t1,2)*(-2 + t2) + 306*t2 + 50*Power(t2,2) - 
                  12*Power(t2,3) + t1*(146 + 27*t2 + 13*Power(t2,2))) + 
               s2*(-415 + 24*Power(t1,4) + Power(t1,3)*(153 - 42*t2) + 
                  4*t2 + 8*Power(t2,2) - 51*Power(t2,3) + 
                  Power(t1,2)*(131 + 153*t2 + Power(t2,2)) + 
                  t1*(60 + 69*t2 + 136*Power(t2,2) - 9*Power(t2,3))) + 
               Power(s2,2)*(473 - 6*Power(t1,4) - 224*t2 - 
                  41*Power(t2,2) + 50*Power(t2,3) + 
                  Power(t1,3)*(-79 + 12*t2) + 
                  Power(t1,2)*(-228 + 104*t2 + Power(t2,2)) + 
                  t1*(-110 - 359*t2 - 77*Power(t2,2) + 2*Power(t2,3))))) + 
         Power(s,2)*(8 - 146*t1 + 69*Power(t1,2) - 5*Power(t1,3) - 
            9*Power(t1,4) + 88*t2 + 74*t1*t2 - 105*Power(t1,2)*t2 + 
            9*Power(t1,3)*t2 + 7*Power(t2,2) - 95*t1*Power(t2,2) + 
            36*Power(t1,2)*Power(t2,2) + 21*Power(t2,3) - 
            6*t1*Power(t2,3) + 
            Power(s1,3)*(-1 + s2)*
             (4 + s2 + Power(s2,2) - 9*t1 + 3*s2*t1 + 6*t2 - 2*s2*t2) + 
            Power(s2,4)*(-12 + 18*t1 - 8*Power(t1,2) - 40*t2 + 23*t1*t2) + 
            Power(s2,3)*(11*Power(t1,3) - Power(t1,2)*(11 + 17*t2) - 
               6*t1*(-1 + 13*t2 + 3*Power(t2,2)) + 
               2*(7 + 102*t2 + 8*Power(t2,2))) + 
            Power(s2,2)*(133 - 3*Power(t1,4) - 248*t2 - 73*Power(t2,2) + 
               8*Power(t2,3) + Power(t1,3)*(-35 + 3*t2) + 
               2*Power(t1,2)*(35 + 31*t2 + 6*Power(t2,2)) - 
               t1*(146 + 59*t2 - 55*Power(t2,2) + 2*Power(t2,3))) + 
            s2*(-207 + 12*Power(t1,4) + Power(t1,3)*(29 - 12*t2) + 
               68*t2 + 42*Power(t2,2) - 29*Power(t2,3) - 
               4*Power(t1,2)*(22 - 7*t2 + 12*Power(t2,2)) + 
               2*t1*(86 + 80*t2 + 17*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,2)*(79 - Power(s2,4) - 21*Power(t1,2) + 
               t1*(39 - 27*t2) - 33*t2 + 21*Power(t2,2) + 
               Power(s2,3)*(5 + 13*t1 + 7*t2) + 
               Power(s2,2)*(-1 - 7*Power(t1,2) + t1*(-53 + t2) - 45*t2 + 
                  8*Power(t2,2)) + 
               s2*(-74 + 28*Power(t1,2) + 63*t2 - 29*Power(t2,2) + 
                  t1*(25 + 2*t2))) + 
            s1*(-150 + 51*Power(t1,3) - 85*t2 - Power(t2,2) - 
               15*Power(t2,3) - 8*Power(t1,2)*(-3 + 5*t2) + 
               Power(s2,4)*(-4 + 7*t1 + 17*t2) + 
               t1*(56 + 125*t2 + 32*Power(t2,2)) + 
               Power(s2,3)*(-23*Power(t1,2) + t1*(-28 + 26*t2) + 
                  2*(6 - 71*t2 + Power(t2,2))) - 
               s2*(-342 + 68*Power(t1,3) + Power(t1,2)*(120 - 99*t2) + 
                  198*t2 - 11*Power(t2,2) - 21*Power(t2,3) + 
                  2*t1*(21 + 109*t2 + 5*Power(t2,2))) + 
               Power(s2,2)*(17*Power(t1,3) + Power(t1,2)*(87 - 27*t2) + 
                  t1*(79 - 29*t2 + 2*Power(t2,2)) - 
                  2*(72 - 172*t2 + 2*Power(t2,2) + 3*Power(t2,3)))))))/
     ((-1 + s1)*Power(-1 + s2,3)*(-1 + t1)*Power(s - s2 + t1,2)*
       (-s + s1 - t2)*(-1 + t2)) - 
    (8*(-96 + 105*s2 + 22*Power(s2,2) - 27*Power(s2,3) - 6*Power(s2,4) + 
         2*Power(s2,5) + 55*t1 - 93*s2*t1 + Power(s2,2)*t1 + 
         41*Power(s2,3)*t1 - 2*Power(s2,4)*t1 - 2*Power(s2,5)*t1 + 
         39*Power(t1,2) - 12*s2*Power(t1,2) - 32*Power(s2,2)*Power(t1,2) - 
         3*Power(s2,3)*Power(t1,2) + 8*Power(s2,4)*Power(t1,2) + 
         6*Power(t1,3) + 2*s2*Power(t1,3) + 3*Power(s2,2)*Power(t1,3) - 
         11*Power(s2,3)*Power(t1,3) - 5*Power(t1,4) - s2*Power(t1,4) + 
         6*Power(s2,2)*Power(t1,4) + Power(t1,5) - s2*Power(t1,5) + 
         2*Power(s1,4)*(-1 + s2)*
          (Power(s2,3) - Power(s2,2)*(4 + t1) + 
            s2*(4 + 4*t1 - Power(t1,2)) + t1*(-4 + Power(t1,2))) + 40*t2 - 
         90*s2*t2 + 20*Power(s2,2)*t2 + 9*Power(s2,3)*t2 - 
         3*Power(s2,4)*t2 - 4*Power(s2,5)*t2 - 62*t1*t2 + 182*s2*t1*t2 - 
         136*Power(s2,2)*t1*t2 + 28*Power(s2,3)*t1*t2 + 
         4*Power(s2,5)*t1*t2 - 146*Power(t1,2)*t2 + 
         209*s2*Power(t1,2)*t2 + 3*Power(s2,2)*Power(t1,2)*t2 + 
         9*Power(s2,3)*Power(t1,2)*t2 - 19*Power(s2,4)*Power(t1,2)*t2 - 
         26*Power(t1,3)*t2 - 44*s2*Power(t1,3)*t2 - 
         6*Power(s2,2)*Power(t1,3)*t2 + 28*Power(s2,3)*Power(t1,3)*t2 + 
         16*Power(t1,4)*t2 + 3*s2*Power(t1,4)*t2 - 
         15*Power(s2,2)*Power(t1,4)*t2 - 2*Power(t1,5)*t2 + 
         2*s2*Power(t1,5)*t2 + 40*Power(t2,2) + 15*s2*Power(t2,2) - 
         78*Power(s2,2)*Power(t2,2) + 117*Power(s2,3)*Power(t2,2) - 
         80*Power(s2,4)*Power(t2,2) + 14*Power(s2,5)*Power(t2,2) + 
         9*t1*Power(t2,2) + 15*s2*t1*Power(t2,2) - 
         130*Power(s2,2)*t1*Power(t2,2) + 87*Power(s2,3)*t1*Power(t2,2) + 
         9*Power(s2,4)*t1*Power(t2,2) - 6*Power(s2,5)*t1*Power(t2,2) + 
         23*Power(t1,2)*Power(t2,2) - 38*s2*Power(t1,2)*Power(t2,2) + 
         5*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         60*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         14*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         27*Power(t1,3)*Power(t2,2) - 14*s2*Power(t1,3)*Power(t2,2) + 
         45*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         10*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         2*Power(t1,4)*Power(t2,2) - 8*s2*Power(t1,4)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,4)*Power(t2,2) + 16*Power(t2,3) - 
         30*s2*Power(t2,3) + 30*Power(s2,2)*Power(t2,3) - 
         24*Power(s2,3)*Power(t2,3) + 8*Power(s2,4)*Power(t2,3) - 
         2*t1*Power(t2,3) - 8*s2*t1*Power(t2,3) + 
         16*Power(s2,2)*t1*Power(t2,3) - 4*Power(s2,3)*t1*Power(t2,3) - 
         2*Power(s2,4)*t1*Power(t2,3) - 6*Power(t1,2)*Power(t2,3) + 
         14*s2*Power(t1,2)*Power(t2,3) - 
         12*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         4*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         6*Power(t1,3)*Power(t2,3) + 8*s2*Power(t1,3)*Power(t2,3) - 
         2*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         Power(s,5)*(-1 + s2)*
          (-12 + 5*Power(t1,2) + t1*(6 + s1 - 11*t2) - (-12 + s1)*t2) + 
         Power(s1,3)*(-1 + s2)*
          (-Power(t1,4) - 4*Power(t1,2)*(-8 + t2) + 16*(-1 + t2) - 
            2*Power(t1,3)*(4 + t2) + t1*(44 + 8*t2) + 
            Power(s2,3)*(11*t1 - 4*(1 + t2)) + 
            Power(s2,2)*(-23*Power(t1,2) + 12*(2 + t2) + 
               t1*(-20 + 6*t2)) + 
            s2*(32*Power(t1,2) + 13*Power(t1,3) - 8*t1*(7 + t2) - 
               4*(7 + 6*t2))) + 
         Power(s1,2)*(-3*Power(t1,5) + 5*Power(t1,4)*(-6 + t2) + 
            Power(s2,5)*(-6 + 6*t1 + 8*t2) + 
            Power(t1,2)*(-27 + 59*t2 - 8*Power(t2,2)) + 
            t1*(98 + 36*t2 - 6*Power(t2,2)) + 
            Power(t1,3)*(-101 + 24*t2 - 4*Power(t2,2)) + 
            8*(-9 + 5*t2 + 4*Power(t2,2)) - 
            Power(s2,4)*(-17 + 15*Power(t1,2) + 40*t2 - 8*Power(t2,2) + 
               11*t1*(3 + 2*t2)) + 
            Power(s2,3)*(-66 + 15*Power(t1,3) + 17*t2 - 22*Power(t2,2) + 
               Power(t1,2)*(109 + 11*t2) + 
               t1*(54 + 122*t2 - 12*Power(t2,2))) + 
            Power(s2,2)*(157 - 9*Power(t1,4) + 15*t2 + 40*Power(t2,2) + 
               2*Power(t1,3)*(-55 + 6*t2) - 9*Power(t1,2)*(21 + 11*t2) + 
               t1*(13 - 38*t2 + 18*Power(t2,2))) + 
            s2*(3*Power(t1,5) + Power(t1,4)*(43 - 9*t2) - 
               2*t1*(61 + 57*t2) + 
               4*Power(t1,3)*(37 + 3*t2 + Power(t2,2)) + 
               Power(t1,2)*(178 - 27*t2 + 8*Power(t2,2)) - 
               2*(29 + 6*t2 + 29*Power(t2,2)))) + 
         s1*(2*Power(t1,5)*(1 + t2) + 
            Power(t1,4)*(-18 + 33*t2 - 4*Power(t2,2)) - 
            8*(-19 + 8*t2 + 9*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,3)*(49 + 46*t2 - 27*Power(t2,2) + 4*Power(t2,3)) + 
            Power(t1,2)*(136 - 21*t2 - 25*Power(t2,2) + 4*Power(t2,3)) + 
            t1*(-141 + 38*t2 + 13*Power(t2,2) + 6*Power(t2,3)) + 
            2*Power(s2,5)*(5 - 8*t2 - 4*Power(t2,2) + t1*(-5 + 4*t2)) + 
            Power(s2,4)*(-13 + Power(t1,2)*(35 - 23*t2) + 91*t2 + 
               30*Power(t2,2) - 6*Power(t2,3) + t1*t2*(20 + 19*t2)) + 
            Power(s2,2)*(-155 + Power(t1,4)*(19 - 3*t2) + 37*t2 + 
               4*Power(t2,2) - 20*Power(t2,3) + 
               Power(t1,3)*(18 + 40*t2 - 13*Power(t2,2)) + 
               t1*(176 + 94*t2 + 40*Power(t2,2) - 14*Power(t2,3)) + 
               Power(t1,2)*(76 + 126*t2 + 67*Power(t2,2) + 2*Power(t2,3))\
) - s2*(27 - 58*t2 - 27*Power(t2,2) - 26*Power(t2,3) + 
               2*Power(t1,5)*(1 + t2) + 
               t1*(-27 + 16*t2 - 29*Power(t2,2)) + 
               Power(t1,4)*(5 + 30*t2 - 8*Power(t2,2)) + 
               Power(t1,3)*(-23 + 106*t2 + 8*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(278 + 66*t2 - 20*Power(t2,2) + 6*Power(t2,3))\
) + Power(s2,3)*(61 - 106*t2 - 9*Power(t2,2) + 16*Power(t2,3) + 
               Power(t1,3)*(-42 + 20*t2) - 
               Power(t1,2)*(25 + 16*t2 + 6*Power(t2,2)) + 
               t1*(-68 - 144*t2 - 85*Power(t2,2) + 8*Power(t2,3)))) + 
         Power(s,4)*(-34 + 65*t1 - 31*Power(t1,2) - 13*Power(t1,3) + 
            18*t2 - 54*t1*t2 + 27*Power(t1,2)*t2 - Power(t2,2) + 
            6*t1*Power(t2,2) - Power(s1,2)*(-1 + s2)*(s2 + t2) + 
            Power(s2,2)*(34 - 18*Power(t1,2) - 50*t2 + 
               3*t1*(-4 + 13*t2)) + 
            s2*(-24 + 13*Power(t1,3) + Power(t1,2)*(49 - 27*t2) + 
               56*t2 + Power(t2,2) + t1*(-41 + 3*t2 - 6*Power(t2,2))) + 
            s1*(-9 + 9*Power(t1,2) - Power(s2,2)*(2 + t1 - 11*t2) + 
               7*t2 - 5*Power(t2,2) + t1*(13 + t2) + 
               s2*(35 - 9*Power(t1,2) - 42*t2 + 5*Power(t2,2) + 
                  t1*(-24 + 11*t2)))) + 
         Power(s,3)*(74 - 155*t1 + 91*Power(t1,2) - 42*Power(t1,3) - 
            11*Power(t1,4) - 28*t2 + 61*t1*t2 - 97*Power(t1,2)*t2 + 
            19*Power(t1,3)*t2 + 19*Power(t2,2) - 29*t1*Power(t2,2) + 
            18*Power(t1,2)*Power(t2,2) + 8*Power(t2,3) - 
            2*t1*Power(t2,3) + 
            Power(s2,3)*(-34 + 21*Power(t1,2) + t1*(8 - 51*t2) + 78*t2) - 
            Power(s1,3)*(-1 + s2)*(s2 + 3*t1 - 2*(2 + t2)) - 
            Power(s2,2)*(20 + 32*Power(t1,3) + 
               Power(t1,2)*(85 - 61*t2) + 134*t2 + 17*Power(t2,2) + 
               t1*(-122 + 43*t2 - 24*Power(t2,2))) + 
            Power(s1,2)*(11 + 2*Power(s2,3) - 7*Power(t1,2) + 9*t2 + 
               8*Power(t2,2) - 5*t1*(3 + 2*t2) - 
               2*Power(s2,2)*(-1 + 7*t1 + 3*t2) + 
               s2*(-39 + 7*Power(t1,2) + t1*(41 - 2*t2) + 21*t2 - 
                  8*Power(t2,2))) + 
            s2*(56 + 11*Power(t1,4) + Power(t1,3)*(74 - 19*t2) - 16*t2 + 
               22*Power(t2,2) - 8*Power(t2,3) + 
               Power(t1,2)*(1 + 8*t2 - 18*Power(t2,2)) + 
               t1*(-79 + 149*t2 - 7*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(-20 + 27*Power(t1,3) + Power(s2,3)*(10 - 7*t1 - 27*t2) + 
               Power(t1,2)*(83 - 11*t2) + 20*t2 - 25*Power(t2,2) - 
               6*Power(t2,3) + 
               Power(s2,2)*(-90 + 33*Power(t1,2) + t1*(85 - 28*t2) + 
                  150*t2 - 7*Power(t2,2)) + 
               t1*(-35 + 39*t2 + 9*Power(t2,2)) + 
               s2*(48 - 27*Power(t1,3) - 67*t2 + 8*Power(t2,2) + 
                  6*Power(t2,3) + 3*Power(t1,2)*(-48 + 13*t2) + 
                  t1*(49 - 115*t2 + 3*Power(t2,2))))) + 
         Power(s,2)*(-17 - 78*t1 - 150*Power(t1,2) + 58*Power(t1,3) - 
            19*Power(t1,4) - 3*Power(t1,5) + 
            2*Power(s1,4)*(-1 + s2)*(-2 + s2 + t1) + 80*t2 - 71*t1*t2 + 
            99*Power(t1,2)*t2 - 74*Power(t1,3)*t2 + Power(t1,4)*t2 + 
            11*Power(t2,2) + 6*t1*Power(t2,2) - 
            53*Power(t1,2)*Power(t2,2) + 18*Power(t1,3)*Power(t2,2) + 
            16*Power(t2,3) + 6*t1*Power(t2,3) - 
            4*Power(t1,2)*Power(t2,3) + 
            Power(s1,3)*(-1 + s2)*
             (22 + 2*Power(s2,2) - 7*Power(t1,2) + 
               s2*(-14 + 15*t1 - 8*t2) + 2*t1*(-2 + t2) + 8*t2) + 
            Power(s2,4)*(14 - 8*Power(t1,2) - 54*t2 + t1*(-4 + 29*t2)) + 
            Power(s2,3)*(58 + 19*Power(t1,3) + 
               Power(t1,2)*(53 - 41*t2) + 144*t2 + 45*Power(t2,2) + 
               t1*(-91 + 37*t2 - 36*Power(t2,2))) + 
            s2*(-177 + 3*Power(t1,5) - Power(t1,4)*(-33 + t2) + 165*t2 - 
               11*Power(t2,2) - 40*Power(t2,3) + 
               Power(t1,3)*(22 + 41*t2 - 18*Power(t2,2)) + 
               t1*(382 - 109*t2 + 129*Power(t2,2)) + 
               Power(t1,2)*(-102 + 146*t2 - 25*Power(t2,2) + 
                  4*Power(t2,3))) - 
            Power(s2,2)*(30 + 14*Power(t1,4) + 
               Power(t1,3)*(79 - 13*t2) + 107*t2 + 121*Power(t2,2) - 
               24*Power(t2,3) + 
               Power(t1,2)*(-79 + 48*t2 - 50*Power(t2,2)) + 
               t1*(77 + 122*t2 - 5*Power(t2,2) + 6*Power(t2,3))) - 
            Power(s1,2)*(Power(s2,4) + 17*Power(t1,3) + 
               Power(s2,3)*(15 - 32*t1 - 23*t2) + 
               2*Power(t1,2)*(34 + 9*t2) - 2*(43 + t2 + 7*Power(t2,2)) - 
               t1*(12 + 13*t2 + 12*Power(t2,2)) + 
               Power(s2,2)*(-99 + 42*Power(t1,2) + 87*t2 - 
                  24*Power(t2,2) + t1*(127 + 20*t2)) + 
               s2*(93 - 17*Power(t1,3) + 14*t2 + 38*Power(t2,2) + 
                  2*Power(t1,2)*(-69 + 5*t2) + 
                  3*t1*(7 - 37*t2 + 4*Power(t2,2)))) + 
            s1*(-109 + 23*Power(t1,4) + Power(t1,3)*(117 - 19*t2) - 
               96*t2 + Power(t2,2) - 10*Power(t2,3) + 
               Power(s2,4)*(-14 + 13*t1 + 25*t2) - 
               Power(s2,3)*(-93 + 47*Power(t1,2) + t1*(104 - 31*t2) + 
                  194*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(34 + 81*t2 + 29*Power(t2,2)) - 
               2*t1*(-70 - 6*t2 + 19*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(-96 + 57*Power(t1,3) + 
                  Power(t1,2)*(290 - 96*t2) + 210*t2 + 29*Power(t2,2) - 
                  18*Power(t2,3) + t1*(-61 + 265*t2 + 13*Power(t2,2))) - 
               s2*(-202 + 23*Power(t1,4) + Power(t1,3)*(194 - 39*t2) + 
                  97*t2 - 55*Power(t2,2) - 28*Power(t2,3) + 
                  Power(t1,2)*(177 + 113*t2 + Power(t2,2)) + 
                  t1*(16 + 176*t2 + 79*Power(t2,2) - 8*Power(t2,3))))) + 
         s*(-97 + 94*t1 - 162*Power(t1,2) - 34*Power(t1,3) + 
            21*Power(t1,4) - 2*Power(t1,5) - 
            4*Power(s1,4)*(-1 + s2)*
             (2 - 3*s2 + Power(s2,2) + t1 - Power(t1,2)) + 2*t2 - 
            106*t1*t2 - 53*Power(t1,2)*t2 + 72*Power(t1,3)*t2 - 
            21*Power(t1,4)*t2 - 2*Power(t1,5)*t2 + 81*Power(t2,2) + 
            2*t1*Power(t2,2) + 14*Power(t1,2)*Power(t2,2) - 
            23*Power(t1,3)*Power(t2,2) + 6*Power(t1,4)*Power(t2,2) + 
            14*Power(t2,3) + 10*t1*Power(t2,3) - 
            8*Power(t1,2)*Power(t2,3) - 2*Power(t1,3)*Power(t2,3) + 
            2*Power(s2,5)*(-1 + t1 + 7*t2 - 3*t1*t2) + 
            Power(s2,4)*(-28 - 50*t2 - 43*Power(t2,2) + 
               Power(t1,2)*(-12 + 7*t2) + 6*t1*(3 - 2*t2 + 4*Power(t2,2))) \
+ Power(s2,3)*(Power(t1,3)*(20 + 6*t2) + 
               Power(t1,2)*(-45 + 32*t2 - 46*Power(t2,2)) + 
               2*(7 + 54*t2 + 90*Power(t2,2) - 12*Power(t2,3)) + 
               t1*(85 + 27*t2 - 13*Power(t2,2) + 6*Power(t2,3))) - 
            Power(s2,2)*(-122 + 138*t2 + 125*Power(t2,2) - 
               56*Power(t2,3) + 3*Power(t1,4)*(4 + 3*t2) + 
               Power(t1,3)*(-36 + 58*t2 - 28*Power(t2,2)) + 
               t1*(274 + 70*t2 + 187*Power(t2,2) - 6*Power(t2,3)) + 
               Power(t1,2)*(6 + 2*t2 - 67*Power(t2,2) + 8*Power(t2,3))) + 
            s2*(-37 - 60*t2 + 59*Power(t2,2) - 46*Power(t2,3) + 
               2*Power(t1,5)*(1 + t2) + 
               Power(t1,4)*(-5 + 26*t2 - 6*Power(t2,2)) + 
               t1*(91 + 283*t2 + 42*Power(t2,2) - 22*Power(t2,3)) + 
               Power(t1,3)*(-70 + 48*t2 - 25*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,2)*(281 - 168*t2 + 93*Power(t2,2) + 
                  16*Power(t2,3))) - 
            Power(s1,3)*(-1 + s2)*
             (Power(s2,3) + 5*Power(t1,3) + 2*Power(t1,2)*(8 + t2) - 
               2*t1*(27 + 2*t2) - 4*(7 + 6*t2) + 
               Power(s2,2)*(23*t1 - 2*(7 + 5*t2)) + 
               s2*(46 - 29*Power(t1,2) + 20*t2 + t1*(-22 + 8*t2))) - 
            Power(s1,2)*(6 + 13*Power(t1,4) - 108*t2 - 26*Power(t2,2) + 
               Power(t1,3)*(83 + 2*t2) + 
               6*Power(s2,4)*(-3 + 4*t1 + 4*t2) - 
               4*Power(t1,2)*(-25 + 7*t2) - 
               t1*(91 + 29*t2 + 6*Power(t2,2)) - 
               Power(s2,3)*(-77 + 49*Power(t1,2) + 105*t2 - 
                  24*Power(t2,2) + t1*(117 + 44*t2)) + 
               Power(s2,2)*(38*Power(t1,3) + Power(t1,2)*(251 + 2*t2) + 
                  t1*(14 + 225*t2 - 24*Power(t2,2)) - 
                  4*(37 - 3*t2 + 13*Power(t2,2))) + 
               s2*(235 - 13*Power(t1,4) + 25*t2 + 54*Power(t2,2) + 
                  3*Power(t1,3)*(-47 + 6*t2) - 
                  6*Power(t1,2)*(29 + 17*t2) + 
                  t1*(38 - 20*t2 + 30*Power(t2,2)))) + 
            s1*(99 + 6*Power(t1,5) + Power(s2,5)*(6 - 6*t1 - 8*t2) + 
               Power(t1,4)*(49 - 6*t2) - 82*t2 - 91*Power(t2,2) - 
               10*Power(t2,3) + 
               Power(t1,3)*(42 + 82*t2 + 11*Power(t2,2)) + 
               Power(s2,4)*(-48 + 23*Power(t1,2) + t1*(54 - 22*t2) + 
                  101*t2 + 19*Power(t2,2)) + 
               Power(t1,2)*(225 + 22*t2 - 40*Power(t2,2) + 
                  2*Power(t2,3)) - 
               t1*(77 + 45*t2 - 8*Power(t2,2) + 6*Power(t2,3)) - 
               Power(s2,3)*(-70 + 34*Power(t1,3) + 
                  Power(t1,2)*(194 - 80*t2) + 241*t2 + 62*Power(t2,2) - 
                  18*Power(t2,3) + t1*(-7 + 171*t2 + 35*Power(t2,2))) + 
               Power(s2,2)*(-235 + 23*Power(t1,4) + 
                  Power(t1,3)*(204 - 60*t2) + 175*t2 - 21*Power(t2,2) - 
                  38*Power(t2,3) + 
                  Power(t1,2)*(170 + 144*t2 + 7*Power(t2,2)) + 
                  t1*(97 + 295*t2 + 155*Power(t2,2) - 16*Power(t2,3))) - 
               s2*(6*Power(t1,5) + Power(t1,4)*(76 - 10*t2) + 
                  Power(t1,3)*(184 + 70*t2 - 9*Power(t2,2)) + 
                  t1*(223 + 41*t2 - 4*Power(t2,2) - 22*Power(t2,3)) + 
                  Power(t1,2)*
                   (152 + 190*t2 + 95*Power(t2,2) + 2*Power(t2,3)) - 
                  3*(96 + 9*t2 + Power(t2,2) + 10*Power(t2,3)))))))/
     ((-1 + s1)*Power(-1 + s2,2)*(-1 + t1)*Power(s - s2 + t1,3)*
       (-s + s1 - t2)*(-1 + t2)) + 
    (8*(52 - 81*s2 + 29*Power(s2,2) + 37*t1 - 5*s2*t1 - 
         32*Power(s2,2)*t1 - 94*Power(t1,2) + 91*s2*Power(t1,2) + 
         3*Power(s2,2)*Power(t1,2) + 5*Power(t1,3) - 5*s2*Power(t1,3) - 
         Power(s1,5)*(12 + Power(s2,2) - 7*t1 - 4*Power(t1,2) + 
            s2*(-11 + 9*t1))*(-1 + t2) - 57*t2 + 40*s2*t2 + 
         39*Power(s2,2)*t2 + 7*Power(s2,3)*t2 + 191*t1*t2 - 165*s2*t1*t2 - 
         5*Power(s2,2)*t1*t2 - 11*Power(s2,3)*t1*t2 + 25*Power(t1,2)*t2 - 
         92*s2*Power(t1,2)*t2 + 26*Power(s2,2)*Power(t1,2)*t2 + 
         21*Power(t1,3)*t2 - 19*s2*Power(t1,3)*t2 - 39*Power(t2,2) - 
         257*s2*Power(t2,2) + 152*Power(s2,2)*Power(t2,2) - 
         13*Power(s2,3)*Power(t2,2) + 25*t1*Power(t2,2) + 
         140*s2*t1*Power(t2,2) - 66*Power(s2,2)*t1*Power(t2,2) + 
         11*Power(s2,3)*t1*Power(t2,2) - 29*Power(t1,2)*Power(t2,2) + 
         91*s2*Power(t1,2)*Power(t2,2) - 
         13*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         10*Power(t1,3)*Power(t2,2) + 8*s2*Power(t1,3)*Power(t2,2) + 
         41*Power(t2,3) + 84*s2*Power(t2,3) - 3*Power(s2,2)*Power(t2,3) + 
         6*Power(s2,3)*Power(t2,3) - 27*t1*Power(t2,3) - 
         100*s2*t1*Power(t2,3) + 7*Power(s2,2)*t1*Power(t2,3) + 
         2*Power(t1,2)*Power(t2,3) - 10*s2*Power(t1,2)*Power(t2,3) + 
         3*Power(t2,4) - 6*s2*Power(t2,4) + 3*Power(s2,2)*Power(t2,4) - 
         6*t1*Power(t2,4) + 6*s2*t1*Power(t2,4) - 
         2*Power(s,4)*(3 - 4*s1 + Power(s1,2))*(-1 + t2)*(-2 + t1 + t2) - 
         Power(s,3)*(-1 + t2)*
          (10 - 79*t1 + 18*Power(t1,2) + 2*s2*(-1 + 4*t1 - 9*t2) + 
            Power(s1,3)*(4 + 2*s2 - 3*t1 - t2) - 19*t2 - 3*t1*t2 + 
            3*Power(t2,2) + Power(s1,2)*
             (-18 + t1 + 6*Power(t1,2) + s2*(2 - 4*t1 - 6*t2) - 3*t2 - 
               t1*t2 + Power(t2,2)) + 
            s1*(20 + 65*t1 - 24*Power(t1,2) + 23*t2 + 4*t1*t2 - 
               4*Power(t2,2) + 6*s2*(-3 + 2*t1 + 4*t2))) - 
         Power(s1,4)*(17 + 4*Power(s2,3)*(-1 + t2) + 
            2*Power(t1,3)*(-1 + t2) + 12*t2 - 29*Power(t2,2) + 
            2*Power(t1,2)*(-10 + 15*t2 + Power(t2,2)) + 
            t1*(-8 - 42*t2 + 26*Power(t2,2)) + 
            Power(s2,2)*(8 - 37*t2 + 5*Power(t2,2) + t1*(5 + 7*t2)) - 
            s2*(38 - 50*t2 - 12*Power(t2,2) + 3*Power(t1,2)*(3 + t2) + 
               t1*(-51 + 23*t2 + 16*Power(t2,2)))) + 
         Power(s1,3)*(9 - 2*Power(s2,3)*(-12 + 6*t1 - t2)*(-1 + t2) + 
            66*t2 - 57*Power(t2,2) - 18*Power(t2,3) + 
            2*Power(t1,3)*(-12 + 7*t2 + Power(t2,2)) - 
            2*Power(t1,2)*(23 - 43*t2 - 7*Power(t2,2) + Power(t2,3)) + 
            t1*(18 - 155*t2 + 24*Power(t2,2) + 21*Power(t2,3)) + 
            Power(s2,2)*(55 - 147*t2 - 5*Power(t2,2) + 5*Power(t2,3) + 
               2*Power(t1,2)*(-13 + 9*t2) + 
               t1*(16 + 44*t2 - 8*Power(t2,2))) + 
            s2*(-53 + 83*t2 + 59*Power(t2,2) + 3*Power(t2,3) - 
               2*Power(t1,3)*(-9 + 5*t2) + 
               Power(t1,2)*(-5 - 52*t2 + 13*Power(t2,2)) + 
               t1*(50 + 43*t2 - 44*Power(t2,2) - 9*Power(t2,3)))) + 
         s1*(-69 + 166*t2 - 19*Power(t2,2) - 74*Power(t2,3) - 
            4*Power(t2,4) + 2*Power(t1,3)*(-25 - 4*t2 + 9*Power(t2,2)) + 
            Power(t1,2)*(27 + 60*t2 + 25*Power(t2,2) - 20*Power(t2,3)) + 
            t1*(-88 - 374*t2 + 35*Power(t2,2) + 63*Power(t2,3) + 
               8*Power(t2,4)) - 
            Power(s2,3)*(-1 + t2)*
             (-21 - 15*t2 + 8*Power(t2,2) + t1*(25 + 17*t2)) + 
            Power(s2,2)*(60 - 254*t2 - 185*Power(t2,2) + 
               27*Power(t2,3) - 4*Power(t2,4) + 
               Power(t1,2)*(-61 + 21*Power(t2,2)) + 
               t1*(-59 + 154*t2 + 19*Power(t2,2) - 22*Power(t2,3))) + 
            s2*(1 + 216*t2 + 222*Power(t2,2) - 91*Power(t2,3) + 
               8*Power(t2,4) + 4*Power(t1,3)*(12 + t2 - 3*Power(t2,2)) + 
               Power(t1,2)*(75 - 120*t2 - 33*Power(t2,2) + 
                  26*Power(t2,3)) + 
               t1*(112 + 158*t2 - 97*Power(t2,2) + 99*Power(t2,3) - 
                  8*Power(t2,4)))) + 
         Power(s1,2)*(13 - 151*t2 + 86*Power(t2,2) + 51*Power(t2,3) + 
            Power(t2,4) + Power(t1,3)*(67 - 25*t2 - 10*Power(t2,2)) + 
            Power(t1,2)*(41 - 41*t2 - 48*Power(t2,2) + 12*Power(t2,3)) + 
            t1*(88 + 185*t2 - 18*Power(t2,2) - 49*Power(t2,3) - 
               2*Power(t2,4)) + 
            Power(s2,3)*(-1 + t2)*
             (-41 - 10*t2 + 2*Power(t2,2) + t1*(37 + 6*t2)) + 
            Power(s2,2)*(-81 + 222*t2 + 83*Power(t2,2) - 
               21*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(84 - 44*t2 - 8*Power(t2,2)) + 
               t1*(24 - 82*t2 + 15*Power(t2,2) + 7*Power(t2,3))) + 
            s2*(Power(t1,3)*(-61 + 25*t2 + 4*Power(t2,2)) - 
               Power(t1,2)*(114 - 157*t2 + 31*Power(t2,2) + 
                  8*Power(t2,3)) - 
               2*(-25 + 98*t2 + 26*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4)) + 
               t1*(-115 - 50*t2 - 15*Power(t2,2) + 10*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         Power(s,2)*(23 + 100*t1 + 6*Power(t1,2) + 6*Power(t1,3) + 
            204*t2 - 289*t1*t2 + 10*Power(t1,2)*t2 - 6*Power(t1,3)*t2 - 
            20*Power(t2,2) + 90*t1*Power(t2,2) + 10*Power(t2,3) + 
            3*t1*Power(t2,3) + 3*Power(t2,4) + 
            Power(s1,4)*(-1 + t2)*(s2 + t2) - 
            2*Power(s2,2)*(-1 + t2)*(-1 + 3*t1 + 9*t2) + 
            s2*(-40 - 193*t2 + Power(t2,2) + 12*Power(t2,3) + 
               Power(t1,2)*(-37 + 21*t2) + 
               t1*(66 + 72*t2 - 42*Power(t2,2))) - 
            Power(s1,3)*(1 + 2*Power(s2,2)*(-1 + t2) - 
               16*Power(t1,2)*(-1 + t2) + 20*t2 + Power(t2,2) + 
               2*Power(t2,3) + t1*(-6 - 13*t2 + 7*Power(t2,2)) + 
               s2*(5 - 42*t2 + 13*Power(t2,2) + t1*(-1 + 13*t2))) + 
            s1*(43 + 8*Power(t1,3)*(-1 + t2) - 161*t2 + 6*Power(t2,2) - 
               20*Power(t2,3) - 4*Power(t2,4) + 
               2*Power(s2,2)*(-1 + t2)*(-18 + 17*t1 + 12*t2) + 
               Power(t1,2)*(-83 + 59*t2) - 
               t1*(63 - 175*t2 + 112*Power(t2,2) + 4*Power(t2,3)) - 
               s2*(55 - 229*t2 + 22*Power(t2,2) + 16*Power(t2,3) + 
                  Power(t1,2)*(-77 + 53*t2) + 
                  t1*(42 + t2 - 47*Power(t2,2)))) + 
            Power(s1,2)*(-41 + Power(t1,2)*(77 - 69*t2) - 
               2*Power(t1,3)*(-1 + t2) + 90*t2 + 6*Power(t2,2) + 
               12*Power(t2,3) + Power(t2,4) - 
               2*Power(s2,2)*(-1 + t2)*(-10 + 6*t1 + 3*t2) + 
               t1*(-51 - 27*t2 + 37*Power(t2,2) + Power(t2,3)) + 
               s2*(61 - 175*t2 + 42*Power(t2,2) + 4*Power(t2,3) + 
                  8*Power(t1,2)*(-3 + 2*t2) + 
                  t1*(-1 + 54*t2 - 13*Power(t2,2))))) + 
         s*(85 + 89*t1 - 35*Power(t1,2) + 41*Power(t1,3) + 
            Power(s1,5)*(s2 - t1)*(-1 + t2) - 62*t2 + 253*t1*t2 - 
            90*Power(t1,2)*t2 - 19*Power(t1,3)*t2 + 173*Power(t2,2) - 
            208*t1*Power(t2,2) + 27*Power(t1,2)*Power(t2,2) - 
            6*Power(t1,3)*Power(t2,2) + 18*Power(t2,3) - 
            4*t1*Power(t2,3) + 18*Power(t1,2)*Power(t2,3) + 
            6*Power(t2,4) - 6*t1*Power(t2,4) + 
            2*Power(s2,3)*(-1 + t2)*(-8 + 10*t1 + 3*t2) + 
            Power(s2,2)*(18 + Power(t1,2)*(53 - 37*t2) + 227*t2 - 
               10*Power(t2,2) - 15*Power(t2,3) + 
               t1*(-11 - 113*t2 + 28*Power(t2,2))) + 
            s2*(-90 - 319*t2 - 18*Power(t2,2) - 7*Power(t2,3) - 
               6*Power(t2,4) + Power(t1,3)*(-39 + 23*t2) + 
               Power(t1,2)*(-59 + 145*t2 - 22*Power(t2,2)) - 
               2*t1*(24 - 61*t2 + 46*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s1,4)*(-9 + 3*Power(s2,2)*(-1 + t2) - 
               14*Power(t1,2)*(-1 + t2) + 37*t2 - 4*Power(t2,2) + 
               t1*(8 - 26*t2 + 6*Power(t2,2)) + 
               2*s2*(7 - 21*t2 + 2*Power(t2,2) + t1*(-3 + 9*t2))) + 
            Power(s1,3)*(46 + 4*Power(s2,3)*(-1 + t2) + 
               4*Power(t1,3)*(-1 + t2) - 97*t2 - 43*Power(t2,2) + 
               2*Power(t2,3) + 
               Power(t1,2)*(-73 + 75*t2 + 2*Power(t2,2)) + 
               t1*(-24 + 40*t2 + 15*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*(27 - 61*t2 + 10*Power(t2,2) + 
                  6*t1*(-1 + 3*t2)) + 
               s2*(-76 + Power(t1,2)*(15 - 19*t2) + 189*t2 + 
                  6*Power(t2,2) - 3*Power(t2,3) + 
                  t1*(35 - 77*t2 + 2*Power(t2,2)))) + 
            Power(s1,2)*(-71 + 139*t2 + 116*Power(t2,2) + 
               18*Power(t2,3) + 2*Power(t2,4) + 
               2*Power(s2,3)*(-1 + t2)*(-11 + 5*t1 + t2) - 
               2*Power(t1,3)*(-16 + 11*t2 + Power(t2,2)) + 
               2*Power(t1,2)*(60 - 66*t2 - Power(t2,2) + 3*Power(t2,3)) - 
               2*t1*(-62 + 14*t2 + 33*Power(t2,2) - 2*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(-96 + Power(t1,2)*(26 - 18*t2) + 203*t2 - 
                  34*Power(t2,2) - 5*Power(t2,3) + 
                  t1*(23 - 69*t2 + 6*Power(t2,2))) + 
               s2*(126 - 354*t2 - 51*Power(t2,2) + 9*Power(t2,3) - 
                  2*Power(t2,4) + 2*Power(t1,3)*(-9 + 5*t2) + 
                  Power(t1,2)*(-81 + 86*t2 - 5*Power(t2,2)) - 
                  t1*(23 - 71*t2 + 32*Power(t2,2) + 8*Power(t2,3)))) + 
            s1*(5 - 121*t2 - 202*Power(t2,2) - 30*Power(t2,3) - 
               8*Power(t2,4) - 
               2*Power(s2,3)*(-1 + t2)*(-17 + 15*t1 + 4*t2) + 
               Power(t1,3)*(-69 + 37*t2 + 8*Power(t2,2)) - 
               Power(t1,2)*(50 - 49*t2 + 19*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(-230 - 22*t2 + 205*Power(t2,2) - 5*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s2,2)*(78 - 260*t2 + 26*Power(t2,2) + 
                  20*Power(t2,3) + Power(t1,2)*(-79 + 55*t2) + 
                  t1*(-30 + 52*t2 - 26*Power(t2,2))) + 
               s2*(-53 + Power(t1,3)*(57 - 33*t2) + 517*t2 + 
                  27*Power(t2,2) - 7*Power(t2,3) + 8*Power(t2,4) + 
                  Power(t1,2)*(149 - 100*t2 + 19*Power(t2,2)) + 
                  2*t1*(49 - 119*t2 + 81*Power(t2,2) + 13*Power(t2,3)))))))/
     (Power(-1 + s1,3)*(-1 + s2)*(-s + s2 - t1)*(-1 + t1)*Power(-1 + t2,2)*
       (s - s1 + t2)) - (8*(-52 + 69*s2 - 13*Power(s2,2) - 9*Power(s2,3) + 
         17*Power(s2,4) - 12*Power(s2,5) + 57*t1 - 166*s2*t1 + 
         151*Power(s2,2)*t1 - 66*Power(s2,3)*t1 + 12*Power(s2,4)*t1 + 
         12*Power(s2,5)*t1 + 39*Power(t1,2) + 19*s2*Power(t1,2) - 
         86*Power(s2,2)*Power(t1,2) + 57*Power(s2,3)*Power(t1,2) - 
         29*Power(s2,4)*Power(t1,2) - 41*Power(t1,3) + 74*s2*Power(t1,3) - 
         51*Power(s2,2)*Power(t1,3) + 18*Power(s2,3)*Power(t1,3) - 
         3*Power(t1,4) + 4*s2*Power(t1,4) - Power(s2,2)*Power(t1,4) - 
         37*t2 + 88*s2*t2 - 88*Power(s2,2)*t2 - 18*Power(s2,3)*t2 - 
         8*Power(s2,4)*t2 + 7*Power(s2,5)*t2 - 191*t1*t2 + 374*s2*t1*t2 - 
         185*Power(s2,2)*t1*t2 + 155*Power(s2,3)*t1*t2 - 
         42*Power(s2,4)*t1*t2 - 7*Power(s2,5)*t1*t2 - 25*Power(t1,2)*t2 - 
         35*s2*Power(t1,2)*t2 + 18*Power(s2,2)*Power(t1,2)*t2 - 
         24*Power(s2,3)*Power(t1,2)*t2 + 26*Power(s2,4)*Power(t1,2)*t2 + 
         27*Power(t1,3)*t2 - 63*s2*Power(t1,3)*t2 + 
         49*Power(s2,2)*Power(t1,3)*t2 - 21*Power(s2,3)*Power(t1,3)*t2 + 
         6*Power(t1,4)*t2 - 8*s2*Power(t1,4)*t2 + 
         2*Power(s2,2)*Power(t1,4)*t2 + 94*Power(t2,2) - 
         27*s2*Power(t2,2) - 41*Power(s2,2)*Power(t2,2) + 
         46*Power(s2,3)*Power(t2,2) - 20*Power(s2,4)*Power(t2,2) + 
         4*Power(s2,5)*Power(t2,2) - 25*t1*Power(t2,2) - 
         60*s2*t1*Power(t2,2) + 41*Power(s2,2)*t1*Power(t2,2) - 
         86*Power(s2,3)*t1*Power(t2,2) + 30*Power(s2,4)*t1*Power(t2,2) - 
         4*Power(s2,5)*t1*Power(t2,2) + 29*Power(t1,2)*Power(t2,2) - 
         25*s2*Power(t1,2)*Power(t2,2) + 
         48*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         14*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         2*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         2*Power(t1,3)*Power(t2,2) + 20*s2*Power(t1,3)*Power(t2,2) - 
         12*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         2*Power(s2,3)*Power(t1,3)*Power(t2,2) - 5*Power(t2,3) + 
         50*s2*Power(t2,3) - 67*Power(s2,2)*Power(t2,3) + 
         24*Power(s2,3)*Power(t2,3) - 2*Power(s2,4)*Power(t2,3) - 
         21*t1*Power(t2,3) + 8*s2*t1*Power(t2,3) + 
         25*Power(s2,2)*t1*Power(t2,3) - 14*Power(s2,3)*t1*Power(t2,3) + 
         2*Power(s2,4)*t1*Power(t2,3) + 10*Power(t1,2)*Power(t2,3) - 
         18*s2*Power(t1,2)*Power(t2,3) + 
         10*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         2*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         2*Power(s,4)*(3 - 4*s2 + Power(s2,2))*(-1 + t1)*(-2 + t1 + t2) + 
         Power(s1,3)*(-1 + s2)*(-1 + t1)*
          (4*Power(s2,3) + s2*
             (21 - 2*Power(t1,2) + t1*(8 - 6*t2) - 25*t2) - 
            2*Power(s2,2)*(10 + t1 - 6*t2) + t1*(-7 + 6*t1 + 11*t2)) + 
         Power(s,3)*(-1 + t1)*
          (10 - 19*t1 + 3*Power(t1,2) - 79*t2 - 3*t1*t2 + 
            18*Power(t2,2) - Power(s2,3)*(-4 + t1 + 3*t2) + 
            Power(s2,2)*(-18 + Power(t1,2) + t2 + 6*Power(t2,2) - 
               t1*(3 + t2)) + 
            2*s1*(-1 + Power(s2,3) - 9*t1 + 
               Power(s2,2)*(1 - 3*t1 - 2*t2) + 4*t2 + 
               3*s2*(-3 + 4*t1 + 2*t2)) + 
            s2*(20 - 4*Power(t1,2) + 65*t2 - 24*Power(t2,2) + 
               t1*(23 + 4*t2))) + 
         Power(s1,2)*(-29 + Power(s2,5)*(-1 + t1) - 3*Power(t1,4) + 
            Power(t1,3)*(3 - 7*t2) + 32*t2 - 3*Power(t2,2) + 
            t1*(-39 + 5*t2 - 26*Power(t2,2)) + 
            Power(t1,2)*(-152 + 66*t2 + 13*Power(t2,2)) + 
            Power(s2,4)*(8 + 5*Power(t1,2) + 5*t2 + t1*(-37 + 7*t2)) + 
            s2*(-60 + 4*Power(t1,4) + t1*(254 - 154*t2) + 59*t2 + 
               61*Power(t2,2) + Power(t1,3)*(-27 + 22*t2) + 
               Power(t1,2)*(185 - 19*t2 - 21*Power(t2,2))) + 
            Power(s2,3)*(-55 - 5*Power(t1,3) - 16*t2 + 26*Power(t2,2) + 
               Power(t1,2)*(5 + 8*t2) + t1*(147 - 44*t2 - 18*Power(t2,2))\
) - Power(s2,2)*(-81 + Power(t1,4) + 7*Power(t1,3)*(-3 + t2) + 24*t2 + 
               84*Power(t2,2) + t1*(222 - 82*t2 - 44*Power(t2,2)) + 
               Power(t1,2)*(83 + 15*t2 - 8*Power(t2,2)))) + 
         s1*(81 - 6*Power(t1,4)*(-1 + t2) + 5*t2 - 91*Power(t2,2) + 
            5*Power(t2,3) + Power(s2,5)*(-1 + t1)*(-11 + 9*t2) + 
            2*Power(t1,3)*(-42 + 50*t2 + 5*Power(t2,2)) - 
            Power(t1,2)*(-257 + 140*t2 + 91*Power(t2,2) + 
               8*Power(t2,3)) + 
            t1*(-40 + 165*t2 + 92*Power(t2,2) + 19*Power(t2,3)) - 
            Power(s2,4)*(38 - 51*t2 + 9*Power(t2,2) + 
               4*Power(t1,2)*(-3 + 4*t2) + 
               t1*(-50 + 23*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(-50 - 2*Power(t1,4)*(-1 + t2) + 115*t2 + 
               114*Power(t2,2) + 61*Power(t2,3) + 
               2*Power(t1,3)*(2 - 5*t2 + 4*Power(t2,2)) + 
               t1*(196 + 50*t2 - 157*Power(t2,2) - 25*Power(t2,3)) + 
               Power(t1,2)*(52 + 15*t2 + 31*Power(t2,2) - 4*Power(t2,3))) \
+ Power(s2,3)*(53 - 50*t2 + 5*Power(t2,2) - 18*Power(t2,3) + 
               Power(t1,3)*(-3 + 9*t2) + 
               Power(t1,2)*(-59 + 44*t2 - 13*Power(t2,2)) + 
               t1*(-83 - 43*t2 + 52*Power(t2,2) + 10*Power(t2,3))) + 
            s2*(-1 + 8*Power(t1,4)*(-1 + t2) - 112*t2 - 75*Power(t2,2) - 
               48*Power(t2,3) + 
               Power(t1,3)*(91 - 99*t2 - 26*Power(t2,2)) - 
               2*t1*(108 + 79*t2 - 60*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(-222 + 97*t2 + 33*Power(t2,2) + 
                  12*Power(t2,3)))) + 
         Power(s,2)*(-23 - 204*t1 - Power(s2,4)*(-1 + t1)*t1 + 
            20*Power(t1,2) - 10*Power(t1,3) - 3*Power(t1,4) - 100*t2 + 
            289*t1*t2 - 90*Power(t1,2)*t2 - 3*Power(t1,3)*t2 - 
            6*Power(t2,2) - 10*t1*Power(t2,2) - 6*Power(t2,3) + 
            6*t1*Power(t2,3) + 
            2*Power(s1,2)*(-1 + t1)*
             (-1 + Power(s2,3) + 9*t1 + s2*(18 - 12*t1 - 17*t2) + 3*t2 + 
               Power(s2,2)*(-10 + 3*t1 + 6*t2)) + 
            Power(s2,3)*(1 + 2*Power(t1,3) - 6*t2 + 16*Power(t2,2) + 
               Power(t1,2)*(1 + 7*t2) + t1*(20 - 13*t2 - 16*Power(t2,2))) \
- Power(s2,2)*(-41 + Power(t1,4) - 51*t2 + 77*Power(t2,2) + 
               2*Power(t2,3) + Power(t1,3)*(12 + t2) + 
               Power(t1,2)*(6 + 37*t2) - 
               t1*(-90 + 27*t2 + 69*Power(t2,2) + 2*Power(t2,3))) + 
            s2*(-43 + 4*Power(t1,4) + 63*t2 + 83*Power(t2,2) + 
               8*Power(t2,3) + 4*Power(t1,3)*(5 + t2) + 
               2*Power(t1,2)*(-3 + 56*t2) - 
               t1*(-161 + 175*t2 + 59*Power(t2,2) + 8*Power(t2,3))) + 
            s1*(40 - Power(s2,4)*(-1 + t1) - 12*Power(t1,3) - 66*t2 + 
               37*Power(t2,2) + Power(t1,2)*(-1 + 42*t2) + 
               t1*(193 - 72*t2 - 21*Power(t2,2)) + 
               Power(s2,3)*(5 + 13*Power(t1,2) - t2 + t1*(-42 + 13*t2)) + 
               Power(s2,2)*(-61 - 4*Power(t1,3) + t2 + 24*Power(t2,2) + 
                  Power(t1,2)*(-42 + 13*t2) + 
                  t1*(175 - 54*t2 - 16*Power(t2,2))) + 
               s2*(55 + 16*Power(t1,3) + Power(t1,2)*(22 - 47*t2) + 
                  42*t2 - 77*Power(t2,2) + t1*(-229 + t2 + 53*Power(t2,2))\
))) + s*(-85 + 62*t1 - 173*Power(t1,2) - 18*Power(t1,3) - 6*Power(t1,4) - 
            89*t2 + Power(s2,5)*(-1 + t1)*t2 - 253*t1*t2 + 
            208*Power(t1,2)*t2 + 4*Power(t1,3)*t2 + 6*Power(t1,4)*t2 + 
            35*Power(t2,2) + 90*t1*Power(t2,2) - 
            27*Power(t1,2)*Power(t2,2) - 18*Power(t1,3)*Power(t2,2) - 
            41*Power(t2,3) + 19*t1*Power(t2,3) + 
            6*Power(t1,2)*Power(t2,3) - 
            2*Power(s1,3)*(-1 + s2)*(-1 + t1)*
             (8 + 2*Power(s2,2) - 3*t1 - 10*t2 + s2*(-9 + t1 + 5*t2)) + 
            Power(s2,4)*(9 + Power(t1,2)*(4 - 6*t2) - 8*t2 - 
               14*Power(t2,2) + t1*(-37 + 26*t2 + 14*Power(t2,2))) + 
            s2*(-5 - 8*Power(t1,4)*(-1 + t2) + 230*t2 + 50*Power(t2,2) + 
               69*Power(t2,3) + 
               Power(t1,3)*(30 + 5*t2 + 24*Power(t2,2)) + 
               t1*(121 + 22*t2 - 49*Power(t2,2) - 37*Power(t2,3)) + 
               Power(t1,2)*(202 - 205*t2 + 19*Power(t2,2) - 8*Power(t2,3))\
) + Power(s2,3)*(-46 + 24*t2 + 73*Power(t2,2) + 4*Power(t2,3) + 
               Power(t1,3)*(-2 + 3*t2) + 
               Power(t1,2)*(43 - 15*t2 - 2*Power(t2,2)) - 
               t1*(-97 + 40*t2 + 75*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,2)*(71 + 2*Power(t1,4)*(-1 + t2) - 124*t2 - 
               120*Power(t2,2) - 32*Power(t2,3) - 
               2*Power(t1,3)*(9 + 2*t2 + 3*Power(t2,2)) + 
               2*Power(t1,2)*(-58 + 33*t2 + Power(t2,2) + Power(t2,3)) + 
               t1*(-139 + 28*t2 + 132*Power(t2,2) + 22*Power(t2,3))) + 
            Power(s1,2)*(-18 - 3*Power(s2,4)*(-1 + t1) + 15*Power(t1,3) + 
               Power(t1,2)*(10 - 28*t2) + 11*t2 - 53*Power(t2,2) + 
               Power(s2,3)*(-27 - 10*Power(t1,2) + t1*(61 - 18*t2) + 
                  6*t2) + t1*(-227 + 113*t2 + 37*Power(t2,2)) + 
               s2*(-78 - 20*Power(t1,3) + 26*Power(t1,2)*(-1 + t2) + 
                  30*t2 + 79*Power(t2,2) + 
                  t1*(260 - 52*t2 - 55*Power(t2,2))) + 
               Power(s2,2)*(96 + 5*Power(t1,3) + 
                  Power(t1,2)*(34 - 6*t2) - 23*t2 - 26*Power(t2,2) + 
                  t1*(-203 + 69*t2 + 18*Power(t2,2)))) + 
            s1*(90 - Power(s2,5)*(-1 + t1) + 6*Power(t1,4) + 48*t2 + 
               59*Power(t2,2) + 39*Power(t2,3) + Power(t1,3)*(7 + 10*t2) + 
               2*Power(t1,2)*(9 + 46*t2 + 11*Power(t2,2)) - 
               t1*(-319 + 122*t2 + 145*Power(t2,2) + 23*Power(t2,3)) - 
               2*Power(s2,4)*(7 + 2*Power(t1,2) - 3*t2 + 
                  3*t1*(-7 + 3*t2)) + 
               Power(s2,3)*(76 + 3*Power(t1,3) - 35*t2 - 15*Power(t2,2) - 
                  2*Power(t1,2)*(3 + t2) + 
                  t1*(-189 + 77*t2 + 19*Power(t2,2))) + 
               Power(s2,2)*(-126 + 2*Power(t1,4) + 23*t2 + 
                  81*Power(t2,2) + 18*Power(t2,3) + 
                  Power(t1,3)*(-9 + 8*t2) + 
                  Power(t1,2)*(51 + 32*t2 + 5*Power(t2,2)) - 
                  t1*(-354 + 71*t2 + 86*Power(t2,2) + 10*Power(t2,3))) - 
               s2*(-53 + 8*Power(t1,4) + 98*t2 + 149*Power(t2,2) + 
                  57*Power(t2,3) + Power(t1,3)*(-7 + 26*t2) + 
                  Power(t1,2)*(27 + 162*t2 + 19*Power(t2,2)) - 
                  t1*(-517 + 238*t2 + 100*Power(t2,2) + 33*Power(t2,3))))))\
)/((-1 + s1)*Power(-1 + s2,3)*Power(-1 + t1,2)*(s - s2 + t1)*(-s + s1 - t2)*
       (-1 + t2)) + (8*(-140 + 204*s2 - 80*Power(s2,2) + 16*Power(s2,3) + 
         76*t1 - 108*s2*t1 + 48*Power(s2,2)*t1 - 16*Power(s2,3)*t1 + 
         48*Power(t1,2) - 80*s2*Power(t1,2) + 32*Power(s2,2)*Power(t1,2) + 
         16*Power(t1,3) - 16*s2*Power(t1,3) + 110*t2 - 265*s2*t2 + 
         156*Power(s2,2)*t2 - 54*Power(s2,3)*t2 + 8*Power(s2,4)*t2 - 
         85*t1*t2 + 98*s2*t1*t2 + 11*Power(s2,2)*t1*t2 - 
         2*Power(s2,3)*t1*t2 - 24*Power(t1,2)*t2 + 54*s2*Power(t1,2)*t2 - 
         9*Power(s2,2)*Power(t1,2)*t2 - Power(t1,3)*t2 + 
         3*s2*Power(t1,3)*t2 + 23*Power(t2,2) + 264*s2*Power(t2,2) - 
         72*Power(s2,2)*Power(t2,2) - 20*Power(s2,3)*Power(t2,2) - 
         6*Power(s2,4)*Power(t2,2) - 168*t1*Power(t2,2) - 
         84*s2*t1*Power(t2,2) + 69*Power(s2,2)*t1*Power(t2,2) + 
         9*Power(s2,3)*t1*Power(t2,2) + 53*Power(t1,2)*Power(t2,2) - 
         55*s2*Power(t1,2)*Power(t2,2) - 
         11*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         10*Power(t1,3)*Power(t2,2) + 8*s2*Power(t1,3)*Power(t2,2) + 
         4*Power(t2,3) - 73*s2*Power(t2,3) - 78*Power(s2,2)*Power(t2,3) + 
         3*Power(s2,3)*Power(t2,3) + 47*t1*Power(t2,3) + 
         92*s2*t1*Power(t2,3) + 13*Power(s2,2)*t1*Power(t2,3) + 
         2*Power(t1,2)*Power(t2,3) - 10*s2*Power(t1,2)*Power(t2,3) + 
         3*Power(t2,4) + 6*s2*Power(t2,4) - 9*Power(s2,2)*Power(t2,4) - 
         6*t1*Power(t2,4) + 6*s2*t1*Power(t2,4) + 
         2*Power(s1,5)*(t1*(t1*(7 - 3*t2) + t2) + 
            Power(s2,2)*(-2 + 4*t1 + 3*t2) + 
            s2*(4 - 4*Power(t1,2) - 5*t2 + t1*(-9 + 4*t2))) + 
         Power(s,4)*(-1 + s1)*
          (3*(4 + (-10 + s2)*t2 + 5*Power(t2,2) - 
               t1*(-12 + s2 + 11*t2)) + 
            s1*(-8 - (-14 + s2)*t2 - 5*Power(t2,2) + 
               t1*(-12 + s2 + 11*t2))) + 
         Power(s1,4)*(2*Power(s2,4) - 2*Power(t1,3)*(-4 + t2) + 
            Power(s2,3)*(-3 - 4*t1 + 11*t2) + 
            8*Power(t1,2)*(-11 + 3*t2 + Power(t2,2)) + 
            2*(-10 + 9*t2 + Power(t2,2)) + 
            t1*(44 - 67*t2 + 9*Power(t2,2)) + 
            Power(s2,2)*(38 + 8*Power(t1,2) - 53*t2 - 9*Power(t2,2) - 
               2*t1*(24 + 7*t2)) + 
            s2*(-56 - 6*Power(t1,3) + 65*t2 + 5*Power(t2,2) + 
               Power(t1,2)*(37 + 11*t2) + t1*(88 + 7*t2 - 15*Power(t2,2)))\
) + Power(s1,3)*(28 - 8*Power(s2,4) + 15*t2 - 40*Power(t2,2) - 
            3*Power(t2,3) + Power(t1,3)*(-35 + 2*t2 + 2*Power(t2,2)) + 
            t1*(-146 + 95*t2 + 58*Power(t2,2) - 9*Power(t2,3)) + 
            Power(t1,2)*(187 + 18*t2 - 50*Power(t2,2) - 2*Power(t2,3)) + 
            Power(s2,3)*(17 - 38*t2 - 12*Power(t2,2) + t1*(19 + 2*t2)) + 
            Power(s2,2)*(-150 + 94*t2 + 89*Power(t2,2) + 6*Power(t2,3) - 
               4*Power(t1,2)*(10 + t2) + t1*(80 + 96*t2 - 3*Power(t2,2))) \
+ s2*(152 - 91*t2 - 62*Power(t2,2) + 3*Power(t2,3) + 
               Power(t1,3)*(29 + 2*t2) + 
               Power(t1,2)*(-44 - 70*t2 + 5*Power(t2,2)) + 
               t1*(-89 - 121*t2 + 13*Power(t2,2) + 5*Power(t2,3)))) + 
         Power(s1,2)*(-62 + 18*t2 + 17*Power(t2,2) + 26*Power(t2,3) + 
            Power(t2,4) + Power(t1,3)*(50 + 9*t2 - 10*Power(t2,2)) - 
            2*Power(s2,4)*(-7 + Power(t2,2)) + 
            3*Power(t1,2)*(-47 - 64*t2 + 34*Power(t2,2) + 
               4*Power(t2,3)) + 
            t1*(161 - 28*t2 - 102*Power(t2,2) + Power(t2,3) - 
               2*Power(t2,4)) + 
            Power(s2,3)*(-44 - 15*t2 + 45*Power(t2,2) + Power(t2,3) + 
               t1*(-41 + 2*Power(t2,2))) - 
            Power(s2,2)*(-228 - 109*t2 + 259*Power(t2,2) + 
               44*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,2)*(-71 - 9*t2 + 4*Power(t2,2)) + 
               t1*(33 + 149*t2 + 6*Power(t2,2) - 9*Power(t2,3))) + 
            s2*(-185 + 56*t2 + 100*Power(t2,2) - 3*Power(t2,3) + 
               2*Power(t2,4) + Power(t1,3)*(-44 - 9*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(25 + 131*t2 - 5*Power(t2,2) - 
                  8*Power(t2,3)) + 
               t1*(1 + 61*t2 + 118*Power(t2,2) + 6*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         s1*(194 - 161*t2 - 2*Power(t2,2) - 27*Power(t2,3) - 
            4*Power(t2,4) + 8*Power(s2,4)*(-1 - t2 + Power(t2,2)) + 
            Power(t1,3)*(-39 - 8*t2 + 18*Power(t2,2)) + 
            Power(t1,2)*(52 + 28*t2 - 25*Power(t2,2) - 20*Power(t2,3)) + 
            t1*(-207 + 235*t2 + 115*Power(t2,2) - 31*Power(t2,3) + 
               8*Power(t2,4)) + 
            Power(s2,3)*(14 + 96*t2 - 13*Power(t2,2) - 4*Power(t2,3) + 
               t1*(42 - 11*Power(t2,2))) + 
            Power(s2,2)*(-104 - 160*t2 + 163*Power(t2,2) + 
               124*Power(t2,3) + 12*Power(t2,4) + 
               Power(t1,2)*(-71 + 4*t2 + 15*Power(t2,2)) + 
               t1*(17 - 96*t2 + 28*Power(t2,2) - 30*Power(t2,3))) - 
            s2*(51 - 93*t2 + 219*Power(t2,2) - 65*Power(t2,3) + 
               8*Power(t2,4) + 
               Power(t1,3)*(-37 - 4*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(2 - 26*t2 + 33*Power(t2,2) - 
                  26*Power(t2,3)) + 
               t1*(-126 + 53*t2 + 32*Power(t2,2) + 103*Power(t2,3) + 
                  8*Power(t2,4)))) + 
         Power(s,3)*(80 + 2*t1 - 6*Power(t1,2) + 
            Power(s2,2)*(-4 + 3*t1) - 31*t2 - 119*t1*t2 + 
            18*Power(t1,2)*t2 + 31*Power(t2,2) + 48*t1*Power(t2,2) - 
            24*Power(t2,3) + Power(s1,3)*
             (20 + Power(s2,2) - 10*s2*t1 + t1*(38 - 28*t2) - 34*t2 + 
               13*Power(t2,2)) - 
            s2*(29 + 12*Power(t1,2) + 53*t2 - 30*Power(t2,2) + 
               t1*(-75 + 9*t2)) + 
            Power(s1,2)*(-46 + Power(s2,2)*(-8 + t1) + 78*t2 - 
               27*Power(t2,2) - 8*Power(t2,3) + 
               Power(t1,2)*(-1 + 6*t2) + 
               s2*(15 - 5*Power(t1,2) + t1*(68 - 12*t2) - 14*t2 + 
                  10*Power(t2,2)) + t1*(-161 + 88*t2 + 16*Power(t2,2))) + 
            s1*(-46 + Power(s2,2)*(11 - 4*t1) + Power(t1,2)*(7 - 24*t2) + 
               11*t2 - 17*Power(t2,2) + 32*Power(t2,3) + 
               t1*(113 + 35*t2 - 64*Power(t2,2)) + 
               s2*(6 + 17*Power(t1,2) + 43*t2 - 40*Power(t2,2) + 
                  5*t1*(-25 + 9*t2)))) - 
         s*(94 - 23*t1 - 56*Power(t1,2) - 15*Power(t1,3) - 111*t2 + 
            128*t1*t2 - 28*Power(t1,2)*t2 - 11*Power(t1,3)*t2 + 
            238*Power(t2,2) - 135*t1*Power(t2,2) + 
            87*Power(t1,2)*Power(t2,2) + 6*Power(t1,3)*Power(t2,2) - 
            91*Power(t2,3) + 28*t1*Power(t2,3) - 
            18*Power(t1,2)*Power(t2,3) + 6*Power(t2,4) + 
            6*t1*Power(t2,4) + Power(s2,4)*(-8 + 6*t2) + 
            Power(s2,3)*(38 - 3*t1*(-6 + t2) + 24*t2 - 12*Power(t2,2)) + 
            2*Power(s1,5)*(-t2 + t1*(-7 + 3*t2) + 
               s2*(-2 + 4*t1 + 3*t2)) - 
            Power(s2,2)*(44 + 39*t2 - 35*Power(t2,2) - 30*Power(t2,3) + 
               Power(t1,2)*(23 + 10*t2) + t1*(91 + 4*t2 - 17*Power(t2,2))\
) + Power(s1,4)*(Power(s2,3) + Power(t1,2)*(29 - 18*t2) + 
               Power(s2,2)*(-7 + 16*t1 + 18*t2) - 
               t1*(-91 + 32*t2 + Power(t2,2)) - 
               2*(6 - 18*t2 + 5*Power(t2,2)) - 
               s2*(-35 + 11*Power(t1,2) + t1*(96 - 22*t2) + 44*t2 + 
                  17*Power(t2,2))) + 
            s2*(-35 - 2*t2 - 28*Power(t2,2) - 83*Power(t2,3) - 
               18*Power(t2,4) + Power(t1,3)*(13 + 7*t2) + 
               Power(t1,2)*(58 + 24*t2 - 34*Power(t2,2)) + 
               t1*(74 + 89*t2 - 126*Power(t2,2) + 22*Power(t2,3))) + 
            Power(s1,3)*(51 + 2*Power(s2,4) - 4*Power(t1,3)*(-4 + t2) - 
               65*t2 + 6*Power(t2,2) + 10*Power(t2,3) + 
               Power(s2,3)*(-3 - 6*t1 + 13*t2) + 
               Power(t1,2)*(-154 + 55*t2 + 20*Power(t2,2)) - 
               t1*(123 + 88*t2 - 45*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(48 + 16*Power(t1,2) - 99*t2 - 
                  22*Power(t2,2) - t1*(97 + 14*t2)) + 
               s2*(-190 - 12*Power(t1,3) + 146*t2 + 86*Power(t2,2) + 
                  17*Power(t2,3) + Power(t1,2)*(50 + 13*t2) + 
                  t1*(306 + 27*t2 - 43*Power(t2,2)))) + 
            s1*(50 + Power(s2,4)*(14 - 8*t2) - 145*t2 - 
               123*Power(t2,2) + 106*Power(t2,3) - 8*Power(t2,4) + 
               Power(t1,3)*(63 + 5*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(-102 - 150*t2 - 7*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(121 - 68*t2 + 39*Power(t2,2) - 37*Power(t2,3) - 
                  8*Power(t2,4)) + 
               Power(s2,3)*(3*t1*(-13 + t2) + 
                  8*(-7 + 2*t2 + 2*Power(t2,2))) + 
               Power(s2,2)*(291 + 84*t2 - 150*Power(t2,2) - 
                  40*Power(t2,3) + 2*Power(t1,2)*(38 + 7*t2) + 
                  t1*(-34 - 185*t2 + 24*Power(t2,2))) + 
               s2*(-415 + 60*t2 + 131*Power(t2,2) + 153*Power(t2,3) + 
                  24*Power(t2,4) - 3*Power(t1,3)*(17 + 3*t2) + 
                  Power(t1,2)*(8 + 136*t2 + Power(t2,2)) + 
                  t1*(4 + 69*t2 + 153*Power(t2,2) - 42*Power(t2,3)))) + 
            Power(s1,2)*(-111 + 2*Power(s2,4)*(-4 + t2) + 135*t2 - 
               23*Power(t2,2) - 33*Power(t2,3) + 2*Power(t2,4) + 
               Power(s2,3)*(20 + 27*t1 - 53*t2 - 4*Power(t2,2)) + 
               2*Power(t1,3)*(-32 + 5*t2 + Power(t2,2)) + 
               Power(t1,2)*(219 + 45*t2 - 68*Power(t2,2) - 
                  6*Power(t2,3)) + 
               2*t1*(-30 + 151*t2 - 34*Power(t2,2) + 12*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(-224 + 132*t2 + 105*Power(t2,2) + 
                  10*Power(t2,3) - Power(t1,2)*(69 + 4*t2) + 
                  t1*(142 + 107*t2 - 9*Power(t2,2))) + 
               s2*(473 - 110*t2 - 228*Power(t2,2) - 79*Power(t2,3) - 
                  6*Power(t2,4) + 2*Power(t1,3)*(25 + t2) + 
                  Power(t1,2)*(-41 - 77*t2 + Power(t2,2)) + 
                  t1*(-224 - 359*t2 + 104*Power(t2,2) + 12*Power(t2,3))))) \
+ Power(s,2)*(8 + 88*t1 + 7*Power(t1,2) + 21*Power(t1,3) - 146*t2 + 
            74*t1*t2 - 95*Power(t1,2)*t2 - 6*Power(t1,3)*t2 + 
            69*Power(t2,2) - 105*t1*Power(t2,2) + 
            36*Power(t1,2)*Power(t2,2) - 5*Power(t2,3) + 
            9*t1*Power(t2,3) - 9*Power(t2,4) + 
            Power(s2,3)*(-4 - 6*t1 + 9*t2) - 
            Power(s1,4)*(12 + Power(s2,2) + 40*t1 + 
               s2*(4 - 17*t1 - 7*t2) - 18*t2 - 23*t1*t2 + 8*Power(t2,2)) + 
            Power(s2,2)*(79 + 21*Power(t1,2) + 39*t2 - 21*Power(t2,2) - 
               3*t1*(11 + 9*t2)) - 
            s2*(150 + 15*Power(t1,3) + Power(t1,2)*(1 - 32*t2) - 56*t2 - 
               24*Power(t2,2) - 51*Power(t2,3) + 
               5*t1*(17 - 25*t2 + 8*Power(t2,2))) + 
            Power(s1,3)*(14 + Power(s2,3) + 6*t2 - 11*Power(t2,2) + 
               11*Power(t2,3) - 2*Power(t1,2)*(-8 + 9*t2) + 
               Power(s2,2)*(5 + 7*t1 + 13*t2) + 
               t1*(204 - 78*t2 - 17*Power(t2,2)) + 
               s2*(12 + 2*Power(t1,2) - 28*t2 - 23*Power(t2,2) + 
                  2*t1*(-71 + 13*t2))) - 
            Power(s1,2)*(-133 + Power(s2,3)*(2*t1 - 3*t2) + 
               2*Power(t1,3)*(-4 + t2) + 146*t2 - 70*Power(t2,2) + 
               35*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,2)*(73 - 55*t2 - 12*Power(t2,2)) + 
               Power(s2,2)*(1 - 8*Power(t1,2) - t1*(-45 + t2) + 53*t2 + 
                  7*Power(t2,2)) + 
               t1*(248 + 59*t2 - 62*Power(t2,2) - 3*Power(t2,3)) + 
               s2*(144 + 6*Power(t1,3) - 2*Power(t1,2)*(-2 + t2) - 
                  79*t2 - 87*Power(t2,2) - 17*Power(t2,3) + 
                  t1*(-344 + 29*t2 + 27*Power(t2,2)))) + 
            s1*(-207 + Power(s2,3)*(3 + 8*t1 - 12*t2) + 172*t2 - 
               88*Power(t2,2) + 29*Power(t2,3) + 12*Power(t2,4) + 
               Power(t1,3)*(-29 + 8*t2) + 
               Power(t1,2)*(42 + 34*t2 - 48*Power(t2,2)) + 
               4*t1*(17 + 40*t2 + 7*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*(-74 - 29*Power(t1,2) + 25*t2 + 
                  28*Power(t2,2) + t1*(63 + 2*t2)) + 
               s2*(21*Power(t1,3) + Power(t1,2)*(11 - 10*t2) + 
                  t1*(-198 - 218*t2 + 99*Power(t2,2)) - 
                  2*(-171 + 21*t2 + 60*Power(t2,2) + 34*Power(t2,3)))))))/
     (Power(-1 + s1,3)*(-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + t2)*
       Power(s - s1 + t2,2)) - 
    (8*(-77 + 86*s2 + 2*Power(s2,2) + Power(s2,3) - 12*Power(s2,4) + 
         130*t1 - 241*s2*t1 + 94*Power(s2,2)*t1 - 3*Power(s2,3)*t1 + 
         20*Power(s2,4)*t1 - 19*Power(t1,2) + 132*s2*Power(t1,2) - 
         96*Power(s2,2)*Power(t1,2) - 9*Power(s2,3)*Power(t1,2) - 
         8*Power(s2,4)*Power(t1,2) - 43*Power(t1,3) + 30*s2*Power(t1,3) + 
         2*Power(s2,2)*Power(t1,3) + 11*Power(s2,3)*Power(t1,3) + 
         8*Power(t1,4) - 6*s2*Power(t1,4) - 2*Power(s2,2)*Power(t1,4) + 
         Power(t1,5) - s2*Power(t1,5) + 
         Power(s1,3)*(-1 + s2)*(-1 + t1)*
          (4 - 2*Power(t1,3) + Power(s2,2)*(-9 + 4*t1) - 
            2*s2*(Power(t1,2) + t1*(4 - 6*t2) + 13*(-1 + t2)) + 
            Power(t1,2)*(11 - 6*t2) + 12*t1*(-1 + t2) - 4*t2) + 
         2*Power(s,4)*(-1 + s2)*(-1 + t1)*
          (4 + Power(t1,2) + t1*(-3 + t2) - 3*t2) - 28*t2 + 32*s2*t2 - 
         14*Power(s2,2)*t2 - 109*Power(s2,3)*t2 + 27*Power(s2,4)*t2 - 
         186*t1*t2 + 376*s2*t1*t2 - 48*Power(s2,2)*t1*t2 + 
         106*Power(s2,3)*t1*t2 - 40*Power(s2,4)*t1*t2 - 
         12*Power(t1,2)*t2 - 183*s2*Power(t1,2)*t2 + 
         35*Power(s2,2)*Power(t1,2)*t2 + 11*Power(s2,3)*Power(t1,2)*t2 + 
         13*Power(s2,4)*Power(t1,2)*t2 + 70*Power(t1,3)*t2 - 
         40*s2*Power(t1,3)*t2 + 2*Power(s2,2)*Power(t1,3)*t2 - 
         16*Power(s2,3)*Power(t1,3)*t2 - 10*Power(t1,4)*t2 + 
         13*s2*Power(t1,4)*t2 + Power(s2,2)*Power(t1,4)*t2 - 
         2*Power(t1,5)*t2 + 2*s2*Power(t1,5)*t2 + 107*Power(t2,2) - 
         30*s2*Power(t2,2) - 19*Power(s2,2)*Power(t2,2) + 
         46*Power(s2,3)*Power(t2,2) - 12*Power(s2,4)*Power(t2,2) - 
         48*t1*Power(t2,2) - 85*s2*t1*Power(t2,2) - 
         4*Power(s2,2)*t1*Power(t2,2) - 87*Power(s2,3)*t1*Power(t2,2) + 
         16*Power(s2,4)*t1*Power(t2,2) + 44*Power(t1,2)*Power(t2,2) + 
         37*s2*Power(t1,2)*Power(t2,2) + 
         44*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         15*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         4*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         17*Power(t1,3)*Power(t2,2) + 14*s2*Power(t1,3)*Power(t2,2) - 
         15*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         2*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         2*Power(t1,4)*Power(t2,2) - 8*s2*Power(t1,4)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,4)*Power(t2,2) - 2*Power(t2,3) + 
         48*s2*Power(t2,3) - 52*Power(s2,2)*Power(t2,3) + 
         6*Power(s2,3)*Power(t2,3) - 32*t1*Power(t2,3) - 
         4*s2*t1*Power(t2,3) + 44*Power(s2,2)*t1*Power(t2,3) - 
         8*Power(s2,3)*t1*Power(t2,3) + 24*Power(t1,2)*Power(t2,3) - 
         20*s2*Power(t1,2)*Power(t2,3) - 
         6*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         2*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         6*Power(t1,3)*Power(t2,3) + 8*s2*Power(t1,3)*Power(t2,3) - 
         2*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         Power(s,3)*(-1 + t1)*
          (6 - 23*t1 + 3*Power(t1,2) - Power(t1,3) - 79*t2 + 17*t1*t2 + 
            Power(t1,2)*t2 + 18*Power(t2,2) - 6*t1*Power(t2,2) - 
            Power(s2,2)*(4 + t1 + Power(t1,2) - 9*t2 + 3*t1*t2) + 
            s1*(-8 + 2*Power(s2,2)*(-3 + t1) - 5*t1 + 6*Power(t1,2) + 
               7*t2 - 4*t1*t2 + 
               s2*(-2 + 11*t1 - 6*Power(t1,2) + 9*t2 - 4*t1*t2)) + 
            s2*(14 + Power(t1,3) + 54*t2 - 18*Power(t2,2) - 
               Power(t1,2)*(2 + t2) + 2*t1*(8 - 3*t2 + 3*Power(t2,2)))) + 
         Power(s1,2)*(-52 + Power(t1,5) + 
            Power(s2,4)*(3 - 4*t1 + Power(t1,2)) + 
            3*Power(t1,4)*(-1 + t2) + 44*t2 + 8*Power(t2,2) + 
            t1*(48 - 30*t2 - 44*Power(t2,2)) + 
            Power(t1,3)*(70 - 44*t2 - 8*Power(t2,2)) + 
            Power(t1,2)*(-232 + 115*t2 + 28*Power(t2,2)) + 
            Power(s2,3)*(-71 + 5*Power(t1,3) + t1*(102 - 28*t2) - 3*t2 + 
               Power(t1,2)*(-44 + 7*t2)) + 
            s2*(-92 - Power(t1,5) + Power(t1,4)*(12 - 7*t2) + 72*t2 + 
               46*Power(t2,2) + 
               Power(t1,2)*(111 + 43*t2 - 10*Power(t2,2)) - 
               4*t1*(-64 + 50*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(-86 + 20*t2 + 8*Power(t2,2))) + 
            Power(s2,2)*(-5*Power(t1,4) + Power(t1,3)*(27 + 8*t2) + 
               Power(t1,2)*(28 - 29*t2 - 18*Power(t2,2)) - 
               3*(-40 + 7*t2 + 18*Power(t2,2)) + 
               2*t1*(-97 + 25*t2 + 28*Power(t2,2)))) + 
         s1*(125 + 2*Power(t1,5)*(-1 + t2) - 12*t2 - 115*Power(t2,2) + 
            2*Power(t2,3) + Power(s2,4)*(3 - 4*t1 + Power(t1,2))*
             (-11 + 9*t2) + Power(t1,4)*(41 - 39*t2 - 4*Power(t2,2)) + 
            Power(t1,3)*(-226 + 158*t2 + 49*Power(t2,2) + 
               4*Power(t2,3)) + 
            2*t1*(-99 + 91*t2 + 76*Power(t2,2) + 13*Power(t2,3)) - 
            Power(t1,2)*(-428 + 211*t2 + 154*Power(t2,2) + 
               16*Power(t2,3)) + 
            Power(s2,3)*(89 + 14*t2 + 18*Power(t2,2) - 
               2*Power(t1,3)*(-7 + 8*t2) + 
               Power(t1,2)*(-39 + 56*t2 - 3*Power(t2,2)) + 
               t1*(-56 - 22*t2 + 9*Power(t2,2))) - 
            s2*(-20 + 2*Power(t1,5)*(-1 + t2) + 64*t2 + 76*Power(t2,2) + 
               42*Power(t2,3) + 
               Power(t1,4)*(40 - 30*t2 - 8*Power(t2,2)) + 
               t1*(175 + 98*t2 - 171*Power(t2,2) - 8*Power(t2,3)) + 
               Power(t1,2)*(166 - 104*t2 + 43*Power(t2,2) - 
                  6*Power(t2,3)) + 
               Power(t1,3)*(-159 + 98*t2 + 20*Power(t2,2) + 
                  4*Power(t2,3))) + 
            Power(s2,2)*(-109 + 35*t2 + 81*Power(t2,2) + 40*Power(t2,3) + 
               Power(t1,4)*(-5 + 9*t2) + 
               Power(t1,3)*(37 - 44*t2 - 13*Power(t2,2)) + 
               2*Power(t1,2)*(-38 + 21*t2 + 32*Power(t2,2) + 
                  5*Power(t2,3)) - 
               t1*(-177 + 26*t2 + 124*Power(t2,2) + 34*Power(t2,3)))) + 
         Power(s,2)*(9 - 271*t1 + 134*Power(t1,2) - 39*Power(t1,3) - 
            2*Power(t1,4) + Power(t1,5) - 
            Power(s2,3)*(-4 + 11*t1 - 8*Power(t1,2) + Power(t1,3)) - 
            79*t2 + 329*t1*t2 - 193*Power(t1,2)*t2 + 30*Power(t1,3)*t2 + 
            Power(t1,4)*t2 - 7*Power(t2,2) - 13*t1*Power(t2,2) + 
            4*Power(t1,2)*Power(t2,2) - 6*Power(t2,3) + 
            8*t1*Power(t2,3) - 2*Power(t1,2)*Power(t2,3) + 
            Power(s1,2)*(-1 + t1)*
             (-4 - 6*Power(t1,2) + Power(s2,2)*(-3 + 2*t1) + 
               s2*(6*Power(t1,2) + 12*t1*(-2 + t2) - 23*(-1 + t2)) + 
               t1*(14 - 4*t2) + 7*t2) + 
            Power(s2,2)*(55 + 2*Power(t1,4) + 90*t2 - 48*Power(t2,2) + 
               Power(t1,3)*(-13 + 7*t2) - 
               4*Power(t1,2)*(-2 + 5*t2 + 4*Power(t2,2)) + 
               t1*(-44 - 53*t2 + 64*Power(t2,2))) - 
            s2*(100 + Power(t1,5) - 5*t2 + Power(t1,4)*t2 - 
               71*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,3)*(-49 + 33*t2) - 
               Power(t1,2)*(-110 + 165*t2 + 20*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-194 + 120*t2 + 75*Power(t2,2) + 8*Power(t2,3))) + 
            s1*(7 + 4*Power(t1,4) - 
               Power(s2,3)*(3 - 4*t1 + Power(t1,2)) + 
               Power(t1,3)*(3 - 17*t2) - 70*t2 + 39*Power(t2,2) + 
               t1*(234 - 77*t2 - 31*Power(t2,2)) + 
               Power(t1,2)*(-80 + 76*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*(13*Power(t1,3) + t1*(93 - 46*t2) + 
                  9*(-6 + t2) + Power(t1,2)*(-60 + 13*t2)) - 
               s2*(-98 + 4*Power(t1,4) + Power(t1,3)*(12 - 13*t2) - 
                  29*t2 + 55*Power(t2,2) + 
                  t1*(223 + 9*t2 - 55*Power(t2,2)) + 
                  Power(t1,2)*(-109 + 49*t2 + 16*Power(t2,2))))) + 
         s*(-92 + 151*t1 - 315*Power(t1,2) + 105*Power(t1,3) - 
            19*Power(t1,4) + 2*Power(t1,5) - 66*t2 - 276*t1*t2 + 
            361*Power(t1,2)*t2 - 110*Power(t1,3)*t2 + 13*Power(t1,4)*t2 - 
            2*Power(t1,5)*t2 + Power(s2,4)*(3 - 4*t1 + Power(t1,2))*t2 + 
            68*Power(t2,2) + 43*t1*Power(t2,2) - 
            40*Power(t1,2)*Power(t2,2) - 5*Power(t1,3)*Power(t2,2) + 
            6*Power(t1,4)*Power(t2,2) - 46*Power(t2,3) + 
            36*t1*Power(t2,3) - 4*Power(t1,2)*Power(t2,3) - 
            2*Power(t1,3)*Power(t2,3) - 
            Power(s1,3)*(-1 + s2)*(-1 + t1)*
             (2*Power(t1,2) + s2*(-9 + 4*t1) - 20*(-1 + t2) + 
               5*t1*(-3 + 2*t2)) + 
            Power(s2,3)*(-37 + Power(t1,3)*(2 - 6*t2) - 57*t2 + 
               42*Power(t2,2) + t1*(28 + 25*t2 - 56*Power(t2,2)) + 
               Power(t1,2)*(-1 + 14*t2 + 14*Power(t2,2))) + 
            s2*(-78 + 2*Power(t1,5)*(-1 + t2) + 165*t2 + 21*Power(t2,2) + 
               58*Power(t2,3) + 
               Power(t1,4)*(23 - 20*t2 - 6*Power(t2,2)) + 
               t1*(205 - 151*t2 + 7*Power(t2,2) - 52*Power(t2,3)) + 
               Power(t1,3)*(-63 + 84*t2 + 11*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,2)*(115 - 120*t2 + 15*Power(t2,2) + 8*Power(t2,3))\
) + Power(s2,2)*(115 + 15*t2 + 3*Power(t1,4)*t2 - 99*Power(t2,2) - 
               12*Power(t2,3) - 2*Power(t1,3)*(14 - 6*t2 + Power(t2,2)) - 
               Power(t1,2)*(-65 + 80*t2 + 29*Power(t2,2) + 
                  4*Power(t2,3)) + 
               2*t1*(-88 + 33*t2 + 69*Power(t2,2) + 8*Power(t2,3))) + 
            Power(s1,2)*(3 - 5*Power(t1,4) - 
               3*Power(s2,3)*(2 - 3*t1 + Power(t1,2)) + 25*t2 - 
               54*Power(t2,2) + 2*Power(t1,3)*(3 + 5*t2) + 
               Power(t1,2)*(83 - 54*t2 - 18*Power(t2,2)) + 
               t1*(-255 + 107*t2 + 56*Power(t2,2)) + 
               Power(s2,2)*(91 - 10*Power(t1,3) - 
                  18*Power(t1,2)*(-4 + t2) - 17*t2 + t1*(-145 + 59*t2)) + 
               s2*(5*Power(t1,4) - 6*Power(t1,3)*t2 + 
                  t1*(259 - 34*t2 - 56*Power(t2,2)) + 
                  6*(-20 + 4*t2 + 9*Power(t2,2)) + 
                  2*Power(t1,2)*(-56 + 16*t2 + 9*Power(t2,2)))) + 
            s1*(73 - 2*Power(t1,5) - 
               Power(s2,4)*(3 - 4*t1 + Power(t1,2)) + 
               Power(t1,4)*(5 - 4*t2) + 3*t2 + 46*Power(t2,2) + 
               40*Power(t2,3) - 3*Power(t1,3)*(3 - 4*t2 + 3*Power(t2,2)) + 
               2*Power(t1,2)*(-10 + 32*t2 + 28*Power(t2,2) + 
                  5*Power(t2,3)) - 
               t1*(-289 + 83*t2 + 149*Power(t2,2) + 34*Power(t2,3)) + 
               Power(s2,3)*(85 - 4*Power(t1,3) - 27*t2 - 
                  6*Power(t1,2)*(-7 + 3*t2) + 23*t1*(-5 + 3*t2)) + 
               Power(s2,2)*(-202 + 3*Power(t1,4) - 29*t2 + 
                  37*Power(t2,2) - 2*Power(t1,3)*(7 + t2) + 
                  t1*(280 - 31*t2 - 64*Power(t2,2)) + 
                  Power(t1,2)*(-51 + 22*t2 + 19*Power(t2,2))) + 
               s2*(171 + 2*Power(t1,5) - 39*t2 - 115*Power(t2,2) - 
                  40*Power(t2,3) + 4*Power(t1,4)*(-3 + 2*t2) + 
                  Power(t1,3)*(15 + 6*t2 + 5*Power(t2,2)) - 
                  Power(t1,2)*
                   (-126 + 204*t2 + 35*Power(t2,2) + 10*Power(t2,3)) + 
                  t1*(-534 + 253*t2 + 81*Power(t2,2) + 34*Power(t2,3)))))))/
     ((-1 + s1)*Power(-1 + s2,2)*Power(-1 + t1,3)*(s - s2 + t1)*
       (-s + s1 - t2)*(-1 + t2)) + 
    (8*(128 - 176*s2 + 40*Power(s2,2) + 8*Power(s2,3) - 80*t1 + 
         144*s2*t1 - 56*Power(s2,2)*t1 - 8*Power(s2,3)*t1 - 
         56*Power(t1,2) + 40*s2*Power(t1,2) + 16*Power(s2,2)*Power(t1,2) + 
         8*Power(t1,3) - 8*s2*Power(t1,3) - 56*t2 + 253*s2*t2 - 
         181*Power(s2,2)*t2 + 54*Power(s2,3)*t2 + 4*Power(s2,4)*t2 - 
         21*t1*t2 - 53*s2*t1*t2 + 36*Power(s2,2)*t1*t2 - 
         44*Power(s2,3)*t1*t2 + 66*Power(t1,2)*t2 - 
         107*s2*Power(t1,2)*t2 + 45*Power(s2,2)*Power(t1,2)*t2 + 
         2*Power(s2,3)*Power(t1,2)*t2 + 9*Power(t1,3)*t2 - 
         7*s2*Power(t1,3)*t2 + 2*Power(t1,4)*t2 - 2*s2*Power(t1,4)*t2 - 
         76*Power(t2,2) - 25*s2*Power(t2,2) - 44*Power(s2,2)*Power(t2,2) - 
         3*Power(s2,3)*Power(t2,2) + 32*Power(s2,4)*Power(t2,2) + 
         49*t1*Power(t2,2) + 99*s2*t1*Power(t2,2) + 
         67*Power(s2,2)*t1*Power(t2,2) - 86*Power(s2,3)*t1*Power(t2,2) - 
         2*Power(s2,4)*t1*Power(t2,2) - 3*Power(t1,2)*Power(t2,2) - 
         87*s2*Power(t1,2)*Power(t2,2) + 
         67*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         5*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         23*Power(t1,3)*Power(t2,2) - 14*s2*Power(t1,3)*Power(t2,2) - 
         2*Power(s2,2)*Power(t1,3)*Power(t2,2) + Power(t1,4)*Power(t2,2) - 
         s2*Power(t1,4)*Power(t2,2) + 28*Power(t2,3) - 70*s2*Power(t2,3) + 
         41*Power(s2,2)*Power(t2,3) + 8*Power(s2,3)*Power(t2,3) + 
         12*Power(s2,4)*Power(t2,3) + 34*t1*Power(t2,3) - 
         11*s2*t1*Power(t2,3) - 12*Power(s2,2)*t1*Power(t2,3) - 
         21*Power(s2,3)*t1*Power(t2,3) - Power(s2,4)*t1*Power(t2,3) - 
         30*Power(t1,2)*Power(t2,3) + 31*s2*Power(t1,2)*Power(t2,3) - 
         6*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         4*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         19*Power(t1,3)*Power(t2,3) + 15*s2*Power(t1,3)*Power(t2,3) - 
         3*Power(s2,2)*Power(t1,3)*Power(t2,3) - 24*Power(t2,4) + 
         19*s2*Power(t2,4) + 17*Power(s2,2)*Power(t2,4) + 
         9*Power(s2,3)*Power(t2,4) + 17*t1*Power(t2,4) - 
         38*s2*t1*Power(t2,4) - 12*Power(s2,2)*t1*Power(t2,4) + 
         Power(s2,3)*t1*Power(t2,4) + 9*Power(t1,2)*Power(t2,4) + 
         7*s2*Power(t1,2)*Power(t2,4) - 
         3*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         4*Power(t1,3)*Power(t2,4) + 2*s2*Power(t1,3)*Power(t2,4) - 
         s2*Power(t2,5) + 3*Power(s2,2)*Power(t2,5) + t1*Power(t2,5) - 
         3*s2*t1*Power(t2,5) + 
         Power(s1,5)*(s2 - t1)*
          (-12 + 5*Power(s2,2) - t1*(-12 + t2) + s2*(6 - 11*t1 + t2)) + 
         2*Power(s,5)*(-7*(-1 + s2)*t2 + t1*(6 - 2*s2 - 8*t2 + 4*s2*t2) + 
            s1*(1 - 7*t1 - 3*t2 + 4*t1*t2 + s2*(-1 + 3*t1 + 3*t2))) + 
         Power(s1,4)*(5*Power(s2,4) - 24*(-1 + t2) - 
            Power(t1,3)*(9 + t2) - Power(s2,3)*(26 + 10*t1 + 15*t2) + 
            Power(t1,2)*(42 + 37*t2 - 5*Power(t2,2)) - 
            t1*(40 + 5*t2 + Power(t2,2)) + 
            Power(s2,2)*(1 - 5*Power(t1,2) - 7*t2 - 4*Power(t2,2) + 
               t1*(68 + 52*t2)) + 
            s2*(4 + 10*Power(t1,3) + 41*t2 + Power(t2,2) - 
               3*Power(t1,2)*(11 + 12*t2) + 
               t1*(-31 - 42*t2 + 9*Power(t2,2)))) + 
         Power(s,4)*(20 + 47*t1 + 11*Power(t1,2) + 3*Power(t1,3) + 5*t2 + 
            43*t1*t2 - 29*Power(t1,2)*t2 + 28*Power(t2,2) - 
            45*t1*Power(t2,2) + 3*Power(t2,3) + 
            Power(s2,2)*(12 + Power(t1,2) + t1*(4 - 17*t2) + 40*t2) + 
            Power(s1,2)*(-14 + 8*Power(s2,2) + t1*(54 - 25*t2) + 
               s2*(4 - 29*t1 - 13*t2) + 14*t2 + Power(t2,2)) - 
            s2*(12 + 3*Power(t1,3) + Power(t1,2)*(3 - 20*t2) + 77*t2 + 
               19*Power(t2,2) + t1*(47 + 2*t2 - 17*Power(t2,2))) - 
            s1*(10 + Power(t1,2)*(5 - 9*t2) + 18*t2 + 12*Power(t2,2) + 
               3*Power(t2,3) + Power(s2,2)*(18 + 7*t1 + 23*t2) + 
               2*s2*(-5 - 42*t1 + 2*Power(t1,2) - 36*t2 - Power(t2,2)) - 
               t1*(-106 + t2 + 28*Power(t2,2)))) + 
         Power(s1,3)*(-20 - 3*Power(t1,4) - 52*t2 + 72*Power(t2,2) + 
            Power(t1,3)*(-10 + 33*t2 + 4*Power(t2,2)) + 
            Power(s2,3)*(60 + 58*t1 - 3*Power(t1,2) + 60*t2 + 46*t1*t2 + 
               5*Power(t2,2)) + 
            t1*(122 + 22*t2 - 43*Power(t2,2) + Power(t2,3)) + 
            Power(t1,2)*(-16 - 102*t2 - 33*Power(t2,2) + 7*Power(t2,3)) + 
            Power(s2,4)*(t1 - 3*(8 + 7*t2)) - 
            Power(s2,2)*(-31 + Power(t1,3) + Power(t1,2)*(34 - 4*t2) - 
               4*t2 + 15*Power(t2,2) - 2*Power(t2,3) + 
               t1*(90 + 202*t2 + 37*Power(t2,2))) + 
            s2*(-86 + 3*Power(t1,4) + Power(t1,3)*(3 - 29*t2) + 50*t2 - 
               65*Power(t2,2) - Power(t2,3) + 
               Power(t1,2)*(48 + 101*t2 + 28*Power(t2,2)) + 
               t1*(-39 + 86*t2 + 84*Power(t2,2) - 9*Power(t2,3)))) + 
         Power(s1,2)*(68 - 4*t2 + 8*Power(t2,2) - 72*Power(t2,3) + 
            Power(t1,4)*(7 + 6*t2) + 
            Power(s2,4)*(24 - 2*t1 + 72*t2 - 3*t1*t2 + 27*Power(t2,2)) + 
            Power(t1,3)*(55 + 12*t2 - 45*Power(t2,2) - 5*Power(t2,3)) + 
            Power(t1,2)*(33 + 34*t2 + 61*Power(t2,2) + 7*Power(t2,3) - 
               3*Power(t2,4)) + 
            t1*(-253 - 53*t2 + 69*Power(t2,2) + 58*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,3)*(-17 - 92*t2 - 13*Power(t2,2) + 15*Power(t2,3) + 
               Power(t1,2)*(3 + 10*t2) - 
               t1*(58 + 177*t2 + 61*Power(t2,2))) + 
            Power(s2,2)*(-198 - Power(t1,3)*(-6 + t2) - 66*t2 + 
               28*Power(t2,2) + 38*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,2)*(79 + 99*t2 + 4*Power(t2,2)) + 
               t1*(53 + 244*t2 + 138*Power(t2,2) - 18*Power(t2,3))) - 
            s2*(-229 + 31*t2 + 69*Power(t2,2) - 50*Power(t2,3) + 
               Power(t2,4) + Power(t1,4)*(7 + 6*t2) + 
               Power(t1,3)*(52 - 30*Power(t2,2)) + 
               Power(t1,2)*(107 + 164*t2 + 64*Power(t2,2) - 
                  8*Power(t2,3)) + 
               t1*(-137 - 120*t2 + 113*Power(t2,2) + 81*Power(t2,3) + 
                  Power(t2,4)))) + 
         s1*(-(Power(t1,4)*(2 + 8*t2 + 3*Power(t2,2))) - 
            Power(s2,4)*(4 - 4*(-14 + t1)*t2 - 
               3*(-20 + t1)*Power(t2,2) + 11*Power(t2,3)) + 
            Power(t1,2)*(-98 + 42*t2 + 28*Power(t2,2) - 10*Power(t2,3) + 
               Power(t2,4)) + 
            Power(t1,3)*(7 - 102*t2 + 17*Power(t2,2) + 25*Power(t2,3) + 
               2*Power(t2,4)) + 
            4*(-50 + 34*t2 - Power(t2,2) + 11*Power(t2,3) + 
               6*Power(t2,4)) - 
            t1*(-293 - 28*t2 + 119*Power(t2,2) + 68*Power(t2,3) + 
               23*Power(t2,4) + Power(t2,5)) - 
            Power(s2,3)*(38 + 4*t2 - 24*Power(t2,2) + 30*Power(t2,3) + 
               10*Power(t2,4) + 
               Power(t1,2)*(2 + 8*t2 + 11*Power(t2,2)) - 
               4*t1*(7 + 42*t2 + 35*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s2,2)*(181 + 218*t2 - 22*Power(t2,2) - 
               50*Power(t2,3) - 25*Power(t2,4) - 3*Power(t2,5) + 
               Power(t1,3)*t2*(-4 + 5*t2) - 
               Power(t1,2)*(13 + 194*t2 + 59*Power(t2,2)) + 
               t1*(-68 - 48*t2 - 126*Power(t2,2) + 8*Power(t2,3) + 
                  19*Power(t2,4))) + 
            s2*(-13 - 284*t2 + 203*Power(t2,2) - 4*Power(t2,3) - 
               13*Power(t2,4) + Power(t2,5) + 
               Power(t1,4)*(2 + 8*t2 + 3*Power(t2,2)) - 
               Power(t1,3)*(9 - 90*t2 + 18*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(t1,2)*(107 + 170*t2 + 69*Power(t2,2) - 
                  11*Power(t2,3) - 11*Power(t2,4)) + 
               t1*(-171 - 156*t2 - 70*Power(t2,2) + 96*Power(t2,3) + 
                  36*Power(t2,4) + 3*Power(t2,5)))) + 
         Power(s,3)*(-69 + 83*t1 + 52*Power(t1,2) + 6*Power(t1,3) + 
            3*Power(t1,4) + 102*t2 + 152*t1*t2 - 7*Power(t1,2)*t2 - 
            7*Power(t1,3)*t2 - 26*Power(t2,2) + 62*t1*Power(t2,2) - 
            75*Power(t1,2)*Power(t2,2) + 21*Power(t2,3) - 
            39*t1*Power(t2,3) + 6*Power(t2,4) - 
            Power(s2,3)*(20 + Power(t1,2) + 38*t2 - 10*t1*t2) + 
            2*Power(s2,2)*(13 + 2*Power(t1,3) + 
               Power(t1,2)*(2 - 11*t2) + 73*t2 + 31*Power(t2,2) + 
               t1*(28 + 13*t2 - 18*Power(t2,2))) + 
            Power(s1,3)*(-21*Power(s2,2) + s2*(-8 + 51*t1 + 7*t2) + 
               3*t1*(-26 + 9*t2) - 2*(-17 + 5*t2 + Power(t2,2))) - 
            s2*(-11 + 3*Power(t1,4) + Power(t1,3)*(9 - 6*t2) + 97*t2 + 
               122*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(37 - 43*t2 - 43*Power(t2,2)) - 
               2*t1*(-67 - 106*t2 + 11*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s1,2)*(12 - 13*Power(s2,3) + 
               Power(t1,2)*(27 - 28*t2) - 35*t2 + 17*Power(t2,2) + 
               8*Power(t2,3) + t1*(208 + 85*t2 - 60*Power(t2,2)) + 
               Power(s2,2)*(44*t1 + 68*(1 + t2)) + 
               s2*(-27 + Power(t1,2) - 105*t2 + 7*Power(t2,2) - 
                  t1*(224 + 53*t2))) + 
            s1*(-8 - 66*t2 + Power(t1,3)*t2 + 5*Power(t2,2) - 
               19*Power(t2,3) - 6*Power(t2,4) + 
               Power(s2,3)*(34 + 28*t2) + 
               2*Power(t1,2)*(-58 + 33*t2 + 16*Power(t2,2)) + 
               t1*(-154 - 280*t2 + 47*Power(t2,2) + 29*Power(t2,3)) + 
               s2*(53 - Power(t1,3) + Power(t1,2)*(61 - 52*t2) + 
                  200*t2 + 85*Power(t2,2) - 8*Power(t2,3) + 
                  t1*(266 + 263*t2 - 35*Power(t2,2))) + 
               Power(s2,2)*(9*Power(t1,2) - 2*t1*(59 + 9*t2) - 
                  2*(36 + 100*t2 + 13*Power(t2,2))))) + 
         Power(s,2)*(-41 - 71*t1 + 104*Power(t1,2) + 7*Power(t1,3) + 
            7*Power(t1,4) - 36*t2 + 126*t1*t2 + 151*Power(t1,2)*t2 - 
            28*Power(t1,3)*t2 + 6*Power(t1,4)*t2 + 96*Power(t2,2) + 
            92*t1*Power(t2,2) - 20*Power(t1,2)*Power(t2,2) - 
            27*Power(t1,3)*Power(t2,2) - 38*Power(t2,3) + 
            47*t1*Power(t2,3) - 63*Power(t1,2)*Power(t2,3) + 
            8*Power(t2,4) - 7*t1*Power(t2,4) + 3*Power(t2,5) + 
            Power(s2,4)*(8 - (-12 + t1)*t2) + 
            Power(s1,4)*(-34 + 18*Power(s2,2) + t1*(50 - 11*t2) + 2*t2 + 
               Power(t2,2) + s2*(12 - 39*t1 + t2)) + 
            Power(s2,3)*(-18 - 123*t2 - 67*Power(t2,2) + 
               Power(t1,2)*(-5 + 2*t2) + t1*(-17 - 6*t2 + 21*Power(t2,2))\
) + Power(s2,2)*(-97 + 113*t2 + 157*Power(t2,2) + 7*Power(t2,3) + 
               Power(t1,3)*(12 + 5*t2) - 
               2*Power(t1,2)*(-5 + 18*t2 + 25*Power(t2,2)) + 
               t1*(141 + 262*t2 + 52*Power(t2,2) - 21*Power(t2,3))) - 
            s2*(-246 + 79*t2 + 79*Power(t2,2) + 69*Power(t2,3) + 
               3*Power(t2,4) + Power(t1,4)*(7 + 6*t2) + 
               Power(t1,3)*(12 - 20*t2 - 23*Power(t2,2)) + 
               Power(t1,2)*(105 + 88*t2 - 74*Power(t2,2) - 
                  26*Power(t2,3)) + 
               t1*(98 + 338*t2 + 225*Power(t2,2) - 42*Power(t2,3) - 
                  Power(t2,4))) + 
            Power(s1,3)*(14 + 31*Power(s2,3) + 68*t2 - 4*Power(t2,2) - 
               5*Power(t2,3) + Power(t1,2)*(-51 + 30*t2) - 
               Power(s2,2)*(70 + 83*t1 + 60*t2) + 
               t1*(-134 - 109*t2 + 36*Power(t2,2)) + 
               s2*(-4 + 21*Power(t1,2) + 42*t2 - 12*Power(t2,2) + 
                  t1*(218 + 72*t2))) + 
            Power(s1,2)*(-66 + 5*Power(s2,4) + 51*t2 - 66*Power(t2,2) + 
               13*Power(t2,3) + 7*Power(t2,4) - 3*Power(t1,3)*(6 + t2) - 
               Power(s2,3)*(110 + 10*t1 + 87*t2) + 
               Power(t1,2)*(241 - 8*t2 - 69*Power(t2,2)) + 
               t1*(77 + 348*t2 + 36*Power(t2,2) - 31*Power(t2,3)) + 
               Power(s2,2)*(149 - 26*Power(t1,2) + 265*t2 + 
                  24*Power(t2,2) + 153*t1*(2 + t2)) + 
               s2*(-28 + 21*Power(t1,3) - 80*t2 - 86*Power(t2,2) + 
                  9*Power(t2,3) + 2*Power(t1,2)*(-73 + 4*t2) + 
                  t1*(-388 - 520*t2 + 29*Power(t2,2)))) + 
            s1*(202 - 9*Power(t1,4) + Power(s2,4)*(-14 + t1 - 11*t2) - 
               111*t2 - 3*Power(t2,2) + 20*Power(t2,3) - 14*Power(t2,4) - 
               3*Power(t2,5) + 
               Power(t1,3)*(-22 + 47*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(-228 - 214*t2 + 134*Power(t2,2) + 
                  37*Power(t2,3)) + 
               t1*(-28 - 309*t2 - 210*Power(t2,2) + 20*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,3)*(92 - Power(t1,2) + 208*t2 + 46*Power(t2,2) + 
                  t1*(38 + 6*t2)) - 
               Power(s2,2)*(70 + 9*Power(t1,3) + 
                  Power(t1,2)*(28 - 62*t2) + 337*t2 + 157*Power(t2,2) - 
                  14*Power(t2,3) + t1*(253 + 427*t2 + 26*Power(t2,2))) + 
               s2*(-225 + 9*Power(t1,4) + Power(t1,3)*(21 - 41*t2) + 
                  162*t2 + 150*Power(t2,2) + 49*Power(t2,3) + 
                  2*Power(t2,4) + 
                  Power(t1,2)*(214 + 93*t2 - 69*Power(t2,2)) + 
                  t1*(290 + 754*t2 + 195*Power(t2,2) - 57*Power(t2,3))))) \
+ s*(120 - 165*t1 + 26*Power(t1,2) + 17*Power(t1,3) + 2*Power(t1,4) + 
            Power(s1,5)*(12 - 5*Power(s2,2) + s2*(-6 + 11*t1 - t2) + 
               t1*(-12 + t2)) - 157*t2 + 34*t1*t2 + 85*Power(t1,2)*t2 + 
            30*Power(t1,3)*t2 + 8*Power(t1,4)*t2 + 53*Power(t2,2) + 
            85*t1*Power(t2,2) + 69*Power(t1,2)*Power(t2,2) - 
            53*Power(t1,3)*Power(t2,2) + 3*Power(t1,4)*Power(t2,2) - 
            10*Power(t2,3) + 4*t1*Power(t2,3) + 
            7*Power(t1,2)*Power(t2,3) - 21*Power(t1,3)*Power(t2,3) - 
            7*Power(t2,4) + 17*t1*Power(t2,4) - 
            17*Power(t1,2)*Power(t2,4) + Power(t2,5) + 3*t1*Power(t2,5) + 
            Power(s2,4)*(4 - 2*(-20 + t1)*t2 - 2*(-12 + t1)*Power(t2,2)) + 
            Power(s2,3)*(62 - 21*t2 - 95*Power(t2,2) - 20*Power(t2,3) + 
               Power(t1,2)*(2 + 7*Power(t2,2)) + 
               t1*(-52 - 103*t2 - 27*Power(t2,2) + 12*Power(t2,3))) - 
            s2*(-45 - 245*t2 + 152*Power(t2,2) - 25*Power(t2,3) + 
               25*Power(t2,4) + 6*Power(t2,5) + 
               Power(t1,4)*(2 + 8*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(15 + 26*t2 - 44*Power(t2,2) - 
                  16*Power(t2,3)) + 
               t1*(-139 + 39*t2 + 223*Power(t2,2) + 98*Power(t2,3) - 
                  19*Power(t2,4)) + 
               Power(t1,2)*(83 + 176*t2 + 20*Power(t2,2) - 
                  35*Power(t2,3) - 3*Power(t2,4))) + 
            Power(s2,2)*(-157 - 125*t2 - 2*Power(t1,3)*(-5 + t2)*t2 + 
               128*Power(t2,2) + 40*Power(t2,3) - 12*Power(t2,4) + 
               Power(t1,2)*(61 + 77*t2 - 46*Power(t2,2) - 
                  30*Power(t2,3)) - 
               2*t1*(2 - 96*t2 - 97*Power(t2,2) - 9*Power(t2,3) + 
                  Power(t2,4))) - 
            Power(s1,4)*(16 + 23*Power(s2,3) + 29*t2 + Power(t2,2) - 
               2*Power(s2,2)*(7 + 31*t1 + 7*t2) + 
               Power(t1,2)*(-41 + 12*t2) + 
               t1*(-8 - 39*t2 + 4*Power(t2,2)) + 
               s2*(-33 + 29*Power(t1,2) - 5*t2 - 3*Power(t2,2) + 
                  5*t1*(16 + 5*t2))) + 
            Power(s1,3)*(30 - 10*Power(s2,4) + 42*t2 + 29*Power(t2,2) + 
               Power(t2,3) + 3*Power(t1,3)*(8 + t2) + 
               2*Power(s2,3)*(51 + 10*t1 + 37*t2) + 
               2*Power(t1,2)*(-89 - 33*t2 + 21*Power(t2,2)) + 
               t1*(70 - 106*t2 - 37*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(-90 + 21*Power(t1,2) - 92*t2 + 
                  9*Power(t2,2) - 10*t1*(26 + 17*t2)) + 
               s2*(-17 - 27*Power(t1,3) - 100*t2 + 19*Power(t2,2) + 
                  3*Power(t2,3) + Power(t1,2)*(121 + 60*t2) + 
                  t1*(200 + 301*t2 - 20*Power(t2,2)))) + 
            Power(s1,2)*(-121 + 9*Power(t1,4) + 23*t2 - 67*Power(t2,2) - 
               14*Power(t2,3) + Power(t2,4) + 
               Power(s2,4)*(38 - 2*t1 + 32*t2) + 
               Power(t1,3)*(26 - 73*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(192 + 323*t2 - 26*Power(t2,2) - 
                  44*Power(t2,3)) + 
               t1*(-169 + 77*t2 + 139*Power(t2,2) + 20*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,3)*(-132 + 5*Power(t1,2) - 246*t2 - 
                  59*Power(t2,2) - 2*t1*(48 + 31*t2)) + 
               Power(s2,2)*(13 + 6*Power(t1,3) + 
                  Power(t1,2)*(58 - 44*t2) + 227*t2 + 82*Power(t2,2) - 
                  32*Power(t2,3) + t1*(287 + 617*t2 + 106*Power(t2,2))) - 
               s2*(-308 + 9*Power(t1,4) + Power(t1,3)*(15 - 64*t2) + 
                  85*t2 - 94*Power(t2,2) + 51*Power(t2,3) + 
                  11*Power(t2,4) + 
                  Power(t1,2)*(225 + 237*t2 + 2*Power(t2,2)) + 
                  t1*(125 + 586*t2 + 269*Power(t2,2) - 59*Power(t2,3)))) + 
            s1*(29 + 92*t2 - 59*Power(t2,2) + 48*Power(t2,3) + 
               Power(t2,4) - Power(t2,5) - 2*Power(t1,4)*(7 + 6*t2) + 
               2*Power(s2,4)*(-16 + t1 - 37*t2 + 2*t1*t2 - 
                  11*Power(t2,2)) + 
               Power(t1,3)*(-62 + 16*t2 + 72*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(-105 - 325*t2 - 126*Power(t2,2) + 
                  64*Power(t2,3) + 14*Power(t2,4)) - 
               t1*(-236 - 7*t2 + 111*Power(t2,2) + 62*Power(t2,3) + 
                  13*Power(t2,4) + 3*Power(t2,5)) + 
               Power(s2,3)*(35 + Power(t1,2)*(2 - 12*t2) + 207*t2 + 
                  144*Power(t2,2) + 8*Power(t2,3) + 
                  t1*(75 + 163*t2 + 30*Power(t2,2))) - 
               Power(s2,2)*(-263 + 64*t2 + 199*Power(t2,2) - 
                  14*Power(t2,4) + 2*Power(t1,3)*(9 + 2*t2) + 
                  Power(t1,2)*(89 + 49*t2 - 53*Power(t2,2)) + 
                  t1*(162 + 589*t2 + 325*Power(t2,2) - 4*Power(t2,3))) + 
               s2*(-499 - 7*t2 + 69*Power(t2,2) + 2*Power(t2,3) + 
                  39*Power(t2,4) + 6*Power(t2,5) + 
                  2*Power(t1,4)*(7 + 6*t2) + 
                  Power(t1,3)*(64 - 20*t2 - 53*Power(t2,2)) + 
                  Power(t1,2)*
                   (180 + 376*t2 + 49*Power(t2,2) - 32*Power(t2,3)) + 
                  t1*(17 + 247*t2 + 464*Power(t2,2) + 41*Power(t2,3) - 
                     25*Power(t2,4)))))))/
     ((-1 + s1)*(-1 + s2)*(-1 + t1)*Power(s - s2 + t1,2)*(-1 + t2)*
       Power(s - s1 + t2,3)) - 
    (8*(-96 + 152*s2 - 72*Power(s2,2) + 16*Power(s2,3) + 40*t1 - 
         64*s2*t1 + 40*Power(s2,2)*t1 - 16*Power(s2,3)*t1 + 
         40*Power(t1,2) - 72*s2*Power(t1,2) + 32*Power(s2,2)*Power(t1,2) + 
         16*Power(t1,3) - 16*s2*Power(t1,3) + 55*t2 - 141*s2*t2 + 
         98*Power(s2,2)*t2 - 44*Power(s2,3)*t2 + 8*Power(s2,4)*t2 - 
         62*t1*t2 + 38*s2*t1*t2 + 36*Power(s2,2)*t1*t2 - 
         8*Power(s2,3)*t1*t2 + 9*Power(t1,2)*t2 + 13*s2*Power(t1,2)*t2 - 
         6*Power(s2,2)*Power(t1,2)*t2 - 2*Power(t1,3)*t2 + 
         6*s2*Power(t1,3)*t2 + 39*Power(t2,2) + 136*s2*Power(t2,2) - 
         27*Power(s2,2)*Power(t2,2) - 32*Power(s2,3)*Power(t2,2) - 
         146*t1*Power(t2,2) - 21*s2*t1*Power(t2,2) + 
         59*Power(s2,2)*t1*Power(t2,2) + 4*Power(s2,3)*t1*Power(t2,2) + 
         23*Power(t1,2)*Power(t2,2) - 25*s2*Power(t1,2)*Power(t2,2) - 
         8*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         6*Power(t1,3)*Power(t2,2) + 4*s2*Power(t1,3)*Power(t2,2) + 
         6*Power(t2,3) + 49*s2*Power(t2,3) - 101*Power(s2,2)*Power(t2,3) + 
         8*Power(s2,3)*Power(t2,3) - 2*Power(s2,4)*Power(t2,3) - 
         26*t1*Power(t2,3) + 46*s2*t1*Power(t2,3) + 
         24*Power(s2,2)*t1*Power(t2,3) + 2*Power(s2,3)*t1*Power(t2,3) + 
         27*Power(t1,2)*Power(t2,3) - 27*s2*Power(t1,2)*Power(t2,3) - 
         4*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         6*Power(t1,3)*Power(t2,3) + 4*s2*Power(t1,3)*Power(t2,3) - 
         5*Power(t2,4) - 18*s2*Power(t2,4) - 30*Power(s2,2)*Power(t2,4) + 
         Power(s2,3)*Power(t2,4) + 16*t1*Power(t2,4) + 
         33*s2*t1*Power(t2,4) + 5*Power(s2,2)*t1*Power(t2,4) + 
         2*Power(t1,2)*Power(t2,4) - 4*s2*Power(t1,2)*Power(t2,4) + 
         Power(t2,5) + 2*s2*Power(t2,5) - 3*Power(s2,2)*Power(t2,5) - 
         2*t1*Power(t2,5) + 2*s2*t1*Power(t2,5) + 
         2*Power(s1,5)*(1 + s2*
             (5 - 4*Power(t1,2) + 4*t1*(-2 + t2) - 5*t2) + 
            Power(t1,2)*(7 - 3*t2) + 2*t1*(-1 + t2) - t2 + 
            Power(s2,2)*(-3 + 4*t1 + 3*t2)) + 
         Power(s,5)*(-1 + s1)*
          (-12 + (6 + s2)*t2 + 5*Power(t2,2) - t1*(-12 + s2 + 11*t2)) + 
         Power(s1,4)*(-6 + 2*Power(s2,4) - 2*Power(t1,3)*(-4 + t2) - 
            2*t2 + 8*Power(t2,2) + Power(s2,3)*(-4 - 4*t1 + 11*t2) + 
            Power(t1,2)*(-80 + 9*t2 + 14*Power(t2,2)) - 
            t1*(3 + 19*Power(t2,2)) + 
            Power(s2,2)*(17 + 8*Power(t1,2) - 33*t2 - 15*Power(t2,2) - 
               2*t1*(20 + 11*t2)) + 
            s2*(-13 - 6*Power(t1,3) + 35*Power(t2,2) + 
               Power(t1,2)*(30 + 19*t2) + t1*(91 + 20*t2 - 23*Power(t2,2))\
)) - Power(s1,3)*(27 - 41*t2 + 3*Power(t2,2) + 11*Power(t2,3) + 
            2*Power(s2,4)*(5 + t2) - 
            4*Power(t1,3)*(-6 - t2 + Power(t2,2)) + 
            Power(t1,2)*(-117 - 87*t2 + 60*Power(t2,2) + 
               10*Power(t2,3)) - 
            t1*(9 + 28*t2 + 9*Power(t2,2) + 28*Power(t2,3)) + 
            Power(s2,3)*(-28 + 31*t2 + 23*Power(t2,2) - 
               2*t1*(8 + 3*t2)) + 
            Power(s2,2)*(66 - 54*t2 - 109*Power(t2,2) - 15*Power(t2,3) + 
               2*Power(t1,2)*(11 + 6*t2) - 
               t1*(17 + 122*t2 + 11*Power(t2,2))) + 
            s2*(-61 + 68*t2 + 25*Power(t2,2) + 42*Power(t2,3) - 
               8*Power(t1,3)*(2 + t2) + 
               Power(t1,2)*(9 + 85*t2 + 6*Power(t2,2)) + 
               2*t1*(53 + 72*t2 + 8*Power(t2,2) - 10*Power(t2,3)))) + 
         Power(s1,2)*(22 + t2 - 32*Power(t2,2) + 3*Power(t2,3) + 
            6*Power(t2,4) - 2*Power(s2,4)*(-8 - 5*t2 + Power(t2,2)) - 
            2*Power(t1,3)*(-15 - 8*t2 + 6*Power(t2,2) + Power(t2,3)) + 
            t1*(20 - 136*t2 + 3*Power(t2,2) - 6*Power(t2,3) - 
               15*Power(t2,4)) + 
            Power(t1,2)*(-78 - 130*t2 + 5*Power(t2,2) + 45*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,3)*(-52 - 36*t2 + 55*Power(t2,2) + 13*Power(t2,3) - 
               2*t1*(18 + 7*t2)) + 
            Power(s2,2)*(157 + 13*t2 - 189*Power(t2,2) - 
               110*Power(t2,3) - 9*Power(t2,4) + 
               2*Power(t1,2)*(20 + 9*t2) + 
               t1*(15 - 38*t2 - 99*Power(t2,2) + 12*Power(t2,3))) + 
            s2*(-155 + 176*t2 + 76*Power(t2,2) + 18*Power(t2,3) + 
               19*Power(t2,4) + 
               2*Power(t1,3)*(-10 - 7*t2 + Power(t2,2)) + 
               Power(t1,2)*(4 + 40*t2 + 67*Power(t2,2) - 
                  13*Power(t2,3)) + 
               t1*(37 + 94*t2 + 126*Power(t2,2) + 40*Power(t2,3) - 
                  3*Power(t2,4)))) + 
         s1*(105 - 93*t2 - 12*Power(t2,2) + 2*Power(t2,3) - Power(t2,4) - 
            Power(t2,5) + 2*Power(s2,4)*
             (-4 - 8*t2 + Power(t2,2) + Power(t2,3)) + 
            2*Power(t1,3)*(-15 - 4*t2 + 7*Power(t2,2) + 4*Power(t2,3)) + 
            Power(t1,2)*(15 + 15*t2 - 38*Power(t2,2) - 14*Power(t2,3) - 
               8*Power(t2,4)) + 
            t1*(-90 + 182*t2 + 209*Power(t2,2) - 44*Power(t2,3) + 
               3*Power(t2,4) + 2*Power(t2,5)) - 
            Power(s2,3)*(-12 - 100*t2 + 21*Power(t2,3) + Power(t2,4) + 
               2*t1*(-20 - 8*t2 + 2*Power(t2,2) + Power(t2,3))) + 
            Power(s2,2)*(-58 - 122*t2 + 178*Power(t2,2) + 
               148*Power(t2,3) + 43*Power(t2,4) + 3*Power(t2,5) + 
               Power(t1,2)*(-58 + 8*Power(t2,2) + 4*Power(t2,3)) - 
               3*t1*(4 + 38*t2 + 9*Power(t2,2) - 4*Power(t2,3) + 
                  3*Power(t2,4))) - 
            s2*(27 - 27*t2 + 278*Power(t2,2) - 23*Power(t2,3) + 
               5*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,3)*(-26 + 6*Power(t2,2) + 4*Power(t2,3)) - 
               Power(t1,2)*(27 + 29*t2 + 20*Power(t2,2) - 
                  8*Power(t2,3) + 8*Power(t2,4)) + 
               2*t1*(-29 + 8*t2 + 33*Power(t2,2) + 53*Power(t2,3) + 
                  15*Power(t2,4) + Power(t2,5)))) + 
         Power(s,4)*(-34 + 18*t1 + Power(s2,2)*t1 - Power(t1,2) + 65*t2 - 
            54*t1*t2 + 6*Power(t1,2)*t2 - 31*Power(t2,2) + 
            27*t1*Power(t2,2) - 13*Power(t2,3) + 
            s2*(-9 - 5*Power(t1,2) + 13*t2 + 9*Power(t2,2) + 
               t1*(7 + t2)) - 
            Power(s1,2)*(-34 + Power(s2,2) + 50*t1 + 12*t2 - 39*t1*t2 + 
               18*Power(t2,2) + s2*(2 - 11*t1 + t2)) + 
            s1*(-24 - Power(s2,2)*(-1 + t1) + Power(t1,2)*(1 - 6*t2) - 
               41*t2 + 49*Power(t2,2) + 13*Power(t2,3) + 
               t1*(56 + 3*t2 - 27*Power(t2,2)) + 
               s2*(35 + 5*Power(t1,2) - 24*t2 - 9*Power(t2,2) + 
                  t1*(-42 + 11*t2)))) + 
         Power(s,3)*(74 - 28*t1 + 19*Power(t1,2) + 8*Power(t1,3) - 
            155*t2 + 61*t1*t2 - 29*Power(t1,2)*t2 - 2*Power(t1,3)*t2 + 
            91*Power(t2,2) - 97*t1*Power(t2,2) + 
            18*Power(t1,2)*Power(t2,2) - 42*Power(t2,3) + 
            19*t1*Power(t2,3) - 11*Power(t2,4) + 
            Power(s2,3)*(-4 - 2*t1 + 3*t2) + 
            Power(s2,2)*(11 + 8*Power(t1,2) + t1*(9 - 10*t2) - 15*t2 - 
               7*Power(t2,2)) + 
            Power(s1,3)*(-34 + 2*Power(s2,2) + t1*(78 - 51*t2) + 
               s2*(10 - 27*t1 - 7*t2) + 8*t2 + 21*Power(t2,2)) + 
            s2*(-20 - 6*Power(t1,3) - 35*t2 + 83*Power(t2,2) + 
               27*Power(t2,3) + Power(t1,2)*(-25 + 9*t2) + 
               t1*(20 + 39*t2 - 11*Power(t2,2))) - 
            Power(s1,2)*(20 + Power(s2,3) + Power(t1,2)*(17 - 24*t2) - 
               122*t2 + 85*Power(t2,2) + 32*Power(t2,3) + 
               2*Power(s2,2)*(-1 + 3*t1 + 7*t2) + 
               t1*(134 + 43*t2 - 61*Power(t2,2)) + 
               s2*(90 + 7*Power(t1,2) - 85*t2 - 33*Power(t2,2) + 
                  2*t1*(-75 + 14*t2))) + 
            s1*(56 + Power(s2,3)*(5 + 2*t1 - 3*t2) + 
               2*Power(t1,3)*(-4 + t2) - 79*t2 + Power(t2,2) + 
               74*Power(t2,3) + 11*Power(t2,4) + 
               Power(t1,2)*(22 - 7*t2 - 18*Power(t2,2)) + 
               Power(s2,2)*(-39 - 8*Power(t1,2) + t1*(21 - 2*t2) + 
                  41*t2 + 7*Power(t2,2)) + 
               t1*(-16 + 149*t2 + 8*Power(t2,2) - 19*Power(t2,3)) + 
               s2*(48 + 6*Power(t1,3) + 49*t2 - 144*Power(t2,2) - 
                  27*Power(t2,3) + Power(t1,2)*(8 + 3*t2) + 
                  t1*(-67 - 115*t2 + 39*Power(t2,2))))) - 
         Power(s,2)*(17 - 80*t1 - 11*Power(t1,2) - 16*Power(t1,3) + 
            2*Power(s2,4)*(-2 + t2) + 78*t2 + 71*t1*t2 - 
            6*Power(t1,2)*t2 - 6*Power(t1,3)*t2 + 150*Power(t2,2) - 
            99*t1*Power(t2,2) + 53*Power(t1,2)*Power(t2,2) + 
            4*Power(t1,3)*Power(t2,2) - 58*Power(t2,3) + 
            74*t1*Power(t2,3) - 18*Power(t1,2)*Power(t2,3) + 
            19*Power(t2,4) - t1*Power(t2,4) + 3*Power(t2,5) + 
            Power(s1,4)*(-14 + Power(s2,2) + t1*(54 - 29*t2) + 
               s2*(14 - 25*t1 - 13*t2) + 4*t2 + 8*Power(t2,2)) + 
            Power(s2,3)*(22 - 4*t2 - 7*Power(t2,2) + 2*t1*(4 + t2)) - 
            Power(s2,2)*(86 + 12*t2 - 68*Power(t2,2) - 17*Power(t2,3) + 
               2*Power(t1,2)*(7 + 6*t2) + t1*(2 + 13*t2 - 18*Power(t2,2))\
) - Power(s1,3)*(58 + 2*Power(s2,3) - 91*t2 + 53*Power(t2,2) + 
               19*Power(t2,3) - 9*Power(t1,2)*(-5 + 4*t2) + 
               Power(s2,2)*(-15 + 23*t1 + 32*t2) + 
               t1*(144 + 37*t2 - 41*Power(t2,2)) - 
               s2*(-93 + 9*Power(t1,2) + t1*(194 - 31*t2) + 104*t2 + 
                  47*Power(t2,2))) + 
            s2*(109 - 140*t2 - 34*Power(t2,2) - 117*Power(t2,3) - 
               23*Power(t2,4) + 2*Power(t1,3)*(5 + 4*t2) + 
               Power(t1,2)*(-1 + 38*t2 - 29*Power(t2,2)) + 
               t1*(96 - 12*t2 - 81*Power(t2,2) + 19*Power(t2,3))) + 
            Power(s1,2)*(30 - 2*Power(s2,4) + 
               Power(s2,3)*(16 + 8*t1 - 15*t2) + 
               6*Power(t1,3)*(-4 + t2) + 77*t2 - 79*Power(t2,2) + 
               79*Power(t2,3) + 14*Power(t2,4) + 
               Power(t1,2)*(121 - 5*t2 - 50*Power(t2,2)) + 
               t1*(107 + 122*t2 + 48*Power(t2,2) - 13*Power(t2,3)) + 
               Power(s2,2)*(-99 - 24*Power(t1,2) + 127*t2 + 
                  42*Power(t2,2) + t1*(87 + 20*t2)) + 
               s2*(96 + 18*Power(t1,3) + 61*t2 - 290*Power(t2,2) - 
                  57*Power(t2,3) - Power(t1,2)*(29 + 13*t2) + 
                  t1*(-210 - 265*t2 + 96*Power(t2,2)))) + 
            s1*(177 - 2*Power(s2,4)*(-3 + t2) - 382*t2 + 
               102*Power(t2,2) - 22*Power(t2,3) - 33*Power(t2,4) - 
               3*Power(t2,5) - 4*Power(t1,3)*(-10 + Power(t2,2)) + 
               Power(t1,2)*(11 - 129*t2 + 25*Power(t2,2) + 
                  18*Power(t2,3)) + 
               t1*(-165 + 109*t2 - 146*Power(t2,2) - 41*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,3)*(-36 + 19*t2 + 7*Power(t2,2) - 
                  2*t1*(8 + t2)) + 
               Power(s2,2)*(93 + 21*t2 - 138*Power(t2,2) - 
                  17*Power(t2,3) + 2*Power(t1,2)*(19 + 6*t2) + 
                  t1*(14 - 111*t2 + 10*Power(t2,2))) + 
               s2*(-202 + 16*t2 + 177*Power(t2,2) + 194*Power(t2,3) + 
                  23*Power(t2,4) - 4*Power(t1,3)*(7 + 2*t2) + 
                  Power(t1,2)*(-55 + 79*t2 + Power(t2,2)) + 
                  t1*(97 + 176*t2 + 113*Power(t2,2) - 39*Power(t2,3))))) + 
         s*(-97 + 2*t1 + 81*Power(t1,2) + 14*Power(t1,3) + 94*t2 - 
            106*t1*t2 + 2*Power(t1,2)*t2 + 10*Power(t1,3)*t2 - 
            162*Power(t2,2) - 53*t1*Power(t2,2) + 
            14*Power(t1,2)*Power(t2,2) - 8*Power(t1,3)*Power(t2,2) - 
            34*Power(t2,3) + 72*t1*Power(t2,3) - 
            23*Power(t1,2)*Power(t2,3) - 2*Power(t1,3)*Power(t2,3) + 
            21*Power(t2,4) - 21*t1*Power(t2,4) + 
            6*Power(t1,2)*Power(t2,4) - 2*Power(t2,5) - 2*t1*Power(t2,5) + 
            Power(s2,4)*(8 + 4*t2 - 4*Power(t2,2)) - 
            2*Power(s1,5)*(1 - t2 + t1*(-7 + 3*t2) + 
               s2*(-3 + 4*t1 + 3*t2)) + 
            Power(s2,3)*(-28 - 54*t2 + 16*Power(t2,2) + 5*Power(t2,3) + 
               2*t1*(-12 - 2*t2 + Power(t2,2))) - 
            Power(s1,4)*(Power(s2,3) + Power(t1,2)*(43 - 24*t2) + 
               6*Power(s2,2)*(-3 + 4*t1 + 4*t2) + 
               t1*(50 + 12*t2 - 7*Power(t2,2)) + 
               2*(14 - 9*t2 + 6*Power(t2,2)) - 
               s2*(-48 + 19*Power(t1,2) + t1*(101 - 22*t2) + 54*t2 + 
                  23*Power(t2,2))) + 
            Power(s2,2)*(-6 + 91*t2 - 100*Power(t2,2) - 83*Power(t2,3) - 
               13*Power(t2,4) + Power(t1,2)*(26 + 6*t2) + 
               t1*(108 + 29*t2 + 28*Power(t2,2) - 2*Power(t2,3))) + 
            s2*(99 - 77*t2 + 225*Power(t2,2) + 42*Power(t2,3) + 
               49*Power(t2,4) + 6*Power(t2,5) + 
               2*Power(t1,3)*(-5 - 3*t2 + Power(t2,2)) + 
               Power(t1,2)*(-91 + 8*t2 - 40*Power(t2,2) + 
                  11*Power(t2,3)) + 
               t1*(-82 - 45*t2 + 22*Power(t2,2) + 82*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s1,3)*(14 - 4*Power(s2,4) + 
               Power(s2,3)*(15 + 10*t1 - 23*t2) + 
               6*Power(t1,3)*(-4 + t2) + 85*t2 - 45*Power(t2,2) + 
               20*Power(t2,3) + 
               Power(t1,2)*(180 - 13*t2 - 46*Power(t2,2)) + 
               t1*(108 + 27*t2 + 32*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(-77 - 24*Power(t1,2) + 117*t2 + 
                  49*Power(t2,2) + t1*(105 + 44*t2)) + 
               s2*(70 + 18*Power(t1,3) + 7*t2 - 194*Power(t2,2) - 
                  34*Power(t2,3) - Power(t1,2)*(62 + 35*t2) + 
                  t1*(-241 - 171*t2 + 80*Power(t2,2)))) + 
            Power(s1,2)*(16*Power(s2,4) + 
               Power(t1,3)*(56 + 6*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(-125 - 187*t2 + 67*Power(t2,2) + 
                  28*Power(t2,3)) - 
               2*(-61 + 137*t2 + 3*Power(t2,2) - 18*Power(t2,3) + 
                  6*Power(t2,4)) - 
               t1*(138 + 70*t2 + 2*Power(t2,2) + 58*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(s2,3)*(-60 + 45*t2 + 29*Power(t2,2) - 
                  2*t1*(15 + 4*t2)) + 
               Power(s2,2)*(148 - 14*t2 - 251*Power(t2,2) - 
                  38*Power(t2,3) + 4*Power(t1,2)*(13 + 6*t2) - 
                  t1*(12 + 225*t2 + 2*Power(t2,2))) + 
               s2*(-235 + 97*t2 + 170*Power(t2,2) + 204*Power(t2,3) + 
                  23*Power(t2,4) - 2*Power(t1,3)*(19 + 8*t2) + 
                  Power(t1,2)*(-21 + 155*t2 + 7*Power(t2,2)) + 
                  t1*(175 + 295*t2 + 144*Power(t2,2) - 60*Power(t2,3)))) + 
            s1*(-37 + 91*t2 + 281*Power(t2,2) - 70*Power(t2,3) - 
               5*Power(t2,4) + 2*Power(t2,5) + 
               4*Power(s2,4)*(-5 - t2 + Power(t2,2)) + 
               2*Power(t1,3)*(-23 - 11*t2 + 8*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(59 + 42*t2 + 93*Power(t2,2) - 
                  25*Power(t2,3) - 6*Power(t2,4)) + 
               t1*(-60 + 283*t2 - 168*Power(t2,2) + 48*Power(t2,3) + 
                  26*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,3)*(74 + 32*t2 - 45*Power(t2,2) - 5*Power(t2,3) - 
                  2*t1*(-22 - 6*t2 + Power(t2,2))) + 
               Power(s2,2)*(-235 - 38*t2 + 174*Power(t2,2) + 
                  141*Power(t2,3) + 13*Power(t2,4) - 
                  6*Power(t1,2)*(9 + 5*t2) + 
                  t1*(-25 + 20*t2 + 102*Power(t2,2) - 18*Power(t2,3))) - 
               s2*(-288 + 223*t2 + 152*Power(t2,2) + 184*Power(t2,3) + 
                  76*Power(t2,4) + 6*Power(t2,5) + 
                  2*Power(t1,3)*(-15 - 11*t2 + Power(t2,2)) - 
                  Power(t1,2)*
                   (3 + 4*t2 - 95*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(-27 + 41*t2 + 190*Power(t2,2) + 70*Power(t2,3) - 
                     10*Power(t2,4)))))))/
     (Power(-1 + s1,2)*(-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + t2)*
       Power(s - s1 + t2,3)) + 
    (8*(-6 - 42*s2 + 74*Power(s2,2) - 34*Power(s2,3) + 8*Power(s2,4) + 
         35*t1 - 32*s2*t1 - 7*Power(s2,2)*t1 + 12*Power(s2,3)*t1 - 
         8*Power(s2,4)*t1 - 55*Power(t1,2) + 83*s2*Power(t1,2) - 
         50*Power(s2,2)*Power(t1,2) + 22*Power(s2,3)*Power(t1,2) + 
         29*Power(t1,3) - 12*s2*Power(t1,3) - 17*Power(s2,2)*Power(t1,3) - 
         3*Power(t1,4) + 3*s2*Power(t1,4) + 24*t2 + 89*s2*t2 - 
         202*Power(s2,2)*t2 + 70*Power(s2,3)*t2 - 14*Power(s2,4)*t2 - 
         226*t1*t2 + 385*s2*t1*t2 - 79*Power(s2,2)*t1*t2 - 
         15*Power(s2,3)*t1*t2 + 14*Power(s2,4)*t1*t2 - 24*Power(t1,2)*t2 - 
         136*s2*Power(t1,2)*t2 + 140*Power(s2,2)*Power(t1,2)*t2 - 
         39*Power(s2,3)*Power(t1,2)*t2 + 2*Power(t1,3)*t2 - 
         18*s2*Power(t1,3)*t2 + 29*Power(s2,2)*Power(t1,3)*t2 + 
         4*Power(t1,4)*t2 - 4*s2*Power(t1,4)*t2 - 31*Power(t2,2) + 
         218*s2*Power(t2,2) - 12*Power(s2,2)*Power(t2,2) - 
         54*Power(s2,3)*Power(t2,2) + 8*Power(s2,4)*Power(t2,2) + 
         15*t1*Power(t2,2) - 385*s2*t1*Power(t2,2) + 
         168*Power(s2,2)*t1*Power(t2,2) + 8*Power(s2,3)*t1*Power(t2,2) - 
         8*Power(s2,4)*t1*Power(t2,2) + 207*Power(t1,2)*Power(t2,2) - 
         9*s2*Power(t1,2)*Power(t2,2) - 
         121*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         22*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         54*Power(t1,3)*Power(t2,2) + 43*s2*Power(t1,3)*Power(t2,2) - 
         15*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         Power(t1,4)*Power(t2,2) + s2*Power(t1,4)*Power(t2,2) + 
         15*Power(t2,3) - 96*s2*Power(t2,3) - 47*Power(s2,2)*Power(t2,3) + 
         19*Power(s2,3)*Power(t2,3) - 2*Power(s2,4)*Power(t2,3) - 
         19*t1*Power(t2,3) + 204*s2*t1*Power(t2,3) - 
         46*Power(s2,2)*t1*Power(t2,3) - 6*Power(s2,3)*t1*Power(t2,3) + 
         2*Power(s2,4)*t1*Power(t2,3) - 99*Power(t1,2)*Power(t2,3) + 
         23*s2*Power(t1,2)*Power(t2,3) + 
         42*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         5*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         35*Power(t1,3)*Power(t2,3) - 23*s2*Power(t1,3)*Power(t2,3) + 
         3*Power(s2,2)*Power(t1,3)*Power(t2,3) - 3*Power(t2,4) + 
         2*s2*Power(t2,4) + 19*Power(s2,2)*Power(t2,4) - 
         Power(s2,3)*Power(t2,4) + 28*t1*Power(t2,4) - 
         39*s2*t1*Power(t2,4) - 4*Power(s2,2)*t1*Power(t2,4) + 
         Power(s2,3)*t1*Power(t2,4) + 3*Power(t1,2)*Power(t2,4) - 
         s2*Power(t1,2)*Power(t2,4) - 
         3*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         4*Power(t1,3)*Power(t2,4) + 2*s2*Power(t1,3)*Power(t2,4) + 
         Power(t2,5) - 3*s2*Power(t2,5) - t1*Power(t2,5) + 
         3*s2*t1*Power(t2,5) + 
         2*Power(s1,4)*(-1 + t1)*(-2 + s2 + t1)*(3 - 4*t2 + Power(t2,2)) - 
         Power(s,3)*(-1 + t1)*(-1 + t2)*
          (-16 + 15*t1 + 23*t2 - 6*t1*t2 - 6*Power(t2,2) + 
            2*s1*(8 + t1*(-3 + t2) + 5*s2*(-2 + t2) - 9*t2 + 
               2*Power(t2,2)) + 
            s2*(20 - 15*t2 + 2*Power(t2,2) + t1*(-9 + 4*t2))) + 
         Power(s1,3)*(-1 + t1)*
          (-2*Power(s2,2)*(13 - 6*t2 + Power(t2,2)) + 
            Power(t1,2)*(3 - 4*t2 + Power(t2,2)) + 
            t1*(-37 + 7*t2 + 19*Power(t2,2) - 5*Power(t2,3)) + 
            2*(20 - 9*t2 - 16*Power(t2,2) + 5*Power(t2,3)) + 
            s2*(49 - 51*t2 + 25*Power(t2,2) - 7*Power(t2,3) + 
               t1*(53 - 48*t2 + 11*Power(t2,2)))) + 
         Power(s1,2)*(67 - 6*t2 - 54*Power(t2,2) - 14*Power(t2,3) + 
            7*Power(t2,4) - Power(t1,3)*
             (26 + 10*t2 - 15*Power(t2,2) + 3*Power(t2,3)) + 
            t1*(-174 - 69*t2 + 116*Power(t2,2) + 33*Power(t2,3) - 
               10*Power(t2,4)) + 
            Power(t1,2)*(-87 + 221*t2 - 145*Power(t2,2) + 
               8*Power(t2,3) + 3*Power(t2,4)) - 
            Power(s2,3)*(-1 + t2)*(25 - 10*t2 + t1*(-9 + 2*t2)) + 
            Power(s2,2)*(-123 + 38*t2 - 26*Power(t2,2) + 7*Power(t2,3) + 
               Power(t1,2)*(-9 - 20*t2 + 5*Power(t2,2)) + 
               t1*(20 + 2*t2 - 27*Power(t2,2) + 5*Power(t2,3))) + 
            s2*(55 + 140*t2 - 107*Power(t2,2) + 24*Power(t2,3) - 
               8*Power(t2,4) + 3*Power(t1,3)*(12 - 5*t2 + Power(t2,2)) + 
               Power(t1,2)*(18 - 76*t2 + 109*Power(t2,2) - 
                  27*Power(t2,3)) + 
               t1*(207 - 181*t2 + 103*Power(t2,2) - 33*Power(t2,3) + 
                  8*Power(t2,4)))) + 
         s1*(-33 - 20*t2 + 49*Power(t2,2) + 9*Power(t2,3) - 
            4*Power(t2,4) - Power(t2,5) + 
            2*Power(s2,4)*(-1 + t1)*(3 - 4*t2 + Power(t2,2)) + 
            3*Power(t1,4)*(3 - 4*t2 + Power(t2,2)) + 
            Power(t1,3)*(-105 + 149*t2 - 13*Power(t2,2) - 
               17*Power(t2,3) + 2*Power(t2,4)) - 
            Power(t1,2)*(-292 + 381*t2 + Power(t2,2) - 81*Power(t2,3) + 
               7*Power(t2,4)) + 
            t1*(57 + 348*t2 - 106*Power(t2,2) - 29*Power(t2,3) - 
               15*Power(t2,4) + Power(t2,5)) + 
            Power(s2,3)*(-1 + t2)*
             (56 - 7*Power(t1,2)*(-3 + t2) + 31*t2 - 19*Power(t2,2) + 
               t1*(-61 - 16*t2 + 11*Power(t2,2))) + 
            Power(s2,2)*(63 + 174*t2 - 8*Power(t2,2) + 42*Power(t2,3) - 
               15*Power(t2,4) + 8*Power(t1,3)*(3 - 4*t2 + Power(t2,2)) + 
               Power(t1,2)*(-105 + 77*t2 + 71*Power(t2,2) - 
                  27*Power(t2,3)) + 
               t1*(130 - 127*t2 - 43*Power(t2,2) + 21*Power(t2,3) + 
                  3*Power(t2,4))) + 
            s2*(65 - 340*t2 - 52*Power(t2,2) + 65*Power(t2,3) + 
               3*Power(t2,4) + 3*Power(t2,5) - 
               3*Power(t1,4)*(3 - 4*t2 + Power(t2,2)) + 
               Power(t1,3)*(68 - 101*t2 + 12*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(-107 + 255*t2 - 162*Power(t2,2) + 
                  9*Power(t2,3) + 5*Power(t2,4)) - 
               t1*(333 + 10*t2 - 229*Power(t2,2) + 151*Power(t2,3) - 
                  28*Power(t2,4) + 3*Power(t2,5)))) + 
         Power(s,2)*(-93 - 131*t1 + 7*Power(t1,2) - 3*Power(t1,3) - 
            71*t2 + 156*t1*t2 + 48*Power(t1,2)*t2 + 3*Power(t1,3)*t2 - 
            38*Power(t2,2) + 37*t1*Power(t2,2) - 
            67*Power(t1,2)*Power(t2,2) + 34*Power(t2,3) - 
            30*t1*Power(t2,3) + 20*Power(t1,2)*Power(t2,3) + 
            Power(s2,2)*(-1 + t2)*
             (-4 + 14*t2 - 6*Power(t2,2) + Power(t1,2)*(-3 + 2*t2) + 
               t1*(23 - 24*t2 + 6*Power(t2,2))) + 
            2*Power(s1,2)*(-1 + t1)*
             (-1 + 18*t2 - 10*Power(t2,2) + Power(t2,3) + 
               3*t1*(3 - 4*t2 + Power(t2,2)) + 
               s2*(3 - 17*t2 + 6*Power(t2,2))) + 
            s2*(49 + 134*t2 - Power(t1,3)*(-1 + t2)*t2 - 3*Power(t2,2) - 
               11*Power(t2,3) - Power(t2,4) + 
               Power(t1,2)*(-24 + t2 + 25*Power(t2,2) - 
                  10*Power(t2,3)) + 
               t1*(71 - 132*t2 + 19*Power(t2,2) + 9*Power(t2,3) + 
                  Power(t2,4))) + 
            s1*(62 + 44*t2 + 2*Power(t2,2) - 21*Power(t2,3) + 
               Power(t2,4) + Power(t1,3)*(3 - 4*t2 + Power(t2,2)) + 
               Power(t1,2)*(-2 + 8*t2 + 28*Power(t2,2) - 
                  10*Power(t2,3)) + 
               t1*(157 - 184*t2 + 37*Power(t2,2) + 7*Power(t2,3) - 
                  Power(t2,4)) + 
               Power(s2,2)*(-1 + t2)*(7 - 4*t2 + t1*(-23 + 12*t2)) + 
               s2*(-11 - 98*t2 + 15*Power(t2,2) + 6*Power(t2,3) + 
                  Power(t1,2)*(-2 - 28*t2 + 6*Power(t2,2)) + 
                  t1*(-83 + 122*t2 - 61*Power(t2,2) + 6*Power(t2,3))))) - 
         s*(75 + 47*t1 + 104*Power(t1,2) - 15*Power(t1,3) + 
            9*Power(t1,4) + 42*t2 + 308*t1*t2 - 263*Power(t1,2)*t2 + 
            9*Power(t1,3)*t2 - 12*Power(t1,4)*t2 + 35*Power(t2,2) - 
            226*t1*Power(t2,2) + 117*Power(t1,2)*Power(t2,2) + 
            3*Power(t1,3)*Power(t2,2) + 3*Power(t1,4)*Power(t2,2) + 
            39*Power(t2,3) + 3*t1*Power(t2,3) + 
            7*Power(t1,2)*Power(t2,3) - 5*Power(t1,3)*Power(t2,3) - 
            20*Power(t2,4) + t1*Power(t2,4) - 5*Power(t1,2)*Power(t2,4) - 
            3*Power(t2,5) + 3*t1*Power(t2,5) + 
            2*Power(s1,3)*(-1 + t1)*
             (1 + 9*t2 - Power(t2,2) - Power(t2,3) + 
               3*t1*(3 - 4*t2 + Power(t2,2)) + 
               2*s2*(-2 - 3*t2 + Power(t2,2))) + 
            Power(s2,3)*(-1 + t2)*
             (8 - 2*Power(t1,2)*(-3 + t2) + 5*t2 - 6*Power(t2,2) + 
               t1*(2 - 11*t2 + 6*Power(t2,2))) + 
            Power(s2,2)*(39 + 157*t2 - 28*Power(t2,2) + 2*Power(t2,3) - 
               2*Power(t2,4) + 5*Power(t1,3)*(3 - 4*t2 + Power(t2,2)) - 
               Power(t1,2)*(24 + 4*t2 - 35*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(82 - 137*t2 + 12*Power(t2,2) + 9*Power(t2,3) + 
                  2*Power(t2,4))) - 
            s2*(73 + 291*t2 - 44*Power(t2,2) + 35*Power(t2,3) - 
               19*Power(t2,4) + 3*Power(t1,4)*(3 - 4*t2 + Power(t2,2)) + 
               Power(t1,3)*(13 - 40*t2 + 27*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,2)*(15 - 174*t2 + 149*Power(t2,2) - 
                  41*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(206 - 101*t2 - 23*Power(t2,2) + 18*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s1,2)*(50 + 43*t2 + 27*Power(t2,2) - 11*Power(t2,3) - 
               5*Power(t2,4) + 2*Power(t1,3)*(3 - 4*t2 + Power(t2,2)) - 
               Power(t1,2)*(27 + 2*t2 - 52*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(191 - 169*t2 - 13*Power(t2,2) + 2*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,2)*(25 - 9*t2 + 
                  t1*(-9 - 15*t2 + 8*Power(t2,2))) + 
               s2*(-98 - 22*t2 - 7*Power(t2,2) + 7*Power(t2,3) + 
                  Power(t1,2)*(42 - 63*t2 + 13*Power(t2,2)) + 
                  t1*(-40 + 81*t2 - 46*Power(t2,2) + 5*Power(t2,3)))) + 
            s1*(-100 - 119*t2 - 32*Power(t2,2) - 30*Power(t2,3) + 
               22*Power(t2,4) + 3*Power(t2,5) - 
               Power(t1,3)*(29 + 7*t2 - 15*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(-2 + 115*t2 - 165*Power(t2,2) + 
                  12*Power(t2,3) + 8*Power(t2,4)) + 
               t1*(-309 + 63*t2 + 182*Power(t2,2) + Power(t2,3) - 
                  6*Power(t2,4) - 3*Power(t2,5)) + 
               Power(s2,3)*(-1 + t2)*(-7 + 4*t2 + t1*(-9 + 4*t2)) + 
               Power(s2,2)*(-97 - 23*t2 + 41*Power(t2,2) - 
                  9*Power(t2,3) + 
                  Power(t1,2)*(-12 - 17*t2 + 5*Power(t2,2)) + 
                  t1*(-19 + 68*t2 - 78*Power(t2,2) + 13*Power(t2,3))) + 
               s2*(174 + 236*t2 - 93*Power(t2,2) + 41*Power(t2,3) - 
                  14*Power(t2,4) + 
                  2*Power(t1,3)*(18 - 7*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (-78 + 76*t2 + 88*Power(t2,2) - 30*Power(t2,3)) + 
                  t1*(280 - 330*t2 + 155*Power(t2,2) - 19*Power(t2,3) + 
                     2*Power(t2,4)))))))/
     ((-1 + s1)*(-1 + s2)*(-s + s2 - t1)*Power(-1 + t1,2)*(-s + s1 - t2)*
       Power(-1 + t2,3)) - (8*(-77 + 125*s2 - 52*Power(s2,2) + 
         4*Power(s2,3) - 28*t1 - 12*s2*t1 + 44*Power(s2,2)*t1 - 
         4*Power(s2,3)*t1 + 107*Power(t1,2) - 115*s2*Power(t1,2) + 
         8*Power(s2,2)*Power(t1,2) - 2*Power(t1,3) + 2*s2*Power(t1,3) + 
         130*t2 - 198*s2*t2 + 48*Power(s2,2)*t2 - 16*Power(s2,3)*t2 - 
         186*t1*t2 + 182*s2*t1*t2 - 30*Power(s2,2)*t1*t2 + 
         16*Power(s2,3)*t1*t2 - 48*Power(t1,2)*t2 + 
         152*s2*Power(t1,2)*t2 - 44*Power(s2,2)*Power(t1,2)*t2 - 
         32*Power(t1,3)*t2 + 26*s2*Power(t1,3)*t2 - 19*Power(t2,2) + 
         428*s2*Power(t2,2) - 232*Power(s2,2)*Power(t2,2) + 
         23*Power(s2,3)*Power(t2,2) - 12*t1*Power(t2,2) - 
         211*s2*t1*Power(t2,2) + 115*Power(s2,2)*t1*Power(t2,2) - 
         18*Power(s2,3)*t1*Power(t2,2) + 44*Power(t1,2)*Power(t2,2) - 
         154*s2*Power(t1,2)*Power(t2,2) + 
         28*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         24*Power(t1,3)*Power(t2,2) - 16*s2*Power(t1,3)*Power(t2,2) - 
         43*Power(t2,3) - 226*s2*Power(t2,3) + 
         70*Power(s2,2)*Power(t2,3) - 13*Power(s2,3)*Power(t2,3) + 
         70*t1*Power(t2,3) + 158*s2*t1*Power(t2,3) - 
         44*Power(s2,2)*t1*Power(t2,3) + 6*Power(s2,3)*t1*Power(t2,3) - 
         17*Power(t1,2)*Power(t2,3) + 49*s2*Power(t1,2)*Power(t2,3) - 
         8*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         6*Power(t1,3)*Power(t2,3) + 4*s2*Power(t1,3)*Power(t2,3) + 
         8*Power(t2,4) + 41*s2*Power(t2,4) - 3*Power(s2,2)*Power(t2,4) + 
         2*Power(s2,3)*Power(t2,4) - 10*t1*Power(t2,4) - 
         39*s2*t1*Power(t2,4) + 3*Power(s2,2)*t1*Power(t2,4) + 
         2*Power(t1,2)*Power(t2,4) - 4*s2*Power(t1,2)*Power(t2,4) + 
         Power(t2,5) - 2*s2*Power(t2,5) + Power(s2,2)*Power(t2,5) - 
         2*t1*Power(t2,5) + 2*s2*t1*Power(t2,5) + 
         2*Power(s,4)*(-1 + s1)*(-1 + t2)*
          (4 + t1*(-3 + t2) - 3*t2 + Power(t2,2)) + 
         Power(s1,4)*(-1 + t2)*
          (12 + Power(s2,2)*(-3 + t2) - 4*Power(t1,2)*(-3 + t2) + 
            s2*(-11 + 9*t1)*(-3 + t2) - 8*t2 + t1*(-27 + 13*t2)) + 
         Power(s,3)*(-1 + t2)*
          (6 - 8*s2 - 79*t1 + 7*s2*t1 + 18*Power(t1,2) - 23*t2 - 
            5*s2*t2 + 17*t1*t2 - 4*s2*t1*t2 - 6*Power(t1,2)*t2 + 
            3*Power(t2,2) + 6*s2*Power(t2,2) + t1*Power(t2,2) - 
            Power(t2,3) - Power(s1,2)*
             (4 - 2*s2*(-3 + t2) + 3*t1*(-3 + t2) + t2 + Power(t2,2)) + 
            s1*(14 + 6*Power(t1,2)*(-3 + t2) + 16*t2 - 2*Power(t2,2) + 
               Power(t2,3) + s2*
                (-2 + t1*(9 - 4*t2) + 11*t2 - 6*Power(t2,2)) - 
               t1*(-54 + 6*t2 + Power(t2,2)))) + 
         Power(s1,3)*(1 - 3*t2 - 9*Power(t2,2) + 11*Power(t2,3) + 
            2*Power(t1,3)*(3 - 4*t2 + Power(t2,2)) + 
            Power(s2,3)*(9 - 13*t2 + 4*Power(t2,2)) + 
            t1*(-109 + 106*t2 + 11*Power(t2,2) - 16*Power(t2,3)) + 
            Power(t1,2)*(46 - 87*t2 + 15*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s2,2)*(-71 + 102*t2 - 44*Power(t2,2) + 5*Power(t2,3) + 
               t1*(-3 - 28*t2 + 7*Power(t2,2))) + 
            s2*(89 - 56*t2 - 39*Power(t2,2) + 14*Power(t2,3) - 
               3*Power(t1,2)*(-6 - 3*t2 + Power(t2,2)) - 
               2*t1*(-7 + 11*t2 - 28*Power(t2,2) + 8*Power(t2,3)))) + 
         Power(s1,2)*(2 + 94*t2 - 96*Power(t2,2) + 2*Power(t2,3) - 
            2*Power(t2,4) - 2*Power(t1,3)*
             (26 - 22*t2 + 3*Power(t2,2) + Power(t2,3)) + 
            t1*(-14 - 48*t2 + 35*Power(t2,2) + 2*Power(t2,3) + 
               Power(t2,4)) + 
            Power(t1,2)*(-19 - 4*t2 + 44*Power(t2,2) - 15*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,3)*(-1 + t2)*
             (35 - 12*t2 - 2*Power(t2,2) + 2*t1*(-13 + 6*t2)) + 
            Power(s2,2)*(120 - 194*t2 + 28*Power(t2,2) + 
               27*Power(t2,3) - 5*Power(t2,4) - 
               2*Power(t1,2)*(27 - 28*t2 + 9*Power(t2,2)) + 
               t1*(-21 + 50*t2 - 29*Power(t2,2) + 8*Power(t2,3))) + 
            s2*(-109 + 177*t2 - 76*Power(t2,2) + 37*Power(t2,3) - 
               5*Power(t2,4) + 
               2*Power(t1,3)*(20 - 17*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(81 - 124*t2 + 64*Power(t2,2) - 
                  13*Power(t2,3)) + 
               t1*(35 - 26*t2 + 42*Power(t2,2) - 44*Power(t2,3) + 
                  9*Power(t2,4)))) - 
         s1*(-86 + 241*t2 - 132*Power(t2,2) - 30*Power(t2,3) + 
            6*Power(t2,4) + Power(t2,5) + 
            4*Power(t1,3)*(-12 + t2 + 5*Power(t2,2) - 2*Power(t2,3)) + 
            Power(t1,2)*(30 + 85*t2 - 37*Power(t2,2) - 14*Power(t2,3) + 
               8*Power(t2,4)) - 
            t1*(32 + 376*t2 - 183*Power(t2,2) - 40*Power(t2,3) + 
               13*Power(t2,4) + 2*Power(t2,5)) + 
            Power(s2,3)*(-1 + t2)*
             (22 + 4*t2 - 13*Power(t2,2) + 2*Power(t2,3) + 
               t1*(-22 + 6*Power(t2,2))) + 
            Power(s2,2)*(92 - 256*t2 - 111*Power(t2,2) + 
               86*Power(t2,3) - 12*Power(t2,4) + Power(t2,5) - 
               2*Power(t1,2)*
                (23 - 6*t2 - 5*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(-72 + 200*t2 - 43*Power(t2,2) - 20*Power(t2,3) + 
                  7*Power(t2,4))) + 
            s2*(-20 + 175*t2 + 166*Power(t2,2) - 159*Power(t2,3) + 
               40*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,3)*(42 - 8*t2 - 6*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(76 - 171*t2 + 43*Power(t2,2) + 
                  20*Power(t2,3) - 8*Power(t2,4)) + 
               2*t1*(32 + 49*t2 - 52*Power(t2,2) + 49*Power(t2,3) - 
                  15*Power(t2,4) + Power(t2,5)))) + 
         Power(s,2)*(9 - 79*t1 - 7*Power(t1,2) - 6*Power(t1,3) - 271*t2 + 
            329*t1*t2 - 13*Power(t1,2)*t2 + 8*Power(t1,3)*t2 + 
            134*Power(t2,2) - 193*t1*Power(t2,2) + 
            4*Power(t1,2)*Power(t2,2) - 2*Power(t1,3)*Power(t2,2) - 
            39*Power(t2,3) + 30*t1*Power(t2,3) - 2*Power(t2,4) + 
            t1*Power(t2,4) + Power(t2,5) - 
            Power(s1,3)*(-1 + t2)*
             (4 + s2*(-3 + t2) - 7*t2 + Power(t2,2)) - 
            Power(s2,2)*(-1 + t2)*
             (4 - 14*t2 + 6*Power(t2,2) + t1*(-7 + 4*t2)) + 
            s2*(7 + 234*t2 - 80*Power(t2,2) + 3*Power(t2,3) + 
               4*Power(t2,4) + 
               Power(t1,2)*(39 - 31*t2 + 8*Power(t2,2)) - 
               t1*(70 + 77*t2 - 76*Power(t2,2) + 17*Power(t2,3))) + 
            Power(s1,2)*(55 - 44*t2 + 8*Power(t2,2) - 13*Power(t2,3) + 
               2*Power(t2,4) - 16*Power(t1,2)*(3 - 4*t2 + Power(t2,2)) + 
               Power(s2,2)*(3 - 5*t2 + 2*Power(t2,2)) + 
               t1*(90 - 53*t2 - 20*Power(t2,2) + 7*Power(t2,3)) + 
               s2*(-54 + 93*t2 - 60*Power(t2,2) + 13*Power(t2,3) + 
                  t1*(9 - 46*t2 + 13*Power(t2,2)))) + 
            s1*(-100 + 194*t2 - 110*Power(t2,2) + 49*Power(t2,3) - 
               Power(t2,5) + 2*Power(t1,3)*(3 - 4*t2 + Power(t2,2)) + 
               Power(t1,2)*(71 - 75*t2 + 20*Power(t2,2)) - 
               t1*(-5 + 120*t2 - 165*Power(t2,2) + 33*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(-1 + t2)*
                (23 - 24*t2 + 6*Power(t2,2) + t1*(-23 + 12*t2)) - 
               s2*(-98 + 223*t2 - 109*Power(t2,2) + 12*Power(t2,3) + 
                  4*Power(t2,4) + 
                  Power(t1,2)*(55 - 55*t2 + 16*Power(t2,2)) + 
                  t1*(-29 + 9*t2 + 49*Power(t2,2) - 13*Power(t2,3))))) + 
         s*(-92 - 66*t1 + 68*Power(t1,2) - 46*Power(t1,3) + 151*t2 - 
            276*t1*t2 + 43*Power(t1,2)*t2 + 36*Power(t1,3)*t2 - 
            315*Power(t2,2) + 361*t1*Power(t2,2) - 
            40*Power(t1,2)*Power(t2,2) - 4*Power(t1,3)*Power(t2,2) + 
            105*Power(t2,3) - 110*t1*Power(t2,3) - 
            5*Power(t1,2)*Power(t2,3) - 2*Power(t1,3)*Power(t2,3) - 
            19*Power(t2,4) + 13*t1*Power(t2,4) + 
            6*Power(t1,2)*Power(t2,4) + 2*Power(t2,5) - 2*t1*Power(t2,5) - 
            Power(s1,4)*(s2 - t1)*(3 - 4*t2 + Power(t2,2)) + 
            Power(s2,3)*(-1 + t2)*
             (20 + 10*t1*(-2 + t2) - 15*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*(3 - 255*t2 + 83*Power(t2,2) + 6*Power(t2,3) - 
               5*Power(t2,4) - 
               2*Power(t1,2)*(27 - 28*t2 + 9*Power(t2,2)) + 
               t1*(25 + 107*t2 - 54*Power(t2,2) + 10*Power(t2,3))) + 
            s2*(73 + 289*t2 - 20*Power(t2,2) - 9*Power(t2,3) + 
               5*Power(t2,4) - 2*Power(t2,5) + 
               2*Power(t1,3)*(20 - 17*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(46 - 149*t2 + 56*Power(t2,2) - 
                  9*Power(t2,3)) + 
               t1*(3 - 83*t2 + 64*Power(t2,2) + 12*Power(t2,3) - 
                  4*Power(t2,4))) - 
            Power(s1,3)*(37 - 28*t2 + Power(t2,2) - 2*Power(t2,3) - 
               14*Power(t1,2)*(3 - 4*t2 + Power(t2,2)) + 
               3*Power(s2,2)*(2 - 3*t2 + Power(t2,2)) + 
               t1*(57 - 25*t2 - 14*Power(t2,2) + 6*Power(t2,3)) + 
               s2*(-85 + 115*t2 - 42*Power(t2,2) + 4*Power(t2,3) + 
                  3*t1*(9 - 23*t2 + 6*Power(t2,2)))) - 
            Power(s1,2)*(-115 + 176*t2 - 65*Power(t2,2) + 
               28*Power(t2,3) + 4*Power(t1,3)*(3 - 4*t2 + Power(t2,2)) + 
               Power(s2,3)*(9 - 13*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(99 - 138*t2 + 29*Power(t2,2) + 
                  2*Power(t2,3)) - 
               t1*(15 + 66*t2 - 80*Power(t2,2) + 12*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,2)*(-91 + 145*t2 - 72*Power(t2,2) + 
                  10*Power(t2,3) + t1*(17 - 59*t2 + 18*Power(t2,2))) + 
               s2*(202 - 280*t2 + 51*Power(t2,2) + 14*Power(t2,3) - 
                  3*Power(t2,4) + 
                  Power(t1,2)*(-37 + 64*t2 - 19*Power(t2,2)) + 
                  t1*(29 + 31*t2 - 22*Power(t2,2) + 2*Power(t2,3)))) + 
            s1*(-78 + 205*t2 + 115*Power(t2,2) - 63*Power(t2,3) + 
               23*Power(t2,4) - 2*Power(t2,5) - 
               Power(s2,3)*(-1 + t2)*
                (29 + 10*t1*(-2 + t2) - 19*t2 + 2*Power(t2,2)) + 
               2*Power(t1,3)*(29 - 26*t2 + 4*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(21 + 7*t2 + 15*Power(t2,2) + 11*Power(t2,3) - 
                  6*Power(t2,4)) + 
               t1*(165 - 151*t2 - 120*Power(t2,2) + 84*Power(t2,3) - 
                  20*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,2)*(-120 + 259*t2 - 112*Power(t2,2) + 
                  5*Power(t2,4) + 
                  2*Power(t1,2)*(27 - 28*t2 + 9*Power(t2,2)) + 
                  t1*(24 - 34*t2 + 32*Power(t2,2) - 6*Power(t2,3))) + 
               s2*(171 - 534*t2 + 126*Power(t2,2) + 15*Power(t2,3) - 
                  12*Power(t2,4) + 2*Power(t2,5) - 
                  2*Power(t1,3)*(20 - 17*t2 + 5*Power(t2,2)) + 
                  Power(t1,2)*
                   (-115 + 81*t2 - 35*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(-39 + 253*t2 - 204*Power(t2,2) + 6*Power(t2,3) + 
                     8*Power(t2,4)))))))/
     (Power(-1 + s1,2)*(-1 + s2)*(-s + s2 - t1)*(-1 + t1)*Power(-1 + t2,3)*
       (s - s1 + t2)) + (8*(128 - 200*s2 + 68*Power(s2,2) - 
         20*Power(s2,3) + 24*Power(s2,4) - 56*t1 + 136*s2*t1 - 
         4*Power(s2,2)*t1 - 52*Power(s2,3)*t1 - 24*Power(s2,4)*t1 - 
         76*Power(t1,2) - 4*s2*Power(t1,2) + 8*Power(s2,2)*Power(t1,2) + 
         72*Power(s2,3)*Power(t1,2) + 28*Power(t1,3) + 44*s2*Power(t1,3) - 
         72*Power(s2,2)*Power(t1,3) - 24*Power(t1,4) + 24*s2*Power(t1,4) - 
         80*t2 + 293*s2*t2 - 253*Power(s2,2)*t2 + 122*Power(s2,3)*t2 - 
         40*Power(s2,4)*t2 + 12*Power(s2,5)*t2 - 21*t1*t2 + 28*s2*t1*t2 - 
         53*Power(s2,2)*t1*t2 + 22*Power(s2,3)*t1*t2 - 
         5*Power(s2,4)*t1*t2 + 49*Power(t1,2)*t2 - 119*s2*Power(t1,2)*t2 + 
         69*Power(s2,2)*Power(t1,2)*t2 - 43*Power(s2,3)*Power(t1,2)*t2 - 
         Power(s2,4)*Power(t1,2)*t2 + 34*Power(t1,3)*t2 - 
         68*s2*Power(t1,3)*t2 + 58*Power(s2,2)*Power(t1,3)*t2 + 
         Power(s2,3)*Power(t1,3)*t2 + 17*Power(t1,4)*t2 - 
         23*s2*Power(t1,4)*t2 + Power(s2,2)*Power(t1,4)*t2 + 
         Power(t1,5)*t2 - s2*Power(t1,5)*t2 - 56*Power(t2,2) - 
         98*s2*Power(t2,2) + 33*Power(s2,2)*Power(t2,2) - 
         16*Power(s2,3)*Power(t2,2) + 42*Power(s2,4)*Power(t2,2) - 
         12*Power(s2,5)*Power(t2,2) + 66*t1*Power(t2,2) + 
         42*s2*t1*Power(t2,2) + 34*Power(s2,2)*t1*Power(t2,2) - 
         102*Power(s2,3)*t1*Power(t2,2) + 37*Power(s2,4)*t1*Power(t2,2) + 
         Power(s2,5)*t1*Power(t2,2) - 3*Power(t1,2)*Power(t2,2) + 
         28*s2*Power(t1,2)*Power(t2,2) + 
         61*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         33*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         5*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         30*Power(t1,3)*Power(t2,2) - 10*s2*Power(t1,3)*Power(t2,2) + 
         7*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         7*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         9*Power(t1,4)*Power(t2,2) + s2*Power(t1,4)*Power(t2,2) - 
         3*Power(s2,2)*Power(t1,4)*Power(t2,2) + 8*Power(t2,3) + 
         7*s2*Power(t2,3) + 55*Power(s2,2)*Power(t2,3) - 
         10*Power(s2,3)*Power(t2,3) - 9*Power(s2,4)*Power(t2,3) + 
         9*t1*Power(t2,3) - 102*s2*t1*Power(t2,3) + 
         12*Power(s2,2)*t1*Power(t2,3) + 33*Power(s2,3)*t1*Power(t2,3) - 
         Power(s2,4)*t1*Power(t2,3) + 23*Power(t1,2)*Power(t2,3) + 
         17*s2*Power(t1,2)*Power(t2,3) - 
         45*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         4*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         19*Power(t1,3)*Power(t2,3) + 25*s2*Power(t1,3)*Power(t2,3) - 
         5*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         4*Power(t1,4)*Power(t2,3) + 2*s2*Power(t1,4)*Power(t2,3) - 
         2*s2*Power(t2,4) + 7*Power(s2,2)*Power(t2,4) - 
         3*Power(s2,3)*Power(t2,4) + 2*t1*Power(t2,4) - 
         8*s2*t1*Power(t2,4) + 6*Power(s2,2)*t1*Power(t2,4) + 
         Power(t1,2)*Power(t2,4) - 3*s2*Power(t1,2)*Power(t2,4) + 
         Power(s1,4)*(s2 - t1)*
          (-4 + 5*Power(s2,3) + 
            s2*(24 + 11*Power(t1,2) - 2*t1*(-24 + t2) - 2*t2) + 
            2*t1*(-16 + t2) + Power(t1,2)*(-12 + t2) + 
            Power(s2,2)*(-24 - 16*t1 + t2)) + 
         2*Power(s,5)*(-2*(-3 + s1)*t2 + t1*(7 - 7*s1 - 8*t2 + 4*s1*t2) + 
            s2*(1 - 3*t1 - 7*t2 + 4*t1*t2 + s1*(-1 + 3*t1 + 3*t2))) + 
         Power(s,4)*(20 + 5*t1 + 28*Power(t1,2) + 3*Power(t1,3) + 47*t2 + 
            43*t1*t2 - 45*Power(t1,2)*t2 + 11*Power(t2,2) - 
            29*t1*Power(t2,2) + 3*Power(t2,3) + 
            Power(s2,2)*(-14 + Power(t1,2) + t1*(14 - 25*t2) + 54*t2) + 
            Power(s1,2)*(12 + 8*Power(s2,2) + t1*(40 - 17*t2) + 4*t2 + 
               Power(t2,2) - s2*(18 + 23*t1 + 7*t2)) - 
            s1*(12 + Power(t1,2)*(19 - 17*t2) + 47*t2 + 3*Power(t2,2) + 
               3*Power(t2,3) + Power(s2,2)*(-4 + 13*t1 + 29*t2) + 
               t1*(77 + 2*t2 - 20*Power(t2,2)) - 
               2*s2*(5 + 36*t1 + Power(t1,2) + 42*t2 - 2*Power(t2,2))) - 
            s2*(10 + 3*Power(t1,3) + 106*t2 + 5*Power(t2,2) - 
               4*Power(t1,2)*(-3 + 7*t2) - t1*(-18 + t2 + 9*Power(t2,2)))) \
+ Power(s1,3)*(8 + 5*Power(s2,5) - 8*t2 + Power(t1,4)*(9 + t2) - 
            Power(s2,4)*(26 + 15*t1 + 10*t2) + 
            2*t1*(27 - 22*t2 + Power(t2,2)) + 
            Power(t1,3)*(8 - 21*t2 + 4*Power(t2,2)) + 
            Power(t1,2)*(-3 - 86*t2 + 5*Power(t2,2)) + 
            Power(s2,3)*(60 + 5*Power(t1,2) + 58*t2 - 3*Power(t2,2) + 
               t1*(60 + 46*t2)) + 
            Power(s2,2)*(-17 + 15*Power(t1,3) - 58*t2 + 3*Power(t2,2) - 
               Power(t1,2)*(13 + 61*t2) + 
               t1*(-92 - 177*t2 + 10*Power(t2,2))) - 
            s2*(10*Power(t1,4) - 6*Power(t1,3)*(-5 + 4*t2) + 
               2*(19 - 14*t2 + Power(t2,2)) + 
               4*t1*(1 - 42*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-24 - 140*t2 + 11*Power(t2,2)))) + 
         Power(s1,2)*(3*Power(t1,5) + Power(s2,5)*(6 + t1 - 16*t2) + 
            Power(t1,4)*(17 - 12*t2 - 3*Power(t2,2)) + 
            8*(5 - 7*t2 + 2*Power(t2,2)) + 
            t1*(-181 + 36*t2 + 45*Power(t2,2)) + 
            Power(t1,2)*(-44 + 67*t2 + 67*Power(t2,2) - 2*Power(t2,3)) - 
            Power(t1,3)*(-41 + 12*t2 + 6*Power(t2,2) + 3*Power(t2,3)) + 
            Power(s2,4)*(1 - 4*Power(t1,2) + 68*t2 - 5*Power(t2,2) + 
               t1*(-7 + 52*t2)) + 
            Power(s2,3)*(31 + 2*Power(t1,3) - 90*t2 - 34*Power(t2,2) - 
               Power(t2,3) - Power(t1,2)*(15 + 37*t2) + 
               t1*(4 - 202*t2 + 4*Power(t2,2))) + 
            Power(s2,2)*(-198 + 4*Power(t1,4) + 
               Power(t1,3)*(38 - 18*t2) + 53*t2 + 79*Power(t2,2) + 
               6*Power(t2,3) + 
               2*Power(t1,2)*(14 + 69*t2 + 2*Power(t2,2)) - 
               t1*(66 - 244*t2 - 99*Power(t2,2) + Power(t2,3))) - 
            s2*(-181 + 3*Power(t1,5) + Power(t1,4)*(25 - 19*t2) + 
               Power(t1,3)*(50 - 8*t2) + 68*t2 + 13*Power(t2,2) + 
               Power(t1,2)*(22 + 126*t2 + 59*Power(t2,2) - 
                  5*Power(t2,3)) + 
               2*t1*(-109 + 24*t2 + 97*Power(t2,2) + 2*Power(t2,3)))) - 
         s1*(Power(t1,5)*(1 + 3*t2) + 
            Power(s2,5)*(12 + 2*(-3 + t1)*t2 - 11*Power(t2,2)) + 
            Power(t1,3)*(70 + 11*t2 - 31*Power(t2,2) - 15*Power(t2,3)) + 
            8*(22 - 18*t2 - 5*Power(t2,2) + Power(t2,3)) - 
            Power(t1,4)*(19 - 38*t2 + 7*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,2)*(25 - 99*t2 + 87*Power(t2,2) + 14*Power(t2,3) + 
               Power(t2,4)) + 
            t1*(-253 + 53*t2 + 107*Power(t2,2) + 7*Power(t2,3) + 
               2*Power(t2,4)) - 
            Power(s2,4)*(4 - 31*t2 - 33*Power(t2,2) + 10*Power(t2,3) + 
               Power(t1,2)*(1 + 9*t2) + t1*(41 - 42*t2 - 36*Power(t2,2))) \
+ Power(s2,3)*(86 + 39*t2 - 48*Power(t2,2) - 3*Power(t2,3) - 
               3*Power(t2,4) + Power(t1,3)*(1 + 9*t2) + 
               Power(t1,2)*(65 - 84*t2 - 28*Power(t2,2)) + 
               t1*(-50 - 86*t2 - 101*Power(t2,2) + 29*Power(t2,3))) - 
            s2*(-13 - 171*t2 + 107*Power(t2,2) - 9*Power(t2,3) + 
               2*Power(t2,4) + Power(t1,5)*(1 + 3*t2) + 
               Power(t1,4)*(-13 + 36*t2 - 11*Power(t2,2)) - 
               Power(t1,3)*(4 - 96*t2 + 11*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(t1,2)*(203 - 70*t2 + 69*Power(t2,2) - 
                  18*Power(t2,3) + 3*Power(t2,4)) + 
               2*t1*(-142 - 78*t2 + 85*Power(t2,2) + 45*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s2,2)*(-229 - 137*t2 + 107*Power(t2,2) + 
               52*Power(t2,3) + 7*Power(t2,4) + Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(-50 + 81*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(69 + 113*t2 + 64*Power(t2,2) - 
                  30*Power(t2,3)) + 
               t1*(31 - 120*t2 + 164*Power(t2,2) + 6*Power(t2,4)))) + 
         Power(s,3)*(-69 + 102*t1 - 26*Power(t1,2) + 21*Power(t1,3) + 
            6*Power(t1,4) + 83*t2 + 152*t1*t2 + 62*Power(t1,2)*t2 - 
            39*Power(t1,3)*t2 + 52*Power(t2,2) - 7*t1*Power(t2,2) - 
            75*Power(t1,2)*Power(t2,2) + 6*Power(t2,3) - 
            7*t1*Power(t2,3) + 3*Power(t2,4) - 
            Power(s1,3)*(20 + 13*Power(s2,2) - 2*s2*(17 + 14*t1) + 
               t1*(38 - 10*t2) + Power(t2,2)) + 
            Power(s2,3)*(34 - 2*Power(t1,2) - 78*t2 + t1*(-10 + 27*t2)) + 
            Power(s2,2)*(12 + 8*Power(t1,3) + Power(t1,2)*(17 - 60*t2) + 
               208*t2 + 27*Power(t2,2) + 
               t1*(-35 + 85*t2 - 28*Power(t2,2))) + 
            s2*(-6*Power(t1,4) + Power(t1,3)*(-19 + 29*t2) + 
               Power(t1,2)*(5 + 47*t2 + 32*Power(t2,2)) - 
               2*(4 + 77*t2 + 58*Power(t2,2)) + 
               t1*(-66 - 280*t2 + 66*Power(t2,2) + Power(t2,3))) + 
            Power(s1,2)*(26 - 21*Power(s2,3) + 
               Power(t1,2)*(62 - 36*t2) + 56*t2 + 4*Power(t2,2) + 
               4*Power(t2,3) + Power(s2,2)*(68 + 68*t1 + 44*t2) + 
               t1*(146 + 26*t2 - 22*Power(t2,2)) - 
               s2*(72 + 26*Power(t1,2) + 118*t2 - 9*Power(t2,2) + 
                  2*t1*(100 + 9*t2))) + 
            s1*(11 - 134*t2 - 37*Power(t2,2) - 9*Power(t2,3) - 
               3*Power(t2,4) + 2*Power(t1,3)*(-1 + 5*t2) + 
               Power(s2,3)*(-8 + 7*t1 + 51*t2) + 
               Power(t1,2)*(-122 + 22*t2 + 43*Power(t2,2)) + 
               t1*(-97 - 212*t2 + 43*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(-27 + 7*Power(t1,2) - 224*t2 + Power(t2,2) - 
                  t1*(105 + 53*t2)) + 
               s2*(53 - 8*Power(t1,3) + Power(t1,2)*(85 - 35*t2) + 
                  266*t2 + 61*Power(t2,2) - Power(t2,3) + 
                  t1*(200 + 263*t2 - 52*Power(t2,2))))) + 
         Power(s,2)*(-41 - 36*t1 + 96*Power(t1,2) - 38*Power(t1,3) + 
            8*Power(t1,4) + 3*Power(t1,5) - 71*t2 + 126*t1*t2 + 
            92*Power(t1,2)*t2 + 47*Power(t1,3)*t2 - 7*Power(t1,4)*t2 + 
            104*Power(t2,2) + 151*t1*Power(t2,2) - 
            20*Power(t1,2)*Power(t2,2) - 63*Power(t1,3)*Power(t2,2) + 
            7*Power(t2,3) - 28*t1*Power(t2,3) - 
            27*Power(t1,2)*Power(t2,3) + 7*Power(t2,4) + 
            6*t1*Power(t2,4) + 
            Power(s2,4)*(-34 + Power(t1,2) + t1*(2 - 11*t2) + 50*t2) + 
            Power(s1,4)*(8 + 5*Power(s2,2) - t1*(-12 + t2) + 
               s2*(-14 - 11*t1 + t2)) + 
            Power(s2,3)*(14 - 5*Power(t1,3) - 134*t2 - 51*Power(t2,2) + 
               4*Power(t1,2)*(-1 + 9*t2) + 
               t1*(68 - 109*t2 + 30*Power(t2,2))) - 
            s2*(-202 + 3*Power(t1,5) + Power(t1,4)*(14 - 6*t2) + 28*t2 + 
               228*Power(t2,2) + 22*Power(t2,3) + 9*Power(t2,4) - 
               Power(t1,3)*(20 + 20*t2 + 37*Power(t2,2)) + 
               t1*(111 + 309*t2 + 214*Power(t2,2) - 47*Power(t2,3)) + 
               Power(t1,2)*(3 + 210*t2 - 134*Power(t2,2) - 4*Power(t2,3))\
) + Power(s2,2)*(-66 + 7*Power(t1,4) + Power(t1,3)*(13 - 31*t2) + 
               77*t2 + 241*Power(t2,2) - 18*Power(t2,3) + 
               Power(t1,2)*(-66 + 36*t2 - 69*Power(t2,2)) + 
               t1*(51 + 348*t2 - 8*Power(t2,2) - 3*Power(t2,3))) + 
            Power(s1,3)*(-18 + 31*Power(s2,3) - 17*t2 - 5*Power(t2,2) + 
               Power(t1,2)*(-67 + 21*t2) + 
               t1*(-123 - 6*t2 + 2*Power(t2,2)) - 
               Power(s2,2)*(87*t1 + 10*(11 + t2)) + 
               s2*(92 + 46*Power(t1,2) + 38*t2 - Power(t2,2) + 
                  t1*(208 + 6*t2))) + 
            Power(s1,2)*(-97 + 18*Power(s2,4) + 
               Power(t1,3)*(7 - 21*t2) + 141*t2 + 10*Power(t2,2) + 
               12*Power(t2,3) - Power(s2,3)*(70 + 60*t1 + 83*t2) + 
               Power(t1,2)*(157 + 52*t2 - 50*Power(t2,2)) + 
               t1*(113 + 262*t2 - 36*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*(149 + 24*Power(t1,2) + 306*t2 - 
                  26*Power(t2,2) + t1*(265 + 153*t2)) + 
               s2*(-70 + 14*Power(t1,3) - 253*t2 - 28*Power(t2,2) - 
                  9*Power(t2,3) - Power(t1,2)*(157 + 26*t2) + 
                  t1*(-337 - 427*t2 + 62*Power(t2,2)))) + 
            s1*(246 + Power(s2,4)*(12 + t1 - 39*t2) + 
               Power(t1,4)*(-3 + t2) - 98*t2 - 105*Power(t2,2) - 
               12*Power(t2,3) - 7*Power(t2,4) + 
               Power(t1,3)*(-69 + 42*t2 + 26*Power(t2,2)) + 
               Power(t1,2)*(-79 - 225*t2 + 74*Power(t2,2) + 
                  23*Power(t2,3)) - 
               t1*(79 + 338*t2 + 88*Power(t2,2) - 20*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,3)*(-4 - 12*Power(t1,2) + 218*t2 + 
                  21*Power(t2,2) + 6*t1*(7 + 12*t2)) + 
               Power(s2,2)*(-28 + 9*Power(t1,3) - 388*t2 - 
                  146*Power(t2,2) + 21*Power(t2,3) + 
                  Power(t1,2)*(-86 + 29*t2) + 
                  8*t1*(-10 - 65*t2 + Power(t2,2))) + 
               s2*(-225 + 2*Power(t1,4) + Power(t1,3)*(49 - 57*t2) + 
                  290*t2 + 214*Power(t2,2) + 21*Power(t2,3) + 
                  9*Power(t2,4) + 
                  Power(t1,2)*(150 + 195*t2 - 69*Power(t2,2)) + 
                  t1*(162 + 754*t2 + 93*Power(t2,2) - 41*Power(t2,3))))) + 
         s*(120 - 157*t1 + 53*Power(t1,2) - 10*Power(t1,3) - 
            7*Power(t1,4) + Power(t1,5) - 165*t2 + 34*t1*t2 + 
            85*Power(t1,2)*t2 + 4*Power(t1,3)*t2 + 17*Power(t1,4)*t2 + 
            3*Power(t1,5)*t2 + 26*Power(t2,2) + 85*t1*Power(t2,2) + 
            69*Power(t1,2)*Power(t2,2) + 7*Power(t1,3)*Power(t2,2) - 
            17*Power(t1,4)*Power(t2,2) + 17*Power(t2,3) + 
            30*t1*Power(t2,3) - 53*Power(t1,2)*Power(t2,3) - 
            21*Power(t1,3)*Power(t2,3) + 2*Power(t2,4) + 
            8*t1*Power(t2,4) + 3*Power(t1,2)*Power(t2,4) + 
            Power(s2,5)*(12 + (-12 + t1)*t2) - 
            2*Power(s1,4)*(-2 + 5*Power(s2,3) + 
               s2*(16 + 11*Power(t1,2) + t1*(37 - 2*t2) - t2) + 
               t1*(-20 + t2) + Power(t1,2)*(-12 + t2) + 
               Power(s2,2)*(-19 - 16*t1 + t2)) - 
            Power(s2,4)*(16 - 8*t2 - 41*Power(t2,2) + 
               Power(t1,2)*(1 + 4*t2) + t1*(29 - 39*t2 + 12*Power(t2,2))) \
+ Power(s2,3)*(30 + 70*t2 - 178*Power(t2,2) + 24*Power(t2,3) + 
               Power(t1,3)*(1 + 2*t2) + 
               Power(t1,2)*(29 - 37*t2 + 42*Power(t2,2)) + 
               t1*(42 - 106*t2 - 66*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s2,2)*(-121 - 169*t2 + 192*Power(t2,2) + 
               26*Power(t2,3) + 9*Power(t2,4) + Power(t1,4)*(1 + 4*t2) - 
               2*Power(t1,3)*(7 - 10*t2 + 22*Power(t2,2)) + 
               t1*(23 + 77*t2 + 323*Power(t2,2) - 73*Power(t2,3)) - 
               Power(t1,2)*(67 - 139*t2 + 26*Power(t2,2) + 8*Power(t2,3))) \
+ s2*(29 + 236*t2 - 105*Power(t2,2) - 62*Power(t2,3) - 14*Power(t2,4) - 
               Power(t1,5)*(1 + 3*t2) + 
               Power(t1,4)*(1 - 13*t2 + 14*Power(t2,2)) + 
               Power(t1,3)*(48 - 62*t2 + 64*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(-59 - 111*t2 - 126*Power(t2,2) + 
                  72*Power(t2,3)) + 
               t1*(92 + 7*t2 - 325*Power(t2,2) + 16*Power(t2,3) - 
                  12*Power(t2,4))) + 
            Power(s1,3)*(-23*Power(s2,4) + 4*Power(t1,3)*(-5 + 3*t2) + 
               2*Power(s2,3)*(51 + 37*t1 + 10*t2) - t1*(21 + 103*t2) + 
               2*(31 - 26*t2 + Power(t2,2)) + 
               Power(t1,2)*(-95 - 27*t2 + 7*Power(t2,2)) - 
               Power(s2,2)*(132 + 59*Power(t1,2) + 96*t2 - 
                  5*Power(t2,2) + t1*(246 + 62*t2)) + 
               s2*(35 + 8*Power(t1,3) + 75*t2 + 2*Power(t2,2) + 
                  6*Power(t1,2)*(24 + 5*t2) + 
                  t1*(207 + 163*t2 - 12*Power(t2,2)))) + 
            Power(s1,2)*(-157 - 5*Power(s2,5) - 4*t2 + 61*Power(t2,2) - 
               2*Power(t1,4)*(6 + t2) + 
               2*Power(s2,4)*(7 + 7*t1 + 31*t2) + 
               Power(t1,3)*(40 + 18*t2 - 30*Power(t2,2)) - 
               2*Power(t1,2)*
                (-64 - 97*t2 + 23*Power(t2,2) + Power(t2,3)) + 
               t1*(-125 + 192*t2 + 77*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,3)*(-90 + 9*Power(t1,2) - 260*t2 + 
                  21*Power(t2,2) - 2*t1*(46 + 85*t2)) + 
               Power(s2,2)*(13 - 32*Power(t1,3) + 287*t2 + 
                  58*Power(t2,2) + 6*Power(t2,3) + 
                  2*Power(t1,2)*(41 + 53*t2) + 
                  t1*(227 + 617*t2 - 44*Power(t2,2))) + 
               s2*(263 + 14*Power(t1,4) - 162*t2 + 4*Power(t1,3)*t2 - 
                  89*Power(t2,2) - 18*Power(t2,3) + 
                  Power(t1,2)*(-199 - 325*t2 + 53*Power(t2,2)) - 
                  t1*(64 + 589*t2 + 49*Power(t2,2) + 4*Power(t2,3)))) - 
            s1*(-45 + 6*Power(t1,5) + Power(s2,5)*(6 + t1 - 11*t2) - 
               139*t2 + 83*Power(t2,2) + 15*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,4)*(25 - 19*t2 - 3*Power(t2,2)) - 
               Power(t1,3)*(25 - 98*t2 + 35*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(152 + 223*t2 + 20*Power(t2,2) - 
                  44*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(-245 + 39*t2 + 176*Power(t2,2) + 26*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s2,4)*(-33 - 3*Power(t1,2) + 80*t2 + 
                  29*Power(t2,2) + 5*t1*(-1 + 5*t2)) - 
               Power(s2,3)*(-17 + 3*Power(t1,3) + 
                  Power(t1,2)*(19 - 20*t2) + 200*t2 + 121*Power(t2,2) - 
                  27*Power(t2,3) + t1*(-100 + 301*t2 + 60*Power(t2,2))) + 
               Power(s2,2)*(-308 + 11*Power(t1,4) + 
                  Power(t1,3)*(51 - 59*t2) + 125*t2 + 225*Power(t2,2) + 
                  15*Power(t2,3) + 9*Power(t2,4) + 
                  Power(t1,2)*(-94 + 269*t2 + 2*Power(t2,2)) + 
                  t1*(85 + 586*t2 + 237*Power(t2,2) - 64*Power(t2,3))) - 
               s2*(-499 + 6*Power(t1,5) + Power(t1,4)*(39 - 25*t2) + 
                  17*t2 + 180*Power(t2,2) + 64*Power(t2,3) + 
                  14*Power(t2,4) + 
                  Power(t1,3)*(2 + 41*t2 - 32*Power(t2,2)) + 
                  Power(t1,2)*
                   (69 + 464*t2 + 49*Power(t2,2) - 53*Power(t2,3)) + 
                  t1*(-7 + 247*t2 + 376*Power(t2,2) - 20*Power(t2,3) + 
                     12*Power(t2,4)))))))/
     ((-1 + s1)*(-1 + s2)*(-1 + t1)*Power(s - s2 + t1,3)*(-1 + t2)*
       Power(s - s1 + t2,2)) - 
    (3*(32*((-2*(2 + s*(-3 + s1) - s2 - Power(s1,2)*s2 + 2*s1*(3 + s2))*
               (-1 + t2))/(s - s1 + t2) + 
            (2*(-1 + s1)*(-1 + t2)*
               (5 + Power(s1,2) - 2*s2 + 2*Power(s2,2) + 6*t1 - 
                 2*s2*t1 + Power(t1,2) + 2*s1*(1 + t1) + 
                 s*(-4 + Power(s1,2) + s2*(-3 + t1 - t2) + 
                    s1*(-1 + s2 + t1 - t2)) - 
                 Power(s,2)*(-2 + s1 + t1 - t2) - 6*t2 - 2*t1*t2 + 
                 Power(t2,2)))/((-1 + s2)*(-s + s2 - t1)) - 
            (2*(-1 + s1)*(2 + s2 - 4*t1 + s1*(-3 + t2) - 
                 s*Power(-1 + t2,2) - 6*t2 - 2*s2*t2 + 3*t1*t2 + 
                 s2*Power(t2,2) - t1*Power(t2,2)))/(-1 + t1) + 
            2*(7 - s2 + 3*t1 + 2*s*(-1 + t2) - 8*t2 + s2*t2 - t1*t2 + 
               3*Power(t2,2) + 
               s1*(-1 + s2 + t1*(-3 + t2) + 8*t2 - s2*t2 - Power(t2,2))) \
+ ((-1 + s1)*(-1 + t2)*(12 + s2 - 2*Power(s2,2) - 3*t1 + 3*s2*t1 + 
                 Power(t1,2) + Power(s1,2)*(-2 + s2 + t1) - 3*t2 - 
                 s2*t2 + Power(s2,2)*t2 + 2*t1*t2 - s2*t1*t2 + 
                 Power(t2,2) + s2*Power(t2,2) - 
                 Power(s,2)*(-2 + t1 + t2) + 
                 s1*(1 + Power(s2,2) + Power(t1,2) + 3*t2 - 
                    t1*(1 + t2)) + 
                 s*(-13 + t1 - Power(t1,2) + s2*(2 + t1) + t2 - 
                    Power(t2,2) + s1*(2 - s2 + t2))))/
             ((-1 + s2)*(-1 + t1)) - 
            ((-1 + s1)*(-1 + t2)*
               (-3 - 3*s2 + 2*Power(s2,2) - 5*t1 - s2*t1 - 
                 2*Power(t1,2) + Power(s1,2)*(-6 + s2 + t1) + 6*t2 - 
                 Power(s2,2)*t2 + 5*t1*t2 - 3*Power(t2,2) + 
                 s2*Power(t2,2) - 2*Power(s,2)*(1 + t2) - 
                 s1*(7 + s2 + Power(s2,2) + 4*t1 - 3*s2*t1 - 5*t2 + 
                    4*s2*t2 - t1*t2) - 
                 s*(-4 + s1*(-4 + t1 - 3*t2) + t2 - 3*s2*t2 + 
                    Power(t2,2) + t1*(1 + s2 + t2))))/
             ((s - s2 + t1)*(s - s1 + t2))) + 
         24*(-18 + 8*s + 10*s1 + 6*s2 - 6*s1*s2 - 14*t1 + 14*s1*t1 + 
            40*t2 - 8*s*t2 - 24*s1*t2 - 6*s2*t2 + 6*s1*s2*t2 + 6*t1*t2 - 
            6*s1*t1*t2 - 14*Power(t2,2) + 6*s1*Power(t2,2) + 
            ((-1 + s1)*(11 + 2*s2 - 7*t1 - 14*t2 - 5*s2*t2 + 7*t1*t2 + 
                 7*Power(t2,2) + 3*s2*Power(t2,2) - 4*t1*Power(t2,2) - 
                 s*(-1 + t2)*(-2 + t1 + 3*t2) - 
                 s1*(4 + s2 + t1*(-5 + t2) - s2*t2)))/(-1 + t1) + 
            ((-1 + s1)*(-1 + t2)*
               (-1 + 2*s2 - 6*Power(s2,2) + 
                 Power(s1,2)*(-2 + 3*s2 - t1) + 6*s2*t1 - 
                 3*Power(t1,2) + 4*t2 - 3*s2*t2 + 2*Power(s2,2)*t2 + 
                 2*t1*t2 - s2*t1*t2 - 3*Power(t2,2) + s2*Power(t2,2) + 
                 Power(s,2)*(-4 + 2*s1 + t1 + t2) + 
                 s1*(1 + 2*Power(s2,2) + Power(t1,2) - s2*(3 + t1) + 
                    t2 - t1*t2) - 
                 s*(-3 + 2*Power(s1,2) + 4*t1 + Power(t1,2) + 
                    s1*(-2 + 3*s2 - t2) + 3*s2*(-2 + t2) + 2*t2 - 
                    4*t1*t2 + Power(t2,2))))/((-1 + s2)*(-s + s2 - t1)) - 
            ((-1 + t2)*(5 + 2*s2 - t1 + 
                 Power(s1,2)*(4 + 3*s2 + t1 - 2*t2) + 5*t2 - s2*t2 + 
                 2*t1*t2 - 6*Power(t2,2) + 
                 s*(4 + t1 - s1*t1 + (-5 + s1)*t2) + 
                 s1*(-13 + s2*(-5 + t2) + (5 - 2*t1)*t2 + 2*Power(t2,2)))\
)/(s - s1 + t2) - ((-1 + s1)*(-1 + t2)*
               (12 - s2 - 2*Power(s2,2) - 11*t1 + s2*t1 + Power(t1,2) + 
                 Power(s1,2)*(-2 + s2 + t1) - 11*t2 + 3*s2*t2 + 
                 Power(s2,2)*t2 + 6*t1*t2 - s2*t1*t2 + Power(t2,2) + 
                 s2*Power(t2,2) - 2*Power(s,2)*(-2 + t1 + t2) + 
                 s1*(-1 + Power(s2,2) + Power(t1,2) - t1*(-3 + t2) + 
                    t2 - 2*s2*(t1 + t2)) + 
                 s*(-14 + 6*t1 - Power(t1,2) + 6*t2 - 2*t1*t2 - 
                    Power(t2,2) + s2*(2 + t1 + t2) + 
                    s1*(2 - 2*s2 + t1 + t2))))/((-1 + s2)*(-1 + t1)) + 
            ((-1 + s1)*(-1 + t2)*
               (4 + s2 + 2*Power(s2,2) - 3*t1 - s2*t1 - 3*Power(t1,2) + 
                 2*Power(s1,2)*(-2 + s2 + t1) - 6*t2 + s2*t2 - 
                 Power(s2,2)*t2 + 3*t1*t2 - s2*t1*t2 + 2*Power(t2,2) + 
                 2*t1*Power(t2,2) - 
                 s*(6 + 2*s1*(-3 + t1) + 2*t1 + Power(t1,2) + 
                    s2*(2 + t1 - t2) - 4*t2 - 3*t1*t2) + 
                 s1*(2 - Power(s2,2) + 2*t1 + Power(t1,2) + 2*t2 - 
                    4*t1*t2 + s2*(4*t1 - 2*(2 + t2)))))/
             ((s - s2 + t1)*(s - s1 + t2)))))/
     (Power(-1 + s1,2)*Power(-1 + t2,2)) + 
    (8*(t1*(4*s2 - 5*Power(s2,2) - Power(s2,3) + 2*Power(s2,4) - 8*t1 - 
            26*s2*t1 + 58*Power(s2,2)*t1 - 30*Power(s2,3)*t1 + 
            6*Power(s2,4)*t1 + 26*Power(t1,2) - 28*s2*Power(t1,2) + 
            6*Power(s2,2)*Power(t1,2) + 6*Power(s2,3)*Power(t1,2) - 
            10*Power(s2,4)*Power(t1,2) - 25*Power(t1,3) + 
            27*s2*Power(t1,3) - 34*Power(s2,2)*Power(t1,3) + 
            30*Power(s2,3)*Power(t1,3) + 2*Power(s2,4)*Power(t1,3) + 
            5*Power(t1,4) + 29*s2*Power(t1,4) - 
            29*Power(s2,2)*Power(t1,4) - 5*Power(s2,3)*Power(t1,4) + 
            Power(t1,5) - 5*s2*Power(t1,5) + 4*Power(s2,2)*Power(t1,5) + 
            Power(t1,6) - s2*Power(t1,6) + 
            Power(s1,4)*(-1 + t1)*
             (s2*(-1 - 4*t1 + Power(t1,2)) + 
               t1*(7 - 4*t1 + Power(t1,2)))*(-1 + t2) - 4*t2 - 2*s2*t2 + 
            5*Power(s2,2)*t2 + 5*Power(s2,3)*t2 - 3*Power(s2,4)*t2 + 
            48*t1*t2 + 25*s2*t1*t2 - 140*Power(s2,2)*t1*t2 + 
            58*Power(s2,3)*t1*t2 - 9*Power(s2,4)*t1*t2 - 
            179*Power(t1,2)*t2 + 284*s2*Power(t1,2)*t2 - 
            82*Power(s2,2)*Power(t1,2)*t2 - 
            10*Power(s2,3)*Power(t1,2)*t2 + 
            15*Power(s2,4)*Power(t1,2)*t2 - 18*Power(t1,3)*t2 + 
            16*s2*Power(t1,3)*t2 + 49*Power(s2,2)*Power(t1,3)*t2 - 
            44*Power(s2,3)*Power(t1,3)*t2 - 
            3*Power(s2,4)*Power(t1,3)*t2 - 54*Power(t1,4)*t2 - 
            39*s2*Power(t1,4)*t2 + 69*Power(s2,2)*Power(t1,4)*t2 + 
            7*Power(s2,3)*Power(t1,4)*t2 + 40*Power(t1,5)*t2 - 
            29*s2*Power(t1,5)*t2 - 5*Power(s2,2)*Power(t1,5)*t2 - 
            Power(t1,6)*t2 + s2*Power(t1,6)*t2 + 6*Power(t2,2) - 
            14*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) - 
            3*Power(s2,3)*Power(t2,2) + Power(s2,4)*Power(t2,2) - 
            63*t1*Power(t2,2) + 234*s2*t1*Power(t2,2) - 
            43*Power(s2,2)*t1*Power(t2,2) - 
            32*Power(s2,3)*t1*Power(t2,2) + 
            3*Power(s2,4)*t1*Power(t2,2) + 11*Power(t1,2)*Power(t2,2) - 
            262*s2*Power(t1,2)*Power(t2,2) + 
            142*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
            7*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
            5*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
            125*Power(t1,3)*Power(t2,2) - 32*s2*Power(t1,3)*Power(t2,2) - 
            93*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
            14*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
            Power(s2,4)*Power(t1,3)*Power(t2,2) - 
            38*Power(t1,4)*Power(t2,2) + 56*s2*Power(t1,4)*Power(t2,2) - 
            8*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
            2*Power(s2,3)*Power(t1,4)*Power(t2,2) - 
            9*Power(t1,5)*Power(t2,2) + 2*s2*Power(t1,5)*Power(t2,2) + 
            Power(s2,2)*Power(t1,5)*Power(t2,2) + 8*s2*Power(t2,3) + 
            3*Power(s2,2)*Power(t2,3) - Power(s2,3)*Power(t2,3) + 
            26*t1*Power(t2,3) - 96*s2*t1*Power(t2,3) - 
            23*Power(s2,2)*t1*Power(t2,3) + 
            4*Power(s2,3)*t1*Power(t2,3) - 8*Power(t1,2)*Power(t2,3) + 
            110*s2*Power(t1,2)*Power(t2,3) - 
            14*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
            3*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
            29*Power(t1,3)*Power(t2,3) + 4*s2*Power(t1,3)*Power(t2,3) + 
            10*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
            19*Power(t1,4)*Power(t2,3) - 10*s2*Power(t1,4)*Power(t2,3) - 
            2*Power(t2,4) + t1*Power(t2,4) + 7*s2*t1*Power(t2,4) + 
            2*Power(t1,2)*Power(t2,4) - 8*s2*Power(t1,2)*Power(t2,4) - 
            Power(t1,3)*Power(t2,4) + s2*Power(t1,3)*Power(t2,4) - 
            Power(s,3)*(-1 + t1)*(-1 + t2)*
             (2 + (-2 + s1)*Power(t1,3) + 
               Power(t1,2)*(-2*(-7 + t2) + s1*(-6 + t2)) + 3*t2 - 
               2*s1*t2 + t1*(-13 - 4*s1*(-3 + t2) + 8*t2) + 
               s2*(-2 + Power(t1,3) + t1*(15 - 14*s1 - 4*t2) - t2 + 
                  Power(t1,2)*(-12 + 4*s1 + t2))) + 
            Power(s1,3)*(-1 + t1)*
             (1 - Power(t2,2) + 
               Power(s2,2)*(-1 + 4*Power(t1,2) + 6*t1*(-3 + t2) + t2) + 
               t1*(16 + 3*t2 - 19*Power(t2,2)) + 
               Power(t1,3)*(9 - 3*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(-11 - 13*t2 + 12*Power(t2,2)) + 
               s2*(-5 + 4*Power(t1,3)*(-2 + t2) + 3*t2 + 2*Power(t2,2) + 
                  Power(t1,2)*(36 - 25*t2 - 3*Power(t2,2)) + 
                  3*t1*(15 - 16*t2 + 5*Power(t2,2)))) + 
            Power(s1,2)*(Power(t1,5)*(11 - 7*t2) + 
               8*Power(s2,3)*(-3 + t1)*t1*(-1 + t2) - 
               3*Power(-1 + t2,2)*(1 + t2) + 
               Power(t1,4)*(-57 - 11*t2 + 19*Power(t2,2) + 
                  Power(t2,3)) - 
               t1*(-52 + 13*t2 + 33*Power(t2,2) + 10*Power(t2,3)) - 
               Power(t1,3)*(68 - 107*t2 + 53*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,2)*(-103 - 47*t2 + 72*Power(t2,2) + 
                  22*Power(t2,3)) + 
               Power(s2,2)*(1 - 9*t2 + 4*Power(t2,2) + 
                  Power(t1,4)*(3 + t2) + 
                  Power(t1,2)*(-17 + 9*t2 - 16*Power(t2,2)) + 
                  t1*(-87 + 44*t2 - 13*Power(t2,2)) + 
                  Power(t1,3)*(-4 - 45*t2 + Power(t2,2))) + 
               s2*(-1 + Power(t1,5)*(-5 + t2) - 4*t2 + 8*Power(t2,2) + 
                  Power(t2,3) + 
                  Power(t1,4)*(37 + 16*t2 - 9*Power(t2,2)) + 
                  Power(t1,2)*
                   (148 - 90*t2 + 43*Power(t2,2) - 21*Power(t2,3)) + 
                  Power(t1,3)*
                   (40 - 36*t2 + 65*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(37 + 97*t2 - 91*Power(t2,2) + 17*Power(t2,3)))) + 
            s1*(4 + Power(t1,6)*(-1 + t2) + 
               Power(s2,4)*(1 + 3*t1 - 5*Power(t1,2) + Power(t1,3))*
                (-1 + t2) + t2 - 10*Power(t2,2) + 3*Power(t2,3) + 
               2*Power(t2,4) + 
               Power(t1,5)*(-31 - 9*t2 + 4*Power(t2,2)) + 
               Power(t1,4)*(5 + 115*t2 + 4*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,2)*(30 + 271*t2 - 105*Power(t2,2) - 
                  2*Power(t2,3) - 2*Power(t2,4)) - 
               t1*(34 + 30*t2 - 82*Power(t2,2) + 17*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,3)*(195 - 213*t2 - 15*Power(t2,2) + 
                  20*Power(t2,3) + Power(t2,4)) + 
               Power(s2,3)*(1 - 3*Power(t1,4)*(-1 + t2) - 4*t2 + 
                  3*Power(t2,2) + 
                  Power(t1,2)*(10 + 21*t2 - 31*Power(t2,2)) + 
                  Power(t1,3)*(2 - 6*t2 + 4*Power(t2,2)) + 
                  8*t1*(-4 - t2 + 5*Power(t2,2))) + 
               Power(s2,2)*(4 + 3*Power(t1,5)*(-1 + t2) + t2 - 
                  2*Power(t2,2) - 3*Power(t2,3) - 
                  Power(t1,4)*(19 + 8*t2 + 9*Power(t2,2)) + 
                  Power(t1,3)*
                   (-44 + 73*t2 + 82*Power(t2,2) + Power(t2,3)) - 
                  Power(t1,2)*
                   (-151 + 119*t2 + 38*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*(15 + 154*t2 - 9*Power(t2,2) + 32*Power(t2,3))) + 
               s2*(-9 - Power(t1,6)*(-1 + t2) + 13*t2 + 4*Power(t2,2) - 
                  8*Power(t2,3) + 
                  2*Power(t1,5)*(14 + 3*t2 + Power(t2,2)) + 
                  Power(t1,4)*
                   (28 - 95*t2 - 11*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(72 - 244*t2 - 80*Power(t2,2) + 67*Power(t2,3) - 
                     7*Power(t2,4)) + 
                  Power(t1,3)*
                   (-152 + 162*t2 - 110*Power(t2,2) + Power(t2,3) - 
                     Power(t2,4)) + 
                  Power(t1,2)*
                   (-224 - 81*t2 + 195*Power(t2,2) - 78*Power(t2,3) + 
                     8*Power(t2,4)))) + 
            Power(s,2)*(3 - 64*t1 - 61*Power(t1,2) - 85*Power(t1,3) + 
               39*Power(t1,4) + 2*t2 - 94*t1*t2 + 91*Power(t1,2)*t2 + 
               48*Power(t1,3)*t2 - 15*Power(t1,4)*t2 - 4*Power(t2,2) + 
               19*t1*Power(t2,2) + 16*Power(t1,2)*Power(t2,2) - 
               31*Power(t1,3)*Power(t2,2) + 8*Power(t1,4)*Power(t2,2) + 
               3*Power(t2,3) - 9*t1*Power(t2,3) + 
               6*Power(t1,2)*Power(t2,3) + 
               Power(s2,2)*(-4 + t2 + 3*Power(t2,2) + 
                  Power(t1,2)*(-34 + 49*t2 - 15*Power(t2,2)) + 
                  Power(t1,3)*(14 - 17*t2 + 3*Power(t2,2)) + 
                  t1*(8 - 17*t2 + 9*Power(t2,2))) + 
               s2*(1 + 4*t2 - 8*Power(t2,2) - Power(t2,3) + 
                  Power(t1,4)*(-31 + 3*t2 - 4*Power(t2,2)) + 
                  2*Power(t1,3)*(17 + 7*t2 + 10*Power(t2,2)) + 
                  Power(t1,2)*
                   (39 - 94*t2 + 6*Power(t2,2) - 3*Power(t2,3)) + 
                  t1*(45 + 89*t2 + 10*Power(t2,2) + 4*Power(t2,3))) + 
               Power(s1,2)*(-1 + t1)*
                (t1 + Power(t1,2)*(6 - 10*t2) + 
                  3*Power(t1,3)*(-1 + t2) + 11*t1*t2 - (-1 + t2)*t2 + 
                  s2*(t1*(6 - 18*t2) + 2*(-1 + t2) + 
                     Power(t1,2)*(-1 + 5*t2))) + 
               s1*(2 + Power(s2,2)*
                   (-1 + t1 - 21*Power(t1,2) + 5*Power(t1,3))*(-1 + t2) \
+ t2 + 3*Power(t2,2) - 2*Power(t2,3) - 
                  4*Power(t1,4)*(2 - 2*t2 + Power(t2,2)) + 
                  Power(t1,3)*(33 + 4*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*
                   (123 - 94*t2 + 14*Power(t2,2) - 3*Power(t2,3)) + 
                  t1*(18 + 49*t2 - 28*Power(t2,2) + 5*Power(t2,3)) + 
                  s2*(-5 - 4*t2 + 5*Power(t2,2) + Power(t1,4)*(3 + t2) + 
                     Power(t1,2)*(-90 + 71*t2 - 21*Power(t2,2)) + 
                     t1*(3 - 38*t2 - 9*Power(t2,2)) + 
                     Power(t1,3)*(1 - 46*t2 + Power(t2,2))))) + 
            s*(-44*t1 - 53*Power(t1,2) - 28*Power(t1,3) - 
               74*Power(t1,4) + 30*Power(t1,5) + Power(t1,6) + 5*t2 - 
               42*t1*t2 - 201*Power(t1,2)*t2 + 53*Power(t1,3)*t2 + 
               51*Power(t1,4)*t2 - Power(t1,5)*t2 - Power(t1,6)*t2 + 
               4*Power(t2,2) - 73*t1*Power(t2,2) + 
               168*Power(t1,2)*Power(t2,2) - 46*Power(t1,3)*Power(t2,2) - 
               16*Power(t1,4)*Power(t2,2) + 3*Power(t1,5)*Power(t2,2) - 
               5*Power(t2,3) + 22*t1*Power(t2,3) - 
               18*Power(t1,2)*Power(t2,3) + 6*Power(t1,3)*Power(t2,3) + 
               3*Power(t1,4)*Power(t2,3) - 7*t1*Power(t2,4) + 
               8*Power(t1,2)*Power(t2,4) - Power(t1,3)*Power(t2,4) + 
               Power(s2,3)*(Power(t1,4)*(-1 + t2) - 
                  3*Power(t1,3)*Power(-1 + t2,2) - 3*(-1 + t2)*t2 + 
                  t1*(3 + 6*t2 - 9*Power(t2,2)) + 
                  Power(t1,2)*(17 - 32*t2 + 15*Power(t2,2))) + 
               Power(s2,2)*(-2*Power(t1,5)*(-1 + t2) + 
                  Power(t1,3)*(-57 + 25*t2 - 36*Power(t2,2)) + 
                  2*t2*(-3 + 4*t2 + Power(t2,2)) + 
                  Power(t1,4)*(25 + t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*
                   (-37 + 86*t2 - 3*Power(t2,2) + 6*Power(t2,3)) - 
                  t1*(37 + 120*t2 - 17*Power(t2,2) + 8*Power(t2,3))) + 
               s2*(1 + Power(t1,6)*(-1 + t2) - 11*t2 + 8*Power(t2,2) - 
                  6*Power(t2,3) - 
                  Power(t1,5)*(26 + 3*t2 + 3*Power(t2,2)) + 
                  Power(t1,4)*(33 - 37*t2 + 8*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (-44 + 53*t2 - 56*Power(t2,2) + 5*Power(t2,3)) + 
                  Power(t1,2)*
                   (101 - 16*t2 - 49*Power(t2,2) + 8*Power(t2,3)) + 
                  4*t1*(15 + 61*t2 - 11*Power(t2,2) + 8*Power(t2,3))) - 
               Power(s1,3)*(-1 + t1)*
                (3*Power(t1,3)*(-1 + t2) + (-1 + t2)*t2 - 
                  Power(t1,2)*(-4 + 7*t2 + Power(t2,2)) + 
                  2*t1*(3 + t2 + 2*Power(t2,2)) + 
                  s2*(-1 + t2 + 2*Power(t1,2)*(1 + t2) - 4*t1*(1 + 2*t2))\
) + Power(s1,2)*(1 + t2 - 5*Power(t2,2) - Power(t2,3) + 
                  Power(t1,4)*(1 - 7*t2 + 6*Power(t2,2)) - 
                  Power(t1,3)*
                   (29 - 15*t2 + 16*Power(t2,2) + 2*Power(t2,3)) - 
                  t1*(26 + 47*t2 - 27*Power(t2,2) + 10*Power(t2,3)) + 
                  Power(t1,2)*
                   (-115 + 70*t2 - 4*Power(t2,2) + 13*Power(t2,3)) + 
                  Power(s2,2)*
                   (3*(-1 + t2) - 4*Power(t1,3)*t2 + 
                     2*Power(t1,2)*(3 + 5*t2) + t1*(-19 + 7*t2)) + 
                  s2*(-1 - 4*Power(t1,4)*(-1 + t2) + 9*t2 - 
                     4*Power(t2,2) + Power(t1,3)*(-31 + 67*t2) + 
                     Power(t1,2)*(41 - 32*t2 + 11*Power(t2,2)) + 
                     t1*(75 - 24*t2 + 17*Power(t2,2)))) + 
               s1*(1 - 2*Power(s2,3)*t1*(-5 - 4*t1 + Power(t1,2))*
                   (-1 + t2) - 10*t2 + 4*Power(t2,2) + 5*Power(t2,3) + 
                  Power(t1,5)*(-11 + 7*t2) + 
                  Power(t1,4)*
                   (17 + 8*t2 - 14*Power(t2,2) - 3*Power(t2,3)) + 
                  Power(t1,3)*
                   (79 - 6*t2 + 71*Power(t2,2) + 7*Power(t2,3) + 
                     Power(t2,4)) + 
                  t1*(38 + 160*t2 - 10*Power(t2,2) + Power(t2,3) + 
                     7*Power(t2,4)) - 
                  Power(t1,2)*
                   (-212 + 55*t2 + 99*Power(t2,2) + 18*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  Power(s2,2)*
                   (7 - 4*Power(t1,4) + 3*t2 - 6*Power(t2,2) + 
                     t1*(51 + 22*t2 - 29*Power(t2,2)) - 
                     4*Power(t1,3)*(2 - 14*t2 + Power(t2,2)) + 
                     Power(t1,2)*(74 - 81*t2 + 47*Power(t2,2))) - 
                  s2*(5 + Power(t1,5)*(-5 + t2) - 3*t2 + 7*Power(t2,2) - 
                     5*Power(t2,3) + 
                     Power(t1,4)*(4 + 10*t2 - 10*Power(t2,2)) + 
                     Power(t1,2)*
                      (242 - 242*t2 + 81*Power(t2,2) - 9*Power(t2,3)) + 
                     Power(t1,3)*
                      (4 + 112*t2 + 79*Power(t2,2) + Power(t2,3)) + 
                     t1*(94 + 210*t2 - 101*Power(t2,2) + 37*Power(t2,3))))\
)) + (-1 + t1)*(-4*s2 + 5*Power(s2,2) + Power(s2,3) - 2*Power(s2,4) + 
            2*t1 + 25*s2*t1 - 43*Power(s2,2)*t1 + 2*Power(s2,3)*t1 + 
            14*Power(s2,4)*t1 - 16*Power(t1,2) + 12*s2*Power(t1,2) + 
            46*Power(s2,2)*Power(t1,2) - 28*Power(s2,3)*Power(t1,2) - 
            14*Power(s2,4)*Power(t1,2) + 22*Power(t1,3) - 
            60*s2*Power(t1,3) + 6*Power(s2,2)*Power(t1,3) + 
            30*Power(s2,3)*Power(t1,3) + 2*Power(s2,4)*Power(t1,3) - 
            2*Power(t1,4) + 26*s2*Power(t1,4) - 
            19*Power(s2,2)*Power(t1,4) - 5*Power(s2,3)*Power(t1,4) - 
            8*Power(t1,5) + 3*s2*Power(t1,5) + 
            5*Power(s2,2)*Power(t1,5) + 2*Power(t1,6) - 
            2*s2*Power(t1,6) + 
            Power(s1,4)*(-1 + t1)*
             (s2*(1 - 6*t1 + Power(t1,2)) + t1*(5 - 2*t1 + Power(t1,2)))*
             (-1 + t2) + 4*t2 + 2*s2*t2 - 5*Power(s2,2)*t2 - 
            5*Power(s2,3)*t2 + 3*Power(s2,4)*t2 - 31*t1*t2 - 
            14*s2*t1*t2 + 44*Power(s2,2)*t1*t2 + 31*Power(s2,3)*t1*t2 - 
            21*Power(s2,4)*t1*t2 + 81*Power(t1,2)*t2 - 
            64*s2*Power(t1,2)*t2 - 81*Power(s2,2)*Power(t1,2)*t2 + 
            23*Power(s2,3)*Power(t1,2)*t2 + 
            21*Power(s2,4)*Power(t1,2)*t2 + 5*Power(t1,3)*t2 + 
            56*s2*Power(t1,3)*t2 + Power(s2,2)*Power(t1,3)*t2 - 
            39*Power(s2,3)*Power(t1,3)*t2 - 
            3*Power(s2,4)*Power(t1,3)*t2 - 11*Power(t1,4)*t2 - 
            20*s2*Power(t1,4)*t2 + 14*Power(s2,2)*Power(t1,4)*t2 + 
            6*Power(s2,3)*Power(t1,4)*t2 + 2*Power(t1,5)*t2 + 
            6*s2*Power(t1,5)*t2 - 5*Power(s2,2)*Power(t1,5)*t2 - 
            2*Power(t1,6)*t2 + 2*s2*Power(t1,6)*t2 - 6*Power(t2,2) + 
            14*s2*Power(t2,2) - Power(s2,2)*Power(t2,2) + 
            3*Power(s2,3)*Power(t2,2) - Power(s2,4)*Power(t2,2) + 
            34*t1*Power(t2,2) - 104*s2*t1*Power(t2,2) + 
            26*Power(s2,2)*t1*Power(t2,2) - 
            26*Power(s2,3)*t1*Power(t2,2) + 
            7*Power(s2,4)*t1*Power(t2,2) - 29*Power(t1,2)*Power(t2,2) + 
            23*s2*Power(t1,2)*Power(t2,2) + 
            83*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
            2*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
            7*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
            24*Power(t1,3)*Power(t2,2) - 57*s2*Power(t1,3)*Power(t2,2) - 
            6*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
            10*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
            Power(s2,4)*Power(t1,3)*Power(t2,2) + 
            15*Power(t1,4)*Power(t2,2) + 11*s2*Power(t1,4)*Power(t2,2) - 
            6*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
            Power(s2,3)*Power(t1,4)*Power(t2,2) - 
            6*Power(t1,5)*Power(t2,2) + s2*Power(t1,5)*Power(t2,2) - 
            8*s2*Power(t2,3) - 3*Power(s2,2)*Power(t2,3) + 
            Power(s2,3)*Power(t2,3) + 5*t1*Power(t2,3) + 
            65*s2*t1*Power(t2,3) + 5*Power(s2,2)*t1*Power(t2,3) - 
            7*Power(s2,3)*t1*Power(t2,3) - 18*Power(t1,2)*Power(t2,3) - 
            63*s2*Power(t1,2)*Power(t2,3) + 
            12*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
            7*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
            11*Power(t1,3)*Power(t2,3) + 17*s2*Power(t1,3)*Power(t2,3) - 
            17*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
            Power(s2,3)*Power(t1,3)*Power(t2,3) - 
            18*Power(t1,4)*Power(t2,3) + 7*s2*Power(t1,4)*Power(t2,3) + 
            3*Power(s2,2)*Power(t1,4)*Power(t2,3) + 
            4*Power(t1,5)*Power(t2,3) - 2*s2*Power(t1,5)*Power(t2,3) + 
            2*Power(t2,4) - 14*t1*Power(t2,4) + 
            14*Power(t1,2)*Power(t2,4) - 2*Power(t1,3)*Power(t2,4) + 
            Power(s,3)*(-1 + t1)*(-1 + t2)*
             (2 - s1*Power(t1,3) + 3*t2 - 2*s1*t2 + 
               Power(t1,2)*(1 + s1*(11 + t2)) + 
               t1*(-1 - 9*t2 + s1*(-8 + 3*t2)) + 
               s2*(-2 + Power(t1,3) - t2 - 
                  Power(t1,2)*(10 + 2*s1 + t2) + t1*(3 + 6*s1 + 6*t2))) + 
            Power(s1,3)*(-1 + t1)*
             (-1 - Power(t1,4)*(-1 + t2) + Power(t2,2) + 
               t1*(6 + 9*t2 - 15*Power(t2,2)) - 
               Power(t1,3)*(-5 + Power(t2,2)) - 
               Power(t1,2)*(41 - 30*t2 + Power(t2,2)) + 
               Power(s2,2)*(1 - t2 + 2*Power(t1,2)*(1 + t2) - 
                  3*t1*(3 + t2)) + 
               s2*(5 + Power(t1,3)*(-5 + t2) - 3*t2 - 2*Power(t2,2) + 
                  Power(t1,2)*(21 - 11*t2 - 2*Power(t2,2)) + 
                  t1*(-25 + 25*t2 + 12*Power(t2,2)))) + 
            Power(s1,2)*(2*Power(s2,3)*t1*(-1 - 8*t1 + Power(t1,2))*
                (-1 + t2) + 3*Power(-1 + t2,2)*(1 + t2) + 
               Power(t1,4)*(-28 + 13*t2 - 9*Power(t2,2)) + 
               Power(t1,5)*(2 - 5*t2 + 3*Power(t2,2)) + 
               t1*(-26 + 28*t2 + 27*Power(t2,2) - 25*Power(t2,3)) + 
               Power(t1,3)*(108 + 53*t2 - 22*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,2)*(-11 - 54*t2 - 12*Power(t2,2) + 
                  21*Power(t2,3)) - 
               Power(s2,2)*(1 + 2*Power(t1,4)*(-1 + t2) - 9*t2 + 
                  4*Power(t2,2) + 
                  2*Power(t1,3)*(13 - 2*t2 + Power(t2,2)) + 
                  Power(t1,2)*(21 - 165*t2 + 4*Power(t2,2)) - 
                  2*t1*(7 - 40*t2 + 5*Power(t2,2))) + 
               s2*(1 + 4*t2 - 8*Power(t2,2) - Power(t2,3) + 
                  Power(t1,4)*(17 + 7*t2) + 
                  Power(t1,3)*
                   (-44 - 89*t2 + 16*Power(t2,2) + Power(t2,3)) - 
                  Power(t1,2)*
                   (-6 + 27*t2 + 56*Power(t2,2) + 7*Power(t2,3)) + 
                  t1*(-12 - 7*t2 + 64*Power(t2,2) + 7*Power(t2,3)))) + 
            s1*(-4 + Power(s2,4)*
                (-1 + 7*t1 - 7*Power(t1,2) + Power(t1,3))*(-1 + t2) - 
               t2 + 10*Power(t2,2) - 3*Power(t2,3) - 2*Power(t2,4) + 
               Power(t1,5)*(7 + 2*t2 + Power(t2,2) - 2*Power(t2,3)) + 
               Power(t1,4)*(43 - 19*t2 + 5*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(t1,2)*(1 - 52*t2 + 93*Power(t2,2) - 
                  16*Power(t2,3) - 14*Power(t2,4)) + 
               Power(t1,3)*(-119 - 32*t2 - 44*Power(t2,2) + 
                  9*Power(t2,3) + 2*Power(t2,4)) + 
               t1*(24 + 22*t2 - 81*Power(t2,2) + 21*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,3)*(-1 - 2*Power(t1,4)*(-1 + t2) + 4*t2 - 
                  3*Power(t2,2) + 
                  Power(t1,2)*(-9 + 14*t2 - 5*Power(t2,2)) + 
                  Power(t1,3)*(-19 + 18*t2 + Power(t2,2)) + 
                  t1*(11 - 34*t2 + 23*Power(t2,2))) + 
               Power(s2,2)*(-4 + Power(t1,4)*(19 - 11*t2) + 
                  Power(t1,5)*(-1 + t2) - t2 + 2*Power(t2,2) + 
                  3*Power(t2,3) + 
                  Power(t1,3)*(45 - 22*t2 + 13*Power(t2,2)) + 
                  Power(t1,2)*
                   (-39 - 44*t2 - 110*Power(t2,2) + 9*Power(t2,3)) - 
                  t1*(-12 - 13*t2 + Power(t2,2) + 12*Power(t2,3))) + 
               s2*(9 - 13*t2 - 4*Power(t2,2) + 8*Power(t2,3) + 
                  Power(t1,5)*(-9 + Power(t2,2)) + 
                  t1*(-49 + 77*t2 + 19*Power(t2,2) - 59*Power(t2,3)) + 
                  Power(t1,3)*
                   (74 + 27*t2 + 68*Power(t2,2) - 21*Power(t2,3)) + 
                  Power(t1,4)*
                   (-53 + 22*t2 - 14*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (60 + 31*t2 + 26*Power(t2,2) + 55*Power(t2,3)))) + 
            Power(s,2)*(-3 + 25*t1 + 22*Power(t1,2) + 19*Power(t1,3) - 
               15*Power(t1,4) - 2*t2 + 31*t1*t2 - Power(t1,2)*t2 + 
               Power(t1,3)*t2 + 3*Power(t1,4)*t2 + 4*Power(t2,2) - 
               36*t1*Power(t2,2) + 48*Power(t1,2)*Power(t2,2) - 
               36*Power(t1,3)*Power(t2,2) + 4*Power(t1,4)*Power(t2,2) - 
               3*Power(t2,3) + 12*t1*Power(t2,3) - 
               9*Power(t1,2)*Power(t2,3) + 
               Power(s2,2)*(4 - 2*Power(t1,4)*(-1 + t2) - t2 - 
                  3*Power(t2,2) + 
                  3*Power(t1,2)*(6 + t2 - 7*Power(t2,2)) + 
                  3*Power(t1,3)*(-6 + 5*t2 + Power(t2,2)) + 
                  t1*(-22 + t2 + 21*Power(t2,2))) + 
               s2*(-1 + Power(t1,5)*(-1 + t2) - 4*t2 + 8*Power(t2,2) + 
                  Power(t2,3) - 
                  2*Power(t1,4)*(-9 + 4*t2 + Power(t2,2)) - 
                  Power(t1,3)*
                   (4 - 11*t2 - 10*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (11 - 88*t2 + 10*Power(t2,2) + 7*Power(t2,3)) - 
                  t1*(7 - 8*t2 + 26*Power(t2,2) + 7*Power(t2,3))) + 
               Power(s1,2)*(-1 + t1)*
                (3*Power(t1,3)*(-1 + t2) + (-1 + t2)*t2 + 
                  3*t1*Power(1 + t2,2) - 
                  2*Power(t1,2)*(-8 + 9*t2 + Power(t2,2)) + 
                  s2*(2 - 2*t2 - 3*t1*(3 + t2) + Power(t1,2)*(1 + 3*t2))\
) + s1*(-2 - Power(t1,5)*(-1 + t2) + 
                  Power(s2,2)*
                   (1 - 3*t1 - 17*Power(t1,2) + 3*Power(t1,3))*(-1 + t2) \
- t2 - 3*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t1,4)*(-5 + 7*t2 - 2*Power(t2,2)) + 
                  t1*(9 + 8*t2 + 32*Power(t2,2) - 5*Power(t2,3)) + 
                  Power(t1,3)*
                   (10 - 19*t2 + 28*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-61 - 26*t2 - 39*Power(t2,2) + 2*Power(t2,3)) + 
                  s2*(5 - 3*Power(t1,4)*(-1 + t2) + 4*t2 - 
                     5*Power(t2,2) + 
                     Power(t1,3)*(-24 + 7*t2 - 3*Power(t2,2)) + 
                     Power(t1,2)*(16 + 111*t2 - 3*Power(t2,2)) + 
                     t1*(-16 - 39*t2 + 11*Power(t2,2))))) - 
            s*(-9*t1 - 17*Power(t1,2) - 51*Power(t1,3) + 25*Power(t1,4) + 
               4*Power(t1,5) + 5*t2 - 21*t1*t2 - 37*Power(t1,2)*t2 - 
               19*Power(t1,3)*t2 - 16*Power(t1,4)*t2 + 8*Power(t1,5)*t2 + 
               4*Power(t2,2) - 43*t1*Power(t2,2) + 
               17*Power(t1,2)*Power(t2,2) - 9*Power(t1,3)*Power(t2,2) + 
               19*Power(t1,4)*Power(t2,2) - 4*Power(t1,5)*Power(t2,2) - 
               5*Power(t2,3) + 45*t1*Power(t2,3) - 
               55*Power(t1,2)*Power(t2,3) + 35*Power(t1,3)*Power(t2,3) - 
               4*Power(t1,4)*Power(t2,3) + 
               Power(s2,3)*(-(Power(t1,4)*(-1 + t2)) - 3*(-1 + t2)*t2 + 
                  Power(t1,3)*(-5 + 2*t2 + 3*Power(t2,2)) - 
                  3*Power(t1,2)*(3 - 10*t2 + 7*Power(t2,2)) + 
                  3*t1*(-1 - 6*t2 + 7*Power(t2,2))) + 
               Power(s2,2)*(Power(t1,5)*(-1 + t2) + 
                  Power(t1,4)*(10 + t2 - 3*Power(t2,2)) + 
                  2*t2*(-3 + 4*t2 + Power(t2,2)) + 
                  t1*(5 + 17*t2 - 40*Power(t2,2) - 14*Power(t2,3)) - 
                  2*Power(t1,3)*
                   (-14 + 15*t2 - 10*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-10 - 63*t2 - Power(t2,2) + 14*Power(t2,3))) + 
               s2*(1 - 11*t2 + 8*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t1,5)*(-6 - 3*t2 + Power(t2,2)) + 
                  Power(t1,3)*
                   (8 + 78*t2 - 41*Power(t2,2) - 17*Power(t2,3)) + 
                  Power(t1,4)*
                   (-25 - 8*t2 - 2*Power(t2,2) + 3*Power(t2,3)) + 
                  Power(t1,2)*
                   (56 - 49*t2 + 142*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(-2 + 89*t2 - 44*Power(t2,2) + 17*Power(t2,3))) + 
               Power(s1,3)*(-1 + t1)*
                (3*Power(t1,3)*(-1 + t2) - (-1 + t2)*t2 + 
                  6*t1*(1 + Power(t2,2)) - 
                  Power(t1,2)*(-7 + 10*t2 + Power(t2,2)) + 
                  s2*(1 - t2 + 2*Power(t1,2)*(1 + t2) - 3*t1*(3 + t2))) + 
               Power(s1,2)*(1 - 2*Power(t1,5)*(-1 + t2) + t2 - 
                  5*Power(t2,2) - Power(t2,3) + 
                  Power(t1,4)*(-1 + 8*t2 - 3*Power(t2,2)) + 
                  Power(t1,3)*
                   (-41 + 23*t2 + 25*Power(t2,2) + Power(t2,3)) + 
                  t1*(3 + 3*t2 + 43*Power(t2,2) + 7*Power(t2,3)) - 
                  Power(t1,2)*
                   (12 + 65*t2 + 44*Power(t2,2) + 7*Power(t2,3)) + 
                  Power(s2,2)*
                   (t1*(20 - 8*t2) + 3*(-1 + t2) + 4*Power(t1,3)*t2 - 
                     Power(t1,2)*(1 + 15*t2)) - 
                  s2*(1 - 9*t2 + 4*Power(t2,2) + Power(t1,4)*(3 + t2) - 
                     t1*(8 - 77*t2 + Power(t2,2)) - 
                     4*Power(t1,2)*(-9 + 43*t2 + 2*Power(t2,2)) + 
                     Power(t1,3)*(-16 + 23*t2 + 5*Power(t2,2)))) + 
               s1*(1 + 2*Power(s2,3)*t1*(-1 - 8*t1 + Power(t1,2))*
                   (-1 + t2) - 10*t2 + 4*Power(t2,2) + 5*Power(t2,3) + 
                  Power(t1,5)*(2 - 5*t2 + 3*Power(t2,2)) + 
                  t1*(3 + 58*t2 - 31*Power(t2,2) - 38*Power(t2,3)) + 
                  Power(t1,3)*
                   (107 + 31*t2 - 32*Power(t2,2) - 18*Power(t2,3)) + 
                  Power(t1,4)*
                   (-50 + 32*t2 - 11*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (33 + 6*t2 + 67*Power(t2,2) + 34*Power(t2,3)) + 
                  Power(s2,2)*
                   (7 - 5*Power(t1,4)*(-1 + t2) + 3*t2 - 6*Power(t2,2) + 
                     Power(t1,2)*(32 + 98*t2 - 6*Power(t2,2)) - 
                     Power(t1,3)*(59 - 40*t2 + Power(t2,2)) + 
                     t1*(-33 - 40*t2 + 29*Power(t2,2))) + 
                  s2*(-5 + Power(t1,4)*(41 - 13*t2) + 
                     Power(t1,5)*(-1 + t2) + 3*t2 - 7*Power(t2,2) + 
                     5*Power(t2,3) + 
                     t1*(2 - 4*t2 + 71*Power(t2,2) - 17*Power(t2,3)) + 
                     Power(t1,3)*
                      (-13 - 97*t2 + 41*Power(t2,2) + Power(t2,3)) + 
                     Power(t1,2)*
                      (-40 - 66*t2 - 169*Power(t2,2) + 11*Power(t2,3))))))*
          Log(1 - t1)))/
     ((-1 + s1)*(-1 + s2)*(-s + s2 - t1)*Power(-1 + t1,3)*Power(t1,2)*
       Power(-1 + t2,2)*(s - s1 + t2)) + 
    (8*((-128 + 8*s2 + 160*Power(s2,2) + 20*Power(s2,3) - 
            28*Power(s2,4) - 32*Power(s2,5) + 248*t1 - 208*s2*t1 - 
            260*Power(s2,2)*t1 + 32*Power(s2,3)*t1 + 
            156*Power(s2,4)*t1 + 32*Power(s2,5)*t1 - 80*Power(t1,2) + 
            340*s2*Power(t1,2) + 112*Power(s2,2)*Power(t1,2) - 
            244*Power(s2,3)*Power(t1,2) - 128*Power(s2,4)*Power(t1,2) - 
            100*Power(t1,3) - 208*s2*Power(t1,3) + 
            116*Power(s2,2)*Power(t1,3) + 192*Power(s2,3)*Power(t1,3) + 
            92*Power(t1,4) + 36*s2*Power(t1,4) - 
            128*Power(s2,2)*Power(t1,4) - 32*Power(t1,5) + 
            32*s2*Power(t1,5) - 
            Power(s1,4)*(s2 - t1)*
             (-4 + Power(s2,4) + 3*Power(t1,3) - 
               Power(s2,3)*(18 + 5*t1) - 
               s2*(34*Power(t1,2) + 3*Power(t1,3) + t1*(28 - 6*t2) + 
                  2*(-8 + t2)) + 
               Power(s2,2)*(10 + 49*t1 + 7*Power(t1,2) - 3*t2) + 
               2*t1*(-12 + t2) - 3*Power(t1,2)*(-10 + t2)) + 80*t2 - 
            165*s2*t2 - 147*Power(s2,2)*t2 + 81*Power(s2,3)*t2 - 
            19*Power(s2,4)*t2 + 7*Power(s2,5)*t2 - 16*Power(s2,6)*t2 - 
            107*t1*t2 + 340*s2*t1*t2 + 18*Power(s2,2)*t1*t2 + 
            32*Power(s2,3)*t1*t2 - 29*Power(s2,4)*t1*t2 + 
            43*Power(s2,5)*t1*t2 - 17*Power(t1,2)*t2 - 
            132*s2*Power(t1,2)*t2 + 11*Power(s2,2)*Power(t1,2)*t2 + 
            52*Power(s2,3)*Power(t1,2)*t2 - 
            13*Power(s2,4)*Power(t1,2)*t2 + 49*Power(t1,3)*t2 - 
            2*s2*Power(t1,3)*t2 - 33*Power(s2,2)*Power(t1,3)*t2 - 
            62*Power(s2,3)*Power(t1,3)*t2 + Power(s2,4)*Power(t1,3)*t2 - 
            22*Power(t1,4)*t2 - 13*s2*Power(t1,4)*t2 + 
            72*Power(s2,2)*Power(t1,4)*t2 - 
            3*Power(s2,3)*Power(t1,4)*t2 + 16*Power(t1,5)*t2 - 
            25*s2*Power(t1,5)*t2 + 3*Power(s2,2)*Power(t1,5)*t2 + 
            Power(t1,6)*t2 - s2*Power(t1,6)*t2 + 56*Power(t2,2) + 
            178*s2*Power(t2,2) + 147*Power(s2,2)*Power(t2,2) + 
            28*Power(s2,3)*Power(t2,2) - 33*Power(s2,4)*Power(t2,2) - 
            34*Power(s2,5)*Power(t2,2) + 3*Power(s2,6)*Power(t2,2) - 
            146*t1*Power(t2,2) - 338*s2*t1*Power(t2,2) - 
            234*Power(s2,2)*t1*Power(t2,2) + 
            18*Power(s2,3)*t1*Power(t2,2) + 
            104*Power(s2,4)*t1*Power(t2,2) - 
            12*Power(s2,5)*t1*Power(t2,2) + 
            119*Power(t1,2)*Power(t2,2) + 
            208*s2*Power(t1,2)*Power(t2,2) + 
            47*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
            101*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
            18*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
            Power(s2,5)*Power(t1,2)*Power(t2,2) - 
            18*Power(t1,3)*Power(t2,2) - 6*s2*Power(t1,3)*Power(t2,2) + 
            31*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
            14*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
            3*Power(s2,4)*Power(t1,3)*Power(t2,2) - 
            26*Power(t1,4)*Power(t2,2) - 9*s2*Power(t1,4)*Power(t2,2) + 
            7*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
            3*Power(s2,3)*Power(t1,4)*Power(t2,2) + 
            9*Power(t1,5)*Power(t2,2) - 2*s2*Power(t1,5)*Power(t2,2) - 
            Power(s2,2)*Power(t1,5)*Power(t2,2) - 8*Power(t2,3) - 
            23*s2*Power(t2,3) - 63*Power(s2,2)*Power(t2,3) - 
            64*Power(s2,3)*Power(t2,3) - Power(s2,4)*Power(t2,3) + 
            3*Power(s2,5)*Power(t2,3) + 7*t1*Power(t2,3) + 
            102*s2*t1*Power(t2,3) + 192*Power(s2,2)*t1*Power(t2,3) + 
            10*Power(s2,3)*t1*Power(t2,3) - 
            16*Power(s2,4)*t1*Power(t2,3) - 15*Power(t1,2)*Power(t2,3) - 
            171*s2*Power(t1,2)*Power(t2,3) - 
            34*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
            33*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
            43*Power(t1,3)*Power(t2,3) + 44*s2*Power(t1,3)*Power(t2,3) - 
            30*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
            19*Power(t1,4)*Power(t2,3) + 10*s2*Power(t1,4)*Power(t2,3) + 
            2*s2*Power(t2,4) - 7*Power(s2,2)*Power(t2,4) - 
            6*Power(s2,3)*Power(t2,4) + Power(s2,4)*Power(t2,4) - 
            2*t1*Power(t2,4) + 8*s2*t1*Power(t2,4) + 
            13*Power(s2,2)*t1*Power(t2,4) - 
            3*Power(s2,3)*t1*Power(t2,4) - Power(t1,2)*Power(t2,4) - 
            8*s2*Power(t1,2)*Power(t2,4) + 
            3*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
            Power(t1,3)*Power(t2,4) - s2*Power(t1,3)*Power(t2,4) + 
            2*Power(s,6)*(-7 + 6*t1 + s2*(5 + t1*(-5 + t2) - 2*t2) + 
               4*t2 - 2*t1*t2 + 
               s1*(3 + t1*(-2 + t2) - 3*t2 + s2*(-1 + t1 + t2))) + 
            Power(s1,3)*(-Power(s2,6) + 3*Power(t1,5) + 8*(-1 + t2) + 
               Power(s2,5)*(1 + 5*t1 + 3*t2) + 
               Power(t1,2)*(67 + 26*t2 - Power(t2,2)) - 
               2*t1*(19 - 14*t2 + Power(t2,2)) + 
               Power(t1,4)*(6 - t2 + Power(t2,2)) + 
               Power(t1,3)*(-27 - 76*t2 + 6*Power(t2,2)) - 
               Power(s2,4)*(47 + 6*Power(t1,2) + 48*t2 - Power(t2,2) + 
                  t1*(5 + 17*t2)) + 
               Power(s2,3)*(-3 - 2*Power(t1,3) + 5*t2 - 
                  3*Power(t2,2) + Power(t1,2)*(-8 + 33*t2) + 
                  t1*(106 + 174*t2 - 4*Power(t2,2))) + 
               s2*(-3*Power(t1,5) + Power(t1,4)*(-21 + 8*t2) - 
                  4*t1*(27 + 16*t2) + 
                  Power(t1,2)*(39 + 149*t2 - 15*Power(t2,2)) - 
                  4*Power(t1,3)*(-5 - 20*t2 + Power(t2,2)) + 
                  2*(11 - 6*t2 + Power(t2,2))) + 
               Power(s2,2)*(65 + 7*Power(t1,4) + 
                  Power(t1,3)*(30 - 27*t2) + 14*t2 + Power(t2,2) + 
                  3*t1*(-3 - 26*t2 + 4*Power(t2,2)) + 
                  Power(t1,2)*(-85 - 205*t2 + 6*Power(t2,2)))) + 
            Power(s1,2)*(Power(t1,6) + Power(t1,5)*t2 + 
               Power(s2,6)*(-15 + 4*t2) + 
               Power(s2,5)*(Power(t1,2) + t1*(44 - 19*t2) + 
                  (-34 + t2)*t2) + t1*(229 - 116*t2 - 13*Power(t2,2)) - 
               8*(5 - 7*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(-21 + 76*t2 + 73*Power(t2,2) - 
                  2*Power(t2,3)) - 
               Power(t1,4)*(-43 + 28*t2 + 14*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(t1,2)*(220 - 13*t2 + 15*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,4)*(42 - 2*Power(t1,3) + 31*t2 + 
                  23*Power(t2,2) + Power(t1,2)*(-41 + 28*t2) + 
                  t1*(-25 + 126*t2)) - 
               Power(s2,3)*(-137 - 50*t2 + 57*Power(t2,2) + 
                  6*Power(t2,3) + 2*Power(t1,3)*(-2 + 5*t2) + 
                  2*Power(t1,2)*(-13 + 68*t2 + 3*Power(t2,2)) + 
                  t1*(71 + 189*t2 + 77*Power(t2,2) - Power(t2,3))) + 
               Power(s2,2)*(-98 + 2*Power(t1,5) + 
                  Power(t1,4)*(15 - 8*t2) + 91*t2 - 59*Power(t2,2) - 
                  10*Power(t2,3) + 
                  Power(t1,3)*(15 + 31*t2 + 8*Power(t2,2)) + 
                  Power(t1,2)*
                   (53 + 291*t2 + 71*Power(t2,2) - 3*Power(t2,3)) + 
                  2*t1*(-93 - 114*t2 + 107*Power(t2,2) + 5*Power(t2,3))\
) - s2*(229 + Power(t1,6) + Power(t1,5)*(8 - 5*t2) - 148*t2 + 
                  19*Power(t2,2) + 
                  Power(t1,4)*(16 - 12*t2 + 3*Power(t2,2)) + 
                  Power(t1,3)*
                   (67 + 105*t2 + 3*Power(t2,2) - 3*Power(t2,3)) + 
                  2*Power(t1,2)*
                   (-43 - 43*t2 + 115*Power(t2,2) + Power(t2,3)) - 
                  2*t1*(171 - 88*t2 + 61*Power(t2,2) + 6*Power(t2,3)))) \
+ s1*(-(Power(t1,6)*(1 + t2)) + 
               Power(t1,5)*(20 - 13*t2 - 4*Power(t2,2)) + 
               Power(s2,6)*(16 + 12*t2 - 3*Power(t2,2)) + 
               8*(22 - 18*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               Power(t1,4)*(-46 - 45*t2 + 45*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(485 - 275*t2 - 77*Power(t2,2) + 
                  14*Power(t2,3) + Power(t2,4)) - 
               Power(t1,3)*(125 - 215*t2 + 92*Power(t2,2) + 
                  28*Power(t2,3) + Power(t2,4)) + 
               t1*(-509 + 277*t2 + 155*Power(t2,2) - 9*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,5)*(29 + 30*t2 - 2*Power(t1,2)*t2 + 
                  30*Power(t2,2) - 3*Power(t2,3) + 
                  t1*(-79 - 28*t2 + 14*Power(t2,2))) + 
               Power(s2,4)*(15 + 27*t2 + 13*Power(t2,2) + 
                  6*Power(t2,3) - Power(t2,4) + 
                  Power(t1,3)*(-1 + 5*t2) + 
                  Power(t1,2)*(157 + 7*t2 - 22*Power(t2,2)) + 
                  t1*(-111 - 99*t2 - 101*Power(t2,2) + 11*Power(t2,3))) \
+ Power(s2,2)*(-105 + 39*t2 - 89*Power(t2,2) + 68*Power(t2,3) + 
                  7*Power(t2,4) - Power(t1,5)*(3 + t2) + 
                  Power(t1,4)*(72 - 38*t2 + Power(t2,2)) + 
                  Power(t1,3)*
                   (9 - 150*t2 - 19*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(546 + 140*t2 + 21*Power(t2,2) - 
                     187*Power(t2,3) - 13*Power(t2,4)) + 
                  Power(t1,2)*
                   (-443 + 212*t2 - 164*Power(t2,2) + 48*Power(t2,3) - 
                     3*Power(t2,4))) + 
               Power(s2,3)*(-221 - 3*Power(t1,4)*(-1 + t2) - 53*t2 + 
                  25*Power(t2,2) + 68*Power(t2,3) + 6*Power(t2,4) + 
                  2*Power(t1,3)*(-77 + 17*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*
                   (116 + 163*t2 + 99*Power(t2,2) - 15*Power(t2,3)) + 
                  t1*(156 - 171*t2 + 77*Power(t2,2) - 27*Power(t2,3) + 
                     3*Power(t2,4))) + 
               s2*(269 - 53*t2 - 155*Power(t2,2) + 25*Power(t2,3) - 
                  2*Power(t2,4) + Power(t1,6)*(1 + t2) + 
                  Power(t1,5)*(-11 + 14*t2 - 2*Power(t2,2)) - 
                  Power(t1,4)*
                   (63 - 69*t2 + 5*Power(t2,2) + 2*Power(t2,3)) + 
                  Power(t1,3)*
                   (318 - 23*t2 + 29*Power(t2,2) - 39*Power(t2,3) + 
                     Power(t2,4)) - 
                  2*t1*(150 - 78*t2 - 95*Power(t2,2) + 
                     53*Power(t2,3) + 4*Power(t2,4)) + 
                  Power(t1,2)*
                   (-216 - 302*t2 + 62*Power(t2,2) + 147*Power(t2,3) + 
                     8*Power(t2,4)))) + 
            Power(s,5)*(66 - 81*t1 + 42*Power(t1,2) + Power(t1,3) - 
               15*t2 + 25*t1*t2 - 15*Power(t1,2)*t2 + 15*Power(t2,2) - 
               7*t1*Power(t2,2) + Power(t2,3) + 
               Power(s2,2)*(-54 + t1*(39 - 8*t2) + 19*t2) + 
               Power(s1,2)*(-4 + 2*Power(s2,2) + t1*(11 - 4*t2) + 
                  11*t2 - s2*(6 + 7*t1 + 3*t2)) - 
               s2*(-30 + Power(t1,3) - 9*Power(t1,2)*(-4 + t2) + 
                  61*t2 + Power(t2,2) + t1*(-33 + 3*t2 - 2*Power(t2,2))\
) - s1*(Power(t1,2)*(9 - 6*t2) + Power(s2,2)*(-20 + 7*t1 + 11*t2) + 
                  t1*(26 + 12*t2 - 5*Power(t2,2)) + 
                  t2*(-4 + 13*t2 + Power(t2,2)) + 
                  s2*(54 - 3*Power(t1,2) - 56*t2 + Power(t2,2) - 
                     2*t1*(19 + t2)))) + 
            Power(s,4)*(-93 + 250*t1 - 194*Power(t1,2) + 
               60*Power(t1,3) + 3*Power(t1,4) + 109*t2 + 18*t1*t2 + 
               41*Power(t1,2)*t2 - 20*Power(t1,3)*t2 - 14*Power(t2,2) + 
               6*t1*Power(t2,2) - 24*Power(t1,2)*Power(t2,2) + 
               13*Power(t2,3) + Power(t2,4) + 
               Power(s1,3)*(-8 - 3*Power(s2,2) + 2*t1*(-5 + t2) - 
                  5*t2 + s2*(15 + 8*t1 + t2)) + 
               Power(s2,3)*(118 - 36*t2 + 3*t1*(-19 + 4*t2)) + 
               Power(s2,2)*(23 + 3*Power(t1,3) + 145*t2 + 
                  7*Power(t2,2) - 4*Power(t1,2)*(-27 + 7*t2) + 
                  t1*(-248 + 54*t2 - 8*Power(t2,2))) + 
               s2*(-215 - 3*Power(t1,4) + 34*t2 - 91*Power(t2,2) - 
                  Power(t2,3) + Power(t1,3)*(-57 + 14*t2) + 
                  Power(t1,2)*(76 + 12*t2 + 9*Power(t2,2)) + 
                  t1*(122 - 181*t2 + 32*Power(t2,2))) + 
               Power(s1,2)*(55 - 7*Power(s2,3) - 23*t2 + 
                  17*Power(t2,2) + Power(t2,3) - 
                  3*Power(t1,2)*(-9 + 4*t2) + 
                  2*Power(s2,2)*(5 + 15*t1 + 9*t2) + 
                  t1*(73 + 22*t2 - 6*Power(t2,2)) - 
                  s2*(-25 + 15*Power(t1,2) + 99*t2 - 3*Power(t2,2) + 
                     t1*(110 + 9*t2))) + 
               s1*(-96 - 67*t2 + 12*Power(t2,2) - 14*Power(t2,3) - 
                  Power(t2,4) + Power(t1,3)*(-5 + 6*t2) + 
                  3*Power(s2,3)*(-21 + 3*t1 + 8*t2) - 
                  6*t1*(-3 + 11*t2 + Power(t2,2)) + 
                  5*Power(t1,2)*(-16 - t2 + 3*Power(t2,2)) + 
                  Power(s2,2)*
                   (138 - 5*Power(t1,2) - 158*t2 + Power(t2,2) - 
                     t1*(56 + 31*t2)) + 
                  s2*(101 - Power(t1,3) + Power(t1,2)*(83 - 8*t2) + 
                     57*t2 + 65*Power(t2,2) + Power(t2,3) + 
                     t1*(-29 + 179*t2 - 18*Power(t2,2))))) + 
            Power(s,3)*(-43 - 121*t1 + 337*Power(t1,2) - 
               223*Power(t1,3) + 43*Power(t1,4) + 3*Power(t1,5) + 
               Power(s1,4)*(-1 + s2)*(s2 - 3*(2 + t1)) - 100*t2 + 
               151*t1*t2 + 42*Power(t1,2)*t2 + 60*Power(t1,3)*t2 - 
               10*Power(t1,4)*t2 + 94*Power(t2,2) + 99*t1*Power(t2,2) - 
               55*Power(t1,2)*Power(t2,2) - 
               30*Power(t1,3)*Power(t2,2) - 20*Power(t2,3) + 
               6*t1*Power(t2,3) - 6*Power(t1,2)*Power(t2,3) + 
               6*Power(t2,4) + 3*t1*Power(t2,4) + 
               Power(s2,4)*(-130 + t1*(37 - 8*t2) + 34*t2) + 
               Power(s2,2)*(241 + 6*Power(t1,4) - 79*t2 + 
                  217*Power(t2,2) - 6*Power(t2,3) - 
                  5*Power(t1,3)*(-23 + 6*t2) + 
                  t1*(78 + 309*t2 - 42*Power(t2,2)) + 
                  Power(t1,2)*(-431 + 75*t2 - 28*Power(t2,2))) - 
               Power(s2,3)*(105 + 3*Power(t1,3) + 133*t2 + 
                  18*Power(t2,2) - 6*Power(t1,2)*(-18 + 5*t2) - 
                  4*t1*(106 - 26*t2 + 3*Power(t2,2))) - 
               s2*(-313 + 3*Power(t1,5) + Power(t1,4)*(47 - 8*t2) + 
                  163*t2 + 66*Power(t2,2) + 38*Power(t2,3) + 
                  4*Power(t2,4) + 
                  Power(t1,2)*(-263 + 218*t2 - 99*Power(t2,2)) - 
                  3*Power(t1,3)*(32 + 5*Power(t2,2)) + 
                  t1*(610 + 92*t2 + 158*Power(t2,2) - 16*Power(t2,3))) \
+ Power(s1,3)*(-41 + 10*Power(s2,3) + 13*t2 - 5*Power(t2,2) + 
                  3*Power(t1,2)*(-9 + 2*t2) - 
                  Power(s2,2)*(62 + 35*t1 + 6*t2) + 
                  t1*(-101 - 3*t2 + Power(t2,2)) + 
                  s2*(51 + 21*Power(t1,2) + 41*t2 - Power(t2,2) + 
                     t1*(133 + 5*t2))) + 
               Power(s1,2)*(-64 + 9*Power(s2,4) + 
                  Power(s2,3)*(26 - 45*t1 - 40*t2) + 154*t2 - 
                  41*Power(t2,2) + 11*Power(t2,3) - 
                  4*Power(t1,3)*(-4 + 3*t2) - 
                  18*Power(t1,2)*(-8 - t2 + Power(t2,2)) + 
                  t1*(105 + 119*t2 + 7*Power(t2,2) + 2*Power(t2,3)) + 
                  Power(s2,2)*
                   (-11 + 38*Power(t1,2) + 278*t2 - 10*Power(t2,2) + 
                     t1*(175 + 76*t2)) - 
                  s2*(234 + 4*Power(t1,3) + 14*t2 + 61*Power(t2,2) + 
                     3*Power(t2,3) + 2*Power(t1,2)*(83 + 8*t2) + 
                     t1*(126 + 337*t2 - 24*Power(t2,2)))) + 
               s1*(298 + Power(s2,4)*(89 - 5*t1 - 26*t2) - 94*t2 - 
                  58*Power(t2,2) + 14*Power(t2,3) - 6*Power(t2,4) + 
                  Power(t1,4)*(-1 + 2*t2) + 
                  Power(s2,3)*
                   (-178 - 21*t1 + 186*t2 + 61*t1*t2 + 6*Power(t2,2)) + 
                  Power(t1,3)*(-70 + 3*t2 + 15*Power(t2,2)) + 
                  Power(t1,2)*
                   (22 - 104*t2 + 37*Power(t2,2) + 6*Power(t2,3)) - 
                  t1*(286 + 245*t2 + 46*Power(t2,2) + 13*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  Power(s2,2)*
                   (-273 + 6*Power(t1,3) - 173*t2 - 147*Power(t2,2) + 
                     6*Power(t2,3) - Power(t1,2)*(127 + 13*t2) + 
                     t1*(279 - 437*t2 + 10*Power(t2,2))) + 
                  s2*(-Power(t1,4) + Power(t1,3)*(67 - 21*t2) + 
                     Power(t1,2)*(34 + 180*t2 - 35*Power(t2,2)) + 
                     t1*(119 + 505*t2 + 93*Power(t2,2) - 
                        11*Power(t2,3)) + 
                     2*(52 + 61*t2 + 45*Power(t2,2) + 18*Power(t2,3) + 
                        2*Power(t2,4))))) + 
            s*(72 + 101*t1 - 357*Power(t1,2) + 257*Power(t1,3) - 
               58*Power(t1,4) - 16*Power(t1,5) + Power(t1,6) + 
               Power(s1,4)*(-4 + 3*Power(s2,4) + 9*Power(t1,3) - 
                  Power(s2,3)*(43 + 15*t1) - 
                  s2*(81*Power(t1,2) + 9*Power(t1,3) + 
                     t1*(90 - 12*t2) + 2*(-8 + t2)) + 
                  Power(s2,2)*(36 + 115*t1 + 21*Power(t1,2) - 6*t2) + 
                  2*t1*(-12 + t2) - 6*Power(t1,2)*(-11 + t2)) + 37*t2 - 
               194*t1*t2 + 144*Power(t1,2)*t2 - 7*Power(t1,3)*t2 - 
               10*Power(t1,4)*t2 + 23*Power(t1,5)*t2 + Power(t1,6)*t2 - 
               106*Power(t2,2) + 99*t1*Power(t2,2) - 
               51*Power(t1,2)*Power(t2,2) + 
               57*Power(t1,3)*Power(t2,2) - 4*Power(t1,4)*Power(t2,2) - 
               3*Power(t1,5)*Power(t2,2) - Power(t2,3) + 
               2*t1*Power(t2,3) + 87*Power(t1,2)*Power(t2,3) - 
               58*Power(t1,3)*Power(t2,3) - 3*Power(t1,4)*Power(t2,3) - 
               2*Power(t2,4) - 8*t1*Power(t2,4) + 
               8*Power(t1,2)*Power(t2,4) + Power(t1,3)*Power(t2,4) + 
               Power(s2,6)*(-16 + 3*t2) + 
               Power(s2,5)*(-25 + 38*t2 + Power(t1,2)*t2 - 
                  13*Power(t2,2) + t1*(75 - 21*t2 + 2*Power(t2,2))) - 
               Power(s2,4)*(-81 + 70*t2 - 148*Power(t2,2) + 
                  11*Power(t2,3) + Power(t1,3)*(-1 + 2*t2) + 
                  t1*(1 + 64*t2 - 25*Power(t2,2)) + 
                  3*Power(t1,2)*(47 - 18*t2 + 4*Power(t2,2))) + 
               Power(s2,3)*(153 - 3*Power(t1,4) + 27*t2 - 
                  28*Power(t2,2) - 10*Power(t2,3) - 4*Power(t2,4) + 
                  Power(t1,2)*(192 - 31*t2 + 15*Power(t2,2)) + 
                  Power(t1,3)*(130 - 74*t2 + 21*Power(t2,2)) + 
                  2*t1*(-186 + 56*t2 - 177*Power(t2,2) + 
                     24*Power(t2,3))) + 
               Power(s2,2)*(-87 - 14*Power(t1,4)*Power(-2 + t2,2) - 
                  110*t2 - 118*Power(t2,2) + 108*Power(t2,3) + 
                  18*Power(t2,4) + Power(t1,5)*(3 + 2*t2) + 
                  Power(t1,3)*(-301 + 121*t2 - 52*Power(t2,2)) + 
                  Power(t1,2)*
                   (483 - 50*t2 + 234*Power(t2,2) - 72*Power(t2,3)) + 
                  t1*(-118 - 162*t2 + 349*Power(t2,2) - 
                     14*Power(t2,3) + 9*Power(t2,4))) - 
               s2*(357 - 356*t2 + 143*Power(t2,2) - 46*Power(t2,3) - 
                  14*Power(t2,4) + Power(t1,6)*(1 + t2) + 
                  Power(t1,4)*(-151 + 87*t2 - 28*Power(t2,2)) + 
                  Power(t1,5)*(-7 + 19*t2 - 3*Power(t2,2)) + 
                  Power(t1,3)*
                   (134 - 22*t2 + 20*Power(t2,2) - 38*Power(t2,3)) + 
                  Power(t1,2)*
                   (276 - 86*t2 + 388*Power(t2,2) - 80*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  t1*(-612 + 245*t2 - 361*Power(t2,2) + 
                     216*Power(t2,3) + 26*Power(t2,4))) + 
               Power(s1,3)*(6*Power(s2,5) + Power(t1,4)*(-1 + 2*t2) - 
                  Power(s2,4)*(34 + 29*t1 + 10*t2) - 
                  2*(23 - 18*t2 + Power(t2,2)) + 
                  Power(t1,3)*(-73 + 5*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*(-4 - 154*t2 + 7*Power(t2,2)) + 
                  t1*(165 - 33*t2 + 8*Power(t2,2)) + 
                  Power(s2,3)*
                   (113 + 39*Power(t1,2) + 127*t2 - 3*Power(t2,2) + 
                     t1*(155 + 43*t2)) - 
                  Power(s2,2)*
                   (43 + 15*Power(t1,3) + 17*t2 - Power(t2,2) + 
                     Power(t1,2)*(181 + 54*t2) + 
                     t1*(201 + 307*t2 - 9*Power(t2,2))) + 
                  s2*(-163 - Power(t1,4) + 45*t2 - 10*Power(t2,2) + 
                     Power(t1,3)*(61 + 19*t2) + 
                     Power(t1,2)*(181 + 175*t2 - 9*Power(t2,2)) + 
                     t1*(35 + 163*t2 - 8*Power(t2,2)))) + 
               Power(s1,2)*(205 + Power(s2,6) - 3*Power(t1,5) + 
                  Power(s2,5)*(58 - 6*t1 - 21*t2) - 76*t2 - 
                  29*Power(t2,2) + 
                  Power(t1,4)*(-14 + 9*t2 - 6*Power(t2,2)) + 
                  Power(t1,3)*
                   (197 + 85*t2 - 55*Power(t2,2) - 2*Power(t2,3)) + 
                  Power(t1,2)*
                   (-94 + 112*t2 + 143*Power(t2,2) + 7*Power(t2,3)) - 
                  t1*(387 + 80*t2 - 59*Power(t2,2) + 18*Power(t2,3)) + 
                  Power(s2,4)*
                   (33 + 3*Power(t1,2) + 181*t2 - 6*Power(t2,2) + 
                     2*t1*(-55 + 42*t2)) + 
                  Power(s2,3)*
                   (-168 + 11*Power(t1,3) - 108*t2 - 73*Power(t2,2) - 
                     Power(t2,3) - 34*Power(t1,2)*(-2 + 3*t2) + 
                     t1*(-64 - 571*t2 + 12*Power(t2,2))) + 
                  Power(s2,2)*
                   (-339 - 12*Power(t1,4) - 45*t2 + 87*Power(t2,2) + 
                     23*Power(t2,3) + Power(t1,3)*(-33 + 36*t2) + 
                     Power(t1,2)*(155 + 530*t2 - 12*Power(t2,2)) + 
                     t1*(234 + 538*t2 + 135*Power(t2,2))) + 
                  s2*(281 + 3*Power(t1,5) - 14*t2 - 15*Power(t2,2) + 
                     26*Power(t2,3) + Power(t1,4)*(20 + 3*t2) + 
                     Power(t1,3)*(-102 - 149*t2 + 12*Power(t2,2)) + 
                     t1*(292 + 169*t2 - 257*Power(t2,2) - 
                        30*Power(t2,3)) + 
                     Power(t1,2)*
                      (-257 - 549*t2 - 7*Power(t2,2) + 3*Power(t2,3)))) \
- s1*(301 + 2*Power(t1,6) + 3*Power(s2,6)*(-5 + t2) - 85*t2 - 
                  131*Power(t2,2) + Power(t2,3) - 2*Power(t2,4) + 
                  Power(t1,5)*(14 + t2) + 
                  Power(s2,5)*
                   (72 + Power(t1,2) + t1*(35 - 13*t2) + 10*t2 - 
                     11*Power(t2,2)) - 
                  Power(t1,4)*
                   (81 + 3*t2 + 2*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(-387 + 25*t2 + 136*Power(t2,2) - 
                     10*Power(t2,3) - 8*Power(t2,4)) + 
                  Power(t1,3)*
                   (230 + 313*t2 - 48*Power(t2,2) - 39*Power(t2,3) + 
                     Power(t2,4)) + 
                  Power(t1,2)*
                   (-87 - 355*t2 + 165*Power(t2,2) + 65*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  Power(s2,4)*
                   (145 - Power(t1,3) + 107*t2 + 116*Power(t2,2) - 
                     11*Power(t2,3) + Power(t1,2)*(-5 + 7*t2) + 
                     t1*(-333 + 59*t2 + 39*Power(t2,2))) - 
                  Power(s2,3)*
                   (3*Power(t1,4) + Power(t1,3)*(56 - 18*t2) + 
                     3*Power(t1,2)*(-172 + 46*t2 + 13*Power(t2,2)) + 
                     t1*(357 + 473*t2 + 283*Power(t2,2) - 
                        33*Power(t2,3)) + 
                     4*(-24 - 4*t2 + 22*Power(t2,2) - Power(t2,3) + 
                        Power(t2,4))) + 
                  s2*(-203 - 2*Power(t1,6) + 5*Power(t1,5)*(-5 + t2) + 
                     305*t2 - 164*Power(t2,2) + 64*Power(t2,3) + 
                     14*Power(t2,4) + 
                     Power(t1,4)*(46 - 4*t2 + 6*Power(t2,2)) + 
                     Power(t1,3)*
                      (141 - 347*t2 + 9*Power(t2,2) + 17*Power(t2,3)) \
+ t1*(995 + 111*t2 + 62*Power(t2,2) - 210*Power(t2,3) - 
                        26*Power(t2,4)) - 
                     2*Power(t1,2)*
                      (382 + 43*t2 + 183*Power(t2,2) - 
                        38*Power(t2,3) + 3*Power(t2,4))) + 
                  Power(s2,2)*
                   (-663 + 5*Power(t1,5) + Power(t1,4)*(64 - 20*t2) + 
                     15*t2 - 32*Power(t2,2) + 122*Power(t2,3) + 
                     18*Power(t2,4) + 
                     Power(t1,3)*(-315 + 72*t2 + 5*Power(t2,2)) + 
                     4*Power(t1,2)*
                      (39 + 182*t2 + 40*Power(t2,2) - 9*Power(t2,3)) + 
                     t1*(414 - 263*t2 + 486*Power(t2,2) - 
                        41*Power(t2,3) + 9*Power(t2,4))))) + 
            Power(s,2)*(141 - 308*t1 + 137*Power(t1,2) + 
               127*Power(t1,3) - 112*Power(t1,4) + 14*Power(t1,5) + 
               Power(t1,6) + Power(s1,4)*
                (-3*Power(s2,3) + 4*Power(s2,2)*(8 + 3*t1) - 
                  s2*(32 + 51*t1 + 9*Power(t1,2) - 3*t2) + 
                  3*t1*(14 + 3*t1 - t2)) - 121*t2 + 3*t1*t2 + 
               57*Power(t1,2)*t2 - 17*Power(t1,3)*t2 + 
               58*Power(t1,4)*t2 - 36*Power(t2,2) + 61*t1*Power(t2,2) + 
               196*Power(t1,2)*Power(t2,2) - 
               59*Power(t1,3)*Power(t2,2) - 16*Power(t1,4)*Power(t2,2) + 
               17*Power(t2,3) + 24*t1*Power(t2,3) - 
               46*Power(t1,2)*Power(t2,3) - 8*Power(t1,3)*Power(t2,3) - 
               7*Power(t2,4) + 13*t1*Power(t2,4) + 
               3*Power(t1,2)*Power(t2,4) + 
               Power(s2,5)*(-8 + t1)*(-9 + 2*t2) + 
               Power(s2,4)*(91 + Power(t1,3) - 
                  12*Power(t1,2)*(-3 + t2) + 19*t2 + 22*Power(t2,2) + 
                  t1*(-296 + 78*t2 - 8*Power(t2,2))) + 
               Power(s2,3)*(-141 - 3*Power(t1,4) + 123*t2 - 
                  255*Power(t2,2) + 14*Power(t2,3) + 
                  6*Power(t1,3)*(-10 + 3*t2) + 
                  2*t1*(-75 - 66*t2 + 2*Power(t2,2)) + 
                  2*Power(t1,2)*(227 - 63*t2 + 15*Power(t2,2))) + 
               Power(s2,2)*(-345 + 3*Power(t1,5) + 
                  Power(t1,4)*(49 - 8*t2) + 46*t2 + 141*Power(t2,2) + 
                  36*Power(t2,3) + 6*Power(t2,4) + 
                  Power(t1,2)*(-121 + 221*t2 - 108*Power(t2,2)) - 
                  3*Power(t1,3)*(98 - 31*t2 + 11*Power(t2,2)) + 
                  t1*(568 - 9*t2 + 402*Power(t2,2) - 48*Power(t2,3))) - 
               s2*(-118 + 17*Power(t1,5) + Power(t1,6) - 121*t2 + 
                  4*Power(t2,2) + 24*Power(t2,3) + 18*Power(t2,4) + 
                  Power(t1,4)*(-50 + 29*t2 - 11*Power(t2,2)) - 
                  8*Power(t1,3)*(37 - 20*t2 + 12*Power(t2,2)) + 
                  Power(t1,2)*
                   (594 + 94*t2 + 78*Power(t2,2) - 45*Power(t2,3)) + 
                  t1*(-229 - 53*t2 + 466*Power(t2,2) + 2*Power(t2,3) + 
                     9*Power(t2,4))) + 
               Power(s1,3)*(98 - 12*Power(s2,4) - 59*t2 + 
                  9*Power(t2,2) + 3*Power(t1,3)*(-7 + 2*t2) + 
                  Power(s2,3)*(80 + 51*t1 + 12*t2) + 
                  Power(t1,2)*(-172 + 8*t2 + 3*Power(t2,2)) - 
                  t1*(18 + 65*t2 + 4*Power(t2,2)) - 
                  Power(s2,2)*
                   (109 + 54*Power(t1,2) + 115*t2 - 3*Power(t2,2) + 
                     3*t1*(91 + 11*t2)) + 
                  s2*(87 + 15*Power(t1,3) - t2 + 7*Power(t2,2) + 
                     5*Power(t1,2)*(40 + 3*t2) + 
                     t1*(212 + 136*t2 - 6*Power(t2,2)))) - 
               Power(s1,2)*(151 + 5*Power(s2,5) + 
                  Power(s2,4)*(73 - 28*t1 - 42*t2) + 109*t2 - 
                  74*Power(t2,2) + 16*Power(t2,3) + 
                  4*Power(t1,4)*(1 + t2) + 
                  51*Power(t1,2)*(-4 - 5*t2 + Power(t2,2)) + 
                  Power(t1,3)*(-53 - 15*t2 + 18*Power(t2,2)) - 
                  t1*(-137 + 190*t2 + 29*Power(t2,2) + 
                     20*Power(t2,3)) + 
                  Power(s2,3)*
                   (43 + 27*Power(t1,2) + 337*t2 - 12*Power(t2,2) + 
                     2*t1*(5 + 64*t2)) + 
                  Power(s2,2)*
                   (-305 + 4*Power(t1,3) - 114*t2 - 94*Power(t2,2) - 
                     3*Power(t2,3) - 3*Power(t1,2)*(39 + 34*t2) + 
                     2*t1*(-68 - 380*t2 + 15*Power(t2,2))) + 
                  s2*(-266 - 8*Power(t1,4) + 159*t2 - 11*Power(t2,2) + 
                     28*Power(t2,3) + 2*Power(t1,3)*(17 + 6*t2) + 
                     Power(t1,2)*(237 + 399*t2 - 36*Power(t2,2)) + 
                     t1*(308 + 482*t2 + 65*Power(t2,2) + 3*Power(t2,3)))\
) + s1*(-74 - 3*Power(t1,5) + 210*t2 - 43*Power(t2,2) - 4*Power(t2,3) + 
                  7*Power(t2,4) + Power(s2,5)*(-59 + t1 + 14*t2) + 
                  Power(s2,4)*
                   (144 + 78*t1 + 3*Power(t1,2) - 80*t2 - 47*t1*t2 - 
                     14*Power(t2,2)) + 
                  Power(t1,4)*(-35 + 2*t2 + 5*Power(t2,2)) + 
                  Power(t1,3)*
                   (65 - 18*t2 + 36*Power(t2,2) + 8*Power(t2,3)) + 
                  t1*(518 + 38*t2 - 131*Power(t2,2) - 23*Power(t2,3) - 
                     13*Power(t2,4)) - 
                  Power(t1,2)*
                   (374 + 446*t2 + 55*Power(t2,2) - 28*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  Power(s2,3)*
                   (288 - 6*Power(t1,3) + 189*t2 + 181*Power(t2,2) - 
                     14*Power(t2,3) + 24*Power(t1,2)*(2 + t2) + 
                     t1*(-478 + 357*t2 + 28*Power(t2,2))) - 
                  Power(s2,2)*
                   (-73 + Power(t1,4) - 28*Power(t1,3)*(-4 + t2) + 
                     66*t2 + 203*Power(t2,2) + 24*Power(t2,3) + 
                     6*Power(t2,4) + 
                     Power(t1,2)*(-385 + 320*t2 - 3*Power(t2,2)) + 
                     t1*(367 + 813*t2 + 269*Power(t2,2) - 
                        33*Power(t2,3))) + 
                  s2*(-748 + 3*Power(t1,5) + Power(t1,4)*(48 - 19*t2) + 
                     170*t2 + Power(t2,2) + 40*Power(t2,3) + 
                     18*Power(t2,4) + 
                     Power(t1,3)*(-26 + 47*t2 - 22*Power(t2,2)) + 
                     Power(t1,2)*
                      (-5 + 715*t2 + 24*Power(t2,2) - 27*Power(t2,3)) + 
                     t1*(530 + 95*t2 + 455*Power(t2,2) - Power(t2,3) + 
                        9*Power(t2,4))))))/
          ((-1 + s1)*(-1 + s2)*(1 - s + s2 - t1)*(-1 + t1)*(-1 + t2)) - 
         ((s - s2 + t1)*(s - s1 + t2)*
            ((-2*(s - s2 + t1)*
                 (2*Power(s,4) - 4*s1 - 8*Power(s1,2) - 20*s2 - 
                   10*s1*s2 - 10*Power(s1,2)*s2 - 34*Power(s2,2) - 
                   3*Power(s1,2)*Power(s2,2) - 8*Power(s2,3) + 
                   6*s1*Power(s2,3) - Power(s1,2)*Power(s2,3) + 
                   4*Power(s2,4) - s1*Power(s2,4) + 16*t1 - 6*s1*t1 + 
                   14*Power(s1,2)*t1 + 60*s2*t1 - 14*s1*s2*t1 + 
                   10*Power(s1,2)*s2*t1 + 23*Power(s2,2)*t1 - 
                   15*s1*Power(s2,2)*t1 + 
                   2*Power(s1,2)*Power(s2,2)*t1 - 14*Power(s2,3)*t1 + 
                   2*s1*Power(s2,3)*t1 - 34*Power(t1,2) + 
                   18*s1*Power(t1,2) - 7*Power(s1,2)*Power(t1,2) - 
                   32*s2*Power(t1,2) + 16*s1*s2*Power(t1,2) - 
                   Power(s1,2)*s2*Power(t1,2) + 
                   17*Power(s2,2)*Power(t1,2) - 
                   s1*Power(s2,2)*Power(t1,2) + 17*Power(t1,3) - 
                   7*s1*Power(t1,3) - 8*s2*Power(t1,3) + Power(t1,4) + 
                   Power(s,3)*(-9*s2 + s1*(2 + s2) + 11*t1) - 
                   Power(s,2)*
                    (-2 - 16*Power(s2,2) + Power(s1,2)*(7 + s2) - 
                      5*t1 - 17*Power(t1,2) + s2*(4 + 34*t1) + 
                      s1*(2 + 3*Power(s2,2) + 3*t1 - 2*s2*(1 + t1))) + 
                   s*(-4 - 13*Power(s2,3) + 
                      2*Power(s1,2)*
                       (7 + Power(s2,2) - s2*(-5 + t1) - 7*t1) - 
                      24*t1 + 22*Power(t1,2) + 9*Power(t1,3) + 
                      Power(s2,2)*(12 + 37*t1) + 
                      s2*(28 - 24*t1 - 33*Power(t1,2)) + 
                      s1*(6 + 3*Power(s2,3) + 16*t1 - 
                        12*Power(t1,2) - 2*Power(s2,2)*(5 + 2*t1) + 
                        s2*(2 + 18*t1 + Power(t1,2)))) + 
                   (-8 - Power(s,3) - 14*s2 - 5*Power(s2,2) + 
                      Power(s2,3) + 
                      Power(s,2)*(-5 + s1 + 3*s2 - 2*t1) + 10*t1 + 
                      6*s2*t1 - 2*Power(s2,2)*t1 - Power(t1,2) + 
                      s2*Power(t1,2) + 
                      s1*(-4 + Power(s2,2) + 4*t1 + Power(t1,2) - 
                       2*s2*(2 + t1)) + 
                      s*(14 + 10*s2 - 3*Power(s2,2) - 6*t1 + 
                        4*s2*t1 - Power(t1,2) + s1*(4 - 2*s2 + 2*t1))\
)*Power(s1 - s2 + t1 - t2,2) + 
                   (8 + 26*s2 + 21*Power(s2,2) - Power(s2,4) + 
                      Power(s,3)*(5 - s1 + s2) - 26*t1 - 38*s2*t1 + 
                      2*Power(s2,2)*t1 + 2*Power(s2,3)*t1 + 
                      21*Power(t1,2) - Power(s2,2)*Power(t1,2) - 
                      2*Power(t1,3) + 
                      Power(s,2)*
                       (7 + Power(s1,2) - 3*Power(s2,2) + 
                        2*s1*(-7 + s2 - t1) + 2*s2*(-5 + t1) + 8*t1) \
- 2*s1*(4 + 5*Power(s2,2) + s2*(8 - 10*t1) - 8*t1 + 5*Power(t1,2)) + 
                      Power(s1,2)*
                       (-4 + Power(s2,2) + 4*t1 + Power(t1,2) - 
                        2*s2*(2 + t1)) + 
                      s*(-18 + 3*Power(s2,3) + 
                        Power(s2,2)*(5 - 4*t1) + 28*t1 + 
                        Power(t1,2) + Power(s1,2)*(4 - 2*s2 + 2*t1) + 
                        s2*(-28 - 10*t1 + Power(t1,2)) - 
                        s1*(-20 + Power(s2,2) + 24*t1 + Power(t1,2) - 
                        2*s2*(12 + t1))))*(-s1 + s2 - t1 + t2)))/
               ((-1 + t1)*(-1 + s - s2 + t1)*(-1 + t2)) - 
              (2*(-32 + Power(s,5) - 60*s2 - 10*Power(s2,2) + 
                   24*Power(s2,3) + 60*t1 + 20*s2*t1 - 
                   72*Power(s2,2)*t1 - 10*Power(t1,2) + 
                   72*s2*Power(t1,2) - 24*Power(t1,3) + 
                   s1*(-24 + 10*Power(s2,3) + 54*t1 - 
                      25*Power(t1,2) - 9*Power(t1,3) - 
                      Power(s2,2)*(15 + 29*t1) + 
                      s2*(-46 + 40*t1 + 28*Power(t1,2))) + 24*t2 + 
                   50*s2*t2 + 17*Power(s2,2)*t2 - 12*Power(s2,3)*t2 - 
                   58*t1*t2 - 44*s2*t1*t2 + 35*Power(s2,2)*t1*t2 + 
                   27*Power(t1,2)*t2 - 34*s2*Power(t1,2)*t2 + 
                   11*Power(t1,3)*t2 + 
                   Power(s,4)*(12 - s1 - 3*s2 + 3*t1 + t2) + 
                   Power(s,3)*
                    (1 + 3*Power(s2,2) + s1*(-10 + 3*s2 - 3*t1) + 
                      35*t1 + 3*Power(t1,2) + 12*t2 + 3*t1*t2 - 
                      3*s2*(12 + 2*t1 + t2)) + 
                   Power(s,2)*
                    (-68 - Power(s2,3) - 20*t1 + 34*Power(t1,2) + 
                      Power(t1,3) - 
                      s1*(23 + 3*Power(s2,2) + 29*t1 + 
                        3*Power(t1,2) - 6*s2*(5 + t1)) + 25*t2 + 
                      35*t1*t2 + 3*Power(t1,2)*t2 + 
                      3*Power(s2,2)*(12 + t1 + t2) - 
                      s2*(-30 + 70*t1 + 3*Power(t1,2) + 36*t2 + 
                        6*t1*t2)) + 
                   s*(84 - 78*t1 - 45*Power(t1,2) + 11*Power(t1,3) + 
                      s1*(54 + Power(s2,3) - 48*t1 - 28*Power(t1,2) - 
                        Power(t1,3) - 3*Power(s2,2)*(10 + t1) + 
                        s2*(38 + 58*t1 + 3*Power(t1,2))) - 58*t2 + 
                      52*t1*t2 + 34*Power(t1,2)*t2 + Power(t1,3)*t2 - 
                      Power(s2,3)*(12 + t2) + 
                      Power(s2,2)*(-55 + 36*t2 + t1*(35 + 3*t2)) - 
                      s2*(-70 + 42*t2 + Power(t1,2)*(34 + 3*t2) + 
                        10*t1*(-10 + 7*t2)))))/
               ((-1 + s - s2 + t1)*(s - s1 + t2)) + 
              ((s - s2 + t1)*
                 (-12 - 48*s2 - 69*Power(s2,2) - 17*Power(s2,3) + 
                   24*Power(s2,4) + 44*t1 + 106*s2*t1 + 
                   40*Power(s2,2)*t1 - 65*Power(s2,3)*t1 - 
                   53*Power(t1,2) - 41*s2*Power(t1,2) + 
                   61*Power(s2,2)*Power(t1,2) + 18*Power(t1,3) - 
                   23*s2*Power(t1,3) + 3*Power(t1,4) + 
                   2*Power(s,4)*(1 + t1) + 
                   Power(s1,2)*
                    (8 + Power(s2,3) - 12*t1 + 2*Power(t1,2) + 
                      Power(t1,3) - Power(s2,2)*(6 + t1) + 
                      s2*(4 + 4*t1 - Power(t1,2))) - 20*t2 - 
                   48*s2*t2 - 17*Power(s2,2)*t2 + 20*Power(s2,3)*t2 - 
                   Power(s2,4)*t2 + 52*t1*t2 + 62*s2*t1*t2 - 
                   31*Power(s2,2)*t1*t2 + Power(s2,3)*t1*t2 - 
                   37*Power(t1,2)*t2 + 10*s2*Power(t1,2)*t2 + 
                   Power(s2,2)*Power(t1,2)*t2 + Power(t1,3)*t2 - 
                   s2*Power(t1,3)*t2 - 8*Power(t2,2) - 
                   8*s2*Power(t2,2) + 2*Power(s2,2)*Power(t2,2) + 
                   8*t1*Power(t2,2) - 4*s2*t1*Power(t2,2) + 
                   2*Power(t1,2)*Power(t2,2) + 
                   Power(s,3)*
                    (10 + 5*Power(t1,2) - 5*t2 + s1*t2 + 
                      t1*(19 - 3*s1 + t2) + s2*(-20 - 7*t1 + t2)) - 
                   s1*(Power(s2,4) + 4*Power(t1,3) + Power(t1,4) + 
                      s2*(-6*Power(t1,3) + 3*Power(t1,2)*(-7 + t2) - 
                        4*(-6 + t2) + 2*t1*(-3 + t2)) + 
                      Power(s2,2)*
                       (9 + 10*Power(t1,2) + t1*(22 - 6*t2) - 7*t2) + 
                      5*Power(t1,2)*(1 + t2) + 4*(3 + t2) - 
                      4*t1*(5 + 2*t2) + Power(s2,3)*(-5 - 6*t1 + 3*t2)\
) + Power(s,2)*(-37 + 10*t1 + 31*Power(t1,2) + 4*Power(t1,3) + 
                      Power(s1,2)*(-2 + s2 + t1) + 
                      Power(s2,2)*(58 + 8*t1 - 3*t2) - 5*t2 - 
                      5*t1*t2 + 2*Power(t1,2)*t2 + 2*Power(t2,2) - 
                      s2*(25 + 12*Power(t1,2) - 30*t2 + 
                       t1*(87 + t2)) - 
                      s1*(9 + Power(s2,2) + 7*Power(t1,2) - 
                        2*t1*(-6 + t2) + t2 + s2*(-1 - 10*t1 + 5*t2))) \
+ s*(36 - 74*t1 + 22*Power(t1,2) + 17*Power(t1,3) + Power(t1,4) - 
                      2*Power(s1,2)*
                       (4 - 4*s2 + Power(s2,2) - Power(t1,2)) + 
                      Power(s2,2)*
                       (32 + 7*Power(t1,2) - t1*(-133 + t2) - 45*t2) + 
                      28*t2 - 46*t1*t2 + Power(t1,2)*t2 + 
                      Power(t1,3)*t2 + 8*Power(t2,2) + 
                      4*t1*Power(t2,2) + 
                      Power(s2,3)*(-64 - 3*t1 + 3*t2) - 
                      s2*(-90 + 5*Power(t1,3) + t1*(34 - 36*t2) - 
                        22*t2 + 4*Power(t2,2) + 
                        Power(t1,2)*(86 + 3*t2)) + 
                      s1*(2*Power(s2,3) - 5*Power(t1,3) + 
                        2*s2*
                        (11 + 8*Power(t1,2) + t1*(15 - 4*t2) - 3*t2) + 
                        Power(t1,2)*(-16 + t2) - 6*t1*(1 + t2) + 
                        4*(5 + t2) + Power(s2,2)*(-6 - 13*t1 + 7*t2))))\
)/((-1 + s2)*(1 - s + s2 - t1)*(-1 + t1)) + 
              (48 + 114*s2 + 67*Power(s2,2) - 6*Power(s2,3) - 
                 6*Power(s2,4) - 98*t1 - 128*s2*t1 - Power(s2,2)*t1 + 
                 20*Power(s2,3)*t1 + 61*Power(t1,2) + 
                 20*s2*Power(t1,2) - 24*Power(s2,2)*Power(t1,2) - 
                 13*Power(t1,3) + 12*s2*Power(t1,3) - 2*Power(t1,4) + 
                 s1*(16 - Power(s2,3) - 12*t1 - 8*Power(t1,2) + 
                    Power(t1,3) + Power(s2,2)*(-8 + 3*t1) + 
                    s2*(12 + 16*t1 - 3*Power(t1,2))) + 
                 2*Power(s,4)*(-9 + t2) - 16*t2 - 10*s2*t2 + 
                 15*Power(s2,2)*t2 + 11*Power(s2,3)*t2 + 
                 2*Power(s2,4)*t2 + 10*t1*t2 - 24*s2*t1*t2 - 
                 24*Power(s2,2)*t1*t2 - 8*Power(s2,3)*t1*t2 + 
                 9*Power(t1,2)*t2 + 15*s2*Power(t1,2)*t2 + 
                 12*Power(s2,2)*Power(t1,2)*t2 - 2*Power(t1,3)*t2 - 
                 8*s2*Power(t1,3)*t2 + 2*Power(t1,4)*t2 + 
                 Power(s,3)*(20 + 10*s1 - 47*t1 + s2*(60 - 8*t2) - 
                    11*t2 + 8*t1*t2) + 
                 Power(s,2)*(-7*s1*(2 + 3*s2 - 3*t1) + 
                    12*Power(s2,2)*(-6 + t2) + 
                    3*(21 + t1*(7 - 8*t2) + 5*t2 + 
                       2*Power(t1,2)*(-7 + 2*t2)) + 
                    s2*(-46 + 33*t2 - 6*t1*(-19 + 4*t2))) + 
                 s*(-114 + 124*t1 - 12*Power(t1,2) - 15*Power(t1,3) + 
                    2*s1*(-6 + 6*Power(s2,2) + s2*(11 - 12*t1) - 
                       11*t1 + 6*Power(t1,2)) + 
                    Power(s2,3)*(36 - 8*t2) + 10*t2 + 24*t1*t2 - 
                    15*Power(t1,2)*t2 + 8*Power(t1,3)*t2 - 
                    2*s2*(65 + t1*(10 - 24*t2) + 15*t2 + 
                       3*Power(t1,2)*(-11 + 4*t2)) + 
                    Power(s2,2)*(32 - 33*t2 + 3*t1*(-29 + 8*t2))))/
               ((-1 + s2)*(1 - s + s2 - t1)) + 
              (2*(s - s2 + t1)*
                 (24 + 28*s2 + 4*Power(s2,3) - 28*t1 + Power(s,4)*t1 - 
                   12*Power(s2,2)*t1 + 12*s2*Power(t1,2) - 
                   4*Power(t1,3) + 
                   Power(s1,2)*t1*
                    (-4 + Power(s2,2) + 4*t1 + Power(t1,2) - 
                      2*s2*(2 + t1)) - 20*t2 - 12*s2*t2 + 
                   15*Power(s2,2)*t2 - Power(s2,3)*t2 + 12*t1*t2 - 
                   32*s2*t1*t2 + 17*Power(t1,2)*t2 + 
                   3*s2*Power(t1,2)*t2 - 2*Power(t1,3)*t2 + 
                   4*Power(t2,2) + 2*s2*Power(t2,2) - 
                   4*Power(s2,2)*Power(t2,2) - 6*t1*Power(t2,2) + 
                   4*s2*t1*Power(t2,2) + Power(s2,2)*t1*Power(t2,2) - 
                   2*s2*Power(t1,2)*Power(t2,2) + 
                   Power(t1,3)*Power(t2,2) - 
                   Power(s,3)*
                    (-7 + s2 + 2*s2*t1 - 2*Power(t1,2) + 
                      2*t1*(-1 + s1 - t2) + 4*t2) + 
                   Power(s,2)*
                    (16 + 12*t1 + Power(s1,2)*t1 + Power(t1,3) + 
                      Power(s2,2)*(2 + t1) + 9*t2 - 2*t1*t2 + 
                      4*Power(t1,2)*t2 - 4*Power(t2,2) + 
                      t1*Power(t2,2) - 
                      s2*(14 + 2*t1 + 2*Power(t1,2) - 7*t2 + 
                       4*t1*t2) + 
                      s1*(5 + s2 + 4*s2*t1 - 4*Power(t1,2) + 4*t2 - 
                        2*t1*(3 + t2))) + 
                   s1*(Power(s2,3) - 4*(-7 + t2) - 
                      2*Power(t1,3)*(-1 + t2) - 
                      Power(t1,2)*(1 + 4*t2) + 2*t1*(-16 + 5*t2) + 
                      Power(s2,2)*(1 - 2*(-2 + t1)*t2) + 
                      s2*(32 - 2*t2 + Power(t1,2)*(-3 + 4*t2))) - 
                   s*(48 + Power(s2,3) - 12*t1 - 5*Power(t1,2) + 
                      2*Power(t1,3) - 2*Power(s1,2)*t1*(2 - s2 + t1) - 
                      20*t2 - 18*t1*t2 - 2*Power(t1,3)*t2 + 
                      2*Power(t2,2) + 4*t1*Power(t2,2) - 
                      2*Power(t1,2)*Power(t2,2) + 
                      Power(s2,2)*(-3 - 2*(-1 + t1)*t2) + 
                      s2*(12 + 24*t2 - 8*Power(t2,2) + 
                        Power(t1,2)*(-3 + 4*t2) + 
                        2*t1*(4 - t2 + Power(t2,2))) + 
                      2*s1*(18 - 6*t1 + Power(t1,3) + 
                        Power(s2,2)*(1 + t1) - t2 + 
                        2*Power(t1,2)*(1 + t2) + 
                        s2*(3 - 2*Power(t1,2) + 4*t2 - t1*(3 + 2*t2))))\
))/((-1 + s1)*(-1 + s - s2 + t1)*(-s + s1 - t2)) - 
              ((s - s2 + t1)*
                 (-12 - 16*s2 + 3*Power(s2,2) - Power(s2,3) - 
                   2*Power(s2,4) - 16*t1 - 42*s2*t1 - Power(s2,2)*t1 + 
                   5*Power(s2,3)*t1 + 35*Power(t1,2) + s2*Power(t1,2) - 
                   2*Power(s2,2)*Power(t1,2) + Power(t1,3) - 
                   3*s2*Power(t1,3) + 2*Power(t1,4) - 
                   Power(s1,2)*
                    (24 + Power(s2,3) - Power(s2,2)*(-8 + t1) - 
                      40*t1 + 16*Power(t1,2) + Power(t1,3) - 
                      s2*(-32 + 24*t1 + Power(t1,2))) + 
                   2*Power(s,4)*(-2 + t2) + 24*t2 + 20*s2*t2 - 
                   12*Power(s2,2)*t2 + 2*Power(s2,3)*t2 + 
                   Power(s2,4)*t2 + 44*s2*t1*t2 - 7*Power(s2,2)*t1*t2 - 
                   2*Power(s2,3)*t1*t2 - 36*Power(t1,2)*t2 + 
                   4*s2*Power(t1,2)*t2 + Power(s2,2)*Power(t1,2)*t2 + 
                   Power(t1,3)*t2 - 12*Power(t2,2) - 8*s2*Power(t2,2) + 
                   7*Power(s2,2)*Power(t2,2) - 
                   Power(s2,3)*Power(t2,2) + 12*t1*Power(t2,2) - 
                   10*s2*t1*Power(t2,2) + 
                   2*Power(s2,2)*t1*Power(t2,2) + 
                   3*Power(t1,2)*Power(t2,2) - 
                   s2*Power(t1,2)*Power(t2,2) + 
                   Power(s,3)*
                    (8 - 7*t1 + s2*(14 + t1 - 7*t2) + 
                      s1*(14 + t1 - 3*t2) + 3*t2 + 5*t1*t2 + 
                      Power(t2,2)) - 
                   Power(s,2)*
                    (9 - t1 + Power(s1,2)*(12 + s2 + t1) + 
                      Power(s2,2)*(18 + 2*t1 - 9*t2) + 12*t2 - 
                      3*t1*t2 - 4*Power(t1,2)*t2 - 7*Power(t2,2) - 
                      2*t1*Power(t2,2) + 
                      s1*(15 - Power(s2,2) - 30*t1 - 2*Power(t1,2) + 
                        s2*(33 + 5*t1 - 10*t2) + 11*t2 + 7*t1*t2) + 
                      s2*(17 - 23*t1 - 2*Power(t1,2) + 4*t2 + 
                        12*t1*t2 + 3*Power(t2,2))) + 
                   s1*(Power(s2,4) + t1*(8 - 20*t2) + 
                      Power(s2,2)*
                       (1 + 7*Power(t1,2) + t1*(24 - 9*t2) - 15*t2) - 
                      3*Power(t1,2)*(-11 + t2) - 
                      Power(t1,3)*(2 + t2) + 
                      Power(s2,3)*(-9 - 5*t1 + 4*t2) + 4*(-7 + 5*t2) + 
                      s2*(-28 - 3*Power(t1,3) + 8*t2 + 
                        Power(t1,2)*(-13 + 6*t2) + 2*t1*(-13 + 9*t2))) \
+ s*(20 + 38*t1 - 6*Power(t1,2) + 5*Power(t1,3) + 
                      2*Power(s1,2)*
                       (18 + 10*s2 + Power(s2,2) - 14*t1 - Power(t1,2)) \
+ Power(s2,3)*(10 + t1 - 5*t2) - 24*t2 - 44*t1*t2 + Power(t1,2)*t2 + 
                      Power(t1,3)*t2 + 8*Power(t2,2) + 
                      10*t1*Power(t2,2) + Power(t1,2)*Power(t2,2) - 
                      Power(s2,2)*
                       (-10 + 2*Power(t1,2) + t1*(21 - 9*t2) + t2 - 
                        3*Power(t2,2)) + 
                      s2*(6 + Power(t1,3) + Power(t1,2)*(6 - 5*t2) + 
                        24*t2 - 14*Power(t2,2) - 
                        4*t1*(1 - t2 + Power(t2,2))) + 
                      s1*(40 - 2*Power(s2,3) + Power(t1,3) + 
                         Power(s2,2)*(28 + 9*t1 - 11*t2) + 
                         Power(t1,2)*(14 - 5*t2) - 14*t1*(-1 + t2) - 
                         12*t2 + 
                         s2*(10 - 8*Power(t1,2) + 26*t2 + 
                         2*t1*(-25 + 8*t2))))))/
               ((-1 + s1)*(-1 + s - s2 + t1)*(-1 + t2)))*Log(s - s2 + t1))/
          (-1 + s - s2 + t1)))/(Power(s - s2 + t1,3)*Power(s - s1 + t2,2)) + 
    (8*(((s - s1 + t2)*(1 + 3*t1 - 3*s*t1 + 
              Power(s1,4)*(10 + s2 + t1 - 2*t2) - t2 + 3*s*t2 - 
              3*s2*t2 + Power(s1,3)*
               (-55 + s2*(-2 + t2) + 5*t2 + s*t2 + 2*Power(t2,2) - 
                 t1*(3 + s + 2*t2)) + 
              s1*(-40 + s2 + 3*t1 - 30*t2 + 10*s2*t2 - 8*t1*t2 + 
                 30*Power(t2,2) + 2*s*(9 + t1 + 10*t2)) + 
              2*Power(s1,2)*(s*(-5 + t1) + t1*(-2 + 5*t2) - 
                 2*(5 + 5*t2 + 2*s2*t2 + 2*Power(t2,2))) - 
              (-1 + 2*Power(s1,4)*(-9 + s2) + 3*(-1 + s)*t1 + t2 - 
                 3*s*t2 + 3*s2*t2 + 
                 Power(s1,3)*(42 + 10*s - 7*s2 - 15*t1 + 12*t2) + 
                 s1*(10 - 5*s2 + 7*t1 - 2*s*(5 + 6*t1 - 6*t2) - 
                    12*s2*t2) + 
                 Power(s1,2)*
                  (31 + 10*s2 + 11*t1 + s*(-16 + 9*t1 - 9*t2) - 13*t2 + 
                    9*s2*t2))*R1(s1)))/(-1 + t2) - 
         2*(-6 + 2*s1 - 79*Power(s1,2) + 4*Power(s1,3) - 11*Power(s1,4) + 
            2*Power(s1,5) + 6*s1*s2 - 8*Power(s1,2)*s2 + 
            2*Power(s1,3)*s2 - 6*s1*t1 + 15*Power(s1,2)*t1 - 
            11*Power(s1,3)*t1 + 2*Power(s1,4)*t1 + 4*t2 + 4*s1*t2 - 
            9*Power(s1,2)*t2 + 21*Power(s1,3)*t2 - 4*Power(s1,4)*t2 - 
            7*s1*t1*t2 + 9*Power(s1,2)*t1*t2 - 2*Power(s1,3)*t1*t2 + 
            Power(t2,2) + 15*s1*Power(t2,2) - 
            10*Power(s1,2)*Power(t2,2) + 2*Power(s1,3)*Power(t2,2) + 
            s*(4 - 2*Power(s1,4) + Power(s1,2)*(14 + 9*t1 - 10*t2) + 
               t2 + Power(s1,3)*(9 - 2*t1 + 2*t2) + 
               s1*(-1 - 7*t1 + 15*t2)) + 
            (-6 - Power(s1,5) + 4*t2 + Power(t2,2) + 
               Power(s1,2)*(-42 + 8*s2 - 11*t1 + 78*t2 - 4*t1*t2 + 
                  3*Power(t2,2)) - 
               Power(s1,3)*(45 + 2*s2 + 19*t2 + Power(t2,2) - 
                  t1*(6 + t2)) + Power(s1,4)*(-t1 + 2*(8 + t2)) + 
               s*(4 + Power(s1,4) + s1*(-26 + 3*t1 - 11*t2) + 
                  Power(s1,3)*(-14 + t1 - t2) + t2 + 
                  Power(s1,2)*(59 - 4*t1 + 3*t2)) + 
               s1*(-6*s2 + 3*t1*(2 + t2) - 11*(-2 + 3*t2 + Power(t2,2))))*
             R1(s1)) + ((-1 + s1)*
            (8 - 94*s1 - 111*Power(s1,2) - 19*Power(s1,3) + 
              2*Power(s1,4) - 8*s2 + 20*s1*s2 - 32*Power(s1,2)*s2 + 
              11*Power(s1,3)*s2 - Power(s1,4)*s2 + 8*t1 - 24*s1*t1 + 
              29*Power(s1,2)*t1 - 20*Power(s1,3)*t1 + 3*Power(s1,4)*t1 - 
              14*t2 + 109*s1*t2 + 32*Power(s1,2)*t2 - 7*Power(s1,3)*t2 + 
              4*s2*t2 - 20*s1*s2*t2 - 14*Power(s1,2)*s2*t2 + 
              2*Power(s1,3)*s2*t2 + 25*s1*t1*t2 + 24*Power(s1,2)*t1*t2 - 
              5*Power(s1,3)*t1*t2 + 2*Power(t2,2) - 41*s1*Power(t2,2) + 
              5*Power(s1,2)*Power(t2,2) + 7*s1*s2*Power(t2,2) - 
              Power(s1,2)*s2*Power(t2,2) - 2*t1*Power(t2,2) - 
              8*s1*t1*Power(t2,2) + 2*Power(s1,2)*t1*Power(t2,2) + 
              Power(s,2)*(-2*Power(s1,3) - 2*t1 + 
                 Power(s1,2)*(12 + t1 + t2) - s1*(22 + t1 + 7*t2)) + 
              s*(2*Power(s1,4) + 2*(-3 + 2*s2 + t2 - 2*t1*t2) + 
                 Power(s1,3)*(s2 - 4*t1 - 3*(6 + t2)) + 
                 Power(s1,2)*
                  (65 + 24*t2 + Power(t2,2) - s2*(7 + t2) + 
                    t1*(17 + 3*t2)) + 
                 s1*(87 + t1*(25 - 9*t2) - 63*t2 - 7*Power(t2,2) + 
                    s2*(-20 + 7*t2))) - 
              2*(-4 + 4*s2 - 4*t1 + Power(s1,4)*t1 + 
                 Power(s,2)*(1 - 6*s1 + Power(s1,2))*t1 + 7*t2 - 
                 2*s2*t2 - Power(t2,2) + t1*Power(t2,2) + 
                 Power(s1,3)*(-7 + 4*s2 + t2 - 2*t1*(2 + t2)) + 
                 Power(s1,2)*
                  (42 + s2*(6 - 4*t2) + 5*t2 - Power(t2,2) + 
                    t1*(-13 + 10*t2 + Power(t2,2))) + 
                 s1*(21 + 10*s2*(-1 + t2) - 37*t2 + 6*Power(t2,2) - 
                    2*t1*(-6 + 2*t2 + 3*Power(t2,2))) - 
                 s*(-3 + 2*s2 + 2*Power(s1,3)*t1 + t2 - 2*t1*t2 + 
                    s1*(26 - 10*s2 + 4*t1 - 6*t2 + 12*t1*t2) + 
                    Power(s1,2)*(4*s2 - (-1 + 2*t1)*(5 + t2))))*R1(s1)))/
          (s - s2 + t1) - ((-1 + s1)*(-s + s1 - t2)*
            (-4 + 3*s1 - 59*Power(s1,2) - 10*Power(s1,3) - 
              4*Power(s1,4) - 7*s2 - 46*Power(s1,2)*s2 + 
              7*Power(s1,3)*s2 + 7*s1*Power(s2,2) - 
              4*Power(s1,2)*Power(s2,2) + Power(s1,3)*Power(s2,2) + 
              4*t1 - 6*s1*t1 + 47*Power(s1,2)*t1 - 29*Power(s1,3)*t1 + 
              2*Power(s1,4)*t1 + s2*t1 - 9*s1*s2*t1 + 
              8*Power(s1,2)*s2*t1 - 2*Power(s1,3)*s2*t1 + 
              s1*Power(t1,2) - 6*Power(s1,2)*Power(t1,2) + 
              Power(s1,3)*Power(t1,2) - t2 + 19*s1*t2 + 
              8*Power(s1,3)*t2 + 9*s2*t2 - 28*s1*s2*t2 + 
              Power(s1,3)*s2*t2 - Power(s2,2)*t2 - 4*s1*Power(s2,2)*t2 + 
              Power(s1,2)*Power(s2,2)*t2 - 3*t1*t2 - 6*s1*t1*t2 + 
              16*Power(s1,2)*t1*t2 - Power(s1,3)*t1*t2 + 7*s1*s2*t1*t2 - 
              Power(s1,2)*s2*t1*t2 + 5*Power(t2,2) - 58*s1*Power(t2,2) + 
              5*Power(s1,2)*Power(t2,2) - s2*Power(t2,2) - 
              4*s1*s2*Power(t2,2) + Power(s1,2)*s2*Power(t2,2) + 
              14*s1*t1*Power(t2,2) - 2*Power(s1,2)*t1*Power(t2,2) - 
              Power(s,2)*(-1 + 3*s1)*(-2 + t1 + t2) - 
              s*(-5 + s2*(-4 + t1) + 2*Power(s1,3)*(-2 + t1) + 3*t1 + 
                 3*t2 - t1*t2 - Power(t2,2) + 
                 s1*(7 - 7*t1 - 7*Power(t1,2) + s2*(5 + 4*t1 - 7*t2) + 
                    20*t2 + 3*t1*t2 - 4*Power(t2,2)) + 
                 Power(s1,2)*
                  (-20 - 28*t1 + Power(t1,2) + t2 + Power(t2,2) + 
                    s2*(5 - t1 + t2))) + 
              (-4 - 7*s2 + 4*t1 + s2*t1 + 2*Power(s1,4)*(1 + t1) - t2 + 
                 9*s2*t2 - Power(s2,2)*t2 - 3*t1*t2 + 5*Power(t2,2) - 
                 s2*Power(t2,2) + 
                 Power(s,2)*(1 - 6*s1 + Power(s1,2))*(-2 + t1 + t2) - 
                 Power(s1,3)*
                  (18 + Power(s2,2) - 12*t2 + t1*(16 + t2) + 
                    s2*(-4 - 3*t1 + t2)) - 
                 Power(s1,2)*
                  (6 + 4*Power(t1,2) + Power(s2,2)*(-2 + t2) + 29*t2 - 
                    9*Power(t2,2) - t1*(18 + 7*t2) + 
                    s2*(31 + 5*t1 - 19*t2 + Power(t2,2))) + 
                 s1*(-4*Power(t1,2) + Power(s2,2)*(-5 + 6*t2) + 
                    t1*(-8 + 21*t2) + 
                    s2*(14 + 9*t1 - 51*t2 + 6*Power(t2,2)) - 
                    2*(-5 + t2 + 13*Power(t2,2))) + 
                 s*(5 + 4*s2 + Power(s1,3)*(s2 - 3*t1) - 3*t1 - s2*t1 - 
                    3*t2 + t1*t2 + Power(t2,2) + 
                    s1*(-22 + s2*(-19 + 6*t1) + t1*(15 - 6*t2) + 
                       18*t2 - 6*Power(t2,2)) + 
                    Power(s1,2)*
                     (13 - s2*(-2 + t1) - 3*t2 + Power(t2,2) + 
                       t1*(15 + t2))))*R1(s1)))/((-1 + t1)*(-1 + t2)) + 
         ((-1 + s1)*(-s + s1 - t2)*
            (-4 + 61*s1 + 3*Power(s1,2) - 12*Power(s1,3) + 
              2*Power(s1,4) - 4*s2 + 49*s1*s2 - 38*Power(s1,2)*s2 + 
              27*Power(s1,3)*s2 - 2*Power(s1,4)*s2 + 4*Power(s2,2) - 
              12*s1*Power(s2,2) + 33*Power(s1,2)*Power(s2,2) - 
              3*Power(s1,3)*Power(s2,2) + 4*t1 - 62*s1*t1 - 
              23*Power(s1,2)*t1 + 5*Power(s1,3)*t1 - 2*Power(s1,4)*t1 - 
              4*s2*t1 + 19*s1*s2*t1 - 9*Power(s1,2)*s2*t1 - 
              s1*Power(t1,2) + 6*Power(s1,2)*Power(t1,2) - 
              Power(s1,3)*Power(t1,2) - 8*t2 + 102*s1*t2 - 
              8*Power(s1,2)*t2 + 14*Power(s1,3)*t2 + 2*s2*t2 + 
              16*s1*s2*t2 + 7*Power(s1,2)*s2*t2 + Power(s1,3)*s2*t2 - 
              7*s1*Power(s2,2)*t2 + Power(s1,2)*Power(s2,2)*t2 + 
              2*t1*t2 - 20*s1*t1*t2 - 9*Power(s1,2)*t1*t2 + 
              Power(s1,3)*t1*t2 - 7*s1*s2*t1*t2 + Power(s1,2)*s2*t1*t2 - 
              2*Power(t2,2) - s1*Power(t2,2) + Power(s1,2)*Power(t2,2) + 
              7*s1*s2*Power(t2,2) - Power(s1,2)*s2*Power(t2,2) + 
              Power(s,2)*(s1*(18 - 6*s2 + 27*t1 - 13*t2) + 
                 Power(s1,2)*(-6 - 3*t1 + t2) + 2*(-2 + s2 - t1 + t2)) + 
              s*(Power(s1,3)*(6 + s2 + 5*t1 - 2*t2) + 
                 Power(s1,2)*
                  (-19 - 35*t1 + Power(t1,2) + s2*(-9 + t1 - 2*t2) + 
                    4*t2 - 4*t1*t2 + Power(t2,2)) - 
                 2*(-4 + Power(s2,2) - t1 + 2*t2 + s2*(1 - t1 + t2)) + 
                 s1*(-53 + 6*Power(s2,2) - 18*t1 - 7*Power(t1,2) - 
                    6*t2 + 28*t1*t2 - 7*Power(t2,2) + 
                    s2*(4 - 13*t1 + 20*t2))) + 
              2*(-2 - 2*Power(s1,4) - 2*s2 + 2*Power(s2,2) + 2*t1 - 
                 2*s2*t1 - 4*t2 + s2*t2 + t1*t2 - Power(t2,2) + 
                 Power(s,2)*(1 - 6*s1 + Power(s1,2))*
                  (-2 + s2 - t1 + t2) + Power(s1,3)*(5 + 3*s2 + 5*t2) + 
                 s1*(17 - 6*Power(s2,2) + 2*Power(t1,2) + 
                    s2*(13 + 8*t1) + 31*t2 + 6*Power(t2,2) - 
                    4*t1*(5 + 2*t2)) + 
                 Power(s1,2)*
                  (2 + 8*Power(s2,2) + 2*Power(t1,2) - 16*t2 - 
                    Power(t2,2) - t1*(6 + t2) + s2*(-6 - 6*t1 + 7*t2)) \
- s*(-4 + Power(s2,2) - t1 + 2*t2 + s2*(1 - t1 + t2) + 
                    Power(s1,3)*(-3 + s2 - t1 + t2) - 
                    s1*(-21 + s2 + 6*Power(s2,2) - 3*t1 - 6*s2*t1 + 
                       7*t2 + 6*s2*t2) + 
                    Power(s1,2)*
                     (14 + Power(s2,2) + 3*t1 + s2*(-1 - t1 + t2))))*
               R1(s1)))/((-1 + s2)*(-1 + t1)) + 
         ((-1 + s1)*(-s + s1 - t2)*
            (-14 + 133*s1 - 7*Power(s1,2) + 20*Power(s1,3) + 
              8*Power(s1,4) + 2*s2 + 12*s1*s2 - 47*Power(s1,2)*s2 + 
              39*Power(s1,3)*s2 - 2*Power(s1,4)*s2 + 2*Power(s2,2) + 
              32*Power(s1,2)*Power(s2,2) - 2*Power(s1,3)*Power(s2,2) + 
              5*t1 - 40*s1*t1 + 68*Power(s1,2)*t1 - 15*Power(s1,3)*t1 - 
              2*s2*t1 - s1*s2*t1 - 33*Power(s1,2)*s2*t1 + 
              2*Power(s1,3)*s2*t1 - Power(t1,2) + 2*s1*Power(t1,2) + 
              3*Power(s1,2)*Power(t1,2) + 3*t2 - 38*s1*t2 - 
              3*Power(s1,2)*t2 - 8*Power(s1,3)*t2 - 2*s2*t2 + 
              48*s1*s2*t2 - 18*Power(s1,2)*s2*t2 + 2*Power(s1,3)*s2*t2 - 
              21*s1*t1*t2 - Power(s1,2)*t1*t2 + s2*t1*t2 - 
              3*s1*s2*t1*t2 - Power(t2,2) + 3*s1*Power(t2,2) - 
              s2*Power(t2,2) + 3*s1*s2*Power(t2,2) + 
              Power(s,2)*(t1 + t2 - Power(s1,2)*(-10 + t1 + t2) + 
                 2*s1*(7 + 2*t1 + 2*t2)) + 
              s*(11 - t1 + Power(t1,2) + Power(s1,3)*(-20 + 3*s2 + t1) - 
                 t2 + Power(t2,2) - s2*(6 + t2) + 
                 Power(s1,2)*(21 + 8*t1 + s2*(-52 + t2) - 2*t1*t2) - 
                 s1*(128 + 8*t1 + 3*Power(t1,2) - 5*t2 - 14*t1*t2 + 
                    3*Power(t2,2) + s2*(-35 + 4*t2))) + 
              (-14 - 2*Power(s1,4)*(-2 + s2) + 2*s2 + 2*Power(s2,2) + 
                 5*t1 - 2*s2*t1 - Power(t1,2) + 3*t2 - 2*s2*t2 + 
                 s2*t1*t2 - Power(t2,2) - s2*Power(t2,2) + 
                 Power(s,2)*(t1 + t2 - 6*s1*(-2 + t1 + t2) + 
                    Power(s1,2)*(12 + t1 + t2)) + 
                 Power(s1,3)*
                  (6 - 2*Power(s2,2) - Power(t1,2) + t1*(-4 + t2) - 
                    2*t2 + s2*(22 + t1 + t2)) + 
                 Power(s1,2)*
                  (-6 + 22*Power(s2,2) + 5*Power(t1,2) + 
                    t1*(9 - 2*t2) - 9*t2 - Power(t2,2) + 
                    s2*(8 + t1*(-20 + t2) - 16*t2 - Power(t2,2))) + 
                 s1*(74 - 6*Power(s2,2) + 5*Power(t1,2) - 12*t2 + 
                    6*Power(t2,2) + t1*(-22 + 5*t2) + 
                    s2*(2 + t1 + 5*t2 - 6*t1*t2 + 6*Power(t2,2))) + 
                 s*(11 + Power(s1,3)*(-14 + 3*s2 - t1) - t1 + 
                    Power(t1,2) - t2 + Power(t2,2) - s2*(6 + t2) + 
                    Power(s1,2)*
                     (3 + 13*t1 + Power(t1,2) + 7*t2 + Power(t2,2) - 
                       s2*(44 + t2)) + 
                    s1*(-76 + 13*t1 - 6*Power(t1,2) + 14*t2 - 
                       6*Power(t2,2) + s2*(19 + 6*t2))))*R1(s1)))/
          ((-1 + s2)*(-s + s2 - t1))))/
     (Power(-1 + s1,3)*s1*Power(s - s1 + t2,2)) + 
    (8*(((-1 + t2)*(3 - 46*s1 - 20*Power(s1,2) - 49*Power(s1,3) + 
              8*Power(s1,4) + 3*s1*s2 - 4*Power(s1,3)*s2 + 
              Power(s1,4)*s2 - 3*t1 + 5*s1*t1 + 2*Power(s1,2)*t1 - 
              5*Power(s1,3)*t1 + Power(s1,4)*t1 + t2 - 22*s1*t2 - 
              34*Power(s1,2)*t2 + 9*Power(s1,3)*t2 - 2*Power(s1,4)*t2 + 
              3*s2*t2 - 2*s1*s2*t2 - 2*Power(s1,2)*s2*t2 + 
              Power(s1,3)*s2*t2 - 8*s1*t1*t2 + 10*Power(s1,2)*t1*t2 - 
              2*Power(s1,3)*t1*t2 + 30*s1*Power(t2,2) - 
              8*Power(s1,2)*Power(t2,2) + 2*Power(s1,3)*Power(t2,2) + 
              s*(-2 + 3*t1 + Power(s1,2)*(-20 + 8*t1 - 6*t2) - 3*t2 + 
                 Power(s1,3)*(4 - t1 + t2) + s1*(26 - 10*t1 + 32*t2)) + 
              (3 - 2*Power(s1,4)*(-3 + s2) - 3*t1 + 
                 s1*(-18 + 7*t1 + s2*(7 - 12*t2)) + 
                 Power(s1,2)*(-29 + 11*t1 + 9*s2*(-2 + t2) - 13*t2) + 
                 s*(-2 - 4*Power(s1,3) + 3*t1 + 
                    Power(s1,2)*(2 + 9*t1 - 9*t2) - 
                    4*s1*(-5 + 3*t1 - 3*t2) - 3*t2) + t2 + 3*s2*t2 + 
                 Power(s1,3)*(-26 + 13*s2 - 15*t1 + 12*t2))*R1(s1)))/
          (s - s1 + t2) + 2*(1 - 14*s1 + 46*Power(s1,2) - 9*Power(s1,3) + 
            7*s1*s2 - 9*Power(s1,2)*s2 + 2*Power(s1,3)*s2 - 13*s1*t1 + 
            17*Power(s1,2)*t1 - 4*Power(s1,3)*t1 + 
            s*(-1 - 8*s1 + Power(s1,2))*(-1 + t2) + 6*t2 + 29*s1*t2 + 
            10*Power(s1,2)*t2 + 3*Power(s1,3)*t2 - 7*s1*s2*t2 + 
            9*Power(s1,2)*s2*t2 - 2*Power(s1,3)*s2*t2 + 7*s1*t1*t2 - 
            9*Power(s1,2)*t1*t2 + 2*Power(s1,3)*t1*t2 - Power(t2,2) - 
            15*s1*Power(t2,2) + 10*Power(s1,2)*Power(t2,2) - 
            2*Power(s1,3)*Power(t2,2) + 
            (1 + s*(-1 + 8*s1 + Power(s1,2))*(-1 + t2) + 6*t2 - 
               Power(t2,2) + Power(s1,2)*
                (27 + 4*t1*(-3 + t2) - 4*s2*(-1 + t2) + 58*t2 - 
                  3*Power(t2,2)) + 
               Power(s1,3)*(-11 - t1*(-3 + t2) + s2*(-1 + t2) - 14*t2 + 
                  Power(t2,2)) + 
               s1*(7 - 3*t1*(-3 + t2) + 3*s2*(-1 + t2) - 50*t2 + 
                  11*Power(t2,2)))*R1(s1)) + 
         ((-1 + s1)*(Power(s1,3)*
               (s2 + t1*(-5 + t2) - s2*t2 + 2*(15 + t2)) + 
              Power(s1,2)*(-17 + 62*t1 - 28*t2 - 12*t1*t2 - 
                 7*Power(t2,2) + 2*t1*Power(t2,2) + 
                 s*(-1 + t2)*(-10 + t1 + t2) - 
                 s2*(9 - 10*t2 + Power(t2,2))) - 
              2*(1 + s2 + 7*t2 - s*t2 + s*Power(t2,2) - 
                 s2*Power(t2,2) + t1*(-3 - 2*t2 + Power(t2,2))) - 
              s1*(-47 - 36*t2 + s2*t2 - 49*Power(t2,2) - 
                 s2*Power(t2,2) + s*(-1 + t2)*(-6 + 7*t1 + t2) + 
                 t1*(11 + 5*t2 + 8*Power(t2,2))) + 
              2*(-1 - s2 + 3*t1 - 7*t2 + s*t2 + 2*t1*t2 - 
                 s*Power(t2,2) + s2*Power(t2,2) - t1*Power(t2,2) + 
                 Power(s1,3)*(3 + t2) - 
                 Power(s1,2)*
                  (-3 - 17*t1 + 15*t2 + t1*Power(t2,2) + 
                    s*(-2 + t2 + Power(t2,2)) - 
                    s2*(-5 + 4*t2 + Power(t2,2))) + 
                 s1*(-1 - 4*t1 + 41*t2 - 14*t1*t2 + 6*t1*Power(t2,2) + 
                    s2*(2 + 4*t2 - 6*Power(t2,2)) + 
                    s*(2 - 8*t2 + 6*Power(t2,2))))*R1(s1)))/(-1 + t1) - 
         ((-1 + s1)*(-1 + t2)*
            (8 - 40*s1 - 6*Power(s1,2) + 4*Power(s1,3) - s2 + s1*s2 - 
              4*Power(s1,2)*s2 - 12*Power(s1,3)*s2 + s1*Power(s2,2) + 
              Power(s1,2)*Power(s2,2) + 3*t1 - 57*s1*t1 + 
              52*Power(s1,2)*t1 - 16*Power(s1,3)*t1 - 3*s2*t1 + 
              s1*s2*t1 - 30*Power(s1,2)*s2*t1 + 2*Power(s1,3)*s2*t1 + 
              Power(t1,2) - 2*s1*Power(t1,2) - 
              3*Power(s1,2)*Power(t1,2) + t2 - 55*s1*t2 + 
              2*Power(s1,2)*t2 - 8*Power(s1,3)*t2 - 3*s2*t2 + 
              28*s1*s2*t2 - 13*Power(s1,2)*s2*t2 + 2*Power(s1,3)*s2*t2 + 
              Power(s2,2)*t2 - 3*s1*Power(s2,2)*t2 + 21*s1*t1*t2 + 
              Power(s1,2)*t1*t2 - s2*t1*t2 + 3*s1*s2*t1*t2 + 
              Power(t2,2) - 3*s1*Power(t2,2) + s2*Power(t2,2) - 
              3*s1*s2*Power(t2,2) + 
              Power(s,2)*(-1 - 4*s1 + Power(s1,2))*(-2 + t1 + t2) + 
              s*(-7 + Power(s1,3)*(s2 - t1) + (3 + s2)*t1 - 
                 Power(t1,2) + 3*t2 - Power(t2,2) + 
                 Power(s1,2)*
                  (13 + 15*t1 - 7*t2 + 2*t1*t2 - s2*(8 + t2)) + 
                 s1*(10 + 17*t1 + 3*Power(t1,2) + 30*t2 - 14*t1*t2 + 
                    3*Power(t2,2) + s2*(-1 - 3*t1 + 7*t2))) + 
              (8 - s2 + 3*t1 - 3*s2*t1 + Power(t1,2) + 
                 Power(s1,4)*(-2 + s2 + t1) + t2 - 3*s2*t2 + 
                 Power(s2,2)*t2 - s2*t1*t2 + Power(t2,2) + 
                 s2*Power(t2,2) - 
                 Power(s,2)*(1 - 6*s1 + Power(s1,2))*(-2 + t1 + t2) + 
                 Power(s1,3)*
                  (7 - 6*s2 + Power(s2,2) + Power(t1,2) - 3*t2 - 
                    t1*(7 + t2)) - 
                 s*(7 - (3 + s2)*t1 + Power(t1,2) + 
                    Power(s1,3)*(-2 + s2 - t2) - 3*t2 + Power(t2,2) + 
                    s1*(-56 + 14*t1 - 6*Power(t1,2) + s2*(5 + 6*t1) + 
                       13*t2 - 6*Power(t2,2)) + 
                    Power(s1,2)*
                     (7 - 7*t1 + Power(t1,2) - s2*(2 + t1) - t2 + 
                       Power(t2,2))) + 
                 s1*(-57 - 5*Power(t1,2) + Power(s2,2)*(5 - 6*t2) + 
                    3*t2 - 6*Power(t2,2) - t1*(7 + 5*t2) + 
                    2*s2*(1 + 7*t2 - 3*Power(t2,2) + 3*t1*(1 + t2))) + 
                 Power(s1,2)*
                  (-5*Power(t1,2) + Power(s2,2)*(-2 + t2) + 
                    (-1 + t2)*t2 + 2*t1*(11 + t2) - 
                    s2*(4 + 7*t2 - Power(t2,2) + t1*(15 + t2))))*R1(s1)))/
          ((-1 + s2)*(-1 + t1)) - 
         ((-1 + s1)*(-1 + t2)*
            (8 - 69*s1 - 48*Power(s1,2) + 9*Power(s1,3) - 
              16*Power(s1,4) - 4*s1*s2 + 57*Power(s1,2)*s2 - 
              36*Power(s1,3)*s2 + 3*Power(s1,4)*s2 + 8*s1*Power(s2,2) - 
              18*Power(s1,2)*Power(s2,2) + 2*Power(s1,3)*Power(s2,2) + 
              6*t1 - 82*s1*t1 - 40*Power(s1,2)*t1 - 5*Power(s1,3)*t1 - 
              Power(s1,4)*t1 - 4*s2*t1 + 10*s1*s2*t1 - 
              3*Power(s1,2)*s2*t1 - Power(s1,3)*s2*t1 + s1*Power(t1,2) - 
              6*Power(s1,2)*Power(t1,2) + Power(s1,3)*Power(t1,2) - 
              10*t2 + 76*s1*t2 - 17*Power(s1,2)*t2 + 17*Power(s1,3)*t2 + 
              4*s2*t2 + 3*s1*s2*t2 + 11*Power(s1,2)*s2*t2 - 
              14*s1*Power(s2,2)*t2 + 2*Power(s1,2)*Power(s2,2)*t2 - 
              2*t1*t2 + 20*s1*t1*t2 + 9*Power(s1,2)*t1*t2 - 
              Power(s1,3)*t1*t2 + 7*s1*s2*t1*t2 - Power(s1,2)*s2*t1*t2 + 
              2*Power(t2,2) + s1*Power(t2,2) - Power(s1,2)*Power(t2,2) - 
              7*s1*s2*Power(t2,2) + Power(s1,2)*s2*Power(t2,2) + 
              Power(s,2)*(2 - 2*t1 + s1*(-8 + 13*t1 - 27*t2) + 2*t2 + 
                 Power(s1,2)*(2 - t1 + 3*t2)) - 
              s*(Power(s1,3)*(-8 + s2 - 2*t1 + t2) + 
                 2*(5 + t2 + s2*(-t1 + t2)) + 
                 Power(s1,2)*
                  (13 + Power(t1,2) - 4*t1*(-3 + t2) + 5*t2 + 
                    Power(t2,2) + s2*(-7 - 2*t1 + 5*t2)) - 
                 s1*(61 + 7*Power(t1,2) + t1*(18 - 28*t2) + 30*t2 + 
                    7*Power(t2,2) + s2*(8 - 20*t1 + 41*t2))) - 
              2*(-4 + 2*Power(s1,4) - 3*t1 + 2*s2*t1 + 
                 Power(s1,3)*(-3 + 9*s2 + t1 - 3*t2) + 5*t2 - 2*s2*t2 + 
                 t1*t2 - Power(t2,2) + 
                 Power(s1,2)*
                  (14 + 4*Power(s2,2) + 11*t1 + 2*Power(t1,2) + 
                    2*s2*(-8 + t1 - 3*t2) + 7*t2 - t1*t2 - Power(t2,2)) \
+ Power(s,2)*(-1 + Power(s1,3) + t1 + Power(s1,2)*(-5 + t1 - t2) - 
                    t2 + s1*(9 - 6*t1 + 6*t2)) + 
                 s1*(23 + 4*Power(s2,2) + 23*t1 + 2*Power(t1,2) - 
                    33*t2 - 8*t1*t2 + 6*Power(t2,2) + 
                    s2*(-1 - 12*t1 + 8*t2)) - 
                 s*(-5 + Power(s1,4) + s2*t1 + 
                    Power(s1,3)*(-1 + s2 + t1 - t2) - t2 - s2*t2 + 
                    Power(s1,2)*(-8 + (-6 + s2)*t1 + 3*t2 - s2*t2) + 
                    s1*(29 + t1 + 3*t2 + s2*(7 - 6*t1 + 6*t2))))*R1(s1)))/
          ((-1 + s2)*(-s + s2 - t1)) + 
         ((-1 + s1)*(-1 + t2)*
            (-1 + 27*s1 + 69*Power(s1,2) + 53*Power(s1,3) - 
              8*Power(s1,4) - 3*s2 + 26*s1*s2 - 3*Power(s1,2)*s2 - 
              Power(s1,3)*s2 - Power(s1,4)*s2 - s1*Power(s2,2) - 
              Power(s1,2)*Power(s2,2) + t1 + s1*t1 + 71*Power(s1,2)*t1 - 
              2*Power(s1,3)*t1 - Power(s1,4)*t1 + s2*t1 + 
              2*Power(s1,2)*s2*t1 - Power(s1,3)*s2*t1 - s1*Power(t1,2) + 
              6*Power(s1,2)*Power(t1,2) - Power(s1,3)*Power(t1,2) + 
              6*t2 - 65*s1*t2 - 66*Power(s1,2)*t2 + 13*Power(s1,3)*t2 + 
              4*s2*t2 - 9*s1*s2*t2 + 33*Power(s1,2)*s2*t2 - 
              2*Power(s1,3)*s2*t2 - Power(s2,2)*t2 + 
              3*s1*Power(s2,2)*t2 + 3*t1*t2 - 22*s1*t1*t2 - 
              40*Power(s1,2)*t1*t2 + 5*Power(s1,3)*t1*t2 - 
              7*s1*s2*t1*t2 + Power(s1,2)*s2*t1*t2 - 5*Power(t2,2) + 
              44*s1*Power(t2,2) - 3*Power(s1,2)*Power(t2,2) + 
              s2*Power(t2,2) - 10*s1*s2*Power(t2,2) + 
              Power(s1,2)*s2*Power(t2,2) + 14*s1*t1*Power(t2,2) - 
              2*Power(s1,2)*t1*Power(t2,2) - 
              2*Power(s,2)*(2 + t2 + Power(s1,2)*(3 + t2) - 
                 s1*(13 + 10*t2)) + 
              s*(6 + t1 - 7*t2 - t1*t2 - Power(t2,2) + 
                 s2*(4 - t1 + 3*t2) + Power(s1,3)*(8 + t1 + 3*t2) + 
                 s1*(-52 - 5*t1 - 7*Power(t1,2) + 
                    s2*(-26 + 3*t1 - 23*t2) + 53*t2 + 31*t1*t2 + 
                    10*Power(t2,2)) + 
                 Power(s1,2)*(-26 - 5*t1 + Power(t1,2) - 41*t2 - 
                    4*t1*t2 - Power(t2,2) + 2*s2*(5 + t2))) + 
              (-1 - 3*s2 + t1 + s2*t1 + Power(s1,4)*(-24 + s2 + t1) + 
                 6*t2 + 4*s2*t2 - Power(s2,2)*t2 + 3*t1*t2 - 
                 5*Power(t2,2) + s2*Power(t2,2) + 
                 Power(s1,3)*
                  (53 - Power(s2,2) + s2*(-19 + 3*t1 - 4*t2) + 
                    t1*(-10 + t2) + 29*t2) - 
                 2*Power(s,2)*
                  (2 + t2 + Power(s1,2)*(4 + t2) - 2*s1*(5 + 3*t2)) + 
                 s1*(3 + 4*Power(t1,2) + t1*(2 - 21*t2) - 23*t2 + 
                    26*Power(t2,2) + Power(s2,2)*(-5 + 6*t2) + 
                    s2*(13 + t1 - 20*t2 - 6*Power(t2,2))) + 
                 Power(s1,2)*
                  (33 + 4*Power(t1,2) + t1*(38 - 7*t2) - 
                    Power(s2,2)*(-2 + t2) - 56*t2 - 9*Power(t2,2) + 
                    s2*(20 - 13*t1 + 36*t2 + Power(t2,2))) - 
                 s*(-6 - t1 + Power(s1,3)*(-28 + t1 - 3*t2) + 
                    s2*(-4 + t1 - 3*t2) + 7*t2 + t1*t2 + Power(t2,2) + 
                    s1*(20 + t1*(3 - 6*t2) - 33*t2 - 6*Power(t2,2) + 
                       s2*(16 - 6*t1 + 18*t2)) + 
                    Power(s1,2)*
                     (46 + t1*(-11 + t2) + 37*t2 + Power(t2,2) + 
                       s2*(t1 - 3*(4 + t2)))))*R1(s1)))/
          ((s - s2 + t1)*(s - s1 + t2))))/
     (Power(-1 + s1,3)*s1*Power(-1 + t2,2)) + 
    (8*(((-1 + s2)*(-2 + 47*s2 - 6*s*s2 - 17*Power(s2,2) + 
              10*s*Power(s2,2) + 30*Power(s2,3) - 14*t1 + 2*s*t1 + 
              36*s2*t1 + 7*s*s2*t1 - 28*Power(s2,2)*t1 - 
              11*s*Power(s2,2)*t1 + 2*Power(s2,3)*t1 - 2*s*Power(t1,2) + 
              49*s2*Power(t1,2) - s*s2*Power(t1,2) - 
              7*Power(s2,2)*Power(t1,2) + s*Power(s2,2)*Power(t1,2) + 
              s1*(-(Power(s2,3)*(-1 + t1)) + s2*(-1 + t1)*t1 + 
                 2*(-1 + Power(t1,2)) - 
                 Power(s2,2)*(9 - 10*t1 + Power(t1,2))) + 6*t2 - 
              11*s2*t2 + 7*s*s2*t2 + 62*Power(s2,2)*t2 - 
              s*Power(s2,2)*t2 - 5*Power(s2,3)*t2 + 4*t1*t2 - 
              5*s2*t1*t2 - 7*s*s2*t1*t2 - 12*Power(s2,2)*t1*t2 + 
              s*Power(s2,2)*t1*t2 + Power(s2,3)*t1*t2 - 
              2*Power(t1,2)*t2 - 8*s2*Power(t1,2)*t2 + 
              2*Power(s2,2)*Power(t1,2)*t2 + 
              2*(-1 - 7*t1 + s*t1 - s*Power(t1,2) + 
                 Power(s2,3)*(3 + t1) + 
                 s1*(-1 + t1)*
                  (1 + t1 + Power(s2,2)*(5 + t1) - 2*s2*(1 + 3*t1)) + 
                 3*t2 + 2*t1*t2 - Power(t1,2)*t2 - 
                 Power(s2,2)*
                  (-3 + 15*t1 + s*(-2 + t1 + Power(t1,2)) - 17*t2 + 
                    Power(t1,2)*t2) + 
                 s2*(-1 + 41*t1 + s*(2 - 8*t1 + 6*Power(t1,2)) - 4*t2 - 
                    14*t1*t2 + 6*Power(t1,2)*t2))*R1(s2)))/(-1 + t2) + 
         2*(1 - 14*s2 + 7*s1*s2 + 46*Power(s2,2) - 9*s1*Power(s2,2) - 
            9*Power(s2,3) + 2*s1*Power(s2,3) + 
            s*(-1 - 8*s2 + Power(s2,2))*(-1 + t1) + 6*t1 + 29*s2*t1 - 
            7*s1*s2*t1 + 10*Power(s2,2)*t1 + 9*s1*Power(s2,2)*t1 + 
            3*Power(s2,3)*t1 - 2*s1*Power(s2,3)*t1 - Power(t1,2) - 
            15*s2*Power(t1,2) + 10*Power(s2,2)*Power(t1,2) - 
            2*Power(s2,3)*Power(t1,2) - 13*s2*t2 + 17*Power(s2,2)*t2 - 
            4*Power(s2,3)*t2 + 7*s2*t1*t2 - 9*Power(s2,2)*t1*t2 + 
            2*Power(s2,3)*t1*t2 + 
            (1 + s*(-1 + 8*s2 + Power(s2,2))*(-1 + t1) + 6*t1 - 
               Power(t1,2) + Power(s2,3)*
                (-11 + s1*(-1 + t1) + Power(t1,2) + 3*t2 - t1*(14 + t2)) \
+ s2*(7 + 3*s1*(-1 + t1) + 11*Power(t1,2) + 9*t2 - t1*(50 + 3*t2)) + 
               Power(s2,2)*(27 - 4*s1*(-1 + t1) - 3*Power(t1,2) - 
                  12*t2 + t1*(58 + 4*t2)))*R1(s2)) + 
         ((-1 + t1)*(3 - 46*s2 + 3*s1*s2 - 20*Power(s2,2) - 
              49*Power(s2,3) - 4*s1*Power(s2,3) + 8*Power(s2,4) + 
              s1*Power(s2,4) + t1 + 3*s1*t1 - 22*s2*t1 - 2*s1*s2*t1 - 
              34*Power(s2,2)*t1 - 2*s1*Power(s2,2)*t1 + 
              9*Power(s2,3)*t1 + s1*Power(s2,3)*t1 - 2*Power(s2,4)*t1 + 
              30*s2*Power(t1,2) - 8*Power(s2,2)*Power(t1,2) + 
              2*Power(s2,3)*Power(t1,2) - 3*t2 + 5*s2*t2 + 
              2*Power(s2,2)*t2 - 5*Power(s2,3)*t2 + Power(s2,4)*t2 - 
              8*s2*t1*t2 + 10*Power(s2,2)*t1*t2 - 2*Power(s2,3)*t1*t2 + 
              s*(-2 - 3*t1 + 2*s2*(13 + 16*t1 - 5*t2) + 
                 Power(s2,3)*(4 + t1 - t2) + 3*t2 + 
                 Power(s2,2)*(-20 - 6*t1 + 8*t2)) + 
              (3 - 2*(-3 + s1)*Power(s2,4) + t1 + 3*s1*t1 + 
                 Power(s2,3)*(-26 + 13*s1 + 12*t1 - 15*t2) - 3*t2 + 
                 s2*(-18 + s1*(7 - 12*t1) + 7*t2) + 
                 Power(s2,2)*(-29 + 9*s1*(-2 + t1) - 13*t1 + 11*t2) + 
                 s*(-2 - 4*Power(s2,3) - 3*t1 + 
                    4*s2*(5 + 3*t1 - 3*t2) + 3*t2 + 
                    Power(s2,2)*(2 - 9*t1 + 9*t2)))*R1(s2)))/(s - s2 + t1) \
- ((-1 + s2)*(-1 + t1)*(8 - 69*s2 - 4*s1*s2 + 8*Power(s1,2)*s2 - 
              48*Power(s2,2) + 57*s1*Power(s2,2) - 
              18*Power(s1,2)*Power(s2,2) + 9*Power(s2,3) - 
              36*s1*Power(s2,3) + 2*Power(s1,2)*Power(s2,3) - 
              16*Power(s2,4) + 3*s1*Power(s2,4) - 10*t1 + 4*s1*t1 + 
              76*s2*t1 + 3*s1*s2*t1 - 14*Power(s1,2)*s2*t1 - 
              17*Power(s2,2)*t1 + 11*s1*Power(s2,2)*t1 + 
              2*Power(s1,2)*Power(s2,2)*t1 + 17*Power(s2,3)*t1 + 
              2*Power(t1,2) + s2*Power(t1,2) - 7*s1*s2*Power(t1,2) - 
              Power(s2,2)*Power(t1,2) + s1*Power(s2,2)*Power(t1,2) + 
              6*t2 - 4*s1*t2 - 82*s2*t2 + 10*s1*s2*t2 - 
              40*Power(s2,2)*t2 - 3*s1*Power(s2,2)*t2 - 
              5*Power(s2,3)*t2 - s1*Power(s2,3)*t2 - Power(s2,4)*t2 - 
              2*t1*t2 + 20*s2*t1*t2 + 7*s1*s2*t1*t2 + 
              9*Power(s2,2)*t1*t2 - s1*Power(s2,2)*t1*t2 - 
              Power(s2,3)*t1*t2 + s2*Power(t2,2) - 
              6*Power(s2,2)*Power(t2,2) + Power(s2,3)*Power(t2,2) + 
              Power(s,2)*(2*(1 + t1 - t2) + 
                 Power(s2,2)*(2 + 3*t1 - t2) + s2*(-8 - 27*t1 + 13*t2)) \
- s*(Power(s2,3)*(-8 + s1 + t1 - 2*t2) + 2*(5 + t1 + s1*t1 - s1*t2) + 
                 Power(s2,2)*
                  (13 + 5*t1 + Power(t1,2) + s1*(-7 + 5*t1 - 2*t2) + 
                    12*t2 - 4*t1*t2 + Power(t2,2)) - 
                 s2*(61 + 30*t1 + 7*Power(t1,2) + 
                    s1*(8 + 41*t1 - 20*t2) + 18*t2 - 28*t1*t2 + 
                    7*Power(t2,2))) - 
              2*(-4 + 2*Power(s2,4) + 5*t1 - 2*s1*t1 - Power(t1,2) - 
                 3*t2 + 2*s1*t2 + t1*t2 + 
                 Power(s2,3)*(-3 + 9*s1 - 3*t1 + t2) + 
                 Power(s2,2)*
                  (14 + 4*Power(s1,2) - Power(t1,2) - 
                    2*s1*(8 + 3*t1 - t2) - t1*(-7 + t2) + 11*t2 + 
                    2*Power(t2,2)) + 
                 Power(s,2)*(-1 + Power(s2,3) - t1 + 
                    s2*(9 + 6*t1 - 6*t2) + t2 + 
                    Power(s2,2)*(-5 - t1 + t2)) + 
                 s2*(23 + 4*Power(s1,2) + 6*Power(t1,2) + 
                    s1*(-1 + 8*t1 - 12*t2) + 23*t2 + 2*Power(t2,2) - 
                    t1*(33 + 8*t2)) - 
                 s*(-5 + Power(s2,4) - (1 + s1)*t1 + s1*t2 + 
                    Power(s2,3)*(-1 + s1 - t1 + t2) + 
                    s2*(29 + 3*t1 + s1*(7 + 6*t1 - 6*t2) + t2) + 
                    Power(s2,2)*(-8 - (-3 + s1)*t1 + (-6 + s1)*t2)))*
               R1(s2)))/((-1 + s1)*(-s + s1 - t2)) - 
         ((-1 + s2)*(-1 + t1)*
            (8 - s1 - 40*s2 + s1*s2 + Power(s1,2)*s2 - 6*Power(s2,2) - 
              4*s1*Power(s2,2) + Power(s1,2)*Power(s2,2) + 
              4*Power(s2,3) - 12*s1*Power(s2,3) + t1 - 3*s1*t1 + 
              Power(s1,2)*t1 - 55*s2*t1 + 28*s1*s2*t1 - 
              3*Power(s1,2)*s2*t1 + 2*Power(s2,2)*t1 - 
              13*s1*Power(s2,2)*t1 - 8*Power(s2,3)*t1 + 
              2*s1*Power(s2,3)*t1 + Power(t1,2) + s1*Power(t1,2) - 
              3*s2*Power(t1,2) - 3*s1*s2*Power(t1,2) + 3*t2 - 3*s1*t2 - 
              57*s2*t2 + s1*s2*t2 + 52*Power(s2,2)*t2 - 
              30*s1*Power(s2,2)*t2 - 16*Power(s2,3)*t2 + 
              2*s1*Power(s2,3)*t2 - s1*t1*t2 + 21*s2*t1*t2 + 
              3*s1*s2*t1*t2 + Power(s2,2)*t1*t2 + Power(t2,2) - 
              2*s2*Power(t2,2) - 3*Power(s2,2)*Power(t2,2) + 
              Power(s,2)*(-1 - 4*s2 + Power(s2,2))*(-2 + t1 + t2) + 
              s*(-7 + 3*t1 - Power(t1,2) + Power(s2,3)*(s1 - t2) + 
                 3*t2 + s1*t2 - Power(t2,2) + 
                 Power(s2,2)*
                  (13 - 7*t1 - s1*(8 + t1) + 15*t2 + 2*t1*t2) + 
                 s2*(10 + 30*t1 + 3*Power(t1,2) + 
                    s1*(-1 + 7*t1 - 3*t2) + 17*t2 - 14*t1*t2 + 
                    3*Power(t2,2))) + 
              (8 - 57*s2 + 7*Power(s2,3) - 2*Power(s2,4) + t1 + 
                 3*s2*t1 - Power(s2,2)*t1 - 3*Power(s2,3)*t1 + 
                 Power(t1,2) - 6*s2*Power(t1,2) + 
                 Power(s2,2)*Power(t1,2) + 
                 Power(s1,2)*
                  (Power(s2,3) + s2*(5 - 6*t1) + 
                    Power(s2,2)*(-2 + t1) + t1) + 3*t2 - 7*s2*t2 + 
                 22*Power(s2,2)*t2 - 7*Power(s2,3)*t2 + 
                 Power(s2,4)*t2 - 5*s2*t1*t2 + 2*Power(s2,2)*t1*t2 - 
                 Power(s2,3)*t1*t2 + Power(t2,2) - 5*s2*Power(t2,2) - 
                 5*Power(s2,2)*Power(t2,2) + Power(s2,3)*Power(t2,2) - 
                 Power(s,2)*(1 - 6*s2 + Power(s2,2))*(-2 + t1 + t2) + 
                 s1*(-1 - 6*Power(s2,3) + Power(s2,4) + Power(t1,2) - 
                    3*t2 - t1*(3 + t2) + 
                    Power(s2,2)*
                     (-4 + Power(t1,2) - 15*t2 - t1*(7 + t2)) + 
                    s2*(2 - 6*Power(t1,2) + 6*t2 + 2*t1*(7 + 3*t2))) - 
                 s*(7 + Power(s2,3)*(-2 + s1 - t1) - 3*t1 + 
                    Power(t1,2) - 3*t2 - s1*t2 + Power(t2,2) + 
                    Power(s2,2)*
                     (7 - t1 + Power(t1,2) - 7*t2 + Power(t2,2) - 
                       s1*(2 + t2)) + 
                    s2*(-56 + 13*t1 - 6*Power(t1,2) + 14*t2 - 
                       6*Power(t2,2) + s1*(5 + 6*t2))))*R1(s2)))/
          ((-1 + s1)*(-1 + t2)) + 
         ((-1 + s2)*(-1 + t1)*
            (-1 - 3*s1 + 27*s2 + 26*s1*s2 - Power(s1,2)*s2 + 
              69*Power(s2,2) - 3*s1*Power(s2,2) - 
              Power(s1,2)*Power(s2,2) + 53*Power(s2,3) - s1*Power(s2,3) - 
              8*Power(s2,4) - s1*Power(s2,4) + 6*t1 + 4*s1*t1 - 
              Power(s1,2)*t1 - 65*s2*t1 - 9*s1*s2*t1 + 
              3*Power(s1,2)*s2*t1 - 66*Power(s2,2)*t1 + 
              33*s1*Power(s2,2)*t1 + 13*Power(s2,3)*t1 - 
              2*s1*Power(s2,3)*t1 - 5*Power(t1,2) + s1*Power(t1,2) + 
              44*s2*Power(t1,2) - 10*s1*s2*Power(t1,2) - 
              3*Power(s2,2)*Power(t1,2) + s1*Power(s2,2)*Power(t1,2) - 
              2*Power(s,2)*(2 + t1 + Power(s2,2)*(3 + t1) - 
                 s2*(13 + 10*t1)) + t2 + s1*t2 + s2*t2 + 
              71*Power(s2,2)*t2 + 2*s1*Power(s2,2)*t2 - 
              2*Power(s2,3)*t2 - s1*Power(s2,3)*t2 - Power(s2,4)*t2 + 
              3*t1*t2 - 22*s2*t1*t2 - 7*s1*s2*t1*t2 - 
              40*Power(s2,2)*t1*t2 + s1*Power(s2,2)*t1*t2 + 
              5*Power(s2,3)*t1*t2 + 14*s2*Power(t1,2)*t2 - 
              2*Power(s2,2)*Power(t1,2)*t2 - s2*Power(t2,2) + 
              6*Power(s2,2)*Power(t2,2) - Power(s2,3)*Power(t2,2) + 
              s*(6 - 7*t1 - Power(t1,2) + t2 - t1*t2 + 
                 Power(s2,3)*(8 + 3*t1 + t2) + 
                 s2*(-52 + 53*t1 + 10*Power(t1,2) - 5*t2 + 31*t1*t2 - 
                    7*Power(t2,2)) - 
                 Power(s2,2)*
                  (26 + 41*t1 + Power(t1,2) + 5*t2 + 4*t1*t2 - 
                    Power(t2,2)) + 
                 s1*(4 + 3*t1 + 2*Power(s2,2)*(5 + t1) - t2 + 
                    s2*(-26 - 23*t1 + 3*t2))) + 
              (-1 + 3*s2 + 33*Power(s2,2) + 53*Power(s2,3) - 
                 24*Power(s2,4) + 6*t1 - 23*s2*t1 - 56*Power(s2,2)*t1 + 
                 29*Power(s2,3)*t1 - 5*Power(t1,2) + 26*s2*Power(t1,2) - 
                 9*Power(s2,2)*Power(t1,2) - 
                 Power(s1,2)*
                  (Power(s2,3) + s2*(5 - 6*t1) + 
                    Power(s2,2)*(-2 + t1) + t1) - 
                 2*Power(s,2)*
                  (2 + t1 + Power(s2,2)*(4 + t1) - 2*s2*(5 + 3*t1)) + 
                 t2 + 2*s2*t2 + 38*Power(s2,2)*t2 - 10*Power(s2,3)*t2 + 
                 Power(s2,4)*t2 + 3*t1*t2 - 21*s2*t1*t2 - 
                 7*Power(s2,2)*t1*t2 + Power(s2,3)*t1*t2 + 
                 4*s2*Power(t2,2) + 4*Power(s2,2)*Power(t2,2) + 
                 s1*(-3 + Power(s2,4) + 4*t1 + Power(t1,2) + 
                    Power(s2,2)*(20 + 36*t1 + Power(t1,2) - 13*t2) + 
                    t2 + s2*(13 - 20*t1 - 6*Power(t1,2) + t2) + 
                    Power(s2,3)*(-19 - 4*t1 + 3*t2)) + 
                 s*(6 - 7*t1 - Power(t1,2) + 
                    Power(s2,3)*(28 + 3*t1 - t2) + 
                    s1*(4 + 3*t1 - 2*s2*(8 + 9*t1 - 3*t2) + 
                       Power(s2,2)*(12 + 3*t1 - t2) - t2) + t2 - t1*t2 - 
                    Power(s2,2)*
                     (46 + Power(t1,2) - 11*t2 + t1*(37 + t2)) + 
                    s2*(-20 + 6*Power(t1,2) - 3*t2 + t1*(33 + 6*t2))))*
               R1(s2)))/((s - s2 + t1)*(s - s1 + t2))))/
     (Power(-1 + s2,3)*s2*Power(-1 + t1,2)) + 
    (8*(-2*(-6 + 2*s2 + 6*s1*s2 - 79*Power(s2,2) - 8*s1*Power(s2,2) + 
            4*Power(s2,3) + 2*s1*Power(s2,3) - 11*Power(s2,4) + 
            2*Power(s2,5) + 4*t1 + 4*s2*t1 - 9*Power(s2,2)*t1 + 
            21*Power(s2,3)*t1 - 4*Power(s2,4)*t1 + Power(t1,2) + 
            15*s2*Power(t1,2) - 10*Power(s2,2)*Power(t1,2) + 
            2*Power(s2,3)*Power(t1,2) - 6*s2*t2 + 15*Power(s2,2)*t2 - 
            11*Power(s2,3)*t2 + 2*Power(s2,4)*t2 - 7*s2*t1*t2 + 
            9*Power(s2,2)*t1*t2 - 2*Power(s2,3)*t1*t2 + 
            s*(4 - 2*Power(s2,4) + t1 + s2*(-1 + 15*t1 - 7*t2) + 
               Power(s2,3)*(9 + 2*t1 - 2*t2) + 
               Power(s2,2)*(14 - 10*t1 + 9*t2)) + 
            (-6 - Power(s2,5) + 4*t1 + Power(t1,2) + 
               Power(s2,2)*(-42 + 8*s1 + 3*Power(t1,2) + 
                  t1*(78 - 4*t2) - 11*t2) - 
               Power(s2,3)*(45 + 2*s1 + Power(t1,2) - t1*(-19 + t2) - 
                  6*t2) + Power(s2,4)*(16 + 2*t1 - t2) + 
               s2*(22 - 6*s1 - 11*Power(t1,2) + 3*t1*(-11 + t2) + 
                  6*t2) + s*(4 + Power(s2,4) + t1 + 
                  Power(s2,2)*(59 + 3*t1 - 4*t2) + 
                  Power(s2,3)*(-14 - t1 + t2) + s2*(-26 - 11*t1 + 3*t2)))*
             R1(s2)) + ((s - s2 + t1)*
            (1 - t1 + 3*s*t1 - 3*s1*t1 + 3*t2 - 3*s*t2 + 
              Power(s2,4)*(10 + s1 - 2*t1 + t2) + 
              Power(s2,3)*(-55 + s1*(-2 + t1) + 2*Power(t1,2) + 
                 t1*(5 + s - 2*t2) - 3*t2 - s*t2) - 
              2*Power(s2,2)*(4*Power(t1,2) + t1*(10 + 4*s1 - 5*t2) - 
                 s*(-5 + t2) + 2*(5 + t2)) + 
              s2*(-40 + s1 - 30*t1 + 10*s1*t1 + 30*Power(t1,2) + 3*t2 - 
                 8*t1*t2 + 2*s*(9 + 10*t1 + t2)) - 
              (-1 + 2*(-9 + s1)*Power(s2,4) + t1 - 3*s*t1 + 3*s1*t1 + 
                 Power(s2,3)*(42 + 10*s - 7*s1 + 12*t1 - 15*t2) - 
                 3*t2 + 3*s*t2 + 
                 s2*(10 - 5*s1 - 12*s1*t1 + 2*s*(-5 + 6*t1 - 6*t2) + 
                    7*t2) + Power(s2,2)*
                  (31 + 10*s1 - 13*t1 + 9*s1*t1 + 11*t2 + 
                    s*(-16 - 9*t1 + 9*t2)))*R1(s2)))/(-1 + t1) + 
         ((-1 + s2)*(8 - 8*s1 - 94*s2 + 20*s1*s2 - 111*Power(s2,2) - 
              32*s1*Power(s2,2) - 19*Power(s2,3) + 11*s1*Power(s2,3) + 
              2*Power(s2,4) - s1*Power(s2,4) - 14*t1 + 4*s1*t1 + 
              109*s2*t1 - 20*s1*s2*t1 + 32*Power(s2,2)*t1 - 
              14*s1*Power(s2,2)*t1 - 7*Power(s2,3)*t1 + 
              2*s1*Power(s2,3)*t1 + 2*Power(t1,2) - 41*s2*Power(t1,2) + 
              7*s1*s2*Power(t1,2) + 5*Power(s2,2)*Power(t1,2) - 
              s1*Power(s2,2)*Power(t1,2) + 8*t2 - 24*s2*t2 + 
              29*Power(s2,2)*t2 - 20*Power(s2,3)*t2 + 3*Power(s2,4)*t2 + 
              25*s2*t1*t2 + 24*Power(s2,2)*t1*t2 - 5*Power(s2,3)*t1*t2 - 
              2*Power(t1,2)*t2 - 8*s2*Power(t1,2)*t2 + 
              2*Power(s2,2)*Power(t1,2)*t2 + 
              Power(s,2)*(-2*Power(s2,3) - 2*t2 + 
                 Power(s2,2)*(12 + t1 + t2) - s2*(22 + 7*t1 + t2)) + 
              s*(-6 + 2*Power(s2,4) + 2*t1 + 
                 s1*(4 + Power(s2,3) - Power(s2,2)*(7 + t1) + 
                    s2*(-20 + 7*t1)) - 4*t1*t2 - 
                 Power(s2,3)*(18 + 3*t1 + 4*t2) + 
                 s2*(87 - 7*Power(t1,2) + 25*t2 - 9*t1*(7 + t2)) + 
                 Power(s2,2)*(65 + Power(t1,2) + 17*t2 + 3*t1*(8 + t2))) \
- 2*(-4 + 4*s1 + 21*s2 - 10*s1*s2 + 42*Power(s2,2) + 6*s1*Power(s2,2) - 
                 7*Power(s2,3) + 4*s1*Power(s2,3) + 7*t1 - 2*s1*t1 - 
                 37*s2*t1 + 10*s1*s2*t1 + 5*Power(s2,2)*t1 - 
                 4*s1*Power(s2,2)*t1 + Power(s2,3)*t1 - Power(t1,2) + 
                 6*s2*Power(t1,2) - Power(s2,2)*Power(t1,2) - 4*t2 + 
                 12*s2*t2 - 13*Power(s2,2)*t2 - 4*Power(s2,3)*t2 + 
                 Power(s2,4)*t2 + 
                 Power(s,2)*(1 - 6*s2 + Power(s2,2))*t2 - 4*s2*t1*t2 + 
                 10*Power(s2,2)*t1*t2 - 2*Power(s2,3)*t1*t2 + 
                 Power(t1,2)*t2 - 6*s2*Power(t1,2)*t2 + 
                 Power(s2,2)*Power(t1,2)*t2 - 
                 s*(-3 + 2*s1*(1 - 5*s2 + 2*Power(s2,2)) + t1 + 
                    2*Power(s2,3)*t2 - 2*t1*t2 - 
                    Power(s2,2)*(5 + t1)*(-1 + 2*t2) + 
                    2*s2*(13 - 3*t1 + 2*t2 + 6*t1*t2)))*R1(s2)))/
          (s - s1 + t2) + ((-1 + s2)*(-s + s2 - t1)*
            (-14 + 2*s1 + 2*Power(s1,2) + 133*s2 + 12*s1*s2 - 
              7*Power(s2,2) - 47*s1*Power(s2,2) + 
              32*Power(s1,2)*Power(s2,2) + 20*Power(s2,3) + 
              39*s1*Power(s2,3) - 2*Power(s1,2)*Power(s2,3) + 
              8*Power(s2,4) - 2*s1*Power(s2,4) + 3*t1 - 2*s1*t1 - 
              38*s2*t1 + 48*s1*s2*t1 - 3*Power(s2,2)*t1 - 
              18*s1*Power(s2,2)*t1 - 8*Power(s2,3)*t1 + 
              2*s1*Power(s2,3)*t1 - Power(t1,2) - s1*Power(t1,2) + 
              3*s2*Power(t1,2) + 3*s1*s2*Power(t1,2) + 5*t2 - 2*s1*t2 - 
              40*s2*t2 - s1*s2*t2 + 68*Power(s2,2)*t2 - 
              33*s1*Power(s2,2)*t2 - 15*Power(s2,3)*t2 + 
              2*s1*Power(s2,3)*t2 + s1*t1*t2 - 21*s2*t1*t2 - 
              3*s1*s2*t1*t2 - Power(s2,2)*t1*t2 - Power(t2,2) + 
              2*s2*Power(t2,2) + 3*Power(s2,2)*Power(t2,2) + 
              Power(s,2)*(t1 + t2 - Power(s2,2)*(-10 + t1 + t2) + 
                 2*s2*(7 + 2*t1 + 2*t2)) + 
              s*(11 + s1*(-6 + 3*Power(s2,3) + s2*(35 - 4*t1) + 
                    Power(s2,2)*(-52 + t1) - t1) - t1 + Power(t1,2) + 
                 Power(s2,3)*(-20 + t2) - t2 + Power(t2,2) + 
                 Power(s2,2)*(21 + 8*t2 - 2*t1*t2) - 
                 s2*(128 - 5*t1 + 3*Power(t1,2) + 8*t2 - 14*t1*t2 + 
                    3*Power(t2,2))) + 
              (-14 + 74*s2 - 6*Power(s2,2) + 6*Power(s2,3) + 
                 4*Power(s2,4) - 
                 2*Power(s1,2)*
                  (-1 + 3*s2 - 11*Power(s2,2) + Power(s2,3)) + 3*t1 - 
                 12*s2*t1 - 9*Power(s2,2)*t1 - 2*Power(s2,3)*t1 - 
                 Power(t1,2) + 6*s2*Power(t1,2) - 
                 Power(s2,2)*Power(t1,2) + 5*t2 - 22*s2*t2 + 
                 9*Power(s2,2)*t2 - 4*Power(s2,3)*t2 + 5*s2*t1*t2 - 
                 2*Power(s2,2)*t1*t2 + Power(s2,3)*t1*t2 - 
                 Power(t2,2) + 5*s2*Power(t2,2) + 
                 5*Power(s2,2)*Power(t2,2) - Power(s2,3)*Power(t2,2) + 
                 Power(s,2)*(t1 + t2 - 6*s2*(-2 + t1 + t2) + 
                    Power(s2,2)*(12 + t1 + t2)) + 
                 s1*(2 - 2*Power(s2,4) - Power(t1,2) + t1*(-2 + t2) - 
                    2*t2 + Power(s2,3)*(22 + t1 + t2) + 
                    s2*(2 + 6*Power(t1,2) + t1*(5 - 6*t2) + t2) - 
                    Power(s2,2)*
                     (-8 + Power(t1,2) - t1*(-16 + t2) + 20*t2)) + 
                 s*(11 - t1 + Power(t1,2) + 
                    s1*(-6 + 3*Power(s2,3) - t1 - 
                       Power(s2,2)*(44 + t1) + s2*(19 + 6*t1)) - t2 + 
                    Power(t2,2) - Power(s2,3)*(14 + t2) + 
                    s2*(-76 + 14*t1 - 6*Power(t1,2) + 13*t2 - 
                       6*Power(t2,2)) + 
                    Power(s2,2)*
                     (3 + 7*t1 + Power(t1,2) + 13*t2 + Power(t2,2))))*
               R1(s2)))/((-1 + s1)*(-s + s1 - t2)) - 
         ((-1 + s2)*(-s + s2 - t1)*
            (-4 - 7*s1 + 3*s2 + 7*Power(s1,2)*s2 - 59*Power(s2,2) - 
              46*s1*Power(s2,2) - 4*Power(s1,2)*Power(s2,2) - 
              10*Power(s2,3) + 7*s1*Power(s2,3) + 
              Power(s1,2)*Power(s2,3) - 4*Power(s2,4) - t1 + 9*s1*t1 - 
              Power(s1,2)*t1 + 19*s2*t1 - 28*s1*s2*t1 - 
              4*Power(s1,2)*s2*t1 + Power(s1,2)*Power(s2,2)*t1 + 
              8*Power(s2,3)*t1 + s1*Power(s2,3)*t1 + 5*Power(t1,2) - 
              s1*Power(t1,2) - 58*s2*Power(t1,2) - 4*s1*s2*Power(t1,2) + 
              5*Power(s2,2)*Power(t1,2) + s1*Power(s2,2)*Power(t1,2) + 
              4*t2 + s1*t2 - 6*s2*t2 - 9*s1*s2*t2 + 47*Power(s2,2)*t2 + 
              8*s1*Power(s2,2)*t2 - 29*Power(s2,3)*t2 - 
              2*s1*Power(s2,3)*t2 + 2*Power(s2,4)*t2 - 3*t1*t2 - 
              6*s2*t1*t2 + 7*s1*s2*t1*t2 + 16*Power(s2,2)*t1*t2 - 
              s1*Power(s2,2)*t1*t2 - Power(s2,3)*t1*t2 + 
              14*s2*Power(t1,2)*t2 - 2*Power(s2,2)*Power(t1,2)*t2 + 
              s2*Power(t2,2) - 6*Power(s2,2)*Power(t2,2) + 
              Power(s2,3)*Power(t2,2) - 
              Power(s,2)*(-1 + 3*s2)*(-2 + t1 + t2) - 
              s*(-5 + 3*t1 - Power(t1,2) + 2*Power(s2,3)*(-2 + t2) + 
                 3*t2 - t1*t2 + 
                 Power(s2,2)*
                  (-20 + t1 + Power(t1,2) - 28*t2 + Power(t2,2)) + 
                 s1*(-4 + Power(s2,2)*(5 + t1 - t2) + t2 + 
                    s2*(5 - 7*t1 + 4*t2)) + 
                 s2*(-4*Power(t1,2) + t1*(20 + 3*t2) - 
                    7*(-1 + t2 + Power(t2,2)))) + 
              (-4 + 10*s2 - 6*Power(s2,2) - 18*Power(s2,3) + 
                 2*Power(s2,4) - t1 - 2*s2*t1 - 29*Power(s2,2)*t1 + 
                 12*Power(s2,3)*t1 + 5*Power(t1,2) - 
                 26*s2*Power(t1,2) + 9*Power(s2,2)*Power(t1,2) - 
                 Power(s1,2)*
                  (Power(s2,3) + s2*(5 - 6*t1) + 
                    Power(s2,2)*(-2 + t1) + t1) + 4*t2 - 8*s2*t2 + 
                 18*Power(s2,2)*t2 - 16*Power(s2,3)*t2 + 
                 2*Power(s2,4)*t2 - 3*t1*t2 + 21*s2*t1*t2 + 
                 7*Power(s2,2)*t1*t2 - Power(s2,3)*t1*t2 - 
                 4*s2*Power(t2,2) - 4*Power(s2,2)*Power(t2,2) + 
                 Power(s,2)*(1 - 6*s2 + Power(s2,2))*(-2 + t1 + t2) + 
                 s1*(-7 + 9*t1 - Power(t1,2) + t2 + 
                    Power(s2,3)*(4 - t1 + 3*t2) - 
                    Power(s2,2)*(31 - 19*t1 + Power(t1,2) + 5*t2) + 
                    s2*(14 - 51*t1 + 6*Power(t1,2) + 9*t2)) + 
                 s*(5 - 3*t1 + Power(t1,2) - 3*t2 - 3*Power(s2,3)*t2 + 
                    t1*t2 + s2*
                     (-22 - 6*Power(t1,2) - 6*t1*(-3 + t2) + 15*t2) + 
                    Power(s2,2)*
                     (13 + Power(t1,2) + t1*(-3 + t2) + 15*t2) + 
                    s1*(4 + Power(s2,3) - Power(s2,2)*(-2 + t2) - t2 + 
                       s2*(-19 + 6*t2))))*R1(s2)))/((-1 + t1)*(-1 + t2)) + 
         ((-1 + s2)*(-s + s2 - t1)*
            (-4 - 4*s1 + 4*Power(s1,2) + 61*s2 + 49*s1*s2 - 
              12*Power(s1,2)*s2 + 3*Power(s2,2) - 38*s1*Power(s2,2) + 
              33*Power(s1,2)*Power(s2,2) - 12*Power(s2,3) + 
              27*s1*Power(s2,3) - 3*Power(s1,2)*Power(s2,3) + 
              2*Power(s2,4) - 2*s1*Power(s2,4) - 8*t1 + 2*s1*t1 + 
              102*s2*t1 + 16*s1*s2*t1 - 7*Power(s1,2)*s2*t1 - 
              8*Power(s2,2)*t1 + 7*s1*Power(s2,2)*t1 + 
              Power(s1,2)*Power(s2,2)*t1 + 14*Power(s2,3)*t1 + 
              s1*Power(s2,3)*t1 - 2*Power(t1,2) - s2*Power(t1,2) + 
              7*s1*s2*Power(t1,2) + Power(s2,2)*Power(t1,2) - 
              s1*Power(s2,2)*Power(t1,2) + 4*t2 - 4*s1*t2 - 62*s2*t2 + 
              19*s1*s2*t2 - 23*Power(s2,2)*t2 - 9*s1*Power(s2,2)*t2 + 
              5*Power(s2,3)*t2 - 2*Power(s2,4)*t2 + 2*t1*t2 - 
              20*s2*t1*t2 - 7*s1*s2*t1*t2 - 9*Power(s2,2)*t1*t2 + 
              s1*Power(s2,2)*t1*t2 + Power(s2,3)*t1*t2 - s2*Power(t2,2) + 
              6*Power(s2,2)*Power(t2,2) - Power(s2,3)*Power(t2,2) + 
              Power(s,2)*(s1*(2 - 6*s2) + Power(s2,2)*(-6 + t1 - 3*t2) + 
                 2*(-2 + t1 - t2) + s2*(18 - 13*t1 + 27*t2)) + 
              s*(8 + Power(s1,2)*(-2 + 6*s2) - 4*t1 + 2*t2 + 
                 Power(s2,3)*(6 - 2*t1 + 5*t2) + 
                 Power(s2,2)*
                  (-19 + 4*t1 + Power(t1,2) - 35*t2 - 4*t1*t2 + 
                    Power(t2,2)) - 
                 s2*(53 + 6*t1 + 7*Power(t1,2) + 18*t2 - 28*t1*t2 + 
                    7*Power(t2,2)) + 
                 s1*(Power(s2,3) + s2*(4 + 20*t1 - 13*t2) - 
                    2*(1 + t1 - t2) + Power(s2,2)*(-9 - 2*t1 + t2))) + 
              2*(-2 + 17*s2 + 2*Power(s2,2) + 5*Power(s2,3) - 
                 2*Power(s2,4) + 
                 Power(s1,2)*(2 - 6*s2 + 8*Power(s2,2)) - 4*t1 + 
                 31*s2*t1 - 16*Power(s2,2)*t1 + 5*Power(s2,3)*t1 - 
                 Power(t1,2) + 6*s2*Power(t1,2) - 
                 Power(s2,2)*Power(t1,2) + 
                 Power(s,2)*(1 - 6*s2 + Power(s2,2))*
                  (-2 + s1 + t1 - t2) + 2*t2 - 20*s2*t2 - 
                 6*Power(s2,2)*t2 + t1*t2 - 8*s2*t1*t2 - 
                 Power(s2,2)*t1*t2 + 2*s2*Power(t2,2) + 
                 2*Power(s2,2)*Power(t2,2) + 
                 s1*(3*Power(s2,3) + t1 - 2*(1 + t2) + s2*(13 + 8*t2) + 
                    Power(s2,2)*(7*t1 - 6*(1 + t2))) - 
                 s*(-4 + Power(s1,2)*(1 - 6*s2 + Power(s2,2)) + 2*t1 + 
                    Power(s2,3)*(-3 + t1 - t2) - t2 + 
                    Power(s2,2)*(14 + 3*t2) + s2*(21 - 7*t1 + 3*t2) + 
                    s1*(1 + Power(s2,3) + t1 + 
                       Power(s2,2)*(-1 + t1 - t2) - t2 + 
                       s2*(-1 - 6*t1 + 6*t2))))*R1(s2)))/
          ((-1 + s1)*(-1 + t2))))/(Power(-1 + s2,3)*s2*Power(s - s2 + t1,2)) \
- (8*((2*(-1 + s + t1)*(-2 + s - s2 + t1)*(s - s2 + t1)*
            (-7 - 2*Power(s2,2) + s*(-5 + 2*s2) - 5*t1 + 2*s2*(4 + t1)))/
          (-1 + s - s2 + t1) + 
         (2*(-1 + s2)*(-3 - 2*s + 2*s2 - 2*t1)*Power(s - s2 + t1,2)*
            (1 - s + s1 + s2 - t2))/(1 - s + s2 - t1) - 
         (2*(-2 + s - s2 + t1)*Power(s - s2 + t1,2)*
            (2 + 3*s - 2*s1*(-1 + s2) - 3*s2 + 2*Power(s2,2) + 5*t1 - 
              2*s2*t1 - 2*t2 + 2*s2*t2))/(-1 + s - s2 + t1) + 
         4*(3*Power(s,3) + 22*s2 - 27*Power(s2,2) + 21*Power(s2,3) - 
            2*Power(s2,4) - 22*t1 + 29*s2*t1 - 39*Power(s2,2)*t1 + 
            4*Power(s2,3)*t1 - 2*Power(t1,2) + 18*s2*Power(t1,2) - 
            2*Power(s2,2)*Power(t1,2) + 
            Power(s,2)*(3 - 2*s1*(-1 + s2) + 11*s2 - 2*Power(s2,2) + 
               6*t1) - 2*s1*(-1 + s2)*
             (-4 + Power(s2,2) + t1 + Power(t1,2) - s2*(1 + 2*t1)) + 
            8*t2 - 9*s2*t2 + Power(s2,2)*t2 + t1*t2 - s2*t1*t2 + 
            s*(-14 + 4*Power(s2,3) + 2*s1*(-1 + s2)*(-1 + 2*s2 - 2*t1) + 
               t1 + 3*Power(t1,2) - Power(s2,2)*(35 + 4*t1) + 
               s2*(16 + 29*t1 - t2) + t2)) + 
         (2*(-15*s2 + 6*Power(s2,2) + 12*Power(s2,3) - 3*Power(s2,4) - 
              2*s1*(-1 + s2)*(4 + 3*s2 - 3*t1) + 15*t1 + 3*s2*t1 - 
              31*Power(s2,2)*t1 + 9*Power(s2,3)*t1 - 9*Power(t1,2) + 
              26*s2*Power(t1,2) - 9*Power(s2,2)*Power(t1,2) - 
              7*Power(t1,3) + 3*s2*Power(t1,3) - 8*t2 + 5*s2*t2 + 
              6*Power(s2,2)*t2 - 3*Power(s2,3)*t2 + 3*t1*t2 - 
              9*s2*t1*t2 + 6*Power(s2,2)*t1*t2 + 3*Power(t1,2)*t2 - 
              3*s2*Power(t1,2)*t2 + 
              Power(s,2)*(-10 - 3*Power(s2,2) - 7*t1 + 
                 3*s2*(3 + t1 - t2) + 3*t2) + 
              s*(7 + 6*s1*(-1 + s2) + 6*Power(s2,3) - 19*t1 - 
                 14*Power(t1,2) - 3*Power(s2,2)*(7 + 4*t1 - 2*t2) + 
                 3*t2 + 6*t1*t2 + 
                 s2*(12 + 35*t1 + 6*Power(t1,2) - 9*t2 - 6*t1*t2))))/
          (-1 + s - s2 + t1) + 
         (32 - 20*Power(s,2) - 4*Power(s,3) - 2*Power(s,2)*s1 + 8*s2 - 
            22*Power(s,2)*s2 + 2*Power(s,2)*s1*s2 + 64*Power(s2,2) + 
            2*Power(s,2)*Power(s2,2) - 34*Power(s2,3) + 2*Power(s2,4) + 
            24*t1 - 8*Power(s,2)*t1 - 52*s2*t1 + 64*Power(s2,2)*t1 - 
            4*Power(s2,3)*t1 - 12*Power(t1,2) - 30*s2*Power(t1,2) + 
            2*Power(s2,2)*Power(t1,2) - 
            ((-3 + s2)*(-1 + s + t1)*(-2 + s - s2 + t1)*(s - s2 + t1)*
               (2 + s - s2 + t1))/(-1 + s - s2 + t1) + 
            2*s1*(-1 + s2)*(-8 + Power(s2,2) + 2*t1 + Power(t1,2) - 
               2*s2*(1 + t1)) - 
            4*s*(-2 + Power(s2,3) + s1*(-1 + s2)*(-1 + s2 - t1) + 
               8*t1 + Power(t1,2) - Power(s2,2)*(15 + t1) + 
               s2*(7 + 13*t1)) - 16*t2 + 16*s2*t2 + 
            ((-1 + s2)*Power(s - s2 + t1,2)*(2 + s - s2 + t1)*
               (-1 + s - s1 - s2 + t2))/(-1 + s - s2 + t1) + 
            ((-2 + s - s2 + t1)*Power(s - s2 + t1,2)*
               (2 + 2*s + s1 - s2 - s1*s2 + Power(s2,2) + 3*t1 - 
                 s2*t1 - t2 + s2*t2))/(-1 + s - s2 + t1))*
          (2 + R1(1 - s + s2 - t1)) - 
         ((s - s2 + t1)*(-62 + 4*s1 - 111*s2 + 12*s1*s2 - 
              94*Power(s2,2) - 7*s1*Power(s2,2) - 33*Power(s2,3) - 
              10*s1*Power(s2,3) + 4*Power(s2,4) + s1*Power(s2,4) + 
              63*t1 - 8*s1*t1 + 64*s2*t1 - 6*s1*s2*t1 + 
              46*Power(s2,2)*t1 + 15*s1*Power(s2,2)*t1 + 
              Power(s2,3)*t1 - s1*Power(s2,3)*t1 - 2*Power(s2,4)*t1 + 
              12*Power(t1,2) + 5*s1*Power(t1,2) - 2*s2*Power(t1,2) - 
              4*s1*s2*Power(t1,2) - 20*Power(s2,2)*Power(t1,2) - 
              s1*Power(s2,2)*Power(t1,2) + 6*Power(s2,3)*Power(t1,2) - 
              11*Power(t1,3) - s1*Power(t1,3) + 21*s2*Power(t1,3) + 
              s1*s2*Power(t1,3) - 6*Power(s2,2)*Power(t1,3) - 
              6*Power(t1,4) + 2*s2*Power(t1,4) + 6*t2 - 5*s2*t2 - 
              5*Power(s2,2)*t2 + 3*Power(s2,3)*t2 + Power(s2,4)*t2 - 
              7*t1*t2 + 10*s2*t1*t2 + Power(s2,2)*t1*t2 - 
              4*Power(s2,3)*t1*t2 + Power(t1,2)*t2 - 
              6*s2*Power(t1,2)*t2 + 5*Power(s2,2)*Power(t1,2)*t2 + 
              2*Power(t1,3)*t2 - 2*s2*Power(t1,3)*t2 + 
              Power(s,3)*(30 - 5*t1 + s2*(2 + t1 - t2) + t2) + 
              Power(s,2)*(-83 + 54*t1 - 16*Power(t1,2) + 
                 s1*(-1 + s2)*(-10 + s2 + t1) - 4*t2 + 4*t1*t2 + 
                 Power(s2,2)*(-4*t1 + 3*t2) + 
                 s2*(-57 + 18*t1 + 4*Power(t1,2) + t2 - 4*t1*t2)) - 
              s*(-117 + 77*t1 - 13*Power(t1,2) + 17*Power(t1,3) + 
                 s1*(-1 + s2)*
                  (-14 - 19*s2 + 2*Power(s2,2) + 15*t1 - 2*Power(t1,2)) \
+ t2 + 3*t1*t2 - 5*Power(t1,2)*t2 + Power(s2,3)*(6 - 5*t1 + 3*t2) + 
                 Power(s2,2)*
                  (-60 + 14*t1 + 10*Power(t1,2) + 5*t2 - 8*t1*t2) + 
                 s2*(-137 - 5*Power(t1,3) - 9*t2 + 
                    Power(t1,2)*(-37 + 5*t2) + t1*(58 + 5*t2))) + 
              2*(-8 - 32*s2 - 22*Power(s2,2) + 6*Power(s2,3) + 
                 Power(s,3)*(3 + s2) + 8*t1 + 12*s2*t1 - 
                 24*Power(s2,2)*t1 + 10*Power(t1,2) + 
                 30*s2*Power(t1,2) - 12*Power(t1,3) - 
                 s1*(-1 + s2)*
                  (Power(s2,3) - 2*Power(s2,2)*(1 + t1) + 
                    s2*(2 + Power(t1,2)) + 2*(2 - 3*t1 + Power(t1,2))) \
+ 2*s2*t2 + Power(s2,2)*t2 - 3*Power(s2,3)*t2 - 2*t1*t2 - 4*s2*t1*t2 + 
                 6*Power(s2,2)*t1*t2 + 3*Power(t1,2)*t2 - 
                 3*s2*Power(t1,2)*t2 - 
                 Power(s,2)*(14 + 2*Power(s2,2) + 
                    s1*(-2 + s2 + Power(s2,2)) + 6*t1 - 3*t2 + 
                    s2*(-4 - 2*t1 + 3*t2)) + 
                 s*(20 + Power(s2,3) - 4*t1 - 21*Power(t1,2) + 
                    2*s1*(-1 + s2)*(3 + Power(s2,2) - 2*t1 - s2*t1) - 
                    2*t2 + 6*t1*t2 + Power(s2,2)*(-13 - 2*t1 + 6*t2) + 
                    s2*(32 + 34*t1 + Power(t1,2) - 4*t2 - 6*t1*t2)))*
               R1(1 - s + s2 - t1)))/((-1 + t1)*(-1 + s - s2 + t1)) - 
         ((-1 + s2)*(-96 - 8*s1 - 227*s2 - 22*s1*s2 - 151*Power(s2,2) - 
              15*s1*Power(s2,2) - 15*Power(s2,3) - s1*Power(s2,3) + 
              8*Power(s2,4) - s1*Power(s2,4) + 195*t1 - 2*s1*t1 + 
              281*s2*t1 + 4*s1*s2*t1 + 52*Power(s2,2)*t1 + 
              s1*Power(s2,2)*t1 - 33*Power(s2,3)*t1 + 
              4*s1*Power(s2,3)*t1 - 130*Power(t1,2) + 
              11*s1*Power(t1,2) - 59*s2*Power(t1,2) + 
              s1*s2*Power(t1,2) + 51*Power(s2,2)*Power(t1,2) - 
              6*s1*Power(s2,2)*Power(t1,2) + 22*Power(t1,3) - 
              s1*Power(t1,3) - 35*s2*Power(t1,3) + 4*s1*s2*Power(t1,3) + 
              9*Power(t1,4) - s1*Power(t1,4) + 8*t2 + 17*s2*t2 + 
              6*Power(s2,2)*t2 + 2*Power(s2,3)*t2 + 3*Power(s2,4)*t2 + 
              7*t1*t2 + 11*s2*t1*t2 - 3*Power(s2,2)*t1*t2 - 
              11*Power(s2,3)*t1*t2 - 17*Power(t1,2)*t2 + 
              15*Power(s2,2)*Power(t1,2)*t2 + Power(t1,3)*t2 - 
              9*s2*Power(t1,3)*t2 + 2*Power(t1,4)*t2 + 
              Power(s,4)*(8 - 2*s2 + t1 + t2) + 
              Power(s,3)*(13 + 6*Power(s2,2) + s1*(4 + s2 - t1) + 
                 38*t1 + 3*Power(t1,2) - 4*t2 + 5*t1*t2 - 
                 s2*(34 + 9*t1 + 6*t2)) + 
              Power(s,2)*(-127 - 6*Power(s2,3) + 45*t1 + 
                 61*Power(t1,2) + 3*Power(t1,3) + 
                 s1*(8 - 3*Power(s2,2) + 7*t1 - 3*Power(t1,2) + 
                    s2*(-9 + 6*t1)) - 14*t2 - 7*t1*t2 + 
                 9*Power(t1,2)*t2 + Power(s2,2)*(52 + 15*t1 + 12*t2) - 
                 s2*(61 + 113*t1 + 12*Power(t1,2) - 10*t2 + 21*t1*t2)) + 
              s*(203 + 2*Power(s2,4) - 257*t1 + 54*Power(t1,2) + 
                 40*Power(t1,3) + Power(t1,4) + 
                 s1*(-2 + 3*Power(s2,3) + Power(s2,2)*(6 - 9*t1) + 
                    19*t1 + 2*Power(t1,2) - 3*Power(t1,3) + 
                    s2*(7 - 8*t1 + 9*Power(t1,2))) + 7*t2 - 31*t1*t2 - 
                 2*Power(t1,2)*t2 + 7*Power(t1,3)*t2 - 
                 Power(s2,3)*(34 + 7*t1 + 10*t2) + 
                 Power(s2,2)*
                  (63 + 9*Power(t1,2) - 8*t2 + 27*t1*(4 + t2)) + 
                 s2*(302 - 5*Power(t1,3) + 8*t2 - 
                    6*Power(t1,2)*(19 + 4*t2) + t1*(-117 + 10*t2))) - 
              (48 + 16*s1 + 118*s2 + 12*s1*s2 + 77*Power(s2,2) - 
                 16*s1*Power(s2,2) - 8*Power(s2,3) - 
                 13*s1*Power(s2,3) - 18*Power(s2,4) - 102*t1 - 
                 12*s1*t1 - 136*s2*t1 + 32*s1*s2*t1 + 
                 19*Power(s2,2)*t1 + 39*s1*Power(s2,2)*t1 + 
                 62*Power(s2,3)*t1 + 59*Power(t1,2) - 
                 16*s1*Power(t1,2) - 14*s2*Power(t1,2) - 
                 39*s1*s2*Power(t1,2) - 78*Power(s2,2)*Power(t1,2) + 
                 3*Power(t1,3) + 13*s1*Power(t1,3) + 
                 42*s2*Power(t1,3) - 8*Power(t1,4) + 
                 2*Power(s,4)*(-3 + t2) - 16*t2 - 14*s2*t2 + 
                 Power(s2,2)*t2 - Power(s2,3)*t2 + 2*Power(s2,4)*t2 + 
                 14*t1*t2 - 8*s2*t1*t2 - 6*Power(s2,2)*t1*t2 - 
                 8*Power(s2,3)*t1*t2 + 7*Power(t1,2)*t2 + 
                 15*s2*Power(t1,2)*t2 + 12*Power(s2,2)*Power(t1,2)*t2 - 
                 8*Power(t1,3)*t2 - 8*s2*Power(t1,3)*t2 + 
                 2*Power(t1,4)*t2 + 
                 Power(s,3)*(-6 + 4*s1 - 35*t1 + s2*(36 - 8*t2) + t2 + 
                    8*t1*t2) + 
                 Power(s,2)*(81 - 3*t1 - 60*Power(t1,2) + 
                    s1*(-10 - 21*s2 + 21*t1) + 
                    12*Power(s2,2)*(-6 + t2) + t2 - 6*t1*t2 + 
                    12*Power(t1,2)*t2 + 
                    s2*(4 + 132*t1 - 3*t2 - 24*t1*t2)) + 
                 s*(-118 + 140*t1 + 6*Power(t1,2) - 39*Power(t1,3) + 
                    2*s1*(-6 + 15*Power(s2,2) + s2*(13 - 30*t1) - 
                       13*t1 + 15*Power(t1,2)) + 
                    Power(s2,3)*(60 - 8*t2) + 14*t2 + 8*t1*t2 - 
                    15*Power(t1,2)*t2 + 8*Power(t1,3)*t2 - 
                    2*s2*(79 + t1*(8 - 6*t2) + t2 + 
                       3*Power(t1,2)*(-23 + 4*t2)) + 
                    Power(s2,2)*(10 + 3*t2 + 3*t1*(-53 + 8*t2))))*
               R1(1 - s + s2 - t1)))/((-1 + s - s2 + t1)*(s - s1 + t2)) + 
         ((-1 + s2)*(-s + s2 - t1)*
            (((-1 + s + t1)*(-2 + s - s2 + t1)*(s - s2 + t1)*
                 (-13 + 4*Power(s,2) + 2*Power(s1,2) - 5*s2 + 
                   2*Power(s2,2) + s1*(-3 + 5*s2 - t1) - 2*s2*t1 + 
                   s*(9 - 6*s1 - 6*s2 + t1) - t2))/(-1 + s - s2 + t1) - 
              (Power(s - s2 + t1,2)*(-1 + s - s1 - s2 + t2)*
                 (-5 + Power(s,2) - 5*s2 + 2*Power(s2,2) + 
                   2*s1*(-2 + s2 - t1) + 2*t1 - 2*s2*t1 + 2*t2 + 
                   2*s2*t2 - s*(-7 + 2*s1 + 3*s2 + 2*t2)))/
               (-1 + s - s2 + t1) + 
              ((-2 + s - s2 + t1)*Power(s - s2 + t1,2)*
                 (-3 + 3*Power(s,2) + s2 - 2*Power(s2,2) - 2*t1 + 
                   2*s2*t1 - s1*(-4 + 2*s2 + t1) - 5*t2 + 
                   s*(-3 - 3*s1 + s2 + t1 + 2*t2)))/(-1 + s - s2 + t1) + 
              2*(4 - 3*Power(s,4) + 2*Power(s2,4) + 
                 Power(s,3)*(-14 + 3*s1 + 5*s2 - 6*t1) + 
                 Power(s2,3)*(1 + 4*s1 - 4*t1) - 19*t1 + s1*t1 + 
                 11*Power(t1,2) + 5*s1*Power(t1,2) + 
                 Power(s2,2)*
                  (3 + 2*Power(s1,2) - 7*t1 - 4*s1*t1 + 
                    2*Power(t1,2) - 3*t2) + 
                 Power(s,2)*(22 + Power(s2,2) - 25*t1 - 
                    3*Power(t1,2) + 7*s2*(3 + t1) + 
                    s1*(10 - s2 + 6*t1) - 2*t2) - 6*t2 + 4*s1*t2 + 
                 t1*t2 + 2*s1*t1*t2 - 2*Power(t2,2) - 
                 s2*(-13 + s1 + 12*t1 + 9*s1*t1 + 2*Power(s1,2)*t1 - 
                    6*Power(t1,2) - 7*t2 + 2*s1*t2 - 5*t1*t2 + 
                    2*Power(t2,2)) + 
                 s*(-13 - 2*Power(s1,2)*s2 - 5*Power(s2,3) + 31*t1 - 
                    13*Power(t1,2) + Power(s2,2)*(-8 + 3*t1) - 
                    s1*(7 + 6*Power(s2,2) - 13*t1 - 3*Power(t1,2) + 
                       s2*(10 + t1)) + t2 - 2*t1*t2 + 2*Power(t2,2) + 
                    s2*(-21 + 28*t1 + 2*Power(t1,2) + 5*t2))) + 
              (-4 - 2*Power(s2,3) + 8*t1 - 4*s1*t1 + 
                 2*Power(s1,2)*t1 - 5*Power(t1,2) + 
                 10*s1*Power(t1,2) - Power(t1,3) - s1*Power(t1,3) + 
                 6*t2 - 4*s1*t2 - 5*t1*t2 - 6*Power(t1,2)*t2 + 
                 s1*Power(t1,2)*t2 + 2*Power(t2,2) - t1*Power(t2,2) + 
                 2*Power(s,3)*(2 + t1 + t2) + 
                 Power(s2,2)*
                  (-2 - t1*(-2 + t2) - 8*t2 + Power(t2,2) + 
                    s1*(8 + t1 + t2)) - 
                 Power(s,2)*(4 - 3*Power(t1,2) + 2*s1*(2 + t1) + 
                    4*t2 - 4*t1*t2 - Power(t2,2) + 
                    2*s2*(2 + t1 + 2*t2)) + 
                 s*(2 + 2*Power(s1,2) - 7*t1 - 3*Power(t1,2) + 
                    Power(t1,3) - 5*t2 - 10*t1*t2 + 
                    2*Power(t1,2)*t2 - 3*Power(t2,2) + 
                    t1*Power(t2,2) + 2*Power(s2,2)*(1 + t2) + 
                    s1*(4 + 8*t1 - 3*Power(t1,2) + 
                       s2*(-4 + t1 - t2) + 2*t2 + t1*t2) - 
                    s2*(-2 + Power(t1,2) - 12*t2 + 3*t1*t2 + 
                       2*Power(t2,2))) + 
                 s2*(-2 - 2*Power(s1,2) - 3*t2 + 3*Power(t2,2) + 
                    Power(t1,2)*(1 + t2) + 
                    t1*(5 + 12*t2 - Power(t2,2)) - 
                    2*s1*(-2 + t1*(7 + t2))))/(-1 + s - s2 + t1) + 
              (-(((-1 + s + t1)*(-2 + s - s2 + t1)*(s - s2 + t1)*
                      (-9 + 2*Power(s,2) + Power(s1,2) - 3*s2 + 
                        Power(s2,2) + s1*(-4 + 2*s2 - t1) + t1 - 
                        s2*t1 + s*(7 - 3*s1 - 3*s2 + t1) + t2))/
                    (-1 + s - s2 + t1)) - 
                 ((-2 + s - s2 + t1)*Power(s - s2 + t1,2)*
                    (-1 + Power(s,2) - Power(s2,2) - 2*t1 + 
                      s2*(1 - s1 + t1) - t2 + s*(-1 - s1 + s2 + t2)))/
                  (-1 + s - s2 + t1) + 
                 (Power(s - s2 + t1,2)*(-1 + s - s1 - s2 + t2)*
                    (-5 + Power(s,2) - 3*s2 + Power(s2,2) + 2*t1 - 
                      s2*t1 - s1*(3 + t1) + t2 + s2*t2 - 
                      s*(-5 + s1 + 2*s2 - t1 + t2)))/(-1 + s - s2 + t1) \
+ 2*(20 + Power(s,4) + 3*s2 + 2*Power(s2,2) + Power(s2,3) - 
                    Power(s2,4) - Power(s1,2)*(1 + s2)*(-4 + s2 - t1) + 
                    3*t1 + 7*s2*t1 + 2*Power(s2,3)*t1 - 
                    11*Power(t1,2) - s2*Power(t1,2) - 
                    Power(s2,2)*Power(t1,2) - 
                    Power(s,3)*(s1 + s2 - 2*(5 + t1)) - 
                    Power(s,2)*
                     (5 + 2*Power(s2,2) - 17*t1 - Power(t1,2) + 
                       s2*(13 + t1) + s1*(8 + s2 + 2*t1) - 2*t2) - 
                    2*t2 + 3*s2*t2 + Power(s2,2)*t2 - 3*t1*t2 - 
                    3*s2*t1*t2 + 2*Power(t2,2) + 2*s2*Power(t2,2) + 
                    s*(-25 + 3*Power(s2,3) + Power(s1,2)*(1 + s2) + 
                       Power(s2,2)*(2 - 3*t1) - 20*t1 + 
                       7*Power(t1,2) + 
                       s1*(2 + s2 + 4*Power(s2,2) - 10*t1 - 2*s2*t1 - 
                        Power(t1,2)) + s2*(11 - 13*t1 - 3*t2) + 3*t2 + 
                       4*t1*t2 - 2*Power(t2,2)) - 
                    s1*(2*Power(s2,3) - Power(s2,2)*(5 + 3*t1) + 
                       2*(Power(t1,2) + t1*(-3 + t2) + 4*(-1 + t2)) + 
                       s2*(6 - t1 + Power(t1,2) + 2*t2))))*
               (2 + R1(1 - s + s2 - t1))))/((-1 + s1)*(-s + s1 - t2)) + 
         ((-1 + s2)*(-s + s2 - t1)*
            (-((Power(s - s2 + t1,2)*(-1 + s - s1 - s2 + t2)*
                   (-9 + 3*Power(s,2) + s1*(-1 + s2) + 3*s2 - 6*t1 - 
                     s*(3*s1 + 3*s2 + t1 - t2) + 5*t2 - s2*t2))/
                 (-1 + s - s2 + t1)) - 
              ((-2 + s - s2 + t1)*Power(s - s2 + t1,2)*
                 (3*Power(s,2) - t1 + s2*(1 + s1 - t2) + 4*t2 - 
                   s*(1 + 3*s1 + 3*s2 + t2)))/(-1 + s - s2 + t1) - 
              ((-1 + s + t1)*(-2 + s - s2 + t1)*(s - s2 + t1)*
                 (10 + Power(s1,2) + 6*t1 + s*t1 + 2*s2*(-3 + t2) - 
                   4*t2 - 2*s*t2 + s1*(1 - s + 2*s2 - t1 + t2)))/
               (-1 + s - s2 + t1) - 
              (-2 + 9*s2 - 6*Power(s2,3) - 5*t1 - 8*s2*t1 + 
                 10*Power(s2,2)*t1 + 2*Power(t1,2) - 
                 7*s2*Power(t1,2) + 3*Power(t1,3) + 
                 Power(s1,2)*
                  (-4 + 3*Power(s2,2) + s2*(2 - 4*t1) + 2*t1 + 
                    Power(t1,2)) + 8*t2 - 6*s2*t2 + Power(s2,2)*t2 + 
                 2*Power(s2,3)*t2 + 4*t1*t2 + 3*s2*t1*t2 - 
                 3*Power(s2,2)*t1*t2 - 2*Power(t1,2)*t2 + 
                 s2*Power(t1,2)*t2 - 6*Power(t2,2) - s2*Power(t2,2) + 
                 Power(s2,2)*Power(t2,2) + 3*t1*Power(t2,2) - 
                 s2*t1*Power(t2,2) - 
                 Power(s,3)*(-2 + 2*s1 + t1 + t2) + 
                 s1*(2*Power(s2,3) - Power(t1,3) - 
                    3*Power(s2,2)*(1 + t1) + 
                    s2*(1 + 2*Power(t1,2) - t1*(-7 + t2) - 3*t2) + 
                    t1*(-7 + t2) + Power(t1,2)*t2 + 2*(1 + t2)) + 
                 Power(s,2)*(1 + 2*Power(s1,2) + 4*t1 + 
                    s1*(5*s2 - 2*t1 - t2) - 5*t1*t2 + Power(t2,2) + 
                    s2*(-6 + t1 + 4*t2)) - 
                 s*(-1 + Power(s1,2)*(5*s2 - 3*t1) - 9*t1 - 
                    Power(t1,2) - Power(t1,3) + 
                    5*Power(s2,2)*(-2 + t2) + 4*t2 - 4*t1*t2 + 
                    4*Power(t1,2)*t2 - Power(t2,2) - t1*Power(t2,2) + 
                    s2*(9 + 8*t1 + Power(t1,2) + t2 - 8*t1*t2 + 
                       2*Power(t2,2)) + 
                    s1*(3 + 5*Power(s2,2) + 6*t1 + Power(t1,2) - 
                       5*t2 - s2*(5 + 4*t1 + t2))))/(-1 + s - s2 + t1) + 
              2*(-2 + 3*Power(s,4) + 20*s2 - 10*Power(s2,2) + 
                 7*Power(s2,3) - Power(s,3)*(8 + 3*s1 + 9*s2 - 6*t1) - 
                 Power(s1,2)*(-1 + s2)*(-4 + s2 - t1) - 16*t1 - 
                 16*Power(s2,2)*t1 + 4*Power(t1,2) + 9*s2*Power(t1,2) + 
                 Power(s,2)*(12 + 9*Power(s2,2) + 
                    s1*(4 + 7*s2 - 6*t1) - 17*t1 + 3*Power(t1,2) + 
                    s2*(17 - 11*t1 - 2*t2)) + 8*t2 - 14*s2*t2 + 
                 10*Power(s2,2)*t2 - 2*Power(s2,3)*t2 + 12*t1*t2 - 
                 8*s2*t1*t2 + 2*Power(s2,2)*t1*t2 - 6*Power(t2,2) + 
                 2*s2*Power(t2,2) + 
                 s1*(2*Power(t1,2) + t1*(-7 + t2) + 2*(1 + t2) - 
                    Power(s2,2)*(-2 + t1 + t2) + 
                    s2*(1 + Power(t1,2) - 3*t2 + t1*t2)) + 
                 s*(-10 + Power(s1,2)*(-3 + s2) - 3*Power(s2,3) + 
                    22*t1 - 13*Power(t1,2) + 4*t2 + 6*t1*t2 - 
                    2*Power(t2,2) + Power(s2,2)*(-16 + 5*t1 + 4*t2) - 
                    s2*(10 - 29*t1 + 2*Power(t1,2) + 10*t2 + 2*t1*t2) + 
                    s1*(-3 - 4*Power(s2,2) - 3*Power(t1,2) + 5*t2 + 
                       s2*(-4 + 8*t1 + t2)))) + 
              (((-1 + s + t1)*(-2 + s - s2 + t1)*(s - s2 + t1)*
                    (7 + s1 - 2*s2 + 3*t1 - t2))/(-1 + s - s2 + t1) + 
                 ((-2 + s - s2 + t1)*Power(s - s2 + t1,2)*
                    (-2 + 2*Power(s,2) + s1 + s2 - 2*s*(s1 + s2) - 
                      t1 + t2))/(-1 + s - s2 + t1) + 
                 (Power(s - s2 + t1,2)*(-1 + s - s1 - s2 + t2)*
                    (-7 + 2*Power(s,2) + s2 - 2*s*(s1 + s2) - 4*t1 + 
                      2*t2))/(-1 + s - s2 + t1) - 
                 2*(-20 + 2*Power(s,4) + 13*s2 - 11*Power(s2,2) + 
                    3*Power(s2,3) - 2*Power(s,3)*(s1 + 3*s2 - 2*t1) - 
                    2*Power(s1,2)*(2 + s2 - t1) - 29*t1 + 4*s2*t1 - 
                    7*Power(s2,2)*t1 + 3*Power(t1,2) + 
                    4*s2*Power(t1,2) + 
                    Power(s,2)*
                     (-4 + s1 + s2 + 4*s1*s2 + 6*Power(s2,2) - 
                       4*s1*t1 - 8*s2*t1 + 2*Power(t1,2) - 6*t2) + 
                    24*t2 + s2*t2 + Power(s2,2)*t2 + 7*t1*t2 - 
                    s2*t1*t2 - 4*Power(t2,2) + 
                    s1*(-8 - 2*Power(s2,2) + Power(t1,2) + 
                       s2*(3 + t1 - 2*t2) + t1*(-11 + 2*t2)) + 
                    s*(11 - 2*Power(s1,2) - 2*Power(s2,3) + 
                       4*Power(s2,2)*(-1 + t1) + 11*t1 - 
                       4*Power(t1,2) - t2 - 2*t1*t2 + 
                       s2*(11 + 5*t1 - 2*Power(t1,2) + 5*t2) + 
                       s1*(1 + s2 - 2*Power(s2,2) - 6*t1 + 4*s2*t1 - 
                         2*Power(t1,2) + 6*t2))))*
               (2 + R1(1 - s + s2 - t1))))/((-1 + s1)*(-1 + t2)) + 
         ((-1 + s2)*(-s + s2 - t1)*
            ((Power(s - s2 + t1,2)*(-1 + s - s1 - s2 + t2)*
                 (4 - Power(s2,2) + 7*t1 - s2*t1 - Power(t1,2) - 
                   3*s1*(-2 + s2 + t1) + s*(2 + s2 - t2) - 5*t2 + s2*t2)\
)/(-1 + s - s2 + t1) + ((-2 + s - s2 + t1)*Power(s - s2 + t1,2)*
                 (-2 + s2 - Power(s2,2) + 6*t1 - 2*s2*t1 - 
                   s1*(1 + t1) - t2 - 2*s2*t2 - t1*t2 + 
                   s*(2 + s2 + t1 + 2*t2)))/(-1 + s - s2 + t1) - 
              ((-1 + s + t1)*(-2 + s - s2 + t1)*(s - s2 + t1)*
                 (4 + 4*s2 + 2*t1 - 3*s2*t1 + s*(-4 + t1 - t2) - 4*t2 + 
                   s2*t2 - s1*(-9 + 2*s2 + t1 + 2*t2)))/
               (-1 + s - s2 + t1) - 
              (-2 + 8*s2 + 14*Power(s2,2) + 
                 2*Power(s1,2)*(Power(s2,2) - Power(-2 + t1,2)) - 
                 2*t1 - 22*s2*t1 - 10*Power(s2,2)*t1 + 4*Power(t1,2) + 
                 12*s2*Power(t1,2) - 2*Power(t1,3) + 8*t2 - 3*s2*t2 - 
                 6*Power(s2,2)*t2 + 3*t1*t2 + 7*s2*t1*t2 - 
                 5*Power(t1,2)*t2 - 2*s2*Power(t1,2)*t2 + 
                 2*Power(t1,3)*t2 - 6*Power(t2,2) - s2*Power(t2,2) + 
                 Power(s2,2)*Power(t2,2) + 3*t1*Power(t2,2) - 
                 s2*t1*Power(t2,2) + Power(s,3)*(-2 + t1 + t2) + 
                 s1*(-8 - 2*Power(t1,3) + s2*(2 + 4*t1*(-1 + t2)) + 
                    Power(s2,2)*(2 + 2*t1 - 5*t2) + 16*t2 + 
                    Power(t1,2)*(10 + t2) - 6*t1*(1 + 2*t2)) + 
                 Power(s,2)*(5 - 9*t1 + 3*Power(t1,2) + 
                    s1*(2 + s2 + t1 - 2*t2) - t2 + 2*t1*t2 + 
                    Power(t2,2) - s2*(-4 + t1 + 2*t2)) - 
                 s*(-2 - 13*t1 + 11*Power(t1,2) - 2*Power(t1,3) + 
                    2*Power(s1,2)*(-2 + s2 + t1) - 
                    Power(s2,2)*(-2 + t2) + 7*t2 + 2*t1*t2 - 
                    3*Power(t1,2)*t2 - Power(t2,2) - t1*Power(t2,2) + 
                    s1*(-4 + Power(s2,2) + Power(t1,2) + 
                       s2*(6 + 2*t1 - 7*t2) + t1*(-6 + t2) + 6*t2) + 
                    s2*(19 + 2*Power(t1,2) - 7*t2 + 2*Power(t2,2) + 
                       t1*(-17 + 2*t2))))/(-1 + s - s2 + t1) - 
              2*(2 - 13*s2 - 6*Power(s2,2) - Power(s2,4) + 7*t1 + 
                 7*s2*t1 + 13*Power(s2,2)*t1 + 3*Power(t1,2) - 
                 11*s2*Power(t1,2) - 2*Power(t1,3) + s2*Power(t1,3) - 
                 4*Power(s1,2)*(-2 + s2 + t1) - 8*t2 + 11*s2*t2 - 
                 10*Power(s2,2)*t2 - Power(s2,3)*t2 - 11*t1*t2 + 
                 20*s2*t1*t2 + 3*Power(s2,2)*t1*t2 - 6*Power(t1,2)*t2 - 
                 2*s2*Power(t1,2)*t2 + 6*Power(t2,2) - 
                 2*s2*Power(t2,2) + Power(s,3)*(2 + s2 + t1 + 2*t2) - 
                 Power(s,2)*(6 + s1 + 4*s2 + 3*Power(s2,2) - 4*t1 + 
                    s1*t1 - 2*Power(t1,2) + 6*t2 + 5*s2*t2 - 4*t1*t2) - 
                 s1*(-8 + 2*Power(s2,3) + 5*Power(t1,2) + Power(t1,3) + 
                    s2*(15 - 3*Power(t1,2) - 2*t1*(-8 + t2) - 10*t2) + 
                    16*t2 + Power(s2,2)*(-13 + 2*t2) - t1*(19 + 2*t2)) + 
                 s*(3 + 3*Power(s2,3) - 7*t1 + 2*Power(t1,2) + 
                    Power(t1,3) - t2 - 16*t1*t2 + 2*Power(t1,2)*t2 + 
                    2*Power(t2,2) + Power(s2,2)*(2 - t1 + 4*t2) + 
                    s2*(12 - 15*t1 + 16*t2 - 7*t1*t2) + 
                    s1*(9 + 2*Power(s2,2) - 2*Power(t1,2) - 4*t2 + 
                       s2*(-10 + 3*t1 + 2*t2)))) + 
              (-((Power(s - s2 + t1,2)*
                      (3 - Power(s2,2) + s*(2 + s2) + 4*t1 - 
                        s1*(-2 + s2 + t1) - 2*t2)*
                      (-1 + s - s1 - s2 + t2))/(-1 + s - s2 + t1)) - 
                 ((-1 + s + t1)*(-2 + s - s2 + t1)*(s - s2 + t1)*
                    (-1 + s - s2 - 2*t1 + s2*t1 + t2 + 
                      s1*(-4 + s2 + t2)))/(-1 + s - s2 + t1) - 
                 ((-2 + s - s2 + t1)*Power(s - s2 + t1,2)*
                    (2 + s2 - Power(s2,2) + 3*t1 - s2*t1 - 
                      s1*(2 + t1) - s2*t2 + s*(1 + s2 + t1 + t2)))/
                  (-1 + s - s2 + t1) + 
                 2*(4 - s2 + 4*Power(s2,2) + 2*Power(s2,3) - 
                    Power(s2,4) + 11*t1 - 8*s2*t1 + 5*Power(s2,2)*t1 + 
                    Power(s2,3)*t1 + 14*Power(t1,2) - 6*s2*Power(t1,2) - 
                    Power(t1,3) - 2*Power(s1,2)*(-2 + s2 + t1) - 8*t2 + 
                    3*s2*t2 + 2*Power(s2,2)*t2 - Power(s2,3)*t2 - 
                    9*t1*t2 + 3*s2*t1*t2 + 2*Power(s2,2)*t1*t2 - 
                    3*Power(t1,2)*t2 - s2*Power(t1,2)*t2 + 
                    4*Power(t2,2) + Power(s,3)*(1 + s2 + t1 + t2) + 
                    Power(s,2)*
                     (-1 - 3*Power(s2,2) + 5*t1 + 2*Power(t1,2) - 
                       s1*(2 + t1) + t2 - 3*s2*t2 + 2*t1*t2) - 
                    s1*(Power(s2,3) + 6*Power(t1,2) + Power(t1,3) + 
                       Power(s2,2)*(-5 + t2) - t1*(12 + t2) + 
                       2*(-7 + 5*t2) - 
                       s2*(-2 + 2*Power(t1,2) + t1*(-3 + t2) + 5*t2)) + 
                    s*(3 + 3*Power(s2,3) + 5*t1 + 3*Power(t1,2) + 
                       Power(t1,3) - 5*t2 - 2*t1*t2 + Power(t1,2)*t2 + 
                       Power(s2,2)*(-3 - 2*t1 + 3*t2) - 
                       s2*(3 + 8*t1 + Power(t1,2) + 3*t2 + 4*t1*t2) + 
                       s1*(2 + Power(s2,2) - 6*t1 - 2*Power(t1,2) - t2 + 
                         s2*(-1 + 2*t1 + t2)))))*(2 + R1(1 - s + s2 - t1))\
))/((-1 + t1)*(-1 + t2))))/(Power(-1 + s2,2)*Power(s - s2 + t1,3)) + 
    (8*(((-1 + t1)*(-2 + 2*s1 + 2*s2 - 2*s1*Power(s2,2) - 31*t1 - 
              23*s2*t1 + s1*s2*t1 - 22*Power(s2,2)*t1 - 
              s1*Power(s2,2)*t1 - 50*Power(t1,2) + 9*s1*Power(t1,2) - 
              38*s2*Power(t1,2) - 10*s1*s2*Power(t1,2) + 
              12*Power(s2,2)*Power(t1,2) + s1*Power(s2,2)*Power(t1,2) + 
              33*Power(t1,3) - s1*Power(t1,3) - 3*s2*Power(t1,3) + 
              s1*s2*Power(t1,3) - 2*Power(s2,2)*Power(t1,3) - 
              6*Power(t1,4) + 2*s2*Power(t1,4) - 2*t2 + 2*s2*t2 + 
              t1*t2 + 6*s2*t1*t2 - 7*Power(s2,2)*t1*t2 - 
              7*Power(t1,2)*t2 + 6*s2*Power(t1,2)*t2 + 
              Power(s2,2)*Power(t1,2)*t2 + 2*Power(t1,3)*t2 - 
              2*s2*Power(t1,3)*t2 + 
              s*(4 - 5*Power(t1,3) + Power(t1,2)*(59 + t2) - 
                 t1*(4 + 7*t2) + 
                 s2*(4 + Power(t1,3) - Power(t1,2)*(7 + t2) + 
                    t1*(-20 + 7*t2))) + 
              2*(-1 + s2 + 4*t1 - 16*s2*t1 - 23*Power(t1,2) - 
                 9*s2*Power(t1,2) + 12*Power(t1,3) + 
                 2*s*(1 + s2 - t1 - 5*s2*t1 + 6*Power(t1,2) + 
                    2*s2*Power(t1,2)) - 
                 s1*(-1 + s2)*
                  (1 - 2*t1 + 5*Power(t1,2) + 
                    s2*(1 - 6*t1 + Power(t1,2))) - t2 + s2*t2 + 
                 4*t1*t2 - 4*s2*t1*t2 - 3*Power(t1,2)*t2 + 
                 3*s2*Power(t1,2)*t2)*R1(t1)))/(s - s2 + t1) + 
         ((-1 + s2)*(-1 + 21*t1 + s*t1 + 18*s2*t1 + 12*Power(t1,2) - 
              10*s2*Power(t1,2) + 67*Power(t1,3) - 2*s*Power(t1,3) - 
              11*Power(t1,4) + s*Power(t1,4) - 
              s1*(-1 + t1)*(t1*(-3 - 3*t1 + Power(t1,2)) + 
                 s2*(-3 - t1 + Power(t1,2))) + t2 - 3*s*t2 + 3*s2*t2 + 
              16*t1*t2 + 10*s*t1*t2 + 20*s2*t1*t2 - 2*Power(t1,2)*t2 - 
              8*s*Power(t1,2)*t2 - 9*Power(t1,3)*t2 + s*Power(t1,3)*t2 + 
              s2*Power(t1,3)*t2 + 2*Power(t1,4)*t2 + 
              (-1 + (29 + 7*s - 10*s2)*Power(t1,3) + 
                 s1*(-1 + t1)*
                  (s2*(3 - 9*t1) + t1*(7 - 11*t1 + 2*Power(t1,2))) + 
                 t2 - 3*s*t2 + 3*s2*t2 - 2*Power(t1,4)*(3 + s + t2) + 
                 t1*(-5 + 5*s + 10*s2 + 12*t2 + 12*s*t2 - 12*s2*t2) + 
                 Power(t1,2)*
                  (15 - 10*s + 16*s2 + 5*t2 - 9*s*t2 + 9*s2*t2))*R1(t1)))/
          (-1 + t2) + 2*(2 - 11*t1 + 6*s*t1 + 7*s1*t1 + 74*Power(t1,2) - 
            8*s*Power(t1,2) - 9*s1*Power(t1,2) - 29*Power(t1,3) + 
            2*s*Power(t1,3) + 2*s1*Power(t1,3) + 4*Power(t1,4) - t2 - 
            15*t1*t2 + 10*Power(t1,2)*t2 - 2*Power(t1,3)*t2 + 
            s2*(4 - 2*Power(t1,4) + Power(t1,2)*(14 + 9*s1 - 10*t2) + 
               t2 + Power(t1,3)*(9 - 2*s1 + 2*t2) + 
               t1*(-1 - 7*s1 + 15*t2)) + 
            (2 - 3*Power(t1,4) + Power(t1,2)*(1 + 8*s + 4*s1 - 3*t2) - 
               t2 + Power(t1,3)*(2 - 2*s - s1 + t2) + 
               t1*(6 - 6*s - 3*s1 + 11*t2) + 
               s2*(4 + Power(t1,4) + t1*(-26 + 3*s1 - 11*t2) + 
                  Power(t1,3)*(-14 + s1 - t2) + t2 + 
                  Power(t1,2)*(59 - 4*s1 + 3*t2)))*R1(t1)) - 
         ((-1 + s2)*(-1 + t1)*
            (-4 - s1 - 5*s2 - 2*s1*s2 + Power(s1,2)*s2 - 2*Power(s2,2) + 
              s1*Power(s2,2) + 9*t1 - 4*s1*t1 + Power(s1,2)*t1 + 
              4*s2*t1 + 6*s1*s2*t1 - 3*Power(s1,2)*s2*t1 + 
              6*Power(s2,2)*t1 - 3*s1*Power(s2,2)*t1 - 96*Power(t1,2) + 
              18*s1*Power(t1,2) + Power(s1,2)*Power(t1,2) - 
              27*s2*Power(t1,2) - 18*s1*s2*Power(t1,2) + 
              23*Power(t1,3) - 3*s1*Power(t1,3) - 4*s2*Power(t1,3) + 
              2*s1*s2*Power(t1,3) + 
              Power(s,2)*(Power(t1,3) + t1*(7 - 4*t2) + 
                 Power(t1,2)*(-4 + t2) - t2) + 3*t2 + 3*s1*t2 + s2*t2 + 
              Power(s2,2)*t2 - 39*t1*t2 - 24*s1*t1*t2 + 24*s2*t1*t2 - 
              14*s1*s2*t1*t2 - 3*Power(s2,2)*t1*t2 + 18*Power(t1,2)*t2 + 
              5*s1*Power(t1,2)*t2 + 3*s2*Power(t1,2)*t2 + 
              2*s1*s2*Power(t1,2)*t2 - 4*Power(t1,3)*t2 + Power(t2,2) + 
              s2*Power(t2,2) - 3*t1*Power(t2,2) - 3*s2*t1*Power(t2,2) + 
              s*(3 + Power(t1,2)*(64 + 3*s1 - 21*t2) - 
                 Power(t1,3)*(4 + s1 - 2*t2) - 3*t2 + s1*t2 - 
                 Power(t2,2) + 
                 t1*(-13 + (38 - 3*s1)*t2 + 3*Power(t2,2)) + 
                 s2*(4 + s1*(-1 - 4*t1 + Power(t1,2)) - 
                    Power(t1,2)*(5 + t2) + t1*(-5 + 7*t2))) + 
              (-4 - 5*s2 - 2*Power(s2,2) - 5*t1 + 21*s2*t1 + 
                 12*Power(s2,2)*t1 - 41*Power(t1,2) - 
                 35*s2*Power(t1,2) - 2*Power(s2,2)*Power(t1,2) + 
                 9*Power(t1,3) + 3*s2*Power(t1,3) + Power(t1,4) + 
                 Power(s1,2)*
                  (s2*(1 - 6*t1 + Power(t1,2)) + 
                    t1*(5 - 2*t1 + Power(t1,2))) + 3*t2 + s2*t2 + 
                 Power(s2,2)*t2 - 4*t1*t2 - 3*s2*t1*t2 - 
                 6*Power(s2,2)*t1*t2 + 3*Power(t1,2)*t2 + 
                 11*s2*Power(t1,2)*t2 + Power(s2,2)*Power(t1,2)*t2 + 
                 2*Power(t1,3)*t2 - s2*Power(t1,3)*t2 + Power(t2,2) + 
                 s2*Power(t2,2) - 6*t1*Power(t2,2) - 
                 6*s2*t1*Power(t2,2) + Power(t1,2)*Power(t2,2) + 
                 s2*Power(t1,2)*Power(t2,2) - 
                 Power(s,2)*(Power(t1,3) + t1*(5 - 6*t2) + 
                    Power(t1,2)*(-2 + t2) + t2) + 
                 s1*(-1 + Power(t1,4) + 
                    s2*(-2 + 8*t1 - 6*Power(t1,2)) + 
                    Power(s2,2)*(1 - 6*t1 + Power(t1,2)) + 
                    t1*(3 - 19*t2) + 3*t2 - Power(t1,3)*(1 + t2) + 
                    Power(t1,2)*(-6 + 9*t2)) - 
                 s*(-3 + 5*Power(t1,3) + Power(t1,4) + 
                    s2*(-4 + 19*t1 - 2*Power(t1,2) - Power(t1,3) + 
                       s1*(1 - 6*t1 + Power(t1,2))) + 3*t2 - s1*t2 + 
                    Power(t2,2) + 
                    t1*(-11 + 4*s1 - 10*t2 + 6*s1*t2 - 6*Power(t2,2)) + 
                    Power(t1,2)*
                     (-44 + 4*s1 + 11*t2 - s1*t2 + Power(t2,2))))*R1(t1))\
)/((-1 + s1)*(-1 + t2)) - ((-1 + s2)*(-1 + t1)*
            (2 - 2*s1 - 2*s1*s2 + 2*Power(s2,2) - 67*t1 + 14*s1*t1 + 
              8*Power(s1,2)*t1 - 23*s2*t1 + 27*s1*s2*t1 - 
              14*Power(s1,2)*s2*t1 - 12*Power(s2,2)*t1 - 
              21*s1*Power(s2,2)*t1 + 57*Power(t1,2) + 
              23*s1*Power(t1,2) - 18*Power(s1,2)*Power(t1,2) - 
              16*s2*Power(t1,2) - 7*s1*s2*Power(t1,2) + 
              2*Power(s1,2)*s2*Power(t1,2) + 6*Power(s2,2)*Power(t1,2) + 
              3*s1*Power(s2,2)*Power(t1,2) + 9*Power(t1,3) - 
              4*s1*Power(t1,3) + 2*Power(s1,2)*Power(t1,3) - 
              5*s2*Power(t1,3) - Power(t1,4) + s1*Power(t1,4) + 10*t2 - 
              2*s1*t2 + 4*s2*t2 - 100*t1*t2 - 2*s1*t1*t2 - 40*s2*t1*t2 + 
              7*s1*s2*t1*t2 + 7*Power(s2,2)*t1*t2 + 16*Power(t1,2)*t2 + 
              7*s1*Power(t1,2)*t2 + 11*s2*Power(t1,2)*t2 - 
              s1*s2*Power(t1,2)*t2 - Power(s2,2)*Power(t1,2)*t2 - 
              2*Power(t1,3)*t2 - s1*Power(t1,3)*t2 - s2*Power(t1,3)*t2 + 
              2*Power(t2,2) + t1*Power(t2,2) - 7*s2*t1*Power(t2,2) - 
              Power(t1,2)*Power(t2,2) + s2*Power(t1,2)*Power(t2,2) + 
              Power(s,2)*(3*Power(t1,3) + s2*(-2 + 6*t1) - 2*(1 + t2) - 
                 Power(t1,2)*(39 + t2) + t1*(8 + 13*t2)) + 
              s*(Power(s2,2)*(2 - 6*t1) + 33*t1 + 25*Power(t1,2) + 
                 7*Power(t1,3) - Power(t1,4) - 2*t2 + 28*t1*t2 - 
                 30*Power(t1,2)*t2 + 4*Power(t1,3)*t2 + 
                 7*t1*Power(t2,2) - Power(t1,2)*Power(t2,2) + 
                 s2*(-Power(t1,3) + 2*(1 + t2) + 
                    Power(t1,2)*(21 + 2*t2) - 4*t1*(2 + 5*t2)) + 
                 s1*(-5*Power(t1,3) + s2*(2 + t1 - Power(t1,2)) + 
                    2*(1 + t2) + Power(t1,2)*(57 + 2*t2) - 
                    4*t1*(2 + 5*t2))) - 
              2*(-1 - Power(s2,2) + 16*t1 + 4*s2*t1 + 
                 6*Power(s2,2)*t1 - 14*Power(t1,2) + 4*s2*Power(t1,2) - 
                 Power(s2,2)*Power(t1,2) - Power(t1,4) + 
                 4*Power(s1,2)*t1*(1 + t1) - 5*t2 - 2*s2*t2 + 
                 34*t1*t2 + 12*s2*t1*t2 - 15*Power(t1,2)*t2 - 
                 2*s2*Power(t1,2)*t2 + 2*Power(t1,3)*t2 - Power(t2,2) + 
                 6*t1*Power(t2,2) - Power(t1,2)*Power(t2,2) + 
                 s1*(s2*(1 - 4*t1 + 3*Power(t1,2)) - 
                    (-1 + 8*t1 + Power(t1,2))*(1 + t2)) + 
                 Power(s,2)*(1 - Power(t1,3) + 
                    s2*(1 - 6*t1 + Power(t1,2)) + t2 + 
                    Power(t1,2)*(13 + t2) - t1*(1 + 6*t2)) - 
                 s*(7*t1 + 12*Power(t1,2) - 3*Power(t1,3) + 
                    Power(s2,2)*(1 - 6*t1 + Power(t1,2)) - t2 + 
                    4*t1*t2 - 3*Power(t1,2)*t2 + 
                    s1*(1 + t1 - Power(t1,3) + 
                       s2*(1 - 6*t1 + Power(t1,2)) + t2 - 6*t1*t2 + 
                       Power(t1,2)*(15 + t2)) + 
                    s2*(1 - Power(t1,3) + t2 + Power(t1,2)*(11 + t2) - 
                       3*t1*(1 + 2*t2))))*R1(t1)))/
          ((-1 + s1)*(-s + s1 - t2)) - 
         ((-1 + s2)*(-1 + t1)*
            (5 - 3*s1 + 5*s2 - 5*s1*s2 + Power(s1,2)*s2 - 
              s1*Power(s2,2) - 71*t1 + 3*s1*t1 + Power(s1,2)*t1 - 
              65*s2*t1 + 37*s1*s2*t1 - 3*Power(s1,2)*s2*t1 - 
              14*Power(s2,2)*t1 - 4*s1*Power(s2,2)*t1 + 108*Power(t1,2) - 
              43*s1*Power(t1,2) + Power(s1,2)*Power(t1,2) + 
              17*s2*Power(t1,2) - 38*s1*s2*Power(t1,2) - 
              10*Power(s2,2)*Power(t1,2) + s1*Power(s2,2)*Power(t1,2) - 
              43*Power(t1,3) + 16*s1*Power(t1,3) + 3*s2*Power(t1,3) + 
              2*s1*s2*Power(t1,3) + Power(t1,4) - s1*Power(t1,4) + 
              2*Power(s,2)*(-1 - 16*Power(t1,2) + Power(t1,3)) + 5*t2 - 
              s1*t2 + 6*s2*t2 - 3*s1*s2*t2 - Power(s2,2)*t2 - 39*t1*t2 + 
              3*s1*t1*t2 - 52*s2*t1*t2 + 2*s1*s2*t1*t2 - 
              4*Power(s2,2)*t1*t2 + 28*Power(t1,2)*t2 + 
              3*s1*Power(t1,2)*t2 + 39*s2*Power(t1,2)*t2 + 
              s1*s2*Power(t1,2)*t2 + Power(s2,2)*Power(t1,2)*t2 - 
              8*Power(t1,3)*t2 - s1*Power(t1,3)*t2 - 
              5*s2*Power(t1,3)*t2 + 2*Power(t1,4)*t2 + 2*Power(t2,2) + 
              t1*Power(t2,2) - 7*s2*t1*Power(t2,2) - 
              Power(t1,2)*Power(t2,2) + s2*Power(t1,2)*Power(t2,2) + 
              s*(41*t1 - 14*Power(t1,3) + Power(t1,4) - t2 + 8*t1*t2 - 
                 33*Power(t1,2)*t2 + 4*Power(t1,3)*t2 + 
                 7*t1*Power(t2,2) - Power(t1,2)*Power(t2,2) + 
                 s1*(2 + 31*Power(t1,2) - 2*Power(t1,3) + t2 - 
                    t1*(1 + 3*t2)) + 
                 s2*(6 - 3*Power(t1,3) - Power(t1,2)*(-52 + t2) + t2 + 
                    t1*(-35 + 4*t2))) + 
              (5 + 5*s2 - 30*t1 - 33*s2*t1 - 12*Power(s2,2)*t1 + 
                 38*Power(t1,2) + 7*s2*Power(t1,2) - 
                 12*Power(s2,2)*Power(t1,2) - 10*Power(t1,3) + 
                 13*s2*Power(t1,3) - 3*Power(t1,4) + 
                 2*Power(s,2)*
                  (-1 + 3*t1 - 11*Power(t1,2) + Power(t1,3)) + 
                 Power(s1,2)*
                  (s2*(1 - 6*t1 + Power(t1,2)) + 
                    t1*(5 - 2*t1 + Power(t1,2))) + 5*t2 + 6*s2*t2 - 
                 Power(s2,2)*t2 - 37*t1*t2 - 33*s2*t1*t2 + 
                 6*Power(s2,2)*t1*t2 + 43*Power(t1,2)*t2 + 
                 16*s2*Power(t1,2)*t2 - Power(s2,2)*Power(t1,2)*t2 - 
                 11*Power(t1,3)*t2 - s2*Power(t1,3)*t2 + 2*Power(t2,2) - 
                 12*t1*Power(t2,2) + 2*Power(t1,2)*Power(t2,2) + 
                 s*(3*t1 + 27*Power(t1,2) - 23*Power(t1,3) + 
                    Power(t1,4) - t2 + 3*t1*t2 - 11*Power(t1,2)*t2 + 
                    Power(t1,3)*t2 + 
                    s1*(2 - 3*Power(t1,3) + t2 + 
                       Power(t1,2)*(24 + t2) - t1*(11 + 6*t2)) + 
                    s2*(6 - 3*Power(t1,3) + t2 + 
                       Power(t1,2)*(44 + t2) - t1*(19 + 6*t2))) - 
                 s1*(3 - 18*Power(t1,3) + Power(t1,4) + 
                    Power(s2,2)*(1 - 6*t1 + Power(t1,2)) + t2 - 
                    2*t1*(5 + t2) + Power(t1,2)*(32 + 5*t2) + 
                    s2*(5 - 4*Power(t1,3) + 3*t2 + 
                       Power(t1,2)*(41 + 3*t2) - 2*t1*(11 + 9*t2))))*
               R1(t1)))/((s - s2 + t1)*(s - s1 + t2))))/
     (Power(-1 + s2,2)*Power(-1 + t1,3)*t1) - 
    (8*((-2*(3*Power(s1,4) - 8*s2 + 8*t1 + 
              3*Power(s1,3)*(-4 + t1 - 3*t2) - 15*t2 + 6*s2*t2 - 
              3*t1*t2 + 9*Power(t2,2) - 3*t1*Power(t2,2) + 
              7*Power(t2,3) + 
              Power(s,2)*(10 + 3*Power(s1,2) - 3*t1 + 
                 3*s1*(-3 + t1 - t2) + 7*t2) + 
              Power(s1,2)*(-6 + 6*s2 + 31*t2 + 9*Power(t2,2) - 
                 6*t1*(1 + t2)) + 
              s1*(15 + s2*(2 - 6*t2) - 3*t2 - 26*Power(t2,2) - 
                 3*Power(t2,3) + t1*(-5 + 9*t2 + 3*Power(t2,2))) - 
              s*(7 + 6*Power(s1,3) - 6*s2 + 3*t1 + 
                 3*Power(s1,2)*(-7 + 2*t1 - 4*t2) - 19*t2 + 6*t1*t2 - 
                 14*Power(t2,2) + 
                 s1*(12 + 6*s2 - 9*t1 + 35*t2 - 6*t1*t2 + 6*Power(t2,2)))\
))/(-1 + s - s1 + t2) - ((s - s1 + t2)*
            (-62 - 111*s1 - 94*Power(s1,2) - 33*Power(s1,3) + 
              4*Power(s1,4) + 4*s2 + 12*s1*s2 - 7*Power(s1,2)*s2 - 
              10*Power(s1,3)*s2 + Power(s1,4)*s2 + 6*t1 - 5*s1*t1 - 
              5*Power(s1,2)*t1 + 3*Power(s1,3)*t1 + Power(s1,4)*t1 + 
              63*t2 + 64*s1*t2 + 46*Power(s1,2)*t2 + Power(s1,3)*t2 - 
              2*Power(s1,4)*t2 - 8*s2*t2 - 6*s1*s2*t2 + 
              15*Power(s1,2)*s2*t2 - Power(s1,3)*s2*t2 - 7*t1*t2 + 
              10*s1*t1*t2 + Power(s1,2)*t1*t2 - 4*Power(s1,3)*t1*t2 + 
              12*Power(t2,2) - 2*s1*Power(t2,2) - 
              20*Power(s1,2)*Power(t2,2) + 6*Power(s1,3)*Power(t2,2) + 
              5*s2*Power(t2,2) - 4*s1*s2*Power(t2,2) - 
              Power(s1,2)*s2*Power(t2,2) + t1*Power(t2,2) - 
              6*s1*t1*Power(t2,2) + 5*Power(s1,2)*t1*Power(t2,2) - 
              11*Power(t2,3) + 21*s1*Power(t2,3) - 
              6*Power(s1,2)*Power(t2,3) - s2*Power(t2,3) + 
              s1*s2*Power(t2,3) + 2*t1*Power(t2,3) - 
              2*s1*t1*Power(t2,3) - 6*Power(t2,4) + 2*s1*Power(t2,4) + 
              Power(s,3)*(30 + t1 - 5*t2 + s1*(2 - t1 + t2)) + 
              Power(s,2)*(-83 - 4*t1 + Power(s1,2)*(s2 + 3*t1 - 4*t2) - 
                 s2*(-10 + t2) + 54*t2 + 4*t1*t2 - 16*Power(t2,2) + 
                 s1*(-57 + t1 + s2*(-11 + t2) + 18*t2 - 4*t1*t2 + 
                    4*Power(t2,2))) - 
              s*(-117 + t1 + Power(s1,3)*(6 + 2*s2 + 3*t1 - 5*t2) + 
                 77*t2 + 3*t1*t2 - 13*Power(t2,2) - 5*t1*Power(t2,2) + 
                 17*Power(t2,3) + s2*(14 - 15*t2 + 2*Power(t2,2)) + 
                 Power(s1,2)*
                  (-60 - 21*s2 + 5*t1 + 14*t2 - 8*t1*t2 + 
                    10*Power(t2,2)) + 
                 s1*(-137 + 58*t2 - 37*Power(t2,2) - 5*Power(t2,3) + 
                    s2*(5 + 15*t2 - 2*Power(t2,2)) + 
                    t1*(-9 + 5*t2 + 5*Power(t2,2)))) + 
              2*(-8 + Power(s,3)*(3 + s1) + 4*s2 - Power(s1,4)*s2 + 
                 8*t2 - 6*s2*t2 - 2*t1*t2 + 10*Power(t2,2) + 
                 2*s2*Power(t2,2) + 3*t1*Power(t2,2) - 12*Power(t2,3) - 
                 Power(s,2)*(14 - 2*s2 + Power(s1,2)*(2 + s2) - 3*t1 + 
                    s1*(-4 + s2 + 3*t1 - 2*t2) + 6*t2) + 
                 Power(s1,3)*(6 - 3*t1 + s2*(3 + 2*t2)) - 
                 Power(s1,2)*
                  (22 + 24*t2 - t1*(1 + 6*t2) + 
                    s2*(4 + 2*t2 + Power(t2,2))) - 
                 s1*(32 - 12*t2 - 30*Power(t2,2) + 
                    s2*(2 - 6*t2 + Power(t2,2)) + 
                    t1*(-2 + 4*t2 + 3*Power(t2,2))) + 
                 s*(20 + Power(s1,3)*(1 + 2*s2) - 2*t1 - 4*t2 + 
                    6*t1*t2 - 21*Power(t2,2) + s2*(-6 + 4*t2) + 
                    s1*(32 - 4*t1 - 2*s2*(-3 + t2) + 34*t2 - 6*t1*t2 + 
                       Power(t2,2)) - 
                    Power(s1,2)*(13 - 6*t1 + 2*t2 + 2*s2*(1 + t2))))*
               R1(1 - s + s1 - t2)))/((-1 + t2)*(-1 + s - s1 + t2)) - 
         ((-1 + s1)*(-96 - 227*s1 - 151*Power(s1,2) - 15*Power(s1,3) + 
              8*Power(s1,4) - 8*s2 - 22*s1*s2 - 15*Power(s1,2)*s2 - 
              Power(s1,3)*s2 - Power(s1,4)*s2 + 8*t1 + 17*s1*t1 + 
              6*Power(s1,2)*t1 + 2*Power(s1,3)*t1 + 3*Power(s1,4)*t1 + 
              195*t2 + 281*s1*t2 + 52*Power(s1,2)*t2 - 
              33*Power(s1,3)*t2 - 2*s2*t2 + 4*s1*s2*t2 + 
              Power(s1,2)*s2*t2 + 4*Power(s1,3)*s2*t2 + 7*t1*t2 + 
              11*s1*t1*t2 - 3*Power(s1,2)*t1*t2 - 11*Power(s1,3)*t1*t2 - 
              130*Power(t2,2) - 59*s1*Power(t2,2) + 
              51*Power(s1,2)*Power(t2,2) + 11*s2*Power(t2,2) + 
              s1*s2*Power(t2,2) - 6*Power(s1,2)*s2*Power(t2,2) - 
              17*t1*Power(t2,2) + 15*Power(s1,2)*t1*Power(t2,2) + 
              22*Power(t2,3) - 35*s1*Power(t2,3) - s2*Power(t2,3) + 
              4*s1*s2*Power(t2,3) + t1*Power(t2,3) - 
              9*s1*t1*Power(t2,3) + 9*Power(t2,4) - s2*Power(t2,4) + 
              2*t1*Power(t2,4) + Power(s,4)*(8 - 2*s1 + t1 + t2) + 
              Power(s,3)*(13 + 6*Power(s1,2) - 4*t1 + 
                 s1*(-34 + s2 - 6*t1 - 9*t2) - s2*(-4 + t2) + 38*t2 + 
                 5*t1*t2 + 3*Power(t2,2)) + 
              Power(s,2)*(-127 - 6*Power(s1,3) - 14*t1 + 45*t2 - 
                 7*t1*t2 + 61*Power(t2,2) + 9*t1*Power(t2,2) + 
                 3*Power(t2,3) + 
                 Power(s1,2)*(52 - 3*s2 + 12*t1 + 15*t2) + 
                 s2*(8 + 7*t2 - 3*Power(t2,2)) - 
                 s1*(61 - 10*t1 + s2*(9 - 6*t2) + 113*t2 + 21*t1*t2 + 
                    12*Power(t2,2))) + 
              s*(203 + 2*Power(s1,4) + 7*t1 + 
                 Power(s1,3)*(-34 + 3*s2 - 10*t1 - 7*t2) - 257*t2 - 
                 31*t1*t2 + 54*Power(t2,2) - 2*t1*Power(t2,2) + 
                 40*Power(t2,3) + 7*t1*Power(t2,3) + Power(t2,4) + 
                 s2*(-2 + 19*t2 + 2*Power(t2,2) - 3*Power(t2,3)) + 
                 Power(s1,2)*
                  (s2*(6 - 9*t2) + t1*(-8 + 27*t2) + 
                    9*(7 + 12*t2 + Power(t2,2))) + 
                 s1*(302 - 117*t2 - 114*Power(t2,2) - 5*Power(t2,3) + 
                    t1*(8 + 10*t2 - 24*Power(t2,2)) + 
                    s2*(7 - 8*t2 + 9*Power(t2,2)))) - 
              (48 + 16*s2 + 2*Power(s1,4)*(-9 + t1) + 
                 2*Power(s,4)*(-3 + t1) - 16*t1 - 102*t2 - 12*s2*t2 + 
                 14*t1*t2 + 59*Power(t2,2) - 16*s2*Power(t2,2) + 
                 7*t1*Power(t2,2) + 3*Power(t2,3) + 13*s2*Power(t2,3) - 
                 8*t1*Power(t2,3) - 8*Power(t2,4) + 2*t1*Power(t2,4) - 
                 Power(s1,3)*(8 + 13*s2 + t1 - 62*t2 + 8*t1*t2) + 
                 Power(s,3)*(-6 + 4*s2 + s1*(36 - 8*t1) + t1 - 35*t2 + 
                    8*t1*t2) + 
                 Power(s1,2)*
                  (77 + t1 + 19*t2 - 6*t1*t2 - 78*Power(t2,2) + 
                    12*t1*Power(t2,2) + s2*(-16 + 39*t2)) + 
                 s1*(s2*(12 + 32*t2 - 39*Power(t2,2)) - 
                    t1*(14 + 8*t2 - 15*Power(t2,2) + 8*Power(t2,3)) + 
                    2*(59 - 68*t2 - 7*Power(t2,2) + 21*Power(t2,3))) + 
                 Power(s,2)*(81 - 10*s2 + 12*Power(s1,2)*(-6 + t1) + 
                    t1 - 3*t2 + 21*s2*t2 - 6*t1*t2 - 60*Power(t2,2) + 
                    12*t1*Power(t2,2) + 
                    s1*(4 - 21*s2 + 132*t2 - 3*t1*(1 + 8*t2))) + 
                 s*(-118 + Power(s1,3)*(60 - 8*t1) + 14*t1 + 140*t2 + 
                    8*t1*t2 + 6*Power(t2,2) - 15*t1*Power(t2,2) - 
                    39*Power(t2,3) + 8*t1*Power(t2,3) + 
                    Power(s1,2)*
                     (10 + 30*s2 + 3*t1 - 159*t2 + 24*t1*t2) + 
                    2*s2*(-6 - 13*t2 + 15*Power(t2,2)) - 
                    2*s1*(79 + t1 + 8*t2 - 6*t1*t2 - 69*Power(t2,2) + 
                       12*t1*Power(t2,2) + s2*(-13 + 30*t2))))*
               R1(1 - s + s1 - t2)))/((s - s2 + t1)*(-1 + s - s1 + t2)) + 
         ((-1 + s1)*(-s + s1 - t2)*
            (-(((3*Power(s,2) + s1*(1 + s2 - t1) + 4*t1 - 
                     s*(1 + 3*s1 + 3*s2 + t1) - t2)*(-2 + s - s1 + t2)*
                   Power(s - s1 + t2,2))/(-1 + s - s1 + t2)) - 
              ((-1 + s + t2)*(-2 + s - s1 + t2)*(s - s1 + t2)*
                 (10 + Power(s2,2) - 4*t1 - 2*s*t1 + 
                   2*s1*(-3 + s2 + t1) + 6*t2 + s*t2 - 
                   s2*(-1 + s - t1 + t2)))/(-1 + s - s1 + t2) - 
              ((-1 + s - s1 - s2 + t1)*Power(s - s1 + t2,2)*
                 (-9 + 3*Power(s,2) - s2 + s1*(3 + s2 - t1) + 5*t1 - 
                   6*t2 - s*(3*s1 + 3*s2 - t1 + t2)))/(-1 + s - s1 + t2) \
- (-2 + 2*s2 - 4*Power(s2,2) + 8*t1 + 2*s2*t1 - 6*Power(t1,2) + 
                 2*Power(s1,3)*(-3 + s2 + t1) - 5*t2 - 7*s2*t2 + 
                 2*Power(s2,2)*t2 + 4*t1*t2 + s2*t1*t2 + 
                 3*Power(t1,2)*t2 + 2*Power(t2,2) + 
                 Power(s2,2)*Power(t2,2) - 2*t1*Power(t2,2) + 
                 s2*t1*Power(t2,2) + 3*Power(t2,3) - s2*Power(t2,3) - 
                 Power(s,3)*(-2 + 2*s2 + t1 + t2) + 
                 Power(s1,2)*
                  (3*Power(s2,2) + t1 + Power(t1,2) + 10*t2 - 
                    3*t1*t2 - 3*s2*(1 + t2)) + 
                 Power(s,2)*(1 + 2*Power(s2,2) + Power(t1,2) + 4*t2 - 
                    5*t1*t2 + s1*(-6 + 5*s2 + 4*t1 + t2) - 
                    s2*(t1 + 2*t2)) - 
                 s1*(-9 + 8*t2 + 7*Power(t2,2) + 
                    Power(t1,2)*(1 + t2) + Power(s2,2)*(-2 + 4*t2) - 
                    t1*(-6 + 3*t2 + Power(t2,2)) + 
                    s2*(-1 - 7*t2 - 2*Power(t2,2) + t1*(3 + t2))) + 
                 s*(1 - 4*t1 + Power(t1,2) - 
                    5*Power(s1,2)*(-2 + s2 + t1) + 9*t2 + 
                    3*Power(s2,2)*t2 + 4*t1*t2 + Power(t1,2)*t2 + 
                    Power(t2,2) - 4*t1*Power(t2,2) + Power(t2,3) + 
                    s2*(-3 + 5*t1 - 6*t2 - Power(t2,2)) - 
                    s1*(9 + 5*Power(s2,2) + t1 + 2*Power(t1,2) + 
                       8*t2 - 8*t1*t2 + Power(t2,2) - 
                       s2*(5 + t1 + 4*t2))))/(-1 + s - s1 + t2) + 
              2*(-2 + 3*Power(s,4) + 2*s2 - 4*Power(s2,2) + 
                 Power(s1,3)*(7 - 2*t1) + 8*t1 + 2*s2*t1 - 
                 6*Power(t1,2) - Power(s,3)*(8 + 9*s1 + 3*s2 - 6*t2) - 
                 16*t2 - 7*s2*t2 - Power(s2,2)*t2 + 12*t1*t2 + 
                 s2*t1*t2 + 4*Power(t2,2) + 2*s2*Power(t2,2) + 
                 Power(s,2)*(12 + 9*Power(s1,2) + 
                    s1*(17 + 7*s2 - 2*t1 - 11*t2) + s2*(4 - 6*t2) - 
                    17*t2 + 3*Power(t2,2)) + 
                 s1*(20 + 2*Power(t1,2) + 9*Power(t2,2) + 
                    Power(s2,2)*(5 + t2) - 2*t1*(7 + 4*t2) + 
                    s2*(1 + t1*(-3 + t2) + Power(t2,2))) - 
                 Power(s1,2)*
                  (Power(s2,2) + s2*(-2 + t1 + t2) - 
                    2*(-5 - 8*t2 + t1*(5 + t2))) - 
                 s*(10 + 3*Power(s1,3) + 3*s2 + 3*Power(s2,2) - 4*t1 - 
                    5*s2*t1 + 2*Power(t1,2) + 
                    Power(s1,2)*(16 + 4*s2 - 4*t1 - 5*t2) - 22*t2 - 
                    6*t1*t2 + 13*Power(t2,2) + 3*s2*Power(t2,2) - 
                    s1*(-10 + Power(s2,2) + 29*t2 - 2*Power(t2,2) - 
                       2*t1*(5 + t2) + s2*(-4 + t1 + 8*t2)))) + 
              (-(((-7 + 2*s1 - s2 + t1 - 3*t2)*(-1 + s + t2)*
                      (-2 + s - s1 + t2)*(s - s1 + t2))/
                    (-1 + s - s1 + t2)) + 
                 ((-1 + s - s1 - s2 + t1)*
                    (-7 + 2*Power(s,2) + s1 - 2*s*(s1 + s2) + 2*t1 - 
                      4*t2)*Power(s - s1 + t2,2))/(-1 + s - s1 + t2) + 
                 ((-2 + 2*Power(s,2) + s1 + s2 - 2*s*(s1 + s2) + 
                      t1 - t2)*(-2 + s - s1 + t2)*Power(s - s1 + t2,2)\
)/(-1 + s - s1 + t2) - 2*(-20 + 2*Power(s,4) + 3*Power(s1,3) - 8*s2 - 
                    4*Power(s2,2) + 24*t1 - 4*Power(t1,2) + 
                    Power(s1,2)*(-11 - 2*s2 + t1 - 7*t2) - 
                    2*Power(s,3)*(3*s1 + s2 - 2*t2) - 29*t2 - 
                    11*s2*t2 + 2*Power(s2,2)*t2 + 7*t1*t2 + 
                    2*s2*t1*t2 + 3*Power(t2,2) + s2*Power(t2,2) + 
                    Power(s,2)*
                     (-4 + s1 + 6*Power(s1,2) + s2 + 4*s1*s2 - 6*t1 - 
                       8*s1*t2 - 4*s2*t2 + 2*Power(t2,2)) + 
                    s1*(13 - 2*Power(s2,2) + t1 + 4*t2 - t1*t2 + 
                       4*Power(t2,2) + s2*(3 - 2*t1 + t2)) + 
                    s*(11 - 2*Power(s1,3) + s2 - 2*Power(s2,2) - t1 + 
                       6*s2*t1 - 2*Power(s1,2)*(2 + s2 - 2*t2) + 
                       11*t2 - 6*s2*t2 - 2*t1*t2 - 4*Power(t2,2) - 
                       2*s2*Power(t2,2) + 
                       s1*(11 + s2 + 5*t1 + 5*t2 + 4*s2*t2 - 
                         2*Power(t2,2)))))*(2 + R1(1 - s + s1 - t2))))/
          ((-1 + s2)*(-1 + t1)) + 
         ((-1 + s1)*(-s + s1 - t2)*
            (-(((-1 + s - s1 - s2 + t1)*Power(s - s1 + t2,2)*
                   (-5 + Power(s,2) + 2*Power(s1,2) - 4*s2 + 2*t1 - 
                     s*(-7 + 3*s1 + 2*s2 + 2*t1) + 
                     s1*(-5 + 2*s2 + 2*t1 - 2*t2) + 2*t2 - 2*s2*t2))/
                 (-1 + s - s1 + t2)) + 
              ((-1 + s + t2)*(-2 + s - s1 + t2)*(s - s1 + t2)*
                 (-13 + 4*Power(s,2) + 2*Power(s1,2) - 3*s2 + 
                   2*Power(s2,2) - t1 + s1*(-5 + 5*s2 - 2*t2) - 
                   s2*t2 + s*(9 - 6*s1 - 6*s2 + t2)))/(-1 + s - s1 + t2) \
+ ((-2 + s - s1 + t2)*Power(s - s1 + t2,2)*
                 (-3 + 3*Power(s,2) + s1 - 2*Power(s1,2) + 4*s2 - 
                   2*s1*s2 - 5*t1 - 2*t2 + 2*s1*t2 - s2*t2 + 
                   s*(-3 + s1 - 3*s2 + 2*t1 + t2)))/(-1 + s - s1 + t2) + 
              (-4 - 2*Power(s1,3) + 6*t1 - 4*s2*t1 + 2*Power(t1,2) + 
                 8*t2 - 4*s2*t2 + 2*Power(s2,2)*t2 - 5*t1*t2 - 
                 Power(t1,2)*t2 - 5*Power(t2,2) + 10*s2*Power(t2,2) - 
                 6*t1*Power(t2,2) + s2*t1*Power(t2,2) - Power(t2,3) - 
                 s2*Power(t2,3) + 2*Power(s,3)*(2 + t1 + t2) + 
                 Power(s1,2)*
                  (Power(t1,2) + 2*(-1 + t2) - t1*(8 + t2) + 
                    s2*(8 + t1 + t2)) - 
                 Power(s,2)*(4 + 4*t1 - Power(t1,2) - 4*t1*t2 - 
                    3*Power(t2,2) + 2*s2*(2 + t2) + 
                    2*s1*(2 + 2*t1 + t2)) + 
                 s1*(-2 - 2*Power(s2,2) - Power(t1,2)*(-3 + t2) + 
                    5*t2 + Power(t2,2) - 2*s2*(-2 + (7 + t1)*t2) + 
                    t1*(-3 + 12*t2 + Power(t2,2))) + 
                 s*(2 + 2*Power(s2,2) - 5*t1 - 3*Power(t1,2) + 
                    2*Power(s1,2)*(1 + t1) - 7*t2 - 10*t1*t2 + 
                    Power(t1,2)*t2 - 3*Power(t2,2) + 
                    2*t1*Power(t2,2) + Power(t2,3) - 
                    s1*(-2 + 2*Power(t1,2) + s2*(4 + t1 - t2) + 
                       3*t1*(-4 + t2) + Power(t2,2)) + 
                    s2*(4 + 8*t2 - 3*Power(t2,2) + t1*(2 + t2))))/
               (-1 + s - s1 + t2) + 
              2*(4 - 3*Power(s,4) + 2*Power(s1,4) - 6*t1 + 4*s2*t1 - 
                 2*Power(t1,2) + 
                 Power(s,3)*(-14 + 5*s1 + 3*s2 - 6*t2) + 
                 Power(s1,3)*(1 + 4*s2 - 4*t2) - 19*t2 + s2*t2 + 
                 t1*t2 + 2*s2*t1*t2 + 11*Power(t2,2) + 
                 5*s2*Power(t2,2) - 
                 s1*(-13 + s2 - 7*t1 + 2*s2*t1 + 2*Power(t1,2) + 
                    12*t2 + 9*s2*t2 + 2*Power(s2,2)*t2 - 5*t1*t2 - 
                    6*Power(t2,2)) + 
                 Power(s1,2)*
                  (3 + 2*Power(s2,2) - 3*t1 - 7*t2 - 4*s2*t2 + 
                    2*Power(t2,2)) + 
                 Power(s,2)*(22 + Power(s1,2) - 2*t1 - 25*t2 - 
                    3*Power(t2,2) + 2*s2*(5 + 3*t2) + 
                    s1*(-s2 + 7*(3 + t2))) + 
                 s*(-13 - 5*Power(s1,3) + t1 + 2*Power(t1,2) + 31*t2 - 
                    2*t1*t2 - 13*Power(t2,2) + 
                    Power(s1,2)*(-8 - 6*s2 + 3*t2) + 
                    s2*(-7 + 13*t2 + 3*Power(t2,2)) - 
                    s1*(21 + 2*Power(s2,2) - 5*t1 - 28*t2 - 
                       2*Power(t2,2) + s2*(10 + t2)))) + 
              (((-1 + s - s1 - s2 + t1)*Power(s - s1 + t2,2)*
                    (-5 + Power(s,2) + Power(s1,2) - 3*s2 + t1 + 
                      s1*(-3 + t1 - t2) - 
                      s*(-5 + 2*s1 + s2 + t1 - t2) + 2*t2 - s2*t2))/
                  (-1 + s - s1 + t2) - 
                 ((-1 + s + t2)*(-2 + s - s1 + t2)*(s - s1 + t2)*
                    (-9 + 2*Power(s,2) + Power(s1,2) - 4*s2 + 
                      Power(s2,2) + t1 + s1*(-3 + 2*s2 - t2) + t2 - 
                      s2*t2 + s*(7 - 3*s1 - 3*s2 + t2)))/
                  (-1 + s - s1 + t2) - 
                 ((-2 + s - s1 + t2)*Power(s - s1 + t2,2)*
                    (-1 + Power(s,2) - Power(s1,2) - t1 + 
                      s*(-1 + s1 - s2 + t1) - 2*t2 + s1*(1 - s2 + t2))\
)/(-1 + s - s1 + t2) + 2*(20 + Power(s,4) - Power(s1,4) + 8*s2 + 
                    4*Power(s2,2) - 2*t1 - 8*s2*t1 + 2*Power(t1,2) + 
                    3*t2 + 6*s2*t2 + Power(s2,2)*t2 - 3*t1*t2 - 
                    2*s2*t1*t2 - 11*Power(t2,2) - 2*s2*Power(t2,2) + 
                    Power(s1,3)*(1 - 2*s2 + 2*t2) - 
                    Power(s,3)*(s1 + s2 - 2*(5 + t2)) - 
                    Power(s,2)*
                     (5 + 2*Power(s1,2) - 2*t1 - 17*t2 - Power(t2,2) + 
                       2*s2*(4 + t2) + s1*(13 + s2 + t2)) + 
                    Power(s1,2)*
                     (2 - Power(s2,2) + t1 - Power(t2,2) + 
                       s2*(5 + 3*t2)) + 
                    s1*(3 + 2*Power(t1,2) - 3*t1*(-1 + t2) + 7*t2 - 
                       Power(t2,2) + Power(s2,2)*(3 + t2) - 
                       s2*(6 + 2*t1 - t2 + Power(t2,2))) + 
                    s*(-25 + 3*Power(s1,3) + Power(s2,2) + 3*t1 - 
                       2*Power(t1,2) + Power(s1,2)*(2 + 4*s2 - 3*t2) - 
                       20*t2 + 4*t1*t2 + 7*Power(t2,2) + 
                       s1*(11 + s2 + Power(s2,2) - 3*t1 - 13*t2 - 
                        2*s2*t2) - s2*(-2 + 10*t2 + Power(t2,2)))))*
               (2 + R1(1 - s + s1 - t2))))/((-1 + s2)*(-s + s2 - t1)) + 
         2*(((-1 + s1)*(1 - s + s1 + s2 - t1)*(-3 - 2*s + 2*s1 - 2*t2)*
               Power(s - s1 + t2,2))/(1 - s + s1 - t2) + 
            ((-1 + s + t2)*(-2 + s - s1 + t2)*(s - s1 + t2)*
               (-7 - 2*Power(s1,2) + s*(-5 + 2*s1) - 5*t2 + 
                 2*s1*(4 + t2)))/(-1 + s - s1 + t2) - 
            ((-2 + s - s1 + t2)*Power(s - s1 + t2,2)*
               (2 + 3*s + 2*Power(s1,2) + 2*s2 - 2*t1 + 5*t2 - 
                 s1*(3 + 2*s2 - 2*t1 + 2*t2)))/(-1 + s - s1 + t2) + 
            2*(3*Power(s,3) - 2*Power(s1,4) - 8*s2 + 8*t1 - 22*t2 + 
               2*s2*t2 + t1*t2 - 2*Power(t2,2) + 2*s2*Power(t2,2) + 
               Power(s1,3)*(21 - 2*s2 + 4*t2) + 
               Power(s,2)*(3 - 2*Power(s1,2) + s1*(11 - 2*s2) + 2*s2 + 
                  6*t2) + Power(s1,2)*
                (-27 + t1 - 39*t2 - 2*Power(t2,2) + 4*s2*(1 + t2)) + 
               s1*(22 + 29*t2 + 18*Power(t2,2) - t1*(9 + t2) - 
                  2*s2*(-3 + 3*t2 + Power(t2,2))) + 
               s*(-14 + 4*Power(s1,3) + t1 + 
                  Power(s1,2)*(-35 + 4*s2 - 4*t2) + t2 + 3*Power(t2,2) + 
                  s2*(2 + 4*t2) - s1*(-16 + t1 - 29*t2 + s2*(6 + 4*t2)))) \
+ ((16 + Power(s1,5) + 8*s2 - 8*t1 + Power(s1,4)*(-14 + t1 - 4*t2) - 
                 10*t2 - 10*s2*t2 + 8*t1*t2 - 11*Power(t2,2) + 
                 s2*Power(t2,2) + 9*Power(t2,3) + t1*Power(t2,3) - 
                 3*Power(t2,4) - 
                 Power(s1,3)*
                  (-14 + s2 + t1 - 45*t2 + 3*t1*t2 - 6*Power(t2,2)) + 
                 Power(s,3)*(11 - Power(s1,2) + t1 - 3*t2 + 
                    s1*(14 - t1 + t2)) + 
                 s1*(26 - 8*(3 + t1)*t2 - (4 + 3*t1)*Power(t2,2) - 
                    (-23 + t1)*Power(t2,3) + Power(t2,4) + 
                    s2*(2 + 8*t2 - Power(t2,2))) + 
                 Power(s1,2)*
                  (35 - 19*t2 - 51*Power(t2,2) - 4*Power(t2,3) + 
                    s2*(-9 + 2*t2) + t1*(8 + 3*t2 + 3*Power(t2,2))) + 
                 Power(s,2)*(-7 + 3*Power(s1,3) + s2 + 31*t2 + 
                    3*t1*t2 - 9*Power(t2,2) - 
                    s1*(16 + s2 - 51*t2 - 3*Power(t2,2) + 
                       3*t1*(1 + t2)) + 3*Power(s1,2)*(t1 - 2*(7 + t2))\
) - s*(18 + 3*Power(s1,4) - 8*t1 + 3*Power(s1,3)*(-14 + t1 - 3*t2) - 
                    2*s2*(-5 + t2) + 18*t2 - 29*Power(t2,2) - 
                    3*t1*Power(t2,2) + 9*Power(t2,3) + 
                    Power(s1,2)*
                     (9 - 2*s2 + 93*t2 + 9*Power(t2,2) - 
                       3*t1*(1 + 2*t2)) + 
                    s1*(20 + 2*s2*(-4 + t2) + 20*t2 - 60*Power(t2,2) - 
                       3*Power(t2,3) + t1*(8 + 6*t2 + 3*Power(t2,2)))))*
               (2 + R1(1 - s + s1 - t2)))/(1 - s + s1 - t2)) + 
         ((-1 + s1)*(-s + s1 - t2)*
            (((-1 + s - s1 - s2 + t1)*Power(s - s1 + t2,2)*
                 (4 - Power(s1,2) + 6*s2 + s*(2 + s1 - t1) - 5*t1 + 
                   s1*(-3*s2 + t1 - t2) + 7*t2 - 3*s2*t2 - Power(t2,2)))/
               (-1 + s - s1 + t2) - 
              ((-1 + s + t2)*(-2 + s - s1 + t2)*(s - s1 + t2)*
                 (4 + 9*s2 - 4*t1 - 2*s2*t1 + 
                   s1*(4 - 2*s2 + t1 - 3*t2) + 2*t2 - s2*t2 + 
                   s*(-4 - t1 + t2)))/(-1 + s - s1 + t2) + 
              ((-2 + s - s1 + t2)*Power(s - s1 + t2,2)*
                 (-2 + s1 - Power(s1,2) - s2 - t1 - 2*s1*t1 + 6*t2 - 
                   2*s1*t2 - s2*t2 - t1*t2 + s*(2 + s1 + 2*t1 + t2)))/
               (-1 + s - s1 + t2) + 
              (2 + 8*s2 + 8*Power(s2,2) - 8*t1 - 16*s2*t1 + 
                 6*Power(t1,2) + 2*t2 + 6*s2*t2 - 8*Power(s2,2)*t2 - 
                 3*t1*t2 + 12*s2*t1*t2 - 3*Power(t1,2)*t2 - 
                 4*Power(t2,2) - 10*s2*Power(t2,2) + 
                 2*Power(s2,2)*Power(t2,2) + 5*t1*Power(t2,2) - 
                 s2*t1*Power(t2,2) + 2*Power(t2,3) + 2*s2*Power(t2,3) - 
                 2*t1*Power(t2,3) - Power(s,3)*(-2 + t1 + t2) - 
                 Power(s,2)*(5 - t1 + Power(t1,2) + 
                    s1*(4 + s2 - 2*t1 - t2) - 9*t2 + 2*t1*t2 + 
                    3*Power(t2,2) + s2*(2 - 2*t1 + t2)) - 
                 Power(s1,2)*
                  (14 + 2*Power(s2,2) - 6*t1 + Power(t1,2) - 10*t2 + 
                    s2*(2 - 5*t1 + 2*t2)) + 
                 s1*(-8 + 22*t2 - 12*Power(t2,2) + 
                    Power(t1,2)*(1 + t2) + s2*(-2 - 4*(-1 + t1)*t2) + 
                    t1*(3 - 7*t2 + 2*Power(t2,2))) + 
                 s*(-2 + Power(s1,2)*(2 + s2 - t1) + 7*t1 - 
                    Power(t1,2) + 2*Power(s2,2)*(-2 + t2) - 13*t2 + 
                    2*t1*t2 - Power(t1,2)*t2 + 11*Power(t2,2) - 
                    3*t1*Power(t2,2) - 2*Power(t2,3) + 
                    s2*(-4 - 6*t2 + Power(t2,2) + t1*(6 + t2)) + 
                    s1*(19 + 2*Power(s2,2) - 7*t1 + 2*Power(t1,2) - 
                       17*t2 + 2*t1*t2 + 2*Power(t2,2) + 
                       s2*(6 - 7*t1 + 2*t2))))/(-1 + s - s1 + t2) - 
              2*(2 - Power(s1,4) + 8*s2 + 8*Power(s2,2) - 8*t1 - 
                 16*s2*t1 + 6*Power(t1,2) - Power(s1,3)*(2*s2 + t1) + 
                 7*t2 + 19*s2*t2 - 4*Power(s2,2)*t2 - 11*t1*t2 + 
                 2*s2*t1*t2 + 3*Power(t2,2) - 5*s2*Power(t2,2) - 
                 6*t1*Power(t2,2) - 2*Power(t2,3) - s2*Power(t2,3) + 
                 Power(s,3)*(2 + s1 + 2*t1 + t2) - 
                 Power(s,2)*(6 + 3*Power(s1,2) + s2 + 6*t1 + 
                    s1*(4 + 5*t1) - 4*t2 + s2*t2 - 4*t1*t2 - 
                    2*Power(t2,2)) + 
                 Power(s1,2)*
                  (-6 + s2*(13 - 2*t1) + 13*t2 + t1*(-10 + 3*t2)) + 
                 s1*(-13 - 4*Power(s2,2) - 2*Power(t1,2) + 7*t2 - 
                    11*Power(t2,2) + Power(t2,3) + 
                    t1*(11 + 20*t2 - 2*Power(t2,2)) + 
                    s2*(-15 - 16*t2 + 3*Power(t2,2) + 2*t1*(5 + t2))) + 
                 s*(3 + 3*Power(s1,3) - t1 + 2*Power(t1,2) + 
                    Power(s1,2)*(2 + 2*s2 + 4*t1 - t2) - 7*t2 - 
                    16*t1*t2 + 2*Power(t2,2) + 2*t1*Power(t2,2) + 
                    Power(t2,3) + s2*(9 - 4*t1 - 2*Power(t2,2)) + 
                    s1*(12 + 16*t1 - 15*t2 - 7*t1*t2 + 
                       s2*(-10 + 2*t1 + 3*t2)))) + 
              (-(((-1 + s - s1 - s2 + t1)*Power(s - s1 + t2,2)*
                      (3 - Power(s1,2) + s*(2 + s1) + 2*s2 - s1*s2 - 
                        2*t1 + 4*t2 - s2*t2))/(-1 + s - s1 + t2)) - 
                 ((-1 + s + t2)*(-2 + s - s1 + t2)*(s - s1 + t2)*
                    (-1 + s - 4*s2 + t1 + s2*t1 - 2*t2 + 
                      s1*(-1 + s2 + t2)))/(-1 + s - s1 + t2) - 
                 ((-2 + s - s1 + t2)*Power(s - s1 + t2,2)*
                    (2 - Power(s1,2) - 2*s2 + 3*t2 - s2*t2 - 
                      s1*(-1 + t1 + t2) + s*(1 + s1 + t1 + t2)))/
                  (-1 + s - s1 + t2) + 
                 2*(4 - Power(s1,4) + 14*s2 + 4*Power(s2,2) - 8*t1 - 
                    10*s2*t1 + 4*Power(t1,2) - 
                    Power(s1,3)*(-2 + s2 + t1 - t2) + 11*t2 + 12*s2*t2 - 
                    2*Power(s2,2)*t2 - 9*t1*t2 + s2*t1*t2 + 
                    14*Power(t2,2) - 6*s2*Power(t2,2) - 
                    3*t1*Power(t2,2) - Power(t2,3) - s2*Power(t2,3) + 
                    Power(s,3)*(1 + s1 + t1 + t2) + 
                    Power(s1,2)*
                     (4 - s2*(-5 + t1) + 5*t2 + 2*t1*(1 + t2)) + 
                    Power(s,2)*
                     (-1 - 3*Power(s1,2) + t1 - 3*s1*t1 + 5*t2 + 
                       2*t1*t2 + 2*Power(t2,2) - s2*(2 + t2)) - 
                    s1*(1 + 2*Power(s2,2) + 8*t2 + 6*Power(t2,2) + 
                       t1*(-3 - 3*t2 + Power(t2,2)) - 
                       s2*(-2 - 3*t2 + 2*Power(t2,2) + t1*(5 + t2))) + 
                    s*(3 + 3*Power(s1,3) - 5*t1 + 
                       Power(s1,2)*(-3 + s2 + 3*t1 - 2*t2) + 5*t2 - 
                       2*t1*t2 + 3*Power(t2,2) + t1*Power(t2,2) + 
                       Power(t2,3) - 
                       s2*(-2 + t1 + 6*t2 + 2*Power(t2,2)) - 
                       s1*(3 + 3*t1 + 8*t2 + 4*t1*t2 + Power(t2,2) - 
                         s2*(-1 + t1 + 2*t2)))))*(2 + R1(1 - s + s1 - t2))\
))/((-1 + t1)*(-1 + t2))))/(Power(-1 + s1,2)*Power(s - s1 + t2,3)) + 
    (8*(((-1 + t2)*(-2 + 2*s1 + 2*s2 - 2*Power(s1,2)*s2 - 2*t1 + 
              2*s1*t1 - 31*t2 - 23*s1*t2 - 22*Power(s1,2)*t2 + 
              s1*s2*t2 - Power(s1,2)*s2*t2 + t1*t2 + 6*s1*t1*t2 - 
              7*Power(s1,2)*t1*t2 - 50*Power(t2,2) - 38*s1*Power(t2,2) + 
              12*Power(s1,2)*Power(t2,2) + 9*s2*Power(t2,2) - 
              10*s1*s2*Power(t2,2) + Power(s1,2)*s2*Power(t2,2) - 
              7*t1*Power(t2,2) + 6*s1*t1*Power(t2,2) + 
              Power(s1,2)*t1*Power(t2,2) + 33*Power(t2,3) - 
              3*s1*Power(t2,3) - 2*Power(s1,2)*Power(t2,3) - 
              s2*Power(t2,3) + s1*s2*Power(t2,3) + 2*t1*Power(t2,3) - 
              2*s1*t1*Power(t2,3) - 6*Power(t2,4) + 2*s1*Power(t2,4) + 
              s*(4 - (4 + 7*t1)*t2 + (59 + t1)*Power(t2,2) - 
                 5*Power(t2,3) + 
                 s1*(4 + (-20 + 7*t1)*t2 - (7 + t1)*Power(t2,2) + 
                    Power(t2,3))) + 
              2*(-1 + s2 - t1 + 4*t2 - 2*s2*t2 + 4*t1*t2 - 
                 23*Power(t2,2) + 5*s2*Power(t2,2) - 3*t1*Power(t2,2) + 
                 12*Power(t2,3) - 
                 Power(s1,2)*s2*(1 - 6*t2 + Power(t2,2)) + 
                 2*s*(1 + s1 - t2 - 5*s1*t2 + 6*Power(t2,2) + 
                    2*s1*Power(t2,2)) + 
                 s1*(1 - 4*(4 + s2)*t2 - (9 + 4*s2)*Power(t2,2) + 
                    t1*(1 - 4*t2 + 3*Power(t2,2))))*R1(t2)))/(s - s1 + t2) \
+ ((-1 + s1)*(-1 + t1 - 3*s*t1 + 21*t2 + s*t2 - 3*s2*t2 + 16*t1*t2 + 
              10*s*t1*t2 + 12*Power(t2,2) - 2*t1*Power(t2,2) - 
              8*s*t1*Power(t2,2) + 67*Power(t2,3) - 2*s*Power(t2,3) + 
              4*s2*Power(t2,3) - 9*t1*Power(t2,3) + s*t1*Power(t2,3) - 
              11*Power(t2,4) + s*Power(t2,4) - s2*Power(t2,4) + 
              2*t1*Power(t2,4) + 
              s1*(2*(9 - 5*t2)*t2 + t1*(3 + 20*t2 + Power(t2,3)) - 
                 s2*(3 - 2*t2 - 2*Power(t2,2) + Power(t2,3))) + 
              (-1 - 5*t2 + 5*s*t2 - 7*s2*t2 + 15*Power(t2,2) - 
                 10*s*Power(t2,2) + 18*s2*Power(t2,2) + 
                 29*Power(t2,3) + 7*s*Power(t2,3) - 13*s2*Power(t2,3) - 
                 6*Power(t2,4) - 2*s*Power(t2,4) + 2*s2*Power(t2,4) + 
                 t1*(1 + 12*t2 + 5*Power(t2,2) - 2*Power(t2,4) - 
                    3*s*(1 - 4*t2 + 3*Power(t2,2))) + 
                 s1*(2*t2*(5 + 8*t2 - 5*Power(t2,2)) - 
                    3*s2*(1 - 4*t2 + 3*Power(t2,2)) + 
                    3*t1*(1 - 4*t2 + 3*Power(t2,2))))*R1(t2)))/(-1 + t1) + 
         2*(2 - t1 - 11*t2 + 6*s*t2 + 7*s2*t2 - 15*t1*t2 + 
            74*Power(t2,2) - 8*s*Power(t2,2) - 9*s2*Power(t2,2) + 
            10*t1*Power(t2,2) - 29*Power(t2,3) + 2*s*Power(t2,3) + 
            2*s2*Power(t2,3) - 2*t1*Power(t2,3) + 4*Power(t2,4) + 
            s1*(4 - (1 + 7*s2)*t2 + (14 + 9*s2)*Power(t2,2) + 
               (9 - 2*s2)*Power(t2,3) - 2*Power(t2,4) + 
               t1*(1 + 15*t2 - 10*Power(t2,2) + 2*Power(t2,3))) + 
            (2 - t1 + 6*t2 - 6*s*t2 - 3*s2*t2 + 11*t1*t2 + Power(t2,2) + 
               8*s*Power(t2,2) + 4*s2*Power(t2,2) - 3*t1*Power(t2,2) + 
               2*Power(t2,3) - 2*s*Power(t2,3) - s2*Power(t2,3) + 
               t1*Power(t2,3) - 3*Power(t2,4) + 
               s1*(4 + (-26 + 3*s2)*t2 + (59 - 4*s2)*Power(t2,2) + 
                  (-14 + s2)*Power(t2,3) + Power(t2,4) - 
                  t1*(-1 + 11*t2 - 3*Power(t2,2) + Power(t2,3))))*R1(t2)) \
- ((-1 + s1)*(-1 + t2)*(-4 - 5*s1 - 2*Power(s1,2) - s2 - 2*s1*s2 + 
              Power(s1,2)*s2 + s1*Power(s2,2) + 3*t1 + s1*t1 + 
              Power(s1,2)*t1 + 3*s2*t1 + Power(t1,2) + s1*Power(t1,2) + 
              9*t2 + 4*s1*t2 + 6*Power(s1,2)*t2 - 4*s2*t2 + 6*s1*s2*t2 - 
              3*Power(s1,2)*s2*t2 + Power(s2,2)*t2 - 
              3*s1*Power(s2,2)*t2 - 39*t1*t2 + 24*s1*t1*t2 - 
              3*Power(s1,2)*t1*t2 - 24*s2*t1*t2 - 14*s1*s2*t1*t2 - 
              3*Power(t1,2)*t2 - 3*s1*Power(t1,2)*t2 - 96*Power(t2,2) - 
              27*s1*Power(t2,2) + 18*s2*Power(t2,2) - 
              18*s1*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) + 
              18*t1*Power(t2,2) + 3*s1*t1*Power(t2,2) + 
              5*s2*t1*Power(t2,2) + 2*s1*s2*t1*Power(t2,2) + 
              23*Power(t2,3) - 4*s1*Power(t2,3) - 3*s2*Power(t2,3) + 
              2*s1*s2*Power(t2,3) - 4*t1*Power(t2,3) + 
              Power(s,2)*(t1*(-1 - 4*t2 + Power(t2,2)) + 
                 t2*(7 - 4*t2 + Power(t2,2))) + 
              s*(3 - 13*t2 + 64*Power(t2,2) + 3*s2*Power(t2,2) - 
                 4*Power(t2,3) - s2*Power(t2,3) + 
                 Power(t1,2)*(-1 + 3*t2) + 
                 t1*(-3 + s2 + 38*t2 - 3*s2*t2 - 21*Power(t2,2) + 
                    2*Power(t2,3)) - 
                 s1*(-4 + (5 - 7*t1)*t2 + (5 + t1)*Power(t2,2) + 
                    s2*(1 + 4*t2 - Power(t2,2)))) + 
              (-4 - s2 + 3*t1 + 3*s2*t1 + Power(t1,2) - 5*t2 + 
                 3*s2*t2 + 5*Power(s2,2)*t2 - 4*t1*t2 - 19*s2*t1*t2 - 
                 6*Power(t1,2)*t2 - 41*Power(t2,2) - 6*s2*Power(t2,2) - 
                 2*Power(s2,2)*Power(t2,2) + 3*t1*Power(t2,2) + 
                 9*s2*t1*Power(t2,2) + Power(t1,2)*Power(t2,2) + 
                 9*Power(t2,3) - s2*Power(t2,3) + 
                 Power(s2,2)*Power(t2,3) + 2*t1*Power(t2,3) - 
                 s2*t1*Power(t2,3) + Power(t2,4) + s2*Power(t2,4) + 
                 Power(s1,2)*(-2 + s2 + t1)*(1 - 6*t2 + Power(t2,2)) - 
                 Power(s,2)*(t1*(1 - 6*t2 + Power(t2,2)) + 
                    t2*(5 - 2*t2 + Power(t2,2))) + 
                 s1*(-5 + 21*t2 - 35*Power(t2,2) + 3*Power(t2,3) + 
                    s2*(-2 + 8*t2 - 6*Power(t2,2)) + 
                    Power(s2,2)*(1 - 6*t2 + Power(t2,2)) + 
                    Power(t1,2)*(1 - 6*t2 + Power(t2,2)) - 
                    t1*(-1 + 3*t2 - 11*Power(t2,2) + Power(t2,3))) - 
                 s*(-3 - 11*t2 + 4*s2*t2 - 44*Power(t2,2) + 
                    4*s2*Power(t2,2) + 5*Power(t2,3) + Power(t2,4) + 
                    Power(t1,2)*(1 - 6*t2 + Power(t2,2)) - 
                    t1*(-3 + s2 + 10*t2 - 6*s2*t2 - 11*Power(t2,2) + 
                       s2*Power(t2,2)) + 
                    s1*(-4 + 19*t2 - 2*Power(t2,2) - Power(t2,3) + 
                       s2*(1 - 6*t2 + Power(t2,2)))))*R1(t2)))/
          ((-1 + s2)*(-1 + t1)) - 
         ((-1 + s1)*(-1 + t2)*
            (5 + 5*s1 - 3*s2 - 5*s1*s2 - Power(s1,2)*s2 + 
              s1*Power(s2,2) + 5*t1 + 6*s1*t1 - Power(s1,2)*t1 - s2*t1 - 
              3*s1*s2*t1 + 2*Power(t1,2) - 71*t2 - 65*s1*t2 - 
              14*Power(s1,2)*t2 + 3*s2*t2 + 37*s1*s2*t2 - 
              4*Power(s1,2)*s2*t2 + Power(s2,2)*t2 - 
              3*s1*Power(s2,2)*t2 - 39*t1*t2 - 52*s1*t1*t2 - 
              4*Power(s1,2)*t1*t2 + 3*s2*t1*t2 + 2*s1*s2*t1*t2 + 
              Power(t1,2)*t2 - 7*s1*Power(t1,2)*t2 + 108*Power(t2,2) + 
              17*s1*Power(t2,2) - 10*Power(s1,2)*Power(t2,2) - 
              43*s2*Power(t2,2) - 38*s1*s2*Power(t2,2) + 
              Power(s1,2)*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) + 
              28*t1*Power(t2,2) + 39*s1*t1*Power(t2,2) + 
              Power(s1,2)*t1*Power(t2,2) + 3*s2*t1*Power(t2,2) + 
              s1*s2*t1*Power(t2,2) - Power(t1,2)*Power(t2,2) + 
              s1*Power(t1,2)*Power(t2,2) - 43*Power(t2,3) + 
              3*s1*Power(t2,3) + 16*s2*Power(t2,3) + 
              2*s1*s2*Power(t2,3) - 8*t1*Power(t2,3) - 
              5*s1*t1*Power(t2,3) - s2*t1*Power(t2,3) + Power(t2,4) - 
              s2*Power(t2,4) + 2*t1*Power(t2,4) + 
              2*Power(s,2)*(-1 - 16*Power(t2,2) + Power(t2,3)) + 
              s*(-t1 + 41*t2 + 8*t1*t2 + 7*Power(t1,2)*t2 - 
                 33*t1*Power(t2,2) - Power(t1,2)*Power(t2,2) - 
                 14*Power(t2,3) + 4*t1*Power(t2,3) + Power(t2,4) + 
                 s1*(6 + t1 - 35*t2 + 4*t1*t2 + 52*Power(t2,2) - 
                    t1*Power(t2,2) - 3*Power(t2,3)) + 
                 s2*(2 + t1 - t2 - 3*t1*t2 + 31*Power(t2,2) - 
                    2*Power(t2,3))) + 
              (5 - 3*s2 + 5*t1 - s2*t1 + 2*Power(t1,2) - 30*t2 + 
                 10*s2*t2 + 5*Power(s2,2)*t2 - 37*t1*t2 + 2*s2*t1*t2 - 
                 12*Power(t1,2)*t2 + 38*Power(t2,2) - 
                 32*s2*Power(t2,2) - 2*Power(s2,2)*Power(t2,2) + 
                 43*t1*Power(t2,2) - 5*s2*t1*Power(t2,2) + 
                 2*Power(t1,2)*Power(t2,2) - 10*Power(t2,3) + 
                 18*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) - 
                 11*t1*Power(t2,3) - 3*Power(t2,4) - s2*Power(t2,4) + 
                 2*Power(s,2)*
                  (-1 + 3*t2 - 11*Power(t2,2) + Power(t2,3)) - 
                 Power(s1,2)*
                  (12*t2*(1 + t2) + s2*(1 - 6*t2 + Power(t2,2)) + 
                    t1*(1 - 6*t2 + Power(t2,2))) + 
                 s1*(5 - 33*t2 + 7*Power(t2,2) + 13*Power(t2,3) + 
                    Power(s2,2)*(1 - 6*t2 + Power(t2,2)) - 
                    t1*(-6 + 33*t2 - 16*Power(t2,2) + Power(t2,3)) + 
                    s2*(-5 + 22*t2 - 41*Power(t2,2) + 4*Power(t2,3) - 
                       3*t1*(1 - 6*t2 + Power(t2,2)))) + 
                 s*(-t1 + 3*t2 + 3*t1*t2 + 27*Power(t2,2) - 
                    11*t1*Power(t2,2) - 23*Power(t2,3) + 
                    t1*Power(t2,3) + Power(t2,4) + 
                    s2*(2 + t1 - 11*t2 - 6*t1*t2 + 24*Power(t2,2) + 
                       t1*Power(t2,2) - 3*Power(t2,3)) + 
                    s1*(6 - 19*t2 + 44*Power(t2,2) - 3*Power(t2,3) + 
                       t1*(1 - 6*t2 + Power(t2,2)))))*R1(t2)))/
          ((s - s2 + t1)*(s - s1 + t2)) - 
         ((-1 + s1)*(-1 + t2)*
            (2 + 2*Power(s1,2) - 2*s2 - 2*s1*s2 + 10*t1 + 4*s1*t1 - 
              2*s2*t1 + 2*Power(t1,2) - 67*t2 - 23*s1*t2 - 
              12*Power(s1,2)*t2 + 14*s2*t2 + 27*s1*s2*t2 - 
              21*Power(s1,2)*s2*t2 + 8*Power(s2,2)*t2 - 
              14*s1*Power(s2,2)*t2 - 100*t1*t2 - 40*s1*t1*t2 + 
              7*Power(s1,2)*t1*t2 - 2*s2*t1*t2 + 7*s1*s2*t1*t2 + 
              Power(t1,2)*t2 - 7*s1*Power(t1,2)*t2 + 57*Power(t2,2) - 
              16*s1*Power(t2,2) + 6*Power(s1,2)*Power(t2,2) + 
              23*s2*Power(t2,2) - 7*s1*s2*Power(t2,2) + 
              3*Power(s1,2)*s2*Power(t2,2) - 18*Power(s2,2)*Power(t2,2) + 
              2*s1*Power(s2,2)*Power(t2,2) + 16*t1*Power(t2,2) + 
              11*s1*t1*Power(t2,2) - Power(s1,2)*t1*Power(t2,2) + 
              7*s2*t1*Power(t2,2) - s1*s2*t1*Power(t2,2) - 
              Power(t1,2)*Power(t2,2) + s1*Power(t1,2)*Power(t2,2) + 
              9*Power(t2,3) - 5*s1*Power(t2,3) - 4*s2*Power(t2,3) + 
              2*Power(s2,2)*Power(t2,3) - 2*t1*Power(t2,3) - 
              s1*t1*Power(t2,3) - s2*t1*Power(t2,3) - Power(t2,4) + 
              s2*Power(t2,4) - 
              Power(s,2)*(2 + s1*(2 - 6*t2) - 8*t2 + 39*Power(t2,2) - 
                 3*Power(t2,3) + t1*(2 - 13*t2 + Power(t2,2))) + 
              s*(-2*t1 + Power(s1,2)*(2 - 6*t2) + 33*t2 + 28*t1*t2 + 
                 7*Power(t1,2)*t2 + 25*Power(t2,2) - 30*t1*Power(t2,2) - 
                 Power(t1,2)*Power(t2,2) + 7*Power(t2,3) + 
                 4*t1*Power(t2,3) - Power(t2,4) + 
                 s2*(2 - 8*t2 + 57*Power(t2,2) - 5*Power(t2,3) + 
                    2*t1*(1 - 10*t2 + Power(t2,2))) + 
                 s1*(2 - 8*t2 + 21*Power(t2,2) - Power(t2,3) + 
                    s2*(2 + t2 - Power(t2,2)) + 
                    2*t1*(1 - 10*t2 + Power(t2,2)))) - 
              2*(-1 + s2 - 5*t1 + s2*t1 - Power(t1,2) + 16*t2 - 
                 8*s2*t2 + 4*Power(s2,2)*t2 + 34*t1*t2 - 8*s2*t1*t2 + 
                 6*Power(t1,2)*t2 - 14*Power(t2,2) - s2*Power(t2,2) + 
                 4*Power(s2,2)*Power(t2,2) - 15*t1*Power(t2,2) - 
                 s2*t1*Power(t2,2) - Power(t1,2)*Power(t2,2) + 
                 2*t1*Power(t2,3) - Power(t2,4) - 
                 Power(s1,2)*(1 - 6*t2 + Power(t2,2)) + 
                 Power(s,2)*(1 - t2 + 13*Power(t2,2) - Power(t2,3) + 
                    s1*(1 - 6*t2 + Power(t2,2)) + 
                    t1*(1 - 6*t2 + Power(t2,2))) - 
                 s*(-t1 + 7*t2 + 4*t1*t2 + 12*Power(t2,2) - 
                    3*t1*Power(t2,2) - 3*Power(t2,3) + 
                    Power(s1,2)*(1 - 6*t2 + Power(t2,2)) + 
                    s2*(1 + t1 + t2 - 6*t1*t2 + 15*Power(t2,2) + 
                       t1*Power(t2,2) - Power(t2,3)) + 
                    s1*(1 + t1 - 3*t2 - 6*t1*t2 + 11*Power(t2,2) + 
                       t1*Power(t2,2) - Power(t2,3) + 
                       s2*(1 - 6*t2 + Power(t2,2)))) + 
                 s1*(s2*(1 - 4*t2 + 3*Power(t2,2)) - 
                    2*(-2*t2*(1 + t2) + t1*(1 - 6*t2 + Power(t2,2)))))*
               R1(t2)))/((-1 + s2)*(-s + s2 - t1))))/
     (Power(-1 + s1,2)*Power(-1 + t2,3)*t2) + 
    (4*((-2*Power(-1 + s - s2 + t1,2)*(s - s2 + t1)*
             (-128 - 72*s2 + 389*Power(s2,2) + 348*Power(s2,3) + 
               35*Power(s2,4) + 78*Power(s2,5) + 26*Power(s2,6) - 
               25*Power(s2,7) + Power(s2,8) + 328*t1 - 386*s2*t1 - 
               1279*Power(s2,2)*t1 - 541*Power(s2,3)*t1 - 
               429*Power(s2,4)*t1 - 216*Power(s2,5)*t1 + 
               136*Power(s2,6)*t1 - 3*Power(s2,7)*t1 - 
               131*Power(t1,2) + 1202*s2*Power(t1,2) + 
               1236*Power(s2,2)*Power(t1,2) + 
               863*Power(s2,3)*Power(t1,2) + 
               604*Power(s2,4)*Power(t1,2) - 
               298*Power(s2,5)*Power(t1,2) - 
               7*Power(s2,6)*Power(t1,2) + Power(s2,7)*Power(t1,2) - 
               271*Power(t1,3) - 997*s2*Power(t1,3) - 
               840*Power(s2,2)*Power(t1,3) - 
               804*Power(s2,3)*Power(t1,3) + 
               326*Power(s2,4)*Power(t1,3) + 
               44*Power(s2,5)*Power(t1,3) - 5*Power(s2,6)*Power(t1,3) + 
               267*Power(t1,4) + 417*s2*Power(t1,4) + 
               562*Power(s2,2)*Power(t1,4) - 
               167*Power(s2,3)*Power(t1,4) - 
               81*Power(s2,4)*Power(t1,4) + 
               10*Power(s2,5)*Power(t1,4) - 89*Power(t1,5) - 
               204*s2*Power(t1,5) + 10*Power(s2,2)*Power(t1,5) + 
               73*Power(s2,3)*Power(t1,5) - 
               10*Power(s2,4)*Power(t1,5) + 32*Power(t1,6) + 
               26*s2*Power(t1,6) - 33*Power(s2,2)*Power(t1,6) + 
               5*Power(s2,3)*Power(t1,6) - 8*Power(t1,7) + 
               6*s2*Power(t1,7) - Power(s2,2)*Power(t1,7) + 
               Power(s1,4)*Power(s2 - t1,2)*
                (4 + 2*Power(s2,3) + 2*t1 - 3*Power(t1,2) - 
                  Power(t1,3) - 5*Power(s2,2)*(1 + t1) + 
                  s2*(-6 + 8*t1 + 4*Power(t1,2))) - 
               Power(s1,3)*(Power(s2,6) + Power(s2,5)*(15 - 7*t1) + 
                  Power(s2,4)*(49 - 62*t1 + 20*Power(t1,2)) + 
                  Power(s2,3)*
                   (41 - 117*t1 + 90*Power(t1,2) - 30*Power(t1,3)) + 
                  Power(s2,2)*
                   (24 - 91*t1 + 65*Power(t1,2) - 48*Power(t1,3) + 
                     25*Power(t1,4)) + 
                  t1*(-16 + 56*t1 - 25*Power(t1,2) - 22*Power(t1,3) + 
                     6*Power(t1,4) + 2*Power(t1,5)) - 
                  s2*(-16 + 80*t1 - 75*Power(t1,2) - 25*Power(t1,3) + 
                     Power(t1,4) + 11*Power(t1,5))) + 
               Power(s1,2)*(-128 - 2*Power(s2,7) + 296*t1 - 
                  131*Power(t1,2) - 35*Power(t1,3) - 41*Power(t1,4) + 
                  31*Power(t1,5) + 4*Power(t1,6) - Power(t1,7) + 
                  Power(s2,6)*(1 + 13*t1) + 
                  Power(s2,5)*(50 - 37*t1 - 34*Power(t1,2)) + 
                  Power(s2,4)*
                   (197 - 383*t1 + 152*Power(t1,2) + 45*Power(t1,3)) + 
                  Power(s2,3)*
                   (396 - 887*t1 + 821*Power(t1,2) - 
                     248*Power(t1,3) - 30*Power(t1,4)) + 
                  Power(s2,2)*
                   (325 - 1107*t1 + 1134*Power(t1,2) - 
                     662*Power(t1,3) + 187*Power(t1,4) + 7*Power(t1,5)\
) + s2*(-40 - 322*t1 + 746*Power(t1,2) - 403*Power(t1,3) + 
                     143*Power(t1,4) - 59*Power(t1,5) + 2*Power(t1,6))) \
+ s1*(256 - Power(s2,8) - 640*t1 + 378*Power(t1,2) + 59*Power(t1,3) + 
                  12*Power(t1,4) - 47*Power(t1,5) - 20*Power(t1,6) + 
                  6*Power(t1,7) + Power(s2,7)*(22 + 7*t1) - 
                  Power(s2,6)*(11 + 135*t1 + 17*Power(t1,2)) + 
                  Power(s2,5)*
                   (-134 + 208*t1 + 319*Power(t1,2) + 16*Power(t1,3)) \
+ Power(s2,4)*(-261 + 999*t1 - 812*Power(t1,2) - 362*Power(t1,3) + 
                     Power(t1,4)) + 
                  Power(s2,3)*
                   (-669 + 1477*t1 - 2168*Power(t1,2) + 
                     1314*Power(t1,3) + 186*Power(t1,4) - 
                     13*Power(t1,5)) + 
                  Power(s2,2)*
                   (-630 + 2005*t1 - 2143*Power(t1,2) + 
                     1828*Power(t1,3) - 989*Power(t1,4) - 
                     13*Power(t1,5) + 9*Power(t1,6)) + 
                  s2*(128 + 508*t1 - 1395*Power(t1,2) + 
                     915*Power(t1,3) - 478*Power(t1,4) + 
                     310*Power(t1,5) - 23*Power(t1,6) - 2*Power(t1,7))) \
+ 2*Power(s,6)*(-7 - Power(s2,2)*(-2 + t1) + 10*t1 - 2*Power(t1,2) + 
                  Power(s1,2)*(-5 + s2 + t1) + 
                  s2*(-3 - 3*t1 + Power(t1,2)) + 
                  s1*(11 - Power(s2,2) - 9*t1 + Power(t1,2) + 
                     2*s2*(1 + t1))) - 
               Power(s,5)*(-66 + Power(s1,4) + 
                  Power(s2,3)*(21 - 10*t1) + 96*t1 - 82*Power(t1,2) + 
                  20*Power(t1,3) + Power(s1,3)*(-4 + s2 + 2*t1) + 
                  Power(s2,2)*(-64 - 40*t1 + 21*Power(t1,2)) + 
                  s2*(-81 + 131*t1 - 10*Power(t1,3)) + 
                  Power(s1,2)*
                   (-21 + 7*Power(s2,2) + 29*t1 - 9*Power(t1,2) + 
                     s2*(-79 + 8*t1)) + 
                  s1*(51 - 11*Power(s2,3) - 81*t1 + 74*Power(t1,2) - 
                     10*Power(t1,3) + 4*Power(s2,2)*(12 + 7*t1) + 
                     s2*(197 - 163*t1 - 4*Power(t1,2)))) + 
               Power(s,4)*(-93 + Power(s1,4)*(-3 + 6*s2 - 5*t1) + 
                  359*t1 - 190*Power(t1,2) + 120*Power(t1,3) - 
                  40*Power(t1,4) - 5*Power(s2,4)*(-9 + 4*t1) + 
                  Power(s2,3)*(-221 - 103*t1 + 65*Power(t1,2)) + 
                  Power(s2,2)*
                   (-157 + 497*t1 + 31*Power(t1,2) - 65*Power(t1,3)) + 
                  s2*(-346 + 321*t1 - 402*Power(t1,2) + 
                     66*Power(t1,3) + 20*Power(t1,4)) + 
                  Power(s1,3)*
                   (6 + 3*Power(s2,2) + 10*t1 - 10*Power(t1,2) + 
                     s2*(-41 + 7*t1)) + 
                  Power(s1,2)*
                   (-49 + 6*Power(s2,3) + 89*t1 - 12*Power(t1,2) + 
                     15*Power(t1,3) + Power(s2,2)*(-213 + 29*t1) + 
                     s2*(-55 + 183*t1 - 50*Power(t1,2))) + 
                  s1*(35 - 25*Power(s2,4) - 214*t1 + 72*Power(t1,2) - 
                     110*Power(t1,3) + 20*Power(t1,4) + 
                     Power(s2,3)*(174 + 79*t1) + 
                     Power(s2,2)*(543 - 621*t1 - 47*Power(t1,2)) + 
                     s2*(385 - 611*t1 + 543*Power(t1,2) - 
                        26*Power(t1,3)))) + 
               Power(s,3)*(-43 - 221*t1 + 582*Power(t1,2) - 
                  102*Power(t1,3) + 60*Power(t1,4) - 40*Power(t1,5) + 
                  10*Power(s2,5)*(-5 + 2*t1) + 
                  Power(s2,4)*(364 + 132*t1 - 90*Power(t1,2)) + 
                  Power(s2,3)*
                   (99 - 1041*t1 - 46*Power(t1,2) + 140*Power(t1,3)) + 
                  Power(s2,2)*
                   (559 - 167*t1 + 1069*Power(t1,2) - 
                     140*Power(t1,3) - 90*Power(t1,4)) + 
                  s2*(419 - 1205*t1 + 205*Power(t1,2) - 
                     454*Power(t1,3) + 144*Power(t1,4) + 
                     20*Power(t1,5)) - 
                  2*Power(s1,4)*
                   (-1 + 7*Power(s2,2) + 6*t1 + 5*Power(t1,2) - 
                     s2*(7 + 12*t1)) + 
                  Power(s1,3)*
                   (13 - 2*Power(s2,3) + 40*t1 - 20*Power(t1,3) - 
                     2*Power(s2,2)*(-57 + 8*t1) + 
                     s2*(43 - 122*t1 + 38*Power(t1,2))) + 
                  s1*(192 + 30*Power(s2,5) + 52*t1 - 
                     381*Power(t1,2) - 70*Power(t1,3) - 
                     60*Power(t1,4) + 20*Power(t1,5) - 
                     4*Power(s2,4)*(74 + 29*t1) + 
                     2*Power(s2,3)*(-328 + 600*t1 + 57*Power(t1,2)) + 
                     2*Power(s2,2)*
                      (-349 + 544*t1 - 755*Power(t1,2) + 
                        8*Power(t1,3)) + 
                     s2*(-246 + 860*t1 - 295*Power(t1,2) + 
                        668*Power(t1,3) - 64*Power(t1,4))) + 
                  Power(s1,2)*
                   (6*Power(s2,4) + Power(s2,3)*(264 - 72*t1) + 
                     Power(s2,2)*(-15 - 332*t1 + 136*Power(t1,2)) + 
                     s2*(-67 + 82*t1 + 16*Power(t1,2) - 
                        80*Power(t1,3)) + 
                     2*(-16 - 97*t1 + 86*Power(t1,2) + 
                        21*Power(t1,3) + 5*Power(t1,4)))) + 
               Power(s,2)*(141 - 10*Power(s2,6)*(-3 + t1) - 429*t1 + 
                  104*Power(t1,2) + 262*Power(t1,3) + 84*Power(t1,4) - 
                  20*Power(t1,5) - 20*Power(t1,6) + 
                  4*Power(s2,5)*(-79 - 22*t1 + 15*Power(t1,2)) + 
                  Power(s2,4)*
                   (45 + 1151*t1 + 12*Power(t1,2) - 130*Power(t1,3)) + 
                  Power(s2,3)*
                   (-266 - 461*t1 - 1558*Power(t1,2) + 
                     206*Power(t1,3) + 130*Power(t1,4)) + 
                  Power(s2,2)*
                   (-532 + 902*t1 + 769*Power(t1,2) + 
                     913*Power(t1,3) - 266*Power(t1,4) - 
                     60*Power(t1,5)) + 
                  s2*(234 + 451*t1 - 935*Power(t1,2) - 
                     433*Power(t1,3) - 170*Power(t1,4) + 
                     126*Power(t1,5) + 10*Power(t1,6)) + 
                  2*Power(s1,4)*
                   (2 + 8*Power(s2,3) + 3*t1 - 9*Power(t1,2) - 
                     5*Power(t1,3) - 3*Power(s2,2)*(4 + 7*t1) + 
                     s2*(-5 + 21*t1 + 18*Power(t1,2))) - 
                  Power(s1,3)*
                   (40 + 2*Power(s2,4) + Power(s2,3)*(136 - 26*t1) - 
                     51*t1 - 84*Power(t1,2) + 20*Power(t1,3) + 
                     20*Power(t1,4) + 
                     3*Power(s2,2)*(51 - 92*t1 + 22*Power(t1,2)) + 
                     s2*(67 - 61*t1 + 120*Power(t1,2) - 
                        62*Power(t1,3))) + 
                  Power(s1,2)*
                   (21 - 14*Power(s2,5) - 83*t1 - 282*Power(t1,2) + 
                     192*Power(t1,3) + 58*Power(t1,4) + 
                     2*Power(s2,4)*(-77 + 46*t1) + 
                     Power(s2,3)*(161 + 194*t1 - 192*Power(t1,2)) + 
                     2*Power(s2,2)*
                      (239 - 411*t1 + 81*Power(t1,2) + 
                       82*Power(t1,3)) + 
                     s2*(468 - 541*t1 + 472*Power(t1,2) - 
                        260*Power(t1,3) - 50*Power(t1,4))) + 
                  s1*(-190 - 20*Power(s2,6) + 627*t1 + 
                     11*Power(t1,2) - 371*Power(t1,3) - 
                     162*Power(t1,4) + 10*Power(t1,5) + 
                     10*Power(t1,6) + Power(s2,5)*(264 + 94*t1) - 
                     2*Power(s2,4)*(-175 + 617*t1 + 65*Power(t1,2)) + 
                     Power(s2,3)*
                      (311 - 459*t1 + 2006*Power(t1,2) + 
                        22*Power(t1,3)) + 
                     Power(s2,2)*
                      (134 - 53*t1 - 498*Power(t1,2) - 
                        1362*Power(t1,3) + 80*Power(t1,4)) + 
                     s2*(-733 + 249*t1 + 83*Power(t1,2) + 
                        765*Power(t1,3) + 316*Power(t1,4) - 
                        56*Power(t1,5)))) + 
               s*(72 + 138*t1 - 657*Power(t1,2) + 499*Power(t1,3) - 
                  116*Power(t1,4) + 110*Power(t1,5) - 30*Power(t1,6) - 
                  4*Power(t1,7) + Power(s2,7)*(-9 + 2*t1) + 
                  Power(s2,6)*(140 + 28*t1 - 17*Power(t1,2)) + 
                  2*Power(s2,5)*
                   (-40 - 316*t1 + 7*Power(t1,2) + 25*Power(t1,3)) + 
                  Power(s2,4)*
                   (-91 + 619*t1 + 1107*Power(t1,2) - 
                     156*Power(t1,3) - 70*Power(t1,4)) + 
                  Power(s2,3)*
                   (171 + 373*t1 - 1388*Power(t1,2) - 
                     905*Power(t1,3) + 243*Power(t1,4) + 50*Power(t1,5)\
) + Power(s2,2)*(-539 + 303*t1 - 507*Power(t1,2) + 1341*Power(t1,3) + 
                     287*Power(t1,4) - 164*Power(t1,5) - 17*Power(t1,6)\
) + s2*(-402 + 1380*t1 - 965*Power(t1,2) + 341*Power(t1,3) - 
                     602*Power(t1,4) + 33*Power(t1,5) + 
                     48*Power(t1,6) + 2*Power(t1,7)) + 
                  Power(s1,4)*
                   (-9*Power(s2,4) + 2*Power(s2,3)*(9 + 16*t1) - 
                     2*Power(s2,2)*(-7 + 24*t1 + 21*Power(t1,2)) + 
                     t1*(8 + 6*t1 - 12*Power(t1,2) - 5*Power(t1,3)) + 
                     s2*(-8 - 20*t1 + 42*Power(t1,2) + 24*Power(t1,3))) \
+ Power(s1,3)*(16 + 3*Power(s2,5) + Power(s2,4)*(74 - 22*t1) - 96*t1 + 
                     63*Power(t1,2) + 72*Power(t1,3) - 
                     20*Power(t1,4) - 10*Power(t1,5) + 
                     Power(s2,3)*(153 - 226*t1 + 58*Power(t1,2)) + 
                     Power(s2,2)*
                      (95 - 218*t1 + 210*Power(t1,2) - 72*Power(t1,3)) \
+ s2*(64 - 142*t1 - 7*Power(t1,2) - 38*Power(t1,3) + 43*Power(t1,4))) + 
                  Power(s1,2)*
                   (168 + 9*Power(s2,6) + Power(s2,5)*(33 - 56*t1) - 
                     110*t1 - 86*Power(t1,2) - 178*Power(t1,3) + 
                     119*Power(t1,4) + 27*Power(t1,5) - 
                     3*Power(t1,6) + 
                     Power(s2,4)*(-162 + 21*t1 + 131*Power(t1,2)) - 
                     Power(s2,3)*
                      (559 - 1034*t1 + 318*Power(t1,2) + 
                        144*Power(t1,3)) + 
                     Power(s2,2)*
                      (-832 + 1622*t1 - 1469*Power(t1,2) + 
                        468*Power(t1,3) + 71*Power(t1,4)) - 
                     s2*(346 - 1198*t1 + 877*Power(t1,2) - 
                        478*Power(t1,3) + 231*Power(t1,4) + 
                        8*Power(t1,5))) + 
                  s1*(-256 + 7*Power(s2,7) + 60*t1 + 494*Power(t1,2) + 
                     6*Power(t1,3) - 200*Power(t1,4) - 99*Power(t1,5) + 
                     22*Power(t1,6) + 2*Power(t1,7) - 
                     40*Power(s2,6)*(3 + t1) + 
                     Power(s2,5)*(-51 + 645*t1 + 74*Power(t1,2)) - 
                     Power(s2,4)*
                      (-187 + 307*t1 + 1284*Power(t1,2) + 
                        38*Power(t1,3)) + 
                     Power(s2,3)*
                      (338 - 1592*t1 + 1533*Power(t1,2) + 
                        1166*Power(t1,3) - 37*Power(t1,4)) + 
                     Power(s2,2)*
                      (1210 - 1770*t1 + 2475*Power(t1,2) - 
                        2032*Power(t1,3) - 438*Power(t1,4) + 
                        52*Power(t1,5)) + 
                     s2*(692 - 2184*t1 + 1410*Power(t1,2) - 
                        870*Power(t1,3) + 956*Power(t1,4) + 
                        9*Power(t1,5) - 20*Power(t1,6))))) + 
            (64 + 120*s2 + 996*Power(s2,2) + 2757*Power(s2,3) + 
               3028*Power(s2,4) + 1518*Power(s2,5) + 350*Power(s2,6) + 
               26*Power(s2,7) - 20*Power(s2,8) - 15*Power(s2,9) - 
               248*t1 - 1896*s2*t1 - 8207*Power(s2,2)*t1 - 
               13389*Power(s2,3)*t1 - 9806*Power(s2,4)*t1 - 
               3484*Power(s2,5)*t1 - 481*Power(s2,6)*t1 + 
               34*Power(s2,7)*t1 + 25*Power(s2,8)*t1 + 
               16*Power(s2,9)*t1 + 964*Power(t1,2) + 
               7975*s2*Power(t1,2) + 20961*Power(s2,2)*Power(t1,2) + 
               22173*Power(s2,3)*Power(t1,2) + 
               10818*Power(s2,4)*Power(t1,2) + 
               2114*Power(s2,5)*Power(t1,2) + 
               209*Power(s2,6)*Power(t1,2) + 
               199*Power(s2,7)*Power(t1,2) - 
               111*Power(s2,8)*Power(t1,2) - 2525*Power(t1,3) - 
               13915*s2*Power(t1,3) - 23029*Power(s2,2)*Power(t1,3) - 
               15495*Power(s2,3)*Power(t1,3) - 
               4198*Power(s2,4)*Power(t1,3) - 
               924*Power(s2,5)*Power(t1,3) - 
               883*Power(s2,6)*Power(t1,3) + 
               348*Power(s2,7)*Power(t1,3) + 3315*Power(t1,4) + 
               11173*s2*Power(t1,4) + 11183*Power(s2,2)*Power(t1,4) + 
               4388*Power(s2,3)*Power(t1,4) + 
               1718*Power(s2,4)*Power(t1,4) + 
               1641*Power(s2,5)*Power(t1,4) - 
               653*Power(s2,6)*Power(t1,4) - 2029*Power(t1,5) - 
               3833*s2*Power(t1,5) - 2447*Power(s2,2)*Power(t1,5) - 
               1882*Power(s2,3)*Power(t1,5) - 
               1785*Power(s2,4)*Power(t1,5) + 
               810*Power(s2,5)*Power(t1,5) + 461*Power(t1,6) + 
               656*s2*Power(t1,6) + 1289*Power(s2,2)*Power(t1,6) + 
               1285*Power(s2,3)*Power(t1,6) - 
               681*Power(s2,4)*Power(t1,6) - 58*Power(t1,7) - 
               516*s2*Power(t1,7) - 649*Power(s2,2)*Power(t1,7) + 
               376*Power(s2,3)*Power(t1,7) + 92*Power(t1,8) + 
               218*s2*Power(t1,8) - 123*Power(s2,2)*Power(t1,8) - 
               36*Power(t1,9) + 18*s2*Power(t1,9) + 
               2*Power(s,8)*(-36 + 3*Power(s1,2) + s2*(10 - 4*t1) + 
                  s1*(29 - 6*s2 - 4*t1) + 8*t1) + 
               Power(s1,4)*Power(s2 - t1,2)*
                (16 + 22*Power(s2,4) - 10*t1 - 29*Power(t1,2) + 
                  8*Power(t1,3) + 11*Power(t1,4) - 
                  Power(s2,3)*(5 + 77*t1) + 
                  Power(s2,2)*(-55 + 18*t1 + 99*Power(t1,2)) - 
                  s2*(6 - 84*t1 + 21*Power(t1,2) + 55*Power(t1,3))) + 
               Power(s1,3)*(-47*Power(s2,7) + 
                  Power(s2,6)*(-22 + 328*t1) + 
                  Power(s2,5)*(179 + 53*t1 - 971*Power(t1,2)) + 
                  Power(s2,4)*
                   (204 - 1055*t1 + 65*Power(t1,2) + 
                     1580*Power(t1,3)) - 
                  Power(s2,3)*
                   (12 + 866*t1 - 2384*Power(t1,2) + 
                     350*Power(t1,3) + 1525*Power(t1,4)) + 
                  2*Power(s2,2)*
                   (-37 + 49*t1 + 661*Power(t1,2) - 
                     1306*Power(t1,3) + 230*Power(t1,4) + 
                     436*Power(t1,5)) + 
                  t1*(16 - 106*t1 + 90*Power(t1,2) + 
                     202*Power(t1,3) - 293*Power(t1,4) + 
                     57*Power(t1,5) + 36*Power(t1,6)) - 
                  s2*(16 - 180*t1 + 176*Power(t1,2) + 
                     862*Power(t1,3) - 1397*Power(t1,4) + 
                     263*Power(t1,5) + 273*Power(t1,6))) + 
               Power(s1,2)*(64 + 12*Power(s2,8) - 392*t1 + 
                  1160*Power(t1,2) - 1649*Power(t1,3) + 
                  737*Power(t1,4) + 554*Power(t1,5) - 
                  523*Power(t1,6) + 18*Power(t1,7) + 41*Power(t1,8) - 
                  3*Power(s2,7)*(30 + 37*t1) + 
                  Power(s2,6)*(-139 + 426*t1 + 465*Power(t1,2)) + 
                  Power(s2,5)*
                   (386 + 699*t1 - 725*Power(t1,2) - 
                     1121*Power(t1,3)) + 
                  Power(s2,4)*
                   (1054 - 1491*t1 - 1846*Power(t1,2) + 
                     458*Power(t1,3) + 1675*Power(t1,4)) + 
                  Power(s2,3)*
                   (1077 - 3889*t1 + 1589*Power(t1,2) + 
                     3257*Power(t1,3) + 48*Power(t1,4) - 
                     1577*Power(t1,5)) + 
                  Power(s2,2)*
                   (664 - 3603*t1 + 5321*Power(t1,2) + 
                     305*Power(t1,3) - 3600*Power(t1,4) - 
                     142*Power(t1,5) + 911*Power(t1,6)) + 
                  s2*(264 - 1760*t1 + 4175*Power(t1,2) - 
                     3223*Power(t1,3) - 1343*Power(t1,4) + 
                     2152*Power(t1,5) + 7*Power(t1,6) - 
                     295*Power(t1,7))) - 
               s1*(128 + Power(s2,9) - 624*t1 + 1938*Power(t1,2) - 
                  3632*Power(t1,3) + 3357*Power(t1,4) - 
                  1006*Power(t1,5) - 251*Power(t1,6) + 
                  6*Power(t1,7) + 110*Power(t1,8) - 18*Power(t1,9) - 
                  2*Power(s2,8)*(23 + 24*t1) + 
                  Power(s2,7)*(-257 + 350*t1 + 266*Power(t1,2)) + 
                  Power(s2,6)*
                   (275 + 900*t1 - 880*Power(t1,2) - 673*Power(t1,3)) \
+ Power(s2,5)*(2390 - 3862*t1 - 129*Power(t1,2) + 647*Power(t1,3) + 
                     1003*Power(t1,4)) + 
                  Power(s2,4)*
                   (4293 - 13676*t1 + 12745*Power(t1,2) - 
                     3312*Power(t1,3) + 932*Power(t1,4) - 
                     996*Power(t1,5)) + 
                  Power(s2,3)*
                   (3642 - 17556*t1 + 27610*Power(t1,2) - 
                     17546*Power(t1,3) + 5559*Power(t1,4) - 
                     2272*Power(t1,5) + 708*Power(t1,6)) + 
                  Power(s2,2)*
                   (1506 - 10900*t1 + 25510*Power(t1,2) - 
                     24758*Power(t1,3) + 10679*Power(t1,4) - 
                     3618*Power(t1,5) + 1892*Power(t1,6) - 
                     361*Power(t1,7)) + 
                  s2*(368 - 3316*t1 + 10890*Power(t1,2) - 
                     15604*Power(t1,3) + 9440*Power(t1,4) - 
                     2040*Power(t1,5) + 851*Power(t1,6) - 
                     733*Power(t1,7) + 118*Power(t1,8))) + 
               2*Power(s,7)*(267 + 11*Power(s1,3) - 304*t1 + 
                  56*Power(t1,2) + Power(s2,2)*(-74 + 26*t1) + 
                  s2*(281 + 30*t1 - 29*Power(t1,2)) + 
                  Power(s1,2)*(-30*s2 + 32*(1 + t1)) + 
                  s1*(-302 + 48*Power(s2,2) + 235*t1 - 
                     27*Power(t1,2) - s2*(233 + 19*t1))) + 
               Power(s,6)*(-1380 + 11*Power(s1,4) + 
                  Power(s2,3)*(453 - 128*t1) + 3424*t1 - 
                  2050*Power(t1,2) + 300*Power(t1,3) + 
                  Power(s1,3)*(29 - 167*s2 + 168*t1) + 
                  Power(s2,2)*(-1901 - 754*t1 + 309*Power(t1,2)) - 
                  2*s2*(1861 - 2082*t1 + 48*Power(t1,2) + 
                     81*Power(t1,3)) + 
                  Power(s1,2)*
                   (-515 + 235*Power(s2,2) + 392*t1 + 
                     299*Power(t1,2) - 31*s2*(15 + 16*t1)) + 
                  s1*(-325*Power(s2,3) + 
                     Power(s2,2)*(1635 + 476*t1) + 
                     s2*(4278 - 3370*t1 + 36*Power(t1,2)) + 
                     2*(917 - 1938*t1 + 751*Power(t1,2) - 
                        69*Power(t1,3)))) + 
               Power(s,4)*(317 + 3670*t1 - 12933*Power(t1,2) + 
                  12006*Power(t1,3) - 3280*Power(t1,4) + 
                  20*Power(t1,5) + 5*Power(s2,5)*(127 + 8*t1) + 
                  5*Power(s2,4)*(-838 - 667*t1 + 43*Power(t1,2)) + 
                  Power(s2,3)*
                   (-15878 + 18286*t1 + 4795*Power(t1,2) - 
                     600*Power(t1,3)) + 
                  Power(s2,2)*
                   (-17017 + 44660*t1 - 28093*Power(t1,2) - 
                     1945*Power(t1,3) + 395*Power(t1,4)) - 
                  s2*(4911 - 29441*t1 + 40874*Power(t1,2) - 
                     17300*Power(t1,3) + 170*Power(t1,4) + 
                     50*Power(t1,5)) + 
                  Power(s1,4)*
                   (-29 + 220*Power(s2,2) + 40*t1 + 
                     165*Power(t1,2) - s2*(37 + 385*t1)) + 
                  Power(s1,3)*
                   (164 - 1025*Power(s2,3) - 1065*t1 + 
                     575*Power(t1,2) + 980*Power(t1,3) + 
                     28*Power(s2,2)*(7 + 110*t1) + 
                     s2*(923 - 771*t1 - 3035*Power(t1,2))) + 
                  Power(s1,2)*
                   (-46 + 640*Power(s2,4) + 3720*t1 - 
                     7321*Power(t1,2) + 1450*Power(t1,3) + 
                     1265*Power(t1,4) - 
                     Power(s2,3)*(2552 + 2735*t1) + 
                     Power(s2,2)*
                      (-5507 + 5770*t1 + 4865*Power(t1,2)) - 
                     s2*(2731 - 12190*t1 + 4595*Power(t1,2) + 
                        4035*Power(t1,3))) + 
                  s1*(-44 - 675*Power(s2,5) - 7427*t1 + 
                     20453*Power(t1,2) - 14470*Power(t1,3) + 
                     1760*Power(t1,4) + 30*Power(t1,5) + 
                     10*Power(s2,4)*(403 + 264*t1) + 
                     Power(s2,3)*
                      (19581 - 16762*t1 - 3110*Power(t1,2)) + 
                     Power(s2,2)*
                      (21192 - 51647*t1 + 22746*Power(t1,2) + 
                        1085*Power(t1,3)) + 
                     s2*(6744 - 39410*t1 + 46147*Power(t1,2) - 
                        11771*Power(t1,3) + 30*Power(t1,4)))) + 
               Power(s,5)*(1278 - 6838*t1 + 8906*Power(t1,2) - 
                  3558*Power(t1,3) + 344*Power(t1,4) + 
                  2*Power(s2,4)*(-365 + 62*t1) + 
                  Power(s1,4)*(8 - 77*s2 + 66*t1) + 
                  Power(s2,3)*(3615 + 2285*t1 - 564*Power(t1,2)) + 
                  Power(s2,2)*
                   (10611 - 11933*t1 - 1469*Power(t1,2) + 
                     642*Power(t1,3)) - 
                  2*s2*(-3903 + 9904*t1 - 6027*Power(t1,2) + 
                     231*Power(t1,3) + 101*Power(t1,4)) + 
                  Power(s1,3)*
                   (-193 + 552*Power(s2,2) + 202*t1 + 
                     546*Power(t1,2) - s2*(127 + 1108*t1)) + 
                  Power(s1,2)*
                   (814 - 497*Power(s2,3) - 2987*t1 + 
                     1018*Power(t1,2) + 786*Power(t1,3) + 
                     2*Power(s2,2)*(729 + 788*t1) + 
                     s2*(2636 - 2313*t1 - 1875*Power(t1,2))) + 
                  s1*(606*Power(s2,4) - 
                     3*Power(s2,3)*(1087 + 526*t1) + 
                     Power(s2,2)*
                      (-12506 + 10178*t1 + 891*Power(t1,2)) + 
                     s2*(-10003 + 22488*t1 - 9221*Power(t1,2) + 
                        212*Power(t1,3)) - 
                     2*(995 - 4823*t1 + 5143*Power(t1,2) - 
                        1190*Power(t1,3) + 71*Power(t1,4)))) + 
               Power(s,3)*(-1171 + 3774*t1 + 1275*Power(t1,2) - 
                  11110*Power(t1,3) + 8754*Power(t1,4) - 
                  1292*Power(t1,5) - 384*Power(t1,6) - 
                  4*Power(s2,6)*(60 + 53*t1) + 
                  Power(s2,5)*(2992 + 2574*t1 + 630*Power(t1,2)) - 
                  2*Power(s2,4)*
                   (-6576 + 7907*t1 + 3245*Power(t1,2) + 
                     390*Power(t1,3)) + 
                  Power(s2,3)*
                   (17665 - 48891*t1 + 32246*Power(t1,2) + 
                     6490*Power(t1,3) + 680*Power(t1,4)) - 
                  Power(s2,2)*
                   (-5529 + 43646*t1 - 67297*Power(t1,2) + 
                     30402*Power(t1,3) + 2990*Power(t1,4) + 
                     480*Power(t1,5)) + 
                  s2*(-3407 - 5479*t1 + 37190*Power(t1,2) - 
                     40338*Power(t1,3) + 12270*Power(t1,4) + 
                     1040*Power(t1,5) + 162*Power(t1,6)) - 
                  2*Power(s1,4)*
                   (5 + 165*Power(s2,3) + 58*t1 - 40*Power(t1,2) - 
                     110*Power(t1,3) - 2*Power(s2,2)*(17 + 220*t1) + 
                     s2*(-71 + 74*t1 + 385*Power(t1,2))) + 
                  2*Power(s1,3)*
                   (27 + 575*Power(s2,4) + 347*t1 - 
                     1165*Power(t1,2) + 430*Power(t1,3) + 
                     525*Power(t1,4) - Power(s2,3)*(47 + 2300*t1) + 
                     Power(s2,2)*
                      (-895 + 524*t1 + 3400*Power(t1,2)) - 
                     s2*(338 - 2083*t1 + 907*Power(t1,2) + 
                        2200*Power(t1,3))) + 
                  Power(s1,2)*
                   (-857 - 526*Power(s2,5) + 583*t1 + 
                     6830*Power(t1,2) - 9714*Power(t1,3) + 
                     1220*Power(t1,4) + 1276*Power(t1,5) + 
                     4*Power(s2,4)*(672 + 715*t1) + 
                     Power(s2,3)*
                      (5994 - 7778*t1 - 6800*Power(t1,2)) + 
                     Power(s2,2)*
                      (2907 - 19269*t1 + 8420*Power(t1,2) + 
                       8400*Power(t1,3)) - 
                     2*s2*(486 + 4593*t1 - 11453*Power(t1,2) + 
                        2275*Power(t1,3) + 2605*Power(t1,4))) + 
                  s1*(1556 + 452*Power(s2,6) - 2885*t1 - 
                     9331*Power(t1,2) + 22074*Power(t1,3) - 
                     11400*Power(t1,4) + 78*Power(t1,5) + 
                     222*Power(t1,6) - 
                     2*Power(s2,5)*(1574 + 1261*t1) + 
                     2*Power(s2,4)*
                      (-8842 + 8109*t1 + 2350*Power(t1,2)) - 
                     Power(s2,3)*
                      (21623 - 59673*t1 + 28812*Power(t1,2) + 
                        3820*Power(t1,3)) + 
                     Power(s2,2)*
                      (-5840 + 55855*t1 - 76005*Power(t1,2) + 
                        21628*Power(t1,3) + 1590*Power(t1,4)) + 
                     s2*(4019 + 12221*t1 - 56022*Power(t1,2) + 
                        45442*Power(t1,3) - 5964*Power(t1,4) - 
                        622*Power(t1,5)))) + 
               Power(s,2)*(548 - 4635*t1 + 9912*Power(t1,2) - 
                  5377*Power(t1,3) - 3412*Power(t1,4) + 
                  3140*Power(t1,5) + 222*Power(t1,6) - 
                  428*Power(t1,7) + Power(s2,7)*(-37 + 208*t1) - 
                  3*Power(s2,6)*(419 + 320*t1 + 319*Power(t1,2)) + 
                  2*Power(s2,5)*
                   (-2841 + 3682*t1 + 2239*Power(t1,2) + 
                     975*Power(t1,3)) - 
                  Power(s2,4)*
                   (8253 - 25433*t1 + 17970*Power(t1,2) + 
                     7680*Power(t1,3) + 2390*Power(t1,4)) + 
                  Power(s2,3)*
                   (81 + 22837*t1 - 45482*Power(t1,2) + 
                     22212*Power(t1,3) + 6755*Power(t1,4) + 
                     1920*Power(t1,5)) - 
                  Power(s2,2)*
                   (-8859 + 9862*t1 + 24604*Power(t1,2) - 
                     40611*Power(t1,3) + 13463*Power(t1,4) + 
                     3788*Power(t1,5) + 933*Power(t1,6)) + 
                  s2*(5123 - 19817*t1 + 15024*Power(t1,2) + 
                     13448*Power(t1,3) - 18020*Power(t1,4) + 
                     2892*Power(t1,5) + 1660*Power(t1,6) + 
                     202*Power(t1,7)) + 
                  Power(s1,4)*
                   (16 + 275*Power(s2,4) - 30*t1 - 174*Power(t1,2) + 
                     80*Power(t1,3) + 165*Power(t1,4) - 
                     2*Power(s2,3)*(31 + 495*t1) + 
                     12*Power(s2,2)*
                      (-21 + 17*t1 + 110*Power(t1,2)) + 
                     s2*(14 + 426*t1 - 222*Power(t1,2) - 
                        770*Power(t1,3))) + 
                  Power(s1,3)*
                   (-90 - 777*Power(s2,5) + 198*t1 + 
                     1098*Power(t1,2) - 2530*Power(t1,3) + 
                     715*Power(t1,4) + 672*Power(t1,5) + 
                     Power(s2,4)*(-59 + 3880*t1) - 
                     2*Power(s2,3)*
                      (-881 + 269*t1 + 3825*Power(t1,2)) + 
                     8*Power(s2,2)*
                      (133 - 774*t1 + 246*Power(t1,2) + 
                       930*Power(t1,3)) - 
                     s2*(120 + 2214*t1 - 6960*Power(t1,2) + 
                        2086*Power(t1,3) + 3565*Power(t1,4))) + 
                  Power(s1,2)*
                   (816 + 275*Power(s2,6) - 3347*t1 + 
                     2041*Power(t1,2) + 6310*Power(t1,3) - 
                     7361*Power(t1,4) + 604*Power(t1,5) + 
                     789*Power(t1,6) - 
                     7*Power(s2,5)*(243 + 262*t1) + 
                     Power(s2,4)*
                      (-3575 + 5964*t1 + 5475*Power(t1,2)) - 
                     Power(s2,3)*
                      (491 - 14615*t1 + 7852*Power(t1,2) + 
                       8960*Power(t1,3)) + 
                     Power(s2,2)*
                      (3136 + 5689*t1 - 25617*Power(t1,2) + 
                       5220*Power(t1,3) + 8245*Power(t1,4)) - 
                     s2*(-2775 + 5151*t1 + 11522*Power(t1,2) - 
                        21938*Power(t1,3) + 2235*Power(t1,4) + 
                        3990*Power(t1,5))) - 
                  s1*(1130 + 171*Power(s2,7) - 6432*t1 + 
                     8995*Power(t1,2) + 3335*Power(t1,3) - 
                     12454*Power(t1,4) + 4784*Power(t1,5) + 
                     810*Power(t1,6) - 226*Power(t1,7) - 
                     Power(s2,6)*(1511 + 1388*t1) + 
                     Power(s2,5)*
                      (-9140 + 9182*t1 + 3756*Power(t1,2)) - 
                     Power(s2,4)*
                      (10237 - 35877*t1 + 19640*Power(t1,2) + 
                        4900*Power(t1,3)) + 
                     Power(s2,3)*
                      (2982 + 28202*t1 - 54842*Power(t1,2) + 
                        18110*Power(t1,3) + 3705*Power(t1,4)) - 
                     Power(s2,2)*
                      (-12167 + 11607*t1 + 37281*Power(t1,2) - 
                        43475*Power(t1,3) + 5663*Power(t1,4) + 
                        2046*Power(t1,5)) + 
                     2*s2*(3413 - 11293*t1 + 2619*Power(t1,2) + 
                        15893*Power(t1,3) - 10077*Power(t1,4) - 
                        644*Power(t1,5) + 464*Power(t1,6)))) + 
               s*(-120 + Power(s2,8)*(62 - 92*t1) + 1448*t1 - 
                  5989*Power(t1,2) + 9770*Power(t1,3) - 
                  6289*Power(t1,4) + 684*Power(t1,5) + 
                  318*Power(t1,6) + 370*Power(t1,7) - 
                  200*Power(t1,8) + 
                  Power(s2,7)*(271 + 89*t1 + 536*Power(t1,2)) - 
                  Power(s2,6)*
                   (-959 + 1493*t1 + 1529*Power(t1,2) + 
                     1398*Power(t1,3)) + 
                  Power(s2,5)*
                   (829 - 4337*t1 + 3604*Power(t1,2) + 
                     4180*Power(t1,3) + 2170*Power(t1,4)) - 
                  Power(s2,4)*
                   (3495 - 1690*t1 - 8039*Power(t1,2) + 
                     4628*Power(t1,3) + 5580*Power(t1,4) + 
                     2200*Power(t1,5)) + 
                  Power(s2,3)*
                   (-8797 + 21477*t1 - 10471*Power(t1,2) - 
                     8081*Power(t1,3) + 2755*Power(t1,4) + 
                     4513*Power(t1,5) + 1452*Power(t1,6)) + 
                  Power(s2,2)*
                   (-6709 + 29400*t1 - 38458*Power(t1,2) + 
                     13208*Power(t1,3) + 4916*Power(t1,4) + 
                     167*Power(t1,5) - 2529*Power(t1,6) - 
                     566*Power(t1,7)) + 
                  s2*(-1608 + 12930*t1 - 30325*Power(t1,2) + 
                     26765*Power(t1,3) - 5940*Power(t1,4) - 
                     1814*Power(t1,5) - 1046*Power(t1,6) + 
                     994*Power(t1,7) + 98*Power(t1,8)) + 
                  Power(s1,4)*
                   (-121*Power(s2,5) + Power(s2,4)*(28 + 550*t1) - 
                     2*Power(s2,3)*(-97 + 62*t1 + 495*Power(t1,2)) + 
                     Power(s2,2)*
                      (2 - 504*t1 + 204*Power(t1,2) + 
                       880*Power(t1,3)) + 
                     s2*(-32 + 28*t1 + 426*Power(t1,2) - 
                        148*Power(t1,3) - 385*Power(t1,4)) + 
                     2*t1*(16 - 15*t1 - 58*Power(t1,2) + 
                        20*Power(t1,3) + 33*Power(t1,4))) + 
                  Power(s1,3)*
                   (16 + 292*Power(s2,6) + 
                     Power(s2,5)*(77 - 1748*t1) - 196*t1 + 
                     234*Power(t1,2) + 770*Power(t1,3) - 
                     1365*Power(t1,4) + 314*Power(t1,5) + 
                     238*Power(t1,6) + 
                     Power(s2,4)*(-881 + 6*t1 + 4310*Power(t1,2)) - 
                     2*Power(s2,3)*
                      (378 - 2073*t1 + 397*Power(t1,2) + 
                        2800*Power(t1,3)) + 
                     2*Power(s2,2)*
                      (39 + 1193*t1 - 3507*Power(t1,2) + 
                        788*Power(t1,3) + 2020*Power(t1,4)) - 
                     s2*(-164 + 296*t1 + 2400*Power(t1,2) - 
                        5114*Power(t1,3) + 1179*Power(t1,4) + 
                        1532*Power(t1,5))) + 
                  Power(s1,2)*
                   (-328 - 85*Power(s2,7) + 1976*t1 - 
                     4139*Power(t1,2) + 2149*Power(t1,3) + 
                     2940*Power(t1,4) - 3019*Power(t1,5) + 
                     162*Power(t1,6) + 274*Power(t1,7) + 
                     26*Power(s2,6)*(23 + 26*t1) + 
                     Power(s2,5)*
                      (1106 - 2461*t1 - 2429*Power(t1,2)) + 
                     Power(s2,4)*
                      (-885 - 5248*t1 + 3734*Power(t1,2) + 
                        4930*Power(t1,3)) + 
                     Power(s2,3)*
                      (-3172 + 1268*t1 + 11878*Power(t1,2) - 
                        2578*Power(t1,3) - 5975*Power(t1,4)) + 
                     Power(s2,2)*
                      (-2995 + 8457*t1 + 3087*Power(t1,2) - 
                        15455*Power(t1,3) + 970*Power(t1,4) + 
                        4280*Power(t1,5)) - 
                     s2*(1480 - 6934*t1 + 7402*Power(t1,2) + 
                        6410*Power(t1,3) - 10738*Power(t1,4) + 
                        425*Power(t1,5) + 1671*Power(t1,6))) + 
                  s1*(432 + 30*Power(s2,8) - 3004*t1 + 
                     8508*Power(t1,2) - 9511*Power(t1,3) + 
                     1565*Power(t1,4) + 3272*Power(t1,5) - 
                     846*Power(t1,6) - 528*Power(t1,7) + 
                     102*Power(t1,8) - Power(s2,7)*(405 + 406*t1) + 
                     Power(s2,6)*
                      (-2462 + 2798*t1 + 1559*Power(t1,2)) - 
                     Power(s2,5)*
                      (1362 - 10139*t1 + 6735*Power(t1,2) + 
                        2912*Power(t1,3)) + 
                     Power(s2,4)*
                      (6458 - 1751*t1 - 14827*Power(t1,2) + 
                        6520*Power(t1,3) + 3230*Power(t1,4)) + 
                     Power(s2,3)*
                      (12485 - 30077*t1 + 11033*Power(t1,2) + 
                        9191*Power(t1,3) - 527*Power(t1,4) - 
                        2450*Power(t1,5)) + 
                     Power(s2,2)*
                      (8912 - 37225*t1 + 42209*Power(t1,2) - 
                        8061*Power(t1,3) - 2993*Power(t1,4) - 
                        3654*Power(t1,5) + 1391*Power(t1,6)) + 
                     s2*(2700 - 17468*t1 + 34171*Power(t1,2) - 
                        20155*Power(t1,3) - 3131*Power(t1,4) + 
                        1798*Power(t1,5) + 2531*Power(t1,6) - 
                        544*Power(t1,7)))))*Power(s1 - s2 + t1 - t2,2) + 
            (36 + 6*Power(s,7)*(-1 + s1) - 477*s2 - 100*Power(s2,2) + 
               201*Power(s2,3) - 392*Power(s2,4) - 74*Power(s2,5) - 
               58*Power(s2,6) + 233*t1 + 775*s2*t1 - 
               223*Power(s2,2)*t1 + 966*Power(s2,3)*t1 + 
               416*Power(s2,4)*t1 + 113*Power(s2,5)*t1 - 
               48*Power(s2,6)*t1 + 12*Power(s2,7)*t1 - 
               455*Power(t1,2) - 248*s2*Power(t1,2) - 
               894*Power(s2,2)*Power(t1,2) - 
               764*Power(s2,3)*Power(t1,2) + 
               75*Power(s2,4)*Power(t1,2) + 
               257*Power(s2,5)*Power(t1,2) - 
               76*Power(s2,6)*Power(t1,2) + 258*Power(t1,3) + 
               503*s2*Power(t1,3) + 645*Power(s2,2)*Power(t1,3) - 
               374*Power(s2,3)*Power(t1,3) - 
               571*Power(s2,4)*Power(t1,3) + 
               204*Power(s2,5)*Power(t1,3) - 183*Power(t1,4) - 
               288*s2*Power(t1,4) + 415*Power(s2,2)*Power(t1,4) + 
               691*Power(s2,3)*Power(t1,4) - 
               296*Power(s2,4)*Power(t1,4) + 65*Power(t1,5) - 
               225*s2*Power(t1,5) - 497*Power(s2,2)*Power(t1,5) + 
               244*Power(s2,3)*Power(t1,5) + 54*Power(t1,6) + 
               208*s2*Power(t1,6) - 108*Power(s2,2)*Power(t1,6) - 
               40*Power(t1,7) + 20*s2*Power(t1,7) + 
               Power(s,6)*(23*Power(s1,2) - 60*(1 + t1) + 
                  s2*(44 + 7*t1) + s1*(44 - 51*s2 + 53*t1)) - 
               Power(s1,4)*(2*Power(s2,4) - Power(s2,3)*(19 + 7*t1) + 
                  Power(s2,2)*(17 + 48*t1 + 9*Power(t1,2)) + 
                  t1*(4 + 13*t1 + 10*Power(t1,2) + Power(t1,3)) - 
                  s2*(4 + 30*t1 + 39*Power(t1,2) + 5*Power(t1,3))) + 
               Power(s1,3)*(-18*Power(s2,5) + 
                  15*Power(s2,4)*(-3 + 5*t1) + 
                  Power(s2,3)*(45 + 175*t1 - 122*Power(t1,2)) + 
                  Power(s2,2)*
                   (64 - 155*t1 - 235*Power(t1,2) + 96*Power(t1,3)) + 
                  s2*(-2 - 76*t1 + 165*Power(t1,2) + 
                     125*Power(t1,3) - 36*Power(t1,4)) + 
                  t1*(2 + 12*t1 - 55*Power(t1,2) - 20*Power(t1,3) + 
                     5*Power(t1,4))) + 
               Power(s1,2)*(-36 + 36*Power(s2,6) + 189*t1 - 
                  185*Power(t1,2) + 176*Power(t1,3) - 
                  81*Power(t1,4) + 13*Power(t1,5) + 16*Power(t1,6) - 
                  2*Power(s2,5)*(9 + 100*t1) + 
                  Power(s2,4)*(-233 + 87*t1 + 454*Power(t1,2)) - 
                  Power(s2,3)*
                   (250 - 677*t1 + 161*Power(t1,2) + 538*Power(t1,3)) \
+ Power(s2,2)*(-102 + 633*t1 - 711*Power(t1,2) + 146*Power(t1,3) + 
                     350*Power(t1,4)) - 
                  s2*(217 - 355*t1 + 563*Power(t1,2) - 
                     348*Power(t1,3) + 67*Power(t1,4) + 
                     118*Power(t1,5))) + 
               s1*(-12*Power(s2,7) + 2*Power(s2,6)*(21 + 52*t1) - 
                  3*Power(s2,5)*(-88 + 59*t1 + 117*Power(t1,2)) + 
                  Power(s2,4)*
                   (592 - 1070*t1 + 287*Power(t1,2) + 
                     623*Power(t1,3)) - 
                  Power(s2,3)*
                   (89 + 1379*t1 - 1469*Power(t1,2) + 
                     174*Power(t1,3) + 643*Power(t1,4)) + 
                  Power(s2,2)*
                   (195 - 77*t1 + 1067*Power(t1,2) - 
                     777*Power(t1,3) - 51*Power(t1,4) + 
                     393*Power(t1,5)) + 
                  s2*(644 - 1020*t1 + 425*Power(t1,2) - 
                     387*Power(t1,3) + 103*Power(t1,4) + 
                     109*Power(t1,5) - 134*Power(t1,6)) + 
                  t1*(-372 + 537*t1 - 243*Power(t1,2) + 
                     107*Power(t1,3) + 11*Power(t1,4) - 
                     36*Power(t1,5) + 20*Power(t1,6))) + 
               Power(s,5)*(381 + 12*Power(s1,3) - 355*t1 - 
                  246*Power(t1,2) - Power(s2,2)*(129 + 47*t1) + 
                  Power(s1,2)*(49 - 148*s2 + 138*t1) + 
                  s2*(318 + 387*t1 + 50*Power(t1,2)) + 
                  s1*(-423 + 176*Power(s2,2) + 248*t1 + 
                     196*Power(t1,2) - s2*(236 + 371*t1))) - 
               Power(s,4)*(570 + Power(s1,4) + 
                  Power(s1,3)*(15 + 66*s2 - 53*t1) - 1776*t1 + 
                  782*Power(t1,2) + 544*Power(t1,3) - 
                  2*Power(s2,3)*(98 + 65*t1) + 
                  Power(s2,2)*(713 + 1005*t1 + 276*Power(t1,2)) - 
                  s2*(-1633 + 1437*t1 + 1327*Power(t1,2) + 
                     150*Power(t1,3)) + 
                  Power(s1,2)*
                   (133 - 399*Power(s2,2) - 195*t1 - 
                     338*Power(t1,2) + s2*(250 + 741*t1)) + 
                  s1*(-612 + 326*Power(s2,3) + 1830*t1 - 
                     507*Power(t1,2) - 394*Power(t1,3) - 
                     11*Power(s2,2)*(49 + 96*t1) + 
                     s2*(-1961 + 1071*t1 + 1114*Power(t1,2)))) + 
               Power(s,3)*(19 + Power(s1,4)*(-10 + 5*s2 - 4*t1) - 
                  1856*t1 + 3115*Power(t1,2) - 762*Power(t1,3) - 
                  706*Power(t1,4) - 2*Power(s2,4)*(82 + 95*t1) + 
                  Power(s1,3)*
                   (-55 + 144*Power(s2,2) + s2*(90 - 234*t1) - 
                     65*t1 + 92*Power(t1,2)) + 
                  Power(s2,3)*(889 + 1341*t1 + 604*Power(t1,2)) - 
                  Power(s2,2)*
                   (-2702 + 2271*t1 + 2795*Power(t1,2) + 
                     654*Power(t1,3)) + 
                  s2*(2034 - 5924*t1 + 2258*Power(t1,2) + 
                     2307*Power(t1,3) + 240*Power(t1,4)) + 
                  Power(s1,2)*
                   (168 - 575*Power(s2,3) - 521*t1 + 
                     304*Power(t1,2) + 432*Power(t1,3) + 
                     Power(s2,2)*(483 + 1598*t1) + 
                     s2*(606 - 798*t1 - 1453*Power(t1,2))) + 
                  s1*(-67 + 354*Power(s2,4) + 1982*t1 - 
                     2969*Power(t1,2) + 437*Power(t1,3) + 
                     466*Power(t1,4) - Power(s2,3)*(683 + 1576*t1) + 
                     Power(s2,2)*
                      (-3630 + 1906*t1 + 2517*Power(t1,2)) - 
                     s2*(2310 - 6612*t1 + 1673*Power(t1,2) + 
                        1760*Power(t1,3)))) + 
               Power(s,2)*(17 + 77*t1 - 2177*Power(t1,2) + 
                  2491*Power(t1,3) - 244*Power(t1,4) - 
                  540*Power(t1,5) + Power(s2,5)*(72 + 155*t1) - 
                  Power(s2,4)*(669 + 963*t1 + 656*Power(t1,2)) + 
                  3*Power(s2,3)*
                   (-701 + 588*t1 + 950*Power(t1,2) + 
                     354*Power(t1,3)) - 
                  Power(s2,2)*
                   (2723 - 6947*t1 + 2100*Power(t1,2) + 
                     3588*Power(t1,3) + 776*Power(t1,4)) + 
                  s2*(122 + 4667*t1 - 7269*Power(t1,2) + 
                     1252*Power(t1,3) + 2169*Power(t1,4) + 
                     215*Power(t1,5)) - 
                  Power(s1,4)*
                   (13 + 9*Power(s2,2) + 30*t1 + 6*Power(t1,2) - 
                     3*s2*(13 + 5*t1)) + 
                  Power(s1,3)*
                   (-156*Power(s2,3) + 
                     12*Power(s2,2)*(-15 + 32*t1) + 
                     s2*(155 + 305*t1 - 306*Power(t1,2)) + 
                     3*(6 - 55*t1 - 35*Power(t1,2) + 26*Power(t1,3))) \
+ Power(s1,2)*(-76 + 466*Power(s2,4) + 524*t1 - 724*Power(t1,2) + 
                     232*Power(t1,3) + 303*Power(t1,4) - 
                     5*Power(s2,3)*(86 + 345*t1) + 
                     Power(s2,2)*
                      (-1042 + 1116*t1 + 2349*Power(t1,2)) - 
                     s2*(545 - 1634*t1 + 913*Power(t1,2) + 
                        1393*Power(t1,3))) + 
                  s1*(72 - 227*Power(s2,5) - 262*t1 + 
                     2245*Power(t1,2) - 2129*Power(t1,3) + 
                     89*Power(t1,4) + 325*Power(t1,5) + 
                     Power(s2,4)*(515 + 1307*t1) + 
                     Power(s2,3)*
                      (3333 - 1768*t1 - 2827*Power(t1,2)) + 
                     Power(s2,2)*
                      (3329 - 8811*t1 + 2141*Power(t1,2) + 
                        2963*Power(t1,3)) + 
                     s2*(35 - 5171*t1 + 7498*Power(t1,2) - 
                        968*Power(t1,3) - 1541*Power(t1,4)))) + 
               s*(233 - 402*t1 + 316*Power(t1,2) - 1074*Power(t1,3) + 
                  836*Power(t1,4) + 85*Power(t1,5) - 226*Power(t1,6) - 
                  Power(s2,6)*(13 + 67*t1) + 
                  Power(s2,5)*(293 + 348*t1 + 354*Power(t1,2)) - 
                  Power(s2,4)*
                   (-727 + 688*t1 + 1393*Power(t1,2) + 
                     762*Power(t1,3)) + 
                  Power(s2,3)*
                   (1651 - 3215*t1 + 549*Power(t1,2) + 
                     2396*Power(t1,3) + 832*Power(t1,4)) - 
                  Power(s2,2)*
                   (338 + 3754*t1 - 4914*Power(t1,2) + 
                     127*Power(t1,3) + 2166*Power(t1,4) + 
                     459*Power(t1,5)) + 
                  s2*(31 + 157*t1 + 3128*Power(t1,2) - 
                     3266*Power(t1,3) - 112*Power(t1,4) + 
                     1054*Power(t1,5) + 102*Power(t1,6)) + 
                  Power(s1,3)*
                   (2 + 84*Power(s2,4) + Power(s2,3)*(150 - 278*t1) + 
                     30*t1 - 165*Power(t1,2) - 75*Power(t1,3) + 
                     32*Power(t1,4) + 
                     Power(s2,2)*(-145 - 415*t1 + 336*Power(t1,2)) + 
                     s2*(-82 + 320*t1 + 340*Power(t1,2) - 
                        174*Power(t1,3))) + 
                  Power(s1,4)*
                   (7*Power(s2,3) - 6*Power(s2,2)*(8 + 3*t1) + 
                     3*s2*(10 + 26*t1 + 5*Power(t1,2)) - 
                     2*(2 + 13*t1 + 15*Power(t1,2) + 2*Power(t1,3))) + 
                  Power(s1,2)*
                   (153 - 201*Power(s2,5) - 261*t1 + 
                     532*Power(t1,2) - 417*Power(t1,3) + 
                     87*Power(t1,4) + 110*Power(t1,5) + 
                     2*Power(s2,4)*(83 + 465*t1) - 
                     2*Power(s2,3)*
                      (-401 + 300*t1 + 844*Power(t1,2)) + 
                     Power(s2,2)*
                      (627 - 1786*t1 + 779*Power(t1,2) + 
                        1500*Power(t1,3)) + 
                     s2*(182 - 1120*t1 + 1376*Power(t1,2) - 
                        432*Power(t1,3) - 651*Power(t1,4))) + 
                  s1*(-336 + 80*Power(s2,6) + 573*t1 - 
                     438*Power(t1,2) + 982*Power(t1,3) - 
                     556*Power(t1,4) - 81*Power(t1,5) + 
                     124*Power(t1,6) - Power(s2,5)*(221 + 573*t1) + 
                     Power(s2,4)*(-1505 + 862*t1 + 1579*Power(t1,2)) - 
                     Power(s2,3)*
                      (2223 - 5099*t1 + 1262*Power(t1,2) + 
                        2220*Power(t1,3)) + 
                     Power(s2,2)*
                      (117 + 4525*t1 - 5984*Power(t1,2) + 
                        723*Power(t1,3) + 1719*Power(t1,4)) - 
                     s2*(219 - 281*t1 + 3258*Power(t1,2) - 
                        2950*Power(t1,3) + 21*Power(t1,4) + 
                        709*Power(t1,5)))))*Power(s1 - s2 + t1 - t2,4) + 
            (-56 + 2*Power(s,5)*(-1 + s1) + 21*s2 - 44*Power(s2,2) + 
               16*Power(s2,3) - 14*Power(s2,4) + 5*Power(s2,5) - 
               Power(s,4)*(10 + Power(s1,2) + s1*(-8 + 9*s2 - 3*t1) + 
                  3*s2*(-4 + t1)) + 55*t1 + 22*s2*t1 - 
               25*Power(s2,2)*t1 + 23*Power(s2,3)*t1 + 
               3*Power(s2,4)*t1 - 4*Power(s2,5)*t1 - 2*Power(t1,2) + 
               38*s2*Power(t1,2) - 2*Power(s2,2)*Power(t1,2) - 
               50*Power(s2,3)*Power(t1,2) + 
               17*Power(s2,4)*Power(t1,2) - 25*Power(t1,3) - 
               19*s2*Power(t1,3) + 84*Power(s2,2)*Power(t1,3) - 
               28*Power(s2,3)*Power(t1,3) + 12*Power(t1,4) - 
               54*s2*Power(t1,4) + 21*Power(s2,2)*Power(t1,4) + 
               12*Power(t1,5) - 6*s2*Power(t1,5) + 
               Power(s1,3)*(3*Power(s2,3) - 7*Power(s2,2)*(1 + t1) - 
                  t1*(4 + 5*t1 + Power(t1,2)) + 
                  s2*(4 + 12*t1 + 5*Power(t1,2))) + 
               Power(s1,2)*(-8 - 4*Power(s2,4) + 6*t1 - 
                  18*Power(t1,2) - 15*Power(t1,3) - 3*Power(t1,4) + 
                  Power(s2,3)*(26 + 17*t1) - 
                  Power(s2,2)*(32 + 69*t1 + 25*Power(t1,2)) + 
                  s2*(2 + 50*t1 + 58*Power(t1,2) + 15*Power(t1,3))) - 
               s1*(-64 + Power(s2,5) + 55*t1 - 20*Power(t1,2) - 
                  29*Power(t1,3) + 6*Power(t1,5) + 
                  Power(s2,4)*(-2 + 4*t1) + 
                  Power(s2,3)*(27 + 3*t1 - 23*Power(t1,2)) + 
                  Power(s2,2)*
                   (-79 - 58*t1 - 5*Power(t1,2) + 36*Power(t1,3)) + 
                  s2*(29 + 75*t1 + 64*Power(t1,2) + 4*Power(t1,3) - 
                     24*Power(t1,4))) - 
               Power(s,3)*(-1 + 2*Power(s1,3) + 
                  Power(s2,2)*(29 - 13*t1) + 11*t1 - 24*Power(t1,2) + 
                  Power(s1,2)*(17 - 6*s2 + 8*t1) + 
                  s2*(-46 + t1 + 14*Power(t1,2)) + 
                  s1*(-9 - 16*Power(s2,2) - 14*t1 + 10*Power(t1,2) + 
                     s2*(30 + 7*t1))) + 
               Power(s,2)*(-8 + Power(s1,3)*(-6 + 7*s2 - 5*t1) - 
                  4*t1 + 20*Power(t1,2) + 52*Power(t1,3) - 
                  7*Power(s2,3)*(-5 + 3*t1) + 
                  Power(s2,2)*(-76 + 5*t1 + 45*Power(t1,2)) + 
                  s2*(13 + 57*t1 - 91*Power(t1,2) - 25*Power(t1,3)) - 
                  Power(s1,2)*
                   (26 + 13*Power(s2,2) + 51*t1 + 16*Power(t1,2) - 
                     s2*(59 + 31*t1)) + 
                  s1*(36 - 14*Power(s2,3) + 32*t1 + 3*Power(t1,2) - 
                     27*Power(t1,3) + Power(s2,2)*(38 + t1) + 
                     s2*(-45 - 45*t1 + 39*Power(t1,2)))) - 
               s*(9 + 2*t1 + 30*Power(t1,2) - 33*Power(t1,3) - 
                  42*Power(t1,4) - 3*Power(s2,4)*(-7 + 5*t1) + 
                  Power(s2,3)*(-54 + 7*t1 + 48*Power(t1,2)) + 
                  Power(s2,2)*
                   (30 + 69*t1 - 117*Power(t1,2) - 53*Power(t1,3)) + 
                  4*s2*(-12 - 8*t1 + 2*Power(t1,2) + 33*Power(t1,3) + 
                     5*Power(t1,4)) + 
                  Power(s1,3)*
                   (4 + 8*Power(s2,2) + 11*t1 + 4*Power(t1,2) - 
                     s2*(13 + 12*t1)) + 
                  Power(s1,2)*
                   (2 - 12*Power(s2,3) + 44*t1 + 49*Power(t1,2) + 
                     12*Power(t1,3) + Power(s2,2)*(68 + 40*t1) - 
                     s2*(58 + 119*t1 + 40*Power(t1,2))) + 
                  s1*(-17 - 6*Power(s2,4) + Power(s2,3)*(18 - 7*t1) - 
                     48*t1 - 52*Power(t1,2) + 3*Power(t1,3) + 
                     22*Power(t1,4) + 
                     Power(s2,2)*(-63 - 34*t1 + 52*Power(t1,2)) + 
                     s2*(111 + 94*t1 + 18*Power(t1,2) - 61*Power(t1,3))\
)))*Power(s1 - s2 + t1 - t2,6) - 
            2*(-1 + s1)*(s + Power(s,2) - s2 - 2*s*s2 + Power(s2,2) + 
               t1 + 2*s*t1 - 2*s2*t1 + Power(t1,2))*
             Power(s1 - s2 + t1 - t2,8) - 
            2*(Power(s,2) + s2 + Power(s2,2) - 2*s2*t1 + 
               (-1 + t1)*t1 + s*(-1 - 2*s2 + 2*t1))*
             (-184 + 173*s2 + 1204*Power(s2,2) + 1123*Power(s2,3) + 
               453*Power(s2,4) + 243*Power(s2,5) + 26*Power(s2,6) - 
               46*Power(s2,7) + 315*t1 - 1871*s2*t1 - 
               4098*Power(s2,2)*t1 - 2844*Power(s2,3)*t1 - 
               1634*Power(s2,4)*t1 - 369*Power(s2,5)*t1 + 
               258*Power(s2,6)*t1 - 7*Power(s2,7)*t1 + 
               2*Power(s2,8)*t1 + 371*Power(t1,2) + 
               4122*s2*Power(t1,2) + 5156*Power(s2,2)*Power(t1,2) + 
               3625*Power(s2,3)*Power(t1,2) + 
               1252*Power(s2,4)*Power(t1,2) - 
               585*Power(s2,5)*Power(t1,2) + 
               24*Power(s2,6)*Power(t1,2) - 
               9*Power(s2,7)*Power(t1,2) - 1155*Power(t1,3) - 
               3665*s2*Power(t1,3) - 3626*Power(s2,2)*Power(t1,3) - 
               1875*Power(s2,3)*Power(t1,3) + 
               659*Power(s2,4)*Power(t1,3) - 
               17*Power(s2,5)*Power(t1,3) + 
               17*Power(s2,6)*Power(t1,3) + 900*Power(t1,4) + 
               1702*s2*Power(t1,4) + 1425*Power(s2,2)*Power(t1,4) - 
               331*Power(s2,3)*Power(t1,4) - 
               28*Power(s2,4)*Power(t1,4) - 
               20*Power(s2,5)*Power(t1,4) - 310*Power(t1,5) - 
               544*s2*Power(t1,5) - 15*Power(s2,2)*Power(t1,5) + 
               47*Power(s2,3)*Power(t1,5) + 
               20*Power(s2,4)*Power(t1,5) + 85*Power(t1,6) + 
               86*s2*Power(t1,6) - 16*Power(s2,2)*Power(t1,6) - 
               17*Power(s2,3)*Power(t1,6) - 26*Power(t1,7) - 
               7*s2*Power(t1,7) + 9*Power(s2,2)*Power(t1,7) + 
               4*Power(t1,8) - 2*s2*Power(t1,8) + 
               2*Power(s,7)*(6 + s2*(-2 + t1) - 2*t1 + 
                  s1*(-5 + s2 + t1)) + 
               Power(s1,4)*(8*Power(s2,5) - 
                  2*Power(s2,4)*(7 + 18*t1) + 
                  Power(s2,3)*(-19 + 51*t1 + 64*Power(t1,2)) + 
                  Power(s2,2)*
                   (4 + 45*t1 - 69*Power(t1,2) - 56*Power(t1,3)) + 
                  t1*(-4 + 8*t1 + 7*Power(t1,2) - 9*Power(t1,3) - 
                     4*Power(t1,4)) + 
                  s2*(4 - 12*t1 - 33*Power(t1,2) + 41*Power(t1,3) + 
                     24*Power(t1,4))) - 
               Power(s1,3)*(9*Power(s2,6) + 
                  Power(s2,5)*(25 - 57*t1) + 
                  Power(s2,4)*(38 - 79*t1 + 148*Power(t1,2)) + 
                  Power(s2,3)*
                   (80 + 4*t1 + 60*Power(t1,2) - 202*Power(t1,3)) + 
                  Power(s2,2)*
                   (112 - 148*t1 - 220*Power(t1,2) + 
                     44*Power(t1,3) + 153*Power(t1,4)) + 
                  s2*(56 - 240*t1 + 80*Power(t1,2) + 
                     276*Power(t1,3) - 77*Power(t1,4) - 
                     61*Power(t1,5)) + 
                  t1*(-56 + 128*t1 - 12*Power(t1,2) - 
                     98*Power(t1,3) + 27*Power(t1,4) + 10*Power(t1,5))\
) + Power(s1,2)*(-56 - 4*Power(s2,7) - 93*t1 + 533*Power(t1,2) - 
                  407*Power(t1,3) - 113*Power(t1,4) + 
                  148*Power(t1,5) + Power(t1,6) - 8*Power(t1,7) + 
                  Power(s2,6)*(-13 + 23*t1) + 
                  Power(s2,5)*(133 - 16*t1 - 45*Power(t1,2)) + 
                  Power(s2,4)*
                   (463 - 790*t1 + 224*Power(t1,2) + 22*Power(t1,3)) \
+ Power(s2,3)*(837 - 1867*t1 + 1441*Power(t1,2) - 447*Power(t1,3) + 
                     42*Power(t1,4)) + 
                  Power(s2,2)*
                   (906 - 2522*t1 + 2215*Power(t1,2) - 
                     896*Power(t1,3) + 352*Power(t1,4) - 
                     69*Power(t1,5)) + 
                  s2*(325 - 1607*t1 + 2084*Power(t1,2) - 
                     698*Power(t1,3) - 36*Power(t1,4) - 
                     101*Power(t1,5) + 39*Power(t1,6))) + 
               s1*(240 - 2*Power(s2,8) - 338*t1 - 436*Power(t1,2) + 
                  844*Power(t1,3) - 231*Power(t1,4) - 
                  76*Power(t1,5) - 32*Power(t1,6) + 27*Power(t1,7) - 
                  2*Power(t1,8) + Power(s2,7)*(47 + 19*t1) + 
                  Power(s2,6)*(30 - 300*t1 - 56*Power(t1,2)) + 
                  Power(s2,5)*
                   (-399 + 267*t1 + 697*Power(t1,2) + 71*Power(t1,3)) \
- Power(s2,4)*(1048 - 2905*t1 + 1599*Power(t1,2) + 706*Power(t1,3) + 
                     36*Power(t1,4)) + 
                  Power(s2,3)*
                   (-1886 + 5231*t1 - 6353*Power(t1,2) + 
                     2915*Power(t1,3) + 204*Power(t1,4) + Power(t1,5)\
) + Power(s2,2)*(-1846 + 5970*t1 - 7523*Power(t1,2) + 
                     5507*Power(t1,3) - 2283*Power(t1,4) + 
                     167*Power(t1,5)) + 
                  s2*(-382 + 2746*t1 - 4912*Power(t1,2) + 
                     3571*Power(t1,3) - 1584*Power(t1,4) + 
                     702*Power(t1,5) - 136*Power(t1,6) + 5*Power(t1,7)\
)) + Power(s,6)*(-95 - 3*Power(s1,3) + Power(s2,2)*(34 - 16*t1) + 
                  119*t1 - 34*Power(t1,2) + 
                  Power(s1,2)*(-26 + 5*s2 + 2*t1) + 
                  s2*(-106 - 12*t1 + 17*Power(t1,2)) + 
                  s1*(121 - 18*Power(s2,2) - 101*t1 + 
                     17*Power(t1,2) + 8*s2*(11 + t1))) + 
               Power(s,5)*(293 - 4*Power(s1,4) + 
                  Power(s1,3)*(18*s2 - 25*t1) - 534*t1 + 
                  369*Power(t1,2) - 106*Power(t1,3) + 
                  2*Power(s2,3)*(-55 + 24*t1) + 
                  Power(s2,2)*(420 + 150*t1 - 106*Power(t1,2)) + 
                  Power(s1,2)*
                   (119 - 17*Power(s2,2) + s2*(181 - 4*t1) - 94*t1 + 
                     2*Power(t1,2)) + 
                  s2*(617 - 807*t1 + 80*Power(t1,2) + 
                     53*Power(t1,3)) + 
                  s1*(-353 + 62*Power(s2,3) + 591*t1 - 
                     311*Power(t1,2) + 53*Power(t1,3) - 
                     Power(s2,2)*(352 + 95*t1) + 
                     s2*(-874 + 770*t1 - 34*Power(t1,2)))) + 
               Power(s,4)*(-319 + Power(s2,4)*(180 - 70*t1) + 
                  Power(s1,4)*(-9 + 24*s2 - 20*t1) + 1251*t1 - 
                  1026*Power(t1,2) + 500*Power(t1,3) - 
                  160*Power(t1,4) - 
                  Power(s1,3)*
                   (-48 + 51*Power(s2,2) + s2*(39 - 133*t1) + 
                     27*t1 + 80*Power(t1,2)) + 
                  Power(s2,3)*(-906 - 407*t1 + 245*Power(t1,2)) + 
                  Power(s2,2)*
                   (-1497 + 2408*t1 + 36*Power(t1,2) - 
                     255*Power(t1,3)) + 
                  s2*(-1462 + 2511*t1 - 2033*Power(t1,2) + 
                     353*Power(t1,3) + 80*Power(t1,4)) + 
                  Power(s1,2)*
                   (-202 + 14*Power(s2,3) + 539*t1 - 
                     115*Power(t1,2) - 20*Power(t1,3) + 
                     Power(s2,2)*(-472 + 35*t1) + 
                     s2*(-386 + 478*t1 - 27*Power(t1,2))) + 
                  s1*(360 - 110*Power(s2,4) - 1473*t1 + 
                     1080*Power(t1,2) - 407*Power(t1,3) + 
                     80*Power(t1,4) + 31*Power(s2,3)*(25 + 9*t1) + 
                     Power(s2,2)*(2273 - 2403*t1 - 74*Power(t1,2)) + 
                     s2*(1779 - 3234*t1 + 1994*Power(t1,2) - 
                        171*Power(t1,3)))) + 
               Power(s,3)*(-142 - 617*t1 + 1636*Power(t1,2) - 
                  739*Power(t1,3) + 270*Power(t1,4) - 
                  120*Power(t1,5) + 10*Power(s2,5)*(-16 + 5*t1) + 
                  Power(s2,4)*(1124 + 508*t1 - 260*Power(t1,2)) + 
                  Power(s2,3)*
                   (1713 - 3830*t1 - 272*Power(t1,2) + 
                     430*Power(t1,3)) + 
                  Power(s2,2)*
                   (2363 - 3969*t1 + 4662*Power(t1,2) - 
                     468*Power(t1,3) - 280*Power(t1,4)) + 
                  s2*(1132 - 3949*t1 + 3067*Power(t1,2) - 
                     2231*Power(t1,3) + 512*Power(t1,4) + 
                     60*Power(t1,5)) + 
                  Power(s1,4)*
                   (7 - 56*Power(s2,2) - 36*t1 - 40*Power(t1,2) + 
                     s2*(41 + 96*t1)) + 
                  2*Power(s1,3)*
                   (42*Power(s2,3) + Power(s2,2)*(71 - 153*t1) + 
                     t1*(121 - 54*t1 - 65*Power(t1,2)) + 
                     s2*(-37 - 20*t1 + 176*Power(t1,2))) + 
                  Power(s1,2)*
                   (-79 + 14*Power(s2,4) + 
                     Power(s2,3)*(603 - 124*t1) - 705*t1 + 
                     1051*Power(t1,2) - 40*Power(t1,3) - 
                     60*Power(t1,4) + 
                     Power(s2,2)*(299 - 839*t1 + 138*Power(t1,2)) + 
                     s2*(114 - 873*t1 + 247*Power(t1,2) + 
                        32*Power(t1,3))) + 
                  s1*(253 + 110*Power(s2,5) + 972*t1 - 
                     2359*Power(t1,2) + 872*Power(t1,3) - 
                     188*Power(t1,4) + 60*Power(t1,5) - 
                     198*Power(s2,4)*(5 + 2*t1) + 
                     Power(s2,3)*
                      (-2815 + 3899*t1 + 312*Power(t1,2)) + 
                     Power(s2,2)*
                      (-2757 + 5725*t1 - 4822*Power(t1,2) + 
                        168*Power(t1,3)) + 
                     s2*(-926 + 4192*t1 - 3604*Power(t1,2) + 
                        2106*Power(t1,3) - 254*Power(t1,4)))) + 
               Power(s,2)*(359 + Power(s2,6)*(74 - 12*t1) - 1246*t1 + 
                  633*Power(t1,2) + 381*Power(t1,3) + 
                  24*Power(t1,4) - 33*Power(t1,5) - 34*Power(t1,6) + 
                  Power(s2,5)*(-798 - 318*t1 + 115*Power(t1,2)) + 
                  Power(s2,4)*
                   (-902 + 3327*t1 + 310*Power(t1,2) - 
                     290*Power(t1,3)) + 
                  Power(s2,3)*
                   (-1269 + 2172*t1 - 5286*Power(t1,2) + 
                     284*Power(t1,3) + 300*Power(t1,4)) - 
                  Power(s2,2)*
                   (869 - 2486*t1 + 1807*Power(t1,2) - 
                     3765*Power(t1,3) + 630*Power(t1,4) + 
                     130*Power(t1,5)) + 
                  s2*(952 + 3*t1 - 1710*Power(t1,2) + 
                     525*Power(t1,3) - 975*Power(t1,4) + 
                     314*Power(t1,5) + 17*Power(t1,6)) + 
                  Power(s1,4)*
                   (8 + 64*Power(s2,3) + 21*t1 - 54*Power(t1,2) - 
                     40*Power(t1,3) - 3*Power(s2,2)*(23 + 56*t1) + 
                     3*s2*(-11 + 41*t1 + 48*Power(t1,2))) - 
                  Power(s1,3)*
                   (100 + 81*Power(s2,4) + 
                     Power(s2,3)*(192 - 370*t1) - 12*t1 - 
                     438*Power(t1,2) + 162*Power(t1,3) + 
                     115*Power(t1,4) + 
                     Power(s2,2)*(34 - 240*t1 + 612*Power(t1,2)) + 
                     s2*(92 + 424*t1 - 114*Power(t1,2) - 
                        438*Power(t1,3))) + 
                  Power(s1,2)*
                   (402 - 31*Power(s2,5) - 543*t1 - 
                     917*Power(t1,2) + 1109*Power(t1,3) + 
                     20*Power(t1,4) - 70*Power(t1,5) + 
                     Power(s2,4)*(-397 + 172*t1) + 
                     Power(s2,3)*(217 + 604*t1 - 248*Power(t1,2)) + 
                     Power(s2,2)*
                      (845 - 685*t1 + 90*Power(t1,2) + 
                       34*Power(t1,3)) + 
                     s2*(1008 - 540*t1 - 624*Power(t1,2) - 
                        317*Power(t1,3) + 143*Power(t1,4))) + 
                  s1*(-577 - 62*Power(s2,6) + 1319*t1 + 
                     631*Power(t1,2) - 1787*Power(t1,3) + 
                     231*Power(t1,4) + 73*Power(t1,5) + 
                     17*Power(t1,6) + Power(s2,5)*(730 + 302*t1) + 
                     Power(s2,4)*(1732 - 3464*t1 - 407*Power(t1,2)) + 
                     Power(s2,3)*
                      (1190 - 3845*t1 + 5603*Power(t1,2) + 
                        36*Power(t1,3)) + 
                     Power(s2,2)*
                      (-273 - 945*t1 + 2170*Power(t1,2) - 
                        3676*Power(t1,3) + 268*Power(t1,4)) + 
                     s2*(-1802 + 1039*t1 + 1417*Power(t1,2) - 
                        300*Power(t1,3) + 734*Power(t1,4) - 
                        154*Power(t1,5)))) - 
               s*(-75 - 786*t1 + 2259*Power(t1,2) - 1831*Power(t1,3) + 
                  607*Power(t1,4) - 261*Power(t1,5) + 91*Power(t1,6) - 
                  6*Power(t1,7) + 2*Power(s2,7)*(7 + 2*t1) + 
                  2*Power(s2,6)*(-150 - 45*t1 + Power(t1,2)) + 
                  Power(s2,5)*
                   (-138 + 1475*t1 + 144*Power(t1,2) - 45*Power(t1,3)) \
+ Power(s2,4)*(168 - 189*t1 - 2873*Power(t1,2) + 46*Power(t1,3) + 
                     80*Power(t1,4)) + 
                  Power(s2,3)*
                   (397 - 1846*t1 + 1486*Power(t1,2) + 
                     2693*Power(t1,3) - 306*Power(t1,4) - 
                     50*Power(t1,5)) + 
                  Power(s2,2)*
                   (1925 - 3435*t1 + 3554*Power(t1,2) - 
                     2090*Power(t1,3) - 1076*Power(t1,4) + 
                     258*Power(t1,5) + 6*Power(t1,6)) + 
                  s2*(1379 - 4705*t1 + 4804*Power(t1,2) - 
                     2479*Power(t1,3) + 1192*Power(t1,4) - 
                     10*Power(t1,5) - 60*Power(t1,6) + 3*Power(t1,7)) \
+ Power(s1,4)*(4 + 36*Power(s2,4) - 16*t1 - 21*Power(t1,2) + 
                     36*Power(t1,3) + 20*Power(t1,4) - 
                     Power(s2,3)*(51 + 128*t1) + 
                     3*Power(s2,2)*(-15 + 46*t1 + 56*Power(t1,2)) - 
                     3*s2*(-4 - 22*t1 + 41*Power(t1,2) + 
                        32*Power(t1,3))) - 
                  Power(s1,3)*
                   (56 + 42*Power(s2,5) + 
                     Power(s2,4)*(114 - 229*t1) - 228*t1 + 
                     24*Power(t1,2) + 342*Power(t1,3) - 
                     108*Power(t1,4) - 53*Power(t1,5) + 
                     Power(s2,3)*(98 - 252*t1 + 488*Power(t1,2)) + 
                     Power(s2,2)*
                      (172 + 186*t1 + 54*Power(t1,2) - 
                        510*Power(t1,3)) + 
                     2*s2*(106 - 86*t1 - 313*Power(t1,2) + 
                        96*Power(t1,3) + 131*Power(t1,4))) + 
                  Power(s1,2)*
                   (149 - 19*Power(s2,6) - 935*t1 + 871*Power(t1,2) + 
                     527*Power(t1,3) - 626*Power(t1,4) - 
                     14*Power(t1,5) + 38*Power(t1,6) + 
                     4*Power(s2,5)*(-31 + 26*t1) + 
                     Power(s2,4)*(382 + 133*t1 - 180*Power(t1,2)) + 
                     Power(s2,3)*
                      (1220 - 1809*t1 + 446*Power(t1,2) + 
                        68*Power(t1,3)) + 
                     Power(s2,2)*
                      (1766 - 3116*t1 + 1880*Power(t1,2) - 
                        809*Power(t1,3) + 121*Power(t1,4)) + 
                     s2*(1300 - 3070*t1 + 1352*Power(t1,2) + 
                        173*Power(t1,3) + 368*Power(t1,4) - 
                        132*Power(t1,5))) + 
                  s1*(42 - 18*Power(s2,7) + 1069*t1 - 
                     1910*Power(t1,2) + 212*Power(t1,3) + 
                     624*Power(t1,4) + 63*Power(t1,5) - 
                     101*Power(t1,6) + 3*Power(t1,7) + 
                     Power(s2,6)*(288 + 119*t1) + 
                     Power(s2,5)*(467 - 1599*t1 - 242*Power(t1,2)) + 
                     Power(s2,4)*
                      (-540 - 496*t1 + 3161*Power(t1,2) + 
                        157*Power(t1,3)) + 
                     Power(s2,3)*
                      (-1887 + 4679*t1 - 1953*Power(t1,2) - 
                        2683*Power(t1,3) + 58*Power(t1,4)) + 
                     Power(s2,2)*
                      (-3427 + 7231*t1 - 7347*Power(t1,2) + 
                        3565*Power(t1,3) + 738*Power(t1,4) - 
                        103*Power(t1,5)) + 
                     s2*(-2231 + 6507*t1 - 5538*Power(t1,2) + 
                        2580*Power(t1,3) - 1646*Power(t1,4) + 
                        196*Power(t1,5) + 26*Power(t1,6)))))*
             (-s1 + s2 - t1 + t2) + 
            (-136 + 2*Power(s,8)*(-1 + s1) - 2*s2 - 30*Power(s2,2) - 
               1006*Power(s2,3) - 646*Power(s2,4) + 210*Power(s2,5) + 
               48*Power(s2,6) - 18*Power(s2,7) + 20*Power(s2,8) + 
               346*t1 + 286*s2*t1 + 2406*Power(s2,2)*t1 + 
               2959*Power(s2,3)*t1 - 222*Power(s2,4)*t1 - 
               284*Power(s2,5)*t1 + 278*Power(s2,6)*t1 - 
               6*Power(s2,7)*t1 - 23*Power(s2,8)*t1 - 
               472*Power(t1,2) - 2080*s2*Power(t1,2) - 
               4391*Power(s2,2)*Power(t1,2) - 
               530*Power(s2,3)*Power(t1,2) + 
               661*Power(s2,4)*Power(t1,2) - 
               1097*Power(s2,5)*Power(t1,2) - 
               366*Power(s2,6)*Power(t1,2) + 
               152*Power(s2,7)*Power(t1,2) + 688*Power(t1,3) + 
               2523*s2*Power(t1,3) + 716*Power(s2,2)*Power(t1,3) - 
               928*Power(s2,3)*Power(t1,3) + 
               2037*Power(s2,4)*Power(t1,3) + 
               1247*Power(s2,5)*Power(t1,3) - 
               445*Power(s2,6)*Power(t1,3) - 445*Power(t1,4) - 
               8*s2*Power(t1,4) + 928*Power(s2,2)*Power(t1,4) - 
               2121*Power(s2,3)*Power(t1,4) - 
               1968*Power(s2,4)*Power(t1,4) + 
               750*Power(s2,5)*Power(t1,4) - 166*Power(t1,5) - 
               584*s2*Power(t1,5) + 1319*Power(s2,2)*Power(t1,5) + 
               1812*Power(s2,3)*Power(t1,5) - 
               785*Power(s2,4)*Power(t1,5) + 159*Power(t1,6) - 
               480*s2*Power(t1,6) - 1042*Power(s2,2)*Power(t1,6) + 
               508*Power(s2,3)*Power(t1,6) + 82*Power(t1,7) + 
               363*s2*Power(t1,7) - 187*Power(s2,2)*Power(t1,7) - 
               60*Power(t1,8) + 30*s2*Power(t1,8) + 
               2*Power(s,7)*(-1 + s1)*(43 + 10*s1 - 15*s2 + 9*t1) - 
               Power(s1,4)*(10*Power(s2,5) + 
                  Power(s2,4)*(21 - 45*t1) + 
                  Power(s2,3)*(-33 - 71*t1 + 80*Power(t1,2)) + 
                  Power(s2,2)*
                   (2 + 89*t1 + 87*Power(t1,2) - 70*Power(t1,3)) + 
                  t1*(-4 + 6*t1 + 23*Power(t1,2) + 8*Power(t1,3) - 
                     5*Power(t1,4)) + 
                  s2*(4 - 8*t1 - 79*Power(t1,2) - 45*Power(t1,3) + 
                     30*Power(t1,4))) + 
               Power(s1,3)*(50*Power(s2,6) + 
                  Power(s2,5)*(1 - 283*t1) + 
                  Power(s2,4)*(-149 - 2*t1 + 659*Power(t1,2)) - 
                  2*Power(s2,3)*
                   (101 - 296*t1 + 10*Power(t1,2) + 403*Power(t1,3)) \
+ Power(s2,2)*(-76 + 530*t1 - 870*Power(t1,2) + 62*Power(t1,3) + 
                     544*Power(t1,4)) + 
                  s2*(12 + 80*t1 - 426*Power(t1,2) + 
                     560*Power(t1,3) - 61*Power(t1,4) - 
                     191*Power(t1,5)) + 
                  t1*(-12 - 4*t1 + 98*Power(t1,2) - 133*Power(t1,3) + 
                     20*Power(t1,4) + 27*Power(t1,5))) + 
               Power(s1,2)*(-72 - 43*Power(s2,7) + 90*t1 + 
                  48*Power(t1,2) - 84*Power(t1,3) + 361*Power(t1,4) - 
                  343*Power(t1,5) + 42*Power(t1,6) + 44*Power(t1,7) + 
                  Power(s2,6)*(98 + 302*t1) + 
                  Power(s2,5)*(532 - 508*t1 - 915*Power(t1,2)) + 
                  Power(s2,4)*
                   (333 - 2261*t1 + 1056*Power(t1,2) + 
                     1544*Power(t1,3)) + 
                  Power(s2,3)*
                   (24 - 1296*t1 + 3876*Power(t1,2) - 
                     1142*Power(t1,3) - 1561*Power(t1,4)) + 
                  2*Power(s2,2)*
                   (191 - 209*t1 + 978*Power(t1,2) - 
                     1720*Power(t1,3) + 350*Power(t1,4) + 
                     471*Power(t1,5)) + 
                  s2*(126 - 582*t1 + 486*Power(t1,2) - 
                     1354*Power(t1,3) + 1636*Power(t1,4) - 
                     246*Power(t1,5) - 313*Power(t1,6))) + 
               s1*(208 + 3*Power(s2,8) - 460*t1 + 592*Power(t1,2) - 
                  899*Power(t1,3) + 405*Power(t1,4) + 
                  261*Power(t1,5) - 88*Power(t1,6) - 99*Power(t1,7) + 
                  30*Power(t1,8) - Power(s2,7)*(6 + 79*t1) + 
                  Power(s2,6)*(-387 + 24*t1 + 398*Power(t1,2)) + 
                  Power(s2,5)*
                   (-652 + 1836*t1 + 118*Power(t1,2) - 
                     945*Power(t1,3)) + 
                  Power(s2,4)*
                   (637 + 1731*t1 - 3250*Power(t1,2) - 
                     704*Power(t1,3) + 1305*Power(t1,4)) + 
                  Power(s2,3)*
                   (1057 - 2686*t1 - 1476*Power(t1,2) + 
                     2702*Power(t1,3) + 1353*Power(t1,4) - 
                     1133*Power(t1,5)) + 
                  Power(s2,2)*
                   (-212 - 2365*t1 + 3798*Power(t1,2) + 
                     632*Power(t1,3) - 1151*Power(t1,4) - 
                     1249*Power(t1,5) + 624*Power(t1,6)) - 
                  s2*(100 + 12*t1 - 2191*Power(t1,2) + 
                     2154*Power(t1,3) + 496*Power(t1,4) - 
                     338*Power(t1,5) - 563*Power(t1,6) + 
                     203*Power(t1,7))) + 
               Power(s,6)*(644 + 28*Power(s1,3) - 602*t1 - 
                  106*Power(t1,2) - Power(s2,2)*(127 + 19*t1) + 
                  Power(s1,2)*(69 - 154*s2 + 165*t1) + 
                  3*s2*(171 + 84*t1 + 5*Power(t1,2)) + 
                  s1*(-715 + 146*Power(s2,2) + 426*t1 + 
                     91*Power(t1,2) - 2*s2*(202 + 111*t1))) + 
               Power(s,5)*(-1267 + 5*Power(s1,4) + 3570*t1 - 
                  1669*Power(t1,2) - 390*Power(t1,3) + 
                  Power(s2,3)*(235 + 118*t1) + 
                  Power(s1,3)*(31 - 186*s2 + 167*t1) - 
                  3*Power(s2,2)*(413 + 339*t1 + 69*Power(t1,2)) + 
                  s2*(-3511 + 3048*t1 + 1069*Power(t1,2) + 
                     105*Power(t1,3)) + 
                  Power(s1,2)*
                   (-446 + 508*Power(s2,2) + 411*t1 + 
                     569*Power(t1,2) - s2*(441 + 1069*t1)) + 
                  s1*(1618 - 353*Power(s2,3) - 3946*t1 + 
                     1062*Power(t1,2) + 285*Power(t1,3) + 
                     5*Power(s2,2)*(205 + 186*t1) + 
                     s2*(4021 - 2212*t1 - 824*Power(t1,2)))) + 
               Power(s,4)*(595 - 5205*t1 + 7974*Power(t1,2) - 
                  2294*Power(t1,3) - 850*Power(t1,4) + 
                  Power(s1,4)*(-8 - 30*s2 + 25*t1) - 
                  5*Power(s2,4)*(36 + 61*t1) + 
                  2*Power(s2,3)*(764 + 951*t1 + 415*Power(t1,2)) - 
                  3*Power(s2,2)*
                   (-2557 + 1960*t1 + 1208*Power(t1,2) + 
                     275*Power(t1,3)) + 
                  s2*(5554 - 15852*t1 + 6803*Power(t1,2) + 
                     2719*Power(t1,3) + 300*Power(t1,4)) + 
                  Power(s1,3)*
                   (-115 + 514*Power(s2,2) + 144*t1 + 
                     415*Power(t1,2) - s2*(131 + 935*t1)) + 
                  Power(s1,2)*
                   (483 - 935*Power(s2,3) - 2170*t1 + 
                     996*Power(t1,2) + 1070*Power(t1,3) + 
                     Power(s2,2)*(1185 + 2908*t1) + 
                     s2*(2209 - 2153*t1 - 3049*Power(t1,2))) + 
                  s1*(-970 + 485*Power(s2,4) + 6911*t1 - 
                     8760*Power(t1,2) + 1209*Power(t1,3) + 
                     550*Power(t1,4) - 7*Power(s2,3)*(198 + 277*t1) + 
                     Power(s2,2)*
                      (-9309 + 4582*t1 + 2778*Power(t1,2)) - 
                     s2*(7160 - 17967*t1 + 4363*Power(t1,2) + 
                        1879*Power(t1,3)))) + 
               Power(s,3)*(416 + 1064*t1 - 8133*Power(t1,2) + 
                  9101*Power(t1,3) - 1516*Power(t1,4) - 
                  1102*Power(t1,5) + 28*Power(s2,5)*(-1 + 15*t1) - 
                  2*Power(s2,4)*(486 + 924*t1 + 775*Power(t1,2)) + 
                  Power(s2,3)*
                   (-8435 + 5094*t1 + 5884*Power(t1,2) + 
                     2290*Power(t1,3)) - 
                  Power(s2,2)*
                   (9228 - 26461*t1 + 9364*Power(t1,2) + 
                     7082*Power(t1,3) + 1610*Power(t1,4)) + 
                  s2*(-1283 + 16931*t1 - 27065*Power(t1,2) + 
                     6771*Power(t1,3) + 4176*Power(t1,4) + 
                     450*Power(t1,5)) + 
                  Power(s1,4)*
                   (-23 + 70*Power(s2,2) - 32*t1 + 50*Power(t1,2) - 
                     15*s2*(-3 + 8*t1)) + 
                  Power(s1,3)*
                   (82 - 756*Power(s2,3) - 478*t1 + 
                     266*Power(t1,2) + 550*Power(t1,3) + 
                     Power(s2,2)*(206 + 2086*t1) - 
                     2*s2*(-249 + 227*t1 + 940*Power(t1,2))) + 
                  Power(s1,2)*
                   (-38 + 1040*Power(s2,4) + 1804*t1 - 
                     4177*Power(t1,2) + 1254*Power(t1,3) + 
                     1190*Power(t1,4) - 
                     Power(s2,3)*(1697 + 4262*t1) + 
                     Power(s2,2)*
                      (-4473 + 4540*t1 + 6618*Power(t1,2)) - 
                     s2*(1688 - 8398*t1 + 4059*Power(t1,2) + 
                        4586*Power(t1,3))) + 
                  s1*(-369 - 392*Power(s2,5) - 2447*t1 + 
                     11302*Power(t1,2) - 9842*Power(t1,3) + 
                     426*Power(t1,4) + 652*Power(t1,5) + 
                     2*Power(s2,4)*(532 + 1133*t1) - 
                     2*Power(s2,3)*
                      (-5669 + 2379*t1 + 2349*Power(t1,2)) + 
                     Power(s2,2)*
                      (12310 - 31947*t1 + 6575*Power(t1,2) + 
                        4838*Power(t1,3)) + 
                     s2*(2313 - 22718*t1 + 30231*Power(t1,2) - 
                        3294*Power(t1,3) - 2666*Power(t1,4)))) + 
               Power(s,2)*(-368 + 1654*t1 - 106*Power(t1,2) - 
                  5885*Power(t1,3) + 5499*Power(t1,4) - 
                  250*Power(t1,5) - 838*Power(t1,6) - 
                  5*Power(s2,6)*(-29 + 65*t1) + 
                  3*Power(s2,5)*(83 + 300*t1 + 505*Power(t1,2)) - 
                  Power(s2,4)*
                   (-4729 + 1452*t1 + 5008*Power(t1,2) + 
                     2970*Power(t1,3)) + 
                  2*Power(s2,3)*
                   (3536 - 9965*t1 + 1949*Power(t1,2) + 
                     4407*Power(t1,3) + 1535*Power(t1,4)) - 
                  Power(s2,2)*
                   (-129 + 18383*t1 - 30894*Power(t1,2) + 
                     4725*Power(t1,3) + 7767*Power(t1,4) + 
                     1665*Power(t1,5)) + 
                  s2*(-1920 + 585*t1 + 17104*Power(t1,2) - 
                     21202*Power(t1,3) + 2280*Power(t1,4) + 
                     3754*Power(t1,5) + 375*Power(t1,6)) - 
                  Power(s1,4)*
                   (6 + 80*Power(s2,3) + Power(s2,2)*(87 - 210*t1) + 
                     69*t1 + 48*Power(t1,2) - 50*Power(t1,3) + 
                     s2*(-79 - 135*t1 + 180*Power(t1,2))) + 
                  2*Power(s1,3)*
                   (-12 + 312*Power(s2,4) + 131*t1 - 
                     372*Power(t1,2) + 122*Power(t1,3) + 
                     205*Power(t1,4) - Power(s2,3)*(71 + 1159*t1) + 
                     Power(s2,2)*
                      (-400 + 237*t1 + 1587*Power(t1,2)) - 
                     s2*(183 - 778*t1 + 288*Power(t1,2) + 
                        945*Power(t1,3))) + 
                  Power(s1,2)*
                   (106 - 700*Power(s2,5) - 180*t1 + 
                     2520*Power(t1,2) - 3971*Power(t1,3) + 
                     861*Power(t1,4) + 785*Power(t1,5) + 
                     Power(s2,4)*(1356 + 3553*t1) + 
                     Power(s2,3)*
                      (4635 - 4773*t1 - 7280*Power(t1,2)) + 
                     Power(s2,2)*
                      (2256 - 12527*t1 + 6225*Power(t1,2) + 
                       7486*Power(t1,3)) + 
                     s2*(74 - 4690*t1 + 11805*Power(t1,2) - 
                        3669*Power(t1,3) - 3844*Power(t1,4))) + 
                  s1*(290 + 180*Power(s2,6) - 1615*t1 - 
                     1583*Power(t1,2) + 8604*Power(t1,3) - 
                     5701*Power(t1,4) - 360*Power(t1,5) + 
                     463*Power(t1,6) - 4*Power(s2,5)*(113 + 378*t1) + 
                     Power(s2,4)*
                      (-7645 + 2512*t1 + 4301*Power(t1,2)) - 
                     Power(s2,3)*
                      (10264 - 27613*t1 + 4191*Power(t1,2) + 
                        6124*Power(t1,3)) + 
                     Power(s2,2)*
                      (-1061 + 26196*t1 - 37240*Power(t1,2) + 
                        2255*Power(t1,3) + 4956*Power(t1,4)) + 
                     s2*(1991 + 2124*t1 - 24480*Power(t1,2) + 
                        22983*Power(t1,3) + 236*Power(t1,4) - 
                        2264*Power(t1,5)))) + 
               s*(138 - 768*t1 + 1926*Power(t1,2) - 1020*Power(t1,3) - 
                  1856*Power(t1,4) + 1605*Power(t1,5) + 
                  207*Power(t1,6) - 346*Power(t1,7) + 
                  Power(s2,7)*(-93 + 134*t1) - 
                  5*Power(s2,6)*(-5 + 33*t1 + 151*Power(t1,2)) + 
                  3*Power(s2,5)*
                   (-382 - 162*t1 + 717*Power(t1,2) + 615*Power(t1,3)) \
+ Power(s2,4)*(-2341 + 6035*t1 + 1429*Power(t1,2) - 
                     5308*Power(t1,3) - 2510*Power(t1,4)) + 
                  Power(s2,3)*
                   (1205 + 6879*t1 - 12464*Power(t1,2) - 
                     1789*Power(t1,3) + 6409*Power(t1,4) + 
                     2000*Power(t1,5)) + 
                  Power(s2,2)*
                   (2502 - 4606*t1 - 8397*Power(t1,2) + 
                     13032*Power(t1,3) + 1317*Power(t1,4) - 
                     4461*Power(t1,5) - 879*Power(t1,6)) + 
                  s2*(534 - 4278*t1 + 4395*Power(t1,2) + 
                     5719*Power(t1,3) - 7062*Power(t1,4) - 
                     703*Power(t1,5) + 1813*Power(t1,6) + 
                     165*Power(t1,7)) + 
                  Power(s1,4)*
                   (4 + 45*Power(s2,4) + Power(s2,3)*(71 - 160*t1) - 
                     12*t1 - 69*Power(t1,2) - 32*Power(t1,3) + 
                     25*Power(t1,4) + 
                     Power(s2,2)*(-89 - 174*t1 + 210*Power(t1,2)) + 
                     s2*(8 + 158*t1 + 135*Power(t1,2) - 
                        120*Power(t1,3))) + 
                  Power(s1,3)*
                   (-12 - 274*Power(s2,5) - 28*t1 + 278*Power(t1,2) - 
                     514*Power(t1,3) + 111*Power(t1,4) + 
                     163*Power(t1,5) + Power(s2,4)*(35 + 1283*t1) - 
                     2*Power(s2,3)*
                      (-283 + 81*t1 + 1184*Power(t1,2)) + 
                     2*Power(s2,2)*
                      (243 - 835*t1 + 165*Power(t1,2) + 
                        1073*Power(t1,3)) - 
                     2*s2*(-50 + 396*t1 - 809*Power(t1,2) + 
                        157*Power(t1,3) + 475*Power(t1,4))) + 
                  Power(s1,2)*
                   (18 + 264*Power(s2,6) + 154*t1 - 226*Power(t1,2) + 
                     1560*Power(t1,3) - 1861*Power(t1,4) + 
                     303*Power(t1,5) + 285*Power(t1,6) - 
                     Power(s2,5)*(570 + 1597*t1) + 
                     Power(s2,4)*
                      (-2457 + 2483*t1 + 4057*Power(t1,2)) - 
                     2*Power(s2,3)*
                      (692 - 4280*t1 + 2109*Power(t1,2) + 
                        2757*Power(t1,3)) + 
                     2*Power(s2,2)*
                      (-30 + 2089*t1 - 5747*Power(t1,2) + 
                        1785*Power(t1,3) + 2105*Power(t1,4)) - 
                     s2*(496 - 580*t1 + 4356*Power(t1,2) - 
                        7252*Power(t1,3) + 1568*Power(t1,4) + 
                        1705*Power(t1,5))) + 
                  s1*(-180 - 41*Power(s2,7) + 810*t1 - 
                     2145*Power(t1,2) + 299*Power(t1,3) + 
                     2856*Power(t1,4) - 1476*Power(t1,5) - 
                     378*Power(t1,6) + 181*Power(t1,7) + 
                     Power(s2,6)*(93 + 538*t1) + 
                     Power(s2,5)*(2697 - 574*t1 - 2046*Power(t1,2)) + 
                     Power(s2,4)*
                      (4148 - 11523*t1 + 799*Power(t1,2) + 
                        3825*Power(t1,3)) + 
                     Power(s2,3)*
                      (-919 - 12120*t1 + 19019*Power(t1,2) + 
                        534*Power(t1,3) - 4145*Power(t1,4)) + 
                     Power(s2,2)*
                      (-2671 + 3019*t1 + 14530*Power(t1,2) - 
                        15753*Power(t1,3) - 2012*Power(t1,4) + 
                        2736*Power(t1,5)) + 
                     s2*(-206 + 4304*t1 - 2339*Power(t1,2) - 
                        9418*Power(t1,3) + 7036*Power(t1,4) + 
                        1538*Power(t1,5) - 1048*Power(t1,6)))))*
             Power(-s1 + s2 - t1 + t2,3) + 
            (84 + 6*Power(s,6)*(-1 + s1) + 230*s2 - 144*Power(s2,2) + 
               210*Power(s2,3) - 18*Power(s2,4) + 66*Power(s2,5) - 
               12*Power(s2,6) - 270*t1 - 87*s2*t1 - 
               266*Power(s2,2)*t1 - 173*Power(s2,4)*t1 + 
               32*Power(s2,5)*t1 + 2*Power(s2,6)*t1 + 
               187*Power(t1,2) + 80*s2*Power(t1,2) - 
               35*Power(s2,2)*Power(t1,2) + 
               156*Power(s2,3)*Power(t1,2) - 
               13*Power(s2,4)*Power(t1,2) - 
               6*Power(s2,5)*Power(t1,2) - 24*Power(t1,3) + 
               102*s2*Power(t1,3) - 30*Power(s2,2)*Power(t1,3) - 
               38*Power(s2,3)*Power(t1,3) + 
               6*Power(s2,4)*Power(t1,3) - 49*Power(t1,4) - 
               47*s2*Power(t1,4) + 47*Power(s2,2)*Power(t1,4) - 
               2*Power(s2,3)*Power(t1,4) + 28*Power(t1,5) - 
               16*s2*Power(t1,5) + 
               Power(s,5)*(-32 + 9*Power(s1,2) - 38*t1 + 
                  s2*(38 + t1) + s1*(24 - 39*s2 + 37*t1)) + 
               Power(s1,4)*(2*Power(s2,3) - t1*Power(1 + t1,2) - 
                  Power(s2,2)*(3 + 5*t1) + 
                  s2*(1 + 5*t1 + 4*Power(t1,2))) - 
               Power(s1,2)*(-48 + 6*Power(s2,5) + 
                  Power(s2,4)*(38 - 18*t1) + 68*t1 - 51*Power(t1,2) + 
                  24*Power(t1,3) + 15*Power(t1,4) + 4*Power(t1,5) + 
                  Power(s2,3)*(-7 - 139*t1 + 12*Power(t1,2)) + 
                  Power(s2,2)*
                   (-125 + 62*t1 + 177*Power(t1,2) + 10*Power(t1,3)) \
- s2*t1*(-152 + 75*t1 + 91*Power(t1,2) + 14*Power(t1,3))) - 
               Power(s1,3)*(4*Power(s2,4) - 
                  Power(s2,3)*(39 + 17*t1) + 
                  Power(s2,2)*(45 + 98*t1 + 25*Power(t1,2)) + 
                  3*t1*(2 + 11*t1 + 6*Power(t1,2) + Power(t1,3)) - 
                  s2*(6 + 78*t1 + 77*Power(t1,2) + 15*Power(t1,3))) + 
               s1*(-132 + 10*Power(s2,6) + 333*t1 - 171*Power(t1,2) + 
                  25*Power(t1,3) + 63*Power(t1,4) + 2*Power(t1,5) - 
                  2*Power(s2,5)*(19 + 24*t1) + 
                  Power(s2,4)*(-12 + 139*t1 + 95*Power(t1,2)) - 
                  2*Power(s2,3)*
                   (108 - 6*t1 + 109*Power(t1,2) + 48*Power(t1,3)) + 
                  Power(s2,2)*
                   (65 + 335*t1 + 107*Power(t1,2) + 
                     172*Power(t1,3) + 49*Power(t1,4)) - 
                  s2*(225 - 126*t1 + 140*Power(t1,2) + 
                     170*Power(t1,3) + 57*Power(t1,4) + 10*Power(t1,5)\
)) + Power(s,4)*(80 - 2*Power(s1,3) - 124*t1 - 92*Power(t1,2) - 
                  Power(s2,2)*(103 + 2*t1) + 
                  Power(s1,2)*(1 - 45*s2 + 31*t1) + 
                  s2*(177 + 190*t1 + 5*Power(t1,2)) + 
                  s1*(-83 + 105*Power(s2,2) + 102*t1 + 
                     87*Power(t1,2) - s2*(130 + 197*t1))) - 
               Power(s,3)*(109 + Power(s1,4) - 278*t1 + 
                  151*Power(t1,2) + 108*Power(t1,3) + 
                  Power(s2,3)*(-153 + 2*t1) + 
                  Power(s1,3)*(25 - 10*s2 + 9*t1) + 
                  Power(s2,2)*(405 + 375*t1 + 9*Power(t1,2)) - 
                  s2*(-247 + 505*t1 + 340*Power(t1,2) + 
                     9*Power(t1,3)) + 
                  Power(s1,2)*
                   (15 - 88*Power(s2,2) + 34*t1 - 35*Power(t1,2) + 
                     s2*(-19 + 121*t1)) + 
                  s1*(-108 + 151*Power(s2,3) + 219*t1 - 
                     151*Power(t1,2) - 99*Power(t1,3) - 
                     Power(s2,2)*(285 + 416*t1) + 
                     s2*(-290 + 439*t1 + 366*Power(t1,2)))) + 
               Power(s,2)*(-61 + Power(s1,4)*(-2 + 4*s2 - 3*t1) - 
                  215*t1 + 268*Power(t1,2) - 30*Power(t1,3) - 
                  62*Power(t1,4) + Power(s2,4)*(-131 + 8*t1) + 
                  Power(s2,3)*(473 + 364*t1 - 3*Power(t1,2)) - 
                  Power(s2,2)*
                   (-239 + 819*t1 + 422*Power(t1,2) + 12*Power(t1,3)) \
+ s2*(364 - 590*t1 + 439*Power(t1,2) + 246*Power(t1,3) + 
                     7*Power(t1,4)) - 
                  Power(s1,3)*
                   (37 + 18*Power(s2,2) + 68*t1 + 15*Power(t1,2) - 
                     s2*(89 + 35*t1)) + 
                  Power(s1,2)*
                   (57 - 83*Power(s2,3) - 67*t1 - 86*Power(t1,2) + 
                     9*Power(t1,3) + 13*Power(s2,2)*(-6 + 13*t1) + 
                     s2*(32 + 170*t1 - 93*Power(t1,2))) + 
                  s1*(46 + 123*Power(s2,4) + 251*t1 - 
                     134*Power(t1,2) + 94*Power(t1,3) + 
                     55*Power(t1,4) - Power(s2,3)*(314 + 437*t1) + 
                     Power(s2,2)*(-345 + 719*t1 + 567*Power(t1,2)) - 
                     s2*(358 - 483*t1 + 521*Power(t1,2) + 
                        307*Power(t1,3)))) - 
               s*(138 - 78*t1 + 130*Power(t1,2) - 21*Power(t1,3) - 
                  57*Power(t1,4) + 14*Power(t1,5) + 
                  Power(s2,5)*(-61 + 7*t1) + 
                  Power(s2,4)*(279 + 173*t1 - 13*Power(t1,2)) + 
                  Power(s2,3)*
                   (54 - 611*t1 - 187*Power(t1,2) + 3*Power(t1,3)) + 
                  Power(s2,2)*
                   (461 - 311*t1 + 452*Power(t1,2) + 
                     103*Power(t1,3) + 5*Power(t1,4)) - 
                  s2*(185 + 437*t1 - 242*Power(t1,2) + 
                     64*Power(t1,3) + 42*Power(t1,4) + 2*Power(t1,5)) \
+ Power(s1,4)*(1 + 5*Power(s2,2) + 4*t1 + 3*Power(t1,2) - 
                     s2*(5 + 8*t1)) + 
                  Power(s1,3)*
                   (6 - 14*Power(s2,3) + 70*t1 + 61*Power(t1,2) + 
                     11*Power(t1,3) + Power(s2,2)*(103 + 43*t1) - 
                     2*s2*(41 + 83*t1 + 20*Power(t1,2))) + 
                  Power(s1,2)*
                   (20 - 37*Power(s2,4) - 108*t1 + 76*Power(t1,2) + 
                     66*Power(t1,3) + 8*Power(t1,4) + 
                     Power(s2,3)*(-96 + 97*t1) + 
                     Power(s2,2)*(24 + 274*t1 - 71*Power(t1,2)) + 
                     s2*(178 - 120*t1 - 242*Power(t1,2) + 
                        3*Power(t1,3))) + 
                  s1*(-153 + 54*Power(s2,5) + 77*t1 - 
                     168*Power(t1,2) - 65*Power(t1,3) - 
                     23*Power(t1,4) - 12*Power(t1,5) - 
                     Power(s2,4)*(173 + 229*t1) + 
                     Power(s2,3)*(-150 + 521*t1 + 383*Power(t1,2)) - 
                     Power(s2,2)*
                      (462 - 274*t1 + 595*Power(t1,2) + 
                        305*Power(t1,3)) + 
                     s2*(95 + 528*t1 - 31*Power(t1,2) + 
                        269*Power(t1,3) + 109*Power(t1,4)))))*
             Power(-s1 + s2 - t1 + t2,5) - 
            (-8 + 6*s2 - 10*Power(s2,2) + 4*Power(s2,3) + 2*t1 + 
               10*s2*t1 - 13*Power(s2,2)*t1 + 4*Power(s2,3)*t1 - 
               Power(s2,4)*t1 + 15*s2*Power(t1,2) - 
               13*Power(s2,2)*Power(t1,2) + 4*Power(s2,3)*Power(t1,2) - 
               6*Power(t1,3) + 13*s2*Power(t1,3) - 
               5*Power(s2,2)*Power(t1,3) - 4*Power(t1,4) + 
               2*s2*Power(t1,4) - 
               Power(s1,2)*(-5 + s2)*
                (-s2 + Power(s2,2) + t1 - 2*s2*t1 + Power(t1,2)) + 
               Power(s,3)*(-2 + Power(s1,2) + (-4 + s2)*t1 + 
                  s1*(2 - s2 + 3*t1)) + 
               s1*(8 + Power(s2,4) - 6*t1 - 3*Power(t1,2) + 
                  5*Power(t1,3) + 2*Power(t1,4) - 
                  Power(s2,3)*(6 + 5*t1) + 
                  Power(s2,2)*(7 + 17*t1 + 9*Power(t1,2)) - 
                  s2*(2 + 4*t1 + 16*Power(t1,2) + 7*Power(t1,3))) + 
               Power(s,2)*(-3*Power(s2,2)*t1 + 
                  Power(s1,2)*(6 - 3*s2 + 2*t1) + 
                  4*s2*(2 + 3*t1 + Power(t1,2)) - 
                  2*(4 + 5*t1 + 6*Power(t1,2)) + 
                  s1*(4 + 3*Power(s2,2) + 10*t1 + 8*Power(t1,2) - 
                     s2*(10 + 11*t1))) + 
               s*(3*Power(s2,3)*t1 - 
                  2*Power(s2,2)*(5 + 6*t1 + 4*Power(t1,2)) + 
                  s2*(18 + 23*t1 + 25*Power(t1,2) + 5*Power(t1,3)) - 
                  2*(3 + 4*t1 + 7*Power(t1,2) + 6*Power(t1,3)) + 
                  Power(s1,2)*
                   (5 + 3*Power(s2,2) + 11*t1 + Power(t1,2) - 
                     4*s2*(3 + t1)) + 
                  s1*(2 - 3*Power(s2,3) + t1 + 13*Power(t1,2) + 
                     7*Power(t1,3) + Power(s2,2)*(14 + 13*t1) - 
                     s2*(11 + 27*t1 + 17*Power(t1,2)))))*
             Power(-s1 + s2 - t1 + t2,7))/
          ((-1 + s1)*(-1 + s2)*(-1 + t1)*Power(-1 + s - s2 + t1,2)*
            (-1 + t2)) - (2*(s - s2 + t1)*
            (Power(s,3) + Power(s1,2) - Power(s2,3) + 
              3*Power(s2,2)*t1 - 3*s2*Power(t1,2) + Power(t1,3) + 
              Power(s,2)*(-2 - 3*s2 + 3*t1) + 
              s*(1 + s1 + 3*s2 + 3*Power(s2,2) - 3*t1 - 6*s2*t1 + 
                 3*Power(t1,2) - t2) + t2 + 3*s2*t2 - 3*t1*t2 + 
              Power(t2,2) - s1*(1 + 3*s2 - 3*t1 + 2*t2))*
            (-64 + 64*s2 + 16*Power(s2,2) - 16*Power(s2,3) + 64*t1 - 
              96*s2*t1 + 16*Power(s2,2)*t1 + 16*Power(s2,3)*t1 + 
              16*Power(t1,2) + 16*s2*Power(t1,2) - 
              32*Power(s2,2)*Power(t1,2) - 16*Power(t1,3) + 
              16*s2*Power(t1,3) + 136*t2 - 206*s2*t2 + 
              14*Power(s2,2)*t2 + 24*Power(s2,3)*t2 - 
              8*Power(s2,4)*t2 - 138*t1*t2 + 226*s2*t1*t2 - 
              48*Power(s2,2)*t1*t2 + 4*Power(s2,3)*t1*t2 - 
              24*Power(t1,2)*t2 - 2*s2*Power(t1,2)*t2 + 
              30*Power(s2,2)*Power(t1,2)*t2 - 
              4*Power(s2,3)*Power(t1,2)*t2 + 18*Power(t1,3)*t2 - 
              34*s2*Power(t1,3)*t2 + 12*Power(s2,2)*Power(t1,3)*t2 + 
              8*Power(t1,4)*t2 - 8*s2*Power(t1,4)*t2 - 36*Power(t2,2) + 
              205*s2*Power(t2,2) - 9*Power(s2,2)*Power(t2,2) - 
              20*Power(s2,3)*Power(t2,2) - 4*Power(s2,4)*Power(t2,2) + 
              39*t1*Power(t2,2) - 249*s2*t1*Power(t2,2) + 
              42*Power(s2,2)*t1*Power(t2,2) + 
              4*Power(s2,4)*t1*Power(t2,2) + 
              38*Power(t1,2)*Power(t2,2) + 
              11*s2*Power(t1,2)*Power(t2,2) - 
              21*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
              4*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
              21*Power(t1,3)*Power(t2,2) + 
              33*s2*Power(t1,3)*Power(t2,2) - 
              8*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
              8*Power(t1,4)*Power(t2,2) + 
              8*s2*Power(t1,4)*Power(t2,2) - 84*Power(t2,3) - 
              42*s2*Power(t2,3) - 64*Power(s2,2)*Power(t2,3) + 
              22*Power(s2,3)*Power(t2,3) + 14*Power(s2,4)*Power(t2,3) + 
              82*t1*Power(t2,3) + 159*s2*t1*Power(t2,3) - 
              13*Power(s2,2)*t1*Power(t2,3) - 
              33*Power(s2,3)*t1*Power(t2,3) - 
              4*Power(s2,4)*t1*Power(t2,3) - 
              51*Power(t1,2)*Power(t2,3) - 
              38*s2*Power(t1,2)*Power(t2,3) + 
              38*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
              11*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
              29*Power(t1,3)*Power(t2,3) - 
              17*s2*Power(t1,3)*Power(t2,3) - 
              9*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
              2*Power(t1,4)*Power(t2,3) + 
              2*s2*Power(t1,4)*Power(t2,3) + 56*Power(t2,4) - 
              21*s2*Power(t2,4) + 31*Power(s2,2)*Power(t2,4) - 
              7*Power(s2,3)*Power(t2,4) + 3*Power(s2,4)*Power(t2,4) - 
              55*t1*Power(t2,4) - 16*s2*t1*Power(t2,4) + 
              8*Power(s2,2)*t1*Power(t2,4) - 
              3*Power(s2,3)*t1*Power(t2,4) - 
              Power(s2,4)*t1*Power(t2,4) + 9*Power(t1,2)*Power(t2,4) - 
              7*s2*Power(t1,2)*Power(t2,4) + 
              3*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
              Power(s2,3)*Power(t1,2)*Power(t2,4) + 
              2*Power(t1,3)*Power(t2,4) - 
              3*s2*Power(t1,3)*Power(t2,4) - 8*Power(t2,5) - 
              2*s2*Power(t2,5) + 4*Power(s2,2)*Power(t2,5) + 
              10*t1*Power(t2,5) - 10*s2*t1*Power(t2,5) - 
              3*Power(s2,2)*t1*Power(t2,5) + 
              Power(s2,3)*t1*Power(t2,5) + 6*Power(t1,2)*Power(t2,5) + 
              7*s2*Power(t1,2)*Power(t2,5) - 
              3*Power(s2,2)*Power(t1,2)*Power(t2,5) - 
              4*Power(t1,3)*Power(t2,5) + 
              2*s2*Power(t1,3)*Power(t2,5) + 2*s2*Power(t2,6) - 
              2*t1*Power(t2,6) - 
              Power(s1,6)*(s2 - t1)*
               (-4 + 2*Power(s2,2) - t1*(-3 + t2) + s2*(1 - 2*t1 + t2)) \
+ 2*Power(s,6)*(1 + s1*(t1*(-1 + t2) - 2*t2) + 3*t2 - 2*t1*t2 + 
                 s2*(-3 + 2*s1 + t1 - t2 + t1*t2)) + 
              Power(s1,5)*(-2*Power(s2,4) + 8*(-1 + t2) + 
                 Power(t1,3)*t2 + t1*(-13 + 8*t2 + Power(t2,2)) + 
                 Power(t1,2)*(2 - 12*t2 + 3*Power(t2,2)) + 
                 Power(s2,3)*(t1 + 5*(2 + t2)) + 
                 Power(s2,2)*
                  (-21 + 2*Power(t1,2) + 2*t2 + 2*Power(t2,2) - 
                    t1*(10 + 11*t2)) - 
                 s2*(-21 + Power(t1,3) + 16*t2 - 5*Power(t1,2)*t2 + 
                    Power(t2,2) + t1*(-19 - 10*t2 + 5*Power(t2,2)))) + 
              Power(s1,4)*(Power(s2,4)*(13 - t1 + 8*t2) - 
                 8*(5 - 9*t2 + 4*Power(t2,2)) - 
                 Power(t1,3)*(-10 + t2 + 5*Power(t2,2)) + 
                 t1*(29 + 2*t2 + 12*Power(t2,2) - 5*Power(t2,3)) - 
                 Power(t1,2)*
                  (37 + 13*t2 - 27*Power(t2,2) + 3*Power(t2,3)) - 
                 Power(s2,3)*
                  (41 + 28*t2 + 2*Power(t2,2) + 5*t1*(4 + t2)) + 
                 Power(s2,2)*
                  (38 + Power(t1,3) - 9*Power(t1,2)*(-2 + t2) + 
                    39*t2 + 11*Power(t2,2) + 
                    t1*(9 + 36*t2 + 5*Power(t2,2))) + 
                 s2*(31 - 94*t2 + 20*Power(t2,2) + 5*Power(t2,3) + 
                    Power(t1,3)*(-11 + 6*t2) + 
                    Power(t1,2)*(26 - 11*t2 + 2*Power(t2,2)) + 
                    t1*(-25 - 2*t2 - 38*Power(t2,2) + 3*Power(t2,3)))) \
+ Power(s1,3)*(2*Power(t1,4) + 
                 Power(t1,3)*
                  (-29 - 45*t2 + 11*Power(t2,2) + 9*Power(t2,3)) + 
                 4*(3 + 25*t2 - 40*Power(t2,2) + 12*Power(t2,3)) + 
                 Power(t1,2)*
                  (-11 + 120*t2 - Power(t2,2) - 35*Power(t2,3) + 
                    Power(t2,4)) + 
                 t1*(48 - 105*t2 + 72*Power(t2,2) - 38*Power(t2,3) + 
                    9*Power(t2,4)) + 
                 2*Power(s2,4)*
                  (-7 - 21*t2 - 6*Power(t2,2) + 2*t1*(1 + t2)) + 
                 Power(s2,3)*
                  (18 + 70*t2 + 32*Power(t2,2) - 4*Power(t2,3) - 
                    Power(t1,2)*(7 + t2) + 
                    t1*(17 + 71*t2 + 8*Power(t2,2))) - 
                 Power(s2,2)*
                  (-82 + 58*t2 + 57*Power(t2,2) + 27*Power(t2,3) + 
                    2*Power(t2,4) + Power(t1,3)*(-5 + 3*t2) + 
                    Power(t1,2)*(26 + 74*t2 - 18*Power(t2,2)) + 
                    t1*(33 + 9*t2 + 47*Power(t2,2) - 11*Power(t2,3))) \
+ s2*(-144 - 2*Power(t1,4) + 29*t2 + 148*Power(t2,2) - 
                    10*Power(t2,3) - 9*Power(t2,4) + 
                    Power(t1,3)*(21 + 45*t2 - 14*Power(t2,2)) - 
                    4*Power(t1,2)*
                     (-9 + 5*t2 - 4*Power(t2,2) + 4*Power(t2,3)) + 
                    t1*(21 - 82*t2 - 14*Power(t2,2) + 62*Power(t2,3) + 
                       Power(t2,4)))) - 
              Power(s1,2)*(Power(t1,4)*(8 + 6*t2) + 
                 Power(t1,2)*
                  (-98 + 117*t2 + 128*Power(t2,2) - 43*Power(t2,3) - 
                    22*Power(t2,4)) + 
                 Power(t1,3)*
                  (13 - 77*t2 - 66*Power(t2,2) + 23*Power(t2,3) + 
                    7*Power(t2,4)) + 
                 4*(-27 + 45*t2 + 6*Power(t2,2) - 32*Power(t2,3) + 
                    8*Power(t2,4)) + 
                 t1*(149 - 140*t2 - 56*Power(t2,2) + 84*Power(t2,3) - 
                    26*Power(t2,4) + 7*Power(t2,5)) + 
                 2*Power(s2,4)*
                  (6 - 21*t2 - 24*Power(t2,2) - 4*Power(t2,3) + 
                    t1*(-2 + 6*t2 + 3*Power(t2,2))) + 
                 Power(s2,3)*
                  (-68 + 78*t2 + 16*Power(t2,2) + 20*Power(t2,3) - 
                    4*Power(t2,4) - Power(t1,2)*t2*(25 + 3*t2) + 
                    t1*(20 + 59*t2 + 85*Power(t2,2) + 4*Power(t2,3))) \
- Power(s2,2)*(-121 - 6*t2 - 47*Power(t2,2) + 93*Power(t2,3) + 
                    18*Power(t2,4) + Power(t2,5) + 
                    Power(t1,3)*(-12 - 19*t2 + 3*Power(t2,2)) + 
                    Power(t1,2)*
                     (15 + 76*t2 + 97*Power(t2,2) - 20*Power(t2,3)) + 
                    t1*(58 - 27*t2 + 15*Power(t2,2) + 
                       23*Power(t2,3) - 13*Power(t2,4))) + 
                 s2*(39 - 308*t2 + 160*Power(t2,2) + 104*Power(t2,3) - 
                    6*Power(t2,4) - 7*Power(t2,5) - 
                    2*Power(t1,4)*(4 + 3*t2) + 
                    Power(t1,3)*
                     (-25 + 53*t2 + 60*Power(t2,2) - 16*Power(t2,3)) + 
                    Power(t1,2)*
                     (117 - 52*t2 + 73*Power(t2,2) - 8*Power(t2,3) - 
                       16*Power(t2,4)) + 
                    t1*(-107 + 169*t2 - 311*Power(t2,2) + 
                       64*Power(t2,3) + 40*Power(t2,4) + Power(t2,5)))) \
+ s1*(2*Power(t1,4)*(-4 + 8*t2 + 3*Power(t2,2)) - 
                 2*Power(s2,4)*
                  (-4 + 4*(-2 + t1)*t2 + (21 - 6*t1)*Power(t2,2) + 
                    (11 - 2*t1)*Power(t2,3) + Power(t2,4)) + 
                 Power(t1,2)*
                  (88 - 232*t2 + 195*Power(t2,2) + 36*Power(t2,3) - 
                    37*Power(t2,4) - 5*Power(t2,5)) + 
                 4*(-2 - 34*t2 + 63*Power(t2,2) - 23*Power(t2,3) - 
                    6*Power(t2,4) + 2*Power(t2,5)) + 
                 Power(t1,3)*
                  (-2 + 34*t2 - 77*Power(t2,2) - 33*Power(t2,3) + 
                    17*Power(t2,4) + 2*Power(t2,5)) + 
                 t1*(-70 + 270*t2 - 286*Power(t2,2) + 
                    75*Power(t2,3) + 13*Power(t2,4) - 2*Power(t2,5) + 
                    2*Power(t2,6)) - 
                 Power(s2,3)*
                  (8 + 48*t2 - 38*Power(t2,2) + 6*Power(t2,3) - 
                    6*Power(t2,4) + Power(t2,5) + 
                    Power(t1,2)*
                     (-4 - 4*t2 + 29*Power(t2,2) + 3*Power(t2,3)) + 
                    t1*(20 - 20*t2 - 75*Power(t2,2) - 
                       37*Power(t2,3) + Power(t2,4))) + 
                 Power(s2,2)*
                  (-110 + 226*t2 - 28*Power(t2,2) + 36*Power(t2,3) - 
                    58*Power(t2,4) - 3*Power(t2,5) - 
                    Power(t1,3)*
                     (12 - 20*t2 - 23*Power(t2,2) + Power(t2,3)) + 
                    2*Power(t1,2)*
                     (1 + 3*t2 - 44*Power(t2,2) - 22*Power(t2,3) + 
                       6*Power(t2,4)) + 
                    t1*(112 - 196*t2 + 89*Power(t2,2) - 
                       23*Power(t2,3) + Power(t2,4) + 4*Power(t2,5))) \
- s2*(-158 + 198*t2 + 106*Power(t2,2) - 121*Power(t2,3) - 
                    31*Power(t2,4) + 6*Power(t2,5) + 2*Power(t2,6) + 
                    2*Power(t1,4)*(-4 + 8*t2 + 3*Power(t2,2)) + 
                    Power(t1,3)*
                     (-18 + 58*t2 - 49*Power(t2,2) - 29*Power(t2,3) + 
                       9*Power(t2,4)) + 
                    t1*(66 - 78*t2 + 11*Power(t2,2) + 
                       188*Power(t2,3) - 71*Power(t2,4) - 
                       8*Power(t2,5)) + 
                    Power(t1,2)*
                     (94 - 202*t2 + 66*Power(t2,2) - 74*Power(t2,3) + 
                       20*Power(t2,4) + 5*Power(t2,5)))) + 
              Power(s,5)*(Power(s1,2)*
                  (-6 + 2*Power(s2,2) + t1*(11 - 9*t2) - 
                    2*s2*(7 + t1 - t2) + 15*t2 + Power(t2,2)) + 
                 Power(s2,2)*
                  (12 + Power(t1,2) + 7*t2 - 5*t1*(1 + t2)) + 
                 2*(19 + Power(t1,2)*(3 - 4*t2) - 2*t2 + 
                    9*Power(t2,2) + t1*(-11 + 9*t2 - 8*Power(t2,2))) + 
                 s2*(-24 - 41*t2 - 6*Power(t2,2) + 
                    Power(t1,2)*(-4 + 5*t2) + 
                    t1*(21 + 2*t2 + 7*Power(t2,2))) + 
                 s1*(-26 + 2*Power(s2,2)*(-6 + t1 - t2) - 17*t2 - 
                    12*Power(t2,2) + Power(t1,2)*(-2 + 3*t2) + 
                    t1*(1 + 2*t2 + 9*Power(t2,2)) - 
                    s2*(-30 + Power(t1,2) - 40*t2 + Power(t2,2) + 
                       t1*(6 + 8*t2)))) - 
              Power(s,4)*(52 - 101*t1 + 39*Power(t1,2) - 
                 4*Power(t1,3) - 93*t2 + 58*t1*t2 - 
                 41*Power(t1,2)*t2 + 4*Power(t1,3)*t2 + Power(t2,2) - 
                 51*t1*Power(t2,2) + 32*Power(t1,2)*Power(t2,2) - 
                 16*Power(t2,3) + 24*t1*Power(t2,3) + 
                 Power(s2,3)*
                  (8 - 3*t1 + Power(t1,2) + 8*t2 - 4*t1*t2) + 
                 Power(s1,3)*
                  (-22 + 8*Power(s2,2) + 21*t2 + 3*Power(t2,2) - 
                    8*t1*(-3 + 2*t2) + s2*(-17 - 8*t1 + 7*t2)) - 
                 Power(s2,2)*
                  (49 + Power(t1,3) + 71*t2 - 3*Power(t1,2)*t2 + 
                    21*Power(t2,2) - t1*(31 + 3*t2 + 17*Power(t2,2))) \
+ Power(s1,2)*(-79 + 4*Power(s2,3) + 
                    3*Power(s2,2)*(-16 + t1 - 3*t2) + 11*t2 - 
                    36*Power(t2,2) - 3*Power(t2,3) + 
                    Power(t1,2)*(-11 + 13*t2) + 
                    s2*(65 + t1 - 2*Power(t1,2) + 102*t2 - 
                       11*t1*t2 - 7*Power(t2,2)) + 
                    t1*(11 - 33*t2 + 29*Power(t2,2))) + 
                 s2*(61 + Power(t1,3)*(4 - 3*t2) + 22*t2 + 
                    92*Power(t2,2) + 6*Power(t2,3) - 
                    2*Power(t1,2)*(19 - 11*t2 + 9*Power(t2,2)) - 
                    t1*(-52 + 11*t2 + 6*Power(t2,2) + 9*Power(t2,3))) \
+ s1*(123 + Power(s2,3)*(-13 + 3*t1 - 4*t2) + 25*t2 - Power(t1,3)*t2 + 
                    53*Power(t2,2) + 10*Power(t2,3) + 
                    Power(t1,2)*(27 - 19*t2 - 14*Power(t2,2)) - 
                    t1*(59 - 71*t2 + 17*Power(t2,2) + 
                       15*Power(t2,3)) + 
                    Power(s2,2)*
                     (65 + Power(t1,2) + 93*t2 + 4*Power(t2,2) - 
                       t1*(10 + 19*t2)) + 
                    s2*(-158 + Power(t1,3) - 112*t2 - 89*Power(t2,2) + 
                       3*Power(t2,3) + Power(t1,2)*(-17 + 22*t2) + 
                       t1*(56 - 38*t2 + 28*Power(t2,2))))) + 
              Power(s,3)*(-45 - 46*t1 + 98*Power(t1,2) - 
                 23*Power(t1,3) - 2*Power(t1,4) - 6*t2 + 90*t1*t2 - 
                 69*Power(t1,2)*t2 + 23*Power(t1,3)*t2 + 
                 43*Power(t2,2) - 6*t1*Power(t2,2) + 
                 75*Power(t1,2)*Power(t2,2) - 
                 16*Power(t1,3)*Power(t2,2) + 20*Power(t2,3) + 
                 52*t1*Power(t2,3) - 48*Power(t1,2)*Power(t2,3) - 
                 16*t1*Power(t2,4) + Power(s2,4)*(2 - (-3 + t1)*t2) + 
                 Power(s1,4)*
                  (-30 + 12*Power(s2,2) + t1*(26 - 14*t2) + 13*t2 + 
                    3*Power(t2,2) + s2*(-7 - 12*t1 + 9*t2)) + 
                 Power(s2,3)*
                  (-35 + Power(t1,2)*(3 - 2*t2) - 54*t2 - 
                    24*Power(t2,2) + t1*(8 + 9*t2 + 13*Power(t2,2))) + 
                 Power(s1,3)*
                  (-109 + 14*Power(s2,3) + 65*t2 - 35*Power(t2,2) - 
                    6*Power(t2,3) - Power(s2,2)*(71 + 7*t1 + 14*t2) + 
                    Power(t1,2)*(-24 + 22*t2) + 
                    s2*(75 + 2*Power(t1,2) - 7*t1*(-3 + t2) + 
                       93*t2 - 12*Power(t2,2)) + 
                    t1*(31 - 71*t2 + 33*Power(t2,2))) + 
                 Power(s2,2)*
                  (-19 + 48*t2 + 135*Power(t2,2) + 21*Power(t2,3) + 
                    Power(t1,3)*(-5 + 3*t2) + 
                    Power(t1,2)*(-31 + 5*t2 - 18*Power(t2,2)) + 
                    t1*(131 - 25*t2 + 12*Power(t2,2) - 21*Power(t2,3))\
) + s2*(187 + 2*Power(t1,4) - 173*t2 - 14*Power(t2,2) - 
                    87*Power(t2,3) - 2*Power(t2,4) + 
                    Power(t1,3)*(27 - 20*t2 + 11*Power(t2,2)) + 
                    Power(t1,2)*
                     (-58 + 46*t2 - 25*Power(t2,2) + 24*Power(t2,3)) \
+ t1*(-126 - 21*t2 - 79*Power(t2,2) + 21*Power(t2,3) + 5*Power(t2,4))) \
+ Power(s1,2)*(145 + 2*Power(s2,4) + 
                    Power(s2,3)*(-57 + 8*t1 - 23*t2) + 108*t2 - 
                    4*Power(t1,3)*t2 + 14*Power(t2,2) + 
                    20*Power(t2,3) + 3*Power(t2,4) + 
                    Power(t1,2)*(43 + 3*t2 - 45*Power(t2,2)) + 
                    Power(s2,2)*
                     (164 - 5*Power(t1,2) + t1*(14 - 13*t2) + 
                       214*t2 + 4*Power(t2,2)) + 
                    t1*(-72 + 64*t2 + 53*Power(t2,2) - 
                       30*Power(t2,3)) + 
                    s2*(-323 + 4*Power(t1,3) - 105*t2 - 
                       172*Power(t2,2) + 6*Power(t2,3) + 
                       Power(t1,2)*(-27 + 31*t2) + 
                       t1*(59 - 133*t2 + 50*Power(t2,2)))) + 
                 s1*(192 + Power(s2,4)*(-5 + t1 - 2*t2) - 283*t2 + 
                    7*Power(t2,2) - 49*Power(t2,3) + 2*Power(t2,4) + 
                    Power(t1,3)*(-22 + 13*t2 + 5*Power(t2,2)) + 
                    Power(t1,2)*
                     (78 - 149*t2 + 72*Power(t2,2) + 24*Power(t2,3)) + 
                    t1*(-240 + 195*t2 - 203*Power(t2,2) + 
                       17*Power(t2,3) + 11*Power(t2,4)) + 
                    Power(s2,3)*
                     (61 + 3*Power(t1,2) + 89*t2 + 11*Power(t2,2) - 
                       t1*(5 + 19*t2)) - 
                    Power(s2,2)*
                     (224 + 4*Power(t1,3) + 173*t2 + 
                       173*Power(t2,2) - 7*Power(t1,2)*(-2 + 3*t2) + 
                       t1*(-59 + 66*t2 - 40*Power(t2,2))) + 
                    s2*(71 + Power(t1,3)*(23 - 15*t2) + 191*t2 + 
                       213*Power(t2,2) + 75*Power(t2,3) - 
                       3*Power(t2,4) + 
                       Power(t1,2)*(-68 + 92*t2 - 59*Power(t2,2)) + 
                       t1*(150 + 12*t2 + 87*Power(t2,2) - 
                        39*Power(t2,3))))) + 
              Power(s,2)*(50 - 57*t1 - 32*Power(t1,2) + 
                 35*Power(t1,3) - 8*Power(t1,4) - 190*t2 + 114*t1*t2 + 
                 75*Power(t1,2)*t2 - 23*Power(t1,3)*t2 - 
                 6*Power(t1,4)*t2 + 164*Power(t2,2) - 
                 170*t1*Power(t2,2) + 36*Power(t1,3)*Power(t2,2) - 
                 35*Power(t2,3) + 78*t1*Power(t2,3) + 
                 57*Power(t1,2)*Power(t2,3) - 
                 24*Power(t1,3)*Power(t2,3) + 17*Power(t2,4) + 
                 21*t1*Power(t2,4) - 32*Power(t1,2)*Power(t2,4) - 
                 6*Power(t2,5) - 4*t1*Power(t2,5) - 
                 Power(s2,4)*(4 + 3*t2)*(-2 + (-3 + t1)*t2) - 
                 Power(s1,5)*
                  (-18 + s2 + 8*Power(s2,2) + 14*t1 - 8*s2*t1 + 
                    3*t2 + 5*s2*t2 - 6*t1*t2 + Power(t2,2)) + 
                 Power(s1,4)*
                  (75 - 18*Power(s2,3) + Power(t1,2)*(26 - 18*t2) - 
                    59*t2 + 10*Power(t2,2) + 3*Power(t2,3) + 
                    Power(s2,2)*(45 + 19*t1 + 8*t2) + 
                    t1*(-37 + 55*t2 - 15*Power(t2,2)) - 
                    s2*(41 + 8*Power(t1,2) + t1*(27 - 5*t2) + 30*t2 - 
                       7*Power(t2,2))) + 
                 Power(s2,3)*
                  (46 - 22*t2 - 91*Power(t2,2) - 24*Power(t2,3) + 
                    Power(t1,2)*(4 + 17*t2) + 
                    t1*(-54 - 23*t2 + 6*Power(t2,2) + 15*Power(t2,3))) \
+ Power(s2,2)*(-119 + 26*t2 - 4*Power(t2,2) + 109*Power(t2,3) + 
                    7*Power(t2,4) + 
                    Power(t1,3)*(-12 - 19*t2 + 3*Power(t2,2)) + 
                    Power(t1,2)*
                     (69 - 20*t2 + 13*Power(t2,2) - 26*Power(t2,3)) + 
                    t1*(-34 + 153*t2 + 51*Power(t2,2) + 
                       10*Power(t2,3) - 11*Power(t2,4))) + 
                 s2*(23 + 202*t2 - 100*Power(t2,2) - 58*Power(t2,3) - 
                    30*Power(t2,4) + Power(t1,4)*(8 + 6*t2) + 
                    Power(t1,3)*
                     (-19 + 39*t2 - 31*Power(t2,2) + 15*Power(t2,3)) \
+ Power(t1,2)*(-49 - 64*t2 - 33*Power(t2,2) + 7*Power(t2,3) + 
                       14*Power(t2,4)) + 
                    t1*(109 - 155*t2 + 50*Power(t2,2) - 
                       115*Power(t2,3) + 22*Power(t2,4) + Power(t2,5))\
) - Power(s1,3)*(73 + 6*Power(s2,4) + 
                    Power(s2,3)*(-85 + 6*t1 - 39*t2) + 129*t2 - 
                    6*Power(t1,3)*t2 - 41*Power(t2,2) + 
                    5*Power(t2,3) + 3*Power(t2,4) + 
                    Power(t1,2)*(27 + 43*t2 - 51*Power(t2,2)) - 
                    t1*(42 + 21*t2 - 92*Power(t2,2) + 
                       15*Power(t2,3)) + 
                    Power(s2,2)*
                     (202 - 11*Power(t1,2) + 175*t2 - 
                       6*Power(t2,2) + t1*(48 + 22*t2)) + 
                    s2*(-285 + 6*Power(t1,3) - 26*t2 - 
                       111*Power(t2,2) + Power(t1,2)*(-19 + 11*t2) + 
                       t1*(15 - 154*t2 + 49*Power(t2,2)))) - 
                 Power(s1,2)*
                  (272 + Power(s2,4)*(-23 + 3*t1 - 12*t2) - 339*t2 - 
                    14*Power(t2,2) - 13*Power(t2,3) + 8*Power(t2,4) - 
                    Power(t2,5) + 
                    3*Power(t1,3)*(-14 + 5*t2 + 5*Power(t2,2)) + 
                    Power(s2,3)*
                     (131 + 3*Power(t1,2) + t1*(19 - 21*t2) + 
                       198*t2 + 30*Power(t2,2)) + 
                    Power(t1,2)*
                     (76 - 162*t2 + 21*Power(t2,2) + 51*Power(t2,3)) \
+ t1*(-258 + 290*t2 - 196*Power(t2,2) - 52*Power(t2,3) + 
                       9*Power(t2,4)) - 
                    Power(s2,2)*
                     (319 + 6*Power(t1,3) + 
                       Power(t1,2)*(46 - 42*t2) + 236*t2 + 
                       249*Power(t2,2) - 10*Power(t2,3) + 
                       t1*(-20 + 185*t2 - 21*Power(t2,2))) + 
                    s2*(-68 + 378*t2 + 145*Power(t2,2) + 
                       101*Power(t2,3) + Power(t2,4) - 
                       9*Power(t1,3)*(-5 + 3*t2) - 
                       3*Power(t1,2)*(16 - 43*t2 + 22*Power(t2,2)) + 
                       t1*(213 - 17*t2 + 215*Power(t2,2) - 
                        61*Power(t2,3)))) + 
                 s1*(162 + 6*Power(t1,4) + 46*t2 - 219*Power(t2,2) + 
                    19*Power(t2,3) - 7*Power(t2,4) + 6*Power(t2,5) + 
                    Power(s2,4)*
                     (-26 + 4*t1 - 32*t2 + 6*t1*t2 - 6*Power(t2,2)) + 
                    Power(t1,3)*
                     (17 - 91*t2 + 43*Power(t2,2) + 9*Power(t2,3)) + 
                    Power(t1,2)*
                     (-123 + 110*t2 - 226*Power(t2,2) + 
                       74*Power(t2,3) + 18*Power(t2,4)) + 
                    t1*(-12 - 67*t2 + 204*Power(t2,2) - 
                       205*Power(t2,3) + 3*Power(t2,4) + 3*Power(t2,5)\
) + Power(s2,3)*(92 + 142*t2 + 145*Power(t2,2) + 9*Power(t2,3) + 
                       Power(t1,2)*(-13 + 3*t2) + 
                       t1*(17 + 21*t2 - 30*Power(t2,2))) + 
                    Power(s2,2)*
                     (146 + Power(t1,3)*(15 - 9*t2) - 262*t2 - 
                       205*Power(t2,2) - 118*Power(t2,3) + 
                       4*Power(t2,4) + 
                       Power(t1,2)*(32 - 76*t2 + 57*Power(t2,2)) + 
                       t1*(-265 + 17*t2 - 149*Power(t2,2) + 
                        35*Power(t2,3))) + 
                    s2*(-548 - 6*Power(t1,4) + 389*t2 + 
                       45*Power(t2,2) + 198*Power(t2,3) + 
                       21*Power(t2,4) - Power(t2,5) + 
                       Power(t1,3)*(-33 + 85*t2 - 36*Power(t2,2)) + 
                       Power(t1,2)*
                        (76 + 36*t2 + 93*Power(t2,2) - 61*Power(t2,3)) \
+ t1*(367 - 214*t2 + 237*Power(t2,2) + 54*Power(t2,3) - 26*Power(t2,4))\
))) + s*(72 - 42*t1 - 40*Power(t1,2) + 2*Power(t1,3) + 8*Power(t1,4) - 
                 2*t2 - 34*t1*t2 + 38*Power(t1,2)*t2 + 
                 14*Power(t1,3)*t2 - 16*Power(t1,4)*t2 - 
                 213*Power(t2,2) + 226*t1*Power(t2,2) - 
                 74*Power(t1,2)*Power(t2,2) + 
                 29*Power(t1,3)*Power(t2,2) - 
                 6*Power(t1,4)*Power(t2,2) + 174*Power(t2,3) - 
                 214*t1*Power(t2,3) + 39*Power(t1,2)*Power(t2,3) + 
                 19*Power(t1,3)*Power(t2,3) - 31*Power(t2,4) + 
                 58*t1*Power(t2,4) + 23*Power(t1,2)*Power(t2,4) - 
                 16*Power(t1,3)*Power(t2,4) + 2*Power(t2,5) - 
                 8*Power(t1,2)*Power(t2,5) - 2*Power(t2,6) - 
                 Power(s2,4)*(-2 + (-3 + t1)*t2)*
                  (-4 + 8*t2 + 3*Power(t2,2)) + 
                 Power(s1,6)*
                  (-4 + 2*Power(s2,2) - t1*(-3 + t2) + 
                    s2*(1 - 2*t1 + t2)) + 
                 Power(s1,5)*
                  (-21 + 10*Power(s2,3) - 3*Power(s2,2)*(3 + 5*t1) + 
                    7*Power(t1,2)*(-2 + t2) + 16*t2 + Power(t2,2) + 
                    s2*(3 + 7*Power(t1,2) + t1*(13 - 5*t2) + t2 - 
                       Power(t2,2)) + t1*(20 - 15*t2 + 2*Power(t2,2))) \
+ Power(s2,3)*(8 + 26*t2 + 35*Power(t2,2) - 52*Power(t2,3) - 
                    8*Power(t2,4) + 
                    Power(t1,2)*
                     (-4 + 25*Power(t2,2) + 2*Power(t2,3)) + 
                    t1*(20 - 54*t2 - 64*Power(t2,2) - 3*Power(t2,3) + 
                       7*Power(t2,4))) + 
                 Power(s2,2)*
                  (62 - 160*t2 - 19*Power(t2,2) + 28*Power(t2,3) + 
                    37*Power(t2,4) + 
                    Power(t1,3)*
                     (12 - 20*t2 - 23*Power(t2,2) + Power(t2,3)) + 
                    Power(t1,2)*
                     (-2 + 48*t2 + 49*Power(t2,2) + 11*Power(t2,3) - 
                       15*Power(t2,4)) + 
                    t1*(-64 + 40*t2 + 9*Power(t2,2) + 53*Power(t2,3) - 
                       3*Power(t2,4) - 2*Power(t2,5))) + 
                 s2*(-174 + 276*t2 - 43*Power(t2,2) - 9*Power(t2,3) - 
                    44*Power(t2,4) + 2*Power(t2,5) + 
                    2*Power(t1,4)*(-4 + 8*t2 + 3*Power(t2,2)) + 
                    Power(t1,3)*
                     (-18 + 14*t2 - 5*Power(t2,2) - 18*Power(t2,3) + 
                       9*Power(t2,4)) + 
                    Power(t1,2)*
                     (46 - 70*t2 - 44*Power(t2,2) - 48*Power(t2,3) + 
                       21*Power(t2,4) + 3*Power(t2,5)) + 
                    t1*(130 - 156*t2 + 146*Power(t2,2) + 
                       3*Power(t2,3) - 56*Power(t2,4) + 7*Power(t2,5))) \
+ Power(s1,4)*(21 + 6*Power(s2,4) + 42*t2 - 4*Power(t1,3)*t2 - 
                    20*Power(t2,2) - 5*Power(t2,3) - 
                    Power(s2,3)*(51 + 25*t2) + 
                    Power(t1,2)*(3 + 41*t2 - 23*Power(t2,2)) + 
                    t1*(6 - 40*t2 + 37*Power(t2,2)) + 
                    Power(s2,2)*
                     (112 - 8*Power(t1,2) + 45*t2 - 8*Power(t2,2) + 
                       t1*(39 + 32*t2)) + 
                    s2*(4*Power(t1,3) - Power(t1,2)*(5 + 8*t2) + 
                       t1*(-28 - 71*t2 + 25*Power(t2,2)) - 
                       3*(39 - 8*t2 + 7*Power(t2,2) + Power(t2,3)))) + 
                 Power(s1,3)*
                  (172 + Power(s2,4)*(-31 + 3*t1 - 18*t2) - 213*t2 + 
                    8*Power(t2,2) + 10*Power(t2,3) + 9*Power(t2,4) + 
                    Power(t1,3)*(-34 + 7*t2 + 15*Power(t2,2)) + 
                    Power(s2,3)*
                     (119 + Power(t1,2) - t1*(-41 + t2) + 145*t2 + 
                       21*Power(t2,2)) + 
                    Power(t1,2)*
                     (74 - 41*t2 - 46*Power(t2,2) + 30*Power(t2,3)) - 
                    t1*(148 - 151*t2 + 56*Power(t2,2) + 
                       40*Power(t2,3) + 2*Power(t2,4)) - 
                    Power(s2,2)*
                     (182 + 4*Power(t1,3) + 
                       Power(t1,2)*(50 - 33*t2) + 173*t2 + 
                       108*Power(t2,2) - 10*Power(t2,3) + 
                       t1*(17 + 152*t2 + 7*Power(t2,2))) + 
                    s2*(-109 + Power(t1,3)*(37 - 21*t2) + 299*t2 + 
                       4*Power(t2,2) + 32*Power(t2,3) + 
                       5*Power(t2,4) + 
                       Power(t1,2)*(-44 + 70*t2 - 27*Power(t2,2)) - 
                       2*t1*(-70 + 19*t2 - 80*Power(t2,2) + 
                        17*Power(t2,3)))) - 
                 Power(s1,2)*
                  (113 + 6*Power(t1,4) + 160*t2 - 320*Power(t2,2) + 
                    52*Power(t2,3) + 6*Power(t2,4) + 7*Power(t2,5) + 
                    Power(t1,3)*
                     (-35 - 113*t2 + 38*Power(t2,2) + 18*Power(t2,3)) \
+ Power(t1,2)*(-36 + 161*t2 - 152*Power(t2,2) - 9*Power(t2,3) + 
                       19*Power(t2,4)) - 
                    t1*(-6 + 150*t2 - 306*Power(t2,2) + 
                       152*Power(t2,3) + 14*Power(t2,4) + Power(t2,5)) \
+ Power(s2,4)*(-38 - 71*t2 - 18*Power(t2,2) + t1*(8 + 9*t2)) + 
                    Power(s2,3)*
                     (75 - 17*Power(t1,2) + 150*t2 + 
                       161*Power(t2,2) + 7*Power(t2,3) + 
                       t1*(42 + 101*t2 - 9*Power(t2,2))) - 
                    Power(s2,2)*
                     (-209 + 252*t2 + 171*Power(t2,2) + 
                       101*Power(t2,3) - 6*Power(t2,4) + 
                       3*Power(t1,3)*(-5 + 3*t2) + 
                       Power(t1,2)*(25 + 145*t2 - 57*Power(t2,2)) + 
                       t1*(167 + 13*t2 + 188*Power(t2,2) - 
                        24*Power(t2,3))) + 
                    s2*(-489 - 6*Power(t1,4) + 233*t2 + 
                       175*Power(t2,2) + 94*Power(t2,3) + 
                       10*Power(t2,4) + 2*Power(t2,5) + 
                       Power(t1,3)*(15 + 110*t2 - 39*Power(t2,2)) + 
                       Power(t1,2)*
                        (54 + 62*t2 + 84*Power(t2,2) - 53*Power(t2,3)) \
+ t1*(246 - 257*t2 + 104*Power(t2,2) + 128*Power(t2,3) - 
                        23*Power(t2,4)))) + 
                 s1*(-222 + 494*t2 - 202*Power(t2,2) - 97*Power(t2,3) + 
                    21*Power(t2,4) + 6*Power(t2,5) + 2*Power(t2,6) + 
                    4*Power(t1,4)*(4 + 3*t2) + 
                    Power(t1,3)*
                     (-22 - 54*t2 - 102*Power(t2,2) + 47*Power(t2,3) + 
                       7*Power(t2,4)) + 
                    t1*(286 - 486*t2 + 240*Power(t2,2) + 
                       87*Power(t2,3) - 76*Power(t2,4) + Power(t2,5)) + 
                    Power(t1,2)*
                     (-82 + 142*t2 + 56*Power(t2,2) - 
                       141*Power(t2,3) + 18*Power(t2,4) + 5*Power(t2,5)\
) + Power(s2,4)*(4 - 68*t2 - 49*Power(t2,2) - 6*Power(t2,3) + 
                       t1*(-4 + 16*t2 + 9*Power(t2,2))) + 
                    Power(s2,3)*
                     (-114 + 104*t2 + 75*Power(t2,2) + 
                       75*Power(t2,3) + Power(t2,4) - 
                       Power(t1,2)*(4 + 42*t2 + 3*Power(t2,2)) + 
                       t1*(74 + 98*t2 + 63*Power(t2,2) - 
                        15*Power(t2,3))) - 
                    Power(s2,2)*
                     (-256 + 10*t2 + 18*Power(t2,2) + 
                       155*Power(t2,3) + 29*Power(t2,4) - 
                       2*Power(t2,5) + 
                       Power(t1,3)*(-24 - 38*t2 + 6*Power(t2,2)) + 
                       Power(t1,2)*
                        (84 + 60*t2 + 106*Power(t2,2) - 
                        47*Power(t2,3)) + 
                       t1*(40 + 80*t2 + 65*Power(t2,2) + 
                        72*Power(t2,3) - 16*Power(t2,4))) + 
                    s2*(64 - 588*t2 + 355*Power(t2,2) + 
                       41*Power(t2,3) + 61*Power(t2,4) - 
                       3*Power(t2,5) - 4*Power(t1,4)*(4 + 3*t2) + 
                       Power(t1,3)*
                        (-6 + 14*t2 + 91*Power(t2,2) - 31*Power(t2,3)) \
- 2*Power(t1,2)*(-91 + 40*t2 - 91*Power(t2,2) + Power(t2,3) + 
                        14*Power(t2,4)) + 
                       t1*(-280 + 482*t2 - 504*Power(t2,2) + 
                         238*Power(t2,3) + 19*Power(t2,4) - 
                         7*Power(t2,5))))))*Log(s - s2 + t1))/
          ((-1 + s1)*(-1 + s2)*Power(1 - s + s2 - t1,3)*(-1 + t1)*
            (-1 + t2)) + 2*Power(s - s2 + t1,2)*(s - s1 + t2)*
          Power(-s1 + s2 - t1 + t2,3)*
          ((2*(-32 + Power(s,5) - 24*s2 + 
                 2*Power(s1,3)*(12 + 5*s2 - 6*t1) + 24*t1 + 60*t2 + 
                 54*s2*t2 - 58*t1*t2 - 10*Power(t2,2) - 
                 25*s2*Power(t2,2) + 27*t1*Power(t2,2) - 
                 24*Power(t2,3) - 9*s2*Power(t2,3) + 
                 11*t1*Power(t2,3) + 
                 Power(s,4)*(12 - 3*s1 - s2 + t1 + 3*t2) + 
                 Power(s1,2)*
                  (-10 + 17*t1 - 72*t2 + 35*t1*t2 - s2*(15 + 29*t2)) + 
                 s1*(-60 + 20*t2 + 72*Power(t2,2) + 
                    t1*(50 - 44*t2 - 34*Power(t2,2)) + 
                    s2*(-46 + 40*t2 + 28*Power(t2,2))) + 
                 Power(s,3)*(1 + 3*Power(s1,2) - 10*s2 + 12*t1 + 
                    35*t2 - 3*s2*t2 + 3*t1*t2 + 3*Power(t2,2) + 
                    3*s1*(s2 - t1 - 2*(6 + t2))) + 
                 Power(s,2)*(-68 - Power(s1,3) - 23*s2 + 25*t1 - 
                    20*t2 - 29*s2*t2 + 35*t1*t2 + 34*Power(t2,2) - 
                    3*s2*Power(t2,2) + 3*t1*Power(t2,2) + 
                    Power(t2,3) + 3*Power(s1,2)*(12 - s2 + t1 + t2) + 
                    s1*(30 - 70*t2 - 3*Power(t2,2) + 6*s2*(5 + t2) - 
                       6*t1*(6 + t2))) + 
                 s*(84 + Power(s1,3)*(-12 + s2 - t1) - 58*t1 - 78*t2 + 
                    52*t1*t2 - 45*Power(t2,2) + 34*t1*Power(t2,2) + 
                    11*Power(t2,3) + t1*Power(t2,3) - 
                    s2*(-54 + 48*t2 + 28*Power(t2,2) + Power(t2,3)) + 
                    Power(s1,2)*
                     (-55 + 35*t2 - 3*s2*(10 + t2) + 3*t1*(12 + t2)) + 
                    s1*(70 + 100*t2 - 34*Power(t2,2) + 
                       s2*(38 + 58*t2 + 3*Power(t2,2)) - 
                       t1*(42 + 70*t2 + 3*Power(t2,2))))))/
             ((s - s2 + t1)*(s - s1 + t2)) + 
            (-48 - 16*s2 - 2*Power(s,4)*(-9 + t1) - 
               2*Power(s1,4)*(-3 + t1) + 16*t1 + 98*t2 + 12*s2*t2 - 
               10*t1*t2 - 61*Power(t2,2) + 8*s2*Power(t2,2) - 
               9*t1*Power(t2,2) + 13*Power(t2,3) - s2*Power(t2,3) + 
               2*t1*Power(t2,3) + 2*Power(t2,4) - 2*t1*Power(t2,4) + 
               Power(s,3)*(-20 - 10*s2 + 11*t1 + s1*(-60 + 8*t1) + 
                  47*t2 - 8*t1*t2) + 
               Power(s1,3)*(6 + s2 - 20*t2 + t1*(-11 + 8*t2)) + 
               Power(s1,2)*(-67 + s2*(8 - 3*t2) + t2 + 
                  24*Power(t2,2) - 3*t1*(5 - 8*t2 + 4*Power(t2,2))) + 
               s1*(s2*(-12 - 16*t2 + 3*Power(t2,2)) - 
                  2*(57 - 64*t2 + 10*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*(10 + 24*t2 - 15*Power(t2,2) + 8*Power(t2,3))) + 
               Power(s,2)*(-12*Power(s1,2)*(-6 + t1) - 
                  7*s2*(-2 + 3*t2) + 
                  s1*(46 + 21*s2 - 33*t1 - 114*t2 + 24*t1*t2) - 
                  3*(7*(3 + t2 - 2*Power(t2,2)) + 
                     t1*(5 - 8*t2 + 4*Power(t2,2)))) + 
               s*(114 + 12*s2 - 10*t1 + 4*Power(s1,3)*(-9 + 2*t1) - 
                  124*t2 + 22*s2*t2 - 24*t1*t2 + 12*Power(t2,2) - 
                  12*s2*Power(t2,2) + 15*t1*Power(t2,2) + 
                  15*Power(t2,3) - 8*t1*Power(t2,3) + 
                  Power(s1,2)*(-32 - 12*s2 + t1*(33 - 24*t2) + 87*t2) + 
                  2*s1*(65 + 10*t2 - 33*Power(t2,2) + 
                     s2*(-11 + 12*t2) + 3*t1*(5 - 8*t2 + 4*Power(t2,2)))\
))/((-1 + s1)*(-s + s1 - t2)) - 
            (2*(24 + 28*s2 + Power(s1,3)*(4 + s2 - t1) - 20*t1 - 
                 4*s2*t1 + 4*Power(t1,2) - 28*t2 + Power(s,4)*t2 - 
                 32*s2*t2 - 4*Power(s2,2)*t2 + 12*t1*t2 + 10*s2*t1*t2 - 
                 6*Power(t1,2)*t2 - s2*Power(t2,2) + 
                 4*Power(s2,2)*Power(t2,2) + 17*t1*Power(t2,2) - 
                 4*s2*t1*Power(t2,2) - 4*Power(t2,3) + 
                 2*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) - 
                 2*t1*Power(t2,3) - 2*s2*t1*Power(t2,3) + 
                 Power(t1,2)*Power(t2,3) + 
                 Power(s1,2)*
                  (15*t1 + s2*(1 - 2*t1*(-2 + t2)) + 
                    Power(t1,2)*(-4 + t2) - 12*t2 + Power(s2,2)*t2) - 
                 Power(s,3)*(-7 + s1 - 2*t1*(-2 + t2) - 2*t2 + 
                    2*s1*t2 + 2*s2*t2 - 2*Power(t2,2)) + 
                 Power(s,2)*(16 + 5*s2 + 9*t1 + 4*s2*t1 - 
                    4*Power(t1,2) + 12*t2 - 6*s2*t2 + Power(s2,2)*t2 - 
                    2*t1*t2 - 2*s2*t1*t2 + Power(t1,2)*t2 - 
                    4*s2*Power(t2,2) + 4*t1*Power(t2,2) + 
                    Power(t2,3) + Power(s1,2)*(2 + t2) + 
                    s1*(s2 + t1*(7 - 4*t2) + 4*s2*t2 - 
                       2*(7 + t2 + Power(t2,2)))) + 
                 s1*(-2*Power(s2,2)*t2*(2 + t2) + 
                    Power(t1,2)*(2 + 4*t2 - 2*Power(t2,2)) + 
                    4*(7 + 3*Power(t2,2)) + 
                    t1*(-12 - 32*t2 + 3*Power(t2,2)) + 
                    s2*(32 - 3*Power(t2,2) + t1*(-2 + 4*Power(t2,2)))) \
- s*(48 + Power(s1,3) - 20*t1 + 2*Power(t1,2) - 12*t2 - 18*t1*t2 + 
                    4*Power(t1,2)*t2 - 5*Power(t2,2) - 
                    2*Power(t1,2)*Power(t2,2) + 2*Power(t2,3) - 
                    2*t1*Power(t2,3) - 2*Power(s2,2)*t2*(2 + t2) + 
                    Power(s1,2)*(-3 - 2*t1*(-1 + t2) + 2*s2*(1 + t2)) + 
                    2*s2*(18 - 6*t2 + 2*Power(t2,2) + Power(t2,3) + 
                       t1*(-1 + 2*Power(t2,2))) + 
                    s1*(12 + 2*Power(t1,2)*(-4 + t2) + 8*t2 + 
                       2*Power(s2,2)*t2 - 3*Power(t2,2) + 
                       s2*(6 - 4*t1*(-2 + t2) - 6*t2 - 4*Power(t2,2)) + 
                       t1*(24 - 2*t2 + 4*Power(t2,2))))))/
             ((-1 + s2)*(-s + s2 - t1)) + 
            (2*(2*Power(s,4) - 12*s2 - 8*Power(s2,2) + 8*t1 + 8*s2*t1 + 
                 4*Power(s2,2)*t1 - 8*Power(t1,2) - 4*s2*Power(t1,2) + 
                 8*t2 + 12*s2*t2 + 4*Power(s2,2)*t2 - 10*t1*t2 + 
                 4*s2*t1*t2 - 4*Power(s2,2)*t1*t2 + 10*Power(t1,2)*t2 + 
                 4*s2*Power(t1,2)*t2 - 16*Power(t2,2) - 
                 3*s2*Power(t2,2) + 6*Power(s2,2)*Power(t2,2) + 
                 t1*Power(t2,2) - 16*s2*t1*Power(t2,2) - 
                 Power(s2,2)*t1*Power(t2,2) - Power(t1,2)*Power(t2,2) + 
                 s2*Power(t1,2)*Power(t2,2) + 6*Power(t2,3) + 
                 7*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) - 
                 2*s2*t1*Power(t2,3) + 2*Power(t2,4) + s2*Power(t2,4) - 
                 Power(s1,4)*(1 + s2 - t1 + t2) + 
                 Power(s,3)*(5*t1 - Power(t1,2) + s2*(-3 + t1 - t2) + 
                    6*t2 + 2*t1*t2 - Power(t2,2) + 
                    s1*(-4 + s2 - t1 + t2)) + 
                 Power(s1,3)*
                  (-1 - Power(s2,2) + Power(t1,2) + 4*t2 + 
                    3*Power(t2,2) + 2*s2*(1 + t2) - 2*t1*(5 + 2*t2)) + 
                 Power(s,2)*(2 + 7*t1 - 5*Power(t1,2) + 
                    Power(s1,2)*(1 - 3*s2 + 3*t1 - 3*t2) - 2*t2 + 
                    18*t1*t2 - 2*Power(t1,2)*t2 + 4*Power(t2,2) + 
                    4*t1*Power(t2,2) - 2*Power(t2,3) + 
                    Power(s2,2)*(2 - t1 + t2) - 
                    s1*(-3 + Power(s2,2) + 20*t1 - 3*Power(t1,2) + 
                       2*s2*(-4 + t1 - 2*t2) + 6*t2 + 8*t1*t2 - 
                       5*Power(t2,2)) + 
                    s2*(-9 - 4*t1 + Power(t1,2) - 7*t2 - Power(t2,2))) \
- Power(s1,2)*(16 - 2*t2 + 3*Power(t2,2) + 3*Power(t2,3) + 
                    Power(t1,2)*(5 + 2*t2) + 
                    t1*(7 - 24*t2 - 5*Power(t2,2)) + 
                    Power(s2,2)*(t1 - 3*(2 + t2)) + 
                    s2*(13 - Power(t1,2) - 3*t2 + 2*t1*(4 + t2))) + 
                 s1*(-12 + 24*t2 - 7*Power(t2,2) - 2*Power(t2,3) + 
                    Power(t2,4) + 
                    Power(t1,2)*(-14 + 6*t2 + Power(t2,2)) - 
                    2*t1*(-5 - 5*t2 + 7*Power(t2,2) + Power(t2,3)) + 
                    Power(s2,2)*
                     (-4 - 12*t2 - 3*Power(t2,2) + 2*t1*(2 + t2)) - 
                    2*s2*(14 - 8*t2 + 6*Power(t2,2) + Power(t2,3) + 
                       Power(t1,2)*(2 + t2) - 
                       2*t1*(1 + 6*t2 + Power(t2,2)))) + 
                 s*(-4 - 18*t1 + 14*Power(t1,2) - 6*t2 - 
                    6*Power(t1,2)*t2 + 8*Power(t2,2) + 
                    13*t1*Power(t2,2) - Power(t1,2)*Power(t2,2) + 
                    2*Power(t2,3) + 2*t1*Power(t2,3) - Power(t2,4) - 
                    2*Power(s2,2)*(-2 + t1 - t2)*(2 + t2) + 
                    Power(s1,3)*(2 + 3*s2 - 3*t1 + 3*t2) + 
                    Power(s1,2)*
                     (-2 + 2*Power(s2,2) - 3*Power(t1,2) + 
                       s2*(-7 + t1 - 5*t2) - 4*t2 - 7*Power(t2,2) + 
                       5*t1*(5 + 2*t2)) + 
                    s2*(24 - 4*t2 + 3*Power(t2,2) + Power(t2,3) + 
                       2*Power(t1,2)*(2 + t2) - 
                       t1*(8 + 20*t2 + 3*Power(t2,2))) + 
                    s1*(10 + 4*t2 + 5*Power(t2,3) + 
                       2*Power(t1,2)*(5 + 2*t2) - 3*t1*t2*(14 + 3*t2) + 
                       2*Power(s2,2)*(t1 - 2*(2 + t2)) + 
                       s2*(22 - 2*Power(t1,2) + 4*t2 + Power(t2,2) + 
                         2*t1*(6 + t2))))))/((-1 + t1)*(-1 + t2)) + 
            (-12 - 12*s2 + 8*Power(s2,2) - 20*t1 - 4*s2*t1 - 
               8*Power(t1,2) - Power(s1,4)*(-24 + s2 + t1) + 44*t2 + 
               20*s2*t2 - 12*Power(s2,2)*t2 + 52*t1*t2 + 8*s2*t1*t2 + 
               8*Power(t1,2)*t2 - 53*Power(t2,2) - 5*s2*Power(t2,2) + 
               2*Power(s2,2)*Power(t2,2) - 37*t1*Power(t2,2) - 
               5*s2*t1*Power(t2,2) + 2*Power(t1,2)*Power(t2,2) + 
               18*Power(t2,3) - 4*s2*Power(t2,3) + 
               Power(s2,2)*Power(t2,3) + t1*Power(t2,3) + 
               3*Power(t2,4) - s2*Power(t2,4) + 2*Power(s,4)*(1 + t2) + 
               Power(s,3)*(10 + s1*(-20 + t1 - 7*t2) + 19*t2 - 
                  3*s2*t2 + 5*Power(t2,2) + t1*(-5 + s2 + t2)) + 
               Power(s1,3)*(-17 + Power(s2,2) - 65*t2 + t1*(20 + t2) + 
                  s2*(5 - 3*t1 + 6*t2)) - 
               Power(s,2)*(37 + 5*t1 - 2*Power(t1,2) + 
                  Power(s1,2)*(-58 + s2 + 3*t1 - 8*t2) - 
                  Power(s2,2)*(-2 + t2) - 10*t2 + 5*t1*t2 - 
                  31*Power(t2,2) - 2*t1*Power(t2,2) - 4*Power(t2,3) - 
                  s1*(-25 + s2 + Power(s2,2) + 30*t1 - 5*s2*t1 - 
                     87*t2 + 10*s2*t2 - t1*t2 - 12*Power(t2,2)) + 
                  s2*(9 + t1 + 12*t2 - 2*t1*t2 + 7*Power(t2,2))) - 
               Power(s1,2)*(69 - 2*Power(t1,2) - 40*t2 - 
                  61*Power(t2,2) + Power(s2,2)*(6 + t2) + 
                  t1*(17 + 31*t2 - Power(t2,2)) + 
                  s2*(9 + 22*t2 + 10*Power(t2,2) - t1*(7 + 6*t2))) - 
               s1*(48 - 106*t2 + 41*Power(t2,2) + 23*Power(t2,3) + 
                  4*Power(t1,2)*(2 + t2) + 
                  Power(s2,2)*(-4 - 4*t2 + Power(t2,2)) + 
                  t1*(48 - 62*t2 - 10*Power(t2,2) + Power(t2,3)) + 
                  s2*(t1*(-4 + 2*t2 + 3*Power(t2,2)) - 
                     3*(-8 + 2*t2 + 7*Power(t2,2) + 2*Power(t2,3)))) + 
               s*(36 + 28*t1 + 8*Power(t1,2) + 
                  Power(s1,3)*(-64 + 2*s2 + 3*t1 - 3*t2) - 74*t2 - 
                  46*t1*t2 + 4*Power(t1,2)*t2 + 22*Power(t2,2) + 
                  t1*Power(t2,2) + 17*Power(t2,3) + t1*Power(t2,3) + 
                  Power(t2,4) + 2*Power(s2,2)*(-4 + Power(t2,2)) - 
                  Power(s1,2)*
                   (-32 + 2*Power(s2,2) - 133*t2 - 7*Power(t2,2) + 
                     t1*(45 + t2) + s2*(6 - 7*t1 + 13*t2)) + 
                  s2*(20 - 6*t2 - 16*Power(t2,2) - 5*Power(t2,3) + 
                     t1*(4 - 6*t2 + Power(t2,2))) + 
                  s1*(90 + 8*Power(s2,2) - 4*Power(t1,2) - 34*t2 - 
                     86*Power(t2,2) - 5*Power(t2,3) + 
                     t1*(22 + 36*t2 - 3*Power(t2,2)) + 
                     2*s2*(11 + 15*t2 + 8*Power(t2,2) - t1*(3 + 4*t2))))\
)/((-1 + s1)*(-1 + t2)) + (-12 - 28*s2 - 24*Power(s2,2) + 
               2*Power(s,4)*(-2 + t1) + 24*t1 + 20*s2*t1 - 
               12*Power(t1,2) + Power(s1,4)*(-2 + s2 + t1) - 16*t2 + 
               8*s2*t2 + 40*Power(s2,2)*t2 - 20*s2*t1*t2 + 
               12*Power(t1,2)*t2 + 35*Power(t2,2) + 33*s2*Power(t2,2) - 
               16*Power(s2,2)*Power(t2,2) - 36*t1*Power(t2,2) - 
               3*s2*t1*Power(t2,2) + 3*Power(t1,2)*Power(t2,2) + 
               Power(t2,3) - 2*s2*Power(t2,3) - 
               Power(s2,2)*Power(t2,3) + t1*Power(t2,3) - 
               s2*t1*Power(t2,3) + 2*Power(t2,4) + 
               Power(s,3)*(8 + 3*t1 + Power(t1,2) - 7*t2 + 5*t1*t2 + 
                  s1*(14 - 7*t1 + t2) + s2*(14 - 3*t1 + t2)) - 
               Power(s1,3)*(1 + Power(s2,2) + Power(t1,2) + 
                  2*t1*(-1 + t2) - 5*t2 + s2*(9 - 4*t1 + 5*t2)) + 
               Power(s1,2)*(3 + Power(s2,2)*(-8 + t2) - t2 - 
                  2*Power(t2,2) + Power(t1,2)*(7 + 2*t2) + 
                  t1*(-12 - 7*t2 + Power(t2,2)) + 
                  s2*(1 + 24*t2 + 7*Power(t2,2) - 3*t1*(5 + 3*t2))) + 
               Power(s,2)*(-9 - 12*t1 + 7*Power(t1,2) + t2 + 3*t1*t2 + 
                  2*Power(t1,2)*t2 + 4*t1*Power(t2,2) - 
                  Power(s2,2)*(12 + t2) + 
                  Power(s1,2)*(s2 + 9*t1 - 2*(9 + t2)) - 
                  s1*(17 + Power(s2,2) + 3*Power(t1,2) - 23*t2 - 
                     2*Power(t2,2) + 4*t1*(1 + 3*t2) + 
                     s2*(33 - 10*t1 + 5*t2)) + 
                  s2*(-15 + 30*t2 + 2*Power(t2,2) - t1*(11 + 7*t2))) + 
               s1*(-16 - 42*t2 + Power(t2,2) - 3*Power(t2,3) - 
                  Power(t1,2)*(8 + 10*t2 + Power(t2,2)) + 
                  4*t1*(5 + 11*t2 + Power(t2,2)) + 
                  Power(s2,2)*(-32 + 24*t2 + Power(t2,2)) + 
                  s2*(-28 - 26*t2 - 13*Power(t2,2) - 3*Power(t2,3) + 
                     2*t1*(4 + 9*t2 + 3*Power(t2,2)))) + 
               s*(20 - 24*t1 + 8*Power(t1,2) + 38*t2 - 44*t1*t2 + 
                  10*Power(t1,2)*t2 - 6*Power(t2,2) + t1*Power(t2,2) + 
                  Power(t1,2)*Power(t2,2) + 5*Power(t2,3) + 
                  t1*Power(t2,3) + Power(s1,3)*(10 - 2*s2 - 5*t1 + t2) - 
                  2*Power(s2,2)*(-18 + 14*t2 + Power(t2,2)) + 
                  Power(s1,2)*
                   (10 + 2*Power(s2,2) + 3*Power(t1,2) - 21*t2 - 
                     2*Power(t2,2) + t1*(-1 + 9*t2) + 
                     s2*(28 - 11*t1 + 9*t2)) + 
                  s2*(40 + 14*t2 + 14*Power(t2,2) + Power(t2,3) - 
                     t1*(12 + 14*t2 + 5*Power(t2,2))) + 
                  s1*(6 + 20*Power(s2,2) - 4*t2 + 6*Power(t2,2) + 
                     Power(t2,3) - 2*Power(t1,2)*(7 + 2*t2) + 
                     t1*(24 + 4*t2 - 5*Power(t2,2)) + 
                     2*s2*(5 - 25*t2 - 4*Power(t2,2) + t1*(13 + 8*t2)))))/
             ((-1 + s2)*(-1 + t1)))*R1n(1 - s + s1 - t2,1 - s + s2 - t1)))/
     (Power(s - s2 + t1,3)*(-1 + s - s1 + t2)*Power(s - s1 + t2,3)) + 
    (4*((-24*s2*Power(t1,2) + 18*Power(s2,2)*Power(t1,2) + 
            6*Power(s2,3)*Power(t1,2) + 24*Power(t1,3) + 
            14*s2*Power(t1,3) - 33*Power(s2,2)*Power(t1,3) - 
            5*Power(s2,3)*Power(t1,3) - 56*Power(t1,4) + 
            65*s2*Power(t1,4) - 3*Power(s2,2)*Power(t1,4) - 
            6*Power(s2,3)*Power(t1,4) + 30*Power(t1,5) - 
            68*s2*Power(t1,5) + 33*Power(s2,2)*Power(t1,5) + 
            5*Power(s2,3)*Power(t1,5) + 12*Power(t1,6) + 
            3*s2*Power(t1,6) - 15*Power(s2,2)*Power(t1,6) - 
            10*Power(t1,7) + 10*s2*Power(t1,7) + 24*s2*t1*t2 - 
            18*Power(s2,2)*t1*t2 - 6*Power(s2,3)*t1*t2 + 
            154*s2*Power(t1,2)*t2 - 178*Power(s2,2)*Power(t1,2)*t2 + 
            6*Power(s2,3)*Power(t1,2)*t2 + 6*Power(s2,4)*Power(t1,2)*t2 - 
            190*Power(t1,3)*t2 - 56*s2*Power(t1,3)*t2 + 
            326*Power(s2,2)*Power(t1,3)*t2 - 
            33*Power(s2,3)*Power(t1,3)*t2 - 
            7*Power(s2,4)*Power(t1,3)*t2 + 427*Power(t1,4)*t2 - 
            556*s2*Power(t1,4)*t2 + 34*Power(s2,2)*Power(t1,4)*t2 + 
            45*Power(s2,3)*Power(t1,4)*t2 + Power(s2,4)*Power(t1,4)*t2 - 
            272*Power(t1,5)*t2 + 525*s2*Power(t1,5)*t2 - 
            215*Power(s2,2)*Power(t1,5)*t2 - 
            12*Power(s2,3)*Power(t1,5)*t2 - 13*Power(t1,6)*t2 - 
            47*s2*Power(t1,6)*t2 + 55*Power(s2,2)*Power(t1,6)*t2 + 
            44*Power(t1,7)*t2 - 44*s2*Power(t1,7)*t2 - 8*s2*Power(t2,2) + 
            6*Power(s2,2)*Power(t2,2) + 2*Power(s2,3)*Power(t2,2) - 
            4*t1*Power(t2,2) - 142*s2*t1*Power(t2,2) + 
            147*Power(s2,2)*t1*Power(t2,2) + 
            41*Power(s2,3)*t1*Power(t2,2) - 
            30*Power(s2,4)*t1*Power(t2,2) + 62*Power(t1,2)*Power(t2,2) - 
            71*s2*Power(t1,2)*Power(t2,2) - 
            113*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
            158*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
            44*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
            460*Power(t1,3)*Power(t2,2) - 
            135*s2*Power(t1,3)*Power(t2,2) - 
            336*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
            334*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
            15*Power(s2,4)*Power(t1,3)*Power(t2,2) - 
            629*Power(t1,4)*Power(t2,2) + 
            829*s2*Power(t1,4)*Power(t2,2) + 
            131*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
            165*Power(s2,3)*Power(t1,4)*Power(t2,2) + 
            Power(s2,4)*Power(t1,4)*Power(t2,2) + 
            155*Power(t1,5)*Power(t2,2) - 
            547*s2*Power(t1,5)*Power(t2,2) + 
            120*Power(s2,2)*Power(t1,5)*Power(t2,2) - 
            20*Power(s2,3)*Power(t1,5)*Power(t2,2) + 
            34*Power(t1,6)*Power(t2,2) + 48*s2*Power(t1,6)*Power(t2,2) - 
            19*Power(s2,2)*Power(t1,6)*Power(t2,2) - 
            38*Power(t1,7)*Power(t2,2) + 38*s2*Power(t1,7)*Power(t2,2) + 
            4*Power(t2,3) + 48*s2*Power(t2,3) - 
            52*Power(s2,2)*Power(t2,3) - 14*Power(s2,3)*Power(t2,3) + 
            10*Power(s2,4)*Power(t2,3) + 32*t1*Power(t2,3) + 
            14*s2*t1*Power(t2,3) + 148*Power(s2,2)*t1*Power(t2,3) - 
            293*Power(s2,3)*t1*Power(t2,3) + 
            27*Power(s2,4)*t1*Power(t2,3) - 313*Power(t1,2)*Power(t2,3) - 
            734*s2*Power(t1,2)*Power(t2,3) + 
            874*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
            211*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
            63*Power(s2,4)*Power(t1,2)*Power(t2,3) + 
            414*Power(t1,3)*Power(t2,3) + 
            402*s2*Power(t1,3)*Power(t2,3) - 
            683*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
            222*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
            27*Power(s2,4)*Power(t1,3)*Power(t2,3) - 
            246*Power(t1,4)*Power(t2,3) - s2*Power(t1,4)*Power(t2,3) - 
            76*Power(s2,2)*Power(t1,4)*Power(t2,3) - 
            194*Power(s2,3)*Power(t1,4)*Power(t2,3) - 
            Power(s2,4)*Power(t1,4)*Power(t2,3) + 
            138*Power(t1,5)*Power(t2,3) + 
            104*s2*Power(t1,5)*Power(t2,3) + 
            68*Power(s2,2)*Power(t1,5)*Power(t2,3) + 
            28*Power(s2,3)*Power(t1,5)*Power(t2,3) - 
            41*Power(t1,6)*Power(t2,3) - 37*s2*Power(t1,6)*Power(t2,3) - 
            23*Power(s2,2)*Power(t1,6)*Power(t2,3) + 
            4*Power(t1,7)*Power(t2,3) - 4*s2*Power(t1,7)*Power(t2,3) - 
            32*Power(t2,4) + 2*s2*Power(t2,4) - 
            22*Power(s2,2)*Power(t2,4) + 94*Power(s2,3)*Power(t2,4) - 
            14*Power(s2,4)*Power(t2,4) + 46*t1*Power(t2,4) + 
            609*s2*t1*Power(t2,4) - 745*Power(s2,2)*t1*Power(t2,4) + 
            187*Power(s2,3)*t1*Power(t2,4) + 
            3*Power(s2,4)*t1*Power(t2,4) - 99*Power(t1,2)*Power(t2,4) + 
            116*s2*Power(t1,2)*Power(t2,4) + 
            146*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
            373*Power(s2,3)*Power(t1,2)*Power(t2,4) + 
            21*Power(s2,4)*Power(t1,2)*Power(t2,4) - 
            51*Power(t1,3)*Power(t2,4) - 414*s2*Power(t1,3)*Power(t2,4) + 
            442*Power(s2,2)*Power(t1,3)*Power(t2,4) + 
            150*Power(s2,3)*Power(t1,3)*Power(t2,4) - 
            9*Power(s2,4)*Power(t1,3)*Power(t2,4) + 
            137*Power(t1,4)*Power(t2,4) + 57*s2*Power(t1,4)*Power(t2,4) - 
            250*Power(s2,2)*Power(t1,4)*Power(t2,4) - 
            9*Power(s2,3)*Power(t1,4)*Power(t2,4) - 
            Power(s2,4)*Power(t1,4)*Power(t2,4) - 
            189*Power(t1,5)*Power(t2,4) + 83*s2*Power(t1,5)*Power(t2,4) + 
            67*Power(s2,2)*Power(t1,5)*Power(t2,4) - 
            Power(s2,3)*Power(t1,5)*Power(t2,4) + 
            84*Power(t1,6)*Power(t2,4) - 37*s2*Power(t1,6)*Power(t2,4) + 
            2*Power(s2,2)*Power(t1,6)*Power(t2,4) + 44*Power(t2,5) - 
            238*s2*Power(t2,5) + 216*Power(s2,2)*Power(t2,5) - 
            92*Power(s2,3)*Power(t2,5) + 6*Power(s2,4)*Power(t2,5) - 
            203*s2*t1*Power(t2,5) + 166*Power(s2,2)*t1*Power(t2,5) + 
            82*Power(s2,3)*t1*Power(t2,5) - 
            3*Power(s2,4)*t1*Power(t2,5) + 67*Power(t1,2)*Power(t2,5) + 
            320*s2*Power(t1,2)*Power(t2,5) - 
            301*Power(s2,2)*Power(t1,2)*Power(t2,5) - 
            16*Power(s2,3)*Power(t1,2)*Power(t2,5) - 
            7*Power(s2,4)*Power(t1,2)*Power(t2,5) - 
            182*Power(t1,3)*Power(t2,5) - 
            101*s2*Power(t1,3)*Power(t2,5) + 
            140*Power(s2,2)*Power(t1,3)*Power(t2,5) + 
            3*Power(s2,3)*Power(t1,3)*Power(t2,5) + 
            4*Power(s2,4)*Power(t1,3)*Power(t2,5) + 
            221*Power(t1,4)*Power(t2,5) - 55*s2*Power(t1,4)*Power(t2,5) - 
            24*Power(s2,2)*Power(t1,4)*Power(t2,5) - 
            Power(s2,3)*Power(t1,4)*Power(t2,5) - 
            54*Power(t1,5)*Power(t2,5) + 23*s2*Power(t1,5)*Power(t2,5) - 
            9*Power(s2,2)*Power(t1,5)*Power(t2,5) - 
            12*Power(t1,6)*Power(t2,5) + 6*s2*Power(t1,6)*Power(t2,5) - 
            4*Power(t2,6) + 86*s2*Power(t2,6) - 
            56*Power(s2,2)*Power(t2,6) + 8*Power(s2,3)*Power(t2,6) - 
            2*Power(s2,4)*Power(t2,6) + 28*t1*Power(t2,6) - 
            123*s2*t1*Power(t2,6) + 42*Power(s2,2)*t1*Power(t2,6) - 
            8*Power(s2,3)*t1*Power(t2,6) + 3*Power(s2,4)*t1*Power(t2,6) + 
            3*Power(t1,2)*Power(t2,6) + 47*s2*Power(t1,2)*Power(t2,6) + 
            7*Power(s2,2)*Power(t1,2)*Power(t2,6) + 
            7*Power(s2,3)*Power(t1,2)*Power(t2,6) - 
            Power(s2,4)*Power(t1,2)*Power(t2,6) - 
            17*Power(t1,3)*Power(t2,6) + 7*s2*Power(t1,3)*Power(t2,6) - 
            29*Power(s2,2)*Power(t1,3)*Power(t2,6) - 
            3*Power(s2,3)*Power(t1,3)*Power(t2,6) - 
            26*Power(t1,4)*Power(t2,6) + 11*s2*Power(t1,4)*Power(t2,6) + 
            12*Power(s2,2)*Power(t1,4)*Power(t2,6) + 
            16*Power(t1,5)*Power(t2,6) - 8*s2*Power(t1,5)*Power(t2,6) - 
            16*Power(t2,7) + 14*s2*Power(t2,7) + 
            4*Power(s2,2)*Power(t2,7) + 2*Power(s2,3)*Power(t2,7) - 
            3*s2*t1*Power(t2,7) - 12*Power(s2,2)*t1*Power(t2,7) - 
            3*Power(s2,3)*t1*Power(t2,7) + 6*Power(t1,2)*Power(t2,7) + 
            11*Power(s2,2)*Power(t1,2)*Power(t2,7) + 
            Power(s2,3)*Power(t1,2)*Power(t2,7) + 
            6*Power(t1,3)*Power(t2,7) - 5*s2*Power(t1,3)*Power(t2,7) - 
            3*Power(s2,2)*Power(t1,3)*Power(t2,7) - 
            4*Power(t1,4)*Power(t2,7) + 2*s2*Power(t1,4)*Power(t2,7) + 
            4*Power(t2,8) - 6*t1*Power(t2,8) + 
            2*Power(t1,2)*Power(t2,8) - 
            Power(s1,4)*(-2 + s2 + t1)*(-1 + t2)*
             (Power(t1,4)*(5 - 10*t2 + Power(t2,2)) + 
               2*Power(t2,2)*(1 - 6*t2 + Power(t2,2)) + 
               Power(t1,3)*(-11 + 22*t2 + 21*Power(t2,2) - 
                  4*Power(t2,3)) - 
               3*t1*t2*(2 - 11*t2 - 4*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(6 - 6*t2 - 57*Power(t2,2) + 4*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s,3)*(-1 + t1)*(-1 + t2)*
             (Power(t1,4)*(-15 + 11*t2 + 4*Power(t2,2) + 
                  s1*(5 - 10*t2 + Power(t2,2)) - 
                  s2*(-10 + t2 + 5*Power(t2,2))) + 
               2*Power(t2,2)*
                (2 - (1 + 8*s2)*t2 + (1 + 11*s2)*Power(t2,2) - 
                  s2*Power(t2,3) + 
                  s1*(-2 + (3 + 6*s2)*t2 - 2*(5 + s2)*Power(t2,2) + 
                     Power(t2,3))) + 
               Power(t1,3)*(8 + 21*t2 - 71*Power(t2,2) + 
                  4*Power(t2,3) + 
                  s1*(4 + (-27 + 10*s2)*t2 - 
                     2*(-38 + s2)*Power(t2,2) - 9*Power(t2,3)) + 
                  s2*(-12 - 4*t2 - 3*Power(t2,2) + 5*Power(t2,3))) - 
               t1*t2*(12 - 2*(5 + 22*s2)*t2 + 
                  (23 + 52*s2)*Power(t2,2) + (1 + 3*s2)*Power(t2,3) - 
                  s2*Power(t2,4) + 
                  s1*(-12 + 18*(1 + 2*s2)*t2 - 
                     3*(23 + 2*s2)*Power(t2,2) - 
                     2*(1 + s2)*Power(t2,3) + Power(t2,4))) + 
               Power(t1,2)*(12 + 4*(-10 + s2)*t2 + 
                  (79 - 16*s2)*Power(t2,2) + (9 + 33*s2)*Power(t2,3) - 
                  5*s2*Power(t2,4) + 
                  s1*(-12 - 4*(-10 + s2)*t2 + 
                     3*(-29 + 8*s2)*Power(t2,2) - 
                     2*(17 + 4*s2)*Power(t2,3) + 5*Power(t2,4)))) + 
            Power(s1,3)*(-1 + t1)*
             (Power(t1,5)*(-5 + 29*t2 - 27*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,4)*(-14 + 11*t2 - Power(t2,2) + 
                  25*Power(t2,3) - 5*Power(t2,4) + 
                  s2*(15 + 11*t2 - 47*Power(t2,2) + 5*Power(t2,3))) + 
               2*Power(t2,2)*
                (-1 - 2*t2 + 28*Power(t2,2) - 30*Power(t2,3) + 
                  5*Power(t2,4) + 
                  2*Power(s2,2)*t2*(-1 - 8*t2 + Power(t2,2)) + 
                  s2*(1 - 11*t2 + 9*Power(t2,2) + 19*Power(t2,3) - 
                     2*Power(t2,4))) + 
               Power(t1,3)*(29 - 175*Power(t2,2) + 23*Power(t2,3) + 
                  30*Power(t2,4) - 3*Power(t2,5) - 
                  2*Power(s2,2)*t2*(21 - 32*t2 + 3*Power(t2,2)) + 
                  s2*(-23 + 97*t2 + 87*Power(t2,2) - 85*Power(t2,3) + 
                     4*Power(t2,4))) + 
               Power(t1,2)*(-6 - 96*t2 + 312*Power(t2,2) + 
                  41*Power(t2,3) - 155*Power(t2,4) + 15*Power(t2,5) + 
                  Power(t2,6) + 
                  8*Power(s2,2)*t2*
                   (5 - 11*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                  s2*(6 - 86*t2 - 123*Power(t2,2) + 123*Power(t2,3) + 
                     73*Power(t2,4) - 9*Power(t2,5))) + 
               t1*t2*(6 + 21*t2 - 232*Power(t2,2) + 162*Power(t2,3) + 
                  18*Power(t2,4) - 7*Power(t2,5) + 
                  2*Power(s2,2)*t2*
                   (6 + 49*t2 + 2*Power(t2,2) - Power(t2,3)) + 
                  s2*(-6 + 59*t2 + 3*Power(t2,2) - 133*Power(t2,3) - 
                     5*Power(t2,4) + 2*Power(t2,5)))) + 
            Power(s1,2)*(Power(t1,6)*
                (15 - 61*t2 - 64*Power(t2,2) + 71*Power(t2,3) - 
                  9*Power(t2,4) + 
                  s2*(-15 + 100*t2 - 39*Power(t2,2) + 2*Power(t2,3))) + 
               Power(t1,5)*(-13 - 110*t2 + 131*Power(t2,2) - 
                  208*Power(t2,3) - 54*Power(t2,4) + 14*Power(t2,5) + 
                  4*Power(s2,2)*
                   (5 - 14*t2 - 5*Power(t2,2) + 2*Power(t2,3)) + 
                  s2*(8 - 115*t2 + 254*Power(t2,2) + 159*Power(t2,3) - 
                     18*Power(t2,4))) - 
               2*Power(t2,2)*
                (-5 + 43*t2 - 46*Power(t2,2) - 6*Power(t2,3) + 
                  19*Power(t2,4) - 5*Power(t2,5) + 
                  Power(s2,3)*
                   (1 - 10*t2 + 11*Power(t2,2) - 2*Power(t2,3)) + 
                  Power(s2,2)*
                   (1 - 14*t2 + 21*Power(t2,2) + 26*Power(t2,3) - 
                     2*Power(t2,4)) + 
                  s2*(4 - 12*t2 + 39*Power(t2,2) - 45*Power(t2,3) - 
                     19*Power(t2,4) + Power(t2,5))) + 
               Power(t1,4)*(19 - 208*t2 + 204*Power(t2,2) + 
                  512*Power(t2,3) + 154*Power(t2,4) - 22*Power(t2,5) - 
                  3*Power(t2,6) - 
                  Power(s2,3)*
                   (5 + 16*t2 - 27*Power(t2,2) + 6*Power(t2,3)) - 
                  Power(s2,2)*
                   (69 - 378*t2 + 421*Power(t2,2) + 128*Power(t2,3)) + 
                  s2*(17 + 273*t2 - 405*Power(t2,2) - 382*Power(t2,3) + 
                     78*Power(t2,4) + 3*Power(t2,5))) + 
               t1*t2*(-30 + 243*t2 - 145*Power(t2,2) - 
                  244*Power(t2,3) + 110*Power(t2,4) + 17*Power(t2,5) - 
                  15*Power(t2,6) + 
                  Power(s2,3)*
                   (6 - 57*t2 + 60*Power(t2,2) - 11*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  Power(s2,2)*
                   (6 - 65*t2 - 48*Power(t2,2) + 507*Power(t2,3) + 
                     22*Power(t2,4) - 6*Power(t2,5)) + 
                  s2*(24 - 70*t2 + 248*Power(t2,2) - 319*Power(t2,3) - 
                     203*Power(t2,4) - 35*Power(t2,5) + 3*Power(t2,6))) \
+ Power(t1,3)*(-55 + 687*t2 - 271*Power(t2,2) - 1018*Power(t2,3) - 
                  102*Power(t2,4) + 47*Power(t2,5) - 8*Power(t2,6) + 
                  Power(s2,3)*
                   (11 + 26*t2 - 35*Power(t2,2) - 10*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  Power(s2,2)*
                   (59 - 564*t2 + 295*Power(t2,2) + 850*Power(t2,3) + 
                     22*Power(t2,4) - 6*Power(t2,5)) + 
                  s2*(14 - 278*t2 + 543*Power(t2,2) + 4*Power(t2,3) - 
                     161*Power(t2,4) - 62*Power(t2,5) + 4*Power(t2,6))) \
+ Power(t1,2)*(30 - 238*t2 - 261*Power(t2,2) + 770*Power(t2,3) + 
                  247*Power(t2,4) - 161*Power(t2,5) + 24*Power(t2,6) + 
                  5*Power(t2,7) + 
                  Power(s2,3)*
                   (-6 - 4*t2 + 27*Power(t2,2) - 16*Power(t2,3) + 
                     Power(t2,4) - 2*Power(t2,5)) + 
                  Power(s2,2)*
                   (-6 + 172*t2 + 469*Power(t2,2) - 1070*Power(t2,3) - 
                     299*Power(t2,4) + 12*Power(t2,5) + 2*Power(t2,6)) + 
                  s2*(-24 + 8*t2 - 483*Power(t2,2) + 361*Power(t2,3) + 
                     250*Power(t2,4) + 192*Power(t2,5) + Power(t2,6) - 
                     Power(t2,7)))) + 
            s1*(-2*(-1 + s2)*Power(t1,7)*t2*(7 - 8*t2 + Power(t2,2)) + 
               Power(t1,6)*(40 - 401*t2 + 425*Power(t2,2) - 
                  45*Power(t2,3) - 41*Power(t2,4) + 6*Power(t2,5) + 
                  Power(s2,2)*
                   (5 + 13*t2 - 21*Power(t2,2) + 3*Power(t2,3)) + 
                  s2*(-40 + 325*t2 - 337*Power(t2,2) + 67*Power(t2,3) + 
                     Power(t2,4))) + 
               Power(t1,5)*(-118 + 1200*t2 - 805*Power(t2,2) - 
                  85*Power(t2,3) + 195*Power(t2,4) + 21*Power(t2,5) - 
                  8*Power(t2,6) - 
                  2*Power(s2,3)*(5 - 8*t2 + 3*Power(t2,2)) + 
                  Power(s2,2)*
                   (34 - 384*t2 + 296*Power(t2,2) + 52*Power(t2,3) - 
                     14*Power(t2,4)) + 
                  s2*(68 - 555*t2 + 452*Power(t2,2) - 290*Power(t2,3) - 
                     64*Power(t2,4) + 5*Power(t2,5))) - 
               2*Power(t2,2)*
                (4 - 25*t2 - 12*Power(t2,2) + 60*Power(t2,3) - 
                  26*Power(t2,4) - 3*Power(t2,5) + 2*Power(t2,6) + 
                  Power(s2,4)*
                   (-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                  Power(s2,2)*
                   (-1 + 12*t2 - 6*Power(t2,2) + 44*Power(t2,3) - 
                     17*Power(t2,4)) + 
                  Power(s2,3)*
                   (5 - 30*t2 + 46*Power(t2,2) - 26*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  s2*(-9 + 49*t2 - 60*Power(t2,2) - 74*Power(t2,3) + 
                     53*Power(t2,4) + 9*Power(t2,5))) + 
               Power(t1,4)*(74 - 688*t2 + 578*Power(t2,2) + 
                  105*Power(t2,3) - 353*Power(t2,4) - 91*Power(t2,5) + 
                  5*Power(t2,6) + 2*Power(t2,7) - 
                  Power(s2,4)*
                   (-5 + 15*t2 - 11*Power(t2,2) + Power(t2,3)) + 
                  Power(s2,3)*
                   (-3 + 164*t2 - 190*Power(t2,2) + 32*Power(t2,3) - 
                     3*Power(t2,4)) + 
                  Power(s2,2)*
                   (-68 + 627*t2 - 194*Power(t2,2) - 60*Power(t2,3) + 
                     94*Power(t2,4) + Power(t2,5)) + 
                  s2*(41 - 547*t2 - 119*Power(t2,2) + 568*Power(t2,3) + 
                     53*Power(t2,4) - 29*Power(t2,5) + Power(t2,6))) + 
               t1*t2*(24 - 142*t2 - 125*Power(t2,2) + 220*Power(t2,3) + 
                  16*Power(t2,4) - 84*Power(t2,5) + 21*Power(t2,6) + 
                  6*Power(t2,7) + 
                  3*Power(s2,4)*
                   (-2 + 13*t2 - 7*Power(t2,2) - 5*Power(t2,3) + 
                     Power(t2,4)) - 
                  Power(s2,2)*
                   (6 - 79*t2 + 38*Power(t2,2) - 204*Power(t2,3) + 
                     236*Power(t2,4) + 3*Power(t2,5)) + 
                  Power(s2,3)*
                   (30 - 169*t2 + 190*Power(t2,2) - 38*Power(t2,3) - 
                     20*Power(t2,4) + 7*Power(t2,5)) + 
                  s2*(-54 + 259*t2 - 157*Power(t2,2) - 
                     442*Power(t2,3) + 298*Power(t2,4) + 
                     151*Power(t2,5) + 9*Power(t2,6))) + 
               Power(t1,3)*(32 - 309*t2 - 378*Power(t2,2) + 
                  266*Power(t2,3) + 395*Power(t2,4) + 8*Power(t2,5) + 
                  31*Power(t2,6) + 3*Power(t2,7) + 
                  Power(s2,4)*
                   (-11 + 33*t2 - Power(t2,2) - 25*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  Power(s2,2)*
                   (19 - 48*t2 - 56*Power(t2,2) + 106*Power(t2,3) - 
                     391*Power(t2,4) + 2*Power(t2,5)) + 
                  Power(s2,3)*
                   (43 - 404*t2 + 316*Power(t2,2) + 94*Power(t2,3) - 
                     55*Power(t2,4) + 6*Power(t2,5)) - 
                  s2*(123 - 1059*t2 + 128*Power(t2,2) + 
                     911*Power(t2,3) - 277*Power(t2,4) - 
                     133*Power(t2,5) - 14*Power(t2,6) + Power(t2,7))) - 
               Power(t1,2)*(Power(s2,4)*
                   (-6 + 12*t2 + 51*Power(t2,2) - 61*Power(t2,3) + 
                     3*Power(t2,4) + Power(t2,5)) + 
                  Power(s2,3)*
                   (30 - 182*t2 - 87*Power(t2,2) + 384*Power(t2,3) - 
                     164*Power(t2,4) + 18*Power(t2,5) + Power(t2,6)) + 
                  Power(s2,2)*
                   (-6 + 142*t2 + 298*Power(t2,2) - 65*Power(t2,3) - 
                     267*Power(t2,4) - 157*Power(t2,5) + 7*Power(t2,6)) \
+ s2*(-54 + 226*t2 - 35*Power(t2,2) - 615*Power(t2,3) + 
                     113*Power(t2,4) + 327*Power(t2,5) + 
                     88*Power(t2,6) - 2*Power(t2,7)) + 
                  2*(12 - 62*t2 - 157*Power(t2,2) + 28*Power(t2,3) + 
                     210*Power(t2,4) - 38*Power(t2,5) - 6*Power(t2,6) + 
                     12*Power(t2,7) + Power(t2,8)))) + 
            Power(s,2)*(Power(t1,6)*(-1 + t2)*
                (-15 + 39*t2 + s2*(10 - 15*t2 - 3*Power(t2,2)) + 
                  s1*(5 - 24*t2 + 3*Power(t2,2))) + 
               Power(t1,5)*(-3*Power(s1,2)*
                   (-5 + 15*t2 - 11*Power(t2,2) + Power(t2,3)) + 
                  Power(s2,2)*
                   (5 + 4*t2 - 15*Power(t2,2) + 6*Power(t2,3)) + 
                  s2*(27 - 174*t2 + 139*Power(t2,2) - 58*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  2*(-19 + 100*t2 - 56*Power(t2,2) + 5*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  s1*(-34 + 66*t2 - 12*Power(t2,2) + 34*Power(t2,3) - 
                     6*Power(t2,4) + 
                     s2*(25 - 51*t2 - 33*Power(t2,2) + 11*Power(t2,3)))) \
- 2*Power(t2,2)*(3 - 25*t2 - 22*Power(t2,2) - 19*Power(t2,3) + 
                  15*Power(t2,4) + 
                  Power(s2,2)*t2*
                   (3 + 13*t2 - 19*Power(t2,2) + 3*Power(t2,3)) - 
                  s2*(-2 + 9*t2 - 61*Power(t2,2) + 10*Power(t2,3) - 
                     5*Power(t2,4) + Power(t2,5)) + 
                  s1*(1 + 7*t2 - 11*Power(t2,2) + 4*Power(t2,3) - 
                     18*Power(t2,4) + Power(t2,5) - 
                     Power(s2,2)*
                      (-2 + 11*t2 - 10*Power(t2,2) + Power(t2,3)) + 
                     s2*(-5 + 16*t2 - 16*Power(t2,2) + 
                        24*Power(t2,3) - 3*Power(t2,4))) + 
                  Power(s1,2)*
                   (s2*(1 - 3*t2 - 17*Power(t2,2) + 3*Power(t2,3)) - 
                     2*(2 - 11*t2 + 9*Power(t2,2) - 9*Power(t2,3) + 
                        Power(t2,4)))) + 
               Power(t1,4)*(34 - 429*t2 + 132*Power(t2,2) + 
                  245*Power(t2,3) - 174*Power(t2,4) + 16*Power(t2,5) - 
                  Power(s2,2)*
                   (11 + 9*t2 + 18*Power(t2,2) - 49*Power(t2,3) + 
                     11*Power(t2,4)) + 
                  s2*(-13 + 234*t2 - 138*Power(t2,2) + 
                     99*Power(t2,4) - 6*Power(t2,5)) + 
                  Power(s1,2)*
                   (-13 + 17*t2 + 119*Power(t2,2) - 157*Power(t2,3) + 
                     18*Power(t2,4) + 
                     s2*(-5 + 11*t2 + 9*Power(t2,2) + Power(t2,3))) + 
                  s1*(68 + 56*t2 + 41*Power(t2,2) + 69*Power(t2,4) - 
                     10*Power(t2,5) + 
                     Power(s2,2)*
                      (-10 + 37*t2 - 28*Power(t2,2) + Power(t2,3)) - 
                     s2*(50 - 83*t2 + 117*Power(t2,2) + 
                        139*Power(t2,3) + Power(t2,4)))) + 
               Power(t1,3)*(3 + 77*t2 - 44*Power(t2,2) - 
                  551*Power(t2,3) + 319*Power(t2,4) + 24*Power(t2,5) - 
                  4*Power(t2,6) + 
                  Power(s2,2)*
                   (6 + 9*t2 + 68*Power(t2,2) - 29*Power(t2,3) - 
                     68*Power(t2,4) + 14*Power(t2,5)) + 
                  s2*(4 - 43*t2 + 360*Power(t2,2) + 40*Power(t2,3) - 
                     190*Power(t2,4) + 7*Power(t2,5) - 2*Power(t2,6)) + 
                  Power(s1,2)*
                   (-26 + 134*t2 - 327*Power(t2,2) + 293*Power(t2,3) + 
                     49*Power(t2,4) - 11*Power(t2,5) + 
                     s2*(11 - 21*t2 - 47*Power(t2,2) - 
                        67*Power(t2,3) + 12*Power(t2,4))) + 
                  s1*(-19 - 99*t2 + 63*Power(t2,2) - 304*Power(t2,3) - 
                     166*Power(t2,4) - 25*Power(t2,5) + 6*Power(t2,6) + 
                     Power(s2,2)*
                      (22 - 77*t2 + 70*Power(t2,2) - 27*Power(t2,3) + 
                        12*Power(t2,4)) + 
                     s2*(-1 + 20*t2 - 143*Power(t2,2) + 
                        645*Power(t2,3) + 32*Power(t2,4) - 9*Power(t2,5)\
))) + t1*t2*(18 - 151*t2 - 95*Power(t2,2) - 138*Power(t2,3) + 
                  73*Power(t2,4) + 21*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (20 + 67*t2 - 53*Power(t2,2) - 43*Power(t2,3) + 
                     9*Power(t2,4)) + 
                  s2*(12 - 54*t2 + 373*Power(t2,2) - 51*Power(t2,3) - 
                     18*Power(t2,4) + 13*Power(t2,5) - 3*Power(t2,6)) + 
                  s1*(6 + 35*t2 - 43*Power(t2,2) - 163*Power(t2,3) - 
                     82*Power(t2,4) - 28*Power(t2,5) + 3*Power(t2,6) + 
                     Power(s2,2)*
                      (12 - 60*t2 + 51*Power(t2,2) - 8*Power(t2,3) + 
                        5*Power(t2,4)) + 
                     s2*(-30 + 99*t2 - 158*Power(t2,2) + 
                        350*Power(t2,3) + 20*Power(t2,4) - 
                        9*Power(t2,5))) + 
                  3*Power(s1,2)*
                   (s2*(2 - 5*t2 - 37*Power(t2,2) - 11*Power(t2,3) + 
                        3*Power(t2,4)) - 
                     2*(4 - 21*t2 + 14*Power(t2,2) - 16*Power(t2,3) - 
                        6*Power(t2,4) + Power(t2,5)))) - 
               Power(t1,2)*(18 - 228*t2 - 134*Power(t2,2) - 
                  237*Power(t2,3) - 29*Power(t2,4) + 151*Power(t2,5) - 
                  5*Power(t2,6) + 
                  Power(s2,2)*t2*
                   (16 + 15*t2 + 135*Power(t2,2) - 182*Power(t2,3) + 
                     13*Power(t2,4) + 3*Power(t2,5)) + 
                  s2*(12 + 2*t2 + 507*Power(t2,2) + 58*Power(t2,3) - 
                     98*Power(t2,4) - 17*Power(t2,5) + Power(t2,6) - 
                     Power(t2,7)) + 
                  Power(s1,2)*
                   (-24 + 82*t2 - 41*Power(t2,2) + 5*Power(t2,3) + 
                     199*Power(t2,4) - 11*Power(t2,5) - 2*Power(t2,6) + 
                     s2*(6 - 4*t2 - 55*Power(t2,2) - 171*Power(t2,3) + 
                        13*Power(t2,4) + 3*Power(t2,5))) + 
                  s1*(6 + 98*t2 + 90*Power(t2,2) - 428*Power(t2,3) - 
                     160*Power(t2,4) - 125*Power(t2,5) + 6*Power(t2,6) + 
                     Power(t2,7) + 
                     Power(s2,2)*
                      (12 - 40*t2 + 18*Power(t2,2) - Power(t2,3) + 
                        8*Power(t2,4) + 3*Power(t2,5)) + 
                     s2*(-30 + 74*t2 - 400*Power(t2,2) + 
                        639*Power(t2,3) + 249*Power(t2,4) - 
                        17*Power(t2,5) - 3*Power(t2,6))))) + 
            s*(-30*Power(t1,3) + Power(t1,4) + 50*Power(t1,5) - 
               25*Power(t1,6) + 154*Power(t1,2)*t2 + 57*Power(t1,3)*t2 + 
               125*Power(t1,4)*t2 - 479*Power(t1,5)*t2 + 
               193*Power(t1,6)*t2 - 14*Power(t1,7)*t2 - 
               64*t1*Power(t2,2) + 81*Power(t1,2)*Power(t2,2) + 
               233*Power(t1,3)*Power(t2,2) - 
               480*Power(t1,4)*Power(t2,2) + 
               463*Power(t1,5)*Power(t2,2) - 
               217*Power(t1,6)*Power(t2,2) + 16*Power(t1,7)*Power(t2,2) + 
               18*Power(t2,3) - 69*t1*Power(t2,3) + 
               171*Power(t1,2)*Power(t2,3) - 
               122*Power(t1,3)*Power(t2,3) - 
               176*Power(t1,4)*Power(t2,3) - 51*Power(t1,5)*Power(t2,3) + 
               119*Power(t1,6)*Power(t2,3) - 2*Power(t1,7)*Power(t2,3) + 
               34*Power(t2,4) - 249*t1*Power(t2,4) + 
               67*Power(t1,2)*Power(t2,4) + 38*Power(t1,3)*Power(t2,4) + 
               201*Power(t1,4)*Power(t2,4) - 
               105*Power(t1,5)*Power(t2,4) - 6*Power(t1,6)*Power(t2,4) + 
               102*Power(t2,5) + 137*t1*Power(t2,5) - 
               273*Power(t1,2)*Power(t2,5) + 
               141*Power(t1,3)*Power(t2,5) - 33*Power(t1,4)*Power(t2,5) + 
               10*Power(t1,5)*Power(t2,5) - 50*Power(t2,6) + 
               81*t1*Power(t2,6) - 24*Power(t1,2)*Power(t2,6) - 
               25*Power(t1,3)*Power(t2,6) + 10*Power(t1,4)*Power(t2,6) - 
               8*Power(t2,7) - 12*t1*Power(t2,7) + 
               16*Power(t1,2)*Power(t2,7) - 4*Power(t1,3)*Power(t2,7) + 
               Power(s2,3)*(-1 + t2)*
                (-(Power(t1,5)*(5 - 10*t2 + Power(t2,2))) + 
                  2*Power(t2,3)*(-6 - 7*t2 + 3*Power(t2,2)) + 
                  t1*Power(t2,2)*
                   (34 + 48*t2 + 9*Power(t2,2) - 9*Power(t2,3)) + 
                  Power(t1,4)*
                   (11 - 16*t2 - 38*Power(t2,2) + 7*Power(t2,3)) + 
                  Power(t1,3)*
                   (-6 + 74*Power(t2,2) + 31*Power(t2,3) - 
                     13*Power(t2,4)) + 
                  Power(t1,2)*t2*
                   (-6 - 41*t2 - 94*Power(t2,2) + 22*Power(t2,3) + 
                     3*Power(t2,4))) - 
               Power(s2,2)*(Power(t1,6)*
                   (5 - t2 - 5*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,5)*
                   (19 - 179*t2 + 166*Power(t2,2) - 71*Power(t2,3) + 
                     Power(t2,4)) + 
                  Power(t1,4)*
                   (-35 + 297*t2 + 2*Power(t2,2) - 172*Power(t2,3) + 
                     91*Power(t2,4) - 7*Power(t2,5)) + 
                  2*Power(t2,2)*
                   (1 + 3*t2 - 12*Power(t2,2) - 41*Power(t2,3) - 
                     Power(t2,4) + 2*Power(t2,5)) + 
                  t1*t2*(-6 - 17*t2 + 75*Power(t2,2) + 
                     128*Power(t2,3) + 93*Power(t2,4) + 5*Power(t2,5) - 
                     6*Power(t2,6)) + 
                  Power(t1,3)*
                   (1 - 5*t2 - 97*Power(t2,2) + 278*Power(t2,3) - 
                     9*Power(t2,4) + 13*Power(t2,5) - 5*Power(t2,6)) + 
                  Power(t1,2)*
                   (6 - 42*t2 - 295*Power(t2,2) + 203*Power(t2,3) - 
                     327*Power(t2,4) - 17*Power(t2,5) + 6*Power(t2,6) + 
                     2*Power(t2,7))) + 
               s2*(2*Power(t1,7)*t2*(7 - 8*t2 + Power(t2,2)) + 
                  Power(t1,6)*
                   (25 - 126*t2 + 82*Power(t2,2) - 42*Power(t2,3) - 
                     3*Power(t2,4)) - 
                  2*Power(t2,2)*
                   (1 + 3*t2 + 33*Power(t2,2) + 107*Power(t2,3) - 
                     50*Power(t2,4) + 2*Power(t2,5)) + 
                  Power(t1,5)*
                   (-10 + 17*t2 + 55*Power(t2,2) - 32*Power(t2,3) + 
                     13*Power(t2,4) + 5*Power(t2,5)) + 
                  Power(t1,4)*
                   (-74 + 652*t2 - 261*Power(t2,2) - 35*Power(t2,3) + 
                     238*Power(t2,4) + 19*Power(t2,5) - 11*Power(t2,6)) \
+ t1*t2*(6 + 39*t2 + 85*Power(t2,2) + 663*Power(t2,3) - 
                     255*Power(t2,4) - 102*Power(t2,5) + 12*Power(t2,6)) \
+ Power(t1,3)*(65 - 403*t2 + 185*Power(t2,2) + 790*Power(t2,3) - 
                     577*Power(t2,4) - 200*Power(t2,5) + 
                     25*Power(t2,6) + 3*Power(t2,7)) - 
                  Power(t1,2)*
                   (6 + 152*t2 + 238*Power(t2,2) + 562*Power(t2,3) + 
                     204*Power(t2,4) - 501*Power(t2,5) - 
                     16*Power(t2,6) + 11*Power(t2,7))) + 
               Power(s1,3)*(-1 + t1)*
                (3*Power(t1,4)*
                   (-5 + 15*t2 - 11*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,3)*
                   (18 + (-51 + 14*s2)*t2 - (37 + 32*s2)*Power(t2,2) + 
                     (99 + 2*s2)*Power(t2,3) - 13*Power(t2,4)) + 
                  2*Power(t2,3)*
                   (-3 - 9*t2 - 5*Power(t2,2) + Power(t2,3) - 
                     2*s2*(-1 - 8*t2 + Power(t2,2))) + 
                  Power(t1,2)*t2*
                   (-24 + 109*t2 - 167*Power(t2,2) - 21*Power(t2,3) + 
                     7*Power(t2,4) + 
                     s2*(-12 + 56*t2 + 60*Power(t2,2) - 8*Power(t2,3))) \
- t1*Power(t2,2)*(-24 - 15*t2 - 81*Power(t2,2) + 7*Power(t2,3) + 
                     Power(t2,4) + 
                     s2*(12 + 98*t2 + 4*Power(t2,2) - 2*Power(t2,3)))) + 
               Power(s1,2)*(Power(t1,6)*
                   (10 - 58*t2 + 54*Power(t2,2) - 6*Power(t2,3)) + 
                  2*Power(t2,3)*
                   (-5 + 10*t2 - 28*Power(t2,2) - 10*Power(t2,3) + 
                     Power(t2,4)) + 
                  Power(t1,5)*
                   (28 - 22*t2 - 21*Power(t2,2) - 60*Power(t2,3) + 
                     11*Power(t2,4)) + 
                  t1*Power(t2,2)*
                   (42 - 89*t2 + 284*Power(t2,2) + 148*Power(t2,3) + 
                     2*Power(t2,4) - 3*Power(t2,5)) + 
                  Power(t1,4)*
                   (-78 - 83*t2 + 71*Power(t2,2) + 26*Power(t2,3) - 
                     73*Power(t2,4) + 9*Power(t2,5)) + 
                  Power(t1,3)*
                   (36 + 185*t2 - 328*Power(t2,2) + 274*Power(t2,3) + 
                     285*Power(t2,4) + 3*Power(t2,5) - 7*Power(t2,6)) + 
                  Power(t1,2)*t2*
                   (18 + 174*t2 - 239*Power(t2,2) - 443*Power(t2,3) - 
                     104*Power(t2,4) + 17*Power(t2,5) + Power(t2,6)) + 
                  Power(s2,2)*
                   (2*Power(t2,2)*(3 - 20*t2 + Power(t2,2)) + 
                     Power(t1,4)*
                      (15 - 10*t2 - 25*Power(t2,2) + 4*Power(t2,3)) + 
                     Power(t1,3)*
                      (-33 + 28*t2 + 81*Power(t2,2) + 52*Power(t2,3) - 
                        16*Power(t2,4)) + 
                     t1*t2*(-18 + 111*t2 + 30*Power(t2,2) + 
                        29*Power(t2,3) - 8*Power(t2,4)) + 
                     Power(t1,2)*
                      (18 - 12*t2 - 133*Power(t2,2) - 94*Power(t2,3) + 
                        9*Power(t2,4) + 4*Power(t2,5))) + 
                  s2*(Power(t1,5)*
                      (-30 + 29*t2 + 76*Power(t2,2) - 11*Power(t2,3)) + 
                     Power(t1,4)*
                      (51 - 110*t2 - 54*Power(t2,2) + 268*Power(t2,3) - 
                        11*Power(t2,4)) - 
                     2*Power(t2,2)*
                      (7 - 33*t2 + 32*Power(t2,2) - 59*Power(t2,3) + 
                        5*Power(t2,4)) + 
                     t1*t2*(42 - 197*t2 + 239*Power(t2,2) - 
                        518*Power(t2,3) - 109*Power(t2,4) + 
                        15*Power(t2,5)) + 
                     Power(t1,3)*
                      (17 + 71*t2 + 342*Power(t2,2) - 864*Power(t2,3) - 
                        145*Power(t2,4) + 19*Power(t2,5)) + 
                     Power(t1,2)*
                      (-42 + 20*t2 - 369*Power(t2,2) + 614*Power(t2,3) + 
                        574*Power(t2,4) - 8*Power(t2,5) - 5*Power(t2,6)))\
) + s1*(Power(t1,6)*(-30 + 115*t2 + 25*Power(t2,2) - 71*Power(t2,3) + 
                     9*Power(t2,4) + 
                     s2*(25 - 125*t2 + 51*Power(t2,2) + Power(t2,3))) + 
                  Power(t1,5)*
                   (76 - 328*t2 + 232*Power(t2,2) + 177*Power(t2,3) + 
                     66*Power(t2,4) - 15*Power(t2,5) + 
                     Power(s2,2)*
                      (-20 + 37*t2 + 46*Power(t2,2) - 15*Power(t2,3)) + 
                     s2*(-65 + 537*t2 - 663*Power(t2,2) - 
                        81*Power(t2,3) + 16*Power(t2,4))) + 
                  2*Power(t2,2)*
                   (-1 + 2*t2 - 56*Power(t2,2) - 8*Power(t2,3) + 
                     25*Power(t2,4) + 6*Power(t2,5) + 
                     Power(s2,3)*
                      (1 - 10*t2 + 11*Power(t2,2) - 2*Power(t2,3)) + 
                     Power(s2,2)*
                      (1 - 8*t2 + 36*Power(t2,2) - 16*Power(t2,3) + 
                        3*Power(t2,4)) + 
                     s2*(5 - 2*t2 + 40*Power(t2,2) + 13*Power(t2,3) - 
                        41*Power(t2,4) + Power(t2,5))) + 
                  Power(t1,4)*
                   (-103 + 951*t2 - 377*Power(t2,2) - 571*Power(t2,3) - 
                     41*Power(t2,4) - 4*Power(t2,5) + Power(t2,6) + 
                     Power(s2,3)*
                      (5 - 12*t2 + 5*Power(t2,2) + 2*Power(t2,3)) + 
                     Power(s2,2)*
                      (69 - 301*t2 + 394*Power(t2,2) + 53*Power(t2,3) + 
                        9*Power(t2,4)) + 
                     s2*(57 - 860*t2 + 743*Power(t2,2) + 
                        204*Power(t2,3) - 226*Power(t2,4) + 2*Power(t2,5)\
)) + t1*t2*(6 + 11*t2 + 156*Power(t2,2) + 202*Power(t2,3) - 
                     298*Power(t2,4) - 45*Power(t2,5) + 
                     Power(s2,3)*
                      (-6 + 57*t2 - 60*Power(t2,2) + 11*Power(t2,3) - 
                        2*Power(t2,4)) - 
                     Power(s2,2)*
                      (6 - 29*t2 + 54*Power(t2,2) + 284*Power(t2,3) - 
                        44*Power(t2,4) + Power(t2,5)) + 
                     s2*(-30 + t2 - 230*Power(t2,2) + 14*Power(t2,3) + 
                        427*Power(t2,4) + 61*Power(t2,5) - 3*Power(t2,6))\
) - Power(t1,3)*(-71 + 638*t2 + 2*Power(t2,2) - 1070*Power(t2,3) + 
                     325*Power(t2,4) + 39*Power(t2,5) - 6*Power(t2,6) - 
                     Power(t2,7) + 
                     Power(s2,3)*
                      (11 - 30*t2 + 29*Power(t2,2) - 18*Power(t2,3) + 
                        8*Power(t2,4)) + 
                     Power(s2,2)*
                      (59 - 470*t2 + 340*Power(t2,2) + 
                        695*Power(t2,3) - 81*Power(t2,4) + Power(t2,5)) \
+ s2*(43 - 200*t2 + 399*Power(t2,2) + 80*Power(t2,3) - 630*Power(t2,4) - 
                        96*Power(t2,5) + 4*Power(t2,6))) + 
                  Power(t1,2)*
                   (-6 - 182*t2 + 89*Power(t2,2) - 549*Power(t2,3) + 
                     137*Power(t2,4) + 288*Power(t2,5) + 4*Power(t2,6) - 
                     5*Power(t2,7) + 
                     Power(s2,3)*
                      (6 - 24*t2 + 5*Power(t2,2) + 12*Power(t2,3) - 
                        Power(t2,4) + 2*Power(t2,5)) - 
                     Power(s2,2)*
                      (-6 + 124*t2 + 415*Power(t2,2) - 
                        1095*Power(t2,3) + 42*Power(t2,4) + 
                        7*Power(t2,5) + Power(t2,6)) + 
                     s2*(30 + 218*t2 + 629*Power(t2,2) - 
                        322*Power(t2,3) - 414*Power(t2,4) - 
                        427*Power(t2,5) - 3*Power(t2,6) + Power(t2,7))))))/
          Power(t1,2) + (2*(-1 + t1)*
            (-3*Power(t1,2) + Power(t1,3) + 3*t1*t2 - Power(t2,2))*
            (-4*s2 + 3*Power(s2,2) + Power(s2,3) + 4*t1 - s2*t1 - 
              3*Power(s2,2)*t1 - 6*Power(t1,2) + 10*s2*Power(t1,2) - 
              3*Power(s2,2)*Power(t1,2) - Power(s2,3)*Power(t1,2) - 
              3*s2*Power(t1,3) + 3*Power(s2,2)*Power(t1,3) + 
              2*Power(t1,4) - 2*s2*Power(t1,4) + 2*t2 + 24*s2*t2 - 
              26*Power(s2,2)*t2 - 7*Power(s2,3)*t2 + 5*Power(s2,4)*t2 - 
              31*t1*t2 + 22*s2*t1*t2 + 28*Power(s2,2)*t1*t2 - 
              9*Power(s2,3)*t1*t2 - 5*Power(s2,4)*t1*t2 + 
              34*Power(t1,2)*t2 - 81*s2*Power(t1,2)*t2 + 
              27*Power(s2,2)*Power(t1,2)*t2 + 
              16*Power(s2,3)*Power(t1,2)*t2 + 5*Power(t1,3)*t2 + 
              21*s2*Power(t1,3)*t2 - 25*Power(s2,2)*Power(t1,3)*t2 - 
              14*Power(t1,4)*t2 + 14*s2*Power(t1,4)*t2 - 
              16*Power(t2,2) + s2*Power(t2,2) - 
              11*Power(s2,2)*Power(t2,2) + 47*Power(s2,3)*Power(t2,2) - 
              7*Power(s2,4)*Power(t2,2) + 81*t1*Power(t2,2) - 
              52*s2*t1*Power(t2,2) - 54*Power(s2,2)*t1*Power(t2,2) - 
              21*Power(s2,3)*t1*Power(t2,2) + 
              7*Power(s2,4)*t1*Power(t2,2) - 
              29*Power(t1,2)*Power(t2,2) + 
              93*s2*Power(t1,2)*Power(t2,2) - 
              12*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
              14*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
              18*Power(t1,3)*Power(t2,2) - 
              16*s2*Power(t1,3)*Power(t2,2) + 
              21*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
              14*Power(t1,4)*Power(t2,2) - 
              14*s2*Power(t1,4)*Power(t2,2) + 22*Power(t2,3) - 
              119*s2*Power(t2,3) + 108*Power(s2,2)*Power(t2,3) - 
              46*Power(s2,3)*Power(t2,3) + 3*Power(s2,4)*Power(t2,3) + 
              5*t1*Power(t2,3) - 32*s2*t1*Power(t2,3) + 
              53*Power(s2,2)*t1*Power(t2,3) + 
              30*Power(s2,3)*t1*Power(t2,3) - 
              3*Power(s2,4)*t1*Power(t2,3) + 
              24*Power(t1,2)*Power(t2,3) - 
              44*s2*Power(t1,2)*Power(t2,3) - 
              22*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
              11*Power(t1,3)*Power(t2,3) + 
              9*s2*Power(t1,3)*Power(t2,3) + 
              Power(s2,2)*Power(t1,3)*Power(t2,3) - 
              2*Power(t1,4)*Power(t2,3) + 2*s2*Power(t1,4)*Power(t2,3) - 
              2*Power(t2,4) + 43*s2*Power(t2,4) - 
              28*Power(s2,2)*Power(t2,4) + 4*Power(s2,3)*Power(t2,4) - 
              Power(s2,4)*Power(t2,4) - 11*t1*Power(t2,4) - 
              19*s2*t1*Power(t2,4) + 13*Power(s2,2)*t1*Power(t2,4) + 
              Power(s2,3)*t1*Power(t2,4) + Power(s2,4)*t1*Power(t2,4) + 
              15*Power(t1,2)*Power(t2,4) + 
              5*s2*Power(t1,2)*Power(t2,4) - 
              9*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
              Power(s2,3)*Power(t1,2)*Power(t2,4) - 
              18*Power(t1,3)*Power(t2,4) + 
              7*s2*Power(t1,3)*Power(t2,4) - 8*Power(t2,5) + 
              7*s2*Power(t2,5) + 2*Power(s2,2)*Power(t2,5) + 
              Power(s2,3)*Power(t2,5) + 2*t1*Power(t2,5) + 
              2*s2*t1*Power(t2,5) - 5*Power(s2,2)*t1*Power(t2,5) - 
              Power(s2,3)*t1*Power(t2,5) - 6*Power(t1,2)*Power(t2,5) + 
              s2*Power(t1,2)*Power(t2,5) + 
              3*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
              4*Power(t1,3)*Power(t2,5) - 2*s2*Power(t1,3)*Power(t2,5) + 
              2*Power(t2,6) - 2*t1*Power(t2,6) + 
              Power(s1,4)*(-1 + t1)*(-2 + s2 + t1)*
               (-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) - 
              Power(s,3)*(-1 + t1)*(-1 + t2)*
               (-2 + t2 + 8*s2*t2 - Power(t2,2) - 11*s2*Power(t2,2) + 
                 s2*Power(t2,3) + 
                 s1*(2 - 3*(1 + 2*s2)*t2 + 2*(5 + s2)*Power(t2,2) - 
                    Power(t2,3) + t1*(1 - 6*t2 + Power(t2,2))) - 
                 t1*(3 - 9*t2 + s2*(-2 + 3*t2 + Power(t2,2)))) + 
              Power(s1,3)*(-1 + t1)*
               (-1 - 2*t2 + 28*Power(t2,2) - 30*Power(t2,3) + 
                 5*Power(t2,4) + 
                 2*Power(s2,2)*t2*(-1 - 8*t2 + Power(t2,2)) - 
                 Power(t1,2)*
                  (-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                 t1*(4 - 33*t2 + 5*Power(t2,2) + 9*Power(t2,3) - 
                    Power(t2,4)) + 
                 s2*(1 - 11*t2 + 9*Power(t2,2) + 19*Power(t2,3) - 
                    2*Power(t2,4) + 
                    t1*(-3 + 23*t2 - 5*Power(t2,2) + Power(t2,3)))) + 
              s1*(-4 + 25*t2 + 12*Power(t2,2) - 60*Power(t2,3) + 
                 26*Power(t2,4) + 3*Power(t2,5) - 2*Power(t2,6) + 
                 Power(s2,4)*(-1 + t1)*
                  (-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                 Power(t1,3)*
                  (-8 + 65*t2 - 63*Power(t2,2) + 17*Power(t2,3) + 
                    7*Power(t2,4) - 2*Power(t2,5)) + 
                 Power(t1,2)*
                  (14 - 104*t2 + 23*Power(t2,2) - 57*Power(t2,3) + 
                    11*Power(t2,4) + Power(t2,5)) + 
                 2*t1*(1 - 7*t2 - 32*Power(t2,2) + 28*Power(t2,3) - 
                    10*Power(t2,4) + 3*Power(t2,5) + Power(t2,6)) + 
                 Power(s2,3)*(-1 + t2)*
                  (5 - 25*t2 + 21*Power(t2,2) - 5*Power(t2,3) - 
                    2*Power(t1,2)*(1 - 6*t2 + Power(t2,2)) + 
                    t1*(-3 + 25*t2 - 11*Power(t2,2) + Power(t2,3))) + 
                 Power(s2,2)*
                  (1 - 12*t2 + 6*Power(t2,2) - 44*Power(t2,3) + 
                    17*Power(t2,4) + 
                    Power(t1,3)*
                     (-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                    8*Power(t1,2)*
                     (-1 + 8*t2 - 7*Power(t2,2) + 2*Power(t2,3)) + 
                    t1*(4 - 7*t2 - 27*Power(t2,2) - 89*Power(t2,3) + 
                       7*Power(t2,4))) + 
                 s2*(9 - 49*t2 + 60*Power(t2,2) + 74*Power(t2,3) - 
                    53*Power(t2,4) - 9*Power(t2,5) + 
                    Power(t1,3)*
                     (8 - 59*t2 + 55*Power(t2,2) - 21*Power(t2,3) + 
                       Power(t2,4)) + 
                    t1*(-13 + 77*t2 + 31*Power(t2,2) + 
                       27*Power(t2,3) + 22*Power(t2,4)) + 
                    Power(t1,2)*
                     (-4 + 19*t2 + 26*Power(t2,2) + 68*Power(t2,3) - 
                       14*Power(t2,4) + Power(t2,5)))) + 
              Power(s1,2)*(5 - 43*t2 + 46*Power(t2,2) + 6*Power(t2,3) - 
                 19*Power(t2,4) + 5*Power(t2,5) + 
                 Power(t1,2)*
                  (-1 + 26*t2 + 83*Power(t2,2) - 6*Power(t2,3) - 
                    6*Power(t2,4)) + 
                 Power(t1,3)*
                  (-3 + 5*t2 + 12*Power(t2,2) - 17*Power(t2,3) + 
                    3*Power(t2,4)) + 
                 t1*(-5 + 44*t2 - 81*Power(t2,2) + Power(t2,3) + 
                    14*Power(t2,4) - 5*Power(t2,5)) + 
                 Power(s2,3)*(-1 + t2)*
                  (1 - 9*t2 + 2*Power(t2,2) + 
                    t1*(-1 - 3*t2 + 2*Power(t2,2))) - 
                 Power(s2,2)*
                  (1 - 14*t2 + 21*Power(t2,2) + 26*Power(t2,3) - 
                    2*Power(t2,4) + 
                    2*Power(t1,2)*
                     (2 - 5*t2 + 2*Power(t2,2) + Power(t2,3)) + 
                    t1*(-9 + 80*t2 - 165*Power(t2,2) - 4*Power(t2,3) + 
                       2*Power(t2,4))) + 
                 s2*(-4 + 12*t2 - 39*Power(t2,2) + 45*Power(t2,3) + 
                    19*Power(t2,4) - Power(t2,5) + 
                    3*Power(t1,3)*(1 - 4*t2 + 3*Power(t2,2)) + 
                    Power(t1,2)*
                     (2 - t2 - 110*Power(t2,2) + 13*Power(t2,3)) + 
                    t1*(-1 + 13*t2 - 44*Power(t2,2) - 22*Power(t2,3) - 
                       11*Power(t2,4) + Power(t2,5)))) + 
              Power(s,2)*(-3 - 2*t1 + 4*Power(t1,2) - 3*Power(t1,3) + 
                 25*t2 + 31*t1*t2 - 36*Power(t1,2)*t2 + 
                 12*Power(t1,3)*t2 + 22*Power(t2,2) - t1*Power(t2,2) + 
                 48*Power(t1,2)*Power(t2,2) - 
                 9*Power(t1,3)*Power(t2,2) + 19*Power(t2,3) + 
                 t1*Power(t2,3) - 36*Power(t1,2)*Power(t2,3) - 
                 15*Power(t2,4) + 3*t1*Power(t2,4) + 
                 4*Power(t1,2)*Power(t2,4) + 
                 Power(s2,2)*(-1 + t2)*
                  (t2*(3 + 16*t2 - 3*Power(t2,2)) + 
                    Power(t1,2)*(1 + 3*t2 - 2*Power(t2,2)) + 
                    t1*(-1 + 6*t2 - 18*Power(t2,2) + 3*Power(t2,3))) + 
                 Power(s1,2)*(-1 + t1)*
                  (3*t1*(-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                    s2*(1 - 3*t2 - 17*Power(t2,2) + 3*Power(t2,3)) - 
                    2*(2 - 11*t2 + 9*Power(t2,2) - 9*Power(t2,3) + 
                       Power(t2,4))) + 
                 s2*(-2 + 9*t2 - 61*Power(t2,2) + 10*Power(t2,3) - 
                    5*Power(t2,4) + Power(t2,5) + 
                    Power(t1,3)*
                     (2 - 5*t2 + 2*Power(t2,2) + Power(t2,3)) + 
                    Power(t1,2)*
                     (-3 + 32*t2 - 39*Power(t2,2) + 28*Power(t2,3) - 
                       2*Power(t2,4)) - 
                    t1*(1 - 8*t2 + 26*Power(t2,2) + 19*Power(t2,3) - 
                       7*Power(t2,4) + Power(t2,5))) + 
                 s1*(-1 - 7*t2 + 11*Power(t2,2) - 4*Power(t2,3) + 
                    18*Power(t2,4) - Power(t2,5) - 
                    Power(t1,3)*
                     (-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) - 
                    2*Power(t1,2)*
                     (-4 + 13*t2 - 5*Power(t2,2) - 5*Power(t2,3) + 
                       Power(t2,4)) + 
                    t1*(-4 + 8*t2 - 88*Power(t2,2) + 11*Power(t2,3) - 
                       8*Power(t2,4) + Power(t2,5)) + 
                    Power(s2,2)*(-1 + t2)*
                     (2 - 9*t2 + Power(t2,2) + 
                       t1*(-2 - 3*t2 + 3*Power(t2,2))) + 
                    s2*(5 - 16*t2 + 16*Power(t2,2) - 24*Power(t2,3) + 
                       3*Power(t2,4) - 
                       Power(t1,2)*
                        (5 - 11*t2 + 3*Power(t2,2) + 3*Power(t2,3)) + 
                       t1*(4 - 39*t2 + 111*Power(t2,2) + 
                         7*Power(t2,3) - 3*Power(t2,4))))) - 
              s*(5*t1 + 4*Power(t1,2) - 5*Power(t1,3) - 9*t2 - 
                 21*t1*t2 - 43*Power(t1,2)*t2 + 45*Power(t1,3)*t2 - 
                 17*Power(t2,2) - 37*t1*Power(t2,2) + 
                 17*Power(t1,2)*Power(t2,2) - 
                 55*Power(t1,3)*Power(t2,2) - 51*Power(t2,3) - 
                 19*t1*Power(t2,3) - 9*Power(t1,2)*Power(t2,3) + 
                 35*Power(t1,3)*Power(t2,3) + 25*Power(t2,4) - 
                 16*t1*Power(t2,4) + 19*Power(t1,2)*Power(t2,4) - 
                 4*Power(t1,3)*Power(t2,4) + 4*Power(t2,5) + 
                 8*t1*Power(t2,5) - 4*Power(t1,2)*Power(t2,5) + 
                 Power(s2,3)*(-1 + t2)*
                  (t2*(6 + 7*t2 - 3*Power(t2,2)) - 
                    Power(t1,2)*(1 - 6*t2 + Power(t2,2)) + 
                    t1*(1 - 10*Power(t2,2) + 3*Power(t2,3))) + 
                 Power(s2,2)*
                  (1 + 3*t2 - 12*Power(t2,2) - 41*Power(t2,3) - 
                    Power(t2,4) + 2*Power(t2,5) + 
                    Power(t1,3)*
                     (-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                    Power(t1,2)*
                     (-5 + 43*t2 - 44*Power(t2,2) + 25*Power(t2,3) - 
                       3*Power(t2,4)) + 
                    t1*(1 + 3*t2 - 65*Power(t2,2) + 23*Power(t2,3) + 
                       8*Power(t2,4) - 2*Power(t2,5))) + 
                 s2*(1 + 3*t2 + 33*Power(t2,2) + 107*Power(t2,3) - 
                    50*Power(t2,4) + 2*Power(t2,5) + 
                    Power(t1,3)*
                     (5 - 38*t2 + 34*Power(t2,2) - 18*Power(t2,3) + 
                       Power(t2,4)) + 
                    t1*(-10 + 58*t2 + 6*Power(t2,2) + 31*Power(t2,3) + 
                       32*Power(t2,4) - 5*Power(t2,5)) + 
                    Power(t1,2)*
                     (4 - 31*t2 + 67*Power(t2,2) - 32*Power(t2,3) - 
                       11*Power(t2,4) + 3*Power(t2,5))) + 
                 Power(s1,3)*(-1 + t1)*
                  (3*t1*(-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                    t2*(3 + 9*t2 + 5*Power(t2,2) - Power(t2,3) + 
                       2*s2*(-1 - 8*t2 + Power(t2,2)))) + 
                 Power(s1,2)*
                  (-2*Power(t1,3)*
                     (-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                    t2*(5 - 10*t2 + 28*Power(t2,2) + 10*Power(t2,3) - 
                       Power(t2,4)) - 
                    Power(t1,2)*
                     (-8 + 40*t2 + Power(t2,2) - 20*Power(t2,3) + 
                       3*Power(t2,4)) + 
                    t1*(-6 + 17*t2 - 63*Power(t2,2) - 30*Power(t2,3) + 
                       Power(t2,4) + Power(t2,5)) + 
                    Power(s2,2)*
                     (-3 + 20*t2 - Power(t2,2) + 
                       t1*(3 - 8*t2 - 15*Power(t2,2) + 4*Power(t2,3))) \
+ s2*(7 - 33*t2 + 32*Power(t2,2) - 59*Power(t2,3) + 5*Power(t2,4) - 
                       Power(t1,2)*
                        (6 - 29*t2 + 6*Power(t2,2) + Power(t2,3)) + 
                       t1*(3 - 40*t2 + 98*Power(t2,2) + 
                         40*Power(t2,3) - 5*Power(t2,4)))) + 
                 s1*(1 - 2*t2 + 56*Power(t2,2) + 8*Power(t2,3) - 
                    25*Power(t2,4) - 6*Power(t2,5) + 
                    Power(t1,3)*
                     (-6 + 17*t2 + 3*Power(t2,2) - 17*Power(t2,3) + 
                       3*Power(t2,4)) + 
                    Power(t1,2)*
                     (8 - 44*t2 + 142*Power(t2,2) - 41*Power(t2,3) - 
                       2*Power(t2,4) + Power(t2,5)) - 
                    t1*(11 - 89*t2 + 49*Power(t2,2) - 78*Power(t2,3) + 
                       8*Power(t2,4) + 3*Power(t2,5)) + 
                    Power(s2,3)*(-1 + t2)*
                     (1 - 9*t2 + 2*Power(t2,2) + 
                       t1*(-1 - 3*t2 + 2*Power(t2,2))) - 
                    Power(s2,2)*
                     (1 - 8*t2 + 36*Power(t2,2) - 16*Power(t2,3) + 
                       3*Power(t2,4) + 
                       Power(t1,2)*
                        (4 - t2 - 8*Power(t2,2) + 5*Power(t2,3)) + 
                       t1*(-9 + 77*t2 - 172*Power(t2,2) + 
                         23*Power(t2,3) + Power(t2,4))) + 
                    s2*(-5 + 2*t2 - 40*Power(t2,2) - 13*Power(t2,3) + 
                       41*Power(t2,4) - Power(t2,5) + 
                       Power(t1,3)*
                        (5 - 17*t2 + 11*Power(t2,2) + Power(t2,3)) + 
                       Power(t1,2)*
                        (-7 + 71*t2 - 169*Power(t2,2) + 41*Power(t2,3)) \
+ t1*(3 - 4*t2 - 66*Power(t2,2) - 97*Power(t2,3) - 13*Power(t2,4) + 
                         Power(t2,5))))))*Log(1 - t1))/Power(t1,3) - 
         2*(-1 + t1)*Power(-t1 + t2,3)*
          (-4*s2 + 3*Power(s2,2) + Power(s2,3) + 4*t1 - s2*t1 - 
            3*Power(s2,2)*t1 - 6*Power(t1,2) + 10*s2*Power(t1,2) - 
            3*Power(s2,2)*Power(t1,2) - Power(s2,3)*Power(t1,2) - 
            3*s2*Power(t1,3) + 3*Power(s2,2)*Power(t1,3) + 2*Power(t1,4) - 
            2*s2*Power(t1,4) + 2*t2 + 24*s2*t2 - 26*Power(s2,2)*t2 - 
            7*Power(s2,3)*t2 + 5*Power(s2,4)*t2 - 31*t1*t2 + 22*s2*t1*t2 + 
            28*Power(s2,2)*t1*t2 - 9*Power(s2,3)*t1*t2 - 
            5*Power(s2,4)*t1*t2 + 34*Power(t1,2)*t2 - 
            81*s2*Power(t1,2)*t2 + 27*Power(s2,2)*Power(t1,2)*t2 + 
            16*Power(s2,3)*Power(t1,2)*t2 + 5*Power(t1,3)*t2 + 
            21*s2*Power(t1,3)*t2 - 25*Power(s2,2)*Power(t1,3)*t2 - 
            14*Power(t1,4)*t2 + 14*s2*Power(t1,4)*t2 - 16*Power(t2,2) + 
            s2*Power(t2,2) - 11*Power(s2,2)*Power(t2,2) + 
            47*Power(s2,3)*Power(t2,2) - 7*Power(s2,4)*Power(t2,2) + 
            81*t1*Power(t2,2) - 52*s2*t1*Power(t2,2) - 
            54*Power(s2,2)*t1*Power(t2,2) - 
            21*Power(s2,3)*t1*Power(t2,2) + 7*Power(s2,4)*t1*Power(t2,2) - 
            29*Power(t1,2)*Power(t2,2) + 93*s2*Power(t1,2)*Power(t2,2) - 
            12*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
            14*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
            18*Power(t1,3)*Power(t2,2) - 16*s2*Power(t1,3)*Power(t2,2) + 
            21*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
            14*Power(t1,4)*Power(t2,2) - 14*s2*Power(t1,4)*Power(t2,2) + 
            22*Power(t2,3) - 119*s2*Power(t2,3) + 
            108*Power(s2,2)*Power(t2,3) - 46*Power(s2,3)*Power(t2,3) + 
            3*Power(s2,4)*Power(t2,3) + 5*t1*Power(t2,3) - 
            32*s2*t1*Power(t2,3) + 53*Power(s2,2)*t1*Power(t2,3) + 
            30*Power(s2,3)*t1*Power(t2,3) - 3*Power(s2,4)*t1*Power(t2,3) + 
            24*Power(t1,2)*Power(t2,3) - 44*s2*Power(t1,2)*Power(t2,3) - 
            22*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
            11*Power(t1,3)*Power(t2,3) + 9*s2*Power(t1,3)*Power(t2,3) + 
            Power(s2,2)*Power(t1,3)*Power(t2,3) - 
            2*Power(t1,4)*Power(t2,3) + 2*s2*Power(t1,4)*Power(t2,3) - 
            2*Power(t2,4) + 43*s2*Power(t2,4) - 
            28*Power(s2,2)*Power(t2,4) + 4*Power(s2,3)*Power(t2,4) - 
            Power(s2,4)*Power(t2,4) - 11*t1*Power(t2,4) - 
            19*s2*t1*Power(t2,4) + 13*Power(s2,2)*t1*Power(t2,4) + 
            Power(s2,3)*t1*Power(t2,4) + Power(s2,4)*t1*Power(t2,4) + 
            15*Power(t1,2)*Power(t2,4) + 5*s2*Power(t1,2)*Power(t2,4) - 
            9*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
            Power(s2,3)*Power(t1,2)*Power(t2,4) - 
            18*Power(t1,3)*Power(t2,4) + 7*s2*Power(t1,3)*Power(t2,4) - 
            8*Power(t2,5) + 7*s2*Power(t2,5) + 2*Power(s2,2)*Power(t2,5) + 
            Power(s2,3)*Power(t2,5) + 2*t1*Power(t2,5) + 
            2*s2*t1*Power(t2,5) - 5*Power(s2,2)*t1*Power(t2,5) - 
            Power(s2,3)*t1*Power(t2,5) - 6*Power(t1,2)*Power(t2,5) + 
            s2*Power(t1,2)*Power(t2,5) + 
            3*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
            4*Power(t1,3)*Power(t2,5) - 2*s2*Power(t1,3)*Power(t2,5) + 
            2*Power(t2,6) - 2*t1*Power(t2,6) + 
            Power(s1,4)*(-1 + t1)*(-2 + s2 + t1)*
             (-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) - 
            Power(s,3)*(-1 + t1)*(-1 + t2)*
             (-2 + t2 + 8*s2*t2 - Power(t2,2) - 11*s2*Power(t2,2) + 
               s2*Power(t2,3) + 
               s1*(2 - 3*(1 + 2*s2)*t2 + 2*(5 + s2)*Power(t2,2) - 
                  Power(t2,3) + t1*(1 - 6*t2 + Power(t2,2))) - 
               t1*(3 - 9*t2 + s2*(-2 + 3*t2 + Power(t2,2)))) + 
            Power(s1,3)*(-1 + t1)*
             (-1 - 2*t2 + 28*Power(t2,2) - 30*Power(t2,3) + 
               5*Power(t2,4) + 
               2*Power(s2,2)*t2*(-1 - 8*t2 + Power(t2,2)) - 
               Power(t1,2)*(-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               t1*(4 - 33*t2 + 5*Power(t2,2) + 9*Power(t2,3) - 
                  Power(t2,4)) + 
               s2*(1 - 11*t2 + 9*Power(t2,2) + 19*Power(t2,3) - 
                  2*Power(t2,4) + 
                  t1*(-3 + 23*t2 - 5*Power(t2,2) + Power(t2,3)))) + 
            s1*(-4 + 25*t2 + 12*Power(t2,2) - 60*Power(t2,3) + 
               26*Power(t2,4) + 3*Power(t2,5) - 2*Power(t2,6) + 
               Power(s2,4)*(-1 + t1)*
                (-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(-8 + 65*t2 - 63*Power(t2,2) + 
                  17*Power(t2,3) + 7*Power(t2,4) - 2*Power(t2,5)) + 
               Power(t1,2)*(14 - 104*t2 + 23*Power(t2,2) - 
                  57*Power(t2,3) + 11*Power(t2,4) + Power(t2,5)) + 
               2*t1*(1 - 7*t2 - 32*Power(t2,2) + 28*Power(t2,3) - 
                  10*Power(t2,4) + 3*Power(t2,5) + Power(t2,6)) + 
               Power(s2,3)*(-1 + t2)*
                (5 - 25*t2 + 21*Power(t2,2) - 5*Power(t2,3) - 
                  2*Power(t1,2)*(1 - 6*t2 + Power(t2,2)) + 
                  t1*(-3 + 25*t2 - 11*Power(t2,2) + Power(t2,3))) + 
               Power(s2,2)*(1 - 12*t2 + 6*Power(t2,2) - 44*Power(t2,3) + 
                  17*Power(t2,4) + 
                  Power(t1,3)*
                   (-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                  8*Power(t1,2)*
                   (-1 + 8*t2 - 7*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(4 - 7*t2 - 27*Power(t2,2) - 89*Power(t2,3) + 
                     7*Power(t2,4))) + 
               s2*(9 - 49*t2 + 60*Power(t2,2) + 74*Power(t2,3) - 
                  53*Power(t2,4) - 9*Power(t2,5) + 
                  Power(t1,3)*
                   (8 - 59*t2 + 55*Power(t2,2) - 21*Power(t2,3) + 
                     Power(t2,4)) + 
                  t1*(-13 + 77*t2 + 31*Power(t2,2) + 27*Power(t2,3) + 
                     22*Power(t2,4)) + 
                  Power(t1,2)*
                   (-4 + 19*t2 + 26*Power(t2,2) + 68*Power(t2,3) - 
                     14*Power(t2,4) + Power(t2,5)))) + 
            Power(s1,2)*(5 - 43*t2 + 46*Power(t2,2) + 6*Power(t2,3) - 
               19*Power(t2,4) + 5*Power(t2,5) + 
               Power(t1,2)*(-1 + 26*t2 + 83*Power(t2,2) - 
                  6*Power(t2,3) - 6*Power(t2,4)) + 
               Power(t1,3)*(-3 + 5*t2 + 12*Power(t2,2) - 
                  17*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(-5 + 44*t2 - 81*Power(t2,2) + Power(t2,3) + 
                  14*Power(t2,4) - 5*Power(t2,5)) + 
               Power(s2,3)*(-1 + t2)*
                (1 - 9*t2 + 2*Power(t2,2) + 
                  t1*(-1 - 3*t2 + 2*Power(t2,2))) - 
               Power(s2,2)*(1 - 14*t2 + 21*Power(t2,2) + 
                  26*Power(t2,3) - 2*Power(t2,4) + 
                  2*Power(t1,2)*
                   (2 - 5*t2 + 2*Power(t2,2) + Power(t2,3)) + 
                  t1*(-9 + 80*t2 - 165*Power(t2,2) - 4*Power(t2,3) + 
                     2*Power(t2,4))) + 
               s2*(-4 + 12*t2 - 39*Power(t2,2) + 45*Power(t2,3) + 
                  19*Power(t2,4) - Power(t2,5) + 
                  3*Power(t1,3)*(1 - 4*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (2 - t2 - 110*Power(t2,2) + 13*Power(t2,3)) + 
                  t1*(-1 + 13*t2 - 44*Power(t2,2) - 22*Power(t2,3) - 
                     11*Power(t2,4) + Power(t2,5)))) + 
            Power(s,2)*(-3 - 2*t1 + 4*Power(t1,2) - 3*Power(t1,3) + 
               25*t2 + 31*t1*t2 - 36*Power(t1,2)*t2 + 12*Power(t1,3)*t2 + 
               22*Power(t2,2) - t1*Power(t2,2) + 
               48*Power(t1,2)*Power(t2,2) - 9*Power(t1,3)*Power(t2,2) + 
               19*Power(t2,3) + t1*Power(t2,3) - 
               36*Power(t1,2)*Power(t2,3) - 15*Power(t2,4) + 
               3*t1*Power(t2,4) + 4*Power(t1,2)*Power(t2,4) + 
               Power(s2,2)*(-1 + t2)*
                (t2*(3 + 16*t2 - 3*Power(t2,2)) + 
                  Power(t1,2)*(1 + 3*t2 - 2*Power(t2,2)) + 
                  t1*(-1 + 6*t2 - 18*Power(t2,2) + 3*Power(t2,3))) + 
               Power(s1,2)*(-1 + t1)*
                (3*t1*(-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                  s2*(1 - 3*t2 - 17*Power(t2,2) + 3*Power(t2,3)) - 
                  2*(2 - 11*t2 + 9*Power(t2,2) - 9*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(-2 + 9*t2 - 61*Power(t2,2) + 10*Power(t2,3) - 
                  5*Power(t2,4) + Power(t2,5) + 
                  Power(t1,3)*(2 - 5*t2 + 2*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-3 + 32*t2 - 39*Power(t2,2) + 28*Power(t2,3) - 
                     2*Power(t2,4)) - 
                  t1*(1 - 8*t2 + 26*Power(t2,2) + 19*Power(t2,3) - 
                     7*Power(t2,4) + Power(t2,5))) + 
               s1*(-1 - 7*t2 + 11*Power(t2,2) - 4*Power(t2,3) + 
                  18*Power(t2,4) - Power(t2,5) - 
                  Power(t1,3)*(-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) - 
                  2*Power(t1,2)*
                   (-4 + 13*t2 - 5*Power(t2,2) - 5*Power(t2,3) + 
                     Power(t2,4)) + 
                  t1*(-4 + 8*t2 - 88*Power(t2,2) + 11*Power(t2,3) - 
                     8*Power(t2,4) + Power(t2,5)) + 
                  Power(s2,2)*(-1 + t2)*
                   (2 - 9*t2 + Power(t2,2) + 
                     t1*(-2 - 3*t2 + 3*Power(t2,2))) + 
                  s2*(5 - 16*t2 + 16*Power(t2,2) - 24*Power(t2,3) + 
                     3*Power(t2,4) - 
                     Power(t1,2)*
                      (5 - 11*t2 + 3*Power(t2,2) + 3*Power(t2,3)) + 
                     t1*(4 - 39*t2 + 111*Power(t2,2) + 7*Power(t2,3) - 
                        3*Power(t2,4))))) - 
            s*(5*t1 + 4*Power(t1,2) - 5*Power(t1,3) - 9*t2 - 21*t1*t2 - 
               43*Power(t1,2)*t2 + 45*Power(t1,3)*t2 - 17*Power(t2,2) - 
               37*t1*Power(t2,2) + 17*Power(t1,2)*Power(t2,2) - 
               55*Power(t1,3)*Power(t2,2) - 51*Power(t2,3) - 
               19*t1*Power(t2,3) - 9*Power(t1,2)*Power(t2,3) + 
               35*Power(t1,3)*Power(t2,3) + 25*Power(t2,4) - 
               16*t1*Power(t2,4) + 19*Power(t1,2)*Power(t2,4) - 
               4*Power(t1,3)*Power(t2,4) + 4*Power(t2,5) + 
               8*t1*Power(t2,5) - 4*Power(t1,2)*Power(t2,5) + 
               Power(s2,3)*(-1 + t2)*
                (t2*(6 + 7*t2 - 3*Power(t2,2)) - 
                  Power(t1,2)*(1 - 6*t2 + Power(t2,2)) + 
                  t1*(1 - 10*Power(t2,2) + 3*Power(t2,3))) + 
               Power(s2,2)*(1 + 3*t2 - 12*Power(t2,2) - 41*Power(t2,3) - 
                  Power(t2,4) + 2*Power(t2,5) + 
                  Power(t1,3)*(-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-5 + 43*t2 - 44*Power(t2,2) + 25*Power(t2,3) - 
                     3*Power(t2,4)) + 
                  t1*(1 + 3*t2 - 65*Power(t2,2) + 23*Power(t2,3) + 
                     8*Power(t2,4) - 2*Power(t2,5))) + 
               s2*(1 + 3*t2 + 33*Power(t2,2) + 107*Power(t2,3) - 
                  50*Power(t2,4) + 2*Power(t2,5) + 
                  Power(t1,3)*
                   (5 - 38*t2 + 34*Power(t2,2) - 18*Power(t2,3) + 
                     Power(t2,4)) + 
                  t1*(-10 + 58*t2 + 6*Power(t2,2) + 31*Power(t2,3) + 
                     32*Power(t2,4) - 5*Power(t2,5)) + 
                  Power(t1,2)*
                   (4 - 31*t2 + 67*Power(t2,2) - 32*Power(t2,3) - 
                     11*Power(t2,4) + 3*Power(t2,5))) + 
               Power(s1,3)*(-1 + t1)*
                (3*t1*(-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                  t2*(3 + 9*t2 + 5*Power(t2,2) - Power(t2,3) + 
                     2*s2*(-1 - 8*t2 + Power(t2,2)))) + 
               Power(s1,2)*(-2*Power(t1,3)*
                   (-1 + 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
                  t2*(5 - 10*t2 + 28*Power(t2,2) + 10*Power(t2,3) - 
                     Power(t2,4)) - 
                  Power(t1,2)*
                   (-8 + 40*t2 + Power(t2,2) - 20*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  t1*(-6 + 17*t2 - 63*Power(t2,2) - 30*Power(t2,3) + 
                     Power(t2,4) + Power(t2,5)) + 
                  Power(s2,2)*
                   (-3 + 20*t2 - Power(t2,2) + 
                     t1*(3 - 8*t2 - 15*Power(t2,2) + 4*Power(t2,3))) + 
                  s2*(7 - 33*t2 + 32*Power(t2,2) - 59*Power(t2,3) + 
                     5*Power(t2,4) - 
                     Power(t1,2)*
                      (6 - 29*t2 + 6*Power(t2,2) + Power(t2,3)) + 
                     t1*(3 - 40*t2 + 98*Power(t2,2) + 40*Power(t2,3) - 
                        5*Power(t2,4)))) + 
               s1*(1 - 2*t2 + 56*Power(t2,2) + 8*Power(t2,3) - 
                  25*Power(t2,4) - 6*Power(t2,5) + 
                  Power(t1,3)*
                   (-6 + 17*t2 + 3*Power(t2,2) - 17*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  Power(t1,2)*
                   (8 - 44*t2 + 142*Power(t2,2) - 41*Power(t2,3) - 
                     2*Power(t2,4) + Power(t2,5)) - 
                  t1*(11 - 89*t2 + 49*Power(t2,2) - 78*Power(t2,3) + 
                     8*Power(t2,4) + 3*Power(t2,5)) + 
                  Power(s2,3)*(-1 + t2)*
                   (1 - 9*t2 + 2*Power(t2,2) + 
                     t1*(-1 - 3*t2 + 2*Power(t2,2))) - 
                  Power(s2,2)*
                   (1 - 8*t2 + 36*Power(t2,2) - 16*Power(t2,3) + 
                     3*Power(t2,4) + 
                     Power(t1,2)*
                      (4 - t2 - 8*Power(t2,2) + 5*Power(t2,3)) + 
                     t1*(-9 + 77*t2 - 172*Power(t2,2) + 23*Power(t2,3) + 
                        Power(t2,4))) + 
                  s2*(-5 + 2*t2 - 40*Power(t2,2) - 13*Power(t2,3) + 
                     41*Power(t2,4) - Power(t2,5) + 
                     Power(t1,3)*
                      (5 - 17*t2 + 11*Power(t2,2) + Power(t2,3)) + 
                     Power(t1,2)*
                      (-7 + 71*t2 - 169*Power(t2,2) + 41*Power(t2,3)) + 
                     t1*(3 - 4*t2 - 66*Power(t2,2) - 97*Power(t2,3) - 
                        13*Power(t2,4) + Power(t2,5))))))*R1n(t2,t1)))/
     ((-1 + s1)*(-1 + s2)*(-s + s2 - t1)*Power(-1 + t1,3)*Power(-1 + t2,3)*
       t2*(s - s1 + t2)));
    return a;
};
