#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_1_m231_3_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(-8*Power(s1,6)*(-1 + s2)*(-1 + t1)*Power(t1,2)*t2*
          (-t1 + 2*Power(t1,2) + t2 - 3*t1*t2 + Power(t2,2)) - 
         Power(s1,5)*t1*(5*Power(t1,6)*t2 + 
            Power(t1,5)*(2 + (-66 + 5*s + 42*s2)*t2 - 11*Power(t2,2)) + 
            Power(t1,4)*(-2 + 
               (121 - 5*s - 93*s2 - 17*s*s2 + 5*Power(s2,2))*t2 + 
               (136 - 10*s - 87*s2)*Power(t2,2) + 6*Power(t2,3)) + 
            Power(t1,3)*(-2 + 
               (-76 - 20*s + 62*s2 + 37*s*s2 - 3*Power(s2,2))*t2 + 
               (-185 + 18*s + 117*s2 + 26*s*s2 - 2*Power(s2,2))*
                Power(t2,2) + (-92 + 5*s + 67*s2)*Power(t2,3)) + 
            Power(t1,2)*(2 + (6 + 8*s + 7*s2 - 8*s*s2 - 7*Power(s2,2))*
                t2 + (74 + 24*s - 37*s2 - 58*s*s2 + Power(s2,2))*
                Power(t2,2) + 
               (88 - 13*s - 56*s2 - 9*s*s2 + 3*Power(s2,2))*
                Power(t2,3) - 16*(-1 + s2)*Power(t2,4)) + 
            Power(t2,2)*(Power(s2,2)*(1 + t2) - 8*t2*(1 + t2) + 
               2*s2*t2*(5 + 4*t2)) + 
            t1*t2*(Power(s2,2)*(5 - 4*Power(t2,2)) - 
               2*(-5 + (7 + 4*s)*t2 + (-3 + 2*s)*Power(t2,2) + 
                  4*Power(t2,3)) + 
               s2*(-18 + (7 + 8*s)*t2 + 21*(-1 + s)*Power(t2,2) + 
                  8*Power(t2,3)))) + 
         Power(s1,3)*(-9*Power(t1,9)*t2 + 
            Power(t1,8)*(-12 + (36 + 7*s - 6*s2)*t2 + 23*Power(t2,2)) + 
            Power(t1,7)*(12 + 
               (-149 + 11*Power(s,2) + 171*s2 - 17*Power(s2,2) + 
                  s*(-106 + 39*s2))*t2 + 
               (-100 - 28*s + 49*s2)*Power(t2,2) - 18*Power(t2,3)) + 
            Power(t1,6)*(16 - 
               (-192 + Power(s,3) + 143*s2 + 63*Power(s2,2) - 
                  4*Power(s2,3) + Power(s,2)*(101 + 7*s2) + 
                  s*(-129 - 7*s2 + 10*Power(s2,2)))*t2 + 
               (64 - 26*Power(s,2) + s*(244 - 103*s2) - 167*s2 + 
                  4*Power(s2,2))*Power(t2,2) + 
               3*(38 + 13*s - 39*s2)*Power(t2,3) + 4*Power(t2,4)) + 
            Power(t1,5)*(-16 + 
               (-13*Power(s,3) + Power(s,2)*(3 + 70*s2) + 
                  s*(43 - 125*s2 + 3*Power(s2,2)) + 
                  2*(1 - 92*s2 + 57*Power(s2,2) + 8*Power(s2,3)))*t2 + 
               (233 + 9*Power(s,3) - 128*s2 + 96*Power(s2,2) + 
                  8*Power(s2,3) + 2*Power(s,2)*(99 + 2*s2) + 
                  s*(-307 - 45*s2 + 17*Power(s2,2)))*Power(t2,2) + 
               (320 + 22*Power(s,2) - 54*s2 - 11*Power(s2,2) + 
                  s*(-166 + 97*s2))*Power(t2,3) + 
               (-60 - 22*s + 88*s2)*Power(t2,4)) + 
            Power(t1,4)*(-4 + 
               (-96 + 2*Power(s,3) - 39*Power(s,2)*(-1 + s2) + 93*s2 + 
                  62*Power(s2,2) - 54*Power(s2,3) + 
                  s*(-65 + 74*s2 + 16*Power(s2,2)))*t2 + 
               (-387 + 31*Power(s,3) + Power(s,2)*(22 - 144*s2) + 
                  425*s2 - 78*Power(s2,2) - 39*Power(s2,3) + 
                  s*(-218 + 448*s2 + 9*Power(s2,2)))*Power(t2,2) + 
               (-743 - 15*Power(s,3) + 332*s2 - 33*Power(s2,2) + 
                  8*Power(s2,3) + Power(s,2)*(-77 + 13*s2) + 
                  s*(288 + 17*s2 - 12*Power(s2,2)))*Power(t2,3) + 
               (-417 + 16*s - 10*Power(s,2) + 234*s2 - 33*s*s2 + 
                  21*Power(s2,2))*Power(t2,4) + 
               4*(4 + s - 5*s2)*Power(t2,5)) + 
            Power(t1,3)*(4 + (16 + 115*s2 - 131*Power(s2,2) + 
                  44*Power(s2,3) + s*(4 - 7*s2 - 9*Power(s2,2)))*t2 + 
               (79 - 4*Power(s,3) + 26*s2 - 99*Power(s2,2) + 
                  49*Power(s2,3) + Power(s,2)*(-74 + 68*s2) + 
                  s*(165 - 161*s2 - 45*Power(s2,2)))*Power(t2,2) - 
               (23*Power(s,3) + Power(s,2)*(48 - 78*s2) + 
                  s*(-242 + 490*s2 + 11*Power(s2,2)) + 
                  3*(-160 + 93*s2 - 18*Power(s2,2) + 5*Power(s2,3)))*
                Power(t2,3) + 
               (7*Power(s,3) - 2*Power(s,2)*(18 + 5*s2) + 
                  5*s*(-23 + 5*s2 + Power(s2,2)) - 
                  2*(-289 + 161*s2 + 5*Power(s2,2) + Power(s2,3)))*
                Power(t2,4) + 
               (124 + 12*s + 3*Power(s,2) - 108*s2 - 3*Power(s2,2))*
                Power(t2,5)) - 
            s2*Power(t2,3)*(-1 + (2 + s)*t2 - (-3 + s)*Power(t2,2) + 
               Power(s2,2)*(3 + 2*t2) + s2*t2*(2 - s + 2*t2)) + 
            t1*Power(t2,2)*(-6 - 4*(4 + s)*t2 + 
               (-4 + 11*s + 4*Power(s,2))*Power(t2,2) + 
               (-22 + 15*s + 5*Power(s,2))*Power(t2,3) + 8*Power(t2,4) + 
               Power(s2,3)*(-5 + 7*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(25 + (3 - 11*s)*t2 + 
                  (4 - 5*s)*Power(t2,2) + 7*Power(t2,3)) + 
               s2*(-3 + (41 + 3*s)*t2 + 
                  (30 + s - 10*Power(s,2))*Power(t2,2) + 
                  (30 - 33*s)*Power(t2,3) - 8*Power(t2,4))) + 
            Power(t1,2)*t2*(8 + 94*t2 + 
               (-137 - 111*s + 31*Power(s,2) + 2*Power(s,3))*
                Power(t2,2) + 
               (-101 - 82*s + 18*Power(s,2) + 5*Power(s,3))*Power(t2,3) + 
               (-118 + 5*s + 16*Power(s,2))*Power(t2,4) - 8*Power(t2,5) + 
               Power(s2,3)*(-10 - 13*t2 + 3*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,2)*(35 + (52 + 19*s)*t2 + 
                  (-13 + 34*s)*Power(t2,2) - (13 + s)*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s2*(-46 + (-202 + 5*s)*t2 + 
                  (76 + 85*s - 19*Power(s,2))*Power(t2,2) - 
                  4*(7 - 50*s + Power(s,2))*Power(t2,3) + 
                  (101 - 4*s)*Power(t2,4) + 8*Power(t2,5)))) + 
         Power(s1,2)*(-2*Power(t1,10)*t2 + 
            Power(t1,9)*(-8 + (-20 + 9*s + 2*s2)*t2 + 6*Power(t2,2)) + 
            Power(t1,8)*(8 + (-10 - 129*s + 4*Power(s,2) + 102*s2 + 
                  5*s*s2 - 6*Power(s2,2))*t2 + 
               (42 - 25*s + 8*s2)*Power(t2,2) - 6*Power(t2,3)) + 
            Power(t1,7)*(16 - 
               (-79 + Power(s,3) + 46*s2 + 87*Power(s2,2) - 
                  2*Power(s2,3) + Power(s,2)*(127 + 11*s2) + 
                  s*(-118 - 97*s2 + 2*Power(s2,2)))*t2 + 
               (-70 + 5*Power(s,2) - 166*s2 - 5*Power(s2,2) - 
                  7*s*(-39 + 7*s2))*Power(t2,2) + 
               (-16 + 29*s - 43*s2)*Power(t2,3) + 2*Power(t2,4)) + 
            Power(t1,6)*(-18 + 
               (17 - 28*Power(s,3) - 193*s2 + 113*Power(s2,2) + 
                  29*Power(s2,3) + Power(s,2)*(25 + 101*s2) + 
                  s*(103 - 205*s2 - 18*Power(s2,2)))*t2 + 
               (281 + 14*Power(s,3) - 258*s2 + 154*Power(s2,2) + 
                  7*Power(s2,3) + Power(s,2)*(247 + 5*s2) + 
                  s*(-91 - 229*s2 + 10*Power(s2,2)))*Power(t2,2) + 
               (240 - 20*Power(s,2) + 76*s2 - 11*Power(s2,2) + 
                  s*(-101 + 91*s2))*Power(t2,3) + 
               (-10 - 19*s + 47*s2)*Power(t2,4)) + 
            Power(t1,5)*(-6 + 
               (-102 + 3*Power(s,3) + Power(s,2)*(73 - 78*s2) + 8*s2 + 
                  123*Power(s2,2) - 84*Power(s2,3) + 
                  s*(-96 + 125*s2 + 38*Power(s2,2)))*t2 + 
               (75*Power(s,3) - 6*Power(s,2)*(-23 + 36*s2) + 
                  s*(-431 + 432*s2 + 70*Power(s2,2)) + 
                  5*(-93 + 97*s2 + 5*Power(s2,2) - 14*Power(s2,3)))*
                Power(t2,2) - 
               (545 + 26*Power(s,3) + Power(s,2)*(55 - 26*s2) - 
                  345*s2 + 67*Power(s2,2) - 15*Power(s2,3) + 
                  s*(195 - 194*s2 + 21*Power(s2,2)))*Power(t2,3) + 
               (-358 + 9*Power(s,2) + 158*s2 + 32*Power(s2,2) - 
                  5*s*(21 + 11*s2))*Power(t2,4) + 
               2*(2 + 3*s - 7*s2)*Power(t2,5)) + 
            Power(t1,4)*(10 + 
               (20 + Power(s,2) + 2*Power(s,3) + 231*s2 - 
                  207*Power(s2,2) + 73*Power(s2,3) - 
                  2*s*(4 + 17*s2 + 9*Power(s2,2)))*t2 + 
               (52 + Power(s,3) + 406*s2 - 327*Power(s2,2) + 
                  77*Power(s2,3) + Power(s,2)*(-118 + 61*s2) + 
                  s*(143 - 65*s2 - 127*Power(s2,2)))*Power(t2,2) - 
               (-421 + 73*Power(s,3) + Power(s,2)*(337 - 136*s2) + 
                  414*s2 - 20*Power(s2,2) + 43*Power(s2,3) + 
                  s*(-717 + 439*s2 + 61*Power(s2,2)))*Power(t2,3) + 
               (345 + 14*Power(s,3) - 211*s2 - 22*Power(s2,2) - 
                  7*Power(s2,3) - Power(s,2)*(126 + 23*s2) + 
                  s*(190 - 89*s2 + 16*Power(s2,2)))*Power(t2,4) + 
               2*(119 + Power(s,2) - 102*s2 - 5*Power(s2,2) + 
                  s*(33 + 4*s2))*Power(t2,5)) + 
            Power(t1,3)*(-2 + 
               (19 - 110*s2 + 64*Power(s2,2) - 20*Power(s2,3) + 
                  3*s*(1 + 4*s2))*t2 + 
               (196 - 6*Power(s,3) - 404*s2 - 47*Power(s2,2) + 
                  38*Power(s2,3) + Power(s,2)*(-44 + 30*s2) + 
                  s*(105 - 30*s2 + 44*Power(s2,2)))*Power(t2,2) + 
               (73 - 15*Power(s,3) - 301*s2 + 83*Power(s2,2) + 
                  50*Power(s2,3) + 3*Power(s,2)*(5 + 38*s2) + 
                  s*(9 - 290*s2 + 111*Power(s2,2)))*Power(t2,3) + 
               (113 + 33*Power(s,3) + Power(s,2)*(151 - 28*s2) - 
                  29*s2 + 35*Power(s2,2) + Power(s2,3) + 
                  s*(-564 + 399*s2 + 8*Power(s2,2)))*Power(t2,4) + 
               (-18 - Power(s,3) + 55*s2 - 29*Power(s2,2) + 
                  Power(s2,3) + Power(s,2)*(64 + 3*s2) + 
                  s*(-17 + 27*s2 - 3*Power(s2,2)))*Power(t2,5) + 
               (-50 - 4*s + 52*s2)*Power(t2,6)) + 
            s2*Power(t2,3)*(-1 + (-1 + s)*t2 - 7*Power(t2,2) - 
               (-1 + s)*Power(t2,3) + 
               Power(s2,2)*(2 - t2 + Power(t2,2)) + 
               s2*(1 + t2)*(-1 - (4 + s)*t2 + Power(t2,2))) - 
            t1*Power(t2,2)*(9 + (44 - 7*s)*t2 + 
               (-99 - 81*s + 42*Power(s,2) + 2*Power(s,3))*Power(t2,2) + 
               (-102 - 7*s + 2*Power(s,2) + 4*Power(s,3))*Power(t2,3) + 
               (-68 + 3*s + 9*Power(s,2))*Power(t2,4) + 
               Power(s2,3)*(-10 - 5*t2 + 11*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(41 + 2*(36 + s)*t2 - 
                  (17 + 2*s)*Power(t2,2) - 3*(-1 + s)*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s2*(-37 - (117 + 10*s)*t2 + 
                  (107 + 82*s - 30*Power(s,2))*Power(t2,2) + 
                  (53 + 75*s - 2*Power(s,2))*Power(t2,3) + 
                  (66 - 13*s)*Power(t2,4))) + 
            Power(t1,2)*t2*(-1 - (33 + 10*s)*t2 + 
               (-123 - 178*s + 85*Power(s,2) + 6*Power(s,3))*
                Power(t2,2) + 
               (-191 - 63*s + 32*Power(s,2) + 15*Power(s,3))*
                Power(t2,3) + 
               (-326 + 178*s + 32*Power(s,2) - 7*Power(s,3))*
                Power(t2,4) - (18 + 5*s + 3*Power(s,2))*Power(t2,5) - 
               Power(s2,3)*t2*
                (62 + 29*t2 - 18*Power(t2,2) + Power(t2,3)) + 
               s2*(6 - (108 + 23*s)*t2 + 
                  (221 + 146*s - 60*Power(s,2))*Power(t2,2) + 
                  (143 + 306*s - 99*Power(s,2))*Power(t2,3) + 
                  (223 - 200*s + 7*Power(s,2))*Power(t2,4) + 
                  13*Power(t2,5)) + 
               Power(s2,2)*t2*
                (241 + 48*t2 - 57*Power(t2,2) + 45*Power(t2,3) + 
                  3*Power(t2,4) + 
                  s*(3 - 27*t2 - 25*Power(t2,2) + Power(t2,3))))) + 
         Power(s1,4)*(-12*Power(t1,8)*t2 + 
            s2*Power(t2,3)*(s2 + Power(s2,2) + 2*t2 + s2*t2) + 
            Power(t1,7)*(-8 + (92 - 5*s - 34*s2)*t2 + 28*Power(t2,2)) + 
            Power(t1,6)*(8 + (-218 - 21*s + 6*Power(s,2) + 164*s2 + 
                  47*s*s2 - 16*Power(s2,2))*t2 + 
               (-232 + 3*s + 106*s2)*Power(t2,2) - 18*Power(t2,3)) + 
            Power(t1,5)*(8 - (-192 + 148*s2 + 9*Power(s2,2) - 
                  2*Power(s2,3) + Power(s,2)*(26 + s2) + 
                  s*(-73 + 77*s2 + 6*Power(s2,2)))*t2 + 
               (278 - 19*Power(s,2) + s*(29 - 87*s2) - 126*s2 + 
                  7*Power(s2,2))*Power(t2,2) + 
               (212 + 9*s - 139*s2)*Power(t2,3) + 2*Power(t2,4)) + 
            Power(t1,4)*(-8 + 
               (-10 - 69*s2 + 47*Power(s2,2) + Power(s2,3) + 
                  Power(s,2)*(-4 + 13*s2) + 
                  s*(-19 - 3*s2 + 8*Power(s2,2)))*t2 + 
               (-68 - 13*s2 + 8*Power(s2,2) + 3*Power(s2,3) + 
                  Power(s,2)*(47 + 2*s2) + 
                  s*(-138 + 147*s2 + 8*Power(s2,2)))*Power(t2,2) + 
               (-72 + 20*Power(s,2) + 4*s2 - 9*Power(s2,2) + 
                  s*(-11 + 49*s2))*Power(t2,3) + 
               (-68 - 7*s + 63*s2)*Power(t2,4)) + 
            t1*Power(t2,2)*(-(Power(s2,3)*t2) + 
               2*Power(s2,2)*(1 + t2 + s*t2 - Power(t2,2)) - 
               2*(-5 + (11 + 4*s)*t2 + (13 + 2*s)*Power(t2,2) + 
                  8*Power(t2,3)) + 
               s2*(-19 + (17 + 9*s)*t2 + 2*(7 + 10*s)*Power(t2,2) + 
                  16*Power(t2,3))) + 
            Power(t1,2)*t2*(8 + 2*(11 + 6*s)*t2 + 
               (102 + 13*s - 4*Power(s,2))*Power(t2,2) + 
               (134 - 28*s - 5*Power(s,2))*Power(t2,3) + 8*Power(t2,4) + 
               Power(s2,3)*(5 + 3*t2 - Power(t2,2)) + 
               Power(s2,2)*(-15 + 4*t2 - 5*Power(t2,3)) - 
               s2*(-7 + 3*(11 + 6*s)*t2 + 
                  (84 + 59*s - 13*Power(s,2))*Power(t2,2) + 
                  (111 - 25*s)*Power(t2,3) + 8*Power(t2,4))) - 
            Power(t1,3)*t2*(52 + 38*t2 + 202*Power(t2,2) + 
               42*Power(t2,3) - 8*Power(t2,4) + 
               Power(s2,3)*(8 + 6*t2 - Power(t2,2)) + 
               Power(s2,2)*(7 + 21*t2 - 6*Power(t2,2) - 6*Power(t2,3)) + 
               s2*(-80 - 85*t2 - 202*Power(t2,2) - 32*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s,2)*t2*
                (-8 + 16*t2 + 7*Power(t2,2) + s2*(26 + t2)) + 
               s*(4 - 10*t2 - 93*Power(t2,2) - 3*Power(t2,3) + 
                  2*Power(s2,2)*(1 + 4*t2 + Power(t2,2)) + 
                  s2*(-9 - 42*t2 + 95*Power(t2,2) + 9*Power(t2,3))))) + 
         t2*(Power(t1,9)*(1 - 3*Power(s,2) + Power(s,3) + s*(3 - 2*t2) + 
               14*t2) + Power(t1,3)*
             (4 - (11 + 12*Power(s,2) - 101*s2 + 29*Power(s2,2) - 
                  10*Power(s2,3) + 4*s*(1 + 6*s2))*t2 + 
               (-2 + 24*Power(s,3) + 85*s2 + 306*Power(s2,2) - 
                  117*Power(s2,3) - 6*Power(s,2)*(-29 + 20*s2) + 
                  s*(-295 + 30*s2 + 6*Power(s2,2)))*Power(t2,2) + 
               (-83 + 84*Power(s,3) + Power(s,2)*(36 - 192*s2) + 
                  379*s2 - 77*Power(s2,2) - 89*Power(s2,3) + 
                  s*(-529 + 468*s2 - 30*Power(s2,2)))*Power(t2,3) - 
               (136 + 102*Power(s,3) + Power(s,2)*(230 - 116*s2) + 
                  165*s2 + 91*Power(s2,2) - Power(s2,3) + 
                  s*(-465 + 9*s2 + 23*Power(s2,2)))*Power(t2,4) - 
               (5*Power(s,2) + s*(-43 + 23*s2) + 
                  4*(7 + 26*s2 - 5*Power(s2,2)))*Power(t2,5) + 
               12*(1 + s - s2)*Power(t2,6)) - 
            Power(t1,8)*(-3 + (17 + 23*s2)*t2 + 
               2*(16 + s2)*Power(t2,2) + Power(s,3)*(2 + 4*t2) - 
               Power(s,2)*(9 + (10 + 3*s2)*t2) + 
               s*(12 + (-43 + 2*s2)*t2 - 4*Power(t2,2))) - 
            4*s2*Power(t2,3)*(1 + t2)*
             (-1 - s*(-1 + t2)*t2 + Power(t2,2) + Power(s2,2)*(2 + t2) + 
               s2*(-1 - s*t2 + Power(t2,2))) + 
            Power(t1,7)*(-14 + 9*(-1 - 2*s2 + 4*Power(s2,2))*t2 + 
               (-5 + 51*s2 + Power(s2,2))*Power(t2,2) + 
               (20 + 6*s2)*Power(t2,3) + 
               Power(s,3)*(-1 + 7*t2 + 6*Power(t2,2)) - 
               Power(s,2)*(3 + 4*(-7 + s2)*t2 + 
                  3*(7 + 3*s2)*Power(t2,2)) - 
               s*(-12 + (21 + 26*s2 + 2*Power(s2,2))*t2 + 
                  (112 - 19*s2 - 3*Power(s2,2))*Power(t2,2) + 
                  4*Power(t2,3))) + 
            Power(t1,6)*(10 - 
               (8 - 95*s2 + 39*Power(s2,2) + 14*Power(s2,3))*t2 + 
               (45 + 100*s2 - 42*Power(s2,2) - 2*Power(s2,3))*
                Power(t2,2) + 
               (83 - 44*s2 + 9*Power(s2,2) + Power(s2,3))*Power(t2,3) - 
               6*s2*Power(t2,4) + 
               Power(s,3)*(2 + 23*t2 - 13*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s,2)*(-9 - (24 + 41*s2)*t2 + 
                  3*(-37 + 7*s2)*Power(t2,2) + (25 + 9*s2)*Power(t2,3)) \
+ s*(6 + (-55 + 67*s2 + 13*Power(s2,2))*t2 + 
                  (-84 + 44*s2 - 13*Power(s2,2))*Power(t2,2) + 
                  (76 - 40*s2 - 6*Power(s2,2))*Power(t2,3) + 
                  4*Power(t2,4))) + 
            4*t1*Power(t2,2)*(8 + (6 - 2*s - 3*Power(s,2))*t2 + 
               (-11 - 60*s + 46*Power(s,2) + 3*Power(s,3))*Power(t2,2) + 
               s*(16 + s + 3*Power(s,2))*Power(t2,3) + 
               (-3 + 2*s + 9*Power(s,2))*Power(t2,4) + 
               Power(s2,3)*(-10 - 11*t2 + 10*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,2)*(41 + (61 + 2*s)*t2 - 
                  5*(5 + s)*Power(t2,2) - 6*(-1 + s)*Power(t2,3) + 
                  4*Power(t2,4)) - 
               s2*(37 + 4*(11 + s)*t2 + 
                  (-46 - 36*s + 33*Power(s,2))*Power(t2,2) + 
                  (16 + s - Power(s,2))*Power(t2,3) + 
                  (-3 + 13*s)*Power(t2,4))) + 
            Power(t1,5)*(9 + (54 + 9*s2 - 59*Power(s2,2) + 
                  38*Power(s2,3))*t2 + 
               (27 - 46*s2 - 123*Power(s2,2) + 27*Power(s2,3))*
                Power(t2,2) - 
               (171 + 177*s2 + 3*Power(s2,2) + 13*Power(s2,3))*
                Power(t2,3) - 
               (64 + 3*s2 + 17*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
               2*(-1 + s2)*Power(t2,5) + 
               Power(s,3)*t2*
                (-8 - 73*t2 + 13*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(6 + (2 + 48*s2)*t2 + 
                  (-89 + 94*s2)*Power(t2,2) + 
                  (121 - 30*s2)*Power(t2,3) - 3*(4 + s2)*Power(t2,4)) + 
               s*(-15 + (7 - 72*s2 - 20*Power(s2,2))*t2 + 
                  (195 - 13*s2 - 5*Power(s2,2))*Power(t2,2) + 
                  (378 - 33*s2 + 32*Power(s2,2))*Power(t2,3) + 
                  (9 + 31*s2 + 3*Power(s2,2))*Power(t2,4) - 2*Power(t2,5)\
)) + Power(t1,4)*(-13 - (35 + 140*s2 - 91*Power(s2,2) + 34*Power(s2,3))*
                t2 + (-5 - 369*s2 + 195*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,2) + 
               (91 + 67*s2 + 152*Power(s2,2) + 32*Power(s2,3))*
                Power(t2,3) + 
               (160 + 183*s2 + 19*Power(s2,2) + 9*Power(s2,3))*
                Power(t2,4) + 
               (-10 + 31*s2 + 7*Power(s2,2))*Power(t2,5) - 
               Power(s,3)*t2*
                (6 + 28*t2 - 117*Power(t2,2) + 5*Power(t2,3)) + 
               s*(6 + (32 + 57*s2 + 9*Power(s2,2))*t2 + 
                  (286 - 124*s2 + 21*Power(s2,2))*Power(t2,2) - 
                  (443 + 136*s2 + 5*Power(s2,2))*Power(t2,3) + 
                  (-332 + 38*s2 - 17*Power(s2,2))*Power(t2,4) - 
                  (31 + 8*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(-4 - t2 + 322*Power(t2,2) - 
                  54*Power(t2,3) + Power(t2,4) + 
                  s2*(-6 + 26*t2 - 117*Power(t2,2) + 13*Power(t2,3)))) + 
            Power(t1,2)*t2*(Power(s2,3)*t2*
                (130 + 121*t2 - 37*Power(t2,2) - 4*Power(t2,3)) + 
               s2*(-24 + (329 + 44*s)*t2 + 
                  (-59 - 231*s + 258*Power(s,2))*Power(t2,2) + 
                  (-197 - 272*s + 114*Power(s,2))*Power(t2,3) + 
                  (139 + 143*s - 52*Power(s,2))*Power(t2,4) + 
                  4*Power(t2,5)) + 
               2*(6 + 3*(-10 + s + 4*Power(s,2))*t2 + 
                  (18 + 259*s - 180*Power(s,2) - 15*Power(s,3))*
                   Power(t2,2) + 
                  (42 + 83*s - 16*Power(s,2) - 31*Power(s,3))*
                   Power(t2,3) + 
                  (20 - 91*s - 6*Power(s,2) + 18*Power(s,3))*
                   Power(t2,4) + 2*s*(7 + 3*s)*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-501 - 329*t2 + 185*Power(t2,2) - 47*Power(t2,3) - 
                  12*Power(t2,4) + 
                  s*(-12 + t2 + 53*Power(t2,2) + 20*Power(t2,3))))) + 
         s1*(2*Power(t1,10)*(-1 + (-7 + s)*t2) + 
            Power(t1,9)*(2 - (-18 + 49*s + Power(s,2) - 26*s2 + 4*s*s2)*
                t2 + (34 - 4*s + 2*s2)*Power(t2,2)) + 
            Power(t1,8)*(6 + (Power(s,3) - 5*Power(s,2)*(11 + s2) + 
                  3*(4 + s2 - 12*Power(s2,2)) + 
                  s*(45 + 50*s2 + 2*Power(s2,2)))*t2 + 
               (25 + 12*Power(s,2) + s*(106 - 7*s2) - 56*s2 - 
                  4*Power(s2,2))*Power(t2,2) + 
               (-26 + 4*s - 6*s2)*Power(t2,3)) + 
            Power(t1,7)*(-8 + 
               (6 - 17*Power(s,3) - 71*s2 + 39*Power(s2,2) + 
                  14*Power(s2,3) + Power(s,2)*(27 + 44*s2) + 
                  s*(37 - 91*s2 - 13*Power(s2,2)))*t2 + 
               (-35 + Power(s,3) - 187*s2 + 63*Power(s2,2) + 
                  2*Power(s2,3) + 2*Power(s,2)*(67 + 3*s2) + 
                  s*(201 - 97*s2 + Power(s2,2)))*Power(t2,2) - 
               (116 + 22*Power(s,2) + s*(51 - 34*s2) - 36*s2 + 
                  6*Power(s2,2))*Power(t2,3) + 
               (6 - 4*s + 6*s2)*Power(t2,4)) + 
            Power(t1,6)*(-2 + 
               (-66 + Power(s,2)*(31 - 39*s2) - 15*s2 + 
                  59*Power(s2,2) - 38*Power(s2,3) + 
                  s*(-31 + 60*s2 + 20*Power(s2,2)))*t2 + 
               (-91 + 59*Power(s,3) + Power(s,2)*(228 - 86*s2) + 
                  74*s2 + 186*Power(s2,2) - 29*Power(s2,3) + 
                  s*(-316 - 48*s2 + 19*Power(s2,2)))*Power(t2,2) + 
               (205 - 5*Power(s,3) + 3*Power(s,2)*(-45 + s2) + 349*s2 - 
                  7*Power(s2,2) + 8*Power(s2,3) - 
                  2*s*(329 - 53*s2 + 4*Power(s2,2)))*Power(t2,3) + 
               (57 + 12*Power(s,2) + 46*s2 + 17*Power(s2,2) - 
                  s*(38 + 31*s2))*Power(t2,4) + 2*(s - s2)*Power(t2,5)) - 
            Power(t1,5)*(-6 + 
               (-42 + 8*Power(s,2) - 4*Power(s,3) - 119*s2 + 
                  91*Power(s2,2) - 34*Power(s2,3) + 
                  s*(2 + 27*s2 + 9*Power(s2,2)))*t2 + 
               (33 - 68*Power(s,3) - 550*s2 + 302*Power(s2,2) + 
                  31*Power(s2,3) + 8*Power(s,2)*(9 + 20*s2) + 
                  s*(353 - 377*s2 - 3*Power(s2,2)))*Power(t2,2) + 
               (224 + 111*Power(s,3) + Power(s,2)*(604 - 100*s2) - 
                  11*s2 + 271*Power(s2,2) + 31*Power(s2,3) + 
                  s*(-652 - 332*s2 + 23*Power(s2,2)))*Power(t2,3) + 
               (-3*Power(s,3) + Power(s,2)*(-74 + 4*s2) + 
                  s*(-461 + 129*s2 - 5*Power(s2,2)) + 
                  4*(72 + 77*s2 + 2*Power(s2,2) + Power(s2,3)))*
                Power(t2,4) + 
               (-38 + Power(s,2) + 78*s2 + 7*Power(s2,2) - 
                  2*s*(25 + 4*s2))*Power(t2,5)) + 
            Power(t1,4)*(-2 + 
               (20 + 6*Power(s,2) - 80*s2 + 29*Power(s2,2) - 
                  10*Power(s2,3) + 4*s*(1 + 3*s2))*t2 - 
               (-94 + 18*Power(s,3) + Power(s,2)*(250 - 210*s2) + 
                  60*s2 + 408*Power(s2,2) - 201*Power(s2,3) + 
                  s*(-376 + 215*s2 + 59*Power(s2,2)))*Power(t2,2) + 
               (367 - 166*Power(s,3) - 694*s2 + 127*Power(s2,2) + 
                  153*Power(s2,3) + Power(s,2)*(-37 + 463*s2) + 
                  s*(807 - 979*s2 - 54*Power(s2,2)))*Power(t2,3) + 
               (525 + 113*Power(s,3) + Power(s,2)*(326 - 118*s2) + 
                  24*s2 + 163*Power(s2,2) - 22*Power(s2,3) + 
                  s*(-512 - 106*s2 + 41*Power(s2,2)))*Power(t2,4) + 
               (244 - 4*Power(s,2) + 63*s2 - 52*Power(s2,2) + 
                  s*(23 + 78*s2))*Power(t2,5) - 
               2*(10 + 9*s - 13*s2)*Power(t2,6)) + 
            4*s2*Power(t2,4)*(-1 + (2 + s)*t2 - (-3 + s)*Power(t2,2) + 
               Power(s2,2)*(3 + 2*t2) + s2*t2*(2 - s + 2*t2)) + 
            2*t1*Power(t2,3)*(Power(s,3)*(-1 + t2)*t2 - 
               2*Power(s2,3)*(-6 + 3*t2 + 2*Power(t2,2)) - 
               Power(s2,2)*(51 - 9*t2 + Power(t2,2) + 11*Power(t2,3)) + 
               s*(-3 + (13 - 13*s2 + 18*Power(s2,2))*t2 + 
                  (-23 + 10*s2 + 12*Power(s2,2))*Power(t2,2) + 
                  (-31 + 61*s2)*Power(t2,3)) + 
               2*s2*(4 - 44*t2 + 15*Power(t2,2) + 3*Power(t2,3) + 
                  8*Power(t2,4)) - 
               2*(-7 - 13*t2 + 25*Power(t2,2) + 11*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s,2)*t2*(4 - 14*t2 - 8*Power(t2,2) + s2*(3 + 17*t2))) \
- Power(t1,2)*Power(t2,2)*(2*Power(s,3)*t2*(-2 + 7*t2 + 15*Power(t2,2)) + 
               Power(s2,3)*(-60 - 67*t2 + 42*Power(t2,2) + 
                  8*Power(t2,3)) + 
               2*Power(s2,2)*(111 + 125*t2 - 61*Power(t2,2) + 
                  12*Power(t2,3) + 4*Power(t2,4)) + 
               s*(-12 + 4*(2 - 5*s2 + 17*Power(s2,2))*t2 + 
                  (-376 + 225*s2 + 109*Power(s2,2))*Power(t2,2) + 
                  (-188 + 337*s2 - 4*Power(s2,2))*Power(t2,3) + 
                  (16 - 58*s2)*Power(t2,4)) - 
               2*(-28 - 67*t2 + 84*Power(t2,2) + 143*Power(t2,3) + 
                  112*Power(t2,4) + 16*Power(t2,5)) + 
               s2*(-246 - 469*t2 + 154*Power(t2,2) + 71*Power(t2,3) + 
                  162*Power(t2,4) + 32*Power(t2,5)) + 
               2*Power(s,2)*t2*
                (5 + 105*t2 + 28*Power(t2,2) + 45*Power(t2,3) + 
                  s2*(6 - 71*t2 - 13*Power(t2,2)))) + 
            Power(t1,3)*t2*(Power(s2,3)*t2*
                (-203 - 221*t2 + 68*Power(t2,2) + 8*Power(t2,3)) - 
               2*Power(s,3)*t2*
                (1 - 13*t2 - 64*Power(t2,2) + 22*Power(t2,3)) + 
               Power(s2,2)*t2*
                (687 + 509*t2 - 312*Power(t2,2) + 77*Power(t2,3) + 
                  22*Power(t2,4)) - 
               2*(9 - 31*t2 + 50*Power(t2,2) + 260*Power(t2,3) + 
                  234*Power(t2,4) + 80*Power(t2,5)) + 
               s2*(18 - 569*t2 - 181*Power(t2,2) + 566*Power(t2,3) + 
                  20*Power(t2,4) + 112*Power(t2,5)) - 
               s*(6 + (22 + 10*s2 - 36*Power(s2,2))*t2 + 
                  (704 - 451*s2 - 153*Power(s2,2))*Power(t2,2) + 
                  (549 - 757*s2 - 27*Power(s2,2))*Power(t2,3) + 
                  (-155 + 145*s2 + 24*Power(s2,2))*Power(t2,4) + 
                  8*(9 + s2)*Power(t2,5)) + 
               Power(s,2)*t2*(-4 + 496*t2 + 150*Power(t2,2) + 
                  113*Power(t2,3) - 14*Power(t2,4) + 
                  s2*(6 - 386*t2 - 290*Power(t2,2) + 60*Power(t2,3)))))))/
     ((-1 + s2)*(-s + s2 - t1)*Power(-1 + t1,2)*t1*
       (Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*Power(t1 - t2,2)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*(-1 + t2)*t2) + 
    (8*(2 + Power(-1 + s,3)*Power(t1,8) - 
         Power(s1,7)*Power(t1,2)*((-3 + t1)*t1 + s2*(1 + t1)) + 4*t2 + 
         s*t2 + s2*t2 - 6*s*s2*t2 - 12*s*Power(t2,2) - 
         Power(s,2)*Power(t2,2) + 2*s2*Power(t2,2) - 7*s*s2*Power(t2,2) + 
         4*Power(s,2)*s2*Power(t2,2) - 2*Power(s2,2)*Power(t2,2) + 
         4*s*Power(s2,2)*Power(t2,2) + 37*Power(t2,3) - 
         65*s*Power(t2,3) + 17*Power(s,2)*Power(t2,3) - 
         61*s2*Power(t2,3) + 83*s*s2*Power(t2,3) - 
         13*Power(s,2)*s2*Power(t2,3) + 12*Power(s2,2)*Power(t2,3) - 
         10*s*Power(s2,2)*Power(t2,3) - Power(s2,3)*Power(t2,3) - 
         43*Power(t2,4) + 32*s*Power(t2,4) - 16*Power(s,2)*Power(t2,4) + 
         2*Power(s,3)*Power(t2,4) + 32*s2*Power(t2,4) + 
         37*s*s2*Power(t2,4) - 8*Power(s,2)*s2*Power(t2,4) - 
         31*Power(s2,2)*Power(t2,4) + 2*s*Power(s2,2)*Power(t2,4) + 
         4*Power(s2,3)*Power(t2,4) - 9*Power(t2,5) + 12*s*Power(t2,5) - 
         11*Power(s,2)*Power(t2,5) + 8*s2*Power(t2,5) + 
         19*s*s2*Power(t2,5) - Power(s,2)*s2*Power(t2,5) - 
         6*Power(s2,2)*Power(t2,5) + 2*s*Power(s2,2)*Power(t2,5) - 
         Power(s2,3)*Power(t2,5) + 9*Power(t2,6) - 
         Power(s,2)*Power(t2,6) + 2*s2*Power(t2,6) + 2*s*s2*Power(t2,6) - 
         Power(s2,2)*Power(t2,6) + 
         Power(s1,6)*t1*(-5*Power(t1,4) + 2*s2*t2 + 
            Power(t1,3)*(14 + 2*s - s2 + 3*t2) + 
            Power(t1,2)*(12 - 5*s2 + s*(2 + s2 - t2) - 6*t2 + 4*s2*t2) + 
            t1*(1 + s*(s2 - t2) - 9*t2 + 6*s2*t2)) - 
         (-1 + s)*Power(t1,7)*
          (3 + 4*Power(s,2)*t2 + (2 + 3*s2)*t2 - 3*s*(1 + (3 + s2)*t2)) + 
         Power(t1,6)*(1 - (5 + 7*s2)*t2 - 
            (3 + 5*s2 + 3*Power(s2,2))*Power(t2,2) + 
            Power(s,3)*(-4 - 2*t2 + 6*Power(t2,2)) - 
            Power(s,2)*(-12 + (2 + 4*s2)*t2 + (23 + 9*s2)*Power(t2,2)) + 
            s*(-9 + (12 + 11*s2)*t2 + 
               (22 + 17*s2 + 3*Power(s2,2))*Power(t2,2))) + 
         Power(t1,5)*(-9 - (14 + 3*s2)*t2 + 
            (4 + 6*s2 + 5*Power(s2,2))*Power(t2,2) + 
            (5 + 6*s2 + 4*Power(s2,2) + Power(s2,3))*Power(t2,3) + 
            Power(s,3)*(-2 + 16*t2 + 7*Power(t2,2) - 4*Power(t2,3)) + 
            Power(s,2)*(-6 - 2*(33 + 4*s2)*t2 + 
               3*(-6 + s2)*Power(t2,2) + 3*(7 + 3*s2)*Power(t2,3)) - 
            s*(-18 - (65 + 11*s2)*t2 + s2*(14 + 5*s2)*Power(t2,2) + 
               (28 + 20*s2 + 6*Power(s2,2))*Power(t2,3))) + 
         Power(t1,4)*(3 + (32 + 15*s2)*t2 + 
            2*(6 + 22*s2 + Power(s2,2))*Power(t2,2) - 
            (7 + 2*s2 + Power(s2,3))*Power(t2,3) - 
            (7 + 5*s2 + 3*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
            Power(s,3)*(3 + 8*t2 - 22*Power(t2,2) - 9*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s,2)*(-15 + (-5 + 12*s2)*t2 + 
               (125 + 32*s2)*Power(t2,2) + (40 + 9*s2)*Power(t2,3) - 
               (10 + 3*s2)*Power(t2,4)) + 
            s*(9 - 2*(25 + 14*s2)*t2 - 
               (151 + 79*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (-31 - 9*s2 + Power(s2,2))*Power(t2,3) + 
               (20 + 13*s2 + 3*Power(s2,2))*Power(t2,4))) + 
         Power(t1,3)*(9 + (4 - 3*s2)*t2 - 
            4*(1 + 14*s2 + 2*Power(s2,2))*Power(t2,2) - 
            (35 + 28*s2 + 32*Power(s2,2))*Power(t2,3) - 
            (-11 + s2 + 5*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
            (5 + 2*s2 + 2*Power(s2,2))*Power(t2,5) + 
            Power(s,3)*(2 - 11*t2 - 11*Power(t2,2) + 9*Power(t2,3) + 
               5*Power(t2,4)) + 
            Power(s,2)*(3 + (57 + 11*s2)*t2 + 
               (66 - 34*s2)*Power(t2,2) - 3*(33 + 10*s2)*Power(t2,3) - 
               (34 + 11*s2)*Power(t2,4) + 2*Power(t2,5)) + 
            s*(-18 - 2*(35 + 2*s2)*t2 + 
               (-36 + 103*s2 + 8*Power(s2,2))*Power(t2,2) + 
               (162 + 123*s2 + 16*Power(s2,2))*Power(t2,3) + 
               (42 + 27*s2 + 7*Power(s2,2))*Power(t2,4) - 
               (7 + 4*s2)*Power(t2,5))) - 
         Power(t1,2)*(5 + (31 + 9*s2)*t2 + 
            (29 + 17*s2 - 3*Power(s2,2))*Power(t2,2) - 
            (-6 + 7*s2 + 20*Power(s2,2) + 2*Power(s2,3))*Power(t2,3) - 
            2*(31 + 4*s2 + 12*Power(s2,2) + Power(s2,3))*Power(t2,4) - 
            (-6 + 5*Power(s2,2) + Power(s2,3))*Power(t2,5) + 
            (1 + s2)*Power(t2,6) + 
            Power(s,3)*t2*(5 - 15*t2 - 7*Power(t2,2) - 4*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s,2)*(-6 + (-7 + 8*s2)*t2 + 
               (94 + 22*s2)*Power(t2,2) + (100 - 22*s2)*Power(t2,3) + 
               2*(-12 + s2)*Power(t2,4) - 3*(4 + s2)*Power(t2,5)) + 
            s*(3 - (37 + 23*s2)*t2 + 
               (-161 - 45*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (-124 + 33*s2 + 30*Power(s2,2))*Power(t2,3) + 
               (74 + 67*s2 + 4*Power(s2,2))*Power(t2,4) + 
               (20 + 17*s2 + 3*Power(s2,2))*Power(t2,5) - Power(t2,6))) - 
         t1*(3 - (8 + 3*s2)*t2 - 
            (20 + 26*s2 + 3*Power(s2,2))*Power(t2,2) + 
            (-6 - 78*s2 + 4*Power(s2,2) + Power(s2,3))*Power(t2,3) + 
            (-23 + 40*s2 - 33*Power(s2,2) + 6*Power(s2,3))*Power(t2,4) + 
            (40 + 2*s2 + 9*Power(s2,2) - 2*Power(s2,3))*Power(t2,5) + 
            (-2 + s2 + Power(s2,2))*Power(t2,6) + 
            Power(s,3)*Power(t2,2)*
             (-3 + 9*t2 + 2*Power(t2,2) + 3*Power(t2,3)) + 
            s*(-6 + (-16 + s2)*t2 + 
               (-16 + 65*s2 + 3*Power(s2,2))*Power(t2,2) + 
               (118 + 118*s2 - 23*Power(s2,2))*Power(t2,3) + 
               (68 + 50*s2 - 6*Power(s2,2))*Power(t2,4) + 
               (-9 - 6*s2 + 7*Power(s2,2))*Power(t2,5) - 
               (3 + 2*s2)*Power(t2,6)) + 
            Power(s,2)*t2*(4 + 31*t2 - 71*Power(t2,2) - 56*Power(t2,3) - 
               5*Power(t2,4) + Power(t2,5) - 
               s2*(-6 + 24*t2 + 17*Power(t2,2) + 2*Power(t2,3) + 
                  8*Power(t2,4)))) + 
         Power(s1,5)*(Power(s2,3)*(-1 + t1) + 
            Power(s2,2)*t1*(-1 + s - s*t1 + t2 + t1*(1 + t2) + 
               2*Power(t1,2)*(2 + t2)) + 
            s2*(3*Power(t1,5) - Power(t2,2) + 
               Power(t1,4)*(-13 + s + 6*t2) - t1*t2*(2*s + 9*t2) + 
               Power(t1,3)*(s*(4 - 5*t2) - 6*(-2 + t2)*t2) + 
               Power(t1,2)*(8 + 12*t2 - 14*Power(t2,2) - s*(3 + 7*t2))) \
- t1*(9*Power(t1,5) + Power(s,2)*t1*(1 + t1)*(t1 - t2) + (2 - 9*t2)*t2 + 
               3*t1*(13 - 8*t2)*t2 - 11*Power(t1,4)*(2 + t2) + 
               Power(t1,2)*(1 + 72*t2) + 
               Power(t1,3)*(-34 + 12*t2 + 3*Power(t2,2)) + 
               s*(-12*Power(t1,4) - 2*Power(t2,2) + 
                  Power(t1,3)*(-1 + 12*t2) + 
                  t1*(1 + 5*t2 - 5*Power(t2,2)) + 
                  Power(t1,2)*(4 + 13*t2 - 3*Power(t2,2))))) + 
         Power(s1,4)*(-7*Power(t1,7) + 
            Power(s2,3)*(3 - 5*t1 + 2*Power(t1,2) + 
               2*Power(t1,3)*(-1 + t2)) + 
            Power(t1,6)*(15 + 23*s + 13*t2) - 
            Power(t2,2)*(-1 + (3 + s)*t2) + 
            Power(t1,5)*(40 - 8*Power(s,2) + s*(6 - 38*t2) + 8*t2 - 
               7*Power(t2,2)) + 
            t1*t2*(-2*Power(s,2)*t2 + 2*(21 - 13*t2)*t2 + 
               s*(2 + 4*t2 - 7*Power(t2,2))) + 
            Power(t1,4)*(9 - 134*t2 + 13*Power(s,2)*t2 - 
               21*Power(t2,2) + Power(t2,3) + 
               s*(-21 - 47*t2 + 19*Power(t2,2))) + 
            Power(t1,2)*(-8 + 6*t2 + 134*Power(t2,2) - 18*Power(t2,3) + 
               Power(s,2)*(-2 + t2 - 6*Power(t2,2)) + 
               s*(8 + 17*t2 + 24*Power(t2,2) - 9*Power(t2,3))) + 
            Power(t1,3)*(-3 - 127*t2 + 97*Power(t2,2) + 6*Power(t2,3) + 
               Power(s,2)*(4 + 4*t2 - 4*Power(t2,2)) + 
               s*(-16 + 20*t2 + 41*Power(t2,2) - 3*Power(t2,3))) + 
            Power(s2,2)*(3 + t2 - 4*s*t2 - Power(t2,2) + 
               Power(t1,4)*(12 + 5*t2) + 
               Power(t1,2)*(-1 + 5*s - 19*t2 - 8*Power(t2,2)) + 
               t1*(4 + (-3 + 2*s)*t2 - 3*Power(t2,2)) + 
               Power(t1,3)*(s - 4*s*t2 - 2*t2*(4 + 3*t2))) + 
            s2*(5*Power(t1,6) + Power(t1,5)*(-22 - 2*s + t2) + 
               Power(t2,2)*(s + 4*t2) + 
               Power(t1,4)*(-2 + s + Power(s,2) + 3*t2 - 15*s*t2 - 
                  10*Power(t2,2)) - 
               Power(t1,2)*(4 + Power(s,2) + 11*t2 + 23*Power(t2,2) - 
                  16*Power(t2,3) + s*(4 - 4*t2 - 19*Power(t2,2))) + 
               Power(t1,3)*(15 + 2*Power(s,2)*(-2 + t2) + 41*t2 - 
                  18*Power(t2,2) + 4*Power(t2,3) + 
                  s*(-4 - 4*t2 + 11*Power(t2,2))) + 
               t1*(2 - 18*t2 + 2*Power(s,2)*t2 - 9*Power(t2,2) + 
                  16*Power(t2,3) + s*(1 + t2 + 13*Power(t2,2))))) + 
         Power(s1,3)*(-2*Power(t1,8) + Power(t1,7)*(4 + 18*s + 5*t2) + 
            Power(t1,6)*(23 - 17*Power(s,2) + s*(19 - 42*t2) + 17*t2 - 
               4*Power(t2,2)) + 
            Power(s2,3)*(-2 + t1*(2 - 3*t2) + t2 + 3*Power(t2,2) + 
               Power(t1,4)*(-2 + 3*t2) + 
               Power(t1,3)*(-2 + 7*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*(4 - 5*Power(t2,2))) + 
            Power(t2,2)*(Power(s,2)*t2 + 3*t2*(-5 + 3*t2) + 
               s*(-1 - t2 + 3*Power(t2,2))) + 
            Power(t1,5)*(24 + Power(s,3) - 105*t2 - 38*Power(t2,2) + 
               Power(t2,3) + Power(s,2)*(-4 + 39*t2) + 
               3*s*(-14 - 36*t2 + 11*Power(t2,2))) + 
            Power(t1,4)*(Power(s,3)*(1 - 3*t2) + 
               Power(s,2)*(13 + 24*t2 - 27*Power(t2,2)) + 
               s*(-29 + 43*t2 + 135*Power(t2,2) - 10*Power(t2,3)) + 
               4*(-2 - 41*t2 + 19*Power(t2,2) + 5*Power(t2,3))) + 
            Power(t1,3)*(-43 + 27*t2 + 281*Power(t2,2) - 
               15*Power(t2,3) - 3*Power(t2,4) + 
               2*Power(s,3)*(1 - t2 + Power(t2,2)) + 
               Power(s,2)*(4 - 25*t2 - 35*Power(t2,2) + 
                  5*Power(t2,3)) + 
               s*(18 + 99*t2 + 37*Power(t2,2) - 55*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(t1,2)*(3 + 19*t2 + Power(s,3)*(-3 + t2)*t2 + 
               170*Power(t2,2) - 167*Power(t2,3) + 
               Power(s,2)*t2*(-7 + 5*t2 + 12*Power(t2,2)) + 
               s*(-26 + 67*t2 - 84*Power(t2,2) - 45*Power(t2,3) + 
                  7*Power(t2,4))) + 
            t1*(-1 + 17*t2 - 9*Power(t2,2) + Power(s,3)*Power(t2,2) - 
               108*Power(t2,3) + 24*Power(t2,4) + 
               Power(s,2)*t2*(5 - 3*t2 + 10*Power(t2,2)) + 
               s*(-2 - 11*t2 - 24*Power(t2,2) - 17*Power(t2,3) + 
                  9*Power(t2,4))) + 
            Power(s2,2)*(-9 + (-7 + 12*s)*t2 - (-2 + s)*Power(t2,2) + 
               2*Power(t2,3) + Power(t1,5)*(7 + 6*t2) - 
               Power(t1,4)*(-6 + 7*(3 + s)*t2 + 12*Power(t2,2)) + 
               t1*(10 - 4*t2 + 32*Power(t2,2) + 8*Power(t2,3) + 
                  s*(-1 - 22*t2 + Power(t2,2))) + 
               Power(t1,2)*(-13 + 8*t2 + 36*Power(t2,2) + 
                  16*Power(t2,3) + 3*s*(-3 + t2 + 4*Power(t2,2))) + 
               Power(t1,3)*(-1 - 54*t2 - 2*Power(t2,2) + 
                  6*Power(t2,3) + 2*s*(8 - 9*t2 + 7*Power(t2,2)))) + 
            s2*(-3 + 2*Power(t1,7) + (-2 + 5*s)*t2 + 
               (10 + 2*s - 5*Power(s,2))*Power(t2,2) + 
               (2 - 7*s)*Power(t2,3) - 6*Power(t2,4) - 
               Power(t1,6)*(16 + 3*s + t2) + 
               Power(t1,5)*(-8 + 3*Power(s,2) + s*(3 - 24*t2) + 10*t2 - 
                  6*Power(t2,2)) + 
               Power(t1,4)*(-1 + 81*t2 - 8*Power(t2,2) + 
                  6*Power(t2,3) + Power(s,2)*(-8 + 6*t2) + 
                  s*(3 + 25*t2 + 36*Power(t2,2))) - 
               Power(t1,3)*(-33 + 36*t2 + 12*Power(t2,2) - 
                  22*Power(t2,3) + Power(t2,4) + 
                  Power(s,2)*(8 - 9*t2 + 10*Power(t2,2)) + 
                  s*(36 - 39*t2 - 24*Power(t2,2) + 11*Power(t2,3))) - 
               t1*(11 - 10*t2 + Power(s,2)*(-3 + t2)*t2 - 
                  24*Power(t2,2) - 14*Power(t2,3) + 14*Power(t2,4) + 
                  s*(12 - 39*t2 + 29*Power(t2,2) + 23*Power(t2,3))) + 
               Power(t1,2)*(4 - 38*t2 - 52*Power(t2,2) + 
                  16*Power(t2,3) - 9*Power(t2,4) + 
                  Power(s,2)*(1 + 18*t2 - 8*Power(t2,2)) - 
                  s*(-19 + 20*t2 + 7*Power(t2,2) + 31*Power(t2,3))))) + 
         Power(s1,2)*(1 + 5*s*Power(t1,8) + t2 - s*t2 - 9*Power(t2,2) + 
            3*s*Power(t2,2) + 4*Power(t2,3) + 11*s*Power(t2,3) + 
            3*Power(s,2)*Power(t2,3) - 2*Power(s,3)*Power(t2,3) + 
            32*Power(t2,4) + 4*s*Power(t2,4) - 
            5*Power(s,2)*Power(t2,4) - 9*Power(t2,5) - 3*s*Power(t2,5) + 
            Power(t1,7)*(-2 - 14*Power(s,2) + s*(17 - 15*t2) + 7*t2) + 
            Power(s2,3)*t2*(1 + Power(t1,5) + Power(t1,4)*(5 - 7*t2) - 
               7*t2 - 3*Power(t2,2) + 3*Power(t1,3)*t2*(-3 + 2*t2) + 
               t1*(-1 + 15*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(-6 - 4*t2 + 11*Power(t2,2))) + 
            Power(t1,6)*(-1 + 3*Power(s,3) - 11*t2 - 17*Power(t2,2) + 
               Power(s,2)*(-12 + 41*t2) + 
               s*(-42 - 85*t2 + 16*Power(t2,2))) + 
            Power(t1,5)*(20 + Power(s,3)*(3 - 10*t2) - 78*t2 - 
               3*Power(t2,2) + 13*Power(t2,3) + 
               Power(s,2)*(16 + 70*t2 - 42*Power(t2,2)) + 
               s*(-94 + 58*t2 + 135*Power(t2,2) - 7*Power(t2,3))) + 
            Power(t1,4)*(-17 - 63*t2 + 228*Power(t2,2) + 
               31*Power(t2,3) - 3*Power(t2,4) + 
               Power(s,3)*t2*(-9 + 11*t2) + 
               Power(s,2)*(6 - 30*t2 - 118*Power(t2,2) + 
                  17*Power(t2,3)) + 
               s*(30 + 248*t2 + 86*Power(t2,2) - 88*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(t1,3)*(-4 + 22*t2 + 233*Power(t2,2) - 
               180*Power(t2,3) - 17*Power(t2,4) - 
               2*Power(s,3)*t2*(1 - 6*t2 + 2*Power(t2,2)) + 
               Power(s,2)*(15 - 53*t2 + 9*Power(t2,2) + 
                  72*Power(t2,3) - 2*Power(t2,4)) + 
               s*(45 + 24*t2 - 190*Power(t2,2) - 147*Power(t2,3) + 
                  23*Power(t2,4))) + 
            Power(t1,2)*(-3 + 129*t2 - 126*Power(t2,2) - 
               257*Power(t2,3) + 58*Power(t2,4) + 3*Power(t2,5) + 
               Power(s,3)*t2*(-5 + 4*t2 - 6*Power(t2,2)) + 
               Power(s,2)*(13 - 54*t2 + 84*Power(t2,2) + 
                  19*Power(t2,3) - 12*Power(t2,4)) + 
               s*(27 - 118*t2 - 117*Power(t2,2) + 35*Power(t2,3) + 
                  53*Power(t2,4) - 2*Power(t2,5))) - 
            t1*(-6 + 7*t2 + 30*Power(t2,2) + 95*Power(t2,3) - 
               116*Power(t2,4) + 6*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(-7 + 2*t2) + 
               Power(s,2)*t2*
                (16 - 33*t2 + 29*Power(t2,2) + 11*Power(t2,3)) + 
               s*(-12 - 21*t2 + 77*Power(t2,2) - 104*Power(t2,3) - 
                  15*Power(t2,4) + 5*Power(t2,5))) + 
            Power(s2,2)*(6 + (2 - 14*s)*t2 + 2*(-2 + 7*s)*Power(t2,2) + 
               (-17 + 8*s)*Power(t2,3) - 2*Power(t2,4) + 
               Power(t1,6)*(-1 + 3*t2) + 
               Power(t1,4)*(6 - (32 + 17*s)*t2 + 
                  (-9 + 23*s)*Power(t2,2) + 9*Power(t2,3)) - 
               t1*(7 - (32 + 37*s)*t2 + (34 + 28*s)*Power(t2,2) + 
                  3*(16 + s)*Power(t2,3) + 12*Power(t2,4)) - 
               Power(t1,2)*(11 + (-27 + 38*s)*t2 - 
                  (65 + 29*s)*Power(t2,2) + (26 + 31*s)*Power(t2,3) + 
                  12*Power(t2,4)) - 
               Power(t1,5)*(5 + 9*Power(t2,2) + s*(2 + 4*t2)) + 
               Power(t1,3)*(12 - 32*t2 + 99*Power(t2,2) + 
                  18*Power(t2,3) - 2*Power(t2,4) + 
                  s*(2 + 18*t2 + 22*Power(t2,2) - 16*Power(t2,3)))) + 
            s2*(9 - (3 + s)*Power(t1,7) - 2*(-7 + 6*s)*t2 + 
               (-6 - 35*s + 13*Power(s,2))*Power(t2,2) + 
               (-13 + 21*s - 2*Power(s,2))*Power(t2,3) + 
               (-2 + 9*s)*Power(t2,4) + 4*Power(t2,5) + 
               Power(t1,6)*(7 + 3*Power(s,2) + s*(6 - 19*t2) + 5*t2 - 
                  Power(t2,2)) + 
               Power(t1,5)*(11 + 34*t2 - 7*Power(t2,2) + 
                  2*Power(t2,3) + 2*Power(s,2)*(-4 + 5*t2) + 
                  s*(40 - 7*t2 + 42*Power(t2,2))) - 
               Power(t1,4)*(3 + 25*t2 + 33*Power(t2,2) - 
                  14*Power(t2,3) + Power(t2,4) + 
                  Power(s,2)*(-2 - 7*t2 + 27*Power(t2,2)) + 
                  s*(20 - 17*t2 - 56*Power(t2,2) + 26*Power(t2,3))) + 
               Power(t1,3)*(-27 + 50*t2 - 118*Power(t2,2) + 
                  9*Power(t2,3) - 13*Power(t2,4) + 
                  Power(s,2)*
                   (1 + 16*t2 - 22*Power(t2,2) + 14*Power(t2,3)) + 
                  s*(-32 + 16*t2 - 129*Power(t2,2) - 79*Power(t2,3) + 
                     4*Power(t2,4))) + 
               Power(t1,2)*(11 - 82*t2 + 110*Power(t2,2) + 
                  7*Power(t2,3) - 12*Power(t2,4) + 2*Power(t2,5) + 
                  Power(s,2)*t2*(21 - 23*t2 + 26*Power(t2,2)) + 
                  s*(-10 + 145*t2 - 137*Power(t2,2) + 34*Power(t2,3) + 
                     24*Power(t2,4))) + 
               t1*(-5 + 4*t2 + 19*Power(t2,2) + 37*Power(t2,3) - 
                  2*Power(t2,4) + 6*Power(t2,5) + 
                  Power(s,2)*t2*(-16 - 23*t2 + 8*Power(t2,2)) + 
                  s*(17 - 62*t2 + 59*Power(t2,2) + 36*Power(t2,3) + 
                     25*Power(t2,4))))) + 
         s1*(-3 + 19*Power(t1,3) - 10*Power(t1,4) - 20*Power(t1,5) + 
            14*Power(t1,6) + Power(t1,7) - Power(t1,8) - 7*t2 - 
            13*Power(t1,2)*t2 + 49*Power(t1,3)*t2 - 19*Power(t1,4)*t2 - 
            12*Power(t1,5)*t2 - Power(t1,6)*t2 + 3*Power(t1,7)*t2 + 
            4*Power(t2,2) - 123*t1*Power(t2,2) - 
            20*Power(t1,2)*Power(t2,2) + 45*Power(t1,3)*Power(t2,2) + 
            90*Power(t1,4)*Power(t2,2) + 14*Power(t1,5)*Power(t2,2) - 
            10*Power(t1,6)*Power(t2,2) + 14*Power(t2,3) + 
            133*t1*Power(t2,3) - 132*Power(t1,2)*Power(t2,3) - 
            200*Power(t1,3)*Power(t2,3) - 18*Power(t1,4)*Power(t2,3) + 
            19*Power(t1,5)*Power(t2,3) + 18*Power(t2,4) + 
            97*t1*Power(t2,4) + 145*Power(t1,2)*Power(t2,4) - 
            14*Power(t1,4)*Power(t2,4) - 29*Power(t2,5) - 
            42*t1*Power(t2,5) + 2*Power(t1,2)*Power(t2,5) + 
            3*Power(t1,3)*Power(t2,5) + 3*Power(t2,6) - t1*Power(t2,6) - 
            Power(s2,3)*Power(t2,2)*
             (-2 + 2*Power(t1,5) + Power(t1,4)*(2 - 5*t2) + t2 - 
               2*Power(t2,2) + 7*Power(t1,2)*Power(t2,2) + 
               t1*t2*(1 + t2) + Power(t1,3)*(-2 - 5*t2 + 2*Power(t2,2))) \
+ Power(s,3)*(3*Power(t1,7) + Power(t1,6)*(2 - 11*t2) - 
               (-2 + t2)*Power(t2,3) + Power(t1,5)*t2*(-13 + 15*t2) + 
               t1*Power(t2,2)*(-11 + 7*t2 + 4*Power(t2,2)) - 
               Power(t1,4)*(3 + 4*t2 - 29*Power(t2,2) + 9*Power(t2,3)) + 
               Power(t1,2)*t2*
                (13 - 14*t2 - 9*Power(t2,2) + 9*Power(t2,3)) + 
               Power(t1,3)*(-4 + 11*t2 + 9*Power(t2,2) - 
                  27*Power(t2,3) + 2*Power(t2,4))) + 
            Power(s,2)*(-4*Power(t1,8) + 2*Power(t1,7)*(-5 + 7*t2) + 
               Power(t1,6)*(27 + 50*t2 - 18*Power(t2,2)) + 
               Power(t2,2)*(1 - 27*t2 + 21*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,5)*(29 - 68*t2 - 93*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,3)*(-19 + 25*t2 + 158*Power(t2,2) + 
                  58*Power(t2,3) - 29*Power(t2,4)) + 
               Power(t1,4)*(-6 - 118*t2 + 25*Power(t2,2) + 
                  79*Power(t2,3) - 2*Power(t2,4)) + 
               t1*t2*(15 + 21*t2 - 49*Power(t2,2) + 14*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(t1,2)*(-17 + 34*t2 + 2*Power(t2,2) - 
                  87*Power(t2,3) - 50*Power(t2,4) + 3*Power(t2,5))) + 
            s*(5*Power(t1,8) + Power(t1,7)*(3 - 24*t2) + 
               Power(t1,6)*(-47 - 29*t2 + 43*Power(t2,2)) + 
               Power(t1,5)*(5 + 184*t2 + 101*Power(t2,2) - 
                  36*Power(t2,3)) + 
               Power(t2,2)*(5 + 26*t2 - 41*Power(t2,2) + Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,4)*(52 + 166*t2 - 239*Power(t2,2) - 
                  135*Power(t2,3) + 14*Power(t2,4)) + 
               Power(t1,2)*(-10 - 97*t2 + 123*Power(t2,2) + 
                  209*Power(t2,3) + 41*Power(t2,4) - 16*Power(t2,5)) + 
               Power(t1,3)*(8 - 182*t2 - 339*Power(t2,2) + 
                  73*Power(t2,3) + 75*Power(t2,4) - 2*Power(t2,5)) + 
               t1*(-16 - 18*t2 + 174*Power(t2,2) + 7*Power(t2,3) - 
                  49*Power(t2,4) - 17*Power(t2,5) + Power(t2,6))) + 
            Power(s2,2)*t2*(-2 - Power(t1,6)*(-4 + t2) + 5*t2 + 
               35*Power(t2,2) + 20*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,3)*t2*(41 - 24*t2 - 14*Power(t2,2)) + 
               Power(t1,5)*(2 - 15*t2 + 3*Power(t2,2)) + 
               Power(t1,4)*(-14 + 32*t2 + 17*Power(t2,2) - 
                  2*Power(t2,3)) + 
               3*Power(t1,2)*
                (4 + 4*t2 - 40*Power(t2,2) + Power(t2,3) + Power(t2,4)) \
+ t1*(-2 - 74*t2 + 17*Power(t2,2) + 29*Power(t2,3) + 7*Power(t2,4)) - 
               s*(-6 + Power(t1,6) + Power(t1,5)*(7 - 14*t2) + 22*t2 + 
                  12*Power(t2,2) + 5*Power(t2,3) + 
                  Power(t1,4)*(-12 - 6*t2 + 19*Power(t2,2)) + 
                  Power(t1,2)*
                   (17 + 59*t2 - 15*Power(t2,2) - 23*Power(t2,3)) + 
                  t1*(7 - 77*t2 + 4*Power(t2,2) - 6*Power(t2,3)) - 
                  2*Power(t1,3)*(7 + t2 - 14*Power(t2,2) + 3*Power(t2,3))\
)) + s2*(-6 + (-7 + 13*s)*t2 + (1 + 49*s - 12*Power(s,2))*Power(t2,2) + 
               (4 - 53*s + 21*Power(s,2))*Power(t2,3) + 
               (-13 - 26*s + 4*Power(s,2))*Power(t2,4) - 
               (2 + 5*s)*Power(t2,5) - Power(t2,6) + 
               Power(t1,7)*(-2 + s + Power(s,2) + 2*t2 - 5*s*t2) + 
               Power(t1,6)*(3*(3 - 2*t2)*t2 + Power(s,2)*(-4 + 9*t2) + 
                  s*(7 - 15*t2 + 14*Power(t2,2))) + 
               Power(t1,5)*(12 - 51*t2 - 8*Power(t2,2) + 6*Power(t2,3) + 
                  Power(s,2)*(-6 + 4*t2 - 27*Power(t2,2)) + 
                  s*(-8 + 37*t2 + 52*Power(t2,2) - 13*Power(t2,3))) + 
               Power(t1,4)*(-6 + 9*t2 + 13*Power(t2,2) + 9*Power(t2,3) - 
                  4*Power(t2,4) + 
                  Power(s,2)*
                   (4 + 18*t2 - 25*Power(t2,2) + 23*Power(t2,3)) + 
                  s*(-14 - 53*t2 - 95*Power(t2,2) - 75*Power(t2,3) + 
                     4*Power(t2,4))) + 
               Power(t1,3)*(-18 + 38*t2 + 35*Power(t2,2) + 
                  10*Power(t2,3) - 8*Power(t2,4) + 2*Power(t2,5) + 
                  Power(s,2)*
                   (5 - 31*t2 + 4*Power(t2,2) + 50*Power(t2,3) - 
                     6*Power(t2,4)) + 
                  s*(13 + 11*t2 - 59*Power(t2,2) + 35*Power(t2,3) + 
                     43*Power(t2,4))) + 
               Power(t1,2)*(12 + 37*t2 - 136*Power(t2,2) + 
                  99*Power(t2,3) + 8*Power(t2,5) - 
                  Power(s,2)*t2*
                   (13 - 38*t2 + 10*Power(t2,2) + 25*Power(t2,3)) + 
                  s*(7 + 7*t2 + 81*Power(t2,2) + 174*Power(t2,3) + 
                     37*Power(t2,4) - 6*Power(t2,5))) - 
               t1*(-8 + 37*t2 - 101*Power(t2,2) + 104*Power(t2,3) + 
                  9*Power(t2,4) + 2*Power(t2,5) + Power(t2,6) + 
                  Power(s,2)*t2*
                   (-17 + 18*t2 + 12*Power(t2,2) + 9*Power(t2,3)) + 
                  s*(6 - 5*t2 + 120*Power(t2,2) - 60*Power(t2,3) + 
                     62*Power(t2,4) + 15*Power(t2,5))))))*
       B1(1 - s1 - t1 + t2,t1,t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,3)*
       (-1 + t2)) - (8*((-2 + 2*Power(s,3) - 2*s1 - Power(s1,3) - 
            2*Power(s,2)*(3 + 4*s1) + s*(6 + 10*s1 + 7*Power(s1,2)) + 
            Power(s1,2)*(17 + 2*s2))*Power(t1,11) + 
         s2*(-1 + s1 - t2)*Power(t2,4)*
          (1 + Power(s2,2)*(-2 + s1 - t2) + 2*s1*t2 + s*(-1 + t2)*t2 - 
            Power(t2,2) + s2*(1 + s1 + s*t2 + s1*t2 - Power(t2,2))) + 
         Power(t1,10)*(-5*Power(s1,4) + s1*(11 - 4*s2 - 26*t2) + 
            Power(s,3)*(-6 + 3*s1 - 10*t2) + 
            Power(s1,3)*(9 + 10*s2 + 5*t2) + 
            Power(s1,2)*(6 - 59*s2 + 2*Power(s2,2) - 56*t2 - 7*s2*t2) + 
            6*(2 + t2 + s2*t2) + 
            Power(s,2)*(24 - 20*Power(s1,2) + 32*t2 + 6*s2*t2 + 
               s1*(13 + 2*s2 + 36*t2)) + 
            s*(22*Power(s1,3) + Power(s1,2)*(43 - 3*s2 - 32*t2) - 
               2*(15 + 14*t2 + 6*s2*t2) - 
               s1*(33 - 2*s2 + 52*t2 + 10*s2*t2))) + 
         t1*Power(t2,3)*(Power(s2,3)*
             (-8 - 24*t2 - 14*Power(t2,2) + Power(t2,3)) + 
            (1 + t2)*(-1 + t2 - s*t2 + (-5 + 7*s)*Power(t2,2) + 
               (5 - 6*s + 5*Power(s,2))*Power(t2,3)) + 
            Power(s2,2)*(10 - (-25 + s)*t2 + (18 - 5*s)*Power(t2,2) - 
               5*(-1 + s)*Power(t2,3) + 2*Power(t2,4)) + 
            s2*(2 + (16 + s)*t2 + 
               (-5 - 18*s + 4*Power(s,2))*Power(t2,2) + 
               (-26 - 24*s + 4*Power(s,2))*Power(t2,3) - 
               7*(1 + s)*Power(t2,4)) - 
            Power(s1,3)*s2*(3*Power(s2,2) - 2*(-1 + t2)*t2 + 
               2*s2*(1 + t2)) + 
            Power(s1,2)*(Power(s2,3)*(5 + 3*t2) - 
               Power(s2,2)*(1 + t2 + 7*s*t2 + 2*Power(t2,2)) + 
               2*t2*(1 - (-3 + s)*t2 + (1 + s)*Power(t2,2)) - 
               s2*(-1 + (7 + 2*s)*t2 + (16 + 7*s)*Power(t2,2) + 
                  8*Power(t2,3))) + 
            s1*(1 + (-3 + s)*t2 - (3 + 5*s)*Power(t2,2) + 
               (-13 + 6*s - 5*Power(s,2))*Power(t2,3) - 
               2*(1 + s)*Power(t2,4) + 
               Power(s2,3)*(6 + 13*t2 - Power(t2,2)) + 
               Power(s2,2)*(-7 + (-5 + 8*s)*t2 + 
                  6*(1 + 2*s)*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-3 + (-5 + s)*t2 + 
                  (45 + 27*s - 4*Power(s,2))*Power(t2,2) + 
                  (29 + 14*s)*Power(t2,3) + 6*Power(t2,4)))) + 
         Power(t1,2)*Power(t2,2)*
          (7 - 9*t2 + 2*s*t2 - 25*Power(t2,2) + 35*s*Power(t2,2) - 
            2*Power(s,2)*Power(t2,2) + 63*Power(t2,3) - 
            42*s*Power(t2,3) - 8*Power(s,2)*Power(t2,3) + 
            6*Power(s,3)*Power(t2,3) - 22*Power(t2,4) + 
            25*s*Power(t2,4) - 33*Power(s,2)*Power(t2,4) + 
            8*Power(s,3)*Power(t2,4) - 18*Power(t2,5) + 
            14*s*Power(t2,5) + 2*Power(s,2)*Power(t2,5) + 
            Power(s2,3)*(-6 + 27*t2 + 70*Power(t2,2) + 17*Power(t2,3) - 
               7*Power(t2,4)) + 
            Power(s2,2)*(33 + (2 + 9*s)*t2 + 
               (-123 + 28*s)*Power(t2,2) + (-71 + 24*s)*Power(t2,3) + 
               (-26 + 15*s)*Power(t2,4) - 5*Power(t2,5)) + 
            s2*(-43 + 7*(-12 + s)*t2 + 
               (11 + 46*s - 24*Power(s,2))*Power(t2,2) + 
               (46 + 207*s - 54*Power(s,2))*Power(t2,3) + 
               (75 + 95*s - 16*Power(s,2))*Power(t2,4) + 
               3*(5 + s)*Power(t2,5)) + 
            Power(s1,4)*s2*(s2*(1 + t2) - 2*t2*(1 + 2*t2)) + 
            Power(s1,3)*(Power(s2,3)*(6 + 7*t2) + 
               Power(s2,2)*(11 + 10*t2 + 8*Power(t2,2)) - 
               2*t2*(6 + (5 - 2*s)*t2 + (5 + 2*s)*Power(t2,2)) + 
               s2*(-5 + (24 + 7*s)*t2 + (29 + 11*s)*Power(t2,2) + 
                  20*Power(t2,3))) + 
            Power(s1,2)*(-14 + (23 + 6*s)*t2 + 
               2*(1 + 6*s)*Power(t2,2) + 
               (7 - 13*s + 10*Power(s,2))*Power(t2,3) + 
               14*Power(t2,4) - 
               Power(s2,3)*(18 + 39*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(-7 + (-13 + 19*s)*t2 + 
                  (1 + 3*s)*Power(t2,2) + 8*Power(t2,3)) + 
               s2*(15 + (2 + 11*s)*t2 - 
                  (45 + 17*s + 5*Power(s,2))*Power(t2,2) + 
                  (11 - 43*s)*Power(t2,3) - 12*Power(t2,4))) + 
            s1*(10 - (-25 + s)*t2 + 
               (-15 - 38*s + 7*Power(s,2))*Power(t2,2) + 
               (29 + 7*s + 9*Power(s,2) - 5*Power(s,3))*Power(t2,3) + 
               (31 + s - 10*Power(s,2))*Power(t2,4) + 
               4*(-1 + s)*Power(t2,5) + 
               Power(s2,3)*(15 + 10*t2 - 4*Power(t2,2) + 
                  5*Power(t2,3)) - 
               Power(s2,2)*(29 + 3*(-3 + 7*s)*t2 + 
                  (-16 + 41*s)*Power(t2,2) + 2*(13 + 9*s)*Power(t2,3) + 
                  12*Power(t2,4)) - 
               s2*(-24 + t2 + 39*s*t2 + 
                  (36 + 83*s - 24*Power(s,2))*Power(t2,2) + 
                  (111 + 73*s - 25*Power(s,2))*Power(t2,3) + 
                  (73 - 29*s)*Power(t2,4) + 4*Power(t2,5)))) - 
         Power(t1,3)*t2*(-10 + 
            (-26 - 167*s2 + 128*Power(s2,2) - 24*Power(s2,3) + 
               s*(17 + 21*s2 + 7*Power(s2,2)))*t2 + 
            (-109 - 145*s2 + 18*Power(s2,2) + 33*Power(s2,3) - 
               6*Power(s,2)*(3 + 8*s2) + 
               s*(141 + 72*s2 + 49*Power(s2,2)))*Power(t2,2) + 
            (77 + 26*Power(s,3) + 82*s2 - 239*Power(s2,2) + 
               83*Power(s2,3) - Power(s,2)*(35 + 158*s2) + 
               s*(20 + 451*s2 + 36*Power(s2,2)))*Power(t2,3) + 
            (112 + 45*Power(s,3) + 89*s2 - 102*Power(s2,2) + 
               5*Power(s2,3) - Power(s,2)*(151 + 81*s2) + 
               s*(19 + 432*s2 + 22*Power(s2,2)))*Power(t2,4) - 
            (75 + 2*Power(s,3) - 87*s2 + 23*Power(s2,2) + 
               7*Power(s2,3) + Power(s,2)*(33 + 6*s2) - 
               s*(58 + 54*s2 + 15*Power(s2,2)))*Power(t2,5) - 
            (15 + 5*Power(s,2) + s*(4 - 7*s2) - 9*s2 + 2*Power(s2,2))*
             Power(t2,6) - 2*Power(s1,5)*s2*t2*(1 + t2) + 
            Power(s1,4)*(Power(s2,2)*(4 + 7*t2 + 4*Power(t2,2)) - 
               2*t2*(9 + t2 - s*t2 + (7 + s)*Power(t2,2)) + 
               s2*(-3 + (19 + 4*s)*t2 + (-2 + 5*s)*Power(t2,2) + 
                  16*Power(t2,3))) + 
            Power(s1,3)*(-25 + (29 + 15*s)*t2 + 
               (-25 + 9*s)*Power(t2,2) + 
               (-37 - 8*s + 5*Power(s,2))*Power(t2,3) - 
               6*(-3 + s)*Power(t2,4) + Power(s2,3)*t2*(11 + 5*t2) + 
               2*Power(s2,2)*
                (3 - 2*(-4 + s)*t2 + (13 - 2*s)*Power(t2,2) + 
                  9*Power(t2,3)) + 
               s2*(32 + (-5 + s)*t2 + 
                  (51 + 37*s - 3*Power(s,2))*Power(t2,2) + 
                  (119 - 44*s)*Power(t2,3))) + 
            Power(s1,2)*(12 + (37 + 14*s)*t2 + 
               (17 - 26*s + Power(s,2))*Power(t2,2) + 
               (67 + 10*s + 25*Power(s,2) - 3*Power(s,3))*Power(t2,3) + 
               (101 - 48*s - 3*Power(s,2))*Power(t2,4) + 
               (2 + 6*s)*Power(t2,5) + 
               Power(s2,3)*(6 - 59*t2 - 63*Power(t2,2) + 
                  7*Power(t2,3)) - 
               Power(s2,2)*(58 + (52 - 28*s)*t2 + 
                  (2 - 26*s)*Power(t2,2) + (-4 + 15*s)*Power(t2,3) + 
                  2*Power(t2,4)) - 
               s2*(-9 + (5 - 15*s)*t2 + 
                  (103 + 67*s + 17*Power(s,2))*Power(t2,2) + 
                  (212 + 240*s - 27*Power(s,2))*Power(t2,3) + 
                  (133 - 18*s)*Power(t2,4) + 16*Power(t2,5))) + 
            s1*(29 + (57 - 25*s)*t2 + 
               (-44 - 61*s + 37*Power(s,2))*Power(t2,2) + 
               (49 - 127*s + 72*Power(s,2) - 18*Power(s,3))*
                Power(t2,3) + 
               (20 + 59*s - 28*Power(s,2) + 8*Power(s,3))*Power(t2,4) + 
               (-27 + 62*s + 9*Power(s,2))*Power(t2,5) + 
               2*(-3 + s)*Power(t2,6) + 
               Power(s2,3)*(-12 + 45*t2 + 86*Power(t2,2) + 
                  7*Power(t2,3) - 5*Power(t2,4)) - 
               Power(s2,2)*(-66 + 2*(-11 + 5*s)*t2 + 
                  (93 + 38*s)*Power(t2,2) + (56 + 26*s)*Power(t2,3) + 
                  (94 - 4*s)*Power(t2,4) + 18*Power(t2,5)) + 
               s2*(-56 - (33 + 83*s)*t2 + 
                  (110 - 225*s + 48*Power(s,2))*Power(t2,2) + 
                  (-81 + 45*s + 34*Power(s,2))*Power(t2,3) + 
                  (-85 + 183*s - 6*Power(s,2))*Power(t2,4) + 
                  (-31 + 14*s)*Power(t2,5) + 2*Power(t2,6)))) + 
         Power(t1,9)*(-26 - 4*Power(s1,5) - 37*t2 - 32*s2*t2 + 
            7*Power(t2,2) - 14*s2*Power(t2,2) - 
            6*Power(s2,2)*Power(t2,2) + 
            Power(s1,4)*(-4 + 20*s2 + 15*t2) + 
            Power(s1,3)*(29 + 7*Power(s2,2) + 2*t2 - 9*Power(t2,2) - 
               11*s2*(7 + 3*t2)) + 
            Power(s,3)*(2 + Power(s1,2) + 29*t2 + 20*Power(t2,2) - 
               2*s1*(1 + 8*t2)) + 
            s1*(-9 - 51*t2 - 2*Power(s2,2)*(-2 + t2)*t2 + 
               105*Power(t2,2) + s2*(12 + 107*t2 - 2*Power(t2,2))) + 
            Power(s1,2)*(-84 + 6*t2 + 64*Power(t2,2) + 
               Power(s2,2)*(28 + 3*t2) + 
               s2*(86 + 124*t2 + 9*Power(t2,2))) - 
            Power(s,2)*(24 + 12*Power(s1,3) + (115 + 26*s2)*t2 + 
               24*(3 + s2)*Power(t2,2) + 
               s1*(-29 - 7*s2*(-2 + t2) + 31*t2 + 64*Power(t2,2)) - 
               Power(s1,2)*(3*s2 + 76*(1 + t2))) + 
            s*(48 + 15*Power(s1,4) + (129 + 58*s2)*t2 + 
               (63 + 46*s2 + 6*Power(s2,2))*Power(t2,2) - 
               2*Power(s1,3)*(10 + 7*s2 + 37*t2) - 
               Power(s1,2)*(78 + 175*t2 - 54*Power(t2,2) + 
                  5*s2*(10 + 3*t2)) + 
               2*s1*(-4 + 25*t2 - Power(s2,2)*t2 + 59*Power(t2,2) + 
                  s2*(4 + 9*t2 + 19*Power(t2,2))))) + 
         Power(t1,8)*(20 + 69*t2 + 66*s2*t2 + 61*Power(t2,2) + 
            30*s2*Power(t2,2) + 30*Power(s2,2)*Power(t2,2) - 
            48*Power(t2,3) + 15*s2*Power(t2,3) + 
            14*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) + 
            2*Power(s1,5)*(-3 + 6*s2 + 5*t2) + 
            Power(s1,4)*(46 + 5*Power(s2,2) + 30*t2 - 18*Power(t2,2) - 
               s2*(65 + 47*t2)) + 
            Power(s1,3)*(-102 + Power(s2,2)*(1 - 3*t2) - 39*t2 - 
               51*Power(t2,2) + 7*Power(t2,3) + 
               s2*(115 + 69*t2 + 37*Power(t2,2))) - 
            s1*(34 - 212*t2 - 9*Power(t2,2) + 
               4*Power(s2,3)*Power(t2,2) + 139*Power(t2,3) + 
               Power(s2,2)*t2*(80 + 17*t2 - 8*Power(t2,2)) + 
               s2*(-4 + 193*t2 + 211*Power(t2,2) - 2*Power(t2,3))) - 
            Power(s1,2)*(-40 - 151*t2 + 78*Power(t2,2) + 
               26*Power(t2,3) + Power(s2,3)*(6 + 4*t2) + 
               Power(s2,2)*(81 + 71*t2 + 18*Power(t2,2)) + 
               s2*(-75 + 88*t2 + 52*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s,3)*(-10*Power(s1,2)*(-1 + t2) + 
               s1*(-3 + 13*t2 + 34*Power(t2,2)) - 
               2*(-3 + 5*t2 + 27*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s,2)*(-12 + (79 + 38*s2)*t2 + 
               (219 + 95*s2)*Power(t2,2) + 4*(22 + 9*s2)*Power(t2,3) + 
               Power(s1,3)*(32 + s2 + 45*t2) + 
               Power(s1,2)*(-1 + 6*s2*(-7 + t2) - 259*t2 - 
                  115*Power(t2,2)) + 
               s1*(-49 - 258*t2 + Power(t2,2) + 56*Power(t2,3) + 
                  s2*(22 + 19*t2 - 45*Power(t2,2)))) - 
            s*(12 + 2*(77 + 52*s2)*t2 + 
               (201 + 207*s2 + 28*Power(s2,2))*Power(t2,2) + 
               2*(46 + 37*s2 + 9*Power(s2,2))*Power(t2,3) + 
               Power(s1,4)*(24 + 11*s2 + 43*t2) - 
               Power(s1,3)*(-36 + 77*t2 + 93*Power(t2,2) + 
                  10*s2*(2 + t2)) + 
               Power(s1,2)*(20 + 3*Power(s2,2)*(-7 + t2) - 306*t2 - 
                  278*Power(t2,2) + 40*Power(t2,3) - 
                  s2*(85 + 253*t2 + 74*Power(t2,2))) + 
               s1*(-108 - 131*t2 + Power(s2,2)*(8 - 21*t2)*t2 - 
                  73*Power(t2,2) + 142*Power(t2,3) + 
                  2*s2*(24 - 67*t2 + 29*Power(t2,2) + 27*Power(t2,3))))) \
+ Power(t1,7)*(10 - 16*t2 - 64*s2*t2 - 170*Power(t2,2) - 
            73*s2*Power(t2,2) - 26*Power(s2,2)*Power(t2,2) - 
            26*Power(t2,3) + 29*s2*Power(t2,3) - 
            64*Power(s2,2)*Power(t2,3) - 8*Power(s2,3)*Power(t2,3) + 
            70*Power(t2,4) - 11*s2*Power(t2,4) - 
            14*Power(s2,2)*Power(t2,4) - 4*Power(s2,3)*Power(t2,4) + 
            Power(s1,5)*(-22*s2*(1 + t2) + 8*(3 + t2 - Power(t2,2))) + 
            Power(s1,4)*(-79 - 66*t2 - 61*Power(t2,2) + 
               10*Power(t2,3) - Power(s2,2)*(7 + 8*t2) + 
               s2*(78 + 49*t2 + 43*Power(t2,2))) - 
            Power(s1,3)*(-52 - 63*t2 + 5*Power(t2,2) - 67*Power(t2,3) + 
               2*Power(t2,4) + 10*Power(s2,2)*(3 + t2) + 
               Power(s2,3)*(1 + 5*t2) + 
               s2*(-2 - 23*t2 - 88*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s,3)*(-4 - 29*t2 + 16*Power(t2,2) + 46*Power(t2,3) + 
               10*Power(t2,4) + 
               Power(s1,2)*(1 - 32*t2 + 24*Power(t2,2)) - 
               2*s1*(-1 + 6*t2 + 10*Power(t2,2) + 18*Power(t2,3))) + 
            Power(s1,2)*(55 + 5*t2 - 73*Power(t2,2) + 167*Power(t2,3) - 
               Power(t2,4) + Power(s2,3)*(24 + 17*t2 - 4*Power(t2,2)) + 
               2*Power(s2,2)*
                (17 + 63*t2 + 49*Power(t2,2) + 15*Power(t2,3)) + 
               s2*(-138 - 281*t2 + 14*Power(t2,2) - 64*Power(t2,3) + 
                  Power(t2,4))) + 
            s1*(76 - 27*t2 - 462*Power(t2,2) + 148*Power(t2,3) + 
               79*Power(t2,4) + 
               Power(s2,3)*t2*(12 + 20*t2 + 11*Power(t2,2)) + 
               Power(s2,2)*t2*
                (154 + 190*t2 + 19*Power(t2,2) - 10*Power(t2,3)) + 
               s2*(-56 - 66*t2 + 365*Power(t2,2) + 111*Power(t2,3) + 
                  2*Power(t2,4))) - 
            Power(s,2)*(-30 + (-97 + 10*s2)*t2 + 
               10*(1 + 13*s2)*Power(t2,2) + 
               (209 + 123*s2)*Power(t2,3) + (62 + 24*s2)*Power(t2,4) + 
               Power(s1,3)*(-4 + 98*t2 + 63*Power(t2,2) + 
                  s2*(10 + 3*t2)) + 
               Power(s1,2)*(56 + 49*t2 - 305*Power(t2,2) - 
                  89*Power(t2,3) + s2*(-41 - 115*t2 + 36*Power(t2,2))) \
+ s1*(1 - 230*t2 - 682*Power(t2,2) - 65*Power(t2,3) + 24*Power(t2,4) + 
                  s2*(10 + 3*t2 - 30*Power(t2,2) - 73*Power(t2,3)))) + 
            s*(-42 + 12*(-5 + 6*s2)*t2 + 
               (145 + 240*s2 + 56*Power(s2,2))*Power(t2,2) + 
               (128 + 289*s2 + 75*Power(s2,2))*Power(t2,3) + 
               2*(44 + 33*s2 + 9*Power(s2,2))*Power(t2,4) + 
               Power(s1,4)*(-17 + 80*t2 + 43*Power(t2,2) + 
                  s2*(26 + 21*t2)) + 
               Power(s1,3)*(51 + 99*t2 - 94*Power(t2,2) - 
                  54*Power(t2,3) + 4*Power(s2,2)*(1 + t2) + 
                  s2*(-7 + 31*t2 + 26*Power(t2,2))) + 
               Power(s1,2)*(69 + 101*t2 - 539*Power(t2,2) - 
                  221*Power(t2,3) + 11*Power(t2,4) + 
                  Power(s2,2)*(-49 - 62*t2 + 18*Power(t2,2)) - 
                  s2*(15 + 337*t2 + 424*Power(t2,2) + 102*Power(t2,3))) \
- 2*s1*(48 + 143*t2 + 211*Power(t2,2) + 92*Power(t2,3) - 
                  44*Power(t2,4) + 
                  Power(s2,2)*t2*(-3 + 10*t2 + 24*Power(t2,2)) + 
                  s2*(-38 + 87*t2 + 280*Power(t2,2) - 11*Power(t2,3) - 
                     17*Power(t2,4))))) - 
         Power(t1,6)*(28 + 96*t2 - 26*s2*t2 - 27*Power(t2,2) - 
            141*s2*Power(t2,2) + 13*Power(s2,2)*Power(t2,2) + 
            6*Power(s2,3)*Power(t2,2) - 290*Power(t2,3) + 
            74*s2*Power(t2,3) - 54*Power(s2,2)*Power(t2,3) - 
            10*Power(s2,3)*Power(t2,3) + 67*Power(t2,4) + 
            20*s2*Power(t2,4) - 47*Power(s2,2)*Power(t2,4) - 
            15*Power(s2,3)*Power(t2,4) + 44*Power(t2,5) - 
            3*s2*Power(t2,5) - 10*Power(s2,2)*Power(t2,5) - 
            2*Power(s2,3)*Power(t2,5) - 
            2*Power(s1,5)*(-11 - 15*t2 - 3*Power(t2,2) + Power(t2,3) + 
               4*s2*(2 + 3*t2 + 2*Power(t2,2))) - 
            Power(s1,4)*(43 + 33*t2 + 54*Power(t2,2) + 43*Power(t2,3) - 
               2*Power(t2,4) + 
               Power(s2,2)*(-2 + 13*t2 + 13*Power(t2,2)) - 
               2*s2*(11 - 5*t2 - 20*Power(t2,2) + 9*Power(t2,3))) + 
            Power(s,3)*t2*(Power(s1,2)*(6 - 36*t2 + 22*Power(t2,2)) - 
               s1*(-9 + 62*t2 + 2*Power(t2,2) + 19*Power(t2,3)) + 
               2*(-10 - 36*t2 + 5*Power(t2,2) + 7*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s1,3)*(-52 - 33*t2 - 92*Power(t2,2) - 
               92*Power(t2,3) + 37*Power(t2,4) + 
               Power(s2,3)*(-5 - 9*t2 + Power(t2,2)) + 
               Power(s2,2)*(-20 - 18*t2 - 31*Power(t2,2) + 
                  4*Power(t2,3)) + 
               s2*(84 + 171*t2 + 254*Power(t2,2) + 181*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,2)*(12 + 129*t2 + 83*Power(t2,3) + 
               146*Power(t2,4) - 2*Power(t2,5) + 
               Power(s2,3)*(36 + 27*t2 - 25*Power(t2,2) - 
                  6*Power(t2,3)) + 
               s2*(53 - 269*t2 - 148*Power(t2,2) + 47*Power(t2,3) - 
                  52*Power(t2,4)) + 
               Power(s2,2)*(-74 + 58*t2 + 151*Power(t2,2) + 
                  101*Power(t2,3) + 21*Power(t2,4))) + 
            s1*(57 + 262*t2 - 309*Power(t2,2) - 325*Power(t2,3) + 
               239*Power(t2,4) + 19*Power(t2,5) + 
               Power(s2,3)*t2*
                (48 + 30*t2 + 31*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-44 + 321*t2 + 235*Power(t2,2) + 20*Power(t2,3) - 
                  4*Power(t2,4)) + 
               s2*(-84 - 230*t2 - 62*Power(t2,2) + 188*Power(t2,3) - 
                  61*Power(t2,4) + 2*Power(t2,5))) + 
            Power(s,2)*(12 + (131 + 20*s2)*t2 + 
               (301 + 3*s2)*Power(t2,2) - 3*(-81 + 52*s2)*Power(t2,3) - 
               (98 + 59*s2)*Power(t2,4) - 6*(4 + s2)*Power(t2,5) + 
               s1*(-16 + (-78 + 11*s2)*t2 + 
                  (394 + 129*s2)*Power(t2,2) + 
                  (779 + 64*s2)*Power(t2,3) + 
                  (86 + 49*s2)*Power(t2,4) - 4*Power(t2,5)) - 
               Power(s1,3)*(3*s2*(-1 + 10*t2 + Power(t2,2)) + 
                  t2*(-7 + 102*t2 + 39*Power(t2,2))) + 
               Power(s1,2)*(-1 - 163*t2 - 150*Power(t2,2) + 
                  121*Power(t2,3) + 37*Power(t2,4) + 
                  s2*(2 + 96*t2 + 93*Power(t2,2) - 42*Power(t2,3)))) + 
            s*(-42 - 2*(117 + 4*s2)*t2 + 
               (-234 + 7*s2 + 39*Power(s2,2))*Power(t2,2) + 
               (-34 + 75*s2 + 139*Power(s2,2))*Power(t2,3) + 
               2*(13 + 96*s2 + 30*Power(s2,2))*Power(t2,4) + 
               (48 + 34*s2 + 6*Power(s2,2))*Power(t2,5) + 
               Power(s1,4)*(-22 - 13*t2 + 84*Power(t2,2) + 
                  17*Power(t2,3) + s2*(7 + 42*t2 + 17*Power(t2,2))) + 
               Power(s1,3)*(14 + 63*t2 + 165*Power(t2,2) - 
                  37*Power(t2,3) - 15*Power(t2,4) + 
                  4*Power(s2,2)*(1 + 6*t2 + Power(t2,2)) + 
                  s2*(4 + 95*t2 + 125*Power(t2,2) + 26*Power(t2,3))) + 
               Power(s1,2)*(23 + 284*t2 + 103*Power(t2,2) - 
                  525*Power(t2,3) - 97*Power(t2,4) + 
                  Power(s2,2)*
                   (-35 - 144*t2 - 34*Power(t2,2) + 27*Power(t2,3)) - 
                  s2*(-10 + 30*t2 + 411*Power(t2,2) + 296*Power(t2,3) + 
                     57*Power(t2,4))) - 
               s1*(5 + 200*t2 + 214*Power(t2,2) + 622*Power(t2,3) + 
                  117*Power(t2,4) - 22*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (22 + 20*t2 + 85*Power(t2,2) + 41*Power(t2,3)) + 
                  s2*(-50 - 132*t2 + 926*Power(t2,2) + 820*Power(t2,3) + 
                     66*Power(t2,4) - 8*Power(t2,5))))) + 
         Power(t1,4)*(-4 + (-59 - 2*s2 + 4*s*(7 + 3*s2))*t2 + 
            (-175 - 176*s2 + 158*Power(s2,2) - 36*Power(s2,3) - 
               2*Power(s,2)*(21 + 20*s2) + 
               3*s*(69 + 26*s2 + 9*Power(s2,2)))*Power(t2,2) + 
            (-93 + 46*Power(s,3) - 17*s2 - 42*Power(s2,2) + 
               17*Power(s2,3) - Power(s,2)*(137 + 190*s2) + 
               s*(300 + 390*s2 + 32*Power(s2,2)))*Power(t2,3) + 
            (308 + 98*Power(s,3) + 26*s2 - 187*Power(s2,2) + 
               49*Power(s2,3) - Power(s,2)*(366 + 137*s2) + 
               s*(69 + 744*s2 - 44*Power(s2,2)))*Power(t2,4) + 
            (-11 - 4*Power(s,3) + 80*s2 - 42*Power(s2,2) + 
               2*Power(s2,3) - 2*Power(s,2)*(97 + s2) + 
               s*(128 + 192*s2 + 3*Power(s2,2)))*Power(t2,5) + 
            (-66 + 2*Power(s,3) + 36*s2 + Power(s2,2) - 
               2*Power(s2,3) - Power(s,2)*(13 + 6*s2) + 
               s*(-15 + 11*s2 + 6*Power(s2,2)))*Power(t2,6) - 
            2*Power(t2,7) + 2*Power(s1,5)*t2*
             (-4 + t2 - 3*Power(t2,2) + 2*s2*(1 - 3*t2 + Power(t2,2))) + 
            Power(s1,4)*(-12 + t2 + 8*s*t2 + 2*(-22 + s)*Power(t2,2) - 
               (43 + s)*Power(t2,3) + (2 - 4*s)*Power(t2,4) + 
               Power(s2,2)*(-3 + 11*t2 + 18*Power(t2,2) + 
                  7*Power(t2,3)) + 
               s2*(15 + 5*t2 + (56 + 27*s)*Power(t2,2) - 
                  14*(-6 + s)*Power(t2,3) + 12*Power(t2,4))) + 
            Power(s1,3)*(5 + t2 + 18*s*t2 + (71 + 28*s)*Power(t2,2) + 
               (17 - s + 11*Power(s,2))*Power(t2,3) + 
               (83 - 75*s + 2*Power(s,2))*Power(t2,4) + 
               18*Power(t2,5) + 
               Power(s2,3)*(3 - t2 + 3*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(-9 + (9 - 4*s)*t2 + 
                  (6 - 24*s)*Power(t2,2) + (50 - 4*s)*Power(t2,3) + 
                  12*Power(t2,4)) - 
               s2*(-7 + (-30 + 11*s)*t2 + 
                  (58 - 13*s + 9*Power(s,2))*Power(t2,2) + 
                  (-21 + 173*s - 10*Power(s,2))*Power(t2,3) + 
                  (3 + 11*s)*Power(t2,4) + 20*Power(t2,5))) + 
            Power(s1,2)*(14 + 6*(17 + s)*t2 + 
               (28 - 91*s + 3*Power(s,2))*Power(t2,2) - 
               (2 + 107*s - 71*Power(s,2) + 10*Power(s,3))*
                Power(t2,3) + 
               (176 - 53*s + 43*Power(s,2) + 2*Power(s,3))*
                Power(t2,4) + (27 + 65*s + 16*Power(s,2))*Power(t2,5) + 
               2*(-7 + 2*s)*Power(t2,6) + 
               Power(s2,3)*(-6 + 7*t2 - 47*Power(t2,2) - 
                  23*Power(t2,3) + 12*Power(t2,4)) + 
               Power(s2,2)*(21 + (-123 + 23*s)*t2 + 
                  2*(-52 + 45*s)*Power(t2,2) + 
                  (34 + 4*s)*Power(t2,3) - (53 + 23*s)*Power(t2,4) - 
                  13*Power(t2,5)) + 
               s2*(-43 + 13*(-6 + s)*t2 - 
                  (23 + 89*s + 21*Power(s,2))*Power(t2,2) + 
                  (-302 - 327*s + 40*Power(s,2))*Power(t2,3) + 
                  (-375 + 20*s + 11*Power(s,2))*Power(t2,4) + 
                  (-125 + 7*s)*Power(t2,5) + 4*Power(t2,6))) + 
            s1*(Power(s2,3)*t2*
                (-48 + 43*t2 + 109*Power(t2,2) - 22*Power(t2,3) - 
                  11*Power(t2,4)) + 
               s2*(12 - (165 + 56*s)*t2 + 
                  (13 - 340*s + 40*Power(s,2))*Power(t2,2) + 
                  (291 + 249*s - 38*Power(s,2))*Power(t2,3) + 
                  (-30 + 721*s - 81*Power(s,2))*Power(t2,4) - 
                  3*(-21 - 54*s + Power(s,2))*Power(t2,5) + 
                  2*(8 + 9*s)*Power(t2,6)) + 
               Power(s2,2)*t2*
                (260 + 131*t2 - 350*Power(t2,2) - 204*Power(t2,3) - 
                  96*Power(t2,4) - 6*Power(t2,5) + 
                  s*(2 + 11*t2 + 12*Power(t2,2) + 44*Power(t2,3) + 
                     21*Power(t2,4))) - 
               t2*(-44 + 84*t2 - 13*Power(t2,2) - 154*Power(t2,3) + 
                  150*Power(t2,4) + 47*Power(t2,5) + 
                  Power(s,3)*Power(t2,2)*(26 - 45*t2 + 7*Power(t2,2)) + 
                  Power(s,2)*t2*
                   (-69 - 213*t2 + 107*Power(t2,2) + 55*Power(t2,3) + 
                     12*Power(t2,4)) + 
                  s*(39 + 19*t2 + 255*Power(t2,2) - 77*Power(t2,3) - 
                     251*Power(t2,4) - 7*Power(t2,5))))) + 
         Power(t1,5)*(18 + 123*t2 + 217*Power(t2,2) - 32*s2*Power(t2,2) - 
            48*Power(s2,2)*Power(t2,2) + 24*Power(s2,3)*Power(t2,2) - 
            222*Power(t2,3) - 16*s2*Power(t2,3) + 
            44*Power(s2,2)*Power(t2,3) - 7*Power(s2,3)*Power(t2,3) - 
            209*Power(t2,4) + 61*s2*Power(t2,4) + 
            14*Power(s2,2)*Power(t2,4) - 25*Power(s2,3)*Power(t2,4) + 
            108*Power(t2,5) - 34*s2*Power(t2,5) - 
            16*Power(s2,2)*Power(t2,5) - 5*Power(s2,3)*Power(t2,5) + 
            13*Power(t2,6) + s2*Power(t2,6) - 4*Power(s2,2)*Power(t2,6) + 
            Power(s,3)*Power(t2,2)*
             (-42 - 110*t2 + 4*Power(t2,2) - 3*Power(t2,3) + 
               Power(s1,2)*(12 - 16*t2 + 7*Power(t2,2)) + 
               s1*(20 - 84*t2 + 14*Power(t2,2) - 4*Power(t2,3))) + 
            Power(s1,5)*(-6*s2*(1 + t2 + Power(t2,2) + Power(t2,3)) + 
               4*(2 + 5*t2 + 3*Power(t2,2) + Power(t2,3))) - 
            Power(s1,4)*(-11 + 13*t2 - 51*Power(t2,2) + 12*Power(t2,3) + 
               14*Power(t2,4) + 
               Power(s2,2)*(-7 + 12*t2 + 25*Power(t2,2) + 
                  4*Power(t2,3)) + 
               s2*(26 + 20*t2 + 120*Power(t2,2) + 66*Power(t2,3) - 
                  8*Power(t2,4))) + 
            Power(s1,3)*(-44 - 90*t2 - 69*Power(t2,2) - 
               196*Power(t2,3) - 71*Power(t2,4) + 10*Power(t2,5) + 
               Power(s2,3)*(-7 - 3*t2 + 3*Power(t2,2)) - 
               Power(s2,2)*(-11 + 8*t2 + 32*Power(t2,2) + 
                  28*Power(t2,3)) + 
               s2*(27 + 114*t2 + 187*Power(t2,2) + 197*Power(t2,3) + 
                  97*Power(t2,4) - 2*Power(t2,5))) + 
            Power(s1,2)*(-36 - 67*t2 + 110*Power(t2,2) - 
               62*Power(t2,3) + 34*Power(t2,4) + 59*Power(t2,5) + 
               Power(s2,3)*(24 + 13*t2 - 15*Power(t2,2) - 
                  12*Power(t2,3) - 4*Power(t2,4)) + 
               s2*(130 + 70*t2 - 116*Power(t2,2) + 312*Power(t2,3) + 
                  162*Power(t2,4) - 5*Power(t2,5)) + 
               Power(s2,2)*(-78 + 65*t2 + 130*Power(t2,2) + 
                  49*Power(t2,3) + 77*Power(t2,4) + 4*Power(t2,5))) + 
            s1*(15 + 139*t2 + 170*Power(t2,2) - 417*Power(t2,3) + 
               73*Power(t2,4) + 163*Power(t2,5) + 2*Power(t2,6) + 
               Power(s2,3)*t2*
                (72 + t2 - 19*Power(t2,2) + 34*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-316 + 70*t2 + 463*Power(t2,2) + 167*Power(t2,3) + 
                  20*Power(t2,4)) - 
               s2*(52 - 31*t2 + 284*Power(t2,2) + 102*Power(t2,3) + 
                  74*Power(t2,4) + 78*Power(t2,5))) + 
            Power(s,2)*t2*(38 + 206*t2 + 459*Power(t2,2) + 
               357*Power(t2,3) - 9*Power(t2,4) - 4*Power(t2,5) + 
               s2*(12 + 102*t2 + 85*Power(t2,2) - 68*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s1,3)*(s2*(-9 + 30*t2 + Power(t2,2)) + 
                  t2*(3 + 38*t2 + 9*Power(t2,2))) + 
               Power(s1,2)*(-3 - 163*t2 - 146*Power(t2,2) - 
                  17*Power(t2,3) + 7*Power(t2,4) + 
                  s2*(11 + 42*t2 + 9*Power(t2,2) - 15*Power(t2,3))) + 
               s1*(-55 - 222*t2 + 302*Power(t2,2) + 390*Power(t2,3) + 
                  50*Power(t2,4) + 
                  s2*(-12 + 68*t2 + 185*Power(t2,2) + 32*Power(t2,3) + 
                     12*Power(t2,4)))) + 
            s*(-12 - (149 + 34*s2)*t2 - 
               (431 + 129*s2 + 15*Power(s2,2))*Power(t2,2) + 
               3*(-77 - 155*s2 + 30*Power(s2,2))*Power(t2,3) + 
               (-157 - 202*s2 + 95*Power(s2,2))*Power(t2,4) + 
               (11 + 48*s2 + 7*Power(s2,2))*Power(t2,5) + 
               (11 + 8*s2)*Power(t2,6) + 
               Power(s1,4)*(-8 - 22*t2 + 3*Power(t2,2) + 
                  32*Power(t2,3) + 2*Power(t2,4) + 
                  s2*(4 - 15*t2 + 30*Power(t2,2) + 7*Power(t2,3))) + 
               Power(s1,3)*(-3 - 9*t2 + 9*Power(t2,2) + 
                  171*Power(t2,3) - 2*Power(t2,5) + 
                  24*Power(s2,2)*t2*(1 + t2) + 
                  s2*(5 + 17*t2 + 231*Power(t2,2) + 85*Power(t2,3) + 
                     4*Power(t2,4))) + 
               Power(s1,2)*(2 + 78*t2 + 343*Power(t2,2) + 
                  27*Power(t2,3) - 273*Power(t2,4) - 26*Power(t2,5) + 
                  Power(s2,2)*
                   (-7 - 102*t2 - 114*Power(t2,2) + 30*Power(t2,3) + 
                     12*Power(t2,4)) - 
                  s2*(7 - 56*t2 - 115*Power(t2,2) + 161*Power(t2,3) + 
                     82*Power(t2,4) + 11*Power(t2,5))) - 
               s1*(-14 + 4*t2 - 11*Power(t2,2) + 53*Power(t2,3) + 
                  516*Power(t2,4) + 28*Power(t2,5) + 
                  2*Power(s2,2)*t2*
                   (10 + 21*t2 + 33*Power(t2,2) + 39*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  s2*(-12 - 220*t2 + 89*Power(t2,2) + 1271*Power(t2,3) + 
                     550*Power(t2,4) + 68*Power(t2,5))))))*R1(t1))/
     ((-1 + s2)*(-s + s2 - t1)*Power(-1 + t1,3)*t1*Power(t1 - t2,3)*
       (-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)) + 
    (8*(-2*Power(s1,9)*Power(t1,2)*(t1 - t2)*t2*
          (Power(t1,2) + (2 - 3*s2)*t2 - t1*(-2 + s2 + t2)) + 
         Power(s1,8)*t1*(-12*Power(t1,5)*t2 + 
            4*(-2 + 3*s2)*Power(t2,4) + 
            2*Power(t1,4)*(-1 + (-6 + s + 6*s2)*t2 + 13*Power(t2,2)) - 
            Power(t1,3)*(2 + (-17 - 4*s + 6*s2)*t2 + 
               2*(7 + s - 13*s2)*Power(t2,2) + 17*Power(t2,3)) + 
            Power(t1,2)*t2*(8 + 13*t2 + 4*Power(s2,2)*t2 - 
               2*(-16 + s)*Power(t2,2) + 3*Power(t2,3) - 
               s2*(3 + 4*(7 + 2*s)*t2 + 57*Power(t2,2))) + 
            t1*Power(t2,2)*(2*Power(s2,2)*(3 + t2) + 
               s2*(-15 + 2*(5 + 4*s)*t2 + 25*Power(t2,2)) + 
               2*(3 - (7 + 2*s)*t2 + (-6 + s)*Power(t2,2)))) - 
         Power(s1,7)*(30*Power(t1,7)*t2 + 
            2*Power(t2,4)*(-Power(s2,3) - 2*t2 + 3*s2*t2) - 
            2*Power(t1,6)*(-6 + 3*(2 + 2*s + 5*s2)*t2 + 
               34*Power(t2,2)) + 
            Power(t1,5)*(12 + (-109 - 24*s + 40*s2)*t2 + 
               (97 + 8*s - 70*s2)*Power(t2,2) + 48*Power(t2,3)) + 
            t1*Power(t2,3)*(12 - Power(s2,3)*(-5 + t2) - 
               4*(7 + 2*s)*t2 + (-30 + 4*s)*Power(t2,2) + 
               2*Power(s2,2)*(6 + t2 + s*t2 + Power(t2,2)) + 
               2*s2*(-15 + 8*(2 + s)*t2 + 25*Power(t2,2))) - 
            Power(t1,4)*t2*(59 + 47*t2 + 32*Power(s2,2)*t2 + 
               128*Power(t2,2) + 8*Power(t2,3) + 
               s*(1 - 60*s2*t2 - 31*Power(t2,2)) - 
               s2*(23 + 183*t2 + 204*Power(t2,2))) + 
            Power(t1,3)*t2*(-3 + 6*(3 + s)*t2 + 2*Power(s2,3)*t2 + 
               (37 + 44*s)*Power(t2,2) + (88 - 38*s)*Power(t2,3) - 
               2*Power(t2,4) + Power(s2,2)*t2*(-61 - 2*s + 11*t2) + 
               s2*(2 + (86 + 8*s)*t2 - (83 + 88*s)*Power(t2,2) - 
                  153*Power(t2,3))) + 
            Power(t1,2)*Power(t2,2)*
             (21 + (19 + 3*s)*t2 + (89 - 24*s)*Power(t2,2) + 
               (-21 + 11*s)*Power(t2,3) + Power(s2,3)*(3 + 5*t2) + 
               Power(s2,2)*(-18 + 29*t2 - 5*Power(t2,2)) + 
               s2*(-2 - 3*(27 + 8*s)*t2 + 2*(-59 + 14*s)*Power(t2,2) + 
                  25*Power(t2,3)))) - 
         Power(s1,6)*(40*Power(t1,8)*t2 - 
            2*Power(t1,7)*(-15 + 5*(10 + 3*s + 4*s2)*t2 + 
               46*Power(t2,2)) + 
            Power(t1,6)*(30 - 2*(148 + 30*s - 55*s2)*t2 + 
               (281 + 8*s - 96*s2)*Power(t2,2) + 57*Power(t2,3)) + 
            Power(t1,5)*(-4 + (-197 - 7*s + 68*s2)*t2 + 
               (131 + s + 8*Power(s,2) + 499*s2 + 180*s*s2 - 
                  100*Power(s2,2))*Power(t2,2) + 
               (-311 + 144*s + 349*s2)*Power(t2,3) + 13*Power(t2,4)) - 
            Power(t1,4)*(4 - 2*(-12 + s2)*t2 + 
               (-355 - 173*s2 + 4*Power(s,2)*s2 + 231*Power(s2,2) - 
                  8*Power(s2,3) + s*(-80 - 56*s2 + 6*Power(s2,2)))*
                Power(t2,2) + 
               (259 + 24*Power(s,2) + 350*s2 - 99*Power(s2,2) + 
                  s*(-214 + 336*s2))*Power(t2,3) + 
               (-279 + 208*s + 335*s2)*Power(t2,4) + 23*Power(t2,5)) + 
            Power(t2,4)*(-6 + 2*(7 + 2*s)*t2 - 2*(-8 + s)*Power(t2,2) - 
               2*Power(s2,2)*t2*(4*s + t2) + Power(s2,3)*(3 + 5*t2) - 
               s2*(-15 + 2*(9 + 4*s)*t2 + 25*Power(t2,2))) - 
            t1*Power(t2,3)*(18 + 3*(5 + 2*s)*t2 + 
               (66 - 42*s)*Power(t2,2) + (-51 + 22*s)*Power(t2,3) + 
               Power(s2,3)*(21 + 32*t2 - 7*Power(t2,2)) + 
               Power(s2,2)*(-21 + (27 - 13*s)*t2 + 
                  (-8 + 11*s)*Power(t2,2) + 4*Power(t2,3)) - 
               s2*(-5 + (90 + 26*s)*t2 + 
                  (61 - 66*s + 4*Power(s,2))*Power(t2,2) + 
                  (-50 + 4*s)*Power(t2,3))) + 
            Power(t1,2)*Power(t2,2)*
             (2 + (63 - 4*s)*t2 - 4*(64 + 31*s)*Power(t2,2) + 
               (-347 + 128*s - 8*Power(s,2))*Power(t2,3) - 
               4*(-7 + 4*s)*Power(t2,4) - 
               6*Power(s2,3)*(1 - 3*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(51 - 6*(-31 + s)*t2 + 
                  (-47 + 32*s)*Power(t2,2) + 34*Power(t2,3)) + 
               s2*(-35 - 10*(35 + 2*s)*t2 + 
                  (447 + 284*s - 12*Power(s,2))*Power(t2,2) + 
                  (455 - 32*s)*Power(t2,3) + 5*Power(t2,4))) + 
            Power(t1,3)*t2*(14 + (200 + 6*s)*t2 + 
               (148 + 11*s)*Power(t2,2) + 
               3*(170 - 87*s + 8*Power(s,2))*Power(t2,3) + 
               3*(-47 + 34*s)*Power(t2,4) + 5*Power(t2,5) + 
               3*Power(s2,3)*t2*(5 + 11*t2) + 
               Power(s2,2)*t2*
                (-81 + s + 242*t2 - 15*s*t2 - 65*Power(t2,2)) + 
               s2*(-11 + 2*(-21 + s)*t2 + 
                  2*(-377 - 137*s + 6*Power(s,2))*Power(t2,2) + 
                  8*(-56 + 23*s)*Power(t2,3) + 81*Power(t2,4)))) + 
         Power(s1,5)*(-30*Power(t1,9)*t2 + 
            2*Power(t1,8)*(-20 + 5*(4*s + 3*(6 + s2))*t2 + 
               34*Power(t2,2)) + 
            Power(t1,7)*(-40 + 2*(217 + 40*s - 80*s2)*t2 + 
               (-437 + 8*s + 64*s2)*Power(t2,2) - 23*Power(t2,3)) - 
            Power(t1,6)*(-16 - 2*(175 + 9*s - 51*s2)*t2 + 
               (731 + 40*Power(s,2) + 719*s2 - 160*Power(s2,2) + 
                  5*s*(1 + 56*s2))*Power(t2,2) + 
               (307*s + 57*(-8 + 5*s2))*Power(t2,3) + 57*Power(t2,4)) + 
            Power(t1,5)*(14 + (45 - 8*s + 20*s2)*t2 + 
               (-1140 + 2*Power(s,3) - 106*s2 + 427*Power(s2,2) - 
                  12*Power(s2,3) + Power(s,2)*(-11 + 18*s2) + 
                  4*s*(-65 - 31*s2 + Power(s2,2)))*Power(t2,2) + 
               (1281 + 143*Power(s,2) + 588*s*(-1 + s2) + 818*s2 - 
                  231*Power(s2,2))*Power(t2,3) + 
               (-455 + 506*s + 296*s2)*Power(t2,4) + 57*Power(t2,5)) - 
            Power(t1,4)*(2 + (71 + 11*s - 51*s2)*t2 - 
               (-631 + 145*s2 + s*(42 - 17*s2)*s2 + 123*Power(s2,2) - 
                  27*Power(s2,3) + Power(s,2)*(2 + 7*s2))*Power(t2,2) + 
               (49 + 4*Power(s,3) - 2398*s2 + 738*Power(s2,2) + 
                  93*Power(s2,3) + Power(s,2)*(-41 + 69*s2) + 
                  s*(81 - 926*s2 - 87*Power(s2,2)))*Power(t2,3) + 
               (1593 + 187*Power(s,2) - 604*s2 - 169*Power(s2,2) + 
                  2*s*(-587 + 176*s2))*Power(t2,4) + 
               (-353 + 334*s + 38*s2)*Power(t2,5) + 15*Power(t2,6)) - 
            Power(t2,4)*(5 + (5 + 3*s)*t2 + (13 - 20*s)*Power(t2,2) + 
               (-27 + 11*s)*Power(t2,3) + 
               Power(s2,3)*(8 + 15*t2 - 7*Power(t2,2)) + 
               Power(s2,2)*(-27 + (-10 + 17*s)*t2 + 
                  (-3 + 17*s)*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-2 + (-35 + 2*s)*t2 + 2*(19 - 5*s)*s*Power(t2,2) + 
                  (25 - 4*s)*Power(t2,3))) - 
            t1*Power(t2,3)*(-3 + 2*(-57 + s)*t2 + 
               (337 + 118*s + 2*Power(s,2))*Power(t2,2) + 
               2*(181 - 73*s + 4*Power(s,2) + Power(s,3))*Power(t2,3) + 
               2*(-25 + 16*s + Power(s,2))*Power(t2,4) + 
               Power(s2,3)*(22 + 22*t2 + 45*Power(t2,2) - 
                  11*Power(t2,3)) + 
               Power(s2,2)*(-57 - (147 + 44*s)*t2 + 
                  (20 - 98*s)*Power(t2,2) + 
                  12*(-5 + 2*s)*Power(t2,3) + 2*Power(t2,4)) - 
               s2*(-79 + (-383 + 40*s)*t2 + 
                  (537 + 216*s - 37*Power(s,2))*Power(t2,2) + 
                  (443 - 52*s + 15*Power(s,2))*Power(t2,3) + 
                  2*(5 + 2*s)*Power(t2,4))) + 
            Power(t1,2)*Power(t2,2)*
             (25 + (257 - 10*s)*t2 + 
               (283 + 80*s + 6*Power(s,2))*Power(t2,2) + 
               (386 - 606*s + 35*Power(s,2) + 4*Power(s,3))*
                Power(t2,3) + 
               (-484 + 335*s - 17*Power(s,2))*Power(t2,4) + 
               (21 - 11*s)*Power(t2,5) + 
               Power(s2,3)*t2*(143 + 175*t2 - 60*Power(t2,2)) + 
               Power(s2,2)*(18 - (194 + 33*s)*t2 - 
                  6*(-61 + 19*s)*Power(t2,2) + 
                  (-131 + 119*s)*Power(t2,3) + 9*Power(t2,4)) + 
               s2*(-7 - (37 + 66*s)*t2 + 
                  (-1317 - 244*s + 51*Power(s,2))*Power(t2,2) + 
                  (35 + 594*s - 63*Power(s,2))*Power(t2,3) + 
                  (313 + 8*s)*Power(t2,4) + 11*Power(t2,5))) + 
            Power(t1,3)*t2*(-3 + (9 + 26*s)*t2 + 
               (253 + 26*s - 6*Power(s,2))*Power(t2,2) + 
               (432 + 794*s - 57*Power(s,2))*Power(t2,3) + 
               (1347 - 964*s + 103*Power(s,2))*Power(t2,4) + 
               2*(-71 + 49*s)*Power(t2,5) + 
               2*Power(s2,3)*t2*(10 - 7*t2 + 47*Power(t2,2)) + 
               s2*(-6 + 4*(40 + 7*s)*t2 + 
                  (1263 + 24*s - 31*Power(s,2))*Power(t2,2) + 
                  (-2223 - 1348*s + 99*Power(s,2))*Power(t2,3) + 
                  2*(-577 + 16*s)*Power(t2,4) - 54*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-233 - 832*t2 + 422*Power(t2,2) - 81*Power(t2,3) + 
                  s*(6 + 50*t2 - 186*Power(t2,2))))) - 
         Power(s1,4)*(12*Power(t1,10)*t2 - 
            2*Power(t1,9)*(-15 + 3*(26 + 5*s + 2*s2)*t2 + 
               13*Power(t2,2)) - 
            Power(t1,8)*(-30 + (361 + 60*s - 130*s2)*t2 + 
               (-385 + 22*s + 10*s2)*Power(t2,2) + 6*Power(t2,3)) + 
            Power(t1,7)*(-24 + (-342 - 22*s + 83*s2)*t2 + 
               (1174 + 80*Power(s,2) + 573*s2 - 140*Power(s2,2) + 
                  3*s*(1 + 80*s2))*Power(t2,2) + 
               (-402 + 337*s + 72*s2)*Power(t2,3) + 58*Power(t2,4)) - 
            Power(t1,6)*(18 + (29 - 24*s + 52*s2)*t2 + 
               (-1702 + 10*Power(s,3) + 135*s2 + 415*Power(s2,2) - 
                  8*Power(s2,3) + Power(s,2)*(-39 + 32*s2) - 
                  2*s*(206 + 57*s2 + 2*Power(s2,2)))*Power(t2,2) + 
               (2275 + 321*Power(s,2) + 1018*s2 - 239*Power(s2,2) + 
                  s*(-862 + 498*s2))*Power(t2,3) + 
               (-431 + 610*s + 43*s2)*Power(t2,4) + 53*Power(t2,5)) + 
            Power(t1,5)*(6 + (169 + 29*s - 83*s2)*t2 - 
               (-968 + 5*Power(s,3) + 311*s2 + 47*Power(s2,2) - 
                  21*Power(s2,3) + Power(s,2)*(21 + 11*s2) + 
                  s*(-149 + 182*s2 - 45*Power(s2,2)))*Power(t2,2) + 
               (-1254 + 33*Power(s,3) - 3424*s2 + 1048*Power(s2,2) + 
                  131*Power(s2,3) + Power(s,2)*(-252 + 125*s2) + 
                  s*(8 - 1344*s2 - 169*Power(s2,2)))*Power(t2,3) + 
               (2671 + 501*Power(s,2) - 64*s2 - 145*Power(s2,2) + 
                  2*s*(-1136 + 77*s2))*Power(t2,4) + 
               (-463 + 470*s - 92*s2)*Power(t2,5) + 15*Power(t2,6)) + 
            Power(t1,3)*t2*(-6 + 
               (-228 + 99*s2 - 84*Power(s2,2) + s*(13 + 24*s2))*t2 + 
               (-911 + 371*s2 + 296*Power(s2,2) - 337*Power(s2,3) - 
                  Power(s,2)*(79 + 29*s2) + 
                  s*(-123 + 436*s2 + 135*Power(s2,2)))*Power(t2,2) + 
               (-8*Power(s,3) - 3*Power(s,2)*(-54 + 85*s2) + 
                  s*(272 + 984*s2 + 371*Power(s2,2)) - 
                  3*(358 - 1625*s2 + 451*Power(s2,2) + 
                     139*Power(s2,3)))*Power(t2,3) + 
               (-1633 + 4*Power(s,3) - 821*s2 + 540*Power(s2,2) + 
                  156*Power(s2,3) + 2*Power(s,2)*(-305 + 82*s2) - 
                  6*s*(-419 + 235*s2 + 54*Power(s2,2)))*Power(t2,4) + 
               (1311 + 135*Power(s,2) - 289*s2 + 67*Power(s2,2) - 
                  s*(1289 + 202*s2))*Power(t2,5) + 
               (-63 + 23*s - 23*s2)*Power(t2,6)) + 
            Power(t2,4)*(Power(s2,3)*
                (-12 - 42*t2 - 35*Power(t2,2) + 7*Power(t2,3)) - 
               2*Power(s2,2)*
                (-18 + (-29 + s)*t2 - 8*(1 + 3*s)*Power(t2,2) + 
                  (-13 + 9*s)*Power(t2,3) + Power(t2,4)) + 
               s2*(-22 - (113 + 8*s)*t2 + 
                  (199 + 8*s + 11*Power(s,2))*Power(t2,2) + 
                  (141 - 20*s + 15*Power(s,2))*Power(t2,3) + 
                  (5 + 4*s)*Power(t2,4)) - 
               2*(-2 + (-25 + 3*s)*t2 + 
                  (67 + 20*s - 2*Power(s,2))*Power(t2,2) + 
                  2*(30 - 14*s + Power(s,3))*Power(t2,3) + 
                  (-12 + 8*s + Power(s,2))*Power(t2,4))) + 
            t1*Power(t2,3)*(3 + (157 + 9*s)*t2 + 
               (248 + 60*s - 37*Power(s,2))*Power(t2,2) + 
               (-120 - 541*s + 75*Power(s,2) + 13*Power(s,3))*
                Power(t2,3) - 
               (513 - 368*s + 50*Power(s,2) + 5*Power(s,3))*
                Power(t2,4) + (27 - 22*s)*Power(t2,5) + 
               Power(s2,3)*t2*
                (212 + 212*t2 - 93*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*(6 + (-329 + 8*s)*t2 + 
                  (113 + 7*s)*Power(t2,2) + 
                  2*(-17 + 97*s)*Power(t2,3) + (16 - 15*s)*Power(t2,4)) \
+ s2*(-26 + (-58 + 40*s)*t2 + 
                  (-983 + 16*s - 39*Power(s,2))*Power(t2,2) - 
                  6*(-97 - 99*s + 19*Power(s,2))*Power(t2,3) + 
                  (379 + 34*s + 15*Power(s,2))*Power(t2,4) + 
                  22*Power(t2,5))) - 
            Power(t1,4)*t2*(-49 + 132*t2 + 1382*Power(t2,2) - 
               935*Power(t2,3) + 2410*Power(t2,4) - 274*Power(t2,5) + 
               2*Power(s,3)*Power(t2,2)*(-7 + 17*t2) + 
               2*Power(s2,3)*t2*(11 + 4*t2 + 95*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-433 - 1576*t2 + 1018*Power(t2,2) + 9*Power(t2,3)) - 
               2*s2*(12 - 133*t2 - 891*Power(t2,2) + 
                  2323*Power(t2,3) + 465*Power(t2,4) + 51*Power(t2,5)) \
+ 2*s*(3 + (16 + 77*s2 + 12*Power(s2,2))*t2 + 
                  (385 - 81*s2 + 65*Power(s2,2))*Power(t2,2) + 
                  (907 - 1233*s2 - 196*Power(s2,2))*Power(t2,3) - 
                  (1243 + 135*s2)*Power(t2,4) + 84*Power(t2,5)) + 
               Power(s,2)*t2*
                (-25 + 12*t2 - 578*Power(t2,2) + 377*Power(t2,3) + 
                  6*s2*(-1 - 17*t2 + 33*Power(t2,2)))) + 
            Power(t1,2)*Power(t2,2)*
             (11 + (2 - 10*s)*t2 + 
               (-230 + 106*s + 87*Power(s,2))*Power(t2,2) + 
               (1625 + 810*s - 204*Power(s,2) - 10*Power(s,3))*
                Power(t2,3) + 
               (1771 - 1450*s + 297*Power(s,2) + 12*Power(s,3))*
                Power(t2,4) + 
               (-317 + 292*s - 18*Power(s,2))*Power(t2,5) + 
               2*Power(s2,3)*t2*
                (68 + 82*t2 + 107*Power(t2,2) - 25*Power(t2,3)) + 
               s2*(6 + (666 - 56*s)*t2 + 
                  3*(690 - 102*s + 17*Power(s,2))*Power(t2,2) + 
                  (-3657 - 1538*s + 263*Power(s,2))*Power(t2,3) + 
                  (-1423 + 136*s - 74*Power(s,2))*Power(t2,4) + 
                  4*(-32 + 9*s)*Power(t2,5)) - 
               Power(s2,2)*t2*
                (605 + 1140*t2 - 468*Power(t2,2) + 259*Power(t2,3) + 
                  18*Power(t2,4) + 
                  s*(6 + 166*t2 + 462*Power(t2,2) - 112*Power(t2,3))))) + 
         Power(s1,3)*(-2*Power(t1,11)*t2 + 
            2*Power(t1,10)*(-6 + (34 + 6*s + s2)*t2 + 2*Power(t2,2)) + 
            Power(t1,9)*(-12 + (161 + 24*s - 56*s2)*t2 + 
               2*(-91 + 8*s - 5*s2)*Power(t2,2) + 5*Power(t2,3)) + 
            Power(t1,8)*(16 + (179 + 13*s - 35*s2)*t2 - 
               (862 + 80*Power(s,2) + 238*s2 - 64*Power(s2,2) + 
                  s*(-11 + 108*s2))*Power(t2,2) + 
               (204 - 186*s + 35*s2)*Power(t2,3) - 19*Power(t2,4)) + 
            Power(t1,7)*(10 + (8 - 24*s + 46*s2)*t2 + 
               (20*Power(s,3) + Power(s,2)*(-57 + 28*s2) - 
                  2*s*(173 + 19*s2 + 3*Power(s2,2)) - 
                  2*(661 - 126*s2 - 102*Power(s2,2) + Power(s2,3)))*
                Power(t2,2) + 
               (1942 + 345*Power(s,2) + 639*s2 - 114*Power(s2,2) + 
                  6*s*(-111 + 29*s2))*Power(t2,3) + 
               (-237 + 356*s - 77*s2)*Power(t2,4) + 17*Power(t2,5)) - 
            Power(t1,6)*(6 + (189 + 25*s - 57*s2)*t2 + 
               (759 - 13*Power(s,3) - 337*s2 + 41*Power(s2,2) + 
                  6*Power(s2,3) + Power(s,2)*(-79 + 9*s2) + 
                  s*(369 - 242*s2 + 43*Power(s2,2)))*Power(t2,2) + 
               (-2419 + 85*Power(s,3) - 2341*s2 + 721*Power(s2,2) + 
                  90*Power(s2,3) + Power(s,2)*(-506 + 75*s2) - 
                  9*s*(17 + 100*s2 + 15*Power(s2,2)))*Power(t2,3) + 
               (2286 + 601*Power(s,2) + 466*s2 - 16*Power(s2,2) - 
                  2*s*(1043 + 95*s2))*Power(t2,4) + 
               (-302 + 291*s - 99*s2)*Power(t2,5) + 5*Power(t2,6)) + 
            Power(t1,4)*t2*(36 + 
               (486 + 13*s + 18*Power(s,2) - 189*s2 - 108*s*s2 + 
                  150*Power(s2,2))*t2 - 
               (-1373 + 9*Power(s,3) + Power(s,2)*(58 - 81*s2) + 
                  719*s2 + 22*Power(s2,2) - 341*Power(s2,3) + 
                  s*(-419 + 920*s2 + 137*Power(s2,2)))*Power(t2,2) + 
               (255 + 95*Power(s,3) - 6839*s2 + 2147*Power(s2,2) + 
                  499*Power(s2,3) + Power(s,2)*(-181 + 435*s2) - 
                  s*(1415 + 2046*s2 + 571*Power(s2,2)))*Power(t2,3) + 
               (3752 - 86*Power(s,3) + 2171*s2 - 608*Power(s2,2) - 
                  146*Power(s2,3) - 4*Power(s,2)*(-454 + 7*s2) + 
                  s*(-4909 + 852*s2 + 260*Power(s2,2)))*Power(t2,4) - 
               (1369 + 243*Power(s,2) + 327*s2 + 155*Power(s2,2) - 
                  s*(1905 + 398*s2))*Power(t2,5) + 
               (55 - 13*s + 13*s2)*Power(t2,6)) + 
            Power(t2,5)*(47 + 78*t2 - 150*Power(t2,2) - 
               178*Power(t2,3) + 11*Power(t2,4) - 
               Power(s,3)*Power(t2,2)*(5 + 3*t2) + 
               Power(s2,3)*(64 + 30*t2 - 49*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,2)*(-216 - 67*t2 + 20*Power(t2,2) + 
                  7*Power(t2,3)) + 
               s*(-6 + (-17 + 10*s2 + 124*Power(s2,2))*t2 + 
                  (-165 + 196*s2 + 117*Power(s2,2))*Power(t2,2) + 
                  (135 + 26*s2 - 9*Power(s2,2))*Power(t2,3) - 
                  11*Power(t2,4)) + 
               s2*(-4 - 249*t2 + 303*Power(t2,2) + 147*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(s,2)*t2*
                (-7 + 64*t2 - 33*Power(t2,2) + 
                  s2*(6 - 63*t2 + 9*Power(t2,2)))) - 
            t1*Power(t2,4)*(57 + 317*t2 - 1467*Power(t2,2) + 
               2*Power(s,3)*(-21 + t2)*Power(t2,2) - 855*Power(t2,3) + 
               300*Power(t2,4) + 
               2*Power(s2,3)*
                (-112 - 211*t2 - 94*Power(t2,2) + 41*Power(t2,3)) + 
               Power(s2,2)*(600 + 865*t2 - 109*Power(t2,2) + 
                  250*Power(t2,3) + 34*Power(t2,4)) + 
               2*s2*(-342 - 614*t2 + 1181*Power(t2,2) + 
                  364*Power(t2,3) + 47*Power(t2,4)) - 
               2*s*(9 + (111 - 129*s2 - 154*Power(s2,2))*t2 + 
                  (58 - 197*s2 - 203*Power(s2,2))*Power(t2,2) + 
                  (-411 + 96*s2 + 81*Power(s2,2))*Power(t2,3) + 
                  (145 + 34*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (-3 + 131*t2 - 226*Power(t2,2) + 34*Power(t2,3) + 
                  6*s2*(3 - 24*t2 + 13*Power(t2,2)))) + 
            Power(t1,5)*t2*(-73 + 252*t2 + 2142*Power(t2,2) - 
               3287*Power(t2,3) + 1990*Power(t2,4) - 210*Power(t2,5) + 
               6*Power(s,3)*t2*(-1 - 9*t2 + 22*Power(t2,2)) + 
               Power(s2,3)*t2*(8 + 11*t2 + 161*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-359 - 1448*t2 + 960*Power(t2,2) + 143*Power(t2,3)) - 
               2*s2*(15 - 74*t2 - 455*Power(t2,2) + 2318*Power(t2,3) - 
                  79*Power(t2,4) + 31*Power(t2,5)) + 
               2*s*(6 + (29 + 87*s2 + 15*Power(s2,2))*t2 + 
                  (931 - 114*s2 + 63*Power(s2,2))*Power(t2,2) + 
                  (1116 - 928*s2 - 157*Power(s2,2))*Power(t2,3) - 
                  281*(5 + s2)*Power(t2,4) + 53*Power(t2,5)) + 
               Power(s,2)*t2*
                (-1 - 140*t2 - 1416*Power(t2,2) + 533*Power(t2,3) + 
                  2*s2*(-6 - 55*t2 + 33*Power(t2,2)))) + 
            Power(t1,2)*Power(t2,3)*
             (-203 - 790*t2 - 1680*Power(t2,2) + 411*Power(t2,3) + 
               1895*Power(t2,4) - 113*Power(t2,5) + 
               3*Power(s,3)*Power(t2,2)*(-30 + 13*t2 + Power(t2,2)) - 
               Power(s2,3)*t2*
                (742 + 628*t2 - 341*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*(-168 + 747*t2 - 870*Power(t2,2) + 
                  497*Power(t2,3) + 114*Power(t2,4)) + 
               s2*(196 + 844*t2 + 4881*Power(t2,2) - 
                  2795*Power(t2,3) - 752*Power(t2,4) - 78*Power(t2,5)) \
+ s*(-18 + 2*(-122 + 157*s2 + 98*Power(s2,2))*t2 + 
                  (779 - 324*s2 + 280*Power(s2,2))*Power(t2,2) + 
                  (2265 - 2206*s2 - 697*Power(s2,2))*Power(t2,3) + 
                  (-1840 - 456*s2 + 9*Power(s2,2))*Power(t2,4) + 
                  78*Power(t2,5)) + 
               Power(s,2)*t2*
                (33 + 10*t2 - 577*Power(t2,2) + 342*Power(t2,3) + 
                  s2*(18 - 42*t2 + 317*Power(t2,2) - 9*Power(t2,3)))) + 
            Power(t1,3)*Power(t2,2)*
             (-15 - 230*t2 - 519*Power(t2,2) - 1868*Power(t2,3) - 
               3653*Power(t2,4) + 573*Power(t2,5) + 
               4*Power(s,3)*Power(t2,2)*(17 - 22*t2 + 4*Power(t2,2)) + 
               2*Power(s2,3)*t2*
                (-101 - 164*t2 - 242*Power(t2,2) + 25*Power(t2,3)) + 
               Power(s2,2)*t2*
                (1271 + 2502*t2 - 1674*Power(t2,2) + 163*Power(t2,3) + 
                  46*Power(t2,4)) + 
               4*s2*(3 - 374*t2 - 946*Power(t2,2) + 2130*Power(t2,3) + 
                  253*Power(t2,4) + 86*Power(t2,5)) - 
               2*s*(-3 + (-7 - 21*s2 + 6*Power(s2,2))*t2 + 
                  (591 - 634*s2 - 58*Power(s2,2))*Power(t2,2) - 
                  6*(-136 + 335*s2 + 86*Power(s2,2))*Power(t2,3) + 
                  (-2209 - 265*s2 + 42*Power(s2,2))*Power(t2,4) + 
                  (314 + 46*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(-47 + 116*t2 + 626*Power(t2,2) - 
                  1157*Power(t2,3) + 46*Power(t2,4) + 
                  6*s2*(-1 - 18*t2 - 94*Power(t2,2) + 3*Power(t2,3))))) + 
         Power(t2,2)*(2*Power(-1 + s,3)*Power(t1,10) - 
            Power(t1,5)*(6 - 
               2*(-28 + 39*Power(s,2) + 6*s*(-9 + s2) + 6*s2)*t2 + 
               (133 + 126*Power(s,3) + 92*s2 + 8*Power(s2,2) + 
                  Power(s,2)*(-399 + 40*s2) + 
                  s*(163 - 254*s2 + 22*Power(s2,2)))*Power(t2,2) + 
               (-224 - 32*Power(s,3) + 38*s2 + 193*Power(s2,2) + 
                  20*Power(s2,3) + Power(s,2)*(463 + 261*s2) - 
                  s*(777 + 788*s2 + 101*Power(s2,2)))*Power(t2,3) + 
               (473 - 136*Power(s,3) + 279*s2 + 69*Power(s2,2) + 
                  6*Power(s2,3) + Power(s,2)*(586 + 223*s2) - 
                  s*(628 + 400*s2 + 93*Power(s2,2)))*Power(t2,4) + 
               (-34 + 2*Power(s,3) - 33*s2 - 10*Power(s2,2) - 
                  2*Power(s2,3) - 6*Power(s,2)*(4 + s2) + 
                  s*(72 + 34*s2 + 6*Power(s2,2)))*Power(t2,5)) + 
            Power(t1,3)*t2*(48 + 
               (326 - 48*Power(s,2) + 52*s2 + 96*Power(s2,2) - 
                  12*s*(5 + 16*s2))*t2 + 
               (415 + 84*Power(s,3) - 6*s2 + 162*Power(s2,2) + 
                  132*Power(s2,3) + Power(s,2)*(-62 + 348*s2) - 
                  4*s*(118 + 271*s2 + 33*Power(s2,2)))*Power(t2,2) + 
               (238 - 302*Power(s,3) - 389*s2 + 157*Power(s2,2) + 
                  88*Power(s2,3) + Power(s,2)*(1865 + 412*s2) - 
                  s*(2441 + 950*s2 + 54*Power(s2,2)))*Power(t2,3) + 
               (702 - 141*Power(s,3) + 607*s2 - 242*Power(s2,2) - 
                  109*Power(s2,3) + 3*Power(s,2)*(222 + 7*s2) + 
                  s*(-665 + 272*s2 + 229*Power(s2,2)))*Power(t2,4) + 
               (-180 + 15*Power(s,3) - 185*s2 - 109*Power(s2,2) - 
                  15*Power(s2,3) - 5*Power(s,2)*(41 + 9*s2) + 
                  s*(455 + 314*s2 + 45*Power(s2,2)))*Power(t2,5) + 
               (3 - s + s2)*Power(t2,6)) - 
            2*Power(t1,9)*(Power(s,3)*(1 + 5*t2) - 
               3*(1 + (3 + s2)*t2) - Power(s,2)*(6 + (16 + 3*s2)*t2) + 
               s*(9 + (13 + 6*s2)*t2)) + 
            2*Power(t1,8)*(-5 + (9 - 15*s2)*t2 - 
               (25 + 10*s2 + 3*Power(s2,2))*Power(t2,2) + 
               2*Power(s,3)*(-1 - t2 + 5*Power(t2,2)) - 
               Power(s,2)*(-3 + (6 + 7*s2)*t2 + 
                  12*(3 + s2)*Power(t2,2)) + 
               s*(3 + (19 + 17*s2)*t2 + 
                  (30 + 23*s2 + 3*Power(s2,2))*Power(t2,2))) - 
            16*Power(t2,4)*(1 + (-6 + s - 3*Power(s,2))*t2 + 
               (-19 + 29*s - 11*Power(s,2))*Power(t2,2) + 
               (18 - 4*s + Power(s,2))*Power(t2,3) + 
               (6 - 8*s + 3*Power(s,2))*Power(t2,4) + 
               2*Power(s2,3)*(-6 - 9*t2 + 2*Power(t2,3)) + 
               Power(s2,2)*(36 + (49 - 2*s)*t2 + 
                  (5 - 6*s)*Power(t2,2) + (9 - 8*s)*Power(t2,3) + 
                  3*Power(t2,4)) - 
               s2*(22 + (5 - 4*s)*t2 - 
                  3*(9 - 10*s + 2*Power(s,2))*Power(t2,2) + 
                  (22 + 20*s - 4*Power(s,2))*Power(t2,3) + 
                  6*s*Power(t2,4))) - 
            4*t1*Power(t2,3)*
             (48 + (56 - 51*s + 36*Power(s,2))*t2 + 
               (138 - 285*s + 142*Power(s,2) - 7*Power(s,3))*
                Power(t2,2) + 
               (-168 + 153*s - 90*Power(s,2) + 11*Power(s,3))*
                Power(t2,3) + 
               (-98 + 131*s - 64*Power(s,2) + 4*Power(s,3))*
                Power(t2,4) - 4*s*Power(t2,5) + 
               Power(s2,3)*t2*
                (48 + 17*t2 - 29*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,2)*(24 + 8*(-13 + 4*s)*t2 + 
                  (-86 + 67*s)*Power(t2,2) + 
                  (-114 + 97*s)*Power(t2,3) + 12*(-2 + s)*Power(t2,4)) \
+ s2*(-104 + (-231 + 16*s)*t2 + 
                  (-277 + 408*s - 93*Power(s,2))*Power(t2,2) + 
                  (209 + 328*s - 79*Power(s,2))*Power(t2,3) + 
                  (-17 + 88*s - 12*Power(s,2))*Power(t2,4) + 
                  4*Power(t2,5))) + 
            Power(t1,7)*(Power(s,3)*t2*(29 + 51*t2 - 20*Power(t2,2)) + 
               Power(s,2)*(-12 + (-127 + 4*s2)*t2 + 
                  (-125 + 8*s2)*Power(t2,2) + 4*(22 + 9*s2)*Power(t2,3)\
) - s*(-18 + 2*(-67 + 12*s2)*t2 + 
                  (-55 + 54*s2 + 16*Power(s2,2))*Power(t2,2) + 
                  (101 + 74*s2 + 18*Power(s2,2))*Power(t2,3)) + 
               t2*(-16 - 141*t2 + 65*Power(t2,2) + 
                  2*Power(s2,3)*Power(t2,2) + 
                  14*Power(s2,2)*t2*(2 + t2) + 
                  2*s2*(7 - 9*t2 + 20*Power(t2,2)))) - 
            Power(t1,4)*t2*(70 + 43*t2 - 65*Power(t2,2) + 
               477*Power(t2,3) - 365*Power(t2,4) + 16*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(14 - 79*t2 - 25*Power(t2,2)) + 
               2*Power(s,3)*t2*
                (14 - 133*t2 - 43*Power(t2,2) + 36*Power(t2,3)) + 
               Power(s2,2)*t2*
                (134 + 109*t2 - 383*Power(t2,2) - 130*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s2*(12 - 64*t2 - 273*Power(t2,2) + 457*Power(t2,3) - 
                  283*Power(t2,4) + 9*Power(t2,5)) - 
               2*s*(42 + (272 + 94*s2 + 6*Power(s2,2))*t2 + 
                  (710 - 111*s2 - 2*Power(s2,2))*Power(t2,2) - 
                  (92 + 475*s2 + 161*Power(s2,2))*Power(t2,3) - 
                  (351 + 252*s2 + 61*Power(s2,2))*Power(t2,4) + 
                  2*(5 + 2*s2)*Power(t2,5)) + 
               Power(s,2)*t2*
                (194 + 1311*t2 + 117*Power(t2,2) - 464*Power(t2,3) + 
                  4*Power(t2,4) + 
                  s2*(84 + 76*t2 - 245*Power(t2,2) - 169*Power(t2,3)))) \
+ Power(t1,6)*(12 + 2*(31 + 5*s2)*t2 + 
               (-43 + 126*s2 + 24*Power(s2,2))*Power(t2,2) + 
               (359 + 173*s2 - 20*Power(s2,2) - 4*Power(s2,3))*
                Power(t2,3) - 
               (52 + 51*s2 + 14*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,4) + 
               2*Power(s,3)*t2*
                (13 - 33*t2 - 62*Power(t2,2) + 5*Power(t2,3)) + 
               2*s*(-6 - (61 + 5*s2)*t2 + 
                  (-284 - 97*s2 + 8*Power(s2,2))*Power(t2,2) - 
                  (162 + 55*s2)*Power(t2,3) + 
                  (57 + 33*s2 + 9*Power(s2,2))*Power(t2,4)) + 
               Power(s,2)*t2*
                (-13 + 429*t2 + 412*Power(t2,2) - 62*Power(t2,3) + 
                  3*s2*(4 + 25*t2 + 35*Power(t2,2) - 8*Power(t2,3)))) + 
            2*Power(t1,2)*Power(t2,2)*
             (-16 + (-109 - 106*s + 72*Power(s,2))*t2 + 
               (2 - 314*s + 285*Power(s,2) - 42*Power(s,3))*
                Power(t2,2) + 
               (-314 + 918*s - 636*Power(s,2) + 90*Power(s,3))*
                Power(t2,3) + 
               (-338 + 448*s - 301*Power(s,2) + 40*Power(s,3))*
                Power(t2,4) + (23 - 74*s + 20*Power(s,2))*Power(t2,5) + 
               2*Power(s2,3)*t2*
                (-24 - 49*t2 - 18*Power(t2,2) + 7*Power(t2,3)) - 
               2*s2*(24 + (225 - 80*s)*t2 + 
                  (284 - 509*s + 135*Power(s,2))*Power(t2,2) + 
                  2*(-101 - 240*s + 70*Power(s,2))*Power(t2,3) + 
                  (83 - 91*s + 33*Power(s,2))*Power(t2,4) + 
                  2*(-9 + 10*s)*Power(t2,5)) + 
               Power(s2,2)*t2*
                (112 + 73*t2 - 260*Power(t2,2) - Power(t2,3) + 
                  20*Power(t2,4) + 
                  2*s*(24 + 73*t2 + 85*Power(t2,2) + 6*Power(t2,3))))) + 
         Power(s1,2)*(2*Power(t1,11)*(-1 + (6 + s)*t2) + 
            2*Power(t1,10)*(-1 + (15 + 2*s - 5*s2)*t2 + 
               2*(-9 + s - s2)*Power(t2,2)) + 
            Power(t1,9)*(4 + 3*(15 + s - 2*s2)*t2 - 
               4*(70 + 10*Power(s,2) + 10*s2 - 3*Power(s2,2) + 
                  s*(-4 + 5*s2))*Power(t2,2) + 
               (47 - 41*s + 18*s2)*Power(t2,3)) + 
            Power(t1,8)*(2 + (7 - 8*s + 14*s2)*t2 + 
               (-487 + 20*Power(s,3) + 129*s2 + 40*Power(s2,2) + 
                  Power(s,2)*(-47 + 12*s2) - 
                  2*s*(63 - s2 + Power(s2,2)))*Power(t2,2) + 
               (752 + 179*Power(s,2) + 174*s2 - 20*Power(s2,2) - 
                  2*s*(132 + s2))*Power(t2,3) + 
               (-58 + 80*s - 31*s2)*Power(t2,4)) + 
            Power(t1,7)*(-2 + (-85 - 7*s + 14*s2)*t2 + 
               (-270 + 9*Power(s,3) - 23*Power(s,2)*(-5 + s2) + 
                  147*s2 - 28*Power(s2,2) + 
                  s*(-303 + 116*s2 - 14*Power(s2,2)))*Power(t2,2) + 
               (1596 - 97*Power(s,3) + 728*s2 - 214*Power(s2,2) - 
                  24*Power(s2,3) + Power(s,2)*(452 + 15*s2) + 
                  s*(13 + 234*s2 + 36*Power(s2,2)))*Power(t2,3) - 
               (943 + 327*Power(s,2) + 320*s2 + 22*Power(s2,2) - 
                  s*(887 + 186*s2))*Power(t2,4) + 
               (72 - 64*s + 25*s2)*Power(t2,5)) + 
            Power(t1,3)*Power(t2,2)*
             (-42 - (633 + 6*Power(s,2) + s*(56 - 180*s2) - 470*s2 + 
                  402*Power(s2,2))*t2 + 
               (-1254 - 54*Power(s,3) + Power(s,2)*(13 - 68*s2) + 
                  1626*s2 + 139*Power(s2,2) - 816*Power(s2,3) + 
                  2*s*(-334 + 633*s2 + 193*Power(s2,2)))*Power(t2,2) - 
               (2432 + 88*Power(s,3) - 6915*s2 + 1702*Power(s2,2) + 
                  688*Power(s2,3) + Power(s,2)*(254 + 490*s2) - 
                  s*(3879 + 188*s2 + 634*Power(s2,2)))*Power(t2,3) + 
               (-1123 + 217*Power(s,3) - 4323*s2 + 1135*Power(s2,2) + 
                  413*Power(s2,3) + Power(s,2)*(-2239 + 169*s2) + 
                  s*(3807 - 2038*s2 - 799*Power(s2,2)))*Power(t2,4) + 
               (1969 - 11*Power(s,3) + 434*s2 + 414*Power(s2,2) + 
                  11*Power(s2,3) + Power(s,2)*(718 + 33*s2) - 
                  s*(2912 + 1132*s2 + 33*Power(s2,2)))*Power(t2,5) + 
               (-65 + 42*s - 42*s2)*Power(t2,6)) + 
            Power(t1,5)*t2*(34 + 
               (460 - 12*Power(s,2) + s*(143 - 72*s2) - 185*s2 + 
                  84*Power(s2,2))*t2 + 
               (851 + 7*Power(s,3) - 680*s2 + 189*Power(s2,2) + 
                  126*Power(s2,3) + Power(s,2)*(-448 + 75*s2) + 
                  s*(951 - 868*s2 + 19*Power(s2,2)))*Power(t2,2) + 
               (-1864 + 221*Power(s,3) - 3429*s2 + 1404*Power(s2,2) + 
                  279*Power(s2,3) + Power(s,2)*(-133 + 289*s2) - 
                  s*(2313 + 1760*s2 + 392*Power(s2,2)))*Power(t2,3) - 
               (-3804 + 164*Power(s,3) - 1856*s2 + 82*Power(s2,2) + 
                  27*Power(s2,3) - 2*Power(s,2)*(1031 + 120*s2) + 
                  s*(3919 + 570*s2 + 49*Power(s2,2)))*Power(t2,4) - 
               (610 + 149*Power(s,2) + 363*s2 + 93*Power(s2,2) - 
                  s*(1017 + 242*s2))*Power(t2,5) + 
               (13 - s + s2)*Power(t2,6)) + 
            2*Power(t2,5)*(13 + (40 - 34*s + 3*Power(s,2))*t2 - 
               2*(106 - 29*s + 7*Power(s,2) + 3*Power(s,3))*
                Power(t2,2) - 
               (52 - 64*s + 13*Power(s,2) + 2*Power(s,3))*Power(t2,3) + 
               (51 - 48*s + 8*Power(s,2))*Power(t2,4) + 
               8*Power(s2,3)*
                (-6 - 12*t2 - 3*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(144 + (223 - 8*s)*t2 + 
                  2*(21 + s)*Power(t2,2) + (39 - 34*s)*Power(t2,3) + 
                  8*Power(t2,4)) - 
               2*s2*(44 + (46 + 13*s)*t2 - 
                  2*(65 - 27*s + 13*Power(s,2))*Power(t2,2) + 
                  (-31 + 25*s - 10*Power(s,2))*Power(t2,3) + 
                  (-5 + 8*s)*Power(t2,4))) + 
            t1*Power(t2,4)*(42 + (461 + 72*s - 18*Power(s,2))*t2 + 
               (906 - 511*s + 91*Power(s,2) + 14*Power(s,3))*
                Power(t2,2) + 
               (-1310 - 995*s + 254*Power(s,2) + 17*Power(s,3))*
                Power(t2,3) - 
               (1204 - 1153*s + 303*Power(s,2) + 7*Power(s,3))*
                Power(t2,4) + (97 - 87*s)*Power(t2,5) + 
               Power(s2,3)*t2*
                (560 + 344*t2 - 239*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(48 + 2*(-569 + 32*s)*t2 + 
                  (71 + 342*s)*Power(t2,2) + 
                  (-118 + 551*s)*Power(t2,3) - (47 + 21*s)*Power(t2,4)) \
+ s2*(-208 + (-618 + 284*s)*t2 + 
                  (-2419 + 654*s - 300*Power(s,2))*Power(t2,2) + 
                  (2421 + 1600*s - 329*Power(s,2))*Power(t2,3) + 
                  (785 + 350*s + 21*Power(s,2))*Power(t2,4) + 
                  87*Power(t2,5))) + 
            Power(t1,6)*t2*(-31 + 148*t2 + 1607*Power(t2,2) - 
               3139*Power(t2,3) + 811*Power(t2,4) - 50*Power(t2,5) + 
               2*Power(s2,3)*Power(t2,2)*(1 + 22*t2) + 
               2*Power(s,3)*t2*(-8 - 35*t2 + 91*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-108 - 618*t2 + 325*Power(t2,2) + 93*Power(t2,3)) - 
               s2*(12 + 5*t2 + 150*Power(t2,2) + 2035*Power(t2,3) - 
                  405*Power(t2,4) + 9*Power(t2,5)) + 
               2*s*(3 + (49 + 11*s2 + 6*Power(s2,2))*t2 + 
                  2*(421 + 9*s2 + 8*Power(s2,2))*Power(t2,2) + 
                  (739 - 184*s2 - 21*Power(s2,2))*Power(t2,3) - 
                  (671 + 173*s2)*Power(t2,4) + 10*Power(t2,5)) + 
               Power(s,2)*t2*
                (67 - 348*t2 - 1390*Power(t2,2) + 307*Power(t2,3) - 
                  2*s2*(3 + 26*t2 + 75*Power(t2,2)))) - 
            Power(t1,2)*Power(t2,3)*
             (-70 + (101 - 40*s - 18*Power(s,2))*t2 - 
               (41 + 1086*s - 93*Power(s,2) + 30*Power(s,3))*
                Power(t2,2) + 
               (-3319 + 180*s + 359*Power(s,2) - 14*Power(s,3))*
                Power(t2,3) + 
               (-1895 + 3382*s - 1300*Power(s,2) + 44*Power(s,3))*
                Power(t2,4) + 
               2*(324 - 440*s + 67*Power(s,2))*Power(t2,5) + 
               2*Power(s2,3)*t2*
                (-260 - 486*t2 - 209*Power(t2,2) + 73*Power(t2,3)) + 
               2*s2*(-24 + (-1177 + 206*s)*t2 + 
                  (-1607 + 673*s - 138*Power(s,2))*Power(t2,2) + 
                  (3602 + 1329*s - 345*Power(s,2))*Power(t2,3) + 
                  (13 + 58*s + 29*Power(s,2))*Power(t2,4) + 
                  (191 - 134*s)*Power(t2,5)) + 
               Power(s2,2)*t2*
                (1750 + 2613*t2 - 1313*Power(t2,2) + 452*Power(t2,3) + 
                  134*Power(t2,4) + 
                  s*(48 + 726*t2 + 994*Power(t2,2) - 248*Power(t2,3)))) \
+ Power(t1,4)*Power(t2,2)*(87 - 560*t2 - 785*Power(t2,2) + 
               1116*Power(t2,3) - 3265*Power(t2,4) + 307*Power(t2,5) - 
               Power(s2,3)*t2*
                (104 + 165*t2 + 441*Power(t2,2) + 10*Power(t2,3)) + 
               2*Power(s,3)*t2*
                (11 + 35*t2 - 163*Power(t2,2) + 35*Power(t2,3)) + 
               Power(s2,2)*t2*
                (1011 + 1944*t2 - 1752*Power(t2,2) - 331*Power(t2,3) + 
                  30*Power(t2,4)) + 
               s2*(6 - 1004*t2 - 2269*Power(t2,2) + 6920*Power(t2,3) - 
                  793*Power(t2,4) + 196*Power(t2,5)) - 
               2*s*(-6 + (86 + 143*s2 + 3*Power(s2,2))*t2 + 
                  (1937 - 908*s2 + 77*Power(s2,2))*Power(t2,2) + 
                  (271 - 1722*s2 - 473*Power(s2,2))*Power(t2,3) - 
                  (2335 + 783*s2 + 45*Power(s2,2))*Power(t2,4) + 
                  30*(6 + s2)*Power(t2,5)) + 
               Power(s,2)*t2*(29 + 766*t2 + 1592*Power(t2,2) - 
                  1661*Power(t2,3) + 30*Power(t2,4) - 
                  2*s2*(6 - 10*t2 + 173*Power(t2,2) + 75*Power(t2,3))))) \
- s1*t2*(Power(t1,10)*(-4 + (26 - 6*s + 8*Power(s,2))*t2) - 
            2*Power(t1,9)*(2 + 
               (-30 + 5*Power(s,3) + Power(s,2)*(-12 + s2) + 8*s2 + 
                  s*s2)*t2 + (43 + 18*Power(s,2) + 6*s2 - s*(23 + 5*s2))*
                Power(t2,2)) + 
            Power(t1,8)*(8 + (21 + Power(s,3) + s*(107 - 12*s2) - 
                  16*s2 + Power(s,2)*(-67 + 10*s2))*t2 + 
               (-353 + 51*Power(s,3) - 92*s2 + 16*Power(s2,2) - 
                  7*Power(s,2)*(27 + 4*s2) + 
                  s*(101 + 10*s2 + 2*Power(s2,2)))*Power(t2,2) + 
               2*(58 + 32*Power(s,2) + 26*s2 + Power(s2,2) - 
                  s*(68 + 19*s2))*Power(t2,3)) + 
            Power(t1,7)*(4 + (25 - 49*Power(s,2) + 14*Power(s,3) + 
                  26*s*(-2 + s2) + 12*s2)*t2 + 
               (-401 + 34*Power(s,3) + 187*s2 + 88*Power(s2,2) + 
                  Power(s,2)*(202 + 27*s2) + 
                  4*s*(-136 - 33*s2 + 2*Power(s2,2)))*Power(t2,2) + 
               (913 - 104*Power(s,3) + 337*s2 - 4*Power(s2,2) + 
                  4*Power(s2,3) + 3*Power(s,2)*(189 + 43*s2) - 
                  2*s*(277 + 74*s2 + 21*Power(s2,2)))*Power(t2,3) - 
               (105 + 56*Power(s,2) + 82*s2 + 8*Power(s2,2) - 
                  2*s*(97 + 27*s2))*Power(t2,4)) + 
            Power(t1,6)*(-4 + 
               (-113 + 42*Power(s,2) + 56*s2 - s*(103 + 12*s2))*t2 - 
               (214 + 45*Power(s,3) - 116*s2 + 24*Power(s2,2) + 
                  Power(s,2)*(-502 + 27*s2) + 
                  s*(543 - 214*s2 + 54*Power(s2,2)))*Power(t2,2) + 
               (1134 - 185*Power(s,3) + Power(s,2)*(149 - 66*s2) + 
                  560*s2 - 331*Power(s2,2) - 52*Power(s2,3) + 
                  s*(693 + 662*s2 + 107*Power(s2,2)))*Power(t2,3) + 
               (-1186 + 106*Power(s,3) - 552*s2 - 91*Power(s2,2) - 
                  18*Power(s2,3) - Power(s,2)*(877 + 199*s2) + 
                  s*(1317 + 550*s2 + 111*Power(s2,2)))*Power(t2,4) + 
               (87 + 24*Power(s,2) + 58*s2 + 10*Power(s2,2) - 
                  34*s*(4 + s2))*Power(t2,5)) + 
            Power(t1,4)*t2*(64 + 
               (569 - 24*Power(s,2) - 136*s2 + 192*Power(s2,2) - 
                  2*s*(-65 + 96*s2))*t2 + 
               (749 + 36*Power(s,3) - 522*s2 + 346*Power(s2,2) + 
                  262*Power(s2,3) + 5*Power(s,2)*(-127 + 54*s2) - 
                  6*s*(-62 + 237*s2 + 24*Power(s2,2)))*Power(t2,2) + 
               (-221 + 94*Power(s,3) - 2032*s2 + 1100*Power(s2,2) + 
                  309*Power(s2,3) + 2*Power(s,2)*(929 + 223*s2) - 
                  s*(4444 + 1990*s2 + 401*Power(s2,2)))*Power(t2,3) + 
               (2690 - 349*Power(s,3) + 1925*s2 - 248*Power(s2,2) - 
                  104*Power(s2,3) + 3*Power(s,2)*(721 + 137*s2) + 
                  3*s*(-945 - 68*s2 + 14*Power(s2,2)))*Power(t2,4) + 
               (-546 + 11*Power(s,3) - 449*s2 - 202*Power(s2,2) - 
                  11*Power(s2,3) - 3*Power(s,2)*(118 + 11*s2) + 
                  s*(1181 + 556*s2 + 33*Power(s2,2)))*Power(t2,5) + 
               (11 - 2*s + 2*s2)*Power(t2,6)) - 
            4*Power(t2,5)*(-36 - 34*t2 + 120*Power(t2,2) + 
               70*Power(t2,3) - 8*Power(t2,4) + 
               Power(s,3)*t2*(3 + 5*t2) - 
               2*Power(s2,2)*(-54 - 3*t2 + Power(t2,2)) + 
               Power(s2,3)*(-32 - 5*t2 + 21*Power(t2,2)) - 
               s2*(13 - 109*t2 + 177*Power(t2,2) + 71*Power(t2,3) + 
                  8*Power(t2,4)) + 
               s*(15 + (29 + 40*s2 - 47*Power(s2,2))*t2 + 
                  (55 - 104*s2 - 49*Power(s2,2))*Power(t2,2) - 
                  (67 + 24*s2)*Power(t2,3) + 8*Power(t2,4)) + 
               Power(s,2)*t2*(-14 - 34*t2 + 24*Power(t2,2) + 
                  s2*(-15 + 23*t2))) - 
            4*t1*Power(t2,4)*(60 - 35*t2 - 447*Power(t2,2) + 
               37*Power(t2,3) + 97*Power(t2,4) + 
               3*Power(s,3)*t2*(-3 - 10*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(-136 - 245*t2 - 46*Power(t2,2) + 
                  35*Power(t2,3)) + 
               Power(s2,2)*(372 + 496*t2 - 109*Power(t2,2) + 
                  103*Power(t2,3) + 34*Power(t2,4)) + 
               s2*(-341 - 254*t2 + 712*Power(t2,2) - 176*Power(t2,3) + 
                  35*Power(t2,4)) + 
               s*(-45 + (-146 + 32*s2 + 105*Power(s2,2))*t2 + 
                  2*(130 + 9*s2 + 51*Power(s2,2))*Power(t2,2) - 
                  55*(-4 + 2*s2 + Power(s2,2))*Power(t2,3) - 
                  (137 + 68*s2)*Power(t2,4)) + 
               Power(s,2)*t2*(36 + 19*t2 - 129*Power(t2,2) + 
                  34*Power(t2,3) + s2*(45 - 42*t2 + 5*Power(t2,2)))) + 
            Power(t1,5)*t2*(-71 + 113*t2 + 715*Power(t2,2) - 
               2089*Power(t2,3) + 945*Power(t2,4) - 49*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(-18 + 119*t2 + 25*Power(t2,2)) + 
               2*Power(s,3)*t2*
                (-16 + 16*t2 + 184*Power(t2,2) - 27*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-272 - 619*t2 + 498*Power(t2,2) + 211*Power(t2,3) - 
                  4*Power(t2,4)) + 
               s2*(-36 + 69*t2 + 125*Power(t2,2) - 1903*Power(t2,3) + 
                  611*Power(t2,4) - 18*Power(t2,5)) + 
               2*s*(27 + 3*(79 + 31*s2 + 6*Power(s2,2))*t2 + 
                  2*(697 - 11*s2 + 25*Power(s2,2))*Power(t2,2) + 
                  (349 - 434*s2 - 119*Power(s2,2))*Power(t2,3) - 
                  (844 + 413*s2 + 52*Power(s2,2))*Power(t2,4) + 
                  4*(5 + s2)*Power(t2,5)) - 
               Power(s,2)*t2*(-17 + 1536*t2 + 1312*Power(t2,2) - 
                  759*Power(t2,3) + 4*Power(t2,4) + 
                  s2*(54 + 88*t2 + 145*Power(t2,2) - 133*Power(t2,3)))) + 
            Power(t1,2)*Power(t2,3)*
             (-448 - 733*t2 - 2106*Power(t2,2) + 1178*Power(t2,3) + 
               1398*Power(t2,4) - 25*Power(t2,5) - 
               Power(s,3)*t2*
                (36 + 208*t2 - 199*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s2,3)*t2*
                (-708 - 418*t2 + 379*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s2,2)*(-384 + 424*t2 - 279*Power(t2,2) + 
                  1068*Power(t2,3) + 355*Power(t2,4)) + 
               s2*(780 + 1980*t2 + 3839*Power(t2,2) - 
                  3221*Power(t2,3) + 267*Power(t2,4) - 45*Power(t2,5)) + 
               s*(-180 + 2*(-163 + 208*s2 + 74*Power(s2,2))*t2 + 
                  (3639 - 2126*s2 + 204*Power(s2,2))*Power(t2,2) + 
                  (263 - 2508*s2 - 807*Power(s2,2))*Power(t2,3) - 
                  3*(667 + 362*s2 + 27*Power(s2,2))*Power(t2,4) + 
                  45*Power(t2,5)) + 
               Power(s,2)*t2*(96 - 731*t2 - 648*Power(t2,2) + 
                  731*Power(t2,3) + 
                  s2*(180 + 102*t2 + 229*Power(t2,2) + 81*Power(t2,3)))) \
+ Power(t1,3)*Power(t2,2)*(-2*Power(s2,3)*t2*
                (98 + 185*t2 + 196*Power(t2,2) + Power(t2,3)) + 
               2*Power(s,3)*t2*
                (6 + 52*t2 - 117*Power(t2,2) + 79*Power(t2,3)) + 
               Power(s2,2)*t2*
                (936 + 1147*t2 - 1539*Power(t2,2) - 190*Power(t2,3) + 
                  70*Power(t2,4)) + 
               t2*(-517 + 123*t2 - 997*Power(t2,2) - 2405*Power(t2,3) + 
                  196*Power(t2,4)) + 
               2*s2*(-42 - 774*t2 - 1170*Power(t2,2) + 
                  2117*Power(t2,3) - 462*Power(t2,4) + 103*Power(t2,5)) + 
               2*s*(30 + (-163 + 32*s2 + 42*Power(s2,2))*t2 + 
                  (-1561 + 1515*s2 + 58*Power(s2,2))*Power(t2,2) + 
                  (1300 + 1883*s2 + 471*Power(s2,2))*Power(t2,3) + 
                  9*(187 + 76*s2 + 9*Power(s2,2))*Power(t2,4) - 
                  (201 + 70*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(16 + 1247*t2 - 547*Power(t2,2) - 
                  1730*Power(t2,3) + 70*Power(t2,4) - 
                  2*s2*(30 + 197*t2 + 270*Power(t2,2) + 159*Power(t2,3))))\
))*R1(t2))/((-1 + s2)*(-s + s2 - t1)*
       Power(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2,2)*Power(t1 - t2,3)*
       (-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)*t2) + 
    (8*(2*Power(-1 + s,3)*Power(t1,8) - 
         2*Power(s1,8)*Power(t1,2)*(6 - 3*s - 5*s2 + t1) - 
         Power(t1,3)*(4 + (-19 - 84*Power(s,2) + 2*s2 - 
               2*s*(-67 + 6*s2))*t2 + 
            (-92 + 78*Power(s,3) + 149*s2 + 5*Power(s2,2) - 
               6*Power(s2,3) + Power(s,2)*(-399 + 30*s2) + 
               s*(367 - 190*s2 + 10*Power(s2,2)))*Power(t2,2) + 
            (174 + 45*Power(s,3) - 105*s2 + 116*Power(s2,2) + 
               3*Power(s2,3) + Power(s,2)*(-108 + 65*s2) - 
               s*(7 + 208*s2 + 113*Power(s2,2)))*Power(t2,3) + 
            (64 - 15*Power(s,3) + 51*s2 + 23*Power(s2,2) + 
               15*Power(s2,3) + Power(s,2)*(119 + 45*s2) - 
               s*(175 + 142*s2 + 45*Power(s2,2)))*Power(t2,4) + 
            (-3 + s - s2)*Power(t2,5)) - 
         2*(-1 + s)*Power(t1,7)*
          (4 + t2 + 3*s2*t2 + Power(s,2)*(1 + 3*t2) - 
            s*(5 + (7 + 3*s2)*t2)) + 
         Power(t1,6)*(-8 + (5 - 20*s2)*t2 - 
            2*(5 + s2 + 3*Power(s2,2))*Power(t2,2) + 
            Power(s,3)*(-4 - 9*t2 + 6*Power(t2,2)) + 
            Power(s,2)*(6 + (15 - 14*s2)*t2 - 
               2*(13 + 6*s2)*Power(t2,2)) + 
            s*(6 + (-5 + 34*s2)*t2 + 
               (24 + 22*s2 + 6*Power(s2,2))*Power(t2,2))) + 
         Power(t1,5)*(-4 + (-57 + 20*s2)*t2 + 
            2*(5 - 25*s2 + 9*Power(s2,2))*Power(t2,2) + 
            (7 + 12*s2 + 2*Power(s2,2) + 2*Power(s2,3))*Power(t2,3) + 
            Power(s,3)*t2*(22 + 39*t2 - 2*Power(t2,2)) + 
            Power(s,2)*(-12 + (-121 + 4*s2)*t2 - 
               (126 + 23*s2)*Power(t2,2) + 2*(8 + 3*s2)*Power(t2,3)) + 
            s*(18 - 8*(-19 + 3*s2)*t2 + 
               (93 + 35*s2 - 16*Power(s2,2))*Power(t2,2) - 
               (29 + 18*s2 + 6*Power(s2,2))*Power(t2,3))) + 
         Power(s1,7)*(Power(s2,3)*(2 + t1) + 
            2*Power(s2,2)*t1*(1 - s + t1 - t2) + 
            s2*t1*(49*Power(t1,2) - 20*t2 - t1*(19 + 7*s + 11*t2)) + 
            t1*(-8*Power(t1,3) - 12*(-2 + s)*t2 + 
               Power(t1,2)*(-65 + 41*s + 5*t2) + 
               t1*(12 + 16*s - 6*Power(s,2) + 36*t2 - 20*s*t2))) - 
         8*Power(t2,2)*(Power(s,3)*t2*(1 + t2 + 2*Power(t2,2)) + 
            2*Power(s2,2)*Power(t2,2)*(21 + t2 + 3*Power(t2,2)) + 
            Power(s2,3)*t2*(1 - 3*t2 + 6*Power(t2,2)) + 
            s2*(9 + 39*t2 - 33*Power(t2,2) + Power(t2,3)) + 
            2*(-5 - 20*t2 + Power(t2,2) + 18*Power(t2,3) + 
               6*Power(t2,4)) - 
            s*(-9 + (-35 - 4*s2 + 9*Power(s2,2))*t2 + 
               (-87 + 24*s2 + 17*Power(s2,2))*Power(t2,2) + 
               (19 + 12*s2 + 10*Power(s2,2))*Power(t2,3) + 
               4*(4 + 3*s2)*Power(t2,4)) + 
            Power(s,2)*t2*(s2*(-9 + 19*t2 + 2*Power(t2,2)) + 
               2*t2*(-29 - 5*t2 + 3*Power(t2,2)))) + 
         2*Power(t1,2)*t2*(-36 - 
            (126 - 35*s2 + 24*Power(s2,2) + 2*Power(s2,3))*t2 - 
            (90 + 157*s2 - 78*Power(s2,2) + 9*Power(s2,3))*Power(t2,2) + 
            (-2 + 9*s2 - 6*Power(s2,2) + 23*Power(s2,3))*Power(t2,3) + 
            (22 + 17*s2 + 20*Power(s2,2))*Power(t2,4) + 
            Power(s,3)*t2*(-20 + 49*t2 + 31*Power(t2,2)) + 
            s*(42 + (161 + 72*s2)*t2 + 
               (433 + 208*s2 - 37*Power(s2,2))*Power(t2,2) - 
               5*(-23 - 8*s2 + 3*Power(s2,2))*Power(t2,3) - 
               5*(11 + 8*s2)*Power(t2,4)) + 
            Power(s,2)*t2*(8 - 358*t2 - 154*Power(t2,2) + 
               20*Power(t2,3) - s2*(42 + 59*t2 + 39*Power(t2,2)))) - 
         Power(t1,4)*(-10 + (-75 + 4*s2)*t2 + 
            (-64 - 145*s2 + 15*Power(s2,2))*Power(t2,2) + 
            (-59 - 10*s2 - 34*Power(s2,2) + 4*Power(s2,3))*Power(t2,3) + 
            (8 + 7*s2 + 4*Power(s2,2))*Power(t2,4) + 
            Power(s,3)*t2*(-28 + 17*t2 + 43*Power(t2,2)) + 
            s*(12 + (81 + 10*s2)*t2 + 
               (252 + 258*s2 - 19*Power(s2,2))*Power(t2,2) + 
               (169 + 126*s2 + 35*Power(s2,2))*Power(t2,3) - 
               2*(9 + 4*s2)*Power(t2,4)) + 
            Power(s,2)*t2*(40 - 189*t2 - 182*Power(t2,2) + 
               4*Power(t2,3) - 2*s2*(6 + 43*t2 + 41*Power(t2,2)))) + 
         4*t1*t2*(7 + 2*Power(t2,2) + 96*Power(t2,3) + 39*Power(t2,4) + 
            Power(s,3)*Power(t2,2)*(13 - 9*t2 - 4*Power(t2,2)) + 
            Power(s2,3)*Power(t2,2)*(3 - 3*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*t2*(9 + 20*t2 + 35*Power(t2,2)) + 
            Power(s,2)*t2*(-27 + (-92 + 41*s2)*t2 + 
               (67 + 43*s2)*Power(t2,2) + 4*(10 + 3*s2)*Power(t2,3)) + 
            s2*t2*(23 + 69*t2 - 19*Power(t2,2) + 3*Power(t2,3) - 
               4*Power(t2,4)) + 
            s*t2*(41 + 47*t2 - 105*Power(t2,2) - 59*Power(t2,3) + 
               4*Power(t2,4) - 
               Power(s2,2)*t2*(17 + 31*t2 + 12*Power(t2,2)) - 
               2*s2*(15 + 62*t2 + 69*Power(t2,2) + 20*Power(t2,3)))) + 
         Power(s1,6)*(-12*Power(t1,5) + 
            Power(s2,3)*(-4 + 4*Power(t1,2) - 7*t1*(-2 + t2) - 5*t2) + 
            6*(-2 + s)*Power(t2,2) + 
            2*Power(t1,4)*(-73 + 55*s + 10*t2) + 
            Power(t1,3)*(70 - 42*Power(s,2) + s*(88 - 101*t2) + 
               152*t2 - 5*Power(t2,2)) + 
            2*t1*t2*(6*Power(s,2) + 4*s*(-4 + 5*t2) - 3*(4 + 11*t2)) + 
            Power(s2,2)*(10*Power(t1,3) + 
               Power(t1,2)*(9 - 7*s - 9*t2) + 
               2*(-3 - t2 + 4*s*t2 + Power(t2,2)) + 
               t1*(-15 - 2*s - 6*t2 + 11*s*t2 + 4*Power(t2,2))) + 
            Power(t1,2)*(23 + 202*t2 - 39*Power(t2,2) + 
               3*Power(s,2)*(-2 + 9*t2) + 
               s*(-11 - 180*t2 + 21*Power(t2,2))) + 
            s2*(98*Power(t1,4) + 10*Power(t2,2) - 
               Power(t1,3)*(101 + 33*s + 40*t2) - 
               2*t1*(2 + s - 21*t2 - 12*s*t2 + 2*Power(s,2)*t2 - 
                  11*Power(t2,2) + 2*s*Power(t2,2)) + 
               Power(t1,2)*(-18 + Power(s,2) - 127*t2 - 10*Power(t2,2) - 
                  4*s*(1 + 3*t2)))) + 
         Power(s1,5)*(-8*Power(t1,6) + 
            6*Power(t1,5)*(-29 + 25*s + 5*t2) + 
            2*Power(t2,2)*(6 - 3*Power(s,2) + s*(8 - 10*t2) + 16*t2) + 
            Power(t1,4)*(-116*Power(s,2) + s*(202 - 204*t2) + 
               3*(50 + 77*t2 - 5*Power(t2,2))) + 
            Power(s2,3)*(6*Power(t1,3) + t2*(-10 + 7*t2) - 
               4*Power(t1,2)*(-9 + 7*t2) + 
               t1*(-16 - 31*t2 + 11*Power(t2,2))) + 
            Power(t1,3)*(101 + 3*Power(s,3) + 519*t2 - 
               129*Power(t2,2) + Power(s,2)*(-27 + 128*t2) + 
               s*(-75 - 637*t2 + 81*Power(t2,2))) + 
            t1*(2 - 48*t2 - 209*Power(t2,2) - 
               2*Power(s,3)*Power(t2,2) + 63*Power(t2,3) - 
               2*Power(s,2)*t2*(-5 + 23*t2 + Power(t2,2)) + 
               s*(4 + 12*t2 + 241*Power(t2,2) - 42*Power(t2,3))) + 
            Power(s2,2)*(12 + 20*Power(t1,4) + (27 - 16*s)*t2 + 
               (4 - 17*s)*Power(t2,2) - 4*Power(t2,3) - 
               8*Power(t1,3)*(-2 + s + 2*t2) - 
               Power(t1,2)*(70 + s*(8 - 42*t2) + 15*t2 + 
                  9*Power(t2,2)) + 
               t1*(-44 + (21 + 55*s)*t2 - 6*(-3 + 4*s)*Power(t2,2) - 
                  2*Power(t2,3))) + 
            Power(t1,2)*(52 + Power(s,3)*(6 - 9*t2) - 332*t2 - 
               458*Power(t2,2) + 21*Power(t2,3) + 
               Power(s,2)*(15 + 149*t2 - 35*Power(t2,2)) - 
               s*(78 + 223*t2 - 391*Power(t2,2) + 11*Power(t2,3))) + 
            s2*(6 + 102*Power(t1,5) + (4 - 10*s)*t2 + 
               (-23 - 17*s + 10*Power(s,2))*Power(t2,2) + 
               (-11 + 4*s)*Power(t2,3) - 
               2*Power(t1,4)*(107 + 31*s + 25*t2) + 
               Power(t1,3)*(-58 + 6*Power(s,2) - 253*t2 - 
                  37*Power(t2,2) - 2*s*(7 + 29*t2)) + 
               Power(t1,2)*(24 + 325*t2 + 127*Power(t2,2) + 
                  11*Power(t2,3) - 5*Power(s,2)*(2 + t2) + 
                  4*s*(6 + 26*t2 + 11*Power(t2,2))) + 
               t1*(27 + 40*t2 + 99*Power(t2,2) + 20*Power(t2,3) + 
                  Power(s,2)*t2*(-8 + 15*t2) + 
                  s*(22 - 60*t2 + 28*Power(t2,2) + 4*Power(t2,3))))) + 
         Power(s1,4)*(-2 - 2*Power(t1,7) - 2*t2 + 2*s*t2 + 
            25*Power(t2,2) - s*Power(t2,2) - 10*Power(s,2)*Power(t2,2) + 
            72*Power(t2,3) - 102*s*Power(t2,3) + 
            19*Power(s,2)*Power(t2,3) + 4*Power(s,3)*Power(t2,3) - 
            29*Power(t2,4) + 21*s*Power(t2,4) + 
            2*Power(s,2)*Power(t2,4) + 
            2*Power(t1,6)*(-58 + 55*s + 10*t2) + 
            Power(t1,5)*(148 - 164*Power(s,2) + s*(248 - 206*t2) + 
               157*t2 - 15*Power(t2,2)) + 
            Power(s2,3)*(4*Power(t1,4) + Power(t1,3)*(44 - 42*t2) + 
               t1*t2*(-54 + 61*t2 - 5*Power(t2,2)) - 
               7*t2*(-4 - 5*t2 + Power(t2,2)) + 
               Power(t1,2)*(-24 - 55*t2 + 29*Power(t2,2))) + 
            Power(t1,4)*(174 + 14*Power(s,3) + 627*t2 - 
               159*Power(t2,2) + Power(s,2)*(-54 + 242*t2) + 
               s*(-166 - 941*t2 + 117*Power(t2,2))) + 
            Power(t1,2)*(-95 - 298*t2 - 578*Power(t2,2) + 
               314*Power(t2,3) + 
               Power(s,3)*(-4 - 8*t2 + 9*Power(t2,2)) + 
               s*(5 + 460*t2 + 1132*Power(t2,2) - 272*Power(t2,3)) + 
               Power(s,2)*(38 - 22*t2 - 403*Power(t2,2) + 
                  18*Power(t2,3))) + 
            Power(t1,3)*(121 + Power(s,3)*(22 - 42*t2) - 832*t2 - 
               842*Power(t2,2) + 63*Power(t2,3) + 
               Power(s,2)*(82 + 511*t2 - 121*Power(t2,2)) - 
               s*(282 + 534*t2 - 925*Power(t2,2) + 23*Power(t2,3))) + 
            t1*(-13 - 106*t2 + 458*Power(t2,2) + 492*Power(t2,3) - 
               27*Power(t2,4) + Power(s,2)*Power(t2,2)*(-233 + 82*t2) + 
               Power(s,3)*t2*(-12 + 14*t2 + 5*Power(t2,2)) + 
               s*(-20 + 213*t2 + 178*Power(t2,2) - 483*Power(t2,3) + 
                  22*Power(t2,4))) + 
            Power(s2,2)*(20*Power(t1,5) - 
               2*Power(t1,4)*(-7 + s + 7*t2) - 
               Power(t1,3)*(120 + s*(12 - 58*t2) + 5*t2 + 
                  53*Power(t2,2)) + 
               Power(t1,2)*(-92 + (59 + 79*s)*t2 + 
                  (45 - 49*s)*Power(t2,2) + 18*Power(t2,3)) + 
               t1*(48 + (241 - 70*s)*t2 - (37 + 103*s)*Power(t2,2) + 
                  (16 + 15*s)*Power(t2,3)) + 
               t2*(32 - 30*t2 - 9*Power(t2,2) + 2*Power(t2,3) + 
                  6*s*(2 - 7*t2 + 3*Power(t2,2)))) + 
            s2*(-12 + 58*Power(t1,6) + (-39 + 14*s)*t2 + 
               (-22 + 76*s - 17*Power(s,2))*Power(t2,2) - 
               (21 + 16*s + 15*Power(s,2))*Power(t2,3) - 
               2*(5 + 2*s)*Power(t2,4) - 
               2*Power(t1,5)*(113 + 29*s + 10*t2) + 
               Power(t1,4)*(-70 + 14*Power(s,2) - 221*t2 - 
                  51*Power(t2,2) - 16*s*(1 + 7*t2)) + 
               Power(t1,3)*(92 + 792*t2 + 179*Power(t2,2) + 
                  23*Power(t2,3) + Power(s,2)*(-40 + 26*t2) + 
                  2*s*(48 + 73*t2 + 87*Power(t2,2))) + 
               Power(t1,2)*(125 + 11*t2 + 238*Power(t2,2) + 
                  108*Power(t2,3) + Power(s,2)*t2*(-21 + 11*t2) - 
                  4*s*(-8 + 7*t2 - 46*Power(t2,2) + 9*Power(t2,3))) - 
               t1*(-46 + 105*t2 + 347*Power(t2,2) + 130*Power(t2,3) + 
                  22*Power(t2,4) + 
                  Power(s,2)*t2*(-42 - 28*t2 + 15*Power(t2,2)) + 
                  s*(12 + 86*t2 + 55*Power(t2,2) + 98*Power(t2,3))))) + 
         Power(s1,3)*(4 + (17 + s*(2 - 12*s2) - 34*s2 - 84*Power(s2,2))*
             t2 + (54 + 6*Power(s,3) + Power(s,2)*(9 - 2*s2) + 81*s2 - 
               195*Power(s2,2) - 6*Power(s2,3) + 
               s*(-141 + 50*s2 + 114*Power(s2,2)))*Power(t2,2) - 
            (196 + 15*Power(s,3) - 123*s2 - 30*Power(s2,2) + 
               41*Power(s2,3) + 7*Power(s,2)*(-18 + 5*s2) + 
               s*(43 + 4*s2 - 91*Power(s2,2)))*Power(t2,3) + 
            (-178 - 3*Power(s,3) + 43*s2 - 7*Power(s2,2) + 
               3*Power(s2,3) + Power(s,2)*(-47 + 9*s2) + 
               s*(193 + 54*s2 - 9*Power(s2,2)))*Power(t2,4) - 
            11*(-1 + s - s2)*Power(t2,5) + 
            Power(t1,7)*(-41 + 41*s + 17*s2 + 5*t2) + 
            Power(t1,6)*(64 - 126*Power(s,2) + 10*Power(s2,2) + 
               s*(172 - 27*s2 - 104*t2) + 49*t2 - 5*Power(t2,2) + 
               s2*(-119 + 5*t2)) + 
            Power(t1,5)*(140 + 26*Power(s,3) + Power(s2,3) - 
               6*Power(s2,2)*(-1 + t2) + 401*t2 - 87*Power(t2,2) + 
               2*Power(s,2)*(-33 + 8*s2 + 114*t2) - 
               s2*(42 + 91*t2 + 31*Power(t2,2)) + 
               s*(-152 + 2*Power(s2,2) - 661*t2 + 75*Power(t2,2) - 
                  4*s2*(1 + 27*t2))) + 
            t1*(-16 + 215*t2 + 293*Power(t2,2) + 181*Power(t2,3) - 
               321*Power(t2,4) + 
               Power(s2,3)*t2*(52 + 103*t2 - 55*Power(t2,2)) + 
               Power(s,3)*t2*(8 + 33*t2 - 29*Power(t2,2)) - 
               Power(s2,2)*t2*
                (-136 + 227*t2 + 19*Power(t2,2) + 34*Power(t2,3)) - 
               s2*(48 + 383*t2 - 193*Power(t2,2) + 105*Power(t2,3) + 
                  105*Power(t2,4)) + 
               s*(12 + (-13 + 64*s2 + 48*Power(s2,2))*t2 + 
                  (-725 + 214*s2 - 101*Power(s2,2))*Power(t2,2) + 
                  (-783 - 202*s2 + 81*Power(s2,2))*Power(t2,3) + 
                  (301 + 68*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (-128 + 197*t2 + 389*Power(t2,2) - 34*Power(t2,3) + 
                  s2*(-12 - 67*t2 + 3*Power(t2,2)))) + 
            Power(t1,4)*(107 + Power(s,3)*(28 - 78*t2) + 
               Power(s2,3)*(26 - 28*t2) - 788*t2 - 560*Power(t2,2) + 
               55*Power(t2,3) - 
               5*Power(s2,2)*(18 - 3*t2 + 13*Power(t2,2)) + 
               s2*(100 + 828*t2 + 49*Power(t2,2) + 13*Power(t2,3)) + 
               Power(s,2)*(168 + 747*t2 - 153*Power(t2,2) + 
                  s2*(-60 + 74*t2)) + 
               s*(-404 - 640*t2 + 887*Power(t2,2) - 13*Power(t2,3) + 
                  8*Power(s2,2)*(-1 + 4*t2) + 
                  2*s2*(62 + 29*t2 + 109*Power(t2,2)))) + 
            Power(t1,3)*(-207 - 591*t2 - 654*Power(t2,2) + 
               356*Power(t2,3) + 
               Power(s2,3)*(-16 - 37*t2 + 21*Power(t2,2)) + 
               Power(s,3)*(-16 - 41*t2 + 45*Power(t2,2)) + 
               Power(s2,2)*(-76 + 37*t2 + 35*Power(t2,2) + 
                  46*Power(t2,3)) + 
               Power(s,2)*(116 - (161 + 37*s2)*t2 - 
                  (893 + 69*s2)*Power(t2,2) + 46*Power(t2,3)) + 
               s2*(185 - 139*t2 + 96*Power(t2,2) + 170*Power(t2,3)) + 
               s*(25 + 1155*t2 + 1884*Power(t2,2) - 454*Power(t2,3) + 
                  Power(s2,2)*t2*(11 + 3*t2) + 
                  s2*(-28 + 226*t2 + 394*Power(t2,2) - 92*Power(t2,3)))) \
+ Power(t1,2)*(-41 - 301*t2 + 1636*Power(t2,2) + 1275*Power(t2,3) - 
               113*Power(t2,4) + 
               Power(s2,3)*t2*(-90 + 119*t2 - 3*Power(t2,2)) + 
               Power(s,3)*t2*(-50 + 95*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(60 + 447*t2 - 71*Power(t2,2) + 
                  122*Power(t2,3)) - 
               Power(s,2)*(24 + (243 - 134*s2)*t2 + 
                  (759 + 17*s2)*Power(t2,2) + (-350 + 9*s2)*Power(t2,3)) \
- s2*(-22 + 249*t2 + 1067*Power(t2,2) + 304*Power(t2,3) + 
                  78*Power(t2,4)) + 
               s*(46 + 809*t2 + 341*Power(t2,2) - 1590*Power(t2,3) + 
                  78*Power(t2,4) + 
                  Power(s2,2)*t2*(-62 - 197*t2 + 9*Power(t2,2)) - 
                  2*s2*(6 + 161*t2 + 108*Power(t2,2) + 236*Power(t2,3))))\
) - s1*(2*(1 - 5*s + 4*Power(s,2))*Power(t1,8) + 
            Power(t1,2)*(16 - 
               (35 + 132*Power(s,2) + 38*s2 - 72*Power(s2,2) + 
                  s*(-310 + 36*s2))*t2 - 
               (102 + 6*Power(s,3) + Power(s,2)*(859 - 178*s2) - 
                  165*s2 - 217*Power(s2,2) + 10*Power(s2,3) + 
                  s*(-927 + 598*s2 + 18*Power(s2,2)))*Power(t2,2) + 
               (1244 + 133*Power(s,3) - 565*s2 + 122*Power(s2,2) + 
                  67*Power(s2,3) + Power(s,2)*(-214 + 49*s2) - 
                  s*(571 + 684*s2 + 249*Power(s2,2)))*Power(t2,3) + 
               (582 - 27*Power(s,3) + 3*s2 + 85*Power(s2,2) + 
                  27*Power(s2,3) + Power(s,2)*(461 + 81*s2) - 
                  s*(935 + 546*s2 + 81*Power(s2,2)))*Power(t2,4) + 
               5*(-5 + 9*s - 9*s2)*Power(t2,5)) - 
            4*t2*(-7 + (-26 - 9*Power(s,2) - 23*s2 + 27*Power(s2,2) + 
                  s*(31 + 30*s2))*t2 + 
               (-12 + 3*Power(s,3) - 49*s2 + 66*Power(s2,2) + 
                  5*Power(s2,3) - 5*Power(s,2)*(6 + 5*s2) + 
                  s*(93 + 32*s2 - 23*Power(s2,2)))*Power(t2,2) + 
               (106 + 9*Power(s,3) - 25*s2 - Power(s2,2) + 
                  19*Power(s2,3) + Power(s,2)*(-57 + 13*s2) - 
                  s*(27 + 18*s2 + 41*Power(s2,2)))*Power(t2,3) + 
               (59 + 32*Power(s,2) - 23*s2 + 8*Power(s2,2) - 
                  s*(89 + 40*s2))*Power(t2,4) + 
               8*(-1 + s - s2)*Power(t2,5)) + 
            Power(t1,7)*(1 - 11*Power(s,3) + 4*s2 + 
               Power(s,2)*(27 - 2*s2 - 20*t2) - 16*t2 + 
               s*(-11 + 30*t2 + 2*s2*(-1 + 5*t2))) - 
            4*t1*t2*(-25 - 195*t2 - 55*Power(t2,2) + 163*Power(t2,3) + 
               80*Power(t2,4) + 
               Power(s2,3)*t2*(-1 - 20*t2 + 21*Power(t2,2)) + 
               Power(s,3)*t2*(-1 - 20*t2 + 29*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-55 + 133*t2 + 4*Power(t2,2) + 34*Power(t2,3)) + 
               s2*(15 + 182*t2 - 208*Power(t2,2) - 14*Power(t2,3) + 
                  17*Power(t2,4)) + 
               s*(15 + (90 + 6*s2 - 15*Power(s2,2))*t2 + 
                  (540 + 10*s2 - 24*Power(s2,2))*Power(t2,2) + 
                  (46 + 28*s2 - 13*Power(s2,2))*Power(t2,3) - 
                  17*(7 + 4*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (65 - 243*t2 - 168*Power(t2,2) + 34*Power(t2,3) + 
                  s2*(-15 + 48*t2 - 37*Power(t2,2)))) + 
            Power(t1,6)*(-37 + 16*t2 + 2*Power(s2,2)*(-2 + t2)*t2 + 
               14*Power(t2,2) + Power(s,3)*(2 + 33*t2) + 
               s2*(-4 - 63*t2 + 16*Power(t2,2)) + 
               Power(s,2)*(-73 - 164*t2 + 16*Power(t2,2) - 
                  5*s2*(-2 + 7*t2)) + 
               s*(110 + 145*t2 + 2*Power(s2,2)*t2 - 50*Power(t2,2) - 
                  6*s2*(2 - 7*t2 + 3*Power(t2,2)))) + 
            Power(t1,3)*(-44 - 381*t2 - 483*Power(t2,2) - 
               171*Power(t2,3) + 135*Power(t2,4) + 
               Power(s2,3)*t2*(-8 - 29*t2 + 25*Power(t2,2)) + 
               Power(s,3)*t2*(-52 - 19*t2 + 131*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-100 + 81*t2 - 27*Power(t2,2) + 70*Power(t2,3)) + 
               s2*(-12 + 197*t2 - 479*Power(t2,2) - 29*Power(t2,3) + 
                  99*Power(t2,4)) + 
               s*(48 + (295 + 8*s2 + 12*Power(s2,2))*t2 + 
                  (1627 + 718*s2 - 65*Power(s2,2))*Power(t2,2) + 
                  9*(101 + 38*s2 + 9*Power(s2,2))*Power(t2,3) - 
                  5*(59 + 28*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (212 - 887*t2 - 867*Power(t2,2) + 70*Power(t2,3) - 
                  3*s2*(16 + 37*t2 + 79*Power(t2,2)))) + 
            Power(t1,5)*(51 + 109*t2 + 149*Power(t2,2) + 
               4*Power(s2,3)*Power(t2,2) - 21*Power(t2,3) + 
               Power(s,3)*(16 + 43*t2 - 33*Power(t2,2)) + 
               Power(s2,2)*t2*(10 + 21*t2 - 4*Power(t2,2)) + 
               s2*(-16 + 69*t2 + 11*Power(t2,2) - 14*Power(t2,3)) + 
               Power(s,2)*(-52 + (57 + 47*s2)*t2 + 
                  (277 + 70*s2)*Power(t2,2) - 4*Power(t2,3)) - 
               s*(39 + 249*t2 + 347*Power(t2,2) - 36*Power(t2,3) + 
                  Power(s2,2)*t2*(-14 + 41*t2) - 
                  2*s2*(13 - 85*t2 - 77*Power(t2,2) + 4*Power(t2,3)))) + 
            Power(t1,4)*(11 + 147*t2 - 392*Power(t2,2) - 
               173*Power(t2,3) + 11*Power(t2,4) + 
               Power(s2,3)*t2*(12 - 18*t2 - 11*Power(t2,2)) + 
               Power(s,3)*t2*(-18 - 145*t2 + 11*Power(t2,2)) - 
               Power(s2,2)*t2*(18 + 31*t2 + 50*Power(t2,2)) + 
               Power(s,2)*(48 + (513 - 58*s2)*t2 + 
                  (535 + 89*s2)*Power(t2,2) - (202 + 33*s2)*Power(t2,3)) \
+ s2*(28 - 37*t2 + 362*Power(t2,2) - 87*Power(t2,3) + 2*Power(t2,4)) + 
               s*(-98 - 647*t2 - 247*Power(t2,2) + 434*Power(t2,3) - 
                  2*Power(t2,4) + 
                  Power(s2,2)*t2*(-30 + 74*t2 + 33*Power(t2,2)) + 
                  s2*(-12 + 172*t2 + 47*Power(t2,2) + 252*Power(t2,3))))) \
+ Power(s1,2)*(2*(-3 + 3*s + s2)*Power(t1,8) + 
            t1*(16 + (137 + 72*Power(s,2) - 2*s2 - 132*Power(s2,2) - 
                  2*s*(67 + 18*s2))*t2 + 
               (224 + 2*Power(s,3) + Power(s,2)*(221 - 78*s2) + 
                  329*s2 - 591*Power(s2,2) + 54*Power(s2,3) + 
                  s*(-869 + 218*s2 + 166*Power(s2,2)))*Power(t2,2) + 
               (-1378 - 47*Power(s,3) + Power(s,2)*(568 - 91*s2) + 
                  583*s2 + 72*Power(s2,2) - 129*Power(s2,3) + 
                  3*s*(35 + 72*s2 + 89*Power(s2,2)))*Power(t2,3) + 
               (-888 - 7*Power(s,3) + 21*Power(s,2)*(-17 + s2) + 
                  267*s2 - 101*Power(s2,2) + 7*Power(s2,3) + 
                  s*(1225 + 458*s2 - 21*Power(s2,2)))*Power(t2,4) + 
               (97 - 87*s + 87*s2)*Power(t2,5)) + 
            Power(t1,7)*(6 - 50*Power(s,2) + 2*Power(s2,2) + 
               s*(64 - 5*s2 - 21*t2) + 7*t2 + s2*(-25 + 4*t2)) + 
            Power(t1,6)*(45 + 24*Power(s,3) - Power(s2,2)*(-1 + t2) + 
               131*t2 - 18*Power(t2,2) + 
               Power(s,2)*(-54 + 9*s2 + 107*t2) - 
               s2*(16 + 16*t2 + 7*Power(t2,2)) + 
               s*(-45 + 4*s2 + Power(s2,2) - 219*t2 - 52*s2*t2 + 
                  18*Power(t2,2))) + 
            2*t2*(6 + (-60 + 7*s + 30*Power(s,2) - 2*Power(s,3))*t2 - 
               (48 - 167*s + 68*Power(s,2) + 5*Power(s,3))*Power(t2,2) + 
               (12 + 89*s - 54*Power(s,2) + 3*Power(s,3))*Power(t2,3) + 
               (58 - 55*s + 8*Power(s,2))*Power(t2,4) + 
               Power(s2,3)*t2*(-20 - 27*t2 + 11*Power(t2,2)) + 
               s2*(42 + (141 - 84*s)*t2 + 
                  (-67 - 116*s + 71*Power(s,2))*Power(t2,2) + 
                  (11 + 32*s + 5*Power(s,2))*Power(t2,3) + 
                  (17 - 16*s)*Power(t2,4)) + 
               Power(s2,2)*t2*
                (14 + 88*t2 - 2*Power(t2,2) + 8*Power(t2,3) - 
                  s*(42 + 15*t2 + 19*Power(t2,2)))) + 
            Power(t1,5)*(67 + Power(s,3)*(12 - 72*t2) + 
               Power(s2,3)*(6 - 7*t2) - 282*t2 - 156*Power(t2,2) + 
               13*Power(t2,3) + 
               Power(s2,2)*(-25 + 15*t2 - 27*Power(t2,2)) + 
               s2*(40 + 376*t2 - 41*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(162 + 517*t2 - 83*Power(t2,2) + 
                  s2*(-40 + 76*t2)) + 
               s*(-292 - 426*t2 + 363*Power(t2,2) - Power(t2,3) + 
                  Power(s2,2)*(-2 + 3*t2) + 
                  2*s2*(33 - 19*t2 + 55*Power(t2,2)))) + 
            Power(t1,2)*(44 + 675*t2 + 655*Power(t2,2) - 
               163*Power(t2,3) - 451*Power(t2,4) + 
               Power(s,3)*t2*(32 + 53*t2 - 121*Power(t2,2)) + 
               Power(s2,3)*t2*(32 + 81*t2 - 69*Power(t2,2)) - 
               Power(s2,2)*t2*
                (-212 + 319*t2 + 33*Power(t2,2) + 134*Power(t2,3)) + 
               s2*(-24 - 601*t2 + 765*Power(t2,2) + 87*Power(t2,3) - 
                  187*Power(t2,4)) + 
               s*(-24 + 3*(-55 + 12*s2 + 8*Power(s2,2))*t2 + 
                  (-2459 - 370*s2 + 35*Power(s2,2))*Power(t2,2) + 
                  (-1429 - 466*s2 + 17*Power(s2,2))*Power(t2,3) + 
                  (685 + 268*s2)*Power(t2,4)) + 
               Power(s,2)*t2*(-292 + 753*t2 + 1231*Power(t2,2) - 
                  134*Power(t2,3) + s2*(24 - 41*t2 + 173*Power(t2,2)))) + 
            Power(t1,4)*(-153 - 447*t2 - 452*Power(t2,2) + 
               158*Power(t2,3) - Power(s2,3)*(4 + 8*t2 + Power(t2,2)) + 
               Power(s,3)*(-24 - 67*t2 + 61*Power(t2,2)) + 
               Power(s2,2)*(-22 - 9*t2 - 9*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(s,2)*(124 - 3*(67 + 19*s2)*t2 - 
                  (787 + 123*s2)*Power(t2,2) + 30*Power(t2,3)) + 
               s2*(103 - 159*t2 - 14*Power(t2,2) + 96*Power(t2,3)) + 
               s*(57 + 961*t2 + 1230*Power(t2,2) - 260*Power(t2,3) + 
                  7*Power(s2,2)*t2*(-5 + 9*t2) + 
                  s2*(-64 + 330*t2 + 370*Power(t2,2) - 60*Power(t2,3)))) \
- Power(t1,3)*(35 + 213*t2 - 1500*Power(t2,2) - 837*Power(t2,3) + 
               65*Power(t2,4) + 
               Power(s2,3)*t2*(58 - 83*t2 - 11*Power(t2,2)) + 
               Power(s,3)*t2*(42 - 187*t2 + 11*Power(t2,2)) - 
               Power(s2,2)*(24 + 251*t2 - 17*Power(t2,2) + 
                  150*Power(t2,3)) + 
               Power(s,2)*(60 + (635 - 146*s2)*t2 + 
                  (1005 + 101*s2)*Power(t2,2) - (454 + 33*s2)*Power(t2,3)\
) + s2*(46 + 139*t2 + 1079*Power(t2,2) + 38*Power(t2,3) + 
                  42*Power(t2,4)) + 
               s*(-146 - 1107*t2 - 229*Power(t2,2) + 1380*Power(t2,3) - 
                  42*Power(t2,4) + 
                  Power(s2,2)*t2*(-22 + 169*t2 + 33*Power(t2,2)) + 
                  2*s2*(-6 + 197*t2 + 114*Power(t2,2) + 302*Power(t2,3))))\
))*R2(1 - s1 - t1 + t2))/
     ((-1 + s2)*(-s + s2 - t1)*
       Power(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2,2)*
       (-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)) - 
    (8*(Power(-1 + s,3)*Power(t1,12) + 
         Power(s1,12)*Power(t1,2)*(s2 + t1) - 
         4*t1*Power(t2,2)*(-15 + 
            3*(9*Power(s,2) + 2*s*(-5 + 9*s2) - 
               3*(6 + 4*s2 + Power(s2,2)))*t2 - 
            (179 + 14*Power(s,3) + 110*s2 + 12*Power(s2,2) + 
               4*Power(s2,3) + 8*Power(s,2)*(-10 + 9*s2) - 
               2*s*(54 + 126*s2 + 5*Power(s2,2)))*Power(t2,2) + 
            2*(18 + 22*Power(s,3) - 213*s2 + 27*Power(s2,2) + 
               11*Power(s2,3) - Power(s,2)*(151 + 65*s2) + 
               s*(223 + 372*s2 - 80*Power(s2,2)))*Power(t2,3) + 
            (-125 + 26*Power(s,3) + 78*s2 + 4*Power(s2,2) + 
               16*Power(s2,3) - 4*Power(s,2)*(118 + 9*s2) + 
               s*(734 + 164*s2 - 22*Power(s2,2)))*Power(t2,4) + 
            (162 + 24*Power(s,3) + 102*s2 + 11*Power(s2,2) - 
               2*Power(s2,3) - Power(s,2)*(101 + 50*s2) + 
               2*s*(-28 + 9*s2 + 14*Power(s2,2)))*Power(t2,5) + 
            (47 + 16*Power(s,2) + 8*s2 + 16*Power(s2,2) - 
               2*s*(25 + 16*s2))*Power(t2,6)) + 
         Power(t1,5)*(2 + (8 - 60*Power(s,2) + s*(91 - 6*s2) + s2)*t2 + 
            (6 + 80*Power(s,3) + 114*s2 - 2*Power(s2,2) + 
               3*Power(s,2)*(-197 + 8*s2) + 
               s*(610 - 199*s2 + 4*Power(s2,2)))*Power(t2,2) + 
            (-253 + 360*Power(s,3) + 859*s2 + 12*Power(s2,2) - 
               5*Power(s2,3) + Power(s,2)*(-1355 + 299*s2) + 
               s*(807 - 1253*s2 + 10*Power(s2,2)))*Power(t2,3) + 
            (409 + 30*Power(s,3) + 412*s2 + 435*Power(s2,2) - 
               18*Power(s2,3) + Power(s,2)*(792 + 482*s2) - 
               s*(1740 + 1663*s2 + 274*Power(s2,2)))*Power(t2,4) + 
            (89 - 162*Power(s,3) + 140*s2 + 136*Power(s2,2) + 
               31*Power(s2,3) + Power(s,2)*(871 + 355*s2) - 
               s*(1026 + 733*s2 + 224*Power(s2,2)))*Power(t2,5) - 
            (53 + 21*Power(s,2) + 22*s2 + 21*Power(s2,2) - 
               2*s*(37 + 21*s2))*Power(t2,6)) + 
         2*Power(t1,3)*t2*(-10 + 
            5*(-8 + 18*Power(s,2) - s2 + s*(-28 + 6*s2))*t2 - 
            (261 + 85*Power(s,3) + 204*s2 - 13*Power(s2,2) - 
               4*Power(s2,3) + Power(s,2)*(-477 + 50*s2) + 
               s*(229 - 350*s2 + 21*Power(s2,2)))*Power(t2,2) + 
            (78 - 84*Power(s,3) + Power(s,2)*(164 - 457*s2) - 
               1109*s2 - 8*Power(s2,2) + 15*Power(s2,3) + 
               s*(443 + 1804*s2 - 98*Power(s2,2)))*Power(t2,3) + 
            (-446 + 135*Power(s,3) - 267*s2 - 314*Power(s2,2) + 
               22*Power(s2,3) - 2*Power(s,2)*(745 + 212*s2) + 
               s*(2281 + 1500*s2 + 147*Power(s2,2)))*Power(t2,4) + 
            (114 + 106*Power(s,3) + 46*s2 - 48*Power(s2,2) - 
               9*Power(s2,3) - Power(s,2)*(582 + 221*s2) + 
               s*(517 + 382*s2 + 124*Power(s2,2)))*Power(t2,5) + 
            (85 + 37*Power(s,2) + 35*s2 + 37*Power(s2,2) - 
               2*s*(60 + 37*s2))*Power(t2,6)) + 
         Power(s1,11)*t1*(6*Power(t1,3) + 
            Power(t1,2)*(-7 + 2*s + 10*s2 - 3*t2) - 2*s2*t2 - 
            t1*(1 + s*(s2 - t2) + 3*t2 + 4*s2*t2)) - 
         (-1 + s)*Power(t1,11)*
          (4 + (2 + 3*s2)*t2 + Power(s,2)*(1 + 4*t2) - 
            s*(5 + 3*(3 + s2)*t2)) + 
         Power(t1,9)*(-6 + (-45 + 7*s2)*t2 + 
            (-13 - 19*s2 + 8*Power(s2,2))*Power(t2,2) + 
            (5 + 6*s2 + 4*Power(s2,2) + Power(s2,3))*Power(t2,3) + 
            Power(s,3)*(1 + 22*t2 + 41*Power(t2,2) - 4*Power(t2,3)) + 
            Power(s,2)*(-12 - (105 + s2)*t2 - 
               (125 + 18*s2)*Power(t2,2) + 3*(7 + 3*s2)*Power(t2,3)) + 
            s*(18 - 6*(-21 + s2)*t2 + 
               (88 + 29*s2 - 8*Power(s2,2))*Power(t2,2) - 
               2*(14 + 10*s2 + 3*Power(s2,2))*Power(t2,3))) + 
         Power(t1,8)*(9 + (59 + 8*s2)*t2 + 
            (43 + 127*s2 - 6*Power(s2,2))*Power(t2,2) + 
            (24 + 42*s2 + 26*Power(s2,2) - 2*Power(s2,3))*Power(t2,3) - 
            (7 + 5*s2 + 3*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
            Power(s,3)*(2 + 28*t2 - 7*Power(t2,2) - 65*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s,2)*(-3 + (-32 + 13*s2)*t2 + 
               4*(40 + 21*s2)*Power(t2,2) + 
               3*(83 + 30*s2)*Power(t2,3) - (10 + 3*s2)*Power(t2,4)) + 
            s*(-9 - 2*(34 + 11*s2)*t2 + 
               (-253 - 206*s2 + 6*Power(s2,2))*Power(t2,2) - 
               (221 + 159*s2 + 23*Power(s2,2))*Power(t2,3) + 
               (20 + 13*s2 + 3*Power(s2,2))*Power(t2,4))) + 
         Power(t1,10)*(-3 + (3 - 10*s2)*t2 - 
            (3 + 5*s2 + 3*Power(s2,2))*Power(t2,2) + 
            Power(s,3)*(-3 - 8*t2 + 6*Power(t2,2)) + 
            Power(s,2)*(6 + (15 - 7*s2)*t2 - (23 + 9*s2)*Power(t2,2)) + 
            s*t2*(-7 + 22*t2 + 3*Power(s2,2)*t2 + 17*s2*(1 + t2))) - 
         Power(t1,6)*(5 - 2*(-50 + s2)*t2 + 
            (404 + 54*s2 - 5*Power(s2,2))*Power(t2,2) + 
            (215 + 555*s2 - 94*Power(s2,2))*Power(t2,3) + 
            (-12 + 68*s2 + 26*Power(s2,2) - 20*Power(s2,3))*
             Power(t2,4) - (73 + 52*s2 + 33*Power(s2,2) + 
               11*Power(s2,3))*Power(t2,5) + (1 + s2)*Power(t2,6) + 
            Power(s,3)*t2*(20 + 67*t2 - 209*Power(t2,2) - 
               232*Power(t2,3) + 11*Power(t2,4)) - 
            s*(6 + (117 + 5*s2)*t2 + 
               (760 + 184*s2 - 7*Power(s2,2))*Power(t2,2) + 
               (1775 + 783*s2 - 77*Power(s2,2))*Power(t2,3) + 
               (616 + 335*s2 + 18*Power(s2,2))*Power(t2,4) - 
               (215 + 143*s2 + 33*Power(s2,2))*Power(t2,5) + Power(t2,6)\
) + Power(s,2)*t2*(2*(-13 + 69*t2 + 705*Power(t2,2) + 425*Power(t2,3) - 
                  55*Power(t2,4)) + 
               s2*(6 + 122*t2 + 326*Power(t2,2) + 270*Power(t2,3) - 
                  33*Power(t2,4)))) + 
         Power(t1,7)*(Power(s,3)*t2*
             (-15 - 148*t2 - 122*Power(t2,2) + 44*Power(t2,3)) + 
            Power(s,2)*(6 + (143 - 2*s2)*t2 + 
               (620 - 24*s2)*Power(t2,2) + (250 - 48*s2)*Power(t2,3) - 
               2*(117 + 49*s2)*Power(t2,4) + 2*Power(t2,5)) + 
            s*(-9 + 2*(-94 + 9*s2)*t2 + 
               (-587 + 165*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (19 + 220*s2 + 83*Power(s2,2))*Power(t2,3) + 
               (300 + 214*s2 + 64*Power(s2,2))*Power(t2,4) - 
               (7 + 4*s2)*Power(t2,5)) + 
            t2*(43 + 171*t2 - 33*Power(t2,2) + 
               2*Power(s2,3)*(1 - 5*t2)*Power(t2,2) - 46*Power(t2,3) + 
               5*Power(t2,4) + 
               2*Power(s2,2)*t2*
                (-1 - 51*t2 - 21*Power(t2,2) + Power(t2,3)) + 
               s2*(-11 - 133*t2 - 36*Power(t2,2) - 58*Power(t2,3) + 
                  2*Power(t2,4)))) - 
         8*Power(t2,3)*(2*Power(s2,2)*Power(t2,2)*
             (9 + 35*t2 + 7*Power(t2,2) + 5*Power(t2,3)) + 
            Power(s2,3)*t2*(1 - 7*t2 - 11*Power(t2,2) + 7*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s,3)*(t2 - 3*Power(t2,2) - 7*Power(t2,3) + 
               3*Power(t2,4) - 2*Power(t2,5)) + 
            s2*(9 + 41*t2 + 118*Power(t2,2) - 36*Power(t2,3) - 
               45*Power(t2,4) - 21*Power(t2,5) - 2*Power(t2,6)) - 
            2*(3 + 9*t2 + 41*Power(t2,2) - 32*Power(t2,3) - 
               29*Power(t2,4) + 7*Power(t2,5) + Power(t2,6)) + 
            s*(9 + (37 - 8*s2 - 9*Power(s2,2))*t2 + 
               (190 - 236*s2 + 35*Power(s2,2))*Power(t2,2) + 
               (-16 - 168*s2 + 7*Power(s2,2))*Power(t2,3) - 
               (89 + 68*s2 + 19*Power(s2,2))*Power(t2,4) - 
               (5 + 32*s2 + 6*Power(s2,2))*Power(t2,5) + 2*Power(t2,6)) \
+ Power(s,2)*t2*(2*t2*(-23 + 13*t2 + 23*Power(t2,2) + 11*Power(t2,3)) + 
               3*s2*(-3 + 13*t2 + 9*Power(t2,2) + 3*Power(t2,3) + 
                  2*Power(t2,4)))) - 
         2*Power(t1,4)*t2*(-25 + 5*(-37 + 2*s2)*t2 + 
            (-605 + 31*s2 + 15*Power(s2,2))*Power(t2,2) - 
            (64 + 477*s2 - 270*Power(s2,2) + 6*Power(s2,3))*
             Power(t2,3) + (251 + 100*s2 + 127*Power(s2,2) + 
               36*Power(s2,3))*Power(t2,4) + 
            (121 + 69*s2 + 64*Power(s2,2) + 20*Power(s2,3))*
             Power(t2,5) - 5*(1 + s2)*Power(t2,6) + 
            Power(s,3)*t2*(-30 + 15*t2 + 278*Power(t2,2) + 
               143*Power(t2,3) - 20*Power(t2,4)) + 
            s*(30 + 5*(54 + 5*s2)*t2 + 
               (1423 + 151*s2 - 27*Power(s2,2))*Power(t2,2) + 
               (2059 + 274*s2 - 160*Power(s2,2))*Power(t2,3) - 
               (56 + 279*s2 + 99*Power(s2,2))*Power(t2,4) - 
               3*(121 + 89*s2 + 20*Power(s2,2))*Power(t2,5) + 
               5*Power(t2,6)) + 
            Power(s,2)*t2*(25 - 556*t2 - 1766*Power(t2,2) - 
               382*Power(t2,3) + 203*Power(t2,4) + 
               2*s2*(-15 - 78*t2 - 86*Power(t2,2) - 40*Power(t2,3) + 
                  30*Power(t2,4)))) + 
         2*Power(t1,2)*Power(t2,2)*
          (-75 - (240 - 41*s2 + 33*Power(s2,2) + 2*Power(s2,3))*t2 + 
            (-753 + 457*s2 + 62*Power(s2,2) - 19*Power(s2,3))*
             Power(t2,2) + (268 - 264*s2 + 620*Power(s2,2) - 
               5*Power(s2,3))*Power(t2,3) + 
            (567 + 168*s2 + 282*Power(s2,2) + 43*Power(s2,3))*
             Power(t2,4) + (116 - s2 + 77*Power(s2,2) + 23*Power(s2,3))*
             Power(t2,5) - (11 + 17*s2)*Power(t2,6) + 
            Power(s,3)*t2*(-20 + 93*t2 + 139*Power(t2,2) + 
               43*Power(t2,3) - 23*Power(t2,4)) + 
            s*(90 + (493 + 118*s2)*t2 + 
               (1939 - 448*s2 + 19*Power(s2,2))*Power(t2,2) + 
               (1452 - 980*s2 - 299*Power(s2,2))*Power(t2,3) - 
               (958 + 952*s2 + 171*Power(s2,2))*Power(t2,4) - 
               (377 + 330*s2 + 69*Power(s2,2))*Power(t2,5) + 
               17*Power(t2,6)) + 
            Power(s,2)*t2*(-45 - 886*t2 - 1232*Power(t2,2) + 
               294*Power(t2,3) + 253*Power(t2,4) + 
               s2*(-90 - 61*t2 + 245*Power(t2,2) + 85*Power(t2,3) + 
                  69*Power(t2,4)))) + 
         Power(s1,9)*(Power(t1,5)*(-165 + 89*s - 16*t2) + 
            Power(t2,2)*(-1 + (-1 + s)*t2) + 
            Power(t1,4)*(45 - 27*Power(s,2) + s*(30 - 64*t2) + 86*t2 + 
               10*Power(t2,2)) - 
            Power(s2,3)*(3 + 2*Power(t1,2)*(-1 + t2) + 2*t2 + 
               t1*(-9 + 2*t2)) + 
            Power(t1,3)*(17 + 222*t2 + 31*Power(t2,2) - Power(t2,3) + 
               Power(s,2)*(2 + 8*t2) + s*(3 - 104*t2 + 2*Power(t2,2))) + 
            t1*t2*(2*Power(s,2)*t2 - t2*(29 + 9*t2) + 
               s*(-2 + 8*t2 + 6*Power(t2,2))) + 
            Power(s2,2)*(-3 - t2 + 4*s*t2 - 9*Power(t1,3)*t2 + 
               Power(t2,2) + 
               Power(t1,2)*(1 + 4*s*(-3 + t2) + 6*Power(t2,2)) + 
               t1*(-7 + 4*s*t2 + 6*Power(t2,2))) + 
            Power(t1,2)*(8 + 2*t2 - 36*Power(t2,2) - 9*Power(t2,3) + 
               Power(s,2)*(2 + 11*t2 + 4*Power(t2,2)) + 
               s*(-8 - 15*t2 + 8*Power(t2,2) + 3*Power(t2,3))) + 
            s2*(112*Power(t1,5) - Power(t2,2)*(s + 4*t2) - 
               Power(t1,4)*(51 + 29*s + 95*t2) - 
               t1*(2 + s - 18*t2 + s*t2 + 2*Power(s,2)*t2 - 
                  34*Power(t2,2) + 12*s*Power(t2,2) + 12*Power(t2,3)) + 
               Power(t1,3)*(-59 + Power(s,2) - 172*t2 + 
                  28*Power(t2,2) + s*(20 + 22*t2)) + 
               Power(t1,2)*(2 + Power(s,2)*(1 - 2*t2) + 39*t2 + 
                  100*Power(t2,2) - 4*Power(t2,3) + 
                  s*(3 + 15*t2 - 11*Power(t2,2))))) + 
         Power(s1,10)*(Power(s2,3) - 
            Power(s2,2)*t1*(-1 + s + t2 + 2*t1*t2) + 
            s2*(44*Power(t1,4) + Power(t2,2) + 2*t1*t2*(s + 4*t2) - 
               2*Power(t1,3)*(3 + 4*s + 15*t2) + 
               Power(t1,2)*(-8 - 32*t2 + 6*Power(t2,2) + s*(3 + 5*t2))) \
+ t1*(12*Power(t1,4) - Power(s,2)*t1*(3*t1 + t2) + 
               Power(t1,2)*t2*(-2 + 3*t2) + t2*(2 + 3*t2) - 
               2*Power(t1,3)*(26 + 7*t2) + t1*t2*(25 + 9*t2) + 
               s*(20*Power(t1,3) - 3*Power(t1,2)*(-1 + t2) - 
                  2*Power(t2,2) + t1*(1 - 7*t2 - 3*Power(t2,2))))) - 
         Power(s1,8)*(42*Power(t1,7) + 
            Power(t1,6)*(290 - 231*s - 30*t2) + 
            Power(t1,5)*(-211 + 109*Power(s,2) - 395*t2 - Power(t2,2) + 
               4*s*(-31 + 70*t2)) + 
            Power(s2,3)*(-2 + 7*t2 - Power(t2,2) + 
               Power(t1,3)*(-12 + 11*t2) + 
               t1*(19 + 16*t2 - 7*Power(t2,2)) - 
               2*Power(t1,2)*(15 - 8*t2 + 3*Power(t2,2))) + 
            Power(t2,2)*(Power(s,2)*t2 - t2*(11 + 3*t2) + 
               s*(-1 + 3*t2 + 3*Power(t2,2))) - 
            Power(t1,4)*(96 + Power(s,3) + 722*t2 - 68*Power(t2,2) - 
               2*Power(t2,3) + Power(s,2)*(9 + 96*t2) + 
               s*(-41 - 563*t2 + 90*Power(t2,2))) + 
            Power(t1,3)*(-67 + 271*t2 + 501*Power(t2,2) + 
               30*Power(t2,3) + Power(s,3)*(-2 + 3*t2) + 
               Power(s,2)*(-10 - 128*t2 + 9*Power(t2,2)) + 
               s*(78 + 124*t2 - 294*Power(t2,2) + 3*Power(t2,3))) + 
            Power(t1,2)*(2 + 70*t2 + 364*Power(t2,2) + 5*Power(t2,3) - 
               3*Power(t2,4) + Power(s,3)*t2*(-3 + 2*t2) + 
               Power(s,2)*t2*(14 + 28*t2 + 5*Power(t2,2)) + 
               s*(-28 + 30*t2 - 223*Power(t2,2) + Power(t2,3) + 
                  Power(t2,4))) + 
            t1*(-1 + 17*t2 + 4*Power(t2,2) + Power(s,3)*Power(t2,2) - 
               54*Power(t2,3) - 9*Power(t2,4) + 
               Power(s,2)*t2*(5 + 9*t2 + 9*Power(t2,2)) + 
               s*(-2 - 11*t2 - 23*Power(t2,2) + 7*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s2,2)*(-9 + (-13 + 12*s)*t2 + 7*s*Power(t2,2) + 
               4*Power(t2,3) + Power(t1,4)*(1 + 9*t2) + 
               Power(t1,3)*(17 + s*(53 - 21*t2) - 26*t2 - 
                  14*Power(t2,2)) + 
               Power(t1,2)*(41 - 13*t2 - 30*Power(t2,2) + 
                  6*Power(t2,3) + s*(-8 - 35*t2 + 14*Power(t2,2))) + 
               t1*(22 - 12*t2 + Power(t2,2) + 12*Power(t2,3) + 
                  s*(-1 - 50*t2 + 16*Power(t2,2)))) + 
            s2*(-3 - 182*Power(t1,6) + (-2 + 5*s)*t2 + 
               (10 + 2*s - 5*Power(s,2))*Power(t2,2) + 
               (12 - 7*s)*Power(t2,3) - 6*Power(t2,4) + 
               Power(t1,5)*(183 + 63*s + 164*t2) - 
               Power(t1,4)*(-171 + 8*Power(s,2) - 471*t2 + 
                  40*Power(t2,2) + s*(63 + 13*t2)) + 
               Power(t1,3)*(-15 - 347*t2 - 400*Power(t2,2) + 
                  6*Power(t2,3) + Power(s,2)*(-2 + 8*t2) + 
                  3*s*(-14 - 14*t2 + 5*Power(t2,2))) + 
               t1*(-14 + 4*t2 + Power(s,2)*(3 - 10*t2)*t2 + 
                  64*Power(t2,2) + 110*Power(t2,3) - 8*Power(t2,4) + 
                  2*s*(-6 + 21*t2 + Power(t2,2) - 12*Power(t2,3))) + 
               Power(t1,2)*(4 - 222*t2 - 229*Power(t2,2) + 
                  102*Power(t2,3) - Power(t2,4) + 
                  Power(s,2)*(1 + 25*t2 - 10*Power(t2,2)) + 
                  s*(14 + 47*t2 + 74*Power(t2,2) - 11*Power(t2,3))))) + 
         Power(s1,7)*(-1 - 84*Power(t1,8) - t2 + s*t2 + 9*Power(t2,2) - 
            3*s*Power(t2,2) + 2*Power(t2,3) - 11*s*Power(t2,3) + 
            Power(s,2)*Power(t2,3) + 2*Power(s,3)*Power(t2,3) - 
            22*Power(t2,4) + 2*s*Power(t2,4) + 
            5*Power(s,2)*Power(t2,4) - 3*Power(t2,5) + 3*s*Power(t2,5) + 
            5*Power(t1,7)*(-61 + 77*s + 22*t2) + 
            Power(t1,6)*(448 - 259*Power(s,2) + 832*t2 - 
               40*Power(t2,2) - 7*s*(-41 + 90*t2)) + 
            Power(t1,5)*(218 + 8*Power(s,3) + 1149*t2 - 
               505*Power(t2,2) + 3*Power(t2,3) + 
               Power(s,2)*(9 + 366*t2) + 
               s*(-259 - 1614*t2 + 331*Power(t2,2))) - 
            Power(s2,3)*(6*Power(t1,4)*(-5 + 4*t2) + 
               Power(t1,3)*(-50 + 46*t2 - 23*Power(t2,2)) + 
               t2*(-29 - 25*t2 + Power(t2,2)) + 
               Power(t1,2)*(50 + 66*t2 - 55*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-10 + 53*t2 - 25*Power(t2,2) + 9*Power(t2,3))) + 
            Power(t1,4)*(212 + Power(s,3)*(14 - 25*t2) - 1455*t2 - 
               1870*Power(t2,2) + 46*Power(t2,3) + 
               Power(s,2)*(36 + 590*t2 - 147*Power(t2,2)) - 
               s*(348 + 389*t2 - 1517*Power(t2,2) + 61*Power(t2,3))) + 
            Power(t1,3)*(-56 - 487*t2 - 1121*Power(t2,2) + 
               482*Power(t2,3) + 8*Power(t2,4) + 
               Power(s,3)*t2*(4 + t2) + 
               Power(s,2)*(12 - 115*t2 - 434*Power(t2,2) + 
                  8*Power(t2,3)) + 
               2*s*(54 + 130*t2 + 690*Power(t2,2) - 197*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(t1,2)*(3 - 246*t2 + 560*Power(t2,2) + 
               885*Power(t2,3) + 29*Power(t2,4) + 
               Power(s,3)*t2*(-7 + 4*Power(t2,2)) + 
               s*(-25 + 307*t2 + 194*Power(t2,2) - 520*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s,2)*(-13 + 11*t2 - 238*Power(t2,2) + 
                  24*Power(t2,3) + 2*Power(t2,4))) + 
            t1*(-7 + 4*t2 + 91*Power(t2,2) + 270*Power(t2,3) - 
               27*Power(t2,4) - 3*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(-7 + 6*t2) + 
               Power(s,2)*t2*
                (16 - 11*t2 + 39*Power(t2,2) + 10*Power(t2,3)) + 
               s*(-12 - 24*t2 + 47*Power(t2,2) - 216*Power(t2,3) - 
                  4*Power(t2,4) + 2*Power(t2,5))) + 
            Power(s2,2)*(-6 + 2*(8 + 7*s)*t2 - 
               2*(5 + 19*s)*Power(t2,2) + (1 + 6*s)*Power(t2,3) + 
               6*Power(t2,4) + Power(t1,5)*(-6 + 23*t2) + 
               t1*(58 + (139 - 99*s)*t2 - (30 + 83*s)*Power(t2,2) + 
                  (-37 + 27*s)*Power(t2,3) + 10*Power(t2,4)) + 
               Power(t1,4)*(-69 + 92*t2 - 29*Power(t2,2) + 
                  s*(-122 + 41*t2)) - 
               Power(t1,3)*(98 - 62*t2 + 4*Power(t2,2) + Power(t2,3) + 
                  s*(-42 - 100*t2 + 47*Power(t2,2))) + 
               Power(t1,2)*(-67 + 178*t2 - 141*Power(t2,2) - 
                  49*Power(t2,3) + 2*Power(t2,4) + 
                  s*(5 + 243*t2 - 121*Power(t2,2) + 16*Power(t2,3)))) + 
            s2*(-9 + 196*Power(t1,7) + 4*(-5 + 3*s)*t2 + 
               (2 + 45*s - 13*Power(s,2))*Power(t2,2) + 
               (31 - 5*s - 8*Power(s,2))*Power(t2,3) + 
               (40 - 13*s)*Power(t2,4) - 4*Power(t2,5) - 
               Power(t1,6)*(363 + 91*s + 165*t2) + 
               Power(t1,5)*(-240 + 28*Power(s,2) + s*(127 - 109*t2) - 
                  750*t2 - 11*Power(t2,2)) + 
               Power(t1,4)*(56 - 14*Power(s,2) + 1235*t2 + 
                  774*Power(t2,2) + 26*Power(t2,3) + 
                  s*(200 + 67*t2 + 132*Power(t2,2))) + 
               Power(t1,3)*(66 + 676*t2 + 719*Power(t2,2) - 
                  192*Power(t2,3) - 2*Power(t2,4) + 
                  Power(s,2)*(-4 - 94*t2 + 23*Power(t2,2)) - 
                  s*(78 + 219*t2 + 6*Power(t2,2) + 7*Power(t2,3))) + 
               t1*(17 - 14*t2 - 265*Power(t2,2) - 118*Power(t2,3) + 
                  120*Power(t2,4) - 2*Power(t2,5) + 
                  Power(s,2)*t2*(16 + 63*t2 - 24*Power(t2,2)) + 
                  s*(-17 + 27*t2 + 44*Power(t2,2) + 74*Power(t2,3) - 
                     20*Power(t2,4))) + 
               Power(t1,2)*(85 - 106*t2 - 845*Power(t2,2) - 
                  617*Power(t2,3) + 32*Power(t2,4) + 
                  Power(s,2)*t2*(-4 + 69*t2 - 14*Power(t2,2)) + 
                  s*(75 - 387*t2 + 176*Power(t2,2) + 63*Power(t2,3) - 
                     4*Power(t2,4))))) + 
         Power(s1,3)*(-2*Power(t1,12) + 
            Power(t1,11)*(41*s + 2*s2 + 5*t2) + 
            Power(t1,10)*(1 - 129*Power(s,2) + 
               s*(129 - 8*s2 - 115*t2) + s2*(-33 + t2) + 75*t2 - 
               4*Power(t2,2)) + 
            2*t2*(10 - 5*(-5 + 26*s2 + 18*Power(s2,2) + s*(-1 + 6*s2))*
                t2 + (102 + 4*Power(s,3) - 235*s2 - 198*Power(s2,2) + 
                  15*Power(s2,3) + 5*Power(s,2)*(-2 + 3*s2) + 
                  2*s*(-59 + 56*s2 + 115*Power(s2,2)))*Power(t2,2) + 
               (74 + 3*Power(s,3) + Power(s,2)*(169 - 242*s2) + 
                  159*s2 - 335*Power(s2,2) + 100*Power(s2,3) + 
                  s*(-449 + 670*s2 + 27*Power(s2,2)))*Power(t2,3) + 
               (-402 + 6*Power(s,3) + 361*s2 + 43*Power(s2,2) - 
                  21*Power(s2,3) - 5*Power(s,2)*(13 + 21*s2) + 
                  s*(285 + 14*s2 + 96*Power(s2,2)))*Power(t2,4) + 
               (-107 + 3*Power(s,3) + 103*s2 - 23*Power(s2,2) - 
                  6*Power(s2,3) - Power(s,2)*(113 + 12*s2) + 
                  s*(184 + 80*s2 + 15*Power(s2,2)))*Power(t2,5) + 
               (42 + 3*Power(s,2) - 2*s2 + 3*Power(s2,2) - 
                  s*(35 + 6*s2))*Power(t2,6)) - 
            2*Power(t1,2)*(10 + 
               (70 + 24*Power(s,2) - 151*s2 - 72*Power(s2,2) + 
                  s*(29 + 6*s2))*t2 + 
               (453 + 58*Power(s,3) + Power(s,2)*(87 - 180*s2) - 
                  258*s2 - 100*Power(s2,2) + 84*Power(s2,3) + 
                  s*(-490 + 61*s2 + 242*Power(s2,2)))*Power(t2,2) + 
               (429 + 136*Power(s,3) + Power(s,2)*(522 - 453*s2) + 
                  409*s2 - 1037*Power(s2,2) + 339*Power(s2,3) + 
                  s*(-3400 + 3651*s2 - 1150*Power(s2,2)))*Power(t2,3) + 
               (-5603 - 248*Power(s,3) + 2955*s2 + 93*Power(s2,2) - 
                  226*Power(s2,3) - 2*Power(s,2)*(247 + 63*s2) + 
                  s*(4910 + 429*s2 + 468*Power(s2,2)))*Power(t2,4) + 
               (-271 + 102*Power(s,3) + 1492*s2 - 693*Power(s2,2) - 
                  49*Power(s2,3) - 253*Power(s,2)*(8 + s2) + 
                  s*(2737 + 2123*s2 + 200*Power(s2,2)))*Power(t2,5) + 
               (840 + 69*Power(s,2) + 345*s2 + 69*Power(s2,2) - 
                  2*s*(389 + 69*s2))*Power(t2,6)) + 
            Power(t1,9)*(10 + 56*Power(s,3) + 11*t2 - 171*Power(t2,2) + 
               Power(t2,3) + 3*Power(s2,2)*(-2 + 7*t2) + 
               Power(s,2)*(-121 + 28*s2 + 386*t2) + 
               s2*(34 + 24*t2 - 15*Power(t2,2)) + 
               s*(-251 + s2*(53 - 167*t2) - 958*t2 + 115*Power(t2,2))) + 
            Power(t1,8)*(282 + Power(s,3)*(22 - 203*t2) - 591*t2 - 
               752*Power(t2,2) + 124*Power(t2,3) + 
               Power(s2,3)*(2 + 4*t2) + 
               Power(s2,2)*(-27 + 36*t2 - 71*Power(t2,2)) + 
               s2*(24 + 589*t2 - 94*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s,2)*(410 + 1538*t2 - 419*Power(t2,2) + 
                  2*s2*(-49 + 86*t2)) - 
               s*(836 + 559*t2 - 1943*Power(t2,2) + 47*Power(t2,3) + 
                  Power(s2,2)*(18 + 29*t2) + 
                  s2*(-262 + 103*t2 - 390*Power(t2,2)))) + 
            Power(t1,7)*(-458 - 223*t2 + 735*Power(t2,2) + 
               1520*Power(t2,3) - 28*Power(t2,4) - 
               7*Power(s2,3)*t2*(-2 + 7*t2) + 
               Power(s,3)*(-60 - 312*t2 + 263*Power(t2,2)) + 
               Power(s2,2)*(36 + 42*t2 - 362*Power(t2,2) + 
                  89*Power(t2,3)) + 
               s2*(156 - 1172*t2 - 225*Power(t2,2) + 272*Power(t2,3) - 
                  6*Power(t2,4)) - 
               Power(s,2)*(-308 + 705*t2 + 3594*Power(t2,2) - 
                  196*Power(t2,3) + s2*(6 + 170*t2 + 485*Power(t2,2))) \
+ s*(438 + 4560*t2 + 5234*Power(t2,2) - 1642*Power(t2,3) + 
                  6*Power(t2,4) + 
                  Power(s2,2)*(18 - 120*t2 + 271*Power(t2,2)) + 
                  s2*(-344 + 175*t2 + 1684*Power(t2,2) - 
                     285*Power(t2,3)))) - 
            2*Power(t1,3)*(14 + 367*t2 + 2894*Power(t2,2) - 
               246*Power(t2,3) - 5056*Power(t2,4) - 3031*Power(t2,5) + 
               98*Power(t2,6) + 
               Power(s2,2)*t2*
                (166 + 264*t2 - 679*Power(t2,2) + 488*Power(t2,3) - 
                  579*Power(t2,4)) + 
               Power(s2,3)*t2*
                (16 + 66*t2 + 115*Power(t2,2) - 38*Power(t2,3) - 
                  25*Power(t2,4)) + 
               Power(s,3)*t2*
                (16 + 67*t2 + 111*Power(t2,2) - 493*Power(t2,3) + 
                  25*Power(t2,4)) + 
               s2*(-6 - 149*t2 - 1132*Power(t2,2) + 2717*Power(t2,3) + 
                  8*Power(t2,4) - 134*Power(t2,5) + 104*Power(t2,6)) - 
               s*(6 + (91 + 8*s2 - 6*Power(s2,2))*t2 + 
                  (3886 - 2129*s2 + 923*Power(s2,2))*Power(t2,2) + 
                  (8779 + 90*s2 + 161*Power(s2,2))*Power(t2,3) + 
                  (1534 + 1005*s2 + 187*Power(s2,2))*Power(t2,4) - 
                  (3748 + 1606*s2 + 75*Power(s2,2))*Power(t2,5) + 
                  104*Power(t2,6)) + 
               Power(s,2)*t2*
                (-164 - 163*t2 + 3315*Power(t2,2) + 3899*Power(t2,3) - 
                  1027*Power(t2,4) + 
                  s2*(6 + 62*t2 + 331*Power(t2,2) + 718*Power(t2,3) - 
                     75*Power(t2,4)))) - 
            2*t1*t2*(70 + 65*t2 - 985*Power(t2,2) - 579*Power(t2,3) + 
               1363*Power(t2,4) + 910*Power(t2,5) - 12*Power(t2,6) + 
               Power(s,3)*t2*
                (-6 - 63*t2 + 22*Power(t2,2) + 57*Power(t2,3) - 
                  10*Power(t2,4)) + 
               Power(s2,3)*t2*
                (-30 - 69*t2 - 138*Power(t2,2) + 83*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-255 - 418*t2 + 351*Power(t2,2) - 396*Power(t2,3) + 
                  94*Power(t2,4)) + 
               s2*(90 + 278*t2 + 966*Power(t2,2) - 563*Power(t2,3) - 
                  369*Power(t2,4) - 79*Power(t2,5) - 19*Power(t2,6)) + 
               s*(-30 - 2*(-6 + 53*s2 + 45*Power(s2,2))*t2 + 
                  (1178 - 1738*s2 + 963*Power(s2,2))*Power(t2,2) + 
                  (1479 - 292*s2 + 506*Power(s2,2))*Power(t2,3) + 
                  (405 + 670*s2 - 197*Power(s2,2))*Power(t2,4) - 
                  (847 + 294*s2 + 30*Power(s2,2))*Power(t2,5) + 
                  19*Power(t2,6)) + 
               Power(s,2)*t2*
                (137 - 12*t2 - 331*Power(t2,2) - 954*Power(t2,3) + 
                  200*Power(t2,4) + 
                  s2*(30 + 33*t2 - 390*Power(t2,2) + 57*Power(t2,3) + 
                     30*Power(t2,4)))) + 
            Power(t1,6)*(-39 - 1662*t2 + 5400*Power(t2,2) + 
               2551*Power(t2,3) - 921*Power(t2,4) + 
               Power(s2,3)*(-4 - 86*t2 + 11*Power(t2,2) + 
                  78*Power(t2,3)) - 
               Power(s,3)*(30 + 27*t2 - 966*Power(t2,2) + 
                  142*Power(t2,3)) + 
               s2*(-211 + 362*t2 - 2703*Power(t2,2) + 
                  121*Power(t2,3) - 262*Power(t2,4)) + 
               Power(s2,2)*(11 + 98*t2 + 309*Power(t2,2) + 
                  737*Power(t2,3) - 34*Power(t2,4)) + 
               Power(s,2)*(-209 - 2625*t2 - 3896*Power(t2,2) + 
                  3314*Power(t2,3) - 34*Power(t2,4) + 
                  s2*(50 + 456*t2 - 635*Power(t2,2) + 362*Power(t2,3))) \
+ s*(631 + 4987*t2 - 3456*Power(t2,2) - 9042*Power(t2,3) + 
                  606*Power(t2,4) + 
                  Power(s2,2)*
                   (1 + 523*t2 + 3*Power(t2,2) - 298*Power(t2,3)) + 
                  s2*(-57 - 2279*t2 - 982*Power(t2,2) - 
                     3237*Power(t2,3) + 68*Power(t2,4)))) + 
            Power(t1,5)*(235 + 3224*t2 + 265*Power(t2,2) - 
               5762*Power(t2,3) - 5041*Power(t2,4) + 151*Power(t2,5) + 
               Power(s2,2)*(-8 + 71*t2 - 438*Power(t2,2) + 
                  987*Power(t2,3) - 684*Power(t2,4)) + 
               Power(s2,3)*(2 + t2 + 41*Power(t2,2) + 
                  149*Power(t2,3) - 26*Power(t2,4)) + 
               Power(s,3)*(20 + 204*t2 + 485*Power(t2,2) - 
                  1066*Power(t2,3) + 26*Power(t2,4)) + 
               Power(s,2)*(-170 - 2*(397 + 4*s2)*t2 + 
                  (5507 + 427*s2)*Power(t2,2) + 
                  5*(1867 + 376*s2)*Power(t2,3) - 
                  2*(664 + 39*s2)*Power(t2,4)) + 
               s2*(-55 - 860*t2 + 4975*Power(t2,2) - 18*Power(t2,3) - 
                  912*Power(t2,4) + 80*Power(t2,5)) + 
               s*(-86 - 3962*t2 - 17213*Power(t2,2) - 
                  8184*Power(t2,3) + 6412*Power(t2,4) - 
                  80*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-301 + 281*t2 - 963*Power(t2,2) + 78*Power(t2,3)) + 
                  s2*(129 + 1797*t2 - 912*Power(t2,2) - 
                     3622*Power(t2,3) + 2012*Power(t2,4)))) + 
            Power(t1,4)*(19 + 131*t2 + 2671*Power(t2,2) - 
               14406*Power(t2,3) - 2368*Power(t2,4) + 2297*Power(t2,5) + 
               Power(s2,3)*t2*
                (87 + 445*t2 - 279*Power(t2,2) - 234*Power(t2,3)) + 
               2*Power(s,3)*t2*
                (47 + 26*t2 - 636*Power(t2,2) + 220*Power(t2,3)) + 
               Power(s2,2)*(-6 - 806*Power(t2,2) - 1085*Power(t2,3) - 
                  1890*Power(t2,4) + 188*Power(t2,5)) + 
               Power(s,2)*(48 + (684 - 128*s2)*t2 - 
                  7*(-542 + 99*s2)*Power(t2,2) + 
                  15*(97 + 20*s2)*Power(t2,3) - 
                  (7123 + 1114*s2)*Power(t2,4) + 188*Power(t2,5)) + 
               s2*(71 + 464*t2 - 1638*Power(t2,2) + 6201*Power(t2,3) + 
                  1934*Power(t2,4) + 904*Power(t2,5)) + 
               s*(-160 - 2003*t2 - 8773*Power(t2,2) + 
                  14091*Power(t2,3) + 13534*Power(t2,4) - 
                  1977*Power(t2,5) + 
                  2*Power(s2,2)*t2*
                   (-30 - 1149*t2 + 332*Power(t2,2) + 454*Power(t2,3)) + 
                  s2*(-24 + 184*t2 + 7265*Power(t2,2) + 
                     3717*Power(t2,3) + 7143*Power(t2,4) - 
                     376*Power(t2,5))))) + 
         Power(s1,2)*(5*s*Power(t1,12) + 
            2*t1*t2*(30 + 3*(50 + 18*Power(s,2) - 47*s2 - 
                  24*Power(s2,2) - 2*s*(1 + 9*s2))*t2 - 
               (-349 + 5*Power(s,3) + 334*s2 + 163*Power(s2,2) - 
                  34*Power(s2,3) + Power(s,2)*(27 + 100*s2) + 
                  s*(421 - 54*s2 - 287*Power(s2,2)))*Power(t2,2) + 
               (-98 + 178*Power(s,3) + Power(s,2)*(338 - 661*s2) + 
                  285*s2 - 882*Power(s2,2) + 239*Power(s2,3) + 
                  s*(-1887 + 2472*s2 - 412*Power(s2,2)))*Power(t2,3) - 
               (2534 + 69*Power(s,3) - 1295*s2 + 84*Power(s2,2) + 
                  132*Power(s2,3) + 2*Power(s,2)*(426 + 61*s2) - 
                  s*(2983 + 696*s2 + 251*Power(s2,2)))*Power(t2,4) + 
               (484 + 32*Power(s,3) + 804*s2 - 390*Power(s2,2) - 
                  29*Power(s2,3) - Power(s,2)*(1016 + 93*s2) + 
                  s*(793 + 1078*s2 + 90*Power(s2,2)))*Power(t2,5) + 
               (435 + 39*Power(s,2) + 171*s2 + 39*Power(s2,2) - 
                  2*s*(203 + 39*s2))*Power(t2,6)) - 
            2*Power(t1,3)*(-10 + 
               (-14 + 72*Power(s,2) + 79*s2 - 24*Power(s2,2) - 
                  s*(245 + 6*s2))*t2 + 
               (-243 + 36*Power(s,3) + Power(s,2)*(617 - 62*s2) + 
                  62*s2 - 28*Power(s2,2) + 50*Power(s2,3) + 
                  s*(-1242 + 275*s2 - 60*Power(s2,2)))*Power(t2,2) - 
               (-1024 + 46*Power(s,3) + 2132*s2 + 519*Power(s2,2) - 
                  253*Power(s2,3) + Power(s,2)*(-1472 + 359*s2) + 
                  3*s*(843 - 1657*s2 + 464*Power(s2,2)))*Power(t2,3) - 
               (5440 + 454*Power(s,3) - 1942*s2 + 897*Power(s2,2) + 
                  188*Power(s2,3) + 2*Power(s,2)*(887 + 120*s2) - 
                  s*(9231 + 2875*s2 + 574*Power(s2,2)))*Power(t2,4) + 
               (806 + 316*Power(s,3) + 1163*s2 - 771*Power(s2,2) - 
                  155*Power(s2,3) - Power(s,2)*(3358 + 787*s2) + 
                  s*(3828 + 3111*s2 + 626*Power(s2,2)))*Power(t2,5) + 
               (1013 + 143*Power(s,2) + 390*s2 + 143*Power(s2,2) - 
                  s*(987 + 286*s2))*Power(t2,6)) - 
            Power(t1,11)*(7 + 34*Power(s,2) + 3*s2 - 7*t2 + 
               s*(-37 + s2 + 15*t2)) + 
            Power(t1,10)*(-1 + 28*Power(s,3) + 15*t2 - 
               s2*(-15 + t2)*t2 - 17*Power(t2,2) + 
               Power(s2,2)*(-1 + 3*t2) + 
               Power(s,2)*(-66 + 8*s2 + 111*t2) - 
               2*s*(12 + 117*t2 - 8*Power(t2,2) + s2*(-6 + 22*t2))) - 
            Power(t1,9)*(-117 + 18*t2 - Power(s2,3)*t2 + 
               142*Power(t2,2) - 13*Power(t2,3) + 
               Power(s,3)*(1 + 105*t2) + 
               Power(s2,2)*(4 - 17*t2 + 14*Power(t2,2)) + 
               Power(s,2)*(-211 + s2*(34 - 85*t2) - 625*t2 + 
                  132*Power(t2,2)) - 
               s2*(7 + 128*t2 - 42*Power(t2,2) + 2*Power(t2,3)) + 
               s*(345 + 349*t2 - 478*Power(t2,2) + 7*Power(t2,3) + 
                  Power(s2,2)*(2 + 9*t2) + 
                  s2*(-65 + 98*t2 - 112*Power(t2,2)))) + 
            Power(t1,8)*(-174 - 121*t2 + Power(s2,3)*(4 - 17*t2)*t2 + 
               22*Power(t2,2) + 313*Power(t2,3) - 3*Power(t2,4) + 
               Power(s,3)*(-45 - 181*t2 + 146*Power(t2,2)) + 
               Power(s2,2)*(10 + 3*t2 - 132*Power(t2,2) + 
                  24*Power(t2,3)) - 
               s2*(-54 + 448*t2 + 181*Power(t2,2) - 64*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s,2)*(-154 + 239*t2 + 1533*Power(t2,2) - 
                  67*Power(t2,3) + 2*s2*(2 + 57*t2 + 126*Power(t2,2))) \
+ s*(170 + 1621*t2 + 2055*Power(t2,2) - 427*Power(t2,3) + Power(t2,4) + 
                  Power(s2,2)*(2 - 45*t2 + 123*Power(t2,2)) + 
                  s2*(-113 + 244*t2 + 726*Power(t2,2) - 91*Power(t2,3)))\
) - Power(t1,4)*(65 + 815*t2 + 5716*Power(t2,2) - 1036*Power(t2,3) - 
               7732*Power(t2,4) - 4037*Power(t2,5) + 169*Power(t2,6) + 
               Power(s2,2)*t2*
                (78 + 361*t2 - 1313*Power(t2,2) + 128*Power(t2,3) - 
                  1032*Power(t2,4)) + 
               Power(s2,3)*t2*
                (12 + 60*t2 + 127*Power(t2,2) + 52*Power(t2,3) - 
                  88*Power(t2,4)) + 
               Power(s,3)*t2*
                (60 + 312*t2 + 90*Power(t2,2) - 1523*Power(t2,3) + 
                  88*Power(t2,4)) + 
               s2*(30 - 71*t2 - 1031*Power(t2,2) + 6868*Power(t2,3) - 
                  359*Power(t2,4) - 656*Power(t2,5) + 143*Power(t2,6)) \
- s*(60 + (588 - 187*s2 + 30*Power(s2,2))*t2 + 
                  (9373 - 2409*s2 + 654*Power(s2,2))*Power(t2,2) + 
                  (21486 + 1061*s2 - 260*Power(s2,2))*Power(t2,3) + 
                  (1467 + 832*s2 + 879*Power(s2,2))*Power(t2,4) - 
                  (6689 + 3237*s2 + 264*Power(s2,2))*Power(t2,5) + 
                  143*Power(t2,6)) + 
               Power(s,2)*t2*
                (-470 + 245*t2 + 9877*Power(t2,2) + 7887*Power(t2,3) - 
                  2205*Power(t2,4) + 
                  s2*(60 + 6*t2 + 657*Power(t2,2) + 2350*Power(t2,3) - 
                     264*Power(t2,4)))) + 
            2*Power(t1,2)*t2*
             (42 + 546*t2 + 2893*Power(t2,2) - 1291*Power(t2,3) - 
               4003*Power(t2,4) - 1355*Power(t2,5) + 80*Power(t2,6) + 
               Power(s2,2)*t2*
                (231 + 127*t2 - 850*Power(t2,2) + 3*Power(t2,3) - 
                  519*Power(t2,4)) + 
               Power(s2,3)*t2*
                (18 + 159*t2 + 108*Power(t2,2) - 109*Power(t2,3) - 
                  36*Power(t2,4)) + 
               Power(s,3)*t2*
                (18 + 75*t2 - 44*Power(t2,2) - 345*Power(t2,3) + 
                  36*Power(t2,4)) + 
               s*(-18 + 6*(-27 - 4*s2 + 3*Power(s2,2))*t2 + 
                  (-4782 + 2602*s2 - 1183*Power(s2,2))*Power(t2,2) - 
                  2*(2819 - 65*s2 + 136*Power(s2,2))*Power(t2,3) + 
                  (920 + 386*s2 + 101*Power(s2,2))*Power(t2,4) + 
                  2*(1072 + 733*s2 + 54*Power(s2,2))*Power(t2,5) - 
                  96*Power(t2,6)) + 
               2*s2*(-9 - 126*t2 - 520*Power(t2,2) + 
                  1190*Power(t2,3) + 201*Power(t2,4) + 
                  124*Power(t2,5) + 48*Power(t2,6)) + 
               Power(s,2)*t2*
                (-219 + 155*t2 + 2596*Power(t2,2) + 1759*Power(t2,3) - 
                  947*Power(t2,4) + 
                  s2*(18 + 5*t2 + 376*Power(t2,2) + 353*Power(t2,3) - 
                     108*Power(t2,4)))) - 
            Power(t1,7)*(38 + 906*t2 - 1211*Power(t2,2) - 
               807*Power(t2,3) + 226*Power(t2,4) + 
               Power(s2,3)*t2*(16 + 4*t2 - 41*Power(t2,2)) + 
               Power(s,3)*(10 - 95*t2 - 673*Power(t2,2) + 
                  89*Power(t2,3)) + 
               Power(s2,2)*(-2 + 21*t2 - 175*Power(t2,2) - 
                  255*Power(t2,3) + 12*Power(t2,4)) + 
               s2*(95 - 259*t2 + 626*Power(t2,2) - 324*Power(t2,3) + 
                  58*Power(t2,4)) + 
               Power(s,2)*(177 + 1956*t2 + 2301*Power(t2,2) - 
                  1580*Power(t2,3) + 12*Power(t2,4) - 
                  s2*(25 + 232*t2 - 401*Power(t2,2) + 219*Power(t2,3))) \
+ s*(-375 - 2745*t2 + 940*Power(t2,2) + 3693*Power(t2,3) - 
                  168*Power(t2,4) + 
                  Power(s2,2)*t2*(-155 + 25*t2 + 171*Power(t2,2)) + 
                  s2*(15 + 885*t2 + 370*Power(t2,2) + 
                     1423*Power(t2,3) - 24*Power(t2,4)))) - 
            2*Power(t2,2)*(-45 + 
               (-60 + 11*s - 39*Power(s,2) + 2*Power(s,3))*t2 + 
               (293 - 305*s + 34*Power(s,2) + 15*Power(s,3))*
                Power(t2,2) + 
               3*(48 - 68*s - 40*Power(s,2) + 11*Power(s,3))*
                Power(t2,3) + 
               (-347 - 12*s + 150*Power(s,2) + Power(s,3))*
                Power(t2,4) + 
               (-180 + 161*s - 41*Power(s,2) + 5*Power(s,3))*
                Power(t2,5) - 3*(-1 + s)*Power(t2,6) + 
               Power(s2,3)*t2*
                (20 + 63*t2 + 121*Power(t2,2) - 15*Power(t2,3) - 
                  5*Power(t2,4)) + 
               s2*(-90 + (-281 + 242*s)*t2 + 
                  (-451 + 968*s - 223*Power(s,2))*Power(t2,2) + 
                  (160 + 284*s + 55*Power(s,2))*Power(t2,3) - 
                  11*(-14 + 16*s + 3*Power(s,2))*Power(t2,4) + 
                  (25 + 58*s - 15*Power(s,2))*Power(t2,5) + 
                  3*Power(t2,6)) + 
               Power(s2,2)*t2*
                (45 + 174*t2 - 76*Power(t2,2) + 114*Power(t2,3) - 
                  17*Power(t2,4) + 
                  s*(90 - 271*t2 - 289*Power(t2,2) + 47*Power(t2,3) + 
                     15*Power(t2,4)))) + 
            Power(t1,6)*(Power(s2,3)*t2*
                (-2 + 6*t2 + 93*Power(t2,2) - 20*Power(t2,3)) + 
               Power(s2,2)*(-13 + 16*t2 - 165*Power(t2,2) + 
                  511*Power(t2,3) - 304*Power(t2,4)) + 
               Power(s,3)*(20 + 225*t2 + 294*Power(t2,2) - 
                  884*Power(t2,3) + 20*Power(t2,4)) + 
               Power(s,2)*(-115 + (-515 + 50*s2)*t2 + 
                  (4239 + 470*s2)*Power(t2,2) + 
                  (6011 + 1479*s2)*Power(t2,3) - 
                  (733 + 60*s2)*Power(t2,4)) + 
               s2*(-12 - 297*t2 + 3213*Power(t2,2) + 345*Power(t2,3) - 
                  466*Power(t2,4) + 22*Power(t2,5)) + 
               2*(76 + 903*t2 + 174*Power(t2,2) - 899*Power(t2,3) - 
                  909*Power(t2,4) + 24*Power(t2,5)) + 
               s*(-108 - 2559*t2 - 10778*Power(t2,2) - 
                  5217*Power(t2,3) + 2924*Power(t2,4) - 
                  22*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-49 + 258*t2 - 688*Power(t2,2) + 60*Power(t2,3)) + 
                  s2*(82 + 737*t2 - 1578*Power(t2,2) - 
                     2368*Power(t2,3) + 1037*Power(t2,4)))) + 
            Power(t1,5)*(-4 - 140*t2 + 2362*Power(t2,2) - 
               6437*Power(t2,3) - 1045*Power(t2,4) + 1132*Power(t2,5) + 
               Power(s2,3)*t2*
                (25 + 157*t2 - 79*Power(t2,2) - 229*Power(t2,3)) + 
               Power(s,3)*t2*
                (40 - 309*t2 - 1307*Power(t2,2) + 481*Power(t2,3)) + 
               Power(s2,2)*(6 + 12*t2 + 3*Power(t2,2) - 
                  1505*Power(t2,3) - 1093*Power(t2,4) + 127*Power(t2,5)) \
+ Power(s,2)*(60 + (997 - 85*s2)*t2 + (4946 - 653*s2)*Power(t2,2) + 
                  (637 + 267*s2)*Power(t2,3) - 
                  3*(1866 + 397*s2)*Power(t2,4) + 127*Power(t2,5)) + 
               s2*(79 + 466*t2 - 2406*Power(t2,2) + 1519*Power(t2,3) - 
                  102*Power(t2,4) + 408*Power(t2,5)) + 
               s*(-170 - 2005*t2 - 6258*Power(t2,2) + 
                  11120*Power(t2,3) + 9738*Power(t2,4) - 
                  1053*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-85 - 1157*t2 + 450*Power(t2,2) + 939*Power(t2,3)) + 
                  s2*(-30 + 188*t2 + 4852*Power(t2,2) + 
                     4532*Power(t2,3) + 5129*Power(t2,4) - 
                     254*Power(t2,5))))) + 
         Power(s1,4)*(-15*Power(t1,11) + 
            Power(t1,10)*(-10 + 149*s + 17*s2 + 34*t2) - 
            t1*(10 + 5*(4 - 81*s2 - 36*Power(s2,2) + s*(33 + 6*s2))*
                t2 + (24*Power(s,3) - 5*Power(s,2)*(37 + 36*s2) + 
                  s*(-178 + 147*s2 + 540*Power(s2,2)) + 
                  2*(216 - 405*s2 - 89*Power(s2,2) + 90*Power(s2,3)))*
                Power(t2,2) + 
               (156*Power(s,3) + Power(s,2)*(381 - 615*s2) + 
                  s*(-2627 + 2965*s2 - 862*Power(s2,2)) + 
                  3*(221 + 349*s2 - 426*Power(s2,2) + 155*Power(s2,3))\
)*Power(t2,3) + (-3159 - 54*Power(s,3) + Power(s,2)*(156 - 230*s2) + 
                  2756*s2 + 557*Power(s2,2) - 194*Power(s2,3) + 
                  s*(1466 - 725*s2 + 418*Power(s2,2)))*Power(t2,4) - 
               (1319 + 2*Power(s,3) - 906*s2 + 166*Power(s2,2) + 
                  11*Power(s2,3) + Power(s,2)*(737 + 7*s2) - 
                  s*(1714 + 589*s2 + 20*Power(s2,2)))*Power(t2,5) + 
               (297 + 13*Power(s,2) + 26*s2 + 13*Power(s2,2) - 
                  2*s*(134 + 13*s2))*Power(t2,6)) - 
            Power(t1,9)*(-105 + 287*Power(s,2) + 141*s2 - 321*t2 + 
               25*Power(t2,2) + s*(-280 + 29*s2 + 384*t2)) + 
            Power(t1,8)*(53 + 70*Power(s,3) + 38*t2 - 616*Power(t2,2) + 
               6*Power(t2,3) + Power(s2,2)*(-15 + 61*t2) + 
               2*Power(s,2)*(-69 + 28*s2 + 384*t2) + 
               s2*(81 - 77*t2 - 62*Power(t2,2)) + 
               s*(-652 + s2*(125 - 353*t2) - 2189*t2 + 352*Power(t2,2))) \
+ Power(t1,4)*(185 + 2973*t2 + 1441*Power(t2,2) - 5895*Power(t2,3) - 
               6506*Power(t2,4) + 148*Power(t2,5) + 
               Power(s2,2)*(89 + 275*t2 - 644*Power(t2,2) + 
                  1222*Power(t2,3) - 681*Power(t2,4)) + 
               Power(s2,3)*(10 - 17*t2 + 129*Power(t2,2) + 
                  30*Power(t2,3) - 13*Power(t2,4)) + 
               Power(s,3)*(10 + 72*t2 + 349*Power(t2,2) - 
                  663*Power(t2,3) + 13*Power(t2,4)) + 
               Power(s,2)*(-91 + (-648 + 67*s2)*t2 + 
                  (3838 + 409*s2)*Power(t2,2) + 
                  2*(4028 + 561*s2)*Power(t2,3) - 
                  3*(386 + 13*s2)*Power(t2,4)) + 
               s2*(-65 - 1432*t2 + 2987*Power(t2,2) - 
                  668*Power(t2,3) - 735*Power(t2,4) + 112*Power(t2,5)) \
- s*(37 + (3102 - 2115*s2 + 712*Power(s2,2))*t2 + 
                  (13588 - 240*s2 + 57*Power(s2,2))*Power(t2,2) + 
                  (8214 + 3372*s2 + 489*Power(s2,2))*Power(t2,3) - 
                  (6785 + 1839*s2 + 39*Power(s2,2))*Power(t2,4) + 
                  112*Power(t2,5))) + 
            Power(t1,7)*(443 + Power(s,3)*(55 - 245*t2) - 2185*t2 - 
               2117*Power(t2,2) + 376*Power(t2,3) + 
               3*Power(s2,3)*(4 + t2) + 
               Power(s2,2)*(-79 + 70*t2 - 174*Power(t2,2)) + 
               s2*(73 + 1602*t2 + 4*Power(t2,2) + 62*Power(t2,3)) + 
               Power(s,2)*(462 + 2351*t2 - 741*Power(t2,2) + 
                  s2*(-154 + 213*t2)) - 
               s*(1305 + 625*t2 - 4094*Power(t2,2) + 133*Power(t2,3) + 
                  Power(s2,2)*(67 + 41*t2) + 
                  s2*(-548 + 20*t2 - 749*Power(t2,2)))) - 
            2*t2*(15 + 45*t2 + 
               (-50 - 31*s + 13*Power(s,2))*Power(t2,2) + 
               (-155 + 65*s + 80*Power(s,2) - 18*Power(s,3))*
                Power(t2,3) + 
               (30 + 150*s - 113*Power(s,2) - 4*Power(s,3))*
                Power(t2,4) + (98 - 63*s)*Power(t2,5) - 
               (-1 + s)*Power(t2,6) + 
               Power(s2,3)*t2*
                (-30 - 45*t2 - 46*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(30 + (50 - 65*s)*t2 + 
                  (175 - 273*s + 61*Power(s,2))*Power(t2,2) + 
                  (51 - 76*s - 60*Power(s,2))*Power(t2,3) + 
                  (-52 + 109*s + 13*Power(s,2))*Power(t2,4) + 
                  (-39 + s)*Power(t2,5) + Power(t2,6)) - 
               Power(s2,2)*t2*
                (125 + 194*t2 - 22*Power(t2,2) + 50*Power(t2,3) + 
                  Power(t2,4) + 
                  2*s*(15 - 116*t2 - 80*Power(t2,2) + 6*Power(t2,3)))) + 
            Power(t1,6)*(-701 - 561*t2 + 1475*Power(t2,2) + 
               3321*Power(t2,3) - 69*Power(t2,4) + 
               Power(s2,3)*(4 + 8*t2 - 60*Power(t2,2)) + 
               5*Power(s,3)*(-9 - 63*t2 + 56*Power(t2,2)) + 
               Power(s2,2)*(31 + 111*t2 - 597*Power(t2,2) + 
                  170*Power(t2,3)) + 
               s2*(310 - 1380*t2 + 242*Power(t2,2) + 534*Power(t2,3) - 
                  17*Power(t2,4)) - 
               Power(s,2)*(-362 + 989*t2 + 4877*Power(t2,2) - 
                  309*Power(t2,3) + s2*(5 + 192*t2 + 535*Power(t2,2))) \
+ s*(578 + 6477*t2 + 7821*Power(t2,2) - 3079*Power(t2,3) + 
                  17*Power(t2,4) + 
                  Power(s2,2)*(60 - 131*t2 + 315*Power(t2,2)) - 
                  s2*(536 + 178*t2 - 2315*Power(t2,2) + 479*Power(t2,3))\
)) - Power(t1,2)*(-55 - 10*t2 + 2394*Power(t2,2) + 2123*Power(t2,3) - 
               3740*Power(t2,4) - 3727*Power(t2,5) + 39*Power(t2,6) + 
               Power(s2,2)*t2*
                (550 + 785*t2 - 996*Power(t2,2) + 1134*Power(t2,3) - 
                  335*Power(t2,4)) + 
               Power(s2,3)*t2*
                (60 + 38*t2 + 236*Power(t2,2) - 178*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s,3)*t2*
                (12 - 7*t2 + 33*Power(t2,2) - 204*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s*(30 + (-99 - 203*s2 + 60*Power(s2,2))*t2 + 
                  (-2698 + 3122*s2 - 1931*Power(s2,2))*Power(t2,2) + 
                  (-5839 + 543*s2 - 713*Power(s2,2))*Power(t2,3) + 
                  (-3148 - 2071*s2 + 318*Power(s2,2))*Power(t2,4) + 
                  (3447 + 973*s2 + 15*Power(s2,2))*Power(t2,5) - 
                  73*Power(t2,6)) + 
               s2*(-60 - 166*t2 - 2104*Power(t2,2) + 943*Power(t2,3) + 
                  328*Power(t2,4) + 162*Power(t2,5) + 73*Power(t2,6)) + 
               Power(s,2)*t2*
                (-84 - 296*t2 + 1784*Power(t2,2) + 3876*Power(t2,3) - 
                  638*Power(t2,4) + 
                  s2*(-30 + 328*t2 + 666*Power(t2,2) + 64*Power(t2,3) - 
                     15*Power(t2,4)))) - 
            Power(t1,5)*(119 + 1840*t2 - 10139*Power(t2,2) - 
               5035*Power(t2,3) + 1475*Power(t2,4) + 
               Power(s2,3)*(23 + 192*t2 - 91*Power(t2,2) - 
                  67*Power(t2,3)) + 
               Power(s,3)*(35 + 148*t2 - 798*Power(t2,2) + 
                  118*Power(t2,3)) + 
               Power(s2,2)*(-2 - 436*t2 - 25*Power(t2,2) - 
                  1048*Power(t2,3) + 48*Power(t2,4)) + 
               s2*(188 + 25*t2 + 5559*Power(t2,2) + 1002*Power(t2,3) + 
                  492*Power(t2,4)) + 
               s*(-570 - 5671*t2 + 3951*Power(t2,2) + 
                  11677*Power(t2,3) - 992*Power(t2,4) + 
                  Power(s2,2)*
                   (-5 - 936*t2 + 114*Power(t2,2) + 252*Power(t2,3)) + 
                  s2*(42 + 3158*t2 + 745*Power(t2,2) + 
                     3902*Power(t2,3) - 96*Power(t2,4))) + 
               Power(s,2)*(-(s2*
                     (50 + 456*t2 - 490*Power(t2,2) + 303*Power(t2,3))) \
+ 2*(69 + 944*t2 + 2094*Power(t2,2) - 1869*Power(t2,3) + 24*Power(t2,4))\
)) + Power(t1,3)*(20 + 645*t2 + 1905*Power(t2,2) - 13531*Power(t2,3) - 
               4306*Power(t2,4) + 1735*Power(t2,5) + 
               2*Power(s2,3)*t2*
                (83 + 278*t2 - 206*Power(t2,2) - 33*Power(t2,3)) + 
               Power(s,3)*t2*
                (83 + 190*t2 - 590*Power(t2,2) + 158*Power(t2,3)) + 
               2*Power(s2,2)*
                (-24 + 23*t2 - 814*Power(t2,2) + 268*Power(t2,3) - 
                  664*Power(t2,4) + 53*Power(t2,5)) + 
               Power(s,2)*(6 + (191 - 180*s2)*t2 - 
                  8*(-157 + 57*s2)*Power(t2,2) - 
                  2*(-914 + 51*s2)*Power(t2,3) - 
                  2*(2131 + 191*s2)*Power(t2,4) + 106*Power(t2,5)) + 
               s2*(-104 - 195*t2 + 749*Power(t2,2) + 7736*Power(t2,3) + 
                  3034*Power(t2,4) + 728*Power(t2,5)) + 
               s*(31 - 748*t2 - 7221*Power(t2,2) + 7285*Power(t2,3) + 
                  9426*Power(t2,4) - 1541*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (128 - 2256*t2 + 841*Power(t2,2) + 290*Power(t2,3)) + 
                  s2*(24 - 82*t2 + 5997*Power(t2,2) - 36*Power(t2,3) + 
                     4452*Power(t2,4) - 212*Power(t2,5))))) - 
         Power(s1,6)*(-3 + 84*Power(t1,9) + 
            Power(t1,8)*(192 - 427*s - 142*t2) - 9*t2 + 2*Power(t2,2) + 
            7*s*Power(t2,2) + Power(s,2)*Power(t2,2) + 38*Power(t2,3) + 
            20*s*Power(t2,3) - 23*Power(s,2)*Power(t2,3) + 
            2*Power(s,3)*Power(t2,3) + 76*Power(t2,4) - 
            77*s*Power(t2,4) + 19*Power(s,2)*Power(t2,4) + 
            3*Power(s,3)*Power(t2,4) - 15*Power(t2,5) - 
            3*s*Power(t2,5) + 5*Power(s,2)*Power(t2,5) - Power(t2,6) + 
            s*Power(t2,6) + Power(t1,7)*
             (-523 + 399*Power(s,2) - 1007*t2 + 75*Power(t2,2) + 
               14*s*(-30 + 61*t2)) - 
            Power(t1,6)*(253 + 28*Power(s,3) + 944*t2 - 
               1052*Power(t2,2) + 12*Power(t2,3) + 
               26*Power(s,2)*(-1 + 29*t2) + 
               s*(-647 - 2799*t2 + 585*Power(t2,2))) + 
            Power(t1,5)*(-385 + 3210*t2 + 3395*Power(t2,2) - 
               313*Power(t2,3) + Power(s,3)*(-41 + 91*t2) + 
               Power(s,2)*(-127 - 1491*t2 + 478*Power(t2,2)) + 
               s*(871 + 635*t2 - 3664*Power(t2,2) + 161*Power(t2,3))) + 
            Power(s2,3)*(5*Power(t1,5)*(-8 + 5*t2) + 
               Power(t1,4)*(-45 + 60*t2 - 23*Power(t2,2)) + 
               t2*(20 + 2*t2 + 17*Power(t2,2) - 2*Power(t2,3)) - 
               t1*t2*(125 + 147*t2 - 67*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,3)*(70 + 160*t2 - 146*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(t1,2)*(-20 + 110*t2 - 104*Power(t2,2) + 
                  65*Power(t2,3) - 2*Power(t2,4))) + 
            Power(t1,3)*(78 + 796*t2 - 3831*Power(t2,2) - 
               3447*Power(t2,3) + 224*Power(t2,4) + 
               Power(s,3)*(4 + 49*t2 - 73*Power(t2,2) - 
                  3*Power(t2,3)) + 
               s*(21 - 1595*t2 - 48*Power(t2,2) + 3175*Power(t2,3) - 
                  250*Power(t2,4)) + 
               Power(s,2)*(41 + 54*t2 + 1185*Power(t2,2) - 
                  614*Power(t2,3) + 4*Power(t2,4))) + 
            Power(t1,4)*(280 + 1111*t2 + 1054*Power(t2,2) - 
               2087*Power(t2,3) + 15*Power(t2,4) + 
               Power(s,3)*(3 + 43*t2 - 50*Power(t2,2)) + 
               Power(s,2)*(-86 + 405*t2 + 1859*Power(t2,2) - 
                  109*Power(t2,3)) - 
               s*(240 + 1955*t2 + 4143*Power(t2,2) - 1693*Power(t2,3) + 
                  15*Power(t2,4))) + 
            Power(t1,2)*(46 - 218*t2 - 992*Power(t2,2) - 
               674*Power(t2,3) + 892*Power(t2,4) + 12*Power(t2,5) + 
               Power(s,3)*t2*
                (13 + 12*t2 - 4*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s,2)*(-17 + 33*t2 - 199*Power(t2,2) - 
                  817*Power(t2,3) + 23*Power(t2,4)) + 
               s*(54 + 283*t2 + 570*Power(t2,2) + 1717*Power(t2,3) - 
                  646*Power(t2,4) + 2*Power(t2,5))) + 
            t1*(4 - 4*t2 - 290*Power(t2,2) + 487*Power(t2,3) + 
               671*Power(t2,4) + 8*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(-23 - 7*t2 + 7*Power(t2,2)) + 
               Power(s,2)*t2*
                (15 - 6*t2 - 169*Power(t2,2) + 22*Power(t2,3) + 
                  3*Power(t2,4)) - 
               s*(16 + 25*t2 - 382*Power(t2,2) - 128*Power(t2,3) + 
                  412*Power(t2,4) + 5*Power(t2,5))) + 
            Power(s2,2)*(Power(t1,6)*(15 - 75*t2) + 
               Power(t1,5)*(125 + s*(165 - 29*t2) - 144*t2 + 
                  146*Power(t2,2)) + 
               Power(t1,2)*(-155 + 31*(-12 + 13*s)*t2 + 
                  (253 + 280*s)*Power(t2,2) - 
                  (145 + 152*s)*Power(t2,3) + (-8 + 6*s)*Power(t2,4)) + 
               t1*(30 - (90 + 73*s)*t2 + (329 + 345*s)*Power(t2,2) - 
                  (195 + 158*s)*Power(t2,3) + 
                  (-59 + 17*s)*Power(t2,4) + 3*Power(t2,5)) + 
               Power(t1,4)*(112 - 129*t2 + 242*Power(t2,2) - 
                  68*Power(t2,3) + s*(-90 - 109*t2 + 11*Power(t2,2))) + 
               t2*(88 + 131*t2 - 5*Power(t2,2) - 16*Power(t2,3) + 
                  4*Power(t2,4) + 
                  s*(6 - 130*t2 - 84*Power(t2,2) + 7*Power(t2,3))) + 
               Power(t1,3)*(94 - 541*t2 + 463*Power(t2,2) - 
                  121*Power(t2,3) + 4*Power(t2,4) - 
                  s*(10 + 635*t2 - 301*Power(t2,2) + 29*Power(t2,3)))) - 
            s2*(6 + 140*Power(t1,8) - (11 + 13*s)*t2 + 
               (15 - 13*s + 12*Power(s,2))*Power(t2,2) + 
               (102 - 5*s - 51*Power(s,2))*Power(t2,3) + 
               (17 - 22*s + 8*Power(s,2))*Power(t2,4) + 
               (-46 + 9*s)*Power(t2,5) + Power(t2,6) - 
               Power(t1,7)*(435 + 91*s + 94*t2) + 
               Power(t1,6)*(-142 + 56*Power(s,2) + s*(181 - 325*t2) - 
                  715*t2 - 95*Power(t2,2)) + 
               Power(t1,5)*(113 + 2320*t2 + 798*Power(t2,2) + 
                  86*Power(t2,3) + Power(s,2)*(-70 + 59*t2) + 
                  s*(481 + 80*t2 + 514*Power(t2,2))) - 
               Power(t1,4)*(-256 - 656*t2 - 1194*Power(t2,2) - 
                  32*Power(t2,3) + 15*Power(t2,4) + 
                  2*Power(s,2)*(3 + 87*t2 + 31*Power(t2,2)) + 
                  s*(249 + 452*t2 - 680*Power(t2,2) + 177*Power(t2,3))) \
+ Power(t1,3)*(141 - 377*t2 - 3232*Power(t2,2) - 1558*Power(t2,3) - 
                  86*Power(t2,4) + 
                  Power(s,2)*
                   (5 + 52*t2 + 115*Power(t2,2) - 19*Power(t2,3)) + 
                  s*(141 - 1349*t2 + 512*Power(t2,2) - 
                     535*Power(t2,3) + 8*Power(t2,4))) + 
               Power(t1,2)*(42 - 389*t2 - 929*Power(t2,2) - 
                  375*Power(t2,3) + 340*Power(t2,4) + 2*Power(t2,5) + 
                  Power(s,2)*t2*
                   (102 + 232*t2 - 91*Power(t2,2) + 6*Power(t2,3)) + 
                  s*(-98 + 447*t2 + 354*Power(t2,2) - 228*Power(t2,3) + 
                     15*Power(t2,4))) + 
               t1*(-59 - 248*t2 + 166*Power(t2,2) + 861*Power(t2,3) + 
                  418*Power(t2,4) - 46*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-17 - 79*t2 - 105*Power(t2,2) + 19*Power(t2,3)) + 
                  s*(6 - 52*t2 + 754*Power(t2,2) - 324*Power(t2,3) - 
                     91*Power(t2,4) + 6*Power(t2,5))))) - 
         Power(s1,5)*(2 + 48*Power(t1,10) - 2*t2 + s*t2 - 89*s2*t2 - 
            6*s*s2*t2 - 60*Power(s2,2)*t2 + 6*Power(t2,2) - 
            Power(s,2)*Power(t2,2) - 196*s2*Power(t2,2) + 
            101*s*s2*Power(t2,2) + 4*Power(s,2)*s2*Power(t2,2) - 
            52*Power(s2,2)*Power(t2,2) + 144*s*Power(s2,2)*Power(t2,2) + 
            80*Power(s2,3)*Power(t2,2) + 111*Power(t2,3) - 
            147*s*Power(t2,3) + 15*Power(s,2)*Power(t2,3) + 
            4*Power(s,3)*Power(t2,3) + 63*s2*Power(t2,3) + 
            481*s*s2*Power(t2,3) - 147*Power(s,2)*s2*Power(t2,3) - 
            192*Power(s2,2)*Power(t2,3) - 86*s*Power(s2,2)*Power(t2,3) + 
            109*Power(s2,3)*Power(t2,3) - 153*Power(t2,4) - 
            28*s*Power(t2,4) + 26*Power(s,2)*Power(t2,4) + 
            14*Power(s,3)*Power(t2,4) + 312*s2*Power(t2,4) - 
            123*s*s2*Power(t2,4) - 74*Power(s,2)*s2*Power(t2,4) + 
            77*Power(s2,2)*Power(t2,4) + 58*s*Power(s2,2)*Power(t2,4) - 
            10*Power(s2,3)*Power(t2,4) - 189*Power(t2,5) + 
            122*s*Power(t2,5) - 7*Power(s,2)*Power(t2,5) - 
            2*Power(s,3)*Power(t2,5) + 106*s2*Power(t2,5) - 
            43*s*s2*Power(t2,5) + 5*Power(s,2)*s2*Power(t2,5) + 
            24*Power(s2,2)*Power(t2,5) - 4*s*Power(s2,2)*Power(t2,5) + 
            Power(s2,3)*Power(t2,5) + Power(t2,6) + 4*s*Power(t2,6) - 
            Power(s,2)*Power(t2,6) - 20*s2*Power(t2,6) + 
            2*s*s2*Power(t2,6) - Power(s2,2)*Power(t2,6) - 
            Power(t1,9)*(-67 + 315*s + 64*s2 + 96*t2) + 
            Power(t1,8)*(-340 + 413*Power(s,2) - 738*t2 + 
               62*Power(t2,2) + s2*(321 + 25*t2) + 
               7*s*(-59 + 9*s2 + 104*t2)) - 
            Power(t1,7)*(158 + 56*Power(s,3) + 359*t2 - 
               1099*Power(t2,2) + 13*Power(t2,3) + 
               Power(s2,2)*(-20 + 93*t2) + 
               Power(s,2)*(-93 + 70*s2 + 954*t2) + 
               s2*(27 - 382*t2 - 114*Power(t2,2)) + 
               s*(-866 + s2*(183 - 445*t2) - 3092*t2 + 592*Power(t2,2))) \
+ Power(t1,6)*(-484 + 10*Power(s2,3)*(-3 + t2) + 3652*t2 + 
               3484*Power(t2,2) - 511*Power(t2,3) + 
               Power(s,3)*(-64 + 189*t2) + 
               Power(s2,2)*(129 - 124*t2 + 225*Power(t2,2)) - 
               s2*(124 + 2515*t2 + 394*Power(t2,2) + 104*Power(t2,3)) + 
               Power(s,2)*(-311 - 2328*t2 + 780*Power(t2,2) - 
                  2*s2*(-70 + 79*t2)) + 
               s*(1332 + 671*t2 - 5017*Power(t2,2) + 199*Power(t2,3) + 
                  Power(s2,2)*(136 + 15*t2) - 
                  s2*(665 + 61*t2 + 835*Power(t2,2)))) + 
            Power(t1,5)*(609 + 1121*t2 - 530*Power(t2,2) - 
               3704*Power(t2,3) + 64*Power(t2,4) + 
               Power(s,3)*(18 + 178*t2 - 171*Power(t2,2)) + 
               Power(s2,3)*(-21 + 30*t2 + 20*Power(t2,2)) + 
               Power(s2,2)*(43 - 154*t2 + 555*Power(t2,2) - 
                  165*Power(t2,3)) + 
               s2*(-386 + 400*t2 - 999*Power(t2,2) - 454*Power(t2,3) + 
                  24*Power(t2,4)) + 
               Power(s,2)*(-244 + 811*t2 + 3964*Power(t2,2) - 
                  266*Power(t2,3) + s2*(5 + 202*t2 + 314*Power(t2,2))) \
- s*(444 + 4990*t2 + 7240*Power(t2,2) - 3134*Power(t2,3) + 
                  24*Power(t2,4) + 
                  Power(s2,2)*(100 - 10*t2 + 163*Power(t2,2)) - 
                  s2*(475 + 481*t2 - 1825*Power(t2,2) + 431*Power(t2,3))\
)) + Power(t1,3)*(-16 - 1303*t2 - 2148*Power(t2,2) + 1513*Power(t2,3) + 
               3879*Power(t2,4) - 33*Power(t2,5) - 
               Power(s2,3)*(20 - 84*t2 + 172*Power(t2,2) - 
                  96*Power(t2,3) + Power(t2,4)) + 
               Power(s,3)*(-2 + 15*t2 - 90*Power(t2,2) + 
                  173*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(-190 - 454*t2 + 575*Power(t2,2) - 
                  774*Power(t2,3) + 249*Power(t2,4)) - 
               Power(s,2)*(10 + (-275 + 167*s2)*t2 + 
                  2*(705 + 197*s2)*Power(t2,2) + 
                  (3776 + 171*s2)*Power(t2,3) + 
                  3*(-143 + s2)*Power(t2,4)) + 
               s2*(-8 + 1183*t2 + 249*Power(t2,2) + 725*Power(t2,3) - 
                  33*Power(t2,4) - 56*Power(t2,5)) + 
               s*(59 + 1285*t2 + 5021*Power(t2,2) + 5125*Power(t2,3) - 
                  3437*Power(t2,4) + 56*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (772 + 358*t2 - 98*Power(t2,2) + 3*Power(t2,3)) + 
                  s2*(141 - 1387*t2 - 666*Power(t2,2) + 
                     1654*Power(t2,3) - 678*Power(t2,4)))) + 
            Power(t1,4)*(178 + 1422*t2 - 9150*Power(t2,2) - 
               5785*Power(t2,3) + 1010*Power(t2,4) + 
               Power(s2,3)*(55 + 230*t2 - 174*Power(t2,2) - 
                  14*Power(t2,3)) + 
               Power(s,3)*(19 + 130*t2 - 363*Power(t2,2) + 
                  41*Power(t2,3)) + 
               Power(s2,2)*(53 - 705*t2 + 465*Power(t2,2) - 
                  684*Power(t2,3) + 30*Power(t2,4)) + 
               s2*(-14 + 456*t2 + 5854*Power(t2,2) + 
                  1939*Power(t2,3) + 394*Power(t2,4)) + 
               Power(s,2)*(72 + 659*t2 + 2877*Power(t2,2) - 
                  2252*Power(t2,3) + 30*Power(t2,4) - 
                  s2*(25 + 238*t2 - 88*Power(t2,2) + 96*Power(t2,3))) + 
               s*(-216 - 3967*t2 + 1683*Power(t2,2) + 
                  8356*Power(t2,3) - 782*Power(t2,4) + 
                  Power(s2,2)*
                   (-10 - 988*t2 + 314*Power(t2,2) + 69*Power(t2,3)) + 
                  s2*(-78 + 2628*t2 - 207*Power(t2,2) + 
                     2380*Power(t2,3) - 60*Power(t2,4)))) + 
            t1*(-20 - 125*t2 + 266*Power(t2,2) + 911*Power(t2,3) + 
               50*Power(t2,4) - 690*Power(t2,5) - 8*Power(t2,6) - 
               Power(s,3)*Power(t2,2)*
                (-3 + 11*t2 - 11*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*t2*
                (60 - 30*t2 + 101*Power(t2,2) - 58*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*t2*
                (390 + 614*t2 - 275*Power(t2,2) + 226*Power(t2,3) + 
                  9*Power(t2,4)) + 
               s2*(-30 + 17*t2 - 649*Power(t2,2) - 523*Power(t2,3) + 
                  89*Power(t2,4) + 266*Power(t2,5) - 2*Power(t2,6)) + 
               s*(6 + (-93 - 100*s2 + 30*Power(s2,2))*t2 + 
                  (-149 + 777*s2 - 819*Power(s2,2))*Power(t2,2) + 
                  (-487 + 350*s2 - 407*Power(s2,2))*Power(t2,3) + 
                  (-1111 - 433*s2 + 165*Power(s2,2))*Power(t2,4) + 
                  (468 + 6*s2 - 3*Power(s2,2))*Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s,2)*t2*
                (-4 + 19*t2 - 37*Power(t2,2) + 705*Power(t2,3) - 
                  15*Power(t2,4) + 
                  s2*(-6 + 102*t2 + 305*Power(t2,2) - 118*Power(t2,3) + 
                     3*Power(t2,4)))) + 
            Power(t1,2)*(5 - 314*t2 - 1100*Power(t2,2) + 
               4942*Power(t2,3) + 3077*Power(t2,4) - 390*Power(t2,5) + 
               Power(s,3)*t2*
                (-29 - 105*t2 + 97*Power(t2,2) + Power(t2,3)) - 
               Power(s2,3)*t2*
                (200 + 380*t2 - 272*Power(t2,2) + 19*Power(t2,3)) + 
               Power(s2,2)*(60 - 132*t2 + 1227*Power(t2,2) - 
                  812*Power(t2,3) + 215*Power(t2,4) - 11*Power(t2,5)) + 
               Power(s,2)*(6 + (-2 + 113*s2)*t2 + 
                  4*(-25 + 58*s2)*Power(t2,2) + 
                  3*(-322 + 89*s2)*Power(t2,3) + 
                  (981 - 21*s2)*Power(t2,4) - 11*Power(t2,5)) - 
               s2*(-160 - 529*t2 + 1011*Power(t2,2) + 
                  4208*Power(t2,3) + 1648*Power(t2,4) + 90*Power(t2,5)) \
+ s*(-89 - 10*t2 + 2976*Power(t2,2) - 1116*Power(t2,3) - 
                  3284*Power(t2,4) + 387*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-175 + 1207*t2 - 579*Power(t2,2) + 39*Power(t2,3)) + 
                  s2*(-30 + 206*t2 - 2960*Power(t2,2) + 
                     1197*Power(t2,3) - 838*Power(t2,4) + 22*Power(t2,5))\
))) - s1*((1 - 5*s + 4*Power(s,2))*Power(t1,12) + 
            4*Power(t2,2)*(15 + 
               3*(20 + 3*Power(s,2) - 2*s2 - 9*Power(s2,2) - 
                  6*s*(2 + 3*s2))*t2 - 
               (-77 + 4*Power(s,3) + 72*s2 + 82*Power(s2,2) + 
                  6*Power(s2,3) - 2*Power(s,2)*(13 + 25*s2) + 
                  s*(154 + 72*s2 - 72*Power(s2,2)))*Power(t2,2) + 
               2*(-22 + Power(s,3) + Power(s,2)*(76 - 70*s2) + 19*s2 - 
                  126*Power(s2,2) + 12*Power(s2,3) + 
                  s*(-149 + 194*s2 + 41*Power(s2,2)))*Power(t2,3) + 
               (-251 + 126*s2 - 58*Power(s2,2) - 30*Power(s2,3) - 
                  2*Power(s,2)*(67 + 39*s2) + 
                  s*(350 + 208*s2 + 92*Power(s2,2)))*Power(t2,4) + 
               (96 + 2*Power(s,3) + 88*s2 - 49*Power(s2,2) - 
                  4*Power(s2,3) - Power(s,2)*(121 + 8*s2) + 
                  2*s*(27 + 65*s2 + 5*Power(s2,2)))*Power(t2,5) + 
               (47 + 4*Power(s,2) + 18*s2 + 4*Power(s2,2) - 
                  4*s*(11 + 2*s2))*Power(t2,6)) - 
            2*Power(t1,2)*t2*(-30 + 
               3*(-33 + 24*Power(s,2) + 8*s2 - 18*Power(s2,2) + 
                  s*(-85 + 18*s2))*t2 + 
               (-562 + 26*Power(s,3) + Power(s,2)*(564 - 77*s2) - 
                  191*s2 - 104*Power(s2,2) - Power(s2,3) - 
                  4*s*(113 - 117*s2 + 5*Power(s2,2)))*Power(t2,2) - 
               (-454 + 29*Power(s,3) + 2147*s2 + 259*Power(s2,2) - 
                  114*Power(s2,3) + Power(s,2)*(11 + 228*s2) + 
                  s*(-457 - 3878*s2 + 897*Power(s2,2)))*Power(t2,3) - 
               (1990 + 156*Power(s,3) - 743*s2 + 481*Power(s2,2) + 
                  97*Power(s2,3) + Power(s,2)*(2253 + 305*s2) - 
                  5*s*(1147 + 350*s2 + 78*Power(s2,2)))*Power(t2,4) + 
               (1077 + 223*Power(s,3) + 751*s2 - 223*Power(s2,2) - 
                  88*Power(s2,3) - 3*Power(s,2)*(495 + 178*s2) + 
                  s*(706 + 1124*s2 + 399*Power(s2,2)))*Power(t2,5) + 
               (478 + 105*Power(s,2) + 148*s2 + 105*Power(s2,2) - 
                  15*s*(33 + 14*s2))*Power(t2,6)) + 
            Power(t1,4)*(-10 + 
               5*(-10 + 36*Power(s,2) + 27*s2 - 3*s*(29 + 2*s2))*t2 + 
               (-60*Power(s,3) + Power(s,2)*(1667 - 60*s2) + 
                  s*(-1964 + 465*s2 - 156*Power(s2,2)) + 
                  4*(-159 + 24*s2 + 7*Power(s2,2) + 6*Power(s2,3)))*
                Power(t2,2) - 
               (-1339 + 484*Power(s,3) + 3949*s2 + 6*Power(s2,2) - 
                  69*Power(s2,3) + Power(s,2)*(-3453 + 875*s2) + 
                  s*(1945 - 6105*s2 + 942*Power(s2,2)))*Power(t2,3) - 
               (3769 + 550*Power(s,3) + 362*s2 + 1799*Power(s2,2) + 
                  82*Power(s2,3) + Power(s,2)*(4046 + 778*s2) - 
                  s*(11924 + 6257*s2 + 790*Power(s2,2)))*Power(t2,4) + 
               (271 + 566*Power(s,3) + 90*s2 - 626*Power(s2,2) - 
                  199*Power(s2,3) - Power(s,2)*(4189 + 1331*s2) + 
                  s*(5096 + 3405*s2 + 964*Power(s2,2)))*Power(t2,5) + 
               (695 + 167*Power(s,2) + 278*s2 + 167*Power(s2,2) - 
                  2*s*(410 + 167*s2))*Power(t2,6)) - 
            Power(t1,11)*(8*Power(s,3) + (3 + 2*s2)*(-1 + t2) + 
               Power(s,2)*(-21 + s2 + 14*t2) + 
               s*(13 + s2 - 24*t2 - 5*s2*t2)) + 
            Power(t1,10)*(Power(s2,2)*(-4 + t2)*t2 + 
               Power(s,3)*(4 + 31*t2) + 2*(-15 - 8*t2 + 5*Power(t2,2)) + 
               s2*(-2 - 22*t2 + 6*Power(t2,2)) + 
               Power(s,2)*(-57 + 5*s2 - 141*t2 - 24*s2*t2 + 
                  18*Power(t2,2)) + 
               s*(84 + 110*t2 + Power(s2,2)*t2 - 43*Power(t2,2) - 
                  2*s2*(3 - 20*t2 + 7*Power(t2,2)))) + 
            Power(t1,9)*(37 + 29*t2 + 39*Power(t2,2) + 
               2*Power(s2,3)*Power(t2,2) - 19*Power(t2,3) + 
               Power(s,3)*(18 + 58*t2 - 45*Power(t2,2)) + 
               Power(s2,2)*t2*(2 + 29*t2 - 3*Power(t2,2)) + 
               Power(s,2)*(-44 + s2 + t2 + 44*s2*t2 + 330*Power(t2,2) + 
                  72*s2*Power(t2,2) - 10*Power(t2,3)) + 
               s2*(-10 + 82*t2 + 47*Power(t2,2) - 6*Power(t2,3)) - 
               s*(26 + 208*t2 + 402*Power(t2,2) - 36*Power(t2,3) + 
                  Power(s2,2)*t2*(-6 + 29*t2) + 
                  s2*(-14 + 110*t2 + 173*Power(t2,2) - 13*Power(t2,3)))) \
+ Power(t1,8)*(26 + 274*t2 - 55*Power(t2,2) - 128*Power(t2,3) - 
               10*Power(s2,3)*Power(t2,3) + 14*Power(t2,4) + 
               2*Power(s2,2)*t2*
                (6 - 20*t2 - 22*Power(t2,2) + Power(t2,3)) + 
               Power(s,3)*(-1 - 84*t2 - 255*Power(t2,2) + 
                  29*Power(t2,3)) + 
               s2*(16 - 60*t2 + 98*Power(t2,2) - 99*Power(t2,3) + 
                  4*Power(t2,4)) - 
               s*(122 + (887 - 146*s2 + 18*Power(s2,2))*t2 + 
                  (213 + 24*s2 - 32*Power(s2,2))*Power(t2,2) - 
                  (663 + 302*s2 + 49*Power(s2,2))*Power(t2,3) + 
                  2*(7 + 2*s2)*Power(t2,4)) + 
               Power(s,2)*(74 + 735*t2 + 797*Power(t2,2) - 
                  354*Power(t2,3) + 2*Power(t2,4) - 
                  s2*(5 + 46*t2 - 130*Power(t2,2) + 68*Power(t2,3)))) + 
            Power(t1,5)*(28 + 439*t2 + 2478*Power(t2,2) + 
               281*Power(t2,3) - 1594*Power(t2,4) - 1060*Power(t2,5) + 
               36*Power(t2,6) - 
               Power(s2,3)*Power(t2,2)*
                (2 - 49*t2 + 14*Power(t2,2) + 55*Power(t2,3)) + 
               Power(s2,2)*t2*
                (2 + 154*t2 - 495*Power(t2,2) + 46*Power(t2,3) - 
                  343*Power(t2,4)) + 
               Power(s,3)*t2*
                (60 + 235*t2 - 295*Power(t2,2) - 981*Power(t2,3) + 
                  55*Power(t2,4)) + 
               s2*(6 - 11*t2 - 191*Power(t2,2) + 3121*Power(t2,3) - 
                  93*Power(t2,4) - 394*Power(t2,5) + 26*Power(t2,6)) - 
               s*(30 + (465 - 92*s2 + 6*Power(s2,2))*t2 + 
                  (4655 - 181*s2 + 43*Power(s2,2))*Power(t2,2) + 
                  (10883 + 1994*s2 - 269*Power(s2,2))*Power(t2,3) + 
                  (1745 + 629*s2 + 363*Power(s2,2))*Power(t2,4) - 
                  (2360 + 1286*s2 + 165*Power(s2,2))*Power(t2,5) + 
                  26*Power(t2,6)) + 
               Power(s,2)*t2*(-240 + 515*t2 + 6703*Power(t2,2) + 
                  4009*Power(t2,3) - 943*Power(t2,4) + 
                  s2*(30 + 218*t2 + 733*Power(t2,2) + 1358*Power(t2,3) - 
                     165*Power(t2,4)))) - 
            4*t1*Power(t2,2)*(-21 - 168*t2 - 755*Power(t2,2) + 
               506*Power(t2,3) + 771*Power(t2,4) + 70*Power(t2,5) - 
               19*Power(t2,6) + 
               Power(s2,3)*t2*
                (1 - 53*t2 - 27*Power(t2,2) + 57*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-81 - 6*t2 + 318*Power(t2,2) + 82*Power(t2,3) + 
                  119*Power(t2,4)) + 
               Power(s,3)*(t2 - 69*Power(t2,2) + 25*Power(t2,3) + 
                  81*Power(t2,4) - 14*Power(t2,5)) + 
               s2*(9 + 101*t2 + 384*Power(t2,2) - 538*Power(t2,3) - 
                  243*Power(t2,4) - 139*Power(t2,5) - 22*Power(t2,6)) + 
               s*(9 + (85 + 22*s2 - 9*Power(s2,2))*t2 + 
                  (1500 - 976*s2 + 413*Power(s2,2))*Power(t2,2) + 
                  (726 - 308*s2 + 135*Power(s2,2))*Power(t2,3) - 
                  (555 + 344*s2 + 105*Power(s2,2))*Power(t2,4) - 
                  (267 + 346*s2 + 42*Power(s2,2))*Power(t2,5) + 
                  22*Power(t2,6)) + 
               Power(s,2)*t2*(99 - 114*t2 - 386*Power(t2,2) - 
                  66*Power(t2,3) + 227*Power(t2,4) + 
                  s2*(-9 + 61*t2 - 149*Power(t2,2) - 33*Power(t2,3) + 
                     42*Power(t2,4)))) + 
            2*Power(t1,3)*t2*(-90 - 495*t2 - 2327*Power(t2,2) + 
               719*Power(t2,3) + 2373*Power(t2,4) + 796*Power(t2,5) - 
               48*Power(t2,6) + 
               Power(s,3)*t2*
                (-30 - 81*t2 + 150*Power(t2,2) + 479*Power(t2,3) - 
                  54*Power(t2,4)) + 
               Power(s2,3)*t2*
                (-6 - 43*t2 - 42*Power(t2,2) + 69*Power(t2,3) + 
                  54*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-109 - 192*t2 + 983*Power(t2,2) + 422*Power(t2,3) + 
                  336*Power(t2,4)) + 
               s2*(-30 - 2*t2 + 600*Power(t2,2) - 1947*Power(t2,3) + 
                  331*Power(t2,4) + 81*Power(t2,5) - 57*Power(t2,6)) + 
               s*(90 + (624 + 74*s2 + 30*Power(s2,2))*t2 + 
                  (4836 - 890*s2 + 317*Power(s2,2))*Power(t2,2) + 
                  (6403 - 648*s2 - 198*Power(s2,2))*Power(t2,3) - 
                  (1731 + 1322*s2 + 3*Power(s2,2))*Power(t2,4) - 
                  (1759 + 1182*s2 + 162*Power(s2,2))*Power(t2,5) + 
                  57*Power(t2,6)) + 
               Power(s,2)*t2*(195 - 1022*t2 - 3967*Power(t2,2) - 
                  996*Power(t2,3) + 846*Power(t2,4) + 
                  s2*(-90 - 33*t2 - 102*Power(t2,2) - 545*Power(t2,3) + 
                     162*Power(t2,4)))) - 
            Power(t1,7)*(56 + 517*t2 + 296*Power(t2,2) - 
               35*Power(t2,3) - 249*Power(t2,4) + 3*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(2 + 12*t2 - 7*Power(t2,2)) + 
               Power(s2,2)*t2*
                (12 - 25*t2 + 176*Power(t2,2) - 57*Power(t2,3)) + 
               Power(s,3)*(10 + 123*t2 + 56*Power(t2,2) - 
                  375*Power(t2,3) + 7*Power(t2,4)) + 
               Power(s,2)*(-32 + (-175 + 57*s2)*t2 + 
                  2*(765 + 158*s2)*Power(t2,2) + 
                  (1982 + 573*s2)*Power(t2,3) - 
                  3*(59 + 7*s2)*Power(t2,4)) + 
               s2*(-2 - 51*t2 + 927*Power(t2,2) + 193*Power(t2,3) - 
                  101*Power(t2,4) + 2*Power(t2,5)) + 
               s*(-51 - 741*t2 - 3001*Power(t2,2) - 1847*Power(t2,3) + 
                  527*Power(t2,4) - 2*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-4 + 80*t2 - 210*Power(t2,2) + 21*Power(t2,3)) + 
                  s2*(13 + 61*t2 - 990*Power(t2,2) - 918*Power(t2,3) + 
                     234*Power(t2,4)))) + 
            Power(t1,6)*(1 - 36*t2 - 892*Power(t2,2) + 1134*Power(t2,3) + 
               417*Power(t2,4) - 180*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(-10 + 4*t2 + 85*Power(t2,2)) + 
               Power(s,3)*t2*(15 + 401*t2 + 645*Power(t2,2) - 
                  237*Power(t2,3)) + 
               Power(s2,2)*Power(t2,2)*
                (-87 + 604*t2 + 305*Power(t2,2) - 33*Power(t2,3)) + 
               s2*(-14 - 113*t2 + 959*Power(t2,2) + 144*Power(t2,3) + 
                  370*Power(t2,4) - 62*Power(t2,5)) + 
               Power(s,2)*(-30 + (-630 + 43*s2)*t2 + 
                  (-2934 + 274*s2)*Power(t2,2) + 
                  (-462 + 13*s2)*Power(t2,3) + 
                  (2017 + 559*s2)*Power(t2,4) - 33*Power(t2,5)) + 
               s*(61 + 940*t2 + 2800*Power(t2,2) - 2884*Power(t2,3) - 
                  3160*Power(t2,4) + 195*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (13 + 213*t2 - 283*Power(t2,2) - 407*Power(t2,3)) + 
                  s2*(6 - 82*t2 - 1498*Power(t2,2) - 1877*Power(t2,3) - 
                     1736*Power(t2,4) + 66*Power(t2,5))))))*
       T2(t2,1 - s1 - t1 + t2))/
     ((-1 + s2)*(-s + s2 - t1)*
       Power(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2,2)*
       Power(-1 + s1 + t1 - t2,2)*Power(s1*t1 - t2,3)*(-1 + t2)) + 
    (8*((-1 + Power(s,3) - s1 - 2*Power(s1,3) - Power(s,2)*(3 + 4*s1) + 
            s*(3 + 5*s1 + 5*Power(s1,2)))*Power(t1,10) + 
         Power(t1,9)*(4 - 7*Power(s1,4) + 
            Power(s,3)*(-1 + 3*s1 - 7*t2) + 5*t2 + 3*s2*t2 + 
            2*s1*(1 + s2*(-1 + t2) + 3*t2) + 
            Power(s1,2)*(-2 - 3*s2 + 7*t2) + 
            Power(s1,3)*(2 + 2*s2 + 11*t2) + 
            Power(s,2)*(6 - 14*Power(s1,2) + 22*t2 + 3*s2*t2 + 
               s1*(-6 + s2 + 26*t2)) + 
            s*(-9 + 18*Power(s1,3) - 20*t2 - 6*s2*t2 - 
               Power(s1,2)*(-12 + s2 + 30*t2) + 
               s1*(-2 + s2 - 39*t2 - 5*s2*t2))) + 
         Power(t2,3)*(2 - 4*t2 - 3*s*t2 - 16*s*Power(t2,2) - 
            Power(s,2)*Power(t2,2) - 15*Power(t2,3) + 39*s*Power(t2,3) - 
            21*Power(s,2)*Power(t2,3) + 5*Power(t2,4) + 
            22*s*Power(t2,4) - 8*Power(s,2)*Power(t2,4) + 
            11*Power(t2,5) - 8*s*Power(t2,5) + Power(s,2)*Power(t2,5) + 
            2*Power(s,3)*Power(t2,5) + Power(t2,6) - 2*s*Power(t2,6) + 
            Power(s,2)*Power(t2,6) + 
            Power(s1,5)*s2*(Power(s2,2) + Power(t2,2)) - 
            Power(s2,3)*(4 + 22*t2 + 24*Power(t2,2) + Power(t2,3) - 
               2*Power(t2,4) + Power(t2,5)) + 
            Power(s2,2)*(14 + 2*(32 + s)*t2 + (62 - 18*s)*Power(t2,2) - 
               4*(1 + 5*s)*Power(t2,3) + (5 - 2*s)*Power(t2,4) + 
               2*(5 + 2*s)*Power(t2,5) + Power(t2,6)) - 
            s2*(14 + (45 - 6*s)*t2 + 
               (48 - 57*s + 2*Power(s,2))*Power(t2,2) - 
               (7 + 21*s + 9*Power(s,2))*Power(t2,3) + 
               (6 + 17*s)*Power(t2,4) + 
               (6 + 13*s + 5*Power(s,2))*Power(t2,5) + 2*s*Power(t2,6)) \
- Power(s1,4)*(Power(s2,3)*(3 + 2*t2) + 
               Power(s2,2)*(3 + t2 - 4*s*t2 - Power(t2,2)) + 
               s2*t2*(4 + (4 + s)*t2 + 4*Power(t2,2)) + 
               t2*(-4 - 3*t2 - (-1 + s)*Power(t2,2))) + 
            Power(s1,3)*(4 - 4*(3 + s)*t2 - (28 + 3*s)*Power(t2,2) + 
               (-9 + s - Power(s,2))*Power(t2,3) - 
               3*(-1 + s)*Power(t2,4) + 
               Power(s2,3)*(2 + 3*t2 + Power(t2,2)) + 
               Power(s2,2)*(11 + (19 - 12*s)*t2 - 7*s*Power(t2,2) - 
                  4*Power(t2,3)) + 
               s2*(-3 - (-6 + s)*t2 + 
                  (26 + 2*s + 5*Power(s,2))*Power(t2,2) + 
                  7*(2 + s)*Power(t2,3) + 6*Power(t2,4))) - 
            Power(s1,2)*(7 + (7 - 3*s)*t2 - (49 + 17*s)*Power(t2,2) - 
               (52 + 5*s - 3*Power(s,2) + 2*Power(s,3))*Power(t2,3) + 
               (-12 + 8*s - 5*Power(s,2))*Power(t2,4) - 
               3*(-1 + s)*Power(t2,5) + 
               Power(s2,3)*(2 + 9*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(2 - 12*(-1 + s)*t2 + 42*Power(t2,2) - 
                  (13 + 6*s)*Power(t2,3) - 6*Power(t2,4)) + 
               s2*(5 + (2 - 12*s)*t2 + 
                  (24 - 25*s + 13*Power(s,2))*Power(t2,2) + 
                  (49 + 29*s + 8*Power(s,2))*Power(t2,3) + 
                  (16 + 13*s)*Power(t2,4) + 4*Power(t2,5))) + 
            s1*(1 + (19 + 4*s)*t2 + (-10 + s + Power(s,2))*Power(t2,2) - 
               (34 + 54*s - 21*Power(s,2) + 2*Power(s,3))*Power(t2,3) - 
               (42 - 13*s + 3*Power(s,2) + 3*Power(s,3))*Power(t2,4) + 
               (-7 + 9*s - 5*Power(s,2))*Power(t2,5) - 
               (-1 + s)*Power(t2,6) + 
               Power(s2,3)*(6 + 30*t2 + 8*Power(t2,2) - 7*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s2,2)*(20 + (70 + 6*s)*t2 - 
                  (13 + 24*s)*Power(t2,2) - 7*(3 + 2*s)*Power(t2,3) + 
                  (22 + 7*s)*Power(t2,4) + 4*Power(t2,5)) + 
               s2*(22 + (45 - 17*s)*t2 + 
                  (11 - 81*s + 10*Power(s,2))*Power(t2,2) + 
                  (22 + 9*s + 3*Power(s,2))*Power(t2,3) + 
                  (33 + 40*s + 8*Power(s,2))*Power(t2,4) + 
                  (6 + 9*s)*Power(t2,5) + Power(t2,6)))) + 
         t1*Power(t2,2)*(6 - (-41 + s + 5*s2 + 18*s*s2 + 
               14*Power(s2,2) + 2*s*Power(s2,2) - 8*Power(s2,3))*t2 - 
            2*Power(s1,6)*s2*Power(t2,2) + 
            (42 + 28*s2 - 52*Power(s2,2) + 28*Power(s2,3) + 
               Power(s,2)*(5 + 14*s2) + 
               s*(-7 - 138*s2 + 42*Power(s2,2)))*Power(t2,2) - 
            (-29 + 3*Power(s,3) + 3*s2 + 3*Power(s2,2) - 
               5*Power(s2,3) + Power(s,2)*(-103 + 37*s2) + 
               s*(197 + 35*s2 - 77*Power(s2,2)))*Power(t2,3) + 
            (-22 + Power(s,3) + 7*s2 - 41*Power(s2,2) - 
               4*Power(s2,3) - 8*Power(s,2)*(-5 + 3*s2) + 
               s*(-99 + 150*s2 + 23*Power(s2,2)))*Power(t2,4) + 
            (-53 - 8*Power(s,3) + 14*s2 - 52*Power(s2,2) + 
               3*Power(s2,3) + 5*Power(s,2)*(-7 + 3*s2) + 
               s*(94 + 102*s2 - 10*Power(s2,2)))*Power(t2,5) + 
            (-4 + Power(s,3) + 2*s2 - 6*Power(s2,2) - Power(s2,3) - 
               Power(s,2)*(13 + 3*s2) + s*(19 + 19*s2 + 3*Power(s2,2)))*
             Power(t2,6) + (1 - s + s2)*Power(t2,7) + 
            Power(s1,5)*(-3*Power(s2,3) - 
               Power(s2,2)*t2*(-1 + s + t2) + 
               s2*t2*(12 + (9 + 2*s)*t2 + 8*Power(t2,2)) - 
               t2*(12 + 10*t2 + (-3 + 2*s)*Power(t2,2))) - 
            Power(s1,4)*(12 - 4*(8 + 3*s)*t2 - 
               (83 + 10*s)*Power(t2,2) + 
               (-34 + 7*s - 2*Power(s,2))*Power(t2,3) + 
               (9 - 6*s)*Power(t2,4) + 
               Power(s2,3)*(-9 - 10*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-3 + 2*(11 + 6*s)*t2 + 
                  (3 - 4*s)*Power(t2,2) - 6*Power(t2,3)) + 
               s2*(-18 + (10 + 13*s)*t2 + 
                  2*(43 + 5*s + Power(s,2))*Power(t2,2) + 
                  3*(9 + 4*s)*Power(t2,3) + 12*Power(t2,4))) + 
            Power(s1,3)*(18 + 45*t2 - 
               (109 + 48*s + 5*Power(s,2))*Power(t2,2) - 
               (148 + 28*s - 6*Power(s,2) + Power(s,3))*Power(t2,3) + 
               (-56 + 37*s - 9*Power(s,2))*Power(t2,4) + 
               (9 - 6*s)*Power(t2,5) + 
               Power(s2,3)*t2*(11 - 9*t2 + 7*Power(t2,2)) - 
               Power(s2,2)*(39 + (50 - 43*s)*t2 - 
                  (95 + 47*s)*Power(t2,2) + 2*(5 + 8*s)*Power(t2,3) + 
                  12*Power(t2,4)) + 
               s2*(-21 + (-72 + 23*s)*t2 + 
                  (58 + 20*s - 18*Power(s,2))*Power(t2,2) + 
                  2*(75 + 22*s + 5*Power(s,2))*Power(t2,3) + 
                  4*(5 + 6*s)*Power(t2,4) + 8*Power(t2,5))) - 
            Power(s1,2)*(-9 + (46 + 29*s)*t2 + 
               (83 + 77*s - 10*Power(s,2))*Power(t2,2) + 
               (9 - 153*s - 4*Power(s,2) + 13*Power(s,3))*Power(t2,3) - 
               (109 + 27*s - 29*Power(s,2) + 6*Power(s,3))*
                Power(t2,4) + 
               (-45 + 46*s - 10*Power(s,2))*Power(t2,5) + 
               (3 - 2*s)*Power(t2,6) + 
               3*Power(s2,3)*
                (6 + 29*t2 + 15*Power(t2,2) - Power(t2,3) + 
                  3*Power(t2,4)) - 
               Power(s2,2)*(78 - 5*(-57 + 8*s)*t2 + 
                  (36 - 85*s)*Power(t2,2) - (121 + 26*s)*Power(t2,3) + 
                  (19 + 27*s)*Power(t2,4) + 10*Power(t2,5)) + 
               s2*(21 + (24 + 41*s)*t2 - 
                  (110 + s + 61*Power(s,2))*Power(t2,2) + 
                  (16 - 17*s - 30*Power(s,2))*Power(t2,3) + 
                  2*(38 + 9*s + 12*Power(s,2))*Power(t2,4) + 
                  (-6 + 20*s)*Power(t2,5) + 2*Power(t2,6))) + 
            s1*(-21 + 6*(-10 + 3*s)*t2 + 
               (15 + 127*s - 10*Power(s,2))*Power(t2,2) + 
               (163 + 135*s - 91*Power(s,2) + 17*Power(s,3))*
                Power(t2,3) + 
               (113 - 180*s + 39*Power(s,2) + 14*Power(s,3))*
                Power(t2,4) - 
               (16 + 49*s - 46*Power(s,2) + 7*Power(s,3))*Power(t2,5) + 
               (-14 + 17*s - 3*Power(s,2))*Power(t2,6) + 
               Power(s2,3)*(12 + 58*t2 + 70*Power(t2,2) + 
                  29*Power(t2,3) - 4*Power(t2,4) + 5*Power(t2,5)) - 
               Power(s2,2)*(42 - 4*(-50 + 3*s)*t2 + 
                  (221 + 3*s)*Power(t2,2) + (-22 + 29*s)*Power(t2,3) - 
                  (100 + s)*Power(t2,4) + (1 + 17*s)*Power(t2,5) + 
                  3*Power(t2,6)) + 
               s2*(24 + (99 + 49*s)*t2 + 
                  (49 + 115*s - 55*Power(s,2))*Power(t2,2) - 
                  (122 + 74*s + 27*Power(s,2))*Power(t2,3) - 
                  (39 + 173*s + 7*Power(s,2))*Power(t2,4) + 
                  (-2 - 35*s + 19*Power(s,2))*Power(t2,5) + 
                  (-9 + 6*s)*Power(t2,6)))) + 
         Power(t1,8)*(-3 - 9*Power(s1,5) - 19*t2 - 10*s2*t2 - 
            12*Power(t2,2) - 14*s2*Power(t2,2) - 
            3*Power(s2,2)*Power(t2,2) + Power(s1,4)*(4 + 5*s2 + 34*t2) + 
            Power(s,3)*(-3 + s1 + 3*Power(s1,2) + 5*t2 - 20*s1*t2 + 
               21*Power(t2,2)) - 
            Power(s1,3)*(1 - 18*t2 + 25*Power(t2,2) + s2*(6 + 7*t2)) + 
            Power(s1,2)*(-1 - 38*Power(t2,2) + 
               Power(s2,2)*(-1 + 3*t2) + s2*(10 + 14*t2 - Power(t2,2))) \
+ s1*(-(Power(s2,2)*(-4 + t2)*t2) + s2*(2 + 13*t2 - 12*Power(t2,2)) - 
               2*(-5 + 5*t2 + 11*Power(t2,2))) - 
            Power(s,2)*(-6 + 17*Power(s1,3) + 
               Power(s1,2)*(6 - 3*s2 - 83*t2) + (33 + 7*s2)*t2 + 
               (71 + 18*s2)*Power(t2,2) + 
               s1*(s2*(5 - 6*t2) + 9*(-3 - 6*t2 + 8*Power(t2,2)))) + 
            s*(23*Power(s1,4) - 3*Power(s1,3)*(-3 + s2 + 32*t2) + 
               Power(s1,2)*(-44 + s2*(7 - 16*t2) - 106*t2 + 
                  76*Power(t2,2)) + 
               t2*(50 + 64*t2 + 3*Power(s2,2)*t2 + s2*(17 + 35*t2)) + 
               s1*(-39 + t2 - Power(s2,2)*t2 + 130*Power(t2,2) + 
                  s2*(6 - 13*t2 + 29*Power(t2,2))))) + 
         Power(t1,7)*(-6 - 5*Power(s1,6) + 4*t2 + 7*s2*t2 + 
            40*Power(t2,2) + 41*s2*Power(t2,2) + 
            8*Power(s2,2)*Power(t2,2) + 21*Power(t2,3) + 
            30*s2*Power(t2,3) + 13*Power(s2,2)*Power(t2,3) + 
            Power(s2,3)*Power(t2,3) + Power(s1,5)*(3 + 3*s2 + 38*t2) - 
            Power(s1,4)*(8 - 25*t2 + 67*Power(t2,2) + 7*s2*(1 + 2*t2)) + 
            Power(s1,3)*(-17 - 40*t2 - 112*Power(t2,2) + 
               30*Power(t2,3) + Power(s2,2)*(-13 + 6*t2) + 
               3*s2*(18 + 9*t2 + Power(t2,2))) + 
            Power(s,3)*(1 + Power(s1,3) + Power(s1,2)*(4 - 19*t2) + 
               21*t2 - 8*Power(t2,2) - 35*Power(t2,3) + 
               s1*(-3 - 11*t2 + 57*Power(t2,2))) + 
            Power(s,2)*(-12 - 8*Power(s1,4) - (63 + s2)*t2 + 
               (68 + 33*s2)*Power(t2,2) + 3*(44 + 15*s2)*Power(t2,3) + 
               Power(s1,3)*(-5 + 3*s2 + 90*t2) + 
               Power(s1,2)*(16 + s2*(-9 + t2) + 67*t2 - 
                  207*Power(t2,2)) - 
               s1*(-14 + s2 + 157*t2 - 16*s2*t2 + 201*Power(t2,2) + 
                  51*s2*Power(t2,2) - 110*Power(t2,3))) - 
            s1*(22 + 34*t2 - 30*Power(t2,2) + 
               2*Power(s2,3)*Power(t2,2) - 59*Power(t2,3) + 
               2*Power(s2,2)*t2*(1 + 13*t2 - 3*Power(t2,2)) + 
               s2*(-10 + 58*t2 + 29*Power(t2,2) - 30*Power(t2,3))) + 
            Power(s1,2)*(27 + t2 + Power(s2,3)*t2 - 10*Power(t2,2) + 
               85*Power(t2,3) - 2*Power(s2,2)*(2 + 9*Power(t2,2)) + 
               s2*(-3 - 33*t2 - 30*Power(t2,2) + 5*Power(t2,3))) + 
            s*(18 + 12*Power(s1,5) + Power(s1,4)*(1 - 2*s2 - 107*t2) - 
               6*(-6 + s2)*t2 - 
               2*(59 + 41*s2 + 4*Power(s2,2))*Power(t2,2) - 
               (130 + 89*s2 + 15*Power(s2,2))*Power(t2,3) + 
               Power(s1,3)*(-61 + s2*(22 - 15*t2) - 111*t2 + 
                  213*Power(t2,2)) + 
               Power(s1,2)*(-60 + 218*t2 + 365*Power(t2,2) - 
                  105*Power(t2,3) - 2*Power(s2,2)*(1 + 2*t2) + 
                  s2*(35 - 19*t2 + 96*Power(t2,2))) + 
               s1*(26 + 280*t2 + 67*Power(t2,2) - 242*Power(t2,3) + 
                  Power(s2,2)*t2*(-6 + 17*t2) + 
                  s2*(-14 + 17*t2 + 71*Power(t2,2) - 70*Power(t2,3))))) - 
         Power(t1,6)*(-9 + Power(s1,7) + Power(s1,6)*(-1 + s2 - 18*t2) - 
            47*t2 - 8*s2*t2 - 9*Power(t2,2) - 6*s2*Power(t2,2) + 
            6*Power(s2,2)*Power(t2,2) + 54*Power(t2,3) + 
            71*s2*Power(t2,3) + 28*Power(s2,2)*Power(t2,3) + 
            2*Power(s2,3)*Power(t2,3) + 33*Power(t2,4) + 
            41*s2*Power(t2,4) + 24*Power(s2,2)*Power(t2,4) + 
            4*Power(s2,3)*Power(t2,4) + 
            Power(s1,5)*(7 - 22*t2 + 63*Power(t2,2) + 3*s2*(4 + 3*t2)) + 
            Power(s1,4)*(47 + 45*t2 + 141*Power(t2,2) - 
               68*Power(t2,3) + Power(s2,2)*(22 + t2) - 
               s2*(109 + 65*t2 + 4*Power(t2,2))) - 
            Power(s1,3)*(37 + 52*t2 + 88*Power(t2,2) + 
               231*Power(t2,3) - 20*Power(t2,4) + 
               Power(s2,3)*(2 + 3*t2) - 
               3*Power(s2,2)*(5 - 2*t2 + 10*Power(t2,2)) + 
               s2*(-21 - 130*t2 - 47*Power(t2,2) + 19*Power(t2,3))) + 
            Power(s,3)*(-2 + 5*t2 + Power(s1,2)*(23 - 50*t2)*t2 + 
               62*Power(t2,2) + Power(t2,3) - 35*Power(t2,4) + 
               Power(s1,3)*(-2 + 6*t2) + 
               s1*(4 - 13*t2 - 47*Power(t2,2) + 90*Power(t2,3))) - 
            s1*(4 + 37*t2 + 46*Power(t2,2) - 51*Power(t2,3) + 
               11*Power(s2,3)*Power(t2,3) - 104*Power(t2,4) - 
               2*Power(s2,2)*t2*
                (6 - 23*t2 - 34*Power(t2,2) + 7*Power(t2,3)) + 
               s2*(-16 + 15*t2 + 205*Power(t2,2) + 32*Power(t2,3) - 
                  42*Power(t2,4))) + 
            Power(s1,2)*(34 + 26*t2 - 103*Power(t2,2) - 
               104*Power(t2,3) + 100*Power(t2,4) + 
               2*Power(s2,3)*t2*(-2 + 5*t2) - 
               Power(s2,2)*(10 + 51*t2 + 4*Power(t2,2) + 
                  45*Power(t2,3)) + 
               s2*(-4 + 189*t2 - 10*Power(t2,2) - 48*Power(t2,3) + 
                  10*Power(t2,4))) + 
            Power(s,2)*(3 + Power(s1,5) - (64 + 13*s2)*t2 - 
               (253 + 17*s2)*Power(t2,2) + (47 + 57*s2)*Power(t2,3) + 
               5*(31 + 12*s2)*Power(t2,4) - 
               Power(s1,4)*(-2 + s2 + 37*t2) - 
               Power(s1,2)*(24 + s2 - 75*t2 + 38*s2*t2 - 
                  257*Power(t2,2) - 48*s2*Power(t2,2) + 280*Power(t2,3)\
) + Power(s1,3)*(8 - 40*t2 + 195*Power(t2,2) + s2*(3 + 5*t2)) + 
               s1*(14 + 102*t2 - 363*Power(t2,2) - 408*Power(t2,3) + 
                  100*Power(t2,4) - 
                  s2*(5 + 4*t2 - 28*Power(t2,2) + 130*Power(t2,3)))) + 
            s*(9 - 2*Power(s1,6) + 2*(61 + 11*s2)*t2 + 
               (221 + 18*s2 - 6*Power(s2,2))*Power(t2,2) - 
               (141 + 155*s2 + 31*Power(s2,2))*Power(t2,3) - 
               (181 + 130*s2 + 30*Power(s2,2))*Power(t2,4) + 
               Power(s1,5)*(1 - s2 + 48*t2) + 
               Power(s1,4)*(38 + s2*(-29 + t2) + 52*t2 - 
                  202*Power(t2,2)) + 
               s1*(-32 + (31 + 14*s2 - 18*Power(s2,2))*t2 + 
                  (843 + 188*s2 - 16*Power(s2,2))*Power(t2,2) + 
                  (286 + 207*s2 + 64*Power(s2,2))*Power(t2,3) - 
                  5*(55 + 18*s2)*Power(t2,4)) + 
               Power(s1,3)*(40 - 256*t2 - 395*Power(t2,2) + 
                  253*Power(t2,3) + Power(s2,2)*(8 + 5*t2) + 
                  s2*(-33 + 53*t2 - 97*Power(t2,2))) + 
               Power(s1,2)*(-40 - 406*t2 + 361*Power(t2,2) + 
                  660*Power(t2,3) - 85*Power(t2,4) + 
                  Power(s2,2)*(-2 + 9*t2 - 35*Power(t2,2)) + 
                  s2*(43 + 148*t2 - 47*Power(t2,2) + 208*Power(t2,3))))) \
+ Power(t1,5)*(-(Power(s1,7)*(s2 - 3*t2)) + 
            Power(s1,6)*(5 + 14*t2 - 24*Power(t2,2) - 3*s2*(4 + t2)) + 
            Power(s1,5)*(-62 - 48*t2 - 91*Power(t2,2) + 
               51*Power(t2,3) - 6*Power(s2,2)*(2 + t2) + 
               s2*(90 + 98*t2 + 5*Power(t2,2))) + 
            Power(s,3)*t2*(-11 + 11*t2 + 98*Power(t2,2) + 
               20*Power(t2,3) - 21*Power(t2,4) + 
               Power(s1,3)*(-9 + 14*t2) + 
               Power(s1,2)*(-5 + 58*t2 - 70*Power(t2,2)) + 
               5*s1*(5 - 4*t2 - 21*Power(t2,2) + 17*Power(t2,3))) + 
            Power(s1,4)*(40 + 105*t2 + 108*Power(t2,2) + 
               244*Power(t2,3) - 37*Power(t2,4) + 
               Power(s2,3)*(2 + 4*t2) + 
               Power(s2,2)*(-30 + 5*t2 - 7*Power(t2,2)) + 
               s2*(-12 - 194*t2 - 129*Power(t2,2) + 24*Power(t2,3))) + 
            Power(s1,3)*(-2 + 125*t2 + Power(s2,3)*(12 - 13*t2)*t2 + 
               113*Power(t2,2) + 38*Power(t2,3) - 234*Power(t2,4) + 
               7*Power(t2,5) + 
               2*Power(s2,2)*
                (15 + 51*t2 + 26*Power(t2,2) + 29*Power(t2,3)) - 
               s2*(34 + 351*t2 + 24*Power(t2,2) - 57*Power(t2,3) + 
                  36*Power(t2,4))) + 
            t2*(-44 - 104*t2 - 22*Power(t2,2) + 58*Power(t2,3) + 
               44*Power(t2,4) + 
               2*Power(s2,3)*Power(t2,2)*(1 + 3*t2 + 3*Power(t2,2)) + 
               2*Power(s2,2)*t2*
                (-1 - 2*t2 + 17*Power(t2,2) + 13*Power(t2,3)) + 
               s2*(-11 - 87*t2 - 88*Power(t2,2) + 71*Power(t2,3) + 
                  40*Power(t2,4))) + 
            Power(s1,2)*(2 - 9*t2 - 51*Power(t2,2) - 311*Power(t2,3) - 
               248*Power(t2,4) + 65*Power(t2,5) + 
               2*Power(s2,3)*t2*(-8 - 9*t2 + 15*Power(t2,2)) + 
               Power(s2,2)*(2 - 7*t2 - 12*Power(t2,2) + 
                  11*Power(t2,3) - 59*Power(t2,4)) + 
               s2*(-15 + 102*t2 + 516*Power(t2,2) + 65*Power(t2,3) - 
                  71*Power(t2,4) + 10*Power(t2,5))) + 
            s1*(11 + 50*t2 - 44*Power(t2,2) - 119*Power(t2,3) + 
               21*Power(t2,4) + 112*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(2 + 2*t2 - 23*Power(t2,2)) + 
               Power(s2,2)*t2*
                (12 - 15*t2 - 160*Power(t2,2) - 100*Power(t2,3) + 
                  16*Power(t2,4)) + 
               s2*(-2 + 57*t2 + 77*Power(t2,2) - 270*Power(t2,3) - 
                  26*Power(t2,4) + 38*Power(t2,5))) + 
            Power(s,2)*(6 + 4*Power(s1,5)*t2 - 2*(-16 + s2)*t2 - 
               (130 + 73*s2)*Power(t2,2) - (533 + 63*s2)*Power(t2,3) + 
               (-51 + 35*s2)*Power(t2,4) + (118 + 45*s2)*Power(t2,5) + 
               Power(s1,4)*(-4 + s2 + 9*t2 - 3*s2*t2 - 
                  67*Power(t2,2)) + 
               Power(s1,2)*(13 - 2*(52 + 3*s2)*t2 + 
                  (128 - 77*s2)*Power(t2,2) + 
                  122*(4 + s2)*Power(t2,3) - 220*Power(t2,4)) + 
               s1*(-17 + (47 - 33*s2)*t2 + 10*(30 + s2)*Power(t2,2) + 
                  (-396 + 71*s2)*Power(t2,3) - 
                  165*(3 + s2)*Power(t2,4) + 54*Power(t2,5)) + 
               Power(s1,3)*(2 + 36*t2 - 120*Power(t2,2) + 
                  220*Power(t2,3) + s2*(-1 + 20*t2 - 13*Power(t2,2)))) + 
            s*(-9 + Power(s1,6)*(s2 - 7*t2) + (19 + 18*s2)*t2 + 
               (301 + 171*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (586 + 180*s2 - 15*Power(s2,2))*Power(t2,3) - 
               (58 + 129*s2 + 41*Power(s2,2))*Power(t2,4) - 
               (173 + 120*s2 + 30*Power(s2,2))*Power(t2,5) + 
               Power(s1,5)*(-19 - 16*t2 + 75*Power(t2,2) + 
                  s2*(17 + 4*t2)) + 
               Power(s1,4)*(4 + 127*t2 + 184*Power(t2,2) - 
                  197*Power(t2,3) - Power(s2,2)*(7 + 4*t2) + 
                  s2*(11 - 76*t2 + 24*Power(t2,2))) + 
               Power(s1,3)*(-4 + 250*t2 - 355*Power(t2,2) - 
                  656*Power(t2,3) + 172*Power(t2,4) + 
                  Power(s2,2)*(8 - t2 + 29*Power(t2,2)) - 
                  s2*(29 + 181*t2 - 23*Power(t2,2) + 182*Power(t2,3))) + 
               Power(s1,2)*(35 - 58*t2 - 1045*Power(t2,2) + 
                  107*Power(t2,3) + 692*Power(t2,4) - 40*Power(t2,5) + 
                  Power(s2,2)*t2*(39 + 52*t2 - 97*Power(t2,2)) + 
                  s2*(-15 + 96*t2 + 159*Power(t2,2) - 152*Power(t2,3) + 
                     227*Power(t2,4))) + 
               s1*(-6 - 237*t2 - 269*Power(t2,2) + 1341*Power(t2,3) + 
                  537*Power(t2,4) - 195*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-4 - 64*t2 - 27*Power(t2,2) + 106*Power(t2,3)) + 
                  s2*(13 + 31*t2 + 188*Power(t2,2) + 463*Power(t2,3) + 
                     349*Power(t2,4) - 65*Power(t2,5))))) + 
         Power(t1,2)*t2*(-6 + 3*(-13 - s2 + s*(5 + 6*s2))*t2 + 
            Power(s1,7)*s2*Power(t2,2) - 
            (74 - 22*s2 + 18*Power(s2,2) + Power(s,2)*(17 + 28*s2) + 
               2*s*(-48 - 55*s2 + 14*Power(s2,2)))*Power(t2,2) + 
            (-2 + 14*Power(s,3) + 4*s2 + 11*Power(s2,2) + 
               3*Power(s2,3) + 3*Power(s,2)*(-68 + 19*s2) + 
               s*(365 - 47*s2 - 101*Power(s2,2)))*Power(t2,3) + 
            (59 - 5*Power(s,3) + 30*s2 + 111*Power(s2,2) + 
               4*Power(s2,3) + 2*Power(s,2)*(-31 + 53*s2) + 
               s*(124 - 455*s2 - 59*Power(s2,2)))*Power(t2,4) + 
            (98 + 4*Power(s,3) + 16*s2 + 120*Power(s2,2) - 
               5*Power(s2,3) + Power(s,2)*(191 + 3*s2) + 
               s*(-373 - 318*s2 + 3*Power(s2,2)))*Power(t2,5) + 
            (-7*Power(s,3) + Power(s,2)*(57 + 17*s2) + 
               s2*(-10 + 14*s2 + 3*Power(s2,2)) - 
               s*(67 + 59*s2 + 13*Power(s2,2)))*Power(t2,6) - 
            (8 + 2*Power(s,2) + 5*s2 + 2*Power(s2,2) - 2*s*(5 + 2*s2))*
             Power(t2,7) - Power(s1,6)*t2*
             (-12 - 11*t2 - (-3 + s)*Power(t2,2) + 
               s2*(12 + (6 + s)*t2 + 4*Power(t2,2))) + 
            Power(s1,4)*(-18 - 6*(16 + s)*t2 + 
               (41 + 46*s + 2*Power(s,2))*Power(t2,2) + 
               (114 + 48*s - 7*Power(s,2))*Power(t2,3) + 
               (90 - 50*s + 4*Power(s,2))*Power(t2,4) + 
               3*(-3 + s)*Power(t2,5) - 
               Power(s2,3)*(15 + 42*t2 - 8*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,2)*(3 + 6*(3 + s)*t2 - 
                  (73 + 25*s)*Power(t2,2) + (-7 + 4*s)*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s2*(12 + 3*(46 + 5*s)*t2 + 
                  (8 - 45*s + 7*Power(s,2))*Power(t2,2) - 
                  (144 + 11*s + 2*Power(s,2))*Power(t2,3) + 
                  (8 - 11*s)*Power(t2,4) - 4*Power(t2,5))) + 
            Power(s1,3)*(-6 + 3*(21 + 4*s)*t2 + 
               (273 + 136*s + 21*Power(s,2))*Power(t2,2) + 
               2*(108 - 53*s - 16*Power(s,2) + 3*Power(s,3))*
                Power(t2,3) - 
               (23 + 148*s - 57*Power(s,2) + 2*Power(s,3))*
                Power(t2,4) + (-89 + 65*s - 5*Power(s,2))*Power(t2,5) - 
               (-3 + s)*Power(t2,6) + 
               3*Power(s2,3)*
                (8 + 37*t2 + 19*Power(t2,2) - 9*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s2,2)*(33 + 9*(22 + 5*s)*t2 + 
                  (87 + 37*s)*Power(t2,2) - (127 + 63*s)*Power(t2,3) + 
                  2*(-11 + 7*s)*Power(t2,4) + 6*Power(t2,5)) + 
               s2*(57 + (66 - 63*s)*t2 + 
                  (-336 - 63*s + 17*Power(s,2))*Power(t2,2) - 
                  (196 + 48*s + 33*Power(s,2))*Power(t2,3) + 
                  10*(6 - 6*s + Power(s,2))*Power(t2,4) + 
                  (-30 + 11*s)*Power(t2,5) + Power(t2,6))) + 
            Power(s1,2)*(9 + (96 + 45*s)*t2 + 
               (9 + 4*s - 55*Power(s,2))*Power(t2,2) + 
               (-280 - 525*s + 50*Power(s,2) + 32*Power(s,3))*
                Power(t2,3) + 
               (-368 + 166*s + 58*Power(s,2) - 31*Power(s,3))*
                Power(t2,4) + 
               (-93 + 228*s - 89*Power(s,2) + 4*Power(s,3))*
                Power(t2,5) + (35 - 28*s + 2*Power(s,2))*Power(t2,6) - 
               Power(s2,3)*(12 + 45*t2 + 39*Power(t2,2) + 
                  5*Power(t2,3) - 33*Power(t2,4) + 6*Power(t2,5)) + 
               Power(s2,2)*(24 + 3*(39 + 16*s)*t2 + 
                  5*(50 + 33*s)*Power(t2,2) + 27*(6 + s)*Power(t2,3) - 
                  2*(69 + 47*s)*Power(t2,4) + 
                  (-37 + 16*s)*Power(t2,5) + 2*Power(t2,6)) + 
               s2*(-69 + (-312 + 75*s)*t2 + 
                  (-151 + 22*s - 93*Power(s,2))*Power(t2,2) + 
                  (412 + 120*s - 51*Power(s,2))*Power(t2,3) + 
                  (217 + 195*s + 89*Power(s,2))*Power(t2,4) + 
                  (-15 + 115*s - 14*Power(s,2))*Power(t2,5) + 
                  (18 - 4*s)*Power(t2,6))) + 
            Power(s1,5)*(12 + 3*Power(s2,3) - 12*(2 + s)*t2 - 
               (78 + 11*s)*Power(t2,2) - 
               (44 - 11*s + Power(s,2))*Power(t2,3) - 
               3*(-3 + s)*Power(t2,4) + 
               Power(s2,2)*(6 + 3*(5 + s)*t2 + 3*Power(t2,2) - 
                  2*Power(t2,3)) + 
               s2*(-18 + 91*Power(t2,2) + 12*Power(t2,3) + 
                  6*Power(t2,4) + s*t2*(12 + 9*t2 + 5*Power(t2,2)))) + 
            s1*(9 - 6*(2 + 9*s)*t2 + 
               (-74 - 281*s + 49*Power(s,2))*Power(t2,2) + 
               (-257 + 56*s + 134*Power(s,2) - 52*Power(s,3))*
                Power(t2,3) - 
               (94 - 660*s + 177*Power(s,2) + 25*Power(s,3))*
                Power(t2,4) + 
               (161 - 9*s - 129*Power(s,2) + 41*Power(s,3))*
                Power(t2,5) - 
               2*(-29 + 53*s - 18*Power(s,2) + Power(s,3))*Power(t2,6) + 
               (-3 + 2*s)*Power(t2,7) + 
               Power(s2,3)*t2*
                (-24 - 94*t2 - 53*Power(t2,2) - 8*Power(t2,3) - 
                  17*Power(t2,4) + 2*Power(t2,5)) + 
               s2*(18 + (123 - 57*s)*t2 + 
                  (87 - 12*s + 97*Power(s,2))*Power(t2,2) + 
                  (119 + 161*s + 89*Power(s,2))*Power(t2,3) + 
                  (-103 + 225*s - 43*Power(s,2))*Power(t2,4) - 
                  (27 + 10*s + 90*Power(s,2))*Power(t2,5) + 
                  (25 - 57*s + 6*Power(s,2))*Power(t2,6) - 2*Power(t2,7)\
) + Power(s2,2)*t2*(48 + 169*t2 - 154*Power(t2,2) - 179*Power(t2,3) + 
                  55*Power(t2,4) + 21*Power(t2,5) - 
                  s*(12 + 85*t2 + 23*Power(t2,2) - 49*Power(t2,3) - 
                     66*Power(t2,4) + 6*Power(t2,5))))) + 
         Power(t1,3)*(2 + (23 + s2 - s*(17 + 6*s2))*t2 + 
            (Power(s,2)*(29 + 22*s2) - 2*(-23 + s2 + Power(s2,2)) + 
               2*s*(-59 - 17*s2 + 2*Power(s2,2)))*Power(t2,2) - 
            (31 + 26*Power(s,3) + 52*s2 + 15*Power(s2,2) + 
               5*Power(s2,3) + Power(s,2)*(-209 + 41*s2) + 
               s*(294 - 133*s2 - 51*Power(s2,2)))*Power(t2,3) + 
            (-96 + 11*Power(s,3) - 130*s2 - 125*Power(s2,2) - 
               2*Power(s2,3) - Power(s,2)*(2 + 187*s2) + 
               s*(72 + 655*s2 + 59*Power(s2,2)))*Power(t2,4) + 
            (-85 + 33*Power(s,3) - 105*s2 - 140*Power(s2,2) + 
               7*Power(s2,3) - Power(s,2)*(473 + 63*s2) + 
               s*(748 + 508*s2 + 5*Power(s2,2)))*Power(t2,5) + 
            (22 + 20*Power(s,3) + 26*s2 - 12*Power(s2,2) - 
               Power(s2,3) - Power(s,2)*(122 + 33*s2) + 
               2*s*(55 + 36*s2 + 7*Power(s2,2)))*Power(t2,6) + 
            (25 - Power(s,3) + 14*s2 + 9*Power(s2,2) + Power(s2,3) + 
               Power(s,2)*(16 + 3*s2) - s*(44 + 25*s2 + 3*Power(s2,2)))*
             Power(t2,7) + Power(s1,7)*t2*
             (-4 - 4*t2 + Power(t2,2) + s2*(4 + t2)) + 
            Power(s1,6)*(-4 + 19*Power(t2,2) + 22*Power(t2,3) - 
               3*Power(t2,4) - 2*Power(s2,2)*(1 + 3*t2) + 
               s*t2*(4 + 4*t2 - 5*Power(t2,2)) + 
               s2*(6 - 4*(-2 + s)*t2 - (30 + s)*Power(t2,2) + 
                  Power(t2,3))) + 
            Power(s1,5)*(6 + 2*(42 + 5*s)*t2 + 
               (50 - 11*s)*Power(t2,2) + 
               (11 - 34*s + 4*Power(s,2))*Power(t2,3) + 
               3*(-20 + 7*s)*Power(t2,4) + 3*Power(t2,5) + 
               Power(s2,3)*(1 + 8*t2) + 
               Power(s2,2)*(-4 - (-7 + s)*t2 + 
                  (25 + 2*s)*Power(t2,2) + 4*Power(t2,3)) - 
               s2*(4 + 12*(8 + s)*t2 + (89 - 5*s)*Power(t2,2) + 
                  (-38 + 4*s)*Power(t2,3) + 12*Power(t2,4))) + 
            Power(s1,4)*(2 - 2*(23 + 5*s)*t2 - 
               (255 + 104*s + 8*Power(s,2))*Power(t2,2) + 
               (-259 - 28*s + 13*Power(s,2))*Power(t2,3) + 
               (-137 + 165*s - 25*Power(s,2))*Power(t2,4) + 
               (71 - 28*s)*Power(t2,5) - Power(t2,6) + 
               Power(s2,3)*(-3 - 16*t2 - 28*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s2,2)*(23 + 2*(47 + s)*t2 + 
                  (39 + 25*s)*Power(t2,2) - (43 + 12*s)*Power(t2,3) - 
                  19*Power(t2,4)) + 
               s2*(-16 + (-56 + s)*t2 + 
                  (300 + 127*s - 7*Power(s,2))*Power(t2,2) + 
                  (325 + 40*s + 3*Power(s,2))*Power(t2,3) + 
                  (-24 + 38*s)*Power(t2,4) + 18*Power(t2,5))) - 
            Power(s1,3)*(4 + (67 + 4*s)*t2 + 
               (133 + 116*s + 25*Power(s,2))*Power(t2,2) + 
               (-97 - 462*s - 70*Power(s,2) + 14*Power(s,3))*
                Power(t2,3) - 
               (322 + 171*s - 142*Power(s,2) + 9*Power(s,3))*
                Power(t2,4) + 
               (-241 + 275*s - 42*Power(s,2))*Power(t2,5) + 
               (32 - 13*s)*Power(t2,6) + 
               Power(s2,3)*(-2 + 17*t2 + 19*Power(t2,2) - 
                  43*Power(t2,3) + 21*Power(t2,4)) + 
               Power(s2,2)*(23 - 13*(-8 + s)*t2 + 
                  3*(43 + 9*s)*Power(t2,2) + 
                  (154 + 95*s)*Power(t2,3) - (15 + 47*s)*Power(t2,4) - 
                  28*Power(t2,5)) + 
               s2*(-11 - (198 + 53*s)*t2 + 
                  (-360 - 21*s + Power(s,2))*Power(t2,2) + 
                  (293 + 179*s - 50*Power(s,2))*Power(t2,3) + 
                  (276 - 31*s + 34*Power(s,2))*Power(t2,4) + 
                  (-56 + 67*s)*Power(t2,5) + 9*Power(t2,6))) + 
            Power(s1,2)*(1 + (16 - 31*s)*t2 + 
               3*(35 + 58*s + 31*Power(s,2))*Power(t2,2) + 
               (476 + 593*s - 156*Power(s,2) - 38*Power(s,3))*
                Power(t2,3) + 
               (385 - 794*s - 23*Power(s,2) + 68*Power(s,3))*
                Power(t2,4) - 
               (43 + 467*s - 299*Power(s,2) + 23*Power(s,3))*
                Power(t2,5) + 
               (-141 + 153*s - 23*Power(s,2))*Power(t2,6) - 
               (-3 + s)*Power(t2,7) + 
               Power(s2,3)*t2*
                (25 + 97*t2 + 19*Power(t2,2) - 43*Power(t2,3) + 
                  25*Power(t2,4)) + 
               Power(s2,2)*(6 + (11 - 20*s)*t2 - 
                  (202 + 71*s)*Power(t2,2) + (-6 + 43*s)*Power(t2,3) + 
                  3*(89 + 45*s)*Power(t2,4) + 
                  (59 - 71*s)*Power(t2,5) - 15*Power(t2,6)) + 
               s2*(9 + (2 - 63*s)*t2 + 
                  5*(-31 - 30*s + 11*Power(s,2))*Power(t2,2) + 
                  (-698 - 165*s + 40*Power(s,2))*Power(t2,3) - 
                  (59 + 316*s + 134*Power(s,2))*Power(t2,4) + 
                  (54 - 257*s + 69*Power(s,2))*Power(t2,5) + 
                  (-54 + 38*s)*Power(t2,6) + Power(t2,7))) + 
            s1*(-3 + 6*(-1 + 8*s)*t2 + 
               (80 + 181*s - 89*Power(s,2))*Power(t2,2) + 
               (127 - 474*s - 60*Power(s,2) + 78*Power(s,3))*
                Power(t2,3) + 
               (-41 - 1085*s + 387*Power(s,2) + 17*Power(s,3))*
                Power(t2,4) + 
               (-335 + 468*s + 107*Power(s,2) - 101*Power(s,3))*
                Power(t2,5) + 
               (-99 + 325*s - 159*Power(s,2) + 15*Power(s,3))*
                Power(t2,6) + (23 - 20*s + 2*Power(s,2))*Power(t2,7) + 
               Power(s2,3)*Power(t2,2)*
                (2 + 13*t2 + 16*Power(t2,2) + 21*Power(t2,3) - 
                  11*Power(t2,4)) + 
               s2*(-6 + (-61 + 31*s)*t2 + 
                  (-149 + 12*s - 69*Power(s,2))*Power(t2,2) + 
                  (167 - 105*s - 132*Power(s,2))*Power(t2,3) + 
                  (346 + 27*s + 90*Power(s,2))*Power(t2,4) + 
                  (10 + 251*s + 166*Power(s,2))*Power(t2,5) + 
                  (-34 + 197*s - 41*Power(s,2))*Power(t2,6) + 
                  (10 - 4*s)*Power(t2,7)) + 
               Power(s2,2)*t2*
                (-2 + 73*t2 + 206*Power(t2,2) + 117*Power(t2,3) - 
                  165*Power(t2,4) - 59*Power(t2,5) + 2*Power(t2,6) + 
                  s*(6 + 77*t2 + 73*Power(t2,2) - 101*Power(t2,3) - 
                     94*Power(t2,4) + 37*Power(t2,5))))) + 
         Power(t1,4)*(-5 - 10*t2 + 2*s2*t2 + 54*Power(t2,2) + 
            59*s2*Power(t2,2) + 5*Power(s2,2)*Power(t2,2) + 
            117*Power(t2,3) + 178*s2*Power(t2,3) + 
            52*Power(s2,2)*Power(t2,3) + 41*Power(t2,4) + 
            156*s2*Power(t2,4) + 72*Power(s2,2)*Power(t2,4) - 
            6*Power(s2,3)*Power(t2,4) - 48*Power(t2,5) - 
            49*s2*Power(t2,5) - 11*Power(s2,2)*Power(t2,5) - 
            5*Power(s2,3)*Power(t2,5) - 42*Power(t2,6) - 
            28*s2*Power(t2,6) - 19*Power(s2,2)*Power(t2,6) - 
            4*Power(s2,3)*Power(t2,6) - 
            Power(s1,7)*(-4 - 4*t2 + 3*Power(t2,2) + s2*(4 + t2)) + 
            Power(s1,6)*(-26 - 31*t2 - 32*Power(t2,2) + 14*Power(t2,3) - 
               2*Power(s2,2)*(1 + t2) + s2*(26 + 48*t2 + 5*Power(t2,2))) \
+ Power(s,3)*Power(t2,2)*(24 - 16*Power(s1,3)*(-1 + t2) - 14*t2 - 
               85*Power(t2,2) - 29*Power(t2,3) + 7*Power(t2,4) + 
               Power(s1,2)*(22 - 82*t2 + 55*Power(t2,2)) + 
               s1*(-62 + 7*t2 + 135*Power(t2,2) - 48*Power(t2,3))) + 
            Power(s1,5)*(12 + 70*t2 + 2*Power(s2,3)*t2 + 
               73*Power(t2,2) + 117*Power(t2,3) - 20*Power(t2,4) + 
               Power(s2,2)*(-15 - 13*t2 + 4*Power(t2,2)) + 
               s2*(8 - 76*t2 - 132*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s1,4)*(22 + 172*t2 + 82*Power(t2,2) + Power(t2,3) - 
               194*Power(t2,4) + 10*Power(t2,5) + 
               Power(s2,3)*(4 + 16*t2 - 6*Power(t2,2)) + 
               Power(s2,2)*(9 + 68*t2 + 55*Power(t2,2) + 
                  21*Power(t2,3)) - 
               s2*(36 + 288*t2 + 89*Power(t2,2) - 99*Power(t2,3) + 
                  33*Power(t2,4))) + 
            Power(s1,3)*(-13 - 117*t2 - 350*Power(t2,2) - 
               394*Power(t2,3) - 246*Power(t2,4) + 124*Power(t2,5) - 
               Power(t2,6) + Power(s2,3)*
                (-4 - 34*t2 - 45*Power(t2,2) + 25*Power(t2,3)) + 
               Power(s2,2)*(21 + 103*t2 - 22*Power(t2,2) - 
                  70*Power(t2,3) - 56*Power(t2,4)) + 
               s2*(-6 + 34*t2 + 713*Power(t2,2) + 290*Power(t2,3) - 
                  65*Power(t2,4) + 27*Power(t2,5))) + 
            s1*(-1 - 52*t2 - 57*Power(t2,2) + 93*Power(t2,3) + 
               294*Power(t2,4) + 62*Power(t2,5) - 70*Power(t2,6) + 
               Power(s2,3)*Power(t2,2)*
                (10 - 8*t2 - 11*Power(t2,2) + 23*Power(t2,3)) + 
               Power(s2,2)*Power(t2,2)*
                (-83 + 11*t2 + 231*Power(t2,2) + 95*Power(t2,3) - 
                  9*Power(t2,4)) + 
               s2*(14 + 14*t2 - 239*Power(t2,2) - 339*Power(t2,3) + 
                  134*Power(t2,4) + 29*Power(t2,5) - 24*Power(t2,6))) - 
            Power(s1,2)*(-7 - 2*t2 + 178*Power(t2,2) + 88*Power(t2,3) - 
               302*Power(t2,4) - 265*Power(t2,5) + 22*Power(t2,6) + 
               Power(s2,3)*t2*
                (2 - 12*t2 - 33*Power(t2,2) + 40*Power(t2,3)) + 
               Power(s2,2)*(13 + 82*t2 + 87*Power(t2,2) + 
                  189*Power(t2,3) + 46*Power(t2,4) - 42*Power(t2,5)) + 
               s2*(2 - 173*t2 - 251*Power(t2,2) + 390*Power(t2,3) + 
                  93*Power(t2,4) - 80*Power(t2,5) + 5*Power(t2,6))) + 
            Power(s,2)*t2*(-22 - 116*t2 - 6*Power(s1,5)*t2 + 
               110*Power(t2,2) + 653*Power(t2,3) + 134*Power(t2,4) - 
               57*Power(t2,5) + 
               Power(s1,4)*(10 + s2 - 15*t2 + s2*t2 + 59*Power(t2,2)) + 
               s2*(-6 + 14*t2 + 165*Power(t2,2) + 97*Power(t2,3) + 
                  15*Power(t2,4) - 18*Power(t2,5)) + 
               Power(s1,3)*(7 - 71*t2 + 179*Power(t2,2) - 
                  135*Power(t2,3) + s2*(-2 - 44*t2 + 39*Power(t2,2))) - 
               Power(s1,2)*(61 - 185*t2 + 80*Power(t2,2) + 
                  512*Power(t2,3) - 99*Power(t2,4) + 
                  s2*(10 + 6*t2 - 117*Power(t2,2) + 133*Power(t2,3))) + 
               s1*(66 - 37*t2 - 458*Power(t2,2) + 144*Power(t2,3) + 
                  366*Power(t2,4) - 16*Power(t2,5) + 
                  s2*(17 + 95*t2 - 61*Power(t2,2) - 149*Power(t2,3) + 
                     114*Power(t2,4)))) + 
            s*(6 + (54 + 5*s2)*t2 + 
               (77 - 90*s2 - 7*Power(s2,2))*Power(t2,2) - 
               (316 + 482*s2 + 23*Power(s2,2))*Power(t2,3) + 
               (-862 - 435*s2 + 7*Power(s2,2))*Power(t2,4) + 
               (-66 + 9*s2 + 14*Power(s2,2))*Power(t2,5) + 
               (110 + 71*s2 + 15*Power(s2,2))*Power(t2,6) + 
               Power(s1,6)*(-4 - 4*t2 + 9*Power(t2,2) + s2*(4 + t2)) - 
               Power(s1,5)*(-2 - 41*t2 - 42*Power(t2,2) + 
                  57*Power(t2,3) + Power(s2,2)*(1 + 2*t2) + 
                  3*s2*t2*(11 + 2*t2)) + 
               Power(s1,4)*(4 + 44*t2 - 103*Power(t2,2) - 
                  254*Power(t2,3) + 104*Power(t2,4) + 
                  3*Power(s2,2)*t2*(1 + 4*t2) - 
                  s2*(3 + 82*t2 - 30*Power(t2,2) + 48*Power(t2,3))) + 
               Power(s1,3)*(-4 + 35*t2 - 539*Power(t2,2) + 
                  103*Power(t2,3) + 579*Power(t2,4) - 66*Power(t2,5) + 
                  Power(s2,2)*
                   (1 + 16*t2 + 57*Power(t2,2) - 57*Power(t2,3)) + 
                  s2*(-12 + 49*t2 + 324*Power(t2,2) + 13*Power(t2,3) + 
                     159*Power(t2,4))) + 
               Power(s1,2)*(12 - 153*t2 - 208*Power(t2,2) + 
                  1308*Power(t2,3) + 362*Power(t2,4) - 430*Power(t2,5) + 
                  10*Power(t2,6) + 
                  Power(s2,2)*t2*
                   (-9 - 91*t2 - 109*Power(t2,2) + 121*Power(t2,3)) + 
                  s2*(17 + 117*t2 + 4*Power(t2,2) + 106*Power(t2,3) + 
                     279*Power(t2,4) - 132*Power(t2,5))) - 
               s1*(16 + 22*t2 - 542*Power(t2,2) - 866*Power(t2,3) + 
                  1158*Power(t2,4) + 552*Power(t2,5) - 84*Power(t2,6) + 
                  Power(s2,2)*t2*
                   (13 + 31*t2 - 104*Power(t2,2) - 62*Power(t2,3) + 
                     89*Power(t2,4)) + 
                  s2*(6 + 47*t2 + 22*Power(t2,2) + 279*Power(t2,3) + 
                     513*Power(t2,4) + 347*Power(t2,5) - 25*Power(t2,6))))\
))*T3(t2,t1))/((-1 + s2)*(-s + s2 - t1)*Power(t1 - t2,2)*
       Power(-1 + s1 + t1 - t2,2)*Power(s1*t1 - t2,3)*(-1 + t2)) + 
    (8*(-2 + Power(-1 + s,3)*Power(t1,10) - 
         Power(s1,7)*(-1 + t1)*Power(t1,2)*
          (t1*(-7 - 2*t1 + Power(t1,2)) + s2*(1 + 6*t1 + Power(t1,2))) - 
         8*t2 - s*t2 - s2*t2 + 6*s*s2*t2 - 16*Power(t2,2) + 
         10*s*Power(t2,2) + Power(s,2)*Power(t2,2) - 4*s2*Power(t2,2) + 
         19*s*s2*Power(t2,2) - 4*Power(s,2)*s2*Power(t2,2) + 
         2*Power(s2,2)*Power(t2,2) - 4*s*Power(s2,2)*Power(t2,2) + 
         23*Power(t2,3) - 29*s*Power(t2,3) + 11*Power(s,2)*Power(t2,3) + 
         4*Power(s,3)*Power(t2,3) - 39*s2*Power(t2,3) + 
         89*s*s2*Power(t2,3) - 27*Power(s,2)*s2*Power(t2,3) + 
         8*Power(s2,2)*Power(t2,3) - 26*s*Power(s2,2)*Power(t2,3) + 
         Power(s2,3)*Power(t2,3) + 11*Power(t2,4) - 4*s*Power(t2,4) - 
         4*Power(s,2)*Power(t2,4) + 8*Power(s,3)*Power(t2,4) - 
         16*s2*Power(t2,4) + 45*s*s2*Power(t2,4) - 
         18*Power(s,2)*s2*Power(t2,4) + Power(s2,2)*Power(t2,4) - 
         16*s*Power(s2,2)*Power(t2,4) + 2*Power(s2,3)*Power(t2,4) - 
         7*Power(t2,5) + 10*s*Power(t2,5) + 5*Power(s,2)*Power(t2,5) + 
         2*Power(s,3)*Power(t2,5) - 4*s2*Power(t2,5) - 
         17*s*s2*Power(t2,5) - Power(s,2)*s2*Power(t2,5) + 
         10*Power(s2,2)*Power(t2,5) - Power(s2,3)*Power(t2,5) - 
         Power(t2,6) - 2*s*Power(t2,6) + 7*Power(s,2)*Power(t2,6) - 
         10*s*s2*Power(t2,6) + 3*Power(s2,2)*Power(t2,6) - 
         (-1 + s)*Power(t1,9)*
          (7 + (2 + 3*s2)*t2 + 4*Power(s,2)*(1 + t2) - 
            s*(11 + 3*(3 + s2)*t2)) + 
         Power(t1,8)*(-18 - (13 + 19*s2)*t2 - 
            (3 + 5*s2 + 3*Power(s2,2))*Power(t2,2) + 
            Power(s,3)*(3 + 14*t2 + 6*Power(t2,2)) - 
            Power(s,2)*(21 + 2*(27 + 8*s2)*t2 + 
               (23 + 9*s2)*Power(t2,2)) + 
            s*(36 + 7*(8 + 5*s2)*t2 + 
               (22 + 17*s2 + 3*Power(s2,2))*Power(t2,2))) + 
         Power(t1,7)*(16 + (22 + 46*s2)*t2 + 
            (16 + 26*s2 + 17*Power(s2,2))*Power(t2,2) + 
            (5 + 6*s2 + 4*Power(s2,2) + Power(s2,3))*Power(t2,3) - 
            Power(s,3)*(-6 + 6*t2 + 17*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s,2)*(-9 + (39 + 29*s2)*t2 + 
               (74 + 39*s2)*Power(t2,2) + 3*(7 + 3*s2)*Power(t2,3)) - 
            s*(12 + (66 + 75*s2)*t2 + 
               (88 + 82*s2 + 17*Power(s2,2))*Power(t2,2) + 
               (28 + 20*s2 + 6*Power(s2,2))*Power(t2,3))) + 
         Power(t1,6)*(14 + (21 - 46*s2)*t2 - 
            3*(9 + 7*s2 + 13*Power(s2,2))*Power(t2,2) - 
            (19 + 26*s2 + 16*Power(s2,2) + 5*Power(s2,3))*Power(t2,3) - 
            (7 + 5*s2 + 3*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
            Power(s,3)*(-9 - 28*t2 - 2*Power(t2,2) + 7*Power(t2,3) + 
               Power(t2,4)) - 
            Power(s,2)*(-45 + (-105 + 8*s2)*t2 + 
               (-16 + 49*s2)*Power(t2,2) + (44 + 27*s2)*Power(t2,3) + 
               (10 + 3*s2)*Power(t2,4)) + 
            s*(-54 + (-96 + 53*s2)*t2 + 
               (19 + 108*s2 + 39*Power(s2,2))*Power(t2,2) + 
               (81 + 71*s2 + 25*Power(s2,2))*Power(t2,3) + 
               (20 + 13*s2 + 3*Power(s2,2))*Power(t2,4))) + 
         Power(t1,5)*(-42 - 4*(28 + s2)*t2 + 
            (16 - 106*s2 + 43*Power(s2,2))*Power(t2,2) + 
            (22 + 22*s2 + 2*Power(s2,2) + 11*Power(s2,3))*Power(t2,3) + 
            (23 + 19*s2 + 7*Power(s2,2) + 3*Power(s2,3))*Power(t2,4) + 
            (5 + 2*s2 + 2*Power(s2,2))*Power(t2,5) + 
            Power(s,3)*t2*(35 + 50*t2 + 11*Power(t2,2) + Power(t2,3)) + 
            Power(s,2)*(-27 - (181 + 37*s2)*t2 - 
               (272 + 31*s2)*Power(t2,2) + (-86 + 9*s2)*Power(t2,3) + 
               (6 + s2)*Power(t2,4) + 2*Power(t2,5)) - 
            s*(-72 - (281 + 49*s2)*t2 + 
               (-294 - 103*s2 + 43*Power(s2,2))*Power(t2,2) + 
               (-68 + 3*s2 + 36*Power(s2,2))*Power(t2,3) + 
               (38 + 25*s2 + 5*Power(s2,2))*Power(t2,4) + 
               (7 + 4*s2)*Power(t2,5))) - 
         Power(t1,4)*(-28 - (133 + 52*s2)*t2 + 
            5*(5 - 57*s2 + 3*Power(s2,2))*Power(t2,2) + 
            (-25 - 13*s2 - 80*Power(s2,2) + 13*Power(s2,3))*
             Power(t2,3) + (27 + 25*s2 - 21*Power(s2,2) + 
               3*Power(s2,3))*Power(t2,4) + 
            (14 + 8*s2 + 3*Power(s2,2) - Power(s2,3))*Power(t2,5) + 
            (1 + s2)*Power(t2,6) + 
            Power(s,3)*(-5 + 3*t2 + 55*Power(t2,2) + 36*Power(t2,3) + 
               7*Power(t2,4) + Power(t2,5)) - 
            Power(s,2)*(-15 + 8*(7 + 5*s2)*t2 + 
               7*(41 + 20*s2)*Power(t2,2) + (298 + 69*s2)*Power(t2,3) + 
               (74 + 15*s2)*Power(t2,4) + (4 + 3*s2)*Power(t2,5)) + 
            s*(12 + (189 + 109*s2)*t2 + 
               (366 + 428*s2 - 13*Power(s2,2))*Power(t2,2) + 
               (325 + 308*s2 - Power(s2,2))*Power(t2,3) + 
               (84 + 66*s2 + 5*Power(s2,2))*Power(t2,4) + 
               (-8 + s2 + 3*Power(s2,2))*Power(t2,5) - Power(t2,6))) + 
         Power(t1,2)*(-21 + (-37 + 14*s2)*t2 + 
            (-121 + 129*s2 + 23*Power(s2,2))*Power(t2,2) + 
            (155 - 148*s2 + 136*Power(s2,2) + Power(s2,3))*Power(t2,3) + 
            (31 - 46*s2 + 77*Power(s2,2) + 6*Power(s2,3))*Power(t2,4) + 
            (1 - 24*s2 + 33*Power(s2,2))*Power(t2,5) + 
            (4 - 7*s2 + 3*Power(s2,2))*Power(t2,6) + 
            Power(s,3)*t2*(5 + 7*t2 + 3*Power(t2,2) - 12*Power(t2,3) + 
               3*Power(t2,4)) + 
            Power(s,2)*(-6 - (35 + 16*s2)*t2 + 
               (-101 + 46*s2)*Power(t2,2) + 
               (-121 + 89*s2)*Power(t2,3) + 10*(1 + 4*s2)*Power(t2,4) + 
               (37 - 6*s2)*Power(t2,5) + 3*Power(t2,6)) + 
            s*(27 + (86 + 15*s2)*t2 + 
               (227 - 156*s2 - 35*Power(s2,2))*Power(t2,2) + 
               (25 - 96*s2 - 132*Power(s2,2))*Power(t2,3) - 
               2*(4 + 112*s2 + 17*Power(s2,2))*Power(t2,4) + 
               (-44 - 76*s2 + 3*Power(s2,2))*Power(t2,5) + 
               (1 - 6*s2)*Power(t2,6))) + 
         Power(t1,3)*(8 - 2*(21 + 23*s2)*t2 + 
            (88 - 290*s2 - 17*Power(s2,2))*Power(t2,2) + 
            (-115 + 38*s2 - 160*Power(s2,2) + 7*Power(s2,3))*
             Power(t2,3) - (-2 - 27*s2 + 76*Power(s2,2) + Power(s2,3))*
             Power(t2,4) + (7 + 18*s2 - 11*Power(s2,2) - 2*Power(s2,3))*
             Power(t2,5) + (2 + 3*s2 - Power(s2,2))*Power(t2,6) + 
            Power(s,3)*(-2 - 13*t2 + 14*Power(t2,2) + 34*Power(t2,3) + 
               5*Power(t2,4) + Power(t2,5)) - 
            Power(s,2)*(-21 + (-53 + s2)*t2 + 
               17*(1 + 8*s2)*Power(t2,2) + (162 + 139*s2)*Power(t2,3) + 
               (162 + 23*s2)*Power(t2,4) + (25 + 4*s2)*Power(t2,5) + 
               Power(t2,6)) + 
            s*(-36 + (-36 + 55*s2)*t2 + 
               (-8 + 440*s2 + 25*Power(s2,2))*Power(t2,2) + 
               (206 + 443*s2 + 79*Power(s2,2))*Power(t2,3) + 
               (154 + 251*s2 + 19*Power(s2,2))*Power(t2,4) + 
               (28 + 38*s2 + 5*Power(s2,2))*Power(t2,5) + 
               (-1 + 2*s2)*Power(t2,6))) - 
         Power(s1,6)*t1*(5*Power(t1,6) + 
            Power(t1,5)*(-10 - 2*s + s2 - 3*t2) + 2*s2*t2 + 
            t1*(1 + s*(s2 - t2) - 21*t2 + 22*s2*t2) + 
            Power(t1,4)*(-27 + 15*s2 + 6*t2 - 4*s2*t2 + 
               s*(12 - s2 + t2)) - 
            Power(t1,3)*(-59 + 29*s2 - 2*Power(s2,2) - 24*t2 + 
               14*s2*t2 + 3*s*(8 + s2 + t2)) + 
            Power(t1,2)*(-28 + 13*s2 - 2*Power(s2,2) - 6*t2 - 6*s2*t2 + 
               s*(2 + 15*s2 + 3*t2))) + 
         t1*(11 + (34 + s2)*t2 + 
            (72 - 14*s2 - 11*Power(s2,2))*Power(t2,2) - 
            (96 - 134*s2 + 54*Power(s2,2) + 3*Power(s2,3))*Power(t2,3) - 
            (33 - 46*s2 + 27*Power(s2,2) + 6*Power(s2,3))*Power(t2,4) + 
            (8 + 16*s2 - 31*Power(s2,2) + 2*Power(s2,3))*Power(t2,5) + 
            (-4 + 5*s2 - 5*Power(s2,2))*Power(t2,6) + 
            Power(s,3)*Power(t2,2)*
             (-3 - 19*t2 - 8*Power(t2,2) + 7*Power(t2,3)) + 
            s*(-6 - (24 + 23*s2)*t2 + 
               (-110 - 21*s2 + 19*Power(s2,2))*Power(t2,2) + 
               (2 - 176*s2 + 95*Power(s2,2))*Power(t2,3) + 
               (-40 + 6*s2 + 38*Power(s2,2))*Power(t2,4) + 
               (5 + 60*s2 - 5*Power(s2,2))*Power(t2,5) + 
               (13 + 2*s2)*Power(t2,6)) + 
            Power(s,2)*t2*(4 + 35*t2 + 83*Power(t2,2) + 86*Power(t2,3) + 
               Power(t2,4) + 3*Power(t2,5) + 
               s2*(6 + 4*t2 + 17*Power(t2,2) - 12*Power(t2,3) - 
                  4*Power(t2,4)))) + 
         Power(s1,5)*(-(Power(s2,3)*Power(-1 + t1,3)) + 
            Power(s2,2)*(-1 + t1)*t1*
             (-1 + s*(1 - 2*t1 - 3*Power(t1,2)) + 
               2*Power(t1,3)*(-1 + t2) + t2 + Power(t1,2)*(1 + 3*t2) + 
               t1*(2 + 6*t2)) + 
            s2*(3*Power(t1,7) + Power(t2,2) + 
               Power(t1,6)*(-29 + s + 6*t2) + t1*t2*(2*s + 29*t2) + 
               Power(t1,5)*(75 + s*(8 - 5*t2) + 22*t2 - 
                  6*Power(t2,2)) + 
               Power(t1,4)*(-83 + 2*Power(s,2) + 4*t2 - 
                  14*Power(t2,2) + s*(-14 + 5*t2)) + 
               Power(t1,3)*(42 + 10*Power(s,2) - 74*t2 - 
                  43*Power(t2,2) + s*(2 + 17*t2)) + 
               Power(t1,2)*(-8 + 42*t2 + 33*Power(t2,2) + s*(3 + 53*t2))\
) + t1*(Power(s,2)*t1*(-Power(t1,4) - t2 + t1*(1 + t2) + 
                  Power(t1,3)*(7 + t2) - Power(t1,2)*(31 + 13*t2)) - 
               (-1 + t1)*(9*Power(t1,6) + (2 - 21*t2)*t2 - 
                  Power(t1,5)*(7 + 11*t2) - 3*t1*t2*(25 + 23*t2) + 
                  Power(t1,2)*(-29 + 39*t2 - 36*Power(t2,2)) + 
                  3*Power(t1,4)*(-18 - t2 + Power(t2,2)) + 
                  3*Power(t1,3)*(27 + 28*t2 + Power(t2,2))) + 
               s*(12*Power(t1,6) - 2*Power(t2,2) - 
                  Power(t1,5)*(47 + 12*t2) + 
                  Power(t1,2)*(-10 - 65*t2 + 3*Power(t2,2)) + 
                  t1*(1 + 5*t2 + 3*Power(t2,2)) + 
                  Power(t1,4)*(52 + 31*t2 + 3*Power(t2,2)) - 
                  Power(t1,3)*(8 + 31*t2 + 7*Power(t2,2))))) + 
         Power(s1,4)*(-7*Power(t1,9) + 
            Power(s2,3)*Power(-1 + t1,3)*
             (3 + 2*t1*(-2 + t2) + 2*Power(t1,2)*(-1 + t2) + 2*t2) + 
            Power(t2,2)*(-1 + (7 + s)*t2) + 
            Power(t1,8)*(23*s + 13*(1 + t2)) - 
            Power(t1,7)*(-29 + 8*Power(s,2) - 2*t2 + 7*Power(t2,2) + 
               38*s*(2 + t2)) + 
            Power(t1,6)*(-114 - 101*t2 - 19*Power(t2,2) + Power(t2,3) + 
               Power(s,2)*(24 + 13*t2) + s*t2*(71 + 19*t2)) + 
            t1*t2*(2*Power(s,2)*t2 + 2*t2*(35 + 29*t2) + 
               s*(-2 - 4*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(-1 + t1)*
             (3 + t2 - 4*s*t2 - Power(t2,2) + Power(t1,5)*(4 + 5*t2) + 
               Power(t1,3)*(3 - 2*s + 17*t2 + 12*s*t2) + 
               Power(t1,2)*(-7 + 7*s - 10*t2 + 12*s*t2 - 
                  13*Power(t2,2)) + 
               t1*(1 - 2*t2 + 4*s*t2 - 10*Power(t2,2)) - 
               Power(t1,4)*(4 + 5*s + 11*t2 + 4*s*t2 + 6*Power(t2,2))) + 
            Power(t1,2)*(8 + 84*t2 - 9*Power(t2,2) + 15*Power(t2,3) + 
               Power(s,2)*(2 - t2 + 4*Power(t2,2)) + 
               s*(-8 + 21*t2 + 58*Power(t2,2) - 12*Power(t2,3))) + 
            Power(t1,5)*(135 - 2*Power(s,3) + 17*t2 + 94*Power(t2,2) + 
               6*Power(t2,3) - 
               2*Power(s,2)*(56 + 23*t2 + 2*Power(t2,2)) + 
               s*(104 + 88*t2 - 15*Power(t2,2) - 3*Power(t2,3))) + 
            Power(t1,4)*(-59 + 280*t2 + 115*Power(t2,2) - 
               23*Power(t2,3) + 2*Power(s,3)*(-7 + 5*t2) + 
               Power(s,2)*(72 + 86*t2 + 26*Power(t2,2)) + 
               s*(-27 - 230*t2 - 17*Power(t2,2) + 3*Power(t2,3))) + 
            Power(t1,3)*(-5 - 295*t2 - 243*Power(t2,2) - 
               64*Power(t2,3) + 2*Power(s,3)*(2 + t2) + 
               Power(s,2)*(22 + 68*t2 + 32*Power(t2,2)) + 
               s*(-16 + 90*t2 + 139*Power(t2,2) + 8*Power(t2,3))) + 
            s2*(5*Power(t1,8) + Power(t1,7)*(-36 - 2*s + t2) - 
               Power(t2,2)*(s + 12*t2) + 
               Power(t1,6)*(111 + Power(s,2) + s*(17 - 15*t2) + 17*t2 - 
                  10*Power(t2,2)) + 
               Power(t1,2)*(8 + Power(s,2)*(1 - 26*t2) - 87*t2 + 
                  29*Power(t2,2) + 20*Power(t2,3) + 
                  s*(6 - 44*t2 - 92*Power(t2,2))) + 
               Power(t1,3)*(-45 + 143*t2 + 113*Power(t2,2) + 
                  44*Power(t2,3) - 4*Power(s,2)*(11 + 3*t2) + 
                  s*(55 + 105*t2 - 12*Power(t2,2))) + 
               Power(t1,5)*(-169 - 14*t2 - 4*Power(t2,2) + 
                  4*Power(t2,3) + 2*Power(s,2)*(1 + t2) + 
                  s*(36 + 34*t2 + 11*Power(t2,2))) - 
               Power(t1,4)*(-128 + 78*t2 + 83*Power(t2,2) - 
                  8*Power(t2,3) + Power(s,2)*(-40 + 22*t2) + 
                  s*(111 + 79*t2 + 23*Power(t2,2))) - 
               t1*(2 - 18*t2 + 2*Power(s,2)*t2 + 45*Power(t2,2) + 
                  64*Power(t2,3) + s*(1 + t2 + 63*Power(t2,2))))) + 
         Power(s1,3)*(-2*Power(t1,10) + Power(t1,9)*(4 + 18*s + 5*t2) - 
            Power(t1,8)*(-3 + 17*Power(s,2) - 13*t2 + 4*Power(t2,2) + 
               7*s*(7 + 6*t2)) + 
            Power(s2,3)*Power(-1 + t1,3)*
             (-2 - 3*t2 - 6*Power(t1,2)*(-1 + t2)*t2 - Power(t2,2) + 
               Power(t1,3)*(-2 + 3*t2) + t1*(4 + 6*t2 - 7*Power(t2,2))) \
+ Power(t2,2)*(-(Power(s,2)*t2) - 21*t2*(1 + t2) + 
               s*(1 + t2 - 3*Power(t2,2))) + 
            Power(t1,7)*(-30 + Power(s,3) - 52*t2 - 34*Power(t2,2) + 
               Power(t2,3) + Power(s,2)*(44 + 39*t2) + 
               s*(-72 + 50*t2 + 33*Power(t2,2))) + 
            Power(t1,6)*(79 - 51*t2 + 74*Power(t2,2) + 20*Power(t2,3) - 
               3*Power(s,3)*(1 + t2) - 
               Power(s,2)*(106 + 80*t2 + 27*Power(t2,2)) + 
               s*(222 + 305*t2 + 15*Power(t2,2) - 10*Power(t2,3))) - 
            Power(t1,2)*(5 + 25*t2 - 244*Power(t2,2) - 
               224*Power(t2,3) - 33*Power(t2,4) + 
               Power(s,3)*t2*(9 + 5*t2) + 
               Power(s,2)*t2*(77 + 45*t2 + 35*Power(t2,2)) + 
               s*(-22 - 71*t2 + 181*Power(t2,2) + 136*Power(t2,3) - 
                  8*Power(t2,4))) - 
            t1*(-1 + 17*t2 + 81*Power(t2,2) + Power(s,3)*Power(t2,2) + 
               88*Power(t2,3) + 48*Power(t2,4) + 
               Power(s,2)*t2*(5 - 3*t2 + 12*Power(t2,2)) + 
               s*(-2 - 11*t2 + 10*Power(t2,2) + 17*Power(t2,3) - 
                  3*Power(t2,4))) + 
            Power(t1,5)*(-103 + 336*t2 + 141*Power(t2,2) - 
               20*Power(t2,3) - 3*Power(t2,4) + 
               Power(s,3)*(-17 + 20*t2 + 2*Power(t2,2)) + 
               Power(s,2)*(122 + 136*t2 + 45*Power(t2,2) + 
                  5*Power(t2,3)) + 
               s*(-42 - 379*t2 - 230*Power(t2,2) - 21*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(t1,4)*(61 - 465*t2 - 266*Power(t2,2) - 
               159*Power(t2,3) + 
               Power(s,3)*(25 + 12*t2 - 17*Power(t2,2)) + 
               Power(s,2)*(-17 + 181*t2 + 16*Power(t2,2) - 
                  12*Power(t2,3)) + 
               s*(-131 - 54*t2 + 105*Power(t2,2) + 45*Power(t2,3) + 
                  3*Power(t2,4))) - 
            Power(t1,3)*(8 - 256*t2 + 74*Power(t2,2) - 43*Power(t2,3) - 
               39*Power(t2,4) + 
               Power(s,3)*(6 - 28*t2 + 27*Power(t2,2)) + 
               Power(s,2)*(26 + 194*t2 + 232*Power(t2,2) + 
                  65*Power(t2,3)) + 
               s*(-30 - 38*t2 - 267*Power(t2,2) + 102*Power(t2,3) + 
                  12*Power(t2,4))) + 
            Power(s2,2)*(-1 + t1)*
             (-9 + (-13 + 12*s)*t2 + 7*s*Power(t2,2) + 6*Power(t2,3) + 
               Power(t1,6)*(3 + 6*t2) - 
               t1*(-25 + s + t2 + 44*s*t2 - 22*Power(t2,2) + 
                  10*s*Power(t2,2) - 22*Power(t2,3)) + 
               Power(t1,4)*(23 + (7 + 11*s)*t2 + 
                  2*(12 + 7*s)*Power(t2,2) + 6*Power(t2,3)) - 
               Power(t1,5)*(13 + 25*t2 + 12*Power(t2,2) + 
                  s*(4 + 7*t2)) + 
               Power(t1,2)*(-17 + 16*t2 - 24*Power(t2,2) + 
                  4*Power(t2,3) + s*(-6 + t2 - 35*Power(t2,2))) + 
               Power(t1,3)*(s*(11 + 27*t2 - 16*Power(t2,2)) + 
                  2*(-6 + 5*t2 - 5*Power(t2,2) + Power(t2,3)))) + 
            s2*(3 + 2*Power(t1,9) + (2 - 5*s)*t2 + 
               (-10 - 2*s + 5*Power(s,2))*Power(t2,2) + 
               (16 + 25*s)*Power(t2,3) + 30*Power(t2,4) - 
               Power(t1,8)*(20 + 3*s + t2) + 
               Power(t1,7)*(84 + 3*Power(s,2) + 8*t2 - 6*Power(t2,2) - 
                  3*s*(-7 + 8*t2)) + 
               Power(t1,6)*(-153 + 2*t2 + 10*Power(t2,2) + 
                  6*Power(t2,3) + 2*Power(s,2)*(-5 + 3*t2) + 
                  s*(48 + 67*t2 + 36*Power(t2,2))) - 
               Power(t1,5)*(-119 + 138*t2 + 56*Power(t2,2) - 
                  4*Power(t2,3) + Power(t2,4) + 
                  Power(s,2)*(-53 + 35*t2 + 10*Power(t2,2)) + 
                  s*(231 + 171*t2 + 82*Power(t2,2) + 11*Power(t2,3))) + 
               Power(t1,4)*(-35 + 341*t2 + 140*Power(t2,2) + 
                  36*Power(t2,3) - 5*Power(t2,4) + 
                  Power(s,2)*(-89 - 18*t2 + 36*Power(t2,2)) + 
                  s*(228 + 81*t2 + 77*Power(t2,2) + 13*Power(t2,3))) + 
               Power(t1,2)*(-27 + 112*t2 - 12*Power(t2,2) - 
                  162*Power(t2,3) - 53*Power(t2,4) + 
                  Power(s,2)*(-1 + 120*t2 + 19*Power(t2,2)) + 
                  s*(-45 - 87*t2 - 135*Power(t2,2) + 38*Power(t2,3))) + 
               Power(t1,3)*(22 - 316*t2 - 104*Power(t2,2) + 
                  52*Power(t2,3) - 9*Power(t2,4) + 
                  Power(s,2)*(44 - 70*t2 + 45*Power(t2,2)) + 
                  s*(-30 + 166*t2 + 35*Power(t2,2) + 46*Power(t2,3))) + 
               t1*(5 - 10*t2 + 38*Power(t2,2) + 48*Power(t2,3) + 
                  38*Power(t2,4) + Power(s,2)*t2*(-3 + 25*t2) + 
                  s*(12 - 27*t2 + 71*Power(t2,2) + 129*Power(t2,3))))) + 
         Power(s1,2)*(-1 + 5*s*Power(t1,10) - t2 + s*t2 + 
            9*Power(t2,2) - 3*s*Power(t2,2) + 26*Power(t2,3) - 
            s*Power(t2,3) - 3*Power(s,2)*Power(t2,3) + 
            2*Power(s,3)*Power(t2,3) + 42*Power(t2,4) + 
            7*Power(s,2)*Power(t2,4) + 21*Power(t2,5) + 
            3*s*Power(t2,5) - 
            Power(t1,9)*(2 + 14*Power(s,2) - 7*t2 + 3*s*(1 + 5*t2)) + 
            Power(s2,3)*Power(-1 + t1,3)*t2*
             (1 + Power(t1,4) + Power(t1,3)*(4 - 7*t2) - 5*t2 + 
               Power(t2,2) + Power(t1,2)*(-4 - 6*t2 + 6*Power(t2,2)) + 
               t1*(-2 + 9*Power(t2,2))) + 
            Power(t1,8)*(5 + 3*Power(s,3) - 15*t2 - 17*Power(t2,2) + 
               Power(s,2)*(36 + 41*t2) + 
               s*(-65 - 25*t2 + 16*Power(t2,2))) + 
            Power(t1,7)*(24 - 11*t2 + 17*Power(t2,2) + 13*Power(t2,3) - 
               5*Power(s,3)*(1 + 2*t2) - 
               2*Power(s,2)*(4 + 37*t2 + 21*Power(t2,2)) + 
               s*(103 + 263*t2 + 71*Power(t2,2) - 7*Power(t2,3))) + 
            Power(t1,6)*(-116 + 108*t2 + 43*Power(t2,2) + 
               15*Power(t2,3) - 3*Power(t2,4) + 
               Power(s,3)*(-3 + 19*t2 + 11*Power(t2,2)) + 
               Power(s,2)*(-28 - 45*t2 + 34*Power(t2,2) + 
                  17*Power(t2,3)) + 
               s*(76 - 113*t2 - 304*Power(t2,2) - 60*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(t1,5)*(186 - 264*t2 + 8*Power(t2,2) - 
               111*Power(t2,3) - 17*Power(t2,4) + 
               Power(s,3)*(9 + 4*t2 - 20*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s,2)*(-5 + 341*t2 + 113*Power(t2,2) + 
                  8*Power(t2,3) - 2*Power(t2,4)) + 
               s*(-221 - 498*t2 - 203*Power(t2,2) + 120*Power(t2,3) + 
                  19*Power(t2,4))) + 
            Power(t1,4)*(-128 + 328*t2 - 294*Power(t2,2) - 
               42*Power(t2,3) + 57*Power(t2,4) + 3*Power(t2,5) + 
               Power(s,3)*(-4 + 34*t2 - 41*Power(t2,2) + 
                  6*Power(t2,3)) - 
               Power(s,2)*(-17 + 338*t2 + 472*Power(t2,2) + 
                  94*Power(t2,3) + 4*Power(t2,4)) + 
               s*(87 + 256*t2 + 795*Power(t2,2) + 145*Power(t2,3) - 
                  14*Power(t2,4) - 2*Power(t2,5))) + 
            Power(t1,3)*(20 - 195*t2 + 497*Power(t2,2) + 
               301*Power(t2,3) + 89*Power(t2,4) - 6*Power(t2,5) + 
               Power(s,3)*t2*(-64 - 17*t2 + 40*Power(t2,2)) + 
               Power(s,2)*(15 - 27*t2 + 62*Power(t2,2) + 
                  139*Power(t2,3) + 37*Power(t2,4)) + 
               s*(29 + 349*t2 - 305*Power(t2,2) - 281*Power(t2,3) + 
                  16*Power(t2,4) + 3*Power(t2,5))) + 
            Power(t1,2)*(16 + 36*t2 - 333*Power(t2,2) - 
               159*Power(t2,3) - 134*Power(t2,4) - 24*Power(t2,5) + 
               Power(s,3)*t2*(17 - 10*t2 + 22*Power(t2,2)) + 
               Power(s,2)*(-13 + 86*t2 + 238*Power(t2,2) + 
                  168*Power(t2,3) + 53*Power(t2,4)) + 
               s*(1 - 191*t2 + 16*Power(t2,2) - 32*Power(t2,3) + 
                  119*Power(t2,4) + 3*Power(t2,5))) + 
            t1*(-4 + 7*t2 + 70*Power(t2,2) - 43*Power(t2,3) - 
               34*Power(t2,4) + 6*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(5 + 6*t2) + 
               Power(s,2)*t2*
                (16 + 67*t2 + 5*Power(t2,2) + 29*Power(t2,3)) - 
               s*(12 + 27*t2 + 83*Power(t2,2) - 116*Power(t2,3) - 
                  39*Power(t2,4) + 7*Power(t2,5))) + 
            Power(s2,2)*(-1 + t1)*
             (6 - 14*(-1 + s)*t2 - 2*(-10 + s)*Power(t2,2) - 
               (13 + 2*s)*Power(t2,3) - 12*Power(t2,4) + 
               Power(t1,7)*(-1 + 3*t2) + 
               t1*(-25 + (-32 + 67*s)*t2 + (2 + 48*s)*Power(t2,2) + 
                  (-13 + 9*s)*Power(t2,3) - 16*Power(t2,4)) + 
               Power(t1,2)*(34 + (21 - 59*s)*t2 + 
                  (-7 + s)*Power(t2,2) + (58 + 44*s)*Power(t2,3) + 
                  6*Power(t2,4)) - 
               Power(t1,6)*(2 + 9*t2 + 9*Power(t2,2) + s*(2 + 4*t2)) + 
               Power(t1,5)*(17 + 16*Power(t2,2) + 9*Power(t2,3) + 
                  s*(6 - 7*t2 + 23*Power(t2,2))) - 
               Power(t1,3)*(7 + 11*t2 + 74*Power(t2,2) + 
                  36*Power(t2,3) + 6*Power(t2,4) + 
                  s*(-2 + 42*t2 + 43*Power(t2,2) - 5*Power(t2,3))) - 
               Power(t1,4)*(22 - 14*t2 - 52*Power(t2,2) + 
                  5*Power(t2,3) + 2*Power(t2,4) + 
                  s*(6 - 59*t2 + 27*Power(t2,2) + 16*Power(t2,3)))) - 
            s2*(9 + (3 + s)*Power(t1,9) - 4*(-5 + 3*s)*t2 + 
               (-2 - 45*s + 13*Power(s,2))*Power(t2,2) + 
               (-7 + 29*s + 18*Power(s,2))*Power(t2,3) + 
               (32 + 57*s)*Power(t2,4) + 28*Power(t2,5) + 
               Power(t1,8)*(-19 - 3*Power(s,2) - 5*t2 + Power(t2,2) + 
                  s*(-10 + 19*t2)) - 
               Power(t1,7)*(-42 + 6*t2 - 3*Power(t2,2) + 
                  2*Power(t2,3) + 2*Power(s,2)*(-9 + 5*t2) + 
                  s*(11 + 59*t2 + 42*Power(t2,2))) + 
               Power(t1,6)*(-46 + 192*t2 + 2*Power(t2,2) - 
                  6*Power(t2,3) + Power(t2,4) + 
                  Power(s,2)*(-37 + 19*t2 + 27*Power(t2,2)) + 
                  2*s*(63 + 38*t2 + 47*Power(t2,2) + 13*Power(t2,3))) - 
               Power(t1,5)*(-46 + 544*t2 + 85*Power(t2,2) - 
                  15*Power(t2,3) - 9*Power(t2,4) + 
                  Power(s,2)*
                   (-33 - 12*t2 + 58*Power(t2,2) + 14*Power(t2,3)) + 
                  s*(212 - 149*t2 + 9*Power(t2,2) + 17*Power(t2,3) + 
                     4*Power(t2,4))) + 
               Power(t1,4)*(-58 + 571*t2 + 121*Power(t2,2) - 
                  43*Power(t2,3) - 15*Power(t2,4) - 2*Power(t2,5) + 
                  2*Power(s,2)*
                   (-6 + 43*t2 - 27*Power(t2,2) + 9*Power(t2,3)) - 
                  s*(-102 + 556*t2 + 355*Power(t2,2) + 
                     108*Power(t2,3) + 8*Power(t2,4))) + 
               Power(t1,3)*(34 - 194*t2 + 63*Power(t2,2) + 
                  116*Power(t2,3) + 79*Power(t2,4) + 2*Power(t2,5) + 
                  Power(s,2)*
                   (1 - 250*t2 - 7*Power(t2,2) + 70*Power(t2,3)) + 
                  s*(53 + 512*t2 + 386*Power(t2,2) + 245*Power(t2,3) + 
                     47*Power(t2,4))) + 
               t1*(-29 - 16*t2 + 83*Power(t2,2) + 55*Power(t2,3) - 
                  44*Power(t2,4) - 10*Power(t2,5) + 
                  Power(s,2)*t2*(-16 + 69*t2 + 20*Power(t2,2)) + 
                  s*(17 - 6*t2 - 95*Power(t2,2) + 4*Power(t2,3) + 
                     75*Power(t2,4))) + 
               Power(t1,2)*(Power(s,2)*t2*
                   (159 + 10*t2 + 8*Power(t2,2)) + 
                  s*(-66 - 123*t2 + 66*Power(t2,2) - 179*Power(t2,3) + 
                     13*Power(t2,4)) - 
                  2*(-9 + 9*t2 + 93*Power(t2,2) + 64*Power(t2,3) + 
                     31*Power(t2,4) + 9*Power(t2,5))))) - 
         s1*(-3 + (1 - 5*s + 4*Power(s,2))*Power(t1,10) - 9*t2 + 
            2*Power(t2,2) + 7*s*Power(t2,2) + Power(s,2)*Power(t2,2) + 
            40*Power(t2,3) - 28*s*Power(t2,3) + 
            3*Power(s,2)*Power(t2,3) + 6*Power(s,3)*Power(t2,3) + 
            16*Power(t2,4) + 17*s*Power(t2,4) - 
            3*Power(s,2)*Power(t2,4) + 5*Power(s,3)*Power(t2,4) + 
            19*Power(t2,5) - s*Power(t2,5) + 13*Power(s,2)*Power(t2,5) + 
            7*Power(t2,6) + s*Power(t2,6) - 
            Power(t1,9)*(5 + 3*Power(s,3) + 3*t2 + 
               2*Power(s,2)*(3 + 7*t2) - s*(17 + 24*t2)) + 
            Power(s2,3)*Power(-1 + t1,3)*Power(t2,2)*
             (-2 + 2*Power(t1,4) - 7*t2 - 5*Power(t1,3)*t2 + 
               2*Power(t2,2) + 2*Power(t1,2)*(-1 - t2 + Power(t2,2)) + 
               t1*(2 + 2*t2 + 5*Power(t2,2))) + 
            Power(t1,8)*(-1 + 13*t2 + 10*Power(t2,2) + 
               Power(s,3)*(8 + 11*t2) + 
               s*(18 - 67*t2 - 43*Power(t2,2)) + 
               3*Power(s,2)*(-11 + 2*t2 + 6*Power(t2,2))) - 
            Power(t1,7)*(-45 + 17*t2 + 30*Power(t2,2) + 19*Power(t2,3) + 
               Power(s,3)*(3 + 25*t2 + 15*Power(t2,2)) + 
               s*(132 + 106*t2 - 71*Power(t2,2) - 36*Power(t2,3)) + 
               Power(s,2)*(-81 - 142*t2 - 21*Power(t2,2) + 
                  10*Power(t2,3))) + 
            Power(t1,6)*(Power(s,3)*
                (-5 + 5*t2 + 25*Power(t2,2) + 9*Power(t2,3)) + 
               s*(161 + 433*t2 + 294*Power(t2,2) - 9*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s,2)*(-31 - 262*t2 - 225*Power(t2,2) - 
                  39*Power(t2,3) + 2*Power(t2,4)) + 
               2*(-49 + 16*t2 + 7*Power(t2,2) + 23*Power(t2,3) + 
                  7*Power(t2,4))) - 
            Power(t1,5)*(-77 + 141*t2 - 103*Power(t2,2) + 
               23*Power(t2,3) + 20*Power(t2,4) + 3*Power(t2,5) + 
               Power(s,3)*(2 - 12*t2 - 14*Power(t2,2) + 7*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s,2)*(-40 + 79*t2 + 297*Power(t2,2) + 
                  156*Power(t2,3) + 21*Power(t2,4)) + 
               s*(15 + 212*t2 + 522*Power(t2,2) + 315*Power(t2,3) + 
                  19*Power(t2,4) - 2*Power(t2,5))) - 
            Power(t1,4)*(Power(s,3)*
                (-9 - 19*t2 + 25*Power(t2,2) + 28*Power(t2,3) + 
                  Power(t2,4)) + 
               t2*(-288 + 300*t2 + 12*Power(t2,2) + 37*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s*(72 + 361*t2 + 82*Power(t2,2) - 180*Power(t2,3) - 
                  125*Power(t2,4) - 8*Power(t2,5)) + 
               Power(s,2)*(-5 - 104*t2 - 163*Power(t2,2) + 
                  140*Power(t2,3) + 44*Power(t2,4) + 3*Power(t2,5))) + 
            t1*(8 + 8*t2 + 51*Power(t2,2) - 125*Power(t2,3) - 
               95*Power(t2,4) - 54*Power(t2,5) - 5*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(1 - t2 + 8*Power(t2,2)) + 
               Power(s,2)*t2*
                (15 + 119*t2 + 115*Power(t2,2) + 34*Power(t2,3) + 
                  22*Power(t2,4)) + 
               s*(-16 - 40*t2 - 202*Power(t2,2) + 23*Power(t2,3) + 
                  67*Power(t2,4) + 31*Power(t2,5) - 3*Power(t2,6))) + 
            Power(t1,3)*(-29 - 263*t2 + 380*Power(t2,2) - 
               41*Power(t2,3) + 15*Power(t2,4) + 33*Power(t2,5) + 
               Power(t2,6) + Power(s,3)*
                (-4 - 35*t2 + 26*Power(t2,2) - 8*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s,2)*(37 - 30*t2 - 409*Power(t2,2) - 
                  317*Power(t2,3) + Power(t2,4) + 4*Power(t2,5)) + 
               s*(2 + 350*t2 + 269*Power(t2,2) + 408*Power(t2,3) - 
                  46*Power(t2,4) - 15*Power(t2,5) - Power(t2,6))) + 
            Power(t1,2)*(5 + 92*t2 - 230*Power(t2,2) + 134*Power(t2,3) + 
               107*Power(t2,4) + 7*Power(t2,5) - 3*Power(t2,6) + 
               Power(s,3)*t2*
                (13 - 26*t2 - 19*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s,2)*(-17 - 40*t2 + 15*Power(t2,2) + 
                  232*Power(t2,3) + 109*Power(t2,4) + 24*Power(t2,5)) + 
               s*(42 - 21*t2 + 208*Power(t2,2) - 295*Power(t2,3) - 
                  130*Power(t2,4) + 47*Power(t2,5) + 3*Power(t2,6))) + 
            Power(s2,2)*(-1 + t1)*t2*
             (2 + Power(t1,7)*(-4 + t2) - t2 + 9*Power(t2,2) - 
               22*Power(t2,3) - 10*Power(t2,4) + 
               Power(t1,6)*(10 + 12*t2 - 3*Power(t2,2)) + 
               Power(t1,5)*(4 - 67*t2 - 8*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,4)*(-34 + 97*t2 + 59*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-4 + 25*t2 + 28*Power(t2,2) + 27*Power(t2,3) - 
                  Power(t2,4)) + 
               Power(t1,2)*(-10 - 44*t2 - 49*Power(t2,2) + 
                  14*Power(t2,3) + 2*Power(t2,4)) - 
               Power(t1,3)*(-36 + 23*t2 + 36*Power(t2,2) + 
                  29*Power(t2,3) + 3*Power(t2,4)) + 
               s*(-6 + Power(t1,7) + Power(t1,6)*(4 - 14*t2) + 10*t2 + 
                  26*Power(t2,2) + Power(t2,3) + 
                  Power(t1,5)*(-29 + 30*t2 + 19*Power(t2,2)) + 
                  7*Power(t1,2)*
                   (-4 - 6*t2 + 3*Power(t2,2) + 3*Power(t2,3)) - 
                  Power(t1,3)*
                   (13 - 13*t2 + 25*Power(t2,2) + 5*Power(t2,3)) - 
                  Power(t1,4)*
                   (-46 + 8*t2 + 23*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*(25 + 11*t2 - 18*Power(t2,2) + 9*Power(t2,3)))) - 
            s2*(6 + (19 - 13*s)*t2 + 
               (35 - 63*s + 12*Power(s,2))*Power(t2,2) + 
               (16 - 87*s + 41*Power(s,2))*Power(t2,3) + 
               (7 + 48*s + 14*Power(s,2))*Power(t2,4) + 
               (16 + 43*s)*Power(t2,5) + 9*Power(t2,6) + 
               Power(t1,9)*(-2 + s + Power(s,2) + 2*t2 - 5*s*t2) + 
               Power(t1,8)*(8 + t2 - 6*Power(t2,2) + 
                  Power(s,2)*(-8 + 9*t2) + s*(3 + 5*t2 + 14*Power(t2,2))) \
- Power(t1,7)*(2 + 67*t2 - 16*Power(t2,2) - 6*Power(t2,3) + 
                  Power(s,2)*(-17 + 26*t2 + 27*Power(t2,2)) + 
                  s*(29 - 50*t2 + 4*Power(t2,2) + 13*Power(t2,3))) + 
               Power(t1,6)*(-38 + 220*t2 + 7*Power(t2,2) - 
                  15*Power(t2,3) - 4*Power(t2,4) + 
                  Power(s,2)*
                   (-8 + 25*t2 + 71*Power(t2,2) + 23*Power(t2,3)) + 
                  s*(59 - 186*t2 - 173*Power(t2,2) - 23*Power(t2,3) + 
                     4*Power(t2,4))) + 
               Power(t1,5)*(74 - 289*t2 + 71*Power(t2,2) + 
                  14*Power(t2,3) + 8*Power(t2,4) + 2*Power(t2,5) - 
                  Power(s,2)*
                   (13 + 27*t2 + 35*Power(t2,2) + 36*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  s*(-35 + 314*t2 + 435*Power(t2,2) + 208*Power(t2,3) + 
                     27*Power(t2,4))) + 
               t1*(-32 - 65*t2 - Power(t2,2) + 18*Power(t2,3) - 
                  17*Power(t2,4) - 52*Power(t2,5) - 11*Power(t2,6) + 
                  Power(s,2)*t2*
                   (-17 + 52*t2 + 34*Power(t2,2) + Power(t2,3)) + 
                  s*(6 + 47*t2 - 62*Power(t2,2) - 58*Power(t2,3) - 
                     126*Power(t2,4) + 5*Power(t2,5))) - 
               Power(t1,4)*(38 - 168*t2 + 429*Power(t2,2) + 
                  78*Power(t2,3) + 14*Power(t2,4) + 
                  Power(s,2)*
                   (-16 - 73*t2 + 19*Power(t2,2) + 41*Power(t2,3) + 
                     Power(t2,4)) + 
                  s*(31 + 304*t2 + 186*Power(t2,2) + 349*Power(t2,3) + 
                     91*Power(t2,4) + 6*Power(t2,5))) + 
               Power(t1,3)*(-38 - 61*t2 + 642*Power(t2,2) + 
                  194*Power(t2,3) + 17*Power(t2,4) - 2*Power(t2,5) - 
                  Power(t2,6) + 
                  Power(s,2)*
                   (-5 - 106*t2 + 70*Power(t2,2) + 30*Power(t2,3) + 
                     31*Power(t2,4)) + 
                  s*(57 + 186*t2 - 493*Power(t2,2) - Power(t2,3) + 
                     65*Power(t2,4) + 9*Power(t2,5))) + 
               Power(t1,2)*(62 + 72*t2 - 335*Power(t2,2) - 
                  155*Power(t2,3) + 3*Power(t2,4) + 36*Power(t2,5) + 
                  3*Power(t2,6) + 
                  Power(s,2)*t2*
                   (69 - 124*t2 - 51*Power(t2,2) + 21*Power(t2,3)) + 
                  s*(-31 - 94*t2 + 532*Power(t2,2) + 323*Power(t2,3) + 
                     73*Power(t2,4) + 21*Power(t2,5))))))*T4(t1))/
     ((-1 + s2)*(-s + s2 - t1)*Power(-1 + t1,2)*Power(-1 + s1 + t1 - t2,2)*
       Power(s1*t1 - t2,3)*(-1 + t2)) - 
    (8*(2 + Power(-1 + s,3)*Power(t1,7) - 
         Power(s1,7)*Power(t1,2)*(s2 + t1) + 8*t2 + s*t2 + s2*t2 - 
         6*s*s2*t2 + 16*Power(t2,2) - 10*s*Power(t2,2) - 
         Power(s,2)*Power(t2,2) + 4*s2*Power(t2,2) - 19*s*s2*Power(t2,2) + 
         4*Power(s,2)*s2*Power(t2,2) - 2*Power(s2,2)*Power(t2,2) + 
         4*s*Power(s2,2)*Power(t2,2) - 15*Power(t2,3) - 27*s*Power(t2,3) + 
         25*Power(s,2)*Power(t2,3) + 29*s2*Power(t2,3) - 
         41*s*s2*Power(t2,3) - 13*Power(s,2)*s2*Power(t2,3) - 
         8*Power(s2,2)*Power(t2,3) + 22*s*Power(s2,2)*Power(t2,3) - 
         Power(s2,3)*Power(t2,3) - 5*Power(t2,4) - 16*s*Power(t2,4) + 
         8*Power(s,2)*Power(t2,4) - 12*s2*Power(t2,4) + 
         9*s*s2*Power(t2,4) - 2*Power(s,2)*s2*Power(t2,4) + 
         Power(s2,2)*Power(t2,4) + 4*s*Power(s2,2)*Power(t2,4) - 
         2*Power(s2,3)*Power(t2,4) - 5*Power(t2,5) + 10*s*Power(t2,5) - 
         Power(s,2)*Power(t2,5) - 2*Power(s,3)*Power(t2,5) + 
         2*s2*Power(t2,5) + 11*s*s2*Power(t2,5) + 
         5*Power(s,2)*s2*Power(t2,5) - 8*Power(s2,2)*Power(t2,5) - 
         4*s*Power(s2,2)*Power(t2,5) + Power(s2,3)*Power(t2,5) - 
         Power(t2,6) + 2*s*Power(t2,6) - Power(s,2)*Power(t2,6) + 
         2*s*s2*Power(t2,6) - Power(s2,2)*Power(t2,6) + 
         Power(s1,6)*t1*(-5*Power(t1,3) + 2*s2*t2 + 
            Power(t1,2)*(-1 + 2*s - s2 + 3*t2) + 
            t1*(1 + s*(s2 - t2) + 3*t2 + 4*s2*t2)) - 
         (-1 + s)*Power(t1,6)*
          (4 + (2 + 3*s2)*t2 + Power(s,2)*(1 + 4*t2) - 
            s*(5 + 3*(3 + s2)*t2)) + 
         Power(t1,4)*(-6 + (-5 + 7*s2)*t2 + 
            (7 + 11*s2 + 8*Power(s2,2))*Power(t2,2) + 
            (5 + 6*s2 + 4*Power(s2,2) + Power(s2,3))*Power(t2,3) + 
            Power(s,3)*(1 + 12*t2 + Power(t2,2) - 4*Power(t2,3)) + 
            Power(s,2)*(-12 - (45 + s2)*t2 + (5 + 12*s2)*Power(t2,2) + 
               3*(7 + 3*s2)*Power(t2,3)) - 
            s*(-18 + 6*(-6 + s2)*t2 + 
               (22 + 31*s2 + 8*Power(s2,2))*Power(t2,2) + 
               (28 + 20*s2 + 6*Power(s2,2))*Power(t2,3))) + 
         Power(t1,3)*(9 + (29 + 8*s2)*t2 + 
            (3 + 27*s2 - 6*Power(s2,2))*Power(t2,2) - 
            2*(4 + 4*s2 + 2*Power(s2,2) + Power(s2,3))*Power(t2,3) - 
            (7 + 5*s2 + 3*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
            Power(s,3)*(2 - 2*t2 - 17*Power(t2,2) - 5*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s,2)*(-3 + (28 + 13*s2)*t2 + 
               2*(50 + 7*s2)*Power(t2,2) + 19*Power(t2,3) - 
               (10 + 3*s2)*Power(t2,4)) + 
            s*(-9 - 2*(34 + 11*s2)*t2 + 
               (-113 - 36*s2 + 6*Power(s2,2))*Power(t2,2) + 
               (-3 + 11*s2 + 7*Power(s2,2))*Power(t2,3) + 
               (20 + 13*s2 + 3*Power(s2,2))*Power(t2,4))) + 
         Power(t1,5)*(-3 - (7 + 10*s2)*t2 - 
            (3 + 5*s2 + 3*Power(s2,2))*Power(t2,2) + 
            Power(s,3)*(-3 + 2*t2 + 6*Power(t2,2)) - 
            Power(s,2)*(-6 + (15 + 7*s2)*t2 + (23 + 9*s2)*Power(t2,2)) + 
            s*t2*(23 + 22*t2 + 3*Power(s2,2)*t2 + 17*s2*(1 + t2))) + 
         Power(t1,2)*(Power(s,3)*t2*
             (-5 + 2*t2 + 8*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s,2)*(6 + (23 - 2*s2)*t2 - 2*(5 + 17*s2)*Power(t2,2) - 
               2*(46 + 9*s2)*Power(t2,3) - 8*(3 + s2)*Power(t2,4) + 
               2*Power(t2,5)) + 
            s*(-9 + 2*(-4 + 9*s2)*t2 + 
               (43 + 105*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (139 + 90*s2 + 3*Power(s2,2))*Power(t2,3) + 
               2*(11 + 7*s2 + 2*Power(s2,2))*Power(t2,4) - 
               (7 + 4*s2)*Power(t2,5)) + 
            t2*(-17 + t2 - 5*Power(t2,2) + 2*Power(s2,3)*Power(t2,2) + 
               6*Power(t2,3) + 5*Power(t2,4) + 
               2*Power(s2,2)*t2*
                (-1 - 11*t2 - Power(t2,2) + Power(t2,3)) + 
               s2*(-11 - 63*t2 - 20*Power(t2,2) + 4*Power(t2,3) + 
                  2*Power(t2,4)))) - 
         t1*(5 - 2*(-5 + s2)*t2 + 
            (24 - 26*s2 - 5*Power(s2,2))*Power(t2,2) + 
            (-23 + 7*s2 - 30*Power(s2,2))*Power(t2,3) - 
            2*(8 - s2 + 12*Power(s2,2))*Power(t2,4) - 
            (1 - 2*s2 + 3*Power(s2,2) + Power(s2,3))*Power(t2,5) + 
            (1 + s2)*Power(t2,6) + 
            Power(s,3)*Power(t2,2)*
             (-3 + t2 - 2*Power(t2,2) + Power(t2,3)) + 
            s*(-6 - (27 + 5*s2)*t2 + 
               (-80 + 36*s2 + 7*Power(s2,2))*Power(t2,2) + 
               (-33 + 93*s2 + 17*Power(s2,2))*Power(t2,3) + 
               (70 + 63*s2 + 2*Power(s2,2))*Power(t2,4) + 
               (13 + 13*s2 + 3*Power(s2,2))*Power(t2,5) - Power(t2,6)) + 
            Power(s,2)*t2*(4 + 38*t2 + 14*Power(t2,2) - 32*Power(t2,3) - 
               10*Power(t2,4) - 
               s2*(-6 + 8*t2 + 24*Power(t2,2) + 3*Power(t2,4)))) - 
         Power(s1,5)*(Power(s2,3) - 
            Power(s2,2)*t1*(-1 + s + t2 + 2*t1*t2) + 
            s2*(-3*Power(t1,4) - Power(t1,3)*(s + 6*(-2 + t2)) + 
               Power(t2,2) + 2*t1*t2*(s + 4*t2) + 
               Power(t1,2)*(-8 + 6*Power(t2,2) + s*(3 + 5*t2))) + 
            t1*(9*Power(t1,4) + Power(t1,3)*(1 - 11*t2) + 
               Power(s,2)*t1*(t1 - t2) + t2*(2 + 3*t2) + 
               t1*t2*(1 + 9*t2) + 
               Power(t1,2)*(-11 - 21*t2 + 3*Power(t2,2)) + 
               s*(-12*Power(t1,3) - 2*Power(t2,2) + 
                  Power(t1,2)*(-1 + 12*t2) + t1*(1 + 5*t2 - 3*Power(t2,2))\
))) + Power(s1,4)*(-7*Power(t1,6) + 
            Power(s2,3)*(3 + 2*t1*(-2 + t2) + 2*Power(t1,2)*(-1 + t2) + 
               2*t2) + Power(t1,5)*(4 + 23*s + 13*t2) + 
            Power(t2,2)*(1 + t2 - s*t2) + 
            Power(t1,4)*(16 - 8*Power(s,2) + s*(7 - 38*t2) + 37*t2 - 
               7*Power(t2,2)) + 
            t1*t2*(-2*Power(s,2)*t2 + t2*(5 + 9*t2) + 
               s*(2 + 4*t2 - 6*Power(t2,2))) + 
            Power(s2,2)*(3 + t2 - 4*s*t2 - Power(t2,2) + 
               Power(t1,3)*(6 + 5*t2) + 
               t1*(7 - 4*s*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*(4 + 7*s - 5*t2 - 4*s*t2 - 6*Power(t2,2))) + 
            Power(t1,3)*(5 - 23*t2 - 40*Power(t2,2) + Power(t2,3) + 
               Power(s,2)*(-2 + 13*t2) + s*(-16 - 41*t2 + 19*Power(t2,2))\
) + Power(t1,2)*(-8 - 30*t2 - 27*Power(t2,2) + 9*Power(t2,3) + 
               Power(s,2)*(-2 + t2 - 4*Power(t2,2)) + 
               s*(8 + 3*t2 + 32*Power(t2,2) - 3*Power(t2,3))) + 
            s2*(5*Power(t1,5) + Power(t1,4)*(-27 - 2*s + t2) + 
               Power(t2,2)*(s + 4*t2) + 
               Power(t1,3)*(7 + Power(s,2) + 8*t2 - 10*Power(t2,2) - 
                  3*s*(1 + 5*t2)) + 
               Power(t1,2)*(-2 + 15*t2 - 8*Power(t2,2) + 4*Power(t2,3) + 
                  Power(s,2)*(-1 + 2*t2) + 
                  s*(-3 + 11*t2 + 11*Power(t2,2))) + 
               t1*(2 - 18*t2 + 2*Power(s,2)*t2 + 3*Power(t2,2) + 
                  12*Power(t2,3) + s*(1 + t2 + 12*Power(t2,2))))) + 
         Power(s1,2)*(1 + 5*s*Power(t1,7) + t2 - s*t2 - 9*Power(t2,2) + 
            3*s*Power(t2,2) - 8*Power(t2,3) + 7*s*Power(t2,3) + 
            3*Power(s,2)*Power(t2,3) - 2*Power(s,3)*Power(t2,3) + 
            8*s*Power(t2,4) - 5*Power(s,2)*Power(t2,4) + 3*Power(t2,5) - 
            3*s*Power(t2,5) + 
            Power(t1,6)*(-2 - 14*Power(s,2) + 7*t2 - 3*s*(-4 + 5*t2)) + 
            Power(t1,5)*(-1 + 3*Power(s,3) - 6*t2 - 17*Power(t2,2) + 
               Power(s,2)*(-6 + 41*t2) + 
               2*s*(-22 - 35*t2 + 8*Power(t2,2))) + 
            Power(s2,3)*t2*(1 + Power(t1,4) + Power(t1,3)*(4 - 7*t2) - 
               5*t2 + Power(t2,2) + 
               Power(t1,2)*(-4 - 6*t2 + 6*Power(t2,2)) + 
               t1*(-2 + 9*Power(t2,2))) + 
            Power(t1,4)*(27 + Power(s,3)*(4 - 10*t2) - 14*t2 - 
               22*Power(t2,2) + 13*Power(t2,3) + 
               Power(s,2)*(16 + 49*t2 - 42*Power(t2,2)) + 
               s*(-60 + 86*t2 + 119*Power(t2,2) - 7*Power(t2,3))) + 
            Power(t1,3)*(-34 + 43*t2 + 11*Power(s,3)*(-1 + t2)*t2 + 
               76*Power(t2,2) + 54*Power(t2,3) - 3*Power(t2,4) + 
               Power(s,2)*(24 - 21*t2 - 92*Power(t2,2) + 
                  17*Power(t2,3)) + 
               s*(40 + 220*t2 + 29*Power(t2,2) - 81*Power(t2,3) + 
                  Power(t2,4))) - 
            Power(t1,2)*(-2 + 27*t2 - 57*Power(t2,2) + 26*Power(t2,4) + 
               Power(s,3)*t2*(5 - 13*t2 + 4*Power(t2,2)) + 
               s*(-35 - 98*t2 + 157*Power(t2,2) + 102*Power(t2,3) - 
                  22*Power(t2,4)) + 
               Power(s,2)*(-13 + 26*t2 + Power(t2,2) - 59*Power(t2,3) + 
                  2*Power(t2,4))) + 
            t1*(7 - 4*t2 - 25*Power(t2,2) + 
               Power(s,3)*(7 - 6*t2)*Power(t2,2) - 57*Power(t2,3) - 
               24*Power(t2,4) + 3*Power(t2,5) + 
               Power(s,2)*t2*
                (-16 + 11*t2 + 14*Power(t2,2) - 10*Power(t2,3)) + 
               s*(12 + 24*t2 - 76*Power(t2,2) + 27*Power(t2,3) + 
                  37*Power(t2,4) - 2*Power(t2,5))) + 
            Power(s2,2)*(6 - 14*(-1 + s)*t2 - 2*(-10 + s)*Power(t2,2) - 
               (11 + 6*s)*Power(t2,3) - 6*Power(t2,4) + 
               Power(t1,5)*(-1 + 3*t2) + 
               Power(t1,2)*(2 + (-1 + 21*s)*t2 + 
                  (63 + 19*s)*Power(t2,2) + (13 - 16*s)*Power(t2,3) - 
                  2*Power(t2,4)) - 
               t1*(13 + (4 - 39*s)*t2 - 8*(6 + s)*Power(t2,2) + 
                  (25 + 27*s)*Power(t2,3) + 10*Power(t2,4)) - 
               Power(t1,4)*(4 + 3*t2 + 9*Power(t2,2) + s*(2 + 4*t2)) + 
               Power(t1,3)*(10 - 9*t2 - 2*Power(t2,2) + 9*Power(t2,3) + 
                  s*(2 - 15*t2 + 23*Power(t2,2)))) - 
            s2*(-9 + (3 + s)*Power(t1,6) + 4*(-5 + 3*s)*t2 + 
               (2 + 45*s - 13*Power(s,2))*Power(t2,2) + 
               (13 - 17*s - 8*Power(s,2))*Power(t2,3) - 
               (4 + 13*s)*Power(t2,4) - 4*Power(t2,5) + 
               Power(t1,5)*(-10 - 3*Power(s,2) - 5*t2 + Power(t2,2) + 
                  s*(-7 + 19*t2)) - 
               Power(t1,4)*(-3 + 21*t2 - 6*Power(t2,2) + 2*Power(t2,3) + 
                  Power(s,2)*(-9 + 10*t2) + 
                  s*(35 + 2*t2 + 42*Power(t2,2))) + 
               Power(t1,3)*(-4 + 114*t2 + 29*Power(t2,2) - 
                  12*Power(t2,3) + Power(t2,4) + 
                  Power(s,2)*(-1 - 11*t2 + 27*Power(t2,2)) + 
                  s*(43 + 19*t2 - 32*Power(t2,2) + 26*Power(t2,3))) + 
               t1*(2 - 44*t2 + Power(t2,2) - 12*Power(t2,3) + 
                  6*Power(t2,4) - 2*Power(t2,5) - 
                  4*Power(s,2)*t2*(-4 - 3*t2 + 6*Power(t2,2)) + 
                  s*(-17 + 42*t2 + 38*Power(t2,2) - 27*Power(t2,3) - 
                     20*Power(t2,4))) + 
               Power(t1,2)*(Power(s,2)*t2*(9 + 23*t2 - 14*Power(t2,2)) + 
                  s*(15 + 69*t2 + 93*Power(t2,2) + 61*Power(t2,3) - 
                     4*Power(t2,4)) + 
                  3*(5 - 8*t2 + 17*Power(t2,2) - Power(t2,3) + 
                     4*Power(t2,4))))) + 
         Power(s1,3)*(-2*Power(t1,7) + Power(t1,6)*(2 + 18*s + 5*t2) + 
            Power(t1,5)*(3 - 17*Power(s,2) + s*(9 - 42*t2) + 24*t2 - 
               4*Power(t2,2)) + 
            Power(s2,3)*(-2 - 3*t2 - 6*Power(t1,2)*(-1 + t2)*t2 - 
               Power(t2,2) + Power(t1,3)*(-2 + 3*t2) + 
               t1*(4 + 6*t2 - 7*Power(t2,2))) + 
            Power(t2,2)*(Power(s,2)*t2 - 3*t2*(1 + t2) + 
               s*(-1 - t2 + 3*Power(t2,2))) + 
            Power(t1,4)*(-13 + Power(s,3) - 35*t2 - 46*Power(t2,2) + 
               Power(t2,3) + Power(s,2)*(-7 + 39*t2) + 
               s*(-59 - 84*t2 + 33*Power(t2,2))) + 
            Power(t1,3)*(9 + Power(s,3)*(2 - 3*t2) - 55*t2 - 
               40*Power(t2,2) + 23*Power(t2,3) + 
               Power(s,2)*(-10 + 25*t2 - 27*Power(t2,2)) + 
               s*(-52 + 51*t2 + 114*Power(t2,2) - 10*Power(t2,3))) + 
            Power(t1,2)*(2 + 4*t2 + 65*Power(t2,2) + 50*Power(t2,3) - 
               3*Power(t2,4) + Power(s,3)*t2*(-3 + 2*t2) + 
               Power(s,2)*t2*(14 - 30*t2 + 5*Power(t2,2)) + 
               s*(-28 + 64*t2 + 25*Power(t2,2) - 47*Power(t2,3) + 
                  Power(t2,4))) + 
            t1*(-1 + 17*t2 + 27*Power(t2,2) + Power(s,3)*Power(t2,2) + 
               11*Power(t2,3) - 9*Power(t2,4) + 
               Power(s,2)*t2*(5 - 3*t2 + 9*Power(t2,2)) + 
               s*(-2 - 11*t2 - 11*Power(t2,2) - 28*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s2,2)*(-9 + (-13 + 12*s)*t2 + 7*s*Power(t2,2) + 
               4*Power(t2,3) + Power(t1,4)*(3 + 6*t2) - 
               Power(t1,3)*(7 + (15 + 7*s)*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(6 - 31*t2 + 2*Power(t2,2) + 6*Power(t2,3) + 
                  s*(-8 - 15*t2 + 14*Power(t2,2))) + 
               t1*(7 - 27*t2 + 16*Power(t2,2) + 12*Power(t2,3) + 
                  s*(-1 - 20*t2 + 16*Power(t2,2)))) + 
            s2*(-3 + 2*Power(t1,6) + (-2 + 5*s)*t2 + 
               (10 + 2*s - 5*Power(s,2))*Power(t2,2) - 
               (2 + 7*s)*Power(t2,3) - 6*Power(t2,4) - 
               Power(t1,5)*(14 + 3*s + t2) + 
               Power(t1,4)*(26 + 3*Power(s,2) + s*(14 - 24*t2) + 9*t2 - 
                  6*Power(t2,2)) + 
               Power(t1,3)*(9 + 66*t2 - 4*Power(t2,2) + 6*Power(t2,3) + 
                  Power(s,2)*(-1 + 6*t2) + 
                  s*(45 + 19*t2 + 36*Power(t2,2))) + 
               Power(t1,2)*(-6 - 16*t2 - 24*Power(t2,2) + 
                  18*Power(t2,3) - Power(t2,4) + 
                  Power(s,2)*(1 + 9*t2 - 10*Power(t2,2)) + 
                  s*(9 + 6*t2 + 18*Power(t2,2) - 11*Power(t2,3))) - 
               t1*(Power(s,2)*t2*(-3 + 10*t2) + 
                  s*(12 - 42*t2 + 29*Power(t2,2) + 24*Power(t2,3)) + 
                  2*(7 - 2*t2 - 5*Power(t2,2) + Power(t2,3) + 
                     4*Power(t2,4))))) + 
         s1*(-3 + (-1 + 5*s - 4*Power(s,2))*Power(t1,7) - 9*t2 + 
            2*Power(t2,2) + 7*s*Power(t2,2) + Power(s,2)*Power(t2,2) + 
            16*Power(t2,3) + 28*s*Power(t2,3) - 
            23*Power(s,2)*Power(t2,3) + 2*Power(s,3)*Power(t2,3) + 
            16*Power(t2,4) - 23*s*Power(t2,4) + 3*Power(s,2)*Power(t2,4) + 
            3*Power(s,3)*Power(t2,4) + 3*Power(t2,5) - 9*s*Power(t2,5) + 
            5*Power(s,2)*Power(t2,5) - Power(t2,6) + s*Power(t2,6) + 
            Power(t1,6)*(2 + 3*Power(s,3) + 3*t2 + 
               2*Power(s,2)*(-3 + 7*t2) - 2*s*(1 + 12*t2)) + 
            Power(s2,3)*Power(t2,2)*
             (2 - 2*Power(t1,4) + 7*t2 + 5*Power(t1,3)*t2 - 
               2*Power(t2,2) + Power(t1,2)*(2 + 2*t2 - 2*Power(t2,2)) - 
               t1*(2 + 2*t2 + 5*Power(t2,2))) + 
            Power(t1,5)*(Power(s,3)*(1 - 11*t2) - 
               9*Power(s,2)*(-3 - 4*t2 + 2*Power(t2,2)) - 
               2*(-5 + 2*t2 + 5*Power(t2,2)) + 
               s*(-39 - 5*t2 + 43*Power(t2,2))) + 
            Power(t1,4)*(-22 - 4*t2 + 12*Power(t2,2) + 19*Power(t2,3) + 
               Power(s,3)*(-3 - 8*t2 + 15*Power(t2,2)) + 
               s*(26 + 163*t2 + 58*Power(t2,2) - 36*Power(t2,3)) + 
               Power(s,2)*(14 - 76*t2 - 75*Power(t2,2) + 10*Power(t2,3))) \
- Power(t1,3)*(-4 + 29*t2 - 16*Power(t2,2) + Power(t2,3) + 
               14*Power(t2,4) + 
               Power(s,3)*(4 - 4*t2 - 20*Power(t2,2) + 9*Power(t2,3)) + 
               s*(-32 - 47*t2 + 237*Power(t2,2) + 99*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s,2)*(14 + 60*t2 - 54*Power(t2,2) - 69*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(t1,2)*(11 + 62*t2 - 53*Power(t2,2) - 61*Power(t2,3) - 
               22*Power(t2,4) + 3*Power(t2,5) + 
               Power(s,3)*t2*(13 + t2 - 20*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s,2)*(-17 + 5*t2 + 72*Power(t2,2) + 
                  21*Power(t2,3) - 27*Power(t2,4)) - 
               s*(6 + 141*t2 + 200*Power(t2,2) - 102*Power(t2,3) - 
                  61*Power(t2,4) + 2*Power(t2,5))) + 
            t1*(-1 - 19*t2 + 33*Power(t2,2) - 13*Power(t2,3) + 
               17*Power(t2,4) + 11*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(-11 - 5*t2 + 7*Power(t2,2)) + 
               Power(s,2)*t2*(15 + 14*t2 - 30*Power(t2,2) - 
                  31*Power(t2,3) + 3*Power(t2,4)) - 
               s*(16 + 40*t2 + 13*Power(t2,2) - 115*Power(t2,3) - 
                  16*Power(t2,4) + 14*Power(t2,5))) + 
            Power(s2,2)*t2*(-2 - Power(t1,5)*(-4 + t2) + t2 - 
               11*Power(t2,2) + 18*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,4)*(-2 - 14*t2 + 3*Power(t2,2)) - 
               2*Power(t1,3)*(6 - 20*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               t1*t2*(-23 - 52*t2 + 7*Power(t2,2) + 3*Power(t2,3)) - 
               Power(t1,2)*(-12 + 3*t2 + 34*Power(t2,2) + 
                  12*Power(t2,3)) - 
               s*(-6 + Power(t1,5) + Power(t1,4)*(6 - 14*t2) + 10*t2 + 
                  14*Power(t2,2) - 7*Power(t2,3) + 
                  Power(t1,3)*(-18 + 2*t2 + 19*Power(t2,2)) + 
                  t1*(13 + 19*t2 - 14*Power(t2,2) - 17*Power(t2,3)) + 
                  Power(t1,2)*(4 + 10*t2 + 15*Power(t2,2) - 6*Power(t2,3))\
)) + s2*(-6 + (-19 + 13*s)*t2 + (-35 + 63*s - 12*Power(s,2))*Power(t2,2) + 
               (10 + 23*s - Power(s,2))*Power(t2,3) + 
               (1 - 30*s - 8*Power(s,2))*Power(t2,4) - 
               (2 + 9*s)*Power(t2,5) - Power(t2,6) + 
               Power(t1,6)*(-2 + s + Power(s,2) + 2*t2 - 5*s*t2) + 
               Power(t1,5)*(2 + 7*t2 - 6*Power(t2,2) + 
                  Power(s,2)*(-5 + 9*t2) + 2*s*(3 - 5*t2 + 7*Power(t2,2))) \
- Power(t1,4)*(Power(s,2)*(1 - t2 + 27*Power(t2,2)) + 
                  2*(-5 + 26*t2 + Power(t2,2) - 3*Power(t2,3)) + 
                  s*(14 - 35*t2 - 38*Power(t2,2) + 13*Power(t2,3))) + 
               Power(t1,3)*(-16 + 45*t2 + 19*Power(t2,2) + 
                  3*Power(t2,3) - 4*Power(t2,4) + 
                  s*t2*(-56 - 101*t2 - 62*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(s,2)*(5 + t2 - 10*Power(t2,2) + 23*Power(t2,3))) + 
               Power(t1,2)*(-2 + 9*t2 + 98*Power(t2,2) + 17*Power(t2,3) - 
                  4*Power(t2,4) + 2*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-18 + 16*t2 + 33*Power(t2,2) - 6*Power(t2,3)) + 
                  s*(13 + 31*t2 + 38*Power(t2,2) + 61*Power(t2,3) + 
                     39*Power(t2,4))) + 
               t1*(14 + 8*t2 - 74*Power(t2,2) + 24*Power(t2,3) - 
                  2*Power(t2,4) + 6*Power(t2,5) + 
                  Power(s,2)*t2*
                   (17 + 32*t2 - 11*Power(t2,2) - 19*Power(t2,3)) + 
                  s*(-6 - 8*t2 + 107*Power(t2,2) + 79*Power(t2,3) + 
                     14*Power(t2,4) - 6*Power(t2,5))))))*
       T5(1 - s1 - t1 + t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,3)*
       (-1 + t2)));
   return a;
};
