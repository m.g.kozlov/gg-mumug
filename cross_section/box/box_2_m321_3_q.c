#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m321_3_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(-2*Power(t1,2)*(1 + Power(s,2) - 3*t1 + Power(t1,2) + 
            s*(-2 + 3*t1))*(1 + t1 - t2) + 
         2*Power(s2,6)*(1 + t1*(2 + s1 + t2)) + 
         Power(s2,5)*(2*(-1 + t2) + 
            2*Power(t1,2)*(-11 + s - 4*s1 + 5*t2) + 
            t1*(s*(8 + s1 - 3*t2) + 
               2*(17 - 10*s1 + Power(s1,2) - 9*t2 + 3*s1*t2 - 
                  2*Power(t2,2)))) + 
         Power(s2,4)*(-4 - 2*Power(t1,3)*(-15 + s - 4*s1 + 13*t2) + 
            Power(t1,2)*(-56 + Power(s,2) + 52*s1 - 10*Power(s1,2) + 
               s*(-51 + 7*s1 - 16*t2) - 28*t2 + 8*Power(t2,2)) + 
            t1*(Power(s,2)*t2 + 
               s*(-11 - 25*s1 + 42*t2 - 2*s1*t2 + 8*Power(t2,2)) + 
               2*(-13 - 6*s1 + 5*Power(s1,2) + 26*t2 - 22*s1*t2 + 
                  14*Power(t2,2)))) + 
         Power(s2,2)*(2*(-1 + s - s1)*Power(t1,5) + 2*(-1 + t2) - 
            Power(t1,4)*(22 + 3*Power(s,2) + 6*Power(s1,2) + 64*t2 + 
               s*(51 - 9*s1 + 2*t2) - 4*s1*(2 + 3*t2)) + 
            2*t1*(-4 + t2 + Power(s,2)*(12 - 5*t2 + 2*Power(t2,2)) + 
               s*(1 - 4*s1*(-1 + t2) - 5*t2 + 6*Power(t2,2))) + 
            2*Power(t1,2)*(8 + 4*Power(s1,2) + 19*t2 + 8*Power(t2,2) - 
               2*s1*(7 + 11*t2) + 
               Power(s,2)*(3 - 4*t2 + 2*Power(t2,2)) + 
               s*(16 + 6*s1*(-4 + t2) + 9*t2 + 12*Power(t2,2))) + 
            Power(t1,3)*(14*Power(s1,2) - 20*s1*(3 + t2) + 
               Power(s,2)*(-20 + 3*t2) + 4*(8 + 6*t2 + 7*Power(t2,2)) + 
               s*(-45 - 26*t2 + 4*Power(t2,2) + s1*(-31 + 6*t2)))) - 
         s2*t1*(Power(s,2)*(-3*Power(t1,3) - 10*(-1 + t2) + 
               Power(t1,2)*(-20 + 3*t2) + 2*t1*(5 + t2 + 2*Power(t2,2))) \
+ s*(4 + 2*Power(t1,4) - 4*t2 - Power(t1,3)*(51 + 2*t2) + 
               4*t1*t2*(-1 + 3*t2) + 
               Power(t1,2)*(31 - 7*t2 + 4*Power(t2,2)) + 
               s1*t1*(8 + Power(t1,2) - 8*t2 + 2*t1*(-16 + 7*t2))) + 
            2*(t1 - (10 + s1)*Power(t1,4) + 
               Power(t1,3)*(14 + Power(s1,2) + 6*s1*(-2 + t2) - 8*t2) + 
               2*(-1 + t2) - 4*t1*t2 + 
               Power(t1,2)*(8 + 2*Power(s1,2) + 11*t2 + 4*Power(t2,2) - 
                  s1*(7 + 11*t2)))) + 
         Power(s2,3)*(Power(s,2)*t1*
             (-14 + 2*Power(t1,2) - t2 - 4*Power(t2,2) + t1*(5 + 8*t2)) + 
            s*t1*(-2*Power(t1,3) + 3*Power(t1,2)*(32 + 7*t2) + 
               t1*(5 - 18*t2 - 12*Power(t2,2)) - 
               5*(-1 + 5*t2 + 4*Power(t2,2)) + 
               s1*(-17*Power(t1,2) + t1*(57 - 4*t2) + 2*(8 + t2))) + 
            2*(3 + Power(s1,2)*t1*(-2 - 11*t1 + 7*Power(t1,2)) - 2*t2 - 
               t1*t2*(9 + 4*t2) + Power(t1,4)*(-5 + 7*t2) + 
               Power(t1,2)*(13 - 47*t2 - 28*Power(t2,2)) + 
               Power(t1,3)*(11 + 55*t2 - 2*Power(t2,2)) + 
               s1*t1*(7 + 11*t2 - 3*Power(t1,2)*(7 + 3*t2) + 
                  t1*(24 + 38*t2))))))/
     (Power(-1 + s2,2)*s2*Power(s2 - t1,2)*(-s + s2 - t1)*(-1 + t1)*t1*
       (-1 + s2 - t1 + t2)) - (8*
       (-2*(s + s1)*Power(t1,5) + 
         2*(-1 + t2)*t2*Power(1 + (-1 + s)*t2,2) + 
         Power(s2,7)*(Power(s1,2) - 2*s1*t2 - Power(t2,2)) + 
         Power(t1,4)*(-13 - 2*Power(s1,2) + 18*t2 + s1*(-15 + 2*t2) + 
            s*(31 - 2*s1 + 6*t2)) + 
         Power(t1,3)*(-7 + Power(s1,2)*(-1 + t2) + 44*t2 - 
            21*Power(t2,2) + Power(s,2)*(5 + 3*t2) + 
            s*(23 + 12*s1 - 47*t2 - 3*Power(t2,2)) + 
            s1*(-19 + 26*t2 - 3*Power(t2,2))) + 
         t1*(-(Power(-1 + t2,2)*(1 + t2)*(-11 + 3*s1 + 3*t2)) - 
            Power(s,2)*t2*(-7 + 4*t2 + Power(t2,2)) + 
            s*(7 + (-4 + s1)*t2 - 4*s1*Power(t2,2) + 
               3*(-2 + s1)*Power(t2,3) + 3*Power(t2,4))) + 
         Power(s2,6)*(Power(s1,2)*(-2 - 2*t1 + 3*t2) + 
            t2*(2 + s*(-2 + t2) - 3*t2 - 2*Power(t2,2) + 
               t1*(2 + 3*t2)) + 
            s1*(-2 + (11 + s)*t2 - 5*Power(t2,2) + t1*(2 + 2*s + 5*t2))) \
- Power(s2,5)*(-1 + 11*t2 - 5*s*t2 - Power(t2,2) + 4*s*Power(t2,2) + 
            11*Power(t2,3) - 2*s*Power(t2,3) + Power(t2,4) + 
            Power(t1,2)*(1 - Power(s,2) + 6*t2 - 3*s*t2 + Power(t2,2)) - 
            t1*(-2 + (1 + 5*s - Power(s,2))*t2 + 
               (11 - 4*s)*Power(t2,2) + 3*Power(t2,3)) + 
            Power(s1,2)*(4 + 5*t2 - 3*Power(t2,2) + t1*(-3 + 5*t2)) + 
            s1*(-4 + 2*(3 + s)*t2 - (23 + 3*s)*Power(t2,2) + 
               4*Power(t2,3) + Power(t1,2)*(5 + 2*s + 3*t2) + 
               t1*(7 + s*(7 - 5*t2) + 27*t2 - 11*Power(t2,2)))) + 
         Power(t1,2)*(Power(s,2)*(1 + t2 - 4*Power(t2,2)) - 
            s*(-13 + 5*t2 - 26*Power(t2,2) + 4*Power(t2,3) + 
               s1*(-6 + 15*t2 + Power(t2,2))) + 
            (-1 + t2)*(-1 - 23*t2 + 6*Power(t2,2) + 
               Power(s1,2)*(3 + t2) + s1*(-7 - 10*t2 + 3*Power(t2,2)))) - 
         Power(s2,3)*(4 + Power(t1,4)*
             (1 + Power(s,2) + s*(-2 + t2) - 2*t2) - 26*t2 + 4*s*t2 + 
            13*Power(t2,2) + 40*s*Power(t2,2) + 
            4*Power(s,2)*Power(t2,2) + 31*Power(t2,3) - 
            9*s*Power(t2,3) - 11*Power(t2,4) - 6*s*Power(t2,4) + 
            5*Power(t2,5) - Power(t1,3)*
             (Power(s,2)*(1 + 2*t2) + 4*(1 + 7*t2 + Power(t2,2)) + 
               s*(21 + 9*t2 + 2*Power(t2,2))) + 
            t1*(-3 + 12*t2 - Power(s,2)*(-2 + t2)*t2 + 10*Power(t2,2) + 
               10*Power(t2,3) - 15*Power(t2,4) + 
               s*(2 + 68*t2 + 34*Power(t2,2) - 10*Power(t2,3))) + 
            Power(s1,2)*(-3 + Power(t1,4) + t2 + 2*Power(t2,2) - 
               Power(t1,3)*(1 + t2) - 
               Power(t1,2)*(-16 + 9*t2 + Power(t2,2)) + 
               t1*(8 + 2*t2 - Power(t2,2) + Power(t2,3))) + 
            Power(t1,2)*(Power(s,2)*(2 + 2*t2 + Power(t2,2)) + 
               s*(-10 - 2*t2 + 27*Power(t2,2) + Power(t2,3)) + 
               4*(9 + 7*t2 + 4*Power(t2,2) + 4*Power(t2,3))) - 
            s1*(-4 + (-3 + 2*s)*t2 + 2*(9 + 4*s)*Power(t2,2) - 
               (15 + 7*s)*Power(t2,3) + s*Power(t2,4) + 
               Power(t1,4)*(1 + 2*s + t2) + 
               Power(t1,2)*(18 - 4*s*(-5 + t2) + 69*t2 + 
                  13*Power(t2,2) - 2*Power(t2,3)) - 
               Power(t1,3)*(36 + 9*t2 + Power(t2,2) + s*(-1 + 3*t2)) + 
               t1*(-3 + 136*t2 + 16*Power(t2,2) - 15*Power(t2,3) + 
                  2*Power(t2,4) + s*(20 + 36*t2 - 10*Power(t2,2))))) + 
         Power(s2,4)*(-2 + 3*t2 + 6*s*t2 + 27*s*Power(t2,2) + 
            15*Power(t2,3) + s*Power(t2,3) - 13*Power(t2,4) + 
            s*Power(t2,4) - Power(t1,3)*
             (-3 - 2*t2 + Power(t2,2) + s*(3 + 2*t2)) + 
            Power(s1,2)*(2 + 2*Power(t1,3) + Power(t1,2)*(-1 + t2) + 
               4*t2 - 3*Power(t2,2) + Power(t2,3) + 
               t1*(17 + t2 - 4*Power(t2,2))) + 
            Power(t1,2)*(2 + Power(s,2)*(-3 + t2) - 13*t2 - 
               18*Power(t2,2) + Power(t2,3) + 
               s*(-4 - 15*t2 + 5*Power(t2,2))) + 
            t1*(9 + 35*t2 - Power(s,2)*(-3 + t2)*t2 + 9*Power(t2,2) + 
               21*Power(t2,3) + 
               s*(1 - 16*t2 + 15*Power(t2,2) - 4*Power(t2,3))) - 
            s1*(-8 + (21 + 8*s)*t2 + (39 + 7*s)*Power(t2,2) - 
               3*(4 + s)*Power(t2,3) + Power(t2,4) + 
               Power(t1,3)*(-3 + 2*s + t2) + 
               Power(t1,2)*(-32 - 21*t2 + 5*Power(t2,2) + 
                  s*(-5 + 3*t2)) + 
               t1*(6 + 29*t2 + 39*Power(t2,2) - 8*Power(t2,3) + 
                  s*(2 + 15*t2 - 3*Power(t2,2))))) - 
         Power(s2,2)*(-2 - (-1 + s)*Power(t1,5) + t2 + 4*s*t2 - 
            8*Power(t2,2) - 34*s*Power(t2,2) - 
            8*Power(s,2)*Power(t2,2) + Power(t2,3) + 2*s*Power(t2,3) - 
            2*Power(s,2)*Power(t2,3) + 10*Power(t2,4) + s*Power(t2,4) - 
            2*Power(t2,5) - 3*s*Power(t2,5) + 
            Power(t1,4)*(13 + 12*t2 + s*(16 + 5*t2)) + 
            Power(t1,3)*(-32 + 8*t2 - 30*Power(t2,2) + 
               Power(s,2)*(-3 + 5*t2) + s*(17 - 40*t2 - 7*Power(t2,2))) \
+ Power(s1,2)*(Power(t1,4) + Power(t1,3)*(1 + 5*t2) + 
               t1*(9 - 7*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(-5 + t2 - 2*Power(t2,2)) + 
               t2*(5 - 6*t2 + Power(t2,2))) - 
            Power(t1,2)*(6 - 5*t2 + 30*Power(t2,2) - 7*Power(t2,3) + 
               s*(33 + 89*t2 - 2*Power(t2,2)) + 
               2*Power(s,2)*(5 + 3*t2 + 5*Power(t2,2))) + 
            t1*(14 + 39*t2 - 102*Power(t2,2) - 11*Power(t2,3) + 
               12*Power(t2,4) + 
               Power(s,2)*t2*(-6 + 11*t2 + 5*Power(t2,2)) + 
               s*(8 - 104*t2 + 38*Power(t2,2) + 21*Power(t2,3) + 
                  6*Power(t2,4))) + 
            s1*(6 + Power(t1,5) - (22 + 7*s)*t2 + 
               5*(2 + 3*s)*Power(t2,2) + 8*Power(t2,3) + 
               2*(-1 + s)*Power(t2,4) + 
               Power(t1,3)*(30 + s*(3 - 9*t2) + 35*t2 + 3*Power(t2,2)) - 
               t1*(11 - 5*(7 + 6*s)*t2 + 30*(1 + s)*Power(t2,2) + 
                  (5 + 4*s)*Power(t2,3) + Power(t2,4)) - 
               Power(t1,4)*(s + 4*(4 + t2)) + 
               Power(t1,2)*(73 + 120*t2 - 32*Power(t2,2) + Power(t2,3) + 
                  s*(49 - 3*t2 + 12*Power(t2,2))))) + 
         s2*(3 + (7 + 3*s - 3*s1)*Power(t1,5) - (21 + s - 7*s1)*t2 + 
            (26 - 4*Power(s,2) + 3*s*(-2 + s1) - 15*s1)*Power(t2,2) + 
            (2 + 4*Power(s,2) + s*(2 - 4*s1) + 9*s1)*Power(t2,3) - 
            (13 + 2*Power(s,2) + s1 - s*(8 + s1))*Power(t2,4) - 
            3*(-1 + s)*Power(t2,5) + 
            Power(t1,4)*(3 + 5*Power(s,2) + 4*Power(s1,2) - 14*t2 + 
               7*s1*(2 + t2) - s*(7 + 9*s1 + 4*t2)) - 
            Power(t1,3)*(-2 + Power(s1,2)*(-3 + t2) - 6*t2 + 
               26*Power(t2,2) + Power(s,2)*(13 + 12*t2) + 
               s1*(-84 + t2 + 3*Power(t2,2)) + 
               s*(52 + 40*t2 + 6*Power(t2,2) - 2*s1*(4 + 9*t2))) + 
            Power(t1,2)*(40 - 7*Power(s1,2)*(-1 + t2) - 60*t2 - 
               66*Power(t2,2) + 38*Power(t2,3) + 
               Power(s,2)*(-7 + 6*t2 + 7*Power(t2,2)) + 
               s1*(17 - 4*t2 - 27*Power(t2,2) + 2*Power(t2,3)) + 
               s*(-44 + 2*t2 + 58*Power(t2,2) + 9*Power(t2,3) + 
                  s1*(12 - 21*t2 - 11*Power(t2,2)))) + 
            t1*(Power(s,2)*t2*(-13 + 3*t2 + 2*Power(t2,2)) + 
               (-1 + t2)*(3 + 12*t2 + 29*Power(t2,2) - 8*Power(t2,3) + 
                  Power(s1,2)*(-3 - 6*t2 + Power(t2,2)) + 
                  s1*(2 + 23*t2 + 6*Power(t2,2) - 3*Power(t2,3))) + 
               s*(2 - 37*t2 - 7*Power(t2,2) - 19*Power(t2,3) + 
                  Power(t2,4) + 
                  s1*(-13 + 19*t2 + 13*Power(t2,2) + Power(t2,3))))))*
       B1(1 - s2 + t1 - t2,s2,t1))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s2 - t1 + t2)*
       Power(-t1 + s2*t2,2)) - 
    (16*(Power(t1,3)*(1 + Power(s,2) - 3*t1 + Power(t1,2) + 
            s*(-2 + 3*t1))*(1 + t1 - t2) + Power(s2,10)*t2 - 
         Power(s2,9)*(Power(s1,2) + t1 - 5*s1*t2 + 3*t1*t2 + 
            (3 + s - t2)*t2) + 
         Power(s2,8)*(Power(s1,2)*(4 + 3*t1 - t2) + 
            (1 + 3*s - 3*t2)*t2 + 3*Power(t1,2)*(1 + t2) + 
            t1*(3 + s + 6*t2 + 3*s*t2) - 
            s1*(-2 + 2*(9 + s)*t2 - 5*Power(t2,2) + t1*(5 + 2*s + 20*t2))\
) + s2*Power(t1,2)*(-2 + (-8 + 3*s1)*Power(t1,4) + t2 + Power(t2,2) + 
            Power(t1,3)*(6 + 3*s1 + 6*t2 - 3*s1*t2) + 
            Power(t1,2)*(23 + 2*s1 - 20*t2 + 2*Power(t2,2)) + 
            t1*(2 + 2*s1 - 9*t2 - 2*s1*t2 + 3*Power(t2,2)) + 
            Power(s,2)*(-4 + 3*Power(t1,3) + 3*t2 + Power(t2,2) - 
               Power(t1,2)*(1 + 4*t2) + t1*(-9 + 2*t2 + Power(t2,2))) - 
            s*(Power(t1,4) + Power(t1,3)*(26 + s1 - 4*t2) - 
               2*t1*(3 + s1*(-1 + t2) + 4*t2 - 2*Power(t2,2)) + 
               2*(-3 + 2*t2 + Power(t2,2)) + 
               Power(t1,2)*(30 - s1*(-3 + t2) - 32*t2 + 3*Power(t2,2)))) \
- Power(s2,7)*(1 + Power(s1,2)*(3 + 13*t1 + 2*Power(t1,2) - 7*t2) - 
            13*t2 - 4*s*t2 - 5*Power(s,2)*t2 - 6*Power(t2,2) - 
            7*s*Power(t2,2) + Power(s,2)*Power(t2,2) - 3*Power(t2,3) - 
            s*Power(t2,3) + Power(t1,3)*(3 + t2) + 
            t1*(1 + s - 13*t2 + 15*s*t2 - Power(s,2)*t2 + 
               3*Power(t2,2) + 3*s*Power(t2,2)) + 
            Power(t1,2)*(7 + Power(s,2) + 13*t2 + 3*Power(t2,2) + 
               s*(3 + 7*t2)) - 
            s1*(-8 + (13 - 3*s)*t2 - (21 + 2*s)*Power(t2,2) + 
               4*Power(t1,2)*(5 + s + 7*t2) + 
               t1*(12 + 67*t2 - 11*Power(t2,2) + 6*s*(2 + t2)))) - 
         Power(s2,2)*t1*(-1 + (-22 + s1)*Power(t1,5) - t2 + 
            2*Power(t2,2) + Power(t1,4)*
             (-11 + 2*Power(s1,2) + 22*t2 + s1*(13 + 2*t2)) + 
            t1*(-2 - 13*t2 + 5*Power(t2,2) + 4*Power(t2,3) - 
               2*s1*(-2 + t2 + Power(t2,2))) + 
            Power(t1,2)*(37 - 44*t2 - 4*Power(t2,2) + Power(t2,3) + 
               2*s1*(7 - 5*t2 + 2*Power(t2,2))) - 
            Power(t1,3)*(-73 + 2*Power(s1,2)*(-2 + t2) + 22*t2 + 
               Power(t2,2) + s1*(-9 + 10*t2 + 3*Power(t2,2))) + 
            Power(s,2)*(3 + 5*Power(t1,4) + Power(t1,3)*(4 - 5*t2) - 
               7*t2 + 4*Power(t2,2) + 
               Power(t1,2)*(-19 - 3*t2 + Power(t2,2)) - 
               t1*(10 - 7*Power(t2,2) + Power(t2,3))) + 
            s*(4 + Power(t1,5) + 2*t2 - 6*Power(t2,2) + 
               Power(t1,4)*(-51 - 5*s1 + t2) + 
               t1*(19 + (5 - 2*s1)*t2 + (-3 + 2*s1)*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(t1,3)*(-103 + 53*t2 + Power(t2,2) + 
                  s1*(-13 + 8*t2)) + 
               Power(t1,2)*(-12 + 34*t2 + 9*Power(t2,2) - 
                  3*Power(t2,3) + s1*(-13 + 8*t2 - 3*Power(t2,2))))) + 
         Power(s2,3)*((-7 + 2*s - 2*s1)*Power(t1,6) - 
            (-1 + 4*s + 3*Power(s,2))*(-1 + t2)*t2 + 
            Power(t1,5)*(-73 + 3*Power(s,2) - Power(s1,2) - 14*t2 - 
               s*(32 + 2*s1 + t2) + s1*(25 + 3*t2)) + 
            t1*(-2 - 11*t2 + Power(t2,2) + 8*Power(t2,3) + 
               s1*(2 + 2*t2 - 4*Power(t2,2)) - 
               2*s*(-6 + s1*(-1 + t2) - 6*t2 + 5*Power(t2,2)) + 
               2*Power(s,2)*(5 - 8*t2 + 6*Power(t2,2))) + 
            Power(t1,4)*(71 - Power(s1,2)*(-13 + t2) - 
               2*Power(s,2)*(-1 + t2) + 45*t2 + 29*Power(t2,2) - 
               s1*(-22 + 7*t2 + Power(t2,2)) + 
               s*(-141 + 5*t2 + 2*Power(t2,2) + s1*(-16 + 3*t2))) + 
            Power(t1,3)*(129 - 4*Power(s1,2)*(-3 + t2) - 23*t2 - 
               38*Power(t2,2) - 8*Power(t2,3) + 
               s1*(15 - 24*t2 - 7*Power(t2,2)) + 
               Power(s,2)*(-16 + t2 + Power(t2,2)) - 
               s*(47 + t2 - 55*Power(t2,2) + 3*Power(t2,3) + 
                  s1*(31 - 18*t2 + Power(t2,2)))) - 
            Power(t1,2)*(-10 + 24*t2 + 31*Power(t2,2) - 15*Power(t2,3) - 
               2*s1*(11 - 7*t2 + 2*Power(t2,2)) + 
               Power(s,2)*(2 + 20*t2 - 9*Power(t2,2) + 2*Power(t2,3)) + 
               s*(-32 - 13*t2 - 31*Power(t2,2) + 16*Power(t2,3) + 
                  s1*(5 + 6*t2 + Power(t2,2))))) + 
         Power(s2,6)*(4 + Power(t1,4) - 11*t2 - 9*s*t2 - 
            14*Power(s,2)*t2 + 19*Power(t2,2) - 8*s*Power(t2,2) + 
            4*Power(s,2)*Power(t2,2) - 4*Power(t2,3) - 2*s*Power(t2,3) - 
            Power(s,2)*Power(t2,3) + 
            Power(t1,3)*(13 + Power(s,2) + 27*t2 + 2*Power(t2,2) + 
               7*s*(1 + t2)) - 
            2*Power(s1,2)*(Power(t1,3) + t2 - Power(t1,2)*(8 + 3*t2) + 
               t1*(-1 + 10*t2)) + 
            Power(t1,2)*(-10 + 9*t2 + 7*Power(t2,2) + 
               Power(s,2)*(5 + t2) + s*(11 + 66*t2 - 2*Power(t2,2))) + 
            t1*(-10 - 100*t2 - 12*Power(t2,2) - 5*Power(t2,3) + 
               Power(s,2)*(-5 - 12*t2 + 4*Power(t2,2)) - 
               s*(14 + 13*t2 + 19*Power(t2,2))) + 
            s1*(6 + 2*(-6 + s)*t2 + (10 + 3*s)*Power(t2,2) - 
               14*Power(t1,3)*(2 + t2) - 
               Power(t1,2)*(68 + 100*t2 - 5*Power(t2,2) + 
                  10*s*(3 + t2)) + 
               t1*(15 - 10*t2 + 56*Power(t2,2) + 
                  s*(-11 + 22*t2 + 2*Power(t2,2))))) + 
         Power(s2,5)*(-3 + 6*s1*t2 - 17*Power(t2,2) - 4*s1*Power(t2,2) + 
            13*Power(t2,3) + 2*Power(s,2)*t2*(8 - 3*t2 + Power(t2,2)) + 
            s*t2*(11 + s1*(5 - 7*t2) - 2*t2 + 4*Power(t2,2)) + 
            Power(t1,4)*(Power(s,2) + 3*Power(s1,2) - s*(7 + 4*s1) - 
               s1*(-14 + t2) - 24*(1 + t2)) + 
            Power(t1,3)*(-6 - 75*t2 + 7*Power(t2,2) - 
               3*Power(s,2)*(3 + t2) - 2*Power(s1,2)*(5 + 4*t2) + 
               s1*(115 + 74*t2 + 3*Power(t2,2)) + 
               s*(-63 - 80*t2 + 3*Power(t2,2) + 2*s1*(11 + 5*t2))) + 
            Power(t1,2)*(92 + 162*t2 + 35*Power(t2,2) + Power(t2,3) + 
               2*Power(s1,2)*(7 + 9*t2) + 
               Power(s,2)*(1 - t2 + 4*Power(t2,2)) - 
               s1*(1 + 30*t2 + 50*Power(t2,2)) - 
               s*(-30 + 69*t2 - 59*Power(t2,2) + Power(t2,3) + 
                  2*s1*(-6 + 12*t2 + Power(t2,2)))) + 
            t1*(-3 + 61*t2 - 74*Power(t2,2) + 4*Power(s1,2)*(1 + t2) + 
               s1*(-6 + 11*t2 - 21*Power(t2,2)) + 
               Power(s,2)*(14 + 15*t2 - 10*Power(t2,2)) + 
               s*(23 + 29*t2 + 31*Power(t2,2) - 9*Power(t2,3) + 
                  s1*(4 - 26*t2 + 5*Power(t2,2))))) + 
         Power(s2,4)*(Power(t1,5)*
             (22 - Power(s,2) + s1 - Power(s1,2) + 2*s*(s1 - t2) + 
               7*t2 + 2*s1*t2) + 
            t2*(4 + 2*s1*(-1 + t2) + t2 - 4*Power(t2,2) + 
               s*(-12 + 2*s1*(-1 + t2) + 11*t2 - 3*Power(t2,2)) - 
               Power(s,2)*(10 - 6*t2 + Power(t2,2))) + 
            Power(t1,4)*(68 + 66*t2 - 8*Power(t2,2) + 
               Power(s,2)*(1 + t2) + Power(s1,2)*(4 + 3*t2) - 
               s1*(85 + 26*t2 + 2*Power(t2,2)) + 
               s*(86 + 27*t2 + 2*Power(t2,2) - 2*s1*(1 + 2*t2))) + 
            Power(t1,2)*(-64 - 12*Power(s1,2) - 55*t2 + 92*Power(t2,2) + 
               12*Power(t2,3) + 3*s1*(-3 + 6*t2 + 5*Power(t2,2)) + 
               Power(s,2)*(-9 + 17*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               s*(-53 + 30*t2 - 77*Power(t2,2) + 14*Power(t2,3) + 
                  s1*(15 + 14*t2 - 7*Power(t2,2)))) + 
            Power(t1,3)*(-155 - 98*t2 - 58*Power(t2,2) + Power(t2,3) - 
               4*Power(s1,2)*(6 + t2) - 
               Power(s,2)*(-13 + 2*t2 + Power(t2,2)) + 
               2*s1*(-9 + 18*t2 + 8*Power(t2,2)) + 
               s*(75 + 74*t2 - 49*Power(t2,2) + 
                  2*s1*(5 + t2 + Power(t2,2)))) + 
            t1*(9 + t2 + 42*Power(t2,2) - 27*Power(t2,3) + 
               Power(s,2)*(-16 + 5*t2 - 2*Power(t2,2)) + 
               2*s1*(-5 - t2 + 2*Power(t2,2)) + 
               s*(-17 - 22*t2 - 17*Power(t2,2) + 9*Power(t2,3) + 
                  s1*(-5 + 8*t2 + 5*Power(t2,2))))))*R1q(s2))/
     (Power(-1 + s2,3)*s2*Power(s2 - t1,3)*(-s + s2 - t1)*(-1 + t1)*
       (-1 + s2 - t1 + t2)*(-t1 + s2*t2)) - 
    (16*(-(Power(s2,8)*t2*(-1 + Power(t1,2) + t1*(2 - s1 + t2))) + 
         Power(s2,7)*(3*Power(t2,2) + Power(t1,3)*(1 + 3*t2) - 
            t1*(1 + (4 + s)*t2 - (-5 + s + 3*s1)*Power(t2,2) + 
               3*Power(t2,3)) + 
            Power(t1,2)*(2 + Power(s1,2) + 8*t2 + Power(t2,2) - 
               s1*(1 + 8*t2))) + 
         Power(s2,6)*(-3*Power(t1,4)*(1 + t2) + 
            t2*(-2 + t2 + 3*Power(t2,2)) - 
            Power(t1,3)*(7 + 3*Power(s1,2) + t2 - 3*s*t2 + 
               2*Power(t2,2) - 2*s1*(4 + s + 11*t2)) - 
            t1*t2*(11 + 8*t2 + 3*Power(t2,2) + 3*Power(t2,3) + 
               s*(3 + 3*t2 - 3*Power(t2,2)) - s1*(1 + 3*Power(t2,2))) + 
            Power(t1,2)*(4 + 31*t2 + 20*Power(t2,2) + 8*Power(t2,3) + 
               Power(s1,2)*(-3 + 2*t2) + 
               s*(1 + 2*(5 + s1)*t2 - 5*Power(t2,2)) - 
               2*s1*(1 + 10*Power(t2,2)))) - 
         Power(s2,4)*(Power(t1,6) - 
            Power(t2,2)*(2 - 4*t2 + Power(t2,2)) - 
            t1*(-1 + (11 - 2*s)*t2 + (9 + 10*s)*Power(t2,2) - 
               2*(6 + 2*s + s1)*Power(t2,3) - (-7 + s)*Power(t2,4) + 
               (1 + s)*Power(t2,5)) + 
            Power(t1,5)*(-11 + Power(s,2) - 2*Power(s1,2) + 11*t2 + 
               7*Power(t2,2) - s*(5 + 3*t2) - s1*(26 + 11*t2)) + 
            Power(t1,4)*(-76 + Power(s,2)*(2 - 6*t2) + 14*t2 + 
               91*Power(t2,2) - 18*Power(t2,3) + 
               4*Power(s1,2)*(2 + t2) - 
               4*s*(9 + 5*s1 - 9*t2 - 7*Power(t2,2)) + 
               3*s1*(-3 + 6*t2 + 11*Power(t2,2))) + 
            Power(t1,3)*(11 - 78*t2 - 30*Power(t2,2) - 75*Power(t2,3) + 
               12*Power(t2,4) - 2*Power(s1,2)*(7 + 12*t2) + 
               Power(s,2)*(-9 - 25*t2 + 16*Power(t2,2)) + 
               s*(-18 + (-13 + 48*s1)*t2 + (46 + 6*s1)*Power(t2,2) - 
                  43*Power(t2,3)) + 
               s1*(27 + 65*t2 + 30*Power(t2,2) - 23*Power(t2,3))) + 
            Power(t1,2)*(14 + 4*t2 - 60*Power(t2,2) + 39*Power(t2,3) + 
               21*Power(t2,4) - 3*Power(t2,5) + 
               2*Power(s1,2)*t2*(-4 + 3*t2) + 
               Power(s,2)*t2*(-15 + 14*t2 - 4*Power(t2,2)) + 
               s1*(-2 + t2 + 19*Power(t2,2) - 18*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s*(6 + (-11 + 12*s1)*t2 - 22*Power(t2,2) - 
                  2*(7 + 3*s1)*Power(t2,3) + 15*Power(t2,4)))) + 
         Power(s2,5)*(Power(t1,5)*(3 + t2) + 
            t2*(1 - 5*t2 + 2*Power(t2,2) + Power(t2,3)) + 
            t1*(2 + 2*(5 + 3*s - s1)*t2 + 
               (-19 - 7*s + s1)*Power(t2,2) - (1 + 3*s)*Power(t2,3) + 
               (1 + 3*s + s1)*Power(t2,4) - Power(t2,5)) + 
            Power(t1,4)*(Power(s,2) + 2*Power(s1,2) - 
               s*(3 + 4*s1 + 5*t2) + t2*(-9 + 7*t2) - 2*s1*(11 + 13*t2)) \
+ Power(t1,2)*(9 + 16*t2 + Power(s,2)*(-9 + t2)*t2 + 
               Power(s1,2)*(-9 + t2)*t2 - 4*Power(t2,2) - 
               2*Power(t2,3) + 9*Power(t2,4) + 
               s1*(5 + 7*t2 + 15*Power(t2,2) - 16*Power(t2,3)) + 
               s*(3 + (-7 + 6*s1)*t2 + (11 + 6*s1)*Power(t2,2) - 
                  15*Power(t2,3))) - 
            Power(t1,3)*(26 + 93*t2 + 3*Power(s,2)*t2 - 19*Power(t2,2) + 
               14*Power(t2,3) + Power(s1,2)*(-8 + 3*t2) - 
               s1*(3 + 7*t2 + 41*Power(t2,2)) + 
               s*(13 + 27*t2 - 25*Power(t2,2) + 2*s1*(5 + t2)))) + 
         Power(t1,3)*(-((-1 - 2*s + 6*Power(s,2))*(-1 + t2)*
               Power(t2,2)) - 
            2*Power(t1,5)*(29 + Power(s,2) + Power(s1,2) + 2*t2 - 
               s1*t2 + s*(-2 - 2*s1 + t2)) + 
            t1*(4 - 4*t2 - 2*s1*Power(t2,2) + (5 + 2*s1)*Power(t2,3) - 
               6*Power(t2,4) + 
               Power(s,2)*(-24 + 24*t2 + 7*Power(t2,2) + Power(t2,3)) + 
               2*s*(4 - 4*t2 + (-1 + 2*s1)*Power(t2,2) - 
                  (5 + 2*s1)*Power(t2,3) + 5*Power(t2,4))) + 
            Power(t1,4)*(-20 + 78*t2 + 24*Power(t2,2) + 
               2*Power(s,2)*(7 + t2) + Power(s1,2)*(2 + 4*t2) - 
               3*s1*(2 + 2*t2 + Power(t2,2)) - 
               2*s*(43 - 4*t2 + 3*s1*(2 + t2))) + 
            Power(t1,3)*(50 - 38*t2 - 14*Power(t2,2) - 25*Power(t2,3) + 
               Power(s,2)*(16 - 18*t2 - 3*Power(t2,2)) - 
               2*Power(s1,2)*(-4 + 2*t2 + Power(t2,2)) + 
               s1*(-14 + 2*t2 + 7*Power(t2,2) + Power(t2,3)) + 
               s*(-106 + 140*t2 + 7*Power(t2,2) + Power(t2,3) + 
                  2*s1*(-12 + 9*t2 + 2*Power(t2,2)))) + 
            Power(t1,2)*(2*s1*(4 - 6*t2 + Power(t2,2)) + 
               Power(s,2)*(-28 - 4*t2 - 2*Power(t2,2) + 3*Power(t2,3)) + 
               t2*(-12 + 7*t2 + 11*Power(t2,2) + 5*Power(t2,3)) + 
               s*(8 + 44*t2 - 13*Power(t2,2) - 34*Power(t2,3) + 
                  Power(t2,4) - 
                  2*s1*(8 - 8*t2 - Power(t2,2) + Power(t2,3))))) - 
         Power(s2,3)*((-1 + t2)*Power(t2,3) + 
            t1*t2*(6 + 4*(-1 + s)*t2 - (5 + 6*s + 2*s1)*Power(t2,2) + 
               (3 + 2*s1)*Power(t2,3) - 4*Power(t2,4)) + 
            Power(t1,6)*(-4 + Power(s,2) + 3*Power(s1,2) + 
               s1*(11 - 2*t2) - 25*t2 - 2*Power(t2,2) + 
               s*(3 - 4*s1 + 3*t2)) + 
            Power(t1,2)*(6 + (21 + 8*s1)*t2 - (7 + 18*s1)*Power(t2,2) + 
               (-5 + 6*s1)*Power(t2,3) - 4*(-7 + s1)*Power(t2,4) + 
               7*Power(t2,5) + 
               Power(s,2)*t2*
                (6 - 20*t2 + 6*Power(t2,2) - 5*Power(t2,3)) + 
               s*(-2 + 16*t2 - 8*(-3 + s1)*Power(t2,2) + 
                  2*(-3 + 5*s1)*Power(t2,3) - (19 + 2*s1)*Power(t2,4) + 
                  5*Power(t2,5))) + 
            Power(t1,5)*(13 - 275*t2 + 4*Power(s,2)*t2 - 
               72*Power(t2,2) + 11*Power(t2,3) - 
               2*Power(s1,2)*(4 + 5*t2) + 
               s1*(27 - 14*t2 - 6*Power(t2,2)) + 
               s*(-11 - 93*t2 - 8*Power(t2,2) + 4*s1*(2 + t2))) + 
            Power(t1,4)*(Power(s,2)*(19 - 13*Power(t2,2)) + 
               Power(s1,2)*(44 + 22*t2 + 6*Power(t2,2)) + 
               s1*(-69 - 121*t2 - 23*Power(t2,2) + 7*Power(t2,3)) + 
               3*(23 + 46*t2 + 58*Power(t2,2) + 51*Power(t2,3) - 
                  3*Power(t2,4)) + 
               s*(45 - 195*t2 + 43*Power(t2,2) + 35*Power(t2,3) - 
                  2*s1*(19 + 35*t2))) + 
            Power(t1,3)*(-30 + 94*t2 - 2*Power(t2,2) - 102*Power(t2,3) - 
               63*Power(t2,4) + 3*Power(t2,5) + 
               Power(s1,2)*(8 + 20*t2 - 20*Power(t2,2)) + 
               3*Power(s,2)*(5 - 11*Power(t2,2) + 8*Power(t2,3)) + 
               s1*(10 - 57*t2 - 8*Power(t2,2) + 43*Power(t2,3) - 
                  4*Power(t2,4)) + 
               s*(10 + 43*t2 + 34*Power(t2,2) + 53*Power(t2,3) - 
                  26*Power(t2,4) + 
                  2*s1*(-6 - 29*t2 + 24*Power(t2,2) + Power(t2,3))))) + 
         Power(s2,2)*t1*((-1 + t2)*Power(t2,2)*(1 + 2*(1 + s)*t2) + 
            t1*(4 + 4*(2 + 3*s)*t2 - 
               2*(7 + 5*s + 6*Power(s,2) + s1)*Power(t2,2) + 
               (7 + 19*Power(s,2) + 4*s*(-3 + s1) - 2*s1)*Power(t2,3) - 
               (3 + Power(s,2) - 4*s1 + s*(2 + 4*s1))*Power(t2,4) + 
               2*(-4 + 3*s + Power(s,2))*Power(t2,5)) + 
            Power(t1,6)*(Power(s,2) + Power(s1,2) - 2*s1*(1 + t2) + 
               s*(3 - 2*s1 + 2*t2) - 2*(9 + 5*t2)) + 
            Power(t1,5)*(-170 + 6*Power(s,2) - 278*t2 - 
               17*Power(t2,2) + 2*Power(t2,3) - 
               Power(s1,2)*(11 + 6*t2) + 5*s1*(5 + Power(t2,2)) - 
               3*s*(23 - 2*(-6 + s1)*t2 + Power(t2,2))) + 
            Power(t1,2)*(15 - 7*t2 - 14*Power(t2,2) + 29*Power(t2,3) + 
               36*Power(t2,4) + 11*Power(t2,5) + 
               Power(s,2)*(6 - 48*t2 - 7*Power(t2,2) + 5*Power(t2,3) - 
                  13*Power(t2,4)) + 
               s1*(8 + 4*t2 - 34*Power(t2,2) + 18*Power(t2,3) - 
                  8*Power(t2,4)) + 
               s*(6 - 6*(-9 + 4*s1)*t2 + (43 + 22*s1)*Power(t2,2) + 
                  2*(-18 + s1)*Power(t2,3) - 42*Power(t2,4) + 
                  5*Power(t2,5))) - 
            Power(t1,4)*(-115 - 111*t2 - 320*Power(t2,2) - 
               104*Power(t2,3) + 3*Power(t2,4) - 
               4*Power(s1,2)*(12 + 3*t2 + 2*Power(t2,2)) + 
               Power(s,2)*(-12 + t2 + 5*Power(t2,2)) + 
               s1*(83 + 86*t2 + 19*Power(t2,2) + 6*Power(t2,3)) + 
               s*(126 + 232*t2 - 95*Power(t2,2) - 4*Power(t2,3) + 
                  2*s1*(42 + 17*t2 + Power(t2,2)))) + 
            Power(t1,3)*(24 + 132*t2 - 127*Power(t2,2) - 
               123*Power(t2,3) - 67*Power(t2,4) + Power(t2,5) + 
               12*Power(s1,2)*(2 + t2 - 2*Power(t2,2)) + 
               s1*t2*(-101 + 44*t2 + 33*Power(t2,2)) + 
               Power(s,2)*(11 - 55*t2 + 26*Power(t2,2) + 
                  10*Power(t2,3)) + 
               s*(28 - 57*t2 + 207*Power(t2,2) + 31*Power(t2,3) - 
                  13*Power(t2,4) - 
                  2*s1*(34 + 7*t2 - 32*Power(t2,2) + 3*Power(t2,3))))) - 
         s2*Power(t1,2)*(2*(-4 + s - s1)*Power(t1,6) - 
            (-1 + t2)*Power(t2,2)*
             (-2 - t2 + 6*Power(s,2)*t2 - 2*s*(1 + t2)) + 
            t1*(8 - 2*t2 - 4*(2 + s1)*Power(t2,2) + 
               (13 + 2*s1)*Power(t2,3) + (-11 + 2*s1)*Power(t2,4) - 
               4*Power(t2,5) + 
               Power(s,2)*t2*
                (-36 + 43*t2 + 8*Power(t2,2) - Power(t2,3) + 
                  2*Power(t2,4)) + 
               2*s*(4 + 2*t2 + 2*(-4 + s1)*Power(t2,2) - 8*Power(t2,3) - 
                  2*(-2 + s1)*Power(t2,4) + 3*Power(t2,5))) - 
            Power(t1,5)*(Power(s,2)*(-2 + t2) + Power(s1,2)*(8 + t2) + 
               2*(94 + 43*t2 + Power(t2,2)) - 
               s1*(8 + 5*t2 + 2*Power(t2,2)) - 
               s*(-30 + t2 - 2*Power(t2,2) + 2*s1*(3 + t2))) + 
            Power(t1,4)*(24 + 134*t2 + 191*Power(t2,2) + 
               21*Power(t2,3) + Power(s,2)*(22 + 5*t2 - Power(t2,2)) + 
               Power(s1,2)*(20 + 9*t2 + 3*Power(t2,2)) - 
               s1*(42 + 28*t2 + 14*Power(t2,2) + 3*Power(t2,3)) - 
               2*s*(118 + 13*t2 - 7*Power(t2,2) + 
                  s1*(29 + 6*t2 + Power(t2,2)))) + 
            Power(t1,3)*(92 + 4*t2 - 97*Power(t2,2) - 83*Power(t2,3) - 
               24*Power(t2,4) - 
               4*Power(s1,2)*(-6 + t2 + 3*Power(t2,2)) + 
               2*Power(s,2)*(-6 + t2 - 3*Power(t2,2) + Power(t2,3)) + 
               s1*(-22 - 45*t2 + 41*Power(t2,2) + 9*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(-94 + 57*t2 + 195*Power(t2,2) - 10*Power(t2,3) + 
                  Power(t2,4) - 
                  2*s1*(40 - 25*t2 - 10*Power(t2,2) + Power(t2,3)))) + 
            Power(t1,2)*(8 - 28*t2 + 4*Power(t2,2) + 35*Power(t2,3) + 
               21*Power(t2,4) + 5*Power(t2,5) - 
               2*s1*(-8 + 8*t2 + 7*Power(t2,2) - 5*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s,2)*(28 + 46*t2 - 11*Power(t2,2) + 
                  10*Power(t2,3) + 2*Power(t2,4)) + 
               s*(16 + 80*t2 + 16*Power(t2,2) - 68*Power(t2,3) - 
                  23*Power(t2,4) + Power(t2,5) + 
                  2*s1*(-8 - 4*t2 + 16*Power(t2,2) - 5*Power(t2,3) + 
                     Power(t2,4))))))*R1q(t1))/
     ((-1 + s2)*Power(s2 - t1,3)*(-s + s2 - t1)*(-1 + t1)*t1*
       (-1 + s2 - t1 + t2)*(-t1 + s2*t2)*
       (Power(s2,2) - 4*t1 + 2*s2*t2 + Power(t2,2))) - 
    (8*(-2*Power(s2,6)*t2 - 4*Power(t2,2)*Power(1 + (-1 + s)*t2,2) + 
         Power(s2,5)*(2*Power(s1,2) - 11*s1*t2 + (8 + 2*s - 3*t2)*t2 + 
            2*t1*(1 + t2)) - 4*Power(t1,4)*
          (23 + Power(s,2) + Power(s1,2) + 2*t2 - s1*t2 + 
            s*(-2 - 2*s1 + t2)) + 
         t1*(2*Power(s1,2)*t2*(-4 + 3*t2) + 
            4*Power(s,2)*t2*(-2 + 7*t2 + 2*Power(t2,2)) + 
            s1*(16 - 2*t2 + 11*Power(t2,2) - 7*Power(t2,3)) + 
            s*(-16 + 2*(19 + 8*s1)*t2 + (-32 + 6*s1)*Power(t2,2) - 
               (20 + 11*s1)*Power(t2,3)) + 
            2*(-8 + t2 - 4*Power(t2,3) + 3*Power(t2,4))) + 
         2*Power(t1,3)*(4 + 23*t2 + 5*Power(s1,2)*t2 + 23*Power(t2,2) + 
            Power(s,2)*(14 + 3*t2) - 4*s1*(12 - t2 + Power(t2,2)) + 
            s*(-78 + 10*t2 + Power(t2,2) - 2*s1*(5 + 4*t2))) + 
         Power(s2,4)*(-2*Power(t1,2) + Power(s1,2)*(-6 - 2*t1 + 7*t2) + 
            t1*(-8 + s*(-2 + t2) + 6*t2 + 4*Power(t2,2)) + 
            t2*(31 + 7*t2 + s*(4 + t2)) + 
            s1*(-4 + 22*t2 + 4*s*t2 - 31*Power(t2,2) + 
               t1*(11 + 4*s + 19*t2))) + 
         Power(t1,2)*(4 + 40*t2 - 2*Power(t2,2) - 32*Power(t2,3) + 
            Power(s1,2)*(8 - 2*t2 - 7*Power(t2,2)) - 
            2*Power(s,2)*(4 + 23*t2 + 4*Power(t2,2)) + 
            s1*(-64 + 10*t2 + 37*Power(t2,2)) + 
            s*(2*(-6 + 65*t2 + 3*Power(t2,2)) + 
               s1*(-96 + 60*t2 + 13*Power(t2,2)))) + 
         Power(s2,3)*(2 - 24*t2 - 16*s*t2 - 10*Power(s,2)*t2 + 
            56*Power(t2,2) + 16*s*Power(t2,2) + 
            2*Power(s,2)*Power(t2,2) - 16*Power(t2,3) - 
            4*s*Power(t2,3) + Power(t2,4) + 
            Power(s1,2)*(t1 - 2*Power(t1,2) - t1*t2 + 
               2*t2*(-9 + 4*t2)) + 
            Power(t1,2)*(-3 + 2*Power(s,2) - 2*t2 + 4*Power(t2,2) + 
               s*(-1 + 4*t2)) - 
            t1*(31 + 73*t2 + 2*Power(s,2)*t2 - 6*Power(t2,2) - 
               2*Power(t2,3) + s*(8 + 29*t2 - 2*Power(t2,2))) + 
            s1*(12 + (-15 + 2*s)*t2 + (59 + 11*s)*Power(t2,2) - 
               29*Power(t2,3) - Power(t1,2)*(19 + 6*t2) + 
               t1*(-18 + 20*t2 + 38*Power(t2,2) + s*(-20 + 7*t2)))) + 
         Power(s2,2)*(-6 + 2*Power(t1,3)*
             (-1 + Power(s,2) + Power(s1,2) + s1*(3 - 2*t2) - 
               2*s*(1 + s1 - t2) - 10*t2) + 
            2*(-11 + 4*Power(s,2) + 9*s1 + 4*Power(s1,2) - 
               s*(3 + 8*s1))*t2 - 
            2*(5 + 12*Power(s,2) + 19*s1 - 2*s*s1 + 9*Power(s1,2))*
             Power(t2,2) + (35 + 4*Power(s,2) + 52*s1 + 
               3*Power(s1,2) + 10*s*(2 + s1))*Power(t2,3) - 
            3*(7 + s + 3*s1)*Power(t2,4) + 
            Power(t1,2)*(66 + Power(s1,2)*(1 - 10*t2) + 
               2*Power(s,2)*(-4 + t2) - 48*t2 - 50*Power(t2,2) + 
               4*Power(t2,3) + s1*(15 - 46*t2 + 2*Power(t2,2)) + 
               s*(28 - 3*s1 - 46*t2 + 8*s1*t2 + 2*Power(t2,2))) + 
            t1*(22 - 34*t2 + 50*Power(t2,2) + 46*Power(t2,3) + 
               Power(s,2)*(10 + 18*t2 - 4*Power(t2,2)) + 
               4*Power(s1,2)*(7 + t2 + Power(t2,2)) + 
               s1*(-1 - 165*t2 - 31*Power(t2,2) + 19*Power(t2,3)) + 
               s*(32 - 10*t2 - 2*Power(t2,2) + Power(t2,3) + 
                  s1*(10 - 75*t2 + 2*Power(t2,2))))) + 
         s2*(-4*(-4 + s - s1)*Power(t1,4) + 
            2*Power(t1,3)*(21 + Power(s,2)*(-5 + t2) + 70*t2 + 
               2*Power(t2,2) + Power(s1,2)*(5 + t2) - 
               s1*(-2 + t2 + 2*Power(t2,2)) + 
               s*(24 - (5 + 2*s1)*t2 + 2*Power(t2,2))) + 
            t2*(26 - 20*t2 + 2*Power(s1,2)*(4 - 3*t2)*t2 + 
               6*Power(t2,2) + 10*Power(t2,3) - 6*Power(t2,4) + 
               2*Power(s,2)*t2*(4 - 9*t2 + Power(t2,2)) + 
               s1*(-16 + 10*t2 - 27*Power(t2,2) + 15*Power(t2,3)) + 
               s*(16 - 2*(9 + 8*s1)*t2 + 2*(12 + s1)*Power(t2,2) + 
                  (8 + 3*s1)*Power(t2,3))) - 
            Power(t1,2)*(24 + 40*t2 + 93*Power(t2,2) + 46*Power(t2,3) + 
               Power(s,2)*(6 + 4*t2) + 
               Power(s1,2)*(30 - 2*t2 + 8*Power(t2,2)) - 
               s1*(106 + 156*t2 - 23*Power(t2,2) + 8*Power(t2,3)) + 
               s*(10 - 154*t2 + 25*Power(t2,2) + 2*Power(t2,3) + 
                  s1*(-80 + 2*t2 - 8*Power(t2,2)))) + 
            t1*(30 + 6*t2 - 75*Power(t2,2) + 21*Power(t2,3) + 
               32*Power(t2,4) - 
               2*Power(s,2)*(4 - 13*t2 - 9*Power(t2,2) + Power(t2,3)) + 
               Power(s1,2)*(-8 + 6*t2 + 7*Power(t2,2) + 3*Power(t2,3)) - 
               2*s1*(19 - 69*t2 + 37*Power(t2,2) + 16*Power(t2,3)) - 
               s*(6 + 4*t2 + 130*Power(t2,2) - 5*Power(t2,3) + 
                  s1*(-16 - 72*t2 + 62*Power(t2,2) + Power(t2,3))))))*
       R2q(1 - s2 + t1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s2 - t1 + t2)*(-t1 + s2*t2)*
       (Power(s2,2) - 4*t1 + 2*s2*t2 + Power(t2,2))) + 
    (8*(-24*Power(t1,6) - 2*Power(t2,4)*Power(1 + (-1 + s)*t2,2) + 
         Power(s2,8)*(Power(s1,2) - 2*s1*t2 - Power(t2,2)) + 
         t1*Power(t2,2)*(12 + (-13 + 31*s - 3*s1)*t2 + 
            (7 + 19*Power(s,2) + s*(-21 + s1))*Power(t2,2) + 
            (-9 - s + 3*Power(s,2) + 3*s1 - 3*s*s1)*Power(t2,3) - 
            3*(-1 + s)*Power(t2,4)) + 
         2*Power(t1,5)*(-24 + 49*t2 + Power(s,2)*t2 + Power(s1,2)*t2 + 
            5*Power(t2,2) - s1*(24 - 2*t2 + Power(t2,2)) + 
            s*(8 - 2*(1 + s1)*t2 + Power(t2,2))) - 
         Power(t1,2)*(-16 + 30*t2 - 10*Power(t2,2) - 54*Power(t2,3) - 
            3*Power(t2,4) + 9*Power(t2,5) + 
            Power(s1,2)*t2*(-8 - 4*t2 + 7*Power(t2,2) + Power(t2,3)) + 
            Power(s,2)*t2*(-8 + 62*t2 + 25*Power(t2,2) + 
               3*Power(t2,3)) + 
            s*(-16 + 2*(29 + 8*s1)*t2 + (2 + 6*s1)*Power(t2,2) + 
               (2 - 32*s1)*Power(t2,3) - 4*(4 + s1)*Power(t2,4) - 
               7*Power(t2,5)) + 
            s1*(16 - 6*t2 + 8*Power(t2,2) + 22*Power(t2,3) - 
               15*Power(t2,4) + 3*Power(t2,5))) + 
         Power(s2,7)*(-(Power(s1,2)*(2 + t1 - 5*t2)) + 
            t2*(2 + s*(-2 + t2) - 3*t2 - 4*Power(t2,2) + 
               2*t1*(1 + t2)) + 
            s1*(-2 + (11 + s)*t2 - 13*Power(t2,2) + t1*(2 + 2*s + 3*t2))) \
- Power(s2,6)*(-1 + 11*t2 - 5*s*t2 - 20*Power(t2,2) + 4*s*Power(t2,2) + 
            18*Power(t2,3) - 4*s*Power(t2,3) + 6*Power(t2,4) - 
            Power(t1,2)*(-1 + Power(s,2) - 4*t2 + 3*s*t2 + 
               Power(t2,2)) + 
            t1*(2 + (-3 - 3*s + Power(s,2))*t2 + 
               (-25 + 3*s)*Power(t2,2) - 6*Power(t2,3)) + 
            Power(s1,2)*(3 + Power(t1,2) + 8*t2 - 10*Power(t2,2) + 
               t1*(4 + 5*t2)) + 
            s1*(-4 + 3*Power(t1,2) + 2*(6 + s)*t2 - 
               (47 + 5*s)*Power(t2,2) + 32*Power(t2,3) + 
               t1*(9 + s*(7 - 10*t2) - 6*t2 - 22*Power(t2,2)))) - 
         2*Power(t1,4)*(-28 - 95*t2 + 49*Power(t2,2) + 17*Power(t2,3) + 
            2*Power(s1,2)*(-2 + 3*t2 + Power(t2,2)) + 
            Power(s,2)*(12 + 6*t2 + Power(t2,2)) - 
            s1*(-60 + 45*t2 + 4*Power(t2,2) + Power(t2,3)) - 
            s*(84 - 11*t2 + 3*Power(t2,2) + 
               s1*(8 + 12*t2 + 3*Power(t2,2)))) + 
         Power(t1,3)*(-32 - 50*t2 - 102*Power(t2,2) - 3*Power(t2,3) + 
            32*Power(t2,4) + 2*Power(s,2)*
             (24 + 23*t2 + 13*Power(t2,2) + Power(t2,3)) + 
            2*Power(s1,2)*(-16 + 7*t2 + 5*Power(t2,2) + 2*Power(t2,3)) + 
            s1*(56 + 68*t2 - 34*Power(t2,2) - 19*Power(t2,3) + 
               2*Power(t2,4)) - 
            s*(-88 + 20*t2 + 50*Power(t2,2) + 11*Power(t2,3) + 
               6*Power(t2,4) + 
               6*s1*(-8 + 10*t2 + 6*Power(t2,2) + Power(t2,3)))) + 
         Power(s2,5)*(-2 + 7*t2 + 4*s*t2 + 5*Power(t2,2) + 
            32*s*Power(t2,2) + 67*Power(t2,3) + 8*s*Power(t2,3) - 
            41*Power(t2,4) + 6*s*Power(t2,4) - 4*Power(t2,5) + 
            (-2 + s)*Power(t1,3)*(-1 + s + t2) + 
            Power(t1,2)*(Power(s,2)*(-3 + 2*t2) + 
               4*s*(-1 - 3*t2 + 2*Power(t2,2)) + 
               t2*(-38 - 39*t2 + 3*Power(t2,2))) + 
            Power(s1,2)*(Power(t1,3) - 3*Power(t1,2)*(-2 + t2) + 
               t1*(21 - 14*t2 - 6*Power(t2,2)) + 
               t2*(-4 - 15*t2 + 10*Power(t2,2))) + 
            t1*(10 - 20*t2 - 3*Power(s,2)*(-1 + t2)*t2 - 
               2*Power(t2,2) + 85*Power(t2,3) + 6*Power(t2,4) + 
               s*(1 - 9*t2 + 2*Power(t2,2) - 9*Power(t2,3))) - 
            s1*(-6 + (4 + 7*s)*t2 + (41 + 6*s)*Power(t2,2) - 
               2*(41 + 5*s)*Power(t2,3) + 38*Power(t2,4) + 
               Power(t1,3)*(2*s + t2) + 
               Power(t1,2)*(-9 - s*(-12 + t2) + 36*t2 + 4*Power(t2,2)) - 
               t1*(10 - (107 + 34*s)*t2 + (7 + 15*s)*Power(t2,2) + 
                  46*Power(t2,3)))) + 
         Power(s2,4)*(-3 + (1 - s + s1)*Power(t1,4) + 
            (12 + s + 11*s1)*t2 - 
            (27 + 12*Power(s,2) + 2*s1 + s*(33 + 4*s1))*Power(t2,2) + 
            (47 + s*(47 - 7*s1) - 62*s1 - 17*Power(s1,2))*Power(t2,3) + 
            (88 + 74*s1 + 5*Power(s1,2) + s*(31 + 10*s1))*Power(t2,4) + 
            (-45 + 4*s - 22*s1)*Power(t2,5) - Power(t2,6) + 
            Power(t1,3)*(17 + 61*t2 - 12*Power(t2,2) + 
               3*Power(s1,2)*(2 + t2) + Power(s,2)*(-7 + 3*t2) - 
               3*s1*(-4 - 2*t2 + Power(t2,2)) + 
               s*(17 + s1 - 24*t2 - 6*s1*t2 + 3*Power(t2,2))) + 
            Power(t1,2)*(-1 + Power(s,2)*(-1 + t2) + 50*t2 - 
               188*Power(t2,2) - 95*Power(t2,3) + 3*Power(t2,4) + 
               Power(s1,2)*(-10 + 14*t2 - 7*Power(t2,2)) - 
               2*s1*(-29 - 74*t2 + 50*Power(t2,2) + 2*Power(t2,3)) + 
               s*(15 - 7*t2 - 14*Power(t2,2) + 6*Power(t2,3) + 
                  s1*(51 - 35*t2 + 7*Power(t2,2)))) + 
            t1*(-6 + 8*t2 - 140*Power(t2,2) + 30*Power(t2,3) + 
               133*Power(t2,4) + 2*Power(t2,5) + 
               Power(s,2)*t2*(-3 + 14*t2 - 3*Power(t2,2)) + 
               Power(s1,2)*(15 + 49*t2 - 15*Power(t2,2) + 
                  2*Power(t2,3)) + 
               s1*(-22 + 91*t2 - 259*Power(t2,2) - 8*Power(t2,3) + 
                  36*Power(t2,4)) + 
               s*(-2 - 72*t2 - 59*Power(t2,2) - 9*Power(t2,3) - 
                  9*Power(t2,4) + 
                  s1*(13 + 22*t2 - 69*Power(t2,2) + 5*Power(t2,3))))) + 
         Power(s2,3)*(Power(t1,4)*
             (-28 - 6*Power(s,2) - 6*Power(s1,2) + 
               s*(16 + 12*s1 - 9*t2) + 15*t2 + s1*(-2 + 9*t2)) - 
            t1*(-1 - 39*t2 + 117*Power(t2,2) + 317*Power(t2,3) - 
               72*Power(t2,4) - 101*Power(t2,5) + 
               Power(s1,2)*t2*
                (9 - 45*t2 + 5*Power(t2,2) - 7*Power(t2,3)) + 
               Power(s,2)*t2*
                (-25 + 8*t2 - 22*Power(t2,2) + Power(t2,3)) + 
               s1*(33 - 26*t2 - 232*Power(t2,2) + 229*Power(t2,3) + 
                  20*Power(t2,4) - 7*Power(t2,5)) + 
               s*(7 - (71 + 40*s1)*t2 - 7*(-27 + 7*s1)*Power(t2,2) + 
                  (148 + 67*s1)*Power(t2,3) + (19 + 5*s1)*Power(t2,4) + 
                  3*Power(t2,5))) + 
            Power(t1,3)*(-29 + 147*t2 + 299*Power(t2,2) + 
               6*Power(t2,3) + Power(s,2)*(16 - 3*t2 + 3*Power(t2,2)) + 
               Power(s1,2)*(-4 + 14*t2 + 3*Power(t2,2)) - 
               3*s1*(43 - 6*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               s*(-1 + 109*t2 - 14*Power(t2,2) + 3*Power(t2,3) + 
                  s1*(12 - 11*t2 - 6*Power(t2,2)))) + 
            t2*(-7 + 2*Power(s,2)*(4 - 15*t2)*t2 - 23*Power(t2,2) + 
               57*Power(t2,3) + 42*Power(t2,4) - 24*Power(t2,5) + 
               Power(s1,2)*t2*(8 - 11*Power(t2,2) + Power(t2,3)) - 
               s1*t2*(3 + 2*t2 + 38*Power(t2,2) - 35*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s*t2*(7 - 59*t2 + 17*Power(t2,2) + 35*Power(t2,3) + 
                  Power(t2,4) + 
                  s1*(-16 + 16*t2 - 5*Power(t2,2) + 5*Power(t2,3)))) + 
            Power(t1,2)*(-12 + 16*t2 + 89*Power(t2,2) - 
               400*Power(t2,3) - 95*Power(t2,4) + Power(t2,5) + 
               Power(s,2)*(7 - 34*t2 + 3*Power(t2,2) - 2*Power(t2,3)) - 
               Power(s1,2)*(65 + 9*t2 + 6*Power(t2,2) + 9*Power(t2,3)) + 
               s1*(-22 + 279*t2 + 327*Power(t2,2) - 78*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s*(18 + 121*t2 + 4*Power(t2,3) + 
                  s1*(-14 + 131*t2 - 11*Power(t2,2) + 11*Power(t2,3))))) \
+ Power(s2,2)*(6*(-1 + s - s1)*Power(t1,5) - 
            t1*(-18 + (41 + 16*Power(s,2) + s*(27 - 32*s1) - 15*s1 + 
                  16*Power(s1,2))*t2 - 
               (31 + 91*Power(s,2) + s*(195 - 26*s1) + 46*s1 - 
                  17*Power(s1,2))*Power(t2,2) - 
               (-149 + 4*Power(s,2) + 134*s1 + 31*Power(s1,2) + 
                  s*(-71 + 17*s1))*Power(t2,3) + 
               (185 - 14*Power(s,2) + 88*s1 + Power(s1,2) + 
                  2*s*(65 + 14*s1))*Power(t2,4) + 
               3*(-17 + 2*s1 - Power(s1,2) + s*(6 + s1))*Power(t2,5) + 
               2*(-15 + s1)*Power(t2,6)) - 
            Power(t1,4)*(38 + 6*Power(s,2)*(-2 + t2) + 340*t2 + 
               45*Power(t2,2) + 2*Power(s1,2)*(5 + 3*t2) - 
               3*s1*(14 + 2*t2 + Power(t2,2)) + 
               s*(92 + s1*(2 - 12*t2) - 16*t2 + 3*Power(t2,2))) - 
            Power(t2,2)*(-31 + 28*t2 - 12*Power(t2,2) - 
               34*Power(t2,3) + 5*Power(t2,5) + 
               2*Power(s,2)*t2*(-4 + 13*t2) + 
               Power(s1,2)*t2*(-8 + t2 + 3*Power(t2,2)) + 
               s1*(16 - 3*t2 + 16*Power(t2,2) + 6*Power(t2,3) - 
                  7*Power(t2,4)) + 
               s*(-16 + (7 + 16*s1)*t2 + (21 - 16*s1)*Power(t2,2) + 
                  (4 + 3*s1)*Power(t2,3) - (17 + s1)*Power(t2,4))) + 
            Power(t1,3)*(60 - 63*t2 + 363*Power(t2,2) + 
               285*Power(t2,3) + 8*Power(t2,4) + 
               Power(s,2)*(14 - 6*t2 + 5*Power(t2,2) + Power(t2,3)) + 
               Power(s1,2)*(42 + 20*Power(t2,2) + Power(t2,3)) - 
               s1*(110 + 521*t2 - 42*Power(t2,2) + 10*Power(t2,3) + 
                  Power(t2,4)) - 
               s*(62 + 53*t2 + 7*Power(t2,2) + 4*Power(t2,3) - 
                  Power(t2,4) + 
                  s1*(104 - 22*t2 + 25*Power(t2,2) + 2*Power(t2,3)))) - 
            Power(t1,2)*(26 - 84*t2 - 472*Power(t2,2) - 
               139*Power(t2,3) + 291*Power(t2,4) + 35*Power(t2,5) + 
               Power(s,2)*(16 - 31*t2 + 56*Power(t2,2) + 
                  5*Power(t2,3) + Power(t2,4)) + 
               Power(s1,2)*(6 + 27*t2 - 11*Power(t2,2) + 
                  22*Power(t2,3) + 4*Power(t2,4)) - 
               s1*(38 - 314*t2 + 107*Power(t2,2) + 193*Power(t2,3) - 
                  11*Power(t2,4) + 4*Power(t2,5)) + 
               s*(40 - 278*t2 - 365*Power(t2,2) - 62*Power(t2,3) - 
                  12*Power(t2,4) + Power(t2,5) + 
                  s1*(58 + 116*t2 - 101*Power(t2,2) - 23*Power(t2,3) - 
                     5*Power(t2,4))))) + 
         s2*(2*Power(t1,5)*(65 + 3*Power(s,2) - 4*s1 + 3*Power(s1,2) - 
               2*s*(2 + 3*s1) + 30*t2) + 
            Power(t2,3)*(-3 + (-6 - 13*s + 7*s1)*t2 + 
               (4 - 10*Power(s,2) - 8*s1 + s*(5 + 3*s1))*Power(t2,2) + 
               (8 + s1 - s*(1 + s1))*Power(t2,3) + 3*(-1 + s)*Power(t2,4)\
) - Power(t1,4)*(18 + 44*t2 + 290*Power(t2,2) + 19*Power(t2,3) + 
               2*Power(s1,2)*(2 + 5*t2 + 2*Power(t2,2)) + 
               2*Power(s,2)*(6 + 5*t2 + 2*Power(t2,2)) - 
               s1*(246 + 42*t2 + 2*Power(t2,2) + 3*Power(t2,3)) + 
               s*(-62 - 2*(1 + 10*s1)*t2 - 8*(1 + s1)*Power(t2,2) + 
                  3*Power(t2,3))) - 
            t1*t2*(62 - 93*t2 + 47*Power(t2,2) + 83*Power(t2,3) + 
               3*Power(t2,4) - 14*Power(t2,5) + 
               Power(s1,2)*t2*
                (16 + 5*t2 - 14*Power(t2,2) + Power(t2,3)) - 
               Power(s,2)*t2*
                (-16 + 73*t2 + 12*Power(t2,2) + 3*Power(t2,3)) - 
               s1*(32 - 27*t2 + 62*Power(t2,2) + 6*Power(t2,3) - 
                  20*Power(t2,4) + 3*Power(t2,5)) + 
               s*(32 - (35 + 32*s1)*t2 + (-45 + 28*s1)*Power(t2,2) + 
                  (-16 + 13*s1)*Power(t2,3) + (35 + 3*s1)*Power(t2,4) + 
                  7*Power(t2,5))) + 
            Power(t1,3)*(-10 - 302*t2 - 353*Power(t2,2) + 
               289*Power(t2,3) + 69*Power(t2,4) + 
               Power(s,2)*(-38 + 64*t2 + 28*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(s1,2)*(34 - 44*t2 + 32*Power(t2,2) + 
                  8*Power(t2,3)) + 
               s1*(88 + 184*t2 - 243*Power(t2,2) - 2*Power(t2,3) - 
                  6*Power(t2,4)) - 
               s*(104 + 432*t2 + 31*Power(t2,2) + 17*Power(t2,3) - 
                  Power(t2,4) + 
                  s1*(-84 + 36*t2 + 52*Power(t2,2) + 13*Power(t2,3)))) + 
            Power(t1,2)*(10 + 52*t2 + 134*Power(t2,2) + 246*Power(t2,3) - 
               25*Power(t2,4) - 62*Power(t2,5) + 
               Power(s1,2)*(8 + 58*t2 - 53*Power(t2,2) + Power(t2,3) - 
                  8*Power(t2,4)) - 
               Power(s,2)*(-8 + 98*t2 + 29*Power(t2,2) + 
                  42*Power(t2,3) + 4*Power(t2,4)) + 
               s1*(18 - 170*t2 - 114*Power(t2,2) + 77*Power(t2,3) + 
                  30*Power(t2,4)) + 
               s*(42 - 234*t2 + 50*Power(t2,2) + 155*Power(t2,3) + 
                  30*Power(t2,4) + 6*Power(t2,5) + 
                  s1*(-16 - 8*t2 + 26*Power(t2,2) + 49*Power(t2,3) + 
                     11*Power(t2,4))))))*T2q(t1,1 - s2 + t1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s2 - t1 + t2)*
       Power(-t1 + s2*t2,2)*(Power(s2,2) - 4*t1 + 2*s2*t2 + Power(t2,2))) + 
    (8*(Power(s2,8)*(-Power(s1,2) + 2*s1*t2 + Power(t2,2)) + 
         Power(s2,7)*(Power(s1,2)*(2 + 4*t1 - 2*t2) + 
            t2*(-2 - s*(-2 + t2) + 3*t2 + Power(t2,2) - t1*(2 + 5*t2)) - 
            s1*(-2 + (11 + s)*t2 - 3*Power(t2,2) + t1*(2 + 2*s + 9*t2))) \
+ Power(s2,6)*(-1 + 11*t2 - 5*s*t2 - 14*Power(t2,2) + 2*s*Power(t2,2) + 
            5*Power(t2,3) - s*Power(t2,3) - 
            Power(s1,2)*(-3 + 5*Power(t1,2) - 8*t1*(-1 + t2) - 2*t2 + 
               Power(t2,2)) + 
            Power(t1,2)*(1 - Power(s,2) + 10*t2 - 3*s*t2 + 
               8*Power(t2,2)) + 
            t1*(2 + (3 - 9*s + Power(s,2))*t2 + 
               2*(-5 + 3*s)*Power(t2,2) - 3*Power(t2,3)) + 
            s1*(-4 + 2*(3 + s)*t2 - 2*(7 + s)*Power(t2,2) + 
               Power(t2,3) + 3*Power(t1,2)*(3 + 2*s + 5*t2) + 
               t1*(3 - s*(-7 + t2) + 53*t2 - 14*Power(t2,2)))) - 
         Power(s2,5)*(-2 + 4*t2 + 4*s*t2 + 2*Power(t2,2) + 
            5*s*Power(t2,2) + 21*Power(t2,3) + 2*s*Power(t2,3) - 
            Power(t2,4) + Power(t1,3)*
             (5 - 2*Power(s,2) + 16*t2 + 4*Power(t2,2) - s*(3 + 8*t2)) + 
            Power(s1,2)*(12*Power(t1,2)*(-1 + t2) - (-5 + t2)*t2 + 
               t1*(18 + 4*t2 - 4*Power(t2,2))) + 
            Power(t1,2)*(6 - 2*t2 - 23*Power(t2,2) + Power(t2,3) + 
               Power(s,2)*(-3 + 2*t2) + s*(-4 - 27*t2 + 11*Power(t2,2))) \
+ t1*(7 + 31*t2 + 3*Power(s,2)*t2 - 55*Power(t2,2) + 11*Power(t2,3) - 
               2*Power(t2,4) + 
               s*(1 - 28*t2 + 13*Power(t2,2) - 3*Power(t2,3))) + 
            s1*(6 - (16 + 7*s)*t2 - (15 + 2*s)*Power(t2,2) + 
               (5 + s)*Power(t2,3) + Power(t1,3)*(15 + 4*s + 10*t2) + 
               Power(t1,2)*(46 + s*(21 - 8*t2) + 100*t2 - 
                  24*Power(t2,2)) + 
               t1*(-14 - (-4 + s)*t2 - 5*(11 + s)*Power(t2,2) + 
                  7*Power(t2,3)))) - 
         Power(t1,2)*(Power(s,2)*
             (4 + 2*Power(t1,4) - 4*t2 - Power(t1,3)*(7 + 5*t2) + 
               Power(t1,2)*(-6 + 9*t2 + 5*Power(t2,2)) - 
               2*t1*(-8 + 5*t2 - Power(t2,2) + Power(t2,3))) + 
            t1*(-2*(-4 + s1)*Power(t1,4) - 2*Power(-1 + t2,2)*t2 + 
               t1*(-1 + t2)*(-5 + 3*Power(t2,2) + s1*(-1 + 3*t2)) + 
               Power(t1,3)*(23 + 2*Power(s1,2) - 10*t2 + 
                  s1*(-1 + 4*t2)) + 
               Power(t1,2)*(4 - 29*t2 + Power(t2,2) - 
                  Power(s1,2)*(3 + t2) + s1*(20 + 3*t2 - 3*Power(t2,2)))\
) + s*t1*(2*Power(t1,4) + Power(t1,3)*(7 - 8*t2) + 
               3*Power(t1,2)*(6 - 4*t2 + 3*Power(t2,2)) + 
               t1*(13 - 47*t2 + 7*Power(t2,2) - 3*Power(t2,3)) + 
               2*(-5 + 2*t2 + Power(t2,2) + 2*Power(t2,3)) + 
               s1*(4 - 4*Power(t1,3) - 4*t2 + Power(t1,2)*(6 + 4*t2) - 
                  t1*(-10 + t2 + 3*Power(t2,2))))) + 
         s2*t1*(-(Power(s,2)*
               (Power(t1,5) + 8*(-1 + t2)*t2 - Power(t1,4)*(5 + 4*t2) + 
                 Power(t1,3)*(3 + 18*t2 + 7*Power(t2,2)) + 
                 Power(t1,2)*
                  (2 + 3*t2 - 19*Power(t2,2) - 4*Power(t2,3)) + 
                 2*t1*(-2 - 17*t2 + 9*Power(t2,2) + Power(t2,3)))) + 
            t1*((1 + s1)*Power(t1,5) - 6*Power(-1 + t2,2)*t2 + 
               t1*(-1 + t2)*(-18 + s1 - 9*t2 + 8*s1*t2 + 
                  16*Power(t2,2) - s1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,4)*(27 + 11*t2 - 2*s1*(10 + t2)) + 
               Power(t1,3)*(38 + 46*t2 - 20*Power(t2,2) + 
                  Power(s1,2)*(2 + 4*t2) + 
                  s1*(5 + 17*t2 + 4*Power(t2,2))) + 
               Power(t1,2)*(-4 - 51*t2 - 71*Power(t2,2) + 
                  6*Power(t2,3) + 
                  Power(s1,2)*(-12 - 9*t2 + Power(t2,2)) + 
                  s1*(62 + 54*t2 - 13*Power(t2,2) - 3*Power(t2,3)))) + 
            s*t1*(-Power(t1,5) + Power(t1,4)*(23 + 3*t2) + 
               Power(t1,3)*(50 - 43*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(16 + 7*t2 + 15*Power(t2,2) + 
                  11*Power(t2,3)) + 
               2*(-5 - 8*t2 + Power(t2,2) + 12*Power(t2,3)) - 
               t1*(3 + 38*t2 + 84*Power(t2,2) - 8*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s1*(4 + Power(t1,4) + 4*t2 - 8*Power(t2,2) - 
                  Power(t1,3)*(3 + 7*t2) + 
                  3*Power(t1,2)*(15 + Power(t2,2)) + 
                  t1*(20 + 19*t2 - 16*Power(t2,2) + Power(t2,3))))) + 
         Power(s2,2)*(4*Power(s,2)*(-1 + t2)*Power(t2,2) + 
            Power(t1,6)*(-1 + Power(s,2) + Power(s1,2) - 2*t2 - 
               s1*(3 + t2) + s*(-2*s1 + t2)) - 
            Power(t1,5)*(29 + 44*t2 + Power(s,2)*t2 - Power(t2,2) - 
               s1*(73 + 13*t2) + 
               s*(61 - s1*(-3 + t2) + 21*t2 + Power(t2,2))) - 
            2*t1*t2*(-3*Power(-1 + t2,2) + 
               Power(s,2)*(4 + 10*t2 - 6*Power(t2,2) + Power(t2,3)) + 
               s*(-1 + t2)*(10 + 17*t2 + 3*Power(t2,2) - 2*s1*(2 + t2))) \
- Power(t1,4)*(-16 + 148*t2 + Power(t2,2) - 8*Power(t2,3) + 
               Power(s,2)*(3 + 9*t2) + 
               Power(s1,2)*(-15 + 14*t2 + Power(t2,2)) + 
               s1*(19 + 53*t2 + 13*Power(t2,2)) - 
               s*(-81 + 18*t2 + 59*Power(t2,2) + 2*Power(t2,3) + 
                  s1*(-15 + 14*t2 + Power(t2,2)))) + 
            Power(t1,2)*(Power(s,2)*
                (8 - 15*t2 + 9*Power(t2,2) - 6*Power(t2,3) - 
                  2*Power(t2,4)) + 
               (-1 + t2)*(s1*(-5 - 14*t2 + 7*Power(t2,2)) + 
                  3*(6 + 11*t2 - 8*Power(t2,2) - 5*Power(t2,3))) + 
               s*(9 + 46*t2 + 92*Power(t2,2) + 32*Power(t2,3) + 
                  Power(t2,4) - 
                  s1*(10 + 41*t2 - 16*Power(t2,2) + Power(t2,3)))) + 
            Power(t1,3)*(18 + 35*t2 + 161*Power(t2,2) + 31*Power(t2,3) - 
               5*Power(t2,4) + 
               Power(s1,2)*(18 + 26*t2 - 4*Power(t2,2)) + 
               Power(s,2)*(7 - 18*t2 + 21*Power(t2,2) + 2*Power(t2,3)) + 
               2*s1*(-30 - 80*t2 + 3*Power(t2,2) + 7*Power(t2,3)) - 
               s*(-18 + 9*t2 + 22*Power(t2,2) + 31*Power(t2,3) + 
                  2*Power(t2,4) + 
                  s1*(81 + 21*t2 - 11*Power(t2,2) + Power(t2,3))))) + 
         Power(s2,4)*(3 - (18 + s - 7*s1)*t2 + 
            (16 + 4*Power(s,2) + s*(23 - 7*s1) - 12*s1)*Power(t2,2) + 
            (8 + 2*Power(s,2) + 5*s1 + s*(9 + s1))*Power(t2,3) - 
            (9 + s)*Power(t2,4) - 
            Power(t1,4)*(-8 - 10*s1 - 5*Power(s1,2) - 8*t2 + 
               Power(t2,2) + s*(8 + 4*s1 + 6*t2)) + 
            Power(t1,3)*(7 - 8*Power(s,2) + 8*Power(s1,2)*(-1 + t2) - 
               27*t2 - 27*Power(t2,2) + 7*Power(t2,3) + 
               s1*(114 + 94*t2 - 18*Power(t2,2)) + 
               s*(-29 + s1*(20 - 8*t2) - 50*t2 + 7*Power(t2,2))) + 
            Power(t1,2)*(40 + 3*t2 - 109*Power(t2,2) + 15*Power(t2,3) - 
               4*Power(t2,4) + Power(s,2)*(1 + 5*t2) + 
               Power(s1,2)*(40 - 4*t2 - 6*Power(t2,2)) + 
               s1*(-22 - 41*t2 - 73*Power(t2,2) + 11*Power(t2,3)) - 
               s*(8 + 61*t2 - 35*Power(t2,2) - 7*Power(t2,3) + 
                  s1*(9 + 8*t2 + 3*Power(t2,2)))) + 
            t1*(-6 + 28*t2 + Power(s,2)*(3 - 11*t2)*t2 + 
               20*Power(t2,2) + 85*Power(t2,3) - 7*Power(t2,4) + 
               Power(s1,2)*(3 + 21*t2 - 4*Power(t2,2)) + 
               s1*(16 - 93*t2 - 41*Power(t2,2) + 18*Power(t2,3)) + 
               s*(2 + 33*t2 + 3*Power(t2,2) - 5*Power(t2,3) - 
                  4*Power(t2,4) + 
                  s1*(-13 - 35*t2 + Power(t2,2) + 5*Power(t2,3))))) + 
         Power(s2,3)*(Power(t1,5)*
             (-4 - 2*Power(s,2) - 4*Power(s1,2) + 6*s*(1 + s1) + 2*t2 + 
               3*s1*t2 + Power(t2,2)) + 
            2*t2*(-1 + (2 + 2*Power(s,2) + s*(-7 + 2*s1))*t2 + 
               (-1 - 2*s*(-2 + s1))*Power(t2,2) + s*(3 + s)*Power(t2,3)) \
+ Power(t1,4)*(7 - 2*Power(s1,2)*(-1 + t2) + 57*t2 + 10*Power(t2,2) - 
               4*Power(t2,3) + 2*Power(s,2)*(3 + t2) + 
               s*(65 + s1*(-4 + t2) + 48*t2) + 
               s1*(-128 - 47*t2 + 5*Power(t2,2))) + 
            t1*(Power(s,2)*t2*
                (-9 + 9*t2 - 12*Power(t2,2) + 2*Power(t2,3)) + 
               s*(7 + 3*(-18 + 7*s1)*t2 + 4*(-6 + s1)*Power(t2,2) - 
                  (52 + s1)*Power(t2,3) + 3*Power(t2,4)) + 
               (-1 + t2)*(-2 - 39*t2 + 12*Power(t2,2) + 21*Power(t2,3) + 
                  s1*(3 + 16*t2 - 11*Power(t2,2)))) + 
            Power(t1,3)*(-63 + 109*t2 + 89*Power(t2,2) - 
               17*Power(t2,3) + 2*Power(t2,4) + Power(s,2)*(-1 + 3*t2) + 
               4*Power(s1,2)*(-10 + 4*t2 + Power(t2,2)) + 
               s1*(25 + 79*t2 + 41*Power(t2,2) - 5*Power(t2,3)) - 
               s*(-47 - 55*t2 + 73*Power(t2,2) + 11*Power(t2,3) + 
                  s1*(-23 + 2*t2 + Power(t2,2)))) + 
            Power(t1,2)*(-6 - 37*t2 - 107*Power(t2,2) - 101*Power(t2,3) + 
               11*Power(t2,4) + 
               2*Power(s1,2)*(-6 - 17*t2 + 3*Power(t2,2)) + 
               s1*(8 + 186*t2 + 30*Power(t2,2) - 24*Power(t2,3)) + 
               Power(s,2)*(-7 + 16*t2 + 9*Power(t2,2) - 6*Power(t2,3)) + 
               s*(s1*(55 + 53*t2 - 17*Power(t2,2) - 3*Power(t2,3)) + 
                  3*(-6 - 13*t2 + 6*Power(t2,2) + 9*Power(t2,3) + 
                     2*Power(t2,4))))))*T3q(s2,t1))/
     ((-1 + s2)*Power(s2 - t1,2)*(-s + s2 - t1)*(-1 + t1)*
       (-1 + s2 - t1 + t2)*Power(-t1 + s2*t2,2)) - 
    (8*((8 - 6*s + 6*s1)*Power(t1,4) + 2*t2*Power(1 + (-1 + s)*t2,2) + 
         Power(s2,7)*(Power(s1,2) - 2*s1*t2 - Power(t2,2)) + 
         Power(t1,3)*(15 - 2*Power(s,2) - 8*t2 - s1*(7 + 10*t2) + 
            s*(23 + 2*s1 + 10*t2)) + 
         Power(t1,2)*(-20 + Power(s1,2)*(-5 + t2) - 29*t2 + 
            5*Power(t2,2) + Power(s,2)*(7 + 3*t2) + 
            s*(12 + s1*(6 - 8*t2) - 18*t2 - 7*Power(t2,2)) + 
            s1*(8 + 17*t2 + 3*Power(t2,2))) - 
         t1*(Power(s,2)*t2*(7 + 3*t2) + 
            s*(7 + (3 + s1)*t2 - (1 + 3*s1)*Power(t2,2) - 
               3*Power(t2,3)) + 
            (-1 + t2)*(-11 - 6*t2 + 3*Power(t2,2) + 3*s1*(1 + t2))) + 
         Power(s2,6)*(-(Power(s1,2)*(4 + t1 - 2*t2)) + 
            t2*(2 + s*(-2 + t2) + 7*t2 - Power(t2,2) + 2*t1*(1 + t2)) + 
            s1*(-2 + (15 + s)*t2 - 3*Power(t2,2) + t1*(2 + 2*s + 3*t2))) \
- Power(s2,5)*(-1 + 15*t2 - 9*s*t2 + 17*Power(t2,2) + 8*s*Power(t2,2) - 
            11*Power(t2,3) - s*Power(t2,3) + 
            Power(s1,2)*(-2 + Power(t1,2) + 2*t1*(-2 + t2) + 6*t2 - 
               Power(t2,2)) - 
            Power(t1,2)*(-1 + Power(s,2) - 4*t2 + 3*s*t2 + 
               Power(t2,2)) + 
            t1*(2 + (17 - 3*s + Power(s,2))*t2 + 
               (7 + 3*s)*Power(t2,2)) + 
            s1*(-8 + 3*Power(t1,2) + (30 + 4*s)*t2 - 
               2*(11 + s)*Power(t2,2) + Power(t2,3) + 
               t1*(13 + s*(11 - 4*t2) + 26*t2 - 5*Power(t2,2)))) - 
         Power(s2,2)*(-4 + 10*Power(t1,4) + 30*t2 - 2*s*t2 + 
            14*Power(t2,2) + 5*s*Power(t2,2) + 
            8*Power(s,2)*Power(t2,2) - Power(t2,3) + 34*s*Power(t2,3) - 
            2*Power(s,2)*Power(t2,3) + 5*Power(t2,4) - 6*s*Power(t2,4) + 
            Power(t1,3)*(-29 - Power(s,2) + 32*t2 + s*(44 + 7*t2)) + 
            Power(s1,2)*(Power(t1,3) - 13*Power(t1,2)*(1 + t2) + 
               t2*(-5 + 9*t2) + t1*(-15 + 18*t2 + Power(t2,2))) + 
            2*Power(t1,2)*(-42 + 3*Power(s,2)*(-1 + t2) - 32*t2 - 
               35*Power(t2,2) - s*(9 + 47*t2 + 4*Power(t2,2))) + 
            t1*(1 + 9*t2 + 80*Power(t2,2) + 21*Power(t2,3) - 
               Power(s,2)*t2*(2 + 3*t2) + 
               s*(2 - 82*t2 + 20*Power(t2,2) + 7*Power(t2,3))) + 
            s1*(-6 + (2 + 7*s)*t2 + (3 - 26*s)*Power(t2,2) + 
               (-27 + 7*s)*Power(t2,3) - Power(t1,3)*(13 + 9*t2) + 
               Power(t1,2)*(19 + 126*t2 + 9*Power(t2,2) + 
                  s*(36 + 7*t2)) + 
               t1*(3 + 44*t2 - 60*Power(t2,2) + 
                  s*(26 - 4*t2 - 8*Power(t2,2))))) + 
         Power(s2,4)*(-4 + 28*t2 - 8*s*t2 + 17*Power(t2,2) + 
            42*s*Power(t2,2) - 8*Power(t2,3) - 8*s*Power(t2,3) + 
            5*Power(t2,4) + (-2 + s)*Power(t1,3)*(-1 + s + t2) + 
            t1*(14 + s + 66*t2 - 11*s*t2 + 5*Power(s,2)*t2 - 
               11*Power(t2,2) + 23*s*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s1,2)*(4 + 2*Power(t1,2) + Power(t1,3) + 11*t2 - 
               Power(t2,2) + t1*(4 + 2*t2 - Power(t2,2))) - 
            Power(t1,2)*(-10 - 17*t2 + 9*Power(t2,2) + 
               Power(s,2)*(5 + t2) + s*(4 + 18*t2 + Power(t2,2))) + 
            s1*(-4 + (7 - 2*s)*t2 - 2*(31 + 4*s)*Power(t2,2) + 
               (7 + s)*Power(t2,3) - Power(t1,3)*(2*s + t2) + 
               Power(t1,2)*(31 + (7 + s)*t2) + 
               t1*(18 + 25*t2 - 35*Power(t2,2) + 2*Power(t2,3) + 
                  s*(16 - 15*t2 + Power(t2,2))))) + 
         s2*(-3 + (17 + 7*s - 7*s1)*Power(t1,4) + (14 + s - 7*s1)*t2 + 
            (-2 + 4*Power(s,2) + 8*s1 - s*(1 + 3*s1))*Power(t2,2) - 
            (12 + 4*Power(s,2) + s1 - s*(9 + s1))*Power(t2,3) - 
            3*(-1 + s)*Power(t2,4) + 
            Power(t1,3)*(-61 + 3*Power(s,2) - 6*Power(s1,2) + 
               s*(-7 + 3*s1 - 5*t2) - 41*t2 + 2*s1*(20 + t2)) - 
            Power(t1,2)*(15 + 2*Power(s1,2)*(-2 + t2) - 44*t2 - 
               16*Power(t2,2) + Power(s,2)*(15 + 2*t2) + 
               s*(41 + s1*(-21 + t2) + 51*t2) + 
               s1*(-14 + 5*t2 - 10*Power(t2,2))) + 
            t1*(22 + 38*t2 + 28*Power(t2,2) + 
               Power(s,2)*t2*(11 + 3*t2) + 
               Power(s1,2)*(-3 + 10*t2 + Power(t2,2)) - 
               s1*(4 + 11*t2 + 38*Power(t2,2) + 3*Power(t2,3)) + 
               s*(12 - 15*t2 + 46*Power(t2,2) + Power(t2,3) + 
                  s1*(13 - 28*t2 + 5*Power(t2,2))))) + 
         Power(s2,3)*(2 - (-1 + s)*Power(t1,4) - t2 - 2*s*t2 + 
            22*Power(t2,2) - 33*s*Power(t2,2) + 
            4*Power(s,2)*Power(t2,2) + 23*Power(t2,3) + 
            44*s*Power(t2,3) + 13*Power(t2,4) - 3*s*Power(t2,4) + 
            Power(t1,3)*(-9 - 3*Power(s,2) + 19*t2 + s*(23 + t2)) + 
            Power(t1,2)*(-50 - 28*t2 + 13*Power(t2,2) + 
               6*Power(s,2)*(1 + t2) + s*(15 + 14*t2)) - 
            Power(s1,2)*(3 + 2*Power(t1,3) + Power(t1,2)*(5 - 4*t2) + 
               12*t2 - 17*Power(t2,2) + t1*(19 + 8*t2 + 7*Power(t2,2))) - 
            t1*(22 + 101*t2 - 3*Power(t2,2) + 42*Power(t2,3) + 
               Power(s,2)*t2*(10 + 3*t2) + 
               s*(4 + 56*t2 + 71*Power(t2,2) - 3*Power(t2,3))) + 
            s1*(-8 + (-14 + 5*s)*Power(t1,3) + Power(t1,4) + 
               (19 + 12*s)*t2 + (22 - 17*s)*Power(t2,2) - 
               3*(16 + s)*Power(t2,3) + 
               Power(t1,2)*(-47 - 9*s*(-1 + t2) + 27*t2 - 
                  4*Power(t2,2)) + 
               t1*(-3 + 85*t2 + 75*Power(t2,2) + Power(t2,3) + 
                  s*(6 + 36*t2 + 7*Power(t2,2))))))*T4q(-1 + s2))/
     (Power(-1 + s2,2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s2 - t1 + t2)*
       Power(-t1 + s2*t2,2)) + 
    (8*(2*(2 + s - s1)*Power(t1,5) + 
         2*(-1 + t2)*t2*Power(1 + (-1 + s)*t2,2) + 
         Power(s2,6)*(Power(s1,2) - 2*s1*t2 - Power(t2,2)) + 
         Power(t1,4)*(3 + 2*Power(s,2) + 2*Power(s1,2) + 
            s*(9 - 4*s1 - 8*t2) + 4*t2 + s1*(-3 + 4*t2)) + 
         Power(t1,3)*(-11 + Power(s,2)*(1 - 5*t2) + 24*t2 - 
            13*Power(t2,2) - Power(s1,2)*(1 + 3*t2) + 
            s1*(9 + 4*t2 - 5*Power(t2,2)) + 
            s*(17 + (-9 + 6*s1)*t2 + 13*Power(t2,2))) + 
         t1*(Power(s,2)*t2*(7 - 4*t2 - 5*Power(t2,2)) + 
            s*(-1 + t2)*(-7 - (3 + s1)*t2 + (5 + 3*s1)*Power(t2,2) + 
               3*Power(t2,3)) - 
            Power(-1 + t2,2)*(-11 - 4*t2 + 3*Power(t2,2) + 3*s1*(1 + t2))) \
+ Power(s2,5)*(Power(s1,2)*(-3 - 2*t1 + 3*t2) + 
            t2*(2 + s*(-2 + t2) + 2*t2 - 2*Power(t2,2) + t1*(2 + 3*t2)) + 
            s1*(-2 + (13 + s)*t2 - 5*Power(t2,2) + t1*(2 + 2*s + 5*t2))) - 
         Power(s2,4)*(-1 + 13*t2 - 7*s*t2 - 7*Power(t2,2) + 
            5*s*Power(t2,2) - 11*Power(t2,3) - 2*s*Power(t2,3) + 
            Power(t2,4) + Power(s1,2)*
             (1 + 5*t1*(-1 + t2) + 6*t2 - 3*Power(t2,2)) + 
            Power(t1,2)*(1 - Power(s,2) + 6*t2 - 3*s*t2 + Power(t2,2)) + 
            t1*(2 + (9 - 5*s + Power(s,2))*t2 + 4*(1 + s)*Power(t2,2) - 
               3*Power(t2,3)) + 
            s1*(-6 + (19 + 3*s)*t2 - (22 + 3*s)*Power(t2,2) + 
               4*Power(t2,3) + Power(t1,2)*(5 + 2*s + 3*t2) + 
               t1*(9 + s*(9 - 5*t2) + 32*t2 - 11*Power(t2,2)))) + 
         Power(t1,2)*(Power(s,2)*(-3 + 5*t2 + 6*Power(t2,2)) + 
            s*(-11 + 5*t2 + 2*Power(t2,2) - 10*Power(t2,3) + 
               s1*(18 - 7*t2 - 5*Power(t2,2))) + 
            (-1 + t2)*(-17 - 13*t2 + 8*Power(t2,2) + 
               Power(s1,2)*(3 + t2) + s1*(-7 - 2*t2 + 3*Power(t2,2)))) + 
         Power(s2,2)*(-1 - Power(t1,4)*
             (1 + Power(s,2) + s*(-2 + t2) - 2*t2) + 12*t2 - 3*s*t2 + 
            10*Power(t2,2) + 8*Power(s,2)*Power(t2,2) - 29*Power(t2,3) + 
            6*s*Power(t2,3) - 6*Power(s,2)*Power(t2,3) + 7*Power(t2,4) - 
            17*s*Power(t2,4) + Power(t2,5) + 
            Power(t1,3)*(-11 + 2*t2 + Power(t2,2) + 
               Power(s,2)*(1 + 2*t2) + s*(24 + 11*t2 + 2*Power(t2,2))) - 
            Power(s1,2)*(Power(t1,4) - Power(t1,3)*(-1 + t2) + 
               t2*(5 - 6*t2 + Power(t2,2)) - 
               Power(t1,2)*(-13 + 6*t2 + Power(t2,2)) + 
               t1*(12 - Power(t2,2) + Power(t2,3))) - 
            Power(t1,2)*(35 + t2 - 69*Power(t2,2) - 3*Power(t2,3) + 
               Power(s,2)*(-2 + t2 + Power(t2,2)) + 
               s*(-14 - 16*t2 + 34*Power(t2,2) + Power(t2,3))) + 
            t1*(-8 + 20*t2 + 27*Power(t2,2) - 32*Power(t2,3) - 
               7*Power(t2,4) + 2*Power(s,2)*t2*(1 + 3*t2) + 
               s*(-3 - 11*t2 + 3*Power(t2,2) + 38*Power(t2,3))) + 
            s1*(-6 + (15 + 7*s)*t2 - 3*(1 + 2*s)*Power(t2,2) + 
               (-9 + 4*s)*Power(t2,3) + (3 + s)*Power(t2,4) + 
               Power(t1,4)*(1 + 2*s + t2) - 
               Power(t1,3)*(39 + 3*s*(-1 + t2) + 8*t2 + Power(t2,2)) + 
               Power(t1,2)*(-25 - s*(-9 + t2) + 29*t2 + 16*Power(t2,2) - 
                  2*Power(t2,3)) + 
               t1*(-2 + 33*t2 + 12*Power(t2,2) - 21*Power(t2,3) + 
                  2*Power(t2,4) + s*(13 + 20*t2 - 19*Power(t2,2))))) + 
         Power(s2,3)*(-3 + 16*t2 - s*t2 - 37*Power(t2,2) + 
            12*s*Power(t2,2) - 4*Power(s,2)*Power(t2,2) + 
            14*Power(t2,3) - 15*s*Power(t2,3) + 10*Power(t2,4) + 
            s*Power(t2,4) - Power(t1,3)*
             (-3 - 2*t2 + Power(t2,2) + s*(3 + 2*t2)) + 
            Power(s1,2)*(3 + 2*Power(t1,3) + Power(t1,2)*(-1 + t2) + 
               4*t2 - 4*Power(t2,2) + Power(t2,3) + 
               t1*(10 + 4*t2 - 4*Power(t2,2))) + 
            Power(t1,2)*(7 + Power(s,2)*(-4 + t2) + 17*t2 - 
               5*Power(t2,2) + Power(t2,3) + 
               s*(-4 - 18*t2 + 5*Power(t2,2))) + 
            t1*(11 + 28*t2 - Power(s,2)*(-4 + t2)*t2 - 39*Power(t2,2) - 
               22*Power(t2,3) + 
               s*(1 - 21*t2 + 21*Power(t2,2) - 4*Power(t2,3))) - 
            s1*(-2 + (6 + 5*s)*t2 + (15 + 2*s)*Power(t2,2) - 
               3*(4 + s)*Power(t2,3) + Power(t2,4) + 
               Power(t1,3)*(-3 + 2*s + t2) + 
               Power(t1,2)*(-37 - 24*t2 + 5*Power(t2,2) + 
                  s*(-7 + 3*t2)) - 
               t1*(3 + 15*t2 - 42*Power(t2,2) + 8*Power(t2,3) + 
                  s*(7 - 16*t2 + 3*Power(t2,2))))) + 
         s2*(3 + (-1 + s - s1)*Power(t1,5) - (19 + s - 7*s1)*t2 + 
            (24 - 4*Power(s,2) - 15*s1 + s*(-2 + 3*s1))*Power(t2,2) + 
            (6*Power(s,2) + s*(2 - 4*s1) + 9*s1)*Power(t2,3) + 
            (-11 - s1 + s*(4 + s1))*Power(t2,4) - 3*(-1 + s)*Power(t2,5) - 
            Power(t1,3)*(-31 + 50*t2 + 5*Power(s,2)*t2 + 7*Power(t2,2) + 
               Power(s1,2)*(-2 + 4*t2) + 
               s*(39 + s1*(6 - 8*t2) - 33*t2 - 9*Power(t2,2)) + 
               s1*(-17 + 23*t2 + 2*Power(t2,2))) + 
            Power(t1,4)*(Power(s,2) - s*(18 + s1 + 4*t2) + 
               3*(-2*t2 + s1*(5 + t2))) + 
            Power(t1,2)*(11 - 30*t2 - Power(t2,2) + 20*Power(t2,3) + 
               Power(s1,2)*(10 - t2 + 3*Power(t2,2)) + 
               Power(s,2)*(4 - 11*t2 + 7*Power(t2,2)) + 
               s1*(-6 - 41*t2 + 20*Power(t2,2) + 3*Power(t2,3)) - 
               s*(-3 + t2 + 34*Power(t2,2) + 13*Power(t2,3) + 
                  s1*(34 - 24*t2 + 6*Power(t2,2)))) + 
            t1*(Power(s,2)*t2*(-12 + 5*t2 - 3*Power(t2,2)) + 
               (-1 + t2)*(8 + 41*t2 + 4*Power(t2,2) - 9*Power(t2,3) + 
                  Power(s1,2)*(-3 - 6*t2 + Power(t2,2)) + 
                  s1*(-1 + 15*t2 + Power(t2,2) - 3*Power(t2,3))) + 
               s*(-5 + 27*t2 - 19*Power(t2,2) + 15*Power(t2,3) + 
                  10*Power(t2,4) - 
                  s1*(13 + 6*t2 - 9*Power(t2,2) + 2*Power(t2,3))))))*
       T5q(1 - s2 + t1 - t2))/
     ((-1 + s2)*(-s + s2 - t1)*(-1 + t1)*(-1 + s2 - t1 + t2)*
       Power(-t1 + s2*t2,2)));
   return a;
};
