#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_2_m132_1_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(8*Power(s2,2) + 8*Power(s2,3) - 16*s2*t1 - 40*Power(s2,2)*t1 - 
         16*Power(s2,3)*t1 + 8*Power(t1,2) + 56*s2*Power(t1,2) + 
         56*Power(s2,2)*Power(t1,2) + 8*Power(s2,3)*Power(t1,2) - 
         24*Power(t1,3) - 64*s2*Power(t1,3) - 24*Power(s2,2)*Power(t1,3) + 
         24*Power(t1,4) + 24*s2*Power(t1,4) - 8*Power(t1,5) - 
         2*Power(s1,6)*(3 + 3*s2 + Power(s2,2) - t1 - Power(t1,2)) + 
         24*s2*t2 + 5*Power(s2,2)*t2 - 16*Power(s2,3)*t2 - 
         3*Power(s2,4)*t2 - 4*Power(s2,5)*t2 - 24*t1*t2 - 62*s2*t1*t2 - 
         3*Power(s2,2)*t1*t2 + 6*Power(s2,3)*t1*t2 + 
         15*Power(s2,4)*t1*t2 + 57*Power(t1,2)*t2 + 82*s2*Power(t1,2)*t2 + 
         14*Power(s2,2)*Power(t1,2)*t2 - 34*Power(s2,3)*Power(t1,2)*t2 - 
         63*Power(t1,3)*t2 - 34*s2*Power(t1,3)*t2 + 
         52*Power(s2,2)*Power(t1,3)*t2 + 17*Power(t1,4)*t2 - 
         42*s2*Power(t1,4)*t2 + 13*Power(t1,5)*t2 + 40*Power(t2,2) - 
         30*s2*Power(t2,2) - 51*Power(s2,2)*Power(t2,2) + 
         16*Power(s2,3)*Power(t2,2) + 4*Power(s2,4)*Power(t2,2) - 
         Power(s2,5)*Power(t2,2) - 30*t1*Power(t2,2) + 
         88*s2*t1*Power(t2,2) + 7*Power(s2,2)*t1*Power(t2,2) - 
         Power(s2,3)*t1*Power(t2,2) + Power(s2,4)*t1*Power(t2,2) - 
         17*Power(t1,2)*Power(t2,2) - 86*s2*Power(t1,2)*Power(t2,2) - 
         65*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         6*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         63*Power(t1,3)*Power(t2,2) + 117*s2*Power(t1,3)*Power(t2,2) - 
         14*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         55*Power(t1,4)*Power(t2,2) + 11*s2*Power(t1,4)*Power(t2,2) - 
         3*Power(t1,5)*Power(t2,2) - 63*Power(t2,3) - 28*s2*Power(t2,3) + 
         54*Power(s2,2)*Power(t2,3) + 8*Power(s2,3)*Power(t2,3) - 
         12*Power(s2,4)*Power(t2,3) + 5*Power(s2,5)*Power(t2,3) + 
         109*t1*Power(t2,3) - 14*s2*t1*Power(t2,3) - 
         10*Power(s2,2)*t1*Power(t2,3) + 44*Power(s2,3)*t1*Power(t2,3) - 
         14*Power(s2,4)*t1*Power(t2,3) - 70*Power(t1,2)*Power(t2,3) - 
         30*s2*Power(t1,2)*Power(t2,3) - 
         50*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         12*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         32*Power(t1,3)*Power(t2,3) + 16*s2*Power(t1,3)*Power(t2,3) - 
         2*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         2*Power(t1,4)*Power(t2,3) - s2*Power(t1,4)*Power(t2,3) - 
         5*Power(t2,4) + 40*s2*Power(t2,4) + Power(s2,2)*Power(t2,4) - 
         31*Power(s2,3)*Power(t2,4) + 7*Power(s2,4)*Power(t2,4) - 
         55*t1*Power(t2,4) - 21*s2*t1*Power(t2,4) + 
         98*Power(s2,2)*t1*Power(t2,4) - 9*Power(s2,3)*t1*Power(t2,4) + 
         24*Power(t1,2)*Power(t2,4) - 87*s2*Power(t1,2)*Power(t2,4) - 
         3*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         20*Power(t1,3)*Power(t2,4) + 5*s2*Power(t1,3)*Power(t2,4) + 
         41*Power(t2,5) - 4*s2*Power(t2,5) - 38*Power(s2,2)*Power(t2,5) - 
         Power(s2,3)*Power(t2,5) + 3*t1*Power(t2,5) + 
         78*s2*t1*Power(t2,5) + 8*Power(s2,2)*t1*Power(t2,5) - 
         34*Power(t1,2)*Power(t2,5) - 7*s2*Power(t1,2)*Power(t2,5) - 
         17*Power(t2,6) - 18*s2*Power(t2,6) - 3*Power(s2,2)*Power(t2,6) + 
         15*t1*Power(t2,6) + 3*s2*t1*Power(t2,6) + 
         4*Power(s,5)*(-3 + 2*Power(t1,2) + Power(t1,3) + 
            Power(s2,2)*(1 + t1 - 2*t2) + Power(s1,2)*(-2 + 3*s2 - t2) - 
            4*t1*t2 - 4*Power(t1,2)*t2 + 2*Power(t2,2) + 
            5*t1*Power(t2,2) - 2*Power(t2,3) + 
            s2*(3 - 3*t1 - 2*Power(t1,2) + 3*t2 + 6*t1*t2 - 
               4*Power(t2,2)) + 
            s1*(6 + 3*t1 + Power(t1,2) - s2*(8 + t1 - 3*t2) - 3*t2 - 
               4*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,5)*(-56 - 7*Power(s2,3) + 9*Power(t1,3) - 
            s2*(19 + 32*t1 + 21*Power(t1,2) - 45*t2) + t1*(31 - 23*t2) + 
            Power(t1,2)*(6 - 11*t2) + 46*t2 + 
            Power(s2,2)*(19*t1 + 11*(2 + t2))) + 
         Power(s1,4)*(-71 + 6*Power(s2,4) + 8*Power(t1,4) + 
            Power(t1,3)*(26 - 34*t2) + 268*t2 - 141*Power(t2,2) + 
            Power(s2,3)*(-31 - 18*t1 + 21*t2) + 
            Power(s2,2)*(101 + 26*Power(t1,2) + t1*(78 - 50*t2) - 
               156*t2 - 27*Power(t2,2)) + 
            Power(t1,2)*(29 - 73*t2 + 24*Power(t2,2)) + 
            t1*(-8 - 111*t2 + 87*Power(t2,2)) + 
            s2*(73 - 22*Power(t1,3) + 51*t2 - 138*Power(t2,2) + 
               Power(t1,2)*(-73 + 63*t2) + 
               t1*(-148 + 255*t2 + 3*Power(t2,2)))) + 
         Power(s1,3)*(15 - 9*Power(s2,5) + Power(t1,5) + 
            Power(s2,4)*(8 + 33*t1 - 29*t2) + Power(t1,4)*(40 - 21*t2) + 
            226*t2 - 509*Power(t2,2) + 224*Power(t2,3) - 
            Power(s2,3)*(33 + 46*Power(t1,2) + t1*(44 - 68*t2) - 
               125*t2 + 20*Power(t2,2)) + 
            2*Power(t1,3)*(-35 - 62*t2 + 24*Power(t2,2)) + 
            t1*(-93 + 61*t2 + 144*Power(t2,2) - 158*Power(t2,3)) + 
            Power(t1,2)*(97 - 78*t2 + 217*Power(t2,2) - 26*Power(t2,3)) + 
            Power(s2,2)*(25 + 30*Power(t1,3) + 
               Power(t1,2)*(104 - 70*t2) - 301*t2 + 374*Power(t2,2) + 
               38*Power(t2,3) + t1*(26 - 360*t2 + 28*Power(t2,2))) - 
            s2*(-84 + 9*Power(t1,4) + Power(t1,3)*(108 - 52*t2) + 
               253*t2 + 35*Power(t2,2) - 222*Power(t2,3) + 
               Power(t1,2)*(-77 - 359*t2 + 56*Power(t2,2)) + 
               t1*(116 - 433*t2 + 651*Power(t2,2) + 12*Power(t2,3)))) + 
         Power(s1,2)*(40 - 2*Power(t1,5)*(-3 + t2) - 93*t2 - 
            244*Power(t2,2) + 479*Power(t2,3) - 196*Power(t2,4) + 
            Power(s2,5)*(-5 + 23*t2) + 
            Power(t1,4)*(-43 - 75*t2 + 18*Power(t2,2)) + 
            Power(s2,4)*(1 + t1*(20 - 80*t2) - 32*t2 + 47*Power(t2,2)) + 
            Power(t1,3)*(40 + 146*t2 + 190*Power(t2,2) - 
               30*Power(t2,3)) + 
            Power(t1,2)*(-7 - 225*t2 + 93*Power(t2,2) - 
               273*Power(t2,3) + 14*Power(t2,4)) + 
            t1*(-38 + 279*t2 - 153*Power(t2,2) - 76*Power(t2,3) + 
               152*Power(t2,4)) + 
            Power(s2,3)*(-21 + 81*t2 - 188*Power(t2,2) + 4*Power(t2,3) + 
               4*Power(t1,2)*(-9 + 26*t2) + 
               t1*(22 + 137*t2 - 91*Power(t2,2))) + 
            Power(s2,2)*(-81 + Power(t1,3)*(38 - 62*t2) + 31*t2 + 
               300*Power(t2,2) - 406*Power(t2,3) - 32*Power(t2,4) + 
               Power(t1,2)*(-90 - 253*t2 + 59*Power(t2,2)) + 
               2*t1*(45 - 53*t2 + 292*Power(t2,2) + 13*Power(t2,3))) + 
            s2*(-22 - 180*t2 + 327*Power(t2,2) - 11*Power(t2,3) - 
               198*Power(t2,4) + Power(t1,4)*(-23 + 17*t2) + 
               Power(t1,3)*(110 + 223*t2 - 33*Power(t2,2)) - 
               Power(t1,2)*(109 + 121*t2 + 586*Power(t2,2)) + 
               t1*(108 + 152*t2 - 443*Power(t2,2) + 743*Power(t2,3) + 
                  18*Power(t2,4)))) + 
         s1*(Power(s2,5)*(4 + 6*t2 - 19*Power(t2,2)) + 
            Power(t1,5)*(-5 - 3*t2 + Power(t2,2)) + 
            Power(t1,4)*(-41 + 98*t2 + 33*Power(t2,2) - 5*Power(t2,3)) + 
            Power(t1,3)*(87 - 103*t2 - 108*Power(t2,2) - 
               112*Power(t2,3) + 7*Power(t2,4)) + 
            t2*(-80 + 141*t2 + 94*Power(t2,2) - 223*Power(t2,3) + 
               90*Power(t2,4)) + 
            t1*(24 + 68*t2 - 295*Power(t2,2) + 155*Power(t2,3) + 
               9*Power(t2,4) - 75*Power(t2,5)) + 
            Power(t1,2)*(-65 + 24*t2 + 198*Power(t2,2) - 
               68*Power(t2,3) + 157*Power(t2,4) - 3*Power(t2,5)) + 
            Power(s2,4)*(3 - 5*t2 + 36*Power(t2,2) - 31*Power(t2,3) + 
               t1*(-15 - 21*t2 + 61*Power(t2,2))) + 
            Power(s2,3)*(8 + 5*t2 - 56*Power(t2,2) + 125*Power(t2,3) + 
               3*Power(t2,4) + 
               Power(t1,2)*(26 + 30*t2 - 70*Power(t2,2)) + 
               t1*(10 - 21*t2 - 137*Power(t2,2) + 50*Power(t2,3))) + 
            Power(s2,2)*(-13 + 132*t2 - 110*Power(t2,2) - 
               101*Power(t2,3) + 204*Power(t2,4) + 15*Power(t2,5) + 
               Power(t1,3)*(-28 - 24*t2 + 34*Power(t2,2)) + 
               Power(t1,2)*(-70 + 155*t2 + 199*Power(t2,2) - 
                  12*Power(t2,3)) + 
               t1*(43 - 97*t2 + 90*Power(t2,2) - 400*Power(t2,3) - 
                  31*Power(t2,4))) + 
            s2*(-24 + 52*t2 + 124*Power(t2,2) - 187*Power(t2,3) + 
               18*Power(t2,4) + 93*Power(t2,5) + 
               Power(t1,4)*(18 + 12*t2 - 7*Power(t2,2)) - 
               Power(t1,3)*(-98 + 227*t2 + 131*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,2)*(-138 + 195*t2 + 74*Power(t2,2) + 
                  387*Power(t2,3) + 21*Power(t2,4)) - 
               t1*(-78 + 196*t2 + 22*Power(t2,2) - 179*Power(t2,3) + 
                  393*Power(t2,4) + 12*Power(t2,5)))) - 
         Power(s,4)*(-48 - 6*t1 + 46*Power(t1,2) - 21*Power(t1,3) - 
            3*Power(t1,4) + Power(s2,3)*(2 + 9*t1 - 21*t2) + 66*t2 - 
            84*t1*t2 + 15*Power(t1,2)*t2 + 6*Power(t1,3)*t2 + 
            38*Power(t2,2) + 33*t1*Power(t2,2) + 
            16*Power(t1,2)*Power(t2,2) - 27*Power(t2,3) - 
            38*t1*Power(t2,3) + 19*Power(t2,4) + 
            2*Power(s1,3)*(20*s2 + t1 - 5*(2 + t2)) + 
            Power(s1,2)*(58 + 10*Power(s2,2) + 21*Power(t1,2) + 
               t1*(13 - 44*t2) + 11*t2 + 39*Power(t2,2) - 
               3*s2*(32 + 7*t1 + 9*t2)) + 
            Power(s2,2)*(4 - 21*Power(t1,2) + t2 - 23*Power(t2,2) + 
               3*t1*(-7 + 20*t2)) + 
            s2*(54 + 15*Power(t1,3) - 18*t2 - 28*Power(t2,2) + 
               17*Power(t2,3) - 5*Power(t1,2)*(-8 + 9*t2) + 
               t1*(-50 - 12*t2 + 13*Power(t2,2))) + 
            2*s1*(5*Power(s2,3) + 8*Power(t1,3) - 
               24*Power(t1,2)*(1 + t2) + 
               Power(s2,2)*(-9 - 10*t1 + 14*t2) + 
               2*t1*(8 + 3*t2 + 20*Power(t2,2)) + 
               3*(5 - 24*t2 + 6*Power(t2,2) - 8*Power(t2,3)) + 
               s2*(-13 - 3*Power(t1,2) + 57*t2 - 15*Power(t2,2) + 
                  t1*(31 + 2*t2)))) + 
         Power(s,3)*(-60 - 10*t1 + 105*Power(t1,2) - 122*Power(t1,3) + 
            24*Power(t1,4) - Power(t1,5) + 
            Power(s2,4)*(-6 + 5*t1 - 18*t2) + 
            2*Power(s1,4)*(-6 + 23*s2 + 3*t1 - 4*t2) + 202*t2 - 
            170*t1*t2 + 144*Power(t1,2)*t2 - 11*Power(t1,3)*t2 + 
            8*Power(t1,4)*t2 - 55*Power(t2,2) + 46*t1*Power(t2,2) - 
            7*Power(t1,2)*Power(t2,2) - 28*Power(t1,3)*Power(t2,2) - 
            68*Power(t2,3) - 49*t1*Power(t2,3) + 
            22*Power(t1,2)*Power(t2,3) + 43*Power(t2,4) + 
            13*t1*Power(t2,4) - 14*Power(t2,5) + 
            Power(s2,3)*(-5 - 14*Power(t1,2) - 15*t2 + 12*Power(t2,2) + 
               t1*(4 + 40*t2)) + 
            Power(s1,3)*(76 + 43*Power(s2,2) + 37*Power(t1,2) + 
               t1*(11 - 49*t2) + 24*t2 + 38*Power(t2,2) - 
               s2*(111 + 56*t1 + 107*t2)) + 
            Power(s2,2)*(23 + 12*Power(t1,3) + 
               Power(t1,2)*(34 - 18*t2) + 54*t2 - 25*Power(t2,2) + 
               64*Power(t2,3) + t1*(-92 + 23*t2 - 82*Power(t2,2))) + 
            Power(s1,2)*(145 + 12*Power(s2,3) + 15*Power(t1,3) - 
               278*t2 + 31*Power(t2,2) - 66*Power(t2,3) - 
               6*Power(t1,2)*(9 + 11*t2) + 
               Power(s2,2)*(-58 - 39*t1 + 30*t2) + 
               t1*(24 - t2 + 93*Power(t2,2)) + 
               s2*(-195 + 12*Power(t1,2) + t1*(130 - 34*t2) + 293*t2 + 
                  96*Power(t2,2))) + 
            s2*(70 - 2*Power(t1,4) - 104*t2 + 111*Power(t2,2) + 
               27*Power(t2,3) + 20*Power(t2,4) - 
               4*Power(t1,3)*(14 + 3*t2) + 
               Power(t1,2)*(219 + 3*t2 + 98*Power(t2,2)) - 
               2*t1*(64 + 89*t2 - 13*Power(t2,2) + 52*Power(t2,3))) + 
            s1*(-82 + 19*Power(s2,4) - 17*Power(t1,4) + 
               Power(s2,3)*(19 - 40*t1 - 13*t2) - 150*t2 + 
               270*Power(t2,2) - 86*Power(t2,3) + 50*Power(t2,4) + 
               Power(t1,3)*(67 + 23*t2) + 
               Power(s2,2)*(-32 + t1 + 6*Power(t1,2) + 80*t2 + 
                  113*t1*t2 - 137*Power(t2,2)) + 
               Power(t1,2)*(-126 - 20*t2 + 7*Power(t2,2)) + 
               t1*(46 + 64*t2 + 39*Power(t2,2) - 63*Power(t2,3)) + 
               s2*(108 + 32*Power(t1,3) + 10*t2 - 209*Power(t2,2) - 
                  55*Power(t2,3) - 3*Power(t1,2)*(29 + 41*t2) + 
                  2*t1*(69 - 36*t2 + 97*Power(t2,2))))) - 
         Power(s,2)*(-24 - 12*t1 + 84*Power(t1,2) - 144*Power(t1,3) + 
            70*Power(t1,4) - 6*Power(t1,5) + 
            Power(s1,5)*(4 + 20*s2 + 6*t1 - 2*t2) + 192*t2 - 130*t1*t2 + 
            81*Power(t1,2)*t2 + 44*Power(t1,3)*t2 - 19*Power(t1,4)*t2 + 
            2*Power(t1,5)*t2 - 242*Power(t2,2) + 186*t1*Power(t2,2) - 
            166*Power(t1,2)*Power(t2,2) + 31*Power(t1,3)*Power(t2,2) - 
            7*Power(t1,4)*Power(t2,2) - 3*Power(t2,3) - 
            32*t1*Power(t2,3) - 23*Power(t1,2)*Power(t2,3) + 
            22*Power(t1,3)*Power(t2,3) + 84*Power(t2,4) + 
            59*t1*Power(t2,4) - 28*Power(t1,2)*Power(t2,4) - 
            42*Power(t2,5) + 8*t1*Power(t2,5) + 3*Power(t2,6) - 
            Power(s2,5)*(4 + 5*t2) + 
            Power(s2,4)*(1 - t2 + 29*Power(t2,2) + 2*t1*(5 + 2*t2)) + 
            Power(s1,4)*(98 + 58*Power(s2,2) + 25*Power(t1,2) + 
               t1*(29 - 30*t2) - 35*t2 + 11*Power(t2,2) - 
               s2*(58 + 65*t1 + 81*t2)) + 
            Power(s2,3)*(1 + 86*t2 + 16*Power(t1,2)*t2 + 5*Power(t2,2) + 
               40*Power(t2,3) - t1*(71 + 8*t2 + 98*Power(t2,2))) + 
            Power(s2,2)*(20 + 21*t2 + 61*Power(t2,2) + 19*Power(t2,3) - 
               24*Power(t2,4) - 2*Power(t1,3)*(10 + 11*t2) + 
               Power(t1,2)*(209 + 102*Power(t2,2)) - 
               t1*(118 + 184*t2 + 15*Power(t2,2) + 40*Power(t2,3))) + 
            s2*(36 - 114*t2 + 185*Power(t2,2) - 60*Power(t2,3) - 
               25*Power(t2,4) - 27*Power(t2,5) + 
               5*Power(t1,4)*(4 + t2) + 
               Power(t1,3)*(-209 + 28*t2 - 26*Power(t2,2)) + 
               Power(t1,2)*(261 + 54*t2 - 21*Power(t2,2) - 
                  22*Power(t2,3)) + 
               t1*(-104 - 74*t2 + 47*Power(t2,2) - 2*Power(t2,3) + 
                  70*Power(t2,4))) + 
            Power(s1,3)*(Power(s2,3) - 11*Power(t1,3) + 
               Power(t1,2)*(62 - 41*t2) - 
               Power(s2,2)*(57 + 21*t1 + 89*t2) + 
               t1*(17 - 170*t2 + 46*Power(t2,2)) + 
               3*(72 - 125*t2 + 41*Power(t2,2) - 8*Power(t2,3)) + 
               s2*(-316 + 31*Power(t1,2) + 311*t2 + 150*Power(t2,2) + 
                  t1*(49 + 50*t2))) + 
            Power(s1,2)*(10 + 32*Power(s2,4) - 33*Power(t1,4) - 519*t2 + 
               540*Power(t2,2) - 199*Power(t2,3) + 26*Power(t2,4) + 
               Power(t1,3)*(171 + 62*t2) + 
               Power(s2,3)*(38 - 89*t1 + 66*t2) + 
               Power(s2,2)*(-75 + 49*Power(t1,2) + t1*(27 - 32*t2) + 
                  162*t2 - 20*Power(t2,2)) - 
               Power(t1,2)*(244 + 276*t2 + 21*Power(t2,2)) + 
               t1*(88 + 114*t2 + 312*Power(t2,2) - 18*Power(t2,3)) + 
               s2*(-53 + 41*Power(t1,3) + 508*t2 - 473*Power(t2,2) - 
                  164*Power(t2,3) - 4*Power(t1,2)*(59 + 24*t2) + 
                  t1*(297 - 8*t2 + 165*Power(t2,2)))) + 
            s1*(-144 + 9*Power(s2,5) - 3*Power(t1,5) + 
               Power(s2,4)*(5 - 23*t1 - 57*t2) + 208*t2 + 
               306*Power(t2,2) - 347*Power(t2,3) + 149*Power(t2,4) - 
               14*Power(t2,5) + Power(t1,4)*(8 + 37*t2) - 
               Power(t1,3)*(45 + 164*t2 + 73*Power(t2,2)) + 
               Power(t1,2)*(4 + 299*t2 + 237*Power(t2,2) + 
                  65*Power(t2,3)) - 
               t1*(-46 + 178*t2 + 99*Power(t2,2) + 230*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,3)*(-32 + 18*Power(t1,2) - 39*t2 - 
                  107*Power(t2,2) + t1*(-9 + 182*t2)) + 
               Power(s2,2)*(64 - 6*Power(t1,3) + 
                  Power(t1,2)*(11 - 156*t2) - 33*t2 - 124*Power(t2,2) + 
                  75*Power(t2,3) + t1*(39 + 26*t2 + 93*Power(t2,2))) + 
               s2*(150 + 5*Power(t1,4) - 204*t2 - 132*Power(t2,2) + 
                  245*Power(t2,3) + 102*Power(t2,4) - 
                  3*Power(t1,3)*(5 + 2*t2) + 
                  Power(t1,2)*(38 + 177*t2 + 87*Power(t2,2)) - 
                  t1*(96 + 186*t2 + 39*Power(t2,2) + 220*Power(t2,3))))) + 
         s*(-8*t1 + 9*Power(t1,2) - 23*Power(t1,3) + 17*Power(t1,4) + 
            5*Power(t1,5) + 2*Power(s1,6)*(2 + s2 + t1) + 56*t2 + 
            6*t1*t2 - 77*Power(t1,2)*t2 + 135*Power(t1,3)*t2 - 
            93*Power(t1,4)*t2 + 3*Power(t1,5)*t2 - 195*Power(t2,2) + 
            177*t1*Power(t2,2) - 120*Power(t1,2)*Power(t2,2) + 
            62*Power(t1,3)*Power(t2,2) - 3*Power(t1,4)*Power(t2,2) - 
            Power(t1,5)*Power(t2,2) + 115*Power(t2,3) - 
            161*t1*Power(t2,3) + 32*Power(t1,2)*Power(t2,3) + 
            21*Power(t1,3)*Power(t2,3) + 2*Power(t1,4)*Power(t2,3) + 
            69*Power(t2,4) + 65*t1*Power(t2,4) - 
            27*Power(t1,2)*Power(t2,4) - 4*Power(t1,3)*Power(t2,4) - 
            71*Power(t2,5) - 12*t1*Power(t2,5) + 
            6*Power(t1,2)*Power(t2,5) + 18*Power(t2,6) - 
            3*t1*Power(t2,6) + Power(s2,5)*(-4 + 3*t2 + 10*Power(t2,2)) + 
            Power(s1,5)*(62 + 27*Power(s2,2) + 3*Power(t1,2) + 
               t1*(17 - 9*t2) - 36*t2 - s2*(5 + 26*t1 + 13*t2)) + 
            Power(s2,4)*(5 + 3*t2 - 5*Power(t2,2) - 4*Power(t2,3) + 
               t1*(7 - 9*t2 - 23*Power(t2,2))) + 
            Power(s2,3)*(2*Power(t1,2)*(-1 + 3*t2 + 5*Power(t2,2)) - 
               t2*(-63 + 73*t2 + 23*Power(t2,2) + 32*Power(t2,3)) + 
               t1*(-42 + 38*t2 + 48*Power(t2,2) + 40*Power(t2,3))) - 
            s2*(-8 + 74*t2 - 96*Power(t2,2) + 15*Power(t2,3) + 
               49*Power(t2,4) + 4*Power(t2,5) - 6*Power(t2,6) + 
               Power(t1,4)*(10 + 9*t2 + 4*Power(t2,2)) - 
               2*Power(t1,3)*(-33 + 115*t2 + 22*Power(t2,2) + 
                  14*Power(t2,3)) + 
               Power(t1,2)*(-34 + 131*t2 + 207*Power(t2,2) + 
                  109*Power(t2,3) + 30*Power(t2,4)) - 
               2*t1*(1 + 48*t2 - 26*Power(t2,2) + 64*Power(t2,3) + 
                  39*Power(t2,4))) + 
            Power(s2,2)*(-11 - 7*t2 + 106*Power(t2,2) - 118*Power(t2,3) - 
               37*Power(t2,4) - 12*Power(t2,5) + 
               Power(t1,3)*(4 + 6*t2 + 8*Power(t2,2)) - 
               2*Power(t1,2)*(-43 + 89*t2 + 42*Power(t2,2) + 
                  33*Power(t2,3)) + 
               t1*(-11 - 67*t2 + 218*Power(t2,2) + 111*Power(t2,3) + 
                  66*Power(t2,4))) + 
            Power(s1,4)*(185 + 6*Power(s2,3) - 23*Power(t1,3) - 305*t2 + 
               122*Power(t2,2) + Power(t1,2)*(54 + 4*t2) - 
               Power(s2,2)*(43 + 25*t1 + 94*t2) + 
               t1*(-6 - 118*t2 + 13*Power(t2,2)) + 
               s2*(-140 + 42*Power(t1,2) + 77*t2 + 38*Power(t2,2) + 
                  t1*(25 + 64*t2))) + 
            Power(s1,3)*(99 + 7*Power(s2,4) - 27*Power(t1,4) - 651*t2 + 
               614*Power(t2,2) - 208*Power(t2,3) + 
               Power(s2,3)*(52 - 22*t1 + 37*t2) + 
               Power(t1,3)*(57 + 79*t2) - 
               Power(t1,2)*(101 + 168*t2 + 36*Power(t2,2)) + 
               t1*(92 - 11*t2 + 264*Power(t2,2) - 2*Power(t2,3)) + 
               s2*(-236 + 46*Power(t1,3) + 500*t2 - 197*Power(t2,2) - 
                  62*Power(t2,3) - 9*Power(t1,2)*(4 + 9*t2) + 
                  t1*(265 - 194*t2 - 36*Power(t2,2))) - 
               Power(s2,2)*(4*Power(t1,2) + t1*(73 + 35*t2) - 
                  4*(-35 + 57*t2 + 33*Power(t2,2)))) + 
            Power(s1,2)*(-107 + 18*Power(s2,5) - 3*Power(t1,5) + 
               Power(s2,4)*(3 - 61*t1 - 10*t2) - 115*t2 + 
               816*Power(t2,2) - 632*Power(t2,3) + 192*Power(t2,4) + 
               Power(t1,4)*(-56 + 50*t2) + 
               Power(s2,3)*(6 + 31*t1 + 78*Power(t1,2) - 124*t2 + 
                  74*t1*t2 - 124*Power(t2,2)) + 
               Power(t1,3)*(147 - 29*t2 - 93*Power(t2,2)) + 
               Power(t1,2)*(-158 + 90*t2 + 147*Power(t2,2) + 
                  54*Power(t2,3)) + 
               t1*(133 - 231*t2 + 105*Power(t2,2) - 254*Power(t2,3) - 
                  12*Power(t2,4)) + 
               Power(s2,2)*(88 - 48*Power(t1,3) + 112*t2 - 
                  364*Power(t2,2) - 102*Power(t2,3) - 
                  Power(t1,2)*(127 + 68*t2) + 
                  t1*(81 + 323*t2 + 211*Power(t2,2))) + 
               s2*(36 + 16*Power(t1,4) + Power(t1,3)*(149 - 46*t2) + 
                  379*t2 - 629*Power(t2,2) + 179*Power(t2,3) + 
                  58*Power(t2,4) + 
                  2*Power(t1,2)*(-117 - 85*t2 + 3*Power(t2,2)) + 
                  t1*(20 - 212*t2 + 391*Power(t2,2) - 16*Power(t2,3)))) + 
            s1*(-56 + Power(s2,5)*(1 - 28*t2) + 4*Power(t1,5)*(-3 + t2) + 
               302*t2 - 99*Power(t2,2) - 419*Power(t2,3) + 
               332*Power(t2,4) - 92*Power(t2,5) + 
               Power(t1,4)*(113 + 56*t2 - 25*Power(t2,2)) + 
               Power(t1,3)*(-192 - 183*t2 - 49*Power(t2,2) + 
                  41*Power(t2,3)) + 
               Power(t1,2)*(131 + 239*t2 - 21*Power(t2,2) - 
                  6*Power(t2,3) - 31*Power(t2,4)) + 
               t1*(-14 - 294*t2 + 300*Power(t2,2) - 153*Power(t2,3) + 
                  103*Power(t2,4) + 11*Power(t2,5)) + 
               Power(s2,4)*(t2*(6 + 7*t2) + 2*t1*(-5 + 42*t2)) + 
               Power(s2,3)*(-18 + Power(t1,2)*(36 - 88*t2) + 60*t2 + 
                  95*Power(t2,2) + 113*Power(t2,3) - 
                  t1*(69 + 84*t2 + 92*Power(t2,2))) + 
               Power(s2,2)*(69 - 221*t2 + 146*Power(t2,2) + 
                  216*Power(t2,3) + 49*Power(t2,4) + 
                  Power(t1,3)*(-58 + 40*t2) + 
                  Power(t1,2)*(251 + 206*t2 + 138*Power(t2,2)) - 
                  t1*(96 + 255*t2 + 361*Power(t2,2) + 217*Power(t2,3))) - 
               s2*(-82 + 148*t2 + 128*Power(t2,2) - 318*Power(t2,3) + 
                  50*Power(t2,4) + 29*Power(t2,5) + 
                  Power(t1,4)*(-43 + 12*t2) + 
                  Power(t1,3)*(295 + 184*t2 + 28*Power(t2,2)) - 
                  9*Power(t1,2)*
                   (34 + 42*t2 + 35*Power(t2,2) + 7*Power(t2,3)) + 
                  t1*(212 - 98*t2 + 181*Power(t2,2) + 300*Power(t2,3) - 
                     14*Power(t2,4)))))))/
     ((-1 + s1)*(-1 + s - s2 + t1)*(s - s2 + t1)*Power(-s + s1 - t2,2)*
       (1 - s + s1 - t2)*(-1 + s1 + t1 - t2)*Power(s1 - s2 + t1 - t2,2)) + 
    (8*(24 + 10*s2 + 4*Power(s2,2) - 58*t1 - 36*s2*t1 - 
         12*Power(s2,2)*t1 + 32*Power(t1,2) + 48*s2*Power(t1,2) + 
         12*Power(s2,2)*Power(t1,2) + 12*Power(t1,3) - 
         28*s2*Power(t1,3) - 4*Power(s2,2)*Power(t1,3) - 8*Power(t1,4) + 
         6*s2*Power(t1,4) - 2*Power(t1,5) + 
         Power(s1,7)*(-Power(s2,2) - 2*s2*t1 + Power(t1,2)) + 
         2*Power(s,6)*Power(t1 - t2,2)*(-1 + s1 + t1 - t2) + 41*t2 + 
         19*s2*t2 + 30*Power(s2,2)*t2 + 8*Power(s2,3)*t2 - 10*t1*t2 + 
         6*s2*t1*t2 - 70*Power(s2,2)*t1*t2 - 16*Power(s2,3)*t1*t2 - 
         126*Power(t1,2)*t2 - 64*s2*Power(t1,2)*t2 + 
         50*Power(s2,2)*Power(t1,2)*t2 + 8*Power(s2,3)*Power(t1,2)*t2 + 
         116*Power(t1,3)*t2 + 34*s2*Power(t1,3)*t2 - 
         10*Power(s2,2)*Power(t1,3)*t2 - 19*Power(t1,4)*t2 + 
         5*s2*Power(t1,4)*t2 - 2*Power(t1,5)*t2 - 36*Power(t2,2) + 
         22*s2*Power(t2,2) - 44*Power(s2,2)*Power(t2,2) + 
         26*Power(s2,3)*Power(t2,2) + 4*Power(s2,4)*Power(t2,2) + 
         148*t1*Power(t2,2) + 141*s2*t1*Power(t2,2) + 
         104*Power(s2,2)*t1*Power(t2,2) - 28*Power(s2,3)*t1*Power(t2,2) - 
         4*Power(s2,4)*t1*Power(t2,2) - 134*Power(t1,2)*Power(t2,2) - 
         207*s2*Power(t1,2)*Power(t2,2) - 
         56*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         2*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         8*Power(t1,3)*Power(t2,2) + 39*s2*Power(t1,3)*Power(t2,2) - 
         4*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         14*Power(t1,4)*Power(t2,2) + 5*s2*Power(t1,4)*Power(t2,2) - 
         43*Power(t2,3) - 91*s2*Power(t2,3) - 
         26*Power(s2,2)*Power(t2,3) - 45*Power(s2,3)*Power(t2,3) + 
         6*Power(s2,4)*Power(t2,3) + 52*t1*Power(t2,3) + 
         157*s2*t1*Power(t2,3) + 104*Power(s2,2)*t1*Power(t2,3) + 
         36*Power(s2,3)*t1*Power(t2,3) + 2*Power(s2,4)*t1*Power(t2,3) + 
         29*Power(t1,2)*Power(t2,3) - 13*s2*Power(t1,2)*Power(t2,3) - 
         18*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         30*Power(t1,3)*Power(t2,3) - 29*s2*Power(t1,3)*Power(t2,3) - 
         4*Power(s2,2)*Power(t1,3)*Power(t2,3) - 4*Power(t2,4) - 
         38*s2*Power(t2,4) - 46*Power(s2,2)*Power(t2,4) - 
         15*Power(s2,3)*Power(t2,4) - 6*Power(s2,4)*Power(t2,4) - 
         25*t1*Power(t2,4) - 6*s2*t1*Power(t2,4) - 
         Power(s2,2)*t1*Power(t2,4) - 2*Power(s2,3)*t1*Power(t2,4) + 
         16*Power(t1,2)*Power(t2,4) + 48*s2*Power(t1,2)*Power(t2,4) + 
         17*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         Power(s2,3)*Power(t1,2)*Power(t2,4) + Power(t1,3)*Power(t2,4) + 
         7*Power(t2,5) + 7*s2*Power(t2,5) + 5*Power(s2,3)*Power(t2,5) - 
         6*t1*Power(t2,5) - 28*s2*t1*Power(t2,5) - 
         19*Power(s2,2)*t1*Power(t2,5) - 2*Power(s2,3)*t1*Power(t2,5) + 
         Power(t1,2)*Power(t2,5) - s2*Power(t1,2)*Power(t2,5) + 
         4*Power(t2,6) + 6*s2*Power(t2,6) + 6*Power(s2,2)*Power(t2,6) + 
         Power(s2,3)*Power(t2,6) - t1*Power(t2,6) - Power(t2,7) + 
         s2*Power(t2,7) + Power(s1,6)*
          (Power(s2,2)*(-2 - 5*t1 + 6*t2) + 
            t1*(2 + t1 - 2*t2 - 5*t1*t2) + 
            s2*(2 + t1 + Power(t1,2) + 2*t2 + 11*t1*t2)) - 
         Power(s1,5)*(1 - Power(t1,3) + Power(t1,4) + 2*t2 + 
            Power(t1,2)*(3 - 10*t2)*t2 - Power(t2,2) + 
            Power(s2,3)*(6 + t1 + t2) + 
            t1*(1 + 13*t2 - 10*Power(t2,2)) + 
            Power(s2,2)*(3 + 3*Power(t1,2) + t1*(7 - 24*t2) - 5*t2 + 
               15*Power(t2,2)) + 
            s2*(-4 - 3*Power(t1,3) + 13*t2 + 11*Power(t2,2) + 
               Power(t1,2)*(-9 + 4*t2) + t1*(-14 + 9*t2 + 25*Power(t2,2))\
)) + Power(s1,4)*(-2 - 6*Power(s2,4) - Power(t1,3)*(-9 + t2) + 7*t2 + 
            12*Power(t2,2) - 5*Power(t2,3) + Power(t1,4)*(-2 + 3*t2) + 
            Power(s2,3)*(-15 - Power(t1,2) + 29*t2 + 2*t1*t2 + 
               5*Power(t2,2)) + 
            t1*(-9 + 7*t2 + 31*Power(t2,2) - 20*Power(t2,3)) - 
            2*Power(t1,2)*(4 + 3*t2 - Power(t2,2) + 5*Power(t2,3)) + 
            Power(s2,2)*(23 + Power(t1,3) + 7*t2 + 6*Power(t2,2) + 
               20*Power(t2,3) + Power(t1,2)*(-18 + 11*t2) + 
               t1*(-20 + 7*t2 - 46*Power(t2,2))) + 
            s2*(6 - 10*Power(t1,3)*(-2 + t2) - 14*t2 + 38*Power(t2,2) + 
               25*Power(t2,3) + 
               Power(t1,2)*(24 - 38*t2 + 6*Power(t2,2)) + 
               t1*(-30 - 80*t2 + 26*Power(t2,2) + 30*Power(t2,3)))) + 
         Power(s1,3)*(-3 - 2*Power(s2,4)*(3 + t1 - 12*t2) + 9*t2 - 
            22*Power(t2,2) - 28*Power(t2,3) + 10*Power(t2,4) + 
            Power(t1,3)*(1 - 22*t2 - 3*Power(t2,2)) + 
            Power(t1,4)*(13 + 3*t2 - 3*Power(t2,2)) + 
            Power(t1,2)*(-56 + 4*t2 + 17*Power(t2,2) + 2*Power(t2,3) + 
               5*Power(t2,4)) + 
            t1*(37 + 54*t2 - 9*Power(t2,2) - 34*Power(t2,3) + 
               20*Power(t2,4)) + 
            Power(s2,3)*(35 + 60*t2 - 56*Power(t2,2) - 10*Power(t2,3) + 
               Power(t1,2)*(-7 + 2*t2) + 2*t1*(-10 + t2 + Power(t2,2))) \
- Power(s2,2)*(3 + 3*Power(t1,3)*(-1 + t2) + 15*t2 + 3*Power(t2,2) + 
               34*Power(t2,3) + 15*Power(t2,4) + 
               Power(t1,2)*(9 - 29*t2 + 15*Power(t2,2)) - 
               t1*(-47 + 61*t2 + 40*Power(t2,2) + 44*Power(t2,3))) + 
            s2*(-28 + 3*Power(t1,4) + 33*t2 + 11*Power(t2,2) - 
               62*Power(t2,3) - 30*Power(t2,4) + 
               Power(t1,3)*(11 - 49*t2 + 12*Power(t2,2)) - 
               Power(t1,2)*(35 + 129*t2 - 61*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(25 + 81*t2 + 184*Power(t2,2) - 34*Power(t2,3) - 
                  20*Power(t2,4)))) + 
         Power(s1,2)*(11 + 2*Power(t1,5) - 39*t2 - 16*Power(t2,2) + 
            34*Power(t2,3) + 32*Power(t2,4) - 10*Power(t2,5) + 
            2*Power(s2,4)*(-1 + t1 - 6*t2)*(-2 + 3*t2) + 
            Power(t1,4)*(15 - 24*t2 + Power(t2,3)) + 
            Power(t1,3)*(-58 - 36*t2 + 18*Power(t2,2) + 5*Power(t2,3)) - 
            Power(t1,2)*(-34 - 141*t2 - 32*Power(t2,2) + 
               15*Power(t2,3) + 3*Power(t2,4) + Power(t2,5)) - 
            t1*(4 + 18*t2 + 106*Power(t2,2) + 5*Power(t2,3) - 
               16*Power(t2,4) + 10*Power(t2,5)) + 
            Power(s2,3)*(22 - 115*t2 - 90*Power(t2,2) + 
               54*Power(t2,3) + 10*Power(t2,4) + 
               Power(t1,2)*(-2 + 15*t2) - 
               2*t1*(10 - 38*t2 + 3*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,2)*(-28 - 32*t2 - 85*Power(t2,2) - 3*Power(t2,3) + 
               46*Power(t2,4) + 6*Power(t2,5) + 
               Power(t1,3)*(-8 - 10*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-32 - 12*t2 + 13*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(68 + 222*t2 - 63*Power(t2,2) - 92*Power(t2,3) - 
                  21*Power(t2,4))) + 
            s2*(51 + Power(t1,4)*(8 - 6*t2) - 39*t2 - 122*Power(t2,2) + 
               11*Power(t2,3) + 58*Power(t2,4) + 20*Power(t2,5) - 
               Power(t1,3)*(3 + 43*t2 - 38*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,2)*(-103 + 37*t2 + 234*Power(t2,2) - 
                  45*Power(t2,3) + Power(t2,4)) + 
               t1*(47 + 123*t2 - 78*Power(t2,2) - 212*Power(t2,3) + 
                  21*Power(t2,4) + 7*Power(t2,5)))) - 
         s1*(33 + 2*Power(t1,5)*(-2 + t2) - 25*t2 - 85*Power(t2,2) - 
            13*Power(t2,3) + 25*Power(t2,4) + 18*Power(t2,5) - 
            5*Power(t2,6) + Power(t1,4)*
             (1 + 25*t2 - 11*Power(t2,2) + Power(t2,3)) + 
            Power(t1,3)*(76 - 38*t2 - 65*Power(t2,2) + 6*Power(t2,3) + 
               2*Power(t2,4)) + 
            t1*t2*(148 + 71*t2 - 86*Power(t2,2) - 14*Power(t2,3) + 
               Power(t2,4) - 2*Power(t2,5)) - 
            Power(t1,2)*(106 + 112*t2 - 114*Power(t2,2) - 
               44*Power(t2,3) + 3*Power(t2,4) + Power(t2,5)) + 
            2*Power(s2,4)*t2*
             (4 + 9*t2 - 12*Power(t2,2) + t1*(-4 + 3*t2)) + 
            Power(s2,3)*(8 + 48*t2 - 125*Power(t2,2) - 60*Power(t2,3) + 
               26*Power(t2,4) + 5*Power(t2,5) + 
               Power(t1,2)*(8 + 9*Power(t2,2) + 2*Power(t2,3)) - 
               t1*(16 + 48*t2 - 92*Power(t2,2) + 6*Power(t2,3) + 
                  7*Power(t2,4))) + 
            Power(s2,2)*(26 - 72*t2 - 61*Power(t2,2) - 
               123*Power(t2,3) - 2*Power(t2,4) + 27*Power(t2,5) + 
               Power(t2,6) + 
               Power(t1,3)*(-6 - 12*t2 - 11*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,2)*(38 - 88*t2 - 39*Power(t2,2) + 
                  41*Power(t2,3) + 2*Power(t2,4)) - 
               t1*(58 - 172*t2 - 279*Power(t2,2) + 23*Power(t2,3) + 
                  71*Power(t2,4) + 4*Power(t2,5))) + 
            s2*(25 + 69*t2 - 158*Power(t2,2) - 121*Power(t2,3) + 
               19*Power(t2,4) + 29*Power(t2,5) + 7*Power(t2,6) + 
               Power(t1,4)*(3 + 13*t2 - 3*Power(t2,2)) + 
               Power(t1,3)*(34 + 40*t2 - 61*Power(t2,2) + 
                  9*Power(t2,3) - Power(t2,4)) - 
               Power(t1,2)*(52 + 322*t2 + 11*Power(t2,2) - 
                  177*Power(t2,3) + 14*Power(t2,4)) + 
               t1*(-10 + 200*t2 + 305*Power(t2,2) - 33*Power(t2,3) - 
                  122*Power(t2,4) + 5*Power(t2,5) + Power(t2,6)))) + 
         Power(s,5)*(Power(t1,4) - 
            2*Power(t1,3)*(6 + 5*s1 + s2 - 2*t2) + 
            Power(t1,2)*(8 - 11*Power(s1,2) + 30*t2 - 18*Power(t2,2) + 
               s2*(9 + 8*t2) + s1*(-7 + s2 + 34*t2)) + 
            t1*(-1 - 14*t2 - 24*Power(t2,2) + 20*Power(t2,3) + 
               Power(s1,2)*(-7 + 3*s2 + 18*t2) + 
               s1*(8 + 4*(4 + s2)*t2 - 38*Power(t2,2)) - 
               s2*(3 + 24*t2 + 10*Power(t2,2))) + 
            t2*(-3 + Power(s1,2)*(3 + s2 - 7*t2) + 6*t2 + 
               6*Power(t2,2) - 7*Power(t2,3) + 
               s2*(7 + 15*t2 + 4*Power(t2,2)) - 
               s1*((9 - 14*t2)*t2 + s2*(8 + 5*t2)))) - 
         Power(s,4)*(-3 + 15*t1 - 2*Power(t1,2) - 11*Power(t1,3) + 
            5*Power(t1,4) - 13*t2 + 21*t1*t2 + 8*Power(t1,2)*t2 + 
            17*Power(t1,3)*t2 - 3*Power(t1,4)*t2 + Power(t2,2) + 
            5*t1*Power(t2,2) - 77*Power(t1,2)*Power(t2,2) - 
            2*Power(t2,3) + 83*t1*Power(t2,3) + 
            18*Power(t1,2)*Power(t2,3) - 28*Power(t2,4) - 
            24*t1*Power(t2,4) + 9*Power(t2,5) + 
            Power(s2,2)*(-2 - Power(t1,3) + 25*t2 + 26*Power(t2,2) + 
               3*Power(t2,3) + Power(t1,2)*(7 + 5*t2) - 
               t1*t2*(33 + 7*t2)) + 
            Power(s1,3)*(Power(s2,2) - 25*Power(t1,2) + (8 - 9*t2)*t2 + 
               s2*(-3 + 14*t1 + 3*t2) + t1*(-25 + 32*t2)) + 
            s2*(7 + 9*t2 + 24*Power(t2,2) - 19*Power(t2,3) - 
               13*Power(t2,4) + Power(t1,3)*(-19 + 5*t2) + 
               Power(t1,2)*(40 + 45*t2 - 23*Power(t2,2)) + 
               t1*(-36 - 82*t2 - 7*Power(t2,2) + 31*Power(t2,3))) + 
            Power(s1,2)*(1 + 5*Power(s2,2)*t1 - 20*Power(t1,3) + 8*t2 - 
               44*Power(t2,2) + 27*Power(t2,3) + 
               Power(t1,2)*(-40 + 81*t2) + 
               t1*(8 + 108*t2 - 88*Power(t2,2)) + 
               s2*(5 + 3*Power(t1,2) - 9*t2 - 19*Power(t2,2) - 
                  2*t1*(6 + t2))) + 
            s1*(2 + 5*Power(t1,4) + t2 - 6*Power(t2,2) + 
               64*Power(t2,3) - 27*Power(t2,4) + 
               8*Power(t1,3)*(-5 + 2*t2) + 
               Power(t1,2)*(-3 + 142*t2 - 74*Power(t2,2)) + 
               Power(s2,2)*(1 + 3*Power(t1,2) + t1*(-9 + t2) - 21*t2 - 
                  4*Power(t2,2)) + 
               t1*(-2 - 33*t2 - 166*Power(t2,2) + 80*Power(t2,3)) + 
               s2*(-9 - 11*Power(t1,3) - 11*t2 + 31*Power(t2,2) + 
                  29*Power(t2,3) + Power(t1,2)*(17 + 25*t2) + 
                  t1*(42 - 12*t2 - 43*Power(t2,2))))) + 
         Power(s,3)*(4 + 56*t1 - 107*Power(t1,2) + 48*Power(t1,3) + 
            3*Power(t1,4) - 25*t2 + 139*t1*t2 - 141*Power(t1,2)*t2 + 
            16*Power(t1,3)*t2 - 11*Power(t1,4)*t2 - 40*Power(t2,2) + 
            75*t1*Power(t2,2) - 35*Power(t1,2)*Power(t2,2) + 
            5*Power(t1,3)*Power(t2,2) + 3*Power(t1,4)*Power(t2,2) - 
            22*Power(t2,3) + 38*t1*Power(t2,3) + 
            55*Power(t1,2)*Power(t2,3) - 4*Power(t1,3)*Power(t2,3) - 
            22*Power(t2,4) - 81*t1*Power(t2,4) - 
            6*Power(t1,2)*Power(t2,4) + 32*Power(t2,5) + 
            12*t1*Power(t2,5) - 5*Power(t2,6) + 
            Power(s1,4)*(4*Power(s2,2) - 30*Power(t1,2) + 
               (7 - 5*t2)*t2 + s2*(-8 + 26*t1 + 3*t2) + t1*(-33 + 28*t2)\
) + Power(s2,3)*(-4 + 20*t2 + 21*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(2 + t2) + t1*(2 - 19*t2 - 2*Power(t2,2))) + 
            Power(s2,2)*(34 + 76*t2 + 3*Power(t1,3)*t2 - 
               51*Power(t2,3) - 9*Power(t2,4) + 
               Power(t1,2)*(40 + 9*t2 - 15*Power(t2,2)) + 
               t1*(-70 - 138*t2 + 42*Power(t2,2) + 21*Power(t2,3))) - 
            s2*(28 + 3*Power(t1,4) + 65*t2 + 8*Power(t2,2) + 
               55*Power(t2,3) + 17*Power(t2,4) - 15*Power(t2,5) + 
               3*Power(t1,3)*(17 - 17*t2 + Power(t2,2)) - 
               Power(t1,2)*(77 + 40*t2 - 154*Power(t2,2) + 
                  21*Power(t2,3)) + 
               t1*(3 - 44*t2 - 98*Power(t2,2) - 123*Power(t2,3) + 
                  33*Power(t2,4))) + 
            Power(s1,3)*(14 - 20*Power(t1,3) + 
               Power(s2,2)*(2 + 20*t1 - 6*t2) + 15*t2 - 
               53*Power(t2,2) + 20*Power(t2,3) + 
               Power(t1,2)*(-59 + 104*t2) + 
               t1*(-16 + 185*t2 - 96*Power(t2,2)) + 
               s2*(-10 + 2*Power(t1,2) + 21*t2 - 24*Power(t2,2) - 
                  t1*(34 + 41*t2))) + 
            Power(s1,2)*(10*Power(t1,4) + Power(s2,3)*(8 + t1 + t2) + 
               Power(t1,3)*(-49 + 24*t2) + 
               Power(t1,2)*(-25 + 228*t2 - 124*Power(t2,2)) - 
               t2*(27 + 52*t2 - 117*Power(t2,2) + 30*Power(t2,3)) + 
               2*t1*(27 + 8*t2 - 176*Power(t2,2) + 60*Power(t2,3)) + 
               Power(s2,2)*(18 + 12*Power(t1,2) - 49*t2 - 
                  9*Power(t2,2) - t1*(17 + 21*t2)) - 
               s2*(20 + 24*Power(t1,3) + Power(t1,2)*(9 - 31*t2) + 
                  9*t2 + 35*Power(t2,2) - 54*Power(t2,3) + 
                  t1*(-28 - 135*t2 + 29*Power(t2,2)))) + 
            s1*(-22 + Power(t1,4)*(17 - 12*t2) + 34*t2 + 
               35*Power(t2,2) + 59*Power(t2,3) - 103*Power(t2,4) + 
               20*Power(t2,5) + Power(t1,3)*(-6 + 29*t2) + 
               Power(t1,2)*(82 + 73*t2 - 224*Power(t2,2) + 
                  56*Power(t2,3)) - 
               t1*(57 + 118*t2 + 38*Power(t2,2) - 281*Power(t2,3) + 
                  64*Power(t2,4)) + 
               Power(s2,3)*(Power(t1,2) + t1*(-3 + t2) - 
                  t2*(29 + 2*t2)) - 
               Power(s2,2)*(70 + 4*Power(t1,3) + t2 - 98*Power(t2,2) - 
                  20*Power(t2,3) - Power(t1,2)*(45 + 4*t2) + 
                  t1*(-71 + 39*t2 + 20*Power(t2,2))) + 
               s2*(78 + 34*t2 + 74*Power(t2,2) + 39*Power(t2,3) - 
                  48*Power(t2,4) + Power(t1,3)*(-76 + 25*t2) + 
                  Power(t1,2)*(-31 + 197*t2 - 54*Power(t2,2)) + 
                  t1*(-25 - 172*t2 - 224*Power(t2,2) + 77*Power(t2,3))))) \
+ Power(s,2)*(-14 - 78*t1 + 198*Power(t1,2) - 128*Power(t1,3) + 
            20*Power(t1,4) + 2*Power(t1,5) + 77*t2 - 186*t1*t2 + 
            96*Power(t1,2)*t2 + 14*Power(t1,3)*t2 - 5*Power(t1,4)*t2 + 
            63*Power(t2,2) + 58*t1*Power(t2,2) - 
            172*Power(t1,2)*Power(t2,2) + 24*Power(t1,3)*Power(t2,2) - 
            7*Power(t1,4)*Power(t2,2) - 38*Power(t2,3) + 
            125*t1*Power(t2,3) - 39*Power(t1,2)*Power(t2,3) + 
            13*Power(t1,3)*Power(t2,3) + Power(t1,4)*Power(t2,3) - 
            27*Power(t2,4) + 48*t1*Power(t2,4) + 
            9*Power(t1,2)*Power(t2,4) - 2*Power(t1,3)*Power(t2,4) - 
            30*Power(t2,5) - 29*t1*Power(t2,5) + 14*Power(t2,6) + 
            2*t1*Power(t2,6) - Power(t2,7) - 
            2*Power(s2,4)*t2*(1 - t1 + 3*t2) + 
            Power(s1,5)*(-6*Power(s2,2) + 20*Power(t1,2) + 
               t1*(19 - 12*t2) + (-2 + t2)*t2 - s2*(-7 + 24*t1 + t2)) + 
            Power(s2,3)*(6 - 73*t2 + Power(t2,2) + 47*Power(t2,3) + 
               3*Power(t2,4) + 
               Power(t1,2)*(-6 + 5*t2 + 3*Power(t2,2)) - 
               2*t1*t2*(-36 + 20*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(-61 + 80*t2 + 123*Power(t2,2) + 
               45*Power(t2,3) - 18*Power(t2,4) - 9*Power(t2,5) + 
               Power(t1,3)*(3 - 4*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-55 + 18*t2 + 56*Power(t2,2) - 
                  15*Power(t2,3)) + 
               t1*(113 - 70*t2 - 210*Power(t2,2) - 34*Power(t2,3) + 
                  21*Power(t2,4))) + 
            s2*(82 + 36*t2 - 6*Power(t1,4)*t2 + 24*Power(t2,2) + 
               74*Power(t2,3) - 21*Power(t2,4) - 30*Power(t2,5) + 
               7*Power(t2,6) + 
               Power(t1,3)*(8 - 99*t2 + 45*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(42 + 274*t2 + 182*Power(t2,2) - 
                  138*Power(t2,3) + 5*Power(t2,4)) - 
               t1*(132 + 229*t2 + 179*Power(t2,2) + 34*Power(t2,3) - 
                  129*Power(t2,4) + 13*Power(t2,5))) + 
            Power(s1,4)*(-15 + 10*Power(t1,3) + 
               Power(t1,2)*(37 - 76*t2) - 
               6*Power(s2,2)*(1 + 5*t1 - 3*t2) - 11*t2 + 
               22*Power(t2,2) - 5*Power(t2,3) + 
               2*t1*(13 - 66*t2 + 25*Power(t2,2)) + 
               s2*(32 + 2*Power(t1,2) - 33*t2 + 11*Power(t2,2) + 
                  t1*(33 + 71*t2))) - 
            Power(s1,3)*(20 + 10*Power(t1,4) - 53*t2 - 63*Power(t2,2) + 
               68*Power(t2,3) - 10*Power(t2,4) + 
               Power(s2,3)*(22 + 3*t1 + 3*t2) + 
               Power(t1,3)*(-27 + 16*t2) + 
               Power(s2,2)*(30 + 18*Power(t1,2) - 40*t2 - 69*t1*t2 + 
                  9*Power(t2,2)) - 
               9*Power(t1,2)*(1 - 17*t2 + 12*Power(t2,2)) + 
               t1*(64 + 90*t2 - 311*Power(t2,2) + 80*Power(t2,3)) + 
               s2*(-20 - 26*Power(t1,3) + 78*t2 - 87*Power(t2,2) + 
                  34*Power(t2,3) + Power(t1,2)*(-42 + 23*t2) + 
                  t1*(-73 + 213*t2 + 56*Power(t2,2)))) - 
            Power(s1,2)*(-46 + 6*Power(s2,4) + 14*t2 + 88*Power(t2,2) + 
               123*Power(t2,3) - 92*Power(t2,4) + 10*Power(t2,5) + 
               4*Power(t1,3)*(3 + 2*t2) - 3*Power(t1,4)*(-7 + 6*t2) + 
               Power(s2,3)*(19 - 6*t1 + 3*Power(t1,2) - 91*t2 - 
                  9*Power(t2,2)) + 
               Power(t1,2)*(101 + 102*t2 - 204*Power(t2,2) + 
                  68*Power(t2,3)) - 
               t1*(34 + 284*t2 + 150*Power(t2,2) - 331*Power(t2,3) + 
                  60*Power(t2,4)) + 
               Power(s2,2)*(-172 - 6*Power(t1,3) + 
                  Power(t1,2)*(87 - 18*t2) - 78*t2 + 80*Power(t2,2) + 
                  21*Power(t2,3) + t1*(139 + 8*t2 + 27*Power(t2,2))) + 
               s2*(113 - 29*t2 - 39*Power(t2,2) + 133*Power(t2,3) - 
                  46*Power(t2,4) + 5*Power(t1,3)*(-23 + 9*t2) + 
                  Power(t1,2)*(-167 + 291*t2 - 45*Power(t2,2)) + 
                  t1*(77 + 108*t2 - 456*Power(t2,2) + 18*Power(t2,3)))) \
+ s1*(11 - 2*Power(s2,4)*(-1 + t1 - 6*t2) - 102*t2 + 72*Power(t2,2) + 
               77*Power(t2,3) + 101*Power(t2,4) - 58*Power(t2,5) + 
               5*Power(t2,6) + Power(t1,4)*(7 + 25*t2 - 9*Power(t2,2)) + 
               2*Power(t1,3)*
                (-23 - 7*t2 - 16*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(-2 + 305*t2 + 132*Power(t2,2) - 
                  97*Power(t2,3) + 16*Power(t2,4)) - 
               t1*(-34 + 126*t2 + 345*Power(t2,2) + 134*Power(t2,3) - 
                  162*Power(t2,4) + 20*Power(t2,5)) + 
               Power(s2,3)*(39 - 11*Power(t1,2) + 18*t2 - 
                  116*Power(t2,2) - 9*Power(t2,3) + 
                  t1*(-32 + 34*t2 + 9*Power(t2,2))) + 
               Power(s2,2)*(-57 + Power(t1,3)*(3 - 9*t2) - 311*t2 - 
                  93*Power(t2,2) + 64*Power(t2,3) + 27*Power(t2,4) + 
                  Power(t1,2)*(-61 + 23*t2 + 15*Power(t2,2)) + 
                  t1*(91 + 373*t2 + 42*Power(t2,2) - 33*Power(t2,3))) + 
               s2*(-48 + 9*Power(t1,4) + 98*t2 - 123*Power(t2,2) + 
                  28*Power(t2,3) + 102*Power(t2,4) - 29*Power(t2,5) + 
                  Power(t1,3)*(84 - 149*t2 + 18*Power(t2,2)) - 
                  Power(t1,2)*
                   (173 + 382*t2 - 387*Power(t2,2) + 29*Power(t2,3)) + 
                  t1*(152 + 269*t2 + 69*Power(t2,2) - 405*Power(t2,3) + 
                     40*Power(t2,4))))) + 
         s*(-17 + 78*t1 - 120*Power(t1,2) + 74*Power(t1,3) - 
            15*Power(t1,4) - 106*t2 + 94*t1*t2 + 138*Power(t1,2)*t2 - 
            152*Power(t1,3)*t2 + 24*Power(t1,4)*t2 + 2*Power(t1,5)*t2 + 
            12*Power(t2,2) - 244*t1*Power(t2,2) + 
            284*Power(t1,2)*Power(t2,2) - 44*Power(t1,3)*Power(t2,2) - 
            8*Power(t1,4)*Power(t2,2) + 101*Power(t2,3) - 
            155*t1*Power(t2,3) - 33*Power(t1,2)*Power(t2,3) + 
            20*Power(t1,3)*Power(t2,3) - Power(t1,4)*Power(t2,3) + 
            30*Power(t2,4) + 34*t1*Power(t2,4) - 
            19*Power(t1,2)*Power(t2,4) + 3*Power(t1,3)*Power(t2,4) - 
            Power(t2,5) + 18*t1*Power(t2,5) - Power(t1,2)*Power(t2,5) - 
            13*Power(t2,6) - 3*t1*Power(t2,6) + 2*Power(t2,7) + 
            4*Power(s2,4)*t2*(1 + t1*(-1 + t2) + t2 - 3*Power(t2,2)) + 
            Power(s1,6)*(4*Power(s2,2) + s2*(-2 + 11*t1) + 
               t1*(-4 - 7*t1 + 2*t2)) + 
            Power(s2,3)*(4 + 28*t2 - 114*Power(t2,2) - 34*Power(t2,3) + 
               31*Power(t2,4) + 3*Power(t2,5) + 
               Power(t1,2)*(4 - 4*t2 + 4*Power(t2,2) + 3*Power(t2,3)) - 
               t1*(8 + 24*t2 - 106*Power(t2,2) + 23*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s2,2)*(20 - 127*t2 + 2*Power(t2,2) + Power(t2,3) + 
               20*Power(t2,4) + 13*Power(t2,5) - 3*Power(t2,6) + 
               Power(t1,3)*t2*(-1 - 8*t2 + Power(t2,2)) + 
               Power(t1,2)*(20 - 133*t2 - 40*Power(t2,2) + 
                  57*Power(t2,3) - 5*Power(t2,4)) + 
               t1*(-40 + 261*t2 + 118*Power(t2,2) - 73*Power(t2,3) - 
                  62*Power(t2,4) + 7*Power(t2,5))) + 
            s2*(-41 + 28*t2 - 55*Power(t2,2) + 38*Power(t2,3) + 
               76*Power(t2,4) + 9*Power(t2,5) - 8*Power(t2,6) + 
               Power(t2,7) + Power(t1,4)*(-3 + 5*t2 - 3*Power(t2,2)) + 
               Power(t1,3)*(54 + 75*t2 - 77*Power(t2,2) + 
                  13*Power(t2,3) + Power(t2,4)) - 
               Power(t1,2)*(140 + 193*t2 - 148*Power(t2,2) - 
                  150*Power(t2,3) + 39*Power(t2,4) + Power(t2,5)) - 
               t1*(-130 - 85*t2 + 49*Power(t2,2) + 173*Power(t2,3) + 
                  75*Power(t2,4) - 37*Power(t2,5) + Power(t2,6))) + 
            Power(s1,5)*(2 - 2*Power(t1,3) + 
               2*Power(s2,2)*(3 + 10*t1 - 9*t2) + 4*t2 - 2*Power(t2,2) + 
               10*Power(t1,2)*(-1 + 3*t2) + 
               t1*(-12 + 37*t2 - 10*Power(t2,2)) - 
               s2*(19 + 3*Power(t1,2) - 9*t2 + Power(t2,2) + 
                  t1*(12 + 47*t2))) + 
            Power(s1,4)*(15 + 5*Power(t1,4) - 8*t2 - 29*Power(t2,2) + 
               10*Power(t2,3) + Power(s2,3)*(20 + 3*t1 + 3*t2) + 
               Power(t1,3)*(-7 + 4*t2) + 
               Power(t1,2)*(5 + 40*t2 - 50*Power(t2,2)) + 
               Power(s2,2)*(16 + 12*Power(t1,2) + t1*(15 - 71*t2) - 
                  17*t2 + 29*Power(t2,2)) + 
               t1*(14 + 68*t2 - 111*Power(t2,2) + 20*Power(t2,3)) + 
               s2*(-11 - 14*Power(t1,3) + 77*t2 - 24*Power(t2,2) + 
                  5*Power(t2,3) + Power(t1,2)*(-34 + 13*t2) + 
                  t1*(-70 + 99*t2 + 77*Power(t2,2)))) + 
            Power(s1,3)*(5 + 12*Power(s2,4) + Power(t1,4)*(11 - 12*t2) - 
               61*t2 + 13*Power(t2,2) + 76*Power(t2,3) - 
               20*Power(t2,4) - Power(t1,3)*(2 + 3*t2) + 
               Power(t1,2)*(41 + 43*t2 - 59*Power(t2,2) + 
                  40*Power(t2,3)) + 
               t1*(13 - 111*t2 - 150*Power(t2,2) + 154*Power(t2,3) - 
                  20*Power(t2,4)) + 
               Power(s2,3)*(34 + 3*Power(t1,2) - 91*t2 - 
                  12*Power(t2,2) - 3*t1*(1 + t2)) + 
               Power(s2,2)*(-129 - 4*Power(t1,3) + 
                  Power(t1,2)*(67 - 28*t2) - 53*t2 + 2*Power(t2,2) - 
                  16*Power(t2,3) + t1*(88 + 7*t2 + 86*Power(t2,2))) + 
               s2*(38 - 43*t2 - 126*Power(t2,2) + 46*Power(t2,3) - 
                  10*Power(t2,4) + Power(t1,3)*(-78 + 35*t2) + 
                  Power(t1,2)*(-120 + 177*t2 - 20*Power(t2,2)) + 
                  t1*(96 + 255*t2 - 262*Power(t2,2) - 58*Power(t2,3)))) + 
            Power(s1,2)*(-54 + 4*Power(s2,4)*(1 + t1 - 9*t2) + 97*t2 + 
               107*Power(t2,2) - 11*Power(t2,3) - 94*Power(t2,4) + 
               20*Power(t2,5) + 
               Power(t1,4)*(-23 - 17*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(21 + 20*t2 + 30*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(t1,2)*(109 - 119*t2 - 120*Power(t2,2) + 
                  37*Power(t2,3) - 15*Power(t2,4)) + 
               t1*(-53 - 185*t2 + 214*Power(t2,2) + 162*Power(t2,3) - 
                  106*Power(t2,4) + 10*Power(t2,5)) + 
               Power(s2,3)*(-70 + Power(t1,2)*(16 - 3*t2) - 102*t2 + 
                  153*Power(t2,2) + 18*Power(t2,3) + 
                  t1*(50 - 17*t2 - 9*Power(t2,2))) + 
               Power(s2,2)*(20 + 267*t2 + 78*Power(t2,2) + 
                  36*Power(t2,3) - 6*Power(t2,4) + 
                  Power(t1,3)*(-6 + 9*t2) + 
                  Power(t1,2)*(30 - 61*t2 + 15*Power(t2,2)) - 
                  t1*(-28 + 273*t2 + 121*Power(t2,2) + 32*Power(t2,3))) \
+ s2*(60 - 9*Power(t1,4) - 48*t2 + 195*Power(t2,2) + 106*Power(t2,3) - 
                  54*Power(t2,4) + 10*Power(t2,5) + 
                  Power(t1,3)*(-44 + 147*t2 - 27*Power(t2,2)) + 
                  Power(t1,2)*
                   (119 + 432*t2 - 291*Power(t2,2) + 12*Power(t2,3)) + 
                  t1*(-162 - 375*t2 - 375*Power(t2,2) + 
                     312*Power(t2,3) + 17*Power(t2,4)))) - 
            s1*(-49 + 4*Power(t1,5) - 40*t2 + 203*Power(t2,2) + 
               91*Power(t2,3) - 5*Power(t2,4) - 56*Power(t2,5) + 
               10*Power(t2,6) + 
               Power(t1,4)*(19 - 29*t2 - 7*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(-142 - 43*t2 + 38*Power(t2,2) + 
                  23*Power(t2,3) - 2*Power(t2,4)) + 
               Power(t1,2)*(182 + 429*t2 - 111*Power(t2,2) - 
                  91*Power(t2,3) + 7*Power(t2,4) - 2*Power(t2,5)) + 
               t1*(-14 - 317*t2 - 327*Power(t2,2) + 151*Power(t2,3) + 
                  86*Power(t2,4) - 33*Power(t2,5) + 2*Power(t2,6)) + 
               4*Power(s2,4)*
                (1 + 2*t2 - 9*Power(t2,2) + t1*(-1 + 2*t2)) + 
               Power(s2,3)*(24 - 184*t2 - 102*Power(t2,2) + 
                  113*Power(t2,3) + 12*Power(t2,4) + 
                  Power(t1,2)*(-8 + 20*t2 + 3*Power(t2,2)) - 
                  t1*(16 - 156*t2 + 43*Power(t2,2) + 15*Power(t2,3))) + 
               Power(s2,2)*(-99 + 10*t2 + 139*Power(t2,2) + 
                  61*Power(t2,3) + 40*Power(t2,4) - 10*Power(t2,5) + 
                  Power(t1,3)*(-5 - 14*t2 + 6*Power(t2,2)) - 
                  Power(t1,2)*
                   (97 + 22*t2 - 63*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*(201 + 170*t2 - 258*Power(t2,2) - 161*Power(t2,3) + 
                     10*Power(t2,4))) + 
               s2*(37 + Power(t1,4)*(8 - 12*t2) + 13*t2 + 
                  28*Power(t2,2) + 217*Power(t2,3) + 47*Power(t2,4) - 
                  33*Power(t2,5) + 5*Power(t2,6) + 
                  Power(t1,3)*
                   (21 - 113*t2 + 82*Power(t2,2) - 5*Power(t2,3)) + 
                  Power(t1,2)*
                   (-85 + 259*t2 + 462*Power(t2,2) - 187*Power(t2,3) + 
                     Power(t2,4)) - 
                  t1*(-19 + 219*t2 + 452*Power(t2,2) + 265*Power(t2,3) - 
                     174*Power(t2,4) + Power(t2,5))))))*
       B1(1 - s1 - t1 + t2,1 - s + s1 - t2,1 - s + s2 - t1))/
     ((-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,2)) + 
    (16*(12*s2 + 80*Power(s2,2) + 105*Power(s2,3) + 12*Power(s2,4) - 
         32*Power(s2,5) - 4*Power(s2,6) + 3*Power(s2,7) - 12*t1 - 
         210*s2*t1 - 549*Power(s2,2)*t1 - 346*Power(s2,3)*t1 + 
         98*Power(s2,4)*t1 + 93*Power(s2,5)*t1 - 10*Power(s2,6)*t1 - 
         6*Power(s2,7)*t1 + 130*Power(t1,2) + 855*s2*Power(t1,2) + 
         1198*Power(s2,2)*Power(t1,2) + 195*Power(s2,3)*Power(t1,2) - 
         329*Power(s2,4)*Power(t1,2) - 46*Power(s2,5)*Power(t1,2) + 
         32*Power(s2,6)*Power(t1,2) + 3*Power(s2,7)*Power(t1,2) - 
         411*Power(t1,3) - 1442*s2*Power(t1,3) - 
         929*Power(s2,2)*Power(t1,3) + 405*Power(s2,3)*Power(t1,3) + 
         260*Power(s2,4)*Power(t1,3) - 59*Power(s2,5)*Power(t1,3) - 
         18*Power(s2,6)*Power(t1,3) + 578*Power(t1,4) + 
         1037*s2*Power(t1,4) - 119*Power(s2,2)*Power(t1,4) - 
         497*Power(s2,3)*Power(t1,4) + 15*Power(s2,4)*Power(t1,4) + 
         44*Power(s2,5)*Power(t1,4) - 369*Power(t1,5) - 
         96*s2*Power(t1,5) + 484*Power(s2,2)*Power(t1,5) + 
         99*Power(s2,3)*Power(t1,5) - 56*Power(s2,4)*Power(t1,5) + 
         50*Power(t1,6) - 248*s2*Power(t1,6) - 
         151*Power(s2,2)*Power(t1,6) + 39*Power(s2,3)*Power(t1,6) + 
         54*Power(t1,7) + 90*s2*Power(t1,7) - 
         14*Power(s2,2)*Power(t1,7) - 20*Power(t1,8) + 2*s2*Power(t1,8) + 
         Power(s1,8)*s2*(2 + 2*Power(s2,2) + s2*(5 - 4*t1) - 5*t1 + 
            2*Power(t1,2)) + 48*t2 + 187*s2*t2 + 213*Power(s2,2)*t2 + 
         156*Power(s2,3)*t2 + 111*Power(s2,4)*t2 - 7*Power(s2,5)*t2 - 
         36*Power(s2,6)*t2 + 3*Power(s2,7)*t2 + 3*Power(s2,8)*t2 - 
         333*t1*t2 - 910*s2*t1*t2 - 885*Power(s2,2)*t1*t2 - 
         568*Power(s2,3)*t1*t2 - 108*Power(s2,4)*t1*t2 + 
         155*Power(s2,5)*t1*t2 + 22*Power(s2,6)*t1*t2 - 
         23*Power(s2,7)*t1*t2 - 3*Power(s2,8)*t1*t2 + 
         841*Power(t1,2)*t2 + 1648*s2*Power(t1,2)*t2 + 
         1188*Power(s2,2)*Power(t1,2)*t2 + 
         291*Power(s2,3)*Power(t1,2)*t2 - 
         345*Power(s2,4)*Power(t1,2)*t2 - 
         151*Power(s2,5)*Power(t1,2)*t2 + 69*Power(s2,6)*Power(t1,2)*t2 + 
         20*Power(s2,7)*Power(t1,2)*t2 - 955*Power(t1,3)*t2 - 
         1112*s2*Power(t1,3)*t2 - 39*Power(s2,2)*Power(t1,3)*t2 + 
         739*Power(s2,3)*Power(t1,3)*t2 + 
         445*Power(s2,4)*Power(t1,3)*t2 - 77*Power(s2,5)*Power(t1,3)*t2 - 
         55*Power(s2,6)*Power(t1,3)*t2 + 365*Power(t1,4)*t2 - 
         373*s2*Power(t1,4)*t2 - 1204*Power(s2,2)*Power(t1,4)*t2 - 
         824*Power(s2,3)*Power(t1,4)*t2 - 38*Power(s2,4)*Power(t1,4)*t2 + 
         80*Power(s2,5)*Power(t1,4)*t2 + 242*Power(t1,5)*t2 + 
         1004*s2*Power(t1,5)*t2 + 904*Power(s2,2)*Power(t1,5)*t2 + 
         178*Power(s2,3)*Power(t1,5)*t2 - 65*Power(s2,4)*Power(t1,5)*t2 - 
         313*Power(t1,6)*t2 - 514*s2*Power(t1,6)*t2 - 
         172*Power(s2,2)*Power(t1,6)*t2 + 28*Power(s2,3)*Power(t1,6)*t2 + 
         115*Power(t1,7)*t2 + 70*s2*Power(t1,7)*t2 - 
         5*Power(s2,2)*Power(t1,7)*t2 - 10*Power(t1,8)*t2 + 
         53*Power(t2,2) + 158*s2*Power(t2,2) + 
         278*Power(s2,2)*Power(t2,2) + 184*Power(s2,3)*Power(t2,2) + 
         10*Power(s2,4)*Power(t2,2) + 9*Power(s2,5)*Power(t2,2) - 
         2*Power(s2,6)*Power(t2,2) - 4*Power(s2,7)*Power(t2,2) + 
         7*Power(s2,8)*Power(t2,2) - 144*t1*Power(t2,2) - 
         452*s2*t1*Power(t2,2) - 606*Power(s2,2)*t1*Power(t2,2) - 
         69*Power(s2,3)*t1*Power(t2,2) + 176*Power(s2,4)*t1*Power(t2,2) + 
         90*Power(s2,5)*t1*Power(t2,2) + 7*Power(s2,6)*t1*Power(t2,2) - 
         43*Power(s2,7)*t1*Power(t2,2) - 2*Power(s2,8)*t1*Power(t2,2) - 
         36*Power(t1,2)*Power(t2,2) + 63*s2*Power(t1,2)*Power(t2,2) - 
         403*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         1040*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         638*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         109*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         98*Power(s2,6)*Power(t1,2)*Power(t2,2) + 
         11*Power(s2,7)*Power(t1,2)*Power(t2,2) + 
         511*Power(t1,3)*Power(t2,2) + 1260*s2*Power(t1,3)*Power(t2,2) + 
         2229*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         1756*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         525*Power(s2,4)*Power(t1,3)*Power(t2,2) - 
         87*Power(s2,5)*Power(t1,3)*Power(t2,2) - 
         24*Power(s2,6)*Power(t1,3)*Power(t2,2) - 
         793*Power(t1,4)*Power(t2,2) - 2036*s2*Power(t1,4)*Power(t2,2) - 
         2232*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         964*Power(s2,3)*Power(t1,4)*Power(t2,2) - 
         18*Power(s2,4)*Power(t1,4)*Power(t2,2) + 
         26*Power(s2,5)*Power(t1,4)*Power(t2,2) + 
         648*Power(t1,5)*Power(t2,2) + 1307*s2*Power(t1,5)*Power(t2,2) + 
         820*Power(s2,2)*Power(t1,5)*Power(t2,2) + 
         94*Power(s2,3)*Power(t1,5)*Power(t2,2) - 
         14*Power(s2,4)*Power(t1,5)*Power(t2,2) - 
         281*Power(t1,6)*Power(t2,2) - 317*s2*Power(t1,6)*Power(t2,2) - 
         67*Power(s2,2)*Power(t1,6)*Power(t2,2) + 
         3*Power(s2,3)*Power(t1,6)*Power(t2,2) + 
         42*Power(t1,7)*Power(t2,2) + 16*s2*Power(t1,7)*Power(t2,2) - 
         52*Power(t2,3) - 72*s2*Power(t2,3) - 
         25*Power(s2,2)*Power(t2,3) - 20*Power(s2,3)*Power(t2,3) - 
         66*Power(s2,4)*Power(t2,3) - 68*Power(s2,5)*Power(t2,3) + 
         3*Power(s2,6)*Power(t2,3) + 15*Power(s2,7)*Power(t2,3) + 
         283*t1*Power(t2,3) + 508*s2*t1*Power(t2,3) + 
         692*Power(s2,2)*t1*Power(t2,3) + 
         756*Power(s2,3)*t1*Power(t2,3) + 
         546*Power(s2,4)*t1*Power(t2,3) + 
         148*Power(s2,5)*t1*Power(t2,3) - 39*Power(s2,6)*t1*Power(t2,3) - 
         4*Power(s2,7)*t1*Power(t2,3) - 543*Power(t1,2)*Power(t2,3) - 
         1466*s2*Power(t1,2)*Power(t2,3) - 
         2224*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         1831*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         835*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         55*Power(s2,5)*Power(t1,2)*Power(t2,3) + 
         13*Power(s2,6)*Power(t1,2)*Power(t2,3) + 
         652*Power(t1,3)*Power(t2,3) + 2199*s2*Power(t1,3)*Power(t2,3) + 
         2662*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         1640*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         305*Power(s2,4)*Power(t1,3)*Power(t2,3) - 
         9*Power(s2,5)*Power(t1,3)*Power(t2,3) - 
         633*Power(t1,4)*Power(t2,3) - 1655*s2*Power(t1,4)*Power(t2,3) - 
         1438*Power(s2,2)*Power(t1,4)*Power(t2,3) - 
         420*Power(s2,3)*Power(t1,4)*Power(t2,3) - 
         11*Power(s2,4)*Power(t1,4)*Power(t2,3) + 
         357*Power(t1,5)*Power(t2,3) + 543*s2*Power(t1,5)*Power(t2,3) + 
         247*Power(s2,2)*Power(t1,5)*Power(t2,3) + 
         17*Power(s2,3)*Power(t1,5)*Power(t2,3) - 
         61*Power(t1,6)*Power(t2,3) - 50*s2*Power(t1,6)*Power(t2,3) - 
         6*Power(s2,2)*Power(t1,6)*Power(t2,3) - 
         3*Power(t1,7)*Power(t2,3) - 26*Power(t2,4) - 
         126*s2*Power(t2,4) - 272*Power(s2,2)*Power(t2,4) - 
         252*Power(s2,3)*Power(t2,4) - 129*Power(s2,4)*Power(t2,4) - 
         54*Power(s2,5)*Power(t2,4) - 11*Power(s2,6)*Power(t2,4) + 
         Power(s2,7)*Power(t2,4) + 48*t1*Power(t2,4) + 
         592*s2*t1*Power(t2,4) + 1063*Power(s2,2)*t1*Power(t2,4) + 
         866*Power(s2,3)*t1*Power(t2,4) + 
         510*Power(s2,4)*t1*Power(t2,4) + 
         123*Power(s2,5)*t1*Power(t2,4) - Power(s2,6)*t1*Power(t2,4) - 
         157*Power(t1,2)*Power(t2,4) - 1130*s2*Power(t1,2)*Power(t2,4) - 
         1565*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         1276*Power(s2,3)*Power(t1,2)*Power(t2,4) - 
         390*Power(s2,4)*Power(t1,2)*Power(t2,4) - 
         17*Power(s2,5)*Power(t1,2)*Power(t2,4) + 
         347*Power(t1,3)*Power(t2,4) + 1049*s2*Power(t1,3)*Power(t2,4) + 
         1251*Power(s2,2)*Power(t1,3)*Power(t2,4) + 
         529*Power(s2,3)*Power(t1,3)*Power(t2,4) + 
         47*Power(s2,4)*Power(t1,3)*Power(t2,4) - 
         248*Power(t1,4)*Power(t2,4) - 460*s2*Power(t1,4)*Power(t2,4) - 
         315*Power(s2,2)*Power(t1,4)*Power(t2,4) - 
         41*Power(s2,3)*Power(t1,4)*Power(t2,4) + 
         25*Power(t1,5)*Power(t2,4) + 53*s2*Power(t1,5)*Power(t2,4) + 
         8*Power(s2,2)*Power(t1,5)*Power(t2,4) + 
         11*Power(t1,6)*Power(t2,4) + 3*s2*Power(t1,6)*Power(t2,4) + 
         12*Power(t2,5) - 56*s2*Power(t2,5) - 
         127*Power(s2,2)*Power(t2,5) - 152*Power(s2,3)*Power(t2,5) - 
         125*Power(s2,4)*Power(t2,5) - 32*Power(s2,5)*Power(t2,5) + 
         39*t1*Power(t2,5) + 193*s2*t1*Power(t2,5) + 
         373*Power(s2,2)*t1*Power(t2,5) + 
         478*Power(s2,3)*t1*Power(t2,5) + 
         160*Power(s2,4)*t1*Power(t2,5) + 10*Power(s2,5)*t1*Power(t2,5) - 
         139*Power(t1,2)*Power(t2,5) - 298*s2*Power(t1,2)*Power(t2,5) - 
         576*Power(s2,2)*Power(t1,2)*Power(t2,5) - 
         267*Power(s2,3)*Power(t1,2)*Power(t2,5) - 
         30*Power(s2,4)*Power(t1,2)*Power(t2,5) + 
         79*Power(t1,3)*Power(t2,5) + 208*s2*Power(t1,3)*Power(t2,5) + 
         165*Power(s2,2)*Power(t1,3)*Power(t2,5) + 
         20*Power(s2,3)*Power(t1,3)*Power(t2,5) + 
         24*Power(t1,4)*Power(t2,5) - 10*s2*Power(t1,4)*Power(t2,5) + 
         11*Power(s2,2)*Power(t1,4)*Power(t2,5) - 
         15*Power(t1,5)*Power(t2,5) - 11*s2*Power(t1,5)*Power(t2,5) - 
         15*Power(t2,6) - 3*s2*Power(t2,6) - 21*Power(s2,2)*Power(t2,6) - 
         55*Power(s2,3)*Power(t2,6) - 17*Power(s2,4)*Power(t2,6) - 
         2*Power(s2,5)*Power(t2,6) + 26*t1*Power(t2,6) + 
         44*s2*t1*Power(t2,6) + 131*Power(s2,2)*t1*Power(t2,6) + 
         42*Power(s2,3)*t1*Power(t2,6) + 6*Power(s2,4)*t1*Power(t2,6) + 
         9*Power(t1,2)*Power(t2,6) - 50*s2*Power(t1,2)*Power(t2,6) - 
         12*Power(s2,2)*Power(t1,2)*Power(t2,6) + 
         6*Power(s2,3)*Power(t1,2)*Power(t2,6) - 
         29*Power(t1,3)*Power(t2,6) - 25*s2*Power(t1,3)*Power(t2,6) - 
         25*Power(s2,2)*Power(t1,3)*Power(t2,6) + 
         9*Power(t1,4)*Power(t2,6) + 15*s2*Power(t1,4)*Power(t2,6) + 
         4*Power(t2,7) - 7*s2*Power(t2,7) - 15*Power(s2,2)*Power(t2,7) + 
         Power(s2,3)*Power(t2,7) - 11*t1*Power(t2,7) + 
         s2*t1*Power(t2,7) - 18*Power(s2,2)*t1*Power(t2,7) - 
         6*Power(s2,3)*t1*Power(t2,7) + 9*Power(t1,2)*Power(t2,7) + 
         22*s2*Power(t1,2)*Power(t2,7) + 
         15*Power(s2,2)*Power(t1,2)*Power(t2,7) - 
         2*Power(t1,3)*Power(t2,7) - 9*s2*Power(t1,3)*Power(t2,7) + 
         3*s2*Power(t2,8) + 5*Power(s2,2)*Power(t2,8) + 
         Power(s2,3)*Power(t2,8) - 6*s2*t1*Power(t2,8) - 
         3*Power(s2,2)*t1*Power(t2,8) + 2*s2*Power(t1,2)*Power(t2,8) + 
         Power(s1,7)*(-2 + 7*t1 - 8*Power(t1,2) + 4*Power(t1,3) - 
            Power(t1,4) + Power(s2,3)*(3 + 11*t1 - 15*t2) + 
            s2*(4 + t1 + 13*Power(t1,3) - 17*t2 + 41*t1*t2 - 
               Power(t1,2)*(25 + 16*t2)) + 
            Power(s2,2)*(14 - 23*Power(t1,2) - 40*t2 + t1*(18 + 31*t2))) \
+ Power(s,7)*(-6 - Power(s1,3)*(-1 + s2) + Power(t1,2) + Power(t1,3) + 
            2*Power(t1,4) - 2*t1*t2 - 3*Power(t1,2)*t2 - 
            8*Power(t1,3)*t2 + Power(t2,2) + 3*t1*Power(t2,2) + 
            12*Power(t1,2)*Power(t2,2) - Power(t2,3) - 
            8*t1*Power(t2,3) + 2*Power(t2,4) + 
            Power(s2,3)*(1 - t1 + t2) + 
            Power(s2,2)*(-7 + t1 + 4*Power(t1,2) - t2 - 8*t1*t2 + 
               4*Power(t2,2)) + 
            Power(s1,2)*(-7 - 4*Power(s2,2) + t1 + 2*Power(t1,2) - t2 - 
               4*t1*t2 + 2*Power(t2,2) + s2*(11 - t1 + t2)) + 
            s2*(12 - 5*Power(t1,3) + 3*t1*(2 - 5*t2)*t2 - 
               3*Power(t2,2) + 5*Power(t2,3) + 3*Power(t1,2)*(-1 + 5*t2)\
) + s1*(12 - Power(s2,3) + 4*Power(t1,3) + Power(t1,2)*(1 - 12*t2) + 
               Power(s2,2)*(11 + 2*t1 - 2*t2) + Power(t2,2) - 
               4*Power(t2,3) + 2*t1*t2*(-1 + 6*t2) - 
               s2*(22 + 5*Power(t1,2) + t1*(2 - 10*t2) - 2*t2 + 
                  5*Power(t2,2)))) + 
         Power(s1,6)*(Power(s2,5) + Power(s2,4)*(-9*t1 + t2) + 
            Power(s2,3)*(-50 + 38*Power(t1,2) + t1*(4 - 75*t2) - 5*t2 + 
               49*Power(t2,2)) - 
            (-1 + t1)*(-9 + Power(t1,4) + t1*(8 - 34*t2) + 15*t2 - 
               Power(t1,3)*(11 + 5*t2) + Power(t1,2)*(18 + 17*t2)) + 
            Power(s2,2)*(-61 - 54*Power(t1,3) - 79*t2 + 
               140*Power(t2,2) + 5*Power(t1,2)*(6 + 31*t2) + 
               t1*(134 - 148*t2 - 105*Power(t2,2))) + 
            s2*(-28 + 25*Power(t1,4) - 23*t2 + 63*Power(t2,2) - 
               2*Power(t1,3)*(23 + 43*t2) + 
               t1*(78 - 20*t2 - 147*Power(t2,2)) + 
               2*Power(t1,2)*(-24 + 89*t2 + 28*Power(t2,2)))) + 
         Power(s1,5)*(7*Power(s2,6) + Power(s2,5)*(-6 - 33*t1 + 4*t2) + 
            Power(s2,4)*(-35 + 46*Power(t1,2) + 34*t2 - 5*Power(t2,2) + 
               t1*(13 + 18*t2)) - 
            Power(s2,3)*(74 + Power(t1,3) - 320*t2 + 21*Power(t2,2) + 
               91*Power(t2,3) + Power(t1,2)*(5 + 178*t2) + 
               t1*(26 + 74*t2 - 216*Power(t2,2))) + 
            (-1 + t1)*(-12 + Power(t1,5) - 49*t2 + 49*Power(t2,2) + 
               Power(t1,4)*(19 + 4*t2) + 
               t1*(78 + 28*t2 - 102*Power(t2,2)) - 
               Power(t1,3)*(17 + 63*t2 + 10*Power(t2,2)) + 
               2*Power(t1,2)*(-44 + 61*t2 + 21*Power(t2,2))) + 
            Power(s2,2)*(-12 - 38*Power(t1,4) + 322*t2 + 
               200*Power(t2,2) - 280*Power(t2,3) + 
               Power(t1,3)*(25 + 293*t2) + 
               Power(t1,2)*(195 - 171*t2 - 445*Power(t2,2)) + 
               t1*(-44 - 762*t2 + 488*Power(t2,2) + 203*Power(t2,3))) + 
            s2*(52 + 18*Power(t1,5) + 123*t2 + 62*Power(t2,2) - 
               133*Power(t2,3) - 3*Power(t1,4)*(15 + 47*t2) + 
               Power(t1,3)*(-97 + 281*t2 + 244*Power(t2,2)) + 
               Power(t1,2)*(213 + 221*t2 - 537*Power(t2,2) - 
                  112*Power(t2,3)) + 
               t1*(-167 - 370*t2 + 84*Power(t2,2) + 301*Power(t2,3)))) + 
         Power(s1,4)*(9*Power(s2,7) + Power(s2,6)*(9 - 32*t1 - 17*t2) + 
            Power(s2,5)*(8 + 33*Power(t1,2) + 29*t2 - 28*Power(t2,2) + 
               t1*(-33 + 106*t2)) + 
            Power(s2,4)*(199 - 7*Power(t1,3) + 
               Power(t1,2)*(21 - 170*t2) + 29*t2 - 153*Power(t2,2) + 
               10*Power(t2,3) + t1*(-85 + 23*t2 + 24*Power(t2,2))) + 
            (-1 + t1)*(-6 + Power(t1,6) - 3*Power(t1,5)*(-7 + t2) + 
               35*t2 + 121*Power(t2,2) - 90*Power(t2,3) - 
               2*Power(t1,4)*(-1 + 50*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(82 + 376*t2 - 328*Power(t2,2) - 
                  60*Power(t2,3)) + 
               Power(t1,3)*(-159 + 120*t2 + 151*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(33 - 333*t2 - 43*Power(t2,2) + 175*Power(t2,3))) + 
            Power(s2,2)*(370 - 22*Power(t1,5) - 155*t2 - 
               699*Power(t2,2) - 305*Power(t2,3) + 350*Power(t2,4) + 
               Power(t1,4)*(-29 + 185*t2) + 
               Power(t1,3)*(95 + 2*t2 - 657*Power(t2,2)) + 
               Power(t1,2)*(680 - 1381*t2 + 372*Power(t2,2) + 
                  705*Power(t2,3)) + 
               t1*(-939 + 691*t2 + 1839*Power(t2,2) - 850*Power(t2,3) - 
                  245*Power(t2,4))) + 
            Power(s2,3)*(9*Power(t1,4) + Power(t1,3)*(37 - 8*t2) + 
               Power(t1,2)*(75 - 172*t2 + 338*Power(t2,2)) + 
               t1*(-763 + 618*t2 + 298*Power(t2,2) - 340*Power(t2,3)) + 
               5*(88 + 16*t2 - 167*Power(t2,2) + 15*Power(t2,3) + 
                  21*Power(t2,4))) + 
            s2*(134 + 9*Power(t1,6) - 287*t2 - 215*Power(t2,2) - 
               105*Power(t2,3) + 175*Power(t2,4) - 
               Power(t1,5)*(25 + 93*t2) + 
               Power(t1,4)*(-75 + 216*t2 + 329*Power(t2,2)) + 
               Power(t1,3)*(52 + 514*t2 - 689*Power(t2,2) - 
                  385*Power(t2,3)) + 
               t1*(-352 + 887*t2 + 744*Power(t2,2) - 155*Power(t2,3) - 
                  385*Power(t2,4)) + 
               Power(t1,2)*(238 - 1107*t2 - 454*Power(t2,2) + 
                  890*Power(t2,3) + 140*Power(t2,4)))) + 
         Power(s1,3)*(5*Power(s2,8) + Power(s2,7)*(11 - 20*t1 - 23*t2) + 
            Power(s2,6)*(-39 + 33*Power(t1,2) - 5*t2 + 9*Power(t2,2) + 
               4*t1*(-5 + 21*t2)) - 
            Power(s2,5)*(125 + 31*Power(t1,3) + 12*t2 + 
               19*Power(t2,2) - 52*Power(t2,3) + 
               4*Power(t1,2)*(9 + 19*t2) + 
               2*t1*(-116 - 2*t2 + 65*Power(t2,2))) + 
            Power(s2,4)*(-317 + 15*Power(t1,4) + 
               Power(t1,3)*(111 - 14*t2) - 588*t2 + 248*Power(t2,2) + 
               272*Power(t2,3) - 10*Power(t2,4) + 
               Power(t1,2)*(-535 + 173*t2 + 264*Power(t2,2)) + 
               t1*(786 + 9*t2 - 307*Power(t2,2) - 96*Power(t2,3))) - 
            (-1 + t1)*(-72 + 2*Power(t1,6)*(-8 + t2) + 31*t2 + 
               21*Power(t2,2) + 174*Power(t2,3) - 100*Power(t2,4) + 
               Power(t1,5)*(-15 + 83*t2 - 3*Power(t2,2)) + 
               Power(t1,4)*(98 + 6*t2 - 201*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(t1,2)*(-459 + 429*t2 + 688*Power(t2,2) - 
                  452*Power(t2,3) - 55*Power(t2,4)) + 
               Power(t1,3)*(203 - 663*t2 + 267*Power(t2,2) + 
                  194*Power(t2,3) + 5*Power(t2,4)) + 
               t1*(280 + 8*t2 - 582*Power(t2,2) - 52*Power(t2,3) + 
                  185*Power(t2,4))) + 
            Power(s2,3)*(-659 + 6*Power(t1,5) - 1139*t2 + 
               356*Power(t2,2) + 1140*Power(t2,3) - 95*Power(t2,4) - 
               77*Power(t2,5) - 3*Power(t1,4)*(31 + 2*t2) + 
               Power(t1,3)*(545 - 437*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(-1683 + 603*t2 + 813*Power(t2,2) - 
                  332*Power(t2,3)) + 
               t1*(1732 + 1762*t2 - 2176*Power(t2,2) - 
                  532*Power(t2,3) + 315*Power(t2,4))) + 
            Power(s2,2)*(-764 - 13*Power(t1,6) - 790*t2 + 
               664*Power(t2,2) + 796*Power(t2,3) + 310*Power(t2,4) - 
               280*Power(t2,5) + Power(t1,5)*(47 + 74*t2) + 
               Power(t1,4)*(-211 + 277*t2 - 338*Power(t2,2)) + 
               Power(t1,3)*(1367 - 1216*t2 - 321*Power(t2,2) + 
                  778*Power(t2,3)) - 
               Power(t1,2)*(2763 + 747*t2 - 3549*Power(t2,2) + 
                  378*Power(t2,3) + 665*Power(t2,4)) + 
               t1*(2426 + 1767*t2 - 2182*Power(t2,2) - 
                  2416*Power(t2,3) + 850*Power(t2,4) + 189*Power(t2,5))) \
+ s2*(-409 + 5*Power(t1,7) - 206*t2 + 605*Power(t2,2) + 
               190*Power(t2,3) + 120*Power(t2,4) - 147*Power(t2,5) - 
               Power(t1,6)*(36 + 37*t2) + 
               Power(t1,5)*(9 + 69*t2 + 182*Power(t2,2)) - 
               Power(t1,4)*(233 - 544*t2 + 368*Power(t2,2) + 
                  406*Power(t2,3)) + 
               Power(t1,3)*(1433 - 1091*t2 - 1168*Power(t2,2) + 
                  866*Power(t2,3) + 365*Power(t2,4)) + 
               Power(t1,2)*(-2435 + 528*t2 + 2341*Power(t2,2) + 
                  566*Power(t2,3) - 875*Power(t2,4) - 112*Power(t2,5)) + 
               t1*(1659 + 269*t2 - 1852*Power(t2,2) - 836*Power(t2,3) + 
                  145*Power(t2,4) + 315*Power(t2,5)))) + 
         Power(s1,2)*(Power(s2,8)*(2 + 3*t1 - 10*t2) + 
            Power(s2,7)*(-35 - 17*Power(t1,2) - 12*t2 + 
               20*Power(t2,2) + t1*(16 + 41*t2)) + 
            Power(s2,6)*(-35 + 42*Power(t1,3) + 56*t2 - 
               28*Power(t2,2) + 5*Power(t2,3) - 
               Power(t1,2)*(94 + 75*t2) + 
               t1*(166 + 48*t2 - 73*Power(t2,2))) + 
            Power(s2,5)*(160 - 58*Power(t1,4) + 157*t2 - 
               54*Power(t2,2) - 57*Power(t2,3) - 43*Power(t2,4) + 
               Power(t1,3)*(177 + 94*t2) + 
               Power(t1,2)*(-267 - 100*t2 + 36*Power(t2,2)) + 
               t1*(-83 - 215*t2 + 214*Power(t2,2) + 84*Power(t2,3))) + 
            Power(s2,4)*(364 + 47*Power(t1,5) + 643*t2 + 
               450*Power(t2,2) - 568*Power(t2,3) - 238*Power(t2,4) + 
               5*Power(t2,5) - Power(t1,4)*(159 + 83*t2) + 
               2*Power(t1,3)*(63 + 103*t2 + 48*Power(t2,2)) + 
               Power(t1,2)*(788 + 184*t2 - 799*Power(t2,2) - 
                  232*Power(t2,3)) + 
               t1*(-1125 - 1131*t2 + 747*Power(t2,2) + 
                  653*Power(t2,3) + 99*Power(t2,4))) + 
            (-1 + t1)*(-113 + 6*Power(t1,7) - 80*t2 + 106*Power(t2,2) - 
               27*Power(t2,3) + 151*Power(t2,4) - 67*Power(t2,5) + 
               Power(t1,6)*(5 - 35*t2 + Power(t2,2)) - 
               Power(t1,5)*(-11 + 81*t2 - 114*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(t1,4)*(368 - 456*t2 - 42*Power(t2,2) + 
                  193*Power(t2,3) + Power(t2,4)) + 
               Power(t1,3)*(793 + 78*t2 - 1061*Power(t2,2) + 
                  251*Power(t2,3) + 141*Power(t2,4) + Power(t2,5)) - 
               Power(t1,2)*(804 + 571*t2 - 747*Power(t2,2) - 
                  688*Power(t2,3) + 338*Power(t2,4) + 33*Power(t2,5)) + 
               t1*(463 + 290*t2 - 105*Power(t2,2) - 528*Power(t2,3) - 
                  58*Power(t2,4) + 120*Power(t2,5))) + 
            Power(s2,3)*(555 - 21*Power(t1,6) + 1459*t2 + 
               706*Power(t2,2) - 808*Power(t2,3) - 860*Power(t2,4) + 
               57*Power(t2,5) + 35*Power(t2,6) + 
               Power(t1,5)*(67 + 30*t2) + 
               Power(t1,4)*(88 - 294*t2 - 56*Power(t2,2)) + 
               2*Power(t1,3)*
                (-772 + 185*t2 + 646*Power(t2,2) + 14*Power(t2,3)) + 
               Power(t1,2)*(2972 + 2141*t2 - 2707*Power(t2,2) - 
                  1357*Power(t2,3) + 178*Power(t2,4)) - 
               t1*(2153 + 3260*t2 + 369*Power(t2,2) - 
                  3080*Power(t2,3) - 488*Power(t2,4) + 171*Power(t2,5))) \
+ Power(s2,2)*(641 + 4*Power(t1,7) + 1583*t2 + 198*Power(t2,2) - 
               942*Power(t2,3) - 499*Power(t2,4) - 209*Power(t2,5) + 
               140*Power(t2,6) + Power(t1,6)*(1 + 12*t2) + 
               Power(t1,5)*(-107 + 167*t2 - 74*Power(t2,2)) + 
               Power(t1,4)*(1238 - 771*t2 - 782*Power(t2,2) + 
                  284*Power(t2,3)) + 
               Power(t1,3)*(-3611 - 894*t2 + 3398*Power(t2,2) + 
                  701*Power(t2,3) - 512*Power(t2,4)) + 
               Power(t1,2)*(4565 + 4278*t2 - 2111*Power(t2,2) - 
                  4311*Power(t2,3) + 162*Power(t2,4) + 373*Power(t2,5)) \
+ t1*(-2712 - 4645*t2 + 346*Power(t2,2) + 2840*Power(t2,3) + 
                  1824*Power(t2,4) - 488*Power(t2,5) - 91*Power(t2,6))) \
- s2*(-406 - 733*t2 + 116*Power(t2,2) + 613*Power(t2,3) + 
               90*Power(t2,4) + 89*Power(t2,5) - 77*Power(t2,6) + 
               Power(t1,7)*(16 + 9*t2) - 
               10*Power(t1,6)*(3 + 2*t2 + 5*Power(t2,2)) + 
               2*Power(t1,5)*
                (185 - 211*t2 + 5*Power(t2,2) + 84*Power(t2,3)) + 
               Power(t1,4)*(-1977 + 799*t2 + 1323*Power(t2,2) - 
                  258*Power(t2,3) - 279*Power(t2,4)) + 
               Power(t1,3)*(3952 + 1207*t2 - 3075*Power(t2,2) - 
                  1390*Power(t2,3) + 584*Power(t2,4) + 208*Power(t2,5)) \
+ Power(t1,2)*(-3858 - 3717*t2 + 2900*Power(t2,2) + 2511*Power(t2,3) + 
                  464*Power(t2,4) - 510*Power(t2,5) - 56*Power(t2,6)) + 
               t1*(1934 + 2856*t2 - 1110*Power(t2,2) - 
                  1904*Power(t2,3) - 574*Power(t2,4) + 66*Power(t2,5) + 
                  161*Power(t2,6)))) + 
         s1*(-(Power(s2,8)*(3 + t1*(-3 + t2) + 9*t2 - 5*Power(t2,2))) + 
            Power(s2,7)*(2 + 39*t2 - 14*Power(t2,2) - 7*Power(t2,3) + 
               3*Power(t1,2)*(-5 + 2*t2) + 
               t1*(13 + 27*t2 - 17*Power(t2,2))) + 
            Power(s2,6)*(62 + 44*t2 - 20*Power(t2,2) + 35*Power(t2,3) - 
               4*Power(t2,4) - 9*Power(t1,3)*(-3 + 2*t2) + 
               Power(t1,2)*(13 + 3*t2 + 29*Power(t2,2)) + 
               t1*(-102 - 187*t2 + 11*Power(t2,2) + 22*Power(t2,3))) + 
            Power(s2,5)*(12 - 148*t2 + 36*Power(t2,2) + 
               112*Power(t2,3) + 85*Power(t2,4) + 16*Power(t2,5) + 
               2*Power(t1,4)*(-7 + 16*t2) - 
               Power(t1,3)*(159 + 121*t2 + 54*Power(t2,2)) + 
               Power(t1,2)*(430 + 459*t2 + 191*Power(t2,2) + 
                  24*Power(t2,3)) - 
               t1*(269 + 80*t2 + 165*Power(t2,2) + 308*Power(t2,3) + 
                  37*Power(t2,4))) - 
            Power(s2,4)*(235 + 369*t2 + 260*Power(t2,2) - 
               68*Power(t2,3) - 451*Power(t2,4) - 102*Power(t2,5) + 
               Power(t2,6) + Power(t1,5)*(19 + 33*t2) - 
               Power(t1,4)*(349 + 235*t2 + 79*Power(t2,2)) + 
               2*Power(t1,3)*
                (359 + 413*t2 + 311*Power(t2,2) + 61*Power(t2,3)) - 
               Power(t1,2)*(170 + 31*t2 + 1186*Power(t2,2) + 
                  995*Power(t2,3) + 122*Power(t2,4)) + 
               t1*(-453 - 880*t2 + 201*Power(t2,2) + 1181*Power(t2,3) + 
                  542*Power(t2,4) + 42*Power(t2,5))) - 
            (-1 + t1)*(-48 - 166*t2 + 44*Power(t2,2) + 95*Power(t2,3) - 
               37*Power(t2,4) + 73*Power(t2,5) - 25*Power(t2,6) + 
               Power(t1,7)*(-2 + 4*t2) + 
               Power(t1,6)*(-8 + 48*t2 - 22*Power(t2,2)) + 
               Power(t1,5)*(172 - 193*t2 - 130*Power(t2,2) + 
                  63*Power(t2,3)) + 
               Power(t1,4)*(-499 - 74*t2 + 651*Power(t2,2) + 
                  74*Power(t2,3) - 88*Power(t2,4)) + 
               Power(t1,3)*(725 + 564*t2 - 465*Power(t2,2) - 
                  769*Power(t2,3) + 96*Power(t2,4) + 55*Power(t2,5)) + 
               Power(t1,2)*(-612 - 775*t2 + 200*Power(t2,2) + 
                  535*Power(t2,3) + 376*Power(t2,4) - 
                  130*Power(t2,5) - 12*Power(t2,6)) + 
               t1*(273 + 578*t2 - 221*Power(t2,2) - 102*Power(t2,3) - 
                  252*Power(t2,4) - 40*Power(t2,5) + 44*Power(t2,6))) + 
            Power(s2,3)*(-328 - 788*t2 - 780*Power(t2,2) + 
               245*Power(t2,3) + 598*Power(t2,4) + 340*Power(t2,5) - 
               15*Power(t2,6) - 9*Power(t2,7) + 
               3*Power(t1,6)*(11 + 6*t2) - 
               Power(t1,5)*(357 + 221*t2 + 53*Power(t2,2)) + 
               Power(t1,4)*(559 + 1050*t2 + 807*Power(t2,2) + 
                  94*Power(t2,3)) - 
               Power(t1,3)*(-707 + 331*t2 + 2555*Power(t2,2) + 
                  1421*Power(t2,3) + 49*Power(t2,4)) + 
               Power(t1,2)*(-2149 - 2025*t2 + 1373*Power(t2,2) + 
                  3305*Power(t2,3) + 988*Power(t2,4) - 50*Power(t2,5)) \
+ t1*(1535 + 2369*t2 + 772*Power(t2,2) - 1496*Power(t2,3) - 
                  1974*Power(t2,4) - 226*Power(t2,5) + 50*Power(t2,6))) \
+ Power(s2,2)*(-265 - 983*t2 - 794*Power(t2,2) + 494*Power(t2,3) + 
               572*Power(t2,4) + 162*Power(t2,5) + 84*Power(t2,6) - 
               40*Power(t2,7) - Power(t1,7)*(19 + 4*t2) + 
               Power(t1,6)*(183 + 103*t2 + 7*Power(t2,2)) + 
               Power(t1,5)*(-180 - 801*t2 - 461*Power(t2,2) + 
                  14*Power(t2,3)) + 
               Power(t1,4)*(-1338 + 914*t2 + 2420*Power(t2,2) + 
                  849*Power(t2,3) - 104*Power(t2,4)) + 
               Power(t1,3)*(3609 + 1849*t2 - 3135*Power(t2,2) - 
                  3528*Power(t2,3) - 572*Power(t2,4) + 177*Power(t2,5)) \
+ Power(t1,2)*(-3561 - 4753*t2 + 709*Power(t2,2) + 3743*Power(t2,3) + 
                  2524*Power(t2,4) - 3*Power(t2,5) - 115*Power(t2,6)) + 
               t1*(1571 + 3637*t2 + 1527*Power(t2,2) - 
                  2237*Power(t2,3) - 1678*Power(t2,4) - 
                  750*Power(t2,5) + 148*Power(t2,6) + 25*Power(t2,7))) + 
            s2*(-175 + 4*Power(t1,8) - 588*t2 - 252*Power(t2,2) + 
               314*Power(t2,3) + 299*Power(t2,4) + 23*Power(t2,5) + 
               38*Power(t2,6) - 23*Power(t2,7) + 
               Power(t1,7)*(-41 - 13*t2 + 4*Power(t2,2)) + 
               Power(t1,6)*(3 + 310*t2 + 66*Power(t2,2) - 
                  25*Power(t2,3)) + 
               Power(t1,5)*(848 - 819*t2 - 974*Power(t2,2) - 
                  87*Power(t2,3) + 72*Power(t2,4)) - 
               Power(t1,4)*(2602 + 417*t2 - 2687*Power(t2,2) - 
                  1314*Power(t2,3) + 51*Power(t2,4) + 101*Power(t2,5)) + 
               Power(t1,3)*(3501 + 3401*t2 - 2425*Power(t2,2) - 
                  3085*Power(t2,3) - 847*Power(t2,4) + 
                  197*Power(t2,5) + 66*Power(t2,6)) + 
               Power(t1,2)*(-2544 - 4444*t2 + 184*Power(t2,2) + 
                  3264*Power(t2,3) + 1362*Power(t2,4) + 
                  229*Power(t2,5) - 163*Power(t2,6) - 16*Power(t2,7)) + 
               t1*(1006 + 2572*t2 + 689*Power(t2,2) - 1619*Power(t2,3) - 
                  965*Power(t2,4) - 234*Power(t2,5) + 10*Power(t2,6) + 
                  47*Power(t2,7)))) + 
         Power(s,6)*(18 + Power(s1,4)*(-4 + 5*s2) - 7*t1 - 
            18*Power(t1,2) - 2*Power(t1,3) + 5*Power(t1,5) + 
            5*Power(s2,4)*(-1 + t1 - t2) - 35*t2 + 32*t1*t2 + 
            15*Power(t1,2)*t2 + 9*Power(t1,3)*t2 - 15*Power(t1,4)*t2 - 
            14*Power(t2,2) - 24*t1*Power(t2,2) - 
            27*Power(t1,2)*Power(t2,2) + 10*Power(t1,3)*Power(t2,2) + 
            11*Power(t2,3) + 27*t1*Power(t2,3) + 
            10*Power(t1,2)*Power(t2,3) - 9*Power(t2,4) - 
            15*t1*Power(t2,4) + 5*Power(t2,5) + 
            Power(s1,3)*(23 + 18*Power(s2,2) - 9*Power(t1,2) + 
               s2*(-45 + 6*t1 - 11*t2) + 13*t2 - 5*Power(t2,2) + 
               2*t1*(-4 + 7*t2)) + 
            Power(s2,3)*(21 - 22*Power(t1,2) - 17*Power(t2,2) + 
               t1*(9 + 39*t2)) + 
            Power(s1,2)*(-36 + 26*Power(s2,3) - 13*Power(t1,3) - 
               30*t2 - 23*Power(t2,2) + 15*Power(t2,3) - 
               Power(s2,2)*(97 + 22*t1 + 6*t2) + 
               Power(t1,2)*(-18 + 41*t2) + 
               t1*(-13 + 41*t2 - 43*Power(t2,2)) + 
               s2*(113 + 17*Power(t1,2) + t1*(41 - 27*t2) + 30*t2 + 
                  10*Power(t2,2))) + 
            Power(s2,2)*(34*Power(t1,3) - Power(t1,2)*(5 + 82*t2) + 
               t1*(-33 + 19*t2 + 62*Power(t2,2)) - 
               2*(8 + 11*t2 + 7*Power(t2,2) + 7*Power(t2,3))) + 
            s2*(-17 - 22*Power(t1,4) + 60*t2 + 22*Power(t2,2) - 
               28*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,3)*(1 + 63*t2) + 
               Power(t1,2)*(26 - 30*t2 - 57*Power(t2,2)) + 
               t1*(28 - 48*t2 + 57*Power(t2,2) + 13*Power(t2,3))) + 
            s1*(-1 + 5*Power(s2,4) + Power(t1,4) + 68*t2 - 
               4*Power(t2,2) + 23*Power(t2,3) - 15*Power(t2,4) - 
               Power(s2,3)*(47 + 8*t1 + t2) + 
               2*Power(t1,3)*(-7 + 6*t2) + 
               Power(t1,2)*(4 + 51*t2 - 42*Power(t2,2)) + 
               Power(s2,2)*(95 + 8*Power(t1,2) + t1*(22 - 10*t2) + 
                  61*t2 + 2*Power(t2,2)) + 
               4*t1*(3 - 15*Power(t2,2) + 11*Power(t2,3)) - 
               s2*(56 + 6*Power(t1,3) + 122*t2 - 43*Power(t2,2) + 
                  7*Power(t2,3) - Power(t1,2)*(17 + 5*t2) + 
                  t1*(32 + 60*t2 - 8*Power(t2,2))))) + 
         Power(s,5)*(24 + Power(s1,5)*(6 - 10*s2) - 5*t1 + 
            49*Power(t1,2) - 56*Power(t1,3) - 24*Power(t1,4) + 
            8*Power(t1,5) + 4*Power(t1,6) - 
            10*Power(s2,5)*(-1 + t1 - t2) + 113*t2 - 136*t1*t2 + 
            46*Power(t1,2)*t2 + 85*Power(t1,3)*t2 - 29*Power(t1,4)*t2 - 
            4*Power(t1,5)*t2 - 39*Power(t2,2) + 52*t1*Power(t2,2) - 
            78*Power(t1,2)*Power(t2,2) + 65*Power(t1,3)*Power(t2,2) - 
            20*Power(t1,4)*Power(t2,2) - 42*Power(t2,3) - 
            3*t1*Power(t2,3) - 101*Power(t1,2)*Power(t2,3) + 
            40*Power(t1,3)*Power(t2,3) + 20*Power(t2,4) + 
            83*t1*Power(t2,4) - 20*Power(t1,2)*Power(t2,4) - 
            26*Power(t2,5) - 4*t1*Power(t2,5) + 4*Power(t2,6) + 
            Power(s1,4)*(-6 - 36*Power(s2,2) + 16*Power(t1,2) - 28*t2 + 
               4*Power(t2,2) - 3*t1*(-5 + 6*t2) + 
               s2*(58 - 14*t1 + 35*t2)) + 
            Power(s2,4)*(-14 + 50*Power(t1,2) + 11*t2 + 
               27*Power(t2,2) - t1*(48 + 79*t2)) + 
            Power(s2,3)*(-72 - 93*Power(t1,3) + 34*t2 + 
               75*Power(t2,2) + 7*Power(t2,3) + 
               4*Power(t1,2)*(20 + 47*t2) + 
               t1*(119 - 118*t2 - 102*Power(t2,2))) + 
            Power(s1,3)*(-46 - 78*Power(s2,3) + 10*Power(t1,3) + 
               Power(t1,2)*(41 - 52*t2) + 13*t2 + 74*Power(t2,2) - 
               16*Power(t2,3) + Power(s2,2)*(239 + 56*t1 + 49*t2) + 
               2*t1*(54 - 55*t2 + 29*Power(t2,2)) - 
               s2*(139 + 19*Power(t1,2) + t1*(142 - 42*t2) + 116*t2 + 
                  36*Power(t2,2))) + 
            Power(s2,2)*(167 + 80*Power(t1,4) - 83*t2 - 
               146*Power(t2,2) + 47*Power(t2,3) - 23*Power(t2,4) - 
               Power(t1,3)*(50 + 181*t2) + 
               Power(t1,2)*(-202 + 168*t2 + 99*Power(t2,2)) + 
               t1*(-27 + 165*t2 - 165*Power(t2,2) + 25*Power(t2,3))) + 
            s2*(-115 - 31*Power(t1,5) - 52*t2 + 66*Power(t1,4)*t2 + 
               175*Power(t2,2) + 30*Power(t2,3) - 53*Power(t2,4) - 
               9*Power(t2,5) + 
               Power(t1,3)*(115 - 34*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*(95 - 176*t2 + 15*Power(t2,2) - 
                  77*Power(t2,3)) + 
               t1*(-54 + 6*t2 + 31*Power(t2,2) + 72*Power(t2,3) + 
                  54*Power(t2,4))) + 
            Power(s1,2)*(181 - 70*Power(s2,4) - 24*Power(t1,4) - 
               33*t2 + 12*Power(t2,2) - 114*Power(t2,3) + 
               24*Power(t2,4) + 3*Power(t1,3)*(9 + 10*t2) + 
               Power(s2,3)*(241 + 112*t1 + 45*t2) + 
               9*Power(t1,2)*(4 - 19*t2 + 4*Power(t2,2)) - 
               Power(s2,2)*(238 + 84*Power(t1,2) + t1*(220 - 13*t2) + 
                  334*t2 + 13*Power(t2,2)) - 
               3*t1*(59 + 53*t2 - 86*Power(t2,2) + 22*Power(t2,3)) + 
               s2*(-98 + 59*Power(t1,3) + Power(t1,2)*(28 - 69*t2) + 
                  442*t2 + 5*Power(t2,2) - 2*Power(t2,3) + 
                  3*t1*(71 + 58*t2 + 4*Power(t2,2)))) + 
            s1*(-161 - 10*Power(s2,5) - 14*Power(t1,5) - 118*t2 + 
               121*Power(t2,2) - 39*Power(t2,3) + 88*Power(t2,4) - 
               16*Power(t2,5) + Power(s2,4)*(82 + 10*t1 + 31*t2) + 
               Power(t1,4)*(3 + 60*t2) - 
               4*Power(t1,3)*(-6 + 19*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(-43 - 39*t2 + 231*Power(t2,2) + 
                  20*Power(t2,3)) + 
               2*t1*(58 + 75*t2 + 27*Power(t2,2) - 123*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(s2,3)*(-83 + 25*Power(t1,2) - 212*t2 + 
                  26*Power(t2,2) - 2*t1*(57 + 40*t2)) + 
               Power(s2,2)*(-144 - 54*Power(t1,3) + 415*t2 + 
                  48*Power(t2,2) + 23*Power(t2,3) + 
                  25*Power(t1,2)*(1 + 5*t2) + 
                  t1*(168 + 182*t2 - 94*Power(t2,2))) + 
               s2*(312 + 43*Power(t1,4) - 222*t2 - 333*Power(t2,2) + 
                  106*Power(t2,3) + 22*Power(t2,4) - 
                  2*Power(t1,3)*(-9 + 68*t2) + 
                  Power(t1,2)*(-97 - 20*t2 + 165*Power(t2,2)) - 
                  2*t1*(53 + 16*t2 + 52*Power(t2,2) + 47*Power(t2,3))))) \
+ Power(s,4)*(-156 + 2*Power(s1,6)*(-2 + 5*s2) + 166*t1 + 
            63*Power(t1,2) + 29*Power(t1,3) - 49*Power(t1,4) - 
            76*Power(t1,5) + 24*Power(t1,6) + Power(t1,7) + 
            10*Power(s2,6)*(-1 + t1 - t2) - 46*t2 - 127*t1*t2 + 
            132*Power(t1,2)*t2 - 86*Power(t1,3)*t2 + 
            274*Power(t1,4)*t2 - 106*Power(t1,5)*t2 + 5*Power(t1,6)*t2 + 
            334*Power(t2,2) - 436*t1*Power(t2,2) + 
            212*Power(t1,2)*Power(t2,2) - 353*Power(t1,3)*Power(t2,2) + 
            208*Power(t1,4)*Power(t2,2) - 21*Power(t1,5)*Power(t2,2) + 
            65*Power(t2,3) - 30*t1*Power(t2,3) + 
            240*Power(t1,2)*Power(t2,3) - 207*Power(t1,3)*Power(t2,3) + 
            15*Power(t1,4)*Power(t2,3) - 47*Power(t2,4) - 
            137*t1*Power(t2,4) + 73*Power(t1,2)*Power(t2,4) + 
            15*Power(t1,3)*Power(t2,4) + 52*Power(t2,5) + 
            29*t1*Power(t2,5) - 21*Power(t1,2)*Power(t2,5) - 
            21*Power(t2,6) + 5*t1*Power(t2,6) + Power(t2,7) + 
            Power(s1,5)*(-28 + 42*Power(s2,2) - 14*Power(t1,2) + 
               s2*(-11 + 13*t1 - 49*t2) + 23*t2 - Power(t2,2) + 
               2*t1*(-3 + 5*t2)) - 
            Power(s2,5)*(17 + 58*Power(t1,2) + 27*t2 + 18*Power(t2,2) - 
               t1*(83 + 84*t2)) + 
            Power(s2,4)*(188 + 130*Power(t1,3) + 35*t2 - 
               115*Power(t2,2) + 14*Power(t2,3) - 
               Power(t1,2)*(204 + 229*t2) + 
               t1*(-136 + 196*t2 + 75*Power(t2,2))) + 
            Power(s1,4)*(135 + 98*Power(s2,3) + 10*Power(t1,3) + 
               112*t2 - 73*Power(t2,2) + 5*Power(t2,3) + 
               10*Power(t1,2)*(-2 + 3*t2) - 
               Power(s2,2)*(243 + 47*t1 + 115*t2) - 
               7*t1*(23 - 13*t2 + 5*Power(t2,2)) + 
               s2*(-94 - 12*Power(t1,2) + t1*(223 - 43*t2) + 55*t2 + 
                  89*Power(t2,2))) + 
            Power(s2,3)*(-222 - 143*Power(t1,4) - 64*t2 + 
               257*Power(t2,2) + 54*Power(t2,3) + 33*Power(t2,4) + 
               2*Power(t1,3)*(97 + 136*t2) + 
               Power(t1,2)*(497 - 288*t2 - 67*Power(t2,2)) - 
               t1*(310 + 312*t2 - 125*Power(t2,2) + 95*Power(t2,3))) + 
            Power(s2,2)*(-129 + 79*Power(t1,5) + 163*t2 - 
               170*Power(t2,2) - 125*Power(t2,3) + 182*Power(t2,4) + 
               3*Power(t2,5) - 2*Power(t1,4)*(16 + 69*t2) - 
               Power(t1,3)*(587 + 16*t2 + 43*Power(t2,2)) + 
               Power(t1,2)*(-53 + 708*t2 + 315*Power(t2,2) + 
                  187*Power(t2,3)) - 
               t1*(-726 + 87*t2 + 331*Power(t2,2) + 449*Power(t2,3) + 
                  88*Power(t2,4))) + 
            s2*(314 - 19*Power(t1,6) - 203*t2 - 402*Power(t2,2) + 
               215*Power(t2,3) + 142*Power(t2,4) + 9*Power(t2,5) - 
               7*Power(t2,6) + Power(t1,5)*(-55 + 16*t2) + 
               Power(t1,4)*(319 + 243*t2 + 74*Power(t2,2)) - 
               Power(t1,3)*(-254 + 728*t2 + 543*Power(t2,2) + 
                  121*Power(t2,3)) + 
               Power(t1,2)*(-425 - 213*t2 + 701*Power(t2,2) + 
                  586*Power(t2,3) + 44*Power(t2,4)) + 
               t1*(-396 + 552*t2 + 224*Power(t2,2) - 434*Power(t2,3) - 
                  240*Power(t2,4) + 13*Power(t2,5))) + 
            Power(s1,3)*(-201 + 160*Power(s2,4) + 45*Power(t1,4) + 
               Power(t1,3)*(20 - 90*t2) - 349*t2 - 220*Power(t2,2) + 
               142*Power(t2,3) - 10*Power(t2,4) - 
               Power(s2,3)*(421 + 273*t1 + 113*t2) + 
               Power(t1,2)*(-51 + 89*t2 + 15*Power(t2,2)) + 
               Power(s2,2)*(218 + 206*Power(t1,2) + t1*(474 - 31*t2) + 
                  633*t2 + 90*Power(t2,2)) + 
               t1*(185 + 539*t2 - 266*Power(t2,2) + 40*Power(t2,3)) - 
               s2*(-510 + 118*Power(t1,3) + 
                  Power(t1,2)*(105 - 121*t2) + 19*t2 + 
                  108*Power(t2,2) + 66*Power(t2,3) + 
                  t1*(616 + 398*t2 - 38*Power(t2,2)))) + 
            Power(s1,2)*(-98 + 100*Power(s2,5) + 5*Power(t1,5) + 
               Power(t1,4)*(50 - 85*t2) + 697*t2 + 246*Power(t2,2) + 
               268*Power(t2,3) - 158*Power(t2,4) + 10*Power(t2,5) - 
               Power(s2,4)*(256 + 213*t1 + 151*t2) + 
               3*Power(t1,3)*(-34 - 53*t2 + 55*Power(t2,2)) + 
               Power(t1,2)*(41 + 411*t2 - 45*Power(t2,2) - 
                  85*Power(t2,3)) + 
               t1*(138 - 797*t2 - 732*Power(t2,2) + 312*Power(t2,3) - 
                  10*Power(t2,4)) + 
               Power(s2,3)*(-33 + 137*Power(t1,2) + 763*t2 - 
                  35*Power(t2,2) + t1*(452 + 289*t2)) - 
               Power(s2,2)*(-497 + 20*Power(t1,3) + 1087*t2 + 
                  355*Power(t2,2) + 5*Power(t1,2)*(50 + 47*t2) + 
                  t1*(65 + 740*t2 - 115*Power(t2,2))) + 
               s2*(-504 - 7*Power(t1,4) - 730*t2 + 462*Power(t2,2) + 
                  104*Power(t2,3) + 4*Power(t2,4) + 
                  Power(t1,3)*(-8 + 151*t2) - 
                  2*Power(t1,2)*(28 - 186*t2 + 81*Power(t2,2)) + 
                  t1*(218 + 1150*t2 - 113*Power(t2,2) + 14*Power(t2,3)))\
) + s1*(358 + 10*Power(s2,6) - 15*Power(t1,6) + 
               Power(s2,5)*(-75 + 2*t1 - 74*t2) - 392*t2 - 
               561*Power(t2,2) + 15*Power(t2,3) - 184*Power(t2,4) + 
               91*Power(t2,5) - 5*Power(t2,6) + 
               6*Power(t1,5)*(6 + 5*t2) + 
               Power(t1,4)*(-50 - 216*t2 + 25*Power(t2,2)) + 
               Power(t1,3)*(92 + 343*t2 + 346*Power(t2,2) - 
                  100*Power(t2,3)) + 
               Power(t1,2)*(-82 - 389*t2 - 600*Power(t2,2) - 
                  97*Power(t2,3) + 75*Power(t2,4)) + 
               t1*(-389 + 648*t2 + 642*Power(t2,2) + 491*Power(t2,3) - 
                  160*Power(t2,4) - 10*Power(t2,5)) + 
               Power(s2,4)*(-118 - 93*Power(t1,2) + 254*t2 - 
                  23*Power(t2,2) + t1*(253 + 233*t2)) + 
               Power(s2,3)*(602 + 185*Power(t1,3) - 339*t2 - 
                  396*Power(t2,2) + 17*Power(t2,3) - 
                  Power(t1,2)*(303 + 326*t2) + 
                  t1*(-124 - 190*t2 + 79*Power(t2,2))) + 
               Power(s2,2)*(-381 - 166*Power(t1,4) - 3*t2 + 
                  994*Power(t2,2) - 217*Power(t2,3) - 20*Power(t2,4) + 
                  Power(t1,3)*(161 + 293*t2) + 
                  Power(t1,2)*(495 - 244*t2 - 158*Power(t2,2)) + 
                  t1*(-747 + 25*t2 + 715*Power(t2,2) + 51*Power(t2,3))) \
+ s2*(-241 + 77*Power(t1,5) + 1036*t2 + 5*Power(t2,2) - 
                  491*Power(t2,3) - 49*Power(t2,4) + 19*Power(t2,5) - 
                  2*Power(t1,4)*(37 + 79*t2) + 
                  8*Power(t1,3)*(-30 + 56*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*
                   (287 + 61*t2 - 853*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(540 - 1092*t2 - 100*Power(t2,2) + 
                     528*Power(t2,3) - 35*Power(t2,4))))) - 
         Power(s,3)*(-234 + Power(s1,7)*(-1 + 5*s2) + 386*t1 + 
            332*Power(t1,2) - 708*Power(t1,3) + 140*Power(t1,4) + 
            14*Power(t1,5) + 91*Power(t1,6) - 21*Power(t1,7) + 
            5*Power(s2,7)*(-1 + t1 - t2) + 238*t2 - 1272*t1*t2 + 
            1724*Power(t1,2)*t2 - 580*Power(t1,3)*t2 + 
            146*Power(t1,4)*t2 - 305*Power(t1,5)*t2 + 
            84*Power(t1,6)*t2 - 2*Power(t1,7)*t2 + 700*Power(t2,2) - 
            1370*t1*Power(t2,2) + 514*Power(t1,2)*Power(t2,2) - 
            156*Power(t1,3)*Power(t2,2) + 330*Power(t1,4)*Power(t2,2) - 
            117*Power(t1,5)*Power(t2,2) + 2*Power(t1,6)*Power(t2,2) - 
            6*Power(t2,3) + 252*t1*Power(t2,3) - 
            48*Power(t1,2)*Power(t2,3) - 92*Power(t1,3)*Power(t2,3) + 
            18*Power(t1,4)*Power(t2,3) + 10*Power(t1,5)*Power(t2,3) - 
            116*Power(t2,4) - 10*t1*Power(t2,4) - 
            94*Power(t1,2)*Power(t2,4) + 118*Power(t1,3)*Power(t2,4) - 
            20*Power(t1,4)*Power(t2,4) + 54*Power(t2,5) + 
            123*t1*Power(t2,5) - 111*Power(t1,2)*Power(t2,5) + 
            10*Power(t1,3)*Power(t2,5) - 53*Power(t2,6) + 
            24*t1*Power(t2,6) + 2*Power(t1,2)*Power(t2,6) + 
            5*Power(t2,7) - 2*t1*Power(t2,7) + 
            Power(s1,6)*(-23 + 31*Power(s2,2) - 6*Power(t1,2) + 
               s2*(29 - 32*t2) + 7*t2 + 2*t1*(3 + t2)) - 
            2*Power(s2,6)*(15 + 17*Power(t1,2) + 15*t2 + Power(t2,2) - 
               3*t1*(11 + 8*t2)) + 
            Power(s2,5)*(166 + 92*Power(t1,3) + 110*t2 - 
               69*Power(t2,2) + 19*Power(t2,3) - 
               10*Power(t1,2)*(22 + 15*t2) + 
               t1*(-40 + 152*t2 + 7*Power(t2,2))) + 
            Power(s2,4)*(-36 - 127*Power(t1,4) - 182*t2 + 
               176*Power(t2,2) + 201*Power(t2,3) + 21*Power(t2,4) + 
               Power(t1,3)*(293 + 210*t2) + 
               Power(t1,2)*(485 - 157*t2 + 44*Power(t2,2)) - 
               t1*(625 + 423*t2 + 115*Power(t2,2) + 128*Power(t2,3))) + 
            Power(s2,3)*(-515 + 94*Power(t1,5) - 20*t2 + 
               106*Power(t2,2) - 107*Power(t2,3) + 233*Power(t2,4) + 
               Power(t2,5) - Power(t1,4)*(123 + 130*t2) - 
               Power(t1,3)*(1007 + 257*t2 + 164*Power(t2,2)) + 
               Power(t1,2)*(585 + 1119*t2 + 947*Power(t2,2) + 
                  298*Power(t2,3)) + 
               t1*(988 + 85*t2 - 663*Power(t2,2) - 915*Power(t2,3) - 
                  99*Power(t2,4))) + 
            Power(s2,2)*(314 - 35*Power(t1,6) - 91*t2 - 
               660*Power(t2,2) + 279*Power(t2,3) + 327*Power(t2,4) - 
               30*Power(t2,5) - 5*Power(t2,6) + 
               2*Power(t1,5)*(-37 + 9*t2) + 
               3*Power(t1,4)*(324 + 201*t2 + 61*Power(t2,2)) - 
               2*Power(t1,3)*
                (-35 + 890*t2 + 700*Power(t2,2) + 141*Power(t2,3)) + 
               Power(t1,2)*(-1882 + 181*t2 + 1580*Power(t2,2) + 
                  1302*Power(t2,3) + 113*Power(t2,4)) + 
               t1*(619 + 656*t2 - 70*Power(t2,2) - 734*Power(t2,3) - 
                  401*Power(t2,4) + 8*Power(t2,5))) + 
            s2*(274 + 5*Power(t1,7) - 102*t2 + 214*Power(t2,2) + 
               612*Power(t2,3) + 59*Power(t2,4) - 93*Power(t2,5) - 
               13*Power(t2,6) - Power(t2,7) + 
               Power(t1,6)*(84 + 11*t2) - 
               Power(t1,5)*(471 + 395*t2 + 70*Power(t2,2)) + 
               Power(t1,4)*(-210 + 1291*t2 + 746*Power(t2,2) + 
                  84*Power(t2,3)) - 
               Power(t1,3)*(-844 + 335*t2 + 1428*Power(t2,2) + 
                  590*Power(t2,3) + 16*Power(t2,4)) + 
               Power(t1,2)*(688 - 804*t2 + 1019*Power(t2,2) + 
                  694*Power(t2,3) + 89*Power(t2,4) - 26*Power(t2,5)) + 
               t1*(-1210 + 344*t2 - 272*Power(t2,2) - 
                  1033*Power(t2,3) + 7*Power(t2,4) + 79*Power(t2,5) + 
                  13*Power(t2,6))) + 
            Power(s1,5)*(-22 + 65*Power(s2,3) + 20*Power(t1,3) + 
               127*t2 - 25*Power(t2,2) + Power(t1,2)*(9 + 8*t2) - 
               2*t1*(24 + t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(4*t1 - 69*(1 + 2*t2)) + 
               s2*(-175 - 53*Power(t1,2) - 128*t2 + 86*Power(t2,2) + 
                  t1*(167 + 6*t2))) + 
            Power(s1,4)*(324 + 143*Power(s2,4) + 30*Power(t1,4) + 
               Power(t1,3)*(50 - 90*t2) + 69*t2 - 331*Power(t2,2) + 
               55*Power(t2,3) - Power(s2,3)*(346 + 259*t1 + 125*t2) + 
               Power(t1,2)*(126 - 119*t2 + 30*Power(t2,2)) + 
               Power(s2,2)*(-312 + 227*Power(t1,2) + 
                  t1*(532 - 160*t2) + 397*t2 + 237*Power(t2,2)) + 
               t1*(-573 + 359*t2 - 28*Power(t2,2) + 10*Power(t2,3)) - 
               s2*(-479 + 122*Power(t1,3) + 
                  Power(t1,2)*(121 - 232*t2) - 563*t2 - 
                  209*Power(t2,2) + 125*Power(t2,3) + 
                  t1*(243 + 581*t2 + 11*Power(t2,2)))) + 
            Power(s1,3)*(-714 + 181*Power(s2,5) - 20*Power(t1,5) + 
               Power(t1,4)*(70 - 40*t2) - 766*t2 - 129*Power(t2,2) + 
               514*Power(t2,3) - 75*Power(t2,4) - 
               Power(s2,4)*(295 + 440*t1 + 194*t2) + 
               4*Power(t1,3)*(33 - 94*t2 + 35*Power(t2,2)) + 
               2*t1*(807 + 737*t2 - 456*Power(t2,2) + 6*Power(t2,3)) - 
               Power(t1,2)*(1149 + 56*t2 - 414*Power(t2,2) + 
                  80*Power(t2,3)) + 
               Power(s2,3)*(71 + 368*Power(t1,2) + 1019*t2 - 
                  16*Power(t2,2) + t1*(508 + 422*t2)) - 
               Power(s2,2)*(-1055 + 128*Power(t1,3) - 180*t2 + 
                  747*Power(t2,2) + 188*Power(t2,3) + 
                  Power(t1,2)*(249 + 502*t2) + 
                  t1*(950 + 1086*t2 - 448*Power(t2,2))) + 
               s2*(-320 + 42*Power(t1,4) - 1785*t2 - 546*Power(t2,2) - 
                  136*Power(t2,3) + 105*Power(t2,4) + 
                  2*Power(t1,3)*(7 + 128*t2) + 
                  Power(t1,2)*(442 + 224*t2 - 352*Power(t2,2)) - 
                  2*t1*(45 - 613*t2 - 331*Power(t2,2) + 8*Power(t2,3)))) \
+ Power(s1,2)*(444 + 80*Power(s2,6) - 20*Power(t1,6) + 1682*t2 + 
               444*Power(t2,2) + 193*Power(t2,3) - 481*Power(t2,4) + 
               61*Power(t2,5) + Power(t1,5)*(39 + 70*t2) - 
               Power(s2,5)*(115 + 200*t1 + 236*t2) + 
               Power(t1,4)*(200 - 264*t2 - 30*Power(t2,2)) - 
               Power(t1,3)*(1083 + 296*t2 - 720*Power(t2,2) + 
                  80*Power(t2,3)) + 
               Power(t1,2)*(1742 + 2789*t2 - 360*Power(t2,2) - 
                  618*Power(t2,3) + 70*Power(t2,4)) - 
               t1*(1398 + 3716*t2 + 1239*Power(t2,2) - 
                  1062*Power(t2,3) - 62*Power(t2,4) + 10*Power(t2,5)) + 
               Power(s2,4)*(-527 + 116*Power(t1,2) + 683*t2 - 
                  20*Power(t2,2) + t1*(467 + 644*t2)) + 
               Power(s2,3)*(403 + 72*Power(t1,3) - 1013*t2 - 
                  767*Power(t2,2) + 148*Power(t2,3) - 
                  2*Power(t1,2)*(357 + 284*t2) + 
                  t1*(1183 - 971*t2 - 166*Power(t2,2))) + 
               Power(s2,2)*(-238 - 111*Power(t1,4) - 1995*t2 + 
                  903*Power(t2,2) + 549*Power(t2,3) + 57*Power(t2,4) + 
                  Power(t1,3)*(461 + 182*t2) + 
                  Power(t1,2)*(-1111 + 595*t2 + 436*Power(t2,2)) + 
                  t1*(-34 + 2412*t2 + 175*Power(t2,2) - 
                     424*Power(t2,3))) + 
               s2*(-338 + 63*Power(t1,5) + 1858*t2 + 2192*Power(t2,2) + 
                  10*Power(t2,3) - Power(t2,4) - 50*Power(t2,5) - 
                  33*Power(t1,4)*(4 + 3*t2) - 
                  2*Power(t1,3)*(-81 + 43*t2 + 81*Power(t2,2)) + 
                  Power(t1,2)*
                   (607 - 390*t2 + 4*Power(t2,2) + 194*Power(t2,3)) + 
                  t1*(-6 - 1731*t2 - 1716*Power(t2,2) - 
                     170*Power(t2,3) + 54*Power(t2,4)))) + 
            s1*(230 + 5*Power(s2,7) + 4*Power(t1,7) + 
               2*Power(s2,6)*(-19 + 8*t1 - 38*t2) - 1378*t2 - 
               962*Power(t2,2) + 114*Power(t2,3) - 165*Power(t2,4) + 
               247*Power(t2,5) - 27*Power(t2,6) + 
               4*Power(t1,6)*(-1 + 3*t2) + 
               Power(t1,5)*(100 + 16*t2 - 60*Power(t2,2)) + 
               Power(t1,3)*(1342 + 1600*t2 + 256*Power(t2,2) - 
                  512*Power(t2,3)) + 
               Power(t1,4)*(-531 - 403*t2 + 176*Power(t2,2) + 
                  60*Power(t2,3)) - 
               Power(t1,2)*(1176 + 3326*t2 + 1592*Power(t2,2) - 
                  384*Power(t2,3) - 425*Power(t2,4) + 24*Power(t2,5)) + 
               2*t1*(1 + 1826*t2 + 925*Power(t2,2) + 174*Power(t2,3) - 
                  292*Power(t2,4) - 37*Power(t2,5) + 4*Power(t2,6)) + 
               Power(s2,5)*(-258 - 129*Power(t1,2) + 107*t2 + 
                  36*Power(t2,2) + t1*(279 + 268*t2)) + 
               Power(s2,4)*(703 + 272*Power(t1,3) + 225*t2 - 
                  589*Power(t2,2) + 50*Power(t2,3) - 
                  Power(t1,2)*(597 + 404*t2) + 
                  t1*(194 + 8*t2 - 76*Power(t2,2))) + 
               Power(s2,3)*(390 - 286*Power(t1,4) - 187*t2 + 
                  1049*Power(t2,2) - 139*Power(t2,3) - 73*Power(t2,4) + 
                  Power(t1,3)*(524 + 390*t2) + 
                  Power(t1,2)*(857 - 527*t2 - 98*Power(t2,2)) + 
                  2*t1*(-1163 - 412*t2 + 689*Power(t2,2) + 
                     51*Power(t2,3))) + 
               Power(s2,2)*(-833 + 166*Power(t1,5) + 1318*t2 + 
                  661*Power(t2,2) - 1098*Power(t2,3) - 
                  100*Power(t2,4) + 6*Power(t2,5) - 
                  Power(t1,4)*(193 + 250*t2) + 
                  4*Power(t1,3)*(-299 + 194*t2 + 57*Power(t2,2)) + 
                  Power(t1,2)*
                   (2255 + 896*t2 - 1648*Power(t2,2) - 274*Power(t2,3)) \
+ 2*t1*(123 - 678*t2 - 364*Power(t2,2) + 390*Power(t2,3) + 
                     62*Power(t2,4))) + 
               s2*(52 - 48*Power(t1,6) - 100*t2 - 2150*Power(t2,2) - 
                  945*Power(t2,3) + 241*Power(t2,4) + 40*Power(t2,5) + 
                  12*Power(t2,6) + Power(t1,5)*(29 + 60*t2) - 
                  3*Power(t1,4)*(-97 + 126*t2 + 9*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (-52 + 129*t2 + 331*Power(t2,2) + 22*Power(t2,3)) + 
                  Power(t1,2)*
                   (-1398 - 765*t2 - 746*Power(t2,2) - 
                     196*Power(t2,3) + 5*Power(t2,4)) + 
                  t1*(1160 + 364*t2 + 2854*Power(t2,2) + 
                     726*Power(t2,3) - 157*Power(t2,4) - 46*Power(t2,5)))\
)) + Power(s,2)*(-150 + Power(s1,8)*s2 + 317*t1 + 493*Power(t1,2) - 
            1622*Power(t1,3) + 1149*Power(t1,4) - 108*Power(t1,5) - 
            42*Power(t1,6) - 43*Power(t1,7) + 6*Power(t1,8) + 
            Power(s1,7)*(-5 + 14*Power(s2,2) + 4*t1 - Power(t1,2) + 
               s2*(20 - 7*t1 - 8*t2)) + Power(s2,8)*(-1 + t1 - t2) + 
            385*t2 - 2100*t1*t2 + 3692*Power(t1,2)*t2 - 
            2284*Power(t1,3)*t2 + 78*Power(t1,4)*t2 + 
            168*Power(t1,5)*t2 + 88*Power(t1,6)*t2 - 15*Power(t1,7)*t2 + 
            671*Power(t2,2) - 1690*t1*Power(t2,2) + 
            780*Power(t1,2)*Power(t2,2) + 646*Power(t1,3)*Power(t2,2) - 
            548*Power(t1,4)*Power(t2,2) + 102*Power(t1,5)*Power(t2,2) - 
            19*Power(t1,6)*Power(t2,2) + Power(t1,7)*Power(t2,2) - 
            140*Power(t2,3) + 790*t1*Power(t2,3) - 
            992*Power(t1,2)*Power(t2,3) + 780*Power(t1,3)*Power(t2,3) - 
            439*Power(t1,4)*Power(t2,3) + 107*Power(t1,5)*Power(t2,3) - 
            3*Power(t1,6)*Power(t2,3) - 165*Power(t2,4) + 
            209*t1*Power(t2,4) - 512*Power(t1,2)*Power(t2,4) + 
            509*Power(t1,3)*Power(t2,4) - 137*Power(t1,4)*Power(t2,4) + 
            2*Power(t1,5)*Power(t2,4) + 41*Power(t2,5) + 
            228*t1*Power(t2,5) - 279*Power(t1,2)*Power(t2,5) + 
            58*Power(t1,3)*Power(t2,5) + 2*Power(t1,4)*Power(t2,5) - 
            74*Power(t2,6) + 50*t1*Power(t2,6) + 
            10*Power(t1,2)*Power(t2,6) - 3*Power(t1,3)*Power(t2,6) + 
            12*Power(t2,7) - 10*t1*Power(t2,7) + 
            Power(t1,2)*Power(t2,7) + 
            Power(s2,7)*(-16 - 8*Power(t1,2) - 17*t2 + 3*Power(t2,2) + 
               t1*(24 + 13*t2)) + 
            Power(s2,6)*(26*Power(t1,3) - 15*Power(t1,2)*(7 + 3*t2) + 
               t1*(17 + 54*t2 - 25*Power(t2,2)) + 
               2*(32 + 46*t2 + Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,5)*(83 - 44*Power(t1,4) - 92*t2 + 52*Power(t2,2) + 
               208*Power(t2,3) + 9*Power(t2,4) + 
               Power(t1,3)*(183 + 62*t2) + 
               Power(t1,2)*(209 + 66*t2 + 112*Power(t2,2)) - 
               t1*(439 + 398*t2 + 282*Power(t2,2) + 91*Power(t2,3))) + 
            Power(s2,4)*(-388 + 41*Power(t1,5) - 232*t2 + 
               242*Power(t2,2) + 63*Power(t2,3) + 141*Power(t2,4) + 
               4*Power(t2,5) - 2*Power(t1,4)*(61 + 11*t2) - 
               Power(t1,3)*(750 + 505*t2 + 253*Power(t2,2)) + 
               Power(t1,2)*(894 + 1108*t2 + 1182*Power(t2,2) + 
                  279*Power(t2,3)) + 
               t1*(337 + 153*t2 - 669*Power(t2,2) - 919*Power(t2,3) - 
                  69*Power(t2,4))) - 
            Power(s2,3)*(146 + 20*Power(t1,6) - 108*t2 + 
               241*Power(t2,2) - 103*Power(t2,3) - 310*Power(t2,4) + 
               65*Power(t2,5) + 7*Power(t2,6) + 
               3*Power(t1,5)*(9 + 10*t2) - 
               Power(t1,4)*(1115 + 846*t2 + 288*Power(t2,2)) + 
               Power(t1,3)*(675 + 2016*t2 + 1849*Power(t2,2) + 
                  350*Power(t2,3)) - 
               Power(t1,2)*(-1788 + 348*t2 + 1896*Power(t2,2) + 
                  1463*Power(t2,3) + 98*Power(t2,4)) + 
               t1*(-1533 - 465*t2 + 580*Power(t2,2) + 768*Power(t2,3) + 
                  277*Power(t2,4) - 21*Power(t2,5))) + 
            Power(s2,2)*(549 + 4*Power(t1,7) + 30*t2 + 
               168*Power(t2,2) + 827*Power(t2,3) - 33*Power(t2,4) - 
               196*Power(t2,5) - 30*Power(t2,6) - 5*Power(t2,7) + 
               Power(t1,6)*(82 + 32*t2) - 
               Power(t1,5)*(848 + 624*t2 + 157*Power(t2,2)) + 
               Power(t1,4)*(86 + 1904*t2 + 1260*Power(t2,2) + 
                  179*Power(t2,3)) - 
               2*Power(t1,3)*
                (-1069 + 403*t2 + 911*Power(t2,2) + 446*Power(t2,3) + 
                  3*Power(t2,4)) - 
               Power(t1,2)*(979 + 499*t2 - 1083*Power(t2,2) - 
                  604*Power(t2,3) + 9*Power(t2,4) + 86*Power(t2,5)) + 
               t1*(-1030 + t2 - 472*Power(t2,2) - 710*Power(t2,3) + 
                  121*Power(t2,4) + 213*Power(t2,5) + 39*Power(t2,6))) + 
            s2*(91 + 500*t2 + 1008*Power(t2,2) + 515*Power(t2,3) - 
               250*Power(t2,4) - 250*Power(t2,5) - 9*Power(t2,6) - 
               14*Power(t2,7) + Power(t2,8) - Power(t1,7)*(40 + 9*t2) + 
               Power(t1,6)*(316 + 195*t2 + 31*Power(t2,2)) - 
               2*Power(t1,5)*
                (-56 + 389*t2 + 147*Power(t2,2) + 11*Power(t2,3)) + 
               Power(t1,4)*(-662 + 253*t2 + 411*Power(t2,2) + 
                  44*Power(t2,3) - 36*Power(t2,4)) + 
               Power(t1,3)*(-1273 + 39*t2 - 175*Power(t2,2) + 
                  600*Power(t2,3) + 263*Power(t2,4) + 64*Power(t2,5)) + 
               Power(t1,2)*(2822 + 1571*t2 + 1245*Power(t2,2) - 
                  719*Power(t2,3) - 850*Power(t2,4) - 
                  231*Power(t2,5) - 32*Power(t2,6)) + 
               t1*(-1366 - 1778*t2 - 2107*Power(t2,2) - 
                  667*Power(t2,3) + 1091*Power(t2,4) + 
                  310*Power(t2,5) + 77*Power(t2,6) + 3*Power(t2,7))) + 
            Power(s1,6)*(-43 + 27*Power(s2,3) + 11*Power(t1,3) + 
               Power(s2,2)*(44 + 19*t1 - 87*t2) + t1*(26 - 22*t2) + 
               Power(t1,2)*(-2 + t2) + 36*t2 + 
               s2*(-13 - 47*Power(t1,2) - 128*t2 + 28*Power(t2,2) + 
                  t1*(47 + 48*t2))) + 
            Power(s1,5)*(30 + 55*Power(s2,4) + 3*Power(t1,4) + 234*t2 - 
               42*Power(t1,3)*t2 - 117*Power(t2,2) - 
               Power(s2,3)*(117 + 84*t1 + 95*t2) + 
               3*Power(t1,2)*(45 - 16*t2 + 3*Power(t2,2)) + 
               t1*(-194 - 87*t2 + 60*Power(t2,2)) + 
               Power(s2,2)*(-456 + 85*Power(t1,2) - 113*t2 + 
                  230*Power(t2,2) - 5*t1*(-67 + 32*t2)) - 
               s2*(194 + 53*Power(t1,3) - 80*t2 - 354*Power(t2,2) + 
                  56*Power(t2,3) - 3*Power(t1,2)*(-35 + 82*t2) + 
                  6*t1*(-62 + 49*t2 + 23*Power(t2,2)))) + 
            Power(s1,4)*(292 + 119*Power(s2,5) - 25*Power(t1,5) - 
               131*t2 - 580*Power(t2,2) + 220*Power(t2,3) + 
               5*Power(t1,4)*(-2 + 3*t2) - 
               Power(s2,4)*(175 + 345*t1 + 78*t2) + 
               Power(t1,3)*(113 - 34*t2 + 55*Power(t2,2)) + 
               t1*(-731 + 1003*t2 + 138*Power(t2,2) - 
                  110*Power(t2,3)) + 
               Power(t1,2)*(295 - 699*t2 + 222*Power(t2,2) - 
                  25*Power(t2,3)) + 
               Power(s2,3)*(-137 + 388*Power(t1,2) + 646*t2 + 
                  103*Power(t2,2) + t1*(289 + 87*t2)) - 
               Power(s2,2)*(-850 + 204*Power(t1,3) - 1647*t2 + 
                  18*Power(t2,2) + 335*Power(t2,3) + 
                  Power(t1,2)*(-6 + 250*t2) + 
                  t1*(791 + 1272*t2 - 489*Power(t2,2))) + 
               s2*(1100 + 68*Power(t1,4) + 393*t2 - 199*Power(t2,2) - 
                  550*Power(t2,3) + 70*Power(t2,4) + 
                  Power(t1,3)*(-55 + 202*t2) + 
                  Power(t1,2)*(868 + 213*t2 - 546*Power(t2,2)) + 
                  t1*(-1782 - 1022*t2 + 783*Power(t2,2) + 
                     215*Power(t2,3)))) + 
            Power(s1,3)*(-834 + 115*Power(s2,6) - 10*Power(t1,6) - 
               567*t2 + 172*Power(t2,2) + 840*Power(t2,3) - 
               255*Power(t2,4) + Power(t1,5)*(-44 + 70*t2) - 
               Power(s2,5)*(50 + 337*t1 + 210*t2) + 
               Power(t1,4)*(70 + 179*t2 - 65*Power(t2,2)) + 
               Power(t1,3)*(1161 - 956*t2 + 44*Power(t2,2) - 
                  20*Power(t2,3)) + 
               Power(t1,2)*(-3076 + 8*t2 + 1566*Power(t2,2) - 
                  368*Power(t2,3) + 25*Power(t2,4)) + 
               t1*(2645 + 1558*t2 - 2073*Power(t2,2) - 
                  202*Power(t2,3) + 140*Power(t2,4)) + 
               Power(s2,4)*(-210 + 297*Power(t1,2) + 587*t2 - 
                  100*Power(t2,2) + 30*t1*(7 + 25*t2)) - 
               Power(s2,3)*(-517 + 20*Power(t1,3) + 365*t2 + 
                  1171*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t1,2)*(461 + 986*t2) + 
                  t1*(-293 + 503*t2 - 222*Power(t2,2))) + 
               Power(s2,2)*(-475 - 82*Power(t1,4) - 3239*t2 - 
                  2009*Power(t2,2) + 322*Power(t2,3) + 
                  290*Power(t2,4) + Power(t1,3)*(469 + 502*t2) + 
                  Power(t1,2)*(-685 - 390*t2 + 326*Power(t2,2)) + 
                  t1*(359 + 3596*t2 + 1593*Power(t2,2) - 
                     736*Power(t2,3))) + 
               s2*(-1810 + 37*Power(t1,5) - 3136*t2 + 235*Power(t2,2) + 
                  256*Power(t2,3) + 520*Power(t2,4) - 56*Power(t2,5) - 
                  3*Power(t1,4)*(39 + 43*t2) + 
                  Power(t1,3)*(550 - 24*t2 - 352*Power(t2,2)) + 
                  Power(t1,2)*
                   (-2437 - 2276*t2 + 222*Power(t2,2) + 
                     664*Power(t2,3)) + 
                  t1*(4067 + 4723*t2 + 524*Power(t2,2) - 
                     1132*Power(t2,3) - 195*Power(t2,4)))) + 
            Power(s1,2)*(793 + 34*Power(s2,7) + 6*Power(t1,7) + 
               6*Power(t1,6)*(-10 + t2) + 1641*t2 + 93*Power(t2,2) - 
               30*Power(t2,3) - 735*Power(t2,4) + 180*Power(t2,5) - 
               5*Power(s2,6)*(1 + 18*t1 + 37*t2) - 
               3*Power(t1,5)*(15 - 95*t2 + 21*Power(t2,2)) + 
               Power(t1,4)*(1354 - 678*t2 - 465*Power(t2,2) + 
                  75*Power(t2,3)) + 
               Power(t1,3)*(-3710 - 1827*t2 + 2082*Power(t2,2) + 
                  72*Power(t2,3) - 15*Power(t2,4)) - 
               3*Power(t1,2)*
                (-1545 - 1948*t2 + 471*Power(t2,2) + 618*Power(t2,3) - 
                  94*Power(t2,4) + 3*Power(t2,5)) - 
               t1*(3027 + 4992*t2 + 714*Power(t2,2) - 
                  2141*Power(t2,3) - 258*Power(t2,4) + 114*Power(t2,5)) \
+ Power(s2,5)*(-541 + 39*Power(t1,2) + 246*t2 + 72*Power(t2,2) + 
                  t1*(256 + 574*t2)) + 
               Power(s2,4)*(-7 + 87*Power(t1,3) - 54*t2 - 
                  508*Power(t2,2) + 218*Power(t2,3) - 
                  Power(t1,2)*(752 + 507*t2) + 
                  t1*(1639 - 609*t2 - 534*Power(t2,2))) + 
               Power(s2,3)*(391 - 119*Power(t1,4) - 1195*t2 + 
                  1451*Power(t2,2) + 807*Power(t2,3) - 
                  67*Power(t2,4) + Power(t1,3)*(775 + 4*t2) + 
                  Power(t1,2)*(-1727 + 1032*t2 + 906*Power(t2,2)) - 
                  3*t1*(63 - 2*t2 + 46*Power(t2,2) + 116*Power(t2,3))) \
+ Power(s2,2)*(229 + 73*Power(t1,5) + 2499*t2 + 3895*Power(t2,2) + 
                  705*Power(t2,3) - 388*Power(t2,4) - 
                  149*Power(t2,5) + 43*Power(t1,4)*(-8 + 3*t2) + 
                  Power(t1,3)*(464 - 879*t2 - 398*Power(t2,2)) + 
                  Power(t1,2)*
                   (1626 + 1449*t2 + 753*Power(t2,2) - 
                     328*Power(t2,3)) + 
                  t1*(-1928 - 2380*t2 - 4698*Power(t2,2) - 
                     497*Power(t2,3) + 589*Power(t2,4))) + 
               s2*(1011 - 30*Power(t1,6) + Power(t1,5)*(130 - 21*t2) + 
                  4769*t2 + 2722*Power(t2,2) - 1123*Power(t2,3) - 
                  179*Power(t2,4) - 300*Power(t2,5) + 28*Power(t2,6) + 
                  6*Power(t1,4)*(35 - 14*t2 + 3*Power(t2,2)) + 
                  Power(t1,3)*
                   (-2920 - 648*t2 + 476*Power(t2,2) + 374*Power(t2,3)) \
+ Power(t1,2)*(5561 + 5940*t2 + 1098*Power(t2,2) - 894*Power(t2,3) - 
                     471*Power(t2,4)) + 
                  3*t1*(-1273 - 3597*t2 - 1003*Power(t2,2) + 
                     280*Power(t2,3) + 311*Power(t2,4) + 34*Power(t2,5))\
)) + s1*(-85 + Power(s2,8) + Power(s2,7)*(-10 + 14*t1 - 37*t2) - 
               1614*t2 - 667*Power(t2,2) + 347*Power(t2,3) - 
               82*Power(t2,4) + 358*Power(t2,5) - 71*Power(t2,6) - 
               2*Power(t1,7)*(13 + 3*t2) + 
               Power(t1,6)*(-23 + 125*t2 + 7*Power(t2,2)) + 
               Power(t1,5)*(579 - 153*t2 - 348*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,4)*(-2157 - 1148*t2 + 1047*Power(t2,2) + 
                  433*Power(t2,3) - 30*Power(t2,4)) + 
               Power(t1,3)*(3617 + 4362*t2 - 114*Power(t2,2) - 
                  1748*Power(t2,3) - 140*Power(t2,4) + 14*Power(t2,5)) - 
               Power(t1,2)*(2885 + 6987*t2 + 1776*Power(t2,2) - 
                  1622*Power(t2,3) - 1131*Power(t2,4) + 
                  96*Power(t2,5) + Power(t2,6)) + 
               t1*(968 + 5532*t2 + 1557*Power(t2,2) - 322*Power(t2,3) - 
                  1105*Power(t2,4) - 183*Power(t2,5) + 52*Power(t2,6)) + 
               Power(s2,6)*(-176 - 89*Power(t1,2) - 26*t2 + 
                  62*Power(t2,2) + 2*t1*(80 + 73*t2)) + 
               Power(s2,5)*(331 + 211*Power(t1,3) + 429*t2 - 
                  404*Power(t2,2) + 10*Power(t2,3) - 
                  Power(t1,2)*(505 + 268*t2) + 
                  t1*(341 + 195*t2 - 146*Power(t2,2))) + 
               Power(s2,4)*(815 - 269*Power(t1,4) - 69*t2 + 
                  201*Power(t2,2) - 45*Power(t2,3) - 99*Power(t2,4) + 
                  Power(t1,3)*(644 + 346*t2) + 
                  Power(t1,2)*(423 - 605*t2 - 69*Power(t2,2)) + 
                  t1*(-2139 - 1129*t2 + 1318*Power(t2,2) + 
                     198*Power(t2,3))) + 
               Power(s2,3)*(-555 + 200*Power(t1,5) + 238*t2 + 
                  575*Power(t2,2) - 1259*Power(t2,3) - 
                  100*Power(t2,4) + 41*Power(t2,5) - 
                  Power(t1,4)*(341 + 314*t2) + 
                  Power(t1,3)*(-1475 + 917*t2 + 366*Power(t2,2)) + 
                  Power(t1,2)*
                   (3809 + 967*t2 - 2034*Power(t2,2) - 406*Power(t2,3)) \
+ t1*(-1343 - 461*t2 + 469*Power(t2,2) + 629*Power(t2,3) + 
                     102*Power(t2,4))) - 
               Power(s2,2)*(793 + 83*Power(t1,6) + 455*t2 + 
                  2851*Power(t2,2) + 1473*Power(t2,3) - 
                  309*Power(t2,4) - 183*Power(t2,5) - 42*Power(t2,6) - 
                  4*Power(t1,5)*(1 + 37*t2) + 
                  2*Power(t1,4)*(-584 + 278*t2 + 113*Power(t2,2)) + 
                  Power(t1,3)*
                   (1883 + 182*t2 - 1302*Power(t2,2) - 106*Power(t2,3)) \
+ Power(t1,2)*(1441 + 995*t2 + 1368*Power(t2,2) + 360*Power(t2,3) - 
                     253*Power(t2,4)) + 
                  t1*(-2986 - 1862*t2 - 2731*Power(t2,2) - 
                     1772*Power(t2,3) + 372*Power(t2,4) + 
                     240*Power(t2,5))) + 
               s2*(-184 + 15*Power(t1,7) + Power(t1,6)*(74 - 15*t2) - 
                  2426*t2 - 3474*Power(t2,2) - 436*Power(t2,3) + 
                  939*Power(t2,4) + 64*Power(t2,5) + 98*Power(t2,6) - 
                  8*Power(t2,7) + 
                  Power(t1,5)*(-258 - 50*t2 + 6*Power(t2,2)) + 
                  Power(t1,4)*
                   (-721 + 98*t2 + 157*Power(t2,2) + 79*Power(t2,3)) + 
                  Power(t1,3)*
                   (4191 + 2829*t2 - 502*Power(t2,2) - 
                     660*Power(t2,3) - 235*Power(t2,4)) + 
                  Power(t1,2)*
                   (-5492 - 7930*t2 - 2784*Power(t2,2) + 
                     1160*Power(t2,3) + 795*Power(t2,4) + 
                     186*Power(t2,5)) + 
                  t1*(2382 + 7232*t2 + 7391*Power(t2,2) - 
                     1023*Power(t2,3) - 1024*Power(t2,4) - 
                     414*Power(t2,5) - 28*Power(t2,6))))) + 
         s*(36 - 73*t1 - 386*Power(t1,2) + 1353*Power(t1,3) - 
            1504*Power(t1,4) + 574*Power(t1,5) + 63*Power(t1,6) - 
            57*Power(t1,7) - 6*Power(t1,8) + 
            Power(s1,8)*s2*(-4 - 3*s2 + 3*t1) - 227*t2 + 1394*t1*t2 - 
            2999*Power(t1,2)*t2 + 2675*Power(t1,3)*t2 - 
            555*Power(t1,4)*t2 - 579*Power(t1,5)*t2 + 
            316*Power(t1,6)*t2 - 28*Power(t1,7)*t2 + 4*Power(t1,8)*t2 - 
            306*Power(t2,2) + 869*t1*Power(t2,2) - 
            353*Power(t1,2)*Power(t2,2) - 1016*Power(t1,3)*Power(t2,2) + 
            1430*Power(t1,4)*Power(t2,2) - 803*Power(t1,5)*Power(t2,2) + 
            195*Power(t1,6)*Power(t2,2) - 23*Power(t1,7)*Power(t2,2) + 
            153*Power(t2,3) - 807*t1*Power(t2,3) + 
            1334*Power(t1,2)*Power(t2,3) - 1378*Power(t1,3)*Power(t2,3) + 
            1046*Power(t1,4)*Power(t2,3) - 373*Power(t1,5)*Power(t2,3) + 
            44*Power(t1,6)*Power(t2,3) + 109*Power(t2,4) - 
            199*t1*Power(t2,4) + 540*Power(t1,2)*Power(t2,4) - 
            767*Power(t1,3)*Power(t2,4) + 318*Power(t1,4)*Power(t2,4) - 
            27*Power(t1,5)*Power(t2,4) - 30*Power(t2,5) - 
            171*t1*Power(t2,5) + 334*Power(t1,2)*Power(t2,5) - 
            101*Power(t1,3)*Power(t2,5) - 13*Power(t1,4)*Power(t2,5) + 
            53*Power(t2,6) - 57*t1*Power(t2,6) - 
            25*Power(t1,2)*Power(t2,6) + 22*Power(t1,3)*Power(t2,6) - 
            12*Power(t2,7) + 20*t1*Power(t2,7) - 
            7*Power(t1,2)*Power(t2,7) - 
            Power(s2,8)*(-3 - 4*t2 + Power(t2,2) + t1*(3 + t2)) + 
            Power(s1,7)*(6 - 9*Power(s2,3) - 12*t1 + 7*Power(t1,2) - 
               2*Power(t1,3) + Power(s2,2)*(-28 + 23*t2) + 
               s2*(-18 + 11*Power(t1,2) + t1*(5 - 24*t2) + 33*t2)) - 
            Power(s2,7)*(10 + 32*t2 + 21*Power(t2,2) + Power(t2,3) - 
               Power(t1,2)*(17 + 3*t2) + t1*(7 + 2*t2 - 14*Power(t2,2))) \
+ Power(s2,6)*(2*Power(t1,3)*(-18 + t2) - 
               Power(t1,2)*(49 + 90*t2 + 66*Power(t2,2)) + 
               t1*(129 + 195*t2 + 190*Power(t2,2) + 32*Power(t2,3)) - 
               2*(22 + 3*t2 + 3*Power(t2,2) + 47*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s2,5)*(87 + 148*t2 - 129*Power(t2,2) - 
               85*Power(t2,3) - 17*Power(t2,4) - 2*Power(t2,5) - 
               6*Power(t1,4)*(-5 + 3*t2) + 
               Power(t1,3)*(266 + 333*t2 + 145*Power(t2,2)) - 
               Power(t1,2)*(439 + 630*t2 + 620*Power(t2,2) + 
                  113*Power(t2,3)) + 
               t1*(56 + 15*t2 + 317*Power(t2,2) + 387*Power(t2,3) + 
                  20*Power(t2,4))) + 
            Power(s2,4)*(183 - 92*t2 - 67*Power(t2,2) + 29*Power(t2,3) - 
               72*Power(t2,4) + 84*Power(t2,5) + 7*Power(t2,6) + 
               Power(t1,5)*(5 + 27*t2) - 
               2*Power(t1,4)*(271 + 265*t2 + 80*Power(t2,2)) + 
               Power(t1,3)*(645 + 1186*t2 + 953*Power(t2,2) + 
                  134*Power(t2,3)) + 
               Power(t1,2)*(433 - 189*t2 - 1011*Power(t2,2) - 
                  509*Power(t2,3) + 14*Power(t2,4)) - 
               t1*(724 + 272*t2 - 424*Power(t2,2) - 323*Power(t2,3) + 
                  120*Power(t2,4) + 32*Power(t2,5))) + 
            Power(s2,3)*(-202 - 271*t2 - 190*Power(t2,2) - 
               260*Power(t2,3) + 147*Power(t2,4) + 212*Power(t2,5) + 
               55*Power(t2,6) + 3*Power(t2,7) - 
               Power(t1,6)*(27 + 17*t2) + 
               Power(t1,5)*(568 + 434*t2 + 81*Power(t2,2)) - 
               Power(t1,4)*(396 + 1199*t2 + 707*Power(t2,2) + 
                  26*Power(t2,3)) + 
               Power(t1,3)*(-1221 + 309*t2 + 897*Power(t2,2) + 
                  87*Power(t2,3) - 129*Power(t2,4)) + 
               Power(t1,2)*(1342 + 76*t2 - 160*Power(t2,2) + 
                  181*Power(t2,3) + 580*Power(t2,4) + 125*Power(t2,5)) - 
               t1*(64 - 581*t2 - 293*Power(t2,2) + 316*Power(t2,3) + 
                  430*Power(t2,4) + 383*Power(t2,5) + 37*Power(t2,6))) + 
            Power(s2,2)*(-330 - 391*t2 - 784*Power(t2,2) - 
               441*Power(t2,3) + 480*Power(t2,4) + 305*Power(t2,5) + 
               45*Power(t2,6) + 18*Power(t2,7) - 2*Power(t2,8) + 
               2*Power(t1,7)*(9 + 2*t2) - 
               Power(t1,6)*(314 + 172*t2 + 9*Power(t2,2)) + 
               Power(t1,5)*(-29 + 549*t2 + 188*Power(t2,2) - 
                  52*Power(t2,3)) + 
               Power(t1,4)*(1182 + 259*t2 + 193*Power(t2,2) + 
                  321*Power(t2,3) + 155*Power(t2,4)) - 
               Power(t1,3)*(285 + 331*t2 + 1384*Power(t2,2) + 
                  1378*Power(t2,3) + 768*Power(t2,4) + 141*Power(t2,5)) \
+ Power(t1,2)*(-1773 - 1364*t2 + 13*Power(t2,2) + 2211*Power(t2,3) + 
                  1374*Power(t2,4) + 550*Power(t2,5) + 44*Power(t2,6)) + 
               t1*(1531 + 1499*t2 + 1538*Power(t2,2) - 293*Power(t2,3) - 
                  1528*Power(t2,4) - 554*Power(t2,5) - 155*Power(t2,6) + 
                  Power(t2,7))) - 
            s2*(23 + 4*Power(t1,8) + 594*t2 + 744*Power(t2,2) + 
               53*Power(t2,3) - 344*Power(t2,4) - 216*Power(t2,5) - 
               8*Power(t2,6) - 19*Power(t2,7) + 5*Power(t2,8) + 
               Power(t1,7)*(-81 - 19*t2 + 4*Power(t2,2)) - 
               Power(t1,6)*(157 - 41*t2 + 40*Power(t2,2) + 
                  26*Power(t2,3)) + 
               Power(t1,5)*(469 + 704*t2 + 585*Power(t2,2) + 
                  236*Power(t2,3) + 56*Power(t2,4)) - 
               Power(t1,4)*(-994 + 938*t2 + 2088*Power(t2,2) + 
                  1311*Power(t2,3) + 358*Power(t2,4) + 49*Power(t2,5)) + 
               Power(t1,3)*(-3146 - 1515*t2 + 1692*Power(t2,2) + 
                  3047*Power(t2,3) + 1146*Power(t2,4) + 
                  251*Power(t2,5) + 11*Power(t2,6)) + 
               Power(t1,2)*(2682 + 3667*t2 + 1025*Power(t2,2) - 
                  2402*Power(t2,3) - 2069*Power(t2,4) - 
                  461*Power(t2,5) - 81*Power(t2,6) + 7*Power(t2,7)) + 
               t1*(-788 - 2516*t2 - 1998*Power(t2,2) + 511*Power(t2,3) + 
                  1517*Power(t2,4) + 463*Power(t2,5) + 100*Power(t2,6) + 
                  2*Power(t2,7) - 3*Power(t2,8))) + 
            Power(s1,6)*(33 - 7*Power(s2,4) + 4*Power(t1,4) - 44*t2 + 
               4*Power(t1,3)*(3 + 2*t2) - Power(t1,2)*(19 + 37*t2) + 
               Power(s2,3)*(-5 - 16*t1 + 54*t2) + t1*(-37 + 80*t2) + 
               Power(s2,2)*(56 + 31*Power(t1,2) + 164*t2 - 
                  77*Power(t2,2) + t1*(-99 + 5*t2)) + 
               s2*(62 - 12*Power(t1,3) + Power(t1,2)*(72 - 70*t2) + 
                  112*t2 - 119*Power(t2,2) + 
                  t1*(-129 - 20*t2 + 84*Power(t2,2)))) - 
            Power(s1,5)*(42 + 29*Power(s2,5) - 10*Power(t1,5) + 179*t2 - 
               142*Power(t2,2) - Power(s2,4)*(55 + 112*t1 + 2*t2) + 
               5*Power(t1,4)*(-3 + 4*t2) + 
               Power(t1,2)*(290 - 183*t2 - 87*Power(t2,2)) + 
               2*Power(t1,3)*(-22 + 47*t2 + 5*Power(t2,2)) + 
               4*t1*(-61 - 38*t2 + 60*Power(t2,2)) + 
               Power(s2,3)*(-328 + 121*t1 + 198*Power(t1,2) + 102*t2 - 
                  168*t1*t2 + 138*Power(t2,2)) + 
               Power(s2,2)*(-334 - 170*Power(t1,3) + 386*t2 + 
                  418*Power(t2,2) - 147*Power(t2,3) + 
                  Power(t1,2)*(61 + 215*t2) + 
                  t1*(454 - 694*t2 + 26*Power(t2,2))) + 
               s2*(15 + 65*Power(t1,4) + 294*t2 + 309*Power(t2,2) - 
                  245*Power(t2,3) - 2*Power(t1,3)*(52 + 31*t2) + 
                  Power(t1,2)*(21 + 393*t2 - 192*Power(t2,2)) + 
                  t1*(11 - 682*t2 - 27*Power(t2,2) + 168*Power(t2,3)))) + 
            Power(s1,4)*(-99 - 52*Power(s2,6) + 
               Power(t1,5)*(21 - 30*t2) + 151*t2 + 439*Power(t2,2) - 
               260*Power(t2,3) + 2*Power(s2,5)*(5 + 89*t1 + 25*t2) + 
               Power(t1,4)*(100 - 131*t2 + 40*Power(t2,2)) + 
               Power(t1,3)*(-294 - 113*t2 + 278*Power(t2,2)) - 
               Power(t1,2)*(96 - 1313*t2 + 567*Power(t2,2) + 
                  125*Power(t2,3)) + 
               t1*(342 - 1095*t2 - 295*Power(t2,2) + 420*Power(t2,3)) - 
               Power(s2,4)*(73 + 184*Power(t1,2) + 289*t2 - 
                  69*Power(t2,2) + t1*(-9 + 308*t2)) + 
               Power(s2,3)*(-808 + Power(t1,3) - 1124*t2 + 
                  513*Power(t2,2) + 195*Power(t2,3) + 
                  Power(t1,2)*(15 + 749*t2) + 
                  t1*(876 + 299*t2 - 549*Power(t2,2))) + 
               Power(s2,2)*(-1244 + 95*Power(t1,4) - 873*t2 + 
                  1029*Power(t2,2) + 610*Power(t2,3) - 
                  175*Power(t2,4) - Power(t1,3)*(159 + 725*t2) + 
                  Power(t1,2)*(-1293 + 737*t2 + 594*Power(t2,2)) + 
                  t1*(2440 + 1056*t2 - 1941*Power(t2,2) + 
                     55*Power(t2,3))) + 
               s2*(-723 - 38*Power(t1,5) + 382*t2 + 564*Power(t2,2) + 
                  495*Power(t2,3) - 315*Power(t2,4) + 
                  Power(t1,4)*(104 + 263*t2) + 
                  Power(t1,3)*(346 - 587*t2 - 139*Power(t2,2)) + 
                  Power(t1,2)*
                   (-1301 + 575*t2 + 933*Power(t2,2) - 295*Power(t2,3)) \
+ t1*(1651 - 589*t2 - 1538*Power(t2,2) - 10*Power(t2,3) + 
                     210*Power(t2,4)))) + 
            Power(s1,3)*(415 - 38*Power(s2,7) - 4*Power(t1,7) + 93*t2 - 
               171*Power(t2,2) - 626*Power(t2,3) + 290*Power(t2,4) + 
               4*Power(t1,6)*(3 + t2) + 
               2*Power(s2,6)*(-15 + 64*t1 + 57*t2) + 
               2*Power(t1,5)*(50 - 33*t2 + 15*Power(t2,2)) + 
               Power(s2,5)*(215 + t1 - 143*Power(t1,2) - 96*t2 - 
                  434*t1*t2 + 26*Power(t2,2)) + 
               Power(t1,4)*(155 - 549*t2 + 316*Power(t2,2) - 
                  40*Power(t2,3)) + 
               2*Power(t1,3)*
                (-859 + 861*t2 + 88*Power(t2,2) - 206*Power(t2,3) + 
                  5*Power(t2,4)) - 
               4*t1*(417 + 125*t2 - 498*Power(t2,2) - 100*Power(t2,3) + 
                  115*Power(t2,4)) + 
               Power(t1,2)*(2689 - 600*t2 - 2533*Power(t2,2) + 
                  818*Power(t2,3) + 125*Power(t2,4)) + 
               Power(s2,4)*(257 + 62*Power(t1,3) + 513*t2 + 
                  453*Power(t2,2) - 156*Power(t2,3) + 
                  Power(t1,2)*(258 + 442*t2) + 
                  2*t1*(-509 + 20*t2 + 142*Power(t2,2))) + 
               Power(s2,3)*(751 - 33*Power(t1,4) + 2808*t2 + 
                  1192*Power(t2,2) - 932*Power(t2,3) - 
                  165*Power(t2,4) + 2*Power(t1,3)*(-206 + 71*t2) + 
                  Power(t1,2)*(2121 - 241*t2 - 1184*Power(t2,2)) + 
                  2*t1*(-956 - 1601*t2 + 106*Power(t2,2) + 
                     418*Power(t2,3))) + 
               Power(s2,2)*(1829 + 36*Power(t1,5) + 3493*t2 + 
                  310*Power(t2,2) - 1376*Power(t2,3) - 
                  560*Power(t2,4) + 133*Power(t2,5) - 
                  6*Power(t1,4)*(-32 + 71*t2) + 
                  2*Power(t1,3)*(-954 + 467*t2 + 648*Power(t2,2)) + 
                  Power(t1,2)*
                   (3861 + 3540*t2 - 2395*Power(t2,2) - 
                     846*Power(t2,3)) - 
                  2*t1*(2171 + 3381*t2 - 55*Power(t2,2) - 
                     1402*Power(t2,3) + 30*Power(t2,4))) - 
               s2*(-1574 + 8*Power(t1,6) + Power(t1,5)*(21 - 158*t2) - 
                  1699*t2 + 1272*Power(t2,2) + 556*Power(t2,3) + 
                  500*Power(t2,4) - 259*Power(t2,5) + 
                  Power(t1,4)*(-487 + 573*t2 + 448*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (-1165 - 105*t2 + 694*Power(t2,2) + 88*Power(t2,3)) + 
                  Power(t1,2)*
                   (-5472 - 2047*t2 + 2060*Power(t2,2) + 
                     1242*Power(t2,3) - 275*Power(t2,4)) + 
                  t1*(5078 + 3290*t2 - 2296*Power(t2,2) - 
                     1912*Power(t2,3) + 5*Power(t2,4) + 168*Power(t2,5)))\
) - Power(s1,2)*(502 + 6*Power(s2,8) + 
               Power(s2,7)*(11 - 11*t1 - 70*t2) + 
               Power(t1,7)*(11 - 6*t2) + 672*t2 - 220*Power(t2,2) - 
               27*Power(t2,3) - 539*Power(t2,4) + 196*Power(t2,5) + 
               Power(t1,6)*(-31 - 40*t2 + 8*Power(t2,2)) + 
               Power(t1,5)*(-483 + 585*t2 - 42*Power(t2,2) + 
                  10*Power(t2,3)) - 
               2*Power(t1,4)*
                (-1189 + 315*t2 + 558*Power(t2,2) - 156*Power(t2,3) + 
                  10*Power(t2,4)) + 
               t1*(-2256 - 2565*t2 + 225*Power(t2,2) + 
                  1846*Power(t2,3) + 395*Power(t2,4) - 312*Power(t2,5)) \
+ Power(t1,3)*(-4339 - 2282*t2 + 3329*Power(t2,2) + 290*Power(t2,3) - 
                  328*Power(t2,4) + 8*Power(t2,5)) + 
               Power(t1,2)*(4225 + 4209*t2 - 2028*Power(t2,2) - 
                  2621*Power(t2,3) + 597*Power(t2,4) + 87*Power(t2,5)) + 
               Power(s2,6)*(-214 - 22*Power(t1,2) + 9*t2 + 
                  76*Power(t2,2) + t1*(79 + 242*t2)) + 
               Power(s2,5)*(-71 + 88*Power(t1,3) + 330*t2 - 
                  145*Power(t2,2) + 88*Power(t2,3) - 
                  Power(t1,2)*(381 + 284*t2) + 
                  t1*(748 - 95*t2 - 354*Power(t2,2))) + 
               Power(s2,4)*(551 - 127*Power(t1,4) + 350*t2 + 
                  879*Power(t2,2) + 175*Power(t2,3) - 139*Power(t2,4) + 
                  2*Power(t1,3)*(281 + 91*t2) + 
                  Power(t1,2)*(-697 + 348*t2 + 318*Power(t2,2)) + 
                  t1*(-493 - 1729*t2 + 227*Power(t2,2) + 
                     124*Power(t2,3))) + 
               Power(s2,3)*(823 + 112*Power(t1,5) + 2172*t2 + 
                  3045*Power(t2,2) + 112*Power(t2,3) - 
                  813*Power(t2,4) - 84*Power(t2,5) - 
                  Power(t1,4)*(376 + 205*t2) + 
                  Power(t1,3)*(-309 - 265*t2 + 416*Power(t2,2)) + 
                  Power(t1,2)*
                   (2558 + 3705*t2 - 1017*Power(t2,2) - 
                     1038*Power(t2,3)) + 
                  t1*(-3019 - 4042*t2 - 3346*Power(t2,2) + 
                     1220*Power(t2,3) + 654*Power(t2,4))) + 
               Power(s2,2)*(1270 - 61*Power(t1,6) + 4680*t2 + 
                  2774*Power(t2,2) - 968*Power(t2,3) - 
                  974*Power(t2,4) - 328*Power(t2,5) + 63*Power(t2,6) + 
                  4*Power(t1,5)*(35 + 51*t2) + 
                  Power(t1,4)*(765 - 257*t2 - 722*Power(t2,2)) + 
                  Power(t1,3)*
                   (-3972 - 2778*t2 + 2159*Power(t2,2) + 
                     1238*Power(t2,3)) + 
                  Power(t1,2)*
                   (7139 + 7433*t2 + 1827*Power(t2,2) - 
                     3373*Power(t2,3) - 659*Power(t2,4)) - 
                  t1*(5033 + 10317*t2 + 4676*Power(t2,2) - 
                     2126*Power(t2,3) - 2221*Power(t2,4) + 
                     35*Power(t2,5))) + 
               s2*(1277 + 15*Power(t1,7) + 3387*t2 + 885*Power(t2,2) - 
                  1674*Power(t2,3) - 294*Power(t2,4) - 318*Power(t2,5) + 
                  133*Power(t2,6) - Power(t1,6)*(46 + 63*t2) + 
                  Power(t1,5)*(-262 + 300*t2 + 258*Power(t2,2)) - 
                  Power(t1,4)*
                   (-2451 + 104*t2 + 1192*Power(t2,2) + 416*Power(t2,3)) \
+ Power(t1,3)*(-7126 - 2945*t2 + 1764*Power(t2,2) + 1682*Power(t2,3) + 
                     134*Power(t2,4)) + 
                  2*Power(t1,2)*
                   (4451 + 5193*t2 - 939*Power(t2,2) - 
                     1470*Power(t2,3) - 489*Power(t2,4) + 78*Power(t2,5)\
) - t1*(5284 + 10661*t2 + 110*Power(t2,2) - 3244*Power(t2,3) - 
                     1393*Power(t2,4) + 84*Power(t2,6)))) + 
            s1*(155 - 12*Power(t1,8) + 844*t2 + 104*Power(t2,2) - 
               323*Power(t2,3) + 65*Power(t2,4) - 259*Power(t2,5) + 
               74*Power(t2,6) + Power(s2,8)*(1 - 4*t1 + 7*t2) - 
               2*Power(t1,7)*(-8 - 25*t2 + Power(t2,2)) + 
               4*Power(t1,6)*
                (50 - 65*t2 - 24*Power(t2,2) + Power(t2,3)) + 
               2*Power(t1,5)*(-599 + 81*t2 + 429*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(t1,4)*(2884 + 1653*t2 - 1831*Power(t2,2) - 
                  985*Power(t2,3) + 125*Power(t2,4) - 4*Power(t2,5)) + 
               2*Power(t1,3)*(-1867 - 2213*t2 + 407*Power(t2,2) + 
                  1334*Power(t2,3) + 142*Power(t2,4) - 67*Power(t2,5) + 
                  Power(t2,6)) + 
               Power(t1,2)*(2714 + 5411*t2 + 186*Power(t2,2) - 
                  1872*Power(t2,3) - 1445*Power(t2,4) + 
                  207*Power(t2,5) + 37*Power(t2,6)) - 
               2*t1*(513 + 1710*t2 + 45*Power(t2,2) - 291*Power(t2,3) - 
                  438*Power(t2,4) - 116*Power(t2,5) + 60*Power(t2,6)) + 
               Power(s2,7)*(49 + 25*Power(t1,2) + 37*t2 - 
                  31*Power(t2,2) - t1*(43 + 30*t2)) + 
               Power(s2,6)*(-44 - 68*Power(t1,3) - 197*t2 + 
                  133*Power(t2,2) + 18*Power(t2,3) + 
                  3*Power(t1,2)*(61 + 22*t2) + 
                  2*t1*(-86 - 72*t2 + 41*Power(t2,2))) + 
               Power(s2,5)*(-388 + 102*Power(t1,4) + 10*t2 + 
                  200*Power(t2,2) - 42*Power(t2,3) + 43*Power(t2,4) - 
                  14*Power(t1,3)*(22 + 7*t2) + 
                  Power(t1,2)*(63 + 263*t2 - 28*Power(t2,2)) + 
                  t1*(683 + 496*t2 - 483*Power(t2,2) - 118*Power(t2,3))) \
+ Power(s2,4)*(104 - 88*Power(t1,5) + 474*t2 + 64*Power(t2,2) + 
                  511*Power(t2,3) - 128*Power(t2,4) - 54*Power(t2,5) + 
                  Power(t1,4)*(221 + 75*t2) - 
                  2*Power(t1,3)*(-229 + 141*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*
                   (-1939 - 160*t2 + 599*Power(t2,2) + 46*Power(t2,3)) + 
                  2*t1*(557 - 225*t2 - 517*Power(t2,2) + 
                     149*Power(t2,3) + 34*Power(t2,4))) + 
               Power(s2,3)*(818 + 41*Power(t1,6) + 999*t2 + 
                  1681*Power(t2,2) + 898*Power(t2,3) - 496*Power(t2,4) - 
                  342*Power(t2,5) - 24*Power(t2,6) + 
                  Power(t1,5)*(-9 + 6*t2) + 
                  Power(t1,4)*(-774 + 91*t2 - 146*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (938 - 165*t2 + 30*Power(t2,2) + 201*Power(t2,3)) + 
                  Power(t1,2)*
                   (249 + 1755*t2 + 1403*Power(t2,2) - 
                     1371*Power(t2,3) - 530*Power(t2,4)) + 
                  t1*(-2114 - 2946*t2 - 1814*Power(t2,2) - 
                     590*Power(t2,3) + 1213*Power(t2,4) + 252*Power(t2,5)\
)) - Power(s2,2)*(-644 + 8*Power(t1,7) - 2293*t2 - 3292*Power(t2,2) - 
                  45*Power(t2,3) + 1044*Power(t2,4) + 342*Power(t2,5) + 
                  114*Power(t2,6) - 17*Power(t2,7) + 
                  Power(t1,6)*(89 + 44*t2) - 
                  4*Power(t1,5)*(126 + 40*t2 + 55*Power(t2,2)) + 
                  Power(t1,4)*
                   (245 + 113*t2 + 770*Power(t2,2) + 546*Power(t2,3)) + 
                  Power(t1,3)*
                   (3606 + 2086*t2 - 508*Power(t2,2) - 
                     2152*Power(t2,3) - 638*Power(t2,4)) + 
                  Power(t1,2)*
                   (-6551 - 7551*t2 - 1361*Power(t2,2) + 
                     1794*Power(t2,3) + 2204*Power(t2,4) + 
                     267*Power(t2,5)) + 
                  2*t1*(1902 + 3634*t2 + 2841*Power(t2,2) - 
                     587*Power(t2,3) - 984*Power(t2,4) - 
                     459*Power(t2,5) + 5*Power(t2,6))) + 
               s2*(418 + 2220*t2 + 1866*Power(t2,2) - 435*Power(t2,3) - 
                  985*Power(t2,4) - 78*Power(t2,5) - 117*Power(t2,6) + 
                  39*Power(t2,7) + 2*Power(t1,7)*(28 + 9*t2) - 
                  Power(t1,6)*(144 + 175*t2 + 81*Power(t2,2)) + 
                  Power(t1,5)*
                   (-531 + 564*t2 + 557*Power(t2,2) + 194*Power(t2,3)) + 
                  Power(t1,4)*
                   (3849 + 563*t2 - 1902*Power(t2,2) - 
                     1081*Power(t2,3) - 215*Power(t2,4)) + 
                  2*Power(t1,3)*
                   (-3743 - 3399*t2 + 1216*Power(t2,2) + 
                     1387*Power(t2,3) + 514*Power(t2,4) + 29*Power(t2,5)) \
+ Power(t1,2)*(6484 + 11741*t2 + 2512*Power(t2,2) - 4693*Power(t2,3) - 
                     1895*Power(t2,4) - 429*Power(t2,5) + 50*Power(t2,6)) \
+ t1*(-2628 - 8282*t2 - 5072*Power(t2,2) + 3046*Power(t2,3) + 
                     2011*Power(t2,4) + 566*Power(t2,5) + 5*Power(t2,6) - 
                     24*Power(t2,7))))))*R1(1 - s + s2 - t1))/
     ((-1 + s1)*(-1 + s - s2 + t1)*(s - s2 + t1)*(-s + s1 - t2)*
       (-1 + s1 + t1 - t2)*Power(s1 - s2 + t1 - t2,3)*
       (-1 + s - s*s2 + s1*s2 + t1 - s2*t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) + 
    (16*(Power(s1,10)*s2 - 4*Power(s2,3) + 12*Power(s2,2)*t1 + 
         12*Power(s2,3)*t1 - 12*s2*Power(t1,2) - 
         36*Power(s2,2)*Power(t1,2) - 12*Power(s2,3)*Power(t1,2) + 
         4*Power(t1,3) + 36*s2*Power(t1,3) + 36*Power(s2,2)*Power(t1,3) + 
         4*Power(s2,3)*Power(t1,3) - 12*Power(t1,4) - 36*s2*Power(t1,4) - 
         12*Power(s2,2)*Power(t1,4) + 12*Power(t1,5) + 
         12*s2*Power(t1,5) - 4*Power(t1,6) - 12*Power(s2,2)*t2 + 
         10*Power(s2,3)*t2 - 4*Power(s2,4)*t2 + 24*s2*t1*t2 + 
         6*Power(s2,2)*t1*t2 - 20*Power(s2,3)*t1*t2 + 
         8*Power(s2,4)*t1*t2 - 12*Power(t1,2)*t2 - 42*s2*Power(t1,2)*t2 + 
         48*Power(s2,2)*Power(t1,2)*t2 + 10*Power(s2,3)*Power(t1,2)*t2 - 
         4*Power(s2,4)*Power(t1,2)*t2 + 26*Power(t1,3)*t2 - 
         20*s2*Power(t1,3)*t2 - 66*Power(s2,2)*Power(t1,3)*t2 - 
         4*Power(t1,4)*t2 + 70*s2*Power(t1,4)*t2 + 
         24*Power(s2,2)*Power(t1,4)*t2 - 22*Power(t1,5)*t2 - 
         32*s2*Power(t1,5)*t2 + 12*Power(t1,6)*t2 - 16*s2*Power(t2,2) + 
         12*Power(s2,2)*Power(t2,2) - 4*Power(s2,3)*Power(t2,2) + 
         10*Power(s2,4)*Power(t2,2) + 16*t1*Power(t2,2) + 
         26*s2*t1*Power(t2,2) - 67*Power(s2,2)*t1*Power(t2,2) - 
         8*Power(s2,3)*t1*Power(t2,2) - 24*Power(s2,4)*t1*Power(t2,2) - 
         38*Power(t1,2)*Power(t2,2) + 94*s2*Power(t1,2)*Power(t2,2) + 
         82*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         48*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         14*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         23*Power(t1,3)*Power(t2,2) - 162*s2*Power(t1,3)*Power(t2,2) - 
         51*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         36*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         78*Power(t1,4)*Power(t2,2) + 54*s2*Power(t1,4)*Power(t2,2) + 
         24*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         27*Power(t1,5)*Power(t2,2) + 4*s2*Power(t1,5)*Power(t2,2) - 
         6*Power(t1,6)*Power(t2,2) - 20*Power(t2,3) + 11*s2*Power(t2,3) + 
         32*Power(s2,2)*Power(t2,3) - 14*Power(s2,3)*Power(t2,3) + 
         15*Power(s2,4)*Power(t2,3) + 39*t1*Power(t2,3) - 
         164*s2*t1*Power(t2,3) + 32*Power(s2,2)*t1*Power(t2,3) - 
         71*Power(s2,3)*t1*Power(t2,3) - 2*Power(s2,5)*t1*Power(t2,3) + 
         92*Power(t1,2)*Power(t2,3) + 141*s2*Power(t1,2)*Power(t2,3) + 
         28*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         35*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         149*Power(t1,3)*Power(t2,3) + 23*s2*Power(t1,3)*Power(t2,3) - 
         36*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         18*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         5*Power(t1,4)*Power(t2,3) - 33*s2*Power(t1,4)*Power(t2,3) - 
         28*Power(s2,2)*Power(t1,4)*Power(t2,3) + 
         34*Power(t1,5)*Power(t2,3) + 16*s2*Power(t1,5)*Power(t2,3) - 
         3*Power(t1,6)*Power(t2,3) + 27*Power(t2,4) + 35*s2*Power(t2,4) - 
         56*Power(s2,2)*Power(t2,4) + 82*Power(s2,3)*Power(t2,4) - 
         37*Power(s2,4)*Power(t2,4) + 7*Power(s2,5)*Power(t2,4) - 
         152*t1*Power(t2,4) + 94*s2*t1*Power(t2,4) - 
         145*Power(s2,2)*t1*Power(t2,4) + 74*Power(s2,3)*t1*Power(t2,4) - 
         9*Power(s2,4)*t1*Power(t2,4) + Power(s2,5)*t1*Power(t2,4) + 
         91*Power(t1,2)*Power(t2,4) - 81*s2*Power(t1,2)*Power(t2,4) - 
         8*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         53*Power(s2,3)*Power(t1,2)*Power(t2,4) - 
         8*Power(s2,4)*Power(t1,2)*Power(t2,4) + 
         105*Power(t1,3)*Power(t2,4) + 44*s2*Power(t1,3)*Power(t2,4) + 
         116*Power(s2,2)*Power(t1,3)*Power(t2,4) + 
         18*Power(s2,3)*Power(t1,3)*Power(t2,4) - 
         73*Power(t1,4)*Power(t2,4) - 72*s2*Power(t1,4)*Power(t2,4) - 
         16*Power(s2,2)*Power(t1,4)*Power(t2,4) + 
         11*Power(t1,5)*Power(t2,4) + 5*s2*Power(t1,5)*Power(t2,4) + 
         21*Power(t2,5) - 34*s2*Power(t2,5) + 
         96*Power(s2,2)*Power(t2,5) - 101*Power(s2,3)*Power(t2,5) + 
         37*Power(s2,4)*Power(t2,5) - 9*Power(s2,5)*Power(t2,5) + 
         74*t1*Power(t2,5) - 79*s2*t1*Power(t2,5) + 
         168*Power(s2,2)*t1*Power(t2,5) - 23*Power(s2,3)*t1*Power(t2,5) + 
         35*Power(s2,4)*t1*Power(t2,5) + 2*Power(s2,5)*t1*Power(t2,5) - 
         145*Power(t1,2)*Power(t2,5) - 67*s2*Power(t1,2)*Power(t2,5) - 
         130*Power(s2,2)*Power(t1,2)*Power(t2,5) - 
         59*Power(s2,3)*Power(t1,2)*Power(t2,5) - 
         6*Power(s2,4)*Power(t1,2)*Power(t2,5) + 
         53*Power(t1,3)*Power(t2,5) + 133*s2*Power(t1,3)*Power(t2,5) + 
         48*Power(s2,2)*Power(t1,3)*Power(t2,5) + 
         6*Power(s2,3)*Power(t1,3)*Power(t2,5) - 
         17*Power(t1,4)*Power(t2,5) - 14*s2*Power(t1,4)*Power(t2,5) - 
         2*Power(s2,2)*Power(t1,4)*Power(t2,5) - 
         Power(t1,5)*Power(t2,5) - 38*Power(t2,6) + 42*s2*Power(t2,6) - 
         98*Power(s2,2)*Power(t2,6) + 46*Power(s2,3)*Power(t2,6) - 
         30*Power(s2,4)*Power(t2,6) + 31*t1*Power(t2,6) + 
         126*s2*t1*Power(t2,6) + 6*Power(s2,2)*t1*Power(t2,6) + 
         81*Power(s2,3)*t1*Power(t2,6) + 5*Power(s2,4)*t1*Power(t2,6) - 
         3*Power(t1,2)*Power(t2,6) - 96*s2*Power(t1,2)*Power(t2,6) - 
         73*Power(s2,2)*Power(t1,2)*Power(t2,6) - 
         9*Power(s2,3)*Power(t1,2)*Power(t2,6) + 
         17*Power(t1,3)*Power(t2,6) + 19*s2*Power(t1,3)*Power(t2,6) + 
         3*Power(s2,2)*Power(t1,3)*Power(t2,6) + 
         3*Power(t1,4)*Power(t2,6) + s2*Power(t1,4)*Power(t2,6) + 
         13*Power(t2,7) - 58*s2*Power(t2,7) + 
         23*Power(s2,2)*Power(t2,7) - 33*Power(s2,3)*Power(t2,7) - 
         Power(s2,4)*Power(t2,7) + t1*Power(t2,7) + 5*s2*t1*Power(t2,7) + 
         57*Power(s2,2)*t1*Power(t2,7) + 5*Power(s2,3)*t1*Power(t2,7) - 
         11*Power(t1,2)*Power(t2,7) - 20*s2*Power(t1,2)*Power(t2,7) - 
         Power(s2,2)*Power(t1,2)*Power(t2,7) - 
         3*Power(t1,3)*Power(t2,7) - 3*s2*Power(t1,3)*Power(t2,7) - 
         6*Power(t2,8) + 14*s2*Power(t2,8) - 16*Power(s2,2)*Power(t2,8) - 
         2*Power(s2,3)*Power(t2,8) + 2*t1*Power(t2,8) + 
         14*s2*t1*Power(t2,8) + Power(s2,2)*t1*Power(t2,8) + 
         Power(t1,2)*Power(t2,8) + 3*s2*Power(t1,2)*Power(t2,8) + 
         Power(t2,9) - 4*s2*Power(t2,9) - Power(s2,2)*Power(t2,9) - 
         s2*t1*Power(t2,9) - Power(s1,9)*
          (1 + Power(s2,2) - t1 + Power(t1,2) + s2*(-2 - 7*t1 + 9*t2)) - 
         Power(s1,8)*(2 + Power(t1,3) + Power(s2,2)*(1 + 11*t1 - 6*t2) - 
            8*t2 + t1*(5 + 6*t2) - Power(t1,2)*(5 + 7*t2) + 
            s2*(2 - 16*Power(t1,2) + 16*t2 - 36*Power(t2,2) + 
               t1*(-5 + 56*t2))) + 
         Power(s,7)*(-6 - Power(s1,3)*(-1 + s2) + Power(t1,2) + 
            Power(t1,3) + 2*Power(t1,4) - 2*t1*t2 - 3*Power(t1,2)*t2 - 
            8*Power(t1,3)*t2 + Power(t2,2) + 3*t1*Power(t2,2) + 
            12*Power(t1,2)*Power(t2,2) - Power(t2,3) - 
            8*t1*Power(t2,3) + 2*Power(t2,4) + 
            Power(s2,3)*(1 - t1 + t2) + 
            Power(s2,2)*(-7 + t1 + 4*Power(t1,2) - t2 - 8*t1*t2 + 
               4*Power(t2,2)) + 
            Power(s1,2)*(-7 - 4*Power(s2,2) + t1 + 2*Power(t1,2) - t2 - 
               4*t1*t2 + 2*Power(t2,2) + s2*(11 - t1 + t2)) + 
            s2*(12 - 5*Power(t1,3) + 3*t1*(2 - 5*t2)*t2 - 
               3*Power(t2,2) + 5*Power(t2,3) + 3*Power(t1,2)*(-1 + 5*t2)\
) + s1*(12 - Power(s2,3) + 4*Power(t1,3) + Power(t1,2)*(1 - 12*t2) + 
               Power(s2,2)*(11 + 2*t1 - 2*t2) + Power(t2,2) - 
               4*Power(t2,3) + 2*t1*t2*(-1 + 6*t2) - 
               s2*(22 + 5*Power(t1,2) + t1*(2 - 10*t2) - 2*t2 + 
                  5*Power(t2,2)))) + 
         Power(s1,7)*(3 + 4*Power(s2,4) + Power(t1,4) + 14*t2 - 
            29*Power(t2,2) + Power(t1,3)*(13 + 6*t2) + 
            Power(s2,3)*(-5*t1 + 7*t2) + 
            t1*(-9 + 39*t2 + 14*Power(t2,2)) - 
            Power(t1,2)*(8 + 35*t2 + 21*Power(t2,2)) + 
            Power(s2,2)*(-17 - 11*Power(t1,2) + 22*t2 - 
               13*Power(t2,2) + t1*(2 + 67*t2)) + 
            s2*(5 + 11*Power(t1,3) + 11*t2 + 60*Power(t2,2) - 
               84*Power(t2,3) - 3*Power(t1,2)*(2 + 37*t2) + 
               t1*(13 - 48*t2 + 197*Power(t2,2)))) + 
         Power(s1,6)*(-4 + 2*Power(s2,5) + Power(t1,5) - 
            5*Power(t1,4)*(-3 + t2) - 13*t2 - 48*Power(t2,2) + 
            63*Power(t2,3) - Power(s2,4)*(4 + 4*t1 + 23*t2) - 
            Power(t1,3)*(5 + 82*t2 + 15*Power(t2,2)) + 
            t1*(1 + 56*t2 - 127*Power(t2,2) - 14*Power(t2,3)) + 
            Power(t1,2)*(2 + 44*t2 + 106*Power(t2,2) + 35*Power(t2,3)) + 
            Power(s2,3)*(8 + 7*Power(t1,2) - 18*t2 - 44*Power(t2,2) + 
               4*t1*(1 + 8*t2)) + 
            Power(s2,2)*(-21 - 9*Power(t1,3) + 112*t2 + 
               2*t1*(14 - 85*t2)*t2 - 127*Power(t2,2) + 7*Power(t2,3) + 
               Power(t1,2)*(3 + 66*t2)) + 
            s2*(3*Power(t1,4) - 2*Power(t1,3)*(9 + 35*t2) + 
               Power(t1,2)*(-4 + 15*t2 + 333*Power(t2,2)) - 
               t1*(8 + 52*t2 - 197*Power(t2,2) + 399*Power(t2,3)) + 
               2*(22 - 43*t2 - 5*Power(t2,2) - 70*Power(t2,3) + 
                  63*Power(t2,4)))) + 
         Power(s1,5)*(-27 + Power(s2,5)*(9 - 10*t2) - 
            4*Power(t1,5)*(-2 + t2) + 61*t2 + 7*Power(t2,2) + 
            106*Power(t2,3) - 91*Power(t2,4) + 
            Power(t1,4)*(-1 - 77*t2 + 10*Power(t2,2)) + 
            Power(t1,3)*(-5 + 11*t2 + 218*Power(t2,2) + 
               20*Power(t2,3)) + 
            t1*(16 - 55*t2 - 146*Power(t2,2) + 223*Power(t2,3)) + 
            Power(t1,2)*(24 + 4*t2 - 89*Power(t2,2) - 181*Power(t2,3) - 
               35*Power(t2,4)) + 
            Power(s2,4)*(-23 - 2*Power(t1,2) + 50*t2 + 56*Power(t2,2) + 
               t1*(-20 + 17*t2)) + 
            Power(s2,3)*(6*Power(t1,3) + Power(t1,2)*(9 - 32*t2) + 
               t1*(23 - 88*t2 - 90*Power(t2,2)) + 
               3*(14 - 27*t2 + 41*Power(t2,2) + 39*Power(t2,3))) + 
            Power(s2,2)*(-31 - 6*Power(t1,4) + 177*t2 - 
               328*Power(t2,2) + 356*Power(t2,3) + 21*Power(t2,4) + 
               7*Power(t1,3)*(2 + 7*t2) + 
               Power(t1,2)*(38 + 35*t2 - 164*Power(t2,2)) + 
               t1*(-68 + 8*t2 - 227*Power(t2,2) + 229*Power(t2,3))) + 
            s2*(18 + 2*Power(t1,5) - 262*t2 + 413*Power(t2,2) - 
               59*Power(t2,3) + 224*Power(t2,4) - 126*Power(t2,5) - 
               20*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(-37 + 80*t2 + 188*Power(t2,2)) + 
               Power(t1,2)*(8 + 89*t2 + 35*Power(t2,2) - 
                  563*Power(t2,3)) + 
               t1*(37 - 50*t2 + 60*Power(t2,2) - 454*Power(t2,3) + 
                  511*Power(t2,4)))) + 
         Power(s1,4)*(3 + 2*Power(t1,6) + 133*t2 - 242*Power(t2,2) + 
            55*Power(t2,3) - 160*Power(t2,4) + 91*Power(t2,5) + 
            Power(t1,5)*(-1 - 32*t2 + 6*Power(t2,2)) + 
            Power(s2,5)*(5 + 3*t1 - 45*t2 + 2*t1*t2 + 20*Power(t2,2)) - 
            Power(t1,4)*(33 + 2*t2 - 161*Power(t2,2) + 10*Power(t2,3)) + 
            Power(t1,3)*(66 + 53*t2 + 23*Power(t2,2) - 
               315*Power(t2,3) - 15*Power(t2,4)) + 
            t1*(-56 - 7*t2 + 241*Power(t2,2) + 205*Power(t2,3) - 
               225*Power(t2,4) + 14*Power(t2,5)) + 
            Power(t1,2)*(28 - 220*t2 - 39*Power(t2,2) + 
               65*Power(t2,3) + 190*Power(t2,4) + 21*Power(t2,5)) + 
            Power(s2,4)*(-42 + 2*Power(t1,2)*(-7 + t2) + 127*t2 - 
               190*Power(t2,2) - 75*Power(t2,3) + 
               t1*(10 + 117*t2 - 23*Power(t2,2))) + 
            Power(s2,2)*(-35 + 211*t2 - 596*Power(t2,2) + 
               555*Power(t2,3) - 575*Power(t2,4) - 49*Power(t2,5) + 
               2*Power(t1,4)*(-6 + 11*t2) + 
               Power(t1,3)*(101 - 3*t2 - 103*Power(t2,2)) + 
               Power(t1,2)*(-25 - 291*t2 - 243*Power(t2,2) + 
                  215*Power(t2,3)) + 
               t1*(-98 + 445*t2 - 26*Power(t2,2) + 645*Power(t2,3) - 
                  170*Power(t2,4))) + 
            Power(s2,3)*(Power(t1,3)*(22 - 18*t2) + 
               Power(t1,2)*(-76 - 100*t2 + 49*Power(t2,2)) + 
               t1*(88 - 100*t2 + 393*Power(t2,2) + 145*Power(t2,3)) - 
               5*(-11 + 55*t2 - 58*Power(t2,2) + 69*Power(t2,3) + 
                  34*Power(t2,4))) - 
            s2*(1 + 99*t2 - 650*Power(t2,2) + 950*Power(t2,3) - 
               200*Power(t2,4) + 252*Power(t2,5) - 84*Power(t2,6) + 
               Power(t1,5)*(1 + 8*t2) + 
               Power(t1,4)*(39 - 63*t2 - 51*Power(t2,2)) + 
               Power(t1,3)*(-12 - 266*t2 + 121*Power(t2,2) + 
                  275*Power(t2,3)) + 
               Power(t1,2)*(50 + 80*t2 + 412*Power(t2,2) + 
                  190*Power(t2,3) - 585*Power(t2,4)) + 
               t1*(-88 + 231*t2 - 406*Power(t2,2) - 25*Power(t2,3) - 
                  645*Power(t2,4) + 427*Power(t2,5)))) - 
         Power(s1,3)*(-20 + 36*t2 + 258*Power(t2,2) - 438*Power(t2,3) + 
            135*Power(t2,4) - 162*Power(t2,5) + 63*Power(t2,6) + 
            Power(t1,6)*(-5 + 6*t2) + 
            Power(t1,5)*(39 + 6*t2 - 49*Power(t2,2) + 4*Power(t2,3)) - 
            Power(t1,4)*(21 + 180*t2 + 29*Power(t2,2) - 
               174*Power(t2,3) + 5*Power(t2,4)) + 
            Power(t1,3)*(-84 + 329*t2 + 182*Power(t2,2) + 
               102*Power(t2,3) - 265*Power(t2,4) - 6*Power(t2,5)) + 
            Power(t1,2)*(42 + 151*t2 - 661*Power(t2,2) - 
               76*Power(t2,3) - 30*Power(t2,4) + 125*Power(t2,5) + 
               7*Power(t2,6)) + 
            t1*(47 - 312*t2 + 149*Power(t2,2) + 434*Power(t2,3) + 
               165*Power(t2,4) - 125*Power(t2,5) + 14*Power(t2,6)) + 
            2*Power(s2,5)*(t1*(-1 + 5*t2 + 4*Power(t2,2)) + 
               t2*(11 - 45*t2 + 10*Power(t2,2))) + 
            Power(s2,4)*(17 - 163*t2 + 280*Power(t2,2) - 
               340*Power(t2,3) - 60*Power(t2,4) + 
               Power(t1,2)*(1 - 50*t2 - 12*Power(t2,2)) + 
               t1*(-4 + 21*t2 + 266*Power(t2,2) - 2*Power(t2,3))) + 
            Power(s2,3)*(-33 + 248*t2 - 674*Power(t2,2) + 
               510*Power(t2,3) - 510*Power(t2,4) - 145*Power(t2,5) + 
               Power(t1,3)*(14 + 84*t2 - 12*Power(t2,2)) + 
               Power(t1,2)*(20 - 280*t2 - 305*Power(t2,2) + 
                  16*Power(t2,3)) + 
               t1*(-33 + 336*t2 - 185*Power(t2,2) + 772*Power(t2,3) + 
                  145*Power(t2,4))) + 
            Power(s2,2)*(18 - 171*t2 + 543*Power(t2,2) - 
               1034*Power(t2,3) + 585*Power(t2,4) - 566*Power(t2,5) - 
               49*Power(t2,6) + 
               4*Power(t1,4)*(-7 - 13*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(11 + 415*t2 + 123*Power(t2,2) - 
                  102*Power(t2,3)) + 
               Power(t1,2)*(-60 - 89*t2 - 775*Power(t2,2) - 
                  562*Power(t2,3) + 155*Power(t2,4)) + 
               t1*(87 - 419*t2 + 1095*Power(t2,2) - 24*Power(t2,3) + 
                  940*Power(t2,4) - 61*Power(t2,5))) + 
            s2*(3 + 24*t2 - 223*Power(t2,2) + 860*Power(t2,3) - 
               1215*Power(t2,4) + 283*Power(t2,5) - 196*Power(t2,6) + 
               36*Power(t2,7) + 
               2*Power(t1,5)*(10 + t2 - 6*Power(t2,2)) + 
               Power(t1,4)*(-66 - 184*t2 + 55*Power(t2,2) + 
                  64*Power(t2,3)) + 
               Power(t1,3)*(97 + 96*t2 + 709*Power(t2,2) - 
                  44*Power(t2,3) - 235*Power(t2,4)) + 
               Power(t1,2)*(40 - 278*t2 - 259*Power(t2,2) - 
                  838*Power(t2,3) - 320*Power(t2,4) + 381*Power(t2,5)) + 
               t1*(-100 + 392*t2 - 550*Power(t2,2) + 964*Power(t2,3) + 
                  115*Power(t2,4) + 580*Power(t2,5) - 231*Power(t2,6)))) \
+ Power(s1,2)*(Power(t1,6)*(-2 - 13*t2 + 6*Power(t2,2)) + 
            Power(t1,5)*(-27 + 114*t2 + 26*Power(t2,2) - 
               35*Power(t2,3) + Power(t2,4)) - 
            Power(t1,4)*(-54 + 41*t2 + 334*Power(t2,2) + 
               65*Power(t2,3) - 101*Power(t2,4) + Power(t2,5)) + 
            Power(t1,3)*(9 - 315*t2 + 565*Power(t2,2) + 
               278*Power(t2,3) + 133*Power(t2,4) - 128*Power(t2,5) - 
               Power(t2,6)) + 
            t2*(-60 + 90*t2 + 246*Power(t2,2) - 412*Power(t2,3) + 
               137*Power(t2,4) - 104*Power(t2,5) + 29*Power(t2,6)) + 
            Power(t1,2)*(-50 + 176*t2 + 309*Power(t2,2) - 
               903*Power(t2,3) - 64*Power(t2,4) - 82*Power(t2,5) + 
               50*Power(t2,6) + Power(t2,7)) + 
            t1*(16 + 133*t2 - 608*Power(t2,2) + 329*Power(t2,3) + 
               391*Power(t2,4) + 74*Power(t2,5) - 29*Power(t2,6) + 
               6*Power(t2,7)) + 
            2*Power(s2,5)*t2*
             (t2*(18 - 45*t2 + 5*Power(t2,2)) + 
               t1*(-3 + 6*t2 + 6*Power(t2,2))) + 
            Power(s2,4)*(6 + 49*t2 - 237*Power(t2,2) + 
               308*Power(t2,3) - 320*Power(t2,4) - 29*Power(t2,5) + 
               Power(t1,2)*(10 + t2 - 66*Power(t2,2) - 
                  28*Power(t2,3)) + 
               t1*(-16 - 8*t2 + 3*Power(t2,2) + 296*Power(t2,3) + 
                  22*Power(t2,4))) + 
            Power(s2,3)*(2*Power(t1,3)*
                (-14 + 23*t2 + 60*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(36 + 73*t2 - 385*Power(t2,2) - 
                  405*Power(t2,3) - 31*Power(t2,4)) - 
               t2*(82 - 413*t2 + 792*Power(t2,2) - 480*Power(t2,3) + 
                  420*Power(t2,4) + 72*Power(t2,5)) + 
               t1*(-8 - 133*t2 + 482*Power(t2,2) - 185*Power(t2,3) + 
                  778*Power(t2,4) + 90*Power(t2,5))) + 
            Power(s2,2)*(12*Power(t1,4)*
                (2 - 7*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(-51 - 8*t2 + 643*Power(t2,2) + 
                  247*Power(t2,3) - 43*Power(t2,4)) + 
               Power(t1,2)*(70 - 104*t2 - 111*Power(t2,2) - 
                  959*Power(t2,3) - 623*Power(t2,4) + 56*Power(t2,5)) + 
               t2*(68 - 293*t2 + 673*Power(t2,2) - 981*Power(t2,3) + 
                  382*Power(t2,4) - 337*Power(t2,5) - 27*Power(t2,6)) + 
               t1*(-43 + 212*t2 - 689*Power(t2,2) + 1295*Power(t2,3) + 
                  4*Power(t2,4) + 758*Power(t2,5) - 2*Power(t2,6))) + 
            s2*(-16 + 17*t2 + 86*Power(t2,2) - 255*Power(t2,3) + 
               640*Power(t2,4) - 890*Power(t2,5) + 214*Power(t2,6) - 
               100*Power(t2,7) + 9*Power(t2,8) + 
               Power(t1,5)*(-4 + 56*t2 + 12*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,4)*(58 - 171*t2 - 323*Power(t2,2) - 
                  13*Power(t2,3) + 41*Power(t2,4)) + 
               Power(t1,3)*(-122 + 229*t2 + 200*Power(t2,2) + 
                  901*Power(t2,3) + 64*Power(t2,4) - 116*Power(t2,5)) + 
               Power(t1,2)*(34 + 215*t2 - 487*Power(t2,2) - 
                  377*Power(t2,3) - 872*Power(t2,4) - 269*Power(t2,5) + 
                  151*Power(t2,6)) + 
               t1*(50 - 364*t2 + 614*Power(t2,2) - 634*Power(t2,3) + 
                  1076*Power(t2,4) + 102*Power(t2,5) + 323*Power(t2,6) - 
                  77*Power(t2,7)))) - 
         s1*(Power(t1,6)*(8 - 8*t2 - 11*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,5)*(-10 - 54*t2 + 109*Power(t2,2) + 
               30*Power(t2,3) - 11*Power(t2,4)) + 
            Power(t1,4)*(-16 + 132*t2 - 15*Power(t2,2) - 
               260*Power(t2,3) - 56*Power(t2,4) + 29*Power(t2,5)) + 
            Power(t1,3)*(30 - 14*t2 - 380*Power(t2,2) + 
               407*Power(t2,3) + 197*Power(t2,4) + 77*Power(t2,5) - 
               32*Power(t2,6)) + 
            Power(t2,2)*(-60 + 84*t2 + 115*Power(t2,2) - 
               197*Power(t2,3) + 67*Power(t2,4) - 38*Power(t2,5) + 
               8*Power(t2,6)) + 
            t1*t2*(32 + 125*t2 - 504*Power(t2,2) + 263*Power(t2,3) + 
               175*Power(t2,4) + 16*Power(t2,5) + 3*Power(t2,6) + 
               Power(t2,7)) + 
            Power(t1,2)*(-12 - 88*t2 + 226*Power(t2,2) + 
               277*Power(t2,3) - 583*Power(t2,4) - 24*Power(t2,5) - 
               51*Power(t2,6) + 11*Power(t2,7)) + 
            Power(s2,5)*Power(t2,2)*
             (t2*(26 - 45*t2 + 2*Power(t2,2)) + 
               t1*(-6 + 6*t2 + 8*Power(t2,2))) - 
            Power(s2,4)*(4 - 16*t2 - 47*Power(t2,2) + 153*Power(t2,3) - 
               169*Power(t2,4) + 154*Power(t2,5) + 8*Power(t2,6) + 
               Power(t1,2)*(4 - 24*t2 + Power(t2,2) + 38*Power(t2,3) + 
                  22*Power(t2,4)) + 
               t1*(-8 + 40*t2 + 4*Power(t2,2) + 17*Power(t2,3) - 
                  162*Power(t2,4) - 19*Power(t2,5))) + 
            Power(s2,3)*(6 - 4*t2 - 63*Power(t2,2) + 302*Power(t2,3) - 
               452*Power(t2,4) + 233*Power(t2,5) - 183*Power(t2,6) - 
               19*Power(t2,7) + 
               2*Power(t1,3)*
                (2 - 32*t2 + 25*Power(t2,2) + 38*Power(t2,3) + 
                  9*Power(t2,4)) - 
               2*Power(t1,2)*
                (1 - 42*t2 - 44*Power(t2,2) + 117*Power(t2,3) + 
                  125*Power(t2,4) + 16*Power(t2,5)) + 
               t1*(-8 - 16*t2 - 171*Power(t2,2) + 308*Power(t2,3) - 
                  100*Power(t2,4) + 396*Power(t2,5) + 32*Power(t2,6))) - 
            Power(s2,2)*(12 - 12*t2 - 82*Power(t2,2) + 
               213*Power(t2,3) - 406*Power(t2,4) + 485*Power(t2,5) - 
               142*Power(t2,6) + 112*Power(t2,7) + 8*Power(t2,8) + 
               2*Power(t1,4)*
                (-6 - 24*t2 + 42*Power(t2,2) + 30*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,3)*(30 + 102*t2 + 55*Power(t2,2) - 
                  445*Power(t2,3) - 183*Power(t2,4) + Power(t2,5)) + 
               Power(t1,2)*(-12 - 152*t2 + 16*Power(t2,2) + 
                  55*Power(t2,3) + 567*Power(t2,4) + 339*Power(t2,5) - 
                  6*Power(t2,6)) - 
               t1*(18 - 110*t2 + 157*Power(t2,2) - 513*Power(t2,3) + 
                  745*Power(t2,4) + 16*Power(t2,5) + 323*Power(t2,6) + 
                  5*Power(t2,7))) + 
            s2*(-2*Power(t1,5)*
                (10 - 26*Power(t2,2) - 7*Power(t2,3) + Power(t2,4)) + 
               Power(t1,4)*(34 + 112*t2 - 138*Power(t2,2) - 
                  250*Power(t2,3) - 39*Power(t2,4) + 12*Power(t2,5)) + 
               Power(t1,3)*(16 - 284*t2 + 155*Power(t2,2) + 
                  160*Power(t2,3) + 554*Power(t2,4) + 68*Power(t2,5) - 
                  30*Power(t2,6)) + 
               Power(t1,2)*(-54 + 128*t2 + 316*Power(t2,2) - 
                  340*Power(t2,3) - 257*Power(t2,4) - 457*Power(t2,5) - 
                  115*Power(t2,6) + 33*Power(t2,7)) + 
               t1*(24 + 76*t2 - 428*Power(t2,2) + 404*Power(t2,3) - 
                  357*Power(t2,4) + 586*Power(t2,5) + 38*Power(t2,6) + 
                  102*Power(t2,7) - 14*Power(t2,8)) + 
               t2*(-32 + 25*t2 + 96*Power(t2,2) - 147*Power(t2,3) + 
                  254*Power(t2,4) - 351*Power(t2,5) + 85*Power(t2,6) - 
                  30*Power(t2,7) + Power(t2,8)))) + 
         Power(s,6)*(30 + Power(s1,4)*(-6 + 7*s2) + t1 - 
            20*Power(t1,2) - 4*Power(t1,3) - 6*Power(t1,4) + 
            Power(t1,5) + Power(s2,4)*(-1 + t1 - t2) - 43*t2 + 
            36*t1*t2 + 21*Power(t1,2)*t2 + 33*Power(t1,3)*t2 + 
            5*Power(t1,4)*t2 - 16*Power(t2,2) - 30*t1*Power(t2,2) - 
            63*Power(t1,2)*Power(t2,2) - 30*Power(t1,3)*Power(t2,2) + 
            13*Power(t2,3) + 51*t1*Power(t2,3) + 
            50*Power(t1,2)*Power(t2,3) - 15*Power(t2,4) - 
            35*t1*Power(t2,4) + 9*Power(t2,5) + 
            Power(s2,3)*(-5 - 4*Power(t1,2) - 2*t2 + Power(t2,2) + 
               t1*(11 + 3*t2)) + 
            Power(s1,3)*(31 + 18*Power(s2,2) - 13*Power(t1,2) + 
               s2*(-53 + 12*t1 - 17*t2) + 19*t2 - 9*Power(t2,2) + 
               2*t1*(-7 + 11*t2)) + 
            Power(s2,2)*(38 + 6*Power(t1,3) - 32*t2 - 32*Power(t2,2) + 
               14*Power(t2,3) + Power(t1,2)*(-23 + 2*t2) + 
               t1*(-23 + 55*t2 - 22*Power(t2,2))) + 
            Power(s1,2)*(-38 + 16*Power(s2,3) - 25*Power(t1,3) - 
               40*t2 - 35*Power(t2,2) + 27*Power(t2,3) + 
               Power(s2,2)*(-67 - 30*t1 + 2*t2) + 
               Power(t1,2)*(-30 + 77*t2) + 
               t1*(-3 + 65*t2 - 79*Power(t2,2)) + 
               s2*(95 + 41*Power(t1,2) + t1*(39 - 75*t2) + 32*t2 + 
                  34*Power(t2,2))) + 
            s2*(-61 - 4*Power(t1,4) + Power(t1,3)*(19 - 9*t2) + 76*t2 + 
               28*Power(t2,2) - 46*Power(t2,3) + 21*Power(t2,4) + 
               Power(t1,2)*(32 - 84*t2 + 51*Power(t2,2)) + 
               t1*(12 - 60*t2 + 111*Power(t2,2) - 59*Power(t2,3))) + 
            s1*(-17 + Power(s2,4) - 11*Power(t1,4) + 
               Power(s2,3)*(-11 + 6*t1 - 15*t2) + 84*t2 - 
               4*Power(t2,2) + 37*Power(t2,3) - 27*Power(t2,4) + 
               4*Power(t1,3)*(-7 + 15*t2) + 
               Power(t1,2)*(4 + 93*t2 - 114*Power(t2,2)) + 
               2*t1*(-2 - 51*Power(t2,2) + 46*Power(t2,3)) + 
               Power(s2,2)*(11 - 28*Power(t1,2) + 87*t2 - 
                  34*Power(t2,2) + t1*(-4 + 62*t2)) + 
               s2*(12 + 32*Power(t1,3) + Power(t1,2)*(41 - 109*t2) - 
                  150*t2 + 67*Power(t2,2) - 45*Power(t2,3) + 
                  2*t1*(-2 - 54*t2 + 61*Power(t2,2))))) - 
         Power(s,5)*(54 + 3*Power(s1,5)*(-5 + 7*s2) + t1 - 
            90*Power(t1,2) + 16*Power(t1,3) + 14*Power(t1,4) + 
            Power(t1,5) - 181*t2 + 162*t1*t2 + 84*Power(t1,2)*t2 - 
            23*Power(t1,3)*t2 + 16*Power(t1,4)*t2 - 4*Power(t1,5)*t2 + 
            54*Power(t2,2) - 192*t1*Power(t2,2) - 
            48*Power(t1,2)*Power(t2,2) - 103*Power(t1,3)*Power(t2,2) + 
            92*Power(t2,3) + 109*t1*Power(t2,3) + 
            203*Power(t1,2)*Power(t2,3) + 40*Power(t1,3)*Power(t2,3) - 
            52*Power(t2,4) - 166*t1*Power(t2,4) - 
            80*Power(t1,2)*Power(t2,4) + 49*Power(t2,5) + 
            60*t1*Power(t2,5) - 16*Power(t2,6) - 
            Power(s2,4)*(14 + 2*Power(t1,2) + t1*(-16 + t2) + 7*t2 - 
               5*Power(t2,2)) + 
            Power(s1,4)*(45 + 30*Power(s2,2) - 36*Power(t1,2) + 
               s2*(-101 + 52*t1 - 79*t2) + 72*t2 - 16*Power(t2,2) + 
               t1*(-53 + 50*t2)) + 
            Power(s2,3)*(23 + 6*Power(t1,3) + Power(t1,2)*(-52 + t2) - 
               4*t2 - 25*Power(t2,2) + 8*Power(t2,3) + 
               t1*(29 + 40*t2 - 15*Power(t2,2))) + 
            Power(s1,3)*(-5 + 61*Power(s2,3) - 66*Power(t1,3) - 
               111*t2 - 175*Power(t2,2) + 64*Power(t2,3) + 
               Power(s2,2)*(-166 - 128*t1 + 7*t2) + 
               4*Power(t1,2)*(-32 + 53*t2) + 
               t1*(-50 + 298*t2 - 210*Power(t2,2)) + 
               s2*(154 + 146*Power(t1,2) + t1*(156 - 278*t2) + 158*t2 + 
                  145*Power(t2,2))) + 
            Power(s2,2)*(44 - 6*Power(t1,4) - 
               3*Power(t1,3)*(-19 + t2) - 119*t2 + 24*Power(t2,2) + 
               74*Power(t2,3) - 15*Power(t2,4) + 
               Power(t1,2)*(10 - 61*t2 + 9*Power(t2,2)) + 
               t1*(-121 + 149*t2 - 70*Power(t2,2) + 15*Power(t2,3))) + 
            s2*(-109 + 2*Power(t1,5) + 278*t2 - 122*Power(t2,2) - 
               164*Power(t2,3) + 141*Power(t2,4) - 34*Power(t2,5) + 
               Power(t1,4)*(-22 + 7*t2) + 
               Power(t1,3)*(-39 + 12*t2 + Power(t2,2)) + 
               Power(t1,2)*(82 - 110*t2 + 183*Power(t2,2) - 
                  65*Power(t2,3)) + 
               t1*(100 - 236*t2 + 313*Power(t2,2) - 314*Power(t2,3) + 
                  89*Power(t2,4))) + 
            Power(s1,2)*(-150 + 14*Power(s2,4) - 24*Power(t1,4) + 
               Power(s2,3)*(-44 + 2*t1 - 93*t2) + 179*t2 + 
               35*Power(t2,2) + 243*Power(t2,3) - 96*Power(t2,4) + 
               6*Power(t1,3)*(-19 + 31*t2) + 
               Power(t1,2)*(-23 + 474*t2 - 396*Power(t2,2)) + 
               t1*(49 + 99*t2 - 603*Power(t2,2) + 330*Power(t2,3)) + 
               Power(s2,2)*(-47 - 88*Power(t1,2) + 378*t2 - 
                  119*Power(t2,2) + t1*(26 + 291*t2)) + 
               s2*(191 + 96*Power(t1,3) + Power(t1,2)*(131 - 414*t2) - 
                  584*t2 + 128*Power(t2,2) - 171*Power(t2,3) + 
                  t1*(-5 - 466*t2 + 489*Power(t2,2)))) + 
            s1*(73 + 6*Power(t1,5) + Power(s2,4)*(2 + 6*t1 - 19*t2) + 
               150*t2 - 266*Power(t2,2) + 83*Power(t2,3) - 
               174*Power(t2,4) + 64*Power(t2,5) + 
               Power(t1,4)*(-23 + 20*t2) + 
               Power(s2,3)*(-48 + 16*t1 - 17*Power(t1,2) + 66*t2 + 
                  22*t1*t2 + 24*Power(t2,2)) - 
               2*Power(t1,3)*(20 - 111*t2 + 80*Power(t2,2)) + 
               Power(t1,2)*(-2 + 115*t2 - 549*Power(t2,2) + 
                  300*Power(t2,3)) - 
               2*t1*(30 - 20*t2 + 79*Power(t2,2) - 262*Power(t2,3) + 
                  115*Power(t2,4)) + 
               Power(s2,2)*(151 + 22*Power(t1,3) - 17*t2 - 
                  286*Power(t2,2) + 97*Power(t2,3) + 
                  Power(t1,2)*(-57 + 59*t2) - 
                  2*t1*(25 - 44*t2 + 89*Power(t2,2))) + 
               s2*(-17*Power(t1,4) + Power(t1,3)*(62 - 82*t2) + 
                  9*Power(t1,2)*(14 - 40*t2 + 37*Power(t2,2)) + 
                  t1*(14 - 258*t2 + 624*Power(t2,2) - 352*Power(t2,3)) + 
                  2*(-82 - 37*t2 + 297*Power(t2,2) - 163*Power(t2,3) + 
                     59*Power(t2,4))))) + 
         Power(s,4)*(42 + 5*Power(s1,6)*(-4 + 7*s2) + 3*t1 - 
            156*Power(t1,2) + 73*Power(t1,3) + 50*Power(t1,4) - 
            20*Power(t1,5) + 2*Power(t1,6) - 273*t2 + 295*t1*t2 + 
            251*Power(t1,2)*t2 - 271*Power(t1,3)*t2 + 
            16*Power(t1,4)*t2 - 12*Power(t1,5)*t2 + 311*Power(t2,2) - 
            646*t1*Power(t2,2) + 153*Power(t1,2)*Power(t2,2) + 
            43*Power(t1,3)*Power(t2,2) + 4*Power(t1,4)*Power(t2,2) + 
            6*Power(t1,5)*Power(t2,2) + 112*Power(t2,3) + 
            247*t1*Power(t2,3) + 40*Power(t1,2)*Power(t2,3) + 
            109*Power(t1,3)*Power(t2,3) - 10*Power(t1,4)*Power(t2,3) - 
            179*Power(t2,4) - 173*t1*Power(t2,4) - 
            261*Power(t1,2)*Power(t2,4) - 20*Power(t1,3)*Power(t2,4) + 
            94*Power(t2,5) + 227*t1*Power(t2,5) + 
            60*Power(t1,2)*Power(t2,5) - 69*Power(t2,6) - 
            50*t1*Power(t2,6) + 14*Power(t2,7) + 
            Power(s2,5)*(-5 - 7*t2 + t1*(5 + 2*t2)) + 
            Power(s1,5)*(19*Power(s2,2) - 55*Power(t1,2) + 
               5*s2*(-18 + 23*t1 - 35*t2) + t1*(-91 + 60*t2) + 
               2*(7 + 60*t2 - 7*Power(t2,2))) + 
            Power(s2,4)*(-20 + 2*Power(t1,2)*(-11 + t2) + 32*t2 + 
               13*Power(t2,2) - 11*Power(t2,3) - 
               t1*(-42 + 20*t2 + Power(t2,2))) + 
            Power(s1,4)*(100 + 104*Power(s2,3) - 95*Power(t1,3) - 
               48*t2 - 349*Power(t2,2) + 70*Power(t2,3) + 
               Power(s2,2)*(-219 - 263*t1 + 50*t2) + 
               Power(t1,2)*(-227 + 325*t2) - 
               2*t1*(77 - 290*t2 + 145*Power(t2,2)) + 
               s2*(76 + 290*Power(t1,2) + t1*(289 - 618*t2) + 253*t2 + 
                  376*Power(t2,2))) + 
            Power(s2,3)*(69 + Power(t1,3)*(34 - 18*t2) - 120*t2 - 
               4*Power(t2,2) + 107*Power(t2,3) - 24*Power(t2,4) + 
               Power(t1,2)*(-85 + 117*t2 + 27*Power(t2,2)) + 
               t1*(-12 + 27*t2 - 173*Power(t2,2) + 15*Power(t2,3))) + 
            Power(s2,2)*(-16 - 40*t2 + 128*Power(t2,2) + 
               5*Power(t2,3) - 22*Power(t2,4) - Power(t2,5) + 
               Power(t1,4)*(-20 + 22*t2) + 
               Power(t1,3)*(44 - 158*t2 - 45*Power(t2,2)) + 
               Power(t1,2)*(158 - 212*t2 + 359*Power(t2,2) + 
                  23*Power(t2,3)) + 
               t1*(-184 + 344*t2 - 172*Power(t2,2) - 159*Power(t2,3) + 
                  Power(t2,4))) + 
            s2*(-87 + Power(t1,5)*(1 - 8*t2) + 343*t2 - 
               417*Power(t2,2) + 19*Power(t2,3) + 350*Power(t2,4) - 
               178*Power(t2,5) + 26*Power(t2,6) + 
               Power(t1,4)*(24 + 80*t2 + 13*Power(t2,2)) - 
               Power(t1,3)*(176 - 137*t2 + 203*Power(t2,2) + 
                  17*Power(t2,3)) + 
               Power(t1,2)*(42 + 71*t2 + 64*Power(t2,2) - 
                  16*Power(t2,3) + 53*Power(t2,4)) + 
               t1*(214 - 600*t2 + 566*Power(t2,2) - 575*Power(t2,3) + 
                  316*Power(t2,4) - 67*Power(t2,5))) + 
            Power(s1,3)*(-246 + 50*Power(s2,4) - 25*Power(t1,4) - 
               87*t2 - 34*Power(t2,2) + 596*Power(t2,3) - 
               140*Power(t2,4) - Power(s2,3)*(88 + 37*t1 + 219*t2) + 
               Power(t1,3)*(-183 + 310*t2) + 
               Power(t1,2)*(-131 + 993*t2 - 705*Power(t2,2)) + 
               t1*(151 + 517*t2 - 1421*Power(t2,2) + 560*Power(t2,3)) + 
               Power(s2,2)*(-117 - 163*Power(t1,2) + 681*t2 - 
                  263*Power(t2,2) + t1*(106 + 759*t2)) + 
               s2*(427 + 175*Power(t1,3) + Power(t1,2)*(188 - 977*t2) - 
                  745*t2 - 41*Power(t2,2) - 454*Power(t2,3) + 
                  t1*(-48 - 950*t2 + 1231*Power(t2,2)))) + 
            Power(s1,2)*(-67 + 2*Power(s2,5) + 15*Power(t1,5) + 
               Power(s2,4)*(22 + 10*t1 - 109*t2) + 751*t2 - 
               305*Power(t2,2) + 250*Power(t2,3) - 594*Power(t2,4) + 
               140*Power(t2,5) + Power(t1,4)*(-17 + 25*t2) + 
               Power(t1,3)*(-119 + 484*t2 - 355*Power(t2,2)) + 
               Power(t1,2)*(107 + 459*t2 - 1566*Power(t2,2) + 
                  715*Power(t2,3)) - 
               t1*(29 + 372*t2 + 745*Power(t2,2) - 1693*Power(t2,3) + 
                  540*Power(t2,4)) + 
               Power(s2,3)*(-190 - 21*Power(t1,2) + 291*t2 + 
                  102*Power(t2,2) + 2*t1*(-17 + 61*t2)) + 
               Power(s2,2)*(246 + 19*Power(t1,3) + 83*t2 - 
                  727*Power(t2,2) + 299*Power(t2,3) + 
                  Power(t1,2)*(-31 + 270*t2) + 
                  t1*(145 - 227*t2 - 728*Power(t2,2))) - 
               s2*(112 + 25*Power(t1,4) + 886*t2 - 1612*Power(t2,2) + 
                  495*Power(t2,3) - 331*Power(t2,4) + 
                  4*Power(t1,3)*(-15 + 77*t2) + 
                  Power(t1,2)*(-125 + 559*t2 - 1137*Power(t2,2)) + 
                  t1*(80 + 253*t2 - 1349*Power(t2,2) + 1135*Power(t2,3))\
)) + s1*(189 + Power(s2,5)*(5 - 2*t2) - 202*t2 - 617*Power(t2,2) + 
               471*Power(t2,3) - 276*Power(t2,4) + 316*Power(t2,5) - 
               70*Power(t2,6) - 4*Power(t1,5)*(-3 + 5*t2) + 
               Power(t1,4)*(34 + 9*t2 + 10*Power(t2,2)) + 
               Power(t1,3)*(76 + 55*t2 - 410*Power(t2,2) + 
                  160*Power(t2,3)) - 
               Power(t1,2)*(130 + 161*t2 + 368*Power(t2,2) - 
                  1061*Power(t2,3) + 340*Power(t2,4)) + 
               t1*(-175 + 552*t2 - 26*Power(t2,2) + 555*Power(t2,3) - 
                  988*Power(t2,4) + 260*Power(t2,5)) - 
               Power(s2,4)*(48 + 10*Power(t1,2) + 37*t2 - 
                  70*Power(t2,2) + t1*(-44 + 7*t2)) + 
               Power(s2,3)*(69 + 30*Power(t1,3) + 183*t2 - 
                  310*Power(t2,2) + 37*Power(t2,3) - 
                  3*Power(t1,2)*(59 + 4*t2) + 
                  t1*(60 + 230*t2 - 100*Power(t2,2))) + 
               Power(s2,2)*(151 - 30*Power(t1,4) - 405*t2 + 
                  29*Power(t2,2) + 287*Power(t2,3) - 104*Power(t2,4) + 
                  Power(t1,3)*(214 + 33*t2) - 
                  2*Power(t1,2)*(-53 + 183*t2 + 65*Power(t2,2)) + 
                  t1*(-361 + 71*t2 + 280*Power(t2,2) + 231*Power(t2,3))) \
+ s2*(-295 + 10*Power(t1,5) + 568*t2 + 440*Power(t2,2) - 
                  1293*Power(t2,3) + 551*Power(t2,4) - 139*Power(t2,5) + 
                  Power(t1,4)*(-98 + 8*t2) + 
                  2*Power(t1,3)*(-76 + 82*t2 + 75*Power(t2,2)) + 
                  Power(t1,2)*
                   (192 - 201*t2 + 387*Power(t2,2) - 503*Power(t2,3)) + 
                  2*t1*(142 - 256*t2 + 438*Power(t2,2) - 
                     502*Power(t2,3) + 237*Power(t2,4))))) - 
         Power(s,3)*(12 + 5*Power(s1,7)*(-3 + 7*s2) + 7*t1 - 
            117*Power(t1,2) + 88*Power(t1,3) + 36*Power(t1,4) - 
            35*Power(t1,5) + 7*Power(t1,6) - 175*t2 + 218*t1*t2 + 
            372*Power(t1,2)*t2 - 454*Power(t1,3)*t2 - 3*Power(t1,4)*t2 + 
            36*Power(t1,5)*t2 - 6*Power(t1,6)*t2 + 439*Power(t2,2) - 
            950*t1*Power(t2,2) + 166*Power(t1,2)*Power(t2,2) + 
            498*Power(t1,3)*Power(t2,2) - 110*Power(t1,4)*Power(t2,2) + 
            31*Power(t1,5)*Power(t2,2) - 110*Power(t2,3) + 
            746*t1*Power(t2,3) - 606*Power(t1,2)*Power(t2,3) + 
            62*Power(t1,3)*Power(t2,3) - 51*Power(t1,4)*Power(t2,3) - 
            4*Power(t1,5)*Power(t2,3) - 284*Power(t2,4) - 
            15*t1*Power(t2,4) - 66*Power(t1,2)*Power(t2,4) - 
            21*Power(t1,3)*Power(t2,4) + 10*Power(t1,4)*Power(t2,4) + 
            161*Power(t2,5) + 164*t1*Power(t2,5) + 
            149*Power(t1,2)*Power(t2,5) - 93*Power(t2,6) - 
            150*t1*Power(t2,6) - 20*Power(t1,2)*Power(t2,6) + 
            48*Power(t2,7) + 20*t1*Power(t2,7) - 6*Power(t2,8) - 
            Power(s1,6)*(24 + 4*Power(s2,2) + 50*Power(t1,2) + 
               t1*(79 - 40*t2) - 103*t2 + 6*Power(t2,2) + 
               s2*(27 - 145*t1 + 215*t2)) + 
            Power(s2,5)*(-8 + 8*t2 + 30*Power(t2,2) - 
               8*t1*(-1 + 2*t2 + Power(t2,2))) + 
            Power(s2,4)*(1 + 48*t2 - 56*Power(t2,2) + 43*Power(t2,3) + 
               14*Power(t2,4) + 
               Power(t1,2)*(-19 + 74*t2 + 12*Power(t2,2)) - 
               t1*(-18 + 90*t2 + 71*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s1,5)*(91*Power(s2,3) - 80*Power(t1,3) + 
               Power(s2,2)*(-167 - 302*t1 + 104*t2) + 
               Power(t1,2)*(-193 + 300*t2) + 
               t1*(-195 + 566*t2 - 220*Power(t2,2)) + 
               s2*(-73 + 345*Power(t1,2) + t1*(296 - 842*t2) + 
                  139*t2 + 559*Power(t2,2)) + 
               2*(61 + 60*t2 - 169*Power(t2,2) + 18*Power(t2,3))) + 
            Power(s2,3)*(53 - 197*t2 + 234*Power(t2,2) + 
               12*Power(t2,3) - 103*Power(t2,4) + 31*Power(t2,5) + 
               2*Power(t1,3)*(1 - 60*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(20 + 240*t2 + 20*Power(t2,2) - 
                  38*Power(t2,3)) + 
               t1*(-73 + 32*t2 - 156*Power(t2,2) + 88*Power(t2,3) - 
                  5*Power(t2,4))) - 
            Power(s2,2)*(47 - 157*t2 + 55*Power(t2,2) + 5*Power(t2,3) + 
               9*Power(t2,4) + 57*Power(t2,5) - 14*Power(t2,6) + 
               4*Power(t1,4)*(-7 - 19*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(97 + 206*t2 - 84*Power(t2,2) - 
                  90*Power(t2,3)) + 
               Power(t1,2)*(-199 + 325*t2 - 580*Power(t2,2) + 
                  408*Power(t2,3) + 82*Power(t2,4)) + 
               t1*(89 - 312*t2 + 453*Power(t2,2) + 28*Power(t2,3) - 
                  305*Power(t2,4) - 6*Power(t2,5))) + 
            s2*(-31 + 176*t2 - 318*Power(t2,2) + 309*Power(t2,3) + 
               158*Power(t2,4) - 388*Power(t2,5) + 107*Power(t2,6) - 
               9*Power(t2,7) + 
               2*Power(t1,5)*(-13 - 4*t2 + 6*Power(t2,2)) - 
               2*Power(t1,4)*
                (-51 - 6*t2 + 47*Power(t2,2) + 21*Power(t2,3)) + 
               Power(t1,3)*(-163 + 248*t2 - 258*Power(t2,2) + 
                  328*Power(t2,3) + 63*Power(t2,4)) - 
               Power(t1,2)*(52 - 359*t2 + 384*Power(t2,2) - 
                  116*Power(t2,3) + 235*Power(t2,4) + 57*Power(t2,5)) + 
               t1*(176 - 768*t2 + 835*Power(t2,2) - 624*Power(t2,3) + 
                  544*Power(t2,4) - 98*Power(t2,5) + 33*Power(t2,6))) + 
            Power(s1,4)*(-92 + 80*Power(s2,4) - 10*Power(t1,4) - 
               393*t2 - 333*Power(t2,2) + 670*Power(t2,3) - 
               90*Power(t2,4) + 3*Power(t1,3)*(-39 + 100*t2) - 
               Power(s2,3)*(95 + 83*t1 + 227*t2) + 
               Power(t1,2)*(-198 + 985*t2 - 720*Power(t2,2)) + 
               2*t1*(41 + 458*t2 - 812*Power(t2,2) + 250*Power(t2,3)) + 
               Power(s2,2)*(-97 - 192*Power(t1,2) + 664*t2 - 
                  362*Power(t2,2) + t1*(139 + 1098*t2)) + 
               s2*(442 + 205*Power(t1,3) + 
                  Power(t1,2)*(137 - 1423*t2) - 179*t2 - 
                  179*Power(t2,2) - 795*Power(t2,3) + 
                  t1*(-127 - 1122*t2 + 1951*Power(t2,2)))) + 
            Power(s1,3)*(-297 + 8*Power(s2,5) + 42*Power(t1,4) + 
               20*Power(t1,5) + Power(s2,4)*(40 - 246*t2) + 683*t2 + 
               286*Power(t2,2) + 612*Power(t2,3) - 835*Power(t2,4) + 
               120*Power(t2,5) - 
               2*Power(t1,3)*(71 - 184*t2 + 210*Power(t2,2)) + 
               Power(t1,2)*(85 + 854*t2 - 1946*Power(t2,2) + 
                  880*Power(t2,3)) + 
               t1*(275 - 565*t2 - 1742*Power(t2,2) + 
                  2416*Power(t2,3) - 600*Power(t2,4)) + 
               Power(s2,3)*(-307 + 6*Power(t1,2) + 445*t2 + 
                  104*Power(t2,2) + 4*t1*(-21 + 74*t2)) + 
               Power(s2,2)*(229 - 24*Power(t1,3) + 55*t2 - 
                  933*Power(t2,2) + 488*Power(t2,3) + 
                  Power(t1,2)*(22 + 542*t2) + 
                  t1*(457 - 571*t2 - 1488*Power(t2,2))) - 
               s2*(-45 + 10*Power(t1,4) + 1607*t2 - 1363*Power(t2,2) + 
                  134*Power(t2,3) - 665*Power(t2,4) + 
                  4*Power(t1,3)*(5 + 148*t2) + 
                  Power(t1,2)*(59 + 402*t2 - 2256*Power(t2,2)) + 
                  2*t1*(91 - 128*t2 - 844*Power(t2,2) + 
                     1142*Power(t2,3)))) + 
            Power(s1,2)*(167 + Power(t1,5)*(38 - 40*t2) + 593*t2 - 
               1374*Power(t2,2) + 280*Power(t2,3) - 678*Power(t2,4) + 
               635*Power(t2,5) - 90*Power(t2,6) - 
               8*Power(s2,5)*(-3 + 2*t2) + 
               Power(t1,4)*(37 - 146*t2 + 40*Power(t2,2)) + 
               Power(t1,3)*(87 + 266*t2 - 406*Power(t2,2) + 
                  260*Power(t2,3)) - 
               2*Power(t1,2)*
                (-66 + 238*t2 + 590*Power(t2,2) - 929*Power(t2,3) + 
                  285*Power(t2,4)) + 
               t1*(-357 - 146*t2 + 869*Power(t2,2) + 
                  1680*Power(t2,3) - 1979*Power(t2,4) + 400*Power(t2,5)\
) + Power(s2,4)*(-89 - 20*Power(t1,2) - 43*t2 + 266*Power(t2,2) + 
                  2*t1*(8 + t2)) + 
               Power(s2,3)*(47 + 60*Power(t1,3) + 599*t2 - 
                  708*Power(t2,2) + 140*Power(t2,3) - 
                  2*Power(t1,2)*(105 + 37*t2) + 
                  t1*(79 + 338*t2 - 348*Power(t2,2))) + 
               Power(s2,2)*(237 - 60*Power(t1,4) - 576*t2 + 
                  172*Power(t2,2) + 485*Power(t2,3) - 
                  272*Power(t2,4) + 2*Power(t1,3)*(157 + 83*t2) + 
                  Power(t1,2)*(248 - 589*t2 - 590*Power(t2,2)) + 
                  t1*(-387 - 808*t2 + 1030*Power(t2,2) + 
                     896*Power(t2,3))) + 
               s2*(-293 + 20*Power(t1,5) + 313*t2 + 2046*Power(t2,2) - 
                  2285*Power(t2,3) + 491*Power(t2,4) - 
                  325*Power(t2,5) - 2*Power(t1,4)*(91 + 19*t2) + 
                  Power(t1,3)*(-275 + 440*t2 + 632*Power(t2,2)) + 
                  Power(t1,2)*
                   (174 + 201*t2 + 158*Power(t2,2) - 1680*Power(t2,3)) \
+ t1*(205 - 302*t2 + 413*Power(t2,2) - 1292*Power(t2,3) + 
                     1391*Power(t2,4)))) + 
            s1*(151 + 8*Power(t1,6) - 594*t2 - 186*Power(t2,2) + 
               1067*Power(t2,3) - 456*Power(t2,4) + 396*Power(t2,5) - 
               268*Power(t2,6) + 36*Power(t2,7) + 
               Power(t1,5)*(-51 - 68*t2 + 24*Power(t2,2)) + 
               Power(t1,4)*(107 + 74*t2 + 155*Power(t2,2) - 
                  40*Power(t2,3)) + 
               Power(t1,3)*(251 - 617*t2 - 186*Power(t2,2) + 
                  176*Power(t2,3) - 60*Power(t2,4)) + 
               Power(t1,2)*(-284 - 223*t2 + 997*Power(t2,2) + 
                  590*Power(t2,3) - 853*Power(t2,4) + 180*Power(t2,5)) - 
               t1*(166 - 1248*t2 + 875*Power(t2,2) + 371*Power(t2,3) + 
                  823*Power(t2,4) - 850*Power(t2,5) + 140*Power(t2,6)) + 
               2*Power(s2,5)*
                (-5 - 27*t2 + 4*Power(t2,2) + t1*(9 + 4*t2)) + 
               Power(s2,4)*(-64 + 8*Power(t1,2)*(-10 + t2) + 143*t2 - 
                  40*Power(t2,2) - 114*Power(t2,3) + 
                  t1*(112 + 57*t2 + 4*Power(t2,2))) + 
               Power(s2,3)*(199 + Power(t1,3)*(124 - 72*t2) - 291*t2 - 
                  304*Power(t2,2) + 461*Power(t2,3) - 139*Power(t2,4) + 
                  2*t1*t2*(47 - 171*t2 + 70*Power(t2,2)) + 
                  Power(t1,2)*(-282 + 185*t2 + 106*Power(t2,2))) + 
               Power(s2,2)*(-63 - 183*t2 + 352*Power(t2,2) - 
                  121*Power(t2,3) + 8*Power(t2,4) + 32*Power(t2,5) + 
                  8*Power(t1,4)*(-9 + 11*t2) + 
                  Power(t1,3)*(217 - 393*t2 - 232*Power(t2,2)) + 
                  Power(t1,2)*
                   (387 - 849*t2 + 975*Power(t2,2) + 322*Power(t2,3)) + 
                  t1*(-471 + 851*t2 + 379*Power(t2,2) - 
                     903*Power(t2,3) - 210*Power(t2,4))) + 
               s2*(-180 + Power(t1,5)*(2 - 32*t2) + 646*t2 - 
                  667*Power(t2,2) - 1039*Power(t2,3) + 
                  1562*Power(t2,4) - 397*Power(t2,5) + 85*Power(t2,6) + 
                  Power(t1,4)*(14 + 273*t2 + 90*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (215 - 269*t2 + 374*Power(t2,2) + 154*Power(t2,3)) + 
                  Power(t1,2)*
                   (1 + 241*t2 - 258*Power(t2,2) + 342*Power(t2,3) + 
                     559*Power(t2,4)) - 
                  2*t1*(-281 + 551*t2 - 554*Power(t2,2) + 
                     543*Power(t2,3) - 264*Power(t2,4) + 197*Power(t2,5))\
))) + Power(s,2)*(3*Power(s1,8)*(-2 + 7*s2) + 4*t1 - 32*Power(t1,2) + 
            40*Power(t1,3) - 12*Power(t1,4) - 40*t2 + 43*t1*t2 + 
            233*Power(t1,2)*t2 - 265*Power(t1,3)*t2 - 
            35*Power(t1,4)*t2 + 79*Power(t1,5)*t2 - 17*Power(t1,6)*t2 + 
            241*Power(t2,2) - 556*t1*Power(t2,2) - 
            105*Power(t1,2)*Power(t2,2) + 636*Power(t1,3)*Power(t2,2) - 
            182*Power(t1,4)*Power(t2,2) - Power(t1,5)*Power(t2,2) + 
            6*Power(t1,6)*Power(t2,2) - 257*Power(t2,3) + 
            971*t1*Power(t2,3) - 678*Power(t1,2)*Power(t2,3) - 
            204*Power(t1,3)*Power(t2,3) + 99*Power(t1,4)*Power(t2,3) - 
            31*Power(t1,5)*Power(t2,3) - 139*Power(t2,4) - 
            277*t1*Power(t2,4) + 494*Power(t1,2)*Power(t2,4) - 
            124*Power(t1,3)*Power(t2,4) + 59*Power(t1,4)*Power(t2,4) + 
            Power(t1,5)*Power(t2,4) + 228*Power(t2,5) - 
            103*t1*Power(t2,5) + 94*Power(t1,2)*Power(t2,5) - 
            35*Power(t1,3)*Power(t2,5) - 3*Power(t1,4)*Power(t2,5) - 
            84*Power(t2,6) - 105*t1*Power(t2,6) - 
            29*Power(t1,2)*Power(t2,6) + 2*Power(t1,3)*Power(t2,6) + 
            54*Power(t2,7) + 46*t1*Power(t2,7) + 
            2*Power(t1,2)*Power(t2,7) - 16*Power(t2,8) - 
            3*t1*Power(t2,8) + Power(t2,9) - 
            Power(s1,7)*(24 + 12*Power(s2,2) + 27*Power(t1,2) + 
               t1*(32 - 14*t2) - 45*t2 + Power(t2,2) + 
               s2*(-13 - 106*t1 + 151*t2)) + 
            Power(s1,6)*(40 + 40*Power(s2,3) - 39*Power(t1,3) + 
               148*t2 - 160*Power(t2,2) + 7*Power(t2,3) + 
               Power(s2,2)*(-73 - 200*t1 + 94*t2) + 
               Power(t1,2)*(-68 + 167*t2) + 
               t1*(-118 + 265*t2 - 87*Power(t2,2)) + 
               s2*(-103 + 245*Power(t1,2) + t1*(171 - 683*t2) - 37*t2 + 
                  466*Power(t2,2))) + 
            2*Power(s2,5)*(-2 + 7*t2 + 3*Power(t2,2) - 24*Power(t2,3) + 
               t1*(2 - 9*t2 + 9*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s2,4)*(16 - 8*t2 - 86*Power(t2,2) + 109*Power(t2,3) - 
               122*Power(t2,4) - 11*Power(t2,5) + 
               Power(t1,2)*(4 + 37*t2 - 90*Power(t2,2) - 
                  28*Power(t2,3)) + 
               t1*(-20 - 13*t2 + 45*Power(t2,2) + 173*Power(t2,3) + 
                  19*Power(t2,4))) + 
            Power(s2,3)*(5 - 64*t2 + 250*Power(t2,2) - 
               333*Power(t2,3) + 75*Power(t2,4) - 32*Power(t2,5) - 
               23*Power(t2,6) + 
               2*Power(t1,3)*
                (-18 + 7*t2 + 78*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(81 - 37*t2 - 278*Power(t2,2) - 
                  242*Power(t2,3) + 2*Power(t2,4)) + 
               t1*(-50 + 61*t2 + 110*Power(t2,2) + 92*Power(t2,3) + 
                  209*Power(t2,4) + 9*Power(t2,5))) + 
            Power(s2,2)*(-18 + 129*t2 - 190*Power(t2,2) + 
               179*Power(t2,3) - 224*Power(t2,4) + 48*Power(t2,5) + 
               22*Power(t2,6) - 12*Power(t2,7) + 
               4*Power(t1,4)*
                (11 - 21*t2 - 27*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,3)*(-86 + 136*t2 + 396*Power(t2,2) + 
                  126*Power(t2,3) - 60*Power(t2,4)) + 
               Power(t1,2)*(46 - 199*t2 + 114*Power(t2,2) - 
                  694*Power(t2,3) + 25*Power(t2,4) + 72*Power(t2,5)) + 
               t1*(14 + 36*t2 - 358*Power(t2,2) + 620*Power(t2,3) + 
                  97*Power(t2,4) - 65*Power(t2,5) - 12*Power(t2,6))) + 
            s2*(-4 + 43*t2 - 61*Power(t2,2) + 129*Power(t2,3) - 
               81*Power(t2,4) - 273*Power(t2,5) + 256*Power(t2,6) - 
               36*Power(t2,7) + Power(t2,8) - 
               2*Power(t1,5)*
                (8 - 34*t2 - 9*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*t2*
                (181 + 44*t2 + 394*Power(t2,2) - 171*Power(t2,3) - 
                  67*Power(t2,4)) + 
               Power(t1,4)*(29 - 179*t2 - 168*Power(t2,2) + 
                  22*Power(t2,3) + 38*Power(t2,4)) + 
               Power(t1,2)*(-59 + 299*t2 - 591*Power(t2,2) + 
                  108*Power(t2,3) - 240*Power(t2,4) + 
                  168*Power(t2,5) + 53*Power(t2,6)) - 
               t1*(-50 + 414*t2 - 734*Power(t2,2) + 573*Power(t2,3) - 
                  628*Power(t2,4) + 310*Power(t2,5) + Power(t2,6) + 
                  17*Power(t2,7))) + 
            Power(s1,5)*(60 + 65*Power(s2,4) + 3*Power(t1,4) - 200*t2 - 
               434*Power(t2,2) + 351*Power(t2,3) - 21*Power(t2,4) + 
               6*Power(t1,3)*(1 + 28*t2) - 
               Power(s2,3)*(53 + 76*t1 + 93*t2) - 
               3*Power(t1,2)*(43 - 135*t2 + 144*Power(t2,2)) + 
               t1*(-80 + 724*t2 - 891*Power(t2,2) + 228*Power(t2,3)) + 
               Power(s2,2)*(-61 - 142*Power(t1,2) + 390*t2 - 
                  278*Power(t2,2) + t1*(80 + 888*t2)) + 
               s2*(243 + 150*Power(t1,3) + Power(t1,2)*(41 - 1227*t2) + 
                  292*t2 + 26*Power(t2,2) - 801*Power(t2,3) + 
                  t1*(-97 - 802*t2 + 1842*Power(t2,2)))) + 
            Power(s1,4)*(-232 + 12*Power(s2,5) + 15*Power(t1,5) + 
               Power(s2,4)*(23 - 15*t1 - 259*t2) + 
               Power(t1,4)*(88 - 25*t2) + 5*t2 + 316*Power(t2,2) + 
               790*Power(t2,3) - 510*Power(t2,4) + 35*Power(t2,5) - 
               Power(t1,3)*(72 + 75*t2 + 280*Power(t2,2)) + 
               Power(t1,2)*(-47 + 710*t2 - 969*Power(t2,2) + 
                  600*Power(t2,3)) + 
               t1*(316 + 79*t2 - 1821*Power(t2,2) + 1600*Power(t2,3) - 
                  325*Power(t2,4)) + 
               Power(s2,3)*(-203 + 34*Power(t1,2) + 273*t2 - 
                  51*Power(t2,2) + t1*(-53 + 331*t2)) + 
               Power(s2,2)*(102 - 56*Power(t1,3) + 97*t2 - 
                  808*Power(t2,2) + 400*Power(t2,3) + 
                  Power(t1,2)*(27 + 566*t2) - 
                  2*t1*(-197 + 178*t2 + 782*Power(t2,2))) + 
               s2*(10*Power(t1,4) - Power(t1,3)*(85 + 613*t2) + 
                  3*Power(t1,2)*(-51 - 44*t2 + 837*Power(t2,2)) + 
                  t1*(-136 + 459*t2 + 1497*Power(t2,2) - 
                     2675*Power(t2,3)) + 
                  2*(86 - 675*t2 + 59*Power(t2,2) - 15*Power(t2,3) + 
                     415*Power(t2,4)))) + 
            Power(s1,3)*(-37 + Power(t1,5)*(52 - 40*t2) + 928*t2 - 
               603*Power(t2,2) - 64*Power(t2,3) - 940*Power(t2,4) + 
               491*Power(t2,5) - 35*Power(t2,6) - 
               6*Power(s2,5)*(-7 + 6*t2) + 
               Power(t1,4)*(27 - 332*t2 + 60*Power(t2,2)) + 
               Power(t1,3)*(25 + 246*t2 + 224*Power(t2,2) + 
                  220*Power(t2,3)) + 
               Power(t1,2)*(198 - 59*t2 - 1450*Power(t2,2) + 
                  1186*Power(t2,3) - 475*Power(t2,4)) + 
               t1*(-112 - 989*t2 + 346*Power(t2,2) + 
                  2404*Power(t2,3) - 1670*Power(t2,4) + 270*Power(t2,5)\
) + Power(s2,4)*(-113 - 20*Power(t1,2) + 47*t2 + 398*Power(t2,2) + 
                  t1*(-56 + 38*t2)) + 
               Power(s2,3)*(36 + 60*Power(t1,3) + 518*t2 - 
                  469*Power(t2,2) + 334*Power(t2,3) - 
                  4*Power(t1,2)*(22 + 35*t2) + 
                  t1*(117 + 58*t2 - 546*Power(t2,2))) + 
               Power(s2,2)*(119 - 60*Power(t1,4) - 247*t2 + 
                  27*Power(t2,2) + 792*Power(t2,3) - 280*Power(t2,4) + 
                  6*Power(t1,3)*(38 + 45*t2) + 
                  Power(t1,2)*(256 - 289*t2 - 918*Power(t2,2)) + 
                  t1*(-256 - 1129*t2 + 653*Power(t2,2) + 
                     1376*Power(t2,3))) + 
               s2*(-198 + 20*Power(t1,5) - 376*t2 + 2865*Power(t2,2) - 
                  1332*Power(t2,3) + 145*Power(t2,4) - 
                  521*Power(t2,5) - 2*Power(t1,4)*(89 + 46*t2) + 
                  Power(t1,3)*(-287 + 516*t2 + 1006*Power(t2,2)) + 
                  Power(t1,2)*
                   (78 + 642*t2 - 18*Power(t2,2) - 2674*Power(t2,3)) + 
                  t1*(185 - 166*t2 - 485*Power(t2,2) - 
                     1388*Power(t2,3) + 2230*Power(t2,4)))) + 
            Power(s1,2)*(173 + 12*Power(t1,6) - 155*t2 - 
               1299*Power(t2,2) + 1179*Power(t2,3) - 304*Power(t2,4) + 
               704*Power(t2,5) - 300*Power(t2,6) + 21*Power(t2,7) + 
               Power(t1,5)*(-43 - 132*t2 + 36*Power(t2,2)) + 
               Power(t1,4)*(51 + 58*t2 + 459*Power(t2,2) - 
                  60*Power(t2,3)) - 
               Power(t1,3)*(-279 + 358*t2 + 400*Power(t2,2) + 
                  282*Power(t2,3) + 75*Power(t2,4)) + 
               Power(t1,2)*(-61 - 879*t2 + 753*Power(t2,2) + 
                  1380*Power(t2,3) - 774*Power(t2,4) + 207*Power(t2,5)) \
+ t1*(-356 + 1054*t2 + 753*Power(t2,2) - 712*Power(t2,3) - 
                  1756*Power(t2,4) + 1017*Power(t2,5) - 129*Power(t2,6)\
) + 12*Power(s2,5)*(t1*(2 + t2) + t2*(-11 + 3*t2)) + 
               Power(s2,4)*(-119 + 12*Power(t1,2)*(-9 + t2) + 329*t2 - 
                  285*Power(t2,2) - 290*Power(t2,3) - 
                  3*t1*(-36 - 97*t2 + 4*Power(t2,2))) + 
               Power(s2,3)*(249 - 429*t2 - 352*Power(t2,2) + 
                  299*Power(t2,3) - 366*Power(t2,4) - 
                  12*Power(t1,3)*(-14 + 9*t2) + 
                  Power(t1,2)*(-385 - 81*t2 + 180*Power(t2,2)) + 
                  t1*(146 - 93*t2 + 252*Power(t2,2) + 412*Power(t2,3))) \
+ Power(s2,2)*(-14 - 58*t2 - 36*Power(t2,2) - 53*Power(t2,3) - 
                  333*Power(t2,4) + 62*Power(t2,5) + 
                  12*Power(t1,4)*(-8 + 11*t2) + 
                  Power(t1,3)*(403 - 315*t2 - 432*Power(t2,2)) + 
                  Power(t1,2)*
                   (302 - 1257*t2 + 522*Power(t2,2) + 778*Power(t2,3)) \
+ t1*(-641 + 1141*t2 + 1173*Power(t2,2) - 623*Power(t2,3) - 
                     624*Power(t2,4))) + 
               s2*(-117 + 608*t2 - 48*Power(t1,5)*t2 + 
                  155*Power(t2,2) - 2925*Power(t2,3) + 
                  1913*Power(t2,4) - 233*Power(t2,5) + 
                  186*Power(t2,6) + 
                  Power(t1,4)*(-83 + 369*t2 + 192*Power(t2,2)) - 
                  Power(t1,3)*
                   (380 - 963*t2 + 948*Power(t2,2) + 840*Power(t2,3)) + 
                  Power(t1,2)*
                   (58 + 69*t2 - 1065*Power(t2,2) + 436*Power(t2,3) + 
                     1551*Power(t2,4)) + 
                  t1*(430 - 1107*t2 + 1368*Power(t2,2) - 
                     329*Power(t2,3) + 637*Power(t2,4) - 
                     1041*Power(t2,5)))) - 
            s1*(-40 + 414*t2 - 449*Power(t2,2) - 742*Power(t2,3) + 
               869*Power(t2,4) - 296*Power(t2,5) + 298*Power(t2,6) - 
               105*Power(t2,7) + 7*Power(t2,8) + 
               Power(t1,6)*(-19 + 18*t2) + 
               Power(t1,5)*(99 - 46*t2 - 111*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,4)*(-103 - 129*t2 + 184*Power(t2,2) + 
                  274*Power(t2,3) - 25*Power(t2,4)) - 
               Power(t1,3)*(185 - 925*t2 + 537*Power(t2,2) + 
                  350*Power(t2,3) + 162*Power(t2,4) + 4*Power(t2,5)) + 
               Power(t1,2)*(207 - 184*t2 - 1359*Power(t2,2) + 
                  1141*Power(t2,3) + 605*Power(t2,4) - 
                  249*Power(t2,5) + 42*Power(t2,6)) - 
               t1*(-39 + 904*t2 - 1913*Power(t2,2) + 197*Power(t2,3) + 
                  470*Power(t2,4) + 672*Power(t2,5) - 335*Power(t2,6) + 
                  32*Power(t2,7)) + 
               2*Power(s2,5)*
                (7 + 3*t2 - 69*Power(t2,2) + 6*Power(t2,3) + 
                  3*t1*(-3 + 7*t2 + 4*Power(t2,2))) + 
               Power(s2,4)*(2 - 205*t2 + 325*Power(t2,2) - 
                  337*Power(t2,3) - 97*Power(t2,4) - 
                  3*Power(t1,2)*(-13 + 66*t2 + 12*Power(t2,2)) + 
                  t1*(-25 + 153*t2 + 408*Power(t2,2) + 30*Power(t2,3))) \
+ Power(s2,3)*(-86 + 504*t2 - 726*Power(t2,2) + 38*Power(t2,3) + 
                  18*Power(t2,4) - 159*Power(t2,5) + 
                  Power(t1,3)*(10 + 324*t2 - 36*Power(t2,2)) + 
                  Power(t1,2)*
                   (-19 - 662*t2 - 411*Power(t2,2) + 76*Power(t2,3)) + 
                  t1*(69 + 250*t2 + 116*Power(t2,2) + 466*Power(t2,3) + 
                     130*Power(t2,4))) + 
               Power(s2,2)*(103 - 208*t2 + 240*Power(t2,2) - 
                  405*Power(t2,3) + 58*Power(t2,4) - 10*Power(t2,5) - 
                  26*Power(t2,6) + 
                  12*Power(t1,4)*(-7 - 17*t2 + 7*Power(t2,2)) + 
                  Power(t1,3)*
                   (156 + 795*t2 + 39*Power(t2,2) - 278*Power(t2,3)) + 
                  Power(t1,2)*
                   (-305 + 428*t2 - 1695*Power(t2,2) + 
                     285*Power(t2,3) + 356*Power(t2,4)) + 
                  t1*(148 - 1003*t2 + 1505*Power(t2,2) + 
                     535*Power(t2,3) - 311*Power(t2,4) - 136*Power(t2,5)\
)) + s2*(47 - 186*t2 + 539*Power(t2,2) - 130*Power(t2,3) - 
                  1440*Power(t2,4) + 1144*Power(t2,5) - 
                  152*Power(t2,6) + 31*Power(t2,7) + 
                  18*Power(t1,5)*(4 + t2 - 2*Power(t2,2)) + 
                  Power(t1,4)*
                   (-225 - 246*t2 + 213*Power(t2,2) + 148*Power(t2,3)) + 
                  Power(t1,3)*
                   (337 - 344*t2 + 1070*Power(t2,2) - 688*Power(t2,3) - 
                     364*Power(t2,4)) + 
                  Power(t1,2)*
                   (129 - 544*t2 + 255*Power(t2,2) - 816*Power(t2,3) + 
                     495*Power(t2,4) + 459*Power(t2,5)) + 
                  t1*(-362 + 1186*t2 - 1495*Power(t2,2) + 
                     1694*Power(t2,3) - 762*Power(t2,4) + 
                     114*Power(t2,5) - 238*Power(t2,6))))) + 
         s*(Power(s1,9)*(1 - 7*s2) - 10*Power(t1,3) + 28*Power(t1,4) - 
            26*Power(t1,5) + 8*Power(t1,6) + 8*t1*t2 - 
            34*Power(t1,2)*t2 + 9*Power(t1,3)*t2 + 30*Power(t1,4)*t2 - 
            7*Power(t1,5)*t2 - 6*Power(t1,6)*t2 - 44*Power(t2,2) + 
            77*t1*Power(t2,2) + 160*Power(t1,2)*Power(t2,2) - 
            222*Power(t1,3)*Power(t2,2) - 34*Power(t1,4)*Power(t2,2) + 
            78*Power(t1,5)*Power(t2,2) - 13*Power(t1,6)*Power(t2,2) + 
            135*Power(t2,3) - 435*t1*Power(t2,3) + 
            86*Power(t1,2)*Power(t2,3) + 400*Power(t1,3)*Power(t2,3) - 
            208*Power(t1,4)*Power(t2,3) + 26*Power(t1,5)*Power(t2,3) + 
            2*Power(t1,6)*Power(t2,3) - 32*Power(t2,4) + 
            451*t1*Power(t2,4) - 516*Power(t1,2)*Power(t2,4) + 
            92*Power(t1,3)*Power(t2,4) + 2*Power(t1,4)*Power(t2,4) - 
            12*Power(t1,5)*Power(t2,4) - 139*Power(t2,5) + 
            21*t1*Power(t2,5) + 102*Power(t1,2)*Power(t2,5) - 
            29*Power(t1,3)*Power(t2,5) + 25*Power(t1,4)*Power(t2,5) + 
            86*Power(t2,6) - 26*t1*Power(t2,6) + 
            29*Power(t1,2)*Power(t2,6) - 21*Power(t1,3)*Power(t2,6) - 
            32*Power(t2,7) - 31*t1*Power(t2,7) + 
            3*Power(t1,2)*Power(t2,7) + 16*Power(t2,8) + 
            5*t1*Power(t2,8) - 2*Power(t2,9) + 
            Power(s1,8)*(8 + 6*Power(s2,2) + 8*Power(t1,2) + 
               t1*(3 - 2*t2) - 8*t2 + s2*(-11 - 42*t1 + 57*t2)) + 
            2*Power(s2,5)*t2*(-2 + 3*t2 + 8*Power(t2,2) - 
               17*Power(t2,3) + 
               t1*(2 - 6*t2 + 4*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,4)*(-8 + 22*t2 + 8*Power(t2,2) - 95*Power(t2,3) + 
               108*Power(t2,4) - 104*Power(t2,5) - 5*Power(t2,6) + 
               Power(t1,2)*(-8 + 18*t2 + 17*Power(t2,2) - 
                  46*Power(t2,3) - 22*Power(t2,4)) + 
               t1*(16 - 40*t2 + 5*Power(t2,2) - 12*Power(t2,3) + 
                  133*Power(t2,4) + 17*Power(t2,5))) + 
            Power(s2,3)*(10 - 27*t2 - 49*Power(t2,2) + 204*Power(t2,3) - 
               297*Power(t2,4) + 120*Power(t2,5) - 89*Power(t2,6) - 
               10*Power(t2,7) + 
               2*Power(t1,3)*
                (8 - 36*t2 + 17*Power(t2,2) + 44*Power(t2,3) + 
                  9*Power(t2,4)) - 
               Power(t1,2)*(22 - 97*t2 - 18*Power(t2,2) + 
                  176*Power(t2,3) + 216*Power(t2,4) + 21*Power(t2,5)) + 
               t1*(-4 + 2*t2 - 67*Power(t2,2) + 204*Power(t2,3) - 
                  31*Power(t2,4) + 256*Power(t2,5) + 13*Power(t2,6))) - 
            Power(s2,2)*(2*Power(t1,4)*t2*
                (-34 + 42*t2 + 34*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(30 + 65*t2 - 3*Power(t2,2) - 
                  350*Power(t2,3) - 157*Power(t2,4) + 9*Power(t2,5)) + 
               Power(t1,2)*(-60 + 28*t2 + 44*Power(t2,2) + 
                  61*Power(t2,3) + 446*Power(t2,4) + 181*Power(t2,5) - 
                  19*Power(t2,6)) + 
               t1*(30 - 31*t2 - 135*Power(t2,2) + 351*Power(t2,3) - 
                  558*Power(t2,4) - 29*Power(t2,5) - 122*Power(t2,6) + 
                  3*Power(t2,7)) + 
               t2*(6 - 54*t2 + 153*Power(t2,2) - 224*Power(t2,3) + 
                  280*Power(t2,4) - 66*Power(t2,5) + 30*Power(t2,6) + 
                  5*Power(t2,7))) - 
            s2*(2*Power(t1,5)*
                (8 + 6*t2 - 29*Power(t2,2) - 8*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,4)*(-62 - 19*t2 + 110*Power(t2,2) + 
                  204*Power(t2,3) + 28*Power(t2,4) - 13*Power(t2,5)) + 
               Power(t1,3)*(76 + 26*t2 - 137*Power(t2,2) - 
                  160*Power(t2,3) - 367*Power(t2,4) - 4*Power(t2,5) + 
                  27*Power(t2,6)) + 
               Power(t1,2)*(-30 + 13*t2 - 148*Power(t2,2) + 
                  335*Power(t2,3) + 190*Power(t2,4) + 234*Power(t2,5) + 
                  Power(t2,6) - 23*Power(t2,7)) + 
               t1*t2*(-40 + 282*t2 - 438*Power(t2,2) + 
                  301*Power(t2,3) - 472*Power(t2,4) + 83*Power(t2,5) - 
                  22*Power(t2,6) + 7*Power(t2,7)) + 
               t2*(8 - 23*t2 - 11*Power(t2,2) + 29*Power(t2,3) - 
                  70*Power(t2,4) + 212*Power(t2,5) - 96*Power(t2,6) + 
                  13*Power(t2,7))) + 
            Power(s1,7)*(5 - 7*Power(s2,3) + 10*Power(t1,3) + 
               Power(s2,2)*(16 + 72*t1 - 39*t2) - 59*t2 + 
               30*Power(t2,2) - 2*Power(t1,2)*(1 + 26*t2) + 
               t1*(35 - 38*t2 + 14*Power(t2,2)) + 
               s2*(37 - 96*Power(t1,2) + 63*t2 - 203*Power(t2,2) + 
                  t1*(-50 + 302*t2))) - 
            Power(s1,6)*(36 + 26*Power(s2,4) + 4*Power(t1,4) + 25*t2 - 
               202*Power(t2,2) + 70*Power(t2,3) + 
               Power(s2,3)*(-12 - 32*t1 + t2) + 
               10*Power(t1,3)*(4 + 5*t2) - 
               6*Power(t1,2)*(7 + t2 + 24*Power(t2,2)) + 
               t1*(-73 + 263*t2 - 170*Power(t2,2) + 42*Power(t2,3)) + 
               Power(s2,2)*(-47 - 60*Power(t1,2) + 137*t2 - 
                  103*Power(t2,2) + t1*(20 + 379*t2)) + 
               s2*(66 + 62*Power(t1,3) + 171*t2 + 160*Power(t2,2) - 
                  413*Power(t2,3) - Power(t1,2)*(7 + 572*t2) + 
                  t1*(-4 - 314*t2 + 937*Power(t2,2)))) + 
            Power(s1,5)*(57 - 8*Power(s2,5) - 6*Power(t1,5) + 115*t2 + 
               82*Power(t2,2) - 421*Power(t2,3) + 112*Power(t2,4) + 
               Power(t1,4)*(-61 + 20*t2) + 
               Power(s2,4)*(2 + 14*t1 + 127*t2) + 
               2*Power(t1,3)*(9 + 115*t2 + 50*Power(t2,2)) + 
               Power(s2,3)*(35 - 27*Power(t1,2) + t1*(4 - 170*t2) - 
                  33*t2 + 120*Power(t2,2)) - 
               Power(t1,2)*(-41 + 265*t2 + 3*Power(t2,2) + 
                  220*Power(t2,3)) + 
               t1*(-74 - 322*t2 + 821*Power(t2,2) - 390*Power(t2,3) + 
                  70*Power(t2,4)) + 
               Power(s2,2)*(15 + 38*Power(t1,3) - 222*t2 + 
                  475*Power(t2,2) - 135*Power(t2,3) - 
                  Power(t1,2)*(11 + 303*t2) + 
                  t1*(-109 + 17*t2 + 818*Power(t2,2))) + 
               s2*(-162 - 11*Power(t1,4) + 568*t2 + 204*Power(t2,2) + 
                  253*Power(t2,3) - 525*Power(t2,4) + 
                  Power(t1,3)*(66 + 326*t2) + 
                  Power(t1,2)*(67 - 2*t2 - 1443*Power(t2,2)) + 
                  2*t1*(22 - 48*t2 - 421*Power(t2,2) + 816*Power(t2,3)))) \
+ Power(s1,4)*(32*Power(s2,5)*(-1 + t2) + Power(t1,5)*(-33 + 20*t2) + 
               Power(s2,4)*(81 + 10*Power(t1,2) + t1*(64 - 47*t2) - 
                  110*t2 - 253*Power(t2,2)) + 
               Power(t1,4)*(-9 + 270*t2 - 40*Power(t2,2)) - 
               Power(t1,3)*(-7 + 69*t2 + 541*Power(t2,2) + 
                  100*Power(t2,3)) + 
               Power(t1,2)*(-50 - 166*t2 + 669*Power(t2,2) - 
                  5*Power(t2,3) + 200*Power(t2,4)) + 
               t1*(-53 + 435*t2 + 532*Power(t2,2) - 1385*Power(t2,3) + 
                  520*Power(t2,4) - 70*Power(t2,5)) - 
               2*(-44 + 198*t2 + 7*Power(t2,2) + 105*Power(t2,3) - 
                  290*Power(t2,4) + 63*Power(t2,5)) - 
               Power(s2,3)*(77 + 30*Power(t1,3) + 
                  Power(t1,2)*(6 - 111*t2) + 25*t2 + 77*Power(t2,2) + 
                  340*Power(t2,3) + t1*(92 - 178*t2 - 373*Power(t2,2))) \
+ Power(s2,2)*(42 + 30*Power(t1,4) - 231*t2 + 484*Power(t2,2) - 
                  880*Power(t2,3) + 75*Power(t2,4) - 
                  Power(t1,3)*(85 + 189*t2) + 
                  Power(t1,2)*(-142 - 30*t2 + 631*Power(t2,2)) + 
                  t1*(177 + 391*t2 + 254*Power(t2,2) - 925*Power(t2,3))) \
+ s2*(45 - 10*Power(t1,5) + 714*t2 - 1824*Power(t2,2) + 
                  250*Power(t2,3) - 300*Power(t2,4) + 427*Power(t2,5) + 
                  Power(t1,4)*(92 + 73*t2) + 
                  Power(t1,3)*(162 - 308*t2 - 711*Power(t2,2)) + 
                  Power(t1,2)*
                   (-22 - 439*t2 - 63*Power(t2,2) + 1995*Power(t2,3)) + 
                  t1*(-197 + 190*t2 + 261*Power(t2,2) + 
                     1250*Power(t2,3) - 1735*Power(t2,4)))) + 
            Power(s1,3)*(-71 - 8*Power(t1,6) - 252*t2 + 
               985*Power(t2,2) - 374*Power(t2,3) + 345*Power(t2,4) - 
               533*Power(t2,5) + 98*Power(t2,6) + 
               Power(t1,5)*(13 + 108*t2 - 24*Power(t2,2)) + 
               Power(t1,4)*(39 + 2*t2 - 469*Power(t2,2) + 
                  40*Power(t2,3)) + 
               Power(t1,3)*(-167 - 21*t2 + 128*Power(t2,2) + 
                  664*Power(t2,3) + 50*Power(t2,4)) + 
               Power(t1,2)*(-51 + 525*t2 + 150*Power(t2,2) - 
                  866*Power(t2,3) - 108*Power(t2,5)) + 
               t1*(227 - 193*t2 - 882*Power(t2,2) - 368*Power(t2,3) + 
                  1365*Power(t2,4) - 418*Power(t2,5) + 42*Power(t2,6)) - 
               2*Power(s2,5)*
                (5 - 65*t2 + 24*Power(t2,2) + t1*(7 + 4*t2)) + 
               Power(s2,4)*(117 - 8*Power(t1,2)*(-8 + t2) - 345*t2 + 
                  422*Power(t2,2) + 262*Power(t2,3) + 
                  t1*(-48 - 331*t2 + 40*Power(t2,2))) + 
               Power(s2,3)*(-174 + 548*t2 - 255*Power(t2,2) + 
                  398*Power(t2,3) + 425*Power(t2,4) + 
                  4*Power(t1,3)*(-25 + 18*t2) - 
                  3*Power(t1,2)*(-88 - 83*t2 + 50*Power(t2,2)) - 
                  2*t1*(111 - 130*t2 + 407*Power(t2,2) + 
                     216*Power(t2,3))) + 
               Power(s2,2)*(38 + Power(t1,4)*(56 - 88*t2) - 343*t2 + 
                  883*Power(t2,2) - 656*Power(t2,3) + 950*Power(t2,4) + 
                  19*Power(t2,5) + 
                  Power(t1,3)*(-331 + 83*t2 + 348*Power(t2,2)) + 
                  Power(t1,2)*
                   (-48 + 911*t2 + 337*Power(t2,2) - 694*Power(t2,3)) + 
                  2*t1*(220 - 546*t2 - 274*Power(t2,2) - 
                     393*Power(t2,3) + 290*Power(t2,4))) + 
               s2*(53 - 161*t2 - 1240*Power(t2,2) + 2936*Power(t2,3) - 
                  915*Power(t2,4) + 281*Power(t2,5) - 217*Power(t2,6) + 
                  Power(t1,5)*(2 + 32*t2) + 
                  Power(t1,4)*(112 - 239*t2 - 166*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (57 - 414*t2 + 262*Power(t2,2) + 412*Power(t2,3)) + 
                  Power(t1,2)*
                   (-33 + 151*t2 + 1149*Power(t2,2) + 132*Power(t2,3) - 
                     1630*Power(t2,4)) + 
                  2*t1*(-131 + 499*t2 - 653*Power(t2,2) - 
                     82*Power(t2,3) - 555*Power(t2,4) + 571*Power(t2,5)))\
) + Power(s1,2)*(-44 + 277*t2 + 208*Power(t2,2) - 1149*Power(t2,3) + 
               596*Power(t2,4) - 325*Power(t2,5) + 314*Power(t2,6) - 
               50*Power(t2,7) + Power(t1,6)*(-17 + 18*t2) + 
               Power(t1,5)*(103 - 4*t2 - 129*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,4)*(-88 - 292*t2 + 25*Power(t2,2) + 
                  397*Power(t2,3) - 20*Power(t2,4)) - 
               Power(t1,3)*(165 - 770*t2 - 113*Power(t2,2) + 
                  150*Power(t2,3) + 446*Power(t2,4) + 10*Power(t2,5)) + 
               2*Power(t1,2)*
                (72 + 73*t2 - 708*Power(t2,2) + 68*Power(t2,3) + 
                  302*Power(t2,4) + 6*Power(t2,5) + 16*Power(t2,6)) + 
               t1*(69 - 873*t2 + 996*Power(t2,2) + 776*Power(t2,3) + 
                  37*Power(t2,4) - 785*Power(t2,5) + 198*Power(t2,6) - 
                  14*Power(t2,7)) + 
               2*Power(s2,5)*
                (3 + 18*t2 - 99*Power(t2,2) + 16*Power(t2,3) + 
                  6*t1*(-1 + 3*t2 + 2*Power(t2,2))) + 
               Power(s2,4)*(20 - 329*t2 + 555*Power(t2,2) - 
                  626*Power(t2,3) - 148*Power(t2,4) - 
                  3*Power(t1,2)*(-7 + 58*t2 + 12*Power(t2,2)) + 
                  t1*(-11 + 84*t2 + 603*Power(t2,2) + 22*Power(t2,3))) + 
               Power(s2,3)*(-86 + 558*t2 - 1162*Power(t2,2) + 
                  565*Power(t2,3) - 582*Power(t2,4) - 273*Power(t2,5) + 
                  Power(t1,3)*(26 + 288*t2 - 36*Power(t2,2)) + 
                  3*Power(t1,2)*
                   (7 - 234*t2 - 232*Power(t2,2) + 18*Power(t2,3)) + 
                  t1*(-25 + 640*t2 - 275*Power(t2,2) + 
                     1334*Power(t2,3) + 278*Power(t2,4))) + 
               Power(s2,2)*(38 - 243*t2 + 784*Power(t2,2) - 
                  1413*Power(t2,3) + 579*Power(t2,4) - 
                  601*Power(t2,5) - 51*Power(t2,6) + 
                  12*Power(t1,4)*(-7 - 15*t2 + 7*Power(t2,2)) + 
                  Power(t1,3)*
                   (70 + 1004*t2 + 246*Power(t2,2) - 290*Power(t2,3)) + 
                  Power(t1,2)*
                   (-202 + 41*t2 - 1842*Power(t2,2) - 
                     721*Power(t2,3) + 426*Power(t2,4)) + 
                  t1*(242 - 1215*t2 + 2211*Power(t2,2) + 
                     388*Power(t2,3) + 964*Power(t2,4) - 197*Power(t2,5)\
)) + s2*(31 - 111*t2 + 158*Power(t2,2) + 1056*Power(t2,3) - 
                  2554*Power(t2,4) + 993*Power(t2,5) - 188*Power(t2,6) + 
                  63*Power(t2,7) + 
                  Power(t1,5)*(66 + 12*t2 - 36*Power(t2,2)) + 
                  Power(t1,4)*
                   (-189 - 418*t2 + 174*Power(t2,2) + 170*Power(t2,3)) + 
                  Power(t1,3)*
                   (295 - 60*t2 + 1537*Power(t2,2) - 384*Power(t2,3) - 
                     536*Power(t2,4)) + 
                  Power(t1,2)*
                   (21 - 327*t2 - 426*Power(t2,2) - 1483*Power(t2,3) - 
                     103*Power(t2,4) + 786*Power(t2,5)) + 
                  t1*(-250 + 1018*t2 - 1706*Power(t2,2) + 
                     2338*Power(t2,3) - 174*Power(t2,4) + 
                     590*Power(t2,5) - 447*Power(t2,6)))) + 
            s1*(Power(t1,6)*(2 + 30*t2 - 12*Power(t2,2)) + 
               Power(t1,5)*(27 - 183*t2 - 35*Power(t2,2) + 
                  66*Power(t2,3) - 2*Power(t2,4)) + 
               Power(t1,4)*(-58 + 126*t2 + 461*Power(t2,2) - 
                  20*Power(t2,3) - 162*Power(t2,4) + 4*Power(t2,5)) + 
               Power(t1,3)*(3 + 385*t2 - 1003*Power(t2,2) - 
                  191*Power(t2,3) + 102*Power(t2,4) + 154*Power(t2,5)) - 
               Power(t1,2)*(-34 + 304*t2 + 181*Power(t2,2) - 
                  1457*Power(t2,3) + 263*Power(t2,4) + 213*Power(t2,5) + 
                  11*Power(t2,6) + 4*Power(t2,7)) + 
               t2*(88 - 341*t2 - 12*Power(t2,2) + 642*Power(t2,3) - 
                  373*Power(t2,4) + 160*Power(t2,5) - 107*Power(t2,6) + 
                  15*Power(t2,7)) + 
               t1*(-8 - 146*t2 + 1081*Power(t2,2) - 1201*Power(t2,3) - 
                  276*Power(t2,4) + 74*Power(t2,5) + 243*Power(t2,6) - 
                  50*Power(t2,7) + 2*Power(t2,8)) - 
               2*Power(s2,5)*(-2 + 6*t2 + 21*Power(t2,2) - 
                  67*Power(t2,3) + 4*Power(t2,4) + 
                  t1*(2 - 12*t2 + 15*Power(t2,2) + 12*Power(t2,3))) + 
               Power(s2,4)*(-18 - 28*t2 + 307*Power(t2,2) - 
                  399*Power(t2,3) + 416*Power(t2,4) + 43*Power(t2,5) + 
                  2*Power(t1,2)*
                   (-7 - 19*t2 + 78*Power(t2,2) + 28*Power(t2,3)) + 
                  t1*(32 + 6*t2 - 24*Power(t2,2) - 469*Power(t2,3) - 
                     46*Power(t2,4))) + 
               Power(s2,3)*(15 + 137*t2 - 588*Power(t2,2) + 
                  988*Power(t2,3) - 440*Power(t2,4) + 371*Power(t2,5) + 
                  86*Power(t2,6) - 
                  4*Power(t1,3)*
                   (-16 + 15*t2 + 69*Power(t2,2) + 6*Power(t2,3)) + 
                  Power(t1,2)*
                   (-93 - 37*t2 + 614*Power(t2,2) + 669*Power(t2,3) + 
                     33*Power(t2,4)) - 
                  2*t1*(-7 - 44*t2 + 311*Power(t2,2) - 69*Power(t2,3) + 
                     479*Power(t2,4) + 47*Power(t2,5))) + 
               Power(s2,2)*(6 - 92*t2 + 358*Power(t2,2) - 
                  707*Power(t2,3) + 1026*Power(t2,4) - 298*Power(t2,5) + 
                  207*Power(t2,6) + 27*Power(t2,7) - 
                  4*Power(t1,4)*
                   (17 - 42*t2 - 48*Power(t2,2) + 6*Power(t2,3)) + 
                  Power(t1,3)*
                   (101 - 79*t2 - 1023*Power(t2,2) - 401*Power(t2,3) + 
                     102*Power(t2,4)) + 
                  Power(t1,2)*
                   (-44 + 258*t2 + 68*Power(t2,2) + 1519*Power(t2,3) + 
                     606*Power(t2,4) - 139*Power(t2,5)) + 
                  t1*(5 - 383*t2 + 1126*Power(t2,2) - 1854*Power(t2,3) - 
                     151*Power(t2,4) - 551*Power(t2,5) + 34*Power(t2,6))) \
+ s2*(8 - 54*t2 + 47*Power(t2,2) - 13*Power(t2,3) - 438*Power(t2,4) + 
                  1152*Power(t2,5) - 494*Power(t2,6) + 75*Power(t2,7) - 
                  8*Power(t2,8) + 
                  2*Power(t1,5)*
                   (10 - 62*t2 - 15*Power(t2,2) + 8*Power(t2,3)) + 
                  Power(t1,4)*
                   (-71 + 305*t2 + 510*Power(t2,2) + Power(t2,3) - 
                     79*Power(t2,4)) + 
                  2*Power(t1,3)*
                   (53 - 222*t2 - 107*Power(t2,2) - 619*Power(t2,3) + 
                     49*Power(t2,4) + 93*Power(t2,5)) + 
                  Power(t1,2)*
                   (-23 - 163*t2 + 695*Power(t2,2) + 487*Power(t2,3) + 
                     940*Power(t2,4) + 30*Power(t2,5) - 207*Power(t2,6)) \
+ 2*t1*(-20 + 266*t2 - 597*Power(t2,2) + 603*Power(t2,3) - 
                     869*Power(t2,4) + 126*Power(t2,5) - 87*Power(t2,6) + 
                     46*Power(t2,7))))))*R1(1 - s + s1 - t2))/
     ((-1 + s1)*(s - s2 + t1)*Power(-s + s1 - t2,3)*(1 - s + s1 - t2)*
       (-1 + s1 + t1 - t2)*Power(s1 - s2 + t1 - t2,3)*
       (-1 + s - s*s2 + s1*s2 + t1 - s2*t2)) + 
    (8*(-97 - 2*s2 - 2*Power(s1,6)*s2 + 11*Power(s2,2) - 6*Power(s2,3) + 
         2*Power(s2,4) + 185*t1 - 34*s2*t1 + 9*Power(s2,2)*t1 + 
         6*Power(s2,3)*t1 - 2*Power(s2,4)*t1 - 27*Power(t1,2) + 
         36*s2*Power(t1,2) - 29*Power(s2,2)*Power(t1,2) - 
         97*Power(t1,3) + 4*s2*Power(t1,3) + 9*Power(s2,2)*Power(t1,3) + 
         36*Power(t1,4) - 4*s2*Power(t1,4) - 86*t2 - 114*s2*t2 - 
         4*Power(s2,3)*t2 - 4*Power(s2,4)*t2 + 2*Power(s2,5)*t2 + 
         16*t1*t2 + 66*s2*t1*t2 - 2*Power(s2,2)*t1*t2 + 
         36*Power(s2,3)*t1*t2 - 2*Power(s2,4)*t1*t2 + 
         184*Power(t1,2)*t2 + 118*s2*Power(t1,2)*t2 - 
         42*Power(s2,2)*Power(t1,2)*t2 - 10*Power(s2,3)*Power(t1,2)*t2 - 
         134*Power(t1,3)*t2 - 54*s2*Power(t1,3)*t2 + 
         10*Power(s2,2)*Power(t1,3)*t2 + 20*Power(t1,4)*t2 + 
         54*Power(t2,2) - 81*s2*Power(t2,2) - 
         22*Power(s2,2)*Power(t2,2) - 3*Power(s2,3)*Power(t2,2) - 
         15*Power(s2,4)*Power(t2,2) + 2*Power(s2,5)*Power(t2,2) - 
         148*t1*Power(t2,2) - 129*s2*t1*Power(t2,2) - 
         34*Power(s2,2)*t1*Power(t2,2) + 41*Power(s2,3)*t1*Power(t2,2) + 
         Power(s2,4)*t1*Power(t2,2) + 121*Power(t1,2)*Power(t2,2) + 
         188*s2*Power(t1,2)*Power(t2,2) + 
         14*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         6*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         31*Power(t1,3)*Power(t2,2) - 32*s2*Power(t1,3)*Power(t2,2) + 
         40*Power(t2,3) + 73*s2*Power(t2,3) + 3*Power(s2,2)*Power(t2,3) - 
         Power(s2,3)*Power(t2,3) - 5*Power(s2,4)*Power(t2,3) - 
         32*t1*Power(t2,3) - 124*s2*t1*Power(t2,3) - 
         66*Power(s2,2)*t1*Power(t2,3) + 4*Power(s2,3)*t1*Power(t2,3) - 
         8*Power(t1,2)*Power(t2,3) + 42*s2*Power(t1,2)*Power(t2,3) + 
         12*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         6*Power(t1,3)*Power(t2,3) - 5*Power(t2,4) + 35*s2*Power(t2,4) + 
         21*Power(s2,2)*Power(t2,4) + 4*Power(s2,3)*Power(t2,4) + 
         11*t1*Power(t2,4) + 3*s2*t1*Power(t2,4) - 
         11*Power(s2,2)*t1*Power(t2,4) - 6*Power(t1,2)*Power(t2,4) - 
         6*s2*Power(t1,2)*Power(t2,4) - 2*Power(t2,5) - 
         7*s2*Power(t2,5) - Power(s2,2)*Power(t2,5) + 
         6*s2*t1*Power(t2,5) + 
         Power(s,5)*(4 + 2*s1*(-1 + s2) - t1 - 4*Power(t1,2) - t2 + 
            8*t1*t2 - 4*Power(t2,2) + s2*(-4 + t1 + t2)) + 
         Power(s1,5)*(-3*Power(s2,2) + 2*(1 - t1 + Power(t1,2)) + 
            s2*(2 - 11*t1 + 10*t2)) - 
         Power(s,4)*(-19 + 2*Power(s1,2)*(-4 + 5*s2) + 3*t1 + 
            4*Power(t1,2) + 2*Power(t1,3) - 15*t2 + 7*t1*t2 + 
            6*Power(t1,2)*t2 - 3*Power(t2,2) - 18*t1*Power(t2,2) + 
            10*Power(t2,3) + Power(s2,2)*(-8 + 2*t1 + 5*t2) + 
            s2*(31 - 2*t1 - 13*Power(t1,2) + 5*t2 + 21*t1*t2 - 
               16*Power(t2,2)) + 
            s1*(32 + Power(s2,2) - 14*t1 - 18*Power(t1,2) + 
               s2*(-39 + 9*t1 - 6*t2) + 13*t2 + 28*t1*t2 - 10*Power(t2,2)\
)) + Power(s1,4)*(-2 + 2*Power(s2,3) - 2*Power(t1,3) - 8*t2 - 
            3*Power(t1,2)*(3 + 2*t2) + t1*(13 + 4*t2) + 
            Power(s2,2)*(-1 - 21*t1 + 13*t2) - 
            s2*(-70 + t1 + 3*Power(t1,2) + 23*t2 - 48*t1*t2 + 
               20*Power(t2,2))) + 
         Power(s1,3)*(-67 + 7*Power(s2,4) + t1*(65 - 62*t2) + 21*t2 + 
            14*Power(t2,2) + Power(t1,3)*(-5 + 4*t2) - 
            Power(s2,3)*(19*t1 + 6*t2) + 
            Power(t1,2)*(1 + 37*t2 + 6*Power(t2,2)) + 
            Power(s2,2)*(79 + 2*Power(t1,2) - 20*t2 - 20*Power(t2,2) + 
               t1*(4 + 68*t2)) + 
            s2*(30 - 10*Power(t1,3) - 241*t2 + 64*Power(t2,2) + 
               20*Power(t2,3) + Power(t1,2)*(-24 + 23*t2) + 
               t1*(89 - 24*t2 - 84*Power(t2,2)))) + 
         Power(s1,2)*(-29 + 4*Power(s2,5) - 12*Power(t1,4) + 164*t2 - 
            41*Power(t2,2) - 14*Power(t2,3) - 
            Power(s2,4)*(13 + 9*t1 + 17*t2) + 
            Power(t1,3)*(3 + 22*t2 - 2*Power(t2,2)) - 
            4*t1*(10 + 31*t2 - 24*Power(t2,2) + Power(t2,3)) - 
            Power(t1,2)*(-74 + 44*t2 + 53*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s2,3)*(20 + 7*Power(t1,2) - 3*t2 + 10*Power(t2,2) + 
               t1*(33 + 38*t2)) - 
            Power(s2,2)*(79 + 8*Power(t1,3) + 
               Power(t1,2)*(29 - 14*t2) + 151*t2 - 64*Power(t2,2) - 
               12*Power(t2,3) + t1*(-34 + 78*t2 + 84*Power(t2,2))) + 
            s2*(-274 + 19*t2 + 307*Power(t2,2) - 74*Power(t2,3) - 
               10*Power(t2,4) + 2*Power(t1,3)*(2 + 9*t2) + 
               Power(t1,2)*(63 + 78*t2 - 43*Power(t2,2)) + 
               t1*(171 - 296*t2 + 54*Power(t2,2) + 74*Power(t2,3)))) + 
         s1*(193 - 27*t2 + 8*Power(t1,4)*t2 - 137*Power(t2,2) + 
            27*Power(t2,3) + 8*Power(t2,4) - 2*Power(s2,5)*(1 + 3*t2) + 
            Power(t1,3)*(57 + 24*t2 - 23*Power(t2,2)) + 
            Power(t1,2)*(35 - 179*t2 + 51*Power(t2,2) + 
               31*Power(t2,3)) + 
            t1*(-285 + 182*t2 + 91*Power(t2,2) - 58*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,4)*(2 + 28*t2 + 15*Power(t2,2) + t1*(4 + 8*t2)) - 
            Power(s2,3)*(-2 + 19*t2 + Power(t1,2)*t2 - 4*Power(t2,2) + 
               10*Power(t2,3) + t1*(24 + 72*t2 + 23*Power(t2,2))) + 
            Power(s2,2)*(-25 + 99*t2 + 69*Power(t2,2) - 
               64*Power(t2,3) - Power(t2,4) + Power(t1,3)*(3 + 8*t2) + 
               Power(t1,2)*(19 + 7*t2 - 28*Power(t2,2)) + 
               t1*(37 + 10*t2 + 140*Power(t2,2) + 48*Power(t2,3))) - 
            s2*(-176 + 8*Power(t1,4) - 353*t2 + 122*Power(t2,2) + 
               171*Power(t2,3) - 38*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,3)*(-38 - 36*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(40 + 263*t2 + 96*Power(t2,2) - 
                  29*Power(t2,3)) + 
               t1*(182 + 36*t2 - 331*Power(t2,2) + 32*Power(t2,3) + 
                  33*Power(t2,4)))) + 
         Power(s,3)*(-22 + 4*Power(s1,3)*(-3 + 5*s2) + 22*t1 + 
            28*Power(t1,2) - 21*Power(t1,3) + 15*t2 - 64*t1*t2 + 
            44*Power(t1,2)*t2 - 4*Power(t1,3)*t2 + 38*Power(t2,2) - 
            58*t1*Power(t2,2) + 23*Power(t2,3) + 12*t1*Power(t2,3) - 
            8*Power(t2,4) + Power(s2,3)*(-2 + t1 + 9*t2) + 
            Power(s2,2)*(38 + t1 - 14*Power(t1,2) - 4*t2 + 21*t1*t2 - 
               28*Power(t2,2)) + 
            Power(s1,2)*(38 + 8*Power(s2,2) - 32*Power(t1,2) + 
               s2*(-91 + 32*t1 - 34*t2) + 36*t1*(-1 + t2) + 33*t2 - 
               8*Power(t2,2)) + 
            s2*(-33 + 10*Power(t1,3) + Power(t1,2)*(-9 + t2) - 66*t2 - 
               16*Power(t2,2) + 30*Power(t2,3) + 
               t1*(1 + 46*t2 - 29*Power(t2,2))) + 
            s1*(13 - 8*Power(s2,3) + 8*Power(t1,3) - 60*t2 - 
               44*Power(t2,2) + 16*Power(t2,3) + 
               Power(s2,2)*(-53 + 25*t1 + 6*t2) + 
               Power(t1,2)*(5 + 24*t2) - 
               2*t1*(5 - 36*t2 + 24*Power(t2,2)) + 
               s2*(104 - 36*Power(t1,2) + 111*t2 - 16*Power(t2,2) + 
                  t1*(-46 + 21*t2)))) + 
         Power(s,2)*(-94 + Power(s1,4)*(8 - 20*s2) + 106*t1 - 
            59*Power(t1,2) + 39*Power(t1,3) - 12*Power(t1,4) - 169*t2 + 
            119*t1*t2 - 46*Power(t1,2)*t2 + 14*Power(t1,3)*t2 - 
            57*Power(t2,2) - 50*t1*Power(t2,2) + 
            16*Power(t1,2)*Power(t2,2) - 2*Power(t1,3)*Power(t2,2) + 
            31*Power(t2,3) - 45*t1*Power(t2,3) + 
            2*Power(t1,2)*Power(t2,3) + 19*Power(t2,4) + 
            2*t1*Power(t2,4) - 2*Power(t2,5) - Power(s2,4)*(4 + 7*t2) + 
            Power(s2,3)*(-3 + 5*Power(t1,2) + 25*t2 + 26*Power(t2,2) - 
               t1*(2 + 9*t2)) + 
            Power(s1,3)*(-6 - 16*Power(s2,2) + 28*Power(t1,2) - 27*t2 + 
               2*Power(t2,2) - 4*t1*(-8 + 5*t2) + 
               s2*(79 - 52*t1 + 56*t2)) + 
            Power(s2,2)*(1 - 8*Power(t1,3) + 58*t2 - 8*Power(t2,2) - 
               36*Power(t2,3) + Power(t1,2)*(26 + 6*t2) + 
               t1*(-27 - 69*t2 + 17*Power(t2,2))) + 
            s2*(131 + 89*t2 - 74*Power(t2,2) - 74*Power(t2,3) + 
               18*Power(t2,4) + 2*Power(t1,3)*(10 + 9*t2) - 
               Power(t1,2)*(27 + 114*t2 + 37*Power(t2,2)) + 
               t1*(-92 + 105*t2 + 209*Power(t2,2) + 9*Power(t2,3))) + 
            Power(s1,2)*(-193 + 16*Power(s2,3) - 12*Power(t1,3) + 
               39*t2 + 49*Power(t2,2) - 6*Power(t2,3) + 
               Power(s2,2)*(97 - 65*t1 + 20*t2) - 
               Power(t1,2)*(7 + 36*t2) + 
               t1*(54 - 119*t2 + 42*Power(t2,2)) + 
               s2*(67 + 30*Power(t1,2) - 220*t2 - 34*Power(t2,2) + 
                  t1*(77 + 69*t2))) + 
            s1*(267 + 11*Power(s2,4) + Power(s2,3)*(1 - 25*t1 - 34*t2) + 
               238*t2 - 64*Power(t2,2) - 49*Power(t2,3) + 
               6*Power(t2,4) + Power(t1,3)*(37 + 12*t2) + 
               Power(t1,2)*(5 - 47*t2 + 6*Power(t2,2)) + 
               t1*(-199 + 40*t2 + 132*Power(t2,2) - 24*Power(t2,3)) + 
               Power(s2,2)*(-69 + 30*Power(t1,2) - 95*t2 + 
                  32*Power(t2,2) + t1*(76 + 30*t2)) - 
               s2*(273 + 30*Power(t1,3) + Power(t1,2)*(2 - 21*t2) + 
                  7*t2 - 215*Power(t2,2) + 20*Power(t2,3) + 
                  t1*(-151 + 262*t2 + 26*Power(t2,2))))) + 
         s*(190 + 2*Power(s1,5)*(-1 + 5*s2) - 277*t1 + 50*Power(t1,2) + 
            13*Power(t1,3) + 8*Power(t1,4) + 226*t2 - 72*t1*t2 - 
            152*Power(t1,2)*t2 + 52*Power(t1,3)*t2 - 8*Power(t1,4)*t2 - 
            40*Power(t2,2) + 196*t1*Power(t2,2) - 
            142*Power(t1,2)*Power(t2,2) + 25*Power(t1,3)*Power(t2,2) - 
            86*Power(t2,3) + 58*t1*Power(t2,3) - 
            14*Power(t1,2)*Power(t2,3) - 6*Power(t2,4) - 
            9*t1*Power(t2,4) + 4*Power(t2,5) + 2*Power(s2,5)*(1 + t2) + 
            Power(s2,4)*(-8 + (-17 + t1)*t2 - 12*Power(t2,2)) + 
            Power(s1,4)*(-6 + 12*Power(s2,2) - 12*Power(t1,2) + 
               s2*(-25 + 39*t1 - 39*t2) + 8*t2 + t1*(-7 + 4*t2)) + 
            Power(s2,3)*(17 + t2 + 34*Power(t2,2) + 21*Power(t2,3) - 
               Power(t1,2)*(9 + t2) + t1*(20 + 30*t2 - 6*Power(t2,2))) - 
            Power(s2,2)*(30 + 7*t2 - 46*Power(t2,2) - 39*Power(t2,3) + 
               14*Power(t2,4) + Power(t1,3)*(1 + 8*t2) - 
               2*Power(t1,2)*(13 + 38*t2 + 16*Power(t2,2)) + 
               t1*(51 + 143*t2 + 175*Power(t2,2) + 17*Power(t2,3))) + 
            s2*(-93 + 8*Power(t1,4) + 53*t2 + 193*Power(t2,2) + 
               6*Power(t2,3) - 54*Power(t2,4) + 3*Power(t2,5) + 
               Power(t1,3)*(-90 - 48*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(119 + 231*t2 - 3*Power(t2,2) - 
                  31*Power(t2,3)) + 
               2*t1*(53 - 108*t2 - 34*Power(t2,2) + 62*Power(t2,3) + 
                  11*Power(t2,4))) + 
            Power(s1,3)*(87 - 10*Power(s2,3) + 8*Power(t1,3) + 
               Power(s2,2)*(-51 + 63*t1 - 34*t2) + 6*t2 - 
               16*Power(t2,2) + 3*Power(t1,2)*(5 + 8*t2) - 
               2*t1*(23 - 25*t2 + 6*Power(t2,2)) - 
               s2*(218 + 4*Power(t1,2) - 129*t2 - 54*Power(t2,2) + 
                  t1*(32 + 117*t2))) - 
            Power(s1,2)*(-170 + 18*Power(s2,4) + 
               Power(s2,3)*(1 - 43*t1 - 29*t2) + 242*t2 - 
               20*Power(t2,3) + Power(t1,3)*(11 + 12*t2) + 
               2*Power(t1,2)*(5 + 17*t2 + 6*Power(t2,2)) - 
               2*t1*(-21 + 53*t2 - 44*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(62 + 18*Power(t1,2) - 141*t2 - 
                  18*Power(t2,2) + t1*(79 + 119*t2)) - 
               s2*(352 + 30*Power(t1,3) + 440*t2 - 237*Power(t2,2) - 
                  28*Power(t2,3) - 5*Power(t1,2)*(-7 + 9*t2) + 
                  t1*(-301 + 212*t2 + 139*Power(t2,2)))) + 
            s1*(-439 - 4*Power(s2,5) + 24*Power(t1,4) - 126*t2 + 
               241*Power(t2,2) + 6*Power(t2,3) - 14*Power(t2,4) + 
               Power(s2,4)*(17 + 9*t1 + 28*t2) + 
               2*Power(t1,3)*(-9 - 18*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-59 + 174*t2 + 33*Power(t2,2)) - 
               2*t1*(-232 + 80*t2 + 59*Power(t2,2) - 27*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s2,3)*(12 + 12*Power(t1,2) + 29*t2 + 
                  40*Power(t2,2) + t1*(42 + 33*t2)) + 
               Power(s2,2)*(112 + 16*Power(t1,3) + 
                  Power(t1,2)*(7 - 20*t2) + 22*t2 - 129*Power(t2,2) + 
                  18*Power(t2,3) + t1*(15 + 242*t2 + 73*Power(t2,2))) + 
               s2*(10 - 545*t2 - 228*Power(t2,2) + 187*Power(t2,3) - 
                  12*Power(t1,3)*(2 + 3*t2) + 
                  4*Power(t1,2)*(-34 + t2 + 20*Power(t2,2)) + 
                  t1*(72 + 339*t2 - 304*Power(t2,2) - 83*Power(t2,3))))))*
       R2(1 - s1 - t1 + t2))/
     ((-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       (-1 + s - s*s2 + s1*s2 + t1 - s2*t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) - 
    (8*(183 - 6*s2 - 80*Power(s2,2) + 24*Power(s2,3) - 7*Power(s2,4) + 
         2*Power(s2,5) - 497*t1 + 154*s2*t1 + 140*Power(s2,2)*t1 - 
         28*Power(s2,3)*t1 + 7*Power(s2,4)*t1 - 4*Power(s2,5)*t1 + 
         281*Power(t1,2) - 306*s2*Power(t1,2) - 
         52*Power(s2,2)*Power(t1,2) - 12*Power(s2,3)*Power(t1,2) + 
         7*Power(s2,4)*Power(t1,2) + 2*Power(s2,5)*Power(t1,2) + 
         251*Power(t1,3) + 180*s2*Power(t1,3) + 
         4*Power(s2,2)*Power(t1,3) + 12*Power(s2,3)*Power(t1,3) - 
         7*Power(s2,4)*Power(t1,3) - 264*Power(t1,4) - 
         28*s2*Power(t1,4) - 12*Power(s2,2)*Power(t1,4) + 
         4*Power(s2,3)*Power(t1,4) + 38*Power(t1,5) + 6*s2*Power(t1,5) + 
         8*Power(t1,6) + Power(s1,8)*
          (-Power(s2,2) - 2*s2*t1 + Power(t1,2)) + 
         2*Power(s,8)*Power(t1 - t2,2) + 74*t2 + 315*s2*t2 - 
         105*Power(s2,2)*t2 - 143*Power(s2,3)*t2 + 47*Power(s2,4)*t2 - 
         12*Power(s2,5)*t2 + 4*Power(s2,6)*t2 + 227*t1*t2 - 
         284*s2*t1*t2 + 410*Power(s2,2)*t1*t2 + 108*Power(s2,3)*t1*t2 - 
         13*Power(s2,4)*t1*t2 - 2*Power(s2,5)*t1*t2 - 
         4*Power(s2,6)*t1*t2 - 952*Power(t1,2)*t2 - 
         633*s2*Power(t1,2)*t2 - 333*Power(s2,2)*Power(t1,2)*t2 + 
         17*Power(s2,3)*Power(t1,2)*t2 - 25*Power(s2,4)*Power(t1,2)*t2 + 
         14*Power(s2,5)*Power(t1,2)*t2 + 899*Power(t1,3)*t2 + 
         808*s2*Power(t1,3)*t2 + 52*Power(s2,2)*Power(t1,3)*t2 + 
         14*Power(s2,3)*Power(t1,3)*t2 - 9*Power(s2,4)*Power(t1,3)*t2 - 
         238*Power(t1,4)*t2 - 202*s2*Power(t1,4)*t2 - 
         24*Power(s2,2)*Power(t1,4)*t2 + 4*Power(s2,3)*Power(t1,4)*t2 - 
         10*Power(t1,5)*t2 - 4*s2*Power(t1,5)*t2 - 282*Power(t2,2) - 
         99*s2*Power(t2,2) + 84*Power(s2,2)*Power(t2,2) - 
         128*Power(s2,3)*Power(t2,2) - 63*Power(s2,4)*Power(t2,2) + 
         25*Power(s2,5)*Power(t2,2) - 5*Power(s2,6)*Power(t2,2) + 
         2*Power(s2,7)*Power(t2,2) + 847*t1*Power(t2,2) + 
         1159*s2*t1*Power(t2,2) + 409*Power(s2,2)*t1*Power(t2,2) + 
         200*Power(s2,3)*t1*Power(t2,2) - 23*Power(s2,4)*t1*Power(t2,2) + 
         11*Power(s2,5)*t1*Power(t2,2) - 7*Power(s2,6)*t1*Power(t2,2) - 
         698*Power(t1,2)*Power(t2,2) - 1729*s2*Power(t1,2)*Power(t2,2) - 
         818*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         15*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         2*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         6*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         29*Power(t1,3)*Power(t2,2) + 665*s2*Power(t1,3)*Power(t2,2) + 
         333*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         29*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         8*Power(s2,4)*Power(t1,3)*Power(t2,2) + 
         112*Power(t1,4)*Power(t2,2) + 2*s2*Power(t1,4)*Power(t2,2) - 
         28*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         12*Power(t1,5)*Power(t2,2) - 110*Power(t2,3) - 
         613*s2*Power(t2,3) - 265*Power(s2,2)*Power(t2,3) - 
         59*Power(s2,3)*Power(t2,3) - 46*Power(s2,4)*Power(t2,3) + 
         5*Power(s2,5)*Power(t2,3) + 2*Power(s2,6)*Power(t2,3) - 
         6*t1*Power(t2,3) + 1015*s2*t1*Power(t2,3) + 
         995*Power(s2,2)*t1*Power(t2,3) + 
         288*Power(s2,3)*t1*Power(t2,3) - 4*Power(s2,4)*t1*Power(t2,3) - 
         5*Power(s2,5)*t1*Power(t2,3) - Power(s2,6)*t1*Power(t2,3) + 
         331*Power(t1,2)*Power(t2,3) - 148*s2*Power(t1,2)*Power(t2,3) - 
         626*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         227*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         10*Power(s2,4)*Power(t1,2)*Power(t2,3) + 
         4*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         225*Power(t1,3)*Power(t2,3) - 268*s2*Power(t1,3)*Power(t2,3) + 
         35*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         36*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         26*Power(t1,4)*Power(t2,3) + 36*s2*Power(t1,4)*Power(t2,3) + 
         84*Power(t2,4) - 57*s2*Power(t2,4) - 
         382*Power(s2,2)*Power(t2,4) - 136*Power(s2,3)*Power(t2,4) - 
         13*Power(s2,4)*Power(t2,4) - 5*Power(s2,5)*Power(t2,4) + 
         Power(s2,6)*Power(t2,4) - 193*t1*Power(t2,4) - 
         326*s2*t1*Power(t2,4) + 155*Power(s2,2)*t1*Power(t2,4) + 
         219*Power(s2,3)*t1*Power(t2,4) + 61*Power(s2,4)*t1*Power(t2,4) - 
         Power(s2,5)*t1*Power(t2,4) + 72*Power(t1,2)*Power(t2,4) + 
         421*s2*Power(t1,2)*Power(t2,4) + 
         209*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         36*Power(s2,3)*Power(t1,2)*Power(t2,4) - 
         12*Power(s2,4)*Power(t1,2)*Power(t2,4) + 
         18*Power(t1,3)*Power(t2,4) - 63*s2*Power(t1,3)*Power(t2,4) - 
         36*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         4*Power(t1,4)*Power(t2,4) + 26*Power(t2,5) + 
         153*s2*Power(t2,5) + 61*Power(s2,2)*Power(t2,5) - 
         53*Power(s2,3)*Power(t2,5) - 19*Power(s2,4)*Power(t2,5) - 
         3*Power(s2,5)*Power(t2,5) + t1*Power(t2,5) - 
         154*s2*t1*Power(t2,5) - 221*Power(s2,2)*t1*Power(t2,5) - 
         56*Power(s2,3)*t1*Power(t2,5) + 9*Power(s2,4)*t1*Power(t2,5) - 
         19*Power(t1,2)*Power(t2,5) - 25*s2*Power(t1,2)*Power(t2,5) + 
         48*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
         12*Power(s2,3)*Power(t1,2)*Power(t2,5) + 
         4*Power(t1,3)*Power(t2,5) + 8*s2*Power(t1,3)*Power(t2,5) - 
         2*Power(t2,6) + 11*s2*Power(t2,6) + 62*Power(s2,2)*Power(t2,6) + 
         28*Power(s2,3)*Power(t2,6) + 3*Power(s2,4)*Power(t2,6) + 
         3*t1*Power(t2,6) + 29*s2*t1*Power(t2,6) + 
         8*Power(s2,2)*t1*Power(t2,6) - 11*Power(s2,3)*t1*Power(t2,6) + 
         Power(t1,2)*Power(t2,6) - 8*s2*Power(t1,2)*Power(t2,6) - 
         4*Power(s2,2)*Power(t1,2)*Power(t2,6) - 6*Power(t2,7) - 
         7*s2*Power(t2,7) - 7*Power(s2,2)*Power(t2,7) - 
         Power(s2,3)*Power(t2,7) + 2*t1*Power(t2,7) - s2*t1*Power(t2,7) + 
         4*Power(s2,2)*t1*Power(t2,7) + Power(t2,8) - s2*Power(t2,8) - 
         Power(s1,7)*(3*Power(s2,3) + Power(s2,2)*(4 + 14*t1 - 7*t2) + 
            t1*(-2 - 3*t1 + Power(t1,2) + 2*t2 + 6*t1*t2) - 
            s2*(2 - 3*t1 + 6*Power(t1,2) + 2*t2 + 13*t1*t2)) + 
         Power(s,7)*(Power(t1,3) - 
            Power(t1,2)*(1 + 15*s1 + 8*s2 - 9*t2) + 
            t2*(3 - 7*s2 + s1*(3 + s2 - 11*t2) - 3*t2 - 10*s2*t2 + 
               11*Power(t2,2)) + 
            t1*(1 + 3*s2 + 4*t2 + 18*s2*t2 - 21*Power(t2,2) + 
               s1*(-7 + 3*s2 + 26*t2))) + 
         Power(s1,6)*(-1 - 3*Power(s2,4) - 2*t2 + Power(t2,2) + 
            Power(t1,3)*(3 + 5*t2) + Power(s2,3)*(-7 - 27*t1 + 17*t2) + 
            3*Power(t1,2)*(-1 - 4*t2 + 5*Power(t2,2)) + 
            t1*(3 - 19*t2 + 12*Power(t2,2)) + 
            Power(s2,2)*(30 + 5*Power(t1,2) + 21*t2 - 21*Power(t2,2) + 
               t1*(-25 + 84*t2)) - 
            s2*(-8 + 3*Power(t1,3) + 11*t2 + 13*Power(t2,2) + 
               Power(t1,2)*(2 + 31*t2) + t1*(-34 - 4*t2 + 36*Power(t2,2))\
)) - Power(s,6)*(3 - 7*t1 + 29*Power(t1,2) - 12*Power(t1,3) + 9*t2 - 
            73*t1*t2 + 42*Power(t1,2)*t2 - 5*Power(t1,3)*t2 + 
            16*Power(t2,2) - 60*t1*Power(t2,2) - 
            15*Power(t1,2)*Power(t2,2) + 30*Power(t2,3) + 
            45*t1*Power(t2,3) - 25*Power(t2,4) + 
            Power(s2,2)*(2 - 13*Power(t1,2) - 26*t2 - 21*Power(t2,2) + 
               t1*(13 + 34*t2)) + 
            Power(s1,2)*(Power(s2,2) - 49*Power(t1,2) + 
               (14 - 25*t2)*t2 + s2*(-3 + 20*t1 + 5*t2) + 
               t1*(-39 + 72*t2)) + 
            s2*(-7 + 3*Power(t1,3) + 6*t2 + 15*Power(t2,2) + 
               48*Power(t2,3) + 2*Power(t1,2)*(-8 + 15*t2) + 
               t1*(5 + 27*t2 - 81*Power(t2,2))) + 
            s1*(-7 + 7*Power(t1,3) + 7*t2 - 44*Power(t2,2) + 
               50*Power(t2,3) + Power(s2,2)*(-7 + 13*t1 + 4*t2) + 
               Power(t1,2)*(-9 + 60*t2) + 
               t1*(20 + 83*t2 - 117*Power(t2,2)) - 
               s2*(-18 + 54*Power(t1,2) + t1*(25 - 81*t2) + 19*t2 + 
                  53*Power(t2,2)))) + 
         Power(s1,5)*(-4 - Power(s2,5) - 6*Power(t1,4) + 4*t2 + 
            16*Power(t2,2) - 6*Power(t2,3) + 
            Power(s2,4)*(2 - 21*t1 + 12*t2) + 
            Power(t1,3)*(14 - 13*t2 - 10*Power(t2,2)) + 
            Power(t1,2)*(4 + 7*t2 + 15*Power(t2,2) - 20*Power(t2,3)) - 
            t1*(20 + 10*t2 - 63*Power(t2,2) + 30*Power(t2,3)) + 
            Power(s2,3)*(54 - 3*Power(t1,2) + 21*t2 - 39*Power(t2,2) + 
               t1*(-37 + 140*t2)) - 
            Power(s2,2)*(-105 + 7*Power(t1,3) + 204*t2 + 
               38*Power(t2,2) - 35*Power(t2,3) + 
               2*Power(t1,2)*(25 + 6*t2) + 
               t1*(-93 - 84*t2 + 214*Power(t2,2))) + 
            s2*(-51 - 48*t2 + 32*Power(t2,2) + 36*Power(t2,3) + 
               Power(t1,3)*(11 + 12*t2) + 
               Power(t1,2)*(9 + 32*t2 + 65*Power(t2,2)) + 
               t1*(72 - 213*t2 + 26*Power(t2,2) + 55*Power(t2,3)))) - 
         Power(s1,4)*(-24 + Power(t1,4)*(6 - 18*t2) + 
            Power(s2,5)*(-7 + 7*t1 - t2) - 27*t2 + 8*Power(t2,2) + 
            50*Power(t2,3) - 15*Power(t2,4) + 
            Power(s2,4)*(-12 + 2*Power(t1,2) + t1*(30 - 89*t2) + 7*t2 + 
               15*Power(t2,2)) - 
            Power(t1,3)*(25 - 26*t2 + 22*Power(t2,2) + 10*Power(t2,3)) + 
            t1*(47 - 125*t2 - 13*Power(t2,2) + 100*Power(t2,3) - 
               40*Power(t2,4)) + 
            Power(t1,2)*(19 + 84*t2 - 3*Power(t2,2) - 15*Power(t2,4)) + 
            Power(s2,3)*(-71 + 9*Power(t1,3) + 
               Power(t1,2)*(37 - 31*t2) + 267*t2 - 14*Power(t2,2) - 
               45*Power(t2,3) + t1*(-144 - 59*t2 + 301*Power(t2,2))) + 
            Power(s2,2)*(165 + 394*t2 - 578*Power(t2,2) - 
               15*Power(t2,3) + 35*Power(t2,4) - 
               5*Power(t1,3)*(-4 + 5*t2) + 
               Power(t1,2)*(25 - 251*t2 + 6*Power(t2,2)) + 
               t1*(-253 + 582*t2 + 78*Power(t2,2) - 300*Power(t2,3))) + 
            s2*(189 + 14*Power(t1,4) - 335*t2 - 123*Power(t2,2) + 
               65*Power(t2,3) + 55*Power(t2,4) + 
               Power(t1,3)*(-43 + 21*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(-205 + 122*t2 + 116*Power(t2,2) + 
                  70*Power(t2,3)) + 
               t1*(46 + 374*t2 - 541*Power(t2,2) + 85*Power(t2,3) + 
                  50*Power(t2,4)))) - 
         Power(s1,3)*(-91 + 10*Power(t1,5) + 148*t2 + 83*Power(t2,2) - 
            12*Power(t2,3) - 80*Power(t2,4) + 20*Power(t2,5) + 
            Power(s2,6)*(-8 + t1 + t2) + 
            2*Power(t1,4)*(-24 - 8*t2 + 9*Power(t2,2)) + 
            Power(t1,3)*(-35 + 99*t2 + 10*Power(t2,2) + 
               18*Power(t2,3) + 5*Power(t2,4)) + 
            Power(t1,2)*(160 - 23*t2 - 247*Power(t2,2) + 
               22*Power(t2,3) + 15*Power(t2,4) + 6*Power(t2,5)) + 
            2*t1*(10 - 150*t2 + 128*Power(t2,2) + 6*Power(t2,3) - 
               40*Power(t2,4) + 15*Power(t2,5)) - 
            Power(s2,5)*(-9 + Power(t1,2) - 7*t2 + 6*Power(t2,2) + 
               7*t1*(-4 + 3*t2)) + 
            Power(s2,4)*(-57 + 4*Power(t1,3) + 38*t2 - 28*Power(t2,2) - 
               Power(t1,2)*(7 + 20*t2) + 
               6*t1*(-12 - 3*t2 + 25*Power(t2,2))) + 
            Power(s2,3)*(128 + Power(t1,3)*(19 - 26*t2) + 114*t2 - 
               530*Power(t2,2) + 126*Power(t2,3) + 25*Power(t2,4) + 
               Power(t1,2)*(26 - 145*t2 + 87*Power(t2,2)) - 
               t1*(63 - 587*t2 + 101*Power(t2,2) + 344*Power(t2,3))) + 
            Power(s2,2)*(375 + 20*Power(t1,4) - 906*t2 - 
               491*Power(t2,2) + 872*Power(t2,3) - 40*Power(t2,4) - 
               21*Power(t2,5) + 
               Power(t1,3)*(-31 - 106*t2 + 33*Power(t2,2)) + 
               Power(t1,2)*(-290 + 238*t2 + 501*Power(t2,2) - 
                  44*Power(t2,3)) + 
               t1*(49 + 857*t2 - 1409*Power(t2,2) + 28*Power(t2,3) + 
                  250*Power(t2,4))) - 
            s2*(4*Power(t1,4)*(-4 + 9*t2) + 
               Power(t1,3)*(183 - 21*t2 - 11*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(88 - 1090*t2 + 337*Power(t2,2) + 
                  184*Power(t2,3) + 40*Power(t2,4)) + 
               2*(90 + 333*t2 - 426*Power(t2,2) - 86*Power(t2,3) + 
                  45*Power(t2,4) + 25*Power(t2,5)) + 
               t1*(-449 + 437*t2 + 844*Power(t2,2) - 714*Power(t2,3) + 
                  105*Power(t2,4) + 27*Power(t2,5)))) + 
         Power(s1,2)*(-66 + 2*Power(s2,7) - 293*t2 + 308*Power(t2,2) + 
            127*Power(t2,3) - 13*Power(t2,4) - 70*Power(t2,5) + 
            15*Power(t2,6) + 2*Power(t1,5)*(-5 + 7*t2) + 
            Power(s2,6)*(-7 + t1*(-5 + t2) - 14*t2 + 3*Power(t2,2)) + 
            2*Power(t1,4)*(69 - 23*t2 - 9*Power(t2,2) + 3*Power(t2,3)) + 
            Power(t1,3)*(-211 - 346*t2 + 141*Power(t2,2) + 
               46*Power(t2,3) + 7*Power(t2,4) + Power(t2,5)) + 
            Power(t1,2)*(-88 + 704*t2 + 83*Power(t2,2) - 
               277*Power(t2,3) + 23*Power(t2,4) + 12*Power(t2,5) + 
               Power(t2,6)) + 
            t1*(233 + 15*t2 - 652*Power(t2,2) + 218*Power(t2,3) + 
               13*Power(t2,4) - 27*Power(t2,5) + 12*Power(t2,6)) + 
            Power(s2,5)*(8 + 19*t2 - 12*Power(t2,2) - 14*Power(t2,3) + 
               Power(t1,2)*(-1 + 2*t2) + 
               t1*(35 + 55*t2 - 22*Power(t2,2))) + 
            Power(s2,4)*(-62 - 165*t2 + 27*Power(t2,2) - 
               62*Power(t2,3) + 15*Power(t2,4) + 
               Power(t1,3)*(1 + 8*t2) - 
               Power(t1,2)*(67 + 33*t2 + 46*Power(t2,2)) + 
               t1*(36 - 134*t2 + 115*Power(t2,2) + 126*Power(t2,3))) + 
            Power(s2,3)*(-141 - 8*Power(t1,4) + 211*t2 - 
               121*Power(t2,2) - 528*Power(t2,3) + 189*Power(t2,4) + 
               3*Power(t2,5) + 
               Power(t1,3)*(64 + 83*t2 - 25*Power(t2,2)) + 
               Power(t1,2)*(66 - 191*t2 - 215*Power(t2,2) + 
                  105*Power(t2,3)) + 
               t1*(105 + 155*t2 + 961*Power(t2,2) - 287*Power(t2,3) - 
                  221*Power(t2,4))) + 
            Power(s2,2)*(407 + 36*Power(t1,4)*(-1 + t2) + 477*t2 - 
               1699*Power(t2,2) - 159*Power(t2,3) + 738*Power(t2,4) - 
               61*Power(t2,5) - 7*Power(t2,6) + 
               Power(t1,3)*(56 - 30*t2 - 188*Power(t2,2) + 
                  19*Power(t2,3)) + 
               Power(t1,2)*(-91 - 1176*t2 + 760*Power(t2,2) + 
                  497*Power(t2,3) - 51*Power(t2,4)) + 
               t1*(-356 + 1078*t2 + 1110*Power(t2,2) - 
                  1665*Power(t2,3) + 87*Power(t2,4) + 124*Power(t2,5))) \
- s2*(-529 + 10*Power(t1,5) + 1005*t2 + 822*Power(t2,2) - 
               1056*Power(t2,3) - 138*Power(t2,4) + 77*Power(t2,5) + 
               27*Power(t2,6) + 
               2*Power(t1,4)*(-68 - 38*t2 + 15*Power(t2,2)) + 
               Power(t1,3)*(-194 + 697*t2 + 150*Power(t2,2) - 
                  49*Power(t2,3) + 3*Power(t2,4)) + 
               Power(t1,2)*(447 + 262*t2 - 1986*Power(t2,2) + 
                  369*Power(t2,3) + 146*Power(t2,4) + 11*Power(t2,5)) + 
               2*t1*(202 - 969*t2 + 531*Power(t2,2) + 504*Power(t2,3) - 
                  258*Power(t2,4) + 31*Power(t2,5) + 4*Power(t2,6)))) + 
         s1*(-227 + 354*t2 - 4*Power(s2,7)*t2 + 312*Power(t2,2) - 
            268*Power(t2,3) - 93*Power(t2,4) + 8*Power(t2,5) + 
            32*Power(t2,6) - 6*Power(t2,7) + 
            Power(t1,5)*(70 + 16*t2 - 4*Power(t2,2)) + 
            2*Power(t1,4)*(-7 - 125*t2 - 14*Power(t2,2) + 
               6*Power(t2,3)) - 
            Power(t1,3)*(324 - 208*t2 - 536*Power(t2,2) + 
               85*Power(t2,3) + 28*Power(t2,4) + Power(t2,5)) + 
            Power(t1,2)*(165 + 764*t2 - 875*Power(t2,2) - 
               159*Power(t2,3) + 129*Power(t2,4) - 9*Power(t2,5) - 
               3*Power(t2,6)) - 
            t1*(-330 + 1084*t2 - 11*Power(t2,2) - 592*Power(t2,3) + 
               68*Power(t2,4) + 10*Power(t2,5) + Power(t2,6) + 
               2*Power(t2,7)) + 
            Power(s2,6)*(-4 + 12*t2 + 4*Power(t2,2) - 3*Power(t2,3) + 
               t1*(4 + 12*t2 + Power(t2,2))) - 
            Power(s2,5)*(-14 + 33*t2 + 15*Power(t2,2) - 
               17*Power(t2,3) - 11*Power(t2,4) + 
               Power(t1,2)*(12 + 5*t2 + 7*Power(t2,2)) + 
               t1*(2 + 46*t2 + 22*Power(t2,2) - 9*Power(t2,3))) + 
            Power(s2,4)*(-40 + 125*t2 + 154*Power(t2,2) + 
               12*Power(t2,3) + 58*Power(t2,4) - 12*Power(t2,5) + 
               Power(t1,3)*(4 + 7*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(42 + 65*t2 + 36*Power(t2,2) + 
                  40*Power(t2,3)) - 
               t1*(6 + 13*t2 - 66*Power(t2,2) + 164*Power(t2,3) + 
                  53*Power(t2,4))) + 
            Power(s2,3)*(151 + 277*t2 + 8*Power(t1,4)*t2 - 
               24*Power(t2,2) + 300*Power(t2,3) + 264*Power(t2,4) - 
               119*Power(t2,5) + 3*Power(t2,6) + 
               Power(t1,3)*(-46 - 97*t2 - 100*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(43 - 35*t2 + 444*Power(t2,2) + 
                  143*Power(t2,3) - 58*Power(t2,4)) + 
               t1*(-148 - 325*t2 - 506*Power(t2,2) - 737*Power(t2,3) + 
                  220*Power(t2,4) + 76*Power(t2,5))) - 
            Power(s2,2)*(-83 + 4*Power(t1,5) + 489*t2 - 
               163*Power(t2,2) - 1340*Power(t2,3) + 104*Power(t2,4) + 
               332*Power(t2,5) - 34*Power(t2,6) - Power(t2,7) + 
               4*Power(t1,4)*(-13 - 17*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(54 + 415*t2 + 36*Power(t2,2) - 
                  138*Power(t2,3) + 4*Power(t2,4)) - 
               Power(t1,2)*(247 + 951*t2 + 1512*Power(t2,2) - 
                  706*Power(t2,3) - 245*Power(t2,4) + 24*Power(t2,5)) + 
               t1*(324 + 75*t2 + 2024*Power(t2,2) + 661*Power(t2,3) - 
                  966*Power(t2,4) + 48*Power(t2,5) + 34*Power(t2,6))) + 
            s2*(-473 + 8*Power(t1,5)*(-1 + t2) - 430*t2 + 
               1438*Power(t2,2) + 402*Power(t2,3) - 641*Power(t2,4) - 
               60*Power(t2,5) + 36*Power(t2,6) + 8*Power(t2,7) + 
               2*Power(t1,4)*
                (61 - 63*t2 - 48*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(-392 - 871*t2 + 782*Power(t2,2) + 
                  191*Power(t2,3) - 36*Power(t2,4)) + 
               Power(t1,2)*(-69 + 2172*t2 + 322*Power(t2,2) - 
                  1522*Power(t2,3) + 170*Power(t2,4) + 56*Power(t2,5) + 
                  Power(t2,6)) + 
               t1*(820 - 749*t2 - 2504*Power(t2,2) + 997*Power(t2,3) + 
                  620*Power(t2,4) - 193*Power(t2,5) + 16*Power(t2,6) + 
                  Power(t2,7)))) + 
         Power(s,5)*(3 - 39*t1 + 30*Power(t1,2) - 39*Power(t1,3) + 
            6*Power(t1,4) + 4*t2 - 42*t1*t2 + 13*Power(t1,2)*t2 + 
            23*Power(t1,3)*t2 + 12*Power(t2,2) + 151*t1*Power(t2,2) - 
            120*Power(t1,2)*Power(t2,2) + 10*Power(t1,3)*Power(t2,2) - 
            41*Power(t2,3) + 177*t1*Power(t2,3) + 
            10*Power(t1,2)*Power(t2,3) - 86*Power(t2,4) - 
            50*t1*Power(t2,4) + 30*Power(t2,5) + 
            Power(s1,3)*(6*Power(s2,2) - 91*Power(t1,2) + 
               2*(13 - 15*t2)*t2 + s2*(-14 + 57*t1 + 10*t2) + 
               10*t1*(-9 + 11*t2)) + 
            Power(s2,3)*(6 - 11*Power(t1,2) - 34*t2 - 24*Power(t2,2) + 
               t1*(23 + 35*t2)) + 
            Power(s2,2)*(-31 + 7*Power(t1,3) - 34*t2 + 71*Power(t2,2) + 
               87*Power(t2,3) + Power(t1,2)*(-53 + 29*t2) + 
               t1*(18 + 60*t2 - 123*Power(t2,2))) + 
            s2*(20 + 106*t2 + 102*Power(t2,2) + 58*Power(t2,3) - 
               93*Power(t2,4) - 4*Power(t1,3)*(8 + 3*t2) + 
               Power(t1,2)*(123 + 181*t2 - 39*Power(t2,2)) + 
               t1*(-31 - 310*t2 - 279*Power(t2,2) + 144*Power(t2,3))) + 
            Power(s1,2)*(-8 + 3*Power(s2,3) + 21*Power(t1,3) + 20*t2 - 
               138*Power(t2,2) + 90*Power(t2,3) + 
               Power(s2,2)*(-46 + 79*t1 + 10*t2) + 
               3*Power(t1,2)*(-11 + 57*t2) + 
               t1*(40 + 333*t2 - 270*Power(t2,2)) + 
               s2*(65 - 156*Power(t1,2) + 16*t2 - 113*Power(t2,2) + 
                  2*t1*(-79 + 61*t2))) + 
            s1*(-5 + 13*t2 + 21*Power(t2,2) + 198*Power(t2,3) - 
               90*Power(t2,4) + Power(s2,3)*(-27 + 22*t1 + 7*t2) - 
               3*Power(t1,3)*(21 + 10*t2) + 
               Power(t1,2)*(76 + 189*t2 - 90*Power(t2,2)) - 
               Power(s2,2)*(-97 + 70*Power(t1,2) + t1*(28 - 75*t2) + 
                  51*t2 + 103*Power(t2,2)) + 
               t1*(56 - 268*t2 - 420*Power(t2,2) + 210*Power(t2,3)) + 
               s2*(-69 + 18*Power(t1,3) - 93*t2 - 60*Power(t2,2) + 
                  196*Power(t2,3) + Power(t1,2)*(-77 + 181*t2) + 
                  t1*(51 + 409*t2 - 323*Power(t2,2))))) - 
         Power(s,4)*(13 + 91*t1 - 150*Power(t1,2) + 115*Power(t1,3) - 
            10*Power(t1,4) - 96*t2 + 472*t1*t2 - 405*Power(t1,2)*t2 + 
            162*Power(t1,3)*t2 - 18*Power(t1,4)*t2 - 168*Power(t2,2) + 
            400*t1*Power(t2,2) - 289*Power(t1,2)*Power(t2,2) + 
            4*Power(t1,3)*Power(t2,2) - 140*Power(t2,3) + 
            22*t1*Power(t2,3) + 116*Power(t1,2)*Power(t2,3) - 
            10*Power(t1,3)*Power(t2,3) - 25*Power(t2,4) - 
            212*t1*Power(t2,4) + 110*Power(t2,5) + 30*t1*Power(t2,5) - 
            20*Power(t2,6) + Power(s1,4)*
             (15*Power(s2,2) - 105*Power(t1,2) + 4*(6 - 5*t2)*t2 + 
               2*s2*(-13 + 45*t1 + 5*t2) + 10*t1*(-11 + 10*t2)) + 
            Power(s2,3)*(-35 + 9*Power(t1,3) + 
               2*Power(t1,2)*(-32 + t2) - 91*t2 + 99*Power(t2,2) + 
               85*Power(t2,3) + t1*(13 + 37*t2 - 96*Power(t2,2))) + 
            Power(s2,4)*(-5*Power(t1,2) + 21*t1*(1 + t2) - 
               2*(-3 + 7*t2 + 8*Power(t2,2))) + 
            Power(s2,2)*(24 + 240*t2 + 255*Power(t2,2) + 
               34*Power(t2,3) - 141*Power(t2,4) - 
               Power(t1,3)*(23 + 25*t2) + 
               Power(t1,2)*(155 + 333*t2 + 9*Power(t2,2)) + 
               t1*(-9 - 423*t2 - 539*Power(t2,2) + 157*Power(t2,3))) + 
            s2*(17 + 14*Power(t1,4) + 126*t2 - 20*Power(t2,2) - 
               282*Power(t2,3) - 205*Power(t2,4) + 92*Power(t2,5) + 
               3*Power(t1,3)*(-59 + 14*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(249 + 84*t2 - 406*Power(t2,2) + 
                  16*Power(t2,3)) + 
               t1*(-254 - 467*t2 + 635*Power(t2,2) + 665*Power(t2,3) - 
                  126*Power(t2,4))) + 
            Power(s1,3)*(20 + 15*Power(s2,3) + 35*Power(t1,3) + 
               Power(s2,2)*(-107 + 200*t1 - 7*t2) + 46*t2 - 
               182*Power(t2,2) + 80*Power(t2,3) + 
               5*Power(t1,2)*(-13 + 54*t2) + 
               t1*(-8 + 584*t2 - 330*Power(t2,2)) + 
               s2*(60 - 250*Power(t1,2) + 105*t2 - 122*Power(t2,2) + 
                  t1*(-319 + 25*t2))) + 
            Power(s1,2)*(-83 + 3*Power(s2,4) - 57*t2 - 
               117*Power(t2,2) + 402*Power(t2,3) - 120*Power(t2,4) - 
               15*Power(t1,3)*(9 + 5*t2) + 
               Power(s2,3)*(-118 + 115*t1 + 8*t2) + 
               Power(t1,2)*(49 + 348*t2 - 225*Power(t2,2)) + 
               t1*(269 - 225*t2 - 1050*Power(t2,2) + 390*Power(t2,3)) - 
               Power(s2,2)*(-240 + 155*Power(t1,2) - 111*t2 + 
                  172*Power(t2,2) + 2*t1*(107 + 62*t2)) + 
               s2*(40 + 45*Power(t1,3) - 233*t2 - 337*Power(t2,2) + 
                  306*Power(t2,3) + Power(t1,2)*(-146 + 455*t2) + 
                  t1*(-48 + 1297*t2 - 446*Power(t2,2)))) + 
            s1*(27 + 30*Power(t1,4) + 209*t2 + 177*Power(t2,2) + 
               96*Power(t2,3) - 354*Power(t2,4) + 80*Power(t2,5) + 
               Power(s2,4)*(-41 + 18*t1 + 7*t2) + 
               Power(t1,3)*(-158 + 105*t2 + 50*Power(t2,2)) + 
               Power(t1,2)*(194 + 301*t2 - 399*Power(t2,2) + 
                  60*Power(t2,3)) + 
               t1*(-298 - 631*t2 + 211*Power(t2,2) + 788*Power(t2,3) - 
                  190*Power(t2,4)) + 
               Power(s2,3)*(161 - 41*Power(t1,2) - 17*t2 - 
                  108*Power(t2,2) + t1*(22 + 4*t2)) + 
               Power(s2,2)*(-145 + 35*Power(t1,3) - 395*t2 - 
                  38*Power(t2,2) + 305*Power(t2,3) + 
                  Power(t1,2)*(-185 + 128*t2) + 
                  t1*(-52 + 746*t2 - 233*Power(t2,2))) + 
               s2*(-86 + 24*t2 + 455*Power(t2,2) + 463*Power(t2,3) - 
                  286*Power(t2,4) - Power(t1,3)*(139 + 60*t2) + 
                  Power(t1,2)*(205 + 619*t2 - 221*Power(t2,2)) + 
                  t1*(473 - 788*t2 - 1643*Power(t2,2) + 457*Power(t2,3)))\
)) + Power(s,3)*(-100 + 405*t1 - 566*Power(t1,2) + 333*Power(t1,3) - 
            128*Power(t1,4) + 10*Power(t1,5) - 453*t2 + 729*t1*t2 - 
            493*Power(t1,2)*t2 + 145*Power(t1,3)*t2 - 
            26*Power(t1,4)*t2 - 173*Power(t2,2) - 379*t1*Power(t2,2) + 
            404*Power(t1,2)*Power(t2,2) - 140*Power(t1,3)*Power(t2,2) + 
            18*Power(t1,4)*Power(t2,2) + 213*Power(t2,3) - 
            511*t1*Power(t2,3) + 388*Power(t1,2)*Power(t2,3) - 
            30*Power(t1,3)*Power(t2,3) + 170*Power(t2,4) - 
            197*t1*Power(t2,4) - 33*Power(t1,2)*Power(t2,4) + 
            5*Power(t1,3)*Power(t2,4) + 105*Power(t2,5) + 
            114*t1*Power(t2,5) - 3*Power(t1,2)*Power(t2,5) - 
            69*Power(t2,6) - 9*t1*Power(t2,6) + 7*Power(t2,7) + 
            Power(s2,5)*(2 - Power(t1,2) + 7*t2 - 6*Power(t2,2) + 
               t1*(10 + 7*t2)) + 
            Power(s1,5)*(20*Power(s2,2) - 77*Power(t1,2) + 
               (11 - 7*t2)*t2 + s2*(-24 + 85*t1 + 5*t2) + 
               t1*(-75 + 54*t2)) + 
            Power(s2,4)*(-3 + 4*Power(t1,3) - 74*t2 + 58*Power(t2,2) + 
               48*Power(t2,3) - 4*Power(t1,2)*(7 + 2*t2) - 
               t1*(15 + 27*t2 + 44*Power(t2,2))) + 
            Power(s2,3)*(5 + 210*t2 + 268*Power(t2,2) - 
               12*Power(t2,3) - 113*Power(t2,4) - 
               2*Power(t1,3)*(2 + 13*t2) + 
               Power(t1,2)*(101 + 289*t2 + 69*Power(t2,2)) + 
               t1*(4 - 199*t2 - 451*Power(t2,2) + 70*Power(t2,3))) + 
            Power(s2,2)*(-55 + 20*Power(t1,4) + 48*t2 - 
               53*Power(t2,2) - 431*Power(t2,3) - 244*Power(t2,4) + 
               111*Power(t2,5) + 
               Power(t1,3)*(-238 - 58*t2 + 33*Power(t2,2)) + 
               Power(t1,2)*(260 + 294*t2 - 480*Power(t2,2) - 
                  75*Power(t2,3)) + 
               t1*(-163 - 815*t2 + 547*Power(t2,2) + 1022*Power(t2,3) - 
                  69*Power(t2,4))) + 
            s2*(191 + 233*t2 - 668*Power(t2,2) - 672*Power(t2,3) + 
               153*Power(t2,4) + 227*Power(t2,5) - 48*Power(t2,6) - 
               2*Power(t1,4)*(1 + 18*t2) + 
               Power(t1,3)*(161 + 596*t2 + 50*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,2)*(52 - 1371*t2 - 1171*Power(t2,2) + 
                  320*Power(t2,3) + 6*Power(t2,4)) + 
               t1*(-255 + 1028*t2 + 2224*Power(t2,2) + 14*Power(t2,3) - 
                  661*Power(t2,4) + 54*Power(t2,5))) + 
            Power(s1,4)*(38 + 30*Power(s2,3) + 35*Power(t1,3) + 
               3*Power(s2,2)*(-37 + 90*t1 - 16*t2) + 49*t2 - 
               113*Power(t2,2) + 35*Power(t2,3) + 
               15*Power(t1,2)*(-5 + 17*t2) + 
               t1*(-81 + 526*t2 - 225*Power(t2,2)) - 
               s2*(26 + 240*Power(t1,2) - 132*t2 + 68*Power(t2,2) + 
                  t1*(301 + 130*t2))) + 
            Power(s1,3)*(-88 + 12*Power(s2,4) + 
               Power(s2,3)*(-188 + 240*t1 - 35*t2) - 135*t2 - 
               252*Power(t2,2) + 342*Power(t2,3) - 70*Power(t2,4) - 
               50*Power(t1,3)*(3 + 2*t2) - 
               2*Power(t1,2)*(8 - 171*t2 + 150*Power(t2,2)) + 
               2*t1*(174 + 94*t2 - 621*Power(t2,2) + 180*Power(t2,3)) - 
               Power(s2,2)*(-91 + 180*Power(t1,2) - 340*t2 + 
                  87*Power(t2,2) + t1*(319 + 566*t2)) + 
               s2*(352 + 60*Power(t1,3) - 11*t2 - 479*Power(t2,2) + 
                  222*Power(t2,3) + 2*Power(t1,2)*(-67 + 305*t2) + 
                  t1*(-404 + 1703*t2 - 174*Power(t2,2)))) + 
            Power(s1,2)*(-235 + Power(s2,5) + 60*Power(t1,4) + 406*t2 + 
               326*Power(t2,2) + 462*Power(t2,3) - 458*Power(t2,4) + 
               70*Power(t2,5) + Power(s2,4)*(-126 + 75*t1 + 8*t2) + 
               2*Power(t1,3)*(-127 + 95*t2 + 50*Power(t2,2)) + 
               2*Power(t1,2)*
                (119 + 347*t2 - 246*Power(t2,2) + 70*Power(t2,3)) - 
               t1*(170 + 1401*t2 + 330*Power(t2,2) - 
                  1320*Power(t2,3) + 270*Power(t2,4)) - 
               Power(s2,3)*(-298 + 54*Power(t1,2) - 222*t2 + 
                  133*Power(t2,2) + t1*(48 + 338*t2)) + 
               Power(s2,2)*(30 + 70*Power(t1,3) - 407*t2 - 
                  591*Power(t2,2) + 349*Power(t2,3) + 
                  Power(t1,2)*(-187 + 222*t2) + 
                  t1*(-578 + 1734*t2 + 253*Power(t2,2))) - 
               s2*(63 + 1139*t2 - 253*Power(t2,2) - 885*Power(t2,3) + 
                  308*Power(t2,4) + 4*Power(t1,3)*(59 + 30*t2) + 
                  Power(t1,2)*(88 - 739*t2 + 494*Power(t2,2)) - 
                  t1*(1373 + 274*t2 - 3164*Power(t2,2) + 
                     452*Power(t2,3)))) + 
            s1*(364 + 362*t2 - 531*Power(t2,2) - 399*Power(t2,3) - 
               364*Power(t2,4) + 287*Power(t2,5) - 35*Power(t2,6) - 
               24*Power(t1,4)*(1 + 3*t2) + 
               Power(s2,5)*(-31 + 7*t1 + 4*t2) + 
               Power(s2,4)*(108 - 13*Power(t1,2) + t1*(70 - 23*t2) + 
                  46*t2 - 68*Power(t2,2)) - 
               2*Power(t1,3)*
                (-70 - 202*t2 + 5*Power(t2,2) + 20*Power(t2,3)) + 
               Power(t1,2)*(231 - 815*t2 - 1066*Power(t2,2) + 
                  258*Power(t2,3) - 15*Power(t2,4)) + 
               t1*(-549 + 734*t2 + 1564*Power(t2,2) + 420*Power(t2,3) - 
                  643*Power(t2,4) + 90*Power(t2,5)) + 
               Power(s2,3)*(-113 + 36*Power(t1,3) - 492*t2 - 
                  22*Power(t2,2) + 251*Power(t2,3) - 
                  5*Power(t1,2)*(36 + 5*t2) + 
                  t1*(-189 + 501*t2 + 28*Power(t2,2))) + 
               Power(s2,2)*(79 + 59*t2 + 747*Power(t2,2) + 
                  606*Power(t2,3) - 345*Power(t2,4) - 
                  Power(t1,3)*(49 + 100*t2) + 
                  Power(t1,2)*(131 + 718*t2 + 33*Power(t2,2)) + 
                  t1*(650 - 163*t2 - 2437*Power(t2,2) + 112*Power(t2,3))\
) + s2*(-458 + 56*Power(t1,4) + 652*t2 + 1459*Power(t2,2) - 
                  369*Power(t2,3) - 741*Power(t2,4) + 197*Power(t2,5) + 
                  Power(t1,3)*(-562 + 147*t2 + 72*Power(t2,2)) + 
                  Power(t1,2)*
                   (686 + 1387*t2 - 925*Power(t2,2) + 118*Power(t2,3)) + 
                  t1*(-390 - 3536*t2 + 116*Power(t2,2) + 
                     2423*Power(t2,3) - 287*Power(t2,4))))) - 
         Power(s,2)*(-425 + 855*t1 - 576*Power(t1,2) + 152*Power(t1,3) - 
            18*Power(t1,4) + 26*Power(t1,5) - 703*t2 + 196*t1*t2 + 
            1131*Power(t1,2)*t2 - 778*Power(t1,3)*t2 + 
            142*Power(t1,4)*t2 - 14*Power(t1,5)*t2 + 422*Power(t2,2) - 
            1979*t1*Power(t2,2) + 1791*Power(t1,2)*Power(t2,2) - 
            441*Power(t1,3)*Power(t2,2) + 56*Power(t1,4)*Power(t2,2) + 
            726*Power(t2,3) - 720*t1*Power(t2,3) + 
            58*Power(t1,2)*Power(t2,3) + 6*Power(t1,3)*Power(t2,3) - 
            6*Power(t1,4)*Power(t2,3) + 84*Power(t2,4) + 
            164*t1*Power(t2,4) - 167*Power(t1,2)*Power(t2,4) + 
            16*Power(t1,3)*Power(t2,4) - 39*Power(t2,5) + 
            111*t1*Power(t2,5) - 6*Power(t1,2)*Power(t2,5) - 
            Power(t1,3)*Power(t2,5) - 76*Power(t2,6) - 
            24*t1*Power(t2,6) + Power(t1,2)*Power(t2,6) + 
            20*Power(t2,7) + t1*Power(t2,7) - Power(t2,8) + 
            Power(s2,6)*(-((-8 + t2)*t2) + t1*(2 + t2)) + 
            Power(s1,6)*(15*Power(s2,2) - 35*Power(t1,2) - 
               (-2 + t2)*t2 + s2*(-11 + 48*t1 + t2) + t1*(-27 + 16*t2)) \
- Power(s2,5)*(-10 + 23*t2 - 8*Power(t2,2) - 15*Power(t2,3) + 
               2*Power(t1,2)*(1 + t2) + t1*(16 + 36*t2 + 13*Power(t2,2))\
) - Power(s2,4)*(7 - 73*t2 - 122*Power(t2,2) + 36*Power(t2,3) + 
               51*Power(t2,4) + Power(t1,3)*(1 + 8*t2) - 
               Power(t1,2)*(49 + 103*t2 + 43*Power(t2,2)) + 
               t1*(15 - 13*t2 + 117*Power(t2,2) - 16*Power(t2,3))) + 
            Power(s2,3)*(-44 + 8*Power(t1,4) - 157*t2 - 
               175*Power(t2,2) - 265*Power(t2,3) - 151*Power(t2,4) + 
               68*Power(t2,5) + 
               Power(t1,3)*(-96 - 66*t2 + 25*Power(t2,2)) + 
               Power(t1,2)*(27 + 116*t2 - 284*Power(t2,2) - 
                  112*Power(t2,3)) + 
               t1*(41 - 330*t2 + 143*Power(t2,2) + 705*Power(t2,3) + 
                  19*Power(t2,4))) + 
            Power(s2,2)*(-106 + Power(t1,4)*(22 - 36*t2) + 225*t2 - 
               298*Power(t2,2) - 768*Power(t2,3) + 110*Power(t2,4) + 
               243*Power(t2,5) - 42*Power(t2,6) + 
               Power(t1,3)*(57 + 510*t2 + 205*Power(t2,2) - 
                  19*Power(t2,3)) + 
               Power(t1,2)*(-3 - 819*t2 - 1433*Power(t2,2) + 
                  149*Power(t2,3) + 76*Power(t2,4)) + 
               t1*(116 + 624*t2 + 2278*Power(t2,2) + 483*Power(t2,3) - 
                  756*Power(t2,4) - 15*Power(t2,5))) + 
            s2*(516 + 10*Power(t1,5) + 31*t2 - 1089*Power(t2,2) + 
               140*Power(t2,3) + 808*Power(t2,4) + 81*Power(t2,5) - 
               103*Power(t2,6) + 12*Power(t2,7) + 
               10*Power(t1,4)*(-26 - 10*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(592 + 940*t2 - 426*Power(t2,2) - 
                  106*Power(t2,3) + 3*Power(t2,4)) - 
               Power(t1,2)*(359 + 2473*t2 - 180*Power(t2,2) - 
                  1415*Power(t2,3) + 58*Power(t2,4) + 6*Power(t2,5)) + 
               t1*(-555 + 1432*t2 + 1073*Power(t2,2) - 
                  1911*Power(t2,3) - 625*Power(t2,4) + 
                  291*Power(t2,5) - 9*Power(t2,6))) + 
            Power(s1,5)*(19 + 30*Power(s2,3) + 21*Power(t1,3) + 
               Power(s2,2)*(-46 + 205*t1 - 62*t2) + 23*t2 - 
               30*Power(t2,2) + 6*Power(t2,3) + 
               3*Power(t1,2)*(-17 + 48*t2) + 
               t1*(-74 + 243*t2 - 81*Power(t2,2)) - 
               s2*(64 + 138*Power(t1,2) - 64*t2 + 17*Power(t2,2) + 
                  t1*(131 + 157*t2))) + 
            Power(s1,4)*(31 + 18*Power(s2,4) + 
               Power(s2,3)*(-123 + 250*t1 - 83*t2) - 74*t2 - 
               168*Power(t2,2) + 120*Power(t2,3) - 15*Power(t2,4) - 
               15*Power(t1,3)*(6 + 5*t2) + 
               Power(t1,2)*(-17 + 198*t2 - 225*Power(t2,2)) + 
               t1*(161 + 363*t2 - 726*Power(t2,2) + 165*Power(t2,3)) - 
               Power(s2,2)*(115*Power(t1,2) + 2*t1*(74 + 357*t2) - 
                  28*(-7 + 9*t2 + 2*Power(t2,2))) + 
               s2*(291 + 45*Power(t1,3) + 262*t2 - 249*Power(t2,2) + 
                  70*Power(t2,3) + Power(t1,2)*(-56 + 460*t2) + 
                  t1*(-519 + 1012*t2 + 139*Power(t2,2)))) + 
            Power(s1,3)*(-345 + 3*Power(s2,5) + 60*Power(t1,4) + 
               Power(s2,4)*(-132 + 117*t1 - 17*t2) - 65*t2 + 
               147*Power(t2,2) + 442*Power(t2,3) - 220*Power(t2,4) + 
               20*Power(t2,5) + 
               2*Power(t1,3)*(-102 + 85*t2 + 50*Power(t2,2)) + 
               2*Power(t1,2)*
                (37 + 265*t2 - 141*Power(t2,2) + 80*Power(t2,3)) + 
               t1*(352 - 971*t2 - 756*Power(t2,2) + 1014*Power(t2,3) - 
                  170*Power(t2,4)) + 
               Power(s2,3)*(75 - 26*Power(t1,2) + 339*t2 + 
                  Power(t2,2) - t1*(33 + 688*t2)) + 
               Power(s2,2)*(237 + 70*Power(t1,3) + 567*t2 - 
                  723*Power(t2,2) + 96*Power(t2,3) + 
                  Power(t1,2)*(19 + 188*t2) + 
                  3*t1*(-347 + 462*t2 + 309*Power(t2,2))) - 
               s2*(-463 + 1441*t2 + 483*Power(t2,2) - 576*Power(t2,3) + 
                  130*Power(t2,4) + 2*Power(t1,3)*(97 + 60*t2) + 
                  Power(t1,2)*(308 - 313*t2 + 546*Power(t2,2)) - 
                  t1*(899 + 1765*t2 - 2541*Power(t2,2) + 54*Power(t2,3))\
)) + Power(s1,2)*(-169 + 1340*t2 + 121*Power(t2,2) - 187*Power(t2,3) - 
               548*Power(t2,4) + 210*Power(t2,5) - 15*Power(t2,6) + 
               Power(s2,5)*(-69 + 21*t1 + 7*t2) - 
               12*Power(t1,4)*(1 + 9*t2) - 
               6*Power(t1,3)*
                (-1 - 58*t2 + 9*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,2)*(827 - 123*t2 - 1176*Power(t2,2) + 
                  168*Power(t2,3) - 45*Power(t2,4)) + 
               t1*(-861 - 1251*t2 + 1623*Power(t2,2) + 
                  830*Power(t2,3) - 711*Power(t2,4) + 90*Power(t2,5)) + 
               Power(s2,4)*(154 - 9*Power(t1,2) + 155*t2 - 
                  71*Power(t2,2) - 3*t1*(-35 + 66*t2)) + 
               Power(s2,3)*(-121 + 54*Power(t1,3) - 277*t2 - 
                  460*Power(t2,2) + 195*Power(t2,3) - 
                  Power(t1,2)*(131 + 87*t2) + 
                  t1*(-609 + 835*t2 + 645*Power(t2,2))) - 
               Power(s2,2)*(-477 + 1053*t2 + 436*Power(t2,2) - 
                  1117*Power(t2,3) + 229*Power(t2,4) + 
                  3*Power(t1,3)*(3 + 50*t2) + 
                  Power(t1,2)*(206 - 186*t2 - 45*Power(t2,2)) + 
                  t1*(-1025 - 2125*t2 + 3084*Power(t2,2) + 
                     547*Power(t2,3))) + 
               s2*(-769 + 84*Power(t1,4) - 836*t2 + 2817*Power(t2,2) + 
                  517*Power(t2,3) - 709*Power(t2,4) + 125*Power(t2,5) + 
                  3*Power(t1,3)*(-212 + 63*t2 + 36*Power(t2,2)) + 
                  Power(t1,2)*
                   (-20 + 2436*t2 - 516*Power(t2,2) + 258*Power(t2,3)) \
+ t1*(988 - 3940*t2 - 2598*Power(t2,2) + 2861*Power(t2,3) - 
                     146*Power(t2,4)))) + 
            s1*(889 + 30*Power(t1,5) - 258*t2 - 1721*Power(t2,2) - 
               171*Power(t2,3) + 134*Power(t2,4) + 327*Power(t2,5) - 
               102*Power(t2,6) + 6*Power(t2,7) + 
               Power(s2,6)*(-12 + t1 + t2) + 
               Power(s2,5)*(31 - 3*Power(t1,2) + t1*(45 - 7*t2) + 
                  56*t2 - 25*Power(t2,2)) + 
               Power(t1,4)*(-240 - 68*t2 + 54*Power(t2,2)) + 
               Power(t1,3)*(643 + 597*t2 - 150*Power(t2,2) - 
                  42*Power(t2,3) + 15*Power(t2,4)) - 
               Power(t1,2)*(706 + 2809*t2 + 9*Power(t2,2) - 
                  830*Power(t2,3) + 27*Power(t2,4)) + 
               t1*(-608 + 2902*t2 + 1619*Power(t2,2) - 
                  977*Power(t2,3) - 474*Power(t2,4) + 231*Power(t2,5) - 
                  21*Power(t2,6)) + 
               Power(s2,4)*(-19 + 12*Power(t1,3) - 247*t2 + 
                  13*Power(t2,2) + 121*Power(t2,3) - 
                  4*Power(t1,2)*(16 + 9*t2) + 
                  t1*(-156 + 5*t2 + 65*Power(t2,2))) + 
               Power(s2,3)*(215 + Power(t1,3)*(11 - 78*t2) + 310*t2 + 
                  467*Power(t2,2) + 395*Power(t2,3) - 211*Power(t2,4) + 
                  3*Power(t1,2)*(52 + 148*t2 + 75*Power(t2,2)) + 
                  t1*(159 + 382*t2 - 1507*Power(t2,2) - 226*Power(t2,3))\
) + Power(s2,2)*(-327 + 60*Power(t1,4) - 265*t2 + 1584*Power(t2,2) - 
                  45*Power(t2,3) - 843*Power(t2,4) + 166*Power(t2,5) + 
                  3*Power(t1,3)*(-177 - 74*t2 + 33*Power(t2,2)) + 
                  Power(t1,2)*
                   (271 + 1710*t2 - 354*Power(t2,2) - 194*Power(t2,3)) \
+ t1*(-103 - 3222*t2 - 1567*Power(t2,2) + 2602*Power(t2,3) + 
                     144*Power(t2,4))) - 
               s2*(489 - 1845*t2 - 233*Power(t2,2) + 2475*Power(t2,3) + 
                  313*Power(t2,4) - 432*Power(t2,5) + 61*Power(t2,6) + 
                  12*Power(t1,4)*(-1 + 9*t2) + 
                  Power(t1,3)*
                   (229 - 1069*t2 - 111*Power(t2,2) + 36*Power(t2,3)) + 
                  Power(t1,2)*
                   (-1446 + 367*t2 + 3543*Power(t2,2) - 
                     317*Power(t2,3) + 28*Power(t2,4)) + 
                  t1*(494 + 1874*t2 - 4952*Power(t2,2) - 
                     1977*Power(t2,3) + 1492*Power(t2,4) - 71*Power(t2,5)\
)))) + s*(-495 + 1069*t1 - 411*Power(t1,2) - 395*Power(t1,3) + 
            270*Power(t1,4) - 38*Power(t1,5) - 418*t2 + 
            2*Power(s2,7)*t2 - 319*t1*t2 + 2115*Power(t1,2)*t2 - 
            1716*Power(t1,3)*t2 + 372*Power(t1,4)*t2 - 
            32*Power(t1,5)*t2 + 714*Power(t2,2) - 2255*t1*Power(t2,2) + 
            1877*Power(t1,2)*Power(t2,2) - 308*Power(t1,3)*Power(t2,2) - 
            18*Power(t1,4)*Power(t2,2) + 4*Power(t1,5)*Power(t2,2) + 
            543*Power(t2,3) - 319*t1*Power(t2,3) - 
            544*Power(t1,2)*Power(t2,3) + 259*Power(t1,3)*Power(t2,3) - 
            24*Power(t1,4)*Power(t2,3) - 137*Power(t2,4) + 
            407*t1*Power(t2,4) - 166*Power(t1,2)*Power(t2,4) + 
            15*Power(t1,3)*Power(t2,4) - 88*Power(t2,5) + 
            29*t1*Power(t2,5) + 27*Power(t1,2)*Power(t2,5) - 
            Power(t1,3)*Power(t2,5) - 24*Power(t2,6) - 
            13*t1*Power(t2,6) + 2*Power(t1,2)*Power(t2,6) + 
            19*Power(t2,7) + t1*Power(t2,7) - 2*Power(t2,8) + 
            Power(s1,7)*(6*Power(s2,2) + s2*(-2 + 15*t1) + 
               t1*(-4 - 9*t1 + 2*t2)) - 
            Power(s2,6)*(-2 + 3*t2 + 6*Power(t2,2) - 2*Power(t2,3) + 
               t1*(2 + 9*t2 + 2*Power(t2,2))) + 
            Power(s2,5)*(-5 + 2*t2 + 28*Power(t2,2) - 20*Power(t2,3) - 
               12*Power(t2,4) + Power(t1,2)*(9 + 8*t2 + 7*Power(t2,2)) + 
               t1*(-4 + 30*t2 + 21*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s2,4)*(5 - 83*t2 - 155*Power(t2,2) - 68*Power(t2,3) - 
               27*Power(t2,4) + 22*Power(t2,5) + 
               Power(t1,3)*(-6 - 7*t2 + 4*Power(t2,2)) - 
               Power(t1,2)*(23 + 54*t2 + 85*Power(t2,2) + 
                  42*Power(t2,3)) + 
               t1*(24 + 22*t2 + 3*Power(t2,2) + 184*Power(t2,3) + 
                  16*Power(t2,4))) + 
            Power(s2,3)*(-94 - 74*t2 - 8*Power(t1,4)*t2 + 
               51*Power(t2,2) - 200*Power(t2,3) + 24*Power(t2,4) + 
               126*Power(t2,5) - 17*Power(t2,6) + 
               Power(t1,3)*(37 + 142*t2 + 106*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-64 - 140*t2 - 471*Power(t2,2) + 
                  23*Power(t2,3) + 64*Power(t2,4)) + 
               t1*(121 + 294*t2 + 757*Power(t2,2) + 281*Power(t2,3) - 
                  370*Power(t2,4) - 39*Power(t2,5))) + 
            Power(s2,2)*(118 + 4*Power(t1,5) + 548*t2 - 
               259*Power(t2,2) - 188*Power(t2,3) + 614*Power(t2,4) + 
               154*Power(t2,5) - 85*Power(t2,6) + 6*Power(t2,7) + 
               2*Power(t1,4)*(-31 - 36*t2 + 8*Power(t2,2)) + 
               Power(t1,3)*(159 + 449*t2 - 189*Power(t2,2) - 
                  160*Power(t2,3) + 4*Power(t2,4)) - 
               Power(t1,2)*(212 + 1097*t2 + 358*Power(t2,2) - 
                  1141*Power(t2,3) - 99*Power(t2,4) + 30*Power(t2,5)) + 
               t1*(-7 + 729*Power(t2,2) - 1084*Power(t2,3) - 
                  818*Power(t2,4) + 208*Power(t2,5) + 20*Power(t2,6))) - 
            s2*(-321 + 8*Power(t1,5)*(-1 + t2) + 484*t2 + 
               401*Power(t2,2) - 1146*Power(t2,3) - 598*Power(t2,4) + 
               228*Power(t2,5) + 69*Power(t2,6) - 14*Power(t2,7) + 
               Power(t2,8) + 2*Power(t1,4)*
                (74 - 95*t2 - 64*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(-329 + 116*t2 + 1201*Power(t2,2) + 
                  36*Power(t2,3) - 54*Power(t2,4)) - 
               Power(t1,2)*(229 - 132*t2 + 2576*Power(t2,2) + 
                  1171*Power(t2,3) - 496*Power(t2,4) - 29*Power(t2,5) + 
                  Power(t2,6)) + 
               t1*(739 - 598*t2 + 1313*Power(t2,2) + 2398*Power(t2,3) - 
                  139*Power(t2,4) - 320*Power(t2,5) + 47*Power(t2,6))) + 
            Power(s1,6)*(2 + 15*Power(s2,3) + 7*Power(t1,3) + 
               Power(s2,2)*(1 + 83*t1 - 34*t2) + 4*t2 - 2*Power(t2,2) + 
               Power(t1,2)*(-19 + 45*t2) + 
               t1*(-24 + 49*t2 - 12*Power(t2,2)) - 
               s2*(27 + 44*Power(t1,2) - 7*t2 + Power(t2,2) + 
                  2*t1*(8 + 37*t2))) + 
            Power(s1,5)*(21 + 12*Power(s2,4) + 
               Power(s2,3)*(-19 + 130*t1 - 64*t2) + 2*t2 - 
               39*Power(t2,2) + 12*Power(t2,3) - 
               3*Power(t1,3)*(9 + 10*t2) + 
               Power(t1,2)*(4 + 69*t2 - 90*Power(t2,2)) + 
               Power(s2,2)*(-172 - 38*Power(t1,2) + t1*(23 - 397*t2) + 
                  27*t2 + 74*Power(t2,2)) + 
               2*t1*(8 + 82*t2 - 93*Power(t2,2) + 15*Power(t2,3)) + 
               s2*(33 + 18*Power(t1,3) + 150*t2 - 19*Power(t2,2) + 
                  6*Power(t2,3) + 5*Power(t1,2)*(-1 + 37*t2) + 
                  t1*(-243 + 220*t2 + 145*Power(t2,2)))) + 
            Power(s1,4)*(-34 + 3*Power(s2,5) + 30*Power(t1,4) + 
               Power(s2,4)*(-49 + 81*t1 - 30*t2) - 116*t2 - 
               52*Power(t2,2) + 135*Power(t2,3) - 30*Power(t2,4) + 
               Power(t1,3)*(-83 + 75*t2 + 50*Power(t2,2)) + 
               Power(s2,3)*(-122 + Power(t1,2) + t1*(51 - 521*t2) + 
                  113*t2 + 89*Power(t2,2)) + 
               Power(t1,2)*(-4 + 117*t2 - 84*Power(t2,2) + 
                  90*Power(t2,3)) + 
               t1*(187 - 165*t2 - 429*Power(t2,2) + 335*Power(t2,3) - 
                  40*Power(t2,4)) + 
               Power(s2,2)*(-30 + 35*Power(t1,3) + 821*t2 - 
                  203*Power(t2,2) - 70*Power(t2,3) + 
                  Power(t1,2)*(124 + 77*t2) + 
                  t1*(-626 + 254*t2 + 778*Power(t2,2))) - 
               s2*(-485 + 304*t2 + 399*Power(t2,2) - 60*Power(t2,3) + 
                  15*Power(t2,4) + Power(t1,3)*(76 + 60*t2) + 
                  Power(t1,2)*(147 + 20*t2 + 299*Power(t2,2)) + 
                  t1*(42 - 1226*t2 + 767*Power(t2,2) + 140*Power(t2,3)))) \
+ Power(s1,3)*(-328 + Power(t1,4)*(8 - 72*t2) + 250*t2 + 
               310*Power(t2,2) + 148*Power(t2,3) - 230*Power(t2,4) + 
               40*Power(t2,5) + Power(s2,5)*(-45 + 21*t1 + 2*t2) + 
               Power(s2,4)*(37 + Power(t1,2) + t1*(86 - 243*t2) + 
                  103*t2 - 4*Power(t2,2)) - 
               2*Power(t1,3)*
                (22 - 66*t2 + 31*Power(t2,2) + 20*Power(t2,3)) + 
               Power(t1,2)*(329 + 307*t2 - 402*Power(t2,2) + 
                  26*Power(t2,3) - 45*Power(t2,4)) + 
               2*t1*(56 - 512*t2 + 185*Power(t2,2) + 278*Power(t2,3) - 
                  160*Power(t2,4) + 15*Power(t2,5)) + 
               Power(s2,3)*(-107 + 36*Power(t1,3) + 
                  Power(t1,2)*(22 - 91*t2) + 400*t2 - 351*Power(t2,2) - 
                  16*Power(t2,3) + t1*(-551 + 312*t2 + 822*Power(t2,2))) \
+ Power(s2,2)*(578 + Power(t1,3)*(37 - 100*t2) - 362*t2 - 
                  1585*Power(t2,2) + 522*Power(t2,3) + 10*Power(t2,4) + 
                  Power(t1,2)*(-157 - 450*t2 + 27*Power(t2,2)) + 
                  t1*(49 + 2455*t2 - 1108*Power(t2,2) - 802*Power(t2,3))\
) + s2*(-8 + 56*Power(t1,4) - 2014*t2 + 942*Power(t2,2) + 
                  636*Power(t2,3) - 120*Power(t2,4) + 20*Power(t2,5) + 
                  3*Power(t1,3)*(-98 + 35*t2 + 24*Power(t2,2)) + 
                  Power(t1,2)*
                   (-662 + 1255*t2 + 119*Power(t2,2) + 226*Power(t2,3)) \
+ t1*(1168 - 325*t2 - 2540*Power(t2,2) + 1188*Power(t2,3) + 
                     65*Power(t2,4)))) + 
            Power(s1,2)*(57 + 30*Power(t1,5) + 1164*t2 - 
               535*Power(t2,2) - 444*Power(t2,3) - 182*Power(t2,4) + 
               210*Power(t2,5) - 30*Power(t2,6) + 
               2*Power(s2,6)*(-10 + t1 + t2) + 
               2*Power(t1,4)*(-80 - 29*t2 + 27*Power(t2,2)) + 
               Power(t1,3)*(211 + 455*t2 + 6*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(t1,2)*(628 - 1431*t2 - 768*Power(t2,2) + 
                  464*Power(t2,3) + 21*Power(t2,4) + 9*Power(t2,5)) - 
               t1*(752 + 369*t2 - 1894*Power(t2,2) + 280*Power(t2,3) + 
                  374*Power(t2,4) - 159*Power(t2,5) + 12*Power(t2,6)) + 
               Power(s2,5)*(38 - 3*Power(t1,2) + 56*t2 - 
                  25*Power(t2,2) - 7*t1*(-9 + 5*t2)) + 
               Power(s2,4)*(-89 + 12*Power(t1,3) - 98*t2 - 
                  86*Power(t2,2) + 72*Power(t2,3) - 
                  Power(t1,2)*(43 + 48*t2) + 
                  t1*(-221 + 16*t2 + 259*Power(t2,2))) + 
               Power(s2,3)*(411 + Power(t1,3)*(34 - 78*t2) + 81*t2 - 
                  410*Power(t2,2) + 565*Power(t2,3) - 71*Power(t2,4) + 
                  Power(t1,2)*(81 + 10*t2 + 243*Power(t2,2)) + 
                  t1*(89 + 1223*t2 - 1147*Power(t2,2) - 640*Power(t2,3))\
) + Power(s2,2)*(208 + 60*Power(t1,4) - 1417*t2 + 1428*Power(t2,2) + 
                  1549*Power(t2,3) - 633*Power(t2,4) + 34*Power(t2,5) + 
                  9*Power(t1,3)*(-36 - 30*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*
                   (-363 + 1654*t2 + 627*Power(t2,2) - 163*Power(t2,3)) \
+ t1*(220 - 1236*t2 - 3850*Power(t2,2) + 1570*Power(t2,3) + 
                     463*Power(t2,4))) - 
               s2*(1245 - 1103*t2 - 3171*Power(t2,2) + 
                  1332*Power(t2,3) + 609*Power(t2,4) - 127*Power(t2,5) + 
                  15*Power(t2,6) + 6*Power(t1,4)*(-5 + 18*t2) + 
                  Power(t1,3)*
                   (573 - 494*t2 - 72*Power(t2,2) + 36*Power(t2,3)) + 
                  Power(t1,2)*
                   (-1222 - 2414*t2 + 2565*Power(t2,2) + 
                     187*Power(t2,3) + 74*Power(t2,4)) + 
                  t1*(-397 + 4556*t2 - 915*Power(t2,2) - 
                     2694*Power(t2,3) + 922*Power(t2,4) + 10*Power(t2,5))\
)) + s1*(777 - 2*Power(s2,7) - 774*t2 - 1379*Power(t2,2) + 
               456*Power(t2,3) + 317*Power(t2,4) + 106*Power(t2,5) - 
               99*Power(t2,6) + 12*Power(t2,7) - 
               4*Power(t1,5)*(-9 + 7*t2) + 
               Power(s2,6)*(5 + 7*t1 + 26*t2 - 4*Power(t2,2)) - 
               2*Power(t1,4)*(66 - 62*t2 - 37*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,3)*(951 + 148*t2 - 670*Power(t2,2) - 
                  64*Power(t2,3) + 9*Power(t2,4) - 2*Power(t2,5)) - 
               Power(t1,2)*(918 + 2480*t2 - 1646*Power(t2,2) - 
                  631*Power(t2,3) + 210*Power(t2,4) + 15*Power(t2,5)) + 
               2*t1*(-358 + 1491*t2 + 288*Power(t2,2) - 
                  732*Power(t2,3) + 15*Power(t2,4) + 60*Power(t2,5) - 
                  17*Power(t2,6) + Power(t2,7)) - 
               Power(s2,5)*(-15 + 62*t2 - 9*Power(t2,2) - 
                  32*Power(t2,3) + Power(t1,2)*(1 + 4*t2) + 
                  t1*(54 + 88*t2 - 9*Power(t2,2))) + 
               Power(s2,4)*(56 + 249*t2 + 129*Power(t2,2) + 
                  59*Power(t2,3) - 72*Power(t2,4) - 
                  2*Power(t1,3)*(1 + 8*t2) + 
                  Power(t1,2)*(109 + 137*t2 + 89*Power(t2,2)) - 
                  t1*(41 - 204*t2 + 286*Power(t2,2) + 113*Power(t2,3))) + 
               Power(s2,2)*(-772 + Power(t1,4)*(58 - 72*t2) + 37*t2 + 
                  1027*Power(t2,2) - 1650*Power(t2,3) - 
                  767*Power(t2,4) + 371*Power(t2,5) - 26*Power(t2,6) + 
                  Power(t1,3)*
                   (-47 + 532*t2 + 393*Power(t2,2) - 38*Power(t2,3)) + 
                  Power(t1,2)*
                   (325 + 613*t2 - 2638*Power(t2,2) - 400*Power(t2,3) + 
                     127*Power(t2,4)) + 
                  t1*(608 - 850*t2 + 2271*Power(t2,2) + 
                     2839*Power(t2,3) - 947*Power(t2,4) - 145*Power(t2,5)\
)) + Power(s2,3)*(16*Power(t1,4) + 
                  Power(t1,3)*(-160 - 149*t2 + 50*Power(t2,2)) - 
                  Power(t1,2)*
                   (62 - 394*t2 + 55*Power(t2,2) + 217*Power(t2,3)) + 
                  t1*(-24 - 807*t2 - 953*Power(t2,2) + 
                     1154*Power(t2,3) + 248*Power(t2,4)) + 
                  2*(8 - 248*t2 + 113*Power(t2,2) + 54*Power(t2,3) - 
                     217*Power(t2,4) + 32*Power(t2,5))) + 
               s2*(443 + 20*Power(t1,5) + 1684*t2 - 2241*Power(t2,2) - 
                  2240*Power(t2,3) + 889*Power(t2,4) + 318*Power(t2,5) - 
                  67*Power(t2,6) + 6*Power(t2,7) + 
                  4*Power(t1,4)*(-99 - 44*t2 + 15*Power(t2,2)) + 
                  Power(t1,3)*
                   (298 + 1885*t2 - 164*Power(t2,2) - 155*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  Power(t1,2)*
                   (143 - 3896*t2 - 2923*Power(t2,2) + 
                     1953*Power(t2,3) + 122*Power(t2,4) + 5*Power(t2,5)) \
- t1*(556 - 883*t2 - 5786*Power(t2,2) + 687*Power(t2,3) + 
                     1457*Power(t2,4) - 344*Power(t2,5) + Power(t2,6))))))*
       T2(1 - s + s2 - t1,1 - s1 - t1 + t2))/
     ((-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) - 
    (8*(-8*s2 - 4*Power(s2,2) - 25*Power(s2,3) + 5*Power(s2,4) - 
         2*Power(s2,5) + 8*t1 + 44*s2*t1 + 125*Power(s2,2)*t1 + 
         40*Power(s2,3)*t1 - Power(s2,4)*t1 + 4*Power(s2,5)*t1 - 
         40*Power(t1,2) - 235*s2*Power(t1,2) - 
         244*Power(s2,2)*Power(t1,2) - 18*Power(s2,3)*Power(t1,2) - 
         13*Power(s2,4)*Power(t1,2) - 2*Power(s2,5)*Power(t1,2) + 
         135*Power(t1,3) + 392*s2*Power(t1,3) + 
         134*Power(s2,2)*Power(t1,3) + 16*Power(s2,3)*Power(t1,3) + 
         9*Power(s2,4)*Power(t1,3) - 193*Power(t1,4) - 
         212*s2*Power(t1,4) - 16*Power(s2,2)*Power(t1,4) - 
         13*Power(s2,3)*Power(t1,4) + 99*Power(t1,5) + 
         16*s2*Power(t1,5) + 5*Power(s2,2)*Power(t1,5) - 7*Power(t1,6) + 
         3*s2*Power(t1,6) - 2*Power(t1,7) + 
         Power(s1,8)*(Power(s2,2) + 2*s2*t1 - Power(t1,2)) - 32*t2 + 
         14*s2*t2 - 107*Power(s2,2)*t2 - 18*Power(s2,3)*t2 - 
         61*Power(s2,4)*t2 + 8*Power(s2,5)*t2 - 4*Power(s2,6)*t2 + 
         94*t1*t2 + 226*s2*t1*t2 + 296*Power(s2,2)*t1*t2 + 
         270*Power(s2,3)*t1*t2 + 57*Power(s2,4)*t1*t2 + 
         10*Power(s2,5)*t1*t2 + 4*Power(s2,6)*t1*t2 - 
         251*Power(t1,2)*t2 - 582*s2*Power(t1,2)*t2 - 
         520*Power(s2,2)*Power(t1,2)*t2 - 
         293*Power(s2,3)*Power(t1,2)*t2 - 21*Power(s2,4)*Power(t1,2)*t2 - 
         18*Power(s2,5)*Power(t1,2)*t2 + 372*Power(t1,3)*t2 + 
         470*s2*Power(t1,3)*t2 + 375*Power(s2,2)*Power(t1,3)*t2 + 
         48*Power(s2,3)*Power(t1,3)*t2 + 25*Power(s2,4)*Power(t1,3)*t2 - 
         171*Power(t1,4)*t2 - 117*s2*Power(t1,4)*t2 - 
         35*Power(s2,2)*Power(t1,4)*t2 - 7*Power(s2,3)*Power(t1,4)*t2 - 
         30*Power(t1,5)*t2 - 16*s2*Power(t1,5)*t2 - 
         9*Power(s2,2)*Power(t1,5)*t2 + 18*Power(t1,6)*t2 + 
         5*s2*Power(t1,6)*t2 + 18*Power(t2,2) - 171*s2*Power(t2,2) - 
         10*Power(s2,2)*Power(t2,2) - 199*Power(s2,3)*Power(t2,2) - 
         2*Power(s2,4)*Power(t2,2) - 44*Power(s2,5)*Power(t2,2) + 
         3*Power(s2,6)*Power(t2,2) - 2*Power(s2,7)*Power(t2,2) + 
         61*t1*Power(t2,2) + 422*s2*t1*Power(t2,2) + 
         457*Power(s2,2)*t1*Power(t2,2) + 
         225*Power(s2,3)*t1*Power(t2,2) + 
         144*Power(s2,4)*t1*Power(t2,2) + 13*Power(s2,5)*t1*Power(t2,2) + 
         9*Power(s2,6)*t1*Power(t2,2) - 218*Power(t1,2)*Power(t2,2) - 
         465*s2*Power(t1,2)*Power(t2,2) - 
         434*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         129*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         49*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         11*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         77*Power(t1,3)*Power(t2,2) + 115*s2*Power(t1,3)*Power(t2,2) - 
         53*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         8*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         Power(s2,4)*Power(t1,3)*Power(t2,2) + 
         124*Power(t1,4)*Power(t2,2) + 140*s2*Power(t1,4)*Power(t2,2) + 
         64*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         9*Power(s2,3)*Power(t1,4)*Power(t2,2) - 
         58*Power(t1,5)*Power(t2,2) - 39*s2*Power(t1,5)*Power(t2,2) - 
         4*Power(s2,2)*Power(t1,5)*Power(t2,2) - 21*Power(t2,3) - 
         6*s2*Power(t2,3) - 233*Power(s2,2)*Power(t2,3) + 
         24*Power(s2,3)*Power(t2,3) - 102*Power(s2,4)*Power(t2,3) + 
         9*Power(s2,5)*Power(t2,3) - 8*Power(s2,6)*Power(t2,3) + 
         94*t1*Power(t2,3) + 116*s2*t1*Power(t2,3) + 
         161*Power(s2,2)*t1*Power(t2,3) + 
         137*Power(s2,3)*t1*Power(t2,3) - 24*Power(s2,4)*t1*Power(t2,3) + 
         16*Power(s2,5)*t1*Power(t2,3) - Power(s2,6)*t1*Power(t2,3) - 
         9*Power(t1,2)*Power(t2,3) + 61*s2*Power(t1,2)*Power(t2,3) + 
         134*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         102*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         17*Power(s2,4)*Power(t1,2)*Power(t2,3) + 
         3*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         170*Power(t1,3)*Power(t2,3) - 275*s2*Power(t1,3)*Power(t2,3) - 
         194*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         50*Power(s2,3)*Power(t1,3)*Power(t2,3) - 
         3*Power(s2,4)*Power(t1,3)*Power(t2,3) + 
         89*Power(t1,4)*Power(t2,3) + 106*s2*Power(t1,4)*Power(t2,3) + 
         25*Power(s2,2)*Power(t1,4)*Power(t2,3) + 
         Power(s2,3)*Power(t1,4)*Power(t2,3) + Power(t1,5)*Power(t2,3) - 
         23*Power(t2,4) - 21*s2*Power(t2,4) + 
         40*Power(s2,2)*Power(t2,4) - 75*Power(s2,3)*Power(t2,4) + 
         38*Power(s2,4)*Power(t2,4) - 2*Power(s2,5)*Power(t2,4) + 
         Power(s2,6)*Power(t2,4) - 8*t1*Power(t2,4) - 
         65*s2*t1*Power(t2,4) - 102*Power(s2,2)*t1*Power(t2,4) - 
         142*Power(s2,3)*t1*Power(t2,4) - 35*Power(s2,4)*t1*Power(t2,4) - 
         6*Power(s2,5)*t1*Power(t2,4) + 127*Power(t1,2)*Power(t2,4) + 
         214*s2*Power(t1,2)*Power(t2,4) + 
         252*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         92*Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         9*Power(s2,4)*Power(t1,2)*Power(t2,4) - 
         72*Power(t1,3)*Power(t2,4) - 150*s2*Power(t1,3)*Power(t2,4) - 
         54*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         4*Power(s2,3)*Power(t1,3)*Power(t2,4) - 
         Power(t1,4)*Power(t2,4) - s2*Power(t1,4)*Power(t2,4) + 
         8*Power(t2,5) + 6*s2*Power(t2,5) + 27*Power(s2,2)*Power(t2,5) + 
         51*Power(s2,3)*Power(t2,5) + 17*Power(s2,4)*Power(t2,5) + 
         3*Power(s2,5)*Power(t2,5) - 52*t1*Power(t2,5) - 
         89*s2*t1*Power(t2,5) - 133*Power(s2,2)*t1*Power(t2,5) - 
         72*Power(s2,3)*t1*Power(t2,5) - 9*Power(s2,4)*t1*Power(t2,5) + 
         34*Power(t1,2)*Power(t2,5) + 117*s2*Power(t1,2)*Power(t2,5) + 
         58*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
         6*Power(s2,3)*Power(t1,2)*Power(t2,5) - 
         2*Power(t1,3)*Power(t2,5) + 2*s2*Power(t1,3)*Power(t2,5) + 
         8*Power(t2,6) + 23*s2*Power(t2,6) + 20*Power(s2,2)*Power(t2,6) + 
         21*Power(s2,3)*Power(t2,6) + 3*Power(s2,4)*Power(t2,6) - 
         12*t1*Power(t2,6) - 43*s2*t1*Power(t2,6) - 
         34*Power(s2,2)*t1*Power(t2,6) - 4*Power(s2,3)*t1*Power(t2,6) + 
         2*Power(t1,2)*Power(t2,6) + 3*Power(t2,7) + 4*s2*Power(t2,7) + 
         9*Power(s2,2)*Power(t2,7) + Power(s2,3)*Power(t2,7) + 
         t1*Power(t2,7) - 2*s2*t1*Power(t2,7) - Power(t2,8) + 
         s2*Power(t2,8) + Power(s1,7)*
          (-3*Power(s2,3) + Power(s2,2)*(1 + t1 - 7*t2) - 
            2*t1*(1 + Power(t1,2) - t2 - 3*t1*t2) + 
            s2*(6*Power(t1,2) - 2*(1 + t2) - t1*(3 + 13*t2))) + 
         Power(s1,6)*(1 + 3*Power(s2,4) + 2*t2 + 10*Power(t1,3)*t2 - 
            Power(t2,2) + Power(s2,3)*(5 - 11*t1 + 19*t2) + 
            t1*(3 + 13*t2 - 12*Power(t2,2)) - 
            3*Power(t1,2)*(2 - t2 + 5*Power(t2,2)) + 
            Power(s2,2)*(-2 + 8*Power(t1,2) - 8*t1*(-1 + t2) + 5*t2 + 
               21*Power(t2,2)) + 
            s2*(-2 + 17*t2 + 13*Power(t2,2) - 
               2*Power(t1,2)*(9 + 16*t2) + 
               t1*(-5 + 11*t2 + 36*Power(t2,2)))) + 
         2*Power(s,5)*(-2 + Power(t1,5) + Power(s1,3)*Power(t1 - t2,2) - 
            5*Power(t1,4)*t2 + 10*Power(t1,3)*Power(t2,2) - 
            10*Power(t1,2)*Power(t2,3) + 5*t1*Power(t2,4) - 
            Power(t2,5) - Power(s2,3)*
             (-2 + Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
            3*Power(s2,2)*(-2 + Power(t1,3) - 3*Power(t1,2)*t2 + 
               3*t1*Power(t2,2) - Power(t2,3)) - 
            3*s2*(-2 + Power(t1,4) - 4*Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s1,2)*(-2 - 6*Power(s2,2) + 2*Power(s2,3) + 
               3*Power(t1,3) - 9*Power(t1,2)*t2 + 9*t1*Power(t2,2) - 
               3*Power(t2,3) - 
               3*s2*(-2 + Power(t1,2) - 2*t1*t2 + Power(t2,2))) + 
            s1*(4 - 4*Power(s2,3) + 3*Power(t1,4) - 12*Power(t1,3)*t2 + 
               18*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
               3*Power(t2,4) + 
               3*Power(s2,2)*(4 + Power(t1,2) - 2*t1*t2 + Power(t2,2)) - 
               6*s2*(2 + Power(t1,3) - 3*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - Power(t2,3)))) - 
         Power(s1,5)*(-1 + 3*Power(s2,5) - 2*Power(t1,5) + 10*t2 + 
            13*Power(t2,2) - 6*Power(t2,3) + 
            Power(s2,4)*(15 - 14*t1 + 18*t2) + 
            Power(t1,3)*(15 + 2*t2 + 20*Power(t2,2)) + 
            t1*(-4 + 11*t2 + 36*Power(t2,2) - 30*Power(t2,3)) - 
            5*Power(t1,2)*(4 + 7*t2 - 3*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s2,3)*(-31 + 25*Power(t1,2) + 58*t2 + 
               51*Power(t2,2) - t1*(16 + 59*t2)) + 
            Power(s2,2)*(-7 - 22*Power(t1,3) + 17*t2 + 49*Power(t2,2) + 
               35*Power(t2,3) + Power(t1,2)*(-33 + 40*t2) + 
               t1*(13 - 9*t2 - 25*Power(t2,2))) + 
            s2*(-13 + 39*Power(t1,3) + 10*Power(t1,4) + 7*t2 + 
               59*Power(t2,2) + 36*Power(t2,3) + 
               Power(t1,2)*(27 - 91*t2 - 70*Power(t2,2)) + 
               t1*(-4 - 61*t2 + 8*Power(t2,2) + 55*Power(t2,3)))) + 
         Power(s1,4)*(-8 - 2*Power(s2,6) + Power(t1,6) + 2*t2 - 
            6*Power(t1,5)*t2 + 38*Power(t2,2) + 35*Power(t2,3) - 
            15*Power(t2,4) - Power(t1,4)*(31 + 4*t2) + 
            Power(s2,5)*(-3 + 5*t1 + 11*t2) + 
            Power(t1,3)*(33 + 67*t2 + 8*Power(t2,2) + 20*Power(t2,3)) + 
            t1*(-12 - 57*t2 + 2*Power(t2,2) + 55*Power(t2,3) - 
               40*Power(t2,4)) + 
            Power(t1,2)*(40 - 62*t2 - 78*Power(t2,2) + 30*Power(t2,3) - 
               15*Power(t2,4)) + 
            Power(s2,4)*(1 + 73*t2 + 45*Power(t2,2) - t1*(5 + 53*t2)) - 
            Power(s2,3)*(41 + 11*Power(t1,3) + 
               Power(t1,2)*(11 - 92*t2) + 72*t2 - 203*Power(t2,2) - 
               75*Power(t2,3) + t1*(-12 + 147*t2 + 130*Power(t2,2))) + 
            Power(s2,2)*(-93 + 13*Power(t1,4) + 
               Power(t1,3)*(58 - 76*t2) + 19*t2 + 108*Power(t2,2) + 
               135*Power(t2,3) + 35*Power(t2,4) + 
               Power(t1,2)*(42 - 56*t2 + 80*Power(t2,2)) - 
               t1*(-71 + 111*t2 + 150*Power(t2,2) + 40*Power(t2,3))) - 
            s2*(29 + 6*Power(t1,5) + Power(t1,3)*(44 - 163*t2) + 
               Power(t1,4)*(39 - 32*t2) + 43*t2 - 71*Power(t2,2) - 
               110*Power(t2,3) - 55*Power(t2,4) + 
               Power(t1,2)*(38 - 235*t2 + 184*Power(t2,2) + 
                  80*Power(t2,3)) + 
               t1*(-109 + 115*t2 + 237*Power(t2,2) + 20*Power(t2,3) - 
                  50*Power(t2,4)))) + 
         Power(s1,3)*(17 + 41*t2 - 2*Power(t1,6)*t2 - 20*Power(t2,2) - 
            72*Power(t2,3) - 50*Power(t2,4) + 20*Power(t2,5) + 
            Power(s2,6)*(2 - t1 + 5*t2) + 
            Power(t1,5)*(-33 + 6*Power(t2,2)) + 
            Power(t1,4)*(7 + 94*t2 + 12*Power(t2,2)) - 
            Power(t1,3)*(-83 + 23*t2 + 109*Power(t2,2) + 
               12*Power(t2,3) + 10*Power(t2,4)) + 
            Power(t1,2)*(27 - 266*t2 + 32*Power(t2,2) + 
               82*Power(t2,3) - 30*Power(t2,4) + 6*Power(t2,5)) + 
            t1*(-85 + 64*t2 + 199*Power(t2,2) + 42*Power(t2,3) - 
               50*Power(t2,4) + 30*Power(t2,5)) + 
            Power(s2,5)*(11 + 4*Power(t1,2) + 6*t2 - 18*Power(t2,2) - 
               t1*(11 + 12*t2)) + 
            Power(s2,4)*(89 + 30*Power(t1,2) - 6*Power(t1,3) - 31*t2 - 
               146*Power(t2,2) - 60*Power(t2,3) + 
               t1*(-73 + 55*t2 + 84*Power(t2,2))) + 
            Power(s2,3)*(-15 + 4*Power(t1,4) + 228*t2 - 
               21*Power(t2,2) - 332*Power(t2,3) - 65*Power(t2,4) + 
               Power(t1,3)*(-53 + 25*t2) - 
               2*Power(t1,2)*(-39 + 31*t2 + 66*Power(t2,2)) + 
               t1*(-156 + 67*t2 + 417*Power(t2,2) + 150*Power(t2,3))) - 
            Power(s2,2)*(-159 + Power(t1,5) - 260*t2 + 
               126*Power(t2,2) + 222*Power(t2,3) + 185*Power(t2,4) + 
               21*Power(t2,5) + Power(t1,4)*(-50 + 29*t2) + 
               Power(t1,3)*(-16 + 109*t2 - 96*Power(t2,2)) + 
               Power(t1,2)*(-135 + 372*t2 + 88*Power(t2,2) + 
                  80*Power(t2,3)) + 
               t1*(268 + 151*t2 - 544*Power(t2,2) - 350*Power(t2,3) - 
                  35*Power(t2,4))) + 
            s2*(81 + 86*t2 + 45*Power(t2,2) - 174*Power(t2,3) - 
               120*Power(t2,4) - 50*Power(t2,5) + 
               Power(t1,5)*(-18 + 13*t2) + 
               Power(t1,3)*(-90 + 313*t2 - 257*Power(t2,2)) + 
               Power(t1,4)*(1 + 110*t2 - 36*Power(t2,2)) + 
               2*Power(t1,2)*
                (132 - 62*t2 - 330*Power(t2,2) + 93*Power(t2,3) + 
                  25*Power(t2,4)) + 
               t1*(-248 - 234*t2 + 410*Power(t2,2) + 438*Power(t2,3) + 
                  45*Power(t2,4) - 27*Power(t2,5)))) - 
         Power(s1,2)*(22 + 2*Power(s2,7) + 47*t2 + 81*Power(t2,2) - 
            38*Power(t2,3) - 73*Power(t2,4) - 40*Power(t2,5) + 
            15*Power(t2,6) - Power(t1,6)*(-15 + t2 + Power(t2,2)) + 
            2*Power(t1,5)*(6 - 31*t2 + Power(t2,3)) + 
            2*Power(t1,4)*(-17 - 47*t2 + 48*Power(t2,2) + 
               6*Power(t2,3)) - 
            Power(t1,3)*(199 - 369*t2 - 125*Power(t2,2) + 
               75*Power(t2,3) + 8*Power(t2,4) + 2*Power(t2,5)) + 
            Power(t1,2)*(341 + 31*t2 - 539*Power(t2,2) - 
               76*Power(t2,3) + 38*Power(t2,4) - 15*Power(t2,5) + 
               Power(t2,6)) + 
            t1*(-161 - 242*t2 + 100*Power(t2,2) + 295*Power(t2,3) + 
               73*Power(t2,4) - 27*Power(t2,5) + 12*Power(t2,6)) - 
            Power(s2,6)*(5 - 12*t2 - 3*Power(t2,2) + t1*(7 + t2)) + 
            Power(s2,5)*(29 + 9*t2 + 5*Power(t2,2) - 18*Power(t2,3) + 
               Power(t1,2)*(6 + 5*t2) + t1*(7 - 34*t2 - 3*Power(t2,2))) \
+ Power(s2,4)*(20 + Power(t1,3)*(1 - 9*t2) + 271*t2 - 97*Power(t2,2) - 
               150*Power(t2,3) - 45*Power(t2,4) + 
               Power(t1,2)*(15 + 30*t2 - 9*Power(t2,2)) + 
               2*t1*(-64 - 50*t2 + 65*Power(t2,2) + 37*Power(t2,3))) + 
            Power(s2,3)*(177 - 23*t2 + 408*Power(t2,2) - 
               185*Power(t2,3) - 283*Power(t2,4) - 33*Power(t2,5) + 
               Power(t1,4)*(1 + 7*t2) + 
               Power(t1,3)*(-34 - 36*t2 + 21*Power(t2,2)) + 
               Power(t1,2)*(269 + 33*t2 - 249*Power(t2,2) - 
                  94*Power(t2,3)) + 
               t1*(-327 - 479*t2 + 312*Power(t2,2) + 529*Power(t2,3) + 
                  95*Power(t2,4))) + 
            Power(s2,2)*(-39 + 591*t2 + 201*Power(t2,2) - 
               194*Power(t2,3) - 218*Power(t2,4) - 139*Power(t2,5) - 
               7*Power(t2,6) - 2*Power(t1,5)*(3 + t2) + 
               Power(t1,4)*(35 + 57*t2 - 19*Power(t2,2)) + 
               Power(t1,3)*(-285 + 229*t2 + 10*Power(t2,2) + 
                  52*Power(t2,3)) + 
               Power(t1,2)*(630 + 244*t2 - 870*Power(t2,2) - 
                  270*Power(t2,3) - 40*Power(t2,4)) + 
               t1*(-355 - 830*t2 + 13*Power(t2,2) + 836*Power(t2,3) + 
                  360*Power(t2,4) + 16*Power(t2,5))) + 
            s2*(135 + 3*Power(t1,6) + 174*t2 + 106*Power(t2,2) + 
               7*Power(t2,3) - 196*Power(t2,4) - 77*Power(t2,5) - 
               27*Power(t2,6) + 
               Power(t1,5)*(-33 - 28*t2 + 8*Power(t2,2)) + 
               Power(t1,3)*(-301 + 41*t2 + 644*Power(t2,2) - 
                  183*Power(t2,3)) + 
               Power(t1,4)*(103 - 109*t2 + 104*Power(t2,2) - 
                  16*Power(t2,3)) + 
               Power(t1,2)*(419 + 556*t2 - 576*Power(t2,2) - 
                  840*Power(t2,3) + 94*Power(t2,4) + 16*Power(t2,5)) + 
               t1*(-328 - 656*t2 - 76*Power(t2,2) + 580*Power(t2,3) + 
                  427*Power(t2,4) + 37*Power(t2,5) - 8*Power(t2,6)))) + 
         s1*(32 - 2*Power(t1,7) + 4*t2 + 4*Power(s2,7)*t2 + 
            51*Power(t2,2) + 71*Power(t2,3) - 29*Power(t2,4) - 
            38*Power(t2,5) - 17*Power(t2,6) + 6*Power(t2,7) + 
            Power(t1,5)*(-43 + 72*t2 - 30*Power(t2,2)) - 
            Power(t1,6)*(1 - 13*t2 + Power(t2,2)) + 
            2*Power(t1,4)*(137 - 66*t2 - 95*Power(t2,2) + 
               17*Power(t2,3) + 2*Power(t2,4)) - 
            Power(t1,3)*(409 + 342*t2 - 456*Power(t2,2) - 
               187*Power(t2,3) + 16*Power(t2,4) + 2*Power(t2,5)) + 
            Power(t1,2)*(235 + 615*t2 + 13*Power(t2,2) - 
               440*Power(t2,3) - 100*Power(t2,4) + 3*Power(t2,5) - 
               3*Power(t2,6)) + 
            t1*(-86 - 238*t2 - 251*Power(t2,2) + 56*Power(t2,3) + 
               201*Power(t2,4) + 49*Power(t2,5) - 8*Power(t2,6) + 
               2*Power(t2,7)) + 
            Power(s2,6)*(4 - 8*t2 + 18*Power(t2,2) - Power(t2,3) + 
               t1*(-4 - 16*t2 + Power(t2,2))) - 
            Power(s2,5)*(10 - 73*t2 + 11*Power(t2,2) - 4*Power(t2,3) + 
               11*Power(t2,4) + 
               Power(t1,2)*(-16 - 17*t2 + 2*Power(t2,2)) + 
               t1*(6 + 6*t2 + 39*Power(t2,2) - 10*Power(t2,3))) + 
            Power(s2,4)*(52 + 2*Power(t1,3)*(-9 + t2) + 22*t2 + 
               284*Power(t2,2) - 105*Power(t2,3) - 79*Power(t2,4) - 
               18*Power(t2,5) - 
               Power(t1,2)*(2 - 64*t2 + 17*Power(t2,2) + 
                  18*Power(t2,3)) + 
               t1*(-32 - 272*t2 - 3*Power(t2,2) + 115*Power(t2,3) + 
                  38*Power(t2,4))) + 
            Power(s2,3)*(15 + 372*t2 + 2*Power(t1,4)*(-4 + t2)*t2 - 
               32*Power(t2,2) + 296*Power(t2,3) - 174*Power(t2,4) - 
               122*Power(t2,5) - 9*Power(t2,6) + 
               Power(t1,3)*(-12 - 42*t2 + 67*Power(t2,2) + 
                  11*Power(t2,3)) + 
               Power(t1,2)*(239 + 394*t2 - 147*Power(t2,2) - 
                  268*Power(t2,3) - 35*Power(t2,4)) + 
               t1*(-242 - 544*t2 - 460*Power(t2,2) + 375*Power(t2,3) + 
                  315*Power(t2,4) + 31*Power(t2,5))) - 
            Power(s2,2)*(-99 + 17*t2 - 665*Power(t2,2) + 
               6*Power(t2,3) + 121*Power(t2,4) + 105*Power(t2,5) + 
               55*Power(t2,6) + Power(t2,7) + 
               Power(t1,5)*(-8 + 2*t2 + Power(t2,2)) + 
               Power(t1,4)*(-33 + 31*t2 + 18*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,3)*(391 + 226*t2 - 407*Power(t2,2) - 
                  115*Power(t2,3) - 10*Power(t2,4)) + 
               Power(t1,2)*(-554 - 1070*t2 + 25*Power(t2,2) + 
                  792*Power(t2,3) + 217*Power(t2,4) + 8*Power(t2,5)) - 
               3*t1*(-101 - 278*t2 - 241*Power(t2,2) + 65*Power(t2,3) + 
                  183*Power(t2,4) + 59*Power(t2,5) + Power(t2,6))) + 
            s2*(-22 + 322*t2 + 3*Power(t1,6)*t2 + 99*Power(t2,2) + 
               70*Power(t2,3) - 14*Power(t2,4) - 107*Power(t2,5) - 
               27*Power(t2,6) - 8*Power(t2,7) + 
               Power(t1,5)*(-16 + 10*t2 - 10*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,3)*(-626 - 456*t2 + 406*Power(t2,2) + 
                  525*Power(t2,3) - 52*Power(t2,4)) + 
               Power(t1,4)*(237 - 41*t2 - 216*Power(t2,2) + 
                  34*Power(t2,3) - 2*Power(t2,4)) + 
               Power(t1,2)*(629 + 976*t2 + 231*Power(t2,2) - 
                  628*Power(t2,3) - 505*Power(t2,4) + 19*Power(t2,5) + 
                  2*Power(t2,6)) - 
               t1*(202 + 818*t2 + 524*Power(t2,2) - 114*Power(t2,3) - 
                  370*Power(t2,4) - 213*Power(t2,5) - 14*Power(t2,6) + 
                  Power(t2,7)))) + 
         Power(s,4)*(32 - 22*Power(t1,2) + Power(t1,4) - 7*Power(t1,5) + 
            Power(t1,6) - 20*t2 + 40*t1*t2 + 37*Power(t1,4)*t2 - 
            18*Power(t2,2) - 6*Power(t1,2)*Power(t2,2) - 
            78*Power(t1,3)*Power(t2,2) - 15*Power(t1,4)*Power(t2,2) + 
            8*t1*Power(t2,3) + 82*Power(t1,2)*Power(t2,3) + 
            40*Power(t1,3)*Power(t2,3) - 3*Power(t2,4) - 
            43*t1*Power(t2,4) - 45*Power(t1,2)*Power(t2,4) + 
            9*Power(t2,5) + 24*t1*Power(t2,5) - 5*Power(t2,6) + 
            Power(s2,4)*(4 + 2*Power(t1,2) + 5*t2 + 4*Power(t2,2) - 
               t1*(5 + 6*t2)) + 
            Power(s1,4)*(-9*Power(t1,2) + (3 + s2 - 5*t2)*t2 + 
               t1*(-7 + 3*s2 + 14*t2)) + 
            Power(s2,3)*(-40 - 7*Power(t1,3) + 7*t2 + 28*Power(t2,2) + 
               7*Power(t2,3) + 3*Power(t1,2)*(6 + 7*t2) + 
               t1*(21 - 46*t2 - 21*Power(t2,2))) + 
            Power(s2,2)*(100 + 9*Power(t1,4) - 34*t2 - 23*Power(t2,2) + 
               48*Power(t2,3) - 3*Power(t2,4) - 
               6*Power(t1,3)*(5 + 4*t2) + 
               Power(t1,2)*(-23 + 108*t2 + 18*Power(t2,2)) - 
               2*t1*(19 - 23*t2 + 63*Power(t2,2))) + 
            s2*(-96 - 5*Power(t1,5) + 46*t2 + 34*Power(t2,2) - 
               9*Power(t2,3) + 34*Power(t2,4) - 11*Power(t2,5) + 
               3*Power(t1,4)*(8 + 3*t2) + 
               Power(t1,3)*(-3 - 106*t2 + 14*Power(t2,2)) + 
               Power(t1,2)*(46 - 3*t2 + 174*Power(t2,2) - 
                  46*Power(t2,3)) + 
               t1*(18 - 80*t2 + 15*Power(t2,2) - 126*Power(t2,3) + 
                  39*Power(t2,4))) + 
            Power(s1,3)*(8 - 12*Power(s2,3) - 26*Power(t1,3) + 
               Power(s2,2)*(32 - 9*t1 - 3*t2) + 3*t2 - 18*Power(t2,2) + 
               20*Power(t2,3) + 4*Power(t1,2)*(-7 + 18*t2) + 
               t1*(1 + 46*t2 - 66*Power(t2,2)) + 
               2*s2*(-14 + 17*Power(t1,2) - 8*t2 + 4*Power(t2,2) - 
                  3*t1*(-4 + 7*t2))) + 
            Power(s1,2)*(4 - 8*Power(s2,4) - 24*Power(t1,4) - 26*t2 - 
               9*Power(t2,2) + 36*Power(t2,3) - 30*Power(t2,4) + 
               Power(s2,3)*(32 + 15*t1 + 13*t2) + 
               6*Power(t1,3)*(-7 + 17*t2) + 
               Power(t1,2)*(3 + 120*t2 - 162*Power(t2,2)) + 
               2*t1*(5 + 3*t2 - 57*Power(t2,2) + 57*Power(t2,3)) - 
               Power(s2,2)*(36 + 39*Power(t1,2) + 16*t2 - 
                  3*Power(t2,2) - 4*t1*(-8 + 9*t2)) + 
               s2*(8 + 54*Power(t1,3) + Power(t1,2)*(72 - 138*t2) + 
                  53*t2 + 66*Power(t2,2) - 30*Power(t2,3) + 
                  t1*(-17 - 138*t2 + 114*Power(t2,2)))) + 
            s1*(-44 - 6*Power(t1,5) + 46*t2 + 18*Power(t2,2) + 
               9*Power(t2,3) - 30*Power(t2,4) + 20*Power(t2,5) + 
               Power(s2,4)*(4 - t1 + t2) + 4*Power(t1,4)*(-7 + 11*t2) + 
               Power(t1,3)*(3 + 114*t2 - 116*Power(t2,2)) + 
               Power(t1,2)*(22 + 3*t2 - 174*Power(t2,2) + 
                  144*Power(t2,3)) - 
               t1*(10 + 40*t2 + 15*Power(t2,2) - 118*Power(t2,3) + 
                  86*Power(t2,4)) + 
               2*Power(s2,3)*
                (10 + 5*Power(t1,2) - 22*t2 - 4*Power(t2,2) - 
                  t1*(6 + t2)) + 
               Power(s2,2)*(-96 - 21*Power(t1,3) + 89*t2 - 
                  64*Power(t2,2) + 3*Power(t2,3) + 
                  Power(t1,2)*(-34 + 45*t2) + 
                  t1*(43 + 98*t2 - 27*Power(t2,2))) + 
               2*s2*(58 + 9*Power(t1,4) + Power(t1,3)*(36 - 43*t2) - 
                  54*t2 - 8*Power(t2,2) - 42*Power(t2,3) + 
                  16*Power(t2,4) + 
                  Power(t1,2)*(-26 - 114*t2 + 75*Power(t2,2)) + 
                  t1*(-2 + 34*t2 + 120*Power(t2,2) - 57*Power(t2,3))))) + 
         Power(s,3)*(-76 + 22*t1 + 130*Power(t1,2) - 71*Power(t1,3) + 
            4*Power(t1,4) - 2*Power(t1,5) - 3*Power(t1,6) + 106*t2 - 
            244*t1*t2 + 103*Power(t1,2)*t2 - 22*Power(t1,3)*t2 + 
            16*Power(t1,4)*t2 + 3*Power(t1,5)*t2 + 2*Power(t1,6)*t2 + 
            74*Power(t2,2) - 9*t1*Power(t2,2) + 
            42*Power(t1,2)*Power(t2,2) - 28*Power(t1,3)*Power(t2,2) + 
            36*Power(t1,4)*Power(t2,2) - 6*Power(t1,5)*Power(t2,2) - 
            23*Power(t2,3) - 34*t1*Power(t2,3) + 
            8*Power(t1,2)*Power(t2,3) - 114*Power(t1,3)*Power(t2,3) + 
            10*Power(t2,4) + 14*t1*Power(t2,4) + 
            141*Power(t1,2)*Power(t2,4) + 20*Power(t1,3)*Power(t2,4) - 
            8*Power(t2,5) - 81*t1*Power(t2,5) - 
            30*Power(t1,2)*Power(t2,5) + 18*Power(t2,6) + 
            18*t1*Power(t2,6) - 4*Power(t2,7) - 
            Power(s2,5)*(10 + Power(t1,2) + 9*t2 + 3*Power(t2,2) - 
               2*t1*(3 + 2*t2)) - 
            Power(s1,5)*(Power(s2,2) - 16*Power(t1,2) + 
               18*t1*(-1 + t2) + (5 - 4*t2)*t2 + s2*(-3 + 11*t1 + 2*t2)) \
+ Power(s2,4)*(39 + 3*Power(t1,3) - 10*t2 - 42*Power(t2,2) - 
               Power(t1,2)*(29 + 12*t2) + 
               t1*(11 + 69*t2 + 9*Power(t2,2))) + 
            Power(s2,3)*(-3 - 3*Power(t1,4) - 43*t2 - 40*Power(t2,2) - 
               34*Power(t2,3) + 14*Power(t2,4) + 
               7*Power(t1,3)*(8 + t2) + 
               Power(t1,2)*(13 - 178*t2 + 9*Power(t2,2)) + 
               t1*(-109 + 99*t2 + 156*Power(t2,2) - 27*Power(t2,3))) + 
            Power(s2,2)*(-158 + Power(t1,5) + 139*t2 + 27*Power(t2,2) - 
               104*Power(t2,3) + 32*Power(t2,4) + 12*Power(t2,5) + 
               Power(t1,4)*(-52 + 8*t2) + 
               Power(t1,3)*(-7 + 184*t2 - 42*Power(t2,2)) + 
               Power(t1,2)*(-27 - 90*t2 - 180*Power(t2,2) + 
                  68*Power(t2,3)) + 
               t1*(287 - 168*t2 + 201*Power(t2,2) + 16*Power(t2,3) - 
                  47*Power(t2,4))) - 
            s2*(-206 + 200*t2 + 69*Power(t2,2) - 111*Power(t2,3) + 
               32*Power(t2,4) - 51*Power(t2,5) + 3*Power(t2,6) + 
               Power(t1,5)*(-22 + 9*t2) + 
               Power(t1,4)*(5 + 69*t2 - 33*Power(t2,2)) + 
               Power(t1,3)*(-93 + t2 - 24*Power(t2,2) + 
                  42*Power(t2,3)) + 
               Power(t1,2)*(137 + 27*t2 + 15*Power(t2,2) - 
                  122*Power(t2,3) - 18*Power(t2,4)) + 
               t1*(200 - 342*t2 + 177*Power(t2,2) - 53*Power(t2,3) + 
                  150*Power(t2,4) - 3*Power(t2,5))) + 
            Power(s1,4)*(-5 + 15*Power(s2,3) + 44*Power(t1,3) + 
               Power(t1,2)*(69 - 114*t2) - 7*t2 + 38*Power(t2,2) - 
               20*Power(t2,3) + Power(s2,2)*(-42 + 26*t1 + 8*t2) + 
               t1*(4 - 125*t2 + 90*Power(t2,2)) + 
               s2*(22 - 72*Power(t1,2) + 10*t2 + 5*Power(t2,2) + 
                  t1*(-41 + 75*t2))) + 
            Power(s1,3)*(-47 + 19*Power(s2,4) + 36*Power(t1,4) + 
               Power(t1,3)*(96 - 172*t2) + 19*t2 + 29*Power(t2,2) - 
               102*Power(t2,3) + 40*Power(t2,4) - 
               Power(s2,3)*(18 + 33*t1 + 29*t2) + 
               Power(s2,2)*(-30 + 73*Power(t1,2) + t1*(17 - 79*t2) + 
                  121*t2 - 30*Power(t2,2)) + 
               2*Power(t1,2)*(5 - 171*t2 + 138*Power(t2,2)) + 
               t1*(13 - 23*t2 + 348*Power(t2,2) - 180*Power(t2,3)) - 
               s2*(-96 + 90*Power(t1,3) + 85*t2 + 99*Power(t2,2) - 
                  7*Power(t1,2)*(-17 + 36*t2) + 
                  t1*(23 - 258*t2 + 162*Power(t2,2)))) + 
            Power(s1,2)*(78 + 7*Power(s2,5) + 4*Power(t1,5) + 
               Power(t1,4)*(54 - 72*t2) + 97*t2 - 13*Power(t2,2) - 
               45*Power(t2,3) + 128*Power(t2,4) - 40*Power(t2,5) - 
               2*Power(s2,4)*(11 + 9*t1 + 10*t2) + 
               Power(t1,3)*(6 - 326*t2 + 232*Power(t2,2)) + 
               Power(t1,2)*(61 - 9*t2 + 618*Power(t2,2) - 
                  304*Power(t2,3)) + 
               t1*(-103 - 72*t2 + 48*Power(t2,2) - 474*Power(t2,3) + 
                  180*Power(t2,4)) + 
               Power(s2,3)*(10*Power(t1,2) + t1*(95 + 27*t2) + 
                  3*(-30 + t2 + 9*Power(t2,2))) + 
               Power(s2,2)*(234 + 5*Power(t1,3) - 121*t2 - 
                  84*Power(t2,2) + 52*Power(t2,3) - 
                  18*Power(t1,2)*(2 + 5*t2) + 
                  t1*(-5 + 36*t2 + 33*Power(t2,2))) + 
               s2*(-227 - 8*Power(t1,4) - 33*t2 + 72*Power(t2,2) + 
                  207*Power(t2,3) - 10*Power(t2,4) + 
                  Power(t1,3)*(-81 + 142*t2) - 
                  3*Power(t1,2)*(23 - 139*t2 + 90*Power(t2,2)) + 
                  t1*(105 + 21*t2 - 543*Power(t2,2) + 146*Power(t2,3)))) \
+ s1*(46 - 4*Power(t1,6) + Power(s2,5)*(7 + 2*t1 - 3*t2) - 228*t2 - 
               27*Power(t2,2) - 11*Power(t2,3) + 31*Power(t2,4) - 
               77*Power(t2,5) + 20*Power(t2,6) + 
               6*Power(t1,5)*(1 + t2) + 
               Power(t1,4)*(-2 - 101*t2 + 36*Power(t2,2)) + 
               Power(t1,3)*(87 + 23*t2 + 344*Power(t2,2) - 
                  124*Power(t2,3)) + 
               Power(t1,2)*(-175 - 169*t2 - 9*Power(t2,2) - 
                  486*Power(t2,3) + 156*Power(t2,4)) + 
               t1*(68 + 266*t2 + 93*Power(t2,2) - 43*Power(t2,3) + 
                  314*Power(t2,4) - 90*Power(t2,5)) + 
               Power(s2,4)*(-56 - 14*Power(t1,2) + 60*t2 + 
                  Power(t2,2) + 3*t1*(-1 + 5*t2)) + 
               Power(s2,3)*(136 + 35*Power(t1,3) + 
                  Power(t1,2)*(1 - 41*t2) + 69*t2 + 49*Power(t2,2) - 
                  27*Power(t2,3) + t1*(7 - 186*t2 + 33*Power(t2,2))) - 
               Power(s2,2)*(43 + 40*Power(t1,4) + 
                  Power(t1,3)*(27 - 65*t2) + 207*t2 - 255*Power(t2,2) + 
                  27*Power(t2,3) + 41*Power(t2,4) + 
                  3*Power(t1,2)*(-62 - 41*t2 + 17*Power(t2,2)) + 
                  t1*(225 + 153*t2 + 69*Power(t2,2) - 67*Power(t2,3))) + 
               s2*(-80 + 21*Power(t1,5) + Power(t1,4)*(16 - 42*t2) + 
                  370*t2 - 174*Power(t2,2) + 23*Power(t2,3) - 
                  172*Power(t2,4) + 10*Power(t2,5) + 
                  Power(t1,3)*(-149 + 100*t2 - 10*Power(t2,2)) + 
                  Power(t1,2)*
                   (110 + 177*t2 - 420*Power(t2,2) + 72*Power(t2,3)) + 
                  t1*(90 - 144*t2 - 51*Power(t2,2) + 476*Power(t2,3) - 
                     51*Power(t2,4))))) + 
         Power(s,2)*(72 - 44*t1 - 222*Power(t1,2) + 312*Power(t1,3) - 
            132*Power(t1,4) + 29*Power(t1,5) - Power(t1,6) - 184*t2 + 
            490*t1*t2 - 490*Power(t1,2)*t2 + 272*Power(t1,3)*t2 - 
            139*Power(t1,4)*t2 + 7*Power(t1,5)*t2 - 4*Power(t1,6)*t2 - 
            76*Power(t2,2) + 92*t1*Power(t2,2) - 
            222*Power(t1,2)*Power(t2,2) + 248*Power(t1,3)*Power(t2,2) - 
            7*Power(t1,4)*Power(t2,2) + 15*Power(t1,5)*Power(t2,2) + 
            Power(t1,6)*Power(t2,2) + 46*Power(t2,3) + 
            132*t1*Power(t2,3) - 200*Power(t1,2)*Power(t2,3) + 
            2*Power(t1,3)*Power(t2,3) - 9*Power(t1,4)*Power(t2,3) - 
            4*Power(t1,5)*Power(t2,3) - 50*Power(t2,4) + 
            67*t1*Power(t2,4) - 19*Power(t1,2)*Power(t2,4) - 
            34*Power(t1,3)*Power(t2,4) + 5*Power(t1,4)*Power(t2,4) - 
            5*Power(t2,5) + 31*t1*Power(t2,5) + 
            66*Power(t1,2)*Power(t2,5) - 13*Power(t2,6) - 
            45*t1*Power(t2,6) - 5*Power(t1,2)*Power(t2,6) + 
            11*Power(t2,7) + 4*t1*Power(t2,7) - Power(t2,8) + 
            Power(s2,6)*(4 + 8*t2 + Power(t2,2) - t1*(2 + t2)) + 
            Power(s1,6)*(3*Power(s2,2) - 14*Power(t1,2) - 
               (-2 + t2)*t2 + s2*(-5 + 15*t1 + t2) + 5*t1*(-3 + 2*t2)) + 
            Power(s2,5)*(-2 - 2*t2 + 29*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,2)*(10 + t2) + 2*t1*(-8 - 21*t2 + Power(t2,2))) \
+ Power(s2,4)*(-18 + 3*Power(t1,3)*(-5 + t2) + 76*t2 + 21*Power(t2,2) - 
               4*Power(t2,3) - 9*Power(t2,4) + 
               Power(t1,2)*(28 + 65*t2 - 21*Power(t2,2)) + 
               t1*(31 - 69*t2 - 52*Power(t2,2) + 27*Power(t2,3))) + 
            Power(s2,3)*(58 - 5*Power(t1,4)*(-1 + t2) + 48*t2 + 
               19*Power(t2,2) - 38*Power(t2,3) - 59*Power(t2,4) + 
               Power(t1,3)*(-37 + 2*t2 + 31*Power(t2,2)) - 
               Power(t1,2)*(8 - 193*t2 + 114*Power(t2,2) + 
                  47*Power(t2,3)) + 
               t1*(-82 - 227*t2 - 30*Power(t2,2) + 166*Power(t2,3) + 
                  21*Power(t2,4))) + 
            Power(s2,2)*(56 - 240*t2 + 54*Power(t2,2) + 
               75*Power(t2,3) - 146*Power(t2,4) - 31*Power(t2,5) + 
               9*Power(t2,6) + Power(t1,5)*(5 + 2*t2) + 
               Power(t1,4)*(33 - 75*t2 - 11*Power(t2,2)) + 
               2*Power(t1,3)*
                (-37 - 71*t2 + 149*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(304 - 23*t2 + 39*Power(t2,2) - 
                  422*Power(t2,3) + 10*Power(t2,4)) + 
               t1*(-238 + 320*t2 - 170*Power(t2,2) + 216*Power(t2,3) + 
                  225*Power(t2,4) - 22*Power(t2,5))) + 
            s2*(-172 - 3*Power(t1,6) + 256*t2 - 176*Power(t2,2) - 
               290*Power(t2,3) + 57*Power(t2,4) - 54*Power(t2,5) + 
               14*Power(t2,6) + 3*Power(t2,7) + 
               Power(t1,5)*(-11 + 46*t2 - 3*Power(t2,2)) + 
               Power(t1,4)*(24 + 13*t2 - 176*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(t1,3)*(-72 + 279*t2 + 9*Power(t2,2) + 
                  268*Power(t2,3) - 30*Power(t2,4)) + 
               Power(t1,2)*(-204 - 194*t2 - 501*Power(t2,2) - 
                  67*Power(t2,3) - 163*Power(t2,4) + 30*Power(t2,5)) + 
               t1*(382 - 244*t2 + 700*Power(t2,2) + 141*Power(t2,3) + 
                  110*Power(t2,4) + 14*Power(t2,5) - 15*Power(t2,6))) + 
            Power(s1,4)*(-7 - 11*Power(s2,4) - 24*Power(t1,4) - 51*t2 - 
               33*Power(t2,2) + 75*Power(t2,3) - 15*Power(t2,4) + 
               2*Power(s2,3)*(-6 + 5*t1 + 19*t2) + 
               6*Power(t1,3)*(-11 + 24*t2) + 
               Power(t1,2)*(-13 + 312*t2 - 225*Power(t2,2)) + 
               Power(s2,2)*(98 + 57*t1 - 45*Power(t1,2) - 189*t2 + 
                  90*t1*t2 + 35*Power(t2,2)) + 
               t1*(48 + 38*t2 - 351*Power(t2,2) + 120*Power(t2,3)) + 
               s2*(-98 + 66*Power(t1,3) + Power(t1,2)*(14 - 266*t2) + 
                  82*t2 - 24*Power(t2,2) + 25*Power(t2,3) + 
                  7*t1*t2*(-11 + 25*t2))) - 
            Power(s1,5)*(-13 + 13*Power(s2,3) + 36*Power(t1,3) + 
               Power(t1,2)*(54 - 90*t2) - 5*t2 + 21*Power(t2,2) - 
               6*Power(t2,3) + 2*Power(s2,2)*(-17 + 12*t1 + 7*t2) + 
               3*t1*(3 - 38*t2 + 18*Power(t2,2)) + 
               s2*(22 - 72*Power(t1,2) - 22*t2 + 8*Power(t2,2) + 
                  t1*(-12 + 85*t2))) + 
            Power(s1,3)*(66 - 17*Power(s2,5) + 4*Power(t1,5) + 37*t2 + 
               80*Power(t2,2) + 82*Power(t2,3) - 130*Power(t2,4) + 
               20*Power(t2,5) + 4*Power(t1,4)*(-6 + 13*t2) + 
               Power(s2,4)*(-6 + 53*t1 + 15*t2) + 
               Power(t1,3)*(14 + 268*t2 - 216*Power(t2,2)) + 
               Power(t1,2)*(119 + 19*t2 - 678*Power(t2,2) + 
                  280*Power(t2,3)) - 
               t1*(187 + 151*t2 + 91*Power(t2,2) - 564*Power(t2,3) + 
                  140*Power(t2,4)) - 
               Power(s2,3)*(-106 + 67*Power(t1,2) - 53*t2 + 
                  36*Power(t2,2) + 3*t1*(33 + t2)) + 
               Power(s2,2)*(-262 + 51*Power(t1,3) - 94*t2 + 
                  394*Power(t2,2) - 60*Power(t2,3) + 
                  Power(t1,2)*(185 + 77*t2) + 
                  t1*(44 - 381*t2 - 104*Power(t2,2))) - 
               s2*(-153 + 24*Power(t1,4) - 279*t2 + 60*Power(t2,2) + 
                  24*Power(t2,3) + 40*Power(t2,4) + 
                  Power(t1,3)*(83 + 122*t2) - 
                  Power(t1,2)*(19 + 34*t2 + 336*Power(t2,2)) + 
                  t1*(61 + 119*t2 - 145*Power(t2,2) + 150*Power(t2,3)))) \
- Power(s1,2)*(174 + 2*Power(s2,6) - 6*Power(t1,6) + 100*t2 + 
               103*Power(t2,2) + 64*Power(t2,3) + 98*Power(t2,4) - 
               120*Power(t2,5) + 15*Power(t2,6) + 
               9*Power(t1,5)*(-1 + 2*t2) - 
               Power(s2,5)*(5 + t1 + 25*t2) + 
               3*Power(t1,4)*(-10 - 14*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(-219 + 49*t2 + 372*Power(t2,2) - 
                  144*Power(t2,3)) + 
               Power(t1,2)*(455 + 396*t2 + 18*Power(t2,2) - 
                  702*Power(t2,3) + 180*Power(t2,4)) - 
               t1*(362 + 516*t2 + 225*Power(t2,2) + 135*Power(t2,3) - 
                  501*Power(t2,4) + 90*Power(t2,5)) + 
               Power(s2,4)*(-112 - 22*Power(t1,2) + t2 + 
                  6*Power(t2,2) + t1*(44 + 79*t2)) + 
               Power(s2,3)*(16 + 60*Power(t1,3) + 
                  Power(t1,2)*(16 - 117*t2) + 148*t2 + 
                  129*Power(t2,2) - 10*Power(t2,3) + 
                  t1*(194 - 283*t2 + 3*Power(t2,2))) - 
               Power(s2,2)*(-165 + 66*Power(t1,4) + 631*t2 - 
                  252*Power(t2,2) - 388*Power(t2,3) + 65*Power(t2,4) - 
                  2*Power(t1,3)*(-92 + 67*t2) + 
                  Power(t1,2)*(91 - 696*t2 - 9*Power(t2,2)) + 
                  t1*(53 + 5*t2 + 816*Power(t2,2) + 12*Power(t2,3))) + 
               s2*(-210 + 33*Power(t1,5) + Power(t1,4)*(138 - 89*t2) + 
                  732*t2 + 207*Power(t2,2) + 92*Power(t2,3) - 
                  71*Power(t2,4) - 35*Power(t2,5) + 
                  Power(t1,3)*(49 - 433*t2 - 16*Power(t2,2)) + 
                  Power(t1,2)*
                   (116 + 231*t2 + 273*Power(t2,2) + 132*Power(t2,3)) - 
                  t1*(92 + 491*t2 + 348*Power(t2,2) - 93*Power(t2,3) + 
                     25*Power(t2,4)))) - 
            s1*(-40 + Power(s2,6)*(8 + t1 - t2) + 
               6*Power(t1,6)*(-1 + t2) - 322*t2 + 12*Power(t2,2) - 
               123*Power(t2,3) - 27*Power(t2,4) - 57*Power(t2,5) + 
               57*Power(t2,6) - 6*Power(t2,7) + 
               Power(t1,5)*(-11 + 30*t2 - 18*Power(t2,2)) + 
               Power(t1,4)*(-204 + 28*t2 + 9*Power(t2,2) + 
                  6*Power(t2,3)) + 
               3*Power(t1,3)*
                (173 + 173*t2 - 11*Power(t2,2) - 68*Power(t2,3) + 
                  12*Power(t2,4)) - 
               Power(t1,2)*(492 + 913*t2 + 477*Power(t2,2) + 
                  31*Power(t2,3) - 348*Power(t2,4) + 54*Power(t2,5)) + 
               t1*(204 + 696*t2 + 461*Power(t2,2) + 189*Power(t2,3) + 
                  104*Power(t2,4) - 234*Power(t2,5) + 30*Power(t2,6)) + 
               Power(s2,5)*(-34 - 6*Power(t1,2) + 35*t2 + 
                  5*Power(t2,2) + 3*t1*(-5 + 2*t2)) + 
               Power(s2,4)*(87 + 12*Power(t1,3) + 101*t2 - 
                  11*Power(t2,2) - 11*Power(t2,3) - 
                  Power(t1,2)*(37 + 8*t2) + 
                  t1*(59 - 75*t2 + Power(t2,2))) - 
               Power(s2,3)*(-163 + 10*Power(t1,4) + 
                  17*Power(t1,3)*(-8 + t2) - 7*t2 + 80*Power(t2,2) + 
                  147*Power(t2,3) + Power(t2,4) + 
                  Power(t1,2)*(-119 + 187*t2 - 3*Power(t2,2)) + 
                  t1*(501 + 143*t2 - 350*Power(t2,2) - 25*Power(t2,3))) \
+ Power(s2,2)*(-306 + 3*Power(t1,5) - 5*t2 + 444*Power(t2,2) - 
                  394*Power(t2,3) - 180*Power(t2,4) + 38*Power(t2,5) + 
                  3*Power(t1,4)*(-49 + 15*t2) + 
                  Power(t1,3)*(-310 + 543*t2 - 71*Power(t2,2)) + 
                  Power(t1,2)*
                   (587 + 127*t2 - 933*Power(t2,2) + 33*Power(t2,3)) + 
                  t1*(141 - 311*t2 + 265*Power(t2,2) + 
                     717*Power(t2,3) - 48*Power(t2,4))) + 
               s2*(110 + Power(t1,5)*(61 - 31*t2) + 8*t2 - 
                  869*Power(t2,2) + 31*Power(t2,3) - 146*Power(t2,4) + 
                  54*Power(t2,5) + 16*Power(t2,6) + 
                  Power(t1,4)*(177 - 346*t2 + 80*Power(t2,2)) - 
                  Power(t1,3)*
                   (3 + 91*t2 - 618*Power(t2,2) + 70*Power(t2,3)) + 
                  Power(t1,2)*
                   (-521 - 407*t2 - 279*Power(t2,2) - 388*Power(t2,3) + 
                     40*Power(t2,4)) + 
                  t1*(256 + 706*t2 + 571*Power(t2,2) + 339*Power(t2,3) + 
                     Power(t2,4) - 35*Power(t2,5))))) + 
         s*(-24 + 14*t1 + 150*Power(t1,2) - 378*Power(t1,3) + 
            330*Power(t1,4) - 98*Power(t1,5) + 4*Power(t1,6) + 
            2*Power(t1,7) + Power(s1,7)*
             (-3*Power(s2,2) + s2*(2 - 9*t1) + 2*t1*(2 + 3*t1 - t2)) + 
            130*t2 - 2*Power(s2,7)*t2 - 380*t1*t2 + 652*Power(t1,2)*t2 - 
            625*Power(t1,3)*t2 + 227*Power(t1,4)*t2 + 8*Power(t1,5)*t2 - 
            14*Power(t1,6)*t2 + 2*Power(t2,2) - 144*t1*Power(t2,2) + 
            397*Power(t1,2)*Power(t2,2) - 227*Power(t1,3)*Power(t2,2) - 
            99*Power(t1,4)*Power(t2,2) + 46*Power(t1,5)*Power(t2,2) - 
            Power(t1,6)*Power(t2,2) - 2*Power(t2,3) - 
            191*t1*Power(t2,3) + 145*Power(t1,2)*Power(t2,3) + 
            218*Power(t1,3)*Power(t2,3) - 78*Power(t1,4)*Power(t2,3) + 
            5*Power(t1,5)*Power(t2,3) + 69*Power(t2,4) - 
            43*t1*Power(t2,4) - 200*Power(t1,2)*Power(t2,4) + 
            78*Power(t1,3)*Power(t2,4) - 8*Power(t1,4)*Power(t2,4) - 
            4*Power(t2,5) + 78*t1*Power(t2,5) - 
            58*Power(t1,2)*Power(t2,5) + 2*Power(t1,3)*Power(t2,5) - 
            9*Power(t2,6) + 34*t1*Power(t2,6) + 
            7*Power(t1,2)*Power(t2,6) - 10*Power(t2,7) - 
            7*t1*Power(t2,7) + 2*Power(t2,8) + 
            Power(s2,6)*(-2 + 5*t2 + 2*Power(t2,3) + 
               t1*(2 + 7*t2 - 2*Power(t2,2))) + 
            Power(s2,5)*(7 - 33*t2 + 9*Power(t2,2) + 36*Power(t2,3) + 
               3*Power(t2,4) - 2*t1*t2*(3 + 16*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(-7 - t2 + 5*Power(t2,2))) + 
            Power(s2,4)*(-24 + 13*t2 - 27*Power(t2,2) + 60*Power(t2,3) + 
               60*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,3)*(3 - 16*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*(10 + 10*t2 + 111*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(11 + 115*t2 - 100*Power(t2,2) - 161*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s2,3)*(12 + 9*t2 + 211*Power(t2,2) + 6*Power(t2,3) + 
               44*Power(t2,4) + 24*Power(t2,5) - 4*Power(t2,6) + 
               Power(t1,4)*(11 + 14*t2 - Power(t2,2)) + 
               Power(t1,3)*t2*(-71 - 104*t2 + 13*Power(t2,2)) + 
               Power(t1,2)*(-125 - 21*t2 + 300*Power(t2,2) + 
                  174*Power(t2,3) - 27*Power(t2,4)) + 
               t1*(102 - 145*t2 - 121*Power(t2,2) - 232*Power(t2,3) - 
                  108*Power(t2,4) + 19*Power(t2,5))) + 
            Power(s2,2)*(14 + 248*t2 - 25*Power(t2,2) + 
               257*Power(t2,3) + 102*Power(t2,4) - 53*Power(t2,5) - 
               6*Power(t2,6) + Power(t1,5)*(-11 + t2 + Power(t2,2)) + 
               Power(t1,4)*(-16 + 97*t2 + 2*Power(t2,2) - 
                  10*Power(t2,3)) + 
               Power(t1,3)*(125 - 203*t2 - 309*Power(t2,2) + 
                  30*Power(t2,3) + 24*Power(t2,4)) + 
               Power(t1,2)*(38 + 524*t2 + 302*Power(t2,2) + 
                  318*Power(t2,3) - 76*Power(t2,4) - 22*Power(t2,5)) + 
               t1*(-150 - 495*t2 - 428*Power(t2,2) - 293*Power(t2,3) - 
                  42*Power(t2,4) + 49*Power(t2,5) + 7*Power(t2,6))) + 
            s2*(58 - 116*t2 - 3*Power(t1,6)*t2 + 382*Power(t2,2) + 
               187*Power(t2,3) - 48*Power(t2,4) + 23*Power(t2,5) - 
               31*Power(t2,6) - 2*Power(t2,7) + Power(t2,8) + 
               Power(t1,5)*(4 - 21*t2 + 24*Power(t2,2) + Power(t2,3)) + 
               Power(t1,4)*(80 + 134*t2 + 54*Power(t2,2) - 
                  84*Power(t2,3) - 3*Power(t2,4)) + 
               Power(t1,3)*(-446 - 585*t2 - 109*Power(t2,2) - 
                  53*Power(t2,3) + 140*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,2)*(540 + 807*t2 + 866*Power(t2,2) - 
                  129*Power(t2,3) - 3*Power(t2,4) - 111*Power(t2,5) + 
                  2*Power(t2,6)) - 
               t1*(236 + 264*t2 + 980*Power(t2,2) + 237*Power(t2,3) - 
                  77*Power(t2,4) - 54*Power(t2,5) - 36*Power(t2,6) + 
                  3*Power(t2,7))) + 
            Power(s1,6)*(9*Power(s2,3) + 14*Power(t1,3) + 
               Power(t1,2)*(13 - 36*t2) + 
               Power(s2,2)*(-13 + 6*t1 + 16*t2) + 
               2*(-1 - 2*t2 + Power(t2,2)) + 
               t1*(6 - 37*t2 + 12*Power(t2,2)) + 
               s2*(15 - 34*Power(t1,2) - 13*t2 + Power(t2,2) + 
                  t1*(8 + 53*t2))) - 
            Power(s1,5)*(3*Power(s2,4) - 6*Power(t1,4) + 
               12*Power(t1,3)*(-1 + 5*t2) + 
               Power(s2,3)*(-1 - 19*t1 + 41*t2) + 
               Power(t1,2)*(-6 + 93*t2 - 90*Power(t2,2)) + 
               2*(6 - 8*t2 - 15*Power(t2,2) + 6*Power(t2,3)) + 
               t1*(11 + 50*t2 - 132*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s2,2)*(47 + 3*Power(t1,2) - 76*t2 + 
                  35*Power(t2,2) + t1*(50 + 27*t2)) + 
               s2*(10 + 18*Power(t1,3) + 56*t2 - 37*Power(t2,2) + 
                  6*Power(t2,3) - Power(t1,2)*(51 + 148*t2) + 
                  t1*(-30 + 49*t2 + 127*Power(t2,2)))) + 
            Power(s1,4)*(23 + 13*Power(s2,5) - 6*Power(t1,5) + 43*t2 - 
               53*Power(t2,2) - 90*Power(t2,3) + 30*Power(t2,4) - 
               2*Power(t1,4)*(1 + 7*t2) + 
               Power(s2,4)*(39 - 48*t1 + 22*t2) + 
               2*Power(t1,3)*(-4 - 27*t2 + 50*Power(t2,2)) + 
               Power(s2,3)*(-78 + 74*Power(t1,2) + 57*t2 - 85*t1*t2 + 
                  70*Power(t2,2)) - 
               Power(t1,2)*(59 + 83*t2 - 249*Power(t2,2) + 
                  120*Power(t2,3)) + 
               t1*(-29 + 122*t2 + 174*Power(t2,2) - 245*Power(t2,3) + 
                  40*Power(t2,4)) + 
               Power(s2,2)*(176 - 63*Power(t1,3) + 133*t2 - 
                  180*Power(t2,2) + 40*Power(t2,3) + 
                  2*Power(t1,2)*(-74 + 13*t2) + 
                  t1*(-44 + 225*t2 + 55*Power(t2,2))) + 
               s2*(82 + 30*Power(t1,4) + 29*t2 + 43*Power(t2,2) - 
                  60*Power(t2,3) + 15*Power(t2,4) + 
                  Power(t1,3)*(131 + 42*t2) + 
                  Power(t1,2)*(99 - 304*t2 - 250*Power(t2,2)) + 
                  t1*(-188 - 18*t2 + 152*Power(t2,2) + 155*Power(t2,3)))) \
+ Power(s1,3)*(-44 + 4*Power(s2,6) - 4*Power(t1,6) - 112*t2 - 
               53*Power(t2,2) + 92*Power(t2,3) + 140*Power(t2,4) - 
               40*Power(t2,5) + 2*Power(t1,5)*(-4 + 9*t2) - 
               Power(s2,5)*(9 + 8*t1 + 33*t2) + 
               Power(t1,4)*(2 + 26*t2 + 6*Power(t2,2)) - 
               Power(t1,3)*(119 + 81*t2 - 88*Power(t2,2) + 
                  80*Power(t2,3)) + 
               Power(t1,2)*(-165 + 437*t2 + 271*Power(t2,2) - 
                  326*Power(t2,3) + 90*Power(t2,4)) + 
               t1*(249 + 59*t2 - 378*Power(t2,2) - 316*Power(t2,3) + 
                  260*Power(t2,4) - 30*Power(t2,5)) - 
               Power(s2,4)*(54 + 10*Power(t1,2) + 160*t2 + 
                  46*Power(t2,2) - 3*t1*(19 + 41*t2)) + 
               Power(s2,3)*(-88 + 43*Power(t1,3) + 
                  Power(t1,2)*(8 - 189*t2) + 148*t2 - 201*Power(t2,2) - 
                  50*Power(t2,3) + t1*(144 + 135*t2 + 122*Power(t2,2))) \
- Power(s2,2)*(-31 + 48*Power(t1,4) + Power(t1,3)*(185 - 169*t2) + 
                  730*t2 + 64*Power(t2,2) - 220*Power(t2,3) + 
                  25*Power(t2,4) + 
                  Power(t1,2)*(266 - 499*t2 + 38*Power(t2,2)) + 
                  t1*(-282 - 276*t2 + 424*Power(t2,2) + 70*Power(t2,3))) \
+ s2*(-295 + 23*Power(t1,5) + Power(t1,4)*(137 - 88*t2) - 179*t2 - 
                  50*Power(t2,2) + 88*Power(t2,3) + 60*Power(t2,4) - 
                  20*Power(t2,5) - 
                  5*Power(t1,3)*(-43 + 116*t2 + 4*Power(t2,2)) + 
                  Power(t1,2)*
                   (-275 - 231*t2 + 717*Power(t2,2) + 200*Power(t2,3)) + 
                  t1*(407 + 469*t2 - 180*Power(t2,2) - 278*Power(t2,3) - 
                     95*Power(t2,4)))) + 
            Power(s1,2)*(118 + 2*Power(s2,6)*(3 + t1 - 3*t2) + 76*t2 + 
               224*Power(t2,2) + 21*Power(t2,3) - 88*Power(t2,4) - 
               120*Power(t2,5) + 30*Power(t2,6) + 
               Power(t1,6)*(-3 + 6*t2) - 
               3*Power(t1,5)*(-8 - 9*t2 + 6*Power(t2,2)) + 
               Power(t1,4)*(-55 - 107*t2 - 54*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,3)*(-411 + 524*t2 + 264*Power(t2,2) - 
                  60*Power(t2,3) + 30*Power(t2,4)) + 
               Power(t1,2)*(731 + 414*t2 - 897*Power(t2,2) - 
                  375*Power(t2,3) + 219*Power(t2,4) - 36*Power(t2,5)) + 
               t1*(-430 - 665*t2 - 74*Power(t2,2) + 512*Power(t2,3) + 
                  314*Power(t2,4) - 159*Power(t2,5) + 12*Power(t2,6)) + 
               Power(s2,5)*(-41 - 9*Power(t1,2) + 60*t2 + 
                  30*Power(t2,2) + 2*t1*(-5 + 7*t2)) + 
               Power(s2,4)*(-8 + 15*Power(t1,3) + 126*t2 + 
                  263*Power(t2,2) + 36*Power(t2,3) + 
                  Power(t1,2)*(-38 + 4*t2) + 
                  t1*(120 - 259*t2 - 99*Power(t2,2))) + 
               Power(s2,3)*(271 - 11*Power(t1,4) + 152*t2 - 
                  18*Power(t2,2) + 251*Power(t2,3) + 5*Power(t2,4) - 
                  7*Power(t1,3)*(-19 + 7*t2) + 
                  Power(t1,2)*(40 + 104*t2 + 129*Power(t2,2)) - 
                  2*t1*(160 + 200*t2 + 189*Power(t2,2) + 23*Power(t2,3))\
) + Power(s2,2)*(-47 + 3*Power(t1,5) + 286*t2 + 1034*Power(t2,2) - 
                  136*Power(t2,3) - 145*Power(t2,4) + 8*Power(t2,5) + 
                  Power(t1,4)*(-145 + 66*t2) + 
                  Power(t1,3)*(-309 + 450*t2 - 125*Power(t2,2)) + 
                  Power(t1,2)*
                   (631 + 829*t2 - 630*Power(t2,2) - 12*Power(t2,3)) + 
                  t1*(-452 - 993*t2 - 462*Power(t2,2) + 
                     422*Power(t2,3) + 60*Power(t2,4))) + 
               s2*(132 + Power(t1,5)*(57 - 35*t2) + 865*t2 + 
                  64*Power(t2,2) + 76*Power(t2,3) - 187*Power(t2,4) - 
                  37*Power(t2,5) + 15*Power(t2,6) + 
                  Power(t1,4)*(166 - 382*t2 + 83*Power(t2,2)) - 
                  Power(t1,3)*
                   (258 + 525*t2 - 907*Power(t2,2) + 12*Power(t2,3)) + 
                  Power(t1,2)*
                   (622 + 631*t2 + 162*Power(t2,2) - 837*Power(t2,3) - 
                     70*Power(t2,4)) + 
                  t1*(-524 - 1285*t2 - 297*Power(t2,2) + 
                     348*Power(t2,3) + 292*Power(t2,4) + 19*Power(t2,5)))\
) + s1*(-82 + 2*Power(s2,7) - 144*t2 - 30*Power(t2,2) - 204*Power(t2,3) + 
               5*Power(t2,4) + 44*Power(t2,5) + 54*Power(t2,6) - 
               12*Power(t2,7) - Power(s2,6)*(7 + 5*t1 + 6*t2) + 
               Power(t1,6)*(16 + 3*t2 - 2*Power(t2,2)) + 
               Power(t1,5)*(38 - 77*t2 - 24*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,4)*(-449 + 145*t2 + 183*Power(t2,2) + 
                  38*Power(t2,3) - 4*Power(t2,4)) + 
               Power(t1,3)*(827 + 775*t2 - 623*Power(t2,2) - 
                  253*Power(t2,3) + 12*Power(t2,4) - 4*Power(t2,5)) + 
               Power(t1,2)*(-580 - 1358*t2 - 394*Power(t2,2) + 
                  719*Power(t2,3) + 239*Power(t2,4) - 69*Power(t2,5) + 
                  6*Power(t2,6)) + 
               t1*(232 + 708*t2 + 607*Power(t2,2) + 87*Power(t2,3) - 
                  323*Power(t2,4) - 162*Power(t2,5) + 52*Power(t2,6) - 
                  2*Power(t2,7)) + 
               Power(s2,5)*(18 + 4*Power(t1,2)*(-1 + t2) + 28*t2 - 
                  87*Power(t2,2) - 13*Power(t2,3) + 
                  2*t1*(13 + 23*t2 + Power(t2,2))) + 
               Power(s2,4)*(25 + 26*t2 - 132*Power(t2,2) - 
                  202*Power(t2,3) - 7*Power(t2,4) - 
                  4*Power(t1,3)*(-4 + 3*t2) + 
                  Power(t1,2)*(-36 - 86*t2 + 4*Power(t2,2)) + 
                  t1*(-127 + 2*t2 + 363*Power(t2,2) + 21*Power(t2,3))) + 
               Power(s2,3)*(3 - 443*t2 - 70*Power(t2,2) - 
                  96*Power(t2,3) - 132*Power(t2,4) + 11*Power(t2,5) + 
                  4*Power(t1,4)*(-1 + 3*t2) + 
                  Power(t1,3)*(29 - 9*t2 - 7*Power(t2,2)) + 
                  Power(t1,2)*
                   (243 - 361*t2 - 286*Power(t2,2) + 13*Power(t2,3)) + 
                  t1*(-57 + 403*t2 + 488*Power(t2,2) + 351*Power(t2,3) - 
                     29*Power(t2,4))) - 
               Power(s2,2)*(288 - 90*t2 + 574*Power(t2,2) + 
                  582*Power(t2,3) - 167*Power(t2,4) - 48*Power(t2,5) + 
                  Power(t2,6) + Power(t1,5)*(11 + 4*t2) + 
                  Power(t1,4)*(3 - 125*t2 + 8*Power(t2,2)) + 
                  Power(t1,3)*
                   (143 - 613*t2 + 295*Power(t2,2) + 5*Power(t2,3)) + 
                  Power(t1,2)*
                   (328 + 807*t2 + 881*Power(t2,2) - 355*Power(t2,3) - 
                     49*Power(t2,4)) + 
                  t1*(-601 - 759*t2 - 1004*Power(t2,2) - 
                     272*Power(t2,3) + 222*Power(t2,4) + 31*Power(t2,5))) \
+ s2*(120 + 6*Power(t1,6) - 576*t2 - 757*Power(t2,2) + 81*Power(t2,3) - 
                  68*Power(t2,4) + 128*Power(t2,5) + 13*Power(t2,6) - 
                  6*Power(t2,7) + 
                  Power(t1,5)*(-25 - 73*t2 + 11*Power(t2,2)) - 
                  Power(t1,4)*
                   (29 + 205*t2 - 329*Power(t2,2) + 22*Power(t2,3)) + 
                  Power(t1,3)*
                   (775 + 297*t2 + 363*Power(t2,2) - 598*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  Power(t1,2)*
                   (-1175 - 1519*t2 - 227*Power(t2,2) - 27*Power(t2,3) + 
                     484*Power(t2,4) + 4*Power(t2,5)) + 
                  t1*(376 + 1644*t2 + 1115*Power(t2,2) - 61*Power(t2,3) - 
                     234*Power(t2,4) - 161*Power(t2,5) + 7*Power(t2,6)))))\
)*T3(1 - s + s1 - t2,1 - s + s2 - t1))/
     ((-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       Power(s1 - s2 + t1 - t2,2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,2)) - 
    (8*(8 - 32*t1 + 48*Power(t1,2) - 32*Power(t1,3) + 8*Power(t1,4) + 
         Power(s1,7)*(Power(s2,2) + 2*s2*t1 - Power(t1,2)) + 
         2*Power(s,7)*Power(t1 - t2,2) - 2*t2 + 20*s2*t2 + 12*t1*t2 - 
         60*s2*t1*t2 - 24*Power(t1,2)*t2 + 60*s2*Power(t1,2)*t2 + 
         20*Power(t1,3)*t2 - 20*s2*Power(t1,3)*t2 - 6*Power(t1,4)*t2 - 
         11*Power(t2,2) + 9*s2*Power(t2,2) + 14*Power(s2,2)*Power(t2,2) + 
         9*t1*Power(t2,2) - 11*s2*t1*Power(t2,2) - 
         28*Power(s2,2)*t1*Power(t2,2) + 13*Power(t1,2)*Power(t2,2) - 
         5*s2*Power(t1,2)*Power(t2,2) + 
         14*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         9*Power(t1,3)*Power(t2,2) + 7*s2*Power(t1,3)*Power(t2,2) - 
         2*Power(t1,4)*Power(t2,2) + 35*Power(t2,3) - 15*s2*Power(t2,3) + 
         26*Power(s2,2)*Power(t2,3) - 36*t1*Power(t2,3) - 
         17*s2*t1*Power(t2,3) - 30*Power(s2,2)*t1*Power(t2,3) - 
         15*Power(t1,2)*Power(t2,3) + 27*s2*Power(t1,2)*Power(t2,3) + 
         4*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         16*Power(t1,3)*Power(t2,3) + 5*s2*Power(t1,3)*Power(t2,3) - 
         12*Power(t2,4) + 42*s2*Power(t2,4) + 
         15*Power(s2,3)*Power(t2,4) - 2*Power(s2,4)*Power(t2,4) + 
         36*t1*Power(t2,4) + 4*s2*t1*Power(t2,4) - 
         22*Power(s2,2)*t1*Power(t2,4) - 5*Power(s2,3)*t1*Power(t2,4) - 
         20*Power(t1,2)*Power(t2,4) - 32*s2*Power(t1,2)*Power(t2,4) - 
         4*Power(s2,2)*Power(t1,2)*Power(t2,4) - 8*Power(t2,5) - 
         22*s2*Power(t2,5) + 10*Power(s2,2)*Power(t2,5) + 
         4*Power(s2,3)*Power(t2,5) + 3*t1*Power(t2,5) + 
         30*s2*t1*Power(t2,5) + 17*Power(s2,2)*t1*Power(t2,5) + 
         Power(s2,3)*t1*Power(t2,5) + Power(t1,2)*Power(t2,5) - 
         3*Power(t2,6) - 7*s2*Power(t2,6) - 10*Power(s2,2)*Power(t2,6) - 
         Power(s2,3)*Power(t2,6) + 2*t1*Power(t2,6) - s2*t1*Power(t2,6) + 
         Power(t2,7) - s2*Power(t2,7) + 
         Power(s1,6)*(Power(s2,2)*(-7 + 4*t1 - 6*t2) + 
            t1*(-2 + Power(t1,2) + 2*t2 + 5*t1*t2) - 
            s2*(2 + 3*t1 + 3*Power(t1,2) + 2*t2 + 11*t1*t2)) + 
         Power(s,6)*(Power(t1,3) - 
            Power(t1,2)*(7 + 13*s1 + 2*s2 - 7*t2) + 
            t2*(3 - 7*s2 + s1*(3 + s2 - 9*t2) - 9*t2 - 4*s2*t2 + 
               9*Power(t2,2)) + 
            t1*(1 + 3*s2 + 16*t2 + 6*s2*t2 - 17*Power(t2,2) + 
               s1*(-7 + 3*s2 + 22*t2))) + 
         Power(s1,5)*(1 + 2*t2 - 4*Power(t1,3)*t2 - Power(t2,2) + 
            Power(s2,3)*(-6 + t1 + t2) - Power(t1,2)*t2*(3 + 10*t2) + 
            t1*(3 + 11*t2 - 10*Power(t2,2)) - 
            Power(s2,2)*(-4 + Power(t1,2) - 44*t2 - 15*Power(t2,2) + 
               t1*(6 + 19*t2)) + 
            s2*(14 + 15*t2 + 11*Power(t2,2) + Power(t1,2)*(-9 + 13*t2) + 
               t1*(-21 + 20*t2 + 25*Power(t2,2)))) - 
         Power(s1,4)*(7 + 2*Power(s2,4) + 9*t2 + 11*Power(t2,2) - 
            5*Power(t2,3) - Power(t1,3)*(-9 + t2 + 6*Power(t2,2)) + 
            t1*(-17 + 14*t2 + 22*Power(t2,2) - 20*Power(t2,3)) - 
            Power(t1,2)*(3 + 2*t2 + 12*Power(t2,2) + 10*Power(t2,3)) + 
            Power(s2,3)*(-9 - 28*t2 + 5*Power(t2,2) + t1*(-1 + 3*t2)) + 
            Power(s2,2)*(9 + Power(t1,2)*(9 - 4*t2) + 10*t2 + 
               116*Power(t2,2) + 20*Power(t2,3) + 
               t1*(8 - 45*t2 - 36*Power(t2,2))) + 
            s2*(8 + 3*Power(t1,3) + 75*t2 + 47*Power(t2,2) + 
               25*Power(t2,3) + 
               Power(t1,2)*(22 - 31*t2 + 22*Power(t2,2)) + 
               t1*(-47 - 116*t2 + 51*Power(t2,2) + 30*Power(t2,3)))) + 
         Power(s1,3)*(4 - 2*Power(t1,4) + 31*t2 + 8*Power(s2,4)*t2 + 
            29*Power(t2,2) + 24*Power(t2,3) - 10*Power(t2,4) + 
            2*Power(s2,3)*t2*
             (-21 + t1 - 26*t2 + t1*t2 + 5*Power(t2,2)) - 
            Power(t1,3)*(11 - 25*t2 + 3*Power(t2,2) + 4*Power(t2,3)) + 
            t1*(-41 - 85*t2 + 21*Power(t2,2) + 18*Power(t2,3) - 
               20*Power(t2,4)) + 
            Power(t1,2)*(50 + 13*t2 - 7*Power(t2,2) - 18*Power(t2,3) - 
               5*Power(t2,4)) + 
            Power(s2,2)*(-24 + 27*t2 - 4*Power(t2,2) + 
               164*Power(t2,3) + 15*Power(t2,4) + 
               Power(t1,2)*(-2 + 31*t2 - 6*Power(t2,2)) + 
               t1*(26 + 46*t2 - 116*Power(t2,2) - 34*Power(t2,3))) + 
            s2*(36 + 9*Power(t1,3)*(-2 + t2) - 18*t2 + 163*Power(t2,2) + 
               78*Power(t2,3) + 30*Power(t2,4) + 
               Power(t1,2)*(20 + 98*t2 - 39*Power(t2,2) + 
                  18*Power(t2,3)) + 
               t1*(-38 - 145*t2 - 252*Power(t2,2) + 64*Power(t2,3) + 
                  20*Power(t2,4)))) + 
         Power(s1,2)*(-21 + 4*Power(t1,4)*(-2 + t2) + 27*t2 - 
            53*Power(t2,2) - 12*Power(s2,4)*Power(t2,2) + 
            2*Power(s2,3)*(-6 + t1 - 5*t2)*(-6 + t2)*Power(t2,2) - 
            43*Power(t2,3) - 26*Power(t2,4) + 10*Power(t2,5) + 
            Power(t1,3)*(19 + 38*t2 - 23*Power(t2,2) + 3*Power(t2,3) + 
               Power(t2,4)) + 
            Power(t1,2)*(-35 - 115*t2 - 55*Power(t2,2) + 
               9*Power(t2,3) + 12*Power(t2,4) + Power(t2,5)) + 
            t1*(45 + 46*t2 + 155*Power(t2,2) - 9*Power(t2,3) - 
               2*Power(t2,4) + 10*Power(t2,5)) + 
            Power(s2,2)*(14 + 74*t2 - 27*Power(t2,2) + 32*Power(t2,3) - 
               131*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,2)*(14 + 8*t2 - 39*Power(t2,2) + 
                  4*Power(t2,3)) + 
               2*t1*(-14 - 41*t2 - 45*Power(t2,2) + 69*Power(t2,3) + 
                  8*Power(t2,4))) - 
            s2*(-21 + 87*t2 - 102*Power(t2,2) + 179*Power(t2,3) + 
               72*Power(t2,4) + 20*Power(t2,5) + 
               Power(t1,3)*(5 - 41*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(-31 + 13*t2 + 162*Power(t2,2) - 
                  21*Power(t2,3) + 7*Power(t2,4)) + 
               t1*(47 - 59*t2 - 153*Power(t2,2) - 270*Power(t2,3) + 
                  41*Power(t2,4) + 7*Power(t2,5)))) + 
         s1*(-6 + 32*t2 - 66*Power(t2,2) + 41*Power(t2,3) + 
            8*Power(s2,4)*Power(t2,3) + 30*Power(t2,4) + 
            14*Power(t2,5) - 5*Power(t2,6) - 
            2*Power(t1,4)*(1 - 5*t2 + Power(t2,2)) + 
            Power(s2,3)*Power(t2,3)*
             (-54 + t1*(14 - 3*t2) - 22*t2 + 5*Power(t2,2)) - 
            Power(t1,3)*(-12 + 10*t2 + 43*Power(t2,2) - 7*Power(t2,3) + 
               Power(t2,4)) + 
            Power(t1,2)*(-24 + 22*t2 + 80*Power(t2,2) + 
               59*Power(t2,3) - 5*Power(t2,4) - 3*Power(t2,5)) - 
            t1*(-20 + 54*t2 - 31*Power(t2,2) + 123*Power(t2,3) + 
               4*Power(t2,4) + 5*Power(t2,5) + 2*Power(t2,6)) + 
            Power(s2,2)*t2*(-28 - 76*t2 + 9*Power(t2,2) - 
               32*Power(t2,3) + 56*Power(t2,4) + Power(t2,5) - 
               Power(t1,2)*(28 + 10*t2 - 21*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(56 + 86*t2 + 74*Power(t2,2) - 78*Power(t2,3) - 
                  3*Power(t2,4))) + 
            s2*(-20 - 30*t2 + 66*Power(t2,2) - 118*Power(t2,3) + 
               99*Power(t2,4) + 35*Power(t2,5) + 7*Power(t2,6) + 
               Power(t1,3)*(20 - 2*t2 - 28*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(-60 - 26*t2 - 34*Power(t2,2) + 
                  118*Power(t2,3) - 4*Power(t2,4) + Power(t2,5)) + 
               t1*(60 + 58*t2 - 4*Power(t2,2) - 59*Power(t2,3) - 
                  143*Power(t2,4) + 12*Power(t2,5) + Power(t2,6)))) + 
         Power(s,4)*(-36*t1 + 23*Power(t1,2) - Power(t1,3) + 38*t2 - 
            54*t1*t2 - 10*Power(t1,3)*t2 - 31*Power(t2,2) + 
            29*t1*Power(t2,2) - 16*Power(t1,2)*Power(t2,2) + 
            6*Power(t1,3)*Power(t2,2) + 32*Power(t2,3) + 
            82*t1*Power(t2,3) + 2*Power(t1,2)*Power(t2,3) - 
            56*Power(t2,4) - 22*t1*Power(t2,4) + 14*Power(t2,5) + 
            Power(s2,3)*(-((-2 + t2)*t2) + t1*(2 + t2)) + 
            Power(s1,3)*(5*Power(s2,2) - 55*Power(t1,2) + 
               (15 - 14*t2)*t2 + s2*(-11 + 40*t1 + 6*t2) + 
               t1*(-58 + 60*t2)) + 
            Power(s2,2)*(-16 - 58*t2 + Power(t2,2) + 12*Power(t2,3) + 
               2*Power(t1,2)*(-5 + 2*t2) - 
               8*t1*(-3 - 2*t2 + 2*Power(t2,2))) + 
            s2*(14 - 3*Power(t1,3) + 34*t2 + 79*Power(t2,2) + 
               18*Power(t2,3) - 28*Power(t2,4) + 
               Power(t1,2)*(-2 + 51*t2 - 8*Power(t2,2)) + 
               t1*(5 - 29*t2 - 116*Power(t2,2) + 36*Power(t2,3))) + 
            Power(s1,2)*(-9 + 15*Power(t1,3) + 
               Power(s2,2)*(-35 + 20*t1 - 2*t2) + 22*t2 - 
               86*Power(t2,2) + 42*Power(t2,3) + 
               Power(t1,2)*(-46 + 95*t2) + 
               t1*(-16 + 241*t2 - 142*Power(t2,2)) - 
               s2*(-54 + 35*Power(t1,2) - 4*t2 + 40*Power(t2,2) + 
                  23*t1*(1 + t2))) + 
            s1*(-9 + 46*t2 - 54*Power(t2,2) + 127*Power(t2,3) - 
               42*Power(t2,4) + Power(s2,3)*(-6 + t1 + t2) - 
               4*Power(t1,3)*(-3 + 5*t2) + 
               Power(t1,2)*(18 + 56*t2 - 42*Power(t2,2)) + 
               Power(s2,2)*(40 - 5*Power(t1,2) - 3*t1*(-3 + t2) + 
                  29*t2 - 15*Power(t2,2)) + 
               t1*(23 - 18*t2 - 265*Power(t2,2) + 104*Power(t2,3)) + 
               s2*(-27 - 137*t2 - 11*Power(t2,2) + 62*Power(t2,3) + 
                  Power(t1,2)*(-60 + 41*t2) + 
                  t1*(9 + 153*t2 - 53*Power(t2,2))))) - 
         Power(s,5)*(3 - 4*t1 + 2*Power(t1,2) + 3*Power(t1,3) + 18*t2 - 
            12*t1*t2 + 20*Power(t1,2)*t2 - 4*Power(t1,3)*t2 - 
            14*Power(t2,2) - 59*t1*Power(t2,2) - 
            8*Power(t1,2)*Power(t2,2) + 36*Power(t2,3) + 
            28*t1*Power(t2,3) - 16*Power(t2,4) - 
            Power(s2,2)*(-2 + Power(t1,2) + 5*t2 + 3*Power(t2,2) - 
               4*t1*(1 + t2)) + 
            Power(s1,2)*(Power(s2,2) - 36*Power(t1,2) + 
               (11 - 16*t2)*t2 + s2*(-3 + 17*t1 + 4*t2) + 
               t1*(-32 + 50*t2)) + 
            s2*(-7 - 24*t2 + 11*Power(t2,2) + 17*Power(t2,3) + 
               Power(t1,2)*(-13 + 7*t2) + 
               t1*(11 + 24*t2 - 24*Power(t2,2))) + 
            s1*(-7 + 6*Power(t1,3) + 13*t2 - 47*Power(t2,2) + 
               32*Power(t2,3) + Power(s2,2)*(-7 + 4*t1 + t2) + 
               Power(t1,2)*(-29 + 40*t2) - 
               2*t1*(1 - 51*t2 + 39*Power(t2,2)) + 
               s2*(-13*Power(t1,2) + 2*t1*(1 + 6*t2) - 
                  3*(-6 + 6*t2 + 7*Power(t2,2))))) + 
         Power(s,3)*(-6 + 20*t1 - 8*Power(t1,2) - 8*Power(t1,3) + 
            2*Power(t1,4) - 35*t2 - 2*Power(s2,4)*t2 - 93*t1*t2 + 
            76*Power(t1,2)*t2 - 10*Power(t1,3)*t2 + 123*Power(t2,2) - 
            173*t1*Power(t2,2) + 22*Power(t1,2)*Power(t2,2) - 
            12*Power(t1,3)*Power(t2,2) - 3*Power(t2,3) + 
            22*t1*Power(t2,3) + 2*Power(t1,2)*Power(t2,3) + 
            4*Power(t1,3)*Power(t2,3) + 44*Power(t2,4) + 
            52*t1*Power(t2,4) - 2*Power(t1,2)*Power(t2,4) - 
            42*Power(t2,5) - 8*t1*Power(t2,5) + 6*Power(t2,6) + 
            Power(s1,4)*(-10*Power(s2,2) + 50*Power(t1,2) + 
               t1*(52 - 40*t2) + s2*(15 - 50*t1 - 4*t2) + 
               3*t2*(-3 + 2*t2)) + 
            Power(s2,3)*(14 + 25*t2 + 10*Power(t2,2) - 4*Power(t2,3) + 
               t1*(-14 + t2 + 4*Power(t2,2))) + 
            Power(s2,2)*(-17 - 47*t2 - 134*Power(t2,2) - 
               37*Power(t2,3) + 18*Power(t2,4) + 
               Power(t1,2)*(3 - 34*t2 + 6*Power(t2,2)) + 
               t1*(14 + 33*t2 + 89*Power(t2,2) - 24*Power(t2,3))) + 
            s2*(-4 - 9*Power(t1,3)*(-2 + t2) - 26*t2 - 51*Power(t2,2) + 
               95*Power(t2,3) + 49*Power(t2,4) - 22*Power(t2,5) - 
               Power(t1,2)*(54 + 55*t2 - 75*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(40 + 196*t2 + 4*Power(t2,2) - 175*Power(t2,3) + 
                  24*Power(t2,4))) + 
            Power(s1,3)*(-5 - 20*Power(t1,3) + 
               Power(t1,2)*(34 - 120*t2) - 21*t2 + 69*Power(t2,2) - 
               24*Power(t2,3) + Power(s2,2)*(70 - 40*t1 + 18*t2) + 
               t1*(28 - 273*t2 + 128*Power(t2,2)) + 
               s2*(-55 + 50*Power(t1,2) - 45*t2 + 34*Power(t2,2) + 
                  t1*(54 + 92*t2))) - 
            Power(s1,2)*(-35 + Power(t1,3)*(18 - 40*t2) + 8*t2 - 
               86*Power(t2,2) + 153*Power(t2,3) - 36*Power(t2,4) + 
               4*Power(s2,3)*(-6 + t1 + t2) + 
               Power(t1,2)*(42 + 45*t2 - 88*Power(t2,2)) + 
               t1*(79 + 28*t2 - 442*Power(t2,2) + 144*Power(t2,3)) - 
               Power(s2,2)*(-104 + 10*Power(t1,2) - 161*t2 + 
                  12*Power(t2,2) + t1*(3 + 52*t2)) + 
               s2*(3 - 219*t2 - 94*Power(t2,2) + 78*Power(t2,3) + 
                  Power(t1,2)*(-111 + 94*t2) + 
                  t1*(-54 + 329*t2 + 10*Power(t2,2)))) + 
            s1*(23 + 2*Power(s2,4) - 159*t2 + 16*Power(t2,2) - 
               109*Power(t2,3) + 135*Power(t2,4) - 24*Power(t2,5) + 
               Power(t1,3)*(12 + 29*t2 - 24*Power(t2,2)) - 
               Power(s2,3)*(19 + 7*t1 + 34*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(-76 + 13*t2 + 9*Power(t2,2) - 
                  16*Power(t2,3)) + 
               t1*(103 + 261*t2 - 22*Power(t2,2) - 273*Power(t2,3) + 
                  64*Power(t2,4)) + 
               Power(s2,2)*(64 + Power(t1,2)*(39 - 16*t2) + 242*t2 + 
                  128*Power(t2,2) - 38*Power(t2,3) + 
                  t1*(-55 - 96*t2 + 12*Power(t2,2))) + 
               s2*(-24 + 12*Power(t1,3) + 51*t2 - 259*Power(t2,2) - 
                  113*Power(t2,3) + 70*Power(t2,4) + 
                  Power(t1,2)*(25 - 181*t2 + 46*Power(t2,2)) - 
                  t1*(119 + 60*t2 - 450*Power(t2,2) + 56*Power(t2,3))))) \
+ Power(s,2)*(11 - 3*t1 - 35*Power(t1,2) + 35*Power(t1,3) - 
            8*Power(t1,4) - 21*t2 + 166*t1*t2 - 163*Power(t1,2)*t2 + 
            14*Power(t1,3)*t2 + 4*Power(t1,4)*t2 - 168*Power(t2,2) - 
            6*Power(s2,4)*Power(t2,2) + 66*t1*Power(t2,2) + 
            57*Power(t1,2)*Power(t2,2) - 17*Power(t1,3)*Power(t2,2) + 
            86*Power(t2,3) - 171*t1*Power(t2,3) + 
            37*Power(t1,2)*Power(t2,3) - 6*Power(t1,3)*Power(t2,3) + 
            16*Power(t2,4) + 2*t1*Power(t2,4) + 
            7*Power(t1,2)*Power(t2,4) + Power(t1,3)*Power(t2,4) + 
            34*Power(t2,5) + 14*t1*Power(t2,5) - 
            Power(t1,2)*Power(t2,5) - 15*Power(t2,6) - t1*Power(t2,6) + 
            Power(t2,7) + Power(s1,5)*
             (10*Power(s2,2) - 27*Power(t1,2) - (-2 + t2)*t2 + 
               s2*(-9 + 35*t1 + t2) + t1*(-23 + 14*t2)) + 
            Power(s2,3)*t2*(28 + 65*t2 + 18*Power(t2,2) - 
               6*Power(t2,3) + t1*(-28 - 9*t2 + 6*Power(t2,2))) + 
            Power(s1,4)*(9 + 15*Power(t1,3) + 
               Power(s2,2)*(-70 + 40*t1 - 32*t2) + 13*t2 - 
               23*Power(t2,2) + 5*Power(t2,3) + 
               Power(t1,2)*(-11 + 85*t2) + 
               t1*(-23 + 153*t2 - 57*Power(t2,2)) - 
               s2*(-18 + 49*t1 + 40*Power(t1,2) - 39*t2 + 108*t1*t2 + 
                  12*Power(t2,2))) + 
            Power(s2,2)*(34 + 16*t2 - 42*Power(t2,2) - 92*Power(t2,3) - 
               67*Power(t2,4) + 12*Power(t2,5) + 
               2*Power(t1,2)*
                (17 + 5*t2 - 21*Power(t2,2) + 2*Power(t2,3)) - 
               t1*(68 + 26*t2 + 28*Power(t2,2) - 131*Power(t2,3) + 
                  16*Power(t2,4))) + 
            s2*(-29 + t2 - 2*Power(t2,2) - 190*Power(t2,3) + 
               42*Power(t2,4) + 34*Power(t2,5) - 8*Power(t2,6) + 
               Power(t1,3)*(-7 + 41*t2 - 9*Power(t2,2)) + 
               Power(t1,2)*(-15 - 45*t2 - 136*Power(t2,2) + 
                  49*Power(t2,3) + 2*Power(t2,4)) + 
               3*t1*(17 + t2 + 117*Power(t2,2) + 27*Power(t2,3) - 
                  38*Power(t2,4) + 2*Power(t2,5))) + 
            Power(s1,3)*(-26 + Power(t1,3)*(12 - 40*t2) - 32*t2 - 
               73*Power(t2,2) + 72*Power(t2,3) - 10*Power(t2,4) + 
               6*Power(s2,3)*(-6 + t1 + t2) - 
               Power(t1,2)*(-38 + t2 + 92*Power(t2,2)) + 
               t1*(76 + 73*t2 - 335*Power(t2,2) + 88*Power(t2,3)) + 
               Power(s2,2)*(100 - 10*Power(t1,2) + 259*t2 + 
                  24*Power(t2,2) - t1*(23 + 98*t2)) + 
               s2*(69 - 114*t2 - 97*Power(t2,2) + 38*Power(t2,3) + 
                  Power(t1,2)*(-103 + 106*t2) + 
                  t1*(-112 + 315*t2 + 108*Power(t2,2)))) + 
            Power(s1,2)*(-57 - 6*Power(s2,4) + 143*t2 + 
               53*Power(t2,2) + 141*Power(t2,3) - 98*Power(t2,4) + 
               10*Power(t2,5) + 
               Power(s2,3)*(47 + t1*(9 - 6*t2) + 90*t2 - 
                  18*Power(t2,2)) + 
               3*Power(t1,3)*(-10 - 9*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(86 - 24*t2 + 42*Power(t2,2) + 
                  36*Power(t2,3)) - 
               t1*(61 + 346*t2 + 75*Power(t2,2) - 317*Power(t2,3) + 
                  62*Power(t2,4)) + 
               Power(s2,2)*(-85 - 304*t2 - 375*Power(t2,2) + 
                  20*Power(t2,3) + 3*Power(t1,2)*(-19 + 8*t2) + 
                  3*t1*(10 + 63*t2 + 20*Power(t2,2))) + 
               s2*(28 - 18*Power(t1,3) - 319*t2 + 216*Power(t2,2) + 
                  147*Power(t2,3) - 52*Power(t2,4) - 
                  6*Power(t1,2)*(11 - 40*t2 + 15*Power(t2,2)) + 
                  t1*(260 + 311*t2 - 597*Power(t2,2) - 26*Power(t2,3)))) \
+ s1*(-14 - 6*Power(t1,4) + 223*t2 + 12*Power(s2,4)*t2 - 
               203*Power(t2,2) - 46*Power(t2,3) - 115*Power(t2,4) + 
               62*Power(t2,5) - 5*Power(t2,6) + 
               Power(t1,3)*(5 + 45*t2 + 21*Power(t2,2) - 
                  12*Power(t2,3)) - 
               Power(t1,2)*(-96 + 141*t2 + 51*Power(t2,2) + 
                  37*Power(t2,3) + Power(t2,4)) + 
               t1*(-81 - 3*t2 + 441*Power(t2,2) + 23*Power(t2,3) - 
                  126*Power(t2,4) + 18*Power(t2,5)) - 
               2*Power(s2,3)*
                (14 + 56*t2 + 36*Power(t2,2) - 9*Power(t2,3) + 
                  t1*(-14 + 3*Power(t2,2))) + 
               Power(s2,2)*(-14 + 127*t2 + 296*Power(t2,2) + 
                  253*Power(t2,3) - 34*Power(t2,4) + 
                  Power(t1,2)*(-8 + 99*t2 - 18*Power(t2,2)) + 
                  t1*(22 - 2*t2 - 297*Power(t2,2) + 14*Power(t2,3))) + 
               s2*(48 + 27*Power(t1,3)*(-2 + t2) - 26*t2 + 
                  440*Power(t2,2) - 162*Power(t2,3) - 114*Power(t2,4) + 
                  33*Power(t2,5) + 
                  2*Power(t1,2)*
                   (60 + 101*t2 - 93*Power(t2,2) + 11*Power(t2,3)) - 
                  t1*(114 + 611*t2 + 280*Power(t2,2) - 445*Power(t2,3) + 
                     15*Power(t2,4))))) + 
         s*(-10 + 28*t1 - 24*Power(t1,2) + 4*Power(t1,3) + 
            2*Power(t1,4) + Power(s1,6)*
             (-5*Power(s2,2) + s2*(2 - 13*t1) + 2*t1*(2 + 4*t1 - t2)) + 
            32*t2 - 82*t1*t2 + 58*Power(t1,2)*t2 + 2*Power(t1,3)*t2 - 
            10*Power(t1,4)*t2 + 76*Power(t2,2) + 30*t1*Power(t2,2) - 
            146*Power(t1,2)*Power(t2,2) + 38*Power(t1,3)*Power(t2,2) + 
            2*Power(t1,4)*Power(t2,2) - 121*Power(t2,3) - 
            6*Power(s2,4)*Power(t2,3) + 151*t1*Power(t2,3) - 
            16*Power(t1,2)*Power(t2,3) - 8*Power(t1,3)*Power(t2,3) - 
            4*Power(t2,4) - 53*t1*Power(t2,4) + 
            18*Power(t1,2)*Power(t2,4) - Power(t1,3)*Power(t2,4) + 
            3*Power(t2,5) + 2*Power(t1,2)*Power(t2,5) + 12*Power(t2,6) + 
            t1*Power(t2,6) - 2*Power(t2,7) + 
            Power(s2,3)*Power(t2,2)*
             (14 + 55*t2 + 14*Power(t2,2) - 4*Power(t2,3) + 
               t1*(-14 - 13*t2 + 4*Power(t2,2))) + 
            Power(s2,2)*t2*(48 + 59*t2 - 11*Power(t2,2) - 
               4*Power(t2,3) - 44*Power(t2,4) + 3*Power(t2,5) + 
               Power(t1,2)*(48 + 11*t2 - 22*Power(t2,2) + Power(t2,3)) - 
               t1*(96 + 70*t2 + 59*Power(t2,2) - 79*Power(t2,3) + 
                  4*Power(t2,4))) - 
            s2*(-28 + 4*t2 + 2*Power(t2,2) - 80*Power(t2,3) + 
               134*Power(t2,4) + 5*Power(t2,5) - 6*Power(t2,6) + 
               Power(t2,7) + Power(t1,3)*
                (28 - 28*Power(t2,2) + 3*Power(t2,3)) - 
               Power(t1,2)*(84 - 4*t2 + 36*Power(t2,2) - 
                  115*Power(t2,3) + 12*Power(t2,4) + Power(t2,5)) + 
               t1*(84 - 8*t2 + 62*Power(t2,2) - 164*Power(t2,3) - 
                  89*Power(t2,4) + 29*Power(t2,5))) + 
            Power(s1,5)*(-6*Power(t1,3) + Power(t1,2)*(1 - 32*t2) + 
               Power(s2,2)*(35 - 20*t1 + 23*t2) + 
               2*(-1 - 2*t2 + Power(t2,2)) + 
               t1*(10 - 37*t2 + 10*Power(t2,2)) + 
               s2*(3 + 17*Power(t1,2) - 7*t2 + Power(t2,2) + 
                  4*t1*(5 + 14*t2))) - 
            Power(s1,4)*(Power(t1,3)*(3 - 20*t2) + 
               4*Power(s2,3)*(-6 + t1 + t2) + 
               Power(t1,2)*(12 - 13*t2 - 48*Power(t2,2)) + 
               2*(-1 - 5*t2 - 14*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(27 + 50*t2 - 109*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,2)*(38 - 5*Power(t1,2) + 176*t2 + 
                  39*Power(t2,2) - 3*t1*(7 + 24*t2)) + 
               s2*(60 + 7*t2 - 14*Power(t2,2) + 5*Power(t2,3) + 
                  Power(t1,2)*(-48 + 59*t2) + 
                  t1*(-81 + 135*t2 + 94*Power(t2,2)))) + 
            Power(s1,3)*(25 + 6*Power(s2,4) - 9*t2 - 21*Power(t2,2) - 
               72*Power(t2,3) + 20*Power(t2,4) + 
               Power(t1,3)*(28 + 7*t2 - 24*Power(t2,2)) - 
               Power(t1,2)*(36 - 9*t2 + 47*Power(t2,2) + 
                  32*Power(t2,3)) + 
               t1*(-23 + 153*t2 + 90*Power(t2,2) - 146*Power(t2,3) + 
                  20*Power(t2,4)) + 
               Power(s2,3)*(-37 - 86*t2 + 16*Power(t2,2) + 
                  t1*(-5 + 8*t2)) + 
               Power(s2,2)*(46 + Power(t1,2)*(37 - 16*t2) + 130*t2 + 
                  362*Power(t2,2) + 26*Power(t2,3) + 
                  t1*(9 - 154*t2 - 92*Power(t2,2))) + 
               s2*(-10 + 12*Power(t1,3) + 305*t2 + 8*Power(t2,2) - 
                  26*Power(t2,3) + 10*Power(t2,4) + 
                  Power(t1,2)*(65 - 141*t2 + 74*Power(t2,2)) + 
                  t1*(-193 - 338*t2 + 314*Power(t2,2) + 76*Power(t2,3)))) \
+ s1*(2 - 8*Power(t1,4)*(-2 + t2) - 124*t2 + 263*Power(t2,2) + 
               18*Power(s2,4)*Power(t2,2) + 3*Power(t2,3) - 
               13*Power(t2,4) - 52*Power(t2,5) + 10*Power(t2,6) + 
               Power(t1,2)*(62 + 284*t2 - 51*Power(t2,3) - 
                  22*Power(t2,4)) + 
               Power(t1,3)*(-54 - 52*t2 + 40*Power(t2,2) + 
                  3*Power(t2,3) - 2*Power(t2,4)) + 
               t1*(-26 - 100*t2 - 321*Power(t2,2) + 205*Power(t2,3) + 
                  20*Power(t2,4) - 25*Power(t2,5) + 2*Power(t2,6)) + 
               Power(s2,3)*t2*
                (-28 - 147*t2 - 66*Power(t2,2) + 16*Power(t2,3) + 
                  t1*(28 + 21*t2 - 8*Power(t2,2))) + 
               Power(s2,2)*(-48 - 114*t2 + 68*Power(t2,2) + 
                  62*Power(t2,3) + 203*Power(t2,4) - 9*Power(t2,5) + 
                  t1*(96 + 132*t2 + 127*Power(t2,2) - 
                     270*Power(t2,3)) - 
                  Power(t1,2)*
                   (48 + 18*t2 - 81*Power(t2,2) + 8*Power(t2,3))) + 
               s2*(-8 + 74*t2 - 170*Power(t2,2) + 453*Power(t2,3) + 
                  13*Power(t2,4) - 23*Power(t2,5) + 5*Power(t2,6) + 
                  2*Power(t1,3)*(6 - 41*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*
                   (-32 + 50*t2 + 295*Power(t2,2) - 69*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  t1*(28 - 42*t2 - 521*Power(t2,2) - 354*Power(t2,3) + 
                     162*Power(t2,4) + 4*Power(t2,5)))) + 
            Power(s1,2)*(48 + 6*Power(t1,4) - 167*t2 - 
               18*Power(s2,4)*t2 + 8*Power(t2,2) + 23*Power(t2,3) + 
               88*Power(t2,4) - 20*Power(t2,5) + 
               2*Power(t1,3)*
                (7 - 30*t2 - 3*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(-138 + 52*t2 + 36*Power(t2,2) + 
                  53*Power(t2,3) + 8*Power(t2,4)) + 
               t1*(70 + 193*t2 - 278*Power(t2,2) - 70*Power(t2,3) + 
                  94*Power(t2,4) - 10*Power(t2,5)) + 
               Power(s2,3)*(14 + 129*t2 + 114*Power(t2,2) - 
                  24*Power(t2,3) - t1*(14 + 3*t2)) + 
               Power(s2,2)*(55 - 103*t2 - 150*Power(t2,2) - 
                  380*Power(t2,3) + Power(t2,4) + 
                  Power(t1,2)*(7 - 96*t2 + 18*Power(t2,2)) + 
                  t1*(-62 - 77*t2 + 324*Power(t2,2) + 44*Power(t2,3))) - 
               s2*(27*Power(t1,3)*(-2 + t2) + 
                  Power(t1,2)*
                   (86 + 245*t2 - 150*Power(t2,2) + 38*Power(t2,3)) + 
                  t1*(-104 - 550*t2 - 522*Power(t2,2) + 
                     332*Power(t2,3) + 29*Power(t2,4)) + 
                  2*(36 - 50*t2 + 282*Power(t2,2) + 6*Power(t2,3) - 
                     17*Power(t2,4) + 5*Power(t2,5))))))*T4(-s + s1 - t2))/
     ((-1 + s1)*(s - s2 + t1)*Power(-s + s1 - t2,2)*(-1 + s1 + t1 - t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,2)) + 
    (8*(-21 - 5*s2 - 2*Power(s2,2) + 68*t1 + 18*s2*t1 + 6*Power(s2,2)*t1 - 
         76*Power(t1,2) - 24*s2*Power(t1,2) - 6*Power(s2,2)*Power(t1,2) + 
         30*Power(t1,3) + 14*s2*Power(t1,3) + 2*Power(s2,2)*Power(t1,3) + 
         Power(t1,4) - 3*s2*Power(t1,4) - 2*Power(t1,5) + 
         Power(s1,6)*(Power(s2,2) + 2*s2*t1 - Power(t1,2)) + 
         2*Power(s,5)*Power(t1 - t2,2)*(-1 + s1 + t1 - t2) - 26*t2 - 
         40*s2*t2 - 16*Power(s2,2)*t2 - 4*Power(s2,3)*t2 + 32*t1*t2 + 
         97*s2*t1*t2 + 38*Power(s2,2)*t1*t2 + 8*Power(s2,3)*t1*t2 + 
         28*Power(t1,2)*t2 - 69*s2*Power(t1,2)*t2 - 
         28*Power(s2,2)*Power(t1,2)*t2 - 4*Power(s2,3)*Power(t1,2)*t2 - 
         48*Power(t1,3)*t2 + 7*s2*Power(t1,3)*t2 + 
         6*Power(s2,2)*Power(t1,3)*t2 + 14*Power(t1,4)*t2 + 
         5*s2*Power(t1,4)*t2 - 5*Power(t2,2) - 25*s2*Power(t2,2) - 
         16*Power(s2,2)*Power(t2,2) - 15*Power(s2,3)*Power(t2,2) - 
         2*Power(s2,4)*Power(t2,2) - 40*t1*Power(t2,2) - 
         21*s2*t1*Power(t2,2) + 32*Power(s2,2)*t1*Power(t2,2) + 
         18*Power(s2,3)*t1*Power(t2,2) + 2*Power(s2,4)*t1*Power(t2,2) + 
         73*Power(t1,2)*Power(t2,2) + 75*s2*Power(t1,2)*Power(t2,2) - 
         12*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         3*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         28*Power(t1,3)*Power(t2,2) - 29*s2*Power(t1,3)*Power(t2,2) - 
         4*Power(s2,2)*Power(t1,3)*Power(t2,2) + 16*Power(t2,3) + 
         16*s2*Power(t2,3) + 3*Power(s2,3)*Power(t2,3) - 
         4*Power(s2,4)*Power(t2,3) - 39*t1*Power(t2,3) - 
         76*s2*t1*Power(t2,3) - 31*Power(s2,2)*t1*Power(t2,3) + 
         4*Power(s2,3)*t1*Power(t2,3) + 18*Power(t1,2)*Power(t2,3) + 
         46*s2*Power(t1,2)*Power(t2,3) + 
         17*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         Power(s2,3)*Power(t1,2)*Power(t2,3) + Power(t1,3)*Power(t2,3) + 
         11*Power(t2,4) + 21*s2*Power(t2,4) + 18*Power(s2,2)*Power(t2,4) + 
         3*Power(s2,3)*Power(t2,4) - 8*t1*Power(t2,4) - 
         28*s2*t1*Power(t2,4) - 19*Power(s2,2)*t1*Power(t2,4) - 
         2*Power(s2,3)*t1*Power(t2,4) + Power(t1,2)*Power(t2,4) - 
         s2*Power(t1,2)*Power(t2,4) + 2*Power(t2,5) + 8*s2*Power(t2,5) + 
         6*Power(s2,2)*Power(t2,5) + Power(s2,3)*Power(t2,5) - 
         t1*Power(t2,5) - Power(t2,6) + s2*Power(t2,6) + 
         Power(s1,5)*(Power(s2,2)*(-4 + 5*t1 - 5*t2) + 
            t1*(-2 + t1 + 2*t2 + 4*t1*t2) - 
            s2*(2 + 5*t1 + Power(t1,2) + 2*t2 + 9*t1*t2)) + 
         Power(s1,4)*(1 - Power(t1,3) + Power(t1,4) + 2*t2 - 
            Power(t2,2) + Power(s2,3)*(-2 + t1 + t2) + 
            t1*(5 + 7*t2 - 8*Power(t2,2)) - 
            2*Power(t1,2)*(1 + 2*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*(-1 + 3*Power(t1,2) + 25*t2 + 10*Power(t2,2) - 
               t1*(9 + 19*t2)) + 
            s2*(8 - 3*Power(t1,3) + 3*Power(t1,2)*(-3 + t2) + 15*t2 + 
               9*Power(t2,2) + 2*t1*(-6 + 11*t2 + 8*Power(t2,2)))) + 
         Power(s1,3)*(-4 + 4*Power(s2,4) - 9*Power(t1,3) - 10*t2 - 
            2*Power(t1,4)*t2 - 8*Power(t2,2) + 4*Power(t2,3) + 
            2*Power(t1,2)*(5 + 6*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
            t1*(7 - 16*t2 - 8*Power(t2,2) + 12*Power(t2,3)) + 
            Power(s2,3)*(-1 + Power(t1,2) + 3*t2 - 4*Power(t2,2) - 
               t1*(8 + t2)) - 
            Power(s2,2)*(-35 + Power(t1,3) + 24*t2 + 57*Power(t2,2) + 
               10*Power(t2,3) + Power(t1,2)*(2 + 8*t2) - 
               9*t1*(-2 + 6*t2 + 3*Power(t2,2))) + 
            s2*(2 - 48*t2 - 41*Power(t2,2) - 16*Power(t2,3) + 
               Power(t1,3)*(-12 + 7*t2) + 
               Power(t1,2)*(-18 + 29*t2 - 3*Power(t2,2)) + 
               t1*(42 + 68*t2 - 36*Power(t2,2) - 14*Power(t2,3)))) + 
         Power(s1,2)*(-1 + 2*Power(s2,4)*(-1 + t1 - 6*t2) + 23*t2 + 
            28*Power(t2,2) + 12*Power(t2,3) - 6*Power(t2,4) + 
            Power(t1,4)*(-11 + t2 + Power(t2,2)) + 
            Power(t1,3)*(9 + 17*t2 + 3*Power(t2,2)) + 
            t1*(-33 - 51*t2 + 9*Power(t2,2) + 2*Power(t2,3) - 
               8*Power(t2,4)) - 
            Power(t1,2)*(-36 + 2*t2 + 17*Power(t2,2) + 4*Power(t2,3) + 
               Power(t2,4)) - 
            Power(s2,3)*(13 - 5*t2 - 3*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,2)*(1 + t2) + t1*(-14 - 20*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(-19 - 66*t2 + 69*Power(t2,2) + 61*Power(t2,3) + 
               5*Power(t2,4) + Power(t1,3)*(-3 + 2*t2) + 
               Power(t1,2)*(-17 + 25*t2 + 7*Power(t2,2)) - 
               t1*(-39 + 3*t2 + 100*Power(t2,2) + 17*Power(t2,3))) + 
            s2*(-64 - 3*Power(t1,4) + 21*t2 + 93*Power(t2,2) + 
               53*Power(t2,3) + 14*Power(t2,4) + 
               Power(t1,3)*(-13 + 19*t2 - 5*Power(t2,2)) + 
               Power(t1,2)*(13 + 101*t2 - 32*Power(t2,2) + Power(t2,3)) + 
               t1*(67 - 183*t2 - 128*Power(t2,2) + 26*Power(t2,3) + 
                  6*Power(t2,4)))) - 
         s1*(-31 + 2*Power(t1,5) - 2*t2 + 35*Power(t2,2) + 
            30*Power(t2,3) + 8*Power(t2,4) - 4*Power(t2,5) - 
            4*Power(s2,4)*t2*(1 - t1 + 3*t2) + 
            Power(t1,4)*(5 - 9*t2 + Power(t2,2)) + 
            Power(t1,3)*(-28 - 29*t2 + 9*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,2)*(2 + 127*t2 + 26*Power(t2,2) - 6*Power(t2,3) - 
               Power(t2,4)) - 
            t1*(-50 + 87*t2 + 83*Power(t2,2) + 10*Power(t2,3) + 
               2*Power(t2,4) + 2*Power(t2,5)) + 
            Power(s2,3)*(-4 - 28*t2 + 7*Power(t2,2) + 7*Power(t2,3) + 
               4*Power(t2,4) + Power(t1,2)*(-4 - 4*t2 + Power(t2,2)) + 
               t1*(8 + 32*t2 + 16*Power(t2,2) - 5*Power(t2,3))) + 
            Power(s2,2)*(-14 - 35*t2 - 31*Power(t2,2) + 62*Power(t2,3) + 
               31*Power(t2,4) + Power(t2,5) + 
               Power(t1,3)*(4 - 7*t2 + Power(t2,2)) + 
               Power(t1,2)*(-22 - 29*t2 + 40*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(32 + 71*t2 - 52*Power(t2,2) - 74*Power(t2,3) - 
                  4*Power(t2,4))) + 
            s2*(-41 - 3*Power(t1,4)*(-2 + t2) - 89*t2 + 39*Power(t2,2) + 
               74*Power(t2,3) + 33*Power(t2,4) + 6*Power(t2,5) + 
               Power(t1,2)*(-69 + 88*t2 + 129*Power(t2,2) - 
                  13*Power(t2,3)) - 
               Power(t1,3)*(-5 + 42*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               t1*(99 + 46*t2 - 217*Power(t2,2) - 100*Power(t2,3) + 
                  7*Power(t2,4) + Power(t2,5)))) + 
         Power(s,4)*(Power(t1,4) - 2*Power(t1,3)*(4 + 4*s1 + s2 - t2) + 
            Power(t1,2)*(8 - 9*Power(s1,2) + 20*t2 - 12*Power(t2,2) + 
               s2*(5 + 8*t2) + s1*(-5 + s2 + 26*t2)) + 
            t1*(3 + 4*Power(s2,2) - 14*t2 - 16*Power(t2,2) + 
               14*Power(t2,3) + Power(s1,2)*(-7 + 3*s2 + 14*t2) + 
               4*s1*(2 + (3 + s2)*t2 - 7*Power(t2,2)) - 
               s2*(11 + 16*t2 + 10*Power(t2,2))) + 
            t2*(-7 - 4*Power(s2,2) + Power(s1,2)*(3 + s2 - 5*t2) + 6*t2 + 
               4*Power(t2,2) - 5*Power(t2,3) + 
               s2*(15 + 11*t2 + 4*Power(t2,2)) - 
               s1*((7 - 10*t2)*t2 + s2*(8 + 5*t2)))) - 
         Power(s,3)*(-11 + 9*t1 - 16*Power(t1,2) - Power(t1,3) + 
            3*Power(t1,4) + 2*Power(s2,3)*(2 + t1 - 3*t2) - 7*t2 + 
            32*t1*t2 - 18*Power(t1,2)*t2 + 5*Power(t1,3)*t2 - 
            2*Power(t1,4)*t2 + 29*t1*Power(t2,2) - 
            33*Power(t1,2)*Power(t2,2) + 2*Power(t1,3)*Power(t2,2) - 
            10*Power(t2,3) + 39*t1*Power(t2,3) + 
            6*Power(t1,2)*Power(t2,3) - 14*Power(t2,4) - 
            10*t1*Power(t2,4) + 4*Power(t2,5) + 
            Power(s1,3)*(Power(s2,2) - 16*Power(t1,2) + 
               18*t1*(-1 + t2) + (5 - 4*t2)*t2 + s2*(-3 + 11*t1 + 2*t2)) \
- Power(s2,2)*(18 + Power(t1,3) - 37*t2 - 26*Power(t2,2) - 
               3*Power(t2,3) - Power(t1,2)*(3 + 5*t2) + 
               t1*(12 + 29*t2 + 7*Power(t2,2))) + 
            s2*(27 - 11*t2 - Power(t2,2) - 6*Power(t2,3) - 
               9*Power(t2,4) + Power(t1,3)*(-13 + 3*t2) + 
               Power(t1,2)*(36 + 40*t2 - 15*Power(t2,2)) + 
               t1*(-12 - 49*t2 - 21*Power(t2,2) + 21*Power(t2,3))) + 
            Power(s1,2)*(1 + 5*Power(s2,2)*t1 - 12*Power(t1,3) + 2*t2 - 
               24*Power(t2,2) + 12*Power(t2,3) + 
               Power(t1,2)*(-17 + 46*t2) + 
               t1*(14 + 61*t2 - 46*Power(t2,2)) + 
               s2*(5 + 2*Power(t1,2) - 3*t2 - 13*Power(t2,2) - 
                  3*t1*(6 + t2))) + 
            s1*(6 + 4*Power(t1,4) - 2*t2 + 8*Power(t2,2) + 
               33*Power(t2,3) - 12*Power(t2,4) + 
               2*Power(t1,3)*(-8 + 3*t2) + 
               Power(t1,2)*(13 + 65*t2 - 36*Power(t2,2)) + 
               t1*(1 - 55*t2 - 82*Power(t2,2) + 38*Power(t2,3)) + 
               Power(s2,2)*(5 + 3*Power(t1,2) - 27*t2 - 4*Power(t2,2) + 
                  t1*(9 + t2)) + 
               s2*(-17 - 9*Power(t1,3) + 10*t2 + 12*Power(t2,2) + 
                  20*Power(t2,3) + 2*Power(t1,2)*(-4 + 9*t2) + 
                  t1*(9 + 24*t2 - 29*Power(t2,2))))) + 
         Power(s,2)*(-22 + 70*t1 - 85*Power(t1,2) + 38*Power(t1,3) - 
            Power(t1,4) - 14*t2 - 2*Power(s2,4)*t2 + 120*t1*t2 - 
            89*Power(t1,2)*t2 + 9*Power(t1,3)*t2 - 4*Power(t1,4)*t2 - 
            41*Power(t2,2) + 45*t1*Power(t2,2) + 
            Power(t1,2)*Power(t2,2) + 6*Power(t1,3)*Power(t2,2) + 
            Power(t1,4)*Power(t2,2) - 18*Power(t2,3) - 7*t1*Power(t2,3) + 
            10*Power(t1,2)*Power(t2,3) - 2*Power(t1,3)*Power(t2,3) - 
            2*Power(t2,4) - 22*t1*Power(t2,4) + 10*Power(t2,5) + 
            2*t1*Power(t2,5) - Power(t2,6) + 
            Power(s1,4)*(3*Power(s2,2) - 14*Power(t1,2) - (-2 + t2)*t2 + 
               s2*(-5 + 15*t1 + t2) + 5*t1*(-3 + 2*t2)) + 
            Power(s2,3)*(-6 + 10*t2 + 25*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(2 + t2) + t1*(4 - 15*t2 - 2*Power(t2,2))) + 
            Power(s2,2)*(14 + 2*Power(t1,3)*(-2 + t2) + 62*t2 - 
               33*Power(t2,2) - 21*Power(t2,3) - 6*Power(t2,4) + 
               Power(t1,2)*(12 + 24*t2 - 10*Power(t2,2)) + 
               t1*(-22 - 48*t2 + Power(t2,2) + 14*Power(t2,3))) + 
            s2*(16 - 3*Power(t1,4) - 70*t2 + 3*Power(t2,2) - 
               30*Power(t2,3) - 13*Power(t2,4) + 6*Power(t2,5) + 
               Power(t1,3)*(-15 + 28*t2) + 
               Power(t1,2)*(47 - 34*t2 - 84*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-45 + 20*t2 + 97*Power(t2,2) + 72*Power(t2,3) - 
                  12*Power(t2,4))) + 
            Power(s1,3)*(9 - 8*Power(t1,3) + 
               Power(s2,2)*(-4 + 15*t1 - 5*t2) + 3*t2 - 16*Power(t2,2) + 
               4*Power(t2,3) + 2*Power(t1,2)*(-5 + 21*t2) + 
               t1*(6 + 70*t2 - 32*Power(t2,2)) - 
               s2*(1 - 17*t2 + 9*Power(t2,2) + t1*(38 + 27*t2))) + 
            Power(s1,2)*(6*Power(t1,4) + Power(s2,3)*(2 + t1 + t2) + 
               Power(t1,3)*(-9 + 6*t2) + 
               Power(t1,2)*(14 + 54*t2 - 42*Power(t2,2)) + 
               t1*(37 - 53*t2 - 117*Power(t2,2) + 36*Power(t2,3)) - 
               2*(10 + 9*t2 + 4*Power(t2,2) - 18*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,2)*(17 + 9*Power(t1,2) - 16*t2 - 
                  5*Power(t2,2) - t1*(4 + 17*t2)) + 
               s2*(1 - 15*Power(t1,3) - 20*t2 - 32*Power(t2,2) + 
                  21*Power(t2,3) + Power(t1,2)*(-37 + 15*t2) + 
                  t1*(7 + 135*t2 - 3*Power(t2,2)))) + 
            s1*(31 + 2*Power(s2,4) - 6*Power(t1,4)*(-1 + t2) + 45*t2 + 
               27*Power(t2,2) + 7*Power(t2,3) - 32*Power(t2,4) + 
               4*Power(t2,5) + Power(t1,3)*(7 - 4*t2 + 4*Power(t2,2)) + 
               2*Power(t1,2)*
                (18 - 7*t2 - 27*Power(t2,2) + 7*Power(t2,3)) + 
               t1*(-102 - 61*t2 + 54*Power(t2,2) + 84*Power(t2,3) - 
                  16*Power(t2,4)) + 
               Power(s2,3)*(Power(t1,2) + t1*(3 + t2) - t2*(27 + 2*t2)) + 
               Power(s2,2)*(-30 - 3*Power(t1,3) + 15*t2 + 
                  41*Power(t2,2) + 13*Power(t2,3) + 
                  2*Power(t1,2)*(1 + t2) - 3*t1*(3 - t2 + 4*Power(t2,2))) \
+ s2*(-7 + 16*t2 + 51*Power(t2,2) + 33*Power(t2,3) - 19*Power(t2,4) + 
                  Power(t1,3)*(-37 + 13*t2) + 
                  Power(t1,2)*(23 + 137*t2 - 21*Power(t2,2)) + 
                  t1*(77 - 138*t2 - 169*Power(t2,2) + 27*Power(t2,3))))) + 
         s*(32 - 114*t1 + 140*Power(t1,2) - 64*Power(t1,3) + 
            4*Power(t1,4) + 2*Power(t1,5) + 
            Power(s1,5)*(-3*Power(s2,2) + s2*(2 - 9*t1) + 
               2*t1*(2 + 3*t1 - t2)) + 43*t2 - 104*t1*t2 + 
            47*Power(t1,2)*t2 + 24*Power(t1,3)*t2 - 10*Power(t1,4)*t2 + 
            43*Power(t2,2) + 24*t1*Power(t2,2) - 
            89*Power(t1,2)*Power(t2,2) + 25*Power(t1,3)*Power(t2,2) - 
            Power(t1,4)*Power(t2,2) - 13*Power(t2,3) + 64*t1*Power(t2,3) - 
            22*Power(t1,2)*Power(t2,3) + 3*Power(t1,3)*Power(t2,3) - 
            19*Power(t2,4) + 13*t1*Power(t2,4) - Power(t1,2)*Power(t2,4) - 
            8*Power(t2,5) - 3*t1*Power(t2,5) + 2*Power(t2,6) - 
            2*Power(s2,4)*t2*(1 - t1 + 3*t2) + 
            Power(s2,3)*(-2 - 21*t2 + 15*Power(t2,2) + 22*Power(t2,3) + 
               2*Power(t2,4) + Power(t1,2)*(-2 - t2 + 2*Power(t2,2)) + 
               t1*(4 + 22*t2 - 9*Power(t2,2) - 4*Power(t2,3))) + 
            Power(s2,2)*(-13 + 8*t2 + 51*Power(t2,2) + 20*Power(t2,3) + 
               7*Power(t2,4) - 3*Power(t2,5) + 
               Power(t1,3)*(1 - 8*t2 + Power(t2,2)) + 
               Power(t1,2)*(-15 + 8*t2 + 44*Power(t2,2) - 
                  5*Power(t2,3)) + 
               t1*(27 - 8*t2 - 92*Power(t2,2) - 43*Power(t2,3) + 
                  7*Power(t2,4))) + 
            s2*(-3*Power(t1,4)*(-2 + t2) + 
               Power(t1,3)*(-6 - 46*t2 + 15*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(6 - 151*t2 - 48*Power(t2,2) + 
                  40*Power(t2,3) + Power(t2,4)) - 
               t1*(-6 + 160*t2 + 99*Power(t2,2) - 11*Power(t2,3) - 
                  35*Power(t2,4) + Power(t2,5)) + 
               t2*(58 + 18*t2 + 25*Power(t2,2) - 9*Power(t2,3) - 
                  7*Power(t2,4) + Power(t2,5))) + 
            Power(s1,4)*(2*Power(t1,3) + 
               Power(s2,2)*(8 - 15*t1 + 10*t2) - 
               Power(t1,2)*(1 + 20*t2) + 2*(-1 - 2*t2 + Power(t2,2)) + 
               t1*(2 - 27*t2 + 8*Power(t2,2)) + 
               s2*(5 + 2*Power(t1,2) - 9*t2 + Power(t2,2) + 
                  t1*(25 + 29*t2))) - 
            Power(s1,3)*(2 + 4*Power(t1,4) + 2*Power(t1,3)*(-1 + t2) - 
               16*t2 - 20*Power(t2,2) + 8*Power(t2,3) + 
               2*Power(s2,3)*(t1 + t2) + 
               Power(t1,2)*(7 + 5*t2 - 24*Power(t2,2)) + 
               t1*(21 + 5*t2 - 60*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s2,2)*(11 + 9*Power(t1,2) + 32*t2 + 9*Power(t2,2) - 
                  t1*(18 + 37*t2)) - 
               s2*(-31 + 11*Power(t1,3) + Power(t1,2)*(33 - 8*t2) + t2 + 
                  22*Power(t2,2) - 4*Power(t2,3) - 
                  2*t1*(-8 + 57*t2 + 16*Power(t2,2)))) + 
            Power(s1,2)*(23 - 6*Power(s2,4) - 15*t2 - 45*Power(t2,2) - 
               36*Power(t2,3) + 12*Power(t2,4) + 
               Power(t1,4)*(-3 + 6*t2) + 
               Power(t1,3)*(1 + 9*t2 - 2*Power(t2,2)) + 
               Power(s2,3)*(3 + 7*t1 - 2*Power(t1,2) + 22*t2 + 
                  6*Power(t2,2)) - 
               Power(t1,2)*(31 + 28*t2 - 12*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(12 + 124*t2 + 17*Power(t2,2) - 58*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s2,2)*(-14 + 3*Power(t1,3) + 52*t2 + 
                  47*Power(t2,2) - 3*Power(t2,3) + 
                  Power(t1,2)*(3 + 11*t2) + 
                  t1*(12 - 87*t2 - 22*Power(t2,2))) + 
               s2*(70 + Power(t1,3)*(36 - 17*t2) + 70*t2 - 
                  26*Power(t2,2) - 32*Power(t2,3) + 6*Power(t2,4) + 
                  Power(t1,2)*(22 - 123*t2 + 9*Power(t2,2)) + 
                  t1*(-146 + 9*t2 + 188*Power(t2,2) + 12*Power(t2,3)))) - 
            s1*(59 + 2*Power(s2,4)*(-1 + t1 - 6*t2) + 51*t2 - 
               30*Power(t2,2) - 50*Power(t2,3) - 28*Power(t2,4) + 
               8*Power(t2,5) + Power(t1,4)*(-12 - 3*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(32 + 32*t2 + 14*Power(t2,2) - 2*Power(t2,3)) + 
               Power(t1,2)*(53 - 150*t2 - 57*Power(t2,2) + 
                  5*Power(t2,3) - 2*Power(t2,4)) + 
               t1*(-132 + 74*t2 + 167*Power(t2,2) + 27*Power(t2,3) - 
                  24*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,3)*(-19 + Power(t1,2) + 18*t2 + 44*Power(t2,2) + 
                  6*Power(t2,3) - 2*t1*(-9 + t2 + 3*Power(t2,2))) + 
               Power(s2,2)*(3 + 41*t2 + 61*Power(t2,2) + 30*Power(t2,3) - 
                  8*Power(t2,4) + Power(t1,3)*(-7 + 4*t2) + 
                  Power(t1,2)*(1 + 51*t2 - 3*Power(t2,2)) + 
                  t1*(3 - 88*t2 - 112*Power(t2,2) + 7*Power(t2,3))) + 
               s2*(18 - 6*Power(t1,4) + 97*t2 + 64*Power(t2,2) - 
                  29*Power(t2,3) - 24*Power(t2,4) + 4*Power(t2,5) + 
                  Power(t1,3)*(-25 + 46*t2 - 5*Power(t2,2)) + 
                  Power(t1,2)*
                   (78 + 89*t2 - 130*Power(t2,2) + 2*Power(t2,3)) - 
                  t1*(65 + 268*t2 - 36*Power(t2,2) - 134*Power(t2,3) + 
                     Power(t2,4))))))*T5(1 - s1 - t1 + t2))/
     ((-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,2)));
   return a;
};
