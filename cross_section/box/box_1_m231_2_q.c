#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_1_m231_2_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(Power(t1,2)*(1 - (s + 5*(5 + s1))*t2 + 
            (12 + s + s1)*Power(t2,2)) + 
         t1*(-1 + (-3 + 8*s + 6*s2)*t2 + 2*Power(s1,2)*(-9 + t2)*t2 + 
            (26 - 8*s - 5*s2)*Power(t2,2) - (30 + s2)*Power(t2,3) + 
            s1*(1 + (24 - 6*s + s2)*t2 + (30 + 6*s - s2)*Power(t2,2) + 
               Power(t2,3))) - 
         t2*(-11 + 2*Power(s1,2)*(-2 + s2*(-1 + t2) - 6*t2) + 8*t2 + 
            8*s*t2 + Power(t2,2) - 9*s*Power(t2,2) - 18*Power(t2,3) + 
            s*Power(t2,3) - s2*(-4 - 4*t2 + 7*Power(t2,2) + Power(t2,3)) + 
            s1*(15 + (8 - 6*s)*t2 + 3*(9 + 2*s)*Power(t2,2) + 
               2*Power(t2,3) + s2*(-2 + t2 + Power(t2,2))))))/
     ((-1 + t1)*(t1 - t2)*(-1 + s1 + t1 - t2)*Power(-1 + t2,2)*t2) - 
    (8*(4*Power(s1,3)*(-1 + t1)*t1 - 
         (-1 + s)*Power(t1,3)*(-1 + Power(t2,2)) + 
         (-1 + t2)*(5 + (-15 - 15*s + s2)*t2 + 
            (6*s - 2*(3 + s2))*Power(t2,2)) + 
         Power(s1,2)*(4*Power(t1,3) + t1*(-17 + 2*s*(-1 + t2) - 7*t2) + 
            s2*(3 + t1)*(-1 + t2) + 4*t2 - 
            Power(t1,2)*(7 + 2*s*(-1 + t2) + 9*t2)) + 
         t1*(1 + (14 - 8*s2)*t2 + (4 + 9*s2)*Power(t2,2) - 
            (-1 + s2)*Power(t2,3) + 
            s*(-5 + 15*t2 - 11*Power(t2,2) + Power(t2,3))) + 
         Power(t1,2)*(5 + (6 + s2)*t2 - (-1 + s2)*Power(t2,3) + 
            s*(-4 + 2*t2 + Power(t2,2) + Power(t2,3))) + 
         s1*(3 + s2*(-5 + Power(t1,2)*(-3 + t2) - t2)*(-1 + t2) + 14*t2 - 
            s*t2 + 3*Power(t2,2) + s*Power(t2,2) + 
            Power(t1,2)*(-17 + 3*s*(-1 + t2) - 6*t2 - Power(t2,2)) - 
            Power(t1,3)*(11 + 2*s*(-1 + t2) + Power(t2,2)) + 
            t1*(-15 + 16*t2 + 15*Power(t2,2) - s*(7 - 8*t2 + Power(t2,2)))\
))*B1(1 - s1 - t1 + t2,t1,t2))/((-1 + t1)*(s1*t1 - t2)*Power(-1 + t2,2)) + 
    (32*(Power(t1,4)*(-3 + t2) + t2*(1 + t2)*(1 + s2*(-1 + t2) + 2*t2) + 
         Power(t1,3)*(1 + s2 + s1*(-5 + t2) + s*(-1 + t2) + 4*t2 - 
            s2*t2 - Power(t2,2)) + 
         Power(t1,2)*(-2 + (7 + 2*s - s2)*t2 + 
            (-1 - 2*s + s2)*Power(t2,2) + s1*(5 + s2 + 3*t2 - s2*t2)) - 
         t1*(-4 + 11*t2 + 5*Power(t2,2) + s*Power(t2,2) - s*Power(t2,3) + 
            s1*(2 + s2 - s2*t2 + 2*Power(t2,2)) + 
            s2*(1 - 3*t2 + Power(t2,2) + Power(t2,3))))*R1(t1))/
     (Power(-1 + t1,2)*Power(t1 - t2,2)*Power(-1 + t2,2)) + 
    (16*(-(Power(t1,4)*(-1 + t2 - 11*Power(t2,2) + 3*Power(t2,3))) + 
         Power(t1,3)*t2*(-2 + (1 + 3*s - 4*s2)*t2 + 
            (-19 - 3*s + 4*s2)*Power(t2,2) + 4*Power(t2,3)) + 
         t1*Power(t2,2)*(-4 + (1 + 8*s - 4*s2)*t2 + 
            (110 - 5*s + s2)*Power(t2,2) + (-7 - 3*s + 3*s2)*Power(t2,3)) \
- Power(t1,2)*t2*(4 + (-26 + 4*s)*t2 + (72 + 2*s - 5*s2)*Power(t2,2) + 
            (-6*s + 5*(-3 + s2))*Power(t2,3) + Power(t2,4)) - 
         4*Power(t2,3)*(8 + (-6 + s)*t2 - (-12 + s)*Power(t2,2) + 
            2*s2*(-2 + t2 + Power(t2,2))) - 
         Power(s1,3)*t2*(Power(t1,2)*(-1 + t2) + 
            t2*(2 + s2 + 2*t2 - s2*t2) + 
            t1*(2 + s2 - 7*t2 - s2*t2 + Power(t2,2))) + 
         Power(s1,2)*(-(Power(t1,3)*(-1 + t2)*t2) + 
            Power(t1,2)*(1 - (6 + s + s2)*t2 + 
               (30 + s + s2)*Power(t2,2) - 5*Power(t2,3)) + 
            Power(t2,2)*(8 + 4*s2*(-1 + t2) - (5 + s)*t2 + 
               (12 + s)*Power(t2,2) + Power(t2,3)) + 
            t1*t2*(1 + (-7 + 2*s - 5*s2)*t2 + 
               (-31 - 2*s + 5*s2)*Power(t2,2) + Power(t2,3))) + 
         s1*(Power(t1,3)*(2 - (5 + s)*t2 + (34 + s)*Power(t2,2) - 
               7*Power(t2,3)) + 
            Power(t1,2)*t2*(-1 + (-12 + 5*s - 8*s2)*t2 + 
               (-5*s + 8*(-6 + s2))*Power(t2,2) + 5*Power(t2,3)) + 
            t1*Power(t2,2)*(30 - (41 + 7*s)*t2 + 
               (31 + 7*s)*Power(t2,2) + s2*(-4 + 9*t2 - 5*Power(t2,2))) + 
            Power(t2,3)*(5 + (10 + 3*s)*t2 - 3*(1 + s)*Power(t2,2) + 
               s2*(4 - 7*t2 + 3*Power(t2,2)))))*R1(t2))/
     ((-1 + t1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       Power(t1 - t2,2)*Power(-1 + t2,2)*t2) - 
    (16*(2*Power(t1,4)*(-3 + t2) + 
         Power(s1,3)*(6 - 11*t1 - 3*s2*(-1 + t2) + 6*t2 + 3*t1*t2) + 
         Power(t1,3)*(24 + s*(-1 + t2) - 2*s2*(-1 + t2) + 7*t2 - 
            3*Power(t2,2)) + Power(s1,2)*
          (-16 + 39*t1 - 28*Power(t1,2) + s*(-3 + t1 - 6*t2)*(-1 + t2) - 
            s2*(-8 + 8*t1 - 5*t2)*(-1 + t2) - 17*t2 + 17*t1*t2 + 
            8*Power(t1,2)*t2 - 10*Power(t2,2) - 4*t1*Power(t2,2) - 
            Power(t2,3)) + 2*(-3 + (19 + 9*s + s2)*t2 + 
            (35 - 8*s - 4*s2)*Power(t2,2) + (5 - s + 3*s2)*Power(t2,3)) + 
         Power(t1,2)*(-17 + (23 + 5*s)*t2 - (3 + 5*s)*Power(t2,2) + 
            Power(t2,3) + s2*(-5 + t2 + 4*Power(t2,2))) + 
         t1*(13 - 105*t2 - 57*Power(t2,2) + 5*Power(t2,3) + 
            s2*(2 + 5*t2 - 4*Power(t2,2) - 3*Power(t2,3)) + 
            s*(-2 - 9*t2 + 8*Power(t2,2) + 3*Power(t2,3))) + 
         s1*(11 - 35*t1 + 57*Power(t1,2) - 23*Power(t1,3) - 21*t2 + 
            22*t1*t2 + 18*Power(t1,2)*t2 + 7*Power(t1,3)*t2 - 
            15*Power(t2,2) - 11*t1*Power(t2,2) - 
            7*Power(t1,2)*Power(t2,2) + 9*Power(t2,3) - 
            s2*(-1 + t2)*(4 - 13*t1 + 7*Power(t1,2) + 9*t2 - 9*t1*t2 + 
               3*Power(t2,2)) + 
            s*(-1 + t2)*(4 + 2*Power(t1,2) + 21*t2 + 3*Power(t2,2) - 
               t1*(3 + 11*t2))))*R2(1 - s1 - t1 + t2))/
     ((-1 + t1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       (-1 + s1 + t1 - t2)*Power(-1 + t2,2)) + 
    (8*(-4*Power(s1,6)*t1 - (-1 + s)*Power(t1,5)*(-1 + Power(t2,2)) + 
         4*(-1 + t2)*t2*(-3 + (-3 + 11*s + 9*s2)*t2 + 
            (25 + 2*s - 2*s2)*Power(t2,2) + (5 + s - s2)*Power(t2,3)) + 
         Power(t1,3)*(-5 + (6 + s - 9*s2)*t2 + 
            (17 - 11*s + 11*s2)*Power(t2,2) + 
            2*(3 + 5*s - s2)*Power(t2,3)) + 
         Power(s1,5)*(2*Power(t1,2)*(-9 + t2) - 
            s2*(3 + 2*t1)*(-1 + t2) + 4*t2 + 
            t1*(1 + 2*s*(-1 + t2) + 19*t2)) + 
         Power(t1,4)*(6 + (-8 + s2)*t2 - 11*Power(t2,2) - 
            (-1 + s2)*Power(t2,3) + s*(-5 + 4*t2 + Power(t2,3))) - 
         4*t1*t2*(t2*(-10 + 19*t2 + 14*Power(t2,2) + Power(t2,3)) + 
            s*(-1 + 3*t2 - 4*Power(t2,2) + 2*Power(t2,3)) + 
            s2*(1 - 7*t2 + 4*Power(t2,2) + 2*Power(t2,3))) - 
         2*Power(t1,2)*t2*(11 - 18*t2 - 19*Power(t2,2) + 2*Power(t2,3) - 
            s2*(8 - 13*t2 + Power(t2,2) + 4*Power(t2,3)) + 
            s*(-12 + 7*t2 + Power(t2,2) + 4*Power(t2,3))) + 
         Power(s1,4)*(-3 + 6*Power(t1,3)*(-5 + t2) + 2*t2 + 5*s*t2 - 
            19*Power(t2,2) - 5*s*Power(t2,2) + 
            Power(t1,2)*(8 + 6*s*(-1 + t2) + 67*t2 - 3*Power(t2,2)) + 
            s2*(-1 + t2)*(-6*Power(t1,2) + 5*t1*(-2 + t2) + 
               5*(1 + t2)) + t1*
             (10 + 26*t2 - 28*Power(t2,2) + s*(3 + 3*t2 - 6*Power(t2,2)))\
) + Power(s1,3)*(5 - (12 + 11*s + 13*s2)*t2 + 
            (-11 + 5*s + 19*s2)*Power(t2,2) + 
            (26 + 6*s - 6*s2)*Power(t2,3) + Power(t1,4)*(-22 + 6*t2) + 
            Power(t1,3)*(16 + 6*s*(-1 + t2) - 6*s2*(-1 + t2) + 87*t2 - 
               7*Power(t2,2)) + 
            Power(t1,2)*(25 + 32*t2 - 73*Power(t2,2) + 
               s*(8 + 9*t2 - 17*Power(t2,2)) + 
               s2*(14 - 27*t2 + 13*Power(t2,2))) + 
            t1*(-11 - 28*t2 - 128*Power(t2,2) + 15*Power(t2,3) + 
               s2*(-25 + 17*t2 + 11*Power(t2,2) - 3*Power(t2,3)) + 
               s*(5 + 3*t2 - 11*Power(t2,2) + 3*Power(t2,3)))) + 
         s1*(Power(t1,5)*(3 + 10*t2 - Power(t2,2)) - 
            4*t1*(3 - (5 + 7*s + 13*s2)*t2 + 
               (s + 19*(-2 + s2))*Power(t2,2) + 
               (-2 + 4*s - 4*s2)*Power(t2,3) + 
               2*(5 + s - s2)*Power(t2,4)) + 
            Power(t1,4)*(-1 + 3*s2*Power(-1 + t2,2) - 8*t2 - 
               11*Power(t2,2) + s*(4 + 3*t2 - 7*Power(t2,2))) + 
            4*t2*(-9 + 17*t2 - 10*Power(t2,2) - 23*Power(t2,3) + 
               Power(t2,4) + s2*(-2 + Power(t2,2) + Power(t2,3)) - 
               s*(-2 - 12*t2 + 13*Power(t2,2) + Power(t2,3))) + 
            Power(t1,3)*(7 - 36*t2 - 84*Power(t2,2) + 9*Power(t2,3) - 
               s2*(11 - 19*t2 + 3*Power(t2,2) + 5*Power(t2,3)) + 
               s*(-9 + t2 + 3*Power(t2,2) + 5*Power(t2,3))) + 
            Power(t1,2)*(15 - 64*t2 + 87*Power(t2,2) + 110*Power(t2,3) + 
               4*Power(t2,4) + 
               s2*(4 - 23*t2 + 25*Power(t2,2) - 6*Power(t2,3)) + 
               s*(-4 - 17*t2 - Power(t2,2) + 22*Power(t2,3)))) + 
         Power(s1,2)*(2*Power(t1,5)*(-3 + t2) + 
            Power(t1,4)*(12 + 2*s*(-1 + t2) - 2*s2*(-1 + t2) + 49*t2 - 
               5*Power(t2,2)) + 
            Power(t1,3)*(15 + 2*t2 - 57*Power(t2,2) + 
               s*(8 + 9*t2 - 17*Power(t2,2)) + 
               s2*(10 - 21*t2 + 11*Power(t2,2))) + 
            2*t2*(7 + (8 - 5*s)*t2 + (31 + 7*s)*Power(t2,2) - 
               2*(3 + s)*Power(t2,3) + 
               s2*(20 - 15*t2 - 7*Power(t2,2) + 2*Power(t2,3))) + 
            Power(t1,2)*(-11 - 62*t2 - 178*Power(t2,2) + 
               27*Power(t2,3) + 
               s2*(-31 + 35*t2 + 3*Power(t2,2) - 7*Power(t2,3)) + 
               s*(1 - 5*t2 - 3*Power(t2,2) + 7*Power(t2,3))) + 
            t1*(21 - 86*t2 + 47*Power(t2,2) + 158*Power(t2,3) - 
               4*Power(t2,4) + 
               s2*(8 - 23*t2 + 29*Power(t2,2) - 14*Power(t2,3)) + 
               s*(-8 - 33*t2 + 19*Power(t2,2) + 22*Power(t2,3)))))*
       T2(t2,1 - s1 - t1 + t2))/
     ((-1 + t1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)) + 
    (8*((-1 + s)*Power(t1,4)*(-1 + Power(t2,2)) + 
         Power(t1,2)*(5 + (4 - 15*s + s2)*t2 + 
            (-72 + 8*s + s2)*Power(t2,2) + 2*(7 + 2*s)*Power(t2,3) + 
            (1 + 3*s - 2*s2)*Power(t2,4)) + 
         2*Power(s1,3)*t1*(2 + s2 - 2*Power(t1,2) + 4*t2 - 
            s2*Power(t2,2) + t1*(-5 + Power(t2,2))) + 
         Power(t1,3)*(-6 - (-16 + s2)*t2 - 7*Power(t2,2) + 
            (1 + s2)*Power(t2,3) + s*(5 - 2*Power(t2,2) - 3*Power(t2,3))) \
- t2*(10 + (7 - 2*s)*t2 + (12 + 5*s)*Power(t2,2) - 
            3*(-13 + s)*Power(t2,3) + 4*Power(t2,4) + 
            s2*(-4 - 10*t2 + 9*Power(t2,2) + 5*Power(t2,3))) - 
         t1*t2*(-2 - 24*t2 - 94*Power(t2,2) + 3*Power(t2,3) + 
            Power(t2,4) - s2*(-8 + 4*t2 + 3*Power(t2,2) + Power(t2,4)) + 
            s*(2 - 15*t2 + 10*Power(t2,2) + 2*Power(t2,3) + Power(t2,4))) \
- Power(s1,2)*(4*Power(t1,4) + 
            Power(t1,3)*(17 - 2*s*(-1 + t2) - 9*t2 - 4*Power(t2,2)) + 
            t2*(4 + 8*t2 + s2*(2 - 3*t2 + Power(t2,2))) + 
            Power(t1,2)*(2*s*Power(-1 + t2,2) + 
               s2*(-5 + t2 + 4*Power(t2,2)) + 
               4*(-3 - 16*t2 + Power(t2,3))) + 
            t1*(14 + 2*s*(-1 + t2)*t2 + 29*Power(t2,2) + 9*Power(t2,3) - 
               2*s2*(1 - 3*t2 + Power(t2,2) + Power(t2,3)))) + 
         s1*(Power(t1,4)*(-13 + 2*s*(-1 + t2) + 8*t2 + Power(t2,2)) + 
            t2*(14 + (7 - 2*s)*t2 + (32 + 5*s)*Power(t2,2) + 
               (7 - 3*s)*Power(t2,3) + 
               s2*(-2 - 5*t2 + 6*Power(t2,2) + Power(t2,3))) - 
            Power(t1,3)*(s2*(-5 + 4*t2 + Power(t2,2)) + 
               s*(3 - 7*t2 + 4*Power(t2,2)) + 
               2*(-3 - 32*t2 + 8*Power(t2,2) + Power(t2,3))) + 
            Power(t1,2)*(-15 - 6*t2 - 92*Power(t2,2) + Power(t2,4) + 
               s*(2 + 11*t2 - 15*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(3 - 14*t2 + 9*Power(t2,2) + 2*Power(t2,3))) + 
            t1*(10 + 6*t2 - (56 + 13*s)*Power(t2,2) + 
               (36 + 13*s)*Power(t2,3) + 8*Power(t2,4) - 
               s2*(4 + 2*t2 - 9*Power(t2,2) + 2*Power(t2,3) + Power(t2,4))\
)))*T3(t2,t1))/((-1 + t1)*(t1 - t2)*(-1 + s1 + t1 - t2)*(s1*t1 - t2)*
       Power(-1 + t2,2)) + (8*(-5 - 4*Power(s1,3)*t1*(3 + t1) + 38*t2 - 
         5*s*t2 - 5*s2*t2 + 31*Power(t2,2) + 5*s*Power(t2,2) + 
         5*s2*Power(t2,2) + 8*Power(t2,3) + 
         (-1 + s)*Power(t1,3)*(-1 + Power(t2,2)) + 
         Power(s1,2)*(-4*Power(t1,3) + 12*t2 + 
            Power(t1,2)*(-15 + 2*s*(-1 + t2) + 7*t2) + 
            t1*(43 + 6*s*(-1 + t2) + 21*t2) + 3*s2*(-1 + t1 + t2 - t1*t2)\
) - Power(t1,2)*(7 + (-10 + s2)*t2 - 2*Power(t2,2) - 
            (-1 + s2)*Power(t2,3) + 
            s*(-6 + 2*t2 + 3*Power(t2,2) + Power(t2,3))) + 
         t1*(11 + 6*(-8 + s2)*t2 - (8 + 5*s2)*Power(t2,2) - 
            (-1 + s2)*Power(t2,3) + 
            s*(-5 - t2 + 5*Power(t2,2) + Power(t2,3))) - 
         s1*(-3 + 46*t2 - 3*s*t2 + 17*Power(t2,2) + 3*s*Power(t2,2) + 
            s2*(-1 + t1)*(-1 + t2)*(-5 + t1 - 3*t2 + t1*t2) - 
            Power(t1,3)*(-5 + 2*s*(-1 + t2) + Power(t2,2)) + 
            Power(t1,2)*(-41 - 3*s*(-1 + t2) - 8*t2 + Power(t2,2)) + 
            t1*(39 + 10*t2 + 15*Power(t2,2) + 
               s*(-13 + 8*t2 + 5*Power(t2,2)))))*T4(t1))/
     ((-1 + t1)*(-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)) - 
    (8*(-5 - 4*Power(s1,4)*t1 + 25*t2 + 7*s*t2 - 5*s2*t2 + 
         17*Power(t2,2) - 4*s*Power(t2,2) + 4*s2*Power(t2,2) + 
         15*Power(t2,3) - 3*s*Power(t2,3) + s2*Power(t2,3) - 
         4*Power(t2,4) + (-1 + s)*Power(t1,3)*(-1 + Power(t2,2)) + 
         Power(s1,3)*(-8*Power(t1,2) - 3*s2*(-1 + t2) + 4*t2 + 
            t1*(-3 + 2*s*(-1 + t2) + 15*t2)) - 
         Power(t1,2)*(7 + (-5 + s2)*t2 + 2*Power(t2,2) - s2*Power(t2,3) + 
            s*(-6 + t2 + 3*Power(t2,2) + 2*Power(t2,3))) + 
         t1*(11 + 6*(-5 + s2)*t2 - 2*(7 + 2*s2)*Power(t2,2) - 
            (-8 + s2)*Power(t2,3) - (-1 + s2)*Power(t2,4) + 
            s*(-5 - 6*t2 + 7*Power(t2,2) + 3*Power(t2,3) + Power(t2,4))) + 
         Power(s1,2)*(-3 - 4*Power(t1,3) + 6*t2 + 5*s*t2 - 
            15*Power(t2,2) - 5*s*Power(t2,2) + 
            Power(t1,2)*(-1 + t2)*(24 + 4*s + t2) + 
            t1*(31 + s + 16*t2 + s*t2 - 15*Power(t2,2) - 
               2*s*Power(t2,2)) - s2*(-1 + t2)*(-8 - 6*t2 + t1*(8 + t2))) \
- s1*(-8 + 33*t2 + 12*s*t2 + 14*Power(t2,2) - 7*s*Power(t2,2) - 
            15*Power(t2,3) - 5*s*Power(t2,3) - 
            Power(t1,3)*(-1 + 2*s*(-1 + t2) + 4*t2 + Power(t2,2)) + 
            Power(t1,2)*(-30 - 9*t2 + 10*Power(t2,2) + Power(t2,3) + 
               s*(4 - 5*t2 + Power(t2,2))) + 
            s2*(-1 + t2)*(5 + 9*t2 + 3*Power(t2,2) + 
               Power(t1,2)*(1 + t2) - 2*t1*(3 + 5*t2 + Power(t2,2))) + 
            t1*(37 - 20*t2 + 34*Power(t2,2) - 3*Power(t2,3) + 
               s*(-6 - 7*t2 + 12*Power(t2,2) + Power(t2,3)))))*
       T5(1 - s1 - t1 + t2))/
     ((-1 + t1)*(-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)));
   return a;
};
