#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m123_2_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(Power(s,5)*s1*(-14 - Power(t1,2) + (15 + s1)*t2 + 2*Power(t2,2) + 
            t1*(1 + s1 + 3*t2)) + 
         Power(s,4)*(Power(s1,3)*(4 + s2 - 7*t1 - 2*t2) + 
            Power(s1,2)*(45 + s2 - 2*t1 - 3*s2*t1 + Power(t1,2) - 
               43*t2 + 3*s2*t2 - 8*t1*t2 - 6*Power(t2,2)) + 
            2*(-1 + t2 + Power(t2,2)) + 
            s1*(61 + s2*(14 + t1*(5 - 3*t2) - 20*t2) - 119*t2 + 
               66*Power(t2,2) + 3*Power(t2,3) - Power(t1,2)*(1 + 6*t2) + 
               t1*(-30 + 33*t2 + 11*Power(t2,2)))) + 
         s1*(1 + s1 - t2)*(-1 + t2)*
          (8 + 41*t2 + 26*s2*t2 - 4*Power(s2,2)*t2 - 71*Power(t2,2) - 
            35*s2*Power(t2,2) + 6*Power(s2,2)*Power(t2,2) + 
            19*Power(t2,3) + 12*s2*Power(t2,3) + 
            2*Power(s2,2)*Power(t2,3) + 3*Power(t2,4) - 
            3*s2*Power(t2,4) + Power(t1,2)*(8 - 13*t2 + 9*Power(t2,2)) + 
            t1*(-16 + 10*(-6 + s2)*t2 - 7*(-16 + 3*s2)*Power(t2,2) + 
               3*(-12 + s2)*Power(t2,3)) + 
            Power(s1,3)*(-4 + 4*Power(s2,2) + 7*t1 + 4*Power(t1,2) + 
               4*t2 - 7*t1*t2 + s2*(-3 - 8*t1 + 3*t2)) - 
            Power(s1,2)*(-15 + 22*t2 + 2*t1*(-1 + t2)*t2 - 
               7*Power(t2,2) + 2*Power(s2,2)*(1 + t2) + 
               Power(t1,2)*(1 + 3*t2) - 
               s2*(-19 + 24*t2 - 5*Power(t2,2) + t1*(7 + t2))) - 
            s1*(65 - 80*t2 + Power(t2,2) + 14*Power(t2,3) + 
               4*Power(s2,2)*(-1 + t2 + Power(t2,2)) + 
               Power(t1,2)*(11 - 16*t2 + 9*Power(t2,2)) - 
               t1*(108 - 160*t2 + 43*Power(t2,2) + 9*Power(t2,3)) + 
               s2*(26 - 54*t2 + 33*Power(t2,2) - 5*Power(t2,3) - 
                  2*t1*(-5 + 7*t2 + 2*Power(t2,2))))) + 
         Power(s,3)*(Power(s1,4)*(-8 - 2*s2 + 11*t1 + t2) - 
            Power(s1,3)*(46 + 2*Power(s2,2) + Power(t1,2) + 
               t1*(9 - 7*t2) - 2*s2*(2 + 5*t1 - 6*t2) - 47*t2 - 
               8*Power(t2,2)) + 
            2*(2 - 5*t2 + Power(t2,2) + 3*Power(t2,3)) + 
            Power(s1,2)*(Power(s2,2)*(5 - 4*t2) + 
               2*Power(t1,2)*(-2 + 5*t2) + 
               t1*(148 - 104*t2 - 31*Power(t2,2)) + 
               s2*(-60 + 2*t1*(-6 + t2) + 83*t2 + Power(t2,2)) - 
               2*(89 - 125*t2 + 54*Power(t2,2) + 7*Power(t2,3))) - 
            s1*(72 - 300*t2 + 303*Power(t2,2) - 116*Power(t2,3) + 
               3*Power(t2,4) + 
               Power(s2,2)*(-14 + 11*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-28 + 13*t2 + 12*Power(t2,2)) - 
               3*t1*(38 - 98*t2 + 45*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(83 - 150*t2 + 76*Power(t2,2) - 3*Power(t2,3) + 
                  4*t1*(10 - 13*t2 + 3*Power(t2,2))))) + 
         s*(-2*Power(s1,5)*(4 + Power(s2,2) + Power(t1,2) - 
               2*s2*(2 + t1 - 2*t2) - 4*t2) + 
            2*Power(-1 + t2,2)*t2*(-1 + t2 + Power(t2,2)) + 
            Power(s1,4)*(8 + Power(s2,2)*(10 - 8*t2) - 5*t2 + 
               2*Power(t2,2) - 5*Power(t2,3) - 
               2*Power(t1,2)*(-6 + 5*t2) - 
               2*t1*(-8 + 7*t2 + Power(t2,2)) + 
               s2*(-37 + 32*t2 + 5*Power(t2,2) + 2*t1*(-7 + 5*t2))) + 
            Power(s1,3)*(-97 + 107*t2 + 12*Power(t2,2) - 
               29*Power(t2,3) + 7*Power(t2,4) + 
               Power(t1,2)*(-2 + 7*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*(21 - 27*t2 + 8*Power(t2,2)) + 
               t1*(247 - 380*t2 + 103*Power(t2,2) + 30*Power(t2,3)) + 
               s2*(-142 + 303*t2 - 176*Power(t2,2) + 15*Power(t2,3) + 
                  t1*(-38 + 42*t2 - 8*Power(t2,2)))) + 
            Power(s1,2)*(-198 + 597*t2 - 538*Power(t2,2) + 
               142*Power(t2,3) - 4*Power(t2,4) + Power(t2,5) + 
               Power(s2,2)*(20 - 58*t2 + 28*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-5 + 20*t2 - 43*Power(t2,2) + 
                  26*Power(t2,3)) + 
               t1*(365 - 1133*t2 + 1039*Power(t2,2) - 241*Power(t2,3) - 
                  30*Power(t2,4)) + 
               s2*(-140 + 472*t2 - 515*Power(t2,2) + 204*Power(t2,3) - 
                  21*Power(t2,4) + 
                  2*t1*(-24 + 76*t2 - 53*Power(t2,2) + 3*Power(t2,3)))) - 
            s1*(-1 + t2)*(9 + 168*t2 - 279*Power(t2,2) + 
               119*Power(t2,3) - 20*Power(t2,4) + 3*Power(t2,5) + 
               Power(t1,2)*(29 - 61*t2 + 32*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,2)*(4 - 28*t2 + 21*Power(t2,2) + 6*Power(t2,3)) - 
               t1*(4 + 309*t2 - 461*Power(t2,2) + 146*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*(-26 + 156*t2 - 184*Power(t2,2) + 63*Power(t2,3) - 
                  9*Power(t2,4) + 
                  2*t1*(-5 + 38*t2 - 42*Power(t2,2) + 6*Power(t2,3))))) + 
         Power(s,2)*(Power(s1,5)*(4 + s2 - 5*t1) + 
            2*(-1 + 5*t2 - 6*Power(t2,2) - Power(t2,3) + 3*Power(t2,4)) + 
            Power(s1,4)*(23 + 4*Power(s2,2) + 3*Power(t1,2) - 
               2*t1*(-5 + t2) - 27*t2 - 4*Power(t2,2) + 
               s2*(-13 - 11*t1 + 17*t2)) + 
            Power(s1,3)*(89 - 100*t2 + 30*Power(t2,2) + 17*Power(t2,3) + 
               Power(t1,2)*(-3 + 2*t2) + Power(s2,2)*(-11 + 8*t2) - 
               s2*(-80 + t1*(-13 + t2) + 91*t2 + 8*Power(t2,2)) + 
               t1*(-111 + 53*t2 + 33*Power(t2,2))) - 
            Power(s1,2)*(-262 + 554*t2 - 352*Power(t2,2) + 
               86*Power(t2,3) + 6*Power(t2,4) + 
               Power(t1,2)*(7 + 22*t2 - 26*Power(t2,2)) + 
               Power(s2,2)*(33 - 33*t2 + 2*Power(t2,2)) + 
               t1*(450 - 797*t2 + 282*Power(t2,2) + 43*Power(t2,3)) + 
               s2*(-203 + 402*t2 - 228*Power(t2,2) + 15*Power(t2,3) + 
                  t1*(-77 + 88*t2 - 12*Power(t2,2)))) - 
            s1*(-24 + 315*t2 - 531*Power(t2,2) + 324*Power(t2,3) - 
               91*Power(t2,4) + 7*Power(t2,5) + 
               Power(s2,2)*(18 - 49*t2 + 24*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(47 - 90*t2 + 32*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(97 - 564*t2 + 687*Power(t2,2) - 211*Power(t2,3) - 
                  9*Power(t2,4)) + 
               s2*(-95 + 315*t2 - 321*Power(t2,2) + 110*Power(t2,3) - 
                  9*Power(t2,4) + 
                  t1*(-45 + 145*t2 - 116*Power(t2,2) + 18*Power(t2,3)))))))/
     (s*(-1 + s1)*s1*(-1 + t1)*(-s + s1 - t2)*(1 - s + s1 - t2)*(-1 + t2)*
       Power(-1 + s + t2,2)) + 
    (8*(Power(s,6)*((1 + s1)*Power(t1,2) - (-4 + s1)*t1*t2 + 
            3*Power(t2,2)) - 2*(-1 + t2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          (-Power(s1,2) + Power(s1,3) + t2*(-2 + 3*t2) - 
            s1*(-2 + 2*t2 + Power(t2,2))) + 
         Power(s,4)*(1 - 14*t1 + 25*Power(t1,2) - 19*t2 + 12*s2*t2 - 
            16*t1*t2 - 25*Power(t1,2)*t2 + 20*Power(t2,2) + 
            12*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) - 
            47*t1*Power(t2,2) + 2*s2*t1*Power(t2,2) - 56*Power(t2,3) - 
            27*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) + 
            38*t1*Power(t2,3) + 2*s2*t1*Power(t2,3) + 24*Power(t2,4) - 
            2*s2*Power(t2,4) + 
            Power(s1,3)*(Power(s2,2) + 10*Power(t1,2) - 
               10*t1*(-1 + t2) - 3*(-1 + t2)*t2 - s2*(1 + 8*t1 + 2*t2)) \
+ Power(s1,2)*(-3 - Power(s2,2)*(-1 + t2) + 5*t2 - 4*Power(t2,2) + 
               5*Power(t2,3) - 3*Power(t1,2)*(-9 + 4*t2) + 
               t1*(1 - 13*t2 + 9*Power(t2,2)) + 
               s2*(4 - 9*t2 + t1*(-13 + 3*t2))) + 
            s1*(5 - 2*(4 + 6*s2 + Power(s2,2))*t2 + 
               (3 + 41*s2 - Power(s2,2))*Power(t2,2) + 
               (-23 + 4*s2)*Power(t2,3) - 2*Power(t2,4) + 
               Power(t1,2)*(32 - 24*t2 + 3*Power(t2,2)) - 
               t1*(7 - 8*t2 + 38*Power(t2,2) + Power(t2,3) + 
                  s2*(12 - 7*t2 + Power(t2,2))))) + 
         Power(s,5)*(Power(t1,2)*
             (-8 - 5*Power(s1,2) + 3*t2 + s1*(-10 + 3*t2)) + 
            t2*(6 - 7*t2 + 16*Power(t2,2) + 
               Power(s1,2)*(-1 + s2 + t2) + 
               s1*(1 + 4*s2 - 6*t2 - Power(t2,2)) - 
               s2*(2 + 8*t2 + Power(t2,2))) + 
            t1*(2 - 9*t2 + (19 + s2)*Power(t2,2) + 
               Power(s1,2)*(-3 + 2*s2 + 5*t2) + 
               s1*(s2*(2 + t2) - 2*(-2 + 4*t2 + Power(t2,2))))) - 
         s*(8 - 16*t1 + 8*Power(t1,2) - 8*t2 + 48*t1*t2 - 
            40*Power(t1,2)*t2 + 13*Power(t2,2) - 68*s2*Power(t2,2) + 
            8*Power(s2,2)*Power(t2,2) - 10*t1*Power(t2,2) + 
            20*s2*t1*Power(t2,2) + 53*Power(t1,2)*Power(t2,2) - 
            57*Power(t2,3) + 118*s2*Power(t2,3) - 
            24*Power(s2,2)*Power(t2,3) - 47*t1*Power(t2,3) - 
            30*s2*t1*Power(t2,3) - 20*Power(t1,2)*Power(t2,3) + 
            73*Power(t2,4) - 21*s2*Power(t2,4) + 
            17*Power(s2,2)*Power(t2,4) - t1*Power(t2,4) + 
            8*s2*t1*Power(t2,4) - 35*Power(t2,5) - 31*s2*Power(t2,5) + 
            30*t1*Power(t2,5) + 6*Power(t2,6) + 2*s2*Power(t2,6) - 
            4*t1*Power(t2,6) + 
            Power(s1,6)*(s2 - t1)*(-1 + s2 - t1 + t2) - 
            Power(s1,5)*(-1 + t2)*
             (1 + 3*Power(s2,2) + 3*Power(t1,2) + t1*(5 - 3*t2) - t2 + 
               s2*(-6 - 6*t1 + 4*t2)) + 
            s1*(-28 - 16*(-3 - 7*s2 + Power(s2,2))*t2 + 
               2*(12 - 87*s2 + 26*Power(s2,2))*Power(t2,2) - 
               (87 + 14*s2 + 38*Power(s2,2))*Power(t2,3) + 
               (50 + 80*s2 - 3*Power(s2,2))*Power(t2,4) + 
               (-7 - 4*s2 + Power(s2,2))*Power(t2,5) + 
               Power(t1,2)*(4 + 16*t2 - 27*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(24 - 16*(11 + s2)*t2 + (251 - 10*s2)*Power(t2,2) + 
                  (-42 + 38*s2)*Power(t2,3) - (63 + 4*s2)*Power(t2,4) + 
                  6*Power(t2,5))) + 
            Power(s1,3)*(3*Power(-1 + t2,2)*t2*(1 + t2) + 
               Power(t1,2)*(-1 + t2 + 5*Power(t2,2) - Power(t2,3)) + 
               2*Power(s2,2)*(2 - 5*t2 + 4*Power(t2,2) + Power(t2,3)) + 
               t1*(1 - 46*t2 + 66*Power(t2,2) - 22*Power(t2,3) + 
                  Power(t2,4)) + 
               2*s2*(-5 + 23*t2 - 21*Power(t2,2) + 5*Power(t2,3) - 
                  2*Power(t2,4) + 
                  t1*(1 + t2 - 7*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,2)*(-(Power(-1 + t2,2)*
                  (-3 + 17*t2 - Power(t2,2) + Power(t2,3))) + 
               Power(t1,2)*(-5 - 8*t2 + 14*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,2)*(8 - 32*t2 + 28*Power(t2,2) + 
                  2*Power(t2,3) - 3*Power(t2,4)) + 
               t1*(58 - 93*t2 + 14*Power(t2,2) + 18*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s2*(-44 + 66*t2 + 4*Power(t2,2) - 28*Power(t2,3) + 
                  Power(t2,4) + Power(t2,5) - 
                  2*t1*(2 - 19*t2 + 19*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,4)*(-3*Power(-1 + t2,2)*(1 + t2) + 
               Power(s2,2)*(3 - 10*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(6 - 14*t2 + 3*Power(t2,2)) - 
               3*t1*(-9 + 16*t2 - 8*Power(t2,2) + Power(t2,3)) + 
               s2*(-2*t1*(5 - 13*t2 + 3*Power(t2,2)) + 
                  3*(-5 + 9*t2 - 6*Power(t2,2) + 2*Power(t2,3))))) - 
         Power(s,3)*(6 - 36*t1 + 38*Power(t1,2) - 10*t2 + 24*s2*t2 - 
            37*t1*t2 - 71*Power(t1,2)*t2 + 45*Power(t2,2) - 
            56*s2*Power(t2,2) + 6*Power(s2,2)*Power(t2,2) + 
            48*t1*Power(t2,2) + 17*s2*t1*Power(t2,2) + 
            19*Power(t1,2)*Power(t2,2) - 101*Power(t2,3) - 
            69*s2*Power(t2,3) + 102*t1*Power(t2,3) - 
            5*s2*t1*Power(t2,3) + 4*Power(t1,2)*Power(t2,3) + 
            83*Power(t2,4) + 33*s2*Power(t2,4) - 
            2*Power(s2,2)*Power(t2,4) - 37*t1*Power(t2,4) - 
            s2*t1*Power(t2,4) - 13*Power(t2,5) + s2*Power(t2,5) + 
            Power(s1,4)*(3*Power(s2,2) + 10*Power(t1,2) - 
               3*s2*(1 + 4*t1) - 3*(-1 + t2)*t2 - 2*t1*(-6 + 5*t2)) + 
            Power(s1,3)*(-5 + Power(t1,2)*(31 - 18*t2) - 
               5*Power(s2,2)*(-1 + t2) + 11*t2 - 13*Power(t2,2) + 
               7*Power(t2,3) + t1*(19 - 42*t2 + 15*Power(t2,2)) + 
               s2*(2 + 4*t2 - 4*Power(t2,2) + t1*(-26 + 15*t2))) + 
            Power(s1,2)*(-1 + 22*t2 - 31*Power(t2,2) + Power(t2,3) - 
               5*Power(t2,4) - Power(s2,2)*(-6 + 10*t2 + Power(t2,2)) + 
               Power(t1,2)*(42 - 49*t2 + 9*Power(t2,2)) + 
               t1*(11 - 56*t2 + 8*Power(t2,2) - 5*Power(t2,3)) + 
               s2*(2 - 15*t2 + 42*Power(t2,2) + 9*Power(t2,3) + 
                  t1*(-27 + 33*t2 - 5*Power(t2,2)))) + 
            s1*(7 - 2*(7 - 13*s2 + 6*Power(s2,2))*t2 + 
               (33 + 86*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (-49 - 76*s2 + 5*Power(s2,2))*Power(t2,3) - 
               6*(-3 + s2)*Power(t2,4) + Power(t2,5) - 
               Power(t1,2)*(-42 + 57*t2 - 9*Power(t2,2) + Power(t2,3)) + 
               t1*(25 - 85*t2 - 57*Power(t2,2) + 61*Power(t2,3) + 
                  s2*(-24 + 38*t2 - 6*Power(t2,2) + 3*Power(t2,3))))) + 
         Power(s,2)*(12 - 40*t1 + 28*Power(t1,2) + 5*t2 + 16*s2*t2 + 
            20*t1*t2 - 85*Power(t1,2)*t2 + 24*Power(t2,2) - 
            130*s2*Power(t2,2) + 12*Power(s2,2)*Power(t2,2) + 
            116*t1*Power(t2,2) + 28*s2*t1*Power(t2,2) + 
            60*Power(t1,2)*Power(t2,2) - 115*Power(t2,3) + 
            43*s2*Power(t2,3) - 15*Power(s2,2)*Power(t2,3) - 
            21*t1*Power(t2,3) - 27*s2*t1*Power(t2,3) - 
            2*Power(t1,2)*Power(t2,3) + 112*Power(t2,4) + 
            88*s2*Power(t2,4) - Power(s2,2)*Power(t2,4) - 
            94*t1*Power(t2,4) + 3*s2*t1*Power(t2,4) - 
            2*Power(t1,2)*Power(t2,4) - 40*Power(t2,5) - 
            16*s2*Power(t2,5) + Power(s2,2)*Power(t2,5) + 
            18*t1*Power(t2,5) + 2*Power(t2,6) + 
            Power(s1,5)*(3*Power(s2,2) + 6*t1 + 5*Power(t1,2) + t2 - 
               5*t1*t2 - Power(t2,2) + s2*(-3 - 8*t1 + 2*t2)) + 
            Power(s1,4)*(-1 - 7*Power(s2,2)*(-1 + t2) + 3*t2 - 
               5*Power(t2,2) + 3*Power(t2,3) - 
               4*Power(t1,2)*(-4 + 3*t2) + 
               t1*(19 - 33*t2 + 11*Power(t2,2)) + 
               s2*(-8 + 19*t2 - 8*Power(t2,2) + t1*(-21 + 17*t2))) + 
            Power(s1,3)*(-9 + 31*t2 - 24*Power(t2,2) + 5*Power(t2,3) - 
               3*Power(t2,4) + Power(s2,2)*(7 - 16*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(22 - 40*t2 + 9*Power(t2,2)) + 
               t1*(39 - 103*t2 + 51*Power(t2,2) - 7*Power(t2,3)) + 
               s2*(-9 + 26*t2 - 9*Power(t2,2) + 12*Power(t2,3) + 
                  t1*(-21 + 48*t2 - 11*Power(t2,2)))) + 
            Power(s1,2)*(6 - 12*t2 - 3*Power(t2,2) + 9*Power(t2,3) - 
               Power(t2,4) + Power(t2,5) + 
               Power(t1,2)*(18 - 37*t2 + 14*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(s2,2)*(12 - 29*t2 + 10*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(44 - 81*t2 + 39*Power(t2,2) + Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(-34 + 41*t2 + 32*Power(t2,2) - 35*Power(t2,3) - 
                  8*Power(t2,4) + 
                  t1*(-20 + 43*t2 - 18*Power(t2,2) + 3*Power(t2,3)))) + 
            s1*(-21 + (22 + 116*s2 - 24*Power(s2,2))*t2 + 
               (64 - 27*s2 + 37*Power(s2,2))*Power(t2,2) - 
               (107 + 154*s2)*Power(t2,3) + 
               (44 + 44*s2 - 5*Power(s2,2))*Power(t2,4) + 
               2*(-1 + s2)*Power(t2,5) + 
               Power(t1,2)*(21 - 34*t2 + 17*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(60 - 256*t2 + 135*Power(t2,2) + 115*Power(t2,3) - 
                  35*Power(t2,4) - 
                  s2*(16 - 40*t2 + 43*Power(t2,2) - 4*Power(t2,3) + 
                     Power(t2,4))))))*B1(s,1 - s + s1 - t2,s1))/
     (s*(-1 + s1)*(-1 + t1)*(-1 + t2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) + 
    (8*(2*(-1 + s1)*Power(s1,2)*(2 + Power(s1,2) + s1*(-1 + t2) - 3*t2)*
          Power(-1 + t2,2)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) + 
         2*Power(s,7)*s1*t2*(2 + t1*(-3 + t2) - 3*t2 + Power(t2,2) + 
            2*s1*(-2 + t1 + 2*t2)) - 
         Power(s,6)*(2*t2*(-1 + t2 + Power(t2,2)) + 
            2*Power(s1,3)*(4 + Power(t1,2) - 2*(7 + 2*s2)*t2 + 
               9*Power(t2,2) + t1*(-2 + 8*t2)) + 
            s1*t2*(34 - 61*t2 + 31*Power(t2,2) - 6*Power(t2,3) + 
               t1*(-29 + 33*t2 - 6*Power(t2,2)) + 
               2*s2*(-4 + 3*t2 + Power(t2,2))) + 
            Power(s1,2)*(-4 - 2*Power(t1,2)*(-1 + t2) + 
               (-44 + 6*s2)*t2 + (59 - 2*s2)*Power(t2,2) - 
               19*Power(t2,3) + t1*(6 + (-9 + 4*s2)*t2 + Power(t2,2)))) + 
         Power(s,5)*(2*t2*(-5 + 8*t2 + 2*Power(t2,2) - 3*Power(t2,3)) + 
            s1*(2 + (109 - 44*s2 - 51*t1)*t2 + 
               (-268 + 63*s2 + 107*t1)*Power(t2,2) - 
               (-169 + s2 + 62*t1)*Power(t2,3) + 
               (-50 - 6*s2 + 6*t1)*Power(t2,4) + 6*Power(t2,5)) + 
            2*Power(s1,4)*(4*Power(t1,2) + s2*(4 - 2*t1 - 9*t2) + 
               6*Power(-1 + t2,2) + t1*(-4 + 11*t2)) + 
            Power(s1,3)*(46 - 157*t2 - 8*Power(s2,2)*t2 + 
               150*Power(t2,2) - 33*Power(t2,3) + 
               s2*(3 + 4*t1 + t2)*(-2 + 5*t2) - 
               2*Power(t1,2)*(-9 + 8*t2) + 
               t1*(5 - 21*t2 - 4*Power(t2,2))) + 
            Power(s1,2)*(-8*Power(s2,2)*(-2 + t2)*t2 + 
               2*Power(t1,2)*(7 - 6*t2 + 2*Power(t2,2)) + 
               t1*(19 - 90*t2 + 55*Power(t2,2) - 18*Power(t2,3)) + 
               s2*(8 - (27 + 4*t1)*t2 - 4*(-10 + t1)*Power(t2,2) + 
                  5*Power(t2,3)) + 
               2*(-15 - 35*t2 + 63*Power(t2,2) - 49*Power(t2,3) + 
                  7*Power(t2,4)))) - 
         Power(s,4)*(2*t2*(-10 + 22*t2 - 5*Power(t2,2) - 
               9*Power(t2,3) + 3*Power(t2,4)) + 
            s1*(8 - 2*(-90 + 48*s2 + 17*t1)*t2 + 
               2*(-293 + 100*s2 + 65*t1)*Power(t2,2) + 
               (492 - 72*s2 - 149*t1)*Power(t2,3) + 
               (-161 - 26*s2 + 53*t1)*Power(t2,4) + 
               (31 + 6*s2 - 2*t1)*Power(t2,5) - 2*Power(t2,6)) + 
            2*Power(s1,5)*(2 + Power(s2,2) + 6*Power(t1,2) + 
               s2*(3 - 6*t1 - 5*t2) - 2*t2 + Power(t2,2) + 
               t1*(-1 + 5*t2)) + 
            Power(s1,4)*(46 + Power(t1,2)*(38 - 42*t2) - 164*t2 + 
               107*Power(t2,2) + Power(t2,3) - 
               2*Power(s2,2)*(-5 + 8*t2) - 
               s2*(13 + t1*(36 - 48*t2) - 34*t2 + Power(t2,2)) + 
               t1*(33 - 77*t2 + 14*Power(t2,2))) + 
            Power(s1,2)*(-81 + 106*t2 + 22*Power(t2,2) - 
               56*Power(t2,3) + 68*Power(t2,4) - 3*Power(t2,5) + 
               2*Power(s2,2)*t2*(38 - 46*t2 + 9*Power(t2,2)) - 
               2*Power(t1,2)*
                (-20 + 15*t2 - 4*Power(t2,2) + Power(t2,3)) - 
               s2*(-36 + 11*(21 + 8*t1)*t2 - 
                  2*(203 + 36*t1)*Power(t2,2) + 
                  (139 + 4*t1)*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(4 - 111*t2 + 72*Power(t2,2) - 56*Power(t2,3) + 
                  17*Power(t2,4))) + 
            Power(s1,3)*(103 - 300*t2 + 361*Power(t2,2) - 
               195*Power(t2,3) + 19*Power(t2,4) + 
               2*Power(s2,2)*(-8 + 9*t2) + 
               Power(t1,2)*(42 - 54*t2 + 20*Power(t2,2)) + 
               t1*(37 - 196*t2 + 98*Power(t2,2) - 33*Power(t2,3)) + 
               s2*(23 + 19*t2 + 45*Power(t2,2) + 9*Power(t2,3) - 
                  4*t1*(3 - 5*t2 + 5*Power(t2,2))))) + 
         Power(s,3)*(2*Power(s1,6)*(s2 - t1)*(-2 + 2*s2 - 4*t1 + t2) - 
            2*t2*(10 - 28*t2 + 17*Power(t2,2) + 8*Power(t2,3) - 
               8*Power(t2,4) + Power(t2,5)) - 
            s1*(-1 + t2)*(12 + (178 - 104*s2 + 4*t1)*t2 + 
               2*(-257 + 89*s2 + 26*t1)*Power(t2,2) + 
               (247 - 16*s2 - 79*t1)*Power(t2,3) + 
               (-41 - 29*s2 + 22*t1)*Power(t2,4) + 
               2*(3 + s2)*Power(t2,5)) + 
            Power(s1,2)*(-107 + 401*t2 - 396*Power(t2,2) + 
               182*Power(t2,3) - 53*Power(t2,4) - 27*Power(t2,5) + 
               2*Power(t1,2)*
                (30 - 26*t2 + 2*Power(t2,2) + Power(t2,3)) - 
               2*Power(s2,2)*t2*
                (-66 + 118*t2 - 63*Power(t2,2) + 6*Power(t2,3)) + 
               t1*(-50 + 81*t2 - 180*Power(t2,2) + 113*Power(t2,3) + 
                  2*Power(t2,4) - 4*Power(t2,5)) + 
               s2*(60 - (451 + 208*t1)*t2 + 
                  (991 + 292*t1)*Power(t2,2) - 
                  28*(25 + 4*t1)*Power(t2,3) + 
                  (137 + 4*t1)*Power(t2,4) + Power(t2,5))) + 
            Power(s1,5)*(50 + Power(t1,2)*(36 - 50*t2) - 97*t2 + 
               28*Power(t2,2) + 19*Power(t2,3) - 
               6*Power(s2,2)*(-2 + 3*t2) + 
               t1*(53 - 117*t2 + 48*Power(t2,2)) + 
               s2*(-30 + 85*t2 - 39*Power(t2,2) + t1*(-44 + 64*t2))) + 
            Power(s1,4)*(46 - 178*t2 + 238*Power(t2,2) - 
               84*Power(t2,3) - 22*Power(t2,4) + 
               2*Power(s2,2)*(2 - 7*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(32 - 66*t2 + 36*Power(t2,2)) + 
               t1*(104 - 219*t2 + 152*Power(t2,2) - 49*Power(t2,3)) + 
               s2*(-73 + 80*t2 - 15*Power(t2,2) + 20*Power(t2,3) - 
                  4*t1*(5 - 14*t2 + 8*Power(t2,2)))) + 
            Power(s1,3)*(47 - 150*t2 + 146*Power(t2,2) - 
               148*Power(t2,3) + 111*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,2)*(12 - 20*t2 + 4*Power(t2,2) - 
                  6*Power(t2,3)) + 
               2*Power(s2,2)*
                (-30 + 88*t2 - 66*Power(t2,2) + 7*Power(t2,3)) + 
               t1*(19 - 33*t2 + 92*Power(t2,2) - 37*Power(t2,3) + 
                  23*Power(t2,4)) + 
               s2*(143 - 432*t2 + 407*Power(t2,2) - 178*Power(t2,3) - 
                  4*Power(t2,4) - 
                  4*t1*(-11 + 37*t2 - 30*Power(t2,2) + Power(t2,3))))) + 
         s*(-1 + t2)*(-2*Power(s1,7)*(s2 - t1)*
             (-7 + 3*s2 - 3*t1 + 7*t2) - 
            2*Power(-1 + t2,2)*t2*(-1 + t2 + Power(t2,2)) - 
            s1*Power(-1 + t2,2)*
             (2 + (23 - 12*s2 + 5*t1)*t2 + 
               (-47 + 11*s2 + 2*t1)*Power(t2,2) + 
               2*(-7 + 4*s2 - 2*t1)*Power(t2,3)) + 
            Power(s1,6)*(Power(s2,2)*(22 - 8*t2) - 
               6*Power(-1 + t2,2)*(1 + t2) + 2*Power(t1,2)*(5 + 2*t2) + 
               t1*(33 - 20*t2 - 13*Power(t2,2)) + 
               s2*(-33 + 4*t1*(-8 + t2) + 20*t2 + 13*Power(t2,2))) + 
            Power(s1,2)*(Power(t1,2)*(-22 + 26*t2 - 6*Power(t2,2)) + 
               2*Power(s2,2)*t2*
                (-14 + 20*t2 - 11*Power(t2,2) + 4*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-11 + 54*t2 + 25*Power(t2,2) + 16*Power(t2,3)) + 
               t1*(41 - 120*t2 + 131*Power(t2,2) - 64*Power(t2,3) + 
                  16*Power(t2,4) - 4*Power(t2,5)) + 
               s2*(-12 + (66 + 76*t1)*t2 - (137 + 116*t1)*Power(t2,2) + 
                  4*(32 + 13*t1)*Power(t2,3) - 
                  (55 + 8*t1)*Power(t2,4) + 10*Power(t2,5))) + 
            Power(s1,5)*(Power(t1,2)*(24 - 22*t2 - 6*Power(t2,2)) + 
               2*Power(s2,2)*(2 - 7*t2 + 3*Power(t2,2)) + 
               Power(-1 + t2,2)*(25 + 21*t2 + 6*Power(t2,2)) + 
               t1*(-18 - 17*t2 + 28*Power(t2,2) + 7*Power(t2,3)) + 
               s2*(-9 + 76*t2 - 65*Power(t2,2) - 2*Power(t2,3) + 
                  4*t1*(-6 + 7*t2 + Power(t2,2)))) + 
            Power(s1,4)*(Power(t1,2)*(-60 + 26*t2 + 22*Power(t2,2)) - 
               Power(-1 + t2,2)*(29 + 42*t2 + 23*Power(t2,2)) + 
               t1*(30 - 40*t2 + 34*Power(t2,2) - 24*Power(t2,3)) + 
               2*Power(s2,2)*
                (-8 - 15*t2 + 13*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(11 - 32*t2 - 12*Power(t2,2) + 32*Power(t2,3) + 
                  Power(t2,4) - 
                  4*t1*(-16 - 8*t2 + 17*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,3)*(-2*Power(t1,2)*(-27 + 17*t2 + 5*Power(t2,2)) + 
               Power(-1 + t2,2)*(45 + 2*t2 + 57*Power(t2,2)) + 
               Power(s2,2)*(28 - 24*t2 + 48*Power(t2,2) - 
                  42*Power(t2,3)) + 
               2*t1*(-16 + 30*t2 - 22*Power(t2,2) + 7*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(-47 + 164*t2 - 188*Power(t2,2) + 96*Power(t2,3) - 
                  27*Power(t2,4) + 2*Power(t2,5) + 
                  4*t1*(-15 - 3*t2 + 9*Power(t2,2) + 4*Power(t2,3))))) - 
         Power(s,2)*(2*Power(s1,7)*(s2 - t1)*(-1 + s2 - t1 + t2) - 
            2*Power(-1 + t2,2)*t2*
             (5 - 7*t2 - 3*Power(t2,2) + 2*Power(t2,3)) - 
            s1*Power(-1 + t2,2)*
             (-8 + (-98 + 56*s2 - 15*t1)*t2 + 
               (241 - 74*s2 - 11*t1)*Power(t2,2) + 
               (-23 - 18*s2 + 26*t1)*Power(t2,3) + 
               2*(-1 + 5*s2 - 2*t1)*Power(t2,4)) + 
            Power(s1,2)*(2*Power(t1,2)*
                (25 - 33*t2 + 8*Power(t2,2) + 2*Power(t2,3)) + 
               2*Power(s2,2)*t2*
                (50 - 112*t2 + 92*Power(t2,2) - 29*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(-1 + t2,2)*
                (-67 + 236*t2 - 14*Power(t2,2) + 61*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s2*(44 - (339 + 196*t1)*t2 + 
                  (866 + 380*t1)*Power(t2,2) - 
                  (897 + 244*t1)*Power(t2,3) + 
                  4*(95 + 13*t1)*Power(t2,4) - 54*Power(t2,5)) + 
               t1*(-74 + 240*t2 - 389*Power(t2,2) + 318*Power(t2,3) - 
                  107*Power(t2,4) + 12*Power(t2,5))) + 
            Power(s1,6)*(Power(s2,2)*(8 - 16*t2) + 
               6*Power(-1 + t2,2)*(3 + t2) - 4*Power(t1,2)*(-5 + 7*t2) + 
               t1*(41 - 86*t2 + 45*Power(t2,2)) + 
               s2*(-41 + 86*t2 - 45*Power(t2,2) + 4*t1*(-7 + 11*t2))) + 
            Power(s1,5)*(-2*Power(s2,2)*t2*(-9 + 5*t2) + 
               Power(t1,2)*(4 - 22*t2 + 26*Power(t2,2)) - 
               Power(-1 + t2,2)*(13 + 40*t2 + 27*Power(t2,2)) + 
               t1*(19 - 55*t2 + 77*Power(t2,2) - 41*Power(t2,3)) + 
               s2*(23 - 43*t2 - 7*Power(t2,2) + 27*Power(t2,3) - 
                  4*t1*(1 - t2 + 4*Power(t2,2)))) - 
            Power(s1,4)*(Power(s2,2)*(42 - 90*t2 + 44*Power(t2,2)) + 
               Power(t1,2)*(6 - 42*t2 + 24*Power(t2,2) + 
                  8*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-11 + 64*t2 + 70*Power(t2,2) + 9*Power(t2,3)) + 
               t1*(67 - 70*t2 + 12*Power(t2,2) + 10*Power(t2,3) - 
                  19*Power(t2,4)) + 
               s2*(-45 + 101*t2 - 151*Power(t2,2) + 87*Power(t2,3) + 
                  8*Power(t2,4) - 
                  4*t1*(16 - 42*t2 + 23*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,3)*(Power(t1,2)*
                (-54 + 58*t2 - 34*Power(t2,2) + 20*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-57 + 28*t2 - 138*Power(t2,2) - 29*Power(t2,3) + 
                  2*Power(t2,4)) - 
               2*Power(s2,2)*(36 - 105*t2 + 137*Power(t2,2) - 
                  66*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(3 + 138*t2 - 194*Power(t2,2) + 78*Power(t2,3) - 
                  23*Power(t2,4) - 2*Power(t2,5)) + 
               s2*(169 - 681*t2 + 867*Power(t2,2) - 489*Power(t2,3) + 
                  138*Power(t2,4) - 4*Power(t2,5) + 
                  4*t1*(25 - 47*t2 + 56*Power(t2,2) - 30*Power(t2,3) + 
                     Power(t2,4))))))*R1q(s1))/
     (s*(-1 + s1)*s1*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       (-1 + t1)*(-1 + t2)*Power(-1 + s + t2,3)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) - 
    (8*(Power(s,9)*t2*(-2 + t1 + t2) + 
         2*Power(1 + s1 - t2,2)*Power(-1 + t2,2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          (-Power(s1,2) + Power(s1,3) + t2*(-2 + 3*t2) - 
            s1*(-2 + 2*t2 + Power(t2,2))) + 
         Power(s,8)*(Power(t1,2)*(-2 + t2) + t1*t2*(-17 - s2 + 2*t2) + 
            s1*(-2 + t1 - 2*Power(t1,2) + (9 + s2)*t2 - 7*t1*t2 - 
               6*Power(t2,2)) + t2*(27 + 2*s2 - 19*t2 + 5*Power(t2,2))) + 
         Power(s,7)*(2*Power(t1,2)*(8 - 10*t2 + 3*Power(t2,2)) + 
            Power(s1,2)*(6 + s2 - 2*t1 - 4*s2*t1 + 12*Power(t1,2) - 
               15*t2 - 6*s2*t2 + 20*t1*t2 + 14*Power(t2,2)) - 
            t1*(4 - (59 + 8*s2)*t2 + (97 + 5*s2)*Power(t2,2) + 
               7*Power(t2,3)) + 
            t2*(-116 - Power(s2,2)*(-2 + t2) + 155*t2 - 
               69*Power(t2,2) + 9*Power(t2,3) + 
               s2*(-29 + 36*t2 + Power(t2,2))) + 
            s1*(25 + Power(t1,2)*(23 - 19*t2) - 128*t2 - 
               Power(s2,2)*t2 + 82*Power(t2,2) - 21*Power(t2,3) + 
               t1*(-16 + 59*t2 - 12*Power(t2,2)) + 
               s2*(2 - 2*t2 + Power(t2,2) + 5*t1*(-1 + 2*t2)))) - 
         Power(s,6)*(2 - 170*t2 - 151*s2*t2 + 18*Power(s2,2)*t2 + 
            500*Power(t2,2) + 304*s2*Power(t2,2) - 
            25*Power(s2,2)*Power(t2,2) - 351*Power(t2,3) - 
            173*s2*Power(t2,3) + 6*Power(s2,2)*Power(t2,3) + 
            135*Power(t2,4) - 6*s2*Power(t2,4) - 5*Power(t2,5) + 
            Power(s1,3)*(6 + s2 + 2*Power(s2,2) + 2*t1 - 20*s2*t1 + 
               30*Power(t1,2) - 11*t2 - 12*s2*t2 + 28*t1*t2 + 
               16*Power(t2,2)) - 
            3*Power(t1,2)*(-18 + 34*t2 - 23*Power(t2,2) + 
               5*Power(t2,3)) + 
            t1*(-28 + (85 + 18*s2)*t2 - 4*(85 + 8*s2)*Power(t2,2) + 
               3*(86 + 3*s2)*Power(t2,3) + 28*Power(t2,4)) - 
            Power(s1,2)*(-79 + 247*t2 - 137*Power(t2,2) + 
               24*Power(t2,3) + Power(s2,2)*(-3 + 8*t2) + 
               Power(t1,2)*(-84 + 88*t2) + 
               s2*(1 + t1*(41 - 62*t2) - 30*t2 + 3*Power(t2,2)) + 
               t1*(24 - 28*t2 + 15*Power(t2,2))) + 
            s1*(89 - 566*t2 + 643*Power(t2,2) - 252*Power(t2,3) + 
               13*Power(t2,4) - 
               2*Power(s2,2)*(1 - 5*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(95 - 153*t2 + 66*Power(t2,2)) - 
               t1*(51 - 203*t2 + 254*Power(t2,2) + 42*Power(t2,3)) + 
               s2*(31 - 72*t2 + 98*Power(t2,2) + 16*Power(t2,3) + 
                  t1*(-31 + 71*t2 - 42*Power(t2,2))))) + 
         Power(s,5)*(12 + 32*t2 - 339*s2*t2 + 46*Power(s2,2)*t2 + 
            427*Power(t2,2) + 1051*s2*Power(t2,2) - 
            133*Power(s2,2)*Power(t2,2) - 732*Power(t2,3) - 
            1102*s2*Power(t2,3) + 101*Power(s2,2)*Power(t2,3) + 
            385*Power(t2,4) + 395*s2*Power(t2,4) - 
            15*Power(s2,2)*Power(t2,4) - 165*Power(t2,5) + 
            15*s2*Power(t2,5) - 5*Power(t2,6) + 
            Power(t1,2)*(100 - 254*t2 + 247*Power(t2,2) - 
               115*Power(t2,3) + 20*Power(t2,4)) - 
            t1*(80 - 8*(3 + 4*s2)*t2 + (365 + 64*s2)*Power(t2,2) - 
               4*(219 + 10*s2)*Power(t2,3) + 5*(86 + s2)*Power(t2,4) + 
               35*Power(t2,5)) + 
            Power(s1,4)*(2 + 8*Power(s2,2) + 40*Power(t1,2) - 3*t2 + 
               9*Power(t2,2) - s2*(5 + 40*t1 + 8*t2) + t1*(10 + 17*t2)) \
+ Power(s1,3)*(109 + Power(s2,2)*(17 - 34*t2) - 271*t2 + 
               116*Power(t2,2) + 10*Power(t2,3) - 
               2*Power(t1,2)*(-77 + 94*t2) + 
               t1*(47 - 156*t2 + 38*Power(t2,2)) + 
               s2*(-38 + 141*t2 - 46*Power(t2,2) + t1*(-121 + 180*t2))) \
+ Power(s1,2)*(276 - 1049*t2 + 1086*Power(t2,2) - 319*Power(t2,3) - 
               58*Power(t2,4) + Power(s2,2)*t2*(-1 + 6*t2) + 
               Power(t1,2)*(217 - 439*t2 + 240*Power(t2,2)) + 
               t1*(-52 + 13*t2 + 29*Power(t2,2) - 157*Power(t2,3)) + 
               s2*(16 + 73*t2 - 45*Power(t2,2) + 95*Power(t2,3) + 
                  t1*(-119 + 290*t2 - 192*Power(t2,2)))) + 
            s1*(69 - 948*t2 + 1923*Power(t2,2) - 1305*Power(t2,3) + 
               391*Power(t2,4) + 40*Power(t2,5) + 
               Power(t1,2)*(187 - 447*t2 + 377*Power(t2,2) - 
                  115*Power(t2,3)) + 
               Power(s2,2)*(-16 + 115*t2 - 127*Power(t2,2) + 
                  33*Power(t2,3)) + 
               t1*(-58 + 397*t2 - 912*Power(t2,2) + 553*Power(t2,3) + 
                  145*Power(t2,4)) + 
               s2*(144 - 624*t2 + 852*Power(t2,2) - 467*Power(t2,3) - 
                  60*Power(t2,4) + 
                  t1*(-67 + 162*t2 - 164*Power(t2,2) + 60*Power(t2,3))))) \
- Power(s,4)*(28 - 120*t1 + 110*Power(t1,2) + 351*t2 - 379*s2*t2 + 
            50*Power(s2,2)*t2 - 98*t1*t2 + 61*s2*t1*t2 - 
            351*Power(t1,2)*t2 - 643*Power(t2,2) + 1712*s2*Power(t2,2) - 
            249*Power(s2,2)*Power(t2,2) + 270*t1*Power(t2,2) - 
            146*s2*t1*Power(t2,2) + 445*Power(t1,2)*Power(t2,2) + 
            61*Power(t2,3) - 2773*s2*Power(t2,3) + 
            380*Power(s2,2)*Power(t2,3) + 733*t1*Power(t2,3) + 
            88*s2*t1*Power(t2,3) - 288*Power(t1,2)*Power(t2,3) + 
            244*Power(t2,4) + 1960*s2*Power(t2,4) - 
            200*Power(s2,2)*Power(t2,4) - 1284*t1*Power(t2,4) + 
            100*Power(t1,2)*Power(t2,4) - 185*Power(t2,5) - 
            500*s2*Power(t2,5) + 20*Power(s2,2)*Power(t2,5) + 
            485*t1*Power(t2,5) - 5*s2*t1*Power(t2,5) - 
            15*Power(t1,2)*Power(t2,5) + 137*Power(t2,6) - 
            20*s2*Power(t2,6) + 14*t1*Power(t2,6) + 9*Power(t2,7) + 
            Power(s1,5)*(12*Power(s2,2) + 30*Power(t1,2) - 
               t1*(-13 + t2) + 2*Power(t2,2) + s2*(-11 - 40*t1 + 3*t2)) \
+ Power(s1,4)*(99 + Power(t1,2)*(164 - 217*t2) + 
               Power(s2,2)*(39 - 70*t2) - 208*t2 + 60*Power(t2,2) + 
               39*Power(t2,3) + 
               s2*(-106 - 179*t1 + 280*t2 + 265*t1*t2 - 
                  124*Power(t2,2)) + t1*(143 - 331*t2 + 128*Power(t2,2))\
) + Power(s1,3)*(300 - 986*t2 + 975*Power(t2,2) - 139*Power(t2,3) - 
               162*Power(t2,4) + 
               2*Power(s2,2)*(7 - 30*t2 + 28*Power(t2,2)) + 
               Power(t1,2)*(261 - 647*t2 + 414*Power(t2,2)) + 
               t1*(129 - 621*t2 + 758*Power(t2,2) - 350*Power(t2,3)) + 
               s2*(-101 + 426*t2 - 511*Power(t2,2) + 272*Power(t2,3) + 
                  t1*(-201 + 583*t2 - 422*Power(t2,2)))) + 
            Power(s1,2)*(310 - 1716*t2 + 2806*Power(t2,2) - 
               1835*Power(t2,3) + 299*Power(t2,4) + 192*Power(t2,5) + 
               Power(t1,2)*(254 - 739*t2 + 809*Power(t2,2) - 
                  320*Power(t2,3)) + 
               Power(s2,2)*(-59 + 232*t2 - 199*Power(t2,2) + 
                  48*Power(t2,3)) + 
               t1*(-138 + 349*t2 - 269*Power(t2,2) - 132*Power(t2,3) + 
                  378*Power(t2,4)) + 
               s2*(241 - 608*t2 + 589*Power(t2,2) - 186*Power(t2,3) - 
                  230*Power(t2,4) + 
                  t1*(-111 + 313*t2 - 462*Power(t2,2) + 232*Power(t2,3))\
)) - s1*(141 + 237*t2 - 1909*Power(t2,2) + 2457*Power(t2,3) - 
               1337*Power(t2,4) + 369*Power(t2,5) + 80*Power(t2,6) + 
               Power(t1,2)*(-197 + 649*t2 - 770*Power(t2,2) + 
                  438*Power(t2,3) - 110*Power(t2,4)) + 
               Power(s2,2)*(30 - 292*t2 + 634*Power(t2,2) - 
                  428*Power(t2,3) + 70*Power(t2,4)) + 
               t1*(-58 - 222*t2 + 1503*Power(t2,2) - 2149*Power(t2,3) + 
                  817*Power(t2,4) + 165*Power(t2,5)) + 
               s2*(-251 + 1607*t2 - 3186*Power(t2,2) + 
                  2783*Power(t2,3) - 924*Power(t2,4) - 95*Power(t2,5) + 
                  t1*(85 - 241*t2 + 142*Power(t2,2) - 30*Power(t2,3) + 
                     20*Power(t2,4))))) - 
         s*(-1 + t2)*(2*Power(s1,7)*(s2 - t1)*
             (-7 + 3*s2 - 3*t1 + 7*t2) - 
            Power(s1,6)*(-6*Power(-1 + t2,2)*(1 + t2) + 
               2*Power(s2,2)*(1 + 6*t2) + 2*Power(t1,2)*(-5 + 12*t2) + 
               t1*(-15 + 76*t2 - 61*Power(t2,2)) + 
               s2*(15 + t1*(8 - 36*t2) - 76*t2 + 61*Power(t2,2))) + 
            2*Power(s1,3)*(Power(s2,2)*
                (-16 + 112*t2 - 202*Power(t2,2) + 103*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(t1,2)*(11 + 5*t2 - 49*Power(t2,2) + 
                  10*Power(t2,3) + 18*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (-9 + 9*t2 + 9*Power(t2,2) + 25*Power(t2,3) + 
                  18*Power(t2,4)) + 
               t1*(-32 + 138*t2 - 214*Power(t2,2) + 119*Power(t2,3) + 
                  23*Power(t2,4) - 34*Power(t2,5)) + 
               s2*(44 - (199 + 97*t1)*t2 + (344 + 223*t1)*Power(t2,2) - 
                  (247 + 97*t1)*Power(t2,3) + 
                  (27 - 19*t1)*Power(t2,4) + 31*Power(t2,5))) - 
            Power(-1 + t2,2)*
             (-4 + 8*(-7 + s2)*t2 + 
               2*(87 - 46*s2 + 4*Power(s2,2))*Power(t2,2) - 
               2*(80 - 103*s2 + 22*Power(s2,2))*Power(t2,3) + 
               (73 - 149*s2 + 42*Power(s2,2))*Power(t2,4) - 
               (24 - 26*s2 + Power(s2,2))*Power(t2,5) + 
               (-5 + s2)*Power(t2,6) + 
               Power(t1,2)*(-4 - 8*t2 + 14*Power(t2,2) + 
                  2*Power(t2,3) + Power(t2,4)) + 
               t1*(8 - 8*(-8 + s2)*t2 + 4*(-46 + 15*s2)*Power(t2,2) + 
                  (98 - 58*s2)*Power(t2,3) + (34 - 5*s2)*Power(t2,4) + 
                  (-22 + s2)*Power(t2,5) + 2*Power(t2,6))) - 
            Power(s1,5)*(Power(t1,2)*(-5 + 54*t2 - 53*Power(t2,2)) + 
               Power(s2,2)*(19 - 26*t2 + 3*Power(t2,2)) + 
               Power(-1 + t2,2)*(9 + 17*t2 + 26*Power(t2,2)) + 
               t1*(2 + 49*t2 - 164*Power(t2,2) + 113*Power(t2,3)) + 
               s2*(-27 + 4*t2 + 133*Power(t2,2) - 110*Power(t2,3) + 
                  2*t1*(-5 - 18*t2 + 27*Power(t2,2)))) + 
            Power(s1,4)*(Power(t1,2)*
                (-17 + 32*t2 + 61*Power(t2,2) - 64*Power(t2,3)) + 
               Power(s2,2)*(-38 + 131*t2 - 98*Power(t2,2) + 
                  17*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-3 + 24*t2 + 29*Power(t2,2) + 44*Power(t2,3)) + 
               2*t1*(-8 + 21*t2 + 18*Power(t2,2) - 89*Power(t2,3) + 
                  58*Power(t2,4)) + 
               s2*(41 - 178*t2 + 170*Power(t2,2) + 74*Power(t2,3) - 
                  107*Power(t2,4) + 
                  t1*(49 - 137*t2 + 3*Power(t2,2) + 61*Power(t2,3)))) + 
            Power(s1,2)*(-2*Power(s2,2)*
                (4 - 62*t2 + 236*Power(t2,2) - 310*Power(t2,3) + 
                  128*Power(t2,4) + 3*Power(t2,5)) - 
               2*Power(t1,2)*
                (-10 + 58*t2 - 65*Power(t2,2) - 9*Power(t2,3) + 
                  21*Power(t2,4) + 4*Power(t2,5)) + 
               2*Power(-1 + t2,2)*
                (-30 + 31*t2 - 6*Power(t2,2) + 12*Power(t2,3) + 
                  28*Power(t2,4) + 7*Power(t2,5)) + 
               t1*(36 + 50*t2 - 522*Power(t2,2) + 886*Power(t2,3) - 
                  575*Power(t2,4) + 108*Power(t2,5) + 17*Power(t2,6)) + 
               s2*(76 - 526*t2 + 1294*Power(t2,2) - 1472*Power(t2,3) + 
                  769*Power(t2,4) - 118*Power(t2,5) - 23*Power(t2,6) + 
                  2*t1*(-22 + 69*t2 + 51*Power(t2,2) - 
                     241*Power(t2,3) + 141*Power(t2,4)))) + 
            s1*(-1 + t2)*(60 - 290*t2 + 466*Power(t2,2) - 
               342*Power(t2,3) + 151*Power(t2,4) - 16*Power(t2,5) - 
               27*Power(t2,6) - 2*Power(t2,7) + 
               Power(s2,2)*t2*
                (-16 + 136*t2 - 288*Power(t2,2) + 169*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*(12 - 2*t2 - 70*Power(t2,2) + 
                  54*Power(t2,3) + 7*Power(t2,4) + Power(t2,5)) + 
               t1*(-72 + 284*t2 - 244*Power(t2,2) - 184*Power(t2,3) + 
                  314*Power(t2,4) - 101*Power(t2,5) + 3*Power(t2,6)) + 
               s2*(-8 + 176*t2 - 660*Power(t2,2) + 930*Power(t2,3) - 
                  535*Power(t2,4) + 91*Power(t2,5) + 6*Power(t2,6) + 
                  2*t1*(4 - 56*t2 + 106*Power(t2,2) - Power(t2,3) - 
                     59*Power(t2,4) + 4*Power(t2,5))))) - 
         Power(s,2)*(2*Power(s1,7)*(s2 - t1)*(-1 + s2 - t1 + t2) + 
            Power(-1 + t2,2)*
             (18 + (230 - 64*s2 + 4*Power(s2,2))*t2 + 
               (-656 + 458*s2 - 62*Power(s2,2))*Power(t2,2) + 
               29*(19 - 31*s2 + 6*Power(s2,2))*Power(t2,3) + 
               (-265 + 664*s2 - 125*Power(s2,2))*Power(t2,4) + 
               3*(32 - 51*s2 + 2*Power(s2,2))*Power(t2,5) + 
               (31 - 6*s2)*Power(t2,6) + Power(t2,7) - 
               Power(t1,2)*(-26 + 42*t2 - 38*Power(t2,2) + 
                  27*Power(t2,3) - 3*Power(t2,4) + Power(t2,5)) - 
               t1*(44 - 8*(-24 + 5*s2)*t2 + 
                  2*(-283 + 79*s2)*Power(t2,2) + 
                  (210 - 107*s2)*Power(t2,3) + 
                  (248 - 22*s2)*Power(t2,4) + 
                  (-136 + 5*s2)*Power(t2,5) + 8*Power(t2,6))) + 
            Power(s1,6)*(Power(t1,2)*(38 - 46*t2) + 
               Power(s2,2)*(26 - 34*t2) + 6*Power(-1 + t2,2)*(3 + t2) + 
               t1*(69 - 142*t2 + 73*Power(t2,2)) + 
               s2*(-69 + 142*t2 - 73*Power(t2,2) + 16*t1*(-4 + 5*t2))) + 
            Power(s1,5)*(2*Power(s2,2)*(5 - 34*t2 + 33*Power(t2,2)) - 
               Power(-1 + t2,2)*(-43 + 68*t2 + 55*Power(t2,2)) + 
               2*Power(t1,2)*(37 - 114*t2 + 81*Power(t2,2)) + 
               t1*(110 - 452*t2 + 598*Power(t2,2) - 256*Power(t2,3)) + 
               s2*(-74 + 372*t2 - 546*Power(t2,2) + 248*Power(t2,3) - 
                  4*t1*(21 - 74*t2 + 57*Power(t2,2)))) + 
            Power(s1,4)*(Power(t1,2)*
                (30 - 265*t2 + 492*Power(t2,2) - 253*Power(t2,3)) - 
               Power(s2,2)*(59 - 114*t2 + 31*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (40 - 156*t2 + 103*Power(t2,2) + 145*Power(t2,3)) + 
               2*t1*(19 - 193*t2 + 480*Power(t2,2) - 503*Power(t2,3) + 
                  197*Power(t2,4)) + 
               s2*(35 + 41*t2 - 449*Power(t2,2) + 727*Power(t2,3) - 
                  354*Power(t2,4) + 
                  t1*(31 + 157*t2 - 479*Power(t2,2) + 283*Power(t2,3)))) \
+ Power(s1,3)*(-2*Power(s2,2)*
                (50 - 249*t2 + 361*Power(t2,2) - 179*Power(t2,3) + 
                  22*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (-111 + 277*t2 - 294*Power(t2,2) + 156*Power(t2,3) + 
                  166*Power(t2,4)) + 
               Power(t1,2)*(39 - 97*t2 + 273*Power(t2,2) - 
                  419*Power(t2,3) + 194*Power(t2,4)) - 
               2*t1*(83 - 256*t2 + 202*Power(t2,2) + 136*Power(t2,3) - 
                  328*Power(t2,4) + 163*Power(t2,5)) + 
               s2*(217 - 859*t2 + 1267*Power(t2,2) - 675*Power(t2,3) - 
                  218*Power(t2,4) + 268*Power(t2,5) + 
                  t1*(57 - 377*t2 + 401*Power(t2,2) + 101*Power(t2,3) - 
                     162*Power(t2,4)))) + 
            Power(s1,2)*(Power(t1,2)*
                (78 - 313*t2 + 375*Power(t2,2) - 209*Power(t2,3) + 
                  145*Power(t2,4) - 72*Power(t2,5)) + 
               2*Power(s2,2)*
                (-23 + 231*t2 - 669*Power(t2,2) + 752*Power(t2,3) - 
                  313*Power(t2,4) + 24*Power(t2,5)) + 
               Power(-1 + t2,2)*
                (-106 - 105*t2 + 357*Power(t2,2) - 228*Power(t2,3) + 
                  216*Power(t2,4) + 88*Power(t2,5)) + 
               t1*(16 + 480*t2 - 1886*Power(t2,2) + 2682*Power(t2,3) - 
                  1621*Power(t2,4) + 196*Power(t2,5) + 133*Power(t2,6)) \
+ s2*(276 - 1719*t2 + 3979*Power(t2,2) - 4503*Power(t2,3) + 
                  2494*Power(t2,4) - 408*Power(t2,5) - 
                  119*Power(t2,6) + 
                  t1*(-84 + 91*t2 + 561*Power(t2,2) - 
                     1021*Power(t2,3) + 439*Power(t2,4) + 6*Power(t2,5))\
)) - s1*(-1 + t2)*(-210 + 902*t2 - 1209*Power(t2,2) + 654*Power(t2,3) - 
               186*Power(t2,4) - 95*Power(t2,5) + 125*Power(t2,6) + 
               19*Power(t2,7) + 
               Power(t1,2)*(14 - 144*t2 + 299*Power(t2,2) - 
                  174*Power(t2,3) + 27*Power(t2,4) - 14*Power(t2,5)) + 
               2*Power(s2,2)*
                (-2 + 56*t2 - 276*Power(t2,2) + 441*Power(t2,3) - 
                  227*Power(t2,4) + 12*Power(t2,5)) + 
               2*t1*(100 - 349*t2 + 149*Power(t2,2) + 562*Power(t2,3) - 
                  698*Power(t2,4) + 232*Power(t2,5) + 4*Power(t2,6)) - 
               s2*(-64 + 798*t2 - 2583*Power(t2,2) + 3492*Power(t2,3) - 
                  2157*Power(t2,4) + 480*Power(t2,5) + 34*Power(t2,6) + 
                  t1*(40 - 282*t2 + 329*Power(t2,2) + 194*Power(t2,3) - 
                     295*Power(t2,4) + 30*Power(t2,5))))) + 
         Power(s,3)*(2*Power(s1,6)*(s2 - t1)*(-4 + 4*s2 - 6*t1 + 3*t2) - 
            (-1 + t2)*(32 + (426 - 220*s2 + 24*Power(s2,2))*t2 + 
               (-1079 + 1187*s2 - 178*Power(s2,2))*Power(t2,2) + 
               (770 - 2083*s2 + 345*Power(s2,2))*Power(t2,3) + 
               (-319 + 1508*s2 - 205*Power(s2,2))*Power(t2,4) + 
               (87 - 377*s2 + 15*Power(s2,2))*Power(t2,5) + 
               (84 - 15*s2)*Power(t2,6) + 5*Power(t2,7) + 
               Power(t1,2)*(72 - 194*t2 + 219*Power(t2,2) - 
                  126*Power(t2,3) + 36*Power(t2,4) - 6*Power(t2,5)) + 
               t1*(-100 + 36*(-7 + 2*s2)*t2 + 
                  (744 - 199*s2)*Power(t2,2) + 
                  (26 + 103*s2)*Power(t2,3) + 
                  (-761 + 31*s2)*Power(t2,4) + 
                  (350 - 9*s2)*Power(t2,5) - 7*Power(t2,6))) + 
            Power(s1,5)*(62 + Power(t1,2)*(105 - 139*t2) + 
               Power(s2,2)*(45 - 71*t2) - 113*t2 + 24*Power(t2,2) + 
               27*Power(t2,3) + t1*(143 - 303*t2 + 144*Power(t2,2)) + 
               s2*(-126 + 283*t2 - 141*Power(t2,2) + 2*t1*(-73 + 103*t2))\
) + Power(s1,4)*(150 - 529*t2 + 501*Power(t2,2) + 27*Power(t2,3) - 
               149*Power(t2,4) + 
               Power(s2,2)*(22 - 105*t2 + 99*Power(t2,2)) + 
               Power(t1,2)*(185 - 531*t2 + 370*Power(t2,2)) + 
               t1*(229 - 851*t2 + 1033*Power(t2,2) - 423*Power(t2,3)) + 
               s2*(-145 + 591*t2 - 813*Power(t2,2) + 379*Power(t2,3) - 
                  5*t1*(37 - 120*t2 + 91*Power(t2,2)))) + 
            Power(s1,3)*(Power(s2,2)*
                (-85 + 216*t2 - 117*Power(t2,2) + 6*Power(t2,3)) + 
               s2*(109 - 79*t2 - 261*Power(t2,2) + 587*Power(t2,3) - 
                  420*Power(t2,4) + 
                  t1*(-23 + 288*t2 - 695*Power(t2,2) + 398*Power(t2,3))) \
- 2*(-138 + 580*t2 - 914*Power(t2,2) + 563*Power(t2,3) + 
                  42*Power(t2,4) - 133*Power(t2,5) + 
                  Power(t1,2)*
                   (-74 + 300*t2 - 440*Power(t2,2) + 208*Power(t2,3)) + 
                  t1*(28 + 157*t2 - 523*Power(t2,2) + 577*Power(t2,3) - 
                     271*Power(t2,4)))) + 
            Power(s1,2)*(55 - 969*t2 + 2579*Power(t2,2) - 
               2803*Power(t2,3) + 1464*Power(t2,4) - 128*Power(t2,5) - 
               198*Power(t2,6) + 
               Power(s2,2)*(-94 + 587*t2 - 1020*Power(t2,2) + 
                  595*Power(t2,3) - 92*Power(t2,4)) + 
               Power(t1,2)*(169 - 569*t2 + 783*Power(t2,2) - 
                  623*Power(t2,3) + 220*Power(t2,4)) - 
               2*t1*(62 - 430*t2 + 823*Power(t2,2) - 619*Power(t2,3) + 
                  12*Power(t2,4) + 171*Power(t2,5)) + 
               s2*(431 - 1892*t2 + 3030*Power(t2,2) - 2272*Power(t2,3) + 
                  501*Power(t2,4) + 240*Power(t2,5) + 
                  t1*(-77 + 6*t2 + 207*Power(t2,2) + 16*Power(t2,3) - 
                     108*Power(t2,4)))) + 
            s1*(-298 + 1002*t2 - 600*Power(t2,2) - 804*Power(t2,3) + 
               1136*Power(t2,4) - 735*Power(t2,5) + 240*Power(t2,6) + 
               59*Power(t2,7) + 
               Power(t1,2)*(106 - 500*t2 + 812*Power(t2,2) - 
                  592*Power(t2,3) + 237*Power(t2,4) - 57*Power(t2,5)) + 
               Power(s2,2)*(-20 + 288*t2 - 1025*Power(t2,2) + 
                  1340*Power(t2,3) - 642*Power(t2,4) + 65*Power(t2,5)) + 
               t1*(216 - 550*t2 - 592*Power(t2,2) + 2820*Power(t2,3) - 
                  2781*Power(t2,4) + 809*Power(t2,5) + 78*Power(t2,6)) - 
               s2*(-192 + 1740*t2 - 4931*Power(t2,2) + 6339*Power(t2,3) - 
                  3951*Power(t2,4) + 916*Power(t2,5) + 79*Power(t2,6) + 
                  t1*(76 - 350*t2 + 267*Power(t2,2) + 264*Power(t2,3) - 
                     275*Power(t2,4) + 30*Power(t2,5))))))*
       R1q(1 - s + s1 - t2))/
     (s*(-1 + s1)*(-1 + t1)*Power(-s + s1 - t2,2)*(1 - s + s1 - t2)*
       (-1 + t2)*Power(-1 + s + t2,3)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) + 
    (8*(-3*Power(s,5)*t2*(-2 + t1 + t2) + 
         Power(s,4)*(-(Power(t1,2)*(-2 + t2)) + t1*(27 + 3*s2 - t2)*t2 - 
            t2*(29 + 6*s2 - 15*t2 + 4*Power(t2,2)) + 
            s1*(6 + 2*Power(t1,2) - 13*t2 - 3*s2*t2 + 3*Power(t2,2) + 
               t1*(-3 + 10*t2))) + 
         Power(-1 + s1,2)*(4 + 17*t2 + 15*s2*t2 - 2*Power(s2,2)*t2 - 
            29*Power(t2,2) - 17*s2*Power(t2,2) + 
            3*Power(s2,2)*Power(t2,2) + 7*Power(t2,3) + 
            3*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) + Power(t2,4) - 
            s2*Power(t2,4) + Power(t1,2)*(4 - 5*t2 + 3*Power(t2,2)) + 
            t1*(-8 + (-28 + 3*s2)*t2 - 8*(-6 + s2)*Power(t2,2) + 
               (-12 + s2)*Power(t2,3)) + 
            2*Power(s1,3)*(-1 + Power(s2,2) + Power(t1,2) - 
               2*t1*(-1 + t2) + t2 + s2*(-1 - 2*t1 + t2)) + 
            Power(s1,2)*(Power(t1,2)*(1 - 3*t2) - 
               Power(s2,2)*(1 + t2) + 2*(3 - 4*t2 + Power(t2,2)) + 
               t1*(-3 + 2*t2 + Power(t2,2)) + 
               s2*(-5 + 8*t2 - 3*Power(t2,2) + 2*t1*(1 + t2))) - 
            s1*(29 - 35*t2 + Power(t2,2) + 5*Power(t2,3) + 
               2*Power(s2,2)*(-1 + t2 + Power(t2,2)) + 
               Power(t1,2)*(7 - 8*t2 + 3*Power(t2,2)) - 
               t1*(52 - 69*t2 + 14*Power(t2,2) + 3*Power(t2,3)) - 
               s2*(-15 + 22*t2 - 9*Power(t2,2) + 2*Power(t2,3) + 
                  t1*(-3 + 6*t2 + Power(t2,2))))) + 
         Power(s,3)*(Power(t1,2)*(-10 + 4*t2 + Power(t2,2)) + 
            t1*(4 - (15 + 4*s2)*t2 + 22*Power(t2,2) - 2*Power(t2,3)) + 
            Power(s1,2)*(-4 - 8*Power(t1,2) + t1*(3 - 8*t2) - 6*t2 + 
               3*Power(t2,2) + s2*(-3 + 4*t1 + 3*t2)) + 
            t2*(38 + 3*Power(s2,2)*(-2 + t2) - 2*t2 + 6*Power(t2,2) + 
               Power(t2,3) + s2*(19 - 22*t2 + 3*Power(t2,2))) + 
            s1*(-23 + 23*t2 + 3*Power(s2,2)*t2 + Power(t2,2) + 
               3*Power(t2,3) + Power(t1,2)*(-13 + 6*t2) - 
               8*t1*(-3 + 7*t2) + 
               s2*(t1*(7 - 11*t2) + 2*(-3 + 8*t2 + Power(t2,2))))) + 
         Power(s,2)*(2 + 48*t2 - 21*s2*t2 + 10*Power(s2,2)*t2 - 
            32*Power(t2,2) + 39*s2*Power(t2,2) - 
            9*Power(s2,2)*Power(t2,2) + 35*Power(t2,3) - 
            13*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) - Power(t2,4) - 
            s2*Power(t2,4) + Power(t1,2)*(18 - 10*t2 + Power(t2,2)) + 
            t1*(-16 + (-59 + 2*s2)*t2 + (19 - 4*s2)*Power(t2,2) + 
               s2*Power(t2,3)) + 
            Power(s1,3)*(-8 + 2*Power(s2,2) + 12*Power(t1,2) + 
               t1*(7 - 6*t2) + 21*t2 - 3*Power(t2,2) + 
               s2*(-4 - 12*t1 + 5*t2)) + 
            Power(s1,2)*(-12 + Power(t1,2)*(17 - 12*t2) + 
               Power(s2,2)*(5 - 3*t2) + 9*t2 - 27*Power(t2,2) + 
               2*Power(t2,3) + t1*(-27 + 33*t2 + 4*Power(t2,2)) + 
               s2*(7 - 2*t2 - 7*Power(t2,2) + 5*t1*(-4 + 3*t2))) + 
            s1*(13 + 66*t2 - 38*Power(t2,2) + 7*Power(t2,3) - 
               2*Power(t2,4) + Power(t1,2)*(13 - 2*t2 - 5*Power(t2,2)) + 
               Power(s2,2)*(-6 + 4*t2 - 4*Power(t2,2)) + 
               t1*(9 - 135*t2 - 12*Power(t2,2) + 7*Power(t2,3)) + 
               s2*(17 + 47*t2 + 13*Power(t2,2) + 
                  t1*(-13 + 14*t2 + Power(t2,2))))) - 
         s*(6 - 20*t1 + 14*Power(t1,2) + 84*t2 + 7*s2*t2 + 
            2*Power(s2,2)*t2 - 78*t1*t2 + 4*s2*t1*t2 - 
            12*Power(t1,2)*t2 - 95*Power(t2,2) + 12*s2*Power(t2,2) - 
            3*Power(s2,2)*Power(t2,2) + 80*t1*Power(t2,2) - 
            12*s2*t1*Power(t2,2) + 5*Power(t1,2)*Power(t2,2) + 
            20*Power(t2,3) + 5*s2*Power(t2,3) + 
            2*Power(s2,2)*Power(t2,3) - 22*t1*Power(t2,3) + 
            2*s2*t1*Power(t2,3) + Power(t2,4) - 2*s2*Power(t2,4) + 
            Power(s1,4)*(-8 + 4*Power(s2,2) + 8*Power(t1,2) - 
               11*t1*(-1 + t2) + 10*t2 + s2*(-9 - 12*t1 + 7*t2)) + 
            Power(s1,3)*(19 + Power(t1,2)*(3 - 10*t2) - 17*t2 - 
               Power(s2,2)*t2 - 9*Power(t2,2) + Power(t2,3) + 
               2*t1*(-11 + 7*t2 + 2*Power(t2,2)) + 
               s2*(8 + 12*t2 - 8*Power(t2,2) + t1*(-3 + 9*t2))) + 
            Power(s1,2)*(-38 + 42*t2 + 27*Power(t2,2) - 8*Power(t2,3) - 
               Power(t2,4) + Power(t1,2)*(-12 + 16*t2 - 7*Power(t2,2)) + 
               Power(s2,2)*(4 - 10*t2 - 3*Power(t2,2)) + 
               t1*(137 - 191*t2 + 14*Power(t2,2) + 8*Power(t2,3)) + 
               s2*(-87 + 106*t2 - 20*Power(t2,2) + 5*Power(t2,3) + 
                  2*t1*(2 + 6*t2 + Power(t2,2)))) + 
            s1*(-71 + 81*t2 - 43*Power(t2,2) - 5*Power(t2,3) + 
               4*Power(t2,4) + Power(t1,2)*(-13 + 6*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-4 + 5*t2 + 2*Power(t2,2) + 2*Power(t2,3)) - 
               2*t1*(-37 + 65*t2 - 77*Power(t2,2) + 11*Power(t2,3)) + 
               s2*(t1*(-9 + 19*t2 - 18*Power(t2,2) + 2*Power(t2,3)) - 
                  2*(-8 - 12*t2 + 42*Power(t2,2) - 5*Power(t2,3) + 
                     Power(t2,4))))))*R2q(s))/
     (s*(-1 + s1)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       (-1 + t1)*(-1 + t2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) - 
    (8*(-(Power(s,7)*((1 + s1)*Power(t1,2) + Power(t2,2)*(-1 + 2*t2) + 
              t1*t2*(4 - s1 + 2*t2))) + 
         Power(s,6)*(Power(t1,2)*
             (9 + 7*Power(s1,2) + s1*(13 - 2*t2) - 2*t2) + 
            t2*(-6 - 16*t2 + 9*Power(t2,2) - 4*Power(t2,3) - 
               Power(s1,2)*(-1 + s2 + t2) + 
               s2*(2 + 4*t2 + Power(t2,2)) + 
               s1*(7 - 4*t2 + 7*Power(t2,2) - 2*s2*(2 + t2))) + 
            t1*(-2 + Power(s1,2)*(3 - 2*s2 - 7*t2) + 13*t2 + 
               (3 + s2)*Power(t2,2) - 4*Power(t2,3) - 
               s1*(4 - 11*t2 - 13*Power(t2,2) + s2*(2 + t2)))) + 
         Power(s,5)*(-1 + 25*t2 - 14*s2*t2 + 40*Power(t2,2) - 
            4*s2*Power(t2,2) - 5*Power(s2,2)*Power(t2,2) - 
            24*Power(t2,3) + Power(s2,2)*Power(t2,3) + 9*Power(t2,4) + 
            3*s2*Power(t2,4) - 2*Power(t2,5) + 
            Power(t1,2)*(-34 + 19*t2 - 2*Power(t2,2)) + 
            t1*(16 + 5*t2 - (1 + 3*s2)*Power(t2,2) + 
               (13 + s2)*Power(t2,3) - 2*Power(t2,4)) + 
            Power(s1,3)*(-Power(s2,2) - 21*Power(t1,2) + 
               5*(-1 + t2)*t2 + s2*(1 + 12*t1 + 4*t2) + t1*(-16 + 21*t2)\
) + Power(s1,2)*(7 + 12*Power(t1,2)*(-4 + t2) + Power(s2,2)*(-1 + t2) - 
               22*t2 + Power(t2,2) - 10*Power(t2,3) + 
               t1*(2 + 10*t2 - 37*Power(t2,2)) + 
               s2*(-4 + 14*t2 + 7*Power(t2,2) + t1*(19 + t2))) + 
            s1*(-5 + (-23 + 4*s2 + 2*Power(s2,2))*t2 + 
               3*(25 - 11*s2 + Power(s2,2))*Power(t2,2) - 
               (13 + 6*s2)*Power(t2,3) + 11*Power(t2,4) - 
               Power(t1,2)*(54 - 21*t2 + Power(t2,2)) + 
               t1*(15 + 23*t2 - 19*Power(t2,2) + 18*Power(t2,3) + 
                  s2*(14 - 8*Power(t2,2))))) + 
         Power(-1 + s1,3)*(-4 + 8*t1 - 4*Power(t1,2) - t2 - 10*t1*t2 + 
            11*Power(t1,2)*t2 + 23*Power(t2,2) - 16*s2*Power(t2,2) + 
            4*Power(s2,2)*Power(t2,2) + 3*t1*Power(t2,2) - 
            4*s2*t1*Power(t2,2) - 10*Power(t1,2)*Power(t2,2) - 
            29*Power(t2,3) + 27*s2*Power(t2,3) - 
            7*Power(s2,2)*Power(t2,3) - t1*Power(t2,3) + 
            8*s2*t1*Power(t2,3) + 2*Power(t1,2)*Power(t2,3) + 
            13*Power(t2,4) - 13*s2*Power(t2,4) + 
            2*Power(s2,2)*Power(t2,4) - 2*s2*t1*Power(t2,4) - 
            2*Power(t2,5) + 2*s2*Power(t2,5) + 
            Power(s1,5)*(s2 - t1)*(-1 + s2 - t1 + t2) - 
            s1*(-17 + (55 - 32*s2 + 8*Power(s2,2))*t2 + 
               (-62 + 63*s2 - 13*Power(s2,2))*Power(t2,2) + 
               (27 - 35*s2 + 3*Power(s2,2))*Power(t2,3) + 
               (-3 + 4*s2 + Power(s2,2))*Power(t2,4) + 
               Power(t1,2)*(-5 + 13*t2 - 11*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(22 - 4*(9 + 2*s2)*t2 + (9 + 14*s2)*Power(t2,2) + 
                  (7 - 4*s2)*Power(t2,3) - 2*Power(t2,4))) + 
            Power(s1,4)*(Power(s2,2)*(1 - 2*t2) + 
               Power(t1,2)*(1 - 2*t2) + Power(-1 + t2,2) + 
               t1*(3 - 5*t2 + 2*Power(t2,2)) + 
               s2*(-4 + 7*t2 - 3*Power(t2,2) + t1*(-2 + 4*t2))) - 
            Power(s1,3)*(Power(s2,2)*(1 + t2) + 
               Power(-1 + t2,2)*(-3 + 2*t2) + 
               Power(t1,2)*(2 + t2 - Power(t2,2)) + 
               t1*(-7 + 11*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               s2*(9 - 17*t2 + 11*Power(t2,2) - 3*Power(t2,3) + 
                  2*t1*(-1 - 2*t2 + Power(t2,2)))) + 
            Power(s1,2)*(Power(t1,2)*(-1 + 5*t2 - 2*Power(t2,2)) + 
               Power(-1 + t2,2)*(8 - 4*t2 + Power(t2,2)) + 
               t1*(9 - 29*t2 + 23*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*(4 - 5*t2 + Power(t2,2) + 2*Power(t2,3)) - 
               s2*(16 - 45*t2 + 35*Power(t2,2) - 7*Power(t2,3) + 
                  Power(t2,4) + 4*t1*(1 - t2 + Power(t2,2))))) + 
         Power(s,4)*(7 - 52*t1 + 70*Power(t1,2) - 34*t2 + 38*s2*t2 - 
            64*t1*t2 - 67*Power(t1,2)*t2 - 18*Power(t2,2) - 
            46*s2*Power(t2,2) + 23*Power(s2,2)*Power(t2,2) + 
            37*t1*Power(t2,2) + 18*Power(t1,2)*Power(t2,2) + 
            5*Power(t2,3) + 21*s2*Power(t2,3) - 
            14*Power(s2,2)*Power(t2,3) - 21*t1*Power(t2,3) - 
            2*Power(t1,2)*Power(t2,3) + 21*Power(t2,4) - 
            13*s2*Power(t2,4) + Power(s2,2)*Power(t2,4) + 
            4*t1*Power(t2,4) + 2*s2*Power(t2,5) + 
            5*Power(s1,4)*(Power(s2,2) + 7*Power(t1,2) - 
               7*t1*(-1 + t2) - 2*(-1 + t2)*t2 - s2*(1 + 6*t1 + t2)) + 
            Power(s1,3)*(-15 + Power(t1,2)*(80 - 30*t2) + 
               Power(s2,2)*(8 - 6*t2) + 12*t2 + 15*Power(t2,2) + 
               10*Power(t2,3) + 4*t1*(5 - 18*t2 + 15*Power(t2,2)) + 
               s2*(7 - 8*t2 - 11*Power(t2,2) + 2*t1*(-26 + 5*t2))) + 
            Power(s1,2)*(-12 + 58*t2 - 96*Power(t2,2) - 
               17*Power(t2,3) - 10*Power(t2,4) + 
               Power(s2,2)*(7 - 10*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(100 - 59*t2 + 5*Power(t2,2)) + 
               t1*(33 - 197*t2 + 51*Power(t2,2) - 33*Power(t2,3)) + 
               s2*(2 + 5*t2 + 94*Power(t2,2) + 13*Power(t2,3) + 
                  2*t1*(-27 + 9*t2 + 10*Power(t2,2)))) + 
            s1*(14 + (58 + 52*s2 - 22*Power(s2,2))*t2 + 
               (-116 + 53*s2 + 16*Power(s2,2))*Power(t2,2) + 
               2*(28 - 19*s2 + Power(s2,2))*Power(t2,3) + 
               (2 - 9*s2)*Power(t2,4) + 4*Power(t2,5) + 
               Power(t1,2)*(97 - 82*t2 + 11*Power(t2,2)) - 
               2*t1*(2 + 109*t2 - 47*Power(t2,2) + 10*Power(t2,3) - 
                  3*Power(t2,4) + 
                  s2*(19 - 15*t2 - 7*Power(t2,2) + 3*Power(t2,3))))) - 
         Power(s,3)*(19 - 88*t1 + 85*Power(t1,2) - 11*t2 + 50*s2*t2 - 
            82*t1*t2 - 118*Power(t1,2)*t2 + 60*Power(t2,2) - 
            126*s2*Power(t2,2) + 35*Power(s2,2)*Power(t2,2) + 
            114*t1*Power(t2,2) - 4*s2*t1*Power(t2,2) + 
            52*Power(t1,2)*Power(t2,2) - 64*Power(t2,3) + 
            102*s2*Power(t2,3) - 41*Power(s2,2)*Power(t2,3) - 
            32*t1*Power(t2,3) + 6*s2*t1*Power(t2,3) - 
            8*Power(t1,2)*Power(t2,3) + 116*Power(t2,4) - 
            36*s2*Power(t2,4) + 7*Power(s2,2)*Power(t2,4) + 
            4*t1*Power(t2,4) - 26*Power(t2,5) + 6*s2*Power(t2,5) + 
            4*t1*Power(t2,5) + 
            5*Power(s1,5)*(2*Power(s2,2) + 7*Power(t1,2) - 
               2*s2*(1 + 4*t1) + t1*(8 - 7*t2) - 2*(-1 + t2)*t2) + 
            Power(s1,4)*(-6 + Power(t1,2)*(65 - 40*t2) - 24*t2 + 
               27*Power(t2,2) + 10*Power(t2,3) - 
               2*Power(s2,2)*(-8 + 7*t2) + 
               4*t1*(8 - 27*t2 + 15*Power(t2,2)) + 
               2*s2*((12 - 7*t2)*t2 + t1*(-29 + 15*t2))) + 
            Power(s1,3)*(6 + 22*t2 - 26*Power(t2,2) - 43*Power(t2,3) - 
               4*Power(t2,4) + 
               Power(s2,2)*(18 - 23*t2 - 12*Power(t2,2)) + 
               2*Power(t1,2)*(32 - 33*t2 + 5*Power(t2,2)) + 
               t1*(120 - 310*t2 + 68*Power(t2,2) - 32*Power(t2,3)) + 
               2*s2*(-16 + 24*t2 + 55*Power(t2,2) + 8*Power(t2,3) + 
                  t1*(-28 + 23*t2 + 10*Power(t2,2)))) - 
            s1*(2 + (-105 - 150*s2 + 62*Power(s2,2))*t2 + 
               (56 + 90*s2 - 88*Power(s2,2))*Power(t2,2) + 
               (32 + 22*s2 + 27*Power(s2,2))*Power(t2,3) - 
               2*(16 + s2)*Power(t2,4) + (10 - 4*s2)*Power(t2,5) + 
               Power(t1,2)*(-65 + 110*t2 - 50*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-72 + 365*t2 - 286*Power(t2,2) + 42*Power(t2,3) + 
                  10*Power(t2,4) - 
                  2*s2*(-25 + 44*t2 - 17*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,2)*(-32 + 28*t2 + 19*Power(t2,2) + 25*Power(t2,3) + 
               25*Power(t2,4) + 2*Power(t2,5) + 
               2*Power(t1,2)*(35 - 49*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*(23 - 61*t2 + 31*Power(t2,2) + 
                  12*Power(t2,3)) + 
               2*t1*(66 - 214*t2 + 118*Power(t2,2) + 3*Power(t2,3) + 
                  3*Power(t2,4)) - 
               2*s2*(19 - 59*t2 - 10*Power(t2,2) + 56*Power(t2,3) + 
                  5*Power(t2,4) + 
                  t1*(28 - 40*t2 - 3*Power(t2,2) + 6*Power(t2,3))))) + 
         s*(-16 + 40*t1 - 24*Power(t1,2) - 8*t2 - 8*s2*t2 - 19*t1*t2 + 
            55*Power(t1,2)*t2 + 5*Power(t2,2) + 6*s2*Power(t2,2) + 
            4*Power(s2,2)*Power(t2,2) - 35*t1*Power(t2,2) - 
            9*s2*t1*Power(t2,2) - 42*Power(t1,2)*Power(t2,2) + 
            64*Power(t2,3) - 30*s2*Power(t2,3) + 
            2*Power(s2,2)*Power(t2,3) + 27*t1*Power(t2,3) + 
            13*s2*t1*Power(t2,3) + 8*Power(t1,2)*Power(t2,3) - 
            53*Power(t2,4) + 11*s2*Power(t2,4) - 
            7*Power(s2,2)*Power(t2,4) + 6*t1*Power(t2,4) + 
            8*Power(t2,5) + 10*s2*Power(t2,5) - 8*t1*Power(t2,5) + 
            Power(s1,7)*(-5*Power(s2,2) - 7*Power(t1,2) + 
               s2*(5 + 12*t1 - 4*t2) + (-1 + t2)*t2 + t1*(-8 + 7*t2)) + 
            Power(s1,6)*(-5 + 14*t2 - 7*Power(t2,2) - 2*Power(t2,3) + 
               2*Power(t1,2)*(1 + 6*t2) + Power(s2,2)*(1 + 9*t2) + 
               t1*(-2 + 22*t2 - 13*Power(t2,2)) + 
               s2*(4 - 22*t2 + 11*Power(t2,2) - t1*(5 + 19*t2))) + 
            Power(s1,5)*(3 - 5*t2 - 9*Power(t2,2) + 10*Power(t2,3) + 
               Power(t2,4) + Power(t1,2)*(14 - 7*t2 - 5*Power(t2,2)) + 
               Power(s2,2)*(2 - t2 + Power(t2,2)) + 
               t1*(-35 + 35*t2 - 9*Power(t2,2) + 6*Power(t2,3)) - 
               s2*(-22 + 8*t2 + Power(t2,2) + 10*Power(t2,3) - 
                  2*t1*(-7 + 3*t2 + 2*Power(t2,2)))) + 
            Power(s1,3)*(40 - 74*t2 - 29*Power(t2,2) + 83*Power(t2,3) - 
               18*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,2)*(1 + 18*t2 - 38*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,2)*(9 - 31*t2 - 14*Power(t2,2) + 
                  23*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(34 - 101*t2 + 60*Power(t2,2) + 34*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s2*(-79 + 146*t2 + 2*Power(t2,2) - 82*Power(t2,3) - 
                  20*Power(t2,4) + 
                  2*t1*(1 + t2 + 17*Power(t2,2) - 9*Power(t2,3)))) + 
            Power(s1,4)*(-29 + 85*t2 - 57*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(-8 - 13*t2 + 10*Power(t2,2)) + 
               t1*(20 + 31*t2 - 71*Power(t2,2) - 7*Power(t2,3)) - 
               Power(s2,2)*(11 - 31*t2 + 20*Power(t2,2) + 
                  9*Power(t2,3)) + 
               s2*(32 - 128*t2 + 88*Power(t2,2) + 32*Power(t2,3) + 
                  3*Power(t2,4) + 
                  t1*(12 - 4*t2 + 9*Power(t2,2) + 3*Power(t2,3)))) - 
            s1*(-67 + (8 + 14*s2 - 8*Power(s2,2))*t2 + 
               (115 - 119*s2 + 59*Power(s2,2))*Power(t2,2) + 
               (-11 + 76*s2 - 57*Power(s2,2))*Power(t2,3) + 
               (-67 - 16*s2 + 10*Power(s2,2))*Power(t2,4) + 
               2*(11 + 8*s2)*Power(t2,5) + 
               Power(t1,2)*(-48 + 91*t2 - 51*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(143 - 179*t2 + 43*Power(t2,2) + 32*Power(t2,3) + 
                  6*Power(t2,4) - 16*Power(t2,5) + 
                  2*s2*(-4 + 8*t2 - 17*Power(t2,2) + 11*Power(t2,3)))) - 
            Power(s1,2)*(76 - 61*t2 - 115*Power(t2,2) + 
               103*Power(t2,3) + 13*Power(t2,4) - 16*Power(t2,5) + 
               Power(t1,2)*(26 - 26*t2 - 24*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,2)*(12 - 48*t2 + 8*Power(t2,2) + 
                  9*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(-94 + 154*t2 - 111*Power(t2,2) + 28*Power(t2,3) + 
                  6*Power(t2,4) + 8*Power(t2,5)) - 
               s2*(48 - 90*t2 - 33*Power(t2,2) + 38*Power(t2,3) + 
                  22*Power(t2,4) + 6*Power(t2,5) + 
                  t1*(-15 + 31*t2 - 72*Power(t2,2) + 24*Power(t2,3))))) + 
         Power(s,2)*(25 - 82*t1 + 61*Power(t1,2) + 11*t2 + 32*s2*t2 - 
            23*t1*t2 - 112*Power(t1,2)*t2 + 71*Power(t2,2) - 
            102*s2*Power(t2,2) + 17*Power(s2,2)*Power(t2,2) + 
            115*t1*Power(t2,2) + 3*s2*t1*Power(t2,2) + 
            68*Power(t1,2)*Power(t2,2) - 141*Power(t2,3) + 
            137*s2*Power(t2,3) - 37*Power(s2,2)*Power(t2,3) - 
            48*t1*Power(t2,3) - 12*Power(t1,2)*Power(t2,3) + 
            112*Power(t2,4) - 38*s2*Power(t2,4) + 
            15*Power(s2,2)*Power(t2,4) - 12*t1*Power(t2,4) - 
            2*s2*t1*Power(t2,4) - 58*Power(t2,5) + 8*s2*Power(t2,5) + 
            4*t1*Power(t2,5) + 
            Power(s1,6)*(10*Power(s2,2) + 21*Power(t1,2) + 
               t1*(25 - 21*t2) - 5*s2*(2 + 6*t1 - t2) - 5*(-1 + t2)*t2) + 
            Power(s1,5)*(6 + Power(t1,2)*(21 - 30*t2) - 33*t2 + 
               20*Power(t2,2) + 7*Power(t2,3) - 
               2*Power(s2,2)*(-5 + 8*t2) + 
               t1*(16 - 73*t2 + 37*Power(t2,2)) + 
               s2*(-6 + 40*t2 - 16*Power(t2,2) + t1*(-22 + 35*t2))) + 
            Power(s1,4)*(Power(s2,2)*(10 - 19*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*(-3 - 24*t2 + 10*Power(t2,2)) + 
               t1*(108 - 185*t2 + 43*Power(t2,2) - 18*Power(t2,3)) - 
               2*(-6 - 8*Power(t2,2) + 15*Power(t2,3) + Power(t2,4)) + 
               s2*(-56 + 46*t2 + 48*Power(t2,2) + 15*Power(t2,3) + 
                  t1*(-4 + 30*t2 + 5*Power(t2,2)))) + 
            Power(s1,3)*(18 - 128*t2 + 129*Power(t2,2) - 8*Power(t2,3) + 
               13*Power(t2,4) + 
               2*Power(t1,2)*(4 - 12*t2 + Power(t2,2)) + 
               Power(s2,2)*(17 - 62*t2 + 39*Power(t2,2) + 
                  16*Power(t2,3)) + 
               2*t1*(48 - 118*t2 + 100*Power(t2,2) + 10*Power(t2,3) + 
                  Power(t2,4)) - 
               2*s2*(24 - 89*t2 + 53*Power(t2,2) + 52*Power(t2,3) + 
                  3*Power(t2,4) + 
                  t1*(8 - 25*t2 + 8*Power(t2,2) + 5*Power(t2,3)))) + 
            Power(s1,2)*(-36 + 42*t2 + 74*Power(t2,2) - 
               117*Power(t2,3) + 29*Power(t2,4) - 8*Power(t2,5) + 
               Power(t1,2)*(9 - 32*t2 + 50*Power(t2,2) - 
                  12*Power(t2,3)) + 
               t1*(75 - 159*t2 + 166*Power(t2,2) - 54*Power(t2,3) - 
                  22*Power(t2,4)) + 
               Power(s2,2)*(33 - 85*t2 + 77*Power(t2,2) - 
                  27*Power(t2,3) - 4*Power(t2,4)) + 
               s2*(-80 + 91*t2 - 78*Power(t2,2) + 32*Power(t2,3) + 
                  36*Power(t2,4) + 2*Power(t2,5) + 
                  2*t1*(-4 + 21*t2 - 30*Power(t2,2) + 8*Power(t2,3)))) - 
            s1*(49 + (-31 - 144*s2 + 58*Power(s2,2))*t2 + 
               (17 + 226*s2 - 129*Power(s2,2))*Power(t2,2) + 
               (47 - 80*s2 + 64*Power(s2,2))*Power(t2,3) + 
               (-64 + 16*s2 - 5*Power(s2,2))*Power(t2,4) + 
               2*(3 + s2)*Power(t2,5) + 
               Power(t1,2)*(21 + 2*t2 - 30*Power(t2,2) + 8*Power(t2,3)) - 
               t1*(162 - 295*t2 + 223*Power(t2,2) - 76*Power(t2,3) + 
                  12*Power(t2,5) + 
                  s2*(-32 + 83*t2 - 76*Power(t2,2) + 10*Power(t2,3) + 
                     2*Power(t2,4))))))*T2q(s1,s))/
     (s*(-1 + s1)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       (-1 + t1)*(-1 + t2)*Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) - 
    (8*(Power(s,7)*((1 + s1)*Power(t1,2) - (-4 + s1)*t1*t2 + 
            3*Power(t2,2)) + Power(s,6)*
          (Power(t1,2)*(-9 - 4*Power(s1,2) + 5*s1*(-2 + t2) + 5*t2) + 
            t2*(6 - 18*t2 + 22*Power(t2,2) + 
               Power(s1,2)*(-1 + s2 + t2) + 
               s1*(1 + 4*s2 - 3*t2 - Power(t2,2)) - 
               s2*(2 + 8*t2 + Power(t2,2))) + 
            t1*(2 - 13*t2 + (27 + s2)*Power(t2,2) + 
               Power(s1,2)*(-3 + 2*s2 + 4*t2) + 
               s1*(4 - 3*t2 - 4*Power(t2,2) + s2*(2 + t2)))) - 
         Power(-1 + t2,3)*(-4 + 8*t1 - 4*Power(t1,2) - t2 - 10*t1*t2 + 
            11*Power(t1,2)*t2 + 23*Power(t2,2) - 16*s2*Power(t2,2) + 
            4*Power(s2,2)*Power(t2,2) + 3*t1*Power(t2,2) - 
            4*s2*t1*Power(t2,2) - 10*Power(t1,2)*Power(t2,2) - 
            29*Power(t2,3) + 27*s2*Power(t2,3) - 
            7*Power(s2,2)*Power(t2,3) - t1*Power(t2,3) + 
            8*s2*t1*Power(t2,3) + 2*Power(t1,2)*Power(t2,3) + 
            13*Power(t2,4) - 13*s2*Power(t2,4) + 
            2*Power(s2,2)*Power(t2,4) - 2*s2*t1*Power(t2,4) - 
            2*Power(t2,5) + 2*s2*Power(t2,5) + 
            Power(s1,5)*(s2 - t1)*(-1 + s2 - t1 + t2) - 
            s1*(-17 + (55 - 32*s2 + 8*Power(s2,2))*t2 + 
               (-62 + 63*s2 - 13*Power(s2,2))*Power(t2,2) + 
               (27 - 35*s2 + 3*Power(s2,2))*Power(t2,3) + 
               (-3 + 4*s2 + Power(s2,2))*Power(t2,4) + 
               Power(t1,2)*(-5 + 13*t2 - 11*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(22 - 4*(9 + 2*s2)*t2 + (9 + 14*s2)*Power(t2,2) + 
                  (7 - 4*s2)*Power(t2,3) - 2*Power(t2,4))) + 
            Power(s1,4)*(Power(s2,2)*(1 - 2*t2) + 
               Power(t1,2)*(1 - 2*t2) + Power(-1 + t2,2) + 
               t1*(3 - 5*t2 + 2*Power(t2,2)) + 
               s2*(-4 + 7*t2 - 3*Power(t2,2) + t1*(-2 + 4*t2))) - 
            Power(s1,3)*(Power(s2,2)*(1 + t2) + 
               Power(-1 + t2,2)*(-3 + 2*t2) + 
               Power(t1,2)*(2 + t2 - Power(t2,2)) + 
               t1*(-7 + 11*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               s2*(9 - 17*t2 + 11*Power(t2,2) - 3*Power(t2,3) + 
                  2*t1*(-1 - 2*t2 + Power(t2,2)))) + 
            Power(s1,2)*(Power(t1,2)*(-1 + 5*t2 - 2*Power(t2,2)) + 
               Power(-1 + t2,2)*(8 - 4*t2 + Power(t2,2)) + 
               t1*(9 - 29*t2 + 23*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*(4 - 5*t2 + Power(t2,2) + 2*Power(t2,3)) - 
               s2*(16 - 45*t2 + 35*Power(t2,2) - 7*Power(t2,3) + 
                  Power(t2,4) + 4*t1*(1 - t2 + Power(t2,2))))) + 
         Power(s,5)*(1 - 16*t1 + 34*Power(t1,2) - 25*t2 + 14*s2*t2 + 
            t1*t2 - 43*Power(t1,2)*t2 + 82*Power(t2,2) + 
            28*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) - 
            76*t1*Power(t2,2) - 5*s2*t1*Power(t2,2) + 
            11*Power(t1,2)*Power(t2,2) - 107*Power(t2,3) - 
            40*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) + 
            74*t1*Power(t2,3) + 4*s2*t1*Power(t2,3) + 57*Power(t2,4) - 
            4*s2*Power(t2,4) + 
            Power(s1,3)*(Power(s2,2) + 6*Power(t1,2) + t1*(7 - 6*t2) - 
               2*(-1 + t2)*t2 - s2*(1 + 6*t1 + t2)) + 
            Power(s1,2)*(-3 - Power(s2,2)*(-1 + t2) + 7*t2 - 
               10*Power(t2,2) + 6*Power(t2,3) - 
               6*Power(t1,2)*(-4 + 3*t2) + 
               s2*(4 - 13*t1 - 6*t2 + 8*t1*t2 + 2*Power(t2,2)) + 
               t1*(8 - 29*t2 + 16*Power(t2,2))) + 
            s1*(5 - (19 + 18*s2 + 2*Power(s2,2))*t2 + 
               (31 + 37*s2 - Power(s2,2))*Power(t2,2) + 
               3*(-9 + s2)*Power(t2,3) - 6*Power(t2,4) + 
               2*Power(t1,2)*(18 - 21*t2 + 5*Power(t2,2)) + 
               t1*(-9 + 18*t2 - 26*Power(t2,2) - 8*Power(t2,3) + 
                  2*s2*(-7 + 5*t2 + Power(t2,2))))) + 
         Power(s,4)*(-7 + 37*t2 - 38*s2*t2 - 161*Power(t2,2) - 
            32*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) + 
            280*Power(t2,3) + 173*s2*Power(t2,3) - 
            5*Power(s2,2)*Power(t2,3) - 220*Power(t2,4) - 
            86*s2*Power(t2,4) + 4*Power(s2,2)*Power(t2,4) + 
            71*Power(t2,5) - 6*s2*Power(t2,5) + 
            Power(t1,2)*(-70 + 145*t2 - 86*Power(t2,2) + 
               13*Power(t2,3)) + 
            t1*(52 + 22*t2 + (-3 + 2*s2)*Power(t2,2) - 
               10*(19 + s2)*Power(t2,3) + 6*(18 + s2)*Power(t2,4)) - 
            Power(s1,4)*(2*Power(s2,2) + 5*t1 + 4*Power(t1,2) + t2 - 
               4*t1*t2 - Power(t2,2) + s2*(-2 - 6*t1 + t2)) + 
            Power(s1,3)*(2 - 9*t2 + 15*Power(t2,2) - 8*Power(t2,3) + 
               Power(s2,2)*(-5 + 6*t2) + Power(t1,2)*(-25 + 24*t2) + 
               t1*(-27 + 54*t2 - 23*Power(t2,2)) + 
               s2*(3 - 25*t1*(-1 + t2) - 8*t2 + Power(t2,2))) - 
            Power(s1,2)*(-1 + 3*t2 + 7*Power(t2,2) + 10*Power(t2,3) - 
               19*Power(t2,4) + 
               Power(s2,2)*(7 - 11*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(52 - 86*t2 + 32*Power(t2,2)) + 
               t1*(16 - 83*t2 + 84*Power(t2,2) - 33*Power(t2,3)) + 
               s2*(6 - 15*t2 + 18*Power(t2,2) + 7*Power(t2,3) - 
                  2*t1*(16 - 25*t2 + 7*Power(t2,2)))) + 
            s1*(-11 + 2*(42 + 12*s2 + 7*Power(s2,2))*t2 - 
               (163 + 125*s2 + 9*Power(s2,2))*Power(t2,2) + 
               (163 + 106*s2 - 6*Power(s2,2))*Power(t2,3) + 
               (-61 + 11*s2)*Power(t2,4) - 12*Power(t2,5) + 
               Power(t1,2)*(-61 + 124*t2 - 72*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(-26 + 63*t2 + 16*Power(t2,2) - 59*Power(t2,3) - 
                  10*Power(t2,4) + 
                  s2*(38 - 78*t2 + 39*Power(t2,2) + Power(t2,3))))) + 
         s*Power(-1 + t2,2)*(16 - 40*t1 + 24*Power(t1,2) + 4*t2 + 
            8*s2*t2 + 35*t1*t2 - 67*Power(t1,2)*t2 - 88*Power(t2,2) + 
            64*s2*Power(t2,2) - 20*Power(s2,2)*Power(t2,2) + 
            16*t1*Power(t2,2) + 27*s2*t1*Power(t2,2) + 
            56*Power(t1,2)*Power(t2,2) + 98*Power(t2,3) - 
            154*s2*Power(t2,3) + 32*Power(s2,2)*Power(t2,3) + 
            13*t1*Power(t2,3) - 40*s2*t1*Power(t2,3) - 
            12*Power(t1,2)*Power(t2,3) - 26*Power(t2,4) + 
            102*s2*Power(t2,4) - 12*Power(s2,2)*Power(t2,4) - 
            32*t1*Power(t2,4) + 11*s2*t1*Power(t2,4) - 6*Power(t2,5) - 
            20*s2*Power(t2,5) + Power(s2,2)*Power(t2,5) + 
            8*t1*Power(t2,5) + 2*Power(t2,6) + 
            Power(s1,5)*(s2 - t1)*(-5 + 3*s2 - 3*t1 + 5*t2) + 
            Power(s1,4)*(Power(t1,2)*(9 - 4*t2) + 
               Power(s2,2)*(7 - 2*t2) + Power(-1 + t2,2)*(1 + 3*t2) + 
               2*t1*(7 - 13*t2 + 6*Power(t2,2)) + 
               2*s2*(-5 + 9*t2 - 4*Power(t2,2) + t1*(-8 + 3*t2))) - 
            Power(s1,3)*(Power(t1,2)*(7 + 12*t2 - Power(t2,2)) + 
               Power(s2,2)*(11 + 4*t2 + 3*Power(t2,2)) + 
               Power(-1 + t2,2)*(5 + 5*t2 + 6*Power(t2,2)) + 
               t1*(1 + 20*t2 - 31*Power(t2,2) + 10*Power(t2,3)) - 
               s2*(t1*(21 + 10*t2 + 5*Power(t2,2)) + 
                  3*(7 - 9*t2 + Power(t2,2) + Power(t2,3)))) + 
            s1*(-55 + (183 - 118*s2 + 24*Power(s2,2))*t2 + 
               (-197 + 291*s2 - 43*Power(s2,2))*Power(t2,2) + 
               (53 - 217*s2 + 18*Power(s2,2))*Power(t2,3) + 
               14*(2 + 3*s2)*Power(t2,4) + 2*(-6 + s2)*Power(t2,5) + 
               Power(t1,2)*(-12 + 38*t2 - 35*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(95 - 181*t2 + 49*Power(t2,2) + 56*Power(t2,3) - 
                  19*Power(t2,4) + 
                  s2*(-8 - 12*t2 + 39*Power(t2,2) - 18*Power(t2,3) + 
                     Power(t2,4)))) + 
            Power(s1,2)*(Power(t1,2)*(3 + t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(-4 + 22*t2 - 9*Power(t2,2) + Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-14 - t2 + 14*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(-13 + 69*t2 - 64*Power(t2,2) + 5*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s2*(t1*(9 - 44*t2 + 21*Power(t2,2) - 6*Power(t2,3)) - 
                  2*(-15 + 67*t2 - 72*Power(t2,2) + 19*Power(t2,3) + 
                     Power(t2,4))))) + 
         Power(s,3)*(19 - 88*t1 + 85*Power(t1,2) - 29*t2 + 50*s2*t2 + 
            32*t1*t2 - 250*Power(t1,2)*t2 + 84*Power(t2,2) + 
            28*s2*Power(t2,2) - 17*Power(s2,2)*Power(t2,2) + 
            146*t1*Power(t2,2) + 26*s2*t1*Power(t2,2) + 
            243*Power(t1,2)*Power(t2,2) - 270*Power(t2,3) - 
            332*s2*Power(t2,3) + 33*Power(s2,2)*Power(t2,3) + 
            72*t1*Power(t2,3) - 30*s2*t1*Power(t2,3) - 
            87*Power(t1,2)*Power(t2,3) + 355*Power(t2,4) + 
            358*s2*Power(t2,4) - 23*Power(s2,2)*Power(t2,4) - 
            252*t1*Power(t2,4) + 2*s2*t1*Power(t2,4) + 
            8*Power(t1,2)*Power(t2,4) - 205*Power(t2,5) - 
            100*s2*Power(t2,5) + 6*Power(s2,2)*Power(t2,5) + 
            90*t1*Power(t2,5) + 4*s2*t1*Power(t2,5) + 46*Power(t2,6) - 
            4*s2*Power(t2,6) + 
            Power(s1,5)*(s2 - t1)*(-1 + s2 - t1 + t2) + 
            Power(s1,4)*(Power(t1,2)*(13 - 14*t2) + 
               Power(s2,2)*(7 - 8*t2) + Power(-1 + t2,2)*(1 + 3*t2) + 
               2*t1*(9 - 16*t2 + 7*Power(t2,2)) + 
               2*s2*(-5 + 8*t2 - 3*Power(t2,2) + t1*(-10 + 11*t2))) + 
            Power(s1,2)*(Power(t1,2)*
                (55 - 145*t2 + 122*Power(t2,2) - 26*Power(t2,3)) + 
               Power(s2,2)*(19 - 45*t2 + 28*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (25 - 50*t2 + 52*Power(t2,2) + 27*Power(t2,3)) + 
               t1*(37 - 206*t2 + 260*Power(t2,2) - 128*Power(t2,3) + 
                  37*Power(t2,4)) - 
               2*s2*(8 - 28*t2 + 3*Power(t2,2) + 7*Power(t2,3) + 
                  10*Power(t2,4) + 
                  t1*(21 - 55*t2 + 43*Power(t2,2) - 3*Power(t2,3)))) + 
            Power(s1,3)*(2*Power(s2,2)*(4 - 11*t2 + 6*Power(t2,2)) - 
               Power(-1 + t2,2)*(3 + 11*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(37 - 76*t2 + 37*Power(t2,2)) - 
               2*t1*(-27 + 76*t2 - 72*Power(t2,2) + 23*Power(t2,3)) + 
               s2*(t1*(-37 + 82*t2 - 41*Power(t2,2)) + 
                  2*(-5 + 18*t2 - 22*Power(t2,2) + 9*Power(t2,3)))) + 
            s1*(-14 - 2*(21 + 22*s2 + 11*Power(s2,2))*t2 + 
               (239 + 268*s2 + 24*Power(s2,2))*Power(t2,2) + 
               (-381 - 386*s2 + 6*Power(s2,2))*Power(t2,3) + 
               (273 + 147*s2 - 11*Power(s2,2))*Power(t2,4) + 
               5*(-13 + 3*s2)*Power(t2,5) - 10*Power(t2,6) + 
               Power(t1,2)*(47 - 144*t2 + 143*Power(t2,2) - 
                  54*Power(t2,3) + 5*Power(t2,4)) + 
               t1*(114 - 350*t2 + 274*Power(t2,2) + 36*Power(t2,3) - 
                  67*Power(t2,4) - 7*Power(t2,5) + 
                  s2*(-50 + 158*t2 - 147*Power(t2,2) + 44*Power(t2,3) + 
                     Power(t2,4))))) + 
         Power(s,2)*(-1 + t2)*
          (25 - 82*t1 + 61*Power(t1,2) - 3*t2 + 32*s2*t2 + 45*t1*t2 - 
            174*Power(t1,2)*t2 - 79*Power(t2,2) + 80*s2*Power(t2,2) - 
            31*Power(s2,2)*Power(t2,2) + 102*t1*Power(t2,2) + 
            47*s2*t1*Power(t2,2) + 153*Power(t1,2)*Power(t2,2) + 
            26*Power(t2,3) - 319*s2*Power(t2,3) + 
            51*Power(s2,2)*Power(t2,3) + 26*t1*Power(t2,3) - 
            63*s2*t1*Power(t2,3) - 43*Power(t1,2)*Power(t2,3) + 
            87*Power(t2,4) + 273*s2*Power(t2,4) - 
            25*Power(s2,2)*Power(t2,4) - 132*t1*Power(t2,4) + 
            17*s2*t1*Power(t2,4) + 2*Power(t1,2)*Power(t2,4) - 
            71*Power(t2,5) - 65*s2*Power(t2,5) + 
            4*Power(s2,2)*Power(t2,5) + 41*t1*Power(t2,5) + 
            s2*t1*Power(t2,5) + 15*Power(t2,6) - s2*Power(t2,6) + 
            3*Power(s1,5)*(s2 - t1)*(-1 + s2 - t1 + t2) + 
            Power(s1,4)*(Power(s2,2)*(9 - 12*t2) + 
               Power(-1 + t2,2)*(7 + 5*t2) - 3*Power(t1,2)*(-5 + 6*t2) + 
               t1*(24 - 50*t2 + 26*Power(t2,2)) + 
               2*s2*(-8 + 17*t2 - 9*Power(t2,2) + 3*t1*(-4 + 5*t2))) + 
            Power(s1,3)*(Power(s2,2)*(6 - 26*t2 + 4*Power(t2,2)) - 
               Power(-1 + t2,2)*(-13 + 23*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(23 - 60*t2 + 21*Power(t2,2)) - 
               2*t1*(-38 + 83*t2 - 65*Power(t2,2) + 20*Power(t2,3)) + 
               s2*(-32 + 60*t2 - 50*Power(t2,2) + 22*Power(t2,3) + 
                  t1*(-23 + 74*t2 - 19*Power(t2,2)))) + 
            Power(s1,2)*(Power(t1,2)*
                (29 - 57*t2 + 56*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s2,2)*(17 - 15*t2 + 10*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (21 - 51*t2 + 50*Power(t2,2) + 16*Power(t2,3)) + 
               t1*(32 - 111*t2 + 115*Power(t2,2) - 55*Power(t2,3) + 
                  19*Power(t2,4)) - 
               s2*(14 + 13*t2 - 83*Power(t2,2) + 41*Power(t2,3) + 
                  15*Power(t2,4) + 
                  4*t1*(6 - 5*t2 + 7*Power(t2,2) + 2*Power(t2,3)))) + 
            s1*(-58 + 3*(51 - 40*s2 + 2*Power(s2,2))*t2 + 
               (-75 + 408*s2 - 26*Power(s2,2))*Power(t2,2) + 
               6*(-21 - 69*s2 + 4*Power(s2,2))*Power(t2,3) + 
               (152 + 117*s2 - 7*Power(s2,2))*Power(t2,4) + 
               (-43 + 9*s2)*Power(t2,5) - 3*Power(t2,6) + 
               Power(t1,2)*(6 - 17*t2 + 14*Power(t2,2) - 7*Power(t2,3) + 
                  Power(t2,4)) + 
               t1*(156 - 386*t2 + 217*Power(t2,2) + 65*Power(t2,3) - 
                  50*Power(t2,4) - 2*Power(t2,5) + 
                  s2*(-32 + 71*t2 - 34*Power(t2,2) - Power(t2,3) + 
                     2*Power(t2,4))))))*T3q(1 - s + s1 - t2,s1))/
     (s*(-1 + s1)*(-1 + t1)*(-1 + t2)*Power(-1 + s + t2,2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) + 
    (8*(Power(s,6)*((1 + s1)*Power(t1,2) - (-4 + s1)*t1*t2 + 
            3*Power(t2,2)) + Power(s,5)*
          (Power(t1,2)*(-6 - 6*Power(s1,2) + 4*t2 + s1*(-9 + 4*t2)) - 
            t2*(-6 + t2 - 13*Power(t2,2) - Power(s1,2)*(-1 + s2 + t2) + 
               s2*(2 + 8*t2 + Power(t2,2)) + 
               s1*(-1 - 4*s2 + 9*t2 + Power(t2,2))) + 
            t1*(2 - t2 + (21 + s2)*Power(t2,2) + 
               Power(s1,2)*(-3 + 2*s2 + 6*t2) + 
               s1*(4 - 14*t2 - 3*Power(t2,2) + s2*(2 + t2)))) + 
         Power(s,4)*(1 - 10*t1 + 13*Power(t1,2) - 7*t2 + 8*s2*t2 - 
            16*t1*t2 - 25*Power(t1,2)*t2 + 30*Power(t2,2) - 
            2*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) - 
            12*t1*Power(t2,2) + 7*Power(t1,2)*Power(t2,2) + 
            Power(t2,3) - 31*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) + 
            41*t1*Power(t2,3) + 3*s2*t1*Power(t2,3) + 20*Power(t2,4) - 
            3*s2*Power(t2,4) + 
            Power(s1,3)*(Power(s2,2) + 15*Power(t1,2) + 
               t1*(13 - 15*t2) - 4*(-1 + t2)*t2 - s2*(1 + 10*t1 + 3*t2)) \
+ Power(s1,2)*(-3 + Power(t1,2)*(27 - 20*t2) - Power(s2,2)*(-1 + t2) + 
               2*t2 + 3*Power(t2,2) + 7*Power(t2,3) + 
               t1*(-9 + 2*t2 + 16*Power(t2,2)) + 
               s2*(4 - 11*t2 + Power(t2,2) + t1*(-11 + 4*t2))) + 
            s1*(5 - 2*(6 + s2 + Power(s2,2))*t2 - 
               (9 - 47*s2 + Power(s2,2))*Power(t2,2) + 
               5*(-5 + s2)*Power(t2,3) - 3*Power(t2,4) + 
               Power(t1,2)*(24 - 29*t2 + 6*Power(t2,2)) - 
               t1*(1 + 3*t2 + 63*Power(t2,2) + 3*Power(t2,3) + 
                  s2*(8 - 11*t2 + Power(t2,2))))) + 
         Power(s1 - t2,2)*(-4 + 8*t1 - 4*Power(t1,2) - t2 - 10*t1*t2 + 
            11*Power(t1,2)*t2 + 23*Power(t2,2) - 16*s2*Power(t2,2) + 
            4*Power(s2,2)*Power(t2,2) + 3*t1*Power(t2,2) - 
            4*s2*t1*Power(t2,2) - 10*Power(t1,2)*Power(t2,2) - 
            29*Power(t2,3) + 27*s2*Power(t2,3) - 
            7*Power(s2,2)*Power(t2,3) - t1*Power(t2,3) + 
            8*s2*t1*Power(t2,3) + 2*Power(t1,2)*Power(t2,3) + 
            13*Power(t2,4) - 13*s2*Power(t2,4) + 
            2*Power(s2,2)*Power(t2,4) - 2*s2*t1*Power(t2,4) - 
            2*Power(t2,5) + 2*s2*Power(t2,5) + 
            Power(s1,5)*(s2 - t1)*(-1 + s2 - t1 + t2) - 
            s1*(-17 + (55 - 32*s2 + 8*Power(s2,2))*t2 + 
               (-62 + 63*s2 - 13*Power(s2,2))*Power(t2,2) + 
               (27 - 35*s2 + 3*Power(s2,2))*Power(t2,3) + 
               (-3 + 4*s2 + Power(s2,2))*Power(t2,4) + 
               Power(t1,2)*(-5 + 13*t2 - 11*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(22 - 4*(9 + 2*s2)*t2 + (9 + 14*s2)*Power(t2,2) + 
                  (7 - 4*s2)*Power(t2,3) - 2*Power(t2,4))) + 
            Power(s1,4)*(Power(s2,2)*(1 - 2*t2) + 
               Power(t1,2)*(1 - 2*t2) + Power(-1 + t2,2) + 
               t1*(3 - 5*t2 + 2*Power(t2,2)) + 
               s2*(-4 + 7*t2 - 3*Power(t2,2) + t1*(-2 + 4*t2))) - 
            Power(s1,3)*(Power(s2,2)*(1 + t2) + 
               Power(-1 + t2,2)*(-3 + 2*t2) + 
               Power(t1,2)*(2 + t2 - Power(t2,2)) + 
               t1*(-7 + 11*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               s2*(9 - 17*t2 + 11*Power(t2,2) - 3*Power(t2,3) + 
                  2*t1*(-1 - 2*t2 + Power(t2,2)))) + 
            Power(s1,2)*(Power(t1,2)*(-1 + 5*t2 - 2*Power(t2,2)) + 
               Power(-1 + t2,2)*(8 - 4*t2 + Power(t2,2)) + 
               t1*(9 - 29*t2 + 23*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*(4 - 5*t2 + Power(t2,2) + 2*Power(t2,3)) - 
               s2*(16 - 45*t2 + 35*Power(t2,2) - 7*Power(t2,3) + 
                  Power(t2,4) + 4*t1*(1 - t2 + Power(t2,2))))) + 
         s*(s1 - t2)*(-8 + 16*t1 - 8*Power(t1,2) - 28*t2 + 16*t1*t2 + 
            12*Power(t1,2)*t2 + 25*Power(t2,2) + 42*s2*Power(t2,2) - 
            8*Power(s2,2)*Power(t2,2) - 81*t1*Power(t2,2) + 
            6*s2*t1*Power(t2,2) - 4*Power(t1,2)*Power(t2,2) + 
            46*Power(t2,3) - 86*s2*Power(t2,3) + 
            18*Power(s2,2)*Power(t2,3) + 68*t1*Power(t2,3) - 
            15*s2*t1*Power(t2,3) + 2*Power(t1,2)*Power(t2,3) - 
            35*Power(t2,4) + 29*s2*Power(t2,4) - 
            5*Power(s2,2)*Power(t2,4) - 4*t1*Power(t2,4) + 
            3*s2*t1*Power(t2,4) - 2*Power(t2,5) + 4*s2*Power(t2,5) - 
            Power(s2,2)*Power(t2,5) - 4*t1*Power(t2,5) + 2*Power(t2,6) + 
            Power(s1,5)*(-4*Power(s2,2) - 6*Power(t1,2) + 
               s2*(4 + 10*t1 - 3*t2) + (-1 + t2)*t2 + t1*(-7 + 6*t2)) + 
            Power(s1,4)*(t2 + 2*Power(t2,2) - 3*Power(t2,3) + 
               Power(s2,2)*(-4 + 9*t2) + Power(t1,2)*(-9 + 14*t2) + 
               t1*(-12 + 30*t2 - 13*Power(t2,2)) + 
               s2*(8 + t1*(11 - 21*t2) - 24*t2 + 11*Power(t2,2))) + 
            Power(s1,3)*(-2 + 5*t2 + Power(t1,2)*(17 - 10*t2)*t2 - 
               3*Power(t2,2) - 3*Power(t2,3) + 3*Power(t2,4) + 
               Power(s2,2)*(-2 + 9*t2 - 2*Power(t2,2)) + 
               s2*(24 + t1 - 47*t2 - 26*t1*t2 + 30*Power(t2,2) + 
                  13*t1*Power(t2,2) - 15*Power(t2,3)) + 
               t1*(-30 + 56*t2 - 26*Power(t2,2) + 8*Power(t2,3))) + 
            s1*(44 + (11 - 76*s2 + 16*Power(s2,2))*t2 - 
               2*(74 - 90*s2 + 19*Power(s2,2))*Power(t2,2) + 
               (84 - 81*s2 + 11*Power(s2,2))*Power(t2,3) + 
               (15 - 14*s2 + 6*Power(s2,2))*Power(t2,4) - 
               2*(3 + s2)*Power(t2,5) + 
               Power(t1,2)*(4 + 9*t2 - 22*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-48 - 20*(-5 + s2)*t2 + 47*(-2 + s2)*Power(t2,2) + 
                  (26 - 18*s2)*Power(t2,3) + (9 + s2)*Power(t2,4))) - 
            Power(s1,2)*(36 - 88*t2 + 46*Power(t2,2) + 11*Power(t2,3) - 
               6*Power(t2,4) + Power(t2,5) + 
               Power(t1,2)*(5 - 4*t2 + 4*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*(8 - 22*t2 + 11*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(19 - 88*t2 + 82*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(-34 + 118*t2 - 91*Power(t2,2) - 9*Power(t2,4) + 
                  t1*(-14 + 33*t2 - 30*Power(t2,2) + 3*Power(t2,3))))) - 
         Power(s,3)*(4 - 16*t1 + 12*Power(t1,2) + 3*t2 + 8*s2*t2 + 
            9*t1*t2 - 48*Power(t1,2)*t2 + 24*Power(t2,2) - 
            48*s2*Power(t2,2) + 4*Power(s2,2)*Power(t2,2) + 
            82*t1*Power(t2,2) + s2*t1*Power(t2,2) + 
            38*Power(t1,2)*Power(t2,2) - 51*Power(t2,3) + 
            s2*Power(t2,3) - 3*Power(s2,2)*Power(t2,3) + 
            17*t1*Power(t2,3) - s2*t1*Power(t2,3) - 
            6*Power(t1,2)*Power(t2,3) - 11*Power(t2,4) + 
            46*s2*Power(t2,4) - 3*Power(s2,2)*Power(t2,4) - 
            39*t1*Power(t2,4) - 3*s2*t1*Power(t2,4) - 11*Power(t2,5) + 
            3*s2*Power(t2,5) + 
            Power(s1,4)*(4*Power(s2,2) + 20*Power(t1,2) + 
               t1*(22 - 20*t2) - 6*(-1 + t2)*t2 - 2*s2*(2 + 10*t1 + t2)) \
+ Power(s1,3)*(-8 + Power(t1,2)*(38 - 40*t2) + Power(s2,2)*(4 - 7*t2) + 
               10*t2 - 14*Power(t2,2) + 15*Power(t2,3) + 
               t1*t2*(-45 + 34*t2) + 
               s2*(8 - 2*Power(t2,2) + t1*(-23 + 26*t2))) - 
            Power(s1,2)*(-10 - 9*t2 + 39*Power(t2,2) - 2*Power(t2,3) + 
               12*Power(t2,4) + 
               Power(t1,2)*(-34 + 66*t2 - 24*Power(t2,2)) + 
               Power(s2,2)*(-4 + 11*t2 + Power(t2,2)) + 
               t1*(-16 + 43*t2 + 39*Power(t2,2) + 15*Power(t2,3)) + 
               s2*(6 + 5*t2 - 68*Power(t2,2) - 13*Power(t2,3) + 
                  t1*(21 - 43*t2 + 9*Power(t2,2)))) + 
            s1*(-2 + (-38 + 38*s2 - 8*Power(s2,2))*t2 + 
               (53 + 4*s2 + 10*Power(s2,2))*Power(t2,2) + 
               (44 - 110*s2 + 7*Power(s2,2))*Power(t2,3) + 
               (17 - 12*s2)*Power(t2,4) + 3*Power(t2,5) + 
               Power(t1,2)*(29 - 72*t2 + 33*Power(t2,2) - 
                  4*Power(t2,3)) + 
               t1*(9 - 94*t2 + 24*Power(t2,2) + 97*Power(t2,3) + 
                  Power(t2,4) + 
                  s2*(-8 + 36*t2 - 27*Power(t2,2) + 6*Power(t2,3))))) + 
         Power(s,2)*(4 - 8*t1 + 4*Power(t1,2) - 7*t2 + 42*t1*t2 - 
            35*Power(t1,2)*t2 - 44*Power(t2,2) - 34*s2*Power(t2,2) + 
            4*Power(s2,2)*Power(t2,2) + 71*t1*Power(t2,2) - 
            2*s2*t1*Power(t2,2) + 49*Power(t1,2)*Power(t2,2) - 
            34*Power(t2,3) + 99*s2*Power(t2,3) - 
            15*Power(s2,2)*Power(t2,3) - 131*t1*Power(t2,3) + 
            6*s2*t1*Power(t2,3) - 23*Power(t1,2)*Power(t2,3) + 
            49*Power(t2,4) - 17*s2*Power(t2,4) + 
            5*Power(s2,2)*Power(t2,4) - 2*t1*Power(t2,4) + 
            2*Power(t1,2)*Power(t2,4) + 13*Power(t2,5) - 
            29*s2*Power(t2,5) + 3*Power(s2,2)*Power(t2,5) + 
            19*t1*Power(t2,5) + s2*t1*Power(t2,5) - Power(t2,6) - 
            s2*Power(t2,6) + Power(s1,5)*
             (6*Power(s2,2) + 15*Power(t1,2) - 4*(-1 + t2)*t2 + 
               s2*(-6 - 20*t1 + 2*t2) - 3*t1*(-6 + 5*t2)) + 
            s1*(-9 + (51 + 52*s2 - 8*Power(s2,2))*t2 + 
               (121 - 187*s2 + 37*Power(s2,2))*Power(t2,2) + 
               (-107 + 47*s2 - 17*Power(s2,2))*Power(t2,3) + 
               (-58 + 93*s2 - 12*Power(s2,2))*Power(t2,4) + 
               (7 + 9*s2)*Power(t2,5) - Power(t2,6) + 
               Power(t1,2)*(19 - 55*t2 + 65*Power(t2,2) - 
                  15*Power(t2,3) + Power(t2,4)) - 
               t1*(10 - 4*(-37 + 5*s2)*t2 + 
                  10*(-25 + 6*s2)*Power(t2,2) + 
                  (40 - 31*s2)*Power(t2,3) + (63 + 5*s2)*Power(t2,4))) + 
            Power(s1,4)*(-6 + Power(t1,2)*(27 - 40*t2) + 
               Power(s2,2)*(6 - 15*t2) + 8*t2 - 15*Power(t2,2) + 
               13*Power(t2,3) + t1*(14 - 67*t2 + 36*Power(t2,2)) + 
               s2*(2*(13 - 6*t2)*t2 + t1*(-23 + 44*t2))) + 
            Power(s1,3)*(4 + 20*t2 - 36*Power(t2,2) + 23*Power(t2,3) - 
               15*Power(t2,4) + 
               Power(s2,2)*(7 - 19*t2 + 6*Power(t2,2)) + 
               2*Power(t1,2)*(9 - 32*t2 + 18*Power(t2,2)) + 
               t1*(38 - 90*t2 + 43*Power(t2,2) - 27*Power(t2,3)) + 
               s2*(-21 + 29*t2 + Power(t2,2) + 25*Power(t2,3) + 
                  t1*(-16 + 61*t2 - 31*Power(t2,2)))) + 
            Power(s1,2)*(9 - 91*t2 + 40*Power(t2,2) + 73*Power(t2,3) - 
               18*Power(t2,4) + 7*Power(t2,5) + 
               Power(t1,2)*(22 - 60*t2 + 46*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(s2,2)*(4 - 29*t2 + 25*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(45 - 157*t2 + 126*Power(t2,2) + 50*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s2*(-18 + 109*t2 - 59*Power(t2,2) - 85*Power(t2,3) - 
                  23*Power(t2,4) + 
                  t1*(-18 + 70*t2 - 69*Power(t2,2) + 11*Power(t2,3))))))*
       T4q(-s + s1 - t2))/
     (s*(-1 + s1)*(-1 + t1)*(-s + s1 - t2)*(-1 + t2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) + 
    (8*(Power(s,5)*((1 + s1)*Power(t1,2) - (-4 + s1)*t1*t2 + 
            3*Power(t2,2)) - 2*Power(s1 - t2,2)*Power(-1 + t2,2)*
          (-7 + Power(s1,2)*(s2 - t1) + 4*t1 + 3*Power(t1,2) + 6*t2 - 
            12*t1*t2 + Power(t2,2) + 
            s2*(4 + t1*(-4 + t2) + 5*t2 - Power(t2,2)) + 
            3*s1*(1 + s2*(-3 + t1) - Power(t1,2) - t2 + t1*(2 + t2))) + 
         Power(s,4)*(Power(t1,2)*
             (-6 - 7*s1 - 4*Power(s1,2) + 2*t2 + 2*s1*t2) - 
            t2*(-6 + t2 - 7*Power(t2,2) - Power(s1,2)*(-1 + s2 + t2) + 
               s1*(-1 - 4*s2 + 3*t2 + Power(t2,2)) + 
               s2*(2 + 8*t2 + Power(t2,2))) + 
            t1*(2 - t2 + (13 + s2)*Power(t2,2) + 
               Power(s1,2)*(-3 + 2*s2 + 4*t2) + 
               s1*(4 - 6*t2 - Power(t2,2) + s2*(2 + t2)))) + 
         Power(s,3)*(1 - 10*t1 + 13*Power(t1,2) - 7*t2 + 8*s2*t2 - 
            20*t1*t2 - 13*Power(t1,2)*t2 + 10*Power(t2,2) + 
            2*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) + 
            6*t1*Power(t2,2) - 2*Power(t1,2)*Power(t2,2) + 
            11*Power(t2,3) - 15*s2*Power(t2,3) + 
            Power(s2,2)*Power(t2,3) + 17*t1*Power(t2,3) + 
            s2*t1*Power(t2,3) + Power(t2,4) - s2*Power(t2,4) + 
            Power(s1,3)*(Power(s2,2) + 6*Power(t1,2) + t1*(7 - 6*t2) - 
               2*(-1 + t2)*t2 - s2*(1 + 6*t1 + t2)) - 
            Power(s1,2)*(3 + 6*Power(t1,2)*(-2 + t2) + 
               Power(s2,2)*(-1 + t2) - 4*t2 + 4*Power(t2,2) - 
               3*Power(t2,3) + t1*(1 + 8*t2 - 4*Power(t2,2)) + 
               s2*(-4 + t1*(7 - 2*t2) + 3*t2 + Power(t2,2))) + 
            s1*(5 - 17*Power(t2,2) - Power(t2,3) - Power(t2,4) - 
               Power(s2,2)*t2*(2 + t2) + 
               Power(t1,2)*(12 - 9*t2 + Power(t2,2)) + 
               s2*t2*(-6 + 23*t2 + 3*Power(t2,2)) - 
               t1*(-3 + 13*t2 + 11*Power(t2,2) + 
                  s2*(8 - 7*t2 + Power(t2,2))))) + 
         s*(4 - 8*t1 + 4*Power(t1,2) + t2 + 10*t1*t2 - 
            11*Power(t1,2)*t2 - 35*Power(t2,2) - 2*s2*Power(t2,2) + 
            4*Power(s2,2)*Power(t2,2) + 35*t1*Power(t2,2) - 
            18*s2*t1*Power(t2,2) + 20*Power(t1,2)*Power(t2,2) + 
            59*Power(t2,3) + 35*s2*Power(t2,3) - 
            7*Power(s2,2)*Power(t2,3) - 93*t1*Power(t2,3) + 
            24*s2*t1*Power(t2,3) - 12*Power(t1,2)*Power(t2,3) - 
            39*Power(t2,4) - 39*s2*Power(t2,4) + 
            2*Power(s2,2)*Power(t2,4) + 56*t1*Power(t2,4) - 
            4*s2*t1*Power(t2,4) - 2*Power(t1,2)*Power(t2,4) + 
            12*Power(t2,5) + 6*s2*Power(t2,5) - 2*Power(t2,6) + 
            Power(s1,5)*(s2 - t1)*(-1 + s2 - t1 + t2) + 
            s1*(-17 + (51 + 20*s2 - 8*Power(s2,2))*t2 + 
               (-60 - 111*s2 + 13*Power(s2,2))*Power(t2,2) + 
               (37 + 99*s2 - 3*Power(s2,2))*Power(t2,3) - 
               (13 + 8*s2 + Power(s2,2))*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,2)*(-5 + 5*t2 - 19*Power(t2,2) + 
                  20*Power(t2,3)) - 
               t1*(-22 + (96 - 20*s2)*t2 + (-181 + 14*s2)*Power(t2,2) + 
                  (99 + 8*s2)*Power(t2,3) + 8*Power(t2,4))) + 
            Power(s1,4)*(Power(s2,2)*(1 - 2*t2) + 
               Power(t1,2)*(1 - 2*t2) + Power(-1 + t2,2) + 
               t1*(3 - 5*t2 + 2*Power(t2,2)) + 
               s2*(-4 + 7*t2 - 3*Power(t2,2) + t1*(-2 + 4*t2))) + 
            Power(s1,2)*(Power(t1,2)*(-1 + 17*t2 - 14*Power(t2,2)) + 
               Power(-1 + t2,2)*(8 + 6*t2 + 3*Power(t2,2)) + 
               t1*(13 - 73*t2 + 59*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(4 - 5*t2 + Power(t2,2) + 2*Power(t2,3)) - 
               s2*(18 - 85*t2 + 69*Power(t2,2) - 3*Power(t2,3) + 
                  Power(t2,4) + t1*(2 + 12*t2 - 10*Power(t2,2)))) - 
            Power(s1,3)*(Power(s2,2)*(1 + t2) + 
               Power(-1 + t2,2)*(5 + 4*t2) + 
               Power(t1,2)*(2 + t2 - Power(t2,2)) + 
               t1*(-17 + 27*t2 - 11*Power(t2,2) + Power(t2,3)) + 
               s2*(9 - 13*t2 + 7*Power(t2,2) - 3*Power(t2,3) + 
                  2*t1*(-1 - 2*t2 + Power(t2,2))))) + 
         Power(s,2)*(-4 + 16*t1 - 12*Power(t1,2) - 5*t2 - 8*s2*t2 + 
            11*t1*t2 + 22*Power(t1,2)*t2 + 6*Power(t2,2) + 
            24*s2*Power(t2,2) - 4*Power(s2,2)*Power(t2,2) - 
            64*t1*Power(t2,2) + 7*s2*t1*Power(t2,2) - 
            10*Power(t1,2)*Power(t2,2) - 21*Power(t2,3) - 
            15*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) + 
            32*t1*Power(t2,3) + s2*t1*Power(t2,3) - 
            6*Power(t1,2)*Power(t2,3) + 30*Power(t2,4) - 
            6*s2*Power(t2,4) + Power(s2,2)*Power(t2,4) + 
            10*t1*Power(t2,4) - 6*Power(t2,5) - 
            Power(s1,4)*(2*Power(s2,2) + 5*t1 + 4*Power(t1,2) + t2 - 
               4*t1*t2 - Power(t2,2) + s2*(-2 - 6*t1 + t2)) + 
            Power(s1,3)*(2 - 3*t2 + 3*Power(t2,2) - 2*Power(t2,3) + 
               Power(s2,2)*(-2 + 3*t2) + Power(t1,2)*(-7 + 6*t2) + 
               s2*(-7*t1*(-1 + t2) + 4*(-2 + t2)*t2) + 
               t1*(-6 + 15*t2 - 5*Power(t2,2))) + 
            Power(s1,2)*(-2*Power(t1,2)*(2 - 4*t2 + Power(t2,2)) + 
               Power(s2,2)*(-4 + 5*t2 + Power(t2,2)) + 
               t2*(-17 + 16*t2 + Power(t2,3)) + 
               t1*(-12 + 40*t2 - 15*Power(t2,2) + Power(t2,3)) + 
               s2*(6 - 13*t2 - 2*Power(t2,2) - 5*Power(t2,3) + 
                  t1*(5 - 11*t2 + 2*Power(t2,2)))) + 
            s1*(4 + (-2 - 22*s2 + 8*Power(s2,2))*t2 + 
               (37 + 28*s2 - 4*Power(s2,2))*Power(t2,2) + 
               (-43 + 14*s2 - 3*Power(s2,2))*Power(t2,3) + 
               2*(2 + s2)*Power(t2,4) + 
               Power(t1,2)*(-3 + 2*t2 + 10*Power(t2,2)) - 
               t1*(29 - 84*t2 + 72*Power(t2,2) + 5*Power(t2,3) + 
                  s2*(-8 + 20*t2 - 3*Power(t2,2) + Power(t2,3))))))*T5q(s))/
     (s*(-1 + s1)*(-1 + t1)*(-1 + t2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)));
   return a;
};
