#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m213_2_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-16*(-4*Power(s1,5)*t1*t2 + 
         (-1 + t1 - t2)*Power(t2,2)*(1 + t2)*
          (1 + s*(-1 + t2) - 4*t2 + Power(t2,2)) + 
         Power(s1,4)*(Power(t1,2)*(-9 + t2)*t2 + 
            t1*(-1 + (3 - 2*s + s2)*t2 + (14 + 2*s - s2)*Power(t2,2) - 
               4*Power(t2,3)) + 
            t2*(13 - 2*t2 + 3*Power(t2,2) + 2*Power(t2,3) + 
               s2*(-3 + t2 + 2*Power(t2,2)))) + 
         s1*t2*(1 - 2*t2 + 4*s2*t2 - 6*Power(t2,2) - s2*Power(t2,2) - 
            21*Power(t2,3) + 7*s2*Power(t2,3) + 29*Power(t2,4) - 
            10*s2*Power(t2,4) - 9*Power(t2,5) + 
            4*Power(t1,3)*t2*(-3 - 2*t2 + Power(t2,2)) + 
            Power(t1,2)*(-1 + (25 + 4*s2)*t2 + 42*Power(t2,2) + 
               (14 - 4*s2)*Power(t2,3) - 8*Power(t2,4)) + 
            s*(-1 + t2)*(-5 - 10*t2 + Power(t2,2) + Power(t2,3) - 
               3*Power(t1,2)*(2 + t2) + t1*(11 + 15*t2)) + 
            t1*t2*(-8 - 20*t2 - 47*Power(t2,2) + 3*Power(t2,3) + 
               4*Power(t2,4) + 
               s2*(-8 - t2 + 5*Power(t2,2) + 4*Power(t2,3)))) + 
         Power(s1,3)*(8*Power(t1,3)*(-3 + t2)*t2 + 
            Power(t1,2)*(-1 + (54 - 3*s + 8*s2)*t2 + 
               (50 + 3*s - 8*s2)*Power(t2,2) - 15*Power(t2,3)) + 
            t1*(2 - 99*Power(t2,2) - 3*Power(t2,3) + 12*Power(t2,4) - 
               s*t2*(3 - 4*t2 + Power(t2,2)) + 
               s2*t2*(-17 + 11*t2 + 6*Power(t2,2))) - 
            t2*(14 + 17*t2 - 26*Power(t2,2) + 15*Power(t2,3) + 
               4*Power(t2,4) + s*(-7 + 5*t2 + 2*Power(t2,2)) + 
               s2*(-7 - 9*t2 + 12*Power(t2,2) + 4*Power(t2,3)))) - 
         Power(s1,2)*(4*Power(t1,3)*t2*(-3 - 8*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(-1 + (3*s + 4*(6 + s2))*t2 + 
               8*(12 + s2)*Power(t2,2) + (55 - 3*s - 12*s2)*Power(t2,3) - 
               22*Power(t2,4)) + 
            t1*(1 - (9 + 16*s + 8*s2)*t2 + 
               (s - 6*(4 + 3*s2))*Power(t2,2) + 
               (-142 + 14*s + 17*s2)*Power(t2,3) + 
               (10 + s + 9*s2)*Power(t2,4) + 12*Power(t2,5)) - 
            t2*(s*(-12 - t2 + 12*Power(t2,2) + Power(t2,3)) + 
               t2*(14 + 23*t2 - 52*Power(t2,2) + 21*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*(-4 - 6*t2 - 13*Power(t2,2) + 21*Power(t2,3) + 
                  2*Power(t2,4))))))/
     ((-1 + s1)*s1*(-1 + t1)*(-1 + s1 + t1 - t2)*Power(-1 + t2,2)*t2*
       Power(-s1 + t2,2)) + (8*
       (2*Power(s1,4)*(s2 + t1*(-5 + t2) - s2*t2) - 
         2*Power(t1,2)*(1 + s*(-1 + t2) + 11*t2 - 4*Power(t2,2)) - 
         t1*(-1 + t2)*(2 + (-3 - 9*s + 10*s2)*t2 + 4*Power(t2,2)) + 
         t2*(1 + 34*t2 + 5*Power(t2,2) + 
            s*(-10 + 17*t2 - 7*Power(t2,2)) + 
            s2*(-4 + t2 + 3*Power(t2,2))) - 
         Power(s1,3)*(Power(t1,2)*(22 - 6*t2) + 
            s2*(-1 + 6*t1 - 2*t2)*(-1 + t2) + 
            t1*(-23 - 2*s*(-1 + t2) - 21*t2 + 4*Power(t2,2)) + 
            2*(1 - (6 + s)*t2 + (1 + s)*Power(t2,2))) + 
         Power(s1,2)*(1 + 4*Power(t1,3)*(-3 + t2) - 22*t2 - s*t2 - 
            23*Power(t2,2) + s*Power(t2,2) + 4*Power(t2,3) + 
            2*Power(t1,2)*(18 + s*(-1 + t2) + 8*t2 - 2*Power(t2,2)) + 
            s2*(-1 + t2)*(3 - 4*Power(t1,2) + 2*t2 + 2*t1*(3 + t2)) + 
            t1*(44 + 13*t2 - 9*Power(t2,2) + 
               s*(7 - 5*t2 - 2*Power(t2,2)))) + 
         s1*(3 - 8*Power(t1,3)*(-3 + t2) + (-51 - 9*s + 10*s2)*t2 + 
            (13 + 10*s - 11*s2)*Power(t2,2) + (3 - s + s2)*Power(t2,3) + 
            Power(t1,2)*(-6*s*(-1 + t2) + 4*(1 + 2*s2*(-1 + t2) + t2)) + 
            t1*(-5 + 2*s2*Power(-1 + t2,2) - 63*t2 - 24*Power(t2,2) + 
               4*Power(t2,3) + s*(13 - 20*t2 + 7*Power(t2,2)))))*
       B1(1 - s1 - t1 + t2,s1,t2))/((-1 + t1)*(s1*t1 - t2)*Power(-1 + t2,2)) \
+ (16*(Power(s1,6)*(8 - 19*t1 - 3*s2*(-1 + t2) + 4*t2 + 3*t1*t2) + 
         Power(t2,2)*(1 + t2)*(1 + s*(-1 + t2) - 4*t2 + Power(t2,2)) + 
         Power(s1,5)*(-8 + 52*t1 - 24*Power(t1,2) - 
            s2*(-13 + 8*t1 - 8*t2)*(-1 + t2) + 
            s*(-3 + 5*t1 - 4*t2)*(-1 + t2) - 25*t2 + 57*t1*t2 + 
            8*Power(t1,2)*t2 - 8*Power(t2,2) - 13*t1*Power(t2,2) + 
            Power(t2,3)) + s1*t2*
          (-3 + 5*t2 + s2*t2 + 32*Power(t2,2) + s2*Power(t2,2) + 
            31*Power(t2,3) - 2*s2*Power(t2,3) - 9*Power(t2,4) + 
            s*(-1 + t2)*(-4 + t1 - 8*t2 + 2*t1*t2 - 7*Power(t2,2) + 
               2*t1*Power(t2,2)) + 
            t1*(1 - 5*t2 - 10*Power(t2,2) - 4*Power(t2,3) + 
               2*Power(t2,4))) + 
         Power(s1,3)*(-4 + 10*t1 - 12*Power(t1,2) - 12*t2 + 24*t1*t2 - 
            20*Power(t1,2)*t2 + 147*Power(t2,2) + 78*t1*Power(t2,2) - 
            28*Power(t1,2)*Power(t2,2) - 17*Power(t2,3) + 
            6*t1*Power(t2,3) + 12*Power(t1,2)*Power(t2,3) - 
            3*Power(t2,4) - 6*t1*Power(t2,4) + Power(t2,5) - 
            s*(-1 + t2)*(-7 + 16*t2 + 28*Power(t2,2) + Power(t2,3) + 
               t1*(7 - 5*t2 - 8*Power(t2,2))) + 
            s2*(-1 + t2)*(3 + 25*Power(t2,2) + 2*Power(t2,3) - 
               4*t1*(1 + 2*t2 + 3*Power(t2,2)))) + 
         Power(s1,4)*(s2*(-1 + t2)*
             (-5 - 30*t2 - 7*Power(t2,2) + 8*t1*(1 + 2*t2)) + 
            s*(-1 + t2)*(-1 + 18*t2 + 5*Power(t2,2) - t1*(3 + 5*t2)) - 
            2*(-5 + 11*t2 - 5*Power(t2,2) - 4*Power(t2,3) + 
               Power(t2,4) + 4*Power(t1,2)*(-3 - 5*t2 + 2*Power(t2,2)) + 
               t1*(11 + 66*t2 + 18*Power(t2,2) - 7*Power(t2,3)))) - 
         Power(s1,2)*(-2 - 2*t2 - 2*s2*t2 + 27*Power(t2,2) + 
            9*s2*Power(t2,2) + 149*Power(t2,3) - 15*s2*Power(t2,3) - 
            33*Power(t2,4) + 8*s2*Power(t2,4) + Power(t2,5) + 
            4*Power(t1,2)*t2*(-3 + t2 - 3*Power(t2,2) + Power(t2,3)) - 
            t1*(-1 + t2)*(1 + (6 + 4*s2)*t2 - 2*Power(t2,2) + 
               4*(-2 + s2)*Power(t2,3) + 2*Power(t2,4)) + 
            s*(-1 + t2)*(3 - 6*t2 - 30*Power(t2,2) - 7*Power(t2,3) + 
               t1*(-5 + t2 + 10*Power(t2,2) + 2*Power(t2,3)))))*R1q(s1))/
     (Power(-1 + s1,2)*s1*(-1 + t1)*Power(-1 + t2,2)*Power(-s1 + t2,3)) - 
    (16*(-4*Power(s1,5)*(-1 + t1)*t2 + 
         Power(s1,4)*(2*Power(t1,2)*(-7 + t2)*t2 + 
            (-1 + t2)*t2*(-1 + s2 + 2*(2 + s)*t2 - 5*s2*t2 + 
               Power(t2,2)) + 
            t1*(1 + (6 - 2*s + 2*s2)*t2 + (1 + 2*s - 2*s2)*Power(t2,2) + 
               4*Power(t2,3))) + 
         Power(s1,3)*(4*Power(t1,3)*(-4 + t2)*t2 + 
            2*Power(t1,2)*(1 + (1 - 2*s + 2*s2)*t2 + 
               2*(2 + s - s2)*Power(t2,2) + 2*Power(t2,3)) + 
            t2*(1 + (-25 + 3*s - 6*s2)*t2 + 
               (3 + 8*s - 8*s2)*Power(t2,2) + 
               (-9 - 11*s + 14*s2)*Power(t2,3) - 2*Power(t2,4)) - 
            t1*t2*(3 + s - 39*t2 - 39*Power(t2,2) - s*Power(t2,2) + 
               15*Power(t2,3) + s2*(2 - 8*t2 + 6*Power(t2,2)))) + 
         Power(t2,2)*(-2*Power(t1,4)*(-3 + t2)*t2 + 
            Power(t1,3)*(2 - (13 + 2*s2)*t2 + 2*(-9 + s2)*Power(t2,2) + 
               5*Power(t2,3) + s*(-5 + 2*t2 + 3*Power(t2,2))) + 
            4*t2*(1 + 3*s2*(-1 + t2)*t2 - 29*Power(t2,2) - 
               3*Power(t2,3) + Power(t2,4) + s*(-6 - t2 + 7*Power(t2,2))\
) + Power(t1,2)*(-1 + 8*Power(t2,2) + 12*Power(t2,3) - 3*Power(t2,4) - 
               s2*t2*(-3 + 2*t2 + Power(t2,2)) + 
               s*(6 + t2 - 8*Power(t2,2) + Power(t2,3))) - 
            2*t1*t2*(4 - (25 + 4*s2)*t2 + (-35 + 6*s2)*Power(t2,2) - 
               2*(-5 + s2)*Power(t2,3) + 
               2*s*(-5 + 2*t2 + 2*Power(t2,2) + Power(t2,3)))) + 
         s1*t2*(Power(t1,2)*(1 + (5 - 11*s - 2*s2)*t2 + 
               (-67 + 4*s - 4*s2)*Power(t2,2) + 
               (-55 + 7*s + 6*s2)*Power(t2,3) + 20*Power(t2,4)) - 
            2*t2*(2 - 2*(-1 + s + 2*s2)*t2 + 
               (-63 - 6*s + 6*s2)*Power(t2,2) + 
               (-35 + 6*s)*Power(t2,3) + 2*(4 + s - s2)*Power(t2,4)) + 
            Power(t1,3)*(-3 + 13*t2 + 27*Power(t2,2) - 9*Power(t2,3) + 
               s*(-1 + Power(t2,2))) - 
            2*t1*t2*(-5 + (23 - 3*s2)*t2 - 4*(1 + s2)*Power(t2,2) + 
               (-11 + 7*s2)*Power(t2,3) + Power(t2,4) - 
               s*(8 + t2 - 16*Power(t2,2) + 7*Power(t2,3)))) + 
         Power(s1,2)*(2*Power(t1,4)*(-3 + t2)*t2 + 
            Power(t1,3)*(1 + 2*s*(-1 + t2)*t2 - 2*s2*(-1 + t2)*t2 + 
               7*Power(t2,2)) - 
            Power(t1,2)*t2*(7 - 2*s*(-1 + t2) + s2*Power(-1 + t2,2) - 
               57*t2 - 49*Power(t2,2) + 23*Power(t2,3)) + 
            t1*t2*(-2 + (-2 + s - 12*s2)*t2 + 
               3*(-41 + 2*s - 2*s2)*Power(t2,2) + 
               (-38 - 7*s + 18*s2)*Power(t2,3) + 13*Power(t2,4)) + 
            Power(t2,2)*(3 + 14*t2 - 60*Power(t2,2) + 18*Power(t2,3) + 
               Power(t2,4) + s2*
                (4 + 7*t2 + 2*Power(t2,2) - 13*Power(t2,3)) + 
               s*(6 - 19*t2 + 13*Power(t2,3)))))*R1q(t2))/
     ((-1 + t1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       Power(-1 + t2,2)*t2*Power(-s1 + t2,3)) + 
    (16*(6*Power(t1,4)*(-3 + t2) + 
         Power(t1,3)*(34 + s*(-1 + t2) - 6*s2*(-1 + t2) + 27*t2 - 
            9*Power(t2,2)) + Power(s1,2)*
          (-6 + 34*t1 - 40*Power(t1,2) + s*(-1 + t1 - 6*t2)*(-1 + t2) - 
            3*s2*(-3 + 4*t1 - 2*t2)*(-1 + t2) - 9*t2 + 35*t1*t2 + 
            12*Power(t1,2)*t2 - 4*Power(t2,2) - 9*t1*Power(t2,2) - 
            Power(t2,3)) - 4*(1 - (3 + 4*s + 4*s2)*t2 + 
            (-32 + 3*s + 5*s2)*Power(t2,2) + (-3 + s - s2)*Power(t2,3) + 
            Power(t2,4)) + Power(s1,3)*
          (-3*s2*(-1 + t2) + 4*t2 + t1*(-11 + 3*t2)) + 
         Power(t1,2)*(-16 + 31*t2 - 22*Power(t2,2) + 3*Power(t2,3) + 
            s*(1 + 5*t2 - 6*Power(t2,2)) + 3*s2*(-3 + t2 + 2*Power(t2,2))\
) + 2*t1*(6 - 54*t2 - 52*Power(t2,2) + 12*Power(t2,3) - 
            s2*Power(-1 + t2,2)*(-1 + 2*t2) + 
            s*(-1 - 6*t2 + 5*Power(t2,2) + 2*Power(t2,3))) + 
         s1*(8 - 22*t1 + 68*Power(t1,2) - 47*Power(t1,3) - 20*t2 - 
            6*t1*t2 + 58*Power(t1,2)*t2 + 15*Power(t1,3)*t2 - 
            48*Power(t2,2) - 14*t1*Power(t2,2) - 
            18*Power(t1,2)*Power(t2,2) + 12*Power(t2,3) + 
            2*t1*Power(t2,3) - 
            s2*(-1 + t2)*(2 + 15*Power(t1,2) + 6*t2 + 4*Power(t2,2) - 
               6*t1*(3 + 2*t2)) + 
            2*s*(-1 + t2)*(1 + Power(t1,2) + 9*t2 + 2*Power(t2,2) - 
               t1*(1 + 6*t2))))*R2q(1 - s1 - t1 + t2))/
     ((-1 + t1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       (-1 + s1 + t1 - t2)*Power(-1 + t2,2)) - 
    (8*(2*Power(s1,6)*(s2 + t1*(-5 + t2) - s2*t2) + 
         2*Power(t1,5)*(1 + s*(-1 + t2) + 5*t2 - 2*Power(t2,2)) + 
         Power(t1,4)*(-2 + (-17 + 7*s - 6*s2)*t2 + 
            (-21 - 7*s + 6*s2)*Power(t2,2) + 8*Power(t2,3)) + 
         8*t2*(1 - 2*(5 + 2*s - s2)*t2 + (-25 + s + s2)*Power(t2,2) + 
            (-8 + 3*s - 3*s2)*Power(t2,3) + 2*Power(t2,4)) + 
         Power(t1,3)*t2*(-29 - 36*t2 + 29*Power(t2,2) - 4*Power(t2,3) + 
            s2*(8 + t2 - 9*Power(t2,2)) + s*(4 - 13*t2 + 9*Power(t2,2))) \
+ Power(s1,5)*(-(s2*(-3 + 12*t1 - 2*t2)*(-1 + t2)) + 
            4*Power(t1,2)*(-13 + 3*t2) + 
            t1*(21 + 2*s*(-1 + t2) + 23*t2 - 4*Power(t2,2)) - 
            2*(1 - (6 + s)*t2 + (1 + s)*Power(t2,2))) + 
         2*Power(t1,2)*t2*(21 - 9*(-6 + s)*t2 + 
            (43 + 11*s)*Power(t2,2) - 2*(7 + s)*Power(t2,3) + 
            s2*(2 + 5*t2 - 9*Power(t2,2) + 2*Power(t2,3))) + 
         4*t1*t2*(-5 + 20*t2 + 3*Power(t2,2) - 12*Power(t2,3) + 
            2*Power(t2,4) + s*
             (1 + 7*t2 - 4*Power(t2,2) - 4*Power(t2,3)) + 
            s2*(-1 - 11*t2 + 8*Power(t2,2) + 4*Power(t2,3))) + 
         Power(s1,4)*(3 - (22 + 3*s + 12*s2)*t2 + 
            (-25 + 3*s + 12*s2)*Power(t2,2) + 4*Power(t2,3) + 
            4*Power(t1,3)*(-27 + 7*t2) + 
            Power(t1,2)*(79 + 8*s*(-1 + t2) - 28*s2*(-1 + t2) + 93*t2 - 
               20*Power(t2,2)) + 
            t1*(1 + 94*t2 - 31*Power(t2,2) + 
               s*(5 + 7*t2 - 12*Power(t2,2)) + 
               3*s2*(-5 + t2 + 4*Power(t2,2)))) + 
         Power(s1,3)*(16*Power(t1,4)*(-7 + 2*t2) + 
            Power(t1,3)*(111 + 12*s*(-1 + t2) - 32*s2*(-1 + t2) + 
               141*t2 - 36*Power(t2,2)) + 
            Power(t1,2)*(33 + 226*t2 - 95*Power(t2,2) + 4*Power(t2,3) + 
               s*(13 + 11*t2 - 24*Power(t2,2)) + 
               3*s2*(-9 + t2 + 8*Power(t2,2))) - 
            t2*(1 + 42*t2 - 19*Power(t2,2) + 
               s*(2 + 13*t2 - 15*Power(t2,2)) + 
               s2*(-22 + 7*t2 + 15*Power(t2,2))) - 
            t1*(13 + (187 + 4*s)*t2 + 240*Power(t2,2) - 
               4*(12 + s)*Power(t2,3) + 
               4*s2*(1 + 10*t2 - 12*Power(t2,2) + Power(t2,3)))) + 
         s1*(4*Power(t1,6)*(-3 + t2) - 
            4*t1*(2 + (-34 - 8*s + 2*s2)*t2 + 
               (-101 + s - 5*s2)*Power(t2,2) + 
               (-77 + 5*s + 5*s2)*Power(t2,3) + 
               2*(9 + s - s2)*Power(t2,4)) + 
            2*Power(t1,5)*(s*(-1 + t2) + 
               2*(4 + s2 + 6*t2 - s2*t2 - 2*Power(t2,2))) + 
            Power(t1,4)*(29 + 94*t2 - 47*Power(t2,2) + 4*Power(t2,3) + 
               s*(-1 + 7*t2 - 6*Power(t2,2)) + 6*s2*(-1 + Power(t2,2))) \
- Power(t1,3)*(39 + (173 - 16*s)*t2 + 4*(47 + 5*s)*Power(t2,2) - 
               4*(14 + s)*Power(t2,3) + 
               4*s2*(1 + 7*t2 - 9*Power(t2,2) + Power(t2,3))) + 
            4*t2*(-3 + 22*t2 - 13*Power(t2,2) - 16*Power(t2,3) + 
               2*Power(t2,4) + 
               s*(1 + 5*t2 - 4*Power(t2,2) - 2*Power(t2,3)) + 
               s2*(-1 - 13*t2 + 12*Power(t2,2) + 2*Power(t2,3))) + 
            Power(t1,2)*(20 - 147*t2 - 118*Power(t2,2) + 
               125*Power(t2,3) - 16*Power(t2,4) + 
               s2*(4 + 62*t2 - 37*Power(t2,2) - 29*Power(t2,3)) + 
               s*(-4 - 34*t2 + 9*Power(t2,2) + 29*Power(t2,3)))) + 
         Power(s1,2)*(2*Power(t1,5)*(-29 + 9*t2) + 
            Power(t1,4)*(69 + 8*s*(-1 + t2) - 18*s2*(-1 + t2) + 95*t2 - 
               28*Power(t2,2)) + 
            Power(t1,3)*(57 + 228*t2 - 109*Power(t2,2) + 8*Power(t2,3) + 
               s*(9 + 11*t2 - 20*Power(t2,2)) + 
               s2*(-21 + t2 + 20*Power(t2,2))) + 
            2*t2*(3 + (56 + 11*s)*t2 + (75 - 9*s)*Power(t2,2) - 
               2*(7 + s)*Power(t2,3) + 
               s2*(2 + t2 - 5*Power(t2,2) + 2*Power(t2,3))) - 
            Power(t1,2)*(53 + (321 - 8*s)*t2 + 
               2*(191 + 8*s)*Power(t2,2) - 4*(23 + 2*s)*Power(t2,3) + 
               s2*(8 + 50*t2 - 66*Power(t2,2) + 8*Power(t2,3))) + 
            t1*(12 - 111*t2 - 76*Power(t2,2) + 131*Power(t2,3) - 
               12*Power(t2,4) + 
               s2*(4 + 76*t2 - 45*Power(t2,2) - 35*Power(t2,3)) + 
               s*(-4 - 40*t2 + 9*Power(t2,2) + 35*Power(t2,3)))))*
       T2q(t2,1 - s1 - t1 + t2))/
     ((-1 + t1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)) + 
    (8*(2*Power(s1,6)*(s2 + t1*(-5 + t2) - s2*t2) + 
         Power(s1,5)*(-(s2*(-3 + 6*t1 - 8*t2)*(-1 + t2)) + 
            Power(t1,2)*(-22 + 6*t2) + 
            t1*(25 + 2*s*(-1 + t2) + 49*t2 - 10*Power(t2,2)) - 
            2*(1 - (6 + s)*t2 + (1 + s)*Power(t2,2))) + 
         Power(s1,4)*(3 + 4*Power(t1,3)*(-3 + t2) + 
            (-20 - 3*s + 9*s2)*t2 - 3*(19 + s - s2)*Power(t2,2) + 
            2*(5 + 3*s - 6*s2)*Power(t2,3) + 
            2*Power(t1,2)*(20 + s*(-1 + t2) - 2*s2*(-1 + t2) + 39*t2 - 
               11*Power(t2,2)) - 
            t1*(9 + 69*t2 + 92*Power(t2,2) - 18*Power(t2,3) + 
               s2*(6 + 14*t2 - 20*Power(t2,2)) + 
               s*(-5 - 3*t2 + 8*Power(t2,2)))) - 
         2*Power(s1,3)*(6*Power(t1,3)*(-2 - 3*t2 + Power(t2,2)) + 
            Power(t1,2)*(7 + 63*t2 + 53*Power(t2,2) - 15*Power(t2,3) + 
               s2*(2 + 4*t2 - 6*Power(t2,2)) + 
               s*(-5 + 2*t2 + 3*Power(t2,2))) + 
            t2*(3 - 47*t2 - 49*Power(t2,2) + 9*Power(t2,3) + 
               s*(1 - 6*t2 + 2*Power(t2,2) + 3*Power(t2,3)) - 
               s2*(2 - 7*t2 + Power(t2,2) + 4*Power(t2,3))) - 
            t1*(-5 + 4*t2 + 21*Power(t2,2) + 43*Power(t2,3) - 
               7*Power(t2,4) + 
               s2*(2 + 6*t2 + 4*Power(t2,2) - 12*Power(t2,3)) + 
               s*(-2 - 5*t2 + Power(t2,2) + 6*Power(t2,3)))) - 
         2*Power(s1,2)*(2*Power(t1,4)*(-3 - 2*t2 + Power(t2,2)) + 
            2*Power(t1,3)*(5 + s2 + 23*t2 + 10*Power(t2,2) - 
               s2*Power(t2,2) - 4*Power(t2,3) + 
               s*(-2 + t2 + Power(t2,2))) - 
            Power(t1,2)*(2 + 35*t2 + 69*Power(t2,2) + 31*Power(t2,3) - 
               9*Power(t2,4) - 
               2*s2*(-2 - 2*t2 + Power(t2,2) + 3*Power(t2,3)) + 
               s*(-2 - 3*t2 + 2*Power(t2,2) + 3*Power(t2,3))) + 
            t2*(-6 + 10*t2 + 71*Power(t2,2) + 36*Power(t2,3) - 
               7*Power(t2,4) + 
               s*(-2 + t2 + 7*Power(t2,2) - 5*Power(t2,3) - 
                  Power(t2,4)) + 
               s2*(2 + 4*t2 - 9*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4))) + 
            t1*(-2 - t2 - 29*Power(t2,2) - 21*Power(t2,3) + 
               23*Power(t2,4) - 2*Power(t2,5) + 
               2*s2*(1 + t2 + Power(t2,2) - 3*Power(t2,4)) + 
               2*s*(1 + 3*t2 - 8*Power(t2,2) + 2*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         t2*(s*(-1 + t2)*(4 + 20*t2 + 16*Power(t2,2) + 2*Power(t2,3) + 
               Power(t2,4) + 2*Power(t1,2)*(2 + 4*t2 + Power(t2,2)) - 
               t1*(8 + 28*t2 + 12*Power(t2,2) + 3*Power(t2,3))) + 
            t2*(4 - 16*t2 - 35*Power(t2,2) - 18*Power(t2,3) + 
               Power(t2,4) - 4*Power(t1,3)*(-3 - 2*t2 + Power(t2,2)) + 
               2*Power(t1,2)*
                (-10 - 33*t2 - 9*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(4 + 74*t2 + 79*Power(t2,2) + 7*Power(t2,3) - 
                  4*Power(t2,4)) + 
               s2*(-1 + t2)*(4 + 8*t2 + 4*Power(t2,2) - Power(t2,3) + 
                  4*Power(t1,2)*(1 + t2) - 2*t1*(4 + 6*t2 + Power(t2,2)))\
)) + s1*(-2*s*(-1 + t2)*(Power(t1,3)*(2 + 4*t2) + 
               Power(t1,2)*(-4 - 18*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               t2*(2 + 4*t2 + Power(t2,2) + 3*Power(t2,3)) + 
               t1*(2 + 12*t2 + 10*Power(t2,2) - 5*Power(t2,3) - 
                  Power(t2,4))) + 
            t2*(-4 + 4*t2 + 58*Power(t2,2) + 88*Power(t2,3) + 
               18*Power(t2,4) - 4*Power(t2,5) + 
               4*Power(t1,4)*(-3 - 2*t2 + Power(t2,2)) + 
               Power(t1,3)*(8 + 60*t2 + 20*Power(t2,2) - 8*Power(t2,3)) + 
               2*Power(t1,2)*(8 + 5*t2 - 17*Power(t2,2) - 
                  10*Power(t2,3) + 2*Power(t2,4)) + 
               t1*(-8 - 66*t2 - 136*Power(t2,2) - 47*Power(t2,3) + 
                  17*Power(t2,4)) - 
               s2*(-1 + t2)*(4 + 12*t2 + 12*Power(t2,2) - 3*Power(t2,3) + 
                  4*Power(t1,3)*(1 + t2) - 
                  4*Power(t1,2)*Power(1 + t2,2) + 
                  2*t1*(-2 - 4*t2 + Power(t2,3))))))*T3q(t2,s1))/
     ((-1 + t1)*(-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)*
       Power(-s1 + t2,2)) + (8*
       (8*Power(t1,3)*(-3 + t2)*t2 + 
         2*Power(s1,5)*(s2 + t1*(-5 + t2) - s2*t2) - 
         2*Power(t1,2)*(1 + s*(-1 + t2) - (45 + 4*s2)*t2 + 
            4*(-3 + s2)*Power(t2,2) + 8*Power(t2,3)) + 
         t1*(2 - (37 + s + 30*s2)*t2 + (-153 + s + 22*s2)*Power(t2,2) + 
            4*(5 + 2*s2)*Power(t2,3) + 8*Power(t2,4)) + 
         t2*(-23 + 50*t2 + 57*Power(t2,2) - 20*Power(t2,3) + 
            s2*(20 + t2 - 21*Power(t2,2)) + s*(-2 + t2 + Power(t2,2))) + 
         Power(s1,4)*(-(s2*(-7 + 6*t1 - 2*t2)*(-1 + t2)) + 
            Power(t1,2)*(-22 + 6*t2) + 
            t1*(45 + 2*s*(-1 + t2) + 15*t2 - 4*Power(t2,2)) - 
            2*(1 - (6 + s)*t2 + (1 + s)*Power(t2,2))) - 
         Power(s1,3)*(-7 - 4*Power(t1,3)*(-3 + t2) + 50*t2 + 7*s*t2 + 
            17*Power(t2,2) - 7*s*Power(t2,2) - 4*Power(t2,3) - 
            2*Power(t1,2)*(41 + s*(-1 + t2) + t2 - 2*Power(t2,2)) + 
            2*s2*(-1 + t2)*(2*Power(t1,2) + 2*(2 + t2) - t1*(10 + t2)) + 
            t1*(57 + 38*t2 + Power(t2,2) + s*(-9 + 7*t2 + 2*Power(t2,2)))\
) + Power(s1,2)*(-8 - 20*Power(t1,3)*(-3 + t2) + 55*t2 + 6*s*t2 + 
            70*Power(t2,2) - 5*s*Power(t2,2) - 5*Power(t2,3) - 
            s*Power(t2,3) - 4*Power(t1,2)*
             (29 + 2*s*(-1 + t2) + 10*t2 - 5*Power(t2,2)) + 
            s2*(-1 + t2)*(3 + 20*Power(t1,2) - 4*t2 + Power(t2,2) - 
               4*t1*(9 + 2*t2)) + 
            t1*(7 + 22*t2 - 13*Power(t2,2) + 
               3*s*(-4 + t2 + 3*Power(t2,2)))) + 
         s1*(3 - 8*Power(t1,4)*(-3 + t2) + (6 + s - 26*s2)*t2 - 
            (117 + s - 22*s2)*Power(t2,2) + 4*(2 + s2)*Power(t2,3) + 
            4*Power(t2,4) + 8*Power(t1,3)*
             (-11 + s2*(-1 + t2) - 3*t2 + 2*Power(t2,2)) + 
            Power(t1,2)*(42 + 8*s*(-1 + t2) + 86*t2 - 8*Power(t2,3) - 
               8*s2*(-4 + 3*t2 + Power(t2,2))) + 
            t1*(13 + 68*t2 + 3*Power(t2,2) - 4*Power(t2,3) + 
               s*(5 + 3*t2 - 8*Power(t2,2)) + 
               2*s2*(-11 + 7*t2 + 4*Power(t2,2)))))*T4q(s1))/
     ((-1 + s1)*(-1 + t1)*(-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)) \
- (8*(2*Power(s1,4)*(s2 + t1*(-5 + t2) - s2*t2) - 
         2*Power(t1,3)*(1 + s*(-1 + t2) - 7*t2 + 2*Power(t2,2)) - 
         t1*(2 - (32 + 5*s + 10*s2)*t2 + (-84 + s + 9*s2)*Power(t2,2) + 
            (-10 + 4*s + s2)*Power(t2,3) + 4*Power(t2,4)) + 
         Power(t1,2)*(4 - (51 + 2*s2)*t2 + (-25 + 2*s2)*Power(t2,2) + 
            8*Power(t2,3) + s*(-2 - 3*t2 + 5*Power(t2,2))) + 
         Power(s1,3)*(8*Power(t1,2)*(-4 + t2) - 
            s2*(-5 + 8*t1 - 4*t2)*(-1 + t2) + 
            t1*(35 + 2*s*(-1 + t2) + 27*t2 - 6*Power(t2,2)) - 
            2*(1 - (6 + s)*t2 + (1 + s)*Power(t2,2))) + 
         t2*(5 - 49*t2 - 21*Power(t2,2) + Power(t2,3) - 
            s2*(8 - 5*t2 - 4*Power(t2,2) + Power(t2,3)) + 
            s*(2 - 5*t2 + 2*Power(t2,2) + Power(t2,3))) + 
         Power(s1,2)*(5 - 36*t2 - 5*s*t2 - 31*Power(t2,2) + 
            3*s*Power(t2,2) + 6*Power(t2,3) + 2*s*Power(t2,3) + 
            2*Power(t1,3)*(-17 + 5*t2) + 
            Power(t1,2)*(67 + 4*s*(-1 + t2) + 51*t2 - 14*Power(t2,2)) - 
            s2*(-1 + t2)*(3 + 10*Power(t1,2) + 5*t2 + 2*Power(t2,2) - 
               5*t1*(3 + 2*t2)) - 
            t1*(32 + 30*t2 + 22*Power(t2,2) - 4*Power(t2,3) + 
               s*(-7 + t2 + 6*Power(t2,2)))) + 
         s1*(-3 + 4*Power(t1,4)*(-3 + t2) + (22 + s + 4*s2)*t2 + 
            (73 + 3*s - 5*s2)*Power(t2,2) + (16 - 4*s + s2)*Power(t2,3) - 
            4*Power(t2,4) + 2*Power(t1,3)*
             (s*(-1 + t2) + 2*(13 + s2 + 5*t2 - s2*t2 - 2*Power(t2,2))) - 
            Power(t1,2)*(43 + 40*t2 + 17*Power(t2,2) - 4*Power(t2,3) - 
               2*s2*(-7 + 4*t2 + 3*Power(t2,2)) + 
               s*(-5 + t2 + 4*Power(t2,2))) + 
            t1*(6 - 18*t2 - 41*Power(t2,2) + 13*Power(t2,3) - 
               2*s2*(-5 + 2*t2 + 2*Power(t2,2) + Power(t2,3)) + 
               s*(-5 - 4*t2 + 7*Power(t2,2) + 2*Power(t2,3)))))*
       T5q(1 - s1 - t1 + t2))/
     ((-1 + t1)*(-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)));
   return a;
};
