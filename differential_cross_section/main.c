#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>


#include <gsl/gsl_math.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_monte_miser.h>
#include <gsl/gsl_monte_vegas.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_pow_int.h>
#include "nlo_functions.h"


#define Pi M_PI
#define Power gsl_pow_int

#define E_MIN_GAMMA 30.
#define FILE_RESULT "dist.txt"
#define Ndef 10000

#define NLO1_1   1
#define NLO1_2   1
#define NLO2     1
#define NLO3     1




double nlo1(double *);
double nlo1_2(double *);
double nlo2(double *);
double nlo3(double *);
double nlo_qcd(double *);
double born(double *);


double PhaseVol(double, double, double, double);
double t2_func(double,double,double,double,double);
int HevTheta(double);
double Gk(double,double,double,double,double,double);
double f_lambda(double, double, double);
double delta4(double,double,double,double,double);



double C(double s,double s1,double s2,double t1,double t2);

void display_results(char *, double, double);

int save(int,double *);



double born_mc(double *, size_t, void *);
double nlo1_mc(double *, size_t, void *);
double nlo1_2_mc(double *, size_t, void *);
double nlo2_mc(double *, size_t, void *);
double nlo3_mc(double *, size_t, void *);
double nlo_qcd_mc(double *, size_t, void *);






int main(int argc, char *argv[])
{
    time_t Ts, Tf;
    size_t N;
    
    int Tdist=1;
    
    double mu, Ecut, Ecm, me, Const, mtau, mQ, m_up;
    double x;
    mu=105.6583745;     // Muon mass (MeV)
    me=0.511/mu;       // Electron mass 
    mtau=1776.84/mu;    // Tau mass
    //m_pi=134.9766/mu;
    m_up=2./mu;
    //m_d=4.5/mu;
    //m_s=104./mu;
    //m_c=1270./mu;
    //m_b=4200./mu;
    mQ=300./mu;
    Ecut=E_MIN_GAMMA/mu;	// Cut for invisible gamma
    
    //Const=Power(4*Pi/137.,3)/Power(mu,2)*0.00257*Power(10.,12); //  pb
    Const=24./137.*Power(Pi*me,2)*Power(10.,9)*0.6652; //nb
    
    if(argc==1)
    {
        Tdist=1;
        Ecm=500.;// Energy of center mass
        x=0.5;
        N=Ndef;
        
    };
        
    if(argc==2)
    {
        Tdist=atoi(argv[1]);
        Ecm=500;
        x=0.5;
        N=Ndef;
    };
    
    if(argc==3)
    {
        Tdist=atoi(argv[1]);
        Ecm=atof(argv[2]);
        x=0.5;
        N=Ndef;
    };
    
    if(argc==4)
    {
        Tdist=atoi(argv[1]);
        Ecm=atof(argv[2]);
        x=atof(argv[3]);
        N=Ndef;
    };
    
    if(argc==5)
    {
        Tdist=atoi(argv[1]);
        Ecm=atof(argv[2]);
        x=atof(argv[3]);
        N=atoi(argv[4]);
    };

    if( Tdist>3 || Tdist<1 ) return 0;
    
    double s = 4*Power(Ecm/mu,2);
    
    
    
    
    
    //double si_cut = 2.+E_MIN_GAMMA*Ecm/(mu*mu);
    //double si_cut2 = (si_cut-1.)/3.+1.;
    //double si_cut2=2.*si_cut;
    
    double si_max, t1_min, t1_max, si_cut1, si_cut2;
    
    si_max = Power(sqrt(s)-1.,2);
    t1_min = (2-s-sqrt(s*(s-2)))/2;
    t1_max = (2-s+sqrt(s*(s-2)))/2;
    si_cut2 = 2.+E_MIN_GAMMA*Ecm/(mu*mu);
    si_cut1 = (si_cut2-1.)/3.+1.;
    
    //si_min_2 = 1.;
    //si_max_2 = si_cut;
    
    

    
    // integration limits
    double xl[4], xu[4];
    double xl_1[4], xu_1[4];
    double xl_2[4], xu_2[4];
    double xl_3[4], xu_3[4];
    
        
    // s2
    xl[0] = 1.;
    xu[0] = si_max;
    // t1
    xl[1] = t1_min;
    xu[1] = t1_max;
    // lambda
    xl[2] = 0.;
    xu[2] = 2.*Pi;
    
        
    // s2
    xl_1[0] = 1.;
    xu_1[0] = si_cut1;
    // t1
    xl_1[1] = t1_min;
    xu_1[1] = t1_max;
    // lambda
    xl_1[2] = 0.;
    xu_1[2] = 2.*Pi;
    
    // s2
    xl_2[0] = si_cut1;
    xu_2[0] = si_cut2;
    // t1
    xl_2[1] = t1_min;
    xu_2[1] = t1_max;
    // lambda
    xl_2[2] = 0.;
    xu_2[2] = 2.*Pi;
    
    // s2
    xl_3[0] = si_cut2;
    xu_3[0] = si_max;
    // t1
    xl_3[1] = t1_min;
    xu_3[1] = t1_max;
    // lambda
    xl_3[2] = 0.;
    xu_3[2] = 2.*Pi;
    

    
    
    // initialisation of random numbers generator
    const gsl_rng_type *T;
    gsl_rng *r;
    gsl_rng_env_setup ();
    T = gsl_rng_default;
    r = gsl_rng_alloc (T);

    
    
    double res, err;
    double res1, err1, res2, err2, res3, err3;
    double tres1, terr1;
    double results[30];
    double params[5];
    gsl_monte_function func;
    gsl_monte_vegas_state *state;
    
    func.dim = 3;
    func.params = &params;
    
    int i;
    for(i=0;i<30;i++) results[i]=0.;
    
    
    params[0] = s; 
    params[1] = Tdist;
    params[2] = x*s;
    params[3] = Ecut;
    
    
    
    //################## BORN #########################

    func.f = &born_mc;
    
    
    
    gsl_rng_set(r,time(NULL));
    
    state = gsl_monte_vegas_alloc(3);
    gsl_monte_vegas_integrate (&func, xl, xu, 3, 1000, r, state, &res, &err);
    gsl_monte_vegas_integrate (&func, xl, xu, 3, N, r, state, &res, &err);
    
    gsl_monte_vegas_free(state);
    
    
    
    results[0]=Ecm;
    results[1]=Tdist;
    results[2]=x;
    results[3]=res*Const*s;
    results[4]=err*Const*s;
    
    
    printf("==========Born============\n");
    printf("E gamma = %.1f  x=%.2f type N %i\n",Ecm,x,Tdist);
    printf("%.6f + pm %.6f nb\n",results[3],results[4]);
    
    double Cb=Const*s*4*Pi/137.;
 
    //################## NLO_1 ###################################
   
#if NLO1_1
    printf("========== NLO 1 ===============\n");    
    
    
    func.f = &nlo1_mc;   
    
    
    Ts=time(NULL);
        
    
    gsl_rng_set(r,time(NULL));
    state = gsl_monte_vegas_alloc(3);
    gsl_monte_vegas_integrate (&func, xl_1, xu_1, 3, 1000, r, state, &res1, &err1);
    gsl_monte_vegas_integrate (&func, xl_1, xu_1, 3, N, r, state, &res1, &err1);
    gsl_monte_vegas_free(state);
  
    
    gsl_rng_set(r,time(NULL));
    state = gsl_monte_vegas_alloc(3);
    gsl_monte_vegas_integrate (&func, xl_2, xu_2, 3, 1000, r, state, &res2, &err2);
    gsl_monte_vegas_integrate (&func, xl_2, xu_2, 3, N, r, state, &res2, &err2);
    gsl_monte_vegas_free(state);
    
    
    gsl_rng_set(r,time(NULL));
    state = gsl_monte_vegas_alloc(3);
    gsl_monte_vegas_integrate (&func, xl_3, xu_3, 3, 1000, r, state, &res3, &err3);
    gsl_monte_vegas_integrate (&func, xl_3, xu_3, 3, N, r, state, &res3, &err3);
    gsl_monte_vegas_free(state);
        
    Tf=time(NULL);
    
    
    
    printf("---- part 1 ---\n");
    printf("Calc time: %f h\n",difftime(Tf,Ts)/3600.);
    printf("N1 \n %.6f    %.6f\n", res1*Cb, err1*Cb);
    printf("N2 \n %.6f    %.6f\n", res2*Cb, err2*Cb);
    printf("N3 \n %.6f    %.6f\n", res3*Cb, err3*Cb);
    
    
    tres1=res1+res2+res3;
    terr1=sqrt(err1*err1+err2*err2+err3*err3);
    
    results[5]=tres1*Cb;
    results[6]=terr1*Cb;
        
    printf("nlo1 part1: %.10f  pm %.10f\n",results[5],results[6]);   
    printf("-------\n");
#endif
  
    //#################### NLO 1 part 2  (4 diagrams of box)############################
    
#if NLO1_2
    
    
    func.f = &nlo1_2_mc;   
    
    
    Ts=time(NULL);
/*    
    gsl_rng_set(r,time(NULL));
    mstate = gsl_monte_miser_alloc(3);
    gsl_monte_miser_integrate(&func, xl_1, xu_1, 3, N, r, mstate, &res1, &err1);
    gsl_monte_miser_free(mstate);
    
    printf("MISER N1 \n %.6f    %.6f\n", res1*Cb, err1*Cb);
*/    
    
    printf("---- part 2 ---\n");
    
    
    gsl_rng_set(r,time(NULL));
    state = gsl_monte_vegas_alloc(3);
    gsl_monte_vegas_integrate (&func, xl_1, xu_1, 3, 1000, r, state, &res1, &err1);
    gsl_monte_vegas_integrate (&func, xl_1, xu_1, 3, N, r, state, &res1, &err1);
    gsl_monte_vegas_free(state);
    
    printf("N1 \n %.6f    %.6f\n", res1*Cb, err1*Cb);
  
    
    
    gsl_rng_set(r,time(NULL));
    state = gsl_monte_vegas_alloc(3);
    gsl_monte_vegas_integrate (&func, xl_2, xu_2, 3, 1000, r, state, &res2, &err2);
    gsl_monte_vegas_integrate (&func, xl_2, xu_2, 3, N, r, state, &res2, &err2);
    gsl_monte_vegas_free(state);
    
    printf("N2 \n %.6f    %.6f\n", res2*Cb, err2*Cb);
    
    
    gsl_rng_set(r,time(NULL));
    state = gsl_monte_vegas_alloc(3);
    gsl_monte_vegas_integrate (&func, xl_3, xu_3, 3, 1000, r, state, &res3, &err3);
    gsl_monte_vegas_integrate (&func, xl_3, xu_3, 3, N, r, state, &res3, &err3);
    gsl_monte_vegas_free(state);
        
    printf("N3 \n %.6f    %.6f\n", res3*Cb, err3*Cb);
    
    Tf=time(NULL);
    
    
    

    
    printf("Calc time: %f h\n",difftime(Tf,Ts)/3600.);
    
    
    
    
    
    tres1=res1+res2+res3;
    terr1=sqrt(err1*err1+err2*err2+err3*err3);
    
    results[7]=tres1*Cb;
    results[8]=terr1*Cb;
        
    printf("nlo1 part1: %.10f  pm %.10f\n",results[7],results[8]);   
    printf("-------\n");
  
#endif
    
    //################### NLO 3 #####################################

#if NLO3    
    

    func.f = &nlo3_mc;
    
    gsl_rng_set(r,time(NULL));
    state = gsl_monte_vegas_alloc(3);
    gsl_monte_vegas_integrate (&func, xl, xu, 3, 1000, r, state, &res, &err);
    gsl_monte_vegas_integrate (&func, xl, xu, 3, N, r, state, &res, &err);
    
    gsl_monte_vegas_free(state);
    
    results[9]=res*Cb;
    results[10]=err*Cb;
    
    printf("========== NLO 3 ================\n");
    printf("%.6f + pm %.6f \n",results[9],results[10]);
    
    
#endif
    //################### NLO_2###################################

#if NLO2
    
    func.f = &nlo2_mc;
    
    Ts=time(NULL);
    
    printf("========= NLO 2 ===========\n");

    params[3] = me;       
    gsl_rng_set(r,time(NULL));
    state = gsl_monte_vegas_alloc(3);
    gsl_monte_vegas_integrate (&func, xl, xu, 3, 1000, r, state, &res, &err);
    gsl_monte_vegas_integrate (&func, xl, xu, 3, N, r, state, &res, &err);
    
    results[11]=res*Cb;
    results[12]=err*Cb;
    
    printf("nlo2, e\n %.6f    %.6f\n",results[11],results[12]);
    gsl_monte_vegas_free(state);
       
 
    params[3] = 1.;     
    gsl_rng_set(r,time(NULL));
    state = gsl_monte_vegas_alloc(3);
    gsl_monte_vegas_integrate (&func, xl, xu, 3, 1000, r, state, &res, &err);
    gsl_monte_vegas_integrate (&func, xl, xu, 3, N, r, state, &res, &err);

    results[13]=res*Cb;
    results[14]=err*Cb;
    
    printf("nlo2, mu\n %.6f    %.6f\n",results[13],results[14]);
    gsl_monte_vegas_free(state);


    params[3] = mtau;
    gsl_rng_set(r,time(NULL));
    state = gsl_monte_vegas_alloc(3);
    gsl_monte_vegas_integrate (&func, xl, xu, 3, 1000, r, state, &res, &err);
    gsl_monte_vegas_integrate (&func, xl, xu, 3, N, r, state, &res, &err);
   
    results[15]=res*Cb;
    results[16]=err*Cb;
    
    printf("nlo2, tau\n %.6f    %.6f\n",results[15],results[16]);
    gsl_monte_vegas_free(state);
    
      
    printf("======= NLO2 quarks ===========\n");
    
    params[3] = mQ;
    gsl_rng_set(r,time(NULL));
    state = gsl_monte_vegas_alloc(3);
    gsl_monte_vegas_integrate (&func, xl, xu, 3, 1000, r, state, &res, &err);
    gsl_monte_vegas_integrate (&func, xl, xu, 3, N, r, state, &res, &err);
    
    results[17]=res*Cb*(pow(2./3.,4)+pow(1./3.,4)+pow(1./3.,4));
    results[18]=err*Cb*(pow(2./3.,4)+pow(1./3.,4)+pow(1./3.,4));
        
    printf("3q with m=300 MeV %.6f    %.6f\n",results[17],results[18]);
    gsl_monte_vegas_free(state);
    
    
    params[3] = m_up;
    gsl_rng_set(r,time(NULL));
    state = gsl_monte_vegas_alloc(3);
    gsl_monte_vegas_integrate (&func, xl, xu, 3, 1000, r, state, &res, &err);
    gsl_monte_vegas_integrate (&func, xl, xu, 3, N, r, state, &res, &err);
    
    results[19]=res*Cb*(pow(2./3.,4));
    results[20]=err*Cb*(pow(2./3.,4));
    
    Tf=time(NULL);
    
    
    printf("up quark  %.6f    %.6f\n",results[19],results[20]);
    gsl_monte_vegas_free(state);
    
    printf("Calc time: %f h\n",difftime(Tf,Ts)/3600.);
#endif

    save(21,results);
    
     

    
    gsl_rng_free (r);
    
    return 1;
};


void display_results(char *title, double result, double error)
{
  printf ("%s ==================\n", title);
  printf ("result = % .6f\n", result);
  printf ("sigma  = % .6f\n", error);
};



int save(int len,double *Mres)
{
    FILE *file;
    int i;
    file=fopen(FILE_RESULT,"a");
    for(i=0;i<len;i++)
    {
        fprintf(file,"%.10f  ",Mres[i]);
    };
    fprintf(file,"\n");
    
    fclose(file);
    return 1;
};


double f_lambda(double x, double y, double z)
{
    return Power(x-y-z,2)-4*y*z;
};

double Gk(double x,double y,double z,double u,double v,double w)
{
    return Power(v,2)*w + Power(u,2)*z + u*((w - x)*(-v + y) - (v + w + x + y)*z + Power(z,2)) + x*(y*(x + y - z) + w*(-y + z)) + v*(Power(w,2) + y*(-x + z) - w*(x + y + z)) ;
};


double PhaseVol(double s,double s1, double s2, double t1)
{
    return Pi/( 8.*sqrt(f_lambda(s,0.,0.)*f_lambda(s,s2,1.)) );
};


double t2_func(double s,double s1,double s2,double t1,double lam)
{
	return 1.+(2*sqrt(Gk(s,t1,s2,0.,0.,1.)*Gk(s1,s2,s,0.,1.,1.))*cos(lam) + (-1 + s1)*(-1 + s2)*(s2 - t1) + Power(s,2)*(-1 + t1) - s*(-1 + (2 + s1)*t1 + s2*(-4 + s1 + t1)) )/f_lambda(s,s2,1.)  ;
};


int HevTheta(double a)
{
    if (a<0) return 0;
    else return 1;
};



double born_mc(double *inv, size_t dim, void *params)
{
    (void)(dim);
    double *pars = (double *)params;
    double vars[5];
    double s, s1, s2, t1, t2, lam; // variables
    double result;
    int flag;
            
    t1=inv[1];
    lam=inv[2];
    
    
    s=pars[0];
    flag=(int)(pars[1]);
    
    switch(flag)
    {
        case 1:
            s1 = pars[2];
            s2 = inv[0];
            break;
        case 2:
            s1 = inv[0];
            s2 = pars[2];
            break;
        case 3:
            s2 = inv[0];
            s1 = s - s2 + 2. - pars[2];
            break;
    };
    
    
    
    
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        if( cuts(s,s1,s2,t1,t2) )
        {
            vars[0] = s;
            vars[1] = s1;
            vars[2] = s2;
            vars[3] = t1;
            vars[4] = t2;
            result = born(vars)*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );        
        }
        else result=0.;
    }
    else  result = 0.;
    
    return result;
};





double nlo1_mc(double *inv, size_t dim, void *params)
{
    (void)(dim);
    double *pars = (double *)params;
    double vars[7];
    double s, s1, s2, t1, t2, lam; // variables
    double result;
    int flag;
            
    t1=inv[1];
    lam=inv[2];
    
    
    s=pars[0];
    flag=(int)(pars[1]);
    
    switch(flag)
    {
        case 1:
            s1 = pars[2];
            s2 = inv[0];
            break;
        case 2:
            s1 = inv[0];
            s2 = pars[2];
            break;
        case 3:
            s2 = inv[0];
            s1 = s - s2 + 2. - pars[2];
            break;
    };
    
    
        
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        if( cuts(s,s1,s2,t1,t2) )
        {
            vars[0] = s;
            vars[1] = s1;
            vars[2] = s2;
            vars[3] = t1;
            vars[4] = t2;
            result = nlo1(vars)*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );   
            if(isnanl(result)!=0) printf("NAN nlo1 func: s1=%e  s2=%e  t1=%e  t2=%e  nlo1=%e\n",s1,s2,t1,t2, nlo1(vars) );
        }
        else result=0.;
        
        
    }
    else
    {
        result = 0.;
    };
    
    return result;

};

double nlo2_mc(double *inv, size_t dim, void *params)
{
    (void)(dim);
    double *pars = (double *)params;
    double vars[6];
    double s, s1, s2, t1, t2, lam, m; // variables
    double result;
    int flag;
            
    t1=inv[1];
    lam=inv[2];
    
    
    s=pars[0];
    flag=(int)(pars[1]);
    m=pars[3];
    
    switch(flag)
    {
        case 1:
            s1 = pars[2];
            s2 = inv[0];
            break;
        case 2:
            s1 = inv[0];
            s2 = pars[2];
            break;
        case 3:
            s2 = inv[0];
            s1 = s - s2 + 2. - pars[2];
            break;
    };
    
    
        
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        if( cuts(s,s1,s2,t1,t2) )
        {
            vars[0] = s;
            vars[1] = s1;
            vars[2] = s2;
            vars[3] = t1;
            vars[4] = t2;
            vars[5] = m;
            result = nlo2(vars)*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );   
            //if(isnanl(result)!=0) printf("NAN nlo2 func: s1=%e  s2=%e  t1=%e  t2=%e  nlo2=%e\n",s1,s2,t1,t2, nlo2(vars) );
        }
        else result=0.;
    }
    else  result = 0.;
        
    return result;
};

double nlo3_mc(double *inv, size_t dim, void *params)
{
    (void)(dim);
    double *pars = (double *)params;
    double vars[7];
    double s, s1, s2, t1, t2, lam, omega ; // variables
    double result;
    int flag;
            
    t1=inv[1];
    lam=inv[2];
    
    
    s=pars[0];
    flag=(int)(pars[1]);
    omega=pars[3];
    
    switch(flag)
    {
        case 1:
            s1 = pars[2];
            s2 = inv[0];
            break;
        case 2:
            s1 = inv[0];
            s2 = pars[2];
            break;
        case 3:
            s2 = inv[0];
            s1 = s - s2 + 2. - pars[2];
            break;
    };
    
    
        
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        if( cuts(s,s1,s2,t1,t2) )
        {
            vars[0] = s;
            vars[1] = s1;
            vars[2] = s2;
            vars[3] = t1;
            vars[4] = t2;
            vars[5] = omega;
            result = nlo3(vars)*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );   
            if(isnanl(result)!=0) printf("NAN nlo1 func: s1=%e  s2=%e  t1=%e  t2=%e  nlo1=%e\n",s1,s2,t1,t2, nlo1(vars) );
        }
        else result=0.;
        
        
    }
    else
    {
        result = 0.;
    };
    
    return result;

};



double nlo1_2_mc(double *inv, size_t dim, void *params)
{
    (void)(dim);
    double *pars = (double *)params;
    double vars[7];
    double s, s1, s2, t1, t2, lam; // variables
    double result;
    int flag;
            
    t1=inv[1];
    lam=inv[2];
    
    
    s=pars[0];
    flag=(int)(pars[1]);
    
    switch(flag)
    {
        case 1:
            s1 = pars[2];
            s2 = inv[0];
            break;
        case 2:
            s1 = inv[0];
            s2 = pars[2];
            break;
        case 3:
            s2 = inv[0];
            s1 = s - s2 + 2. - pars[2];
            break;
    };
    
    
        
    if( Gk(s,t1,s2,0.,0.,1.)<0 && Gk(s1,s2,s,0.,1.,1.)<0 ) 
    {
        t2 = t2_func(s,s1,s2,t1,lam);
        if( cuts(s,s1,s2,t1,t2) )
        {
            vars[0] = s;
            vars[1] = s1;
            vars[2] = s2;
            vars[3] = t1;
            vars[4] = t2;
            result = nlo1_2(vars)*PhaseVol(s,s1,s2,t1)/( 2*s*Power(2*Pi,5) );   
            if(isnanl(result)!=0) printf("NAN nlo1_2 func: s1=%e  s2=%e  t1=%e  t2=%e\n",s1,s2,t1,t2);
        }
        else result=0.;
        
        
    }
    else
    {
        result = 0.;
    };
    
    return result;

};